<?php

define('DO_CACHE', true);

define('DO_WATERMARK', true);

define('DEBUG', true);

define('IMAGE_QUALITY', 55);

$act = isset($_REQUEST['a']) ? $_REQUEST['a'] : null;



if ( ! is_null($act) ) {

	header('Content-Type: text/html');

	process_action($act);

	die;

}



function __error($msg)

{

	if ( defined('DEBUG') && DEBUG ) {

		echo date('[H:i:s] ') . $msg . "<br/>\n";

		print_r(debug_backtrace());

	}



	die;

}



// unsharp library

@include_once 'unsharp.inc.php';

$plugins = array(

	'unsharp' => 'plugin_unsharp'

);



//Thumb Size

/*

 big : 198 x 25

 medium : 148 x 18

 thumb : 78 x 11

 */



$size_map = array(
	'cads_orig' => array('width' => 1024, 'height' => 1024, 'action' => 'fitbox2', 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'cads_thumb' => array('width' => 226, 'height' => 235, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'cads_thumb2' => array('width' => 170, 'height' => 176, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'cads_small' => array('width' => 85, 'height' => 110, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'thumb_ed' => array('width' => 190, 'height' => 300, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'thumb_ed_v2' => array('width' => 233, 'height' => 283, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	//'thumb_ed_v3' => array('width' => 263, 'height' => 319, 'action' => 'crop', 'use_crop_args' => false, 'unsharp' => array(100, 1, 0)),
	'thumb_ed_v3' => array('width' => 221, 'height' => 268, 'action' => 'crop', 'use_crop_args' => false, 'unsharp' => array(100, 1, 0)),
	'thumb_ed_v3_nomark' => array('width' => 221, 'height' => 268, 'action' => 'crop', 'use_crop_args' => false, 'unsharp' => array(100, 1, 0)),

	'ppportrait_pf' => array('width' => 176, 'height' => 200, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'lllandscape_pf' => array('width' => 300, 'height' => 200, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'h200res_pf' => array('width' => 0, 'height' => 200, 'action' => 'resize', 'unsharp' => array(100, 1, 0)),
	'h250res_pf' => array('width' => 0, 'height' => 250, 'action' => 'resize', 'unsharp' => array(100, 1, 0)),

	'thumb_pf' => array('width' => 173, 'height' => 206, 'action' => 'crop', 'use_crop_args' => false, 'unsharp' => array(100, 1, 0)),
	'big_thumb_pf' => array('width' => 350, 'height' => 394, 'action' => 'crop', 'use_crop_args' => false, 'unsharp' => array(100, 1, 0)),
	'main_pf' => array('width' => 124, 'height' => 124, 'action' => 'crop', 'use_crop_args' => false, 'unsharp' => array(100, 1, 0)),
	'more_thumb_pf' => array('width' => 71, 'height' => 98, 'action' => 'crop', 'use_crop_args' => false, 'unsharp' => array(100, 1, 0)),

	'thumb_ed_agency' => array('width' => 263, 'height' => 319, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'thumb_ed_agency_xl' => array('width' => 355, 'height' => 460, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'thumb_ed_agency_list' => array('width' => 226, 'height' => 226, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'thumb_ed_agency_list_v2' => array('width' => 220, 'height' => 175, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'thumb_cropper_agency' => array('width' => 233, 'height' => 283, 'action' => 'fitbox', 'unsharp' => array(100, 1, 0)),

	//'thumb_ed_xl' => array('width' => 355, 'height' => 460, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'thumb_ed_xl' => array('width' => 297, 'height' => 385, 'action' => 'crop', 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'thumb_ed_list' => array('width' => 226, 'height' => 226, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'profile_thumb_ed' => array('width' => 254, 'height' => 508, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'profile_thumb_ed_v2' => array('width' => 314, 'height' => 512, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'p_thumb_small_v2' => array('width' => 86, 'height' => 86, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'newest_thumb' => array('width' => 118, 'height' => 148, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'sthumb-agency' => array('width' => 59, 'height' => 81, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'sthumb-agency2' => array('width' => 71, 'height' => 98, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'lthumb-agency' => array('width' => 135, 'height' => 178, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'e-thumb-agency' => array('width' => 130, 'height' => 128, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'f_thumb_ed' => array('width' => 141, 'height' => 184, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'alert_ed' => array('width' => 200, 'height' => 255, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'pp' => array('width' => 400, 'height' => 600, 'action' => 'crop'/*, 'unsharp' => array(100, 1, 0)*/, 'use_crop_args' => true),
	'pl' => array('width' => 500, 'height' => 375, 'action' => 'crop'/*, 'unsharp' => array(100, 1, 0)*/, 'use_crop_args' => true),

	'pp_ed' => array('width' => 314, 'height' => 512, 'action' => 'crop'/*, 'unsharp' => array(100, 1, 0)*/, 'use_crop_args' => true),
	'orig_2' => array('width' => 550, 'height' => 650, 'action' => 'fitbox'),
	'pl_ed' => array('width' => 314, 'height' => 213, 'action' => 'crop'/*, 'unsharp' => array(100, 1, 0)*/, 'use_crop_args' => true),

	'backend_thumb' => array('width' => 150, 'height' => 205, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'backend_agency_thumb' => array('width' => 100, 'height' => 78, 'unsharp' => array(100, 1, 0)),

	'backend_smallest' => array('width' => 64, 'height' => 64, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'laction_thumb' => array('width' => 167, 'height' => 167, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'laction_thumb_v2' => array('width' => 167, 'height' => 167, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'laction_xl_thumb' => array('width' => 290, 'height' => 290, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'la_details' => array('width' => 109, 'height' => 109, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'la_details_v2' => array('width' => 94, 'height' => 119, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'xl_la_thumb' => array('width' => 296, 'height' => 216, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'sedcard_main' => array('width' => 290, 'height' => 391, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	// Front-End

	//'medium' => array('width' => 225, 'height' => 300, 'use_crop_args' => true),

	'bubble_thumb' => array('width' => 43, 'height' => 57, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'bubble_th' => array('width' => 38, 'height' => 38, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'medium' => array('width' => 200, 'height' => 267, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'medium2' => array('width' => 200, 'height' => 290, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'new-main' => array('width' => 246, 'height' => 175, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'new-main2' => array('width' => 191, 'height' => 286, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'new-entries' => array('width' => 110, 'height' => 110, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'gotm' => array('width' => 178, 'height' => 111, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'new-thumb' => array('width' => 112, 'height' => 78, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'new-thumb2' => array('width' => 60, 'height' => 81, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'book_me' => array('width' => 62, 'height' => 62, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'thumb' => array('width' => 150, 'height' => 200, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'ia_thumb' => array('width' => 66, 'height' => 66, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'pp_thumb' => array('width' => 56, 'height' => 56, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'xl_thumb' => array('width' => 302, 'height' => 455, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'thumb_small' => array('width' => 120, 'height' => 160, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'nlthumb' => array('width' => 120, 'height' => 160, 'unsharp' => array(100, 1, 0)), // newsletter

	//'sthumb' => array('width' => 76, 'height' => 103, 'use_crop_args' => true),

	'sthumb' => array('width' => 60, 'height' => 81, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'sthumb2' => array('width' => 89, 'height' => 136, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'lvthumb' => array('width' => 75, 'height' => 100, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'lvthumb_v2' => array('width' => 143, 'height' => 185, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'lcthumb' => array('width' => 93, 'height' => 112, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'wthumb' => array('width' => 141, 'height' => 88, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'agency_thumb' => array('width' => 179, 'height' => 139, 'unsharp' => array(100, 1, 0)),

	'agency_p100' => array('width' => 90, 'height' => 120, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'agency_pt' => array('width' => 113, 'height' => 159, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'agency_pt_v2' => array('width' => 135, 'height' => 173, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'orig' => array('width' => 1024, 'height' => 1024, 'action' => 'fitbox'),
	'imported' => array('width' => 1024, 'height' => 1024, 'action' => 'fitbox','use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'imported_v2' => array('width' => 1024, 'height' => 1024, 'action' => 'fitbox','use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'w68' => array('width' => 68, 'height' => 91, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'w68c' => array('width' => 68, 'height' => 68, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'vip_thumb' => array('width' => 124, 'height' => 186, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	't100p' => array('width' => 117, 'height' => 97),

	'review_e' => array('width' => 222, 'height' => 288, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'review_e_n' => array('width' => 200, 'height' => 259, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'm45' => array('width' => 45, 'height' => 59, 'unsharp' => array(100, 1, 0)),

	'm95' => array('width' => 95, 'height' => 129, 'unsharp' => array(100, 1, 0)),

	'm320' => array('width' => 320, 'height' => 0, 'action' => 'resize', 'unsharp' => array(100, 1, 0)),
	'm330' => array('width' => 330, 'height' => 245, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'm500' => array('width' => 500, 'height' => 0, 'action' => 'resize', 'unsharp' => array(100, 1, 0)),
	'mobile_thumb' => array('width' => 300, 'height' => 410, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'nat_p_thumb' => array('width' => 143, 'height' => 177, 'unsharp' => array(100, 1, 0)),
	'nat_thumb' => array('width' => 377, 'height' => 468, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'nat_pic_thumb' => array('width' => 203, 'height' => 270, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'euro_thumb' => array('width' => 293, 'height' => 444, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'euro_thumb_big' => array('width' => 461, 'height' => 574, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'euro_thumb_small' => array('width' => 115, 'height' => 146, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'l_punder_thumb' => array('width' => 950, 'height' => 310, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'punder_thumb' => array('width' => 210, 'height' => 316, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'thumb_cropper' => array('width' => 150, 'height' => 200, 'action' => 'fitbox', 'unsharp' => array(100, 1, 0)),
	'thumb_cropper_ed' => array('width' => 190, 'height' => 300, 'action' => 'fitbox', 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'backend_thumb_cropper' => array('width' => 150, 'height' => 205, 'action' => 'fitbox', 'unsharp' => array(100, 1, 0)),

	'w514' => array('width' => 514, 'height' => 495, 'action' => 'fitbox2', 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'blog' => array('width' => 250, 'height' => 150, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'blog_v2' => array('width' => 270, 'height' => 225, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'blog_inner' => array('width' => 250, 'height' => 0, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'blog_inner_v2' => array('width' => 360, 'height' => 0, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'blog_slideshow' => array('width' => 740, 'height' => 400, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'new_blog_slider' => array('width' => 455, 'height' => 345, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'new_blog_middel' => array('width' => 360, 'height' => 270, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'new_blog_middel_2' => array('width' => 263, 'height' => 200, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'new_blog_small' => array('width' => 170, 'height' => 170, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'new_blog_small_2' => array('width' => 170, 'height' => 140, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'new_blog_big' => array('width' => 457, 'height' => 347, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	'nsl_238_159' => array('width' => 238, 'height' => 159, 'unsharp' => array(100, 1, 0)),
	'nsl_198_257' => array('width' => 198, 'height' => 257, 'unsharp' => array(100, 1, 0)),
	'nsl_295_101' => array('width' => 295, 'height' => 101, 'unsharp' => array(100, 1, 0)),
	'nsl_295_208' => array('width' => 295, 'height' => 208, 'unsharp' => array(100, 1, 0)),
	'nsl_179_231' => array('width' => 179, 'height' => 231, 'unsharp' => array(100, 1, 0)),
	'nsl_196_255' => array('width' => 196, 'height' => 255, 'unsharp' => array(100, 1, 0)),
	'nsl_136_180' => array('width' => 136, 'height' => 180, 'unsharp' => array(100, 1, 0)),
	'nsl_160_185' => array('width' => 160, 'height' => 185, 'unsharp' => array(100, 1, 0)),
	'nsl_254_400' => array('width' => 254, 'height' => 400, 'unsharp' => array(100, 1, 0)),
	'nsl_254_280' => array('width' => 254, 'height' => 280, 'unsharp' => array(100, 1, 0)),
	'nsl_254_360' => array('width' => 254, 'height' => 360, 'unsharp' => array(100, 1, 0)),
	'nsl_254_350' => array('width' => 254, 'height' => 350, 'unsharp' => array(100, 1, 0)),
	'nsl_254_190' => array('width' => 254, 'height' => 190, 'unsharp' => array(100, 1, 0)),
	'nsl_254_321' => array('width' => 254, 'height' => 321, 'unsharp' => array(100, 1, 0)),
	'nsl_170_221' => array('width' => 170, 'height' => 221, 'unsharp' => array(100, 1, 0)),
	'nsl_210_148' => array('width' => 210, 'height' => 148, 'unsharp' => array(100, 1, 0)),
	'nsl_34_34' => array('width' => 34, 'height' => 34, 'unsharp' => array(100, 1, 0)),
	'nsl_600_184' => array('width' => 600, 'height' => 184, 'unsharp' => array(100, 1, 0)),
	'nsl_600_243' => array('width' => 600, 'height' => 243, 'unsharp' => array(100, 1, 0)),

	'nsl_279_400' => array('width' => 279, 'height' => 400, 'unsharp' => array(100, 1, 0)),
	'nsl_136_180' => array('width' => 136, 'height' => 180, 'unsharp' => array(100, 1, 0)),

	'list_v4' => array('width' => 265, 'height' => 354,'action' => 'crop', 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'list_v3' => array('width' => 265, 'height' => 354, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'bubble_th_v3' => array('width' => 75, 'height' => 75, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'list_agency_v3' => array('width' => 265, 'height' => 130, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'list_agency_escort_v3' => array('width' => 75, 'height' => 75, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'review_list_v3' => array('width' => 265, 'height' => 354, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'review_escort_small_v3' => array('width' => 102, 'height' => 102, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'lcthumb_v3' => array('width' => 114, 'height' => 113, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'pp_ed_v3' => array('width' => 360, 'height' => 480, 'action' => 'crop', 'use_crop_args' => true),
	'pl_ed_v3' => array('width' => 360, 'height' => 202, 'action' => 'crop', 'use_crop_args' => true),
	'p_thumb_v3' => array('width' => 75, 'height' => 75, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'nat_pic_v3' => array('width' => 360, 'height' => 360, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'la_v3' => array('width' => 170, 'height' => 227, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'cads_v3' => array('width' => 265, 'height' => 354, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'cads_ad_v3' => array('width' => 360, 'height' => 480, 'action' => 'crop', 'use_crop_args' => true),
	'cads_ad_thumb_v3' => array('width' => 75, 'height' => 75, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'cads_orig_v3' => array('width' => 1024, 'height' => 1024, 'action' => 'fitbox2', 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'pf_more_thumb_v3' => array('width' => 85, 'height' => 113, 'action' => 'crop', 'use_crop_args' => false, 'unsharp' => array(100, 1, 0)),
	'pf_big_thumb_v3' => array('width' => 350, 'height' => 394, 'action' => 'crop', 'use_crop_args' => false, 'unsharp' => array(100, 1, 0)),
	'list_thumb_v3' => array('width' => 162, 'height' => 162, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'agency_v3' => array('width' => 360, 'height' => 177, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'pa_avatar' => array('width' => 60, 'height' => 60, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'mem_avatar' => array('width' => 360, 'height' => 480, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'pa_mem_avatar' => array('width' => 300, 'height' => 300, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'follow_thumb' => array('width' => 173, 'height' => 173, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'follow_email_thumb' => array('width' => 211, 'height' => 270, 'action' => 'crop', 'use_crop_args' => false, 'unsharp' => array(100, 1, 0)),
	'follow_thumb_pop' => array('width' => 240, 'height' => 360, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),
	'follow_email_conf' => array('width' => 200, 'height' => 200, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0)),

	/* mobile cassified ads */
	'mobile_classified' => array('width' => 240, 'height' => 320, 'use_crop_args' => true, 'unsharp' => array(100, 1, 0))
);



if ( ! defined('DO_CACHE') ) {

	define('DO_CACHE', true);

}



define('WATERMARK_FILE', 'sexindex.ch/watermark/');



if ( ! defined('DO_WATERMARK') ) {

	define('DO_WATERMARK', false);

}


$file = str_replace('?' . $_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']);



$images_dir = isset($images_dir) ? $images_dir : '';

$resized_dir = isset($resized_dir) ? $resized_dir : dirname($file);



/* --> On Demand Picture Download */

/*$a = array();

if ( ! preg_match('#[^/]+/(_|[0-9]+)/([0-9]+)/[a-f0-9]+_(.+)\..+$#i', $file, $a) ) die ('Wrong Size');

$escort_id = trim($a[1] . $a[2], '_');

//echo $escort_id;

require('migrate_photos.php');

return;*/

/* <-- */



$a = array();

$a = array();

if ( ! preg_match('#[^/]+/[a-f0-9]+_(.+)\..+$#i', $file, $a) ) die ('Wrong Size');



$size_name = $a[1];



$src = preg_replace('/([a-f0-9]+)_.+(\..+)$/i', '$1$2', trim($file, '/'));



$app = explode('/', $src);

$app = $app[0];



if ( ! isset($size_map[$size_name]) ) {

	die('404 Not Found');

}



$size = $size_map[$size_name];

$width = $size['width'];

$height = $size['height'];



$filename = $src;

$src = $images_dir . $src;

if (!is_file($src)) die('404 Not Found');



$ext = @end(explode('.', $src));



if ( ! is_file($file_path = $src) ) {

	die('Error: Original file does not exist');

}



list($orig_width, $orig_height, $type) = getimagesize($file_path);



if ($size_name == 'orig')

{

	$width = $orig_width;

	$height = $orig_height;

	$size['width'] = $width;

	$size['height'] = $height;

}



if ($type == IMAGETYPE_JPEG) { $gd_ext = 'jpeg'; $mime = 'image/jpeg'; }

if ($type == IMAGETYPE_GIF) { $gd_ext = 'gif'; $mime = 'image/gif'; }

if ($type == IMAGETYPE_PNG) { $gd_ext = 'png'; $mime = 'image/png'; }

//if ($type == 6) { $gd_ext = 'bmp'; $mime = 'image/bmp'; }

if ( ! isset($mime) ) die('Error: Wrong mime type in database');



if ( ! $width && ! $height ) {

	$width = $orig_width;

	$height = $orig_height;

}



$func = 'imagecreatefrom' . $gd_ext;



$im = $func($file_path);



if(isset($_REQUEST['cheight']) && isset($_REQUEST['cwidth']) && is_numeric($_REQUEST['cwidth']) &&  is_numeric($_REQUEST['cheight']))
{
	$action = 'rezisecrop';
}
else
{
	$action = 'crop';
if ( ! $width || ! $height ) $action = 'resize';

if ( isset($size['action']) ) $action = $size['action'];

}

$result = doAction($action);



// Main Controller



function doAction($action) {

	global $orig_width, $orig_height, $width, $height, $im;

	global $resized_dir, $file, $filename, $mime, $gd_ext;

	global $size_name, $app;



	// $res_sizes = call_user_func_array('calculate_' . $action, array($orig_width, $orig_height, $width, $height));



	$parts = explode('.', $filename);

	$hash = implode('.', array_slice($parts, 0, count($parts) - 1));

	$ext = end($parts);



	$cache_file = trim($file, '/');



	//header('Content-type: ' . $mime);



	if ( DO_CACHE ) {

		if ( is_file($cache_file) ) {

			/*header('307 Temporary Redirect');
			header('Location: /' .  $cache_file);
			die;*/

			header('Content-type: image/jpeg');
			echo file_get_contents($cache_file);
			exit(0);

		}

	}



	if ( $action == 'crop' ) {

		$result = actionCrop($im, $orig_width, $orig_height, $width, $height);

	}

	elseif ( $action == 'resize' ) {

		$result = actionResize($im, $orig_width, $orig_height, $width, $height);

	}

	elseif ( $action == 'fitbox' ) {

		$result = actionFitbox($im, $orig_width, $orig_height, $width, $height);

	}

	elseif ( $action == 'fitbox2' ) {

		$result = actionFitbox2($im, $orig_width, $orig_height, $width, $height);

	}
	elseif($action=='rezisecrop')
	{
		$cwidth = intval($_REQUEST['cwidth']);
		$cheight = intval($_REQUEST['cheight']);
		$result = acrionResizeCrop($im,$cwidth,$cheight);
	}


	if ( DO_WATERMARK )

	{

		switch($size_name)
		{
			case 'lvthumb_v2':
				$watermark_size = 'thumb';
				break;
			case 'h250res_pf':
					$watermark_size = 'medium';
				break;
			case 'h200res_pf':
					$watermark_size = 'medium';
				break;
			case 'lllandscape_pf':
					$watermark_size = 'medium';
				break;
			case 'ppportrait_pf':
					$watermark_size = 'medium';
				break;
			case 'profile_thumb_ed':
				$watermark_size = 'medium';
			break;

			case 'thumb_ed':
				$watermark_size = 'medium';
			break;
			case 'thumb_ed_v2':
				$watermark_size = 'medium';
			break;
			case 'thumb_ed_v3':
				$watermark_size = 'medium';
			break;
			case 'thumb_pf':
				$watermark_size = 'medium';
			break;
			case 'big_thumb_pf':
				$watermark_size = 'medium';
			break;
			case 'thumb_ed_xl':
				$watermark_size = 'middle';
			break;
			case 'thumb_ed_list':
				$watermark_size = 'medium';
			break;
			case 'review_e_n':
				$watermark_size = 'medium';
			break;
			case 'pp_ed':
				$watermark_size = 'middle';
			break;
			case 'pl_ed':
				$watermark_size = 'middle';
			break;
			case 'thumb':

				$watermark_size = 'medium';

			break;
			case 'xl_thumb':
				$watermark_size = 'middle';
			break;

			case 'm320':

				$watermark_size = 'middle';

			break;

			case 'thumb_small':

				$watermark_size = 'thumb';

			break;

			case 'vip_thumb':

				$watermark_size = 'thumb';

			break;

			case 'euro_thumb':
				$watermark_size = 'middle';
			break;

			case 'euro_thumb_big':
				$watermark_size = 'middle';
			break;

			case 'euro_thumb_small':
				$watermark_size = 'thumb';
				break;

			case 'lthumb-agency':
				$watermark_size = 'thumb';
			break;

			case 'backend_thumb':

				$watermark_size = 'medium';

			break;

			case 'medium':

				$watermark_size = 'middle';

			break;
			case 'medium2':

				$watermark_size = 'medium';

			break;
			case 'new-main2':

				$watermark_size = 'medium';

			break;

			case 'orig':

				$watermark_size = 'biggest';

			break;

			case 'w514':

				$watermark_size = 'biggest';

			break;
			case 'blog':
				$watermark_size = 'middle';
			break;
			case 'blog_v2':
				$watermark_size = 'middle';
			break;
			case 'blog_inner':
				$watermark_size = 'middle';
			break;
			case 'blog_inner_v2':
				$watermark_size = 'middle';
			break;
			case 'blog_slideshow':
				$watermark_size = 'biggest';
			break;
			case 'new_blog_slider':
				$watermark_size = 'biggest';
			break;
			case 'new_blog_middel':
				$watermark_size = 'middle';
			break;
			case 'new_blog_middel_2':
				$watermark_size = 'middle';
			break;
			case 'new_blog_small':
				$watermark_size = 'thumb';
			break;
			case 'new_blog_small_2':
				$watermark_size = 'thumb';
			break;
			case 'new_blog_big':
				$watermark_size = 'biggest';
			break;
			case 'cads_thumb':
				$watermark_size = 'middle';
			break;
			case 'cads_thumb2':
				$watermark_size = 'thumb';
			break;
			case 'cads_orig':
				$watermark_size = 'biggest';
			break;




			case 'list_v3':
				$watermark_size = 'middle';
			break;
			case 'review_list_v3':
				$watermark_size = 'middle';
			break;
			case 'pp_ed_v3':
				$watermark_size = 'middle';
			break;
			case 'pl_ed_v3':
				$watermark_size = 'middle';
			break;
			case 'nat_pic_v3':
				$watermark_size = 'middle';
			break;
			case 'la_v3':
				$watermark_size = 'thumb';
			break;
			case 'cads_v3':
				$watermark_size = 'middle';
			break;
			case 'cads_ad_v3':
				$watermark_size = 'middle';
			break;
			case 'cads_orig_v3':
				$watermark_size = 'biggest';
			break;
			case 'pf_big_thumb_v3':
				$watermark_size = 'medium';
			break;
			case 'list_thumb_v3':
				$watermark_size = 'thumb';
			break;
			case 'agency_v3':
				$watermark_size = 'middle';
			break;
			case 'follow_email_conf':
				$watermark_size = 'medium';
			break;
		}



		$watermark_file = $app . '/watermark/' . $watermark_size . '.png';


		define('CENTER', 1);
		define('CORNERS_AND_CENTER', 2);
		define('CHESSBOARD', 3);


		if ( is_file($watermark_file) ) {

			// Get image dimensions
			$width = imagesx($result);
			$height = imagesy($result);

			// Load the watermark (new version)
			$watermark = imagecreatefrompng($watermark_file);
			$wm_width = imagesx($watermark);
			$wm_height = imagesy($watermark);

//			echo $width . "x" . $height . "\n";
//			echo $wm_width . "x" . $wm_height . "\n";
//			die;
			// Decision Algorithm {{{
			// If image is too small for watermarking just return the original image
			/*if ( $width < 2 * $wm_width && $height < 2 * $wm_width ) {
				return $result;
			}
			var_dump($height); die;*/
			$margins = array(
				'left' => 10,
				'top' => 10,
				'right' => 10,
				'bottom' => 10
			);

			$type = CENTER;

			/*if ( ($width >= $wm_width && $width < 2.8 * $wm_width) ||
					 ($height >= $wm_width && $height < 2.8 * $wm_width) ) {
				$type = CENTER;
			}
			elseif ( $width >= 2 * $wm_width || $height >= 2 * $wm_width ) {
				$margins['left'] = $width * 0.08;
				$margins['right'] = $width * 0.08;
				$margins['top'] = $width * 0.08;
				$margins['bottom'] = $width * 0.08;


				$type = CORNERS_AND_CENTER;
			}*/


			$data = array();

			switch ( $type ) {
				case CORNERS_AND_CENTER:
					$data[] = array(0 + $margins['left'], 0 + $margins['top']); // Top left

					$data[] = array(0 + $margins['left'], $height / 2 - $wm_height / 2); // Middle left
					$data[] = array(0 + $margins['left'], ($height / 2 - $wm_height / 2) / 2); // Middle left

					$data[] = array($width - $wm_width - $margins['right'], 0 + $margins['top']); // Top Right
					$data[] = array(0 + $margins['left'], $height - $wm_height - $margins['bottom']); // Bottom Left
					$data[] = array($width - $wm_width - $margins['right'], $height - $wm_height - $margins['bottom']); // Bottom Right

					$data[] = array($width - $wm_width - $margins['right'], $height / 2 - $wm_height / 2); // Bottom middle
					$data[] = array($width - $wm_width - $margins['right'], $height - ($height / 2 - $wm_height / 2) / 2); // Bottom middle

					$data[] = array($width / 2 - $wm_width / 2, $height / 2 - $wm_height / 2);
					break;
				case CHESSBOARD:
					$data[] = array(0 + $margins['left'], 0 + $margins['right']);
					$data[] = array($width - $wm_width - $margins['right'], $height / 3 - $wm_height / 2);
					$data[] = array(0 + $margins['left'], $height / 3 * 2 - $wm_height / 2);
					$data[] = array($width - $wm_width - $margins['right'], $height - $wm_height - $margins['bottom']);
					break;
				case CENTER:
					$data[] = array($width / 2 - $wm_width / 2, $height / 2 - $wm_height / 2);
					break;
			}

			foreach ( $data as $wm ) {
				list($x, $y) = $wm;

				imagecopyresampled($result, $watermark, $x, $y, 0, 0, $wm_width, $wm_height, $wm_width, $wm_height);
				//$base = $base->merge($watermark, $x, $y);
			}

			/*$wm = imagecreatefrompng($watermark_file);

			$w_w = imagesx($wm);
			$w_h = imagesy($wm);

			$d_w = $w_w;
			$d_h = $w_h;

			if ( $size_name == 'm320' ) {
				$d_x = 320 / 2 - $d_w / 2;
				$d_y = imagesy($result) / 2 - $d_h / 2;
			}
			else {
				$d_x = $width / 2 - $d_w / 2;
				$d_y = $height / 2 - $d_h / 2;
			}

			imagecopyresampled($result, $wm, $d_x, $d_y, 0, 0, $d_w, $d_h, $w_w, $w_h);*/
		}

	}



	global $plugins, $size;

	foreach ( $plugins as $name => $callback ) {

		if ( ! isset($size[$name]) || ! $size[$name] || ! function_exists($callback) ) {

			continue;

		}

		$result = call_user_func_array($callback, array_merge(array($result), $size[$name]));

	}



	$func = 'image' . $gd_ext;

	$quailty = 85;
	if ( $size_name == 'm95' || $size_name == 'm320' ) {
		$quailty = 45;
	}

	if ( DO_CACHE ) {

		$func($result, $cache_file, $gd_ext == 'jpeg' ? $quailty : NULL);

		chmod($cache_file, 0666);

		/*header('307 Temporary Redirect');
		header('Location: /' .  $cache_file);
		die;*/

		header('Content-type: image/jpeg');
		echo file_get_contents($cache_file);
		exit(0);
	}

	else {

		$func($result, NULL, $gd_ext == 'jpeg' ? $quailty : NULL);

	}



	return $result;

}



// Size Calculation



function calculate_crop($orig_width, $orig_height, $width, $height)

{

	$k = $width / $height;

	$n_k = $orig_width / $orig_height;



	if ( $n_k >= $k ) {

		$r_height = $height;

		$r_width = $r_height * $orig_width / $orig_height;

	}

	else  {

		$r_width = $width;

		$r_height = $r_width * $orig_height / $orig_width;

	}



	$x = floor($r_width / 2) - floor($width / 2);

	$y = floor($r_height / 2) - floor($height / 2);



	if ($x < 0) {

		$x = 0;

	}



	if ($y < 0) {

		$y = 0;

	}



	if (($r_width - $x) < $width) {

		$width = $r_width - $x;

	}



	if (($r_height - $y) < $height) {

		$width = $r_height - $y;

	}



	return array(

		'res_width' => round($width),

		'res_height' => round($height),

		'r_width' => $r_width,

		'r_height' => $r_height,

		'x' => $x,

		'y' => $y

	);

}



function calculate_resize($orig_width, $orig_height, $width, $height)

{

	if ($width && $orig_width > $orig_height) 	$k = $orig_height / $orig_width;

	elseif ($height && $orig_width > $orig_height)  $k = $orig_width  / $orig_height;

	elseif ($width && $orig_width <= $orig_height)  $k = $orig_height / $orig_width;

	elseif ($height && $orig_width <= $orig_height) $k = $orig_width  / $orig_height;



	if ($height) {

		$width = $k * $height;

	}

	elseif ($width) {

		$height = $k * $width;

	}



	return array(

		'res_width' => round($width),

		'res_height' => round($height)

	);

}



// Image Manipulation

function acrionResizeCrop($im,$cwidth,$cheight) {

	global $size,$width,$height;
	$use_args = isset($size['use_crop_args']) && $size['use_crop_args'];
	if($use_args)
	{	$args = getCropArgs();
		$x = $args['x'];
		$y = $args['y'];
		$cropped_image = imagecreatetruecolor($cwidth, $cheight);

		imagecopy($cropped_image, $im, 0, 0, $x, $y,$cwidth, $cheight);
	/*  for debug

	 	$tempdir = sys_get_temp_dir().'tmp_photo/';
		if(!is_dir($tempdir))
		 mkdir($tempdir);

		$tempdir.=uniqid().'.'.$ext;
		imagejpeg($cropped_image, $tempdir,100);
		$func = 'imagecreatefrom'.$ext;

	 */
		$resized = imagecreatetruecolor($width, $height);

	imagecopyresampled($resized, $cropped_image, 0, 0, 0, 0, $size['width'], $size['height'], $cwidth, $cheight);
	return $resized;

	}
}

function actionCrop($im, $orig_width, $orig_height, $width, $height) {

	global $size;



	$use_args = isset($size['use_crop_args']) && $size['use_crop_args'];



	$args = getCropArgs();



	$c_x = isset($args['x']) ? $args['x'] : 0;

	$c_y = isset($args['y']) ? $args['y'] : 0;

	$c_px = isset($args['px']) ? $args['px'] : 0;

	$c_py = isset($args['py']) ? $args['py'] : 0;

//echo $c_py;

	// header('Content-Type: text/plain', true);

	// echo $c_px . ':' . $c_py;die;



	$res_sizes = calculate_crop($orig_width, $orig_height, $width, $height);

	extract($res_sizes);



	if ( $use_args ) {

		$x = abs($c_px * $width);

		$y = abs($c_py * $height);



		if ( $r_width - $x < $width ) $x = $r_width - $width;

		if ( $r_height - $y < $height ) $y = $r_height - $height;

	}



	$resized = imagecreatetruecolor($r_width, $r_height);

	imagecopyresampled($resized, $im, 0, 0, 0, 0, $r_width, $r_height, $orig_width, $orig_height);



	$cropped = imagecreatetruecolor($width, $height);

	imagecopy($cropped, $resized, 0, 0, $x, $y, $r_width, $r_height);



	return $cropped;

}



function actionResize($im, $orig_width, $orig_height, $width, $height) {

	$res_sizes = calculate_resize($orig_width, $orig_height, $width, $height);

	extract($res_sizes);



	$resized = imagecreatetruecolor($res_width, $res_height);

	imagecopyresampled($resized, $im, 0, 0, 0, 0, $res_width, $res_height, $orig_width, $orig_height);



	return $resized;

}



function actionFitbox($im, $orig_width, $orig_height, $width, $height) {

	$res_sizes = calculate_crop($orig_width, $orig_height, $width, $height);

	extract($res_sizes);



	$resized = imagecreatetruecolor($r_width, $r_height);

	imagecopyresampled($resized, $im, 0, 0, 0, 0, $r_width, $r_height, $orig_width, $orig_height);



	return $resized;

}



function calculate_fitbox($orig_width, $orig_height, $width, $height)

{

	$r_width = $orig_width; $r_height = $orig_height;

	$ratio = $r_width / $r_height;

	if ( $r_width > $width ) {

		$r_width = $width;

		$r_height = round($r_width / $ratio);

	}



	if ( $r_height > $height ) {

		$r_height = $height;

		$r_width = round($r_height * $ratio);

	}



	return array(

		'res_width' => $r_width,

		'res_height' => $r_height

	);

}



function actionFitbox2($im, $orig_width, $orig_height, &$width, &$height) {

	$res_sizes = calculate_fitbox($orig_width, $orig_height, $width, $height);

	extract($res_sizes);



	$resized = imagecreatetruecolor($res_width, $res_height);

	imagecopyresampled($resized, $im, 0, 0, 0, 0, $res_width, $res_height, $orig_width, $orig_height);

	$width = $res_width;

	$height = $res_height;

/*header('Content-Type: text/plain');

var_dump($res_sizes);ob_flush();die;*/

	return $resized;

}



function getEscortPhotos($escort_id)

{

	mysql_connect('127.0.0.1:3307', 'root', 'Ghhy25p4');

	mysql_select_db('main_v2');



	$result = mysql_query("

		SELECT

			hash, ext

		FROM escort_photos

		WHERE

			escort_id = $escort_id

	");



	if ( mysql_errno() ) {

		__error('MySQL: ' . mysql_error());

	}



	$rows = array();

	while ( $row = mysql_fetch_row($result) ) {

		$rows[] = implode('.', $row);

	}



	return $rows;

}



function getCropArgs()

{

	global $file, $size, $size_map;



	$args = isset($_REQUEST['args']) ? $_REQUEST['args'] : '0:0';



	$args = explode(':', $args);

	if ( 2 != count($args) ) {

		$args = array(0, 0);

	}



	$args = array_combine(array('x', 'y'), $args);



	$args['x'] = intval($args['x']);

	$args['y'] = intval($args['y']);



	$args['px'] = floatval($args['x'] / $size['width']/*$size_map['backend_thumb']['width']*/);

	$args['py'] = floatval($args['y'] / $size['height']/*$size_map['backend_thumb']['height']*/);



	return $args;



	$a = array();

	if ( ! preg_match('#^/[^/]+/([0-9]+|_)/([0-9]+)/([a-f0-9]+)_[^.]+\.(.+)$#i', $file, $a) ) {

		return array('x' => 0, 'y' => 0);

	}

	array_shift($a);

	$escort_id = ($a[0] == '_' ? '' : $a[0]) . $a[1];

	$hash = $a[2];

	$ext = $a[3];



	mysql_connect('127.0.0.1:3307', 'root', 'Ghhy25p4');

	mysql_select_db('main_v2');



	$escort_id = intval($escort_id);

	$hash = mysql_real_escape_string($hash);

	$ext = mysql_real_escape_string($ext);



	$result = mysql_query("

		SELECT

			args

		FROM escort_photos

		WHERE

			escort_id = $escort_id AND

			hash = '$hash' AND

			ext = '$ext'

	");



	if ( mysql_errno() ) {

		__error('MySQL: ' . mysql_error());

	}



	if ( mysql_num_rows($result) > 0 ) {

		$s = mysql_fetch_object($result)->args;

		$args = unserialize($s);



		if ( ! $args ) {

			return array('x' => 0, 'y' => 0);

		}



		return $args;

	}

	else {

		return array('x' => 0, 'y' => 0);

	}

}



function ConvertBMP2GD($src, $dest = false) {

if(!($src_f = fopen($src, "rb"))) {

return false;

}

if(!($dest_f = fopen($dest, "wb"))) {

return false;

}

$header = unpack("vtype/Vsize/v2reserved/Voffset", fread($src_f,

14));

$info = unpack("Vsize/Vwidth/Vheight/vplanes/vbits/Vcompression/Vimagesize/Vxres/Vyres/Vncolor/Vimportant",

fread($src_f, 40));



extract($info);

extract($header);



if($type != 0x4D42) { // signature "BM"

return false;

}



$palette_size = $offset - 54;

$ncolor = $palette_size / 4;

$gd_header = "";

// true-color vs. palette

$gd_header .= ($palette_size == 0) ? "\xFF\xFE" : "\xFF\xFF";

$gd_header .= pack("n2", $width, $height);

$gd_header .= ($palette_size == 0) ? "\x01" : "\x00";

if($palette_size) {

$gd_header .= pack("n", $ncolor);

}

// no transparency

$gd_header .= "\xFF\xFF\xFF\xFF";



fwrite($dest_f, $gd_header);



if($palette_size) {

$palette = fread($src_f, $palette_size);

$gd_palette = "";

$j = 0;

while($j < $palette_size) {

$b = $palette{$j++};

$g = $palette{$j++};

$r = $palette{$j++};

$a = $palette{$j++};

$gd_palette .= "$r$g$b$a";

}

$gd_palette .= str_repeat("\x00\x00\x00\x00", 256 - $ncolor);

fwrite($dest_f, $gd_palette);

}



$scan_line_size = (($bits * $width) + 7) >> 3;

$scan_line_align = ($scan_line_size & 0x03) ? 4 - ($scan_line_size &

0x03) : 0;



for($i = 0, $l = $height - 1; $i < $height; $i++, $l--) {

// BMP stores scan lines starting from bottom

fseek($src_f, $offset + (($scan_line_size + $scan_line_align) *

$l));

$scan_line = fread($src_f, $scan_line_size);

if($bits == 24) {

$gd_scan_line = "";

$j = 0;

while($j < $scan_line_size) {

$b = $scan_line{$j++};

$g = $scan_line{$j++};

$r = $scan_line{$j++};

$gd_scan_line .= "\x00$r$g$b";

}

}

else if($bits == 8) {

$gd_scan_line = $scan_line;

}

else if($bits == 4) {

$gd_scan_line = "";

$j = 0;

while($j < $scan_line_size) {

$byte = ord($scan_line{$j++});

$p1 = chr($byte >> 4);

$p2 = chr($byte & 0x0F);

$gd_scan_line .= "$p1$p2";

} $gd_scan_line = substr($gd_scan_line, 0, $width);

}

else if($bits == 1) {

$gd_scan_line = "";

$j = 0;

while($j < $scan_line_size) {

$byte = ord($scan_line{$j++});

$p1 = chr((int) (($byte & 0x80) != 0));

$p2 = chr((int) (($byte & 0x40) != 0));

$p3 = chr((int) (($byte & 0x20) != 0));

$p4 = chr((int) (($byte & 0x10) != 0));

$p5 = chr((int) (($byte & 0x08) != 0));

$p6 = chr((int) (($byte & 0x04) != 0));

$p7 = chr((int) (($byte & 0x02) != 0));

$p8 = chr((int) (($byte & 0x01) != 0));

$gd_scan_line .= "$p1$p2$p3$p4$p5$p6$p7$p8";

} $gd_scan_line = substr($gd_scan_line, 0, $width);

}



fwrite($dest_f, $gd_scan_line);

}

fclose($src_f);

fclose($dest_f);

return true;

}



if (!function_exists('imagecreatefrombmp')) {
function imagecreatefrombmp($filename) {

	$tmp_name = tempnam("/tmp", "GD");

	if(ConvertBMP2GD($filename, $tmp_name)) {

		$img = imagecreatefromgd($tmp_name);

		unlink($tmp_name);



		return $img;

	}

	return false;

}
}





function process_action($act)

{

	$app = isset($_REQUEST['app']) ? $_REQUEST['app'] : null;

	if ( is_null($app) ) {

		__error('`app` parameter is required');

	}



	if ( ! is_dir($app) ) {

		__error("app `$app` does not exist");

	}

	if ( 'clear_cache_agency' == $act ) {
		$agency_id = isset($_REQUEST['aid']) ? $_REQUEST['aid'] : null;

		if ( ! $agency_id ) return;

		$hash = isset($_REQUEST['hash']) ? $_REQUEST['hash'] : null;

		$dir = $app . '/agencies';

		$files = new DirectoryIterator($dir);

		foreach ( $files as $file ) {
			if ( ! $file->isFile() ) continue;

			$a = array();

			if ( preg_match('/([a-f0-9]+)\_.+\.(jpg|jpeg|png)/', $file->getFilename(), $a) ) {
				if ( ! is_null($hash) && $hash != $a[1] ) {
					continue;
				}
				echo $file->getPathname() . ' - ';

				if ( @unlink($file->getPathname()) ) {
					echo 'deleted';
				}
				else {
					echo 'failed';
				}

				echo "<br/>\r\n";

			}

		}
	}

    elseif ( $act == 'rotate' ) {
		$escort_id = isset($_REQUEST['eid']) ? $_REQUEST['eid'] : null;
		$hash = isset($_REQUEST['hash']) ? $_REQUEST['hash'] : null;
		$ext = isset($_REQUEST['ext']) ? $_REQUEST['ext'] : null;
		$degrees = isset($_REQUEST['d']) ? $_REQUEST['d'] : 90;
		$parts = array();
		if ( strlen($escort_id) > 2 ) {
			$parts[] = substr($escort_id, 0, 2);
			$parts[] = substr($escort_id, 2);
		} else {
			$parts[] = '_';
			$parts[] = $escort_id;
		}

		if(isset($_REQUEST['sub_dir'])){
			$parts[] = $_REQUEST['sub_dir'];
		}
		$path = implode('/', $parts);

		if ( ! is_dir($dir = $app . '/' . $path) ) {
			__error("escort with id `$escort_id` does not have photos");
		}

		//$degrees = 90;
		$image_name = $dir . '/' . $hash . '.' . $ext;
		//if ( in_array($ext, array('jpg', 'jpeg')) ) {
			$source = imagecreatefromjpeg($image_name);
			$rotate = imagerotate($source, $degrees, 0);

			@unlink($image_name);

			imagejpeg($rotate, $image_name, IMAGE_QUALITY);
			imagedestroy($source);
			imagedestroy($rotate);
		//} elseif ( $ext == 'png' ) {

			/*$source = imagecreatefrompng($image_name);
			$rotate = imagerotate($source, $degrees, 1);

			@unlink($image_name);

			imagepng($rotate, $image_name, IMAGE_QUALITY);
			imagedestroy($source);
			imagedestroy($rotate);*/

			/*$source = imagecreatefrompng($image_name);
			imagealphablending($source, false);
			imagesavealpha($source, true);

			$rotation = imagerotate($source, $degrees, imageColorAllocateAlpha($source, 0, 0, 0, 127));
			imagealphablending($rotation, false);
			imagesavealpha($rotation, true);

			@unlink($image_name);

			imagepng($rotation, $image_name, IMAGE_QUALITY);
			imagedestroy($source);
			imagedestroy($rotation);*/
		//}
	}

    elseif ( 'clear_cache' == $act ) {

		$escort_id = isset($_REQUEST['eid']) ? $_REQUEST['eid'] : null;



		if ( is_null($escort_id) ) {

			$dir = new DirectoryIterator($app);

			foreach ( $dir as $item ) {

				if ( ! $item->isDir() || $item->isDot() ) continue;



				$dirs1 = new DirectoryIterator($item->getPathname());

				foreach ( $dirs1 as $dir1 ) {

					if ( ! $dir1->isDir() || $dir1->isDot() ) continue;



					$files = new DirectoryIterator($dir1->getPathname());

					foreach ( $files as $file ) {

						if ( ! $file->isFile() ) continue;



						if ( preg_match('/([a-f0-9]+)\.(jpg|jpeg|png)/', $file->getFilename()) ) {

							list($orig_width, $orig_height, $type) = getimagesize($file->getPathname());

							if ( $type == IMAGETYPE_BMP ) {

								echo $file->getPathname();



								$img = imagecreatefrombmp($file->getPathname());



								if ( imagejpeg($img, $file->getPathname(), IMAGE_QUALITY) ) {

									echo " - converted<br/>\r\n";

								}

								else {

									echo " - failed<br/>\r\n";

								}



								imagedestroy($img);

							}

						}



						if ( preg_match('/[a-f0-9]+\_.+\.(jpg|jpeg|png)/', $file->getFilename()) ) {

							echo $file->getPathname() . ' - ';

							if ( @unlink($file->getPathname()) ) {

								echo 'deleted';

							}

							else {

								echo 'failed';

							}

							echo "<br/>\r\n";

						}

					}

				}

			}

		}

		else {

			/* >>> Construct path for escort */

			$parts = array();



			if ( strlen($escort_id) > 2 ) {

				$parts[] = substr($escort_id, 0, 2);

				$parts[] = substr($escort_id, 2);

			}

			else {

				$parts[] = '_';

				$parts[] = $escort_id;

			}

			if(isset($_REQUEST['sub_dir'])){
				$parts[] = $_REQUEST['sub_dir'];
			}

			$path = implode('/', $parts);



			/* <<< */



			if ( ! is_dir($dir = $app . '/' . $path) ) {

				__error("escort with id `$escort_id` does not have photos");

			}



			$hash = isset($_REQUEST['hash']) ? $_REQUEST['hash'] : null;



			$files = new DirectoryIterator($dir);

			foreach ( $files as $file ) {

				if ( ! $file->isFile() ) continue;



				if ( preg_match('/([a-f0-9]+)\.(jpg|jpeg|png)/', $file->getFilename()) ) {

					list($orig_width, $orig_height, $type) = getimagesize($file->getPathname());

					if ( $type == IMAGETYPE_BMP ) {

						echo $file->getPathname();



						$img = imagecreatefrombmp($file->getPathname());



						if ( imagejpeg($img, $file->getPathname(), IMAGE_QUALITY) ) {

							echo " - converted<br/>\r\n";

						}

						else {

							echo " - failed<br/>\r\n";

						}



						imagedestroy($img);

					}

				}



				$a = array();

				if ( preg_match('/([a-f0-9]+)\_.+\.(jpg|jpeg|png)/', $file->getFilename(), $a) ) {

					if ( ! is_null($hash) && $hash != $a[1] ) {

						continue;

					}



					echo $file->getPathname() . ' - ';

					if ( @unlink($file->getPathname()) ) {

						echo 'deleted';

					}

					else {

						echo 'failed';

					}

					echo "<br/>\r\n";

				}

			}



		}

	}

	elseif ( 'cleanup' == $act ) { // Deletes all photos that are not specified in database

		$escort_id = isset($_REQUEST['eid']) ? $_REQUEST['eid'] : null;



		if ( is_null($escort_id) ) {

			__error('`eid` parameter is required');

		}



		/* >>> Construct path for escort */

		$parts = array();



		if ( strlen($escort_id) > 2 ) {

			$parts[] = substr($escort_id, 0, 2);

			$parts[] = substr($escort_id, 2);

		}

		else {

			$parts[] = '_';

			$parts[] = $escort_id;

		}



		$path = implode('/', $parts);



		/* <<< */



		if ( ! is_dir($dir = $app . '/' . $path) ) {

			__error("escort with id `$escort_id` does not have photos");

		}



		$photos = getEscortPhotos($escort_id);



		$files = new DirectoryIterator($dir);

		foreach ( $files as $file ) {

			if ( ! $file->isFile() ) continue;



			$a = array();

			if ( preg_match('/([a-f0-9]+)(\_.+)?\.(jpg|jpeg|png)/', $file->getFilename(), $a) ) {



				if ( in_array($a[1] . '.' . $a[3], $photos) ) {

					continue;

				}



				echo $file->getPathname() . ' - ';

				if ( @unlink($file->getPathname()) ) {

					echo 'deleted';

				}

				else {

					echo 'failed';

				}

				echo "<br/>\r\n";

			}

		}

	}



	die;

}