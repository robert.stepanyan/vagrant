<?php
class Private_FavoritesController extends Zend_Controller_Action
{
	public function init()
	{
        $this->_request->setParam('no_tidy', true);

		$anonym = array();

		$this->user = Model_Users::getCurrent();
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
            if ( is_null($this->_getParam('ajax')) ) {
                $this->_redirect($this->view->getLink('signin'));
                return;
            }else{
                die(json_encode(
                    array(
                        'status' => 'error',
                        'redirect_to_signin' => $this->view->getLink('signin')
                    )
                )
                );
            }
		}

		if($this->_getParam ('ajax')){
			$this->view->layout()->disableLayout();
		}

		$this->view->user = $this->user;

		$this->client = Cubix_Api::getInstance();
		$this->model = new Model_Members();
	}

	public function indexAction()
	{
        $modelCount = new Model_Countries();
        $countries = $modelCount->getCountries();
        $ar = array();

        foreach ($countries as $c)
            $ar[] = $c->id;

        $modelC = new Model_Cities();
        $this->view->cities = $modelC->getByCountries($ar);

	}

    public function ajaxFavoritesAction()
    {
        $this->view->layout()->disableLayout();

        $page = $this->view->page = $this->_request->page ? intval($this->_request->page) : 1;

        if (in_array($this->_request->per_page, array(12, 24, 48, 96, 1000)))
            $per_page = $this->_request->per_page;
        else
            $per_page = 12;

        $this->view->per_page = $per_page;

        $filter = array();

        if (strlen($this->_request->showname) > 0 ) {
            $showname = preg_replace('/[^-_a-z0-9\s]/i', '', trim($this->_request->showname));
            $filter['showname'] = $showname;
        }

        if (in_array($this->_request->act, array(1, 2, 3)))
            $filter['act'] = $this->_request->act;
        else
            $filter['act'] = 1;

        setcookie("favoriteAct", $filter['act']);

        if (intval($this->_request->city))
        {
            $filter['city'] = intval($this->_request->city);
        }

        $model = new Model_Members();

        $ret = $model->getFavorites($this->user->id, $filter, $page, $per_page);

        $this->view->items = $ret['data'];
        $this->view->count = $ret['count'];

        if ($this->view->allowChat2())
        {
            $model = new Model_EscortsV2();

            $res = $model->getChatNodeOnlineEscorts(1, 1000);

            $ids = $res['ids'];
            $this->view->chat_escort_ids = explode(',', $ids);
        }
    }

    public function ajaxFavoritesTopAction()
    {
        $this->view->layout()->disableLayout();

        $model = new Model_Members();

        $top10 = $model->getFavoritesTop10($this->user->id);
        $this->view->top10 = $top10;

        if ($top10)
        {
            $max = 0;

            foreach ($top10 as $t)
            {
                if ($t['rank'] > $max)
                    $max = $t['rank'];
            }

            $this->view->max_rank = $max;

            /* requests */
            $all_requests = $model->getFavPendingRequests($this->user->id);

            $arr = array();

            if ($all_requests)
            {
                foreach ($all_requests as $r)
                {
                    if (!isset($arr[$r['fav_id']]))
                    {
                        $arr[$r['fav_id']] = array($r['user_id'] => array('name' => $r['name'], 'req_id' => $r['req_id']));
                    }
                    else
                    {
                        $arr[$r['fav_id']][$r['user_id']] = array('name' => $r['name'], 'req_id' => $r['req_id']);
                    }
                }
            }

            $this->view->requests = $arr;
            /**/
        }
    }

    public function ajaxFavoritesAddToTopAction()
    {
        $this->view->layout()->disableLayout();

        $form = new Cubix_Form_Data($this->_request);
        $fields = array(
            'escort_id' => 'int-nz'
        );

        $form->setFields($fields);
        $data = $form->getData();

        $model = new Model_Members();
        $ret = $model->favoritesAddToTop($this->user->id, $data['escort_id']);

        if ($ret == 'error')
            die(json_encode(array('error' => $this->view->t('top_10_limit'))));

        /* recalculation rating */
        $model->recalculateRating($data['escort_id']);
        /**/

        die(json_encode(array('success' => $this->view->t('top_10_added'))));
    }


    public function ajaxFavoritesUpAction()
    {
        $this->view->layout()->disableLayout();

        $form = new Cubix_Form_Data($this->_request);
        $fields = array(
            'escort_id' => 'int-nz'
        );

        $form->setFields($fields);
        $data = $form->getData();

        $model = new Model_Members();
        $model->favoritesMoveUp($this->user->id, $data['escort_id']);

        /* recalculation rating */
        $model->recalculateRating($data['escort_id']);
        /**/

        die(json_encode(array('success' => true)));
    }

    public function removeFromFavoritesAction()
    {
        $user_id = $this->user->id;
        $escort_id = $this->_request->escort_id;

        $model = new Model_Members();
        $model->removeFromFavorites($user_id, $escort_id);

        /* reordring top 10 */
        $model->reorderingTop10($user_id);
        /**/

        /* recalculation rating */
        $model->recalculateRating($escort_id);
        /**/

        die(json_encode(array('success' => $this->view->t('removed_from_favorites'))));
    }


    public function ajaxFavoritesDownAction()
    {
        $this->view->layout()->disableLayout();

        $form = new Cubix_Form_Data($this->_request);
        $fields = array(
            'escort_id' => 'int-nz'
        );

        $form->setFields($fields);
        $data = $form->getData();

        $model = new Model_Members();
        $model->favoritesMoveDown($this->user->id, $data['escort_id']);

        /* recalculation rating */
        $model->recalculateRating($data['escort_id']);
        /**/

        die(json_encode(array('success' => true)));
    }


    public function ajaxFavoritesRemoveFromTopAction()
    {
        $this->view->layout()->disableLayout();

        $form = new Cubix_Form_Data($this->_request);
        $fields = array(
            'escort_id' => 'int-nz'
        );

        $form->setFields($fields);
        $data = $form->getData();

        $model = new Model_Members();
        $model->favoritesRemoveFromTop($this->user->id, $data['escort_id']);

        /* reordring top 10 */
        $model->reorderingTop10($this->user->id);
        /**/

        /* recalculation rating */
        $model->recalculateRating($data['escort_id']);
        /**/

        die(json_encode(array('success' => $this->view->t('removed_from_top_favorites'))));
    }

    public function changeFavCommentTypeAction()
    {
        $this->view->layout()->disableLayout();

        $form = new Cubix_Form_Data($this->_request);
        $fields = array(
            'f_id' => 'int-nz',
            'new_val' => 'int-nz'
        );

        $form->setFields($fields);
        $data = $form->getData();

        $model = new Model_Members();
        $model->changeFavCommentType($this->user->id, $data['f_id'], $data['new_val']);

        die(json_encode(array('success' => true)));
    }

    public function addFavCommentAction()
    {
        $form = new Cubix_Form_Data($this->_request);
        $fields = array(
            'escort_id' => 'int-nz',
            'comment' => 'notags|special'
        );

        $form->setFields($fields);
        $data = $form->getData();

        $data['user_id'] = $this->user->id;

        $model = new Model_Members();
        $model->updateFavoritesComment($data);

        die(json_encode(array('success' => true)));
    }

}