<?php

class Private_ToursController extends Zend_Controller_Action
{	
	/**
	 * @var Model_Escort_Profile
	 */
	protected $profile;

	/**
	 * @var Model_EscortItem
	 */
	protected $escort;

	/**
	 * @var Model_AgencyItem
	 */
	protected $agency;

	/**
	 * @var Zend_Session_Namespace
	 */
	protected $session;
	
	public static $linkHelper;


	public function init(){

		
		$this->view->escort_id = intval($this->_getParam('escort'));
		
		$cache = Zend_Registry::get('cache');
		
		self::$linkHelper = $this->view->getHelper('GetLink');

		$this->_request->setParam('no_tidy', true);
		
		$anonym = array();

		$this->user = Model_Users::getCurrent();
		$this->defines = $this->view->defines = Zend_Registry::get('defines');
		$this->client = Cubix_Api::getInstance();

		if ( in_array($this->_request->getActionName(), array('upgrade')) ) {
			return;
		}

		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
            if ( is_null($this->_getParam('ajax')) ) {
                $this->_redirect($this->view->getLink('signin'));
                return;
            }else{
                die(json_encode(
                    array(
                        'status' => 'error',
                        'redirect_to_signin' => $this->view->getLink('signin')
                    )
                )
                );
            }
		}

		$cache_key =  'v2_user_pva_' . $this->user->id;

		if ( $this->user->isAgency() ) {

			if ( ! $agency = $cache->load($cache_key) ) {
				$agency = $this->user->getAgency();
				$cache->save($agency, $cache_key, array(), 300);
			}

			$this->agency = $this->view->agency = $agency;

			
		}
		else if ( $this->user->isEscort() ) {

			if ( ! $escort = $cache->load($cache_key) ) {
				$escort = $this->user->getEscort();
				$cache->save($escort, $cache_key, array(), 300);
			}

			$this->escort = $this->view->escort = $escort;


			if ( $this->escort->status & Model_Escorts::ESCORT_STATUS_SUSPICIOUS_DELETED &&
				in_array($this->_request->getActionName(), array( 'tours', 'escort-reviews', 'settings', 'support', 'happy-hour', 'client-blacklist', 'plain-photos'  )) ) {
				$this->_redirect($this->view->getLink('private'));
			}

		}		

		$this->view->user = $this->user;
		$this->view->escort = $this->escort;
		
	}

	public function indexAction()
	{

		if ( ! is_null($this->_getParam('ajax')) ) {
			  $this->view->layout()->disableLayout();
		}
		
		$this->_request->setParam('no_tidy', true);

		$this->view->is_agency = $this->user->isAgency();

		if ( $this->user->isAgency() ) {
			$this->_helper->viewRenderer->setScriptAction('agency-tours');
			$this->view->escorts = $this->agency->getEscorts();
		}
		else {
			$this->_helper->viewRenderer->setScriptAction('escort-tours');
			$this->escort = $this->view->escort = $this->user->getEscort();
		}
		
		$this->view->disable_tour_history = $this->client->call('getTourHistory', array($this->user->id));
	}
	
	public function toggleTourHistoryAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$o_t_v = intval($this->_request->o_t_v);
		
		$o_t_v == 1 ? $o_t_v = 0 : $o_t_v = 1;
				
		$this->client->call('updateTourHistory', array($this->user->id, $o_t_v));
	}

	public function ajaxToursAction($data = array())
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('no_tidy', true);

		$is_old_tour = intval($this->_getParam('old_tour')) ? true : false;
		$page = (isset($this->_request->page) && intval($this->_request->page) > 0) ? intval($this->_request->page) : 1;
		
		$per_page = 6;
		$this->view->is_agency = $this->user->isAgency();
		$this->view->tours = array();
		$this->view->page = $page;
		$this->view->per_page = $per_page;
        $getall = false;
        $agency_id = 0;

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));

            $agency =  $this->user->getAgency();
            $agency_id = $agency->id;

            if($escort_id == -1){
                $getall = true;
            }
      		if ( ! $this->agency->hasEscort($escort_id) && !$getall) return;
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}

		if ( is_null($escort_id) ) {
			return;
		}

		$tours = $this->client->call('getEscortToursV2', array($escort_id, Cubix_I18n::getLang(), $agency_id,$page, $per_page, $is_old_tour));
        $this->view->tours = $tours[0];
		$this->view->count = $tours[1];
        if($is_old_tour){
		    $this->_helper->viewRenderer->setScriptAction('ajax-old-tours');
	    }
	}

	public function removeToursAction()
	{
		$tours = (array) $this->_getParam('tours');
		foreach ( $tours as $tour_id ) {
			$tour_id = intval($tour_id);
			
			$tour = $this->client->call('getEscortTour', array($tour_id, $this->_request->lang_id));
			if ( ! $tour ) continue;

			if ( $this->user->isAgency() ) {
				if ( ! $this->agency->hasEscort($tour['escort_id']) ) {
					continue; // wrong owner
				}
			}
			elseif ( $this->user->isEscort() ) {
				if ( $this->escort->getId() != $tour['escort_id'] ) {
					continue; // wrong owner
				}
			}
			
			$this->client->call('removeEscortTour', array($tour['escort_id'], $tour['id']));
		}

		die;
	}

	public function ajaxToursAddAction()
	{
		$this->_request->setParam('no_tidy', true);

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( ! $this->agency->hasEscort($escort_id) ) die;
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}

		if ( is_null($escort_id) ) die;

		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'id' => 'int-nz',
			'country_id' => 'int-nz',
			'city_id' => 'int-nz',
			'date_from' => 'int',
			'date_to' => 'int',
			'phone' => '',
			'email' => ''
		));
		$i_data = $form->getData();

		$validator = new Cubix_Validator();

		$tours = $this->client->call('getEscortTours', array($escort_id, $this->_request->lang_id));
		$data['tours_tmp'] = $tours[0];
		if ( count($data['tours_tmp']) ) {
			foreach ( $data['tours_tmp'] as $t_k => $tr ) {
				unset($data['tours_tmp'][$t_k]['country']);
				unset($data['tours_tmp'][$t_k]['city']);
			}
		}
		$data['tours_tmp'][] = $i_data;

		$today = strtotime(date('d-m-Y', time()));

		foreach ( $data['tours_tmp'] as $i1 => $tour ) {

			$id = $tour['id'];
			$country_id = $tour['country_id'];
			$city_id = $tour['city_id'];
			$date_from = $tour['date_from'];
			$date_to = $tour['date_to'];
			$phone = $tour['phone'];
			
			if ( $id == $i_data['id'] ) continue;

			if ( $date_to < $today ) {
				$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}

			if ( $date_from > $date_to ) {
				$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}

			$found = false;

			foreach ( $data['tours_tmp'] as $i2 => $t1 ) {
				if ( $i1 == $i2 ) continue;

				//list($nil, $nil, $d_f, $d_t, $phone) = $t1;
				$d_f = $t1['date_from'];
				$d_t = $t1['date_to'];

				if ( ($d_f >= $date_from && $d_f < $date_to) || ($d_t > $date_from && $d_t <= $date_to) ) {
					$found = true;

					$validator->setError('tours', 'Some date intervals are invalid or overlapped');
					break;
				}
			}

			if ( $found ) {
				break;
			}
		}
		unset($data['tours_tmp']);

		$model = new Cubix_Geography_Countries();
		if ( ! $model->exists($i_data['country_id']) ) {
			$validator->setError('country', 'Invalid country');
		}

		if ( ! $model->hasCity($i_data['country_id'], $i_data['city_id']) ) {
			$validator->setError('city', 'Invalid city');
		}

		if ( ! $i_data['date_from'] || ! $i_data['date_to'] ) {
			$validator->setError('date', 'Invalid date range');
		}

		if ( $i_data['email'] ) {
			if ( ! Cubix_Validator::validateEmailAddress($i_data['email']) ) {
				$validator->setError('email', 'Invalid email address');
			}
		}

		if ( $validator->isValid() ) {
			$this->client->call('addEscortTour', array($escort_id, $i_data));
		}
		else {
			die(json_encode($validator->getStatus()));
		}

		$this->ajaxToursAction(array('status' => 'success'));
	}


	public function ajaxToursRemoveAction()
	{
		$this->_request->setParam('no_tidy', true);

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( ! $this->agency->hasEscort($escort_id) ) die;
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}

		$tour_id = intval($this->_getParam('id'));

		$this->client->call('removeEscortTour', array($escort_id, $tour_id));

		$this->ajaxToursAction(array('status' => 'success'));
	}

	public function editTourAction()
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('no_tidy', true);

		$tour = null;
		//var_dump($this->_getParam('id'));die;
		if ( ($tour_id = intval($this->_getParam('id'))) > 0 ) {
			$tour = $this->client->call('getEscortTourV2', array($tour_id, $this->_request->lang_id));
		}
		$this->view->tour = $tour;

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( $tour ) $escort_id = $tour['escort_id'];
			if ( ! $this->agency->hasEscort($escort_id) ) {
				die('Permission denied!');
			}
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
			if ( $tour && $escort_id != $tour['escort_id'] ) {
				die('Permission denied!');
			}
		}
		
		if ( ! $escort_id ) die;


		$model = new Model_Escorts();
		$this->view->escort = $escort = $model->getById($escort_id);

		if ( $this->_request->isPost() ) {
			$form = new Cubix_Form_Data($this->_request);

			$form->setFields(array(
				'id' => 'int-nz',
				'country_id' => 'int-nz',
				'city_id' => 'int-nz',
				'date_from' => '',
				'date_to' => '',
				'is_time_on' => 'int',
				'minutes_from' => '',
				'minutes_to' => '',
				'diff_time' => '',
				'phone' => '',
				'email' => ''
			));
			$i_data = $form->getData();
			
			
			$i_data['date_from'] = strtotime($i_data['date_from']);
			$i_data['date_to'] = strtotime($i_data['date_to']);
			
			$time_arr_from = explode( ':',$i_data['minutes_from']);
			$time_arr_to = explode( ':',$i_data['minutes_to']);
			
			$i_data['minutes_from'] = (int)$time_arr_from[0]*60+$time_arr_from[1];
			$i_data['minutes_to'] = (int)$time_arr_to[0]*60+$time_arr_to[1];

			$validator = new Cubix_Validator();
					
			$model = new Cubix_Geography_Countries();
			if ( ! $model->exists($i_data['country_id']) ) {
				$validator->setError('country_id', 'Country is required');
			}

			if ( ! $model->hasCity($i_data['country_id'], $i_data['city_id']) ) {
				$validator->setError('city_id', 'City is required');
			}
			
			if ( $i_data['email'] ) {
				if ( ! Cubix_Validator::validateEmailAddress($i_data['email']) ) {
					$validator->setError('email', 'Invalid email address');
				}
			}
						
			if ( ! $i_data['date_from'] || ! $i_data['date_to'] ) {
				$validator->setError('date_from', 'Date is required');
			}
			if ( (! isset($i_data['minutes_from']) && isset($i_data['minutes_to'])) || ( isset($i_data['minutes_from']) && !isset($i_data['minutes_to'])) ) {
				$validator->setError('minutes_from', 'Time required');
			}
			if ( $validator->isValid() ) {
				$tours = $this->client->call('getEscortToursV2', array($escort_id, $this->_request->lang_id));
				$data['tours_tmp'] = $tours[0];
				
				$i_data['diff_time'] = intval($i_data['diff_time']);
				$date_to = $i_data['date_to'] = strtotime(date('d-m-Y', ($i_data['date_to'] + $i_data['diff_time'])));
				$date_from = $i_data['date_from'] = strtotime(date('d-m-Y', ($i_data['date_from'] + $i_data['diff_time'])));
				
				$i_data['minutes_from'] = (is_null($i_data['minutes_from']) || $i_data['minutes_from'] == "" )  ? null : intval($i_data['minutes_from']);
				$i_data['minutes_to']   = (is_null($i_data['minutes_to']) || $i_data['minutes_to'] == "" )  ? null : intval($i_data['minutes_to']);
				
				if($i_data['is_time_on']){
					$date_to = $i_data['date_to'] + $i_data['minutes_to'] * 60;
					$date_from = $i_data['date_from'] + $i_data['minutes_from'] * 60;
				}
				else{
					$date_to = $i_data['date_to'] += 60 * 60 * 24 - 1; // 23:59:59 
					
				}
				
				unset($i_data['is_time_on']);
				unset($i_data['diff_time']);
				if ( count($data['tours_tmp']) ) {
					foreach ( $data['tours_tmp'] as $t_k => $tr ) {
						unset($data['tours_tmp'][$t_k]['country']);
						unset($data['tours_tmp'][$t_k]['city']);
						$data['tours_tmp'][$t_k]['date_from'] = is_null($data['tours_tmp'][$t_k]['minutes_from']) ? $data['tours_tmp'][$t_k]['date_from'] : $data['tours_tmp'][$t_k]['date_from'] + $data['tours_tmp'][$t_k]['minutes_from'] * 60;
						$data['tours_tmp'][$t_k]['date_to'] = is_null($data['tours_tmp'][$t_k]['minutes_to']) ? $data['tours_tmp'][$t_k]['date_to'] : $data['tours_tmp'][$t_k]['date_to'] + $data['tours_tmp'][$t_k]['minutes_to'] * 60;
					}
				}
				
				$today = strtotime(date('d-m-Y', time()));
				if ( $date_to < $today || $date_from < $today ) {				
					$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
				}
				elseif($date_from >= $date_to ){
					$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
				}
				else{
										
					foreach ( $data['tours_tmp'] as $t ) {
						if ($t['id'] == $i_data['id']) continue;
						if ( ( $date_from >= $t['date_from'] && $date_from < $t['date_to']) ||
							 ( $date_to > $t['date_from'] && $date_to <= $t['date_to']) ||
							 ( $date_from < $t['date_from'] && $date_to > $t['date_to']) )
						{
							$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
							break;
						}
					}
				}
			}
			unset($data['tours_tmp']);
			if ( $validator->isValid() ) {
				if ( ! $tour ) {
					$this->client->call('addEscortTour', array($escort_id, $i_data));
				}
				else {
					$this->client->call('updateEscortTour', array($escort_id, $tour_id, $i_data));
				}
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
}