<?php
class Private_VideoController extends Zend_Controller_Action
{
	protected $session;
	private $model;
	private $user;
	protected $config;
	private $user_id;
	
	public function init()
	{
	
		$this->user = Model_Users::getCurrent();

		if ( !$this->user) { 
			
			$this->_redirect($this->view->getLink('signin'));
			return;
		}

		$this->client = Cubix_Api::getInstance();
		$this->app_id = Cubix_Application::getId();
		$cache = Zend_Registry::get('cache');
		$this->view->user = $this->user;
		
		$cache_key =  'v2_user_pva_' . $this->user->id;

		if ( $this->user->isAgency() ) {

			if ( ! $agency = $cache->load($cache_key) ) {
				$agency = $this->user->getAgency();
				$cache->save($agency, $cache_key, array(), 300);
			}
			
			$this->agency = $this->view->agency = $agency;
			$escort_id = intval($this->_getParam('escort_id'));
			
			if ( 1 > $escort_id ) $this->_redirect($this->_redirect($this->view->getLink('signin')));
			$client = new Cubix_Api_XmlRpc_Client();		
			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				$this->_redirect($this->_redirect($this->view->getLink('signin')));
			}
			$this->view->escort_id = $this->escort_id = $escort_id;
			$modelE = new Model_Escorts();
			$esc = $modelE->getById($escort_id);
			$this->view->is_suspicious = $this->is_suspicious = $esc->is_suspicious;
		}
		else if ( $this->user->isEscort() ) {

			if ( ! $escort = $cache->load($cache_key) ) {
				$escort = $this->user->getEscort();
				$cache->save($escort, $cache_key, array(), 300);
			}
			$this->escort = $this->view->escort = $escort;
			$this->view->escort_id = $this->escort_id = $this->escort->id;
			$this->view->is_suspicious = $this->is_suspicious = $escort->is_suspicious;
		}
	}
	
	
	public function indexAction()
	{
		$this->view->layout()->setLayout('private');
		$this->view->config =$this->config;
		if($this->user['user_type']!='agency')
		{	$video = $this->model->GetEscortVideo($this->user_id,$this->config['VideoCount']);
			$this->view->photo = $video[0];
			$this->view->video = $video[1];
		}
		else
		{
			$this->_helper->viewRenderer->setScriptAction("agency");
			$this->view->escorts = $this->model->GetAgencyEscort($this->user_id);
			
		}
		$sess = new Zend_Session_Namespace('video-tmp-session');
		if(isset($sess->tpm_error))
		{	
			$this->view->error = $sess->tpm_error;
			unset($sess->tpm_error);	
		}
		elseif(isset($sess->tpm_success))
		{
			$this->view->success = $sess->tpm_success;
			unset($sess->tpm_success);
		}
		$this->view->escort_id = $this->user['escort_data']['escort_id'];
		$this->view->host =  Cubix_Application::getById(Cubix_Application::getId())->host;
	}
		
	public function removeAction()
	{	    
		$this->_helper->layout()->disableLayout(); 
		$this->_helper->viewRenderer->setNoRender(true);
		$req =  $this->_request;
		if($req->isXmlHttpRequest() && $req->isPost() && isset($req->check) && is_array($req->check) && !empty($req->check))
		{	
			$res = $this->model->Remove(reset($req->check),$this->user['escort_data']['escort_id']);
			die("$res");
		}
		
	}
	
	public function getvideoAction()
	{	
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setScriptAction("agency_video");
		$req =  $this->_request;
		if($req->isXmlHttpRequest() && $this->user['user_type']=='agency' && $req->isPost() && isset($req->select_escort) && is_numeric($req->select_escort))
		{			
			$video = $this->model->GetEscortVideo($req->select_escort,$this->config['VideoCount'],$this->user_id);

			$this->view->host =  Cubix_Application::getById(Cubix_Application::getId())->host;
			$this->view->config =  Zend_Registry::get('videos_config');
			$this->view->escort_id = $req->select_escort;
			$this->view->photo = $video[0];
			$this->view->video = $video[1];
		}
		
	}
	
	public function videoAction()
	{
		$this->view->user = $this->user;
		$video_model = new Model_Video();
		$client = new Cubix_Api_XmlRpc_Client();
		$config =  Zend_Registry::get('videos_config');
		
		$action = $this->_getParam('act');
		
		switch($action){
			case 'upload':
				$host =  Cubix_Application::getById(Cubix_Application::getId())->host;
				$response = array(
					'error' => '',
					'finish' => FALSE
				);

				try {

					if($video_model->UserVideoCount($this->escort_id) < $config['VideoCount'])
					{			
						$backend_video_count = $client->call('Application.UserVideoCount',array($this->escort_id));
						
						$check = Cubix_Videos::CheckVideoStatus($backend_video_count, $config['VideoCount'], true);

						if(!$check)
						{	
							$video = new Cubix_Videos($this->escort_id,$config, true);
							$response = $video->getResponse();

							if($response['finish'] && $response['error'] === 0 ){
								
								ignore_user_abort(true);
								ob_start();
								Cubix_Videos::showResponse($response);
								header('Connection', 'close');
								header('Content-Length: '.ob_get_length());	
								ob_end_flush();
								ob_flush();
								flush();
								session_write_close();
								fastcgi_finish_request(); 

								$video->ConvertToMP4();
								
								//$video->AddMetaData();
								$image = $video->SaveImage();		
										
								if(is_array($image))
								{	

									if(!$video->SaveVideoImageFtpFrontend($image,$client)){
										throw new Exception('system error while uploading Video');
										//$response['error'] = Cubix_I18n::translate('sys_error_while_upload');
									}
								}
								else{
									throw new Exception($image);
									//$response['error'] = $image;
								}
							}
						}
						else{
							$response['error'] = $check;
						}
					}
					else{
						$response['error'] = Cubix_I18n::translate('sys_error_upload_video_count',array('count' => $config['VideoCount']));
					}
				} catch ( Exception $e ) {
					 $response['error'] = $e->getMessage();

				}
				Cubix_Videos::showResponse($response);
				die;
				
			case 'delete':
			   $escort_id = 0;
			   $video_id = intval($this->_getParam('video_id'));
		       $escort_id = intval($this->_getParam('escort_id'));
		       
			   $video_model->Remove($escort_id,array($video_id));
			die;
		}
	}

	public function naturalPicAction()
	{
		$this->view->user = $this->user;
		$nat_model = new Model_NaturalPic();
		$config =  Zend_Registry::get('images_config');
		$client = new Cubix_Api_XmlRpc_Client();


		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;

		}
		else {

			$escort_id = intval($this->_getParam('escort'));

			if ( 1 > $escort_id ) die;

			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}

			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}

		$action = $this->_getParam('a');

		switch($action){
			case 'upload':
				try {
					$model = new Model_Escort_Photos();
					$response = $model->upload();

					if($response['error'] == 0 && $response['finish']){
						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($response['tmp_name'], $escort_id. '/natural-pics', $this->app_id, strtolower(@end(explode('.', $response['name']))));
						$image = new Cubix_Images_Entry($image);
						$image->setSize('nat_pic_thumb');
						$image->setCatalogId($escort_id. '/natural-pics');
						$image_url = $images->getUrl($image);


						$photo_arr = array(
							'escort_id' => $escort_id,
							'hash' => $image->getHash(),
							'ext' => $image->getExt(),
							'creation_date' => date('Y-m-d H:i:s', time())
						);
						$new_id = $nat_model->save($photo_arr);


						$response['photo_id'] = $new_id;
						$response['photo_url'] = $image_url;
					}

					$model->showResponse($response);
					die;

				} catch ( Exception $e ) {
					echo json_encode(array(
						'finish' => 0,
						'error' => $e->getMessage()
					));
					die;
				}
				break;

			case 'delete':
				$nat_model->remove($escort_id);
				die;
		}




		$this->view->escort = $escort;

	}
}


