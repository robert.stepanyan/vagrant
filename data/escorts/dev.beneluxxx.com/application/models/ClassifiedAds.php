<?php

class Model_ClassifiedAds extends Cubix_Model
{
	public static function getAllCitiesForFilter()
    {
        $lng = Cubix_I18n::getLang();
        $sql = '
			SELECT
				ct.id AS city_id,
				ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug,
				r.title_' . $lng . ' AS region_title, ct.country_id, c.title_' . $lng . ' AS country_title
			FROM cities ct
			INNER JOIN classified_ads_cities ca ON ca.city_id = ct.id
			LEFT JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = ct.country_id
			GROUP BY ct.id ORDER BY ct.title_en ASC;
		';

        $cities = self::db()->fetchAll($sql);

        // divide cities array into array of arrays by city's country id
        $result = array();
        foreach ( $cities as $city ) {
            if ( ! isset($result[$city->country_id]) )
                $result[$city->country_id] = array();
            $result[$city->country_id][] = $city;
        }

        // find cities for each country from $result array
        $countries = Model_Statistics::getCountries($gender, $is_agency, $is_tour, $is_upcoming);
        foreach ( $countries as &$country ) {
            if ( isset($result[$country->country_id])) {
                $country->cities = $result[$country->country_id];
            }
        }

        return $countries;
    }
	
	//protected $_table = 'comments';
	//protected $_itemClass = 'Model_CommentItem';
	public function getList($filter = array(), $page = 1, $perPage = 20, &$count = null)
	{
		if( ! $page ) $page = 1;
		
		$limit = ' LIMIT ' . ($page - 1) * $perPage . ', ' . $perPage;
		$fields = '';
		$where = '';
		$order = '';
								
		$where = '';
		$order = ' is_premium DESC, approvation_date DESC ';
		
		if ( isset($filter['category']) && $filter['category'] ) {
			$where .= parent::quote(' AND ca.category_id = ?', $filter['category']);
		}
		
		if ( isset($filter['city']) && $filter['city'] ) {
			$where .= parent::quote(' AND cac.city_id = ?', $filter['city']);
		}
		
		if ( isset($filter['text']) && $filter['text'] ) {
			$where .= parent::quote(' AND ca.search_text LIKE ?', '%' . $filter['text'] . '%');
		}
		
		$sql = "
			SELECT SQL_CALC_FOUND_ROWS ca.*, c." . Cubix_I18n::getTblField('title') . " AS city_title
			FROM classified_ads ca
			INNER JOIN classified_ads_cities cac ON cac.ad_id = ca.id
			INNER JOIN cities c ON c.id = cac.city_id
			WHERE 1 {$where}
			GROUP BY ca.id
			ORDER BY {$order}
			{$limit}
		";
		//echo $sql;die;
		$items = $this->getAdapter()->fetchAll($sql);
		$count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		
		if ( count($items) ) {
			foreach ( $items as $i => $item ) {
				$items[$i]->images = $this->getAdapter()->fetchAll('
					SELECT 
						id AS image_id, hash, ext, ' . Cubix_Application::getId() . ' AS application_id 
					FROM classified_ads_images
					WHERE ad_id = ?
				', array($item->id));
			}
		}
		
		return $items;
	}	
	
	public function updateViewCount($ad_id)
	{
		$this->getAdapter()->query('UPDATE classified_ads SET view_count = view_count + 1 WHERE id = ?', array($ad_id));
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$client->call('ClassifiedAds.updateViewCount', array($ad_id));
	}
	
	public function save($data)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$ad_id = $client->call('ClassifiedAds.save', array($data));
		
		return $ad_id;
	}
	
	public function get($id)
	{
		$sql = "
			SELECT
				ca.*, c." . Cubix_I18n::getTblField('title') . " AS city_title
			FROM classified_ads ca
			INNER JOIN classified_ads_cities cac ON cac.ad_id = ca.id
			INNER JOIN cities c ON c.id = cac.city_id
			WHERE ca.id = ?
		";
		
		$ad = $this->getAdapter()->fetchRow($sql, array($id));
		
		$ad_sql = '
			SELECT
				id AS image_id, hash, ext, ' . Cubix_Application::getId() . ' AS application_id 
			FROM classified_ads_images
			WHERE ad_id = ?
		';
		
		$ad->images = $this->getAdapter()->fetchAll($ad_sql, array($ad->id));
		
		return $ad;
	}

	public function checkPhoneInProfile($phone)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$esc = $client->call('ClassifiedAds.checkPhoneInProfile', array($phone));

		return $esc;
	}

    /**
     * @param $ip string Ip address of client
     * @param $interval int Hour
     * @return string count of found rows
     * @throws Exception
     */
    public function checkCreationIntervalByIp($ip, $interval) {
        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $count = $client->call('ClassifiedAds.checkCreationIntervalByIp', array($ip, $interval));

        return $count;
	}
}
