<?php
class Model_MembersList extends Cubix_Model
{
	function getMembers( $page, $config, $arg_filter, $ord_field, $ord_dir ){
		$client = new Cubix_Api_XmlRpc_Client();
		$data = $client->call('Members.getMembers', array($page, $config, $arg_filter, $ord_field, $ord_dir ));
		return $data;
	}
}