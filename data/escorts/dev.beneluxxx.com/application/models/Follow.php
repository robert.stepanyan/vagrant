<?php

class Model_Follow extends Cubix_Model
{
	public function searchMember($name){

		$client = new Cubix_Api_XmlRpc_Client();

		$user = Model_Users::getCurrent();

		$members = $client->call('Members.searchMember', array($name, $user->id));
		return $members;
	}

	public function follow($followed_id,$events,$edit_mode = false){

		$client = new Cubix_Api_XmlRpc_Client();

		$user = Model_Users::getCurrent();

		$escortId = false;
		if(!$user->isMember()){
			$escortModel = new Model_EscortsV2();
			$escort = $escortModel->getByUserId($user->id);
			if($escort)
				$escortId = $escort->id;
		}

		$response = $client->call('Members.follow', array($followed_id, $events, $user->id, $escortId, $edit_mode));

		return $response;
	}

	public function getFollowers($type, $page, $per_page, $search){

		$client = new Cubix_Api_XmlRpc_Client();

		$user = Model_Users::getCurrent();

		$response = $client->call('Members.getFollowers', array($user->id, $type, $page, $per_page, $search));


		return $response;
	}

	public function getEvents($followed_id){

		$client = new Cubix_Api_XmlRpc_Client();

		$user = Model_Users::getCurrent();

		$response = $client->call('Members.getEvents', array($user->id, $followed_id));

		return $response;
	}

	public function approve($follow_id){
		$client = new Cubix_Api_XmlRpc_Client();

		$user = Model_Users::getCurrent();

		$response = $client->call('Members.approve', array($follow_id, $user->id));

		return $response;	
	}

	public function deny($follow_id){
		$client = new Cubix_Api_XmlRpc_Client();

		$user = Model_Users::getCurrent();

		$response = $client->call('Members.deny', array($follow_id, $user->id));

		return $response;	
	}

	public function unFollow($follow_id){
		$client = new Cubix_Api_XmlRpc_Client();

		$user = Model_Users::getCurrent();

		$response = $client->call('Members.unFollow', array($follow_id, $user->id));

		return $response;	
	}

	public function removeMyFollower($follow_id){
		$client = new Cubix_Api_XmlRpc_Client();

		$user = Model_Users::getCurrent();

		$response = $client->call('Members.removeMyFollower', array($follow_id, $user->id));

		return $response;	
	}
	
	public function getFollowCount($member_id){
		$client = new Cubix_Api_XmlRpc_Client();

		$member_id = (int)$member_id;

		if($member_id){
			$response = $client->call('Members.getFollowCount', array($member_id));
			return	$response;
		}
	}

	public function checkAlreadyFollow($member_id){
		$client = new Cubix_Api_XmlRpc_Client();

		$member_id = (int)$member_id;
		$user = Model_Users::getCurrent();

		if($member_id){
			$response = $client->call('Members.alreadyFollowed', array($member_id, $user->id));
			return	$response;
		}
	}
}
