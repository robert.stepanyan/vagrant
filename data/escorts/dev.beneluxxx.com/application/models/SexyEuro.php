<?php

class Model_SexyEuro extends Cubix_Model
{
	public function getAll($filter = array(), $sort = "r", $page = 1, $page_size = 15, &$count = 0)
	{	
		
		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;

		
		$lng = Cubix_I18n::getLang();


		$sql = '
			SELECT el.escort_id, el.hash, el.ext, e.showname,  el.votes,  c.title_' . $lng . ', el.ordering
			FROM euro_lottery AS el
			INNER JOIN
				escorts AS e ON el.escort_id = e.id 
			INNER JOIN
				cities AS c ON c.id = e.city_id
			ORDER BY el.votes
		';


		$escorts = self::db()->fetchAll($sql);

		$images = new Cubix_Images();

		for ($i = 0; $i < count($escorts); $i++) {
			
			$escorts[$i]->photo_url = $images->getUrl(new Cubix_Images_Entry(array(
				'application_id' => $this->application_id,
				'catalog_id' => $escorts[$i]->escort_id . '/euro-lottery',
				'hash' => $escorts[$i]->hash,
				'ext' => $escorts[$i]->ext,
				'size' => 'xl_thumb'
			)));
		}	

		return $escorts;

	}

	public function getAllAjax()
	{
		$lng = Cubix_I18n::getLang();

		$sql = '
			SELECT el.escort_id, el.hash, el.ext, e.showname, el.votes, c.title_' . $lng . ', el.ordering
			FROM euro_lottery AS el
			INNER JOIN
				escorts AS e ON el.escort_id = e.id 
			INNER JOIN
				cities AS c ON c.id = e.city_id
			ORDER BY el.ordering
		';

		$escorts = self::db()->fetchAll($sql);

		$images = new Cubix_Images();

		for ($i = 0; $i < count($escorts); $i++) {
			
			$escorts[$i]->photo_url_small = $images->getUrl(new Cubix_Images_Entry(array(
				'application_id' => $this->application_id,
				'catalog_id' => $escorts[$i]->escort_id . '/euro-lottery',
				'hash' => $escorts[$i]->hash,
				'ext' => $escorts[$i]->ext,
				'size' => 'euro_thumb_small'
			)));
			
			$escorts[$i]->photo_url_big = $images->getUrl(new Cubix_Images_Entry(array(
				'application_id' => $this->application_id,
				'catalog_id' => $escorts[$i]->escort_id . '/euro-lottery',
				'hash' => $escorts[$i]->hash,
				'ext' => $escorts[$i]->ext,
				'size' => 'euro_thumb_big'
			)));
		}

		return $escorts;
	}
}