<?php
class Zend_View_Helper_HumanTiming extends Zend_View_Helper_Abstract
{
	function humanTiming($time, $format = false){

	    $time = time() - $time; // to get the time since that moment
	    $time = ($time<1)? 1 : $time;
	    if($format){
	    	$tokens = array (
		        31536000 => ' year',
		        2592000 => ' month',
		        604800 => ' week',
		        86400 => ' day',
		        3600 => ' hour',
		        60 => ' minute',
		        1 => ' second'
		    );
	    }else{
	    	$tokens = array (
		        31536000 => 'year',
		        2592000 => 'm’th',
		        604800 => 'w',
		        86400 => 'd',
		        3600 => 'h',
		        60 => 'm',
		        1 => 's'
		    );
	    }
	    

	    foreach ($tokens as $unit => $text) {
	        if ($time < $unit) continue;
	        $numberOfUnits = floor($time / $unit);

	        if($format){
	        	 // You can add "s" when unit is multiple 
	        	$text .= (($numberOfUnits>1)?'s':'');
	        }
	       
	       
	      	$howlong = $numberOfUnits.$text.' ago';
	      	if($text == 's'){
				$howlong = 'just now';
	      	}

	        return $howlong;
	    }

	}
}