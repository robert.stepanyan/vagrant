function Config() {
	var config = {
		production : {
			common : {
				applicationId : 30,
				listenPort : 8899,
				host: 'www.beneluxxx.com',
				authPath : '/get_user_info.php',
				sessionChacheTime: 1000 * 60 * 10, //10 minutes
				sessionCheckInterval : 1 * 60 * 60 * 1000, //1 hour
				imagesHost : 'https://pic.beneluxxx.com/beneluxxx.com/',
				avatarSize : 'lvthumb',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusLsdf8A9',
				blWordsLifeTime : 12 * 60 * 60 * 1000, //12 hours
				redisBlWordsDb: 3,
				redisBlWordsKey: 'bl-words-bl'
			},
			db : {
				user:     'bl_chat',
				database: 'beneluxxx',
				// password: 'BLserrGfg^^%5GF',
				password: '63d9faab109e34d1',
				// host:     'db.bl.com'
				host:     'galera02'
			},
			memcache : {
				host : 'memcached01',
				port : 11214
			}
		},
		development: {
			common : {
				applicationId : 30,
				listenPort : 8888,
				host: 'www.escortforum.net.dev',
				authPath: '/get_user_info.php',
				sessionChacheTime: 1000 * 60 * 10, //10 minutes
				imagesHost : 'http://pic.escortforum.net/escortforum.net/',
				avatarSize : 'lvthumb',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusLsdf8A9',
				blWordsLifeTime : 12 * 60 * 60 * 1000, //12 hours
				redisBlWordsDb: 3,
				redisBlWordsKey: 'bl-words-bl'
			},
			db : {
				user:     'sceon',
				database: 'escortforum.net_backend',
				password: '123456',
				host:     '192.168.0.1'
			},
			memcache : {
				host : '172.16.1.51',
				port : 11211
			}
		}
	};
	
	this.get = function(type) {
		if ( type == 'development' ) {
			return config.development;
		} else {
			return config.production;
		}
	}
}

function get(type) {
	conf = new Config();
	return conf.get(type);
}



exports.get = get;
