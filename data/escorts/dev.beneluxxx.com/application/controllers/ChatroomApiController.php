<?php

class ChatroomApiController extends Zend_Controller_Action
{
    /**
     * @var Zend_Db_Adapter_Mysqli
     */
    protected $_db;
    protected $_debug = true;
    protected $_log;
    /**
     * @var Model_Users
     */
    protected $_usersModel;

    /**
     * @var Cubix_Api
     */
    protected $_client;

    public function init()
    {
        $this->_db = Zend_Registry::get('db');
        $this->_client = Cubix_Api::getInstance();

        $this->_usersModel = new Model_Users();
        $this->user = Model_Users::getCurrent();
        $this->view->layout()->disableLayout();
    }

    /**
     *
     */
    public function getInitialStateAction()
    {
        if($this->user) {
            $userData = $this->_usersModel->getById($this->user->id);
            $this->user['im_is_blocked'] = $userData->im_is_blocked;
            $this->user['chat_info']['imIsBlocked'] = $userData->im_is_blocked;
        }

        $response = [
            'user' => $this->user,
        ];

        exit(json_encode($response));
    }

    /**
     *
     */
    public function reportAction()
    {

        $senderId = $this->_getParam('senderId');
        $reportedUserId = $this->_getParam('reportedUserId');
        $reason = $this->_getParam('reason');

        $client = new Cubix_Api_XmlRpc_Client();
        $sender = $client->call('Users.getById', array($senderId));
        $scammer = $client->call('Users.getById', array($reportedUserId));

        $body = "
            <h3>Beneluxxx.com Chat new Report</h3> <br/><br/>
            
            <b>Reporter</b>: </br>
                <ul>
                    <li><b>Username: </b> {$sender['username']}</li>
                    <li><b>Email: </b> {$sender['email']}</li>
                    <li><b>Last IP: </b> {$sender['last_ip']}</li>
                    <li><b>User Type: </b> {$sender['user_type']}</li>
                    <li><b>Reason: </b> {$reason}</li>
                </ul> 
                
            <b>Scammer</b>: </br>
                <ul>
                    <li><b>Username: </b> {$scammer['username']}</li>
                    <li><b>Email: </b> {$scammer['email']}</li>
                    <li><b>Last IP: </b> {$scammer['last_ip']}</li>
                    <li><b>User Type: </b> {$scammer['user_type']}</li>
                </ul> 
        ";


        if(APPLICATION_ENV === 'development') {
            Cubix_Email::send('edhovhannisyan97@gmail.com', 'New Report on User', $body, true);
        }else {
            Cubix_Email::send('tom@beneluxxx.com', 'New Report on User', $body, true);
        }

        $response = [
            'status' => 'success',
            'body' => $body
        ];

        exit(json_encode($response));
    }

}