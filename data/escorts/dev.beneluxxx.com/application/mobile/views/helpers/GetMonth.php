<?php

class Zend_View_Helper_GetMonth
{
	public function getMonth($month_index, $lang_id = 'en_US')
	{
		switch ($lang_id) {
			case 'en':
				$lang_id = 'en_US';
			break;
			case 'de':
				$lang_id = 'de_AT';
			break;
			case 'it':
				$lang_id = 'it_IT';
			break;
			case 'fr':
				$lang_id = 'fr_FR';
			break;
			case 'gr':
				$lang_id = 'gr_GR';
			break;
		}

		$date = new Zend_Date();

		$date->set(1, Zend_Date::DAY);
		$date->set($month_index, Zend_Date::MONTH);
		$month = ucwords($date->get(Zend_Date::MONTH_NAME, $lang_id));

		return $month;
	}
}
