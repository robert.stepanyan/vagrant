<?php

class Mobile_ChatroomApiController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;
	protected $_debug = true;
	protected $_log;

	/**
	 * @var Cubix_Api
	 */
	protected $_client;

	public function init()
	{
        $this->_db = Zend_Registry::get('db');
		$this->_client = Cubix_Api::getInstance();

        $this->user = Model_Users::getCurrent();
        $this->view->layout()->disableLayout();
	}

    /**
     *
     */
    public function getInitialStateAction() {

	    $response = [
	        'user' => $this->user,
            'ln' => $_COOKIE
        ];

	    exit(json_encode($response));
    }

}