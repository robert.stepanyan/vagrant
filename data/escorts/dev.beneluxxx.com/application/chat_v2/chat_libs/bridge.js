/*jslint es6*/
/*jslint node*/

'use strict';

const config = require('../configs').get('production');
const request = require('superagent');

let cacheState = {};

const getUserInfoPromise = (sessionId, useCache = true) => {
  return new Promise((resolve, reject) => {
    let currentDate = new Date();

    if (useCache && cacheState[sessionId] && currentDate - cacheState[sessionId].date < config.common.sessionCacheTime) {
      return resolve(cacheState[sessionId].info);
    }

    request.post(`http:\/\/${config.common.host}/${config.common.authPath}`)
      .set('Content-Type', 'application/x-www-form-urlencoded')
      // .type('json')
      .send({
        compilation_level: 'ADVANCED_OPTIMIZATIONS',
        output_format: 'json',
        output_info: 'compiled_code',
        warning_level: 'QUIET',
        sid: sessionId
      })
      .then(resp => {
        cacheState[sessionId] = {
          info: resp.body,
          date: new Date
        };

        return resolve(resp.body);
    })
    .catch(err => console.log(`ERROR: Error in response ${err}`));
  });
};

module.exports.getUserInfo = getUserInfoPromise
