/*jslint es6*/
/*jslint node*/

'use strict';

const http = require('http');

const socketIo = require('socket.io');

const config = require('../configs').get('production');
const userManager = require('./userManager');
const bookingManager = require('./bookingManager');

const app = http.createServer((req, resp) => { }).listen(config.common.instantBookListenPort);
const io = socketIo(app);

const start = () => {
  io.on('connection', (socket) => {
    const userType = socket.handshake.query.uType;
    const escortId = socket.handshake.query.eId;
    const token = socket.handshake.query.t;

    if (userType === 'escort') {
      userManager.addUser(userType, escortId, socket.id);
    } else {
      userManager.addUser(userType, token, socket.id);
    }

    socket.on('request_booking', data => {
      data.hash = token;
      console.log(data);
      bookingManager.addBookingRequestPromise(data).then(result => {
        if (result.status === 'success') {
          userManager.getUserList('escort')[data.escort_id].sockets.forEach(sId => {
            io.to(sId).emit('new_booking', data);
          });

          socket.emit('instant_request_success', { status: 'success', count: data.wait_time });
        } else {
          socket.emit('request_booking_error', result.msg);
        }
      }).catch((error) => {
        console.log(error);
      });
    });

    socket.on('booking_request_reply', data => {

      io.to(data.token).emit("request_reply_guest", {
        status: data.status
      });
    });

    // remove user from the list if disconnected
    socket.on('disconnect', () => {
      userManager.removeUser(userType, socket.id);
    });

  });
};

exports.start = start;