/*jslint es6*/
/*jslint node*/

'use strict';

module.exports = (() => {
  const configs = {
    production: {
      common: {
        applicationId: 30,
        listenPort: 8888,
        host: 'dev.beneluxxx.com',
        authPath: 'get_user_info.php',
        sessionCacheTime: 1000 * 60 * 10, //10 minutes
        sessionCheckInterval: 1 * 60 * 60 * 1000, //1 hour
        imagesHost: 'https://pic.beneluxxx.com/beneluxxx.com/',
        avatarSize: 'lvthumb',
        noPhotoUrl: '/img/chat/img-no-avatar.gif',
        userSettingsBackupPath: 'user_settings.bk',
        key: 'JFusLsdf8A9',
        blWordsLifeTime: 12 * 60 * 60 * 1000, //12 hours
        redisBlWordsDb: 0,
        redisBlWordsKey: 'bl-words-ef'
      },
      pushNotifications: {
        auth: 'ul46sers9wf9u12nz40mgyj6weiybjpo:3kfa34irl0h3a75jf4rpjxpeaj5pemhm',
        url: 'https://dashboard.goroost.com/api/v2/notifications'
      },
      db: {
        user: 'bl_cubix',
        database: 'beneluxxx',
        password: 'vmm177np1f666sB3a84554DSu',
        host: 'galera',
        port: 3306
      },
      redis: {
        host: '127.0.0.1',
        port: 6379
      },
      memcache: {
        host: 'memcached01',
        port: 11214
      }
    },

    development: {
      common: {
        applicationId: 1,
        listenPort: 8888,
        instantBookListenPort: 8889,
        host: 'dev.beneluxxx.com.test',
        authPath: 'get_user_info.php',
        sessionCacheTime: 1000 * 60 * 10, //10 minutes
        sessionCheckInterval: 30 * 1000, //1 hour
       imagesHost: 'https://pic.beneluxxx.com/beneluxxx.com/',
        avatarSize: 'lvthumb',
        noPhotoUrl: '/img/chat/img-no-avatar.gif',
        userSettingsBackupPath: 'user_settings.bk',
        key: 'JFusLsdf8A9',
        blWordsLifeTime: 12 * 60 * 60 * 1000, //12 hours
        redisBlWordsDb: 0,
        redisBlWordsKey: 'bl-words-ef'
      },
      db: {
        user: 'root',
        database: 'beneluxxx_com_back',
        password: 'sceon123',
        host: 'mysql-dev1.ers.sceon.lan'
      },
      redis: {
        host: '127.0.0.1',
        port: 6379
      },
      memcache: {
        host: '127.0.0.1',
        port: 49664
      }
    }
  };


  const get = env_ => {
    !env_ && (env_ = 'production');

    return configs[env_];
  };

  return { get: get };
})();
