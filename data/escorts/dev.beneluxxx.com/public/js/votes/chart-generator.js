var Charts = {
    labels : null,
    data_1 : null,
    data_2 : null,
    proc : null,
    height: 85,

    init: function( labels, data_1, data_2 ){
        var self = this;

        self.labels = labels;
        self.data_1 = data_1;
        self.data_2 = data_2;

        self.renderLabels();
        self.proc = self.checkMax();

        self.renderLeftBars();
        self.renderRightBars();
    },

    renderLabels: function(){
        var self = this;

        for ( var i in self.labels ){
            var j = Number( i ) + 1;
            $$( '.label-' + j ).set( 'text', self.labels[i] );
        }
    },

    checkMax: function(){
        var self = this;

        var max_1 = Math.max.apply( null, self.data_1 );
        var max_2 = Math.max.apply( null, self.data_2 );
        return 100 / ( Math.max( max_1, max_2 ) );
    },

    renderLeftBars: function(){
        var self = this;

        for ( var i in self.data_1 ){
            var j = Number( i ) + 1;
            var height = ( ( Number( self.data_1[i] ) * Number( self.proc ) ) * self.height ) / 100;
            var top = self.height - height;
            $$( '.left-' + j ).morph({ 'top': top + '%', 'height': height + '%' });
            if( Number( self.data_1[i] ) > 0 ){
                $$( '.left-' + j  + ' .bar-value' ).set("html", self.data_1[i] + '<br/>Votes');
                $$( '.left-' + j  + ' .bar-value' ).morph({ 'top': '-26%' });
            }
        }
    },

    renderRightBars: function(){
        var self = this;

        for ( var i in self.data_2 ){
            var j = Number( i ) + 1;
            var height = ( ( Number( self.data_2[i] ) * Number( self.proc ) ) * self.height ) / 100;
            var top = self.height - height;
            $$( '.right-' + j ).morph({ 'top': top + '%', 'height': height + '%' });
            if( Number( self.data_2[i] ) > 0 ){
                $$( '.right-' + j  + ' .bar-value' ).set("html", self.data_2[i] + '<br/>Votes');
                $$( '.right-' + j  + ' .bar-value' ).morph({ 'top': '-26%' });
            }
        }
    }
};
