Cubix.gotmSlideshow = {};

Cubix.gotmSlideshow.container = 'gotm-slideshow-pics';
Cubix.gotmSlideshow.duration = 4000;
Cubix.gotmSlideshow.currentIndex = 0;
Cubix.gotmSlideshow.images;
Cubix.gotmSlideshow.periodicalID;

Cubix.gotmSlideshow.Init = function () {
    Cubix.gotmSlideshow.images = $(Cubix.gotmSlideshow.container).getElements('img');
    /* opacity and fade */
    Cubix.gotmSlideshow.images.each(function(img,i){
        if(i > 0) {
            img.set('opacity',0);
        }
    });
}

Cubix.gotmSlideshow.Start = function () {
    Cubix.gotmSlideshow.periodicalID = (function(){
        Cubix.gotmSlideshow.images[Cubix.gotmSlideshow.currentIndex].fade('out');
        Cubix.gotmSlideshow.currentIndex = Cubix.gotmSlideshow.currentIndex < Cubix.gotmSlideshow.images.length - 1 ? Cubix.gotmSlideshow.currentIndex + 1 : 0;
        Cubix.gotmSlideshow.images[Cubix.gotmSlideshow.currentIndex].fade('in');
    }).periodical(Cubix.gotmSlideshow.duration);

}

window.addEvent('domready', function () {
    new Request({
        method: 'get',
        url: '/escorts/gotm-current',
        onComplete: function(response) {
            $('gotm-widget-wrap').set('html', response);

            if (response.length)
            {
                Cubix.gotmSlideshow.Init();
                Cubix.gotmSlideshow.Start();

                $(Cubix.gotmSlideshow.container).addEvents({
                    mouseenter: function() {
                        //temporarily stop
                        $clear(Cubix.gotmSlideshow.periodicalID);
                    },
                    mouseleave: Cubix.gotmSlideshow.Start
                });
            }
        }
    }).send();
});