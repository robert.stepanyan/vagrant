$('document').ready(function () {
	var containerMain = 'common-fav';
	var url = '/private-v3/favorites/ajax-favorites';
	var containerTop = 'top-fav';
	var urlTop = '/private-v3/favorites/ajax-favorites-top';
	var search_input_text = 'search by showname';
	var showname = "";
	var allowScroll = true;

	CubixShow = function(data, allowScroll) {
		if (event)
		{
			event.preventDefault();
			$('.nav-link').removeClass('active');
			if ( $(this).hasClass('nav-link') )
				$(this).addClass('active');
		}

		$('#fav-msg').attr('class', '');
		$('#fav-msg').html('');
		$('#fav-msg').addClass('none');

		var container = containerMain;
		data.showname = data.showname ? data.showname : showname;

		if ($('#city').val())
			data.city = $('#city').val();

		if (data.showname == showname)
		{
			if ($('#searchV3').val().length > 0 )
				data.showname = $('#searchV3').val();
		}

		if (!data.act)
		{
			data.act = $('#active-action').val();
		}
		else
		{
			$('#active-action').attr('value', data.act);
		}

		if (!data.per_page)
		{
			data.per_page = $('#page-action').val();
		}
		else
		{
			$('#page-action').attr('value', data.per_page);
		}

		$.ajax({
			url: url,
			type: 'GET',
			data: data,
			beforeSend:function () {
				$('.card-body-fav').LoadingOverlay("show", true);
			},
			success: function(resp) {
				$('#'+container).html(resp);
				$('#act-all').attr('class', '');
				$('#act-active').attr('class', '');
				$('#act-inactive').attr('class', '');
				if (data.act == 1)
				{
					$('act-all').addClass('sel');
				}
				else if (data.act == 2)
				{
					$('act-active').addClass('sel');
				}
				else if (data.act == 3)
				{
					$('act-inactive').addClass('sel');
				}

				$('#page-12').attr('class', '');
				$('#page-24').attr('class', '');
				$('#page-48').attr('class', '');
				$('#page-96').attr('class', '');
				$('#page-all').attr('class', '');

				if (data.per_page == 12)
				{
					$('#page-12').addClass('sel');
				}
				else if (data.per_page == 24)
				{
					$('#page-24').addClass('sel');
				}
				else if (data.per_page == 48)
				{
					$('#page-48').addClass('sel');
				}
				else if (data.per_page == 96)
				{
					$('#page-96').addClass('sel');
				}
				else if (data.per_page == 1000)
				{
					$('#page-all').addClass('sel');
				}
				$('.card-body-fav').LoadingOverlay("hide", true);
			},
			error: function(e) {
				alert('Somethings went wrong, please try again')
				$('.card-body-fav').LoadingOverlay("hide", true);
				//called when there is an error
				//console.log(e.message);
			}
		});

		return false;
	}

	CubixShowTop = function() {
		var container = containerTop;

		$.ajax({
			url: urlTop,
			type: 'GET',
			beforeSend:function () {
				$('.card-body-top').LoadingOverlay("show", true);
			},
			success: function(resp) {
				$('#'+container).html(resp);
				$('.card-body-top').LoadingOverlay("hide", true);
			},
			error: function(e) {
				alert('Somethings went wrong, please try again')
				$('.card-body-top').LoadingOverlay("hide", true);
			}
		});

		return false;
	};

	$(document).on('keyup','#searchV3',function () {
		var showname = $(this).val();
		CubixShow({ page: 1, act: 1,showname: showname}, true,);
	});

	var page = $('input[name=page]').val();
	var perPage = $('input[name=per-page]').val();

	CubixShow({ page: page, per_page: perPage}, true);
	CubixShowTop();
});


$(document).on('click','.plus', function(event) {
	$(this).closest('div.escort').addClass('comm-height');
	event.preventDefault();
	var $this = $(this);

	$this.removeClass('plus');
	$this.addClass('minus');

	var controlBox = $this.closest('div.control-box');
	controlBox.css('height', '228px');
	$(this).closest('.comment-box').children("form").removeClass('none');
});



$(document).on('click','.minus', function(event) {
	$(this).closest('div.escort').removeClass('comm-height');
	event.preventDefault();
	var $this = $(this);

	$this.removeClass('minus');
	$this.addClass('plus');

	var controlBox = $this.closest('div.control-box');
	controlBox.css('height', '100px');
	$(this).closest('.comment-box').children("form").addClass('none');
});


CubixFavoritesRemove = function(escortId) {
	var data = {
		escort_id: escortId
	};
	$.ajax({
		url: '/private-v3/favorites/remove-from-favorites',
		type: 'post',
		data: data,
		beforeSend:function () {
			$('.card-body-fav').LoadingOverlay("show", true);
		},
		success: function(resp) {
			if (JSON.parse(resp).success) {
				CubixShow({page: 1}, false);
				CubixShowTop();

				$('#fav-msg').removeClass('none');
				$('#fav-msg').addClass('msg-suc');
				$('#fav-msg').html(resp.success);
			}
			$('.card-body-fav').LoadingOverlay("hide", true);
		},
		error: function(e) {
			alert('Somethings went wrong, please try again')
			$('.card-body-fav').LoadingOverlay("hide", true);
		}
	});

	return false;
};

CubixFavoritesAddToTop10 = function(escortId) {
	event.preventDefault();
	var data = {
		escort_id: escortId
	};
	var page = $('input[name=page]').val();
	var perPage = $('input[name=per-page]').val();

	$.ajax({
		url: '/private-v3/favorites/ajax-favorites-add-to-top',
		type: 'post',
		data: data,
		beforeSend:function () {
			$('.card-body-fav').LoadingOverlay("show", true);
		},
		success: function(resp) {
			if (JSON.parse(resp).success) {
				CubixShow({ page: page, per_page: perPage}, true);
				CubixShowTop();

				$('fav-msg').removeClass('none');
				$('fav-msg').addClass('msg-suc');
				$('fav-msg').html(resp.success);

			}
			else
			{
				$('fav-msg').removeClass('none');
				$('fav-msg').addClass('msg-err');
				$('fav-msg').html(resp.error);

			}
			$('.card-body-fav').LoadingOverlay("hide", true);
		},
		error: function(e) {
			alert('Somethings went wrong, please try again')
			$('.card-body-fav').LoadingOverlay("hide", true);
		}
	});

	return false;
};

CubixFavoritesAddComment = function(form) {
	event.preventDefault();
	var fd = new FormData(form);

	var data = {
		comment: fd.get("textarea"),
		escort_id: fd.get("input")
	};
	var page = $('input[name=page]').val();
	var perPage = $('input[name=per-page]').val();
	$.ajax({
		url: '/private-v3/favorites/add-fav-comment',
		type: 'post',
		data: data,
		beforeSend:function () {
			$('.card-body-fav').LoadingOverlay("show", true);
		},
		success: function(resp) {
			if (JSON.parse(resp).success) {
				CubixShow({ page: page, per_page: perPage}, true);
				CubixShowTop();
			}
			$('.card-body-fav').LoadingOverlay("hide", true);
		},
		error: function(e) {
			alert('Somethings went wrong, please try again')
			$('.card-body-fav').LoadingOverlay("hide", true);
		}
	});

	return false;
};

CubixFavoritesRemoveFromTop10 = function(escortId) {
	event.preventDefault();

	var data = {
		escort_id: escortId
	};

	$.ajax({
		url: '/private-v3/favorites/ajax-favorites-remove-from-top',
		type: 'post',
		data: data,
		beforeSend:function () {
			$('.card-body-top').LoadingOverlay("show", true);
		},
		success: function(resp) {
			if (JSON.parse(resp).success) {
				CubixShow({page: 1}, false);
				CubixShowTop();

				$('fav-msg').removeClass('none');
				$('fav-msg').addClass('msg-suc');
				$('fav-msg').html(resp.success);
			}
			$('.card-body-top').LoadingOverlay("hide", true);
		},
		error: function(e) {
			alert('Somethings went wrong, please try again')
			$('.card-body-top').LoadingOverlay("hide", true);
		}
	});

	return false;
};

CubixFavoritesDown = function(escortId) {
	event.preventDefault();
	var data = {
		escort_id: escortId
	};

	$.ajax({
		url: '/private-v3/favorites/ajax-favorites-down',
		type: 'post',
		data: data,
		beforeSend:function () {
			$('.card-body-top').LoadingOverlay("show", true);
		},
		success: function(resp) {
			if (JSON.parse(resp).success) {
				CubixShowTop();
			}
			$('.card-body-top').LoadingOverlay("hide", true);
		},
		error: function(e) {
			alert('Somethings went wrong, please try again')
			$('.card-body-top').LoadingOverlay("hide", true);
		}
	});

	return false;
};

CubixFavoritesUp = function(escortId) {
	event.preventDefault();
	var data = {
		escort_id: escortId
	};
	$.ajax({
		url: '/private-v3/favorites/ajax-favorites-up',
		type: 'post',
		data: data,
		beforeSend:function () {
			$('.card-body-top').LoadingOverlay("show", true);
		},
		success: function(resp) {
			if (JSON.parse(resp).success) {
				CubixShowTop();
			}
			$('.card-body-top').LoadingOverlay("hide", true);
		},
		error: function(e) {
			alert('Somethings went wrong, please try again')
			$('.card-body-top').LoadingOverlay("hide", true);

		}
	})

	return false;
}


CubixFavoritesOpenPopup = function (f_id) {

	$('#fr-popup-' + f_id).removeClass('none');

};

CubixFavoritesX = function (f_id) {
	$('#fr-popup-' + f_id).addClass('none');

	return false;
}
CubixFavoritesShare = function (req_id, f_id, type) {
	var data = {
		req_id: req_id,
		type: type
	};
	$.ajax({
		url: '/private-v3/favorites/share-fav-comment',
		type: 'post',
		data: data,
		beforeSend:function () {
			$('#fav-popup-middle-wrap-' + f_id).LoadingOverlay("hide", true);
		},
		success: function(resp) {
			if (JSON.parse(resp).success) {
				$('#r-row-' + req_id).remove();
				var c = parseInt($('#c-' + f_id).html());
				c--;

				if (c > 0) {
					$('#c-' + f_id).html(c);
				}else
				{
					$('#c-' + f_id).remove();
					$('#fr-popup-' + f_id).remove();
				}

			}
			$('#fav-popup-middle-wrap-' + f_id).LoadingOverlay("hide", true);
		},
		error: function(e) {
			alert('Somethings went wrong, please try again')
			$('#fav-popup-middle-wrap-' + f_id).LoadingOverlay("hide", true);
		}
	});


	return false;
};
CubixFavoritesChangeType = function (f_id) {
	var new_val = $('input[name="type_'+f_id+'"]:checked').val();
	var data = {
		new_val: new_val,
		f_id: f_id
	};
	$.ajax({
		url: '/private-v3/favorites/change-fav-comment-type',
		type: 'post',
		data: data,
		beforeSend:function () {
			$('.card-body-top').LoadingOverlay("show", true);
		},
		success: function(resp) {
			if (JSON.parse(resp).success) {
				if (new_val == 1)
				{
					$('#blocked-' + f_id).removeClass('none');

					if ($('#c-' + f_id))
					{
						$('#c-' + f_id).addClass('none');
					}
				}
				else if (new_val == 2)
				{
					$('#blocked-' + f_id).addClass('none');

					if ($('#c-' + f_id))
					{
						$('#c-' + f_id).removeClass('none');
					}
				}
			}

			$('.card-body-top').LoadingOverlay("hide", true);
		},
		error: function(e) {
			alert('Somethings went wrong, please try again')
			$('.card-body-top').LoadingOverlay("hide", true);
		}
	});
}
