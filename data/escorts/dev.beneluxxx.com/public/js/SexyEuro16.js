Cubix.SexyEuro16 = {
    // parameters
    escortPhotos: [],
    popup: null,
    closeBtn: null,
    overlay: null,
    overlay2: null,
    escortsInfoHdn: null,
    successCloseBtn: [],
    successOkBtn: [],
    mainImg: null,
    smallImgs: [],
    viewProfileBtn: null,
    voteBtn: null,
    nextBtn: null,
    previousBtn: null,
    escortShownameSpn: null,
    escortCitySpn: null,
    successPopup: null,
    successPopupSignin: null,
    successPopupSignup: null,
    votedEscortId: 0,

    // methods
    initialize: function() {
        this.setElements();
        this.eventManager();
    },

    setElements: function() {
        this.escortPhotos = $$('.escort-photo');
        this.popup = $('photoContestPopup');
        this.successPopup = $('successPopup');
        this.successPopupSignin = $('successPopupSignin');
        this.successPopupSignup = $('successPopupSignup');
        this.successCloseBtn = $$('.successCloseBtn');
        this.successOkBtn = $$('.successOkBtn');
        this.signUpInPopup = $('signUpIn');
        this.overlay = $$('.overlay')[0];
        this.overlay2 = $$('.overlay-2')[0];
        this.closeBtn = $('closeBtn');
        this.escortsInfoHdn = $('escortsInfo');
        this.smallImgs = this.popup.getElements('.left')[0].getElements('img');
        this.mainImg = this.popup.getElements('.main-img')[0].getElements('img')[0];
        this.loader = $$('.loader')[0];
        this.viewProfileBtn = $('viewProfileBtn');
        this.voteBtn = $('voteBtn');
        this.nextBtn = $('next');
        this.previousBtn = $('previous');
        this.escortShownameSpn = this.popup.getElements('.escort-showname')[0];
        this.escortCitySpn = this.popup.getElements('.escort-city')[0];
    },

    eventManager: function() {
        var self = this;

        // clicking the escort photo
        this.escortPhotos.addEvent('click', function() {
            var id = $(this).get('id');
            self.makePopup(id);
            
            self.modalManager(1,0,0);
        });

        // clicking the close buttons
        this.closeBtn.addEvent('click', function() {
            self.closeOverlay();
        });

        this.successCloseBtn[0].addEvent('click', function() {
            self.closeOverlay2();
        });

        this.successCloseBtn[2].addEvent('click', function() {
            self.closeOverlay2();
        });

        this.successCloseBtn[1].addEvent('click', function() {
            window.location.reload();
        });

        this.successOkBtn[0].addEvent('click', function() {
            self.closeOverlay2();
        });
        
        this.successOkBtn[2].addEvent('click', function() {
            self.closeOverlay2();
        });

        this.successOkBtn[1].addEvent('click', function() {
            window.location.reload();
        });

        // clicking the vote for her button
        this.voteBtn.addEvent('click', function() {
            if (headerVars.currentUser) {
                if (headerVars.currentUser.user_type == 'member') {
                    self.vote(this.get('data-id'));
                    self.updateVotes(this.get('data-id'));
                    self.modalManager(0,1,0);
                } else {
                    alert('Only members can participate in voting');
                }
            } else {
                self.votedEscortId = parseInt(this.get('data-id'));
                self.modalManager(0,0,1);
            }
        });

        // clicking next button
        this.nextBtn.addEvent('click', function() {
            var id = this.get('data-id');
            self.makePopup(id);
        });


        // clicking previouse button
        this.previousBtn.addEvent('click', function() {
            var id = this.get('data-id');
            self.makePopup(id);
        });

        // clicking thumbs
        this.smallImgs.addEvent('click', function() {
            var id = this.get('data-id');
            self.makePopup(id);
        });
    },

    updateVotes: function(id) {
        var jsonFromHdn = this.escortsInfoHdn.get('value');
        var objEscortsInfo = JSON.parse(jsonFromHdn);

        for (var i = objEscortsInfo.length - 1; i >= 0; i--) {
            if (objEscortsInfo[i].escort_id == id) {
                objEscortsInfo[i].votes++;
            }
        }

        this.escortsInfoHdn.set('value', JSON.stringify(objEscortsInfo));
    },

    openOverlay: function() {
        this.overlay.fade('in');

        // preventing the page from scrolling
        document.addEvent('mousewheel', function(e) {
            e.preventDefault();
        });
    },

    openOverlay2: function() {
        this.overlay2.fade('in');

        // preventing the page from scrolling
        document.addEvent('mousewheel', function(e) {
            e.preventDefault();
        });
    },

    closeOverlay: function() {
        this.overlay.fade('out');
        document.removeEvents();
    },

    closeOverlay2: function() {
        this.overlay2.fade('out');
        document.removeEvents();
    },

    openSuccessVote: function() {
        this.closeOverlay();
        this.openOverlay2();
    },

    getEscortsInfo: function() {
        var self = this;

        var req = new Request({
            method: 'get',
            url: '/sexy-euro-16/',
            data: {
                'ajax': 1
            },
            onRequest: function() {
                self.loader.removeClass('hidden');
            },
            onComplete: function(response) {
                self.escortsInfoHdn.set('value', response);
                self.loader.addClass('hidden');
            }
        }).send();
    },

    vote: function(id) {
        var self = this;

        var req = new Request({
            method: 'get',
            url: '/sexy-euro-16/vote-for-escort',
            data: {
                'ajax': 1,
                'escort_id': id
            },

            onRequest: function() {
                self.popup.getElements('.loader')[0].show();
            },

            onComplete: function(response) {
                self.popup.getElements('.loader')[0].hide();
            }
        }).send();
    },

    makePopup: function(id) {

        var jsonFromHdn = this.escortsInfoHdn.get('value');
        if (jsonFromHdn) {
            var objEscortsInfo = JSON.parse(jsonFromHdn);

            for (var i = 0; i < objEscortsInfo.length; i++) {

                if (objEscortsInfo[i].escort_id == id) {
                    this.mainImg.set('src', objEscortsInfo[i].photo_url_big);
                    this.viewProfileBtn.set('href', '/escort/' + objEscortsInfo[i].showname + '-' + objEscortsInfo[i].escort_id);
                    this.voteBtn.set('data-id', objEscortsInfo[i].escort_id);

                    if (i == objEscortsInfo.length - 1 && i == 0) {
                        this.nextBtn.hide();
                        this.previousBtn.hide();
                    } else if (i == objEscortsInfo.length - 1 && (i !== 0)) {
                        this.nextBtn.hide();
                        this.previousBtn.show();
                        this.previousBtn.set('data-id', objEscortsInfo[i - 1].escort_id);
                    } else if (i == 0 && (i !== objEscortsInfo.length - 1)) {
                        this.nextBtn.show();
                        this.nextBtn.set('data-id', objEscortsInfo[i + 1].escort_id);
                        this.previousBtn.hide();
                    } else {
                        this.nextBtn.show();
                        this.nextBtn.set('data-id', objEscortsInfo[i + 1].escort_id);
                        this.previousBtn.show();
                        this.previousBtn.set('data-id', objEscortsInfo[i - 1].escort_id);
                    }

                    this.escortShownameSpn.set('html', objEscortsInfo[i].showname + '&nbsp; (' + objEscortsInfo[i].votes + ')');
                    var cityNameLng = 'title_' + headerVars.lang;
                    var mapMark = '<i class="fa fa-map-marker" aria-hidden="true" style="color:#737373"></i>';
                    this.escortCitySpn.set('html', mapMark + '&nbsp; ' + objEscortsInfo[i][cityNameLng]);

                    if (this.smallImgs.length > objEscortsInfo.length) {

                        for (var j = 0; j < objEscortsInfo.length; j++) {
                            if ((i == objEscortsInfo.length - 1) && (j !== 0)) {
                                k = 0;
                                this.smallImgs[j].set('src', objEscortsInfo[k + j - 1].photo_url_small);
                                this.smallImgs[j].set('data-id', objEscortsInfo[k + j - 1].escort_id);
                            } else {
                                this.smallImgs[j].set('src', objEscortsInfo[i + j].photo_url_small);
                                this.smallImgs[j].set('data-id', objEscortsInfo[i + j].escort_id);
                            }
                        }
                    } else {
                        for (var j = 0; j < objEscortsInfo.length; j++) {
                            if ((i == objEscortsInfo.length - 1) && (j !== 0)) {
                                k = 0;
                                this.smallImgs[j].set('src', objEscortsInfo[k + j - 1].photo_url_small);
                                this.smallImgs[j].set('data-id', objEscortsInfo[k + j - 1].escort_id);
                            } else {
                                this.smallImgs[j].set('src', objEscortsInfo[i + j].photo_url_small);
                                this.smallImgs[j].set('data-id', objEscortsInfo[i + j].escort_id);
                            }
                        }
                    }

                    break;
                }

            };
        }
    },

    modalManager: function(voteModal, successModal, signUpInModal) {
        var self = this;

        // Vote Modal manager
        if (voteModal) {
            self.openOverlay();
        } else {
            self.closeOverlay();
        }

        // Success Modal manager


        if (successModal) {
            // Hiding all the successful messages
            [self.successPopup, self.successPopupSignin, self.successPopupSignup].each(function(el){
                el.hide();
            });

            self.openOverlay2();

            if (successModal == 1) self.successPopup.show();
            if (successModal == 2) self.successPopupSignin.show();
            if (successModal == 3) self.successPopupSignup.show();
        } else {
            if (signUpInModal) {
                [self.successPopup, self.successPopupSignin, self.successPopupSignup].each(function(el){
                    el.hide();
                });
            } else {
                self.closeOverlay2();
            }
        }

        // Sign up/Sign in Modal manager
        if (signUpInModal) {
            self.openOverlay2();
            self.signUpInPopup.show();
        } else {
            if (successModal) {
                self.signUpInPopup.hide();
            } else {
                self.closeOverlay2();
            }
        }
    }
};

Cubix.SexyEuro16.Modal = {

    indexModal: null,
    indexOverlay: null,
    modalCloseBtn: null,

    init: function(text) {
        this.setModal();
        this.eventManager();

        if (Cookie.read('18') == '1' && Cookie.read('sexy_euro_16_modal') !== '0') {
            this.showModal();
        } else {
            this.indexOverlay.destroy();
        }
    },

    setModal: function() {
        this.indexModal = $('sexyEuroModal');
        this.indexOverlay = $$('.sexyEuroModalOverlay')[0];
        this.modalCloseBtn = $('modalCloseBtn');
    },

    eventManager: function() {
        var self = this;

        this.modalCloseBtn.addEvent('click', function() {
            Cookie.write('sexy_euro_16_modal', '0');
            self.indexOverlay.fade('out');
            self.indexOverlay.destroy();
        })
    },

    showModal: function() {
        this.indexOverlay.fade('in');
    }
};

Cubix.SexyEuro16.PABanner = {
    banner: null,

    init: function() {
        this.getBanner();
        this.eventManager();
    },

    getBanner: function() {
        this.banner = $('sexyEuro16PABanner');
    },

    eventManager: function() {
        if (headerVars.currentUser.user_type == 'escort' || headerVars.currentUser.user_type == 'agency') {
            this.banner.addEvent('click', function() {
                Cubix.SexyEuro16.TermsModal.showModal();
            });
        } else if (headerVars.currentUser.user_type == 'member') {
            this.banner.set('href', '/sexy-euro-16')
        }
    }
};

Cubix.SexyEuro16.TermsModal = {

    termsModal: null,
    termsOverlay: null,
    termsModalCloseBtn: null,

    init: function(text) {
        this.setModal();
        this.eventManager();
    },

    setModal: function() {
        this.termsModal = $('sexyEuroTermsModal');
        this.termsOverlay = $$('.sexyEuroTermsOverlay')[0];
        this.termsModalCloseBtn = $('termsModalCloseBtn');
    },

    eventManager: function() {
        var self = this;

        this.termsModalCloseBtn.addEvent('click', function() {
            self.termsOverlay.fade('out');
        })
    },

    showModal: function() {
        this.termsOverlay.fade('in');
    }
};

Cubix.SexyEuro16.SignupPopup = {
    // properties
    signupCloseBtn: null,
    signupBtn: null,
    loginBtn: null,
    overlay2: null,

    // form fields
    username: null,
    password: null,
    rePassword: null,
    email: null,

    // methods
    initialize: function() {
        this.setSignupPopup();
        this.eventManager();
    },

    setSignupPopup: function() {
        this.signupCloseBtn = $('signupInCloseBtn');
        this.overlay2 = $$('.overlay-2')[0];
    },

    eventManager: function() {
        var self = this;

        this.signupCloseBtn.addEvent('click', function() {
            self.closeOverlay2();
        });
    },

    openOverlay2: function() {
        this.overlay2.fade('in');

        // preventing the page from scrolling
        document.addEvent('mousewheel', function(e) {
            e.preventDefault();
        });
    },

    closeOverlay2: function() {
        this.overlay2.fade('out');
        document.removeEvents();
    },
};

/*--------------------*/
/* SIGN UP / SIGN IN  */
/*--------------------*/

Cubix.Lang.signup = {
    username_invalid: '',
    username_invalid_characters: '',
    username_exists: '',
    password_invalid: '',
    password2_invalid: '',
    email_invalid: '',
    email_exists: '',
    terms_required: '',
    form_errors: '',
    domain_blacklisted: ''
};

Cubix.SexyEuro16.Validation = {
    // sign up properties
    messages: null,
    usernameInpt: null,
    passwordInpt: null,
    password2Inpt: null,
    emailInpt: null,
    captchInpt: null,
    captchaImg: null,
    termsChk: null,

    actionBtn: null,

    // sign in properties
    siUsername: null,
    siPassword: null,

    siActionBtn: null,

    // methods
    initialize: function() {
        this.setFields();
        this.eventManager();
    },

    getErrorElement: function(input) {
        var el = input.getPrevious('.err');
        if (!el) {
            el = new Element('span', {
                'class': 'err'
            }).inject(input, 'before');
        }

        return el;
    },

    setFields: function() {
        // signup elements
        this.usernameInpt = $('username');
        this.passwordInpt = $('password');
        this.password2Inpt = $('password2');
        this.emailInpt = $('email');
        this.captchInpt = $$('.captcha-text')[0];
        this.captchaImg = $('captcha');
        this.termsChk = $('terms');
        this.actionBtn = $('signupBtn');

        // signin elements
        this.siUsername = $('signInUsername');
        this.siPassword = $('signInPassword');
        this.siActionBtn = $('signinBtn');
    },

    setMessages: function(langMsgs) {
        this.messages = langMsgs;
    },

    eventManager: function() {
        var self = this,
            regex = /^[-_a-z0-9]+$/i;

        this.usernameInpt.addEvent('blur', function() {
            self.getErrorElement(this).destroy();

            // username must be at least 6 characters
            if (this.get('value').length < 6) {
                self.getErrorElement(this).set('html', self.messages.username_invalid);
                return;
            }

            // username must contain dash, underscore, numbers and letters only
            if (!regex.test(this.get('value'))) {
                self.getErrorElement(this).set('html', self.messages.username_invalid_characters);
                return;
            }

            // username must be unique
            var loadingEl = new Element('span', {
                class: 'loading'
            });

            loadingEl.inject(this, 'before');

            new Request({
                url: '/' + headerVars.lang + '/private/check',
                method: 'get',
                data: {
                    username: this.get('value'),
                },

                onSuccess: function(resp) {
                    var resp = JSON.decode(resp);

                    loadingEl.destroy();

                    if (resp.status == 'found') {
                        self.getErrorElement(this).set('html', self.messages.username_exists);
                    } else if (resp.status == 'blocked') {
                        self.getErrorElement(this).set('html', self.messages.username_has_blocked_word);
                    } else if (resp.status == 'not found') {}
                }.bind(this)
            }).send();
        });

        this.passwordInpt.addEvent('blur', function() {
            self.getErrorElement(this).destroy();

            if (this.get('value') == self.usernameInpt.get('value')) {
                self.getErrorElement(this).set('html', self.messages.username_equal_password)
                return;
            }

            if (this.get('value').length < 6) {
                self.getErrorElement(this).set('html', self.messages.password_invalid);
            } else {
                self.getErrorElement(this).destroy();
            }
        });

        this.password2Inpt.addEvent('blur', function() {
            self.getErrorElement(this).destroy();

            if (this.get('value') != self.passwordInpt.get('value')) {
                self.getErrorElement(this).set('html', self.messages.password2_invalid);
            } else {
                self.getErrorElement(this).destroy();
            }
        });

        this.captchInpt.addEvent('blur', function() {
            self.getErrorElement(self.captchaImg).destroy();
        });

        this.emailInpt.addEvent('blur', function() {
            self.getErrorElement(this).destroy();
            var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            if (!this.get('value').length || !regex.test(this.get('value'))) {
                self.getErrorElement(this).set('html', self.messages.email_invalid)
                return;
            }

            // the email has to be unique
            var loadingEl = new Element('span', {
                class: 'loading'
            });

            loadingEl.inject(this, 'before');

            new Request({
                url: '/' + headerVars.lang + '/private/check',
                method: 'get',
                data: {
                    email: this.get('value')
                },

                onSuccess: function(resp) {
                    var resp = JSON.decode(resp);
                    loadingEl.destroy();

                    if (resp.status == 'found') {
                        self.getErrorElement(this).set('html', self.messages.email_exists);
                    } else if (resp.status == 'not found') {} else if (resp.status == 'domain blacklisted') {
                        self.getErrorElement(this).set('html', self.messages.domain_blacklisted);
                    }
                }.bind(this)
            }).send();
        });

        this.termsChk.addEvent('click', function() {
            self.getErrorElement(this).destroy();

            if (self.termsChk.get('checked')) {
                self.termsChk.set('value', 1);
            } else {
                self.termsChk.set('value', 0);
            }
        });

        this.actionBtn.addEvent('click', function(e) {
            var inputEls = [self.usernameInpt, self.passwordInpt, self.password2Inpt, self.emailInpt];
            inputEls.each(function(el) {
                el.fireEvent('blur');
            });

            if ($$('.err').length > 0) {
                e.stop();
                alert(self.messages.form_errors);
                return false;
            } else {
                new Request({
                    url: '/private/signup-member?type=member',
                    method: 'post',
                    data: {
                        ajax: 1,
                        username: self.usernameInpt.get('value'),
                        password: self.passwordInpt.get('value'),
                        password2: self.password2Inpt.get('value'),
                        email: self.emailInpt.get('value'),
                        terms: self.termsChk.get('value'),
                        captcha: self.captchInpt.get('value'),
                        for_euro_vote: Cubix.SexyEuro16.votedEscortId
                    },

                    onSuccess: function(resp) {
                        
                        response = JSON.decode(resp);

                        if (response.status == 'success') {
                            Cubix.SexyEuro16.modalManager(0,3,0);
                        } else if (response.status == 'error') {
                            if (typeOf(response.msgs) !== 'array') {
                                Object.each(response.msgs, function(value, key) {
                                    var errField = key,
                                        errMsgs = value;
                                    self.getErrorElement($(errField)).set('html', errMsgs);
                                });
                            }
                        }
                    }.bind(this)
                }).send();

                e.stop();
            }
        });


        this.siUsername.addEvent('blur', function() {
            self.getErrorElement(self.siUsername).destroy();

            if (this.get('value').trim() == '') {
                self.getErrorElement(this).set('html', self.messages.username_invalid);

                return false;
            }
        })

        this.siPassword.addEvent('blur', function() {
            self.getErrorElement(self.siPassword).destroy();

            if (this.get('value').trim() == '') {
                self.getErrorElement(this).set('html', self.messages.username_invalid);

                return false;
            }
        })

        this.siActionBtn.addEvent('click', function(e) {
            var inputEls = [self.siUsername, self.siPassword];

            inputEls.each(function(el) {
                el.fireEvent('blur');
            });

            if ($$('.signin .err').length > 0) {
                e.stop();
                alert(self.messages.form_errors);
                return false;
            } else {
                new Request({
                    url: '/private/signin',
                    method: 'post',
                    data: {
                        ajax: 1,
                        login: 'Login',
                        username: self.siUsername.get('value'),
                        password: self.siPassword.get('value'),
                        for_euro_vote: Cubix.SexyEuro16.votedEscortId
                    },

                    onSuccess: function(resp) {
                        response = JSON.decode(resp);
                        if (response.status == 'success') {
                            Cubix.SexyEuro16.modalManager(0,2,0);
                        } else if (response.status == 'error') {
                            if (typeOf(response.msgs) !== 'array') {
                                console.log(response.msgs);
                                Object.each(response.msgs, function(value, key) {
                                    var errField = key,
                                        errMsgs = value;

                                    self.getErrorElement($('signInUsername')).set('html', errMsgs);
                                });
                            }
                        }
                    }.bind(this)
                }).send();
            }
        });
    }
};