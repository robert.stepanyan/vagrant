Cubix.FitGallery = new Class({
	Implements: Options,
	
	options: {
		itemSelector: '.photo',
		minMargin: null,
	    maxMargin: null,
	    resize: true,
	    lastRowClass: 'last-row',
	    firstItemClass: 'first-item',
	    parentClass: '.item'
	},
	
	element: null,	
	
	initialize: function (element, options) {
		this.element = $(element);
		
		this.setOptions(options || {});

		this.layout();
	},

	layout: function() {
		var rowWidth = 0,
	        rowElems = [],
	        items = this.element.getElements(this.options.itemSelector),
	        itemsSize = items.length;

	    var containerWidth = this.element.clientWidth - parseFloat( this.element.getStyle('padding-left')) - parseFloat(this.element.getStyle('padding-right') );
	    var itemAttrs = [];
	    var theImage, w, h;

	    for ( var i = 0; i < itemsSize; ++i ) {
	      theImage = items[i].getElement('img');
	      if ( ! theImage ) {
	        items.splice(i, 1);
	        --i;
	        --itemsSize;
	        continue;
	      }
	      // get width and height via attribute or js value
	      if (!(w = parseInt(theImage.get('width')))) {
	        theImage.set('width', w = theImage.offsetWidth);
	      } 
	      if (!(h = parseInt(theImage.get('height')))) {
	        theImage.set('height', h = theImage.offsetHeight);
	      } 
	      
	      itemAttrs[i] = {
	        width: w,
	        height: h
	      };
	    }
	    itemsSize = items.length;


	    // write
	    for(var index = 0; index < itemsSize; ++index) {

	      if (items[index].classList) {
	        items[index].classList.remove(this.options.firstItemClass);
	        items[index].classList.remove(this.options.lastRowClass);
	      } else {
	        // IE <10
	        items[index].className = items[index].className.replace(new RegExp('(^|\\b)' + this.options.firstItemClass + '|' + this.options.lastRowClass + '(\\b|$)', 'gi'), ' ');
	      }

	      rowWidth += itemAttrs[index].width;
	      rowElems.push(items[index]);
	      
	      // check if it is the last element
	      if(index === itemsSize - 1) {
	        for(var rowElemIndex = 0; rowElemIndex < rowElems.length; rowElemIndex++) {
	        	rowElems[rowElemIndex].getParent(this.options.parentClass).destroy();

	          // if first element in row
	          /*if(rowElemIndex === 0) {
	            rowElems[rowElemIndex].className += ' ' + this.options.lastRowClass;
	          }
	          rowElems[rowElemIndex].style.cssText =
	              'width: ' + itemAttrs[index+parseInt(rowElemIndex)-rowElems.length+1].width + 'px;' +
	              'height: ' + itemAttrs[index+parseInt(rowElemIndex)-rowElems.length+1].height + 'px;' +
	              'margin-right:' + ((rowElemIndex < rowElems.length - 1) ? this.options.minMargin+'px' : 0);*/
	        }
	      }      
	      
	      // check whether width of row is too high
	      if(rowWidth + this.options.maxMargin * (rowElems.length - 1) > containerWidth) {
	        var diff = rowWidth + this.options.maxMargin * (rowElems.length - 1) - containerWidth;
	        var nrOfElems = rowElems.length;
	        // change margin
	        var maxSave = (this.options.maxMargin - this.options.minMargin) * (nrOfElems - 1);
	        if(maxSave < diff) {
	          var rowMargin = this.options.minMargin;
	          diff -= (this.options.maxMargin - this.options.minMargin) * (nrOfElems - 1);
	        } else {
	          var rowMargin = this.options.maxMargin - diff / (nrOfElems - 1);
	          diff = 0;
	        }
	        var rowElem,
	          widthDiff = 0;
	        for(var rowElemIndex = 0; rowElemIndex<rowElems.length; rowElemIndex++) {
	          rowElem = rowElems[rowElemIndex];
	          var rowElemWidth = itemAttrs[index+parseInt(rowElemIndex)-rowElems.length+1].width;
	          var newWidth = rowElemWidth - (rowElemWidth / rowWidth) * diff;
	          var newHeight = Math.round(itemAttrs[index+parseInt(rowElemIndex)-rowElems.length+1].height * (newWidth / rowElemWidth));
	          if (widthDiff + 1 - newWidth % 1 >= 0.5 ) {
	            widthDiff -= newWidth % 1;
	            newWidth = Math.floor(newWidth);
	          } else {
	            widthDiff += 1 - newWidth % 1;
	            newWidth = Math.ceil(newWidth);
	          }
	          rowElem.style.cssText =
	              'width: ' + newWidth + 'px;' +
	              'height: ' + newHeight + 'px;';/* +
	              'margin-right: ' + ((rowElemIndex < rowElems.length - 1)?rowMargin : 0) + 'px';*/

	          rowElem.getParent(this.options.parentClass).setStyle('margin-right', ((rowElemIndex < rowElems.length - 1) ? rowMargin : 0) + 'px');

	          
	          if( rowElemIndex == rowElems.length - 1 ) {
	          	var last_photo = rowElem.getParent(this.options.parentClass);

	          	var clear = new Element('div', {'class':'clear'}).injectAfter(last_photo);	          	
	          	var vote_box = new Element('div', {'class':'vote-box-container'}).injectAfter(clear);
	          	var clear = new Element('div', {'class':'clear'}).injectAfter(vote_box);
	          }

	          if(rowElemIndex === 0) {
	            rowElem.className += ' ' + this.options.firstItemClass;
	          }
	        }
	        rowElems = [],
	          rowWidth = 0;
	      }
	    }

	    //console.log(itemsSize);
	},
});


Cubix.PrivateMessaging = {};
Cubix.PrivateMessaging.isLoggedIn = false;

/* --> LocationHash */
Cubix.LocationHash = {};

Cubix.LocationHash.Set = function (hash) {
	if ( hash.length ) {
		document.location.hash = '#' + hash;
	}
	else {
		document.location.hash = '';
	}
	
	return false;
}

Cubix.LocationHash.Parse = function () {
	var hash = document.location.hash.substring(1);
	
	if ( ! hash.length ) {
		return {};
	}
	
	var params = hash.split(';');
	
	var filter = '';
	
	var not_array_params = ['page', 'country', 'city', 'only_unrated', 'sort', 'date'];
	
	params.each(function (param) {
		var key_value = param.split('=');
		
		var key = key_value[0];
		var val = key_value[1];
		
		if ( val === undefined ) return;
		
		val = val.split(',');
				
		val.each(function(it) {
			filter += key + ( ( ! not_array_params.contains(key) ) ? '[]=' : '=' ) + it + '&';
		});
	});
	
	return filter;
}

Cubix.LocationHash.Make = function (params) {
	
	var hash = '';
	var sep = ';';
	var value_glue = ',';
	
	var places = $$('#pf-header-wrp');
	
	var map = {};
	places.each(function(place) {
		var inputs = place.getElements('input[type=text]');
		inputs.append(place.getElements('input[type=hidden]'));
		//inputs.append(place.getElements('input[type=text]'));
		inputs.each(function(input) {
			if ( input.get('value').length ) {
				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.get('value')]);
			}
		});
		
		var selects = place.getElements('select');
		
		selects.each(function(select) {			
			if ( select.getSelected().get('value') != 0 ) {
				var index = select.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([select.getSelected().get('value')]);
			}
		});

		var inputs_ch = place.getElements('input:checked');
		inputs_ch.each(function(input) {
			if ( input.get('value').length > 0 ) {
				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.get('value')]);
			}
		});
	});
	
	map = $merge(map, params);
	
	for (var i in map) {
		hash += i + '=' + map[i].join(value_glue) + sep;
	}
	
	return hash;
}
/* <-- */

/* --> HashController */
Cubix.HashController = {
	_current: '',
	
	init: function () {
		setInterval('Cubix.HashController.check()', 100);
	},
	
	check: function () {
		var hash = document.location.hash.substring(1);
		
		if (hash != this._current) {
			this._current = hash;
					
			var data = Cubix.LocationHash.Parse();
			
			var myScroll = new Fx.Scroll(window);
			myScroll.toElement($('pf-header-wrp'));
			
			Cubix.PhotoFeed.Load(data);
			Cubix.PhotoFeed.LoadHeader(data);
		}
	}
};
/* <-- */

Cubix.Filter = {};
Cubix.Filter.Set = function (filter) {
		
	Cubix.LocationHash.Set(Cubix.LocationHash.Make(filter));

	return false;
}

Cubix.PhotoFeed = {};

Cubix.PhotoFeed.container = 'pf-photos-wrp';
Cubix.PhotoFeed.header_container = 'pf-header-wrp';
Cubix.PhotoFeed.vote_container = 'pf-vote';

Cubix.PhotoFeed.voting_box = '';
Cubix.PhotoFeed.more_photos_url = '';
Cubix.PhotoFeed.list_url = '';
Cubix.PhotoFeed.vote_url = '';

Cubix.PhotoFeed.LoadHeader = function (data) {
	
	var url = Cubix.PhotoFeed.header_url;
	
	var overlay = new Cubix.Overlay($(Cubix.PhotoFeed.header_container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'POST',
		data: data,
		onSuccess: function (resp) {			
			$(Cubix.PhotoFeed.header_container).set('html', resp);
			overlay.enable();
			
			Cubix.HashController.InitAll();			
		}
	}).send();
	
	return false;
}

Cubix.PhotoFeed.Load = function (data) {
	
	var url = Cubix.PhotoFeed.list_url;
	
	var overlay = new Cubix.Overlay($(Cubix.PhotoFeed.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'POST',
		data: data + '&ajax=1',
		onSuccess: function (resp) {
			
			$(Cubix.PhotoFeed.container).set('html', resp);
			
			overlay.enable();

			new Cubix.FitGallery('pf-photos', {minMargin: 10, maxMargin: 10,});

			Cubix.PhotoFeed.initVotingBox();
		}
	}).send();
	
	return false;
}


Cubix.PhotoFeed.InitFilters = function() {
	var elements = container.getElements('select, input[type=checkbox]');	
	//var moo = new Mooniform(elements);
	
	$$('#pf-header-wrp select, #pf-header-wrp input[type=checkbox]').addEvent('change', function(e){
		e.stop();
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
};

Cubix.PhotoFeed.initVotingBox = function() {

	var voting_box_url = Cubix.PhotoFeed.voting_box;

	$$('.pf-photos .item .count').addEvent('mouseenter', function(){
		this.getNext('.p-wrp').removeClass('none');
		var p_right = -1 * (this.getCoordinates(this.getParent('.info')).width / 2 + 5);
		this.getNext('.p-wrp').setStyle('right', p_right);
	});
	$$('.pf-photos .item .count').addEvent('mouseleave', function(){
		this.getNext('.p-wrp').addClass('none');
	});

	$$('.pf-photos .item a').removeEvents('click');
	$$('.pf-photos .item a').addEvent('click', function(e){
		e.stop();

		var self = this;
		var escort_id = this.get('escortid');
		var photo_id = this.get('photoid');
		var voting_box_cont = self.getParent('div.item').getNext('div.vote-box-container');

		if ( voting_box_cont.getElements('.voting-box').length ) {
			if ( voting_box_cont.getElements('.voting-box')[0].get('photoid') == photo_id ) {
				return false;
			}
		}

		$$('div.vote-box-container').set('html', '');

		var url = voting_box_url + escort_id + '/' + photo_id;

		Cubix.PhotoFeed.loadVotingBox(url, voting_box_cont, this);
	});
};

Cubix.PhotoFeed.loadVotingBox = function(url, voting_box_cont, thumb) {

	var f_overlay = new Cubix.Overlay(voting_box_cont, {color:'#191c1e', opacity : '.4', has_loader: false});
	f_overlay.disable();

	if ( thumb ) {
		var t_overlay = new Cubix.Overlay(thumb, {color:'#fff', opacity : '.4', has_loader: true, loader: _st('loader-small.gif'), position : ''});
		t_overlay.disable();
	}

	new Request({
		url: url,
		method: 'GET',
		data: {},
		onSuccess: function (resp) {
			
			voting_box_cont.set('html', resp);

			// set pntik to center
			if ( thumb ) {
				t_overlay.enable();

				voting_box_cont.getElements('.voting-box')[0].set('photoid', thumb.get('photoid'));

				voting_box_cont.getElements('.pnt').setStyle('left', thumb.getCoordinates(thumb.getParent('.pf-photos')).left + (thumb.getCoordinates().width / 2) );

				var myFx = new Fx.Scroll(window,{
					offset: {
						'x': 0,
						'y': -5
					}
				});
				myFx.toElement(thumb);
			} else {
				voting_box_cont.getElements('.pnt').destroy();
			}

			voting_box_cont.getElements('.close-x').addEvent('click', function(e){
				e.stop();
				
				voting_box_cont.set('html', '');
			});

			//init more photos
			Cubix.PhotoFeed.initMorePhotos(voting_box_cont);

			Cubix.PhotoFeed.initVotingBox();

			Cubix.PhotoFeed.initRateIcons();
			
		}
	}).send();
};

Cubix.PhotoFeed.initRateIcons = function() {
	if ( $$('.vote-stars a').length ) {
		$$('.vote-stars a').addEvent('mouseenter', function() {
			this.getParent('li').getAllPrevious('li').each(function(li){
				li.getElements('a').addClass('hover');	
			});

			this.getParent('li').getAllNext('li').each(function(li){
				li.getElements('a').addClass('hover-white');	
			});
			this.addClass('hover');
		});

		$$('.vote-stars a').addEvent('mouseleave', function() {
			$$('.vote-stars a').removeClass('hover').removeClass('hover-white');
		});

		$$('.vote-stars a').addEvent('click', function(e) {
			e.stop();

			var self = this;

			var f_overlay = new Cubix.Overlay($$('.' + Cubix.PhotoFeed.vote_container)[0], {color:'#191c1e', opacity : '.4', has_loader: false});
			f_overlay.disable();

			new Request({
				url: Cubix.PhotoFeed.vote_url + self.get('escortid') + '/' + self.get('photoid') + '/' + self.get('rate'),
				method: 'GET',
				data: {},
				onSuccess: function (resp) {							
					$$('.' + Cubix.PhotoFeed.vote_container)[0].set('html', resp);
				}
			}).send();
		});
	}	
};

Cubix.PhotoFeed.initMorePhotos = function(voting_box_cont) {
	voting_box_cont.getElements('.more-photos-ul-wrp a.prev, .more-photos-ul-wrp a.next').addEvent('click', function(e){
		e.stop();

		var f_overlay = new Cubix.Overlay(voting_box_cont.getElements('.more-photos-ul-wrp')[0], {color:'#191c1e', opacity : '.4', has_loader: false});
		f_overlay.disable();

		new Request({
			url: this.get('href'),
			method: 'GET',
			data: {},
			onSuccess: function (resp) {							
				voting_box_cont.getElements('.more-photos-ul-wrp')[0].set('html', resp);

				Cubix.PhotoFeed.initMorePhotos(voting_box_cont);
			}
		}).send();
	})

	voting_box_cont.getElements('.more-photos-ul-wrp a.photo').addEvent('click', function(e){
		e.stop();

		Cubix.PhotoFeed.loadVotingBox(this.get('href'), voting_box_cont);
	})
};

Cubix.HashController.InitAll = function(){
	Cubix.HashController.init();
	
	Cubix.PhotoFeed.InitFilters();
	Cubix.PhotoFeed.initVotingBox();
};

window.addEvent('domready', function () {
	Cubix.HashController.InitAll();

	new Cubix.FitGallery('pf-photos', {minMargin: 10, maxMargin: 10,});
});
