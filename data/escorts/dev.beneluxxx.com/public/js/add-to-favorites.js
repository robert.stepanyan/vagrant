Cubix.Favorites = {};

Cubix.Favorites.lng = ''; // Must be set from php

Cubix.Favorites.Init = function () {

	var fav_button = $('fav_button');

	var overlay = new Cubix.Overlay($('fav_wrapper'), {
		loader: _st('loader-circular-tiny.gif'),
		position: '50%'
	});

	if ( $defined(fav_button) ) {
		fav_button.removeEvents('click').addEvent('click', function(e) {
			e.stop();

			var f_url = fav_button.getParent().get('href');
			var f_new_url = fav_button.getParent().get('rel');

			var img_path = '';
			var actions = fav_button.get('class').split(' ');
			var action = actions[1];
			
			if ( action == 'remove' )
				img_path = _st('v2.1/add-favorite-' + Cubix.Favorites.lng + '.jpg');
			else
				img_path = _st('v2.1/remove-favorite-' + Cubix.Favorites.lng + '.jpg');

			overlay.disable();
		
			Cubix.Favorites.ToggleFavorites(f_url, f_new_url, img_path, action, overlay, Cubix.Favorites.lng);
		});
	}
	else {
		$('fav').removeEvents('click').addEvent('click', function(e) {
			e.stop();
			Cubix.Popup.Show('500', '520');
		});
	}

	return false;
}

Cubix.Favorites.ToggleFavorites = function (url, new_url, img_path, action, overlay, lng) {

	var fav_wrapper = $('fav_wrapper');
	var fav_button = $('fav_button');

	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {
			var resp = JSON.decode(resp, true);

			var status_el = fav_wrapper.getFirst('span');
			if ( resp.msg.length ) {
				if ( status_el ) status_el.set('html', resp.msg);
				/*else status_el = new Element('span', {html: resp.msg, styles: {marginBottom: '7px', fontSize: '14px', color: '#080', display: 'block'}}).inject(fav_wrapper, 'top');*/
			}
			else {
				if ( status_el ) status_el.destroy();
			}

			overlay.enable();

			fav_button.getParent().set('href', new_url);
			fav_button.getParent().set('rel', url);

			if( action == 'remove' ) {
				fav_button.set('class', 'btn-add-fav-' + lng + ' add');
			}
			else {
				fav_button.set('class', 'btn-remove-fav-' + lng + ' remove');
			}

			fav_button.set('src', img_path);
		}
	}).send();
	

	return false;
}


window.addEvent('domready', function () {
	Cubix.Favorites.Init();
});