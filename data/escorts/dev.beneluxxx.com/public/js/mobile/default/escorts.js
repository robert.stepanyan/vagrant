MCubix.Escorts = {
    escort_id: null,
    page_ex: false,

    init: function( url, pageCount ){
        this.pageLoad( url, pageCount );
    },

    /*
    * Page Data load and initialisation in content
    */
    pageLoad: function( url, pageCount ){

//        var scrollTo = _.throttle()

        var elm = $(document), hash;
        var self = this;

        var lastScroll = 0;

        var processScroll = true;

        elm.off( 'scroll' );
        elm.on( 'scroll', function() {

            var st = $(this).scrollTop();

            (function(DOMParser) {
                "use strict";
                var DOMParser_proto = DOMParser.prototype
                    , real_parseFromString = DOMParser_proto.parseFromString;

                // Firefox/Opera/IE throw errors on unsupported types
                try {
                    // WebKit returns null on unsupported types
                    if ((new DOMParser).parseFromString("", "text/html")) {
                        // text/html parsing is natively supported
                        return;
                    }
                } catch (ex) {}

                DOMParser_proto.parseFromString = function(markup, type) {
                    if (/^\s*text\/html\s*(?:;|$)/i.test(type)) {
                        var doc = document.implementation.createHTMLDocument(""),
                            doc_elt = doc.documentElement,
                            first_elt;



                        doc_elt.innerHTML = markup;
                        first_elt = doc_elt.firstElementChild;

                        if (doc_elt.childElementCount === 1
                            && first_elt.localName.toLowerCase() === "html") {
                            doc.replaceChild(first_elt, doc_elt);
                        }

                        return doc;
                    } else {
                        return real_parseFromString.apply(this, arguments);
                    }
                };

            }(DOMParser));
            //console.log(st);

            //if( $(this).hasClass('ui-page-active') ){
                //console.log( st + ' / ' + lastScroll );
                var loadData = $('.mmm');
//
            var pageElm = $('#current_page');
            var maxPage = Number( pageCount );


//            console.log( $(window).scrollTop() + ' - ' + $(window).height() + ' - ' + st + ' --= ' + $(document).height() );

//                if( ( $(this).find('div.ui-footer').offset().top < ( $(document).height() ) ) && ( st > lastScroll ) ) {
                if( ( ( $(window).scrollTop() + $(window).height() ) > ( $(document).height() - 400 ) ) && ( st > lastScroll ) ) {
                    //alert('555');
//                    loadData.stop( true, true ).slideDown(300);


                    //var page = Number( pageElm.val() );

                    //console.log(pageElm.val() + '/' + maxPage);

                    if( pageElm.val() >= maxPage ){
                        elm.off( 'scroll' );
                        return false;
                    }

                    //console.log(maxPage + '/' + pageElm.val());

                    if( Number( pageElm.val() ) < Number( maxPage + 1 ) /*&& ( Number( pageElm.val() ) != 1 )*/ && ( Number( pageElm.val() ) < maxPage + 1 ) && ( processScroll ) ){

                        processScroll = false;

                        var contentCont = $('.load-page-content').find('.page-load-block');
                        var data = MCubix.Functions.List.hashParse();

                        var c_page = MCubix.Cookies.get('page');
                        if( Number( c_page ) < Number( data.page ) ){
                            MCubix.Cookies.set( 'page', data.page, 1, document.location.hostname, document.location.pathname );
                        }

                        //console.log( url );

                        if( url.indexOf('?sort=newest') === -1 ) {
                            if( ( url.indexOf('&page=') === -1 ) && ( url.indexOf('&search=') === -1 ) ) {
                                url = url.split('/');
                                url[ url.length - 1 ] = Number( pageElm.val() ) + 1;
                                url = url.join('/');
                                url = url.replace( '//', '/' );
                            }else if( ( url.indexOf('&page=') === -1 ) && ( url.indexOf('&search=') != -1 ) ){
                                url = url + '&page=' + ( Number( pageElm.val() ) + 1 );
                            } else {
                                url = url.split('&page=');
                                url = url[ 0 ] + '&page=' + ( Number( pageElm.val() ) + 1 );
                                url = url.replace( '//', '/' );
                            }
                        } else {
                            if( !self.page_ex ){
                                url = url.split('?sort=newest');
                                url = url[ 0 ] + '/' + ( Number( pageElm.val() ) + 1 ) + '?sort=newest';
                                url = url.replace( '//', '/' );
                            } else {
                                url = url.split('?sort=newest');
                                var url_sl = url[ 0 ].split('/');
                                url_sl[ url_sl.length - 1 ] = Number( pageElm.val() ) + 1;
                                url = url_sl.join('/') + '?sort=newest';
                                url = url.replace( '//', '/' );
                            }
                            self.page_ex = true;
                        }

                        $.post( url, data, function() {
                            //alert(555);

                            if( pageElm.val() ){
                                //alert(555);

//                                if( url.indexOf('#') === -1 ) {
//                                    window.location.replace = "#page=" + pageElm.val() + ";segmented=1;";
//                                } else {
//                                    window.location.hash = "#page=" + pageElm.val() + ";segmented=1;";
//                                }


                            }

                            MCubix.Functions.List.disable_scroll( true );

                        }, "json" )
                            .always( function( html ) {

                                var parser = new DOMParser();
                                var contentParse = parser.parseFromString( html.responseText, "text/html" );

                                var pageContent = contentParse.getElementsByClassName('page-load-block');
//
                                var content = pageContent[0].innerHTML;
//
//                            //if( pageContent[0].innerHTML.length > 100 )
//                            //alert(pageContent[0].innerHTML);
                                if( content.length > 0 ){
                                    contentCont.html( contentCont.html() + content );


                                    if( ( Number( pageElm.val() ) < maxPage + 1 ) ){
                                        pageElm.val( Number( pageElm.val() ) + 1 );
                                    }
                                }

                                //elm.off( 'scroll' );
                            })
                            .complete( function(){
                                MCubix.Cookies.set( 'page', pageElm.val(), 1, document.location.hostname, document.location.pathname );

                                MCubix.Functions.List.disable_scroll( false );

                                loadData.html('Complete...');
                                if ( contentCont.hasClass('ui-listview') ){
                                    contentCont.listview('refresh');
                                } else {
                                    contentCont.trigger('create');
                                }

                                loadData.slideUp(300, function(){
                                    loadData.html('Loading Data...');
                                });
                                //self.pageLoad();

                                processScroll = true;

                                hash = "page=" + pageElm.val() + ";segmented=0;";

                                if( !window.location.hash ){
//                                    window.location.replace ( hash );
                                    window.location = ('' + window.location).split('#')[0] + '#' + hash;
                                } else {
                                    window.location.hash = hash;
                                }

                            });

                    } else {

//                        pageElm.val( Number( pageElm.val() ) - 1 );
//                        elm.off( 'scroll' );

                        loadData.delay(800).slideUp({
                            duration: 1000,
                            start: function(){ loadData.html('No results found !'); },
                            complete: function(){ loadData.html('Loading Data...'); }
                        });
                    }
                }
            //}

            lastScroll = st;
        });
    },

    openContact: function( escort_id, contact_type ){
        var self = this;
            self.validateForm( escort_id, contact_type );
    },

    validateForm: function( escort_id, contact_type ){
//        var url = $().attr('data-action');
        var self = this;
        self.escort_id = escort_id;

        self.refreshCaptcha();

        $.validator.addMethod(
            "serverControl",
            function(value, element) {
                var result = true;
                if ( value.length < 5 || value.length > 5 ) {
                    $.validator.messages.serverControl = "Required: Captcha code is 5 characters";
                    return false;
                } else {
                    var myCaptcha = $.ajax({ type: "POST", url: "/escorts/get-captcha-status", data: { hash: value }, async: false }).responseText;
                    if (myCaptcha == "false") {
                        $.validator.messages.serverControl = "Captcha code is invalid, please try the new code.";
                        $("span.refresh-captcha").click();
                        return false;
                    }
                }
                return result;
            },
            "captcha invalid"
        );

        var validator = $('form#contact-form-' + escort_id).removeAttr('novalidate').validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                name: {
                    required: true
                },
                message: {
                    required: true
                },
                captcha: {
                    required: true,
                    serverControl: true
                }
            },
            errorPlacement: function ( error, element ) {
                if ( $( element ).is('#captcha') ) {
                    element.closest('div.form-input').find('.error-after').after( error ); // special placement for select elements
                } else {
                    error.insertAfter( element );  // default placement for everything else
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        var attrVal = "contact-submit-" + escort_id;

        $('input[id=' + attrVal + ']').on( "click", function(){

            var form = $(this).closest('form');
            var url = form.attr('data-action');
            var type = $(this).siblings('contact-submit-input[name="contact-type"]').val();
            var form_overlay = $( 'div.form-overlay-' + self.escort_id);

            if( form.valid() ){
                form.ajaxSubmit({
                    type: "POST",
                    url: url,
                    beforeSubmit: function( formData, jqForm, options ) {
                        form_overlay.show(300);
                        form.css('pointerEvents', 'none').fadeTo( "slow" , 0.5, function() {
                            // Animation complete.
                        });
                    },
                    success: function( result ){
                        var req = $.parseJSON( result );
                        var form = $('form.contact-form');
                        var msgArea = form.find( 'p.form-message-' + self.escort_id );

                        form_overlay.hide(300);
                        form.css('pointerEvents', 'auto').fadeTo( "slow" , 1, function() {
                            // Animation complete.
                        });
//                        $( 'img.form-loading-' + self.escort_id).removeClass('show').addClass('hide');

                        if( req.status == 'error' ){
                            if( req.msgs ){
                                for ( var j in req.msgs ){
                                    msgArea.removeClass('success').addClass('error').text( req.msgs[j] );
                                    msgArea.show('500').delay(4000).hide('1000');
//                                    validator.showErrors({
//                                        j : req.msgs[j]
//                                    });
                                }
                            } else {
                                msgArea.removeClass('success').addClass('error').text('Server error, please try later !');
                                msgArea.show('500').delay(4000).hide('1000');
                            }
                        } else if( req.status == 'success' ){
                            msgArea.removeClass('error').addClass('success').text('Message Sent Successfully !');
                            msgArea.show('500').delay(4000).hide('1000');

                            form.resetForm();
                        }
                    }
                });
            } else {
                form.submit();
            }
        });

        $('span[class^=close-contact-form-]').on('click', function(){
            $( this).closest('span[class^=escort-contact-form-]').add('.contact-form-overlay').fadeOut(300);
        });
    },

    refreshCaptcha: function(){
        $('span.refresh-captcha').on('click', function(){
            var d = new Date();
            $('img#mail-captcha').attr("src", "/captcha?" + d.getTime() + '&no_tidy' );
        });

    },

    scrollTabs: function(){
        this.dynamicHeight();

        var elm = $('ul#escort-profile-tabs li').not('li[data-show=to-escort-photos]');

        elm.bind("click", function( e ){
            e.preventDefault();
            e.stopPropagation();

            $('div#escort-popup-tabs').css('display', 'block');
            $('div#popup-escort-image-block').css('display', 'block');
            $('div#popup-image-wrapper').css('display', 'none');

            var scroller = $(this).closest('div#popup-wrapper').find('.scrolling-area');
            var scrollBlock = $(this).attr('data-show');
            var _elm = $(this).closest('div#popup-wrapper').find('div.show').find( '[data-id=' + scrollBlock + ']' );

            var scrl = $(this).closest('#container');
            //console.log(scrl.attr('class'));

            var parent = $(this).closest('div#popup-wrapper');

            parent.find('ul#escort-profile-tabs li').removeClass('active');
            $(this).addClass('active');

            var offset = _elm.position();


//            console.log( offset.top );
            var _toScroll = offset.top;

//            $(this).closest('.ui-page-active').find('#popup-wrapper').stop().animate({
            $(this).closest('.ui-page-active').find('#page').stop().animate({
                scrollTop: _toScroll - 155
            }, 300);

            return false;

        });

        $('li[data-show=to-escort-photos]').click(function(){
            $('ul#escort-profile-tabs li').removeClass('active');
            $('li[data-show=to-escort-photos]').addClass('active');

            $(this).closest('.ui-page-active').find('#page').stop().animate({
                scrollTop: 0
            }, 300);

            $('div#escort-popup-tabs').css('display', 'none');
            $('div#popup-escort-image-block').css('display', 'none');
            $('div#popup-image-wrapper').css('display', 'block');
        });
    },

    dynamicHeight: function(){
        var ht = $(window).height();
        $('#page_wrapper').css('height', ht);

        $(window).on("resize", function(){
            var h = $(window).height();
            $('#page_wrapper').css('height', h);
        });
    }
};