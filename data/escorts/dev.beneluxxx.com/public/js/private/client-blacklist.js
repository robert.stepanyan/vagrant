/**
 * Created by SceonDev on 17.10.2017.
 */
var ClientBlacklist = {
    init: function () {
        this.initDatepicker();
        this.initSend();
        this.initDatatable(  );
    },

    initDatepicker: function () {
        $('.date-picker').datepicker({
            format: 'dd M yyyy',
            autoclose: true
        });
    },

    initSend: function () {
        $('#add_blacklist_client').submit(function (e) {
            e.preventDefault();

            var data = $(this).serializeArray();
            var black_list_modal = $("#black_list_modal");


            $.ajax({
                url: '/private-v3/add-client-to-blacklist',
                type: 'POST',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    black_list_modal.find('.modal-body').LoadingOverlay("show", true);
                },
                success: function (data) {
                    $('.is-invalid').removeClass('is-invalid').next().html(' ');
                    if (data.status == 'error') {
                        $.each(data.msgs, function (key, value) {
                            $('p[data-error="' + key + '"]').html(value).prev().addClass('is-invalid');
                        });
                    } else {

                        black_list_modal.modal('hide');
                        Notify.alert('success','Your entry is under review.');

                    }

                    black_list_modal.find('.modal-body').LoadingOverlay("hide", true);
                }
            });

        });
    },
    initDatatable : function () {
        $('#blacklist-table').DataTable({
            responsive: true,
            "bInfo": false,
            'bLengthChange': false

        });

        $('body').on('click','button[data-toggle="popover"]',function () {
            var text = $(this).attr('data-content');
            var author = $(this).attr('data-author');

            var comment_modal = $('#comment_modal');
            comment_modal.find('.modal-title').html(author+' comment !')
            comment_modal.find('.modal-body').html(text);
            comment_modal.modal('show');
        });
    }
};

$(document).ready(function () {
    ClientBlacklist.init();
});