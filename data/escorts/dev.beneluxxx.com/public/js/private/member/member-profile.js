/**
 * Created by Zhora on 24.10.2017.
 */

var MemberProfile = {

    init: function () {
        this.initForm();
        this.initCountries();
        this.initSummernote();
    },

    initForm: function () {
        $('#member-profile-form').submit(function (event) {
            event.preventDefault();

            var action = $(document.activeElement).attr('data-action');

            var self = $(this);

            //CHECKING | user want save or reset changes
            // var data = (action === 'save') ? $(this).serializeArray() : '';
            var data = $(this).serializeArray();

            $.ajax({
                url: '/private-v3/member-profile',
                type: 'POST',
                data: data,
                beforeSend: function () {
                    self.closest('.card-body').LoadingOverlay('show', true);
                },
                success: function (resp) {
                    try {
                        resp = JSON.parse(resp);

                        $.each(resp['msgs'], function (key, value) {
                            $('#' + key).html(value);
                        });


                    } catch (err) {

                        $('#ui-view').html(resp);
                        MemberProfile.init();

                        //IF user reseted changes, we don't need show notify alert
                        if (action === 'save') {
                            Notify.alert('success', "Your change has been saved!");
                        }
                    }
                    self.closest('.card-body').LoadingOverlay('show', true);
                }
            });


        });
    },

    initCountries: function () {
        $('select#country,select#city').select2({
            theme: 'bootstrap'
        });

        $("#country").change(function () {
            var self = $(this);
            var country_id = $(this).val();

            $.ajax({
                url: '/geography/ajax-get-cities',
                type: 'POST',
                data: {country_id: country_id},
                dataType: 'json',
                beforeSend: function () {
                    self.closest('.card-body').LoadingOverlay("show", true);
                },
                success: function (data) {

                    var city_area = $('#city');

                    city_area.find('option').remove();

                    $.each(data['data'], function (key, city) {
                        $('<option/>', {value: city.id, html: city.title}).appendTo(city_area);
                    });

                    self.closest('.card-body').LoadingOverlay("hide", true);
                }
            });
        });
    },

    initSummernote: function () {

        document.emojiSource = '/js/private/emoji/';
        $('#member-profile-form textarea').summernote({
            height: 100,
            disableDragAndDrop: true,
            disableResizeEditor: true,
            shortcuts: false,
            toolbar: [
                ['misc', ['emoji', 'undo', 'redo']],
                ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
            ],
            popover: {}
        });
    },

};

$(document).ready(function () {
    MemberProfile.init();
});
