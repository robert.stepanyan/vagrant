/**
 * Created by SceonDev on 20.10.2017.
 */
var Support = {
    attachmentInProgress : false,

    init : function () {
        this.initSummernote();
        this.loadTicket();
        this.initForm();
    },


    initSummernote: function (area) {

        var elem = area || $('#ui-view');
        elem.find('textarea').summernote({
            height: 100,
            disableDragAndDrop: true,
            disableResizeEditor: true,
            shortcuts: false,
            toolbar: [
                ['misc', ['undo', 'redo']],
                ['style', ['bold', 'italic', 'underline',  'strikethrough','clear']],
            ],
            popover: { }
        });
    },

    loadTicket: function (ticket_id) {
        var _self = this;
        $('.ticket_id').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('data-value');

            $.ajax({
                url: '/private-v3/support/ticket',
                type: 'GET',
                data: {id: id},
                beforeSend:function () {
                  $('.tickets-block').LoadingOverlay('show',true);
                },
                success: function (resp) {
                    $('#add_ticket_form').hide();
                    $('#preview_ticket').html(resp);
                    Support.initSummernote($('#preview_ticket'));
                    Support.initReply();
                    $('.tickets-block').LoadingOverlay('hide',true);
                    _self.initNewTicket();
                    _self.initAttachment();
                    window.lightbox.init();
                    if(CheckDevice.isMobile()){
                        $('#support-first-section').removeClass('mobile-hide');
                        $('#support-second-section').addClass('mobile-hide');
                    }
                    _self.initAttachment($('#reply-form'));
                    var chat = document.querySelector('.msg_container_base');
                    chat.scrollTop = chat.scrollHeight;
                }
            });


        });
    },

    initForm: function () {
        var _self = this;
        $('#add_ticket_form').submit(function (event) {
            var __self = $(this);

            event.preventDefault();
            var data = $(this).serializeArray();

            $.ajax({
                url: '/private-v3/support/ajax-add-ticket',
                type: 'POST',
                data: data,

                beforeSend: function () {
                    __self.closest('.card-body').LoadingOverlay("show", true);
                },

                success: function (resp) {
                    try {
                        resp = JSON.parse(resp);
                        $.each(resp['msgs'], function (key, value) {
                            $('#' + key).addClass('is-invalid');
                            $('.' + key).addClass('text-danger');
                        });

                    } catch (e) {
                      $('#ui-view').html(resp);
                        _self.init();

                        if(CheckDevice.isMobile()){
                            $('#support-first-section').addClass('mobile-hide');
                            $('#support-second-section').removeClass('mobile-hide');
                        }
                    }
                    __self.closest('.card-body').LoadingOverlay("hide", true);
                }
            });
        });


        _self.initAttachment($('#add_ticket_form'));

        $("body").on("click","#close_ticket_notification",function () {
            setUpUrl('support/index');
        });

        // $('body').on('change','.custom-file-input',function(){
        //     if($(this).attr('data-target') === '#attachment_ticket'){
        //         $('.form-control-file').html($(this).val());
        //     }
        // });
        _self.initNewTicket();
    },

    initReply : function () {
        var _self = this;
        $('#reply-form').submit(function (event) {
            event.preventDefault();

            var data = $(this).serializeArray();
            var message = $('input[name="message"]');
            var attachment = $('input[name="attach"]');

            if (!message.val() && !attachment.val()) {
                message.focus();
                return false;
            }else{
                message.val('');
            }


            $.ajax({
                url:'/private-v3/support/ajax-add-comment',
                type:'POST',
                data:data,
                beforeSend:function () {
                    $('#btn-chat').attr('disabled','disabled');
                },
                success:function (resp) {
                    try {

                        resp = JSON.parse(resp);
                        $('#message').html(resp['msgs'].message);

                    } catch (e) {
                        $('#message').html(" ");
                        $('#comments_wrapper').html(resp);

                        var chat = document.querySelector('.msg_container_base');
                        chat.scrollTop = chat.scrollHeight;

                    }

                    $('#btn-chat').removeAttr('disabled');
                    _self.initNewTicket();
                    _self.initAttachment($('#reply-form'));

                }
            });

        });
        
       _self.initBackButton();
    },
    initBackButton:function () {
        $('.back_to_tickets_list').click(function () {
            if(CheckDevice.isMobile()){
                $('#support-first-section').addClass('mobile-hide');
                $('#support-second-section').removeClass('mobile-hide');
            }
        });
    },
    initNewTicket:function () {
        var _self = this;
        $('.new_ticket').click(function () {
            $('#preview_ticket').html(' ');
            $('#add_ticket_form').fadeIn();
            if(CheckDevice.isMobile()){
                $('#support-first-section').removeClass('mobile-hide');
                $('#support-second-section').addClass('mobile-hide');
            }
        });

        $('#attach_file').click(function () {
            $(this).closest('form').find('.attachment').click();
        });
    },
    initAttachment:function (form) {
        if (typeof form === "undefined") return false;
        form.find('.attachment').change(function(el){
            var __self = $(this);
            var url = "/private-v3/support/add-attached-files";

            $(this).simpleUpload(url, {

                allowedExts: ["jpg", "jpeg","png"],
                allowedTypes: ["image/jpeg", "image/png", "image/x-png","image/jpg"],
                limit: 50,
                maxFileSize: 5000000, //5MB in bytes

                start: function(file){
                    $('#attachment_error').html(' ');
                    this.block = $('<div class="block-attachment"></div>');
                    this.progressBar = $('<div class="progressBarAttachment"></div>');
                    this.block.append(this.progressBar);
                    $('#attached-image').prepend(this.block);
                    $('#submit_support_form,#btn-chat').attr('disabled',true);
                },

                progress: function(progress){
                    this.progressBar.width(progress + "%");
                },

                success: function (response) {
                    response = JSON.parse(response);
                    this.progressBar.remove();
                    this.block.remove();
                    if(response.success){
                        var target = $(__self.attr('data-target'));
                        target.val((target.val()) ? target.val()+ ','+response.attached.id : response.attached.id);
                        if(form.attr('id') === 'add_ticket_form'){
                            $('#attached-images').append('<div><i class="fa fa-times-circle fa-lg close-icon" data-id="'+ response.attached.id +'" aria-hidden="true"></i><a href="' + response.attached.file_url + '" data-lightbox="images" data-title="image">' +
                                '<img src="'+ response.attached.file_url +'" class="img-thumbnail attached-image-style"/></a></div>');

                            $("html, body").animate({ scrollTop: $(document).height() }, 1000);
                        }
                    }else{
                        __self.closest('form').find('.attachment_error').html(response.error);
                    }

                    $('#submit_support_form,#btn-chat').removeAttr('disabled');


                },

                error: function(error){
                    $('.attachment_error').html(error.message);
                }
            });

        });

        if(form.attr('id') === 'add_ticket_form'){
            $('body').on('click','.close-icon', function () {

                var attachmentTicket = $('#attachment_ticket');
                var images = attachmentTicket.val();
                var imagesArray = images.split(',');
                var imageIndex = imagesArray.indexOf($(this).attr('data-id'));

                imagesArray.splice(imageIndex,1);
                images = imagesArray.join(',');
                attachmentTicket.val(images);

                $(this).parent('div').remove();

            });
        }
    }


};


$(document).ready(function () {
    Support.init();
});