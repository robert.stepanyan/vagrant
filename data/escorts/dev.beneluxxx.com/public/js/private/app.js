// Active ajax page loader
$.ajaxLoad = true;

$.defaultPage = '#index';
$.subPagesDirectory = 'private-v3/';
$.page404 = 'profile/index';
$.mainContent = $('#ui-view');
$.isRunning = false;

//Main navigation
$.navigation = $('nav > ul.nav.desktop-version');

$.panelIconOpened = 'icon-arrow-up';
$.panelIconClosed = 'icon-arrow-down';

Number.prototype.isNumeric = function (n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};
'use strict';

var CheckDevice = {
    isMobile:function () {
        return ($(window).width() < 567);
    }
};

if ($.ajaxLoad) {
   
  var url = location.hash.replace(/^#/, '');
  if (url != '') {
    setUpUrl(url);
  }

  $(document).on('click', 'button[data-ajax-url],a[data-ajax-url],option[data-ajax-url]', function(e) {
      e.preventDefault();
      var target = $(e.currentTarget);
      $('.nav li .nav-link, .mob-pa-nav').removeClass('active');
      target.addClass('active');
      var targeturl = target.attr('data-ajax-url').replace(/\/private-v3#?\/?/g, '').replace('#','/');
     routie(targeturl);
  });

    routie('private *', function(url) {
        $(".main").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
          setUpUrl(url);
    });
}

function setUpUrl(url,area) {
  if($.isRunning) return false;

  $.isRunning = true;
  if(area){
      $.mainContent = area;
  }
  $(".main").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });



  $('.modal').modal('hide');
  loadPage(url);
}

function innerMenustart(response, url){
  window.location.hash = url;
  $.mainContent.html(response);
  innerjsroutes(url);
  innerStylesroutes(url);
  userNotification();
  $('html, body').animate({
      scrollTop: $("html").offset().top
  }, 1000);
}

function loadPage(url,area) {
  $.ajax({
    method: 'GET',
    url: $.subPagesDirectory + url,
    dataType: 'html',
    data: { ajax: true },
    cache : true,

    success: function(response) {
        try {
            response = JSON.parse(response);
            if (typeof response == 'object' && response['status'] == 'error') {
                if (typeof response.redirect_to_signin !== undefined) {
                    window.location.href = response.redirect_to_signin;
                }
            }
            if(typeof response == 'object'){
              innerMenustart(response, url);
            }
        } catch (e) {
          innerMenustart(response, url);
        }

      if($('body').hasClass('sidebar-mobile-show')) {
        $( "button.navbar-toggler" ).trigger( "click" );
      }
      
      $('.main').LoadingOverlay("hide", true);
      $.isRunning = false;
    },

    /*error : function() {
      window.location.href = $.page404;
    },*/
   
  });
}

function innerjsroutes(url){
  var jsFiles = '';

    if ( ~url.indexOf('verify/idcard' ) || ~url.indexOf('verify/index')) {
        url = 'verify';
    }else if ( ~url.indexOf('profile/' )) {
        url = 'profile';
    }else if( ~url.indexOf('tours' ) ){
       url = 'tours';
    }
 
  switch (url) {

    case 'index':  
      jsFiles = [
          'js/private/profile-dash.js'
      ];
    break;
    case 'agency-profile':
      jsFiles = [
      "js/private/datepair/jquery.timepicker.js",
      "js/private/datepair/datepair.min.js",
      "js/private/datepair/jquery.datepair.min.js",
      "js/private/profile-agency.js?v1",
      ];
    break;
    case 'billing/index/':
      jsFiles = [
       "js/private/datepair/lib/moment.js",
       "js/private/datepair/lib/moment-timezone-with-data-2012-2022.min.js",
       "js/private/datepair/lib/pikaday.js",
       "js/private/self-checkout.js?v="+Math.random(),
      ];
    break;
    case 'tours':  
      jsFiles = [
        "js/private/dataTables/jquery.dataTables.min.js",
        "js/private/dataTables/dataTables.bootstrap4.min.js",
        "js/private/dataTables/dataTables.responsive.min.js",
        "js/private/dataTables/responsive.bootstrap4.min.js",
        "js/private/datepair/bootstrap-datepicker.min.js",
        "js/private/datepair/bootstrap-datepicker.en-GB.min.js",
        "js/private/datepair/jquery.timepicker.js",
        "js/private/datepair/datepair.min.js",
        "js/private/datepair/jquery.datepair.min.js",
        "js/private/profile-tours.js"

      ];
    break;
    case 'profile':
      jsFiles = [
        "js/private/libs/select2.full.js",
        "js/private/libs/jquery.maskedinput.min.js",
        "js/private/libs/moment.min.js",
        "js/private/datepair/jquery.timepicker.js",
        "js/private/datepair/jquery.datepair.min.js",
        "js/private/libs/daterangepicker.min.js",
        "js/private/libs/advanced-forms.js",

       // 'js/private/blueimp/load-image.all.min.js',
        'js/private/blueimp/jquery.ui.widget.js',
       // 'js/private/blueimp/jquery.iframe-transport.js',
        'js/private/blueimp/jquery.fileupload.js?v2',
        'js/private/blueimp/jquery.fileupload-process.js',
        'js/private/blueimp/jquery.fileupload-validate.js',
        'js/private/profile-steps.js?v3',
        //+ Date.now()
       // 'js/private/cropper/rcrop.min.js'
      ];break;
    case 'verify':
      jsFiles = [
          "js/private/verification.js"
      ];
      break;
    case 'show-escorts':
      jsFiles = [
          'js/private/agency-escorts.js'
      ];
      break;

    case 'settings':
      jsFiles = [
          'js/private/libs/select2.full.js',
          'js/private/settings.js'
      ];
      break;

    case 'client-blacklist':
      jsFiles = [
          "js/private/dataTables/jquery.dataTables.min.js",
          "js/private/dataTables/dataTables.bootstrap4.min.js",
          "js/private/dataTables/dataTables.responsive.min.js",
          "js/private/dataTables/responsive.bootstrap4.min.js",
          'js/private/datepair/bootstrap-datepicker.min.js',
          'js/private/client-blacklist.js',
      ];
      break;

    case 'support/index':
      jsFiles = [
          'js/private/libs/lightbox.js',
          'js/private/support.js'
      ];
      break;

    case 'premium':
      jsFiles = [
          'js/private/premium-models.js'
      ];
      break;

      case 'city-alerts/index':
      jsFiles = [
          'js/private/libs/select2.full.js',
          'js/private/member/city-alerts.js?v1'
      ];
      break;
       case 'alerts':
      jsFiles = [
          'js/private/libs/select2.full.js',
          'js/private/member/profile-alerts.js',
      ];
      break;
      case 'member-profile':
      jsFiles = [
          'js/private/libs/select2.full.js',
          'js/private/member/member-profile.js?v6'
      ];
      break;
      case  'happy-hour':
      jsFiles = [
          'js/private/happy-hour.js'
      ];break;
  }
  loadJS(jsFiles);
}
function innerStylesroutes(url){
   if( ~url.indexOf('tours' ) ){
    url = 'tours';
  }
  
  var styles = '';

  switch (url) {
    
    case 'tours':  
      var styles = [
        "css/private/responsive.bootstrap4.min.css",
        'css/private/bootstrap-datepicker.css',
      ];
    break;
    case 'billing/index/':  
      var styles = [
        "css/private/self-checkout.css?v=523",
        "css/private/pikaday.css",
        
      ];
    break;
    case 'client-blacklist':
      var styles = [
        'css/private/bootstrap-datepicker.css',
        'css/private/responsive.bootstrap4.min.css',
      ];
    break;
   case 'support/index':
          var styles = [
             'css/private/lightbox.css'
          ];
    break;
  }
  loadCSS(styles);
}

function userNotification(){
  $.ajax({
    method: 'POST',
    url: 'private-v3/user-notifications',
    dataType: 'html',
    data: { ajax: true },
    cache : true,

    success: function(responseText) {
      $('.user-notifications').html('');
      $('.user-notifications').append(responseText);
    },

    error : function() {
     
    },
   
  });
}

function loadJS(jsFiles) {
  $('.inner-js-file').remove();
  for(var i = 0; i<jsFiles.length;i++){
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.async = false;
    script.src = jsFiles[i];
        // +'?rand='+Math.random();
    script.className = 'inner-js-file';
    document.body.appendChild(script);
  }
}

function loadCSS(styles){
  $('.inner-css-file').remove();
  for(var i = 0; i<styles.length;i++){
    var style = document.createElement('link');
    style.type = 'text/css';
    style.href = styles[i];
    style.rel  = 'stylesheet';
    style.className = 'inner-css-file';
    document.head.appendChild(style);
  }
}

/****
* MAIN NAVIGATION
*/

$(document).ready(function($){

  // Add class .active to current link - AJAX Mode off
  $.navigation.find('a').each(function(){

    var cUrl = String(window.location).split('?')[0];

    if (cUrl.substr(cUrl.length - 1) == '#') {
      cUrl = cUrl.slice(0,-1);
    }

    if ($($(this))[0].href==cUrl) {
      $(this).addClass('active');

      $(this).parents('ul').add(this).each(function(){
        $(this).parent().addClass('open');
      });
    }
  });

  // Dropdown Menu
  $.navigation.on('click', 'a', function(e){
    
    if ($.ajaxLoad) {
      e.preventDefault();
    }

    if ($(this).hasClass('nav-dropdown-toggle')) {
      $(this).parent().toggleClass('open');
      resizeBroadcast();
    }
  });

  
  $('nav > ul.nav').on('click', 'a', function(e){
    if ($(this).hasClass('nav-dropdown-toggle')) {
      $(this).parent().toggleClass('open');
      resizeBroadcast();
    }
  });

  function resizeBroadcast() {

    var timesRun = 0;
    var interval = setInterval(function(){
      timesRun += 1;
      if(timesRun === 5){
        clearInterval(interval);
      }
      window.dispatchEvent(new Event('resize'));
    }, 62.5);
  }

  /* ---------- Main Menu Open/Close, Min/Full ---------- */
  $('.navbar-toggler').click(function(){
    if ($(this).hasClass('sidebar-toggler')) {
      $('body').toggleClass('sidebar-hidden');
      resizeBroadcast();
    }

    if ($(this).hasClass('sidebar-minimizer')) {
      $('body').toggleClass('sidebar-minimized');
      resizeBroadcast();
    }

    if ($(this).hasClass('aside-menu-toggler')) {
      $('body').toggleClass('aside-menu-hidden');
      resizeBroadcast();
    }

    if ($(this).hasClass('mobile-sidebar-toggler')) {
      
      $('body').toggleClass('sidebar-mobile-show');
      resizeBroadcast();
      
    }
  });

  $('.sidebar-close').click(function(){
    $('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');
  });

  /* ---------- Disable moving to top ---------- */
   $('a[href="#"][data-top!=true]').click(function(e){
    e.preventDefault();
  });

  $('.loged-user-data').on('show.bs.dropdown', function () {
    if ($(window).width() < 769) {
      if($('body').hasClass('sidebar-mobile-show')) {
        $( "button.navbar-toggler" ).trigger( "click" );
      }
    }
  });

    $(".chat-info-online-escorts").mCustomScrollbar({theme:"dark", autoHideScrollbar: true, scrollButtons:{ enable: true }});

});

/****
* CARDS ACTIONS
*/

$(document).on('click', '.card-actions a', function(e){
  e.preventDefault();

  if ($(this).hasClass('btn-close')) {
    $(this).parent().parent().parent().fadeOut();
  } else if ($(this).hasClass('btn-minimize')) {
    var $target = $(this).parent().parent().next('.card-block');
    if (!$(this).hasClass('collapsed')) {

      $('i',$(this)).removeClass($.panelIconOpened).addClass($.panelIconClosed);
    } else {
      $('i',$(this)).removeClass($.panelIconClosed).addClass($.panelIconOpened);
    }

  } else if ($(this).hasClass('btn-setting')) {
    $('#myModal').modal('show');
  }

});
