Cubix.EmailCollectingPopup = {};

Cubix.EmailCollectingPopup.inProcess = false;
Cubix.EmailCollectingPopup.url = '';
Cubix.EmailCollectingPopup.domain = '.beneluxxx.com';

Cubix.EmailCollectingPopup.Show = function (box_height, box_width) {

	if ( Cubix.EmailCollectingPopup.inProcess ) return false;

	/*var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();*/

	var y_offset = 130;

	var container = new Element('div', { 'class': 'email-collecting-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0,
        position: 'absolute',
        'z-index': 151
	}).inject(document.body);
	
	Cubix.EmailCollectingPopup.inProcess = true;

	var city_slug = "";
	var r_url = "";
	
	if (document.URL.indexOf("/city_") != -1)
	{
		var g_url = document.URL;
		
		if (document.URL.indexOf("#") != -1)
		{
			var ar = document.URL.split("#");
			g_url = ar[0];
		}
		
		var arr = g_url.split("/");
		var len = arr.length;
		var city = arr[len - 1];
		var city_arr = city.split("_");
		city_slug = city_arr[2];
	}
	
	if (city_slug.length > 0)		
		r_url = "?cs=" + city_slug;

	new Request({
		url: Cubix.EmailCollectingPopup.url + r_url,
		method: 'get',
		evalScripts: true,
		onSuccess: function (resp) {
			Cubix.EmailCollectingPopup.inProcess = false;
			var asd = Elements.from(resp, false);

			asd.inject(container);
			// container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'email-collecting-close-btn'
			}).inject(container);

			close_btn.addEvent('click', function() {
				Cookie.write('ec_session_closed', true, {domain: Cubix.EmailCollectingPopup.domain});
				$$('.email-collecting-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
			
			fireEvent('onload');	

			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.EmailCollectingPopup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
		}
	}).send();
		
	
	return false;
}

Cubix.EmailCollectingPopup.Send = function (e) {
	if (e != null ){
		e.stop();
	}
   
	var overlay = new Cubix.Overlay($$('.email-collecting-wrapper')[0], { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();

	this.set('send', {
		onSuccess: function (resp) {

			resp = JSON.decode(resp);

			overlay.enable();
			$$('.err').set('html', '');
			if ( resp.status == 'error' ) {
				$$('.err').set('html', resp.msg);
			}
			else if ( resp.status == 'success' ) {
				$$('.email-collecting-wrapper').destroy();
				$$('.overlay').destroy();
				
				Cookie.write('email_collecting', 'done', {domain: Cubix.EmailCollectingPopup.domain, duration: 365});
			}
		}.bind(this)
	});

	this.send();
}