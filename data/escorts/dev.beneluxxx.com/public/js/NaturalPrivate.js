var NaturalPic = this.NaturalPic = new Class({

	Implements: [Events, Options],

	options: {
		contentWrapper: ".natural-box-wrapper"
	},

	form : null,
    picOverlay : null,
		
	initialize: function(form, options) {
		this.setOptions(options);
		this.picOverlay = new Cubix.Overlay(document.getElement(this.options.contentWrapper), {color: '#fdfdfd', opacity: .6, position: '55%', loader: '/img/photo-video/ajax_small.gif'});
		this.form = document.id(form);
		this.initUpload(this.form);
		
		if(!$$('.no-natural-pic')[0]){
			this.initRemovePic();
		}
	},
	
	initUpload: function(form){
		
		var self = this;
		var myUpload = new MooUpload('filecontrol', {
			action: form.action,
			accept: 'image/*',
			method: 'auto',				// Automatic upload method (Choose the best)
			/*blocksize: 999999,	// Load per one chunk*/
			/*multiple:false,*/
			multiple: false,
			deleteBtn: true,
			onAddFiles: function(){
				var items = $$('#filecontrol_listView .item');
				if(items.length > 1){
					var close = items[0].getElement('.delete');
					close.fireEvent('click', {
						stop: function(){}
					});
				}
			},
			onBeforeUpload: function(){
				form.getElement('.progresscont').removeClass('none').fade('in');
			},
			onFileUpload: function(fileindex, response){ 
				
				var container = $$('.natural-box')[0];
				
				if(response.error == 0 && response.finish){
					
					var item = $('filecontrol_file_' + (fileindex));
					var itemBox = item.getParent('li');
					itemBox.set('tween', { onComplete: function() {this.element.destroy()}});
					itemBox.tween('opacity', 0);
					var fxProg = new Fx.Tween(form.getElement('.progresscont'),{duration: 2000});
					fxProg.addEvent('complete', function(){this.element.addClass('none'); });
					fxProg.start('opacity','100','0');
					
					container.set('html','');					
					var div = new Element('div', {'id': 'nat_' + response.pic_id, 'class': 'wrapper'}).inject(container, 'top');
					var strong = new Element('strong', {'html': 'Pending' }).inject(div, 'top');
					var img = new Element('img', {'src': response.photo_url}).inject(div, 'top');
									
					if(form.getElement('.delete').hasClass('none')){
						self.initRemovePic();
					}
				}
			},
			onFileUploadError: function(filenum, response){
				$$('.mooupload_error').appendText(response.error);
			}
		});
	},
	
	initRemovePic : function() {
		self = this;
		$$('#form-upload .delete').removeClass('none').addEvent('click', function(e){
			e.stop();
			self.picOverlay.disable();
			var picBox = $$('.natural-box')[0];
			var picWrap = picBox.getElement('.wrapper');
			new Request({
				url: '/photo-video/natural-pic',
				method: 'get',
				data: {'act': 'delete', pic_id: picWrap.get('id').replace('nat_' , '')},
				onSuccess: function (resp) {
					picWrap.destroy();
					new Element('div', {'class' : "no-natural-pic" }).inject(picBox, 'top');
					this.addClass('none');
					this.removeEvents('click');
					self.picOverlay.enable();
				}.bind(this)
			}).send();
		});
	}
});