Cubix.PrivateMessaging = {};
Cubix.PrivateMessaging.SendMessageFromThreads = function(container) {
	var overlay = new Cubix.Overlay(container, {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});

	overlay.disable();

	var data = {
		participants: $('participants').get('value'),
		message: $('message-body').get('value'),
		captcha: $('captcha').get('value')
	}
	
	if ( $defined($('agency-escort-id')) ) {
		data.escort_id = $('agency-escort-id').get('value');
	}
	
	new Request.JSON({
		url: 'private-messaging/send-message',
		method: 'POST',
		data: data,
		onSuccess: function (resp) {
			$$('.send-message-box span.error').set('html', '');
			if ( resp.status == 'error' ) {
				for ( field in resp.msgs ) {
					$(field).getNext('span.error').set('html', resp.msgs[field]);
				}
			} else {
				$('message-body').set('value', '');
				$('success-message').wink(2000);
				Cubix.PrivateMessaging.Threads.Load();
			}
			$$('#captcha-block img')[0].set('src', Cubix.PrivateMessaging.captchaUrl);
			$('captcha').set('value', '');
			overlay.enable();
		}
	}).send();
}

Cubix.PrivateMessaging.SendMessageFromThread = function(participant, container, threadId, escortId) {
	var overlay = new Cubix.Overlay(container, {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});

	overlay.disable();

	var data = {
		participants: participant,
		message: $('message-body').get('value'),
		captcha: $('captcha').get('value')
	}
	
	if ( typeof escortId !== 'undefined' ) {
		data.escort_id = escortId;
	}
	
	new Request.JSON({
		url: '/private-messaging/send-message',
		method: 'POST',
		data: data,
		onSuccess: function (resp) {
			$$('.send-message-box span.error').set('html', '');
			if ( resp.status == 'error' ) {
				for ( field in resp.msgs ) {
					$(field).getNext('span.error').set('html', resp.msgs[field]);
				}
			} else {
				$('message-body').set('value', '');
				$('success-message').wink(2000);
				Cubix.PrivateMessaging.Threads.LoadThread(threadId, 1, escortId);
			}
			$$('#captcha-block img')[0].set('src', Cubix.PrivateMessaging.captchaUrl);
			$('captcha').set('value', '');
			overlay.enable();
		}
	}).send();
}

Cubix.PrivateMessaging.wrapper = 'private-message-container'
Cubix.PrivateMessaging.Show = function(element, participant) {
	if ( Cubix.PrivateMessaging.isLoggedIn == 'false' ) {
		Cubix.Popup.Show('489', '652');
		return false;
	}
	if ( Cubix.PrivateMessaging.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();

	if ( ! element.overlay ) {
		element.overlay = new Cubix.Overlay(element, { loader: _st('loader-circular-tiny.gif'), position: '50%' });
	}
	
	if ( $defined($$('.' + Cubix.PrivateMessaging.wrapper)[0])) {
		$$('.' + Cubix.PrivateMessaging.wrapper).destroy();
	}
	
	var box_height = 498;
	var box_width = 375;
	
	var y_offset = 50;

	var container = new Element('div', { 'class': Cubix.PrivateMessaging.wrapper}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0
	}).inject(document.body);
	
	Cubix.PrivateMessaging.inProcess = true;
	element.overlay.disable();

	var url = '/private-messaging/send-message-ajax?participant=' + participant;
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.PrivateMessaging.inProcess = false;
			container.set('html', resp);
			
			var close_btn = new Element('div', {
				'class': 'cm-close-btn'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.' + Cubix.PrivateMessaging.wrapper).destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
			
			var form = container.getElement('form');
			form.addEvent('submit', Cubix.PrivateMessaging.Send);
			form.set('action', form.get('action'));
			element.overlay.enable();

			
			var f_m_d = ' ...';
			
			$$('.textinput, .textareainput').addEvent('click', function() {
				var self = this;
				
				if (self.hasClass('defaultText'))
				{
					self.removeClass('defaultText');
					self.set('value', '');
				}
			});
			
			$$('textareainput').addEvent('blur', function() {
				var self = this;
				
				if (self.get('value').length == 0)
				{
					self.addClass('defaultText');
					
					if (self.hasClass('f-m'))
						self.set('value', f_m_d);
				}
			});
			
			$$('.f-m').addEvent('keyup', function() {
				var self = this;
				
				$('message').set('value', self.get('value'));
			});
		}
	}).send();
		
	
	return false;
}

Cubix.PrivateMessaging.Send = function (e) {
	e.stop();
	function getErrorElement(el) {
		var error = el.getNext('.error');
		if ( error ) return error;
		
		return new Element('div', { 'class': 'error' }).inject(el, 'after');
	}
	
	var send_overlay = new Cubix.Overlay($$('.private-messaging-form')[0], { loader: _st('loader-circular-tiny.gif'), position: '50%' });
	send_overlay.disable();
	
	this.set('send', {
		onSuccess: function (resp) {
			resp = JSON.decode(resp);
			
			this.getElements('.error').destroy();
			this.getElements('.invalid').removeClass('invalid');
			
			if ( resp.status == 'error' ) {
				var c = false;
				for ( var field in resp.msgs ) {
					var input = this.getElement('*[name=' + field + ']');
					input.addClass('invalid');
					getErrorElement(input).set('html', resp.msgs[field]);

					if ( field == 'captcha' ) {
						c = true;
					}
				}
				
				
				// Regenerate new captcha
				var img = this.getElement('img'),
					src = img.get('src');
				
				src = src.substr(0, src.indexOf('?'));
				src = src + '?' + Math.random();
				
				img.set('src', src);
				$('f-captcha').set('value', '');
			}
			else if ( resp.status == 'success' ) {
				this.getParent().set('html', resp.msg);

				var close_btn = new Element('div', {
					'class': 'cm-close-btn'
				}).inject($$('.' + Cubix.PrivateMessaging.wrapper)[0]);

				close_btn.addEvent('click', function() {
					$$('.' + Cubix.PrivateMessaging.wrapper).destroy();
					$$('.overlay').destroy();
				});

				//Recaptcha.destroy();
			}
			
			send_overlay.enable();
		}.bind(this)
	});
	
	this.send();
}

Cubix.PrivateMessaging.Contacts = {};


Cubix.PrivateMessaging.Contacts.Load = function(page) {
	if ( undefined == page || page < 1 ) page = 1;
	var url = '/private-messaging/get-contacts?ajax=1&page=' + page;
	Cubix.PrivateMessaging.Contacts.Show(url);
	
	return false;
}
Cubix.PrivateMessaging.Contacts.Show = function(url) {
	var overlay = new Cubix.Overlay(Cubix.PrivateMessaging.Contacts.Container, {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			Cubix.PrivateMessaging.Contacts.Container.set('html', resp);
			overlay.enable();
		}
	}).send();
}

Cubix.PrivateMessaging.Contacts.Remove = function(id)
{
	var contactsOverlay = new Cubix.Overlay(Cubix.PrivateMessaging.Contacts.Container, {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	var threadsOverlay = new Cubix.Overlay(Cubix.PrivateMessaging.Threads.Container, {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	threadsOverlay.disable();
	contactsOverlay.disable();
		
	var url = '/private-messaging/remove-contact?id=' + id;
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			threadsOverlay.enable();
			contactsOverlay.enable();
			Cubix.PrivateMessaging.Contacts.Load();
			Cubix.PrivateMessaging.Threads.Load();
		}
	}).send();
}

Cubix.PrivateMessaging.Contacts.Add = function(id)
{
	
	if ( $$('#contacts table tbody tr').length >= 10 ) {
		alert('You can have only 10 contacts.');
		return;
	}
	
	var contactsOverlay = new Cubix.Overlay(Cubix.PrivateMessaging.Contacts.Container, {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	var threadsOverlay = new Cubix.Overlay(Cubix.PrivateMessaging.Threads.Container, {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	threadsOverlay.disable();
	contactsOverlay.disable();
		
	var url = '/private-messaging/add-contact?id=' + id;
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			contactsOverlay.enable();
			threadsOverlay.enable();
			Cubix.PrivateMessaging.Contacts.Load();
			Cubix.PrivateMessaging.Threads.Load();
		}
	}).send();
}



Cubix.PrivateMessaging.Threads = {};

CubixPrivateMessagingThreadsLoad = function(page) {
	if ( undefined == page || page < 1 ) page = 1;
	var url = '/private-messaging/get-threads?ajax=1&page=' + page;
	
	if ( $defined($('thread-agency-escort-id')) && $('thread-agency-escort-id').get('value') ) {
		url += '&a_escort_id=' + $('thread-agency-escort-id').get('value');
	}
	
	Cubix.PrivateMessaging.Threads.Show(url);
	
	return false;
}
Cubix.PrivateMessaging.Threads.Show = function(url) {
	var overlay = new Cubix.Overlay(Cubix.PrivateMessaging.Threads.Container, {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			Cubix.PrivateMessaging.Threads.Container.set('html', resp);
			overlay.enable();
		}
	}).send();
}

Cubix.PrivateMessaging.Threads.Remove = function(id, selfEscortId)
{
	var overlay = new Cubix.Overlay(Cubix.PrivateMessaging.Threads.Container, {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
		
	url = '/private-messaging/remove-thread?id=' + id
		
	if ( typeof selfEscortId !== 'undefined' ) {
		url += '&escort_id=' + selfEscortId;
	}
		
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			overlay.enable();
			Cubix.PrivateMessaging.Threads.Load();
		}
	}).send();
	

}

Cubix.PrivateMessaging.Threads.LoadThread = function(id, page, escortId) {
	if ( undefined == page || page < 1 ) page = 1;
	var url = '/private-messaging/get-thread?ajax=1&id=' + id + '&page=' + page;
	
	if ( typeof escortId !== 'undefined' ) {
		url += '&escort_id=' + escortId;
	}
	
	Cubix.PrivateMessaging.Threads.ShowThread(url);
	
	return false;
}
Cubix.PrivateMessaging.Threads.ShowThread = function(url)
{
	var overlay = new Cubix.Overlay($('thread-container'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
		
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			$('thread-container').set('html', resp);
			overlay.enable();
		}
	}).send();
}

Cubix.PrivateMessaging.Threads.Block = function(id, selfEscortId)
{
	
	var overlay = new Cubix.Overlay(Cubix.PrivateMessaging.Threads.Container, {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
		
	url = '/private-messaging/block-thread?id=' + id;
	if ( typeof selfEscortId !== 'undefined' ) {
		url += '&a_escort_id=' + selfEscortId;
	}
		
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			overlay.enable();
			Cubix.PrivateMessaging.Threads.Load();
		}
	}).send();
	
}

Cubix.PrivateMessaging.Threads.UnBlock = function(id, selfEscortId)
{	
	var overlay = new Cubix.Overlay(Cubix.PrivateMessaging.Threads.Container, {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
		
	url = '/private-messaging/unblock-thread?id=' + id;
	if ( typeof selfEscortId !== 'undefined' ) {
		url += '&a_escort_id=' + selfEscortId;
	}
		
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {
			overlay.enable();
			Cubix.PrivateMessaging.Threads.Load();
		}
	}).send();
	
}


