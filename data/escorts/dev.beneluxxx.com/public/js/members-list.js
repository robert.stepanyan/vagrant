Cubix.membersList = {};

Cubix.membersList.url = ''; // Must be set from php
Cubix.membersList.lng = '';
Cubix.membersList.container = '';

window.addEvent('domready', function(){
    var lng = Cubix.membersList.lng;
    if (lng == 'fr')
        lng = '';
    if (lng.length > 0)
        lng = '/' + lng;

    Cubix.membersList.container = $$('.ajax')[0];
    Cubix.membersList.initFiltration = function() {
     	
        if ( $('f_search') ) {
            $('f_search').removeEvents('click');
            $('f_search').addEvent('click', function(){
                //var city_id = $('r_city') ? $('r_city').get('value') : '';
                var username = $('f_username') ? $('f_username').get('value') : '';
				var url = '';
                var hash = '';

                if (username){
                    var o_d = 'desc';
                    if ( $$('input[name="ord_dir"]').length ) o_d = $$('input[name="ord_dir"]')[0].get('value');
                    var o_f = 'creation_date';
                    if ( $$('input[name="ord_field"]').length ) o_f = $$('input[name="ord_field"]')[0].get('value');

                    hash = 'username=' + username + '&ajax=1' + '&ord_dir=' + o_d + '&ord_field=' + o_f;
                    url = lng + '/members-list?' +  hash;
                }
                else
                    url = lng + '/members-list?ajax=1';

                Cubix.LocationHash.Set(hash);
                Cubix.membersList.doFiltrationRequest(url);
            });
        }
    };

    Cubix.membersList.doFiltrationRequest = function(url) {

        var overlay = new Cubix.Overlay($$('.ajax')[0], {
            loader: _st('loader-small.gif'),
            position: '50%'
        });

        overlay.disable();
		
        var req = new Request({
            method: 'get',
            url: url,
            onComplete: function(response) {
            	
                $$('.ajax')[0].set('html', response);
                //Cubix.membersList.initFiltration();
                Cubix.membersList.initOrdering();
                overlay.enable();
            }
        }).send();
    };

    Cubix.membersList.initOrdering = function() {

        if ( $$('.ord').length > 0 ) {
            $$('.ord').addEvent('click', function(e) {
                e.stop();

                var rel = this.get('rel');
                var page = $$('input[name=page]')[0].get('value');
                
                var username = '';
                if ( $('f_username') ) {
                    username = $('f_username').get('value');
                }
				
                var arr = new Array();

                arr = rel.split('|');

              var ord_field = arr[0];
                var ord_dir = '';

                if (arr[1] == undefined)
                    ord_dir = 'desc';
                else
                    ord_dir = arr[1];

                if ( ord_dir == 'asc' ) {
                    ord_dir = 'desc';
                }
                else {
                    ord_dir = 'asc';
                }

                var url = lng + '/members-list?ord_field=' + ord_field + '&ord_dir=' + ord_dir + '&ajax=1&page=' + page + '&username=' + username;

                Cubix.membersList.doSortRequest(url);
            });
        }

        if ( $$('.ord_es').length > 0 ) {
            $$('.ord_es').addEvent('click', function(e) {
                e.stop();

                var rel = this.get('rel');
                var page = $$('input[name=page]')[0].get('value');

                var username = '';
                if ( $('f_username') ) {
                    username = $('f_username').get('value');
                }

                var arr = new Array();

                arr = rel.split('|');

                var ord_field = arr[0];
                var ord_dir = '';

                if (arr[1] == undefined)
                    ord_dir = 'desc';
                else
                    ord_dir = arr[1];

                if ( ord_dir == 'asc' ) {
                    ord_dir = 'desc';
                }
                else {
                    ord_dir = 'asc';
                }

                var url = lng + '/members-list/?ord_field=' + ord_field + '&ord_dir=' + ord_dir + '&ajax=1&page=' + page + '&username=' + username;

                Cubix.membersList.doSortRequest(url);
            });
        }
    };

    Cubix.membersList.doSortRequest = function(url) {

        var overlay = new Cubix.Overlay($$('.ajax')[0], {
            loader: _st('loader-small.gif'),
            position: '50%'
        });

        overlay.disable();

        var req = new Request({
            method: 'get',
            url: url,
            onComplete: function(response) {
                $$('.ajax')[0].set('html', response);
                Cubix.membersList.initOrdering();
                overlay.enable();
            }
        }).send();
    };

	 var hash = document.location.hash.substring(1);
   
    Cubix.membersList.initFiltration();
    Cubix.membersList.initOrdering();

	new Autocompleter.Request.JSON($('f_username'), '/members-list/ajax-members-list-search', {
		postVar: 'f_username',
		indicatorClass: 'autocompleter-loading'
	});
});

Cubix.membersList.Load = function (el) {
    var url = el.get('href');
    url = url + '&ajax=1';

    if ($('perpage'))
        url = url + '&perpage=' + $('perpage').get('value');

    Cubix.membersList.Show(url);

    return false;
}

Cubix.membersList.Show = function (url) {

    var overlay = new Cubix.Overlay($(Cubix.membersList.container), {
        loader: _st('loader-small.gif'),
        position: '50%'
    });

    overlay.disable();

    new Request({
        url: url,
        method: 'get',
        onSuccess: function (resp) {
            $(Cubix.membersList.container).set('html', resp);
            overlay.enable();
            Cubix.membersList.initFiltration();
            Cubix.membersList.initOrdering();
        }
    }).send();
}

/* <-- */

/* --> LocationHash */
Cubix.LocationHash = {};

Cubix.LocationHash.Set = function (hash) {
    if ( hash.length ) {
        document.location.hash = '#' + hash;
    }
    else {
        document.location.hash = '';
    }

    return false;
}

Cubix.LocationHash.Parse = function (hash) {

    var params = hash.split('&');
    var filter = {};

    params.each(function (param) {
        var key_value = param.split('=');

        filter[key_value[0]] = key_value[1];

        if ( key_value[1] === undefined ) return;

    });
    return filter;
}
