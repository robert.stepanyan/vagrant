<?php
$CACHE_OFFSET = 7 * 24 * 60 * 60;
$JS_LOCATION  = "js/combined/";
$CSS_LOCATION = "css/combined/";

function getMaxModifTime( $js_sources ){
	$max_modif_time = null;
	foreach( $js_sources as $source ){
		$m_time = @filemtime($source);
		if( is_null($max_modif_time) ){
			$max_modif_time = $m_time;
		}
		if( $m_time > $max_modif_time ){
			$max_modif_time = $m_time;
		}
	}
	return $max_modif_time;
}

function writeToFile( $js_sources, $file_path ){

	//ATTENTION !!! Minify class is changed by Vahag. CLIENT CACHE VALIDATION IS DISABLED.
	$output = Minify::serve('Files', array(
        'files'  => $js_sources
        ,'quiet' => true
        //,'lastModifiedTime' => $lastModified
        ,'encodeOutput' => false
    ));
	$js = $output['content'];

	//echo $js;

	$f = @fopen($file_path, 'w');

	if (!$f) {
		echo 'Problem while writing to file.' . $file_path ; die;
	} else {
		$bytes = fwrite($f, $js);
		fclose($f);
	}
}


require dirname(__FILE__) . '/min/config.php';

set_include_path($min_libPath . PATH_SEPARATOR . get_include_path());

require_once 'Minify.php';
//JS FILES
$js_sources = array(
	'js/jquery-3.2.1.min.js',
	'js/jquery.browser.min.js',
	'js/popper.min.js',
	'js/bootstrap.min.js',
	'js/private/libs/loadingoverlay.min.js',
	'js/jquery.mCustomScrollbar.concat.min.js',
	'js/subscribe.js'
);

$other_sources = array(
	'front' => array(
		'js/filter.js',
		'js/slick.min.js',
		'js/front.js',
		'js/socket-io.js',
		'js/chat.js',
		'js/jquery.appear.js',
		'js/bubble-appear.js',
		'js/scroll-loading.js',
		'js/photo-roller.js'
	),

	'profile' => array(
		'js/filter.js',
		'js/slick.min.js',
        'js/private/libs/bootstrap-notify.min.js',
        'js/private/notify.js',
        'js/report.js',
		'js/profile.js',
		'js/front.js',
        'js/socket-io.js',
        'js/chat.js'
	),
	'agency' => array(
        'js/private/libs/bootstrap-notify.min.js',
        'js/private/notify.js',
		'js/filter.js',
		'js/slick.min.js',
		'js/agency.js',
        'js/front.js',
        'js/socket-io.js',
        'js/chat.js'
	),
	'review' => array(
		'js/filter.js',
		'js/slick.min.js',
		'js/review.js',
        'js/front.js',
        "js/private/datepair/bootstrap-datepicker.min.js",
        "js/private/datepair/bootstrap-datepicker.en-GB.min.js",
        'js/socket-io.js',
        'js/chat.js'
	),
	'classified-ad' => array(
		'js/filter.js',
		'js/slick.min.js',
		'js/classified-ad.js',
        'js/front.js',
        'js/private/libs/lightbox.js',
        'js/socket-io.js',
        'js/chat.js'
	),
	'classified-place-ad' => array(
		'js/filter.js',
		'js/private/libs/summernote.js',
		'js/private/libs/summernote-ext-emoji.js',
		'js/classified-place-ad.js',
		'js/classified-ad.js',
		'js/slick.min.js',
		'js/front.js',
        'js/socket-io.js',
        'js/chat.js'
	),
	'pm' => array(
		'js/private/chat/front.js',
		'js/private/chat/socket-io.js',
		'js/private/chat/chat-pm.js'
	),
	'advertise' => array(
        'js/filter.js',
        'js/socket-io.js',
		'js/chat.js',
		'js/advertise.js',
	),

	'private' => array(
	    'js/private/libs/routie.min.js',
        'js/private/notify.js',
		'js/private/libs/summernote.js',
		'js/private/libs/summernote-ext-emoji.js',
        'js/private/libs/simpleUpload.min.js',
        'js/jquery.smoothscroll.min.js',
        'js/private/libs/bootstrap-notify.min.js',
        'js/private/app.js',
        'js/socket-io.js',
        'js/private/chat/chat.js',
        'js/private/libs/select2.full.js',
	)
);


$css_sources = array();
$css_other = array(
	'chat' => array(
        'css/private/bootstrap4-style.css',
        'css/private/font-awesome.min.css',
        'css/private/style.css',
		'css/private/chat/style.css',
		'css/private/chat/chat.css',
		'css/private/chat/custom.css',
		'css/jquery.mCustomScrollbar.css',
	),
	'review' => array(
		'css/style.css',
		'css/custom.css',
		"css/private/responsive.bootstrap4.min.css",
		'css/private/bootstrap-datepicker.css',
        'css/chat.css',
        'css/custom.css',
	),
	'classified-place-ad' => array(
		'css/private/font-awesome.min.css',
		'css/private/summernote.css',
		'css/style.css',
		'css/custom.css',
		'css/private/lightbox.css',
        'css/chat.css',
	),

	'private' => array(
	 'css/private/bootstrap4-style.css',
	 'css/private/select2-bootstrap.css',
     'css/private/font-awesome.min.css',
     'css/private/style.css',
	 'css/private/simple-line-icons.css',
	 'css/private/glyphicons.css',
	 'css/private/glyphicons-filetypes.css',
	 'css/private/glyphicons-social.css',
	 'css/private/summernote.css',
	 'css/private/jquery.timepicker.css',
     'css/private/cropper/rcrop.min.css',
     'css/custom.css',
     'css/private/chat/chat.css',
     'css/jquery.mCustomScrollbar.css',
	)
);

$load_key = $_GET['load_key'];
$type = $_GET['type'];

/* Validate all get params */
$types = array('css', 'js');

if (!in_array($type, $types)) {
	die('Invalid type was specified');
}

if ($type == 'css') {
	if (!array_key_exists($load_key, $css_other)) {
		die('Invalid load_key was specified');		
	} 
} elseif ($type == 'js') {
	if ( ! array_key_exists($load_key, $other_sources) ) {
		die('Invalid load_key was specified');		
	}
}

/* Validate all get params */
$SOURCE_LOCATION = ($type == 'css') ? $CSS_LOCATION : $JS_LOCATION;
if (strlen($load_key)) {
	$SOURCE_LOCATION .= '_' . $load_key . '.' . $type;
} else {
	$SOURCE_LOCATION .= $type == 'js' ? '_scripts.js' : '_styles.css';
}

if ($type == 'js') {
	$_sources = array_merge($js_sources, (strlen($load_key) &&  isset( $other_sources[$load_key]) ? $other_sources[$load_key] : array()));

} elseif ( $type == 'css' ) {
	$_sources = array_merge($css_sources, ( strlen($load_key) &&  isset($css_other[$load_key]) ? $css_other[$load_key] : array()) );

}

if( file_exists($SOURCE_LOCATION)  ){
	$file_modif_time = @filemtime($SOURCE_LOCATION);

	if(getMaxModifTime($_sources) > $file_modif_time){
		writeToFile($_sources, $SOURCE_LOCATION);
	}else{
		// no need to write again (cache issue)
		//writeToFile($_sources, $SOURCE_LOCATION);
	}
}else{
	writeToFile($_sources, $SOURCE_LOCATION);
}

$js = @file_get_contents( $SOURCE_LOCATION );

if ( $type == 'js' ) {
	header ('content-type: text/javascript; charset: UTF-8');
} elseif ( $type == 'css' ) {
	header ('content-type: text/css;');
}
//USER CACHE VALIDATION
if( getMaxModifTime($_sources) < $file_modif_time && $_SERVER['HTTP_CACHE_CONTROL'] == 'max-age=0' && array_key_exists("HTTP_IF_MODIFIED_SINCE",$_SERVER) ){
	header("HTTP/1.1 304 Not Modified");
} else {
	header ('last-modified: ' . gmdate ('D, d M Y H:i:s', getMaxModifTime($_sources) ) . ' GMT');
	header ('cache-control: max-age=0');
}
header ('expires: ' . gmdate ('D, d M Y H:i:s', time() + $CACHE_OFFSET ) . ' GMT');
header ('date: ' . gmdate ('D, d M Y H:i:s', time() ) . ' GMT');


ob_start ('ob_gzhandler');
echo $js;
ob_flush();
