<? $PAGE_TITLE .= 'Working Cities'; ?>
<form class="form" action="" method="post">
<fieldset>
	<legend>Wroking Locations</legend>
	<div class="body">
		<div class="inner">
			<label for="country">Country</label>
			<select id="country" name="country">
				<option></option>
			<? foreach ( $COUNTRIES AS $id => $country ): ?>
				<option<?= ! $id ? ' disabled="disabled"' : '' ?> value="<?= $id ?>"<?= $id == 101 ? ' selected="selected"' : '' ?>><?= $country['title'] ?>&nbsp;&nbsp;</option>
			<? endforeach ?>
			</select>
			<span class="tip" title="If you choose a different country then Switzerland, your <br/>profile will be internationally listed on EscortDirector.com"></span>
			<div class="clear"></div>
		</div>

		<table>
			<tr>
				<td>
					<select multiple="multiple" style="width: 200px; background: none" size="10">
					<? foreach ( $COUNTRIES[199]['regions'] as $id => $region ): ?>
						<optgroup label="<?= $region['title'] ?>">
						<? foreach ( $region['cities'] as $city_id => $city ): ?>
							<option value="<?= $city_id ?>"><?= $city['title'] ?></option>
						<? endforeach ?>
						</optgroup>
					<? endforeach ?>
					</select>
				</td>
				<td>
					<input type="button" value="&gt;&gt;" id="add-city" /><br/>
					<input type="button" value="&lt;&lt;" id="remove-city" />
				</td>
				<td>
					<select multiple="multiple" style="width: 200px; background: none" size="10">
					
					</select>
				</td>
			</tr>
		</table>

		<p class="attention" style="margin: 15px 0 0 0"><strong>Note: </strong> when you offer outcall you can choose up to 5 cities. To set your base city just double click on it.</p>
	</div>
</fieldset>

<fieldset class="vindent">
	<legend>Available For <span class="tip" title="Please mention where you offer your services"></span></legend>
	<div class="body">
		<div class="inner fleft" style="clear: none">
			<label class="option" for="incall"><input id="incall" type="checkbox" name="incall" value="1" /> <span>Incall</span></label>
			<input id="zip" type="text" class="txt disabled" name="zip" value="Please enter ZIP code" size="21" disabled="disabled" />
			<div class="clear"></div>
			<dl class="options-vertical">
				<dt><label for="private-apartment"><input id="private-apartment" type="radio" name="incall_type" value="private_apartment" disabled="disabled" /> <span>Private Apartment</span></label></dt>
				<dt><label for="hotel-room"><input id="hotel-room" type="radio" name="incall_type" value="hotel_room" disabled="disabled" /> <span>Hotel Room</span></label></dt>
				<dl class="options-vertical">
					<dt><label for="five-star"><input id="five-star" type="radio" name="hotel_room" value="5" disabled="disabled" /> <span><img src="img/_/icon_star.png"><img src="img/_/icon_star.png"><img src="img/_/icon_star.png"><img src="img/_/icon_star.png"><img src="img/_/icon_star.png"></span></label></dt>
					<dt><label for="four-star"><input id="four-star" type="radio" name="hotel_room" value="4" disabled="disabled" /> <span><img src="img/_/icon_star.png"><img src="img/_/icon_star.png"><img src="img/_/icon_star.png"><img src="img/_/icon_star.png"></span></label></dt>
					<dt><label for="three-star"><input id="three-star" type="radio" name="hotel_room" value="3" disabled="disabled" /> <span><img src="img/_/icon_star.png"><img src="img/_/icon_star.png"><img src="img/_/icon_star.png"></span></label></dt>
					<dt><label for="two-star"><input id="two-star" type="radio" name="hotel_room" value="2" disabled="disabled" /> <span><img src="img/_/icon_star.png"><img src="img/_/icon_star.png"></span></label></dt>
					<dt><label for="one-star"><input id="one-star" type="radio" name="hotel_room" value="1" disabled="disabled" /> <span><img src="img/_/icon_star.png"><img src="img/_/icon_star.png"></span></label></dt>
				</dl>
				<dt><label for="club-studio"><input id="club-studio" type="radio" name="incall_type" value="hotel_room" disabled="disabled" /> <span>Club / Studio</span></label></dt>
				<dt>
					<label for="incall-other"><input id="incall-other" type="radio" name="incall_type" value="other" disabled="disabled" /> <span>Other (please provide details)</span></label>
					<input type="text" class="txt disabled" name="incall_other" value="" size="35" disabled="disabled" />
				</dt>
			</dl>
		</div>

		<div class="inner fleft" style="clear: none; margin-left: 40px">
			<label class="option" for="outcall"><input id="outcall" type="checkbox" name="outcall" value="1" /> <span>Outcall</span></label>
			<div class="clear"></div>
			<dl class="options-vertical">
				<dt><label for="hotel-visits"><input id="hotel-visits" type="radio" name="outcall_type" value="hotel_visits" disabled="disabled" /> <span>Hotel Visits Only</span></label></dt>
				<dt><label for="home-visits"><input id="home-visits" type="radio" name="outcall_type" value="home_visits" disabled="disabled" /> <span>Home Visits Only</span></label></dt>
				<dt><label for="hotel-home"><input id="hotel-home" type="radio" name="outcall_type" value="hotel_home" disabled="disabled" /> <span>Hotel &amp; Home Visits</span></label></dt>
				<dt>
					<label for="outcall-other"><input id="outcall-other" type="radio" name="outcall_type" value="other" disabled="disabled" /> <span>Other (please provide details)</span></label>
					<input type="text" class="txt disabled" name="outcall_other" value="" size="35" disabled="disabled" />
				</dt>
			</dl>
		</div>
		<div class="clear"></div>
	</div>
</fieldset>
</form>

<script type="text/javascript">
	var handleIncall = function (el) {
		el.blur();
		var disable = el.get('checked') ? null : 'disabled';
		$(el).getParent('.inner').getElements('input').each(function (it) {
			if ( it.get('id') == 'incall' ) return;
			it.set('disabled', disable);
			if ( disable ) it.addClass('disabled');
			else it.removeClass('disabled');
		});

		disable = $('hotel-room').get('checked') ? ($('incall').get('checked') ? null : 'disabled') : 'disabled';
		$('hotel-room').getParent('dt').getNext('dl').getElements('input').set('disabled', disable);

		disable = $('incall-other').get('checked') && el.get('checked') ? null : 'disabled';
		$('incall-other').getParent('label').getNext('input').set('disabled', disable)[(disable ? 'add' : 'remove') + 'Class']('disabled');
	};
	
	$('incall').getParent('.inner').getElements('input').each(function (el) {
		if ( ! ['checkbox', 'radio'].contains(el.get('type')) ) return;

		el.addEvent('change', handleIncall.pass($('incall')));
	});

	var handleOutcall = function (el) {
		el.blur();
		var disable = el.get('checked') ? null : 'disabled';
		$(el).getParent('.inner').getElements('input').each(function (it) {
			if ( it.get('id') == 'outcall' ) return;
			it.set('disabled', disable);
			if ( disable ) it.addClass('disabled');
			else it.removeClass('disabled');
		});

		disable = $('outcall-other').get('checked') && el.get('checked') ? null : 'disabled';
		$('outcall-other').getParent('label').getNext('input').set('disabled', disable)[(disable ? 'add' : 'remove') + 'Class']('disabled');
	};

	$('outcall').getParent('.inner').getElements('input').each(function (el) {
		if ( ! ['checkbox', 'radio'].contains(el.get('type')) ) return;

		el.addEvent('change', handleOutcall.pass($('outcall')));
	});
</script>

