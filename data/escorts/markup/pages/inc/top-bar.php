<div id="top-bar">
	<div id="btn-bar">
		<a class="btn-small fleft" href="#">
			<span class="icon-back">back</span>
		</a>

		<div class="indent">
			<a class="btn-small fleft" href="#">
				<span class="icon-tours">my tours</span>
			</a>

			<a class="btn-small fleft" href="#">
				<span class="icon-gallery">my gallery</span>
			</a>
			
			<div class="clear"></div>
		</div>

		<div class="clear"></div>
	</div>

	<h2>Create your profile in a few simple steps</h2>
	<p>Just fill in the neccessary information in the easy step by step form. All information can be changed at any time.</p>
</div>
