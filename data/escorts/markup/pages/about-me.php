<? $PAGE_TITLE .= "About Me" ?>
<form class="form" action="" method="POST">
<fieldset>
	<legend>About Me</legend>
	<div class="body">
		<div class="inner">
			<?= render_lng_picker() ?>
			<span class="tip" title="Describe yourself and write some additional information"></span>
			<div class="clear"></div>
		</div>

		<div class="inner">
		<? foreach ( $LANGS as $lng => $title ): ?>
			<textarea class="about-field" id="about-<?= $lng ?>" name="about[<?= $lng ?>]" cols="80" rows="10" style="display: none"></textarea>
		<? endforeach ?>
		</div>
	</div>
</fieldset>

<script type="text/javascript">
	var lngPicker = new Controls.LngPicker($$('.lng-picker')[0], {
		onChange: function (el) {
			var lng = el.get('cx:lng');
			$$('.about-field').setStyle('display', 'none');
			$('about-' + lng).setStyle('display', 'block').focus();
		}
	});
	
</script>

<fieldset class="vindent">
	<legend>Additional Information</legend>
	<div class="body">
		<div class="inner">
			<label class="block">Smoking:</label>
			<dl class="options-inline">
				<dt>
					<input id="smoking-yes" type="radio" name="smoking" value="yes" />
					<label for="smoking-yes">Yes</label>
				</dt>

				<dt>
					<input id="smoking-no" type="radio" name="smoking" value="no" />
					<label for="smoking-no">No</label>
				</dt>

				<dt>
					<input id="smoking-occasionally" type="radio" name="smoking" value="occasionally" />
					<label for="smoking-occasionally">Ocasionally</label>
				</dt>
			</dl>

			<div class="clear"></div>
		</div>

		<div class="inner">
			<label class="block">Drinking:</label>
			<dl class="options-inline">
				<dt>
					<input id="drinking-yes" type="radio" name="drinking" value="yes" />
					<label for="drinking-yes">Yes</label>
				</dt>

				<dt>
					<input id="drinking-no" type="radio" name="drinking" value="no" />
					<label for="drinking-no">No</label>
				</dt>

				<dt>
					<input id="drinking-occasionally" type="radio" name="drinking" value="occasionally" />
					<label for="drinking-occasionally">Ocasionally</label>
				</dt>
			</dl>

			<div class="clear"></div>
		</div>

		<div class="inner vindent long">
			<label for="showname">Special Characteristics</label>
			<input id="showname" class="txt" type="text" name="showname" value="" size="40" />

			<span class="tip" title="Please mention any special charactistics e.g. tattoos, piercings, etc."></span>

			<div class="clear"></div>
		</div>
	</div>
</fieldset>
</form>
