<? $PAGE_TITLE .= "Biography" ?>
<form class="form" action="" method="POST">
<fieldset>
	<legend>Basic BIO</legend>
	<div class="body">
		<div class="inner">
			<label for="showname">Showname</label>
			<input id="showname" class="txt" type="text" name="showname" value="" size="40" />
			
			<span class="tip" title="Name which will appear on the site on your setcard"></span>
			
			<div class="clear"></div>
		</div>

		<div class="inner">
			<label>Gender</label>
			<dl class="options">
				<dt>
					<label for="gender-female">Female</label><br/>
					<input id="gender-female" type="radio" name="gender" value="female" />
				</dt>

				<dt>
					<label for="gender-male">Male</label><br/>
					<input id="gender-male" type="radio" name="gender" value="male" />
				</dt>

				<dt>
					<label for="gender-trans">Trans</label><br/>
					<input id="gender-trans" type="radio" name="gender" value="trans" />
				</dt>
			</dl>
			
			<div class="clear"></div>
		</div>

		<div class="inner">
			<label for="age">Age</label>
			<select id="age" name="age">
				<option></option>
			<? for ( $i = 18; $i <= 60; $i++ ): ?>
				<option value="<?= $i ?>"><?= $i ?>&nbsp;&nbsp;</option>
			<? endfor ?>
			</select>

			<div class="clear"></div>
		</div>

		<div class="inner">
			<label>Ethnicity</label>
			<dl class="options">
				<dt>
					<label for="ethnicity-asian">Asian</label><br/>
					<input id="ethnicity-asian" type="radio" name="ethnicity" value="asian" />
				</dt>

				<dt>
					<label for="ethnicity-black">Black</label><br/>
					<input id="ethnicity-black" type="radio" name="ethnicity" value="black" />
				</dt>

				<dt>
					<label for="ethnicity-caucasian">Caucasian</label><br/>
					<input id="ethnicity-caucasian" type="radio" name="ethnicity" value="caucasian" />
				</dt>

				<dt>
					<label for="ethnicity-latin">Latin</label><br/>
					<input id="ethnicity-latin" type="radio" name="ethnicity" value="latin" />
				</dt>

				<dt>
					<label for="ethnicity-indian">Indian</label><br/>
					<input id="ethnicity-indian" type="radio" name="ethnicity" value="indian" />
				</dt>

				<dt>
					<label for="ethnicity-mixed">Mixed</label><br/>
					<input id="ethnicity-mixed" type="radio" name="ethnicity" value="mixed" />
				</dt>
			</dl>

			<div class="clear"></div>
		</div>

		<div class="inner">
			<label for="nationality">Nationality</label>
			<select id="nationality" name="nationality">
				<option></option>
			<? foreach ( $NATIONALITIES as $nationality ): ?>
				<option value="<?= $nationality ?>"><?= $nationality ?>&nbsp;&nbsp;</option>
			<? endforeach; ?>
			</select>

			<div class="clear"></div>
		</div>
	</div>
</fieldset>

<fieldset class="vindent">
	<legend>Vital Statistics</legend>
	<div class="body">
		<p class="attention"><strong>ATTENTION: </strong>all units are displayed in Metric System. If you would like to change it to Royal System, <a href="#">click here</a></p>

		<div class="inner">
			<label>Hair Color</label>
			<dl class="options">
				<dt>
					<label for="hair-color-blond">Blond</label><br/>
					<input id="hair-color-blond" type="radio" name="hair_color" value="asian" />
				</dt>

				<dt>
					<label for="hair-color-light-brown">Light Brown</label><br/>
					<input id="hair-color-light-brown" type="radio" name="hair_color" value="light-brown" />
				</dt>

				<dt>
					<label for="hair-color-brunette">Brunette</label><br/>
					<input id="hair-color-brunette" type="radio" name="hair_color" value="brunette" />
				</dt>

				<dt>
					<label for="hair-color-black">Black</label><br/>
					<input id="hair-color-black" type="radio" name="hair_color" value="black" />
				</dt>

				<dt>
					<label for="hair-color-red">Red</label><br/>
					<input id="hair-color-red" type="radio" name="hair_color" value="red" />
				</dt>
			</dl>

			<div class="clear"></div>
		</div>

		<div class="inner">
			<label>Eye Color</label>
			<dl class="options">
				<dt>
					<label for="eye-color-blue">Blue</label><br/>
					<input id="eye-color-blue" type="radio" name="eye_color" value="blue" />
				</dt>

				<dt>
					<label for="eye-color-green">Green</label><br/>
					<input id="eye-color-green" type="radio" name="eye_color" value="green" />
				</dt>

				<dt>
					<label for="eye-color-gray">Gray</label><br/>
					<input id="eye-color-gray" type="radio" name="eye_color" value="gray" />
				</dt>

				<dt>
					<label for="eye-color-brown">Brown</label><br/>
					<input id="eye-color-brown" type="radio" name="eye_color" value="brown" />
				</dt>
			</dl>

			<div class="clear"></div>
		</div>

		<div class="inner">
			<label for="height">Height</label>
			<select id="height" name="height">
				<option></option>
			<? for ( $i = 80; $i <= 220; $i += 10): ?>
				<option value="<?= $i ?>"><?= $i ?>&nbsp;&nbsp;</option>
			<? endfor ?>
			</select>
			<span class="units">cm</span>

			<div class="clear"></div>
		</div>

		<div class="inner">
			<label for="weight">Weight</label>
			<select id="weight" name="weight">
				<option></option>
			<? for ( $i = 35; $i <= 120; $i += 5): ?>
				<option value="<?= $i ?>"><?= $i ?>&nbsp;&nbsp;</option>
			<? endfor ?>
			</select>
			<span class="units">kg</span>

			<div class="clear"></div>
		</div>

		<div class="inner">
			<label for="dress-size">Dress Size</label>
			<select id="dress-size" name="dress_size">
				<option></option>
			<? for ( $i = 35; $i <= 120; $i += 5): ?>
				<option value="<?= $i ?>"><?= $i ?>&nbsp;&nbsp;</option>
			<? endfor ?>
			</select>

			<div class="clear"></div>
		</div>

		<div class="inner">
			<label for="shoe-size">Shoe Size</label>
			<select id="shoe-size" name="shoe_size">
				<option></option>
			<? for ( $i = 1; $i <= 10; $i += 0.5): ?>
				<option value="<?= $i ?>"><?= $i ?>&nbsp;&nbsp;</option>
			<? endfor ?>
			</select>

			<div class="clear"></div>
		</div>

		<div class="inner">
			<label for="bust">Bust-Waist-Hip</label>
			<select id="bust" name="bust">
				<option></option>
			<? for ( $i = 10; $i <= 100; $i += 5): ?>
				<option value="<?= $i ?>"><?= $i ?>&nbsp;&nbsp;</option>
			<? endfor ?>
			</select>
			<select id="waist" name="waist">
				<option></option>
			<? for ( $i = 10; $i <= 100; $i += 5): ?>
				<option value="<?= $i ?>"><?= $i ?>&nbsp;&nbsp;</option>
			<? endfor ?>
			</select>
			<select id="hip" name="hip">
				<option></option>
			<? for ( $i = 10; $i <= 100; $i += 5): ?>
				<option value="<?= $i ?>"><?= $i ?>&nbsp;&nbsp;</option>
			<? endfor ?>
			</select>

			<div class="clear"></div>
		</div>

		<div class="inner">
			<label for="cup-size">Cup Size</label>
			<select id="cup-size" name="cup_size">
				<option></option>
			<? for ( $i = 10; $i <= 50; $i++): ?>
				<? foreach ( array('A', 'B', 'C', 'D', 'DD') as $s ): ?>
				<option value="<?= $i ?><?= $s ?>"><?= $i ?><?= $s ?>&nbsp;&nbsp;</option>
				<? endforeach ?>
			<? endfor ?>
			</select>

			<div class="clear"></div>
		</div>

		<div class="inner">
			<label>Pubic Hair</label>
			<dl class="options">
				<dt>
					<label for="pubic-hair-shaved-completely">Shaved Copmletely</label><br/>
					<input id="pubic-hair-shaved-completely" type="radio" name="pubic_hair" value="shaved-completely" />
				</dt>

				<dt>
					<label for="pubic-hair-shaved-mostly">Shaved Mostly</label><br/>
					<input id="pubic-hair-shaved-mostly" type="radio" name="pubic_hair" value="shaved-mostly" />
				</dt>

				<dt>
					<label for="pubic-hair-trimmed">Trimmed</label><br/>
					<input id="pubic-hair-trimmed" type="radio" name="pubic_hair" value="trimmed" />
				</dt>

				<dt>
					<label for="pubic-hair-all-natural">All Natural</label><br/>
					<input id="pubic-hair-all-natural" type="radio" name="pubic_hair" value="all-natural" />
				</dt>
			</dl>

			<div class="clear"></div>
		</div>
	</div>
</fieldset>
</form>
