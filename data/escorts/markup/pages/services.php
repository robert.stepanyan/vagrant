<? $PAGE_TITLE .= 'Services'; ?>
<form class="form" action="" method="post">
<fieldset class="fleft" style="width: 410px; height: 155px">
	<legend>Sexual Orientation<span class="tip" title="text"></span></legend>
	<div class="body">
		<dl class="options-vertical">
			<dt><label for="orientation-hetero"><input id="orientation-hetero" type="radio" name="orientation" value="hetero" /> <span>Heterosexual</span></label></dt>
			<dt><label for="orientation-bi"><input id="orientation-bi" type="radio" name="orientation" value="bi" /> <span>Bisexual</span></label></dt>
			<dt><label for="orientation-homo"><input id="orientation-homo" type="radio" name="orientation" value="homo" /> <span>Homosexual</span></label></dt>
		</dl>
	</div>
</fieldset>

<fieldset class="fleft" style="margin-left: 20px; width: 410px; height: 155px">
	<legend>Services Offered For</legend>
	<div class="body">
		<dl class="options-vertical">
			<dt><label for="service-for-men"><input id="service-for-men" type="checkbox" name="service_for" value="men" /> <span>Men</span></label></dt>
			<dt><label for="service-for-women"><input id="service-for-women" type="checkbox" name="service_for" value="women" /> <span>Women</span></label></dt>
			<dt><label for="service-for-couples"><input id="service-for-couples" type="checkbox" name="service_for" value="couples" /> <span>Couples</span></label></dt>
			<dt><label for="service-for-2plus"><input id="service-for-2plus" type="checkbox" name="service_for" value="2plus" /> <span>2+ (threesome)</span></label></dt>
			<dt><label for="service-for-trans"><input id="service-for-trans" type="checkbox" name="service_for" value="trans" /> <span>Trans</span></label></dt>
		</dl>
	</div>
</fieldset>

<div class="clear"></div>

<fieldset class="vindent">
	<legend>Provided Services</legend>
	<div class="body">
		<table class="services">
			<tr>
				<th colspan="2">Most Common Services</th>
				<th class="surcharge">(surcharge)</th>
			</tr>
			<tbody>
			<? foreach ( $SERVICES['common'] as $id => $svc ): ?>
			<tr>
				<td class="chk"><input id="svc-<?= $id ?>" type="checkbox" name="svc_common[]" value="<?= $id ?>" /></td>
				<td><label for="svc-<?= $id ?>"><?= $svc ?></label></td>
				<td></td>
			</tr>
			<? endforeach ?>
			</tbody>
		</table>

		<table class="services services-extra">
			<tr>
				<th colspan="2">Extra Services</th>
				<th class="surcharge">(surcharge)</th>
			</tr>
			<tbody>
			<? foreach ( $SERVICES['extra'] as $id => $svc ): ?>
			<tr>
				<td class="chk"><input id="svc-<?= $id ?>" type="checkbox" name="svc_extra[]" value="<?= $id ?>" /></td>
				<td><label for="svc-<?= $id ?>"><?= $svc ?></label></td>
				<td></td>
			</tr>
			<? endforeach ?>
			</tbody>
		</table>

		<table class="services services-fetish">
			<tr>
				<th colspan="2">Fetish/Bizzare</th>
				<th class="surcharge">(surcharge)</th>
			</tr>
			<tbody>
			<? foreach ( $SERVICES['fetish'] as $id => $svc ): ?>
			<tr>
				<td class="chk"><input id="svc-<?= $id ?>" type="checkbox" name="svc_extra[]" value="<?= $id ?>" /></td>
				<td><label for="svc-<?= $id ?>"><?= $svc ?></label></td>
				<td></td>
			</tr>
			<? endforeach ?>
			</tbody>
		</table>

		<div class="clear"></div>

		<div class="inner long" style="margin-top: 40px">
			<label for="additional-svc">Additional Services</label>
			<div class="clear"></div>
			<textarea id="additional-svc" name="additional_services" cols="60" rows="6"></textarea>
		</div>
	</div>
</fieldset>

</form>
