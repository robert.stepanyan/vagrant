<? $PAGE_TITLE .= 'Prices'; ?>
<form class="form" action="" method="post">
	<fieldset>
		<legend>Rates</legend>
		<div class="body" style="padding: 20px 0 0 0">
			<fieldset class="rates" style="margin-right: 15px">
				<legend>Incall Rates</legend>
				<div class="body">
					<table>
						<thead>
							<tr>
								<th>Time</th>
								<th>Unit</th>
								<th>Amount</th>
								<th>Currency</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<input type="text" class="txt" name="incall_time" value="" size="3" />
								</td>
								<td>
									<label>
										<span>Minutes</span><br/>
										<input type="radio" name="incall_units" value="minutes" />
									</label>
									<label>
										<span>Hours</span><br/>
										<input type="radio" name="incall_units" value="hours" checked="checked" />
									</label>
									<label>
										<span>Days</span><br/>
										<input type="radio" name="incall_units" value="days" />
									</label>
								</td>
								<td>
									<input type="text" class="txt" name="incall_price" value="" size="5" />
								</td>
								<td class="currency">
									<span>EUR</span>
								</td>
								<td>
									<div class="btn-add" onclick="IncallRates.addRate('normal')"></div>
								</td>
							</tr>
						</tbody>
					</table>

					<div class="quick-options">
						<strong>Quick Options</strong>
						<ul>
							<li>
								<label for="incall-additional-hour">any additional hour</label>
								<input id="incall-additional-hour" type="text" class="txt" value="" size="5" />
								<div class="currency">EUR</div>
								<div class="btn-add" onclick="addIncallRate('additional-hour')"></div>
								<div class="clear"></div>
							</li>
							<li>
								<label for="incall-overnight">overnight</label>
								<input id="incall-overnight" type="text" class="txt" value="" size="5" />
								<div class="currency">EUR</div>
								<div class="btn-add" onclick="addIncallRate('overnight')"></div>
								<div class="clear"></div>
							</li>
						</ul>
					</div>
				</div>
			</fieldset>
			<fieldset class="rates">
				<legend>Outcall Rates</legend>
				<div class="body">
					<table>
						<thead>
							<tr>
								<th>Time</th>
								<th>Unit</th>
								<th>Amount</th>
								<th>Currency</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<input type="text" class="txt" value="" size="3" />
								</td>
								<td>
									<label>
										<span>Minutes</span><br/>
										<input type="radio" name="outcall_units" value="minutes" />
									</label>
									<label>
										<span>Hours</span><br/>
										<input type="radio" name="outcall_units" value="hours" checked="checked" />
									</label>
									<label>
										<span>Days</span><br/>
										<input type="radio" name="outcall_units" value="days" />
									</label>
								</td>
								<td>
									<input type="text" class="txt" value="" size="5" />
								</td>
								<td class="currency">
									<span>EUR</span>
								</td>
								<td>
									<div class="btn-add"></div>
								</td>
							</tr>
						</tbody>
					</table>

					<div class="quick-options">
						<strong>Quick Options</strong>
						<ul>
							<li>
								<label for="outcall-additional-hour">any additional hour</label>
								<input id="outcall-additional-hour" type="text" class="txt" value="" size="5" />
								<div class="currency">EUR</div>
								<div class="btn-add"></div>
								<div class="clear"></div>
							</li>
							<li>
								<label for="outcall-overnight">overnight</label>
								<input id="outcall-overnight" type="text" class="txt" value="" size="5" />
								<div class="currency">EUR</div>
								<div class="btn-add"></div>
								<div class="clear"></div>
							</li>
							<li>
								<label for="outcall-dinner-date">dinner date</label>
								<input id="outcall-dinner-date" type="text" class="txt" value="" size="5" />
								<div class="currency">EUR</div>
								<div class="btn-add"></div>
								<div class="clear"></div>
							</li>
							<li>
								<label for="outcall-weekend">weekend</label>
								<input id="outcall-weekend" type="text" class="txt" value="" size="5" />
								<div class="currency">EUR</div>
								<div class="btn-add"></div>
								<div class="clear"></div>
							</li>
						</ul>
					</div>
				</div>
			</fieldset>
		</div>

		<div class="clear"></div>
		
		<div class="defined-rates" style="margin-right: 15px">
			<strong>Your incall rates</strong>
			<p>You don't have set any incall rates yet</p>
		</div>
		<div class="defined-rates">
			<strong>Your outcall rates</strong>
			<p>You don't have set any outcall rates yet</p>
		</div>
	</fieldset>
</form>

<script type="text/javascript">
	var Rates = new Class({
		rates: new Hash(),
		container: null,

		_id: 0,

		id: function () {
			return ++this._id;
		},

		initialize: function () {
			
		},

		addRate: function (type) {
			type = ('add-rate-' + type).camelCase();

			var result = this[type](),
				container = this.container,
				list = container.getElement('.list');

			var found = this.find(result.rate);
			if ( found ) this.removeRate({}, found);

			result.rate.id = this.id();
			this.rates.set(result.rate.id, result);

			if ( ! list ) {
				list = new Element('ul', {
					'class': 'list'
				}).replaces(container.getElement('p'));
			}

			result.inject(list);
		},

		removeRate: function (e, rate) {
			var item = this.rates.get(rate.id);
			item.destroy();
			this.rates.erase(rate.id);
		},

		addRateNormal: function () {
			var values = this.getValues(), rate = new Rates.Normal(
				values.time,
				values.unit,
				values.price,
				values.currency
			);

			var item = new Element('li', function () {

			}).adopt(
				new Element('div', { 'class': 'time', html: rate.time }),
				new Element('div', { 'class': 'unit', html: rate.unit }),
				new Element('div', { 'class': 'sep' }),
				new Element('div', { 'class': 'time', html: rate.price }),
				new Element('div', { 'class': 'unit', html: rate.currency }),
				new Element('div', { 'class': 'btn-remove', events: { click: this.removeRate.bindWithEvent(this, [rate]) } })
			);

			item.rate = rate;

			return item;
		},

		addRateAdditional: function () {
			
		},

		addRateOvernight: function () {
			
		},

		getValues: function () {
			
		},

		find: function (rate) {
			var found = false;
			this.rates.each(function (item) {
				if ( found ) return;
				if ( JSON.encode(item.rate.plain()) == JSON.encode(rate.plain()) ) {
					found = item;
				}
			});

			return found;
		}
	});

	var IncallRates = new Class({
		Extends: Rates,

		initialize: function () {
			this.container = $$('.defined-rates')[0];
			
		},

		getValues: function () {
			return {
				time: $$('input[name=incall_time]')[0].get('value'),
				unit: $$('input[name=incall_units]').filter(function (el) { return el.get('checked') ? true : false })[0].get('value'),
				price: $$('input[name=incall_price]')[0].get('value'),
				currency: 'EUR'
			};
		}
	});

	var IncallRates = new IncallRates();

	

	var addIncallRate = function (type) {
		
	};

	var addIncallNormal = function () {
		
	};

	var addIncallAdditionalHour = function () {
		
	};

	Rates.Normal = function (time, unit, price, currency) {
		this.time = time;
		this.unit = unit;
		this.price = price;
		this.currency = currency;

		this.plain = function () {
			return {
				type: 'normal',
				time: this.time,
				unit: this.unit,
				price: this.price,
				currency: this.currency
			};
		};
	};

	Rates.Additional = function (price, currency) {
		this.price = price;
		this.currency = currency;

		this.plain = function () {
			return {
				type: 'additional',
				price: this.price,
				currency: this.currency
			};
		};
	};

//
//	Overnight: function (price, currency) {
//
//	},


</script>
