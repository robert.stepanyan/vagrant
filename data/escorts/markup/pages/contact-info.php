<? $PAGE_TITLE .= 'Contact Information'; ?>
<form class="form" action="" method="post">
<fieldset>
	<legend>Contact Information</legend>
	<div class="body">
		<fieldset>
			<legend>Contact</legend>
			<div class="body">
				<div class="inner long">
					<label for="phone">Phone Number</label>
					<input id="phone" type="text" class="txt" name="phone" value="" size="40" />
					<div class="clear"></div>
				</div>
				<div class="inner long">
					<label for="phone-alt">Alternative Phone</label>
					<input id="phone-alt" type="text" class="txt" name="phone_alt" value="" size="40" />
					<div class="clear"></div>
				</div>

				<div class="inner long vindent">
					<label>Phone Instructions</label>
					<dl class="options">
						<dt>
							<label for="sms-only">SMS Only</label><br/>
							<input id="sms-only" type="checkbox" name="phone_instr" value="sms-only" />
						</dt>

						<dt>
							<label for="no-sms">No SMS</label><br/>
							<input id="no-sms" type="checkbox" name="phone_instr" value="no-sms" />
						</dt>

						<dt>
							<label for="no-withheld">No Withheld Numbers</label><br/>
							<input id="no-withheld" type="checkbox" name="phone_instr" value="no-withheld" />
						</dt>

						<dt style="text-align: left">
							<label for="instr-other">Other</label><br/>
							<input id="instr-other" type="text" class="txt" name="phone_instr_other" value="" />
						</dt>
					</dl>

					<div class="clear"></div>
				</div>
			</div>
		</fieldset>

		<fieldset class="vindent">
			<legend>Web</legend>
			<div class="body">
				<div class="inner long">
					<label for="email">E-mail Address</label>
					<input id="email" type="text" class="txt" name="email" value="" size="40" />
					<div class="clear"></div>
				</div>
				<div class="inner long">
					<label for="website">WebSite / URL</label>
					<input id="website" type="text" class="txt" name="website" value="" size="40" />
					<div class="clear"></div>
				</div>
			</div>
		</fieldset>

		<fieldset class="vindent">
			<legend>Address Details</legend>
			<div class="body">
				<div class="inner long">
					<label for="club-name">Club Name</label>
					<input id="club-name" type="text" class="txt" name="club_name" value="" size="40" />
					<div class="clear"></div>
				</div>
				<div class="inner long">
					<label for="street">Street / N</label>
					<input id="street" type="text" class="txt" name="street" value="" size="40" />
					<input style="margin-left: 10px" id="street_no" type="text" class="txt" name="street_no" value="" size="5" />
					<div class="clear"></div>
				</div>
			</div>
		</fieldset>
	</div>
</fieldset>
</form>
