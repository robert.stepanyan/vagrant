<? $PAGE_TITLE .= 'Languages';
$langs = array(
	'en' => 'English',
	'fr' => 'French',
	'de' => 'German',
	'it' => 'Italian',
	'pt' => 'Portuguese',
	'ru' => 'Russian',
	'es' => 'Spanish',
	'sa' => 'Arabic',
	'be' => 'Belgian',
	'bg' => 'Bulgarian',
	'cn' => 'Chinese',
	'cz' => 'Czech',
	'hr' => 'Croatian',
	'nl' => 'Dutch',
	'fi' => 'Finnish',
	'gr' => 'Greek',
	'hu' => 'Hungarian',
	'in' => 'Indian',
	'jp' => 'Japanese',
	'lv' => 'Lett',
	'pl' => 'Polish',
	'ro' => 'Romanian',
	'tr' => 'Turkish'
) ?>
<script type="text/javascript">
	var moreLangs = function (el) {
		$$('.list')[1].setStyle('display', 'block');
		$(el).setStyle('display', 'none');
		return false;
	};
</script>
<form class="form" action="" method="post">
<fieldset>
	<legend>Spoken Languages</legend>
	<div class="body">
		<div class="lngs-box">
			<div class="heading">
				<div class="col">
					<strong>Basic</strong>
					<img src="img/_/icon_star.png" />
				</div>
				<div class="col">
					<strong>Fair</strong>
					<img src="img/_/icon_star.png" /><img src="img/_/icon_star.png" />
				</div>
				<div class="col">
					<strong>Good</strong>
					<img src="img/_/icon_star.png" /><img src="img/_/icon_star.png" /><img src="img/_/icon_star.png" />
				</div>
				<div class="col">
					<strong>Excellent / Native</strong>
					<img src="img/_/icon_star.png" /><img src="img/_/icon_star.png" /><img src="img/_/icon_star.png" /><img src="img/_/icon_star.png" />
				</div>
				<div class="clear"></div>
			</div>
			<div class="list">
			<? $c = 0; foreach ( $langs as $id => $title ): $c++ ?>
				<div class="row<?= $c % 2 == 0 ? ' alter' : '' ?>">
					<img src="img/_/flag_<?= $id ?>-big.png" width="29" height="17" alt="<?= $title ?>" title="<?= $title ?>" />
					<span><?= $title ?></span>
					<div class="col">
						<input type="radio" name="language[<?= $id ?>]" value="1" />
					</div>
					<div class="col">
						<input type="radio" name="language[<?= $id ?>]" value="2" />
					</div>
					<div class="col">
						<input type="radio" name="language[<?= $id ?>]" value="3" />
					</div>
					<div class="col">
						<input type="radio" name="language[<?= $id ?>]" value="4" />
					</div>
					<div class="clear"></div>
				</div>
				<? if ( $c == 7 ): ?></div><a href="#" onclick="return moreLangs(this)" class="more-lngs">display more languages</a><div class="list" style="display: none"><? endif ?>
			<? endforeach ?>
			</div>
		</div>
	</div>
</fieldset>
</form>
