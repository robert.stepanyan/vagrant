<? $PAGE_TITLE .= 'Working Times'; ?>
<form class="form" action="" method="post">
<fieldset>
	<legend>Working Times</legend>
	<div class="body">
		<div class="inner long">
			<label for="available-24-7" class="option"><input id="available-24-7" type="checkbox" name="available_24_7" value="1" /> <span>I am available 24/7</span></label>
		</div>
		<div class="clear"></div>
		<div class="gray">
			or
		</div>
		<div><strong>My working schedule is as follows:</strong></div>
		<table class="work-times" id="times">
			<thead>
				<tr>
					<th><input id="selectall" type="checkbox" value="" /></th>
					<th><label for="selectall">select all</label></th>
					<th class="spec"><span>from</span></th>
					<th class="spec"><span>to</span></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<? for ($j = 1; $j <= 7; $j++): ?>
				<tr class="work-times-disabled">
					<td><input id="day-<?= $j ?>" type="checkbox" name="working_day[<?= $j ?>]" value="<?= $j ?>" /></td>
					<td><label for="day-<?= $j ?>"><?= date('l', mktime(null, null, null, null, $j - 1)) ?></label></td>
					<td>
						<select name="work_hours_from[<?= $j ?>]" class="w6" disabled="disabled">
						<? for ($i = 0; $i < 24; $i ++): ?>
							<option value="<?= $i ?>"><?= (strlen($i) == 1 ? '0' : '') . $i ?>&nbsp;&nbsp;</option>
						<? endfor; ?>
						</select>
						<span>hrs</span>
						<select name="work_minutes_from[<?= $j ?>]" class="w6" disabled="disabled">
						<? for ($i = 0; $i < 24; $i ++): ?>
							<option value="<?= $i ?>"><?= (strlen($i) == 1 ? '0' : '') . $i ?>&nbsp;&nbsp;</option>
						<? endfor; ?>
						</select>
						<span>min</span>
					</td>
					<td>
						<select name="work_hours_to[<?= $j ?>]" class="w6" disabled="disabled">
						<? for ($i = 0; $i < 24; $i ++): ?>
							<option value="<?= $i ?>"><?= (strlen($i) == 1 ? '0' : '') . $i ?>&nbsp;&nbsp;</option>
						<? endfor; ?>
						</select>
						<span>hrs</span>
						<select name="work_minutes_to[<?= $j ?>]" class="w6" disabled="disabled">
						<? for ($i = 0; $i < 24; $i ++): ?>
							<option value="<?= $i ?>"><?= (strlen($i) == 1 ? '0' : '') . $i ?>&nbsp;&nbsp;</option>
						<? endfor; ?>
						</select>
						<span>min</span>
					</td>
					<td>
					<? if ( $j == 1 ): ?><div class="btn-work-days-all"></div><? endif ?>
					</td>
				</tr>
				<? endfor ?>
			</tbody>
		</table>
	</div>
</fieldset>
</form>

<script type="text/javascript">
	$('available-24-7').addEvent('change', function () {
		this.blur();
		var disable = ! this.get('checked') ? null : 'disabled';

		$('times')[(disable ? 'add' : 'remove') + 'Class']('work-times-disabled').getElements('input, select').set('disabled', disable);
		if ( ! disable ) {
			$$('.work-times tbody input').fireEvent('change');
		}
	});

	$$('.work-times tbody input').addEvent('change', function () {
		this.blur();
		var disable = this.get('checked') ? null : 'disabled';

		var tr = this.getParent('tr')[(disable ? 'add' : 'remove') + 'Class']('work-times-disabled');
		tr.getElements('select').set('disabled', disable);
	});

	$('selectall').addEvent('change', function () {
		this.blur();
		var flag = this.get('checked') ? true : false;

		$$('.work-times tbody input').set('checked', flag ? 'checked' : '').fireEvent('change');
	});

	$$('.btn-work-days-all').addEvent('click', function () {
		var selects = this.getParent('tr').getElements('select');
		
		$$('.work-times tbody input').each(function (el) {
			if ( el.get('checked') ) {
				el.getParent('tr').getElements('select').each(function (sel, i) {
					sel.set('value', selects[i].getSelected()[0].get('value')).getParent('td').highlight();
				});
			}
		});
	});
</script>
