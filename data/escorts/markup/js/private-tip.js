var Controls = {};


Controls.Tip = new Class({
	Implements: [Options, Events],

	options: {
		
	},

	initialize: function () {
		this.render();
		this.bind = {
			enter: this.mouseenter.bindWithEvent(this),
			leave: this.mouseleave.bindWithEvent(this)
		};
	},

	render: function () {
		this.tip = new Element('div', { 'class': 'cx-tip' }).inject($(document.body));
	},

	attach: function (el) {
		el.addEvents({
			mouseenter: this.bind.enter,
			mouseleave: this.bind.leave
		});
	},

	mouseenter: function (e) {
		var l = this.el = e.target;
		var pos = this.el.getPosition($(document.body));

		var t = this.tip;
		if ( ! l.retrieve('tip') ) {
			l.store('tip', l.get('title')).set('title', '');
		}
		
		t.set('html', l.retrieve('tip'));
		t.setStyles({
			left: pos.x + 22,
			top: pos.y - 22,
			display: 'block'
		});
	},

	mouseleave: function (e) {
		this.tip.setStyle('display', 'none');
	}
});

Controls.LngPicker = new Class({
	Implements: [Options, Events],

	options: {

	},

	el: null,
	langs: [],

	initialize: function (el, options) {
		this.setOptions(options || {});
		
		this.el = el;
		this.langs = this.el.getElements('span');
		
		this.bind = {
			click: this.click.bindWithEvent(this)
		};

		this.attach();
		
		this.click({}, this.langs[0]);
	},

	attach: function () {
		this.langs.each(function (lng) {
			lng.addEvent('click', this.bind.click.bindWithEvent(this, [lng]));
		}.bind(this));
	},

	click: function (e, el) {
		this.langs.removeClass('active');
		el.addClass('active');

		this.fireEvent('change', [el]);
	}
});

document.addEvent('domready', function () {
	var tip = new Controls.Tip;

	$$('.form span.tip').each(function (el) {
		tip.attach(el);
	});
});
