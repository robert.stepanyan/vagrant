<?php

$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : null;

if ( is_null($page) ) {
	global $PAGES;
	$PAGES = array();
	
	$dir = new DirectoryIterator('pages');
	foreach ( $dir as $item ) {
		if ( $item->isFile() ) {
			$PAGES[] = preg_replace('/\.(.+)$/', '', $item->getFilename());
		}
	}

	require('page_list.php');
	die;
}

$page_file = 'pages/' . $page . '.php';
if ( ! is_file($page_file) ) {
	die('Wrong page has been requested');
}

global $PAGE, $PAGE_TITLE, $CONTENT, $NATIONALITIES, $LANGS, $COUNTRIES, $SERVICES;
$PAGE_TITLE = 'Private Area - ';
$PAGE = $page;

$NATIONALITIES = require('data/nationalities.php');
$COUNTRIES = require('data/countries.php');
require('data/regions.php');
require('data/cities.php');
$LANGS = array('en' => 'English', 'it' => 'Italian', 'gr' => 'Greek', 'fr' => 'French', 'de' => 'Gemran');

$SERVICES = require('data/services.php');

function render_lng_picker() {
	global $LANGS;
	$html = '<div class="lng-picker">';

	foreach ( $LANGS as $id => $title ) {
		$html .= '<span cx:lng="' . $id . '"><img width="16" height="11" alt="' . $title . '" title="' . $title . '" src="/img/_/flag_' . $id . '.png" /></span>';
	}

	$html .= '</div>';

	return $html;
}

ob_start();
require($page_file);
$CONTENT = $page_html = ob_get_clean();

ob_start();
require('layout-2.php');
$layout = ob_get_clean();

header('Content-Type: text/html; charset=UTF-8');
echo $layout;
