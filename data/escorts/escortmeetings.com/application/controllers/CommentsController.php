<?php

class CommentsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->view->layout()->disableLayout();
		$this->model = new Model_Comments();
	}

	public function indexAction()
	{
		if (!is_null($this->_getParam('ajax')))
		{

        }
		else
		{
            $this->view->layout()->setLayout('main-simple');
        }

		$req = $this->_request;

		$page = $req->page;
		
		if (!$page)
			$page = 1;

        $per_page = 10;

        $comments = $this->model->getLatestComments($page, $per_page);

        $count = $comments['count'];
        unset($comments['count']);
        $resComments = array();
		
        if ($comments) 
		{	
			$escort_users = array();
			
            foreach ($comments as $k => $comment)
			{
                $resComments[$k]['comments'] = $comment;

                if (isset($comment[0]))
				{
                    $data['hash'] = $comment[0]['hash'];
                    $data['ext'] = $comment[0]['ext'];
                    $data['escort_id'] = $k;
                    $data['application_id'] = Cubix_Application::getId();

                    $photo_url = new Model_Escort_PhotoItem($data);
					
                    $resComments[$k]['escort']['photo'] = $photo_url->getUrl('lvthumb');
                    $resComments[$k]['escort']['showname'] = $comment[0]['showname'];
					$resComments[$k]['escort']['escort_id'] = $comment[0]['escort_id'];
                }
				
				$users = array();
				
				foreach ($comment as $c)
				{
					$users[] = $c->user_id;
				}
				
				if (count($users))
				{
					$escort_users[$k] = implode(',', $users);
				}
            }
			
			if (count($escort_users))
			{
				$e = implode('_', array_keys($escort_users));
				$m = implode('_', $escort_users);
				$cache = Zend_Registry::get('cache');
				$cache_key = Cubix_Application::getId() . '_' . $e . '_' . $m;				
				$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
				
				if ( ! $tops = $cache->load($cache_key) )
				{
					$modelM = new Model_Members();
					$tops = $modelM->getEscortsTopOfUsers($escort_users);

					$cache->save($tops, $cache_key, array(), 1800); //30 min
				}
				
				$this->view->tops = $tops;
			}
        }

		$this->view->comments = $resComments;
		$this->view->page = $page;
		$this->view->count = $count;
		$this->view->per_page = $per_page;
	}
	
	public function ajaxShowAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$config = Zend_Registry::get('escorts_config');
		$per_page = $config['comments']['perPage'];

		$page = $req->page;
		if ( ! $page )
			$page = 1;

		$escort_id = intval($req->escort_id);
		$city = Model_EscortsV2::getCityByEscortId($escort_id);

		$count = 0;
		$comments = array();
		
		$comments = $this->model->getEscortComments($page, $per_page, $count, $escort_id);

		foreach( $comments as $k => $comment ) {
			$comments[$k]->insert_date = $this->getCommentTime($comment->time);
		}

		$this->view->comments = $comments;
		$this->view->page = $page;
		$this->view->count = $count;
		$this->view->per_page = $per_page;

		$this->view->escort_id = $escort_id;
		$this->view->city = $city;
	}

	protected function getCommentTime($insertdate)
	{
        $tm = explode(" ", $insertdate);
        $days = explode("-", $tm[0]);
        $hours = explode(":", $tm[1]);
        
        $now = time();
		$u_insertdate = strtotime($insertdate);
		$inserted = mktime(date("H", $u_insertdate), date('i', $u_insertdate), date("s", $u_insertdate), date("m", $u_insertdate), date("d", $u_insertdate), date('Y', $u_insertdate));
		//$inserted = mktime($hours[0],$hours[1],$hours[2],$days[1],$days[2],$days[0]);
        $diff = $now - $inserted;
		
        $activedays = abs(floor($diff / 86400));
        $activehours = abs(floor(($diff % 86400) / 3600));
        $activeminutes = abs(floor((($diff % 86400) % 3600) / 60));

        if(date("m-d-Y", strtotime($insertdate)) == date("m-d-Y"))
        {
        	$rslt = Cubix_I18n::translate('today');
        }

        if ($activedays >= 2) {
           //$rslt = str_replace('%TIME%', $activedays . " " . ((1 < $activedays)? $LNG['days'] : $LNG['day']), $LNG['time_ago']);
			if ( $activedays > 1 )
				$rslt = Cubix_I18n::translate('days_ago', array('DAYS' => $activedays));
			else
				$rslt = Cubix_I18n::translate('day_ago', array('DAYS' => $activedays));
        }
        elseif (0 < $activedays && 2 > $activedays) {
            $rslt = Cubix_I18n::translate('yesterday');
        }
        else {
            if (0 < $activehours) {
                //$rslt = str_replace('%TIME%', $activehours . " " . ((1 < $activehours)? $LNG['hours'] : $LNG['hour']) . " " . $activeminutes . "  " . ((1 < $activeminutes)? strtolower($LNG['minutes']) : strtolower($LNG['minute'])), $LNG['time_ago']);
                //$rslt = str_replace('%TIME%', $activehours . " " . ((1 < $activehours)? $LNG['hours'] : $LNG['hour']) . " " . $activeminutes . "  " . ((1 < $activeminutes)? strtolower($LNG['minutes']) : strtolower($LNG['minute'])), $LNG['time_ago']);
				if ( $activehours > 1 )
					$rslt = Cubix_I18n::translate('hours_ago', array('HOURS' => $activehours));
				else
					$rslt = Cubix_I18n::translate('hour_ago', array('HOURS' => $activehours));
            }
            else {
                //$rslt = str_replace('%TIME%', $activeminutes . " " . ((1 < $activeminutes)? $LNG['minutes'] : $LNG['minute']), $LNG['time_ago']);
				if ( $activeminutes > 1 )
					$rslt = Cubix_I18n::translate('minutes_ago', array('MINUTES' => $activeminutes));
				else
					$rslt = Cubix_I18n::translate('minute_ago', array('MINUTES' => $activeminutes));
            }
        }

        return $rslt;
    }


	public function ajaxAddCommentAction()
	{
		$config = Zend_Registry::get('escorts_config');
		$textLength = $config['comments']['textLength'];

		$req = $this->_request;

		$this->view->escort_id = $escort_id = intval($req->escort_id);
		$this->view->user_id = $user_id = intval($req->user_id);
		
		$this->view->comment_id = $comment_id = intval($req->comment_id);
		$current_user = Model_Users::getCurrent();
		$this->view->user_type  =  $user_type = $current_user->user_type;
		
		/*if ( $comment_id ) {
			$comment = $this->model->getComment($comment_id);
		}*/

		$comment = $req->comment;
		$captcha_text = $req->captcha_text;

		if ( $req->isPost() ) {

			$session = new Zend_Session_Namespace('captcha');
			$blackListModel = new Model_BlacklistedWords();
			$errors = array();

			if ( $session->captcha != strtolower(trim($captcha_text)) ) {
				$errors[] = 'captcha_error';
			}

			if ( strlen($comment) < $textLength ) {
				$errors[] = 'comment_to_short';
			}

			/*if ( $comment_id && ( $comment_id == $comment->id ) ) {
				$errors[] = 'cannot_reply';
			}*/

			if ( count($errors) > 0 ) {
				die(json_encode($errors));
			}
			else {
				$data = array(
					'user_id' => $user_id,
					'escort_id' => $escort_id,
					'status' => Model_Comments::COMMENT_NOT_APPROVED,
					'message' => $comment,
					'user_type' => $user_type
				);

				if ( $comment_id ) {
					$data['is_reply_to'] = $comment_id;
				}
				
				if($blackListModel->checkWords($data['message'])){
					
					$data['has_bl'] = 1;
				}
				
				// ip
				if ($_SERVER["HTTP_X_FORWARDED_FOR"] != "")
				{
					// for proxy
					$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
				}
				else
				{
					// for normal user
					$ip = $_SERVER["REMOTE_ADDR"];
				}
				
				$data['ip'] = $ip;
				//

				$this->model->addComment($data);
			}
		}
	}

	public function ajaxVoteAction()
	{
		$current_user = Model_Users::getCurrent();
		$req = $this->_request;

		if ($current_user && $current_user->isMember() ) {
			$comment_id = intval($req->comment_id);
			$escort_id = intval($req->escort_id);
			$type = $req->type;

			$cook_name = "v2_is_escort_comment_vote_" . $escort_id . '_' . $comment_id;
			$cook_value = array("comment_added_" . $comment_id . '_' . $escort_id);

			if ( ! isset( $_COOKIE[$cook_name]) ) {				
				setcookie($cook_name, serialize($cook_value), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);

				$this->model->vote($type, $comment_id);
				die;
			}
			/*else {
				$cook_data = unserialize($_COOKIE[$cook_name]);

				if ( is_array($cook_data) ) {
					if ( in_array($cook_value[0], $cook_data) ) {
						$this->view->alredy_voted = true;
					}
					else {
						$new_data = array_merge($cook_data, $cook_value);
						setcookie($cook_name, serialize($new_data), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);

						$this->model->vote($type, $comment_id);
						die;
					}
				}
			}*/
			else
			{
				$this->view->alredy_voted = true;
			}
		}
		else {
			$this->view->not_loged_in = true;
		}
	}

    public function getEscortCommentsAction()
	{ 
        $escort_id = $this->_getParam('escort_id');
        $req = $this->_request;
        $page = $req->page;
		
		if (!$page)
			$page = 1;
		
        $per_page = 10;
        $resComments = array();
        $count = 0;
        $comments = $this->model->getEscortLatestComments($escort_id, $page, $per_page);  
		
        if ($comments)
		{
            $resComments['comments'] = $comments['comments'];
            $count = $comments['count'];
        }
				
		$users = array();
				
		foreach ($resComments['comments'] as $c)
		{
			$users[] = $c->user_id;
		}

		if (count($users))
		{
			$escort_users = array();
			$escort_users[$escort_id] = implode(',', $users);
			
			$e = implode('_', array_keys($escort_users));
			$m = implode('_', $escort_users);
			$cache = Zend_Registry::get('cache');
			$cache_key = Cubix_Application::getId() . '_' . $e . '_' . $m;				
			$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

			if ( ! $tops = $cache->load($cache_key) )
			{
				$modelM = new Model_Members();
				$tops = $modelM->getEscortsTopOfUsers($escort_users);

				$cache->save($tops, $cache_key, array(), 1800); //30 min
			}

			$this->view->tops = $tops;
		}
		
        $this->view->comments = $resComments;
        $this->view->page = $page;
        $this->view->count = $count;
        $this->view->escort_id = $escort_id;
        $this->view->per_page = $per_page;
    }
}
