<?php

class Model_Statistics extends Cubix_Model
{
	public static function getTotalCount($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null)
	{
		$where = array(
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				COUNT(DISTINCT(eic.escort_id)) AS escort_count
			FROM escorts_in_cities eic
			/*INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN regions r ON r.id = ct.region_id*/
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
		';

		return self::db()->fetchOne($sql);
	}

	static public function getCountries($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null)
	{
		$where = array(
			'eeic.gender = ?' => $gender,
			'eeic.is_agency = ?' => $is_agency,
			'eeic.is_tour = ?' => $is_tour,
			'eeic.is_upcoming = ?' => $is_upcoming
		);

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		/*$sql = '
			SELECT
				eic.country_id, COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				c.title_' . $lng . ' AS country_title, c.iso
			FROM escorts_in_countries eic
			INNER JOIN countries c ON c.id = eic.country_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.country_id;
		';*/
		$sql = '
			SELECT
				eic.country_id, COUNT(DISTINCT(eeic.escort_id)) AS escort_count,
				c.title_' . $lng . ' AS country_title, c.iso
			FROM escorts_in_cities eeic
			INNER JOIN cities cc ON cc.id = eeic.city_id
			INNER JOIN escorts_in_countries eic ON cc.country_id = eic.country_id
			INNER JOIN countries c ON c.id = eic.country_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY c.id
			ORDER BY c.title_' . $lng . '
		';

		$result = self::db()->fetchAll($sql);
		//usort($result, array('Model_Statistics', '_orderByEscortCount'));

		return $result;
	}

	public static function getCities($region_id = null, $gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null,$wheretmp = null, $limit = null)
	{
		$where = array(
			'eic.region_id = ?' => $region_id,
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

        if($wheretmp){
            $where = array_merge($wheretmp,$where);
        }

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				eic.city_id, COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug,
				r.title_' . $lng . ' AS region_title, c.iso AS country_iso
			FROM escorts_in_cities eic
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = ct.country_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.city_id
		';
		
		if ( $limit ) {
			$sql .= ' LIMIT ' . $limit;
		}

		$result = self::db()->fetchAll($sql);
		usort($result, array('Model_Statistics', '_orderByEscortCount'));

		return $result;
	}

	public static function getCitiesByCountry($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null)
	{
		$where = array(
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				eic.city_id, COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug,
				r.title_' . $lng . ' AS region_title, ct.country_id, c.title_' . $lng . ' AS country_title
			FROM escorts_in_cities eic
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = ct.country_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.city_id
		';
		
		$cities = self::db()->fetchAll($sql);
		
		// divide cities array into array of arrays by city's country id
		$result = array();
		foreach ( $cities as $city ) {
			if ( ! isset($result[$city->country_id]) )
				$result[$city->country_id] = array();
			$result[$city->country_id][] = $city;
		}

		// find cities for each country from $result array
		$countries = self::getCountries($gender, $is_agency, $is_tour, $is_upcoming);
		foreach ( $countries as &$country ) {
			if ( isset($result[$country->country_id])) {
				$country->cities = $result[$country->country_id];
			}
		}

		return $countries;
	}

	public static function getRegions($gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null)
	{
		$where = array(
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				r.title_' . $lng . ' AS region_title, r.slug AS region_slug
			FROM escorts_in_cities eic
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN regions r ON r.id = ct.region_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.region_id;
		';

		$result = self::db()->fetchAll($sql);
		usort($result, array('Model_Statistics', '_orderByEscortCount'));

		return $result;
	}

	public static function getZones($city_id = null, $gender = null, $is_agency = null, $is_tour = null, $is_upcoming = null)
	{
		$where = array(
			'eic.city_id = ?' => $city_id,
			'eic.gender = ?' => $gender,
			'eic.is_agency = ?' => $is_agency,
			'eic.is_tour = ?' => $is_tour,
			'eic.is_upcoming = ?' => $is_upcoming
		);

		$where = self::getWhereClause($where, true);

		$lng = Cubix_I18n::getLang();
		$sql = '
			SELECT
				COUNT(DISTINCT(eic.escort_id)) AS escort_count,
				ct.title_' . $lng . ' AS city_title, ct.slug AS city_slug,
				cz.title_' . $lng . ' AS zone_title, cz.slug AS zone_slug
			FROM escorts_in_cityzones eic
			INNER JOIN cityzones cz ON cz.id = eic.cityzone_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			' . (!is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.cityzone_id;
		';

		$result = self::db()->fetchAll($sql);

		return $result;
	}

	public static function _orderByEscortCount($a, $b)
	{
		$a = (int) $a->escort_count; $b = (int) $b->escort_count;
		if ( $a == $b ) return 0;
		return $a < $b ? 1 : -1;
	}

	public static function ajaxSearchArray()
	{
		
	}
	
	public static function getCountryByCitySlug($city_slug)
	{
		$country_id = self::db()->fetchOne('
			SELECT co.id FROM countries co
			INNER JOIN cities c ON c.country_id = co.id
			WHERE c.slug = ?
		', array($city_slug));

		return $country_id;
	}
}
