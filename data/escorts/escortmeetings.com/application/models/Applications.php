<?php

class Model_Applications
{
	public static function getAll()
	{
		$db = Zend_Registry::get('db');
		
		return $db->query('
			SELECT a.id, a.host, c.iso AS country_iso, c.' . Cubix_I18n::getTblField('title') . ' AS country_title
			FROM applications a
			INNER JOIN countries c ON c.id = a.country_id
			ORDER BY c.iso DESC
		')->fetchAll();
	}

	public static function getDefaultCurrencyTitle()
	{
		$db = Zend_Registry::get('db');
		$currency = $db->fetchOne('
				SELECT currency_title
				FROM applications
				WHERE id = '.Cubix_Application::getId()
			);
		return $currency;
	}

	public static function clearCache()
	{
		// Clear whole cache except hits count
		$cache = Zend_Registry::get('cache');
		$cache_native = Zend_Registry::get('cache_native');
		
		$keys = array('v2_escort_profile_views_%s', 'v2_banners_cache_%s', 'v2_%s_banners_cache', 'latest_action_date_%s', '%s_chat_fake_users');
		$apps = array(
			array('em', 33),
			array('a6', 16),
			array('bl', 30),
			array('efv2', 1)
		);
		$saved = array();
		$saved_native = array();
		foreach ( $apps as $app ) {
			list($name, $aid) = $app;
			foreach ( $keys as $key ) {
				$s_key = $key;
				$key = sprintf($key, $name);
				$cache->setKeyPrefix($aid);
				$value = $cache->load($key);
				if ( ! isset($saved[$aid]) ) {
					$saved[$aid] = array();
				}
				if ( $value )
					$saved[$aid][$key] = $value;
				
				
				
				$s_key = sprintf($s_key, $aid);
				$value = $cache_native->get($s_key);
				if ( ! isset($saved_native[$aid]) ) {
					$saved_native[$aid] = array();
				}
				if ( $value )
					$saved_native[$aid][$s_key] = $value;
			}
		}
		
		$cache->clean();
		
		foreach ( $saved as $aid => $data ) {
			foreach ( $data as $key => $value ) {
				
				if ( $value ) {
					$cache->setKeyPrefix($aid);
					$cache->save($value, $key);
				}
			}
		}
		
		foreach ( $saved_native as $aid => $data ) {
			foreach ( $data as $key => $value ) {
				
				if ( $value ) {
					$cache_native->set($key, $value, 0, 600);
				}
			}
		}
		
		$cache->setKeyPrefix(false);
		
		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$ses = new Zend_Session_Namespace($sid);
		$ses->unsetAll();

		return 'Cache Cleared';
	}
}
