var config = require('../configs/config.js').get('production');
var http = require('http');
var querystring = require('querystring');

var cache = {};

function getUserInfo(sessionId, callback, useCache)
{
	if ( typeof useCache == 'undefined' ) {
		useCache = true;
	}
	
	currentDate = new Date();
	if ( typeof cache[sessionId] != 'undefined' && ( currentDate - cache[sessionId]['date'] < config.common.sessionCacheTime ) && useCache ) {
		callback(cache[sessionId]['info']);
		return;
	}
	var post_data = querystring.stringify({
		'compilation_level' : 'ADVANCED_OPTIMIZATIONS',
		'output_format': 'json',
		'output_info': 'compiled_code',
		'warning_level' : 'QUIET',
		'sid': sessionId
	});

	var post_options = {
		host: config.common.host,
		port: '80',
		path: config.common.authPath,
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Content-Length': post_data.length
		}
	};
	var post_req = http.request(post_options, function(res) {
		res.setEncoding('utf8');
		res.on('data', function (resp) {
			resp = JSON.parse(resp);
			
			//Setting cache
			cache[sessionId] = {
				info: resp,
				date: new Date()
			};
			callback(resp);
		});
	});

	post_req.write(post_data);
	post_req.end();

}

exports.getUserInfo = getUserInfo;
