/* --> Feedback */
Cubix.Popup = {};


Cubix.Popup.inProcess = false;

Cubix.Popup.url = '';

Cubix.Popup.Show = function (box_height, box_width) {
	if ( Cubix.Popup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', z_index: 200 });
	page_overlay.disable();

	var y_offset = 30;

	var container = new Element('div', { 'class': 'popup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0
	}).inject(document.body);

	Cubix.Popup.inProcess = true;

	new Request({
		url: Cubix.Popup.url,
		method: 'get',
		onSuccess: function (resp) {

			Cubix.Popup.inProcess = false;
			container.set('html', resp);

			validateUsername(container);
			validateEmail(container);

			var close_btn = new Element('div', {
				html: '',
				'class': 'popup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.popup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');

			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.Popup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
		}
	}).send();


	return false;
}

Cubix.Popup.Send = function (e) {
	e.stop();
	function getErrorElement(el) {
		var error = el.getNext('.error');
		if ( error ) return error;

		var target = el;
		//if ( el.get('name') == 'terms' ) {
		//	target = el.getNext('label');
		//}

		return new Element('div', { 'class': 'error' }).inject(target, 'after');
	}

	var overlay = new Cubix.Overlay($$('.popup-wrapper')[0], { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();

	this.get('send').removeEvents('success');
	this.set('send', {
		onSuccess: function (resp) {
			resp = JSON.decode(resp);

			this.getElements('.error').destroy();
			this.getElements('.invalid').removeClass('invalid');

			overlay.enable();

			if ( resp.status == 'error' ) {
				for ( var field in resp.msgs ) {
					var input = this.getElement('*[name=' + field + ']');
					input.addClass('invalid');
					getErrorElement(input).set('html', resp.msgs[field]);
				}
			}
			else if ( resp.status == 'success' ) {

				if ( resp.signin ) {
					window.location.reload();
					return;
				}

				this.getParent().set('html', resp.msg);

				var close_btn = new Element('div', {
					html: '',
					'class': 'popup-close-btn-v2'
				}).inject($$('.popup-wrapper')[0]);

				close_btn.addEvent('click', function() {
					$$('.popup-wrapper').destroy();
					$$('.overlay').destroy();
				});
			}
		}.bind(this)
	});

	this.send();
}


var validateUsername = function(el)
{
	if (el.getElement('#username')){
		el.getElement('#username').addEvent('blur', function () {
			this.removeClass('invalid');
			this.removeClass('valid');

			if ( this.get('value').length < 6 ) {
				this.addClass('invalid');
				this.removeClass('valid');
				return;
			}

			var regex = /^[-_a-z0-9]+$/i;
			if ( ! regex.test(this.get('value')) ) {
				this.addClass('invalid');
				this.removeClass('valid');
				return;
			}

			this.addClass('loading');

			new Request({
				url: '/private/check?username=' + this.get('value'),
				method: 'get',
				onSuccess: function (resp) {
					var resp = JSON.decode(resp);
					this.removeClass('loading');
					if ( resp.status == 'found' ) {
						this.addClass('invalid');
						this.removeClass('valid');
					}
					else if ( resp.status == 'not found' ) {
						this.addClass('valid');
						this.removeClass('invalid');
					}
				}.bind(this)
			}).send();
		});
	}
}

var validateEmail = function(el)
{
	if (el.getElement('#email')){
		el.getElement('#email').addEvent('blur', function ()
		{
			this.removeClass('invalid');
			this.removeClass('valid');

			var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			if ( ! this.get('value').length || ! regex.test(this.get('value') ) ) {
				this.addClass('invalid');
				this.removeClass('valid');
				return;
			}

			this.addClass('loading');

			new Request({
				url: '/private/check?email=' + this.get('value'),
				method: 'get',
				onSuccess: function (resp) {
					var resp = JSON.decode(resp);

					this.removeClass('loading');

					if ( resp.status == 'found' ) {
						this.addClass('invalid');
						this.removeClass('valid');
					}
					else if ( resp.status == 'not found' ) {
						this.removeClass('invalid');
						this.addClass('valid');
					}
					else if ( resp.status == 'domain blacklisted' ) {
						this.addClass('invalid');
						this.removeClass('valid');
					}
				}.bind(this)
			}).send();
		});
	}
}

/* <-- */


/* --> Feedback */
Cubix.ChatPopup = {};

Cubix.ChatPopup.inProcess = false;

Cubix.ChatPopup.url = '';

Cubix.ChatPopup.Show = function (box_height, box_width, el, y_offset, x_offset) {
	if ( Cubix.ChatPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();

	var container = new Element('div', { 'class': 'ChatPopup-wrapper'}).setStyles({
		/*left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset+130,*/
		left: el.getPosition().x + x_offset,
		top: el.getPosition().y - 213 + y_offset,
		opacity: 0,
        position: 'absolute',
        'z-index': 101
        //background: '#fff'
	}).inject(document.body);

	Cubix.ChatPopup.inProcess = true;

	new Request({
		url: Cubix.ChatPopup.url,
		method: 'get',
		onSuccess: function (resp) {

			Cubix.ChatPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'chat-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.ChatPopup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
		}
	}).send();


	return false;
}

/* --> VipMemberPopup */
Cubix.VipMemberPopup = {};

Cubix.VipMemberPopup.inProcess = false;

Cubix.VipMemberPopup.url = '';

Cubix.VipMemberPopup.Show = function (box_height, box_width, y_offset, x_offset) {
	if ( Cubix.VipMemberPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();

	//var y_offset = 30;

	var container = new Element('div', { 'class': 'VipMemberPopup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2 - x_offset,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0
	}).inject(document.body);

	Cubix.VipMemberPopup.inProcess = true;

	new Request({
		url: Cubix.VipMemberPopup.url,
		method: 'get',
		onSuccess: function (resp) {

			Cubix.VipMemberPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'vip-member-close-btn'
			}).inject(container);

			container.getElement('.q-no').addEvent('click', function(e) {
				e.stop();
				$$('.VipMemberPopup-wrapper').destroy();
				page_overlay.enable();
			});

			container.getElement('.q-yes').addEvent('click', function(e) {
				e.stop();

				new Request({
					url: Cubix.VipMemberPopup.url + '?cancel=1',
					method: 'get',
					onSuccess: function (resp) {
						window.location.reload();
					}
				}).send();

				$$('.VipMemberPopup-wrapper').destroy();
				page_overlay.enable();
			});

			close_btn.addEvent('click', function() {
				$$('.VipMemberPopup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
		}
	}).send();


	return false;
}
