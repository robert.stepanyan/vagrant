var Cubix = {};

Cubix.Lang = {};

Cubix.ShowTerms = function (link) {
	window.open(link, 'showTerms', 'menubar=no, resizable=yes, status=no, scrollbars=yes, toolbar=no, width=640, height=480');
	return false;
}

var _log = function (data) {
	if ( console && console.log != undefined ) {
		console.log(data);
	}
}

var _st = function (object) {
	var parts = object.split('.');
	if ( parts.length < 2 ) {
		return _log('invalid parameter, must contain filename with extension');
	}

	var ext = parts[parts.length - 1];
	var protocol = (location.protocol === 'https:') ? 'https' : 'http';
	var base_url = protocol + '://st.6annonce.com',
		prefix = '';

	switch ( ext ) {
		case 'jpg':
		case 'gif':
		case 'png':
			prefix = '/img/';
			break;
		case 'css':
			prefix = '/css/';
			break;
		case 'js':
			prefix = '/js/';
			break;
		default:
			return _log('invalid extension "' + ext + '"');
	}

	url = base_url + prefix + object;

	return url;
}

// Taken from viewed-escorts.js
/* --> Viewed Escorts */
Cubix.Viewed = {};

Cubix.Viewed.url = ''; // Must be set from php
Cubix.Viewed.container = '';

window.addEvent('domready', function() {
	if ( ! $(Cubix.Viewed.container) || ! Cubix.Viewed.url ) return;
	
	new Request({
		url: Cubix.Viewed.url,
		method: 'get',
		onSuccess: function (resp) {
			if(resp.contains('rapper'))
			{
				$(Cubix.Viewed.container).setStyle('opacity', 0);
				var myFx = new Fx.Tween($(Cubix.Viewed.container), {
					duration: 400,
					onComplete: function() {
						$(Cubix.Viewed.container).set('html', resp);
						$(Cubix.Viewed.container).tween('opacity', 0, 1);
					}
				});
				myFx.start('height', '137');
			}
		}
	}).send();
});
/* <-- */


Cubix.EmailCatcher = {};

Cubix.EmailCatcher.Init = function () {
	
	window.addEvent('domready', function() {
		$$('.email-catcher-form').addEvent('submit', function(e) {
			e.stop();
			
			Cubix.EmailCatcher.Send(this);
			
		});
	});
}

Cubix.EmailCatcher.Send = function (el) {
	
	var overlay = new Cubix.Overlay($('newsletter-box'), { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();

	el.set('send', {
		onSuccess: function (resp) {

			overlay.enable();		

			//$$('.em_wrp')[0].set('html', resp);
			
			if( resp.search('success_send') != -1){
				var message = "<span style='padding-left:230px'>You have successfully subscribed !</span>";
				$('newsletter-box').set('html', message);
				Cubix.EmailCatcher.Init();
			}
		}.bind(el)
	});

	el.send();
}


var resizeProfile = function() {
	if ( $$('.cTab')[0] ) {
	
		var leftSide = $('left');
		var profile = $('profile-container');
		var comments = $$('.cTab')[0];

		if ( ! profile.get('rel') ) {
			profile.set('rel', profile.getSize().y);
		}

		var commentsHeight = comments.setStyles({
			visibility: 'hidden',
			display: 'block'
		}).getHeight();

		comments.setStyles({
			visibility: null,
			display: null
		})	

		var profileHeight = 0;

		profileHeight = (profile.get('rel')*1) + (commentsHeight*1) - 4700;


		if ( leftSide.getSize().y > profileHeight ) {
			profile.setStyle('height', left.getSize().y);
		} else {
			profile.setStyle('height', profileHeight);
		}
	} else {
		var $leftHeight = $("left").getCoordinates().height;
		var $profileHeight = $("profile-container").getCoordinates().height - 4600;

		if ( $leftHeight > $profileHeight ) {
			$("profile-container").setStyle('height', $leftHeight);
		} else {
			$("profile-container").setStyle('height', $profileHeight);
		}
	}
};

var initGallery = function() {
	Slimbox.scanPage();
};
