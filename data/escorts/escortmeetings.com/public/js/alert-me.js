window.addEvent('domready', function(){
	var page_overlay = new Cubix.Overlay(document.body, {has_loader: false, opacity: 0.5, color: '#000', z_index: 100});
	var escort_id = $('escort_id').get('value');
	
	if ($('alm-bg'))
	{
		var alertme = $('alm-bg');
		$('alm-btn').removeEvents();
		$('alm-btn').addEvent('click', function(e){
			e.stop();

			if ( alertme.hasClass('none') ) {
				page_overlay.disable();
				
				alertme.removeClass('none');
			}
			else {
				page_overlay.enable();
				alertme.addClass('none');
			}
		});
		
		$$('.alm-x').addEvent('click', function(e) {
			e.stop();

			page_overlay.enable();
			alertme.addClass('none')
		});
	}
	
	if ($('alm-bg-m'))
	{
		var alertme_m = $('alm-bg-m');
		$$('.alm-btn-m').removeEvents();
		$$('.alm-btn-m').addEvent('click', function(e){
			e.stop();

			if ( alertme_m.hasClass('none') ) {
				page_overlay.disable();
				
				var req = new Request({
					method: 'get',
					url: '/escorts/ajax-alerts?escort_id=' + escort_id,
					onComplete: function(response) {
						var object = JSON.decode(response);
						
						if (object.join(',') == '1,2,3,4')
						{
							$('alm-5').set('checked', 'checked');
						}
						else
						{
							object.each(function(el) {
								$('alm-' + el).set('checked', 'checked');
							})
						}
						
						//
						checkCHB();
						alertme_m.removeClass('none');
					}
				}).send();
			}
			else {
				page_overlay.enable();
				alertme_m.addClass('none');
			}
		});
		
		$$('.alm-x').addEvent('click', function(e) {
			e.stop();

			page_overlay.enable();
			alertme_m.addClass('none');
		});
		
		$('alm-5').addEvent('change', function(e) {
			e.stop();

			checkCHB();
		});
		$('alm-save').removeEvents();
		$('alm-save').addEvent('click', function(e) {
			e.stop();
			
			var i = 0;
			var events = Array();
			
			$$("#alertme_form input[type=checkbox]:checked").each(function(el)
			{
				events[i] = el.get('id').substr(4, 1);
				i++;
			});
			
			/*var alert_overlay = new Cubix.Overlay($('alm-bg-m'), {
				loader: _st('loader-small.gif'),
				position: '50%',
				opacity: 0.5, 
				color: '#FFF', 
				z_index: 900
			});
			alert_overlay.disable();*/

			var ev = events.join(',');
			
			if (ev == '5')
				ev = '1,2,3,4';
			
			var req = new Request({
				method: 'get',
				url: '/escorts/ajax-alerts-save?escort_id=' + escort_id + '&events=' + ev,
				onComplete: function(response) {
					//alert_overlay.enable();
					var object = JSON.decode(response);
					if (object.count > 0)
					{
						if ($('alm-b-a').hasClass('none') == true)
							$('alm-b-a').removeClass('none');
						if ($('alm-b').hasClass('none') == false)
							$('alm-b').addClass('none');
					}
					else
					{
						if ($('alm-b-a').hasClass('none') == false)
							$('alm-b-a').addClass('none');
						if ($('alm-b').hasClass('none') == true)
							$('alm-b').removeClass('none');
					}
					
					page_overlay.enable();
					alertme_m.addClass('none');
				}
			}).send();
		});
	}
});

function checkCHB()
{
	if ($$('input[name=any]').get('checked') == 'true')
	{
		$$('input[name=comment]').set('checked', '');
		$$('input[name=comment]').set('disabled', 'disabled');
		$$('input[name=profile]').set('checked', '');
		$$('input[name=profile]').set('disabled', 'disabled');
		$$('input[name=pictures]').set('checked', '');
		$$('input[name=pictures]').set('disabled', 'disabled');
		$$('input[name=review]').set('checked', '');
		$$('input[name=review]').set('disabled', 'disabled');
	}
	else
	{
		$$('input[name=comment]').set('disabled', '');
		$$('input[name=profile]').set('disabled', '');
		$$('input[name=pictures]').set('disabled', '');
		$$('input[name=review]').set('disabled', '');
	}
}
/* <-- */
