/* --> Photos */
Cubix.Bubble = {};

Cubix.Bubble.container = 'bubbles-widget';

Cubix.Bubble.Load = function (page) {
	if ( undefined == page || page < 1 ) page = 1;
	var url = '/ajax-bubble?page=' + page;
	Cubix.Bubble.Show(url);
	
	return false;
}

Cubix.Bubble.Show = function (url) {
	var overlay = new Cubix.Overlay($(Cubix.Bubble.container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			$(Cubix.Bubble.container).set('html', resp);
			overlay.enable();
		}
	}).send();
}
