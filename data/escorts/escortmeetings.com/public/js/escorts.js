/* --> Escorts */
Cubix.Escorts = {};

Cubix.Escorts.url = '/escorts'; // Must be set from php
Cubix.Escorts.loaded = false; // Must be set from php

Cubix.Escorts.GetRequestUrl = function (url) {
	//var uri = new URI(document.location.href);
	var uri = window.location.href;

	uri = uri.replace(/\/$/, '');

	url = uri + '/' + url;

	if ( -1 == url.indexOf('?') ) {
		url += '?';
	}
	else {
		url += '&';
	}

	url += 'ajax';

	if ( url == '/escorts/?ajax' ) url = '?ajax'; //TO AVOID REDIRECTS IN FILTERING

	return url;
};

Cubix.Escorts.Load = function (url, data, callback) {
	if(Cubix.Escorts.loaded == false){
		return true;
	}
	this.container = $$('#page > .i')[0];
	
	var overlay = new Cubix.Overlay(this.container, {});
	overlay.disable();

	url = Cubix.Escorts.GetRequestUrl(url);

	var myScroll = new Fx.Scroll(document.getElement('body'));

	new Request({
		url: url,
		method: 'post',
		data: data,
		/*evalScripts:true,*/
		onSuccess: function (resp) {
			this.container.set('html', resp);

			/*if ( $defined($$('.right_premium_escorts')[0]) ) {
				var right_premium_escorts = $$('.right_premium_escorts')[0].get('html');
				var right_block = $$('.right_premiums_block_p')[0];
				var right_block_body = right_block.getElement('#right_premiums_block');

				right_block_body.set('html', '');

				if ( right_premium_escorts.length > 10) {
					right_block_body.set('html', right_premium_escorts);
					right_block.removeClass('none');
					
					right_block.tween('opacity', [0, 1]);
					$$('.right_premium_escorts')[0].set('html', '');
				}
				else {
					right_block.addClass('none');
					
				}
			}*/
			
			if ( $defined($$('.big-red-btn-wrapper')[0]) ) {
				$$('.big-red-btn-wrapper')[0].destroy();
			}
			if ( $defined($$('.bubble')[0]) ) {
				$$('.bubble')[0].destroy();
			}
			
			if ( $defined(Cubix.PhotoRoller) ) {
				Cubix.PhotoRoller.Init();
			}
			
			Cubix.Tip.Init($$('.availability img'));

			//myScroll.toTop();

			Cubix.EscortHover.Init();
			Cubix.ListSwitcher.Init();
			Cubix.Filter.InitSorting();

			overlay.enable();
			
			if ( $('sort-select') ) {
				$('sort-select').fancySelect({
					showText: true,
					showImages: false,
					legacyEvents: true
				});
			}
			
			if ( $defined(callback) ) {
				callback.call(this, resp);
			}
		}.bind(this)
	}).send();
	
	return false;
}
/* <-- */

/* --> Tool Tip */
Cubix.Tip = {};

Cubix.Tip.Init = function (elements) {
	var els = [];
	elements.each(function(element) {
		if ( element.get('title') )
		{
			var content = element.get('title').split('::');
			element.store('tip:title', content[0]);
			element.store('tip:text', content[1]);
			els.include(element);
		}
	});
	
	var tip = new Tips($$(els));
};
/* <-- */

/* --> Filter */
Cubix.Filter = {};

Cubix.Filter.Change = function (filter) {
	var els = [];
	
	if ( $defined(filter.filter) ) {
		els.include({ el: $('filter-options'), value: filter.filter });
	}
	
	if ( $defined(filter.sort) ) {
		els.include({ el: $('sort-options'), value: filter.sort });
	}
	
	if ( ! els.length ) return;
	
	els.each(function (el) {
		var value = el.value;
		el = el.el;
		
		var selected = el.getPrevious('.input-w').getElement('.i a.first');
		var options = el.getElements('a').filter(function (el) { return ! el.hasClass('sub') });
		var sel = el.getElements('a').filter(function (el) { return el.getStyle('display') == 'none' })[0];
		
		options.each(function (opt) {
			if ( value == opt.get('rel') ) {
				var title = opt.get('html');
				
				if ( ! opt.getParent('li').getParent('li').getParent('ul').hasClass('nav') ) {
					title = opt.getParent('li').getParent('li').getElement('a').get('html') + '-' + title;
				}
				
				opt.clone().setStyle('display', 'block').set('html', title).addClass('first').replaces(selected);
				if ( sel ) {
					sel.setStyle('display', null);
				}
				opt.setStyle('display', 'none');
				
				options.empty();
			}
		});
	});
}

Cubix.Filter.InitSearchInput = function (input) {
	Cubix.Filter.InitSearchInput.input = $(input);
	
	this.timer = null;
	
	if ( $defined(input) ) {
		input.addEvents({
			keyup: function () {
				$clear(this.timer);
				this.timer = setTimeout('Cubix.Filter.InitSearchInput.KeyUp(Cubix.Filter.InitSearchInput.input)', 500);
			}.bind(this)
		});
	}
}

Cubix.Filter.InitSearchInput.KeyUp = function (input) {
	Cubix.Filter.Set({ name: input.get('value').length ? input.get('value') : null });
}

Cubix.Filter.Set = function (filter, clear, el, c_menu) {
	
	if ( ! clear ) {
		this.filter = $merge(this.filter, filter);
	}
	else {
		this.filter = filter;
	}
	
	var f = this.filter;
	
	if ( el ) {
	
		$$('#quick-links a').each(function(it) {
			
			if ( it.get('class') == el.get('class') && el.hasClass('active')) {
				el.removeClass('active');
				if ( ! clear ) {
					Cubix.LocationHash.Set('');
				}

				window.fireEvent('escortsFilterChange', {});
			}
			else if ( it.get('class') == el.get('class') && ! el.hasClass('active')) {
				el.addClass('active');
				if ( ! clear ) {
					Cubix.LocationHash.Set(Cubix.LocationHash.Make(f));
				}

				window.fireEvent('escortsFilterChange', {});
			}
			
			if ( it.get('class') != el.get('class') ) {
				it.removeClass('active');
			}
		});
	
	}
	else {
		if ( ! clear ) {
			Cubix.LocationHash.Set(Cubix.LocationHash.Make(this.filter));
		}

		window.fireEvent('escortsFilterChange', {});
	}

	return false;
}
/* <-- */

/* --> LocationHash */
Cubix.LocationHash = {};

Cubix.LocationHash.Set = function (hash) {
	if ( hash.length ) {
		document.location.hash = '#' + hash;
	}
	else {
		document.location.hash = '';
	}
	
	return false;
}

Cubix.LocationHash.Parse = function () {
	var hash = document.location.hash.substring(1);
	
	if ( ! hash.length ) {
		return {};
	}
	
	var params = hash.split(';');
	
	var filter = '';
	
	var not_array_params = ['page', 'sort', 'reg', 'name', 'segmented', 'e_id'];
	
	params.each(function (param) {
		var key_value = param.split('=');
		
		var key = key_value[0];
		var val = key_value[1];
		
		if ( val === undefined ) return;
		
		val = val.split(',');
				
		val.each(function(it) {
			filter += key + ( ( ! not_array_params.contains(key) ) ? '[]=' : '=' ) + it + '&';
		});
	});
	
	return filter;
}

Cubix.Filter.InitSorting = function() {

	var sorting_bar = $('sorting-box');
	var sort_select = sorting_bar.getElements('select[name=sort]');


	sort_select.addEvent('change', function() {
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
};

Cubix.LocationHash.Make = function (filter) {
	Cubix.Escorts.loaded = true;
	var hash = '';

	var sorting_bar = $('sorting-box');
	var sort_select = sorting_bar.getElements('select[name=sort]');

	for ( var key in filter ) {
		var value;
		eval('value = filter.' + key + ';');
		
		if ( value == null ) continue;
		
		hash += ';' + key;
		
		if ( value.length ) {
			hash +=  '=' + value;
		}
	}

	hash = hash.substring(1);
	
	if(sort_select){
		hash += ';sort=' + sort_select.getSelected()[0].get('value');
	}
	
	return hash;
}
/* <-- */

/* --> HashController */
Cubix.HashController = {
	_current: '',
	
	init: function () {
		setInterval('Cubix.HashController.check()', 300);
	},
	
	check: function () {
		var hash = document.location.hash.substring(1);
		
		if (hash != this._current) {
			this._current = hash;
			
			var data = Cubix.LocationHash.Parse();

			//Cubix.Escorts.Load(hash, Cubix.HashController.Callback);
			Cubix.Escorts.Load(hash,data);// Cubix.HashController.Callback);
		}
	}
};

window.addEvent('domready', function(){
	Cubix.HashController.init();

	if ( $('sort-select') ) {
		$('sort-select').fancySelect({
			showText: true,
			showImages: false,
			legacyEvents: true
		});
	}

	Cubix.Filter.InitSorting();
});

Cubix.HashController.Callback = function () {
	Cubix.Filter.Change(Cubix.LocationHash.Parse());
	Cubix.Filter.Set(Cubix.LocationHash.Parse(), true);
}
/* <-- */

Cubix.EscortHover = {};

Cubix.EscortHover.Init = function () {
	
	window.addEvent('domready', function() {
		$$('.h').addEvent('mouseenter', function() {
			/*this.setStyles({
				border: '2px solid #c8e7ff',
				'padding': null,
				'background-image': 'url("' + _st('v2.1/escort_hover_bg.png') + '")',
				'background-repeat': 'repeat-x'
			});*/
			this.removeClass('esc-hover-out');
			this.addClass('esc-hover');			
		});

		$$('.h').addEvent('mouseleave', function() {
			/*this.setStyle('border', '2px solid transparent');
			this.setStyle('background-image', 'none');*/
			this.removeClass('esc-hover');
			this.addClass('esc-hover-out');
		});

		$$(".wrap .diamond ,.wrap .premium, .wrap .p100s, .wrap .new").addEvent("click", function(event) {
			var sib = this.getNext(["a"]);
			var link = sib.getProperty("href");
			window.location = link;
		});

		$$('.comment_icon').addEvent('mouseenter', function(){
		var self = this;
		var commentCount = self.getNext('.comment_count').get('text');
		tooltip.show(commentCount +' '+ Cubix.CommentTip);

		});

		$$('.comment_icon').addEvent('mouseleave', function(){
			tooltip.hide();
		});

		$$('.review_icon').addEvent('mouseenter', function(){
			var self = this;
			var reviewCount = self.getNext('.review_count').get('text');
			tooltip.show(reviewCount +' '+ Cubix.ReviewTip);

		});

		$$('.review_icon').addEvent('mouseleave', function(){
			tooltip.hide();
		});
	});
	
}


Cubix.ListSwitcher = {};
Cubix.ListSwitcher.Init = function () {

var reorderEscortsToGrid = function() {
	
	$$('.gcl').destroy();
	
	if ($('premium'))
		$('premium').setStyle('padding-left', '10px');
	
	//$$('.escorts')[0].getParent('.i').setStyle('padding', '0 18px 20px 18px');
	var thumbs = $$('.escorts')[0].getElements('div.h');
	
	var escorts = [];
	var premium_escorts = [];
	var gotd_escort = [];
	var vip_escort = [];
	
	thumbs.each(function(thumb) {
		if ( thumb.hasClass('prem') ) {
			premium_escorts.append([thumb]);
		} else if ( thumb.hasClass('gotd-thumb') ) {			
			gotd_escort.append([thumb]);
		} else if ( thumb.hasClass('vip') ) {
			vip_escort.append([thumb]);
		} else {
			escorts.append([thumb]);
		}
		thumb.destroy();
	});

	escorts = premium_escorts.append(escorts);
	escorts = gotd_escort.append(escorts);
	
	if ( $('vip-box') ) {
		var vip_box = $('vip-box');

		if ( vip_escort.length ) {
			vip_escort.each(function(it){
				it.inject(vip_box, 'bottom');
			});
		}
	}
	
	$$('.escorts div.r_new').destroy();
	
	$$('.escorts')[0].getLast('div.row').addClass('last');
	
	var rows = $$('.escorts')[0].getElements('div.row');
	var i = 0;
	rows.each(function(row) {		
		for(d = 0; d < 6; d++) {
			if ( typeof escorts[i] != 'undefined' ) {
				escorts[i].inject(row, 'bottom');
				i++;
			}
		}		
	});
	
	/*if ( premium_escorts.length ) {
		var premium_box = $('premium');
		premium_escorts.each(function(esc){
			esc.inject(premium_box, 'bottom');
		});
		new Element('div', {'class':'clear'}).inject(premium_box, 'bottom');
	}*/
	
	if ( $$('.gotd-thumb')[0] ) {
		$$('.gotd-thumb').each(function(it){
			var img = it.getElements('img')[0];
			if (Browser.ie){
				var src = img.href;
			} else {
				var src = img.get('src');
			}
			src = src.replace("_gotm_xl", "_gotm");
			img.set('src', src);
			img.set('width', 158);
			img.set('height', 99);
		});
	}
	
	$$('img.escort-thumb').each(function(it) {
		if (Browser.ie){
			src = it.href.replace("_xl_thumb", "_thumb");
		} else {
			src = it.get('src').replace("_xl_thumb", "_thumb");
		}		

		it.set('src', src);
	});
	
	Cubix.PhotoRoller.thumb = 'thumb';
	Cubix.PhotoRoller.Init();
	
	$$('.escorts')[0].getElements('div.row').each(function(el){
		if ( ! el.getElements('.escort').length ) {
			el.destroy();
		}
	});
};

var reorderEscortsToXL = function(from_click) {
	
	if ( ! from_click ) {
		from_click = false;
	}
	
	$$('.gcl').destroy();
	
	//$$('.escorts')[0].getParent('.i').setStyle('padding', '0 0 20px 0');
	if ($('premium'))
		$('premium').setStyle('padding-left', '30px');
	var rows = $$('.escorts div.row');
	var rows_count = $$('.escorts div.row').length;
	
	var thumbs_count = 0;
	rows.each(function(row) {
		thumbs_count = thumbs_count + row.getElements('div.h').length;
	});
	//console.log(thumbs_count);
	var total_row_count = Math.ceil(thumbs_count / 3);
	
	for ( i = 0; i < (total_row_count - rows_count); i++ ) {
		n_row = new Element('div', {'class':'row r_new'});
		n_row.inject($$('.escorts')[0], 'bottom');
	}
	
	var rows = $$('.escorts div.row');
	rows.each(function(it, key) {
		var escorts = rows[key].getElements('div.h');
	
		if ( escorts.length >= 6 ) {
			var k = key + 1;
			
			escorts.each(function(it, i){
				if ( i >= 3 ) {
					if ( rows[k] ) {
						//it.addClass('e_new');
						rows[k].grab(it, 'top');
						
						if ( k == rows_count - 1 ) {
							rows[k].removeClass('last');
						}
						
						if ( k == total_row_count - 1 ) {
							rows[k].addClass('last');
						}
					}
				}
			});
		}
	});
	
	
	if ( from_click ) {
		if ( $$('.gotd-thumb')[0] ) {
			$$('.gotd-thumb').each(function(it) {
				var img = it.getElements('img')[0];
				if (Browser.ie){
					var src = img.href;
				} else {
					var src = img.get('src');
				}
				src = src.replace("_gotm", "_gotm_xl");
				img.set('src', src);
				img.set('width', 297);
				img.set('height', 338);
			});
		}
		
		$$('img.escort-thumb').each(function(it){
			if (Browser.ie){
				var src = it.href;
			} else {
				var src = it.get('src');
			}
			
			if ( ! src.test('/_xl_thumb/') ) {
				src = src.replace("_thumb", "_xl_thumb");

				it.set('src', src);
			}
		});
	}
	
	Cubix.PhotoRoller.thumb = 'xl_thumb';
	//Cubix.PhotoRoller.Clear();
	Cubix.PhotoRoller.Init();
}

window.addEvent('domready', function() {
	if ($defined($$('.go-top')))
	{
		$$('.go-top').addEvent('click', function(){
			var myFx = new Fx.Scroll(window,{
				offset: {
					'x': 0,
					'y': -5
				}
			});
			myFx.toElement('header');
		});
	}
	
	if ( $$('.escorts')[0].hasClass('xl') ) {
		reorderEscortsToXL(false);
	}
	
	if ( $defined($$('.list_grid_switcher')) ) {
		$$('.list_grid_switcher a').addEvent('click', function(e){
			e.stop();
			
			var cont = $$('.gl_sw')[0];
			
			if ( this.hasClass('grid_btn') ) {
				
				$$('.gcl').destroy();
				
				if ( $$('.escorts')[0].hasClass('xl') ) {
					reorderEscortsToGrid();
				}
				
				/*$$('img.escort-thumb').each(function(it){
					var src = it.get('src');
					
					src = src.replace("_xl_thumb", "_thumb");
					
					it.set('src', src);
				});*/
				
				cont.removeClass('list');
				this.removeClass('grid_btn');
				cont.removeClass('xl');
				this.addClass('grid_btn_act');
				
				this.getParent('div.list_grid_switcher').getElements('a.list_btn_act').removeClass('list_btn_act').addClass('list_btn');
				this.getParent('div.list_grid_switcher').getElements('a.xl_btn_act').removeClass('xl_btn_act').addClass('xl_btn');
				
				Cookie.write('list_type', 'grid', {domain: '.escortmeetings.com', duration: 30, path: '/'});
			}
			
			if ( this.hasClass('grid_btn_act') ) {
				return;
			}
			
			if ( this.hasClass('list_btn') ) {
				if ( $$('.escorts')[0].hasClass('xl') ) {
					reorderEscortsToGrid();
				}
				
				if ( $$('.gotd-thumb')[0] ) {
					var last_gotd = $$('.gotd-thumb').getLast();
					new Element('div', {
						'class': 'clear gcl'
					}).inject(last_gotd, 'after');
				}
				
				//$$('.escorts')[0].getParent('.i').setStyle('padding', '0 18px 20px 18px');
				/*$$('img.escort-thumb').each(function(it){
					var src = it.get('src');
					
					src = src.replace("_xl_thumb", "_thumb");
					
					it.set('src', src);
				});*/
				cont.removeClass('xl');
				cont.addClass('list');
				this.removeClass('list_btn');
				this.addClass('list_btn_act');
				
				this.getParent('div.list_grid_switcher').getElements('a.grid_btn_act').removeClass('grid_btn_act').addClass('grid_btn');
				this.getParent('div.list_grid_switcher').getElements('a.xl_btn_act').removeClass('xl_btn_act').addClass('xl_btn');
				
				Cookie.write('list_type', 'list', {domain: '.escortmeetings.com', duration: 30, path: '/'});
			}
			
			if ( this.hasClass('list_btn_act') ) {
				return;
			}
			
			if ( this.hasClass('xl_btn') ) {
				
				reorderEscortsToXL(true);
				
				/*$$('img.escort-thumb').each(function(it){
					var src = it.get('src');
					
					src = src.replace("_thumb", "_xl_thumb");
					
					it.set('src', src);
				});*/
				
				cont.removeClass('list');
				cont.addClass('xl');
				this.removeClass('xl_btn');
				this.addClass('xl_btn_act');
				
				this.getParent('div.list_grid_switcher').getElements('a.grid_btn_act').removeClass('grid_btn_act').addClass('grid_btn');
				this.getParent('div.list_grid_switcher').getElements('a.list_btn_act').removeClass('list_btn_act').addClass('list_btn');
				
				Cookie.write('list_type', 'xl', {domain: '.escortmeetings.com', duration: 30, path: '/'});
			}
			
			if ( this.hasClass('xl_btn_act') ) {
				return;
			}
		});
	}
	
	if ($defined($$('.chat_login')))
	{
		$$('.chat_login').addEvent('click', function(e) {
			e.stop();
			
			Cubix.Popup.Show('500', '520');
		});
	}
	
	new ScrollSpy({
		min: 1,
		max: 100000,
		onEnter: function(position,state,enters) {
			//var dx = (window.getSize().x - $('container').getSize().x) / 2 + ($('container').getSize().x - $('wrapper').getSize().x) + 21;
			$$('.go-top').set('styles', {
				position: 'fixed',
				bottom: '25px',
				right: '10px',
				'z-index': '100'
			});
		},
		onLeave: function(position,state,leaves) {
			$$('.go-top').set('style', '');
		},
		container: window
	});
});

}

Cubix.ListSwitcher.Init();