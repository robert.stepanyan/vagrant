/* --> Feedback */
Cubix.Popup = {};


Cubix.Popup.inProcess = false;

Cubix.Popup.url = '';

Cubix.Popup.Show = function (box_height, box_width) {
	if ( Cubix.Popup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', z_index: 200 });
	page_overlay.disable();

	var y_offset = 30;

	var container = new Element('div', { 'class': 'popup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0
	}).inject(document.body);
	
	Cubix.Popup.inProcess = true;

	new Request({
		url: Cubix.Popup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.Popup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'popup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.popup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
			
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.Popup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
		}
	}).send();
		
	
	return false;
}

Cubix.Popup.Send = function (e) {
	e.stop();
	function getErrorElement(el) {
		var error = el.getNext('.error');
		if ( error ) return error;

		var target = el;
		if ( el.get('name') == 'terms' ) {
			target = el.getNext('label');
		}

		return new Element('div', { 'class': 'error' }).inject(target, 'after');
	}

	var overlay = new Cubix.Overlay($$('.popup-wrapper')[0], { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();
	
	this.set('send', {
		onSuccess: function (resp) {
			resp = JSON.decode(resp);

			this.getElements('.error').destroy();
			this.getElements('.invalid').removeClass('invalid');

			overlay.enable();
			
			if ( resp.status == 'error' ) {
				for ( var field in resp.msgs ) {
					var input = this.getElement('*[name=' + field + ']');
					input.addClass('invalid');
					getErrorElement(input).set('html', resp.msgs[field]);
				}
			}
			else if ( resp.status == 'success' ) {

				if ( resp.signin ) {
					window.location.reload();
					return;
				}

				this.getParent().set('html', resp.msg);

				var close_btn = new Element('div', {
					html: '',
					'class': 'popup-close-btn-v2'
				}).inject($$('.popup-wrapper')[0]);

				close_btn.addEvent('click', function() {
					$$('.popup-wrapper').destroy();
					$$('#page .overlay').destroy();
				});
			}
		}.bind(this)
	});
	
	this.send();
}

/* <-- */
