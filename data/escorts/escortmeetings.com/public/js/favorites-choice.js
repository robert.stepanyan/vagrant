Cubix.Choice = {};
Cubix.Choice.container = 'ch-l';
Cubix.Choice.url = '/members/choice-list';
Cubix.Choice.urlRank = '/members/choice-rank';
Cubix.Choice.urlComment = '/members/choice-comments';
Cubix.Choice.R = '';
Cubix.Choice.userId = '';
Cubix.Choice.Msg = '';

Cubix.Choice.Init = function (msg, userId) {
	Cubix.Choice.userId = userId;
	Cubix.Choice.Msg = msg;
	Cubix.Choice.InitSearchInput('showname');
	Cubix.Choice.Show({page: 1});
	
	return false;
}

Cubix.Choice.InitSearchInput = function (input) {
	Cubix.Choice.InitSearchInput.input = $(input);
	
	this.timer = null;
	
	Cubix.Choice.InitSearchInput.input.addEvents({
		keyup: function () {
			$clear(this.timer);
			this.timer = setTimeout('Cubix.Choice.InitSearchInput.KeyUp(Cubix.Choice.InitSearchInput.input)', 500);
		}.bind(this)
	});
}

Cubix.Choice.InitSearchInput.KeyUp = function (input) {
	Cubix.Choice.Show({page: 1});
}

Cubix.Choice.InitTips = function () {
	$$('.show-rank').removeEvents('mouseenter');
	$$('.show-rank').removeEvents('mouseleave');
	$$('.show-rank').addEvent('mouseenter', function() {
		Cubix.Choice.ShowTip(this);
	});
	
	$$('.show-rank').addEvent('mouseleave', function() {
		Cubix.Choice.RemoveTip();
	});
}

Cubix.Choice.ShowTip = function(el) {	
	var rt = new Element('div', {'class': 'rank-tip', 'id': 'r' + el.get('rank') + '-' + el.get('escort_id')});
	var con = new Element('div', {'class': 'cont'}).inject(rt);
			
	rt.inject(el, 'top');
	
	var overlay = new Cubix.Overlay(con, {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 21,
			top: 13,
			right: -21,
			bottom: -12
		}
	});
	
	overlay.disable();
	
	Cubix.Choice.R = new Request({
		url: Cubix.Choice.urlRank,
		method: 'get',
		data: {
			escort_id: el.get('escort_id'), 
			rank: el.get('rank')
		},
		onSuccess: function (resp) {			
			overlay.enable();
			con.setStyles({'overflow-y': 'auto', 'overflow-x': 'hidden'});
			con.set('html', resp);
		}
	}).send();
}

Cubix.Choice.RemoveTip = function() {
	Cubix.Choice.R.cancel();
	
	$$('.rank-tip').destroy();
}

Cubix.Choice.Show = function(data) {
	var container = Cubix.Choice.container;
	var overlay = new Cubix.Overlay($(container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	var ret = false;
	
	overlay.disable();
	
	data.sort_by = $('sort_by').getSelected()[0].get('value');
	
	if ($('showname').get('value').length > 0)
		data.showname = $('showname').get('value');
	
	if ($('city') && $('city').getSelected()[0].get('value').length > 0)
		data.city = $('city').getSelected()[0].get('value');
	
	if ($('with_comments').get('checked'))
		data.with_comments = 1;
	
	if ($('active_girls').get('checked'))
		data.active_girls = 1;
	
	if ($('show').getSelected()[0].get('value').length > 0)
	{
		data.show = $('show').getSelected()[0].get('value');
		
		if (data.show == 'most_girls' || data.show == 'gainers' || data.show == 'loosers' || data.show == 'top1_only')
		{
			$('sort_by').set('disabled', 'disabled');
		}
		else
		{
			$('sort_by').set('disabled', '');
		}
	}
	else
	{
		$('sort_by').set('disabled', '');
	}
		
	new Request({
		url: Cubix.Choice.url,
		method: 'get',
		data: data,
		onSuccess: function (resp) {			
			$(container).set('html', resp);
			
			overlay.enable();
			
			Cubix.Choice.InitTips();
		}
	}).send();
	
	if (data.ret == true)
		ret = data.ret;
	
	return ret;
}

Cubix.Choice.Comments = function(el, escort_id) {
	if ($defined($$('.comment-popup')))
	{
		$$('.comment-popup').destroy();
	}
	
	var com = new Element('div', {'class': 'comment-popup', 'id': 'c-' + escort_id});
	var con = new Element('div', {'class': 'cont'}).inject(com);
	var par = el.getParent();
	
	com.inject(par, 'top');
	
	var overlay = new Cubix.Overlay(con, {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 21,
			top: 13,
			right: -21,
			bottom: -12
		}
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Choice.urlComment,
		method: 'get',
		data: {
			escort_id: escort_id
		},
		onSuccess: function (resp) {			
			overlay.enable();
			con.setStyles({'overflow-y': 'auto', 'overflow-x': 'hidden'});
			con.set('html', resp);
			
			Cubix.Choice.SendRequest();
			
			$$('.close').addEvent('click', function(e) {
				e.stop();
				
				Cubix.Choice.CommentsClose();
			})
		}
	}).send();
	
	return false;
}

Cubix.Choice.CommentsClose = function() {
	$$('.comment-popup').destroy();
}

Cubix.Choice.SendRequest = function() {
	$$('.request-access').addEvent('click', function(e) {
		e.stop();

		var self = this;
		
		if (!Cubix.Choice.userId)
		{
			Cubix.Popup.Show('489', '652');
			return false;
		}

		var f_id = self.get('id');
		var ov_cont = self.getParent().getParent().getParent();
		var overlay = new Cubix.Overlay(ov_cont, {
			loader: _st('loader-small.gif'),
			position: '50%',
			offset: {
				left: 21,
				top: 13,
				right: -21,
				bottom: -15
			}
		});

		overlay.disable();

		var data = {
			f_id: f_id
		};

		new Request.JSON({
			url: '/private/send-fav-request',
			method: 'post',
			data: data,
			onSuccess: function (resp) {
				if (resp.success) {
					self.getParent().set('html', Cubix.Choice.Msg);
					overlay.enable();
				}
			}
		}).send();
	});
}