<?php
/*
+---------------------------------------------------------------------------+
| humpchies.ca Routes                            				            |
| ===================                                                       |
|                                                                           |
| Copyright (c) Ivan Manzanilla                                             |
|                                                                           |
|   Email:              ivan_manzanilla@videotron.ca                        |
|   Phone:              514-882-2800                                        |
|   Fax:                n/a                                                 |
|   Mail Address:       574 Beaurepaire                                     |
|                       Beaconsfield, Quebec, Canada                        |
|                       H9W 3E5                                             |
|                                                                           |
| This program is NOT free software; you can NOT redistribute it and/or     |
| modify it, unless explicitly authorized by Ivan Manzanilla:               |
|                                                                           |
| This program is distributed in the hope that it will be useful, but       |
| WITHOUT ANY WARRANTY; without even the implied warranty of                |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      |
|                                                                           |
| DESCRIPTION: This file declares one array to define the routes 		    |
| (controller/action combos) that are available through the site. All       | 
| routes are mapped. The keys are actual URL components, the values are the |
| Controller action combinations.								            |
|										                                    |
| HISTORY:                                                                  |
|                                                                           |
|                   December 7, 2014: [ymanzanilla] Creation Putous!        |
|                                                                           |
+---------------------------------------------------------------------------+
*/

// ==========================================================================
// ROUTES
// ==========================================================================   
$config = array(    ''                              => array('\Modules\Humpchies\RedirectionController::siteRoot'),
                    'index.php'                     => array('\Modules\Humpchies\RedirectionController::siteRoot'),
                    '404'                           => array('\Modules\Humpchies\RedirectionController::notFound'));?>