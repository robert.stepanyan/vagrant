<?php

abstract class Cubix_Model_Item extends ArrayObject
{
	protected $_idField = 'id';
	
	/**
	 * Database adapter object
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $_adapter;
	
	protected $_throwExceptions = false;
	
	public function setAdapter(Zend_Db_Adapter_Abstract $adapter)
	{
		$this->_adapter = $adapter;
	}
	
	/**
	 * An emergency function
	 * 
	 * @return Zend_Db_Adapter_Abstract
	 */
	public static function getAdapter()
	{
		return Cubix_Model::getAdapter();
	}
	
	public function __construct($rowData = array())
	{
		foreach ((array) $rowData as $key => $value) {
			$this->$key = $value;
		}
		
		$this->setAdapter(Zend_Registry::get('db'));
	}
	
	public function __get($key)
	{
		if ( ! isset($this[$key]) ) {
			if ( $this->_throwExceptions ) {
				throw new Exception('Unknown property "' . $key . '"');
			}
			else {
				return null;
			}
		}
		
		return $this[$key];
	}
	
	public function __set($key, $value)
	{
		$this[$key] = $value;
	}
	
	public function __isset($key)
	{
		return isset($this[$key]);
	}
	
	public function __unset($key)
	{
		unset($this[$key]);
	}
	
	public function __call($meth, $args)
	{
		$a = array();
		if ( substr($meth, 0, 3) !== 'get' && substr($meth, 0, 3) !== 'set' ) {
			throw new Exception('Unknown method "' . $meth . '"');
		}
		
		preg_match_all('/^(get|set)(.+)$/', $meth, $a);
		
		array_shift($a);
		
		$meth = reset($a[0]);
		$key = preg_replace('/([A-Z])/', '_$1', reset($a[1]));
		$key = strtolower(trim($key, '_'));
		
		if ( $meth == 'get' ) {
			return $this->$key;
		}
		else {
			if ( count($args) == 0 ) {
				throw new Exception('The setter requires the value');
			}
			
			$this->$key = $args[0];
		}
	}
	
	public function getId()
	{
		return isset($this[$this->_idField]) ? $this[$this->_idField] : null;
	}
	
	public function getData($fields = null)
	{
		if ( is_null($fields) ) {
			$data = (array) $this;
			
			if ( isset($data[$this->_idField]) ) {
				unset($data[$this->_idField]);
			}
			
			return $data;
		}
		
		$data = array();
		foreach ($fields as $field) {
			$data[$field] = $this->$field;
		}
		return $data;
	}

	public function setThrowExceptions($flag)
	{
		$this->_throwExceptions = $flag;
	}
}
