<?php

class Cubix_Utils
{
	public static function makeSlug($str)
	{
		$slug = preg_replace('/[^-a-z0-9]/i', '-', $str);
		while ( FALSE !== strpos($slug, '--') ) {
			$slug = str_replace('--', '-', $slug);
		}
		$slug = trim($slug, '-');
		$slug = strtolower($slug);
		
		return $slug;
	}

	static function arrToObj($array) {
		if ( ! is_array($array) ) {
			return $array;
		}

		$object = new stdClass();
		foreach ( $array as $name => $value ) {
			if ( ! empty($name) ) {
				$object->$name = self::arrToObj($value);
			}
		}

		return $object;
	}

	static function getDaysInBetween($start, $end) {
		$day = 86400; // Day in seconds
		$format = 'Y-m-d'; // Output format (see PHP date funciton)
		$sTime = /*strtotime(*/$start/*)*/; // Start as time
		$eTime = /*strtotime(*/$end/*)*/; // End as time
		$numDays = round(($eTime - $sTime) / $day);
		$days = array();

		// Return days
		return abs($numDays);
	}

	static function print_row($label_id, $value, $th_width = null, $is_bold = true) {
		global $VIEW;

		if ( ! $value ) {
			return '';
		}

		$bold_class = '';
		if ( ! $is_bold ) {
			$bold_class = 'class="no_bold"';
		}

		return '<tr><th ' . (($th_width) ? 'style="width:' . $th_width . 'px"' : '') . '>' . __($label_id) . ':</th><td ' . $bold_class . '>' . $value . '</td></tr>';
	}
	
	/*
    case 0 = Unknown
    case 1 = Offline
    case 2 = Online
    case 3 = Away
    case 4 = Not Available
    case 5 = Do Not Disturb
    case 6 = Invisible
    case 7 = Skype Me
	*/
	
	static function getSkypeStatus($username, $lang = "de", $img_type = "bigclassic")
	{
		$url = "http://mystatus.skype.com/".$username.".xml";
		$data = @file_get_contents($url);

		$status = array();
		if($data === false)
		{
		$status = array("num" =>0,
						"text"=>"http error"
					);
		if(isset($http_response_header)) $status["error_info"] = $http_response_header;
		}
		else
		{
		$pattern = '/xml:lang="NUM">(.*)</';
		preg_match($pattern,$data, $match);
		
		switch ($match[1]) {
			//online top priority
			case 2:
				$status["num"] = 5;
				break;
			case 3:
				$status["num"] = 4;
				break;
			case 5:
				$status["num"] = 3;
				break;
			default:	
			$status["num"] = 0;
		}
		//$status["num"] = $match[1];
		$pattern = '/xml:lang="' . $lang .'">(.*)</';
		preg_match($pattern,$data, $match);

		$status["text"]    = $match[1];
		$status["img_url"] = "http://mystatus.skype.com/".$img_type."/".$username;
		}
		return $status;
	}

	public static function dateToAge($timestamp)
	{
		if ( $timestamp === false || is_null($timestamp) || ! $timestamp ) return null;

		if (strpos($timestamp, '-') ) {
			$timestamp = strtotime($timestamp);
		}

		$curr_time = time();
		$age = ($timestamp < 0 ) ? ($curr_time + ($timestamp * -1)) : $curr_time - $timestamp;
		$year = 60 * 60 * 24 * 365;

		$age_in_years = floor($age / $year);

		if ($age_in_years < 18) $age_in_years = 18;

		return $age_in_years;
	}
}