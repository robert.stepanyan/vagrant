<?php

class Cubix_SmsApi_Response
{
    public $status;
    public $headers;
    public $body;

    public function __construct($status = 200, array $headers = [], $body = null) {
        $this->status = $status;
        $this->headers = $headers;
        $this->body = $body;
        $this->mapper = new Cubix_SmsApi_Response_Mapper();
    }

    /**
     * @return int
     */
    public function getStatusCode() {
        return (int)$this->status;
    }

    /**
     * @return null|string
     */
    public function getBody() {
        return $this->body;
    }

    /**
     * @return array
     */
    public function getHeaders() {
        return $this->headers;
    }

    /**
     * @throws Cubix_SmsApi_ErrorException
     */
    public function getParsedBody() {
       return Cubix_SmsApi_Response_Mapper::map($this);
    }
}