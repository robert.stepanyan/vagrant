<?php

class Cubix_SmsApi_RequestExecutor
{
    private $apiToken;
    private $uri;
    private $backupUri = 'https://api2.smsapi.com';

    public function __construct($apiToken, $uri) {
        $this->apiToken = $apiToken;
        $this->uri = $uri;
    }

    /**
     * @param $action
     * @param array $data
     * @param bool $backup
     * @return mixed
     * @throws Exception
     */
    public function request($action, array $data, $backup = false) {
        static $content;

        if ($backup == true) {
            $url = $this->backupUri.'/'.trim($action, '/');
        } else {
            $url = rtrim($this->uri, '/').'/'.trim($action, '/');
        }

        try{
            $params = $this->buildParams($data);
            $c = curl_init();
            $headers = array(
                "Authorization: Bearer {$this->apiToken}"
            );
            curl_setopt($c, CURLOPT_URL, $url);
            curl_setopt($c, CURLOPT_POST, true);
            curl_setopt($c, CURLOPT_POSTFIELDS, $params);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($c, CURLOPT_HTTPHEADER, $headers);

            $content = curl_exec($c);
            $http_status = curl_getinfo($c, CURLINFO_HTTP_CODE);

            if ($http_status != 200 && $backup == false) {
                $backup = true;
                $this->request($action, $data, $backup);
            }

            curl_close($c);
            $response = new Cubix_SmsApi_Response($http_status, $headers, $content);
            return $response->getParsedBody();
        }catch (Exception $exception){
            throw  new \Exception($exception->getMessage(),0, $exception);
        }
    }

    private function buildParams(array $data) {
        $params = array();
        foreach ($data as $key => $value){
            if (strpos($key, 'param') !== 0){
                continue;
            }
            $params[$key] =$value;
            unset($data[$key]);
        }

        $data = array_intersect_key($data, [
            'to'=> null,
            'from' => null,
            'message' => null,
            'template' => null,
            'encoding' => null,
            'idx' => null,
            'check_idx' => null,
            'expiration_date' => null,
            'single' => null,
            'nounicode' => null,
            'normalize' => null,
            'notify_url' => null,
            'test' => null,
            'fast' => null
        ]);

        if (empty($data['to'])){
            throw new Exception("Recipient's mobile phone number required");
        }

        if (empty($data['message'])){
            throw new Exception("The message text required");
        }

        if (empty($data['from'])){
//            $data['from'] = 'TEST';
        }

        $data =array_filter($data, function ($var){
            return !is_null($var);
        });
        $data['details']=1;

        if (is_array($data['to'])){
            $data['to'] =  implode(',', $data['to']);
        }

        if (is_array($data['idx'])){
            $data['idx'] =  implode('|', $data['idx']);
        }
        if (!empty($data['check_idx'])){
            $data['check_idx'] = 1;
        }else{
            unset($data['check_idx']);
        }

        foreach ($params as $param => $value){
            if (is_array($value)){
                $data[$param] = implode('|', $value);
            }else{
                $data[$param] = $value;
            }
        }

        $data['format'] = 'json';

        return $data;
    }
}