<?php
class Cubix_SmsApi_Response_Mapper{

    /**
     * @param Cubix_SmsApi_Response $response
     * @return stdClass
     * @throws Cubix_SmsApi_ErrorException
     */
    public static function map(Cubix_SmsApi_Response $response)
    {
        $statusCode = $response->getStatusCode();
        $contents = $response->getBody();

        if (in_array($statusCode, [Cubix_SmsApi_Response_HttpCode::OK, Cubix_SmsApi_Response_HttpCode::CREATED])) {
            $object = json_decode($contents);
            return $object;
        } elseif (in_array($statusCode, [Cubix_SmsApi_Response_HttpCode::ACCEPTED, Cubix_SmsApi_Response_HttpCode::NO_CONTENT])) {
            return new stdClass();
        } elseif ($statusCode == Cubix_SmsApi_Response_HttpCode::SERVICE_UNAVAILABLE) {
            throw Cubix_SmsApi_ErrorException::withMessageAndStatusCode('Service unavailable', $statusCode);
        } elseif ($contents) {
            $object = json_decode($contents);

            if (isset($object->message, $object->error)) {
                throw Cubix_SmsApi_ErrorException::withMessageErrorAndStatusCode($object->message, $object->error, $statusCode);
            }
        }

        throw Cubix_SmsApi_ErrorException::withStatusCode($statusCode);
    }

}
