<?php
class Cubix_SmsApi_Data_Sms{
    /** @var string */
    public $id;

    /** @var float */
    public $points;

    /** @var string */
    public $number;

    /** @var DateTimeInterface */
    public $dateSent;

    /** @var string */
    public $status;

    /** @var string|null */
    public $idx;

    /** @var stdClass|null */
    public $content;
}