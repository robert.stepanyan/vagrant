<?php

/**
 * @property string $from
 * @property string $message
 * @property string $template
 * @property string $encoding
 * @property array $idx
 * @property bool $check_idx
 * @property DateTimeInterface $expiration_date
 * @property bool $single
 * @property bool $nounicode
 * @property bool $normalize
 * @property string $notify_url
 * @property bool $test
 * @property bool $fast
 * @property string $param1
 * @property string $param2
 * @property string $param3
 * @property string $param4
 */
class Cubix_SmsApi_Send_SmsBulk
{
    /** @var array */
    public $to;

    /**
     * @param array $receivers
     * @param string $message
     * @return Cubix_SmsApi_Send_SmsBulk
     */
    public static function withMessage(array $receivers, $message)
    {
        $bag = new self();
        $bag->to = $receivers;
        $bag->message = $message;

        return $bag;
    }

    /**
     * @param array $receivers
     * @param string $template
     * @return Cubix_SmsApi_Send_SmsBulk
     */
    public static function withTemplateName(array $receivers, $template)
    {
        $bag = new self();
        $bag->to = $receivers;
        $bag->template = $template;

        return $bag;
    }

    /**
     * @param array $params
     * @return Cubix_SmsApi_Send_SmsBulk
     */
    public function setParams(array $params)
    {
        $index= 1;
        foreach ($params as $indexParam) {
            $this->{'param' . $index} = $indexParam;
            $index++;
        }

        return $this;
    }

    /**
     * @param array $idx
     * @param bool|null $checkIdx
     * @return $this
     */
    public function setExternalId(array $idx, $checkIdx = null)
    {
        $this->idx = $idx;
        $this->checkIdx = $checkIdx;

        return $this;
    }
}
