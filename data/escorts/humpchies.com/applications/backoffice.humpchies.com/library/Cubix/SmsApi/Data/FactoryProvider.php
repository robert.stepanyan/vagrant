<?php
class Cubix_SmsApi_Data_FactoryProvider{

    /**
     * @return Cubix_SmsApi_Data_SmsFactory
     */
    public function provideSmsFactory()
    {
        return new Cubix_SmsApi_Data_SmsFactory();
    }
}