<?php
class Cubix_SmsApi_ErrorException extends Exception {

    /** @var string */
    private $error;

    /**
     * @param $message
     * @param $error
     * @param $statusCode
     * @return Cubix_SmsApi_ErrorException
     */
    public static function withMessageErrorAndStatusCode($message, $error, $statusCode)
    {
        $exception = new self($message, $statusCode);
        $exception->error = $error;

        return $exception;
    }

    /**
     * @param $statusCode
     * @return Cubix_SmsApi_ErrorException
     */
    public static function withStatusCode($statusCode)
    {
        return new self('Api error', $statusCode);
    }

    /**
     * @param $message
     * @param $error
     * @return Cubix_SmsApi_ErrorException
     */
    public static function withMessageAndError($message, $error)
    {
        $exception = new self($message);
        $exception->error = (string)$error;

        return $exception;
    }

    /**
     * @param $message
     * @param $statusCode
     * @return Cubix_SmsApi_ErrorException
     */
    public static function withMessageAndStatusCode($message, $statusCode)
    {
        return new self($message, $statusCode);
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
}