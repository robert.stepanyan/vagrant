<?php

/**
 * Class Cubix_SmsApi_Data_SmsFactory
 *
 *
 */
class Cubix_SmsApi_Data_SmsFactory
{

    /**
     * @param stdClass $object
     * @return Cubix_SmsApi_Data_Sms
     * @throws Exception
     */
    public function createFromObject(stdClass $object) {
        $sms = new Cubix_SmsApi_Data_Sms();
        $sms->id = $object->id;
        $sms->points = (float)$object->points;
        $sms->status = $object->status;
        $sms->dateSent = new \DateTime('@' . $object->date_sent);
        $sms->number = $object->number;
        $sms->idx = $object->idx;

        return $sms;
    }

    /**
     * @param stdClass $object
     * @param string $message
     * @param int $length
     * @param $parts
     * @return Cubix_SmsApi_Data_Sms
     * @throws Exception
     */
    public function createFromObjectWithDetails(stdClass $object, $message, int $length, $parts) {
        $parts = (int)$parts;

        $sms = $this->createFromObject($object);

        $sms->content = $this->crateContent($message, $length, $parts);

        return $sms;
    }

    /**
     * @param $message
     * @param $length
     * @param $parts
     * @return stdClass
     */
    private function crateContent($message, $length, $parts) {
        $smsDetails = new stdClass();

        $smsDetails->message = $message;
        $smsDetails->length = $length;
        $smsDetails->parts = $parts;

        return $smsDetails;
    }
}