<?php
class Cubix_SmsApi_HttpClient {

    private $smsapiPlUri = 'https://api.smsapi.pl';
    private $smsapiComUri = 'https://api.smsapi.com';


    /**
     * @param string $apiToken
     * @return Cubix_SmsApi_HttpService
     */
    public function smsapiComService($apiToken)
    {
        return $this->smsapiComServiceWithUri($apiToken, $this->smsapiComUri);
    }

    /**
     * @param string $apiToken
     * @param string $uri
     * @return Cubix_SmsApi_HttpService
     */
    public function smsapiComServiceWithUri($apiToken, $uri)
    {
        return new Cubix_SmsApi_HttpService($apiToken, $uri);
    }
}
