<?php

/**
 * @property string $from
 * @property string $message
 * @property string $template
 * @property string $encoding
 * @property array $idx
 * @property bool $check_idx
 * @property DateTimeInterface $expiration_date
 * @property bool $single
 * @property bool $nounicode
 * @property bool $normalize
 * @property string $notify_url
 * @property bool $test
 * @property bool $fast
 * @property string $param1
 * @property string $param2
 * @property string $param3
 * @property string $param4
 */
class Cubix_SmsApi_Send_Sms
{
    /** @var string */
    public $to;

    /**
     * @param string $receiver
     * @param string $message
     * @return Cubix_SmsApi_Send_Sms
     */
    public static function withMessage($receiver, $message)
    {
        $bag = new self();
        $bag->to = $receiver;
        $bag->message = $message;

        return $bag;
    }

    /**
     * @param string $receiver
     * @param string $template
     * @return Cubix_SmsApi_Send_Sms
     */
    public static function withTemplateName($receiver, $template)
    {
        $bag = new self();
        $bag->to = $receiver;
        $bag->template = $template;

        return $bag;
    }

    /**
     * @param array $params
     * @return Cubix_SmsApi_Send_Sms
     */
    public function setParams(array $params)
    {
        $index= 1;
        foreach ($params as $indexParam) {
            $this->{'param' . $index} = $indexParam;
            $index++;
        }

        return $this;
    }

    /**
     * @param string $idx
     * @param bool|null $checkIdx
     * @return Cubix_SmsApi_Send_Sms
     */
    public function setExternalId($idx, $checkIdx = null)
    {
        $this->idx = [$idx];
        $this->check_idx = $checkIdx;

        return $this;
    }

    /**
     * @param array $idx
     * @param null $checkIdx
     * @return $this
     */
    public function setIdx(array $idx, $checkIdx = null)
    {
        $this->idx = $idx;
        $this->check_idx = $checkIdx;

        return $this;
    }
}
