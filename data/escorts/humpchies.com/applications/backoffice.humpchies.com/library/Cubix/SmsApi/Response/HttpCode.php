<?php
class Cubix_SmsApi_Response_HttpCode {
    const OK = 200;
    const CREATED = 201;
    const ACCEPTED = 202;
    const NO_CONTENT = 204;
    const SERVICE_UNAVAILABLE = 503;
}