<?php


class Cubix_SmsApi_Feature{
    private $requestExecutor;
    private $dataFactoryProvider;

    public function __construct(
        Cubix_SmsApi_Data_FactoryProvider $dataFactoryProvider,
        Cubix_SmsApi_RequestExecutor $requestExecutor
    ) {
        $this->dataFactoryProvider = $dataFactoryProvider;
        $this->requestExecutor = $requestExecutor;
    }

    /**
     * @param Cubix_SmsApi_Send_Sms $sendSmsBag
     * @return Cubix_SmsApi_Data_Sms
     *
     * @throws Exception
     */
    public function sendSms(Cubix_SmsApi_Send_Sms $Sms)
    {
        try{
            $response = $this->makeRequest($Sms);
            if ($response->error){
                throw new Exception($response->message. " code ::".$response->error);
            }
            return $this->dataFactoryProvider->provideSmsFactory()->createFromObjectWithDetails(
                current($response->list),
                $response->message,
                $response->length,
                $response->parts
            );
        }catch (Exception $exception){
            throw new Exception($exception->getMessage(),0, $exception);
        }
    }

    /**
     * @param Cubix_SmsApi_Send_SmsBulk $sendSmssBag
     * @return Cubix_SmsApi_Data_Sms[]
     * @throws Exception
     */
    public function sendSmsBulk(Cubix_SmsApi_Send_SmsBulk $SmsBulk)
    {
     
        $response = $this->makeRequest($SmsBulk);
        if ($response->error){
            throw new Exception($response->message. " code ::".$response->error);
        }
        try{
            return array_map(
                [$this->dataFactoryProvider->provideSmsFactory(), 'createFromObject'],
                $response->list
            );
        }catch (Exception $exception){
            throw new Exception($exception->getMessage(),0, $exception);
        }
    }


    /**
     * @param $data
     * @return stdClass
     *
     * @throws Exception
     */
    private function makeRequest($data)
    {
        try{
            return $this->requestExecutor->request('sms.do', (array)$data);
        }catch (Exception $exception){
            throw new Exception($exception->getMessage(),0, $exception);
        }
    }


}
