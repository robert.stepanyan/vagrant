<?php

/**
 * Class Cubix_SmsApiCom_HttpService
 *
 */
class Cubix_SmsApi_HttpService{
    private $dataFactoryProvider;
    private $requestExecutor;

    public function __construct($apiToken, $uri) {
        $this->dataFactoryProvider =  new Cubix_SmsApi_Data_FactoryProvider();
        $this->requestExecutor =  new Cubix_SmsApi_RequestExecutor($apiToken, $uri);
    }

    /**
     * @return Cubix_SmsApi_Feature
     */
    public function smsFeature()
    {
        return new Cubix_SmsApi_Feature($this->dataFactoryProvider, $this->requestExecutor);
    }
}