<?php

class Cubix_Email
{
	public static function send($email, $subject, $body, $htmlEmail = null, $from = null, $from_name = null, $application_id = null, $attachments = array(), $smtp2 = false)
	{
		if ( is_array($application_id)) $application_id = reset($application_id);
		
		define('MAIL_SERVER_HOST', "smtp-hmpchs.newsmanapp.com");
		define('MAIL_SERVER_PORT', 25);
		define('MAIL_SERVER_AUTH', true);
		define('MAIL_SERVER_USER', "hmpchs_comacc@tmail.humpchies.com");
		define('MAIL_SERVER_PASS', "HmpC%28UqnZo384#d");
		define('MAIL_FROM', "info@humpchies.com");
		define('MAIL_FROM_NAME', "humpchies.com");
			
		
		$config = array(
			'auth' => 'login',
			'port' => MAIL_SERVER_PORT,
			'ssl' => 'tls',
			'username' => MAIL_SERVER_USER,
			'password' => MAIL_SERVER_PASS
		);
		
				
		try {
			$tr = new Zend_Mail_Transport_Smtp(MAIL_SERVER_HOST, $config);
			Zend_Mail::setDefaultTransport($tr);

			$mail = new Zend_Mail('UTF-8');

			if ($htmlEmail)
				$mail->setBodyHtml($body);
			else
				$mail->setBodyText($body);

			$mail->addTo($email);
			$mail->setSubject($subject);
			$mail_from_name = MAIL_FROM_NAME;
						
			$mail->setFrom( (false == empty($from)) ? $from : MAIL_FROM, (false == empty($from_name)) ? $from_name : $mail_from_name );

			if ( count( $attachments ) ) {
				foreach($attachments as $attachment) {
					$at = $mail->createAttachment($attachment['item']);
					$at->type        = $attachment['type'];
					$at->filename    = $attachment['name'];
				}
			}
			
			$mail->send();
			
			//mail($email,$subject,$body);
			return true;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function sendTemplate($template_id, $to_addr, $data = array(), $application_id, $lang_id, $from_addr = null, $from_name = null, $smtp2 = false)
	{
		if ( is_array($from_addr) ) $from_addr = reset($from_addr);
		if ( is_array($lang_id) ) $lang_id = reset($lang_id);

		if ( ! $lang_id ) $lang_id = 'en';
		$db = Zend_Registry::get('db');
		$template = $db->fetchRow('
			SELECT from_addr, subject, body
			FROM email_templates
			WHERE id = ? AND application_id = ? AND lang_id = ?
		', array($template_id, $application_id, $lang_id));


		if ( ! $template ) {
			throw new Exception('LNG: {' . $lang_id . '}, Template with id "' . $template_id . '" for application "' . $application_id . '" does not exist');
		}

		$template = self::_processTemplate($template, $data, $application_id, $lang_id);
        $result = self::send($to_addr, $template->subject, $template->body, true, ( ! strlen($from_addr) ) ? $template->from_addr : $from_addr, $from_name, $application_id, array(), $smtp2);
        if ($application_id == APP_6B){
            $text= date("Y-m-d H:i:s ") . " email sent to $to_addr\r\n";
            $text .= "   with data : \r\n";
            $dataA = array(
                'template'=>$template_id,
                'language'=>$lang_id,
                'data'=>$data,
                'result' => $result
            );
            $text .= var_export($dataA, true);
            $fp = fopen("./send-email.log",'a+');
            if (flock($fp, LOCK_EX)){
                fwrite($fp, $text);
                flock($fp, LOCK_UN);
                fclose($fp);
            }
        }
        return $result;
	}

	protected static function _processTemplate($template, array $data, $application_id, $lang_id = 'en')
	{
		$app = Cubix_Application::getById($application_id);

		$data = array_merge($data, array(
			'application_title' => $app->title,
			'project' => $app->title,
			'application_host' => $app->host,
			'lang_id' => $lang_id
		));

		$fields = array('from_addr', 'subject', 'body');

		foreach ( $fields as $field ) {
			$a = array();
			if ( preg_match_all('/%(.+?)%/', $template->$field, $a) ) {
				array_shift($a);
				$params = reset($a);
				$params = array_unique($params);

				foreach ( $params as $param ) {
					if ( ! isset($data[$param]) ) {
						$data[$param] = '';
					}

					$template->$field = str_replace('%' . $param . '%', $data[$param], $template->$field);
				}
			}
		}

		return $template;
	}
}
