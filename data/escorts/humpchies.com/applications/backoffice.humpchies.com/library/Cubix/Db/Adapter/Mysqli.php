<?php


class Cubix_Db_Adapter_Mysqli extends Zend_Db_Adapter_Mysqli
{
	/**
     * Prepares and executes an SQL statement with bound data.
     *
     * @param  mixed  $sql  The SQL statement with placeholders.
     *                      May be a string or Zend_Db_Select.
     * @param  mixed  $bind An array of data to bind to the placeholders.
     * @return Zend_Db_Statement_Interface
     */
    public function query($sql, $bind = array())
    {
        // connect to the database if needed
        $this->_connect();

        // is the $sql a Zend_Db_Select object?
        if ($sql instanceof Zend_Db_Select) {
            if (empty($bind)) {
                $bind = $sql->getBind();
            }

            $sql = $sql->assemble();
        }

        // make sure $bind to an array;
        // don't use (array) typecasting because
        // because $bind may be a Zend_Db_Expr object
        if (!is_array($bind)) {
            $bind = array($bind);
        }

		list($usec, $sec) = explode(" ", microtime());
		$execution_start_time = ((float)$usec + (float)$sec);

		// prepare and execute the statement with profiling
        $stmt = $this->prepare($sql);
        $stmt->execute($bind);

		list($usec, $sec) = explode(" ", microtime());
		$execution_end_time = ((float)$usec + (float)$sec);
		
		if ( (defined('IS_DEBUG') && IS_DEBUG) || isset($_GET['debug_mode']) ) {
			if ( ! defined('APP_ROOT_PATH') ) define('APP_ROOT_PATH', realpath(APPLICATION_PATH));
			
			$execution_time = $execution_end_time - $execution_start_time;
			$GLOBALS['SQLS_TOTAL_EXEC'] += $execution_time;
			$delim = str_repeat('#', 60) . "\n";
			
			$text = $delim.
				'SQL: ' . $sql . "\n" . str_repeat('-', 60) . "\n\n";
			
			$backtrace = debug_backtrace();
			array_shift($backtrace);

			$trace_list = array();
			foreach($backtrace as $i => $bt) {
				if ( ! isset($bt['class']) ) $bt['class'] = '';
				if ( ! isset($bt['type']) ) $bt['type'] = '';
				if ( ! isset($bt['file']) ) $bt['file'] = '';
				if ( ! isset($bt['line']) ) $bt['line'] = '';

				$trace_list[] =
					($i + 1) . ". " .
					str_replace('\\', '/', str_replace(APP_ROOT_PATH, '', $bt['file'])) . ", " .
					$bt['line'] . "\n" .
					$bt['class'] . $bt['type'] . $bt['function'];
			}

			$trace_list = implode("\n\n", $trace_list);

			$text .= $trace_list . "\nExecution Time: " . $execution_time . "\n" . $delim . "\n";

			$GLOBALS['SQLS'][] = $text;
			//file_put_contents('mysql.log', $text, FILE_APPEND);
		}

		// return the results embedded in the prepared statement object
        $stmt->setFetchMode($this->_fetchMode);
        return $stmt;
    }
}
