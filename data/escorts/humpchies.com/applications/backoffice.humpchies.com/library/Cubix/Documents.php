<?php

class Cubix_Documents
{
    /**
     * Configuration from application init
     *
     * @var array
     */
    protected $_config;

    public $allowed_ext = array('rtf','doc','txt','pdf','jpg','gif','png','bmp','doc','docx','rtf','ppt','pptx','xls','xlsx');

    public function __construct()
    {
        $this->_config = Zend_Registry::get('images_config');
    }

    /**
     * Save the file on the remote machine
     *
     * @param string $file
     * @param string $catalog_id the same as as id
     * @param string $ext_override
     * @return array
     *
     * @throws \Exception
     */
    public function save($file, $catalog_id, $ext_override = null)
    {

        // Create a random file name
        $hash = md5(microtime(true) * mt_rand(1, 1000000));


        if (is_null($ext_override)) {
            $ext = pathinfo($file, PATHINFO_EXTENSION);
        }
        else {
            $ext = $ext_override;
        };

        $ext = strtolower($ext);

        if (!in_array($ext, $this->allowed_ext  ) )
        {
            throw new Exception('Not allowed file extension.');
        }
        $remote = $this->_getFullPath(array(
            'catalog_id' => $catalog_id,
            'hash' => $hash,
            'ext' => $ext
        ));

        $filePath = rtrim($this->_config['filePath'], DIRECTORY_SEPARATOR). DIRECTORY_SEPARATOR;

        $remote = $filePath.$remote;
        $dir = explode(DIRECTORY_SEPARATOR, $remote);
        array_pop($dir);
        $dir = implode(DIRECTORY_SEPARATOR, $dir);

        if (!is_dir($dir)){
            mkdir($dir,0777, true);
        }

        if (is_uploaded_file($file)){
            move_uploaded_file($file, $remote);
        }else{
            rename($file, $remote);
        }

        return array('hash' => $hash, 'ext' => $ext);
    }

    /**
     * @param array $entries
     * @return bool
     */
    public function remove($entries)
    {
        if ( ! is_array($entries) ) {
            $entries = array($entries);
        }

        if ( ! count($entries) ) return false;

        $filePath = rtrim($this->_config['filePath'], DIRECTORY_SEPARATOR). DIRECTORY_SEPARATOR;

        foreach ($entries as $entry){
            $remote = $this->_getFullPath($entry);
            unlink($filePath.$remote);
        }

        return true;
    }

    /**
     * @param array | stdClass $entry
     * @return string|boolean
     */
    protected function _getFullPath($entry)
    {
        if (is_object($entry)){
            $entry = (array) $entry;
        }

        if (!is_array($entry)){
            return false;
        }

        $catalog = $entry['catalog_id'];
        $parts = array($catalog);
        $DS = DIRECTORY_SEPARATOR;
        $a = array();
        if ( is_numeric($catalog) ) {
            $parts = array();

            if ( strlen($catalog) > 2 ) {
                $parts[] = substr($catalog, 0, 2);
                $parts[] = substr($catalog, 2);
            }
            else {
                $parts[] = '_';
                $parts[] = $catalog;
            }
        }
        else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
            array_shift($a);
            $catalog = $a[0];

            $parts = array();

            if ( strlen($catalog) > 2 ) {
                $parts[] = substr($catalog, 0, 2);
                $parts[] = substr($catalog, 2);
            }
            else {
                $parts[] = '_';
                $parts[] = $catalog;
            }

            $parts[] = $a[1];
        }

        $catalog = implode($DS, $parts);

        $path = $DS . $catalog . $DS . $entry['hash'] . '.' . $entry['ext'];

        return $path;
    }

    /**
     * Returns url of file
     * @param array $entry
     * @return string
     */
    public function getUrl($entry)
    {

        return trim($this->_config['url'], DIRECTORY_SEPARATOR) . $this->_getFullPath($entry);
    }

    /**
     * @see Cubix_Documents::getUrl
     * @param array $entry
     * @return string
     */
    public function getServerUrl($entry)
    {
        return trim($this->_config['url'], DIRECTORY_SEPARATOR) . $this->_getFullPath($entry);
    }

}
