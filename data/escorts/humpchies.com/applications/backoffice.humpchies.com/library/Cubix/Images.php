<?php

class Cubix_Images
{
	const MAX_SIZE_IMAGE_IN_BITES                   = 2097152;                                          // Max 2MB file size on images... 
    
    const ORIENTATION_LANDSCAPE                     = 'L';
    const ORIENTATION_PORTRAIT                      = 'P';
    const ORIENTATION_SQUARE                        = 'S';
    
    const ERROR_INVALID_IMAGE_HEIGHT                = 'Core.Utils.Image.ErrorInvalidImageHeight';
    const ERROR_INVALID_IMAGE_WIDTH                 = 'Core.Utils.Image.ErrorInvalidImageWidth';
    const ERROR_INVALID_IMAGE                       = 'Core.Utils.Image.ErrorInvalidImage';
    const ERROR_UNABLE_TO_CREATE_RESAMPLING_DIR     = 'Core.Utils.Image.ErrorUnableToCreateResamplingDir';

    const ERROR_IMAGE_ERROR_UNHANDLED               = 'Core.Utils.Security.ErrorImageErrorUnhandled';
    const ERROR_IMAGE_HEIGHT_IS_INVALID             = 'Core.Utils.Security.ErrorImageHeightIsInvalid';                                             // ERROR: image height less than or equal to 0
    const ERROR_IMAGE_WIDTH_IS_INVALID              = 'Core.Utils.Security.ErrorImageWidthIsInvalid';                                             // ERROR: image width less than or equal to 0
    const ERROR_IMAGE_SIZE_IS_TOO_BIG               = 'Core.Utils.Security.ErrorImageSizeIsTooBig';
    const ERROR_IMAGE_TYPE_IS_INVALID               = 'Core.Utils.Security.ErrorImageTypeIsInvalid';                                             // ERROR: image not a jpeg
    const ERROR_IMAGE_RESAMPLING_FAILED             = 'Core.Utils.Security.ErrorUnableResamplingFailed';                                             // Unable to resample an image
    const NO_ERROR                                  = 'Core.Utils.Security.NoError';
	const AD_THUMB_HEIGHT_PORTRAIT                  = 180;
    const AD_THUMB_HEIGHT_LANDSCAPE                 = 80;
    const AD_THUMB_HEIGHT_SQUARED                   = 120;
    const AD_THUMB_WIDTH                            = 120;
    const AD_IMAGE_HEIGHT_PORTRAIT                  = 390;
    const AD_IMAGE_HEIGHT_LANDSCAPE                 = 173;
    const AD_IMAGE_HEIGHT_SQUARED                   = 260;
    const AD_IMAGE_WIDTH                            = 260;

    // MINIMUM SIZES FOR AD IMAGES
    const AD_IMAGE_MINHEIGHT_LANDSCAPE = 400;
    const AD_IMAGE_MINWIDTH_LANDSCAPE = 600;

    const AD_IMAGE_MINHEIGHT_PORTRAIT = 600;
    const AD_IMAGE_MINWIDTH_PORTRAIT = 400;

    const AD_IMAGE_MINHEIGHT_SQUARED = 400;
    const AD_IMAGE_MINWIDTH_SQUARED = 400;

    // IMAGE ORIENTATIONS
    const AD_IMAGE_LANDSCAPE = 1;
    const AD_IMAGE_PORTRAIT = 2;
    const AD_IMAGE_SQUARED = 3;

    static $MimeTypes = array(
//        'png' => 'image/png',
//        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
//        'gif' => 'image/gif',
    );
	/**
   	* Disable object copying
    	*/
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
   	/**
    	* Disable object deserialization
    	*/
    private function __sleep()
   	{}
    
    /**
    * Prevent the class from being instantiated
    * 
    * @returns Nothing
    */
    private function __construct()
    {} 

	/***
        * This public static function is used to retrive a time stamp that falls at the beginning of a particular chunk.
        * The concept of a chunk comes from partitionning the 24 hrs in a day into a max of twelve parts.
        * Each part is a chunk and the chucnk size is the number of hours in it. The snallest chunk size is
        * 1, meanning 1 hr per chunk which yields 24 chunks, which implies that the public static function will return
        * the time stamp of the current hour when it gets called. The largest chunck size is 12, meanning
        * two chunks.
        *
        * This public static function is useful for database calls where a time stamp is used to retrieve records based
        * on the current time. Use it instead of time() with cached queries where the query itself is used
        * to produce the cache key.
        *
        * @param mixed $chunk_size the number of hours in each chunk, max. 12, min. 1, anything in between
        * must be a multiple of 2.
        *
        * @return int
        */
    public static function getImageOrientation($image_url)
    {
       list($image_width, $image_height) = getimagesize($image_url);
       
       // Validate the height and width of the original image: both must be greater than 0 
        if($image_height <= 0)  
            return self::ERROR_INVALID_IMAGE_HEIGHT;
        elseif($image_width <= 0)
            return self::ERROR_INVALID_IMAGE_WIDTH;
       
       if(($image_orientation = ($image_width/$image_height)) < 1)      // Less than 1 => width < height: Portrait   
            return self::ORIENTATION_PORTRAIT;                                            
       elseif($image_orientation > 1)                                   // More than 1 => width > height: Landscape
            return self::ORIENTATION_LANDSCAPE;    
        else                                                            // Exactly 1 => wicth is equal to height: Square
            return self::ORIENTATION_SQUARE;    
    }

    /**
    * put your comment there...
    * 
    * @param mixed $absolut_path_to_original_image
    * @param mixed $absolut_path_to_replica_image
    * @param mixed $replica_image_width
     *
     * @return bool
    */
    public static function resampleJPEGImage($absolut_path_to_original_image, $absolut_path_to_replica_image, $replica_image_width = NULL, $force_destination_directory_creation = FALSE)
    {
        // Get the height and width of the original image
        list($original_image_width, $original_image_height) = @getimagesize($absolut_path_to_original_image);
        
        // Validate the height and width of the original image: both must be greater than 0 
        if($original_image_height <= 0)  
            return self::ERROR_INVALID_IMAGE_HEIGHT;
        elseif($original_image_width <= 0)
            return self::ERROR_INVALID_IMAGE_WIDTH;
        
        if($replica_image_width === NULL)
            $replica_image_width = $original_image_width; 
        
        /**
        * Get the orientation of original image. Based on the original orientaion of the image
        * define the height of our replica image. Nota bene: calculations based on 3:2 aspect ratio  
        */
        if(($original_image_orientation = ($original_image_width/$original_image_height)) < 1)      // Less than 1 => width < height: Portrait  
            $replica_height = (($replica_image_width / 2) * 3);                                           
        elseif($original_image_orientation > 1)                                                     // More than 1 => width > height: Landscape    
            $replica_height = (($replica_image_width / 3) * 2);
        else                                                                                        // Exactly 1 => wicth is equal to height: Square
            $replica_height = $replica_image_width;       
        
        /**
        * Adjust height or width so that their values allow us to resample the image proportionally 
        */
        if(($replica_image_width/$replica_height) > $original_image_orientation)
        {
            $height_for_resampling = ($replica_image_width/$original_image_orientation);
            $width_for_resampling = $replica_image_width; 
        }      
        else
        {
            $width_for_resampling = ($replica_height*$original_image_orientation);
            $height_for_resampling = $replica_height; 
        }
             
        $process = imagecreatetruecolor(round($width_for_resampling), round($height_for_resampling));
        $imagecreatefrom = 'imagecreatefrom';
        $image = "image";
        $ext = pathinfo($absolut_path_to_original_image, PATHINFO_EXTENSION);
        $ext =  strtolower($ext);
        if (!in_array($ext, array_keys(self::$MimeTypes))){
            return self::ERROR_INVALID_IMAGE;
        }
        if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'jpe'){
            $imagecreatefrom .= 'jpeg';
            $image .= 'jpeg';
        }else{
            $imagecreatefrom .= $ext;
            $image .= $ext;
        }

        $image_for_resampling = @$imagecreatefrom($absolut_path_to_original_image);
        
        if($image_for_resampling === FALSE || !is_resource($image_for_resampling))
            return self::ERROR_INVALID_IMAGE;

        imagecopyresampled($process, $image_for_resampling, 0, 0, 0, 0, $width_for_resampling, $height_for_resampling, $original_image_width, $original_image_height);
        $replica_image = @imagecreatetruecolor($replica_image_width, $replica_height);
        imagecopyresampled($replica_image, $process, 0, 0, (($width_for_resampling/2)-($replica_image_width/2)), (($height_for_resampling/2)-($replica_height/2)), $replica_image_width, $replica_height, $replica_image_width, $replica_height);

        imagedestroy($process);
        imagedestroy($image_for_resampling);
        
        if($force_destination_directory_creation === TRUE && is_dir(dirname($absolut_path_to_replica_image)) === FALSE && @mkdir(dirname($absolut_path_to_replica_image), 0777, TRUE) === FALSE)
            return self::ERROR_UNABLE_TO_CREATE_RESAMPLING_DIR;

        return @$image($replica_image, $absolut_path_to_replica_image, 100);
    }

    /**
     * @param $posted_images
     * @return array|bool|string
     */
    public static function areJPEGImagesInputValid(&$posted_images)
    {
        $image_number = 0;
        $orientation_errors = false;

        foreach($posted_images as $image_name => &$image_details)
        {
            list($_width, $_height) = getimagesize($image_details['tmp_name']);
            $image_dimension = array('width' => $_width, 'height' => $_height);
            $image_orientation = self::getImageOrientation($image_dimension);
            $ext = pathinfo ( $image_details['tmp_name'] ,PATHINFO_EXTENSION);
            if($image_details['error'] === UPLOAD_ERR_INI_SIZE || ($image_details['size'] > self::MAX_SIZE_IMAGE_IN_BITES))
            {
                $posted_images = $image_details['name'];
                return self::ERROR_IMAGE_SIZE_IS_TOO_BIG;
            }
            elseif($image_details['error'] === UPLOAD_ERR_NO_FILE || $image_details['size'] === 0)
            {
                unset($posted_images[$image_name]);
                continue;
            }
            elseif($image_details['error'] !== UPLOAD_ERR_OK)
            {
                $posted_images = (string)$image_details['error'];
                return self::ERROR_IMAGE_ERROR_UNHANDLED;
            }
            elseif($image_details['type'] !== self::getMimeTypeByExt($ext))
            {
                $posted_images = $image_details['name'];
                return self::ERROR_IMAGE_TYPE_IS_INVALID;
            }
            elseif(self::getOrientationDimensionErrors($image_dimension, $image_orientation))
            {
                return self::getOrientationDimensionErrors($image_dimension, $image_orientation);
            }
            else
            {
                $image_details['number'] = $image_number++;
                $image_details['orientation'] = self::getImageOrientation($image_details['tmp_name']);
            }
        }

        if(empty($posted_images))
            $posted_images = NULL;

        return self::NO_ERROR;
    }

    public static function getMimeTypeByExt($ext) {
        return self::$MimeTypes[$ext];
    }

    /**
     * @param $image_dimension
     * @param $orientation
     * @return array|bool
     */
    public static function getOrientationDimensionErrors( $image_dimension, $orientation ){
        if( $orientation == self::AD_IMAGE_LANDSCAPE ){
            if( $image_dimension['width'] < self::AD_IMAGE_MINWIDTH_LANDSCAPE || $image_dimension['height'] < self::AD_IMAGE_MINHEIGHT_LANDSCAPE ){
                return array('dimention' => 'error', 'msg' => 'Image dimension should be bigger than 600X400');
            }
        } elseif( $orientation == self::AD_IMAGE_PORTRAIT ){
            if( $image_dimension['width'] < self::AD_IMAGE_MINWIDTH_PORTRAIT || $image_dimension['height'] < self::AD_IMAGE_MINHEIGHT_PORTRAIT ){
                return array('dimention' => 'error', 'msg' => 'Image dimension should be bigger than 400X600');
            }
        } elseif( $orientation == self::AD_IMAGE_SQUARED ){
            if( $image_dimension['width'] < self::AD_IMAGE_MINWIDTH_SQUARED | $image_dimension['height'] < self::AD_IMAGE_MINHEIGHT_SQUARED ){
                return array('dimention' => 'error', 'msg' => 'Image dimension should be bigger than 400X400');
            }
        }

        return false;
    }

}
