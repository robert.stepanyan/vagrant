<?php

class Cubix_Controller_Request_Cli extends Zend_Controller_Request_Simple
{
    public function __get($key)
	{
		return $this->getParam($key);
	}
	
	public function isPost()
	{
		return true;
	}
}
