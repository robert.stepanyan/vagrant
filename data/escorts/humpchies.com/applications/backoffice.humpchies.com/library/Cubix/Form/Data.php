<?php

class Cubix_Form_Data {
	/**
	 * Request object
	 *
	 * @var Zend_Controller_Request_Abstract
	 */
	protected $_request;
	
	/**
	 * Request fields array
	 *
	 * @var array
	 */
	protected $_fields;
	
	public function __construct(Zend_Controller_Request_Abstract $request)
	{
		$this->_request = $request;
	}
	
	public function setFields(array $fields)
	{
		$this->_fields = $fields;
	}
	
	/**
	 * Returns an array with param values
	 *
	 * @return array
	 * @author GuGo
	 */
	public function getData()
	{
		$data = array();
		
		foreach ( $this->_fields as $field => $filter ) {
			$data[$field] = $this->_request->getParam($field);
			
			if ( ! is_array($data[$field]) ) {
				$data[$field] = trim($data[$field]);
			}
			else {
				foreach ( $data[$field] as $i => $v ) {
					$data[$field][$i] = trim($v);
				}
			}
			
			$filter = explode('|', $filter);
			foreach ( $filter as $f ) {
				$a = array();
				
				if ( $f == 'int' ) {
					$data[$field] = abs(intval($data[$field]));
				}
				elseif ( $f == 'int-nz' ) {
					$data[$field] = abs(intval($data[$field]));
					if ( $data[$field] === 0 ) $data[$field] = null;
				}
				elseif ( $f == 'double' ) {
					$data[$field] = doubleval($data[$field]);
				}
				elseif ( $f == 'special' ) {
					$data[$field] = htmlspecialchars($data[$field],ENT_QUOTES);
				}
				elseif ( $f == 'notags' ) {
					$data[$field] = strip_tags($data[$field]);
				}
				elseif ( preg_match('/^max\[([0-9]+)\]$/', $f, $a) ) {
					$data[$field] = substr($data[$field], $a[1]);
				}
				elseif ( $f == 'arr-int' ) {
					if (  is_array($data[$field]) && count($data[$field]) ) {
						$data[$field] = (array) $data[$field];
						foreach ( $data[$field] as $i => $v ) {
							$data[$field][$i] = intval($v);
						}
					} else {
						$data[$field] = array();
					}
				}
				elseif ( $f == 'arr-double' ) {
					if (  is_array($data[$field]) && count($data[$field]) ) {
						$data[$field] = (array) $data[$field];
						foreach ( $data[$field] as $i => $v ) {
							$data[$field][$i] = doubleval($v);
						}
					} else {
						$data[$field] = array();
					}
				}
				elseif ( $f == 'date' ) {
					$data[$field] = $data[$field] ? $data[$field] : NULL;
				} 
				elseif ( $f == 'xss' ) {
					if ( Cubix_Application::getId() != 16 ) {
						$xss = new Cubix_Security();
						$data[$field] = $xss->clean_html($data[$field]);
						$data[$field] = $xss->xss_clean($data[$field]);
					}
					else {
						$data[$field] = strip_tags($data[$field]);
					}
				}
			}
		}
		
		return $data;
	}
}
