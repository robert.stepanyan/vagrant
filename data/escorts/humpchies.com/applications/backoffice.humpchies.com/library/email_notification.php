<?php
  class emailNotification{
        static $RedisNotificationHash       = "notificationHash";    
        static $RedisNotificationStack      = "notifications";    
        //static $RedisSentStack              = "notificationSendStack";
        static $RedisLastPullNotifications  = "notificationPullDate";
        
        private static function getLastPullDate(){
            $last =  Core_Redis::get(self::$RedisLastPullNotifications);
            //var_dump($last);
            if($last) {
                return $last;
            }else{
                return "2020-06-25 01:01:01";
                //return date("Y-m-d H:i:s");
            }
        }
        
        private static function setLastPullDate(){
            return Core_Redis::set(self::$RedisLastPullNotifications, date("Y-m-d H:i:s"));
        }
        
        private static function populateStack($userID, $email, $siteSection, $language){
            if(!Core_Redis::hexists(self::$RedisNotificationHash, 'U' . $userID)){ 
                Core_Redis::rpush(self::$RedisNotificationStack, 'U' . $userID);
                $stack = ['email' => $email, 'section' => $siteSection, 'language' => $language ];
                Core_Redis::hset(self::$RedisNotificationHash, 'U' . $userID, json_encode($stack) );
                //var_dump([self::$RedisNotificationStack, 'U' . $userID]);
                
            }
        }
        
        private static function pullFromStack(){
             while($key = Core_Redis::lpop(self::$RedisNotificationStack)){
                 if(Core_Redis::hexists(self::$RedisNotificationHash, $key)){
                     return json_decode(Core_Redis::hget(self::$RedisNotificationHash, $key), true);
                 }
             }
             return false;
        }
        
        public static function getMessages(){
            //echo "messages";
            $lastTime = self::getLastPullDate();
            self::setLastPullDate();
            
            $messages = Model_PrivateMessage::getNewMessages($lastTime);
            //var_dump($messages);
            foreach($messages as $user){
                self::populateStack($user->to_user_id, $user->email, 'site', 'fr');
            }
            
            $messages = Model_PrivateMessage::getNewForumMessages($lastTime);
            foreach($messages as $user){
                self::populateStack($user->to_user_id, $user->email, 'forum', 'fr');
            }
            
        }
        
        public static function sendMessages(){
            $from = "users@humpchiesmail.com";
            $fromName = "Support Humpchies";          
            
            $sections['en'] = ['site' => "Messages", 'forum' => "Forum" ]; 
            $sections['fr'] = ['site' => "Messagerie", 'forum' => "Forum" ]; 
            $mailSubject['fr'] = "Vous avez recu des nouveaux messages prives!!" ;       
           
            $mailBody['fr'] = "Vous avez des nouveaux messages priv&eacute;s dans votre Bo&icirc;te sur humpchies.
Pour le consulter et y r&eacute;pondre, veuillez vous connecter &agrave; votre compte sur humpchies.com et acc&eacute;der au \"{{SECTION}}\".<br><br>
<a href='https://www.humpchies.com/user/login'style='display: block; width: 193px; text-align: center; background-color: #ff9933; text-transform: uppercase; padding: 12px 21px; border-radius: 20px; text-decoration: none; color: black; font-weight: bold;' >Acc&eacute;der &agrave; mon compte</a>";
            

            while($stack = self::pullFromStack()){
                //$section = $sections[$stack['section']];
                $body = str_replace("{{SECTION}}", $sections[$stack['language']][$stack['section']], $mailBody[$stack['language']] );
                $subject =  $mailSubject[$stack['language']];
                SendMail::send($stack['email'], $subject, $body, $body, $from, $fromName);
                
                //SendMail::send($email, $subject, $body, $body);
            }   
              
            die("SENT");  
        }
        
        public static function testSendMessages(){
            $from = "users@humpchiesmail.com";
            $fromName = "Support Humpchies";          
            $subject = "Vous avez recu des nouveaux messages prives!!" ;       
           
            $body =  "Vous avez des nouveaux messages priv&eacute;s dans votre Bo&icirc;te sur humpchies.
Pour le consulter et y r&eacute;pondre, veuillez vous connecter &agrave; votre compte sur humpchies.com et aller sur Messages.<br><br>
<a href='https://www.humpchies.com/user/login'style='display: block; width: 193px; text-align: center; background-color: #ff9933; text-transform: uppercase; padding: 12px 21px; border-radius: 20px; text-decoration: none; color: black; font-weight: bold;' >Acc&eacute;der &agrave; mon compte</a>";
            


            //while($email = self::pullFromStack()){                              
           SendMail::send("webmaster@humpchies.com", $subject, $body, $body, $from, $fromName);
            //}   
              
            die("SENT");  
        }
        
        
  }
?>
