<?php 
  class Core_Redis
  {
    static $instance = false;
    static $server = "";
    static $port = "";
    static $password ="";
    static $prefix = "";
    static $connection = false;
    static $timeout = 30;
    static $separator = "\r\n";
    static $expire = false; 
    
    function __construct() {
        
         if(!self::$instance) {  
             
             self::init();
         }
         

         
    }
    
    private static function init() {
        
        if(self::$connection){
            return false;
        }
        //$cfg = Core_Config::get("appconfig.redis");
        
        //var_dump($cfg);
        $cfg['host']     = "localhost";//'172.16.238.13';
        $cfg['port']     = '6379';
        $cfg['password'] = '';
        $cfg['timeout']  = 30;
        $cfg['prefix']   = 'hump-';
        self::$server   = $cfg["host"];
        self::$port     = $cfg["port"];
        self::$password = $cfg["password"];
        
        !($cfg["password"]) || self::$password = $cfg["password"];
        
        !($cfg["timeout"])  || self::$timeout  = $cfg["timeout"];
        
        self::$prefix  = $cfg["prefix"];
        
        // Log::debug( "config key:" . self::$prefix);
             
        self::$connection = fsockopen(self::$server, self::$port , $errno, $errstr, self::$timeout);

        if (!self::$connection) {
         
               throw new \Exception('Connection to Redis Server Failed!', E_ERROR);
        }
    }
    
    public static function lpush($listName, $value){
          
        if(!self::$instance) {  
             
             self::init();
        }
          
        return self::_call("LPUSH", [ self::$prefix . $listName, $value ]);
        
          
    }
    
    public static function rpush($listName, $value){
          
        if(!self::$instance) {  
             
             self::init();
        }
          
        return self::_call("RPUSH", [ self::$prefix . $listName, $value ]);          
    }
    public static function lpop($listName){
          
        if(!self::$instance) {  
             
             self::init();
        }
          
        return self::_call("LPOP", [ self::$prefix . $listName ]);          
    }
    
    public static function getList($listName, $start = 0, $end = -1){
          
        if(!self::$instance) {  
             
             self::init();
        }
          
        return self::_call("LRANGE", [ self::$prefix . $listName, $start, $end ]);          
    }   
    
    public static function listTrim($listName, $start = 0, $end = -1){
          
        if(!self::$instance) {  
             
             self::init();
        }
          
        return self::_call("LTRIM", [ self::$prefix . $listName, $start, $end ]);          
    }   
    
    private static function auth() {
        
        if(self::$password) {
            
            self::_call("AUTH", [self::$password]);
        }
    }
    
    
    public static function exists($key){
          
        if(!self::$instance) {  
             
             self::init();
        }
          
        return self::_call("EXISTS", [ self::$prefix . $key]);          
    }   
    
    
    public static function get($key) {
         
        if(!self::$instance) {  
             
             self::init();
         }
        
         return  self::_call("GET", [ self::$prefix . $key]);
        
    }
    
    public static function set($key, $value, $expire = false){
           
        if(!self::$instance) {  
             
             self::init();
        }
          
        self::_call("SET", [ self::$prefix . $key, $value ]);
        if($expire){
            self::ttl($key, $expire);
        }
        
    }
    
    public static function hget($key,$array_key) {
         
        if(!self::$instance) {  
             
             self::init();
        }
        
        return  self::_call("HGET", [ self::$prefix . $key, $array_key]);
    }
    
    public static function hgetAll($key) {
         
        if(!self::$instance) {  
             
             self::init();
        }
        
        $return = self::_call("HGETALL", [ self::$prefix . $key ]);
        //log::debug($return);
        $hlist = [];
        for($i=0;$i< count($return);$i = $i + 2){ 
           $hlist[$return[$i]] = $return[$i + 1];     
        }
        
        return $hlist;
    }
    
    public static function hExists($key,$array_key) {
         
        if(!self::$instance) {  
             
             self::init();
        }
        
        return  self::_call("HEXISTS", [ self::$prefix . $key, $array_key]);
    }
    
    
    public static function hset($key,$list_key,$value){
        if(!self::$instance) {  
            self::init();
        }
        
        self::_call("HSET", [ self::$prefix . $key,  $list_key, $value ]);
    }
       
    
    public static function hdel($key,$list_key){
        if(!self::$instance) {  
            self::init();
        }
        
        self::_call("HDEL", [ self::$prefix . $key,  $list_key ]);
    }
    
    public static function ttl($key, $expiration = false){
         
        if(!self::$instance) {       
            self::init();
        }
        
        if(!intval($expiration)){
            
            $expiration = 300;//self::$expiration;  
        }
        
        self::_call("EXPIRE", [ self::$prefix . $key, intval($expiration) ]);   
        
    }
    
    public static function persistent($key){
          
        if(!self::$instance) {  
             
             self::init();
         }
        
         self::_call("PERSIST", [ self::$prefix . $key]);   
   
    }
    
    public static function dec($key, $by = 1){
         
        if(!self::$instance) {  
             
             self::init();
         }
        
        self::_call("DECRBY", [ self::$prefix . $key, $by]);
   
    }
    
    public static function inc($key, $by = 1){
         
        if(!self::$instance) {  
             
             self::init();
         }
        
        self::_call("INCRBY", [ self::$prefix . $key, $by]);
     
    }
    
    public static function geoadd($key, $long, $lat, $member){
        if(!self::$instance) {  
            self::init();
        }
        
        self::_call("GEOADD", [ self::$prefix . $key,  $long, $lat, $member ]);
    }
    
        public static function geoRadius($key, $long, $lat, $radius, $unit = "km" ) {
         
        if(!self::$instance) {  
             
             self::init();
        }
        
        $return = self::_call("GEORADIUS", [ self::$prefix . $key, $long, $lat, $radius, $unit ]);
        //log::debug($return);
       // $hlist = [];
//        for($i=0;$i< count($return);$i = $i + 2){ 
//           $hlist[$return[$i]] = $return[$i + 1];     
//        }
//        
        return $return;
    }
    
    public static function del($keys){
         
        if(!self::$instance) {  
             
             self::init();
         }
        if(! is_array($keys)){
            $keys = [$keys];    
        } 
        
        $keys = self::prepkey($keys);
                                         
    
        return self::_call("DEL",$keys);
        
        
    }
    
    public static function flush(){
         
        if(!self::$instance) {  
             
             self::init();
         }
        
        self::_call("FLUSHALL", []);
        
       
    }
    
    private static function prepkey($keys){
        
        array_walk($keys,function(&$key){
            $key = self::$prefix.$key;    
        });
        
        return $keys;
    }
    
    private static function readResponse() {
    
        $response = trim(fgets(self::$connection ));
        //log::debug(">".$response."<");
       // var_dump($response);
        switch(substr($response, 0, 1)) {
       
            case '+':  //success
                return true;
            break;
            
            case '-':  //ERROR
                return substr($response,1);
            break;
            
            case ':': //INTERGER
                return intval(substr(trim($response), 1));  
            break;

            case '$'://bulk strings 
                if( $response == '$1'){
                    //log::debug('numar');
                    return "1-";
                }    
                $raspuns = self::getBulkStrings($response);
               // Log::debug("read_reponse:" . $raspuns);
                return $raspuns;       
             break;
            
            case '*'://array                             
                $count = intval(substr($response, 1));
                if ($count == '-1')
                {
                    return -1;
                }
                
                $response = array();
                for ($i = 0; $i < $count; $i++)
                {
                    $response[] = self::readResponse();
                }
                return $response;
                break;
        }
        
        return false;
    }
    
    private static function getBulkStrings($reply){
     
       if($reply == "\$-1") {
            
            return false;
       }
       
       $read = 0;
       $size = intval(substr($reply, 1));
       
       if ($size == 0) {
           
             throw new Exception('Invalid Size ', E_ERROR);
       }
       
       do{
            $block_size = ($size - $read) > 1024 ? 1024 : ($size - $read);
            
            if (!$r = fread(self::$connection, $block_size)) {
                
                 throw new Exception('Failed to read response from Redis Stream. ', E_ERROR);
            }else {
                
                $read += strlen($r);
                $reply .= $r;
            }
            
       }while ($read < $size);
        
       // discard the crlf
       fread(self::$connection, 2);
       return self::parseResponse($reply);
    }      
    
    private static function parseResponse($reply){
        
       $replies = explode(self::$separator, $reply); 
       preg_match("`\\$([0-9]+)(.*)`s", $reply, $matches);
       //var_dump($matches[2]);
       return $matches[2]; 
        
    }
   
    private static function makeCommand($command, $args){
        
               
        array_unshift($args, strtoupper($command));
        
        //var_dump($args);
        
        $text[]= "*" . count($args);
        
        foreach($args as $arg){
        
            $text[] = "\$" . strlen($arg);
            $text[] = $arg;
        }
        
        return implode(self::$separator, $text);
    }
    
    private static function _call($command, $arg ){
       
        //var_dump($arg);
        $command = self::makeCommand($command, $arg) . self::$separator;
        //log::debug($command);
        fwrite( self::$connection, $command );
        
        $response = self::readResponse();

        switch ($response){
             case 1 : return true; break;
             
             case -1: return false; break;
             
             default: return $response; break;
        }
        
    }
    
    function __destruct(){
        
        fclose(self::$connection);
    }
  }   