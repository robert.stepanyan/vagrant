<?php

// Load the Google API PHP Client Library.
require_once __DIR__ . '/google-api-php-client-2.4.0/vendor/autoload.php';



class GoogleAnalytics {
    
    private $serviceAccountName = "humpowner@humpdata.iam.gserviceaccount.com";
    private $jsonFilePath = __DIR__  . '/HumpData-7e4ae09e4629.json';
    
    public function __construct() {
        
    }
    
    public function initializeAnalytics() {
         
        // Creates and returns the Analytics Reporting service object.
        // exit("here");
        // Create and configure a new client object.
        //
        
        $client = new \Google_Client();
        $client->setApplicationName("Hello Analytics Reporting");
        $client->setAuthConfig($this->jsonFilePath);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $analytics = new \Google_Service_Analytics($client);

        return $analytics;
    }

    public function getFirstProfileId($analytics) {
      // Get the user's first view (profile) ID.

      // Get the list of accounts for the authorized user.
      $accounts = $analytics->management_accounts->listManagementAccounts();

      if (count($accounts->getItems()) > 0) {
        $items = $accounts->getItems();
        $firstAccountId = $items[0]->getId();

        // Get the list of properties for the authorized user.
        $properties = $analytics->management_webproperties
            ->listManagementWebproperties($firstAccountId);

        if (count($properties->getItems()) > 0) {
          $items = $properties->getItems();
          $firstPropertyId = $items[0]->getId();

          // Get the list of views (profiles) for the authorized user.
          $profiles = $analytics->management_profiles
              ->listManagementProfiles($firstAccountId, $firstPropertyId);

          if (count($profiles->getItems()) > 0) {
            $items = $profiles->getItems();

            // Return the first view (profile) ID.
            return $items[0]->getId();

          } else {
            throw new Exception('No views (profiles) found for this user.');
          }
        } else {
          throw new Exception('No properties found for this user.');
        }
      } else {
        throw new Exception('No accounts found for this user.');
      }
    }


   public function getInitialViewsRows($analytics, $startDate, $endDate) {
       
        // Add Analytics View ID, prefixed with "ga:"
        $analyticsViewId    = 'ga:49214741';

        $startDate          = $startDate;
        $endDate            = $endDate;
        $metrics            = 'ga:pageviews';

        $data = $analytics->data_ga->get($analyticsViewId, $startDate, $endDate, $metrics, array(
            'dimensions'    => 'ga:pagePath',
            'filters'       => 'ga:pagePath=@/ad/details/',
            'sort'          => '-ga:pageviews',
        ));    
       
        if($data) {
            return [
                'start_date'    => $startDate,
                'end_date'      => $endDate,
                'total_rows'    => $data->totalResults,
                'current_page'  => 0,
                'total_pages'   => ($data->query->maxResults > 0)? ceil(($data->totalResults / $data->query->maxResults)) : 0,
                'rows_per_page' => $data->query->maxResults
            ];
            
        }       
        return false;
   }
    
    public function getViewCounts($analytics, $startDate, $endDate, $startIndex = 1) {
        
        // Add Analytics View ID, prefixed with "ga:"
        $analyticsViewId    = 'ga:49214741';

        $startDate          = $startDate;
        $endDate            = $endDate;
        $metrics            = 'ga:pageviews';
        
        $data = $analytics->data_ga->get($analyticsViewId, $startDate, $endDate, $metrics, array(
            'dimensions'    => 'ga:pagePath',
            'filters'       => 'ga:pagePath=@/ad/details/',
            'sort'          => '-ga:pageviews',
            'start-index'   => $startIndex
        ));

        // Data 
//        $items = $data->;
        $items = $data->getRows();
        return $items;
    }
    
    public function getFiltersCounts($analytics, $startDate, $endDate, $startIndex = 1) {
         /*
         https://ga-dev-tools.appspot.com/query-explorer/
         ?start-date=2021-05-06&end-date=2021-05-18
         &metrics=ga%3AuniqueEvents&dimensions=ga%3AeventCategory%2Cga%3AeventAction%2Cga%3AeventLabel&filters=ga%3AeventAction%3D%3Dcustom_filters&start-index=1&max-results=10
         
         https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A49214741&start-date=2021-05-06&end-date=2021-05-18&metrics=ga%3Asessions&dimensions=ga%3Amedium&start-index=1&max-results=10
         */
        // Add Analytics View ID, prefixed with "ga:"
        $analyticsViewId    = 'ga:49214741';

        $startDate          = $startDate;
        $endDate            = $endDate;
        $metrics            = 'ga:uniqueEvents';
        
        $data = $analytics->data_ga->get($analyticsViewId, $startDate, $endDate, $metrics, array(
            'dimensions'    => 'ga:eventCategory,ga:eventAction,ga:eventLabel',
            'filters'       => 'ga:eventAction==custom_filters',
            'sort'          => '-ga:uniqueEvents',
            'start-index'   => $startIndex
        ));

        // Data 
//        $items = $data->;
        $items = $data->getRows();
        return $items;
    }
    
    public function getSessionsCounts($analytics, $startDate, $endDate, $startIndex = 1) {
         /*
         https://ga-dev-tools.appspot.com/query-explorer/
         ?start-date=2021-05-06&end-date=2021-05-18
         &metrics=ga%3AuniqueEvents&dimensions=ga%3AeventCategory%2Cga%3AeventAction%2Cga%3AeventLabel&filters=ga%3AeventAction%3D%3Dcustom_filters&start-index=1&max-results=10
         
         https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A49214741&start-date=2021-05-06&end-date=2021-05-18&metrics=ga%3Asessions&dimensions=ga%3Amedium&start-index=1&max-results=10
         */
        // Add Analytics View ID, prefixed with "ga:"
        $analyticsViewId    = 'ga:49214741';

        $startDate          = $startDate;
        $endDate            = $endDate;
        $metrics            = 'ga:sessions';
        
        $data = $analytics->data_ga->get($analyticsViewId, $startDate, $endDate, $metrics, array(
            'dimensions'    => 'ga:medium',
            'sort'          => '-ga:sessions',
            'start-index'   => $startIndex
        ));

        // Data 
//        $items = $data->;
        $items = $data->getRows();
        return $items;
    }
}


?>
