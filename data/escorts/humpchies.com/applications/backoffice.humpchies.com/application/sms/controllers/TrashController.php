<?php

class Sms_TrashController extends Zend_Controller_Action {
    /**
     * @var $model Model_SMS_Trash
     */
    private $model;
	public function init()
	{
		$this->model = new Model_SMS_Trash();
	}
	
	public function indexAction() 
	{
		
	}
		
	public function dataAction()
	{
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		

		$req = $this->_request;
			
					
		$filter = array(
			/*'s.status' => Model_SMS_SMS::SMS_STATUS_DELETED
			'e.showname' => $req->showname,
			'vr.status' => $status,
			'vr.inform_method' => $req->inform_method,*/
			'date_from' => $this->_getParam('date_from'),
			'date_to' => $this->_getParam('date_to'),
		);
		
		$count = 0;
		$data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter, $count);

		
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function toggleIsReadAction()
	{
		$request = $this->_request;

		$type = $this->_getParam('type');
		$set_as = $this->_getParam('set_as');
		$id = $this->_getParam('id');
		$model = new Model_SMS_SMS();
		$model->toggleIsRead($type, $set_as, $id);
		die;
	}
	
	public function deleteForeverAction()
	{
		$id = $this->_request->id;
		
		if ( !is_array($id) )
			$id = array($id);
		
		$this->model->deleteForever($id);
		
		die;
	}
}
