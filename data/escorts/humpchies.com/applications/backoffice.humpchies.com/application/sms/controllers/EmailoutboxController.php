<?php

class Sms_EmailoutboxController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->model = new Model_SMS_EmailOutbox();
	}
	
	public function indexAction() 
	{
		
	}
		
	public function dataAction()
	{
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		
		$req = $this->_request;
			
		$filter = array(
			'backend_user_id' => $req->backend_user_id,
			'email' => $req->email,
			'escort_id' => $req->escort_id,
			'status' => $req->status,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to
		);
		
		$count = 0;
		$data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter, $count);
		
		foreach ($data as $i => $item)
		{
			$data[$i]->text = strip_tags($item->text);
		}
				
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
}
