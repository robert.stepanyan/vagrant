<?php

class Sms_IndexController extends Zend_Controller_Action {

    /**
     * @var $model Model_SMS_SMS
     */
    private $model;

	public function init()
	{
		$this->model = new Model_SMS_SMS();
	}

	public function smsCallbackAction()
	{
		$this->view->layout()->disableLayout();
		$Statuses = array(
            401 => Model_SMS_SMS::SMS_STATUS_NOT_FOUND,
            402 => Model_SMS_SMS::SMS_STATUS_EXPIRED,
            403 => Model_SMS_SMS::SMS_STATUS_SENT,
            404 => Model_SMS_SMS::SMS_STATUS_DELIVERED,
            405 => Model_SMS_SMS::SMS_STATUS_UNDELIVERED,
            406 => Model_SMS_SMS::SMS_STATUS_FAILED,
            407 => Model_SMS_SMS::SMS_STATUS_REJECTED,
            408 => Model_SMS_SMS::SMS_STATUS_UNKNOWN,
            409 => Model_SMS_SMS::SMS_STATUS_QUEUE,
            410 => Model_SMS_SMS::SMS_STATUS_ACCEPTED,
            412 => Model_SMS_SMS::SMS_STATUS_STOP
        );

		$m_sms = new Model_SMS_SMS();
		$Msglds = $this->_getParam('Msgld'); //Message ID. Length of this parameter in each request may vary, but it won't be more than 32 characters long.
		$Status = $this->_getParam('status'); //Status code
		$idxs = $this->_getParam('idx'); //Optional parameter send with SMS
		$donedate = $this->_getParam('donedate',time()); // Date in UNIX timestamp of delivery report
		$to = $this->_getParam('to'); //Mobile phone number of the recipient
        //Cubix_Email::send("webmaster@humpchies.com",'HUMP SMS', var_export($this->_getAllParams(), true),true,null);
         
        if ($idxs == 'testing'){
            Cubix_Email::send("webmaster@humpchies.com",'HUMP SMS', var_export($this->_getAllParams(), true),true,null);
            die('Ok');
        }
        if (!$idxs || !$Status) {
            die;
        }
        $arrStatus = explode(',', $Status);
        $arrIdx = explode(',', $idxs);
        foreach ($arrIdx as $i => $idx) {
            $sms_exists = $m_sms->get($idx);
            if (!$sms_exists) {
                continue;
            }
            $status = $Statuses[$arrStatus[$i]];
            $m_sms->updateStatus($idx, $status);

            if ($status == Model_SMS_SMS::SMS_STATUS_DELIVERED) {
                $m_sms->updateDeliveryDate($idx, $donedate);
            }

        }

        die("Ok");
	}

	public function smsCallback3Action()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;
		$m_sms = new Model_SMS_SMS();

		$id = $req->msg_id;
		$status = intval($req->status);

		$sms_exists = $m_sms->get($id);

		//file_put_contents('/tmp/sms-callback.log', var_export($sms_exists, true) );

		if ( ! $sms_exists ) {
			die;
		}

		$m_sms->updateStatus($id, $status);

		if ( $status == 2 ) {
			$m_sms->updateDeliveryDate($id, time());
		}
		/*elseif ( $status == 2 ) {

		}
		elseif ( $status == 3 ) {

		}*/

		die;
	}

	public function smsCallback2Action()
	{
		$this->view->layout()->disableLayout();

		$req = $this->_request;
		$m_sms = new Model_SMS_SMS();

		$id = intval($req->cliMsgId);
		$status = intval($req->status);

		$sms_exists = $m_sms->get($id);

		if ( ! $sms_exists ) {
			die;
		}

		$m_sms->updateStatus($id, $status);

		if ( $status == 1 ) {
			$m_sms->updateDeliveryDate($id, time());
		}
		elseif ( $status == 2 ) {

		}
		elseif ( $status == 3 ) {

		}

		die;
	}

	public function hex_decode_string($source)
	{
		$string = '';

		for ( $i = 0; $i < strlen($source) - 1; $i += 4 ) {
			$num = (($i + 4) / 4);
			$hex = substr($source, $i, 4);
			list($hex1, $hex2) = array(substr($hex, 0, 2), substr($hex, 2));

			$dec = base_convert($hex, 16, 10);
			$char = html_entity_decode('&#' . $dec . ';', ENT_NOQUOTES, 'UTF-8');

			$string .= $char;
		 }

		 return $string;
	}

	public function smsInboxAction()
	{
		$this->view->layout()->disableLayout();

		$m_inbox = new Model_SMS_Inbox();

        $sms_from   = $this->_getParam('sms_from');
        $sms_to     = $this->_getParam('sms_to');
        $sms_date   = $this->_getParam('sms_date');
        $sms_text   = $this->_getParam('sms_text');

//        if (strpos($sms_from, '1') !== 0){
//            $data['orig'] = "1{$sms_from}";
//        }
        $sms_text = utf8_encode($sms_text);

		file_put_contents('/tmp/sms-data.log', var_export($this->_getAllParams(), true) . ':' . $sms_text . "\n----------------------------\n\n", FILE_APPEND);

		if ( ! $sms_from || ! $sms_to ) die("ok");

		if ( ! strlen($sms_text) ) die("ok");

		$data = array(
			'orig' => $sms_from,
			'recipient' => $sms_to,
			'date' => $sms_date,
			'text' => $sms_text,
		);

		$m_inbox->saveToInbox($data);

		die("ok");
	}

	public function indexAction()
	{

	}

	public function testAction()
	{
		$conf = Zend_Registry::get('system_config');

		$data = new Cubix_Form_Data($this->_request);

		$data->setFields(array(
			'from' => '',
			'to' => '',
			'sms' => ''
		));

		$data = $data->getData();

		$validator = new Cubix_Validator();

		if ( ! $data['from'] ) {
			$validator->setError('from', 'From Number is required');
		}

		if ( ! $data['to'] ) {
			$validator->setError('to', 'To number is required');
		}

		if ( ! $data['sms'] ) {
			$validator->setError('sms', 'SMS is required');
		}

		if ( $validator->isValid() ) {
			if ( $this->_getParam('send') && $this->_request->isPost() ) {
                $recipients = array("+{$data['to']}");
                $apiToken = $conf['smssapi']['apiToken'];
                $sms = Cubix_SmsApi_Send_SmsBulk::withMessage($recipients, $data['sms']);
//                $sms->test= 1;
                $sms->idx= array('testing');
                try{
                    $resp = (new Cubix_SmsApi_HttpClient())
                        ->smsapiComService($apiToken)
                        ->smsFeature()
                        ->sendSmsBulk($sms);
                    var_dump($resp);
                    die;
                }catch(Exception $exception){
                    die(json_encode(array(
                        'status' => 'error',
                        'msgs' => $exception->getMessage()
                    )));
                }
			}
			else {
				$this->view->data = $data;
			}
		}
		else {
			$status = $validator->getStatus();
			print_r($status);
			die;
		}

		// die;
	}

	public function sendAction()
	{
       
		$conf = Zend_Registry::get('system_config');

		if ( $this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);

			$data->setFields(array(
				'sms_inbox_id' => '',
				'ad_ids' => '',
				'phone_number' => '',
				'phone_number_original' => '',
				'text' => '',
				'send_time' => '',
				'advertisement_numbers' => ''

			));

			$data = $data->getData();
          

			$validator = new Cubix_Validator();

			if($advertisement_numbers = $data['advertisement_numbers']) {
                $advertisement_numbers = unserialize($advertisement_numbers);
            } else {

                if (!$data['phone_number']) {
                    $validator->setError('phone_number', 'Required');
                }
            }
            $data['ad_ids'] = array();
            
            if(is_array($advertisement_numbers)){
                foreach( $advertisement_numbers as $i => $advertisement_number ) {
                    $data['ad_ids'][] = $advertisement_number['id'];
                }
            }

			if ( ! $data['text'] ) {
				$validator->setError('text', 'Required');
			}
		}

		if ( $validator->isValid() ) {

			if(isset($data['sms_inbox_id']) AND $data['phone_number'] == $data['phone_number_original']){
				$modelsms_inbox = new Model_SMS_Inbox();
				$modelsms_inbox->sent($data['sms_inbox_id']);
			}
			$model_sms = new Model_SMS_Outbox();
			$originator = 'testing';

			$data['phone_from'] = $originator;

            $apiToken = $conf['smssapi']['apiToken'];

            $b_user = Zend_Auth::getInstance()->getIdentity();
            $data['sender_sales_person'] = $b_user->id;

            $idx = array();
            $recipients = array();
            if (is_array($advertisement_numbers)) {
                foreach ($advertisement_numbers as $item => $i) {
                    $data['phone_number'] = $i['phone_number'];
                    $data['ad_id'] = $i['id'];
                    $id = $model_sms->save($data);
                    array_push($idx, $id);
                    if ($i['phone_number'] == '37494081906' || $i['phone_number'] == '3749408190'){
                        array_push($recipients, '+37494081906');
                    }else{
                        array_push($recipients, '+1'.$i['phone_number']);
                    }

                }
            }
            elseif (!empty($data['phone_number'])) {
                $idx[] = $model_sms->save($data);
                if ($data['phone_number'] == '0736622195' || $data['phone_number'] == '3749408190'){
                    array_push($recipients, '+40736622195');
                }else{
                    if(preg_match("`^\\+`is",$data['phone_number'])){ 
                        array_push($recipients, $data['phone_number']);
                    }else{
                        array_push($recipients, "+1" . $data['phone_number']);
                    }   
                }
            }

            if (!empty($recipients)){
                $sms = Cubix_SmsApi_Send_SmsBulk::withMessage($recipients, $data['text']);
                $sms->idx= $idx;
                $sms->from= '12046900460';
               
                try{
                    (new Cubix_SmsApi_HttpClient())
                        ->smsapiComService($apiToken)
                        ->smsFeature()
                        ->sendSmsBulk($sms);
                }catch(Exception $exception){
                    die(json_encode(array(
                        'status' => 'error',
                        'msgs' => $exception->getMessage()
                    )));
                }
            }
		}

		die(json_encode($validator->getStatus()));
	}

	public function statusAction()
	{

	}

	public function composeAction()
	{
		$this->view->layout()->disableLayout();
		$ids = $this->_getParam('id');
        $sms_id = $this->_getParam('sms_id');
		if(!empty($sms_id)){
			$this->view->sms_inbox_id = $sms_id;
		}
		$model_advertisements = new Model_Advertisements();


		if ( count($ids) ) {
			$this->view->ids = $ids;
            $advertisements = array();
            $advertisement_numbers = array();
			foreach( $ids as $id ) {
				$advertisement = $model_advertisements->get($id);

                if($advertisement->phone){
                    $advertisement_info = array(
                        'id' => $id,
                        'phone_number' => $advertisement->phone
                    );
                    $advertisement_numbers[] = $advertisement_info;
                    $advertisements[] = array(
                      'phone'=>$advertisement->phone,
                      'title'=>$advertisement->title,
                    );
                }
			}
            $this->view->advertisements = $advertisements;
            $this->view->advertisement_numbers = serialize($advertisement_numbers);
		}

		$ad_id = $this->_getParam('escort_id');
        if(isset($escort_id)){
			$this->view->phone_from = $model_advertisements->getPhoneByAdId($ad_id);
		}
		else{
		    $phone_from= $this->_getParam('phone_from');
			$this->view->phone_from = urldecode($phone_from);
		}
		$this->view->no_btn = intval($this->_getParam('no_btn'));
        $this->view->gyot = $this->gyot;
		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if (!in_array($bu_user->type, array('superadmin', 'admin','data entry plus')))
			$sales_user_id = $bu_user->id;
		else
			$sales_user_id = null;

		$modelSms = new Model_SMS_SMS();
		$this->view->temlates = $modelSms->getTemplates($sales_user_id);
	}

	public function templateAction()
	{
		$this->view->layout()->disableLayout();
		$modelSms = new Model_SMS_SMS();

		$id = $this->_request->id;
		$template = '';

		if (intval($id) > 0)
			$template = $modelSms->getTemplate($id);

		echo $template;
		die;
	}


	public function readAction()
	{
		$this->view->layout()->disableLayout();

		$id = $this->view->id = $this->_request->id;
		$type = $this->view->type = $this->_request->type;

		if ( $type != 'outbox' )
			$this->model->toggleIsRead($type, Model_SMS_SMS::SMS_IS_READ, array($id));

		switch($type)
		{
			case 'inbox':
				$model = new Model_SMS_Inbox();
				$this->view->sms = $model->get($id);
			break;
			case 'outbox':
				$model = new Model_SMS_Outbox();
				$this->view->sms = $model->get($id);
			break;
			case 'saved':
				$model = new Model_SMS_Saved();
				$this->view->sms = $model->get($id);
			break;
			case 'trash':
				$model = new Model_SMS_Trash();
				$this->view->sms = $model->get($id);
			break;
		}
	}

	public function adsListAction()
	{
		$this->view->layout()->disableLayout();
	}

	public function adsListDataAction()
	{
    
		$this->view->layout()->disableLayout();

		$filter = array(
			// Personal
			'ads.title' => $this->_getParam('title'),
			'ads.phone' => $this->_getParam('phone_parsed'),
			'showname_is_not_null' => true,
			// Status
			'ads.status' => $this->_getParam('status'),
			'excl_status' => Model_Advertisements::STATUS_DELETED
		);
        
        $count = 0;
		$model = new Model_Advertisements();
		$data = $model->getAll(
			$this->_getParam('page'),
			$this->_getParam('per_page'),
			$filter,
			$this->_getParam('sort_field'),
			$this->_getParam('sort_dir'),
			$count
		);

       
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);

		die(json_encode($this->view->data));

	}

	public function readThreadAction()
	{
		$this->view->layout()->disableLayout();

		$id = $this->view->id = $this->_request->id;
		$phone_number = $this->view->phone_number = $this->_request->phone_number;
		$this->view->is_ajax = $this->_request->is_ajax;

		$model = new Model_SMS_Inbox();
		$this->view->thread = $model->getThread($phone_number);

		$this->model->toggleIsRead('inbox', Model_SMS_SMS::SMS_IS_READ, array($id));

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if (!in_array($bu_user->type, array('superadmin', 'admin','data entry plus')))
			$sales_user_id = $bu_user->id;
		else
			$sales_user_id = null;

		$modelSms = new Model_SMS_SMS();
		$this->view->temlates = $modelSms->getTemplates($sales_user_id);
	}

}
