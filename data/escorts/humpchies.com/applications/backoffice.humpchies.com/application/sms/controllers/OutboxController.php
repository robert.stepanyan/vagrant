<?php



class Sms_OutboxController extends Zend_Controller_Action {
    /**
     * @var $model Model_SMS_Outbox
     */
    private $model;

	public function init()
	{
		$this->model = new Model_SMS_Outbox();
	}
	
	public function indexAction() 
	{
		$this->view->can_change_sender = true;
	}
		
	public function dataAction()
	{
			
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');

		$filter = array(
			'ad_title' => $this->_getParam('showname'),
			'status' => $this->_getParam('status'),
			'escort_id' => $this->_getParam('escort_id'),
			'sender_sales_person' => $this->_getParam('sender_sales_person'),
			'ad_status' => $this->_getParam('e_status'),
			'phone_to' => $this->_getParam('phone_to'),
			'year' => $this->_getParam('year'),
			'date_from' => $this->_getParam('date_from'),
			'date_to' => $this->_getParam('date_to'),
			'is_spam'	=> $this->_getParam('is_spam')
		);
		$count = 0;
		$data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter, $count);
		
		foreach ($data as $d)
		{
			$d->status = Model_Advertisements::$supported_statuses[$d->ad_status];
			$phone = $d->phone ? $d->phone : $d->phone_to;
			$this->model->getAdsByPhone($phone, $d);
		}

		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function deleteAction()
	{
		$id = $this->_request->id;
		
		if ( !is_array($id) )
			$id = array($id);
		
		$this->model->delete($id);
		
		die;
	}
	
	public function resendAction() 
	{
		$conf = Zend_Registry::get('system_config');

		$ids = $this->_getParam('id');
		
		if ( !is_array($ids) ){
            $ids = array($ids);
        }

		$resend_sms = $this->model->getByIds($ids);
		
		$model_sms = new Model_SMS_Outbox();

		$originator = 'testing';

        $apiToken = $conf['smssapi']['apiToken'];

        $smsFeature = (new Cubix_SmsApi_HttpClient())
            ->smsapiComService($apiToken)
            ->smsFeature();

        $b_user = Zend_Auth::getInstance()->getIdentity();
        $idx = array();
        $recipients = array();
        $messages = array();
        foreach ($resend_sms as $r_sms) {
            $r_sms = (array)$r_sms;
            $r_sms['sender_sales_person'] = $b_user->id;
            $id = $model_sms->save($r_sms);
            $phone_number = strpos($r_sms['phone_number'], "+1") === 0 ? $r_sms['phone_number'] : '+1'.$r_sms['phone_number'];
            $r_sms['phone_number']= $phone_number;
            array_push($idx, $id);
            array_push($recipients, $phone_number);
            array_push($messages,$r_sms['text']);
        }

        if (!empty($recipients)){
            $sms = Cubix_SmsApi_Send_SmsBulk::withMessage($recipients, '[%1%]')
            ->setParams([1=>$messages]);
            $sms->idx= $idx;
            try{
                $smsFeature->sendSmsBulk($sms);
            }catch(Exception $exception){
                die(json_encode(array(
                    'status' => 'error',
                    'msgs' => $exception->getMessage()
                )));
            }
        }

		die;
	}		
}
