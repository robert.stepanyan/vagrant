<?php

class Sms_InstantBookController extends Zend_Controller_Action {
  public function init() {
    $this->model = new Model_SMS_InstantBook();
  }

  public function indexAction() {
    
  }

  public function requestsDataAction() {
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		

		$req = $this->_request;
			
					
		$filter = array(
      'escort_id' => $req->escort_id,
      'phone_number' => $req->phone_number,
			'status' => $req->status,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'application_id' => $req->application_id
		);
		
		$count = 0;
		$data = $this->model->getAll($page, $per_page, $sort_field, $sort_dir, $filter, $count);
		
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
  }

  public function escortsAction() {

  }

  public function escortsDataAction() {
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		

		$req = $this->_request;
			
					
		$filter = array(
      'escort_id' => $req->escort_id,
      'location' => $req->location,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to,
			'application_id' => $req->application_id
		);
		
		$count = 0;
		$data = $this->model->getAllEscorts($page, $per_page, $sort_field, $sort_dir, $filter, $count);
		
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
  }

}