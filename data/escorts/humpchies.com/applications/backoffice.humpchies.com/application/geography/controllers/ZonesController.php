<?php

class Geography_ZonesController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Geography_Zones();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$country_id = intval($this->_getParam('country_id'));
		$region_id = intval($this->_getParam('region_id'));
		$city_id = intval($this->_getParam('city_id'));
		
		$name = trim($this->_getParam('name'));
		
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		$count = 0;
		
		$data = $this->model->getAll( $name);
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function ajaxAction()
	{
		$city_id = intval($this->_getParam('city_id'));
		
		echo json_encode(array(
			'data' => $this->model->ajaxGetAll($city_id)
		));
		die;
	}
	
	public function createAction()
	{
		$this->view->name = $name = $this->_getParam('name');
	
		if ( $this->_request->isPost() ) {
			
			$validator = new Cubix_Validator();
			
			if ( ! $name ) {
				$validator->setError('name', 'Zone name is required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_Geography_CityzoneItem(
					array(
						'name' =>  $name
					)
				));
			}
			
			echo json_encode($validator->getStatus());
			die;
		}
	}
	
	public function editAction()
	{
		$id = intval($this->_getParam('id'));
		
		if ( $this->_request->isPost() ) {
			$city_id = intval($this->_getParam('city_id'));
			$titles = Cubix_I18n::getValues($this->_request->getParams(), 'title', '');
			
			$validator = new Cubix_Validator();
			
			if ( ! $city_id ) {
				$validator->setError('city_id', 'Please select city');
			}
			
			if ( ! strlen($titles['title_en']) ) {
				$validator->setError('title_en', 'English title is required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_Geography_CityzoneItem(array_merge(
					array(
						'id' => $id,
						'city_id' => $city_id
					),
					$titles
				)));
			}
			
			echo json_encode($validator->getStatus());
			die;
		}
		else {
			$this->view->cityzone = $this->model->get($id);
		}
	}
	
	public function removeAction()
	{
		$id = intval($this->_getParam('id'));
		
		$this->model->remove($id);
	}
}

