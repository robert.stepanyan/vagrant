<?php

class Geography_CitiesController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Geography_Cities();

	}
	
	public function indexAction()
	{
	
	}
	
	public function dataAction()
	{

		$country_id = intval($this->_getParam('country_id'));
		$title = trim($this->_getParam('title'));
		$no_timezone = $this->_getParam('no_timezone');
		if ( ! $country_id ) {
			$country_id = null;
		}
		
		$region_id = intval($this->_getParam('region_id'));
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		$count = 0;
		$sort_field = 'id'; $sort_dir = 'ASC';
		$data = $this->model->getAll($region_id, $country_id, $page, $per_page, $sort_field, $sort_dir, $count, $title);
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}

	public function ajaxHomeCitiesSearchAction()
	{
		$name = trim($this->_getParam('city_search_name'));
		$lng = trim($this->_getParam('lng'));

		if ( ! strlen($name) ) {
			die(json_encode(array()));
		}
		$model = new Cubix_Geography_Cities();

		$cities = $model->ajaxGetAllCities($name, $lng);

		$tmp_cities = array();
		if ( count($cities) ) {
			foreach ( $cities as $i => $city ) {
				$tmp_cities[$i] = array('id' => $city->id, 'name' => $city->title . ' (' . $city->country_title . ')');
			}
		}

		echo json_encode($tmp_cities);
		die;
	}

	public function ajaxGetEscortCitiesAction()
	{
		$escort_id = intval($this->_getParam('escort_id'));

		$m_escorts = new Model_Escorts();
		$escort = $m_escorts->get($escort_id);

		$escort_cities = $escort->getCities();

		foreach( $escort_cities as $k => $esc_city ) {
			$escort_cities[$k]->is_base = 0;
			if ( $esc_city->id == $escort->city_id ) {
				$escort_cities[$k]->is_base = 1;
			}
		}
		
		die(json_encode($escort_cities));
	}
	
	public function ajaxAction()
	{
		$country_id = intval($this->_getParam('country_id'));
		$region_id = intval($this->_getParam('region_id'));
		$city_criteria = $this->_getParam('city_criteria');
		
		echo json_encode(array(
			'data' => $this->model->ajaxGetAll($region_id, $country_id, $city_criteria)
		));
		die;
	}
	
	public function createAction()
	{

		
		$url_slug = $this->_getParam('url_slug');
		$title = $this->_getParam('title');
		$zone_id = $this->_getParam('zone_id');

		if ( $this->_request->isPost() ) {
			
			$validator = new Cubix_Validator();

			if ( ! $title ) {
				$validator->setError('title', 'Title is required');
			}

			if ( ! $zone_id ) {
				$validator->setError('zone_id', 'Zone is required');
			}
		
			if( preg_match('/[^a-zA-Z0-9-]+/', $url_slug) ) {
				$validator->setError('url_slug', 'Only alphanumeric symbols');
			}
			
			if ( $validator->isValid() ) {

				$save_data = array(
					'zone_id' => $zone_id,
					'slug'	=> $url_slug,
					'name'	=> $title
				);

				$this->model->save(new Model_Geography_CityItem($save_data));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$id = intval($this->_getParam('id'));
		
		if ( $this->_request->isPost() ) {
			
			
			$title = $this->_getParam('title');
			$url_slug = $this->_getParam('url_slug');
			$zone_id = $this->_getParam('zone_id');
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($title) ) {
				$validator->setError('title', 'English title is required');
			}

			if( preg_match('/[^a-zA-Z0-9-]+/', $url_slug) ) {
				$validator->setError('url_slug', 'Only alphanumeric symbols');
			}
			
			if ( ! $zone_id ) {
				$validator->setError('zone_id', 'Zone is required');
			}
			
			if ( $validator->isValid() ) {

				$save_data = array(
					'id'	=> $id,
					'zone_id' => $zone_id,
					'slug'	=> $url_slug,
					'name'	=> $title
				);

				$this->model->save(new Model_Geography_CityItem($save_data));
			}
			
			die(json_encode($validator->getStatus()));
		}
		else {
			$this->view->city = $this->model->get($id);
		}
	}
	
	
	public function removeAction()
	{
		$id = intval($this->_getParam('id'));
		
		$this->model->remove($id);
	}
}