<?php
class Model_Advertisements extends Cubix_Model
{
	protected $_table = 'ads';
	
	// CONSTANTS
    // =========
    
    const CATEGORY_ESCORT                           = 'escort';
    const CATEGORY_ESCORT_AGENCY                    = 'escort-agency';
    const CATEGORY_EROTIC_MASSAGE                   = 'erotic-massage';
    const CATEGORY_EROTIC_MASSAGE_PARLOR            = 'erotic-massage-parlor'; 
	
	const LOCATION_OUTCALL                          = 'outcalls';
    const LOCATION_INCALL                           = 'incalls';
    const LOCATION_INCALL_OUTCALL                   = 'both';
    const LOCATION_UNKNOWN                          = 'unknown';
	
	const STATUS_ACTIVE								= 'active';
    const STATUS_INACTIVE                           = 'inactive';
    const STATUS_NEW								= 'new';
    const STATUS_DELETED                            = 'delete';
	const STATUS_BANNED                             = 'banned';
        const STATUS_PAUSED                             = 'paused';

    const STATUS_FAKE                               = 1;
    const STATUS_NOT_FAKE                           = 0;

	// GEO Zones
    // =========
    const GEO_ZONE_MONTREAL_METRO               = 'montreal_metro';
    const GEO_ZONE_QUEBEC_CENTRE                = 'quebec_centre';
    const GEO_ZONE_EASTERN_TOWNSHIPS            = 'eastern_townships';
    const GEO_ZONE_ONTARIO                      = 'ontario';
    const GEO_ZONE_OTTAWA_METRO                 = 'ottawa_metro';
    const GEO_ZONE_NEW_BRUNSWICK                = 'new_brunswick';



	
	public static $supported_categories = array(
		
		self::CATEGORY_ESCORT => 'escort',
		self::CATEGORY_ESCORT_AGENCY => 'escort agency',
		self::CATEGORY_EROTIC_MASSAGE => 'erotic massage',
		self::CATEGORY_EROTIC_MASSAGE_PARLOR => 'erotic massage parlor'
	);
	
	public static $supported_locations = array( 
		self::LOCATION_INCALL,
		self::LOCATION_OUTCALL,
		self::LOCATION_INCALL_OUTCALL,
		self::LOCATION_UNKNOWN
	);
	
	public static $supported_statuses = array(
		self::STATUS_ACTIVE,
		self::STATUS_INACTIVE,   
		self::STATUS_NEW,
		self::STATUS_DELETED,
		self::STATUS_BANNED,
                self::STATUS_PAUSED
	);
	
	public static $supported_cities = array(
		'acton-vale'                    => 'Acton Vale',
		'alma'                          => 'Alma',
		'anjou'                         => 'Anjou',
		'baie-comeau'                   => 'Baie-Comeau',
		'beauport'                      => 'Beauport',
		'beloeil'                       => 'Beloeil',
		'berthierville'                 => 'Berthierville',
		'blainville'                    => 'Blainville',
		'bois-des-filion'               => 'Bois-des-Filion',
		'boisbriand'                    => 'Boisbriand',
		'boucherville'                  => 'Boucherville',
		'bromont'                       => 'Bromont',
		'brossard'                      => 'Brossard',
		'burlington'                    => 'Burlington',
		'campbellton'                   => 'Campbellton',
		'candiac'                       => 'Candiac',
		'cap-de-la-madeleine'           => 'Cap-de-la-Madeleine',
		'carignan'                      => 'Carignan',
		'chambly'                       => 'Chambly',
		'charlemagne'                   => 'Charlemagne',
		'charlesbourg'					=> 'Charlesbourg',
		'charny'                        => 'Charny',
		'chibougamau'                   => 'Chibougamau',
		'chicoutimi'                    => 'Chicoutimi',
		'chateauguay'                   => 'Ch�teauguay',
		'contrecoeur'                   => 'Contrecoeur',
		'cornwall'                      => 'Cornwall',
		'cote-saint-luc'                => 'C�te-Saint-Luc',
		'delson'                        => 'Delson',
		'deux-montagnes'                => 'Deux-Montagnes',
		'dollard-des-ormeaux'           => 'Dollard-des-Ormeaux',
		'dorval'                        => 'Dorval',
		'drummondville'                 => 'Drummondville',
		'durham-sud'                    => 'Durham-Sud',
		'edmundston'                    => 'Edmundston',
		'fermont'                       => 'Fermont',
		'gatineau'                      => 'Gatineau',
		'granby'                        => 'Granby',
		'greenfield-park'               => 'Greenfield-Park',
		'hawkesbury'                    => 'Hawkesbury',
		'joliette'                      => 'Joliette',
		'jonquiere'                     => 'Jonqui�re',
		'l-ancienne-lorette'            => "L'Ancienne-Lorette",
		'l-assomption'                  => "L'Assomption",
		'l-epiphanie'                   => "L'�piphanie",
		'la-prairie'                    => 'La-Prairie',
		'lasalle'                       => 'LaSalle',
		'lac-simon'                     => 'Lac-Simon',
		'lachine'                       => 'Lachine',
		'lanoraie'                      => 'Lanoraie',
		'laval'                         => 'Laval',
		'lavaltrie'						=> 'Lavaltrie',
		'lemoyne'                       => 'LeMoyne',
		'les-coteaux'					=> 'Les Coteaux',
		'longue-rive'                   => 'Longue-Rive',
		'longueuil'                     => 'Longueuil',
		'levis'                         => 'L�vis',
		'magog'                         => 'Magog',
		'marieville'                    => 'Marieville',
		'mascouche'                     => 'Mascouche',
		'matane'                        => 'Matane',
		'massena'						=> 'Massena',
		'mcmasterville'                 => 'McMasterville',
		'mirabel'                       => 'Mirabel',
		'mont-laurier'                  => 'Mont-Laurier',
		'mont-royal'                    => 'Mont-Royal',
		'mont-saint-hilaire'            => 'Mont-Saint-Hilaire',
		'mont-tremblant'				=> 'Mont-Tremblant',
		'montpellier'                   => 'Montpellier',
		'montreal'                      => 'Montr�al',
		'montreal-est'                  => 'Montr�al-Est',
		'montreal-nord'                 => 'Montr�al-Nord',
		'montreal-ouest'                => 'Montr�al-Ouest',
		'napierville'                   => 'Napierville',
		'newport'						=> 'Newport',
		'ottawa'                        => 'Ottawa',
		'pierrefonds'                   => 'Pierrefonds',
		'pierreville'                   => 'Pierreville',
		'plattsburgh'					=> 'Plattsburgh',
		'pointe-calumet'                => 'Pointe-Calumet',
		'pointe-claire'					=> 'Pointe-Claire',
		'pont-rouge'                    => 'Pont-Rouge',
		'portneuf'                      => 'Portneuf',
		'quebec'                        => 'Qu�bec',
		'rawdon'                        => 'Rawdon',
		'repentigny'                    => 'Repentigny',
		'richelieu'                     => 'Richelieu',
		'rigaud'                        => 'Rigaud',
		'rimouski'                      => 'Rimouski',
		'riviere-du-loup'               => 'Rivi�re-du-Loup',
		'rosemere'                      => 'Rosem�re',
		'rouyn-noranda'					=> 'Rouyn-Noranda',
		'roxboro'						=> 'Roxboro',
		'saguenay'						=> 'Saguenay',
		'saint-augustin'				=> 'Saint-Augustin',
		'saint-basile'                  => 'Saint-Basile',
		'saint-blaise-sur-richelieu'    => 'Saint-Blaise-sur-Richelieu',
		'saint-constant'                => 'Saint-Constant',
		'saint-cyrille-de-wendover'     => 'Saint-Cyrille-de-Wendover',
		'saint-esprit'                  => 'Saint-Esprit',
		'saint-eustache'                => 'Saint-Eustache',
		'saint-gabriel-de-valcartier'   => 'Saint-Gabriel-de-Valcartier',
		'saint-georges'                 => 'Saint-Georges',
		'saint-hippolyte'				=> 'Saint-Hippolyte',
		'saint-hubert'                  => 'Saint-Hubert',
		'saint-hyacinthe'               => 'Saint-Hyacinthe',
		'saint-jean-baptiste'           => 'Saint-Jean-Baptiste',
		'saint-jean-sur-richelieu'      => 'Saint-Jean-sur-Richelieu',
		'saint-jerome'                  => 'Saint-J�r�me',
		'saint-lambert'                 => 'Saint-Lambert',
		'saint-laurent'                 => 'Saint-Laurent',
		'saint-lazare'					=> 'Saint-Lazare',
		'saint-lin-laurentides'         => 'Saint-Lin-Laurentides',
		'saint-leonard'                 => 'Saint-L�onard',
		'saint-mathias-sur-richelieu'   => 'Saint-Mathias-sur-Richelieu',
		'saint-sauveur'                 => 'Saint-Sauveur',
		'sainte-angele-de-monnoir'      => 'Sainte-Ang�le-de-Monnoir',
		'sainte-foy'                    => 'Sainte-Foy',
		'sainte-marguerite'             => 'Sainte-Marguerite',
		'sainte-marthe-sur-le-lac'      => 'Sainte-Marthe-sur-le-Lac',
		'sainte-sophie'                 => 'Sainte-Sophie',
		'sainte-therese'                => 'Sainte-Th�r�se',
		'saint-zotique'					=> 'Saint-Zotique',
		'sainte-agathe-des-monts'		=> 'Sainte-Agathe-des-Monts',
		'sainte-anne-des-lacs'			=> 'Sainte-Anne-des-Lacs',
		'sainte-julie'					=> 'Sainte-Julie',
		'sainte-marie'					=> 'Sainte-Marie',
		'salaberry-de-valleyfield'      => 'Salaberry-de-Valleyfield',
		'sept-iles'                     => 'Sept-�les',
		'shawinigan'                    => 'Shawinigan',
		'sherbrooke'                    => 'Sherbrooke',
		'sorel-tracy'					=> 'Sorel-Tracy',
		'terrebonne'                    => 'Terrebonne',
		'thetford-mines'                => 'Thetford-Mines',
		'trois-rivieres'                => 'Trois-Rivi�res',
		'trois-rivieres-ouest'          => 'Trois-Rivi�res-Ouest',
		'val-d-or'                      => "Val-d'Or",
		'varennes'						=> 'Varennes',
		'vaudreuil-dorion'              => 'Vaudreuil-Dorion',
		'verdun'                        => 'Verdun',
		'victoriaville'                 => 'Victoriaville',
		'yamaska'                       => 'Yamaska'
	);
        
    public static function getSupportedCitiesV2( $only_slug_title = false ){
        $selecting_items = ( $only_slug_title ) ? 'c.slug, c.name' : 'c.*';

        return parent::_fetchAll(
            'SELECT ' . $selecting_items . ' FROM cities c'
        );
    }

    public static function getCitiesSlugNames(){
        $data = array();
        $cities_data = self::getSupportedCitiesV2(true);

        foreach($cities_data as $k => $v){
            $data[$v->slug] = $v->name;
        }

        return $data;
    }
    
    public static function getCitiesByZoneId($zone_id){
        return parent::_fetchAll(
            'SELECT * FROM cities WHERE zone_id = ' . $zone_id
        );
    }
    
    public static function getZoneByCityIdOrName($city_id_or_name){
        return parent::_fetchRow(
            'SELECT z.id, z.name FROM zones
            INNER JOIN cities c ON c.zone_id = z.id
            WHERE c.id = ' . $city_id . ' OR c.name = ' . $city_id
        );
    }
	
	public static function getDefinedZoneForCity($cityName){
        $cityName = strtolower($cityName);

        $definedZones = self::getGeoZoneDefinitions();

        foreach($definedZones as $zoneName => $definedCities) {
            foreach($definedCities as $definedCity) {
                if($definedCity === $cityName) {
                    return $zoneName;
                }
            }
        }

       // return (self::$cityZones[$cityName] = FALSE);
        return false;
    }

    /**
     * Returns cities grouped by Geographical zones
     *
     * @return array A multidimentional array containing the zone name and the cities within it
     */
    public static function getGeoZoneDefinitions() {

        return array(
            self::GEO_ZONE_EASTERN_TOWNSHIPS => array(
                'sherbrooke',            // Eastern Townships
                'newport'
            ),      
            self::GEO_ZONE_ONTARIO => array(
               'burlington',
               'hawkesbury',
               'gatineau',
               
            ),
             self::GEO_ZONE_OTTAWA_METRO => array(
               'lac-simon',
               'ottawa',
            ),
            self::GEO_ZONE_NEW_BRUNSWICK=> array(
               'campbellton',
               'edmundston'
            ),                           // =================
            self::GEO_ZONE_QUEBEC_CENTRE => array(
                'quebec',
                'drummondville',        // Centre du Quebec + Saint-Hyacinthe
                'becancour',            // ==================================
                'kingsey-falls',
                'nicolet',
                'jonquiere',
                'levis',
                'plessisville',
                'longue-rive',
                'mont-laurier',
                'mont-tremblant',
                'princeville',
                'riviere-du-loup',
                'saint-cyrille-de-wendover',
                'saint-germain-de-grantham',
                'victoriaville',
                'warwick',
                'charny',
                'chibougamau',
                'portneuf',
                'saint-gabriel-de-valcartier',
                'rimouski',
                'sainte-marie',
                'val-d-or',
                'sainte-foy',
                'sept-iles',
                'pont-rouge',
                'montpellier',
                'l-ancienne-lorette',
                'matane',
                'rouyn-noranda',
                'saint-jerome',
                'saguenay',
                'saint-augustin',
                'chicoutimi',
                'charlesbourg',
                'saint-basile',
                'sainte-marguerite',
                'salaberry-de-valleyfield',
                'saint-georges',
                'alma',
                'beauport',
                'thetford-mines',
                'fermont',
                'shawinigan',
                'cap-de-la-madeleine',
                'saint-hyacinthe'),
            self::GEO_ZONE_MONTREAL_METRO => array(
                'contrecoeur',         // South Shore
                'sainte-julie',        // ===========
                'varennes',
                'pierreville',
                'saint-lazare',
                'saint-hippolyte',
                'beloeil',
                'saint-blaise-sur-richelieu',
                'plattsburgh',
                'carignan',
                'chambly',
                'marieville',
                'napierville',
                'saint-zotique',
                'trois-rivieres',
                'trois-rivieres-ouest',
                'sainte-agathe-des-monts',
                'vaudreuil-dorion',
                'sainte-anne-des-lacs',
                'sainte-sophie',
                'saint-esprit',
                'sorel-tracy',
                'mcmasterville',
                'massena',
                'mont-saint-hilaire',
                'saint-mathias-sur-richelieu',
                'otterburn-park',
                'magog',
                'saint-basile-le-grand',
                'brossard',
                'greenfield-park',
                'lemoyne',
                'longueuil',
                'saint-hubert',
                'saint-Lambert',
                'candiac',
                'chateauguay',
                'saint-jean-baptiste',
                'saint-lin-laurentides',
                'delson',
                'la-prairie',
                'lery',
                'mercier',
                'saint-constant',
                'sainte-catherine',
                'blainville',          // North Shore
                'bois-des-filion',     // ===========
                'boisbriand',
                'deux-montagnes',
                'lorraine',
                'mirabel',
                'oka',
                'saint-jean-sur-richelieu',
                'laval',
                'pointe-calumet',
                'rosemere',
                'rawdon',
                'richelieu',
                'rigaud',
                'saint-eustache',
                'saint-joseph-du-Lac',
                'sainte-anne-des-plaines',
                'sainte-marthe-sur-le-lac',
                'sainte-therese',
                'charlemagne',
                'l-assomption',
                "l'assomption",
                'mascouche',
                'repentigny',
                'saint-sulpice',
                'terrebonne',
                'joliette',
                'montreal',            // Montreal
                'montreal-est',        // ========
                'montreal-ouest',
                'montreal-nord',
                'beaconsfield',
                'kirkland',
                'pointe-claire',
                'anjou',
                "baie-d'urfe",
                'baie-d-urfe',
                'dollard-des-ormeaux',
                'saint-sauveur',
                'dorval',
                'sainte-angele-de-monnoir',
                'lachine',
                'lasalle',
                'mont-royal',
                'cote-saint-luc',
                'pierrefonds',
                'roxboro',
                'saint-leonard',
                'verdun',
                'westmount',
                'l-epiphanie',
                'saint-laurent',
                'pointe-aux-trembles',
                'outremont',
                'acton-vale',
                'baie-comeau',
                'berthierville',
                'boucherville',
                'bromont',
                'cornwall',
                'durham-sud',
                'granby',
                'lanoraie',
                'lavaltrie',
                'les-coteaux',
                'yamaska'
            )
        );
    }
	
	public function get($id){
        return parent::_fetchRow('
			SELECT a.*, a.comment as ad_comment, op.status as package_status, op.expiration_date, u.username, u.date_banned, u.comment, p.name as package_name, u.email, ad_ip, attached_video, a.price_min, a.price_max
                        FROM ads a 
            INNER JOIN users u ON u.id = a.user_id
                        LEFT JOIN order_packages op ON op.ads_id = a.id
            LEFT JOIN packages p ON p.id = op.package_id
            WHERE a.id = ?
        ', array($id));
    }
    public function getShoppingCart($ad_id){
		return parent::_fetchRow('
			SELECT id, `status`, ad_id, transaction_id, payment_processor, package_id, amount from shopping_cart WHERE ad_id= ?  AND `status` = 1 AND package_id IS NOT NULL AND transaction_id is not NULL
		    ', array($ad_id));
	}
        
    public function getAdsStatus($id){
        return parent::getAdapter()->fetchOne('SELECT status FROM ads WHERE id = ?', array($id));
    }

	public function getForPicker($id){
		return parent::_fetchRow('
			SELECT a.id, a.user_id, a.title, category, u.username FROM ads a 
			INNER JOIN users u ON u.id = a.user_id 
			WHERE u.date_activated <> 0 AND u.date_banned = 0 AND a.id = ?
		', array($id));
	}
	
	public function hasPendingPackage($ads_id){
		$sql = "SELECT TRUE FROM order_packages WHERE ads_id = ? AND status = ?";
		
		return parent::getAdapter()->fetchOne($sql, array($ads_id, Model_Packages::STATUS_PENDING));
	}
	
	public function getActivePackages($adv_id){
		$sql = '
			SELECT
				op.id, op.order_id, op.ads_id, op.package_id,
				op.period, UNIX_TIMESTAMP(op.date_activated) AS date_activated, UNIX_TIMESTAMP(op.expiration_date) AS expiration_date,
				op.price,
				p.name AS package_name,
				bu.username AS sales_person
			FROM ads a
			INNER JOIN order_packages op ON op.ads_id = a.id
			LEFT JOIN orders o ON o.id = op.order_id
			INNER JOIN packages p ON p.id = op.package_id
			LEFT JOIN backend_users bu ON bu.id = o.backend_user_id
			WHERE a.id = ? AND op.status = ?
		';

		return parent::getAdapter()->fetchAll($sql, array($adv_id, Model_Packages::STATUS_ACTIVE));
	}
	
    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count){
        
        $imagesCountSql = ($filter['one_pic'] || $filter['no_pic']) ?  " ,(SELECT COUNT(*) FROM ad_images WHERE ad_id = a.id) as pics ":"";
        $tagsCountSQl =  (strlen($filter['tags']))? ", (SELECT COUNT(*) FROM ads_tags WHERE ad_id = a.id) as tags " : "";
        
        $having = '';
        
        $where = '';
        if(isset($filter['unblocked']) && $filter['unblocked'] == 1){
            $where .= " and ad_country_id NOT IN ( " . geo::getLockedSQL() . ")" ;    
        }
        
        if ( strlen($filter['auto_approved']) ) {
            $where .= self::quote('AND a.auto_approved = ? ', $filter['auto_approved']);
            $sqlUserList = ", u.comment as user_comment, u.auto_approval ";
            $sqlUserJoin = " INNER JOIN users u ON u.id = a.user_id" ;
        }else{
            $sqlUserList = "";
            $sqlUserJoin = "";
        }
        if ( strlen($filter['naa'])) {
            $where .= 'AND a.auto_approved = 0 ';
            $sqlUserList = ", u.comment as user_comment, u.auto_approval ";
            $sqlUserJoin = " INNER JOIN users u ON u.id = a.user_id ";
        }
         $sqlUserJoin = " INNER JOIN users u ON u.id = a.user_id ";
        
        if ( strlen($filter['has_map'])) {
            $where .= 'AND al.hide_map = 0 AND al.lat > 0 ';
            //$sqlUserList = ", u.comment as user_comment, u.auto_approval ";
            $sqlUserJoin .= " LEFT JOIN ads_location al ON al.ad_id = a.id ";
        }
        
        if ( strlen($filter['has_price'])) {
            $where .= ' AND ( a.price_min > 0 OR a.price_max > 0 )';
        }
        
        
        if ( strlen($filter['disable_comments'])) {
            $where .= 'AND u.disable_comments = 1 ';
            //$sqlUserList = ", u.comment as user_comment, u.auto_approval ";
            //$sqlUserJoin .= " LEFT JOIN ads_location al ON al.ad_id = a.id ";
        }    
        
        
        
        $sql = '
            SELECT SQL_CALC_FOUND_ROWS a.id, a.user_id, a.title, a.description, a.phone, a.phone_country_iso, a.phone_country_code, a.price, a.price_conditions,
            a.status, a.category, a.city, a.state, a.date_added, a.date_released, a.date_revision, a.comment, a.is_updated, attached_video,
            op.expiration_date, if(op.status = 2 OR (op.status = 1 AND a.status="new"), p.name,"-") as package_name, a.fake_status, a.ad_country, if(op.status = 1 AND a.status="new", p.name,"") as pending  
            ' . $sqlUserList . '
            ' . $imagesCountSql . '
             ' . $tagsCountSQl . '
            FROM ads a
            LEFT JOIN order_packages op ON a.id = op.ads_id AND op.status in (1,2) 
            LEFT JOIN packages p ON op.package_id = p.id 
            ' . $sqlUserJoin . '
           
            WHERE 1
        ';
        
      
        
        if ( strlen($filter['id']) ) {
            $where .= self::quote('AND a.id = ?', $filter['id']);
        }
        
        if ( strlen($filter['user_id']) ) {
            $where .= self::quote('AND a.user_id = ?', $filter['user_id']);
        }
        
        if ( strlen($filter['title']) ) {
            $where .= self::quote('AND a.title LIKE ?', '%'. $filter['title'] . '%');
        }
        
        if ( strlen($filter['description']) ) {
            $where .= self::quote('AND a.description LIKE ?', '%'. $filter['description'] . '%');
        }
        
        if ( strlen($filter['status']) ) {
            $where .= self::quote('AND a.status = ?', $filter['status']);
        }
        
        if ( strlen($filter['category']) ) {
            $where .= self::quote('AND a.category = ?', $filter['category']);
        }
        
        if ( strlen($filter['ad_country']) ) {
            $where .= self::quote('AND a.ad_country_id = ? ', geo::getCountryID($filter['ad_country']) );
        }
        
        if ( strlen($filter['city']) ) {
            $where .= self::quote('AND a.city = ? ', $filter['city']);
        }
        
        if ( strlen($filter['date_banned']) ) {
            $where .= self::quote('AND a.date_banned = ?', $filter['date_banned']);
        }
        
        if ( strlen($filter['phone']) ) {
            $where .= self::quote('AND a.phone LIKE ? COLLATE utf8_bin ', '%'. $filter['phone'] . '%');
        }
        if ( strlen($filter['verified']) ) {
            $where .= "AND a.verification_status = 'verified' ";
        }
        
        if ( strlen($filter['modified']) ) {
            $where .= "AND a.`is_updated` = '1' ";
        }
        
        if ( strlen($filter['active_package_id']) ) {
            if($filter['active_package_id'] == -1){
                $where .= ' AND op.status IN (1,2)' ;
            }
            else{
                $where .= self::quote('AND op.package_id =  ? ', $filter['active_package_id']);
            }
        }
        
        // <editor-fold defaultstate="collapsed" desc="Filtration by Date">
        if ( isset($filter['filter_by']) && $filter['filter_by'] ) {
            
            if ( $filter['filter_by'] == 'added_date' ) {
                $date_field = 'date_added';
            }
            elseif ( $filter['filter_by'] == 'released_date' ) {
                $date_field = 'date_released';
            }
            
            if ( $filter['date_from'] && $filter['date_to'] ) {
                if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
                    $where .= self::quote(' AND DATE(FROM_UNIXTIME(' . $date_field . ')) = DATE(FROM_UNIXTIME(?))', $filter['date_from']);
                }
            }

            if ( $filter['date_from'] ) {
                $where .= self::quote(' AND DATE(FROM_UNIXTIME(' . $date_field . ')) >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
            }

            if ( $filter['date_to'] ) {
                $where .= self::quote(' AND DATE(FROM_UNIXTIME(' . $date_field . ')) <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
            }
        }

       
        if ( strlen($filter['fake_status']) ) {
            $where .= self::quote('AND a.fake_status = ?', $filter['fake_status']);
        }

        if ( strlen($filter['is_updated']) ) {
            $where .= self::quote('AND a.is_updated = ? AND a.status IN ("active", "paused") ', $filter['is_updated']);
        }
        
        if ( strlen($filter['tags']) && $filter['tags'] == 'yes' ) {
            $having .= " HAVING  tags > 0 ";
        }
        
        if ( strlen($filter['tags']) && $filter['tags'] == 'no' ) {
            $having .= " HAVING  tags = 0 ";
        }
        
        if ( $filter['one_pic'] ) {
            $having .= " HAVING  pics = 1 ";
        }
        
        if ( $filter['no_pic'] ) {
            $having .= " HAVING  pics = 0 ";
        }

        unset($filter['filter_by']);
        unset($filter['date_from']);
            unset($filter['date_to']);
            unset($filter['one_pic']);
        unset($filter['no_pic']);
        // </editor-fold>
        
        $sql .= $where;
           
        $sqlCount = "SELECT FOUND_ROWS();";
        
        $sql .= $having ;
        $sql .= '
            ORDER BY ' . $sort_field . ' ' . $sort_dir . '
            LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
        ';
       // echo($sql);   
//        die();
        $results = parent::_fetchAll($sql);
        $count = $this->getAdapter()->fetchOne($sqlCount);
        
        return $results;
    }
    
    
    public function getAllNew($page, $per_page, $filter, $sort_field, $sort_dir, &$count){
        
        $imagesCountSql = ($filter['one_pic'] || $filter['no_pic']) ?  " ,(SELECT COUNT(*) FROM ad_images WHERE ad_id = a.id) as pics ":"";
        $having = '';
        
        $where = '';
        $where .= self::quote('AND a.status = ?', 'new');
        $where .= self::quote('AND u.date_banned <> ?', 30);
        if(isset($filter['unblocked']) && $filter['unblocked'] == 1){
            $where .= " and ad_country_id NOT IN ( " . geo::getLockedSQL() . ")" ;    
        }
        
        if ( strlen($filter['auto_approved']) ) {
            $where .= self::quote('AND a.auto_approved = ? ', $filter['auto_approved']);
            $sqlUserList = ", u.comment as user_comment, u.auto_approval ";
            $sqlUserJoin = " INNER JOIN users u ON u.id = a.user_id" ;
        }else{
            $sqlUserList = "";
            $sqlUserJoin = "";
        }
        if ( strlen($filter['naa'])) {
            $where .= 'AND a.auto_approved = 0 ';
            $sqlUserList = ", u.comment as user_comment, u.auto_approval ";
            $sqlUserJoin = " INNER JOIN users u ON u.id = a.user_id ";
        }
          $sqlUserJoin = " INNER JOIN users u ON u.id = a.user_id ";
          
        if ( strlen($filter['has_map'])) {
            $where .= 'AND al.hide_map = 0 AND al.lat > 0 ';
            //$sqlUserList = ", u.comment as user_comment, u.auto_approval ";
            $sqlUserJoin .= " LEFT JOIN ads_location al ON al.ad_id = a.id ";
        }
        
        if ( strlen($filter['disable_comments'])) {
            $where .= 'AND u.disable_comments = 1 ';
            //$sqlUserList = ", u.comment as user_comment, u.auto_approval ";
            //$sqlUserJoin .= " LEFT JOIN ads_location al ON al.ad_id = a.id ";
        }    
        
          
        $sql = '
            SELECT a.id, a.user_id, a.title, a.description, a.phone,a.vpn, a.phone_country_iso, a.phone_country_code, a.price, a.price_conditions,
            a.status, a.category, a.city, a.state, a.date_added, a.date_released, a.date_revision, a.comment, a.is_updated,
            op.expiration_date, p.name as package_name, a.fake_status, a.ad_country, (rta.contor - 1) as phonedup, u.email, tads.total_ads, attached_video
            ' . $sqlUserList . '
            ' . $imagesCountSql . '
            FROM ads a
            LEFT JOIN order_packages op ON a.id = op.ads_id AND op.status = 1
            LEFT JOIN packages p ON op.package_id = p.id 
            LEFT JOIN (select count( DISTINCT user_id) as contor, phone from ads ta  group by ta.phone ) rta on rta.phone = a.phone
            LEFT JOIN (SELECT COUNT(*) AS total_ads, user_id FROM ads WHERE `status` =\'active\'  and date_released > 0  GROUP BY user_id) tads on tads.user_id = a.user_id
            ' . $sqlUserJoin . '
           
            WHERE 1
        ';
        
        $countSql =   '
            SELECT COUNT(*) FROM ads a
            LEFT JOIN order_packages op ON a.id = op.ads_id AND op.status = 1
            LEFT JOIN packages p ON op.package_id = p.id 
            LEFT JOIN (select count( DISTINCT user_id) as contor, phone from ads ta  group by ta.phone ) rta on rta.phone = a.phone
            LEFT JOIN (SELECT COUNT(*) AS total_ads, user_id FROM ads WHERE `status` =\'active\'  and date_released > 0  GROUP BY user_id) tads on tads.user_id = a.user_id
            ' . $sqlUserJoin . '
           
            WHERE 1
        ';
        
        
        
        if ( strlen($filter['id']) ) {
            $where .= self::quote('AND a.id = ?', $filter['id']);
        }
        
        if ( strlen($filter['user_id']) ) {
            $where .= self::quote('AND a.user_id = ?', $filter['user_id']);
        }
        
        if ( strlen($filter['title']) ) {
            $where .= self::quote('AND a.title LIKE ?', '%'. $filter['title'] . '%');
        }
        
        if ( strlen($filter['description']) ) {
            $where .= self::quote('AND a.description LIKE ?', '%'. $filter['description'] . '%');
        }
        
        if ( strlen($filter['status']) ) {
            $where .= self::quote('AND a.status = ?', $filter['status']);
        }
        
        if ( strlen($filter['category']) ) {
            $where .= self::quote('AND a.category = ?', $filter['category']);
        }
        
        if ( strlen($filter['ad_country']) ) {
            $where .= self::quote('AND a.ad_country_id = ? ', geo::getCountryID($filter['ad_country']) );
        }
        
        if ( strlen($filter['city']) ) {
            $where .= self::quote('AND a.city = ? ', $filter['city']);
        }
        
        if ( strlen($filter['date_banned']) ) {
            $where .= self::quote('AND a.date_banned = ?', $filter['date_banned']);
        }
        
        if ( strlen($filter['phone']) ) {
            $where .= self::quote('AND a.phone LIKE ? COLLATE utf8_bin ', '%'. $filter['phone'] . '%');
        }
        if ( strlen($filter['verified']) ) {
            $where .= "AND a.verification_status = 'verified' ";
        }
        
        if ( strlen($filter['modified']) ) {
            $where .= "AND a.`is_updated` = '1' ";
        }
        
        if ( strlen($filter['active_package_id']) ) {
            if($filter['active_package_id'] == -1){
                $where .= ' AND op.status IN (1,2)' ;
            }
            else{
                $where .= self::quote('AND op.package_id =  ? ', $filter['active_package_id']);
            }
        }
        
        // <editor-fold defaultstate="collapsed" desc="Filtration by Date">
        if ( isset($filter['filter_by']) && $filter['filter_by'] ) {
            
            if ( $filter['filter_by'] == 'added_date' ) {
                $date_field = 'date_added';
            }
            elseif ( $filter['filter_by'] == 'released_date' ) {
                $date_field = 'date_released';
            }
            
            if ( $filter['date_from'] && $filter['date_to'] ) {
                if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
                    $where .= self::quote(' AND DATE(FROM_UNIXTIME(' . $date_field . ')) = DATE(FROM_UNIXTIME(?))', $filter['date_from']);
                }
            }

            if ( $filter['date_from'] ) {
                $where .= self::quote(' AND DATE(FROM_UNIXTIME(' . $date_field . ')) >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
            }

            if ( $filter['date_to'] ) {
                $where .= self::quote(' AND DATE(FROM_UNIXTIME(' . $date_field . ')) <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
            }
        }

       
        if ( strlen($filter['fake_status']) ) {
            $where .= self::quote('AND a.fake_status = ?', $filter['fake_status']);
        }

        if ( strlen($filter['is_updated']) ) {
            $where .= self::quote('AND a.is_updated = ? AND a.status IN ("active", "paused") ', $filter['is_updated']);
        }
        
        if ( $filter['one_pic'] ) {
            $having .= " HAVING  pics = 1 ";
        }
        
        if ( $filter['no_pic'] ) {
            $having .= " HAVING  pics = 0 ";
        }

        unset($filter['filter_by']);
        unset($filter['date_from']);
            unset($filter['date_to']);
            unset($filter['one_pic']);
        unset($filter['no_pic']);
        // </editor-fold>
        
        $sql .= $where;
        $countSql .= $where;
           
        //$sqlCount = "SELECT FOUND_ROWS();";
        
        $sql .= $having ;
        $countSql .= $having ;
                $sql .= '
            ORDER BY ' . $sort_field . ' ' . $sort_dir . '
            LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
        ';
        
        //MyLogs::debug($sql, "============ APPROVALS SQL ============");
        
        //echo $sql;
//        die();     
        $results = parent::_fetchAll($sql);
        $count = $this->getAdapter()->fetchOne($countSql);
        
        //MyLogs::debug($results, "============ APPROVALS RESULTS ============");
        //MyLogs::debug($count, "============ APPROVALS COUNT ============");
        
        return $results;
    }
    
   	public function getAllRevisionList($page, $per_page, $filter, $sort_field, $sort_dir, &$count){
        
        $imagesCountSql = ($filter['one_pic'] || $filter['no_pic']) ?  " ,(SELECT COUNT(*) FROM ad_images WHERE ad_id = a.id) as pics ":"";
        $having = '';
        $where = '';
        
        if ( strlen($filter['auto_approved']) ) {
            $where .= self::quote('AND a.auto_approved = ?', $filter['auto_approved']);
            $sqlUserList = ", u.comment as user_comment, u.auto_approval";
        }else{
            $sqlUserList = "";
        }
        
        $sqlUserJoin = " INNER JOIN users u ON u.id = a.user_id";
        
        if ( strlen($filter['has_map'])) {
            $where .= 'AND al.hide_map = 0 AND al.lat > 0 ';
            //$sqlUserList = ", u.comment as user_comment, u.auto_approval ";
            $sqlUserJoin .= " LEFT JOIN ads_location al ON al.ad_id = a.id ";
        }
        
        if ( strlen($filter['disable_comments'])) {
            $where .= 'AND u.disable_comments = 1 ';
            //$sqlUserList = ", u.comment as user_comment, u.auto_approval ";
            //$sqlUserJoin .= " LEFT JOIN ads_location al ON al.ad_id = a.id ";
        }    
        
        
        
        
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS a.id, a.user_id, if(AR.title IS not  NULL,AR.title,a.title) as title, a.title as old_title , AR.description, AR.category, a.category AS old_categpry, a.description as old_description, AR.phone, AR.phone_country_iso, a.phone_country_code, a.price, a.price_conditions,
			a.status, a.category, a.city, a.state, a.date_added, a.date_released, a.date_revision, a.comment, a.is_updated, AR.vpn,
			op.expiration_date, p.name as package_name, a.fake_status
            ' . $imagesCountSql . ', ad_location, ad_country, ad_ip , rev_location, rev_country,attached_video
            ' . $sqlUserList . '
            ' . $imagesCountSql . ' 
			FROM ads a
			LEFT JOIN order_packages op ON a.id = op.ads_id AND op.status = 2
			LEFT JOIN packages p ON op.package_id = p.id 
            ' . $sqlUserJoin . '
			LEFT JOIN ads_revision AR ON AR.ads_id = a.id
			WHERE 1 and a.is_updated = \'1\' AND a.status IN ("active", "paused")
		';
		
		
		
		if ( strlen($filter['id']) ) {
			$where .= self::quote('AND a.id = ?', $filter['id']);
		}
		
		if ( strlen($filter['user_id']) ) {
			$where .= self::quote('AND a.user_id = ?', $filter['user_id']);
		}
		
		if ( strlen($filter['title']) ) {
			$where .= self::quote('AND a.title LIKE ?', '%'. $filter['title'] . '%');
		}
		
		if ( strlen($filter['description']) ) {
            $where .= self::quote('AND a.description LIKE ?', '%'. $filter['description'] . '%');
        }
        if ( strlen($filter['ad_country']) ) {
			$where .= self::quote('AND rev_country LIKE ?', '%'. $filter['ad_country'] . '%');
		}
		
		if ( strlen($filter['status']) ) {
			$where .= self::quote('AND a.status = ?', $filter['status']);
		}
		
		if ( strlen($filter['category']) ) {
			$where .= self::quote('AND a.category = ?', $filter['category']);
		}
		
		if ( strlen($filter['city']) ) {
			$where .= self::quote('AND a.city = ?', $filter['city']);
		}
		
		if ( strlen($filter['date_banned']) ) {
			$where .= self::quote('AND a.date_banned = ?', $filter['date_banned']);
		}
		
		if ( strlen($filter['phone']) ) {
            $where .= self::quote('AND a.phone LIKE ? ', '%'. $filter['phone'] . '%');
        }
        if ( strlen($filter['verified']) ) {
            $where .= "AND a.verification_status = 'verified' ";
        }
        
        if ( strlen($filter['modified']) ) {
			$where .= "AND a.`is_updated` = '1' ";
		}
		
		if ( strlen($filter['active_package_id']) ) {
			if($filter['active_package_id'] == -1){
				$where .= ' AND op.status IN (1,2)' ;
			}
			else{
				$where .= self::quote('AND op.package_id =  ? ', $filter['active_package_id']);
			}
		}
		
		// <editor-fold defaultstate="collapsed" desc="Filtration by Date">
		if ( isset($filter['filter_by']) && $filter['filter_by'] ) {
			
			if ( $filter['filter_by'] == 'added_date' ) {
				$date_field = 'a.date_added';
			}
			elseif ( $filter['filter_by'] == 'released_date' ) {
				$date_field = 'a.date_released';
			}
			
			if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote(' AND DATE(FROM_UNIXTIME(' . $date_field . ')) = DATE(FROM_UNIXTIME(?))', $filter['date_from']);
				}
			}

			if ( $filter['date_from'] ) {
				$where .= self::quote(' AND DATE(FROM_UNIXTIME(' . $date_field . ')) >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
			}

			if ( $filter['date_to'] ) {
				$where .= self::quote(' AND DATE(FROM_UNIXTIME(' . $date_field . ')) <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
			}
		}

        if ( strlen($filter['auto_approved']) ) {
            $where .= self::quote('AND a.auto_approved = ?', $filter['auto_approved']);
        }

        if ( strlen($filter['fake_status']) ) {
            $where .= self::quote('AND a.fake_status = ?', $filter['fake_status']);
        }

        if ( strlen($filter['is_updated']) ) {
            $where .= self::quote('AND a.is_updated = ? AND a.status IN ("active", "paused") ', $filter['is_updated']);
        }
        
        if ( $filter['one_pic'] ) {
            $having .= " HAVING  pics = 1 ";
        }
        
        if ( $filter['no_pic'] ) {
            $having .= " HAVING  pics = 0 ";
        }

		unset($filter['filter_by']);
		unset($filter['date_from']);
        	unset($filter['date_to']);
        	unset($filter['one_pic']);
		unset($filter['no_pic']);
		// </editor-fold>
		
		$sql .= $where;
		
		$sqlCount = "SELECT FOUND_ROWS();";
		
		$sql .= $having ;
                $sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
        //echo $sql ;
//        die();
		$results = parent::_fetchAll($sql);
		$count = $this->getAdapter()->fetchOne($sqlCount);
		
		return $results;
	}
        
    public function getAllRevisions($page, $per_page, $filter, $sort_field, $sort_dir, &$count){
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS a.id, a.ads_id, a.title, a.description, a.phone, a.price, a.price_conditions,
			a.status, a.category, a.city, a.state, a.date_added, a.date_released, a.comment,
			op.expiration_date, p.name as package_name, u.comment as user_comment,attached_video
			FROM ads_revision a
			LEFT JOIN order_packages op ON a.id = op.ads_id AND op.status = 2
			LEFT JOIN packages p ON op.package_id = p.id 
                        INNER JOIN users u ON u.id = a.user_id
			WHERE 1
		';
		
		$where = '';		
		
		if ( strlen($filter['id']) ) {
			$where .= self::quote('AND a.id = ?', $filter['id']);
		}
		
		if ( strlen($filter['user_id']) ) {
			$where .= self::quote('AND a.user_id = ?', $filter['user_id']);
		}
		
		if ( strlen($filter['title']) ) {
			$where .= self::quote('AND a.title LIKE ?', '%'. $filter['title'] . '%');
		}
		
		if ( strlen($filter['description']) ) {
			$where .= self::quote('AND a.description LIKE ?', '%'. $filter['description'] . '%');
		}
		
		if ( strlen($filter['status']) ) {
			$where .= self::quote('AND a.status = ?', $filter['status']);
		}
		
		if ( strlen($filter['category']) ) {
			$where .= self::quote('AND a.category = ?', $filter['category']);
		}
		
		if ( strlen($filter['city']) ) {
			$where .= self::quote('AND a.city = ?', $filter['city']);
		}
		
		if ( strlen($filter['date_banned']) ) {
			$where .= self::quote('AND a.date_banned = ?', $filter['date_banned']);
		}
		
		if ( strlen($filter['phone']) ) {
			$where .= self::quote('AND a.phone LIKE ? ', '%'. $filter['phone'] . '%');
		}
		
		if ( strlen($filter['active_package_id']) ) {
			if($filter['active_package_id'] == -1){
				$where .= ' AND op.status IN (1,2)' ;
			}
			else{
				$where .= self::quote('AND op.package_id =  ? ', $filter['active_package_id']);
			}
		}
		
		// <editor-fold defaultstate="collapsed" desc="Filtration by Date">
		if ( isset($filter['filter_by']) && $filter['filter_by'] ) {
			
			if ( $filter['filter_by'] == 'added_date' ) {
				$date_field = 'date_added';
			}
			elseif ( $filter['filter_by'] == 'released_date' ) {
				$date_field = 'date_released';
			}
			
			if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote(' AND DATE(FROM_UNIXTIME(' . $date_field . ')) = DATE(FROM_UNIXTIME(?))', $filter['date_from']);
				}
			}

			if ( $filter['date_from'] ) {
				$where .= self::quote(' AND DATE(FROM_UNIXTIME(' . $date_field . ')) >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
			}

			if ( $filter['date_to'] ) {
				$where .= self::quote(' AND DATE(FROM_UNIXTIME(' . $date_field . ')) <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
			}
		}

		unset($filter['filter_by']);
		unset($filter['date_from']);
		unset($filter['date_to']);
		// </editor-fold>
		
		$sql .= $where;
		
		$sqlCount = "SELECT FOUND_ROWS();";
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$results = parent::_fetchAll($sql);
		$count = $this->getAdapter()->fetchOne($sqlCount);
		return $results;
	}
    
    public function disableUserAds( $user_id ){
            parent::getAdapter()->update('ads', array(
                'status' => self::STATUS_INACTIVE,
                'is_updated' => '0'
            ), parent::getAdapter()->quoteInto('user_id = ?', $user_id));

            return true;
        }

    public function hasUserPaidPackage( $user_id ){
            $sql = '
                SELECT COUNT(a.id) as count
                FROM ads a
                LEFT JOIN order_packages op ON a.id = op.ads_id AND op.status = 2
                WHERE op.status IN (1,2) AND user_id = ' . $user_id . '
            ';
            
            $results = parent::_fetchAll($sql);
            return $results[0]->count;
        }
	
	public function activate($user_id)
	{
		$this->_db->update('users', array(
			'activation_hash' => null,
            		'status' => STATUS_ACTIVE,
			'is_updated' => 0
		), array(
			$this->_db->quoteInto('id = ?', $user_id)
		));

		return true;
	}
	
	public function setStatus($user_id,$status)
	{
		$this->_db->update('users', array(
			'status' => $status
		), array(
			$this->_db->quoteInto('id = ?', $user_id)
		));

		return true;
	}

	public function insert($data)
	{
		parent::getAdapter()->insert($this->_table, $data);

		return parent::getAdapter()->lastInsertId();
	}

	public function update($id, $data)
	{
		try{    
            parent::getAdapter()->update($this->_table, $data, parent::getAdapter()->quoteInto('id = ?', $id)); 
        }catch(Exception $e){
            //die("Duplicate Description");
           var_dump($e);
//            die();
        }
	}
	
	public function remove($id)
	{
		parent::getAdapter()->update($this->_table, array('status' => self::STATUS_DELETED), parent::getAdapter()->quoteInto('id = ?', $id));
		//parent::remove(self::quote('id = ?', $id));
	}

    public function deactivate($id)
	{
		parent::getAdapter()->update($this->_table, array('status' => self::STATUS_INACTIVE, 'is_updated' => 0 ), parent::getAdapter()->quoteInto('id = ?', $id));
	}

    public function activateIt($id, $oldStatus = false, $adminID)
    {
        
        $data['status'] = self::STATUS_ACTIVE;
        $data['is_updated'] = 0;
        
        if($oldStatus && $oldStatus != self::STATUS_ACTIVE && $oldStatus == Model_Advertisements::STATUS_NEW){
            $data['activated_by'] = $adminID;
            $data['date_released'] = time();
        }
        
        parent::getAdapter()->update($this->_table, $data, parent::getAdapter()->quoteInto('id = ?', $id));
    }
    
    public function unlinkvideo($id)
	{
        
		parent::getAdapter()->update($this->_table, ['attached_video' => Null], parent::getAdapter()->quoteInto('id = ?', $id));
	}
        
    public function addMultiComment($data, $aid){
                parent::getAdapter()->update($this->_table, array('comment' => $data['ad_comment']), parent::getAdapter()->quoteInto('id = ?', $aid));
        }

	public function descriptionExists($description, $id = null)
	{
		$where = '';
		if($id){
			$where = 'AND id <> '. $id;
		}
		
		return parent::getAdapter()->fetchOne('
			SELECT id FROM ads WHERE description = ? '. $where
		, $description);
	}
	
	public function savePostedImages($ad_fields, $posted_images)
    {
        $return_values = NULL;
        $thumb_md5 = NULL;
        $image_index = 0;
		//var_dump($posted_images);
        foreach($posted_images as $image_details)
        {
            $image_destination_path         = $this->getImagePath($ad_fields, $image_index, FALSE);
            $image_thumb_destination_path   = $this->getImagePath($ad_fields, $image_index, TRUE);
			
			if($image_details['tmp_name'] == $image_destination_path){
				$return_values[$image_index]    = Cubix_Images::getImageOrientation($image_details['tmp_name']);
				$image_index++;
				continue;
			}
			
			$x = (Cubix_Images::resampleJPEGImage($image_details['tmp_name'], $image_destination_path, Cubix_Images::AD_IMAGE_WIDTH, TRUE) === TRUE ? 1 : 0);
			$y = (Cubix_Images::resampleJPEGImage($image_details['tmp_name'], $image_thumb_destination_path, Cubix_Images::AD_THUMB_WIDTH, TRUE) === TRUE ? 2: 0);
						
            switch($x + $y)
            {
                case 3:
                    $return_values[$image_index]    = Cubix_Images::getImageOrientation($image_details['tmp_name']);
										
					unlink($image_details['tmp_name']);
					if(isset($image_details['tmp_thumb_name'])){
						unlink($image_details['tmp_thumb_name']);
					}
										
                    if($thumb_md5 === NULL)
                    {
                        $return_values[$image_index] .= ':T';
                        $thumb_md5 = md5(file_get_contents($image_thumb_destination_path));
                    }
                    
                    break;
                    
                default:
                    foreach(glob(dirname($image_destination_path) . '/*') as $file)
                            unlink($file);
    
                    rmdir(dirname($image_destination_path));
                    return 'ERROR_SOME_IMAGES_COULD_NOT_BE_SAVED';
                    break;
            } 
			$image_index++;
        }
        
        return array('images' => serialize($return_values), 'thumb_md5' => $thumb_md5);
    } 
	
	public function getImagePath($ad_record, $image_number, $is_thumb = FALSE, $is_web = FALSE)
    {
		
		$image_configs = Zend_Registry::get('images_config');
			
        if($is_web === TRUE){
			$path = 'https://' . $image_configs['gui_ads_' . $ad_record['category']] . '/';
		}
		else{
			$path = $image_configs['path'] . $ad_record['category'] . '/';
		}
		
        return  $path . self::stringToUrl(mb_convert_encoding($ad_record['city'], "Windows-1252", "UTF-8")) . '/id-' . $ad_record['id']. '/' . ($is_thumb === TRUE ? 'thumb' : 'img') . '-' . $image_number . '-' . self::stringToUrl(mb_convert_encoding($ad_record['title'], "Windows-1252", "UTF-8")) . '.jpg';
    }
	
	public static function stringToUrl($string, $replacement_character = '-')
    {
        $string = mb_convert_encoding($string, "Windows-1252", "UTF-8");
        return strtolower(preg_replace("/$replacement_character+/", $replacement_character, preg_replace('/[^a-zA-Z0-9]/', $replacement_character, self::translateDiacriaticsAndLigatures($string))));
    }

    public static function getSetCardUrl($adDetails) {
        $url = "/ad/details/category/";
        $url .= self::stringToUrl($adDetails->category);
        $url .= '/city/'.self::stringToUrl($adDetails->city);
        $url .= '/id/'.$adDetails->id;
        $url .= '/title/'.self::stringToUrl($adDetails->title);
        return $url;
    }
	
	public static function translateDiacriaticsAndLigatures($string)
    {
        $french_diacriatics_and_ligatures = array(  '�' => 'A', '�' => 'a', '�' => 'A', '�' => 'a', '�' => 'a', '�' => 'a',
                                                    '�' => 'C', '�' => 'c',
                                                    '�' => 'E', '�' => 'e', '�' => 'E', '�' => 'e', '�' => 'E', '�' => 'e', '�' => 'E', '�' => 'e',
                                                    '�' => 'I', '�' => 'i', '�' => 'I', '�' => 'i',
                                                    '�' => 'O', '�' => 'o', '�' => 'O', '�' => 'o',
                                                    '�' => 'U', '�' => 'u', '�' => 'U', '�' => 'u', '�' => 'U', '�' => 'u',
                                                    '�' => 'y');


        return str_replace(array_keys($french_diacriatics_and_ligatures), array_values($french_diacriatics_and_ligatures), $string);
    }

    /**
     * @param $ad_id
     * @return string
     */
    public function getPhoneByAdId($ad_id) {
        return self::getAdapter()->fetchOne("SELECT phone from ads WHERE  id = ?", $ad_id);
    }

    public function getAllAdsByUserId( $user_id ){

        return parent::_fetchAll(
            'SELECT * FROM ads WHERE user_id = ?', array($user_id)
        );
    }

    public function getPublishedAdsByUserId( $user_id ){

        return parent::_fetchAll(
            'SELECT * FROM ads WHERE status = ? AND user_id = ?', array(self::STATUS_ACTIVE, $user_id)
        );
    }

    public function getInactiveAdsByUserId( $user_id ){

        return parent::_fetchAll(
            'SELECT * FROM ads WHERE status = ? AND user_id = ?', array(self::STATUS_INACTIVE, $user_id)
        );
    }


    public function getNewAdsByUserId( $user_id ){

        return parent::_fetchAll(
            'SELECT * FROM ads WHERE status = ? AND user_id = ?', array(self::STATUS_NEW, $user_id)
        );
    }
    
    public function getCountNewAdsByUserId($user_id) {
        return self::getAdapter()->fetchOne('SELECT count(id) FROM ads WHERE status = ? AND user_id = ?', array(self::STATUS_NEW, $user_id));
    }


    public function getDeletedAdsByUserId( $user_id ){

        return parent::_fetchAll(
            'SELECT * FROM ads WHERE status = ? AND user_id = ?', array(self::STATUS_DELETED, $user_id)
        );
    }
    
    public function getCountDeletedAdsByUserId($user_id) {
        return self::getAdapter()->fetchOne('SELECT count(id) FROM ads WHERE status = ? AND user_id = ?', array(self::STATUS_DELETED, $user_id));
    }

    public function getBannedAdsByUserId( $user_id ){

        return parent::_fetchAll(
            'SELECT * FROM ads WHERE status = ? AND user_id = ?', array(self::STATUS_BANNED, $user_id)
        );
    }
    
    public function getCountBannedAdsByUserId($user_id) {
        return self::getAdapter()->fetchOne('SELECT count(id) FROM ads WHERE status = ? AND user_id = ?', array(self::STATUS_BANNED, $user_id));
    }
    

    public function getExpiredAdsByUserId( $user_id ){

        return parent::_fetchAll(
            'SELECT * FROM ads WHERE status = ? AND user_id = ?', array(self::STATUS_PAUSED, $user_id)
        );
    }
    
     public function getCountExpiredAdsByUserId($user_id) {
        return self::getAdapter()->fetchOne('SELECT count(id) FROM ads WHERE status = ? AND user_id = ?', array(self::STATUS_PAUSED, $user_id));
    }
    

    public function getUsersBySamePhone( $user_id, $phone_number ){

        return parent::_fetchAll(
            'SELECT DISTINCT user_id  FROM ads WHERE user_id <> ? AND phone = ? ', array($user_id, $phone_number)
        );
    }

    public function getPremiumsCountByUserId( $user_id ){
        return self::getAdapter()->fetchOne(
            'SELECT COUNT(*) 
            FROM ads a
            INNER JOIN order_packages op ON op.ads_id = a.id
            WHERE a.user_id = ? AND op.package_id IN (19, 20, 21)', array($user_id)
        );
    }
    
    public function getAdIsPremium( $ad_id ){
        
        return self::getAdapter()->fetchOne(
            'SELECT COUNT(*) 
            FROM ads a
            INNER JOIN order_packages op ON op.ads_id = a.id
            WHERE a.id = ? AND op.package_id >= 1 and expiration_date >= CURDATE() ', array($ad_id)
        );
    }

    public function getFakedAdsByUserId( $user_id ){

        return parent::_fetchAll(
            'SELECT * FROM ads WHERE fake_status = ? AND user_id = ?', array(self::STATUS_FAKE, $user_id)
        );
    }

    public function makeFake($id){
        parent::getAdapter()->update($this->_table, array('fake_status' => self::STATUS_FAKE), parent::getAdapter()->quoteInto('id = ?', $id));
    }

    public function getRevisionData($id){
        return parent::_fetchRow(
            'SELECT ar.id, ar.ads_id, ar.location, ar.price, ar.price_conditions, ar.title, ar.vpn, ar.description, ar.category, ar.date_added, ar.`status`, ar.city, 
                ar.phone, ar.user_id, ar.phone_country_iso, ar.phone_country_code, ar.approved_status, ar.revision_ip, ar.rev_country, 
                ar.rev_country_id, ar.rev_location, ar.myfans_url, ar.validation_id, ar.dialing_number, ar.phone_validated, ar.validate_date, 
                a.title as old_title, a.description as old_description, a.category as old_category, a.phone as old_phone, a.attached_video FROM ads_revision ar LEFT JOIN ads a  ON a.id = ads_id WHERE ads_id = ?', array($id)
        );
    }
    
    public function getRevisionCleanData($id){
        return parent::_fetchRow(
            'SELECT * FROM ads_revision  WHERE ads_id = ?', array($id)
        );
    }

    public function approveRevisionStatus($id){
        parent::getAdapter()->update('ads_revision', array('approved_status' => 1), parent::getAdapter()->quoteInto('ads_id = ?', $id));
    }

    public function getUserEditedAds( $user_id ){
        return parent::_fetchAll(
            'SELECT id  FROM ads WHERE is_updated = "1" AND user_id = ? ', array($user_id)
        );
    }

    public function removeFake($id){
        parent::getAdapter()->update($this->_table, array('fake_status' => self::STATUS_NOT_FAKE), parent::getAdapter()->quoteInto('id = ?', $id));
    }
    
    public static function getCountries(){
        
        $countries = parent::_fetchAll(
            'SELECT ad_country FROM ads  WHERE ad_country != \'\'  group by ad_country order by ad_country=\'CA\' desc '
        );  
        $list = [];  
        foreach($countries as  $country){
            $list[$country->ad_country] = geo::$countryCodes[$country->ad_country];
        }
        
        return $list;
    }
    
    public function getUserPhoneNumbers($user_id){
        
        return parent::_fetchAll(
            'SELECT phone FROM ads WHERE user_id = ?', array($user_id)
        );
    }
    
    public function getTempList(){
        
        return parent::_fetchAll(
            ' SELECT DISTINCT email, username, `language`, last_login, U.id FROM  users U 
            LEFT JOIN (SELECT MAX(login_date) AS last_login ,user_id FROM users_login_history GROUP BY user_id HAVING MAX(login_date)  > DATE_SUB(CURDATE(), INTERVAL 1 YEAR )  )  UH ON UH.user_id = U.id 
            WHERE date_banned = 0 AND `type` = 1 
                AND U.id NOT IN (SELECT user_id FROM shopping_cart WHERE STATUS = 1 AND creation_date > DATE_SUB(NOW(), INTERVAL 42 DAY))
                AND U.id NOT IN (SELECT user_id FROM newsletter_sent_list )
            
            ORDER BY last_login DESC LIMIT 20' 
        );
        
    }

    public function updateAdsByUserId($user_id, $data){
        parent::getAdapter()->update($this->_table, $data, parent::getAdapter()->quoteInto('user_id = ?', $user_id));
    }
    
    public function updateBump($id){
            
    }
    
}
