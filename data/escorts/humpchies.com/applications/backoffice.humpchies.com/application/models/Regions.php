<?php
class Model_Regions extends Cubix_Model
{
    protected $_table = 'cities_regions';

    
    public static function getAll() {
        
        $redisKey = "RegionssList";
        $regionsList = Core_Redis::get($redisKey);
         
        if($regionsList){
            return unserialize($regionsList);
        }
        
        $sql = 'SELECT id, `slug`, `name` FROM cities_regions WHERE 1 = 1';
        
        $regionsList = parent::_fetchAll($sql);
        Core_Redis::set($redisKey, serialize($regionsList));   

        return $regionsList;
       
    }    
    
    
}
