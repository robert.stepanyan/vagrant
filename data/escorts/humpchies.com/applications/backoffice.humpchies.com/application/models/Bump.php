<?php

class Model_Bump extends Cubix_Model
{
	const BUMPS_SETTINGS_ACTIVE = 1;
	const BUMPS_SETTINGS_EXPIRED = 2;
	const BUMPS_SETTINGS_CANCELED = 3;
	
	public static $STATUSES = array(
		self::BUMPS_SETTINGS_ACTIVE => 'Active',
		self::BUMPS_SETTINGS_EXPIRED => 'Expired',
		self::BUMPS_SETTINGS_CANCELED => 'Canceled'
	);
	
	public static $CREDIT_OPTIONS = array(
		'cr_20' => 20,
		'cr_50' => 48,
		'cr_100' => 90,
		'cr_200' => 180,
		'cr_500' => 400,
        'cr_25' => 20,
        'cr_58' => 48,
        'cr_110' => 90,
        'cr_225' => 180,
        'cr_600' => 400
	);
   /*
    public static $CREDIT_OPTIONS = array(
        'cr_25' => 20,
        'cr_58' => 48,
        'cr_110' => 90,
        'cr_225' => 180,
        'cr_600' => 400
    );
	*/
	public function getActiveBumps()
    {
		$sql = '
			SELECT bs.id, bs.user_id, bs.ads_id, bs.bumps_count, bs.interval_sec, TIME_TO_SEC(TIMEDIFF(NOW(), bs.update_date)) as seconds_passed FROM bump_settings bs
			INNER JOIN users u ON u.id = bs.user_id
			INNER JOIN ads a ON a.id = bs.ads_id
			WHERE TIME_TO_SEC(TIMEDIFF(NOW(), bs.update_date)) > bs.interval_sec 
			AND (TIME_TO_SEC(TIMEDIFF(NOW(),bs.creation_date)) - 60) < bs.periode_sec 
			AND bs.bumps_count <= bs.bumps_total 
			AND bs.status = ? 
			AND u.credits > 0
			AND u.date_banned = 0
			AND a.status = "active"
		';

		return parent::getAdapter()->fetchAll($sql, array(self::BUMPS_SETTINGS_ACTIVE));
    }
	
	public function updateExpiredBumps()
    {
		$sql = '
			UPDATE bump_settings SET status = ? 
			WHERE (TIME_TO_SEC(TIMEDIFF(NOW(),creation_date)) - 60) > periode_sec AND status = ? 
		';
		
		return parent::getAdapter()->query($sql, array(self::BUMPS_SETTINGS_EXPIRED, self::BUMPS_SETTINGS_ACTIVE));
    }
	
	public function cleanup()
	{
		try {
			parent::db()->beginTransaction();
			
			$sql = 'SELECT * FROM bump_settings WHERE status <> ? ';

			$not_active_bumps = parent::getAdapter()->fetchAll($sql, array(self::BUMPS_SETTINGS_ACTIVE), Zend_Db::FETCH_ASSOC);
			$not_active_bump_ids = array();
			foreach($not_active_bumps as $bump_setting) {
				
				parent::getAdapter()->insert('expired_bump_settings', $bump_setting);
				
				$not_active_bump_ids[] = $bump_setting['id'];
			}
			
			if(count($not_active_bump_ids) > 0){
				parent::getAdapter()->query('DELETE FROM bump_settings WHERE id IN (' . implode(',' ,$not_active_bump_ids) . ')');
			}
					
			parent::db()->commit();
		} catch(Exception $ex) {
			parent::db()->rollback();
			throw $ex;
		}
	}
	
	public function	triggerBump($bump_settings_id, $user_id, $ads_id, $interval)
	{
		$has_credits = parent::getAdapter()->fetchOne('SELECT TRUE FROM users WHERE id = ? AND credits > 0 ', array($user_id));
		if($has_credits){
			$data_settings = array(
				'update_date' => new Zend_Db_Expr('DATE_ADD(update_date, INTERVAL '. $interval . ' SECOND )'),
				'bumps_count' =>  new Zend_Db_Expr('bumps_count + 1')
			);

			parent::getAdapter()->update('bump_settings', $data_settings, parent::getAdapter()->quoteInto('id = ?', $bump_settings_id));

			$data_log = array(
				'user_id' => $user_id,
				'ads_id' => $ads_id,
				'is_one_time' => 0,
				'bump_settings_id' => $bump_settings_id
			);

			parent::getAdapter()->insert('bumps_log', $data_log);
			parent::getAdapter()->update('ads', array('date_bumped' =>  time()), parent::getAdapter()->quoteInto('id = ?', $ads_id));
			parent::getAdapter()->update('users', array('credits' =>  new Zend_Db_Expr('credits - 1')), parent::getAdapter()->quoteInto('id = ?', $user_id));
		}
		
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
    {
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS a.id, a.user_id, a.title, a.status, a.category, a.city, 
			op.expiration_date, p.name as package_name,
			UNIX_TIMESTAMP(bl.date) as bump_date , bl.is_one_time, bl.bump_settings_id
			FROM bumps_log bl 
			INNER JOIN ads a ON a.id = bl.ads_id
			LEFT JOIN order_packages op ON a.id = op.ads_id AND op.status = 2
			LEFT JOIN packages p ON op.package_id = p.id 
			WHERE 1
		';
		
		$where = '';
		
		if ( strlen($filter['id']) ) {
			$where .= self::quote('AND a.id = ?', $filter['id']);
		}
		
		if ( strlen($filter['user_id']) ) {
			$where .= self::quote('AND a.user_id = ?', $filter['user_id']);
		}
		
		if ( $filter['date_from'] && $filter['date_to'] ) {
			if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
				$where .= self::quote(' AND DATE(bl.date) = DATE(FROM_UNIXTIME(?))', $filter['date_from']);
			}
		}

		if ( $filter['date_from'] ) {
			$where .= self::quote(' AND DATE(bl.date) >= DATE(FROM_UNIXTIME(?))', $filter['date_from']);
		}

		if ( $filter['date_to'] ) {
			$where .= self::quote(' AND DATE(bl.date) <= DATE(FROM_UNIXTIME(?))', $filter['date_to']);
		}
		
		$sql .= $where;
		$sqlCount = "SELECT FOUND_ROWS();";
		
		$sql .= '
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		$results = parent::_fetchAll($sql);
		$count = $this->getAdapter()->fetchOne($sqlCount);

		return $results;
    }
	
	public function getSettingsViewById($id)
	{
		$settings = $this->getAdapter()->fetchRow('SELECT interval_sec, periode_sec, bumps_count, bumps_total, status FROM bump_settings WHERE id = ? ', array($id));
		
		if(!$settings){
			$settings = $this->getAdapter()->fetchRow('SELECT interval_sec, periode_sec, bumps_count, bumps_total, status FROM expired_bump_settings WHERE id = ? ', array($id));
		}
		
		return 'every <b>'. $settings->interval_sec / 60 .'</b> minute for <b>' . $settings->periode_sec /3600 
				. '</b> hour, <br> bumped <b>' . $settings->bumps_count . '/' . $settings->bumps_total . '</b>,'. ' status <b>'.self::$STATUSES[$settings->status].'<b>';
		
	}		
	
	public function getAllSettings($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
    {
		$where = '';
		$sql_active = ' SELECT bs.id, bs.ads_id, bs.user_id, bs.interval_sec, bs.periode_sec, bs.status, bs.bumps_count, bs.bumps_total,
				UNIX_TIMESTAMP(bs.creation_date) as creation_date, UNIX_TIMESTAMP(bs.update_date) as update_date  FROM bump_settings bs WHERE 1 ';

		$sql_expired = 'SELECT ebs.id, ebs.ads_id, ebs.user_id, ebs.interval_sec, ebs.periode_sec, ebs.status,  ebs.bumps_count, ebs.bumps_total,
				UNIX_TIMESTAMP(ebs.creation_date) as creation_date, UNIX_TIMESTAMP(ebs.update_date) as update_date  FROM expired_bump_settings ebs WHERE 1 ';
		
		if ( strlen($filter['id']) ) {
			$where .= self::quote('AND id = ?', $filter['id']);
		}
		
		if ( strlen($filter['user_id']) ) {
			$where .= self::quote('AND user_id = ?', $filter['user_id']);
		}
		
		if ( strlen($filter['status']) ) {
			$where .= self::quote('AND status = ?', $filter['status']);
		}
		
		$sql_active .= $where;
		$sql_expired .= $where;
		
		$sql .= 'SELECT SQL_CALC_FOUND_ROWS x.* FROM ('
			.$sql_active. ' UNION '. $sql_expired .
			') x';	
	
		$sqlCount = "SELECT FOUND_ROWS();";

		$sql .= '
				ORDER BY ' . $sort_field . ' ' . $sort_dir . '
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		$results = parent::_fetchAll($sql);
		$count = $this->getAdapter()->fetchOne($sqlCount);
		
		return $results;
    }
}
