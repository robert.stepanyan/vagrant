<?php
class Model_Issues extends Cubix_Model
{
	protected $_table = 'support_tickets';
	protected $_itemClass = 'Model_IssueItem';

	const STATUS_TICKET_OPENED = 1;
	const STATUS_TICKET_CLOSED = 2;	
	
	const STATUS_NEW_UNREAD = 1;
	const STATUS_NOT_REPLIED = 2;
	const STATUS_READ_REPLIED = 3;
	const STATUS_NEW_REPLY = 4;
	const STATUS_ADMIN_SENT = 5;


	public $keywords = ["ticketid" =>"id", 'username' => "username"];

	public static function getUserIdByUserTypeAndId($user_type, $id)
	{
		if ( ! $user_type || ! $id ) return false;
		$db = Zend_Registry::get('db');

		$sql = 'SELECT user_id FROM ' . $user_type . ' WHERE id = ?';
		return $db->fetchOne($sql, $id);
	}

	public function get($id)
	{
		$sql = '
			SELECT
				st.id AS id, st.user_id, st.subject,
				st.message, st.status, UNIX_TIMESTAMP(st.creation_date) AS creation_date,
				UNIX_TIMESTAMP(st.resolve_date) AS resolve_date, st.backend_user_id, u.username, u.type as usertype, u.id as user_id
			FROM support_tickets st
			INNER JOIN users u ON u.id = st.user_id
            LEFT OUTER JOIN ads as a ON a.user_id = u.id
			WHERE 1 AND st.id = ?
		';
		
		$issue = parent::_fetchRow($sql, array($id));
		
		$attached_files = parent::_fetchAll('SELECT * FROM ticket_attached_files WHERE ticket_comment_id IS NULL AND ticket_id = ?
		', $id);
		$data = array('issue'=> $issue , 'attach' => $attached_files  );
		
		return $data;
	}

	public function getComments($ticket_id)
	{
		$sql = '
			SELECT
				tc.id AS comment_id, st.id AS ticket_id, tc.date AS comment_date, tc.comment, u.username, bu.username AS bu_username
			FROM ticket_comments tc
			INNER JOIN support_tickets st ON st.id = tc.ticket_id
			INNER JOIN users u ON u.id = st.user_id
			LEFT JOIN backend_users bu ON bu.id = tc.backend_user_id
			WHERE 1 AND st.id = ?
		';
		
		$comments = parent::_fetchAll($sql, array($ticket_id));
		foreach($comments as &$comment)
		{
			$comment['attached_files'] = parent::getAdapter()->fetchAll('SELECT * FROM ticket_attached_files WHERE ticket_comment_id = ?
										', $comment['comment_id']);
		}
		
		return $comments;
	}

	public function addComment($data)
	{
		parent::getAdapter()->insert('ticket_comments', $data);
		return parent::getAdapter()->lastInsertId();
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
	    $where='';
		$sql = '
			SELECT
				st.id AS id, st.user_id, st.subject,
				st.message, st.status, st.status_progress , UNIX_TIMESTAMP(st.creation_date) AS creation_date,
				UNIX_TIMESTAMP(st.resolve_date) AS resolve_date, st.backend_user_id, u.username, u.type,
			    if(u.type =1, "advertisement", "member") as user_type,   
			    a.id AS ad_id, bu.first_name, st.disabled_in_pa
			FROM support_tickets st
			INNER JOIN users u ON u.id = st.user_id
			LEFT JOIN ads a ON a.user_id = u.id
			LEFT JOIN backend_users bu ON bu.id = st.backend_user_id
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(DISTINCT st.id)
			FROM support_tickets st
			INNER JOIN users u ON u.id = st.user_id
			LEFT JOIN ads a ON a.user_id = u.id
			WHERE 1
		';

		if ( strlen($filter['st.disabled_in_pa']) ) {
			$where .= ' AND st.disabled_in_pa = 1 ';
		}
		
		if ( strlen($filter['u.username']) ) {
			$where .= self::quote('AND u.username LIKE ?', $filter['u.username'] . '%');
		}
		
		if ( strlen($filter['st.id']) ) {
			$where .= self::quote('AND st.id = ?', $filter['st.id']);
		}
		
		if ( strlen($filter['ad.id']) ) {
			$where .= self::quote('AND a.id = ?', $filter['ad.id'] );
		}
		if ( strlen($filter['st.status']) ) {
			$where .= self::quote('AND st.status = ?', $filter['st.status']);
		}
		
		if ( strlen($filter['st.backend_user_id']) ) {
			$where .= self::quote('AND st.backend_user_id = ?', $filter['st.backend_user_id']);
		}
		
		if ( strlen($filter['st.status_progress']) ) {
			$where .= self::quote('AND st.status_progress = ?', $filter['st.status_progress']);
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY st.id
			ORDER BY '  . $sort_field . ' ' . $sort_dir .  '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		/*echo $sql;
		echo "<br/>";
		echo $countSql;die;*/
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}
	
	public function insert($data)
	{
		parent::getAdapter()->insert('support_tickets', $data);
		return self::db()->lastInsertId();
		
	}
	
	public function remove($id)
	{
		parent::remove(self::quote('id = ?', $id));
		parent::getAdapter()->delete('ticket_comments', parent::quote('ticket_id = ?', $id));

        $attached_files = parent::getAdapter()->fetchAll('SELECT *, "attached" as catalog_id FROM ticket_attached_files WHERE ticket_id = ?', $id);
        if (!empty($attached_files)){
            (new Cubix_Documents())->remove($attached_files);
            parent::getAdapter()->delete('ticket_attached_files', parent::quote('ticket_id = ?', $id));
        }

	}

	public function setStatus($data)
	{
		$up_data = array('status' => $data['status'], 'resolve_date'=> $data['resolve_date']);
		
		if ( isset($data['disabled_in_pa']) ) {
			$up_data = array_merge($up_data, array('disabled_in_pa' => $data['disabled_in_pa'], 'is_read' => 1, 'status_progress' => self::STATUS_READ_REPLIED));
		}
		elseif ($data['status'] == self::STATUS_TICKET_CLOSED){
			$up_data = array_merge($up_data, array('is_read' => 1, 'status_progress' => self::STATUS_NOT_REPLIED));
		}
		
		parent::getAdapter()->update('support_tickets', $up_data, parent::getAdapter()->quoteInto('id = ?', $data['id']));
	}

	public function getOpenedCount()
	{
		$_sql = array(
			'tables' => array('support_tickets st'),
			'fields' => array(
				'COUNT(st.id)'
			),
			'joins' => array(
				'INNER JOIN users u ON u.id = st.user_id'
			),
			'where' => array(
				'st.status = ?' => self::STATUS_TICKET_OPENED
			)
		);

		$sql = self::getSql($_sql);

		return parent::getAdapter()->fetchOne($sql);
	}

	public function changeBackendUserId($id, $backend_user_id)
	{
		parent::getAdapter()->update($this->_table, array('backend_user_id' => $backend_user_id), parent::quote('id = ?', $id));
	}
	
	public function update($id, $data)
	{
		parent::getAdapter()->update($this->_table, $data, parent::getAdapter()->quoteInto('id = ?', $id));
	}
	
	public function addAttached($data)
	{
		parent::getAdapter()->insert('ticket_attached_files', $data);
		return parent::getAdapter()->lastInsertId();
	}


    /**
     * @param array|int $id
     * @return array
     */
	public function getAttachedById($id)
	{
	    if (is_integer($id)){
	        $id = array($id);
        }
	    $ids = implode(',', $id);
	    $sql = "SELECT *, 'attached' as catalog_id FROM ticket_attached_files WHERE id in({$ids})";

        return parent::getAdapter()->fetchAll($sql);
	}


    /**
     * @param int $id attached file id
     * @return int count
     */
	public function removeAttachedById($id)
	{
	    $attached = $this->getAttachedById($id);
	    if(!empty($attached)){
            (new Cubix_Documents())->remove($attached);
            if (is_integer($id)){
                $id = array($id);
            }
            $ids = implode(',', $id);
            return parent::getAdapter()->delete('ticket_attached_files', new Zend_Db_Expr("id IN({$ids})"));
        }
	    return 0;
	}

	public function updateAttached($data, $id){
		parent::getAdapter()->update('ticket_attached_files', $data, parent::getAdapter()->quoteInto('id = ?', $id));
	}


	public static function getMailInfo($ticket_id){
        $sql = "SELECT SP.id, SP.`status_progress`, SP.is_read, SP.disabled_in_pa, U.`language`, U.username, U.email 
        			FROM support_tickets SP 
				LEFT JOIN users U ON  U.id = SP.user_id
                WHERE SP.id = ? ";
                                
       return parent::_fetchRow($sql, array($ticket_id));
    } 
}
