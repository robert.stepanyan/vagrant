<?php
class Model_Cities extends Cubix_Model
{
    protected $_table = 'cities';

  

    
    
    public static function getAll() {
        
        $redisKey = "CitiesList";
        $citiesList = Core_Redis::get($redisKey);
         
        if($citiesList){
            return unserialize($citiesList);
        }
        
        $sql = 'SELECT id, `region_id`, `slug`, `name` FROM cities WHERE 1  = 1';
        
        $citiesList = parent::_fetchAll($sql);
        Core_Redis::set($redisKey, serialize($citiesList));   

        return $citiesList;
       
    }    
    
    
}
