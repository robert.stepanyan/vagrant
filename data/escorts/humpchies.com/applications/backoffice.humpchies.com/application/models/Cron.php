<?php

class Model_Cron
{
	/**
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $_db;

	/**
	 * @var Model_Cron
	 */
	protected static $_instance;

	/**
	 * @return Model_Cron
	 */
	public static function getInstance()
	{
		if ( empty(self::$_instance) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public static function run(array $stages) {
		$result = array('count' => 0, 'escorts' => array(), 'stages' => array(), 'counts' => array());

		$instance = self::getInstance();

		foreach ( $stages as $stage ) {
			$stage = 'process ' . str_replace(array('_', '-'), array(' ', ' '), $stage);
			$stage = str_replace(' ', '', ucwords($stage));
			if ( ! isset($result['stages'][$stage]) ) $result['stages'][$stage] = array();
            echo $stage. "\n";   
			list($count, $escorts) = $instance->$stage();
			$result['count'] += $count;
			$result['counts'][$stage] = $count;
			if (is_array($escorts) ) {
				$result['escorts'] = array_merge($result['escorts'], $escorts);
			}
			$result['stages'][$stage][] = $escorts;
		}

		if (is_array($result['escorts']) ) {
			$result['escorts'] = array_unique($result['escorts']);
		}
		return $result;
	}

	public function __construct()
	{
		$this->_db = Zend_Registry::get('db');
	}

	public function ProcessPendingTransfers()
	{
		$result = array(0, array());
		return $result;
		//if ( ! isset(Cubix_Application::getId()) || ! Cubix_Application::getId() ) return $result;
		if ( Cubix_Application::getId() != APP_A6 && Cubix_Application::getId() != APP_A6_AT ) return $result;
		
		$transfers = $this->_db->fetchAll('
			SELECT
				t.id, t.date_transfered, t.status
			FROM transfers t
			INNER JOIN users u ON u.id = t.user_id
			INNER JOIN backend_users bu ON bu.id = t.backend_user_id			
			WHERE t.status = ? /*AND transfer_type_id = 1*/ AND DATE_SUB(CURDATE(),INTERVAL 4 DAY) >= DATE(t.date_transfered) AND transfer_type_id <> 8
			GROUP BY t.id
		', array(Model_Billing_Transfers::STATUS_PENDING));
		
		
		
		$model = new Model_Billing_Transfers();
		
		foreach ( $transfers as $tr ) {
			$transfer = $model->getDetails($tr->id);
			
			if ( Model_Billing_Transfers::STATUS_PENDING == $transfer->status || Model_Billing_Transfers::STATUS_CONFIRMED == $transfer->status ) {
				$transfer->autoReject("Transfer is auto rejected.", false);
			}
		}

		return $result;
	}
	
	public function ProcessPendingBankTransfers()
	{
		$bank_transfers = $this->_db->fetchAll('
			SELECT
				t.id, t.date_transfered, t.status
			FROM transfers t
			INNER JOIN users u ON u.id = t.user_id
			INNER JOIN backend_users bu ON bu.id = t.backend_user_id			
			WHERE t.status = ? AND transfer_type_id = 1 AND DATE_SUB(CURDATE(),INTERVAL 10 DAY) >= DATE(t.date_transfered)
			GROUP BY t.id
		', array(Model_Billing_Transfers::STATUS_PENDING));
		
		$result = array(0, array());
		
		$model = new Model_Billing_Transfers();
		
		foreach ( $bank_transfers as $tr ) {
			$transfer = $model->getDetails($tr->id);
			
			if ( Model_Billing_Transfers::STATUS_PENDING == $transfer->status || Model_Billing_Transfers::STATUS_CONFIRMED == $transfer->status ) {
				$transfer->autoReject("Bank Transfer is auto rejected.", false);
			}
		}

		return $result;
	}
	
	public function ProcessPaymentRejectedOrders()
	{
		$orders = $this->_db->fetchAll('
			SELECT
				o.id, u.id AS user_id, system_order_id, o.price, op.id AS order_package_id
			FROM orders o
			INNER JOIN users u ON u.id = o.user_id
			INNER JOIN backend_users bu ON bu.id = o.backend_user_id
			INNER JOIN order_packages op ON op.order_id = o.id
			WHERE o.status = ? AND op.status = ? /*AND o.use_balance = 1*/
			GROUP BY o.id
			ORDER BY u.id ASC, o.order_date ASC
		', array(Model_Billing_Orders::STATUS_PAYMENT_REJECTED, Model_Billing_Packages::STATUS_ACTIVE));
		
		$result = array(0, array());
		$model = new Model_Billing_Orders();
		foreach ( $orders as $order ) {
			$this->_db->update('order_packages', array(
					'status' => Model_Billing_Packages::STATUS_PENDING
			), $this->_db->quoteInto('id = ?', $order->order_package_id));

			Zend_Registry::get('BillingHooker')->notify('order_created', array($order->order_package_id, $activated));
		}

		return $result;
	}
	
	public function ProcessPendingOrders()
	{
		$orders = $this->_db->fetchAll('
			SELECT
				o.id, u.id AS user_id, system_order_id, o.price
			FROM orders o
			INNER JOIN users u ON u.id = o.user_id
			INNER JOIN backend_users bu ON bu.id = o.backend_user_id
			INNER JOIN order_packages op ON op.order_id = o.id
			WHERE o.status IN (?, ?) AND o.use_balance = 1
			GROUP BY o.id
			ORDER BY u.id ASC, o.order_date ASC
		', array(Model_Billing_Orders::STATUS_PENDING, Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED));
		
		$result = array(0, array());
		$model = new Model_Billing_Orders();
		foreach ( $orders as $order ) {
			if ( true === $model->pay($order->id) ) {
				$this->_log('Order paid', array('Order' => $order->id));
				$result[0]++;
			}
		}

		return $result;
	}

	public function ProcessExpiredPackages()
	{
		$exclude = " ";
		
		/* --> Get all expired packages */
		$expired_packages = $this->_db->fetchAll("
			SELECT
				op.id, op.ads_id, op.status, previous_order_package_id, op.package_id, u.email AS user_email
			FROM order_packages op
			INNER JOIN ads a ON a.id = op.ads_id
			INNER JOIN users u ON u.id = a.user_id
			WHERE DATE(op.expiration_date) <= DATE(NOW()) AND op.status = ? AND op.status <> ? {$exclude}
		", array(Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED));
		/* <-- */

		$result = array(0, array());
		foreach ( $expired_packages as $package ) {
			$result[1][] = $package->ads_id;
			/* --> Get packages that are needed to be activate after current's expiration */
			$packages = $this->_db->fetchAll('
				SELECT
					op.id, op.ads_id, o.status, o.activation_condition
				FROM order_packages op
				INNER JOIN orders o ON o.id = op.order_id
				WHERE
					op.activation_type = ? AND
					op.status = ? AND
					op.previous_order_package_id = ? AND
					(o.status = ? OR o.status = ? )
			', array(
				Model_Packages::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES,
				Model_Packages::STATUS_PENDING,
				$package->id,
				Model_Billing_Orders::CONDITION_AFTER_PAID,
				Model_Billing_Orders::CONDITION_AFTER_RECEIVING_PAYMENT_DETAILS
			));
			/* <-- */

			$this->_db->beginTransaction();
			try {
				$activated = array();
				foreach ( $packages as $p ) {
					$this->_ActivatePackage($p->id, 'ProcessExpiredPackages');
					$activated[] = $p->id;
					$result[0]++;
					$result[1][] = $p->ads_id;
				}
				
				$this->_db->update('order_packages', array(
					'status' => Model_Billing_Packages::STATUS_EXPIRED
				), $this->_db->quoteInto('id = ?', $package->id));

				Zend_Registry::get('BillingHooker')->notify('package_expired', array($package->id, $activated));

				$this->_db->commit();

				$result[0]++;
				$result[1][] = $package->ads_id;
				
				$this->_log('Package expired', array('Expired Package' => $package->id, 'Activated Instead' => $activated));
			}
			catch (Exception $e) {
				$this->_db->rollBack();
				$this->_log('Unable to expire the package', array('Package' => $package->id), 'ERROR');
			}
		}

		return $result;
	}
    
    public function ProcessSendExpireNotifications()
    {
        $exclude = " ";
        try {
            $result[0] = 4;
            $result[1]  =array(1,2,3,4);
        }catch (Exception $e) {
                $this->_log('jumped', array('Package' => 'no'), 'ERROR');
            }
        return $result;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
	
	public function ProcessExpiredPhonePackages()
	{		
		/* --> Get all expired packages */
		$expired_packages = $this->_db->fetchAll("
			SELECT
				op.id, op.ads_id, op.status, op.application_id, previous_order_package_id
			FROM order_packages op
			INNER JOIN phone_packages pp ON pp.order_package_id = op.id
			WHERE pp.expiration_date <= NOW() AND op.status = ? AND op.status <> ? AND op.package_id IN (" . Model_Billing_Packages::PHONE_PACKAGE_1_DAY_PASS . ", " . Model_Billing_Packages::PHONE_PACKAGE_3_DAY_PASS . ", " . Model_Billing_Packages::PHONE_PACKAGE_5_DAY_PASS . ")
		", array(Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED));
		/* <-- */
//print_r($expired_packages);
		$result = array(0, array());
		foreach ( $expired_packages as $package ) {
			$result[1][] = $package->ads_id;
			/* --> Get packages that are needed to be activate after current's expiration */
			$packages = $this->_db->fetchAll('
				SELECT
					op.id, op.ads_id
				FROM order_packages op
				INNER JOIN orders o ON o.id = op.order_id
				WHERE
					op.activation_type = ? AND
					op.status = ? AND
					op.previous_order_package_id = ? AND
					op.application_id = ?
			', array(
				Model_Packages::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES,
				Model_Packages::STATUS_PENDING,
				$package->id,
				$package->application_id
			));
			/* <-- */

			$this->_db->beginTransaction();
			try {
				$activated = array();
				foreach ( $packages as $p ) {
					$this->_ActivatePackage($p->id, 'ProcessExpiredPhonePackages');
					$activated[] = $p->id;
					$result[0]++;
					$result[1][] = $p->ads_id;
				}

				$this->_db->update('order_packages', array(
					'status' => Model_Billing_Packages::STATUS_EXPIRED
				), $this->_db->quoteInto('id = ?', $package->id));

				Zend_Registry::get('BillingHooker')->notify('package_expired', array($package->id, $activated));

				$this->_db->commit();

				$result[0]++;
				$result[1][] = $package->ads_id;

				$this->_log('Package expired', array('Expired Package' => $package->id, 'Activated Instead' => $activated));
			}
			catch (Exception $e) {
				$this->_db->rollBack();

				$this->_log('Unable to expire the package', array('Package' => $package->id), 'ERROR');
			}
		}

		return $result;
	}

	public function ProcessExpiredPostcheckPackages()
	{
		$expired_transfers = $this->_db->fetchAll("
			SELECT
				t.id, t.date_transfered
			FROM transfers t			
			WHERE t.transfer_type_id = 8 AND t.status = 1 AND t.date_transfered < DATE_SUB(NOW(), INTERVAL ? DAY)
		", array(3));

		if ( count($expired_transfers) ) {
			foreach( $expired_transfers as $transfer ) {
				$transfer = new Model_Billing_TransferItem($transfer);

				$transfer->autoReject('72 Postcheck Auto Rejection!', true);
			}
		}
	}

	public function ProcessAsapPackages()
	{
		$packages = $this->_GetWaittingPackages(Model_Packages::ACTIVATE_ASAP);

		$result = array(0, array());
		foreach ( $packages as $package ) {
			$this->_ActivatePackage($package->id, 'ProcessAsapPackages');
			$this->_log('Asap package activated', array('Package' => $package->id));
			$result[0]++;
			$result[1][] = $package->ads_id;
		}

		return $result;
	}

	public function ProcessExactDatePackages()
	{
		$packages = $this->_GetWaittingPackages(Model_Packages::ACTIVATE_AT_EXACT_DATE, 'DATE_FORMAT(op.activation_date, "%Y-%m-%d %H:%i:%s") <= DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i:%s")');

		$result = array(0, array());
		foreach ( $packages as $package ) {
			$this->_ActivatePackage($package->id, 'ProcessExactDatePackages');
			$this->_log('Exact date package activated', array('Package' => $package->id));
			$result[0]++;
			$result[1][] = $package->ads_id;
		}

		return $result;
	}
	
	protected function ProcessFixAfterCurrentPackageExpiresPackages()
	{
		$packages = $this->_GetWaittingPackages(Model_Packages::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES);
		$model_package = new Model_Billing_Packages();
		
		$result = array(0, array());
		foreach ( $packages as $package ) {
			$prev_order_package = $model_package->get($package->previous_order_package_id);
			if ( $prev_order_package->status != Model_Billing_Packages::STATUS_ACTIVE &&  $prev_order_package->status != Model_Billing_Packages::STATUS_SUSPENDED ) 
			{
				$this->_ActivatePackage($package->id, 'ProcessFixAfterCurrentPackageExpiresPackages');
				$this->_log('Fixing After Current Package Expires Package activation', array('Package' => $package->id));
				$result[0]++;
				$result[1][] = $package->ads_id;
			}
		}

		return $result;
	}

	/**
	 * Returns all packages that are waitting for activation
	 *
	 * @param int $which Package activation type
	 * @return array
	 */
	protected function _GetWaittingPackages($which, $additional = '')
	{
		$packages = $this->_db->fetchAll('
			SELECT
				op.id, op.activation_date, op.ads_id, op.previous_order_package_id
			FROM order_packages op
			INNER JOIN orders o ON o.id = op.order_id
			INNER JOIN ads a ON a.id = op.ads_id
			WHERE
				op.status = ? AND (
					( o.activation_condition = ? AND o.status = ? ) OR
					( o.activation_condition = ? AND o.status IN (?, ?) ) OR
					( o.activation_condition = ? AND o.status IN (?, ?, ?) )
				) AND op.activation_type = ?' . (strlen($additional) ? ' AND ' . $additional : '') . '
				AND a.status = "active"
			ORDER BY op.ads_id, o.order_date DESC
		', array(
			Model_Billing_Packages::STATUS_PENDING,

			/* --> Main Conditions-Statuses Logic (it describes which ones are "waitting for activation" packages) */
			Model_Billing_Orders::CONDITION_AFTER_PAID, Model_Billing_Orders::STATUS_PAID,
			Model_Billing_Orders::CONDITION_AFTER_RECEIVING_PAYMENT_DETAILS, Model_Billing_Orders::STATUS_PAID, Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED,
			Model_Billing_Orders::CONDITION_WITHOUT_PAYMENT, Model_Billing_Orders::STATUS_PAID, Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED, Model_Billing_Orders::STATUS_PENDING,
			/* <-- */
			$which
		));

		return $packages;
	}
	

	protected function _ActivatePackage($order_package_id, $func_name = null)
	{
		list($ads_id, $period, $de_days, $package_id) = $this->_db->fetchRow('
			SELECT ads_id, period, de_days, package_id FROM order_packages WHERE id = ? AND order_id IS NOT NULL
		', $order_package_id, Zend_Db::FETCH_NUM);

		$m_ads = new Model_Advertisements();
		$ads_data = $m_ads->get($ads_id);	
		// Process if escort status is active
		if ( $ads_data->status == 'active' ) {
			/* --> Cancel Current Active Package of Escort and Chargeback to Balance */
			$current_order_package_id = $this->_db->fetchOne('
				SELECT id FROM order_packages WHERE status = ? AND ads_id = ?
			', array(Model_Packages::STATUS_ACTIVE, $ads_id));

			if ( $current_order_package_id ) {
				$comment = 'Package #' . $current_order_package_id . ' cancelled by cron. Needed to activate package #' . $order_package_id;
				
				if ( $func_name ) {
					$comment .= ' : Function name: ' . $func_name;
				}
				
				$model = new Model_Billing_Packages();
				$model
					->get($current_order_package_id)
					->cancel($comment);
			}
			/* <-- */

			/* --> Activate Package Immediately */
			$date_activated = new Zend_Db_Expr('NOW()');
			$expiration_date = new Zend_Db_Expr('FROM_UNIXTIME(' . strtotime('+' . $period + $de_days . ' days', time()) . ')');
			
			
			$order = $this->_db->fetchRow('
				SELECT o.id, has_rejected_transfer 
				FROM order_packages op 
				INNER JOIN orders o ON o.id = op.order_id
				WHERE op.id = ?
			', array($order_package_id));
			
			
			$order_packages_update_data = array(
				'expiration_date' => $expiration_date,
				'status' => Model_Packages::STATUS_ACTIVE,
				'date_activated' => $date_activated
			
			);
			//IF PAYING FOR ORDER WITH STATUS transfer_rejected update only status
			//KEEP expiration and activation dates THE SAME
			if ( $order->has_rejected_transfer ) {
				unset($order_packages_update_data['expiration_date']);
				unset($order_packages_update_data['date_activated']);
			}
			//REMOVING has_rejected_transfer STATUS
			$this->_db->update('orders', array('has_rejected_transfer' => 0), $this->_db->quoteInto('id = ?', $order->id));
			
			$this->_db->update('order_packages', $order_packages_update_data, $this->_db->quoteInto('id = ?', $order_package_id));
			/* <-- */
			
			Zend_Registry::get('BillingHooker')->notify('package_activated', array($order_package_id, $ads_id, $date_activated, $expiration_date));

			/*$this->_db->update('balance_packages_activation', array(
				'date' => new Zend_Db_Expr('NOW()')
			), $this->_db->quoteInto('order_package_id = ?', $order_package_id));*/
		}
	}

	public function ProcessGracePeriod()
	{
		$config = Zend_Registry::get('system_config');
		$config = $config['billing'];
		$first_grace_period = $config['firstGracePeriod'];
		$second_grace_period = $config['secondGracePeriod'];

		$result = array(0, array());
		/* --> First Grace Period */
		$transfers = $this->_db->fetchAll('
			SELECT
				t.id
			FROM transfers t
			INNER JOIN transfer_orders `to` ON to.transfer_id = t.id
			WHERE t.status = ? AND t.date_transfered < DATE_SUB(NOW(), INTERVAL ? DAY)
		', array(Model_Billing_Transfers::STATUS_PENDING, $first_grace_period));

		$errors = array();
		foreach ( $transfers as $transfer ) {
			try {
				$transfer = new Model_Billing_TransferItem($transfer);
				$transfer->reject('First Grace Period Expired', false);
			}
			catch ( Exception $e ) {
				$errors[] = $transfer->id;
			}
		}

		if ( count($errors) ) {
			$this->_log('Could not reject following transfers in first grace period', array('Transfers' => $errors), 'ERROR');
		}
		/* <-- */
		
		/* --> Second Grace Period */
		$transfers = $this->_db->fetchAll('
			SELECT
				t.id
			FROM transfers t
			INNER JOIN transfer_rejections tr ON tr.transfer_id = t.id
			WHERE t.status = ? AND tr.date < DATE_SUB(NOW(), INTERVAL ? DAY)
		', array(Model_Billing_Transfers::STATUS_REJECTED, $second_grace_period));
		
		$errors = array();
		foreach ( $transfers as $transfer ) {
			try {
				$transfer = new Model_Billing_TransferItem($transfer);
				foreach ( $transfer->getOrders() as $order ) {
					if ( $order->getStatus() == Model_Billing_Orders::STATUS_PAYMENT_REJECTED ) {
						// This function returns escort ids for which packages have been cancelled
						$escorts = $order->close('Automatic close grace period expired');
						$result[1] = array_merge($result[1], $escorts);
					}
				}
			}
			catch ( Exception $e ) {
				$errors[] = $transfer->id;
			}

			$this->_db->update('transfers', array(
				'status' => Model_Billing_Transfers::STATUS_AUTO_REJECTED
			), $this->_db->quoteInto('id = ?', $transfer->id));

			Zend_Registry::get('BillingHooker')->notify('transfer_rejected', array($transfer->id));
		}

		if ( count($errors) ) {
			$this->_log('Could not close all orders for transfers in second grace period', array('Transfers' => $errors), 'ERROR');
		}
		/* <-- */

		return $result;
	}

	protected $_log_file = 'billing-cron.log';

	public function _log($message, array $data = array(), $type = 'INFO')
	{
		foreach ( $data as $key => $value ) {
			if ( is_array($value) ) {
				$value = '(' . implode(', ', $value) . ')';
			}
			
			$data[$key] = $key . ': ' . $value;
		}

		$message = '[' . $type . ' ' . date('d.m.y H:i:s') . ']: ' . $message . ' (' . implode(', ', $data) . ')' . "\r\n";

		// echo nl2br($message);

		@file_put_contents($this->_log_file, $message, FILE_APPEND);
	}

	const HH_STATUS_ACTIVE = 1;
	const HH_STATUS_PENDING = 2;
	const HH_STATUS_EXPIRED = 3;
	const HH_STATUS_ADMIN_DISABLED = 4;
	
	private function dateDiff($date_start, $date_end)
	{
	    $difference = $date_end - $date_start;
        $minutes = floor(($difference / 3600) * 60); // 3600 seconds in an hour

		return $minutes;
	}

	

	public function ProcessTrialPackageExpiresEmail()
	{
		$trial_escorts = $this->_db->fetchAll('
			SELECT
				e.id AS ads_id, e.showname, /*UNIX_TIMESTAMP(op.expiration_date) AS exp_date,*/ u.email, op.id AS order_package_id
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN order_packages op ON op.ads_id = e.id
			WHERE op.package_id = 98 AND op.status = ? AND u.user_type = \'escort\' AND e.home_city_id IS NULL AND op.trial_sent = 0 AND (op.expiration_date = DATE_ADD(CURDATE(), INTERVAL +1 DAY) OR op.expiration_date = CURDATE())
		', array(Model_Packages::STATUS_ACTIVE));

		if ( count($trial_escorts) ) {
			Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById()->api_key);

			foreach( $trial_escorts as $escort ) {

				echo "Sending \"Trial Package Expires\" email to escort " . $escort->showname . "\r\n";
				Cubix_Email::sendTemplate('free_trial_package_expires', $escort->email, array('showname' => $escort->showname));

				$this->_db->update('order_packages', array('trial_sent' => 1), $this->_db->quoteInto('id = ?', $escort->order_package_id));
			}
		}
	}
		
	public function ProcessActivePackagesWithoutExpDate()
	{		
		$order_packages = $this->_db->fetchAll('
			SELECT id, order_id FROM order_packages WHERE status = ? and order_id IS NOT NULL AND expiration_date IS NULL
		', array(Model_Billing_Packages::STATUS_ACTIVE));
		
		
		foreach ( $order_packages as $package ) {
			$this->_db->update('order_packages', array('status' => Model_Billing_Packages::STATUS_EXPIRED, 'status_comment' => 'Package expired because of no expiration and activation dates defined.'), $this->_db->quoteInto('id = ?', $package->id));
			echo "Package #" . $package->id . " is expired. \n";
		}
	}
}
