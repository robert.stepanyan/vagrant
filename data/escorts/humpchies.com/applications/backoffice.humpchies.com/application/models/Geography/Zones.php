<?php

class Model_Geography_Zones extends Cubix_Model
{
	protected $_table = 'zones';
	
	public function getAll( $name = null)
	{	

		$where = '';

		if ( $name ) {
			$where = ' AND name LIKE "%' . $name . '%"';
		}
		
		$count = self::getAdapter()->fetchOne('
			SELECT COUNT(id) FROM zones 
			WHERE 1 ' . $where);
		
		return parent::_fetchAll('
			SELECT id, name FROM zones
			WHERE 1 ' . $where);
	}
	
	public function get($id)
	{
		return parent::_fetchRow('
			SELECT id, name  FROM zones
			WHERE id = ?
		', $id);
	}
	
	public function save(Model_Geography_CityzoneItem $item) {
		$name = Cubix_Utils::makeSlug($item->name);
		$item->setName($name);
		
		parent::save($item);
	}
	
	public function remove($id)
	{
		parent::remove(self::quote('id = ?' , $id));
	}
}
