<?php

class Model_Plugin_Filter extends Zend_Controller_Plugin_Abstract
{	
	/**
	 * @param Zend_Controller_Request_Abstract $request
	 */
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		$module = $request->getModuleName();
		$controller = $request->getControllerName();
		$action = $request->getActionName();

		if ( $controller == 'escorts' && $action == 'index' ) {
			header('Location: /escorts-v2');
			die;
		}
		
		if ( ! defined('IS_CLI') || ! IS_CLI ) {
			$bu_user = Zend_Auth::getInstance()->getIdentity();
			Model_BOUsersSessions::refresh($bu_user->id);
		}

		$controllers_map = array(
			'escorts',
			'issues',
			'escorts-v2',
			'agencies',
			'members',
			'orders',
			'transfers',
			'packages',
			'reviews',
			'comments',
			'instances',
			'pinboard',
			'blog',
			'followers'
		);

		$actions_map = array(
			'data',
			'vacation-data'
		);

		$ignore_params_map = array(
			'application_id',
			'controller',
			'action',
			'module',
			'lang_id',
			'sort_field',
			'sort_dir',
			'page',
			'per_page'
		);

		if ( in_array($controller, $controllers_map) && in_array($action, $actions_map) )
		{
			$req_params = $request->getParams();
			$ses_filter = new Zend_Session_Namespace($module . '_' . $controller . '_' . $action);
			//echo $module . '_' . $controller . '_' . $action; die;
			$params = array();
			foreach ( $req_params as $k => $param ) {
				if ( ! in_array($k, $ignore_params_map) ) {
					if ( @strlen($param) == 0 ) {
						$params[$k] = '';
					}
					else {
						$params[$k] = trim($param);
					}
				}
			}

			$ses_filter->filter_params = $params;
		}
	}

	/**
	 * @param Zend_Controller_Request_Abstract $request
	 */
	public function  routeShutdown(Zend_Controller_Request_Abstract  $request)
	{
		
	}
	
	
}
