<?php

class Model_Plugin_Auth extends Zend_Controller_Plugin_Abstract
{
	/**
	 * @var Zend_Auth
	 */
	protected $_auth;

	public function __construct()
	{
		$this->_auth = Zend_Auth::getInstance();
	}

	public function hasAccess($request, array $permissions = array())
	{
		$m = $request->getModuleName();
		$c = $request->getControllerName();
		$a = $request->getActionName();
		
		$allowed = isset($permissions['allowed']) ? $permissions['allowed'] : null;
		$disallowed = isset($permissions['disallowed']) ? $permissions['disallowed'] : null;

		if ( ! isset($allowed[$m]) ) {
			return false;
		}
		elseif ( $allowed[$m] == '*' ) {}
		elseif ( ! isset($allowed[$m][$c]) ) {
			return false;
		}
		elseif ( $allowed[$m][$c] == '*' ) {}
		elseif ( ! in_array($a, $allowed[$m][$c]) ) {
			return false;
		}

		if ( ! isset($disallowed[$m]) ) {}
		elseif ( ! isset($disallowed[$m][$c]) ) {}
		elseif ( in_array($a, $disallowed[$m][$c])) {
			return false;
		}

		// If passed through filter return true
		return true;
	}

//	public function routeStartup(Zend_Controller_Request_Abstract  $request)
//	{
//		if ( defined('IS_CLI') && IS_CLI ) return;
//
//		$this->_auth = Zend_Auth::getInstance();
//		
//		if ( $this->_auth->hasIdentity() ) {
//			
//			$user = $this->_auth->getIdentity();
//			
//			$current_app_id = isset($_COOKIE['application_id']) ? $_COOKIE['application_id'] : null;
//			
//			if ( ! $current_app_id ) $current_app_id = $request->application_id;
//			
//			if ( ! $current_app_id ) {
//				$current_app_id = $user->application_id;
//			}
//			else if ( $user->type !== 'superadmin' && $user->type !== 'seo manager' && $current_app_id != $user->application_id ) {
//				$current_app_id = $user->application_id;
//			}
//
//			if ( ! $current_app_id ) {
//				$current_app_id = Cubix_Application::getAll();
//				$current_app_id = $current_app_id[0]->id;
//			}
//			
//			setcookie('application_id', $current_app_id, time() + 2 * 7 * 24 * 60 * 60 /* two weeks */, '/');
//			
//			$request->setParam('application_id', $current_app_id);
//			
//		}
//	}
	
	public function routeShutdown(Zend_Controller_Request_Abstract  $request)
	{
		if ( defined('IS_CLI') && IS_CLI ) return;
		
		$modul = $request->getModuleName();
		$controller = $request->getControllerName();
		$action =  $request->getActionName();
        //var_dump([$controller, $action]);
//        die();
        if (
            !($modul == 'billing' && $controller == 'online-billing' && ($action == 'mmg-bill-callback' || $action == 'bitcoin-callback' || $action == 'eth-callback') )
            && !($request->getModuleName() == 'sms' && $request->getControllerName() == 'index' && ($request->getActionName() == 'sms-callback' || $request->getActionName() == 'sms-inbox'))
        ) {

			if ( ! $this->_auth->hasIdentity() && $request->getControllerName() != 'auth' ) {

				header('Cubix-redirect: /auth');
				header('Location: /auth');
				die('Access denied!');
			}
		}
		
		$user = $this->_auth->getIdentity();
		
		if ( isset($user->type) && 'data entry' == $user->type ) {
			$permissions = array(
				'allowed' => array(
					'default' => array(
						'escorts' => '*',
						'escorts-v2' => '*',
						'agencies' => '*',
						'comments' => '*',
						'reviews' => '*',
						'retouch-photos' => '*',
						'settings' => '*',
						'dashboard' => array('index', 'dataentry', 'tip-data'),
						'auth' => array('login', 'logout', 'index'),
						'index' => array('quick-email'),
						'blog' => '*',
						'blog-comments' => '*',
					),
					'geography' => '*',
					'seo' => '*',
					'system' => array(
						'issue-templates' => '*'
					)
				),
				'disallowed' => array(
					'default' => array(
						'escorts' => array('delete'),
						'agencies' => array('delete')
					)
				)
			);

			if ( ! $this->hasAccess($request, $permissions)) {
				die(header('Location: /dashboard'));
			}
		}
		
		
		
		
	}
		
}
