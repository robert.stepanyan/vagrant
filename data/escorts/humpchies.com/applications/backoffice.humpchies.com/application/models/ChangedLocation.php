<?php

class Model_ChangedLocation extends Cubix_Model
{	
	
    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT e.showname, cl.id, cl.escort_id, cl.new_city_id, cl.old_city_id, UNIX_TIMESTAMP(cl.change_date) as change_date, cl.sales_id, bu.first_name as sales_person, cl.package_name, cl.order_id
			FROM changed_location as cl
			LEFT JOIN backend_users bu ON bu.id = cl.sales_id  
			LEFT JOIN escorts e ON e.id = cl.escort_id
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM changed_location
			WHERE 1
		';
		

		if ( !empty($filter['sales_user_id']) ) {
			$sql .= self::quote(' AND sales_id = ?', $filter['sales_user_id']);
			$countSql .= self::quote(' AND sales_id = ?', $filter['sales_user_id']);
		}

		if ( !empty($filter['date_from']) ) {

			$sql .= self::quote(" AND DATE(change_date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			$countSql .= self::quote(" AND DATE(change_date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
		}

		if ( !empty($filter['date_to']) ) {
			$sql .= self::quote(" AND DATE(change_date) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
			$countSql .= self::quote(" AND DATE(change_date) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}

		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		return parent::_fetchAll($sql);
	}

    public function save($data){
		self::getAdapter()->insert('changed_location', $data);
    }

    public function add($data){

    	$active_package = $this->getAdapter()->fetchRow(
    	   'SELECT op.order_id, p.name
			FROM order_packages as op
			LEFT JOIN packages p ON p.id = op.package_id
			WHERE op.id = ? ', $data['active_package']);

    	unset($data['active_package']);
    	$data['order_id'] = $active_package->order_id;
    	$data['package_name'] = $active_package->name;

		$this->save($data);
    }

    public function delete($id = array()){
        self::getAdapter()->delete('changed_location', self::getAdapter()->quoteInto('id = ?', $id));
    }

    public function checkChangedRegion($escortId,$new_city_id){

    	$oldRegion = $this->getAdapter()->fetchRow(
    	   'SELECT c.region_id, c.id
    		FROM escorts as e
    		LEFT JOIN cities c on c.id = e.city_id
    		WHERE e.id = ? 
    		', $escortId);

    	$newRegionId = $this->getAdapter()->fetchOne(
    	   'SELECT region_id
			FROM cities
			WHERE id =  ? 
    		', $new_city_id);

    	if($oldRegion->region_id != $newRegionId){
    		return $oldRegion->id;
    	}

    	return false;
    }
}