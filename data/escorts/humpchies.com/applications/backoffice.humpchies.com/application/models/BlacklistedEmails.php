<?php

class Model_BlacklistedEmails extends Cubix_Model
{

    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT id, name, reason, UNIX_TIMESTAMP(block_date) AS  block_date
			FROM blacklisted_emails
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM blacklisted_emails
			WHERE 1
		';

		if ( strlen($filter['name']) ) {
			$sql .= self::quote('AND name LIKE ?', '%' . $filter['name'] . '%');
			$countSql .= self::quote('AND name LIKE ?', '%' . $filter['name'] . '%');
		}


		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql .= '';

		$count = intval($this->getAdapter()->fetchOne($countSql));
		return parent::_fetchAll($sql);
	}

	public function get($id)
	{
		return self::_fetchRow('
			SELECT * FROM blacklisted_emails WHERE id = ?
		', array($id));
	}

    

    public function save($data,$id = null){
        if ( ! is_null($id) ) {
			self::getAdapter()->update('blacklisted_emails', $data, self::quote('id = ?', $id));
		}
		else {
			self::getAdapter()->insert('blacklisted_emails', $data);
		}
    }

    public function delete($id = array()){
        self::getAdapter()->delete('blacklisted_emails', self::getAdapter()->quoteInto('id = ?', $id));
    }


    public function checkWords($text){
        $sql = '
			SELECT id, name
			FROM blacklisted_emails
			WHERE 1';

        $wordList = parent::_fetchAll($sql);

        if( count($wordList) > 0 ){
            foreach ( $wordList as $word ){
                $pattern = '/^'.$word.'/';
                preg_match($pattern, $text, $matches, PREG_OFFSET_CAPTURE);
                var_dump($matches);
                exit;
            }
            
        }
        
    }

}
