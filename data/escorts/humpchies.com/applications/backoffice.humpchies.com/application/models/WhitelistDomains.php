<?php

class Model_WhitelistDomains extends Cubix_Model
{

    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT id, domain, added
			FROM domain_whitelist
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM domain_whitelist
			WHERE 1
		';

		if ( strlen($filter['name']) ) {
			$sql .= self::quote('AND domain LIKE ?', '%' . $filter['name'] . '%');
			$countSql .= self::quote('AND domain LIKE ?', '%' . $filter['name'] . '%');
		}


		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql .= '';

		$count = intval($this->getAdapter()->fetchOne($countSql));
		return parent::_fetchAll($sql);
	}

	public function get($id)
	{
		return self::_fetchRow('
			SELECT * FROM domain_whitelist WHERE id = ?
		', array($id));
	}

    

    public function save($data,$id = null){
        if ( ! is_null($id) ) {
			self::getAdapter()->update('domain_whitelist', $data, self::quote('id = ?', $id));
		}
		else {
			self::getAdapter()->insert('domain_whitelist', $data);
		}
    }

    public function delete($id = array()){
        self::getAdapter()->delete('blacklisted_emails', self::getAdapter()->quoteInto('id = ?', $id));
    }


    public function checkWords($text){
        $sql = '
			SELECT id, domain
			FROM domain_whitelist
			WHERE 1';

        $wordList = parent::_fetchAll($sql);

        if( count($wordList) > 0 ){
            foreach ( $wordList as $word ){
                $pattern = '/^'.$word.'/';
                preg_match($pattern, $text, $matches, PREG_OFFSET_CAPTURE);
                var_dump($matches);
                exit;
            }
            
        }
        
    }

}
