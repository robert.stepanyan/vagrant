<?php

class Model_Billing_Packages extends Cubix_Model
{
	protected $_table = 'order_packages';
	protected $_itemClass = 'Model_Billing_PackageItem';
	public $gotd_sum;

	const ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES = 6;
	const ACTIVATE_AT_EXACT_DATE = 7;
	const ACTIVATE_ASAP = 9;

	public static $ACTIVATE_LABELS = array(
		self::ACTIVATE_ASAP => 'ASAP',
		self::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES => 'after current package expires',
		self::ACTIVATE_AT_EXACT_DATE => 'at exact date'
	);

	const STATUS_PENDING  = 1;
	const STATUS_ACTIVE   = 2;
	const STATUS_EXPIRED  = 3;
	const STATUS_CANCELLED = 4;
	const STATUS_UPGRADED = 5;
	const STATUS_SUSPENDED = 6;

	public static $STATUS_LABELS = array(
		self::STATUS_PENDING  => 'pending',
		self::STATUS_ACTIVE   => 'active',
		self::STATUS_EXPIRED  => 'expired',
		self::STATUS_CANCELLED => 'cancelled',
		self::STATUS_UPGRADED => 'upgraded',
		self::STATUS_SUSPENDED => 'suspended'
	);
	
	const PRODUCT_SUSPENDABLE = 17;
	
	const PACKAGE_ABO = 14;
	const PACKAGE_TRANSITION = 9;
	const PACKAGE_7_DAYS = 13;
	
	const PHONE_PACKAGE_1_DAY_PASS = 19;
	const PHONE_PACKAGE_3_DAY_PASS = 20;
	const PHONE_PACKAGE_5_DAY_PASS = 23;
	
	const PACKAGE_DIAMOND_LIGHT = 102;
	const PACKAGE_DIAMOND_PREMIUM = 101;

	const PACKAGE_GOLD_TOUR_PREMIUM = 100;

	const DL_PRODUCT_PLUS_3_DAYS = 17;
	const DL_PRODUCT_PLUS_6_DAYS = 18;
	const DL_PRODUCT_PLUS_9_DAYS = 20;
	const DP_PRODUCT_PLUS_3_DAYS = 21;
	const DP_PRODUCT_PLUS_6_DAYS = 22;
	const DP_PRODUCT_PLUS_9_DAYS = 23;

	const GT_PRODUCT_PLUS_3_DAYS = 24;
	const GT_PRODUCT_PLUS_6_DAYS = 25;

	public static $EXTEND_PACKAGE_PRODUCTS = array(
		self::DL_PRODUCT_PLUS_3_DAYS,
		self::DL_PRODUCT_PLUS_6_DAYS,
		self::DL_PRODUCT_PLUS_9_DAYS,
		self::DP_PRODUCT_PLUS_3_DAYS,
		self::DP_PRODUCT_PLUS_6_DAYS,
		self::DP_PRODUCT_PLUS_9_DAYS,

		self::GT_PRODUCT_PLUS_3_DAYS,
		self::GT_PRODUCT_PLUS_6_DAYS,
	);
	
	
	const BL_HOLLAND_SPECIAL  = 20;
	const BL_BELGIQUE_SPECIAL = 19;

	public function get($id)
	{
		$wh = "  ";
		
		$sql = '
			SELECT
				a.id as ads_id,
				u.id AS user_id,
				op.id,
				op.id AS order_package_id,
				op.order_id,
				op.base_period,
				op.period,
				op.discount,
				op.discount_fixed,
				op.surcharge,
				op.price,
				op.base_price,
				op.date_activated,
				op.expiration_date,
				op.activation_type,
				op.previous_order_package_id,
				op.activation_date,
				/*ADDDATE(op.date_activated, op.period) AS expiration_date,*/

				p.name AS package_name,
				p.id AS package_id,
				u.id AS user_id,
				o.status AS order_status,
				op.status,
				o.id AS order_id,
				o.price AS order_price
				' . $wh . '
			FROM ads a
			INNER JOIN users u ON u.id = a.user_id
			LEFT JOIN order_packages op ON op.ads_id = a.id
			LEFT JOIN orders o ON o.id = op.order_id
			INNER JOIN packages p ON op.package_id = p.id
			WHERE op.id = ?
		';

		return parent::_fetchRow($sql, array($id));
	}
	
	
	public function getAllV2($page, $per_page, $filter, $sort_field, $sort_dir, &$count, $gotd = false)
	{
		$join = ' ';
		if ( strlen($filter['ag.name']) || $gotd ) {
			if( strlen($filter['ag.name']) ) {
				$join .= ' INNER JOIN agencies ag ON ag.id = e.agency_id ';
			}
			
			if ( $gotd ) {
				$join .= ' LEFT JOIN agencies ag ON ag.id = e.agency_id ';
				$join .= ' INNER JOIN gotd_orders go ON go.order_package_id = op.id ';
				$join .= ' INNER JOIN cities ct ON ct.id = go.city_id ';
			}
		}
		
		$wh = "  ";
		
		
		if ( $filter['more30'] == 1  ) {
			$join .= ' LEFT JOIN order_packages op2 ON op.escort_id = op2.escort_id AND op.expiration_date < op2.expiration_date';
		}

		$where = '';

		if ( $gotd ) {
			$where .= ' AND (go.status = ' . Model_Products::GOTD_STATUS_ACTIVE . ' OR go.status = ' . Model_Products::GOTD_STATUS_EXPIRED . ')';
			//$where .= ' AND (op.status = ' . Model_Billing_Packages::STATUS_ACTIVE . ' OR op.status = ' . Model_Billing_Packages::STATUS_PENDING . ')';
		}
		
		// Date
		if ( strlen($filter['filter_by']) )
		{
			if ( $filter['filter_by'] == 'activation_date' ) {
				if ( $gotd ) {
					$date_field = 'go.activation_date';
				} else {
					$date_field = 'op.date_activated';
				}
			}
			else if ( $filter['filter_by'] == 'expiration_date' )
				$date_field = 'op.expiration_date';
			else if ( $filter['filter_by'] == 'creation_date' )
				$date_field = 'go.creation_date';


			if (strlen($filter['date_from']) && strlen($filter['date_to']))
			{
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) )
				{
					$where .= self::quote(" AND DATE({$date_field}) = DATE(FROM_UNIXTIME(?))", $filter['date_from']);
				}
				else
				{
					$where .= self::quote(" AND DATE({$date_field}) >= DATE(FROM_UNIXTIME(?))", $filter['date_from']);
					$where .= self::quote(" AND DATE({$date_field}) <= DATE(FROM_UNIXTIME(?))", $filter['date_to']);
				}
			}
			else if ( strlen($filter['date_from']) ) {
				$where .= self::quote(" AND DATE({$date_field}) >= DATE(FROM_UNIXTIME(?))", $filter['date_from']);
			}
			else if ( strlen($filter['date_to']) ) {
				$where .= self::quote(" AND DATE({$date_field}) <= DATE(FROM_UNIXTIME(?))", $filter['date_to']);
			}
		}
		//---//

		if ( strlen($filter['a.id']) ) {
			$where .= self::quote('AND a.id = ?', $filter['a.id']);
		}

		if ( strlen($filter['o.sys_order_id']) ) {
			$where .= self::quote(' AND o.system_order_id = ?', $filter['o.sys_order_id']);
		}
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( $bu_user->type == 'superadmin' || $bu_user->type == 'admin') {
			if ( $filter['sales_user_id'] ) {
				$where .= self::quote(' AND o.backend_user_id = ?', $filter['sales_user_id']);
			}
		}
		else {
			if ( $bu_user->type == 'sales manager' || $bu_user->type == 'sales' || $bu_user->type == 'sales clerk' ) {
				$where .= self::quote(' AND o.backend_user_id = ?', $bu_user->id);
			}
		}

		if ( $filter['sales_user_id_real'] ) {
			$join .= ' INNER JOIN users u ON u.id = e.user_id ';
			if ( $bu_user->type == 'superadmin' ) {
				$where .= self::quote(' AND u.sales_user_id = ?', $filter['sales_user_id_real']);
			}
			else if ($bu_user->type == 'admin') {
				$where .= self::quote(' AND u.sales_user_id = ?', $filter['sales_user_id_real']);
			}
		}

		if ( strlen($filter['op.status']) ) {
			
			if ( $filter['op.status'] == self::STATUS_EXPIRED ) {
				/*$where .= ' AND (e.active_package_id IS NULL OR e.package_expiration_date IS NULL)';*/
			}
			
			$where .= self::quote(' AND op.status = ?', $filter['op.status']);
			
		}

		if ( strlen($filter['more30']) && $filter['more30'] == 1 ) {
			$where .= ' AND op2.id IS NULL AND op.expiration_date < DATE(DATE_ADD(NOW(), INTERVAL -30 DAY))';
		}

		if ( strlen($filter['active_package_id']) ) {
			if($filter['active_package_id'] == -1){
				$where .= self::quote(' AND op.package_id <> ?', Model_Packages::getZeroPackageId());
			}
			else{
				$where .= self::quote(' AND op.package_id = ?', $filter['active_package_id']);
			}
		}
		
		$sql = "
			SELECT SQL_CALC_FOUND_ROWS
				a.id AS ads_id,
				a.status,
				a.title,
				op.id AS order_package_id,
				op.period,
				op.discount,
				op.discount_fixed,
				op.surcharge,
				op.price,
				op.package_id,
				op.status AS package_status,
				UNIX_TIMESTAMP(op.date_activated) AS date_activated,
				UNIX_TIMESTAMP(op.expiration_date) AS expiration_date,
				UNIX_TIMESTAMP(op.activation_date) AS activation_date,
				p.is_default,
				p.name AS package_name,
				o.id AS order_id,
				o.system_order_id
				" . $wh . "
			FROM ads a
			INNER JOIN order_packages op ON op.ads_id = a.id
			LEFT JOIN orders o ON o.id = op.order_id
			INNER JOIN packages p ON op.package_id = p.id
			{$join}
			WHERE 1
		";

		$sql_sum = "
			SELECT SUM(a) FROM (SELECT t.amount AS a
			FROM ads a
			INNER JOIN order_packages op ON op.ads_id = a.id
			LEFT JOIN orders o ON o.id = op.order_id
			INNER JOIN packages p ON op.package_id = p.id
			{$join}
			LEFT JOIN transfers t ON t.id = go.transfer_id
			WHERE 1
		";

		$countSql = "SELECT FOUND_ROWS()";

		$sql .= $where;
		
		$sql_sum .= $where;
		$sql_sum .= ' GROUP BY t.id) AS aa';
				
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		//echo $sql;die;
		$result = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne($countSql));

		return $result;
	}
	
	public function cancel($package_id, $comment)
	{
		$this->get($package_id)->cancel($comment);

		Model_Activity::getInstance()->log('cancel_package', array('package id' => $package_id));

		/* make next pending package from after current package expires to asap */
		$ads_id = parent::getAdapter()->fetchOne('SELECT ads_id FROM order_packages WHERE id = ?', $package_id);
		parent::getAdapter()->query('
			UPDATE order_packages
			SET activation_type = ?
			WHERE ads_id = ? AND order_id IS NOT NULL AND status = ? AND activation_type = ?
		', array(self::ACTIVATE_ASAP, $ads_id, self::STATUS_PENDING, self::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES));
	}
	
	public function cancelDefaultPackages($escort_id, $comment)
	{
		parent::getAdapter()->beginTransaction();

		try {
			parent::getAdapter()->query('
				UPDATE order_packages SET status = ?, status_comment = ? WHERE escort_id = ? AND order_id IS NULL AND status = ?
			', array(
				Model_Billing_Packages::STATUS_CANCELLED,
				$comment,
				$escort_id,
				Model_Billing_Packages::STATUS_ACTIVE
			));

			parent::getAdapter()->commit();
		}
		catch (Exception $e) {
			parent::getAdapter()->rollBack();

			throw $e;
		}
	}
	
	private function dayDiff($date_start, $date_end)
	{
	    $difference = $date_end - $date_start;
        $days = round($difference / 86400); // 3600 seconds in an hour

		return $days;
	}
	
	public function upgrade($old_package, $data)
	{
		parent::getAdapter()->beginTransaction();
	
		try
		{
			parent::getAdapter()->update('order_packages', array('status' => Model_Packages::STATUS_UPGRADED), parent::quote('id = ?', $old_package->order_package_id));

			$m_orders = new Model_Billing_Orders();
			$old_order = $m_orders->get($old_package->order_id);
			if ( $old_order->status == Model_Billing_Orders::STATUS_PAID ) {
				
				$used_days = $this->dayDiff(strtotime($old_package->date_activated), mktime(0, 0, 0, date('m'), date('d'), date('Y')));
				$back_price = ($old_package->base_price / $old_package->base_period) * $old_package->period;
				
				if ( $used_days > 0 ) {
					$day_price = (double) ($old_package->base_price / $old_package->base_period);
					
					$used_price = $day_price * $used_days;
					
					$back_price = ceil(($old_package->period - $used_days) * $day_price);
				}
				
				parent::getAdapter()->update('users', array('balance' => new Zend_Db_Expr('balance + ' . $back_price)), parent::quote('id = ?', $old_package->user_id));
			}
			else {
				//die('noot paid');
				parent::getAdapter()->update('orders', array('price' => new Zend_Db_Expr('price - ' . $old_package->price)), parent::quote('id = ?', $old_order->id));
			}

			$m_packages = new Model_Packages();
			$m_products = new Model_Products();
			$new_package = $m_packages->getWithPrice($old_package->application_id, $old_package->agency_id, $old_package->gender, $data->package);
			$new_package = $new_package[0];

			$new_package_products = $new_package->getPackageProducts();
			
			foreach ($new_package_products as $k => $new_package_product)
			{
				$new_package_products[$k]['price'] = $m_products->getWithPrice($old_package->application_id, $old_package->agency_id, $old_package->gender, $new_package_product->id)->price;
			}
			
			$price_data = array(
				'old_package_price' => $old_package->price,
				'package_price' => $new_package->price,
				'default_period' => $new_package->period,
				'period' => $data->period,
				'discount' => $data->discount,
				'fix_discount' => $data->fix_discount,
				'surcharge' => $data->surcharge,
				'add_areas' => $data->add_areas_id
			);

			$new_package_opt_products = array();
			if ( count($data->opt_products) > 0 ) {
				foreach($data->opt_products as $product_id) {
					$price_data['opt_product_prices'][] = $m_products->getWithPrice($old_package->application_id, $old_package->agency_id, $old_package->gender, $product_id)->price;
					$new_package_opt_products[] = $m_products->getWithPrice($old_package->application_id, $old_package->agency_id, $old_package->gender, $product_id);
				}
			}

			$price_data = array_merge($price_data, array('opt_products' => $new_package_opt_products));
			
			$upgrade_price = $this->_calculateUpgradePrice($price_data);

			parent::getAdapter()->insert('orders', array(
				'user_id' => $old_package->user_id,
				'backend_user_id' => Zend_Auth::getInstance()->getIdentity()->id,
				'status' => Model_Billing_Orders::STATUS_PENDING,
				'activation_condition' => $data->activate_condition,
				'order_date' => new Zend_Db_Expr('NOW()'),
				'system_order_id' => $data->order_id,
				'application_id' => $old_package->application_id,
				'price' => $upgrade_price,
				'price_package' => $upgrade_price
			));
			$new_order_id = parent::getAdapter()->lastInsertId();

			$gotd_activation_date = null;
			$gotd_status = null;
			$gotd_city_id = null;
			if ( $data->gotd_activation_date ) {
				
				$gotd_activation_date = $data->gotd_activation_date;
				if ( date('d-m-Y', time()) < date('d-m-Y', $data->gotd_activation_date) ) {
					$gotd_activation_date = mktime(0, 1, 0, date('m', $data->gotd_activation_date), date('d', $data->gotd_activation_date), date('Y', $data->gotd_activation_date));
				}
				
				$gotd_activation_date = new Zend_Db_Expr("FROM_UNIXTIME({$gotd_activation_date})");
				$gotd_status = Model_Products::GOTD_STATUS_PENDING;
				
				$gotd_city_id = $data->gotd_city_id;
			}
			
			parent::getAdapter()->insert('order_packages', array(
				'order_id' => $new_order_id,
				'escort_id' => $old_package->escort_id,
				'package_id' => $new_package->id,
				'application_id' => $old_package->application_id,
				'base_period' => $new_package->period,
				'period' => $data->period,
				'status' => Model_Packages::STATUS_PENDING,
				'activation_type' => $data->activate_promo_package,
				'discount' => $data->discount,
				'discount_fixed' => $data->fix_discount,
				'discount_comment' => $data->comment,
				'surcharge' => $data->surcharge,
				'base_price' => $new_package->price,
				'price' => $upgrade_price,
				'gotd_activation_date' => $gotd_activation_date,
				'gotd_status' => $gotd_status,
				'gotd_city_id' => $gotd_city_id
			));
			$new_order_package_id = parent::getAdapter()->lastInsertId();

			if ( count($data->premium_city_spots) > 0 ) {
				foreach($data->premium_city_spots as $city) {
					parent::getAdapter()->insert('premium_escorts', array('order_package_id' => $new_order_package_id, 'city_id' => $city ));
				}
			}
			
			if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
				if ( count($data->add_areas_id) > 0 ) {
					foreach ( $data->add_areas_id as $add_area ) {
						parent::getAdapter()->insert('additional_areas', array('order_package_id' => $new_order_package_id, 'escort_id' => $old_package->escort_id, 'area_id' => $add_area));
					}
					parent::getAdapter()->update('order_packages', array('add_areas_count' => count($data->add_areas_id)), parent::getAdapter()->quoteInto('id = ?', $new_order_package_id));
				}
			}

			if ( count($new_package_products) > 0 )
			{
				foreach($new_package_products as $prod)
				{
					parent::getAdapter()->insert('order_package_products', array(
						'order_package_id' => $new_order_package_id,
						'product_id' => $prod['id'],
						'is_optional' => 0,
						'price' => $prod['price']
					));
				}
			}

			if ( count($new_package_opt_products) > 0 )
			{
				foreach($new_package_opt_products as $prod)
				{
					parent::getAdapter()->insert('order_package_products', array(
						'order_package_id' => $new_order_package_id,
						'product_id' => $prod['id'],
						'is_optional' => 1,
						'price' => $prod['price']
					));
				}
			}

			$config = Zend_Registry::get('system_config');
			parent::getAdapter()->insert('order_package_upgrades', array(
				'new_order_id' => $new_order_id,
				'old_package_id' => $old_package->package_id,
				'new_package_id' => $new_package->id,
				'upgrade_percent' => $config['billing']['packageUpgrade']['percent'],
				'old_package_price' => $old_package->price,
				'new_package_price' => $upgrade_price
			));

			parent::getAdapter()->commit();
			Zend_Registry::get('BillingHooker')->notify('package_cancelled', array($old_package->order_package_id));
			Zend_Registry::get('BillingHooker')->notify('order_created', array($new_order_id));

			//Model_Activity::getInstance()->log('upgrade_package', array('package id' => $transfer_id));

			return $new_order_package_id;
		}
		catch( Exception $e )
		{
			parent::getAdapter()->rollBack();
		}
	}

	protected function _calculateUpgradePrice($price_data)
	{
		$old_package_price = $price_data['old_package_price'];
		$price = $price_data['package_price'];
		$default_period = $price_data['default_period'];
		$period = $price_data['period'];
		$discount = $price_data['discount'];
		$fix_discount = $price_data['fix_discount'];
		$surcharge = $price_data['surcharge'];
		$opt_products_price = 0;
		
		if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
			$add_area_price = 0;
			
			$add_areas_count = count($price_data['add_areas']);
			
			$opt_products = $price_data['opt_products'];
			
			if ( count($opt_products) > 0 ) {
				foreach ($opt_products as $opt_prod_price) {
					if ( $opt_prod_price['id'] == Model_Products::ADDITIONAL_AREA ) {
						$add_area_price = $opt_prod_price['price'];
					}
				}
			}
		}

		if ( count($price_data['opt_product_prices']) > 0 ) {
			foreach ($price_data['opt_product_prices'] as $opt_prod_price) {
				$opt_products_price += $opt_prod_price;
			}
		}

		$total_price = 0;

		if ( $default_period > 0 ) {
			$day_price = ceil($price / $default_period);
		}

		//$parts = $period / $default_period;
		$total_price = ceil(($price * ($period / $default_period) + $opt_products_price));

		if ( $period == $default_period ) {
			$total_price = $price + $opt_products_price;
		}

		// discount
		if ( $discount > 0 ) {
			$total_price -= $total_price * ($discount / 100);
		}

		//fixed_discount
		if ( $fix_discount > 0 ) {
			$total_price -= $fix_discount;
		}
		//surcharge
		if ( $surcharge > 0 ) {
			$total_price += $surcharge;
		}

		$config = Zend_Registry::get('system_config');

		if ( $add_areas_count > 0 ) {
			$add_areas_count--;
			$total_price = $total_price + ($add_area_price * $add_areas_count);
		}
		
		/*$upgrade_price = ($total_price - $old_package_price) * ( $config['billing']['packageUpgrade']['percent'] / 100 );
		$upgrade_price = $total_price + $upgrade_price;*/
		$upgrade_price = $total_price;

		return round($upgrade_price, 2);
	}

	public function getByAdId($ad_id)
	{
		
		
		$sql = 'SELECT
					op.id,
					a.id AS ad_id,
					u.username,
					op.id AS order_package_id,
					op.order_id,

					op.period,
					op.discount,
					op.discount_fixed,
					op.surcharge,
					op.price,
					op.base_price,
					op.date_activated,
					op.expiration_date,
					p. NAME AS package_name,
					p.id AS package_id,
					u.id AS user_id,
					o. STATUS AS order_status,
					op. STATUS,
					o.id AS order_id
				FROM
					ads a
				INNER JOIN users u ON u.id = a.user_id
				LEFT JOIN order_packages op ON op.ads_id = a.id
				LEFT JOIN orders o ON o.id = op.order_id
				INNER JOIN packages p ON p.id = op.package_id

				WHERE op.status = ? AND a.id = ?
			';
		
		return parent::_fetchRow($sql, array(self::STATUS_ACTIVE, $ad_id));
	}

	public function updateExpirationDate($new_exp_date, $op_id, $ad_id)
	{
		// parent::getAdapter()->update('ads', array('package_expiration_date' => new Zend_Db_Expr('FROM_UNIXTIME(' . $new_exp_date . ')')), parent::getAdapter()->quoteInto('id = ?', $ad_id));
		parent::getAdapter()->update('order_packages', array('expiration_date' => new Zend_Db_Expr('FROM_UNIXTIME(' . $new_exp_date . ')')), parent::getAdapter()->quoteInto('id = ?', $op_id));
	}
	
	public function suspend($op_id, $date)
	{
		if ( ! $op_id ) return;
		
		$resp = array('status' => 'success', 'msg' => '');
		
		$db = parent::getAdapter();
		
		$package = $this->get($op_id);
		
		
		$data = array(
			'status' => self::STATUS_SUSPENDED,
			'date_suspended' => new Zend_Db_Expr('NOW()'),
			'sus_res_date' => null
		);

		if ( $date ) {
			$data['sus_res_date'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $date . ')');
		}
		
		$db->update('order_packages', $data, $db->quoteInto('id = ?', $op_id));
		
		$sr_date = array(
			'order_package_id' => $op_id,
			'suspend_date' => new Zend_Db_Expr('NOW()')
		);
		$db->insert('suspend_resume_dates', $sr_date);
		
		$days_left = $this->_getDaysLeft($package, $op_id);
		
		$db->update('order_packages', array('days_left' => $days_left), $db->quoteInto('id = ?', $op_id));

		Cubix_SyncNotifier::notify($package->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('escort_id' => $package->escort_id, 'action' => 'package suspended'));
		$st = new Cubix_EscortStatus($package->escort_id);
		$st->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
		$st->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED);
		
		$st->save();
		
		return $resp;
	}
	
	/*protected function _getDaysLeft($package, $op_id)
	{
		$db = parent::getAdapter();
		
		$package_date_activated = $package->date_activated;
		
		$sus_res_dates = $db->fetchAll('SELECT suspend_date, resume_date FROM suspend_resume_dates WHERE order_package_id = ?', $op_id);
		
		$resume_suspend_dates = array();
		
		if ( count($sus_res_dates) ) {
			
			foreach ( $sus_res_dates as $i => $sr_date ) {
				if ( $i == 0 ) {
					$resume_suspend_dates[$i] = array('resume_date' => $package_date_activated . ' 00:00:00', 'suspend_date' => $sr_date->suspend_date);
				} else {
					$resume_suspend_dates[$i] = array('resume_date' => $sus_res_dates[$i-1]->resume_date, 'suspend_date' => $sr_date->suspend_date);
				}
				
			}
		}
		
		$used_days = 0;
		foreach ( $resume_suspend_dates as $u_date ) {
			$used_days += $this->dateDiff(strtotime($u_date['resume_date']), strtotime($u_date['suspend_date']));
		}
		
		$days_left = $package->period - $used_days;
		
		return $days_left;
	}*/
	
	protected function _getDaysLeft($package, $op_id)
	{
		$db = parent::getAdapter();		
		
		$suspend_date = $db->fetchOne('SELECT UNIX_TIMESTAMP(DATE(suspend_date)) FROM suspend_resume_dates WHERE order_package_id = ? ORDER BY suspend_date DESC', $op_id);
		$expiration_date = $db->fetchOne('SELECT UNIX_TIMESTAMP(expiration_date) expiration_date FROM order_packages WHERE id = ?', $op_id);
		
		$days_left = $this->dateDiff($suspend_date, $expiration_date); //Fixing expiration_date logic
		
		return $days_left;
	}
	
	protected function _getDaysToExtend($package, $op_id)
	{
		$db = parent::getAdapter();
		
		$suspend_resume_date = $db->fetchRow('SELECT suspend_date, resume_date FROM suspend_resume_dates WHERE order_package_id = ? ORDER BY resume_date DESC LIMIT 1', array($op_id));
		
		$extend_days = $this->dateDiff(strtotime($suspend_resume_date->suspend_date), strtotime($suspend_resume_date->resume_date));
		
		if ( $extend_days >= 1 ) {
			$extend_days = floor($extend_days);
		} else {
			$extend_days = 0;
		}
		
		return $extend_days;
	}
	
	private function dateDiff($date_start, $date_end)
	{
	    $difference = $date_end - $date_start;
        $days = $difference / 86400; // 3600 seconds in an hour

		if ( $days < 0 ) $days = 0;
		
		return $days;
	}
	
	/*public function resume($op_id, $date)
	{
		if ( ! $op_id ) return;
		
		$db = parent::getAdapter();
		
		$package = $this->get($op_id);
		
		$resp = array('status' => 'success', 'msg' => '');
		
		$suspend_max_date = $db->fetchRow('SELECT MAX(suspend_date) AS max_date FROM suspend_resume_dates WHERE order_package_id = ?', array($op_id));
		
		$db->query('UPDATE suspend_resume_dates SET resume_date = NOW() WHERE suspend_date = ? AND order_package_id = ?', array($suspend_max_date->max_date, $op_id));
		
		//$resume_max_date = $db->fetchRow('SELECT MAX(resume_date) AS max_resume_date FROM suspend_resume_dates WHERE order_package_id = ?', array($op_id));
		
		$data = array(
			'status' => Model_Billing_Packages::STATUS_ACTIVE, 
			'date_suspended' => null,
			'sus_res_date' => null
		);

		if ( $date ) {
			$data['sus_res_date'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $date . ')');
		}
		
		$db->update('order_packages', $data, $db->quoteInto('id = ?', $op_id));
		
		$diff = $this->dateDiff(strtotime($suspend_max_date->max_date), time());
		
		if ( $diff >= 1 ) {
			$d = 86400 * $diff;
			$new_expiration_date = strtotime($package->expiration_date) + $d;
			
			$this->updateExpirationDate($new_expiration_date, $op_id, $package->escort_id);
			$resp['msg'] = 'New expiration date is ' . date('d M Y', $new_expiration_date);
		}
			
		Cubix_SyncNotifier::notify($package->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('escort_id' => $package->escort_id, 'action' => 'package resumed'));
		
		$st = new Cubix_EscortStatus($package->escort_id);
		$st->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED);
		$st->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
		
		$st->save();
		
		return $resp;
	}*/
	
	public function resume($op_id, $date)
	{
		if ( ! $op_id ) return;
		
		$db = parent::getAdapter();
		
		$package = $this->get($op_id);
		
		$resp = array('status' => 'success', 'msg' => '');
		
		$suspend_max_date = $db->fetchRow('SELECT MAX(suspend_date) AS max_date FROM suspend_resume_dates WHERE order_package_id = ?', array($op_id));		
		$db->query('UPDATE suspend_resume_dates SET resume_date = NOW() WHERE suspend_date = ? AND order_package_id = ?', array($suspend_max_date->max_date, $op_id));
		
		$expiration_date = $db->fetchOne('SELECT UNIX_TIMESTAMP(expiration_date) AS expiration_date FROM order_packages WHERE id = ?', array($op_id));
		$days_to_extend = $this->_getDaysToExtend($package, $op_id);		
		
		
		$new_expiration_date = $expiration_date + ($days_to_extend *24*60*60 );
		
		$this->updateExpirationDate($new_expiration_date, $op_id, $package->escort_id);
		$resp['msg'] = 'New expiration date is ' . date('d M Y', $new_expiration_date);
		
		$data = array(
			'status' => Model_Billing_Packages::STATUS_ACTIVE, 
			'date_suspended' => null,
			'sus_res_date' => null
		);

		if ( $date ) {
			$data['sus_res_date'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $date . ')');
		}
		
		$db->update('order_packages', $data, $db->quoteInto('id = ?', $op_id));
		
		$db->update('order_packages', array('days_left' => null), $db->quoteInto('id = ?', $op_id));
		
		Cubix_SyncNotifier::notify($package->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('escort_id' => $package->escort_id, 'action' => 'package resumed'));
		
		$st = new Cubix_EscortStatus($package->escort_id);
		$st->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED);
		$st->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
		
		$st->save();
		
		return $resp;
	}	
	
	
	public function getSusResData($op_id)
	{
		$db = parent::getAdapter();
		
		$data = $db->fetchAll("SELECT suspend_date, resume_date FROM suspend_resume_dates WHERE order_package_id = ?", $op_id);
		
		return $data;
	}
	
	public function hasPaidActivePackage($adv_id)
	{
		$db = parent::getAdapter();
		
		return $db->fetchOne("SELECT TRUE FROM order_packages WHERE order_id IS NOT NULL AND ads_id = ? AND status = ?", array($adv_id, self::STATUS_ACTIVE));
	}
	
	public function hasPaidActivePackageForEscort($user_id)
	{
		$escort = $this->_db->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetch();
		
		if ($escort)
		{			
			return $this->_db->fetchOne('SELECT	TRUE FROM order_packages WHERE escort_id = ? AND order_id IS NOT NULL AND status = ?', array($escort->id, self::STATUS_ACTIVE));
		}
		else
			return FALSE;
	}
	
	public function hasPaidActivePackageForAgency($user_id)
	{
		$escorts = $this->_db->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetchAll();
		
		if ($escorts)
		{
			$arr = array();
			
			foreach ($escorts as $escort)
				$arr[] = $escort->id;
			
			return $this->_db->fetchOne('SELECT	TRUE FROM order_packages WHERE escort_id IN (' . implode(',', $arr) . ') AND order_id IS NOT NULL AND status = ?', self::STATUS_ACTIVE);
		}
		else
			return FALSE;
	}
	
	public function cancelPendingGotd($gotd_id)
	{
		$this->_db->query('DELETE FROM gotd_orders WHERE id = ? AND status = 2', array($gotd_id));
	}
	
	
	//Works only on and6. there is no field add_areas_count in order_packages for other projects
	public function checkIfHasAddAreas($escort_id)
	{
		//Checing if has active package with product "Additional area (ID: 18)"
		//If has return order_package_id and count of add areas
		$result = parent::getAdapter()->fetchRow('
			SELECT op.id AS order_package_id, op.add_areas_count FROM order_packages op
			INNER JOIN order_package_products opp ON opp.order_package_id = op.id AND product_id = ?
			WHERE op.status = ? AND op.escort_id = ?
		', array(18, self::STATUS_ACTIVE, $escort_id) );

		if ( ! $result ) {
			return false;
		}
		
		return $result;
	}
	
	//Getting premium cities of paid escort and premium cities count
	//uses in escort edit for add/remove premium city functionality
	public function getPremiumCities($order_package_id)
	{
		$result = parent::db()->fetchAll('
			SELECT c.id, c.title_en AS title
			FROM order_packages op
			INNER JOIN premium_escorts pe ON pe.order_package_id = op.id
			INNER JOIN cities c ON c.id = pe.city_id
			WHERE op.id = ?
		', array($order_package_id));
		
		return $result;
	}
	
	public function changePremiumCities($escort_id, $order_package_id, $premium_cities)
	{
		try {
			parent::db()->beginTransaction();
			
			parent::db()->delete('premium_escorts', parent::quote('order_package_id = ?', $order_package_id));
			foreach($premium_cities as $city_id) {
				parent::db()->insert('premium_escorts', array('order_package_id' => $order_package_id, 'city_id' => $city_id));
				Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_PREMIUM_CITY_CHANGED, array('city_id' => $city_id));
			}
			
			parent::db()->commit();
		} catch(Exception $ex) {
			parent::db()->rollback();
			throw $ex;
		}
	}
	
	public function checkIfHasAdditionalCity($order_package_id)
	{
		$add_city_products = array(
			Model_Products::ADDITIONAL_CITY,
			Model_Products::ADDITIONAL_CITY_PREMIUM_SPOT_CITY_1,
			Model_Products::ADDITIONAL_CITY_PREMIUM_SPOT_CITY_2,
			Model_Products::ADDITIONAL_CITY_PREMIUM_SPOT_CITY_3,
		);
		return parent::db()->fetchOne('
			SELECT product_id
			FROM order_package_products
			WHERE order_package_id = ? AND product_id IN (' . implode(',', $add_city_products) . ')
		', array($order_package_id));
	}
	
	public function addEscortComment($escort_id, $comment, $sales_user_id)
	{
		parent::getAdapter()->insert('escort_comments', array(
			'escort_id' => $escort_id,
			'comment' => $comment,
			'sales_user_id' => $sales_user_id,
			'date' => new Zend_Db_Expr('NOW()')
		));
	}
	
	public function getPackageMovingHistory($order_package_id)
	{
		return parent::db()->fetchAll('
			SELECT e1.showname AS from_showname, e2.showname AS to_showname, bu.username AS admin, pmh.date
			FROM packages_moving_history pmh
			INNER JOIN escorts e1 ON e1.id = pmh.from_escort_id
			INNER JOIN escorts e2 ON e2.id = pmh.to_escort_id
			LEFT JOIN backend_users bu ON bu.id = pmh.backend_user_id
			WHERE order_package_id = ?',
		array($order_package_id));
	}
	
	public function addEdProduct($order_package_id)
	{
		parent::db()->query('
			DELETE FROM order_package_products
			WHERE order_package_id = ? AND product_id = ?
		', array($order_package_id, PRODUCT_NO_ED_LISTING));
	}
	
	public function removeEdProduct($order_package_id)
	{
		parent::db()->insert('order_package_products', array(
			'order_package_id'	=> $order_package_id,
			'product_id'	=> PRODUCT_NO_ED_LISTING,
			'is_optional'	=> 1,
			'price'	=> 0
		));
	}
}
