<?php

class Model_Billing_Transfers extends Cubix_Model
{
	protected $_table = 'transfers';
	protected $_itemClass = 'Model_Billing_TransferItem';

	const STATUS_PENDING = 1;
	const STATUS_CONFIRMED = 2;
	const STATUS_REJECTED = 3;
	const STATUS_AUTO_REJECTED = 4;
	const STATUS_CHARGEBACK = 5;

	const WEBSITE_CARDGATE = 1;
	const WEBSITE_ADSGATE = 2;
	
	public static $STATUS_LABELS = array(
		self::STATUS_PENDING => 'Pending',
		self::STATUS_CONFIRMED => 'Confirmed',
		self::STATUS_REJECTED => 'Rejected',
		self::STATUS_AUTO_REJECTED => 'Auto Rejected',
		self::STATUS_CHARGEBACK => 'Chargeback'
	);

	public function getTotalTransfersPaid()
	{
		return parent::getAdapter()->fetchOne('SELECT SUM(tc.amount) FROM transfers t INNER JOIN transfer_confirmations tc ON tc.transfer_id = t.id');
	}

	public function getTotalTransfers()
	{
		return parent::getAdapter()->fetchOne('SELECT SUM(amount) FROM transfers WHERE status <> ?', self::STATUS_REJECTED);
	}

	public function getPullPersons()
	{
		return parent::getAdapter()->fetchAll('SELECT id, CONCAT(first_name, " ", last_name) AS name FROM moneygram_pull_persons WHERE status = 1 ');
	}

	/**
	 *
	 * @param int $id
	 * @return Model_Billing_TransferItem
	 */
	public function get($id)
	{
		return parent::_fetchRow('SELECT * FROM transfers WHERE id = ?', $id);
	}

	public function getAllV2(array $data = array(), $page = 1, $per_page = 10, $sort_field = 'date_transfered', $sort_dir = 'DESC')
	{
		$special_data = array();
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		$joins = array(		
			10 => 'INNER JOIN ads a ON a.user_id = u.id',
			30 => 'INNER JOIN transfer_orders too ON too.transfer_id = t.id',
			40 => 'INNER JOIN orders o ON o.id = too.order_id',
			50 => 'INNER JOIN order_packages op ON op.order_id = o.id',
			
			60 => 'INNER JOIN transfer_orders too2 ON too2.transfer_id = t.id',
			888 => 'INNER JOIN order_packages op ON op.order_id = too2.order_id',
			999 => 'INNER JOIN escorts e ON e.id = op.escort_id',
		);
		
		$active_joins = array();
		$group_by = " GROUP BY t.id ";
		
		$where = array(
			't.is_credit_transfer = 0',
		);
				
		if ( $data['ads_id'] > 0 ) {
			$where[] = 'a.id = '. $data['ads_id'];
		}
		
		if ( strlen($data['user_id']) > 0 ) {
			$where[] = 'u.id = '. $data['user_id'];
		}
		
        if ( $data['email'] ) {
            $where[] = 'u.email LIKE "' . $data['email'] . '%"';
        }
         
		if ( $data['cc'] ) {
			$where[] = ' t.cc_number LIKE "%' . $data['cc'] . '"';
		}
                
		$data['filter_by'] = 'date_transfered';
		if ( $data['filter_by'] ) {
			if ( in_array($data['filter_by'], array('date_transfered')) ) {
				if ( $data['date_from'] ) {
					$where[] = 'DATE(' . $data['filter_by'] . ') >= ' . new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_from'] . '))'); 
				}

				if ( $data['date_to'] ) {
					$where[] = 'DATE(' . $data['filter_by'] . ') <= ' . new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_to'] . '))');
				}
			}
		}
		
		if ( $data['sales_user_id_real'] ) {
			if ( $bu_user->type == 'superadmin' ) {
				$where[] = 'u.sales_user_id = '. $data['sales_user_id_real'];
			}
			
			else if ($bu_user->type == 'admin') {
				$where[] = 'u.sales_user_id = '. $data['sales_user_id_real'];
			}
		}
		
		if ( $bu_user->type == 'superadmin' ) {
			if ( $data['sales_user_id'] ) {
				$where[] = 't.backend_user_id = ' . $data['sales_user_id'];
			}
		}
		else {
			if ( $bu_user->type == 'sales manager' || $bu_user->type == 'sales' || $bu_user->type == 'sales clerk') {
				$where[] = 't.backend_user_id = ' . $bu_user->id;
			}
			else if ($bu_user->type == 'admin') {
				if ( $data['sales_user_id'] ) {
					$where[] = 't.backend_user_id = ' . $data['sales_user_id'];
				}
			}
		}
		
		if ( $data['status'] ) {
			$where[] = 't.status = ' . $data['status'];
		}
		
		if ( $data['sys_order_id'] ) {
			$where[] = 'o.system_order_id = "' . $data['sys_order_id'] . '"';
			$active_joins[30] = $joins[30];
			$active_joins[40] = $joins[40];
		}
		
		if ( $data['contains_disc_orders'] ) {
			$where[] = '(op.discount <> 0 OR op.discount_fixed <> 0)';
			$active_joins[30] = $joins[30];
			$active_joins[40] = $joins[40];
			$active_joins[50] = $joins[50];
		}
		
		if ( $data['has_comment'] ) {
            $where[] = 't.comment IS NOT NULL AND t.comment <> \'\'';
        }
        if ( $data['contains_payment_ad_reation'] ) {
			$where[] = 't.new_ad IS NOT NULL ';
		}
		
		/* PAYMENT INFO FILTER */
		if ( $data['transfer_type_id'] ) {
			$where[] = 't.transfer_type_id = ' . $data['transfer_type_id'];
		}
				
		if ( $data['transaction_id'] ) {
			$where[] = 't.transaction_id = "' . $data['transaction_id'] . '"';
			$special_data['transaction_id'] = $data['transaction_id'];
		}
		
		if ( $data['is_self_checkout'] ) {
			$where[] = 't.is_self_checkout = ' . $data['is_self_checkout'] ;
		}
		
		if ( isset($data['phone_status']) && $data['phone_status'] != "") {
			$where[] = 't.phone_billing_paid = ' . $data['phone_status'] ;
		}
		
		if ( $data['var_symbol'] ) {
			$where[] = 't.var_symbol = ' . $data['var_symbol'];
		}
		
		if ( $data['mtcn'] ) {
			$where[] = 't.mtcn = ' . $data['mtcn'];
		}
		
		if ( $data['cc_number'] ) {
			$where[] = 't.cc_number LIKE "%' . $data['cc_number'] . '"';
			
			$special_data['special_cc_number'] = $data['cc_number'];
		}
		
		if ( $data['cc_email'] ) {
			$where[] = 't.cc_email LIKE "%' . $data['cc_email'] . '%"';
			
			$special_data['special_cc_email'] = $data['cc_email'];
		}
		
		if ( $data['first_name'] ) {
			$where[] = 't.first_name = "' . $data['first_name'] . '"';
			
			$special_data['special_first_name'] = $data['first_name'];
		}
		
		if ( $data['last_name'] ) {
			$where[] = 't.last_name = "' . $data['last_name'] . '"';
			
			$special_data['special_last_name'] = $data['last_name'];
		}
		
        if ( $data['pull_person_id'] ) {
            $where[] = 't.pull_person_id = ' . $data['pull_person_id'];
        }
        
		if ( $data['fraudulent'] ) {
			$where[] =  ' u.id IN (SELECT user_id FROM user_cookies WHERE cookie IN (SELECT cookie FROM users U LEFT JOIN user_cookies C ON U.id = C.user_id WHERE date_banned  = 29))';
		}

        /*if (in_array(Cubix_Application::getId(),array(APP_EF,APP_A6))){
            if ( $data['is_mobile'] ) {
                $where[] = 't.is_mobile = ' . $data['is_mobile'];
            }
        }*/

		/* PAYMENT INFO FILTER */
		
		/*if ( $data['showname'] || $data['escort_id'] ) {
			unset($active_joins[10]);
			$active_joins[60] = $joins[60];
			$active_joins[62] = $joins[888];
			$active_joins[63] = $joins[999];
			ksort($active_joins);
		}*/
		
		$base_sql = "
			SELECT
				SQL_CALC_FOUND_ROWS
				a.title,
				a.id as ads_id,
				u.id AS user_id, u.username,
				t.id, t.status, t.comment_grid, t.backend_user_id, t.first_name, t.last_name, if(date_banned = 29 , 'charge','clean') as chargeback,
				UNIX_TIMESTAMP(t.date_transfered) AS date_transfered, t.amount, t.transfer_type_id,
				tc.amount AS paid_amount, UNIX_TIMESTAMP(tc.date) AS payment_date, t.is_self_checkout, new_ad 
			FROM transfers t
			INNER JOIN transfer_orders tord ON tord.transfer_id = t.id
			INNER JOIN order_packages op ON op.order_id = tord.order_id
			INNER JOIN ads a ON a.id = op.ads_id
			INNER JOIN users u ON u.id = a.user_id
			LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id
			" . implode(' ', $active_joins) . "
			WHERE " . implode(' AND ', $where) . "
			" . $group_by . "
			ORDER BY " . $sort_field . " " . $sort_dir . "			
			LIMIT " . ($page - 1) * $per_page . ", " . $per_page
		;

		
		$data = parent::_fetchAll($base_sql);
		$count = $this->getAdapter()->fetchOne("SELECT FOUND_ROWS()");
		$uniq_users = 0;
		// UNIQ USERS COUNT -----------------------------
		
		/*if (Cubix_Application::getId() == APP_EF){
			$uniq_users_sql = "
				SELECT COUNT(DISTINCT u.id)
				FROM transfers t 
				INNER JOIN users u ON u.id = t.user_id 
				LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id 
				" . implode(' ', $active_joins) . " 
				WHERE " . implode(' AND ', $where) . " 
				"
			;

			$uniq_users = $this->getAdapter()->fetchOne($uniq_users_sql);
		}*/
		
		// ---------------------------------------------
		
		if ( count($data) ) {
			foreach ( $data as $i => $transfer ) {
				$data[$i]->status = self::$STATUS_LABELS[$transfer->status];
				$data[$i]->sales_username = $this->getAdapter()->fetchOne("SELECT bu.username FROM backend_users bu WHERE bu.id = ?", array($transfer->backend_user_id));
				$data[$i]->transfer_type = $this->getAdapter()->fetchOne("SELECT name FROM transfer_types WHERE id = ?", array($transfer->transfer_type_id));
				
			}
		}
		
		/* INCLUDE SPECIAL TRANSFERS */
		if ( FALSE && count($special_data) ) {
			/* $sp_sql = '
				SELECT 
					t.id, 2 AS status, CONCAT(t.first_name, " ", t.last_name) AS showname, "" AS username,
					"Credit Card" AS transfer_type, null AS comment_grid, UNIX_TIMESTAMP(t.date_transfered) AS date_transfered, 
					t.amount, bu.username AS sales_username, "escort" AS user_type, t.first_name, 
					t.last_name, UNIX_TIMESTAMP(t.date_transfered) AS payment_date, t.amount AS paid_amount, 
					0 AS is_self_checkout, 1 as is_special, is_charged_back
				FROM special_transfers t
				INNER JOIN backend_users bu ON bu.id = t.backend_user_id
				WHERE 1
			'; */

			$sp_sql = '
				SELECT 
					t.id, t.status, t.comment_grid, t.backend_user_id, t.first_name, t.last_name,
					UNIX_TIMESTAMP(t.date_transfered) AS date_transfered, t.amount, t.transfer_type_id, t.is_self_checkout
				FROM transfers t
				WHERE 1
			';
			
			if ( isset($special_data['transaction_id']) && $special_data['transaction_id'] ) {
				$sp_sql .= ' AND t.transaction_id="' . $special_data['transaction_id'] . '"';
			}
			
			if ( isset($special_data['special_first_name']) && $special_data['special_first_name'] ) {
				$sp_sql .= " AND t.first_name='" . $special_data['special_first_name'] . "'";
			}
			
			if ( isset($special_data['special_last_name']) && $special_data['special_last_name'] ) {
				$sp_sql .= " AND t.last_name='" . $special_data['special_last_name'] . "'";
			}
			
			if ( isset($special_data['special_cc_number']) && $special_data['special_cc_number'] ) {
				$sp_sql .= " AND t.cc_number LIKE '%" . $special_data['special_cc_number'] . "'";
			}
			
			if ( isset($special_data['special_cc_email']) && $special_data['special_cc_email'] ) {
				$sp_sql .= " AND t.email LIKE '%" . $special_data['special_cc_email'] . "%'";
			}
			
			$sp_data = parent::_fetchAll($sp_sql);
			
			foreach ( $sp_data as $i => $row ) {
				$sp_data[$i]->status = self::$STATUS_LABELS[$row->status];
			}
			
			$data = array_merge($data, $sp_data);
		}
		/* INCLUDE SPECIAL TRANSFERS */

		return array(
			'data' => $data,
			'count' => $count,
			'total' => 0,
			'paid' => 0,
			'dif_members_count' => $uniq_users
		);
	}
	
	public function getAll(array $filter = array(), $page = 1, $per_page = 10, $sort_field = 'date_transfered', $sort_dir = 'DESC', $special_data = array())
	{
		$joins = array();
		$show_special = false;
		$sp_tr_id = false;
		if ( isset($filter['show_special']) && $filter['show_special'] ) {
			$show_special = true;
			$sp_tr_id = $filter['show_special'];
			unset($filter['show_special']);
		}
		
		if ( isset($filter['contains_disc_orders']) && $filter['contains_disc_orders'] ) {
			$joins['joins'] = 'INNER JOIN order_packages op ON op.order_id = o.id';
			$filter['(op.discount <> 0 OR op.discount_fixed <> 0)'] = true;
			
			unset($filter['contains_disc_orders']);
		}
		
		$sql = array(
			'tables' => array('transfers t'),
			'fields' => array(
				't.id', 't.status', 'u.username', 't.user_id', 'tt.name AS transfer_type', 't.comment_grid',
				'UNIX_TIMESTAMP(date_transfered) AS date_transfered', 't.amount', 'bu.username AS sales_username', 'bu2.username AS sales_username_real',
				'u.user_type', 't.first_name', 't.last_name', 'UNIX_TIMESTAMP(tc.date) AS payment_date', 'tc.amount AS paid_amount', 'a.name AS agency_name',
				'e.showname', 'a.id AS agency_id', 'e.id AS escort_id', 't.is_self_checkout'
			),
			'joins' => array(
				'INNER JOIN users u ON u.id = t.user_id',
				'INNER JOIN backend_users bu ON bu.id = t.backend_user_id',
				'INNER JOIN backend_users bu2 ON bu2.id = u.sales_user_id',
				'INNER JOIN transfer_types tt ON tt.id = t.transfer_type_id',
				'LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id',
				'LEFT JOIN escorts e ON e.user_id = t.user_id',
				'LEFT JOIN agencies a ON a.user_id = t.user_id',
				'LEFT JOIN transfer_orders too ON too.transfer_id = t.id',
				'LEFT JOIN orders o ON o.id = too.order_id'
			) + $joins,
			'where' => array(
                
			) + $filter,
			'group' => 't.id',
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);

		$dataSql = Cubix_Model::getSql($sql);
		//die($dataSql);
		
		$sql['order'] = false;
		$sql['page'] = false;
		$sql['group'] = false;	

//        $sql['fields'] = array('COUNT(DISTINCT(t.id))');
//		$countSql = Cubix_Model::getSql($sql);


        $sql['group'] =  't.id';
    
		$sql['fields'] = array('t.amount','tc.amount AS paid_amount,t.id');
		$aggrSql = Cubix_Model::getSql($sql);
		
        $sqlTotal = array(
			'tables' => array('('.$aggrSql.') x'),
			'fields' => array(
				'SUM(x.amount) as total',
                'SUM(x.paid_amount) AS paid_total',
                'COUNT(x.id) AS count'
			),
			'joins' => array(
                
			),
			'where' => array(

			));
        $sqlTotals = Cubix_Model::getSql($sqlTotal);
		
        $totals = $this->getAdapter()->fetchRow($sqlTotals);
      
        $total = @$totals->total;
        $paid = @$totals->paid_total;
        $count = @$totals->count;

//		$count = $this->getAdapter()->fetchOne($countSql);

		$data = parent::_fetchAll($dataSql);
     
		if ($show_special || count($special_data)) {
			$sp_sql = '
				SELECT 
					t.id, 2 AS status, CONCAT(t.first_name, " ", t.last_name) AS showname, "" AS username,
					"Credit Card" AS transfer_type, null AS comment_grid, UNIX_TIMESTAMP(t.date_transfered) AS date_transfered, 
					t.amount, bu.username AS sales_username, "escort" AS user_type, t.first_name, 
					t.last_name, UNIX_TIMESTAMP(t.date_transfered) AS payment_date, t.amount AS paid_amount, 
					0 AS is_self_checkout, 1 as is_special, is_charged_back
				FROM special_transfers t
				INNER JOIN backend_users bu ON bu.id = t.backend_user_id
				WHERE 1
			';
			
			if ( $show_special ) {
				$sp_sql .= " AND t.transaction_id =" . $sp_tr_id;
			}
			
			if ( isset($special_data['special_first_name']) && $special_data['special_first_name'] ) {
				$sp_sql .= " AND t.first_name='" . $special_data['special_first_name'] . "'";
			}
			
			if ( isset($special_data['special_last_name']) && $special_data['special_last_name'] ) {
				$sp_sql .= " AND t.last_name='" . $special_data['special_last_name'] . "'";
			}
			
			if ( isset($special_data['special_cc_number']) && $special_data['special_cc_number'] ) {
				$sp_sql .= " AND t.cc_number LIKE '%" . $special_data['special_cc_number'] . "'";
			}
			
			if ( isset($special_data['special_cc_email']) && $special_data['special_cc_email'] ) {
				$sp_sql .= " AND t.email LIKE '%" . $special_data['special_cc_email'] . "%'";
			}
			
			$sp_data = parent::_fetchAll($sp_sql);
			
			$data = array_merge($data, $sp_data);
		}

		foreach ( $data as $i => $row ) {
			$data[$i]->status = self::$STATUS_LABELS[$row->status];
		}

		return array(
			'data' => $data,
			'count' => $count,
			'total' => number_format($total, 2) . ' $',
			'paid' => number_format($paid, 2) . ' $' 
		);
	}
	
	public function getAllCount(array $filter = array(), $page = 1, $per_page = 10, $sort_field = 'date_transfered', $sort_dir = 'DESC', $special_data = array())
	{
		$joins = array();
		$show_special = false;
		$sp_tr_id = false;
		if ( isset($filter['show_special']) && $filter['show_special'] ) {
			$show_special = true;
			$sp_tr_id = $filter['show_special'];
			unset($filter['show_special']);
		}
		
		if ( isset($filter['contains_disc_orders']) && $filter['contains_disc_orders'] ) {
			$joins['joins'] = 'INNER JOIN order_packages op ON op.order_id = o.id';
			$filter['(op.discount <> 0 OR op.discount_fixed <> 0)'] = true;
			
			unset($filter['contains_disc_orders']);
		}
		
		$sql = array(
			'tables' => array('transfers t'),
			'fields' => array(
				't.id', 't.status', 'u.username', 't.user_id', 'tt.name AS transfer_type', 't.comment_grid',
				'UNIX_TIMESTAMP(date_transfered) AS date_transfered', 't.amount', 'bu.username AS sales_username', 'bu2.username AS sales_username_real',
				'u.user_type', 't.first_name', 't.last_name', 'UNIX_TIMESTAMP(tc.date) AS payment_date', 'tc.amount AS paid_amount', 'a.name AS agency_name',
				'e.showname', 'a.id AS agency_id', 'e.id AS escort_id', 't.is_self_checkout'
			),
			'joins' => array(
				'INNER JOIN users u ON u.id = t.user_id',
				'INNER JOIN backend_users bu ON bu.id = t.backend_user_id',
				'INNER JOIN backend_users bu2 ON bu2.id = u.sales_user_id',
				'INNER JOIN transfer_types tt ON tt.id = t.transfer_type_id',
				'LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id',
				/*'LEFT JOIN escorts e ON e.user_id = t.user_id AND u.user_type = "escort"',*/
				'LEFT JOIN escorts e ON e.user_id = t.user_id',
				/*'LEFT JOIN agencies a ON a.user_id = t.user_id AND u.user_type = "agency"',*/
				'LEFT JOIN agencies a ON a.user_id = t.user_id',
				'LEFT JOIN transfer_orders too ON too.transfer_id = t.id',
				'LEFT JOIN orders o ON o.id = too.order_id'
			) + $joins,
			'where' => array(
                
			) + $filter,
			'group' => 't.id',
			'order' => array($sort_field, $sort_dir),
			'page' => array($page, $per_page)
		);

		$dataSql = Cubix_Model::getSql($sql);
		//die($dataSql);
		
		$sql['order'] = false;
		$sql['page'] = false;
		$sql['group'] = false;	

//        $sql['fields'] = array('COUNT(DISTINCT(t.id))');
//		$countSql = Cubix_Model::getSql($sql);


        $sql['group'] =  't.id';
    
		$sql['fields'] = array('t.amount','tc.amount AS paid_amount,t.id');
		$aggrSql = Cubix_Model::getSql($sql);
		
        $sqlTotal = array(
			'tables' => array('('.$aggrSql.') x'),
			'fields' => array(
				'SUM(x.amount) as total',
                'SUM(x.paid_amount) AS paid_total',
                'COUNT(x.id) AS count'
			),
			'joins' => array(
                
			),
			'where' => array(

			));
        $sqlTotals = Cubix_Model::getSql($sqlTotal);
		//echo $sqlTotals;die;
        $totals = $this->getAdapter()->fetchRow($sqlTotals);
      
        $total = @$totals->total;
        $paid = @$totals->paid_total;
        $count = @$totals->count;

//		$count = $this->getAdapter()->fetchOne($countSql);

		$data = parent::_fetchAll($dataSql);
     
		if ($show_special || count($special_data)) {
			$sp_sql = '
				SELECT 
					t.id, 2 AS status, CONCAT(t.first_name, " ", t.last_name) AS showname, "" AS username,
					"Credit Card" AS transfer_type, null AS comment_grid, UNIX_TIMESTAMP(t.date_transfered) AS date_transfered, 
					t.amount, bu.username AS sales_username, "escort" AS user_type, t.first_name, 
					t.last_name, UNIX_TIMESTAMP(t.date_transfered) AS payment_date, t.amount AS paid_amount, 
					0 AS is_self_checkout, 1 as is_special, is_charged_back
				FROM special_transfers t
				INNER JOIN backend_users bu ON bu.id = t.backend_user_id
				WHERE 1
			';
			
			if ( $show_special ) {
				$sp_sql .= " AND t.transaction_id =" . $sp_tr_id;
			}
			
			if ( isset($special_data['special_first_name']) && $special_data['special_first_name'] ) {
				$sp_sql .= " AND t.first_name='" . $special_data['special_first_name'] . "'";
			}
			
			if ( isset($special_data['special_last_name']) && $special_data['special_last_name'] ) {
				$sp_sql .= " AND t.last_name='" . $special_data['special_last_name'] . "'";
			}
			
			if ( isset($special_data['special_cc_number']) && $special_data['special_cc_number'] ) {
				$sp_sql .= " AND t.cc_number LIKE '%" . $special_data['special_cc_number'] . "'";
			}
			
			if ( isset($special_data['special_cc_email']) && $special_data['special_cc_email'] ) {
				$sp_sql .= " AND t.email LIKE '%" . $special_data['special_cc_email'] . "%'";
			}
			
			$sp_data = parent::_fetchAll($sp_sql);
			
			$data = array_merge($data, $sp_data);
		}
//echo $dataSql;die;
		foreach ( $data as $i => $row ) {
			$data[$i]->status = self::$STATUS_LABELS[$row->status];
		}

		return array(
			'data' => $data,
			'count' => $count,
			'total' => number_format($total, 2) . ' $' ,
			'paid' => number_format($paid, 2) . ' $'
		);
	}

	
	public function getAllCountV2(array $filter = array(), $type)
	{
//	    var_dump($filter);
//        var_dump($filter['DATE(date_transfered) >= ?']);
//        var_dump($filter['DATE(date_transfered) <= ?']);
//        die;
		$joins = array();
				
		if ( isset($filter['show_special']) && $filter['show_special'] ) {
			unset($filter['show_special']);
		}
		
		if ( isset($filter['contains_disc_orders']) && $filter['contains_disc_orders'] ) {
			$joins[11] = 'INNER JOIN order_packages op ON op.order_id = o.id';
			$filter['(op.discount <> 0 OR op.discount_fixed <> 0)'] = true;
			unset($filter['contains_disc_orders']);
		}
				
		if( isset($filter['o.system_order_id = ?']) ){
			$joins[12] = 'INNER JOIN transfer_orders too ON too.transfer_id = t.id';
			$joins[13] = 'INNER JOIN orders o ON o.id = too.order_id';	
		}
		
		if ( isset($filter['a.name LIKE ?']) ) {
			$joins[14] = 'INNER JOIN agencies a ON a.user_id = t.user_id AND u.user_type = \'agency\'';
		}
		
//		if ( isset($filter['e.showname LIKE ?']) || isset($filter['e.id = ?']) || isset($filter['e.agency_id IS NULL']) || isset($filter['e.agency_id IS NOT NULL']) ) {
//			$joins[15] = 'INNER JOIN escorts e ON e.user_id = t.user_id';
//		}
                
		if ( isset($filter['a.id = ?']) ) {
			$joins[15] = 'INNER JOIN ads a ON a.user_id = t.user_id';
		}

        if( $filter['include_special_transfers'] ){
		    $is_special = $filter['include_special_transfers'];
		    unset( $filter['include_special_transfers'] );
        }
		
		$sql = array(
			'tables' => array('transfers t'),
			'fields' => array('t.amount','tc.amount AS paid_amount'),
			'joins' => array(
				'INNER JOIN users u ON u.id = t.user_id',
				/*'INNER JOIN backend_users bu ON bu.id = t.backend_user_id',
				'INNER JOIN backend_users bu2 ON bu2.id = u.sales_user_id',
				'INNER JOIN transfer_types tt ON tt.id = t.transfer_type_id',*/
				'LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id',
			) + $joins,
			'where' => array(
                
			) + $filter,

		);
		
		$aggrSql = Cubix_Model::getSql($sql);
		
        $sqlTotal = array(
			'tables' => array('('.$aggrSql.') x'),
			'fields' => array(
				'SUM(x.amount) as total',
                'SUM(x.paid_amount) AS paid_total'
            ),
			'joins' => array(
                
			),
			'where' => array(

			));
        $sqlTotals = Cubix_Model::getSql($sqlTotal);
		//echo $sqlTotals; die;
        $totals = $this->getAdapter()->fetchRow($sqlTotals);
      
        $total = @$totals->total;

        $paid = @$totals->paid_total;


        return array(
			'total' => number_format($total, 2) . ' $' ,
			'paid' => number_format($paid, 2) . ' $'
		);
	}
	
	public function getDetails($id)
	{
		$fields = '';
		$and = '';
		
		$transfer = parent::_fetchRow('
			SELECT
				t.id, t.status, UNIX_TIMESTAMP(t.date_transfered) AS date_transfered,
				t.amount, bu.username AS sales_username, t.backend_user_id, t.transfer_type_id, t.cc_holder, t.cc_number, t.cc_exp_day, t.cc_exp_month, t.cc_ip, t.cc_address, 
				t.cc_email,	tt.name AS transfer_type,u.id AS user_id, u.username, t.status,
				tc.amount AS paid_amount, UNIX_TIMESTAMP(tc.date) AS payment_date, UNIX_TIMESTAMP(tr.date) AS reject_date,
				tr.comment AS reject_comment, tc.comment AS confirm_comment,
				t.first_name, t.last_name, t.country_id, t.city, t.var_symbol, t.transaction_id, t.pull_person_id, t.mtcn, t.comment, t.website_id,
				wat.account_type AS webmoney_account, a.status AS ads_status, a.id as ads_id
			FROM transfers t
			INNER JOIN backend_users bu ON bu.id = t.backend_user_id
			INNER JOIN users u ON u.id = t.user_id
			INNER JOIN transfer_orders tord ON tord.transfer_id = t.id
			INNER JOIN order_packages op ON op.order_id = tord.order_id
			INNER JOIN ads a ON a.id = op.ads_id
			INNER JOIN transfer_types tt ON tt.id = t.transfer_type_id
			LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id
			LEFT JOIN transfer_rejections tr ON tr.transfer_id = t.id
			LEFT JOIN webmoney_account_types wat ON wat.id = t.webmoney_account
			WHERE t.id = ?
		', array($id));

			
			
		$orders = array();

		$model = new Model_Billing_Orders();
		
		foreach ( self::getAdapter()->fetchAll('SELECT ot.order_id FROM transfer_orders ot WHERE ot.transfer_id = ?', $transfer->id) as $order ) {
			$orders[] = $model->getDetailsSimple($order->order_id);
		}
		
		
		//Getting gotds paid with this transfer
		/*$gotds = self::getAdapter()->fetchAll('
			SELECT UNIX_TIMESTAMP(go.activation_date) AS activation_date, go.status, order_package_id,
				c.' . Cubix_I18n::getTblField('title') . ' AS city_title
			FROM gotd_orders go
			INNER JOIN transfers t ON t.id = go.transfer_id
			INNER JOIN cities c ON c.id = go.city_id
			WHERE go.transfer_id = ?
		', array($id));*/
		
		/*if(Cubix_Application::getId() == APP_A6 && count($orders) == 0 && isset($gotds)){
			$first_gotd = reset($gotds);
			$order_id = self::getAdapter()->fetchOne('SELECT order_id FROM order_packages WHERE id = ?', $first_gotd->order_package_id);
			$orders[] = $model->getDetailsSimple($order_id);
		}*/
		
		//$transfer->gotds = $gotds;
		$transfer->orders = $orders;

		return $transfer;
	}

	public function delete($id)
	{
		
	}

	public function getAllByUsertId($user_id)
	{
		$sql = "SELECT t.* FROM transfers t WHERE t.user_id = ?";
		
		return self::getAdapter()->query($sql, array($user_id))->fetchAll();
	}

	public function save(Model_Billing_TransferItem $item, $return_transfer_id = false)
	{
		$edit_mode = false;
		if ( $item->id ) $edit_mode = true;
		
		$orders = $item->orders;
		unset($item->orders);
		
		if ( ! $edit_mode ) {
			$item->date_transfered = new Zend_Db_Expr('FROM_UNIXTIME(' . $item->date_transfered . ')');
		}

		self::getAdapter()->beginTransaction();
		try {
			parent::save($item);
			$transfer_id = self::getAdapter()->lastInsertId();
			
			if ( ! $transfer_id ) {
				$transfer_id = $item->id;
			}
			//return $transfer_id;
			$item->setId($transfer_id);
			
			if ( $edit_mode ) {
				if ( is_array($orders) && count($orders) > 0 ) {
					foreach ( $orders as $order_id ) {
						if ( $order_id > 0 ) {
							$item->deleteTransferOrders($order_id);
						}
					}
				}
				//die;
			}

			if ( is_array($orders) && count($orders) > 0 ) {
				foreach ( $orders as $order_id ) {
					if ( $order_id > 0 ) {
						$item->addOrder($order_id);
					}
				}
			}
			
			self::getAdapter()->commit();
			
			Zend_Registry::get('BillingHooker')->notify('transfer_created', array($transfer_id));

			$result = Model_Activity::getInstance()->log('add_transfer', array('transfer id' => $transfer_id));
			
		}
		catch (Exception $e) {
			self::getAdapter()->rollBack();
			return $e->__toString();//$e->getMessage();
		}

		if ( $return_transfer_id ) {
			return $transfer_id;
		} else {
			return true;
		}
		
	}

	public function checkTransactionId($transaction_id, $transfer_id = null)
	{
		if ( $transfer_id ) {
			return ! ((bool) self::getAdapter()->fetchOne('SELECT TRUE FROM transfers WHERE transaction_id = ? AND id <> ?', array($transaction_id, $transfer_id)));
		} else {
			return ! ((bool) self::getAdapter()->fetchOne('SELECT TRUE FROM transfers WHERE transaction_id = ?', $transaction_id));
		}
		
	}

	public function getTransferTypes()
	{
		return self::getAdapter()->fetchAll('
			SELECT * FROM transfer_types
		');
	}

	public function getWebmoneyAccounts()
	{
		return self::getAdapter()->fetchAll('
			SELECT * FROM webmoney_account_types
		');
	}

	public function setImg($image)
	{
		self::getAdapter()->insert('transfer_imgs', array('transfer_id' => $image['transfer_id'], 'hash' => $image['hash'],'ext' => $image['ext']));
		return self::getAdapter()->lastInsertId();
		
	}

	public function getImgUrl($image, $size = false)
	{
		if ( $image['hash'] ) {
			$images_model = new Cubix_Images();

			$img_datas = array(
				'catalog_id' => 'transfers',
				'hash' => $image['hash'],
				'ext' => $image['ext']
			);
			if($size){
				$img_datas['size'] = $size;
			}
			return $images_model->getUrl(new Cubix_Images_Entry($img_datas));
		}

		return NULL;
	}

	public function getTransferImgsById($id)
	{
		$results = self::getAdapter()->fetchAll('SELECT * FROM transfer_imgs WHERE transfer_id = ?', $id);
		foreach($results as &$result){
			$result->img_url = $this->getImgUrl((array)$result);
			if($result->ext != 'pdf'){
				$result->img_url_small = $this->getImgUrl((array)$result,'agency_p100');
			}
		}
		return $results;
	}
	
	/**
	 * Returns a data which needs to be exported as CSV, the task about ascii files
	 * requested by Davide
	 *
	 * @param array $filter
	 * @param type $page
	 * @param type $per_page
	 * @param type $sort_field
	 * @param type $sort_dir
	 * @return array
	 */
	public function getDataForExport(array $filter = array(), $page = 1, $per_page = 10, $sort_field = 'order_date', $sort_dir = 'DESC')
	{
		
		$fields = array(
			't.id', 'u.user_type', 'u.username', 'tt.name AS transfer_type', 't.status', 'CONCAT(t.first_name, " ", t.last_name) AS full_name',
			'GROUP_CONCAT(DISTINCT(o.system_order_id)) AS orders', 't.date_transfered', 'tc.date AS payment_date', 'tc.amount AS paid_amount',
			'bu.username AS sales_person', 'o.id AS order_id'
		);
		
		
		$sql = array(
			'tables' => array('transfers t'),
			'fields' => $fields,
			'joins' => array(
				'INNER JOIN users u ON u.id = t.user_id',
				'INNER JOIN backend_users bu ON bu.id = t.backend_user_id',
				'INNER JOIN transfer_types tt ON tt.id = t.transfer_type_id',
				'LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id',
				'LEFT JOIN transfer_orders too ON too.transfer_id = t.id',
				'LEFT JOIN orders o ON o.id = too.order_id',
				'LEFT JOIN escorts e ON e.user_id = t.user_id',
				'LEFT JOIN agencies a ON a.user_id = t.user_id'
			),
			'where' => array(
			) + $filter,
			'group' => 't.id',
			'order' => array($sort_field, $sort_dir)
		);
		
		$dataSql = Cubix_Model::getSql($sql);

		$data = parent::_fetchAll($dataSql);
		
		foreach ( $data as $i => $row ) {
			$data[$i]->status = self::$STATUS_LABELS[$row->status];
		}

		return array(
			'data' => $data
		);
	}
	
	public function zeroBalance()
	{
		return parent::db()->query('
			UPDATE users 
			SET balance = 0
		');
	}
	
	public function getCommentGrid($id)
	{
		return parent::getAdapter()->fetchOne('SELECT comment_grid FROM transfers WHERE id = ?', $id);
	}
	
	public function updateCommentGrid($data)
	{
		return parent::db()->update($this->_table, array('comment_grid' => $data['comment']), parent::quote('id = ?', $data['id']));
		
	}
	public function autoConfirm($comment, $amount, $auto_user_id)
	{
		self::getAdapter()->beginTransaction();

		try {
			self::getAdapter()->insert('transfer_confirmations', array(
				'transfer_id' => $this->getId(),
				'backend_user_id' => $auto_user_id,
				'comment' => $comment,
				'amount' => doubleval($amount),
				'date' => new Zend_Db_Expr('NOW()')
			));

			self::getAdapter()->update('transfers', array(
				'status' => Model_Billing_Transfers::STATUS_CONFIRMED
			), array(
				self::getAdapter()->quoteInto('id = ?', $this->getId())
			));

			self::getAdapter()->commit();
			Zend_Registry::get('BillingHooker')->notify('transfer_confirmed', array($this->getId()));

			Model_Activity::getInstance()->log('confirm_transfer', array('transfer id' => $this->getId()));
		}
		catch (Exception $e) {
			self::getAdapter()->rollback();

			throw $e;
		}
	}
    
    public function xadconfirm($transactionID, $amount, $auto_user_id)
    {
        self::getAdapter()->beginTransaction();

        try {
            self::getAdapter()->insert('transfer_confirmations', array(
                'transfer_id' => $transactionID,
                'backend_user_id' => $auto_user_id,
                'comment' => "",
                'amount' => doubleval($amount),
                'date' => new Zend_Db_Expr('NOW()')
            ));
            echo "aa1";
            self::getAdapter()->update('transfers', array(
                'status' => Model_Billing_Transfers::STATUS_CONFIRMED
            ), array(
                self::getAdapter()->quoteInto('id = ?', $transactionID)
            ));
            echo "aa2";
            self::getAdapter()->commit();
            Zend_Registry::get('BillingHooker')->notify('transfer_confirmed', array($transactionID));
            echo "aa3";
            Model_Activity::getInstance()->log('confirm_transfer', array('transfer id' => $transactionID));
            echo "aa4";
        }
        catch (Exception $e) {
            self::getAdapter()->rollback();
            echo aaa5;
            throw $e;
        }
    }
    
    public function getTransfer($transactionID){
        return parent::_fetchRow('SELECT * FROM transfers WHERE transaction_id = ?', $transactionID);
    }
    
     public function MarkTransferasChargeback($transactionID){
         return self::getAdapter()->update('transfers', array('status' => self::STATUS_CHARGEBACK), self::quote('transaction_id = ?', $transactionID));
        //return parent::_fetchRow('SELECT * FROM transfers WHERE transaction_id = ?', $transactionID);
    } 
    
    public function MarkTransferasNewAd($transferID){
         return self::getAdapter()->update('transfers', array('new_ad' => 1), self::quote('id = ?', $transferID));
        //return parent::_fetchRow('SELECT * FROM transfers WHERE transaction_id = ?', $transactionID);
    }

    public function findTransactionId($transaction_id){
		
		return parent::getAdapter()->fetchOne('SELECT user_id FROM transfers WHERE transaction_id = ?', array($transaction_id));
    }
}
