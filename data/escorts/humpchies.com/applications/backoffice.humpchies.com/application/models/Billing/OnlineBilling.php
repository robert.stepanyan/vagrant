<?php

class Model_Billing_OnlineBilling extends Cubix_Model {
	
	static $secret_prefixes = array(
		2 => 'A7W9DC',
		1 => 'ipnI3Inr'
	);
	
	static $site_ids = array(
		1 => 3076
	);
	
	static $backend_users = array(
		99	=> array('id' => 199, 'username' => 'online_billing_manager', 'password' => ",D3.,Y^N'>WaFN<e"),
	);
	
    const BITCOIN_MULTIPLIER = 1.25; // 25%
    const BITCOIN_MULTIPLIER_UNDER = 1.37; // 37%
    const BITCOIN_URL = "https://www.coiniverse.ch/apps/"; // 25%
    //const BITCOIN_CHECK_URL = "https://www.coiniverse.ch/check/"; // 25%
    const BITCOIN_CHECK_URL = "https://www.coinfly.ch/check/"; // 25%
    const BITCOIN_TOKEN = "z32wed81779041ebb2210d05dcbzre34"; // 25%
	const BITCOIN_PRICES = [19 => 32 , 20 => 47 , 21 => 75, 22 => 195, 19 => 32 , 20 => 47 , 21 => 75, 22 => 195, 23 => 32 , 24 => 47 , 25 => 75, 26 => 195];
    const BITCOIN_BREAKPOINT = 40;
    
	private $_shopping_cart;
	private $_callback;
	
	private $_notify_numbers = array(
		//2 => array('0040753095167'),
		//1 => array('0040753095167')
	);
	//private $_notify_numbers = array('0041765070055', '0040753095167', '0037491636334');

	private $_backend_user_id;
	private $_callback_log_id;
	
	public function __construct($callback, $check_sha1 = true) {
		
		$application_id = 99; // generated id for hump

		if ( ! isset(self::$backend_users[$application_id]) ) {
			throw new Exception('No backend user for online billing manager in this application.');
		}

		$this->_backend_user_id = self::$backend_users[$application_id]['id'];
		$this->_check_sha1 = $check_sha1;
		$host = preg_replace('/^(backend|backoffice)./', '', $_SERVER['HTTP_HOST']);
		
		$this->application = 'humpchies.com';
		
		$auth = Zend_Auth::getInstance();
		$adapter = new Model_Auth_Adapter(self::$backend_users[$application_id]['username'], self::$backend_users[$application_id]['password']);
		$result = $auth->authenticate($adapter);
		
		
		if ( ! $result->isValid() ) {
			var_dump(self::$backend_users[$application_id]['username'], self::$backend_users[$application_id]['password']);
			throw new Exception('Invalid callback user specified in configs');
		}
        
        $hash = $callback['hash']; //in bitcoint retrieves shoppingcard ID

		if($callback['payment_processor'] == 'bitcoin' || $callback['payment_processor'] == 'eth'  ){
            $shopping_cart = parent::_fetchAll('
                SELECT sc.*
                FROM shopping_cart AS sc
                WHERE sc.id = ?  AND sc.status = 0 order by id desc limit 1
                ', array( $hash));
                
            $user_id                = $shopping_cart['user_id'];
            $callback['reference']  = $shopping_cart['user_id'];
        }else{
		$user_id = intval($callback['reference']);
		
		$shopping_cart = parent::_fetchAll('
			SELECT sc.*
			FROM shopping_cart AS sc
			WHERE sc.user_id = ? AND sc.hash = ? AND sc.status = 0 order by id desc limit 1
		', array($user_id, $hash));
        
            
        }
        //var_Dump($shopping_cart);
		$this->setCallback($callback);
        
		$this->setShoppingCart($shopping_cart);
        
	}
	
	public function getShoppingCart()
	{
		return $this->_shopping_cart;
	}
	
	public function setShoppingCart($shopping_cart)
	{
		$this->_shopping_cart = $shopping_cart;
	}
	
	public function getCallback()
	{
		return $this->_callback;
	}
	
	public function setCallback($callback)
	{
		$this->_callback = $callback;
	}
	
	public function updateLogData($data)
	{
		if ( !is_array($data) ) {
			$data = array($data);
		}
		
		parent::getAdapter()->update('online_billing_log', array('data' => serialize($data)), parent::getAdapter()->quoteInto('id = ?', $this->_callback_log_id));
	}
	
	public function addCallbackLog()
	{
		
		parent::getAdapter()->insert('online_billing_log', array_merge(array(
			'transaction_time'	=> $this->_callback['transaction_time'],
			'transaction_id'	=> $this->_callback['transaction_id'],
			'reference'			=> $this->_callback['reference'],
			'status_id'			=> $this->_callback['status_id'],
			'currency'			=> $this->_callback['currency'],
			'amount'			=> $this->_callback['amount'],
			'first_name'		=> $this->_callback['first_name'],
			'last_name'			=> $this->_callback['last_name'],
			'address'			=> $this->_callback['address'],
			'zipcode'			=> $this->_callback['zipcode'],
			'city'				=> $this->_callback['city'],
			'country_code'		=> $this->_callback['country_code'],
			'email'				=> $this->_callback['email'],
			'phone_number'		=> $this->_callback['phone_number'],
			'ip_address'		=> $this->_callback['ip_address'],
			'hash'			    => $this->_callback['hash'],
			'card_name'			=> $this->_callback['card_name'],
			'card_number'		=> $this->_callback['card_number'],
			'exp_date'			=> $this->_callback['exp_date'],
			'sha1'				=> $this->_callback['sha1'],
			'log_data'			=> $this->_callback['log_data']
		), array('date' => date('Y-m-d H:i:s'))));

		$this->_callback_log_id = parent::getAdapter()->lastInsertId();
	}
	
	public function isTransactionIdExists($transaction_id, $payment_system = null)
	{
		if($payment_system){
			return parent::getAdapter()->fetchOne('SELECT TRUE FROM transfers WHERE transaction_id = ? AND payment_system = ?', array($transaction_id, $payment_system ));
		}
		else{
			return parent::getAdapter()->fetchOne('SELECT TRUE FROM transfers WHERE transaction_id = ?', array($transaction_id));
		}
	}
	
	public function notifyThroughSms($text)
	{
		$application_id = Cubix_Application::getId();
		
		$originator = $phone_from = Cubix_Application::getPhoneNumber($application_id);

		$config = Zend_Registry::get('system_config');

		if (Cubix_Application::getId() != APP_6B)
		{
			$sms_config = $config['sms'];

			$SMS_USERKEY = $sms_config['userkey'];
			$SMS_PASSWORD = $sms_config['password'];

			$sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);
			$sms->setOriginator($originator);

			$notify_numbers = $this->_notify_numbers[$application_id];
			foreach ( $notify_numbers as $phone_to ) {
				$sms->setRecipient($phone_to, md5(microtime()));
				$sms->setContent($text);
				$sms->sendSMS();
			}
		}
		else
		{
			$sms_config = $config['sms_6b'];

			$SMS_USERKEY = $sms_config['user'];
			$SMS_PASSWORD = $sms_config['password'];

			$sms = new Cubix_SMS6B($SMS_USERKEY, $SMS_PASSWORD);

			$notify_numbers = $this->_notify_numbers[$application_id];
			foreach ( $notify_numbers as $phone_to ) {
				$sms->setDestination($phone_to);
				$sms->setContent($text);
				$sms->send();
			}
		}
	}
    
    public function markChargeBack(){
        $user_id = $this->_callback['reference'];
        
        parent::db()->query('UPDATE users SET date_banned = 29 WHERE id = ?', array( $user_id));
    }
	
	public function pay($payment_type = null, $autoAuthorize = false, $packID = 0)
	{
		//echo("enter pay");
		$application_id = 99;
	
		$user_id = $this->_callback['reference'];
		
        if($payment_type == 'bitcoin' || $payment_type == 'eth'){
            $user_id = $this->_shopping_cart[0]->user_id;
        }
        
		if ( ! $this->_shopping_cart ) {
			$this->updateLogData('User with id ' . $user_id . ' doesn\'t have shopping card');
			return false;
		}

		$transfer_type_id = 3;
	
		parent::db()->beginTransaction();

        try {

			$system_order_id = $this->_getNewOrderId();
			$sales_user_id = 199;
			// <editor-fold defaultstate="collapsed" desc="Inserting order row">

			$order_data = array(
				'user_id'	=> $user_id,
				'backend_user_id'	=> $sales_user_id, //$this->_backend_user_id,
				'status' => Model_Billing_Orders::STATUS_PENDING,
				'activation_condition' => Model_Billing_Orders::CONDITION_AFTER_PAID,
				'order_date' => new Zend_Db_Expr('NOW()'),
				'payment_date'	=> new Zend_Db_Expr('NOW()'),
				'system_order_id' => $system_order_id,
				'price' => 1, // Temporary price. Will be updated after foreach
				'price_package' => 1, //Temporary price. Will be updated after foreach
				'packages_count'	=> count($this->_shopping_cart),
				'application_id' => $application_id,
				'use_balance' => 0,
				'is_self_checkout' => 1
			);
            
			parent::db()->insert('orders', $order_data);
        
			$order_id = parent::db()->lastInsertId();
            
			// <editor-fold defaultstate="collapsed" desc="Creating transfer">
			$this->_callback['exp_date'] && $exp_date = preg_split("`/`", $this->_callback['exp_date']);
		    switch($payment_type){
            case 'bitcoin':
                $transfer_array = array(
                    'user_id'           => $user_id,
                    'transfer_type_id'  => 6,
                    'date_transfered'   => new Zend_Db_Expr('NOW()'),
                    'amount'            => 1, // Temporary price. Will be updated after foreach
                    'application_id'    => $application_id,
                    'status'            => Model_Billing_Transfers::STATUS_PENDING,
                    'backend_user_id'   => $sales_user_id, //$this->_backend_user_id,
                    'first_name'        => "N/A",
                    'last_name'         => "N/A",
                    'transaction_id'    => "bitcoin:" . $this->_callback['transaction_id'],
                    'is_self_checkout'  => 1,
                    'payment_system'    => $payment_type,
                    'cc_number'         => "N/A",
                    'cc_address'        => "N/A",
                    'cc_ip'             => "N/A",
                    'cc_holder'         => "N/A",
                    'cc_exp_day'        => "0",
                    'cc_exp_month'      => "0"
                );
                break;
            case 'eth':
                $transfer_array = array(
                    'user_id'           => $user_id,
                    'transfer_type_id'  => 7,
                    'date_transfered'   => new Zend_Db_Expr('NOW()'),
                    'amount'            => 1, // Temporary price. Will be updated after foreach
                    'application_id'    => $application_id,
                    'status'            => Model_Billing_Transfers::STATUS_PENDING,
                    'backend_user_id'   => $sales_user_id, //$this->_backend_user_id,
                    'first_name'        => "N/A",
                    'last_name'         => "N/A",
                    'transaction_id'    => "eth:" . $this->_callback['transaction_id'],
                    'is_self_checkout'  => 1,
                    'payment_system'    => $payment_type,
                    'cc_number'         => "N/A",
                    'cc_address'        => "N/A",
                    'cc_ip'             => "N/A",
                    'cc_holder'         => "N/A",
                    'cc_exp_day'        => "0",
                    'cc_exp_month'      => "0"
                );
                break;
            default:
            
			    $transfer_array = array(
				    'user_id' => $user_id,
				    'transfer_type_id' => $transfer_type_id,
				    'date_transfered' => new Zend_Db_Expr('NOW()'),
				    'amount' => 1, // Temporary price. Will be updated after foreach
				    'application_id' => $application_id,
				    'status'	=> Model_Billing_Transfers::STATUS_PENDING,
				    'backend_user_id'	=> $sales_user_id, //$this->_backend_user_id,
				    'first_name'	=> $this->_callback['first_name'],
				    'last_name'	=> $this->_callback['last_name'],
				    'transaction_id' => $this->_callback['transaction_id'],
				    'is_self_checkout' => 1,
				    'payment_system' => $payment_type,
				    'cc_number'	=> $this->_callback['card_number'],
				    'cc_address'	=> $this->_callback['address'],
				    'cc_ip'			=> $this->_callback['ip_address'],
				    'cc_holder'		=> $this->_callback['card_name'],
				    'cc_exp_day'	=> (int)$exp_date[1],
				    'cc_exp_month'	=> (int)$exp_date[0]
			    );
                break;
            }

			parent::db()->insert('transfers', $transfer_array);
			$transfer_id = parent::db()->lastInsertId();
		
			parent::db()->insert('transfer_orders', array(
				'transfer_id'	=> $transfer_id,
				'order_id'		=> $order_id
			));

			//Model_Activity::getInstance()->log('add_transfer', array('transfer id' => $transfer_id));
			// </editor-fold>
			
			$m_package = new Model_Packages();
			$m_products = new Model_Products();
			
			$total_price = 0;
			$total_price_package = 0;
            //$price_multiplier = ($payment_type == 'bitcoin')? 1.25 : 1;  
            $ad_id = 0;
			foreach( $this->_shopping_cart as $order ) {
				$data = unserialize($order->data);
				$ad_id = $order->ad_id;
				$package = $m_package->getWithPrice($order->category,$order->package_id);
				
				if ( $data['optional_products'] && count($data['optional_products']) ) {
					foreach($data['optional_products'] as $k => $v) {
						$opt_product = $m_products->getWithPrice($order->category,$order->package_id);
						if ( $opt_product ) {
							$data['optional_products'][] = array('id' => $opt_product->id, 'price' => $opt_product->price);
						}
						unset($data['optional_products'][$k]);
					}
				}
				
				$period = $package[0]->period;
				if ( $data['period'] ) {
					$period = $data['period'];
				}
				
				$price_data = array(
					'package_price' => $package[0]->price,
					'default_period' => $package[0]->period,
					'period' => $period,
					'discount' => $data['discount'],
					'opt_product_prices' => array()
				);
				
				if ( count($data['optional_products']) ) {
					$price_data['opt_product_prices'] = $data['optional_products'];
				}
				
				$package_price = $this->_calculatePrice($price_data);
				$package_price_without_gotd = $this->_calculatePrice($price_data, true);

				$total_price += $package_price;
				$total_price_package += $package_price_without_gotd;
				// </editor-fold>
				
				// <editor-fold defaultstate="collapsed" desc="Inserting order packages row">
				
				//Checking if has active package. IF has set activation type to after current pacakge expires
				$previous_order_package_id = parent::db()->fetchOne('SELECT id FROM order_packages WHERE order_id IS NOT NULL AND `status` = ? AND ads_id = ?', array(Model_Billing_Packages::STATUS_ACTIVE, $order->ad_id));
				
				$activation_type = Model_Billing_Packages::ACTIVATE_ASAP;
				
				if ( $previous_order_package_id ) {
					$activation_type = Model_Billing_Packages::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES;
				} else {
					$previous_order_package_id = null;
				}
				
				$activation_date = null;
				if ( $data['activation_date'] ) {
					$previous_order_package_id = null;
					$activation_type = Model_Billing_Packages::ACTIVATE_AT_EXACT_DATE;
					$activation_date = $data['activation_date'];
				}
				
				if($order->category == '') $order->category = null;


				if(isset($data['zone_name'])){
					$zone_name = $data['zone_name'];					
				}else{
					$zone_name = null;
				}

				if(isset($data['premium_cities'])){
					$city_name = $data['premium_cities'];					
				}else{
					$city_name = null;
				}

				if($payment_type == 'bitcoin' || $payment_type == 'eth'){
				    $order_package_data = array(
					    'order_id'			        => $order_id,
					    'ads_id'			        => $order->ad_id,
					    'package_id'		        => $order->package_id,
					    'base_period'		        => $package[0]->period,
					    'period'			        => $period,
					    'activation_type'	        => $activation_type,
					    'base_price'		        => self::get_bitcoin_price($order->package_id),//$package[0]->price * $price_multiplier,
					    'price'				        => self::get_bitcoin_price($order->package_id),//$package_price * $price_multiplier,
					    'previous_order_package_id' => $previous_order_package_id,
					    'activation_date'	        => $activation_date,
					    'category'			        => $order->category,
					    'city'				        => $city_name,
					    'zone' 				        => $zone_name,
				    );
                }else{
                    $order_package_data = array(
                        'order_id'            => $order_id,
                        'ads_id'            => $order->ad_id,
                        'package_id'        => $order->package_id,
                        'base_period'        => $package[0]->period,
                        'period'            => $period,
                        'activation_type'    => $activation_type,
                        'base_price'        => $package[0]->price,
                        'price'               => $package_price,
                        'previous_order_package_id'    => $previous_order_package_id,
                        'activation_date'    => $activation_date,
                        'category'            => $order->category,
                        'city'                => $city_name,
                        'zone'                 => $zone_name,
                    );
                }

				if ($data['discount']) {
					$order_package_data['discount'] = $data['discount'];
				}
				
				parent::db()->insert('order_packages', $order_package_data);
				$order_package_id = parent::db()->lastInsertId();			
				
				parent::db()->insert('online_billing_order_history', array(
					'user_id'	=> $user_id,
					'ad_id'	=> $order->ad_id,
					'package_id'	=> $order->package_id,
					'order_id'		=> $order_id,
					'order_package_id'	=> $order_package_id,
					'transfer_id'	=> $transfer_id,
					'data'			=> $order->data,
					'package_title'	=> $package[0]->name,
					'period'		=> $period,
					'order_date'	=> new Zend_Db_Expr('NOW()'),
					'total_price'	=> self::get_bitcoin_price($order->package_id)//$package_price * $price_multiplier
				));
				// </editor-fold>
			}
			
			$secret_prefix = self::$secret_prefixes[$application_id];
			$site_id = self::$site_ids[$application_id];
			
			
			if ( ! $this->_check_sha1 ) {
				//Do not check sha1
			} 				
			else {
				//Comparing sha1
				$sha1_string = $secret_prefix . number_format($total_price, 2, '.', '') . 'EUR' . 'SC-' . $user_id;
				if ( sha1($sha1_string) != $this->_callback['sha1'] ) {
					parent::db()->rollBack();
					$this->updateLogData('Failure. Sha1 doesn\'t match. ---'. $sha1_string);
					return false;
				}
			}
			
			//Updating transfer amount and order price
            if($payment_type != 'bitcoin' && $payment_type != 'eth'){
			    parent::db()->query('UPDATE transfers SET amount = ? WHERE id = ?', array($total_price, $transfer_id));
                parent::db()->query('UPDATE orders SET price = ?, price_package = ? WHERE id = ?', array($total_price , $total_price_package , $order_id));
            }else{
                parent::db()->query('UPDATE transfers SET amount = ? WHERE id = ?', array(self::get_bitcoin_price($packID), $transfer_id));
			    parent::db()->query('UPDATE orders SET price = ?, price_package = ? WHERE id = ?', array(self::get_bitcoin_price($packID), self::get_bitcoin_price($packID), $order_id));
            }
			//Removing shopping cart NO MORE :)
			//parent::db()->query('DELETE FROM shopping_cart WHERE user_id = ?', array($user_id));
			if($payment_type == 'bitcoin' || $payment_type == 'eth'){
                parent::db()->update('shopping_cart', array('status' => 1,'transaction_id' => $transfer_id), array(  parent::db()->quoteInto('id = ?', $this->_callback['hash'])));
            
            }else{
                    
                parent::db()->update('shopping_cart', array('status' => 1,'transaction_id' => $transfer_id), array( parent::db()->quoteInto('user_id  = ?', $user_id), parent::db()->quoteInto('hash = ?', $this->_callback['hash'])));
			}
			$this->updateLogData(array('transfer_id' => $transfer_id, 'msg' => 'Payment successfully recieved'));
			parent::db()->commit();
			
			$comment = 'Auto confirmed.';
			$model_transfers = new Model_Billing_Transfers();
			$transfer = $model_transfers->getDetails($transfer_id);
            $currentAd = new Model_Advertisements();
            $ad_info = $currentAd->get($ad_id);

            if($ad_info->status =='new'){
                $model_transfers->MarkTransferasNewAd($transfer_id);
            }
            
            if($autoAuthorize && $ad_info->status !='new' ){ //LIviu Bistriceanu
                if($payment_type == 'bitcoin' || $payment_type == 'eth'){
                    $transfer->autoConfirm($comment, self::get_bitcoin_price($packID), 199); //89 katie    
                }else{
                    $transfer->autoConfirm($comment, $total_price, 199); 
                }
            }
			
            return true;
		} catch(Exception $ex) {
			parent::db()->rollBack();
			$this->updateLogData($ex->getMessage());
			$err_body = var_export($this->_shopping_cart, true);
			$err_body .= "\n\n";
			$err_body .= 'From application id '. $application_id;
			$err_body .= "\n\n";
			$err_body .= $ex->getMessage();
			Cubix_Email::send('webmaster@humpchies.com', 'payment issue', $err_body);
		}
	}
	
	public function addCredits($payment_type = null)
	{
		$user_id = intval($this->_callback['reference']);
		
		if ( ! $this->_shopping_cart ) {
			$this->updateLogData('User with id ' . $user_id . ' doesn\'t have shopping card');
			return false;
		}
		
		$transfer_type_id = 3;
		$back_user_id = 199;
		
		parent::db()->beginTransaction();
		try {
			// <editor-fold defaultstate="collapsed" desc="Creating transfer">
		    
            $user_id = $this->_shopping_cart[0]->user_id; 
            switch ($payment_type){
                case 'bitcoin':
                    $transfer_array = array(
                        'user_id' => $user_id,
                        'transfer_type_id' => 6,
                        'date_transfered' => new Zend_Db_Expr('NOW()'),
                        'amount' => $this->_callback['amount'],
                        'status'    => Model_Billing_Transfers::STATUS_CONFIRMED,
                        'backend_user_id'    => $back_user_id,
                        'first_name'    => $this->_callback['first_name'],
                        'last_name'    => $this->_callback['last_name'],
                        'transaction_id' => "bitcoin:" . $this->_callback['transaction_id'],
                        'is_self_checkout' => 1,
                        'is_credit_transfer' => 1,
                        'payment_system' => $payment_type,
                        'cc_number'    => "N/A",
                        'cc_address'    => "N/A",
                        'cc_ip'            => "N/A",
                        'cc_holder'        => "N/A",
                        'cc_exp_day'    => 0,
                        'cc_exp_month'    => 0
                    );
                break;
                case 'eth':
                    $transfer_array = array(
                        'user_id' => $user_id,
                        'transfer_type_id' => 7,
                        'date_transfered' => new Zend_Db_Expr('NOW()'),
                        'amount' => $this->_callback['amount'],
                        'status'    => Model_Billing_Transfers::STATUS_CONFIRMED,
                        'backend_user_id'    => $back_user_id,
                        'first_name'    => $this->_callback['first_name'],
                        'last_name'    => $this->_callback['last_name'],
                        'transaction_id' => "eth:" . $this->_callback['transaction_id'],
                        'is_self_checkout' => 1,
                        'is_credit_transfer' => 1,
                        'payment_system' => $payment_type,
                        'cc_number'    => "N/A",
                        'cc_address'    => "N/A",
                        'cc_ip'            => "N/A",
                        'cc_holder'        => "N/A",
                        'cc_exp_day'    => 0,
                        'cc_exp_month'    => 0
                    );
                break;
                default:
                    
                    $exp_date = @preg_split('/', $this->_callback['exp_date']);
                
			        $transfer_array = array(
				        'user_id' => $user_id,
				        'transfer_type_id' => $transfer_type_id,
				        'date_transfered' => new Zend_Db_Expr('NOW()'),
				        'amount' => $this->_callback['amount'],
				        'status'	=> Model_Billing_Transfers::STATUS_CONFIRMED,
				        'backend_user_id'	=> $back_user_id,
				        'first_name'	=> $this->_callback['first_name'],
				        'last_name'	=> $this->_callback['last_name'],
				        'transaction_id' => $this->_callback['transaction_id'],
				        'is_self_checkout' => 1,
				        'is_credit_transfer' => 1,
				        'payment_system' => $payment_type,
				        'cc_number'	=> $this->_callback['card_number'],
				        'cc_address'	=> $this->_callback['address'],
				        'cc_ip'			=> $this->_callback['ip_address'],
				        'cc_holder'		=> $this->_callback['card_name'],
				        'cc_exp_day'	=> (int)$exp_date[1],
				        'cc_exp_month'	=> (int)$exp_date[0]
			        );
			}
           
			parent::db()->insert('transfers', $transfer_array);
			$transfer_id = parent::db()->lastInsertId();
//            echo "transfer inserted: " .$transfer_id;
			$user_model = new Model_Users();
			$total_price = 0;
			
			foreach( $this->_shopping_cart as $order ) {
				$data = unserialize($order->data);
				$credits = Model_Billing_CreditTransfers::$CREDIT_OPTIONS[$data['credit']];
				if(!isset($credits)){
//                    echo "rollback 1";
					parent::db()->rollBack();
					$this->updateLogData('Failure. No such credit plan as - '. $data['credit']);
					return false;
				}
				$total_price += $order->amount;
				$credits_amount = intval(str_replace('cr_', '', $data['credit']));
				$user_model->addCredits($user_id, $credits_amount);
			}
//			echo "<br>" .$this->_callback['amount'] . "______"  . $total_price."<br>"; 
            
			if($this->_callback['amount'] != $total_price){
//                echo "rollback2";
				parent::db()->rollBack();
				$this->updateLogData('Failure. Amount dosnt match - '. $this->_callback['amount']. '--'. $total_price);
				return false;
			}
            
            if($payment_type != 'bitcoin' && $payment_type != 'eth'){
                parent::db()->update('shopping_cart', array('status' => 1), array( parent::db()->quoteInto('user_id  = ?', $user_id), parent::db()->quoteInto('hash = ?', $this->_callback['hash'])));
            }else{
			    parent::db()->update('shopping_cart', array('status' => 1), array(  parent::db()->quoteInto('id = ?', $this->_callback['hash'])));
            }
			$this->updateLogData(array('transfer_id' => $transfer_id, 'msg' => 'Payment successfully recieved'));
//            echo "commit";
			parent::db()->commit();
			
			return true;
		} catch(Exception $ex) {
			parent::db()->rollBack();
			$err_body = var_export($this->_shopping_cart, true);
			$err_body .= "\n\n";
			$err_body .= 'From humpchies';
			$err_body .= "\n\n";
			$err_body .= $ex->getMessage();
			Cubix_Email::send('webmaster@humpchies.com', 'Credit payment issue', $err_body);
           
			$this->updateLogData($ex->getMessage());
            //die($err_body);
		}
	}
	
	public function sendEmailToSales($sales_email, $email_data)
	{
		$conf = Zend_Registry::get('api_config');
		Cubix_Api_XmlRpc_Client::setApiKey(Cubix_Application::getById(Zend_Controller_Front::getInstance()->getRequest()->application_id)->api_key);
		Cubix_Api_XmlRpc_Client::setServer($conf['server']);
		Cubix_Email::sendTemplate('self_checkout', $sales_email, $email_data);
	}

	
	private function _calculatePrice($price_data, $without_gotd = false)
	{
		$price = $price_data['package_price'];
		$default_period = $price_data['default_period'];
		$period = $price_data['period'];
		$discount = $price_data['discount'];
		$fix_discount = $price_data['fix_discount'];
		$surcharge = $price_data['surcharge'];
		$opt_products_price = 0;
		$gotds_count = $price_data['gotds_count'];
		$gotd_price = $price_data['gotd_price'];

		$de_days = $price_data['de_days'];
		$add_areas_count = count($price_data['add_areas']);
		
		$add_area_price = 0;
		$dex_price = 0;
		if ( count($price_data['opt_product_prices']) > 0 ) {
			foreach ($price_data['opt_product_prices'] as $opt_prod_price) {
				
				if ( $opt_prod_price['id'] == Model_Products::ADDITIONAL_AREA ) {
					$add_area_price = $opt_prod_price['price'];
				}
				
				if ( $opt_prod_price['id'] == Model_Products::DAY_EXTEND ) {
					$dex_price = $opt_prod_price['price'] * $de_days;
				}
				else {
					if ( $default_period == $period ) {
						$opt_products_price += $opt_prod_price['price'];
						
					} else {
						$opt_products_price += (float) $opt_prod_price['price'] / $default_period;
					}
				}
				
			}
			if ( $default_period != $period ) {
				$opt_products_price = ceil((float) $opt_products_price * $period);
			}
		}
		$opt_products_price = $opt_products_price + $dex_price;
		
		$total_price = 0;

		if ( $default_period > 0 ) {
			$day_price = ceil($price / $default_period);
		}

		//$parts = $period / $default_period;
		$total_price = ceil(($price * ($period / $default_period) + $opt_products_price));

		if ( $period == $default_period ) {
			$total_price = $price + $opt_products_price;
		}

		// discount
		if ( $discount > 0 ) {
			$total_price -= $total_price * ($discount / 100);
		}

		//fixed_discount
		if ( $fix_discount > 0 ) {
			$total_price -= $fix_discount;
		}
		//surcharge
		if ( $surcharge > 0 ) {
			$total_price += $surcharge;
		}
		
		if ( $add_areas_count > 0 ) {
			$add_areas_count--;
			$total_price = $total_price + ($add_area_price * $add_areas_count);
		}

		if ( !$without_gotd && $gotds_count > 0 ) {
			$total_price = $total_price + ($gotds_count * $gotd_price);
		}

		return round($total_price, 2);
	}
	
	private function _generateOrderId()
	{
		$order_id = md5(microtime() * microtime() + microtime());
		$order_id = substr(strtoupper($order_id), 0, 8);

		return $order_id;
	}

	private function _getNewOrderId()
	{
		$m_orders = new Model_Billing_Orders();
		
		$order_id = $this->_generateOrderId();
		while( $m_orders->isOrderIdExist($order_id) ) {
			$order_id = $this->_generateOrderId();
		}

		return $order_id;
	}
	
	public function fixShit()
	{
		$bk_order = 'order_backup';
		$count = parent::db()->fetchOne('SELECT count(id) FROM orders_old');
		$chank_count = 1000;
		
		$chanks = ceil($count / $chank_count);
		
		for($i = 1; $i <= $chanks; $i++)
		{
			$rows = parent::db()->fetchAll('SELECT id, price FROM orders LIMIT ?, ?', array(($chank - 1) * $chank_count, $chank_count) );
			foreach($rows as $row) {
				parent::db()->update('order', array('price' => $row->price), parent::db()->quoteInto('id = ?', $row['id']));
			}
		}
	}
	
	public static function getIvrStoredCallbacks()
	{
		$tokens = parent::db()->fetchAll('SELECT * FROM ivr_callbacks WHERE status = 0');

		return $tokens;
	}
	
	public static function setIvrCallbackStatus($id, $status)
	{
		parent::db()->update('ivr_callbacks', array('status' => $status), parent::db()->quoteInto('id = ?', $id));

		return true;
	}
	
	public static function getPaymentGatewayTokens($type = 'epg')
	{
		$tokens = parent::db()->fetchAll('SELECT id, user_id, token, attempt FROM payment_gateway_tokens WHERE attempt < 30 AND type = ? ORDER BY id DESC', array($type));

		return $tokens;
	}
	
	public static function removePaymentGatewayToken($id)
	{
		parent::db()->delete('payment_gateway_tokens', parent::db()->quoteInto('id = ?', $id));
	}
	
	public static function updatePaymentGatewayTokenAttempt($id)
	{
		parent::db()->query('UPDATE payment_gateway_tokens SET attempt = attempt + 1 WHERE id = ?', array($id));
	}
	
	public function confirmGotd()
	{
		
	}
	
	public static function getTransfer($transaction_id)
	{
		return parent::db()->fetchRow('SELECT * FROM transfers WHERE transaction_id = ?', array($transaction_id));
	}
	
	public static function insertCallbackLog($callback)
	{
		parent::db()->insert('online_billing_log', array_merge($callback, array('date' => date('Y-m-d H:i:s'))));
		
	} 

	public function autoConfirm($comment, $amount, $auto_user_id)
	{
		self::getAdapter()->beginTransaction();

		try {
			self::getAdapter()->insert('transfer_confirmations', array(
				'transfer_id' => $this->getId(),
				'backend_user_id' => $auto_user_id,
				'comment' => $comment,
				'amount' => doubleval($amount),
				'date' => new Zend_Db_Expr('NOW()')
			));

			self::getAdapter()->update('transfers', array(
				'status' => Model_Billing_Transfers::STATUS_CONFIRMED
			), array(
				self::getAdapter()->quoteInto('id = ?', $this->getId())
			));

			self::getAdapter()->commit();
			Zend_Registry::get('BillingHooker')->notify('transfer_confirmed', array($this->getId()));

			Model_Activity::getInstance()->log('confirm_transfer', array('transfer id' => $this->getId()));
		}
		catch (Exception $e) {
			self::getAdapter()->rollback();

			throw $e;
		}
	}
    
    
    public static function get_bitcoin_price($pack_id){
        return self::BITCOIN_PRICES[$pack_id];    
    }
    
    public static function credit_price($price){
        if($price < self::BITCOIN_BREAKPOINT){
            return ceil($price * self::BITCOIN_MULTIPLIER_UNDER);
        }else{
            return ceil($price * self::BITCOIN_MULTIPLIER);    
        }
    }
}

