<?php

class Model_Billing_OrderItem extends Cubix_Model_Item
{
	public function getPackages()
	{
		$sql = "
			SELECT
				op.*
			FROM order_packages op
			INNER JOIN orders o ON o.id = op.order_id
			
			WHERE o.id = ?
		";

		return parent::getAdapter()->query($sql, array($this->getId()))->fetchAll();
	}

	/**
	 *
	 * @param string $comment
	 * @return array Escort ids which packages have been canceled
	 */
	public function close($comment = '')
	{
		$model = new Model_Billing_Packages();

		$escorts = array();
		try {
			self::getAdapter()->beginTransaction();

			foreach ( $this->getPackages() as $package ) {
				$package = $model->get($package->id);
				$package->cancel($comment);
				$escorts[] = $package->escort_id;
			}

			self::getAdapter()->update('orders', array(
				'status' => Model_Billing_Orders::STATUS_CLOSED
			), self::getAdapter()->quoteInto('id = ?', $this->getId()));

			self::getAdapter()->commit();
			Zend_Registry::get('BillingHooker')->notify('order_closed', array($this->getId()));
		}
		catch ( Exception $e ) {
			self::getAdapter()->rollBack();

			throw $e;
		}
		
		return $escorts;
	}
}
