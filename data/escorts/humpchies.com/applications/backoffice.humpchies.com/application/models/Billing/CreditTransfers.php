<?php

class Model_Billing_CreditTransfers extends Cubix_Model
{
	protected $_table = 'transfers';
	
	     public static $CREDIT_OPTIONS = array(
        'cr_20' => 20,
        'cr_50' => 48,
        'cr_100' => 90,
        'cr_200' => 180,
        'cr_500' => 400,
        'cr_25' => 20,
        'cr_58' => 48,
        'cr_110' => 90,
        'cr_225' => 180,
        'cr_600' => 400
    );
  
    /* public static $CREDIT_OPTIONS = array(
        'cr_25' => 20,
        'cr_58' => 48,
        'cr_110' => 90,
        'cr_225' => 180,
        'cr_600' => 400
    );
    */
	
	public function getAll(array $data = array(), $page = 1, $per_page = 10, $sort_field = 'date_transfered', $sort_dir = 'DESC')
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();
				
		$group_by = " GROUP BY t.id ";
		
		$where = array(
			't.is_credit_transfer = 1 ',
		);
				
		if ( strlen($data['user_id']) > 0 ) {
			$where[] = 'u.id = '. $data['user_id'];
		}
		
		if ( $data['email'] ) {
			$where[] = 'u.email LIKE "' . $data['email'] . '%"';
		}
                
		$data['filter_by'] = 'date_transfered';
		if ( $data['filter_by'] ) {
			if ( in_array($data['filter_by'], array('date_transfered')) ) {
				if ( $data['date_from'] ) {
					$where[] = 'DATE(' . $data['filter_by'] . ') >= ' . new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_from'] . '))'); 
				}

				if ( $data['date_to'] ) {
					$where[] = 'DATE(' . $data['filter_by'] . ') <= ' . new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_to'] . '))');
				}
			}
		}
		
		if ( $data['sales_user_id_real'] ) {
			if ( $bu_user->type == 'superadmin' ) {
				$where[] = 'u.sales_user_id = '. $data['sales_user_id_real'];
			}
			
			else if ($bu_user->type == 'admin') {
				$where[] = 'u.sales_user_id = '. $data['sales_user_id_real'];
			}
		}
		
		if ( $bu_user->type == 'superadmin' ) {
			if ( $data['sales_user_id'] ) {
				$where[] = 't.backend_user_id = ' . $data['sales_user_id'];
			}
		}
		else {
			if ( $bu_user->type == 'sales manager' || $bu_user->type == 'sales' || $bu_user->type == 'sales clerk') {
				$where[] = 't.backend_user_id = ' . $bu_user->id;
			}
			else if ($bu_user->type == 'admin') {
				if ( $data['sales_user_id'] ) {
					$where[] = 't.backend_user_id = ' . $data['sales_user_id'];
				}
			}
		}
		
		if ( $data['status'] ) {
			$where[] = 't.status = ' . $data['status'];
		}
		
		if ( $data['has_comment'] ) {
			$where[] = 't.comment IS NOT NULL AND t.comment <> \'\'';
		}
		
		/* PAYMENT INFO FILTER */
		if ( $data['transfer_type_id'] ) {
			$where[] = 't.transfer_type_id = ' . $data['transfer_type_id'];
		}
				
		if ( $data['transaction_id'] ) {
			$where[] = 't.transaction_id = "' . $data['transaction_id'] . '"';
		}
		
		if ( $data['is_self_checkout'] ) {
			$where[] = 't.is_self_checkout = ' . $data['is_self_checkout'] ;
		}
		
		if ( isset($data['phone_status']) && $data['phone_status'] != "") {
			$where[] = 't.phone_billing_paid = ' . $data['phone_status'] ;
		}
		
		if ( $data['var_symbol'] ) {
			$where[] = 't.var_symbol = ' . $data['var_symbol'];
		}
		
		if ( $data['mtcn'] ) {
			$where[] = 't.mtcn = ' . $data['mtcn'];
		}
		
		if ( $data['cc_number'] ) {
			$where[] = 't.cc_number LIKE "%' . $data['cc_number'] . '"';
		}
		
		if ( $data['cc_email'] ) {
			$where[] = 't.cc_email LIKE "%' . $data['cc_email'] . '%"';
		}
		
		if ( $data['first_name'] ) {
			$where[] = 't.first_name = "' . $data['first_name'] . '"';
		}
		
		if ( $data['last_name'] ) {
			$where[] = 't.last_name = "' . $data['last_name'] . '"';
		}
		
        if ( $data['pull_person_id'] ) {
            $where[] = 't.pull_person_id = ' . $data['pull_person_id'];
        }
		if ( $data['contains_payment_ad_reation'] ) {
			$where[] = 't.new_ad = 1';
		}
        
        if ( $data['fraudulent'] ) {
            $where[] =  ' u.id IN (SELECT user_id FROM user_cookies WHERE cookie IN (SELECT cookie FROM users U LEFT JOIN user_cookies C ON U.id = C.user_id WHERE date_banned  = 29))';
        }

        $base_sql = "
			SELECT
				SQL_CALC_FOUND_ROWS
				u.id AS user_id, u.username,
				t.id, t.status, t.comment_grid, t.backend_user_id, t.first_name, t.last_name, if(date_banned = 29 , 'charge','clean') as chargeback,
				UNIX_TIMESTAMP(t.date_transfered) AS date_transfered, t.amount, t.transfer_type_id,
				tc.amount AS paid_amount, UNIX_TIMESTAMP(tc.date) AS payment_date, t.is_self_checkout, t.new_ad
			FROM transfers t
			INNER JOIN users u ON u.id = t.user_id
			LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id
			WHERE " . implode(' AND ', $where) . "
			" . $group_by . "
			ORDER BY " . $sort_field . " " . $sort_dir . "			
			LIMIT " . ($page - 1) * $per_page . ", " . $per_page
		;

		$data = parent::_fetchAll($base_sql);
		$count = $this->getAdapter()->fetchOne("SELECT FOUND_ROWS()");
				
		if ( count($data) ) {
			foreach ( $data as $i => $transfer ) {
				$data[$i]->status = Model_Billing_Transfers::$STATUS_LABELS[$transfer->status];
				$data[$i]->sales_username = $this->getAdapter()->fetchOne("SELECT bu.username FROM backend_users bu WHERE bu.id = ?", array($transfer->backend_user_id));
				$data[$i]->transfer_type = $this->getAdapter()->fetchOne("SELECT name FROM transfer_types WHERE id = ?", array($transfer->transfer_type_id));
				
			}
		}
		
		
		return array(
			'data' => $data,
			'count' => $count,
			'total' => 0,
			'paid' => 0,
			
		);
	}
	
		
	public function getDetails($id)
	{
		$transfer = parent::_fetchRow('
			SELECT
				t.id, t.status, UNIX_TIMESTAMP(t.date_transfered) AS date_transfered,
				t.amount, bu.username AS sales_username, t.backend_user_id, t.transfer_type_id, t.cc_holder, t.cc_number, t.cc_exp_day, t.cc_exp_month, t.cc_ip, t.cc_address, 
				t.cc_email,	tt.name AS transfer_type,u.id AS user_id, u.username, t.status,
				tc.amount AS paid_amount, UNIX_TIMESTAMP(tc.date) AS payment_date, UNIX_TIMESTAMP(tr.date) AS reject_date,
				tr.comment AS reject_comment, tc.comment AS confirm_comment,
				t.first_name, t.last_name, t.country_id, t.city, t.var_symbol, t.transaction_id, t.pull_person_id, t.mtcn, t.comment, t.website_id
			FROM transfers t
			INNER JOIN backend_users bu ON bu.id = t.backend_user_id
			INNER JOIN users u ON u.id = t.user_id
			INNER JOIN transfer_types tt ON tt.id = t.transfer_type_id 
			LEFT JOIN transfer_confirmations tc ON tc.transfer_id = t.id
			LEFT JOIN transfer_rejections tr ON tr.transfer_id = t.id
			WHERE t.id = ?
		', array($id));

		return $transfer;
	}

	

	public function save(Model_Billing_TransferItem $item, $return_transfer_id = false)
	{
		$edit_mode = false;
		if ( $item->id ) $edit_mode = true;
		
				
		if ( ! $edit_mode ) {
			$item->date_transfered = new Zend_Db_Expr('FROM_UNIXTIME(' . $item->date_transfered . ')');
		}

		self::getAdapter()->beginTransaction();
		try {
			
			$credits = $item->credits;
			unset($item->credits);
			
			parent::save($item);
			$transfer_id = self::getAdapter()->lastInsertId();
			
			if ( ! $transfer_id ) {
				$transfer_id = $item->id;
			}
			
			//$item->setId($transfer_id);
			$user_model = new Model_Users();
			$user_model->addCredits($item->user_id, $credits);
			
			self::getAdapter()->commit();
			
			$result = Model_Activity::getInstance()->log('add_transfer', array('transfer id' => $transfer_id));
			
		}
		catch (Exception $e) {
			self::getAdapter()->rollBack();
			return $e->__toString();//$e->getMessage();
		}

		if ( $return_transfer_id ) {
			return $transfer_id;
		} else {
			return true;
		}
		
	}
	
	public function reject($transfer, $comment)
	{
		
		self::getAdapter()->beginTransaction();

		$user_model = new Model_Users();

		try {
			self::getAdapter()->update('transfers', array(
				'status' => Model_Billing_Transfers::STATUS_REJECTED
			), array(
				self::getAdapter()->quoteInto('id = ?', $transfer->id)
			));
			
			self::getAdapter()->insert('transfer_rejections', array(
				'transfer_id' => $transfer->id,
				'date' => new Zend_Db_Expr('NOW()'),
				'comment' => $comment,
				'backend_user_id' => Zend_Auth::getInstance()->getIdentity()->id
			));

			$credit_key = array_search($transfer->amount, self::$CREDIT_OPTIONS);
			$credits_amount = intval(str_replace('cr_', '', $credit_key));
			$user_model->subCredits($transfer->user_id, $credits_amount);
			self::getAdapter()->commit();

			Model_Activity::getInstance()->log('reject_transfer', array('transfer id' => $transfer->id));
		}
		catch (Exception $e) {
			self::getAdapter()->rollback();

			throw $e;
		}
	}
	
	public function chargeback($transfer, $comment)
	{
		
		self::getAdapter()->beginTransaction();

		$user_model = new Model_Users();

		try {
			self::getAdapter()->update('transfers', array(
				'status' => Model_Billing_Transfers::STATUS_CHARGEBACK
			), array(
				self::getAdapter()->quoteInto('id = ?', $transfer->id)
			));
			
			self::getAdapter()->insert('transfer_rejections', array(
				'transfer_id' => $transfer->id,
				'date' => new Zend_Db_Expr('NOW()'),
				'comment' => $comment,
				'backend_user_id' => Zend_Auth::getInstance()->getIdentity()->id
			));

			$credit_key = array_search($transfer->amount, self::$CREDIT_OPTIONS);
			$credits_amount = intval(str_replace('cr_', '', $credit_key));
			$user_model->subCredits($transfer->user_id, $credits_amount);
			self::getAdapter()->commit();

			Model_Activity::getInstance()->log('reject_transfer', array('transfer id' => $transfer->id));
		}
		catch (Exception $e) {
			self::getAdapter()->rollback();

			throw $e;
		}
	}
	
	public function confirm($transfer, $comment, $amount)
	{
		self::getAdapter()->beginTransaction();

		$user_model = new Model_Users();
		
		try {
			self::getAdapter()->insert('transfer_confirmations', array(
				'transfer_id' => $transfer->id,
				'backend_user_id' => Zend_Auth::getInstance()->getIdentity()->id,
				'comment' => $comment,
				'amount' => doubleval($amount),
				'date' => new Zend_Db_Expr('NOW()')
			));

			self::getAdapter()->update('transfers', array(
				'status' => Model_Billing_Transfers::STATUS_CONFIRMED
			), array(
				self::getAdapter()->quoteInto('id = ?', $transfer->id)
			));
			
			$user_model->addCredits($transfer->user_id, $transfer->amount);
			self::getAdapter()->commit();
			
			Model_Activity::getInstance()->log('confirm_transfer', array('transfer id' => $transfer->id));
		}
		catch (Exception $e) {
			self::getAdapter()->rollback();

			throw $e;
		}
	}

}
