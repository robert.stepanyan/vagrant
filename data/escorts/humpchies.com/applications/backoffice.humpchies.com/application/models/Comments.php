<?php
class Model_Comments extends Cubix_Model
{
	protected $_table = 'comments';

	// comments status

    const STATUS_PENDING = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 2;

	const SITEADMIN_USERNAME = 'siteadmin';

	public static $STATUSES = array(
	    self::STATUS_PENDING => 'Not Approved',
	    self::STATUS_ACTIVE => 'Active',
	    self::STATUS_DISABLED => 'Disabled',
    );

    const DISABLE_TYPE_INTERNAL = 1;
    const DISABLE_TYPE_EXTERNAL_BY_E_OR_M = 2;
    const DISABLE_TYPE_EXTERNAL_BY_ADMIN = 3;

    public static $DISABLE_TYPES = array(
        self::DISABLE_TYPE_INTERNAL=>'Internal',
        self::DISABLE_TYPE_EXTERNAL_BY_E_OR_M=>'External By E. or M',
        self::DISABLE_TYPE_EXTERNAL_BY_ADMIN=>'External By Admin',
    );




	public function getById($id)
	{
		$sql = '
			SELECT
				c.id, u.username as member_name, u.id AS user_id, a.title, a.id AS ad_id, a.user_id as ad_user_id, UNIX_TIMESTAMP(c.date) AS time, c.status, c.comment as message,
			       c.disable_type, c.disabled_reason
			FROM comments c
			INNER JOIN users u ON c.from_user = u.id
			INNER JOIN ads as a ON a.id = c.ad_id
			WHERE c.id = ?
		';

		return parent::_fetchRow($sql, array($id));
	}
	
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				c.id,
			    u.username, 
				a.title,
				a.id AS ad_id,
				UNIX_TIMESTAMP(c.date) AS time,
				c.status,
				c.comment,
				IF(u.date_activated >= DATE_ADD(NOW(), INTERVAL -72 HOUR), 1, 0) AS new_member,
				u.id AS user_id,
				u.email,
				u.type
			FROM comments c
			INNER JOIN ads a ON a.id = c.ad_id
			INNER JOIN users u ON u.id = c.from_user
			WHERE 1
		';
		
		$countSql = '
			SELECT FOUND_ROWS()
		';
		
		$where = '';


		if ( strlen($filter['username']) ) {
			$where .= self::quote(' AND u.username LIKE ?', $filter['username'] . '%');
		}
		if ( intval($filter['from_user']) ) {
			$where .= self::quote(' AND u.id = ?', intval($filter['from_user']));
		}
		if ( intval($filter['user_id']) ) {
			$where .= self::quote(' AND c.user_id = ?', intval($filter['user_id']));
		}
				
		if ( $filter['escort_id_f'] ) {
			$where .= self::quote(' AND a.id = ?', $filter['escort_id_f']);
		}
		
		if ( is_numeric($filter['status']) ) {
			if( $filter['status'] == -99 ){
				$where .= ' AND c.has_bl = 1 ';
			}
			else{
				$where .= self::quote('AND c.status = ?', $filter['status']);
			}
		}

		if ( $filter['is_active_ad'] ) {
			$where .= ' AND a.status = '.Model_Advertisements::STATUS_ACTIVE;
		}
		
		if ( isset($filter['comment']) && $filter['comment']  ) {
			$where .= ' AND  c.comment LIKE "%'.$filter['comment'].'%" ';
		}
		
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote( " AND DATE(c.time) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
				}
			}
		if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE(c.time) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}

		if ( $filter['date_to'] ) {
			$where .= self::quote(" AND DATE(c.time) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}
		
		if ( isset($filter['disable_type']) && $filter['disable_type']  ) {
			$where .= ' AND c.status = ' . self::STATUS_DISABLED . ' AND c.disable_type = ' . $filter['disable_type'];
		}
			
		$sql .= $where;
		//$countSql .= $where;
		
		$sql .= '
			GROUP BY c.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$data = parent::_fetchAll($sql);

		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return $data;
	}	
	
	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$fields = array(
				'status' => $data['status'],
				'comment' => $data['comment']
			);				

			parent::getAdapter()->update($this->_table, $fields, parent::quote('id = ?', $data['id']));
		}
	}
	
	public function remove($id)
	{
		parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));
	}

	public function getUserCommentsCount($user_id)
	{
		return parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM comments WHERE user_id = ?', array($user_id));
	}

	public function getMemberCommentsCount($user_id)
	{
		return parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM comments WHERE from_user = ?', array($user_id));
	}

	public function getUserIdByUsername($username)
	{
		return parent::getAdapter()->fetchOne('SELECT id FROM users WHERE username LIKE ?',array($username));
	}

	public function fakeByPhone($user_id)
	{
		$db = parent::getAdapter();

		$code_count = $db->fetchOne('SELECT COUNT(c.id) FROM comments c INNER JOIN ads a ON c.ad_id = a.id WHERE MID(a.phone, 1, 3) = ? AND c.user_id = ?', array('007', $user_id));
		$all_count = $db->fetchOne('SELECT COUNT(id) FROM comments WHERE user_id = ?', $user_id);

		if ($all_count > 1 && $code_count == $all_count)
			return true;
		else
			return false;
	}
	
	public function disabledReason($data)
	{
		parent::getAdapter()->update($this->_table, array(
			'disabled_reason' => $data['reason'],
			'disable_type' => $data['disable_type'],
			'status' => self::STATUS_DISABLED
		), parent::quote('id = ?', $data['id']));
	}
    
    public function getActiveComments($user_id) {
        
        $db = parent::getAdapter();
        return parent::_fetchAll('SELECT * FROM comments WHERE status = 1 AND user_id = ' . $user_id);    
    }
}
