<?php

class Model_Countries extends Cubix_Model {

	protected $_table = 'countries';

	public function getCountries($exclude = array()) {
		$where = '';
		if (!empty($exclude)) {
			$where = implode(',', $exclude);
			$where = ' WHERE id NOT IN (' . $where . ')';
		}
		$sql = '
			SELECT c.id,
				c.iso,
				c.slug,
				c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM countries c
			' . $where . '
				ORDER BY title
		';

		return parent::_fetchAll($sql);
	}
	
	public function getCountriesForBl($exclude = array()) {
		$where = '';
		if (!empty($exclude)) {
			$where = implode(',', $exclude);
			$where = ' WHERE id NOT IN (' . $where . ')';
		}
		$sql = '
			SELECT c.id,
				c.iso,
				c.slug,
				c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM countries_all c
			' . $where . '
				ORDER BY title
		';

		return parent::_fetchAll($sql);
	}
	
	public function getBlockCountries( $escort_id ){
		
		$coutry_table = Cubix_Application::getId() != APP_BL ? "countries" : "countries_all";
		$result =  $this->_db->fetchAll('
			SELECT  c.id as country_id,
				c.iso,
				c.slug,
				c.' . Cubix_I18n::getTblField('title') . ' as title
				FROM escort_blocked_countries as ebc
				INNER JOIN '.$coutry_table.' c ON c.id = ebc.country_id
				WHERE ebc.escort_id = ?
		',  array($escort_id));


		return $result;
	}
	public function getPhonePrefixs(){
		$result =  $this->_db->fetchAll('
			SELECT id , phone_prefix,ndd_prefix,title_en FROM countries_phone_code WHERE phone_prefix is not NULL order by phone_prefix DESC
			');
		return $result;
	}
	public function getPhoneCountries() {
		$sql = '
			SELECT cpc.id,
				cpc.' . Cubix_I18n::getTblField('title') . ' as title,
				cpc.phone_prefix,
				cpc.ndd_prefix
			FROM countries_phone_code cpc
		';
		return parent::_fetchAll($sql);
	}
	
	public static function getContinentCountries() 
	{
		$db = Zend_Registry::get('db');
		
		$c_sql = '
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM continents c
			ORDER BY c.ordering
		';

		$continents = $db->fetchAll($c_sql);
		
		if ( count($continents) > 0 ) {
			foreach( $continents as $k => $continent ) {
				$sql = '
					SELECT c.id,
						c.iso,
						c.slug,
						c.' . Cubix_I18n::getTblField('title') . ' as title
					FROM countries c
					WHERE c.continent_id = ?
				';
				
				$continents[$k]->countries = $db->fetchAll($sql, $continent->id);
			}
		}
		
		return $continents; 
	}

	static public function byId($id)
	{
		$db = Zend_Registry::get('db');
		return $db->fetchRow('
			SELECT
				id, iso, phone_prefix, ndd_prefix
			FROM countries_phone_code
			WHERE id = ?
		', $id);
	}
	
	static public function byPhonePrefix($phone_prefix)
	{
		$db = Zend_Registry::get('db');
		return $db->fetchRow('
			SELECT
				id
			FROM countries_phone_code
			WHERE phone_prefix = ?
		', $phone_prefix);
	}
	
	public function getById($id)
	{
		return parent::getAdapter()->query('SELECT * FROM countries WHERE id = ?', $id)->fetch();
	}
	
	public function getAll()
	{
		return parent::getAdapter()->query('SELECT * FROM countries')->fetchAll();
	}
}
