<?php
  class Model_Reports extends Cubix_Model
{
    protected $_table = 'reported';
    
    static $types = [1 => 'Fake Pics',
                     2 => 'Doesn\'t respond',   
                     3 => 'Wrong Phone Number',  
                     4 => 'Other' 
    ];
    
    public function getAll($page, $per_page= 20, $filter, $sort_field, $sort_dir, &$count){
       
       if(!intval($per_page)){
           $per_page = 20;
       }
       $page = (intval($page))?$page:1;
       $sql = '
            SELECT SQL_CALC_FOUND_ROWS  A.title, R.id, R.user_id, U.username as reporter, ad_id, report_type, report_more, reporter_email, INET_NTOA(reporter_ip) as reporter_ip , reporter_id, FROM_UNIXTIME(report_date) as report_date, if(checked, \'yes\',\'no\') as checked,  if(replied, \'yes\',\'no\') as replied 
            FROM reported R
            LEFT JOIN ads A ON R.ad_id = A.id 
            LEFT JOIN users U ON R.reporter_id = U.id 
            WHERE 1
       ';
        // <editor-fold defaultstate="collapsed" desc="Filtration by Date">
        /*
         'ad_id' => $req->ad_id,
            'user_id' => $req->user_id,
            'reporter_id' => $req->reporter_id,
            'report_type' => $req->report_type,
        */
       
       if ( strlen($filter['ad_id']) ) {
            $where .= ' AND ad_id  = '.intval($filter['ad_id']).'';
       }
       
       if ( strlen($filter['user_id']) ) {
            $where .= ' AND R.user_id  = '.intval($filter['user_id']).'';
       } 
       
       if ( strlen($filter['reporter_id']) ) {
            $where .= ' AND reporter_id  = '.intval($filter['reporter_id']).'';
       }
       
       if ( intval($filter['report_type']) > 0 ) {
            $where .= ' AND report_type  = '.intval($filter['report_type']).'';
       } 
        
       if ( intval($filter['checked']) >= 0 ) {
            $where .= ' AND checked  = '.intval($filter['checked']).'';
       } 
       
       unset($filter['filter_by']);
       unset($filter['date_from']);
       unset($filter['date_to']);
       unset($filter['one_pic']);
       unset($filter['no_pic']);
        // </editor-fold>
        
       $sql .= $where;
       // die($sql);
       $sqlCount = "SELECT FOUND_ROWS();";
        
       $sql .= '
            ORDER BY ' . $sort_field . ' ' . $sort_dir . '
            LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
        ';
       //die($sql);
        $results = parent::_fetchAll($sql);
        $count = $this->getAdapter()->fetchOne($sqlCount);
        
        return $results;
    }
    
    public function get($reportID){
        return parent::_fetchRow('
            SELECT R.id, R.user_id, ad_id, report_type, report_more, INET_NTOA(reporter_ip) as ip, reporter_id, report_date, reporter_email, UA.username AS advertiser, UR.username AS reporter,
             A.title, UR.email as reporter_mail, BC.username as adminNamechecked, BR.username as adminNameReplied  FROM reported R
            LEFT JOiN ads A ON A.id = R.ad_id
            LEFT JOiN users UA ON UA.id = R.user_id
            LEFT JOiN users UR ON UR.id = R.reporter_id
            LEFT JOIN backend_users BC ON BC.id = R.checkedby
            LEFT JOIN backend_users BR ON BR.id = R.repliedby
            WHERE R.id = ?
        ', array($reportID));
    }
    public function MarkChecked($reportID, $admin){
        
        $data = [
            'checked' => 1,
            'checkedby' => $admin 
        ];
        //var_dump($data);
        parent::getAdapter()->update($this->_table, $data, parent::getAdapter()->quoteInto('id = ?', $reportID));
    } 
    public function MarkReplied($reportID, $admin){
        $data = [
            'replied' => 1,
            'repliedby' => $admin 
        ];
        var_Dump($data);
        var_Dump($reportID);
       // die();
        parent::getAdapter()->update($this->_table, $data, parent::getAdapter()->quoteInto('id = ?', $reportID));
    }
}
?>
