<?php
class Model_Emails extends Cubix_Model
{
	protected $_table = 'log_emails';

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT
				e.id, e.name, e.email, e.subject,
				e.message, e.login_user_id, e.geo_info, e.date_added
			FROM log_emails e			
			
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(e.id)
			FROM log_emails e
			WHERE 1
		';
		$where = '';
		
		if ( strlen($filter['e.id']) ) {
			$where .= self::quote('AND e.id = ?', $filter['e.id']);
		}

		if ( strlen($filter['e.login_user_id']) ) {
			$where .= self::quote('AND e.login_user_id = ?', $filter['e.login_user_id']);
		}

		if ( strlen($filter['e.email']) ) {
			$where .= self::quote('AND e.email LIKE ?', $filter['e.email'] . '%');
		}

		
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY e.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}

	public function remove($id)
	{
		parent::remove(self::quote('id = ?', $id));
	}
	
	
}
