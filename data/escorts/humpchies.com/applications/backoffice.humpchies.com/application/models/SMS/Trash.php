<?php

class Model_SMS_Trash extends Cubix_Model
{
	
	protected $_table = 'sms_trash';
	//protected $_itemClass = 'Model_SMS_SMSItem';
	
	public function get($id)
	{
		$sql = '
			SELECT
				st.id, UNIX_TIMESTAMP(st.date) AS `date`, ads.title, ads.phone, st.phone_from, ads.status,
				st.text, st.is_read
			FROM sms_trash st
			LEFT JOIN ads ON st.ad_id = ads.id			
			LEFT JOIN users u ON u.id = ads.user_id
			WHERE st.id = ?
		';
		
		return parent::_fetchRow($sql, $id);
	}
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$sql = '
			SELECT
				st.id, st.type, UNIX_TIMESTAMP(st.date) AS date, ads.title, ads.phone, st.phone_from, ads.status,
				IF(LENGTH(st.text) > 30, CONCAT(SUBSTRING(st.text, 1, 30), \' ...\'), st.text) AS `text`, st.is_read, ads.id AS ad_id
			FROM sms_trash st
			LEFT JOIN ads ON ads.id = st.ad_id			
			LEFT JOIN users u ON u.id = ads.user_id
			WHERE 1
		';
		
		$countSql = '
			SELECT
				COUNT(st.id)
			FROM sms_trash st
			LEFT JOIN ads ON st.ad_id = ads.id
			LEFT JOIN users u ON u.id = ads.user_id
			WHERE 1
		';
		
		$where = '';
		
		if ( strlen($filter['s.status']) ) {
			$where .= self::quote('AND s.status = ?', $filter['s.status']);
		}
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote( " AND DATE(st.date) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
				}
			}
		if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE(st.date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}

		if ( $filter['date_to'] ) {
			$where .= self::quote(" AND DATE(st.date) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		return parent::_fetchAll($sql);
	}
	
	public function deleteForever($ids)
	{
		foreach ( $ids as $id )
		{
			parent::remove(self::quote('id = ?', $id));
		}
	} 
}
