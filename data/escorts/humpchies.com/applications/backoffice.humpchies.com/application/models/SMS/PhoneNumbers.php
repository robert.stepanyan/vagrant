<?php

class Model_SMS_PhoneNumbers extends Cubix_Model
{
	protected $_table = 'phone_numbers';
	public function get($id)
	{
		$sql = '
			SELECT pn.* FROM phone_numbers pn
			WHERE pn.id = ?
		';
		
		return parent::_fetchRow($sql, $id);
	}
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS pn.*, png.name AS group_name, COUNT(so.id) AS contacted_times
			FROM phone_numbers pn
		';
		
		$joins = array(
			'INNER JOIN phone_number_groups png ON png.id = pn.group_id',
			'LEFT JOIN sms_outbox so ON so.phone_to = pn.number AND so.status = 1'
		);

		$where = ' WHERE 1 ';
		$having = '';

		if ( $filter['number'] ) {
			$where .= self::quote(' AND pn.number LIKE ?', $filter['number'] . '%');
		}
		
		if ( $filter['group_id'] ) {
			$where .= self::quote(' AND pn.group_id = ?', $filter['group_id']);
		}
		
		if ( $filter['blacklisted'] ) {
			$where .= self::quote(' AND pn.is_blacklisted = ?', $filter['blacklisted'] == 1 ? 1 : 0);
		}

        if ( $filter['contact_times'] && !empty($filter['contact_times']) ) {
		    if($filter['contact_state'] !== 'not_contacted') {
                $having .= self::quote(' HAVING COUNT(so.id) = ?', $filter['contact_times']);
            }
        }
		
		
		
		if ( $filter['contact_state'] == 'contacted' ) {
			$joins[1] = 'INNER JOIN sms_outbox so ON so.phone_to = pn.number AND so.status = 1';
		} elseif ( $filter['contact_state'] == 'not_contacted' ) {
			$joins[1] = 'LEFT JOIN sms_outbox so ON so.phone_to = pn.number';
			$having .= ' HAVING contacted_times = 0';
		} elseif ( $filter['contact_state'] == 'contact_failed' ) {
			$joins[1] = 'INNER JOIN sms_outbox so ON so.phone_to = pn.number AND so.status = 2';
		} elseif ( $filter['contact_state'] == 'contact_pending' ) {
			$joins[1] = 'INNER JOIN sms_outbox so ON so.phone_to = pn.number AND so.status = 0';
		}

		$sql .= implode(' ', $joins);
		$sql .= $where;
		$sql .= ' GROUP BY pn.id';
		$sql .= $having;
		
		$sql .= ' ORDER BY ' . $sort_field . ' ' . $sort_dir;
		
		if ( $p !== false && $pp !== false ) {
			$sql .= ' LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp;
		}
		$result = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne('SELECT FOUND_ROWS()'));
		
		return $result;
	}
	
	public function save(Model_SMS_PhoneNumbersItem $item)
	{
		parent::save($item);
	}
	
	public function remove($id)
	{
		parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));
	}
	
	public function checkIfExists($number, $id = null)
	{
		$sql = 'SELECT TRUE FROM phone_numbers WHERE number = ?';
		$bind = array($number);
		if ( $id ) {
			$bind[] = $id;
			$sql .= ' AND id != ?';
		}
		
		return $this->getAdapter()->fetchOne($sql, $bind);
	}
	
	public function checkIfExistsInProject($number)
	{
		$result = array(
			'exists'	=> false
		);
		
		$escort_exists = $this->getAdapter()->fetchOne('SELECT TRUE FROM escort_profiles_v2 WHERE contact_phone_parsed LIKE "%' . $number . '%"');
		if ( $escort_exists ) {
			$result['exists'] = true;
			$result['reason'] = 'Escort with this phone exists.';
			return $result;
		}
		
		$have_in_contacts = $this->getAdapter()->fetchOne('SELECT TRUE FROM phone_numbers WHERE number = ?', $number);
		if ( $have_in_contacts ) {
			$result['exists'] = true;
			$result['reason'] = 'Already in contacts.';
			return $result;
		}
		
		return $result;
	}
	
	
	public function removeExisting()
	{
		 $existing_numbers = $this->getAdapter()->fetchAll('
			SELECT pn.id, pn.number, ep.showname, ep.escort_id
			FROM escort_profiles_v2 ep 
			INNER JOIN phone_numbers pn ON pn.number = ep.contact_phone_parsed 
		');
		 
		 $ids_to_remove = array();
		 foreach($existing_numbers as $en) {
			 $ids_to_remove[] = (string)$en->id;
		 }
		 
		 if ( count($ids_to_remove) ) {
			$this->getAdapter()->query('DELETE FROM phone_numbers WHERE id IN (' . implode(',', $ids_to_remove) . ')');
		 }
		 
		 return $existing_numbers;
	}
	
	public function blacklistNotDelivered()
	{
		$not_delivered = $this->getAdapter()->fetchAll('
			SELECT pn.id, pn.number, so.status
			FROM phone_numbers pn
			INNER JOIN sms_outbox so ON so.phone_to = pn.number AND status = 2 AND is_spam = 1
			WHERE pn.is_blacklisted = 0
		');
		
		 $ids_to_blacklist = array();
		 foreach($not_delivered as $nd) {
			 $ids_to_blacklist[] = (string)$nd->id;
		 }
		 
		 if ( count($ids_to_blacklist) ) {
			$this->getAdapter()->query('UPDATE phone_numbers SET is_blacklisted = 1 WHERE id IN (' . implode(',', $ids_to_blacklist) . ')');
		 }
		 
		 return $not_delivered;
	}
}
