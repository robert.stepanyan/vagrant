<?php 

class Model_Sms_InstantBook extends Cubix_Model {
  public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$sql = '
      SELECT SQL_CALC_FOUND_ROWS
				ibr.id, UNIX_TIMESTAMP(ibr.booking_date) AS start_time, ibr.location, ibr.duration, ibr.outcall_address,
        IF(LENGTH(ibr.special_request) > 30, CONCAT(SUBSTRING(ibr.special_request, 1, 30), " ..."), ibr.special_request) AS special_request,
        ibr.phone, ibr.contact_type, ibr.wait_time, ibr.escort_id, ibr.status, UNIX_TIMESTAMP(ibr.request_date) AS request_date
			FROM instant_book_requests ibr
			WHERE 1
		';
		
		$where = '';
		
		if ( strlen($filter['escort_id']) ) {
			$where .= self::quote('AND ibr.escort_id = ?', $filter['escort_id']);
		}
		
		if ( strlen($filter['phone_number'])) {
			$where .= " AND ibr.phone LIKE '%" . $filter['phone_number'] . "%'";
		}
		
		if ( strlen($filter['status']) ) {
			$where .= self::quote(' AND ibr.status = ?', $filter['status']);
		}
		
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote( " AND DATE(ibr.request_date) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
				}
			}
		if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE(ibr.request_date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}

		if ( $filter['date_to'] ) {
			$where .= self::quote(" AND DATE(ibr.request_date) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}
		
		$sql .= $where;
		$countSql = 'SELECT FOUND_ROWS()';
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
    ';
    
		$items = parent::_fetchAll($sql);
		
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return $items;
  }
  
  public function getAllEscorts($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$sql = '
      SELECT SQL_CALC_FOUND_ROWS
				ibe.id, IF(ibe.incall = 1, "incall", "outcall") AS location, ibe.escort_id,
        UNIX_TIMESTAMP(ibe.start_date) AS start_date, UNIX_TIMESTAMP(ibe.end_date) AS end_date
			FROM instant_book_escorts ibe
			WHERE 1
		';
		
		$where = '';
		
		if ( strlen($filter['escort_id']) ) {
			$where .= self::quote('AND ibe.escort_id = ?', $filter['escort_id']);
    }
    
		
		if ( strlen($filter['location'])) {
      if ($filter['location'] == 'outcall') {
        $where .= self::quote(' AND ibe.outcall = 1');
      } elseif ($filter['location'] == 'incall') {
        $where .= self::quote(' AND ibe.incall = 1');
      }
		}
		
		$sql .= $where;
		$countSql = 'SELECT FOUND_ROWS()';
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
    ';
    
		$items = parent::_fetchAll($sql);
		
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return $items;
  }
}