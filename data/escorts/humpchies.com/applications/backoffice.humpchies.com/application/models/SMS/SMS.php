<?php

class Model_SMS_SMS extends Cubix_Model
{
	
	protected $_table = 'sms';
	protected $_itemClass = 'Model_SMS_SMSItem';
	
	// SMS
	const SMS_TYPE_INCOMING = 1;
	const SMS_TYPE_OUTGOING = 2;
	
	const SMS_IS_READ = 1;
	const SMS_IS_NOT_READ = 0;
	
	const SMS_STATUS_PENDING = 0;
	const SMS_STATUS_DELETED = 99;

	const SMS_STATUS_POSTPONED = 166;

    const SMS_STATUS_NOT_FOUND = 1; //Wrong ID or report has expired
    const SMS_STATUS_EXPIRED = 2; //Messages expired.
    const SMS_STATUS_SENT = 3; //Message is sent without a final delivery report.
    const SMS_STATUS_DELIVERED = 4; //Message is delivered to the recipient
    const SMS_STATUS_UNDELIVERED = 5; //Message is undelivered (invalid number, roaming error etc)
    const SMS_STATUS_FAILED = 6; //Sending message failed – please report it to us
    const SMS_STATUS_REJECTED = 7; //Message is undelivered (invalid number, roaming error etc)
    const SMS_STATUS_UNKNOWN = 8; //No report (the message may be either delivered or not)
    const SMS_STATUS_QUEUE = 9; //Message is waiting to be sent
    const SMS_STATUS_ACCEPTED = 10; //Message is delivered to the operator
    const SMS_STATUS_STOP = 12; //Bulk has been stopped by the user.

    public static $Statuses = array(
        self::SMS_STATUS_NOT_FOUND => "NOT FOUND",
        self::SMS_STATUS_EXPIRED => "EXPIRED",
        self::SMS_STATUS_SENT => "SENT",
        self::SMS_STATUS_DELIVERED => "DELIVERED",
        self::SMS_STATUS_UNDELIVERED => "UNDELIVERED",
        self::SMS_STATUS_FAILED => "FAILED",
        self::SMS_STATUS_REJECTED => "REJECTED",
        self::SMS_STATUS_UNKNOWN => "UNKNOWN",
        self::SMS_STATUS_QUEUE => "QUEUE",
        self::SMS_STATUS_ACCEPTED => "ACCEPTED",
        self::SMS_STATUS_STOP => "STOP",
    );

	static function get($id) {
		//return self::getAdapter()->fetchOne('SELECT TRUE FROM sms_outbox WHERE msg_id_2sms = ?', $id);
		return self::getAdapter()->fetchOne('SELECT TRUE FROM sms_outbox WHERE id = ?', $id);
		//$result = mysql_query('SELECT 1 FROM sms_outbox WHERE id = ' . $id);
	}

	public function updateStatus($id, $status) 
	{
		self::getAdapter()->query('UPDATE sms_outbox SET status = ' . intval($status) . ' WHERE id = ' . $id);
	}

	public function updateDeliveryDate($id, $date) {
		//$this->getAdapter()->query('UPDATE sms_outbox SET delivery_date = FROM_UNIXTIME(' . intval($date) . ') WHERE id = ' . $id);
		//self::getAdapter()->update('sms_outbox', array('delivery_date' => new Zend_Db_Expr('FROM_UNIXTIME(' . intval($date) . ')') ), self::getAdapter()->quoteInto('msg_id_2sms = ?', $id));
		self::getAdapter()->update('sms_outbox', array('delivery_date' => new Zend_Db_Expr('FROM_UNIXTIME(' . intval($date) . ')') ), self::getAdapter()->quoteInto('id = ?', $id));
	}
	
	public function toggleIsRead($type, $set_as, $ids)
	{
		$table = '';
		switch( $type )
		{
			case 'inbox':
				$table = 'sms_inbox';
			break;
			case 'saved':
				$table = 'sms_saved';	
			break;
			case 'trash':
				$table = 'sms_trash';	
			break;
		}
		
		$sql = "UPDATE {$table} SET is_read = {$set_as} WHERE id = ?";
		
		foreach ( $ids as $id )
		{
			self::getAdapter()->query($sql, $id);
		}
	} 
	
	public function getTemplates($sales_user_id)
	{
		if (is_null($sales_user_id))
			return self::getAdapter()->query('SELECT * FROM sms_templates ORDER BY title ASC')->fetchAll();
		else
			return self::getAdapter()->query('SELECT * FROM sms_templates WHERE sales_user_id = ? ORDER BY title ASC', $sales_user_id)->fetchAll();
	}
	
	public function getTemplate($template_id)
	{
		return self::getAdapter()->fetchOne('SELECT template FROM sms_templates WHERE id = ?', $template_id);
	}
	
	public static function encodeString($string)
	{
		$range = array(38, 60, 62);
		$range = array_merge($range, range(128, 255));

		$string = mb_convert_encoding($string, 'ISO-8859-1');
		$new_string = '';
//echo mb_internal_encoding();
//$new_string = str_replace('ü', '&#252;', $string);
		for ( $i = 0; $i < mb_strlen($string, 'ISO-8859-1'); ++$i ) 
		{
			$letter = mb_substr($string, $i, 1, 'ISO-8859-1');
			$ascii = (int) ord($letter);
//var_dump($letter);
			if ( in_array($ascii, $range) ) 
			{
//echo sprintf('%c', 252);
//var_dump($string, $letter, $ascii);die;
				$new_string .= '&#' . $ascii . ';';
			}
			else 
			{
				$new_string .= $letter;
			}
		}
//echo $new_string;die;
		return $new_string;
	}
}
