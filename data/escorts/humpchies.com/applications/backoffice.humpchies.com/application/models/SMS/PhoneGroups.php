<?php

class Model_SMS_PhoneGroups extends Cubix_Model
{
	protected $_table = 'phone_number_groups';
	public function get($id)
	{
		$sql = '
			SELECT png.* FROM phone_number_groups png
			WHERE png.id = ?
		';
		
		return parent::_fetchRow($sql, $id);
	}
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS png.*, COUNT(pn.id) AS numbers_count
			FROM phone_number_groups png
			LEFT JOIN phone_numbers pn ON pn.group_id = png.id
			WHERE 1
		';
		
		$where = '';
		
		if ( $filter['name'] ) {
			$where .= self::quote('AND png.name LIKE ?', $filter['name'] . '%');
		}
		
		$sql .= $where;
		
		$sql .= '
			GROUP by png.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		$result = parent::_fetchAll($sql);
		$count = intval($this->getAdapter()->fetchOne('SELECT FOUND_ROWS()'));
		
		return $result;
	}
	
	public static function getForSelect()
	{
		return parent::_fetchAll('SELECT * FROM phone_number_groups ORDER BY name ASC');
	}
	
	public function save(Model_SMS_PhoneGroupsItem $item)
	{
		parent::save($item);
	}
	
	public function remove($id)
	{
		parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));
	}
}
