<?php

class Model_SMS_Emailtemplates extends Cubix_Model
{
	protected $_table = 'email_templates_internal';
	
	public function get($id)
	{
		$sql = 'SELECT * FROM email_templates_internal	WHERE id = ?';
		
		return parent::_fetchRow($sql, $id);
	}
	
	public function getAll($p, $pp, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT s.*, b.username FROM email_templates_internal s 
			LEFT JOIN backend_users b ON b.id = s.backend_user_id
			WHERE 1 
		';
		
		$countSql = 'SELECT COUNT(id) FROM email_templates_internal WHERE 1 ';
		
		$where = '';
		
		if ( isset($filter['backend_user_id']) ) {
			$where .= self::quote('AND backend_user_id = ?', $filter['sales_user_id']);
		}
				
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		return parent::_fetchAll($sql);
	}
	
	public function getList($bu = null)
	{
		$where = '';
		
		if (!is_null($bu))
			$where = 'AND backend_user_id = ' . $bu;
		
		return parent::getAdapter()->query('SELECT * FROM email_templates_internal WHERE 1 ' . $where)->fetchAll();
	}


	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$id = $data['id'];

			parent::getAdapter()->update($this->_table, array(
				'subject' => $data['subject'],
				'from' => $data['from'],
				'body' => $data['body'],
				'signature' => $data['signature']
			), parent::quote('id = ?', $id));
		}
		else
		{
			parent::getAdapter()->insert($this->_table, array(
				'subject' => $data['subject'],
				'from' => $data['from'],
				'body' => $data['body'],
				'signature' => $data['signature'],
				'backend_user_id' => $data['backend_user_id']
			));

			$id = parent::getAdapter()->lastInsertId();
		}

		return $id;
	}	
	
	public function remove($id)
	{
		parent::getAdapter()->delete('email_templates_internal', parent::quote('id = ?', $id));
	} 
}
