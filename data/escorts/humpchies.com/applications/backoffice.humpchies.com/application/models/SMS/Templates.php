<?php

class Model_SMS_Templates extends Cubix_Model
{
	
	protected $_table = 'sms_templates';
	
	public function get($id)
	{
		$sql = 'SELECT * FROM sms_templates	WHERE id = ?';
		
		return parent::_fetchRow($sql, $id);
	}
	
	public function getAll($p, $pp, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT s.*, b.username FROM sms_templates s 
			LEFT JOIN backend_users b ON b.id = s.sales_user_id
			WHERE 1 
		';
		
		$countSql = 'SELECT COUNT(id) FROM sms_templates WHERE 1 ';
		
		$where = '';
		
		if ( isset($filter['sales_user_id']) ) {
			$where .= self::quote('AND sales_user_id = ?', $filter['sales_user_id']);
		}
				
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		return parent::_fetchAll($sql);
	}
	
	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$id = $data['id'];

			parent::getAdapter()->update($this->_table, array(
				'title' => $data['title'],
				'template' => $data['template']
			), parent::quote('id = ?', $id));
		}
		else
		{
			parent::getAdapter()->insert($this->_table, array(
				'title' => $data['title'],
				'template' => $data['template'],
				'sales_user_id' => $data['sales_user_id']
			));

			$id = parent::getAdapter()->lastInsertId();
		}

		return $id;
	}	
	
	public function remove($id)
	{
		parent::getAdapter()->delete('sms_templates', parent::quote('id = ?', $id));
	} 
}
