<?php

class Model_SMS_EmailOutbox extends Cubix_Model
{	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				eo.id, eo.email, eo.subject, eo.text, eo.user_id, UNIX_TIMESTAMP(eo.date) AS date, bu.username
			FROM email_outbox eo
			LEFT JOIN escorts e ON e.user_id = eo.user_id
			LEFT JOIN backend_users bu ON bu.id = eo.backend_user_id
			WHERE 1
		';
				
		$where = '';
		
		if ( $filter['escort_id'] ) {
			$where .= " AND e.id = " . $filter['escort_id'];
		}
		
		if ( $filter['status'] ) {
			$where .= " AND e.status & " . $filter['status'];
		}
		
		if ( strlen($filter['backend_user_id']) > 0 && $filter['backend_user_id'] != -1) {
			$where .= " AND eo.backend_user_id = " . $filter['backend_user_id'];
		}

		if ( $filter['backend_user_id'] == -1) {
			$where .= " AND eo.backend_user_id IS NULL";
		}
		
		if ( $filter['email'] ) {
			$where .= " AND eo.email LIKE '%" . $filter['email'] . "%'";
		}
		
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote( " AND DATE(eo.date) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
				}
			}
		if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE(eo.date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}
		
		$sql .= $where;
		$countSql = 'SELECT FOUND_ROWS()';
		
		$sql .= '
			GROUP BY eo.id 
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
		
		$items = parent::_fetchAll($sql);
		
		$count = $this->getAdapter()->fetchOne($countSql);
		return $items;
	}
	
	public function add($data)
	{
		$this->getAdapter()->insert('email_outbox', $data);
	}
}
