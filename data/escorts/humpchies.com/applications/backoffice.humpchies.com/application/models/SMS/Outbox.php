<?php

class Model_SMS_Outbox extends Cubix_Model
{
	
	protected $_table = 'sms_outbox';
	//protected $_itemClass = 'Model_SMS_SMSItem';
	
	public function get($id)
	{
		$sql = '
			SELECT
				so.id, UNIX_TIMESTAMP(so.date) AS `date`,ad.title, ad.phone, so.phone_to, so.phone_from, bu.username AS sales_person,
				so.text, so.status AS sms_status, UNIX_TIMESTAMP(so.delivery_date) AS delivery_date, so.ad_ids
			FROM sms_outbox so
			LEFT JOIN ads as ad ON so.ad_id = ad.id			
			LEFT JOIN users as u ON u.id = ad.user_id
			LEFT JOIN backend_users bu ON bu.id = so.sender_sales_person
			WHERE so.id = ?
		';

		return parent::_fetchRow($sql, $id);
	}
	
	public function getAdsByPhone($phone, &$data)
	{
		$sql = '
			SELECT
				ads.id, ads.title, ads.status, ads.user_id 
			FROM ( SELECT id FROM ads WHERE phone = ? LIMIT 100 ) adx
			INNER JOIN ads ON adx.id = ads.id	
			LEFT JOIN users u ON u.id = ads.user_id
		';
		
		$ads = parent::_fetchAll($sql, $phone);
		if ( $ads ) {
			if ( count($ads) == 1 ) {
				$data->showname = "<span class='escort-popup'><a href='#".$ads[0]->id."'>".$ads[0]->title. " (" . $ads[0]->id . ")</a></span>";
				$data->sales_person = $ads[0]->username;
				$data->status = $ads[0]->status;
			}
			else {
				$uIds = array();
				foreach ($ads as $ad)
				{
					$uid = intval($ad->user_id);
					if (!in_array($uid, $uIds)){
                        $uIds[] = $uid;
                    }
				}

				if (count($uIds) == 1 && !in_array(0, $uIds))
				{
					$name = parent::getAdapter()->fetchOne('SELECT username FROM users WHERE id = ?', $uIds[0]);
					$ads_str = $name . ' <strong>(Username)</strong>';
				}
				else
				{
                    $ads_str = "<span class='escort-popup'>";
					foreach ( $ads as $k => $ad )
					{
                        $ads_str .= "<a href='#". $ad->id."'>" . $ad->title . "</a><br/>";

						if ( $k == 9 ) break;
					}
                    $ads_str .= "</span>";
				}
				$data->showname = $ads_str;
			}
		}
	}
	
	public function getAll($p, $pp, $sort_field, $sort_dir, $filter, &$count)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				so.id, UNIX_TIMESTAMP(so.date) AS date, ads.title, ads.phone, so.phone_to, ads.status, ads.status as ad_status, bu.username AS sales_person, bu.username AS sender_sales_person, 
				IF(LENGTH(so.text) > 30, CONCAT(SUBSTRING(so.text, 1, 30), \' ...\'), so.text) AS `text`, so.status AS sms_status, UNIX_TIMESTAMP(so.delivery_date) AS delivery_date, ads.id AS ad_id
			FROM sms_outbox so
			LEFT JOIN ads ON so.ad_id = ads.id			
			LEFT JOIN users u ON u.id = ads.user_id
			LEFT JOIN backend_users bu ON bu.id = so.sender_sales_person
			WHERE 1
		';
		
		$where = '';
		
		if ( strlen($filter['s.status']) ) {
			$where .= self::quote('AND s.status = ?', $filter['s.status']);
		}
		if ( $filter['date_from'] && $filter['date_to'] ) {
				if ( date('d.m.Y', $filter['date_from']) == date('d.m.Y', $filter['date_to']) ) {
					$where .= self::quote( " AND DATE(so.date) = DATE(FROM_UNIXTIME(?))" , $filter['date_from']);
				}
			}
		if ( $filter['date_from'] ) {
				$where .= self::quote(" AND DATE(so.date) >= DATE(FROM_UNIXTIME(?)) ", $filter['date_from']);
			}

		if ( $filter['date_to'] ) {
			$where .= self::quote(" AND DATE(so.date) <= DATE(FROM_UNIXTIME(?)) ", $filter['date_to']);
		}
		
		if ( $filter['ad_title'] ) {
			$where .= " AND ads.title LIKE '" . $filter['showname'] . "%'";
		}
		
		if ( $filter['ad_id'] ) {
			$where .= " AND so.ad_id = " . $filter['ad_id'];
		}
		
		if($filter['only_this_user']){ // ONLY FOR EF AND SALES USERS
			$where .= " AND (so.sender_sales_person = " . $filter['only_this_user']. " OR u.sales_user_id = " . $filter['only_this_user'] . ")";
		}else{
			if ( $filter['sender_sales_person'] ) {
				$where .= " AND so.sender_sales_person = " . $filter['sender_sales_person'];
			}
		}
		
		if ( $filter['ad_status'] ) {
			$where .= " AND ads.status = " . $filter['ad_status'];
		}
		
		if ( strlen($filter['status']) > 0 ) {
			$where .= " AND so.status = " . $filter['status'];
		}
		
		if ( $filter['phone_to'] ) {
			$phone_to = str_replace(' ', '', $filter['phone_to']);
			$where .= " AND so.phone_to LIKE '%" . $phone_to . "%'";
		}
		
		if ( $filter['is_spam'] ) {
			$where .= " AND is_spam = 1";
		} else {
			$where .= " AND is_spam = 0";
		}
		
		if ( $filter['year'] ) {
			$where .= " AND so.year = " . $filter['year'];
		}
		
		$sql .= $where;
		$countSql = 'SELECT FOUND_ROWS()';
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		';
//		var_dump($sql);die;
//        var_dump($filter);die;
		$items = parent::_fetchAll($sql);
		
		$count = $this->getAdapter()->fetchOne($countSql);
		return $items;
	}
	
	public function save($data)
	{
		$ad_id = null;
		$ad_ids = null;
		if ( count($data['ad_ids']) > 1 )
		{
			foreach($data['ad_ids'] as $e_id)
			{
                $ad_ids[] = $e_id;
			}
            $ad_ids = implode(',', $ad_ids);
		}		
		elseif ( count($data['ad_ids']) == 1 ){
            $ad_id = $data['ad_ids'][0];
        }
		
		if ( $data['ad_id'] ) {
			$ad_id = $data['ad_id'];
		}

		$sql = 'INSERT INTO sms_outbox (ad_id, ad_ids, phone_to, phone_from, `text`, sender_sales_person, is_spam, `year` ) VALUES(?, ?, ?, ?, ?, ?, ?, ?)';
		
		if ( is_array($data['phone_number']) ){
			$i = 0;
			foreach ($data['phone_number'] AS $phone)
			{
				$this->getAdapter()->query($sql, array($data['ad_ids'][$i], $ad_ids, $phone, $data['phone_from'], $data['text'], $data['sender_sales_person'], 0 , date("Y")));
				$i++;
			}
		}
		else{
			$this->getAdapter()->query($sql, array($ad_id, $ad_ids, $data['phone_number'], $data['phone_from'], $data['text'], $data['sender_sales_person'], $data['is_spam'] ? 1 : 0, date("Y")));
		}
		return $this->getAdapter()->lastInsertId();
	}

	public function savePostpone($data)
	{
		$ad_id = null;
		$ad_ids = null;
		if ( count($data['ad_ids']) > 1 )
		{
			foreach($data['ad_ids'] as $e_id)
			{
				$ad_ids[] = $e_id;
			}
			$ad_ids = implode(',', $ad_ids);
		}		
		elseif ( count($data['ad_ids']) == 1 )
			$ad_id = $data['ad_ids'][0];
		
		$sql = 'INSERT INTO sms_outbox (ad_id, ad_ids, phone_to, phone_from, `text`, sender_sales_person, is_spam, `status`, exact_send_date) VALUES(?, ?, ?, ?, ?, ?, ?, ?, FROM_UNIXTIME(' . intval($data['exact_send_date']) . '))';
		
		if ( is_array($data['phone_number']) ){
			$i = 0;
			foreach ($data['phone_number'] AS $phone)
			{
				$this->getAdapter()->query($sql, array($data['ad_ids'][$i], $ad_ids, $phone, $data['phone_from'], $data['text'], $data['application_id'], $data['sender_sales_person'], $data['is_spam'] ? 1 : 0, $data['status']));
				$i++;
			}
		}
		else{
			$this->getAdapter()->query($sql, array($ad_id, $ad_ids, $data['phone_number'], $data['phone_from'], $data['text'], $data['application_id'], $data['sender_sales_person'], $data['is_spam'] ? 1 : 0, $data['status']));
		}
		return $this->getAdapter()->lastInsertId();
	}
	
	public function delete($ids)
	{
		$sql = "
			INSERT INTO sms_trash (type, ad_id, phone_from, phone_to, `date`, `text`, is_read) 
				(SELECT " . Model_SMS_SMS::SMS_TYPE_OUTGOING . ", ad_id, phone_from, phone_to, `date`, `text`, 1 
			FROM sms_outbox
			WHERE id = ?)
		";
		
		foreach ( $ids as $id )
		{
			$this->getAdapter()->query($sql, $id);
		
			parent::remove(self::quote('id = ?', $id));
		}
	}
	
	public function update2SMSData($id, $msg_id = null, $status = 0)
	{
		$data = array();
		if ( $msg_id )
			$data = array_merge ($data, array('msg_id_2sms' => $msg_id));
		
		if ( $status !== false ) 
			$data = array_merge ($data, array('status_2sms' => $status));
		
		$this->getAdapter()->update('sms_outbox', $data, $this->getAdapter()->quoteInto('id = ?', $id));
	}
	
	public function getByIds($ids)
	{
		$sql = "SELECT ad_id, phone_to AS phone_number, phone_from, `text` FROM " .$this->_table. "  WHERE id IN (".implode(',', $ids).")";
		return $this->getAdapter()->fetchAll($sql);
	}		
}
