<?php

class Model_BlacklistedClients extends Cubix_Model
{

    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT B.id, B.`type`, `name`, B.phone, `date`, city, country, description, created, advertiser_id, B.`status`, username 
            FROM blacklist B
            LEFT JOIN users U ON (U.id = B.advertiser_id)
            WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM blacklist
			WHERE 1
		';

        if ( strlen($filter['client']) ) {
            $sql .= self::quote('AND name LIKE ?', '%' . $filter['client'] . '%');
            
        }

        if ( strlen($filter['escort']) ) {
            $sql .= self::quote('AND username LIKE ?', '%' . $filter['escort'] . '%');
            
        }
       
        if ( strlen($filter['phone']) ) {
            $sql .= self::quote('AND B.phone LIKE ?', '%' . $filter['phone'] . '%');
            
        }
        
		if ( strlen($filter['comment']) ) {
			$sql .= self::quote('AND B.description LIKE ?', '%' . $filter['comment'] . '%');
			
		}
        
        if ( strlen($filter['type']) ) {
            $sql .= self::quote('AND type =  ?', $filter['type'] );
        }

        if ( strlen($filter['status']) ) {
            $sql .= self::quote('AND B.status =  ?', $filter['status'] );
        }

		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';    
        
		$countSql .= '';

		$count = intval($this->getAdapter()->fetchOne($countSql));
		return parent::_fetchAll($sql);
	}

	public function get($id)
	{
		return self::_fetchRow('
			SELECT B.id, B.`type`, `name`, B.phone, `date`, city, country, description, created, advertiser_id, B.`status`, username 
            FROM blacklist B
            LEFT JOIN users U ON (U.id = B.advertiser_id)
            WHERE B.id = ?
		', array($id));
	}

    

    public function save($data, $id = null){
        if ( ! is_null($id) ) {
            
			self::getAdapter()->update('blacklist', $data, self::quote('id = ?', $id));
		}
		else {
			self::getAdapter()->insert('blacklist', $data);
		}
    }

    public function delete($id = array()){
        self::getAdapter()->delete('blacklist', self::getAdapter()->quoteInto('id = ?', $id));
    }

   

}
