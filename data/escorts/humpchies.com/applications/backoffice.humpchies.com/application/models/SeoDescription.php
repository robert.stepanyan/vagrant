<?php
class Model_SeoDescription extends Cubix_Model
{
    protected $_table                   = 'seo_description';
    protected static $redisKeyCity      = "SeoDescriptionCity";
    
    
    
    // public static function getAllCities() {
//        
//        $citiesList = Core_Redis::get($redisKeyCity);
//         
//        if($citiesList){
//            return unserialize($citiesList);
//        }
//        
//        $sql = 'SELECT `city_id`, `page`, `en`, `fr` FROM seo_description WHERE 1  = 1';
//        
//        $data = parent::_fetchAll($sql);
//        
//        var_dump($data); exit();
//        
//        
//        
//        Core_Redis::set($redisKey, serialize($data));   
//     
//        //var_dump($tagsList);
//        return $tagsList;
//       
//    }  
    
    
    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count) {
        $sql = '
            SELECT SD.id, C.`name` AS city_name, CR.`name` AS region_name, SD.`page`, SD.ad_id, SD.en, SD.fr FROM seo_description SD 
                LEFT JOIN cities C ON SD.city_id = C.id
                LEFT JOIN cities_regions CR ON SD.region_id = CR.id
            WHERE 1
        ';

        $countSql = '
            SELECT COUNT(DISTINCT(id))
            FROM seo_description
            WHERE 1
        ';

        if ( strlen($filter['city']) ) {
            $sql .= self::quote('AND C.`name` LIKE ?', '%' . $filter['city'] . '%');
            
        }

        if ( strlen($filter['region']) ) {
            $sql .= self::quote('AND CR.`name` LIKE ?', '%' . $filter['region'] . '%');
            
        }
        
        $sql .= '
            GROUP BY SD.id
            ORDER BY SD.' . $sort_field . ' ' . $sort_dir . '
            LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
        ';    
        
        $countSql .= '';

        $count = intval($this->getAdapter()->fetchOne($countSql));
        return parent::_fetchAll($sql);
    }


    public function get($id)
    {
        return self::_fetchRow('
             SELECT SD.id, C.`name` AS city_name, CR.`name` AS region_name, SD.`page`, SD.ad_id, SD.en, SD.fr, SD.city_id, SD.region_id  FROM seo_description SD
                LEFT JOIN cities C ON SD.city_id = C.id
                LEFT JOIN cities_regions CR ON SD.region_id = CR.id
            WHERE SD.id = ?
        ', array($id));
    }
    
    
    
    public function save($data, $id = null){
        if ( ! is_null($id) ) {
            
            self::getAdapter()->update('seo_description', $data, self::quote('id = ?', $id));
        }
        else {
            self::getAdapter()->insert('seo_description', $data);
        }
        
        Core_Redis::del(self::$redisKeyCity);    
    }
    
     public function delete($id = array()){
        self::getAdapter()->delete('seo_description', self::getAdapter()->quoteInto('id = ?', $id));
        Core_Redis::del(self::$redisKeyCity); 
    }

    
} 