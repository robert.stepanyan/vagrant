<?


class Model_Settings  extends Cubix_Model {
	protected $_table = 'holiday';
	
	const SKYPE_STATUS_ONLINE = 1;
	const SKYPE_STATUS_AWAY = 2;
	const SKYPE_STATUS_BUSY = 3;
	const SKYPE_STATUS_OFFLINE = 4;
	
	public function __construct() 
	{
		
	}
	
	private function CreateWhere(array $filter,$bydate=NULL)
	{	
		$where = array();	
		foreach($filter as $k=>$v)
		{	
			if(isset($v) && ($v=trim($v))!='')
			{	
				if($k!='date_from' && $k!='date_to')
				{	if($k=='showname')
					{	$v.='%';
						$v = self::$_db->quote($v);
						$where[] = " `$k` LIKE $v ";
					}
					else
					{	$v = self::$_db->quote($v);
						$where[] = " `$k`=$v ";
					}
				}
				elseif(is_numeric($v))
				{	$date =date('Y-m-d',$v);
					if($k=='date_from')
					{	$dates = isset($bydate)?'DATE_FORMAT(`date`,"%Y-%m-%d")':'date_from';
						$where[]=" $dates>='$date' ";
					}
					else
					{	$dates = isset($bydate)?'DATE_FORMAT(`date`,"%Y-%m-%d")':'date_to';
						$where[]=" $dates<='$date' ";
					}
				}
			}
		}
		$where = implode(' AND ', $where);
		return $where;
	}
	
	public static function GetUsers($bu_id=NULL)
	{	
		$user_id = isset($bu_id)?$bu_id:0;
		return parent::_fetchAll("SELECT username,id FROM backend_users WHERE  is_disabled='0' AND id!='$user_id' ");
	}
		
	public static function GetTypes($super=null)
	{	
		$where = isset($super)?" WHERE `type`!='superadmin' ":'';
		return parent::_fetchAll("SELECT `type` FROM backend_users $where GROUP BY `type`");
	}
	
	public static function GetUserEmail($uid)
	{	
		$uid = self::$_db->quote($uid);	
		return parent::_fetchRow("SELECT `email` FROM backend_users WHERE id=$uid AND is_disabled='0' ")->email;
	}
	/* ChangePass Queries */
	
	public function ChangePass($pass,$id)
	{
		$pass = self::$_db->quote($pass);	
		return self::$_db->query("UPDATE backend_users SET `password`=$pass WHERE id='$id' ")->rowCount();
	}
	
	/* Message Queries */
	
	public function GetMsg($array,$my_id,$page,$per_page,$sort_field,$sort_dir,$user=null,$radio=null)
	{	
		$where = $this->CreateWhere($array,true);
		$search = array();
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		$page = !isset($page) || !is_numeric($page)?1:self::$_db->quote($page);
		$start = ($page-1)*$per_page;
		if($radio)
		{	if($radio=='inbox')
			{
				$search[]=" (`to`='$my_id' AND  backend_users.id=`from` ) ";
			}
			else
			{
				$search[]= "(`from`='$my_id' AND backend_users.id=`to`)";
			}
						
		}
		if($user)
		{		
			$user = self::$_db->quote($user);
			if($bu_user->type == 'superadmin' || $bu_user->type == 'admin'){
				$search[]="(`to`= $user AND backend_users.id=`to`) || (`from`=$user AND backend_users.id=`from`)";
			}
			else{
				$search[]="((`from`='$my_id' AND `to`=$user AND backend_users.id=`to` ) || (`to`='$my_id' AND `from`=$user AND backend_users.id=`from`))";
			}
		}
		elseif(!$radio)
		{
			if($bu_user->type == 'superadmin' || $bu_user->type == 'admin'){
				$search[]="backend_users.id=`from`";
			}
			else{
				 $search[]="((`from`='$my_id' AND backend_users.id=`to`) || (`to`='$my_id' AND backend_users.id=`from`))";
			}
		}
		$where = $where!='' ?"  $where AND ":'';
		$where.= implode(' AND ', $search);
		if($where == ''){
			$where = 1;
		}
		$sql = "SELECT SQL_CALC_FOUND_ROWS username,`type`,UNIX_TIMESTAMP(`date`) as `date`,`from`,`to`,title,`msg`,`read`,backend_msg.`id`
					FROM backend_msg JOIN backend_users ON remove_status!='$my_id' AND remove_status!='both' AND
					$where 
		ORDER BY `$sort_field` $sort_dir LIMIT $start,$per_page";
		//die($sql);
		$data = parent::_fetchAll($sql);
		$found =  parent::_fetchRow('SELECT FOUND_ROWS() AS q');
		return array('data'=>$data,'count'=>$found->q);
	}
	
	public function NewMsg($my_id,$user_id,$title,$msg)
	{	$user_id = self::$_db->quote($user_id);
		$title = self::$_db->quote($title);
		$msg = self::$_db->quote($msg);
		$insert = "INSERT INTO backend_msg(`from`,`to`,`title`,`msg`) VALUES('$my_id',$user_id,$title,$msg)";
		return self::$_db->query($insert)->rowCount();
	}
		


	public function GetCurrentMsg($id,$user_id,$my_id)
	{	$id = self::$_db->quote($id);
		$user_id = self::$_db->quote($user_id);

		$result = parent::_fetchRow("SELECT `title`,`msg`,`date`,`from`,`to`,`read`,username FROM backend_msg JOIN backend_users ON backend_users.id=$user_id AND backend_msg.id=$id");
			if(!empty($result) && $result->read!='1' && $my_id==$result->to)
			self::$_db->query("UPDATE backend_msg SET `read`='1' WHERE id=$id AND `to`=$my_id AND `from`=$user_id ");
		return $result;
	}
	
	public function RemoveMsg($msg_id,$from,$to,$my_id)
	{	
		$from = self::$_db->quote($from);
		$msg_id = self::$_db->quote($msg_id);
		$to = self::$_db->quote($to);
		$result = parent::_fetchRow("SELECT `remove_status` FROM backend_msg WHERE id=$msg_id AND `from`=$from AND `to`=$to  ");
		if(empty($result))
			die;
		if($result->remove_status=='both')
			die;
		
		if($result->remove_status==0)
		{
			$sql = "UPDATE backend_msg SET `remove_status`='$my_id' WHERE id=$msg_id AND `from`=$from AND `to`=$to  ";
		}
		elseif(is_numeric($result->remove_status))
		{
			$sql = "UPDATE backend_msg SET `remove_status`='both' WHERE id=$msg_id AND `from`=$from AND `to`=$to  ";
		}
		else
			die;
		
		return 	self::$_db->query($sql)->rowCount();
	}


	public static function GetNewMsgCount($id)
	{
				
		return parent::_fetchRow("SELECT COUNT(`id`) AS q FROM backend_msg WHERE `to`=$id AND `read`='0' AND (`remove_status`!='both' AND `remove_status`!='$id') ")->q;
	}
	
	/* Holidays Queries */
	
	public function GetHolidays($array,$page,$per_page,$sort_field,$sort_dir)
	{	$where = $this->CreateWhere($array);
		$where = $where!=''?" AND $where ":'';
		$page = !isset($page) || !is_numeric($page)?1:self::$_db->quote($page);
		$start = ($page-1)*$per_page;
		$sql = "
			SELECT SQL_CALC_FOUND_ROWS
				backend_users.username, UNIX_TIMESTAMP(date_from) as date_from, backend_users.type AS `type`, UNIX_TIMESTAMP(date_to) as date_to,
				is_approved, holiday.id, UNIX_TIMESTAMP(`date`) as `date`, backend_user_id AS uid, bu2.username AS backup_user
			FROM $this->_table
			INNER JOIN backend_users ON backend_users.id = backend_user_id
			LEFT JOIN backend_users bu2 ON bu2.id = backup_user_id
			$where
			ORDER BY `$sort_field` $sort_dir
			LIMIT $start,$per_page
		";
		
		$data = parent::_fetchAll($sql);
		$found =  parent::_fetchRow('SELECT FOUND_ROWS() AS q');
		return array('data'=>$data,'count'=>$found->q);
	}
	
	public function GetCurrentHoliday($id)
	{
		$id = self::$_db->quote($id);
		return parent::_fetchRow("SELECT date_from,date_to,is_approved,id,backend_user_id, backup_user_id FROM $this->_table WHERE id=$id ");
	}
	
	public function Accept($user_id,$id,$from,$to)
	{
		$id = self::$_db->quote($id);
		$from = self::$_db->quote($from);
		$to = self::$_db->quote($to);
		$user_id = self::$_db->quote($user_id);	
		$sql ="UPDATE $this->_table SET date_from=$from,date_to=$to,is_approved='1' WHERE id=$id AND backend_user_id=$user_id ";
		return self::$_db->query($sql)->rowCount();
		
	}
	
	public function RemoveHoliday($id,$user_id=0)
	{	$id = self::$_db->quote($id);
		$sql = "DELETE FROM $this->_table WHERE id=$id";
		if($user_id!=0)
			$sql.=" AND backend_user_id='$user_id' ";
		return self::$_db->query($sql)->rowCount();
	}

	public function SaveHoliday($user,$date_from,$date_to,$approve, $backup_user_id = null)
	{	
		$user = self::$_db->quote($user);
		$date_from = self::$_db->quote($date_from);
		$date_to = self::$_db->quote($date_to);
		if (!$backup_user_id) $backup_user_id = 0;
		return self::$_db->query("INSERT IGNORE INTO $this->_table(`is_approved`,`backend_user_id`,`date_from`,`date_to`, `backup_user_id`) VALUES('$approve',$user,$date_from,$date_to, $backup_user_id)")->rowCount();
	}
	public function Active($id,$disable)
	{
		$id = self::$_db->quote($id);
		$sql = $disable=='1'?"UPDATE $this->_table SET is_approved='1' WHERE id=$id":"DELETE FROM $this->_table WHERE id=$id";
		return self::$_db->query($sql)->rowCount();
	}
	
	/* Penality Queries */
	
	public static function GetPenaltyTypes()
	{
		return parent::_fetchAll("SELECT `reason_name`,id FROM penalty WHERE  reason_name!='' ");
	}
	
	public function SavePenalty($bu_user,$escort_id,$penality,$reason,$reason_name,$from_settings=true)
	{	
		
		$penality = self::$_db->quote($penality);
		$reason = self::$_db->quote($reason);
		$reason_name = self::$_db->quote($reason_name);
		$escort_id = self::$_db->quote($escort_id);
		if($from_settings)
		{	
			$check = "SELECT id FROM `users` WHERE sales_user_id=$bu_user AND id=(SELECT user_id FROM `escorts` WHERE id=$escort_id)";
		}
		else
		{
			$check = "SELECT user_id FROM `escorts` WHERE id=$escort_id AND user_id=$bu_user";
			$bu_user = self::$_db->quote($bu_user);
			$bu_user =self::$_db->fetchRow("SELECT sales_user_id FROM `users` WHERE id=$bu_user ")->sales_user_id;

		}
		
		$bu_user = self::$_db->quote($bu_user);
		$bu_escort =  self::$_db->fetchRow($check);
		if(empty($bu_escort))
			return 2;
		return self::$_db->query("INSERT IGNORE INTO penalty(backend_user_id,escort_id,penalty,reason,`reason_name`) VALUES($bu_user,$escort_id,$penality,$reason,$reason_name)")->rowCount();
	}

	public function GetPenalty($array,$bu_id,$page,$per_page,$sort_field,$sort_dir)
	{
		$where = $this->CreateWhere($array,true);
		$where = $where!=''?" AND $where ":'';
		$page = !isset($page) || !is_numeric($page)?1:self::$_db->quote($page);
		$start = ($page-1)*$per_page;
		$lastMonth = date("Y-m-d", strtotime("-2 month"));
		
		if(isset($bu_id))
		{
			$user_id = $bu_id;
			$where.= " AND backend_user_id=backend_users.id ";
		}
		else
			$user_id = 'backend_users.id';
	
		$sql_lastmonth = "SELECT   username,SUM(penalty) AS month_summ,`date`
				FROM penalty JOIN backend_users ON backend_user_id=$user_id  AND DATE_FORMAT(`date`,'%Y-%m-%d')>='$lastMonth'  $where GROUP BY `username`, DATE_FORMAT(`date`,'%Y-%m')
				ORDER BY `$sort_field` $sort_dir LIMIT $start,$per_page";
	
		$data = parent::_fetchAll($sql_lastmonth);
		
			
		$total = "SELECT SQL_CALC_FOUND_ROWS username,SUM(penalty) AS total,backend_users.`type`,backend_user_id,penalty.id
				  FROM penalty JOIN backend_users ON backend_user_id=$user_id   $where GROUP BY `username`
				  ORDER BY `$sort_field` $sort_dir LIMIT $start,$per_page";
		
		$total = parent::_fetchAll($total);
		$found =  parent::_fetchRow('SELECT FOUND_ROWS() AS q');
		return array('data'=>$data,'count'=>$found->q,'total'=>$total);
	}
	
	public function GetTpl($id)
	{	
		$id = self::$_db->quote($id);
		return parent::_fetchRow("SELECT penalty,reason FROM penalty WHERE id=$id ");
	}
	public function GetCurrentPenalty($array,$page,$per_page,$sort_field,$sort_dir)
	{
		$page = !isset($page) || !is_numeric($page)?1:self::$_db->quote($page);
		$where = $this->CreateWhere($array,true);

		$start = ($page-1)*$per_page;
		$sql = "SELECT  SQL_CALC_FOUND_ROWS UNIX_TIMESTAMP(`date`) as `date`,reason,reason_name,`escort_id`,`penalty`,penalty.`status`,showname,penalty.id,`read`,backend_user_id
				FROM `escorts` JOIN  penalty WHERE escort_id=escorts.id AND  $where 
				ORDER BY `$sort_field` $sort_dir LIMIT $start,$per_page";
		
		$data = parent::_fetchAll($sql);
		$found =  parent::_fetchRow('SELECT FOUND_ROWS() AS q');
		return array('data'=>$data,'count'=>$found->q,'data'=>$data);
		
	}
	
	public function SetRead($id,$user_id)
	{	
		$id = self::$_db->quote($id);
		$user_id =self::$_db->quote($user_id);
		return self::$_db->query("UPDATE penalty SET `read`='1' WHERE id=$id AND backend_user_id=$user_id ")->rowCount();
	}
	public static function GetNewPenaltyCount($id)
	{
		return parent::_fetchRow("SELECT COUNT(`id`) AS q FROM penalty WHERE `backend_user_id`='$id' AND `read`='0' ")->q;
	}
	public function RemovePenalty($id)
	{
		$id = self::$_db->quote($id);
		return	self::$_db->query("DELETE FROM penalty WHERE id=$id ")->rowCount();
	}
	
	public function getSkypeStatus($user_id)
	{
		return	self::$_db->fetchOne("SELECT skype_status FROM backend_users WHERE id = ?", $user_id);
	}
	
	public function saveSkypeStatus($user_id, $status)
	{
		return	self::$_db->update('backend_users', array('skype_status' => $status), self::$_db->quoteInto('id = ?', $user_id));
	}		
}
