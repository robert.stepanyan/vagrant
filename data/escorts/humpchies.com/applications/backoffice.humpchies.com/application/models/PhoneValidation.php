<?php

class Model_PhoneValidation extends Cubix_Model
{

    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT id, verification_id, number, description, received_date, ad_id, user_id
			FROM phone_validation_callbacks
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(id))
			FROM phone_validation_callbacks
			WHERE 1
		';

		if ( strlen($filter['number']) ) {
			$sql .= self::quote('AND number LIKE ?', '%' . $filter['number'] . '%');
			$countSql .= self::quote('AND number LIKE ?', '%' . $filter['number'] . '%');
		}


		$sql .= '
			GROUP BY id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql .= '';

		$count = intval($this->getAdapter()->fetchOne($countSql));
		return parent::_fetchAll($sql);
	}

	public function get($id)
	{
		return self::_fetchRow('
			SELECT * FROM blacklisted_phones WHERE id = ?
		', array($id));
	}

    

    public function save($data,$id = null){
        if ( ! is_null($id) ) {
			self::getAdapter()->update('blacklisted_phones', $data, self::quote('id = ?', $id));
		}
		else {
			self::getAdapter()->insert('blacklisted_phones', $data);
		}
    }

    public function delete($id = array()){
        self::getAdapter()->delete('blacklisted_phones', self::getAdapter()->quoteInto('id = ?', $id));
    }


    public function checkWords($text){
        $sql = '
			SELECT id, name
			FROM blacklisted_phones
			WHERE 1';

        $wordList = parent::_fetchAll($sql);

        if( count($wordList) > 0 ){
            foreach ( $wordList as $word ){
                $pattern = '/^'.$word.'/';
                preg_match($pattern, $text, $matches, PREG_OFFSET_CAPTURE);
                var_dump($matches);
                exit;
            }
            
        }
        
    }

}
