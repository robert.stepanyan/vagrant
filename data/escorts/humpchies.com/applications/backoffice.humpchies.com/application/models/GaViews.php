<?php
class Model_GaViews extends Cubix_Model
{
    protected $_table = 'ga_views';
    
    
    public function get($id){
        return parent::_fetchRow('
            SELECT a.*, a.comment as ad_comment, op.status as package_status, op.expiration_date, u.username, u.date_banned, u.comment, p.name as package_name, u.email, ad_ip
                        FROM ads a 
            INNER JOIN users u ON u.id = a.user_id
                        LEFT JOIN order_packages op ON op.ads_id = a.id
            LEFT JOIN packages p ON p.id = op.package_id
            WHERE a.id = ?
        ', array($id));
    }
        
    public function getUnfinishedProcess(){
        return parent::_fetchRow('SELECT id, start_date, end_date, current_page, total_pages, rows_per_page FROM ga_views WHERE current_page < total_pages ORDER BY id asc', []);
    }

    
    public function insert($data)
    {
        parent::getAdapter()->insert($this->_table, $data);

        return parent::getAdapter()->lastInsertId();
    }

    public function update($id, $data)
    {
        try{    
        parent::getAdapter()->update($this->_table, $data, parent::getAdapter()->quoteInto('id = ?', $id)); 
        }catch(Exception $e){
            //die("Duplicate Description");
            var_dump($e);
//            die();
        }
    }
    
    public function remove($id)
    {
        parent::getAdapter()->update($this->_table, array('status' => self::STATUS_DELETED), parent::getAdapter()->quoteInto('id = ?', $id));
        //parent::remove(self::quote('id = ?', $id));
    }

   
}

