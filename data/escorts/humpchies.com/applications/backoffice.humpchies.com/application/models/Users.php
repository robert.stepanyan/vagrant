<?php
class Model_Users extends Cubix_Model
{
    protected $_table = 'users';
    protected $_itemClass = 'Model_UserItem';
    const VALIDATION_KEY = '4c6d393d35712744396b623967dfeab913';

    const USER_TYPE_MEMBER = 2;
    const USER_TYPE_ADVERTISEMENT = 1;

    public static $statuses = array(
        0 => 'active',
        1 => 'general',
        2 => 'requested removal',
        3 => 'theft',
        4 => 'invalid phone',
        5 => 'male',
        6 => 'reputation',
        7 => 'explicit',
        8 => 'flooding',
        9 => 'fake pics',
        10 => 'fraud',
        11 => 'copyright',
        12 => 'shady',
        14 => 'promotes other sites',
        15 => 'Dicks',
        16 => 'Illegal shit',
        17 => 'Impersonation',
        18 => 'Remote',
        19 => 'potential minor',
        20 => 'potential human trafficking',
        21 => 'dushbag', 
        22 => 'no initial pics',
        23 => 'Bypassing emojis',
        24 => 'Bypassing image checkup by orientation',
        25 => 'dushbag images',
        26 => 'disrespect',
        //27 => 'Socially wrong',
        27 => 'Blacklisted',
        28 => 'Delete',
        29 => 'WatchChargeBack',
        30 => 'BannedbyIP'
    );

    public function get($id)
    {
            return parent::_fetchRow('
                    SELECT id, username, email, phone, INET_NTOA(ip) as ip, date_banned, comment, auto_approval, disable_comments, date_created, credits, ip_ban_date, INET_NTOA(ip_ban) as ip_ban, language FROM users WHERE id = ?
            ', array($id));
    }
    
    public function getUserLoginHistoryData($id){
        return parent::_fetchAll('
                SELECT *
                FROM users_login_history
                WHERE user_id = ?
                ORDER BY login_date DESC
                LIMIT 50
        ', array($id));
    }
    
    static public function getIpCountryList(){
        return parent::_fetchAll('
                SELECT distinct user_country FROM users where date_banned = 0 and user_country != \'\' and user_country is not null  ORDER BY user_country
        ');
    }
    
    public function getCountUserLoginHistoryData($id){
        
        //$count = $this->getAdapter()->fetchOne($countSql);
        
        return parent::getAdapter()->fetchOne('
                SELECT count(*)
                FROM users_login_history
                WHERE user_id = ?
                ORDER BY login_date DESC
        ', $id);
    }
    

    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
    {
        
            parent::_exec("SET SESSION group_concat_max_len = 1000000");
            $sql = '
                    SELECT SQL_CALC_FOUND_ROWS id, username, email, used_user_ids, phone, INET_NTOA(ip) as ip, 
                    date_created, date_activated, date_banned, comment, type, if(date_banned = 29 , \'charge\',\'clean\') as chargeback,
                    
                    (SELECT GROUP_CONCAT(distinct concat(user_id, \'-\', UC.date_banned) SEPARATOR ", ") FROM `user_cookies` US LEFT JOIN users UC ON UC.id = US.user_id  WHERE `cookie` IN( SELECT cookie FROM user_cookies WHERE user_id = u.id  )  AND  user_id != u.id )
                          AS duplicate_protection   
                    FROM users u			
                    WHERE 1
            ';

            $countSql = '
                    SELECT COUNT(*)
                    FROM users
                    WHERE 1
            ';
            $where = '';

            if ( strlen($filter['username']) ) {
                    $where .= self::quote('AND username LIKE ?', $filter['username'] . '%');
            }

            if ( strlen($filter['id']) ) {
                    $where .= self::quote('AND id = ?', $filter['id']);
            }

            if ( strlen($filter['date_banned']) ) {
                if($filter['date_banned'] == -1){
            $where .= 'AND date_banned = 0 AND date_activated = 0';
        } elseif ($filter['date_banned'] == 0) {
            $where .= self::quote('AND date_activated <> 0 AND date_banned = ?', $filter['date_banned']);
        } elseif ($filter['date_banned'] == -2) {
            //$where .= 'AND date_banned = ANY (SELECT date_banned FROM users)';
        }
        else {
            $where .= self::quote('AND date_banned = ?', $filter['date_banned']);
        }
            }

            if ( strlen($filter['phone']) ) {
                    $where .= self::quote('AND phone LIKE ? ', '%'. $filter['phone'] . '%');
            }

            if ( strlen($filter['email']) ) {
                    $where .= self::quote('AND email LIKE ?', "%" . $filter['email'] . '%');
            }

            if ( strlen($filter['auto_approval']) ) {
                    $where .= self::quote(' AND auto_approval = ?', $filter['auto_approval']);
            }
            
            if ( strlen($filter['fraudulent']) ) {
                
                    $where .= ' AND id IN (SELECT user_id FROM user_cookies WHERE cookie IN (SELECT cookie FROM users U LEFT JOIN user_cookies C ON U.id = C.user_id WHERE date_banned  = 29))';
            }

            if ( strlen($filter['type']) && intval($filter['type']) > 0 ) {
                    
                $where .= self::quote(' AND type = ?', $filter['type']);
            } 


            $sql .= $where;
            
            $sqlCount = "SELECT FOUND_ROWS();";

            $sql .= '
                    ORDER BY ' . $sort_field . ' ' . $sort_dir . '
                    LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
            ';
            

            $results = parent::_fetchAll($sql);
            $count = $this->getAdapter()->fetchOne($countSql);

            return $results;
    }
    public function getAdv($limit, $chunk){
        $sql = "SELECT username, email, phone, user_location, user_country, date_created, `type`, `language` FROM users WHERE `type`=2 AND date_banned = 0  limit " . $limit . ", " . $chunk ;
        //die($sql);
        return parent::_fetchAll($sql);
    }

    public function getIps($page, $per_page, $filter, $sort_field, $sort_dir, &$count){
        $sql = 'SELECT SQL_CALC_FOUND_ROWS id, username, email, phone, date_created, ip, ip_real, ip_class, user_location, user_country, last_ip, last_class, last_location, last_country
                ,(SELECT UNIX_TIMESTAMP(login_date) FROM users_login_history WHERE user_id = id ORDER BY login_date DESC LIMIT 1) AS last_login, type
                    FROM users u            
                    WHERE 1  
               ';

        $countSql = '
                SELECT COUNT(*)
                FROM users
                WHERE 1 
        ';
        
        $where = '';

        if ( strlen($filter['username']) ) {
                $where .= self::quote('AND username LIKE ?', $filter['username'] . '%');
        }

        if ( strlen($filter['id']) ) {
                $where .= self::quote('AND id = ?', $filter['id']);
        }

       if ( strlen($filter['email']) ) {
                $where .= self::quote('AND email LIKE ?', $filter['email'] . '%');
        }

        if ( strlen($filter['date_banned']) && intval($filter['date_banned']) >= 0 ) {
                
            $where .= self::quote(' AND date_banned = ?', $filter['date_banned']);
        }else{
           // $where .= ' AND date_banned = 0 ';
        } 
        
        if ( strlen($filter['type']) && intval($filter['type']) > 0 ) {
                
            $where .= self::quote(' AND type = ?', $filter['type']);
        } 
        
        if ( strlen($filter['country']) && $filter['country'] != -2 &&  $filter['country'] != '' ) {
                
            $where .= self::quote(' AND user_country = ?', $filter['country']);
        } 


        if ( strlen($filter['class'])  ) {
                
            $where .= ' AND ( `ip_class` = \'' . $filter['class'] . '\' OR `last_class` = \'' . $filter['class'] . '\' )';
        } 
        if ( strlen($filter['ads'])  ) {
            switch($filter['ads']){
                
             case 'active'      :   $where .= ' AND (SELECT COUNT(*) FROM ads WHERE user_id = u.id AND `status`=\'active\') > 0'; break;
             case 'any'         :   $where .= ' AND (SELECT COUNT(*) FROM ads WHERE user_id = u.id) > 0'; break;
             case 'noactive'    :   $where .= ' AND (SELECT COUNT(*) FROM ads WHERE user_id = u.id AND `status` = \'active\') = 0'; break;
             case 'noads'       :   $where .= ' AND (SELECT COUNT(*) FROM ads WHERE user_id = u.id ) = 0'; break;
             default            :   $where .= ""; break;   
            }
        }

            $sql .= $where;
            
            $sqlCount = "SELECT FOUND_ROWS();";

            $sql .= '
                    ORDER BY ' . $sort_field . ' ' . $sort_dir . '
                    LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
            ';
            
           //var_dump($sql);
            $results = parent::_fetchAll($sql);
            $count = $this->getAdapter()->fetchOne($countSql);

            return $results;
        
    }
    
    public function getChargebacks($page, $per_page, $filter, $sort_field, $sort_dir, &$count){
        $sql = 'SELECT distinct u.id, u.username, cookie, email, phone, ip_real, user_location, last_location, date_created FROM user_cookies c 
LEFT JOIN users u ON (u.id = c.user_id) 
WHERE u.date_banned = 0 AND  user_id IN (SELECT id FROM users WHERE date_banned != 0)  
               ';

        $countSql = '
                SELECT COUNT(*)
                FROM user_cookies c 
LEFT JOIN users u ON (u.id = c.user_id) 
WHERE u.date_banned = 0 AND  user_id IN (SELECT id FROM users WHERE date_banned = 29)  
        ';
        
        $where = '';

        if ( strlen($filter['username']) ) {
                $where .= self::quote('AND username LIKE ?', $filter['username'] . '%');
        }

        if ( strlen($filter['id']) ) {
                $where .= self::quote('AND id = ?', $filter['id']);
        }

       if ( strlen($filter['email']) ) {
                $where .= self::quote('AND email LIKE ?', $filter['email'] . '%');
        }

        if ( strlen($filter['date_banned']) && intval($filter['date_banned']) >= 0 ) {
                
            $where .= self::quote(' AND date_banned = ?', $filter['date_banned']);
        }else{
           // $where .= ' AND date_banned = 0 ';
        } 
        
        if ( strlen($filter['type']) && intval($filter['type']) > 0 ) {
                
            $where .= self::quote(' AND type = ?', $filter['type']);
        } 
        
        if ( strlen($filter['country']) && $filter['country'] != -2 &&  $filter['country'] != '' ) {
                
            $where .= self::quote(' AND user_country = ?', $filter['country']);
        } 


        if ( strlen($filter['class'])  ) {
                
            $where .= ' AND ( `ip_class` = \'' . $filter['class'] . '\' OR `last_class` = \'' . $filter['class'] . '\' )';
        } 
        
        //    $sql .= $where;
            
            $sqlCount = "SELECT FOUND_ROWS();";

            $sql .= '
                    ORDER BY ' . $sort_field . ' ' . $sort_dir . '
                    LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
            ';
            
           //var_dump($sql);
            $results = parent::_fetchAll($sql);
            $count = $this->getAdapter()->fetchOne($countSql);

            return $results;
        
    }
    
    public function disableAds($id)
    {
        parent::getAdapter()->update('ads', array('status' => 'inactive'), parent::getAdapter()->quoteInto('user_id = ?', $id));
    }
    
    public function getActiveDuplicates($id){
        
        $sql = "SELECT distinct user_id FROM user_cookies WHERE cookie IN (SELECT cookie FROM user_cookies WHERE user_id = $id ) ";
        return parent::_fetchAll($sql);
        
    }
    
    public function deactivateAdsUsers($ids){
         parent::getAdapter()->update('ads', array('status' => 'inactive'), parent::getAdapter()->quoteInto('user_id IN (' . implode(",", $ids). ')', $id));
    }
    
    public function banUsers($ids){
         parent::getAdapter()->update('users', array('date_banned' => '8'), parent::getAdapter()->quoteInto('id IN (' . implode(",", $ids). ')', $id));
    }
    
    public function checkUserOrders($id)
    {
        $count = parent::getAdapter()->fetchAll('
                SELECT op.ads_id FROM order_packages op 
                INNER JOIN ads a ON a.id = op.ads_id
                WHERE op.status IN (1,2) AND a.user_id = ?
		', $id);
        
        if($count) return true;
    }

    public function activateUser($user_id) {
        parent::getAdapter()->update('users', array(
            'date_activated' => time(),
            'date_banned' => 0
        ), array(
            parent::getAdapter()->quoteInto('id = ?', $user_id)
        ));

        return true;
    }

     public function banUser($user_id, $status, $comment) {
        parent::getAdapter()->update('users', array(
            'date_banned' => $status,
            'comment'     => $comment  
            
        ), array(
            parent::getAdapter()->quoteInto('id = ?', $user_id)
        ));

        return true;
    }
    
    public function floodingUser($user_id) {
        parent::getAdapter()->update('users', array(
            'date_banned' => 8
        ), array(
            parent::getAdapter()->quoteInto('id = ?', $user_id)
        ));

        return true;
    }
    
    public function fraudUser($user_id) {
        parent::getAdapter()->update('users', array(
            'date_banned' => 10
        ), array(
            parent::getAdapter()->quoteInto('id = ?', $user_id)
        ));

        return true;
    }
    
    public function chargeBackUser($user_id) {
        parent::getAdapter()->update('users', array(
            'date_banned' => 29
        ), array(
            parent::getAdapter()->quoteInto('id = ?', $user_id)
        ));

        return true;
    }

    public function activate($user_id)
    {
            $this->_db->update('users', array(
                    'date_activated' => time(),
                    'activation_hash' => null,
                    'status' => STATUS_ACTIVE
            ), array(
                    $this->_db->quoteInto('id = ?', $user_id)
            ));

            return true;
    }

    public function setStatus($user_id,$status)
    {
            $this->_db->update('users', array(
                    'status' => $status
            ), array(
                    $this->_db->quoteInto('id = ?', $user_id)
            ));

            return true;
    }

    public function insert($data)
    {
            parent::getAdapter()->insert($this->_table, $data);

            return parent::getAdapter()->lastInsertId();
    }

    public function update($user_id, $data)
    {
            parent::getAdapter()->update($this->_table, $data, parent::getAdapter()->quoteInto('id = ?', $user_id));
    }

    public function remove($id)
    {
            parent::remove(self::quote('id = ?', $id));
    }


    public static function sendPostActivationEmail($user) {
            Cubix_Email::sendTemplate('activate_success', $user['email'], array(
                    'username' => $user['username']
            ));
    }

    public function usernameExists($username, $id = null)
    {
            $where = '';
            if($id){
                    $where = 'AND id <> '. $id;
            }
            return parent::getAdapter()->fetchOne('
                    SELECT id FROM users WHERE username = ? '. $where
            , $username);
    }

    public function emailExists($email, $id = null)
    {
            $where = '';
            if($id){
                    $where = ' AND id <> '. $id;
            }
            return parent::getAdapter()->fetchOne('
                    SELECT id FROM users WHERE email = ? '. $where
            , $email);
    }

    public function userIdExists($id)
    {
            return parent::getAdapter()->fetchOne('
                    SELECT id FROM users WHERE id = ? '
            , $id);
    }
	
	public function getForPicker($id)
	{
		return parent::getAdapter()->fetchRow('
                    SELECT id, username, date_banned, credits FROM users WHERE id = ? '
            , $id);
	}
	
	public function addCredits($id, $credits)
	{
		parent::getAdapter()->update($this->_table, array('credits' => new Zend_Db_Expr('credits + ' . $credits)), parent::getAdapter()->quoteInto('id = ?', $id));
	}
	
	public function subCredits($id, $credits)
	{
		parent::getAdapter()->update($this->_table, array('credits' => new Zend_Db_Expr('credits - ' . $credits)), parent::getAdapter()->quoteInto('id = ?', $id));
	}

    public static function getLastLogin($id) {
        return parent::getAdapter()->query('SELECT login_date, ip FROM `users_login_history` WHERE user_id = ? ORDER BY login_date DESC LIMIT 1', $id)->fetch();
	}
    
    public static function userLocationUpdate(){
       $sql = '
                    SELECT id , (SELECT ip FROM users_login_history WHERE user_id = id ORDER BY login_date ASC LIMIT 1) AS ip_reg FROM users U 
                    WHERE ip IS NOT NULL AND ip_real IS NULL ORDER BY id AND date_banned = 0 AND date_activated <> 0
            '; 
       $results = parent::_fetchAll($sql);
       return $results;
    }
	
    public static function getStatuses($ids){
          return parent::_fetchAll('
                SELECT id, date_banned
                FROM users
                WHERE id IN (' . $ids . ')
        ');
    }		
}
