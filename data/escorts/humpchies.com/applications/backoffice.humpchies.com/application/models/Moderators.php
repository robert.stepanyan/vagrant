<?php
class Model_Moderators extends Cubix_Model
{
	protected $_table = 'backend_users';
		
	public function getAll( )
	{
		$sql = "SELECT * FROM backend_users WHERE is_disabled = 0 ORDER BY username";
		
		return parent::_fetchAll($sql);
	}

	public function getByType($type)
	{
		if ( ! is_array($type) ) {
			$type = array($type);
		}

		if ( is_array($type) ) {
			foreach ( $type as $i => $t ) {
				$type[$i] = parent::getAdapter()->quote($t);
			}
			$type = implode(',', $type);
		}

		$sql = "SELECT * FROM backend_users WHERE is_disabled = 0 AND type IN (" . $type . ") ORDER BY username";

		return parent::_fetchAll($sql);
	}
}
