<?php
  class Model_AdsLocation extends Cubix_Model
{
    protected $_table = 'ads_location';
    
    static $types = [0 => 'Visible',
                     1 => 'Hidden',
    ];
    
    
    public function getAll($page, $per_page= 20, $filter, $sort_field, $sort_dir, &$count){
       
       if(!intval($per_page)){
           $per_page = 20;
       }
       $page = (intval($page))?$page:1;
       $sql = '
            SELECT SQL_CALC_FOUND_ROWS  AL.*, U.username,  if(hide_map, \'Yes\',\'No\') as hide_map 
            FROM ads_location AL
            LEFT JOIN users U ON AL.user_id = U.id 
            WHERE 1
       ';

       if ( strlen($filter['ad_id']) ) {
            $where .= ' AND ad_id  = '.intval($filter['ad_id']).'';
       }
       
       if ( strlen($filter['user_id']) ) {
            $where .= ' AND user_id  = '.intval($filter['user_id']).'';
       } 
       
       if ( isset($filter['hide_map']) && intval($filter['hide_map']) >= 0 ) {
            $where .= ' AND hide_map  = '.intval($filter['hide_map']).'';
       } 
       
       $sql .= $where;
       // die($sql);
       $sqlCount = "SELECT FOUND_ROWS();";
        
       $sql .= '
            ORDER BY ' . $sort_field . ' ' . $sort_dir . '
            LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
        ';
       //die($sql);
        $results = parent::_fetchAll($sql);
        $count = $this->getAdapter()->fetchOne($sqlCount);
        
        return $results;
    }
    
    public function getLogs($locationId){
        $result = [];
        
        
        $result["details"] =  parent::_fetchRow('
            SELECT SQL_CALC_FOUND_ROWS  AL.*, U.username,  if(hide_map, \'Yes\',\'No\') as hide_map 
            FROM ads_location AL
            LEFT JOIN users U ON AL.user_id = U.id
            WHERE AL.id = ?
        ', array($locationId));
        
        $sql = '
            SELECT  *,  if(old_hide_map, \'Yes\',\'No\') as old_hide_map , if(new_hide_map, \'Yes\',\'No\') as new_hide_map 
            FROM ads_location_log 
            WHERE ads_location_id = '. $locationId;
        $result["logs"] = parent::_fetchAll($sql);
        
        return $result;
    }
    
    
    
   
}
?>
