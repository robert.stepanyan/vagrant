<?php
class Model_Tags extends Cubix_Model
{
    protected $_table = 'tags';

  

    
    
    public static function getAll() {
        
        $redisKey = "TagsList-en";
        $tagsList = Core_Redis::get($redisKey);
         
        if($tagsList){
            return unserialize($tagsList);
        }
        
        $sql = 'SELECT id, `en` as name FROM tags WHERE 1  = 1';
        
        $data = parent::_fetchAll($sql);
        Core_Redis::set($redisKey, serialize($data));   
     
        //var_dump($tagsList);
        return $tagsList;
       
    }    
    
    
}
