<?php
  class Model_PrivateMessage extends Cubix_Model {
    protected $_table = 'message';
    
    public function getAll($page, $per_page= 20, $filter, $sort_field, $sort_dir, &$count){
       
       if(!intval($per_page)){
           $per_page = 20;
       }
       $page = (intval($page))?$page:1;
       $sql = '
           SELECT SQL_CALC_FOUND_ROWS  
                M.id, M.ad_id,  M.from_user_id, S.username AS sender_username, 
                M.to_user_id, R.username AS receiver_username, M.`message`, M.created, M.date_read, M.`status`                
            FROM message M
            LEFT JOIN users S ON M.from_user_id = S.id
            LEFT JOIN users R ON M.to_user_id = R.id  
            WHERE 1
       ';
       
       if ( strlen($filter['from_user_id']) ) {
            $where .= ' AND from_user_id  = '.intval($filter['from_user_id']).'';
       }
       
       if ( strlen($filter['to_user_id']) ) {
            $where .= ' AND to_user_id  = '.intval($filter['to_user_id']).'';
       } 
       
       if ( strlen($filter['ad_id']) ) {
            $where .= ' AND ad_id  = '.intval($filter['ad_id']).'';
       }

       if ( in_array($filter['status'], ['visible', 'hidden']) ) {
            $where .= ' AND M.`status`  = "'.$filter['status'].'"';
       } 
       
       unset($filter['filter_by']);
       unset($filter['date_from']);
       unset($filter['date_to']);
        
       $sql .= $where;
       //die($sql);
       $sqlCount = "SELECT FOUND_ROWS();";
        
       $sql .= '
            ORDER BY ' . $sort_field . ' ' . $sort_dir . '
            LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
        ';
       //die($sql);
        $results = parent::_fetchAll($sql);
        $count = $this->getAdapter()->fetchOne($sqlCount);
        
        return $results;
    }
    
    
    public function get($messageID) {
        
        return parent::_fetchRow('
            SELECT 
                M.ad_id, M.id, M.from_user_id, M.to_user_id, M.`message`, 
                M.created, M.date_read, M.`status`,
                A.title AS ad_title,
                S.username AS sender_username,
                R.username AS receiver_username,
                BC.username as adminNameChanged
            FROM message M
                LEFT JOiN ads A ON A.id = M.ad_id
                LEFT JOIN users S ON M.from_user_id = S.id
                LEFT JOIN users R ON M.to_user_id = R.id 
                LEFT JOIN backend_users BC ON BC.id = M.changed_by
            
            WHERE M.id = ?
        ', array($messageID));
    }
    
    
    
    public function ChageStatus($messageId, $status,  $admin){
        
        $data = [
            'status' => $status,
            'changed_by' => $admin 
        ];
        //var_dump($data);
        parent::getAdapter()->update($this->_table, $data, parent::getAdapter()->quoteInto('id = ?', $messageId));
    } 
    
    public function getNewMessages($date){
        
        $sql = "SELECT DISTINCT to_user_id, email, u.language FROM message m
                LEFT JOIN users u ON to_user_id = u.id
                WHERE `created` > ? AND date_read IS null";
        return parent::_fetchAll($sql, array($date));
    }
    public function getNewForumMessages($date){
        
        $sql = "SELECT DISTINCT to_user_id, email, u.language FROM forum_message m
                LEFT JOIN users u ON to_user_id = u.id
                WHERE `created` > ? AND date_read IS null";
        return parent::_fetchAll($sql, array($date));
    }
   
}
?>