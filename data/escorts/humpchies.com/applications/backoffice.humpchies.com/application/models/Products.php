<?php

class Model_Products extends Cubix_Model
{
	protected $_request;
	
	protected $_table = 'products';
	protected $_itemClass = 'Model_ProductItem';

	const TYPE_PRODUCT = 0;
	const TYPE_OPTIONAL_PRODUCT = 1;

	const CITY_PREMIUM_SPOT = 5;
	const TOUR_PREMIUM_SPOT = 6;
	const NO_REVIEWS = 8;
	const ADDITIONAL_CITY = 9;
	
	// Girl of the day
	//const GIRL_OF_THE_DAY = 15;
	
	const DAY_EXTEND = 16;


	const ADDITIONAL_CITY_PREMIUM_SPOT_CITY_1 = 11;
	const ADDITIONAL_CITY_PREMIUM_SPOT_CITY_2 = 12;
	const ADDITIONAL_CITY_PREMIUM_SPOT_CITY_3 = 13;
	
	const ADDITIONAL_AREA = 18;
	const EXTRA_COUNTRY = 18;
	const EXTRA_VIP_COUNTRY = 19;
	const EXTRA_VIP_COUNTRIES = 20;
	const EXTRA_VIP_COUNTRY_L = 21;
	const EXTRA_VIP_COUNTRIES_L = 22;

	const GOTD_STATUS_PENDING = 1;
	const GOTD_STATUS_ACTIVE  = 2;
	const GOTD_STATUS_EXPIRED = 3;
	
	public static $GOTD_STATUS_TITLES = array(
		self::GOTD_STATUS_ACTIVE => 'PENDING',
		self::GOTD_STATUS_EXPIRED => 'USED',
		self::GOTD_STATUS_PENDING => 'PENDING'
	);
	
	/*public function __construct()
	{
		
	}*/	
	
	public function migrate()
	{
		$sql = '
			SELECT
				p.*
			FROM product_prices p
			
		';

		$products = parent::_fetchAll($sql);

		foreach($products as $pr)
		{
			if ( $pr->user_type == 1 )
				$pr->user_type = 'escort';
			else
				$pr->user_type = 'agency';

			parent::getAdapter()->insert('product_prices_2',(array) $pr);
		}
	}

	public function getWithPrice($app_id, $user_type, $gender, $product_id = null, $pseudo_escort = false)
	{
		if ( $user_type ) {
			$user_type = USER_TYPE_AGENCY;
		}
		else {
			$user_type = USER_TYPE_SINGLE_GIRL;
		}

		if ( $pseudo_escort ) {
			$user_type = USER_TYPE_AGENCY;
		}

		if ( $product_id ) {
			$where = " AND p.id = {$product_id} ";
		}

		$sql = "
			SELECT
				p.id,
				p.name,
				p.available_for,
				pp.price
            FROM products p
			INNER JOIN product_prices pp ON pp.product_id = p.id
            WHERE
				pp.application_id = ?
                AND pp.user_type = ?
                AND pp.gender = ?
				{$where}
		";

		return parent::_fetchRow($sql, array($app_id, $user_type, $gender));
	}

	public function get($id)
	{
		$sql = '
			SELECT
				p.*
			FROM products p
			WHERE 1 AND p.id = ?
		';

		$product = parent::_fetchRow($sql, array($id));
		
		return $product;
	}

	public function getAllNames()
	{
		$sql = '
			SELECT
				p.*
			FROM products p
			INNER JOIN product_prices pp ON pp.product_id = p.id
			GROUP BY p.id
		';

		$products = parent::_fetchAll($sql);

		foreach($products as $k => $product)
		{
			$products[$k]->name = $product->name;
		}

		return $products;
	}

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = '
			SELECT
				p.*
			FROM products p
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(p.id))
			FROM products p
			WHERE 1
		';

		$where = '';
		/*if ( strlen($filter['u.application_id']) ) {
			$where .= self::quote('AND u.application_id = ?', $filter['u.application_id']);
		}*/
		
		$sql .= $where;
		$countSql .= $where;

		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = intval($this->getAdapter()->fetchOne($countSql));

		return parent::_fetchAll($sql);
	}

	public function save($name, $available_for, $prices/*, $is_special, $period*/)
	{
		parent::getAdapter()->insert($this->_table, array('name' => $name, 'available_for' => $available_for /*,'is_special' => $is_special, 'period' => $period*/));

		$prod_id = parent::getAdapter()->lastInsertId();

		if ( count($prices) > 0 )
		{
			$langs = Cubix_Application::getAll();

			$data = array();
			foreach($prices as $app_id => $price)
			{
				foreach($price as $pr)
				{
					$price_data = explode(':', $pr);

					$data['product_id'] = $prod_id;
					$data['user_type'] = $price_data[0];
					$data['gender'] = $price_data[1];
					$data['price'] = $price_data[2];

					/*$app_id = '';

					foreach($langs as $l) {
						if ( $l->iso == $lang ) {
							$app_id = $l->id;
						}
					}*/

					$data['application_id'] = $app_id;

					parent::getAdapter()->insert('product_prices', $data);
				}
			}
		}
	}
	
	public function update($available_for, $prod_id, $prices/*, $is_special, $period*/)
	{
		parent::getAdapter()->update($this->_table, array('available_for' => $available_for /*,'is_special' => $is_special, 'period' => $period*/), parent::quote('id = ?', $prod_id));

		parent::getAdapter()->query('DELETE FROM product_prices WHERE product_id = ?', array($prod_id));

		if ( count($prices) > 0 )
		{
			$langs = Cubix_Application::getAll();
			
			$data = array();
			foreach($prices as $app_id => $price)
			{
				foreach($price as $pr)
				{
					$price_data = explode(':', $pr);
					$data['product_id'] = $prod_id;
					$data['user_type'] = $price_data[0];
					$data['gender'] = $price_data[1];
					$data['price'] = $price_data[2];

					/*$app_id = '';
					foreach($langs as $l) {
						if ( $l->id == $app_id ) {
							$app_id = $l->id;
						}
					}*/
					$data['application_id'] = $app_id;

					parent::getAdapter()->insert('product_prices', $data);
				}
			}
		}
	}

	public function remove($id)
	{
		parent::getAdapter()->delete($this->_table, parent::quote('id = ?', $id));
		parent::getAdapter()->delete('product_prices', parent::quote('product_id = ?', $id));
	}

	public function removePrice($id, $app_id)
	{
		parent::getAdapter()->delete('product_prices', parent::quote('product_id = ?', $id));
	}

	public function hasProduct($escort_id, $product_id)
	{
		return parent::getAdapter()->fetchOne('
			SELECT COUNT(*)
			FROM order_packages o_p
			LEFT JOIN order_package_products o_p_p ON o_p_p.order_package_id = o_p.id
			WHERE o_p.escort_id = ? AND o_p.status = ? AND o_p_p.product_id = ?',
			array($escort_id, Model_Packages::STATUS_ACTIVE, $product_id)
		);
	}

	public function changeProduct($escort_id, $product_id, $action)
	{
		$package_id = parent::getAdapter()->fetchOne('SELECT o_p.id FROM order_packages o_p
			LEFT JOIN order_package_products o_p_p ON o_p_p.order_package_id = o_p.id
			WHERE o_p.escort_id = ? AND o_p.status = ?', array($escort_id, Model_Packages::STATUS_ACTIVE));

		if ($package_id)
		{
			if ($action == 'add')
			{
				parent::getAdapter()->insert('order_package_products', array(
					'order_package_id' => $package_id,
					'product_id' => $product_id,
					'is_optional' => 0,
					'price' => 10
				));
			}
			elseif ($action == 'remove')
			{
				parent::getAdapter()->query('DELETE from order_package_products WHERE order_package_id = ' . $package_id . ' AND product_id = ' . $product_id);
			}
		}
	}
	
	public static function validateGotd($date, $city_id)
	{
		$date = date("Y-m-d", $date);
	
		$city_where = '';
		// Zurich zone
		if ( $city_id == 17 || $city_id == 45 ) {
			$city_where = ' AND (o_p.gotd_city_id = 17 OR o_p.gotd_city_id = 45) ';
			$city_where_2 = ' AND (go.city_id = 17 OR go.city_id = 45) ';
		}		
		//Basel und Umgebung
		else if ( $city_id == 19 ) {
			$city_where = ' AND o_p.gotd_city_id = 19 ';
			$city_where_2 = ' AND go.city_id = 19 ';
		}
		//W'thur -Thurgau-St.Gallen
		else if ( $city_id == 18 ) {
			$city_where = ' AND o_p.gotd_city_id = 18 ';
			$city_where_2 = ' AND go.city_id = 18 ';
		}
		// Bern sector
		else if ( $city_id == 21 || $city_id == 22 ) {
			$city_where = ' AND (o_p.gotd_city_id = 22 OR o_p.gotd_city_id = 21) ';
			$city_where_2 = ' AND (go.city_id = 22 OR go.city_id = 21) ';
		}
		//Luzern - Innerschweiz
		else if ( $city_id == 24 ) {
			$city_where = ' AND o_p.gotd_city_id = 24 ';
			$city_where_2 = ' AND go.city_id = 24 ';
		}
		//Mittelland - AG - SO
		else if ( $city_id == 20 || $city_id == 44 ) {
			$city_where = ' AND o_p.gotd_city_id = 20 ';
			$city_where_2 = ' AND go.city_id = 20 ';
		}
		//	Biel - Grenchen - Jura | Wallis | Graubünden - Glarus sectors
		else if ( $city_id == 25 || $city_id == 26 || $city_id == 23 ) {
			$city_where = ' AND (o_p.gotd_city_id = 25 OR o_p.gotd_city_id = 26 OR o_p.gotd_city_id = 23) ';
			$city_where_2 = ' AND (go.city_id = 25 OR go.city_id = 26 OR go.city_id = 23) ';
		}
		
		$is_day_avail = parent::getAdapter()->fetchOne('
			SELECT TRUE FROM order_packages o_p
			LEFT JOIN order_package_products o_p_p ON o_p_p.order_package_id = o_p.id
			WHERE (o_p.status = ? OR o_p.status = ?) AND DATE(o_p.gotd_activation_date) = ? AND (o_p.gotd_status = ? OR o_p.gotd_status = ?) ' . $city_where . ' GROUP BY o_p.escort_id', array(Model_Packages::STATUS_ACTIVE, Model_Packages::STATUS_PENDING, $date, Model_Products::GOTD_STATUS_PENDING, Model_Products::GOTD_STATUS_ACTIVE)
		);
		
		$is_day_avail_2 = parent::getAdapter()->fetchOne('
			SELECT TRUE FROM gotd_orders go
			WHERE DATE(go.activation_date) = ? AND (go.status = ? OR go.status = ?) ' . $city_where_2 . ' GROUP BY go.escort_id', array($date, Model_Products::GOTD_STATUS_PENDING, Model_Products::GOTD_STATUS_ACTIVE)
		);
		
		if ( $is_day_avail || $is_day_avail_2 ) {
			return true;
		}
		
		return false;
	}
	
	public static function validateGotdActivationDate($activation_date, $period, $activation_type, $exact_date) 
	{
		/*$msg = "";
		if ( $activation_type == Model_Packages::ACTIVATE_ASAP ) {
			if (  )
		} 
		else if ( $activation_type == Model_Packages::ACTIVATE_AT_EXACT_DATE ) {
			
		}*/
	}
}
