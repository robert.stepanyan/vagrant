<?php
  class Model_AdVerification extends Cubix_Model
{
    protected $_table = 'ad_verification';
        
    public $rejectReasons = array( 
                    1 => "You haven't sent a photo of you holding close to your face a sheet of paper on which you wrote humpchies and the current date and the phone number or the photo are not clear.",
                    2 => "The photos in your gallery do not show your face and you haven't sent any original versions for those photos with face visible",                   
    );
    
    public $rejectReasonsFrench = array( 
                    1 => "Vous n'avez pas envoy&eacute; une photo de vous tenant pr&egrave;s de votre visage une feuille de papier sur laquelle vous avez &eacute;crit humpchies et la date du jour et le num&eacute;ro de t&eacute;l&eacute;phone ou la photo ne sont pas clairs.",
                    2 => "Les photos de votre galerie ne montrent pas votre visage et vous n'avez pas envoy&eacute; de version originale pour les photos dont le visage est visible",                   
    );
    
    public $keywords = ["id" =>"ad_id", "reason" => "reject_reason" , 'username' => "username"];
    
    public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count){
        
       
        
        $where = '';
        if(isset($filter['unblocked']) && $filter['unblocked'] == 1){
            $where .= " and ad_country_id NOT IN ( " . geo::getLockedSQL() . ")" ;    
        }
        
        $sql = '
           SELECT  SQL_CALC_FOUND_ROWS AV.id, AV.ad_id, A.title, username, AV.user_id, AV.verification_status, AV.verified_by, AV.verification_date, request_date, inform FROM ad_verification AV
            LEFT JOIN ads A ON AV.ad_id = A.id 
            LEFT JOIN users U ON AV.user_id = U.id 
           
            WHERE 1
        ';
        
      
        
        if ( strlen($filter['id']) ) {
            $where .= self::quote('AND AV.id = ?', $filter['id']);
        }
        
         if ( strlen($filter['ad_id']) ) {
            $where .= self::quote('AND AV.ad_id = ?', $filter['ad_id']);
        }
        
        if ( strlen($filter['user_id']) ) {
            $where .= self::quote('AND AV.user_id = ?', $filter['user_id']);
        }
        
        if ( strlen($filter['title']) ) {
            $where .= self::quote('AND A.title LIKE ?', '%'. $filter['title'] . '%');
        }
        
        if ($filter['status'] != '-2' && !empty($filter['status']) ) {
            $where .= self::quote('AND AV.verification_status = ?', $filter['status']);
        }

        if ( strlen($filter['from']) ) {
            $where .= self::quote(' AND request_date >= ? ', date("Y-m-d ", strtotime($filter['from'])));
        }

       if ( strlen($filter['to']) ) {
            $where .= self::quote(' AND request_date <= ? ', date("Y-m-d ", strtotime($filter['to'])));
        }


        unset($filter['filter_by']);
        unset($filter['date_from']);
            unset($filter['date_to']);
            unset($filter['one_pic']);
        unset($filter['no_pic']);
        // </editor-fold>
        
        $sql .= $where;
           
        $sqlCount = "SELECT FOUND_ROWS();";
        
        $sql .= '
            ORDER BY ' . $sort_field . ' ' . $sort_dir . '
            LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
        ';
        //echo($sql);   
        $results = parent::_fetchAll($sql);
        $count = $this->getAdapter()->fetchOne($sqlCount);
        
        return $results;
    }
    
    
    
    
    public static function getInfo($verification_id){
        $sql = "SELECT AV.id, AV.ad_id, AV.user_id, AV.verification_status, AV.verified_by, AV.verification_date, AV.request_date,  AV.images, 
                group_concat(CONCAT('img-',`hash`,'.',ext)) AS ad_images, (SELECT verification_date FROM ad_verification AV2 WHERE AV2.ad_id = AV.ad_id  AND verification_status = 'approved' AND `ignore` = 'no' ORDER BY id ASC LIMIT 1) as  previous
                FROM ad_verification AV 
                LEFT JOIN ads A ON  A.id = AV.ad_id
                LEFT JOIN ad_images AI ON AI.ad_id = AV.ad_id 
                WHERE AV.id = ?  
                GROUP BY AV.id";
                
       return parent::_fetchRow($sql, array($verification_id));
    }
     
    public static function getMailInfo($verification_id){
        $sql = "SELECT AV.id, AV.ad_id, AV.user_id, U.username, U.email,verification_status, reject_reason, U.language
                 FROM ad_verification AV 
                 LEFT JOIN users U ON  U.id = AV.user_id
                 WHERE AV.id = ? ";
                                
       return parent::_fetchRow($sql, array($verification_id));
    } 
    
    public static function setStatus($data, $id){
        
        return parent::getAdapter()->update("ad_verification", $data, parent::getAdapter()->quoteInto('id = ?', $id));

        //return true;
    }
    
    public static function ignoreAccepted($adID){
        $data = array();
        $data['ignore'] = 'yes';
        return parent::getAdapter()->update("ad_verification", $data, parent::getAdapter()->quoteInto('ad_id = ?', $adID));
    }
    
}
?>
