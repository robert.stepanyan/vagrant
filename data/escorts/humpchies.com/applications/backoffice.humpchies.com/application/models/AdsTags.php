<?php
class Model_AdsTags extends Cubix_Model
{
    protected $_table               = 'ads_tags';
    protected static $redisKey      = "TagsAd-%d-%s";
    
    
    
    
    
    // add tags
    public static function create($ad_id, $tags = []) {
        
        if(!empty($tags)) {
            
            foreach($tags as $tag_id) {
            
                if(!is_numeric($tag_id))
                    continue;
                
                $fields = [
                    'ad_id' => $ad_id,
                    'tag_id' => $tag_id,
                ];
                
                parent::getAdapter()->insert("ads_tags", $fields);    
            }
        }
        return true;
    }
    
    
    // edit
    //
    public static function edit($ad_id, $tags = []) {
        //var_dump($ad_id);return;
        // remove all existing tags and empty redis
        parent::getAdapter()->delete("ads_tags", "ad_id = {$ad_id}");
        
         Core_Redis::del(sprintf(self::$redisKey, $ad_id, 'en'));    
         Core_Redis::del(sprintf(self::$redisKey, $ad_id, 'fr'));    
        // recreate ad tags
        
        return self::create($ad_id, $tags);
        
    }
    
    
    
    
    
    
    public static function getAdTags($ad_id, $language = 'en') {
        
        $adTagsList = Core_Redis::get(sprintf(self::$redisKey, $ad_id, $language));
        $adTagsList = false;
        if($adTagsList){
            return unserialize($adTagsList);
        }

        $sql = 'SELECT tag_id, `'. $language .'` as name FROM ads_tags AT LEFT JOIN tags T ON T.id = AT.tag_id WHERE AT.ad_id = ?';
        $adTagsList = parent::_fetchAll($sql, array($ad_id));
        
        Core_Redis::set(sprintf(self::$redisKey, $ad_id, $language), serialize($adTagsList));   
     
        //var_dump($adTagsList);
        return $adTagsList;    
        
    }
    
    
    
    
    
    
    
    
    
    

    public function insert($data)
    {
        parent::getAdapter()->insert($this->_table, $data);

        return parent::getAdapter()->lastInsertId();
    }

    public function update($be_user_id, $data)
    {
        parent::getAdapter()->update($this->_table, $data, parent::getAdapter()->quoteInto('id = ?', $be_user_id));
    }

    

    public function remove($id)
    {
        parent::remove(self::quote('id = ?', $id));
    }

    public function toggle($id)
    {
        parent::getAdapter()->update($this->_table, array(
            'is_disabled' => new Zend_Db_Expr('NOT is_disabled')
        ), parent::quote('id = ?', $id));
    }
    
    
}
