<?php
class Model_BOUsers extends Cubix_Model
{
	protected $_table = 'backend_users';
	protected $_itemClass = 'Model_BOUserItem';

	public function get($id)
	{
		$sql = '
			SELECT
				bu.id, bu.first_name, bu.last_name, bu.username,
				bu.email, bu.skype, bu.type, bu.application_id,
				bu.is_disabled, bu.email_signature, bu.is_auto_approve_photos, bu.receive_emails
			FROM backend_users bu
			WHERE 1 AND bu.id = ?
		';
		
		return parent::_fetchRow($sql, array($id));
	}

	public function userNameExists($username)
	{
		$sql = '
			SELECT TRUE	FROM backend_users bu
			WHERE bu.username = ?
		';

		return parent::getAdapter()->fetchOne($sql, array($username));
	}

	public function insert($data)
	{
		parent::getAdapter()->insert($this->_table, $data);

		return parent::getAdapter()->lastInsertId();
	}

	public function update($be_user_id, $data)
	{
		parent::getAdapter()->update($this->_table, $data, parent::getAdapter()->quoteInto('id = ?', $be_user_id));
	}

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT
				bu.id, bu.first_name, bu.last_name, bu.username,
				bu.email, bu.skype, bu.type, bu.application_id,
				bu.is_disabled
			FROM backend_users bu			
			
			WHERE 1
		';
		
		$countSql = '
			SELECT COUNT(bu.id)
			FROM backend_users bu
			WHERE 1
		';
		$where = '';
		
		if ( strlen($filter['bu.username']) ) {
			$where .= self::quote('AND bu.username LIKE ?', $filter['bu.username'] . '%');
		}
		
		if ( strlen($filter['bu.id']) ) {
			$where .= self::quote('AND bu.id = ?', $filter['bu.id']);
		}
		
		/*if ( strlen($filter['u.last_ip']) ) {
			$where .= self::quote('AND u.last_ip LIKE ?', $filter['u.last_ip'] . '%');
		}
		*/
		if ( strlen($filter['bu.type']) ) {
			$where .= self::quote('AND bu.type = ?', $filter['bu.type']);
		}
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			GROUP BY bu.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = intval($this->getAdapter()->fetchOne($countSql));
		
		return parent::_fetchAll($sql);
	}



	public function remove($id)
	{
		parent::remove(self::quote('id = ?', $id));
	}

	public function toggle($id)
	{
		parent::getAdapter()->update($this->_table, array(
			'is_disabled' => new Zend_Db_Expr('NOT is_disabled')
		), parent::quote('id = ?', $id));
	}
	
	public function getAllSales()
	{
		$bu_type = " bu.type = ? ";
		$arr = array(Cubix_Application::getId(), 'sales manager');
        		
		$sql = '
			SELECT
				bu.id, bu.first_name, bu.last_name, bu.username,
				bu.email, bu.skype, bu.type, bu.application_id
			FROM backend_users bu			
			WHERE ap.id = ? AND ' . $bu_type . ' AND is_disabled <> 1
		';
		
		return parent::_fetchAll($sql, $arr);
	}
	
	public function getAllBoUsers()
	{
		$sql = '
			SELECT
				bu.id, bu.first_name, bu.last_name, bu.username,
				bu.email, bu.skype, bu.type
			FROM backend_users bu			
			WHERE is_disabled <> 1
		';
		
		return parent::_fetchAll($sql);
	}
	
	public function getBoUsers()
	{
		$sql = '
			SELECT
				bu.id, bu.username
			FROM backend_users bu			
		';
		
		return parent::_fetchAll($sql);
	}
	
	public function getEmailById($id)
	{
		$sql = 'SELECT email FROM backend_users WHERE AND id = ?';
		return parent::getAdapter()->fetchOne($sql, array($id));
	}
}
