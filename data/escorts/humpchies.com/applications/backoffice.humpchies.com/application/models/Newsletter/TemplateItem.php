<?php

class Model_Newsletter_TemplateItem extends Cubix_Model_Item
{
	public function getVars()
	{
		$body = $this->getBody();
		$needle = '{EDITABLE_';
		$needle_end = '{/EDITABLE_';
				
		$vars = array();
		$data = array();

		preg_match_all("#" . $needle . "(.+?)}([\s\S]+?)" . $needle_end . "(.+?)}#", $body, $vars);

		if ( count($vars) > 0 && count($vars[0]) > 0 )
		{
			foreach($vars[1] as $i => $key)
			{
				$data[$key] = $vars[2][$i];
			}
		}

		unset($vars);

		return $data;
	}
	
	public function render($tpl_data, $lang_id = 'en')
	{
		$tpl_data = array_merge(array(
			'app_title' => 'Humpchies',
			'app_host' => 'humpchies.com',
			'app_url' => 'http://www.humpchies.com'
		), $tpl_data);
		
		$html = $this->{'body_html_' . $lang_id};
		
		foreach ( $tpl_data as $var => $value ) {
			$html = str_replace('%' . $var . '%', $value, $html);
		}
		
		return $html;
	}
}
