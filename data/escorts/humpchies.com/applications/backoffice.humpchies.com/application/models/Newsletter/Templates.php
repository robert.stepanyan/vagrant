<?php

class Model_Newsletter_Templates extends Cubix_Model
{
	protected $_table = 'newsletter_templates';
	protected $_itemClass = 'Model_Newsletter_TemplateItem';

    /**
     * @param $page
     * @param $per_page
     * @param $filter
     * @param $sort_field
     * @param $sort_dir
     * @param int $count
     * @return array|Model_Newsletter_TemplateItem[]
     */
	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count = 0)
	{
		$sql = '
			SELECT
				id, title, from_email, `subject`
			FROM ' . $this->_table . '
			WHERE 1
		';
		
		$countSql = '
			SELECT
				COUNT(id)
			FROM ' . $this->_table . '
			WHERE 1
		';
		
		$where = '';
		
		if ( $filter['application_id'] ) {
			$where .= self::quote(' AND application_id = ?', $filter['application_id']);
		}

		if ( $filter['is_v2'] == 1 ) {
			$where .= ' AND is_v2 = 1';
		}
		
		// <-- Construct WHERE clause here
		
		$sql .= $where;
		$countSql .= $where;
		
		$sql .= '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		
		$count = $this->getAdapter()->fetchOne($countSql);
		
		return parent::_fetchAll($sql);
	}

    /**
     * @param $id
     * @return Model_Newsletter_TemplateItem|array|null
     */
	public function getById($id)
	{
        $tpl = $this->_fetchRow('
			SELECT * FROM ' . $this->_table . ' WHERE id = ?
		', $id);
        if ($tpl) {
            $tpl->body = mb_convert_encoding($tpl->body, 'windows-1252', mb_detect_encoding($tpl->body));
            $tpl->bodyRaw = $tpl->body;
            $tpl->body_plain = mb_convert_encoding($tpl->body_plain, 'windows-1252', mb_detect_encoding($tpl->body_plain));
            return $tpl;
        }
        return null;
    }

    /**
     * @param $data
     * @return string|void
     * @throws Zend_Db_Adapter_Exception
     */
	public function save($data)
	{
		if (isset($data['id']) && $data['id'] != '')
		{
			$id = $data['id'];

			parent::getAdapter()->update($this->_table, array(
				'title' => $data['title'],
				'from_email' => $data['from_email'],
				'subject' => $data['subject'],
				'body' => $data['body'],
				'body_plain' => $data['body_plain']
			), parent::quote('id = ?', $id));
		}
		else
		{
			$i_data = array(
				'title' => $data['title'],
				'from_email' => $data['from_email'],
				'subject' => $data['subject'],
				'body' => $data['body'],
				'body_plain' => $data['body_plain'],
			);

			parent::getAdapter()->insert($this->_table, $i_data);

			$id = parent::getAdapter()->lastInsertId();
		}

		return $id;
	}

    /**
     * @param $id
     */
	public function remove($id)
	{
		parent::remove(parent::quote('id = ?', $id));
	}


    /**
     * @return array
     */
	public function getList()
	{
		return parent::_fetchAll('
			SELECT
				id, title 
			FROM ' . $this->_table . '
			ORDER BY id DESC
		');
	}

    /**
     * @param $title
     * @return bool
     */
	public function checkTitle($title)
	{
		$count = $this->getAdapter()->fetchOne('SELECT COUNT(*) FROM ' . $this->_table . ' WHERE title = ?', $title);

		if ($count > 0)
			return true;
		else
			return false;
	}

	public function makeAdvertisersList($ads_data)
	{
		$m_advertisements = new Model_Advertisements();

        $Advertisements = array();

		foreach ( $ads_data as $ad_data ) {
		    list($ad_id, $photo_id) = explode(":", $ad_data);

			$photo = Model_AdvertisementPhotos::getThumbWebDetails($photo_id);
            $advertisement = $m_advertisements->get($ad_id);
            $advertisement->photo = $photo;

            $Advertisements[] = $advertisement;
		}

		return $Advertisements;
	}


}
