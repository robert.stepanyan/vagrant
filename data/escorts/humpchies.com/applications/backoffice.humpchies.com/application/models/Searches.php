<?php
  class Model_Searches extends Cubix_Model
{
    protected $_table = 'searches';
    
    public function getAll($page, $per_page= 20, $filter, $sort_field, $sort_dir, &$count){
       
       if(!intval($per_page)){
           $per_page = 20;
       }
       $page = (intval($page))?$page:1;
       $sql = '
            SELECT SQL_CALC_FOUND_ROWS id, keyword, first_searched, results, mobile_searches, desktop_searches, total_searches, redirect
            FROM searches
            WHERE 1
       ';
        // <editor-fold defaultstate="collapsed" desc="Filtration by Date">
       
       if ( strlen($filter['keyword']) ) {
            $where .= ' AND keyword  like \'%'.$filter['keyword'].'%\'';
       } 
       
       if ( intval($filter['results_less']) ) {
            $where .= ' AND results <= '. intval($filter['results_less']);
       }
 
       if ( intval($filter['results_more']) ) {
            $where .= ' AND results >= '. intval($filter['results_more']);
       }
       if ( intval($filter['searches_less']) ) {
            $where .= ' AND total_searches <= '. intval($filter['searches_less']);
       }
 
       if ( intval($filter['searches_more']) ) {
            $where .=' AND total_searches >='. intval($filter['searches_more']);
       }
 
       unset($filter['filter_by']);
       unset($filter['date_from']);
       unset($filter['date_to']);
       unset($filter['one_pic']);
       unset($filter['no_pic']);
        // </editor-fold>
        
       $sql .= $where;
       // die($sql);
       $sqlCount = "SELECT FOUND_ROWS();";
        
       $sql .= '
            ORDER BY ' . $sort_field . ' ' . $sort_dir . '
            LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
        ';
       //die($sql);
        $results = parent::_fetchAll($sql);
        $count = $this->getAdapter()->fetchOne($sqlCount);
        
        return $results;
    }
    
    
    public function getById($id) {
        $sql = 'SELECT * FROM searches WHERE id = ?';

        return parent::_fetchRow($sql, array($id));
    }
    
    
    public function save($data)
    {
        if (isset($data['id']) && $data['id'] != '')
        {
            $fields = array(
                'redirect' => $data['redirect'],
                'admin_id' => $data["admin_id"]
            );                

            parent::getAdapter()->update($this->_table, $fields, parent::quote('id = ?', $data['id']));
        }
    }
}
?>
