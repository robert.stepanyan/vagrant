<?php

class Model_AdvertisementPhotos extends Cubix_Model{
    protected $_table = 'ad_images';

    const AD_THUMB_HEIGHT_PORTRAIT                  = 180;
    const AD_THUMB_HEIGHT_LANDSCAPE                 = 80;
    const AD_THUMB_HEIGHT_SQUARED                   = 120;
    const AD_THUMB_WIDTH                            = 120;
    const AD_IMAGE_HEIGHT_PORTRAIT                  = 600;
    const AD_IMAGE_HEIGHT_LANDSCAPE                 = 600;
    const AD_IMAGE_HEIGHT_SQUARED                   = 400;
    const AD_IMAGE_WIDTH                            = 600;

    const ORIENTATION_LANDSCAPE                     = 'L';
    const ORIENTATION_PORTRAIT                      = 'P';
    const ORIENTATION_SQUARE                        = 'S';

    const ERROR_NO_IMAGES_SAVED                     = 'AdvertisementPhotos.ErrorNoImagesSaved';
    const ERROR_SOME_IMAGES_COULD_NOT_BE_SAVED      = 'AdvertisementPhotos.ErrorSomeImagesCouldNotBeSaved';
    const NO_ERROR                                  = 'AdvertisementPhotos.NoError';

    const AD_PHOTO_TYPE_PUBLIC                      = 1;

    const AD_PHOTO_PORTRAIT                         = 1;
    const AD_PHOTO_LANDSCAPE                        = 2;
    const AD_PHOTO_SQUARE                           = 3;

    public static $AD_PHOTO_ORIENTATIONS = array(
        self::AD_PHOTO_PORTRAIT => 'P',
        self::AD_PHOTO_LANDSCAPE => 'L',
        self::AD_PHOTO_SQUARE => 'S',
    );

    public static function tableName() {
        return "ad_images";
    }
    public function getMainThumbWebDetails(&$ad_record)
    {
        $image = self::getMainImage($ad_record['id']);
        if (empty($image)){
            return array(
                'src'      => 'https://gui.humpchies.com/gui_ads/filler.jpg',
                'height'   => self::getImageHeight(self::AD_PHOTO_LANDSCAPE, true),
                'width'    => self::AD_THUMB_WIDTH
            );
        }
        return array(
            'src'      => self::getImagePath((array)$image, true, true),
            'height'   => self::getImageHeight(self::$AD_PHOTO_ORIENTATIONS[$image['orientation']], true),
            'width'    => self::AD_THUMB_WIDTH
        );
    }

    /**
     * @param $ad_id int
     * @return array
     */
    public static function getMainImage($ad_id) {
        return self::getAdapter()->fetchRow(
            'SELECT * FROM ' . self::tableName() . ' img WHERE img.is_main = 1 AND img.ad_id = ?',
            array($ad_id), Zend_Db::FETCH_ASSOC);
    }

    public static function get($id) {
        return self::getAdapter()->fetchRow(
            'SELECT * FROM ' . self::tableName() . ' img WHERE img.id = ?',
            array($id), Zend_Db::FETCH_ASSOC);
    }

    public static function getThumbWebDetails($id) {
        $image = self::get($id);
        if (empty($image)){
            return array(
                'src'      => 'https://gui.humpchies.com/gui_ads/filler.jpg',
                'height'   => self::getImageHeight(self::AD_PHOTO_LANDSCAPE, true),
                'width'    => self::AD_THUMB_WIDTH
            );
        }
        return array(
            'src'      => self::getImagePath((array)$image, true, true),
            'height'   => self::getImageHeight(self::$AD_PHOTO_ORIENTATIONS[$image['orientation']], true),
            'width'    => self::AD_THUMB_WIDTH
        );
    }

    public static function getMaxOrdering($ad_id) {
        $ordering = self::getAdapter()->fetchOne(
            'SELECT IFNULL(max(ordering), 0) as `ordering` FROM ' . self::tableName() . ' img WHERE img.ad_id = ?',
            array($ad_id));
        return $ordering;
    }

    public static function getImageHeight($image_orientation, $is_thumb = FALSE)
    {
        if($is_thumb)
            return ($image_orientation === Cubix_Images::ORIENTATION_PORTRAIT ? self::AD_THUMB_HEIGHT_PORTRAIT : ($image_orientation === Cubix_Images::ORIENTATION_LANDSCAPE ? self::AD_THUMB_HEIGHT_LANDSCAPE : self::AD_THUMB_HEIGHT_SQUARED));

        return ($image_orientation === Cubix_Images::ORIENTATION_PORTRAIT ?  self::AD_IMAGE_HEIGHT_PORTRAIT : ($image_orientation === Cubix_Images::ORIENTATION_LANDSCAPE ? self::AD_IMAGE_HEIGHT_LANDSCAPE : self::AD_IMAGE_HEIGHT_SQUARED));
    }
    /**
     * @param $ad_id int
     * @return array
     */
    public static function getImages($ad_id) {
        //return  self::getAdapter()->fetchAll("SELECT * FROM ad_images img WHERE img.ad_id = ? order by ordering asc, id asc", array($ad_id));
        return  self::getAdapter()->fetchAll("select id, ad_id, gallery_id, hash, ext, `type`, `status`, is_main, ordering, args, _inter_index, is_approved, 
                                double_approved, orientation, width, height, retouch_status, creation_date, is_rotatable, is_suspicious, is_popunder, 
                                popunder_args, retouch_date, is_edited, '-' as delete_date  FROM ad_images WHERE ad_id = ?
                                UNION
                                select id, ad_id, gallery_id, hash, ext, `type`, `status`, is_main, ordering, args, _inter_index, is_approved, 
                                double_approved, orientation, width, height, retouch_status, creation_date, is_rotatable, is_suspicious, is_popunder, 
                                popunder_args, retouch_date, is_edited, delete_date  FROM ad_images_deleted WHERE ad_id = ? ", array($ad_id, $ad_id));
    }   
    
    
    public static function getCountImages($ad_id) {
        //return  self::getAdapter()->fetchAll("SELECT * FROM ad_images img WHERE img.ad_id = ? order by ordering asc, id asc", array($ad_id));
        return  self::getAdapter()->fetchOne("SELECT COUNT(*)  FROM ad_images WHERE ad_id = ?", array($ad_id));
    } 
      
    
    public static function getCurrentImages($ad_id){
       return  self::getAdapter()->fetchAll("select id, ad_id, gallery_id, hash, ext, `type`, `status`, is_main, ordering, args, _inter_index, is_approved, 
                                double_approved, orientation, width, height, retouch_status, creation_date, is_rotatable, is_suspicious, is_popunder, 
                                popunder_args, retouch_date, is_edited, '-' as delete_date  FROM ad_images WHERE ad_id = ?
                                ", array($ad_id)); 
    }

    public static function getDeletedImages($ad_id){
       return  self::getAdapter()->fetchAll("select id, ad_id, gallery_id, hash, ext, `type`, `status`, is_main, ordering, args, _inter_index, is_approved, 
                                double_approved, orientation, width, height, retouch_status, creation_date, is_rotatable, is_suspicious, is_popunder, 
                                popunder_args, retouch_date, is_edited, delete_date  FROM ad_images_deleted WHERE ad_id = ?
                                ", array($ad_id)); 
    }

    public static function getImagesWebDetails($ad_record)
    {
        $result =array();
        $images = self::getImages($ad_record['id']);

        foreach($images as $image)
        {
            $imgDetails = self::toJSON((array)$image);
            array_push($result, $imgDetails);
        }
        return $result;
    }   



    public static function reorder($imageIds) {
        foreach ($imageIds as $i=> $imageId){
            self::getAdapter()->update( self::tableName(), array('ordering'=>$i+1), "id = {$imageId}");
        }
        return true;
    }

    public static function setMain($id, $ad_id) {
        if (is_array($id)){
            $id = array_shift($id);
        }
        if ($id === null){

            return self::getAdapter()->query("Update ad_images set is_main = 1 where ad_id = ? ORDER BY id ASC LIMIT 1", array($ad_id));
        }
        self::getAdapter()->update( self::tableName(),array("is_main" => 0), new Zend_Db_Expr("ad_id = {$ad_id}"));
        return self::getAdapter()->update( self::tableName(),array("is_main" => 1), new Zend_Db_Expr("id = {$id}"));
    }
    public static function updateRotate($id, $width, $height, $ar, $hash) {
       
        self::getAdapter()->update( self::tableName(),array("orientation" => $ar, "width" => $width, "height" => $height, 'hash' => $hash), new Zend_Db_Expr("id = {$id}"));
        
    }

    /**
     * @param $ids
     * @param $ad_id
     * @return bool|int
     */
    public static function delete($ids, $ad_id) {
        if (empty($ids)){
            return false;
        }
        if (!is_array($ids) && ( (int)$ids == $ids)){
            return self::deleteOne($ids, $ad_id);
        }
        elseif (is_array($ids)){
            foreach ($ids as $id) {
                self::deleteOne($id, $ad_id);
            }
            //check if main image removed
            $mainImage = self::getMainImage($ad_id);
            if (empty($mainImage)){
                self::setMain(null, $ad_id);
            }
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @param $ad_id
     * @return bool|int
     */
    public static function deleteOne($id,$ad_id) {
        if ((int)$id != $id){
            return false;
        }
        $image = self::getAdapter()->fetchRow( "select * from ad_images where id = ?", array($id), Zend_Db::FETCH_ASSOC);
        if (empty($image)){
            return false;
        }

        if (empty($image['ad_id']) || (int)$image['ad_id'] !== (int)$ad_id){
            return false;
        }

        $imageInfo = self::getImagePath($image, false);
        $imageInfoThumb = self::getImagePath($image, true);
        unlink($imageInfo['path']);
        unlink($imageInfoThumb['path']);
        return self::getAdapter()->delete( self::tableName(), new Zend_Db_Expr("id = {$id}"));
    }

    public static function getImagePath($photo, $is_thumb = false, $is_web = false)
    {
        $category = $photo['ad_id'];
        $image_configs = Zend_Registry::get('images_config');

        $ads_path  = $image_configs['path'] ;
        if (strlen($category) > 2) {
            $category = substr($category, 0, 2) . DIRECTORY_SEPARATOR . substr($category, 2). DIRECTORY_SEPARATOR;
        }

        // TODO ad id 
        $image['delete_date'] = $photo['delete_date'];
        $image['is_new'] = $photo['is_edited'];
        $image['hash'] = empty($photo['hash']) ? md5(microtime(true)) : $photo['hash'];
        $image['ext'] = $photo['ext'];
        $image['path'] =
            (
            $is_web === true ? 
                "https://".
                $image_configs['gui_ads_images']
                : $ads_path
            ) .'/'.
            $category .
            ($is_thumb === true ? 'thumb' : 'img')

            . '-' . $image['hash'] . '.'. $image['ext'];
        return $image;
    }

    /**
     * @param $ad_fields
     * @param $posted_images
     * @return string|array
     */
    public static function savePostedImages($ad_id, $posted_images)
    {
        $return_values = NULL;
        $thumb_md5 = NULL;
        $mainImg = self::getMainImage($ad_id);
        $ordering = self::getMaxOrdering($ad_id);
        $is_main = (int) empty($mainImg);
        foreach($posted_images as $i => $image_details)
        {
            $ext = strtolower(pathinfo($image_details['tmp_name'], PATHINFO_EXTENSION));
            $image_destination_path_info         = self::getImagePath(array(
                'ad_id'=>$ad_id,
                'ext'=>$ext,
            ), false, false);
            $image_thumb_destination_path_info   = self::getImagePath(array(
                    'ad_id'=>$ad_id,
                    'ext'=>$ext,
                    "name" =>$image_destination_path_info['hash']
                ),true, false);

            $image_destination_path = $image_destination_path_info['path'];
            $image_thumb_destination_path = $image_thumb_destination_path_info['path'];

            $x = (Cubix_Images::resampleJPEGImage($image_details['tmp_name'], $image_destination_path, self::AD_IMAGE_WIDTH, true) === true ? 1 : 0);
            $y = (Cubix_Images::resampleJPEGImage($image_details['tmp_name'], $image_thumb_destination_path, self::AD_THUMB_WIDTH, true) === true ? 2: 0);

            switch($x + $y)
            {
                case 3:
                    $return_values[$i]    = Cubix_Images::getImageOrientation($image_details['tmp_name']);

                    if($is_main && $thumb_md5 === NULL)
                    {
                        $return_values[$i] .= ':T';
                        $thumb_md5 = md5(file_get_contents($image_thumb_destination_path));
                        $is_main = 0;
                    }
                    $orientation = Cubix_Images::getImageOrientation($image_details['tmp_name']);
                    $reverse_orientations = array_flip(self::$AD_PHOTO_ORIENTATIONS);
                    list($image_width, $image_height) = @getimagesize($image_details['tmp_name']);
                    $fields = array(
                        'ad_id'=>$ad_id,
                        'hash'=>$image_destination_path_info['hash'],
                        'ext'=>$image_destination_path_info['ext'],
                        'type' => self::AD_PHOTO_TYPE_PUBLIC,
                        'orientation' => $reverse_orientations[$orientation],
                        'is_main' => $is_main,
                        'ordering' => $ordering + $i + 1, //continue ordering
                        'width' => $image_width,
                        'height' => $image_height,
                        'creation_date' => date('Y-m-d H:i:s', time())
                    );
                    self::getAdapter()->insert(
                        self::tableName(),
                        $fields
                    );
                    ;
                    $fields['id'] = self::getAdapter()->lastInsertId();
                    break;
                default:
                    if (is_file($image_destination_path)){
                        unlink($image_destination_path);
                    }
                    if (is_file($image_thumb_destination_path)){
                        unlink($image_thumb_destination_path);
                    }
                    return self::ERROR_SOME_IMAGES_COULD_NOT_BE_SAVED;
                    break;
            }
        }

        return self::toJSON($fields);
    }

    public static function canProcessImages($photoIds, $ad_id) {
        if (!is_array($photoIds)){
            $photoIds = array($photoIds);
        }
        $sql = "
        SELECT
			COUNT(DISTINCT id) = ".count($photoIds)." as `canProcess`
		FROM
			ad_images
		WHERE
			id IN (". implode(",", $photoIds). ")
		AND ad_id = ".$ad_id."
        ";
        return !!self::getAdapter()->fetchOne($sql);

    }

    public static function toJSON($photo)
    {
        $data = array(
            'id' => $photo['id'],
            'escort_id' => $photo['ad_id'],
            'image_url' =>self::getImagePath(array(
                "ad_id"=>$photo['ad_id'],
                "hash"=>$photo['hash'],
                "ext"=>$photo['ext'],
            ), true, true)['path'] ,
            'is_main' => $photo['is_main'],
            'type' => $photo['type'],
            'is_verified' => $photo['status'] == 1,
            'status' => $photo['status'],
            'args' => ($args = unserialize($photo['args'])) ? $args : array('x' => 0, 'y' => 0),
            'is_approved' => 1,
            'double_approved' => $photo['double_approved'],
            'orig_url' => self::getImagePath(array(
                "ad_id"=>$photo['ad_id'],
                "hash"=>$photo['hash'],
                "ext"=>$photo['ext'],
            ),false, true)['path'],
            'gotd' => 0,
            'profile_boost' => 0,
            'retouch_status' => $photo['retouch_status'],
            'is_rotatable' => $photo['is_rotatable'],
            'is_portrait' => !empty($photo['is_portrait']) ? $photo['is_portrait'] : $photo['orientation'] === self::AD_PHOTO_PORTRAIT,
            'is_suspicious' => $photo['is_suspicious'],
            'is_popunder' => (isset($photo['is_popunder'])) ? $photo['is_popunder'] : 0,
            'width' =>  $photo['width'],
            'height' => $photo['height'],
            'hash' => $photo['hash'],
            'ext' => $photo['ext'],
            'delete_date' => $photo['delete_date'],
            'is_edited' => $photo['is_edited'],
        );

        if($photo['retouch_status'] ==2){
            $data['image_url'] .=  '?'.time();
        }
        return $data;
    }

    public function approveRevision($id)
    {
        self::getAdapter()->update( self::tableName(), array('is_edited' => 0), "ad_id = {$id}");
    }
    
    public function getImagesbyIds($images){
            //SELECT id, gallery_id, hash, ext FROM ad_images WHERE id IN (411595, 411596)
           
            return  self::getAdapter()->fetchAll("SELECT id, gallery_id, hash, ext, ad_id FROM ad_images WHERE id IN (" . implode(",", $images) . ") ");
    }
    
    public static function checkMain($ad_id) {
        
        // get all images
        $images = self::getCurrentImages($ad_id);
        //var_dump($images);
        
        $active_main = array_filter($images, function($v, $k) {
            return $v->is_main == 1;
            }, ARRAY_FILTER_USE_BOTH);
        
        if(!empty($active_main)) {
            return true;
        }
        
        // set main pic
        return self::getAdapter()->update( self::tableName(),array("is_main" => 1), new Zend_Db_Expr("id = {$images[0]->id}"));
        
    }
    
    
    
}