<?php
class Model_Auth_Adapter implements Zend_Auth_Adapter_Interface
{
	public function __construct($username, $password)
	{
		$this->username = $username;
		$this->password = $password;
	}

	public function authenticate()
	{
		$db = Zend_Registry::get('db');

		$user = $db->fetchRow('
			SELECT *
			FROM backend_users
			WHERE username = ? AND password = MD5(?) AND is_disabled = 0
		', array($this->username, $this->password));
		
		if ( $user ) {
			return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, new Model_UserItem($user));
		}
		else {
			return new Zend_Auth_Result(Zend_Auth_Result::FAILURE, null);
		}
	}

	protected function getSaltByUsername($username)
	{
		$db = Zend_Registry::get('db');

		$salt = $db->fetchOne('
			SELECT salt_hash
			FROM users
			WHERE username = ?
		', array($username));

		if ( ! $salt ) {
			return null;
		}

		return $salt;
	}
}
