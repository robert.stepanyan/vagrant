<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	public function run()
	{
		// Zend_Layout::startMvc();
		
		parent::run();
	}
	
	protected function _initRoutes()
	{
        require('../application/models/Plugin/Auth.php');
        require('../library/redis.php');
		require('../library/mylogs.php');
        require_once("../library/geo.php");
        require_once("../library/google_analytics.php");
		require('../application/models/Plugin/Filter.php');
		$this->bootstrap('frontController');
		$front = $this->getResource('frontController');
		$router = $front->getRouter();
		
		$router->addRoute(
			'dashboard',
			new Zend_Controller_Router_Route(
				'',
				array(
					'module' => 'default',
					'controller' => 'dashboard',
					'action' => 'index'
				)
			)
		);
		
		$router->addRoute(
			'dublicities-view',
			new Zend_Controller_Router_Route(
				'dashboard/dublicities/view',
				array(
					'module' => 'default',
					'controller' => 'dashboard',
					'action' => 'dublicities-view'
				)
			)
		);
		
		$router->addRoute(
			'external-link',
			new Zend_Controller_Router_Route(
				'go',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'go'
				)
			)
		);
	}
	
	protected function _initConfig()
	{
		Zend_Registry::set('system_config', $this->getOption('system'));
		Zend_Registry::set('db_connection', $this->getOption('resources'));
		Zend_Registry::set('escorts_config', $this->getOption('escorts'));
		Zend_Registry::set('statistics', $this->getOption('statistics'));
	}
	
	protected function _initAutoload()
	{
		$moduleLoader = new Zend_Application_Module_Autoloader(array(
			'namespace' => '',
			'basePath' => APPLICATION_PATH
		));

		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('Cubix_');

		return $moduleLoader;
	}
	
	protected function _initDatabase()
	{
		$this->bootstrap('db');
		$db = $this->getResource('db');
		$db->setFetchMode(Zend_Db::FETCH_OBJ);

		Zend_Registry::set('db', $db);
		Cubix_Model::setAdapter($db);

		$db->query('SET NAMES `utf8`');
	}
	
	protected function _initViewHelpers()
	{
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();

		$view->doctype('XHTML1_STRICT');

		$view->headMeta()
			->appendHttpEquiv('Content-Type', 'text/html; charset=windows-1252')
			->appendHttpEquiv('Cache-Control', 'no-cache');
		$view->headTitle()->setSeparator(' - ');
		$view->headTitle('Humpchies Backoffice');
	}
	
	
	
	protected function _initStorage()
	{
		Zend_Registry::set('images_config', $this->getOption('images'));
	}

	protected function _initCache()
	{
		$cache_configs = $this->getOption('cache');
		
		$frontendOptions = array(
			'lifetime' => null,
			'automatic_serialization' => true
		);
		
		$backendOptions = array(
			'servers' => array(
				array(
					'host' => $cache_configs['host'],
					'port' => $cache_configs['port'],
					'persistent' =>  true
				)
			),
		);

		//if (defined('APPLICATION_ENV') && (APPLICATION_ENV == 'staging' || APPLICATION_ENV == 'development' ||  APPLICATION_ENV == 'production' ) ) {
			$cache = Zend_Cache::factory('Core', 'Blackhole', array('automatic_serialization' => true));
			//var_dump($cache);die
		//} else {
			//$cache = Zend_Cache::factory('Core', 'Blackhole', array('automatic_serialization' => true));
			//$cache = Zend_Cache::factory(new Zend_Cache_Core($frontendOptions), new Zend_Cache_Backend_Memcached($backendOptions), $frontendOptions);
		//}

		Zend_Registry::set('cache', $cache);
		
	}
	
	protected function _initHooks()
	{
		$hooker = new Cubix_Hooker();
		$hooker->addHook(new Model_Billing_Hook());
		
		Zend_Registry::set('BillingHooker', $hooker);
	}
	
}
