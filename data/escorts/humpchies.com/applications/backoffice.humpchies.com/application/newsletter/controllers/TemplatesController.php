<?php

class Newsletter_TemplatesController extends Zend_Controller_Action
{
    /* @var $_model Model_Newsletter_Templates */
	protected $_model;
	/* @var $_interval string */
	protected $_interval;

	/* @var $_request Zend_Controller_Request_Http */
    protected $_request = null;
	
	const MAIL_SERVER_HOST = "smtp-hmpchs.newsmanapp.com";
	const MAIL_SERVER_PORT = 25;
	const MAIL_SERVER_AUTH = true;
	const MAIL_SERVER_USER = "hmpchs_comacc@tmail.humpchies.com";
	const MAIL_SERVER_PASS = "HmpC%28UqnZo384#d";
	const MAIL_FROM = "info@humpchies.com";
	const MAIL_FROM_NAME = "humpchies.com";
	
	public function init()
	{
		$this->_model = new Model_Newsletter_Templates();
		$this->_interval = '-140 days';
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$data = $this->_model->getAll(
			$this->_getParam('page'),
			$this->_getParam('per_page'),
			array(),
			$this->_getParam('sort_field'),
			$this->_getParam('sort_dir'),
			$count
		);

		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'title' => '',
				'subject' => '',
				'from_email' => '',
				'body' => '',
				'body_plain' => '',
				'application_id' => 'int'
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['title']) ) {
				$validator->setError('title', 'Required');
			}
			elseif ($this->_model->checkTitle($data['title']))
			{
				$validator->setError('title', 'Title exists, please choose another one');
			}
			
			if ( ! strlen($data['from_email']) ) {
				$validator->setError('from_email', 'Required');
			}

			if ( ! strlen($data['body']) ) {
				$validator->setError('body', 'Required');
			}
			
			if ( $validator->isValid() ) 
			{
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_getParam('id'));
		
		$this->view->item = $template = $this->_model->getById($id);

		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'title' => '',
				'subject' => '',
				'from_email' => '',
				'body' => '',
				'body_plain' => '',
				'application_id' => 'int'
			));
			$data = $data->getData();
			$validator = new Cubix_Validator();

			if ( ! strlen($data['title']) ) {
				$validator->setError('title', 'Required');
			}
			elseif ($this->_model->checkTitle($data['title']) && ($template->title != $data['title']))
			{
				$validator->setError('title', 'Title exists, please choose another one');
			}

			if ( ! strlen($data['from_email']) ) {
				$validator->setError('from_email', 'Required');
			}

			if ( ! strlen($data['body']) ) {
				$validator->setError('body', 'Required');
			}

			if ( $validator->isValid() )
			{
				$data['body'] = $validator->removeEmoji($data['body']);
				$this->_model->save($data);
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function deleteAction()
	{
		$id = intval($this->_getParam('id'));
		if ( ! $id ) die;
		$this->_model->remove($id);

		die;
	}
	
	/* ****************************************************** */
	public function sendAction()
	{
	    $system_config = Zend_Registry::get('system_config');

		$this->view->templates = $this->_model->getList();
		
		if ($this->_getParam('send_newsletter') == 1)
		{
			$template_id = intval($this->_getParam('template_id'));
			$members = $this->_getParam('members');
			$groups = $this->_getParam('groups');
			$no_group = $this->_getParam('no_group');
			$from_email = trim($this->_getParam('from_email', self::MAIL_FROM));
			$from_name = trim($this->_getParam('from_name', self::MAIL_FROM_NAME));
			$subject = trim($this->_getParam('subject'));
			$vars = $this->_getParam('vars');
			$send_time = $this->_getParam('send_time');
			$ads_data = $this->_getParam('ads');
			$assign_plain = $this->_getParam('assign_plain');

			$validator = new Cubix_Validator();

			if ( $send_time == 'custom' && ! strlen($this->_getParam('custom_time')) ) {
				$validator->setError('custom_time', 'Custom Time Required');
			}

			if ( ! strlen($from_name) ) {
				$validator->setError('from_name', 'Required');
			}

			if ( ! strlen($from_email) ) {
				$validator->setError('from_email', 'Required');
			}

			if ( ! strlen($subject) ) {
				$validator->setError('subject', 'Required');
			}


			if ( ! isset($groups) && !isset($members) && !isset($no_group) ) {
				$validator->setError('group_list', 'Required');
			}

			if ( $validator->isValid() ) {
				$tpl = $this->_model->getById($template_id);

				//Replace html with template variables
				//Ads List
				if ($ads_data)
				{
					$ads = $this->_model->makeAdvertisersList($ads_data);
					$html = $this->view->render('templates/tpl-advertisements.phtml');
					$tpl->body = str_replace('{ADS_LIST}', $html, $tpl->body);
					
					if ( $assign_plain ) {
						$url = $system_config['www_url'];
						$ads_urls = "";
						foreach ( $ads as $ad ) {
                            $ads_urls .= $url . Model_Advertisements::getSetCardUrl($ad) . "\n\r";
						}
						$tpl->body_plain = str_replace('{ADS_LIST}', $ads_urls, $tpl->body_plain);
					}
				}

				//Template Vars
				if ($vars)
				{
					foreach ($vars as $k => $var)
					{
						//$tpl->body = preg_replace("#({EDITABLE_$k})([\s\S]+?)({/EDITABLE_$k})#", '$1' . $var . '$3', $tpl->body);
						$tpl->body = preg_replace("#({EDITABLE_$k})([\s\S]+?)({/EDITABLE_$k})#", $var, $tpl->body);
						$tpl->body_plain = preg_replace("#({EDITABLE_$k})([\s\S]+?)({/EDITABLE_$k})#", strip_tags($var), $tpl->body_plain);
					}
				}

                $config = array(
                    'auth' => 'login',
                    'port' => self::MAIL_SERVER_PORT,
                    'ssl' => 'tls',
                    'username' => self::MAIL_SERVER_USER,
                    'password' => self::MAIL_SERVER_PASS
                );

                try {
                    $tr = new Zend_Mail_Transport_Smtp(self::MAIL_SERVER_HOST, $config);
                    Zend_Mail::setDefaultTransport($tr);
                    $mail = new Zend_Mail('UTF-8');

                    if ($assign_plain) {
                        $mail->setBodyText($tpl->body_plain);
                    }

                    $mail->setBodyHtml($tpl->body);

                    $email_map = array(
                        //Test: updated
                        1 => 'ninit-161-d9c4081f94f6495da05053d10d8e247a@humpchiesmail.com',
                        //Members:
                        2 => 'ninit-161-294b265330c5fc2224d30c6cdeaa57fa@humpchiesmail.com',
                        //Advertisers:
                        3 => 'ninit-161-fd55ed1d9bf9c8c1a82aa34dd871af2f@humpchiesmail.com',

                        4 => 'ninit-161-4cfab8854cd1b4c02f4f05a38a49b792@humpchiesmail.com',

                        5 => 'ninit-161-d84c7ae743b170c21c92c37f3eb471c0@humpchiesmail.com',
                    );

                    foreach ($groups as $k => $group) {
                        if ($k == 0) {
                            $mail->addTo($email_map[$group]);
                        }
                        else {
                            $mail->addCc($email_map[$group]);
                        }
                    }


                    $mail->setSubject($subject);

                    $mail->setFrom($from_email, $from_name);

                    $mail->send();
                } catch (Zend_Exception $exception) {
                    var_dump($exception);
                    die(json_encode(array(
                        'status' => 'error',
                        'msgs' => array(
                            'subject' => $exception->getMessage()
                        )
                    )));
                }
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function loadAction()
	{
		$tpl_id = intval($this->_getParam('template_id'));
		if ( ! $tpl_id ) die;
		
		$tpl = $this->_model->getById($tpl_id);
		$tpl->from_name = '';

		$this->view->vars = $tpl->getVars();
		$this->view->tpl = $tpl;

	}
	
	public function advertisementPickerAction()
	{
		$m = new Model_Advertisements();

		$filter =  array(
			'filter_by' => 'released_date',
			'date_from' => strtotime($this->_interval),
		);
		
		$count = 0;
		$ads = $m->getAll(1, 1000, $filter, 'date_released', 'DESC', $count);
		$this->view->assign('count', $count);
		$this->view->assign('ads', $ads);
	}
	
	public function previewAction()
	{
        $system_config = Zend_Registry::get('system_config');
		$this->view->layout()->disableLayout();
		
		$template_id = intval($this->_getParam('template_id'));
		
		if ( ! $template_id ) {
			return;
		}

		$this->view->url = $system_config['www_url'];

		$ads_data = (array) $this->_getParam('ads', array());
		$Ads = $this->_model->makeAdvertisersList($ads_data);

		$this->view->ads = $Ads;
		$html = $this->view->render('templates/tpl-advertisements.phtml');
		
		$tpl = $this->_model->getById($template_id);
		
		$tpl->body = str_replace('{ADS_LIST}', $html, $tpl->body);

		$vars = $this->_getParam('vars');
		//Template Vars
		if ($vars)
		{
			foreach ($vars as $k => $var)
			{
				$tpl->body = preg_replace("#{EDITABLE_$k}([\s\S]+?){/EDITABLE_$k}#", $var, $tpl->body);
			}
		}
				
		die($tpl->body);
	}
}
