<?php

define('IS_CLI', 1);
define('IN_BACKEND', 1);

chdir(dirname(__FILE__) . '/../public');


defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

try {
	require_once 'Zend/Console/Getopt.php';
    $opts = new Zend_Console_Getopt(array(
		'help|h'		=> 'Displays usage information.',
		'action|a=s'	=> 'Action to perform in format of module.controller.action',
		'host'			=> 'Hostname to immitate',
		'application|p-i' => 'Application id.',
		'debug|d'		=> 'Debug mode',
		'staging|s'		=> 'Stating mode',
		'video_id|v-i'=> 'video_id for converting',
	));

    $opts->parse();
}
catch ( Zend_Console_Getopt_Exception $e ) {
    echo($e->getMessage() ."\n\n". $e->getUsageMessage());
	exit;
}

if ( isset($opts->h) ) {
    echo $opts->getUsageMessage();
    exit;
}


if ( isset($opts->a) ) {
	if ( isset($opts->d) ) {
		define('APPLICATION_ENV', 'development');
	}
	elseif ( isset($opts->s) ) {
		define('APPLICATION_ENV', 'staging');
	}
	
	defined('APPLICATION_ENV')
		|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

	require_once 'Zend/Application.php';

	$config_file = 'application.ini';
	
	$application = new Zend_Application(
		APPLICATION_ENV,
		APPLICATION_PATH . '/configs/' . $config_file
	);

	
	$_SERVER['HTTP_HOST'] = $opts->host;
	$_SERVER['REQUEST_URI'] = '';

	$application->bootstrap();

	$reqRoute = array_reverse(explode('.', $opts->a));
    @list($action, $controller, $module) = $reqRoute;

    $request = new Cubix_Controller_Request_Cli($action, $controller, $module, array(
		'lang_id' => 'en',
		'application_id' => $opts->p,
		'video_id'=>$opts->v,
	));

	$front = $application->getBootstrap()->getResource('frontController');

	$front->setRequest($request);
	$front->setRouter(new Cubix_Controller_Router_Cli());
    $front->setResponse(new Zend_Controller_Response_Cli());
    $front->throwExceptions(true);
	
	$application->run();
}

