<?php

class BumpSettingsController extends Zend_Controller_Action
{
   
	public function init()
	{
		$this->model = new Model_Bump();
		$this->bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($this->bu_user->type, array('superadmin', 'admin')))
			die('permission denied');
	}
	
	public function indexAction() 
	{
		
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_users_data');
		$this->view->filter_params = $ses_filter->filter_params;
		$this->view->bu_user = $this->bu_user;
		
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
			'id' => $req->id,
			'user_id' => $req->user_id,
			'status' => $req->status
		);

		$data = $this->model->getAllSettings(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		foreach ( $data as $i => $item ) {
			$data[$i]->interval = $item->interval_sec / 60;
			$data[$i]->periode =  $item->periode_sec / 3600;
			$data[$i]->status = Model_Bump::$STATUSES[$item->status];
			$data[$i]->bumps =  $item->bumps_count . ' / ' . $item->bumps_total;
		}

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function toggleActiveAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->toggle($id);
	}

	
}
