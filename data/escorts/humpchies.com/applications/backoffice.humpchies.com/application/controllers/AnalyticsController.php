<?php

class AnalyticsController extends Zend_Controller_Action
{
	public function init()
	{
		//$this->model = new Model_Searches();
		
		$this->bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($this->bu_user->type, array('superadmin', 'admin')))
			die('permission denied');
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_analytics_data');
		$this->view->filter_params = $ses_filter->filter_params;
		$this->view->bu_user = $this->bu_user;
		
		/* FILTER PARAMS */
        
	}
	
	public function dataAction() {
         
        $req = $this->_request;

		$filter = array(
            'date_to' => $this->_getParam('date_to'),
            'date_from' => $this->_getParam('date_from'),
		);
		
        $startDate = ($filter['date_from'] != '')? date('Y-m-d', $filter['date_from']) :  date('Y-m-d', strtotime("-30 days"));
        $endDate = ($filter['date_to'] != '')? date('Y-m-d', $filter['date_to']) : date('Y-m-d', strtotime("-1 days"));
        $data = []; 
        
        $ga = new GoogleAnalytics();
        $analytics = $ga->initializeAnalytics();
        
        $sessions_data = $ga->getSessionsCounts($analytics, $startDate, $endDate);
        if(!empty($sessions_data)) {
            $sessionsObj = new stdClass();
            $sessionsObj->label = "Total Sessions";  
            $sessionsObj->name = "Total Sessions";  
            $sessionsObj->value =  number_format(array_sum(array_column($sessions_data, 1)), 0, '.', ','); 
            $data[] = $sessionsObj;    
        }
        
        
        
        $filters_data =  $ga->getFiltersCounts($analytics, $startDate, $endDate);
       // var_dump($filters_data);exit();
        
        
        if(!empty($filters_data)) {
            
            foreach($filters_data as $filter) {
                
               $filterObj = new stdClass();
               $filterObj->label = $filter[0];  
               $filterObj->name = $filter[2];  
               $filterObj->value = number_format($filter[3], 0, '.', ','); 
               $data[] = $filterObj; 
            }
        }
        
        //var_dump($data);exit();
        
		//foreach ( $data as $i => $item ) {
//			$data[$i]->status = Model_Users::$statuses[$item->date_banned];
//			$data[$i]->type = ($data[$i]->type == Model_Users::USER_TYPE_MEMBER) ? 'Member' :'Advertisement';
//		}
//		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	
    public function editAction() {

        if (!$this->_request->isPost()) {

            $data = $this->model->getById($this->_getParam('id'));
            $this->view->data = $data;
            
        } else {

            $data = new Cubix_Form_Data($this->_request);
            
            $data->setFields(array(
                'id' => 'int',
                'redirect' => ''

            ));

            $data = $data->getData();
            
            $validator = new Cubix_Validator();
            
            if ($validator->isValid()) {
                $data["admin_id"] = $this->bu_user->id;
                $this->model->save($data);
            }

            die(json_encode($validator->getStatus()));
        }
    }
    
    
}
