<?php

class AdsLocationController extends Zend_Controller_Action
{
    public function init()
    {
        $this->model = new Model_AdsLocation();
        
        $this->bu_user = Zend_Auth::getInstance()->getIdentity();
        
        if (!in_array($this->bu_user->type, array('superadmin', 'admin')))
            die('permission denied');
    }
    
    public function indexAction() 
    {
       
        //$onlineBilling->markChargeBack();
        /* FILTER PARAMS */
        $ses_filter = new Zend_Session_Namespace('default_adslocation_data');
        $this->view->filter_params = $ses_filter->filter_params;
        $this->view->bu_user = $this->bu_user;
        
        /* FILTER PARAMS */
    }
    
    public function dataAction()
    {
        
        $req = $this->_request;

        $filter = array(
            'ad_id'         => $req->ad_id,
            'user_id'       => $req->user_id,
            'hide_map'      => $req->hide_map,

        );
        
        $data = $this->model->getAll(
            $this->_request->page,
            $this->_request->per_page, 
            $filter, 
            $this->_request->sort_field, 
            $this->_request->sort_dir, 
            $count
        );
        $this->view->data = array(
            'data' => $data,
            'count' => $count
        );
    }

    
    public function viewAction(){
        header('Content-Type: text/html; charset=utf-8');
        $req = $this->_request;
        $logs = $this->model->getLogs($req->id);
        $this->view->logs = $logs;
    }
    

}
