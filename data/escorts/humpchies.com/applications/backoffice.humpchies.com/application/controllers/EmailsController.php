<?php

class EmailsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Emails();

		$this->bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($this->bu_user->type, array('superadmin', 'admin')))
			die('permission denied');
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_emails_data');
		$this->view->filter_params = $ses_filter->filter_params;
		$this->view->bu_user = $this->bu_user;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$filter = array(
			'e.id' => $req->id,
			'e.email' => $req->email,
			'e.login_user_id' => $req->login_user_id
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);

	}
	
	public function removeAction()
	{
		/*$id = intval($this->_request->id);

		$this->model->remove($id);*/
		die;
	}
}
