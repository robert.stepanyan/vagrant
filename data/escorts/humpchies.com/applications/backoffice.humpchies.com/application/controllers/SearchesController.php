<?php

class SearchesController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Searches();
		
		$this->bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($this->bu_user->type, array('superadmin', 'admin')))
			die('permission denied');
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_users_data');
		$this->view->filter_params = $ses_filter->filter_params;
		$this->view->bu_user = $this->bu_user;
		
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		
		$req = $this->_request;

		$filter = array(
            'keyword' => $req->keyword,
            'results_less' => $req->results_less,
            'results_more' => $req->results_more,
            'searches_less' => $req->searches_less,
			'searches_more' => $req->searches_more,
//			'id' => $req->id,
//			'email' => $req->email,
//			'phone' => $req->phone,
//			'date_banned' => $req->date_banned,
//            'used_user_ids' => $req->used_user_ids,
//            'auto_approval' => $req->auto_approval,
//            'type'          => $req->user_type
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		//foreach ( $data as $i => $item ) {
//			$data[$i]->status = Model_Users::$statuses[$item->date_banned];
//			$data[$i]->type = ($data[$i]->type == Model_Users::USER_TYPE_MEMBER) ? 'Member' :'Advertisement';
//		}
//		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	
    public function editAction() {

        if (!$this->_request->isPost()) {

            $data = $this->model->getById($this->_getParam('id'));
            $this->view->data = $data;
            
        } else {

            $data = new Cubix_Form_Data($this->_request);
            
            $data->setFields(array(
                'id' => 'int',
                'redirect' => ''

            ));

            $data = $data->getData();
            
            $validator = new Cubix_Validator();
           
            if ($validator->isValid()) {
                $data["admin_id"] = $this->bu_user->id;
                $this->model->save($data);
            }

            die(json_encode($validator->getStatus()));
        }
    }
    
    
}
