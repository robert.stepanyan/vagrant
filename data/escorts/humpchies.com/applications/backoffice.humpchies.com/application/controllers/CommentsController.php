<?php

class CommentsController extends Zend_Controller_Action
{

    const MINIMUM_CHARS_COUNT = 10;
    /**
     * @var $model Model_Comments
     */
    private $model;

    public function init() {
        $this->model = new Model_Comments();
    }

    public function indexAction() {
        /* FILTER PARAMS */
        $ses_filter = new Zend_Session_Namespace('default_comments_data');
        $ses_filter->filter_params['status'] = -3;
        $this->view->filter_params = $ses_filter->filter_params;
        /* FILTER PARAMS */
    }

    public function dataAction() {
        $filter = array(
            'mode' => $this->_getParam('mode'),
            'escort_id' => $this->_getParam('ad_id'),
            'from_user' => $this->_getParam('from_user'),
            'user_id' => $this->_getParam('user_id'),
            'escort_id_f' => $this->_getParam('ad_id_f'),
            'username' => $this->_getParam('username'),
            'status' => $this->_getParam('status'),
            'is_active_escort' => $this->_getParam('is_active_ad'),
            'date_from' => $this->_getParam('date_from'),
            'date_to' => $this->_getParam('date_to'),
            'comment' => $this->_getParam('comment'),
            'disable_type' => $this->_getParam('disable_type'),
        );

        $data = $this->model->getAll(
            $this->_getParam('page'),
            $this->_getParam('per_page'),
            $filter,
            $this->_getParam('sort_field'),
            $this->_getParam('sort_dir'),
            $count
        );


        foreach ($data as $i => $item) {
            $data[$i]->status = Model_Comments::$STATUSES[$item->status];

            $data[$i]->message = mb_substr($item->comment, 0, 50, 'UTF-8') . ' ...';

            $data[$i]->c = '';
            $data[$i]->cities = '';
            if ($item->new_member) {
                $cl = 'red';
                $data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
                $data[$i]->c = 'c1';
            }
            elseif ($this->model->fakeByPhone($item->user_id)) {
                $cl = 'red';
                $data[$i]->username = '<span class="' . $cl . ' bold">' . $item->username . '</span>';
                $data[$i]->c = 'c2';
            }
        }

        $this->view->data = array(
            'data' => $data,
            'count' => $count
        );
    }

    public function editAction() {

        if (!$this->_request->isPost()) {

            $data = $this->model->getById($this->_getParam('id'));
            $data->ip = Model_Users::getLastLogin($data->user_id)->ip;

            $data->ad_ip = Model_Users::getLastLogin($data->ad_user_id)->ip;
            $this->view->data = $data;
            $this->view->comment_count = $this->model->getMemberCommentsCount($data->user_id);
            $this->view->dublCount = 0;
        }
        else {

            $data = new Cubix_Form_Data($this->_request);

            $data->setFields(array(
                'id' => 'int',
                'user_id' => 'int',
                'ad_id' => 'int',
                'status' => 'int',
                'comment' => ''

            ));

            $data = $data->getData();

            $validator = new Cubix_Validator();

            if (strlen($data['comment']) < self::MINIMUM_CHARS_COUNT) {
                $validator->setError('comment', 'Minimum ' . self::MINIMUM_CHARS_COUNT . ' chars');
            }

            switch ($this->_request->act) {
                case 'approve':
                    $data['status'] = Model_Comments::STATUS_ACTIVE;
                    break;
                case 'reject':
                    $data['status'] = Model_Comments::STATUS_DISABLED;
                    break;
            }

            if ($validator->isValid()) {
                $this->model->save($data); 
                // check if the user ads have at least one active comment and update all ads has_comment field
                $active_comments = $this->model->getActiveComments($data['user_id']);
                $model_ads = new Model_Advertisements();
                if($active_comments) {
                    $model_ads->updateAdsByUserId($data['user_id'], ['has_comments' => 1]);    
                } else {
                    $model_ads->updateAdsByUserId($data['user_id'], ['has_comments' => 0]);    
                }
            }

            die(json_encode($validator->getStatus()));
        }
    }

    public function removeAction() {
        $id = intval($this->_request->id);
        $this->model->remove($id);

        $active_comments = $this->model->getActiveComments($this->_request->user_id);
        $model_ads = new Model_Advertisements();
        if($active_comments) {
            $model_ads->updateAdsByUserId($this->_request->user_id, ['has_comments' => 1]);    
        } else {
            $model_ads->updateAdsByUserId($this->_request->user_id, ['has_comments' => 0]);    
        }
        
        die;
    }

    public function disabledReasonAction() {
        if (!$this->_request->isPost()) {
            $this->view->data = $data = $this->model->getById($this->_getParam('id'));
        }
        else {
            $data = new Cubix_Form_Data($this->_request);

            $data->setFields(array(
                'id' => 'int',
                'user_id' => 'int',
                'disable_type' => 'int',
                'reason' => ''
            ));

            $data = $data->getData();

            if (!$data['disable_type'])
                $data['disable_type'] = Model_Comments::DISABLE_TYPE_INTERNAL;

            $validator = new Cubix_Validator();
           // if (!$data['reason']) {
//                $validator->setError('reason', 'Required');
//            }

            if ($validator->isValid()) {
                $this->model->disabledReason($data);
                
                // check if the user ads have at least one active comment and update all ads has_comment field
                $active_comments = $this->model->getActiveComments($data['user_id']);
                $model_ads = new Model_Advertisements();
                if($active_comments) {
                    $model_ads->updateAdsByUserId($data['user_id'], ['has_comments' => 1]);    
                } else {
                    $model_ads->updateAdsByUserId($data['user_id'], ['has_comments' => 0]);    
                }
            }

            die(json_encode($validator->getStatus()));
        }
    }
}
