<?php

class BlacklistedEmailsController extends Zend_Controller_Action
{
	protected $model;

	public function init()
	{
		$this->model = new Model_BlacklistedEmails();
	}

	public function indexAction()
	{

	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array('name' => $req->name );
        
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

    public function createAction()
    {
        $this->view->layout()->disableLayout();

        if ($this->_request->isPost())
        {
	        $data = new Cubix_Form_Data($this->_request);

	        $data->setFields(array(
		        'name' => '',
		        'reason' => '',
	        ));

	        $data = $data->getData();

	        $validator = new Cubix_Validator();

	        if ( ! $data['name'] ) {
		        $validator->setError('name', 'Required');
	        }
                
                //$validator->setError('reason', 'Required');

	        if ($validator->isValid())
	        {
		        $data['block_date'] = new Zend_Db_Expr('NOW()');
			    $this->model->save($data);
	        }

	        die(json_encode($validator->getStatus()));
        }        
    }

    public function editAction(){
        $this->view->layout()->disableLayout();

        $id = $this->_request->getParam('id');
	    $this->view->id = $id;
        $this->view->blockedWork = $this->model->get($id);
        
        if ($this->_request->isPost())
        {
	        $data = new Cubix_Form_Data($this->_request);

	        $data->setFields(array(
		        'name' => '',
		        'reason' => '',
	        ));

	        $data = $data->getData();

	        $validator = new Cubix_Validator();

	        if ( ! $data['name'] ) {
		        $validator->setError('name', 'Required');
	        }

	        /*if ( Cubix_Application::getId() == APP_ED && ! $data['reason'] ) {
		        $validator->setError('reason', 'Required');
	        }*/

	        if ($validator->isValid())
	        {
		        $this->model->save($data, $id);
	        }

	        die(json_encode($validator->getStatus()));
        }
    }

	public function deleteAction()
	{

		if($this->_request->isPost()){
			$ids = $this->_request->getParam('id');
			if(count($ids) > 0 && $ids){
				foreach( $ids as $id ) {
					$this->model->delete($id);
				}
			}
		}
		exit;
	}
}
