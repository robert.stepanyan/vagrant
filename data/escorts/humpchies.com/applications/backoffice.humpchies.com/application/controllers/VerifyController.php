<?php

class VerifyController extends Zend_Controller_Action
{

    const MINIMUM_CHARS_COUNT = 10;

    /**
     * @var $model Model_Comments
     */
    private $model;

    public function init() {
        $this->model = new Model_AdVerification();
        $this->_session = new Zend_Session_Namespace('verification');
        $this->bu_user = Zend_Auth::getInstance()->getIdentity();
    }

    public function indexAction() {
        /* FILTER PARAMS */
        $ses_filter = new Zend_Session_Namespace('default_verify_data');
        $ses_filter->filter_params['status'] = -3;
        $this->view->filter_params = $ses_filter->filter_params;
        /* FILTER PARAMS */
    }

    public function dataAction() {
        $filter = array(
            
            'ad_id' => $this->_getParam('ad_id'),
            'user_id' => $this->_getParam('user_id'),
            'username' => $this->_getParam('username'),
            'status' => $this->_getParam('status'),
            'from' => $this->_getParam('from'),
            'to' => $this->_getParam('date_to'),
        );
        $sort = ($this->_getParam('sort_field'))?$this->_getParam('sort_field'): "verification_status";
        $dir = ($this->_getParam('sort_dir'))?$this->_getParam('sort_dir'): "desc";
        
        $bo = new Model_BOUsers();
        $users=$bo->getBoUsers();
        //var_dump($users);
        $boUsers = [];
        foreach($users as $user){
            //var_dump($user->id);
           $boUsers[$user->id] = $user->username;
        }
        $data = $this->model->getAll(
            $this->_getParam('page'),
            $this->_getParam('per_page'),
            $filter,
            $sort,
            $dir,
            $count
        );


        foreach ($data as $i => $item) {
            $item->admin = $boUsers[$item->verified_by];
        }

        $this->view->data = array(
            'data' => $data,
            'count' => $count
        );
    }
    
    public function verifyAction() {
        
        $verificationID = $this->_getParam('id');
        $data    = $this->model->getInfo($verificationID);
        
        $ad_id   = $data->ad_id;
        $user_id = $data->user_id;
        
        if($_POST['verify_status']){
            $id = $_POST['request_id'];
            $varsa = array();
            $varsa['verification_status'] = $_POST['verify_status'];
            if($_POST['verify_status'] == 'approved' && $data->verification_status != 'approved'){    
                $varsa['verified_by'] = $this->bu_user->id;
                $varsa['verification_date'] = date("Y-m-d H:i:s");    
                $ads = new Model_Advertisements();  
               
                $ads->update(intval($data->ad_id), array('verification_status' => 'verified'));
                
            }
            $this->model->setStatus($varsa, intval($this->_getParam('id')));
            $this->sendVerificationMail($verificationID);
            die(json_encode(['status' => 'success']));
        }
        
        $model_user = new Model_Users(); 
        $ad = new Model_Advertisements();
        $ads = $ad->get($ad_id);
        $this->view->ad_data = $ads;
        
        $ads_user = $model_user->get($user_id);
        $ads_user_history = $model_user->getUserLoginHistoryData($user_id);

        $user_ads = $ad->getAllAdsByUserId($user_id);
        $active_ads = $ad->getPublishedAdsByUserId($user_id);
        $inactive_ads = $ad->getInactiveAdsByUserId($user_id);
        $new_ads = $ad->getNewAdsByUserId($user_id);
        $deleted_ads = $ad->getDeletedAdsByUserId($user_id);
        $expired_ads = $ad->getExpiredAdsByUserId($user_id);
        $banned_ads = $ad->getBannedAdsByUserId($user_id);
        $premium_ads_count = $ad->getPremiumsCountByUserId($user_id);
        $other_users_with_same_phone = $ad->getUsersBySamePhone($user_id, $phone_number);       
        
        $dirImgs = "https://gui.humpchies.com/gui_ads/" . substr($ad_id, 0, 2) . "/" . substr($ad_id, 2) . "/";
         
        $this->view->ownImages      = explode(",", $data->ad_images);
        $this->view->old            = $data->old_id;
        $this->verificationID       = $verificationID;
       
        $old = Model_AdvertisementPhotos::getDeletedImages($ad_id);
        $this->view->OldImageDir = "https://gui.humpchies.com/gui_ads/" . substr($ad_id, 0, 2) . "/" . substr($ad_id, 2) . "/";        
        $this->view->oldDir = "https://gui.humpchies.com/gui_ads/" . substr($ad_id, 0, 2) . "/" . substr($ad_id, 2) . "/";        
        $oldImages = array();
        foreach($old as $img){
            $oldImages[] = 'img-'.$img->hash . "." . $img->ext;
        }
        if($data->verification_status  == 'requested'  && $data->previous ){
            $this->view->auto = true;
            $this->view->approvedate = $data->previous;
            
        }
        $this->view->oldImages      = $oldImages;  
        $this->view->verificationID  = $verificationID;
        $this->view->privateImages  = explode(",", $data->images);
        $this->view->ownImageDir = $dirImgs;
        $this->view->ad_id = $ad_id;
        $this->view->userid = $user_id;
        $this->view->user_created_date = date('m/d/Y', $ads_user->date_created);
        $this->view->username = $ads_user->username;
        $this->view->userid = $user_id;
        $this->view->user_login_count = count($ads_user_history);
        $this->view->user_ads_count = count($user_ads);
        $this->view->user_active_ads_count = count($active_ads);
        $this->view->user_inactive_ads_count = count($inactive_ads);
        $this->view->user_new_ads_count = count($new_ads);
        $this->view->user_deleted_ads_count = count($deleted_ads);
        $this->view->user_expired_ads_count = count($expired_ads);
        $this->view->user_banned_ads_count = count($banned_ads);
        $this->view->other_users_with_same_phone_count = $other_users_with_same_phone ? count($other_users_with_same_phone) : 0;
            $this->view->premium_ads_count = $premium_ads_count;
    }
    
    public function rejectAction(){
        if ($this->_request->isPost()) {
            $data['verification_status'] = "rejected";
            $data["reject_reason"] = "";
            $verif = $this->model->getMailInfo( $this->_request->request_id);
            //var_dump($verif);
            foreach($this->_request->reason as $reason){
                if($verif->language == 'EN'){
                    $data["reject_reason"] .=   ((isset($this->model->rejectReasons[$reason]))?$this->model->rejectReasons[$reason]:$this->_request->custom_reason) . "<br>"; 
                }else{
                    $data["reject_reason"] .=   ((isset($this->model->rejectReasons[$reason]))?$this->model->rejectReasonsFrench[$reason]:$this->_request->custom_reason) . "<br>"; 
                }
            }
            $data['verified_by'] = $this->bu_user->id;
            $data['verification_date'] = date("Y-m-d H:i:s");   
            //var_dump($data);
           // die();
            $this->model->setStatus($data, $this->_request->request_id);
            
            $ads = new Model_Advertisements();  
            $ads->update(intval($verif->ad_id), array('verification_status' => 'rejected'));
            $this->model->ignoreAccepted(intval($verif->ad_id));
            $this->sendVerificationMail($this->_request->request_id);
            die(json_encode(['status' => 'success']));
           
        }else{    
            $verificationID = $this->_getParam('id');
            $this->view->reasons = $this->model->rejectReasons;
            $this->view->verificationID = $verificationID;
        }  
    }
    
    public function editAction() {

        if (!$this->_request->isPost()) {

            $data = $this->model->getById($this->_getParam('id'));
            $data->ip = Model_Users::getLastLogin($data->user_id)->ip;

            $data->ad_ip = Model_Users::getLastLogin($data->ad_user_id)->ip;
            $this->view->data = $data;
            $this->view->comment_count = $this->model->getMemberCommentsCount($data->user_id);
            $this->view->dublCount = 0;
        }
        else {

            $data = new Cubix_Form_Data($this->_request);

            $data->setFields(array(
                'id' => 'int',
                'user_id' => 'int',
                'ad_id' => 'int',
                'status' => 'int',
                'comment' => ''

            ));

            $data = $data->getData();

            $validator = new Cubix_Validator();

            if (strlen($data['comment']) < self::MINIMUM_CHARS_COUNT) {
                $validator->setError('comment', 'Minimum ' . self::MINIMUM_CHARS_COUNT . ' chars');
            }

            switch ($this->_request->act) {
                case 'approve':
                    $data['status'] = Model_Comments::STATUS_ACTIVE;
                    break;
                case 'reject':
                    $data['status'] = Model_Comments::STATUS_DISABLED;
                    break;
            }

            if ($validator->isValid()) {
                $this->model->save($data);
            }

            die(json_encode($validator->getStatus()));
        }
    }

    public function removeAction() {
        $id = intval($this->_request->id);
        $this->model->remove($id);

        die;
    }

    public function disabledReasonAction() {
        if (!$this->_request->isPost()) {
            $this->view->data = $data = $this->model->getById($this->_getParam('id'));
        }
        else {
            $data = new Cubix_Form_Data($this->_request);

            $data->setFields(array(
                'id' => 'int',
                'disable_type' => 'int',
                'reason' => ''
            ));

            $data = $data->getData();

            if (!$data['disable_type'])
                $data['disable_type'] = Model_Comments::DISABLE_TYPE_INTERNAL;

            $validator = new Cubix_Validator();
            if (!$data['reason']) {
                $validator->setError('reason', 'Required');
            }

            if ($validator->isValid()) {
                $this->model->disabledReason($data);
            }

            die(json_encode($validator->getStatus()));
        }
    }
    
    public function loadimageAction(){
        $img = $this->_getParam('img'); 
        if (!in_array($this->bu_user->type, array('superadmin', 'admin')))
            die('permission denied');
        $dir = realpath(getcwd() . "/../../safelocker.humpchies.com/safe");
        $imgComp = explode(".", $img);
        switch(strtolower(end($imgComp))){
            case "jpg":
                header("Content-Type: image/jpeg");
            break;
            case "png":
                header("Content-Type: image/png");
            break;
            
        }
        readfile($dir . "/" . $img);
        exit;
        //die("dir:". $dir);
        
    }
    
    public function testAction(){
        $verificationID = $this->_getParam('id');
        $this->sendVerificationMail($verificationID) ; 
        
    }
    
    public function sendVerificationMail($verificationID){
        $info = $this->model->getMailInfo($verificationID);
        $language = $info->language;
        if(strtolower($language) != 'en' ){
            switch($info->verification_status){
                case 'approved': $templateID = 7;break;
                case 'rejected': $templateID = 8;break;
            }
        }else{       
            switch($info->verification_status){
                case 'approved': $templateID = 11; break;
                case 'rejected': $templateID = 12; break;
            }
        }
        $templateModel = new Model_Newsletter_Templates();
        $template= $templateModel->getById($templateID);
        $old = [];
        foreach($this->model->keywords as $key => $dbField){
            $old[] = '%%' .$key. '%%'; 
            $new[] = $info->$dbField;
        }
        $body = str_replace($old, $new, $template->body);
        $subject = $template->subject;
        Cubix_Email::send($info->email, mb_convert_encoding($subject, 'windows-1252', mb_detect_encoding($subject)), $body, true, $template->from_email );
        //var_dump([$info->email, mb_convert_encoding($subject, 'windows-1252', mb_detect_encoding($subject)), $body, true, $template->from_email]);
        
        //die();
    }
}
