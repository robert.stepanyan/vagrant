<?php

class ResetedPasswordController extends Zend_Controller_Action
{
	protected $model;

	public function init()
	{
		$this->model = new Model_ResetedPassword();
	}

	public function indexAction()
	{
		$bu_model = new Model_BOUsers();
		$this->view->sales_persons = $bu_model->getAllSales();
	}

	public function dataAction()
	{
		$req = $this->_request;


		$filter = array('e.showname' => $req->showname, 'e.id' => $req->id, 'sales_user_id' => $req->sales_user_id,);
       
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);

		foreach ($data as $item) {
			if($item->activation == 1){
				$item->activation = 'used';	
			}else{
				$item->activation = '-';	
			}
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
}
