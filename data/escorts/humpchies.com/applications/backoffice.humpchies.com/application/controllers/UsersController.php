<?php

class UsersController extends Zend_Controller_Action
{
    
        public static $statuses = array(
        0 => 'active',
        1 => 'general',
        2 => 'requested removal',
        3 => 'theft',
        4 => 'invalid phone',
        5 => 'male',
        6 => 'reputation',
        7 => 'explicit',
        8 => 'flooding',
        9 => 'fake pics',
        10 => 'fraud',
        11 => 'copyright',
        12 => 'shady',
        14 => 'promotes other sites',
        15 => 'Dicks',
        16 => 'Illegal shit',
        17 => 'Impersonation',
        18 => 'Remote',
        19 => 'potential minor',
        20 => 'potential human trafficking',
        21 => 'dushbag', 
        22 => 'no initial pics',
        23 => 'Bypassing emojis',
        24 => 'Bypassing image checkup by orientation',
        25 => 'dushbag images',
        26 => 'disrespect',
        //27 => 'Socially wrong',
        27 => 'Blacklisted',
        28 => 'Delete',
        29 => 'WatchChargeBack',
        30 => 'BannedbyIP'
    );
	public function init()
	{
		$this->model = new Model_Users();
		
		$this->bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($this->bu_user->type, array('superadmin', 'admin')))
			die('permission denied');
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_users_data');
		$this->view->filter_params = $ses_filter->filter_params;
		$this->view->bu_user = $this->bu_user;
        $this->view->request = $this->_request;
		
		/* FILTER PARAMS */
	}
    
    public function dataAction()
    {
        
        $req = $this->_request;

        $filter = array(
            'username' => $req->username,
            'id' => $req->id,
            'email' => $req->email,
            'phone' => $req->phone,
            'date_banned' => $req->date_banned,
            'used_user_ids' => $req->used_user_ids,
            'auto_approval' => $req->auto_approval,
            'fraudulent' => $req->fraudulent,
            'type'          => $req->user_type
        );
        
        $data = $this->model->getAll(
            $this->_request->page,
            $this->_request->per_page, 
            $filter, 
            $this->_request->sort_field, 
            $this->_request->sort_dir, 
            $count
        );
        
        foreach ( $data as $i => $item ) {
            $data[$i]->status = Model_Users::$statuses[$item->date_banned];
            $data[$i]->type = ($data[$i]->type == Model_Users::USER_TYPE_MEMBER) ? 'Member' :'Advertisement';
            $data_protection = explode(",", $data[$i]->duplicate_protection);
            $dprot = [];
            foreach($data_protection as $dp){
                $temp = explode("-", $dp);
                if($temp[1] == 0){
                      
                      $dprot[] =  $temp[0]."|" . "|#008000";    
                }else{
                    $dprot[] =  $temp[0]."|"  . "|#ff0000";
                }
                
            }
            //$dprot=[];
            $data[$i]->duplicate_protection = implode(",",  $dprot);
            
            if($item->used_user_ids){
                //$dup = explode($item->used_user_ids);
                // make sure user ids does not end with comma
                $item->used_user_ids = rtrim($item->used_user_ids, ',');
                
                $list = Model_Users::getStatuses($item->used_user_ids);
                $dupusers = [];
                //var_dump($list) ;
                foreach ($list as $userStatus){
                    if($userStatus->date_banned){
                        $dupusers[] = $userStatus->id ."||#ff0000" ;   
                    }else{
                        $dupusers[] = $userStatus->id ."||#008000" ;
                    }
                } 
                $data[$i]->dupusers = implode(",",  $dupusers); 
            }else{
                $data[$i]->dupusers = "";
            }
            
        }
        
        $this->view->data = array(
            'data' => $data,
            'count' => $count
        );
    }	
	public function ipsdataAction()
    {
        
        $req = $this->_request;

        $filter = array(
            'username'      => $req->username,
            'id'            => $req->id,
            'email'         => $req->email,
            'phone'         => $req->phone,
            'date_banned'   => $req->date_banned,
            'class'         => $req->class,
            'country'       => $req->country,
            'ads'           => $req->ads,
            'type'          => $req->user_type
        );
        
        $data = $this->model->getIps(
            $this->_request->page,
            $this->_request->per_page, 
            $filter, 
            $this->_request->sort_field, 
            $this->_request->sort_dir, 
            $count
        );
        
        foreach ( $data as $i => $item ) {
            if(isset($item->date_banned)){
                $data[$i]->status = $this->$statuses[$item->date_banned];
            }
            $data[$i]->type = ($data[$i]->type == Model_Users::USER_TYPE_MEMBER) ? 'Member' :'Advertisement';
            
            
            
        }
        
        $this->view->data = array(
            'data' => $data,
            'count' => $count
        );
    }
    public function chargebacksdataAction()
	{
		
		$req = $this->_request;

		$filter = array(
			'username'      => $req->username,
			'id'            => $req->id,
			'email'         => $req->email,
			'phone'         => $req->phone,
			'date_banned'   => $req->date_banned,
            'class'         => $req->class,
            'country'       => $req->country,
            'ads'           => $req->ads,
            'type'          => $req->user_type
		);
		
		$data = $this->model->getChargebacks(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function toggleActiveAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->toggle($id);
	}

    public function loginAction(){
        $key = uniqid('admin-');
        $varsa['user_id'] = $_GET['id'];
          
        Core_Redis::set($key,serialize($varsa));
       // die("http://www.humpchies.com.test/user/login?admin-login-A66rkgag=" . $key);
        header("Location: https://www.humpchies.com/user/login?admin-login-A66rkgag=" . $key );
        die();
    }

	public function activateUserAction()
    {
        $user_id = intval($this->_request->id);
        if ( ! $user_id ) die;
        $this->model->activateUser($user_id);


    }

    public function disableAction()
    {

        if($this->_request->id){
            $this->model->disableAds($this->_request->id);
        }
        die;
    }
    
    public function disableduplicatesAction()
    {

        if($this->_request->id){
            $ids = $this->model->getActiveDuplicates($this->_request->id);
            $ids_array = [];
            foreach($ids as $idd){
                $ids_array[] = $idd->user_id;
            }
            
            if(count($ids_array)){
                $this->model->deactivateAdsUsers($ids_array);
                $this->model->banUsers($ids_array);
            }
            
        }
        die;
    }
    
    public function floodingAction()
    {
        $model_ads = new Model_Advertisements();
        $paid_count = 0;
        
        if($this->_request->user_id){
            if(is_array($this->_request->user_id)){
                $data_unique = array_unique($this->_request->user_id);
                
                foreach($data_unique as $id){
                    $this->model->floodingUser( intval($id) );
                    $model_ads->disableUserAds(intval($id));
                }
            }
            else{
                $this->model->floodingUser( intval($this->_request->user_id) );
                $model_ads->disableUserAds( intval($this->_request->user_id) );
            }
        }
        
        die;
    }
    
    public function banAction()
    {
        $model_ads = new Model_Advertisements();
        $paid_count = 0;
        
        if($this->_request->user_id){
            if(is_array($this->_request->user_id)){
                $data_unique = array_unique($this->_request->user_id);
                
                foreach($data_unique as $id){
                    $this->model->banUser( intval($id), $this->_request->bstatus, $this->_request->ad_comment );
                    $model_ads->disableUserAds(intval($id));
                }
            }
            else{
                $this->model->floodingUser( intval($this->_request->user_id) );
                $model_ads->disableUserAds( intval($this->_request->user_id) );
            }
        }
        
        die(json_encode(array('status' => 'success')));
    }
    
    public function fraudingAction()
    {
        $model_ads = new Model_Advertisements();
        $paid_count = 0;
        
        if($this->_request->id){
          
                $this->model->fraudUser( intval($this->_request->id) );
                $model_ads->disableUserAds( intval($this->_request->id) );
        }
		
        die;
    }

    public function checkUserOrdersAction()
    {
        $user_id = intval($this->_request->id);
        if ( ! $user_id ) die;
        $status = array('status' => 'error');
        if($this->model->checkUserOrders($user_id)){
           $status = array('status' => 'success');
        }
        if($this->_request->ajax) {
            die(json_encode($status));
        } else {
            return $status;
        }
        
    }

    public function createAction()
    {
            $req = $this->_request;

            if ( $req->isPost() ) {
                    $phone = null;
                    $auto_approval = 0;
                    $validator = new Cubix_Validator();

                    if ( ! $req->username ) {
                            $validator->setError('username', 'Required');
                    }
                    else if ( $this->model->userNameExists($req->username) ) {
                            $validator->setError('username', 'Username already exists!');
                    }

                    if ( ! $req->email ) {
                            $validator->setError('email', 'Required');
                    }
                    elseif($this->model->emailExists($req->email)){
                            $validator->setError('email', 'Email exist');
                    }

                    if ( strlen($req->phone) ) {
                            $phone = preg_replace('/[^0-9]/', '', $req->phone);
                    }

                    if ( isset($req->auto_approval) ) {
                            $auto_approval = $req->auto_approval;
                    }

                    if ( ! $req->password ) {
                            $validator->setError('password', 'Required');
                    } elseif(strlen($req->password) < 6) {
                            $validator->setError('password', 'Password must be at least 6 characters long');
                    }

                    if ( $validator->isValid() )
                    {
                            $bu_user = Zend_Auth::getInstance()->getIdentity();
               
                
                            $data = array(
                                'username' => $req->username,
                                'password' => md5($req->password. Model_Users::VALIDATION_KEY),
                                'email' => $req->email,
                                'phone' => $phone,
                                'date_banned' => $req->date_banned,
                                'date_created' => time(),
                                'date_activated' => time(),
                                'comment' => $req->user_comment,
                                'auto_approval' => $auto_approval
                            );



                            $this->model->insert($data);
                    }

                    die(json_encode($validator->getStatus()));
            }
    }
    

    public function editAction()
    {
        $req = $this->_request;
		
        //MyLogs::debug($req, "============ USER EDIT ADMIN ============");
        
        $user_id = $req->id;
        $user = $this->model->get($user_id);
        $this->view->user = $user;
        $model_ads = new Model_Advertisements();
        
        //MyLogs::debug($user, "user data");
        
        $this->view->checkPackage = 0;
        $packageCheck = $this->checkUserOrdersAction();
        if($packageCheck['status'] == 'success'){
            $this->view->checkPackage = 1;
        }
        
        //MyLogs::debug($packageCheck, "package check");
        $this->view->userHistoryData = $this->model->getUserLoginHistoryData($user_id);
        MyLogs::debug($this->view->userHistoryData, "history data");
        
        if ( $req->isPost() ) {
            $phone = null;
            $auto_approval = 0;
            $disable_comments = 0;
            $blocked_comments = 0;
            $validator = new Cubix_Validator();

            /*if ( ! $req->username ) {
                    $validator->setError('username', 'Required');
            }
            elseif(strlen($req->username) < 4){
                    $validator->setError('username', 'Username is too short');
            }
            elseif($this->model->usernameExist($req->username)){
                    $validator->setError('username', 'Username exist');
            }*/


            if ( ! $req->email ) {
                $validator->setError('email', 'Required');
            }
            elseif($this->model->emailExists($req->email, $req->id)){
                $validator->setError('email', 'Email exist');
            }
            
            if($this->model->usernameExists($req->username,$req->id)){
                $validator->setError('username', 'Username exist');
            }

            if ( strlen($req->phone) ) {
                $phone = preg_replace('/[^0-9]/', '', $req->phone);
            }

            if ( strlen($req->auto_approval) ) {
                $auto_approval = $req->auto_approval;
            }
            
            if ( strlen($req->disable_comments) ) {
                $disable_comments = $req->disable_comments;
                $blocked_comments = $req->disable_comments;
            }
            
            if ( strlen($req->visible_comments) ) {
                $disable_comments = $req->visible_comments;
                $blocked_comments = 0; // 0
            }

            if ( $validator->isValid() )
            {
                $bu_user = Zend_Auth::getInstance()->getIdentity();
                
                $data = array(
                    'username' => $req->username,
                    'email' => $req->email,
                    'phone' => $phone,
                    'date_banned' => $req->date_banned,
                    'comment' => $req->user_comment,
                    'auto_approval' => $auto_approval,
                    'disable_comments' => $disable_comments
                );
                
                if($user->date_banned == 28 and $req->date_banned == 0){
                    $data['date_activated'] = time(); // activating
                    $data['date_grace'] = time() + 86400; //adding grate period to create a new ad    
                }
                if($req->date_banned == 8 || $req->date_banned == 28 || $req->date_banned == 9 ){
                    $model_ads->disableUserAds( intval($req->id) );
                }
                
                if ( strlen($req->password) ) {
                    if(strlen($req->password) < 6) {
                            $validator->setError('password', 'Password must be at least 6 characters long');
                    }

                    $data = array_merge($data, array('password' => md5($req->password . Model_Users::VALIDATION_KEY )));
                }
                

                $this->model->update($req->id,$data);

                if ( strlen($req->auto_approval) ) {
                    $model_ads = new Model_Advertisements();
                    $edited_ads = $model_ads->getUserEditedAds($user_id);

                    if($edited_ads) {
                        foreach ($edited_ads as $key => $ad) {
                            $revision_data = (array)$model_ads->getRevisionData($ad->id);
                            
                            if($revision_data) {
                                unset($revision_data['id']);
                                unset($revision_data['ads_id']);
                                unset($revision_data['date_added']);
                                unset($revision_data['status']);
                                unset($revision_data['user_id']);
                                unset($revision_data['approved_status']);
                            }

                            $revision_data['is_updated'] = '0';

                            $model_ads->update($ad->id, $revision_data);
                            $model_ads->approveRevisionStatus($ad->id);

                            $ad_photos = new Model_AdvertisementPhotos();
                            $ad_photos->approveRevision($ad->id);
                        }
                    }
                }
                
                // update disable comments
                $model_ads = new Model_Advertisements();
                $model_ads->updateAdsByUserId($user_id, ['blocked_comments' => $blocked_comments]);
                
            }

            die(json_encode($validator->getStatus()));
        }
    }
    
    public function banphonesAction()
    {
        $req = $this->_request;
        
        $user_id = $req->id;
        $user = $this->model->get($user_id);
        $this->view->user = $user;
        
        $this->view->checkPackage = 0;
        $packageCheck = $this->checkUserOrdersAction();
        if($packageCheck['status'] == 'success'){
            $this->view->checkPackage = 1;
        }
        if ( $req->isPost() ) {
            $reason = $req->reason; 
            $reason .= " \n banned by " . $this->bu_user->id;
             $blacklist = new Model_BlacklistedPhones();   
             $phones = Model_Advertisements::getUserPhoneNumbers($user_id);
             // $phones = Model_Advertisements::getUserPhoneNumbers($user_id);
             $data = ['reason' => $reason, 'block_date' => date('Y-m-d H:i:s') ];
                 
             foreach($phones as $phone){
                 $data['name'] = $phone->phone;
                 //var_dump($data);
                 $blacklist->save($data);
             }
            die('{"status":"success"}'); 
        }
       

    }

    public function removeAction()
    {
            /*$id = intval($this->_request->id);

            $this->model->remove($id);*/
            die;
    }
	
	public function itemAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$model = new Model_Users();

		$row = $model->getForPicker(intval($req->user_id));
		
		if($row){
			$result = array(
				'id' => $row->id,
				'user_id' => $row->id,
				'status' => Model_Users::$statuses[$row->date_banned],
				'username' => $row->username,
				'credits' => $row->credits,
				
				//'photo' => $row->getMainPhoto()->getUrl('backend_smallest')
			);
		}
		else{
			$result = array('id' => 0);
		}
				
		die(json_encode($result));
	}

    
    public function logAction(){
        //Core_Redis::hset("logget","llog". $data, serialize($_GET));
        $zlogs = Core_Redis::hgetAll("logpost");
        foreach($zlogs as $zlog){
            var_dump(unserialize($zlog));
        }
        die();
    }
    
    public function ipsAction(){
       $ses_filter = new Zend_Session_Namespace('default_users-ip_data');
       $this->view->filter_params = $ses_filter->filter_params;
       $this->view->bu_user = $this->bu_user;
       $this->view->pageTitle ='Ip Lists';
       
       if($_GET['class']){
          $this->view->pageTitle = "Ips for class " . $_GET['class']. "/24"; 
       }
       $this->view->xclass = $_GET['class'];
         
    } 
       
    public function chargebacksAction(){
       $ses_filter = new Zend_Session_Namespace('default_users-ip_data');
       $this->view->filter_params = $ses_filter->filter_params;
       $this->view->bu_user = $this->bu_user;
       $this->view->pageTitle ='Users linked to Chargebacks';
         
    }
    
    public function iplistAction(){
         $req = $this->_request;
        
         $user_id = $req->id;
         $history = $this->model->getUserLoginHistoryData($user_id);
         foreach($history as $row){
            if(!$row->ip_location){              
                $loc = getGeoLocation($row->ip);
                $row->ip_location =  mb_convert_encoding(html_entity_decode($loc['location'], ENT_HTML5, "Windows-1252" ), 'UTF-8', 'Windows-1252');
                $row->ip_country  = $loc['country'];
            } 
         }
         $this->view->userHistoryData = $history;
    }
   
    public function updatelocationAction(){
          //$model_users = new Model_Users();  
//          var_Dump($rows[0]);
//          $loc = geo::getGeoLocation($rows[0]->ip_reg);
//          $loc = geo::getGeoLocation("41.79.219.223");
//          var_dump($loc);
//          die();
    }
    
    

}
