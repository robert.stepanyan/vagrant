<?php

class ReportsController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Reports();
		
		$this->bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($this->bu_user->type, array('superadmin', 'admin')))
			die('permission denied');
	}
	
	public function indexAction() 
	{
       
        //$onlineBilling->markChargeBack();
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_users_data');
		$this->view->filter_params = $ses_filter->filter_params;
		$this->view->bu_user = $this->bu_user;
		
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		
		$req = $this->_request;

		$filter = array(
            'ad_id' => $req->ad_id,
            'user_id' => $req->user_id,
            'reporter_id' => $req->reporter_id,
            'report_type' => $req->report_type,
            'checked' => $req->checked,
//			'id' => $req->id,
//			'email' => $req->email,
//			'phone' => $req->phone,
//			'date_banned' => $req->date_banned,
//            'used_user_ids' => $req->used_user_ids,
//            'auto_approval' => $req->auto_approval,
//            'type'          => $req->user_type
		);
		
		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
            foreach ( $data as $i => $item ) {
			//$data[$i]->status = Model_Users::$statuses[$item->date_banned];
			$data[$i]->report_type = Model_Reports::$types[$item->report_type];
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

    public function viewAction(){
        header('Content-Type: text/html; charset=utf-8');
        $req = $this->_request;
        $report = $this->model->get($req->id);
        $this->view->report = $report;
    }
    
    public function checkedAction(){
        $id = intval($this->_request->id);
        //die($id); 
        $this->model->MarkChecked($id, intval($this->bu_user->id));
        die();
    }
     public function repliedAction(){
        $id = intval($this->_request->id);
        $this->model->MarkReplied($id, intval($this->bu_user->id));
        die();
    }
	
}
