<?php

class DashboardController extends Zend_Controller_Action
{
	/**
	 * @var Model_Dashboard
	 */
	public $dashboard;

	public function init()
	{
		$this->dashboard = new Model_Dashboard();

	}

	public function tipDataAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( $bu_user->use_light_version ) die;

		$this->view->layout()->disableLayout();

		$user_id = $this->_request->user_id;

		$users_model = new Model_Users();

		if ( ! is_null($user_id) ) {
			$user = $users_model->get($user_id);

			if ( ! $user ) {
				die;
			}
		}

		if ( (isset($user) && $user->user_type == Model_Users::USER_TYPE_ESCORT ) ||
				! is_null($escort_id = $this->_getParam('escort_id')) ||
				! is_null($showname = $this->_getParam('showname')) ) {
			$model = new Model_Escorts();
			// var_dump(! is_null($escort_id) || ! is_null($showname) ? ($escort_id ? $escort_id : $showname) : $user->getId());
			if (!is_null($showname))
			{
				$ar = explode ('-', $showname);
				$escort_id = end($ar);
			}
			//$this->view->escort = ! is_null($escort_id) || ! is_null($showname) ? $model->get($escort_id ? $escort_id : $model->getIdByShowname($showname)->id) : $model->getByUserId($user->getId());
			$this->view->escort = ! is_null($escort_id) ? $model->get($escort_id) : $model->getByUserId($user->getId());
			$user = $users_model->get($this->view->escort->user_id);
			$this->_helper->viewRenderer->setScriptAction('tip-data-escort');
		}
		elseif ( $user->user_type == Model_Users::USER_TYPE_AGENCY ) {
			$model = new Model_Agencies();
			$this->view->agency = $model->getByUserId($user->getId());
			$this->_helper->viewRenderer->setScriptAction('tip-data-agency');
		}

		$this->view->user = $user;
	}

	public function indexAction()
	{
		
		$auth = Zend_Auth::getInstance();
		$admin = $auth->getIdentity();
		$this->view->admin_type = $admin->type;
		switch ( $admin->type )
		{
			case 'superadmin':
				$this->_forward('superadmin');
			break;
			case 'admin':
				$this->_forward('superadmin');
			break;
			case 'moderator plus':
				$this->_forward('superadmin');
			break;
			case 'moderator':
				$this->_forward('superadmin');
			break;
			case 'payments manager':
				$this->_forward('superadmin');
			break;
			case 'dash manager':
				$this->_forward('dashmanager');
			break;
			case 'sales manager':
				$this->_forward('salesmanager');
			break;
			case 'sales clerk':
				$this->_forward('salesmanager');
			break;
			case 'data entry':
				$this->_forward('dataentry');
			break;
			case 'data entry plus':
				$this->_forward('dataentry');
			break;
			case 'seo manager':
				$this->_forward('seomanager');
			break;
			default:
				$this->_forward('superadmin');
		}

		/*$client = new Cubix_Api_XmlRpc_Client();
		$agency = $client->call('Agencies.getEscorts', array($agency_id, $page, $config['widgetPerPage'], $escort_id));*/

		//$acl = Zend_Registry::get('acl');
		//echo $acl->isAllowed('sales manager', 'dashboard', 'index') ? "разрешен" : "запрещен";
	}

    public function visitAction()
    {

    }

	public function visitDataAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $req = $this->_request;

        $sort = isset($req->sort_field) ? $req->sort_field : 'date';
        $dir = isset($req->sort_dir) && $sort!='' && in_array($req->sort_dir, array('asc','desc')) ? $req->sort_dir : 'DESC';
        $page = isset($req->page) && is_numeric($req->page) ? intval($req->page) : 1;
        $per_page = isset($req->per_page) && is_numeric($req->per_page) ? intval($req->per_page) : 10;
        $total_visits = (isset($req->total_visits) && intval($req->total_visits) == 1) ? 1 : 0;
        $top_visited = (isset($req->top_visited) && intval($req->top_visited) == 1) ? 1 : 0;

        $result = $this->dashboard->getEscortHits(
            $req->escort_id,
            $req->agency_id,
            $req->select_date_from,
            $req->select_date_to,
            $page,
            $per_page,
            $sort,
            $dir,
            $total_visits,
            $top_visited,
            $req->country_id
        );

        die(json_encode($result));
    }

	public function salesmanagerAction()
	{
		//$this->billingInfoAction();

		/*$this->latestRegisteredEscortsAction();
		$this->notApprovedRevisionsAction();
		$this->verifyNotificationsAction();
		$this->notApprovedPhotosAction();*/

		$app_id = $this->_request->application_id;

		/*if ( $app_id == 16 ) {
			$this->phoneBillingEscortsAction();
		}*/

		//$this->view->statistics = $this->dashboard->getStatistics($app_id);

		$this->view->layout()->enableLayout();
	}

	public function seomanagerAction()
	{
		$this->_redirect('/seo/entities');
	}

	public function dataentryAction()
	{
		$this->_redirect('/escorts');
	}

	public function superadminAction()
	{
		$this->view->layout()->enableLayout();
	}

	public function dashmanagerAction()
	{
		$this->view->layout()->enableLayout();
	}

	
	public function newAdsAction()
	{
		$this->view->layout()->disableLayout();
		$m = new Model_Advertisements();
        $new_ads  = $m->getAllNew(1, 100, array('status'=> 'new','unblocked'=> 1), 'id', 'DESC', $count);
		//$new_ads  = $m->getAll(1, 100, array('status'=> 'new','unblocked'=> 1), 'id', 'DESC', $count);
		$this->view->new_ads = $new_ads;
		$this->view->new_ads_count = count($new_ads);
	}

	public function editedAdsAction()
	{
		$this->view->layout()->disableLayout();
		$m = new Model_Advertisements();
		$edited_ads  = $m->getAll(1, 100, array('is_updated'=> '1'), 'id', 'DESC', $count);
		$this->view->edited_ads = $edited_ads;
		$this->view->edited_ads_count = count($edited_ads);
	}
}
