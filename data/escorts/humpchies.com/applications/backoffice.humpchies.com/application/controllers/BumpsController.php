<?php

class BumpsController extends Zend_Controller_Action
{
   
	public function init()
	{
		$this->model = new Model_Bump();
		$this->bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($this->bu_user->type, array('superadmin', 'admin')))
			die('permission denied');
	}
	
	public function indexAction() 
	{
		
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_users_data');
		$this->view->filter_params = $ses_filter->filter_params;
		$this->view->bu_user = $this->bu_user;
		
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
			'id' => $req->id,
			'user_id' => $req->user_id,
			'status' => $req->status,
			'date_from' => $req->date_from,
			'date_to' => $req->date_to
		);

		$data = $this->model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);

		foreach ( $data as $i => $item ) {
			$data[$i]->settings = $item->is_one_time ? 'One time Bump' :  $this->model->getSettingsViewById($item->bump_settings_id);
			$data[$i]->city_slug = strtolower(Model_Advertisements::translateDiacriaticsAndLigatures(mb_convert_encoding($item->city, "Windows-1252", "UTF-8")));
		}

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	/*public function toggleActiveAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->toggle($id);
	}*/

	
}
