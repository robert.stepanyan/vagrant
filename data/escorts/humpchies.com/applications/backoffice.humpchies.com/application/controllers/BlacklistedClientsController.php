<?php

class BlacklistedClientsController extends Zend_Controller_Action
{
	protected $model;

	public function init()
	{
		$this->model = new Model_BlacklistedClients();
	}

	public function indexAction()
	{
          
        //$onlineBilling->markChargeBack();
        /* FILTER PARAMS */
        $ses_filter = new Zend_Session_Namespace('default_adslocation_data');
        $this->view->filter_params = $ses_filter->filter_params;
        $this->view->bu_user = $this->bu_user;
        
        /* FILTER PARAMS */

	}

	public function dataAction()
	{
		$req = $this->_request;

		$filter = array(
        'escort' => $req->escort , 
        'client' => $req->client , 
        'phone' => $req->phone , 
        'comment' => $req->comment , 
        'status' => $req->status , 
            '' );
        
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,
			$filter,
			$req->sort_field,
			$req->sort_dir,
			$count
		);

		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

    public function createAction()
    {
        $this->view->layout()->disableLayout();

        if ($this->_request->isPost())
        {
	        $data = new Cubix_Form_Data($this->_request);

	        $data->setFields(array(
		        'name' => '',
		        'reason' => '',
	        ));

	        $data = $data->getData();

	        $validator = new Cubix_Validator();

	        if ( ! $data['name'] ) {
		        $validator->setError('name', 'Required');
	        }
                
                //$validator->setError('reason', 'Required');

	        if ($validator->isValid())
	        {
		        $data['block_date'] = new Zend_Db_Expr('NOW()');
			    $this->model->save($data);
	        }

	        die(json_encode($validator->getStatus()));
        }        
    }

    public function editAction(){
        header('Content-Type: text/html; charset=utf-8');
        $this->view->layout()->disableLayout();

        $id = $this->_request->getParam('id');
	    $this->view->id = $id;
        $this->view->client = $this->model->get($id);
        
        if ($this->_request->isPost())
        {
	        $data = new Cubix_Form_Data($this->_request);

	        $data->setFields(array(
		        'type' => '',
                'name' => '',
                'phone' => '',
                'country' => '',
                'city' => '',
	        ));
            
            
	        $data = $data->getData();
            
            $data['name']        = mb_convert_encoding($this->_request->name, 'HTML-ENTITIES', "UTF-8");
            $data['description'] = mb_convert_encoding($this->_request->description, 'HTML-ENTITIES', "UTF-8");

            switch($this->_request->getParam('act')){
                case "approve" :  $data['status'] ='approved'; break;
                case "reject"  :  $data['status'] ='denied';   break;
            }

           
	        $validator = new Cubix_Validator();

	        if ( ! $data['name'] ) {
		        $validator->setError('name', 'Required');
	        }

	        /*if ( Cubix_Application::getId() == APP_ED && ! $data['reason'] ) {
		        $validator->setError('reason', 'Required');
	        }*/

	        if ($validator->isValid())
	        {
		        $this->model->save($data, $id);
	        }

	        die(json_encode($validator->getStatus()));
        }
    }

	public function deleteAction()
	{

		if($this->_request->isPost()){
			$ids = $this->_request->getParam('id');
			if(count($ids) > 0 && $ids){
				foreach( $ids as $id ) {
					$this->model->delete($id);
				}
			}
		}
		exit;
	}
}
