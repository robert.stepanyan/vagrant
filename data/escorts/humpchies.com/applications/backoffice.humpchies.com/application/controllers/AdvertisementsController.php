<?php

/**
 * Class AdvertisementsController
 *
 * @property-read Model_Advertisements|null $model
 * @property-read Zend_Session_Namespace $_session
 * @property-read $bu_user $mixed
 */
class AdvertisementsController extends Zend_Controller_Action
{
    private $model,
    $_session,
    $bu_user;
    
    
    static $html_translation_table = NULL;
	public function init()
	{
		$this->model = new Model_Advertisements();
		$this->_session = new Zend_Session_Namespace('advertisment');
		$this->bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($this->bu_user->type, array('superadmin', 'admin')))
			die('permission denied');
	}
	
	public function indexAction() 
	{
        
      //  $text = "Coucou Sexy!!&rsquo;OOH que sa doit manger les cadres de porte T&rsquo;as envi d&rsquo;une vrai adorable petite mature croquante de 5,2 107lb de 40ans bien assume???? COVID-19;Tout est prot&eacute;ge.Tu sait les pr&eacute;liminaire sont aussi d&eacute;licieux.Massage. Ton temps est illimit&eacute; prenons le temps!! J&rsquo;adore l&rsquo;homme j&rsquo;adore tallumer te rendre fou fou!!cest un besoin bb Cette bulle est &agrave; Beauharnois chez moi discr&egrave;tement Par texte seulement (438) 320-3990";
//        $ztext = self::htmlEncodeText($text);
//        die($ztext); 
        
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_users_data');
		$this->view->filter_params = $ses_filter->filter_params;
		$this->view->bu_user = $this->bu_user;
		
		/* FILTER PARAMS */
	}
	
	public function dataAction()
    {
            set_time_limit(0);
            $req = $this->_request;

            $filter = array(
                'id' => $req->id,
                'user_id' => $req->user_id,
                'title' => $req->title,
                'description' => $req->text,
                'status' => $req->status,
                'phone' => $req->phone,
                'category' => $req->category,
                'city' => $req->city,
                'active_package_id' => $req->active_package_id,
                'date_banned' => $req->date_banned,
                'filter_by' => $req->filter_by,
                'date_from' => $req->date_from,
                'date_to' => $req->date_to,
                'auto_approved' => $req->auto_approved,
                'fake_status' => $req->fake_status,
                'one_pic' => $req->one_pic,
                'no_pic' => $req->no_pic,
                'verified' => $req->verified,
                'ad_country' => $req->ad_country,
                'modified' => $req->modified,
                'naa' => $req->naa,
                'has_map' => $req->has_map,
                'disable_comments' => $req->disable_comments,
                'tags' => $req->tags,
                'has_price' => $req->has_price
            );
           
          

            $data = $this->model->getAll(
                $this->_request->page,
                $this->_request->per_page, 
                $filter, 
                $this->_request->sort_field, 
                $this->_request->sort_dir, 
                $count
            );
           foreach ($data as $d){
                $d->b_user_type = $this->bu_user->type;
                $d->total_ads  = "<span style=\"color:".(($d->total_ads)?"#ccc":"#ff0000")."\">" . $d->total_ads ."</span>";
                //$data[$i]->description = strip_tags($data[$i]->description);
                if( $d->description){
                    $d->description = mb_convert_encoding(html_entity_decode($d->description, ENT_HTML5, "Windows-1252" ), 'UTF-8', 'Windows-1252');
                    $d->description = preg_replace("`&#[0-9]+;`is",' ',$d->description);
                    //$d->description = self::htmlEncodeText($d->description);
                }
               //$d->description = mb_convert_encoding($d->description, "Windows-1252", "UTF-8" );
            }
         
            $show_with_photos = ( isset( $req->gallery_view ) && $req->gallery_view == 1 );
            $show_with_description = ( isset( $req->description_view ) && $req->description_view == 1 );

            foreach ( $data as $i => $item ) {
                $data[$i]->city_slug = Model_Advertisements::translateDiacriaticsAndLigatures(mb_convert_encoding($item->city, "Windows-1252", "UTF-8"));

                if( $show_with_photos ){
                    $ad_param         = $this->model->get($item->id);
                    $data[$i]->images = $this->_generateImages((array)$ad_param);
                    if($item->attached_video){
                        $data[$i]->video  = array("user" => $item->user_id, "video"  => $item->attached_video);
                    }
                    /*
                    <div class="vide-container">
             <video class='video' width="100%"  controls="" controlslist="nodownload" id="movie-load">
                <source src="https://gui.humpchies.com/videos/temp/<?=$this->ad_data->user_id;?>/<?=$this->ad_data->attached_video;?>" type="video/mp4">
             </video>         
        </div>
                    */
                }
                
                if( $show_with_description ){
                    $data[$i]->t_desc = array('title' => $data[$i]->title, 'description' => $data[$i]->description);
                }
               
            }

            $this->view->data = array(
                'data' => $data,
                'count' => $count
            );
    }

    public function approvalsAction() 
    {
        
        /* FILTER PARAMS */
        $ses_filter = new Zend_Session_Namespace('default_users_data');
        $this->view->filter_params = $ses_filter->filter_params;
        $this->view->bu_user = $this->bu_user;
        
        /* FILTER PARAMS */
    }
    
    public function approvalsdataAction()
    {
            $req = $this->_request;

            $filter = array(
                'id' => $req->id,
                'user_id' => $req->user_id,
                'title' => $req->title,
                'description' => $req->text,
                'status' => $req->status,
                'phone' => $req->phone,
                'category' => $req->category,
                'city' => $req->city,
                'active_package_id' => $req->active_package_id,
                'date_banned' => $req->date_banned,
                'filter_by' => $req->filter_by,
                'date_from' => $req->date_from,
                'date_to' => $req->date_to,
                'auto_approved' => $req->auto_approved,
                'fake_status' => $req->fake_status,
                'one_pic' => $req->one_pic,
                'no_pic' => $req->no_pic,
                'verified' => $req->verified,
                'ad_country' => $req->ad_country,
                'modified' => $req->modified,
                'naa' => $req->naa,
                'has_map' => $req->has_map,
                'disable_comments' => $req->disable_comments
            );
           
          

            $data = $this->model->getAllNew(
                $this->_request->page,
                $this->_request->per_page, 
                $filter, 
                $this->_request->sort_field, 
                $this->_request->sort_dir, 
                $count
            );

            //MyLogs::debug($data, "============ APPROVALS DATA BEFORE ============");
           // MyLogs::debug($count, "============ APPROVALS COUNT ============");
            
            foreach ($data as $d){
                $d->b_user_type = $this->bu_user->type;
                //$d->total_ads  = "<span style=\"color:" . (($d->total_ads)?"#ccc":"#ff0000")."\">" . $d->total_ads ."</span>";
                 $d->description = mb_convert_encoding(html_entity_decode($d->description, ENT_HTML5, "Windows-1252" ), 'UTF-8', 'Windows-1252');
                 $d->description = preg_replace("`&#[0-9]+;`is",' ',$d->description);
            }

            $show_with_photos = ( isset( $req->gallery_view ) && $req->gallery_view == 1 );
            $show_with_description = ( isset( $req->description_view ) && $req->description_view == 1 );

            foreach ( $data as $i => $item ) {
                $data[$i]->city_slug = Model_Advertisements::translateDiacriaticsAndLigatures(mb_convert_encoding($item->city, "Windows-1252", "UTF-8"));
                
                if( $show_with_photos ){
                    $ad_param = $this->model->get($item->id);
                    $data[$i]->images = $this->_generateImages((array)$ad_param);
                    if($item->attached_video){
                        $data[$i]->video  = array("user" => $item->user_id, "video"  => $item->attached_video);
                    }
                }
                
                if( $show_with_description ){
                    $data[$i]->t_desc = array('title' => $data[$i]->title, 'description' => $data[$i]->description);
                }
                 
            }

            MyLogs::debug($data, "============ APPROVALS DATA AFTER ============");
            
            $this->view->data = array(
                'data' => $data,
                'count' => $count
            );
    }

    
  public function revisiondataAction(){
            $req = $this->_request;

            $filter = array(
                'id' => $req->id,
                'user_id' => $req->user_id,
                'title' => $req->title,
                'description' => $req->text,
                'status' => $req->status,
                'phone' => $req->phone,
                'category' => $req->category,
                'city' => $req->city,
                'active_package_id' => $req->active_package_id,
                'date_banned' => $req->date_banned,
                'filter_by' => $req->filter_by,
                'date_from' => $req->date_from,
                'date_to' => $req->date_to,
                'auto_approved' => $req->auto_approved,
                'fake_status' => $req->fake_status,
                'one_pic' => $req->one_pic,
                'no_pic' => $req->no_pic,
                'verified' => $req->verified,
                'ad_country' => $req->ad_country,
                'modified' => $req->modified,
                'has_map' => $req->has_map,
                'disable_comments' => $req->disable_comments
            );
           
          

            $data = $this->model->getAllRevisionList(
                $this->_request->page,
                $this->_request->per_page, 
                $filter, 
                $this->_request->sort_field, 
                $this->_request->sort_dir, 
                $count
            );

            foreach ($data as $d){
            	$d->b_user_type = $this->bu_user->type;
                $d->description = mb_convert_encoding(html_entity_decode($d->description, ENT_HTML5, "Windows-1252" ), 'UTF-8', 'Windows-1252');
                $d->description = preg_replace("`&#[0-9]+;`is",' ',$d->description);
                $d->title = preg_replace("`&#[0-9]+;`is",' ',$d->title);
            }

            $show_with_photos = ( isset( $req->gallery_view ) && $req->gallery_view == 1 );
            $show_with_description = ( isset( $req->description_view ) && $req->description_view == 1 );

            foreach ( $data as $i => $item ) {
                $data[$i]->city_slug = Model_Advertisements::translateDiacriaticsAndLigatures(mb_convert_encoding($item->city, "Windows-1252", "UTF-8"));

                if( $show_with_photos ){
                    $ad_param = $this->model->get($item->id);
                    $data[$i]->images = $this->_generateImages((array)$ad_param);
                    if($item->attached_video){
                        $data[$i]->video  = array("user" => $item->user_id, "video"  => $item->attached_video);
                    }
                }
                
                if( $show_with_description ){
                    $data[$i]->t_desc = array('title' => $data[$i]->title, 'description' => $data[$i]->description);
                }
            }

            $this->view->data = array(
                'data' => $data,
                'count' => $count
            );
	}

    
    public function revisionsAction(){
        /* FILTER PARAMS */
        $ses_filter = new Zend_Session_Namespace('default_users_data');
        $this->view->filter_params = $ses_filter->filter_params;
        $this->view->bu_user = $this->bu_user;
        
        /* FILTER PARAMS */
    }
    
	public function toggleActiveAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->model->toggle($id);
	}

	public function createAction()
	{
		
		//header('Content-Type: text/html; charset=windows-1252');
		header('Content-Type: text/html; charset=utf-8');
		$req = $this->_request;
		$model_users = new Model_Users();
		
		if ( $req->isPost() ) {
			$phone = null;
			$validator = new Cubix_Validator();
			$description = mb_convert_encoding($req->description, 'HTML-ENTITIES', "UTF-8");
			
			if ( ! $req->user_id ) {
				$validator->setError('user_id', 'Required');
			}
			elseif(!$model_users->userIdExists($req->user_id)){
				$validator->setError('user_id', 'No user with that ID');
			}
			
			if ( ! $req->description ) {
				$validator->setError('description', 'Required');
			}
			elseif($this->model->descriptionExists($description)){
				$validator->setError('description', 'Description exist');
			}
			
			if ( ! $req->title ) {
				$validator->setError('title', 'Required');
			}
			
			if ( ! $req->city ) {
				$validator->setError('city', 'Required');
			}
			
			if ( !$req->phone ) {
				$validator->setError('phone', 'Required');
			}
			else {
				$phone = preg_replace('/[^0-9]/', '', $req->phone);
			}

			if ( $validator->isValid() )
			{
				$data = array(
					'user_id' => $req->user_id,
					'title' =>  mb_convert_encoding($req->title, 'HTML-ENTITIES', "UTF-8"),
					'description' => $description,
					'city' => $req->city,
					'category' => $req->category,
					'phone' => $phone,
					'location' => $req->location,
					'status' => $req->status,
					'price' => intval($req->price),
					'price_conditions' => $req->price_desc,
					'date_added' => time(),
					'date_released' => time(),
					'comment' => $req->ad_comment
				);
				
                                $ads_id = $this->model->insert($data);
				$data['id'] = $ads_id;
                                Model_Activity::getInstance()->log('create_advertisement', array('data' => $data, 'ads_id' => $ads_id));
                                
				$image_esult = $this->model->savePostedImages($data, $this->_session->photos);
				if(is_array($image_esult)){
					$this->model->update($ads_id, $image_esult);
				}
				else{
					$validator->setError('user_id', $image_esult);
				}
				$this->_session->unsetAll();
			}
			
			die(json_encode($validator->getStatus()));
		}
		else{
			$this->_session->unsetAll();
		}
		
	}

	public function editAction()
	{
		//header('Content-Type: text/html; charset=windows-1252');
		header('Content-Type: text/html; charset=utf-8');
		$req = $this->_request;
		
        if(isset($req->rotate)){
            $imgRotate = array_filter($req->rotate, function($v, $k){
               return $v != 0 && $v != 360;    
            }, ARRAY_FILTER_USE_BOTH);
            
            $rotateKeys = array_keys($imgRotate);
            if(count($rotateKeys)){
                $adImages = new Model_AdvertisementPhotos();
                $imgs = $adImages->getImagesbyIds($rotateKeys);
                $image_configs = Zend_Registry::get('images_config');
                $newhash =  md5(microtime(true));
                foreach ($imgs as $image){ 
                    $category =  $image->ad_id;
                    $ads_path  = $image_configs['path'] . "/" ;    
                    $category = substr($category, 0, 2) . DIRECTORY_SEPARATOR . substr($category, 2) . DIRECTORY_SEPARATOR;
                    $image_path =  $ads_path . $category ;
                    
                    //IMAGE
                    $source = imagecreatefromjpeg($image_path  . "img-" . $image->hash . '.jpg');
                    $rotate = imagerotate($source, $imgRotate[$image->id], 0);
                    imagejpeg($rotate, $image_path . "img-" . $newhash . '.jpg');
                    
                    
                    //THUMB
                    $source = imagecreatefromjpeg($image_path  . "thumb-" . $image->hash . '.jpg');
                    $rotate = imagerotate($source, $imgRotate[$image->id], 0);
                    imagejpeg($rotate, $image_path . "thumb-" . $newhash . '.jpg');
                    list($width, $height, $type, $attr) = getimagesize($image_path . "img-" . $newhash . '.jpg');
                    if($width > $height){
                        $ar = 2; 
                    }elseif($width < $height){
                        $ar = 1; 
                    }else{
                        $ar = 3;
                    }  
                    $adImages->updateRotate($image->id, $width, $height, $ar,$newhash);
                     
                }
            }
        }
        
        $msc = microtime(true);
        
		$ads_id = $req->id;
		$ads = $this->model->get($ads_id);
        $cart = $this->model->getShoppingCart($ads_id);
        
        if($cart->status == 1 && $cart->package_id != 0 && $ads->status == 'new'){
            $this->view->has_payment = "<span class='new-payment'>This ad has Payment Attached to it</span>";
        }else{
            $this->view->has_payment = "";
        }
		$this->view->ad_data = $ads;

        $this->view->tags = Model_Tags::getAll();
        $selected_tags = Model_AdsTags::getAdTags($req->id);  
        $this->view->selected_tags = array_map(
                function($tag) { return get_object_vars($tag); },
                $selected_tags
        );
        
        //var_dump($this->view->selected_tags);
        
        
          // group 0
        $msc0 = microtime(true)-$msc;
        //echo  'group 0 :: ' . $msc0 . ' s ' . "\n"; // in seconds
        
		$user_id = $ads->user_id;
		$phone_number = $ads->phone;
		$model_user = new Model_Users(); 
		$ads_user = $model_user->get($user_id);
		
          // group 0
        $msc05 = microtime(true)-$msc;
        //echo  'group 0.5 :: ' . $msc05 . ' s ' . "\n"; // in seconds
        
//        $ads_user_history = $model_user->getUserLoginHistoryData($user_id);
        $ads_user_history = $model_user->getCountUserLoginHistoryData($user_id);

        // group 1
        $msc1 = microtime(true)-$msc;
        //echo  'group 1 :: ' . $msc1 . ' s ' . "\n"; // in seconds
        
		$user_ads = $this->model->getAllAdsByUserId($user_id);
		$active_ads = $this->model->getPublishedAdsByUserId($user_id);
		$inactive_ads = $this->model->getInactiveAdsByUserId($user_id);
		
        // group 2
        $msc2 = microtime(true)-$msc;
       // echo  'group 2 :: ' . $msc2 . ' s ' . "\n"; // in seconds
        
//        $new_ads = $this->model->getNewAdsByUserId($user_id);
        $new_ads = $this->model->getCountNewAdsByUserId($user_id);

        //$deleted_ads = $this->model->getDeletedAdsByUserId($user_id);
        $deleted_ads = $this->model->getCountDeletedAdsByUserId($user_id);
        //$expired_ads = $this->model->getExpiredAdsByUserId($user_id);
		$expired_ads = $this->model->getCountExpiredAdsByUserId($user_id);
        //$banned_ads = $this->model->getBannedAdsByUserId($user_id);
		$banned_ads = $this->model->getCountBannedAdsByUserId($user_id);
		
        // group 3
        $msc3 = microtime(true)-$msc;
        //echo  'group 3 :: ' . $msc3 . ' s ' . "\n"; // in seconds
        // -----------------------------
        $premium_ads_count = $this->model->getPremiumsCountByUserId($user_id);
		
        // group 4
        $msc4 = microtime(true)-$msc;
        //echo  'group 4 :: ' . $msc4 . ' s ' . "\n"; // in seconds
        
        $other_users_with_same_phone = $this->model->getUsersBySamePhone($user_id, $phone_number);
        
        // group 5
        $msc5 = microtime(true)-$msc;
        //echo  'group 5 :: ' . $msc5 . ' s ' . "\n"; // in seconds
        
        $adminModel = new Model_BOUsers();
        // ------------------------
        
         // group 4
        $msc6 = microtime(true)-$msc;
        //echo  'group 6 :: ' . $msc6 . ' s ' . "\n"; // in seconds
        
        if($ads->activated_by && $this->bu_user->type == 'superadmin'){
           
            $adminInfo = $adminModel->get($ads->activated_by);
            
            $approved_by = $adminInfo->username;
            
            
        }else{
            $approved_by = "N/A";
            $verified_by = "";
        }
        
        if($ads->verification_status && $this->bu_user->type == 'superadmin'){
            $adminVerify = $adminModel->get($ads->verified_by);
            $verified_by = " by " . $adminVerify->username;
        }else{
            $verified_by = " by  N/A";
        }
        
		$this->view->user_created_date = date('m/d/Y', $ads_user->date_created);
//        $this->view->user_login_count = count($ads_user_history);
		$this->view->user_login_count = $ads_user_history;
		$this->view->user_ads_count = count($user_ads);
		$this->view->user_active_ads_count = count($active_ads);
		$this->view->user_inactive_ads_count = count($inactive_ads);
//        $this->view->user_new_ads_count = count($new_ads);
		$this->view->user_new_ads_count = $new_ads;
        //$this->view->user_deleted_ads_count = count($deleted_ads);
		$this->view->user_deleted_ads_count = $deleted_ads;
        //$this->view->user_expired_ads_count = count($expired_ads);
		$this->view->user_expired_ads_count = $expired_ads;
        //$this->view->user_banned_ads_count = count($banned_ads);
		$this->view->user_banned_ads_count = $banned_ads;
		$this->view->other_users_with_same_phone_count = $other_users_with_same_phone ? count($other_users_with_same_phone) : 0;
		$this->view->premium_ads_count = $premium_ads_count;
                $this->view->approved_by = $approved_by;
		$this->view->verified_by = $verified_by;
                   
		$faked_ads = $this->model->getFakedAdsByUserId($user_id); 
		$this->view->faked_ads = $faked_ads;
		$this->view->active_ads = $active_ads;
        if($ads->ad_ip){
            $ip =long2ip($ads->ad_ip); 
            $loc = geo::getLocation($ip);
            $this->view->location =  $loc['city']. ", " . $loc['zone'] . ", ". $loc['countryName'] . "(" . $ip  . ")";
        }else{
            $this->view->location = "N/A"; 
        }

		$this->view->user_auto_approval = $ads_user->auto_approval;
         


        
		if ( $req->isPost() ) {
            
			$phone = null;
			$model_users = new Model_Users();
			$validator = new Cubix_Validator();
			$description = mb_convert_encoding($req->description, 'HTML-ENTITIES', "UTF-8");
			
			if ( ! $req->user_id ) {
				$validator->setError('user_id', 'Required');
			}
			elseif(!$model_users->userIdExists($req->user_id)){
				$validator->setError('user_id', 'No user with that ID');
			}
			
			if ( ! $req->title ) {
				$validator->setError('title', 'Required');
			}
			
			if ( ! $req->city ) {
				$validator->setError('city', 'Required');
			}
			
			if ( ! $req->description ) {
				$validator->setError('description', 'Required');
			}
			elseif($exists_id = $this->model->descriptionExists($description, $ads_id)){
				$validator->setError('description', 'Description exist in '. $exists_id);
			}
						
			if ( !$req->phone ) {
				$validator->setError('phone', 'Required');
			}
			else {
				$phone = preg_replace('/[^0-9]/', '', $req->phone);
			}

			if ( $validator->isValid() )
			{
				
				$data = array(
					'user_id' => $req->user_id,
					'title' =>  mb_convert_encoding($req->title, 'HTML-ENTITIES', "UTF-8"),
					'description' => $description,
					'city' => $req->city,
                    'category' => $req->category,
					'phone' => $phone,
					'location' => $req->location,
					'status' => $req->status,
					'price' => intval($req->price),
                    'price_min' => intval($req->price_min),
					'price_max' => intval($req->price_max),
                    'price_conditions' => $req->price_desc,
					'myfans_url' => $req->myfans_url,
					'comment' => $req->ad_comment
				);
                    $data["verification_status"] = $req->ad_verified;
                if($req->ad_verified){
                    
                    if (/*$ads->ad_verified == 'pending' &&*/ $req->ad_verified =='verified' ){
                        $data['verified_by'] = intval($this->bu_user->id);
                    }   
                }
                              
                $status = $this->model->getAdsStatus($ads_id);
                Model_Activity::getInstance()->log('edit_advertisement', array('data' => $data, 'ads_id' => $ads_id));
                if ($data['status'] !== $status) {
                    Model_Activity::getInstance()->log('status_advertisement', array('ads_id' => $ads_id, 'status' => $data['status']));
                    if($data['status'] == Model_Advertisements::STATUS_ACTIVE && $status == Model_Advertisements::STATUS_NEW){
                        $data['activated_by'] = intval($this->bu_user->id);
                }
                }
                if($data['status'] == Model_Advertisements::STATUS_ACTIVE ){
                    $data['date_released'] = time();
                }
                
				$this->model->update($req->id, $data);
				if($data['category'] !== $ads->category || $data['city'] !== $ads->city ){
					$data['id'] = $req->id;
                    if ($data['category'] !== $ads->category || $data['city'] !== $ads->city){
                        $OrderPackageModel = new Model_Billing_Packages();
                        $OrderPackageItem = $OrderPackageModel->getByAdId($ads_id);
                        if (!empty($OrderPackageItem)){
                            $OrderPackageItemObj = new Model_Billing_PackageItem(array('id'=>$OrderPackageItem['order_package_id'],'category'=>$data['category'], 'city'=>$data['city']));
                            $OrderPackageModel->save($OrderPackageItemObj);
                        }
                    }
				}
                // update tags
                Model_AdsTags::edit($ads_id, $req->tags );
                
                
                
                $this->_session->unsetAll();
			}
			die(json_encode($validator->getStatus()));
		}
		else{
			$this->_session->unsetAll();
			$this->view->ad_img = Model_AdvertisementPhotos::getImagesWebDetails(((array)$ads));;
		}
	}
	
	public function approveAction()
	{

		$req = $this->_request;
        $ads_id = $req->id;
        $ads = $this->model->get($ads_id);
        $user_id = $ads->user_id;
		if ( $req->isPost() ) {
			$phone = null;
            $model_users = new Model_Users();
			$validator = new Cubix_Validator();
            $description = mb_convert_encoding($req->description, 'HTML-ENTITIES', "UTF-8");
            
            if ( ! $req->user_id ) {
                $validator->setError('user_id', 'Required');
            }
            elseif(!$model_users->userIdExists($req->user_id)){
                $validator->setError('user_id', 'No user with that ID');
			}
			
            if ( ! $req->title ) {
                $validator->setError('title', 'Required');
            }
            
            if ( ! $req->city ) {
                $validator->setError('city', 'Required');
            }
            
            if ( ! $req->description ) {
                $validator->setError('description', 'Required');
            }
            elseif($exists_id = $this->model->descriptionExists($description, $ads_id)){
                $validator->setError('description', 'Description exist in '. $exists_id);
            }
                        
            if ( !$req->phone ) {
                $validator->setError('phone', 'Required');
            }
            else {
                $phone = preg_replace('/[^0-9]/', '', $req->phone);
            }

			if ( $validator->isValid() )
			{				
                
				$data = array(
                    'user_id' => $req->user_id,
                    'title' =>  mb_convert_encoding($req->title, 'HTML-ENTITIES', "UTF-8"),
                    'description' => $description,
                    'city' => $req->city,
                    'category' => $req->category,
                    'phone' => $phone,
                    'location' => $req->location,
					'status' => Model_Advertisements::STATUS_ACTIVE,
                    'price' => intval($req->price),
                    'price_conditions' => $req->price_desc,
                    'is_updated'    => 1,
                    'comment' => $req->ad_comment
				);
                if($req->ad_verified){
                    $data["verification_status"] = $req->ad_verified;
                    if (/*$ads->ad_verified == 'pending' &&*/ $req->ad_verified =='verified' ){
                        $data['verified_by'] = intval($this->bu_user->id);
                    }   
                }
                $status = $this->model->getAdsStatus($ads_id);
                Model_Activity::getInstance()->log('edit_advertisement', array('data' => $data, 'ads_id' => $ads_id));
                if ($data['status'] !== $status) {
                    Model_Activity::getInstance()->log('status_advertisement', array('ads_id' => $ads_id, 'status' => $data['status']));
                    if($data['status'] == Model_Advertisements::STATUS_ACTIVE && $status == Model_Advertisements::STATUS_NEW){
                        $data['activated_by'] = intval($this->bu_user->id);
                        $cartinfo = $this->model->getShoppingCart($ads_id);
                        
                        if( $transfer_id = $cartinfo->transaction_id){
                            $model_transfers = new Model_Billing_Transfers();
                            $transfer = $model_transfers->getDetails($transfer_id);
                            
                            if($cartinfo->payment_processor == 'bitcoin' || $cartinfo->payment_processor == 'eth'){
                                $transfer->autoConfirm($comment, Model_Billing_OnlineBilling::get_bitcoin_price($cartinfo->package_id), 199); //89 katie    
                            }else{
                                $transfer->autoConfirm($comment, $cartinfo->amount, 199);
                            }
                            
                        }
                        //$data['date_released'] = time();
                    }
                    if($data['status'] == Model_Advertisements::STATUS_ACTIVE ){
                        
                        $data['date_released'] = time();
                    }
                }
                
				$this->model->update($req->id, $data);
                $ad_photos = new Model_AdvertisementPhotos();
                $ad_photos->approveRevision($ads_id);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function uploadHtml5Action()
	{
		$tmp_dir = sys_get_temp_dir();
		
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		try {						
					
			$response = array(
				'id'	=> $_SERVER['HTTP_X_FILE_ID'],
				'name'	=> $_SERVER['HTTP_X_FILE_NAME'],
				'size'	=> $_SERVER['HTTP_X_FILE_SIZE'],
				'error'	=> 0,
				'msg' => '',
				'finish'	=> TRUE
			);
			
			if ( count($this->_session->photos) >= 5 ) {
				throw new Exception("You've reached maximum amount of pictures ( 5 )");
			}
			
			$image_configs = Zend_Registry::get('images_config');
			
			//$file = $tmp_dir . DIRECTORY_SEPARATOR . $response['name'];
			$ext = strtolower(@end(explode('.', $response['name'])));
			$tmp = $image_configs['path'] . DIRECTORY_SEPARATOR . 'tmp/';
						
			if(!is_dir($tmp)){
				mkdir($tmp);
			}
			
			$filename = uniqid('ads_img') . '.' . $ext;
			$file = $tmp . $filename;
			file_put_contents($file, file_get_contents('php://input'));
			$response['photo'] = $image_configs['temp_url'] . $filename;
			$response['photo_id'] = $filename;		
			$photo = array(
				'tmp_name'	=> $file
			);
									
			$this->_session->photos[$filename] = $photo;
			$this->_session->photos_updated = true;
			
		} catch (Exception $e) {
			$response['error']	= 1;
			$response['msg']	= $e->getMessage();
		}		
		
		echo json_encode($response);die;
	}
	
	public function ajaxRemovePhotoAction()
	{
		$image_id = $this->_request->image_id;
		$status = array('status' => 'error');
		
		if ( isset($this->_session->photos[$image_id]) ) {
			unset($this->_session->photos[$image_id]);
			$this->_session->photos_updated = true;
			$status = array('status' => 'success');
		}
		
		die(json_encode($status));
	}
	
	public function removeAction()
	{
            if(is_array($this->_request->id)){
                    foreach($this->_request->id as $id){
                            $this->model->remove($id);
                            Model_Activity::getInstance()->log('delete_advertisement', array('id' => $id));
                    }
            }
            else{
                    $id = intval($this->_request->id);
                    $this->model->remove($id);
                    Model_Activity::getInstance()->log('delete_advertisement', array('id' => $id));
            }

            die;
	}

	public function fakeAction()
	{
        if(is_array($this->_request->id)){
            foreach($this->_request->id as $id){
                $this->model->makeFake($id);
            }
        }
        else{
            $id = intval($this->_request->id);
            $this->model->makeFake($id);
        }

        die;
	}
	
	public function savefakeAction() {
        
        $req = $this->_request;
        
        $ads_id = $req->id;
        $ads = $this->model->get($ads_id);
        $this->view->ad_data = $ads;

        $user_id = $ads->user_id;
        
        if ( $req->isPost() ) {
            $phone = null;
            $model_users = new Model_Users();
            $validator = new Cubix_Validator();
            $description = mb_convert_encoding($req->description, 'HTML-ENTITIES', "UTF-8");
            
            if ( ! $req->user_id ) {
                $validator->setError('user_id', 'Required');
            }
            elseif(!$model_users->userIdExists($req->user_id)){
                $validator->setError('user_id', 'No user with that ID');
            }
            
            if ( ! $req->title ) {
                $validator->setError('title', 'Required');
            }
            
            if ( ! $req->city ) {
                $validator->setError('city', 'Required');
            }
            
            if ( ! $req->description ) {
                $validator->setError('description', 'Required');
            }
            elseif($exists_id = $this->model->descriptionExists($description, $ads_id)){
                $validator->setError('description', 'Description exist in '. $exists_id);
            }
                        
            if ( !$req->phone ) {
                $validator->setError('phone', 'Required');
            }
            else {
                $phone = preg_replace('/[^0-9]/', '', $req->phone);
            }

            if ( $validator->isValid() )
            {
                
                $data = array(
                    'user_id'           => $req->user_id,
                    'title'             =>  mb_convert_encoding($req->title, 'HTML-ENTITIES', "UTF-8"),
                    'description'       => $description,
                    'city'              => $req->city,
                    'category'          => $req->category,
                    'phone'             => $phone,
                    'location'          => $req->location,
                    'price'             => intval($req->price),
                    'price_conditions'  => $req->price_desc,
                    'comment'           => $req->ad_comment,
                    'status'            => $req->status,
                    'fake_status'       => Model_Advertisements::STATUS_FAKE, // mark as fake
                );
                
                if($data['status'] == Model_Advertisements::STATUS_NEW) {
                    $data['status'] = Model_Advertisements::STATUS_ACTIVE;    
                }
                
                
                
                $status = $this->model->getAdsStatus($ads_id);
                Model_Activity::getInstance()->log('edit_advertisement', array('data' => $data, 'ads_id' => $ads_id));
                if ($data['status'] !== $status) {
                    Model_Activity::getInstance()->log('status_advertisement', array('ads_id' => $ads_id, 'status' => $data['status']));
                }
                $this->model->update($req->id, $data);
                if($data['category'] !== $ads->category || $data['city'] !== $ads->city ){
                    $data['id'] = $req->id;
                    if ($data['category'] !== $ads->category || $data['city'] !== $ads->city){
                        $OrderPackageModel = new Model_Billing_Packages();
                        $OrderPackageItem = $OrderPackageModel->getByAdId($ads_id);
                        if (!empty($OrderPackageItem)){
                            $OrderPackageItemObj = new Model_Billing_PackageItem(array('id'=>$OrderPackageItem['order_package_id'],'category'=>$data['category'], 'city'=>$data['city']));
                            $OrderPackageModel->save($OrderPackageItemObj);
                        }
                    }
                }
                $this->_session->unsetAll();
            }
            die(json_encode($validator->getStatus()));
        }
    }
	
	public function removeFakeAction()
	{
		
        if(is_array($this->_request->id)){
            foreach($this->_request->id as $id){
                $this->model->removeFake($id);
            }
        }
        else{
            $id = intval($this->_request->id);
            $this->model->removeFake($id);
        }

        die;
	}

    public function deactivateAction()
	{
            if(is_array($this->_request->id)){
                foreach($this->_request->id as $id){
                    $this->model->deactivate($id);
                }
            }
            else{
                $id = intval($this->_request->id);
                $this->model->deactivate($id);
            }

            die;
	}

    public function activateAction()
	{
            if(is_array($this->_request->id)){
                foreach($this->_request->id as $id){
                    $adInfo = $this->model->get($id);
                    
                    $cartinfo = $this->model->getShoppingCart($id);
                   // var_dump($cartinfo);
//                    die("a1");
                    $this->model->activateIt($id, $adInfo->status, $this->bu_user->id);
                    if($adInfo->status == Model_Advertisements::STATUS_NEW ){
                        $data['activated_by'] = intval($this->bu_user->id);
                        $cartinfo = $this->model->getShoppingCart($id);
                        
                        if( $transfer_id = $cartinfo->transaction_id){
                            
                            $model_transfers = new Model_Billing_Transfers();
                            $transfer = $model_transfers->getDetails($transfer_id);
                            
                            if($cartinfo->payment_processor == 'bitcoin' || $cartinfo->payment_processor == 'eth'){
                                $transfer->xadconfirm($transfer_id, Model_Billing_OnlineBilling::get_bitcoin_price($cartinfo->package_id), 199); //89 katie    
                            }else{
                                $transfer->xadconfirm($transfer_id, $cartinfo->amount, 199);
                            }
                            
                        }
                        //$data['date_released'] = time();
                    }
                }
            }
            else{
                $id = intval($this->_request->id);
                $adInfo = $this->model->get($id);

                $this->model->activateIt($id, $adInfo->status, $this->bu_user->id);
                if($adInfo->status == Model_Advertisement::STATUS_NEW ){
                    $data['activated_by'] = intval($this->bu_user->id);
                    $cartinfo = $this->model->getShoppingCart($id);
                    if( $transfer_id = $cartinfo->transaction_id){
                        $model_transfers = new Model_Billing_Transfers();
                        $transfer = $model_transfers->getDetails($transfer_id);
                        
                        if($cartinfo->payment_processor == 'bitcoin' || $cartinfo->payment_processor == 'eth'){
                            $transfer->xadconfirm($transfer_id, Model_Billing_OnlineBilling::get_bitcoin_price($cartinfo->package_id), 199); //89 katie    
                        }else{
                            $transfer->xadconfirm($transfer_id, $cartinfo->amount, 199);
                        }
                        
                    }
                    //$data['date_released'] = time();
                }
                
            }

            die;
	}
    public function activateRevisionsAction(){
            if(is_array($this->_request->id)){
                foreach($this->_request->id as $id){
                   $revision_data = (array)$this->model->getRevisionCleanData($id);

                   if($exists_id = $this->model->descriptionExists($revision_data['description'], $id)){
                       unset($revision_data['description']);
                   }
                   
                   if($revision_data) {
                        unset($revision_data['id']);
                        unset($revision_data['ads_id']);
                        unset($revision_data['date_added']);
                        unset($revision_data['status']);
                        unset($revision_data['user_id']);
                        unset($revision_data['approved_status']);
                        unset($revision_data['revision_ip']);
                        unset($revision_data['rev_country']);
                        unset($revision_data['rev_location']);
                        unset($revision_data['rev_country_id']);
                        
                    }

                    $revision_data['is_updated'] = '0';
                    MyLogs::debug($this->bu_user->id, "activateRevisionsAction::adminID::$id" );
                    MyLogs::debug($revision_data, "activateRevisionsAction::data::$id" );
                    $this->model->update($id, $revision_data);
                    MyLogs::debug("-", "activateRevisionsAction::after::$id" );
                    $this->model->approveRevisionStatus($id);
                    MyLogs::debug("approveRevisionStatus", "activateRevisionsAction::data::$id" );
                    
                    $ad_photos = new Model_AdvertisementPhotos();
                    $ad_photos->approveRevision($id);
                    MyLogs::debug("Model_AdvertisementPhotos", "activateRevisionsAction::approveRevision::$id" );
                }
            }
            else{
                $id = intval($this->_request->id);
                $revision_data = (array)$this->model->getRevisionCleanData($id);
                if($revision_data) {
                    unset($revision_data['id']);
                    unset($revision_data['ads_id']);
                    unset($revision_data['date_added']);
                    unset($revision_data['status']);
                    unset($revision_data['user_id']);
                    unset($revision_data['approved_status']);
                    unset($revision_data['revision_ip']);
                    unset($revision_data['rev_country']);
                    unset($revision_data['rev_location']);
                    unset($revision_data['rev_country_id']);
                }
                
                if($exists_id = $this->model->descriptionExists($revision_data['description'], $id)){
                       unset($revision_data['description']);
                }

                $revision_data['is_updated'] = '0';
                MyLogs::debug($this->bu_user->id, "activateRevisionsAction::adminID::$id" );
                MyLogs::debug($revision_data, "activateRevisionsAction::data::$id" );
                $this->model->update($id, $revision_data);
                MyLogs::debug("-", "activateRevisionsAction::after" );
                $this->model->approveRevisionStatus($id);
                MyLogs::debug("approveRevisionStatus", "activateRevisionsAction::data::$id" );
                $ad_photos = new Model_AdvertisementPhotos();
                $ad_photos->approveRevision($id);
                MyLogs::debug("Model_AdvertisementPhotos", "activateRevisionsAction::approveRevision::$id" );
                
            }

            die;
    }

    public function multiCommentAction()
    {
            $this->view->layout()->disableLayout();
            
            if( $this->_request->isPost() ){
                $data = new Cubix_Form_Data($this->_request);

                $params = array(
                    'ad_id' => 'arr-int',
                    'ad_comment' => ''
                );

                $data->setFields($params);

                $data = $data->getData();

                $validator = new Cubix_Validator();

                /*if ( !strlen($data['ad_comment']) ) {
                    $validator->setError('ad_comment', 'Required');
                }*/

                if ( $validator->isValid() ) {
                    $data_va['ad_comment'] = $data['ad_comment'];
                    
                    foreach($data['ad_id'] as $ad_id)
                    {
                        $this->model->addMultiComment($data_va, $ad_id);
                    }
                }

                die(json_encode($validator->getStatus()));
            }
            else{
                $ad_ids = $this->_request->id;
                $this->view->ad_ids = is_array($ad_ids) ? $ad_ids : array($ad_ids);
            }
    }
	public function banformAction()
	{
            $this->view->layout()->disableLayout();
            
           // if( $this->_request->isPost() ){
//                $data = new Cubix_Form_Data($this->_request);
//
//                $params = array(
//                    'ad_id' => 'arr-int',
//                    'ad_comment' => ''
//                );
//
//                $data->setFields($params);
//
//                $data = $data->getData();
//
//                $validator = new Cubix_Validator();
//
//                /*if ( !strlen($data['ad_comment']) ) {
//                    $validator->setError('ad_comment', 'Required');
//                }*/
//
//                if ( $validator->isValid() ) {
//                    $data_va['ad_comment'] = $data['ad_comment'];
//                    
//                    foreach($data['ad_id'] as $ad_id)
//                    {
//                        $this->model->addMultiComment($data_va, $ad_id);
//                    }
//                }
//
//                die(json_encode($validator->getStatus()));
//            }
//            else{
                $ad_ids = $this->_request->user_id;
                $this->view->ids = is_array($ad_ids) ? $ad_ids : array($ad_ids);

                // get users info 

                // check if users have packages

                $users_data = [];
                $model_users = new Model_Users();
                foreach($this->view->ids as $user_id) {
                    
                    $user = $model_users->get($user_id);
                        
                    $users_data[$user_id] = [
                        'username'      => $user->username,
                        'credits'       => $user->credits,
                        'comment'       => $user->comment,
                        'status'        => Model_Users::$statuses[$user->date_banned],
                        'has_packages'  => $model_users->checkUserOrders($user_id)
                    ];    

                }
                
                $this->view->users_data = $users_data;
                //var_dump($this->view->users_data); 




//            }
	}
	
	private function _generateImages($ads_data)
	{
	    $images = Model_AdvertisementPhotos::getCurrentImages($ads_data['id']);

            if(is_array($images) && count($images) > 0){
                foreach($images as $image){
                    $images_display[$image->id] = Model_AdvertisementPhotos::getImagePath((array)$image, TRUE, TRUE);
                }
            }

            return $images_display;
	}	

	public function editExpirationDateAction()
	{
		$ad_id = intval($this->_getParam('ad_id'));
		$m_packages = new Model_Billing_Packages();

		$package = $m_packages->getByAdId($ad_id);
		
		
		//FIXING PACKAGES LOGIC
		//IF PACKAGE EXPIRES 19 APRIL IT'S ACTUALLY EXPIRES 18 APRIL 23:59
		// -1 day sales

		$package->expiration_date = date('d M Y', strtotime($package->expiration_date)); 
		$this->view->package = $package;
		if ( $this->_request->isPost() ) {

			$new_exp_date = $this->_request->new_expiration_date;
			$op_id = $this->_request->op_id;
			$ad_id = $this->_request->ad_id;
			$timezone_diff = $this->_request->timezone_diff;

			try {
				$new_exp_date = $new_exp_date ? ($new_exp_date + (4-$timezone_diff/60)*60*60) : $new_exp_date;
				$m_packages->updateExpirationDate($new_exp_date, $op_id, $ad_id);

				Cubix_SyncNotifier::notify($ad_id, Cubix_SyncNotifier::EVENT_ESCORT_PACKAGE_EXPIRATION_UPDATED, array('id' => $op_id));
			}
			catch ( Exception $e ) {

			}
		}
	}

    public function photosAction() {

        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        /* @var $request Zend_Controller_Request_Http */
        $request = $this->getRequest();
        $photoIds = $this->_getParam('photos');
        $ad_id = $this->_getParam('ad_id');
        $action = $this->_getParam('a');
        $success = 1;
        $message = '';
        $result = array(
            'success' => $success,
            'error' => $message,
        );
        switch ($action) {
            case "upload":
                try {

                    $response = array(
                        'id'	=> $request->getHeader('X_FILE_NAME'),
                        'name'	=> $request->getHeader('X_FILE_NAME'),
                        'size'	=> $request->getHeader('X_FILE_SIZE'),
                        'error'	=> 0,
                        'msg' => '',
                        'finish'	=> TRUE
                    );
                    
                    if ( Model_AdvertisementPhotos::getCountImages($ad_id) >= 5 ) {
                        throw new Exception("You've reached maximum amount of pictures ( 5 )");
                    }

                    $image_configs = Zend_Registry::get('images_config');

                    //$file = $tmp_dir . DIRECTORY_SEPARATOR . $response['name'];
                    $ext = strtolower(@end(explode('.', $response['name'])));
                    $tmp = $image_configs['path'] . DIRECTORY_SEPARATOR . 'tmp/';

                    if(!is_dir($tmp)){
                        mkdir($tmp);
                    }

                    $filename = uniqid('ads_img') . '.' . $ext;
                    $file = $tmp . $filename;
                    $size = file_put_contents($file, file_get_contents('php://input'));
                    $err = UPLOAD_ERR_OK;
                    if ($size === false){
                        $err = UPLOAD_ERR_CANT_WRITE;;
                    }
                    elseif ($size == 0){
                        $err = UPLOAD_ERR_NO_FILE;
                    }
                    elseif ($size != $response['size']){
                        $err = UPLOAD_ERR_PARTIAL;
                    }
                    $response['photo'] = $image_configs['temp_url'] . $filename;
                    $response['photo_id'] = $filename;
                    $photo = array(
                        array(
                            'tmp_name'	=> $file,
                            'name'	=> $filename,
                            'size'=> (int) $response['size'],
                            'error' => $err,
                            "type"=> mime_content_type($file)
                        )
                    );
                    
                    $image_validity = Cubix_Images::areJPEGImagesInputValid($photo);
                    switch($image_validity)
                    {
                        case \Cubix_Images::NO_ERROR:
                            break;
                        case \Cubix_Images::ERROR_IMAGE_SIZE_IS_TOO_BIG:
                            throw  new Exception("{$response['name']} is too big in size. Please make sure images are no bigger than 2MB");
                            break;
                        case \Cubix_Images::ERROR_IMAGE_TYPE_IS_INVALID:
                            throw  new Exception("{$response['name']} is an invalid type of image. Please make sure images are all JPEG");
                            break;
                        case \Cubix_Images::ERROR_IMAGE_ERROR_UNHANDLED:
                            throw  new Exception("{$response['name']} failed to be resampled. Please make sure the file is actually a JPEG image");
                        case \Cubix_Images::ERROR_IMAGE_RESAMPLING_FAILED:
                            throw  new Exception("An unhandled error occurred while uploading the images. Please try again");
                            break;
                        default:
                            if (is_array($image_validity) && !empty($image_validity['msg'])){
                                throw  new Exception($image_validity['msg']);
                            }else{
                                throw  new Exception("An unhandled error occurred while uploading the images. Please try again");
                            }
                            break;
                    }

                    $image_result = Model_AdvertisementPhotos::savePostedImages($ad_id, $photo);
                    
                    // make sure at least one photo is main
                    Model_AdvertisementPhotos::checkMain($ad_id);
                    
                    
                    if (is_array($image_result)){
                        $response['photo'] = $image_result;
                    }else{
                        throw  new Exception($image_result);
                    }
                    unlink($file);
                } catch (Exception $e) {
                    $response['error']	= 1;
                    $response['msg']	= $e->getMessage();
                }

                echo json_encode($response);die;
                break;
            case "sort":
                if (empty($photoIds)) {
                    $message = "Select Images to sort";
                    break;
                }
                if (!Model_AdvertisementPhotos::canProcessImages($photoIds, $ad_id)) {
                    $message = "Something went wrong";
                    $success = 0;
                    break;
                }
                Model_AdvertisementPhotos::reorder($photoIds);
                break;
            case "set_main":
                if (empty($photoIds)) {
                    $message = "Please choose an image(s)";
                    $success = 0;
                    break;
                }
                if (count($photoIds) > 1) {
                    $message = "Please choose an image(s)";
                    $success = 0;
                    break;
                }
                if (!Model_AdvertisementPhotos::canProcessImages($photoIds, $ad_id)) {
                    $message = "Something went wrong";
                    $success = 0;
                    break;
                }
                Model_AdvertisementPhotos::setMain($photoIds[0], $ad_id);
                break;
            case "remove":
                if (empty($photoIds)) {
                    $message = "Please choose an image(s)";
                    $success = 0;
                    break;
                }
                if (!Model_AdvertisementPhotos::canProcessImages($photoIds, $ad_id)) {
                    $message = "Something went wrong";
                    $success = 0;
                    break;
                }

                Model_AdvertisementPhotos::delete($photoIds, $ad_id);
                $mainImage = Model_AdvertisementPhotos::getMainImage($ad_id);
                $result['main'] = $mainImage['id'];
                $images = Model_AdvertisementPhotos::getImages($ad_id);
                $result['count'] = count($images);
                break;
            default:
                $message = "Something went wrong";
                $success = 0;
                break;
        }

        $result['error'] = $message;
        $result['success'] = $success;
        echo json_encode($result);
        die;
	}

	public function approveRevisionAction()
	{
		$req = $this->_request;
		if ( $req->isPost() ) {
            
            
            if(isset($req->rotate)){
                
                $imgRotate = array_filter($req->rotate, function($v, $k){
                   return $v != 0 && $v != 360;    
                }, ARRAY_FILTER_USE_BOTH);

                $rotateKeys = array_keys($imgRotate);
                if(count($rotateKeys)){
                    $adImages = new Model_AdvertisementPhotos();
                    $imgs = $adImages->getImagesbyIds($rotateKeys);
                    $image_configs = Zend_Registry::get('images_config');

                    foreach ($imgs as $image){ 
                        $category =  $image->ad_id;
                        $ads_path  = $image_configs['path']  ;    
                        $category = substr($category, 0, 2) . DIRECTORY_SEPARATOR . substr($category, 2) . DIRECTORY_SEPARATOR;
                        $image_path =  $ads_path . $category ;
                        $newhash =  md5(microtime(true));
                        
                      
                        //IMAGE
                        $source = imagecreatefromjpeg($image_path  . "img-" . $image->hash . '.jpg');
                        $rotate = imagerotate($source, $imgRotate[$image->id], 0);
                       
                        imagejpeg($rotate, $image_path . "img-" . $newhash . '.jpg');
                        
                        //THUMB
                        $source = imagecreatefromjpeg($image_path  . "thumb-" . $image->hash . '.jpg');
                        $rotate = imagerotate($source, $imgRotate[$image->id], 0);
                        imagejpeg($rotate, $image_path . "thumb-" . $newhash . '.jpg');
                        list($width, $height, $type, $attr) = getimagesize($image_path . "img-" . $image->hash . '.jpg');
                        if($width > $height){
                            $ar = 2; 
                        }elseif($width < $height){
                            $ar = 1; 
                        }else{
                            $ar = 3;
                        }
                       
                        $adImages->updateRotate($image->id, $width, $height, $ar, $newhash);

                    }
                }
            }
			
			$validator = new Cubix_Validator();
			if ( ! $req->id ) {
				$validator->setError('id', 'Required');
			}

			if ( $validator->isValid() )
			{			
                $id= $req->id;
				$revision_data = (array)$this->model->getRevisionCleanData($req->id);
                $revision_data['description'] = $_POST['description'];
				if($revision_data) {
					unset($revision_data['id']);
					unset($revision_data['ads_id']);
					unset($revision_data['date_added']);
					unset($revision_data['status']);
					unset($revision_data['user_id']);
					unset($revision_data['approved_status']);
					unset($revision_data['revision_ip']);
                    unset($revision_data['rev_country']);
                    unset($revision_data['rev_location']);
                    unset($revision_data['rev_country_id']);
				}

                $revision_data['is_updated'] = '0';
				$revision_data['date_released'] = time();
                MyLogs::debug($this->bu_user->id, "approveRevisionAction::adminID::$id" );
                MyLogs::debug($revision_data, "approveRevisionAction::data" );
				$this->model->update($req->id, $revision_data);
                MyLogs::debug($revision_data, "approveRevisionAction::beforeRevisionStatus" );
                
				$ad_photos = new Model_AdvertisementPhotos();
				$ad_photos->approveRevision($req->id);
                MyLogs::debug($revision_data, "approveRevisionAction::approveRevision" );
                
                $this->model->approveRevisionStatus($req->id);
                MyLogs::debug($revision_data, "approveRevisionAction::afterRevisionStatus" );
                
				
                
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function editRevisionAction()
	{
		//header('Content-Type: text/html; charset=windows-1252');
		header('Content-Type: text/html; charset=utf-8');
		$req = $this->_request;
		
		$ads_id = $req->id;
		$ads = $this->model->get($ads_id);
		$ads_revision = $this->model->getRevisionData($ads_id);

		if($ads_revision) {
		    unset($ads_revision->id);
		    unset($ads_revision->ads_id);
		    unset($ads_revision->date_added);
		    unset($ads_revision->status);
		    unset($ads_revision->user_id);
		    unset($ads_revision->approved_status);

		    $ads = (object)array_merge((array)$ads, (array)$ads_revision);
		}

		$this->view->ad_data = $ads;

		$user_id = $ads->user_id;
		$phone_number = $ads->phone;
		$model_user = new Model_Users(); 
		$ads_user = $model_user->get($user_id);
		//$ads_user_history = $model_user->getUserLoginHistoryData($user_id);
        $ads_user_history = $model_user->getCountUserLoginHistoryData($user_id);

		$user_ads = $this->model->getAllAdsByUserId($user_id);
		$active_ads = $this->model->getPublishedAdsByUserId($user_id);
		$inactive_ads = $this->model->getInactiveAdsByUserId($user_id);
		//$deleted_ads = $this->model->getDeletedAdsByUserId($user_id);
//		$expired_ads = $this->model->getExpiredAdsByUserId($user_id);
//		$banned_ads = $this->model->getBannedAdsByUserId($user_id);

        $deleted_ads = $this->model->getCountDeletedAdsByUserId($user_id);
        $expired_ads = $this->model->getCountExpiredAdsByUserId($user_id);
        $banned_ads = $this->model->getCountBannedAdsByUserId($user_id);    
       
		$premium_ads_count = $this->model->getPremiumsCountByUserId($user_id);
		$other_users_with_same_phone = $this->model->getUsersBySamePhone($user_id, $phone_number);
		$isPayed = $this->model->getAdIsPremium($ads_id);

		$this->view->user_created_date = date('m/d/Y', $ads_user->date_created);
//        $this->view->user_login_count = count($ads_user_history);
		$this->view->user_login_count = $ads_user_history;
		$this->view->user_ads_count = count($user_ads);
		$this->view->user_active_ads_count = count($active_ads);
		$this->view->user_inactive_ads_count = count($inactive_ads);
		//$this->view->user_deleted_ads_count = count($deleted_ads);
//		$this->view->user_expired_ads_count = count($expired_ads);
//		$this->view->user_banned_ads_count = count($banned_ads);

        $this->view->user_deleted_ads_count = $deleted_ads;
        $this->view->user_expired_ads_count = $expired_ads;
        $this->view->user_banned_ads_count = $banned_ads;
    
		$this->view->other_users_with_same_phone_count = $other_users_with_same_phone ? count($other_users_with_same_phone) : 0;
		$this->view->premium_ads_count = $premium_ads_count;
		$this->view->activepackage = $isPayed;

		$faked_ads = $this->model->getFakedAdsByUserId($user_id); 
		$this->view->faked_ads = $faked_ads;
		$this->view->active_ads = $active_ads;
		if($ads->ad_ip){
		    $ip = long2ip($ads->ad_ip); 
            $loc = geo::getLocation($ip);
		    $this->view->location =  $loc['city']. ", " . $loc['zone'] . ", ". $loc['countryName'] . "(" . $ip  . ")";
		}else{
		    $this->view->location = "N/A"; 
		}
        if($ads_revision->revision_ip){
            $ip = long2ip($ads_revision->revision_ip); 
            $loc = geo::getLocation($ip) ;
            $this->view->revlocation = $loc['city']. ", " . $loc['zone']. ", " . $loc['countryName'] . "(" . $ip  . ")";
        }else{
            $this->view->revlocation = "N/A"; 
        }

		$this->_session->unsetAll();
		$this->view->ad_img = Model_AdvertisementPhotos::getImagesWebDetails(((array)$ads));
	}
    public function unlinkvideoAction(){
         $req = $this->_request;
         if ( $req->isPost() ) {
            $this->model->unlinkvideo($req->id);
         }
         die(json_encode(array("removed" =>"true")));
    }
    
    
    public static function htmlEncodeText($text, $as_references = FALSE)
    {
        //return "111";
        $text = str_ireplace('&#39;', "'", $text);
        $text = str_ireplace('&#34;', '"', $text);

        if(self::$html_translation_table === NULL)
            self::$html_translation_table = get_html_translation_table(HTML_ENTITIES, NULL, 'ISO-8859-1');
         
        unset(self::$html_translation_table['&']);

        if($as_references === TRUE)
        {
            foreach (self::$html_translation_table as $character => $encoded_value)
                self::$html_translation_table[$character]= '&#'. ord($character) . ';';    
        }

        $translatedCharters = strtr($text, self::$html_translation_table);
        $translatedCharters = preg_replace('/\&#(\d+);/', ' ', $translatedCharters);

        $translatedCharters = str_ireplace("'", '&#39;', $translatedCharters);
        $translatedCharters = str_ireplace('"', '&#34;', $translatedCharters);

        return $translatedCharters;
    }
    public function testAction(){
         $packages = Model_Advertisements::getTempList();
        $from = "support@humpchies.com";
        $fromName = "Support Humpchies";          
        
        
        foreach($packages as $package ){
           $sent['user_id']= $package->id ; 
           $sent['username']= $package->username; 
           $sent['email']= $package->email; 
           $sent['language']= $package->language; 
           $sent['last_login']= $package->last_login; 
           $sent['sent']= "Y"; 
           $sent['sent_date']= date("Y-m-d H:i:s");
           var_dump($sent); 
           //Model_Advertisements::updateSent($sent);
        }   
        die();
    }
}
