<?php

class SeoDescriptionController extends Zend_Controller_Action
{
    protected $model;

    public function init()
    {
        $this->model = new Model_SeoDescription();
        $this->bu_user = Zend_Auth::getInstance()->getIdentity();
    }

    public function indexAction()
    {
          
        //$onlineBilling->markChargeBack();
        /* FILTER PARAMS */
        $ses_filter = new Zend_Session_Namespace('default_seodescription_data');
        $this->view->filter_params = $ses_filter->filter_params;
        $this->view->bu_user = $this->bu_user;
        
        /* FILTER PARAMS */
    }

    public function dataAction()
    {
        $req = $this->_request;

        $filter = array(
        'city_id' => $req->city_id , 
        'region_id' => $req->region_id,
            '' );
        
        $data = $this->model->getAll(
            $req->page,
            $req->per_page,
            $filter,
            $req->sort_field,
            $req->sort_dir,
            $count
        );

        $this->view->data = array(
            'data' => $data,
            'count' => $count
        );
        
        // get cities
        // get regions
        $this->view->cities = Model_Cities::getAll();
        $this->view->regions = Model_Regions::getAll();
        
    }

    public function createAction()
    {
        $this->view->layout()->disableLayout();
        $this->view->cities = Model_Cities::getAll();
        $this->view->regions = Model_Regions::getAll();
        
        if ($this->_request->isPost()) {
            
            $data = new Cubix_Form_Data($this->_request);
            
            $data->setFields(array(
                'city_id' => 'int',
                'region_id' => 'int',
                'page' => 'int',
                'fr' => '',
                'en' => '',
            ));

            $data = $data->getData();
            
            $validator = new Cubix_Validator();

            //if ( ! $data['city_id'] ) {
//                $validator->setError('City', 'Required');
//            }
//            
//            if ( ! $data['fr'] ) {
//                $validator->setError('French Description', 'Required');
//            }
//            
//            if ( ! $data['en'] ) {
//                $validator->setError('English Description', 'Required');
//            }
                

            if ($validator->isValid()) {
                $data["admin_id"] = $this->bu_user->id;
                $this->model->save($data);
            }

            die(json_encode($validator->getStatus()));
        }        
    }

    public function editAction() {

        $this->view->cities = Model_Cities::getAll();
        $this->view->regions = Model_Regions::getAll();
        
        if (!$this->_request->isPost()) {

        $id = $this->_request->getParam('id');
            $data = $this->model->get($id);
            $this->view->data = $data;
            //$this->view->comment_count = $this->model->getMemberCommentsCount($data->user_id);
            //$this->view->dublCount = 0;
        } else {

            $data = new Cubix_Form_Data($this->_request);
        
            $data->setFields(array(
                'city_id' => 'int',
                'region_id' => 'int',
                'page' => 'int',
                'fr' => '',
                'en' => '',
            ));

            $data = $data->getData();
            
            $validator = new Cubix_Validator();

            if ($validator->isValid()) {
                $data["admin_id"] = $this->bu_user->id;
                $this->model->save($data);
            }

            die(json_encode($validator->getStatus()));
        }
    }

    public function deleteAction()
    {

        if($this->_request->isPost()){
            $ids = $this->_request->getParam('id');
            if(count($ids) > 0 && $ids){
                foreach( $ids as $id ) {
                    $this->model->delete($id);
                }
            }
        }
        exit;
    }
}
