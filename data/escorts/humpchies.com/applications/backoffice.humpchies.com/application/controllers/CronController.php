<?php

class CronController extends Zend_Controller_Action
{
	/**
	 *
	 * @var Zend_Db_Adapter_Mysqli
	 */
	private $_db;

	public function init()
	{
		$this->_db = Zend_Registry::get('db');
	}

	public function bumpAction()
	{
		if ( defined('IS_CLI') && IS_CLI ) {
			$cli = new Cubix_Cli();
			$pid_file = '/var/run/bumps.pid';
			$cli->setPidFile($pid_file);

			if ( $cli->isRunning() ) {
				$cli->out('Action is already running... exitting...');
				exit(1);
			}
		}
		
		$model_bump = new Model_Bump();
		$active_bumps = $model_bump->getActiveBumps();
		if(count($active_bumps) > 0){
			foreach($active_bumps as $bump){
				echo 'Bumps Ads ID ----' . $bump->ads_id . ' ---- has bumped ' . $bump->bumps_count. '-------';
				//Incase server will go down for some periode of time 
				$interval_delay_times = floor($bump->seconds_passed / $bump->interval_sec);
				$interval = $interval_delay_times * $bump->interval_sec;
				$model_bump->triggerBump($bump->id, $bump->user_id, $bump->ads_id, $interval);
			}
			
			echo 'DONE';
		}
		else{
			echo 'NO BUMPS AT THIS MOMENT';
		}
		
		$model_bump->updateExpiredBumps();
		
		die;
	}
	
	public function bumpsCleanupAction()
	{
		$model_bump = new Model_Bump();
		$model_bump->cleanup();
		die('done');
	}		
	
	public function removeTemporaryBlockedEmailsAction()
	{
		//Remove all temporary blocked emails which blocked more than 48 hours ago
		//Temporary blocked emails has block_date
		
		set_time_limit(0);
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$cli = new Cubix_Cli();
		$pid_file = '/var/run/remove-temporary-block-emails-' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		
		if ( $cli->isRunning() ) {
			$cli->out('Action is already running... exitting...');
			exit(1);
		}
		
		$this->_db->query('DELETE FROM blacklisted_emails WHERE block_date IS NOT NULL AND UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(block_date) > 2 * 24 * 60 * 60');
		die('done');
	}
	
	public function doDispatch($request, $module, $controller, $action)
	{
		//$request->setParam('request_mode', 'copy-listener');

		$request->setModuleName($module);
		$request->setControllerName($controller);
		$request->setActionName($action);

		$front = Zend_Controller_Front::getInstance();
		$dispatcher = $front->getDispatcher();

		$response = new Zend_Controller_Response_Http();
		$dispatcher->dispatch($request, $response);
		$plain = $response->getBody();
		$result = json_decode($plain);

		return $result;
	}
	
	public function statisticsAction(){
        $st = microtime(true);
        ini_set("max_execution_time", -1);
        echo "start caching statistics\r\n";
        $this->view->layout()->disableLayout();
        $app_id = Cubix_Application::getId();
        $dashboard = new Model_Dashboard();
        $cache =Zend_Registry::get('cache');
        $cache_key = 'v2_views_of_profile_' . $app_id;
        echo "getting statistics data\r\n";
        $statistics = $dashboard->getStatistics($app_id);
        echo "save into cache\r\n";
        $result = $cache->save($statistics, $cache_key, array(), 7200);
        echo ($result) ? "cache updated\r\n" : "something went wrong\r\n";
        echo "took". (microtime(true) - $st). "\r\n";
        die;
    }
	
	
	public function testAction()
	{
		$body = 'test test test';
		Cubix_Email::send('badalyano@gmail.com', 'hanem hump ara', $body, false, null, null);
		die('done');
	}
	
	public function testCacheAction()
	{
		$frontendOptions = array(
			'lifetime' => null,
			'automatic_serialization' => true
		);
		
		$backendOptions = array(
			'servers' => array(
				array(
					'host' => 'localhost', 
					'port' => 11211,
					'persistent' =>  true
				)
			),
		);

		//if (defined('APPLICATION_ENV') && (APPLICATION_ENV == 'staging' || APPLICATION_ENV == 'development' ||  APPLICATION_ENV == 'production' ) ) {
			$cache = Zend_Cache::factory('Core', 'Memcached', $frontendOptions, $backendOptions);
			//var_dump($cache);die
		//} else {
			//$cache = Zend_Cache::factory('Core', 'Blackhole', array('automatic_serialization' => true));
			//$cache = Zend_Cache::factory(new Zend_Cache_Core($frontendOptions), new Zend_Cache_Backend_Memcached($backendOptions), $frontendOptions);
		//}
		$cache_key = 'test_test';
		if(!$result = $cache->load($cache_key)){
			echo "no cache";
			$result = array(1,2,3,4,5);
			$cache->save($result, $cache_key, array(), 100);
		}
		var_dump($result);die;
		
	}
    
    public function informExpirationAction(){
        
        $email = "webmaster@humpchies.com";
        $subject = "Humpchies Premium Package Expiration";
        $body = "Your Premium package will expire in 2 days.";
        $from = "info@humpchies.com";
        $fromName = "Info Humpchies";          
                  
        Cubix_Email::send($email, $subject, $body, $htmlBody, $from, $fromName);
        die("SENT");
    }
    
    
    public function googleanalyticsAction() {
        
        // $startDate = date('Y-m-d', strtotime("-2 days"));;
       
        $startDate = date('Y-m-d', strtotime("-60 days"));
        $endDate = date('Y-m-d', strtotime("-2 days"));
        
        $ga = new \Core\library\GoogleAnalytics();
        $analytics = $ga->initializeAnalytics();
        
        $initial_data =  $ga->getInitialViewsRows($analytics, $startDate, $endDate);
        
        if($initial_data) {
            
            $model = new Model_GaViews();
            $model->insert($initial_data);
        }
        die("FINISHED");
        
    }
    
    
    public function updatevisitsAction() {
        
        // read fron db
        $model_ga = new Model_GaViews();
        $unfinished_row = $model_ga->getUnfinishedProcess();
        
        if($unfinished_row) {
            
            for($i = $unfinished_row->current_page; $i < $unfinished_row->total_pages; $i++) {
                
                // read google analytics and update ads
                $ga = new \Core\library\GoogleAnalytics();
                $analytics = $ga->initializeAnalytics();
                
                $startDate = $unfinished_row->start_date;
                $endDate = $unfinished_row->end_date;
                $start_index = ($i * $unfinished_row->rows_per_page) + 1;
                $allViews =  $ga->getViewCounts($analytics, $startDate, $endDate, $start_index);    // read paginated views count from google analytics api
                
                if(empty($allViews))
                    die('NO VIEWS');
        
                $arrUpdates = [];
                foreach($allViews as $key => $view) {
                    
                     preg_match('/id\/([0-9]+)\//', $view[0], $output_array);  // process result to get ad id
                     $idFromUrl = $output_array[1];
                     
                     if(!isset($arrUpdates[$idFromUrl]))
                        $arrUpdates[$idFromUrl] = 0;   
                     
                     $arrUpdates[$idFromUrl] += $view[1]; // increment values                      
                }
                
                // update ads views count
                $ad = new Model_Advertisements();
                foreach($arrUpdates as $key => $views) {
                    
                    $data = [
                        'no_visits'         => 'no_visits' + $views,
                        'no_visits_updated' => $endDate
                    ];
                    $ad->update($key, $data);
                }
                
                // update page
                $model_ga->update($unfinished_row->id, array('current_page' => $i+1));
            }
                    
                    
        } else {            
            die('NOTHING TO PROCESS'); 
        }
       
        die("FINISHED");
        
    }
    
    
    public function getadvAction(){
         $varsa= "start";
       $contor=1;
       $start = 0;
       $chunk = 5000;
       while($varsa){
       $data = Model_Users::getAdv($start, $chunk);
        $varsa = "";
        foreach($data as $item){
           $varsa .= "\n\"".$item->username."\",\"". $item->email ."\",\"". $item->phone."\",\"". $item->user_location."\",\"". $item->user_country."\",\"". $item->date_created."\",\"". $item->type."\",\"". $item->language ."\"";
        } 
        $start += $chunk;     
        file_put_contents("/home/shadybyte/web/viper/logs/clients.csv",$varsa, FILE_APPEND);       
        $contor++;
        if($contor > 40000){
            die("complete contor");
        } 
       }
       die("terminat");
    }
    
    

}
