<?php

class IssuesController extends Zend_Controller_Action {

    /**
     * @var $model Model_Issues
     */
    private $model;
    /**
     * @var $document_model Cubix_Documents
     */
    private $document_model;

	public function init()
	{
		$this->model = new Model_Issues();
		$this->document_model = new Cubix_Documents();
	}
	
	public function indexAction() 
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('default_issues_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}
	
	public function dataAction()
	{
		$filter = array(
			'u.username' => $this->_getParam('username'),
			'st.id' => $this->_getParam('ticket_number'),
			'e.id' => $this->_getParam('escort_id'),
			'st.status' => $this->_getParam('status'),
			'st.backend_user_id' =>$this->_getParam('sales_user_id'),
			'st.status_progress' => $this->_getParam('status_progress'),
			'st.disabled_in_pa' => $this->_getParam('disabled_in_pa')
		);
		
		$data = $this->model->getAll(
			$this->_getParam('page'),
			$this->_getParam('per_page'),
			$filter, 
			$this->_getParam('sort_field'),
			$this->_getParam('sort_dir'),
			$count
		);
		
		foreach ( $data as $i => $item ) {
			
			if ( $data[$i]->status == Model_Issues::STATUS_TICKET_CLOSED )
				$data[$i]->status = 'closed';
			else if ( $data[$i]->status == Model_Issues::STATUS_TICKET_OPENED )
				$data[$i]->status = 'opened';
			
			if ( isset($data[$i]->disabled_in_pa) && $data[$i]->disabled_in_pa ) {
				$data[$i]->status = 'disabled';
			}
			
			$data[$i]->message = mb_substr($data[$i]->message, 0, 50, "UTF-8") . " ...";
			$data[$i]->subject = mb_substr($data[$i]->subject, 0, 20, "UTF-8") . " ...";
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}

	public function setStatusAction()
	{
		$id = intval($this->_getParam('id'));
		$status = intval($this->_getParam('status'));

		$data = array('id' => $id, 'status' => $status, 'resolve_date'=> new Zend_Db_Expr('NOW()'));


		$this->model->setStatus($data);
		// send mail 
		$this->sendMail($id);

		die;
	}
	
	public function setPaStatusAction()
	{
	    $id = intval($this->_getParam('id'));
		$status = intval($this->_getParam('status'));
		$pa_status = intval($this->_getParam('ps_status'));

		$data = array('id' => $id, 'status' => $status, 'disabled_in_pa' => $pa_status, 'resolve_date'=> new Zend_Db_Expr('NOW()'));

		if ( $status == Model_Issues::STATUS_TICKET_OPENED ) {
			$data['resolve_date'] = null;
		}

		$this->model->setStatus($data);
		// send mail 
		//$this->sendMail($id);
		die;
	}
	
	public function createTicketAction()
	{	
		
		/* get issue templates */
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($bu_user->type, array('superadmin', 'admin')))
			$backend_user_id = $bu_user->id;
		else
			$backend_user_id = null;
		
		$modelIT = new Model_System_IssueTemplates();
		$this->view->issue_templates = $modelIT->getByBO($backend_user_id);
		if($this->getRequest()->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);
			
			$data->setFields(array(
				'user_id' => 'int',
				'subject' => '',
				'comment' => ''
			));
			
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( ! $data['user_id'] ) {
				$validator->setError('user_id', 'Required');
			}
			
			if ( ! strlen($data['subject']) ) {
				$validator->setError('subject', 'Required');
			}
			
			if ( ! strlen($data['comment']) ) {
				$validator->setError('comment', 'Required');
			}

			$user_id = (new Model_Users)->userIdExists($data['user_id']);

			if ( ! $user_id ) {
				$validator->setError('user_id', 'Wrong ID');
			}
			
			if ( $validator->isValid() ) {				
				
				$data_ticket = array(
					'user_id' => $user_id,
					'subject' => $data['subject'],
					'message' => $data['comment'],
					'status' => Model_Issues::STATUS_TICKET_OPENED,
					'backend_user_id' => $bu_user->id,
					'status_progress' => Model_Issues::STATUS_ADMIN_SENT,
				);
				
				$this->model->insert($data_ticket);
			}
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{

		header('Content-Type: text/html; charset=utf-8');
		$issue = $this->model->get($this->_request->id);

		$this->view->issue = $issue['issue'];
		//$this->view->issue->subject = $this->fixWrongUTF8Encoding($this->view->issue->subject);
		//$this->view->issue->message = $this->fixWrongUTF8Encoding($this->view->issue->message);
		//var_dump($this->view->issue);


		if($issue['attach']){
			foreach($issue['attach'] as &$attach){
				$url = $this->document_model->getUrl(array(
						'catalog_id' => 'attached',
						'hash' => $attach->hash,
						'ext' => $attach->ext
					));
				$attach->file_url = $url;
			}
		}
		$this->view->issue_attachment = $issue['attach'];
		$this->view->sale_person = is_null($issue['issue']->backend_user_id) ? $issue['issue']->sales_user_id : $issue['issue']->backend_user_id;
		$mod_type = array('admin','superadmin');
		$moderators = new Model_Moderators();
		$this->view->sale_persons = $moderators->getByType($mod_type);
		$this->ajaxCommentsListAction();
		$this->view->layout()->enableLayout();
		
		/* get issue templates */
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($bu_user->type, array('superadmin', 'admin')))
			$backend_user_id = $bu_user->id;
		else
			$backend_user_id = null;
		
		$modelIT = new Model_System_IssueTemplates();
		$this->view->issue_templates = $modelIT->getByBO($backend_user_id);
	}

	public function ajaxCommentsListAction()
	{
		header('Content-Type: text/html; charset=utf-8');
		$this->view->layout()->disableLayout();

		$id = intval($this->_getParam('id', 0));
		$comments = $this->model->getComments($id);
		foreach($comments as &$comment){
			if($comment["attached_files"]){
				foreach($comment["attached_files"] as &$attach){
					$url = $this->document_model->getUrl(array(
							'catalog_id' => 'attached',
							'hash' => $attach->hash,
							'ext' => $attach->ext
						));
					$attach->file_url = $url;
				}
			}
		}
		$this->view->comments = $comments;
	}

	public function ajaxAddCommentAction()
	{
		$this->view->layout()->disableLayout();

		$validator = new Cubix_Validator();

		if ( ! $this->_getParam('comment')) {
			$validator->setError('comment', 'Required');
		}

		if ( $validator->isValid() ) {
			$data = array(
				'ticket_id' => $this->_getParam('id'),
				'comment' => $this->_getParam('comment'),
				'backend_user_id' => Zend_Auth::getInstance()->getIdentity()->id,
				'backend_user_name' => Zend_Auth::getInstance()->getIdentity()->username
			);
			
			$comment_id = $this->model->addComment($data);
			

			$attaches = is_array($this->_getParam('attach')) ? $this->_getParam('attach') : array($this->_getParam('attach'));
			
			if($attaches){
				$data = array(
					'ticket_comment_id'=> $comment_id,
					'ticket_id'=> $this->_getParam('id')
				);
				foreach($attaches as $attach_id){
					
					$this->model->updateAttached($data, $attach_id);
				}
			}
			$progress_status = array('status_progress' => 3, 'is_read' => 0);
			$this->model->update($this->_getParam('id'),$progress_status);

			// send mail 
			$this->sendMail($this->_getParam('id')); 
		}

		die(json_encode($validator->getStatus()));
	}
	
	public function removeAction()
	{
		$id = intval($this->_getParam('id'));

		$this->model->remove($id);
		die;
	}

    public function ajaxRemoveAttachedFilesAction() {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNorender(true);

        $id = intval($this->_getParam('id', 0));

        if ($id > 0){
            $this->model->removeAttachedById($id);
        }
        die;
	}

	public function ajaxChangeSalesPersonAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNorender(true);
		$id = intval($this->_getParam('id'));
		$backend_user_id = intval($this->_getParam('backend_user_id'));
		$this->model->changeBackendUserId($id, $backend_user_id);
		die(json_encode(array('status' => 'success')));
	}
	
	public function setProgressStatusAction()
	{
		$id = intval($this->_getParam('id'));
		$status = intval($this->_getParam('status'));

		$data = array('status_progress' => $status);

		$this->model->update($id,$data);
		// send mail 
		//$this->sendMail($id);
		die;
	}
	
	public function addAttachedFilesAction()
	{
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        try {
            $response = self::HTML5_upload();
            $docs = new Cubix_Documents();
            $docs->allowed_ext = array_merge($docs->allowed_ext, array('webm', 'avi', 'mp4')); 
            $error = false;
            $extension = end(explode(".", $response['name']));
            $extension = strtolower($extension);

            if (!in_array($extension, $docs->allowed_ext))
            {
                $error = 'Not allowed file extension.';
            }

            if ($error) {

                $return = array(
                    'status' => '0',
                    'error' => $error
                );

            } elseif ($response['error'] == 0 && $response['finish']) {

                // Save on remote storage
                $doc = $docs->save($response['tmp_name'], 'attached', $extension);

                $attach_arr = array(
                    'file_name' => $response['name'],
                    'hash' => $doc['hash'],
                    'ext' => $doc['ext'],

                );

                $attached_id = $this->model->addAttached($attach_arr);
                $return = array(
                    'status' => '1',
                    'id' => $attached_id
                );
            } else {
                $return = array(
                    'status' => '0',
                    'error' => 'Upload error .'
                );
            }

            echo json_encode($return);
        } catch (Exception $e) {
            echo json_encode(array(
                'status' => 0,
                'error' => $e->getMessage()
            ));
        }
	}

    public static function HTML5_upload()
    {
        $headers = array();
        $headers['Content-Length'] = $_SERVER['CONTENT_LENGTH'];
        $headers['X-File-Id'] = $_SERVER['HTTP_X_FILE_ID'];
        $headers['X-File-Name'] = $_SERVER['HTTP_X_FILE_NAME'];
        $headers['X-File-Resume'] = $_SERVER['HTTP_X_FILE_RESUME'];
        $headers['X-File-Size'] = $_SERVER['HTTP_X_FILE_SIZE'];

        $response = array();
        $response['id'] = $headers['X-File-Id'];
        $response['name'] = basename($headers['X-File-Name']); 	// Basename for security issues
        $response['size'] = $headers['Content-Length'];
        $response['error'] = UPLOAD_ERR_OK;
        $response['finish'] = FALSE;

        // Is resume?
        $flag = (bool)$headers['X-File-Resume'] ? FILE_APPEND : 0;
        $filename = $response['id'].'_'.$response['name'];
        $response['upload_name'] = $filename;

        // Write file
        $file = sys_get_temp_dir().DIRECTORY_SEPARATOR.$response['upload_name'];

        if (file_put_contents($file, file_get_contents('php://input'), $flag) === FALSE){
            $response['error'] = UPLOAD_ERR_CANT_WRITE;
        }
        else
        {
            $response['file_size'] = filesize($file);
            $response['X-File-Size'] = $headers['X-File-Size'];

            if (filesize($file) == $headers['X-File-Size'])
            {
                $response['tmp_name'] = $file;
                $response['finish'] = TRUE;
            }
        }

        return $response;
    }



    public function testAction() {
        $ticket_id = $this->_getParam('id');
        $this->sendMail($ticket_id) ; 

        exit("here");
        
    }

    public function sendMail($ticket_id) {

        $info = $this->model->getMailInfo($ticket_id);
        $language = $info->language;
        
        if(strtolower($language) == 'en' ) {

	        switch(true) {
	            case ($info->status_progress == 3 && !$info->disabled_in_pa): $templateID = 19; break; // ticket answered
	            case ($info->status_progress == 2 || (($info->status_progress == 3 && $info->disabled_in_pa))): $templateID = 21; break; // ticket closed
	        }
        } else {       
            switch(true) {
                case ($info->status_progress == 3 && !$info->disabled_in_pa): $templateID = 20; break;
                case ($info->status_progress == 2 || (($info->status_progress == 3 && $info->disabled_in_pa))): $templateID = 22; break;
            }
        }

        $templateModel = new Model_Newsletter_Templates();
        $template = $templateModel->getById($templateID);
        $old = [];


        if($template) {
        foreach($this->model->keywords as $key => $dbField) {
            $old[] = '%%' .$key. '%%'; 
            $new[] = $info->$dbField;
        }

        $body = str_replace($old, $new, $template->body);
        $subject = $template->subject;
        Cubix_Email::send($info->email, mb_convert_encoding($subject, 'windows-1252', mb_detect_encoding($subject)), $body, true, $template->from_email );
	        
        }

        //var_dump([$info->email, mb_convert_encoding($subject, 'windows-1252', mb_detect_encoding($subject)), $body, true, $template->from_email]);
        
        //Cubix_Email::send('gigi.ken2004@gmail.com', mb_convert_encoding($subject, 'windows-1252', mb_detect_encoding($subject)), $body, true, $template->from_email );
        //die();
    }


}
