<?php

class System_IssueTemplatesController extends Zend_Controller_Action {
	
	public function init()
	{
		$this->_model = new Model_System_IssueTemplates();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{		
		$filter = array(
			
		);
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		if (!in_array($bu_user->type, array('superadmin', 'admin')))
			$filter['backend_user_id'] = $bu_user->id;
		
		$data = $this->_model->getAll(
			$this->_request->page,
			$this->_request->per_page, 
			$filter, 
			$this->_request->sort_field, 
			$this->_request->sort_dir, 
			$count
		);
		
		die(json_encode(array('data' => $data, 'count' => $count)));
	}
	
	public function createAction()
	{
		$this->view->layout()->disableLayout();
		
		if ( $this->_request->isPost() ) {
			$bu_user = Zend_Auth::getInstance()->getIdentity();
			
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'title' => '',
				'comment' => '',
				'signature' => ''
			));
			$data = $data->getData();
			$data['backend_user_id'] = $bu_user->id;
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['title']) ) {
				$validator->setError('title', 'Required');
			}
			
			if ( ! strlen($data['comment']) && ! strlen($data['signature']) ) {
				$validator->setError('comment', 'Comment or Signature Required');
			}
			
			if ( $validator->isValid() ) {
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		
		$this->view->item = $template = $this->_model->get($id);
		
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'title' => '',
				'comment' => '',
				'signature' => ''
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['title']) ) {
				$validator->setError('title', 'Required');
			}
			
			if ( ! strlen($data['comment']) && ! strlen($data['signature']) ) {
				$validator->setError('comment', 'Comment or Signature Required');
			}
			
			if ( $validator->isValid() ) {
				$this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->remove($id);

		die;
	}
	
	public function ajaxGetAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		
		$temp = $this->_model->get($id);
		$temp->comment = nl2br($temp->comment);
		die(json_encode($temp));
	}
}
