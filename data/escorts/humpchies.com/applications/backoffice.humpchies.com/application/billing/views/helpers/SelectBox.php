<?php

class Zend_View_Helper_SelectBox
{
	public function selectBox($name, $id, array $data, $class = '', $selected = NULL, $empty = FALSE, $disabled = false, $other = NULL)
	{
		$html = '<select id="' . $id . '" name="' . $name . '" class="' . $class . '"' . ($disabled ? ' disabled="disabled"' : '') . '>';

		if ($empty !== FALSE) $html .= '<option value="">- ' . $empty . ' -&nbsp;&nbsp;</option>';

		foreach ($data as $k => $v) {
			$html .= '<option value="' . $k . '"' . (($k == $selected) ? ' selected="selected"' : '') . '>' . $v . '&nbsp;&nbsp;</option>';

			if ($other == 'bp' && $k == 3)
			{
				$html .= '<option value="3-30">expired (more than 30 days)</option>';
			}
		}

		$html .= '</select>';

		return $html;
	}
}
