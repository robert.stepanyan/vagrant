<?php

class Zend_View_Helper_Picker extends Zend_View_Helper_Abstract
{
	public function picker(array $params = array())
	{
		$title = '';
		$image = '/img/no-photo.png';
		$username = '';
		$category = '';
		$adv_id = null;
				
		if ( isset($params['adv_id']) ) {
			$adv_id = $params['adv_id'];
		}
		
		if ( $adv_id ) {
			$model = new Model_Advertisements();
			$adv = $model->get($adv_id);

			$title = $adv->title;
			//$image = $escort->getMainPhoto()->getUrl('backend_smallest');
			if ( ! $image ) $image = '/img/no-photo.png';
			$username = $adv->username;
			$category = $adv->category;
			$user_id = $adv->user_id;
		}
		

		$el_id = md5(microtime());
		if ( isset($params['id']) ){
			$el_id = $params['id'];
		}
				
		ob_start();
		?>
<div id="<?= $el_id ?>" class="cx-picker">
	<div class="thumb fleft">
		<img src="<?= $image ?>" width="64" height="64" />
	</div>
	<div class="info fleft">
		<div class="name">
			<div class="text"><?= $title ?></div>
			<? if ( ! isset($params['read_only']) || false === $params['read_only'] ): ?>
			<a href="#"></a>
			<? endif ?>
			<div class="clear"></div>
		</div>
		<div class="username">
			<strong>Username:</strong> <?= $username ?>
		</div>
		<div class="category">
			<strong>Category:</strong> <?= $category ?>
		</div>
		
		<div class="clear"></div>
	</div>
	
    <div class="searchbox fleft hide">
		
		<div class="search-fields fleft">
			<div class="inner">
				<label for="id">Advertisment ID </label>
				<input type="text" class="txt w15 adsId blur" name="ads_id" value="<?= $adv_id ?>">
			</div>
			
			<div class="inner">
				<a  href="#"></a>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<input type="hidden" name="<?= $params['field_name'] ?>" value="<?= $user_id ?>" />
	
</div>
<div class="clear"></div>
<script type="text/javascript">
	
	function init_<?= $el_id ?> () {
		var el = $('<?= $el_id ?>');
		if ( ! el ) return;
		
		el.readOnly = <?= isset($params['read_only']) && true === $params['read_only'] ? 'true' : 'false' ?>;
		var isPrimaryId = <?= isset($params['primary_id']) ? 1 : 0 ?>;
		var overlay = new Cubix.Overlay( el , { showLoader: true });
		var els = {
			main: el,
			searchbox:el.getElement('.searchbox'),
			info: el.getElement('.info'),
			btn: el.getElement('.info .name a'),
			searchBtn: el.getElement('.search-fields a'),
			input: el.getElement('.adsId')
		};
		
		var info = {
			name: els.info.getElement('.name .text'),
			img: el.getElement('.thumb img'),
			username: el.getElement('.username'),
			category: el.getElement('.category'),
			user_id: el.getElement('input[name=<?= $params['field_name'] ?>]')
		}
		
		function setMode(mode) {
			function modeEdit() {
				els.info.addClass('hide');
				els.searchbox.removeClass('hide');
				els.input.focus();
			};

			function modeView() {
				els.info.removeClass('hide');
				els.searchbox.addClass('hide');
			};

			if ( mode == 'edit' ) {
				modeEdit();
			}
			else if ( mode == 'view' ) {
				modeView();
			}
		};

		function modeAction(data){
			var title = data.title;
			if ( title.length > 40 ) {
				title = title.slice(0, 37) + '...';
				info.name.set('title', data.title);
			}

			//info.balance.set('html', data.balance);
			info.name.set('html', title);

			if ( data.photo ) {
				info.img.set('src', data.photo);
			}
			else {
				info.img.set('src', '/img/no-photo.png');
			}

			info.username.set('html', '<strong>Username:</strong> ' + data.username);
			info.category.set('html', '<strong>Category:</strong> ' + data.category);
			info.user_id.set('value', data.user_id);

			if(isPrimaryId){
				$(el).getParent().getElement('input[name=primary_id]').set('value',data.id);
			}
		}
		
		if ( ! els.input.get('value').length ) {
			setMode('edit');
		}

		el.disable = function () {
			this.set('opacity', 0.5);
			this.disabled = true;
		}.bind(el);

		el.enable = function () {
			this.set('opacity', null);
			this.disabled = false;
		}.bind(el);

		if ( els.btn ) {
			els.btn.addEvents({
				'mousedown': function () {
					if ( el.disabled ) return;

					this.addClass('down');
					document.addEvent('mouseup', this.mouseup = function () { this.removeClass('down'); document.removeEvent('mouseup', this.mouseup) }.bind(this));
				},
				'mouseenter': function () {
					if ( el.disabled ) return;

					this.addClass('hover');
				},
				'mouseleave': function () {
					if ( el.disabled ) return;

					this.removeClass('hover');
				},
				click: function (e) {
					if ( el.disabled ) return;

					e.stop();

					setMode('edit');
				}
			});
		}
		
		if ( els.searchBtn ) {
			els.searchBtn.addEvents({
				'mousedown': function () {
					if ( el.disabled ) return;

					this.addClass('down');
					document.addEvent('mouseup', this.mouseup = function () { this.removeClass('down'); document.removeEvent('mouseup', this.mouseup) }.bind(this));
				},
				'mouseenter': function () {
					if ( el.disabled ) return;

					this.addClass('hover');
				},
				'mouseleave': function () {
					if ( el.disabled ) return;

					this.removeClass('hover');
				},
				click: function (e) {
					if ( el.disabled ) return;
					e.stop();
					overlay.disable();
					
					var post_data = {
						ads_id: els.searchbox.getElement('input[name=ads_id]').get('value')
					};
					
					new Request.JSON({
						url: '/billing/orders/items',
						method: 'post',
						data: post_data,
						onSuccess: function (resp) {
							if (resp.id != 0){
								modeAction(resp);
							}
							else{
								modeAction({
									title:'Nothing found',
									user_type:0,
									user_id:0
								})
							}
							overlay.enable();
						}
					}).send();
					setMode('view');
					
				}
			});
		}
	}
	
	init_<?= $el_id ?>();
</script>
		<?

		return ob_get_clean();
	}
}
