<?php

class Billing_CreditTransfersController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_Transfers
	 */
	protected $_model;

	public function init()
	{
		$this->view->model = $this->_model = new Model_Billing_Transfers();
	}

	public function indexAction()
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('billing_transfers_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */

		/*$this->view->transfer_data = array(
			'total' => number_format($this->_model->getTotalTransfers(), 2),
			'paid' => number_format($this->_model->getTotalTransfersPaid(), 2)
		);*/
	}

	public function dataAction()
	{
		$req = $this->_request;
		$credit_transfer_model = new Model_Billing_CreditTransfers();
		$data = new Cubix_Form_Data($this->_request);
		
		$data->setFields(array(
			'transfer_type_id' => 'int',
			'transaction_id' => '',
			'mtcn' => '',
			'cc_number' => '',
			'cc_email' => '',
			'var_symbol' => '',
			'first_name' => '',
			'last_name' => '',
			'pull_person_id' => '',
			'cc_info' => '',


			'sales_user_id' => 'int',
			'status' => 'int',
			'filter_by' => '',
			'date_from' => 'int',
			'date_to' => 'int',
			'user_id' => '',
			'status' => 'int',

            'ads_id' => 'int',
			'fraudulent' => 'int',
			'email' => '',
			'has_comment' => '',
			'is_mobile' => 'int',
            'contains_payment_ad_reation' => '',
		));
		
		$filter = $data->getData();

		$transfers = $credit_transfer_model->getAll(
			$filter,
			$req->page,
			$req->per_page,
			$req->sort_field,
			$req->sort_dir
		);

		die(json_encode($transfers));
	}
	
	public function dataCountAction()
	{
		$this->view->layout()->disableLayout();
		
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);
		$type = $this->_request->getParam('type');
		$data->setFields(array(
			'transfer_type_id' => 'int',
			'transaction_id' => '',
			'mtcn' => '',
			'cc_number' => '',
			'cc_email' => '',
			'var_symbol' => '',
			'first_name' => '',
			'last_name' => '',
			'pull_person_id' => '',
			'cc_info' => '',


			'app_id' => 'int',
			'sales_user_id' => 'int',
			'status' => 'int',
			'filter_by' => '',
			'date_from' => 'int',
			'date_to' => 'int',
			'user_id' => '',
			'status' => 'int',

			'showname' => '',
			'ads_id' => 'int',
			'email' => '',
			'agency_name' => '',
			'escort_type' => '',
			'has_comment' => '',
		));
		
		$data = $data->getData();

		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		$filter = array();
		//             
		
		if ( $data['ads_id'] ) {
			$filter['a.id = ?'] = $data['ads_id'];
		}

		if ( $data['email'] ) {
			$filter['u.email LIKE ?'] = $data['email'] . '%';
		}

		if ( $data['agency_name'] ) {
			$filter['a.name LIKE ?'] = $data['agency_name'] . '%';
		}
		if ( $data['transfer_type_id'] ) {
			$filter['t.transfer_type_id = ?'] = $data['transfer_type_id'];
		}
		
		if ( $data['transaction_id'] ) {
			$filter['t.transaction_id = ?'] = $data['transaction_id'];
			$filter['show_special'] = $data['transaction_id'];
		}
		
		if ( $data['var_symbol'] ) {
			$filter['t.var_symbol = ?'] = $data['var_symbol'];
		}
		if ( $data['mtcn'] ) {
			$filter['t.mtcn = ?'] = $data['mtcn'];
		}
		if ( $data['cc_number'] ) {
			$filter['t.cc_number LIKE ?'] = '%' . $data['cc_number'];
			
			
		}
		if ( $data['cc_email'] ) {
			$filter['t.cc_email LIKE ?'] = '%' . $data['cc_email'] . '%';
			
			
		}
		if ( $data['first_name'] ) {
			$filter['t.first_name = ?'] = $data['first_name'];
			
			
		}
		if ( $data['last_name'] ) {
			$filter['t.last_name = ?'] = $data['last_name'];
			
			
		}
		if ( $data['pull_person_id'] ) {
			$filter['t.pull_person_id = ?'] = $data['pull_person_id'];
		}
		
		if ( $data['has_comment'] ) {
			$filter['t.comment IS NOT NULL AND t.comment <> \'\''] = true;
		}

		if ( $bu_user->type == 'superadmin' ) {
			if ( $data['sales_user_id'] ) {
				$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
			}
		}
		else {
			
		}

		if ( $data['status'] ) {
			$filter['t.status = ?'] = $data['status'];
		}

		$data['filter_by'] = 'date_transfered';
		if ( $data['filter_by'] ) {
			if ( in_array($data['filter_by'], array('date_transfered')) ) {
				if ( $data['date_from'] ) {
					$filter['DATE(' . $data['filter_by'] . ') >= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_from'] . '))');
				}

				if ( $data['date_to'] ) {
					$filter['DATE(' . $data['filter_by'] . ') <= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_to'] . '))');
				}
			}
		}

		if ( strlen($data['user_id']) > 0 ) {
			$filter['u.id = ?'] = $data['user_id'];
		}
		
		$s = implode('_', $filter);
         
		$cache_key =  'transfers_' . preg_replace('#[^a-zA-Z0-9_]#', '_', $s) . $type.'_'.$data['escort_type'];
		
		$cache = Zend_Registry::get('cache');
//        var_dump($filter);die;
		if ( ! $data = $cache->load($cache_key) )
		{
			$data = $this->_model->getAllCountV2(
				$filter,
				1,
				10,
				'id',
				'asc'
			);
			
			$cache->save($data, $cache_key, array(), 3600); //1 hours
		}

		$this->view->data = $data;
//		var_dump($data);die;
		$this->view->type = $req->type;
	}

	public function createAction()
	{
		$this->view->layout()->disableLayout();

		$this->view->bu_user = $bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( $this->_request->isPost() ) {
			
			$data = new Cubix_Form_Data($this->_request);
			
			$params = array(
				'user_id' => 'int',
				'transfer_type_id' => 'int',
				'date_transfered' => 'int',
				'credits' => '',
				'first_name' => '',
				'last_name' => '',
				'country_id' => 'int',
				'city' => '',
				'var_symbol' => '',
				'transaction_id' => '',
				'pull_person_id' => 'int',
				'webmoney_account' => 'int',
				'website_id' => 'int',
				'mtcn' => '',
				'comment' => 'notags|special',
			);
						
			$data->setFields($params);
			
			$data = $data->getData();

			
			$validator = new Cubix_Validator();

			if ( ! $data['user_id'] ) {
				$validator->setError('global_error', 'Please select a User');
			}

			if ( ! $data['transfer_type_id'] ) {
				$validator->setError('transfer_type_id', 'Required');
			}

			if ( ! $data['date_transfered'] ) {
				$validator->setError('date_transfered', 'Required');
			}
			
			if ( ! $data['credits'] ) {
				$validator->setError('credits', 'Required');
			}
			elseif(!array_key_exists($data['credits'], Model_Billing_CreditTransfers::$CREDIT_OPTIONS)){
				$validator->setError('credits', 'No such credit option');
			}

			if ( $data['transfer_type_id'] == 5 ) {
				if ( ! $data['webmoney_account'] ) {
					$validator->setError('webmoney_account', 'Required');
				}
				
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}

			if ( $data['transfer_type_id'] == 3 ) {
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}
			
			
			if ( $data['transfer_type_id'] != 5 ) {
				if ( $data['transfer_type_id'] && ! $data['first_name'] ) {
					$validator->setError('first_name', 'Required');
				}
				
				if ( $data['transfer_type_id'] && ! $data['last_name'] ) {
					$validator->setError('last_name', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] == 7 ||  $data['transfer_type_id'] == 2) {
				if ( ! $data['mtcn'] ) {
					$validator->setError('mtcn', 'Required');
				}
			}

			$data['backend_user_id'] = Zend_Auth::getInstance()->getIdentity()->id;

			if ( $data['transaction_id'] ) {
				if ( false === $this->_model->checkTransactionId($data['transaction_id']) ) {
					$validator->setError('transaction_id', 'Already in database');
				}
			}

			if ( $validator->isValid() ) {
				$credit_transfer_model = new Model_Billing_CreditTransfers();
				$data['amount'] = Model_Billing_CreditTransfers::$CREDIT_OPTIONS[$data['credits']];  
				$data['credits'] = str_replace('cr_', '', $data['credits']);
				$data['is_credit_transfer'] = 1;
				$data['status'] = Model_Billing_Transfers::STATUS_CONFIRMED;
				if ( true !== ($result = $credit_transfer_model->save($transfer = new Model_Billing_TransferItem($data))) ) {
					$validator->setError('global_error', $result);
				}
				
				header('Cubix-Message: ' . 'Transfer Saved!');
				
			}

			die(json_encode($validator->getStatus()));
		}
		else{
			
		}
	}

	public function detailsAction()
	{
		$this->view->layout()->disableLayout();

		$id = intval($this->_getParam('id'));
		$credit_transfer_model = new Model_Billing_CreditTransfers();
		
		$transfer = $credit_transfer_model->getDetails($id);
		$this->view->transfer = $transfer;
		
		//$this->view->transfer_imgs = $this->_model->getTransferImgsById($id);
		
	}

	public function editAction()
	{
		$this->view->layout()->disableLayout();

		$id = $this->view->id = intval($this->_request->id);

		$this->view->transfer = $this->_model->getDetails($id);
		
		$orders = array();
		if( count($this->view->transfer->orders) ) {
			$model = new Model_Billing_Orders();
			foreach ( $this->view->transfer->orders as $o ) {
				$order = $model->getDetails($o->id);
				
				if ( $order ) {
					$orders[] = $order;
				}
			}			
		}
		//var_dump(json_encode($orders));die;
		$this->view->orders = $orders;
		
		if ( $this->_request->isPost() ) {
			
			$data = new Cubix_Form_Data($this->_request);
			$bu_user = Zend_Auth::getInstance()->getIdentity();
			$params = array(
				'user_id' => 'int',
				'orders' => 'arr-int',
				'transfer_type_id' => 'int',
				'amount' => 'double',
				'first_name' => '',
				'last_name' => '',
				'country_id' => 'int',
				'city' => '',
				'var_symbol' => '',
				'transaction_id' => '',
				'pull_person_id' => 'int',
				'webmoney_account' => 'int',
				'website_id' => 'int',
				'mtcn' => '',
				'comment' => 'notags|special',
			);
			
			$data->setFields($params);
			
			$data = $data->getData();
			
			
			$validator = new Cubix_Validator();

			if ( ! $data['user_id'] ) {
				$validator->setError('global_error', 'Please select an agency or escort');
			}

			if ( ! $data['transfer_type_id'] ) {
				$validator->setError('transfer_type_id', 'Required');
			}

			/*if ( ! $data['date_transfered'] ) {
				$validator->setError('date_transfered', 'Required');
			}*/

			if ( ! $data['amount'] ) {
				$validator->setError('amount', 'Required');
			}

			if ( $data['transfer_type_id'] == 5 ) {
				if ( ! $data['webmoney_account'] ) {
					$validator->setError('webmoney_account', 'Required');
				}
				
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}

			if ( $data['transfer_type_id'] == 3 ) {
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] != 5 ) {
				if ( $data['transfer_type_id'] && ! $data['first_name'] ) {
					$validator->setError('first_name', 'Required');
				}

				if ( $data['transfer_type_id'] && ! $data['last_name'] ) {
					$validator->setError('last_name', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] == 7 ||  $data['transfer_type_id'] == 2) {
				if ( ! $data['mtcn'] ) {
					$validator->setError('mtcn', 'Required');
				}
			}
			//$data['backend_user_id'] = Zend_Auth::getInstance()->getIdentity()->id;

			if ( $data['transaction_id'] ) {
				if ( false === $this->_model->checkTransactionId($data['transaction_id'], $id) ) {
					$validator->setError('transaction_id', 'Already in database');
				}
			}
			
			if ( $validator->isValid() ) {
				var_dump($data);die;
				$data['id'] = $id;
				
				$ret = $this->_model->save(new Model_Billing_TransferItem($data));
				//var_dump($ret);die;
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;

		$transfer = $this->_model->get($id);
		if ( ! $transfer ) die;

		if ( Model_Billing_Transfers::STATUS_REJECTED == $transfer->status ) {
			$transfer->delete();
		}

		die;
	}

	public function rejectAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$transfer_id = intval($req->id);
		if ( ! $transfer_id ) die;
		
		$credit_transfer_model = new Model_Billing_CreditTransfers();
		$this->view->transfer = $transfer = $credit_transfer_model->getDetails($transfer_id);
		if ( ! $transfer ) die;
	
		
		$comment = $this->_getParam('reject_reason');

		if ( $req->isPost() ) {

			$validator = new Cubix_Validator();

			if ( ! strlen($comment) ) {
				$validator->setError('reject_reason', 'You need to leave Reason');
			}
			
			if ( $validator->isValid() ) {
				if ( Model_Billing_Transfers::STATUS_PENDING == $transfer->status || Model_Billing_Transfers::STATUS_CONFIRMED == $transfer->status ) {
					$credit_transfer_model->reject($transfer, $comment);
					
					header('Cubix-Message: Transfer successfully rejected!');
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function chargebackAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$credit_transfer_model = new Model_Billing_CreditTransfers();
		
		$transfer_id = intval($req->id);
		if ( ! $transfer_id ) die;
		
		$this->view->transfer = $transfer = $credit_transfer_model->getDetails($transfer_id);
		if ( ! $transfer ) die;
	
		$comment = $this->_getParam('chargeback_reason');

		if ( $req->isPost() ) {
			$validator = new Cubix_Validator();
			if ( $validator->isValid() ) {
				if ( Model_Billing_Transfers::STATUS_PENDING == $transfer->status || Model_Billing_Transfers::STATUS_CONFIRMED == $transfer->status ) {
					$credit_transfer_model->chargeback($transfer, $comment);
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function confirmAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		
		$credit_transfer_model = new Model_Billing_CreditTransfers();
		$this->view->transfer = $transfer = $credit_transfer_model->getDetails($id);
		
		if ( ! $transfer ) die;
		
		$comment = $this->_getParam('comment');
		$amount = doubleval(doubleval(str_replace(',', '', $this->_getParam('amount'))));

		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			
			if ( ! $amount ) {
				$validator->setError('amount', 'Required');
			}

			if($validator->isValid()){
				if ( (Model_Billing_Transfers::STATUS_PENDING == $transfer->status) || (Model_Billing_Transfers::STATUS_REJECTED == $transfer->status) ) {
					$credit_transfer_model->confirm($transfer,$comment, $amount);
					header('Cubix-Message: Transfer successfully confirmed!');
				}
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function editCreditCardInfoAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = $this->view->id = intval($this->_request->id);
		$this->view->transfer = $this->_model->getDetails($id);
		
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			$data = new Cubix_Form_Data($this->_request);
			$params = array(
				'cc_holder' => '',
				'cc_email' => '',
				'cc_number' => '',
				'cc_exp_month' => '',
				'cc_exp_day'	=> '',
				'cc_ip'	=> '',
				'cc_address'	=> ''
			);
			
			$data->setFields($params);
			$data = $data->getData();
			
			if ( $validator->isValid() ) {
				$data['id'] = $id;
				$ret = $this->_model->save(new Model_Billing_TransferItem($data));

				die(json_encode($validator->getStatus()));
			}
		}
	}
	
}
