<?php

class Billing_OrderPackagesController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_Packages
	 */
	protected $_model;

	public function init()
	{
		$this->_model = new Model_Billing_Packages();
	}

	public function indexAction()
	{
		

		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('billing_packages_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
	}

	public function dataAction()
	{
		$req = $this->_request;

		$order_map = array(
			'exp_date'	=> 'expiration_date'
		);
		
		if ( $this->_request->sort_field && isset($order_map[$this->_request->sort_field]) ) {
			//$this->_request->sort_field = $order_map[$this->_request->sort_field];
			$this->_request->setParam('sort_field', $order_map[$this->_request->sort_field]);
		}
		
		$filter = array(
			'ag.name' => $req->agency_name,
			'a.id' => $req->ads_id,
			// Date
			'filter_by' => $req->filter_by,
			'date_from' => $req->date_from + 60*60*24*1,//package expiration fix
			'date_to' => $req->date_to + 60*60*24*1,//package expiration fix

			'op.status' => $req->package_status,
			/*'op.application_id' => $req->app_id,*/
			'o.sys_order_id' => $req->sys_order_id,
			'active_package_id' => $req->active_package_id,
			'o.backend_user_id' => $req->sales_user_id
		);

		if ($filter['op.status'] == '3-30')
		{
			$filter['op.status'] = 3;
			$filter['more30'] = 1;
		}

		$data = $this->_model->getAllV2(
			$this->_request->page,
			$this->_request->per_page,
			$filter,
			$this->_request->sort_field,
			$this->_request->sort_dir,
			$count
		);
		
		foreach ( $data as $k => $package ) {
			
			$data[$k]->package_status = Model_Packages::$STATUS_LABELS[$package->package_status];
			//FIXING PACKAGES LOGIC
			//IF PACKAGE EXPIRES 19 APRIL IT'S ACTUALLY EXPIRES 18 APRIL 23:59
			if ( $data[$k]->expiration_date ) {
				$data[$k]->exp_date = strtotime('-1 day', $data[$k]->expiration_date);
				$data[$k]->expiration_date = date("d M Y", strtotime('-1 day', $data[$k]->expiration_date)); //-1 day				
			}
		}
		
		die(json_encode(array('data' => $data, 'count' => $count)));
	}

	public function cancelAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$id = intval($req->id);

		$this->view->package = $package = $this->_model->get($id);
		//$this->view->products = $package->getProducts();
		//$this->view->opt_products = $package->getProducts(Model_Products::TYPE_OPTIONAL_PRODUCT);
		$this->view->days_used = $package->getUsedDays();
		$this->view->chargeback_amount = $chargeback_amount = $package->getChargebackAmount();
		$this->view->balance_after = number_format($chargeback_amount + $package->balance, 2);

		if ( $req->isPost() ) {
			$validator = new Cubix_Validator();

			if ( ! strlen($req->comment) ) {
				$validator->setError('comment', 'Required');
			}

			if ( $validator->isValid() ) {
				$this->_model->cancel($id, $req->comment);
				Zend_Registry::get('BillingHooker')->notify('admin_cancelled_package', array($id));
			}

			die(json_encode($validator->getStatus()));
		}
	}

	private function dateDiff($date_start, $date_end)
	{
	    $difference = $date_end - $date_start;
        $days = round($difference / 86400); // 3600 seconds in an hour

		return $days;
	}
	
	public function upgradeAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$escort_id = intval($req->escort_id);
		$order_package_id = intval($req->id);

		$this->view->order_id = $this->_getNewOrderId();
		
		$old_package = $this->_model->get($order_package_id);
		$this->view->old_package = $old_package;
		$old_package->expiration_date = $old_package->expiration_date ? date('d M Y', strtotime('-1 day', strtotime($old_package->expiration_date))) : null;
		
		$this->view->old_package_used_days = $old_package_used_days = $this->dateDiff(strtotime($old_package->date_activated), mktime('00', '00', '00', date('m', time()), date('d', time()), date('Y', time())));
		
		if ( $req->isPost() )
		{
			$validator = new Cubix_Validator();

			$premium_city_spots = $req->premium_city_spots;
			$opt_products = $req->opt_products;

			if ( count($opt_products) > 0 ) {
				foreach($opt_products as $opt_product) {
					if ( $opt_product == Model_Products::CITY_PREMIUM_SPOT && count($premium_city_spots) == 0 ) {
						$validator->setError('city_spot_error', 'City Required');
					}
					
					if ( $opt_product == Model_Products::ADDITIONAL_AREA && count($req->add_areas_id) == 0 ) {
						$validator->setError('add_area_error', 'Area Required');
					}
					
					if ( $opt_product == GIRL_OF_THE_DAY ) {
						
						//$val = Model_Products::validateGotdActivationDate ($req->gotd_activation_date, $req->period, $req->activate_promo_package, $req->activation_date);
						
						if ( ! strlen( $req->gotd_activation_date ) ) {
							$validator->setError('gotd_err', 'Activation date is Required');
						}
						else if (Model_Products::validateGotd($req->gotd_activation_date, $req->gotd_city_id) ) {
							$validator->setError('gotd_err', 'This day and city are not available');
						}
						//else if ( date('d-m-Y', $req->gotd_activation_date) <= date('d-m-Y', time())  ) {
						else if ( mktime(0, 0, 0, date("m", $req->gotd_activation_date), date("d", $req->gotd_activation_date), date("Y", $req->gotd_activation_date)) < mktime(0, 0, 0, date("m", time()), date("d", time()), date("Y", time()))  ) {
							$validator->setError('gotd_err', 'Please select only future date.');
						}
						/*else if ( strlen($val) ) {
							$validator->setError('gotd_err', $val);
						}*/
					}
				}
			}

			if ( ! strlen($req->package) ) {
				$validator->setError('package', 'Required');
			}
			
			if ( Cubix_Application::getId() == APP_BL ) {
				if ( $old_package_used_days > 3 && ! in_array($req->package, array(21,22,23,24,25,26)) ) {
					$validator->setError('package', 'You can\' upgrade to this package. ' . $old_package_used_days . ' days already used');
				}
			}

			if ( ! strlen($req->period) ) {
				$validator->setError('period', 'Required');
			}
			

			if ( $validator->isValid() ) {
				$new_order_package_id = $this->_model->upgrade($old_package, $req);
				Zend_Registry::get('BillingHooker')->notify('admin_upgraded_package', array($order_package_id, $new_order_package_id));
			}

			die(json_encode($validator->getStatus()));
		}
	}

	protected function _generateOrderId()
	{
		$order_id = md5(microtime() * microtime() + microtime());
		$order_id = substr(strtoupper($order_id), 0, 8);

		return $order_id;
	}

	protected function _getNewOrderId()
	{
		$m_orders = new Model_Billing_Orders();

		$order_id = $this->_generateOrderId();
		while( $m_orders->isOrderIdExist($order_id) ) {
			$order_id = $this->_generateOrderId();
		}

		return $order_id;
	}
	
	public function suspendResumeAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$action = $this->view->a = $this->_request->a;
		$op_id = $this->view->op_id = (int) $this->_request->op_id;
		
		$date = $this->view->date = $this->_request->getParam('date', null);
		
		if ( $req->isPost() ) {
			$validator = new Cubix_Validator();
			$m_package = new Model_Billing_Packages();
			
			
			if ( $date && mktime(date('H', $date), date('i', $date), 0, date('m', $date), date('d', $date), date('Y', $date)) <= mktime(date('H', time()), date('i', time()), 0, date('m', time()), date('d', time()), date('Y', time())) ) {
				$validator->setError('date', 'Date must be in future');
			}
			
			
			if ( $validator->isValid() ) {
				if ( $action == 'suspend' ) {
					$resp = $m_package->suspend($op_id, $date);
				}
				else if ( $action == 'resume' ) {
					$resp = $m_package->resume($op_id, $date);
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function susResStatAction()
	{
		$this->view->layout()->disableLayout();
		$op_id = $this->_request->op_id;
		
		$m_package = new Model_Billing_Packages();
		
		$this->view->data = $m_package->getSusResData($op_id);
	}
	
	public function fixPreviousPackagesAction()
	{
		Model_Packages::fixPrevioussPackages();
		die('done');
	}
	
	public function extendPackageAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$m_package = new Model_Billing_Packages();
				
		$op_id = $this->view->op_id = $req->id;
		$package = $m_package->get($op_id);
				
		$extend_products = $this->view->extend_products = $package->getExtendProducts();
		
		if ( $this->_request->isPost() ) { 
			
			$days = 3;
			
			if ( $this->_request->extension == Model_Billing_Packages::DL_PRODUCT_PLUS_6_DAYS || $this->_request->extension == Model_Billing_Packages::DP_PRODUCT_PLUS_6_DAYS ) {
				$days = 6;
			} else if ( $this->_request->extension == Model_Billing_Packages::DL_PRODUCT_PLUS_9_DAYS || $this->_request->extension == Model_Billing_Packages::DP_PRODUCT_PLUS_9_DAYS ) {
				$days = 9;
			}
			
			$ext_data = array(
				'order_id' => $package->order_id,
				'order_package_id' => $op_id,
				'days' => $days,
				'opt_product' => $this->_request->extension,
				'product_price' => $this->_request->amount,
				'order_price' => $package->order_price,
				'package_price' => $package->price
			);
			
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'user_id' => 'int',
				'orders' => 'arr-int',
				'transfer_type_id' => 'int',
				'date_transfered' => 'int',
				'amount' => 'double',
				'first_name' => '',
				'last_name' => '',
				'country_id' => 'int',
				'city' => '',
				'var_symbol' => '',
				'transaction_id' => '',
				'pull_person_id' => 'int',
				'webmoney_account' => 'int',
				'website_id' => 'int',
				'mtcn' => '',
				'comment' => 'notags|special',
				'application_id' => $this->_request->application_id
			));
			$data = $data->getData();			
			
			$validator = new Cubix_Validator();

			$extension = $this->_request->extension;
			if ( ! $extension ) {
				$validator->setError('extension', 'Required');
			}
			
			$data['user_id'] = $package->user_id;
			
			$data['orders'] = array($package->order_id);
			
			/*if ( ! $data['user_id'] ) {
				$validator->setError('global_error', 'Please select an agency or escort');
			}*/

			if ( ! $data['transfer_type_id'] ) {
				$validator->setError('transfer_type_id', 'Required');
			}

			$data['date_transfered'] = time();
			/*if ( ! $data['date_transfered'] ) {
				$validator->setError('date_transfered', 'Required');
			}*/

			if ( ! $data['amount'] ) {
				$validator->setError('amount', 'Required');
			}

			if ( $data['transfer_type_id'] == 5 ) {
				if ( ! $data['webmoney_account'] ) {
					$validator->setError('webmoney_account', 'Required');
				}
				
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}

			if ( $data['transfer_type_id'] == 3 ) {
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] != 5 ) {
				if ( $data['transfer_type_id'] && ! $data['first_name'] ) {
					$validator->setError('first_name', 'Required');
				}

				if ( $data['transfer_type_id'] && ! $data['last_name'] ) {
					$validator->setError('last_name', 'Required');
				}
			}

			$data['backend_user_id'] = Zend_Auth::getInstance()->getIdentity()->id;

			if ( $data['transaction_id'] ) {
				$m_transfer = new Model_Billing_Transfers();
				if ( false === $m_transfer->checkTransactionId($data['transaction_id']) ) {
					$validator->setError('transaction_id', 'Already in database');
				}
			}
			
			if ( $validator->isValid() ) {
				
				$_model = new Model_Billing_Transfers();
				$o_model = new Model_Billing_Orders();
				
				$o_model->extendOrder($ext_data);
				
				if ( true !== ($result = $_model->save($transfer = new Model_Billing_TransferItem($data))) ) {
					//$validator->setError('global_error', $result);
				}
				
				Cubix_SyncNotifier::notify($package->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PACKAGE_EXPIRATION_UPDATED, array('id' => $op_id));
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function addExtraCountryAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$op_id = $this->view->op_id = $req->id;
		
		$m_package = new Model_Billing_Packages();
		
		$this->view->package = $package = $m_package->get($op_id);
		
		$days = ((strtotime($package->expiration_date) - strtotime(date('Y-m-d'))) / 60 / 60 / 24);
		$cur_price = ceil($package->price / $package->period * $days);
		$this->view->extra_country_price = ceil($cur_price * 75 / 100);
		
		if ( $this->_request->isPost() ) { 
			//var_dump($req->extra_country, $req->extra_premium, $req->amount, $req->id, $package->order_id);die;
			
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'user_id' => 'int',
				'orders' => 'arr-int',
				'transfer_type_id' => 'int',
				'date_transfered' => 'int',
				'amount' => 'double',
				'first_name' => '',
				'last_name' => '',
				'country_id' => 'int',
				'city' => '',
				'var_symbol' => '',
				'transaction_id' => '',
				'pull_person_id' => 'int',
				'webmoney_account' => 'int',
				'website_id' => 'int',
				'mtcn' => '',
				'comment' => 'notags|special',
				'application_id' => $this->_request->application_id,
				'extra_country' => 'arr-int',
				'extra_premium' => 'int'
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			
			$data['user_id'] = $package->user_id;
			$data['orders'] = array($package->order_id);
			
			if ( ! $data['transfer_type_id'] ) {
				$validator->setError('transfer_type_id', 'Required');
			}

			$data['date_transfered'] = time();
			
			if ( $data['transfer_type_id'] == 5 ) {
				if ( ! $data['webmoney_account'] ) {
					$validator->setError('webmoney_account', 'Required');
				}
				
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}

			if ( $data['transfer_type_id'] == 3 ) {
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] != 5 ) {
				if ( $data['transfer_type_id'] && ! $data['first_name'] ) {
					$validator->setError('first_name', 'Required');
				}

				if ( $data['transfer_type_id'] && ! $data['last_name'] ) {
					$validator->setError('last_name', 'Required');
				}
			}

			$data['backend_user_id'] = Zend_Auth::getInstance()->getIdentity()->id;
			$data['status'] = Model_Billing_Transfers::STATUS_CONFIRMED;//Set status as confirmed. no need to confirm manually
			
			if ( $data['transaction_id'] ) {
				$m_transfer = new Model_Billing_Transfers();
				if ( false === $m_transfer->checkTransactionId($data['transaction_id']) ) {
					$validator->setError('transaction_id', 'Already in database');
				}
			}
			
			if ( count($data['extra_country']) < 2 ) {
				$validator->setError('e_city_spot_error', 'You must select 2 cities');
			}
			elseif (!$data['extra_premium']) {
				$validator->setError('e_city_spot_error', 'Check radio button for premium city');
			}
			
			if ( $validator->isValid() ) {
				$extra_data = array(
					'order_id' => $package->order_id,
					'order_package_id' => $op_id,
					'opt_product' => Model_Products::EXTRA_COUNTRY,
					'order_price' => $package->order_price,
					'package_price' => $package->price,
					'total_price' => $data['amount'],
					'escort_id'	=> $package->escort_id,
					'extra_country' => $data['extra_country'],
					'extra_premium' => $data['extra_premium']
				);
				
				unset($data['extra_country']);
				unset($data['extra_premium']);
				
				$_model = new Model_Billing_Transfers();
				$o_model = new Model_Billing_Orders();
				
				$o_model->addExtraCountry($extra_data);
				
				if ( true !== ($result = $_model->save($transfer = new Model_Billing_TransferItem($data))) ) {
					
				}
				
				Cubix_SyncNotifier::notify($package->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PACKAGE_EXPIRATION_UPDATED, array('id' => $op_id));
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function addGotdAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$m_package = new Model_Billing_Packages();
		$m_products = new Model_Products();
				
		$op_id = $this->view->op_id = $req->id;
		$package = $m_package->get($op_id);
		
		$this->view->escort_id = $package->escort_id;
		
		$m_escort = new Model_EscortsV2();
		$this->view->escort = $m_escort->get($package->escort_id);
		
		if ( $package->activation_type == Model_Billing_Packages::ACTIVATE_AT_EXACT_DATE && ! $package->expiration_date ) {
			$package->date_activated = $package->activation_date;
			$package->date_activated = date('Y-m-d', strtotime($package->date_activated));
			$package->expiration_date = date('Y-m-d H:i:s', strtotime($package->activation_date) + 60 * 60 * 24 * ($package->period - 1));
		} elseif ( $package->activation_type == Model_Billing_Packages::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES && ! $package->expiration_date ) {
			$prev_package = $m_package->get($package->previous_order_package_id);
			$package->date_activated = $prev_package->expiration_date;
			$package->expiration_date = date('Y-m-d H:i:s', strtotime($package->date_activated) + 60 * 60 * 24 * ($package->period - 1));
			
		} else {
			$package->expiration_date = date("Y-m-d", strtotime($package->expiration_date) - 24 * 60 * 60); //fixing expiration date
		}
		
		// Only for zero packages / default packages
		/*if ( ! $package->order_id ) {
			$package->expiration_date = '2013-03-05';
		}*/
		
		$this->view->package = $package;

		$application_id = Cubix_Application::getId();
		
		$is_agency = $package->agency_id ? true : false;
		$product_gotd = $m_products->getWithPrice($application_id, $is_agency, $package->gender, GIRL_OF_THE_DAY);
		
		//Task BEN-157.
		if ( $application_id == APP_BL) {
			$product_gotd->price = 15;
		}
		
		$this->view->product_gotd = $product_gotd;
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$this->view->booked_days = $booked_days = $client->call('Billing.getGotdBookedDays');
		
		/*if ( Cubix_Application::getId() == APP_A6 ) {
			$this->view->region_relations = $region_relations = array(
				45	=> array(45, 17),
				17	=> array(45, 17),

				21	=> array(21, 22),
				22	=> array(21, 22),
				
				44	=> array(44, 20),
				20	=> array(44, 20),
			);
		} else {*/
			$this->view->region_relations = $region_relations = array();
		//}
		
		if ( $this->_request->isPost() ) { 
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'user_id' => 'int',
				'orders' => 'arr-int',
				'transfer_type_id' => 'int',
				'date_transfered' => 'int',
				'amount' => 'double',
				'first_name' => '',
				'last_name' => '',
				'country_id' => 'int',
				'city' => '',
				'var_symbol' => '',
				'transaction_id' => '',
				'pull_person_id' => 'int',
				'webmoney_account' => 'int',
				'website_id' => 'int',
				'mtcn' => '',
				'comment' => 'notags|special',
				'application_id' => $this->_request->application_id,
				'city_id'	=> 'int',
				'days'	=> '',
				
				'discount'	=> '',
				'fix_discount'	=> '',
				'd_comment'	=> '',
			));
			$data = $data->getData();			
			
			$validator = new Cubix_Validator();

			
			
			$data['user_id'] = $package->user_id;
			$data['orders'] = array($package->order_id);
			
			
			if ( $data['days'] ) {
				$data['days'] = explode(';', $data['days']);
			} else {
				$data['days'] = array();
			}
			
			if ( ! count($data['days']) ) {
				$validator->setError('days', 'Required');
			} else {
				
				foreach($data['days'] as $k => $day) {
					$day = explode(',', $day);
					$data['days'][$k] = mktime(0,0,0, $day[1], $day[2], $day[0]);
					
					//Escort could have only one package per day
					if ($data['city_id'] ) {
						$escort_gotds = $client->call('Billing.getEscortGotdBookedDays', array($package->escort_id));
						
						foreach($escort_gotds as $gotd) {
							if ( date('Y-m-d', $data['days'][$k]) == date('Y-m-d', strtotime($gotd['date'])) ) {
								$validator->setError('days', 'Escort could have only one package per day');
							}
						}
					}
				}
			}
			
			if ( ! $data['city_id'] ) {
				$validator->setError('city_id', 'Required');
			} else {
				$city_also_check[] = $data['city_id'];
				if ( isset($region_relations[$data['city_id']]) ) {
					$city_also_check = $region_relations[$data['city_id']];
				}
			}
			$this->view->city_also_check = $city_also_check;
			
			if ( ! $data['transfer_type_id'] ) {
				$validator->setError('transfer_type_id', 'Required');
			}

			$data['date_transfered'] = time();
			
			if ( ! $data['amount'] && ! $data['discount'] ) {
				$validator->setError('days', 'Amount Required');
			}

			if ( $data['transfer_type_id'] == 5 ) {
				if ( ! $data['webmoney_account'] ) {
					$validator->setError('webmoney_account', 'Required');
				}
				
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}

			if ( $data['transfer_type_id'] == 3 ) {
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] != 5 ) {
				if ( $data['transfer_type_id'] && ! $data['first_name'] ) {
					$validator->setError('first_name', 'Required');
				}

				if ( $data['transfer_type_id'] && ! $data['last_name'] ) {
					$validator->setError('last_name', 'Required');
				}
			}
			
			$d_apps = array(APP_EF, APP_6A);
			if ( in_array(Cubix_Application::getId(), $d_apps) ) {
				if ( $data['discount'] > 0 && ! strlen($data['d_comment']) ) {
					$validator->setError('d_comment', 'Required');
				}
				
				$data['comment'] = $data['d_comment'];
			}
			$d_apps_2 = array(APP_BL);
			if ( in_array(Cubix_Application::getId(), $d_apps_2) ) {
				if ( $data['discount'] > 0) {
					$data['comment'] = $data['comment'] . ' (' .$data['discount'] . '% discount applied.)';
				}
				if ( $data['fix_discount'] > 0) {
					$data['comment'] = $data['comment'] . ' (' .$data['fix_discount'] . ' ' . Cubix_Application::getById()->currency_title .' discount applied.)';
				}
				
			}
			unset($data['d_comment']);
			unset($data['discount']);
			unset($data['fix_discount']);

			$data['backend_user_id'] = Zend_Auth::getInstance()->getIdentity()->id;
			$data['status'] = Model_Billing_Transfers::STATUS_CONFIRMED;//Set status as confirmed. no need to confirm manually
			
			if ( $data['transaction_id'] ) {
				$m_transfer = new Model_Billing_Transfers();
				if ( false === $m_transfer->checkTransactionId($data['transaction_id']) ) {
					$validator->setError('transaction_id', 'Already in database');
				}
			}
			
			if ( $validator->isValid() ) {
				$gotd_data = array(
					'order_id' => $package->order_id,
					'order_package_id' => $op_id,
					'opt_product' => GIRL_OF_THE_DAY,
					'product_price' => $product_gotd->price,
					'order_price' => $package->order_price,
					'package_price' => $package->price,
					'total_price' => $this->_request->amount,
					'escort_id'	=> $package->escort_id,
					'city_id'	=> $data['city_id'],
					'days'		=> $data['days']
				);
				unset($data['city_id']);
				unset($data['days']);
				$_model = new Model_Billing_Transfers();
				$o_model = new Model_Billing_Orders();
				
				
				//var_dump($data);die;
				if ( $transfer_id = $_model->save($transfer = new Model_Billing_TransferItem($data), true) ) {
					//$validator->setError('global_error', $result);					
				}

				
				//ATTENTION !!! confirm only when transfer types is not bank transfer/sales einkassiert.
				//transfer is confirming in save function above
				if ( ($data['transfer_type_id'] != 1 || Cubix_Application::getId() != APP_A6) && $data['transfer_type_id'] != 6 ) {
					$tr = new Model_Billing_TransferItem(array('id' => $transfer_id));
					$tr->confirm('GOTD Transfer', $data['amount']);
				}
				
				$gotd_data['transfer_id'] = $transfer_id;
				$o_model->addGotd($gotd_data);
				Cubix_SyncNotifier::notify($package->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PACKAGE_EXPIRATION_UPDATED, array('id' => $op_id));
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	
	public function premiumCitiesAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$op_id = $this->view->op_id = $req->id;
		$escort_id = $this->view->escort_id = $req->escort_id;
		
		$m_packages = new Model_Billing_Packages();
		
		$package = $m_packages->get($op_id);
		
		$this->view->working_cities = Model_EscortsV2::getWorkingLocations($escort_id);
		$this->view->premium_cities = $m_packages->getPremiumCities($op_id);
		
		if ( $this->_request->isPost() ) { 
			$premium_cities = $req->premium_cities;
			
			$validator = new Cubix_Validator();
			
			if ( ! $package->showChangePremiumCities()) die;//Only with product city premium spot. change on demand
			
			$add_city_product = $m_packages->checkIfHasAdditionalCity($op_id);//Additional city product
			
			$allowed_cities = 1;
			switch ($add_city_product) {
				case Model_Products::ADDITIONAL_CITY:
					$allowed_cities += 1;
					break;
				case Model_Products::ADDITIONAL_CITY_PREMIUM_SPOT_CITY_1:
					$allowed_cities += 1;
					break;
				case Model_Products::ADDITIONAL_CITY_PREMIUM_SPOT_CITY_2:
					$allowed_cities += 2;
					break;
				case Model_Products::ADDITIONAL_CITY_PREMIUM_SPOT_CITY_3:
					$allowed_cities += 3;
					break;
			}
			
			if ( ! count($premium_cities) ) {
				$validator->setError('premium_city', 'Select premium city');
			}
			
			if ( count($premium_cities) != $allowed_cities ) {
				$validator->setError('premium_city', 'Select ' . $allowed_cities . ' cities');
			}
			
			if ( $validator->isValid() ) {
				$m_packages->changePremiumCities($escort_id ,$op_id, $premium_cities);
			}
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function updateGotdAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$m_package = new Model_Billing_Packages();
		$m_products = new Model_Products();
				
		$op_id = $this->view->op_id = $req->id;
		$go_id = $this->view->go_id = $req->go_id;
		$package = $m_package->get($op_id);
		
		$this->view->escort_id = $package->escort_id;
		$m_escort = new Model_EscortsV2();
		$this->view->escort = $m_escort->get($package->escort_id);
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$escort_package = (object)$client->call('Escorts.checkIfHasActivePackage', array($package->escort_id));
		$escort_package->date_activated = date('Y-m-d H:i:s', $escort_package->date_activated);
		$escort_package->expiration_date = date('Y-m-d H:i:s', $escort_package->expiration_date);
		
		if(isset($escort_package->pending_package_activation_date)){
			$escort_package->pending_package_activation_date = date('Y-m-d', $escort_package->pending_package_activation_date);
			$escort_package->pending_package_expiration_date = date('Y-m-d', $escort_package->pending_package_expiration_date);
		}
		$this->view->package = (object) $escort_package;
		/*if ( $package->activation_type == Model_Billing_Packages::ACTIVATE_AT_EXACT_DATE ) {
			$package->date_activated = $package->activation_date;
			$package->expiration_date = date('Y-m-d H:i:s', strtotime($package->activation_date) + 60 * 60 * 24 * $package->period);
		} else {
			$package->expiration_date = date("Y-m-d", strtotime($package->expiration_date) - 24 * 60 * 60); //fixing expiration date
		}*/

		$application_id = Cubix_Application::getId();
		
		$is_agency = $package->agency_id ? true : false;
		$this->view->product_gotd = $product_gotd = $m_products->getWithPrice($application_id, $is_agency, $package->gender, GIRL_OF_THE_DAY);
		
		
		
		$this->view->booked_days = $booked_days = $client->call('Billing.getGotdBookedDays');
		
		if ( Cubix_Application::getId() == APP_A6 ) {
			$this->view->region_relations = $region_relations = array(
				45	=> array(45, 17),
				17	=> array(45, 17),

				44	=> array(44, 20),
				20	=> array(44, 20),
			);
		} else {
			$this->view->region_relations = $region_relations = array();
		}
		
		if ( $this->_request->isPost() ) { 
			
			
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'city_id'	=> 'int',
				'days'	=> ''
			));
			$data = $data->getData();			
			
			$validator = new Cubix_Validator();
			
			if ( $data['days'] ) {
				$data['days'] = explode(';', $data['days']);
			} else {
				$data['days'] = array();
			}
			
			if ( ! count($data['days']) ) {
				$validator->setError('days', 'Required');
			} else {
				foreach($data['days'] as $k => $day) {
					$day = explode(',', $day);
					$data['days'][$k] = mktime(0,0,0, $day[1], $day[2], $day[0]);
					
					//Escort could have only one package per day
					if ($data['city_id'] ) {
						$escort_gotds = $client->call('Billing.getEscortGotdBookedDays', array($package->escort_id, $go_id));
						
						foreach($escort_gotds as $gotd) {
							if ( date('Y-m-d', $data['days'][$k]) == date('Y-m-d', strtotime($gotd['date'])) ) {
								$validator->setError('days', 'Escort could have only one package per day');
							}
						}
					}
				}
			}
			
			if ( ! $data['city_id'] ) {
				$validator->setError('city_id', 'Required');
			} else {
				$city_also_check[] = $data['city_id'];
				if ( isset($region_relations[$data['city_id']]) ) {
					$city_also_check = $region_relations[$data['city_id']];
				}
			}
			$this->view->city_also_check = $city_also_check;
			
			if ( $validator->isValid() ) {
				
				$gotd_data = array(
					'city_id' => $data['city_id'],
					'activation_date' => date('Y-m-d', $data['days'][0])
				);
				
				$o_model = new Model_Billing_Orders();
				
				$o_model->updateGotd($go_id, $package->escort_id, $gotd_data);
				
				Cubix_SyncNotifier::notify($package->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PACKAGE_EXPIRATION_UPDATED, array('id' => $op_id));
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function addEscortCommentAction()
	{
		$this->view->layout()->disableLayout();
		$ids = $this->view->ids = $this->_request->id;
		
		if ( $this->_request->isPost() ) 
		{ 		
			if ( count($ids) ) 
			{
				foreach( $ids as $k => $id ) 
				{
					if (!ctype_digit($id))
						unset($ids[$k]);
				}
			}
			
			$validator = new Cubix_Validator();
			
			if ( !$this->_request->comment ) 
			{
				$validator->setError('comment', 'Required');
			}
			
			if ( $validator->isValid() ) 
			{
				$bu_user = Zend_Auth::getInstance()->getIdentity();
								
				foreach ($ids as $id)
				{
					$this->_model->addEscortComment($id, $this->_request->comment, $bu_user->id);
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function moveAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$this->view->escort_id = $escort_id = $req->escort_id;
		$this->view->agency_id = $agency_id = $req->agency_id;
		$this->view->order_package_id = $order_package_id = $req->id;
		$prem_cities = $req->prem_cities;
		
		
		$m_packages = new Model_Billing_Packages();
		
		$package = $m_packages->get($order_package_id);
		
		$allowed_cities = 0;
		if ( $package->showChangePremiumCities() ) {
			$allowed_cities = 1;
			$add_city_product = $m_packages->checkIfHasAdditionalCity($order_package_id);
			switch ($add_city_product) {
				case Model_Products::ADDITIONAL_CITY:
					$allowed_cities += 1;
					break;
				case Model_Products::ADDITIONAL_CITY_PREMIUM_SPOT_CITY_1:
					$allowed_cities += 1;
					break;
				case Model_Products::ADDITIONAL_CITY_PREMIUM_SPOT_CITY_2:
					$allowed_cities += 2;
					break;
				case Model_Products::ADDITIONAL_CITY_PREMIUM_SPOT_CITY_3:
					$allowed_cities += 3;
					break;
			}
		}
		
		$this->view->allowed_cities = $allowed_cities;
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$this->view->escorts = $escorts = $client->call('premium_getLinkedAgencyNonPremiumEscorts', array($agency_id));
		
		if ( $req->isPost() ) {
			$validator = new Cubix_Validator();
			
			if ( ! $client->call('Agencies.hasEscort', array($agency_id, $escort_id, true)) ||
					! $client->call('Agencies.hasEscort', array($agency_id, $req->to_escort_id, true))
			) {
				throw new Exception('Escort doesn\' belong to agency');
			}
			
			if ( ! $req->to_escort_id ) {
				$validator->setError('to_escort_id', 'Select escort');
			}
			
			if ( $allowed_cities < count($prem_cities) ) {
				$validator->setError('to_escort_id', 'You can select only ' . $allowed_cities . ' cities');
			}
			 
			
			
			if ( $validator->isValid() ) {
				$client->call('premium_switchActivePackages', array($escort_id, $req->to_escort_id, $prem_cities, Zend_Auth::getInstance()->getIdentity()->id));
			}
			die(json_encode($validator->getStatus()));
		}
	}
		
	public function getEscortCitiesAction()
	{
		$escort_id = intval($this->_getParam('escort_id'));
		$cities = Cubix_Api::getInstance()->call('premium_getEscortCities', array($escort_id,Cubix_I18n::getLang()));
		
		die(json_encode($cities['working']));
	}
	
	public function movingHistoryAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$this->view->agency_id = $agency_id = $req->agency_id;
		$this->view->order_package_id = $order_package_id = $req->id;
		$this->view->history = $history = $this->_model->getPackageMovingHistory($order_package_id);
	}
	
	public function addEdProductAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$package = $this->_model->get($req->id);
		$this->_model->addEdProduct($req->id);
		Cubix_SyncNotifier::notify($package->escort_id, Cubix_SyncNotifier::EVENT_ED_LISTING_PRODUCT_ADDED, array());
		die;
	}
	
	public function removeEdProductAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$package = $this->_model->get($req->id);
		$this->_model->removeEdProduct($req->id);
		Cubix_SyncNotifier::notify($package->escort_id, Cubix_SyncNotifier::EVENT_ED_LISTING_PRODUCT_DELETED, array());
		die;
	}
}
