<?php

class Billing_TransfersController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_Transfers
	 */
	protected $_model;

	public function init()
	{
		$this->view->model = $this->_model = new Model_Billing_Transfers();
		$this->temp_docs = new Zend_Session_Namespace('temp_docs');
	}

	public function indexAction()
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('billing_transfers_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */

		/*$this->view->transfer_data = array(
			'total' => number_format($this->_model->getTotalTransfers(), 2),
			'paid' => number_format($this->_model->getTotalTransfersPaid(), 2)
		);*/
	}

	public function dataAction()
	{
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);
		
		$data->setFields(array(
			'transfer_type_id' => 'int',
			'transaction_id' => '',
			'mtcn' => '',
			'cc_number' => '',
			'cc_email' => '',
			'var_symbol' => '',
			'first_name' => '',
			'last_name' => '',
			'pull_person_id' => '',
            'cc_info' => '',
			'cc' => '',


			'sales_user_id' => 'int',
			'status' => 'int',
			'filter_by' => '',
			'date_from' => 'int',
			'date_to' => 'int',
			'user_id' => '',
			'status' => 'int',

			'ads_id' => 'int',
			'email' => '',
			'sys_order_id' => '',
			'has_comment' => '',
            'is_mobile' => 'int',
			'fraudulent' => 'int',
            'contains_disc_orders' => '',
			'contains_payment_ad_reation' => '',

		));
		
		$filter = $data->getData();

		$transfers = $this->_model->getAllV2(
			$filter,
			$req->page,
			$req->per_page,
			$req->sort_field,
			$req->sort_dir
		);

		die(json_encode($transfers));
	}
	
	public function dataCountAction()
	{
		$this->view->layout()->disableLayout();
		
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);
		$type = $this->_request->getParam('type');
		$data->setFields(array(
                    'transfer_type_id' => 'int',
                    'transaction_id' => '',
                    'mtcn' => '',
                    'cc_number' => '',
                    'cc_email' => '',
                    'var_symbol' => '',
                    'first_name' => '',
                    'last_name' => '',
                    'pull_person_id' => '',
                    'cc_info' => '',


                    'is_credit' => 'int',
                    'sales_user_id' => 'int',
                    'status' => 'int',
                    'filter_by' => '',
                    'date_from' => 'int',
                    'date_to' => 'int',
                    'user_id' => '',
                    'status' => 'int',

                    'showname' => '',
                    'ads_id' => 'int',
                    'email' => '',
                    'agency_name' => '',
                    'escort_type' => '',
                    'sys_order_id' => '',

                    'has_comment' => '',

                    'contains_disc_orders' => '',
                    'include_special_transfers' => '',
		));
		$data = $data->getData();

		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		$filter = array();

		$special_data = array();

		if ( $data['sys_order_id'] ) {
			$filter['o.system_order_id = ?'] = $data['sys_order_id'];
		}
		
		if ( $data['contains_disc_orders'] ) {
			$filter['contains_disc_orders'] = true;
		}

		if ( $data['include_special_transfers'] ) {
			$filter['include_special_transfers'] = true;
		}
		
//                $escort_type_filter = $data['escort_type'];
//
//                if ( $escort_type_filter == 'independent' ) {
//                        $filter['e.agency_id IS NULL'] = true;
//                } elseif ( $escort_type_filter == 'agency' ) {
//                        $filter['e.agency_id IS NOT NULL'] = true;
//                }
		

		if ( $data['is_credit'] == 0 ) {
			$filter['t.is_credit_transfer = 0'] = true;
		}
		else{
			$filter['t.is_credit_transfer = 1'] = true;
		}
		
		if ( $data['ads_id'] ) {
			$filter['a.id = ?'] = $data['ads_id'];
		}

		if ( $data['email'] ) {
			$filter['u.email LIKE ?'] = $data['email'] . '%';
		}

		if ( $data['agency_name'] ) {
			$filter['a.name LIKE ?'] = $data['agency_name'] . '%';
		}
		if ( $data['transfer_type_id'] ) {
			$filter['t.transfer_type_id = ?'] = $data['transfer_type_id'];
		}
		
		if ( $data['transaction_id'] ) {
			$filter['t.transaction_id = ?'] = $data['transaction_id'];
			$filter['show_special'] = $data['transaction_id'];
		}
		
		if ( $data['var_symbol'] ) {
			$filter['t.var_symbol = ?'] = $data['var_symbol'];
		}
		if ( $data['mtcn'] ) {
			$filter['t.mtcn = ?'] = $data['mtcn'];
		}
		if ( $data['cc_number'] ) {
			$filter['t.cc_number LIKE ?'] = '%' . $data['cc_number'];
			
			$special_data['special_cc_number'] = $data['cc_number'];
		}
		if ( $data['cc_email'] ) {
			$filter['t.cc_email LIKE ?'] = '%' . $data['cc_email'] . '%';
			
			$special_data['special_cc_email'] = $data['cc_email'];
		}
		if ( $data['first_name'] ) {
			$filter['t.first_name = ?'] = $data['first_name'];
			
			$special_data['special_first_name'] = $data['first_name'];
		}
		if ( $data['last_name'] ) {
			$filter['t.last_name = ?'] = $data['last_name'];
			
			$special_data['special_last_name'] = $data['last_name'];
		}
		if ( $data['pull_person_id'] ) {
			$filter['t.pull_person_id = ?'] = $data['pull_person_id'];
		}
		
		if ( $data['has_comment'] ) {
			$filter['t.comment IS NOT NULL AND t.comment <> \'\''] = true;
		}

		if ( $bu_user->type == 'superadmin' ) {
			if ( $data['sales_user_id'] ) {
				$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
			}
		}
		else {
			// if ( $bu_user->type == 'sales manager' || $bu_user->type == 'sales clerk' ) {
			// 	$filter['t.backend_user_id = ?'] = $bu_user->id;
			// 	//$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
			// }
			// else if ($bu_user->type == 'admin') {
			// 	//$filter['bu.application_id = ?'] = $bu_user->application_id;
			// 	if (Cubix_Application::getId() != APP_BL && $bu_user->username != "tom" ) {
			// 		$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
			// 	}
			// }
		}

		if ( $data['status'] ) {
			$filter['t.status = ?'] = $data['status'];
		}

		$data['filter_by'] = 'date_transfered';
		if ( $data['filter_by'] ) {
			if ( in_array($data['filter_by'], array('date_transfered')) ) {
				if ( $data['date_from'] ) {
					$filter['DATE(' . $data['filter_by'] . ') >= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_from'] . '))');
				}

				if ( $data['date_to'] ) {
					$filter['DATE(' . $data['filter_by'] . ') <= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_to'] . '))');
				}
			}
		}

		if ( strlen($data['user_id']) > 0 ) {
			$filter['u.id = ?'] = $data['user_id'];
		}
		
		$s = implode('_', $filter);
         
		//$cache_key =  'transfers_' . preg_replace('#[^a-zA-Z0-9_]#', '_', $s) . $type.'_'.$data['escort_type'];
		
		//$cache = Zend_Registry::get('cache');
//        var_dump($filter);die;
		//if ( ! $data = $cache->load($cache_key) )
		//{
			$data = $this->_model->getAllCountV2(
				$filter,
				$type
			);
			
			//$cache->save($data, $cache_key, array(), 3600); //1 hours
		//}

		$this->view->data = $data;
//		var_dump($data);die;
		$this->view->type = $req->type;
	}

	public function createAction()
	{
		$this->view->layout()->disableLayout();

		$order_id = intval($this->_request->order_id);

		if ( $order_id ) {
			$model = new Model_Billing_Orders();
			$order = $model->getDetails($order_id);
			if ( $order ) {
				$this->view->order = $order;
			}
		}
		
		$this->view->bu_user = $bu_user = Zend_Auth::getInstance()->getIdentity();

		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			
			$params = array(
				'user_id' => 'int',
				'orders' => 'arr-int',
				'transfer_type_id' => 'int',
				'date_transfered' => 'int',
				'amount' => 'double',
				'first_name' => '',
				'last_name' => '',
				'country_id' => 'int',
				'city' => '',
				'var_symbol' => '',
				'transaction_id' => '',
				'pull_person_id' => 'int',
				'webmoney_account' => 'int',
				'website_id' => 'int',
				'mtcn' => '',
				'comment' => 'notags|special',
			);
						
			$data->setFields($params);
			
			$data = $data->getData();

			
			/*if ( Cubix_Application::getId() == APP_A6 && $bu_user->type != 'superadmin' ) {
				$data['date_transfered'] = time();
			}*/
			
			$validator = new Cubix_Validator();

			if ( ! $data['user_id'] ) {
				$validator->setError('global_error', 'Please select an agency or escort');
			}

			if ( ! $data['transfer_type_id'] ) {
				$validator->setError('transfer_type_id', 'Required');
			}

			if ( ! $data['date_transfered'] ) {
				$validator->setError('date_transfered', 'Required');
			}

			if ( ! $data['amount'] ) {
				$validator->setError('amount', 'Required');
			}

			if ( $data['transfer_type_id'] == 5 ) {
				if ( ! $data['webmoney_account'] ) {
					$validator->setError('webmoney_account', 'Required');
				}
				
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}

			if ( $data['transfer_type_id'] == 3 ) {
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] != 5 ) {
				if ( $data['transfer_type_id'] && ! $data['first_name'] ) {
					$validator->setError('first_name', 'Required');
				}
				
				if ( $data['transfer_type_id'] && ! $data['last_name'] ) {
					$validator->setError('last_name', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] == 7 ||  $data['transfer_type_id'] == 2) {
				if ( ! $data['mtcn'] ) {
					$validator->setError('mtcn', 'Required');
				}
			}

			$data['backend_user_id'] = Zend_Auth::getInstance()->getIdentity()->id;

			if ( $data['transaction_id'] ) {
				if ( false === $this->_model->checkTransactionId($data['transaction_id']) ) {
					$validator->setError('transaction_id', 'Already in database');
				}
			}

			if ( $validator->isValid() ) {
				if ( is_array($data['orders']) ) {
					$orders_model = new Model_Billing_Orders();
					$subtotal = 0;
					foreach ( $data['orders'] as $order_id ) {
						$price = $orders_model->getPrice($order_id);
						if ( is_null($price) ) continue;
						$subtotal += doubleval($price);
					}
					
					//$user = new Model_UserItem(array('id' => $data['user_id']));
					//if ( $subtotal > doubleval($data['amount']) + $user->getBalance()/* && $subtotal > 0*/ ) {
					//	$validator->setError('limited', 'Subtotal is more than balance');
					//}

					/*if ( $subtotal == 0 ) {
						$validator->unsetError('amount');
					}*/
				}
			}
			
			if ( $validator->isValid() ) {
				if ( true !== ($result = $this->_model->save($transfer = new Model_Billing_TransferItem($data))) ) {
					$validator->setError('global_error', $result);
				}
				else {
					// email to orders oweners
					if (is_array($data['orders']))
					{
						
						$orders_str = implode(',', $data['orders']);
						if ( strlen($orders_str) ) {
							$orders_oweners = $orders_model->getOweners($orders_str, $data['backend_user_id']);
						}
						
						if ($orders_oweners)
						{
							$orders_oweners_emails = array();
							$orders_oweners_ids = array();
							
							foreach ($orders_oweners as $owener)
							{
								$orders_oweners_emails[$owener->backend_user_id] = $owener->email;
								$orders_oweners_ids[] = $owener->backend_user_id;
							}
							
							if (count($orders_oweners_emails) > 0)
							{
								foreach ($orders_oweners_ids as $b_id)
								{
									$system_ids = $orders_model->getSystemIds($orders_str, $b_id);
									$sys_ids = array();

									foreach ($system_ids as $sys_id)
										$sys_ids[] = $sys_id->system_order_id;

									if (count($sys_ids) > 0)
									{
										$sys_ids_str = implode(',', $sys_ids);
										
										$email_data = array(
											'current_user_name' => Zend_Auth::getInstance()->getIdentity()->username,
											'sys_ids' => $sys_ids_str
										);

										Cubix_Email::sendTemplate('transfer_for_other_sales', $orders_oweners_emails[$b_id], $email_data);
									}
								}
							}
						}
					}
					//////////////////////////
					header('Cubix-Message: ' . 'Transfer Saved!');
				}
			}

			die(json_encode($validator->getStatus()));
		}
		else{
			if(isset($this->temp_docs->images)){

			/*	foreach ( $this->temp_docs->images as $photo ) {
					$images[] = new Cubix_Images_Entry(array(
						'application_id' => Cubix_Application::getId(),
						'catalog_id' => 'transfers',
						'hash' => $photo->hash,
						'ext' => $photo->ext
					));
			}
			$images_model = new Cubix_Images();
			$images_model->remove($images);*/
			unset($this->temp_docs->images);
				
			}
		}
	}

	public function detailsAction()
	{
		$this->view->layout()->disableLayout();

		$id = intval($this->_getParam('id'));

		$transfer = $this->_model->getDetails($id);
		$this->view->transfer = $transfer;
		
		//$this->view->transfer_imgs = $this->_model->getTransferImgsById($id);
		
	}

	public function editAction()
	{
		$this->view->layout()->disableLayout();

		$id = $this->view->id = intval($this->_request->id);

		$this->view->transfer = $this->_model->getDetails($id);
		
		$orders = array();
		if( count($this->view->transfer->orders) ) {
			$model = new Model_Billing_Orders();
			foreach ( $this->view->transfer->orders as $o ) {
				$order = $model->getDetails($o->id);
				
				if ( $order ) {
					$orders[] = $order;
				}
			}			
		}
		//var_dump(json_encode($orders));die;
		$this->view->orders = $orders;
		
		if ( $this->_request->isPost() ) {
			
			$data = new Cubix_Form_Data($this->_request);
			$bu_user = Zend_Auth::getInstance()->getIdentity();
			$params = array(
				'user_id' => 'int',
				'orders' => 'arr-int',
				'transfer_type_id' => 'int',
				'amount' => 'double',
				'first_name' => '',
				'last_name' => '',
				'country_id' => 'int',
				'city' => '',
				'var_symbol' => '',
				'transaction_id' => '',
				'pull_person_id' => 'int',
				'webmoney_account' => 'int',
				'website_id' => 'int',
				'mtcn' => '',
				'comment' => 'notags|special',
			);
			
			$data->setFields($params);
			
			$data = $data->getData();
			
			
			$validator = new Cubix_Validator();

			if ( ! $data['user_id'] ) {
				$validator->setError('global_error', 'Please select an agency or escort');
			}

			if ( ! $data['transfer_type_id'] ) {
				$validator->setError('transfer_type_id', 'Required');
			}

			/*if ( ! $data['date_transfered'] ) {
				$validator->setError('date_transfered', 'Required');
			}*/

			if ( ! $data['amount'] ) {
				$validator->setError('amount', 'Required');
			}

			if ( $data['transfer_type_id'] == 5 ) {
				if ( ! $data['webmoney_account'] ) {
					$validator->setError('webmoney_account', 'Required');
				}
				
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}

			if ( $data['transfer_type_id'] == 3 ) {
				if ( ! $data['transaction_id'] ) {
					$validator->setError('transaction_id', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] != 5 ) {
				if ( $data['transfer_type_id'] && ! $data['first_name'] ) {
					$validator->setError('first_name', 'Required');
				}

				if ( $data['transfer_type_id'] && ! $data['last_name'] ) {
					$validator->setError('last_name', 'Required');
				}
			}
			
			if ( $data['transfer_type_id'] == 7 ||  $data['transfer_type_id'] == 2) {
				if ( ! $data['mtcn'] ) {
					$validator->setError('mtcn', 'Required');
				}
			}
			//$data['backend_user_id'] = Zend_Auth::getInstance()->getIdentity()->id;

			if ( $data['transaction_id'] ) {
				if ( false === $this->_model->checkTransactionId($data['transaction_id'], $id) ) {
					$validator->setError('transaction_id', 'Already in database');
				}
			}
			
			if ( $validator->isValid() ) {
				$data['id'] = $id;
				//unset($data['date_transfered']);
				$ret = $this->_model->save(new Model_Billing_TransferItem($data));
				//var_dump($ret);die;
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;

		$transfer = $this->_model->get($id);
		if ( ! $transfer ) die;

		if ( Model_Billing_Transfers::STATUS_REJECTED == $transfer->status ) {
			$transfer->delete();
		}

		die;
	}

	public function rejectAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$transfer_id = intval($req->id);
		if ( ! $transfer_id ) die;
		
		$this->view->transfer = $transfer = $this->_model->getDetails($transfer_id);
		if ( ! $transfer ) die;
	
		//print_r($transfer->getActivePackages());

		$comment = $this->_getParam('reject_reason');

		if ( $req->isPost() ) {

			$validator = new Cubix_Validator();

			/*if ( strlen($req->cancel_packages) && count($req->selected) == 0 ) {
				$validator->setError('select_to_reject', 'You need to select orders to reject');
			}*/

			//$selected_orders = $req->selected;
			
			if ( ! strlen($comment) ) {
				$validator->setError('reject_reason', 'You need to leave Reason');
			}
			
			if ( $validator->isValid() ) {
				if ( Model_Billing_Transfers::STATUS_PENDING == $transfer->status || Model_Billing_Transfers::STATUS_CONFIRMED == $transfer->status ) {
					$transfer->reject($comment, $this->_request->cancel_packages);
					
					$bu_user = Zend_Auth::getInstance()->getIdentity();
					
					if (in_array($bu_user->type, array('admin', 'superadmin')) && $bu_user->id != $transfer->backend_user_id)
					{
						$o = $transfer->orders;
						$body = 'Order ID: ' . $o[0]['system_order_id'] . ' was rejected in date ' . date('d M Y');
						
						$m = new Model_BOUsers();
						$email = $m->get($transfer->backend_user_id)->email;
						
						if ($email)
							Cubix_Email::send($email, 'Transfer rejected', $body);
					}
					
					header('Cubix-Message: Transfer successfully rejected!');
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}
	
	public function chargebackAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$transfer_id = intval($req->id);
		if ( ! $transfer_id ) die;
		
		$this->view->transfer = $transfer = $this->_model->getDetails($transfer_id);
		if ( ! $transfer ) die;
	
		$comment = $this->_getParam('chargeback_reason');

		if ( $req->isPost() ) {
			$validator = new Cubix_Validator();
			if ( $validator->isValid() ) {
				if ( Model_Billing_Transfers::STATUS_PENDING == $transfer->status || Model_Billing_Transfers::STATUS_CONFIRMED == $transfer->status ) {
					$transfer->chargeback($comment, $this->_request->cancel_packages);
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function confirmAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		
		$this->view->transfer = $transfer = $this->_model->getDetails($id);
		
		if ( ! $transfer ) die;
		//$this->view->transfer_imgs = $this->_model->getTransferImgsById($id);
		$comment = $this->_getParam('comment');
		$amount = doubleval(doubleval(str_replace(',', '', $this->_getParam('amount'))));

		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			
			if ( ! $amount ) {
				$validator->setError('amount', 'Required');
			}

			if($validator->isValid()){
				if ( (Model_Billing_Transfers::STATUS_PENDING == $transfer->status) || (Model_Billing_Transfers::STATUS_REJECTED == $transfer->status) ) {
					$transfer->confirm($comment, $amount);
					header('Cubix-Message: Transfer successfully confirmed!');
				}
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function exportAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '2048M');
		
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);
		$data->setFields(array(
			'transfer_type_id' => 'int',
			'transaction_id' => '',
			'mtcn' => '',
			'var_symbol' => '',
			'first_name' => '',
			'last_name' => '',
			'pull_person_id' => '',


			'app_id' => 'int',
			'sales_user_id' => 'int',
			'status' => 'int',
			'filter_by' => '',
			'date_from' => 'int',
			'date_to' => 'int',
			'user_id' => '',
			'status' => 'int',
                        
			'showname' => '',
			'email' => '',
			'agency_name' => '',

			'sys_order_id' => ''
		));
		$data = $data->getData();

		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		// <editor-fold defaultstate="collapsed" desc="Filter">
		$filter = array();

		$filter['t.application_id'] = $this->_request->application_id;

		if ( $data['sys_order_id'] ) {
			$filter['o.system_order_id = ?'] = $data['sys_order_id'];
		}

		if ( $data['showname'] ) {
			$filter['e.showname LIKE ?'] = $data['showname'] . '%';
		}

		if ( $data['email'] ) {
			$filter['u.email LIKE ?'] = $data['email'] . '%';
		}

		if ( $data['agency_name'] ) {
			$filter['a.name LIKE ?'] = $data['agency_name'] . '%';
		}
		if ( $data['transfer_type_id'] ) {
			$filter['t.transfer_type_id = ?'] = $data['transfer_type_id'];
		}
		if ( $data['transaction_id'] ) {
			$filter['t.transaction_id = ?'] = $data['transaction_id'];
		}
		if ( $data['var_symbol'] ) {
			$filter['t.var_symbol = ?'] = $data['var_symbol'];
		}
		if ( $data['mtcn'] ) {
			$filter['t.mtcn = ?'] = $data['mtcn'];
		}
		if ( $data['first_name'] ) {
			$filter['t.first_name = ?'] = $data['first_name'];
		}
		if ( $data['last_name'] ) {
			$filter['t.last_name = ?'] = $data['last_name'];
		}
		if ( $data['pull_person_id'] ) {
			$filter['t.pull_person_id = ?'] = $data['pull_person_id'];
		}


		if ( $bu_user->type == 'superadmin' ) {
			if ( $data['sales_user_id'] ) {
				$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
			}
		}
		else {
			if ( $bu_user->type == 'sales manager' || $bu_user->type == 'sales clerk' ) {
				$filter['t.backend_user_id = ?'] = $bu_user->id;
				//$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
			}
			else if ($bu_user->type == 'admin') {
				$filter['bu.application_id = ?'] = $bu_user->application_id;
				$filter['t.backend_user_id = ?'] = $data['sales_user_id'];
			}
		}

		if ( $data['status'] ) {
			$filter['t.status = ?'] = $data['status'];
		}

		$data['filter_by'] = 'date_transfered';
		if ( $data['filter_by'] ) {
			if ( in_array($data['filter_by'], array('date_transfered')) ) {
				if ( $data['date_from'] ) {
					$filter['DATE(' . $data['filter_by'] . ') >= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_from'] . '))');
				}

				if ( $data['date_to'] ) {
					$filter['DATE(' . $data['filter_by'] . ') <= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_to'] . '))');
				}
			}
		}

		if ( strlen($data['user_id']) > 0 ) {
			$filter['u.id = ?'] = $data['user_id'];
		}
		// </editor-fold>
		
		$page = $req->page ? (int) $req->page : null;
		$per_page = $req->per_page ? (int) $req->per_page : null;
		$sort = $req->sort_field ? $req->sort_field : 't.date_transfered';
		$dir = $req->dir ? $req->dir : 'DESC';
		
		$data = $this->_model->getDataForExport(
			$filter,
			$page,
			$per_page,
			$sort,
			$dir
		);
		
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename=transfers.csv');
		
		$handle = fopen('php://output', 'w');
		foreach ( $data['data'] as $order ) {
			$arr = array(
				$order['user_type'], $order['username'], $order['transfer_type'], $order['status'], $order['full_name'], $order['orders']
			);
			
			if (Cubix_Application::getId() == APP_A6)
			{
				$arr[] = $order['region'];
			}
			
			$arr[] = $order['date_transfered'];
			$arr[] = $order['payment_date'];
			$arr[] = $order['paid_amount'];
			$arr[] = $order['sales_person'];
			$arr[] = $order['order_id'];
			
			fputcsv($handle, $arr);
		}
		fclose($handle);
		
		exit;
	}
	
	public function commentAction(){
		
		$this->view->layout()->disableLayout();
				
		if ( $this->_request->isPost() ) {
			$data = new Cubix_Form_Data($this->_request);
			$data->setFields(array(
				'id' => 'int',
				'comment' => 'special|notags'
			));
			$data = $data->getData();
			
			$validator = new Cubix_Validator();
			/*if(strlen(trim($data['comment'])) == 0){
				$validator->setError('comment', 'Comment Required');
			}*/
			
			
			if ( $validator->isValid() ) {
				
				$this->_model->updateCommentGrid($data);
			}

			die(json_encode($validator->getStatus()));
		}
		else
		{
			$id = intval($this->_request->id);
			$this->view->id = $id;
			$this->view->comment = $this->_model->getCommentGrid($id);
		}
		
	}
	
	public function editCreditCardInfoAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = $this->view->id = intval($this->_request->id);
		$this->view->transfer = $this->_model->getDetails($id);
		
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			$data = new Cubix_Form_Data($this->_request);
			$params = array(
				'cc_holder' => '',
				'cc_email' => '',
				'cc_number' => '',
				'cc_exp_month' => '',
				'cc_exp_day'	=> '',
				'cc_ip'	=> '',
				'cc_address'	=> ''
			);
			
			$data->setFields($params);
			$data = $data->getData();
			
			if ( $validator->isValid() ) {
				$data['id'] = $id;
				$ret = $this->_model->save(new Model_Billing_TransferItem($data));

				die(json_encode($validator->getStatus()));
			}
		}
	}
	
	public function findTransactionIdAction()
	{
		$this->view->layout()->disableLayout();
		
		$this->view->result = 'Nothing found.';
		
		$bu = Zend_Auth::getInstance()->getIdentity();
		if ( $bu->type != 'superadmin' ) {
			$this->view->result = 'You don\'t have such privileges.';
			return;
		}
		$transaction_id = $this->_request->transaction_id;
		
		if ( ! $transaction_id ) {
			$this->view->result = 'No transaction id specified';
			return;
		}
		
		
		$api_keys = array(
			'escortforumit.xxx' => array('api_key' => 'n2n6fp5t571b7c8qkdn17tliff5707ie', 'server' => 'http://server.escortforumit.xxx'),
			//'6annnonce.com' => array('api_key' => '188e18301e3c4d071cdc2c1aed9515eb', 'server' => 'http://server.6annonce.com'),
			'and6.ch' => array('api_key' => 'vva724hglxfryk4avfp2jbee67rbsp7u', 'server' => 'http://server.and6.ch'),
			'and6.com' => array('api_key' => 'vva724hglxfryk4avfp2jbee67rbsp7u', 'server' => 'http://server.and6.com'),
			'beneluxxx.com' => array('api_key' => '1fgzkv28mly8e034c496iq0ptttetp90', 'server' => 'http://server.beneluxxx.com'),
			'escortdirectory.com' => array('api_key' => '9p30yrtt4j1xbyzwscg5rkecz78vsitr', 'server' => 'http://server.escortdirectory.com'),
			'escortdguide.co.uk' => array('api_key' => 's4ovt76cgw09vzin88y2ohyxavgzofvm', 'server' => 'http://server.escortguide.co.uk'),
			'escortmeetings.com' => array('api_key' => 'cb4pbyx6wnae77dgi4xb32kk10cmvlkd', 'server' => 'http://server.escortmeetings.com')
			
		);

		$user_id = $this->_model->findTransactionId($transaction_id);
		
		if(is_int($user_id)){
			$this->view->result = 'This transfer is used in <b>Humpchies</b><br>User id - ' . $user_id ;
		}
	}
	
}
