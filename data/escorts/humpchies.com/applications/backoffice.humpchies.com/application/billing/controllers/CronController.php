<?php

class Billing_CronController extends Zend_Controller_Action
{
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $_db;

	public function init()
	{
		$this->_db = Zend_Registry::get('db');
	}

	public function indexAction()
	{
		
		
		/*if ( ! defined('IS_CLI') || ! IS_CLI ) {
			die('Please run from command line');
		}*/

		/*$cli = new Cubix_Cli();
		$pid_file = '/var/run/v2_billing_' . Cubix_Application::getId() . '.pid';
		$cli->setPidFile($pid_file);
		if ( $cli->isRunning() ) {
			die('Cron is already running, exitting...' . PHP_EOL);
		}*/

		set_time_limit(0);
		
		if ( ! is_null($this->_getParam('cron')) && (! defined('IS_CLI') || ! IS_CLI) ) {
			$result = Model_Cron::run(array('grace-period'));
		}

		do {
			//What is working on other projects
			/*$steps = array(
				'pending-transfers', 'pending-bank-transfers', 
				'pending-orders', 'expired-packages', 
				'asap-packages', 'exact-date-packages', 
				'payment-rejected-orders', 
				'fix-after-current-package-expires-packages',
				'active-packages-without-exp-date'
                ,'send-expire-notifications'
			);*/
			
			$steps = array(
				'expired-packages','asap-packages', 'exact-date-packages', 
				'payment-rejected-orders', 
				'fix-after-current-package-expires-packages',
				'active-packages-without-exp-date',
                //'send-expire-notifications'
			);
			
			$result = Model_Cron::run($steps);
			
			foreach ( $result['counts'] as $stage => $count ) {
				echo "$stage: $count<br/>\n";
			}
			
			echo "<br/>\n";
			echo 'Total: ' . (string) $result['count'] . "<br/>\n";
			echo "------------------------------------<br/>\n";
			
		} while ( $result['count'] > 0 );

		
		/*$apps_without_default_package = array(APP_A6, APP_A6_AT);
		
		if ( ! in_array(Cubix_Application::getId(), $apps_without_default_package) ) {
			$this->fixDefaultPackages();
		}*/

		die;
	}

	public function fixDefaultPackages(/*array $escorts*/)
	{		

		/*foreach ( $escorts as $escort ) {
			$escort = $this->_db->fetchRow('SELECT id, agency_id, gender, country_id FROM escorts WHERE id = ?', $escort);
			if ( ! $escort ) continue;
			$escort = new Model_EscortItem($escort);
			$escort->processDefaultPackages();
		}*/

		
		if ( Cubix_Application::getId() == APP_ED ) {//Do not activate default package for escorts synced from other projects
			$config = Zend_Registry::get('system_config');
			$escorts = $this->_db->fetchAll('
				SELECT c.id, c.agency_id, c.gender, c.country_id, c.home_city_id FROM (
					SELECT
						e.id, e.agency_id, e.gender, e.country_id, COUNT(a.id) AS apps, e.home_city_id/*, e.is_super_old*/
					FROM applications a
					RIGHT JOIN order_packages op ON op.application_id = a.id AND op.status = 2 AND a.id = ' . Cubix_Application::getId() . '
					RIGHT JOIN escorts e ON e.id = op.escort_id
					RIGHT JOIN users u ON u.id = e.user_id
					WHERE (e.status & ' . ESCORT_STATUS_ACTIVE  . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ') AND
						u.sales_user_id <> ' . $config['syncManagerUserId'] . '
					GROUP BY e.id
				) c WHERE c.apps = 0
			');
		} else {
			$escorts = $this->_db->fetchAll('
				SELECT c.id, c.agency_id, c.gender, c.country_id, c.home_city_id FROM (
					SELECT
						e.id, e.agency_id, e.gender, e.country_id, COUNT(a.id) AS apps, e.home_city_id/*, e.is_super_old*/
					FROM applications a
					RIGHT JOIN order_packages op ON op.application_id = a.id AND op.status = 2 AND a.id = ' . Cubix_Application::getId() . '
					RIGHT JOIN escorts e ON e.id = op.escort_id
					WHERE (e.status & ' . ESCORT_STATUS_ACTIVE  . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ')
					GROUP BY e.id
				) c WHERE c.apps = 0
			');
		}
		
		foreach ( $escorts as $escort ) {
			echo "Escort id " . $escort->id . "\n\r";
			$escort = new Model_EscortItem($escort);
			$escort->processDefaultPackages();
		}
	}
	
	public function removeUnusedIvrTransfersAction()
	{
		$unused_orders = $this->_db->fetchAll('SELECT o.id, t.transfer_id FROM orders o INNER JOIN transfer_orders t ON t.order_id = o.id WHERE o.backend_user_id = ? AND o.status = ?  AND TIME_TO_SEC(TIMEDIFF(NOW(), o.order_date)) > ?', array(100, Model_Billing_Orders::STATUS_PAYMENT_DETAILS_RECEIVED, 60*30));
		try {
			$this->_db->beginTransaction();
			foreach($unused_orders as $order) {
				echo 'Deleting order_id -> ' . $order->id . "\n";
				$this->_db->beginTransaction();
				$this->_db->delete('orders', $this->_db->quoteInto('id = ?', $order->id));
				$this->_db->delete('transfers', $this->_db->quoteInto('id = ?', $order->transfer_id));
			}
			$this->_db->commit();
		} catch(Exception $e) {
			$this->_db->rollBack();
			throw $e;
		}
		
		die('Done');
		
	}
	
	public function zeroBalanceAction()
	{
		if ( ! defined('IS_CLI') || ! IS_CLI ) {
			die('Please run from command line');
		}
		$model = new Model_Billing_Transfers();
		echo('Setting balance to zero. Please wait...') . "\n";
		$model->zeroBalance();
		die('done');
	}
	
	public function removeNotPaidGotdAction()
	{
		$this->_db->query('
			DELETE FROM gotd_orders
			WHERE status = 1  AND TIME_TO_SEC(TIMEDIFF(NOW(), creation_date)) > ?', array(60*30));
		
		die('Done');
		
	}
	
	public function activateGotdPhoneBillingAction()
	{
		$gotd_orders_to_activate = $this->_db->fetchAll('SELECT * FROM gotd_orders WHERE status = 2 AND DATE(activation_date) = DATE(NOW())');
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$product = $client->call('Billing.getProduct', array(GIRL_OF_THE_DAY));

		
		$this->_db->beginTransaction;
		foreach($gotd_orders_to_activate as $gotd) {
			try {
				$this->_db->update('order_packages', array(
					'gotd_status'	=> 1,
					'gotd_activation_date' => $gotd->activation_date,
					'gotd_city_id'	=> $gotd->city_id
				), $this->_db->quoteInto('id = ?', $gotd->order_package_id));


				$is_alread_exists = $this->_db->fetchOne('SELECT TRUE FROM order_package_products WHERE order_package_id = ? AND product_id = ?', array($gotd->order_package_id, GIRL_OF_THE_DAY));
				if ( ! $is_alread_exists ) {
					$this->_db->insert('order_package_products', array(
						'order_package_id'	=> $gotd->order_package_id,
						'product_id'		=> GIRL_OF_THE_DAY,
						'is_optional'		=> 1,
						'price'				=> $product['price']
					));
				}


				$this->_db->update('gotd_orders', array('status' => 3), $this->_db->quoteInto('id = ?', $gotd->id));

				$this->_db->commit();
				echo 'Escort:' . $gotd->escort_id . ' GOTD moved to order_package_products' . "/n";
			} catch (Exception $e) {
				$this->_db->rollBack();
				var_dump($e);
			}
		}
		
		die('done');
	}
	
	public function removeNotPaidBoostProfilesAction()
	{
		$this->_db->query('
			DELETE FROM profile_boost_orders
			WHERE status = 1 AND TIME_TO_SEC(TIMEDIFF(NOW(), creation_date)) > ?', array(60*30));
		
		die('Done');
	}
	
    public function snAction()
    {
        set_time_limit(0);
        
        include_once("../library/smtp.php");     
        include_once("../library/php_mailer.php");
        include_once("../library/sendmail.php");          
            
        $data = "";
        //$data = "'2019-05-03'";
        $packages = $this->_db->fetchAll("
            SELECT op.id, op.ads_id, op.status, previous_order_package_id, op.package_id, u.email AS user_email,
            DATEDIFF(op.expiration_date, DATE(NOW()) ) AS days_left, e.title, package_id
            FROM order_packages op
            INNER JOIN ads e ON e.id = op.ads_id
            INNER JOIN users u ON u.id = e.user_id
            WHERE 
                (DATEDIFF(op.expiration_date, DATE(NOW()) ) = ?   ) 
                AND op.status = ? 
                AND op.status <> ? 
        ", array(1, Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED));
        $from = "support@humpchies.com";
        $fromName = "Support Humpchies";          
        
        
        foreach($packages as $package ){
            $type = ($package->package_id < 19)? "VIP": "Premium";       
            $subject = ($package->package_id < 19)? "VIP arrive a expiration!": "Votre annonce premium arrive a expiration!" ;       
            //$subject = "Humpchies.com - " . $type. " pour l'annonce #" . $package->ads_id . " ";
            $body = "Bonjour,<br><br>
Votre option " . $type. " pour l'annonce <strong>#" . $package->ads_id . "</strong> expire dans un jour.<br> Si vous voulez la renouveler ou acheter une nouvelle option, vous pouvez le faire &agrave; partir de votre compte utilisateur.<br><br>
<a href='https://www.humpchies.com/user/login'style='display: block; padding: 10px; color: #000; font-weight: bold; line-height: 30px; text-align: center; border: 0px;background: #ff9900;  cursor: pointer; max-width: 150px; text-decoration:none' >ALLER AU COMPTE</a>";
            SendMail::send($package->user_email, $subject, $body, $body);
           
            //die();//show only the first;
        }   
        
        die("SENT");
    }
    
    public function newsAction(){
        set_time_limit(0);
        
        include_once("../library/smtp.php");     
        include_once("../library/php_mailer.php");
        include_once("../library/sendmail.php"); 
        
        $enContent = file_get_contents("/home/shadybyte/web/viper/modules/humpchies/views/generic/newsletter/news1-en.html");
        $frContent = file_get_contents("/home/shadybyte/web/viper/modules/humpchies/views/generic/newsletter/news1-fr.html");
        $sql = "SELECT DISTINCT email, username, `language`, last_login, U.id FROM  users U 
                LEFT JOIN 
                (SELECT MAX(login_date) AS last_login ,user_id FROM users_login_history GROUP BY user_id HAVING MAX(login_date)  > DATE_SUB(CURDATE(), INTERVAL 1 YEAR )  )  UH ON UH.user_id = U.id 
                WHERE date_banned = 0 AND `type` = 1 AND  U.id NOT IN (SELECT user_id FROM shopping_cart WHERE STATUS = 1 AND creation_date > DATE_SUB(NOW(), INTERVAL 42 DAY))
                and U.id not in (SELECT user_id from newsletter_sent_list)
                ORDER BY last_login DESC LIMIT 200";
                
        
        $userList =  $this->_db->fetchAll($sql);
        
        //SendMail::send("adroa79@gmail.com", "Attract more clients with these options", $enContent, true);
        //SendMail::send("adroa79@gmail.com", "Attirez plus de clients avec ces options", $frContent, true);
        // die("SENT3");
        
        $contor = 0;
        foreach($userList as $user){
            $emailData=array('to' => $user->email, 'toName' => $user->username, 'language' => $user->language);
            //var_dump($emailData);
            //user_id, username, email, `language`, last_login, sent, sent_date
            $data =array(
                            'user_id'       => $user->id,
                            'username'      => $user->username,
                            'language'      => $user->language,
                            //'last_login'    => $user->last_login,
                            'sent'          => 'Y',
                            'sent_date'     => date("Y-m-d H:i:s") 
                          );  
            if($user->language == 'EN'){
                $subject = "Attract more clients with these options";
                $body = $enContent;
            }else{
                $subject = "Attirez plus de clients avec ces options";
                $body =  $frContent;
            }
            //echo($subject."\n\n\n----------------------------------------------------------\n\n");
            //var_dump($body."\n\n\n----------------------------------------------------------\n\n\n");
            SendMail::send($emailData['to'], $subject, $body, $body);
            //SendMail::send("metro.nkvd@gmail.com", $subject, $body, true);
            //
            //SendMail::send("adroa79@gmail.com", $subject, $body, true);
            ///SendMail::send("baribalx3@gmail.com", $subject, $body, true);
            //die("SENT2");
           // if(SendMail::send("petic79@gmail.com", $subject, $body, true)){
//                echo "\n - Mail Sent\n\n\n";
//            } else{
//                 echo "\n - Mail Not Sent\n\n\n";
//            }
            //Model_Users::markSent($data);
            $this->_db->insert('newsletter_sent_list',$data);      
            $contor++;
        }
        
         die("SENT:" . $contor);  
        
    }
    
    public function rnAction()
    {
        set_time_limit(0);
        
        include_once("../library/smtp.php");     
        include_once("../library/php_mailer.php");
        include_once("../library/sendmail.php");          
        echo "AAAAA";    
        $data = "";
        //$data = "'2019-05-03'";
        $packages = $this->_db->fetchAll("
            SELECT DISTINCT email, username, `language`, last_login, U.id FROM  users U 
            LEFT JOIN (SELECT MAX(login_date) AS last_login ,user_id FROM users_login_history GROUP BY user_id HAVING MAX(login_date)  > DATE_SUB(CURDATE(), INTERVAL 1 YEAR )  )  UH ON UH.user_id = U.id 
            WHERE date_banned = 0 AND `type` = 1 
                AND U.id NOT IN (SELECT user_id FROM shopping_cart WHERE STATUS = 1 AND creation_date > DATE_SUB(NOW(), INTERVAL 42 DAY))
                AND U.id NOT IN (SELECT user_id FROM newsletter_sent_list )
            
            ORDER BY last_login DESC LIMIT 20
        ");
        $from = "support@humpchies.com";
        $fromName = "Support Humpchies";          
        
        
        foreach($packages as $package ){
           var_dump($package);
        }   
        
        die("SENT");
    }
    
    public function mailnotificationsAction(){
        //die("zzzzz");
        set_time_limit(0);
        
        include_once("../library/smtp.php");     
        include_once("../library/php_mailer.php");                                  
        include_once("../library/sendmail.php");          
        include_once("../library/email_notification.php");
        //echo "fill";       
       
        $messages = emailNotification::getMessages();
        emailNotification::sendMessages();
        exit;
    }
    
    public function sndnotificationsAction(){
        //die("zzzzz");
        set_time_limit(0);
        
        include_once("../library/smtp.php");     
        include_once("../library/php_mailer.php");
        include_once("../library/sendmail.php");          
        include_once("../library/email_notification.php");
        //echo "fill";       
       
        $messages = emailNotification::sendMessages();
        
        exit;
    }
    public function testsndAction(){
        //die("zzzzz");
        set_time_limit(0);
        
        include_once("../library/smtp.php");     
        include_once("../library/php_mailer.php");
        include_once("../library/sendmail.php");          
        include_once("../library/email_notification.php");
        //echo "fill";       
       
        $messages = emailNotification::testSendMessages();
        
        exit;
    }
    
    public function bancountryAction(){
         set_time_limit(0); 
         $sqlBanned = geo::getLockedSQL();
         $sql = "SELECT user_id FROM ads WHERE STATUS='new' AND ad_country_id IN (" . $sqlBanned . ") group by user_id";

         $ads = $this->_db->fetchAll($sql);
         $userModel = new Model_Users();
         $modelAds = new Model_Advertisements();
         
         foreach( $ads as $ad ){
            echo $ad->user_id . "\n";
            $userModel->floodingUser(intval($ad->user_id));
            $modelAds->disableUserAds(intval($ad->user_id));
         }
         die("banned");
    }
    
	public function sendNotificationAboutExpiringPackagesAction()
	{
		set_time_limit(0);
		
		$packages = $this->_db->fetchAll("
			SELECT op.id, op.ads_id, op.status, op.ads_id, previous_order_package_id, op.package_id, u.email AS user_email, u.email AS sales_email,
            DATEDIFF(op.expiration_date, DATE(NOW()) ) AS days_left, e.title
            FROM order_packages op
            INNER JOIN ads e ON e.id = op.ads_id
            INNER JOIN users u ON u.id = e.user_id
            WHERE 
				(DATEDIFF(op.expiration_date, DATE(NOW()) ) = ? || DATEDIFF(op.expiration_date, DATE(NOW()) ) = ?) 
				AND op.status = ? 
				AND op.status <> ? 
		", array(5, 1, Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED));
        print_R($packages);
        die();
		foreach($packages as $package) {
			//Checking if has pending package. 
			//We must send email only if they don't have pending package

			$has_active_package = $this->_db->fetchOne('
				SELECT TRUE FROM order_packages WHERE escort_id = ? AND status = ?
			', array($package->escort_id, Model_Billing_Packages::STATUS_PENDING));

			if ( ! $has_active_package ) {
				Cubix_Email::sendTemplate('package_expiring_notification', $package->user_email, array('days_left' => $package->days_left, 'showname' => $package->showname));
			}
		}
		die('done');
	}

	public function sendPackageExpirationMsgAction()
	{
		set_time_limit(0);		

		$conf = Zend_Registry::get('system_config');
		$conf = $conf['sms'];
		$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
		$sms = new Cubix_SMS($conf['userkey'], $conf['password']);
		$sms->setBufferedNotificationURL($conf['BufferedNotificationURL']);
		$sms->setDeliveryNotificationURL($conf['DeliveryNotificationURL']);
		$sms->setNonDeliveryNotificationURL($conf['NonDeliveryNotificationURL']);
		$sms->setOriginator($originator);

		$model_sms = new Model_SMS_Outbox();
		
		$packages = $this->_db->fetchAll("
			SELECT
				op.id, op.escort_id, op.status, op.application_id, previous_order_package_id, op.package_id, ep.contact_phone_parsed, bu.username AS sales_username, bu.id AS sales_person_id,
				DATEDIFF(op.expiration_date, DATE(NOW()) ) AS days_left, e.showname
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE 
				(DATEDIFF(op.expiration_date, DATE(NOW())) = ? || 
				DATEDIFF(op.expiration_date, DATE(NOW())) = ? ||
				DATEDIFF(op.expiration_date, DATE(NOW())) = ?)
				AND op.status = ? 
				AND op.status <> ?
				AND e.no_renew_sms = 0
		", array(5, 3, 1, Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED));
		
		foreach($packages as $package) {
			//Checking if has pending package. 
			//We must send sms only if they don't have pending package

			$has_active_package = $this->_db->fetchOne('
				SELECT TRUE FROM order_packages WHERE escort_id = ? AND status = ?
			', array($package->escort_id, Model_Billing_Packages::STATUS_PENDING));

			if ( ! $has_active_package ) {
				
				$template_title = $package->sales_username . '_' . $package->days_left;
				$message = $this->_db->fetchRow('
					SELECT template 
					FROM sms_templates WHERE title = ?
				', $template_title);

				$data = array(
					'escort_id' => $package->escort_id,
					'phone_number' => $package->contact_phone_parsed,
					'phone_from' => $originator,
					'text' => $message->template,
					'application_id' => Cubix_Application::getId(),
					'sender_sales_person' => $package->sales_person_id,
					'is_spam' => false
				);

				$msg_id = $model_sms->save($data);

				$sms->setRecipient($package->contact_phone_parsed, $msg_id);
				$sms->setContent($message->template);
				$sms->sendSMS();
			}
		}

		die('done');
	}
	
	public function sendSmsAboutExpiringPackagesAction()
	{
		set_time_limit(0);
		$conf = Zend_Registry::get('system_config');
		$conf = $conf['sms'];
		$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
		$sms = new Cubix_SMS($conf['userkey'], $conf['password']);
		$sms->setBufferedNotificationURL($conf['BufferedNotificationURL']);
		$sms->setDeliveryNotificationURL($conf['DeliveryNotificationURL']);
		$sms->setNonDeliveryNotificationURL($conf['NonDeliveryNotificationURL']);
		$sms->setOriginator($originator);
		
		$packages = $this->_db->fetchAll("
			SELECT
				op.id, op.escort_id, op.package_id, ep.contact_phone_parsed,
				TIMESTAMPDIFF(HOUR,NOW() ,CONVERT(op.expiration_date, DATETIME)) AS hours_left
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id 
			WHERE 
				op.package_id IN (13,18,22,24,25) AND
				TIMESTAMPDIFF(HOUR,NOW() ,CONVERT(op.expiration_date, DATETIME)) = 12 
				AND op.status = ? 
				AND op.status <> ?
			UNION
			SELECT
				op.id, op.escort_id, op.package_id, ep.contact_phone_parsed,
				TIMESTAMPDIFF(HOUR,NOW() ,pp.expiration_date) AS hours_left
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			INNER JOIN phone_packages pp ON pp.order_package_id = op.id
			WHERE 
				op.package_id IN (19,20,23)
				AND	TIMESTAMPDIFF(HOUR,NOW(), pp.expiration_date) = 5 
				AND op.status = ? 
				AND op.status <> ?
		", array(Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED,Model_Packages::STATUS_ACTIVE, Model_Billing_Packages::STATUS_SUSPENDED));
		
		foreach($packages as $package) {
			//Checking if has pending package. 
			//We must send sms only if they don't have pending package
			
			$has_active_package = $this->_db->fetchOne('
				SELECT TRUE FROM order_packages WHERE escort_id = ? AND status = ?
			', array($package->escort_id, Model_Billing_Packages::STATUS_PENDING));

			if ( ! $has_active_package ) {
				/* send sms */
				$message = Cubix_I18n::translateByLng('de', "sms_package_expiration", array('HOUR'=> $package->hours_left ));

				$model_sms = new Model_SMS_Outbox();
				$data = array(
					'escort_id' => $package->escort_id,
					'phone_number' => $package->contact_phone_parsed,
					'phone_from' => $originator,
					'text' => $message,
					'application_id' => Cubix_Application::getId(),
					'is_spam' => false
				);
				
				$msg_id = $model_sms->save($data);
				
				$sms->setRecipient($package->contact_phone_parsed, $msg_id);
				$sms->setContent($message);
				$sms->sendSMS();
			}
		}
		die('done');
	}
	
	
	//Sending email to katie every day for unused transfers
	public function notUsedEpgTransfersAction()
	{
		$epg_payment = new Model_Billing_EpgGateway();
		
		$result = $epg_payment->getTransactions(date('Y-m-d', strtotime('-7 days')), date('Y-m-d', strtotime('-6 days')));
		$result = str_replace("Success\n", '', $result['ResultMessage']);
		$csvData = $result;
		$lines = explode("\n", $csvData);
		unset($lines[0]);
		$array = array();
		foreach ($lines as $line) {
			$array[] = str_getcsv($line);
		}
		$transactions = $array;
		
		$m_transfers = new Model_Billing_Transfers();
		$not_exists = array();
		
		
		ob_start();
		$df = fopen("php://output", 'w');
		
		foreach($transactions as $transaction) {
			$transaction_id = $transaction[5];
			$not_exist = $m_transfers->checkTransactionId($transaction_id);
			if ( $not_exist ) {
				$not_exists[] = $transaction_id;
				fputcsv($df, array($transaction_id));
			}
			
		}
		fclose($df);
		$csv = ob_get_clean();
		
		
		if ( count($not_exists) ) {
			$attachment = array(
				'item' => $csv,
				'type'	=> 'text/csv',
				'name'	=> 'not_used_epg_transfers.csv'
			);
			$body = 'From ' . date('Y-m-d', strtotime('-7 days')) . ' to ' . date('Y-m-d', strtotime('-6 days'));
			Cubix_Email::send('sharp17s@hushmail.com', 'Not used transfers for last 7 days', $body, false, null, null, array($attachment));
		}
		die('DONE');
	}
    
    public function generateSitemapAction()
    {
        set_time_limit(0);

        $fileHandle = fopen('php://stdout', 'w');

        fwrite($fileHandle, '<?xml version="1.0"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">');

        $system_config = Zend_Registry::get('system_config');
        $hostname = rtrim($system_config['www_url'], '/');

        $enLocale = 'en-ca';
        $frLocale = 'fr-ca';

        // home page url's
        fwrite($fileHandle, '<url><loc>' . $hostname . '</loc>'
            . '<xhtml:link rel="alternate" hreflang="x-default" href="' . $hostname . '"/>'
            . '<xhtml:link rel="alternate" hreflang="' . $enLocale . '" href="' . $hostname . '"/>'
            . '<xhtml:link rel="alternate" hreflang="' . $frLocale . '" href="' . $hostname . '/' . $frLocale . '/"/>'
            . '</url>');

        // categories & cities URLs
        $categoriesAndCities = $this->_db->fetchAll("
            SELECT category, city
            FROM ads
            WHERE status = 'active' AND city <> ''
            GROUP BY category, city 
        ");
        $categories = array();
        foreach ($categoriesAndCities as $categoryAndCity) {

            $category = $categoryAndCity->category;
            $city = $this->sitemapSafeForUrl($categoryAndCity->city);

            // category URL
            if (!isset($categories[$category])) {
                $categories[$category] = $category;
                $url = 'ad/listing/category/' . $category;
                fwrite($fileHandle, '<url><loc>' . $hostname . '/' . $url . '</loc>'
                    . '<xhtml:link rel="alternate" hreflang="' . $enLocale . '" href="' . $hostname . '/' . $url . '"/>'
                    . '<xhtml:link rel="alternate" hreflang="' . $frLocale . '" href="' . $hostname . '/' . $frLocale . '/' . $url . '"/>'
                    . '</url>');
            }

            // category & city URL
            $url = 'ad/listing/category/' . $category . '/city/' . $city;
            fwrite($fileHandle, '<url><loc>' . $hostname . '/' . $url . '</loc>'
                . '<xhtml:link rel="alternate" hreflang="' . $enLocale . '" href="' . $hostname . '/' . $url . '"/>'
                . '<xhtml:link rel="alternate" hreflang="' . $frLocale . '" href="' . $hostname . '/' . $frLocale . '/' . $url . '"/>'
                . '</url>');
        }

        // ads URLs
        $page = 1;
        while (true) {
            // read the ads list in pages of #1000
            $paginate = (1000 * ($page - 1)) . "," . 1000;
            $adsList = $this->_db->fetchAll("
                SELECT id, category, city, title
                FROM ads
                WHERE status = 'active' AND city <> ''
                ORDER BY id ASC
                LIMIT {$paginate}
            ");
            if (!$adsList) {
                // exit loop when there are no more ads
                break;
            }
            foreach ($adsList as $ad) {
                $url = 'ad/details/category/' . $ad->category . '/city/' . $this->sitemapSafeForUrl($ad->city) . '/id/' . $ad->id . '/title/' . $this->sitemapSafeForUrl($ad->title);
                fwrite($fileHandle, '<url><loc>' . $hostname . $url . '</loc>'
                    . '<xhtml:link rel="alternate" hreflang="' . $enLocale . '" href="' . $hostname . '/' . $url . '"/>'
                    . '<xhtml:link rel="alternate" hreflang="' . $frLocale . '" href="' . $hostname . '/' . $frLocale . '/' . $url . '"/>'
                    . '</url>');
            }
            $page++;
        }

        fwrite($fileHandle, '</urlset>');
        fclose($fileHandle);
        exit(0);        
    }

    /**
     * Used for sitemap URLs (must match frontend URL creation).
     *
     * @param $string
     * @return string
     */
    private function sitemapSafeForUrl($string)
    {
        $toBeReplaced = array('À' => 'A', 'à' => 'a', 'Â' => 'A', 'â' => 'a', 'Æ' => 'a', 'æ' => 'a',
            'Ç' => 'C', 'ç' => 'c',
            'É' => 'E', 'é' => 'e', 'È' => 'E', 'è' => 'e', 'Ê' => 'E', 'ê' => 'e', 'Ë' => 'E', 'ë' => 'e',
            'Î' => 'I', 'î' => 'i', 'Ï' => 'I', 'ï' => 'i',
            'Ô' => 'O', 'ô' => 'o', 'Ö' => 'O', 'ö' => 'o',
            'Ù' => 'U', 'ù' => 'u', 'Û' => 'U', 'û' => 'u', 'Ü' => 'U', 'ü' => 'u',
            'ÿ' => 'y');
        $string = str_replace(array_keys($toBeReplaced), array_values($toBeReplaced), $string);
        $string = preg_replace('/[^a-zA-Z0-9]/', '-', $string);
        $string = preg_replace("/-{2,}/", '-', $string);
        return strtolower($string);
    }
    
    
    public function googleanalyticsAction() {
        
        set_time_limit(0);
        include_once("../library/google_analytics.php");   
         $startDate = date('Y-m-d', strtotime("-2 days"));;
//        $startDate = date('Y-m-d', strtotime("-60 days"));
        $endDate = date('Y-m-d', strtotime("-1 days"));
        
        $ga = new GoogleAnalytics();
        $analytics = $ga->initializeAnalytics();
        
        $initial_data =  $ga->getInitialViewsRows($analytics, $startDate, $endDate);

        if($initial_data) {
            
            //$model = new Model_GaViews();
           // $model->insert($initial_data);
            
            $this->_db->insert('ga_views', $initial_data);
        }
        die("FINISHED");
        
    }
    
    public function updatevisitsAction() {
        
        // read fron db
        set_time_limit(0);
        $model_ga = new Model_GaViews();
        $unfinished_row = $model_ga->getUnfinishedProcess();
        
        if($unfinished_row) {
            
            for($i = $unfinished_row->current_page; $i < $unfinished_row->total_pages; $i++) {
                
                // read google analytics and update ads
                $ga = new GoogleAnalytics();
                $analytics = $ga->initializeAnalytics();
                
                $startDate = $unfinished_row->start_date;
                $endDate = $unfinished_row->end_date;
                $start_index = ($i * $unfinished_row->rows_per_page) + 1;
                $allViews =  $ga->getViewCounts($analytics, $startDate, $endDate, $start_index);    // read paginated views count from google analytics api
                
                if(empty($allViews))
                    die('NO VIEWS');
        
                $arrUpdates = [];
                foreach($allViews as $key => $view) {
                    
                     preg_match('/id\/([0-9]+)\//', $view[0], $output_array);  // process result to get ad id
                     $idFromUrl = $output_array[1];
                     
                     if(!isset($arrUpdates[$idFromUrl]))
                        $arrUpdates[$idFromUrl] = 0;   
                     
                     $arrUpdates[$idFromUrl] += $view[1]; // increment values                      
                }
                
                // update ads views count
                $ad = new Model_Advertisements();
                foreach($arrUpdates as $key => $views) {
                    
                    $data = [
                        'no_visits'         => 'no_visits' + $views,
                        'no_visits_updated' => $endDate
                    ];
                    $this->_db->query('UPDATE ads SET no_visits = `no_visits` + ' . $views . ', no_visits_updated = "' . $endDate . '" 
                        WHERE id = ?', array($key));
                }
                
                // update page
                //$model_ga->update($unfinished_row->id, array('current_page' => $i+1));
                $this->_db->update('ga_views', array('current_page' => $i+1), $this->_db->quoteInto('id = ?', $unfinished_row->id));
            }
                    
                    
        } else {            
            echo 'NOTHING TO PROCESS'; 
            die(); 
        }
       
        echo 'FINISHED'; die();
        
    }
}
