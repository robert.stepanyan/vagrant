<?php

class Billing_OnlineBillingController extends Zend_Controller_Action {
    const BITCOIN_MULTIPLIER = 1.25; // 25%
    const BITCOIN_URL = "https://www.coiniverse.ch/apps/"; // 25%
    //const BITCOIN_CHECK_URL = "https://www.coiniverse.ch/check/"; // 25%
    const BITCOIN_CHECK_URL = "https://www.coinfly.ch/check/"; // 25%
    const BITCOIN_TOKEN = "z32wed81779041ebb2210d05dcbzre34"; // 25%
	
    public function init()
	{	
		
	}
	
	public function mmgBillCallbackAction()
	{
		$req = $this->_request;

		//
        $data = date("YmdHis");
        Core_Redis::hset("logget","llog". $data, serialize($_GET));
		Core_Redis::hset("logpost","llog". $data, serialize($_POST));
		$zerr_body = var_export($_POST, true);
        $zerr_body .= "\n\n";
        $zerr_body .= 'From application id HUMP';
        $zerr_body .= "\n\n";
        //$zerr_body .= $e->getMessage();
        //Cubix_Email::send('webmaster@humpchies.com', 'MMG Callback', $zerr_body);
            
		list($method, $ref, $hash) = explode('Z', $req->ti_mer);
        
		try{
			
			$callback = array(
				'transaction_time'	=> date('Y-m-d H:i:s'),
				'transaction_id'	=> $req->ti_mmg,
				'reference'			=> $ref,
				'status_id'			=> '',
				'hash'				=> $hash,
				'currency'			=> $req->cu,
				'amount'			=> $req->amc / 100,
				'first_name'		=> $req->name,
				'last_name'			=> '',
				'zipcode'			=> 'not provided',
				'city'				=> 'not provided',
				'country_code'		=> 'not provided',
				'phone_number'		=> 'not provided',
				'address'			=> '',
				'email'				=> '',
				'sha1'				=> '',
				'log_data'			=> '',
				'ip_address'		=> $req->customer_ip,
				'card_name'			=> $req->card_brand,
				'card_number'		=> $req->ccnumm,
				'exp_date'			=> 'not provided',
				'log_data'          => var_export($_POST, true)
			);
            
            $cardCountry = geo::getCardCountry($req->ccnumm);
            $callback['address'] = $cardCountry[1];
            $cardCountry = $cardCountry[0];

			$onlineBilling = new Model_Billing_OnlineBilling($callback, false);
			$onlineBilling->addCallbackLog();

            if($req->txn_type == 'CHARGEBACK'){
                Cubix_Email::send('webmaster@humpchies.com', 'chargebacklog', var_export($_GET, true) . "<\n>" .var_export($_POST, true));
                $transaction = new Model_Billing_Transfers();
                $originalTransaction = $transaction->getTransfer($req->ti_ori);
                $user = new Model_Users();
                $user->disableAds($originalTransaction->user_id);  // disable all ads 
                $user->chargeBackUser($originalTransaction->user_id);// bann user for chargeback
                $transaction->MarkTransferasChargeback($req->ti_ori);
                
                header("HTTP/1.1 200 OK");
                die('<html><head></head><body>OK</body></html>');
            }
            
			if (  $req->txn_status != 'APPROVED' ) {
				$onlineBilling->updateLogData('Failure status not approved:');
				header("HTTP/1.1 200 OK");
				die('<html><head></head><body>OK</body></html>');
			}
			
			//check if transfer already exists
			if ( $onlineBilling->isTransactionIdExists($callback['transaction_id']) ) {
				$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
				header("HTTP/1.1 200 OK");
				die('<html><head></head><body>OK</body></html>');
			}
            
          
            $autoAuthorize = false;
            if($cardCountry == 'CA'){
                $loc = geo::getLocation($req->customer_ip);
                if(in_array($loc['country'],array("CA", "GB", "US", "FR", "CH", "CO"))){
                    $autoAuthorize = true;
                }
            }
            $sc = $onlineBilling->getShoppingCart();
            
			switch($method) {
					case 'SC': 
                        
						$onlineBilling->pay('mmgbill', $autoAuthorize, intval($sc[0]->package_id));
						break;
					case 'CR':
						$onlineBilling->addCredits('mmgbill');
						break;
				default;
			}
		} catch(Exception $e) {
			$err_body = var_export($_POST, true);
			$err_body .= "\n\n";
			$err_body .= 'From application id HUMP';
			$err_body .= "\n\n";
			$err_body .= $e->getMessage();
            var_dump($err_body);
			
		}	
		header("HTTP/1.1 200 OK");
		die('<html><head></head><body>OK</body></html>');
	}
	
	public function bitcoinCallbackAction()
    {
        
         $req = $this->_request;
        $orderID = intval($_GET['id']);
        
        // UNCOMMENT FOR LIVE
        
        $ch = curl_init();
//        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        // Set the url
        curl_setopt($ch, CURLOPT_URL,self::BITCOIN_CHECK_URL . self::BITCOIN_TOKEN . "/" . $orderID);
//        // Execute
        $raw=curl_exec($ch);
//        // Closing
        curl_close($ch);
        //$raw = file_get_contents('php://input');
        $request = json_decode($raw, true);
        //
        $data = date("YmdHis");
        
      //  var_dump($raw);
//        var_dump($request);
//        die();
        //var_dump($raw);
        //var_dump($request);
        //die("bla");
        //list($method, $ref, $hash) = explode('Z', $req->ti_mer);
        
        try{
            
            $callback = array(
                'transaction_time'    => date('Y-m-d H:i:s'),
                'transaction_id'    => $request['address']. "|". $request['orderId'],
                'payment_processor' => "bitcoin",
                'reference'            => "bitcoin",
                'status_id'            => '',
                'hash'                => $request['orderId'],
                'currency'            => "CAD",
                'amount'            => $request['amountUsd'],
                'bitcoinTransactionStatus' => $request['status'],
                'first_name'        => '',
                'last_name'            => '',
                'zipcode'            => 'not provided',
                'city'                => 'not provided',
                'country_code'        => 'not provided',
                'phone_number'        => 'not provided',
                'address'            => '',
                'email'                => '',
                'sha1'                => '',
                'log_data'            => '',
                'ip_address'        => '',
                'card_name'            => '',
                'card_number'        => '',
                'exp_date'            => 'not provided',
                'log_data'          => $raw
            );
            
            $onlineBilling = new Model_Billing_OnlineBilling($callback, false);
            $onlineBilling->addCallbackLog();
            //die("log written");
            if (  $request['status'] != 1 ) {
                $onlineBilling->updateLogData('Failure status not approved:');
                header("HTTP/1.1 200 OK");
                die('<html><head></head><body>OK</body></html>');
            }
            
            //check if transfer already exists
            if ( $onlineBilling->isTransactionIdExists($callback['transaction_id']) ) {
                $onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
                header("HTTP/1.1 200 OK");
                die('<html><head></head><body>OK</body></html>');
            }
            $autoAuthorize = true;
            $sc = $onlineBilling->getShoppingCart();
            //var_dump($sc[0]);
            //die();
            if(intval($sc[0]->package_id) > 0){  
                //die("pay");
               $onlineBilling->pay('bitcoin', $autoAuthorize, intval($sc[0]->package_id)); 
            }else{
               // die("credits");
               $onlineBilling->addCredits('bitcoin'); 
            }
            
        } catch(Exception $e) {
            $err_body = var_export($_POST, true);
            $err_body .= "\n\n";
            $err_body .= 'From application id HUMP';
            $err_body .= "\n\n";
            $err_body .= $e->getMessage();
            Cubix_Email::send('webmaster@humpchies.com', 'Bitcoin Issue', $err_body);
        }    
        header("HTTP/1.1 200 OK");
        die('<html><head></head><body>OK</body></html>');
    }
    public function ethCallbackAction()
    {
        
         $req = $this->_request;
        $orderID = intval($_GET['id']);
         //die("----" . $orderID);
        // UNCOMMENT FOR LIVE
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,self::BITCOIN_CHECK_URL . 'eth/' . self::BITCOIN_TOKEN . "/" . $orderID);
        $raw=curl_exec($ch);
        curl_close($ch);
       // echo self::BITCOIN_CHECK_URL . 'eth/' . self::BITCOIN_TOKEN . "/" . $orderID;
        //$raw = file_get_contents('php://input');
        //if($_GET['force']== 'yeap'){
//            $raw = '{"orderId":29126,"address":"0x3ccc06ca1ec515b73cf17f4b37f8790599ff5465","amountEth":0.0117734,"created_at":"2021-04-09 23:43:57","paid_at":"2021-04-09 23:51:11","total_received":0.0106682,"status":1,"amountUsd":32,"ethValue":2718,"payment_uncomplete":0}';
//        }
        $request = json_decode($raw, true);
        //
        $data = date("YmdHis");
        //$request = json_decode($raw, true);
        
        //var_dump($raw);
        
        //var_dump($request);
       // die();
        //var_dump($raw);
        //var_dump($request);
        //die("bla");
        //list($method, $ref, $hash) = explode('Z', $req->ti_mer);
        
        try{
            
            $callback = array(
                'transaction_time'    => date('Y-m-d H:i:s'),
                'transaction_id'    => $request['address']. "|". $request['orderId'],
                'payment_processor' => "eth",
                'reference'            => "eth",
                'status_id'            => '',
                'hash'                => $request['orderId'],
                'currency'            => "CAD",
                'amount'            => $request['amountUsd'],
                'bitcoinTransactionStatus' => $request['status'],
                'first_name'        => '',
                'last_name'            => '',
                'zipcode'            => 'not provided',
                'city'                => 'not provided',
                'country_code'        => 'not provided',
                'phone_number'        => 'not provided',
                'address'            => '',
                'email'                => '',
                'sha1'                => '',
                'log_data'            => '',
                'ip_address'        => '',
                'card_name'            => '',
                'card_number'        => '',
                'exp_date'            => 'not provided',
                'log_data'          => $raw
            );
            
            $onlineBilling = new Model_Billing_OnlineBilling($callback, false);
            $onlineBilling->addCallbackLog();
            //die("log written");
            if (  $request['status'] != 1 ) {
                $onlineBilling->updateLogData('Failure status not approved:');
                header("HTTP/1.1 200 OK");
                die('<html><head></head><body>OK</body></html>');
            }
            
            //check if transfer already exists
            if ( $onlineBilling->isTransactionIdExists($callback['transaction_id']) ) {
                $onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
                header("HTTP/1.1 200 OK");
                die('<html><head></head><body>OK</body></html>');
            }
            $autoAuthorize = true;
            $sc = $onlineBilling->getShoppingCart();
            //var_dump($sc[0]);
//            die();
            if(intval($sc[0]->package_id) > 0){  
                //die("pay");
               $onlineBilling->pay('eth', $autoAuthorize, intval($sc[0]->package_id)); 
            }else{
               // die("credits");
               $onlineBilling->addCredits('eth'); 
            }
            
        } catch(Exception $e) {
            $err_body = var_export($_POST, true);
            $err_body .= "\n\n";
            $err_body .= 'From application id HUMP';
            $err_body .= "\n\n";
            $err_body .= $e->getMessage();
            Cubix_Email::send('webmaster@humpchies.com', 'Bitcoin Issue', $err_body);
        }    
        header("HTTP/1.1 200 OK");
        die('<html><head></head><body>OK</body></html>');
    }
	
    public function testMmgAction()
	{
		$method = 'SC';
		try{
			
			$callback = array(
				'transaction_time'	=> date('Y-m-d H:i:s'),
				'transaction_id'	=> 'xxxxxx-'.rand(1,10000),
				'reference'			=> '63661',
				'hash'				=> 'zzzzzz',
				'currency'			=> 'CAD',
				'amount'			=>  51,
				'first_name'		=> 'test',
				'last_name'			=> 'test',
				'zipcode'			=> 'not provided',
				'city'				=> 'not provided',
				'country_code'		=> 'not provided',
				'phone_number'		=> 'not provided',
				'ip_address'		=> '8.8.8.8',
				'card_name'			=> '',
				'card_number'		=> '',
				'email'				=> '',
				'exp_date'			=> 'not provided'
			);
			
			$onlineBilling = new Model_Billing_OnlineBilling($callback, false);
			$onlineBilling->addCallbackLog();
			
			//check if transfer already exists
			if ( $onlineBilling->isTransactionIdExists($callback['transaction_id']) ) {
				$onlineBilling->updateLogData('Failure. transaction_id:' . $callback['transaction_id'] . ' already exists.');
				header("HTTP/1.1 200 OK");
				die('<html><head></head><body>OK</body></html>');
			}
			
			switch($method) {
				case 'SC': 
					$onlineBilling->pay('mmgbill');
					break;
				case 'CR':
					$onlineBilling->addCredits('mmgbill');
					break;
				default;
			}
		} catch(Exception $e) {
			$err_body = var_export($_POST, true);
			$err_body .= "\n\n";
			$err_body .= 'From application id HUMP';
			$err_body .= "\n\n";
			$err_body .= $e->getMessage();
			var_dump($err_body);die;
		}	
		header("HTTP/1.1 200 OK");
		die('<html><head></head><body>OK</body></html>');
	}
    
    public function testlogAction(){
         $cardCountry = geo::getCardCountry("551029XXXXXXXX0472");
         $ip = "142.169.78.199";   
         $autoAuthorize = false;
         if($cardCountry == 'CA'){
            $loc = geo::getLocation($ip);
            if(in_array($loc['country'],array("CA", "GB", "US", "FR", "CH", "CO"))){
                            $autoAuthorize = true;
                            die("authorized");
            }
         }
         die("not authorized");
    }
}
