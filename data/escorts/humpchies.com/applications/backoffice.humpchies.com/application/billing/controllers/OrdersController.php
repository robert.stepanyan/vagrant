<?php

class Billing_OrdersController extends Zend_Controller_Action
{
	/**
	 * @var Model_Billing_Orders
	 */
	protected $_model;

	public function init()
	{
		$this->_model = new Model_Billing_Orders();
	}
	
	public function indexAction()
	{
		/* FILTER PARAMS */
		$ses_filter = new Zend_Session_Namespace('billing_orders_data');
		$this->view->filter_params = $ses_filter->filter_params;
		/* FILTER PARAMS */
		
//		$this->view->orders_data = number_format($this->_model->getTotalOrdersPaid(), 2);
	}

	public function dataAction()
	{
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);
		$data->setFields(array(
			'sales_user_id' => 'int',
			'sales_user_id_real' => 'int',
			'status' => '',
			'filter_by' => '',
			'date_from' => 'int',
			'date_to' => 'int',
			//'transfer_type_id' => 'int',
			
			/*'discounted_orders' => '',
			'gotd_booking' => '',*/
			
			'user_id' => '',
			'ads_id' => 'int',
			'email' => '',
			'order_id' => ''
		));
		
		$filter = $data->getData();

		//var_dump($filter);die;
		
		$orders = $this->_model->getAllV2(
			$filter,
			$req->page,
			$req->per_page,
			$req->sort_field,
			$req->sort_dir
		);

		die(json_encode($orders));
	}
	
	/*public function dataCountAction()
	{
		$this->view->layout()->disableLayout();
		
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);
		$data->setFields(array(
			'sales_user_id' => 'int',
			'sales_user_id_real' => 'int',
			'status' => '',
			'filter_by' => '',
			'date_from' => 'int',
			'date_to' => 'int',
			'transfer_type_id' => 'int',
			
			'discounted_orders' => '',
			'gotd_booking' => '',
			
			'user_id' => '',


			'showname' => '',
			'escort_id' => 'int',
			'email' => '',
			'agency_name' => '',
			'order_id' => ''
		));
		$data = $data->getData();

		$bu_user = Zend_Auth::getInstance()->getIdentity();

		$filter = array();

		$filter['o.application_id = ?'] = (int) $this->_request->application_id;

		if ( $data['email'] ) {
			$filter['u.email LIKE ?'] = $data['email'] . '%';
		}

		if ( $data['showname'] ) {
			$filter['e.showname LIKE ?'] = $data['showname'] . '%';
		}
		
		if ( $data['escort_id'] ) {
			$filter['e.id = ?'] = $data['escort_id'];
		}

		if ( $data['agency_name'] ) {
			$filter['a.name LIKE ?'] = $data['agency_name'] . '%';
		}

		if ( $data['order_id'] ) {
			$filter['o.system_order_id = ?'] = $data['order_id'];
		}
		
		if ( $data['discounted_orders'] ) {
			$filter['(op.discount <> 0 OR op.discount_fixed <> 0)'] = true;
		}
		
		if ( $data['gotd_booking'] ) {
			$filter['gotd_booking'] = true;
		}

		if ( $data['transfer_type_id'] ) {
			$filter['t.transfer_type_id = ?'] = $data['transfer_type_id'];
		}

		if ( $bu_user->type == 'superadmin' || $bu_user->type == 'admin' ) {
			if ( $data['sales_user_id'] ) {
				$filter['o.backend_user_id = ?'] = $data['sales_user_id'];
			}
		}
		else {
			if ( $bu_user->type == 'sales manager' || $bu_user->type == 'sales clerk') {
				$filter['o.backend_user_id = ?'] = $bu_user->id;
				//$filter['o.backend_user_id = ?'] = $data['sales_user_id'];
			}
			
		}
		
		if ( $data['sales_user_id_real'] ) {
			if ( $bu_user->type == 'superadmin' ) {
				$filter['u.sales_user_id = ?'] = $data['sales_user_id_real'];
			}
			else if ($bu_user->type == 'admin') {
				//$filter['bu2.application_id = ?'] = $bu_user->application_id;
				$filter['u.sales_user_id = ?'] = $data['sales_user_id_real'];
			}
		}

		if ( $data['status'] ) {
			if ( ! is_array($data['status']) ) {
				$filter['o.status = ?'] = $data['status'];
			}
			else {
				$filter['o.status IN (' . implode(', ', $data['status']) . ')'] = true;
			}
		}

		if ( $data['filter_by'] ) {
			if ( in_array($data['filter_by'], array('order_date', 'payment_date')) ) {
				if ( $data['date_from'] ) {
					$filter['DATE(' . $data['filter_by'] . ') >= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_from'] . '))');
				}

				if ( $data['date_to'] ) {
					$filter['DATE(' . $data['filter_by'] . ') <= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_to'] . '))');
				}
			}
		}
		
		if ( ! is_null($this->_getParam('user_id')) ) {
			$filter['u.id = ?'] = $data['user_id'];
		}
		
		$s = implode('_', $filter);
		$button_type = $req->type;
		$cache_key = 'orders_' . preg_replace('#[^a-zA-Z0-9_]#', '_', $s) . $button_type;
		$cache = Zend_Registry::get('cache');
		
		if ( ! $data = $cache->load($cache_key) )
		{
			if(isset($req->test)){
				echo 'old query';
				$data = $this->_model->getAllCount(
					$filter,
					1,
					10,
					'id',
					'asc'
				);
			}
			else{
				$data = $this->_model->getAllCountV2($filter, $button_type);
			}
						
			$cache->save($data, $cache_key, array(), 3600); //1 hour
		}

		$this->view->data = $data;
		$this->view->type = $button_type;
	}*/

	public function detailsAction()
	{
		$this->view->layout()->disableLayout();

		$id = intval($this->_getParam('id'));

		$order = $this->_model->getDetails($id);
		
		$this->view->order = $order;
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		$bu_model = new Model_BOUsers();
		
		if ( $bu_user->type == 'admin' || $bu_user->type == 'superadmin' ) {
			$this->view->sales_persons = $bu_model->getAllBoUsers();
		}
		else {
			$this->view->sales_persons = $bu_model->getAllSales();
		}
		
		if ( $this->_request->isPost() ) {
			$sales_person = intval($this->_getParam('sales_person'));

			if ( count($order->packages) && $sales_person != $order->sales_id ) {
				foreach($order->packages as $escort) {
					Cubix_SyncNotifier::notify($escort->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_ORDER_TRANSFER, array('order_id' => $order->system_order_id, 'before_sales_id' => $order->sales_id, 'after_sales_id' => $sales_person));
				}
			}
			
			$this->_model->updateSalesPerson($id, $sales_person);
		}
	}

    public function viewAction(){
        $this->view->layout()->disableLayout();

        $id = intval($this->_getParam('id'));
        $this->view->for_email = intval($this->_getParam('for_email'));


        $order = $this->_model->getDetails($id);
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		/*if (!in_array($bu_user->type, array('superadmin', 'admin')))
		{
			if ($order->sales_id != $bu_user->id)
			{
				echo 'Permission denied';
				die;
			}
		}*/
		
        $model = new Model_Users();
		$user = $model->get($order->user_id);
        $this->view->user =  $user;
		$this->view->order = $order;
    }

	public function editAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();

		$not_sales = $this->view->not_sales = false;
		if ( 'data entry' == $bu_user->type || 'data entry plus' == $bu_user->type ) {
			return $this->_forward('permission-denied', 'error', 'default');
		}
		elseif ($bu_user->type != 'sales manager' && $bu_user->type != 'sales clerk') {
			$not_sales = $this->view->not_sales = true;
		}

		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$ses_packages = new Zend_Session_Namespace('order_packages');

		$this->view->order_id = $order_id = intval($req->order_id);
		$m_order = new Model_Billing_Orders();
		$order = $m_order->get($order_id);
		if($order->status == Model_Billing_Orders::STATUS_PAID){
			return $this->_forward('permission-denied', 'error', 'default');
		}
		
		if ( ! $req->isPost() )
		{			
			unset($ses_packages->packages);
			$this->view->order = $order;

			$m_ads = new Model_Advertisements();
						
			$order = new Model_Billing_OrderItem($order);
			$order_packages = $order->getPackages();
			
			if ( count($order_packages) > 0 )
			{
				$m_packages = new Model_Packages();

				foreach($order_packages as $package)
				{
					$adv_data = $m_ads->get($package->ads_id);
					$this->view->adv_id = $package->ads_id;
			
					$package_data = $m_packages->getWithPrice($adv_data->category, $package->package_id);
					foreach($package_data as $k => $p_data)
					{
						$package_data[$k]->setPackageStatus($package->status);
					}
					
					$p_data = new Model_Billing_PackageItem($package);
					$p_data->setOrderPackageId($package->id);

					/*$package_products = $p_data->getProducts(Model_Products::TYPE_OPTIONAL_PRODUCT);
					
					$opt_products = array();
					if ( count($package_products) > 0 )
					{
						$m_products = new Model_Products();

						$opt_products_data = array();
						foreach($package_products as $opt_product) {
							$opt_products_data[] = $m_products->getWithPrice($package->application_id, $escort_data->agency_id, $escort_data->gender, $opt_product->product_id);
							$opt_products[] = $opt_product->product_id;
						}
					}*/

					$package = new Model_Billing_PackageItem($package);
					//$spot_cities = $package->getPremiumSpotCities();
					//$spot_tours = $package->getPremiumSpotTours();

					/*$premium_city_spots = array();
					if ( count($spot_cities ) ) {
						foreach($spot_cities as $city) {
							$premium_city_spots[] = $city->city_id;
						}
					}*/
					
					
					$add_areas_ids = array();
										
					
					
					$data = array(
						'adv_id' => $package->ads_id,
						'adv_data' => $adv_data,
						'activate_promo_package' => $package->activation_type,
						'activation_date' => strtotime($package->activation_date),
						'de_days' => $package->de_days,
						'package' => $package->package_id,
						'package_data' => $package_data,
						'period' => $package->period,
						'discount' => $package->discount,
						'fix_discount' => $package->discount_fixed,
						'surcharge' => $package->surcharge,
						//'opt_products' => $package_data->opt_products,
						//'opt_products' => $opt_products,
						//'opt_products_data' => $opt_products_data,
						'comment' => $package->discount_comment,
						'price' => $package->price,
						'city' => $package->city,
						'category' => $package->category,
						//'premium_city_spots' => $premium_city_spots,
						//'premium_tour_spots' => $premium_tour_spots,
						'add_areas' => $add_areas_ids,
					);

					$ses_packages->packages[$package->ads_id] = $data;
					//print_r($ses_packages->packages); die;
				}
			}
		}
		else
		{

			$validator = new Cubix_Validator();

			if ( count($ses_packages->packages) == 0 ) {
				$validator->setError('err_no_package', 'Please add at least one package to create an order');
			}

			if ( $not_sales && ! $req->sales_user_id ) {
				$validator->setError('sales_user_id', 'Required');
			}

			if ( $bu_user->type == 'sales' || $bu_user->type == 'sales clerk' ) {
				$req->setParam('activate_condition', Model_Billing_Orders::CONDITION_AFTER_PAID);
			}
			
			if ( $validator->isValid() ) {

				$package_price = 0;
				foreach( $ses_packages->packages as $package ) {
					$price_data = array(
						'package_price' => $package['package_data'][0]->price,
						'default_period' => $package['package_data'][0]->period,
						'period' => $package['period'],
						'discount' => $package['discount'],
						'fix_discount' => $package['fix_discount'],
						'surcharge' => $package['surcharge'],
						'opt_product_prices' => array(),
						'de_days' => $package['de_days'],
						'add_areas' => $package['add_areas']
					);

					if ( count($package['opt_products_data']) > 0 ) {
						foreach ($package['opt_products_data'] as $opt_prod_data) {
							$price_data['opt_product_prices'][] = array('price' => $opt_prod_data->price, 'id' => $opt_prod_data->id);
						}
					}

					$package_price += $this->_calculatePrice($price_data);
				}

				$data = array(
					'order_data' => array(
						//'user_id' => $ses_packages->order_user_id,
						'backend_user_id' => $not_sales ? $req->sales_user_id : Zend_Auth::getInstance()->getIdentity()->id,
						//'status' => Model_Billing_Orders::STATUS_PENDING,
						'activation_condition' => $req->activate_condition,
						//'order_date' => new Zend_Db_Expr('NOW()'),
						//'system_order_id' => $order_id,
						'price' => $package_price,
						'price_package' => $package_price,
						'use_balance' => ( ! $req->use_balance ) ? 0 : $req->use_balance
					),
					'additional_data' => $ses_packages->packages
				);
				
				$this->_model->edit($data, $order_id);
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function createAction()
	{
		$bu_user = Zend_Auth::getInstance()->getIdentity();

		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$this->view->order_id = $this->_getNewOrderId();

		if ( $req->category ) {
			$this->view->type = $req->type;
			$this->view->id = $req->id;
			$this->view->name = $req->name;
			
		}

		if ( $this->_request->isPost() ) {
			$order_id = $req->order_id;

			$ses_packages = new Zend_Session_Namespace('order_packages');

			$validator = new Cubix_Validator();
			
			if ( count($ses_packages->packages) == 0 ) {
				$validator->setError('err_no_package', 'Please add at least one package to create order');
			}

			if ( $not_sales && ! $req->sales_user_id ) {
				$validator->setError('sales_user_id', 'Required');
			}
			
			
			if ( $bu_user->type == 'sales' || $bu_user->type == 'sales clerk' ) {
				$req->setParam('activate_condition', Model_Billing_Orders::CONDITION_AFTER_PAID);
			}
			
			if ( $validator->isValid() ) {
				
				$package_price = 0;
				foreach( $ses_packages->packages as $package ) {
					//print_r($package);
					$price_data = array(
						'package_price' => $package['package_data'][0]->price,
						'default_period' => $package['package_data'][0]->period,
						'period' => $package['period'],
						'discount' => $package['discount'],
						'fix_discount' => $package['fix_discount'],
						'surcharge' => $package['surcharge'],
						'opt_product_prices' => array(),
						'de_days' => $package['de_days'],
						'add_areas' => $package['add_areas'],
					);
					

					if ( count($package['opt_products_data']) > 0 ) {
						foreach ($package['opt_products_data'] as $opt_prod_data) {
							$price_data['opt_product_prices'][] = array('price' => $opt_prod_data->price, 'id' => $opt_prod_data->id);
						}
					}
					
					$package_price += $this->_calculatePrice($price_data);
					
				}
				
				$data = array(
					'order_data' => array(
						'user_id' => $ses_packages->order_user_id,
						'backend_user_id' => $not_sales ? $req->sales_user_id : Zend_Auth::getInstance()->getIdentity()->id,
						'status' => Model_Billing_Orders::STATUS_PENDING,
						'activation_condition' => $req->activate_condition,
						'order_date' => new Zend_Db_Expr('NOW()'),
						'system_order_id' => $order_id,
						'price' => $package_price,
						'price_package' => $package_price,
					),
					'additional_data' => $ses_packages->packages
				);

				$new_order_id = $this->_model->save($data);
			}
			
			die(json_encode($validator->getStatus() + ( isset($new_order_id) ? array('order_id' => $new_order_id) : array() ) ));
		}
		else {
			$this->clearSessionAction();
		}
	}

	protected function _calculatePrice($price_data)
	{
		$price = $price_data['package_price'];
		$default_period = $price_data['default_period'];
		$period = $price_data['period'];
		$discount = $price_data['discount'];
		$fix_discount = $price_data['fix_discount'];
		$surcharge = $price_data['surcharge'];
		$opt_products_price = 0;

		$de_days = $price_data['de_days'];
		$add_areas_count = count($price_data['add_areas']);
		
		$add_area_price = 0;
		$dex_price = 0;
		if ( count($price_data['opt_product_prices']) > 0 ) {
			foreach ($price_data['opt_product_prices'] as $opt_prod_price) {
				
				if ( $opt_prod_price['id'] == Model_Products::ADDITIONAL_AREA ) {
					$add_area_price = $opt_prod_price['price'];
				}
				
				if ( $opt_prod_price['id'] == Model_Products::DAY_EXTEND ) {
					$dex_price = $opt_prod_price['price'] * $de_days;
				}
				else {
					$opt_products_price += (float) $opt_prod_price['price'] / $default_period;
				}
			}

			$opt_products_price = ceil((float) $opt_products_price * $period);
		}

		$opt_products_price = $opt_products_price + $dex_price;
		
		$total_price = 0;

		if ( $default_period > 0 ) {
			$day_price = ceil($price / $default_period);
		}

		//$parts = $period / $default_period;
		$total_price = ceil(($price * ($period / $default_period) + $opt_products_price));

		if ( $period == $default_period ) {
			$total_price = $price + $opt_products_price;
		}

		// discount
		if ( $discount > 0 ) {
			$total_price -= $total_price * ($discount / 100);
		}

		//fixed_discount
		if ( $fix_discount > 0 ) {
			$total_price -= $fix_discount;
		}
		//surcharge
		if ( $surcharge > 0 ) {
			$total_price += $surcharge;
		}
		
		if ( $add_areas_count > 0 ) {
			$add_areas_count--;
			$total_price = $total_price + ($add_area_price * $add_areas_count);
		}

		return round($total_price, 2);
	}

	protected function _generateOrderId()
	{
		$order_id = md5(microtime() * microtime() + microtime());
		$order_id = substr(strtoupper($order_id), 0, 8);

		return $order_id;
	}

	protected function _getNewOrderId()
	{
		$m_orders = new Model_Billing_Orders();
		
		$order_id = $this->_generateOrderId();
		while( $m_orders->isOrderIdExist($order_id) ) {
			$order_id = $this->_generateOrderId();
		}

		return $order_id;
	}
	
	
	/*public function deleteAction()
	{
		$id = intval($this->_request->id);
		if ( ! $id ) die;
		$this->_model->delete($id);

		die;
	}*/

	public function orderPackagesAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$ses_packages = new Zend_Session_Namespace('order_packages');
		$this->view->order_packages = $ses_packages->packages;
		//print_r($ses_packages->packages);
	}

	public function removeOrderPackageAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$adv_id = $req->adv_id;

		$ses_packages = new Zend_Session_Namespace('order_packages');

		unset($ses_packages->packages[$adv_id]);

		die;
	}

	public function editPackageAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$adv_id = $req->adv_id;
		$this->view->mode = $mode = $req->mode;
		

		$ses_packages = new Zend_Session_Namespace('order_packages');

		$m_packages = new Model_Packages();

		$m_adv = new Model_Advertisements();
		$this->view->advertisment = $m_adv->get($adv_id);

		$this->view->package_data = $package_data = $ses_packages->packages[$adv_id];

		$this->view->packages = $m_packages->getWithPrice($package_data['adv_data']->category);

		//$this->view->opt_products = $m_packages->getOptProducts($package_data['package'], $package_data['app_id'], $package_data['escort_data']->agency_id, $package_data['escort_data']->gender, $this->view->escort->pseudo_escort, true, $package_data['escort_data']->profile_type);
		//$this->view->products = $m_packages->getProducts($package_data['package'], $package_data['app_id'], $package_data['escort_data']->agency_id, $package_data['escort_data']->gender, $this->view->escort->pseudo_escort, $package_data['escort_data']->profile_type);
		
		
		if ( $req->isPost() )
		{
			$validator = new Cubix_Validator();

			$m_packages = new Model_Packages();

			if ( ! strlen($req->package) ) {
				$validator->setError('package', 'Required');
			}
			else {
				$package = $m_packages->get($req->package);
				$package = new Model_PackageItem($package);
				//$products = $package->getPackageProducts();
			}
			
			/*if ( count($products) ) {
				foreach ( $products as $product ) {
					if ( $product['id'] == Model_Products::CITY_PREMIUM_SPOT && count($req->premium_city_spots) == 0 ) {
						$validator->setError('city_spot_error', 'City Required');
					}

					
				}
			}*/

			$premium_city_spots = $req->premium_city_spots;
			//$premium_tour_spots = $req->premium_tour_spots;
			$opt_products = $req->opt_products;

			/*if ( count($opt_products) ) {
				foreach($opt_products as $opt_product) {
										
					if ( $opt_product == Model_Products::CITY_PREMIUM_SPOT && count($premium_city_spots) == 0 ) {
						$validator->setError('city_spot_error', 'City Required');
					}
				}
			}*/

			if ( ! strlen($req->period) ) {
				$validator->setError('period', 'Required');
			}

			$m_adv = new Model_Advertisements();
			$adv_data = $m_adv->get($req->adv_id);
			
			if ( $mode != 'order_edit' )
			{
				if ( $m_adv->hasPendingPackage($req->adv_id) ) {
					$validator->setError('has_pending_package', 'Escort already has pending package');
				}
			}

			if ( $validator->isValid() )
			{
				$adv_id = $req->adv_id;

				unset($ses_packages->packages[$adv_id]);

				$ses_packages->order_user_id = $adv_data->user_id;
				$package_data = $m_packages->getWithPrice($req->category, $req->package);
				
				/*if ( count($req->opt_products) > 0 )
				{
					$m_products = new Model_Products();

					$opt_products_data = array();
					foreach($req->opt_products as $opt_product) {
						$opt_pr = $m_products->getWithPrice($req->app_id, $escort_data->agency_id, $escort_data->gender, $opt_product, $escort_data->pseudo_escort, false, $escort_data->profile_type);
						
						$opt_products_data[] = $opt_pr;
					}
				}*/
				
				$data = array(
					'adv_id' => $req->adv_id,
					'adv_data' => $adv_data,
					'activate_promo_package' => $req->activate_promo_package,
					'activation_date' => $req->activation_date,
					'de_days' => $req->de_days,
					
					//'activation_date' => date("Y-m-d", $req->activation_date),
					'package' => $req->package,
					'package_data' => $package_data,
					'period' => $req->period,
					'discount' => $req->discount,
					'fix_discount' => $req->fix_discount,
					'surcharge' => $req->surcharge,
					'city' => $req->city,
					'category' => strlen($req->category) > 0 ? $req->category : NULL,
					'opt_products' => $req->opt_products,
					'opt_products_data' => $opt_products_data,
					'comment' => $req->comment,
					'price' => $req->h_package_price
				);
//print_r($data); die;
				$ses_packages->packages[$adv_id] = $data;
				//unset($ses_packages->packages);die;
				//$this->_model->save(new Model_Newsletter_TemplateItem($data));*/
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function checkOverlapAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$mode = $req->mode;

		$adv_id = intval($req->adv_id);

		$date = intval($req->date);
		$m_adv = new Model_Advertisements();
		$adv_data = $m_adv->get($adv_id);

		if ( $mode != 'order_edit' && strlen($adv_id) ) {
			if ( $active_packages = $m_adv->getActivePackages($adv_id) ) {
				$start_date = $active_packages[0]->date_activated;
				$end_date = $active_packages[0]->expiration_date;
				if ($date > $start_date && $date < $end_date) {
					die(json_encode(array('status' => 'error')));
				}
			}
		}
		die(json_encode(array('status' => 'success')));
	}

	public function addPackageAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$this->view->mode = $mode = $req->mode;

		$adv_id = intval($req->adv_id);

		if ( $adv_id ) {
			$m_packages = new Model_Packages();
			$m_ads = new Model_Advertisements();

			$this->view->adv = $adv = $m_ads->get($adv_id);
			$this->view->packages = $m_packages->getWithPrice($adv->category);
			
		}
		

		if ( $req->isPost() )
		{
			$validator = new Cubix_Validator();

			$ses_packages = new Zend_Session_Namespace('order_packages');

			if ( ! is_array($ses_packages->packages) ) {
				$ses_packages->packages = array();
			}

			$m_packages = new Model_Packages();
			
			if ( ! strlen($req->package) ) {
				$validator->setError('package', 'Required');
			}
			else {
				$package = $m_packages->get($req->package);
				$package = new Model_PackageItem($package);
				//$products = $package->getPackageProducts();
			}

			/*if ( count($products) ) {
				foreach ( $products as $product ) {
					if ( $product['id'] == Model_Products::CITY_PREMIUM_SPOT && count($req->premium_city_spots) == 0 ) {
						$validator->setError('city_spot_error', 'City Required');
					}
					
					
				}
			}*/
			$city = $req->city;
			$category = strlen($req->category) > 0 ? $req->category : NULL;
			
			$premium_city_spots = $req->premium_city_spots;
			$opt_products = $req->opt_products;

			
			if ( $req->activate_promo_package == Model_Packages::ACTIVATE_AT_EXACT_DATE && ! strlen($req->activation_date) ) {
				$validator->setError('activation_date', 'Required');
			}
			
			$m_adv = new Model_Advertisements();
			$adv_data = $m_adv->get($adv_id);

			if ( $mode != 'order_edit' && strlen($adv_id) ) {
				if ( $m_adv->hasPendingPackage($adv_id) ) {
					$validator->setError('has_pending_package', 'Escort already has pending package');
				}
			}

			if ( $validator->isValid() ) {
				$ses_packages->order_user_id = $adv_data->user_id;
				$package_data = $m_packages->getWithPrice($req->category, $req->package);

				/*if ( count($req->opt_products) > 0 )
				{
					$m_products = new Model_Products();

					$opt_products_data = array();
					foreach($req->opt_products as $opt_product) {
						$opt_pr = $m_products->getWithPrice($req->app_id, $escort_data->agency_id, $escort_data->gender, $opt_product, $escort_data->pseudo_escort, null, $escort_data->profile_type);
						$opt_products_data[] = $opt_pr;
					}
					//var_dump($opt_products_data);die;
				}*/
				
				if ( ($req->discount != "0.00" || $req->discount != "0") || ($req->fix_discount != "0.00" || $req->fix_discount != "0") ) {
					$comm = trim($req->comment);
					if ( ! strlen($comm) ) {
						$validator->setError('comm', 'Comment is Required');
					}
				}
				
				
				$data = array(
					
					'adv_id' => $req->adv_id,
					'adv_data' => $adv_data,
					'activate_promo_package' => $req->activate_promo_package,
					'activation_date' => $req->activation_date,
					'de_days' => $req->de_days,
					'package' => $req->package,
					'package_data' => $package_data,
					'period' => $req->period,
					'discount' => $req->discount,
					'fix_discount' => $req->fix_discount,
					'surcharge' => $req->surcharge,
					'opt_products' => $req->opt_products,
					'opt_products_data' => $opt_products_data,
					'comment' => mysqli_escape_string($req->comment),
					'price' => $req->h_package_price,
					'city' => $city,
					'category' => $category,
					'zone' => Model_Advertisements::getDefinedZoneForCity($city),
					/*'premium_city_spots' => $req->premium_city_spots,*/
					'add_areas' => $req->add_areas_id,
				);
				
				$ses_packages->packages[$req->adv_id] = $data;
				
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function clearSessionAction()
	{
		$ses_packages = new Zend_Session_Namespace('order_packages');
		unset($ses_packages->packages);
	}

	public function ajaxGetPackagesAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		
		$adv_id = intval($req->adv_id);
		$base_price = $req->base_price;

		$m_packages = new Model_Packages();

		if($req->category){
			$category = $req->category;
		}
		else{
			$m_escorts = new Model_Advertisements();
			$adv = $m_escorts->get($adv_id);
			$category = $adv->category;
		}
		//$m_p = new Model_Billing_Packages();
		//$active_package = $m_p->getByEscortId($escort_id);
		
		$packages = $m_packages->getWithPrice($category);
				
		die(json_encode($packages));
	}
	
	public function ajaxSalesUsersAction()
	{
		$model = new Model_Moderators();

		$users = array();
		foreach ( $model->getByApplicationId($this->_getParam('app_id')) as $user ) {
			$users[] = array('id' => $user->id, 'username' => $user->username);
		}

		die(json_encode($users));
	}

	public function ajaxGetPackageAllProductsAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$package_id = intval($req->package_id);
		$adv_id = intval($req->adv_id);
		
		$data = array('opt_products' => NULL, 'products' => NULL);

		die(json_encode($data));
		// TEMPORARY NO products logic not needed at this moment
		$m_packages = new Model_Packages();

		$m_escorts = new Model_Escorts();
		$escort = $m_escorts->get($escort_id);

		//$package = $m_packages->get($package_id);
		$opt_products = $m_packages->getOptProducts($package_id, $app_id, $escort->agency_id, $escort->gender, $escort->pseudo_escort, true, $escort->profile_type);
		foreach($opt_products as $k => $pr) {
			if ($pr['id'] == GIRL_OF_THE_DAY ) {
				unset($opt_products[$k]);
			}
		}
		$opt_products = array_values($opt_products);
		$products = $m_packages->getProducts($package_id, $app_id, $escort->agency_id, $escort->gender, $escort->pseudo_escort, $escort->profile_type);

		$data = array('opt_products' => $opt_products, 'products' => $products);

		die(json_encode($data));
	}

	public function itemsAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		//$bu_user = Zend_Auth::getInstance()->getIdentity();
			
		$model = new Model_Advertisements();

		$row = $model->getForPicker(intval($req->ads_id));
		
		if($row){
			$result = array(
				'id' => $row->id,
				'title' => $row->title,
				'username' => $row->username,
				'category' => $row->category,
				'user_id' => $row->user_id,
				//'photo' => $row->getMainPhoto()->getUrl('backend_smallest')
			);
		}
		else{
			$result = array('id' => 0);
		}
				
		die(json_encode($result));
	}

	public function testAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNorender(true);
		
		$user_id = $this->_getParam('user_id');

		$mOrders = new Model_Billing_Orders();

		$mOrders->processUserOrders($user_id);
	}

	public function removeAction()
	{
		$this->view->layout()->disableLayout();
		$order_id = $this->_request->order_id;

		$m_orders = new Model_Billing_Orders();
		$m_orders->removeOrder($order_id);

		die;
	}
	
	public function closeAction()
	{
		$this->view->layout()->disableLayout();
		$order_id = $this->_request->order_id;

		$m_orders = new Model_Billing_Orders();
		$m_orders->closeOrder($order_id);

		die;
	}
	
	public function exportAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		$req = $this->_request;

		$data = new Cubix_Form_Data($this->_request);
		$data->setFields(array(
			'sales_user_id' => 'int',
			'status' => '',
			'filter_by' => '',
			'date_from' => 'int',
			'date_to' => 'int',
			'transfer_type_id' => 'int',
			
			'discounted_orders' => '',
			
			'user_id' => '',


			'showname' => '',
			'email' => '',
			'agency_name' => '',
			'order_id' => ''
		));
		$data = $data->getData();
		
		$bu_user = Zend_Auth::getInstance()->getIdentity();
		
		// <editor-fold defaultstate="collapsed" desc="Filter">
		$filter = array();

		$filter['o.application_id = ?'] = $this->_request->application_id;

		if ( $data['email'] ) {
			$filter['u.email LIKE ?'] = $data['email'] . '%';
		}

		if ( $data['showname'] ) {
			$filter['e.showname LIKE ?'] = $data['showname'] . '%';
		}

		if ( $data['agency_name'] ) {
			$filter['a.name LIKE ?'] = $data['agency_name'] . '%';
		}

		if ( $data['order_id'] ) {
			$filter['o.system_order_id = ?'] = $data['order_id'];
		}
		
		if ( $data['discounted_orders'] ) {
			$filter['(op.discount <> 0 OR op.discount_fixed <> 0)'] = true;
		}

		if ( $data['transfer_type_id'] ) {
			$filter['t.transfer_type_id = ?'] = $data['transfer_type_id'];
		}

		if ( $bu_user->type == 'superadmin' ) {
			if ( $data['sales_user_id'] ) {
				$filter['o.backend_user_id = ?'] = $data['sales_user_id'];
			}
		}
		else {
			if ( $bu_user->type == 'sales manager' ||  $bu_user->type == 'sales clerk' ) {
				$filter['o.backend_user_id = ?'] = $bu_user->id;
				//$filter['o.backend_user_id = ?'] = $data['sales_user_id'];
			}
			else if ($bu_user->type == 'admin') {
				$filter['bu.application_id = ?'] = $bu_user->application_id;
				$filter['o.backend_user_id = ?'] = $data['sales_user_id'];
			}
		}

		if ( $data['status'] ) {
			if ( ! is_array($data['status']) ) {
				$filter['o.status = ?'] = $data['status'];
			}
			else {
				$filter['o.status IN (' . implode(', ', $data['status']) . ')'] = true;
			}
		}

		if ( $data['filter_by'] ) {
			if ( in_array($data['filter_by'], array('order_date', 'payment_date')) ) {
				if ( $data['date_from'] ) {
					$filter['DATE(' . $data['filter_by'] . ') >= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_from'] . '))');
				}

				if ( $data['date_to'] ) {
					$filter['DATE(' . $data['filter_by'] . ') <= ?'] = new Zend_Db_Expr('DATE(FROM_UNIXTIME(' . $data['date_to'] . '))');
				}
			}
		}
		
		if ( ! is_null($this->_getParam('user_id')) ) {
			$filter['u.id = ?'] = $data['user_id'];
		}
		// </editor-fold>
		
		$page = $req->page ? (int) $req->page : null;
		$per_page = $req->per_page ? (int) $req->per_page : null;
		$sort = $req->sort_field ? $req->sort_field : 'o.order_date';
		$dir = $req->dir ? $req->dir : '';
		
		$data = $this->_model->getDataForExport(
			$filter,
			$page,
			$per_page,
			$sort,
			$dir
		);
		
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename=orders.csv');
		
		$handle = fopen('php://output', 'w');
		if($bu_user->id == 16  && Cubix_Application::getId() == APP_EF){
			foreach ( $data['data'] as $order ) {
				fputcsv($handle, array(
					$order['showname'], $order['order_id'],$order['order_date'],$order['price']
				));
			}
		}
		else{
			foreach ( $data['data'] as $order ) {
				fputcsv($handle, array(
					$order['user_type'], $order['username'], $order['order_id'], $order['id'], $order['status'], $order['packages'],
					$order['order_date'], $order['payment_date'], $order['price'],
					$order['real'], $order['sales_person']
				));
			}
		}
		
		fclose($handle);
		
		exit;
	}
	
	public function getActivateMethodsAction()
	{
		$req = $this->_request;
		
		$adv_id = $req->adv_id;
		$selected = $req->selected;
		
		$activate_options = Model_Packages::$ACTIVATE_LABELS;
		
		$m_package = new Model_Billing_Packages();

        $has_active_package = $m_package->hasPaidActivePackage($adv_id);

		$select_html = "";

		if ( ! $has_active_package || $selected == Model_Packages::ACTIVATE_ASAP) {

			$activate_options = array(
				Model_Packages::ACTIVATE_ASAP => "ASAP",
				Model_Packages::ACTIVATE_AT_EXACT_DATE => "at exact date"
			);

		} else {
			$activate_options = array(
				Model_Packages::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES => "after current package expires",
				Model_Packages::ACTIVATE_AT_EXACT_DATE => "at exact date",				
			);
		}

		foreach ($activate_options as $key => $value) {
			$select_html .= '<option value="' . $key . '" ' . (($key == $selected) ? 'selected="selected"' : '') . '>' . $value . '</option>';
		}

		die($select_html);
	}
	
	public function changeCcOrderStatusAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$order_id = (int) $req->order_id;
		
		$m_orders = new Model_Billing_Orders();
		$m_orders->changeCCOrderStatus($order_id);
		
		die;		
	}
	
	public function totalAction()
	{
		$this->view->layout()->disableLayout();
		
	}
	
	public function differenceAction()
	{
		$this->view->layout()->disableLayout();
		
	}
	
}
