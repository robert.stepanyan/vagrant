
Cubix.NewsletterConstruct = new Class({
	initialize: function(html) {
		this.html = html;
		this.self = this;
	},
	
	makeEditable: function() {
		var self = this;
		elements = this.html.getElements('.nsl-el');
		elements.each(function(el, i) {
			obj = self._getAppropType(el.get('data-type'));
			if ( obj ) {
				obj.makeEditable(el, i);
			} else {
				console.log('no such type');
			}
		});
	},
	
	construct: function() {
		var self = this;
		
		if ( $$('.swiff-uploader-box').length ) {
			$$('.swiff-uploader-box').destroy();
		}
		
		elements = this.html.getElements('.nsl-el');
		elements.each(function(el, i) {
			obj = self._getAppropType(el.get('data-type'));
			if ( obj ) {
				obj.construct(el, i);
			} else {
				console.log('no such type');
			}
		});
	},
	
	
	_getAppropType: function(type) {
		obj = null;
		cl = 'Cubix.NewsletterConstruct.ElementTypes.' + type;
		eval('if( $defined(' + cl + ') ) obj=' + cl);
		if ( obj ) {
			return obj;
		}
		
		return false;
	}
});


Cubix.NewsletterConstruct.getFinalHtml = function(body) {
	var html = 
		'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' +
		'<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' +
			'<head>' +
				'<title></title>	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' +
				'<meta http-equiv="Cache-Control" content="no-cache" />' +
			'</head>' +
			'<body>' +
			body +
			'</body>' +
		'</html>';
	
	return html;
};

Cubix.NewsletterConstruct.ElementTypes = {};
Cubix.NewsletterConstruct.ElementTypes.Title = {
	makeEditable: function(element, i) {
		var txt = element.get('html');
		
		new Element('input', {
			type: 'text',
			'class': 'title-src-' + i + ' nsl-el-cnvrt',
			value: txt
		}).inject(element, 'after');
		

		element.addClass('title-' + i)
			.set('data-display-type', element.getStyle('display'))
			.setStyle('display', 'none');
	},
	
	construct: function(element, i) {
		var txt = $$('.title-src-' + i).get('value')[0];
		$$('.title-src-' + i).destroy();
		element.set('html', txt).setStyle('display', element.get('data-display-type'));
		element.removeProperty('data-display-type');
	}
};

Cubix.NewsletterConstruct.ElementTypes.Text = {
	makeEditable: function(element, i) {
		var txt = element.get('html');
		
		new Element('textarea', {
			'class': 'text-src-' + i + ' nsl-el-cnvrt',
			value: txt
		}).inject(element, 'after');
		
		element.addClass('text-' + i)
			.setStyle('display', 'none');
	},
	
	construct: function(element, i) {
		var txt = $$('.text-src-' + i).get('value')[0];
		$$('.text-src-' + i).destroy();
		element.set('html', txt).setStyle('display', 'inline');
	}
};

Cubix.NewsletterConstruct.ElementTypes.Link = {
	makeEditable: function(element, i) {
		var text = element.get('html');
		var link = element.get('href');
		
		new Element('input', {
			type: 'text',
			'class': 'link-src-' + i + ' nsl-el-cnvrt',
			value: text + ';' + (link ? link : '')
		}).inject(element, 'after');
		
		element.addClass('link-' + i)
			.setStyle('display', 'none');
	},
	
	construct: function(element, i) {
		var txt = $$('.link-src-' + i).get('value')[0];
		$$('.link-src-' + i).destroy();
		
		var data = txt.split(';');
		
		if ( data[0] ) {
			element.set('html', data[0]);
		}
		
		if ( data[1] ) {
			element.set('href', data[1]);
		}
		
		element.setStyle('display', 'inline');
	}
};

Cubix.NewsletterConstruct.ElementTypes.Button = {
	makeEditable: function(element, i) {
		var link = element.get('href');
		
		new Element('input', {
			type: 'text',
			'class': 'button-src-' + i + ' nsl-el-cnvrt',
			value: link ? link : ''
		}).inject(element, 'after');
		
		element.addClass('button-' + i)
			.setStyle('display', 'none');
	},
	
	construct: function(element, i) {
		var link = $$('.button-src-' + i).get('value')[0];

		$$('.button-src-' + i).destroy();
		
		if ( link ) {
			element.set('href', link);
		}
		
		element.setStyle('display', 'inline');
	}
};

Cubix.NewsletterConstruct.ElementTypes.Image = {
	makeEditable: function(element, i) {
		var width = element.get('width'),
			height = element.get('height');
		
		new Swiff.Uploader({
			appendCookieData: true,
			path: '/js/fancyupload/Swiff.Uploader.swf',
			url: '/newsletter/templates-v2/upload?size=nsl_' + width + '_' + height,
			zIndex: 200,
			verbose: false,
			queued: false,
			multiple: false,
			target: element,
			instantStart: true,
			typeFilter: {
				'Images (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'
			},
			fileSizeMax: 2 * 1024 * 1024,
			onSelect: function(files) {
				
			},
			onFileProgress: function (file) {
				if ( self.progress ) {
					self.progress.set(this.percentLoaded);
				}
			},
			onFileComplete: function(file) {
				var resp = JSON.decode(file.response.text);
				file.remove();
				
				if ( resp.status == 'error' ) {
					alert('An error occured when upload file:\n' + resp.msg);
					return;
				}
				element.set('src', resp.photo);
			},
			onComplete: function () {
				if ( self.progress ) {
					self.progress.set(0);
				}
				
				this.setEnabled(true);
			}
		});
	},
	
	construct: function(element, i) {
	}
};

Cubix.NewsletterConstruct.ElementTypes.EscortPicker = {
	makeEditable: function(element, i) {
		var escortPicker = new EscortPicker();

		escortPicker.addEvent('select', function (data, escortId) {
			rp.setStyle('background-image', 'url("' + data.image + '")');
			rp.set('data-link', data.link)
					.set('data-image', data.image);
		
			if( element.getParent('td').getElement('.escort-name') ){
				element.getParent('td').getElement('.escort-name').set('html', data.showname);
			}

			if( element.getParent('td').getElement('.escort-city') ){
				element.getParent('td').getElement('.escort-city').set('html', data.city);
			}

			if( element.getParent('td').getElement('.escort-country') ){
				element.getParent('td').getElement('.escort-country').set('html', data.country);
			}

			if( element.getParent('td').getElement('.escort-price') ){
				element.getParent('td').getElement('.escort-price').set('html', data.price);
			}

			if ( element.getParent('td').getElement('span[data-type="showname"]') ) {
				element.getParent('td').getElement('span[data-type="showname"]').set('html', data.showname.substring(0, 16));
			}
		});
		
		var width = element.getElement('img').get('width');
		var height = element.getElement('img').get('height');
		var src = element.getElement('img').get('src');
		
		if(element.getElement('.showname')){
			var showname = element.getElement('.showname').get('html');
		}

		if(element.getElement('.city')){
			var city = element.getElement('.city').get('html');
		}

		if(element.getElement('.country')){
			var country = element.getElement('.country').get('html');
		}


		if(element.getElement('.price')){
			var price = element.getElement('.price').get('html');
		}
		
		var rp = new Element('div', {
			'class': 'escort-src-' + i + ' nsl-el-cnvrt'
		}).setStyles({
			position: 'relative',
			width: width + 'px',
			height: height + 'px',
			'background-size': 'cover',
			'background-image': 'url("' + src + '")'
		});
		
		new Element('input', {
			type: 'text',
			placeholder: 'EscortID-> ENTER'
		}).setStyles({
			position: 'relative',
			top: '12px',
			width: '125px',
			display: 'block',
			margin: 'auto'
		}).addEvent('keydown', function(event) {
			if ( event.code == 13 ) {
				escortPicker.open(this.get('value'), width, height);
			}
		}).inject(rp);
		
		rp.inject(element, 'after');

		if(element.getElement('.city')){
			$escort_city = new Element('span', {
				'class': 'escort-src-' + i + ' escort-city',
				'html': city
			}).setStyles({
				color: '#000',
				'font-weight': '900'
			});
			$escort_city.inject(rp, 'after');
			
			$escort_name = new Element('span', {
				'class': 'escort-src-' + i + ' escort-name',
				'html': showname
			}).setStyles({
				color: 'rgb(237, 20, 91)',
				'font-weight': '900', 'display': 'block'
			});
			
			$escort_name.inject(rp, 'after');
		}

		if(element.getElement('.country')){
			$escort_country = new Element('span', {
				'class': 'escort-src-' + i + ' escort-country',
				'html': country
			}).setStyles({
				color: '#000',
				'font-weight': '900'
			});
			$escort_country.inject(rp, 'after');
			
			$escort_name = new Element('span', {
				'class': 'escort-src-' + i + ' escort-name',
				'html': showname
			}).setStyles({
				color: 'rgb(237, 20, 91)',
				'font-weight': '900', 'display': 'block'
			});
			
			$escort_name.inject(rp, 'after');
		}

		if(element.getElement('.price')){
			$escort_price = new Element('span', {
				'class': 'escort-src-' + i + ' escort-price',
				'html': price
			}).setStyles({
				color: '#000',
				'font-weight': '900'
			});
			$escort_price.inject(rp, 'after');
		}

		element.setStyle('display', 'none');
	},
	
	construct: function(element, i) {

		var image, link, price, showname, city, country;

		if ( $$('.escort-src-' + i).length ) {
			image = $$('.escort-src-' + i)[0].get('data-image');
		}
		if ( $$('.escort-src-' + i).length) {
			link = $$('.escort-src-' + i)[0].get('data-link') + '#newsletterPopupTurnOff';
		}

		if( $$('.escort-src-' + i +'.escort-name')[0] !== undefined ){
			showname = $$('.escort-src-' + i +'.escort-name')[0].get('html');
		}

		if( $$('.escort-src-' + i +'.escort-city')[0] !== undefined ){
			city = $$('.escort-src-' + i +'.escort-city')[0].get('html');
		}

		if( $$('.escort-src-' + i +'.escort-country')[0] !== undefined ){
			country = $$('.escort-src-' + i +'.escort-country')[0].get('html');
		}

		if( $$('.escort-src-' + i +'.escort-price')[0] !== undefined ){
			price = $$('.escort-src-' + i +'.escort-price')[0].get('html');
		}
		
		if ( image && link ) {

			element.set('href', link);
			element.getElement('img').set('src', image);
		}
		
		
		
		if( element.getElement('.showname') != null &&  typeof showname !== 'undefined' ){
			element.getElement('.showname').set('html', showname);
		}

		if( typeof city !== 'undefined'    ){
			element.getElement('.city').set('html', city);
		}

		if( typeof country !== 'undefined'    ){
			element.getElement('.country').set('html', country);
		}
		
		if( typeof price !== 'undefined'  ){
			element.getElement('.price').set('html', price);
		}
	
		$$('.escort-src-' + i).destroy();
		element.setStyle('display', 'inline');
	}
};

var EscortPicker = new Class({
	Implements: Events,
	popup: null,
	selected: null,
	initialize: function () {
		this.popup = new Cubix.Popup.Ajax({
			abar: [
				{ caption: 'Select', icon: 'accept', action: this.select.bind(this) },
				{ caption: 'Cancel', icon: 'cancel', action: Cubix.Popup.Actions.Close }
			],
			width: 1000,
			zIndex: 500
		});

		this.popup.setTitle('Escorts Registered in Last 7 Days');

		this.selected = {};
	},

	open: function (escortId, width, height) {
		this.popup.load('/newsletter/templates-v2/' + 'escort-picker?escort_id=' + escortId + '&nsl_img_size=nsl_' + width + '_' + height, function () {
			this.content.getElements('.escort-picker img').addEvent('click', function (e) {
				e.stop();

				var bg = this.getParent('tr').getStyle('background-color');
				if ( this.hasClass('checked') ) {
					bg = '';
					this.removeClass('checked');
				}
				else {
					this.getParent('td').getElements('img').removeClass('checked');
					this.addClass('checked');
					bg = '#efe';
				}
			});

			this.open();
			this.loaded = true;
		});
	},

	select: function () {

		this.selected = {};
		this.popup.content.getElements('.escort-picker img.checked').each(function (el) {

			this.selected = {
				escortId: el.get('cubix:escortId'),
				photo_id: el.get('cubix:photoId'), 
				city: el.get('cubix:city'), 
				country: el.get('cubix:country'), 
				price: el.get('cubix:price'), 
				//showname: el.getParent('tr').getFirst('td').get('data-showname'), 
				// I was unabel to find "showname" where we are using it so I have add new one and commented that line - max
				showname: el.get('cubix:showname'), 
				link: el.getParent('tr').getFirst('td').get('data-link'), 
				image: el.get('data-image'),
				el: el 
			};
		
		}.bind(this));

		this.fireEvent('select', [this.selected]);
		
		this.popup.close();
	}
});