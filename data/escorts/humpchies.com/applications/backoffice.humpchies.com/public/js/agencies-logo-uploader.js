Cubix.AgencyUploader = new Class({
	Implements: [Events, Options],
	
	swf: null,
	
	initialize: function (opt) {
		opt = opt || {};
		this.setOptions(opt);
		
		if ( this.options.progress ) {
			this.progress = new Fx.ProgressBar(this.options.progress);
			this.progress.set(0);
		}
		
		var self = this;
		
		this.swf = new Swiff.Uploader({
			appendCookieData: true,
			path: '/js/fancyupload/Swiff.Uploader.swf',
			url: this.options.url,
			zIndex: 2,
			verbose: false,
			queued: false,
			multiple: false,
			target: opt.el,
			instantStart: true,
			typeFilter: {
				'Images (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'
			},
			fileSizeMax: 2 * 1024 * 1024,
			onSelect: function(files) {
				self.fireEvent('select', files[0]);
				this.setEnabled(false);
			},
			onFileProgress: function (file) {
				if ( self.progress ) {
					self.progress.set(this.percentLoaded);
				}
			},
			onFileComplete: function(file) {
				//if ( console && console.log ) console.log(file.response.text);
				var resp = JSON.decode(file.response.text);
				self.fireEvent('complete', resp);
				
				file.remove();
				
				if ( resp.status == 'error' ) {
					alert('An error occured when upload file:\n' + resp.msg);
					return;
				}
			},
			onComplete: function () {
				if ( self.progress ) {
					self.progress.set(0);
				}
				
				this.setEnabled(true);
			}
		});
	},
	
	setData: function (_data) {
		var data = this.swf.options.data || {};
		this.swf.setOptions({ data: $extend(data, _data) });
	},
	
	upload: function () {
		this.swf.start();
	},
	
	unitLabels: {
		b: [{min: 1, unit: 'B'}, {min: 1024, unit: 'kB'}, {min: 1048576, unit: 'MB'}, {min: 1073741824, unit: 'GB'}],
		s: [{min: 1, unit: 's'}, {min: 60, unit: 'm'}, {min: 3600, unit: 'h'}, {min: 86400, unit: 'd'}]
	},
	
	formatUnit: function(base, type, join) {
		var labels = this.unitLabels[(type == 'bps') ? 'b' : type];
		var append = (type == 'bps') ? '/s' : '';
		var i, l = labels.length, value;

		if (base < 1) return '0 ' + labels[0].unit + append;

		if (type == 's') {
			var units = [];

			for (i = l - 1; i >= 0; i--) {
				value = Math.floor(base / labels[i].min);
				if (value) {
					units.push(value + ' ' + labels[i].unit);
					base -= value * labels[i].min;
					if (!base) break;
				}
			}

			return (join === false) ? units : units.join(join || ', ');
		}

		for (i = l - 1; i >= 0; i--) {
			value = labels[i].min;
			if (base >= value) break;
		}

		return (base / value).toFixed(1) + ' ' + labels[i].unit + append;
	},
	
	destroy: function () {
		this.swf.box.destroy();
		this.swf.toElement().destroy();
	}
});
