/**
 * Created by SceonDev on 19.06.2017.
 */
Escort.VideoPopup = function (popup,config) {

    var date_from = '';
    var date_to = '';

    function RemoveVideo()
	{
		var checkbox = $$('.main input:checked');
		if(typeof checkbox[0]!='undefined' && confirm('Are You Sure?'))
		{
			var removeRequest = new Request({
                url: '/video/remove',
				method:'POST',
				data:{'escort_id': popup.escort_id,'check':checkbox.get('value')},
				onSuccess:function(resp)
				{
					if(resp && resp=='1')
					{	
						checkbox.each(function(el){
							el.getParent('li').dispose();
						});
						var approve = $$('input[rel=approve]').length;
						var pending = $$('input[rel=pending]').length;
						if((pending==(max-1) && approve==1) || (approve==(max-1) && pending==1) || (pending>=max)||(approve>=max))
						{	
							$('videocontrol_btnAddfile').set('disabled','disabled').setStyle('opacity',0.5);
							$('videocontrol_btnbStart').set('disabled','disabled').setStyle('opacity',0.5);;
						}
					else
						{
							$('videocontrol_btnAddfile').setStyle('opacity',1).removeAttribute('disabled');
							$('videocontrol_btnbStart').setStyle('opacity',1).removeAttribute('disabled')
						}
						moo.optioons.maxfiles = max-(pending+approve);

					}
				}
			});
			removeRequest.send();
		}
	}

	function ClickVideo()
	{	

		var img = $(this);
		var width = $(this).get('owidth');
		var height = $(this).get('oheight');
		var file = img.get('rel');
		file = file + '_' + height + 'p.mp4';	
		
		var vm = new VideoModal({	
			"hideFooter":true,
			'width':width,
			'height':height,
			'onAppend':function()	
			{	
				jwplayer('video-modal').setup({
					flashplayer: '/flash.swf',
					height:height,
					logo: {
                        file: "/img/video_logos/"+config.application_host+".png"
					},
					width:width,
					image:img.get('src').replace('m320','orig'),
					autostart: true,
					file: config.remote_url+''+ config.application_host + "/"+ popup.escort_id + file
				});
			}
		});
		vm.addButton("Cancel", "btn");

		vm.show({ 
			"model":"modal", 
			"title":"Escort id "+ popup.escort_id,
			'contents':'<div id="video-modal"></div>'
			}); 

	};
	
	var PlayBtn = function()
	{
		$$('.video_play_btn').addEvent('click',function(){
			$(this).getPrevious('img').fireEvent('click');
		});
	}
	
	var RotateVideo = function(side)
	{
		if(confirm('Are you sure you want to rotate')){
			var escort_id = popup.escort_id;
			var request_id = popup.request_id;
			var overlay = new Cubix.Overlay($('video-tabs') , { showLoader: true });
			overlay.disable();
			
			var req = new Request({
				method: 'post',
				url: '/video/rotate-video',
				data: {
					escort_id: escort_id,
					video_id: request_id,
					side: side
				},
				onComplete: function(data) {
					resp = JSON.decode(data);
					if (resp.finish ) 
					{
						popup.request_id = resp.video_id;
						$$('#video-tabs .main')[0].set('html','');
						var video ='<li><table class="photo"><tr><td class="main"><div class="wrapper"><div class="chk">';
								video+='<input type="checkbox" class="checked" name="check[]" id="label_'+ resp.video_id +'" rel="approve" value="'+ resp.video_id +'" /></div>';
								video+='<div class="actions"><span class="rotate-right"></span><span class="rotate-left"></span></div>';
								video+='<img width="300" owidth="'+ resp.width +'" oheight="'+ resp.height +'" rel="/'+ resp.video +'"  align="left" src="'+resp.src +'" />';
								video+='<span class="video_play_btn"></span></div></td><td class="vlabel"><span style="display:block;">'+ resp.date +'</span><label class="status" for="label_'+ resp.video_id +'" >Approve</label></td></tr></table></li>';
						var tmp =  new Element('ul');
						tmp.set('html',video);
						tmp.getFirst().inject($$('#video-tabs .main')[0],'bottom');
						/*if(resp.hide)
						{	
							$('videocontrol_btnAddfile').set('disabled','disabled').setStyle('opacity',0.5);
							$('videocontrol_btnbStart').set('disabled','disabled').setStyle('opacity',0.5);
						}
						else
							{
								$('videocontrol_btnAddfile').setStyle('opacity',1).removeAttribute('disabled');
								$('videocontrol_btnbStart').setStyle('opacity',1).removeAttribute('disabled');
							}*/

						$$('.photo img').addEvent('click',ClickVideo);
						PlayBtn();
						$$('.rotate-left').addEvent('click',function(){
							RotateVideo(2);
						});
						$$('.rotate-right').addEvent('click',function(){
							RotateVideo(1);
						});
						overlay.enable();
						
					}
					else
					{	
						//$('videocontrol_file_'+(fileindex*1+1)).set('html',resp.error);
					}
					
				}
			}).send();
		}
	}

    function ApproveVideo()
    {
        var checkbox = $$('.main input:checked');
        if(typeof checkbox[0] != 'undefined')
        {
            var type = 'disabled';
            var data = $('edit_video').toQueryString();
            if($(this).hasClass('btn_approve'))
            {
                data += '&approve=1';
                type = 'approved';
                var approve = $$('input[rel=approve]').length;
                if(checkbox.length>max || (approve+checkbox.length>max))
                {
                    alert('You can approve only '+max+' Video');
                    return false;
                }
            }
            else
            {
                data += '&approve=0';
            }

            data += '&escort_id='+ popup.escort_id
            var myRequest = new Request({
                url: $('edit_video').get('action'),
                method:'POST',
                data:data,
                onSuccess: function(res){
                    if(res && res == '1')
                    {	var success = $$('.save_success')[0];
                        if(!success)
                        {
                            var span  = new Element("span", {text: "Save",'class':'save_success'});
                            span.inject('video-tabs','before');
                            success = $$('.save_success')[0];
                        }
                        else
                        {
                            success.fade(1).setStyle('display','block');
                        }

                        checkbox.each(function(el){
                            el.getParent('.photo').getElement('.status').set('html',type.capitalize());
                            el.set('rel',type);
                            if(type == 'disabled')
                            {
                                el.getParent('.wrapper').getElement('img').addClass('disable');
                            }
                            else
                            {
                                el.getParent('.wrapper').getElement('img').removeClass('disable');
                            }
                        });
                        setTimeout(function(){
                            success.fade('hide').setStyle('display','none');

                            var approve = $$('input[rel=approve]').length;
                            var pending = $$('input[rel=pending]').length;
                            if((pending==(max-1) && approve==1) || (approve==(max-1) && pending==1) || (pending>=max)||(approve>=max))
                            {
                                $('videocontrol_btnAddfile').set('disabled','disabled').setStyle('opacity',0.5);
                                $('videocontrol_btnbStart').set('disabled','disabled').setStyle('opacity',0.5);
                            }
                            else
                            {
                                $('videocontrol_btnAddfile').setStyle('opacity',1).removeAttribute('disabled');
                                $('videocontrol_btnbStart').setStyle('opacity',1).removeAttribute('disabled')
                            }

                            moo.options.maxfiles = max-(pending+approve);

                        },1500);

                    }
                }
            });
            myRequest.send();
        }
        else
        {
            alert('Select Video');
        }
    }

    new Cubix.Shadow($('content'));
    var getFilterParams = function () {
        var uri = new URI('?' + $$('form')[0].toQueryString());

        return uri.get('data');
    };

    var Reject_req = function ()
    {
        var escort_id = popup.escort_id;
        var request_id = popup.request_id;
        var status = 'rejected';
        var rejectionText = $('video_rejection_text').get('value');

        if(rejectionText.length < 10 ){
            $('rsn-cont').getElement('.error-msg').set('text','Rejection text required.');
        }
        else{
            var req = new Request({
                method: 'post',
                url: '/video/reject',
                data: {
                    escort_id: escort_id,
                    request_id: request_id,
                    status: status,
                    text: rejectionText
                },
                onComplete: function(data) {
                    data = JSON.decode(data);
                    var status = data.status;

                    if (status == 'success')
                    {
                        window.reject_popup.close();
                        $('rsn-cont').destroy();
                        popup.close();
                    }
                    else
                    {
                        var msgs = data.msgs;
                        $('rsn-cont').set('text',msgs);

                    }
                }
            }).send();
        }

    }

    var reject_popup = new Cubix.Popup.Ajax({
        abar: [
            { caption: 'Reject', icon: 'cancel', action: Reject_req }
        ],
        width: 550,
        zIndex: 20
    });

    window.reject_popup = reject_popup;

        window.fireEvent('resize');
        max = $('max').get('value');
        moo = '';

        new Cubix.Button($('btn-approve'), {icon: 'accept', caption: 'Approve', classes: 'btn_approve approve_ajax'});
        new Cubix.Button($('btn-disable'), {icon: 'delete', caption: 'Disable', classes: 'btn_disable approve_ajax'});
        new Cubix.Button($('btn-remove'), {icon: 'delete', caption: 'Remove', classes: 'btn_remove'});
        var approve = $$('input[rel=approve]').length;
        var pending = $$('input[rel=pending]').length;

        $$('.photo img').addEvent('click', ClickVideo);
        PlayBtn();
        $$('.approve_ajax').addEvent('click', ApproveVideo);
        $$('.btn_remove').addEvent('click', RemoveVideo);
        $$('.rotate-left').addEvent('click', function () {
            RotateVideo(2);
        });
        $$('.rotate-right').addEvent('click', function () {
            RotateVideo(1);
        });

        var upOverlay = new Cubix.Overlay($('video-tabs'), {showLoader: true});


        moo = new MooUpload('videocontrol', {
            action: '/video/upload-video-mp4?escort_id=' + popup.escort_id,// Server side upload script
            method: 'html5',		// Use only HTML5 method
            //blocksize: 999999999,	// Load per one chunk
            accept: '.mp4,.flv,.wmv',
            multiple: false,
            onAddFiles: function () {
            },
            onFileUpload: function (fileindex, response) {

                if (response.error == 0 && response.wait_for_video) {
                    isVideoReady();
                    upOverlay.disable();
                }
            },
            onFileUploadError: function (filenum, response) {
                $$('.mooupload_error').appendText(response.error);
            },

        });


        var isVideoReady = function () {

            setTimeout(function () {
                new Request.JSON({
                    url: '/video/is-video-ready?escort_id=' + popup.escort_id,
                    method: 'get',
                    onSuccess: function (resp) {
                        if (resp.status == 0) {
                            isVideoReady();
                        }else{
                            initSuccess(resp);
                        }

                    }
                }).send();
            }, 2000);
        };

    var initSuccess = function (response) {
        upOverlay.enable();
        var video = '<li><table class="photo"><tr><td class="main"><div class="wrapper"><div class="chk">';
        video += '<input type="checkbox" class="checked" name="check[]" id="label_' + response.video_id + '" rel="approve" value="' + response.video_id + '" /></div>';
        video += '<img width="288" owidth="' + response.video_width + '" oheight="' + response.video_height + '" rel="/' + response.video + '"  align="left" src="' + response.photo_url + '" />';
        video += '<span class="video_play_btn"></span></div></td><td class="vlabel"><span style="display:block;"></span><label class="status" for="label_' + response.video_id + '" >Approve</label></td></tr></table></li>';
        var tmp = new Element('ul');
        tmp.set('html', video);
        tmp.getFirst().inject($$('#video-tabs .main')[0], 'bottom');
        $('videocontrol_btnAddfile').setStyle('opacity', 1).removeAttribute('disabled');
        $('videocontrol_btnbStart').setStyle('opacity', 1).removeAttribute('disabled');

        $$('.approve_ajax').addEvent('click', ApproveVideo);
        $$('#video-tabs img').addEvent('click', ClickVideo);
        PlayBtn();
        $$('.btn_remove').addEvent('click', RemoveVideo);
    };

    };






