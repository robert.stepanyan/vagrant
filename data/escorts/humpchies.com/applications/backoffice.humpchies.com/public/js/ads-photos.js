var Ads = {};
var imageWidth = 150;
var imageHeight = 205;
Ads.Photos = function (popup) {
	this.popup = popup;

	this.content = $(popup);
	
	/* >>> Photos initialization */
	
    /////////////////////////////////
	this.render_photo = function (photo) {
		var container = new Element('table', {
			'class': 'photo'
		});

		var content = new Element('td', {}).inject(new Element('tr', {}).inject(container));
		var wrapper = new Element('div', {'class': 'wrapper'}).inject(content);

		var cropper =  new Element('div', {'class': 'border'}).inject(wrapper);
		new Element('div',{'class':'border_bg',
					styles:{'background-image':'url('+photo.image_url+')'}
					}).inject(cropper);
		var wrapper_chunks = new Element('div', {'class': 'chunks'}).inject(wrapper);
		wrapper.data = photo;
		
		new Element('div', {'class': 'status '}).inject(wrapper);
//		new Element('div', {'class': 'handler'}).inject(wrapper_chunks);
		var checkbox = new Element('a', {
            'href': '#',
			'class': 'btn',
			'rel': photo.id
		}).inject(new Element('div', {'class': 'chk'}).inject(wrapper_chunks));
			
		
		var img = new Element('img', {
			src: photo.image_url
			//'width':photo.is_portrait==1?imageWidth:'',
			//'height':photo.is_portrait==0?imageHeight:''
	
//			events: {
//				click: function () {
//					if ( checkbox.get('checked') ) {
//						checkbox.set('checked', false);
//					}
//					else {
//						checkbox.set('checked', true);
//					}
//				}
//			}
		}).inject(wrapper);
		//console.log(photo);
		var a = new Element('a', {
			href: photo.orig_url + '?origano=true',
			html: 'download',
			target: '_blank',
			'class': 'down_link'

		}).inject(wrapper);

		var li = new Element('li');
		
		li.data = photo;
		
		container.inject(li);
		
		li.cropper = new Cropper(wrapper).addEvent('complete', saveAdjustment);

		window.fireEvent('resize');
		
		return li;
	}.bind(this);
    ///////////////////////////
	
	this.mark_photo = function (photos, Flag) {
		if ( ! photos ) return;
		
		if ( $type(photos) == 'element' ) photos = [photos];
		
		if ( Flag == 'main' ) {
			var tab = this.active_tab;
			this.content.getElement('.photos-' + tab).getElements('li').each(function (el) {
				el.getElement('td').removeClass('main');
				el.data.is_main = 0;
			});
		}
		
			
		
		photos.each(function (photo) {
			var status = photo.getElement('.status');
			var flags = new Hash();
			
			status.getElements('div').each(function (el) {
				flags.set(el.get('class'), el);
			});
			
			if ( Flag == 'hard' ) {
				if ( flags.get('hard') ) return;
				flag = new Element('div', {'class': 'hard', html: 'hard'});
				if ( flags.get('soft') ) flag.replaces(flags.get('soft'));
				else flag.injectBottom(status);
			}
			else if ( Flag == 'soft' ) {
				if ( flags.get('soft') ) return;
				flag = new Element('div', {'class': 'soft', html: 'soft'});
				if ( flags.get('hard') ) flag.replaces(flags.get('hard'));
				else flag.injectBottom(status);
			}
			else if ( Flag == 'p100' ) {
				if ( flags.get('p100') ) {flags.get('p100').destroy();return;}
				new Element('div', {'class': 'p100'}).injectTop(status);
			}
			else if ( Flag == 'gotd' ) {
				if ( flags.get('gotd') ) {flags.get('gotd').destroy();return;}
				this.content.getElements('.gotd').destroy();
				new Element('div', {'class': 'gotd', html: 'GOTD'}).injectTop(status);
			}
			else if ( Flag == 'private' ) {
				if ( flags.get('hard') ) flags.get('hard').destroy();
				if ( flags.get('soft') ) flags.get('soft').destroy();
				photo.getElement('.wrapper').removeClass('main');
				return;
			}
			else if ( Flag == 'main' ) {
				photo.getElement('td').addClass('main');
				photo.data.is_main = 1;
			}
			else if ( Flag == 'rotatable' ) {
				
				if(photo.data.is_main != 1){
					photo.getElement('td').addClass('rotatable');
				}
			}
			else if ( Flag == 'not_approved' ) {
				//var tab = this.is_private ? 'private' : 'public';
				//this.content.getElement('.photos-' + tab).getElements('li').each(function (el) {
					if ( photo.getElement('.wrapper').get('not_approved') ) {photo.getElement('.wrapper').get('not_approved').destroy();return;}
					flag = new Element('div', {'class': 'not_approved', html: 'not approved'});
					flag.injectBottom(photo.getElement('.wrapper'));
				//});
			}
			else if ( Flag == 'auto_approved' ) {
					if ( photo.getElement('.wrapper').get('auto_approved') ) {photo.getElement('.wrapper').get('auto_approved').destroy();return;}
					flag = new Element('div', {'class': 'auto_approved', html: 'auto approved'});
					flag.injectBottom(photo.getElement('.wrapper'));
			}
			else if ( Flag == 'retouch' ) {
				if ( flags.get('retouch') ) {flags.get('retouch').destroy();return;}
				new Element('div', {'class': 'retouch', html: 'retouch'}).injectTop(status);
				if ( photo.getElement('.wrapper').getElement('.not_approved') ) {photo.getElement('.wrapper').getElement('.not_approved').destroy();return;}
			}
			else if ( Flag == 'no_retouch' ) {
				//if ( flags.get('no_retouch') ) {flags.get('no_retouch').destroy();return;}
				new Element('div', {'class': 'retouched', html: 'no retouch'}).injectTop(status);
			}
			else if ( Flag == 'suspicious' ) {
				if ( flags.get('suspicious') ) {flags.get('suspicious').destroy();return;}
				new Element('div', {'class': 'suspicious', html: 'suspicious'}).injectTop(status);
			}
			else if ( Flag == 'investigation' ) {
				if ( flags.get('investigation') ) {flags.get('investigation').destroy();return;}
				new Element('div', {'class': 'suspicious', html: 'investigation'}).injectTop(status);
			}
		}.bind(this));
	};

    //////////
	this.init_photos = function () {

		/* >>> Get the json data from containers */
		['public'].each(function (container) {

			if ( ! this.content.getElement('.photos-' + container) ) return;
			container = this.content.getElement('.photos-' + container);
			var photos = JSON.decode(container.get('text'));
			container.empty();

            //console.log( photos );

			var clearer = new Element('div', {'class': 'clear'}).inject(container);
			
			photos.each(function (photo) {
				this.render_photo(photo).inject(clearer, 'before');
				window.fireEvent('resize');
			}.bind(this));
		}.bind(this));
		/* <<< */
	};
	/* <<< */
    //////////////////


	this.init_sorting = function (list) {
		var self = this;
		
		this.sortables = new Sortables(list, {
			clone: function (event, element, list) {
				if ( event.event.button ) {this.reset();return new Element('div');}
				
				var pos = element.getPosition(element.getOffsetParent());
				pos.x += 5;
				pos.y += 5;
				
				return element.getElement('img').clone(true).setStyles({
					'margin': '0px',
					'position': 'absolute',
					'z-index': 100,
					'visibility': 'hidden',
					'opacity': .7,
					'width': element.getElement('img').getStyle('width')
				}).inject(this.list).position(pos);
			},
			
			opacity: 1,
			revert: true,
			handle: '.handler',
			
			onStart: function (el) {
				this.drag.addEvent('enter', function () {
					this.started = true;
				}.bind(this));
			},
			
			onComplete: function (el) {
				if ( ! this.started ) return;
				else this.started = false;
				
				self.actions.save_order();
			}
		});
	};

	this.init_photos();
    remove_ad_photo();

	this.init_sorting($$('.photos-public')[0]);
	this.init_sorting($$('.photos-private')[0]);
	
	this.actions = {};
	
	this.actions.make_public_private = function (type) {
		var dest_tab;
		if ( type == 'private' ) {
			dest_tab = 'Public';
		}
		else if ( type == 'public' ) {
			dest_tab = 'Private';
		}
		
		var selection = this.get_selection(type);
		
		this.request_action(dest_tab.toLowerCase(), selection, function (resp) {
			var change_main = false;
			if ( type == 'public' ) {
				selection.els.each(function (el) {
					
					/*if ( el.getElement('.photo').data.is_main ) {*/
					if ( el.getElement('.photo').getElement('div.wrapper').hasClass('main') ) {
						change_main = true;
					}
				}.bind(this));
			}
			
			selection.els.tween({'opacity': [1, 0]}, function () {
				this.mark_photo(selection.els, ( type == 'private' ? 'hard' : 'private' ));
				this.tabs.activate(dest_tab);
				selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
				selection.els.inject(this.content.getElement('.photos-' + dest_tab.toLowerCase()).getElement('.clear'), 'before');
				selection.els.tween({'opacity': [0, 1]});
				
				if ( change_main ) {
					this.mark_photo(this.content.getElement('.photos-' + type).getFirst('li'), 'main');
				}
				
				window.fireEvent('resize');
			}.bind(this));
		}.bind(this));
	}.bind(this);
	
	// Refactored function for make_public_private.
	this.actions.changeType = function (from, to) {
		var dest_tab = to.charAt(0).toUpperCase() + to.slice(1);
		var selection = this.get_selection(from);
		
		var change_main = false;
		if ( from == 'public' ) {
			selection.els.each(function (el) {
				if ( el.getElement('.photo').getElement('div.wrapper').getParent('td').hasClass('main') ) {
					change_main = true;
				}
			}.bind(this));
		}
		
		this.request_action(to, selection, function (resp) {
			
			selection.els.tween({'opacity': [1, 0]}, function () {
				switch ( to ) {
					case 'public' :
						this.mark_photo(selection.els, 'hard');
						break;
					case 'public-soft':
						this.mark_photo(selection.els, 'soft');
						dest_tab = 'Public';
						to = 'public';
						break;
					case 'private' :
						this.mark_photo(selection.els, 'private');
						break;
					case 'disabled' :
						this.mark_photo(selection.els, 'private');
						break;
					case 'archived' :
						this.mark_photo(selection.els, 'archived');
						break;
				}
				this.tabs.activate(dest_tab);
				selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
				selection.els.inject(this.content.getElement('.photos-' + to).getElement('.clear'), 'before');
				selection.els.tween({'opacity': [0, 1]});
				
				if ( change_main ) {
					this.mark_photo(this.content.getElement('.photos-' + from).getFirst('li'), 'main');
				}
				
				window.fireEvent('resize');
			}.bind(this));
		}.bind(this));
	}.bind(this);
	

	this.actions.select_all = function (tab) {
		$$('#' + tab + ' .photos input[type=checkbox]').each(function(it){
			if ( it.get('checked') ) {
				it.set('checked', '');
			}
			else {
				it.set('checked', 'checked');
			}
		});
		/*this.request_action('verified', selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, 'p100');
			}.bind(this));

			selection.els.each(function (el) { el.getElements('input[type=checkbox]').set('checked', null); });
		}.bind(this));*/
	}.bind(this);

	this.actions.select_not_approved = function (tab) {
		$$('#' + tab + ' .photos input[type=checkbox]').each(function(it){
			if( $defined(it.getParent('div.wrapper').getElements('div.not_approved')[0]) ) {
				if ( it.get('checked') ) {
					it.set('checked', '');
				}
				else {
					it.set('checked', 'checked');
				}
			}
		});
		/*this.request_action('verified', selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, 'p100');
			}.bind(this));

			selection.els.each(function (el) { el.getElements('input[type=checkbox]').set('checked', null); });
		}.bind(this));*/
	}.bind(this);
	
	this.actions.toggle_verified = function () {
		var selection = this.get_selection();
		
		this.request_action('verified', selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, 'p100');
			}.bind(this));
			
			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);
	
	this.actions.toggle_retouch = function () {
		var selection = this.get_selection();
		
		if (!selection.ids.length){
			var tab = this.active_tab;
			if($$('.photos-'+ tab).getElements('div.retouch')[0].length){
				$$('.photos-'+ tab +' input[type=checkbox]').each(function(it){
					if( $defined(it.getParent('div.wrapper').getElements('div.retouch')[0]) ) {
						it.set('checked', 'checked');
						
					}
				});
				return;
			}
			
		}
				
		this.request_action('retouch', selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, 'retouch');
			}.bind(this));
			
			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);

	this.actions.toggle_approve = function () {
		var selection = this.get_selection();
		
		this.request_action('approve', selection, function (resp) {
			selection.els.each(function (el) {
				if ( el.getElement('.wrapper').getElement('.not_approved') ) {
					el.getElement('.wrapper').getElement('.not_approved').destroy();
				}
				else {
					this.mark_photo(el, 'not_approved');
				}
			}.bind(this));

			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);

	this.actions.double_approve = function () {
		var selection = this.get_selection();

		this.request_action('double_approve', selection, function (resp) {
			selection.els.each(function (el) {
				if ( el.getElement('.wrapper').getElement('.auto_approved') ) {
					el.getElement('.wrapper').getElement('.auto_approved').destroy();
				}
				
			}.bind(this));

			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
			
		}.bind(this));
	}.bind(this);
	
	this.actions.set_main = function () {
		var selection = this.get_selection();

		if ( ! selection.els[0].getElement('.wrapper').getElement('.not_approved') ) {
			this.request_action('set-main', selection, function (resp) {
				this.mark_photo(selection.els[0], 'main'); 

				selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
			}.bind(this));
		}
		else {
			alert('You can\'t use unapproved photo for main image !');
		}
	}.bind(this);
	
	this.actions.set_rotatable = function () {
		var selection = this.get_selection();
		var tab = this.active_tab;		
		this.content.getElement('.photos-' + tab).getElements('li').each(function (el) {
				el.getElement('td').removeClass('rotatable');
				el.data.is_rotatable = 0;
			
		});
		this.request_action('rotatable', selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, 'rotatable');
			}.bind(this));
			
			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);
	
	this.actions.make_soft_hard = function (type) {
		var selection = this.get_selection();
		this.request_action(type, selection, function (resp) {
			selection.els.each(function (el) {
				this.mark_photo(el, type);
			}.bind(this));
			
			selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
		}.bind(this));
	}.bind(this);
	
	this.actions.remove = function () {
		var selection = this.get_selection();
		
		this.request_action('remove', selection, function (resp) {
			selection.els.each(function (el) {
				el.getElements('input[type=checkbox]').set('checked', null);
			}.bind(this));
			
			selection.els.tween({'opacity': [1, 0], 'width': 0}, function () {
				this.sortables.removeItems.pass(selection.els, this.sortables).call();
				selection.els.destroy();
			}.bind(this));
		}.bind(this));
	}.bind(this);
	
	this.actions.save_order = function () {
		var selection = {ids: []};
		var tab = this.active_tab;
		
		this.content.getElement('.photos-' + tab).getElements('input[type=checkbox]').each(function (el) {
			selection.ids.include(el.get('value'));
		});
		
		this.request_action('sort', selection, function () {
			
		});
	}.bind(this);
	
	this.actions.set_gotd = function () {
		var selection = this.get_selection();

		if ( ! selection.els[0].getElement('.wrapper').getElement('.not_approved') ) {
			this.request_action('set-gotd', selection, function (resp) {
				this.mark_photo(selection.els[0], 'gotd'); 

				selection.els.each(function (el) {el.getElements('input[type=checkbox]').set('checked', null);});
			}.bind(this));
		}
		else {
			alert('You can\'t use unapproved photo for gotd image !');
		}
	}.bind(this);
	
	this.request_action = function (action, selection, callback) {
		
		if ( ! selection.ids.length ) {
			alert('Please select some photos and then continue');
			return;
		}
		
		if ( action == 'remove' && ! confirm('Are you sure you want delete these photos?') ) {
			return;
		}
		
		this.overlay.disable();
		
		var ids = [];
		for ( i = 0; i < selection.ids.length; i++ ) {
			ids.include('photos[]=' + selection.ids[i]);
		}
		
		if(action == 'public-soft'){
			action = 'public';
		}
		new Request({
			url: '/classified-ads/set-crop-args?act=' + action + '&' + ids.join('&'),
			method: 'get',
			onSuccess: function (resp) {
				(callback.bind(this))(resp);
				this.overlay.enable();

				if ( action == 'approve' ) {
					this.popup.close();
				}

			}.bind(this)
		}).send();
	};
	
	this.get_selection = function (tab) {
		if ( ! $defined(tab) ) {
			var tab = this.active_tab;
		}
		
		var chks = this.content.getElement('.photos-' + tab);
		
		var result = {ids: [], els: $$()};
		
		chks.getElements('input[type=checkbox]').each(function (chk) {
			if ( chk.get('checked') ) {
				result.ids.include(chk.get('value'));
				result.els.include(chk.getParent('li'));
			}
		});
		
		result.els.tween = function (opt, callback) {
			var opts = {};
			for ( var i = 0; i < result.els.length; i++ ) {
				opts[i] = opt;
			}
			
			new Fx.Elements(result.els, {
				onComplete: callback
			}).start(opts);
		}
		
		return result;
	};
	
	

		
	window.fireEvent('resize');
};

var Cropper = new Class({
	Implements: [Events],
	el: null,
	els: {},
	bind: {},
	wrap:null,
	disabled: false,
	max: {x: 0, y: 0},
	mouse: {start: {x: 0, y: 0}, now: {x: 0, y: 0}, diff: {x: 0, y: 0}},

	moved: false,

	reinitLimits: function () {
		this.max = {
			x: this.els.img.getWidth() - this.el.getWidth(),
			y: this.els.img.getHeight() - this.el.getHeight()
		};
		
	},

	initialize: function (el) {
		this.wrap = $(el);
		this.el = $(el).getElement('.border');
		this.els.img = $(el).getElement('img');
	
		this.els.img.ondragstart = function () {return false;};

	

		
		var initial = el.data.args;

		this.els.img.onload = function () {
		this.bind = {
				start: this.handleStart.bindWithEvent(this),
				move: this.handleMove.bindWithEvent(this),
				end: this.handleEnd.bindWithEvent(this)	
			};
			if(this.wrap.data.is_portrait==1)
			{	
				var y = this.getHeight()-imageHeight;
				var x= 0;
			}
			else
			{
				var x = this.getWidth()-imageWidth;
				var y= 0;
			}
			this.max = {
				x:x,
				y:y
			};

			this.init();
			if ( initial ) {
				
				this.setInitial(initial.x, initial.y);
			}
			this.el.setStyles({'width':imageWidth-4,'height':imageHeight-4});	
		}.bind(this);
		
	},

	init: function () {
		this.el.addEvent('mousedown', this.bind.start);
	},

	handleStart: function (e) {
		e.stop();

		if ( this.disabled ) return;
		
		document.addEvent('mousemove', this.bind.move);
		document.addEvent('mouseup', this.bind.end)
		
		this.mouse.start = e.page;

		this.mouse.start.x -= this.mouse.diff.x;
		this.mouse.start.y -= this.mouse.diff.y;
	},

	handleMove: function (e) {
		this.moved = true;
		
		this.mouse.now = e.page;

		var x = this.mouse.now.x - this.mouse.start.x,
			 y = this.mouse.now.y - this.mouse.start.y;
		
		this.mouse.diff = {x: x, y: y};

		if ( this.mouse.diff.x < 0 ) this.mouse.diff.x = 0;
		if ( this.mouse.diff.y < 0) this.mouse.diff.y = 0;

		if ( this.mouse.diff.x >= this.max.x) {
			this.mouse.diff.x =  this.max.x;
		}

		if (this.mouse.diff.y >= this.max.y ) {
			this.mouse.diff.y = this.max.y;
		}
		
		this.update();
	},

	handleEnd: function (e) {
		document.removeEvent('mousemove', this.bind.move);
		document.removeEvent('mouseup', this.bind.end);

		if ( this.moved ) {
			this.fireEvent('complete', [this.mouse.diff]);
		}
	},

	update: function () {
		this.mouse.diff.x =  Math.abs(this.mouse.diff.x);
		this.mouse.diff.y = Math.abs(this.mouse.diff.y);
		
		this.el.setStyles({
			left: this.mouse.diff.x,
			top: this.mouse.diff.y
		});
		this.el.getElement('div.border_bg').setStyles({
			'background-position':(-this.mouse.diff.x-2)+'px'+' '+(-this.mouse.diff.y-2)+'px'
		})
	},

	enable: function () {
		this.wrap.setStyles({'opacity': 1,'cursor':'auto'});
		this.el.setStyles({'cursor':'move'});
		this.disabled = false;
	},

	disable: function () {
		this.wrap.setStyles({'opacity': 0.5,'cursor':'wait'});
		this.el.setStyles({'cursor':'wait'});
		this.disabled = true;
	},

	revert: function () {
		this.mouse.diff = this.initial;
		this.update();
	},

	initial: {},


	setInitial: function (x, y) {

		this.mouse.diff = {x: x, y: y};
		this.initial = {x: x, y: y};

		this.update();
	},
	getHeight:function()
	{	
			var ratio =this.els.img.width/this.els.img.naturalWidth; 
		return this.els.img.naturalHeight * ratio;
	},
	getWidth:function()
	{	var ratio =this.els.img.height/this.els.img.naturalHeight; 
		return this.els.img.naturalWidth * ratio; 
	
	}
});

///////////////////////////////////////////
var saveAdjustment = function (args) {
	this.disable();

	args = $extend({
		px: args.x /imageWidth,
		py: args.y / imageHeight,
		'rwidth':imageWidth,
		'rheight':imageHeight
	}, args);
	console.log(this.wrap.data);
	var resize = this.wrap.data.is_portrait==0?this.els.img.width:this.els.img.height;
	new Request({
		url: '/classified-ads/set-crop-args',
		method: 'get',
		data: $extend({act: 'adjust', photo_id:this.wrap.data.id,'resize':resize}, args),
		onSuccess: function (resp) {
			resp = JSON.decode(resp);

			if ( resp.error ) {
				alert('An error occured');
				this.revert();
			}
			else {
				this.setInitial(args.x, args.y);
			}

			this.enable();
		}.bind(this)
	}).send()
};
//////////////////////////////