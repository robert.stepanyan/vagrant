Cubix.Grid = new Class({
	Implements: [Events, Options],
	
	el: null,
	sortField: '',
	sortDir: 'asc',
	count: 0,
	page: 1,
	perPage: 10,
	
	portioningBtns: new Array(),
	_odd: false,

	options: {
		preload: true,
		classPrefix: 'cx-grid-',
		imgUrl: '',
		portioning: true,
		perPages: [10, 25, 50, 100],
		noDataText: 'There is no data, try changing filter'
	},
	
	initialize: function (el, options) {
		this.el = $(el);
		
		options = options || {};
		this.setOptions(options);

		if ( $defined(this.options.id) ) {
			Cubix.Grid.add(this.options.id, this);
		}
		if ( $defined(this.options.perPage) ) {
			this.perPage = this.options.perPage
		}
		this.sortField = this.options.sort.field;
		this.sortDir = this.options.sort.dir;
		
		this._decorate(this.el);
		
		this._initSorting();
		this._initPortioning();

		this.options.data.addEvent('load', function (data) {
			this.tbody.empty();

			this.data = data;
			
			if (data.count > 50000) data.count = 50000;

			if ( ! data ) {
				this.overlay.enable();
				return;
			}
			
			if ( data.data.length ) {
				data.data.each(function (row) {
					this._insertRow(new Hash(row));
				}.bind(this));
			}
			else {
				var tr = new Element('tr', {onMouseEnter: function (e) {e.stop()}}).inject(this.tbody);
				new Element('td', {
					colspan: this.options.header.length + this.options.actions.length,
					html: '<div class="cx-grid-alert">' + this.options.noDataText + '</div>'
				}).inject(tr);
			}
			
			if ( this.options.portioning && $defined(data.count) ) {
				this.portionEl.empty();
				
				for (var i = 0; i < Math.ceil(data.count / this.perPage); i++) {
					var title = (i * this.perPage + 1) + '-' + ((i + 1) * this.perPage < data.count ? (i + 1) * this.perPage : data.count);
					title += '&nbsp;&nbsp;';
					new Element('option', {value: i + 1, html: title}).inject(this.portionEl);
				}
				
				this.portionEl.set('value', this.page);
				
				if ( this.page > 1 ) {
					this.portioningBtns[0].removeClass('first-p');
					this.portioningBtns[1].removeClass('prev-p');
				}
				else {
					this.portioningBtns[0].addClass('first-p');
					this.portioningBtns[1].addClass('prev-p');
				}
				
				if ( this.page < Math.ceil(data.count / this.perPage) ) {
					this.portioningBtns[2].removeClass('next-p');
					this.portioningBtns[3].removeClass('last-p');
				}
				else {
					this.portioningBtns[2].addClass('next-p');
					this.portioningBtns[3].addClass('last-p');
				}
				
				this.curPageEl.set('html', this.page);
			}

			if (data.sum && $('lbl-sum'))
			{
				$('lbl-sum').set('html', data.sum);
			}

			var self = this;
			this.tbody.getElements('td').addEvent('dblclick', function (e) {
				e.stop();
				self.fireEvent('rowdblclick', [this.getParent('tr').rowData]);
			});

			this.tbody.getElements('td').addEvent('contextmenu', function (e) {
				self.tbody.getElements('tr.active').removeClass('active');
				this.getParent('tr').addClass('active');
				
				self.fireEvent('rightclick', [this.getParent('tr').rowData, e]);
			});

			this.fireEvent('loaded');
			
			this.overlay.enable();
		}.bind(this));

		if ( this.options.preload ) {
			this.load();
		}
	},
	
	_decorate: function (el) {
		el.addClass(this.options.classPrefix.substring(0, this.options.classPrefix.length - 1));
		
		var table = new Element('table', {
			
		}).inject(el);
		
		var thead = new Element('thead').inject(table);
		this.thead = thead;
		thead = new Element('tr').inject(thead);

		var tbody = new Element('tbody').inject(table);
		
		this.tbody = tbody;
		
		var tfoot = new Element('tfoot').inject(table);
		
		this.options.actions.each(function (action) {
			if(action.type!=null)
				new Element('th', {'class': 'min'}).inject(thead);
		});
		
		this.options.header.each(function (th) {
			var cell = new Element('th', {
				/*'class': (th.type ? this.options.classPrefix + 'type-' + th.type : '')*/
			}).inject(thead);
			
			cell.field = th.field;
			if ( $chk(th.sortField) ) cell.sortField = th.sortField;

			if ( th.width ) cell.setStyle('width', th.width);

            if ( th.display ) cell.setStyle('display', th.display);
			
			if ( ! th.sortable ) {
				cell.set('html', th.title);
			}
			else {
				cell.addClass('sort');

				var curSortField = th.field;
				if ( $chk(th.sortField) ) curSortField = th.sortField;
				
				if ( this.sortField == curSortField ) {
					cell.addClass('sort-' + (this.sortDir == 'desc' ? 'up' : 'down'));
				}
				
				new Element('span', {
					html: th.title
				}).inject(cell);
				
				new Element('a').inject(cell);
			}
		}.bind(this));
		
		if ( this.options.portioning ) {
			var tfoot = new Element('tfoot').inject(table);
			tfoot = new Element('tr').inject(tfoot);
			tfoot = new Element('td', {colspan: thead.getElements('th').length}).inject(tfoot);
			var p = new Element('div', {'class': 'portion'}).inject(tfoot);
			
			this._insertFlEl(p, 'Page:');
			this.curPageEl = this._insertFlEl(p, '1', 'cur-page');
			this._insertFlEl(p, '|');
			this._insertFlEl(p, 'Size:');
			this.perPageEl = this._insertFlEl(p, new Element('select'));
			
			this.options.perPages.each(function (perPage) {
				new Element('option', {value: perPage, html: perPage + '&nbsp;&nbsp;'}).inject(this.perPageEl);
			}.bind(this));
			
			var per_page = this._getCookie('per-page');
			if ( per_page ) {
				this.perPageEl.set('value', per_page);
				this.perPage = per_page;
			}
			
			
			this._insertFlEl(p, '|');
			this._insertFlEl(p, 'Portion:');
			this.portioningBtns.include(this._insertFlEl(p, new Element('a', {'class': 'first first-p'})));
			this.portioningBtns.include(this._insertFlEl(p, new Element('a', {'class': 'prev prev-p'})));
			this.portionEl = this._insertFlEl(p, new Element('select'));
			this.portioningBtns.include(this._insertFlEl(p, new Element('a', {'class': 'next next-p'})));
			this.portioningBtns.include(this._insertFlEl(p, new Element('a', {'class': 'last last-p'})));
			
			new Element('div', {'class': 'clear'}).inject(p);
		}
		
		this.overlay = new Cubix.Overlay.Box(el);
		
		this.colCount = this.options.actions.length + this.options.header.length;
	},
	
	_insertRow: function (rowData) {
		this._odd = ! this._odd;
		
		var tr = new Element('tr').addEvent('click', function () {
			this.tbody.getElements('tr.active').removeClass('active');
			tr.addClass('active');
		}.bind(this));
		
		tr.rowData = rowData;
		
		if ( this._odd ) tr.addClass('odd');
		
		this.options.actions.each(function (action) {
			if(action.type)
			{	
				var td = new Element('td', {'class': 'min'}).inject(tr);
			
				if ($defined(action.renderer) && $type(action.renderer) == 'function' ) {
					action.renderer(rowData, action, td).inject(td);
				}
				else {
					var act = new Element('a', {'class': 'sbtn icon-' + action.type}).inject(td);
					if ( action.func ) {
						act.addEvent('click', function () {
							(action.func.bind(this))(rowData, act);
						}.bind(this));
					}

					if ( action.mouseenter ) {
						act.addEvent('mouseenter', function () {
							(action.mouseenter.bind(this))(rowData, act);
						}.bind(this));
					}

					if ( action.mouseleave ) {
						act.addEvent('mouseleave', function () {
							(action.mouseenter.bind(this))(rowData, act);
						}.bind(this));
					}
				}
			}
		}.bind(this));
		
		this.options.header.each(function (th) {
			var td = new Element('td', {
				'class': 'cx-grid-type-' + th.type
			});
			
			td.inject(tr);
			
			var sClass = th.type.substr(0, 1).toUpperCase() + th.type.substring(1);
			sClass = 'Cubix.Grid.DataTypes.' + sClass;
			var obj;
			eval('if($defined(' + sClass + '))obj=' + sClass);
			
			var value = rowData.get(th.field);
			
			if ( obj ) {
				value = (obj.format.bind(this))(value, th.format, td);
			}
			
			if ( value ) {
				td.set('html', value);
			}
		}.bind(this));
		
		tr.inject(this.tbody);
		this.fireEvent('rowInserted', [this, tr, tr.rowData]);
	},
	
	_initSorting: function () {
		var self = this;
		this.thead.getElements('th.sort').addEvent('click', function () {
			var sortField = this.field;
			if ( $chk(this.sortField) ) sortField = this.sortField;

			if ( self.sortField == sortField ) {
				if ( self.sortDir == 'asc' ) {self.sortDir = 'desc';this.removeClass('sort-down').addClass('sort-up');}
				else {self.sortDir = 'asc';this.removeClass('sort-up').addClass('sort-down');}
			}
			else {
				self.thead.getElements('.sort-up, .sort-down').removeClass('sort-up').removeClass('sort-down');
				this.addClass('sort-down');
				self.sortField = sortField;
				self.sortDir = 'asc';
			}
			
			self.load();
		});
	},
	
	_initPortioning: function () {
		if ( ! this.options.portioning ) return;
		
		this.portionEl.addEvent('change', function () {
			this.page = this.portionEl.get('value');
			this.load();
		}.bind(this));
		
		this.perPageEl.addEvent('change', function () {
			this.perPage = this.perPageEl.get('value');
			this._setCookie('per-page', this.perPage);
			this.page = 1;
			this.load();
		}.bind(this));
		
		var self = this;
		$$(this.portioningBtns).addEvent('click', function () {
			if ( this.get('class').substring(this.get('class').length - 1, this.get('class').length) == 'p' ) return;
			if ( this.hasClass('next') ) {
				self.portionEl.selectedIndex++;
			}
			else if ( this.hasClass('prev') ) {
				self.portionEl.selectedIndex--;
			}
			else if ( this.hasClass('first') ) {
				self.portionEl.selectedIndex = 0;
			}
			else if ( this.hasClass('last') ) {
				self.portionEl.selectedIndex = self.portionEl.options.length - 1;
			}
			self.page = self.portionEl.get('value');
			self.load();
		});
	},
	
	_insertFlEl: function (into, content, cls) {
		var el = new Element('div', {'class': 'fleft' + (cls ? ' ' + cls : '' )}).inject(into);
		if ( $type(content) == 'element' ) {
			content.inject(el);
			return content;                                                                      
		}
		else {
			el.set('html', content);
		}
		return el;
	},

	overlayDisable: function() {
		this.overlay.disable();
	},

	overlayEnable: function() {
		this.overlay.enable();
	},
	
	load: function () {
		this.overlay.disable();
		
		this.fireEvent('beforeload');
		
		this.options.data.load({
			sort_field: this.sortField,
			sort_dir: this.sortDir,
			page: this.page,
			per_page: this.perPage
		});
	},
	
	_getCookieKey: function (param) {
		var uri = new URI(window.location.href);
		var key = uri.get('directory') + uri.get('file');
		key = key.replace(/[^a-z0-9]/g, '-').replace(/^\-/, '').replace(/\-$/, '');
		
		return 'cx-grid-' + key + '-' + param;
	},
	
	_getCookie: function (param) {
		return Cookie.read(this._getCookieKey(param));
	},
	
	_setCookie: function (param, value) {
		Cookie.write(this._getCookieKey(param), value, {path: '/'});
	},

	setPage: function (page) {
		this.page = page;
		this.portionEl.set('value', this.page);
	}
});

Cubix.Grid.Loader = new Class({
	Implements: [Events, Options],
	
	options: {
		url: '',
		method: 'get',
		params: {}
	},
	
	initialize: function (options) {
		options = options || {};
		
		this.setOptions(options);
	},
	
	load: function (params) {
		params = params || {};
		
		this.fireEvent('beforeload');
		
		if ( $type(this.options.params) == 'function' ) {
			var result = this.options.params.call();
			params = $merge(params, result);
		}
		else {
			params = $merge(params, this.options.params);
		}
		
		new Request({
			url: this.options.url,
			method: this.options.method,
			onSuccess: this.onLoaded.bind(this),
			data: params
		}).send();
	},
	
	onLoaded: function (resp) {
		if ( 'string' == $type(resp) ) {
			resp = JSON.decode(resp);
		}
		var id = $(Cubix.Grid.Instance.el).get('id');
		if ( $defined($('grid-show-count')) ) {
			$('grid-show-count').getElement('span').set('html', resp.count);
		}
		else if ($(id+'-show-count')) 
		{	
			$(id+'-show-count').getElement('span').set('html', resp.count);
		}
		if ( $defined($('grid-show-amount')) ) {
			$('grid-show-amount').getElement('span').set('html', resp.turnover);
		}
		
		if ( $defined($('grid-show-count-members')) ) {
			$('grid-show-count-members').getElement('span').set('html', resp.dif_members_count);
		}
		
		this.fireEvent('load', resp);
	}
});

Cubix.Grid.Loader.Static = new Class({
	Extends: Cubix.Grid.Loader,

	options: {
		data: null
	},

	load: function (params) {
		params = params || {};
		
		this.fireEvent('beforeload');
		
		this.onLoaded(this.options.data);
	}
});


/* Grids container implementation */
Cubix.Grid._grids = new Hash();
Cubix.Grid.get = function(id) {
	return Cubix.Grid._grids.get(id);
};
Cubix.Grid.add = function (id, instance) {
	Cubix.Grid._grids.set(id, instance);
};





Cubix.Grid.DataTypes = {};

Cubix.Grid.DataTypes.Real = {
	format: function (data) {
		return data ? data : '0';
	}
}

Cubix.Grid.DataTypes.String = {
	format: function (data) {
		return data && data.length ? data : '-';
	}
}

Cubix.Grid.DataTypes.Date = {
	format: function (data, format) {
		if ( ! data ) return '-';
		var date = new Date();
		date.setTime(data * 1000);
		
		format = format || '%d %b %Y';
		
		return date.format(format);
	}
};

Cubix.Grid.DataTypes.DateTime = {
	format: function (data) {
		return Cubix.Grid.DataTypes.Date.format(data, '%d %b %Y, %H:%M');
	}
}

Cubix.Grid.DataTypes.DateTimeTimezoneDiff = {
	format: function (data) {
		var date = new Date();
		var diff = date.getTimezoneOffset();	
		return Cubix.Grid.DataTypes.Date.format(data ? (data + diff*60) : data, '%d %b %Y, %H:%M');
	}
}

Cubix.Grid.DataTypes.Time = {
	format: function (data) {
		return Cubix.Grid.DataTypes.Date.format(data, '%H %b %s');
	}
}



Cubix.Grid.DataTypes.Currency = {
	format: function (data, format) {
		if ( ! data ) return '-';		

		return data + ' ' + format.currency;
	}
}

Cubix.Grid.DataTypes.RealCurrency = {
	format: function (data, format, td) {
		
		row_data = td.getParent().rowData;

		var str = '-';
		var bal = '';
		if ( row_data.status == 'paid' ) {
			bal = '<p style="font-size:10px;">From Balance: 0' + format.currency + '</p>';
			if ( row_data.real_payment )
				bal = '<p style="font-size:10px;">From Balance: ' + row_data.real_payment + format.currency + '</p>';
			
			str = '<p style="font-size:10px;">Real: ' + (row_data.price - row_data.real_payment) + format.currency + '</p>';
		}
		
		return bal + str;
	}
}

Cubix.Grid.DataTypes.Amount = {
	format: function (data, format, td) {
		if ( ! data ) return '-';
		td.setStyle('text-align', 'right');
		return parseFloat(data) / 100;
	}
}

Cubix.Grid.DataTypes.InformMethod = {
	format: function (data) {
		if(data == 1)
			return 'E-mail';
		else
			return 'SMS';
	}
}

Cubix.Grid.DataTypes.AgencyEscorts = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		
		if( data )
			ret = '<a href="#" rel="' + row_data.id + '" class="agency-escorts" >(' + data + ')</a>';
		else
			ret = '-';
		
		return ret;
	}
}

Cubix.Grid.DataTypes.FakeFree = {
	format: function (data, format, td) {
		td.setStyle('text-align', 'center');
		
		if (data == 0)
			ret = '<img src="/img/famfamfam/minus.png"/>';
		else
			ret = '<img src="/img/famfamfam/tick.png"/>';

		return ret;
	}
}

Cubix.Grid.DataTypes.IsSuspicious = {
	format: function (data, format, td) {
		td.setStyle('text-align', 'center');

		if (data == 0)
			ret = '<img src="/img/famfamfam/minus.png"/>';
		else
			ret = '<img src="/img/famfamfam/tick.png"/>';

		return ret;
	}
}

Cubix.Grid.DataTypes.DueDate = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		var today = new Date().set({hr: 0, min: 0, sec: 0, ms: 0});
		var date = new Date().setTime(row_data.due_date * 1000);
		//console.log(date);
		//today.diff(date, 'second') < 3600 * 24
		var diff = today.diff(date, 'day');
		diff--;
		var day_s = 'day';

		if ( Math.abs(diff) > 1 ) {
			day_s = 'days';
		}

		if( diff < 0 )
			ret = '<strong style="color: red">' + diff + ' ' + day_s + '</strong>';
		else if ( diff > 0 )
			ret = '<strong>+' + diff + ' ' + day_s + '</strong>';
		else {
			ret = '<strong style="color: red">Today</strong>';
		}

		return ret;
	}
}

Cubix.Grid.DataTypes.UserName = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		if( data )
			ret = '<a href="#" rel="' + row_data.id + '" class="username" >' + data + '</a>';
		else
			ret = '-';

		return ret;
	}
}

Cubix.Grid.DataTypes.IssueSubject = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		if( data )
			ret = '<a href="#" rel="' + row_data.id + '" class="subject" >' + data + '</a>';
		else
			ret = '-';

		return ret;
	}
}

Cubix.Grid.DataTypes.IssueStatus = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		switch (row_data.status)
		{
			case 'opened':
				td.getParent('tr').setStyle('background-color', '#FFEFF2');
				td.setStyle('text-align', 'center');
				//return '<img src="/img/famfamfam/exclamation.png" height="16" width="16" />';
			break;
			case 'closed':
				td.getParent('tr').setStyle('background-color', '#EFFFEF');
				td.setStyle('text-align', 'center');
				//return '<img src="/img/famfamfam/accept.png" height="16" width="16" />';
			break;
			case 'disabled':
				td.getParent('tr').setStyle('background-color', '#fadef4');
				td.setStyle('text-align', 'center');
				//return '<img src="/img/famfamfam/accept.png" height="16" width="16" />';
			break;
		}
		switch (data)
		{
			case 1 : // New Unread
				td.getParent('tr').setStyle('font-weight', 'bold');
				return '<img src="/img/famfamfam/new.png" height="16" width="16" title="New Unread"/>';
				break;
			case 2 : // Not Replied
				return '<img src="/img/famfamfam/accept_orange.png" height="16" width="16" title="Read, not replied" />';
				break;
			case 3 : // Read Replied
				return '<img src="/img/famfamfam/accept.png" height="16" width="16" title="Read, replied"/>';
				break;
			case 4 : // New Reply
				return '<img src="/img/famfamfam/accept_red.png" height="16" width="16" title="New reply waiting"/>';
				break;
			case 5 : // ADMIN SENT
				return '<img src="/img/famfamfam/user_go.png" height="16" width="16" title="Sent by admin"/>';
				break;	
		}
	}
}
Cubix.Grid.DataTypes.PaymentStatus = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		if(row_data.status_success)
		{
			td.getParent('tr').setStyle('background-color', '#EFFFEF');
		}
		else
		{
			td.getParent('tr').setStyle('background-color', '#FFEFF2');
		}
		if(row_data.status_rec)
		{
			td.setStyle('text-align', 'center');
			return '<img src="/img/famfamfam/accept.png" height="16" width="16" />';
		}
		else
		{
			td.setStyle('text-align', 'center');
			return '<img src="/img/famfamfam/accept_passive.png" height="16" width="16" />';
		}
	}
}
Cubix.Grid.DataTypes.TrunsactionStatus = {
	format: function (data, format, td) {

		if(data)
		{
			td.setStyle('text-align', 'center');
			return '<img src="/img/famfamfam/accept.png" height="16" width="16" />';
		}
		else
		{
			td.setStyle('text-align', 'center');
			return '<img src="/img/famfamfam/accept_passive.png" height="16" width="16" />';
		}
	}
}
Cubix.Grid.DataTypes.RecStatus = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		var cls = '';

		if ( ! row_data.is_recurring ) cls = '-p';
		{
			td.setStyle('text-align', 'center');
			return '<a class="sbtn icon-accept' + cls + '" href="/members/toggle-rec?member_id=' + row_data.member_id + '" onclick="return false" style="margin: auto"></a>';
		}
	}
}

Cubix.Grid.DataTypes.RequestStatus = {
	format: function (data) {
		if ( data == 1 )
			return 'PENDING';
		else if ( data == 2 )
			return 'VERIFY';
		else if ( data == 3 )
			return 'CONFIRMED';
		else if ( data == 4 )
			return 'REJECTED';
	}
}

Cubix.Grid.DataTypes.RequestStatusComment = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		
		var comment = "";
		var rejectText = "";
		var reasons = JSON.decode(row_data.reasons);
		
		if (data == 'REJECTED' && row_data.reason_ids)
		{
			rejectText += "Rej. Date: <i>" + row_data.rejected_date + "</i></br>Read Status: <i>" + row_data.is_read + "</i></br><hr><br>";
			
			for (var el in reasons)
			{
				rejectText += reasons[el] + "</br></br>";
			}
									
			comment = '<div class="reject-reason" style="float:left;position:relative"><img style="cursor:pointer" src="/img/famfamfam/asterisk_red.png" /><div class="none" style="position: absolute; top: -5px; left: 40px; width: 200px; background-color: #EEE; padding: 10px; border: 1px solid #FF0000; font-size: 12px; font-weight: bold; color:#FF0000">'+ rejectText +'</div></div>';
		}
					
		return "<div class='fleft'>" + data + "</div>" + comment
	}
}

Cubix.Grid.DataTypes.VipRequestStatusComment = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		var comment = "";
		var status = "";
				
		if (data == 3 && row_data.reject_reason.length > 0)
			comment = '<div class="reject-reason" style="float:left;position:relative"><img style="cursor:pointer" src="/img/famfamfam/asterisk_red.png" /><div class="none" style="position: absolute; top: -5px; left: 40px; width: 200px; background-color: #EEE; padding: 10px; border: 1px solid #FF0000; font-size: 12px; font-weight: bold; color:#FF0000">'+ row_data.reject_reason +'</div></div>';
		
		if ( data == 1 )
			status = 'PENDING';
		else if ( data == 2 )
			status =  'VERIFY';
		else if ( data == 3 )
			status = 'REJECTED';
		else if ( data == 4 )
			status = 'SUSPENDED';
				
		return "<div class='fleft'>" + status + "</div>" + comment
	}
}

Cubix.Grid.DataTypes.AgeVerStatusComment = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		
		var comment = "";
				
		if (data == 'REJECTED' && row_data.reject_reason.length > 0)
		{
			comment = '<div class="reject-reason" style="float:left;position:relative"><img style="cursor:pointer" src="/img/famfamfam/asterisk_red.png" /><div class="none" style="position: absolute; top: -5px; left: 40px; width: 200px; background-color: #EEE; padding: 10px; border: 1px solid #FF0000; font-size: 12px; font-weight: bold; color:#FF0000">'+ row_data.reject_reason +'</div></div>';
			
		}
					
		return "<div class='fleft'>" + data + "</div>" + comment
	}
}

Cubix.Grid.DataTypes.NewsletterStatus = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		td.setStyle('text-align', 'center');
		switch(row_data.status)
		{
			case 1:
				td.getParent('tr').setStyle('background-color', '#FFF7E0');
				return data;
			break;
			case 2:
				td.getParent('tr').setStyle('background-color', '#EFFFEF');
				return data;
			break;
			case 4:
				td.getParent('tr').setStyle('background-color', '#FFEFF2');
				return data;
			break;
			default:
				td.getParent('tr').setStyle('background-color', '#FFF');
				return data;
		}
	}
}

Cubix.Grid.DataTypes.DateOption = {
	format: function (data, format, td) {
		if ( ! data )
			return '-';
		
		var today = new Date().set({hr: 0, min: 0, sec: 0, ms: 0});
		var tomorrow = new Date(today.clone().increment());
		var date = new Date().setTime(data * 1000);
		
		var is_expired = false;
		//Expired
		if ( new Date().diff(date, 'second') < 0 ) {
			td.setStyle('background-color', '#FFBFC8');
			is_expired = true;
		}
		//Today
		else if ( today.diff(date, 'second') < 3600 * 24 ) {
			td.setStyle('background-color', '#BFFFC2');
		}
		//Tomorrow
		else if ( tomorrow.diff(date, 'second') > 0 && tomorrow.diff(date, 'second') < 3600 * 24) {
			td.setStyle('background-color', '#BFC4FF');
		}		
		
		dateTime = Cubix.Grid.DataTypes.Date.format(data, '%d %b %Y, %H:%M');
		if ( format != 8  )
		{
			if ( is_expired )
				ret = dateTime;
			else
				ret = '<label style="display:block; width:140px; margin:auto;"><span style="vertical-align: middle; padding-right: 10px">' + dateTime + '</span> <input style="vertical-align: middle" type="radio" name="date_conf[' + td.getParent().rowData.id + ']" value="' + format + '" /></label>';
			return ret;
		}
		else
		{
			row_data = td.getParent().rowData;
			
			if ( row_data.date_1_is_confirmed == '1' )
				d = row_data.date_1;
			else if ( row_data.date_2_is_confirmed == '1' )
				d = row_data.date_2;
			else if ( row_data.date_3_is_confirmed == '1' )
				d = row_data.date_3;
			
			var today = new Date().set({hr: 0, min: 0, sec: 0, ms: 0});
			var tomorrow = new Date(today.clone().increment());
			var date = new Date().setTime(d * 1000);
			
			
			if ( new Date().diff(date, 'second') < 0 ) {
				td.setStyle('background-color', '#FFBFC8');
			}
			else if ( today.diff(date, 'second') < 3600 * 24 ) {
				td.setStyle('background-color', '#BFFFC2');
			}
			else if ( tomorrow.diff(date, 'second') > 0 && tomorrow.diff(date, 'second') < 3600 * 24 ) {
				td.setStyle('background-color', '#BFC4FF');
			}
			
			return Cubix.Grid.DataTypes.Date.format(d, '%d %b %Y, %H:%M');
		}
		 
	}
}

Cubix.Grid.DataTypes.SmsType = {
	format: function (data) {
		if ( data == 1 )
		{
			return "<img src='/img/cubix-button/icons/msg_get.png' height='16' width='16' title='Incoming Message' alt='Incoming Message' />";
		}
		else if ( data == 2 )
		{
			return "<img src='/img/cubix-button/icons/msg_put.png' height='16' width='16' title='Outgoing Message' alt='Outgoing Message' />";
		}
	}
}

Cubix.Grid.DataTypes.AddPhone = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		
		if ( row_data.phone )
			return '<a href="#" class="add-escort-phone" rel="' + row_data.phone + '">' + row_data.title + '</a>';
		else
			return '<span>' + row_data.title + '</span>';
	}
}

Cubix.Grid.DataTypes.PackageExpDate = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		if( data < 0 ) {
			return "-";
		}

		var today = new Date().set({hr: 0, min: 0, sec: 0, ms: 0})/*.format('%d %b %Y')*/;
		var exp_date = Cubix.Grid.DataTypes.Date.format(data, '%d %b %Y');

		if ( today.diff(exp_date) < 0 ) {
			return '<span style="background-color: #FF3030; color: #fff; padding: 2px 4px; font-weight: bold">' + exp_date + '</span>'
		}

		return exp_date;
	}
}

Cubix.Grid.DataTypes.PackageProducts = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		var products = '';

		if ( data.length == 0 ) {
			return '-';
		}

		data.each(function(it){
			products += '<p style="font-size: 10px; color: #5B5B5B; font-weight:bold">' + it.name + '</p>';
		});

		return products;
	}
}

Cubix.Grid.DataTypes.AddPackage = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		return '<a href="#" style="margin: auto" class="sbtn icon-add add-package" rel="' + row_data.id + '"></a>';
	}
}

Cubix.Grid.DataTypes.StrDefis = {
	format: function (data, format, td) {
		
		if ( data )
			return data;
		else
			return '-';
	}
}

Cubix.Grid.DataTypes.ApplicationFlag = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		td.setStyle('text-align', 'center');
		return row_data.app_iso ? '<img src="/img/preview/icon_flag_' + row_data.app_iso + '.gif" title="' + data + '" alt="' + data + '" />' : '-';
	}
}


Cubix.Grid.DataTypes.Link = {
	format: function (data) {
		return '<a href="' + data + '" target="_blank">' + data + '</a>';
	}
}


Cubix.Grid.DataTypes.VideoDownloadBtn = {
	format: function (data) {
		return '<a href="' + data + '" class="video-download-btn" target="_blank">Download</a>';
	}
}

Cubix.Grid.DataTypes.LinkWebsite = {
	format: function (data) {
		return '<a href="//' + data + '" target="_blank">' + data + '</a>';
	}
};

Cubix.Grid.DataTypes.Email = {
	format: function (data) {

		if ( data )
			return '<a href="mailto:' + data + '">' + data + '</a>';
		else
			return '-';
	}
}

Cubix.Grid.DataTypes.Gendername = {
	format: function (data) {
		if ( data )
		{var sex='-';
			if(data==1)
			sex='Female';
			else if(data==2)
			sex='Male';
			else if(data==3)
			sex='Trans';
			return sex;
		}
		else
			return '-';
	}
}
Cubix.Grid.DataTypes.Checkbox = {
	format: function (data, format, td) {
	
		row_data = td.getParent().rowData;
		
		return '<input type="checkbox" name="selected[]" value="' + row_data.id + '" />';
	}
}

Cubix.Grid.DataTypes.CheckboxFeedback = {
	format: function (data, format, td) {
	
		row_data = td.getParent().rowData;
		
		if (data == 2)
			return '<input type="checkbox" name="selected[]" value="" disabled="disabled" />';
		else
			return '<input type="checkbox" name="selected[]" value="' + row_data.id + '" />';
	}
}

Cubix.Grid.DataTypes.CheckboxBillingPackages = {
	format: function (data, format, td) {
	
		row_data = td.getParent().rowData;
		
		return '<input type="checkbox" name="selected[]" value="' + row_data.escort_id + '" />';
	}
}

Cubix.Grid.DataTypes.SentSmsText = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		
		switch (row_data.sms_status)
		{
			/*case 0:
				td.getParent('tr').setStyle('background-color', '#FFF');
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
			break;
			case 1:
				td.getParent('tr').setStyle('background-color', '#FFF');
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
			break;
			case 2:
				td.getParent('tr').setStyle('background-color', '#AAF7AD'); // delivered
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
			break;
			case -1:
				td.getParent('tr').setStyle('background-color', '#FCCCF2');
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
			break;
			case -2:
				td.getParent('tr').setStyle('background-color', '#FCCCF2');
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
			break;
			case -3:
				td.getParent('tr').setStyle('background-color', '#FCCCF2');
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
			break;
			case -4:
				td.getParent('tr').setStyle('background-color', '#FCCCF2');
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
			break;
			case -5:
				td.getParent('tr').setStyle('background-color', '#FCCCF2');
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
			break;
			case -10:
				td.getParent('tr').setStyle('background-color', '#FCCCF2');
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
			break;
			default:
				td.getParent('tr').setStyle('background-color', '#FFF');
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
			break;*/
			case 9:
				td.getParent('tr').setStyle('background-color', '#FFF');
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
				break;
			case 10:
				td.getParent('tr').setStyle('background-color', '#aaf7ad');
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
			break;
			case 4:
				td.getParent('tr').setStyle('background-color', '#0cbc1c');
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
			break;
			case 1:
			case 2:
			case 5:
			case 6:
			case 7:
			case 12:
				td.getParent('tr').setStyle('background-color', '#FCCCF2');
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
			break;
			case 8:
				td.getParent('tr').setStyle('background-color', '#ffce16');
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
				break;
			case 3:
				td.getParent('tr').setStyle('background-color', '#fcab13');
				return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
				break;
			default:
			td.getParent('tr').setStyle('background-color', '#FFF');
			return '<a href="#" id="' + row_data.id + '" class="sms-text">' + data + '</a>';
		}
	}
}

Cubix.Grid.DataTypes.ClAdDisabled = {
	format: function (data, format, td) {
	
		row_data = td.getParent().rowData;
		
		if (row_data.is_disabled == 1) {
			td.getParent('tr').setStyle('background-color', '#ffecec');
			return 'DISABLED';
		} else if ( row_data.status == 2 ) {
			td.getParent('tr').setStyle('background-color', '#EFFFEF');
		}
		
		if (row_data.is_premium == 1) {
			td.getParent('tr').setStyle('background-color', '#ffc06e');
		}
		
		return row_data.status_title;
	}
}

Cubix.Grid.DataTypes.ClAdTransferStatus = {
	format: function (data, format, td) {
	
		row_data = td.getParent().rowData;
		
		if( row_data.status_title == 'CONFIRMED' ) {
			td.getParent('tr').setStyle('background-color', '#EFFFEF');
		} else if ( row_data.status_title == 'REJECTED' ) {
			td.getParent('tr').setStyle('background-color', '#FFEFF2');
		}
		
		return row_data.status_title;
	}
}

Cubix.Grid.DataTypes.ClAdIsPaid = {
	format: function (data, format, td) {
	
		row_data = td.getParent().rowData;
		
		td.setStyle('text-align', 'center');
		
		if ( row_data.is_paid == 1 ) {
			return '<span style="padding:2px 3px; background: green; color:#fff;border-radius: 3px;font-size:11px;font-weight:bold;">' + row_data.is_paid_title + '</span>'
		} else {
			return '<span style="padding:2px 3px; background: red; color:#fff;border-radius: 3px;font-size:11px;font-weight:bold;">' + row_data.is_paid_title + '</span>'
		}
	}
}

Cubix.Grid.DataTypes.FeedbackFlag = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		switch (row_data.flag)
		{
			case 0:
				return '<span>' + row_data.flag_name + '</span>';
			break;
			case 1:
				td.setStyle('background-color', '#FED0D9');
				return '<span>' + row_data.flag_name + '</span>';
			break;
            case 2:
				td.setStyle('background-color', '#BDE5FD');
				return '<span>' + row_data.flag_name + '</span>';
			break;
            case 3:
				return '<span>' + row_data.flag_name + '</span>';
			break;
		}
	}
}

Cubix.Grid.DataTypes.Top10Status = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		
		if (row_data.is_edited)
			td.getParent('tr').setStyle('background-color', '#EFFFEF');
		
		if (row_data.status == 'Hidden')
			td.setStyle('background-color', '#FFECEC');
		
		return row_data.status;
	}
}

Cubix.Grid.DataTypes.FeedbackStatus = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		switch (row_data.status)
		{
			case 1:
				td.getParent('tr').setStyle('background-color', '#FFFFFF');
				return '<span>' + row_data.status_name + '</span>';
			break;
            case 2:
				td.getParent('tr').setStyle('background-color', '#EFFFEF');
				return '<span>' + row_data.status_name + '</span>';
			break;
            case 3:
				td.getParent('tr').setStyle('background-color', '#EEEEEE');
				return '<span>' + row_data.status_name + '</span>';
			break;
		}
	}
}

Cubix.Grid.DataTypes.SmsText = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		
		if ( row_data.is_read )
			return '<a href="#" id="' + row_data.id + '"  class="sms-text">' + data + '</a>';
		else
		{
			td.getParent('tr').setStyle('background-color', '#EFF9FF');			
			return '<a href="#" class="sms-text" id="' + row_data.id + '" style="font-weight:bold" >' + data + '</a>';
		}
	}
}

Cubix.Grid.DataTypes.SmsTextInbox = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		
		if ( row_data.is_read ) {
			var ret = '<a href="#" id="' + row_data.phone_from + '" idd="' + row_data.id + '" class="sms-text">' + data + '</a>';
			if ( row_data.is_sent ) {
				ret += '<span class="icon-replied"></span>';
			}
			return ret;
		} else {
			td.getParent('tr').setStyle('background-color', '#EFF9FF');			
			var ret = '<a href="#" class="sms-text" id="' + row_data.phone_from + '" idd="' + row_data.id + '" style="font-weight:bold" >' + data + '</a>';

			if ( row_data.is_sent ) {
				ret += '<span class="icon-replied"></span>';
			}
			return ret;
		}
	}
}


Cubix.Grid.DataTypes.VerifiedStatus = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		
		var box_class = '';
					
		if (data == 1)
			box_class = 'color-orange';
		else if (data == 2)
			box_class = 'color-green';
		else if (data == 4)
			box_class = 'color-red';
		else
			box_class = 'color-gray';
		
		return '<div class="' + box_class + '"></div>';
	}
}

Cubix.Grid.DataTypes.WikiType = {
	format: function (data, format, td) {
		var typs = new Array();
		typs = data.split(',');
		var ret = '';

		if (inArray('admin', typs) && inArray('sales', typs))
		{
			ret = 'Admin & Sales';
		}
		else if (inArray('admin', typs))
		{
			ret = 'Admin';
		}
		else if (inArray('sales', typs))
		{
			ret = 'Sales';
		}

		return ret;
	}
}

Cubix.Grid.DataTypes.ChatUserRead = {
	format: function (data, format, td) {
		td.setStyle('text-align', 'center');
		
		if (data == 1)
			return '<img src="/img/famfamfam/accept.png" />';
		else
			return '<img src="/img/famfamfam/new.png" />';
	}
}

Cubix.Grid.DataTypes.FaqAnswer = {
	format: function (data, format, td) {
		var txt = data;
		var l = txt.length;

		if (l > 135)
		{
			txt = txt.substr(0, 135) + ' ...';
		}

		return txt;
	}
}

Cubix.Grid.DataTypes.EscortLink = {
	tip: false,
	
	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		if ( ! this.tip ) {
			this.tip = new Cubix.Tip({
				elements: [],
				plugins: ['Fade', {name: 'Shadow', options: {color: 'white-trans'}}, {name: 'AjaxContent', options: {url: '/dashboard/tip-data', params: function () {
					var data = this.el.getParent('tr').rowData;
					return {escort_id: data.id};
				}}}],
				behaviour: 'mouse'
			});
		}

		this.tip.attach(td);

		
		if ( row_data.showname && row_data.status.toLowerCase().indexOf('deleted') == -1 )
			return '<a rel=\'' + row_data.main_photo  +'\' href=\'' + Cubix.Grid.DataTypes.EscortLink.baseUrl + '/' + row_data.showname + '-' + row_data.escort_id + '?f=b' + '\' target=\'_blank\' class="escort-showname">' + row_data.showname + '</a>';
		else if ( row_data.showname && row_data.status.toLowerCase().indexOf('deleted') != -1 )
			return row_data.showname;
		else
			return '-';
		
	}
};

Cubix.Grid.DataTypes.EscortLinkAV = {
	tip: false,
	
	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		if ( ! this.tip ) {
			this.tip = new Cubix.Tip({
				elements: [],
				plugins: ['Fade', {name: 'Shadow', options: {color: 'white-trans'}}, {name: 'AjaxContent', options: {url: '/dashboard/tip-data', params: function () {
					var data = this.el.getParent('tr').rowData;
					return {escort_id: data.id};
				}}}],
				behaviour: 'mouse'
			});
		}

		this.tip.attach(td);

		var color = '';
		if(row_data.need_age_verification == 3){
			color = "green";
		}
		else if(row_data.need_age_verification == 0){
			color = "red";
		}
		else if(row_data.need_age_verification == 4){
			color = "purple";
		}
		else{
			color = 'orange';
		}
		if ( row_data.showname && row_data.status.toLowerCase().indexOf('deleted') == -1 )
			return '<a class="'+ color +' escort-showname" rel=\'' + row_data.main_photo  +'\' href=\'' + Cubix.Grid.DataTypes.EscortLink.baseUrl + '/' + row_data.showname + '-' + row_data.escort_id + '?f=b' + '\' target=\'_blank\' class="escort-showname">' + row_data.showname + '</a><div class="escort-username">'+row_data.username+'</div>';
		else if ( row_data.showname && row_data.status.toLowerCase().indexOf('deleted') != -1 )
			return row_data.showname+'<div class="escort-username">'+row_data.username+'</div>';
		else
			return '-'+'<div class="escort-username">'+row_data.username+'</div>';
		
	}
};

Cubix.Grid.DataTypes.EscortLink2 = {
	tip: false,
	
	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		if ( ! this.tip ) {
			this.tip = new Cubix.Tip({
				elements: [],
				plugins: ['Fade', {name: 'Shadow', options: {color: 'white-trans'}}, {name: 'AjaxContent', options: {url: '/dashboard/tip-data', params: function () {
					var data = this.el.getParent('tr').rowData;
					return {escort_id: data.escort_id};
				}}}],
				behaviour: 'mouse'
			});
		}

		this.tip.attach(td);

		
		if ( row_data.showname )
			return '<a rel=\'' + row_data.main_photo  +'\' href=\'' + Cubix.Grid.DataTypes.EscortLink.baseUrl + '/' + row_data.showname + '-' + row_data.escort_id + '\' target=\'_blank\' class="escort-showname">' + row_data.showname + ' ID:' + row_data.escort_id + '</a>';
		else
			return '-';
		
	}
};

Cubix.Grid.DataTypes.EscortLink3 = {
	tip: false,

	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		td.setStyle('text-align', 'center');

		if ( ! this.tip ) {
			this.tip = new Cubix.Tip({
				elements: [],
				plugins: ['Fade', {name: 'Shadow', options: {color: 'white-trans'}}, {name: 'AjaxContent', options: {url: '/dashboard/tip-data', params: function () {
					var data = this.el.getParent('tr').rowData;
					return {escort_id: data.escort_id};
				}}}],
				behaviour: 'mouse'
			});
		}

		this.tip.attach(td);

		if ( row_data.showname )
		{
			var escort = '<a rel=\'' + row_data.main_photo  +'\' href=\'' + Cubix.Grid.DataTypes.EscortLink.baseUrl + '/' + row_data.showname + '\' target=\'_blank\' class="escort-showname">' + row_data.showname + ' (' + row_data.escort_id + ')</a>';

			if (row_data.escort_reviews_count > 0)
			{
				if (!row_data.escort_reviews_count_approve)
					row_data.escort_reviews_count_approve = 0;
				if (!row_data.escort_reviews_count_disable)
					row_data.escort_reviews_count_disable = 0;
				
				escort += '<br/><span style="font-size: 10px;">(<a target="_blank" href="/reviews?escort_id=' + row_data.escort_id + '" class="esc_revs" rel="' + row_data.escort_id + '">' + row_data.escort_reviews_count + ' reviews</a>)</span>';
				escort += '<br/><span style="font-size: 10px;">(<a href="#" class="esc_revs_app" rel="' + row_data.escort_id + '">' + row_data.escort_reviews_count_approve + ' ok</a>) (<a href="#" class="esc_revs_dis" rel="' + row_data.escort_id + '">' + row_data.escort_reviews_count_disable + ' not</a>)</span>';
			}
			
			if (row_data.escort_last_review_date)
				escort += '<br/><span style="font-size: 10px;">Last ' + row_data.escort_last_review_status + ' review:<br/>' + row_data.escort_last_review_date + '</span>';

			return escort;
		}
		else
			return '-';

	}
};

Cubix.Grid.DataTypes.ReviewMember = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		td.setStyle('text-align', 'center');

		if ( row_data.username )
		{
			var member = row_data.username;

			if (row_data.member_reviews_count > 0)
			{
				if (!row_data.member_reviews_count_approve)
					row_data.member_reviews_count_approve = 0;
				if (!row_data.member_reviews_count_disable)
					row_data.member_reviews_count_disable = 0;
				
				member += '<br/><span style="font-size: 10px;">(<a target="_blank" href="/reviews?username=' + row_data.clear_username + '" class="mem_revs" rel="' + row_data.clear_username + '">' + row_data.member_reviews_count + ' reviews</a>)</span>';
				member += '<br/><span style="font-size: 10px;">(<a href="#" class="mem_revs_app" rel="' + row_data.clear_username + '">' + row_data.member_reviews_count_approve + ' ok</a>) (<a href="#" class="mem_revs_dis" rel="' + row_data.clear_username + '">' + row_data.member_reviews_count_disable + ' not</a>)</span>';
			}
			
			member += '<br/><span style="font-size: 10px;">Signup:<br/>' + row_data.date_registered + '</span>';
			
			if (row_data.member_last_review_date)
				member += '<br/><span style="font-size: 10px;">Last ' + row_data.member_last_review_status + ' review:<br/>' + row_data.member_last_review_date + '</span>';

			var m = member;
			var comment = '<div class="member_comment" style="float:left;position:relative"><img style="cursor:pointer" src="/img/famfamfam/asterisk_red.png" /><div class="none" style="position: absolute; top: -5px; left: 40px; width: 200px; background-color: #EEE; padding: 10px; border: 1px solid #FF0000; font-size: 12px; font-weight: bold; color:#FF0000">'+row_data.member_comment +'</div></div>';
			var premium = '<div style="float:right"><img src="/img/prem.jpg" /></div>'
			if ( row_data.is_premium && row_data.member_comment){
				m =  comment + '<div style="float:left">' + member + '</div>' + premium;
			}
			else if(row_data.is_premium){
				m = '<div style="float:left">' + member + '</div>' + premium;
			}
			else if(row_data.member_comment){
				m = comment + '<div style="float:left">' + member + '</div>';
			}

			return m;
		}
		else
			return '-';

	}
};

Cubix.Grid.DataTypes.CommentMember = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		if ( row_data.username )
		{	
			var m = row_data.username;
			var comment = '<div class="member_comment" style="float:left;position:relative;padding-right:10px"><img alt="" src="/img/famfamfam/asterisk_red.png" /><div class="none" style="position: absolute; top: -5px; left: 40px; width: 200px; background-color: #EEE; padding: 10px; border: 1px solid #FF0000; font-size: 12px; font-weight: bold; color:#FF0000">'+row_data.member_comment +'</div></div>';
			var premium = '<div style="float:right"><img src="/img/prem.jpg" /></div>';
			var escort = '<div style="float:right"><span style="background: #c10002;display: block;height: 16px;width: 16px;text-align: center;border-radius: 50%;font-weight: 600;color: #fff;font-size: 11px;line-height: 1.46;">A</span></div>';
			if ( row_data.is_premium && row_data.member_comment){
				m =  comment + '<div style="float:left">' + row_data.username + '</div>' + premium;
			}
			else if(row_data.is_premium){
				m = '<div style="float:left">' + row_data.username + '</div>' + premium;
			}
			else if(row_data.type == '2'){
				m = comment + '<div style="float:left">' + row_data.username + '</div>';
			}
			else if(row_data.type == '1'){
				m = '<div style="float:left">' + row_data.username + '</div>' + escort;
			}
			return m;
		}
		else
			return '-';
	}
};

Cubix.Grid.DataTypes.CommentTransfer = {
	format: function (data, format, td) {
		td.setStyle('text-align', 'center');
		if ( data != null && data.length > 0 )
		{	
			
			return '<div class="hover_comment" style="position: relative" ><img src="/img/famfamfam/asterisk_red.png" /><div class="none" style="position: absolute; top: -12px; left: 40px; width: 200px; background-color: #EEE; padding: 10px; border: 1px solid #FF0000; font-size: 12px; font-weight: bold; color:#FF0000">'+ data +'</div></div>';
			}
		else
			return '-';
	}
};

Cubix.Grid.DataTypes.CommentTransferAgency = {
	format: function (data, format, td) {
		td.setStyle('text-align', 'center');
		if ( data != null && data.length > 0 )
		{	
			
			return '<div class="hover_comment" style="position: relative" ><img src="/img/famfamfam/asterisk_red.png" /><div class="none" style="position: absolute; top: -12px; left: -225px; width: 200px; background-color: #EEE; padding: 10px; border: 1px solid #FF0000; font-size: 12px; font-weight: bold; color:#FF0000">'+ data +'</div></div>';
			}
		else
			return '-';
	}
};

Cubix.Grid.DataTypes.CommentReview = {
	format: function (data, format, td) {
		td.setStyle('text-align', 'center');
		if ( data != null && data.length > 0 )
		{	
			
			return '<div class="hover_comment" style="position: relative" ><img src="/img/famfamfam/asterisk_red.png" /><div class="none" style="position: absolute; top: -12px; left: 40px; width: 200px; background-color: #EEE; padding: 10px; border: 1px solid #FF0000; font-size: 12px; font-weight: bold; color:#FF0000; z-index:10">'+ data +'</div></div>';
			}
		else
			return '-';
	}
};

Cubix.Grid.DataTypes.PEscortLink = {
	tip: false,

	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		if ( ! this.tip ) {
			this.tip = new Cubix.Tip({
				elements: [],
				plugins: ['Fade', {name: 'Shadow', options: {color: 'white-trans'}}, {name: 'AjaxContent', options: {url: '/dashboard/tip-data', params: function () {
					var data = this.el.getParent('tr').rowData;
					return {escort_id: data.escort_id};
				}}}],
				behaviour: 'mouse'
			});
		}

		this.tip.attach(td);


		if ( row_data.showname )
			return '<a href=\'' + Cubix.Grid.DataTypes.EscortLink.baseUrl + '/' + row_data.showname + '\' target=\'_blank\' class="escort-showname" style="color: #21759B">' + row_data.showname + '</a>';
		else
			return '-';

	}
};

Cubix.Grid.DataTypes.EscortLink.baseUrl = '';

Cubix.Grid.DataTypes.Escort100PLink = {
	tip: false,

	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		if ( row_data.escort_id ) {
		if ( ! this.tip ) {
			this.tip = new Cubix.Tip({
				elements: [],
				plugins: ['Fade', {name: 'Shadow', options: {color: 'white-trans'}}, {name: 'AjaxContent', options: {url: '/dashboard/tip-data', params: function () {
					var data = this.el.getParent('tr').rowData;
					
						return {escort_id: data.escort_id};
					
				}}}],
				behaviour: 'mouse'
			});
		}

		this.tip.attach(td);
		}
		
		if ( row_data.showname ) {
			return '<a rel=\'' + row_data.main_photo  +'\' href=\'' + Cubix.Grid.DataTypes.EscortLink.baseUrl + '/' + row_data.showname + '-' + row_data.escort_id + '\' target=\'_blank\' class="escort-showname">' + row_data.showname + '</a>';
		}
		else {
			return '-';
		}
		
	}
};

Cubix.Grid.DataTypes.Escort100PLinkAV = {
	tip: false,

	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		if ( row_data.escort_id ) {
		if ( ! this.tip ) {
			this.tip = new Cubix.Tip({
				elements: [],
				plugins: ['Fade', {name: 'Shadow', options: {color: 'white-trans'}}, {name: 'AjaxContent', options: {url: '/dashboard/tip-data', params: function () {
					var data = this.el.getParent('tr').rowData;
						
						return {escort_id: data.escort_id};
					
				}}}],
				behaviour: 'mouse'
			});
		}

		this.tip.attach(td);
		}
		
		if ( row_data.showname ) {
			var color = '';
			if(row_data.need_age_verification == 3){
				color = "green";
			}
			else if(row_data.need_age_verification == 0){
				color = "red";
			}
			else if(row_data.need_age_verification == 4){
				color = "purple";
			}
			else{
				color = 'orange';
			}
			
			return '<a rel=\'' + row_data.main_photo  +'\' href=\'' + Cubix.Grid.DataTypes.EscortLink.baseUrl + '/' + row_data.showname + '-' + row_data.escort_id + '\' target=\'_blank\' class="escort-showname '+ color +'">' + row_data.showname + '</a>';
		}
		else {
			return '-';
		}
		
	}
};

Cubix.Grid.DataTypes.EscortAVDuplicity = {
	tip: false,

	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		var str = '';
		var dupIds = JSON.decode(data);
			
		if (dupIds) {
			dupIds.each(function (item) {
				
				var color = '';
				if(item.ver_type == 3){
					color = "green";
				}
				/*else if(item.ver_type == 0){
					color = "red";
				}*/
				else if(item.ver_type == 4){
					color = "red";
				}
				else{
					color = 'orange';
				}
				str += '<a href=\'' + item.req_id + '\' target=\'_blank\' class="escort-duplicity ' + color + '">' + item.escort_id + '&nbsp;</a>';
			});
			
		}
		
		return str; 
		
	}
};


Cubix.Grid.DataTypes.AttemptCountLink = {
	tip: false,

	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		
		if ( row_data.attempt_count ) {
			var color = '';
			if(row_data.attempt_count == 1){
				color = "green";
			}
			else if(row_data.need_age_verification > 1){
				color = "red";
			}

			return '<a href=\"javascript:void(0)\" class="attempt-count '+ color + '">' + row_data.attempt_count + '</a>';
		}
		else {
			return '-';
		}
		
	}
};

Cubix.Grid.DataTypes.EscortStatus = {
	format: function (data, format, td) {
		var tr = td.getParent('tr'),
			icon = tr.getFirst('td').getFirst('a');

		if ( data.toLowerCase().indexOf('deleted') != -1 ) {
			tr.addClass('passive');
			icon.setStyle('background-image', icon.getStyle('background-image').replace(/\.png"\)$/, '.png,gray")'));
		}

		return data;
	}
};

Cubix.Grid.DataTypes.AgencyStatus = {
	format: function (data, format, td) {
		var tr = td.getParent('tr'),
			icon = tr.getFirst('td').getFirst('a');

		if (data) {
			if ( data.toLowerCase() == 'deleted' ) {
				tr.addClass('passive');
				icon.setStyle('background-image', icon.getStyle('background-image').replace(/\.png"\)$/, '.png,gray")'));
			}
		}

		return data;
	}
};

Cubix.Grid.DataTypes.CommentDevice = {
	format: function (data) {
		return data ? 'Yes' : 'No';
	}
};

Cubix.Grid.DataTypes.PreviewLink = {
	format: function (data, format, td) {
		if ( data )
			return '<a class="sbtn icon-magnifier" href=\'' + Cubix.Grid.DataTypes.PreviewLink.baseUrl + '/' + data + '\' target=\'_blank\'></a>';
		else
			return '-';
	}
};

Cubix.Grid.DataTypes.PreviewLink.baseUrl = '';


Cubix.Grid.DataTypes.AgencyLink = {
	tip: false,
	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		if ( ! this.tip ) {
			this.tip = new Cubix.Tip({
				elements: [],
				plugins: ['Fade', {name: 'Shadow', options: {color: 'white-trans'}}, {name: 'AjaxContent', options: {url: '/dashboard/tip-data', params: function () {
					var data = this.el.getParent('tr').rowData;
					return {user_id: data.user_id};
				}}}],
				behaviour: 'mouse'
			});
		}

		this.tip.attach(td);

		if ( data ) {
			return '<a href=\'' + Cubix.Grid.DataTypes.AgencyLink.baseUrl + '/' + data + '\' target=\'_blank\' class="agency-name">' + data + '</a><p style="margin-top:3px; font-size:11px; color:#888">' + row_data.username + '</p>';
		}
			
		else
			return '-';
	}
}

Cubix.Grid.DataTypes.AgencyLink.baseUrl = '';




Cubix.Grid.DataTypes.AgencyLogo = {
	format: function (data, format, td) {
		return '<img class="logo-upload" src="' + (data ? data : '/img/logo_agency.jpg') + '" alt="" title="" width="100" height="78" />';
	}
}

Cubix.Grid.DataTypes.SeoButton = {
	format: function (data, format, td) {
		format = format || {
			primary_id: 'id',
			primary_title: 'showname',
			entity_slug: 'escort'
		};
		
		data = td.getParent().rowData;
		
		if ( data.seo_entity_id ) {
			return '<a onclick="return false" class="sbtn icon-magnifier" href="/seo/instances/compact?primary_id=' + data[format.primary_id] + '&primary_title=' + data[format.primary_title] + '&entity_slug=' + format.entity_slug + '"></a>';
		}
		else {
			return '<a onclick="return false" class="sbtn icon-magnifier-plus" href="/seo/instances/compact?primary_id=' + data[format.primary_id] + '&primary_title=' + data[format.primary_title] + '&entity_slug=' + format.entity_slug + '"></a>';
		}
	},
	
	init: function (grid) {
		grid.tbody.getElements('.icon-magnifier, .icon-magnifier-plus').each(function (btn) {
			btn.addEvent('click', function (e) {
				e.stop();
				
				Cubix.Grid.DataTypes.SeoButton.Popup.setTitle('Edit Seo Data');
				Cubix.Grid.DataTypes.SeoButton.Popup.load(this.get('href'), function () {
					this.open();
				});
			});
		});
	}
}

Cubix.Grid.DataTypes.SeoButtonCa = {
	format: function (data, format, td) {
		format = format || {
			primary_id: 'id',
			primary_title: 'title',
			entity_slug: 'classified-ads'
		};
		
		data = td.getParent().rowData;
		
		/*if ( data.seo_entity_id ) {
			return '<a onclick="return false" class="sbtn icon-magnifier" href="/seo/instances/compact?primary_id=' + data[format.primary_id] + '&primary_title=' + data[format.primary_title] + '&entity_slug=' + format.entity_slug + '"></a>';
		}
		else {*/
			return '<a onclick="return false" class="sbtn icon-magnifier-plus" href="/seo/instances/compact?primary_id=' + data[format.primary_id] + '&primary_title=' + data[format.primary_title] + '&entity_slug=' + format.entity_slug + '"></a>';
		//}
	},
	
	init: function (grid) {
		grid.tbody.getElements('.icon-magnifier, .icon-magnifier-plus').each(function (btn) {
			btn.addEvent('click', function (e) {
				e.stop();
				
				Cubix.Grid.DataTypes.SeoButton.Popup.setTitle('Edit Seo Data');
				Cubix.Grid.DataTypes.SeoButton.Popup.load(this.get('href'), function () {
					this.open();
				});
			});
		});
	}
}

if ( $defined(Cubix.Popup) ) {
	Cubix.Grid.DataTypes.SeoButton.Popup = new Cubix.Popup.Ajax({
		abar: [
			{caption: 'Save', icon: 'accept', action: Cubix.Popup.Actions.Save},
			{caption: 'Close', icon: 'cancel', action: Cubix.Popup.Actions.Close}
		],
		width: 760
	}).addEvent('loaded', function () {
		var tabs = new mootabs('seo-data-tabs', {width: null, height: 'auto'});
		if ( $('tabs') ) {
			var tabss = new mootabs('tabs', {width: null, height: 'auto'});
		}
	});
}

Cubix.Grid.DataTypes.ProfileRevisions = {
	format: function (data, format, td) {
		if ( ! data ) return '-';
		
		var rowData = td.getParent().rowData;
		
		return '<a class="sbtn icon-report' + (rowData.is_updated ? ' tbold tred' : '') + '" onclick="return false" href="/escorts/revisions?escort_id=' + rowData.id + '">(' + data + ')</a>';
	}
}

Cubix.Grid.DataTypes.ProfileRevisionsV2 = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		
		if ( ! data ) return '-';

		return '<a class="sbtn icon-report' + (rowData.is_updated ? ' tbold tred' : '') + '" onclick="return false" href="/escorts-v2/revisions?escort_id=' + rowData.id + '">(' + data + ')</a>';
	}
}

Cubix.Grid.DataTypes.ProfileRevisionsV2Load = {
    format: function (data, format, td) {
        var rowData = td.getParent().rowData;

        return '<a class="load-revision" href="/escorts-v2/load-revision?escort_id=' + rowData.id + '" onclick="return false" style="margin: auto">Show</a>';
    }
}

Cubix.Grid.DataTypes.MemberGeoCity = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
				
		if (!data)
			return '-';
		
		if (rowData.geo == 1)
			td.setStyle('background-color', '#f9f607');
			
		return data;
	}
}

Cubix.Grid.DataTypes.ToggleButton = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';
		
		if ( ! data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/newsletter/subscribers/toggle?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ToggleActive = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';

		if ( ! data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/billing/packages/toggle-active?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ToggleVip = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';

		if ( ! data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/escorts-v2/toggle-vip?escort_id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ToggleReview = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		td.setStyle('text-align', 'center');
		var id = rowData.id;
		var cls = '';

		if ( data ) cls = '-p';
		
		if (!rowData.ag_disabled_reviews)
			return '<a class="sbtn icon-add' + cls + '" href="/escorts-v2/toggle-review?escort_id=' + id + '" onclick="return false" style="margin: auto"></a>';
		else
			return '-';
	}
}

Cubix.Grid.DataTypes.ToggleComment = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		td.setStyle('text-align', 'center');
		var id = rowData.id;
		var cls = '';

		if ( data ) cls = '-p';
		
		if (!rowData.ag_disabled_comments)
			return '<a class="sbtn icon-add' + cls + '" href="/escorts-v2/toggle-comment?escort_id=' + id + '" onclick="return false" style="margin: auto"></a>';
		else
			return '-';
	}
}

Cubix.Grid.DataTypes.ToggleOnlineNews = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';
		var status = 4;
		if (data != 1 ){
			cls = '-p';
			status = 1;
		} 

		return '<a class="sbtn icon-accept' + cls + '" href="/newsletter/online-news/toggle-active?id=' + id + '&status=' + status + '" onclick="return false" style="margin: auto"></a>';
	}
}
Cubix.Grid.DataTypes.CheckWeb = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		td.setStyle('text-align', 'center');
		
		if ( data != null && data.length > 0 ){
			if(rowData.check_website){
				return  '<div class="checkweb-info"><img src="/img/famfamfam/accept.png"/><div class="info">Checked in date:<br/><span class="green">'+ rowData.check_website_date +'</span><br/> Checked by: <br/><strong class="green">'+ rowData.check_website_person +'</strong></div>';
			}
			else if(rowData.block_website){
				return  '<div class="checkweb-info"><img src="/img/famfamfam/accept_red.png"/><div class="info">Blocked in date:<br/><span class="red">'+ rowData.block_website_date +'</span><br/> Blocked by: <br/><strong class="red">'+ rowData.block_website_person +'</strong></div>';
			}
			else{
				return  '<img src="/img/famfamfam/accept_orange.png"/>';
			}
		}
		else{
			return '<img src="/img/famfamfam/accept_passive.png"/>';
		}

	}
}

Cubix.Grid.DataTypes.AgeVerificationStatus = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		td.setStyle('text-align', 'center');
		
		if(rowData.status == 3){
			return  '<div class="ver-info"><img src="/img/famfamfam/accept.png"/><div class="info">Certification:<br/><span class="green">'+ rowData.age_verify_date +'</span><br/> Sent by: <br/><strong class="green">'+ rowData.sent_by +'</strong></div>';
		}
		else if(rowData.status == 4){
			return  '<div class="ver-info"><img src="/img/famfamfam/accept_red.png"/><div class="info">Certification:<br/><span class="red">'+ rowData.age_verify_date +'</span><br/> Sent by: <br/><strong class="red">'+ rowData.sent_by +'</strong></div>';
		}
		else if(rowData.status == 2){
			return  '<div class="ver-info"><img src="/img/famfamfam/accept_orange.png"/><div class="info">Certification:<br/><span class="red">'+ rowData.age_verify_date +'</span><br/> Sent by: <br/><strong class="red">'+ rowData.sent_by +'</strong></div>';
		}
		else{
			return '<img src="/img/famfamfam/accept_passive.png"/>';
		
		}
	}
}

Cubix.Grid.DataTypes.AgeRejectReasons = {
	format: function (data, format, td) {
		td.setStyle('text-align', 'center');
		if ( data == 4 )
		{	
			return '<a href="#" class="age-rejections" ><img src="/img/famfamfam/asterisk_red.png" /></a>';
			}
		else
			return '-';
	}
};

Cubix.Grid.DataTypes.CheckPornNotification = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';
		
		if ( ! data ) 
			return '<a class="sbtn icon-accept-p" href="/escorts-v2/toggle-porn?escort_id=' + id + '" onclick="return false" style="margin: auto"></a>';
		else
			return '<div class="porn-notification-info"><a class="sbtn icon-accept-r" href="/escorts-v2/toggle-porn?escort_id=' + id + '" onclick="return false" style="margin: auto"></a><div class="info">Sms sent in date:<br/><span class="red">'+ rowData.porn_notification_date +'</span><br/> Sent by: <br/><strong class="red">'+ rowData.porn_notification_person +'</strong></div></div>';
	}
}

Cubix.Grid.DataTypes.CheckGallery = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
				
		if ( ! data ) 
			return '<a class="sbtn icon-accept-p" href="/escorts-v2/toggle-check-gallery?escort_id=' + id + '" onclick="return false" style="margin: auto"></a>';
		else
			return '<a class="sbtn icon-accept" href="/escorts-v2/toggle-check-gallery?escort_id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.Website = {
	format: function ( data, format, td ) {
		var rowData = td.getParent().rowData;
		
		if ( data != null && data.length > 0 ) {
			if ( rowData.website ) {
				return '<a href="go?' + rowData.website + '" target="_blank">' + rowData.website + '</a>';
			}
		}
		return '';
	}
}

Cubix.Grid.DataTypes.ToggleAgReview = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		td.setStyle('text-align', 'center');
		var id = rowData.id;
		var cls = '';

		if ( data ) cls = '-p';
		
		return '<a class="sbtn icon-add' + cls + '" href="/agencies/toggle-review?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ToggleAgComment = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		td.setStyle('text-align', 'center');
		var id = rowData.id;
		var cls = '';

		if ( data ) cls = '-p';
		
		return '<a class="sbtn icon-add' + cls + '" href="/agencies/toggle-comment?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ToggleActiveBl = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.bl_id;
		var cls = '';

		if ( ! data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/client-blacklist/toggle-active?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}
Cubix.Grid.DataTypes.ToggleActiveBlH = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.bl_id;
		var cls = '';

		if ( ! data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/client-hotel-blacklist/toggle-active?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ToggleActiveABE = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';

		if ( ! data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/admin-blacklist-entry/toggle-active?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ToggleActiveClubComment = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.ac_id;
		var cls = '';

		if ( data != 1 ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/agencies/toggle-club-comment?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ToggleActiveBubble = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';

		if ( ! data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/bubbles/toggle?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ToggleBlogStatus = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';

		if ( ! data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/blog/toggle?id=' + id + '" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ToggleBlogSpecial = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';

		if ( ! data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/blog/special?id=' + id + '" style="margin: auto"></a>';
	}
}
Cubix.Grid.DataTypes.ToggleBlogSlider = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';

		if ( ! data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/blog/slider?id=' + id + '" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ToggleBlogCatStatus = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';

		if ( ! data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/blog-category/toggle?id=' + id + '" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ToggleActiveRecLink = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';
		
		if ( ! data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/reciprocal-links/toggle?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ToggleActiveSmsReview = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';
		
		
		if (rowData.is_sent){
			td.getParent('tr').setStyle('background-color', '#F7FBDC');
		}
		
		if ( ! data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/sms/inbox/toggle?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ToggleActiveStatus = {
    format: function (data, format, td) {
        var rowData = td.getParent().rowData;
       		
		if (rowData.date_activated == 0){
			td.innerHTML = '<a class="sbtn icon-activate icon-accept-p" data-id = ' + rowData.id + '></a>';
		}
		else if(rowData.date_banned != 0){
			td.innerHTML = '<a class="sbtn icon-activate" data-id = ' + rowData.id + '></a>';
		}
		else{
			td.innerHTML = '<img src="/img/famfamfam/accept.png"/>';
		}



    }
}

Cubix.Grid.DataTypes.ToggleActiveSlogan = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';

		if ( ! data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/slogans/toggle?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ActivateUser = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		
		return '<a class="sbtn icon-accept "   title ="Activate" href="/escorts-v2/activate-user?id=' + id + '&status=1" onclick="return false" style="float:left; margin-left:5px"></a>' +
			   '<a class="sbtn icon-accept-p " title ="Disable" href="/escorts-v2/activate-user?id=' + id + '&status=-4" onclick="return false" style="float:left; margin-left:5px"></a>' +
			   '<a class="sbtn icon-accept-r " title ="Delete"  href="/escorts-v2/activate-user?id=' + id + '&status=-5" onclick="return false" style="float:left; margin-left:5px"></a>';
	}
}

Cubix.Grid.DataTypes.SuspLink = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		
		td.setStyle('word-break', 'break-all');
		
		return '<a href="' + data + '" target="_blank">' + data + '</a>';
	}
}

Cubix.Grid.DataTypes.SuspComment = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		
		td.setStyle('word-break', 'break-all');
		
		return data;
	}
}

Cubix.Grid.DataTypes.ToggleActiveBoUsers = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';
		
		if ( data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/backend-users/toggle-active?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}
Cubix.Grid.DataTypes.ToggleActiveHolidays = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var cls =data=='0'? '-p':'';
		return '<a rel="'+data+'" username="'+rowData.username+'" class="sbtn icon-accept' + cls + '" href="/settings/toggle-active?id=' + rowData.id + '&disable='+Math.abs(data-1)+'&user_id='+rowData.uid+'&date_from='+rowData.date_from+'&date_to='+rowData.date_to+'" onclick="return false;" style="margin: auto"></a>';
	}
}
Cubix.Grid.DataTypes.ToggleActiveHolidaysView = {
	format: function (data, format, td) {
		var cls =data=='0'? '-p':'';
		return '<span class="sbtn icon-accept' + cls + '"style="margin: auto;cursor:auto;"></span>';
	}
}
Cubix.Grid.DataTypes.ToggleActivePullPersons = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';
		
		if ( ! data ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/billing/pull-persons/toggle-active?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.ReviewSMSAnswer = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var tr = td.getParent('tr');
		
		if (rowData.status == "Not approved" && rowData.answer == 2)
			tr.setStyle('background-color', '#bafdcb');
		else if(rowData.status == "Not approved" && rowData.answer == 1)
			tr.setStyle('background-color', '#FCCCF2');
		
		td.setStyle('text-align', 'center');

		if (data)
		{
			var str = '<div style="position:relative;"><a href="#" class="sms-tip"><img src="/img/cubix-button/icons/unread.png" /></a>';
			str += '<div style="position: absolute; top: 16px; left: 40px; width: 100px; background-color: #EEE; padding: 10px; border: 1px solid #000; font-size: 14px; font-weight: bold" class="none">' + data + '</div></div>';

			return str;
		}
		else
			return '-';		
	}
}

Cubix.Grid.DataTypes.IsMobile = {
    format: function (data, format, td) {

        td.setStyle('text-align', 'center');

        if (data === 'M')
        {
            return '<div title="From Mobile Device" style="text-align: center;">' +
				'<span style="display: block; width: 28px; height: 28px; line-height: 28px; border-radius: 50%; background: #cecece; color: #680000; text-align: center; margin: 0 auto; font-weight: bold;">' + data + '</span>' +
				'</div>';
        }
        else
            return '-';
    }
}

Cubix.Grid.DataTypes.ToggleActiveFAQ = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';

		if ( data == 0 ) cls = '-p';

		return '<a class="sbtn icon-accept' + cls + '" href="/system/faq/toggle-active?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.TimeZone = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;

		if ( row_data.time_zone ) {
			var shift = row_data.time_zone_shift;
			if ( parseInt(row_data.time_zone_shift) > 0 ) {
				shift = '+' + shift;
			}
			else if ( parseInt(row_data.time_zone_shift) == 0 ) {
				shift = "";
			}
			return '(GMT ' + shift + ') ' + row_data.time_zone;
		}
		else {
			return "-";
		}
	}
}

Cubix.Grid.DataTypes.ToggleDefault = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var id = rowData.id;
		var cls = '';

		if ( ! data ) 
			cls = '-p';
		else
		{
			td.getParent().setStyle('font-weight', 'bold');
			td.getParent().setStyle('background-color', '#F7FBDC');
		}

		return '<a class="sbtn icon-accept' + cls + '" href="/billing/packages/toggle-default?id=' + id + '" onclick="return false" style="margin: auto"></a>';
	}
}

Cubix.Grid.DataTypes.PremiumMember = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var is_premium = rowData.is_premium;
		
		if ( is_premium ) {
			return '<div class="premium-sticker">PREMIUM</div>';
		}
		else {
			return '-';
		}
	}
}

Cubix.Grid.DataTypes.Toggle = {
	format: function (data, format, td) {
		var field = format.field, url = format.url;
		var rowData = td.getParent().rowData;
		var cls = '';

		if ( ! data )
			cls = '-p';
		else
		{
			td.getParent().setStyle('font-weight', 'bold');
			td.getParent().setStyle('background-color', '#F7FBDC');
		}

		return '<a class="sbtn icon-accept' + cls + '" href="' + url.replace('%f%', rowData[field]).replace('%i%', rowData['id']) + '" onclick="return false" style="margin: auto"></a>';
	},

	init: function (grid) {
		grid.tbody.getElements('.icon-accept, .icon-accept-p, .icon-accept-slider').each(function (btn) {
			btn.addEvent('click', function (e) {
				e.stop();

				new Request({
					url: this.get('href'),
					method: 'get',
					onSuccess: function () {
						grid.load();
					}
				}).send();
			});
		});
	}
}

Cubix.Grid.DataTypes.OrderStatus = {
	format: function (data, format, td) {
		var color = '#000';
		var status = td.getParent().rowData.status;

		 var tr = td.getParent('tr'),
			rowData = tr.rowData;

		switch ( status ) {
			case 'paid':
				color = '#0a0';
				break;
			case 'closed':
			case 'cancelled':
			case 'payment rejected':
				color = '#f00';
				break;
			case 'payment details received':
				color = '#00a';
				break;
			case 'chargeback':
				color = '#777';
				break;
			case 'pending':
				if ( rowData.is_self_checkout ) {
					return '<span style="float:left;">' + status + '</span><img rel="' + rowData.id + '" class="cc-approve-btn" style="float:left;margin-left:5px;cursor:pointer;" src="../img/famfamfam/accept.png" />';
				}
				break;
		}

		return '<span style="color: ' + color + '">' + status + '</span>';
	}
};

Cubix.Grid.DataTypes.OrderPackages = {
	format: function (data, format, td) {
		var str = '';
		data.each(function (p) {
			str += '<span>' + p.qty + 'x ' + p.name + '</span>';
		});
		str = str.replace(/<br\/>$/, '');
		
		return str;
	}
};

Cubix.Grid.DataTypes.OrderId = {
	format: function (data, format, td) {
		var str = '';
        var tr = td.getParent('tr'),
			rowData = tr.rowData;
       str = '<a href="/billing/orders/view?id='+rowData.id+'" target="_blank">' + data + '</a>';

		return str;
	}
};


Cubix.Grid.DataTypes.SystemOrderId = {
	format: function (data, format, td) {
		var str = '';
        var tr = td.getParent('tr'),
			rowData = tr.rowData;
       str = '<a href="/billing/orders/view?id='+rowData.order_id+'" class="system-order-id" target="_blank">' + data + '</a>';

		return str;
	}
};

Cubix.Grid.DataTypes.UserTypeIcon = {
	tip: false,
	
	format: function (data, format, td) {
		/*if ( ! this.tip ) {
			this.tip = new Cubix.Tip({
				elements: [],
				plugins: ['Fade', {name: 'Shadow', options: {color: 'white-trans'}}, {name: 'AjaxContent', options: {url: '/dashboard/tip-data', params: function () {return {user_id: this.el.getParent('tr').rowData.user_id}}}}],
				behaviour: 'mouse'
			});
		}*/

		//this.tip.attach(td);

		var tr = td.getParent('tr'),
			rowData = tr.rowData;
			
		var str = '<span class="user-type-escort">';
		
		str += '<a href="#">' + rowData.title + '</a>';
		str += '<p style="font-size:10px; color: #535353; margin-top:2px;">' + rowData.username + '</p>';
		str += '</span>';

		return str;
	}
}

Cubix.Grid.DataTypes.UserType = {
    format: function (data, format, td) {
        row_data = td.getParent().rowData;
        var u = row_data.username;
        if ( row_data.type )
        {
            u = '';
            var member = '<div style="float:right"><span style="background: #c10002;display: block;height: 16px;width: 16px;text-align: center;border-radius: 50%;font-weight: 600;color: #fff;font-size: 11px;line-height: 1.46;">M</span></div>';
            var ad = '<div style="float:right"><span style="background: #c10002;display: block;height: 16px;width: 16px;text-align: center;border-radius: 50%;font-weight: 600;color: #fff;font-size: 11px;line-height: 1.46;">A</span></div>';
            if(row_data.type == 'Member'){
                u = '<div style="float:left">' + row_data.username + '</div>'+member;
            }
            else if(row_data.type == 'Advertisement'){
                u = '<div style="float:left">' + row_data.username + '</div>' + ad;
            }else{
                u = row_data.username
            }
        }
        return u;
    }
}

Cubix.Grid.DataTypes.zadTitle = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		var varsa = row_data.title;
		if ( row_data.title != row_data.old_title )
		{
            varsa  = '<div style="color:#ff0000" title="Old title">' + row_data.old_title + '</div>';
		    varsa += '<div style="color:#000000" title="New title">' + row_data.title + '</div>';
		} 
		return varsa;
	}
}

Cubix.Grid.DataTypes.UserTypeIconAV = {
	tip: false,
	
	format: function (data, format, td) {
		if ( ! this.tip ) {
			this.tip = new Cubix.Tip({
				elements: [],
				plugins: ['Fade', {name: 'Shadow', options: {color: 'white-trans'}}, {name: 'AjaxContent', options: {url: '/dashboard/tip-data', params: function () {return {user_id: this.el.getParent('tr').rowData.user_id}}}}],
				behaviour: 'mouse'
			});
		}

		this.tip.attach(td);

		var tr = td.getParent('tr'),
			rowData = tr.rowData;
		
		var userTypeClass = rowData.user_type;
		if(rowData.user_type == 'escort' && rowData.pseudo_escort == 1){
			userTypeClass = 'pseudo_escort';
		}
		var str = '<span class="user-type-' + userTypeClass + '">';

		if ( rowData.user_type == 'agency' ) {
			str += '<a href="#">' + rowData.agency_name + '</a>';
			str += '<p style="font-size:10px; color: #535353; margin-top:2px;">' + rowData.username + '</p>';
		}
		else {
			var color = '';
			if(rowData.need_age_verification == 3){
				color = "green";
			}
			else if(rowData.need_age_verification == 0){
				color = "red";
			}
			else if(rowData.need_age_verification == 4){
				color = "purple";
			}
			else{
				color = 'orange';
			}
			str += '<a href="#" class="'+ color +'" >' + rowData.showname + '</a>';
			str += '<p style="font-size:10px; color: #535353; margin-top:2px;">' + rowData.username + '</p>';
		}

		str += '</span>';

		return str;
	}
}

Cubix.Grid.DataTypes.RegLocation = {


    format: function (data, format, td) {

        var tr = td.getParent('tr'),
            rowData = tr.rowData;
        country = rowData.user_country;
        if(country  === void 0 || country == null || rowData.ip_real === void 0 || rowData.ip_real == null){
            return " - ";    
        }else{
            var title = '' ;
            if(row_data.ip_class == row_data.last_class){
                title = 'Same Ip class'; 
            } 
            xlocation = rowData.user_location;
            if(rowData.user_location == ", "){
                xlocation  = rowData.ip_real;
            }
            
            var str = '<a href="/users/ips?class=' + row_data.ip_class + '" title="' + title + '"><img src="/img/flags/' + country.toLowerCase() + '.png"> ';
            str +=  xlocation + "</a>" ;
            return str;
        }
    }
    
}

Cubix.Grid.DataTypes.LoginLocation = {


    format: function (data, format, td) {

        var tr = td.getParent('tr'),
            rowData = tr.rowData;
        country = rowData.last_country;
        if(country  === void 0 || country == null  || rowData.last_ip === void 0 || rowData.last_ip == null ){
            return " - "; 
        }else{
            var title = '' ;
            if(row_data.ip_class == row_data.last_class){
                title = 'Same Ip class'; 
            } 
            
            xlocation = rowData.last_location;
            if(rowData.last_location == ", "){
                xlocation  = rowData.last_ip;
            }
            var str = '<a href="/users/ips?class=' + row_data.last_class + '" title="' + title + '"><img src="/img/flags/' + country.toLowerCase() + '.png"> ';
            str +=  xlocation + "</a>";
            return str;    
        }
        
    }
    
}

Cubix.Grid.DataTypes.UserTypeIssue = {


	format: function (data, format, td) {

		var tr = td.getParent('tr'),
			rowData = tr.rowData;

		var str = '<span class="user-type-' + rowData.user_type + '">';

		//if ( rowData.user_type == 'agency' ) {
			str += '<a href="#">' + rowData.username + '</a>';
			//str += '<p style="font-size:10px; color: #535353; margin-top:2px;">' + rowData.username + '</p>';
		/*}
		else if ( rowData.user_type == 'escort' ) {
			str += '<a href="#">' + rowData.username + '</a>';
			//str += '<p style="font-size:10px; color: #535353; margin-top:2px;">' + rowData.username + '</p>';
		}
		else{
			str += '<a href="#">' + rowData.username + '</a>';
		}*/
		str += '</span>';

		return str;
	}
}

Cubix.Grid.DataTypes.PinboardUser = {
	format: function (data, format, td) {

		var tr = td.getParent('tr'),
			rowData = tr.rowData;
			
		var c = '';
		var r = '';
		var n = '';
		
		if (rowData.user_type == 'escort')
		{
			c = 'escort-showname';
			r = rowData.escort_id;
			n = rowData.showname + ' (' + rowData.escort_id + ')';
		}
		else if (rowData.user_type == 'agency')
		{
			c = 'agency-name';
			r = rowData.agency_id;
			n = rowData.agency_name;
		}
		else if (rowData.user_type == 'member')
		{
			c = 'member-name';
			r = rowData.user_id;
			n = rowData.username;
		}

		var str = '<a href="#" class="' + c + '" rel="' + r + '">' + n + '</a>';

		return str;
	}
}

Cubix.Grid.DataTypes.ReportProblemMember = {
	format: function (data, format, td) {
		var tr = td.getParent('tr'),
			rowData = tr.rowData;

		var str = '';

		if ( rowData.user_id) 
			str = '<a class="rp-member" href="#">' + rowData.username + '</a>';
		else 
			str = rowData.name;
		
		if (rowData.status == 2)
			tr.setStyle('background-color', '#EFF9FF');
		else if (rowData.status == 3)
			tr.setStyle('background-color', '#EFFFEF');
		
		return str;
	}
}

Cubix.Grid.DataTypes.EscortPopup = {

	format: function (data) {

		if ( ! data ) return "-";

		var str = '<span class="escort-popup">';

			str += '<a href="#">' + data + '</a>';

		str += '</span>';

		return str;
	}
}

Cubix.Grid.DataTypes.EscortPopupInbox = {

	format: function (data, format, td) {
		
		var tr = td.getParent('tr'),
			rowData = tr.rowData;

		if ( ! data ) return "-";

		var str = '<span class="escort-popup">';

			str += '<a href="#">' + data + ' (' + rowData.escort_id + ')</a>';

		str += '</span>';

		return str;
	}
}

Cubix.Grid.DataTypes.EscortPopupGOTM = {

	format: function (data, format, td) {
		
		var tr = td.getParent('tr'),
			rowData = tr.rowData;

		if ( ! data ) return "-";

		var str = '<span class="escort-popup">';

			str += '<a href="#">' + data + '</a>';

		str += '</span>';

		return str;
	}
}

Cubix.Grid.DataTypes.AdComment = {

	format: function (data, format, td) {
		
		var tr = td.getParent('tr'),
			rowData = tr.rowData;

		if ( ! data ) return '<p class="ad-comment-field"><span class="icon">&#9432;</span></p>';

		var str = '<p class="ad-comment-field active">';

                        str += '<span class="icon">&#9432;</span>';
			str += '<span class="tooltip">' + data + '</span>';

                    str += '</p>';

		return str;
	}
}

Cubix.Grid.DataTypes.UserComment = {

	format: function (data, format, td) {
		
		var tr = td.getParent('tr'),
			rowData = tr.rowData;

		if ( ! data ) return '<p class="ad-comment-field"><span class="icon">&#9432;</span></p>';

		var str = '<p class="ad-comment-field active">';

                        str += '<span class="icon">&#9432;</span>';
			str += '<span class="tooltip">' + data + '</span>';

                    str += '</p>';

		return str;
	}
}

Cubix.Grid.DataTypes.UserLastIds = {

	format: function (data, format, td) {
		
		var tr = td.getParent('tr'),
			rowData = tr.rowData;
                
                var list_data;
                if( data ){
                    if( data.indexOf(',') != -1 ){
                        list_data = data.split(',');
                    } else {
                        list_data = [data];
                    }
                } else {
                    list_data = [];
                }

		if ( ! data ) return '<p class="dupl-edit-user">-</p>';

		var str = '<div class="dupl-users-field active">';
                        list_data.each(function(v, i){
                            str += '<p class="dupl-edit-user" data-id="' + v + '">' + v + '</p>';
                        });
                    str += '</div>';

		return str;
	}
}


Cubix.Grid.DataTypes.DupIds = {

    format: function (data, format, td) {
        
        var tr = td.getParent('tr'),
            rowData = tr.rowData;
                
                var list_data;
                if( data ){
                    if( data.indexOf(',') != -1 ){
                        list_data = data.split(',');
                    } else {
                        list_data = [data];
                    }
                } else {
                    list_data = [];
                }

        if ( ! data ) return '<p class="dupl-edit-user">-</p>';

        var str = '<div class="dupl-users-field active">';
                        list_data.each(function(v, i){
                             var t = v.split("|");   
                             str += '<p class="dupl-edit-user " style="color:' + t[2] + '" title="' + t[1] + '"  data-id="' + t[0] + '">' + t[0] + '</p>';
                        });
                    str += '</div>';

        return str;
    }
}


Cubix.Grid.DataTypes.ActivePackage = {
	format: function (data, format, td) {
		var tr = td.getParent('tr'),
			rowData = tr.rowData;
			
		var today = new Date().set({hr: 0, min: 0, sec: 0, ms: 0});
		

		if ( $defined(rowData.package_name) ) {
		
			str = '<strong>' + rowData.package_name + '</strong><br/>';

			if ( $defined(rowData.expiration_date) ) {
				
				var exp_date = new Date().parse(rowData.expiration_date).format('%d %b %Y');
				
				var days_left = today.diff(exp_date, 'hour');
				
				days_left = Math.round(days_left / 24);
				
				//days_left += 1; // FIXING PACKAGE EXP DATE LOGIC BY VAHAG
				
				if (rowData.is_for_phone_billing === 1) {
					exp_date = rowData.expiration_date_phone * 1000;
					days_left = today.diff(rowData.expiration_date_phone, 'hour');
					days_left = Math.round(days_left / 24);

					edate = new Date(exp_date).format('%d %b %Y at %H:%M');
				} else {
                    //edate = today.increment('day', days_left - 1 );
					edate = today.increment('day', days_left );
					edate = edate.format('%d %b %Y');
				}

				var color = 'red';
				if ( days_left > 0 ) {
					color = 'green';
				}

				if ( color == 'green' && rowData.b_user_type == 'superadmin' ) {
					str += '<a href="#" class="edit-expiration-date" rel="' + rowData.id + '" style="color:' + color + '; text-decoration: underline;">Exp.' + edate + '</a>';
				}
				else if ( color == 'green' ) {
					str += '<span style="color:' + color + '">Exp.' + edate + '</span>';
				}
				else if ( color == 'red' ) {
					str += '<span style="color:' + color + '">Exp.' + edate + '</span>';
				}

				if ( days_left > 0 ) {
					str += '<br/> (' + days_left + ' days left)';
				}

				
			}

			return str;
		}
		else {
			return '-';
		}
	}
}

Cubix.Grid.DataTypes.ActivePackageBL = {
	format: function (data, format, td) {
		var tr = td.getParent('tr'),
			rowData = tr.rowData;
		var today = new Date().set({hr: 0, min: 0, sec: 0, ms: 0});
		

		if ( $defined(rowData.package_name) ) {
		
			str = '<strong>' + rowData.package_name + '</strong><br/>';

			if ( $defined(rowData.expiration_date) ) {
				
				//var exp_date = Cubix.Grid.DataTypes.Date.format(rowData.expiration_date, '%d %b %Y %H');
				var exp_date = new Date().parse(rowData.expiration_date).format('%d %b %Y');
				
				var days_left = today.diff(exp_date, 'hour');
				
				days_left = Math.round(days_left / 24);
				
				days_left += 1; // FIXING PACKAGE EXP DATE LOGIC BY VAHAG
				
				edate = today.increment('day', days_left - 1 );
				edate = edate.format('%d %b %Y');
				
				var color = 'red';
				if ( days_left > 0 ) {
					color = 'green';
				}

				if ( color == 'green' && ( rowData.b_user_type == 'superadmin' || (rowData.b_user_type == 'admin' && rowData.b_username == "tom")) ) {
					str += '<a href="#" class="edit-expiration-date" rel="' + rowData.id + '" style="color:' + color + '; text-decoration: underline;">Exp.' + edate + '</a>';
				}
				else if ( color == 'green' ) {
					str += '<span style="color:' + color + '">Exp.' + edate + '</span>';
				}
				else if ( color == 'red' ) {
					str += '<span style="color:' + color + '">Exp.' + edate + '</span>';
				}

				if ( days_left > 0 ) {
					str += '<br/> (' + days_left + ' days left)';
				}
			}

			return str;
		}
		else {
			return '-';
		}
	}
}

Cubix.Grid.DataTypes.SusActivePackage = {
	format: function (data, format, td) {
		var tr = td.getParent('tr'),
			rowData = tr.rowData;
		var today = new Date().set({hr: 0, min: 0, sec: 0, ms: 0});
		

		if ( $defined(rowData.package_name) ) {
		
			str = '<strong>' + rowData.package_name + '</strong><br/>';
			
			if ( rowData.suspend_date ) {
				str += '<span style="color:red;">PACKAGE SUSPENDED</span><br/>';
				str += '<span>(' + rowData.days_left + ' days left)</span>';
				return str;
			}

			if ( $defined(rowData.expiration_date) ) {
				
				//var exp_date = Cubix.Grid.DataTypes.Date.format(rowData.expiration_date, '%d %b %Y %H');
				var exp_date = new Date().parse(rowData.expiration_date);
				
				var days_left = today.diff(exp_date, 'hour');
				
				days_left = Math.ceil(days_left / 24);
				
				days_left += 1; // FIXING PACKAGE EXP DATE LOGIC BY VAHAG
				
				edate = today.increment('day', days_left - 1);
				edate = edate.format('%d %b %Y');
				
				var color = 'red';
				if ( days_left > 0 ) {
					color = 'green';
				}

				if ( color == 'green' && rowData.b_user_type == 'superadmin' ) {
					str += '<a href="#" class="edit-expiration-date" rel="' + rowData.id + '" style="color:' + color + '; text-decoration: underline;">Exp.' + edate + '</a>';
				}
				else if ( color == 'green' ) {
					str += '<span style="color:' + color + '">Exp.' + edate + '</span>';
				}
				else if ( color == 'red' ) {
					str += '<span style="color:' + color + '">Exp.' + edate + '</span>';
				}

				if ( days_left > 0 ) {
					str += '<br/> (' + days_left + ' days left)';
				}
			}

			return str;
		}
		else {
			return '-';
		}
	}
}

/*Cubix.Grid.DataTypes.SusActivePackage = {
	format: function (data, format, td) {
		var tr = td.getParent('tr'),
			rowData = tr.rowData;
		var today = new Date().set({hr: 0, min: 0, sec: 0, ms: 0});
		

		if ( $defined(rowData.package_name) ) {
		
			str = '<strong>' + rowData.package_name + '</strong><br/>';

			if ( $defined(rowData.expiration_date) ) {
				//var exp_date = Cubix.Grid.DataTypes.Date.format(rowData.expiration_date, '%d %b %Y');
				var exp_date = new Date().parse(rowData.expiration_date);
				
				var days_left = today.diff(exp_date, 'hour');
				
				days_left = Math.ceil(days_left / 24);
				
				if ( $defined(rowData.suspend_date) ) {
					var date_activated = Cubix.Grid.DataTypes.Date.format(rowData.date_activated, '%d %b %Y');
					var suspend_date = Cubix.Grid.DataTypes.Date.format(rowData.suspend_date, '%d %b %Y');
					dd = new Date(suspend_date);
					days = dd.diff(exp_date);
					//console.log(days);
					days_left = days;
				}
				
				days_left += 1; // FIXING PACKAGE EXP DATE LOGIC BY VAHAG
				
				edate = today.increment('day', days_left - 1);
				edate = edate.format('%d %b %Y');
				
				var color = 'red';
				if ( days_left > 0 ) {
					color = 'green';
				}

				if ( color == 'green' && rowData.b_user_type == 'superadmin' ) {
					str += '<a href="#" class="edit-expiration-date" rel="' + rowData.id + '" style="color:' + color + '; text-decoration: underline;">Exp.' + edate + '</a>';
				}
				else if ( color == 'green' ) {
					str += '<span style="color:' + color + '">Exp.' + edate + '</span>';
				}
				else if ( color == 'red' ) {
					str += '<span style="color:' + color + '">Exp.' + edate + '</span>';
				}

				if ( days_left > 0 ) {
					str += '<br/> (' + days_left + ' days left)';
				}
			}

			return str;
		}
		else {
			return '-';
		}
	}
}*/

Cubix.Grid.DataTypes.TransferType = {
	format: function (data, format, td) {
		
		var tr = td.getParent('tr'),
			rowData = tr.rowData;
		if ( rowData.status.toLowerCase() == 'confirmed' ) {
			tr.setStyle('background-color', '#EFFFEF')
		}
		else if ( ['rejected', 'auto rejected', 'chargeback'].contains(rowData.status.toLowerCase()) ) {
			tr.setStyle('background-color', '#FFEFF2')
		}

		if ( rowData.status.toLowerCase() == 'confirmed' || rowData.status.toLowerCase() == 'rejected' ) {
			//tr.getFirst('td').getFirst().destroy();
			//tr.getFirst('td').getNext().getFirst().destroy();
		}
		
		if ( rowData.is_charged_back ) {
			tr.setStyle('background-color', '#FFEFF2')
		}

		return data;
	}
}

Cubix.Grid.DataTypes.InstantBookContactType = {
	format: function (data, format, td) {
		var contactTypes = data.split(',');
		var html = '';

		contactTypes.each(function(el) {
			html += '<span title="' + el.replace('_', ' ').toUpperCase() + '" class="' + el + '"></span>';
		});
		
		return html;
	}
}

Cubix.Grid.DataTypes.InstantBookStatus = {
	format: function (data, format, td) {
		var html = '';

		switch(data) {
            case 'no_answer':
                html = '<span class="' + data + '">Not Answered</span>'
                break;
			case 'pending':
				html = '<span class="' + data + '">Pending</span>'
				break;
			case 'accept':
				html = '<span class="' + data + '">Accepted</span>'
				break;
			case 'deny':
				html = '<span class="' + data + '">Denied</span>'
				break;
		}
		
		return html;
	}
}

Cubix.Grid.DataTypes.TransferPersonName = {
	format: function (data, format, td) {
		var rowData = td.getParent('tr').rowData;
		
		return rowData.first_name + ' ' + rowData.last_name;
	}
}

Cubix.Grid.DataTypes.PackageStatus = {
	format: function (data, format, td) {
		var tr = td.getParent('tr'),
			rowData = tr.rowData;

		if ( rowData.is_default ) {
			tr.addClass('passive');
		}
		
		return data;
	}
}

Cubix.Grid.DataTypes.BookDuration = {
	format: function (data, format, td) {
		var tr = td.getParent('tr'),
			rowData = tr.rowData;

		return rowData.duration + ' ' + rowData.duration_unit;
	}
}

Cubix.Grid.DataTypes.ChangePackage = {
	format: function (data, format, td) {
		var tr = td.getParent('tr'),
		rowData = tr.rowData;
		
		if ( rowData.movable_package )
            return "<img src='/img/cubix-button/icons/pencil.png' height='16' class='changePackage' style='cursor:pointer' width='16' title='Incoming Message' alt='Incoming Message' />";
		else
			return '-';
	}
}

Cubix.Grid.DataTypes.Suspicious = {
	format: function (data, format, td) {
		var tr = td.getParent('tr'),
			rowData = tr.rowData;
		
		if ( rowData.application_id == 2 || rowData.application_id == 1 || rowData.application_id == 17 || rowData.application_id == 5 || rowData.application_id == 69 ) {
			if (rowData.is_suspicious == 1) {
				return 'Marked as Suspicious';			
			} else if (rowData.is_suspicious == 2) {
				return 'Under Investigation';
			} else {
				return '-';
			}
		}
		
        today = new Date();
        one_day = 60 * 60;

        var data2 = Math.round(new Date().getTime() / 1000) - 86400;
        var data9 = Math.round(new Date().getTime() / 1000) - 777600;

        if ( ! data ) return '-';
		
        difdata2 = data - data2;
        difdata9 = data - data9 - 2;
        
        if (data > data2) {
            var x = Math.ceil(difdata2 / one_day);
            return x + " Hours Left For Blocking";
        } else if (data > data9) {
            var x = Math.ceil(difdata9 / one_day);
            return x + " Hours Left For Deleteing";
        } else {
            return "Deleted";
        }
	}
}

Cubix.Grid.DataTypes.SessionData = {
	format: function (data, format, td) {
		var tr = td.getParent('tr'),
			rowData = tr.rowData;

		return data;
	}
}

Cubix.Grid.DataTypes.Duration = {
    format: function (data, format, td) {
		var numdays = Math.floor(data / 86400);
        var numhours = Math.floor((data % 86400) / 3600);
        var numminutes = Math.floor(((data % 86400) % 3600) / 60);
        var numseconds = ((data % 86400) % 3600) % 60;

        var ret = '';

        if ( numdays ){
            ret +=  numdays + " days ";
        }

        if ( numhours ){
            ret +=  numhours + " hours ";
        }

        if ( numminutes ){
            ret +=  numminutes + " minutes ";
        }

        if ( numseconds ){
            ret +=  numseconds + " seconds ";
        }

        return ret;
	}
}

Cubix.Grid.DataTypes.SpecialProduct = {
	format: function (data, format, td) {
		var tr = td.getParent('tr'),
			rowData = tr.rowData;

		if ( rowData.is_special ) {
			return '<span style="background-color: #f9da91; padding: 2px 4px;font-weight: bold;">SPECIAL PRODUCT</span>';
		}

		return '-';
	}
}


Cubix.Grid.DataTypes.PhonePaymentStatus = {
	format: function (data, format, td) {
		
		row_data = td.getParent().rowData;

		if ( row_data.phone_billing_paid == 1 ) {
			return "Paid";
		}
		else {
			return "Not Paid";
		}
	}
}

Cubix.Grid.DataTypes.TransferStatus = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		
		if ( rowData.is_charged_back ) return "Chargeback";
		
		if (rowData.is_self_checkout)
            if(rowData.transfer_type_id == 6)
                return '<span style="vertical-align: middle;padding-right:5px;">' + data + '</span><img style="vertical-align: middle;" src="../img/famfamfam/bitcoin.png" />';
            else
			    return '<span style="vertical-align: middle;padding-right:5px;">' + data + '</span><img style="vertical-align: middle;" src="../img/famfamfam/creditcards.png" />';
		else
			return data;
	}
}

Cubix.Grid.DataTypes.NewAd = {
    format: function (data, format, td) {
        var rowData = td.getParent().rowData;

        if (parseInt(rowData.new_ad) == 1){
            
            return '<img src="../img/stargreen.png" title="transaction create with new ad" />';
        }else
            return "-";
    }
}


Cubix.Grid.DataTypes.RCCount = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		
		if (data > 0)
			return data;
		else
			return "0";
	}
}

Cubix.Grid.DataTypes.MemberFollow = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		
		if (data > 0)
			return '<a class="follow_list" style="cursor:pointer" onclick="return false">'+data+'</a>';
		else
			return "-";
	}
}

Cubix.Grid.DataTypes.MemberRating = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		
		if ( data ) {
			return '<img src="../img/member-rating/' + data + '.png" />';
		} else {
			return "-";
		}
	}
}

Cubix.Grid.DataTypes.PhoneNumber = {
	format: function (data, format, td) {
		var tr = td.getParent('tr'),
			rowData = tr.rowData;

		contact_phone_parsed = rowData.contact_phone_parsed ? rowData.contact_phone_parsed : '-';
		phone_exists = rowData.phone_exists ? rowData.phone_exists : '-';
		var returnString = '';		
		if ( contact_phone_parsed == phone_exists ) {
			returnString = '<span>' + contact_phone_parsed + '</span>';
		}
		
		else{
			returnString = '<span>' + contact_phone_parsed + '</span><br/><strong class="green">' + phone_exists + '</strong>';
		}
		
		if(rowData.deleted_date && phone_exists != '-'){
			var deleteDate = new Date();
			var today = new Date();
			deleteDate.parse(rowData.deleted_date);
			var deadlineDate = deleteDate.clone().increment('day',60);
			var daysLeft = today.diff(deadlineDate, 'day');

			if(daysLeft > 0){
				returnString += "<br/><span class='red'>(days left <strong class='red'>" +daysLeft + "</strong>)</span>";
			}
		}
		
		return returnString
			
	}
}

Cubix.Grid.DataTypes.OnlineNewsStatus = {
	format: function (data, format, td) {
		row_data = td.getParent().rowData;
		if(row_data.status == 1)
		{
			td.getParent('tr').setStyle('background-color', '#EFFFEF');
			td.setStyle('text-align', 'center');
			return "<span> Active </span>";
		}
		else if(row_data.status == 2){
			td.setStyle('text-align', 'center');
			return "<span> Pending </span>";
		}
		else if(row_data.status == 3)
		{
			td.getParent('tr').setStyle('background-color', '#FFEFF2');
			td.setStyle('text-align', 'center');
			return "<span> Expired </span>";
		}
		else if(row_data.status == 4)
		{
			td.getParent('tr').setStyle('background-color', '#F7FBDC');
			td.setStyle('text-align', 'center');
			return "<span> Disabled </span>";
		}
		else if(row_data.status == -1)
		{
			td.getParent('tr').addClass('passive');
			td.setStyle('text-align', 'center');
			return "<span> Deleted </span>";
		}
		
		
	}
}

Cubix.Grid.DataTypes.IsSpecialChargedBack = {
	format: function (data, format, td) {
		
		var tr = td.getParent('tr'),
			rowData = tr.rowData;
			
		if ( rowData.is_charged_back ) {
			tr.setStyle('background-color', '#FCCCF2')
		}
		/*else{
			tr.setStyle('background-color', '#FFEFF2')
		}*/

		return data;
	}
}
Cubix.Grid.DataTypes.AnalcitycsName = {
	format: function (data, format, td) {
			var pattern = /\[.*?\]/;
			return data.replace(pattern,'');
	}
}
Cubix.Grid.DataTypes.From = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var my_id = $('my_id').get('value');
		return my_id==data?'Me':rowData.username;
		 
	}
}

Cubix.Grid.DataTypes.To = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		var my_id = $('my_id').get('value');
		return my_id==data?'You':rowData.username;
		 
	}
}
Cubix.Grid.DataTypes.New_msg = {
	format: function (data, format, td) {
		var my_id = $('my_id').get('value');
		var rowData = td.getParent().rowData;
		if(my_id!=rowData.from && rowData.read=='0')
			td.getParent('tr').addClass('new_msg');
		return data.length>150?data.substring(0,150)+'...':data;
		 
	}
}
Cubix.Grid.DataTypes.Msg = {
	format: function (data, format, td) {
			return data.length>150?data.substring(0,150)+'...':data;
	}
}
Cubix.Grid.DataTypes.Hide = {
	format: function (data, format, td) {
			if(data.length>150)
				td.addClass('hide')
			return data;
	}
}

Cubix.Grid.DataTypes.Last_Month = {
	format: function (username, format, td) {
			var last_array = this.data.last_month;
			for(var i in last_array)
			{
				if(last_array[i].username==username)
					return last_array[i].month_summ;
			}
			return '0';
	}
}
Cubix.Grid.DataTypes.This_Month = {
	format: function (username, format, td) {
			var this_array = this.data.this_month;
			for(var i in this_array)
			{
				if(this_array[i].username==username)
					return this_array[i].month_summ;
			}
			return '0';
	}
}
Cubix.Grid.DataTypes.Total = {
	format: function (data, format, td) {
		
			return td.getParent().rowData.total;
	}
}
Cubix.Grid.DataTypes.New = {
	format: function (data, format, td) {
			var rowData = td.getParent().rowData;
			if(rowData.read==0)
				td.getParent('tr').addClass('new_msg');
			return data;
	}
}
Cubix.Grid.DataTypes.Active = {
	format: function (data, format, td) {
			var rowData = td.getParent().rowData;
			var	cls =rowData.status==0? '-p':'';
			return '<span class="sbtn icon-accept' + cls + '" style="margin:0 auto;cursor:default;"></span>';
	}
}
Cubix.Grid.DataTypes.Confirm_Active = {
	format: function (data, format, td) {
			var rowData = td.getParent().rowData;
			var	cls =rowData.confirm_by_sales==0? '-p':'';
			return '<span class="sbtn icon-accept' + cls + '" style="margin:0 auto;cursor:default;"></span>';
	}
}

Cubix.Grid.DataTypes.CheckXmas = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		td.setStyle('text-align', 'center');
		
		if(data == 2){
			
			return  '<a class="sbtn icon-accept" href="/system/xmas-calendar/check?id=' + rowData.id + '" onclick="return false" style="margin: auto"></a>';
		}
		else if(data == 1){
			return  '<a class="sbtn icon-accept-o" href="/system/xmas-calendar/check?id=' + rowData.id + '" onclick="return false" style="margin: auto"></a>';
		}
		else{
			return '<img src="/img/famfamfam/accept_passive.png"/>';
		}

	}
}

Cubix.Grid.DataTypes.ClasifiedPhone = {
	format: function (data, format, td) {
			var rowData = td.getParent().rowData;
			var returnString = "";
			if(rowData.phone_escort_id){
				returnString = "<span class='red'>ID: " + rowData.phone_escort_id + "</span></br>"; 
			}
			if(rowData.phone){
				returnString += "<span>" + rowData.phone + "</span>"; 
			}
			else{
				returnString = " - "
			}
			return returnString
	}
}

Cubix.Grid.DataTypes.StingImages = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		
		var returnString = data && data.length ? data : '';
		if(rowData.images && rowData.images.length ){
			returnString += '<div class="image-block">';
			rowData.images.each(function (row) {
				returnString += '<a href='+row.original+' class="highslide" onclick="return hs.expand(this);"><img class="img-file" title="" src='+row.thumb+'></a>';
			});
			returnString += '</div>';
		}

		return returnString;
	}
}

Cubix.Grid.DataTypes.Images = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;

		var returnString = '';
		if(rowData.attach && rowData.attach.length ){
			returnString += '<div class="image-block">';
			rowData.attach.each(function (row) {
				returnString += '<a href='+row.original+' class="highslide" onclick="return hs.expand(this);"><img class="img-file" title="" src='+row.thumb+'></a>';
			});
			returnString += '</div>';
		}

		return returnString;
	}
}

Cubix.Grid.DataTypes.PhoneComment = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;
		
		returnString = '-';

		if(rowData.escort_phone_comment){
			return '<div class="hover_comment" style="position: relative" ><img src="/img/famfamfam/phone_sound.png" /><div class="none" style="position: absolute; top: -50%; left: 40px; width: 200px; background-color: #EEE; padding: 10px; border: 1px solid #FF0000; font-size: 12px; font-weight: bold; color:#FF0000">'+ rowData.escort_phone_comment +'</div></div>';
		}

		return returnString;

	}
}

Cubix.Grid.DataTypes.FollowType = {
	format: function (data, format, td) {
		var rowData = td.getParent().rowData;

		return data + ' - ' + rowData.type_id;
	}
}

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}
