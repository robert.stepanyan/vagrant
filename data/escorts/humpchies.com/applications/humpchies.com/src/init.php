<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file uses sequential code to execute required
 * routines on each request to humpchies.com.
 */

// Canonicalize URLs containning the default locale in them...
if( in_array(($url_locale = \Core\Utils\Session::getPotentialLocaleFromCurrentURI()), \Core\Utils\Config::getConfigValue('\Application\Settings::locales', FALSE))
    &&
    !\Core\Utils\Session::getLocale(TRUE))
{
    $canonical_uri = str_ireplace(array($url_locale, '//'), array('', '/'), trim($_SERVER['REQUEST_URI'], '/'));

    if(empty($canonical_uri))
    {
        $canonical_uri = '/';
    }

    $canonical_url =    \Core\Utils\Device::getProtocol()
                        .
                        Core\Utils\Config::getConfigValue('\Application\Settings::hostname')
                        .
                        $canonical_uri;
	
	
    //\Core\Controller::redirect($canonical_url, '302 Found');
}

if(FALSE !== strstr(\Core\Utils\Device::getServerVarsAndHeaders('http_referer'), 'localhost') || FALSE !== strstr(\Core\Utils\Device::getServerVarsAndHeaders('http_referer'), '127.0.0.1'))
{
    \Core\Controller::redirect('http://www.elblogdelnarco.com/');
}
