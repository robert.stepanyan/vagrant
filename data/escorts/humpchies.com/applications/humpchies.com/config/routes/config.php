<?php
/*
+---------------------------------------------------------------------------+
| humpchies.com Routes                            				            |
| ====================                                                      |
|                                                                           |
| Copyright (c) Ivan Manzanilla                                             |
|                                                                           |
|   Email:              ivan_manzanilla@videotron.ca                        |
|   Phone:              514-882-2800                                        |
|   Fax:                n/a                                                 |
|   Mail Address:       574 Beaurepaire                                     |
|                       Beaconsfield, Quebec, Canada                        |
|                       H9W 3E5                                             |
|                                                                           |
| This program is NOT free software; you can NOT redistribute it and/or     |
| modify it, unless explicitly authorized by Ivan Manzanilla:               |
|                                                                           |
| This program is distributed in the hope that it will be useful, but       |
| WITHOUT ANY WARRANTY; without even the implied warranty of                |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      |
|                                                                           |
| DESCRIPTION: This file declares one array to define the routes 		    |
| (controller/action combos) that are available through the site. All       | 
| routes are mapped. The keys are actual URL components, the values are the |
| Controller action combinations.								            |
|										                                    |
| HISTORY:                                                                  |
|                                                                           |
|                   July 6, 2014: [ymanzanilla] Creation Putous!            |
|                   October 18, 2014: [ymanzanilla] Merged views.           | 
|                                                                           |
+---------------------------------------------------------------------------+

($localized_default_layout = array( 'global_location'   => '\Modules\Humpchies\Default',
                                    'global_mode'       => \Core\View::DEVICE_MODE,
                                    'header_location'   => '\Modules\Humpchies\Default',
                                    'header_mode'       => \Core\View::DEVICE_MODE,
                                    'footer_location'   => '\Modules\Humpchies\Default', 
                                    'footer_mode'       => \Core\View::DEVICE_MODE,
                                    'action_location'   => '\Modules\Humpchies\Generic::privacyPolicy',
                                    'action_mode'       => \Core\View::LOCALIZED_MODE))),
*/

// ==========================================================================
// ROUTES
// ==========================================================================   

if(\Core\Utils\Device::isMobile()){
    //mobile
    $config = array(    ''                          => array(   '\Modules\Humpchies\AdController::browse',
                                                                ($default_layout = array('supported_extensions' => array(\Core\Utils\Device::INTERFACE_PC, \Core\Utils\Device::INTERFACE_TOUCH), 'global_location' => '\Modules\Humpchies\Default', 'global_mode' => \Core\View::DEVICE_MODE))),
                    // Ad related routes
                    // =================
                    'city/search'                   => array(   '\Modules\Humpchies\CityController::search',
                                                                &$default_layout),
                    'city/new-search'                 => array(   '\Modules\Humpchies\CityController::search_new',
                                                                &$default_layout),
                    
                    'ad/listing'                    => array(   '\Modules\Humpchies\AdController::browse',
                                                                &$default_layout), 
                                                        
                    'ad/listing-v2'                    => array(   '\Modules\Humpchies\AdController::browse_v2',
                                                                &$default_layout), 
                                                        
                    //'ad/locate'                    => array(   '\Modules\Humpchies\AdController::locate',
//                                                                &$default_layout), 
                                                                
                    'ad/locate'                     => array(   '\Modules\Humpchies\AdController::locate_v2',
                        array_merge($default_layout, ['no_wrapper_column' => true, 'no_min_height' => true, 'no_footer' => true])),
                    
                    'ad/locate-v2'                     => array(   '\Modules\Humpchies\AdController::locate',
                        array_merge($default_layout, ['no_wrapper_column' => true, 'no_min_height' => true, 'no_footer' => true])),
                    
                    'json/markers'                => array(   '\Modules\Humpchies\AdController::json_markers'),  
                                                                
                    'ad/nearby'                     => array(   '\Modules\Humpchies\AdController::nearby',
                                                                &$default_layout), 
                                                                                                            
                    'ad/comments'                     => array(   '\Modules\Humpchies\AdController::comments',
                                                                &$default_layout), 
                                                                
                    'ad/search'                     => array(   '\Modules\Humpchies\AdController::search',
                                                                &$default_layout),
                    'ad/verified'                   => array(   '\Modules\Humpchies\AdController::verified',
                                                                &$default_layout),
                    'ad/details'                    => array(   '\Modules\Humpchies\AdController::show',
                                                                &$default_layout),
                    'ad/details-v2'                 => array(   '\Modules\Humpchies\AdController::show_v2', ($localized_default_layout = array_merge($default_layout, array('no_wrapper_column' => \Core\View::HIDDEN_MODE)))),
                    
                    'ad/my-listings'                => array(   '\Modules\Humpchies\AdController::browseMyListings_ma', /*&$default_layout*/ ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))), 
                    'ad/my-active-listings'         => array(   '\Modules\Humpchies\AdController::browseMyActiveListings_ma', /*&$default_layout,*/ ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))), 
                    'ad/my-expired-listings'        => array(   '\Modules\Humpchies\AdController::browseMyExpiredListings_ma', /*&$default_layout*/ ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))), 
                   

                    'ad/create'                     => array(   '\Modules\Humpchies\AdController::createMaV2', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'ad/create-v2'                  => array(   '\Modules\Humpchies\AdController::createMaV2', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'ad/confirmation'               => array(   '\Modules\Humpchies\AdController::confirmation_ma', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'ad/confirmation-v2'            => array(   '\Modules\Humpchies\AdController::confirmation2_ma', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'ad/confirmation-payment-success'=> array(   '\Modules\Humpchies\AdController::confirmation_payment_success_ma', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'ad/confirmation-payment-fail'  => array(   '\Modules\Humpchies\AdController::confirmation_payment_fail_ma', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'ad/confirmation-bump-success'  => array(   '\Modules\Humpchies\AdController::confirmation_bump_success_ma', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'ad/confirmation-bump-fail'     => array(   '\Modules\Humpchies\AdController::confirmation_bump_fail_ma', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'verification/images'           => array(   '\Modules\Humpchies\AdController::uploadSafeDocuments' ,array('action_mode' => \Core\View::MODELESS, 'header_mode' => \Core\View::HIDDEN_MODE, 'footer_mode' => \Core\View::HIDDEN_MODE)),
                    'ad/videoupload'                => array(   '\Modules\Humpchies\AdController::video_upload', &$default_layout_modeless_action),
                    'ad/deletevideo'                => array(   '\Modules\Humpchies\AdController::video_delete', &$default_layout_modeless_action),
                    'ad/checkold'                   => array(   '\Modules\Humpchies\AdController::checkold' ,array('action_mode' => \Core\View::MODELESS, 'header_mode' => \Core\View::HIDDEN_MODE, 'footer_mode' => \Core\View::HIDDEN_MODE)), 
                    'ad/verify'                     => array(   '\Modules\Humpchies\AdController::verify_ma', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'ad/edit-v2'                    => array(   '\Modules\Humpchies\AdController::editMaV2', &$default_layout_modeless_action),
                    'ad/edit'                       => array(   '\Modules\Humpchies\AdController::editMaV2', &$default_layout_modeless_action),
                    'ad/preview'                    => array(   '\Modules\Humpchies\AdController::preview_ma', &$default_layout_modeless_action), 
                    'ad/phone-validation'           => array(   '\Modules\Humpchies\AdController::phone_validation', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                     
                    'ad/delete'                     => array(   '\Modules\Humpchies\AdController::delete_ma',
                                                                ($empty_layout = array('supported_extensions' => array(\Core\Utils\Device::INTERFACE_PC, \Core\Utils\Device::INTERFACE_TOUCH), 'global_location' => '\Modules\Humpchies\Empty', 'global_mode' => \Core\View::DEVICE_MODE, 'action_mode' => \Core\View::MODELESS))),  
                    'ad/report'                     => array(   '\Modules\Humpchies\AdController::report',
                                                                ($empty_layout = array('supported_extensions' => array(\Core\Utils\Device::INTERFACE_PC, \Core\Utils\Device::INTERFACE_TOUCH), 'global_location' => '\Modules\Humpchies\Empty', 'global_mode' => \Core\View::DEVICE_MODE, 'action_mode' => \Core\View::MODELESS))),
                                                                                                                  
                    'ad/repost'                     => array(   '\Modules\Humpchies\AdController::repost_ma'),
                    
                    'ad/stop'                       => array(   '\Modules\Humpchies\AdController::stop_ma'),
                    'ad/pa-popup'                   => array('\Modules\Humpchies\AdController::paPopup' ,array('action_mode' => \Core\View::MODELESS, 'header_mode' => \Core\View::HIDDEN_MODE, 'footer_mode' => \Core\View::HIDDEN_MODE)),
                    
                    'favorites/add'                 => array(   '\Modules\Humpchies\FavoriteController::add'),
                    'favorites/remove'              => array(   '\Modules\Humpchies\FavoriteController::remove'),
                    'favorites/listing'             => array(   '\Modules\Humpchies\AdController::favorites', &$default_layout),
                    
                    // Chat related routes
                    'chat/command'                  => array(   '\Modules\Humpchies\ChatController::command',
            ($empty_layout = array('supported_extensions' => array(\Core\Utils\Device::INTERFACE_PC, \Core\Utils\Device::INTERFACE_TOUCH), 'global_location' => '\Modules\Humpchies\Empty', 'global_mode' => \Core\View::DEVICE_MODE, 'action_mode' => \Core\View::MODELESS))),
                    'chat/view'                    => array(   '\Modules\Humpchies\ChatController::view',
                        array_merge($default_layout, ['no_wrapper_column' => true, 'no_min_height' => true, 'no_footer' => true])),
                    
                    //validation related routes
                    'validation/test'               => array(   '\Modules\Humpchies\ValidationController::test', &$default_layout_modeless_action),
                    'validation/phonecallback'      => array(   '\Modules\Humpchies\ValidationController::callback', &$default_layout_modeless_action),
                    'ad/phonecheck'                 => array(   '\Modules\Humpchies\AdController::ajaxcheck',
                                                                ($empty_layout = array('supported_extensions' => array(\Core\Utils\Device::INTERFACE_PC, \Core\Utils\Device::INTERFACE_TOUCH), 'global_location' => '\Modules\Humpchies\Empty', 'global_mode' => \Core\View::DEVICE_MODE, 'action_mode' => \Core\View::MODELESS))),
                    
                    'chat/view'                    => array(   '\Modules\Humpchies\ChatController::view',
                        array_merge($default_layout, ['no_wrapper_column' => true, 'no_min_height' => true, 'no_footer' => true])),
                    // Payment
                    'payment'                       => array( '\Modules\Humpchies\PaymentController::index', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),

                    'payment/process'               => array('\Modules\Humpchies\PaymentController::process'),

                    'payment/send-result-email'     => array('\Modules\Humpchies\PaymentController::send_result_email'),
                    'payment/bitcoin_info'          => array('\Modules\Humpchies\PaymentController::bitcoin_info', &$default_layout),
                    
                    'payment/callback'              => array('\Modules\Humpchies\PaymentController::callback'),
                    'payment/bit-callback'      => array('\Modules\Humpchies\PaymentController::bitcoincallback', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),

                    'payment/sc-callback'           => array('\Modules\Humpchies\PaymentController::sc_callback', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),

                    'payment/checkout'              => array('\Modules\Humpchies\PaymentController::self_checkout', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    
                    'payment/bumps'              => array('\Modules\Humpchies\PaymentController::bumps', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'payment/bumps-purchase'     => array('\Modules\Humpchies\PaymentController::bumps_purchase', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'payment/bumps-onetime'              => array('\Modules\Humpchies\PaymentController::one_time_bump', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'payment/bumps-save'              => array('\Modules\Humpchies\PaymentController::bump_save_settings', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'payment/bumps-stop'              => array('\Modules\Humpchies\PaymentController::stop_bumping', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),

                    'payment/mid'                   => array('\Modules\Humpchies\PaymentController::mid', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS, 'footer_mode' => \Core\View::HIDDEN_MODE)))),
                    
                    'payment/shoping-cart'          => array('\Modules\Humpchies\PaymentController::shoping_cart', &$default_layout),

                    'payment/get-package-price'      => array('\Modules\Humpchies\PaymentController::get_package_price', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),

                    // User related routes
                    // ===================
                    'user/signup'                   => array(   '\Modules\Humpchies\UserController::signup_nma',
                                                                &$default_layout_modeless_action),
                    'user/activation'               => array(   '\Modules\Humpchies\UserController::activateAccount_nma'),
                    
                    'user/request-password-reset'   => array(   '\Modules\Humpchies\UserController::requestPasswordReset_nma',
                                                                &$default_layout_modeless_action),
                    'user/reset-password'           => array(   '\Modules\Humpchies\UserController::resetPassword_nma',
                                                                &$default_layout_modeless_action),
                    'user/login'                    => array(   '\Modules\Humpchies\UserController::login_nma',
                                                                &$default_layout_modeless_action),
                    'user/settings'                 => array(   '\Modules\Humpchies\UserController::settings',
                                                                &$localized_default_layout),
                    'user/logout'                   => array(   '\Modules\Humpchies\UserController::logout_ma'),
                    'user/forumlogin'               => array(   '\Modules\Humpchies\UserController::forum_check'),
                    'user/language'                 => array(   '\Modules\Humpchies\UserController::language'),
                    'user/blacklist'                => array(   '\Modules\Humpchies\UserController::blacklist',
                                                                &$default_layout_modeless_action),
                    'user/blacklistv2'                => array(   '\Modules\Humpchies\UserController::blacklistv2',
                                                                &$default_layout_modeless_action),
                    'user/citylist'                 => array(   '\Modules\Humpchies\UserController::citylist',
                                                                &$default_layout_modeless_action), 
                    
                    'dashboard'                     => array(   '\Modules\Humpchies\UserController::dashboard',
                                                                ($localized_default_layout = array_merge($default_layout, array('no_wrapper_column' => \Core\View::HIDDEN_MODE)))),
                    
                    // Generic routes
                    // ==============
                    'privacy-policy'                => array(   '\Modules\Humpchies\GenericController::privacyPolicy',
                                                                ($localized_default_layout = array_merge($default_layout, array('action_mode' => \Core\View::LOCALIZED_MODE)))),
                    'terms-and-conditions'          => array(   '\Modules\Humpchies\GenericController::termsAndConditions',
                                                                ($localized_default_layout = array_merge($default_layout, array('action_mode' => \Core\View::LOCALIZED_MODE)))),
                    'advertising'                   => array(   '\Modules\Humpchies\GenericController::advertising',
                                                                ($localized_default_layout = array_merge($default_layout, array('action_mode' => \Core\View::LOCALIZED_MODE)))),
                    'site-map'                      => array(   '\Modules\Humpchies\GenericController::sitemap',
                                                                array_merge($localized_default_layout, array('action_mode' => \Core\View::MODELESS))),
                    'rss'                           => array(   '\Modules\Humpchies\AdController::rss',
                                                                array('action_mode' => \Core\View::MODELESS, 'header_mode' => \Core\View::HIDDEN_MODE, 'footer_mode' => \Core\View::HIDDEN_MODE)),
                    'contact-us'                    => array(   '\Modules\Humpchies\MessageController::contactUs',
                                                                &$default_layout_modeless_action),
                    '404'                           => array(   '\Modules\Humpchies\AdController::notFound',
                                                                &$localized_default_layout),
                    'captcha.gif'                   => array(   '\Modules\Humpchies\GenericController::captcha'),

                    'robots.txt'                    => array(   '\Modules\Humpchies\GenericController::robots'),
                    
                    'sitemap.xml'                   => array(   '\Modules\Humpchies\GenericController::xmlSitemap',
                                                                array('action_mode' => \Core\View::MODELESS, 'header_mode' => \Core\View::HIDDEN_MODE, 'footer_mode' => \Core\View::HIDDEN_MODE)),

                    'index.php'                     => array(   '\Modules\Humpchies\RedirectionController::index'),
                    //'bitcoin_info'                  => array('\Modules\Humpchies\GenericController::bitcoin_info', &$default_layout),
                    'about-bitcoin-payment'         => array('\Modules\Humpchies\GenericController::bitcoin_info', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'newsletter/news1-fr'         => array('\Modules\Humpchies\GenericController::frnews1', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'newsletter/news1-en'         => array('\Modules\Humpchies\GenericController::ennews1', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),

                    // LEGACY URLS
                    // ===========
                    
                    'ad/full_listing'               => array(   '\Modules\Humpchies\RedirectionController::fullListing'),
                    'privacy'                       => array(   '\Modules\Humpchies\RedirectionController::privacy'),
                    'contact_us'                    => array(   '\Modules\Humpchies\RedirectionController::contactUs'),
                    'terms-of-service'              => array(   '\Modules\Humpchies\RedirectionController::termsOfService'),
                    'ad/finder'                     => array(   '\Modules\Humpchies\RedirectionController::englishSearch'),
                    'details/photos'                     => array(   '\Modules\Humpchies\AdController::photos_ma'),
                    'comments/add'                  => array(   '\Modules\Humpchies\CommentsController::create_ma'),
                    'comments/vote'                  => array(   '\Modules\Humpchies\CommentsController::vote_ma'),

                    'support/reply'           => array(   '\Modules\Humpchies\SupportController::reply_ma'),
                    'support'                       => array(   '\Modules\Humpchies\SupportController::index_ma', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'support/create-ticket'         => array(   '\Modules\Humpchies\SupportController::create_ma', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'support/ticket'                => array(   '\Modules\Humpchies\SupportController::ticket_ma', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'add-social-share'                => array(   '\Modules\Humpchies\AdController::addSocialShare'),
                    );
}else{ 
    //desktop
    $config = array(    ''                              => array(   '\Modules\Humpchies\AdController::browse',
                                                                ($default_layout = array('supported_extensions' => array(\Core\Utils\Device::INTERFACE_PC, \Core\Utils\Device::INTERFACE_TOUCH), 'global_location' => '\Modules\Humpchies\Default', 'global_mode' => \Core\View::DEVICE_MODE))),
                    
                     'testvpn/check'                  => array(   '\Modules\Humpchies\TestvpnController::check'),  
                    
                    // Ad related routes
                    // ============phone_country_iso=====
                    'city/search'                   => array(   '\Modules\Humpchies\CityController::search', &$default_layout),
                    'city/new-search'               => array(   '\Modules\Humpchies\CityController::search_new', &$default_layout),
                    'ad/listing'                    => array(   '\Modules\Humpchies\AdController::browse', &$default_layout), 
                    'ad/listing-v2'                 => array(   '\Modules\Humpchies\AdController::browse_v2', &$default_layout), 
                    'ad/search'                     => array(   '\Modules\Humpchies\AdController::search', &$default_layout),
                    'ad/verified'                   => array(   '\Modules\Humpchies\AdController::verified', &$default_layout),
                    'ad/details'                    => array(   '\Modules\Humpchies\AdController::show', &$default_layout),
                    'ad/details-v2'                 => array(   '\Modules\Humpchies\AdController::show_v2', ($localized_default_layout = array_merge($default_layout, array('no_wrapper_column' => \Core\View::HIDDEN_MODE)))),
                    
                    'ad/my-listings'                => array(   '\Modules\Humpchies\AdController::browseMyListings_ma', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))), 
                    'ad/my-active-listings'         => array(   '\Modules\Humpchies\AdController::browseMyActiveListings_ma', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))), 
                    'ad/my-expired-listings'        => array(   '\Modules\Humpchies\AdController::browseMyExpiredListings_ma', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))), 
                   

                    'ad/create'                     => array(   '\Modules\Humpchies\AdController::createMaV2', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'ad/create-v2'                  => array(   '\Modules\Humpchies\AdController::createMaV2', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'ad/confirmation'                     => array(   '\Modules\Humpchies\AdController::confirmation_ma', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'ad/confirmation-v2'            => array(   '\Modules\Humpchies\AdController::confirmation2_ma', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'ad/confirmation-payment-success'=> array(   '\Modules\Humpchies\AdController::confirmation_payment_success_ma', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'ad/confirmation-payment-fail'  => array(   '\Modules\Humpchies\AdController::confirmation_payment_fail_ma', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'ad/confirmation-bump-success'  => array(   '\Modules\Humpchies\AdController::confirmation_bump_success_ma', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'ad/confirmation-bump-fail'     => array(   '\Modules\Humpchies\AdController::confirmation_bump_fail_ma', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    
                    
                    'ad/verify'                     => array(   '\Modules\Humpchies\AdController::verify_ma', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS)))),
                    'verification/images'           => array(   '\Modules\Humpchies\AdController::uploadSafeDocuments' ,array('action_mode' => \Core\View::MODELESS, 'header_mode' => \Core\View::HIDDEN_MODE, 'footer_mode' => \Core\View::HIDDEN_MODE)),
                    'ad/checkold'                   => array('\Modules\Humpchies\AdController::checkold' ,array('action_mode' => \Core\View::MODELESS, 'header_mode' => \Core\View::HIDDEN_MODE, 'footer_mode' => \Core\View::HIDDEN_MODE)),
                    'ad/edit'                       => array(   '\Modules\Humpchies\AdController::editMaV2', &$default_layout_modeless_action),
                    'ad/edit-v2'                    => array(   '\Modules\Humpchies\AdController::editMaV2', &$default_layout_modeless_action),
                    'ad/preview'                    => array(   '\Modules\Humpchies\AdController::preview_ma', &$default_layout_modeless_action), 
                    'ad/videoupload'                => array(   '\Modules\Humpchies\AdController::video_upload', ($empty_layout = array('supported_extensions' => array(\Core\Utils\Device::INTERFACE_PC, \Core\Utils\Device::INTERFACE_TOUCH), 'global_location' => '\Modules\Humpchies\Empty', 'global_mode' => \Core\View::DEVICE_MODE, 'action_mode' => \Core\View::MODELESS))), 
                    'ad/deletevideo'                => array(   '\Modules\Humpchies\AdController::video_delete', &$default_layout_modeless_action),
                    'ad/phone-validation'           => array(   '\Modules\Humpchies\AdController::phone_validation',  ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'ad/phonecheck'                 => array(   '\Modules\Humpchies\AdController::ajaxcheck',
                                                                ($empty_layout = array('supported_extensions' => array(\Core\Utils\Device::INTERFACE_PC, \Core\Utils\Device::INTERFACE_TOUCH), 'global_location' => '\Modules\Humpchies\Empty', 'global_mode' => \Core\View::DEVICE_MODE, 'action_mode' => \Core\View::MODELESS))),
                    
                    'ad/delete'                     => array(   '\Modules\Humpchies\AdController::delete_ma',
                                                                ($empty_layout = array('supported_extensions' => array(\Core\Utils\Device::INTERFACE_PC, \Core\Utils\Device::INTERFACE_TOUCH), 'global_location' => '\Modules\Humpchies\Empty', 'global_mode' => \Core\View::DEVICE_MODE, 'action_mode' => \Core\View::MODELESS))),        
                    'ad/report'                     => array(   '\Modules\Humpchies\AdController::report',
                                                                ($empty_layout = array('supported_extensions' => array(\Core\Utils\Device::INTERFACE_PC, \Core\Utils\Device::INTERFACE_TOUCH), 'global_location' => '\Modules\Humpchies\Empty', 'global_mode' => \Core\View::DEVICE_MODE, 'action_mode' => \Core\View::MODELESS))),                                                                                      
                    'ad/repost'                     => array(   '\Modules\Humpchies\AdController::repost_ma'),
                    
                    'ad/stop'                       => array(   '\Modules\Humpchies\AdController::stop_ma'),
                    
                    'ad/comments'                       => array(   '\Modules\Humpchies\AdController::comments', &$default_layout),
                   
                    'favorites/add'                 => array(   '\Modules\Humpchies\FavoriteController::add'),
                    'favorites/remove'              => array(   '\Modules\Humpchies\FavoriteController::remove'),
                    'favorites/listing'             => array(   '\Modules\Humpchies\AdController::favorites', &$default_layout), 
                    // Chat related routes
                    'chat/command'                     => array(   '\Modules\Humpchies\ChatController::command',
            ($empty_layout = array('supported_extensions' => array(\Core\Utils\Device::INTERFACE_PC, \Core\Utils\Device::INTERFACE_TOUCH), 'global_location' => '\Modules\Humpchies\Empty', 'global_mode' => \Core\View::DEVICE_MODE, 'action_mode' => \Core\View::MODELESS))),
                    'chat/view'                     => array(   '\Modules\Humpchies\ChatController::view',
            array_merge($default_layout, ['no_wrapper_column' => true,'no_horizontal_menu' => true])),
                    
                    //validation related routes
                    'validation/test'               => array(   '\Modules\Humpchies\ValidationController::test', &$default_layout_modeless_action),
                    'validation/phonecallback'           => array(   '\Modules\Humpchies\ValidationController::callback', &$default_layout_modeless_action),
                    
                    
                    // Payment ###########
                    'payment'                       => array( '\Modules\Humpchies\PaymentController::index', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),

                    'payment/process'               => array('\Modules\Humpchies\PaymentController::process'),

                    'payment/send-result-email'     => array('\Modules\Humpchies\PaymentController::send_result_email'), 
                    
                    'payment/callback'              => array('\Modules\Humpchies\PaymentController::callback'),
                    'payment/mid'                   => array('\Modules\Humpchies\PaymentController::mid', ($default_layout_modeless_action = array_merge($default_layout, array('action_mode' => \Core\View::MODELESS, 'footer_mode' => \Core\View::HIDDEN_MODE)))),
                    

                    'payment/sc-callback'           => array('\Modules\Humpchies\PaymentController::sc_callback',&$default_layout),
                    'payment/bit-callback'          => array('\Modules\Humpchies\PaymentController::bitcoincallback',&$default_layout),

                    'payment/checkout'              => array('\Modules\Humpchies\PaymentController::self_checkout', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'payment/bumps'                 => array('\Modules\Humpchies\PaymentController::bumps', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'payment/bumps-purchase'        => array('\Modules\Humpchies\PaymentController::bumps_purchase', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'payment/bumps-onetime'         => array('\Modules\Humpchies\PaymentController::one_time_bump', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'payment/bumps-save'              => array('\Modules\Humpchies\PaymentController::bump_save_settings', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'payment/bumps-stop'              => array('\Modules\Humpchies\PaymentController::stop_bumping', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),


                    'payment/shoping-cart'          => array('\Modules\Humpchies\PaymentController::shoping_cart', &$default_layout),

                    'payment/get-package-price'      => array('\Modules\Humpchies\PaymentController::get_package_price', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),

                    //'payment/bitcoin_info'          => array('\Modules\Humpchies\PaymentController::bitcoin_info', &$default_layout),

                    // User related routes
                    // ===================
                    'user/signup'                   => array(   '\Modules\Humpchies\UserController::signup_nma',
                                                                &$default_layout_modeless_action),
                    'user/activation'               => array(   '\Modules\Humpchies\UserController::activateAccount_nma'),
                    
                    'user/request-password-reset'   => array(   '\Modules\Humpchies\UserController::requestPasswordReset_nma',
                                                                &$default_layout_modeless_action),
                    'user/reset-password'           => array(   '\Modules\Humpchies\UserController::resetPassword_nma',
                                                                &$default_layout_modeless_action),
                    'user/login'                    => array(   '\Modules\Humpchies\UserController::login_nma',
                                                                &$default_layout_modeless_action),
                    'user/settings'                 => array(   '\Modules\Humpchies\UserController::settings',
                                                                &$localized_default_layout),
                    'user/logout'                   => array(   '\Modules\Humpchies\UserController::logout_ma'),
                    
                    'user/forumlogin'               => array(   '\Modules\Humpchies\UserController::forum_check'),
                    
                    'user/language'                 => array(   '\Modules\Humpchies\UserController::language'),
                    
                    'test/update'                   => array(   '\Modules\Humpchies\UserController::updateloc'),
                    
                    'user/blacklist'                => array(   '\Modules\Humpchies\UserController::blacklist',
                                                                &$default_layout_modeless_action),
                                                                
                    'user/blacklistv2'                => array(   '\Modules\Humpchies\UserController::blacklistv2',
                                                                ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'user/citylist'                 => array(   '\Modules\Humpchies\UserController::citylist',
                                                                ($empty_layout = array('supported_extensions' => array(\Core\Utils\Device::INTERFACE_PC, \Core\Utils\Device::INTERFACE_TOUCH), 'global_location' => '\Modules\Humpchies\Empty', 'global_mode' => \Core\View::DEVICE_MODE, 'action_mode' => \Core\View::MODELESS))),
                    
                    'dashboard'                     => array(   '\Modules\Humpchies\UserController::dashboard',
                                                                ($localized_default_layout = array_merge($default_layout, array('no_wrapper_column' => \Core\View::HIDDEN_MODE)))),
                    // Generic routes
                    // ==============
                    'privacy-policy'                => array(   '\Modules\Humpchies\GenericController::privacyPolicy',
                                                                ($localized_default_layout = array_merge($default_layout, array('action_mode' => \Core\View::LOCALIZED_MODE)))),
                    'terms-and-conditions'          => array(   '\Modules\Humpchies\GenericController::termsAndConditions',
                                                                ($localized_default_layout = array_merge($default_layout, array('action_mode' => \Core\View::LOCALIZED_MODE)))),
                    'advertising'                   => array(   '\Modules\Humpchies\GenericController::advertising',
                                                                ($localized_default_layout = array_merge($default_layout, array('action_mode' => \Core\View::LOCALIZED_MODE)))),
                    'site-map'                      => array(   '\Modules\Humpchies\GenericController::sitemap',
                                                                array_merge($localized_default_layout, array('action_mode' => \Core\View::MODELESS))),
                    'rss'                           => array(   '\Modules\Humpchies\AdController::rss',
                                                                array('action_mode' => \Core\View::MODELESS, 'header_mode' => \Core\View::HIDDEN_MODE, 'footer_mode' => \Core\View::HIDDEN_MODE)),
                    'contact-us'                    => array(   '\Modules\Humpchies\MessageController::contactUs',
                                                                &$default_layout_modeless_action),
                    '404'                           => array(   '\Modules\Humpchies\AdController::notFound',
                                                                &$localized_default_layout),
                    'captcha.gif'                   => array(   '\Modules\Humpchies\GenericController::captcha'),

                    'robots.txt'                    => array(   '\Modules\Humpchies\GenericController::robots'),
                    
                    'sitemap.xml'                   => array(   '\Modules\Humpchies\GenericController::xmlSitemap',
                                                                array('action_mode' => \Core\View::MODELESS, 'header_mode' => \Core\View::HIDDEN_MODE, 'footer_mode' => \Core\View::HIDDEN_MODE)),

                    'index.php'                     => array(   '\Modules\Humpchies\RedirectionController::index'),
                    
                    'about-bitcoin-payment'         => array('\Modules\Humpchies\GenericController::bitcoin_info', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    
                    'newsletter/news1-fr'         => array('\Modules\Humpchies\GenericController::frnews1', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'newsletter/news1-en'         => array('\Modules\Humpchies\GenericController::ennews1', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),

                    // LEGACY URLS
                    // ===========
                    
                    'ad/full_listing'               => array(   '\Modules\Humpchies\RedirectionController::fullListing'),
                    'privacy'                       => array(   '\Modules\Humpchies\RedirectionController::privacy'),
                    'contact_us'                    => array(   '\Modules\Humpchies\RedirectionController::contactUs'),
                    'terms-of-service'              => array(   '\Modules\Humpchies\RedirectionController::termsOfService'),
                    'ad/finder'                     => array(   '\Modules\Humpchies\RedirectionController::englishSearch'),
                    'details/photos'                     => array(   '\Modules\Humpchies\AdController::photos_ma'),
                    'comments/add'                  => array(   '\Modules\Humpchies\CommentsController::create_ma'),
                    'comments/vote'                 => array(   '\Modules\Humpchies\CommentsController::vote_ma'),
                    'support'                       => array(   '\Modules\Humpchies\SupportController::index_ma', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'support/create-ticket'         => array(   '\Modules\Humpchies\SupportController::create_ma', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'support/ticket'                => array(   '\Modules\Humpchies\SupportController::ticket_ma', ($localized_default_layout = array_merge($default_layout, array('footer_mode' => \Core\View::HIDDEN_MODE)))),
                    'support/reply'                => array(   '\Modules\Humpchies\SupportController::reply_ma'),
                    'add-social-share'                => array(   '\Modules\Humpchies\AdController::addSocialShare'),
                    );
}


                    ?>
