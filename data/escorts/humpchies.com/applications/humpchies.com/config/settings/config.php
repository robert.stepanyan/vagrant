<?php $config = array(  'locales'           =>  (\Modules\Humpchies\Utils::isFrancophoneGeo() ? array('fr-ca', 'en-ca') : array('en-ca', 'fr-ca')) ,
                        'locales_module'    => 'humpchies',
                        'run_init'          => TRUE,
                        'title'             => '[TITLE-DEFAULT]',
                        'keywords'          => '[KEYWORDS-DEFAULT]',
                        'description'       => '[DESCRIPTION-DEFAULT]',
                        'in_page_text'      => '[IN-PAGE-TEXT-DEFAULT]');?>