
window.addEventListener("load", function(){
// Get the modal
var modal = document.getElementById("filters-modal");

// Get the button that opens the modal
var btn = document.getElementById("filters-mod");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close_report")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    
  modal.style.display = "block";
  $('html, body').css({overflow: 'hidden'});
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
  $('html, body').css({overflow: 'auto'});
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
    $('html, body').css({overflow: 'auto'});
  }
}

});
$(function() {
    
// A $( document ).ready() block.
//$( document ).ready(function() {
    
    $(".filters-box").on('click', function() { // open filters modal
        $("#filters-modal").show();
    });
    
    $("#close_filters").on('click', function() { // open filters modal
        $("#filters-modal").hide();
    });
    
    $(".clear-form").on('click', function() { // clear filters
        
        $("input:radio").prop("checked",false);
        $("input:checkbox").prop("checked",false);
         
        $("#location").prop("disabled", true); 
        $("#services").prop("disabled", true);
         
        $(".filter-icon").removeClass("active");
        $(".filter-title").removeClass("active");
    });
    
    // when we apress apply, create the hidden inputs with values inside forms and add tags in the filters box
    
    $("#filters-apply").on('click', function() {
        // clear previously selected filters
        $("#selected_filters").html('');
        
        // get selected radio and write it on the list if needed
        addAvailability();
        // get selected checkboxes and write them on the list
        addServices()
       
        // submit form
        $("#filters-modal").hide();
    });
    
    deleteFilter("service")
    deleteFilter("availability")
    //$(".delete_filter").on('click', function() {
    //    console.log("deleted")
    //});
    
    
    
});


function addAvailability() {
    
    // get selected radio if any
    var availName = $('input[name=location_filters]:checked').data("label");
    var availId = $('input[name=location_filters]:checked').val();
    
    if(availId !== undefined) {
        if(availId != 'all'){
            writeInBox(availName, availId, 'availability');
        }
        
        
        // update location input
        $("#location").val(availId);    
        $("#location").removeAttr("disabled"); 
        
        $(".filter-icon").addClass("active");
        $(".filter-title").addClass("active");
           
    } else {
        $("#location").val('');        
    }
    
}


function addServices() {
    
    var Services = [];
    $("#services").val('');
    $('input[name="selected_services[]"]:checked').each(function() {
       // console.log($(this).val());
            
        var serviceName = $(this).data("label");
        var serviceId = $(this).val();
        //console.log(serviceName)
//        console.log(serviceId)
        writeInBox(serviceName, serviceId, 'service');
        Services.push(serviceId);
        // console.log(Services)
        $("#services").val(Services.join('+'));    
        $("#services").removeAttr("disabled"); 
        
        $(".filter-icon").addClass("active");
        $(".filter-title").addClass("active");
    }); 
}

function writeInBox(name, id, filterType) {
    
    $("#selected_filters").append('<p>' + name +'<a href="javascript:void(0)" id="remove_'+ id +'" class="delete_filter"><i class="fa fa-close"></i></a></p>');
    //console.log($("#selected_filters").html());
    // for each new dynamically added element you must enable deletion
    deleteFilter(filterType)
}

function deleteFilter(filterType) {
    
    $(".delete_filter").on('click', function(e) {
        e.stopPropagation();
        console.log("deleted")
        console.log(filterType)
        
        $(this).closest('p').remove();
    });
      
    
} 