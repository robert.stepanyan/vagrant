ymaps.ready(init);

var firstGeoObject, myMap;
function init() {
    
    $("#no_results").hide(); // hide no results
    
    myMap = new ymaps.Map('map', {
        center: [45.50165694120211, -73.56725063558198],
        zoom: 15,
        controls: ['zoomControl', 'geolocationControl']
    });

    ymaps.geolocation.get({
        // Setting the option for detecting location by IP provider: 'yandex',
        // Automatically geocoding the result.
        autoReverseGeocode: true
    }).then(function (result) {
        // Taking the data resulting from geocoding the object and outputting it to the console.
        var lat = result.geoObjects.get(0).geometry.getCoordinates()[0];
        var lng = result.geoObjects.get(0).geometry.getCoordinates()[1];
        
        firstGeoObject = new ymaps.GeoObject({
            // The geometry description.
            geometry: {
                type: "Point",
                coordinates: [lat, lng]
            }
        }, {
            /**
             * Options.
             * The placemark's icon will stretch to fit its contents.
             */
            preset: 'islands#darkBlueDotIconWithCaption',
            // The placemark can be dragged.
            draggable: true
        })
        myMap.geoObjects.add(firstGeoObject); // add marker to map
        
        // initialize default coords
        loadData(lat,lng); // check result and if there are any, enable btn
        
        //console.log("coordinates" + coords);
        enableDrag(firstGeoObject); // enable dragging of the marker
        enableMapClick(myMap);      // when clicking map, set marker 
        
    });
    
     $("#city_map").on("change", function() {
        console.log("City changed");
        //$("#maps_city").val($(this).val()); // update city val 
        codeAddress($(this).val() + ', ' + 'Canada');
    });
    
    
}

function codeAddress(address) {
    console.log("Started code address");
    $("#no_results").hide(); // hide no results
    // Finding coordinates of the center of Montreal.
    ymaps.geocode(address, {
        results: 1
    }).then(function (res) {

            if (myMap) {
                myMap.destroy();// Destructor of the map
                myMap = null;    
            }
            
            myMap = new ymaps.Map('map', {
                center: res.geoObjects.get(0).geometry.getCoordinates(),
                zoom: 14,
                controls: ['zoomControl', 'typeSelector',  'fullscreenControl']
            });
        
            //console.log("I found the city");
            firstGeoObject = res.geoObjects.get(0); // set marker on map
            
            // The coordinates of the geo object.
            var coords = firstGeoObject.geometry.getCoordinates()

            firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
            firstGeoObject.options.set( 'draggable', 'true');
            
            // Adding first found geo object to the map.
            myMap.geoObjects.add(firstGeoObject);
            
            // initialize default coords
            loadData(coords[0], coords[1]); // check result and if there are any, enable btn
            
            //console.log("coordinates" + coords);
            enableDrag(firstGeoObject); // enable dragging of the marker
            enableMapClick(myMap);      // when clicking map, set marker                 
        });
        
        
    
    
}

function enableDrag(geoObj) {
    
    geoObj.events.add('dragend', function (event) {   // when drag, udate coords
                        
        console.log(event.get("target").geometry.getCoordinates()[0]) 
        console.log(event.get("target").geometry.getCoordinates()[1]) 
        // update lat and long
        loadData(event.get("target").geometry.getCoordinates()[0],event.get("target").geometry.getCoordinates()[1]); // check result and if there are any, enable btn
    }); 
    
}

function enableMapClick(myMap) {
    
    /**
     * Processing events that occur when the user
     * left-clicks anywhere on the map.
     * When such an event occurs, we open the balloon.
     */
    myMap.events.add('click', function (e) {
        console.log("map was clicked");
        var coords = e.get('coords');
        
        // update marker position
        myMap.geoObjects.removeAll();
        
        firstGeoObject = new ymaps.GeoObject({
            // The geometry description.
            geometry: {
                type: "Point",
                coordinates: coords
            }
        }, {
            /**
             * Options.
             * The placemark's icon will stretch to fit its contents.
             */
            preset: 'islands#darkBlueDotIconWithCaption',
            // The placemark can be dragged.
            draggable: true
        })
        myMap.geoObjects.add(firstGeoObject); // add marker to map
        loadData(coords[0],coords[1]); // check result and if there are any, enable btn
        enableDrag(firstGeoObject)

    });   
    
}


function loadData(lat, lng) {
    console.log("loadData start")
     $("#no_results").hide(); // hide no results
     var searchUrl = '/ad/json_markers?lat=' +  lat + '&long=' + lng;
     var mydata = new Object();
     mydata.lat = lat;
     mydata.long = lng;
     
     $.ajax({
            url: '/ad/json_markers',
            method: 'get',
            data: mydata,
            dataType:"json",
            success: function (resp) {
                //console.log(resp.length);
                
                if(resp.status == 'have_results') {
                    enableBtn(resp.count, lat, lng);
                    
                } else {
                    $("#show_list").addClass("disabled"); // disablebtn
                    $("#show_list").attr("href",""); // disablebtn
                    $("#ads_count").html('(0)');
                    
                    if(resp.status == 'nearest') {
                        
                        // update text
                        $("#city_name").html(resp.city)
                        
                        
                        // updade link/ btn
                        $("#show_list").removeClass("disabled");
                        $("#show_list").attr("href","/ad/nearby/lat/" + resp.lat + "/lng/" + resp.lng); 
                        $("#ads_count").html('');
                        
                        $("#show_near").attr("href","/ad/nearby/lat/" + resp.lat + "/lng/" + resp.lng); 
   
                        // scroll to div
                        $("#no_results").show();
                        //var element = document.getElementById("no_results");
//                        element.scrollIntoView(false);
                         $('html, body').animate({scrollTop: '+=100px'}, 800);
                    }
                    
                    
                }
                
            }
        }); 

}

function enableBtn(count, lat, lng) {
    
    $("#show_list").removeClass("disabled");
    $("#ads_count").html('('+ count + ')');
    // update btn href
   $("#show_list").attr("href","/ad/nearby/lat/" + lat + "/lng/" + lng); 
    
    
}