
window.addEventListener("load", function(){
// Get the modal
var modal = document.getElementById("cities_zones");

// Get the button that opens the modal
var btn = document.getElementById("cities_list");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close_report")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
 
  modal.style.display = "block";
  $('html, body').css({overflow: 'hidden'});
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
  $( 'ul.cities_lists:not(.hidden)').removeClass('hovered');
  $(".select_area").removeClass('hovered');
  $('html, body').css({overflow: 'auto'});
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
    $( 'ul.cities_lists:not(.hidden)').removeClass('hovered');
    $(".select_area").removeClass('hovered');
    $('html, body').css({overflow: 'auto'});
  }
}

});
$(function() {
    
    $("#cities_zones").click(function(e) {
        $(".select_area").removeClass('hovered');
        $( 'ul.cities_lists:not(.hidden)').removeClass('hovered');    
    })
    
    $(".area > li").click(function (e) {
       $(".select_area").toggleClass('hovered');
       $( 'ul.cities_lists:not(.hidden)').removeClass('hovered');
       
       e.stopPropagation();
    });
        
    $(".city > li").click(function (e) {
        $( 'ul.cities_lists:not(.hidden)').toggleClass('hovered');
        
        e.stopPropagation();
    });
    
    
    $(".select_area > li").click(function (e) {
    
        $(".selected_area").html($(this).html());
        $(".cities_lists").addClass('hidden');

        $('ul.cities_lists.' + $(this).attr("class")).removeClass('hidden');
//        $(".selected_city").html($('ul.cities_lists.' + $(this).attr("class") + ' li:first a').html());
        $(".selected_city").html($(".city").data("txt"));

    });
    
    
    $("#search-box").keyup(function(){
        //console.log("keyup")
        $.ajax({
        type: "POST",
        url: "/city/search",
        data:'keyword='+$(this).val() + "&category_code=" + $("#category_code").val(),
        beforeSend: function(){
            //$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
        },
        success: function(data){
            
            $("#suggesstion-box").html(''); 
            
            if(data.length > 2) {
                
                var obj = JSON.parse(data)
                $.each(obj, function(idx, city){
                
                    var cityOpt = '<li><a href="' + city.city_url +'">' + city.name +' </a></li>';
                    $("#suggesstion-box").append(cityOpt);
                    
                });       
                
                
            }
            
            //$("#suggesstion-box").show();
//            $("#suggesstion-box").html(data);
//            $("#search-box").css("background","#FFF");
        }
        });
    }); 
    
    
    
    
//    
//    $('.area > li').mouseleave(function () {
////        console.log("hover out");
//       $(".select_area").css("display", "none");
//     }
// ).mouseleave();//trigger mouseleave to hide second div in beginning 
//    
//    $('.area > li').on('touchstart touchend', function(e) {
////        e.preventDefault();
//        $(".select_area").css("display", "block");
//    });
//    
 
});