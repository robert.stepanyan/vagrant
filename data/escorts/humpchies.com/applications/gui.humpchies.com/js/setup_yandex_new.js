$(function () {
    ymaps.ready(init);
});

var firstGeoObject, myMap;

function init() {
    
    // get map info
    var $mapData = $('#map');
    var $hideMap = $('#hide_map');
    
    if (typeof $mapData.data('lat') !== 'undefined' && typeof $mapData.data('long') !== 'undefined') {          // THE MAP ALREADY HAS A MARKER
        //console.log("COORDS ARE SET");
        
        if($('#hide_map_check').val() == 'no'){
            $("#show_map").hide();
            codeCoords($mapData.data('lat') , $mapData.data('long') );
        }
        
        $("#show_map").on("click", function(){
            //console.log("clicked");
            $('#hide_map').val('no');    
            $('#hide_map_check').val('no');    
            codeCoords($mapData.data('lat') , $mapData.data('long') );
            
            $(this).hide();
            $("#remove_map").css('display', 'flex'); // show delete button
            
            $(".add-success").hide();
            $(".remove-success").hide();
        });
        
    } else {
        // create new map
        //console.log("Lat long are totally not set");
        
        // enable hide/show map
        $("#show_map").on("click", function(){
            //console.log("clicked");
            
            $(".add-success").hide();
            $(".remove-success").hide();
            
            $('#hide_map').val('no');
            $('#hide_map_check').val('no');     
            
            var address = $("#maps_city").val() + ', ' + 'Canada';
            codeAddress(address);
            $(this).hide();
            
            $("#remove_map").css('display', 'flex'); // show delete button
        });
    }
    
    
    $("#remove_map").on("click", function() {    // user wants to remove the map
        //console.log("remove map was clicked");
        
        $('#hide_map').val('yes');
        $('#hide_map_check').val('yes');  
        
        if (myMap) {
            myMap.destroy();// Destructor of the map
            myMap = null;    
        }
        $(".add-success").hide();
        $(".remove-success").css('display', 'block');
        $("#show_map").show();
        $(this).hide();
    }); 
   
    $("#city").on("change", function() {
        //console.log("City changed");
        $("#maps_city").val($(this).val()); // update city val 
        codeAddress($(this).val() + ', ' + 'Canada');
    });
    
}


function codeAddress(address) {
     
    // Finding coordinates of the center of Montreal.
    //console.log($("#show_map").length)
    //console.log($('#hide_map').val())
    if( $('#hide_map').val() == 'no') {
        ymaps.geocode(address, {
            /**
             * Request options
             * @see https://api.yandex.com/maps/doc/jsapi/2.1/ref/reference/geocode.xml
              */
            /**
             * Sorting the results from the center of the map window
             *  boundedBy: myMap.getBounds(),
             *  strictBounds: true,
             *  Together with the boundedBy option, the search will be strictly inside the area specified in boundedBy.
             *  If you need only one result, this will minimize the user's traffic.
             */
            results: 1
        }).then(function (res) {
                //myMap.destroy();
                if (myMap) {
                    myMap.destroy();// Destructor of the map
                    myMap = null;    
                }
                
                myMap = new ymaps.Map('map', {
                    center: res.geoObjects.get(0).geometry.getCoordinates(),
                    zoom: 14,
                    controls: ['zoomControl', 'typeSelector',  'fullscreenControl']
                });
            
                //console.log("I found the city");
                firstGeoObject = res.geoObjects.get(0); // set marker on map
                
                // The coordinates of the geo object.
                var coords = firstGeoObject.geometry.getCoordinates()

                firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
                firstGeoObject.options.set( 'draggable', 'true');
                
                // Adding first found geo object to the map.
                myMap.geoObjects.add(firstGeoObject);
                
                // initialize default coords
                $("#maps_lat").val(coords[0]);
                $("#maps_long").val(coords[1]);
                //console.log("coordinates" + coords);
                enableDrag(firstGeoObject); // enable dragging of the marker
                enableMapClick(myMap);      // when clicking map, set marker                 
            });
    }    
}



function codeCoords(lat, lng) {
    //console.log("Started code address");
     //console.log("The map is visible");           
    
    // check if maps is already set
    //console.log($("#hide_map_check").val());
    if( $('#hide_map_check').val() == 'no' ) {
        if (myMap) {
            myMap.destroy();// Destructor of the map
            myMap = null;    
        }
        
        myMap = new ymaps.Map('map', {
            center: [lat, lng],
            zoom: 14,
            controls: ['zoomControl', 'typeSelector',  'fullscreenControl']
        });

        firstGeoObject = new ymaps.GeoObject({
            // The geometry description.
            geometry: {
                type: "Point",
                coordinates: [lat, lng]
            }
        }, {
            /**
             * Options.
             * The placemark's icon will stretch to fit its contents.
             */
            preset: 'islands#darkBlueDotIconWithCaption',
            // The placemark can be dragged.
            draggable: true
        })
        myMap.geoObjects.add(firstGeoObject); // add marker to map

        // initialize default coords
        $("#maps_lat").val(lat);
        $("#maps_long").val(lng);
    //        console.log("coordinates" + coords);
        
        //console.log("coordinates" + coords);
        enableDrag(firstGeoObject); // enable dragging of the marker
        enableMapClick(myMap);      // when clicking map, set marker   
    }     
}

function enableDrag(geoObj) {
    
    geoObj.events.add('dragend', function (event) {   // when drag, udate coords
                        
        //console.log(event.get("target").geometry.getCoordinates()[0]) 
        //console.log(event.get("target").geometry.getCoordinates()[1]) 

        // update lat and long
        $("#maps_lat").val(event.get("target").geometry.getCoordinates()[0]);
        $("#maps_long").val(event.get("target").geometry.getCoordinates()[1]);
        $(".remove-success").hide();
        $(".add-success").css('display', 'block');
    }); 
    
}

function enableMapClick(myMap) {
    
    /**
     * Processing events that occur when the user
     * left-clicks anywhere on the map.
     * When such an event occurs, we open the balloon.
     */
    myMap.events.add('click', function (e) {
        //console.log("map was clicked");
        var coords = e.get('coords');
        
        // update coords
        $("#maps_lat").val(coords[0]);
        $("#maps_long").val(coords[1]);
        
        // update marker position
        myMap.geoObjects.removeAll();
        
        firstGeoObject = new ymaps.GeoObject({
            // The geometry description.
            geometry: {
                type: "Point",
                coordinates: coords
            }
        }, {
            /**
             * Options.
             * The placemark's icon will stretch to fit its contents.
             */
            preset: 'islands#darkBlueDotIconWithCaption',
            // The placemark can be dragged.
            draggable: true
        })
        myMap.geoObjects.add(firstGeoObject); // add marker to map
        enableDrag(firstGeoObject)

    });   
    
}