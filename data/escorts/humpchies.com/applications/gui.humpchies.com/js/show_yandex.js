

function init() {
    if($("#show_map").val() == 'yes') {
        
        var lat = $("#map").data("lat")
        var lng = $("#map").data("long")
    
        
        var myMap = new ymaps.Map("map", {
                center: [lat, lng],
                zoom: 16,
                controls: ['zoomControl', 'typeSelector',  'fullscreenControl']
            }, {
                //searchControlProvider: 'yandex#search'
            });

        myMap.geoObjects
            .add(new ymaps.Placemark([lat, lng], {
                //balloonContent: 'the color of <strong>the water on Bondi Beach</strong>'
            }, {
                preset: 'islands#dotIcon',
                iconColor: '#3240e7'
            }));
        

    }
}


$( document ).ready(function() {
     $("#toggle_sat").on('click', function(){
          $(this).toggleClass("disabled");
          $("#simple_map").toggle();
          $("#map_sat").toggle();
      });  
      
      $("#toggle_interactive").on('click', function(){
          $("#simple_map").hide();
          $("#map_sat").hide();
          $("#map_sat").hide();
          $(".btn_map").hide();
          $(".yandex-logo").hide();
          
          ymaps.ready(init); // init interactive map
          $("#map").css("height", "300px");
      });  

}); 
