function rednupop(name, url, age, event, selector) {
    this.status = 0;
    this.name = name;
    this.url = url;
    this.age = age * 1000;
    this.event = event;
    this.selector = (typeof(selector) !== 'undefined') ? selector: false;
    this.set = function() {
        if (!(this.checkLocalStorage() && this.checkPhone())) {
            return false;
        }
        this.status = (localStorage[this.name+'_status']) ? localStorage[this.name+'_status']: 0;
        this.iniDate = (localStorage[this.name+'_time']) ? new Date(localStorage[this.name+'_time']): (localStorage[this.name+'_time'] = new Date());
        this.endDate = new Date(this.iniDate.getTime()+this.age);
        this.nowDate = new Date();
        if (this.nowDate > this.endDate) {
            if (this.status == 1) {
                this.status = localStorage[this.name+'_status'] = 0;
                localStorage.removeItem(this.name+'_time');
            }
        }
        switch (this.event) {
            case 'load':
                this.pop();
                break;
            case 'click':
                if (typeof(this.selector) == 'string' && typeof(document.querySelectorAll) !== 'undefined') {
                    if ((triggers = document.querySelectorAll(this.selector)) && (total = triggers.length)) {
                        for (var i = 0; i < total; i++) {
                            var that = this;
                            triggers[i].addEventListener('click', function(e){
                                e = e || window.event;
                                var target = e.target || e.srcElement;
                                if (that.status == 0) {
                                    e.preventDefault ? e.preventDefault(): e.returnValue = false;
                                    that.pop((typeof(target.href) !== 'undefined') ?  target.href : (target.alt == 'Logo - Humpchies') ? '/' : '');
                                }
                            }, false);
                        }
                    }
                }
                break;
        }
    }
    this.pop = function(url) {
        this.status = localStorage[this.name+'_status'] = 1;
        window.open(((typeof(url) !== 'undefined' && url !== '') ? url: window.location.href));
        window.location.href = this.url.replace(/&amp;/g, '&');
    };
    if (typeof(window.addEventListener) !== 'undefined') {
        var that = this;
        window.addEventListener('load', function(){
            that.set();
        }, false);
    }
}
rednupop.prototype.checkLocalStorage = function() {
    var storage, check = (this.name + '_storage_' + (new Date).getTime()), result;
    try {
        storage = window.localStorage;
        storage.setItem(check, check);
        result = (storage.getItem(check) == check);
        storage.removeItem(check);
        return (result);
    } catch(e) {
        return false;
    }
}
rednupop.prototype.checkPhone = function() {
    return (!(navigator.userAgent.toLowerCase().match(/Windows CE|IEMobile|Windows Phone OS/i) || ('XDomainRequest' in window))) ? true : false;
}

var rednupop = new rednupop('rednupop', rednupopUrl, 86400, 'click', 'body');
