$( document ).ready(function() {
    $("#add").on('click',function(){
        $("#form-show").show();
    });
     $("#close").on('click',function(){
        $("#form-show").hide();
    });
    
    $("#country").change(function(){
          $.ajax({
            type: "GET",
            url: "/user/citylist?c=" + $("#country").val(),
            cache: false,
            success: function(result){
              $('#city').prop("disabled", false);
              $("#city").html(result);
              $("#country_name").val($( "#country option:selected" ).text());
              
            }
         });
        
    });
    $('#date').datepicker({
        autoClose: true,
        dateFormat: "mm/dd/yyyy",
        maxDate: new Date()
    });
    
    $("#phone").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $("#close").on('click',function(){
        $("#city").html("");
        $('#city').prop("disabled", true);
        window.scrollTo(0, 0);
    });
    
     $("#form-show").validate({
            rules : {
                name        :  { required: true, minlength: 3, maxlength: 64},
                phone        :  { required: true},
                description  :  { required: true, minlength: 3, maxlength: 1000},
                

            },
            messages : {
               // title        :  { required : Messages[ln].label_mandatory_field },
                //phone        :  { required : Messages[ln].label_mandatory_field },
               // description  :  { required : Messages[ln].label_mandatory_field },
            },
            highlight: function(element) {
                console.log(element)
                $(element).parent('.form-group').removeClass('noError').addClass('hasError');

            },
            unhighlight: function(element) {
                $(element).parent('.form-group').removeClass('hasError').addClass('noError');
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
});    