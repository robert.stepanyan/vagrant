
var map, geocoder, marker;
function initMap() {
    
    // first check if we have latitude and longitutde of object
    mapData = article = document.querySelector('#map');   
    if(("lat" in mapData.dataset) && ("long" in mapData.dataset)) {  // t check how we do it on edit, fml
        
         geocoder = new google.maps.Geocoder();
         var latlng = new google.maps.LatLng(mapData.dataset.lat, mapData.dataset.long);    
         
         map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 18
         });
        
         marker = new google.maps.Marker({position: latlng, map: map, draggable:false});
         getMarkerPosition(marker);
        
    } else {
    
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(-0.397, 5.644);
        
        map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 18
        });
        
        var mapData = article = document.querySelector('#map');   
        var address = mapData.dataset.city + ", " + mapData.dataset.country;
        codeAddress(address);
    }
}


function codeAddress(address) {
    console.log("address " + address);
    geocoder.geocode({ 'address': address }, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location,
                draggable:false,
            });
            
        } else {
            alert("Geocode unsuccessful");
        }
      });
}    



