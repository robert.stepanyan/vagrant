
window.addEventListener("load", function(){
// Get the modal
var modal = document.getElementById("report-profile");

// Get the button that opens the modal
var btn = document.getElementById("report-button");

var othertxt = document.getElementById("report-other");

// Get the <span> element that closes the modal
//var spans = document.getElementsByClassName("close-modal");

var showMore = document.getElementsByClassName("xreport-more");
var showMoreInput = document.getElementById("report-other-text");
showMore[0].onclick = function() {
  //othertxt.style.display = "block";
  showMoreInput.placeholder= other_placeholder;
}

// When the user clicks the button, open the modal 
btn.onclick = function() {
  $("#report_1").parent().children('label').html(rep_texts[1]);  
  $("#report_2").parent().children('label').html(rep_texts[2]);  
  $("#report_3").parent().children('label').html(rep_texts[3]);  
  $("#report_4").parent().children('label').html(rep_texts[4]);  
  modal.style.display = "flex";
}

// When the user clicks on <span> (x), close the modal
//span.onclick = function() {
//    console.log(span);
//    modal.style.display = "none";
//}

var spans = document.querySelectorAll('.close-modal')
spans.forEach(function(span) {

  span.addEventListener('click', function() {
    modal.style.display = "none";
  })
})


var hideit = document.getElementsByClassName("xreport");

for (var i = 0; i < hideit.length; i++) {
    hideit[i].addEventListener('click', function(){
       //othertxt.style.display = 'none'; 
       showMoreInput.placeholder= more_placeholder;
    }, false);
}


// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

});

jQuery(function ($) {
    if ($._customShowHide) {
        return;
    }
    $._customShowHide = true;
    var show = $.fn.show,
        hide = $.fn.hide;
    $.fn.show = function (speed, callback) {
        return $(this).each(function () {
            var obj = $(this);
            obj.trigger('show.custom');
            show.apply(obj, [speed, callback]);
   });
    };
    $.fn.hide = function (speed, callback) {
        return $(this).each(function () {
            var obj = $(this), visible = obj.is(':visible');
            hide.apply(obj, [speed, callback]);
            if (visible) {
                obj.trigger('hide.custom');
            }
        });
    };
});

$(function() {
    
    $("*[rel-data]").each(function () {
        //console.log($(this).attr('rel-data'));
        $(this).html ( $(this).attr('rel-data'));
    });

    $("#send-report").on("click", function(){
        
        var sendReport = true;
        $("#error_email").hide(); // make sure the rerror is hidden
        $("#error_general").hide(); 
        // console.log("send it");
        var mydata = new Object();
        mydata.type = $("input[name='report_form']:checked").val();
        mydata.adid = $("#adID").val();
        
        if(mydata.type == 4){
            mydata.more = $("#report-other-text").val();
        }
    
        if($( "#email" ).length) {

            console.log("input exists")
            console.log($("#email").val())
            
            if($("#email").val() == '') {
                sendReport = false;
                $("#error_email").show();        
            } else {
                mydata.email = $("#email").val();    
            }
        }
    
        //console.log(mydata);
        if(sendReport) {
            $.ajax({
                url: '/ad/report',
                method: 'post',
                data: mydata,
                dataType:"json",
                success: function (resp) {
                        //console.log(resp);
                        if(resp.success){
                            //$(".modal-body_report").html(resp.success);
//                            $(".modal-body_report").css('padding',"20px 10px");
//                            $(".modal-body_report").css('text-align',"center");
//                            $(".modal-body_report").css('font-size',"14px");
//                            $(".modal-body_report").css('color',"green");

                            $('.modal-body.report').hide();
                            $('.modal-body.success').show();
                                
                            $(".modal-footer.report").hide();
                        }
                        
                        if(resp.error){
                            $("#error_general").show();
                        }
                }
            });    
            
        }
    
    });

    // send message
    var sendMessageBtn = $('#send-message-button'), 
        messageModal = $('#send-message-modal'),
        messageConfirmModal = $('#send-message-sent-modal');

    if (sendMessageBtn.length && messageModal.length) {

        var closeModal = $('.modal-pm-close'),
            sendMessage = messageModal.find('#send-message'),
            errorNotSent = messageModal.find('#send-message-error-not-sent'),
            errorRequired = messageModal.find('#send-message-error-required'),
            errorMaxLength = messageModal.find('#send-message-error-max-length'),
            adId = messageModal.find('#send-message-ad-id'),
            message = messageModal.find('#send-message-body');

        $(messageModal).add(messageConfirmModal).bind('show.custom', function(){
            document.body.style.position = 'fixed';
        }).bind('hide.custom', function(){
            document.body.style.position = '';
        });
        
        sendMessageBtn.click(function () {
            messageModal.css("display", "flex");
            return false;
        });

        closeModal.click(function () {
            message.val('');
            messageModal.hide();
            messageConfirmModal.hide();
            errorRequired.hide();
            errorMaxLength.hide();
        });

        function checkMessage() {
            errorNotSent.hide();
            errorRequired.hide();
            errorMaxLength.hide();
            if (message.val().length == 0) {
                errorRequired.show();
                return false;
            }
            if (message.val().length > 500) {
                errorMaxLength.show();
                return false;
            }
            return true;
        }

        sendMessage.click(function (e) {
            if (!checkMessage()) {
                return false;
            }
            var data = {
                command: 'send',
                adId: adId.val(),
                message: message.val()
            };
            console.log(data);
            $.ajax({
                url: '/chat/command',
                method: 'post',
                data: data,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if (data.success) {
                        closeModal.trigger('click');
                        messageConfirmModal.css("display", "flex");
                        sendMessageBtn.attr('href', '/chat/view/id/' + data.chatId).unbind('click');
                    } else {
                        errorNotSent.show();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                    errorNotSent.show();
                }
            });
            return false;
        });
    }

});