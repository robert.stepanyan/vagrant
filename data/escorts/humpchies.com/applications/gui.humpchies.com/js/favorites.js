$(document).ready(function () {
    var Messages = {
        'en-ca': {
            not_allowed:                    'Action permitted only for "Member" account type.',
            added_fav:                      'Added to favorites',
            err_too_many:                   'You can not add more than 100 favorites. Please remove some before adding more.',
            
        },
        'fr-ca': {
            not_allowed:                    'Action permis seulement pour le type de compte "Membre".',
            added_fav:                      'Ajouter aux favoris',
            err_too_many:                   'Vous ne pouvez pas ajouter plus de 100 favoris. Nous vous prions d\'en retirer certains avant d\'en ajouter d\'autres.',
        },
    };
    var ln =document.documentElement.lang;
    //console.log("language " + ln);
    
    $("#btn_add_favorite").on('click', function() {
        
        if($("#no_favorites").val() > 100) {
            alert(Messages[ln].err_too_many); 
            return; 
        }
        
        //console.log("clicked")
        
        if($(this).data("ut") == 1) {
            alert(Messages[ln].not_allowed); 
            return;   
        } else {
            // add to favorites
            $.ajax({
                type: "GET",
                url: "/favorites/add?id=" + $(this).data('id'),
                cache: false,
                 dataType: 'JSON',
                success: function(result){
                    var obj = 
                  console.log(result);
                  //console.log('enterdeleted');  
                  if(result.success){
                      //console.log(result.deleted);
                      $("#btn_add_favorite").hide(); 
                      $("#btn_remove_favorite").show(); 
                  } else {
                      console.log("else")
                      alert(result.error)
                  }
                }
              });
        } 
        
    });
    
    $("#btn_remove_favorite").on('click', function() {
        
        console.log("remove clicked")
        
        if($(this).data("ut") == 1) {
            alert(Messages[ln].not_allowed); 
            return;   
        } else {
            // add to favorites
            $.ajax({
                type: "GET",
                url: "/favorites/remove?id=" + $(this).data('id'),
                cache: false,
                 dataType: 'JSON',
                success: function(result){
                  console.log(result);
                  //console.log('enterdeleted');  
                  if(result.success){
                      console.log(result.status);
                      $("#btn_add_favorite").show(); 
                      $("#btn_remove_favorite").hide(); 
                  } else {
                      console.log("else")
                      alert(result.error)
                  }
                }
              });
        } 
        
    });
    
    $(".btn-remove-fav").on('click', function() {
        
        console.log("remove clicked")
        console.log($(this).data("id"))
        var $btn = $(this);
        // remove from favorites
        $.ajax({
            type: "GET",
            url: "/favorites/remove?id=" + $(this).data('id'),
            cache: false,
             dataType: 'JSON',
            success: function(result){
              console.log(result);
              //console.log('enterdeleted');  
              if(result.success){
                  // hide box
                  $btn.closest(".user_content_list_element_box").hide();
                  $btn.closest(".add_list_box").hide();
              } else {
                  console.log("else")
                  alert(result.error)
              }
            }
          }); 
    });
    
});
 