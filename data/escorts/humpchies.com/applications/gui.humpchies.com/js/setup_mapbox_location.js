$(function () {
    init();
});

//
var myMarker, myMap, myNav;
function init() {
    console.log("here");
    
    $("#no_results").hide(); // hide no results
    var lat = 45.503601;    //Montreal
    var lng = -73.579995;
    
    
    mapboxgl.accessToken = 'pk.eyJ1IjoibWF4d2lydGhtYXBzIiwiYSI6ImNrbWFiNm5yaDFxMG8ycWtubDhvaXF6eGkifQ.zn5zSZgAcTS0Ksa7PT12jw';     
    myMap = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [lng, lat],
        zoom: 13
        });
    
    // add geocoder for search
    var geocoder = new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        mapboxgl: mapboxgl,
        marker: false
    });
    myMap.addControl(geocoder);
    // process geocoder result
    geocoder.on('result', function(e) {
        
        var cityLng = e.result.geometry.coordinates[0];
        var cityLat = e.result.geometry.coordinates[1];
        var context = e.result.context;
        
        // remove all markers
        if (myMarker) {
            myMarker.remove();// Destructor of the marker 
        }
            
        if(context[context.length - 1].text == 'Canada') {   // country here
             
            // add new marker
            myMarker = new mapboxgl.Marker()
                .setLngLat([cityLng, cityLat])
                .addTo(myMap);
        } else {
            // not sure what happens now  , must at least disable the btn 
        }
        
        // check if there are ads nearby and enable btn
        loadData(cityLat,cityLng);   
      });
    
    
    // Add zoom and rotation controls to the map.
   // myNav = new mapboxgl.NavigationControl();
//    myMap.addControl(myNav);
//
    
    
    // Add geolocate control to the map.
    var geolocate = new mapboxgl.GeolocateControl({
        positionOptions: {
            enableHighAccuracy: true
        },
        trackUserLocation: true
    })

    myMap.addControl(geolocate);

    geolocate.on('geolocate', function(e) {  // geolocation results
          var lon = e.coords.longitude;
          var lat = e.coords.latitude
          var position = [lon, lat];
          // check if there are ads nearby and enable btn
          loadData(lat, lon); 
    });
        
    // enable marker click
    myMap.on('click', addMarker);  // when clicking map, set marker
}

function addMarker(e) {
    console.log("here");
    if (myMarker) {
        myMarker.remove();// Destructor of the marker 
    } 
    
    myMarker = new mapboxgl.Marker()
        .setLngLat(e.lngLat)
        .addTo(myMap);
     

//    console.log(e.lngLat);
//    console.log(e.lngLat.lat);
    // check result and if there are any, enable btn
    loadData(e.lngLat.lat, e.lngLat.lng); // check result and if there are any, enable btn
}

function loadData(lat, lng) {
    console.log("loadData start")
    console.log(lat)
    console.log(lng)
    $("#no_results").hide(); // hide no results
    var searchUrl = '/json/markers?lat=' +  lat + '&long=' + lng;
    var mydata = new Object();
    mydata.lat = lat;
    mydata.long = lng;
     
    $.ajax({
        url: '/json/markers',
        method: 'get',
        data: mydata,
        dataType:"json",
        success: function (resp) {
            //console.log(resp.length);
            
            if(resp.status == 'have_results') {
                enableBtn(resp.count, lat, lng);
                
            } else {
                $("#show_list").addClass("disabled"); // disablebtn
                $("#show_list").attr("href",""); // disablebtn
                $("#ads_count").html('(0)');
                
                if(resp.status == 'nearest') {
                    
                    // update text
                    $("#city_name").html(resp.city)
                    
                    
                    // updade link/ btn
                    $("#show_list").removeClass("disabled");
                    $("#show_list").attr("href","/ad/nearby/lat/" + resp.lat + "/lng/" + resp.lng); 
                    $("#ads_count").html('');
                    
                    $("#show_near").attr("href","/ad/nearby/lat/" + resp.lat + "/lng/" + resp.lng); 

                    // scroll to div
                    $("#no_results").show();
                    //var element = document.getElementById("no_results");
//                        element.scrollIntoView(false);
                     $('html, body').animate({scrollTop: '+=100px'}, 800);
                }
            }   
        }
    });
}

function enableBtn(count, lat, lng) {
    
    $("#show_list").removeClass("disabled");
    $("#ads_count").html('('+ count + ')');
    // update btn href
   $("#show_list").attr("href","/ad/nearby/lat/" + lat + "/lng/" + lng); 
}


$(document).ready(function() {
    
    // hide list btn
    $("#show_list").hide();
    $("#map").hide();
     
    $("#show_map").click(function(){
        $("#show_list"). show();
        $("#map").show();    
        $("#activate_gps").hide();    
    }) ;
});