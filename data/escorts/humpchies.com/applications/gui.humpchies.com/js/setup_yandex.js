$(function () {
    ymaps.ready(init);
});

var firstGeoObject, myMap;

function init() {
    
    // get map info
    var $mapData = $('#map');
    var $hideMap = $('#hide_map');
    
    if (typeof $mapData.data('lat') !== 'undefined' && typeof $mapData.data('long') !== 'undefined') {          // THE MAP ALREADY HAS A MARKER
        //console.log("COORDS ARE SET");
        codeCoords($mapData.data('lat') , $mapData.data('long') );
        // enable hide/show map
        $hideMap.on("change", function() {
              
            if($(this).is(":checked")) {
                //console.log("Checkbox is checked")
                $('#map').css("height", "0px");
                $('#map').hide();
                $('#hide_map_check').val('yes');
            } else {
                //console.log("Checkbox is not checked")
                //show map
                $('#hide_map_check').val('no');
                $('#map').css("height", "400px");
                $('#map').show();
                var address = $("#maps_city").val() + ', ' + 'Canada';
                codeCoords($mapData.data('lat') , $mapData.data('long') );
            }
       });
        
        
    } else {
        // create new map
        //console.log("Lat long are totally not set");
        
        // enable hide/show map
        $hideMap.on("change", function() {
              
          if($(this).is(":checked")) {
                //console.log("Checkbox is checked")
                $('#map').css("height", "0px");
                $('#map').hide();
                $('#hide_map_check').val('yes');
          } else {
                //console.log("Checkbox is not checked")
                //show map
                $('#map').css("height", "400px");
                $('#map').show();
                var address = $("#maps_city").val() + ', ' + 'Canada';
                $('#hide_map_check').val('no');
                codeAddress(address);
          }
       });
        
    } 
   
    $("#city").on("change", function() {
        //console.log("City changed");
        $("#maps_city").val($(this).val()); // update city val 
        codeAddress($(this).val() + ', ' + 'Canada');
    });
    
}


function codeAddress(address) {
    //console.log("Started code address");
    var $hideMap = $("#hide_map");
    
    if (!$("#hide_map").is(":checked")) {  
        //console.log("The map is visible");           
         
        // Finding coordinates of the center of Montreal.
        ymaps.geocode(address, {
            /**
             * Request options
             * @see https://api.yandex.com/maps/doc/jsapi/2.1/ref/reference/geocode.xml
              */
            /**
             * Sorting the results from the center of the map window
             *  boundedBy: myMap.getBounds(),
             *  strictBounds: true,
             *  Together with the boundedBy option, the search will be strictly inside the area specified in boundedBy.
             *  If you need only one result, this will minimize the user's traffic.
             */
            results: 1
        }).then(function (res) {
                //myMap.destroy();
                if (myMap) {
                    myMap.destroy();// Destructor of the map
                    myMap = null;    
                }
                
                myMap = new ymaps.Map('map', {
                    center: res.geoObjects.get(0).geometry.getCoordinates(),
                    zoom: 14,
                    controls: ['zoomControl', 'typeSelector',  'fullscreenControl']
                });
            
                //console.log("I found the city");
                firstGeoObject = res.geoObjects.get(0); // set marker on map
                
                // The coordinates of the geo object.
                var coords = firstGeoObject.geometry.getCoordinates()

                firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
                firstGeoObject.options.set( 'draggable', 'true');
                
                // Adding first found geo object to the map.
                myMap.geoObjects.add(firstGeoObject);
                
                // initialize default coords
                $("#maps_lat").val(coords[0]);
                $("#maps_long").val(coords[1]);
                //console.log("coordinates" + coords);
                enableDrag(firstGeoObject); // enable dragging of the marker
                enableMapClick(myMap);      // when clicking map, set marker                 
            });
        
        
    } else {
        //console.log("hide map is checked")
        // do really nothing here
    }
    
}



function codeCoords(lat, lng) {
    //console.log("Started code address");
    var $hideMap = $("#hide_map");
    
    if (!$("#hide_map").is(":checked")) {  
        //console.log("The map is visible");           
        
        // check if maps is already set
        if (myMap) {
            myMap.destroy();// Destructor of the map
            myMap = null;    
        }
        
        myMap = new ymaps.Map('map', {
            center: [lat, lng],
            zoom: 14,
            controls: ['zoomControl', 'typeSelector',  'fullscreenControl']
        });

        firstGeoObject = new ymaps.GeoObject({
            // The geometry description.
            geometry: {
                type: "Point",
                coordinates: [lat, lng]
            }
        }, {
            /**
             * Options.
             * The placemark's icon will stretch to fit its contents.
             */
            preset: 'islands#darkBlueDotIconWithCaption',
            // The placemark can be dragged.
            draggable: true
        })
        myMap.geoObjects.add(firstGeoObject); // add marker to map

        // initialize default coords
        $("#maps_lat").val(lat);
        $("#maps_long").val(lng);
//        console.log("coordinates" + coords);
        
        //console.log("coordinates" + coords);
        enableDrag(firstGeoObject); // enable dragging of the marker
        enableMapClick(myMap);      // when clicking map, set marker   
        
    } else {
        //console.log("hide map is checked")
        // do really nothing here
    }
    
}

function enableDrag(geoObj) {
    
    geoObj.events.add('dragend', function (event) {   // when drag, udate coords
                        
        console.log(event.get("target").geometry.getCoordinates()[0]) 
        console.log(event.get("target").geometry.getCoordinates()[1]) 

        // update lat and long
        $("#maps_lat").val(event.get("target").geometry.getCoordinates()[0]);
        $("#maps_long").val(event.get("target").geometry.getCoordinates()[1]);
    }); 
    
}

function enableMapClick(myMap) {
    
    /**
     * Processing events that occur when the user
     * left-clicks anywhere on the map.
     * When such an event occurs, we open the balloon.
     */
    myMap.events.add('click', function (e) {
        console.log("map was clicked");
        var coords = e.get('coords');
        
        // update coords
        $("#maps_lat").val(coords[0]);
        $("#maps_long").val(coords[1]);
        
        // update marker position
        myMap.geoObjects.removeAll();
        
        firstGeoObject = new ymaps.GeoObject({
            // The geometry description.
            geometry: {
                type: "Point",
                coordinates: coords
            }
        }, {
            /**
             * Options.
             * The placemark's icon will stretch to fit its contents.
             */
            preset: 'islands#darkBlueDotIconWithCaption',
            // The placemark can be dragged.
            draggable: true
        })
        myMap.geoObjects.add(firstGeoObject); // add marker to map
        enableDrag(firstGeoObject)

    });   
    
}