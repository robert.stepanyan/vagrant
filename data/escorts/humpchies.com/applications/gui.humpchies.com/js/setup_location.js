var map, infoWindow, ocationSelect;
var markers = [];

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 45.50165694120211, lng: -73.56725063558198},
      zoom: 18
    });
    infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };

        var location = new google.maps.Marker({
          position: pos,
          icon: {
            path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
            scale: 5,
            fillColor: 'red',
            strokeColor: 'red',
            fillOpacity: 1,
            strokeOpacity: 0
          },
          draggable: false,
          map: map
        });
        
        // load data
        var searchUrl = '/ad/json_markers?lat=' + position.coords.latitude + '&long=' + position.coords.longitude;
        //console.log(searchUrl);
        downloadUrl(searchUrl, function(data) {
            
            var arrayData = JSON.parse(data);
            
            if(arrayData !== false) {
                console.log(data);
                console.log(JSON.parse(data).length);    
                
                var bounds = new google.maps.LatLngBounds();
                for (var i = 0; i < arrayData.length; i++) {
                    var id          = arrayData[i].id;
                    var ad_id       = arrayData[i].ad_id;
                    var thumb       = arrayData[i].thumb_info.src;
                    var title       = arrayData[i].title;
                    var url         = arrayData[i].url;
                    var description = arrayData[i].description;
                    console.log(thumb)     
                    console.log(title)     
                    console.log(description)     
                    console.log(url) 
                    
                    var latlng = new google.maps.LatLng(
                      parseFloat(arrayData[i].lat),
                      parseFloat(arrayData[i].long));
                    
                    createMarker(latlng, thumb, title,  description, url);  
                    bounds.extend(latlng);  
                }
                
                map.fitBounds(bounds);
            }
         });
        
        
        map.setCenter(pos);
        
        
      }, function() {
        handleLocationError(true, infoWindow, map.getCenter());
      });
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infoWindow, map.getCenter());
    }
  }

  function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                          'Error: The Geolocation service failed.' :
                          'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
  }
      

      

      
      
      
function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request.responseText, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
   }      
   
function createMarker(latlng, thumb, title,  description, url) {
  
    var html = '<div style="display:flex;">'+
    '<img src="' + thumb +'" style="float:left; margin-right:10px; max-height:100x; max-width: 100px;" />'+
    '<div style="float:left;">'+
        '<h1><a href="' + url + '" target="_blank" style="color:#ff9000">' + title + '</a></h1>'+
        '<p>' + description + '</p>'+
    '</div>'+
    '</div>';
  
  var marker = new google.maps.Marker({
    map: map,
    position: latlng
  });
  google.maps.event.addListener(marker, 'click', function() {
    infoWindow.setContent(html);
    infoWindow.open(map, marker);
  });
  markers.push(marker);
}   

function doNothing() {}