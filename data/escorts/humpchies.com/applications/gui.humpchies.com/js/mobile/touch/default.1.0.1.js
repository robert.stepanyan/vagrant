(function() {

    function hideAddressBar() {
        window.pageYOffset < 1 && window.scrollTo(0, 1)
    }
    
    window.addEventListener && window.addEventListener("load", hideAddressBar, !1),
        function(e) {
            "use strict";

            function n(e) {
                return new RegExp("(^|\\s+)" + e + "(\\s+|$)")
            }
            var s, t, i;

            function o(e, n) {
                (s(e, n) ? i : t)(e, n)
            }
            "classList" in document.documentElement ? (s = function(e, n) {
                return e.classList.contains(n)
            }, t = function(e, n) {
                e.classList.add(n)
            }, i = function(e, n) {
                e.classList.remove(n)
            }) : (s = function(e, s) {
                return n(s).test(e.className)
            }, t = function(e, n) {
                s(e, n) || (e.className = e.className + " " + n)
            }, i = function(e, s) {
                e.className = e.className.replace(n(s), " ")
            }), e.menuHumpchies = {
                hasClass: s,
                addClass: t,
                removeClass: i,
                toggleClass: o,
                has: s,
                add: t,
                remove: i,
                toggle: o
            }
        }(window);
    var menuRight = document.getElementById("right-menu"),
        showRight = document.getElementById("showRight"),
        body = document.getElementById("mainMenu");

    window.showRight = function(el) {
        menuHumpchies.toggle(el, "active"), menuHumpchies.toggle(menuRight, "menu-opened")
    };
    
    
    showRight.onclick = function() {
        menuHumpchies.toggle( this, 'active' );
        menuHumpchies.toggle( menuRight, 'menu-opened' );
        
        header.classList.toggle('full_height');
        headerWrp.classList.toggle('full_height');
        
    };


    /* Toggle Nav with Raw JavaScript */
    // Set variables for key elements
    var mainNav = document.getElementById('header_search_block');
    var navToggle = document.getElementById('search-button');
    var menubtns = document.getElementById('showRight');
    var srchClz = document.getElementById('srch_clz');
    
    var header = document.getElementById('header');
    var headerWrp = document.getElementById('header_wrapper');

    // Start by adding the class "collapse" to the mainNav
    mainNav.classList.add('collapsed');
    navToggle.classList.add('collapsed');

    // Establish a function to toggle the class "collapse"
    function mainNavToggle() {
        mainNav.classList.toggle('collapsed');
        navToggle.classList.toggle('collapsed');
        menubtns.classList.toggle('dnone');
        srchClz.classList.toggle('dnone');
        
        document.getElementById("query").focus();
    }

    // Add a click event to run the mainNavToggle function
    navToggle.addEventListener('click', mainNavToggle);
    //srchClz.addEventListener('click', );

    srchClz.onclick = function() {
        mainNav.classList.add('collapsed');
        menubtns.classList.toggle('dnone');
        srchClz.classList.toggle('dnone');
    }
    
    setTimeout(function() {
        var el = document.getElementsByClassName("success");
        var aNode = el[0];
        if(aNode){
            aNode.style.display = "none";   
        }
        
    }, 5000); // <-- time in milliseconds
    
    var safeBrowsing = document.getElementById("safe_browsing");
    var layout = document.getElementById("layout");
    function changeSafeBrowsing(){
        // set cookie
        if(safeBrowsing.checked == true) {
            layout.classList.add('safe_browsing');    
            setCookie("Sfb", '1', 30);
             
            setTimeout(function() {
                showRight.click();    
            }, 700); // <-- time in milliseconds
            
                   
        } else {
            layout.classList.remove('safe_browsing'); 
            setCookie("Sfb", '0', -30); 
            setTimeout(function() {
                showRight.click();    
            }, 700); // <-- time in milliseconds           
        }
    }
    if(safeBrowsing){    
        safeBrowsing.addEventListener('change', changeSafeBrowsing );
    }   
    
    var showAlert = document.getElementById("show_alert");
    var alerModal = document.getElementById("alert-modal");
    
    if(typeof(showAlert) != 'undefined' && showAlert != null && getCookie("phi-alert") == null) {
        alerModal.style.display = 'block';  
        // set cookie
        setCookie("phi-alert",'seen',30);   
    }
    
    document.getElementById("modal-close").onclick = function(){ 
        alerModal.style.display = 'none'; 
    } 
    
    
    function setCookie(cname, cvalue, exdays) {
          var d = new Date();
          d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
          var expires = "expires="+d.toUTCString();
          document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    
    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }
    
    
    
    var spans = document.getElementsByClassName("spans");
    for (var i = 0; i < spans.length; i++) {
       spansUpdate(spans.item(i));
    }
    
    function spansUpdate(span) {
      var label = span.getAttribute('rel-data');
      span.innerHTML = label;
    }
    
    
})();
