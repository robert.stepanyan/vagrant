$(document).ready(function () {
    
    $("#safe_browsing").on("click", function(){
       
       if($("#layout").hasClass("safe_browsing")) {
       
           $("#layout").removeClass("safe_browsing"); 
           setCookie("Sfb", '1', -30);
           
       } else {
           $("#layout").addClass("safe_browsing");
            setCookie("Sfb", '1', 30);    
       }
           
       toggleMenu(); // close menu
   });
    
   if($('#show_alert').length > 0 && $('#show_alert').val() == 'yes' && getCookie("phi-alert") == null) {
        $("#alert-modal").show();
        setCookie("phi-alert",'seen',30);
   }
   
   $(".modal-close").on("click",function(){
        $("#alert-modal").hide();
   });
   
   $("#search-button").on("click", function(e) {
       
       e.preventDefault();
       
       $(this).toggleClass("collapsed");
       
       $("#search_city_slug").val("");
       $("#search_city_header").val("");
       $("#search_city").val("");
        
       $("#search-form").toggle();
       
   });
   
   $("#search_city_header").keyup(function(){ // HEADER V2
        //console.log("keyup")
        $.ajax({
        type: "POST",
        url: "/city/search",
        data:'keyword='+$(this).val() + "&category_code=" + $("#category_search_city").val(),
        beforeSend: function(){
            $("#search_city_header").addClass("focused");
        },
        success: function(data){
            
            $("#suggesstion-search-city").html(''); 
            
            if(data.length > 2) {
                
                var obj = JSON.parse(data)
                $.each(obj, function(idx, city){
                    $("#suggesstion-search-city").show();
                    var cityOpt = '<li><a href="javacript:void(0)" class="suggested-city" data-slug="' + city.slug + '">' + city.name +' </a></li>';
                    $("#suggesstion-search-city").append(cityOpt);
                    
                    enableClickCity();
                });       
            }
        }
        });
   });
   
   $('#search_city').keypress(function (e) {
      if (e.which == 13) {
        
          $("#category_search_city").attr("disabled", true);
          $("#search_city_header").attr("disabled", true);
          $('form#header_search').submit();
        return false;    //<---- Add this line
      }
   });
   
   $("#open-account").on("click", function(e){
        e.preventDefault();   
       
        $(".account-menu").animate({
          left: 0
        }).addClass("opened"); 
   });
   
   
   $(".spans").each(function(index){
       $(this).html($(this).attr('rel-data'));
   });
   
   $("#showRight").on('click', function(e) {
      toggleMenu();
   });
   
   
   
});

function enableClickCity() {       // HEADER V2
    
    $(".suggested-city").on("click", function(){
        
        $("#search_city_header").val($(this).html());    
        $("#search_city_slug").val($(this).data('slug'));    
        $("#suggesstion-search-city").hide();
    });
}

function toggleMenu() {
    
    // toggle menu and classes
    console.log($(".account-menu").hasClass("opened"))
    // if submenu is opened
    if($(".account-menu").hasClass("opened")) {
        
        $(".account-menu").animate({
          left: '-100vw'
        }).removeClass("opened");   
    
    } else {
    
        //console.log("submenu not opened")
        //$("#header").toggleClass("full_height");
        $("#header").addClass("full_height");
        //$("#header_wrapper").toggleClass("full_height");
        $("#header_wrapper").addClass("full_height");
        $("#showRight").toggleClass("active");
        
        // hide search
        $("#search-button").toggle();
        $(".hump_logo").toggleClass("opened");
        
        // animate menu
        value = $('#right-menu').hasClass('menu-open') ? '-100vw' : '0';
        $("#right-menu").animate({left: value}, 100, 'linear').toggleClass("menu-open");
        
        if(!$('#right-menu').hasClass('menu-open')) {
            //console.log("has class menu open")
            $("#header").removeClass("full_height");
            $("#header_wrapper").removeClass("full_height");
            
        } else {
            //console.log("no class menu open")
            
        }
        
    }
}


function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

(function() {

    function hideAddressBar() {
        window.pageYOffset < 1 && window.scrollTo(0, 1)
    }
    
    window.addEventListener && window.addEventListener("load", hideAddressBar, !1),
        function(e) {
            "use strict";

            function n(e) {
                return new RegExp("(^|\\s+)" + e + "(\\s+|$)")
            }
            var s, t, i;

            function o(e, n) {
                (s(e, n) ? i : t)(e, n)
            }
            "classList" in document.documentElement ? (s = function(e, n) {
                return e.classList.contains(n)
            }, t = function(e, n) {
                e.classList.add(n)
            }, i = function(e, n) {
                e.classList.remove(n)
            }) : (s = function(e, s) {
                return n(s).test(e.className)
            }, t = function(e, n) {
                s(e, n) || (e.className = e.className + " " + n)
            }, i = function(e, s) {
                e.className = e.className.replace(n(s), " ")
            }), e.menuHumpchies = {
                hasClass: s,
                addClass: t,
                removeClass: i,
                toggleClass: o,
                has: s,
                add: t,
                remove: i,
                toggle: o
            }
        }(window);
    //var menuRight = document.getElementById("right-menu"),
//        showRight = document.getElementById("showRight"),
//        body = document.getElementById("mainMenu");
//
//    window.showRight = function(el) {
//        menuHumpchies.toggle(el, "active"), menuHumpchies.toggle(menuRight, "menu-opened")
//    };
//    
//    
//    showRight.onclick = function() {
//        menuHumpchies.toggle( this, 'active' );
//        menuHumpchies.toggle( menuRight, 'menu-opened' );
//        
//        header.classList.toggle('full_height');
//        headerWrp.classList.toggle('full_height');
//        
//    };


    /* Toggle Nav with Raw JavaScript */
    // Set variables for key elements
    //var mainNav = document.getElementById('header_search_block');
    //var navToggle = document.getElementById('search-button');
    var menubtns = document.getElementById('showRight');
    //var srchClz = document.getElementById('srch_clz');
    
    //var header = document.getElementById('header');
    //var headerWrp = document.getElementById('header_wrapper');

    setTimeout(function() {
        var el = document.getElementsByClassName("success");
        var aNode = el[0];
        if(aNode){
            aNode.style.display = "none";   
        }
        
    }, 5000); // <-- time in milliseconds
    
})();

