$(document).ready(function () {
    //var Messages = {
//        'en-ca': {
//            max_votes: "You need to have/create a client account in order to add a comment!",
//        },
//        'fr-ca': {
//            max_votes: "Veuillez vous connecter en tant que membre",
//        },
//    };
    
    
     
    
    $(".thumbsup").on('click', function () {
        //console.log("thumbs up");
//        console.log($(this).data("comment"));
//        console.log($(this).data("ad"));
        sendVote($(this).data("comment"), $(this).data("ad"), 'up');
        
    });
    
    $(".thumbsdown").on('click', function () {
        
        //console.log("thumbs down");
//        console.log($(this).data("comment"));
//        console.log($(this).data("ad"));
        sendVote($(this).data("comment"), $(this).data("ad"), 'down');
    });
});


function sendVote(comment_id, ad_id, vote) { // vote: up or down
    
    var ln = document.documentElement.lang;
    var languageIsDefault = window.location.pathname.indexOf(ln) < 0;
    var content = $("#body > .wrapper_content");
    
    var url = "/comments/vote";
    if (!languageIsDefault) {
        url = '/'+ln+url;
    }  
    
    $.ajax({
        url : url,
        method:'post',
        data: {
            comment_id:comment_id, 
            ad_id:ad_id, 
            vote:vote
        },
        dataType:'json',
        error:function(){
            content.removeClass('overlay');
        },
        beforeSend:function(){
            Noty.closeAll();
            content.addClass('overlay')
        },
        complete: function(){content.removeClass('overlay');},
        success:function (response) {
            if (Number(response.success) !== 1){
               new Noty({
                        layout: 'bottomRight',
                        theme: 'relax',
                        text: response.message,
                        type: 'error',
                        timeout: 1000,
                        progressBar: false,
                        modal: true,
                        closeWith: ["click"],
                        killer : true
                    }).show();
            } else {
                // stuff happens here
                if(typeof response.up != 'undefined') {
                    //object exists, do stuff
                    var upValue  = parseInt($("#up-" + comment_id).html()) + response.up;
                    
                    $("#up-" + comment_id).html(upValue)
                    if(response.up == 1 ){
                        $("#up-" + comment_id).siblings("a.thumbsup").addClass("active");
                    }
                    if(response.up == -1 ){
                        $("#up-" + comment_id).siblings("a.thumbsup").removeClass("active");
                    }
                }
                
                if(typeof response.down != 'undefined'){
                    //object exists, do stuff
                    var downValue  = parseInt($("#down-" + comment_id).html()) + response.down;
                    $("#down-" + comment_id).html(downValue)
                    if(response.down == 1 ){
                        $("#down-" + comment_id).siblings("a.thumbsdown").addClass("active");
                    }
                    if(response.down == -1 ){
                        $("#down-" + comment_id).siblings("a.thumbsdown").removeClass("active");
                    }
                }
                
                //if(typeof response.message != 'undefined' && response.message != ''){
//                    new Noty({
//                        //layout: 'topCenter',
//                        theme: 'relax',
//                        text: response.message,
//                        type: 'success',
//                        timeout: 1000,
//                        progressBar: false,
//                        modal: true,
//                        closeWith: ["click"],
//                        killer : true
//                    }).show();
//                }
            }
        }
    }) 
}