$(document).ready(function(){

     // open filters modal
     $("#filter-container").on("click", function(){
         $("body").addClass("modal_open");
         $("#header_wrapper").css("background-color", "#000000");
         $("#filters_box").show();
     });
     
     $("#close_filters").on("click", function(){
         $("body").removeClass("modal_open");
         $("#header_wrapper").css("background-color", "#222222");
         $("#filters_box").hide();
     });   
    
    
     // ==================================== //
     // AVAILABILITY //
     $("#availability_select").on("click", function() { // display availability radio buttons
         $("#availability_filter").show();
         
     });
    
     $("#close_availability_filter").on("click", function() { // close availability radio buttons
        //add radio to form
        var availName = $('input[name=location_filters]:checked').data("label");
        var availId = $('input[name=location_filters]:checked').val();
        $(".availability_name").remove();

        if(availId !== undefined) {

            $("#availability_select p").append('<span class="availability_name selected">' + availName + '</span>');

            // update location input
            $("#location").val(availId);    
            $("#location").removeAttr("disabled"); 

            $(".filter-icon").addClass("active");

        } else {
            $("#location").val('');        
            $("#location").prop("disabled", true); 

            // if serivces is also empty, remove active bubble
            if($("#services").prop("disabled") == true && $("#prices").prop("disabled") == true){
                $(".filter-icon").removeClass("active");
            }   
        }
        $("#availability_filter").hide();
         
     });
    
     $("#availability-btn").on("click", function() {    // ADD AVAILABILITY
         //add radio to form
        var availName = $('input[name=location_filters]:checked').data("label");
        var availId = $('input[name=location_filters]:checked').val();
        $(".availability_name").remove();
        
        if(availId !== undefined) {
            
            $("#availability_select p").append('<span class="availability_name selected">' + availName + '</span>');
            
            // update location input
            $("#location").val(availId);    
            $("#location").removeAttr("disabled"); 

            $(".filter-icon").addClass("active");

        } else {
            $("#location").val('');        
            $("#location").prop("disabled", true); 
            
            // if serivces is also empty, remove active bubble
            if($("#services").prop("disabled") == true && $("#prices").prop("disabled") == true){
                $(".filter-icon").removeClass("active");
            }   
        }
        
        $("#availability_filter").hide();
         
     });
    
     $("#clear_availability_filter").on("click", function() {
         // remove radio and location from form
         $("#availability_filter input:radio").prop("checked",false);    
     });
    
     // ==================================== //
     // SERVICES //
     $("#services_select").on("click", function() { // display availability radio buttons
         $("#services_filter").show();
         
     });
    
     $("#close_services_filter").on("click", function() { // close availability radio buttons
         
         $(".services_name").remove();
         
        if($('#services_filter input[name="selected_services[]"]:checked').length == 0) {
            $("#services").val('');        
            $("#services").prop("disabled", true);     
            
            // if serivces is also empty, remove active bubble
            if($("#location").prop("disabled") == true){
                $(".filter-icon").removeClass("active");
            } 
            
        } else {
            var Services = [];    
            var ServicesNames = [];    
            $('input[name="selected_services[]"]:checked').each(function() {
                var serviceName = $(this).data("label");
                var serviceId = $(this).val();    
                
                ServicesNames.push(serviceName);
                $(".services_name").html('');
                var servicesList =   ServicesNames.join(', ');
                servicesList = servicesList.replace(/,\s*$/, "");
                $("#services_select p").append('<span class="services_name selected">' + servicesList + '</span>');
                
                Services.push(serviceId);
                $("#services").val(Services.join(','));
                $("#services").removeAttr("disabled");   
            });
            
        }

         $("#services_filter").hide();
         
     });   
    
     $("#clear_services_filter").on("click", function() {
         // remove checkboxes
         $("#services_filter input:checkbox").prop("checked",false);
     });
    
     $("#services-btn").on("click", function() {
     
        $(".services_name").remove();
         
        if($('#services_filter input[name="selected_services[]"]:checked').length == 0) {
            $("#services").val('');        
            $("#services").prop("disabled", true);     
            
            // if serivces is also empty, remove active bubble
            if($("#location").prop("disabled") == true){
                $(".filter-icon").removeClass("active");
            } 
            
        } else {
            var Services = [];    
            var ServicesNames = [];    
            $('input[name="selected_services[]"]:checked').each(function() {
                var serviceName = $(this).data("label");
                var serviceId = $(this).val();    
                
                ServicesNames.push(serviceName);
                $(".services_name").html('');
                var servicesList =   ServicesNames.join(', ');
                servicesList = servicesList.replace(/,\s*$/, "");
                $("#services_select p").append('<span class="services_name selected">' + servicesList + '</span>');
                
                Services.push(serviceId);
                $("#services").val(Services.join(','));
                $("#services").removeAttr("disabled");   
            });
            
        }
        $("#services_filter").hide();  // close services
         
     });
    
     // ==================================== //
     addPrices();
     // ==================================== //
     // ADDITIONAL //
     $("#additional_select").on("click", function() { // display availability radio buttons
         $("#additional_filter").show();
     });
    
     $("#clear_additional_filter").on("click", function() {
         // remove checkboxes
         $("#additional_filter input:checkbox").prop("checked",false);
         $('#additional_filter input[type="text"]').val(''); 
     });
     
     $("#close_additional_filter").on("click", function() { // close availability radio buttons
        addMoreFilters(); 
     });
     
     $("#additional-btn").on("click", function() { // close availability radio buttons
        addMoreFilters();
     });
    
     // ==================================== //
    
     $(".clear-all").on('click', function() { // clear filters
        // remove checkboxes and radio
        $("#availability_filter input:radio").prop("checked",false);
        $("#services_filter input:checkbox").prop("checked",false);
        $("#prices_filter input:checkbox").prop("checked",false);
        $('#additional_filter input[type="tel"]').val(''); 
        
        // remove hidden inputs 
        $("#location").val(""); 
        $("#services").val("");
        $("#prices").val("");
        $("#has_comments").val("");
        $("#real_photos").val("");
        $("#accepts_comments").val("");
        $("#phone1").val(""); 
        $("#phone2").val(""); 
        $("#phone3").val("");
        
        // remove class active from icon 
        $(".filter-icon").removeClass("active");
        // clear texts from filters modal
        $(".services_name").html('');
        $(".prices_name").html('');
        $(".availability_name").html('');
        $(".city_name").html('');
        $(".additional_name").html('');
        
        // clear city from url
        var baseURL = '/ad/listing-v2';
        if($("#lang").val() == 'fr-ca') {
            baseURL = '/fr-ca/listing-v2';
        }
        $("#filters_form").attr("action", baseURL); 
    });
    
    /******* SUBMIT FORM ****************/
    
     // ===============================//
     //*** CITIES SECTION
     
     // open city modal
     $("#location-container").on("click", function(){
         //$("body").addClass("modal_open");
         //$("#header_wrapper").css("background-color", "#000000");
         $("#location_box").show();
     });
     
     // close city modal
     $("#close_location").on("click", function(){
         //$("body").removeClass("modal_open");
         //$("#header_wrapper").css("background-color", "#222222");
         $("#location_box").hide();
     }); 
    
    $(".zone_select").on("click",function(){
        
        $(this).toggleClass("active");
        $(this).find( ".dropdown_cities" ).toggle();
    });
    
    $(".category-filter-box").on("click", function(e){
         e.preventDefault();    
         $("#categories-list").stop(true,true).slideToggle();
   });
     
});


function addMoreFilters() {
    //add checkboxes
    console.log("ADDITIONAL FILTERS apply")
    $(".additional_name").remove();
    var Filters = [];    
    var FiltersNames = []; 
    var phoneNames = [];
    var phoneList = '';
    $('#additional_filter').find(':checkbox').each(function() {
    
        // if filter is checked 
        if($(this).is(":checked")) {
            var filterName = $(this).data("label");
            var filterId = $(this).val();  
            
            $("#" + filterId).val(1);
            
            FiltersNames.push(filterName);
           
            $(".additional_name").html('');
            
            $(".filter-icon").addClass("active");
        
        } else {
            var filterId = $(this).val();   
            $("#" + filterId).val('');        
        }
    });
    
    //validate and add phone number search
    $(".phone_section").each(function(){
        
        var filterId = $(this).data("value");     // phone1 or phone2 or phone3
        var filterValue = $(this).val();  // example 312
        
        if(filterValue != '' && $.isNumeric(filterValue)) {      // input value is numeric
                
                $("#" + filterId).val(filterValue);       // add value to hidden input
                
                // create a complex phone number label to be displayed
                phoneNames.push(filterValue);
                phoneList =   phoneNames.join('.');
                phoneList = phoneList.replace(/\.\s*$/, "");
                
                
                $(".filter-icon").addClass("active");   
                $(".additional_name").html('');
            
            } else {
                 $(this).val('');
            $("#" + filterId).val('');  // clear hidden input    
        }
    });
    
    
    FiltersNames.push(phoneList);
    console.log(FiltersNames);
    var filtersList =   FiltersNames.join(', ');
    filtersList = filtersList.replace(/,\s*$/, "");
    $("#additional_select p").append('<span class="additional_name selected">' + filtersList + '</span>'); 
   
    
    $("#additional_filter").hide();
}


function addPrices() {
    
    // PRICES //
     $("#prices_select").on("click", function() { // display availability radio buttons
         $("#prices_filter").show();
         
     });
    
     $("#close_prices_filter").on("click", function() { // close availability radio buttons
         
         $(".prices_name").remove();
         
        if($('#prices_filter input[name="selected_prices[]"]:checked').length == 0) {
            $("#prices").val('');        
            $("#prices").prop("disabled", true);     
            
            // if serivces is also empty, remove active bubble
            if($("#location").prop("disabled") == true && $("#services").prop("disabled") == true){
                $(".filter-icon").removeClass("active");
            } 
            
        } else {
            var Prices = [];    
            var PricesNames = [];    
            $('input[name="selected_prices[]"]:checked').each(function() {
                var priceName = $(this).data("label");
                var priceId = $(this).val();    
                
                PricesNames.push(priceName);
                $(".prices_name").html('');
                var pricesList =   PricesNames.join(', ');
                pricesList = pricesList.replace(/,\s*$/, "");
                $("#prices_select p").append('<span class="prices_name selected">' + pricesList + '</span>');
                
                Prices.push(priceId);
                $("#prices").val(Prices.join(','));
                $("#prices").removeAttr("disabled");   
            });
            
        }

         $("#prices_filter").hide();
         
     });   
    
     $("#clear_prices_filter").on("click", function() {
         // remove checkboxes
         $("#prices_filter input:checkbox").prop("checked",false);
     });
    
     $("#prices-btn").on("click", function() {
     
        $(".prices_name").remove();
         
        if($('#prices_filter input[name="selected_prices[]"]:checked').length == 0) {
            $("#prices").val('');        
            $("#prices").prop("disabled", true);     
            
            // if serivces is also empty, remove active bubble
            if($("#location").prop("disabled") == true && $("#services").prop("disabled") == true){
                $(".filter-icon").removeClass("active");
            } 
            
        } else {
            var Prices = [];    
            var PricesNames = [];    
            $('input[name="selected_prices[]"]:checked').each(function() {
                var priceName = $(this).data("label");
                var priceId = $(this).val();    
                
                PricesNames.push(priceName);
                $(".prices_name").html('');
                var pricesList =   PricesNames.join(', ');
                pricesList = pricesList.replace(/,\s*$/, "");
                $("#prices_select p").append('<span class="prices_name selected">' + pricesList + '</span>');
                
                Prices.push(priceId);
                $("#prices").val(Prices.join(','));
                $("#prices").removeAttr("disabled");   
                
            });
            
        }
        $("#prices_filter").hide();  // close services
         
     });
    
}
