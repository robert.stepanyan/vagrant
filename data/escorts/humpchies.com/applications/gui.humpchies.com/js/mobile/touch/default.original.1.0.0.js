function hideAddressBar() {
	if (window.pageYOffset < 1){
		window.scrollTo(0,1);
	}
}
if (window.addEventListener){
	window.addEventListener("load",hideAddressBar,false);
}

( function( window ) {

'use strict';

function classReg( className ) {
  return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
}
var hasClass, addClass, removeClass;

if ( 'classList' in document.documentElement ) {
  hasClass = function( elem, c ) {
    return elem.classList.contains( c );
  };
  addClass = function( elem, c ) {
    elem.classList.add( c );
  };
  removeClass = function( elem, c ) {
    elem.classList.remove( c );
  };
}
else {
  hasClass = function( elem, c ) {
    return classReg( c ).test( elem.className );
  };
  addClass = function( elem, c ) {
    if ( !hasClass( elem, c ) ) {
      elem.className = elem.className + ' ' + c;
    }
  };
  removeClass = function( elem, c ) {
    elem.className = elem.className.replace( classReg( c ), ' ' );
  };
}

function toggleClass( elem, c ) {
  var fn = hasClass( elem, c ) ? removeClass : addClass;
  fn( elem, c );
}

window.menuHumpchies = {
  hasClass: hasClass,
  addClass: addClass,
  removeClass: removeClass,
  toggleClass: toggleClass,
  has: hasClass,
  add: addClass,
  remove: removeClass,
  toggle: toggleClass
};

})( window );

var menuRight = document.getElementById( 'right-menu' ),
	showRight = document.getElementById( 'showRight' ),
	body = document.getElementById('mainMenu');


showRight.onclick = function() {
	menuHumpchies.toggle( this, 'active' );
	menuHumpchies.toggle( menuRight, 'menu-opened' );
};