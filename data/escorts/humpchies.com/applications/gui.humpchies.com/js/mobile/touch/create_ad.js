$().ready(function () {

     $("#tips").on("click",function(){
       // $(".tips-container").stop(true,true).slideToggle()
       $("#tips-modal").show();
       //return false;
    });
    $(".modal-close").on("click",function(){
        $("#tips-modal").hide();
    });
    if($('#show_tips').length > 0 && $('#show_tips').val() == 'yes') {
        $("#tips-modal").show();
    }
    //$("#tips").on("click",function(){$(".tips-container").stop(true,true).slideToggle()});
    
    var Messages = {
        'en-ca': {
            img_count_reached:"You can upload 5 Images",
            invalid_file_type: 'Only .jpg files can be uploaded',
            label_montreal_main: 'Main',
            label_montreal_escort: 'Escorts',
            label_montreal_escort_agencies: 'Escort agencies',
            label_montreal_massage: 'Massage',
            label_montreal_parlor: 'Massage Parlors',
            label_mandatory_field: 'This field is mandatory.',
            label_image_dim_square:         'Image dimension should be bigger than 400X400',
            label_image_dim_landscape:      'Image dimension should be bigger than 600X400',
            label_image_dim_portrait:       'Image dimension should be bigger than 400X600',
            label_image_size:               'Image size should be less than 5MB',
            video_size:                     'Video size should be less than 50MB',
            label_delete_photo:             'Are you sure you want to delete this photo?',
            remove_video:                   'Are you sure you want to remve this video from your collection?',
            upload_complete:                'Upload Completed. Saving file.',
            select:                         'SELECT',
        },
        'fr-ca': {
            img_count_reached:"Vous pouvez t\u00E9l\u00E9charger 5 images",
            invalid_file_type: 'Seuls les fichiers .jpg peuvent \u00EAtre t\u00E9l\u00E9charg\u00E9s',
            label_montreal_main: 'Main',
            label_montreal_escort: 'Escortes',
            label_montreal_escort_agencies: "Agences d\'Escortes",
            label_montreal_massage: 'Massages Erotiques',
            label_montreal_parlor: 'Salons de massage',
            label_mandatory_field: 'Ce champ est obligatoire.',
            label_image_dim_square:         'La dimension de l\'image doit \u00EAtre sup\u00E9rieure \u00E0 400X400',
            label_image_dim_landscape:      'La dimension de l\'image doit \u00EAtre sup\u00E9rieure \u00E0 600X400',
            label_image_dim_portrait:       'La dimension de l\'image doit \u00EAtre sup\u00E9rieure \u00E0 400X600',
            label_image_size:               'Dimension de l\'image doit \u00EAtre inf\u00E9rieure \u00E0 5MB',
            label_delete_photo:             '\u00CAtes-vous s\u00FBrr de vouloir effacer cette photo ?',
            video_size:                     'Dimension de l\'video doit \u00EAtre inf\u00E9rieure \u00E0 50MB',
            remove_video:                   'Voulez-vous vraiment supprimer cette vid\u00E1o de votre collection? ',
            upload_complete:                'T\u00E1lu00E1chargement compl\u00E9t\u00E9. Sauvegarde du fichier.',
            select:                         'S\u00EALECTIONN\u00EA',
        },
    };
    var ln = document.documentElement.lang;
    /*Dropdown Menu*/
    $('.dropdown').click(function () {
        $(this).attr('tabindex', 1).focus();
        $(this).toggleClass('active');
        $(this).find('.dropdown-menu').slideToggle(300);
    }).focusout(function () {
        $(this).removeAttr('tabindex');
        $(this).removeClass('active');
        $(this).find('.dropdown-menu').slideUp(300);
    });
    $('.dropdown .dropdown-menu li').click(function () {
        $(this).parent().find('li').removeClass('selected');
        $(this).parents('.dropdown').find('span.label').text($(this).text());
        $(this).parents('.dropdown').find('input').attr('value', $(this).attr('value'));
        $(this).addClass('selected');
        if ($(this).hasClass('package') || $(this).hasClass('mtl_category')) {
            get_price()
        }else if($(this).hasClass('category')){
            var cat = $(this).attr('value').trim();
            var txt  = '';
            switch (cat) {
                case "escort":
                    txt = Messages[ln].label_montreal_escort;
                    break;
                case "escort-agency":
                    txt = Messages[ln].label_montreal_escort_agencies;
                    break;
                case "erotic-massage":
                    txt = Messages[ln].label_montreal_massage;
                    break;
                case "erotic-massage-parlor":
                    txt = Messages[ln].label_montreal_parlor;
                    break;
            }
            $("input[name='mtl_category']").val('mtl_'+cat);
            var li = $("#mtl_category li:last-child");
            li.attr('value','mtl_'+cat).text(txt);
            if (li.hasClass('selected')) {
                li.parents('.dropdown').find('span.label').text(li.text())
            }
        }
    });
    /*Dropdown Menu END*/

    var index = 0;
    var currentIndex = -1;
    $('.btn-select_pics').click(function (e) {
        e.preventDefault();
        if (index < 5){
            var selector = "#images-"+(currentIndex < 0 ? index : currentIndex);
            $(selector).trigger('click');
            currentIndex = -1;
            
            if(index == 4)
                $(this).addClass("disabled");
        }
    });

    function getNextIndex(current){
        var indexes = [];
        if ($("div.image-" + current).length === 0){
            return current;
        }else{
            if($("div.image-0").length === 0){
                return 0;
            }
            if($("div.image-1").length === 0){
                return 1;
            }
            if($("div.image-2").length === 0){
                return 2;
            }
            if($("div.image-3").length === 0){
                return 3;
            }
            if($("div.image-4").length === 0){
                return 4;
            }
        }
    }

    $("input[id^='images-']").on('change', function(e){
        var i = $(this).attr('id').split('-')[1];
        i = Number(i);
        var files = this.files;
        if(files.length < 1){
            return;
        }
        var valid = true;
        if(files[0].type !== 'image/jpeg'){
            valid = false;
        }

        var FileSize = this.files[0].size / 1024 / 1024; // in MB
        if (FileSize > 5) {
            alert(Messages[ln].label_image_size);
            return;
        }
        
        if (!valid){
            e.preventDefault();
            alert(Messages[ln].invalid_file_type);
            return;
        }

        // $("#e_i").val('');
        if (files.length > 1) {
            alert("You can select 1 Image");
            return;
        }

        var reader = new FileReader();

        var file = files[0];
        reader.onload = function (e) {
             var dimensionsValid = true;
            // validate width, height
            var image = new Image();
            image.src = reader.result;
            image.onload = function() {
                console.log("image.width " + parseInt(image.width) < 400 )
                console.log("image.height " + image.height )
                // 4 cases
                if(image.width == image.height && parseInt(image.width) < 400) {
                    e.preventDefault();
                    alert(Messages[ln].label_image_dim_square);
                    dimensionsValid = false;
                    return;
                }
                
                if((image.width > image.height) && (image.width < 600 || image.height < 400)) {
                    e.preventDefault();
                    alert(Messages[ln].label_image_dim_landscape);
                    dimensionsValid = false;
                    return;    
                    
                }
                
                if((image.width < image.height) && (image.width < 400 || image.height < 600)) {
                    e.preventDefault();
                    alert(Messages[ln].label_image_dim_portrait);
                    dimensionsValid = false;
                    return;    
                    
                }
                
                
            var div = $("<div />")
                .data('key',index)
                .attr('class', 'image image-'+i);
                 var actions = $("<span/>").addClass('actions');
                 actions.append(
                     $('<span/>').addClass('set-main').text('Main')
                 );
                 actions.append(
                     $('<span/>').addClass('remove').text('delete')
                 );
                 actions.append(
                    $('<span/>').addClass('rotate').append(
                      $('<img src="https://gui.humpchies.com/images/cw.svg" />').addClass('cw')
                    ).append(
                      $('<img src="https://gui.humpchies.com/images/cw.svg" />').addClass('ccw')
                    )
                ); 
                
                div.append(actions);

            $('<img alt="" src=""/>')
                .data('key', index)
                .addClass('prev_img')
                .attr('src', e.target.result)
                .appendTo(div);

            $('#preview_images').append(div);
            $(this).find('.actions').show()
            //div.on("mouseenter", function () {
//                $(this).find('.actions').show()
//            }).on("mouseleave", function () {
//                $(this).find('.actions').hide()
//            });
            index = getNextIndex(i);
            if ( i === 0 ){
                markMain()
                }   
            };
            
        };

        reader.readAsDataURL(file);

    });

    function markMain() {
        var main = $('#preview_images').find('.image').first();
        main.addClass('main');
        var i = main.find('.prev_img').data('key');
        $('#main-img').val(i)
    }

    $("body").on('click', ".set-main", function (e) {
        $('#preview_images').find('.image').removeClass('main');
        var i = $(this).parents('.image').addClass('main').find('.prev_img').data('key');
        $('#main-img').val(i)
    });

    $("body").on('click', "span.remove", function (e) {
        
        var result = confirm(Messages[ln].label_delete_photo);
        if (result) {
        currentIndex = $(this).parents('.image').removeClass('main').find('.prev_img').data('key');
        $(this).parents('.image').remove();
        var selector = "#images-"+currentIndex;
        $(selector)[0].value = $(selector)[0].defaultValue;
        markMain();
        index = getNextIndex(currentIndex);
        if (index === 0){
            currentIndex = -1;
        }
        // remove disabled from button
        $('.btn-select_pics').removeClass("disabled")
        }
        
        
    });
    
    $("body").on('click', "img.cw",function (e) {
        currentIndex = $(this).parents('.image').find('.prev_img').data('key');
        var rotation = $("#rotator-" + currentIndex);
        rotation.val(parseInt(rotation.val()) + 90);
      
        if(parseInt(rotation.val()) >= 360){
            rotation.val( parseInt(rotation.val()) - 360);
        } 
        if(parseInt(rotation.val()) <= -360){
            rotation.val( parseInt(rotation.val()) + 360);
        } 
    
        $(this).parents('.image').find('.prev_img').css('transform', 'rotate(' + rotation.val() + 'deg)');
    });

    $("body").on('click', "img.ccw",function (e) {
        currentIndex = $(this).parents('.image').find('.prev_img').data('key');
        var rotation = $("#rotator-" + currentIndex);
        rotation.val(parseInt(rotation.val()) - 90);
        
        if(parseInt(rotation.val()) > 360){
            rotation.val( parseInt(rotation.val()) - 360);
        } 
        if(parseInt(rotation.val()) < -360){
            rotation.val( parseInt(rotation.val()) + 360);
        } 
        
        $(this).parents('.image').find('.prev_img').css('transform', 'rotate(' + rotation.val() + 'deg)');
    });
    
    
    //function get_price () {
//        $('.payment-section').addClass('overlay');
//        var pckg = $("input[name='package']").val();
//        var mtl_category = $("input[name='mtl_category']").val();
//        $.ajax({
//            type: "POST",
//            url: '/payment/get-package-price',
//            dataType: 'json',
//            data: {package:pckg, category:mtl_category},
//            beforeSend: function() {
//                $('.payment-section').addClass('overlay');
//           },
//            success: function (res) {
//                if (res.status == 1) {
//                   $('#amount').text(parseInt(res.price))
//                } else {
//                    $('.errors').append('Unexpected error #152422 please contact support ');
//                }
//                $('.payment-section').removeClass('overlay');
//            },
//            error: function() {
//                $('.payment-section').removeClass('overlay');
//            },
//        })
//    }
   
    $(".upload-private").on("click", function(){
       $("#private").trigger("click"); 
       return false;
    });

    $("#private").uploader({
        maxNumberOfFiles : 5,
        ajaxUrl: '/verification/images',
        ajaxAdditionalFields:['adid'],
        callbackFunction: "fillUploadedFiles",
    });
    $(".radio-verified").click(function(){
        if($(this).hasClass('verified-yes') && !$(this).hasClass('radio-selected')){
            $(this).addClass('radio-selected');
            $(".verified-no").removeClass("radio-selected");
            $("#existing-ad").show();
            $(".exiting-skip").show();
            $(".upload-private").hide();
            $("#new-photos").hide();
            $(".upload-gallery-preview-content").show();
            $(".skip-verify-files").hide();
            $("#existsAd").val('yes');
        }
        
        if($(this).hasClass('verified-no') && !$(this).hasClass('radio-selected')){
            $(this).addClass('radio-selected');
            $(".verified-yes").removeClass("radio-selected");
            $("#existing-ad").hide();
            $("#new-photos").show();
            $(".exiting-skip").hide();
            $(".upload-private").show();
            $(".upload-gallery-preview-content").hide();
            $(".skip-verify-files").show();
            $("#existsAd").val('no');
        }
    });
    
    $("#submitVerification").on('click', function(){
         if(uploadedFiles.length < 1 ){
               
               return false;
           }
        if($("#existsAd").val() == 'yes' && $("#ad_id").val() == "" ){
            alert("Please fill the already approved by Ad Id.");
        }else{
            if($("#existsAd").val() == 'yes'){
                var keys = {
                         old : $("#ad_id").val()
                        };
                $.ajax({
                    type: 'POST',
                    url: "/ad/checkold",
                    data: keys,
                    cache: false,
                    dataType: 'JSON',
                    success : function(data){
                        if(data.status == 'error'){
                            alert(data.error);
                        }else{
                             $("#verification-form").submit();
                        } 
                    }
                });
            }else{
                $("#verification-form").submit();   
            }
        }  
    });
    
    $("#post_pad").validate({
        rules : {
            title       :  { required: true},
            phone       :  { required: true},
            description :  { required: true},
            city        :  { required: true},
        },
        messages : {
            title       :  { required : Messages[ln].label_mandatory_field },    
            phone       :  { required : Messages[ln].label_mandatory_field },    
            description :  { required : Messages[ln].label_mandatory_field },
            city        :  { required : Messages[ln].label_mandatory_field },
        },
        highlight: function(element) {
            $(element).parent('.form-group').removeClass('noError').addClass('hasError');
            
        },
        unhighlight: function(element) {
            $(element).parent('.form-group').removeClass('hasError').addClass('noError');
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    
    
    $('.btn-select_video').on('click',function (e) {
           // e.preventDefault();
            console.log('toggle');
            $("#video-upload").trigger('click');
            
        });
        //--- UploadVideo
        
        $("#video-upload").change( function(e){
            e.preventDefault();
            if($(this)[0].files[0].size > 52428800) {
                alert(Messages[ln].video_size);
                return false;
            }
            $(".progress-bar").show();
            var formData = new FormData();
            formData.append('videoupload', $(this)[0].files[0]);
            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = Math.floor((evt.loaded / evt.total) * 100);
                            console.log(percentComplete);
                            $(".progress-bar").html(percentComplete+'%');
                            if(percentComplete>= 96){
                                $(".progress-bar").html("Upload Complete. Saving file.");
                            }
                            if(percentComplete>= 96) percentComplete = 96;
                            $(".progress-bar").width(percentComplete + '%');
                               
                        }
                        
                    }, false);
                    return xhr;
                },
                type: 'POST',
                url: '/ad/videoupload',
                data: formData,
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function(){
                    $(".progress-bar").width('0%');
                  //  $('#uploadStatus').html('<img src="images/loading.gif"/>');
                },
                error:function(){
                    $('#uploadStatus').html('<p style="color:#EA4335;">File upload failed, please try again.</p>');
                    $(".progress-bar").width('96%');
                        
                },
                success: function(resp){
                    $(".progress-bar").hide();
                    console.log(resp);
                    if(resp == 'ok'){

                    }else if(resp == 'err'){
                        $('#uploadStatus').html('<p style="color:#EA4335;">Please select a valid file to upload.</p>');
                        return;
                    }
                    var obj = JSON.parse(resp)
                    $(".video-preview").removeClass("selected");
                    $("#attached-video").val(obj.vid);
                    var item = $("<div>").addClass("video-preview").addClass("new").addClass("selected").attr("rel-movie", obj.movie).attr("rel-id", obj.vid).css('background-image', "url(" + obj.movie+'.jpg)');
                    $("<div>").addClass("video-remove").appendTo(item);
                    $("<div>").addClass("video-select").html(Messages[ln].select).appendTo(item);
                    $("#gallery_videos").append(item);
                    jQuery("div#gallery_videos").scrollLeft(jQuery("div#gallery_videos")[0].scrollWidth);
                }
            });
        });
        $(".video-modal-close").on('click', function(){
            $("#play-video-modal").hide();
        });
        
        $("#gallery_videos").on("click",".video-select", function(event){
            event.stopPropagation()
            $(".video-preview").removeClass("selected");
            $(this).parent().addClass("selected"); 
            $("#attached-video").val($(this).parent().attr('rel-id'));   
        });
        
        $("#gallery_videos").on("click",".video-remove", function(event){
            event.stopPropagation();
            if(window.confirm(Messages[ln].remove_video)){
              $.ajax({
                type: "GET",
                url: "/ad/deletevideo?id=" + $(this).parent().attr('rel-id'),
                cache: false,
                 dataType: 'JSON',
                success: function(result){
                    var obj = 
                  console.log(result.deleted);
                  console.log('enterdeleted');  
                  if(result.deleted){
                      console.log(result.deleted);
                     $("div[rel-id='" + result.deleted + "'").remove(); 
                  }
                }
              });
            }
        });

        $("#gallery_videos").on("click",".video-preview", function(){
            var location =  $(this).attr("rel-movie");
           $("#movie-load").html('<source src="' + location + '" type="video/mp4"></source>' ); 
           $(".play-video-modal").show();
           $("#movie-load").load();;
        });
         $('#movie-load').bind('contextmenu',function() { return false; });
        //--------------------------------------------------------------------------------------
   
});

  var uploadedFiles = new Array();
    function fillUploadedFiles(data, index){
        var obj = new Object();
        for(var j = 0 ; j < data.length; j++){
            obj.filename = data[j].filename;
            obj.index    = index;
            uploadedFiles.push(obj);
            console.log(uploadedFiles);
        }
        resetfileList();
    }
    
    function removeUpload(index){
         for (var i = 0; i < uploadedFiles.length; i++) {
            if (uploadedFiles[i].index === parseInt(index)) {
                uploadedFiles.splice(i, 1);
                break;
            }
         }  
         resetfileList();  
    }
    
    function resetfileList(){
        var newList = new Array();
        for( var i = 0; i < uploadedFiles.length; i++){
            newList.push(uploadedFiles[i].filename);
        }
        
        $("#uploaded-files").val(newList.join(","));
    }
