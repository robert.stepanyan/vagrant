function Loader(container) {
    this.container = container;
    this._init();
}
Loader.prototype._init = function () {
    this.loaderBox = $("<div/>").addClass('flexbox').css({display:'none'});
    var div = $("<div/>");
    $('<div/>').addClass("triple-spinner").appendTo(div);
    div.appendTo(this.loaderBox);
    this.loaderBox.appendTo(this.container);
};

Loader.prototype.enable =function () {
    this.loaderBox.css({display:'none'})
};
Loader.prototype.disable =function () {
    this.loaderBox.css({display:'block'})
};