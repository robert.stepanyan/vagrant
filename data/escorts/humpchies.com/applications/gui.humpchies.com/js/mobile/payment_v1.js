$(function () {

    $('#paymentForm').on('submit', function (e) {

        var $amount = $('#amount-input')

        if (!+$amount.val() || !$amount.val().trim().length || !$.isNumeric($amount.val())) {
            $amount.addClass('invalid');
            e.preventDefault();
            return false;
        }

    })

    $('input[type="number"]').keydown(function (e) {
        var charCode = (e.which) ? e.which : event.keyCode
        if ((charCode > 31 && (charCode < 48 || charCode > 57)) && !(charCode >= 96 && charCode <= 105)) {
            e.preventDefault();
            return false;
        }
    })

    $('.fancy-form input')
        .focus(function () {
            $(this).parents('.fancy-form').addClass('focused');
        })
        .blur(function () {
            if (!$(this).val().length) {
                $(this).parents('.fancy-form').removeClass('focused');
            } else {
                $(this).parents('.fancy-form').addClass('focused');
            }
        })
        .keydown(function () {
            if ($(this).val().length) {
                $(this).parents('.fancy-form').addClass('focused');
            }
        })
})