$(function () {
    $("#sc-form").submit(function(e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.
    $('#sc-form-submit').prop('disabled', true);

    var form = $(this);
    var url = form.attr('action');
    $('.wrapper_content').addClass('overlay');
    $('.errors').empty();

    $.ajax({
           type: "POST",
           url: url,
           dataType: 'json',
           data: form.serialize(), // serializes the form's elements.
           success: function(res)
           {
    
             if( res.status == 1 ){
               if(res.pp !='card'){
                     $("#bitamount").val(res.a); 
                     // $("#bitlang").val(res.lang); 
                     if(parseInt($("#bitamount").val()) != 0){
                        $("#bitform").attr('action', res.url).submit(); 
                     } 
                   }else{
                        window.location.replace($('<textarea/>').html(res.url).text());
                   }
                return false;
             }else{
                $('.errors').append('Unexpected error please contact support');
                $('.wrapper_content').removeClass('overlay');
                $('#sc-form-submit').prop('disabled', false);
             }
           }
         });
    });   

    $('select').change(function(e) {
      var form = $('#sc-form');
      $('.wrapper_content').addClass('overlay');

      $.ajax({
           type: "POST",
           url: '/payment/get-package-price',
           dataType: 'json',
           data: form.serialize(),
           success: function(res)
           {
             if( res.status == 1 ){
              $('#amount').text(parseInt(res.price))
             }else{
                $('.errors').append('Unexpected error #152422 please contact support ');
             }
           }
      })
      $('.wrapper_content').removeClass('overlay');

    });
    
    $('.dropdown').click(function () {
            $(this).attr('tabindex', 1).focus();
            $(this).toggleClass('active');
            $(this).find('.dropdown-menu').slideToggle(300);
    });

    $('.dropdown').focusout(function () {
      $(this).removeClass('active');
      $(this).find('.dropdown-menu').slideUp(300);
    });

    $('.dropdown .dropdown-menu li').click(function () {
      $(this).parents('.dropdown').find('span').text($(this).text());
      $(this).parents('.dropdown').find('input').attr('value', $(this).attr('value'));
      
    });

    $('#sc-form .dropdown .dropdown-menu li').click(function(){
        get_price();
    })
        
    $('input:radio[name="payment_method"]').change(function(){
        get_price();
    })
    
    get_price = function(){
        var form = $('#sc-form');
          $('.wrapper_content').addClass('overlay');
           
          $.ajax({
               type: "POST",
               url: '/payment/get-package-price',
               dataType: 'json',
               data: form.serialize(),
               success: function(res){
                 if( res.status == 1 ){
                    $('#amount').text(parseInt(res.price));
                    if($('input[name="payment_method"]:checked').val() == 'bitcoin'){
                        $("#bitamount").val(res.price);
                    }
                 }else{
                    $('.errors').append('Unexpected error #152422 please contact support ');
                 }
                 $('.wrapper_content').removeClass('overlay');
               }
          })
        
        if($('input[name="payment_method"]:checked').val() == 'bitcoin'){
           $("#bit-note").show(); 
        }else{
           $("#bit-note").hide(); 
        }  
    }
    get_price_back = function(){
        var form = $('#sc-form');
          //$('.wrapper_content').addClass('overlay');
           
          $.ajax({
               type: "POST",
               url: '/payment/get-package-price',
               dataType: 'json',
               data: form.serialize(),
               success: function(res){
                 if( res.status == 1 ){
                  $('#amount').text(parseInt(res.price))
                 }else{
                    $('.errors').append('Unexpected error #152422 please contact support ');
                 }
                 $('.wrapper_content').removeClass('overlay');
               }
          })
        
        if($('input[name="payment_method"]:checked').val() == 'bitcoin'){
           $("#bit-note").show(); 
        }else{
           $("#bit-note").hide(); 
        }  
    }
    $(window).load(function() {
         $('.wrapper_content').removeClass('overlay');
         get_price();
    });
})

$(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
        window.location.reload(); 
    }
});
