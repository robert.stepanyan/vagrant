
 function tagAgeVerified(){
        $('#layout').css({'position': 'relative', 'overflow': 'scroll'})

        var elems = document.querySelectorAll(".popup");

        for(var i = elems.length - 1;i >= 0; i--)
            elems[i].parentNode.removeChild(elems[i]);

        
    }


$(function () {
    
    $('.pa_popup').each(function(index) {
        
        $(this).on("click", function() {
            $('#layout').css({'position': 'fixed', 'overflow': 'hidden'});
            var photo = $(this).data('photo'),
                repost = $(this).data('repost'),
                stop = $(this).data('stop'),
                verify = $(this).data('verify'),
                real = $(this).data('real'),
                edit = $(this).data('edit'),
                id = $(this).data('id'),
                premiumStatus = $(this).data('premstatus'),
                title = $(this).data('title'),
                category = $(this).data('category'),
                city = $(this).data('city'),
                del = $(this).data('del'),
                lang = $(this).data('lang'),
                status = $(this).data('status');
                    
            $.ajax({
               type: "GET",
               dataType: "html",
               url: "/" + lang +"/ad/pa-popup",
               success: function(resp) {
                        $('#body').append(resp);
                        $('.portrait_gov img').attr('src', photo);
                        $('.gov_girl').html(stringTruncate(title, 35) + ' - ' + id);
                        
                        if($('.bumpsPayment') && status === 'active') {
                            $('.bumpsPayment').removeClass('actions_disabled');
                            $('.bumpsPayment').attr('href', '/payment/bumps?ad_id=' + id);
                        }
                        if(status !== '' && status != 'new') {
                            $('.view_ad').removeClass('actions_disabled');
                            $('.view_ad').attr('href', 'https://www.humpchies.com/ad/details/category/' + category + '/city/' + city + '/id/' + id + '/' , '_blank');
                        }
                        if(stop !== ''){
                            if(stop === repost) {
                                $('.stop_gov span').html('Start');
                                $('.stop_gov').attr('href', stop);
                                $('.stop_gov').removeClass('actions_disabled');
                                $('.stop_gov i').css('color', '#adadad')
                            } else {
                                $('.stop_gov').attr('href', stop);
                                $('.stop_gov').removeClass('actions_disabled')
                            }
                        }
                       
                        if(verify == 'verified'){
                           $('.verify_ad').hide(); 
                           $('.verified_ad').show(); 
                        }
                        if(verify != 'verified' &&  verify != 'pending'){
                            $('.verify_ad').attr('href', "/" + lang +'/ad/verify/?id=' + id  , '_blank')
                        }
                        if(repost !== '') {
                            $('.repost_gov').attr('href', repost);
                            $('.repost_gov').removeClass('actions_disabled')
                        }

                        if(edit !== '') {
                            $('.edit_gov').attr('href', edit);
                            $('.edit_gov').removeClass('actions_disabled')
                        }
                        
                        if(premiumStatus !== ''){
                            if(premiumStatus === 2) {
                                var ln = document.documentElement.lang;
                                if(ln = 'fr-ca'){
                                    $('.premium_gov span').html('C\'est d&eacute;j&agrave; Premium');
                                }else{
                                    $('.premium_gov span').html('Is Already Premium');
                                }
                                $('.premium_gov').css('pointer-events', 'none');
                                $('.premium_gov').css('background-color', '#e4e3e3');
                                $('.premium_gov').removeClass('actions_disabled')
                            } else {
                                $('.premium_gov').attr('href', premiumStatus);
                                $('.premium_gov').removeClass('actions_disabled')
                            }
                        }
                         if(real == 'real'){
                             
                            $('.verify_ad').removeClass('actions_disabled');
                         }else{
                             $('.verify_ad').hide(); 
                         }
                        if(del !== '') {
                            $('.delete_gov').attr('href', del);
                            $('.delete_gov').removeClass('actions_disabled');
                        }
                    } 

               
            });
            
            
        })
    });
    
   
})

var stringTruncate = function(str, length){
  var strlist = str.length > length ?  str.substring(0, length - 3 ) + '...'  : str;
  return strlist
};
