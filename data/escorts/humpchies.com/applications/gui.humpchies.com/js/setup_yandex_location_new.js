ymaps.ready(init);

var firstGeoObject, myMap;
function init() {
    
    $("#no_results").hide(); // hide no results
    var montrealLatLng =  [45.50165694120211, -73.56725063558198];
    myMap = new ymaps.Map('map', {
        center: montrealLatLng,
        zoom: 15,
        controls: ['zoomControl', 'geolocationControl']
    });

    var searchControl = new ymaps.control.SearchControl({ // add search
         options: {
             float: 'right',
            floatIndex: 100,
            noPlacemark: true
         }
    });
    myMap.controls.add(searchControl);
    
    
    ymaps.geolocation.get({
        // Setting the option for detecting location by IP provider: 'yandex',
        // Automatically geocoding the result.
        //autoReverseGeocode: true
    }).then(function (result) {
        // Taking the data resulting from geocoding the object and outputting it to the console.
        var lat = result.geoObjects.get(0).geometry.getCoordinates()[0];
        var lng = result.geoObjects.get(0).geometry.getCoordinates()[1];
        var metaDataProperty = result.geoObjects.get(0).properties.get('metaDataProperty');
        
       // console.log(result);
        //console.log(metaDataProperty);
        //console.log("country name " + metaDataProperty.GeocoderMetaData.AddressDetails.Country.CountryName);
        //console.log(lat)
        //console.log(lng)
        
        
        if(metaDataProperty.GeocoderMetaData.AddressDetails.Country.CountryName == 'Canada') {
            
            myMap.setCenter([lat, lng], 14);    
                firstGeoObject = new ymaps.GeoObject({
                // The geometry description.
                geometry: {
                    type: "Point",
                    coordinates: [lat, lng]
                }
            }, {
                reset: 'islands#darkBlueDotIconWithCaption',
                // The placemark can be dragged.
                draggable: true
            }); 
            
            // initialize default coords
            loadData(lat,lng); // check result and if there are any, enable btn
        
        } else {
            
            firstGeoObject = new ymaps.GeoObject({
                // The geometry description.
                geometry: {
                    type: "Point",
                    coordinates: montrealLatLng
                }
            }, {
                preset: 'islands#darkBlueDotIconWithCaption',
                // The placemark can be dragged.
                draggable: true
            })
            loadData(45.50165694120211, -73.56725063558198); // check result and if there are any, enable btn
        }
        
        myMap.geoObjects.add(firstGeoObject); // add marker to map
        //console.log("coordinates" + coords);
        enableDrag(firstGeoObject); // enable dragging of the marker
        enableMapClick(myMap);      // when clicking map, set marker 
        
    }, function (e) {
        // If the location cannot be obtained, we just create a map.
        //console.log("there is an error there");
        enableMapClick(myMap);      // when clicking map, set marker 
    });
    
    
    
    navigator.geolocation.watchPosition(function(position) {
        
        document.getElementById('show_list').style.display = 'flex';
        document.getElementById('map').style.display = 'block';
        document.getElementById('activate_gps').style.display = 'none';

      },
      function(error) {
        //if (error.code == error.PERMISSION_DENIED)
//          console.log("you denied me :-(");
      }
      );
}

function enableDrag(geoObj) {
    
    geoObj.events.add('dragend', function (event) {   // when drag, udate coords
                        
        //console.log(event.get("target").geometry.getCoordinates()[0]) 
        //console.log(event.get("target").geometry.getCoordinates()[1]) 
        // update lat and long
        loadData(event.get("target").geometry.getCoordinates()[0],event.get("target").geometry.getCoordinates()[1]); // check result and if there are any, enable btn
    }); 
    
}

function enableMapClick(myMap) {
    
    /**
     * Processing events that occur when the user
     * left-clicks anywhere on the map.
     * When such an event occurs, we open the balloon.
     */
    myMap.events.add('click', function (e) {
        //console.log("map was clicked");
        var coords = e.get('coords');
        
        // update marker position
        myMap.geoObjects.removeAll();
        
        firstGeoObject = new ymaps.GeoObject({
            // The geometry description.
            geometry: {
                type: "Point",
                coordinates: coords
            }
        }, {
            /**
             * Options.
             * The placemark's icon will stretch to fit its contents.
             */
            preset: 'islands#darkBlueDotIconWithCaption',
            // The placemark can be dragged.
            draggable: true
        })
        myMap.geoObjects.add(firstGeoObject); // add marker to map
        loadData(coords[0],coords[1]); // check result and if there are any, enable btn
        enableDrag(firstGeoObject)

    });   
    
}


function loadData(lat, lng) {
    //console.log("loadData start")
     $("#no_results").hide(); // hide no results
     var searchUrl = '/json/markers?lat=' +  lat + '&long=' + lng;
     var mydata = new Object();
     mydata.lat = lat;
     mydata.long = lng;
     
     $.ajax({
            url: '/json/markers',
            method: 'get',
            data: mydata,
            dataType:"json",
            success: function (resp) {
                //console.log(resp.length);
                
                if(resp.status == 'have_results') {
                    enableBtn(resp.count, lat, lng);
                    
                } else {
                    $("#show_list").addClass("disabled"); // disablebtn
                    $("#show_list").attr("href",""); // disablebtn
                    $("#ads_count").html('(0)');
                    
                    if(resp.status == 'nearest') {
                        
                        // update text
                        $("#city_name").html(resp.city)
                        
                        
                        // updade link/ btn
                        $("#show_list").removeClass("disabled");
                        $("#show_list").attr("href","/ad/nearby/lat/" + resp.lat + "/lng/" + resp.lng); 
                        $("#ads_count").html('');
                        
                        $("#show_near").attr("href","/ad/nearby/lat/" + resp.lat + "/lng/" + resp.lng); 
   
                        // scroll to div
                        $("#no_results").show();
                        //var element = document.getElementById("no_results");
//                        element.scrollIntoView(false);
                         $('html, body').animate({scrollTop: '+=100px'}, 800);
                    }
                    
                    
                }
                
            }
        }); 

}

function enableBtn(count, lat, lng) {
    
    $("#show_list").removeClass("disabled");
    $("#ads_count").html('('+ count + ')');
    // update btn href
   $("#show_list").attr("href","/ad/nearby/lat/" + lat + "/lng/" + lng); 
}


$(document).ready(function(){
    
    // hide list btn
    $("#show_list").hide();
    $("#map").hide();
    
    
    $("#show_map").click(function(){
        $("#show_list"). show();
        $("#map").show();    
        $("#activate_gps").hide();    
    }) ;
});