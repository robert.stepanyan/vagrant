

function init() {
    if($("#show_map").val() == 'yes') {
        
        var lat = $("#map").data("lat")
        var lng = $("#map").data("long")
        
        
         
        mapboxgl.accessToken = 'pk.eyJ1IjoibWF4d2lydGhtYXBzIiwiYSI6ImNrbWFiNm5yaDFxMG8ycWtubDhvaXF6eGkifQ.zn5zSZgAcTS0Ksa7PT12jw';     
    
        myMap = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [lng, lat],
            zoom: 13
            });
            
        // Add zoom and rotation controls to the map.
        myNav = new mapboxgl.NavigationControl();
        myMap.addControl(myNav);
    
        myMarker = new mapboxgl.Marker()    // add default marker
            .setLngLat([lng, lat])
            .addTo(myMap);
    }
}


$( document ).ready(function() {
     $("#toggle_sat").on('click', function(){
          $(this).toggleClass("disabled");
          $("#simple_map").toggle();
          $("#map_sat").toggle();
      });  
      
      $("#toggle_interactive").on('click', function(){
          $("#simple_map").hide();
          $("#map_sat").hide();
          $("#map_sat").hide();
          $(".btn_map").hide();

          
          init(); // init interactive map
          $("#map").css("height", "300px");
      });  

}); 