$(document).ready(function () {
    var input = document.querySelector("#phone");
    var intl = window.intlTelInput(input, {
        allowDropdown: false,
        autoPlaceholder:'',
        autoHideDialCode: true,
        formatOnDisplay: false,
        // autoPlaceholder: "off",
        // dropdownContainer: document.body,
        // excludeCountries: ["us"],
        // formatOnDisplay: false,
        // geoIpLookup: function(callback) {
        //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //     var countryCode = (resp && resp.country) ? resp.country : "";
        //     callback(countryCode);
        //   });
        // },
        // hiddenInput: "full_number",
         initialCountry: "ca",
        // localizedCountries: { 'ca': 'Canada' },
        // nationalMode: false,
        // onlyCountries: ['ca',],
        // placeholderNumberType: "MOBILE",
        preferredCountries: ['ca'],
        // separateDialCode: true,
        utilsScript: "https://gui.humpchies.com/js/intl-tel-input-new/build/js/utils.js",
    });

    input.addEventListener("countrychange", function () {
        // do something with iti.getSelectedCountryData()
        var selectedCountry = intl.getSelectedCountryData();
        $("#phone_country_iso").val(selectedCountry.iso2);
    });
    var phone = $('#phone');
    var phone_country_iso= $('#phone_country_iso');
    function init(){
        var iso = phone_country_iso.val().trim();
        if (!iso){
            for(var i =0; i< intl.preferredCountries.length; i++){
                if (!intl.preferredCountries[i].areaCodes){continue;}
                var areaCodes= intl.preferredCountries[i].areaCodes;
                var areaCode= intl.getNumber().substr(0,3);
                if (!areaCode) {
                    areaCode = phone.val().trim().substr(0,3)
                }
                if (areaCodes.indexOf(areaCode) > -1){
                    intl.setCountry(intl.preferredCountries[i].iso2)
                }
            }
        }else{
            intl.setCountry(iso)
        }

        fillCountryCode(phone, intl)
    }

    if (phone_country_iso.val().trim()) {
        intl.setCountry(phone_country_iso.val().trim());
    }
    phone_country_iso.val((intl.getSelectedCountryData()).iso2);

    function fillCountryCode(inp, intl) {
        var selectedCountry = intl.getSelectedCountryData();
        var phone_country_code = $("#phone_country_code");
        phone_country_code.val('');
        if (inp.val().trim()) {
            if (intl.isValidNumber()) {
                phone_country_code.val(selectedCountry.dialCode)
            }else if(inp.val().trim().length === 10){
                phone_country_code.val(selectedCountry.dialCode)
            }else {
                phone_country_code.val('')
            }
        }
    }

    init();


    phone
        .on('blur', function (e) {
            fillCountryCode($(this), intl)
        })
        .on('keyup', function (e) {
            fillCountryCode($(this), intl)
        })
        .on('change', function (e) {
            fillCountryCode($(this), intl)
        })
        .on('past', function (e) {
            fillCountryCode($(this), intl)
        })
        .on('focusin', function (e) {
            fillCountryCode($(this), intl)
        })
    $(".iti__flag").css("width", "20px").css('position','absolute').css('right', "8px").css('top', "16px");
    $(".iti").css("width", "100%")
})
