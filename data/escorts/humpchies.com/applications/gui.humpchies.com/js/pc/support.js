$(document).ready(function () {
   //
//    $(".user-menu-trigger").on("click",function(e){
//        e.stopPropagation();
//        $(".user-menu-container").stop(true,true).slideToggle();
//    });
//
//    $("body").on("click",function(){
//        $(".user-menu-container").slideUp();
//    });

console.log($('#layout').prop("scrollHeight"));
    $('#chat').scrollTop($('#chat').prop("scrollHeight"));
    //$("html, body").animate({ scrollTop: $(document).height() }, 1000);


    var Messages = {
        'en-ca': {
            files_selected: "files",
            max_file_size: "The Max allowed size for each file is 2MB",
            invalid_file_type:'Only .jpg, .gif, .png, .bmp files can be uploaded',
            you: 'You',
            replayed_on:'replayed on',
        },
        'fr-ca': {
            files_selected: "fichiers",
            max_file_size: "La taille maximale autorisée pour chaque fichier est de 2MB",
            invalid_file_type:'Seuls les fichiers .jpg, .gif, .png, .bmp peuvent être téléchargés',
            you: 'Vous',
            replayed_on: 'rejoué en',
        },
    };
    var ln =document.documentElement.lang;

    $('#attached').on('change', function (e) {
        
        $(".file-preview").hide();
        $(".file-preview > .image").html('');
        
        var files = this.files;
        var valid = true;
        var supportedTypes = [
            'image/jpg',
            'image/jpeg',
            'image/jpe',
            'image/gif',
            'image/png',
            'image/bmp'
        ];
        var errorMsgKey='';
        for (var i = 0; i < files.length; i++) {
            if(supportedTypes.indexOf(files[i].type) < 0){
                valid = false;
                errorMsgKey = 'invalid_file_type';
                break;
            }

            if (files[i].size > 2*1024*1024){
                valid = false;
                errorMsgKey = 'max_file_size';
                break;
            }
        }
        if (!valid){
            e.preventDefault();
            alert(Messages[ln][errorMsgKey]);
            return;
        }

        if (files.length === 0) {
            $("#custom-fileinput .lbl-placeholder").val('');
            $(".file-preview").hide();
        }else if(files.length === 1){
            $("#custom-fileinput .lbl-placeholder").val(files[0].name);
            $(".file-preview > .image").html('<p>' + files[0].name + '</p>');
            $(".file-preview").show();
        }else if(files.length > 1){
            $("#custom-fileinput .lbl-placeholder").val(files.length + " " +Messages[ln].files_selected);
//            console.log(files);
            $.each(files, function(key, val) {
                console.log(val.name)
                 $(".file-preview > .image").append('<p>' + val.name + '</p>');
            });                                  
            
           
             $(".file-preview").show();
             $("#custom-fileinput .lbl-placeholder").val('');
            
        }
    });
    // file-remove
    $('.file-remove').on('click', function (e) {
         e.preventDefault(); 
         e.stopPropagation(); 
         $(".file-preview").hide();
         $(".file-preview > .image").html(''); 
         $("#attached").val('');
         return;  
    });

    $("#custom-fileinput").on('click', function (e) {
        $('#attached').trigger('click');
        e.preventDefault()
    });

    $('.support-tabs .tab').on('click', function (e) {
       e.preventDefault();
       e.stopImmediatePropagation();

       if(!$(this).hasClass('active')){
           var selector = $(this).attr('href');
           $('.support-tabs .tab').removeClass('active');
           $(this).addClass('active');
           $('.tab-content').removeClass('active');
           $(selector).addClass('active')
       }
    });


    $('#message').keypress(function (e) {
        //console.log("clicked enter omg omg")
        //console.log(e.which)
      if (e.which == 13) {
        $('#add-replay').submit();
        return false;    
      }
    });

    // $("#add-replay").on('submit', function (e) {
    //     //console.log("submit the damn form")
    //     e.preventDefault();
    //     e.stopImmediatePropagation();
    //     var content = $('#ticket-form-a');
    //     var formData = new FormData(this);
    //     var url = $(this).attr('action');

    //     var data = {
    //         ticket_id: $("#ticket_id").val(),
    //         message: $("#message").val()
    //     };

//         $.ajax({
//             url:url,
//             method:'POST',
//             data:data,
//             //processData: false,
//             //contentType: false,
//             contentType: "application/x-www-form-urlencoded;charset=windows-1252",
//             dataType:'json',
//             beforeSend:function() {
//                 $("#error-msg").text('').hide();
//                 content.addClass('overlay')
//             },
//             complete: function(){
//                 content.removeClass('overlay');
//                 $(".file-preview").hide();
//                 },
//             success: function (resp) {
//                 if (!resp.success) {
//                     $("#error-msg").text(resp.message).show();
//                 } else {
//                     var comment = resp.details;
//                     var Cmt = $("<ul/>").addClass("tickets by-user");
//                     var CmtHeading = $("<li />").addClass('heading');
//                     CmtHeading.html(
//                     '<div class="row-f"><p>'+comment.comment+'</p></div>'
//                     );
//                     if(comment.hasOwnProperty('attached_files') && comment.attached_files.length){
//                         var attachedFiles= $("<div/>").addClass('row-f attach-container');
//                         var attached = comment.attached_files;
//                         for(var i =0; i< attached.length; i++){
//                             attachedFiles.append(
//                                 $("<p/>").append(
//                                     $("<a/>")
//                                     .attr('href', attached[i].url)
//                                     .attr('target', "_blank")
//                                     .text(attached[i].file_name)
//                                 )
//                             );
//                         }
//                         CmtHeading.append(attachedFiles)
//                     }
//                     Cmt.append(CmtHeading);
// //                    var CmtAuthor = Messages[ln].you+ ' '+ Messages[ln].replayed_on+' '+comment.comment_date;
//                     var CmtAuthor = comment.comment_date;
//                     Cmt.append(
//                         $("<li />")
//                             .addClass('no-padding')
//                             .append(
//                                 $("<i/>")
//                                     .addClass('author')
//                                     .text(CmtAuthor)
//                             )
//                     );
//                     //console.log($('#chat').prop("scrollHeight"))
//                     var scrollTo = $('#chat').prop("scrollHeight") + 64;
//                     //console.log(scrollTo)
//                     $('#chat').scrollTop(scrollTo);
//                     //$('#ticketComments').scrollTop($('#ticketComments').prop("scrollHeight"));

//                     if($("#ticketComments > p").length){
//                         $("#ticketComments > p").remove()
//                     }
//                     $("#ticketComments").append(Cmt);
//                     $("#message").val('');

//                 }
//             }
//         })
    //});
});