$(document).ready(function () {
    var Messages = {
        'en-ca': {
            select_one_main:                "You can set only one image as main",
            select_img:                     "Please select an image",
            img_count_reached:              "You can not upload photos.\r\nYou have reached max count of images.",
            confirm_remove: "Are you sure You want to delete image(s)?",
            files_selected: "files",
            invalid_file_type:              'Only .jpg files can be uploaded',
            label_image_dim_square:         'Image dimension should be bigger than 400X400',
            label_image_dim_landscape:      'Image dimension should be bigger than 600X400',
            label_image_dim_portrait:       'Image dimension should be bigger than 400X600',
            label_image_size:               'Image size should be less than 5MB',
            remove_video:                   'Are you sure you want to remove this video from your collection?',
            upload_complete:                'Upload Complete. Saving file.',
            video_size:                     'Video size should be less than 50MB',
            select:                         'SELECT',
        },
        'fr-ca': {
            select_one_main:                "Vous ne pouvez d\u00E9finir qu'une seule image comme image principale.",
            select_img:                     "S'il vous plaît s\u00E9lectionner une image",
            img_count_reached:              "Vous ne pouvez pas t\u00E9l\u00E9charger de photos.\r\nVous avez atteint le nombre maximum d'images.",
            confirm_remove:                 "\u00CAtes-vous s\u00FBr de vouloir supprimer une ou plusieurs images?",
            files_selected: "fichiers",
            invalid_file_type:              'Seuls les fichiers .jpg peuvent \u00EAtre t\u00E9l\u00E9charg\u00E9s',
            label_image_dim_square:         'La dimension de l\'image doit \u00EAtre sup\u00E9rieure \u00E0 400X400',
            label_image_dim_landscape:      'La dimension de l\'image doit \u00EAtre sup\u00E9rieure \u00E0 600X400',
            label_image_dim_portrait:       'La dimension de l\'image doit \u00EAtre sup\u00E9rieure \u00E0 400X600',
            label_image_size:               'Dimension de l\'image doit \u00EAtre inf\u00E9rieure \u00E0 5MB',
            remove_video:                   'Voulez-vous vraiment supprimer cette vid\u00E1o de votre collection? ',
            upload_complete:                'T\u00E1lu00E1chargement compl\u00E9t\u00E9. Sauvegarde du fichier.',
            video_size:                     'Dimension de l\'video doit \u00EAtre inf\u00E9rieure \u00E0 50MB',
            select:                         'S\u00EALECTIONN\u00EA',
        },
    };
    var ln =document.documentElement.lang;
    var content = $("#body > .wrapper_content");

    function initSortable(reInit){
        var container = $('.images');
        if (reInit) {
            container.sortable("destroy");
        }
        container.sortable({
            helper: 'clone',
            handle:'.move',
            stop: function( event, ui ) {
                var self= this;
                if (self.sorted) {
                    save_order(function (resp) {
                        self.sorted = false
                    })
                }
            },
            change:function (e, ui) {
                this.sorted = true;
            }
        });
        container.disableSelection();
    }
    initSortable(false);

    function save_order(callback) {
        var ids = [];

        $('.image input[type=checkbox]').each(function (i, el) {
            ids.push($(el).val());
        });
        var adID = Number($('#ad_id').val());
        $.ajax({
            url: '/details/photos',
            method: 'post',
            data: {a: 'sort', photo_id: ids, ad_id: adID},
            beforeSend:function(){
                content.addClass('overlay')
            },
            complete: function(){content.removeClass('overlay');},
            success: function (resp) {
                callback(resp);
            }
        });
    }
    $('.set-main').on('click', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var image= getCheckedImages();
        if (image.length > 1) {
            alert(Messages[ln].select_one_main);
            return;
        }
        if (image.length < 1) {
            alert(Messages[ln].select_img);
            return;
        }
        var adID = Number($('#ad_id').val());
        $.ajax({
            url: '/details/photos',
            method: 'post',
            data: {a: 'set_main', photo_id: image[0], ad_id: adID},
            beforeSend:function(){
                content.addClass('overlay')
            },
            complete: function(){content.removeClass('overlay');},
            success: function (resp) {
                $('.image').removeClass('main');
                $('.custom-control-input[type="checkbox"]:checked').parents('.image').addClass('main');
            }
        });
    });

    function getCheckedImages(){
        var ids = [];
        $('.custom-control-input[type="checkbox"]:checked').each(function (i, el) {
            if($.isNumeric($(el).val())) {
            ids.push($(el).val());
            }
        });
        return ids;
    }

    $('.delete').on('click', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var imageIds=getCheckedImages();
        if (imageIds.length < 1) {
            alert(Messages[ln].select_img);
            return;
        }
        var adID = Number($('#ad_id').val());
        if (confirm(Messages[ln].confirm_remove)) {
            $.ajax({
                url: '/details/photos',
                method: 'post',
                data: {a: 'delete', photo_id: imageIds, ad_id: adID},
                dataType: 'json',
                beforeSend:function(){
                    content.addClass('overlay')
                },
                complete: function(){content.removeClass('overlay');},
                success: function (resp) {
                    if (!resp.success) {
                        $("#error-msg").text(resp.error)
                    }else{
                        $('.custom-control-input[type="checkbox"]:checked').each(function (i, el) {
                            $(el).parents('.image').remove();
                        });
                        if (resp.main){
                            $(".image.main").removeClass('main');
                            $('input[value="'+resp.main+'"]').parents('.image').addClass('main')
                        }
                        if (resp.count) {
                            $('#img_count').val(resp.count)
                        }
                    }
                }
            });
        }
    });

    $('#images').on('change', function (e) {
        var files = this.files;
        var valid = true;
        for (var i = 0; i < files.length; i++) {
            if(files[i].type !== 'image/jpeg'){
                valid = false;
                break;
            }
        }
        if (!valid){
            e.preventDefault();
            alert(Messages[ln].invalid_file_type);
            return;
        }
        $(".prev_img").remove();
        if (files.length > 5) {
            alert("You can upload 5 Images at the same time");
            return;
        }
        if (files.length === 0) {
            $("#custom-fileinput .lbl-placeholder").val('');
        }else if(files.length === 1){
            $("#custom-fileinput .lbl-placeholder").val(files[0].name);
        }else if(files.length > 1){
            $("#custom-fileinput .lbl-placeholder").val(files.length + " " +Messages[ln].files_selected);
        }
        for (var i = 0; i < files.length; i++) {
            //console.log("here");
            //console.log(files[i].size);
            if(files[i].size > 0 && files[i].size/1024/1024 >= 5) {
                
                //e.preventDefault();
                alert(Messages[ln].label_image_size);
                $('.btn-select_pics').removeClass("disabled");
                return;    
            }
            
            var reader = new FileReader();

            reader.onload = function(event) {
                
                var dimensionsValid = true;
                // validate width, height
                var image = new Image();
                image.src = reader.result;
                image.onload = function() {
                    //console.log("image.width " + parseInt(image.width) < 400 )
                    //console.log("image.height " + image.height )
                    // 4 cases
                    if(image.width == image.height && parseInt(image.width) < 400) {
                        e.preventDefault();
                        alert(Messages[ln].label_image_dim_square);
                        dimensionsValid = false;
                        return;
                    }
                    
                    if((image.width > image.height) && (image.width < 600 || image.height < 400)) {
                        e.preventDefault();
                        alert(Messages[ln].label_image_dim_landscape);
                        dimensionsValid = false;
                        return;
                    }
                    
                    if((image.width < image.height) && (image.width < 400 || image.height < 600)) {
                        e.preventDefault();
                        alert(Messages[ln].label_image_dim_portrait);
                        dimensionsValid = false;
                        return;
                    }
                    
                $('<img/>')
                    .addClass('prev_img image mb-10')
                    .attr('height', 130)
                    .attr('src', event.target.result)
                    .appendTo($('#preview_images'));
                    
                }
            };

            reader.readAsDataURL(files[i]);
        }

    });

    $("#img-upload").on('submit', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var imgCount = $('#img_count').val();
        if (imgCount >= 5) {
            alert(Messages[ln].img_count_reached);
            return;
        }
        var files = $('#images')[0];
        if (imgCount + files.length > 5) {
            alert(Messages[ln].img_count_reached);
            return;
        }
        if (files.length < 1) {
            alert(Messages[ln].select_img);
            return;
        }
        var formData = new FormData(this);
        formData.append('a','upload');
        $.ajax({
            url:'/details/photos?a=upload',
            method:'post',
            data:formData,
            processData: false,
            contentType: false,
            dataType:'json',
            beforeSend:function(){
                $("#error-msg").text('');
                content.addClass('overlay')
            },
            complete: function(){content.removeClass('overlay');},
            success: function (resp) {
                if (!resp.success) {
                    $("#error-msg").text(resp.error)
                }else{
                    for(var i =0; i< resp.images.length; i++){
                        var li =$("<li/>").addClass('image');
                        var wrapper = $("<div/>").addClass('wrapper');
                        var img = $("<img/>").attr('src', resp.images[i].src)
                            .attr('width', 180)
                            .attr('title', '')
                            .attr('alt', '');
                        var actions = $("<div/>").addClass('action');
                        var html =
                            '<span class="move"></span>' +
                            '<span class="main">' +
                            '<label class="custom-control custom-checkbox" for="p-'+resp.images[i].id+'">' +
                            '<input type="checkbox" class="custom-control-input" id="p-'+resp.images[i].id+'" name="photo_id" value="'+resp.images[i].id+'">' +
                            '<span class="custom-control-indicator"></span>' +
                            '<span class="custom-control-description"></span>' +
                            '</label>' +
                            '</span>';
                        actions.html(html);
                        img.appendTo(wrapper);
                        actions.appendTo(wrapper);
                        wrapper.appendTo(li);
                        li.appendTo($(".images"));
                    }
                    document.getElementById("img-upload").reset();
                    $(".prev_img").remove();
                    initSortable(true);
                }
            }
        })
    });

    $("#custom-fileinput").on('click', function (e) {
        $('#images').trigger('click');
        e.preventDefault()
    })
    
    
    $("#post_pad").validate({
        rules : {
            title        :  { required: true, minlength: 3, maxlength: 64},
            phone        :  { required: true},
            description  :  { required: true, minlength: 3, maxlength: 1000},
            
        },
        messages : {
           // title        :  { required : Messages[ln].label_mandatory_field },    
            //phone        :  { required : Messages[ln].label_mandatory_field },    
           // description  :  { required : Messages[ln].label_mandatory_field },    
        },
        highlight: function(element) {
            console.log(element)
            $(element).parent('.form-group').removeClass('noError').addClass('hasError');
            
        },
        unhighlight: function(element) {
            $(element).parent('.form-group').removeClass('hasError').addClass('noError');
        },
        submitHandler: function(form) {
            form.submit();
        }
    }); 
            $('.btn-select_video').on('click',function (e) {
           // e.preventDefault();
            console.log('toggle');
            $("#video-upload").trigger('click');
            
        });
        //--- UploadVideo
        
        $("#video-upload").change( function(e){
            e.preventDefault();
            if($(this)[0].files[0].size > 52428800) {
                alert(Messages[ln].video_size);
                return false;
            }
            $(".progress-bar").show();
            var formData = new FormData();
            formData.append('videoupload', $(this)[0].files[0]);
            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = Math.floor((evt.loaded / evt.total) * 100);
                            console.log(percentComplete);
                            $(".progress-bar").html(percentComplete+'%');
                            if(percentComplete>= 96){
                                $(".progress-bar").html(Messages[ln].upload_complete);
                            }
                            if(percentComplete>= 96) percentComplete = 96;
                            $(".progress-bar").width(percentComplete + '%');
                               
                        }
                        
                    }, false);
                    return xhr;
                },
                type: 'POST',
                url: '/ad/videoupload',
                data: formData,
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function(){
                    $(".progress-bar").width('0%');
                  //  $('#uploadStatus').html('<img src="images/loading.gif"/>');
                },
                error:function(){
                    $('#uploadStatus').html('<p style="color:#EA4335;">File upload failed, please try again.</p>');
                    $(".progress-bar").width('96%');
                        
                },
                success: function(resp){
                    $(".progress-bar").hide();
                    console.log(resp);
                    if(resp == 'ok'){

                    }else if(resp == 'err'){
                        $('#uploadStatus').html('<p style="color:#EA4335;">Please select a valid file to upload.</p>');
                        return;
                    }
                    var obj = JSON.parse(resp)
                    $(".video-preview").removeClass("selected");
                    $("#attached-video").val(obj.vid);
                    var item = $("<div>").addClass("video-preview").addClass("new").addClass("selected").attr("rel-movie", obj.movie).attr("rel-id", obj.vid).css('background-image', "url(" + obj.movie+'.jpg)');
                    $("<div>").addClass("video-remove").appendTo(item);
                    $("<div>").addClass("video-select").html(Messages[ln].select).appendTo(item);
                    $("#gallery_videos").append(item);
                    jQuery("div#gallery_videos").scrollTop(jQuery("div#gallery_videos")[0].scrollHeight);
                }
            });
        });
        $(".video-modal-close").on('click', function(){
            $("#play-video-modal").hide();
        });
        
        $("#gallery_videos").on("click",".video-select", function(event){
            event.stopPropagation()
            $(".video-preview").removeClass("selected");
            $(this).parent().addClass("selected"); 
            $("#attached-video").val($(this).parent().attr('rel-id'));   
        });
        $("#gallery_videos").on("click",".video-preview", function(){
            var location =  $(this).attr("rel-movie");
           $("#movie-load").html('<source src="' + location + '" type="video/mp4"></source>' ); 
           $(".play-video-modal").show();
           $("#movie-load").load();;
        }); 
        $("#gallery_videos").on("click",".video-remove", function(event){
            event.stopPropagation();
            if(window.confirm(Messages[ln].remove_video)){
              $.ajax({
                type: "GET",
                url: "/ad/deletevideo?id=" + $(this).parent().attr('rel-id'),
                cache: false,
                 dataType: 'JSON',
                success: function(result){
                    var obj = 
                  console.log(result.deleted);
                  console.log('enterdeleted');  
                  if(result.deleted){
                      console.log(result.deleted);
                     $("div[rel-id='" + result.deleted + "'").remove(); 
                  }
                }
              });
            }
        });
         $('#movie-load').bind('contextmenu',function() { return false; });
        //--------------------------------------------------------------------------------------
});