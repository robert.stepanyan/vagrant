$(document).ready(function(){

    $("input[type=password]").on("change",function(){

        var empty = true;
        $('input[type="password"]').each(function(){
           if($(this).val()!=""){
              empty =false;
              return false;
            }
         });

        if (!empty) {
            $("input[type=password]").each(function(){
              $(this).rules("add", "required");  
            });
        } else {
            $("input[type=password]").each(function(){
              $(this).rules('remove', 'required');  
            });
        }
    });

    $("#settings-form").validate({
            rules : {
                current_password :  { required: function() {                        
                    return ($("#new_password").val() != '' ||$("#confirm_password").val() != '')                    
                } ,},
                new_password :      { required: function() {                        
                        return ($("#current_password").val() != '' ||$("#confirm_password").val() != '')                    
            },
                        minlength : 6 },
                confirm_password :  { required: function() {                        
                        return ($("#current_password").val() != '' ||$("#new_password").val() != '')                    
                        },
                        minlength : 6, equalTo : "#new_password" }
            },
            messages : {
                current_password :  { required : "This field is required" },
                new_password :      { required : "This field is required", minlength : "Please enter at least 6 characters" },
                confirm_password :  { required : "This field is required", minlength : "Please enter at least 6 characters", equalTo : "The Passwords did not match" }
            },
            highlight: function(element) {
                $(element).parent('.form-group').removeClass('noError').addClass('hasError');
                
            },
            unhighlight: function(element) {
                $(element).parent('.form-group').removeClass('hasError').addClass('noError');
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
});