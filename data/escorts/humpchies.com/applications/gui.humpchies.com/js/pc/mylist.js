$(document).ready(function () {
    var Messages = {
        'en-ca': {
            cancel_conf :"Cancel",
            delete_conf: "Delete",
            msg_conf:    "Are you sure you want to remove this Ad?"
        },
        'fr-ca': {
            cancel_conf:"Annuler",
            delete_conf: "Supprimer",
            msg_conf:    "\u00C9tes-vous s\u00FBr de vouloir supprimer cette annonce?"
        },
    };

    var ln = document.documentElement.lang;
    var languageIsDefault = window.location.pathname.indexOf(ln) < 0;
    
    console.log(ln);
    
    
    // instanciate new modal
    var modal = new tingle.modal({
        footer: true,
        stickyFooter: false,
        closeMethods: ['overlay', 'button', 'escape'],
        closeLabel: Messages[ln].cancel_conf,
        cssClass: ['modal-confirm'],
        onOpen: function() {
            console.log('modal open');
        },
        onClose: function() {
            console.log('modal closed');
        },
        beforeClose: function() {
            // here's goes some logic
            // e.g. save content before closing the modal
            return true; // close the modal
            return false; // nothing happens
        }
    });
    modal.setContent('<h1>' + Messages[ln].msg_conf + '</h1>'); 
    modal.addFooterBtn(Messages[ln].delete_conf, 'tingle-btn tingle-btn--danger deletebtn', function() {
        // here goes some logic
    modal.close();
    });
    modal.addFooterBtn(Messages[ln].cancel_conf, 'tingle-btn', function() {
        // here goes some logic
    modal.close();
    });
    //var locat;
    $(".delete").on("click", function (e){
       e.preventDefault(); 
       e.stopPropagation();
       var locat = $(this).attr('href');
       
       $('.deletebtn').on('click',function(){
           window.location.href = locat;
       });
       modal.open();
    
    });
    
    
    $(".filter_ads_box").on('click tap', function() {        
        $(".select_options").toggle();
    });
    
    $("#close-holiday").on("click",function(){
        $("#holiday-special").stop(true,true).toggle();
        setCookie("holiday-special", '1', 1);
        return false;
    });
    
    $("#hide-special").on("click",function(){
        $("#ebook-special").stop(true,true).toggle();
        setCookie("ebook-special", '1', 90);
        return false;
    });
    
    
});

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}