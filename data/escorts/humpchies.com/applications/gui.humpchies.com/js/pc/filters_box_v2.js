$(document).ready(function(){
    
    var Messages = {
        'en-ca': {
            region:              "Region",
        },
        'fr-ca': {
            region:              "R\u00E9gion"
        },
    };
    var ln = document.documentElement.lang;
    //console.log(ln);
    $("#search_city").keyup(function(){ // HEADER V2
        
        var baseURL = '/';
        if($("#lang").val() == 'fr-ca') {
           baseURL = '/fr-ca/';
        }

        if($(this).val().length == 0) { // clear form action
             $("#filters_form").attr("action", baseURL + 'ad/listing-v2');
             $("#suggestion-city").hide();   
        }
        
        if($(this).val().length >= 2) { // turn on autocomplete
            
            $.ajax({
                type: "POST",
                url: baseURL + "city/new-search",
                data:'keyword='+$(this).val() + "&category_code=" + $("#category_city").val() + "&region=1&version=2",
                beforeSend: function(){
                    $("#search_city").addClass("focused");
                },
                success: function(data){
                    
                    $("#suggestion-city").html(''); 
                    
                    if(data.length > 2) {
                        
                        var obj = JSON.parse(data)
                        $.each(obj, function(idx, city){
                            
                            var show_name = city.name;
                            if(city.is_region) {
                                show_name = city.name + ' ('+ Messages[ln].region +')';
                            }
                            $("#suggestion-city").show();
                            var cityOpt = '<li><a href="javascript:void(0)" class="suggested-city-location" data-slug="' + city.slug + '" data-url = "' + city.city_url + '">' + show_name +' </a></li>';
                            $("#suggestion-city").append(cityOpt);
                            
                            enableClickLocation();
                        });       
                    }
                }
            });    
        }
   });
   
    // on SUBMIT update filters anchor
   $("#search-filters-btn").on("click", function(e){
        //console.log("form was submitted")
        //console.log($(location).attr('href'))
        e.preventDefault();
        
        var anchor = [];
        //console.log($("#services").val())
        if($("#services").val() != '') {
            anchor.push('services=' + $("#services").val() );         
        }
        
        if($("#prices").val() != '') {
            anchor.push('prices=' + $("#prices").val() );         
        }
        
        if($("#location").val() != '') {
            anchor.push('location=' + $("#location").val());      
        }
        
        if($("#real_photos").val() != '') {
            anchor.push('real_photos=' + $("#real_photos").val());      
        }
        
        if($("#accepts_comments").val() != '') {
            anchor.push('accepts_comments=' + $("#accepts_comments").val());      
        }
        if($("#has_comments").val() != '') {
            anchor.push('has_comments=' + $("#has_comments").val());      
        }
        
        if($("#phone1").val() != '') {
            anchor.push('phone1=' + $("#phone1").val());      
        }
        
        if($("#phone2").val() != '') {
            anchor.push('phone2=' + $("#phone2").val());      
        }
        
        if($("#phone3").val() != '') {
            anchor.push('phone3=' + $("#phone3").val());      
        }
        
        //console.log(anchor);
        //console.log(anchor.length);
        
        if(anchor.length > 0) {
            // get current url
            var form_url = $("#filters_form").attr("action");
            
            if(form_url == '') {
                form_url = $(location).attr('href');    
            }
            form_url = form_url.split("#")[0];
            
            form_url += '#' + anchor.join('&');
            
            //console.log(form_url);
            $("#filters_form").attr("action", form_url);  
        
        } else  {
            var noHashURL = $("#filters_form").attr("action").replace(/#.*$/, '');
           // var noHashURL = $(location).attr('href').replace(/#.*$/, '');
           //console.log("location href " + $(location).attr('href'));
           //console.log("no hash url " + noHashURL);
           //console.log($("#filters_form").attr("action"));
            $("#filters_form").attr("action", noHashURL);
           window.location.href = noHashURL; 
           return; 
        }
        // submit form
        $("#filters_form").submit();  
   })
   
});



function enableClickLocation() {       // HEADER V2
    
    $(".suggested-city-location").on("click", function(){
        $("#search_city").val($(this).html());    
        $("#city_slug").val($(this).data('slug'));    
        $("#suggestion-city").hide();
        $("#filters_form").attr("action", $(this).data('url'));
    });
}


  