$(function () {
    $("#bump-form").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        $('#bump-settings-submit').prop('disabled', true);

        var form = $(this);
        var url = form.attr('action');
        $('.wrapper_content_full').addClass('overlay');
        $('.errors').empty();

        $.ajax({
           type: "POST",
           url: url,
           dataType: 'json',
           data: form.serialize(), // serializes the form's elements.
           success: function(res){
                if( res.status == 1 ){
                  if(res.ad_status != "new"){
					  
					location.reload();
				  }else{
					$('.wrapper_content_full').removeClass('overlay');
					$('.form-wrapper').hide();
					$('.create-ad').show();
				  }
                  //$('#bump-settings-submit').prop('disabled', false);
				  
                   return false;
                }else{
                    if(res.ad_status != "new"){
                        $(".credits").html(res.credits);
                        $(".pack").html(res.pack);
                        $(".price").html(res.amount);
                        $('.form-wrapper').hide();
                        $('.missing-credits').show();
                    }
                    $('#settings-error').append(res.errors[0]);
                    $('.wrapper_content_full').removeClass('overlay');
                    $('#bump-settings-submit').prop('disabled', false);
                }
            }
        });
    });

    
    $("#bumps-purchase").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        $('#bumps-purchase-submit').prop('disabled', true);

        var form = $(this);
        var url = form.attr('action');
        $('.wrapper_content_full').addClass('overlay');
        $('.errors').empty();

        $.ajax({
           type: "POST",
           url: url,
           dataType: 'json',
           data: form.serialize(), // serializes the form's elements.
           success: function(res){
                if( res.status == 1 ){
                   if(res.pp !='card' ){
                      $("#bitamount").val(res.a); 
                      $("#bitlang").val(res.lang); 
                      if(parseInt($("#bitamount").val())){
                        $("#bitform").attr('action', res.url).submit();
                      } 
                   }else{
                        window.location.replace($('<textarea/>').html(res.url).text());
                   } 
                   return false;
                }else{
                    $('.errors').append('Unexpected error please contact support');
                    $('.wrapper_content_full').removeClass('overlay');
                    $('#bumps-purchase-submit').prop('disabled', false);
                }
            }
        });
    });

    filter_bumps_hours = function(hours){
      $('.bumps-hour').hide();
      var hours = hours.toString().split(',');
      hours.filter(function(h){
        $('.bump-for-'+h).show();
      });

       $('.bump-for-'+hours[0]).parents('.dropdown').find('span').text( $('.bump-for-'+hours[0]).text());
       $('.bump-for-'+hours[0]).parents('.dropdown').find('input').attr('value',  $('.bump-for-'+hours[0]).attr('value'));
    }
    
    calc_bumps = function(){
      var form = $('#bump-form').serializeArray();
      var bumpfor = form[3].value * 60;
      var bump = form[2].value;
      var credit_count = bumpfor / bump + 1;
      $('#bump-credits').text(credit_count);
    }

    calc_credits = function(){
      var form = $('#bumps-purchase').serializeArray();
      console.log(form);
      var current_credits = +$('#current-balance').text();
	  var selected_credits = + form[3].value.replace("cr_", "");
      $('.selected-credits').empty();
      $('.total-bumps').empty();

      $('.selected-credits').append(selected_credits);
      $('.total-bumps').append(selected_credits + current_credits);
    }

    one_bump = function(url){
      $('.wrapper_content_full').addClass('overlay');
      $('#onetime-bump').prop('disabled', true);
      $('.errors').empty();
      
      var data = {'user_id': $( "input[name='user_id']").val(), 'ad_id': $( "input[name='ad_id']").val(), };

        $.ajax({
           type: "POST",
           url: url,
           data: data,
           dataType: 'json',
           success: function(res){
            if( res.status == 1 ){
                if(res.ad_status != "new"){
                      
                    $('.wrapper_content_full').removeClass('overlay');
                    //$('#onetime-bump').after('<p id="one-time-notify">YOU ARE ON TOP NOW</p>');
                    
                    setTimeout(function(){  location.reload();/*$( "#one-time-notify" ).hide('fast'); $('#onetime-bump').prop('disabled', false);*/}, 5000);
                    $('.bummped-ad').show();
                    
                  }else{
                    $('.wrapper_content_full').removeClass('overlay');
                    $('.form-wrapper').hide();
                    $('.create-ad').show();
                  }
               
               return false;
            }else{
				$('#one-time-error').append(res.errors[0]);
                $('.wrapper_content_full').removeClass('overlay');
                $('#onetime-bump').prop('disabled', false);
                return false;
            }
           }
        });
    }

    stop_bump = function(){
      $('#stop-bumping').prop('disabled', true);
      $('.wrapper_content_full').addClass('overlay');
      $('.errors').empty();
      var data = {'user_id': $( "input[name='user_id']").val(), 'ad_id': $( "input[name='ad_id']").val(), };
      $.ajax({
           type: "POST",
           url: '/payment/bumps-stop',
           data: data,
           dataType: 'json',
           success: function(res){
            if( res.status == 1 ){
                $('#stop-bumping').empty();
                $('#stop-bumping').append('BUMP STOPPED');
				$('.wrapper_content_full').removeClass('overlay');
				location.reload();
               return false;
            }else{
                $('.errors').append('Unexpected error please contact support');
                $('#stop-bumping').prop('disabled', false);
                return false;
            }
           }

        });
    }
   
    /*Dropdown Menu*/
      $('.dropdown').click(function () {
              $(this).attr('tabindex', 1).focus();
              $(this).toggleClass('active');
              $(this).find('.dropdown-menu').slideToggle(300);
      });
      $('.dropdown').focusout(function () {
        $(this).removeClass('active');
        $(this).find('.dropdown-menu').slideUp(300);
      });
      $('.dropdown .dropdown-menu li').click(function () {
        $(this).parents('.dropdown').find('span').text($(this).text());
        $(this).parents('.dropdown').find('input').attr('value', $(this).attr('value'));
      });
    /*Dropdown Menu END*/

    $('#bump-form .dropdown .bump-settings.dropdown-menu li').click(function(){
      filter_bumps_hours($(this).data('hours'));
    })
    
    $('#bump-form .dropdown  li').click(function(){
      calc_bumps();
    })

    $('#bumps-purchase .dropdown  li').click(function(){
      calc_credits();
    })
    
    $('#onetime-bump').on('click', function(){
		one_bump($(this).data("url"));
    })

    $('#stop-bumping').on('click', function(){
      stop_bump();
    })

    $('#what-bump-means').on('click', function(){
      $('.arrow-up').slideToggle('fast');
      $('.what-bump-means').slideToggle('fast');
    })
    $('input:radio[name="payment_method"]').change(function(){
        var credits = $('.dropdown').find('input').attr('value');
        if($(this).val() == 'card'){
           $(".card").show();
           $(".bitcoin").hide();
           var selected = $('li.card[value="' + credits + '"]').text();
            $('.dropdown').find('span').text( $('li.card[value="' + credits + '"]').text());
           
          
        }else{
           $(".card").hide();
           $(".bitcoin").show(); 
           $('.dropdown').find('span').text( $('li.bitcoin[value="' + credits + '"]').text());
           
        }
        });
    shortcutpay = function(){
        var shortcutForm = new FormData();
        
        shortcutForm.append('payment_method','card');
        shortcutForm.append('ad_id',$("#ad_id").val());
        shortcutForm.append('credits',"cr_" + parseInt($("#credit_pack").html()));
        
        $.ajax({
           type: "POST",
           url: "/payment/bumps-purchase",
           data: shortcutForm, 
           contentType: false,
           processData: false,
           dataType: 'JSON',
           success: function(res){
               console.log(res);
                if( res.status == 1 ){
                    window.location.replace($('<textarea/>').html(res.url).text());
                   return false;
                }else{
                    $('.errors').append('Unexpected error please contact support');
                    $('.wrapper_content_full').removeClass('overlay');
                    $('#bumps-purchase-submit').prop('disabled', false);
                }
            }
        });
        
        
        
        
        
    } 
})