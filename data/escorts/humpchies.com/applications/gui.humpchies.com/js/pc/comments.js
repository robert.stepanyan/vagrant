$(document).ready(function () {
    var Messages = {
        'en-ca': {
            login_as_member:"You need to have/create a client account in order to add a comment!",
        },
        'fr-ca': {
            login_as_member:"Veuillez vous connecter en tant que membre",
        },
    };

    var ln =document.documentElement.lang;
    var languageIsDefault = window.location.pathname.indexOf(ln) < 0;
    var content = $('.comments-box').parents('.content_box_body.details_grid');
    $('#save-comment').on('click', function () {
        var url = "/comments/add";
        if (!languageIsDefault) {
            url = '/'+ln+url;
        }
        var comment = $("#comment").val();
        var ad_id = $("#ad_id").val();
        if ($(this).data('rdr') && $(this).data('rdr') == '1'){
            var href = '/user/login?msg=lam';
            if (!languageIsDefault) {
                href = '/'+ln+href;
            }
            location.href = href;
            return false;
        }else if($(this).data('alert') && $(this).data('alert') == '1'){
            alert(Messages[ln].login_as_member);
            return false;
        }else {
            $.ajax({
                url : url,
                method:'post',
                data:{text:comment, ad_id:ad_id},
                dataType:'json',
                error:function(){
                    content.removeClass('overlay');
                },
                beforeSend:function(){
                    $("#err-msg").text('').hide();
                    content.addClass('overlay')
                },
                complete: function(){content.removeClass('overlay');},
                success:function (responce) {
                    if (Number(responce.success) !== 1){
                        $("#err-msg").text(responce.message).show();
                    } else {
//                        console.log($("#comment").parents('.add-comment-area').children('p').first());
                        $("#comment").parents('.add-comment-area').children('p').first().html('<p>'+responce.message+'</p>')
                        $("#comment").val('');
                    }
                }
            })
        }
    })
});