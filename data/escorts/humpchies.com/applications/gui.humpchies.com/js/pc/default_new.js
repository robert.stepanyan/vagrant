$(document).ready(function () {
    
    $(".user-menu-trigger").on("click",function(e){
        //console.log("here");
        e.stopPropagation();
        $(".user-menu-container").stop(true,true).slideToggle(100,"linear", function () {
    // actions to do when animation finish
});
        $("#noticeUread").toggle();
    });

    $("body").on("click",function(){
        $(".user-menu-container").slideUp();
    });

    $("*[rel-data]").each(function () {
       //console.log($(this).attr('rel-data'));
      $(this).html ( $(this).attr('rel-data'));
   });
   
   $("#safe_browsing").change(function(){
       
       console.log($(this).is(":checked"))
       
       if($(this).is(":checked")) {
           
           $("#layout").addClass("safe_browsing");
           Cookies.set("Sfb", 1, { expires : 30 });
       } else {
           $("#layout").removeClass("safe_browsing"); 
           Cookies.set("Sfb", "", { expires : -30 });   
       }
       
       
   });
   
   $('.video').parent().click(function () {  
            $(this).children(".video").get(0).play();   
            $(this).children(".playpause").fadeOut();
            $('.video').parent().unbind('click');
   });
 
   //console.log(getCookie("phi-alert"));
   

   /*HALLOWEEN POPUP*/
    if($('#show_halloween').length > 0 && $('#show_halloween').val() == 'yes' && getCookie("halloween-alert") == null) {
    //if($('#show_halloween').length > 0 && $('#show_halloween').val() == 'yes') {
        $("#halloween-modal").css("display", "flex");
        setCookie("halloween-alert",'seen',1);
        var ShowHalloween = true;
    } else {
        var ShowHalloween = false;    
    }
    
    $(".modal-halloween-close").on("click",function() {
        $("#halloween-modal").hide();
    });
    //===============//
    //console.log("show halloween popup " + ShowHalloween);


   if($('#show_alert').length > 0 && $('#show_alert').val() == 'yes' && getCookie("phi-alert") == null && !ShowHalloween) {
   //if($('#show_alert').length > 0 && $('#show_alert').val() == 'yes') {
        $("#alert-modal").show();
        setCookie("phi-alert",'seen',30);
   }
   
   $(".modal-close").on("click",function(){
        $("#alert-modal").hide();
   });
   
   
   $("#search-btn").on("click",function(){ // HEADER V2
        $("#search_city_slug").val("");
        $("#search_city_header").val("");
        $("#query").val("");
        $("#search-form").show();
   });
   
   $("#close-search-form").on("click",function(){  // HEADER V2
        $("#search_city_slug").val("");
        $("#search_city_header").val("");
        $("#query").val("");
        $("#search-form").hide();
   });
   
   $("#search_city_header").keyup(function(){ // HEADER V2
        //console.log("keyup")
        $.ajax({
        type: "POST",
        url: "/city/search",
        data:'keyword='+$(this).val() + "&category_code=" + $("#category_search_city").val(),
        beforeSend: function(){
            $("#search_city_header").addClass("focused");
        },
        success: function(data){
            
            $("#suggesstion-search-city").html(''); 
            
            if(data.length > 2) {
                
                var obj = JSON.parse(data)
                $.each(obj, function(idx, city){
                    $("#suggesstion-search-city").show();
                    var cityOpt = '<li><a href="javascript:void(0)" class="suggested-city" data-slug="' + city.slug + '">' + city.name +' </a></li>';
                    $("#suggesstion-search-city").append(cityOpt);
                    
                    enableClickCity();
                });       
            }
        }
        });
   });
   
   $('#query').keypress(function (e) {
      if (e.which == 13) {
        
          $("#category_search_city").attr("disabled", true);
          $("#search_city_header").attr("disabled", true);
          
          if($("#search_city_slug").val() == '') {
              $("#search_city_slug").attr("disabled", true);
          }
          //console.log($("#search_city_slug").val());
          $('form#header_search').submit();
        return false;    //<---- Add this line
      }
   });
   
   
   $(".form-control").each(function() {
       if($(this).val() !== '') {
           $(this).addClass("focused");
       }
   }).on("focusout", function(){
       if($(this).val() !== '') {
           $(this).addClass("focused");
       } else {
         $(this).removeClass("focused");    
       }
   }).focus(function() {
        $(this).addClass("focused")
   });
   
   $(".form-label-group").on("click", function(){
       $(this).find(".form-control").focus();
       
   });
     
});


function enableClickCity() {       // HEADER V2
    
    $(".suggested-city").on("click", function(){
        
        $("#search_city_header").val($(this).html());    
        $("#search_city_slug").val($(this).data('slug'));    
        $("#suggesstion-search-city").hide();
    });
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}