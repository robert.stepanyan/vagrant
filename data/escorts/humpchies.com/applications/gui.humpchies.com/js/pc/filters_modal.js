
window.addEventListener("load", function(){
// Get the modal
var modal = document.getElementById("filters-modal");

// Get the button that opens the modal
var btn = document.getElementById("filters-mod");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close_report")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
 
  modal.style.display = "flex";
  //$('html, body').css({overflow: 'hidden'});
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
  $('html, body').css({overflow: 'auto'});
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
    $('html, body').css({overflow: 'auto'});
  }
}

});
$(function() {
    
// A $( document ).ready() block.
//$( document ).ready(function() {
    
    $(".filters-box").on('click', function() { // open filters modal
        $("#filters-modal").css("display", "flex");
        $("#layout").addClass("modal-open");
    });
    
    $("#close_filters").on('click', function() { // open filters modal
        $("#filters-modal").hide();
        $("#layout").removeClass("modal-open");
    });
    
    $(".clear-form").on('click', function() { // clear filters
        
        $("input:radio").prop("checked",false);
        $("input:checkbox").prop("checked",false);
        $('#filters-modal input[type="text"]').val(''); 
         
        $("#location").val(""); 
        $("#services").val(""); 
        $("#prices").val(""); 
        $("#real_photos").val(""); 
        $("#has_comments").val(""); 
        $("#accepts_comments").val("");
        $("#phone1").val(""); 
        $("#phone2").val(""); 
        $("#phone3").val("");  
        // clear form data
        //$("#filters_form").data("services", '');    
        //$("#filters_form").data("availability", '');
         
        $(".filter-icon").removeClass("active");
        $(".filter-title").removeClass("active");
    });
    
    // when we press apply, create the hidden inputs with values inside forms and add tags in the filters box
    
    $("#filters-apply").on('click', function() {
        // clear previously selected filters
        $("#selected_filters").html('');
        
        // get selected radio and write it on the list if needed
        addAvailability();
        // get selected checkboxes and write them on the list
        addServices()
       
        // get selected checkboxes and write them on the list
        addPrices()
        
        // get selected checkboxes and add the additional filters
        addMoreFilters();
       
       
        // submit form
        $("#filters-modal").hide();
    });
    
    deleteFilter("service")
    deleteFilter("availability")
    
});

function addAvailability() {
    
    // get selected radio if any
    var availName = $('input[name=location_filters]:checked').data("label");
    var availId = $('input[name=location_filters]:checked').val();
    
    if(availId !== undefined) {
        if(availId != 'all'){
            writeInBox(availName, availId, 'availability');
        }
        
        
        // update location input
        $("#location").val(availId);    

        // update form data
        //$("#filters_form").data("availability", availId);
        
        $(".filter-icon").addClass("active");
        $(".filter-title").addClass("active");
           
    } else {
        
        $("#location").val('');        
        //$("#filters_form").data("availability", '');       
    }
}


function addServices() {
    
    var Services = [];
    
    //$("#filters_form").data("services", ''); 
    $("#services").val('');    
    $('input[name="selected_services[]"]:checked').each(function() {
            
        var serviceName = $(this).data("label");
        var serviceId = $(this).val();

        writeInBox(serviceName, serviceId, 'service');
        Services.push(serviceId);

        $("#services").val(Services.join(','));    

        // update form data
        //$("#filters_form").data("services", Services.join(',')); 
        
        $(".filter-icon").addClass("active");
        $(".filter-title").addClass("active");
    }); 
}

function addPrices() {
    
    var Prices = [];
    
    //$("#filters_form").data("services", ''); 
    $("#prices").val('');    
    $('input[name="selected_prices[]"]:checked').each(function() {
            
        var priceName = $(this).data("label");
        var priceId = $(this).val();

        writeInBox(priceName, priceId, 'price');
        Prices.push(priceId);

        $("#prices").val(Prices.join(','));    

        // update form data
        //$("#filters_form").data("services", Services.join(',')); 
        
        $(".filter-icon").addClass("active");
        $(".filter-title").addClass("active");
    }); 
}

function addMoreFilters() {
    
    $('#more_filters').find(':checkbox').each(function() {
        
        // if filter is checked 
        if($(this).is(":checked")) {
            var filterName = $(this).data("label");
            var filterId = $(this).val();  
            
            writeInBox(filterName, filterId, 'additional');  
            $("#" + filterId).val(1);
            
            $(".filter-icon").addClass("active");
            $(".filter-title").addClass("active");
        } else {
             var filterId = $(this).val();      
             $("#" + filterId).val("");
        }
    }); 
    
    var phoneNames = [];
    var phoneList = '';
    
    //validate and add phone number search
    $(".phone_section").each(function(){
        
        var filterId = $(this).data("value");
        var filterValue = $(this).val();
        
        //if(filterValue != '') {
            
            if(filterValue != '' && $.isNumeric(filterValue)) {
                
                $("#" + filterId).val(filterValue);
                
                phoneNames.push(filterValue);
                phoneList =   phoneNames.join('.');
                phoneList = phoneList.replace(/\.\s*$/, "");
                console.log(phoneList);
                
                $("#remove_phones").closest('p').remove();
                writeInBox(phoneList, 'phones', 'additional_phone');
                 
                $(".filter-icon").addClass("active");
                $(".filter-title").addClass("active");    
            
            } else {
                 $(this).val('');
                 $("#" + filterId).val('');
                 //console.log($("#" + filterId).val());
            }
        //}
    });
    
    
}



function writeInBox(name, id, filterType) {
    
    $("#selected_filters").append('<p>' + name +'<a href="javascript:void(0)" id="remove_'+ id +'" data-value="'+ id +'" class="delete_filter ' + filterType + '"><i class="fa fa-close"></i></a></p>');
    //$("#selected_filters").append('<p>' + name +'<a href="javascript:void(0)" id="remove_'+ id +'" data-value="'+ id +'" class="delete_filter ' + filterType + '"><i class="fa fa-times"></i></a></p>');  // BROWSE V2

    // for each new dynamically added element you must enable deletion
    deleteFilter()
}

function deleteFilter() {
    
    $(".delete_filter").on('click', function(e) {
        e.stopPropagation();
        
        $(this).closest('p').remove();
        
        //!!!!!!!!!!!!!! you must uncheck checkboxes or radio and then update form data
        if($(this).hasClass('service')) {
            
            $('input[name="selected_services[]"][value="' + $(this).data("value") + '"]').attr('checked', false) ;
            
            //$("#filters_form").data("services", ''); 
            $("#services").val("");
            var Services = [];    
            $('input[name="selected_services[]"]:checked').each(function() {
                var serviceId = $(this).val();
                Services.push(serviceId);
                // update form data
                //$("#filters_form").data("services", Services.join(',')); 
                $("#services").val(Services.join(','));    
            });
        }
       
        if($(this).hasClass('price')) {
            
            $('input[name="selected_prices[]"][value="' + $(this).data("value") + '"]').attr('checked', false) ;
            
            //$("#filters_form").data("services", ''); 
            $("#prices").val("");
            var Prices = [];    
            $('input[name="selected_prices[]"]:checked').each(function() {
                var priceId = $(this).val();
                Prices.push(priceId);
                // update form data
                //$("#filters_form").data("services", Services.join(',')); 
                $("#prices").val(Prices.join(','));    
            });
        }
       
        if($(this).hasClass('availability')) {
            // remove radio
            $('input[name="location_filters"][value="' + $(this).data("value") + '"]').attr('checked', false) ;
            var availId = $(this).val();
            //$("#filters_form").data("availability", ""); 
            $("#location").val(availId); 
        }
        
        if($(this).hasClass('additional')) {
            console.log("additional")
            console.log($(this).data("value"))
            // remove checkbox
            $('input[name="selected_' + $(this).data("value") + '"]').attr('checked', false) ;
            //$("#filters_form").data("availability", ""); 
            $("#" + $(this).data("value")).val(''); 
        }
        
        if($(this).hasClass('additional_phone')) {
            //console.log($(this).data("value"))
            // remove checkbox
            $('input[name="selected_phone1]').val('');
            $('input[name="selected_phone2]').val('');
            $('input[name="selected_phone3]').val('');
            //$("#filters_form").data("availability", ""); 
            $("#phone1").val(''); 
            $("#phone2").val(''); 
            $("#phone3").val(''); 
        }
        
    });
      
    
} 