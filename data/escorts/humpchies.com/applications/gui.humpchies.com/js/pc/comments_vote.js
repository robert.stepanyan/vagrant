$(document).ready(function () {
    $(".thumbsup").on('click', function () {
        sendVote($(this).data("comment"), $(this).data("ad"), 'up');
    });
    
    $(".thumbsdown").on('click', function () {
        sendVote($(this).data("comment"), $(this).data("ad"), 'down');
    });

    //console.log($(".content-full").height());

    // remove the show more for each elements that is not higher than 40px
    $(".content-full").each(function(){
        console.log($(this).height());
        if($(this).height() <= 40) {
            $(this).parent().next(".txtcol").remove();    
        }

    });



    $(".txtcol").click(function() {
        
        if($(this).prev().hasClass("truncate")) {
                $(this).children('a').html("<i class='fa fa-minus-circle'></i>");
            } else {
                $(this).children('a').html("<i class='fa fa-plus-circle'></i>");
            }
      $(this).prev().toggleClass("truncate");

    });


});


function sendVote(comment_id, ad_id, vote) { // vote: up or down
    
    var ln = document.documentElement.lang;
    var languageIsDefault = window.location.pathname.indexOf(ln) < 0;
    var content = $("#body > .wrapper_content");
    
    var url = "/comments/vote";
    if (!languageIsDefault) {
        url = '/'+ln+url;
    }  
    
    $.ajax({
        url : url,
        method:'post',
        data: {
            comment_id:comment_id, 
            ad_id:ad_id, 
            vote:vote
        },
        dataType:'json',
        error:function(){
            content.removeClass('overlay');
        },
        beforeSend:function(){
            //console.log("before send");
            //$('.success').remove();
            //$('.error').remove();  
            Noty.closeAll(); 
            content.addClass('overlay')
        },
        complete: function(){content.removeClass('overlay');},
        
        success:function (response) {
            if (Number(response.success) !== 1){
               new Noty({
                        //layout: 'topCenter',
                        theme: 'relax',
                        text: response.message,
                        type: 'error',
                        timeout: 1000,
                        progressBar: false,
                        modal: true,
                        closeWith: ["click"],
                        killer : true
                    }).show();
            } else {
                
                // stuff happens here
                if(typeof response.up != 'undefined') {
                    //object exists, do stuff
                    var upValue  = parseInt($("#up-" + comment_id).html()) + response.up;
                    
                    $("#up-" + comment_id).html(upValue)
                    if(response.up == 1 ){
                        $("#up-" + comment_id).siblings("a.thumbsup").addClass("active");
                    }
                    if(response.up == -1 ){
                        $("#up-" + comment_id).siblings("a.thumbsup").removeClass("active");
                    }
                }
                
                if(typeof response.down != 'undefined'){
                    //object exists, do stuff
                    var downValue  = parseInt($("#down-" + comment_id).html()) + response.down;
                    $("#down-" + comment_id).html(downValue)
                    if(response.down == 1 ){
                        $("#down-" + comment_id).siblings("a.thumbsdown").addClass("active");
                    }
                    if(response.down == -1 ){
                        $("#down-" + comment_id).siblings("a.thumbsdown").removeClass("active");
                    }
                }
                
                //if(typeof response.message != 'undefined' && response.message != ''){
//                    //$("#body").prepend('<p class="success">'+response.message+'</p>');
//                    
//                    new Noty({
//                        //layout: 'topCenter',
//                        theme: 'relax',
//                        text: response.message,
//                        type: 'success',
//                        timeout: 1000,
//                        progressBar: false,
//                        modal: true,
//                        closeWith: ["click"],
//                        killer : true
//                    }).show();
//                    
//                }
            }
        }
    }) 
}