$(function () {

    var chatMain = $('.chat-main'),
        chatCommand = chatMain.attr('data-command'),
        chatEmpty = $('.chat-empty'),
        chatLeftPane = chatMain.find('.chat-left'),
        chatLeftContainer = chatLeftPane.find('ul'),
        chatLeftTemplate = chatLeftContainer.find('>li.template').removeClass('template').detach()[0].outerHTML,
        chatLeftPosition,
        chatRightPane = chatMain.find('.chat-right'),
        chatRightCenter = chatRightPane.find('.center'),
        chatRightTemplate = chatRightCenter.find('>div.message.template').removeClass('template').detach()[0].outerHTML,
        chatRightIntro = chatRightCenter.find('>div.intro'),
        chatRightContainer = chatRightCenter.find('>div:eq(1)'),
        chatRightPosition,
        directionUp = 'up',
        directionDown = 'down',
        typeMessage = 'message',
        typeChat = 'chat',
        maxId = 2147483647,
        isLoading = false,
        selectedChatId = chatLeftContainer.find('>.selected:first').attr('data-chat'),
        lastAjaxRequest = null,
        markAsReadTimeout = null;

    var top = chatRightPane.find('.top'),
        conversation = top.find('>span:first>em');

    var btnBlockUser = $('#chat-block-user'),
        btnUnblockUser = $('#chat-unblock-user'),
        btnDeleteChat = $('#chat-delete-conversation'),
        message = $('#chat-message'),
        btnReply = $('#chat-reply'),
        blockUserModal = $('#block-user-modal'),
        buttonBlockUserModal = $('#button-block-user-modal'),
        blockUserConfirmModal = $('#block-user-confirm-modal'),
        deleteChatModal = $('#delete-chat-modal'),
        buttonDeleteChatModal = $('#button-delete-chat-modal'),
        deleteChatConfirmModal = $('#delete-chat-confirm-modal');

    function getNextMessageId(container, direction, idDirection) {
        var item = container.find('>*' + ((direction === directionDown) ? ':last' : ':first'));
        if (item.length) {
            return parseInt(item.attr('data-id')) + idDirection;
        }
        return maxId;
    }

    var chatLeftScrollHandler = function () {
        if (this.scrollTop == chatLeftPosition) {
            return;
        }
        var direction = ((this.scrollTop < chatLeftPosition) ? directionUp : directionDown),
            data = {
                what: typeMessage,
                direction: direction,
                id: getNextMessageId(chatLeftContainer, direction, (direction === directionDown) ? -1 : 1)
            };
        if (
            (direction === directionUp)
            && (this.scrollTop < this.clientHeight * 0.4)
        ) {
            loadMessages(data);
        } else if (
            (direction === directionDown)
            && (this.scrollTop + this.clientHeight * 1.4 > this.scrollHeight)
        ) {
            loadMessages(data);
        }
        chatLeftPosition = this.scrollTop;
    };

    var chatRightScrollHandler = function () {
        if (!selectedChatId) {
            return;
        }
        if (this.scrollTop === chatRightPosition) {
            return;
        }
        var direction = ((this.scrollTop < chatRightPosition) ? directionUp : directionDown),
            data = {
                what: typeChat,
                direction: direction,
                id: getNextMessageId(chatRightContainer, direction, (direction === directionUp) ? -1 : 1),
                chatId: selectedChatId
            };
        if (
            (direction === directionUp)
            && (this.scrollTop < this.clientHeight * 1.4)
        ) {
            loadMessages(data);
        } else if (
            (direction === directionDown)
            && (this.scrollTop + this.clientHeight * 1.4 > this.scrollHeight)
        ) {
            loadMessages(data);
        }
        chatRightPosition = this.scrollTop;
    };

    function enableScrollHandlers() {
        chatLeftPosition = chatLeftPane[0].scrollTop;
        chatRightPosition = chatRightCenter[0].scrollTop;
        chatLeftPane.bind('scroll', chatLeftScrollHandler);
        chatRightCenter.bind('scroll', chatRightScrollHandler);
    }

    function disableScrollHandlers() {
        chatLeftPane.unbind('scroll');
        chatRightCenter.unbind('scroll');
    }

    function loadMessages(xdata, loadedCallback) {
        if (isLoading) {
            return;
        }
        isLoading = true;
        disableScrollHandlers();
        // load messages
        xdata.command = 'get';
        console.log('messages Loaded');
        console.log(xdata);
        lastAjaxRequest = $.ajax({
            type: "POST",
            url: chatCommand,
            data: xdata,
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    console.log("Success!");
                    console.log(data);
                    switch (data.what) {
                        case typeMessage:
                            updateChatLeftDom(data.direction, data.list);
                            var messages = chatLeftContainer.find('li').length;
                            chatMain.attr('data-messages', messages);
                            chatEmpty.attr('data-messages', messages);
                            if (loadedCallback) {
                                loadedCallback();
                            }
                            break;
                        case typeChat:
                            updateChatRightDom(data.direction, data.list);
                            if (data.list.length) {
                                chatLeftContainer.find('>li[data-chat="' + data.chatId + '"]').attr('data-blocked', data.blocked);
                                chatRightPane.attr('data-blocked', data.blocked);
                                chatRightPane.attr('data-isblocked', data.isBlocked);
                            }
                            if (loadedCallback) {
                                loadedCallback();
                            }
                            break;
                    }
                } else {
                    treatErrorResponse(data);
                }
                enableScrollHandlers();
                setTimeout(function(){
                    isLoading = false;
                }, 500);
            },
            error: function (req, status, error) {
                console.log("Error, try again: " + error);
                enableScrollHandlers();
                setTimeout(function(){
                    isLoading = false;
                }, 500);
            }
        });
    }

    function addItemToDom(container, direction, $item, oldItem) {
        if (oldItem.length) {
            oldItem.replaceWith($item);
        } else {
            if (direction === directionDown) {
                container.append($item);
            } else {
                container.prepend($item);
            }
        }
    }

    function updateChatLeftDom(direction, messages) {
        var message, template;
        for (var i in messages) {
            message = messages[i];
            template = chatLeftTemplate;
            template = template.replace('${messageId}', message.id);
            template = template.replace(/\${chatId}/g, message.chat_id);
            template = template.replace('${time}', message.message_time);
            template = template.replace(/\${unread}/g, message.unread);
            template = template.replace(/\${colour}/g, message.colour);
            template = template.replace('${avatar}', message.avatar);
            template = template.replace('${blocked}', message.blocked);
            template = template.replace('${image}', message.image);
            template = template.replace('${title}', message.title);
            template = $(template);
            addItemToDom(chatLeftContainer, direction, template, chatLeftContainer.find('#msg-' + message.chat_id));
        }
    }

    function updateChatRightDom(direction, messages) {
        var message, template;
        for (var i in messages) {
            message = messages[i];
            template = chatRightTemplate;
            template = template.replace(/\${messageId}/g, message.id);
            template = template.replace('${reply}', message.is_reply);
            template = template.replace('${time}', message.chat_time);
            template = template.replace('${message}', message.message);
            template = $(template);
            addItemToDom(chatRightContainer, direction, template, chatRightContainer.find('#msg-' + message.id));
        }
    }

    // reload chat message list
    function reloadChatRight()
    {
        if (!selectedChatId) {
            return;
        }
        if (lastAjaxRequest) {
            lastAjaxRequest.abort();
            lastAjaxRequest = null;
        }
        var data = {
            what: typeChat,
            direction: directionUp,
            id: maxId,
            chatId: selectedChatId
        };
        chatRightContainer.html('');
        isLoading = false; // force reload
        loadMessages(data, function () {
            chatRightCenter[0].scrollTop = chatRightCenter[0].scrollHeight;
            scheduleMarkAsRead(selectedChatId);
        });
    }

    // handle message selection
    chatLeftContainer.on('click', 'li', function () {
        var element = $(this);
        if (!element.hasClass('selected')) {
            chatLeftContainer.find('>li.selected').removeClass('selected');
            element.addClass('selected');
            //conversation.text(element.find('>div:last>span:first').text());
            if (element.attr('data-link').length) {
                conversation.attr('data-link', element.attr('data-link'));
            } else {
                conversation.removeAttr('data-link');
            }            
            //chatRightIntro.text(element.attr('data-intro'));
            conversation.text(element.attr('data-intro'));
            selectedChatId = element.attr('data-chat');
            reloadChatRight();
        }
        return false;
    });

    chatLeftContainer.on('focusin', 'li', function(){
        if (!$(this).hasClass('selected')) {
            $(this).trigger('click');
        }
    });

    chatLeftContainer.on('keyup', 'li', function(e){
        var key = (e.keyCode || e.which) + '';
        switch (key) {
            case '38': // up
                $(this).prev().focus();
                break;
            case '40': // down
                $(this).next().focus();
                break;
            case '36': // home   
                $(this).prevAll(':eq(1)').focus();
                break;
            case '35': // end
                $(this).nextAll(':eq(-1)').focus();
                break;
            case '33': // page up   
                $(this).prevAll(':eq(-10)').focus();
                break;
            case '34': // page down
                $(this).nextAll(':eq(10)').focus();
                break;
        }
    });
    
    // edit message
    message.keydown(function(e){
        if ($(this).val().length > 0) {
            btnReply.removeAttr('disabled');
            var key = (e.keyCode || e.which) + '';
            switch (key) {
                case '13': // enter
                    btnReply.trigger('click');
                    break;
                case '27': // esc
                    $(this).val('');
                    //btnReply.attr('disabled', true);
                    break;
            }
            return;
        }
        //btnReply.attr('disabled', true);
    });

    // reply button
    btnReply.on('click', function (e) {
        console.log('click');
        if($("#chat-message").val() == "" ){
            return false;
        }
        
        if (!selectedChatId) {
            return false;
        }
        
        var data = {
            command: 'reply',
            chatId: selectedChatId,
            message: message.val().substr(0, 500)
        };
        console.log(data);
        $.ajax({
            type: "POST",
            url: chatCommand,
            data: data,
            contentType: "application/x-www-form-urlencoded;charset=windows-1252",
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    console.log("Success!");
                    message.val('');
                    //btnReply.attr('disabled', true);
                    reloadChatRight();
                } else {
                    treatErrorResponse(data);
                }
            },
            error: function (req, status, error) {
                //console.log("Error, try again: " + error);
            }
        });
        return false;
    });

    function markAsRead (chatId) {
        $.ajax({
            type: "POST",
            url: chatCommand,
            data: {
                command: 'read',
                chatId: chatId
            },
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    //console.log("Success!");
                    chatLeftContainer.find('>li[data-chat="' + chatId + '"]').attr('data-unread', 0);
                } else {
                    treatErrorResponse(data);
                }
            },
            error: function (req, status, error) {
                //console.log("Error, try again: " + error);
            }
        });
    }
    
    function scheduleMarkAsRead(chatId) {
        if (!selectedChatId) {
            return;
        }
        if (markAsReadTimeout) {
            clearTimeout(markAsReadTimeout);
        }
        markAsReadTimeout = setTimeout(function(){
            markAsRead(chatId);
        }, 2000);
    }
    
    // block button
    btnBlockUser.click(function(e){
        if (!selectedChatId) {
            return false;
        }
        buttonBlockUserModal.attr('data-chat', selectedChatId).attr('disabled' , false);
        blockUserModal.show();
        return false;
    });

    buttonBlockUserModal.click(function(){
        $(this).attr('disabled' , true);
        $.ajax({
            type: "POST",
            url: chatCommand,
            data: {
                command: 'block',
                chatId: $(this).attr('data-chat')
            },
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    //console.log("Success!");
                    chatLeftContainer.find('>li[data-chat="' + data.chatId + '"]').attr('data-blocked', '1');
                    chatRightPane.attr('data-blocked', '1');
                    blockUserModal.hide();
                    blockUserConfirmModal.show();
                } else {
                    treatErrorResponse(data);
                }
            },
            error: function (req, status, error) {
                //console.log("Error, try again: " + error);
            }
        });
        return false;
    });

    // unblock button
    btnUnblockUser.click(function(){
        if (!selectedChatId) {
            return false;
        }
        $.ajax({
            type: "POST",
            url: chatCommand,
            data: {
                command: 'unblock',
                chatId: selectedChatId
            },
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    //console.log("Success!");
                    chatLeftContainer.find('>li[data-chat="' + data.chatId + '"]').attr('data-blocked', '0');
                    chatRightPane.attr('data-blocked', '0');
                } else {
                    treatErrorResponse(data);
                }
            },
            error: function (req, status, error) {
                //console.log("Error, try again: " + error);
            }
        });
        return false;
    });
    
    // delete button
    btnDeleteChat.click(function(){
        if (!selectedChatId) {
            return false;
        }
        buttonDeleteChatModal.attr('data-chat', selectedChatId).attr('disabled' , false);
        deleteChatModal.show();
        return false;
    });
        
    buttonDeleteChatModal.click(function(){
        $(this).attr('disabled' , true);
        $.ajax({
            type: "POST",
            url: chatCommand,
            data: {
                command: 'delete',
                chatId: $(this).attr('data-chat')
            },
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    //console.log("Success!");
                    var message = chatLeftContainer.find('>li[data-chat="' + data.chatId + '"]'),
                        lastId = message.attr('data-id');
                    message.remove();
                    loadMessages({
                        what: typeMessage,
                        direction: directionUp,
                        id: lastId
                    }, function(){
                        if (chatLeftContainer.find('>.selected:first').length === 0) {
                            // by default select a chat
                            chatLeftContainer.find('li:first').focus();
                        }
                    });
                    deleteChatModal.hide();
                    deleteChatConfirmModal.show();
                } else {
                    treatErrorResponse(data);
                }
            },
            error: function (req, status, error) {
                //console.log("Error, try again: " + error);
            }
        });
        return false;
    });
    
    function treatErrorResponse(data)
    {
        if (data.error && (data.code === 'login')) {
            window.location.href = '/user/login';
        }
    }
    
    // modal
    $('.modal-pm-close').click(function () {
        $(this).parents('.modal-pm:first').hide();
    });
    
    // ad link
    top.on('click', 'em', function(){
        var url = conversation.attr('data-link');
        if (url) {
            var win = window.open(url, '_blank');
            win.focus();
        }
    });
    
    // == initialization ==
   
    // bind events for loading
    enableScrollHandlers();

    if (chatLeftContainer.find('>.selected:first').length === 0) {
        // by default select a chat
        chatLeftContainer.find('li:first').focus().trigger('click');
    }

    if (selectedChatId) {
        // add conversation title & scroll to end
        var selected = chatLeftContainer.find('>.selected:first');
        //conversation.text(selected.find('>div:last>span:first').text());
        conversation.text(selected.attr('data-intro'));
        if (selected.attr('data-link').length) {
            conversation.attr('data-link', selected.attr('data-link'));
        } else {
            conversation.removeAttr('data-link');
        }        
       // chatRightIntro.text(selected.attr('data-intro'));
        chatRightCenter[0].scrollTop = chatRightCenter[0].scrollHeight;
        // mark chat as read
        scheduleMarkAsRead(selectedChatId);
    }

    // disable scroll
    jQuery(function ($) {
        if ($._customShowHide) {
            return;
        }
        $._customShowHide = true;
        var show = $.fn.show,
            hide = $.fn.hide;
        $.fn.show = function (speed, callback) {
            return $(this).each(function () {
                var obj = $(this);
                obj.trigger('show.custom');
                show.apply(obj, [speed, callback]);
            });
        };
        $.fn.hide = function (speed, callback) {
            return $(this).each(function () {
                var obj = $(this), visible = obj.is(':visible');
                hide.apply(obj, [speed, callback]);
                if (visible) {
                    obj.trigger('hide.custom');
                }
            });
        };
    });
    $('.modal-pm').bind('show.custom', function(){
        document.body.style.position = 'fixed';
    }).bind('hide.custom', function(){
        document.body.style.position = '';
    });

});
