(function ($) {
    $.fn.uploader = function (options) {
         return this.each(function (index) {
            options = $.extend({
                inputItem : $(this),
                inputItemName : $(this).attr('name'),
                dropZone: '#gallery-content',
                fileTypeWhiteList: ['jpg', 'png', 'jpeg', 'gif'],
                maxFileSize: 2097152,
                maxNumberOfFiles: 0, 
                badFileTypeMessage: 'Sorry, we\'re unable to accept this type of file.',
                ajaxUrl: '/verification/images',
                ajaxAdditionalFields : [],
                callbackFunction: false,
                gallery_container: '#gallery-content',
            }, options);

            var state = {
                fileBatch: [],
                isUploading: false,
                isOverLimit: false,
                listIndex: 0
            };
            var dom = {
                uploaderBox: $(options.dropzone), 
                contentsContainer: $(options.gallery_container), 
                form: options.inputItem.closest('form')
            }
            // empty out whatever is in there
            //dom.contentsContainer.empty();
            
            // create and attach UI elements
            setupDOM(dom);
            function setupDOM(){
                options.inputItem.change(function(){
                    console.log("this files>>>");
                    console.log(this.files);
                    console.log("---|||");
                    var fl = this.files;
                    console.log( 'length' + fl.length);
                    
                    for( var h = 0; h < fl.length; h++){
                        if(options.maxNumberOfFiles && (state.fileBatch.length) >= options.maxNumberOfFiles){
                           alert("you can upload only " + options.maxNumberOfFiles + " files."); 
                           return;
                        }else{
                            
                            AddImage(fl[h]);   
                        }
                    }
                })
                dom.contentsContainer.on('click', '.gallery-remove-button', removeItemHandler);
            }
            
            function AddImage(file){
                
                var fileName = cleanName(file.name);
                var fileSize = file.size;
                var id = state.listIndex;
                var sizeWrapper;
                
                if(fileSize  > 2 * 1024 * 1024  ){
                    alert('File size exceeds 2 MB');
                    return false;
                }
                
                dom.contentsContainer.parent().addClass("upload-gallery-preview-content").show();
                //console.log(options.inputItemName);
                if (options.fileTypeWhiteList.indexOf(getExtension(file.name).toLowerCase()) !== -1) {
                     state.fileBatch.push({file: file, id: id, fileName: fileName, fileSize: fileSize});   
                       state.listIndex++;
                    var fileNameWrapper = $('<span class="gallery_item_text gallery_filename">' + fileName + '</span>');
                    var fileSizeWrapper = $('<span class="gallery_item_text  gallery_size">' + formatBytes (fileSize, 2) + '</span>');
                    var listItem = $('<div class="gallery_item" data-index="' + id + '"></div>');
                    var thumbnailContainer = $('<span class="gallery_item_thumbnail"></span>');
                    var thumbnail = $('<img class="gallery_img_thumbnail"><i class="fa fa-spinner fa-spin uploader__icon--spinner"></i>');
                    var removeLink = $('<span class=""><button class="gallery-remove-button fa fa-times" data-index="' + id + '"></button></span>');         
                    var uploaded   = $('<div class="gallery-image-uploaded fa fa-check"><div>');         
                    var progress = $('<div class="progress-bar-container " id = "pb_' + id + '"><div class="progress-bar"></div></div>');
                    var transparency =$('<div class="transparency" ></div>'); 
                     
                }else{
                    return;
                }
                // create the thumbnail, if you can
                if (window.FileReader && file.type.indexOf('image') !== -1) {
                    var reader = new FileReader();
                    reader.onloadend = function () {
                        listItem.css('background-image', 'url(' +reader.result + ')');
                        thumbnail.parent().find('i').remove();
                    };
                    reader.onerror = function () {
                        thumbnail.remove();
                    };
                    reader.readAsDataURL(file);
                } else if (file.type.indexOf('image') === -1) {
                    thumbnail = $('<i class="fa fa-file-o uploader__icon">');
                }

                //thumbnailContainer.append(thumbnail);
               // listItem.append(thumbnailContainer);

               listItem
                    .append(transparency)
                    .append(progress)
                    .append(fileNameWrapper)
                    .append(fileSizeWrapper)
                    .append(removeLink)
                    .append(uploaded);

               dom.contentsContainer.append(listItem);
               upload(state.listIndex - 1, file);
               //console.log(state);
            } 
            
            function removeItemHandler (e) {
                e.preventDefault();
                e.stopPropagation();
                if (!state.isUploading) {
                    var removeIndex = $(e.target).data('index');
                    removeItem(removeIndex);
                    $(e.target).parent().remove();
                }

            }

            function removeItem (id) {
                // remove from the batch
                for (var i = 0; i < state.fileBatch.length; i++) {
                    if (state.fileBatch[i].id === parseInt(id)) {
                        state.fileBatch.splice(i, 1);
                        break;
                    }
                }
                // remove from the DOM
               
                dom.contentsContainer.find('div[data-index="' + id + '"]').remove();
                removeUpload(id);
            }
            
            function getExtension (path) {
                var basename = path.split(/[\\/]/).pop();
                var pos = basename.lastIndexOf('.');

                if (basename === '' || pos < 1) {
                    return '';
                }
                return basename.slice(pos + 1);
            }
            
            function cleanName (name) {
                name = name.replace(/\s+/gi, '-'); // Replace white space with dash
                return name.replace(/[^a-zA-Z0-9.\-]/gi, ''); // Strip any special characters
            }
            function formatBytes (bytes, decimals) {
                if (bytes === 0) return '0 Bytes';
                var k = 1024;
                var dm = decimals + 1 || 3;
                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                var i = Math.floor(Math.log(bytes) / Math.log(k));
                return (bytes / Math.pow(k, i)).toPrecision(dm) + ' ' + sizes[i];
            }
            function upload(index, file){
                 var data = new FormData();
                 data.append(options.inputItemName , file, file.fileName);
                 
                 for(var  i = 0 ; i < options.ajaxAdditionalFields.length; i++){
                    console.log(options.ajaxAdditionalFields[i]+ ' = ' + $("#" + options.ajaxAdditionalFields[i]).val() );
                    data.append(options.ajaxAdditionalFields[i], $("#" + options.ajaxAdditionalFields[i]).val());                
                 }
                 
                 $.ajax({
                        type: 'POST',
                        url: options.ajaxUrl,
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: 'JSON',
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            //Download progress
                            xhr.addEventListener("progress", function (evt) {
                                console.log(evt.lengthComputable);
                                console.log("current index:" + index);
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    $("#pb_" + index ).children(".progress-bar").css("width",  Math.round(percentComplete * 100) + "%");
                                }
                            }, false);
                            return xhr;
                        },
                        
                        success : function(data){
                            console.log(data);
                            console.log("success current index:" + index);
                            dom.contentsContainer.find('div[data-index="' + index + '"]').addClass('uploaded');    
                            if ( options.callbackFunction) {
                                console.log("callsuccessdata"); 
                                window[options.callbackFunction]( data, index);
                            } 
                        }
                    });
            }

            
         });
    }

   
}(jQuery));
 $(document).ready(function() {
    $(".gallery-content").on('click','.gallery_item', function(){
      var url = $(this).css('background-image')
      if(url.indexOf('url') !== -1){
        url = url.substring(5, url.length-2);    
      }
       $( ".backgroundLargeGallery" ).remove(); 
       $( ".previewLargeGallery" ).remove(); 
       var blur = $("<div>");
       var plg = $("<div>");
       blur.addClass("backgroundLargeGallery");
       plg.addClass("previewLargeGallery");
       var img = $("<img>");
       img.attr('src',url);
       plg.append(img);
       console.log(img.width());
       plg.css('margin-left', parseInt(img.width()/2) +'px' )
       plg.css('margin-top', parseInt(img.height()/2) +'px' )
       $(body).append(blur);
       $(body).append(plg);
       img.on('load', function(){
        $( ".previewLargeGallery" ).css('margin-left', '-' + parseInt($(this).width()/2) +'px' )
        $( ".previewLargeGallery" ).css('margin-top',  '-' + parseInt($(this).height()/2) +'px' )
       });
    });
    $(body).on('click','.backgroundLargeGallery', function(){
       $( ".backgroundLargeGallery" ).remove(); 
       $( ".previewLargeGallery" ).remove(); 
    }); 
    $(body).on('click','.previewLargeGallery', function(){
       $( ".backgroundLargeGallery" ).remove(); 
       $( ".previewLargeGallery" ).remove(); 
    }); 
 });