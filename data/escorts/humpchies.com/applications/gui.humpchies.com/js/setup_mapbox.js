//TO DO EDIT !!!!!!!!!!!!!!!!!!!!!!!!!!!!

$(function () {
    init();
});

var myMap, myMarker, myNav;

function init() {
    console.log("init"); 
    // get map info
    var $mapData = $('#map');
    var $hideMap = $('#hide_map');
    
    if (typeof $mapData.data('lat') !== 'undefined' && typeof $mapData.data('long') !== 'undefined') {          // THE MAP ALREADY HAS A MARKER
        //console.log("COORDS ARE SET");
        
        if($('#hide_map_check').val() == 'no'){
            $("#show_map").hide();
            codeCoords($mapData.data('lat') , $mapData.data('long') );
        }
        
        $("#show_map").on("click", function(){
            //console.log("clicked");
            $('#hide_map').val('no');    
            $('#hide_map_check').val('no');    
            codeCoords($mapData.data('lat') , $mapData.data('long') );
            
            $(this).hide();
            $("#remove_map").css('display', 'flex'); // show delete button
            
            $(".add-success").hide();
            $(".remove-success").hide();
        });
        
    } else {
        // create new map
        //console.log("Lat long are totally not set");
        
        // enable hide/show map
        $("#show_map").on("click", function(){
            console.log(" show map clicked");
            
            $(".add-success").hide();      // hide messages
            $(".remove-success").hide();
            
            $('#hide_map').val('no');            // update hidden attributes
            $('#hide_map_check').val('no');     
            
            // show montreal metro city
            console.log($("#city").val());
            if($("#city").val() != '') {
                var lat =  $("#city").find(':selected').data('lat')
                var lng =  $("#city").find(':selected').data('lng');    
            } else {
                var lat = 45.503601;    //Montreal
                var lng = -73.579995;
            }
            codeCoords(lat, lng);
            
            $(this).hide(); //hide show_map btn
            $("#remove_map").css('display', 'flex'); // show delete button
        });
    }
    
    
    $("#remove_map").on("click", function() {    // user wants to remove the map
        //console.log("remove map was clicked");
        
        $('#hide_map').val('yes');
        $('#hide_map_check').val('yes');  
        
        console.log(myMap);
         
        if (myMap) {
            myMarker.remove();// Destructor of the navigation controls 
            myMap.remove();// Destructor of the map 
            myMap = null;
        }
        $(".add-success").hide();
        $(".remove-success").css('display', 'block');
        // update map display 
        $(".map-container").css("display", "flex");    
        $("#show_map").show();
        $(this).hide();
    }); 
   
    $("#city").on("change", function() {
        console.log("City changed");
        console.log($(this).find(':selected').data('lat'))
        console.log($(this).find(':selected').data('lng'))
        var lat =  $(this).find(':selected').data('lat')
        var lng =  $(this).find(':selected').data('lng');
        $("#maps_city").val($(this).val()); // update city val  44.699624, -73.452526
        codeCoords(lat, lng)
    });
    
}



/*
https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/geojson(%7B%22type%22%3A%22Point%22%2C%22coordinates%22%3A%5B-73.571504074097%2C45.503189990622%5D%7D)/-73.571504074097,45.503189990622,12/660x300?access_token=pk.eyJ1IjoiZ2lnaWtlbiIsImEiOiJja2YyZXNncWcxMjF5MnFxZHEza202eWVrIn0.4t8DEWW4vaIMxbujBhj3Zg
*/


function codeCoords(lat, lng) {
    console.log(lat);
    console.log(lng);
    console.log($('#hide_map_check').val());
    if( $('#hide_map_check').val() == 'no' ) {
    
        console.log(myMap);
        
        if (myMap) {
            myMarker.remove();// Destructor of the map 
            myMap.remove();// Destructor of the map 
            myMap = null;
        }  
        
        // update map display 
        $(".map-container").css("display", "block");    
        
            
        mapboxgl.accessToken = 'pk.eyJ1IjoibWF4d2lydGhtYXBzIiwiYSI6ImNrbWFiNm5yaDFxMG8ycWtubDhvaXF6eGkifQ.zn5zSZgAcTS0Ksa7PT12jw';     
    
        myMap = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [lng, lat],
            zoom: 13
            });
            
        // Add zoom and rotation controls to the map.
        myNav = new mapboxgl.NavigationControl();
        myMap.addControl(myNav);
    
        myMarker = new mapboxgl.Marker()    // add default marker
            .setLngLat([lng, lat])
            .addTo(myMap);
    
         // initialize default coords
        $("#maps_lat").val(lat);
        $("#maps_long").val(lng);
    
        // enable marker click
        myMap.on('click', addMarker);  // when clicking map, set marker
    }      
}

function addMarker(e) {
    console.log("here");
    if (myMarker) {
        myMarker.remove();// Destructor of the marker 
    } 
    
    myMarker = new mapboxgl.Marker()
        .setLngLat(e.lngLat)
        .addTo(myMap);
     

    console.log(e.lngLat);
    console.log(e.lngLat.lat);
    // update coords
    $("#maps_lat").val(e.lngLat.lat);
    $("#maps_long").val(e.lngLat.lng);         
  
    $(".remove-success").hide();
    $(".add-success").css('display', 'block');
}


