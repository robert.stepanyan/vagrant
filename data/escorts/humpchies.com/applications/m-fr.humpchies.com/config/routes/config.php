<?php
/*
+---------------------------------------------------------------------------+
| fr.humpchies.com Routes                            				        |
| =======================                                                   |
|                                                                           |
| Copyright (c) Ivan Manzanilla                                             |
|                                                                           |
|   Email:              ivan_manzanilla@videotron.ca                        |
|   Phone:              514-882-2800                                        |
|   Fax:                n/a                                                 |
|   Mail Address:       574 Beaurepaire                                     |
|                       Beaconsfield, Quebec, Canada                        |
|                       H9W 3E5                                             |
|                                                                           |
| This program is NOT free software; you can NOT redistribute it and/or     |
| modify it, unless explicitly authorized by Ivan Manzanilla:               |
|                                                                           |
| This program is distributed in the hope that it will be useful, but       |
| WITHOUT ANY WARRANTY; without even the implied warranty of                |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      |
|                                                                           |
| DESCRIPTION: This file declares one array to define the routes 		    |
| (controller/action combos) that are available through the site. All       | 
| routes are mapped. The keys are actual URL components, the values are the |
| Controller action combinations.								            |
|										                                    |
| HISTORY:                                                                  |
|                                                                           |
|                   December 4, 2014: [ymanzanilla] Creation Putous!        |
|                                                                           |
+---------------------------------------------------------------------------+
*/

// ==========================================================================
// ROUTES
// ==========================================================================   
$config = array(    ''                              => array('\Modules\Humpchies\RedirectionController::siteRoot'),
                    'index.php'                     => array('\Modules\Humpchies\RedirectionController::siteRoot'),
                    'mes-annonces'                  => array('\Modules\Humpchies\RedirectionController::myAds'),
                    'carte-du-site'                 => array('\Modules\Humpchies\RedirectionController::siteMap'),
                    'rss'                           => array('\Modules\Humpchies\RedirectionController::rss'),
                    'activer-inscription'           => array('\Modules\Humpchies\RedirectionController::accountActivation'),
                    'BingSiteAuth.xml'              => array('\Modules\Humpchies\RedirectionController::bingSiteAuthXml'),
                    'google1da93fedf20afa03.html'   => array('\Modules\Humpchies\RedirectionController::googleVerification'),
                    'meta.txt'                      => array('\Modules\Humpchies\RedirectionController::meta'),
                    'robots.txt'                    => array('\Modules\Humpchies\RedirectionController::robots'),
                    'sitemap.xml'                   => array('\Modules\Humpchies\RedirectionController::siteMapXml'),
                    'details/annonce'               => array('\Modules\Humpchies\RedirectionController::adDetails'),
                    '404'                           => array('\Modules\Humpchies\RedirectionController::notFound'),
                    'trouver/annonce'               => array('\Modules\Humpchies\RedirectionController::frenchSearch'),
                    'liste/annonces'                => array('\Modules\Humpchies\RedirectionController::adListingFrench'),
                    'liste_complete/annonces'       => array('\Modules\Humpchies\RedirectionController::fullListing'));?>