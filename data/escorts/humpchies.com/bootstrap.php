<?php
// Define the Framework path for the current environment
define('INSTALL_PATH', __DIR__ . '/');

// Indicate whether or not we want to use Memcache
const USE_CACHE             = TRUE;

// Define the type of working environment: .dev, .stage, .prod
const ENVIRONMENT_TYPE      = '.prod';

// Bring the framework in...
require_once(INSTALL_PATH . 'core/execute.php'); ?>

