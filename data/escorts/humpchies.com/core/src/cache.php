<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that are used system wise to manipulate CACHE; MEMCACHE that is.
 */

Namespace Core;

final class Cache
{   
    // Members
    // =======                   
    private static $default_params                  = NULL;                     // The default parameters of this class. It stores values such as: enable, cluster, server, etc.
    private static $clusters                        = NULL;                     // The list of Memcached instances
    
    // Constants
    // =========
    const DEFAULT_EXPIRY                            = 3600;                     // The minimum number of seconds an item should remain cached: 1 hour
    const DEFAULT_LOCK_EXPIRY                       = 5;                        // The maximum number of seconds a lock to fetch data from a data store should remain alive: 5 seconds

    /**
    * Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __sleep()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __wakeup()
    {}
    
    /**
    * This function instantiates Memcached on a per cluster basis and loads the 
    * servers associated with the cluster. No connection is made at this point. 
    * 
    * @return a Memcached object representing the desired memcache cluster
    */
    public static function getCluster($prefix_key = '')
    {
        if(@self::$clusters[$prefix_key] !== NULL)
            return @self::$clusters[$prefix_key];
             
        self::$clusters[$prefix_key] = new \Memcached();
        
        if($prefix_key !== '')
            self::$clusters[$prefix_key]->setOption(\Memcached::OPT_PREFIX_KEY, $prefix_key);

        self::$clusters[$prefix_key]->setOption(\Memcached::OPT_LIBKETAMA_COMPATIBLE, TRUE);                              // Specify that Libketema is available for hashing on this system
        //self::$clusters[$prefix_key]->setOption(\Memcached::OPT_DISTRIBUTION, \Memcached::DISTRIBUTION_CONSISTENT);     // Use the consitent disribution hashing mechanism to store data
        self::$clusters[$prefix_key]->setOption(\Memcached::OPT_CONNECT_TIMEOUT, 250);                                    // Attempt to connect for 250ms before timing out on that specified cache pool
        self::$clusters[$prefix_key]->setOption(\Memcached::OPT_SERVER_FAILURE_LIMIT, 200);                               // The connection limit for get/set failiures
        //self::$clusters[$prefix_key]->setOption(\Memcached::OPT_REMOVE_FAILED_SERVERS, TRUE);                           // Remove a cache pool if it has reached the failiure limite
        self::$clusters[$prefix_key]->setOption(\Memcached::OPT_RETRY_TIMEOUT, 30);                                       // Remove a cache pool if it has reached the failiure limite
        
            
         
        self::$clusters[$prefix_key]->addServers(\Core\Utils\Config::getConfigValue('\Core\Memcached::servers'));     

        // Return then newly created Memcache instance
        return self::$clusters[$prefix_key];
    }
    
    /**
    * This function returns the default parameters of Memcached as an array if 
    * Memcache is globally enabled. FALSE if Memcache is not enabled.
    * 
    * @return mixed Our default Mecahed parameters as an array
    */
    private static function getCacheDefaultParameters()
    {   
        // If the parameters are already loaded... simply return them
        if(self::$default_params !== NULL)  
            return self::$default_params;

        // If Cache is enabled return the following array as the default parameters for our instances
        return (self::$default_params = array(  'enable'        => TRUE,                            // Use memcache
                                                'expire'        => self::DEFAULT_EXPIRY,            // Cache things for one hour default           
                                                'lock'          => self::DEFAULT_LOCK_EXPIRY,       // Get exclusive data access for 5 seconds
                                                'reset'         => FALSE));                         // wheter or not to reset the contents of cache          
    }
    
    /**
    * This public static function tries to add a 5 second "lock" key into memcache. This "lock" is used  throughout
    * data gathering rutines to allow only one thread to access data stores, thus avoiding an stampede
    * of requests to data stores when cache elements die out.
    *
    * @param string $request_id This can be any string, it needs however to be unique across the framework to avoid locking threads needlessly
    * @return bool TRUE if the "lock" is acquired, FALSE otherwise
    */
    public static function getDataAccessLock($key, $lock_length = self::DEFAULT_LOCK_EXPIRY)
    { 
        // Try to add the lock in Memcache...
        return (USE_CACHE !== TRUE ? FALSE : self::getCluster()->add('lock.'.$key, 1, $lock_length));
    }

    /**
    * This public static function deletes a previouly acquired "lock" in memcache. It is used by data gathering rutines
    * as means to clean up after they have retrived their data.
    *
    * @param string $request_id The id of the "lock" we want to delete
    * @return bool TRUE if the "lock" is deleted, FALSE otherwise
    */
    public static function deleteDataAccessLock($key)
    {
        // Try to delete the lock from Memcached...If Cache is enabled...
        if(USE_CACHE === TRUE) self::getCluster()->delete('lock.'.$key);
    }
    
    /**
    * This public static function retrieves a particular key. It wraps around Memcached::get and Memcached::getByKey. It calls
    * one method of the other based on the value of parameter $server. If $server is NULL it calls Memcached:get,
    * Memcached::getByKey otherwise.
    * 
    * @param string The name of the cluster... pass 'DEFAULT' to use the default Memcache cluster
    * @param string The key under which the data we are looking for is stored in Memcache
    * 
    * @return mixed The data stored in Memcache if found, FALSE otherwise 
    */
    public static function get($key, $server_key = NULL)
    {
        // Try to get the value from Memcached...
        return (USE_CACHE !== TRUE ? FALSE : ($server_key === NULL ? self::getCluster()->get($key) : self::getCluster($server_key)->getbyKey($server_key, $key)));
    }
    
    /**
    * This public static function retrieves multiple values stored in Memcached stored under multiple keys. It wraps around 
    * Memcached::getMulti and Memcached::getMulti ByKey. It calls one method of the other based on the value of 
    * parameter $server. If $server is NULL it calls Memcached:getNulti, Memcached::getMultiByKey otherwise.
    * 
    * @param string The name of the cluster... pass 'DEFAULT' to use the default Memcache cluster
    * @param string The keys under which the different pieces of data are stored in Memcache
    * 
    * @return array If the pieces of data are found in Memcache, FALSE otherwise 
    */
    public static function getMulti($keys, $flags, $server_key = NULL)
    {
        // Try to get multiple items from Memcached...
        return (USE_CACHE !== TRUE ? FALSE : ($server_key === NULL ? self::getCluster()->getMulti($keys, $flags) : self::getCluster($server_key)->getMultiByKey($server_key, $keys, $flags)));
    } 
    
    public static function resetExpiry($key, $new_expiry = self::DEFAULT_EXPIRY, $server_key = NULL)
    {
        // Attempt to save a single item to Memcached...
        return (USE_CACHE !== TRUE ? FALSE : ($server_key === NULL ? self::getCluster()->touch($key, $new_expiry) : self::getCluster($server_key)->touchbyKey($server_key, $key, $new_expiry)));    
    }

    /**
    * This public static function sets or replaces a value in cache.
    *
    * @param string The key under which the data is to be stored in Memcache
    * @param mixed The data we wish to store in Memcache
    * @param int The number of seconds we wish to store the data in Memcache for
    * 
    * @return bool TRUE if the item is succefully saved, FALSE on failure
    */
    public static function save($key, $data, $expire = self::DEFAULT_EXPIRY, $server_key = NULL)
    {
        // Attempt to save a single item to Memcached...
        return (USE_CACHE !== TRUE ? FALSE : ($server_key === NULL ? self::getCluster()->set($key, $data, $expire) : self::getCluster($server_key)->setByKey($server_key, $key, $data, $expire)));
    }

    /**
    * This public static function sets multiple item is cache; it wraps around memcache::setMultiByKey or memcached::setMulti
    * depending if a cache server is passed or not respectively.
    * 
    * @param string The name of the cluster... pass 'DEFAULT' to use the default Memcache cluster 
    * @param mixed $items A relational array containing the keys => values we wish to save
    * @param int The number of seconds we wish to store the data in Memcache for
    *
    * @return bool TRUE if the items are succefully saved, FALSE otherwise
    */
    public static function saveMulti($items, $expire = self::DEFAULT_EXPIRY, $server_key = NULL)
    {
        // Attempt to save a multiple items to Memcached...
        return (USE_CACHE !== TRUE ? FALSE : ($server_key === NULL ? self::getCluster()->setMulti($items, $expire) : self::getCluster($server_key)->setMultiByKey($server_key, $items, $expire)));
    }
    
    /**
    * This public static function deletes a value from Memcache
    *
    * @param string The name of the cluster... pass 'DEFAULT' to use the default Memcache cluster
    * @param string The key under which the data is stored in Memcache
    * 
    * @return bool TRUE if the item is succefully deleted, FALSE on failure
    */
    public static function delete($key, $server_key = NULL)
    {
        // Attempt to delete a value from Memcached.... 
        return (USE_CACHE !== TRUE ? FALSE : ($server_key === NULL ? self::getCluster()->delete($key) : self::getCluster($server_key)->deletebyKey($server_key, $key)));
    } 
    
    /**
    * This public static function is used to validate and merge passed cached parameters to public static functions against 
    * an already known valid set. The highest known valid set is the one provided by default by the 
    * Cache class. 
    * 
    * @param array The set of parameters we wish to validate and merge. This parameter overwrites the 
    *               values found in the second parameter of this public static function. Except if the second parameter
    *               is absolut FALSE which implies that Memcache is globally disabled.
    * @param mixed A set of parameters known to be valid or FALSE when cache is disabled. If this parameter 
    *               is not passed the default cache parameters are used instead.
    * 
    * @return mixed An array id containning the merged parameters, FALSE if cache is globally disabled
    */
    public static function validateAndMergeParams($cache_parameters = NULL)
    {
        // Bail out if cache is disabled...
        if(USE_CACHE !== TRUE)
            return FALSE;
            
        $cache_parameters = array_merge(self::getCacheDefaultParameters(), ($cache_parameters === NULL ? array() : $cache_parameters));
        
        if($cache_parameters['enable'] !== TRUE)
            return FALSE;
        
        return $cache_parameters;
    }
	
	public static function cleanAll()
    {
        return (USE_CACHE !== TRUE ? FALSE : self::getCluster()->flush());
    } 
}