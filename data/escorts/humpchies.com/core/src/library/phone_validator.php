<?php


namespace Core\library;


class PhoneValidator
{
    private static $accessKey = '80835743fee970bfea06f09b7adc5eba';
    private static $uri = 'https://checknumber.online/api/';

    const CODE_404_NOT_FOUND = 404;
    const CODE_MISSING_ACCESS_KEY = 101;
    const CODE_INVALID_ACCESS_KEY = 101;
    const CODE_INVALID_API_FUNCTION = 103;
    const CODE_NO_PHONE_NUMBER_PROVIDED = 210;
    const CODE_NON_NUMERIC_PHONE_NUMBER_PROVIDED = 211;
    const CODE_INVALID_COUNTRY_CODE = 310;
    const CODE_USAGE_LIMIT_REACHED = 104;
    const CODE_HTTPS_ACCESS_RESTRICTED = 105;
    const CODE_INACTIVE_USER = 102;

    public static $ERROR_CODES = array(
        self::CODE_404_NOT_FOUND => self::ERROR_404_NOT_FOUND,
        self::CODE_MISSING_ACCESS_KEY => self::ERROR_MISSING_ACCESS_KEY,
        self::CODE_INVALID_ACCESS_KEY => self::ERROR_INVALID_ACCESS_KEY,
        self::CODE_INVALID_API_FUNCTION => self::ERROR_INVALID_API_FUNCTION,
        self::CODE_NO_PHONE_NUMBER_PROVIDED => self::ERROR_NO_PHONE_NUMBER_PROVIDED,
        self::CODE_NON_NUMERIC_PHONE_NUMBER_PROVIDED => self::ERROR_NON_NUMERIC_PHONE_NUMBER_PROVIDED,
        self::CODE_INVALID_COUNTRY_CODE => self::ERROR_INVALID_COUNTRY_CODE,
        self::CODE_USAGE_LIMIT_REACHED => self::ERROR_USAGE_LIMIT_REACHED,
        self::CODE_HTTPS_ACCESS_RESTRICTED => self::ERROR_HTTPS_ACCESS_RESTRICTED,
        self::CODE_INACTIVE_USER => self::ERROR_INACTIVE_USER,
    );

    const ERROR_PHONE_IS_INVALID = 'Core.library.PhoneValidator.ErrorPhoneIsInvalid';
    const ERROR_NOT_SUPPORTED_COUNTRY = 'Core.library.PhoneValidator.ErrorNotSupportedCountry';

    const ERROR_404_NOT_FOUND = 'Core.library.PhoneValidator.ErrorNotFound';
    const ERROR_MISSING_ACCESS_KEY = 'Core.library.PhoneValidator.ErrorMissingAccessKey';
    const ERROR_INVALID_ACCESS_KEY = 'Core.library.PhoneValidator.ErrorInvalidAccessKey';
    const ERROR_INVALID_API_FUNCTION = 'Core.library.PhoneValidator.ErrorInvalidApiFunction';
    const ERROR_NO_PHONE_NUMBER_PROVIDED = 'Core.library.PhoneValidator.ErrorPhoneNumberNotProvided';
    const ERROR_NON_NUMERIC_PHONE_NUMBER_PROVIDED = 'Core.library.PhoneValidator.ErrorNonNumericPhoneNumberProvided';
    const ERROR_INVALID_COUNTRY_CODE = 'Core.library.PhoneValidator.ErrorInvalidCountryCode';
    const ERROR_USAGE_LIMIT_REACHED = 'Core.library.PhoneValidator.ErrorUserLimitReached';
    const ERROR_HTTPS_ACCESS_RESTRICTED = 'Core.library.PhoneValidator.ErrorHttpsAccessRestricted';
    const ERROR_INACTIVE_USER = 'Core.library.PhoneValidator.ErrorInactiveUser';
    
    const ERROR_UNKNOWN = 'Core.library.PhoneValidator.ErrorUnknown';
    const NO_ERROR = 'Core.library.PhoneValidator.NoError';

    /**
     * @param string $phoneNumber
     * @param string $countryCode
     * @return string
     */
    public static function validate($phoneNumber, $countryCode = 'ca') {
        $phoneNumber = filter_var($phoneNumber, FILTER_SANITIZE_NUMBER_INT);
        if ($phoneNumber === false){
            return self::ERROR_UNKNOWN;
        }
        $phoneNumber = (string)((int)$phoneNumber);
        if (strpos($phoneNumber,'1') !== 0){
            $phoneNumber = "1{$phoneNumber}";
        }
        $uri = self::$uri . '?key=' . self::$accessKey . '&number=' . $phoneNumber;
        $ch = curl_init($uri);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Store the data:
        $json = curl_exec($ch);
        curl_close($ch);

        // Decode JSON response:
        $validationResult = json_decode($json, true);

        if (isset($validationResult['success']) && !$validationResult['success']) {
            $code = $validationResult['error']['code'];
            return self::$ERROR_CODES[$code] ? self::$ERROR_CODES[$code] : self::ERROR_UNKNOWN;
        }
        else {
            if ($validationResult['valid']) {
                if (strtolower($validationResult['country_code']) != strtolower($countryCode)) {
                    return self::ERROR_NOT_SUPPORTED_COUNTRY;
                }
            }
            else {
                return self::ERROR_PHONE_IS_INVALID;
            }
        }

        return self::NO_ERROR;
    }
}