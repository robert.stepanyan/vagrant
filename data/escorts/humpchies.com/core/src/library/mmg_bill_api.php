<?php

namespace Core\library;

class MmgBillAPI
{
    private $payment_link = 'https://secure.mmggate.com/payment/?';
    private $payment_link_mid = 'https://www.humpchies.com/payment/mid/?';
    private $merchantId = '';
    private $secret_key = '';
    private $ts = 'd'; //Stands for dynamic system
	private $currencyCode = '124';

    public function __construct()
    {
        // hump account
        // $this->merchantId = 'M1601';
        // $this->secret_key = 'Vf6M6nnrAy8vbgg7CmORps0O7RZJepG7';

        // Below is testing account
        // $this->merchantId = 'M1572';
        // $this->secret_key = 'qaRxBH6laojLkfkIbbUiRupyJLL0rcfv';

        $this->merchantId = 'M1601';
        $this->secret_key = 'Vf6M6nnrAy8vbgg7CmORps0O7RZJepG7';
    }

    public function getHostedPageUrl($amount, $transaction_id, $postback_url, $mid = false)
    {
        $c1 = 'humpchies:' . $amount;
        $amount = $amount * 100;

        $array_request = array(
            'mid' => $this->merchantId,
            'ts' => $this->ts,
            'ti_mer' => $transaction_id,
            'txn_type' => 'SALE',
            'cu_num' => $this->currencyCode,
            'amc' => $amount,
            'surl' => $postback_url,
            'c2' => 'humpchies.com'
        );

        /* create the hash signature */
        $sh = $this->create_hash($array_request, $this->secret_key);

        /* create the payment link */
        if($mid){
            $payment_link = $this->create_payment_link($array_request, $this->payment_link_mid, $sh);  
        }else{
            $payment_link = $this->create_payment_link($array_request, $this->payment_link, $sh);  
        }
        

        return $payment_link;
    }

    /* function to create the hash signature */
    private function create_hash ($array_request, $secret_key)
    {
        $hash_string = "";
        ksort($array_request);
        foreach ($array_request as $key => $value) {
            $hash_string .= "|" . $key . $value;
        }

        return hash_hmac("sha256", $hash_string, $secret_key, false);
    }

    /* function to create payment link */
    private function create_payment_link($array_request, $payment_link, $sh)
    {
        foreach ($array_request as $key => $value) {
            $payment_link .= $key . "=" . $value . "&";
        }

        return $payment_link .= "sh=" . $sh;
    }


}


?>
