<?php
  Namespace Core\library;  
  
  Class checkMobi{ 
     static $signatureKey = "B354FB87-B874-4781-B5E3-DD5D5118D503"; 
     static $apiKey = "863CAC6F-4289-4B0F-B435-8130F19E4861"; 
     static $baseUrl = "https://api.checkmobi.com/";
     static $version = "v1";
     static $validationURL = "/validation/request";
     static $notificationCallback = "https://www.humpchies.com/validation/phonecallback";
     static $platform = "desktop";
     
     static public function validationCall($phoneNumber){
         
         $header    = array();
         $header[]  = "Accept: application/json";   
         $header[]  = "Content-Type: application/json";   
         $header[]  = "Authorization: " . self::$apiKey;
         
         $data = array();
         $data['number']                = $phoneNumber;
         $data['type']                  = 'cli';
         $data['platform']              = self::$platform;
         $data['notification_callback'] = self::$notificationCallback; 
         
         //die();
         $response = self::curlPost(self::$baseUrl . self::$version . self::$validationURL , json_encode($data), $header );
     //             $response ='{
//    "id":"CLI-ED26AC71-807B-49B1-A81E-3956224A0CDC",
//    "type":"cli",
//    "dialing_number":"+40338630001",
//    "validation_info":
//    {
//        "country_code":40,
//        "country_iso_code":"RO",
//        "carrier":"Orange",
//        "is_mobile":true,
//        "e164_format":"+40338630001",
//        "formatting":"+40 338 630 001"
//    }
//}';
         return json_decode($response, true);   
     }
     
     static public function validationReverseCall($phoneNumber){
         
         $header    = array();
         $header[]  = "Accept: application/json";   
         $header[]  = "Content-Type: application/json";   
         $header[]  = "Authorization: " . self::$apiKey;
         
         $data = array();
         $data['number']                = $phoneNumber;
         $data['type']                  = 'reverse_cli';
         $data['platform']              = self::$platform;
         //$data['notification_callback'] = self::$notificationCallback; 
         $response = self::curlPost(self::$baseUrl . self::$version . self::$validationURL , json_encode($data), $header );
         

         return json_decode($response);   
     }  
     
     
     static function curlPost($url, $data=NULL, $headers = NULL) {
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if(!empty($data)){
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 

        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $response = curl_exec($ch);

        if (curl_error($ch)) {
            trigger_error('Curl Error:' . curl_error($ch));
        }

        curl_close($ch);
        return $response;
     }
     static function parseInfos($result){
        
        $infos = []; 
        $infos['callID']        = $result['id'];
        $infos['type']          = $result['type'];
        if($result['type'] == 'reverse_cli'){
            $infos['prefix']    = $result['cli_prefix'];
            $infos['hash']      = $result['pin_hash'];
        }else{
            $infos['number']    = $result['dialing_number']; 
        }
         
     }
     
  }
?>
