<?php
  Namespace Core\library;  
  
  Class checkVpn{ 
     static $apiKey = "62f584dbc94f53";      #https://ipinfo.io/ token
     static $baseUrl = "https://ipinfo.io/";
     const REDISVPNINFO ="vpninfolist"; 
     
     static $ipIntelBaseUrl = "http://check.getipintel.net/check.php";   #http://check.getipintel.net/check.php?ip=IPHere&contact=YourEmailAddressHere
     
     static function checkRedis($ip){
         
        $response = \Core\library\Redis::hget(self::REDISVPNINFO, "I" . $ip);
        if($response){
            return unserialize($response);
        }
        return false;
     }
     
     static function setRedis($ip, $value){
         
        $response = \Core\library\Redis::hset(self::REDISVPNINFO, "I" . $ip, serialize($value));
        
        return true;
     }
     
     static function isVpn($ip){
         if($response = self::checkRedis($ip)){
             $json = json_decode($response, true);
             //echo "read from redis:>>";
             return $json['vpn'];
         }else{
             $response = self::curlData($ip);
             self::setRedis($ip, $response);
             $json = json_decode($response, true);
             return $json['vpn']; 
         }
     }
     
     static function curlData($userIp) {
        //var_dump(self::$baseUrl . $userIp . '/privacy?token=' . self::$apiKey);
        $ch = curl_init(self::$baseUrl . $userIp . '/privacy?token=' . self::$apiKey);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Accept: application/json"] );
     

        $response = curl_exec($ch);

        if (curl_error($ch)) {
            trigger_error('Curl Error:' . curl_error($ch));
        }

        curl_close($ch);
        return $response;
     }
     
     
      static function curlIntelData($userIp) {
        
        var_dump(self::$ipIntelBaseUrl  .'?ip=' . $userIp .'&contact=gigi.ken2004@gmail.com');
        $ch = curl_init(self::$ipIntelBaseUrl  .'?ip=' . $userIp .'&contact=gigi.ken2004@gmail.com&format=json');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, "Accept: application/json" );
     

        $response = curl_exec($ch);

        if (curl_error($ch)) {
            trigger_error('Curl Error:' . curl_error($ch));
        }

        curl_close($ch);
        return $response;
     }
     
     
  }
?>
