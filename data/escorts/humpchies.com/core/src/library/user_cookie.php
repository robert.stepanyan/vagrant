<?php
  
namespace Core\library;

class UserCookie{
    static $cookie = "HLS12345DFR";
    
    static public function makeUniqueCode(){
        
        $code = md5(uniqid(rand(), true));
        //\Core\library\Log::debug($code,"newcode");
        if(\Modules\Humpchies\UniqueCookieModel::checkUnique($code)){
            \Modules\Humpchies\UniqueCookieModel::saveUnique($code);   
            return $code;
        }
        //\Core\library\Log::debug($code,"newcode");
        return false;
    }
    
    static public function getCode(){
        $code = false;
        do{
            $code = self::makeUniqueCode();    
       }while(!$code);
       return $code;
    }
    
    static public function cookieLink($userID){
        if(isset($_COOKIE[self::$cookie])){
             //we need to set a cookie and link the user to the code 
             $cookie = $_COOKIE[self::$cookie];
             //\Core\library\Log::debug($cookie,"COOkIE EXISTIS");
        }else{
             $cookie = self::getCode();
             setcookie(self::$cookie, $cookie, time() + (86400 * 300), "/");
             
        }
        
        // add cookies for forum     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        $forum_cookie = md5(uniqid(rand(), true));
        \Modules\Humpchies\UserModel::updateForumToken($userID, $forum_cookie);
        // cookie valid for 3 hours
        setcookie('humpfor', $forum_cookie, time() + 10800, '/', \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::base_domain'));
        
        $lang = (\Core\Utils\Session::getLocale(TRUE, TRUE) !== null)? \Core\Utils\Session::getLocale(TRUE, TRUE) : 'en-ca';
//        var_dump(\Core\Utils\Session::getLocale(TRUE, TRUE));exit();
        if(\Core\Utils\Session::getLocale(TRUE, TRUE) !== null) {            
            setcookie('hump_lang', trim(\Core\Utils\Session::getLocale(TRUE, TRUE)), null, '/', \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::base_domain'));    
        }
               
        \Modules\Humpchies\UserCookiesModel::addCookie($cookie, $userID); 
    }
    
}
