<?php
Namespace Core\library;
Class Log{
    static $logdir = "logs/";
    static public function debug($message, $title, $context = false){
        $file = @fopen(INSTALL_PATH . self::$logdir . date("Ymd") . ".log", "a+");
        if(is_object($message)){
            $message = var_export($message, true);
        }else{
            $message = (is_array($message))? print_r($message, true):$message;    
        }
        $context = ($context)? self::getContext(): "";
        @fwrite($file, "\n " . date("Y-m-d H:i:s"). " >". $title . ": " . $message . $context);
        @fclose($file);
    }    
    
    
    static public function debugUser($message, $title, $context = false){
        $file = @fopen(INSTALL_PATH . self::$logdir . 'member' . date("Ymd") . ".log", "a+");
        if(is_object($message)){
            $message = var_export($message, true);
        }else{
            $message = (is_array($message))? print_r($message, true):$message;    
        }
        $context = ($context)? self::getContext(): "";
        @fwrite($file, "\n " . date("Y-m-d H:i:s"). " >". $title . ": " . $message . $context);
        @fclose($file);
    }    
    
     static private function getContext(){

        $extend = [];

        $trace = debug_backtrace();

        foreach (array_reverse($trace,true) as $key => $value) {

            $extend[] = "    :: in FILE: " . $value['file'] . " Line: " . $value['line'] . "  >" . $key . ". " ;//. $value["class"] . $value['type'] . $value['function'] . "(" . implode(", ",(array)$value['args']) . ")  ";
        }

        return  implode("\n", $extend) . "\n";
    }  
   
}
