<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file is used to encapsulate all properties and methods
 * associated with templates and views.
 */

Namespace Core;

final class View
{
    // Members
    // =======
    private static $view_paths                  = NULL;     // A Singleton accumulator to store paths to views
    private static $device_extension            = NULL;     // A singleton to store the extension of the device behind the current HTTP request

    /**
     * Within the framework, GUI elements are divided into two categories: layouts
     * and views. The layouts are stored in the /views/layouts/ directory at the
     * module or application level. Layouts can be comprised of many files, that
     * might dynamically be injected. However, the framework always expects a
     * header and a footer.
     *
     * Views on the other hand live in the /views/* directories, also at the module
     * or application level. Views are stand alone files that can dynamically be
     * included anywhere. Nevertheless, the framework always expects a view for
     * each action.
     *
     * The above statements apply exclusively when the framework is used to produce
     * a Web application. CLI and Web services are faceless in nature.
     *
     * Therefore at this point we will declare identifiers for the 3 components that
     * the framework expects in a web application: Header, Footer, and Action. These
     * are useful when using the current class to retrieve the path to a the header,
     * footer, action or view.
     */
    const LAYOUT_COMPONENT_HEADER               = 'Core.View.LayoutComponentHeader';
    const LAYOUT_COMPONENT_FOOTER               = 'Core.View.LayoutComponentFooter';
    const LAYOUT_COMPONENT_ACTION               = 'Core.View.LayoutComponentAction';

    /**
     * View rendering is controlled by three factors:
     *
     *      1.- What the device supports. Support for this feature comes from
     *      \Core\Utils\Device::getPreferredInterface(), which based on headers
     *      and cookies will return a layout extension that best suits the device
     *      behind the current HTTP request. At the time of this writing, these
     *      extensions are:
     *
     *              - 'pc' for non-mobile devices or user defined non-mobile
     *              sessions
     *
     *              - 'rich' for non-webkit tablets or Windows 7+ mobile devices
     *
     *              - 'touch' for ipods, ipads, iphones, androids, webkit tablets,
     *              webkit blackberries, and any WebOS device
     *
     *              - 'lean' for any remaining devices
     *
     *      2.- What has been defined to be supported by the layout associated with
     *      the current route. Support for this feature comes from the routes file.
     *      Each route is expected to be an array comprehended of two elements: the
     *      first one containing the route mapping. The second one containing a
     *      relational array that defines layout properties. at the time of this
     *      writing, the following layout properties can be defined:
     *
     *              'supported_extensions': an array of any of the extensions from
     *              point #1 here above. This defines the layouts that a particular
     *              route will implement. This property is mandatory, when '*_mode'
     *              is set to either DEVICE_MODE or LOCALIZED_DEVICE_MODE
     *
     *              'header_location': the namespace that implements the header for
     *              the route. For example: \Modules\Humpchies\Default. If this
     *              property is not set, the value of 'global_location' is used instead
     *
     *              'header_mode': whether the header should be rendered based on
     *              device capabilities, language, language + device, rendered
     *              without any of the fore mentioned conditions; or not rendered
     *              at all. If this property is not set, the value of 'global_mode'
     *              is used instead
     *
     *              'action_location': the namespace that implements the footer for
     *              the route. For example: \Modules\Humpchies\Default. If this
     *              property is not set, the value of 'global_location' is used instead
     *
     *              'action_mode': whether the action should be rendered based on
     *              device capabilities, language, language + device, rendered
     *              without any of the fore mentioned conditions; or not rendered
     *              at all. If this property is not set, the value of 'global_mode'
     *              is used instead
     *
     *              'footer_location': the namespace that implements the footer for
     *              the route. For example: \Modules\Humpchies\Default. If this
     *              property is not set, the value of 'global_location' is used instead
     *
     *              'footer_mode': whether the footer should be rendered based on
     *              device capabilities, language, language + device, rendered
     *              without any of the fore mentioned conditions; or not rendered
     *              at all. If this property is not set, the value of 'global_mode'
     *              is used instead
     *
     *              'global_location': the default namespace that implements the header,
     *              footer and action. For example: \Modules\Humpchies\Default. This
     *              property is mandatory, unless 'global_mode' is set to HIDDEN_MODE
     *              or MODELESS
     *
     *              'global_mode': This property defines the way the header, the footer
     *              and the action views will be rendered or not. At the time of this
     *              writing, these modes can be:
     *
     *                  DEVICE: instructs the current class to try and use the file
     *                  extension applicable to the device behind the current HTTP
     *                  request (Provided the extension has been declared within
     *                  'supported_extensions'). This is the default rendering mode.
     *                  If the device extension is not found in 'supported_extensions',
     *                  then the very last extension in 'supported_extensions' is used
     *
     *                  LOCALIZED: instructs the current class to try and use the
     *                  locale associated with the current HTTP request
     *
     *                  LOCALIZED_DEVICE: DEVICE + LOCALIZED
     *
     *                  HIDDEN: instructs the current class that the current route
     *                  should not render any header, footer or action views
     *
     *                  MODELESS: instructs the current class to use just '.tpl'
     *
     * At this point we declare the above mentioned modes:
     */
    const DEVICE_MODE                           = 'Core.View.DeviceMode';
    const LOCALIZED_MODE                        = 'Core.View.LocalizedMode';
    const LOCALIZED_DEVICE_MODE                 = 'Core.View.LocalizedDeviceMode';
    const HIDDEN_MODE                           = 'Core.View.HiddenMode';
    const MODELESS                              = 'Core.View.ModelessMode';

    /**
    * Disable class instantiation
    */
    private function __construct()
    {}

    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}

    /**
     * This function detects the required extension based on device detection. It then compares
     * it to the list of supported extensions. If the required extension is supported, it is
     * returned. Otherwise, the very first supported extension is returned.
     *
     * @return string The applicable TPL extension for the device behind the current HTTP request
     */
    protected static function getCurrentDeviceExtension()
    {
        // If the extension has already been retrieved, simply return it
        if(self::$device_extension !== NULL)
        {
            return self::$device_extension;
        }

        // Retrive the required extension for the device behind the current HTTP request
        switch(($prefered_interface = \Core\Utils\Device::getPreferredInterface()))
        {
            // Process valid required extensions
            case \Core\Utils\Device::INTERFACE_PC:
            case \Core\Utils\Device::INTERFACE_TOUCH:
            case \Core\Utils\Device::INTERFACE_RICH:
            case \Core\Utils\Device::INTERFACE_LEAN:

                // Grab the layout properties as defined in the routes
                $layout_settings = @\Core\Router::getParsedURL()->layout_settings;

                // Bail out on the failure to retrieve any 'supported_extensions'
                if(@$layout_settings['supported_extensions'] === NULL || is_array($layout_settings['supported_extensions']) === FALSE)
                    exit('[ x ] No supported extensions have been defined');

                // If the required extension is not supported, use the very last supported extension
                if(in_array($prefered_interface, $layout_settings['supported_extensions']) === FALSE)
                {
                    $prefered_interface = array_pop($layout_settings['supported_extensions']);
                }

                // Build a TPL file extension and return it
                return (self::$device_extension = ".$prefered_interface.tpl");
                break;

            // Bail out on any invalid required extensions
            default:
                exit('[ x ] Invalid prefered device interface');     
        }
    }

    /**
     * This function parses the namespace based name of a view into an absolute path on disk
     * without file extension.
     *
     * @param $view_name string The namespace based name of the view
     * @return string The absolute path pf the view on disk without file extension
     */
    protected static function getViewPath($view_name)
    {
        // If the view path has already been retrieved, simply return it
        if(@self::$view_paths[$view_name] !== NULL)
            return self::$view_paths[$view_name];

        // Split the view name in two: path and view name (trim any back slashes and split at the double colon)
        list($path, $view) = explode('::', trim($view_name, '\\'));

        // Break the view path into its components at each back slash
        $path = explode('\\', $path);

        // The very first element in the newly created path array
        $namespace = strtolower(array_shift($path));

        // Build the possible view paths based on namespace.
        switch($namespace)
        {
            case 'application':
                $path = \Core\Router::getApplicationPath()
                        .
                        'views/'
                        .
                        ($view === 'header' || $view === 'footer' ? 'layouts/' : '')
                        .
                        strtolower((($path = implode('/', $path)) === '' ? '' : $path . '/' ));
                break;
            case 'modules':
                $path = \Core\Router::getModulesPath()
                        .
                        strtolower(array_shift($path)
                        .
                        '/views/'
                        .
                        ($view === 'header' ||$view === 'header_v2' || $view === 'footer' ? 'layouts/' : '')
                        .
                        (($path = implode('/', $path)) === '' ? '' : $path . '/' ));
                break;
            case 'plugins':
                $path = \Core\Router::getPluginsPath()
                        .
                        \Core\Router::camelCaseToUnderscore(array_shift($path))
                        .
                        '/'
                        .
                        strtolower((($path = implode('/', $path)) === '' ? '' : $path . '/'))
                        .
                        'views/';
                break;
            case 'core':
                $path = \Core\Router::getCorePath()
                        .
                        'views/'
                        .
                        strtolower((($path = implode('/', $path)) === '' ? '' : $path . '/' ));
                break;
            default:
                exit("[ x ] Invalid View Namespace: $view_name");
        }

        // Build the path to the view and cache it to boost any future requests
        return (self::$view_paths[$view_name] = ($path . \Core\Router::camelCaseToUnderscore($view)));                  
    }

    /**
     * This function retrieves the absolute path of a view on disk, including its file extension.
     *
     * @param $view_name string The namespace based name of the view
     * @param $mode string The view mode, this defines the file extension
     * @return null|string The fully qualified path to the view file. NULL when using HIDDEN mode
     */
    public static function getFullyQualifiedViewPath($view_name, $mode = self::DEVICE_MODE)
    {
        switch($mode)
        {
            case self::DEVICE_MODE:
                return self::getViewPath($view_name) . self::getCurrentDeviceExtension();
                break;
            case self::LOCALIZED_MODE:
                return self::getViewPath($view_name) . '.' . \Core\Utils\Session::getLocale() . '.tpl';
                break;
            case self::LOCALIZED_DEVICE_MODE:
                return self::getViewPath($view_name) . '.' . \Core\Utils\Session::getLocale() . self::getCurrentDeviceExtension();
                break;
            case self::HIDDEN_MODE:
                return NULL;
                break;
            case self::MODELESS:
                return self::getViewPath($view_name) . '.tpl';
                break;
            default:
                exit('[ x ] Invalid mode for view\'s fully qualified path');         
        }
    }

    /**
     * This function retrieves the fully qualified path on disk for a layout element.
     * Namely, these would be: Header, Footer or an Action
     *
     * @param $component string The component type: Header, Footer or Action
     * @return null|string The fully qualified absolute path on disk of the layout element
     */
    public static function getLayoutElementFullyQualifiedPath($component)
    {
        // Validate layout elements
        if(!in_array($component, array(self::LAYOUT_COMPONENT_ACTION, self::LAYOUT_COMPONENT_HEADER, self::LAYOUT_COMPONENT_FOOTER)))
        {
            exit("[ x ] Invalid component for layout element's fully qualified path $component");
        }

        // Get a hold of the layout properties as defined in the routes of the site
        $layout_settings = @\Core\Router::getParsedURL()->layout_settings;

        // Start building the array keys we'll need to retrieve custom layout values
        $component_location_id = '_location';
        $component_mode_id = '_mode';

        // Complete building the keys to retrieve custom layout values for each supported element
        if($component === self::LAYOUT_COMPONENT_HEADER)
        {
            $component_location_id = "header$component_location_id";
            $component_mode_id = "header$component_mode_id";
        }
        elseif($component === self::LAYOUT_COMPONENT_FOOTER)
        {
            $component_location_id = "footer$component_location_id";
            $component_mode_id = "footer$component_mode_id";
        }
        else
        {
            $component_location_id = "action$component_location_id";
            $component_mode_id = "action$component_mode_id";
        }

        /**
         * Define the namespace based name of the view. Two options exist to retrieve
         * this value:
         *
         *  1.- It wasn't declared in the routes, in which case we simply match it
         *      to the controller/action
         *  2.- It was declared as a custom value in the routes
         * */

        $view_name = NULL;

        // Handle undeclared namespaces
        if(@$layout_settings[$component_location_id] === NULL)
        {
            // If a namespace wasn't declared for the action, simply associate it to the current route
            if($component === self::LAYOUT_COMPONENT_ACTION)
            {
                $view_name = str_replace('Controller::', '::', \Core\Router::getParsedURL()->route);

            // If a namestape wasn't declared for the header or footer, use the 'global_location'
            }
            else
            {
                // Handle the lack of 'global_location'
                if(@$layout_settings['global_location'] === NULL)
                {
                    exit('[ x ] No global location has defined for this route');
                }

                // Build the view name using the 'global_location' + the component type
                $view_name =    $layout_settings['global_location']
                                .
                                '::'
                                .
                                ($component === self::LAYOUT_COMPONENT_HEADER ? 'header' : 'footer');
            }

        // Handle declared namespaces: simply grab it
        }
        else
        {
            $view_name = $layout_settings[$component_location_id];
        }

        /**
         * Define the mode (file extension) we wish for the view. Two options exist to retrieve
         * this value:
         *
         *  1.- It wasn't declared in the routes, in which case we simply match the global mode
         *      defined in the routes
         *  2.- It was declared as a custom value in the routes
         */

        $view_mode = NULL;

        // Handle undeclared component modes: use the 'global_mode'
        if(@$layout_settings[$component_mode_id] === NULL)
        {
            // Handle the lack of 'global_mode'
            if(@$layout_settings['global_mode'] === NULL)
            {
                exit('[ x ] No global layout mode has defined for this route');
            }

            // Use the 'global_mode' as the component's mode
            $view_mode = $layout_settings['global_mode'];

        // Handle declared component modes: simply grab it
        }
        else
        {
            $view_mode = $layout_settings[$component_mode_id];
        }

        // Build and return the path
        return self::getFullyQualifiedViewPath($view_name, $view_mode);

    }
}