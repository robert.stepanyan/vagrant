<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class is the abstract representation of a controller.
 * i.e the business rules to transform and load data.
 */

Namespace Core;

abstract class Controller
{  
    // Protected Members: Singletons
    // =============================
    protected static $title                     = NULL;             // The title of the page
    protected static $keywords                  = NULL;             // The keywords of the page          
    protected static $description               = NULL;             // The description of the page
    protected static $in_page_text              = NULL;             // The in page text of the page
    protected static $extra_head                = NULL;             // Extra HEAD content
    protected static $plugins                   = NULL;             // The plugins of the page
    protected static $data                      = NULL;             // The data of the page
    protected static $extra_body                = NULL;             // Extra BODY tag content
    protected static $headerClass               = NULL;             // Extra header class
    static $hideAds = false;

    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}
    
    /**
    * Prevent the class from being instantiated
    * 
    * @returns Nothing
    */
    private function __construct()
    {}
    
    /**
    * This function retieves data that has been harvested in the controller's action.
    * If 'key' parameter is passed it will retrieve a specific variable. Note that 
    * this function is present for backwards compatibility. 
    * 
    * @parameter string $key The name of a specific variable of which value we want to retrieve
    * @returns mixed Either and array containning all data gathered by the controller's action,
    * or a single value containning the value of a partciular variable. It returns NULL whenever
    * data is empty or the wanted variable is not set.
    */
    public static function getData($key = null)
    {
        /**
        * If the key is a string, if is not a string return NULL. Find out if the key is set 
        * in the array of data. If the key is set, return its value... otherwise return NULL
        */
        return ($key === NULL ? self::$data : self::$data[$key]);
    }
    
    /**
    * put your comment there...
    *
    */
    public static function getTitle()
    {                                      
        if(self::$title !== NULL)
            return self::$title;
            
        return \Core\Utils\Config::getConfigValue('\Application\Settings::title', FALSE);
    }
    
    
    /**
    * put your comment there...
    *
    */
    public static function getHeaderClass() {                                      
        if(self::$headerClass !== NULL)
            return self::$headerClass;
            
        return false;
    }
    
    public static function hideAds(){
        self::$hideAds = true;
    }
    
    
    public static function showAds(){
        return self::$hideAds;
    }
    
    public static function doTranslate($meta, $city)
    {
        $path = '/{(.*?)}/i';
        return preg_replace($path, $city, $meta); 
    }
    
    public static function dynamicTerms($lang, $key){
        $lang = substr($lang, 0, 2);
        
        $__locales = array(
            'DESCRIPTION-DEFAULT' => array(
                'fr' => 'Cherchez et publiez des annonces &eacute;rotiques gratuitement partout au {Qu&eacute;bec}. Publiez votre annonce &eacute;rotique gratuite en quelques secondes. Trouvez de la compagnie sensuelle, des escortes, et massages &eacute;rotiques.',
                'en' => 'FREE erotic ads in {Quebec}. Post your FREE erotic ad in seconds. Find escorts and erotic massages all over {Quebec}, quick and easy!',
            )
        );
        
        return $__locales[$key][$lang];
    }

    /**
    * put your comment there...
    *
    * @param mixed $title
    */
    public static function setTitle($title)
    {
        self::$title = $title;
    }
    
    public static function setHeaderClass($class) {
        self::$headerClass = $class;
    }
    
    
    
    /**
    * put your comment there...
    *
    * @param mixed $title
    */
    public static function appendTitle($title)
    {
        self::$title .= $title;        
    }
    
    /**
    * put your comment there...
    *
    */
    public static function getKeywords()
    {
        if(self::$keywords !== NULL)
            return self::$keywords;
            
        return \Core\Utils\Config::getConfigValue('\Application\Settings::keywords', FALSE);
    }
    
    /**
    * put your comment there...
    *
    * @param mixed $keywords
    */
    public static function setKeywords($keywords)
    {
        self::$keywords = $keywords;
    }
    
    /**
    * put your comment there...
    *
    */
    public static function getDescription()
    {
        if(self::$description !== NULL)
            return self::$description;
            
        return \Core\Utils\Config::getConfigValue('\Application\Settings::description', FALSE);
    }
    
    /**
    * put your comment there...
    *
    * @param mixed $description
    */
    public static function setDescription($description)
    {
        self::$description = $description;
    }
    
    /**
    * put your comment there...
    *
    */
    public static function getInPageText()
    {
        if(self::$in_page_text !== NULL)
            return self::$in_page_text;
            
        return \Core\Utils\Config::getConfigValue('\Application\Settings::in_page_text', FALSE);
    }

    public static function setInPageText($text)
    {
        self::$in_page_text = $text;
    }
    
    /**
    * Retrives code to be added right before the closing tag of the HEAD tag in the HTML
    * 
    * @returns A string containning the code to be added right before the closing tag of 
    * the HEAD tag in the HTML 
    */
    public static function getExtraHead()
    {
        // Simply return the code 
        return self::$extra_head;
    }

    /**
    * Adds code right before the closing tag of the HEAD tag in the HTML  
    * 
    * @parameter string $extra_head The code to be added
    * @parameter bool $concatenate TRUE if we wish to append the code to any existing extra code.
    * FALSE [default]is we wish to overwrite the contants eny existing code.
    * 
    * @returns A string containning the new value of extra head after appending or overwritting  
    */
    public static function setExtraHead($extra_head, $concatenate = TRUE)
    {
        /*
        If we wish to append, add the code to the end of the exisitng value, otherwise simply replace the 
        exisiting value,
        */
        return ($concatenate === TRUE ? self::$extra_head .= $extra_head : self::$extra_head = $extra_head);
    }

    /**
    * Retrives code to be added within the body tag
    *
    * @returns A string containning the code to be added to the body tag
    */
    public static function getExtraBody()
    {
        // Simply return the code
        return self::$extra_body;
    }

    /**
    * Adds code to the body tag
    *
    * @parameter string $extra_body The code to be added
    * @parameter bool $concatenate TRUE if we wish to append the code to any existing extra code.
    * FALSE [default]is we wish to overwrite the contants eny existing code.
    *
    * @returns A string containning the new value of extra body after appending or overwritting
    */
    public static function setExtraBody($extra_body, $concatenate = TRUE)
    {
        /*
        If we wish to append, add the code to the end of the exisitng value, otherwise simply replace the
        exisiting value,
        */
        return ($concatenate === TRUE ? self::$extra_body .= $extra_body : self::$extra_body = $extra_body);
    }

    /**
    * Adds as plugin to the array of available plugins to the current Controller
    * 
    * @parameter string $name The arbitrary name chosen by the programmer to identify the plugin
    * @parameter plugin $plugin The plugin object to be added
    */
    public static function pushPlugin($name, $plugin)
    {
        // Simply append the plugin at the end of the array of plugins
        self::$plugins[$name] = $plugin;
    }
    
    /**
    * Attempts to call the 'render' function of the designated plugin.
    * If the name is invalid nothing is rendered.
    * 
    * @name The name of the plug we wish to render 
    * @returns TRUE if the plug-in is rendered... false otherwise
    */
    public static function renderPlugin($name, $view_name = NULL, $mode = \Core\View::DEVICE_MODE)
    {
        // Try to render the requested Plugin
        if(isset(self::$plugins[$name])){
            self::$plugins[$name]->render($view_name, $mode);
            return TRUE;
        }
        
        return FALSE;
    }
    
    /**
    * Redirects the user to a particular URL with a particular HTTP code and kills the current request.
    * 
    * @parameter string $url The URL where we wish to send the user to
    * @parameter sting $code_and_msg A valid HTTP/1.1 redirection code. More information about 
    * HTTP codes can be found here: http://en.wikipedia.org/wiki/List_of_HTTP_status_codes. 
    * The default redirection code is 301 which indicates that the redirection is permanent.
    * 
    * @returns Nothing
    */
    public static function redirect($url, $code_and_msg = '301 Moved Permanently')
    {
        header("HTTP/1.1 $code_and_msg");
        header("Location: $url"); 
        exit();
    }

    /**
    * Gets a flash value
    *
    * @parameter string The key in the flash array
    *
    * @returns string The value in the flash array
    */
    public static function getFlash($key)
    {
        return (isset($_SESSION['flash_values'][$key]) ? $_SESSION['flash_values'][$key] : NULL);
    }
    
    /**
    * Gets a flash value
    *
    * @parameter string The key in the flash array
    *
    * @returns string The value in the flash array
    */ 
    public static function setFlash($key, $value)
    {
        $_SESSION['flash_values'][$key] = $value;
    }

    /**
    * Clears the flash varaibles
    *
    * @returns nothing
    */
    public static function clearFlash($key = NULL)
    {
        if($key !== NULL && isset($_SESSION['flash_values'][$key]))
            unset($_SESSION['flash_values'][$key]);
        elseif(isset($_SESSION['flash_values']))
            unset($_SESSION['flash_values']);
    }
    
    /**
    * Return the messages that will be printed out in the message box (most likely in the header)
    *
    * @parameter $type string containing the type of the message you want. If left out, returns all 
    * message, where each type is an array of messages
    *
    * @returns array of messages
    */
    public static function getMessages($type = NULL)
    {
        if(isset($_SESSION['flash_values']['messages']) === FALSE)
            return FALSE;
        elseif(isset($_SESSION['flash_values']['messages'][$type]))
            return $_SESSION['flash_values']['messages'][$type];    
        
        return $_SESSION['flash_values']['messages'];
    }
    
    /**
    * Add a message that will be printed out in the message box (most likely in the header)
    *
    * @parameter $message the string containing the message
    * @parameter $type string containing the type of the message (usually "success", "error", "warning")
    *
    * @returns nothing
    */
    public static function addMessage($message, $type = "success", $append = TRUE)
    {
        if($append === TRUE)
            $_SESSION['flash_values']['messages'][$type][] = $message;
        else
            $_SESSION['flash_values']['messages'][$type] = $message;    
    }
    
    /***
    * This function prevents the 'page' from being cached on the browser. It
    * does so by explicitly setting some browser headers.
    */
    public static function disableCache()
    {
        header('Expires: Thu, 19 Nov 1981 08:52:00 GMT', TRUE);
        header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0', TRUE);
        header('Pragma: no-cache', TRUE);
    }
}
