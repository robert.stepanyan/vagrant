<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class that provide us standarized time related
 * utilitarian functions and methods.
 */

Namespace Core\Utils;

class SendMail{
     
	public static function send($email, $subject, $body, $htmlEmail = false, $from = 'info@humpchies.com', $from_name = 'humpchies', $attachments = array())
    {
		$mail = new \Core\library\PhpMailer();
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'smtp-hmpchs.newsmanapp.com';           // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'hmpchs_comacc@tmail.humpchies.com';// SMTP username
		$mail->Password = 'HmpC%28UqnZo384#d';                // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		
		$mail->From = $from;
		$mail->FromName = $from_name;
		$mail->addAddress($email);     // Add a recipient
		//$mail->addReplyTo('info@example.com', 'Information');
		//$mail->addCC('cc@example.com');
		//$mail->addBCC('bcc@example.com');

		//$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
		//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
		if ($htmlEmail){
			$mail->isHTML(true); // Set email format to HTML       
		}
		
		$mail->Subject = $subject;
		$mail->Body    = $body;
		//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
		
		if(!$mail->send()) {
			//echo 'Mailer Error: ' . $mail->ErrorInfo;
			return FALSE;
		} else {
			return TRUE;
		}
		
	}
}