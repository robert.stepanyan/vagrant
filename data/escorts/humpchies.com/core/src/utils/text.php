<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class that provide us standarized time related
 * utilitarian functions and methods.
 */

Namespace Core\Utils;

class Text{

	/**
   	* Disable object copying
    	*/
    private function __clone()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object serialization
    */
    private function __sleep()
    {}
    
    /**
    * Prevent the class from being instantiated
    * 
    * @returns Nothing
    */
    private function __construct()
    {}
    
    /***
    * put your comment there...
    * 
    * @param mixed $locale_file_path
    * @param mixed $prefix_key
    * @param mixed $filemtime
    * @return bool
    */
    private static function loadLocaleIntoMemcached($locale_file_path, $prefix_key, $filemtime)
    {
        if($filemtime === FALSE)
            return FALSE;
        
        if(\Core\Cache::getDataAccessLock("loadLocaleIntoMemcached.$prefix_key") === FALSE)
            return FALSE;
           
        require_once($locale_file_path);

        $result = \Core\Cache::saveMulti($locale, 900, $prefix_key);

        \Core\Cache::deleteDataAccessLock("loadLocaleIntoMemcached.$prefix_key");
        
        return $result; 
    } 
    
    /***
    * put your comment there...
    * 
    * @param mixed $buffer
    * @param mixed $application_id
    * @param mixed $locale
    * @param mixed $locale_module
    */
    public static function translateBufferTokens(&$buffer, $application_id, $locale = 'en-us', $locale_module = NULL)
    {
        preg_match_all('/\[([A-Z0-9]+((\-[A-Z0-9]+)+)?){3}\]/', $buffer, $tokens);
        $tokens = array_unique($tokens[0]);
        
        if(empty($tokens))
            return FALSE;

        $tokens[] = 'VERSION-DATE';
        
        $translations = \Core\Cache::getMulti(  $tokens, 
                                                \Memcached::GET_PRESERVE_ORDER, 
                                                "$application_id.$locale_module.$locale");

        // might be an issue with multissrver deployment 
        if(@$translations['VERSION-DATE'] >= ($filemtime = filemtime(($locale_file_path = ($locale_module === NULL ? \Core\Router::getApplicationPath() : \Core\Router::getModulesPath() . "$locale_module/") . "assets/locales/$locale.php"))))
        {
            $buffer = str_ireplace($tokens, $translations, $buffer);
            return TRUE;    
        }
        elseif(self::loadLocaleIntoMemcached($locale_file_path, "$application_id.$locale_module.$locale", $filemtime) === FALSE)
            return FALSE; 
        
        $translations = \Core\Cache::getMulti(  $tokens, 
                                                \Memcached::GET_PRESERVE_ORDER, 
                                                "$application_id.$locale_module.$locale");
                                                
        $buffer = str_ireplace($tokens, $translations, $buffer);
            return TRUE;
    }


    public static function translateTextWithVars($text, $replace_arr)
    {
        preg_match_all('/\[([A-Z0-9]+((\-[A-Z0-9]+)+)?){3}\]/', $text, $tokens);
        $tokens = array_unique($tokens[0]);
        
        if(empty($tokens))
            return FALSE;

        $tokens[] = 'VERSION-DATE';

        $application_id = \Core\Utils\Config::getConfigValue('\Application\Settings::hostname');
        $locale_module = \Core\Utils\Config::getConfigValue('\Application\Settings::locales_module',FALSE);
        $locale = \Core\Utils\Session::getLocale();
        
        $translations = \Core\Cache::getMulti(  $tokens,  \Memcached::GET_PRESERVE_ORDER,  "$application_id.$locale_module.$locale");
        
        

        // might be an issue with multissrver deployment 
        if(@$translations['VERSION-DATE'] >= ($filemtime = filemtime(($locale_file_path = ($locale_module === NULL ? \Core\Router::getApplicationPath() : \Core\Router::getModulesPath() . "$locale_module/") . "assets/locales/$locale.php"))))
        {
            $text = str_ireplace($tokens, $translations, $text);
            $text = str_replace(array_keys($replace_arr), array_values($replace_arr), $text);
            return $text;    
        }
        elseif(self::loadLocaleIntoMemcached($locale_file_path, "$application_id.$locale_module.$locale", $filemtime) === FALSE)
            return FALSE; 
        
        $translations = \Core\Cache::getMulti(  $tokens, 
                                                \Memcached::GET_PRESERVE_ORDER, 
                                                "$application_id.$locale_module.$locale");
                                                
         $text = str_ireplace($tokens, $translations, $text);
        $text = str_replace(array_keys($replace_arr), array_values($replace_arr), $text);
        return $text;  
    }

    /***
    * This function is intended to be used with urlFor(). It replaces any non alphanumerical character
    * in parameter 1 ($string) by the character found in parameter 2 ($replacement_character).
    *
    * @param string $string The string that contains non alphanumeric characters
    * @param char $replacement_character The character to be used as a substitute to any non alphanumeric characters
    * @return string parameter 1 ($string) with any alphanumeric characters replaced by parameter 2 ($replacement_character)
    */
    public static function stringToUrl($string, $replacement_character = '-')
    {
        return strtolower(preg_replace("/$replacement_character+/", $replacement_character, preg_replace('/[^a-zA-Z0-9]/', $replacement_character, self::translateDiacriaticsAndLigatures($string))));
    }
    
    /**
    * This function replaces diacratics and ligatures from a string.
    * 
    * @param string $string The string we wish to rid of diacratics and ligatures
    * @returns a string without diacratics and/or ligatures 
    */
    public static function translateDiacriaticsAndLigatures($string)
    {
        $french_diacriatics_and_ligatures = array(  '�' => 'A', '�' => 'a', '�' => 'A', '�' => 'a', '�' => 'a', '�' => 'a',
                                                    '�' => 'C', '�' => 'c',
                                                    '�' => 'E', '�' => 'e', '�' => 'E', '�' => 'e', '�' => 'E', '�' => 'e', '�' => 'E', '�' => 'e',
                                                    '�' => 'I', '�' => 'i', '�' => 'I', '�' => 'i',
                                                    '�' => 'O', '�' => 'o', '�' => 'O', '�' => 'o',
                                                    '�' => 'U', '�' => 'u', '�' => 'U', '�' => 'u', '�' => 'U', '�' => 'u',
                                                    '�' => 'y');


        return str_replace(array_keys($french_diacriatics_and_ligatures), array_values($french_diacriatics_and_ligatures), $string);
    }
    
    /***
    * put your comment there...
    * 
    * @param mixed $phone_number
    */
    public static function formatPhoneNumberForGUI($phone_number)
    {
        return preg_replace('/\d{3}/', '$0.', str_replace('.', null, $phone_number), 2);   
    }
    
    /***
    * put your comment there...
    * 
    * @param mixed $string
    */
    public static function extractPhoneNumberFromString($string)
    {
        return (preg_match("/(?:1[- .]?)?\(?\d{3}\)?[- .]?\d{3}[- .]?\d{4}/",$string, $matches) === 1 ? ltrim(trim(preg_replace('/[^0-9]/', '', $matches[0])), '1') : NULL);
    }
}