<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to alter navigation and/or the urls associated with it.
 */

Namespace Core\Utils;

class Navigation{
    
    /**
    * This function returns a valid page number to be used in pagination rutines. It defaults
    * to page number 1 in case of invalid page.
    */
    public static function getPageNumber(&$get_parameters)
    {
        return ((isset($get_parameters["page"]) && is_numeric($get_parameters["page"]) && $get_parameters["page"] > 0) ? max(floor($get_parameters["page"]), 1) : 1);
    }
    
    /***
    * Creates a URL relative to the root of the current domain. It is capable of appending GET params to the URL
    * as a path or as regular params.
    *
    * @param string $controller The name of the controller
    * @param string $action The name of the action
    * @param array $extra GET parameters
    * @param bool $asPath wether or not we should show the get parameters as a PATH or a QUERY STRING
    * 
    * @return string
    */
    public static function urlFor($controller_or_keyword = NULL, $action = NULL, $extra = NULL, $as_path = TRUE, $absolute = FALSE)
    {
        if($controller_or_keyword === '/')
            $controller_or_keyword = NULL;
            
        switch((((int)!($controller_or_keyword === NULL)) + (((int)!($action === NULL)) * 2)))
        {
            case 1:
                $path = "/$controller_or_keyword";
                break;
            case 3:
                $path = "/$controller_or_keyword/$action";
                break;
            default:
                $path = '/';
                break;
        }
        
        if($extra !== NULL)
        {
            foreach($extra as $key => $value)
                $extra[] = $key . ($as_path === TRUE ? '/' . \Core\Utils\Text::stringToUrl($value) : '=' . urlencode($value));
                
            $extra = ($as_path === TRUE ? '/' : '?') . implode(($as_path === TRUE ? '/' : '&'), array_slice($extra, (count($extra)/2)));
        }

        return  ($absolute === TRUE ? \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname') : '') 
                .
                \Core\Utils\Session::getLocale(TRUE, TRUE)
                . 
                $path
                .
                $extra;
    }
}