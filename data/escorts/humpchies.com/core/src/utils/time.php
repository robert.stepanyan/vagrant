<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class that provide us standarized time related
 * utilitarian functions and methods.
 */

Namespace Core\Utils;

class Time{
     
	const DEFAULT_COMMON_TIMESTAMP_CHUNK		= 600;
    const DATE_FORMAT_ISO8601                   = 'Ymd\THis\Z';
    const DATE_FORMAT_RFC1123                   = 'D, d M Y H:i:s \G\M\T';
    const DATE_FORMAT_RFC2822                   = \DateTime::RFC2822;
    const DATE_FORMAT_SHORT                     = 'Ymd';

	/**
   	* Disable object copying
    	*/
    private function __clone()
    {}

    /**
    * Disable object deserialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object serialization
    */
    private function __sleep()
    {}
    
    /**
    * Prevent the class from being instantiated
    * 
    * @returns Nothing
    */
    private function __construct()
    {} 

	/***
        * This public static function is used to retrive a time stamp that falls at the beginning of a particular chunk.
        * The concept of a chunk comes from partitionning the 24 hrs in a day into a max of twelve parts.
        * Each part is a chunk and the chucnk size is the number of hours in it. The snallest chunk size is
        * 1, meanning 1 hr per chunk which yields 24 chunks, which implies that the public static function will return
        * the time stamp of the current hour when it gets called. The largest chunck size is 12, meanning
        * two chunks.
        *
        * This public static function is useful for database calls where a time stamp is used to retrieve records based
        * on the current time. Use it instead of time() with cached queries where the query itself is used
        * to produce the cache key.
        *
        * @param mixed $chunk_size the number of hours in each chunk, max. 12, min. 1, anything in between
        * must be a multiple of 2.
        *
        * @return int
        */
    public static function getCommonTimeStamp($chunk_size = self::DEFAULT_COMMON_TIMESTAMP_CHUNK)
    {
        return (floor((@date('U') / $chunk_size)) * $chunk_size);
    }
}