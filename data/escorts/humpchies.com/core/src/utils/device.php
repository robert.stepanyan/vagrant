<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file encapsulates utilitarian functions related to
 * device detection.
 */

Namespace Core\Utils;

final Class Device{

    // MEMEBERS
    // ======================
    private static $user_agent                          = NULL;                     // The user agent of the current request
    private static $device_ip                           = NULL;                     // The device IP of the cuttent request
    private static $prefered_interface                  = NULL;
    private static $is_apple                            = NULL;                     // Whether a device is apple or not
    private static $is_ipad                             = NULL;                     // Whether a device is an iPad or not
    private static $is_android                          = NULL;                     // Whether a device is an Android or not
    private static $server_vars_and_headers             = NULL;                     // A lower cased merged version of the Server's variables and headers for the current request
    private static $is_mobile                           = NULL;
    private static $is_webos                            = NULL;
    private static $is_blackberry                       = NULL;
    private static $is_webkit                           = NULl;
    private static $is_gecko                            = NULL;
    private static $is_webkitmobile                     = NULL;
    private static $is_tablet                           = NULL;
    private static $manufacturer                        = NULL;
    private static $browser                             = NULL;
    private static $os_version                          = NULL;
    private static $is_windowsphone                     = NULL;
    private static $is_3d                               = NULL;
    private static $geo_location_info_new               = NULL;
    
    // CONSTANTS
    // =========
    const INTERFACE_RICH                                = 'rich';
    const INTERFACE_LEAN                                = 'lean';
    const INTERFACE_TOUCH                               = 'touch';
    const INTERFACE_PC                                  = 'pc';
    const ERROR_GEO_LOCATION_IS_INVALID             = 'Session.Error.GeoLocationIsInvalid';
    
    /**
     * Returns the user agnet behind the current request. Note that the search for the user agent is exhaustive
     *
     * @return string The user agent behind the current HTTP request
     */
    public static function getUserAgent()
    {
        if(self::$user_agent !== NULL)                                       
            return self::$user_agent;
        
        foreach(array('http_x_operamini_phone_ua','x-original-user-agent','user-agent','x-device-user-agent') as $key){
            if(isset(self::$server_vars_and_headers[$key]))
                    return (self::$user_agent = ((self::$server_vars_and_headers[$key] === $_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : self::$server_vars_and_headers[$key]));
        }
        
        // If averything fails >-( then return the gay user agent found in the server variables... I mean this browser is just FUCKED!
        return (self::$user_agent = $_SERVER['HTTP_USER_AGENT']);
    }
    
    /***
    * Cleans the user agent string by removing any language codes and non-necessary ID codes from it
    *
    * @param mixed $in_ua
    */
    public static function cleanUserAgent($user_agent)
    {
        return preg_replace(array("/\s+[a-z]{2,3}(-[a-z]{2,3})?\)/i", "/\s+[a-z]{2,3}(-[a-z]{2,3})?;/i", "/\[TF[0-9]{32,37}\]/i"), array(')','',''), $user_agent);
    }
    
    /**
    * put your comment there...
    * 
    */
    public static function getUserAgentComponents()
    {
        return preg_split('/\s/i', self::getUserAgent());
    }
    
    /***
    * This function returns a lower case merged version of the server variables and headers for the current request.
    *
    * @return array a merged, lowered case array that combines $_SERVER && the results of getallheaders()
    */
    public static function getServerVarsAndHeaders($desired_value = NULL)
    {
        // If the serve's variables and headers have already been asked for previously in the same request, then It must've been saved... so fetch it
        if(self::$server_vars_and_headers)
            return ($desired_value === NULL ? self::$server_vars_and_headers : @self::$server_vars_and_headers[$desired_value]);

        self::$server_vars_and_headers = array_change_key_case((isset($_SERVER['SERVER_SOFTWARE']) && stripos($_SERVER['SERVER_SOFTWARE'],'nginx') !== false ? $_SERVER : array_merge($_SERVER, getallheaders())));
        
        // If they have not previously been requested then do the merge, lower case them, save them and return them
        return ($desired_value === NULL ? self::$server_vars_and_headers : @self::$server_vars_and_headers[$desired_value]);
    }
    
    /***
    * This function returns the device name of the current request. If the device is not
    * recognized as mobile it returns 'pc'.
    *
    * @return string The type of cliennt behind the current request... defaults to PC when all tests have failed
    */
    public static function getManufacturer()
    {
        if(!is_null(self::$manufacturer))
             return self::$manufacturer;

        if(self::isApple() || preg_match('/\bApple\b|Mac OS/i', self::getUserAgent()))
            return (self::$manufacturer = 'APPLE');
        elseif(preg_match('/ gt-|sgh|sch|samsung/i', self::getUserAgent()))
            return (self::$manufacturer = 'SAMSUNG');
        elseif(preg_match("/\b(htc|motorola|huawei|acer|benq|siemens|sony|pantech|panasonic|sharp|hp|nokia|sanyo|inq|zte|dell|nec|casio|hitachi|asus|i-mate|garmin)\b/i", self::getUserAgent(), $matches))
            return (self::$manufacturer = strtoupper($matches[1]));

        return (self::$manufacturer = 'UNKOWN');
    }

    /**
    * put your comment there...
    * 
    */
    public static function getBrowserName()
    {
        if(!is_null(self::$browser))
             return self::$browser;
        elseif(self::isAndroid() && stripos(self::getUserAgent(), 'chrome') !== false)
            return (self::$browser = 'CHROME Android');
        elseif(stripos(self::getUserAgent(), 'firefox') !== false)
            return (self::$browser = 'FIREFOX');
        elseif(preg_match('/\b(firefox|internet explorer|chrome|opera mini|opera mobile|skyfire|ucbrowser)\b/i', self::getUserAgent(), $matches))
            return (self::$browser = strtoupper($matches[1]));
        elseif(preg_match('/\b(kindle|silk)\b/',self::getUserAgent()))
            return (self::$browser = 'Kindle Browser');
        elseif(preg_match('/BlackBerry.*(?!Webkit)/i',self::getUserAgent()))
            return (self::$browser = 'BlackBerry Legacy Browser');
        elseif(preg_match('/BlackBerry\b.*Webkit\b/i',self::getUserAgent()))
            return (self::$browser = 'BlackBerry Webkit Browser');
        elseif(stripos(self::getUserAgent(), 'safari') !== false)
            return (self::$browser = 'SAFARI');
        elseif(stripos(self::getUserAgent(), 'MSIE') !== false && self::isWindowsPhone())
            return (self::$browser = 'Internet Explorer Mobile');
            
        return (self::$browser = 'UNKOWN');
    }

    /**
    * put your comment there...
    * 
    */
    public static function getPreferredVideoResolution()
    {
        if(self::isIpad() || self::isTablet())
            return '368p';
        elseif(self::isAndroid() || (self::isApple() && !self::isIpad()) || (self::isBlackBerry() && self::getOsVersion() >= 6) || (self::isWindowsPhone() && self::getOsVersion() >= 7) || self::isWebOS())
            return '272p';
        else
            return '144p';
    }

    /**
    * put your comment there...
    * 
    * @param mixed $override_ua
    * @return mixed
    */
    public static function getPreferredInterface()
    {
        if(self::$prefered_interface !== NULL)
            return self::$prefered_interface;
            
        if(!self::isMobile() || isset($_SESSION['show_full_version']) && $_SESSION['show_full_version'] === FALSE)
            return (self::$prefered_interface = self::INTERFACE_PC);
        elseif((isset($_SESSION['show_full_version']) && $_SESSION['show_full_version'] === TRUE) || (self::isTablet() && !self::isWebkit()) || (self::isWindowsPhone() && self::getOsVersion() >= 7))
            return (self::$prefered_interface = self::INTERFACE_RICH);     
        elseif(self::isApple() || self::isAndroid() || (self::isTablet() && self::isWebkit()) || (self::isBlackBerry() && self::isWebkit()) || self::isWebOS())
            return (self::$prefered_interface = self::INTERFACE_TOUCH);

        return (self::$prefered_interface = self::INTERFACE_LEAN);
    }

    /**
    * This function gets the IP of the device behind the current HTTP request.
    *
    * @return string The IP of the device making the current request
    */
    public static function getDeviceIp()
    {
        // If the device IP have already been asked for previously in the same request, then it must've been saved... so fetch it
        if(self::$device_ip)
            return self::$device_ip;

        // Merge the Server variables and the haeders
        if(!self::$server_vars_and_headers)
            self::getServerVarsAndHeaders();

        // Seee if any of the potential identifiers for an IP exists within the server variables or headers
        foreach(array('http_x_forwarded_for','x_forwarded_for', 'x-up-forwarded-for') as $key)
        {
            // If any of the identifiers exists within the server variables or headers
            if(array_key_exists($key, self::$server_vars_and_headers))
            {
                // Look for anything that looks like an IP... if multiple IPs are found
                if(preg_match_all("/(\d+\.\d+\.\d+\.\d+)/", self::$server_vars_and_headers[$key], $matches) > 1)
                {
                    // Examin all ips....
                    foreach($matches[1] as $ip){
                        // The first one to be found public
                        if(!self::isPrivateIp($ip)){
                            self::$device_ip = $ip;        // Gets stored as the device IP
                            break(2);                    // and we break out of the two loops
                        }
                    }
                } // else do the same thing as above but for a single IP
                elseif(preg_match("/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/", self::$server_vars_and_headers[$key], $matches)){
                    if(!self::isPrivateIp($matches[0])){
                        self::$device_ip = $matches[0];
                        break;
                    }
                }
            }
        }

        // IF: an non-private IP was found, and the IP has a length greater than 7 characters, return it...else return the lame server value
        return ((self::$device_ip !== FALSE && strlen(self::$device_ip) >= 7)? self::$device_ip : (self::$device_ip = $_SERVER['REMOTE_ADDR']));
    }
    public static function getDeviceIp_new()
    {
        // If the device IP have already been asked for previously in the same request, then it must've been saved... so fetch it
       // if(self::$device_ip)
//            return self::$device_ip;
//            
//        
        if(filter_var($_SERVER["HTTP_X_FORWARDED_FOR"], FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE)){
            self::$device_ip = $_SERVER['HTTP_X_FORWARDED_FOR']; 
            return self::$device_ip;
        }
        if(filter_var($_SERVER['HTTP_INCAP_CLIENT_IP'], FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE)){
            self::$device_ip = $_SERVER['HTTP_INCAP_CLIENT_IP']; 
            return self::$device_ip;
        } 
         
        if(filter_var($_SERVER["REMOTE_ADDR"], FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE)){
            self::$device_ip = $_SERVER['REMOTE_ADDR']; 
            return self::$device_ip;
        }    
    }
     public static function getDeviceIpClass()
    {   
        $ip = self::getDeviceIp_new();
        $ip_class = preg_split("`[.|:]`is", $ip);
        
        if( filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)){
            $glue = ".";
            $ip_class = array_slice($ip_class, 0, 3);    
        }else{
            $glue = ":";
            $ip_class = array_slice($ip_class, 0, 4);  
        }
        
        return implode($glue, $ip_class);
    
    }
     public static function getDeviceClassbyIP($ip)
    {   
        //$ip = self::getDeviceIp_new();
        $ip_class = preg_split("`[.|:]`is", $ip);
        
        if( filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)){
            $glue = ".";
            $ip_class = array_slice($ip_class, 0, 3);    
        }else{
            $glue = ":";
            $ip_class = array_slice($ip_class, 0, 4);  
        }
        
        return implode($glue, $ip_class);
    
    }
    
    public static function getGeoLocation()
    {
        //$deviceIp = '142.169.78.244';     // Montreal invalid
        //$deviceIp = '107.171.187.210';    // Beloil
        //$deviceIp = '128.90.116.132';     // Mexico
        //$deviceIp = '24.114.84.237';      //Moncton
        //$deviceIp = '107.171.191.113';    // Pierrefonds

        $deviceIp = self::getDeviceIp_new(); // NULL for dev
        $cacheKey = "GEOIP_CACHED_DATANEW:$deviceIp";

        if(NULL !== self::$geo_location_info_new) {
            return self::$geo_location_info_new;
        }

        $cachedGeoIpData = \Core\Cache::get($cacheKey);

        if(FALSE !== $cachedGeoIpData && NULL === $cachedGeoIpData) {
                return (self::$geo_location_info_new = self::ERROR_GEO_LOCATION_IS_INVALID);
        } elseif(FALSE !== $cachedGeoIpData) {
            return (self::$geo_location_info_new = json_decode($cachedGeoIpData));
        }

        $file = \Core\Utils\Config::getConfigValue('\Core\Geoip::CityFile');
        $geoIpRawData = shell_exec("mmdblookup --file $file --ip $deviceIp");

        if(NULL === $geoIpRawData) {
            \Core\Cache::save($cacheKey, NULL, 60);
            return (self::$geo_location_info_new = self::ERROR_GEO_LOCATION_IS_INVALID);
        }

        $geoIpRawData = preg_replace('/<(.+)>/', ',', $geoIpRawData);
        $geoIpRawData = str_replace(array("\r", "\n", "\t"), '', $geoIpRawData);
        $geoIpRawData = preg_replace("/\s+/", '', $geoIpRawData);
        $geoIpRawData = str_replace(',}', '}', $geoIpRawData);
        $geoIpRawData = str_replace('}{', '},{', $geoIpRawData);
        $geoIpRawData = preg_replace('/}"(\w+)":{/', "},\"$1\":{", $geoIpRawData);
        $geoIpRawData = preg_replace('/}"(\w+)":\[{/', "},\"$1\":[{", $geoIpRawData);
        $geoIpDecodedData = json_decode($geoIpRawData);

        if(NULL === $geoIpDecodedData) {
            \Core\Cache::save($cacheKey, NULL, 60);
            return (self::$geo_location_info_new = self::ERROR_GEO_LOCATION_IS_INVALID);
        }

        \Core\Cache::save($cacheKey, $geoIpRawData, 3600);
        return (self::$geo_location_info_new = $geoIpDecodedData);
    }

    public static function getLocation(){
         $loc = self::getGeoLocation();
         return ['location' => ucwords( $loc->subdivisions[0]->names->en .", ". $loc->city->names->en), 'country' => strtoupper( $loc->country->iso_code)]; 
    }
    
    /**
    * put your comment there...
    * 
    */
    public static function isMobile()
    {
        // If the is_mobile value has already been set in the current request... then simply return it
        if(self::$is_mobile !== NULL)
            return self::$is_mobile;

        if( self::isAndroid() || self::isApple() || self::isWebOS() || self::isBlackBerry() || self::isWindowsPhone() || self::isWebkitMobile() || self::isTablet() 
            || 
            preg_match("/htc|palm|Samsung|Nokia|\s+SGH|\s+SCH|LG|\s+MOT|Motorolla|SonyEricsson|Sony|PSP|Kyocera|ASUS|PANTECH|Audiovox|BenQ|Fly|i-mobile|LENOVO|Haier|i-mate|Hyundai|Nintendo DSi|O2 Xda|Sagem| SIE |Vario|winwap|DoCoMo|NetFront|Vodafone|UP.Browser|konka|tianyu|ktouch|series60|kddi|sagem|\s+MIDP|\s+CLDC|SoftBank|TelecaBrowser|Teleca|Symbian|Treo|WAP|pocket|kindle|mobile|Opera Mini|Obigo|Windows Mobile|Windows CE|OPWV|T-Mobile/i", self::getUserAgent())
            || 
            preg_match("/playstation vita|nokia|palm|ipaq|skyfire|sonyericsson|\barmv7|mobile|up.browser|up.link|mmp|symbian|smartphone|midp|wap|vodafone|o2|pocket|kindle|mobile|pda|treo/i", self::getUserAgent()))
            return (self::$is_mobile = TRUE);

        return (self::$is_mobile = FALSE);
    }

    /**
    * put your comment there...
    * 
    */
    public static function isApple()
    {
        // If the is_apple value has already been set in the current request... then simply return it
        if(self::$is_apple != NULL)
            return self::$is_apple;

        return preg_match("/iPad|iPhone|iPod/i", self::getUserAgent());
    }

    /**
    * put your comment there...
    * 
    */
    public static function isIpad()
    {
        // If the is_ipad value has already been set in the current request... then simply return it
        if(self::$is_ipad != NULL)
            return self::$is_ipad;

        return (self::$is_ipad = (stripos(self::getUserAgent(),'ipad') === FALSE ? FALSE : TRUE));
    }
    
    /**
    * put your comment there...
    * 
    */
    public static function isAndroid()
    {
        // If the is_android value has already been set in the current request... then simply return it
        if(self::$is_android != NULL)
            return self::$is_android;

        return (self::$is_android = (stripos(self::getUserAgent(),'android') === FALSE ? FALSE : TRUE));
    }

    /**
    * put your comment there...
    * 
    */
    public static function isBlackBerry()
    {
        // If the is_blackberry value has already been set in the current request... then simply return it
        if(self::$is_blackberry != NULL)
            return self::$is_blackberry;

        return (self::$is_blackberry = (bool)preg_match('/RIM|BlackBerry|Playbook/i', self::getUserAgent()));
    }

     /**
     * put your comment there...
     * 
     */
     public static function isWindowsPhone()
    {
        // If the is_windowsphone value has already been set in the current request... then simply return it
        if(self::$is_windowsphone != NULL)
            return self::$is_windowsphone;

        return (self::$is_windowsphone = (bool)preg_match('/(Windows Phone|IEMobile)/i', self::getUserAgent()));
    }

    /**
    * put your comment there...
    * 
    * @param mixed $full_version
    */
    public static function getOsVersion($full_version = false)
    {
        if(self::$os_version !== null)
            return self::$os_version;

        if(preg_match("/Android\s+([1-9])\.([0-9]+)\.?/i", self::getUserAgent(), $matches) && is_array($matches) && isset($matches[1]) && ctype_digit($matches[1]) && ctype_digit($matches[2]))
            return self::$os_version = $matches[1].'.'.$matches[2];
        elseif(preg_match("/(iPhone|iPad|iPod)\b.+\b([1-9]+)_([0-9]+)_?([0-9]+)?/i", self::getUserAgent(), $matches) && is_array($matches) && isset($matches[2]) && isset($matches[3]) && ctype_digit($matches[2]) && ctype_digit($matches[3]))
            return self::$os_version = $matches[2].'.'.$matches[3].(isset($matches[4]) && ctype_digit($matches[4]) ? '.'.$matches[4] : '' );
        elseif( preg_match("/BlackBerry[0-9]{4}\/([1-9]){1}\.[0-9]/i", self::getUserAgent(), $matches) && is_array($matches) && isset($matches[1]) && ctype_digit($matches[1]) && isset($matches[2]) && ctype_digit($matches[2]))
            return self::$os_version = $matches[1];
        elseif(preg_match('/Windows Phone OS ([1-9]\.[0-9]);?/i', self::getUserAgent(), $matches) && isset($matches[1]) && is_float(($matches[1]+0)) )
            return self::$os_version = $matches[1];

        return (self::$os_version = 'Unknown');
    }

    /**
    * put your comment there...
    * 
    */
    public static function isWebOS()
    {
        // If the is_webos value has already been set in the current request... then simply return it
        if(self::$is_webos != NULL)
            return self::$is_webos;

        return (self::$is_webos = (bool)preg_match('/(WebOS|WebOS)/i', self::getUserAgent()));
    }

    /**
    * put your comment there...
    * 
    */
    public static function isWebkitMobile()
    {
        // If the is_webkitmobile value has already been set in the current request... then simply return it
        if(self::$is_webkitmobile != NULL)
            return self::$is_webkitmobile;

        return (self::$is_webkitmobile = (bool)preg_match('/(WebKit).*\s(Mobile)/i', self::getUserAgent()));
    }

    /**
    * put your comment there...
    * 
    */
    public static function isWebkit()
    {
        // If the is_webkit value has already been set in the current request... then simply return it
        if(self::$is_webkit != NULL)
            return self::$is_webkit;

        return (self::$is_webkit = (stripos(self::getUserAgent(),'webkit') === FALSE ? FALSE : TRUE ));
    }

    /**
    * put your comment there...
    * 
    */
    public static function isGecko()
    {
        // If the is_gecko value has already been set in the current request... then simply return it
        if(self::$is_gecko != NULL)
            return self::$is_gecko;

        return (self::$is_gecko = (stripos(self::getUserAgent(),'gecko') === FALSE ? FALSE : TRUE ));
    }

    /**
    * put your comment there...
    * 
    */
    public static function is3D()
    {
        // If the is_3d value has already been set in the current request... then simply return it
        if(self::$is_3d != NULL)
            return self::$is_3d;

        return (self::$is_3d = (bool)preg_match('/(HTC[\s_]?EVO[\s_]?3D|HTC[\s_]?X515a|LG.*P92[0-9]g?|LG[-\s]?SU870|\bIS12SH|SBM006SH)/i', self::getUserAgent()));
    }

    /**
    * put your comment there...
    * 
    */
    public static function isTablet()
    {
        if(self::$is_tablet != NULL)
            return self::$is_tablet;

        if(stripos(self::getUserAgent(),'tablet') === TRUE) {
            return (self::$is_tablet = TRUE);
        } elseif(self::isIpad()) {
            return (self::$is_tablet = TRUE);
        } elseif(self::isBlackBerry()){
            $_regex = 'PlayBook|RIM Tablet';
        } else {
            $_regex = 'iPad|Nexus 7\b|Android\b.*(SHW-M180S?|SPH-P[0-9]+|SGH-T869|GT-P[0-9]+[a-z]?|AT[1-3]00)|NookColor\b|nook\b|BNTV250A|LogicPD Zoom2|Android.*TAB[0-9]+|Transformer|xoom|TF101|Kindle|Silk(-?accelerated)?|HTC-?(Flyer|Jetstream|P715a|EVO View 4G)|PG41200|xoom\b|sholest|MZ6[0-9]{2,3}|Android\b.*(A[0-9]{3}|W[0-9]{2,3})|Tablet\s*(?!.*PC\b)|ViewPad7|LG-V909|MID7015|BNTV250A|LogicPD Zoom2|A7EB|CatNova8|A1_07|CT704|CT1002|M721';
        }

        if(preg_match('/'.$_regex.'/i', self::getUserAgent(), $matches))
            return (self::$is_tablet = TRUE);

        return (self::$is_tablet = FALSE);
    }
    
    /***
    * put your comment there...
    * 
    */
    public static function isPC()
    {
        return (self::getPreferredInterface() === self::INTERFACE_PC);
    }

    /***
    * This function analyzes an IP and defines whether or not it is private, i.e. if it is
    * in the range of 10.*.*.*, 172.*.*.* or 192.*.*.*
    *
    * @param string $in_ip a string representation of the IP we wish to analyze
    * @return bool TRue if the IP is private FALSE otherwise
    */
    public static function isPrivateIp($in_ip)
    {
        // Convert the ip to a long
        $in_ip_dec = ip2long($in_ip);

        // Next get a mathematical (long) representation of the first and last IPs of each private IP range
        $private_ip_range[1] = array('start' => ip2long('10.0.0.0'), 'end' => ip2long('10.255.255.255'));
        $private_ip_range[2] = array('start' => ip2long('172.16.0.0'), 'end' => ip2long('172.31.255.255'));
        $private_ip_range[3] = array('start' => ip2long('192.168.0.0'), 'end' => ip2long('192.168.255.255'));

        // Loop through each private IP range and figure out if the IP we are analizing is within it
        foreach($private_ip_range as $range){
            if($in_ip_dec >= $range['start'] && $in_ip_dec <= $range['end']){
                return true;
                break;
            }
        }

        // yey! the Ip is not private
        return false;
    }
    
    public static function getProtocol()
    {
        return 'http' . (self::getServerVarsAndHeaders('https') !== NULL ? 's' : '') . '://';
    }
    
    
    public static function getGeoIPLocation($deviceIp)
    {
        
        $geoIpRawData = shell_exec("mmdblookup --file /usr/share/GeoIP/GeoIP2-City.mmdb --ip $deviceIp");    
        //var_dump($geoIpRawData);  
        $geoIpRawData = preg_replace('/<(.+)>/', ',', $geoIpRawData);
        $geoIpRawData = str_replace(array("\r", "\n", "\t"), '', $geoIpRawData);
        $geoIpRawData = preg_replace("/\s+/", '', $geoIpRawData);
        $geoIpRawData = str_replace(',}', '}', $geoIpRawData);
        $geoIpRawData = str_replace('}{', '},{', $geoIpRawData);
        $geoIpRawData = preg_replace('/}"(\w+)":{/', "},\"$1\":{", $geoIpRawData);
        $geoIpRawData = preg_replace('/}"(\w+)":\[{/', "},\"$1\":[{", $geoIpRawData);
        $loc = json_decode($geoIpRawData);
        //var_dump($loc);
        
        return ['location' => ucwords( $loc->subdivisions[0]->names->en .", ". $loc->city->names->en), 'country' => strtoupper( $loc->country->iso_code)];
    }
}