<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to enforce hmac authentication.
 */

Namespace Core\Utils;

final class HMAC{
    
    const DEFAULT_REGION                    = 'us-south-1';
    const DEFAULT_SERVICE                   = 'api';
    const DEFAULT_HTTP_SERVICE              = 'POST';
    const TERMINATING_STRING                = 'Viper_request';
    const API_CALL_MAX_LIFE                 = 10;
    
    /**
    * @desc Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object deserialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object serialization
    */
    private function __sleep()
    {}
    
    public static function retrieveSecretAccessKey($access_key_id)
    {
        return array_search($access_key_id, array('st06RFj9oc5KAryjz55mYN5kQ58dmOhhkW1nfIPI' => 'AKIAI556DPQAK2ZQE6HA'), TRUE); 
    }
    
    public static function buildCredential($access_key_id, $short_date, $region, $service_name, $termination_string = self::TERMINATING_STRING)
    {
        return "$access_key_id/$short_date/$region/$service_name/$termination_string";   
    } 
   
    public static function canonicalizeQueryString(array &$query_string)
    {
        ksort($query_string, SORT_STRING);
        return http_build_query($query_string);    
    }
    
    public static function canonicalizeHeaders(array &$headers)
    {
        ksort(($headers = array_change_key_case($headers)), SORT_STRING);
        
        $canonicalized_headers = NULL;
        
        foreach($headers as $key => &$value)
            $canonicalized_headers .= $key . ':' . trim($value) . "\n";
            
        return $canonicalized_headers;        
    }
    
    public static function getHashedCanonicalRequest(       $http_request_method = self::DEFAULT_HTTP_SERVICE, 
                                                            $canonical_uri = '/', 
                                                            $canonical_query_string = '', 
                                                            $canonical_headers = '', 
                                                            $signed_headers = '', 
                                                            $payload = '', 
                                                            $hashing_algorithm = 'sha256'){
                                                             
                    return  hash(   $hashing_algorithm, 
                                    "$http_request_method\n" .
                                    "$canonical_uri\n" .
                                    "$canonical_query_string\n" .
                                    "$canonical_headers\n" .
                                    "$signed_headers\n" .
                                    hash($hashing_algorithm, $payload));
    }
    
    public static function getStringToSign(                 $iso8601_request_date,
                                                            $credential,
                                                            $hashed_canonical_request,
                                                            $Viper_algorithm_id = 'Viper-HMAC-SHA256'){
                    return  "$Viper_algorithm_id\n" .
                            "$iso8601_request_date\n" .
                            "$credential\n" .
                            $hashed_canonical_request;
                }
    
    public static function getDerivedSigningKey(            $secret_key,
                                                            $request_short_date,
                                                            $region = self::DEFAULT_REGION,
                                                            $service = self::DEFAULT_SERVICE,
                                                            $Viper_terminating_string = self::TERMINATING_STRING,
                                                            $hashing_algorithm = 'sha256'){
                                                                
                    return hash_hmac($hashing_algorithm, 
                                hash_hmac($hashing_algorithm,
                                    hash_hmac($hashing_algorithm,
                                        hash_hmac($hashing_algorithm,   "Viper$secret_key", 
                                                                            $request_short_date), 
                                                                                $region), 
                                                                                    $service), 
                                                                                        $Viper_terminating_string);    
                    
                }
    
    public static function getSignature(                    $string_to_sign, 
                                                            $signing_key,
                                                            $hashing_algorithm = 'sha256'){
                                                    
        return hash_hmac(   $hashing_algorithm, 
                            $string_to_sign, 
                            $signing_key);
    }
    
    public static function generateRequestContext(          $access_key_id,
                                                            $secret_key,
                                                            $host,
                                                            $uri,
                                                            $query_string,
                                                            $payload,
                                                            $extra_headers_to_sign = array(),
                                                            $http_method = self::DEFAULT_HTTP_SERVICE,
                                                            $region = self::DEFAULT_REGION,
                                                            $service = self::DEFAULT_SERVICE,
                                                            $Viper_terminating_string = self::TERMINATING_STRING,
                                                            $hashing_algorithm = 'Viper-HMAC-SHA256'){
    
        $request_date = gmdate(\Core\Utils\Time::DATE_FORMAT_ISO8601);
        $request_short_date = substr($request_date, 0, 8); 
        
        $headers_to_sign = array_replace(   $extra_headers_to_sign, 
                                            array(  'host'                      => $host,
                                                    'X-Viper-Date'              => $request_date));
        
   
        $canonicalized_headers = \Core\Utils\HMAC::canonicalizeHeaders(         $headers_to_sign);
        
        $hashed_canonical_request = \Core\Utils\HMAC::getHashedCanonicalRequest($http_method,
                                                                                $uri,
                                                                                \Core\Utils\HMAC::canonicalizeQueryString($query_string),
                                                                                $canonicalized_headers,
                                                                                ($signed_headers = strtolower(implode(';', array_keys($headers_to_sign)))),
                                                                                $payload);

        $credential = \Core\Utils\HMAC::buildCredential(                        $access_key_id, 
                                                                                $request_short_date,
                                                                                $region,
                                                                                $service,
                                                                                $Viper_terminating_string);
                                                                                
        $string_to_sign = \Core\Utils\HMAC::getStringToSign(                    $request_date,
                                                                                str_ireplace("$access_key_id/", '', $credential),
                                                                                $hashed_canonical_request,
                                                                                $hashing_algorithm);
        
        $secret_key = \Core\Utils\HMAC::retrieveSecretAccessKey(                $access_key_id);
        
        $signing_key = \Core\Utils\HMAC::getDerivedSigningKey(                  $secret_key, 
                                                                                $request_short_date);
                                                                                
        $signature = \Core\Utils\HMAC::getSignature(                            $string_to_sign, 
                                                                                $signing_key);
                                                                                
        return array(   'http'      => array( 'method'    => $http_method,
                        'header'    => "Authorization: Viper-HMAC-SHA256 Credential=$credential, SignedHeaders=$signed_headers, Signature=$signature\r\n" . str_ireplace("\n", "\r\n", $canonicalized_headers),
                        'content'   => $payload));                                                        
    }
    
    public static function isRequestValid(                  &$server_vars_and_headers,
                                                            $payload){

        $request_date = $server_vars_and_headers['x-Viper-date'];
        $request_short_date = substr($request_date, 0, 8);
        
        list($credential, $signed_headers, $signature) = explode(', ', $server_vars_and_headers['authorization']);
        
        $signed_headers_array = explode(';', ($signed_headers = substr($signed_headers, 14)));
        $signed_headers_array = (array_intersect_key($server_vars_and_headers, array_flip($signed_headers_array)));
        $canonicalized_headers = NULL;
        
        foreach($signed_headers_array as $header_name => $header_value)
            $canonicalized_headers .= $header_name . ':' . $header_value . "\n";
        
        $signature = substr($signature, 10);
        
        list($algorithm, $credential) = explode(' ', $credential, 2);
        
        $access_key_id = array_shift(explode('/', ($credential = substr($credential, 11))));

        $hashed_canonical_request = self::getHashedCanonicalRequest($server_vars_and_headers['request_method'],
                                                                    $server_vars_and_headers['request_uri'],
                                                                    $server_vars_and_headers['query_string'],
                                                                    $canonicalized_headers,
                                                                    $signed_headers,
                                                                    $payload);
                                                                    
        $credential = self::buildCredential(                        $access_key_id, 
                                                                    $request_short_date,
                                                                    \Core\Utils\Config::getConfigValue('\application\Settings::region', FALSE),
                                                                    \Core\Utils\Config::getConfigValue('\application\Settings::service_name', FALSE),
                                                                    self::TERMINATING_STRING);
                                                                                
        $string_to_sign = self::getStringToSign(                    $request_date,
                                                                    str_ireplace("$access_key_id/", '', $credential),
                                                                    $hashed_canonical_request,
                                                                    $algorithm);
                                                                                                                                  
        $secret_key = self::retrieveSecretAccessKey(                $access_key_id);                                                                               

        $signing_key = self::getDerivedSigningKey(                  $secret_key, 
                                                                    $request_short_date);
                                                                                                                                   
        
         
        $request_signture = self::getSignature(                     $string_to_sign, 
                                                                    $signing_key);

        return (($request_signture === $signature) 
                && 
                (time() - strtotime($request_date)) <= self::API_CALL_MAX_LIFE);
    }
}