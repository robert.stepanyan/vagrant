<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that alow us to dynamically include configuration files.
 */

Namespace Core\Utils;

final class Config{
    
    // PRIVATE MEMBERS
    // ===============
    private static $config_values               = NULL;         // A singleton accumulator to cache the different configuration values as they get requested
     
    /**
    * Disable object copying
    */
    private function __clone()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object serialization
    */
    private function __sleep()
    {}
    
    /**
    * Prevent the class from being instantiated
    * 
    * @returns Nothing
    */
    private function __construct()
    {}
    
    /**
    * This function associates a configuration name with the absolute path of the file containing the
    * configuration values associated with it. Note that no validation is done on either the existence 
    * of the file . This function simply applies an algorithm to to map a configuration name to the 
    * corresponding file based on the sintax of the configuration name.
    * 
    * Background
    * ========== 
    * 
    * Configuration files can be found in the Framework, Modules, Applications and Plugins. All configuration 
    * files must be stored in the 'config' directory and subdirectories within it. 
    *  
    * Examples:
    * 
    *   \Application\Settings
    *   Points to something like /home/your_username/projects/Viper/applications/config/settings/config.dev.php
    * 
    *   \Dental\Database\Doctors\Montreal
    *   Points to something like /home/your_username/projects/Viper/modules/dental/config/database/doctors/montreal/config.dev.php
    * 
    *   \Core\Utils\Security
    *   Points to something like /home/your_username/projects/Viper/core/config/security/config.dev.php
    * 
    *   \Plugins\Navigator
    *   Points to something like /home/your_username/projects/Viper/plugins/navigator/config/config.dev.php
    * 
    *  Note that three config files can exist for each environment:
    * 
    *       - 'config.dev.php' is to be used for developement
    *       - 'config.stage.php' is to be used for stage
    *       - 'config.php' is to be used for live
    * 
    *  A fourth file 'config.php' is used to store configuration files that do not depend on environment.
    *  For example: the language of an application, or a security key, etc. 
    * 
    * @param string $config_name The name of the Config
    * @param string $environment The environment possible values: .dev, .stage, .prod, or an empty string
    * 
    * @returns string The absolute path of the file containning the Config
    */
    private static function getConfigPath($config_name, $environment)                                                                           
    {
        // Split the configuration at the back slashes...
        $config_name = explode('\\', trim($config_name, '\\'));
        $namespace = strtolower(array_shift($config_name));
        
        if($namespace === 'core')
            return \Core\Router::getCorePath() . 'config/' . \Core\Router::camelCaseToUnderscore(implode('/', $config_name)) . "/config$environment.php";     
        elseif($namespace === 'application')
            return \Core\Router::getApplicationPath() . 'config/' . \Core\Router::camelCaseToUnderscore(implode('/', $config_name)) . "/config$environment.php";
        elseif($namespace === 'modules')
            return \Core\Router::getModulesPath() . \Core\Router::camelCaseToUnderscore(array_shift($config_name) . '/config/' . implode('/', $config_name)) . "/config$environment.php";
        elseif($namespace = 'plugins')
            return \Core\Router::getPluginsPath() . \Core\Router::camelCaseToUnderscore(array_shift($config_name) . '/config/' . implode('/', $config_name)) . "/config$environment.php";        
        else
            exit("[ x ] Invalid Namespace: $namespace");
    }
    
    /**
    * This function retrives ONE configuration value. Note that it does not validate the actual
    * on disk existance of the configuration file. It simply looks for the configuration value, 
    * if the value or the configuration file are not found in memory, it will it will attempt 
    * to include the configuration file and return the configuration value. Configuration values 
    * follow the naming convention as classes with the usage of namespace. For example:
    * 
    *   - '\Dental\Database::username' implies that we are looking for the username of the default 
    *       databse in the Dental module. This configuration value should be found in a file like:
    *      
    *       /home/your_username/projects/Viper/modules/dental/config/database/config.dev.php
    * 
    *   - '\Dental\Database\Doctors::username' implies that we are looking for the username of 
    *       the Doctors database in the Dental module. This configuration value should be found 
    *       in a file like:
    *      
    *       /home/your_username/projects/Viper/modules/dental/config/database/doctors/config.dev.php
    * 
    * For more information about configuration files, their nomenclature, their inner works and more, please 
    * refer to the description of 'getConfigPath' here above. 
    * 
    * @param string $config_name_and_value the fully qualified name of the configuration value
    * @param bool $environment_dependant whether we want to retrive the config values from a
    * configuration value that depends on the current environment (.dev, .stage, or .prod) or
    * not 
    * 
    * @returns mixed The configuration value, NULL if the value does not exist
    */
    public static function getConfigValue($config_name_and_value, $environment_dependant = TRUE)
    {
        /*
        * Break the configuration name and value appart at the '::' the first part is the name
        * of the configuration. The second part is the actual value we are looking for... 
        */
        $config_name_and_value = explode('::', $config_name_and_value, 2);
        $dependency = ($environment_dependant === TRUE ? 'dependent' : 'independent');
        
        // Lower case the name of the configuration... we'll use it as an identifier later on...
        $lcase_config_name = strtolower($config_name_and_value[0]);
        
        // If the value we are looking for already exists simply return it 
        if(isset(self::$config_values[$dependency][$lcase_config_name][$config_name_and_value[1]]))
            return self::$config_values[$dependency][$lcase_config_name][$config_name_and_value[1]];
        
        /**
        * If the config file has already been loaded in memory... simply bail this means the
        * value does not exist... so return NULL 
        */
        if(isset(self::$config_values[$dependency][$lcase_config_name]))
            return NULL;
     
        //echo self::getConfigPath($config_name_and_value[0], ($environment_dependant === TRUE ? ENVIRONMENT_TYPE : '')) . "\n";
        @include_once(self::getConfigPath($config_name_and_value[0], ($environment_dependant === TRUE ? ENVIRONMENT_TYPE : '')));
      
        // The load the $config variable fround in the configuration onto memory and return it...
        self::$config_values[$dependency][$lcase_config_name] = @$config;
        
        // Then attempt to return the value... 
        return @self::$config_values[$dependency][$lcase_config_name][$config_name_and_value[1]];
    }
}