<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us interact with a session.
 */

Namespace Core\Utils;

final class Session{
    
    // Members
    // =======
    private static $locale                          = NULL;
    private static $locale_options                  = NULL;
    private static $url_locale                      = NULL;
    private static $geo_location_info               = NULL;
    
    // CONSTANTS
    // =========
    const DEFAULT_SESSION_COOKIE_NAME               = '__rtmfm';                    // Default name of the cookie used to 'remember' the user
    const SESSION_COOKIE_TIME_TO_LIVE               = 604800;                       // Default expiry time of the 'remember' the user cookie: 1 week
    const ERROR_SESSION_ALREADY_AUTHENTICATED       = 0x1;                          // A flag to indicate a session is already authenticated when trying to autheticate a session 
    const SESSION_AUTHENTICATION_KEY_NAME           = 'Session.Authentication.Key';
    const ERROR_GEO_LOCATION_IS_INVALID             = 'Session.Error.GeoLocationIsInvalid';
    const SESSION_USER_TYPE_IS_ADVERTISEMENT        = 1;
    const SESSION_USER_TYPE_MEMBER                  = 2;

    /**
    * @desc Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object deserialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object serialization
    */
    private function __sleep()
    {}

    /**
    * This function drops user credentials within the current session to render the session 'authenticated'. At the time
    * of this writting, the chosen values are: id and username. Nota bene: avoid storing sensitive data, store only 
    * pointers or hints to those sensitive items. Optionally, this function will give the browser a cookie so that the 
    * user is able to auto login in the future. For this cookie to be given, both a persistent key and an authentication 
    * class name are needed.
    * 
    * @param array $authentication_values an array containning the credentials needed to be put in the current session
    * @param mixed $persistent_key a value to be used to later autheticate the user via cookies
    * @param string $authentication_class the name of the class to be used while authenticating via cookies
    * 
    * @returns void
    */
    public static function authenticate($authentication_values, $persistent_key = NULL, $authentication_class = NULL)
    {
        // Bail out if the session has already been authenticated....
        if(!(@$_SESSION[self::SESSION_AUTHENTICATION_KEY_NAME] === NULL))
            return;
        
        // Otherwise, dump the payload in the current session    
        $_SESSION[self::SESSION_AUTHENTICATION_KEY_NAME] = $authentication_values;

        // If a persistent key and an authentication class have both been pased...
        switch(($a = ($persistent_key === NULL ? 0 : 1)) + ($b = ($authentication_class === NULL ? 0 : 2)))
        {
            //  Give the suthentication cookie to the browser.... 
            case 3:
                self::giveAuthenticationCookie($authentication_class, $persistent_key);
                return;
                break;
            // Otheriwse (i.e. none or either one of the the parameters isn't given)... simply bail out
            default:
                return ;
                break;
        }
    }

    /**
     * @return int
     */
    public static function getCurrentUserID()
    {
        return (@$_SESSION[self::SESSION_AUTHENTICATION_KEY_NAME] === NULL ? 0 : $_SESSION[self::SESSION_AUTHENTICATION_KEY_NAME]['id']);
    }

    /**
     * @return boolean
     */
    public static function currentUserIsMember()
    {
        if (@$_SESSION[self::SESSION_AUTHENTICATION_KEY_NAME] === NULL){
            return false;
        }
        $authentication_values = $_SESSION[self::SESSION_AUTHENTICATION_KEY_NAME];
        return @$authentication_values['type'] == self::SESSION_USER_TYPE_MEMBER;
    }

    /**
     * @return boolean
     */
    public static function currentUserIsAdvertisment()
    {
        if (@$_SESSION[self::SESSION_AUTHENTICATION_KEY_NAME] === NULL){
            return false;
        }
        $authentication_values = $_SESSION[self::SESSION_AUTHENTICATION_KEY_NAME];
        return @$authentication_values['type'] == self::SESSION_USER_TYPE_IS_ADVERTISEMENT;
    }

    /**
     * @return mixed
     */
    public static function getCurrentUserCreationDate()
    {
        return @$_SESSION[self::SESSION_AUTHENTICATION_KEY_NAME]['date_created'];
    }
    
    /**
    * This function drops the current session and starts a brand new one.
    * 
    * @returns TRUE if the current session is succesfully destroyed and a new session is succesfully started, FALSE
    * otherwise
    */
    public static function abandon()
    {
        // Destroy the current session and strat a new one...
        return (@session_destroy() && @session_start());   
    }
    
    /**
    * This function retrieves authentication credentials stored in the current session. Those values can then 
    * be used to further increase the knowledge about an authenticated session.
    * 
    * @returns mixed The authentication credentials stored in the session. FALSE if the current session has no
    * authentication credentials stored (i.e. the user hasn't logged in yet).
    */
    public static function getAuthenticatedSessionValues()
    {
        // return the authentication if they have been set... FALSE otherwise 
        return (@$_SESSION[self::SESSION_AUTHENTICATION_KEY_NAME] === NULL ? FALSE : $_SESSION[self::SESSION_AUTHENTICATION_KEY_NAME]);
    }
    
    /**
    * This function sends an encrypted, unencoded cookie to the browser where a persistent key and an authentication 
    * class are stored. This cookie in turn can be used to auto login the user at a later time. To accomplish the 
    * auto login, the authentication class must contain method 'authenticateViaPersintentKey'.
    * 
    * @param string $authentication_class The name of the class that is to be used when authenticating the user via cookies 
    * @param mixed $persistent_key The key value to be used to authenticate the user via cookies. Use an integer or a string.
    *                               Note that any other data type might yield undesirable results as the function is not
    *                               equiped to deal with anything else.
    * @param string $cookie_name The name of the cookie. Default value is '__rtfm' ... yes... you read correctly
    * 
    * @returns TRUE if the cookie is succesfully set, FALSE otherwise  
    */
    public static function giveAuthenticationCookie($authentication_class, $persistent_key, $cookie_name = self::DEFAULT_SESSION_COOKIE_NAME)
    {
        /**
        * send an unecoded cookie to the browser... The cookie contains: The name of the authentication class, the 
        * persistent key, an expiration date, and a hash to validate the cookie contents
        */
        return setrawcookie($cookie_name,  
                            \Core\Utils\Security::encrypt(   base64_encode($authentication_class) . '.' . base64_encode($persistent_key) . '.' . ($expires = gmmktime() + self::SESSION_COOKIE_TIME_TO_LIVE) . '.' . md5($authentication_class . '.' . $persistent_key . '.' . $expires . '.' . \Core\Utils\Security::VALIDATION_KEY), 
                                                        file_get_contents(\Core\Router::getModulesPath() . 'utils/config/security/encryption.key'), 
                                                        TRUE), 
                            time() + self::SESSION_COOKIE_TIME_TO_LIVE, '/',
                            $_SERVER['SERVER_NAME'],
                            FALSE,
                            TRUE);
    }
    
    /**
    * This function authenticates a session based on the value containned in a cookie. The cookie must contain an
    * encrypted string that contains 4 values:
    * 
    *   - The name of the authetication class to be used i.e. the class containning the 'authenticateViaPersintentKey' method)
    *   - A value to be passed to the 'authenticateViaPersintentKey' method so that the user can be authenticated
    *   - The GMT time when the cookie expires
    *   - A hash of the cookie value to check for tampering
    * 
    * @param string $cookie_name The name of the cooki containning the encrypted authentication values
    * 
    * @returns mixed The values returned by $authentication_class::authenticateViaPersintentKey(), please the documentation
    *                   of the authentication class to identify its return values. 
    */
    public static function authenticateViaCookie($cookie_name = self::DEFAULT_SESSION_COOKIE_NAME)
    {
        // Bail out if the session has already been authenticated
        if(self::getAuthenticatedSessionValues() !== FALSE)
        {
            return TRUE;                                                // Interpret the session already authenticated as as succesful... after all the goal is to authenticate a session...no?     
        } // Bail out if the cookie doesn't exist  
        elseif(isset($_COOKIE[$cookie_name]) === FALSE)
            return FALSE;                                               // Interpret the lack of cookie as a failure
        
        // Grab the contents of the cookie, decrypt them, then split them into 4 variables...    
        @list($authentication_class, $persistent_key, $expires, $hash) = @explode(  '.', \Core\Utils\Security::decrypt(  str_replace(' ', '+', $_COOKIE[$cookie_name]),
                                                                                                                    file_get_contents(\Core\Router::getModulesPath() . 'utils/config/security/encryption.key'), 
                                                                                                                    TRUE));

        // Parse the name of the authentication class... 
        $authentication_class = base64_decode($authentication_class);
        
        // Parse the value of the persistent key
        $persistent_key = base64_decode($persistent_key);
        
        // Ensure the contents of cookie haven't been tampered with and that the cookie hasn't expired...
        if(md5($authentication_class . '.' . $persistent_key . '.' . $expires . '.' . \Core\Utils\Security::VALIDATION_KEY) !== $hash || $expires < gmmktime())
        {
            // In either case... Delete the cookie and interpret it as a failure...
            setrawcookie($cookie_name, 0);
            return FALSE;
        }
            
        // All good! pass control now to the authentication class... 
        return call_user_func($authentication_class . '::authenticateViaPersintentKey', $persistent_key);
    }

    /**
     *
     */
    public static function getPotentialLocaleFromCurrentURI()
    {
        if(self::$url_locale !== NULL)
            return self::$url_locale;

        /**
         * Grab the URI of the curent request and break it into two elements:
         *
         * - The first one is the language for all languages except the default one
         * - The second one will contain the controller, action, etc
         */
        $potential_route_elements = explode('/', ($potential_route_elements = trim(parse_url($_SERVER['REQUEST_URI'],  PHP_URL_PATH), '/')), 2);

        // Grab the locale used in the URI...
        return (self::$url_locale =  (@$potential_route_elements[0] ? $potential_route_elements[0] : FALSE));
    }
    
    /***
    * put your comment there...
    * 
    * @param mixed $nullify_if_default
    * @param mixed $as_path
    */
    public static function getLocale($nullify_if_default = FALSE, $as_path = FALSE)
    {
        /**
        * If the locale has previously been defined then...
        * If the locale is the default one of the application and we are being requested to
        * return a NULL value if the locale is the default one... simply return NULL, otherwise 
        * return the locale string either as a path or as a value
        * 
        * Note: this logic is useful in places such as URL creation, where we need to omit the
        * locale and or use it with a slash prefix to make it part of a URL.    
        */
        if(self::$locale !== NULL)
            return ($nullify_if_default === TRUE &&  @self::$locale[1] === TRUE ? NULL : ($as_path === TRUE ? (self::$locale[0] === FALSE ? NULL : '/' . self::$locale[0]) : self::$locale[0]));
            
        /**
        * If no locales have been defined:
        * 
        * 1.- Set the locale to false
        * 2.- Identify the locale as being the default
        * 
        * If we are being asked to return it as a NULL return NULL
        * If we are being asked to return is as a PATH return NULL there is no valid path to be built
        * 
        * Otherwise return FALSE... i.e. no valid locale was found     
        */
        if(($locales = \Core\Utils\Config::getConfigValue('\Application\Settings::locales', FALSE)) === NULL)
        {
            self::$locale = array(FALSE, TRUE); 
            return ($nullify_if_default === TRUE || $as_path === TRUE ? NULL : self::$locale[0]);
        }

        /** 
        * At this point we know that the current application supports localization
        * Grab default values:
        * 
        * 1.- The locale itself
        * 2.- A flag to identify the locale in memory as the default one
        */
        self::$locale = array(array_shift($locales), TRUE) ;

        // Grab the language as defined by the current URI
        $url_language = self::getPotentialLocaleFromCurrentURI();
    
        /**
        * If the locale found in the URI is NOT one of the application locales
        * We bail out using the default values already in memory...
        * 
        * Because we know we are using the default locale... if we are being asked 
        * to return NULL simply return NULL
        * 
        * If we are being asked to return it as a path append a slash at the begining
        * of the locale string...   
        */
        if(in_array($url_language, $locales) === FALSE)
            return ($nullify_if_default === TRUE ? NULL : ($as_path === TRUE ? '/': NULL) . self::$locale[0]);
        
        /**
        * Modify the locale in memory:
        * 
        * 1.- Set it to be the value passed in the URI
        * 2.- Set a flaf to identoify the locale in memory as NOT being the default 
        *       locale of the current application
        */
        self::$locale = array($url_language, FALSE);
        
        /**
        * At this point we know the locale is not the default one, so we also know
        * we don't need to nullify it if asked, therefore we simply append a slash 
        * if asked to return it as a path... and return it. 
        */
        return ($as_path === TRUE ? '/': NULL) . self::$locale[0];
    }
    
    /***
    * put your comment there...
    * 
    */
    public static function getLocaleOptions()
    {
     
        if(self::$locale_options !== NULL)
            return self::$locale_options;
            
       
        $locales = \Core\Utils\Config::getConfigValue('\Application\Settings::locales', FALSE);
        $current_locale = self::getLocale();
  
      
        $uri = '/' . ltrim(str_ireplace(('/' . $current_locale), '', $_SERVER['REQUEST_URI']), '/');
        $url =  \Core\Utils\Device::getProtocol() . 
                \Core\Utils\Config::getConfigValue('\Application\Settings::hostname');
         
        $is_default_locale = TRUE;
        
        foreach($locales as $locale)
        {
            if($locale !== $current_locale)
            {
                self::$locale_options[] = array('[' . strtoupper($locale) . ']', $url . ($is_default_locale === FALSE ? "/$locale" : NULL) . $uri, '[HREF-TITLE-LANGUAGE-OPTION-' . strtoupper($locale) .']', $locale) ;
            }
            
            if($is_default_locale === TRUE)       
                $is_default_locale = FALSE;
        }
    
        return self::$locale_options;  
    }

    public static function getGeoLocation_old()
    {
        //$deviceIp = '142.169.78.244';     // Montreal invalid
        //$deviceIp = '107.171.187.210';    // Beloil
        //$deviceIp = '128.90.116.132';     // Mexico
        //$deviceIp = '24.114.84.237';      //Moncton
        //$deviceIp = '107.171.191.113';    // Pierrefonds

        $deviceIp = \Core\Utils\Device::getDeviceIp(); // NULL for dev
        $cacheKey = "GEOIP_CACHED_DATA:$deviceIp";

        if(NULL !== self::$geo_location_info) {
            return self::$geo_location_info;
        }

        $cachedGeoIpData = \Core\Cache::get($cacheKey);

        if(FALSE !== $cachedGeoIpData && NULL === $cachedGeoIpData) {
                return (self::$geo_location_info = self::ERROR_GEO_LOCATION_IS_INVALID);
        } elseif(FALSE !== $cachedGeoIpData) {
            return (self::$geo_location_info = json_decode($cachedGeoIpData));
        }

        $file = \Core\Utils\Config::getConfigValue('\Core\Geoip::CityFile');
        $geoIpRawData = shell_exec("mmdblookup --file $file --ip $deviceIp");

        if(NULL === $geoIpRawData) {
            \Core\Cache::save($cacheKey, NULL, 60);
            return (self::$geo_location_info = self::ERROR_GEO_LOCATION_IS_INVALID);
        }

        $geoIpRawData = preg_replace('/<(.+)>/', ',', $geoIpRawData);
        $geoIpRawData = str_replace(array("\r", "\n", "\t"), '', $geoIpRawData);
        $geoIpRawData = preg_replace("/\s+/", '', $geoIpRawData);
        $geoIpRawData = str_replace(',}', '}', $geoIpRawData);
        $geoIpRawData = str_replace('}{', '},{', $geoIpRawData);
        $geoIpRawData = preg_replace('/}"(\w+)":{/', "},\"$1\":{", $geoIpRawData);
        $geoIpRawData = preg_replace('/}"(\w+)":\[{/', "},\"$1\":[{", $geoIpRawData);
        $geoIpDecodedData = json_decode($geoIpRawData);

        if(NULL === $geoIpDecodedData) {
            \Core\Cache::save($cacheKey, NULL, 60);
            return (self::$geo_location_info = self::ERROR_GEO_LOCATION_IS_INVALID);
        }

        \Core\Cache::save($cacheKey, $geoIpRawData, 3600);
        return (self::$geo_location_info = $geoIpDecodedData);
    }

      public static function getGeoLocation()
    {
        //$deviceIp = '142.169.78.244';     // Montreal invalid
        //$deviceIp = '107.171.187.210';    // Beloil
        //$deviceIp = '128.90.116.132';     // Mexico
        //$deviceIp = '24.114.84.237';      //Moncton
        //$deviceIp = '107.171.191.113';    // Pierrefonds

        $deviceIp = \Core\Utils\Device::getDeviceIp(); // NULL for dev
        $cacheKey = "GEOIP_CACHED_DATA:$deviceIp";
        //echo  $cacheKey;
        //header_remove(); 
        //var_dump(self::$geo_location_info);
        
        //die();
        
        if(NULL !== self::$geo_location_info && 'Session.Error.GeoLocationIsInvalid' !== self::$geo_location_info) {
            //die("return1 ");
            return self::$geo_location_info;
        }

        $cachedGeoIpData = \Core\Cache::get($cacheKey);
        //var_dump($cachedGeoIpData);
        if(FALSE !== $cachedGeoIpData && (NULL == $cachedGeoIpData || 'Session.Error.GeoLocationIsInvalid' === $cachedGeoIpData) ) {
//                die("ret 2 ") ;
                return (self::$geo_location_info = self::ERROR_GEO_LOCATION_IS_INVALID);
        } elseif(FALSE !== $cachedGeoIpData) {
//            die("ret 3 ") ;
            return (self::$geo_location_info = json_decode($cachedGeoIpData));
        }

       
        //die($deviceIp);
        $file = \Core\Utils\Config::getConfigValue('\Core\Geoip::IP2LocationFile');    //'/code/core/src/ip2loc/data/IP-COUNTRY-REGION-CITY-LATITUDE-LONGITUDE.BIN'
        $db = new \Core\Ip2loc\Database($file, \Core\Ip2loc\Database::FILE_IO);

        $records = $db->lookup($deviceIp, \Core\Ip2loc\Database::ALL);
       
        if($records['regionName']== "Quebec"){
            $regioCode = "QC";
        }else{
            $regioCode = "N/A";
        }
        $geo["country"]         = ['name'  => $records['countryName'], 'iso_code' => $records['countryCode'] ];
        $geo["subdivisions"][]  = ['names' => ['en' => $records['regionName']], "iso_code" => $regioCode];
        $geo["city"]            = ['names' => ["en" => $records['cityName']]];
        $geo["latitude"]        = $records['latitude'];
        $geo["longitude"]        = $records['longitude'];
        
        $geojson = json_encode($geo);
        
        $geoOBJ = json_decode($geojson);
        
        $geoIpRawData = $geojson;
        $geoIpDecodedData = $geoOBJ;


        if(NULL === $geoIpDecodedData) {
            die("a");
            \Core\Cache::save($cacheKey, NULL, 60);
            return (self::$geo_location_info = self::ERROR_GEO_LOCATION_IS_INVALID);
        }
        //die("b");
        self::$geo_location_info = $geoIpDecodedData;
        //var_dump(self::$geo_location_info);
        \Core\Cache::save($cacheKey, $geoIpRawData, 3600);
        //die("zzzz");
        return self::$geo_location_info;
    }

    
    public static function getLocation(){
         $loc = \Core\Utils\Session::getGeoLocation();
         return ['location' => ucwords( $loc->subdivisions[0]->names->en .", ". $loc->city->names->en), 'country' => strtoupper( $loc->country->iso_code)]; 
    }
    public static function getLocationNew(){
         $loc = \Core\Utils\Session::getGeoLocationNew();
         return ['location' => ucwords( $loc['regionName'] .", ". $loc['cityName']), 'country' => strtoupper( $loc['countryCode'])]; 
    }

    public static function setRefferer($parsedUrl) {
        $_SESSION['referrer']=$parsedUrl;
    }

    public static function getRefferer() {
        return !empty($_SESSION['referrer']) ? $_SESSION['referrer'] : '/';
    }
}