<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class that provide us standarized image related
 * utilitarian functions and methods.
 */

Namespace Core\Utils;

class Image{
    
     
    const MAX_SIZE_IMAGE_IN_BITES                   = 5242880;                                          // Max 2MB file size on images... 
    
    const ORIENTATION_LANDSCAPE                     = 'L';
    const ORIENTATION_PORTRAIT                      = 'P';
    const ORIENTATION_SQUARE                        = 'S';
    
    const ERROR_INVALID_IMAGE_HEIGHT                = 'Core.Utils.Image.ErrorInvalidImageHeight';
    const ERROR_INVALID_IMAGE_WIDTH                 = 'Core.Utils.Image.ErrorInvalidImageWidth';
    const ERROR_INVALID_IMAGE                       = 'Core.Utils.Image.ErrorInvalidImage';
    const ERROR_UNABLE_TO_CREATE_RESAMPLING_DIR     = 'Core.Utils.Image.ErrorUnableToCreateResamplingDir';

    public static $allowed_ext = array(
        'jpg', 'jpeg', 'jpe', 'gif', 'png', 'bmp'
    );

	/**
   	* Disable object copying
    	*/
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
   	/**
    	* Disable object deserialization
    	*/
    private function __sleep()
   	{}
    
    /**
    * Prevent the class from being instantiated
    * 
    * @returns Nothing
    */
    private function __construct()
    {} 

	/***
        * This public static function is used to retrive a time stamp that falls at the beginning of a particular chunk.
        * The concept of a chunk comes from partitionning the 24 hrs in a day into a max of twelve parts.
        * Each part is a chunk and the chucnk size is the number of hours in it. The snallest chunk size is
        * 1, meanning 1 hr per chunk which yields 24 chunks, which implies that the public static function will return
        * the time stamp of the current hour when it gets called. The largest chunck size is 12, meanning
        * two chunks.
        *
        * This public static function is useful for database calls where a time stamp is used to retrieve records based
        * on the current time. Use it instead of time() with cached queries where the query itself is used
        * to produce the cache key.
        *
        * @param mixed $chunk_size the number of hours in each chunk, max. 12, min. 1, anything in between
        * must be a multiple of 2.
        *
        * @return int
        */
    public static function getImageOrientation($image_url)
    {
       list($image_width, $image_height) = getimagesize($image_url);
       
       // Validate the height and width of the original image: both must be greater than 0 
        if($image_height <= 0)  
            return self::ERROR_INVALID_IMAGE_HEIGHT;
        elseif($image_width <= 0)
            return self::ERROR_INVALID_IMAGE_WIDTH;
       
       if(($image_orientation = ($image_width/$image_height)) < 1)      // Less than 1 => width < height: Portrait   
            return self::ORIENTATION_PORTRAIT;                                            
       elseif($image_orientation > 1)                                   // More than 1 => width > height: Landscape
            return self::ORIENTATION_LANDSCAPE;    
        else                                                            // Exactly 1 => wicth is equal to height: Square
            return self::ORIENTATION_SQUARE;    
    }

    /**
    * put your comment there...
    * 
    * @param mixed $absolut_path_to_original_image
    * @param mixed $absolut_path_to_replica_image
    * @param mixed $replica_image_width
    */
    public static function resampleJPEGImage($absolut_path_to_original_image, $absolut_path_to_replica_image, $replica_image_width = NULL, $force_destination_directory_creation = FALSE)
    {
        // Get the height and width of the original image
        list($original_image_width, $original_image_height) = @getimagesize($absolut_path_to_original_image);
        
        // Validate the height and width of the original image: both must be greater than 0 
        if($original_image_height <= 0)  
            return self::ERROR_INVALID_IMAGE_HEIGHT;
        elseif($original_image_width <= 0)
            return self::ERROR_INVALID_IMAGE_WIDTH;
        
        if($replica_image_width === NULL)
            $replica_image_width = $original_image_width; 
        
        /**
        * Get the orientation of original image. Based on the original orientaion of the image
        * define the height of our replica image. Nota bene: calculations based on 3:2 aspect ratio  
        */
        if(($original_image_orientation = ($original_image_width/$original_image_height)) < 1)      // Less than 1 => width < height: Portrait  
            $replica_height = (($replica_image_width / 2) * 3);                                           
        elseif($original_image_orientation > 1)                                                     // More than 1 => width > height: Landscape    
            $replica_height = (($replica_image_width / 3) * 2);
        else                                                                                        // Exactly 1 => wicth is equal to height: Square
            $replica_height = $replica_image_width;       
        
        /**
        * Adjust height or width so that their values allow us to resample the image proportionally 
        */
        if(($replica_image_width/$replica_height) > $original_image_orientation)
        {
            $height_for_resampling = ($replica_image_width/$original_image_orientation);
            $width_for_resampling = $replica_image_width; 
        }      
        else
        {
            $width_for_resampling = ($replica_height*$original_image_orientation);
            $height_for_resampling = $replica_height; 
        }
             
        $process = imagecreatetruecolor(round($width_for_resampling), round($height_for_resampling));

        $image_for_resampling = @imagecreatefromjpeg($absolut_path_to_original_image);
        
        if($image_for_resampling === FALSE)
            return self::ERROR_INVALID_IMAGE;

        imagecopyresampled($process, $image_for_resampling, 0, 0, 0, 0, $width_for_resampling, $height_for_resampling, $original_image_width, $original_image_height);
        $replica_image = @imagecreatetruecolor($replica_image_width, $replica_height);
        imagecopyresampled($replica_image, $process, 0, 0, (($width_for_resampling/2)-($replica_image_width/2)), (($height_for_resampling/2)-($replica_height/2)), $replica_image_width, $replica_height, $replica_image_width, $replica_height);

        imagedestroy($process);
        imagedestroy($image_for_resampling);
        
        if($force_destination_directory_creation === TRUE && is_dir(dirname($absolut_path_to_replica_image)) === FALSE && @mkdir(dirname($absolut_path_to_replica_image), 0777, TRUE) === FALSE)
            return self::ERROR_UNABLE_TO_CREATE_RESAMPLING_DIR;

        return imagejpeg($replica_image, $absolut_path_to_replica_image, 100);
    }

    /**
     * @param $file
     * @param $catalog_id
     * @param null|string $ext_override
     * @return array|string
     */
    public static function save($file, $catalog_id, $ext_override = null)
    {

        // Create a random file name
        $hash = md5(microtime(true) * mt_rand(1, 1000000));


        if (is_null($ext_override)) {
            $ext = pathinfo($file, PATHINFO_EXTENSION);
        }
        else {
            $ext = $ext_override;
        };

        $ext = strtolower($ext);

        if (!in_array($ext, self::$allowed_ext  ) )
        {
            return self::ERROR_INVALID_IMAGE;
        }
        $remote = self::_getFullPath(array(
            'catalog_id' => $catalog_id,
            'hash' => $hash,
            'ext' => $ext
        ));

        $filePath  = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_path');
        $filePath = rtrim(dirname($filePath), DIRECTORY_SEPARATOR);

        $remote = $filePath.$remote;
        $dir = explode(DIRECTORY_SEPARATOR, $remote);
        array_pop($dir);
        $dir = implode(DIRECTORY_SEPARATOR, $dir);

        if (!is_dir($dir)){
            mkdir($dir,0777, true);
        }

        if (is_uploaded_file($file)){
            move_uploaded_file($file, $remote);
        }else{
            rename($file, $remote);
        }

        return array('hash' => $hash, 'ext' => $ext);
    }

    /**
     * @param array $entries
     * @return bool
     */
    public static function remove($entries)
    {
        if ( ! is_array($entries) ) {
            $entries = array($entries);
        }

        if ( ! count($entries) ) return false;
        $filePath  = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_path');
        $filePath = rtrim(dirname($filePath), DIRECTORY_SEPARATOR);

        foreach ($entries as $entry){
            $remote = self::_getFullPath($entry);
            unlink($filePath.$remote);
        }

        return true;
    }

    /**
     * @param array | stdClass $entry
     * @return string|boolean
     */
    protected static function _getFullPath($entry)
    {
        if (is_object($entry)){
            $entry = (array) $entry;
        }

        if (!is_array($entry)){
            return false;
        }

        $catalog = $entry['catalog_id'];
        $parts = array($catalog);
        $DS = DIRECTORY_SEPARATOR;
        $a = array();
        if ( is_numeric($catalog) ) {
            $parts = array();

            if ( strlen($catalog) > 2 ) {
                $parts[] = substr($catalog, 0, 2);
                $parts[] = substr($catalog, 2);
            }
            else {
                $parts[] = '_';
                $parts[] = $catalog;
            }
        }
        else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
            array_shift($a);
            $catalog = $a[0];

            $parts = array();

            if ( strlen($catalog) > 2 ) {
                $parts[] = substr($catalog, 0, 2);
                $parts[] = substr($catalog, 2);
            }
            else {
                $parts[] = '_';
                $parts[] = $catalog;
            }

            $parts[] = $a[1];
        }

        $catalog = implode($DS, $parts);

        $path = $DS . $catalog . $DS . $entry['hash'] . '.' . $entry['ext'];

        return $path;
    }

    /**
     * Returns url of file
     * @param array $entry
     * @return string
     */
    public static function getUrl($entry)
    {
        $path = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_images');
        $url = \Core\Utils\Device::getProtocol() . substr($path,0, strpos($path, '/'));
        return trim($url, DIRECTORY_SEPARATOR) . self::_getFullPath($entry);
    }
}