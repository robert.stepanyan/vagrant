<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to enforce security.
 */

Namespace Core\Utils;

final class Security{
    
    // CONSTANTS
    // =========
    const CAPTCHA_SESSION_IDENTIFIER                = 'Captcha.Session.Identifier';
    const LEN_CAPTCHA                               = 5;
    const MIN_LEN_EMAIL                             = 6;                                                // The minimal length of an email address
    const MAX_LEN_EMAIL                             = 100;                                              // The maximum length of an email address
    const MIN_INT_VAL                               = 0;                                                // The default minimum of an inputed integer
    const MAX_INT_VAL                               = 9999;                                             // The default maximum value of an inputed integer
    const MIN_LEN_PHONE                             = 10;
    const MAX_LEN_PHONE                             = 10;

    /**
    * Error constants with values 1 to 8 are 
    * reserved to handle uploaded images errors 
    * without any conflicts. these constants are:
    * 
    * UPLOAD_ERR_INI_SIZE                           = 0x1;
    * UPLOAD_ERR_FORM_SIZE                          = 0x2;
    * UPLOAD_ERR_PARTIAL                            = 0x3;
    * UPLOAD_ERR_NO_FILE                            = 0x4;
    * UPLOAD_ERR_NO_TMP_DIR                         = 0x6;
    * UPLOAD_ERR_CANT_WRITE                         = 0x7;
    * UPLOAD_ERR_EXTENSION                          = 0x8;
    * 
    * For more information about these constants, 
    * please go here: 
    * 
    * http://www.php.net/manual/en/features.file-upload.errors.php 
    */
    const ERROR_STRING_IS_EMPTY                     = 'Core.Utils.Security.ErrorStringIsEmpty';                                              // ERROR: empty string
    const ERROR_STRING_IS_TOO_SHORT                 = 'Core.Utils.Security.ErrorStringIsTooShort';                                              // ERROR: short string
    const ERROR_STRING_IS_TOO_LONG                  = 'Core.Utils.Security.ErrorStringIsTooLong';                                              // ERROR: long string
    const ERROR_PHONE_IS_EMPTY                      = 'Core.Utils.Security.ErrorPhoneIsEmpty';
    const ERROR_PHONE_IS_TOO_SHORT                  = 'Core.Utils.Security.ErrorPhoneIsTooShort';
    const ERROR_PHONE_IS_TOO_LONG                   = 'Core.Utils.Security.ErrorPhoneIsTooLong';
    const ERROR_PHONE_IS_INVALID                    = 'Core.Utils.Security.ErrorPhoneIsInvalid';                                              // ERROR: invalid phone
    const ERROR_EMAIL_IS_EMPTY                      = 'Core.Utils.Security.ErrorEmailIsEmpty';                                              // ERROR: empty email
    const ERROR_EMAIL_IS_TOO_SHORT                  = 'Core.Utils.Security.ErrorEmailIsTooShort';                                              // ERROR: short email
    const ERROR_EMAIL_IS_TOO_LONG                   = 'Core.Utils.Security.ErrorEmailIsTooLong';                                              // ERROR: long email
    const ERROR_EMAIL_IS_INVALID                    = 'Core.Utils.Security.ErrorEmailIsInvalid';                                             // ERROR: invalid email
    const ERROR_IMAGE_ERROR_UNHANDLED               = 'Core.Utils.Security.ErrorImageErrorUnhandled';
    const ERROR_IMAGE_HEIGHT_IS_INVALID             = 'Core.Utils.Security.ErrorImageHeightIsInvalid';                                             // ERROR: image height less than or equal to 0
    const ERROR_IMAGE_WIDTH_IS_INVALID              = 'Core.Utils.Security.ErrorImageWidthIsInvalid';                                             // ERROR: image width less than or equal to 0
    const ERROR_IMAGE_SIZE_IS_TOO_BIG               = 'Core.Utils.Security.ErrorImageSizeIsTooBig';
    const ERROR_IMAGE_TYPE_IS_INVALID               = 'Core.Utils.Security.ErrorImageTypeIsInvalid';                                             // ERROR: image not a jpeg
    const ERROR_IMAGE_RESAMPLING_FAILED             = 'Core.Utils.Security.ErrorUnableResamplingFailed';                                             // Unable to resample an image
    const ERROR_CAPTCHA_IS_INVALID                  = 'Core.Utils.Security.ErrorCaptchaIsInvalid';
    const ERROR_CAPTCHA_IS_TOO_SHORT                = 'Core.Utils.Security.ErrorCaptchaIsTooShort';
    const ERROR_CAPTCHA_IS_TOO_LONG                 = 'Core.Utils.Security.ErrorCaptchaIsTooLong';
    const ERROR_CAPTCHA_IS_EMPTY                    = 'Core.Utils.Security.ErrorCaptchaIsEmpty';
    const ERROR_ID_IS_INVALID                       = 'Core.Utils.Security.ErrorIdIsInvalid';
    const ERROR_INTEGER_IS_INVALID                  = 'Core.Utils.Security.ErrorIntegerInvalid';
    const ERROR_INTEGER_IS_EMPTY                    = 'Core.Utils.Security.ErrorIdIntegerEmpty';
    const ERROR_UNKNOWN                             = 'Core.Utils.Security.ErrorUnknown';
    const NO_ERROR                                  = 'Core.Utils.Security.NoError';
    
    // MINIMUM SIZES FOR AD IMAGES
    const AD_IMAGE_MINHEIGHT_LANDSCAPE = 400;
    const AD_IMAGE_MINWIDTH_LANDSCAPE = 600;
    
    const AD_IMAGE_MINHEIGHT_PORTRAIT = 600;
    const AD_IMAGE_MINWIDTH_PORTRAIT = 400;
    
    const AD_IMAGE_MINHEIGHT_SQUARED = 400;
    const AD_IMAGE_MINWIDTH_SQUARED = 400;
    
    // IMAGE ORIENTATIONS
    const AD_IMAGE_LANDSCAPE = 1;
    const AD_IMAGE_PORTRAIT = 2;
    const AD_IMAGE_SQUARED = 3;
    
    // MEMBERS
    // =======
    private static $html_translation_table          = NULL;
    
    /**
    * @desc Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object deserialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object serialization
    */
    private function __sleep()
    {}

    /** 
    * PBKDF2 Implementation (as described in RFC 2898); 
    *
    * @param   string  p   password
    * @param   string  s   salt
    * @param   int     c   iteration count (use 1000 or higher)
    * @param   int     kl  derived key length
    * @param   string  a   hash algorithm
    *
    * @returns  string  derived key
    */
    private static function pbkdf2($p, $s, $c, $kl, $a = 'sha256' )
    {
        // Hash length
        $hl = strlen(hash($a, null, true));
        
        // Key blocks to compute    
        $kb = ceil($kl/$hl);
        
        // Derived key                
        $dk = '';                          

        // Create key
        for ( $block = 1; $block <= $kb; $block ++ ) {

            // Initial hash for this block
            $ib = $b = hash_hmac($a, $s . pack('N', $block), $p, true);

            // Perform block iterations
            for ( $i = 1; $i < $c; $i ++ )

                // XOR each iterate
                $ib ^= ($b = hash_hmac($a, $b, $p, true));
            
            // Append iterated block
            $dk .= $ib; 
        }

        // Return derived key of correct length
        return substr($dk, 0, $kl);
    }

    /**
    * Tis function will analyze an action name looking for the '_ma' at the end. If the action name 
    * does contain this string and a login url has been defined for the current application, it will
    * then redirect the use to that url.
    * 
    * @param string $action The name of the action
    * @param string $url_after The URL to forward the user after succesful login 
    */
    public static function redirectIfActionRequiresLogin($action, $url_after)
    {   
        // Bail out if an authenticated session is already found or if the action name doesn't mark it as part of the MA   
        if(\Core\Utils\Session::getAuthenticatedSessionValues() !== FALSE || ($action_requires_login = @substr($action, -3, 3) === '_ma') === FALSE)
            return;

        // Default login controller and action
        $login_controller = 'user';
        $login_action = 'login';

        // Retrieve custom login url...
        if(\Core\Utils\Config::getConfigValue('\Application\Settings::login_url', FALSE) !== NULL)
            list($login_controller, $login_action) = explode('/', trim(\Core\Utils\Config::getConfigValue('\Application\Settings::login_url', FALSE), '/'), 2);

        // Forward the user to the login page
        \Core\Controller::redirect(\Core\Utils\Navigation::urlFor($login_controller, $login_action, array('urlafter' => $url_after), FALSE), '303 See Other');
    }
    
    public static function throwNotFoundIfActionRequiresAnonymity($action)
    {
        // Bail out if an authenticated session is NOT found or if the action name doesn't mark it as NON-MA   
        if(\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE || ($action_requires_anonymity = @substr($action, -4, 4) === '_nma') === FALSE)
            return;
            
        // Forward the user to the login page    
        \Core\Router::throw404();           
    }
    
    /**
    * This function does some simple clean up on a RELATIONAL array by removing unwanted charcters from 
    * the keys and values.
    * 
    * @param array $array The array we wish to clean passed by reference
    * @returns Nothing
    */
    public static function cleanArray(&$array)
    {   
        // Loop through each array element and remove unwanted characters     
        foreach ($array as $key => $val)
        {
            if(preg_match('#([^a-z0-9\-\_\:\@\|])#i', urldecode($key)))
                unset($array[$key]);
            elseif(is_array($val))
                self::cleanArray($val);
            else
                $array[$key] =  stripslashes(urldecode(str_replace("\0", '', $val)));
        }
    }
    
	
	public static function checkSqlInject()
	{
		$post = $_POST;
		$get = $_GET;
		$cookie = $_COOKIE;
		$data = array('get' => $get, 'post' => $post, 'cookie' => $cookie);
		$found = false;
		$malicious = array();
		foreach ( $data as $type => $params ) {
			
			foreach ( $params as $key => $value ) {
				if ( self::_checkFor_sql($value) ) {
					$malicious[$key] = $value;
				}
			}
			
			
			if ( count($malicious) > 0 )
				$found = true;
		}

		if ( ! $found ) return;
		//var_dump($malicious);die;
		/*$date = date('Y-m-d H:i:s');
		$user_id = null;

		$user = Model_Users::getCurrent();
		if ( $user ) $user_id = $user->id;
		
		$ip_addrs = $this->_getClientAddrs();
		$host_names = $this->_getHostNames($ip_addrs);

		$url = $_SERVER['REQUEST_URI'];

		$db = Zend_Registry::get('db');

		$db->insert('malicious_requests', array(
			'date_time' => $date,
			'logged_user_id' => $user_id,
			'client_id' => $client_id,
			'ip_addrs' => implode(', ', $ip_addrs),
			'host_names' => implode(', ', $host_names),
			'url' => $url,
			'data' => json_encode($data),
			'malicious' => json_encode($malicious)
		));*/


		die('Mongo DB error');
	}
	
	private static function _checkFor_sql($value)
	{
		
		if ( @preg_match('/(aes_decrypt|load_file|union.+select|union.+delete|union.+update)/xi', $value) ) {
			return true;
		}

		return false;
	}
    /**
    * This function clean up a string to be used as SQL input. It scapes certain characters and appends apostrophes.
    *
    * @param mixed $input The string to be scaped
    * @return string
    */
    public static function sanitizeValue($input)
    {
        return addslashes($input);
    }
    
    /**
    * This function will remove HTML characters from a string. It is useful when dealing with user provided
    * content to rid TEXT of unwanted HTML, tabs, nulls, returns, and new lines
    * 
    * @param string $html The prsumed HTML text
    * @returns a string containning $html without any HTML formating 
    */
	public static function htmlToText($html)
    {   
        return trim(filter_var($html, FILTER_SANITIZE_STRING));
    }

    /**
    * put your comment there...
    * 
    * @param mixed $input_string
    * @param mixed $max_len
    * @param mixed $min_len
    */
    public static function isStringInputValid(&$input_string, $max_len, $min_len = 0)
    {
   
        
        $input_string = self::htmlToText($input_string);
       
        if($min_len > 0 && empty($input_string))
            return self::ERROR_STRING_IS_EMPTY;
        
        if(!(@$input_string{$max_len} === ''))
            return self::ERROR_STRING_IS_TOO_LONG;
            
        if($min_len > 0 && (@$input_string{($min_len - 1)} === ''))
            return self::ERROR_STRING_IS_TOO_SHORT;  

        return self::NO_ERROR;    
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $input_phone
    */
    public static function isPhoneInputValid(&$input_phone)
    {
        $input_phone = self::htmlToText($input_phone);
        
        if(empty($input_phone))
            return self::ERROR_PHONE_IS_EMPTY;
            
        $input_phone = \Core\Utils\Text::extractPhoneNumberFromString($input_phone);
        
        if(empty($input_phone))
            return self::ERROR_PHONE_IS_INVALID;

        $input_phone_validity = self::isStringInputValid($input_phone, self::MIN_LEN_PHONE, self::MAX_LEN_PHONE);
        
        switch($input_phone_validity)
        {
            case self::NO_ERROR:
                for($x = 0; $x <= 9; $x++)
                {
                    $fake_number = NULL;
                    $fake_number = str_pad($fake_number, strlen($input_phone), (string)$x);

                    if($input_phone === $fake_number)
                    {
                        return self::ERROR_PHONE_IS_INVALID;
                    }
                }

                $invalidNumbers = array('5147573945', '4384051009', '4388888370');

                if (in_array($input_phone, $invalidNumbers)) {
                    return self::ERROR_PHONE_IS_INVALID;
                }

                return self::NO_ERROR;
                break;
            case self::ERROR_STRING_IS_EMPTY:
                return self::ERROR_PHONE_IS_EMPTY;
                break;
            case self::ERROR_STRING_IS_TOO_LONG:
                return self::ERROR_PHONE_IS_TOO_LONG;
                break; 
            case self::ERROR_STRING_IS_TOO_SHORT:
                return self::ERROR_PHONE_IS_TOO_SHORT;
                break;
            default:
                return $input_phone_validity;     
        }
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $email
    */
    public static function isEmailInputValid(&$email)
    {
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        
        if(empty($email))
            return self::ERROR_EMAIL_IS_EMPTY;
            
        if($email === FALSE)
        {
            $email = '';
            return self::ERROR_EMAIL_IS_INVALID;        
        }
    
        if(@$email{(self::MIN_LEN_EMAIL - 1)} === '')
            return self::ERROR_EMAIL_IS_TOO_SHORT;
        
        if(!(@$email{self::MAX_LEN_EMAIL} === ''))
            return self::ERROR_EMAIL_IS_TOO_LONG;
      
        if(filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE)    
            return self::ERROR_EMAIL_IS_INVALID; 
     
        return self::NO_ERROR;    
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $id
    * @return mixed
    */
    public static function isIdInputValid(&$id)
    {
        if(($id = filter_var($id, FILTER_VALIDATE_INT, array('options' => array('min_range' => 1)))) === FALSE)
        {
            $id = 0;
            return self::ERROR_ID_IS_INVALID;
        }
        
        return self::NO_ERROR;
    }
    
    /***
    * put your comment there...
    * 
    * @param mixed $input_value
    * @param mixed $minimum_value
    * @param mixed $maximum_value
    */
    public static function isIntegerInputValid(&$input_value, $minimum_value = self::MIN_INT_VAL, $maximum_value = self::MAX_INT_VAL)
    {
        if(empty($input_value))
            return self::ERROR_INTEGER_IS_EMPTY;
        elseif(!(($input_value = filter_var($input_value, FILTER_VALIDATE_FLOAT, array('options' => array('min_range' => $minimum_value, 'max_range' => $maximum_value)))) === FALSE))   
            return self::NO_ERROR;
        
        $input_value = NULL;
        return self::ERROR_INTEGER_IS_INVALID;
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $posted_images
    */
    public static function areJPEGImagesInputValid(&$posted_images)
{
    $image_number = 0;
    $orientation_errors = false;

    foreach($posted_images as $image_name => &$image_details)
    {
        list($_width, $_height) = getimagesize($image_details['tmp_name']);
        $image_dimension = array('width' => $_width, 'height' => $_height);
        $image_orientation = self::getImageOrientation($image_dimension);

        if($image_details['error'] === UPLOAD_ERR_INI_SIZE || ($image_details['size'] > \Core\Utils\Image::MAX_SIZE_IMAGE_IN_BITES))
        {
            $posted_images = $image_details['name'];
            return self::ERROR_IMAGE_SIZE_IS_TOO_BIG;
        }
        elseif($image_details['error'] === UPLOAD_ERR_NO_FILE || $image_details['size'] === 0)
        {
            unset($posted_images[$image_name]);
            continue;
        }
        elseif($image_details['error'] !== UPLOAD_ERR_OK)
        {
            $posted_images = (string)$image_details['error'];
            return self::ERROR_IMAGE_ERROR_UNHANDLED;
        }
        elseif($image_details['type'] !== 'image/jpeg')
        {
            $posted_images = $image_details['name'];
            return self::ERROR_IMAGE_TYPE_IS_INVALID;
        }
        elseif(\Core\Utils\Image::resampleJPEGImage($image_details['tmp_name'], $image_details['tmp_name']) !== TRUE)
        {
            $posted_images = $image_details['name'];
            return self::ERROR_IMAGE_RESAMPLING_FAILED;
        }
        elseif(self::getOrientationDimensionErrors($image_dimension, $image_orientation))
        {
            return self::getOrientationDimensionErrors($image_dimension, $image_orientation);
        }
        else
        {
            $image_details['number'] = $image_number++;
            $image_details['orientation'] = \Core\Utils\Image::getImageOrientation($image_details['tmp_name']);
        }
    }

    if(empty($posted_images))
        $posted_images = NULL;

    return self::NO_ERROR;;
}

    /**
     * @param $posted_files
     * @return string
     */
    public static function areFileInputValid(&$posted_files, $allowedTypes = array(), $allowedExt = array())
    {
        if (empty($allowedExt) || !is_array($allowedExt)) {
            $allowedExt = array(
                'jpg', 'jpeg', 'jpe', 'gif', 'png', 'bmp'
            );
        }
        if (empty($allowedTypes) || !is_array($allowedTypes)) {
            $allowedTypes = array(
                'image/jpg', 'image/jpeg', 'image/jpe', 'image/gif', 'image/png', 'image/bmp'
            );
        }
        $file_number = 0;
        foreach($posted_files as $i => &$file_details)
        {
            $namePats = explode('.', $file_details['name']);
            $ext = strtolower(end($namePats));
            if($file_details['error'] === UPLOAD_ERR_INI_SIZE || ($file_details['size'] > \Core\Utils\Image::MAX_SIZE_IMAGE_IN_BITES))
            {
                $posted_images = $file_details['name'];
                return self::ERROR_IMAGE_SIZE_IS_TOO_BIG;
            }
            elseif($file_details['error'] === UPLOAD_ERR_NO_FILE || $file_details['size'] === 0)
            {
                unset($posted_images[$i]);
                continue;
            }
            elseif($file_details['error'] !== UPLOAD_ERR_OK)
            {
                $posted_images = (string)$file_details['error'];
                return self::ERROR_IMAGE_ERROR_UNHANDLED;
            }
            elseif(!in_array($file_details['type'], $allowedTypes))
            {
                $posted_images = $file_details['name'];
                return self::ERROR_IMAGE_TYPE_IS_INVALID;
            }
            elseif(!in_array($ext, $allowedExt))
            {
                $posted_images = $file_details['name'];
                return self::ERROR_IMAGE_TYPE_IS_INVALID;
            }
            $file_details['ext'] = $ext;
        }

        if(empty($posted_images))
            $posted_images = NULL;

        return self::NO_ERROR;
    }
    
    /** 
    * Get Image Orientation
    * 
    * @param array image_dimension
    *
    * @returns number
    */
    public static function getImageOrientation( $image_dimension ){
        if( $image_dimension['width'] > $image_dimension['height'] ){
            return self::AD_IMAGE_LANDSCAPE;
        } elseif( $image_dimension['width'] < $image_dimension['height'] ){
            return self::AD_IMAGE_PORTRAIT;
        } else {
            return self::AD_IMAGE_SQUARED;
        }
    }
    
    /** 
    * Get Image Orientation
    * 
    * @param array image_dimension
    *
    * @returns number
    */
    public static function getOrientationDimensionErrors( $image_dimension, $orientation ){
        if( $orientation == self::AD_IMAGE_LANDSCAPE ){
            if( $image_dimension['width'] < self::AD_IMAGE_MINWIDTH_LANDSCAPE || $image_dimension['height'] < self::AD_IMAGE_MINHEIGHT_LANDSCAPE ){
                return array('dimention' => 'error', 'msg' => '[ERROR-IMAGE-DIMENSION-LANDSCAPE]');
            }
        } elseif( $orientation == self::AD_IMAGE_PORTRAIT ){
            if( $image_dimension['width'] < self::AD_IMAGE_MINWIDTH_PORTRAIT || $image_dimension['height'] < self::AD_IMAGE_MINHEIGHT_PORTRAIT ){
                return array('dimention' => 'error', 'msg' => '[ERROR-IMAGE-DIMENSION-PORTRAIT]');
            }
        } elseif( $orientation == self::AD_IMAGE_SQUARED ){
            if( $image_dimension['width'] < self::AD_IMAGE_MINWIDTH_SQUARED | $image_dimension['height'] < self::AD_IMAGE_MINHEIGHT_SQUARED ){
                return array('dimention' => 'error', 'msg' => '[ERROR-IMAGE-DIMENSION-SQUARED]');
            }
        }
        
        return false;
    }
    
    /** 
    * Encryption Procedure
    * 
    * @param   mixed    msg      message/data
    * @param   string   k        encryption key
    * @param   boolean  base64   base64 encode result
    *
    * @returns  string   iv+ciphertext+mac or boolean  false on error
    */
    public static function encrypt( $msg, $k, $base64 = false )
    {
        // open cipher module (do not change cipher/mode)
        if ( ! $td = \mcrypt_module_open('rijndael-256', '', 'ctr', ''))
            return false;
        
        // serialize
        $msg = serialize($msg);
        
        // create iv                             
        $iv  = \mcrypt_create_iv(32, MCRYPT_RAND);           
        
        // initialize buffers
        if ( \mcrypt_generic_init($td, $k, $iv) !== 0 )      
            return false;
            
        // encrypt
        $msg  = \mcrypt_generic($td, $msg); 
        
        // prepend iv                 
        $msg  = $iv . $msg;
        
        // create mac                                 
        $mac  = self::pbkdf2($msg, $k, 1000, 32);
        
        // append mac           
        $msg .= $mac;                                       
        
        // clear buffers
        \mcrypt_generic_deinit($td); 
        
        // close cipher module                        
       \mcrypt_module_close($td);                           
        
        // base64 encode?
        if ( $base64 ) $msg = base64_encode($msg);          
        
        // return iv+ciphertext+mac
        return $msg;                                     
    }

    /** 
    * Decryption Procedure
    * 
    * @param   string   msg      output from encrypt()
    * @param   string   k        encryption key
    * @param   boolean  base64   base64 decode msg
    *
    * @returns  string   original message/data or boolean  false on error
    */
    public static function decrypt( $msg, $k, $base64 = false )
    {
        // base64 decode?
        if ( $base64 ) $msg = base64_decode($msg);            

        // open cipher module (do not change cipher/mode)
        if ( ! $td = mcrypt_module_open('rijndael-256', '', 'ctr', '') )
            return false;
        
        // extract iv
        $iv  = substr($msg, 0, 32);
        
        // mac offset                            
        $mo  = strlen($msg) - 32;
        
        // extract mac                            
        $em  = substr($msg, $mo);
        
        // extract ciphertext                           
        $msg = substr($msg, 32, strlen($msg)-64);
        
        // create mac            
        $mac = self::pbkdf2($iv . $msg, $k, 1000, 32);        
        
        // authenticate mac
        if ( $em !== $mac )                                    
            return false;

        // initialize buffers    
        if ( mcrypt_generic_init($td, $k, $iv) !== 0 )        
            return false;
        
        // decrypt
        $msg = mdecrypt_generic($td, $msg); 
        
        // unserialize                   
        $msg = unserialize($msg);                            
        
        // clear buffers
        mcrypt_generic_deinit($td);
        
        // close cipher module                            
        mcrypt_module_close($td);                            
        
        // return original msg
        return $msg;                                        
    }
    
    /***
    * put your comment there...
    * 
    * @param mixed $text
    * @param mixed $as_references
    */
    public static function htmlEncodeText($text, $as_references = FALSE)
    {
        //return "111";
        $text = str_ireplace('&#39;', "'", $text);
        $text = str_ireplace('&#34;', '"', $text);

        if(self::$html_translation_table === NULL)
            self::$html_translation_table = get_html_translation_table(HTML_ENTITIES, NULL, 'ISO-8859-1');
         
        unset(self::$html_translation_table['&']);

        if($as_references === TRUE)
        {
            foreach (self::$html_translation_table as $character => $encoded_value)
                self::$html_translation_table[$character]= '&#'. ord($character) . ';';    
        }

        $translatedCharters = strtr($text, self::$html_translation_table);
        $translatedCharters = preg_replace('/\&#(\d+);/', ' ', $translatedCharters);

        $translatedCharters = str_ireplace("'", '&#39;', $translatedCharters);
        $translatedCharters = str_ireplace('"', '&#34;', $translatedCharters);

        return $translatedCharters;
    }
    
    /***
    * put your comment there...
    * 
    */
    public static function outputCaptcha()
    {
        for(self::LEN_CAPTCHA, $_SESSION[self::CAPTCHA_SESSION_IDENTIFIER] = ''; strlen($_SESSION[self::CAPTCHA_SESSION_IDENTIFIER]) < self::LEN_CAPTCHA; $_SESSION[self::CAPTCHA_SESSION_IDENTIFIER] .= chr(!rand(0, 2) ? rand(48, 57) : (!rand(0, 1) ? rand(65, 90) : rand(97, 122))));

        // Create a 80x25 image
        $im = \imagecreatetruecolor(80, 25);
        $black = imagecolorallocate($im, 0, 0, 0);
        $white = imagecolorallocate($im, 255, 255, 255);

        // Set the background to be black
        imagefilledrectangle($im, 0, 0, 79, 79, $black);

        imageline($im, rand(0, 80), 0, rand(0, 80), 25, imagecolorallocate($im, 255, 0, 255));
        imageline($im, 0, rand(0, 25), 80, rand(0, 25), imagecolorallocate($im, 255, 204, 255));

        // Write it
        imagettftext($im, 15, 0, 5, 20, $white, \Core\Router::getCorePath() . 'assets/fonts/verdana.TTF', $_SESSION[self::CAPTCHA_SESSION_IDENTIFIER]);

        // Output to browser
        header("Content-Type: image/gif", TRUE);
        header('Expires: Thu, 19 Nov 1981 08:52:00 GMT', TRUE);
        header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0', TRUE);
        header('Pragma: no-cache', TRUE);

        imagegif($im);
        imagedestroy($im);
    }
    
    /***
    * put your comment there...
    * 
    * @param mixed $captcha_code
    */
    public static function isInputCaptchaValid(&$captcha_code)
    {
        $string_validity = self::isStringInputValid($captcha_code, self::LEN_CAPTCHA, self::LEN_CAPTCHA);
        
        switch($string_validity)
        {
            case self::NO_ERROR:
                break;
            case self::ERROR_STRING_IS_EMPTY:
                return self::ERROR_CAPTCHA_IS_EMPTY;
                break;
            case self::ERROR_STRING_IS_TOO_SHORT:
                return self::ERROR_CAPTCHA_IS_TOO_SHORT;
                break;
            case self::ERROR_STRING_IS_TOO_LONG:
                return self::ERROR_CAPTCHA_IS_TOO_LONG;
                break;
            default:
                return self::ERROR_UNKNOWN;  
        }

        if(!(strcasecmp($captcha_code, @$_SESSION[self::CAPTCHA_SESSION_IDENTIFIER]) === 0))
            return self::ERROR_CAPTCHA_IS_INVALID;
   
        return self::NO_ERROR;
    }
}
