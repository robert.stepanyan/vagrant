<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that are related to data retrieval.
 */

Namespace Core\Utils;

final class Data{
     
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}
    
    /**
    * Prevent the class from being instantiated
    * 
    * @returns Nothing
    */
    private function __construct()
    {} 
    
    /**
    * This function automates the process of producing the cache key(s) for the find by sql sub-routine
    * 
    * @param string The SQL statement being runned by the find by sql sub-routine
    * @param string The name of the configuration that houses the db information 
    * @param bool Whether the SQL is to be runned on a master or a slave
    * @param bool Whether or not the SQL statement is to retrieve a count... in which case two keys will be returned
    * 
    * @return mixed A string containing the cache key of the SQL statement or and array containing:
    *           - A string containing the cache key of the SQL statement @ INDEX 0
    *           - A string containing the cache key of COUNT of the SQL statement @ INDEX 1
    */
    private static function getFindBySQLCacheKeys($config_name, $sql, $fetch_from_master, $with_count = FALSE) 
    {
        $config_name = strtolower($config_name);
         
        // If no count is involved... simply return a string with the cache key of the SQL statement
        if($with_count === FALSE)
            return  'sql.' . $config_name . '.' . ($fetch_from_master ? 'master' : 'slave') . '.' . md5($sql);
            
        /**
        * Otheriwse return an array which contains:
        *           - A string containing the cache key of the SQL statement @ INDEX 0
        *           - A string containing the cache key of COUNT of the SQL statement @ INDEX 1
        */
        return array('sql.' . $config_name . '.' . ($fetch_from_master ? 'master' : 'slave') . '.' . md5($sql), 'count.' . 'sql.' . $config_name . '.' . ($fetch_from_master ? 'master' : 'slave') . '.' . md5(preg_replace(array('/LIMIT\s[0-9]*,(\s?)[0-9]*/i', '/LIMIT\s[0-9]*/i'), '', $sql)));
    }
    
    public static function updateCacheBySql($config_name, $sql, $fetch_from_master = FALSE, $with_count = FALSE) {
        
        $cache_key = self::getFindBySQLCacheKeys($config_name, $sql, $fetch_from_master, FALSE);
        \Core\Cache::delete($cache_key);
        return false; 
    }
    
    
    
    /**
    * Entry point of every single query in the framework
    * 
    * @param string The FULL SQL to execute  
    * @param string The name of the configuration that houses the db information 
    * @param bool Weather or not to execute the query on the master db server
    * @param bool Whether or not the query should return the number of rows found
    * @param array The different cache settings, supported values are:
    *                   - enable: Whether of not the results should be cached
    *                   - cluster: The name of the cache cluster where we'd like to cache the results 
    *                   - server: The name of the virtual server where we'd like to cache the results
    *                   - expire: The number of seconds to store the results in cache for
    *                   - lock: The number of seconds to lock other threads from accessing a particular piece of data
    *                           when the data is not found in cache and it needs to be retrieved
    *
    * @return array The represents the data retrieved from the data store. Note that whenever the count is not required, 
    * a single dimension relational array is returned containning the field names as keys, and their values as values. In
    * the case when the count is required, a two dimensional array will be returned. This array will contain two elements:
    * 
    *   1.- 'recordset': a single dimension relational array is returned containning the field names as keys, and their 
    *       values as values
    *   2.- 'count': an integer representing the total number of rows found  
    */
    public static function findBySQL($config_name, $sql, $fetch_from_master = FALSE, $with_count = FALSE, $cache_params = NULL)
    {
		
        /*
         We'll setup two pointers that will define the course of action:
         
            1.- Are we being requested to retrieve the record count?
            2.- Is cache enabled?
            
         Keep in mind, cache can be disabled in the following ways:
         
            1.- By setting 'USE_CACHE' to FALSE in the framework. This will disable it on all projects
                that use the framework, for a given user
            2.- By passing '$cache_params['enable']' as FALSE wherever $cache_parameters are supported
        */
        $x = ($with_count === TRUE ? 1 : 0);
        $y = (($cache_params = \Core\Cache::validateAndMergeParams($cache_params)) === FALSE ? 0 : 2);
		
        switch(($x + $y))
        {
            // ******************************************************************************************************
            // We want the COUNT and we want it with NO CACHE                                                       *
            // ******************************************************************************************************
            case 1:
                 // IFF: we retrieve the data, and the count...
                if( ($data = \Core\DBConn::select($config_name, str_ireplace('SELECT', 'SELECT SQL_CALC_FOUND_ROWS', $sql), $fetch_from_master)) && 
                    ($count = \Core\DBConn::select($config_name, 'select FOUND_ROWS()', $fetch_from_master)))
                    return array('recordset' => $data, 'count' => (int)array_pop($count[0]));

                return FALSE;
                break;
            // ******************************************************************************************************
            // We want NO COUNT and we want it WITH CACHE                                                           *
            // ******************************************************************************************************
            case 2:
				
				if(($data = \Core\Cache::get(($cache_key = self::getFindBySQLCacheKeys($config_name, $sql, $fetch_from_master, FALSE)))))
                     return $data;
                elseif( \Core\Cache::getDataAccessLock($cache_key, $cache_params['lock']) && 
                        ($data = \Core\DBConn::select($config_name, $sql, $fetch_from_master)) &&
                        \Core\Cache::save($cache_key, $data, $cache_params['expire']))
                    return $data;        
                
                return FALSE;
                break;
            // ******************************************************************************************************
            // We want the COUNT and we want it WITH CACHE                                                          *
            // ******************************************************************************************************
            case 3:
                
                /*
                    Working with a recordset that requires a count and Memcached implies dealing with 4 posibilities.
                    Talking to Memcached returns:
                    
                        1.- Both the recordset and the count: do nothing simply return both values
                        2.- Only the count is found: retrieve the recordset then return both values
                        3.- Only the recordset is found: retrieve the count then return both values
                        4.- None of the values are found: retrieve both values then return them
                        
                   The two line below cope with case number 1: both values are found... simply return them 
                */
                $data = \Core\Cache::getMulti(($cache_keys = self::getFindBySQLCacheKeys($config_name, $sql, $fetch_from_master, TRUE)), NULL);
                 
                switch(($a = isset($data[$cache_keys[0]]) ? 1 : 0) + ($b = isset($data[$cache_keys[1]]) ? 2 : 0))
                {
                    case 3:     // Both the recordset and the count: do nothing simply return both values
                        return array('recordset' => $data[$cache_keys[0]], 'count' => $data[$cache_keys[1]]);
                        break;
                    case 2:     // Only the count is found: retrieve the recordset then return both values
                        if( \Core\Cache::getDataAccessLock($cache_keys[0] , $cache_params['lock']) && ($data[$cache_keys[0]] = \Core\DBConn::select($config_name, $sql, $fetch_from_master)))
                        {
                            \Core\Cache::save($cache_keys[0], $data[$cache_keys[0]], $cache_params['expire']);
                            return array('recordset' => $data[$cache_keys[0]], 'count' => $data[$cache_keys[1]]); 
                        }
                        
                        break;
                    case 1:     // Only the recordset is found: retrieve the count then return both values 
                        if( \Core\Cache::getDataAccessLock($cache_keys[1] , $cache_params['lock']) 
                            && 
                            ($data[$cache_keys[1]] = \Core\DBConn::selectRow($config_name, 'SELECT count(*) ' . substr(($sql = preg_replace(array('/LIMIT\s[0-9]*,(\s?)[0-9]*/i', '/LIMIT\s[0-9]*/i'), '', $sql)), stripos($sql, 'from')), $fetch_from_master)))
                        {
                            \Core\Cache::save($cache_keys[1], ($data[$cache_keys[1]] = (int)$data[$cache_keys[1]][0]), $cache_params['expire']);
                            return array('recordset' => $data[$cache_keys[0]], 'count' => $data[$cache_keys[1]]); 
                        }
                        
                        break;
                    default:    // None of the values are found: retrieve both values then return them 
                        if( \Core\Cache::getDataAccessLock(implode('+', $cache_keys) , $cache_params['lock']) 
                            && 
                            ($data = \Core\DBConn::select($config_name, str_ireplace('SELECT', 'SELECT SQL_CALC_FOUND_ROWS', $sql), $fetch_from_master)) !== FALSE 
                            && 
                            ($count = \Core\DBConn::select($config_name, 'select FOUND_ROWS()', $fetch_from_master)) !== FALSE)
                        {
                            \Core\Cache::saveMulti(array($cache_keys[0] => $data, $cache_keys[1] => ($count = (int)array_pop($count[0]))), $cache_params['expire']);
                            return array('recordset' => $data, 'count' => $count);
                        }
                         
                        break;
                }
                
                return FALSE;
                break;
            // ******************************************************************************************************
            // We want NO COUNT and we want it with NO CACHE: Fetch the data from the database straight up!         *
            // ******************************************************************************************************
            default:
                return \Core\DBConn::select($config_name, $sql, $fetch_from_master);
                break;  
        }
        
        return FALSE;
    }
                                                                                                                                         
    /**
    * Reads url into a string. This function is a wrapper to file_get_contents and it uses cache to access data
    * exclusivelly, as well as to store the file_get_contents results if any. NOTE: this methods makes a GET
    * request by default. and has a default timeout of 0.05 seconds
    *
    * @param string $url The Uniform Ressource Locator to be the used as data source
    * @param array $context_params an array compatible with stream_context_create to be able to create the request's context
    * @param array $cache_params Valid en params are, PASS ALL PARAMS even as NULL if not present to prevent warnings :-):
    * 
    *               - enable: bool TRUE if you wish to cache the results, FALSE otherwise (default)
    *               - cluster: string The name of the Memcache cluster where you wish to save the results; NULL is default
    *               - expire: integer The number of seconds from now to expire the results after
    *               - lock: the amount of seconds we should prevent other threats from accessing the data source when cache is to be reset  
    *               - reset: bool If memcache is enabled it forces the retrival of the data
    *  
    * @return string The results from file_get_contents, FALSE otherwise
    */
    public static function getURLData($url, $context_params = NULL, $cache_params = NULL)
    { 
        // If cache is not enabled system wise or we don't want to cache the current call... simply return the url contents
        if(($cache_params = \Core\Cache::validateAndMergeParams($cache_params)) === FALSE)
            return @file_get_contents($url, FALSE, stream_context_create((array_replace_recursive(array('http' => array('header' => 'Connection: close', 'method' => 'GET', 'user_agent' => 'Viper HTTP Data Extractor v1.0.0', 'timeout' => 2)), (is_array($context_params) ? $context_params : array())))));

        $cache_key = md5($url);
        
        // If we don't want to reset the data and memcache is enabled, a valid Memcache intance is retrieved, and the data is already cached: return the cached data
        if( $cache_params['reset'] !== TRUE && ($data = \Core\Cache::get($cache_key)) !== FALSE)
            return $data;
            
        if(\Core\Cache::getDataAccessLock($cache_key) === FALSE || ($data = @file_get_contents($url, NULL, stream_context_create((array_replace_recursive(array('http' => array('header' => 'Connection: close', 'method' => 'GET', 'user_agent' => 'Viper HTTP Data Extractor v1.0.0', 'timeout' => 2)), (is_array($context_params) ? $context_params : array())))))) === FALSE)
            return FALSE;

        \Core\Cache::save($cache_key, $data, $cache_params['expire']);

        // Remove the exclusive data access lock
        \Core\Cache::deleteDataAccessLock($cache_key);

        // return the data
        return $data;
    }
}