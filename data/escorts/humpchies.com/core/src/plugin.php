<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file provides support for plugins, i.e. small reusable
 * utilitarian code that accomplishes one task only.
 */

Namespace Core;

abstract class Plugin{
   
    // Members
    // ======= 
	protected $data             = NULL;                 // This is where we store all data related to the plugin
    
    // Constants
    // =========
	const DEFAULT_VIEW_NAME     = 'default';            // The name of the default view of the plugin
    
    /**
    * @desc Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}
    
	/**
	* Render the plugin to the client's screen
	*
	* @param string The name of the view to use
	* @param bool Whether or not we should look for the plugin's tpl in the plugins's view directory
    * or in the site's view directory 
	*/
	public function render($view_name = NULL, $mode = \Core\View::DEVICE_MODE)
	{
        @extract($this->data);

        require \Core\View::getFullyQualifiedViewPath(($view_name === NULL ? get_called_class() . '::' . self::DEFAULT_VIEW_NAME : $view_name), $mode); 
	}
}