<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file uses sequential code to analyze the URL of the
 * current request in order to retrieve the corresponding controller action
 * to fullfil the request.
 */

Namespace Core;

final class Router
{
    // PRIVATE MEMBERS
    // ===============
    private static $parsed_url                         = NULL;         // This is the container that holds our interpretation of the url
    private static $modules_path                       = NULL;         // The absolute path to the Viper modules
    private static $plugins_path                       = NULL;         // The absolute path to the Viper plugins
    private static $gui_path                           = NULL;         // The absolute path to the Viper GUI projects
    private static $site_path                          = NULL;         // The absolute path to the current application using the Viper framework
    private static $core_path                          = NULL;         // The absolute path to the core of the Viper framework 
    
    /**
    * Block access to the constructor... this can only be used by its static methods
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object deserialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object serialization
    */
    private function __sleep()
    {}
    
    /**
    * Grabs the remaining of the URI path after parsing the current URL, 
    * breaks it into keys and values, then appends the result to the $_GET in order
    * to facilitate their usage down the road as GET parameters.
    * 
    * @returns TRUE if succesful, false otherwise
    */
    public static function addPathToGet($path)
    {
        // Bail out if no path is provided
        if($path === NULL)
            return FALSE;
            
        /**
        * Exit the function if after parsing the current URI there is no path left...
        * i.e. there is nothing to add to the $_GET variable 
        */
        if(in_array($path, array('', '/')))
            return FALSE;
        
        // Loop through each element past the controller and action in the path
        // grabing two elements at the time... the first is the key and the second
        // is the value to be put into the GET
        for($i = 0; $i < count(($path_components = explode('/', trim($path, '/')))); $i += 2 )
        {
            /*
            * IFF: the value associated with the key is not empty...
            * Then parse the key/value like elements into the current GET
            * NOTA BENNE: This will OVERRIDE existing values in the current
            * GET if they match keys... so be careful!
            */
            if(isset($path_components[$i + 1]))
                $_GET[$path_components[$i]] = $path_components[$i + 1];
        }
        
        return TRUE;
    }
    
    /**
    * Turns a camel case string into a undescore case. For example: 'MyCheeseProgramRunsOnCoffee' will 
    * end up being 'my_cheese_program_runs_on_coffee'. This function is useful to build file paths based
    * on class or module names.
    * 
    * @param string $camel_case_string The camel case string we wish to convert to underscore
    */
    public static function camelCaseToUnderscore($camel_case_string)    
    {
        /**
        * 1.- Find all Uppper case letters
        * 2.- Replace all upper case letters by an underscore followed by the upper case letter itself
        * 3.- Lower case the string
        * 4.- Remove the trailing underscore
        */
        return strtolower(preg_replace_callback(    '/[a-z][A-Z]/', 
                                                    create_function('$matches',
                                                                    'return (substr($matches[0], 0, 1) . "_" . substr($matches[0], 1, 1));'), 
                                                    $camel_case_string));     
    }    
    
    /**
    * This function is the core of this class. Many things depend on the results of this function. Please
    * be careful when making changes here; they can lead to unwanted results. Background information:
    * 
    *   - Historically speaking, our software has always supported controllers and actions through routes 
    *   based on the structure and values found in the request URI. Up until now, there have always been
    *   two ways to identify a route: by explicit controller/action and by keyword. So far we have been 
    *   using two array routes : explicit and mapped. At this point, the usage of explicit routes has 
    *   become useless because the Viper framework uses namespaces. Therefore, from this point on all, 
    *   routes are mapped. To be backwards compatible two schemas are supported: controller/action and 
    *   keyword. For example:
    * 
    *       Controller/Action example:      http://www.some_site.com/controller/action
    *       Keyword example:                http://www.some_site.com/keyword
    *       Keywork example:                http://www.some_site.com/keyword/paramater1/value1/paramater2/value2
    *       Controller/Action example:      http://www.some_site.com/controller/action/paramater1/value1/paramater2/value2
    * 
    *   @returns FLASE on failure. An object with the following fields on success:
    *       
    *       - public_controller_name:       A string representing the name of the controller associated with the current URI as exposed to the world
    *       - public_action_name:           A string representing the name of the action associated with the current URI as exposed to the world
    *       - controller_name               A string representing the fully qualified class name of the actual controller to be used to fullfil the current request
    *       - action_name                   A string representing the name of the actual action to be used to fullfil the current request
    *       - path                          A string representing the left over path after the public controller and action have been removed. Note that this path later on is used as
    *                                       an aditional source of $_GET paramaters
    */
    public static function getParsedURL()
    {   
        // If the URL has already been parsed... simply return the previous results and bail out.
        if(self::$parsed_url !== NULL)     
            return self::$parsed_url;

        // Give plublic_action_name a default value of NULL (i.e. no action passed)
        self::$parsed_url['public_action_name'] = NULL;

        /**
        * Moving forward, if the locale found seems to be SUPPORTED by the current
        * application:
        * 
        * 1.- Remove the locale from the URI... 
        * 2.- Then Parse the URI into... controller, action and complement
        */
        if(($session_locale = \Core\Utils\Session::getLocale()) !== FALSE)
            $parsed_request_uri = parse_url(str_replace('/' . $session_locale , '', $_SERVER['REQUEST_URI']),  PHP_URL_PATH);
        else
            $parsed_request_uri = parse_url($_SERVER['REQUEST_URI'],  PHP_URL_PATH);
        
        /**
        * 1.- Grab the request URI
        * 2.- Trim any leading and trailing slashes from the request URI
        * 3.- Break apart the request URI at the slashes for a maximum of 3 components: controller, action and the remainder
        * 4.- Loop through each one of the elements captured in step 3, then grab and set the public_controller_name and
        *     the public_action_name 
        */
        foreach(($potential_route_elements = explode('/', trim($parsed_request_uri, '/'), 3)) as $potential_route_element)
        {
            if(@self::$parsed_url['public_controller_name'] === NULL)    
                self::$parsed_url['public_controller_name'] = $potential_route_element;
            else
            {
                self::$parsed_url['public_action_name'] = $potential_route_element; 
                break;
            }   
        }
        
        // Grab and store the the remainder of the URI after taking the controller and action from it    
        self::$parsed_url['path'] = @$potential_route_elements[2];

        /**
        * If the the controller/action or keyword are nowhere to be found in the routes of the current application 
        * and no 404 route is found... throw an error and bail out 
        */
        if((self::$parsed_url['route'] = \Core\Utils\Config::getConfigValue('\Application\Routes::' . self::$parsed_url['public_controller_name'] . (@self::$parsed_url['public_action_name'] === NULL ? '' : '/') . self::$parsed_url['public_action_name'], FALSE)) === NULL
            && 
            (self::$parsed_url['route'] = \Core\Utils\Config::getConfigValue('\Application\Routes::404', FALSE)) === NULL)
            exit('[ x ] Undefined route, and no route has been defined to handle 404 errors');

        list(self::$parsed_url['route'], self::$parsed_url['layout_settings']) = self::$parsed_url['route']; 
        
        // Parse the fully qualified name of the actual controller/action into controller_name and action_name 
        list(self::$parsed_url['controller_name'], self::$parsed_url['action_name']) =  explode('::', self::$parsed_url['route'] , 2);

        // Parse the results of this function into a standard object, then store the results of this function into a singleton and 
        return self::$parsed_url = (object)self::$parsed_url;
    }
   
    /**
    * This function associates a class name with a the absolute path of the file containing that class. 
    * Note that no validation is done on either the existence of the file or the existance of the class
    * in that file. This function simply applies an algorithm to to map a class to the what should be 
    * its corresponding file based on its name. 
    * 
    * Background
    * ========== 
    * 
    * Classes can be found in the Framework, Modules, Websites and Plugins. Clases found in the 
    * Framework can only exist in its 'lib' directory.
    * 
    * Classes in Modules and Websites can be of three types: Controllers, Models or stand-alone. 
    * Controllers use the suffix 'Controller' to identify themselves as such. Models use the suffix 
    * 'Models' to identify themselves as such. Pluginsuse use the suffix 'Plugins' to identify 
    * themselves as such. Stand-alone are all classes that do not use neither one
    * of the previously mentioned suffixes. For example:
    * 
    *   1.- Class name '\Core\Utils\Security' makes reference to stand-alone class Security in the framework
    *   2.- Class name '\Application\Security' makes reference to stand-alone class Security in the current application
    *   3.- Class name '\Application\SecurityController' makes reference to Security Controller in the current application
    *   4.- Class name '\Dental\SecurityController' makes reference to Security Controller in the dental module
    *   5.- Class name '\Application\SecurityModel' makes reference to Security Model in the current application
    *   4.- Class name '\Dental\SecurityModel' makes reference to Security Model in the dental module 
    * 
    * Controllers are stored in the 'controllers' directory of their module or the application. Models are 
    * stored in the 'models' directory of their module or the application. Stand-alone classes are stored in
    * the 'lib' directory of their module or the application. Plugin classes are stored in the lib directory
    * of their respective plugin directory.
    * 
    * @param string $class_name The name of the class
    * @returns string The absolute path of the file containning the class
    */
    public static function getClassPath($class_name)
    { 
        $path = explode('\\', $class_name);
        $namespace = strtolower(array_shift($path));
        $class = array_pop($path);
        
        if($namespace === 'core')
            $path = self::getCorePath() . 'src/' . strtolower((($path = implode('/', $path)) === '' ? '' : $path . '/' ));
        elseif($namespace === 'application')
            $path = self::getApplicationPath() . strtolower((($path = implode('/', $path)) === '' ? '' : $path . '/' )) . 'src/';
        elseif($namespace === 'modules')
            $path = self::getModulesPath() . strtolower((($path = implode('/', $path)) === '' ? '' : $path . '/' )) . 'src/';
        elseif($namespace === 'plugins'){
            $path = self::getPluginsPath()  . strtolower((($path = implode('/', $path)) === '' ? '' : $path . '/' ) . self::camelCaseToUnderscore($class)) . '/src/';
            $class = 'plugin';
        }           
        else
            exit("[ x ] Invalid Namespace: $class_name");
                                                       
        if($class !== 'plugin')
        {
            $class = explode('_', self::camelCaseToUnderscore($class));
        
            if((($class_suffix = array_pop($class)) === 'controller' || $class_suffix === 'model') && @$class[0] !== NULL)
                $path .= $class_suffix . 's/'; 
            else
                array_push($class, $class_suffix);
                
            $class = implode('_', $class);     
        }
            
        return "$path$class.php";
    }
    
    /**
    * Returns the absolut path to the root of the modules directory
    * 
    * @returns string representing the absolut path to the root of the modules directory 
    */
    public static function getModulesPath()
    {
        // If the path has previously been retrieved, simply return it
        if(self::$modules_path !== NULL)
            return self::$modules_path;
        
        /*
         Otherwise:
         
            1.- Build the path by gluing the install path plus 'modules/'
            2.- Store & return the path 
        */        
        return (self::$modules_path = INSTALL_PATH . 'modules/');
    }
    
    /**
    * Returns the absolut path to the root of the plugins directory
    * 
    * @returns string representing the absolut path to the root of the plugins directory 
    */
    public static function getPluginsPath()
    {
        // If the path has previously been retrieved, simply return it
        if(self::$plugins_path !== NULL)
            return self::$plugins_path;
        
        /*
         Otherwise:
         
            1.- Build the path by gluing the install path plus 'plugins/'
            2.- Store & return the path 
        */        
        return (self::$plugins_path = INSTALL_PATH . 'plugins/');
    }
    
    /**
    * Returns the absolut path to the root of the site calling the framework
    * 
    * @returns string representing the absolut path to the root of the site calling the framework 
    */
    public static function getApplicationPath()
    {
        // If the path has previously been retrieved, simply return it
        if(self::$site_path !== NULL)    
            return self::$site_path;
                                                      
        /*
         Otherwise:
         
            1.- Build the path:
                a.- Grab the parent directory of the current document root
                b.- Append '/', at the end the path would be something like: /home/user/Viper_sites/some_site.tld/
            2.- Store & return the path 
        */           
        return (self::$site_path = dirname($_SERVER[(php_sapi_name() === 'cli' ? 'PWD' : 'DOCUMENT_ROOT')]) . '/');
    }
    
    /**
    * Returns the absolut path to the root of the core of the framework
    * 
    * @returns string representing the absolut path to the root of the core of the framework 
    */
    public static function getCorePath()
    {
        // If the path has previously been retrieved, simply return it
        if(self::$core_path !== NULL)    
            return self::$core_path;
        
        /*
         Otherwise:
         
            1.- Build the path:
                a.- Grab the installation root directory
                b.- Append 'core/', at the end the path would be something like: /home/user/Viper/core/
            2.- Store & return the path 
        */           
        return (self::$core_path = INSTALL_PATH . 'core/');   
    }
                                                                
    /**
    * Sets the header of the current request to be 404, changes the Controller and the Action to be those 
    * stored in the routes under the route 'not_found', then simply processes the route. Be careful to 
    * pay attention to the context within which you'd be calling this function. Keep an eye on what is 
    * in the URI, in the GET variable, and whether or not the 404 action needs login or not. 
    * 
    * @returns Nothing
    */
    public static function throw404()
    {
        if((self::$parsed_url->route = \Core\Utils\Config::getConfigValue('\Application\Routes::404', FALSE)) === NULL)
            exit('[ x ] No route has been defined to handle 404 errors');
            
        //var_dump(self::$parsed_url->route);

        @list(self::$parsed_url->route, self::$parsed_url->layout_settings) = self::$parsed_url->route; 
        
        // Parse the fully qualified name of the actual controller/action into controller_name and action_name 
        @list(self::$parsed_url->controller_name, self::$parsed_url->action_name) =  explode('::', self::$parsed_url->route, 2);
        
        // Simply process the route
        self::processRoute();                                
    }
    
    /**
    * This function makes sure we run the site from a single hostname. This function is useful to avoid 
    * search engine penalties due to same content on multiple hostnames.
    * 
    * @returns void
    */
    public static function redirectToCanonical()
    {
        // Let two versions of the robots and sitemap exist
        $currentRoute = self::getParsedURL()->route;
        $isRobotsOrSitemap = false;

        if (in_array(
            $currentRoute,
            [
                '\Modules\Humpchies\GenericController::robots',
                '\Modules\Humpchies\GenericController::xmlSitemap'
            ]
        )) {
            $isRobotsOrSitemap = true;
        }

        /**
         * Redirect:
         *
         *      1.- Non-HTTPs requests if applicable
         *      2.- Improper hostname requests
         */
        $forceHTTPS = \Core\Utils\Config::getConfigValue('\Application\Settings::force_https');
        $protocol = \Core\Utils\Device::getProtocol();
        $uri = trim($_SERVER['REQUEST_URI'], '/');

        if (false === empty($uri)) {
            $uri = "/$uri";
        }

        // Force HTTPS redirection
        if (true === $forceHTTPS && 'https://' !== $protocol && false === $isRobotsOrSitemap) {
            \Core\Controller::redirect(
                'https://' .
                $_SERVER['HTTP_HOST'] .
                $uri
            );
        }

        $hostname = \Core\Utils\Config::getConfigValue('\Application\Settings::hostname');

        if (null !== $hostname && $hostname !== $_SERVER['HTTP_HOST']) {
            \Core\Controller::redirect(
                $protocol .
                $hostname .
                $uri
            );
        }
    }

    /***
    * This function compresses the code sent to the client twice
    *
    * @param mixed $buffer is the output buffer
    * @returns a compressed version of the input $buffer
    */
    public static function preRender($buffer)
    {   // return $buffer;
        // Only compress items created with the framework for wich a layout has been defined
        if(@self::getParsedURL()->layout_settings === NULL)
            return $buffer;
  
        // Translate tokens in output...
        if(\Core\Utils\Config::getConfigValue('\Application\Settings::locales', FALSE) !== NULL)
            \Core\Utils\Text::translateBufferTokens($buffer, \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'), \Core\Utils\Session::getLocale(), \Core\Utils\Config::getConfigValue('\Application\Settings::locales_module', FALSE));

        // Remove tabs, carriage return, new lines
        $buffer = str_replace(array("\r", "\n", "\t"), ' ', $buffer);
        
        // Remove multiple spaces                                                                                                    
        $buffer = preg_replace("/\s+/", ' ', $buffer); 

        // Replace some special characters by their HTML entities                                                                                                                            
        $buffer = preg_replace('/&(?![a-z0-9]{2,8};|#[0-9]+;)/i', '&amp;', $buffer);
        
        // trailing and leading spaces from openning and closing tags                                                                                                                            
        $buffer = str_replace(array(' >', '< ', ' />', '> <', ' <', '> '), array('>', '<', '/>', '><', '<', '>'), $buffer);

        // Zip the output
        return ob_gzhandler($buffer,9);
    }
    
    /**
    * Declare the autoload function that is going to be used by the framework.
    *
    * @param $className - the fully qualified name of the class that needs to be loaded
    * @returns NULL
    */
    public static function autoload($className)
    {
        // Simply include the source code of the class...
        require_once(self::getClassPath($className));
    }
    
    /**
    * This function is the core of each and every single HTTP request that is processed via the Viper Framework.
    * The function executes the following steps:
    * 
    *   1.- Explicitly call the appropriate Controller/Action to fullfil the current request
    *   2.- Viper is data oriented. Therefore a Controller/Action might not have a view associated with it. If
    *       no layout is associated with the current Controller/Action simply bail out.
    *   3.- In the event that a layout is associated with the ControllerAction of step 4 then:
    *           a.- Append Google analytics code in the head of the current page
    *           b.- Render the header of the current page
    *           c.- Render the contents of the current page
    *           d.- Render the footer of the current page
    *           e.- Clear any resident variables passed between redirections
    *           f.- Terminate execution
    * 
    * @returns Nothing
    */
    public static function processRoute()
    {    
        // Get the name of the controller/action needed to fullfil the current request and execute it!
        if(is_callable(self::getParsedURL()->route) === FALSE)
        {
            self::throw404();
            return;
        } 
            
        if( call_user_func(self::getParsedURL()->route) === FALSE 
            ||
            @self::getParsedURL()->layout_settings === NULL
            || 
            @self::getParsedURL()->layout_settings['global_mode'] === \Core\View::HIDDEN_MODE)
            return;                                                                  

        $parsed_request_uri = parse_url($_SERVER['REQUEST_URI'],  PHP_URL_PATH);

        $parsedURL = self::getParsedURL();

        if ($parsedURL->public_controller_name != 'user' && ($parsedURL->public_action_name  != 'logout' || $parsedURL->public_action_name  != 'login')){
            \Core\Utils\Session::setRefferer($parsed_request_uri);
        }
        // Make the variables set inside the Controller during the execution of Controller/Action available 
        @extract(\Core\Controller::getData(), EXTR_REFS);
        
        if((\Core\Utils\Config::getConfigValue('\Application\Settings::service_name', FALSE) !== NULL))
            return require_once(self::getCorePath() . 'views/default/system_messages.json.tpl');

        // Render the header of the page...
        if(@self::getParsedURL()->layout_settings['header_mode'] !== \Core\View::HIDDEN_MODE)
            require_once(\Core\View::getLayoutElementFullyQualifiedPath(\Core\View::LAYOUT_COMPONENT_HEADER));
        
        // Render the contents of the page...
        if(@self::getParsedURL()->layout_settings['action_mode'] !== \Core\View::HIDDEN_MODE)
            require_once(\Core\View::getLayoutElementFullyQualifiedPath(\Core\View::LAYOUT_COMPONENT_ACTION));
        
        // Render the footer of the page...
        if(@self::getParsedURL()->layout_settings['footer_mode'] !== \Core\View::HIDDEN_MODE)
            require_once(\Core\View::getLayoutElementFullyQualifiedPath(\Core\View::LAYOUT_COMPONENT_FOOTER));  
    }
}