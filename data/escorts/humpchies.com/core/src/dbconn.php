<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: A wrapper class for mysql which allows us to execute SQL
 * commands.
 */

Namespace Core;

final class DBConn
{
    // Members
    // =======
	private static $connections     = NULL; 					// This is the list of opened connections                                                     

	/**
	* The constructor of the class: because the class is meant to run as a 
    * singleton, it can only be called locally.
    * 
    * @returns Nothing
	*/
	private function __construct()
	{}
    
    /**
    * The destructor of the class: it closes all db connections
    * 
    * @returns Nothing
    */
    function __destruct()
    {   
        // Loop through each database for whom connections have been stablished...
        foreach(self::$connections as $database => $connections)
        {
            // Loop through each connection...
            foreach($connections as $connection)
            {
                // Close each one of the connections...
                mysqli_close($connection);
            }   
        } 
    }
     
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}
    
    /**
    * This returns a connection based on the database name and wheter or not we wish to connect to a slave or a master
    *
    * @param string The name of the config to use. For mor information on config naming conventions please refer to 
    *               the \Core\Utils\Config class.
    * @param Boolean Whether or not we want to coonect to a MASTER or a SLAVE server 
    *
    * @return Resource The Database connection when succesful, FALSE otherwise.
    */
    private static function getDatabaseConnection($config_name, $master = FALSE)
    {
        // If we already have a connection stablished simply return it
        if(isset(self::$connections[($fully_qualified_name = strtolower($config_name) . '::' . (string)(int)$master)]))
            return self::$connections[$fully_qualified_name];
        
        // Select a server to connect to: master or one of the slaves
        if($master === TRUE)
        {
            $server = \Core\Utils\Config::getConfigValue($config_name . '::master');
        }
        else
        {
            $server = \Core\Utils\Config::getConfigValue($config_name . '::slaves');
            $server = $server[array_rand($server)];
        }

        // If we get a connection, store it and return it               // Set the server  
        if((self::$connections[$fully_qualified_name] = mysqli_connect( $server,
        
                                                                        // Append the username
                                                                        \Core\Utils\Config::getConfigValue($config_name . '::user'),
                                                                        
                                                                        // Append the password
                                                                        \Core\Utils\Config::getConfigValue($config_name . '::pass'),
                                                                        
                                                                        // Append the name of the database we wish to connect to
                                                                        \Core\Utils\Config::getConfigValue($config_name . '::db'))))
                                                                        return self::$connections[$fully_qualified_name];     // Return the connection...
       
        // Otherise simply bail out...
        return FALSE;
    } 

	/**
	* Executes an arbitrary SQL statement
	*
    * @param string The name of the config to use. For mor information on config naming conventions please refer to 
    *               the \Core\Utils\Config class.
	* @param string The SQL code to execute.
	* @param bool TRUE if we wish execute the SQL on the MASTER DB SERVER, FALSE otherwise
	*
	* @return mysqli_result containning the results of the query when succesful. FALSE 
    * on failure.
	*/
	public static function execute($config_name, $sql, $master = FALSE)
	{
        return mysqli_query(self::getDatabaseConnection($config_name, $master), $sql);
	}

	/**
	* Perform a select statement.
	*
    * @param string The name of the config to use. For mor information on config naming conventions please refer to 
    *               the \Core\Utils\Config class.
	* @param string The SQL code to execute.
    * @param bool TRUE if we wish execute the SQL on the MASTER DB SERVER, FALSE otherwise
	*
	* @return array representing the rows returned by the SQL statement, FALSE on failure
	*/
	public static function select($config_name, $sql, $master = FALSE)
	{
        // Execute the query... If the query fails
        if(($results = mysqli_query(self::getDatabaseConnection($config_name, $master), $sql)) === FALSE || $results->num_rows === 0)
            return FALSE;
            
        // Loop through the results.. and grab each row
		while($row = $results->fetch_assoc())
            $rows[] = $row;

        // Retunr either the record or advice the failure    
        return $rows;
	}

    /**
     * Escape value to be used directly in query string.
     * 
     * @param string $config_name
     * @param string|array $value
     * @param bool $master
     * @return string|array
     */
    public static function escape($config_name, $value, $master = FALSE)
    {
        $con = self::getDatabaseConnection($config_name, $master);
        if (is_array($value)) {
            foreach ($value as $index => $entry) {
                $value[$index] = "'" . mysqli_real_escape_string($con, $entry) . "'";
            }
            return $value;
        }
        return "'" . mysqli_real_escape_string($con, $value) . "'";
    }
	
	/**
	* Perform a select statement, returning a single row
	* 
    * @param string The name of the config to use. For mor information on config naming conventions please refer to 
    *               the \Core\Utils\Config class.*
	* @param string The SQL code to execute.
    * @param bool TRUE if we wish execute the SQL on the MASTER DB SERVER, FALSE otherwise
	*
	* @return array The first row found, FALSE on failure
	*/
	public static function selectRow($config_name, $sql, $master = FALSE)
	{
        // Execute the query... If the query fails
        if(($results = mysqli_query(self::getDatabaseConnection($config_name, $master), $sql)) === FALSE || $results->num_rows === 0)
            return FALSE;
        
        // Grab the first row    
        return $results->fetch_assoc(); 
    }
    
    public static function selectRowClean($config_name, $sql, $values = [], $master = FALSE)
    {
        $con = self::getDatabaseConnection($config_name, $master);
        foreach($values as $key => $value){
            $sql = str_replace(":".$key, mysqli_real_escape_string ($con, $value), $sql );
        }
        // Execute the query... If the query fails
        if(($results = mysqli_query($con, $sql)) === FALSE || $results->num_rows === 0)
            return FALSE;
        
        // Grab the first row    
        return $results->fetch_assoc(); 
    }
    

	/**
	* Inserts a row into the table.
	*
    * @param string The name of the config to use. For mor information on config naming conventions please refer to 
    *               the \Core\Utils\Config class.
    * @param string The name of the module under which the database exists
    * @param array The values and the names of the fields to be used in the creation of the new record
	*
	* @return int The insert ID of the record. FALSE on failure
	*/
	public static function insert($config_name, $table, $fields)
	{
        // Validation: There MUST be fields, and we should have a connection
        if(is_array($fields) === FALSE || ($db_connexion = self::getDatabaseConnection($config_name, TRUE)) === FALSE) 
            return FALSE;
        
        // Sanitize the values...    
        foreach($fields as $key => $value)
            $fields[$key] = '\'' . mysqli_real_escape_string($db_connexion, $value) . '\'';
       
        // Execute the insert, if succesful return the id of the new record 
        if(mysqli_query($db_connexion, 'INSERT INTO ' . $table . ' (' . implode(',', array_keys($fields)) . ') VALUES (' . implode(',', array_values($fields)) . ')') === FALSE)
            return FALSE;
            
        // Return the id of the newly created record
        return mysqli_insert_id($db_connexion);
	}
    
    public static function insertIgnore($config_name, $table, $fields, $onDuplicate = false)
    {
        $insertDuplicate = "";
        // Validation: There MUST be fields, and we should have a connection
        if(is_array($fields) === FALSE || ($db_connexion = self::getDatabaseConnection($config_name, TRUE)) === FALSE) 
            return FALSE;
        
        // Sanitize the values...    
        foreach($fields as $key => $value)
            $fields[$key] = '\'' . mysqli_real_escape_string($db_connexion, $value) . '\'';
        if($onDuplicate ){
            $insertDuplicate = " ON DUPLICATE KEY UPDATE " . $onDuplicate;
        } 
               
        // Execute the insert, if succesful return the id of the new record 
        if(mysqli_query($db_connexion, 'INSERT IGNORE INTO ' . $table . ' (' . implode(',', array_keys($fields)) . ') VALUES (' . implode(',', array_values($fields)) . ') ' . $insertDuplicate  ) === FALSE)
            return FALSE;
            
        // Return the id of the newly created record
        return mysqli_insert_id($db_connexion);
    } 
    
    public static function insertDuplicate($config_name, $table, $fields, $onDuplicate = false)
    {
        $insertDuplicate = "";
        // Validation: There MUST be fields, and we should have a connection
        if(is_array($fields) === FALSE || ($db_connexion = self::getDatabaseConnection($config_name, TRUE)) === FALSE) 
            return FALSE;
        
        // Sanitize the values...    
        foreach($fields as $key => $value)
            $fields[$key] = '\'' . mysqli_real_escape_string($db_connexion, $value) . '\'';
        if($onDuplicate ){
            $insertDuplicate = " ON DUPLICATE KEY UPDATE " . $onDuplicate;
        } 
               
        // Execute the insert, if succesful return the id of the new record 
        if(mysqli_query($db_connexion, 'INSERT INTO ' . $table . ' (' . implode(',', array_keys($fields)) . ') VALUES (' . implode(',', array_values($fields)) . ') ' . $insertDuplicate  ) === FALSE)
            return FALSE;
            
        // Return the id of the newly created record
        return mysqli_insert_id($db_connexion);
    }

	/**
	* Performs an update
	*
    * @param string The name of the config to use. For mor information on config naming conventions please refer to 
    *               the \Core\Utils\Config class.
	* @param string The name of the module under which the database exists
	* @param array relational data representing the names and values of the fields to be used in the update of the new record 
	*
	* @return integer The number of row affected by the update
	*/
	public static function update($config_name, $table, $fields, $conditions = NULL)
	{
        // Validation: There MUST be fields, and we should have a connection
        if(is_array($fields) === FALSE || ($db_connexion = self::getDatabaseConnection($config_name, TRUE)) === FALSE) 
            return FALSE;
        else
            $sql = NULL;
            
        // Grab the fields' names and sanitize their values
        foreach($fields as $key => $value)
            $sql .= ($sql !== NULL ? ', ': NULL) . $key . ' = \''. mysqli_real_escape_string($db_connexion, $value) . '\'';

        // Run the sql, and if the update is succesful return the number of rows afected by the update
        if(mysqli_query($db_connexion, 'UPDATE ' . $table . ' SET ' . $sql . ($conditions !== NULL ? ' WHERE ' . $conditions : NULL)) === FALSE)
            return  FALSE;    
        
        // Return the number of rows affetced by the update
        return mysqli_affected_rows($db_connexion);
	}

	/**
	* Performs a delete query
    * 
    * @param string The name of the config to use. For mor information on config naming conventions please refer to 
    *               the \Core\Utils\Config class.
    * @param string The name of the table where the delete is to take place
    * @param array The conditions to be used in the deletion of the record
    * 
    * @return integer The number of records deleted
	*/
	public static function delete($config_name, $table, $conditions)
	{   
        // Validation: we should have a connection
        if(($db_connexion = self::getDatabaseConnection($config_name, TRUE)) === FALSE) 
            return FALSE;

        // Attempt to delete, if deletion is succesful return the number of deleted records
		if(mysqli_query($db_connexion, 'DELETE FROM ' . $table . ' WHERE ' . $conditions) === FALSE)
            return FALSE;

        // Grab the number of records deleted 
        return mysqli_affected_rows($db_connexion);       
	}
    
    /**
    * This function retrieves the error stack on all connections
    * 
    * @returns An array containning a cumulative list of the last errors that have taken 
    * place in all DB connections of the current instance of DBConn.    
    */
    public static function getLastError($config_name, $master = FALSE)
    {
        return mysqli_error(self::getDatabaseConnection($config_name, $master));
    }   
	
	public static function getConnection($config_name, $master = FALSE)
	{
		return self::getDatabaseConnection($config_name, $master);
	}
}
