<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file contains the set of instructions and the sequence
 * in which they called for every single request to the framework. This file
 * is the main entry point for all requests. Without it the framework is a
 * simply a library.
 */

// Bring in the router...

require_once(INSTALL_PATH . 'core/src/router.php');

// Enable class outoloading...
spl_autoload_register('\\Core\\Router::autoload');

// Execute from the CLI
if (php_sapi_name() === "cli") {
    
    // Remove the first ARGV... which is the name of the script...
    array_shift($_SERVER['argv']);
    
    // The second ARGV is the name of the function we wish to call... so we call it 
    // and pass the remainder of the ARGV as arguments...
    call_user_func(array_shift($_SERVER['argv']), $_SERVER['argv']);
    
    // Exit... 
    exit();
}

\Core\Utils\Session::getLocale();

// Make sure we run our stuff from a single hostname...
\Core\Router::redirectToCanonical();

// Grab the path and make it a part of the $_GET
\Core\Router::addPathToGet(@\Core\Router::getParsedURL()->path);

// Recursively clean the GET...
\Core\Utils\Security::cleanArray($_GET);

// Recursively clean the POST...    
\Core\Utils\Security::cleanArray($_POST);

// Recursively clean the COOKIES...
\Core\Utils\Security::cleanArray($_COOKIE);

// Avoid SQL injections ...
\Core\Utils\Security::checkSqlInject();


// Run the Application's initialization code... if explicitly required.
if(\Core\Utils\Config::getConfigValue('\Application\Settings::run_init', FALSE) === TRUE)
    require_once(\Core\Router::getApplicationPath() . 'src/init.php');

// Validate the request to a Web Service
if(\Core\Utils\Config::getConfigValue('\Application\Settings::service_name', FALSE) !== NULL){
    if(\Core\Utils\HMAC::isRequestValid(($server_vars_and_headers = \Core\Utils\Device::getServerVarsAndHeaders()), file_get_contents('php://input')) !== TRUE)
        exit('[ x ] Invalid request!');
        
// If we are no running a Web service
}
else{      
     
    // Indicate that the output is buffered and handled by a custom function
    ob_start('Core\Router::preRender');
     
    // Start or re-capture a session
    session_start();
    
    // Force login if current action's name identifies it as an MA action and the user is not logged in
    \Core\Utils\Security::redirectIfActionRequiresLogin(@\Core\Router::getParsedURL()->action_name,                         // Current action name
                                                        $_SERVER['REQUEST_URI']);                                           // The current request URI
                                                        
    // Throw 404 if the current action requires the user to be anonymous 
    \Core\Utils\Security::throwNotFoundIfActionRequiresAnonymity(@\Core\Router::getParsedURL()->action_name); 
    
    // Try and login users via cookie
    Core\Utils\Session::authenticateViaCookie();
}

// Fullfil the request
\Core\Router::processRoute();

// Clear any flash values dancing around...
\Core\Controller::clearFlash();