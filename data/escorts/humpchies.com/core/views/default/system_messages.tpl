<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents one ad and its details in a list of ads.
 */

// Only execute if there are system messages  
if(($system_messages = \Core\Controller::getMessages()) !== FALSE)
{
    /*
    * Loop through each message type and grab all messages for that particular 
    * type, then simply flush the message(s) to the screen.
    */
    foreach($system_messages as $message_type => $system_message)
    {?>
        <div class="<?=$message_type?>" clear>
<?php       foreach($system_message as $message) echo $message . (count($system_message) > 0 ? '<br />' : NULL);?>
        </div>   
<?php       
    }
}?>