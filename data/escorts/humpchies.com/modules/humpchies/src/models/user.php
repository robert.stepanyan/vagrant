<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to interact with user related tasks. Such tasks might be
 * but are not restricted to: add a new user, retrieve a username, delete a
 * user, etc.
 */

Namespace Modules\Humpchies;

final class UserModel{
    
    // CONSTANTS
    // =========
    const DEFAULT_DB_CONFIG                         = '\Modules\Humpchies\Db\Default';
    const DEFAULT_DB_TABLE                          = 'users';
    
    const MIN_USERNAME_LEN                          = 4;
    const MAX_USERNAME_LEN                          = 16;
    const MIN_PASSWORD_LEN                          = 6;
    const MAX_PASSWORD_LEN                          = 16;
    const MAX_ACTIVATION_STRING_TTL                 = 86400;
    const MAX_PASSWORD_RESET_STRING_TTL             = 1800;
    
    const ERROR_USERNAME_IS_EMPTY                   = 'Modules.Humpchies.UserModel.ErrorUsernameIsEmpty';
    const ERROR_USERNAME_IS_TOO_SHORT               = 'Modules.Humpchies.UserModel.ErrorUsernameIsTooShort';
    const ERROR_USERNAME_IS_TOO_LONG                = 'Modules.Humpchies.UserModel.ErrorUsernameIsTooLong';
    const ERROR_USERNAME_IS_INVALID                 = 'Modules.Humpchies.UserModel.ErrorUsernameIsInvalid';
    const ERROR_PASSWORD_IS_EMPTY                   = 'Modules.Humpchies.UserModel.ErrorPasswordIsEmpty';
    const ERROR_PASSWORD_IS_TOO_SHORT               = 'Modules.Humpchies.UserModel.ErrorPasswordIsTooShort';
    const ERROR_PASSWORD_IS_TOO_LONG                = 'Modules.Humpchies.UserModel.ErrorPasswordIsTooLong';
    const ERROR_LOGIN_INFORMATION_IS_INVALID        = 'Modules.Humpchies.UserModel.ErrorLoginInformationIsInvalid';
    const ERROR_USER_ID_IS_INVALID                  = 'Modules.Humpchies.UserModel.ErrorUserIdIsInvalid';
    const ERROR_ACTIVATION_STRING_IS_EXPIRED        = 'Modules.Humpchies.UserModel.ErrorActivationStringIsExpired';
    const ERROR_ACTIVATION_STRING_IS_INVALID        = 'Modules.Humpchies.UserModel.ErrorActivationStringIsInvalid';
    const ERROR_ACTIVATION_FAILED                   = 'Modules.Humpchies.UserModel.ErrorActivationFailed';
    const ERROR_PASSWORD_RESET_STRING_IS_EXPIRED    = 'Modules.Humpchies.UserModel.ErrorPasswordResetStringIsExpired';
    const ERROR_PASSWORD_RESET_STRING_IS_INVALID    = 'Modules.Humpchies.UserModel.ErrorPasswordResetStringIsInvalid';
    const ERROR_PHONE_IS_EMPTY                      = 'Modules.Humpchies.UserModel.ErrorPhoneIsEmpty';
    const ERROR_USERNAME_ALREADY_EXISTS             = 'Modules.Humpchies.UserModel.ErrorUsernameAlreadyExists';
    const ERROR_EMAIL_ALREADY_EXISTS                = 'Modules.Humpchies.UserModel.ErrorEmailAlreadyExists';
    const ERROR_USERNAME_OR_EMAIL_IS_INVALID        = 'Modules.Humpchies.UserModel.ErrorUsernameOrEmailIsInvalid';
    const ERROR_PASSWORD_RESET_FAILED               = 'Modules.Humpchies.UserModel.ErrorPasswordResetFailed';
    const ERROR_IP_TRACKING_FAILED                  = 'Modules.Humpchies.UserModel.ErrorIpTrackingFailed';
    const ERROR_PHONE_TRACKING_FAILED               = 'Modules.Humpchies.UserModel.ErrorPhoneTrackingFailed';
    const ERROR_UNKNOWN                             = 'Modules.Humpchies.UserModel.ErrorUnknown';
    const NO_ERROR                                  = 'Modules.Humpchies.UserModel.NoError';
    
    const AUTH_KEY                                  = '47637e273562502d437c674f3e6a2d5a';
    const VALIDATION_KEY                            = '4c6d393d35712744396b623967dfeab913';
    const USER_TYPE_ADVERTISEMENT                   = 1;
    const USER_TYPE_MEMBER                          = 2;

    /**
    * Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}
    
    /**
    * put your comment there...
    * 
    * @param mixed $username
    */
    public static function isUsernameValid(&$username)
    {
        switch(($username_validity = \Core\Utils\Security::isStringInputValid($username, self::MAX_USERNAME_LEN, self::MIN_USERNAME_LEN)))
        {
            case \Core\Utils\Security::NO_ERROR:
                if(ctype_alnum($username) === FALSE)
                    return self::ERROR_USERNAME_IS_INVALID;
                else
                    return self::NO_ERROR;
                break; 
            case \Core\Utils\Security::ERROR_STRING_IS_EMPTY:
                return self::ERROR_USERNAME_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_SHORT:
                return self::ERROR_USERNAME_IS_TOO_SHORT;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_LONG:
                return self::ERROR_USERNAME_IS_TOO_LONG;
                break;    
            default:
                return $username_validity;
                break; 
        } 
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $username_or_email
    */
    public static function isUserActive($username_or_email)
    {
        return \Core\DBConn::selectRow(
            self::DEFAULT_DB_CONFIG,
            'SELECT
                id
            FROM 
                ' . self::DEFAULT_DB_TABLE . '
            WHERE
                (username = \'' . $username_or_email . '\'
            OR
                email = \'' . $username_or_email . '\')
            AND 
                date_banned = 0',
            TRUE);
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $username_or_email
    */
    public static function isUserActiveById($id)
    {
        return \Core\DBConn::selectRow(
            self::DEFAULT_DB_CONFIG,
            'SELECT
                username
            FROM 
                ' . self::DEFAULT_DB_TABLE . '
            WHERE
                id = \'' . $id . '\'
            AND 
                date_banned = 0',
            TRUE);
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $password
    */
    public static function isPasswordValid(&$password)
    {
        switch(($password_validity = \Core\Utils\Security::isStringInputValid($password, self::MAX_PASSWORD_LEN, self::MIN_PASSWORD_LEN)))
        {
            case \Core\Utils\Security::NO_ERROR:
                return self::NO_ERROR;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_EMPTY:
                return self::ERROR_PASSWORD_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_SHORT:
                return self::ERROR_PASSWORD_IS_TOO_SHORT;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_LONG:
                return self::ERROR_PASSWORD_IS_TOO_LONG;
                break;    
            default:
                return $password_validity; 
        }  
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $email
    */
    public static function checkIfEmailBlacklisted($email){
        return \Core\DBConn::selectRow(
            self::DEFAULT_DB_CONFIG,
            'SELECT
                name
            FROM 
                blacklisted_emails
            WHERE 
                name = \'' . $email . '\'',
            TRUE);
    }
        
    /**
    * put your comment there...
    * 
    * @param mixed $username
    * @param mixed $password
    * @param mixed $password_confirmation
    * @param mixed $email
    */
    public static function create(&$username, &$password, &$email, $used_user_ids = null, $user_type=null, $user_location = null)
    {
        /*ini_set("display_errors", 1);
        ini_set("track_errors", 1);
        ini_set("html_errors", 1);
        error_reporting(E_ALL);*/

        $user_data = array( 
            'username'      => $username,
            'password'      => md5($password . self::VALIDATION_KEY),
            'email'         => $email,
            'date_created'  => ($date_created = time()),
            'date_activated'=> 0
        );
        
        if(is_array($user_location) && !empty($user_location['ip_real']) ) {
            $user_data['ip_real'] = $user_location['ip_real'];
        }
        
        if(is_array($user_location) && !empty($user_location['ip_class']) ) {
            $user_data['ip_class'] = $user_location['ip_class'];
        }
        
        if(is_array($user_location) && !empty($user_location['user_location']) ) {
            $user_data['user_location'] = $user_location['user_location'];
        }
        
        if(is_array($user_location) && !empty($user_location['user_country']) ) {
            $user_data['user_country'] = $user_location['user_country'];
        }
        
        if( $used_user_ids ){
            $user_data['used_user_ids'] = $used_user_ids;
        }
        if ($user_type){
            $user_data['type'] = $user_type;
        }
        
        if( self::checkIfEmailBlacklisted( $email ) ){
            $user_data['date_banned'] = 27;
        }
        
        $new_user_id = \Core\DBConn::insert(
            self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $user_data
        );

        if($new_user_id !== FALSE)
            return $new_user_id;
            
        $last_db_error = \Core\DBConn::getLastError(self::DEFAULT_DB_CONFIG, TRUE);
          
        if(stristr($last_db_error, "Duplicate entry '$username'"))
            return self::ERROR_USERNAME_ALREADY_EXISTS;
        elseif(stristr($last_db_error, "Duplicate entry '$email'"))
            return self::ERROR_EMAIL_ALREADY_EXISTS;  
        else
            return self::ERROR_UNKNOWN; 
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $encrypted_activation_string
    */
    public static function activate(&$encrypted_activation_string)
    {
        @list($user_id, $date_created, $hash) = explode('.', \Core\Utils\Security::decrypt( str_replace(' ', '+', $encrypted_activation_string), 
                                                                                            file_get_contents(\Core\Router::getModulesPath() . 'humpchies/assets/security/crypto.key'), 
                                                                                            TRUE));
                                                                            
        $user_id = base64_decode($user_id);
        $date_created = base64_decode($date_created);

        if(md5($user_id . $date_created . self::AUTH_KEY) !== $hash)
            return self::ERROR_ACTIVATION_STRING_IS_INVALID;
        elseif(\Core\Utils\Security::isIdInputValid($user_id) !== \Core\Utils\Security::NO_ERROR)
            return self::ERROR_USER_ID_IS_INVALID;
        elseif(($date_created + self::MAX_ACTIVATION_STRING_TTL) < ($current_gmt_time = time()))
            return self::ERROR_ACTIVATION_STRING_IS_EXPIRED;
        
        if(\Core\DBConn::update(self::DEFAULT_DB_CONFIG,
                                self::DEFAULT_DB_TABLE,
                                array('date_activated' => $current_gmt_time),
                                'id = \'' . $user_id . '\' 
                                AND
                                date_activated = 0
                                AND
                                date_banned = 0') === 1) 
                                return self::NO_ERROR;
                                
        return self::ERROR_ACTIVATION_FAILED; 
    }
    
    public static function getUserType(&$encrypted_activation_string){
         @list($user_id, $date_created, $hash) = explode('.', \Core\Utils\Security::decrypt( str_replace(' ', '+', $encrypted_activation_string), 
                                                                                            file_get_contents(\Core\Router::getModulesPath() . 'humpchies/assets/security/crypto.key'), 
                                                                                            TRUE));
                                                                            
        $user_id = base64_decode($user_id);
        $date_created = base64_decode($date_created);
        $row = \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG, 'SELECT type FROM users WHERE id = \'' . $user_id . '\' AND date_banned = 0', TRUE);
        return $row['type'];
        
    }
    /**
    * put your comment there...
    * 
    * @param mixed $user_id
    */
    public static function bann(&$user_id)
    {
        if(\Core\Utils\Security::isIdInputValid($user_id) !== \Core\Utils\Security::NO_ERROR)
            return self::ERROR_USER_ID_IS_INVALID;

        return \Core\DBConn::update(    self::DEFAULT_DB_CONFIG,
                                        self::DEFAULT_DB_TABLE,
                                        array('date_banned' => gmmktime()),
                                        "id = $user_id");    
        
    }
     public static function autoBan(&$user_id, $ip)
    {
        if(\Core\Utils\Security::isIdInputValid($user_id) !== \Core\Utils\Security::NO_ERROR)
            return self::ERROR_USER_ID_IS_INVALID;

            
        return \Core\DBConn::update(    self::DEFAULT_DB_CONFIG,
                                        self::DEFAULT_DB_TABLE,
                                        array(
                                            'date_banned' => 30,
                                            'ip_ban_date' => time(),
                                            'ip_ban'      => ip2long($ip)  
                                        ),
                                        "id = $user_id");    
        
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $user_id
    */
    public static function unBann(&$user_id)
    {
        if(\Core\Utils\Security::isIdInputValid($user_id) !== \Core\Utils\Security::NO_ERROR)
            return self::ERROR_USER_ID_IS_INVALID;
        
        return \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
                                self::DEFAULT_DB_TABLE,
                                array('date_banned' => 0),
                                "id = $user_id");    
        
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $user_id
    */
    public static function delete(&$user_id)
    {
        if(\Core\Utils\Security::isIdInputValid($user_id) !== TRUE)
            return self::ERROR_USER_ID_IS_INVALID;
        
        return \Core\DBConn::delete(  self::DEFAULT_DB_CONFIG,
                                self::DEFAULT_DB_TABLE,
                                "id = $user_id");   
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $user_id
    * @param mixed $new_phone
    */
    public static function updatePhone(&$user_id, &$new_phone)
    {
        // ---------------------------USER ID----------------------------------------------------
        if(\Core\Utils\Security::isIdInputValid($user_id) !== TRUE)
            return self::ERROR_USER_ID_IS_INVALID;
            
        // ---------------------------PHONE------------------------------------------------------
        if(($phone_validity = \Core\Utils\Security::isPhoneInputValid($new_phone)) !== TRUE)
        {
            switch($phone_validity)
            {
                case \Core\Utils\Security::ERROR_STRING_IS_EMPTY:
                    return self::ERROR_PHONE_IS_EMPTY;
                    break;
                default:
                    return $phone_validity;
                    break;         
            }       
        }
        
        return \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
                                self::DEFAULT_DB_TABLE,
                                array('phone' => $new_phone),
                                "id = $user_id");
    }
               
    
    public static function updateForumToken($user_id, $token) {
        // ---------------------------USER ID----------------------------------------------------
      //  if(\Core\Utils\Security::isIdInputValid($user_id) !== TRUE)
//            return self::ERROR_USER_ID_IS_INVALID;
//            
        
        return \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
                                self::DEFAULT_DB_TABLE,
                                array('forum_token' => $token),
                                "id = $user_id");
    }
    
    public static function updateLanguage($user_id, $language) {
        // ---------------------------USER ID----------------------------------------------------
      //  if(\Core\Utils\Security::isIdInputValid($user_id) !== TRUE)
//            return self::ERROR_USER_ID_IS_INVALID;
//            
        
        return \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
                                self::DEFAULT_DB_TABLE,
                                array('language' => $language),
                                "id = $user_id");
    }
    
    
    /**
    * put your comment there...
    * 
    * @param mixed $user_id
    * @param mixed $date_created
    */
    public static function generateActivationString($user_id)
    {
        return \Core\Utils\Security::encrypt(   base64_encode($user_id) . '.' . base64_encode(($date_created = time())) . '.' . md5($user_id . $date_created . self::AUTH_KEY),
                                                file_get_contents(\Core\Router::getModulesPath() . 'humpchies/assets/security/crypto.key'),
                                                TRUE);
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $username
    */
    public static function generatePasswordResetString(&$username_or_email)
    {
        if(($account_details = \Core\DBConn::selectRow(self::DEFAULT_DB_CONFIG,
                                                    'SELECT
                                                            id,
                                                            password,
                                                            email
                                                    FROM 
                                                            ' . self::DEFAULT_DB_TABLE . ' 
                                                    WHERE 
                                                            (username = \'' . $username_or_email . '\'
                                                            OR
                                                            email = \'' . $username_or_email . '\')
                                                            AND 
                                                            date_created > 0
                                                            AND
                                                            date_activated > 0
                                                            AND
                                                            date_banned = 0',
                                                    TRUE)) === FALSE)
                                                        return self::ERROR_USERNAME_OR_EMAIL_IS_INVALID;

        $username_or_email = $account_details['email'];
        
        return \Core\Utils\Security::encrypt(   base64_encode($account_details['id']) . '.' . base64_encode($account_details['password']) . '.' . base64_encode(($date_generated = time())) . '.' . md5($account_details['id'] . $account_details['password'] . $date_generated . self::AUTH_KEY), 
                                                file_get_contents(\Core\Router::getModulesPath() . 'humpchies/assets/security/crypto.key'), 
                                                TRUE);
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $encrypted_reset_string
    * @param mixed $password
    * @param mixed $password_confirmation
    */
    public static function resetPasswordViaEncryptedString(&$encrypted_reset_string, &$password)
    {
        @list($user_id, $encrypted_password, $date_generated, $hash) = explode('.', \Core\Utils\Security::decrypt(  str_replace(' ', '+', $encrypted_reset_string),
                                                                                                                    file_get_contents(\Core\Router::getModulesPath() . 'humpchies/assets/security/crypto.key'),  
                                                                                                                    TRUE));
           
        $user_id = base64_decode($user_id);
        $encrypted_password = base64_decode($encrypted_password);
        $date_generated = base64_decode($date_generated);
        
        if(md5($user_id . $encrypted_password . $date_generated . self::AUTH_KEY) !== $hash)
            return self::ERROR_PASSWORD_RESET_STRING_IS_INVALID;
        if(\Core\Utils\Security::isIdInputValid($user_id) !== \Core\Utils\Security::NO_ERROR)
            return self::ERROR_USER_ID_IS_INVALID;
        elseif(($date_generated + self::MAX_PASSWORD_RESET_STRING_TTL) < time())
            return self::ERROR_PASSWORD_RESET_STRING_IS_EXPIRED;
          
        if(\Core\DBConn::update(self::DEFAULT_DB_CONFIG,
                                self::DEFAULT_DB_TABLE,
                                array('password' => md5($password . self::VALIDATION_KEY)),
                                'id = ' . $user_id . '
                                AND 
                                password = \'' . $encrypted_password . '\' 
                                AND
                                date_created > 0
                                AND
                                date_activated > 0
                                AND
                                date_banned = 0') === 1)                  
                                return self::NO_ERROR;
                      
        return self::ERROR_PASSWORD_RESET_FAILED;
    }
    
    public static function updateSettings($user_id, $disable_comments, $disable_pm, $password = '') {
        
        $arrUpdate = array(
            'disable_comments' => $disable_comments,
            'disable_pm' => intval($disable_pm)
        );
        
        if($password != '') {
            $arrUpdate['password'] = md5($password . self::VALIDATION_KEY);
        }
        
       
        
       // var_dump($arrUpdate);
//        die();
        $update = \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
                                self::DEFAULT_DB_TABLE,
                                $arrUpdate,
                                "id = $user_id");
        
        if($update) {
             
            self::updateUserDetails($user_id);
            if($disable_comments < 2) {
             // update ads
             \Core\DBConn::update(self::DEFAULT_DB_CONFIG, 'ads', ["blocked_comments" => $disable_comments], "user_id = $user_id" );
            }
        }                        
                                
        return $update;
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $username
    * @param mixed $password
    * @param mixed $give_authentication_cookie
    */
    public static function authenticateViaIdAndPassword(&$id, &$password)
    {   
  
        
        if(($authentication_values = \Core\DBConn::selectRow(   self::DEFAULT_DB_CONFIG,
                                                                'SELECT
                                                                        id,
                                                                        username,
                                                                        email,
                                                                        phone,
                                                                        date_created,
                                                                        type
                                                                FROM 
                                                                        ' . self::DEFAULT_DB_TABLE . ' 
                                                                WHERE 
                                                                        id = ' . $id . '
                                                                        AND 
                                                                        password = \'' . md5($password . self::VALIDATION_KEY) . '\' 
                                                                        AND
                                                                        date_created > 0
                                                                        AND
                                                                        date_activated > 0
                                                                        AND
                                                                        date_banned = 0',
                                                                TRUE)) === FALSE)
                                                                    return FALSE;

        return $authentication_values;  
    }
    
  
    
    
    /**
    * put your comment there...
    * 
    * @param mixed $username
    * @param mixed $password
    * @param mixed $give_authentication_cookie
    */
    public static function authenticateViaUsernameOrEmailAndPassword(&$username_or_email, &$password)
    {   
        if(($authentication_values = \Core\DBConn::selectRow(   self::DEFAULT_DB_CONFIG,
                                                                'SELECT
                                                                        id,
                                                                        username,
                                                                        email,
                                                                        phone,
                                                                        date_created,
                                                                        type
                                                                FROM 
                                                                        ' . self::DEFAULT_DB_TABLE . ' 
                                                                WHERE 
                                                                        (username = \'' . $username_or_email . '\'
                                                                        OR 
                                                                        email = \'' . $username_or_email . '\')
                                                                        AND 
                                                                        password = \'' . md5($password . self::VALIDATION_KEY) . '\' 
                                                                        AND
                                                                        date_created > 0
                                                                        AND
                                                                        date_activated > 0
                                                                        AND
                                                                        date_banned = 0',
                                                                TRUE)) === FALSE)
                                                                    return FALSE;

        return $authentication_values;  
    }
    
    
    public static function authenticateViaAdmin($admin_key)
    {   
        //\Core\library\log::debug($admin_key,'admin key');
        $userInfo = unserialize(\Core\library\Redis::get($admin_key));
        \Core\library\Redis::del($admin_key);
        //\Core\library\log::debug($userInfo,'userinfo');
        
        
        if(($authentication_values = \Core\DBConn::selectRow(   self::DEFAULT_DB_CONFIG,
                                                                'SELECT
                                                                        id,
                                                                        username,
                                                                        email,
                                                                        phone,
                                                                        date_created,
                                                                        type
                                                                FROM 
                                                                        ' . self::DEFAULT_DB_TABLE . ' 
                                                                WHERE 
                                                                        `id`  = ' . $userInfo['user_id']. '                                                           
                                                                        AND date_created > 0
                                                                        AND date_activated > 0
                                                                        AND date_banned = 0',
                                                                TRUE)) === FALSE)
                                                                    return FALSE;

        return $authentication_values;  
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $login_data
    */
    public static function saveLoginHistoryRecord( $login_data ){
        $login_history_item = \Core\DBConn::insert(
            self::DEFAULT_DB_CONFIG, 
            'users_login_history', 
            $login_data
        );
    }
    
    public static function saveLastLogin($user_id, $last_login){
        if(\Core\DBConn::update(self::DEFAULT_DB_CONFIG,
                                self::DEFAULT_DB_TABLE,
                                $last_login,
                                "id = $user_id") === 1)
            return self::NO_ERROR;

        return self::ERROR_IP_TRACKING_FAILED;
       
    }
    
    /**
    * put your comment there...
    * 
    * @param mixed $persistent_key
    */
    public static function authenticateViaPersintentKey($persistent_key)
    {
        if(\Core\Utils\Session::getAuthenticatedSessionValues() !== FALSE)
            return \Core\Utils\Session::ERROR_SESSION_ALREADY_AUTHENTICATED;
        elseif(\Core\Utils\Security::isIdInputValid(($persistent_key = (int)$persistent_key)) === FALSE)
            return self::ERROR_USER_ID_IS_INVALID;
              
        if(($authentication_values = \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG,
                                                        'SELECT
                                                                id,
                                                                username
                                                        FROM 
                                                                ' . self::DEFAULT_DB_TABLE . ' 
                                                        WHERE 
                                                                id = \'' . $persistent_key . '\'
                                                                AND
                                                                date_created > 0
                                                                AND
                                                                date_activated > 0
                                                                AND
                                                                date_banned = 0',
                                                        TRUE)) === FALSE)
                                                            return FALSE;
                                                
        \Core\Utils\Session::authenticate($authentication_values, $authentication_values['id'], __CLASS__);
        
        return TRUE;
    }
    
    public static function isAllowed($user_id)
    {
        return (!(\Core\DBConn::selectRow(  self::DEFAULT_DB_CONFIG,
                                            'SELECT 
                                                    null
                                            FROM
                                                    ' . self::DEFAULT_DB_TABLE . '
                                            WHERE
                                                    id = ' . $user_id . '
                                                    AND
                                                    date_created > 0
                                                    AND
                                                    date_activated > 0
                                                    AND
                                                    date_banned = 0') === FALSE));    
    }

    public static function trackIp($user_id, $ip)
    {
        $ip = ip2long($ip);

        if(\Core\DBConn::update(self::DEFAULT_DB_CONFIG,
                                self::DEFAULT_DB_TABLE,
                                array('ip' => $ip),
                                "id = $user_id") === 1)
            return self::NO_ERROR;

        return self::ERROR_IP_TRACKING_FAILED;
    }

    public static function trackPhone($user_id, $phone)
    {
        if(\Core\DBConn::update(self::DEFAULT_DB_CONFIG,
                self::DEFAULT_DB_TABLE,
                array('phone' => $phone),
                "id = $user_id") === 1)
            return self::NO_ERROR;

        return self::ERROR_PHONE_TRACKING_FAILED;
    }
    
    public static function isBehindBannedIp($ip)
    {
        $ip = ip2long($ip);

        return (!(\Core\DBConn::selectRow(  self::DEFAULT_DB_CONFIG,
                                            'SELECT 
                                                    null
                                            FROM
                                                    ' . self::DEFAULT_DB_TABLE . '
                                            WHERE
                                                    ip = ' . $ip . '
                                                    AND
                                                    date_banned > 0') === FALSE));
    }
    
    public static function getBannedUsersByIp($ip)
    {
        $ip = ip2long($ip);

        return \Core\DBConn::select(  self::DEFAULT_DB_CONFIG,
                                            'SELECT 
                                                    id
                                            FROM
                                                    ' . self::DEFAULT_DB_TABLE . '
                                            WHERE
                                                    ip = ' . $ip . '
                                                    AND
                                                    date_banned > 0');
    }

    public static function isBehindBannedPhone($phone)
    {
        return (!(\Core\DBConn::selectRow(  self::DEFAULT_DB_CONFIG,
                                            'SELECT 
                                                    null
                                            FROM
                                                    ' . self::DEFAULT_DB_TABLE . '
                                            WHERE
                                                    phone = ' . $phone . '
                                                    AND
                                                    date_banned > 0') === FALSE));
    }

    public static function getDetailsByIdforSettings($id)
    {
        return \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG,
                                            'SELECT
                                                    id,
                                                    username,
                                                    email,
                                                    phone,
                                                    disable_comments,
                                                    disable_pm
                                            FROM 
                                                    ' . self::DEFAULT_DB_TABLE . ' 
                                            WHERE 
                                                    id = ' . $id);
    }
    public static function getDetailsById($id)
    {
        return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
                                            'SELECT
                                                    id,
                                                    username,
                                                    email,
                                                    phone,
                                                    disable_comments,
                                                    disable_pm
                                            FROM 
                                                    ' . self::DEFAULT_DB_TABLE . ' 
                                            WHERE 
                                                    id = ' . $id);
    }
    
    public static function updateUserDetails($id) {
        
        return \Core\Utils\Data::updateCacheBySql(self::DEFAULT_DB_CONFIG,
                                            'SELECT
                                                    id,
                                                    username,
                                                    email,
                                                    phone,
                                                    disable_comments,
                                                    disable_pm
                                            FROM 
                                                    ' . self::DEFAULT_DB_TABLE . ' 
                                            WHERE 
                                                    id = ' . $id, FALSE, FALSE);
    }
    
    
    

    /**
     * Determines if an account is new i.e. within the probation period as define by
     * Modules\Humpchies::AdController::AD_POST_INITIAL_RESTRICTION_PERIOD
     *
     * @param $userId The ID of the user we want to analyze
     */
    public static function isNew($userId)
    {
        return (!(\Core\DBConn::selectRow(  self::DEFAULT_DB_CONFIG,
                                            'SELECT
                                                    null
                                            FROM 
                                                    ' . self::DEFAULT_DB_TABLE . ' 
                                            WHERE 
                                                    id = ' . $userId . '
                                                    AND
                                                    date_created >= (UNIX_TIMESTAMP() - ' . \Modules\Humpchies\AdController::AD_POST_INITIAL_RESTRICTION_PERIOD . ')') === FALSE));
    }
		
	public static function getCredits($userId)
    {
        $data_row =  \Core\DBConn::selectRow(  self::DEFAULT_DB_CONFIG,
                                            'SELECT
                                                    credits
                                            FROM 
                                                    ' . self::DEFAULT_DB_TABLE . ' 
                                            WHERE 
                                                    id = ' . $userId
											);
		return  $data_row['credits'];
    }
	
	 /**
    * @param mixed $id
    */
    public static function getUserAutoApprovalById($id)
    {
        return \Core\DBConn::selectRow(
            self::DEFAULT_DB_CONFIG,
            'SELECT
                auto_approval
            FROM 
                ' . self::DEFAULT_DB_TABLE . '
            WHERE
                id = \'' . $id . '\'',
            TRUE);
    }

    /**
     * Get user detail list.
     *
     * @param int[] $userIdList The user ID list.
     * @return array
     */
    public static function getDetailsByIdList($userIdList)
    {
        $result = \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT
                 id,
                 username,
                 type
            FROM
                ' . self::DEFAULT_DB_TABLE . ' 
            WHERE 
                id IN (' . implode(',', $userIdList) . ')'
        );
        return $result ? $result : array();
    }      
    
    /**
     * Get user detail list.
     *
     * @param int[] $userIdList The user ID list.
     * @return array
     */
    public static function getipListUpdate()
    {
        $sql = 'SELECT id , (SELECT ip FROM users_login_history WHERE user_id = id and ip <> "" ORDER BY login_date ASC LIMIT 1) AS ip_reg, INET_NTOA(ip) as ip_user FROM users U 
                    WHERE ip IS NOT NULL AND ip_real IS NULL ORDER BY id AND date_banned = 0 AND date_activated <> 0 LIMIT 1000'; 
        
        $result = \Core\DBConn::select( self::DEFAULT_DB_CONFIG,
            $sql ,true
        );
        return $result ? $result : array();
    }    
}
