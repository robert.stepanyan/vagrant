<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to interact with city related items and tasks
 */

Namespace Modules\Humpchies;

final class ShoppingcartModel{
    
    const DEFAULT_DB_CONFIG                         = '\Modules\Humpchies\Db\Default';
    const DEFAULT_CITY                              = 'Montr�al';
    const DEFAULT_DB_TABLE                          = 'shopping_cart';
    
    private static $supported_cities                = NULL;
    private static $cities_with_ads                 = NULL;
    
    // CONSTANTS
    // =========
    const ERROR_CITY_IS_INVALID                     = 'Modules.Humpchies.CityModel.ErrorCityIsInvalid';
    const ERROR_CITY_IS_EMPTY                       = 'Modules.Humpchies.CityModel.ErrorCityIsEmpty';
    const NO_ERROR                                  = 'Modules.Humpchies.CityModel.NoError';
    

    /**
    * Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}

    
    public static function create(&$fields)
    {
        $new_ad_id = \Core\DBConn::insert(  self::DEFAULT_DB_CONFIG, 
                                            self::DEFAULT_DB_TABLE, 
                                            $fields);
      
        if($new_ad_id !== FALSE)
            return $new_ad_id;
            
        $last_db_error = \Core\DBConn::getLastError(self::DEFAULT_DB_CONFIG, TRUE);

        if(strpos($last_db_error, 'Duplicate entry \'') !== FALSE && (strrpos($last_db_error, 'for key \'description\'', -21) !== FALSE || strrpos($last_db_error, 'for key 2', -9)))
            return self::ERROR_DESCRIPTION_IS_DUPLICATED;
        else
            return self::ERROR_UNKNOWN; 
    }

    public static function get($userid, $hash)
    {
       
         return \Core\DBConn::selectRow(
            self::DEFAULT_DB_CONFIG,
            'SELECT
                s.*, a.title
                 FROM 
                 '.self::DEFAULT_DB_TABLE.' s
                LEFT JOIN ads a on a.id = s.ad_id
                WHERE
                s.user_id = '. $userid.' AND s.hash = \''.$hash.'\'',
            TRUE);
    }
    public static function getbyID($userid, $id)
    {
       
         return \Core\DBConn::selectRow(
            self::DEFAULT_DB_CONFIG,
            'SELECT
                s.*, a.title
                 FROM 
                 '.self::DEFAULT_DB_TABLE.' s
                LEFT JOIN ads a on a.id = s.ad_id
                WHERE
                s.user_id = '. $userid.' AND s.id = \''.$id.'\'',
            TRUE);
    }
}
?>
