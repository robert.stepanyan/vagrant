<?php

Namespace Modules\Humpchies;

final class RegionModel{
    
    const DEFAULT_DB_CONFIG                         = '\Modules\Humpchies\Db\Default';
    
    
    /**
    * Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}

    /** 
    * 
    * 
    */
    public static function getAllRegions() {
        
        return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG, 'SELECT id, slug, name FROM cities_regions WHERE 1 = 1', FALSE, FALSE, ['enable' => true]);
    }
    
    public static function getRegionDetails($slug) {
        
        $all_regions = self::getAllRegions();
        $found = array_filter($all_regions, function($v,$k) use ($slug){
            return $v['slug'] == $slug;
        },ARRAY_FILTER_USE_BOTH); 
        
        $found = array_values($found);
        return $found[0];
        
    }
    
    public static function getSearchRegions($search_term = null) {
    
        $regionWhere = ($search_term != '')? ' `name` LIKE "%' . $search_term . '%"' : '';
        
        $sql = 'SELECT id, slug, name FROM cities_regions WHERE ' . $regionWhere . ' ORDER BY name ASC';
        $return =  \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG, $sql, FALSE, FALSE, ['enable' => false]);  
        
        return $return;
        
         
    }
        
}
?>
