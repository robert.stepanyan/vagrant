<?php

Namespace Modules\Humpchies;

final class SearchesModel{
    
    const DEFAULT_DB_CONFIG                         = '\Modules\Humpchies\Db\Default';
    
    
    /**
    * Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}

    /** 
    * 
    * 
    */
   
    public static function getSearchDetails($keyword) {
        
        return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG, 'SELECT id, keyword, redirect FROM searches WHERE keyword = "' . $keyword . '"', FALSE, FALSE, ['enable' => false]);
    }
    
    
        
}
?>
