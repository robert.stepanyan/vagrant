<?php
  Namespace Modules\Humpchies;

  final class UserCookiesModel{
    // CONSTANTS
    
    const DEFAULT_DB_CONFIG                     = '\Modules\Humpchies\Db\Default';      // The config file to use by default
    const DEFAULT_DB_TABLE                      = 'user_cookies';                    // The default table to use
    
    private function __construct()
    {}

    /**
     * Disable object copying
     */
    private function __clone()
    {}

    /**
     * Disable object serialization
     */
    private function __wakeup()
    {}

    /**
     * Disable object deserialization
     */
    private function __sleep()
    {}

    public static function addCookie($cookie, $user_id){
       // \Core\library\Log::debug($clean,"campuri");
        $clean['user_id']     = $user_id;
        $clean['cookie']      = $cookie;
        $clean['access_date'] = date("Y-m-d H:i:s");
       // \Core\library\Log::debug($clean,"$clean");
        
        $new_signature = \Core\DBConn::insertIgnore(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $clean);

        if($new_signature !== FALSE)
            return $new_signature;

        $last_db_error = \Core\DBConn::getLastError(self::DEFAULT_DB_CONFIG, TRUE);
        \Core\library\Log::debug($last_db_error,"last error");     
    }  
  }
