<?php
    Namespace Modules\Humpchies;

    final class UniqueCookieModel{
        // CONSTANTS
        
        const DEFAULT_DB_CONFIG                     = '\Modules\Humpchies\Db\Default';      // The config file to use by default
        const DEFAULT_DB_TABLE                      = 'unique_cookies';                    // The default table to use
        
        private function __construct()
        {}

        /**
         * Disable object copying
         */
        private function __clone()
        {}

        /**
         * Disable object serialization
         */
        private function __wakeup()
        {}

        /**
         * Disable object deserialization
         */
        private function __sleep()
        {}
        
        public function checkUnique($code){
              
            $sql = "SELECT id from unique_cookies where `unique_string` = '{$code}'";
            $row = \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG, $sql);
            return !!empty($row['id']);
        }
        
        public function saveUnique($code){
            
            $varsa = array('unique_string' => $code);
            
            $new_key = \Core\DBConn::insertIgnore(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $varsa);

            if($new_key !== FALSE)
                return $new_key;

            $last_db_error = \Core\DBConn::getLastError(self::DEFAULT_DB_CONFIG, TRUE);
            \Core\library\Log::debug($last_db_error,"last error");     
        }      
    }
?>
