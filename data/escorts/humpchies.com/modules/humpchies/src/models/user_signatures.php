<?php
  Namespace Modules\Humpchies;

  final class UserSignaturesModel{
    // CONSTANTS
    
    const DEFAULT_DB_CONFIG                     = '\Modules\Humpchies\Db\Default';      // The config file to use by default
    const DEFAULT_DB_TABLE                      = 'user_signatures';                    // The default table to use
    
    private function __construct()
    {}

    /**
     * Disable object copying
     */
    private function __clone()
    {}

    /**
     * Disable object serialization
     */
    private function __wakeup()
    {}

    /**
     * Disable object deserialization
     */
    private function __sleep()
    {}

    public static function addSignature($signature, $user_id, $fields = []){
        $dbfields = array('ip', "userAgent", "webDriver", "language", "colorDepth", "deviceMemory", "hardwareConcurrency", "screenResolution", "availableScreenResolution", "timezoneOffset", "timezone", "sessionStorage", "localStorage", "indexedDb", "addBehavior", "openDatabase", "cpuClass", "platform", "plugins", "canvas", "webgl", "webglVendorAndRenderer", "adBlock", "hasLiedLanguages", "hasLiedResolution", "hasLiedOs", "hasLiedBrowser", "touchSupport", "fonts", "audio");
       

        $clean = array();
        foreach($fields as $field ){
            
        if(in_array($field['key'],$dbfields)){
                $clean[$field['key']] = serialize($field['value']);
            }    
        };
        
        
       // \Core\library\Log::debug($clean,"campuri");
        $clean['user_id'] = $user_id;
        $clean['signature'] = $signature;
        $clean['access_date'] = date("Y-m-d H:i:s");
        //\Core\library\Log::debug($clean,"campuri");
        
        $new_signature = \Core\DBConn::insertIgnore(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $clean);

        if($new_ad_id !== FALSE)
            return $new_ad_id;

        $last_db_error = \Core\DBConn::getLastError(self::DEFAULT_DB_CONFIG, TRUE);
        \Core\library\Log::debug($last_db_error,"last error");     
    }  
  }
