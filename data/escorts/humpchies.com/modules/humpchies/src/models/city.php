<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to interact with city related items and tasks
 */

Namespace Modules\Humpchies;

final class CityModel{
    
    const DEFAULT_DB_CONFIG                         = '\Modules\Humpchies\Db\Default';
    const DEFAULT_CITY                              = 'Montr�al';
    
    private static $supported_cities                = NULL;
    private static $cities_with_ads                 = NULL;
    private static $cities_zones                    = NULL;
    private static $all_cities_zones                = NULL;
    
    // CONSTANTS
    // =========
    const ERROR_CITY_IS_INVALID                     = 'Modules.Humpchies.CityModel.ErrorCityIsInvalid';
    const ERROR_CITY_IS_EMPTY                       = 'Modules.Humpchies.CityModel.ErrorCityIsEmpty';
    const NO_ERROR                                  = 'Modules.Humpchies.CityModel.NoError';
    

    /**
    * Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}

    public static function getCitiesByZones() {
        
        if(self::$cities_zones !== NULL)
            return self::$cities_zones;
            
        self::$cities_zones = [
            
            'montreal_metro' => [
                'region_name' => 'Montr&eacute;al Metro',
                'cities' => [
                'cote-saint-luc'                => 'C&ocirc;te-Saint-Luc',
                'dollard-des-ormeaux'           => 'Dollard-des-Ormeaux',
                'dorval'                        => 'Dorval',
                'lasalle'                       => 'LaSalle',
                'lachine'                       => 'Lachine',
                'mont-royal'                    => 'Mont-Royal',
                'montreal'                      => 'Montr&eacute;al',
                'montreal-est'                  => 'Montr&eacute;al-Est',
                'montreal-nord'                 => 'Montr&eacute;al-Nord',
                'montreal-ouest'                => 'Montr&eacute;al-Ouest',
                'pierrefonds'                   => 'Pierrefonds',
                'pointe-claire'                 => 'Pointe-Claire',
                'roxboro'                       => 'Roxboro',
                'saint-laurent'                 => 'Saint-Laurent',
                'saint-leonard'                 => 'Saint-L&eacute;onard',
                'verdun'                        => 'Verdun',                
                'kirkland'                      => 'Kirkland',
                "baie-d-urfe"                   => "Baie-d'urfe",
                'westmount'                     => 'Westmount',
                'pointe-aux-trembles'           => 'Pointe-aux-trembles',
                'outremont'                     => 'Outremont',
                'anjou'                         => 'Anjou'
                ]
            ],
            
            'monteregie' => [
                'region_name' => 'Mont&eacute;r&eacute;gie',
                'cities' => [
                'acton-vale'                    => 'Acton Vale',
                'beloeil'                       => 'Beloeil',
                'boucherville'                  => 'Boucherville',
                'bromont'                       => 'Bromont',
                'brossard'                      => 'Brossard',
                'candiac'                       => 'Candiac',
                'carignan'                      => 'Carignan',
                'chambly'                       => 'Chambly',
                'chateauguay'                   => 'Ch&acirc;teauguay',
                'contrecoeur'                   => 'Contrecoeur',
                'delson'                        => 'Delson',
                'granby'                        => 'Granby',
                'greenfield-park'               => 'Greenfield-Park',
                'la-prairie'                    => 'La-Prairie',
                'lemoyne'                       => 'LeMoyne',
                'les-coteaux'                   => 'Les Coteaux',
                'longueuil'                     => 'Longueuil',
                'marieville'                    => 'Marieville',
                'mcmasterville'                 => 'McMasterville',
                'mont-saint-hilaire'            => 'Mont-Saint-Hilaire',
                'napierville'                   => 'Napierville',
                'richelieu'                     => 'Richelieu',
                'rigaud'                        => 'Rigaud',
                'saint-blaise-sur-richelieu'    => 'Saint-Blaise-sur-Richelieu',
                'saint-constant'                => 'Saint-Constant',
                'saint-hubert'                  => 'Saint-Hubert',
                'saint-hyacinthe'               => 'Saint-Hyacinthe',
                'saint-jean-baptiste'           => 'Saint-Jean-Baptiste',
                'saint-jean-sur-richelieu'      => 'Saint-Jean-sur-Richelieu',
                'saint-lambert'                 => 'Saint-Lambert',
                'saint-lazare'                  => 'Saint-Lazare',
                'saint-mathias-sur-richelieu'   => 'Saint-Mathias-sur-Richelieu',
                'sainte-angele-de-monnoir'      => 'Sainte-Ang&egrave;le-de-Monnoir',
                'saint-zotique'                 => 'Saint-Zotique',
                'sainte-julie'                  => 'Sainte-Julie',
                'salaberry-de-valleyfield'      => 'Salaberry-de-Valleyfield',
                'sorel-tracy'                   => 'Sorel-Tracy',
                'varennes'                      => 'Varennes',
                'vaudreuil-dorion'              => 'Vaudreuil-Dorion',
                'yamaska'                       => 'Yamaska',
                'otterburn-park'                => 'Otterburn-park',
                'saint-basile-le-grand'         => 'Saint-basile-le-grand',
                'lery'                          => 'lery',
                'mercier'                       => 'Mercier',
                    'sainte-catherine'              => 'Sainte-catherine'
                ]    
            ],
            
            'saguenay-lac-saint-jean' => [
                'region_name' => 'Saguenay-Lac-Saint-Jean',
                'cities' => [
                'alma'                          => 'Alma',
                'chicoutimi'                    => 'Chicoutimi',
                'jonquiere'                     => 'Jonquiere',
                'saguenay'                      => 'Saguenay'
                ]
            ],
        
            'cote-nord' => [
                'region_name' => 'C&ocirc;te-Nord',
                'cities' => [
                'baie-comeau'                   => 'Baie-Comeau',
                'fermont'                       => 'Fermont',
                'longue-rive'                   => 'Longue-Rive',
                    'sept-iles'                     => 'Sept-Iles'
                ]
            ],
            
            'capitale-nationale' => [
                'region_name' => 'Capitale Nationale',
                'cities' => [
                'beauport'                      => 'Beauport',
                'charlesbourg'                  => 'Charlesbourg',
                'pont-rouge'                    => 'Pont-Rouge',
                'portneuf'                      => 'Portneuf',
                'quebec'                        => 'Quebec',
                'saint-augustin'                => 'Saint-Augustin',
                'saint-basile'                  => 'Saint-Basile',
                'saint-gabriel-de-valcartier'   => 'Saint-Gabriel-de-Valcartier',
                'sainte-foy'                    => 'Sainte-Foy',
                    'l-ancienne-lorette'            => "L'Ancienne-Lorette"                ]
            ],
            
            'laurentides' => [
                'region_name' => 'Laurentides',
                'cities' => [
                'blainville'                    => 'Blainville',
                'bois-des-filion'               => 'Bois-des-Filion',
                'boisbriand'                    => 'Boisbriand',
                'deux-montagnes'                => 'Deux-Montagnes',
                'mirabel'                       => 'Mirabel',
                'mont-laurier'                  => 'Mont-Laurier',
                'mont-tremblant'                => 'Mont-Tremblant',
                'pointe-calumet'                => 'Pointe-Calumet',
                'rosemere'                      => 'Rosem&egrave;re',
                'saint-hippolyte'               => 'Saint-Hippolyte',
                'saint-jerome'                  => 'Saint-J&eacute;r&ocirc;me',
                'saint-sauveur'                 => 'Saint-Sauveur',
                'sainte-marthe-sur-le-lac'      => 'Sainte-Marthe-sur-le-Lac',
                'sainte-sophie'                 => 'Sainte-Sophie',
                'sainte-therese'                => 'Sainte-Th&eacute;r&egrave;se',
                'sainte-agathe-des-monts'       => 'Sainte-Agathe-des-Monts',
                'sainte-anne-des-lacs'          => 'Sainte-Anne-des-Lacs',
                'lorraine'                      => 'Lorraine',
                'oka'                           => 'Oka',
                'saint-joseph-du-lac'           => 'Saint-joseph-du-Lac',
                'sainte-anne-des-plaines'       => 'Sainte-anne-des-plaines',
                    'saint-eustache'                => 'Saint-Eustache',
                ]    
            ],
            
            'bas-saint-laurent' => [
                'region_name' => 'Bas-Saint-Laurent',
                'cities' => [
                'rimouski'                      => 'Rimouski',
                'riviere-du-loup'               => 'Riviere-du-Loup',
                'matane'                        => 'Matane'
                ]
            ],
            
            "a-l-exterieur-du-quebec" => [
                'region_name' => "A l'exterieur du Quebec",
                'cities' => [
                'burlington'                    => 'Burlington',
                'campbellton'                   => 'Campbellton',
                'cornwall'                      => 'Cornwall',
                'edmundston'                    => 'Edmundston',
                'gatineau'                      => 'Gatineau',
                'hawkesbury'                    => 'Hawkesbury',
                'lac-simon'                     => 'Lac-Simon',
                'ottawa'                        => 'Ottawa',
                'plattsburgh'                   => 'Plattsburgh',
                    'massena'                       => 'Massena'   
                ]
            ],
            
            'mauricie' => [
                'region_name' => "Mauricie",
                'cities' => [
                'cap-de-la-madeleine'           => 'Cap-de-la-Madeleine',
                'shawinigan'                    => 'Shawinigan',
                'trois-rivieres'                => 'Trois-Rivieres',
                'trois-tivieres-ouest'          => 'Trois-Rivieres-Ouest'
                ]
            ],
            
            'chaudiere-appalaches' => [
                'region_name' => "Chaudi&egrave;re-Appalaches",
                'cities' => [
                'charny'                        => 'Charny',
                'levis'                         => 'Levis',
                'saint-georges'                 => 'Saint-Georges',
                'sainte-marguerite'             => 'Sainte-Marguerite',
                'sainte-marie'                  => 'Sainte-Marie',
                'thetford-mines'                => 'Thetford-Mines',
                'montmagny'                     => 'Montmagny'
                ]
            ],
            
            'nord-du-quebec' => [
                'region_name' => "Nord-du-Qu&eacute;bec",
                'cities' => [
                'chibougamau'                   => 'Chibougamau'
                ]
            ],
            
            'centre-du-quebec' => [
                'region_name' => "Centre-du-Qu&eacute;bec",
                'cities' => [
                'drummondville'                 => 'Drummondville',
                'durham-sud'                    => 'Durham-Sud',
                'pierreville'                   => 'Pierreville',
                'saint-cyrille-de-wendover'     => 'Saint-Cyrille-de-Wendover',
                'victoriaville'                 => 'Victoriaville',
                'becancour'                     => 'Becancour',
                'kingsey-falls'                 => 'Kingsey-falls',
                'nicolet'                       => 'Nicolet',
                'plessisville'                  => 'Plessisville',
                'princeville'                   => 'Princeville',
                'saint-germain-de-grantham'     => 'Saint-germain-de-grantham',
                'warwick'                       => 'Warwick'
                ]
            ],
            
            'laval' => [
                'region_name' => "Laval",
                'cities' => [
                'laval'                         => 'Laval'
                ]
            ],
            
            'estrie' => [
                'region_name' => "Estrie",
                'cities' => [
                'magog'                         => 'Magog',
                'newport'                       => 'Newport',
                'sherbrooke'                    => 'Sherbrooke'
                ]
            ],
            
            'outaouais' => [
                'region_name' => "Outaouais",
                'cities' => [
                'montpellier'                   => 'Montpellier'
                ]
            ],
            
            'abitibi-temiscamingue' => [
                'region_name' => "Abitibi-T&eacute;miscamingue",
                'cities' => [
                'rouyn-noranda'                 => 'Rouyn-Noranda',
                    'val-d-or'                      => "Val-d'Or"
                ]
            ],
            
            'lanaudiere' => [
                'region_name' => "Lanaudi&egrave;re",
                'cities' => [
                'berthierville'                 => 'Berthierville',
                'charlemagne'                   => 'Charlemagne',
                'joliette'                      => 'Joliette',
                "l-assomption"                  => "L'Assomption",
                "l-epiphanie"                   => "L'Epiphanie",
                'lanoraie'                      => 'Lanoraie',
                'lavaltrie'                     => 'Lavaltrie',
                'mascouche'                     => 'Mascouche',
                'rawdon'                        => 'Rawdon',
                'repentigny'                    => 'Repentigny',
                'saint-esprit'                  => 'Saint-Esprit',
                'saint-lin-laurentides'         => 'Saint-Lin-Laurentides',
                'terrebonne'                    => 'Terrebonne',
                'saint-sulpice'                 => 'Saint-sulpice'
                ]
            ]            
        ];
    
        return self::$cities_zones;            
    }
    
    
    public static function getCitiesRegions($region_id = null, $with_ads = false) {
    
        $all_cities = [];
        $all_regions =  \Modules\Humpchies\RegionModel::getAllRegions();
        
        if($region_id != null) {
                
                // idk if im using that yet
        } else {
            
            foreach($all_regions as $region_id => $region) {
                
                $all_cities[$region['slug']] = [
                    'region' => $region,
                    'cities' => self::getAllCities($region["id"]) // if we want all the cities with ads, just call a different fucntion. to do after the quesry   
                ]; 
            }
        }
        
        return $all_cities;
    }
    
    
    
    public static function getAllCities($region_id = null, $search_term = null) {
        
        $regionWhere = ($region_id > 0)? ' `region_id` = ' . $region_id : '`region_id` > 0';
        $regionWhere .= ($search_term != '')? ' AND `name` LIKE "%' . $search_term . '%"' : '';
        
        $sql = 'SELECT id, region_id, slug, name, lat, lng FROM cities WHERE ' . $regionWhere . ' ORDER BY name ASC';
        $return =  \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG, $sql, FALSE, FALSE, ['enable' => false]);  
        
         return $return;
        
         
    }
    
    public static function getCityDetails($slug) {
           
        $all_cities = self::getAllCities();
        $found = array_filter($all_cities, function($v,$k) use ($slug){
            return $v['slug'] == $slug;
        },ARRAY_FILTER_USE_BOTH); 
        
        $found = array_values($found);
        return $found[0];
    }
    
    
    
    /***
    * put your comment there...
    * 
    */
    public static function getSupportedCities()
    {
        if(self::$supported_cities !== NULL)
            return self::$supported_cities;
            
        /*self::$supported_cities = array('acton-vale'                    => 'Acton Vale',
                                        'alma'                          => 'Alma',
                                        'anjou'                         => 'Anjou',
                                        'baie-comeau'                   => 'Baie-Comeau',
                                        'beauport'                      => 'Beauport',
                                        'beloeil'                       => 'Beloeil',
                                        'berthierville'                 => 'Berthierville',
                                        'blainville'                    => 'Blainville',
                                        'bois-des-filion'               => 'Bois-des-Filion',
                                        'boisbriand'                    => 'Boisbriand',
                                        'boucherville'                  => 'Boucherville',
                                        'bromont'                       => 'Bromont',
                                        'brossard'                      => 'Brossard',
                                        'burlington'                    => 'Burlington',
                                        'campbellton'                   => 'Campbellton',
                                        'candiac'                       => 'Candiac',
                                        'cap-de-la-madeleine'           => 'Cap-de-la-Madeleine',
                                        'carignan'                      => 'Carignan',
                                        'chambly'                       => 'Chambly',
                                        'charlemagne'                   => 'Charlemagne',
                                        'charny'                        => 'Charny',
                                        'chibougamau'                   => 'Chibougamau',
                                        'chicoutimi'                    => 'Chicoutimi',
                                        'chateauguay'                   => 'Ch�teauguay',
                                        'contrecoeur'                   => 'Contrecoeur',
                                        'cornwall'                      => 'Cornwall',
                                        'cote-saint-luc'                => 'C�te-Saint-Luc',
                                        'delson'                        => 'Delson',
                                        'deux-montagnes'                => 'Deux-Montagnes',
                                        'dollard-des-ormeaux'           => 'Dollard-des-Ormeaux',
                                        'dorval'                        => 'Dorval',
                                        'drummondville'                 => 'Drummondville',
                                        'durham-sud'                    => 'Durham-Sud',
                                        'edmundston'                    => 'Edmundston',
                                        'fermont'                       => 'Fermont',
                                        'gatineau'                      => 'Gatineau',
                                        'granby'                        => 'Granby',
                                        'greenfield-park'               => 'Greenfield Park',
                                        'hawkesbury'                    => 'Hawkesbury',
                                        'joliette'                      => 'Joliette',
                                        'jonquiere'                     => 'Jonqui�re',
                                        'l-ancienne-lorette'            => "L'Ancienne-Lorette",
                                        'l-assomption'                  => "L'Assomption",
                                        'l-epiphanie'                   => "L'�piphanie",
                                        'la-prairie'                    => 'La Prairie',
                                        'lasalle'                       => 'LaSalle',
                                        'lac-simon'                     => 'Lac-Simon',
                                        'lachine'                       => 'Lachine',
                                        'lanoraie'                      => 'Lanoraie',
                                        'laval'                         => 'Laval',
                                        'lemoyne'                       => 'LeMoyne',
                                        'longue-rive'                   => 'Longue-Rive',
                                        'longueuil'                     => 'Longueuil',
                                        'levis'                         => 'L�vis',
                                        'magog'                         => 'Magog',
                                        'marieville'                    => 'Marieville',
                                        'mascouche'                     => 'Mascouche',
                                        'matane'                        => 'Matane',
                                        'mcmasterville'                 => 'McMasterville',
                                        'mirabel'                       => 'Mirabel',
                                        'mont-laurier'                  => 'Mont-Laurier',
                                        'mont-royal'                    => 'Mont-Royal',
                                        'mont-saint-hilaire'            => 'Mont-Saint-Hilaire',
                                        'montpellier'                   => 'Montpellier',
                                        'montreal'                      => 'Montr�al',
                                        'montreal-est'                  => 'Montr�al-Est',
                                        'montreal-nord'                 => 'Montr�al-Nord',
                                        'montreal-ouest'                => 'Montr�al-Ouest',
                                        'napierville'                   => 'Napierville',
                                        'ottawa'                        => 'Ottawa',
                                        'pierrefonds'                   => 'Pierrefonds',
                                        'pierreville'                   => 'Pierreville',
                                        'pointe-calumet'                => 'Pointe-Calumet',
                                        'pont-rouge'                    => 'Pont-Rouge',
                                        'portneuf'                      => 'Portneuf',
                                        'quebec'                        => 'Qu�bec',
                                        'rawdon'                        => 'Rawdon',
                                        'repentigny'                    => 'Repentigny',
                                        'richelieu'                     => 'Richelieu',
                                        'rigaud'                        => 'Rigaud',
                                        'rimouski'                      => 'Rimouski',
                                        'riviere-du-loup'               => 'Rivi�re-du-Loup',
                                        'rosemere'                      => 'Rosem�re',
                                        'saint-basile'                  => 'Saint-Basile',
                                        'saint-blaise-sur-richelieu'    => 'Saint-Blaise-sur-Richelieu',
                                        'saint-constant'                => 'Saint-Constant',
                                        'saint-cyrille-de-wendover'     => 'Saint-Cyrille-de-Wendover',
                                        'saint-esprit'                  => 'Saint-Esprit',
                                        'saint-eustache'                => 'Saint-Eustache',
                                        'saint-gabriel-de-valcartier'   => 'Saint-Gabriel-de-Valcartier',
										'saint-georges'                 => 'Saint-Georges',	
                                        'saint-hubert'                  => 'Saint-Hubert',
                                        'saint-hyacinthe'               => 'Saint-Hyacinthe',
                                        'saint-jean-baptiste'           => 'Saint-Jean-Baptiste',
                                        'saint-jean-sur-richelieu'      => 'Saint-Jean-sur-Richelieu',
                                        'saint-jerome'                  => 'Saint-J�r�me',
                                        'saint-lambert'                 => 'Saint-Lambert',
                                        'saint-laurent'                 => 'Saint-Laurent',
                                        'saint-lin-laurentides'         => 'Saint-Lin-Laurentides',
                                        'saint-leonard'                 => 'Saint-L�onard',
                                        'saint-sauveur'                 => 'Saint-Sauveur',
                                        'sainte-angele-de-monnoir'      => 'Sainte-Ang�le-de-Monnoir',
                                        'sainte-foy'                    => 'Sainte-Foy',
                                        'sainte-marguerite'             => 'Sainte-Marguerite',
                                        'sainte-marthe-sur-le-lac'      => 'Sainte-Marthe-sur-le-Lac',
                                        'sainte-sophie'                 => 'Sainte-Sophie',
                                        'sainte-therese'                => 'Sainte-Th�r�se',
                                        'salaberry-de-valleyfield'      => 'Salaberry-de-Valleyfield',
                                        'sept-iles'                     => 'Sept-�les',
                                        'shawinigan'                    => 'Shawinigan',
                                        'sherbrooke'                    => 'Sherbrooke',
                                        'terrebonne'                    => 'Terrebonne',
                                        'thetford-mines'                => 'Thetford-Mines',
                                        'trois-rivieres'                => 'Trois-Rivi�res',
                                        'trois-rivieres-ouest'          => 'Trois-Rivi�res-Ouest',
                                        'val-d-or'                      => "Val-d'Or",
                                        'vaudreuil-dorion'              => 'Vaudreuil-Dorion',
                                        'verdun'                        => 'Verdun',
                                        'victoriaville'                 => 'Victoriaville',
                                        'yamaska'                       => 'Yamaska');*/
        
        self::$supported_cities = array(
		'acton-vale'                    => 'Acton Vale',
		'alma'                          => 'Alma',
		'anjou'                         => 'Anjou',
		'baie-comeau'                   => 'Baie-Comeau',
		'beauport'                      => 'Beauport',
		'beloeil'                       => 'Beloeil',
		'berthierville'                 => 'Berthierville',
		'blainville'                    => 'Blainville',
		'bois-des-filion'               => 'Bois-des-Filion',
		'boisbriand'                    => 'Boisbriand',
		'boucherville'                  => 'Boucherville',
		'bromont'                       => 'Bromont',
		'brossard'                      => 'Brossard',
		'burlington'                    => 'Burlington',
		'campbellton'                   => 'Campbellton',
		'candiac'                       => 'Candiac',
		'cap-de-la-madeleine'           => 'Cap-de-la-Madeleine',
		'carignan'                      => 'Carignan',
		'chambly'                       => 'Chambly',
		'charlemagne'                   => 'Charlemagne',
		'charlesbourg'					=> 'Charlesbourg',
		'charny'                        => 'Charny',
		'chibougamau'                   => 'Chibougamau',
		'chicoutimi'                    => 'Chicoutimi',
		'chateauguay'                   => 'Ch�teauguay',
		'contrecoeur'                   => 'Contrecoeur',
		'cornwall'                      => 'Cornwall',
		'cote-saint-luc'                => 'C�te-Saint-Luc',
		'delson'                        => 'Delson',
		'deux-montagnes'                => 'Deux-Montagnes',
		'dollard-des-ormeaux'           => 'Dollard-des-Ormeaux',
		'dorval'                        => 'Dorval',
		'drummondville'                 => 'Drummondville',
		'durham-sud'                    => 'Durham-Sud',
		'edmundston'                    => 'Edmundston',
		'fermont'                       => 'Fermont',
		'gatineau'                      => 'Gatineau',
		'granby'                        => 'Granby',
		'greenfield-park'               => 'Greenfield-Park',
		'hawkesbury'                    => 'Hawkesbury',
		'joliette'                      => 'Joliette',
		'jonquiere'                     => 'Jonqui�re',
		'l-ancienne-lorette'            => "L'Ancienne-Lorette",
		'l-assomption'                  => "L'Assomption",
		'l-epiphanie'                   => "L'�piphanie",
		'la-prairie'                    => 'La-Prairie',
		'lasalle'                       => 'LaSalle',
		'lac-simon'                     => 'Lac-Simon',
		'lachine'                       => 'Lachine',
		'lanoraie'                      => 'Lanoraie',
		'laval'                         => 'Laval',
		'lavaltrie'						=> 'Lavaltrie',
		'lemoyne'                       => 'LeMoyne',
		'les-coteaux'					=> 'Les Coteaux',
		'longue-rive'                   => 'Longue-Rive',
		'longueuil'                     => 'Longueuil',
		'levis'                         => 'L�vis',
		'magog'                         => 'Magog',
		'marieville'                    => 'Marieville',
		'mascouche'                     => 'Mascouche',
		'matane'                        => 'Matane',
		'massena'						=> 'Massena',
		'mcmasterville'                 => 'McMasterville',
		'mirabel'                       => 'Mirabel',
		'mont-laurier'                  => 'Mont-Laurier',
		'mont-royal'                    => 'Mont-Royal',
		'mont-saint-hilaire'            => 'Mont-Saint-Hilaire',
		'mont-tremblant'				=> 'Mont-Tremblant',
		'montpellier'                   => 'Montpellier',
		'montreal'                      => 'Montr�al',
		'montreal-est'                  => 'Montr�al-Est',
		'montreal-nord'                 => 'Montr�al-Nord',
		'montreal-ouest'                => 'Montr�al-Ouest',
		'napierville'                   => 'Napierville',
		'newport'						=> 'Newport',
		'ottawa'                        => 'Ottawa',
		'pierrefonds'                   => 'Pierrefonds',
		'pierreville'                   => 'Pierreville',
		'plattsburgh'					=> 'Plattsburgh',
		'pointe-calumet'                => 'Pointe-Calumet',
		'pointe-claire'					=> 'Pointe-Claire',
		'pont-rouge'                    => 'Pont-Rouge',
		'portneuf'                      => 'Portneuf',
		'quebec'                        => 'Qu�bec',
		'rawdon'                        => 'Rawdon',
		'repentigny'                    => 'Repentigny',
		'richelieu'                     => 'Richelieu',
		'rigaud'                        => 'Rigaud',
		'rimouski'                      => 'Rimouski',
		'riviere-du-loup'               => 'Rivi�re-du-Loup',
		'rosemere'                      => 'Rosem�re',
		'rouyn-noranda'					=> 'Rouyn-Noranda',
		'roxboro'						=> 'Roxboro',
		'saguenay'						=> 'Saguenay',
		'saint-augustin'				=> 'Saint-Augustin',
		'saint-basile'                  => 'Saint-Basile',
		'saint-blaise-sur-richelieu'    => 'Saint-Blaise-sur-Richelieu',
		'saint-constant'                => 'Saint-Constant',
		'saint-cyrille-de-wendover'     => 'Saint-Cyrille-de-Wendover',
		'saint-esprit'                  => 'Saint-Esprit',
		'saint-eustache'                => 'Saint-Eustache',
		'saint-gabriel-de-valcartier'   => 'Saint-Gabriel-de-Valcartier',
		'saint-georges'                 => 'Saint-Georges',
		'saint-hippolyte'				=> 'Saint-Hippolyte',
		'saint-hubert'                  => 'Saint-Hubert',
		'saint-hyacinthe'               => 'Saint-Hyacinthe',
		'saint-jean-baptiste'           => 'Saint-Jean-Baptiste',
		'saint-jean-sur-richelieu'      => 'Saint-Jean-sur-Richelieu',
		'saint-jerome'                  => 'Saint-J�r�me',
		'saint-lambert'                 => 'Saint-Lambert',
		'saint-laurent'                 => 'Saint-Laurent',
		'saint-lazare'					=> 'Saint-Lazare',
		'saint-lin-laurentides'         => 'Saint-Lin-Laurentides',
		'saint-leonard'                 => 'Saint-L�onard',
		'saint-mathias-sur-richelieu'   => 'Saint-Mathias-sur-Richelieu',
		'saint-sauveur'                 => 'Saint-Sauveur',
		'sainte-angele-de-monnoir'      => 'Sainte-Ang�le-de-Monnoir',
		'sainte-foy'                    => 'Sainte-Foy',
		'sainte-marguerite'             => 'Sainte-Marguerite',
		'sainte-marthe-sur-le-lac'      => 'Sainte-Marthe-sur-le-Lac',
		'sainte-sophie'                 => 'Sainte-Sophie',
		'sainte-therese'                => 'Sainte-Th�r�se',
		'saint-zotique'					=> 'Saint-Zotique',
		'sainte-agathe-des-monts'		=> 'Sainte-Agathe-des-Monts',
		'sainte-anne-des-lacs'			=> 'Sainte-Anne-des-Lacs',
		'sainte-julie'					=> 'Sainte-Julie',
		'sainte-marie'					=> 'Sainte-Marie',
		'salaberry-de-valleyfield'      => 'Salaberry-de-Valleyfield',
		'sept-iles'                     => 'Sept-�les',
		'shawinigan'                    => 'Shawinigan',
		'sherbrooke'                    => 'Sherbrooke',
		'sorel-tracy'					=> 'Sorel-Tracy',
		'terrebonne'                    => 'Terrebonne',
		'thetford-mines'                => 'Thetford-Mines',
		'trois-rivieres'                => 'Trois-Rivi�res',
		'trois-rivieres-ouest'          => 'Trois-Rivi�res-Ouest',
		'val-d-or'                      => "Val-d'Or",
		'varennes'						=> 'Varennes',
		'vaudreuil-dorion'              => 'Vaudreuil-Dorion',
		'verdun'                        => 'Verdun',
		'victoriaville'                 => 'Victoriaville',
		'yamaska'                       => 'Yamaska'
	);
                                        
        if(!self::getCitiesWithAds())
            return self::$supported_cities;
            
        self::$supported_cities = array_merge(self::$supported_cities, self::getCitiesWithAds());
        
        asort(self::$supported_cities);
               
        return self::$supported_cities;
    }
    
    public static function getSupportedCitiesV2( $only_slug_title = false ){
        $selecting_items = ( $only_slug_title ) ? 'c.slug, c.name' : 'c.*';

        return \Core\DBConn::select(self::DEFAULT_DB_CONFIG,
            'SELECT ' . $selecting_items . ' FROM cities c',
            FALSE
        );
    }
    
    public static function getCitiesSlugNames(){
        
        $data = array();
//        $cities_data = self::getSupportedCitiesV2(true);
        $cities_data = self::getAllCities();
        
        foreach($cities_data as $k => $v){
            $data[$v['slug']] = $v['name'];
        }
        
        return $data;
    }
    
    public static function getCitiesByZoneId($zone_id){
        return \Core\DBConn::select(self::DEFAULT_DB_CONFIG,
            'SELECT * FROM cities WHERE zone_id = ' . $zone_id,
            FALSE
        );
    }
    
    public static function getZoneByCityIdOrName($city_id_or_name){
        return \Core\DBConn::select(self::DEFAULT_DB_CONFIG,
            'SELECT z.id, z.name FROM zones
            INNER JOIN cities c ON c.zone_id = z.id
            WHERE c.id = ' . $city_id . ' OR c.name = ' . $city_id,
            FALSE
        );
    }
    
    public static function getCitiesWithAds($category_code = NULL)
    {   
        if(@self::$cities_with_ads[($category_name = ($category_code === NULL ? 'all' : $category_code))] !== NULL)
            return self::$cities_with_ads[$category_name];
            
        if((self::$cities_with_ads[$category_name] = \Core\Utils\Data::findBySQL(self::DEFAULT_DB_CONFIG,
                                                                                'SELECT 
                                                                                        city 
                                                                                FROM 
                                                                                        ads 
                                                                                WHERE 
                                                                                        ' . ($category_code === NULL ? '' : 'category = \'' . $category_code . '\' AND ') . ' 
                                                                                        status = \'active\' 
                                                                                        AND
                                                                                        city <> \'\'
                                                                                        AND 
                                                                                        date_released <= ' . \Core\Utils\Time::getCommonTimeStamp() . ' 
                                                                                GROUP BY 
                                                                                        city')) === FALSE)
                                                                                return (self::$cities_with_ads[$category_name] = FALSE);
        
        $x = count(self::$cities_with_ads[$category_name]);
        
        for($y = 0; $y < $x  ; $y++)
            self::$cities_with_ads[$category_name][\Core\Utils\Text::stringToUrl(self::$cities_with_ads[$category_name][$y]['city'])] = self::$cities_with_ads[$category_name][$y]['city'];

        self::$cities_with_ads[$category_name] = array_slice(self::$cities_with_ads[$category_name], $x); 
        
        return self::$cities_with_ads[$category_name];
    }
    
    public static function isSupportedCityInputValid(&$city_code)
    {
        if(empty($city_code))
            return self::ERROR_CITY_IS_EMPTY;
//        elseif(@array_key_exists($city_code, self::getSupportedCities()))
        elseif(@array_key_exists($city_code, self::getCitiesSlugNames()))
            return self::NO_ERROR;
            
        $city_code = '';
        return self::ERROR_CITY_IS_INVALID;
    }
    
    public static function isCityWithAdsInputValid(&$city_code, &$category_code = NULL)
    {
        if(empty($city_code))
            return self::ERROR_CITY_IS_EMPTY;
        elseif(@array_key_exists($city_code, self::getCitiesWithAds($category_code)))
            return self::NO_ERROR;
          
        $city_code = NULL;
        return self::ERROR_CITY_IS_INVALID; 
    }
    
    public static function parseCityWithAdsCode($city_code, $category_name = NULL, $sanitize = TRUE)
    {
        if($city_code === NULL)
            return NULL;
        elseif(($cities_with_ads = self::getCitiesWithAds($category_name)) === FALSE) 
            return NULL;
       
        if($sanitize === FALSE)
            return @$cities_with_ads[$city_code];
             
        return \Core\Utils\Security::sanitizeValue(@$cities_with_ads[$city_code]);
    }
    
    public static function parseSupportedCityCode($city_code)
    {
//        $supported_cities = self::getSupportedCities();
        $supported_cities = self::getCitiesSlugNames();
        return @$supported_cities[$city_code]; 
    }
}
?>
