<?php
Namespace Modules\Humpchies;

final class SeoDescriptionModel{

    // CONSTANTS
    // =========
    const DEFAULT_DB_CONFIG       = '\Modules\Humpchies\Db\Default';
    const DEFAULT_DB_TABLE        = 'seo_description';

                                                              
    private static $redisKeyCity      = "SeoDescriptionCity";
    private static $redisKeyRegion    = "SeoDescriptionRegion";
    
    
    /**
     * Because this class is untended to be a singleton we block access to its constructor.
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}

    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}

    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}
  
    
    public static function getAllCities() {
        
        $citiesList = \Core\library\Redis::get(self::$redisKeyCity);
        //$citiesList = false;
        if($citiesList){
            return unserialize($citiesList);
        }
        //exit("here");
        
        $citiesList = \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT `city_id`, `page`, `en`, `fr`, C.slug FROM seo_description SD 
                LEFT JOIN cities C ON SD.city_id = C.id
            WHERE  `city_id` > 0',
            TRUE,
            FALSE,
            array('enable' => FALSE));
        
        //var_dump($citiesList); exit();
        \Core\library\Redis::set(self::$redisKeyCity, serialize($citiesList));   
     
        //var_dump($tagsList);
        return $citiesList;
       
    }
    
    public static function getAllRegions() {
        
        $regionsList = \Core\library\Redis::get(self::$redisKeyRegion);
        //$citiesList = false;
        if($regionsList){
            return unserialize($regionsList);
        }
        //exit("here");
        
        $regionsList = \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT `region_id`, `page`, `en`, `fr`, CR.slug FROM seo_description SD 
                LEFT JOIN cities_regions CR ON SD.region_id = CR.id
            WHERE  `region_id` > 0',
            TRUE,
            FALSE,
            array('enable' => FALSE));
        
        //var_dump($citiesList); exit();
        \Core\library\Redis::set(self::$redisKeyRegion, serialize($regionsList));   
     
        //var_dump($tagsList);
        return $regionsList;
       
    }
    
      
    
    public static function getSEODescriptionCity($city, $page, $lang = 'fr') {
        
        $seo_descriptions = self::getAllCities();
        $description = '';
        
        if(empty($seo_descriptions))
            return $description;
        
        $custom_description = array_filter($seo_descriptions, function($description) use($page, $city ){
            return ($description['slug'] == $city) && ($description['page'] == $page);      
        });
        
        switch(true) {
            
            case count($custom_description) == 0:
                break;
            
            case count($custom_description) == 1:
                reset($custom_description);
                $first_key = key($custom_description);
                $description = $custom_description[$first_key][$lang];
                break;
            
            case count($custom_description) >= 2:
                foreach ($custom_description as $key => $val) {
                    if ($val['page'] == $page) {
                       $description =  $val[$lang];
                    }
                }
                break;
            
            default:
            break;    
        }
        
        return $description;        
    }
    
    public static function getSEODescriptionRegion($region, $page, $lang = 'fr') {
        
        $seo_descriptions = self::getAllRegions();
        $description = '';
        
        if(empty($seo_descriptions))
            return $description;
        
        $custom_description = array_filter($seo_descriptions, function($description) use($page, $region ){
            return ($description['slug'] == $region) && ($description['page'] == $page);      
        });
        
        switch(true) {
            
            case count($custom_description) == 0:
                break;
            
            case count($custom_description) == 1:
                reset($custom_description);
                $first_key = key($custom_description);
                $description = $custom_description[$first_key][$lang];
                break;
            
            case count($custom_description) >= 2:
                foreach ($custom_description as $key => $val) {
                    if ($val['page'] == $page) {
                       $description =  $val[$lang];
                    }
                }
                break;
            
            default:
            break;    
        }
        
        return $description;        
    }
    
}
