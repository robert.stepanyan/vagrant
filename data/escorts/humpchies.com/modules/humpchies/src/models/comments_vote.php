<?php

Namespace Modules\Humpchies;

final class CommentsVoteModel {

    //Db configs
    const DEFAULT_DB_CONFIG = '\Modules\Humpchies\Db\Default';
    const DEFAULT_DB_TABLE = 'comments_votes';

    private static $redisKey      = "UserVotes-%d";
    
    /**
     * Because this class is untended to be a singleton we block access to its constructor.
     * Therefore the class can only be instantiated from within itself.
     */
    private function __construct()
    {}

    /**
     * Disable object copying
     */
    private function __clone()
    {}

    /**
     * Disable object serialization
     */
    private function __wakeup()
    {}

    /**
     * Disable object deserialization
     */
    private function __sleep()
    {}

    public static function getUserVotes($user_id) {
        
        $userVotesList = \Core\library\Redis::get(sprintf(self::$redisKey, $user_id));
        //\Core\library\Redis::del(sprintf(self::$redisKey, $user_id ));    
        //\Core\library\log::debug($userVotesList, "user votes total serialized");
        if($userVotesList){
            return unserialize($userVotesList);
        }
        
        $userVotesList = \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT  `id`, `user_id`, `comment_id`, `ad_id`, `vote`, `created` FROM `comments_votes` WHERE user_id = ' . $user_id . ' ORDER BY `created` DESC',
            TRUE,
            FALSE,
            array('enable' => FALSE));
        
        $sql = 'SELECT  `id`, `user_id`, `comment_id`, `ad_id`, `vote`, `created` FROM `comments_votes` WHERE user_id = ' . $user_id . ' ORDER BY `created` DESC';
        //\Core\library\log::debug($sql, "user votes total sql");
        
        \Core\library\Redis::set(sprintf(self::$redisKey, $user_id), serialize($userVotesList));   
     
        return $userVotesList;
    } 
    
    
    public static function getTodayVotes($user_id) {
        
        $userVotes  = self::getUserVotes($user_id);
        
        if(empty($userVotes))
            return [];
            
        // $param_date==date('d-m-Y',strtotime("now"))
        $todaysVotes = [];
        
        foreach($userVotes as $vote) {
            if( date('Y-m-d', strtotime($vote['created'] )) < date('Y-m-d') ) {
                break;
            }
            $todaysVotes[$vote['comment_id']] = $vote['vote'];
        }
        
        return $todaysVotes;    
    }
    
    public static function getUserVote($user_id, $comment_id){
        
        $userVotes  = self::getUserVotes($user_id);
        //\Core\library\log::debug(print_r($userVotes, true), "user votes total");
        //$vote = array_search($comment_id, array_column($userVotes, 'comment_id'));  
        $vote = array_filter($userVotes, function ($value) use ($comment_id) {
            return ($value["comment_id"] == $comment_id);
        }); 
        $vote = array_values($vote);
        //var_dump($vote); 
        //\Core\library\log::debug(print_r($vote, true), "user vote");
        return (isset($vote[0]['vote']))? $vote[0]['vote'] : false;
    }
    
    
    public static function create($user_id, $comment_id, $ad_id, $vote) {
        
         $fields = [
            'user_id'    => $user_id,
            'comment_id' => $comment_id,
            'ad_id'      => $ad_id,
            'vote'       => $vote,
        ];
                
        $vote = \Core\DBConn::insertDuplicate(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $fields,
            "vote = '$vote'");  // on duplicate key update
        
        if($vote) {
            // clear redis cache
            \Core\library\Redis::del(sprintf(self::$redisKey, $user_id ));    
            return true;
        }
        
        return false;        
    }
    
    public static function getVotesComenntsIds($user_id) {
        
        $userVotes  = self::getUserVotes($user_id);
        
        if(empty($userVotes))
            return [];
            
        $commentsVotes = [];
        foreach($userVotes as $vote) {
            $commentsVotes[$vote['comment_id']] = $vote['vote'];
        }
        
        return $commentsVotes;    
    }
    
    
    
    
}