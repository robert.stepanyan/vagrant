<?php
  Namespace Modules\Humpchies;

final class BlacklistModel{
    
    // CONSTANTS
    // =========
    const DEFAULT_DB_CONFIG                         = '\Modules\Humpchies\Db\Default';
    const DEFAULT_DB_TABLE                          = 'blacklist';
    const REDIS_BLACKLISTS_USER                     = "blacklist_users";
    const REDIS_BLACKLISTS_HOTELS                   = "blacklist_hotels";
    const REDIS_BLACKLISTS                          = "blacklist";
    const LIST_SIZE_DEFAULT_PC                      = "5";

    /**
    * Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}
    
    public static function create($type, $name, $phone, $email, $city, $country, $description, $date, $adv_id)
    {
        $user_data = array( 
            'type'              => $type,
            'name'              => $name,
            'phone'             => $phone,
            'email'             => $email,
            'city'              => $city,
            'country'           => $country,
            'description'       => $description,
            'date'              => $date,
            'advertiser_id'    => $adv_id,
            'created'           => ($date_created = time()),            
        );
        
        // var_dump($user_data);
        $new_user_id = \Core\DBConn::insert(
            self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $user_data
        );
       // var_dump($new_user_id);
       // die();
        if($new_user_id !== FALSE)
            return $new_user_id;
            
        
            return false; 
    }
    public static function getUsers($page_number = 1, $search = ''){
        if($search){
          return  self::getSearchList('client', $search, $page_number);  
        }
        return self::getList('client', $page_number);
    }
    
    public static function getHotels($page_number = 1){
        return self::getList("hotel", $page_number);
    }
    
    public static function getList($type = 'client', $page_number = 1, $page_size = self::LIST_SIZE_DEFAULT_PC){


         return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT id, `type`, name, phone, email, `date`, city, country, description, created, advertiser_id, `status` FROM blacklist
                                            WHERE
                                                   
                                                    status = \'approved\'
                                                    AND
                                                    type = \'' . $type . '\'
    
                                                    
                                            ORDER BY id DESC
                                            LIMIT ' . ($page_size * ($page_number - 1)) . "," . $page_size,
            FALSE,
            TRUE,array('enable' => false));
    }
    
    public static function getSearchList($type = 'client', $search, $page_number = 1, $page_size = self::LIST_SIZE_DEFAULT_PC){


         return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT id, `type`, name, phone, email, `date`, city, country, description, created, advertiser_id, `status` FROM blacklist
                                            WHERE
                                                   
                                                    status = \'approved\'
                                                    AND
                                                    type = \'' . $type . '\'
                                                    and (
                                                    name like \'%' . $search . '%\' OR
                                                    phone like \'%' . $search . '%\' OR
                                                    description like \'%' . $search . '%\'
                                                  
                                                    ) 
                                                    
                                            ORDER BY id DESC
                                            LIMIT ' . ($page_size * ($page_number - 1)) . "," . $page_size,
            FALSE,
            TRUE);
    }
    
    public static function getCountries(){
         return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT DISTINCT country,iso3 FROM world_cities ORDER BY country',  
            FALSE,
            TRUE);
    } 
    
    public static function getCities($country){
        $country = preg_replace("`[^a-z]`is","",$country);
        if(strlen($country)!= 3 ){
            return false;
        }
         return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT DISTINCT city_ascii as city  FROM world_cities  WHERE iso3 = \'' . $country . '\' ORDER BY city',  
            FALSE,
            TRUE);
    }
}
?>