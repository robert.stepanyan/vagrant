<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to interact with user related tasks. Such tasks might be
 * but are not restricted to: add a new user, retrieve a username, delete a
 * user, etc.
 */

Namespace Modules\Humpchies;

final class LogEmailsModel{
    
    // CONSTANTS
    // =========
    const DEFAULT_DB_CONFIG                         = '\Modules\Humpchies\Db\Default';
    const DEFAULT_DB_TABLE                          = 'log_emails';
    
    
    /**
    * Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}
   
  
    public static function create($name, $email, $subject, $message, $login_user_id, $geo_info)
    {
        $email_data = array( 
            'name' => $name,
            'email' => $email,
            'subject' => $subject,
            'message' => $message,
            'login_user_id' => $login_user_id,
            'geo_info' => $geo_info
        );

        $log_id = \Core\DBConn::insert(
            self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $email_data
        );

        if($log_id !== FALSE)
            return $log_id;
            
        // $last_db_error = \Core\DBConn::getLastError(self::DEFAULT_DB_CONFIG, TRUE);
    }
}   
?>