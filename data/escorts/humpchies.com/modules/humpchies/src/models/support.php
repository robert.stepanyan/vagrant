<?php

Namespace Modules\Humpchies;

/**
 * Class SupportModel
 * @package Modules\Humpchies
 *
 */
final class SupportModel
{
    // Database
    const DEFAULT_DB_CONFIG = '\Modules\Humpchies\Db\Default';      // The config file to use by default
    const DEFAULT_DB_TABLE = 'support_tickets';
    const NO_ERROR = 'Modules.Humpchies.SupportModel.NoError';

    const MAX_LEN_SUBJECT = 250;
    const MIN_LEN_SUBJECT  = 2;

    const MAX_LEN_MESSAGE = 2000;
    const MIN_LEN_MESSAGE = 2;

    const ERROR_MESSAGE_IS_EMPTY = "Modules.Humpchies.SupportModel.MessageIsEmpty";
    const ERROR_MESSAGE_IS_TOO_SHORT = "Modules.Humpchies.SupportModel.MessageIsTooShort";
    const ERROR_MESSAGE_IS_TOO_LONG = "Modules.Humpchies.SupportModel.MessageIsTooLong";

    const ERROR_SUBJECT_IS_EMPTY = "Modules.Humpchies.SupportModel.SubjectIsEmpty";
    const ERROR_SUBJECT_IS_TOO_SHORT = "Modules.Humpchies.SupportModel.SubjectIsTooShort";
    const ERROR_SUBJECT_IS_TOO_LONG = "Modules.Humpchies.SupportModel.SubjectIsTooLong";

    const ERROR_UNKNOWN = 'Modules.Humpchies.SupportModel.ErrorUnknown';


    const STATUS_TICKET_OPENED = 1;
    const STATUS_TICKET_CLOSED = 2;

    const STATUS_PROGRESS_NEW_UNREAD = 1;
    const STATUS_PROGRESS_NOT_REPLIED = 2;
    const STATUS_PROGRESS_READ_REPLIED = 3;
    const STATUS_PROGRESS_NEW_REPLY = 4;
    const STATUS_PROGRESS_ADMIN_SENT = 5;
    /**
     * Because this class is untended to be a singleton we block access to its constructor.
     * Therefore the class can only be instantiated from within itself.
     */
    private function __construct() {
    }

    /**
     * Disable object copying
     */
    private function __clone() {
    }

    /**
     * Disable object serialization
     */
    private function __wakeup() {
    }

    /**
     * Disable object deserialization
     */
    private function __sleep() {
    }

    /**
     * @param $subject
     * @return string
     *
     */
    public static function isSubjectInputValid($subject) {
        switch(($subject_validity = \Core\Utils\Security::isStringInputValid($subject, self::MAX_LEN_SUBJECT, self::MIN_LEN_SUBJECT)))
        {
            case \Core\Utils\Security::NO_ERROR:
                return self::NO_ERROR;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_EMPTY:
                return self::ERROR_SUBJECT_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_SHORT:
                return self::ERROR_SUBJECT_IS_TOO_SHORT;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_LONG:
                return self::ERROR_SUBJECT_IS_TOO_LONG;
                break;
            default:
                return $subject_validity;
                break;
        }
    }

    /**
     * @param $message
     * @return string
     */
    public static function isMessageInputValid($message) {
        switch(($message_validity = \Core\Utils\Security::isStringInputValid($message, self::MAX_LEN_MESSAGE, self::MIN_LEN_MESSAGE)))
        {
            case \Core\Utils\Security::NO_ERROR:
                return self::NO_ERROR;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_EMPTY:
                return self::ERROR_MESSAGE_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_SHORT:
                return self::ERROR_MESSAGE_IS_TOO_SHORT;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_LONG:
                return self::ERROR_MESSAGE_IS_TOO_LONG;
                break;
            default:
                return $message_validity;
                break;
        }
    }

    /**
     * @param integer $ticketId
     * @param integer $userId
     * @return string
     */
    public static function isTicketIdValid($ticketId, $userId) {
        switch(($id_validity = \Core\Utils\Security::isIdInputValid($ticketId)))
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_ID_IS_INVALID:
                return self::ERROR_UNKNOWN;
            default:
                return self::ERROR_UNKNOWN;
        }

        if (!self::ticketExists($ticketId, $userId)){
            return self::ERROR_UNKNOWN;
        }

        return self::NO_ERROR;
    }

    /**
     * @param $ticketId
     * @param $userId
     * @return bool
     */
    public static function ticketExists($ticketId, $userId) {
        $table = self::DEFAULT_DB_TABLE;
        $ticketId = intval($ticketId);
        $userId = intval($userId);

        $sql = "SELECT id from {$table} where id = {$ticketId} AND user_id ={$userId}";
        $row = self::selectRow($sql);
        return !empty($row['id']);
    }



    /**
     * @param $fields
     * @return int|string
     */
    public static function create($fields) {

        $attached = array();
        if (!empty($fields['attached'])){
            $attached = $fields['attached'];
        }
        unset($fields['attached']);

        $new_ticket_id = self::insert($fields);

        if ($new_ticket_id){
            if (!empty($attached)){
                foreach ($attached as $row){
                    $row['ticket_id'] = $new_ticket_id;
                    self::insert($row,'ticket_attached_files');
                }
            }
            return $new_ticket_id;
        }

        return self::ERROR_UNKNOWN;
    }

    /**
     * @param $fields
     * @return int|string
     */
    public static function addReply($fields) {
        $attached = array();
        if (!empty($fields['attached'])){
            $attached = $fields['attached'];
        }
        unset($fields['attached']);

        \Core\library\log::debug($fields, "REPLY DATA AFTER VALIDATION");


        $new_reply_id = self::insert($fields, 'ticket_comments');

        if ($new_reply_id){
            self::update(array('status_progress' => self::STATUS_PROGRESS_NEW_REPLY),"id = {$fields['ticket_id']}");
            if (!empty($attached)){
                foreach ($attached as $row){
                    $row['ticket_id'] = $fields['ticket_id'];
                    $row['ticket_comment_id'] = $new_reply_id;
                    self::insert($row,'ticket_attached_files');
                }
            }
            return $new_reply_id;
        }

        return self::ERROR_UNKNOWN;
    }

    /**
     * @param $ticketId
     * @param $userId
     * @return array
     */
    public static function getTicketById($ticketId, $userId) {
        $sql = "
        SELECT
				st.id AS id, st.user_id, st.subject, st.is_read,
				st.message, st.status, UNIX_TIMESTAMP(st.creation_date) AS creation_date,
				UNIX_TIMESTAMP(st.resolve_date) AS resolve_date, st.backend_user_id, u.username, u.type as usertype, u.id as user_id
			FROM support_tickets st
			INNER JOIN users u ON u.id = st.user_id
			WHERE st.id = {$ticketId} AND st.user_id = {$userId}
        ";

        $issue = self::selectRow($sql);

        $attached_files = self::select("SELECT *, 'attached' as catalog_id FROM ticket_attached_files WHERE ticket_comment_id IS NULL AND ticket_id = {$issue['id']}");
        if (empty($attached_files)){
            $attached_files = array();
        }
        $data = array('issue'=> $issue , 'attach' => $attached_files  );

        return $data;
    }

    /**
     * @param $ticketId
     * @return array
     */
    public static function getCommentsByTicketId($ticketId) {
        $sql = "
        SELECT
				tc.id AS comment_id, st.id AS ticket_id, tc.date AS comment_date, tc.comment, u.username, bu.username AS bu_username
			FROM ticket_comments tc
			INNER JOIN support_tickets st ON st.id = tc.ticket_id
			INNER JOIN users u ON u.id = st.user_id
			LEFT JOIN backend_users bu ON bu.id = tc.backend_user_id
			WHERE st.id = {$ticketId}
        ";

        $comments = self::select($sql);
        if (!empty($comments)){
            foreach($comments as &$comment)
            {
                $comment['attached_files'] = self::select("SELECT *, 'attached' as catalog_id FROM ticket_attached_files WHERE ticket_comment_id = {$comment['comment_id']}");
            }
        }else{
            $comments = array();
        }
        return $comments;
    }

    /**
     * @param $commentId
     * @return array
     */
    public static function getCommentById($commentId) {
        $sql = "
        SELECT
				tc.id AS comment_id, st.id AS ticket_id, tc.date AS comment_date, tc.comment, u.username, bu.username AS bu_username
			FROM ticket_comments tc
			INNER JOIN support_tickets st ON st.id = tc.ticket_id
			INNER JOIN users u ON u.id = st.user_id
			LEFT JOIN backend_users bu ON bu.id = tc.backend_user_id
			WHERE tc.id = {$commentId}
        ";

        $comment = self::selectRow($sql);


        if (!empty($comment)){
            $comment['attached_files'] = self::select("SELECT *, 'attached' as catalog_id FROM ticket_attached_files WHERE ticket_comment_id = {$comment['comment_id']}");
        }else{
            $comment = array();
        }
        return $comment;
    }

    /**
     * @param int $user_id
     * @return  array
     */
    public static function getOpenTickets($user_id) {
        $sql = "
        SELECT
				st.id AS id, st.user_id, st.`subject`, st.is_read,
				st.message, st.`status`, st.status_progress , UNIX_TIMESTAMP(st.creation_date) AS creation_date,
				UNIX_TIMESTAMP(st.resolve_date) AS resolve_date, st.backend_user_id, u.username, u.type,
			    bu.first_name, st.disabled_in_pa
			FROM support_tickets st
			INNER JOIN users u ON u.id = st.user_id
			LEFT JOIN backend_users bu ON bu.id = st.backend_user_id
			WHERE u.id = {$user_id} AND  st.status = ".self::STATUS_TICKET_OPENED . 
            " ORDER BY creation_date DESC";
        $Tickets = self::select($sql);
        return !empty($Tickets) ? $Tickets : array();
    }

    /**
     * @param $userId
     * @return array
     */
    public static function hasTicketActions($userId) {
        $sql = "
        SELECT
				st.id
			FROM support_tickets st
			INNER JOIN users u ON u.id = st.user_id
			LEFT JOIN backend_users bu ON bu.id = st.backend_user_id
			WHERE u.id = {$userId} 
			  AND  st.status = ".self::STATUS_TICKET_OPENED. " 
			  AND ( status_progress =".self::STATUS_PROGRESS_READ_REPLIED. " 
			        OR status_progress =".self::STATUS_PROGRESS_ADMIN_SENT."
			       )";
        $Tickets = self::select($sql);
        return !empty($Tickets) ? $Tickets : array();
    }

    /**
     * @param int $user_id
     * @return  array
     */
    public static function getClosedTickets($user_id) {
        $sql = "
        SELECT
				st.id AS id, st.user_id, st.`subject`,
				st.message, st.`status`, st.status_progress , UNIX_TIMESTAMP(st.creation_date) AS creation_date,
				UNIX_TIMESTAMP(st.resolve_date) AS resolve_date, st.backend_user_id, u.username, u.type,
			    bu.first_name, st.disabled_in_pa
			FROM support_tickets st
			INNER JOIN users u ON u.id = st.user_id
			LEFT JOIN backend_users bu ON bu.id = st.backend_user_id
			WHERE u.id = {$user_id} AND  st.status = ".self::STATUS_TICKET_CLOSED. " AND disabled_in_pa != 1";

        $Tickets = self::select($sql);
        return !empty($Tickets) ? $Tickets : array();
    }

    /**
     * @param string $sql
     * @param bool $master
     * @return array
     */
    public static function select($sql, $master =false) {
        return \Core\DBConn::select(self::DEFAULT_DB_CONFIG, $sql, $master);
    }

    /**
     * @param string $sql
     * @param bool $master
     * @return array
     */
    public static function selectRow($sql, $master =false) {
        return \Core\DBConn::selectRow(self::DEFAULT_DB_CONFIG, $sql, $master);
    }

    public static function insert($fields, $table = self::DEFAULT_DB_TABLE) {
        //mysql_query("SET NAMES 'utf8'");
        return \Core\DBConn::insert(self::DEFAULT_DB_CONFIG, $table, $fields);

    }

    public static function update($fields, $condition= null) {
       return \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $fields,
            $condition);
    }
}
