<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to interact with city related items and tasks
 */

Namespace Modules\Humpchies;

final class PaymentModel{
    
    const DEFAULT_DB_CONFIG                        = '\Modules\Humpchies\Db\Default';
    const PACKAGE_STATUS_ACTIVE                    = 1;
    const PACKAGE_STATUS_PENDING                   = 2;
    const BITCOIN_MULTIPLIER = 1.25; // 25%
    const BITCOIN_MULTIPLIER_UNDER = 1.37; // 37%
    const BITCOIN_URL = "https://www.coiniverse.ch/apps/"; // 25%
    const BITCOIN_CHECK_URL = "https://www.coiniverse.ch/check/"; // 25%
    const BITCOIN_TOKEN = "z32wed81779041ebb2210d05dcbzre34"; // 25%
    const BITCOIN_PRICES = [19 => 32 , 20 => 47 , 21 => 75, 22 => 195, 23 => 32 , 24 => 47 , 25 => 75, 26 => 195];
    const BITCOIN_BREAKPOINT = 40;

    /**
    * Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}

    public static function get_bitcoin_price($pack_id){
        return self::BITCOIN_PRICES[$pack_id];    
    }
    
    public static function credit_price($price){
        if($price < self::BITCOIN_BREAKPOINT){
            return ceil($price * self::BITCOIN_MULTIPLIER_UNDER);
        }else{
            return ceil($price * self::BITCOIN_MULTIPLIER);    
        }
    }
    
    public static function get_available_packages()
    {
       return \Core\DBConn::select(
            self::DEFAULT_DB_CONFIG,
            'SELECT
                *
            FROM 
                packages
            WHERE 
                is_premium = 1 AND is_active = 1',
            TRUE);
    }
       
    public static function get_unavailable_packages()
    {
       return \Core\DBConn::select(
            self::DEFAULT_DB_CONFIG,
            'SELECT
                *
            FROM 
                packages
            WHERE 
                is_premium = 1 AND is_active = 0',
            TRUE);
    }
    
    public static function get_available_packages_ids()
    {
       return \Core\DBConn::select(
            self::DEFAULT_DB_CONFIG,
            'SELECT
                id
            FROM 
                packages
            WHERE 
                is_premium = 1',
            TRUE);
    }

    public static function get_packages_price($package_id, $cat_id)
    {
        
       return \Core\DBConn::selectRow(
            self::DEFAULT_DB_CONFIG,
            'SELECT
                price
            FROM 
                package_prices
            WHERE 
                `package_id` = \''.$package_id.'\' AND  `category` = \''.$cat_id.'\' limit 1',
            TRUE);
    }
    
    
    public static function get_ongoing_package($ad_id){
         return \Core\DBConn::select(
            self::DEFAULT_DB_CONFIG,
            "SELECT
                *
            FROM 
                order_packages
            WHERE 
                ads_id = \''.$ad_id.'\' AND status IN('PACKAGE_STATUS_ACTIVE','PACKAGE_STATUS_PENDING')",
            TRUE);
    }
}
?>
