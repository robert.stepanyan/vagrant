<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to interact with ad image related tasks. Such tasks might
 * but are not restricted to: add a new ad image, delete an ad image, etc.
 */

Namespace Modules\Humpchies;

final class AdImageModel{

    // CONSTANTS
    // =========
    const AD_THUMB_HEIGHT_PORTRAIT                  = 180;
    const AD_THUMB_HEIGHT_LANDSCAPE                 = 80;
    const AD_THUMB_HEIGHT_SQUARED                   = 120;
    const AD_THUMB_WIDTH                            = 120;
    const AD_IMAGE_HEIGHT_PORTRAIT                  = 600;
    const AD_IMAGE_HEIGHT_LANDSCAPE                 = 600;
    const AD_IMAGE_HEIGHT_SQUARED                   = 400;
    const AD_IMAGE_WIDTH                            = 600;
    const DEFAULT_DB_CONFIG                         = '\Modules\Humpchies\Db\Default';
    const DEFAULT_DB_TABLE                          = 'ad_images';
//    const AD_IMAGE_HEIGHT_PORTRAIT                  = 390;
//    const AD_IMAGE_HEIGHT_LANDSCAPE                 = 173;
//    const AD_IMAGE_HEIGHT_SQUARED                   = 260;
//    const AD_IMAGE_WIDTH                            = 260;

    const ERROR_NO_IMAGES_SAVED                     = 'Modules.Humpchies.AdImageModel.ErrorNoImagesSaved';
    const ERROR_SOME_IMAGES_COULD_NOT_BE_SAVED      = 'Modules.Humpchies.AdImageModel.ErrorSomeImagesCouldNotBeSaved';
    const NO_ERROR                                  = 'Modules.Humpchies.AdImageModel.NoError';

    const AD_PHOTO_TYPE_PUBLIC                      = 1;

    const AD_PHOTO_PORTRAIT                         = 1;
    const AD_PHOTO_LANDSCAPE                        = 2;
    const AD_PHOTO_SQUARE                           = 3;

    public static $AD_PHOTO_ORIENTATIONS = array(
        self::AD_PHOTO_PORTRAIT => 'P',
        self::AD_PHOTO_LANDSCAPE => 'L',
        self::AD_PHOTO_SQUARE => 'S',
    );

    private static $image_paths = NULL;
    private static $basic_image_path_components = NULL;

    /**
     * Because this class is untended to be a singleton we block access to its constructor.
     * Therefore the class can only be instantiated from within itself.
     */
    private function __construct()
    {}

    /**
     * Disable object copying
     */
    private function __clone()
    {}

    /**
     * Disable object serialization
     */
    private function __wakeup()
    {}

    /**
     * Disable object deserialization
     */
    private function __sleep()
    {}

    public static function getImageHeight($image_orientation, $is_thumb = FALSE)
    {
        if($is_thumb)
            return ($image_orientation === \Core\Utils\Image::ORIENTATION_PORTRAIT ? self::AD_THUMB_HEIGHT_PORTRAIT : ($image_orientation === \Core\Utils\Image::ORIENTATION_LANDSCAPE ? self::AD_THUMB_HEIGHT_LANDSCAPE : self::AD_THUMB_HEIGHT_SQUARED));

        return ($image_orientation === \Core\Utils\Image::ORIENTATION_PORTRAIT ?  self::AD_IMAGE_HEIGHT_PORTRAIT : ($image_orientation === \Core\Utils\Image::ORIENTATION_LANDSCAPE ? self::AD_IMAGE_HEIGHT_LANDSCAPE : self::AD_IMAGE_HEIGHT_SQUARED));
    }

    public static function getImagePath(&$ad_record, $image_number, $is_thumb = FALSE, $is_web = FALSE, $return_cached = TRUE)
    {
        $cache_key = $ad_record['id'] . (int)$is_thumb;

        if($return_cached === TRUE && isset(self::$basic_image_path_components[$cache_key]))
            return self::$basic_image_path_components[$cache_key][0] . $image_number . self::$basic_image_path_components[$cache_key][1] ;

        return  ((self::$basic_image_path_components[$cache_key][0] = (($is_web === TRUE ? \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_' . $ad_record['category']) : \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_path') . $ad_record['category'] . '/') . \Core\Utils\Text::stringToUrl($ad_record['city']) . '/id-' . $ad_record['id']. '/' . ($is_thumb === TRUE ? 'thumb' : 'img') . '-'))
            .
            $image_number
            .
            (self::$basic_image_path_components[$cache_key][1] = '-' . \Core\Utils\Text::stringToUrl($ad_record['title']) . '.jpg'));
    }

    public static function getMainThumbWebDetails(&$ad_record)
    {
        if(unserialize($ad_record['images']) != null){
            foreach((unserialize($ad_record['images'])) as $image_number => $image_info)
            {
                if(substr($image_info, -1, 1) === 'T'){
                    return array('src'      => self::getImagePath($ad_record, $image_number, TRUE, TRUE),
                        'height'   => self::getImageHeight(substr($image_info, -3, 1), TRUE),
                        'width'    => self::AD_THUMB_WIDTH);

                    break;
                }
            }
        }

        return NULL;
    }

    public static function getMainThumbWebDetailsV2(&$ad_record)
    {
        $image = self::getMainImage($ad_record['id']);
        if (empty($image)){
            return array(
                'src'      => 'https://gui.humpchies.com/gui_ads/filler.jpg',
                'height'   => self::getImageHeight(\Core\Utils\Image::ORIENTATION_PORTRAIT, true),
                'real'   => false,
                'width'    => self::AD_THUMB_WIDTH
            );
        }
        return array(
            'src'      => self::getImageSrc($image, true, true),
            'height'   => self::getImageHeight(self::$AD_PHOTO_ORIENTATIONS[$image['orientation']], true),
            'real'   => true,
            'width'    => self::AD_THUMB_WIDTH
        );
    }

    public static function getMainThumbWebDetailsForGuiV2(&$ad_record)
    {
        $image = self::getMainImageForGui($ad_record['id']);
        if (empty($image)){
            return array(
                'src'      => 'https://gui.humpchies.com/gui_ads/filler.jpg',
                'height'   => self::getImageHeight(\Core\Utils\Image::ORIENTATION_PORTRAIT, true),
                'width'    => self::AD_THUMB_WIDTH
            );
        }
        return array(
            'src'      => self::getImageSrc($image, true, true),
            'height'   => self::getImageHeight(self::$AD_PHOTO_ORIENTATIONS[$image['orientation']], true),
            'width'    => self::AD_THUMB_WIDTH
        );
    }

    /**
     * @param $ad_id int
     * @return array
     */
    public static function getMainImage($ad_id) {
        return \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG,
            'SELECT * FROM ' . self::DEFAULT_DB_TABLE . ' img WHERE img.is_main = 1 AND img.ad_id = '.$ad_id,
            FALSE);
    }

    /**
     * @param $ad_id int
     * @return array
     */
    public static function getMainImageForGui($ad_id) {
        return \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG,
            'SELECT * FROM ' . self::DEFAULT_DB_TABLE . ' img WHERE img.is_edited = 0 AND img.is_main = 1 AND img.ad_id = '.$ad_id,
            FALSE);
    }

    public static function getMaxOrdering($ad_id) {
        $result = \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG,
            'SELECT IFNULL(max(ordering), 0) as `ordering` FROM ' . self::DEFAULT_DB_TABLE . ' img WHERE img.ad_id = '.$ad_id,
            FALSE);

        return $result['ordering'];
    }

    /**
     * @param $ad_id int
     * @return array
     */
    public static function getImages($ad_id) {
        return \Core\DBConn::select( self::DEFAULT_DB_CONFIG,
            'SELECT * FROM ' . self::DEFAULT_DB_TABLE . ' img WHERE img.ad_id = '.$ad_id.' order by ordering asc, id asc',
            FALSE);
    }

    /**
     * @param $ad_id int
     * @return array
     */
    public static function getApprovedImages($ad_id) {
        return \Core\DBConn::select( self::DEFAULT_DB_CONFIG,
            'SELECT * FROM ' . self::DEFAULT_DB_TABLE . ' img WHERE img.is_edited = 0 AND img.ad_id = '.$ad_id.' order by ordering asc, id asc',
            FALSE);
    }

    public static function getAdHubImagesWebDetails(&$ad_record)
    {
        $images = NULL;

        foreach((unserialize($ad_record['images'])) as $image_number => $image_info)
        {
            if(($image_orientation = substr($image_info, -1, 1)) === 'T')
                $images['main'] = array('src'     => self::getImagePath($ad_record, $image_number, FALSE, TRUE),
                    'height'  => self::getImageHeight(substr($image_info, -3, 1)),
                    'width'   => self::AD_IMAGE_WIDTH,
                    'href'    => self::getImagePath($ad_record, $image_number, FALSE, TRUE));
            else
                $images['thumbs'][$image_orientation][] = array('src'     => self::getImagePath($ad_record, $image_number, TRUE, TRUE),
                    'height'  => self::getImageHeight($image_orientation, TRUE),
                    'width'   => self::AD_THUMB_WIDTH,
                    'href'    => self::getImagePath($ad_record, $image_number, FALSE, TRUE));
        }

        return $images;
    }

    public static function getAdHubImagesWebDetailsV2(&$ad_record)
    {
        $result =null;
        $images = self::getApprovedImages($ad_record['id']);

        if (empty($images)) {
            return array(
                'main' => array(
                    'src'      => 'https://gui.humpchies.com/gui_ads/filler.jpg',
                    'height'   => self::getImageHeight(\Core\Utils\Image::ORIENTATION_PORTRAIT),
                    'width'    => self::AD_IMAGE_WIDTH
                )
            );
        }

        foreach($images as $image)
        {
            $orientation = self::$AD_PHOTO_ORIENTATIONS[$image['orientation']];
            if ($image['is_main']) {
                $result['main'] = array(
                    'src' => self::getImageSrc($image, false, true),
                    'height' => self::getImageHeight($orientation),
                    'width' => self::AD_IMAGE_WIDTH,
                    'href' => self::getImageSrc($image, false, true));
            }
            else {
                $result['thumbs'][$orientation][]
                    = array(
                    'src' => self::getImageSrc($image, true, true),
                    'height' => self::getImageHeight($orientation, true),
                    'width' => self::AD_THUMB_WIDTH,
                    'href' => self::getImageSrc($image, false, true)
                );
            }
        }

        if(!$result['main']) {
            $result['main'] = array(
                'src'      => 'https://gui.humpchies.com/gui_ads/filler.jpg',
                'height'   => self::getImageHeight(\Core\Utils\Image::ORIENTATION_PORTRAIT),
                'width'    => self::AD_IMAGE_WIDTH
            );
        }

        return $result;
    }
    
    public static function getAdHubImagesWebListV2(&$ad_record)
    {
        $result =null;
        $images = self::getApprovedImages($ad_record['id']);

        if (empty($images)) {
            return array(
                'main' => array(
                    'src'      => 'https://gui.humpchies.com/gui_ads/filler.jpg',
                    'height'   => self::getImageHeight(\Core\Utils\Image::ORIENTATION_PORTRAIT),
                    'width'    => self::AD_IMAGE_WIDTH
                )
            );
        }

        foreach($images as $image)
        {
            $orientation = self::$AD_PHOTO_ORIENTATIONS[$image['orientation']];
            if ($image['is_main']) {
                $result['main'] = array(
                    'src' => self::getImageSrc($image, false, true),
                    'height' => self::getImageHeight($orientation),
                    'width' => self::AD_IMAGE_WIDTH,
                    'href' => self::getImageSrc($image, false, true));
            }
            else {
                $result['thumbs'][$orientation][]
                    = array(
                    'src' => self::getImageSrc($image, true, true),
                    'height' => self::getImageHeight($orientation, true),
                    'width' => self::AD_THUMB_WIDTH,
                    'href' => self::getImageSrc($image, true, true)
                );
            }
        }

        if(!$result['main']) {
            $result['main'] = array(
                'src'      => 'https://gui.humpchies.com/gui_ads/filler.jpg',
                'height'   => self::getImageHeight(\Core\Utils\Image::ORIENTATION_PORTRAIT),
                'width'    => self::AD_IMAGE_WIDTH
            );
        }

        return $result;
    }

    /**
     * Get main AD image list.
     * 
     * @param int[] $adIdList The AD ID list.
     * @return array
     */
    public static function getAdHubMainImageByIdList($adIdList)
    {
        $imageList = \Core\DBConn::select( 
            self::DEFAULT_DB_CONFIG,
            'SELECT 
                * 
            FROM 
                ' . self::DEFAULT_DB_TABLE . '
            WHERE 
                ad_id IN (' . implode(',', $adIdList) .')
                AND is_main = 1 
                AND is_edited = 0
            '
        );

        $images = array();

        // get main image info
        foreach ($imageList as $image) {
            $orientation = self::$AD_PHOTO_ORIENTATIONS[$image['orientation']];
            $images[$image['ad_id']] = array(
                'src' => self::getImageSrc($image, false, true),
                'height' => self::getImageHeight($orientation),
                'width' => self::AD_IMAGE_WIDTH,
                'href' => self::getImageSrc($image, false, true)
            );
        }
        
        // ensure blank image if there is no main image
        $blankImage = array(
            'src'      => 'https://gui.humpchies.com/gui_ads/filler.jpg',
            'height'   => self::getImageHeight(\Core\Utils\Image::ORIENTATION_PORTRAIT),
            'width'    => self::AD_IMAGE_WIDTH
        );
        foreach ($adIdList as $adId) {
            if (!isset($images[$adId])) {
                $images[$adId] = $blankImage;
            }
        }
        
        return $images;
    }

    public static function getImagesWeb($ad_id) {
        $result =null;
        $images = self::getImages($ad_id);
        if (empty($images)) {
            return null;
        }

        foreach($images as $image)
        {
            $orientation = self::$AD_PHOTO_ORIENTATIONS[$image['orientation']];
            $result[]
                = array(
                'src' => self::getImageSrc($image, true, true),
                'height' => self::getImageHeight($orientation, true),
                'width' => self::AD_THUMB_WIDTH,
                'is_main' => $image['is_main'],
                'href' => self::getImageSrc($image, false, true),
                "id"=>$image['id']
            );
        }
        return $result;
    }

    public static function reorder($imageIds) {
        foreach ($imageIds as $i=> $imageId){
            \Core\DBConn::update(self::DEFAULT_DB_CONFIG, self::DEFAULT_DB_TABLE, array('ordering'=>$i+1), "id = {$imageId}");
        }
        return true;
    }

    public static function setMain($id, $ad_id) {
        if (is_array($id)){
            $id = array_shift($id);
        }
        if ($id === null){
            return \Core\DBConn::execute(self::DEFAULT_DB_CONFIG, "UPDATE ad_images SET is_main = 1 WHERE ad_id = {$ad_id} ORDER BY id ASC LIMIT 1;");
        }
        \Core\DBConn::update(self::DEFAULT_DB_CONFIG, self::DEFAULT_DB_TABLE, array('is_main' => 0), "ad_id = {$ad_id}");
        return \Core\DBConn::update(self::DEFAULT_DB_CONFIG, self::DEFAULT_DB_TABLE, array('is_main' => 1), "id = {$id}");
    }

    /**
     * @param $ids
     * @param $ad_id
     * @return bool|int
     */
    public static function delete($ids, $ad_id) {
        if (empty($ids)){
            return false;
        }
        if (!is_array($ids) && ( (int)$ids == $ids)){
            return self::deleteOne($ids, $ad_id);
        }
        elseif (is_array($ids)){
            foreach ($ids as $id) {
                self::deleteOne($id, $ad_id);
            }
            //check if main image removed
            $mainImage = self::getMainImage($ad_id);
            if (empty($mainImage)){
                self::setMain(null, $ad_id);
            }
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @param $ad_id
     * @return bool|int
     */
    public static function deleteOne($id,$ad_id) {
        if ((int)$id != $id){
            return false;
        }
        $image = \Core\DBConn::selectRow(self::DEFAULT_DB_CONFIG, "select * from ad_images where id = {$id}");
        if (empty($image)){
            return false;
        }

        if (empty($image['ad_id']) || (int)$image['ad_id'] !== (int)$ad_id){
            return false;
        }
        //-----------------
        $sql = "INSERT INTO ad_images_deleted 
                (id, ad_id, gallery_id, hash, ext, `type`, `status`, is_main, ordering, args, _inter_index, is_approved, double_approved, orientation, width, height, retouch_status, creation_date, is_rotatable, is_suspicious, is_popunder, popunder_args, retouch_date, is_edited, delete_date) 
                ( SELECT id, ad_id, gallery_id, hash, ext, `type`, `status`, is_main, ordering, args, _inter_index, is_approved, double_approved, orientation, width, height, retouch_status, creation_date, is_rotatable, is_suspicious, is_popunder, popunder_args, retouch_date, is_edited, NOW()  
                  FROM ad_images WHERE id = {$id}) ";
                  
        \Core\DBConn::execute(self::DEFAULT_DB_CONFIG,$sql);
              
//        $imageInfo = self::getImagePathV2($image['ad_id'],$image['hash'], false, false);
//        $imageInfoThumb = self::getImagePathV2($image['ad_id'],$image['hash'], true, false);
//        unlink($imageInfo['path']);
//        unlink($imageInfoThumb['path']);
        return \Core\DBConn::delete(self::DEFAULT_DB_CONFIG, self::DEFAULT_DB_TABLE, "id = {$id}");
            
        //-----------------
        
        
    }

    public static function getImagePathV2($ad_id, $image_name = null, $is_thumb = false, $is_web = false)
    {
        $category = $ad_id;
        $ads_path  = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_path');
        if (strlen($ad_id) > 2) {
            $category = substr($ad_id, 0, 2) . DIRECTORY_SEPARATOR . substr($ad_id, 2). DIRECTORY_SEPARATOR;
        }

        $image['hash'] = empty($image_name) ? md5(microtime(true)) : $image_name;
        $image['ext'] = 'jpg';
        $image['path'] =
            (
            $is_web === true ?
                \Core\Utils\Device::getProtocol() .
                \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_images')
                : $ads_path
            ) .
            $category .
            ($is_thumb === true ? 'thumb' : 'img')

            . '-' . $image['hash'] . '.'. $image['ext'];
        return $image;
    }

    public static function getImageSrc($image, $is_thumb = false, $is_web = false) {
         //var_dump($image);exit();
        
        if(!isset($image['ext']))
            $image['ext'] =  'jpg';
        
        $category = $image['ad_id'];
        $ads_path  = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_path');
        if (strlen($category) > 2) {
            $category = substr($category, 0, 2) . DIRECTORY_SEPARATOR . substr($category, 2). DIRECTORY_SEPARATOR;
        }
        $src = (
            $is_web === true ? \Core\Utils\Device::getProtocol() .
                \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_images') : $ads_path
            ) .
            $category .
            ($is_thumb === true ? 'thumb' : 'img')
            . '-' . $image['hash'] . '.' . $image['ext'];
        return $src;
    }

    public static function savePostedImages(&$ad_fields, &$posted_images)
    {
        $return_values = NULL;
        $thumb_md5 = NULL;

        foreach($posted_images as $image_name => $image_details)
        {
            $image_index                    = (int)substr($image_name, -1, 1);
            $image_destination_path         = self::getImagePath($ad_fields, $image_index, FALSE, FALSE);
            $image_thumb_destination_path   = self::getImagePath($ad_fields, $image_index, TRUE, FALSE);

            $x = (\Core\Utils\Image::resampleJPEGImage($image_details['tmp_name'], $image_destination_path, self::AD_IMAGE_WIDTH, TRUE) === TRUE ? 1 : 0);
            $y = (\Core\Utils\Image::resampleJPEGImage($image_details['tmp_name'], $image_thumb_destination_path, self::AD_THUMB_WIDTH, TRUE) === TRUE ? 2: 0);

            switch($x + $y)
            {
                case 3:
                    $return_values[$image_index]    = \Core\Utils\Image::getImageOrientation($image_details['tmp_name']);

                    if($thumb_md5 === NULL)
                    {
                        $return_values[$image_index] .= ':T';
                        $thumb_md5 = md5(file_get_contents($image_thumb_destination_path));
                    }

                    break;

                default:
                    foreach(glob(dirname($image_destination_path) . '/*') as $file)
                        unlink($file);

                    rmdir(dirname($image_destination_path));
                    return self::ERROR_SOME_IMAGES_COULD_NOT_BE_SAVED;
                    break;
            }
        }

        $posted_images = array('images' => serialize($return_values), 'thumb_md5' => $thumb_md5);

        return self::NO_ERROR;
    }


    /**
     * @param $ad_fields
     * @param $posted_images
     * @return string
     */
    public static function savePostedImagesV2(&$ad_fields, &$posted_images, $is_edited = 0)
    {
        $return_values = NULL;
        $thumb_md5 = NULL;
        $data = [];
        $mainImg = self::getMainImage($ad_fields['id']);
        $ordering = self::getMaxOrdering($ad_fields['id']);
        $is_main = 0;
        foreach($posted_images as $i => $image_details)
        {
            $image_destination_path_info         = self::getImagePathV2($ad_fields['id'], null, false, false);
            $image_thumb_destination_path_info   = self::getImagePathV2($ad_fields['id'], $image_destination_path_info['hash'],true, false);

            $image_destination_path = $image_destination_path_info['path'];
            $image_thumb_destination_path = $image_thumb_destination_path_info['path'];
            $ads_path  = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_path');
            $uploadedFile = $image_details['tmp_name'];
            if($ad_fields["rotator"][$i] != 0 ){
                  //IMAGE
                    $newhash = md5(date("YmdHis").rand(1,20000000));
                    $source = imagecreatefromjpeg($uploadedFile);
                    $rotate = imagerotate($source, -1 * $ad_fields["rotator"][$i], 0);
                    $uploadedFile   = $ads_path . "tmp/".   $newhash . '.jpg';
                    imagejpeg($rotate,$uploadedFile);
                    
            }

            //$x = (\Core\Utils\Image::resampleJPEGImage($image_details['tmp_name'], $image_destination_path, self::AD_IMAGE_WIDTH, true) === true ? 1 : 0);
            //$y = (\Core\Utils\Image::resampleJPEGImage($image_details['tmp_name'], $image_thumb_destination_path, self::AD_THUMB_WIDTH, true) === true ? 2: 0);
            $x = (\Core\Utils\Image::resampleJPEGImage($uploadedFile, $image_destination_path, self::AD_IMAGE_WIDTH, true) === true ? 1 : 0);
            $y = (\Core\Utils\Image::resampleJPEGImage($uploadedFile, $image_thumb_destination_path, self::AD_THUMB_WIDTH, true) === true ? 2: 0);

            switch($x + $y)
            {
                case 3:
                    $return_values[$i]    = \Core\Utils\Image::getImageOrientation($image_details['tmp_name']);

                    if($thumb_md5 === NULL)
                    {
                        $return_values[$i] .= ':T';
                        $thumb_md5 = md5(file_get_contents($image_thumb_destination_path));
                        if (empty($mainImg) && !isset($image_details['is_main'])){
                            $is_main = 1;
                        }
                    }
                    $orientation = \Core\Utils\Image::getImageOrientation($image_details['tmp_name']);
                    $reverse_orientations = array_flip(self::$AD_PHOTO_ORIENTATIONS);
                    list($image_width, $image_height) = @getimagesize($image_details['tmp_name']);
                    $data[$i]['id'] = \Core\DBConn::insert(
                        self::DEFAULT_DB_CONFIG,
                        self::DEFAULT_DB_TABLE,
                        array(
                            'ad_id'=>$ad_fields['id'],
                            'hash'=>$image_destination_path_info['hash'],
                            'ext'=>$image_destination_path_info['ext'],
                            'type' => self::AD_PHOTO_TYPE_PUBLIC,
                            'orientation' => $reverse_orientations[$orientation],
                            'is_main' => isset($image_details['is_main']) ? $image_details['is_main'] : $is_main,
                            'ordering' => $ordering + $i + 1, //continue ordering
                            'width' => $image_width,
                            'height' => $image_height,
                            'creation_date' => date('Y-m-d H:i:s', time()),
                            'is_edited' => $is_edited
                        )
                    );

                    if($is_edited) {
                        \Modules\Humpchies\AdModel::setRevisionStatus($ad_fields['id'], '1');
                    }

                    $is_main = 0;
                    $data[$i]['src'] = self::getImageSrc(array('ad_id'=>$ad_fields['id'], 'hash'=>$image_destination_path_info['hash']), false, true);
                    break;
                default:
                    if (is_file($image_destination_path)){
                        unlink($image_destination_path);
                    }
                    if (is_file($image_thumb_destination_path)){
                        unlink($image_thumb_destination_path);
                    }
                    return self::ERROR_SOME_IMAGES_COULD_NOT_BE_SAVED;
                    break;
            }
            @unlink($uploadedFile);
        }

        $posted_images = array(
            'images' => serialize($return_values),
            'thumb_md5' => $thumb_md5,
            "data"=>$data
        );

        return self::NO_ERROR;
    }
   
   
    public static function getImagesbyIds($ids) {
        return \Core\DBConn::select( self::DEFAULT_DB_CONFIG,
            'SELECT * FROM ' . self::DEFAULT_DB_TABLE . ' img WHERE id in  (' . implode(",", $ids).')',
            FALSE);
    }
    
    public static function rotateImages($ids, $angles, $ad_id)
    {
        $rotator = [];
        foreach($ids as $k => $id){
           if($id && $angles[$k]){
               $rotator[$id]= $angles[$k];
           }    
        }
        if(count($rotator) == 0 ){
            return false;
        }
        
        $images = self::getImagesbyIds(array_keys($rotator));
        
        foreach($images as $i => $image_details)                               
        {
            $image_destination_path_info         = self::getImagePathV2($ad_id, $image_details['hash'], false, false);
            $image_thumb_destination_path_info   = self::getImagePathV2($ad_id, $image_destination_path_info['hash'], true , false);
            $original       = $image_destination_path_info['path'];
            $originalthumb  = $image_thumb_destination_path_info['path'];
            
            $image_destination_path_info         = self::getImagePathV2($ad_id, null, false, false);
            $image_thumb_destination_path_info   = self::getImagePathV2($ad_id, $image_destination_path_info['hash'], true , false);
           // var_dump($image_destination_path_info);
            
            $image_destination_path = $image_destination_path_info['path'];
            $image_thumb_destination_path = $image_thumb_destination_path_info['path'];
            $ads_path  = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_path');
   
            if($rotator[$image_details['id']] != 0 ){
                  //IMAGE
                    $newhash = md5(date("YmdHis").rand(1,20000000));
                    $source = imagecreatefromjpeg($original);
                    $rotate = imagerotate($source, -1 * $rotator[$image_details['id']], 0);
                    $uploadedFile   = $ads_path . "tmp/".   $newhash . '.jpg';
                    imagejpeg($rotate ,$uploadedFile);
                    
            }
           // var_dump([$image_thumb_destination_path_info, $image_thumb_destination_path,$image_thumb_destination_path_info]);
            //die();
            //$x = (\Core\Utils\Image::resampleJPEGImage($image_details['tmp_name'], $image_destination_path, self::AD_IMAGE_WIDTH, true) === true ? 1 : 0);
            //$y = (\Core\Utils\Image::resampleJPEGImage($image_details['tmp_name'], $image_thumb_destination_path, self::AD_THUMB_WIDTH, true) === true ? 2: 0);
            $x = (\Core\Utils\Image::resampleJPEGImage($uploadedFile, $image_destination_path, self::AD_IMAGE_WIDTH, true) === true ? 1 : 0);
            $y = (\Core\Utils\Image::resampleJPEGImage($uploadedFile, $image_thumb_destination_path, self::AD_THUMB_WIDTH, true) === true ? 2: 0);
           //echo $x; echo $y;
           // die($x+$y);
            switch($x + $y)
            {
                case 3:
                    $return_values[$i]    = \Core\Utils\Image::getImageOrientation($uploadedFile);

                    $orientation = \Core\Utils\Image::getImageOrientation($uploadedFile);
                    $reverse_orientations = array_flip(self::$AD_PHOTO_ORIENTATIONS);
                    list($image_width, $image_height) = @getimagesize($uploadedFile);
                    $data[$i]['id'] = \Core\DBConn::update(
                        self::DEFAULT_DB_CONFIG,
                        self::DEFAULT_DB_TABLE,
                        array(
                            'hash'=>$image_destination_path_info['hash'],
                            'width' => $image_width,
                            'orientation' => $reverse_orientations[$orientation],
                            'height' => $image_height
                        ), " id = ".$image_details['id'] . " and ad_id= " . $ad_id
                    );
   
                    unlink($original);     
                    unlink($originalthumb);     
                    $data[$i]['src'] = self::getImageSrc(array('ad_id'=>$ad_fields['id'], 'hash'=>$image_destination_path_info['hash']), false, true);
                    break;
                default:
                    if (is_file($image_destination_path)){
                        unlink($image_destination_path);
                    }
                    if (is_file($image_thumb_destination_path)){
                        unlink($image_thumb_destination_path);
                    }
                    return self::ERROR_SOME_IMAGES_COULD_NOT_BE_SAVED;
                    break;
            }
            @unlink($uploadedFile);
        }

        $posted_images = array(
            'images' => serialize($return_values),
            'thumb_md5' => $thumb_md5,
            "data"=>$data
        );

        return self::NO_ERROR;
    }

    public static function canProcessImages($photoIds, $ad_id) {
        if (!is_array($photoIds)){
            $photoIds = array($photoIds);
        }
        $sql = "
        SELECT
			COUNT(DISTINCT id) = ".count($photoIds)." as `canProcess`
		FROM
			ad_images
		WHERE
			id IN (". implode(",", $photoIds). ")
		AND ad_id = ".$ad_id."
        ";
        $result = \Core\DBConn::selectRow(self::DEFAULT_DB_CONFIG, $sql);
        return !!$result['canProcess'];

    }
}
?>
