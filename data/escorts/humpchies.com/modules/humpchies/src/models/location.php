<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to interact with mobility related tasks.
 */

Namespace Modules\Humpchies;

final class LocationModel{
    
    // SINGLETONS
    // ==========
    private static $supported_locations             = NULL;
    
    // Constants
    // =========
    const LOCATION_OUTCALL                          = 'outcalls';
    const LOCATION_INCALL                           = 'incalls';
    const LOCATION_INCALL_OUTCALL                   = 'both';
    const LOCATION_UNKNOWN                          = 'unknown';
    
    // ERRORS
    // ======
    const ERROR_LOCATION_IS_INVALID                 = 'Modules.Humpchies.LocationModel.ErrorLocationIsInvalid';
    const NO_ERROR                                  = 'Modules.Humpchies.LocationModel.NoError';

    /**
    * Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}
    
    public static function getSupportedLocations()
    {
        if(self::$supported_locations !== NULL)
            return self::$supported_locations;
            
        return (self::$supported_locations = array( self::LOCATION_INCALL,
                                                    self::LOCATION_OUTCALL,
                                                    self::LOCATION_INCALL_OUTCALL,
                                                    self::LOCATION_UNKNOWN));
    }

    public static function isLocationInputValid(&$location)
    {   
        if(@in_array($location, self::getSupportedLocations()))
            return self::NO_ERROR;
            
        $location = NULL;
        return self::ERROR_LOCATION_IS_INVALID;    
    }

    public static function isLocationsInputValid(&$locations) {
        if (empty($locations)){
            $locations= self::LOCATION_UNKNOWN;
            return self::NO_ERROR;
        }
        foreach ($locations as $location){
            if ($location != self::LOCATION_INCALL && $location != self::LOCATION_OUTCALL)
            {
                return self::ERROR_LOCATION_IS_INVALID;
            }
        }
        //if (count($locations) == 2){
//            $locations = self::LOCATION_INCALL_OUTCALL;
//        }else{
//            $locations = $locations[0];
//        }

        return self::NO_ERROR;
    }
}
?>
