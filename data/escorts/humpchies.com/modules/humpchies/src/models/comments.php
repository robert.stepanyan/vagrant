<?php

Namespace Modules\Humpchies;

final class CommentsModel {

    //Db configs
    const DEFAULT_DB_CONFIG = '\Modules\Humpchies\Db\Default';
    const DEFAULT_DB_TABLE = 'comments';

    // Comment Statuses
    const STATUS_PENDING = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 2;

    const MAX_COMMENT_MESSAGE_LENGTH = 500;
    const MIN_COMMENT_MESSAGE_LENGTH = 10;


    const ERROR_COMMENT_IS_EMPTY = 'Modules.Humpchies.CommentsModel.ErrorCommentIsEmpty'; // Comment text is empty
    const ERROR_COMMENT_IS_TOO_SHORT = 'Modules.Humpchies.CommentsModel.ErrorCommentIsTooShort'; // Comment text is too short
    const ERROR_COMMENT_IS_TOO_LONG = 'Modules.Humpchies.CommentsModel.ErrorCommentIsTooLong'; // Comment text is too long
    const ERROR_UNKNOWN = 'Modules.Humpchies.CommentsModel.ErrorUnknown'; //Some error occurrence on creation
    const NO_ERROR = 'Modules.Humpchies.CommentsModel.NoError';
    /**
     * Because this class is untended to be a singleton we block access to its constructor.
     * Therefore the class can only be instantiated from within itself.
     */
    private function __construct()
    {}

    /**
     * Disable object copying
     */
    private function __clone()
    {}

    /**
     * Disable object serialization
     */
    private function __wakeup()
    {}

    /**
     * Disable object deserialization
     */
    private function __sleep()
    {}

    /**
     *   1.- restrict fields
     *   2.- restrict user
     *
     * @param mixed $fields
     *
     * @return string
     */
    public static function create($fields)
    {
        $fields['comment'] = mb_convert_encoding($fields['comment'], 'latin1', mb_detect_encoding($fields['comment']));
        $new_comment_id = \Core\DBConn::insert(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $fields);

        if($new_comment_id !== FALSE)
            return $new_comment_id;

        return self::ERROR_UNKNOWN;
    }

    /**
     * @param $user_id
     * @return array
     */
    public static function getActiveCommentsByUserId($user_id, $page_number = 1, $page_size = self::LIST_SIZE_DEFAULT_PC) {
       
        
        $paginate = ($page_size * ($page_number - 1)) . "," . $page_size;
//        exit();
        $sql =  'SELECT
                                                    c.*,
                                                    DATE_FORMAT(c.date, "%d.%m.%Y") as `date`,
                                                    m.username as member_name,
                        a.category as ad_category, a.city as ad_city, a.title as ad_title, a.status as ad_status
                                            FROM 
                                                    comments as c
                                            INNER JOIN ads as a on c.ad_id = a.id
                                            INNER JOIN users as m on c.from_user = m.id
                                            WHERE
                                                  c.status = '.self::STATUS_ACTIVE.'
                      AND c.user_id = ' . addslashes($user_id) . 
                " ORDER BY c.`date` DESC " . 
                " LIMIT {$paginate}";
        
        
        $Data = \Core\Utils\Data::findBySQL(self::DEFAULT_DB_CONFIG,
            $sql,
            FALSE,
            TRUE,
            array('enable' => false)
        );
        
        $Result['comments']['count'] = $Data['count'];
        return $Data;                                    
                                            
    }

    /**
     * @param $ad_id
     * @return array
     */
    public static function getActiveCommentsByAdId($ad_id) {
        return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    c.*,
                                                    m.username = member_name
                                            FROM 
                                                    comments as c
                                            INNER JOIN ads as a on c.ad_id = a.id
                                            INNER JOIN users as m on c.from_user = m.id
                                            WHERE 
                                                  c.status = '.self::STATUS_ACTIVE.'
                                                  AND c.ad_id = ' . addslashes($ad_id),  FALSE,     true,  array('expire' => 60));
    }

    public static function isCommentTextInputValid($comment_text) {
        switch(($comment_text_validity = \Core\Utils\Security::isStringInputValid($comment_text, self::MAX_COMMENT_MESSAGE_LENGTH, self::MIN_COMMENT_MESSAGE_LENGTH)))
        {
            case \Core\Utils\Security::NO_ERROR:
                return self::NO_ERROR;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_EMPTY:
                return self::ERROR_COMMENT_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_SHORT:
                return self::ERROR_COMMENT_IS_TOO_SHORT;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_LONG:
                return self::ERROR_COMMENT_IS_TOO_LONG;
                break;
            default:
                return $comment_text_validity;
                break;
        }
    }

    public static function hasPendingComment($user_id, $from_user_id) {
        $PendingComments = \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    c.*,
                                                    DATE_FORMAT(c.date, "%d.%m.%Y") as `date`,
                                                    m.username as member_name
                                            FROM 
                                                    comments as c
                                            INNER JOIN ads as a on c.ad_id = a.id
                                            INNER JOIN users as m on c.from_user = m.id
                                            WHERE
                                                  c.status = '.self::STATUS_PENDING.'
                                                  AND c.from_user = '. intval($from_user_id).'
                                                  AND c.user_id = ' . intval($user_id),  FALSE,     true,  array('expire' => 60));

        return !empty($PendingComments);
    }
    
    public static function getComments( $page_number = 1, $page_size = 20){
        $paginate = ($page_size * ($page_number - 1)) . "," . $page_size;
        $sql = "SELECT C.id,C.`comment`,C.`date`, A.title, A.category, A.city, U.username,hash, C.ad_id, C.up, C.down  FROM comments C
                LEFT JOIN ads A ON A.id = C.ad_id
                LEFT JOIN ad_images I ON C.ad_id = I.ad_id AND I.is_main = 1 AND I.`status` = 0
                LEFT JOIN users  U ON U.id = from_user
                WHERE C.`status`= 1 AND A.`status` = 'active' AND A.`blocked_comments` = 0 ORDER BY C.`date` desc  " . 
                " LIMIT {$paginate}";
        $data = \Core\Utils\Data::findBySQL(self::DEFAULT_DB_CONFIG,
            $sql,
            FALSE,
            TRUE,
            array('enable' => false)
        );
        
        return $data;                   
        
    }
    
    
    public static function getUserCommentsCount($user_id, $status = null) {
    
        $where = is_numeric($status)? "AND `status` = {$status}" : '';
        $sql = "SELECT count(id) as count  FROM comments WHERE user_id = {$user_id} {$where}";
        
        $data = \Core\Utils\Data::findBySQL(self::DEFAULT_DB_CONFIG,
            $sql,
            FALSE,
            TRUE,
            array('enable' => false)
        );
        return  $data["recordset"][0]['count'];
    }
    
    public static function updateVotes($comment_id, $vote, $remove = false) {
        
        $fields[] =  "{$vote} = {$vote} + 1";
        if($remove) {
            $fields[] = "{$remove} = {$remove} - 1";
        }
        $set = implode("," , $fields);
        
        //$sql = "UPDATE `comments` SET {$set} WHERE  `id`= {$comment_id}";
        //var_dump($sql);
        
        return \Core\DBConn::execute(self::DEFAULT_DB_CONFIG,
            "UPDATE ". self::DEFAULT_DB_TABLE ." SET {$set} WHERE `id` = " . $comment_id);
        
        
        //execute
    }
}