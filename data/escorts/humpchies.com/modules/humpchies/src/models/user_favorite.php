<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to interact with ad image related tasks. Such tasks might
 * but are not restricted to: add a new ad image, delete an ad image, etc.
 */

Namespace Modules\Humpchies;

final class UserFavoriteModel{

    // CONSTANTS
    // =========
    const DEFAULT_DB_CONFIG       = '\Modules\Humpchies\Db\Default';
    const DEFAULT_DB_TABLE        = 'user_favorites';

    
    private static $redisKey      = "UserFavorite-%d";
    
    
    /**
     * Because this class is untended to be a singleton we block access to its constructor.
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}

    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}

    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}

    // add tags
    
    public static function create($user_id, $ad_id) {
        
        if(!is_numeric($user_id))
            return false;
        
        if(!is_numeric($ad_id))
            return false;        
        
        $fields = [
            'ad_id' => $ad_id,
            'user_id' => $user_id
        ];
        
        $user_favorite = \Core\DBConn::insertIgnore(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $fields);    
        \Core\library\Redis::del(sprintf(self::$redisKey, $user_id ));  // delete redis
        return $user_favorite;
        
    }
    
    
    // edit
    //
    public static function delete($user_id, $ad_id) {
        
        // remove all existing tags and empty redis
        $deleted = \Core\DBConn::delete(self::DEFAULT_DB_CONFIG, self::DEFAULT_DB_TABLE, "ad_id = {$ad_id} AND user_id = {$user_id}");
        \Core\library\Redis::del(sprintf(self::$redisKey, $user_id ));    
        return $deleted;
    }
    
 
    public static function getUserFavoritesList($user_id) {
        
        $userFavritesList = \Core\library\Redis::get(sprintf(self::$redisKey, $user_id));
        //$userFavritesList = false;
        
        if($userFavritesList) {
            return unserialize($userFavritesList);
        }
        $userFavritesList = \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT ad_id FROM user_favorites WHERE user_id = '.$user_id,
            TRUE,
            FALSE,
            array('enable' => FALSE));
       
        if(!$userFavritesList)
            return false;
            
        $favorites = [];
        foreach($userFavritesList as $userFavorite)
            $favorites[] = $userFavorite['ad_id'];
            
        \Core\library\Redis::set(sprintf(self::$redisKey, $user_id), serialize($favorites));   
        return $favorites;    
    }
}
?>