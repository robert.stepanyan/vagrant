<?php


Namespace Modules\Humpchies;

final class BumpsModel{
    
    const DEFAULT_DB_CONFIG                         = '\Modules\Humpchies\Db\Default';
    const BUMP_COST = 1;
	
	const BUMPS_SETTINGS_ACTIVE = 1;
	const BUMPS_SETTINGS_EXPIRED = 2;
	const BUMPS_SETTINGS_CANCELED = 3;

	public static $CREDIT_OPTIONS = array(
        'cr_20' => 20,
        'cr_50' => 48,
        'cr_100' => 90,
        'cr_200' => 180,
        'cr_500' => 400
    );
    /*
    public static $CREDIT_OPTIONS = array(
	    'cr_25' => 20,
        'cr_58' => 48,
        'cr_110' => 90,
        'cr_225' => 180,
        'cr_600' => 400
	);
    */
    public static $CREDIT_OPTIONS_special = array(
        'cr_25' => 20,
        'cr_58' => 48,
        'cr_110' => 90,
        'cr_225' => 180,
        'cr_600' => 400
    );
    
    public static $bonus = array(
        'cr_25' => 5,
        'cr_58' => 8,
        'cr_110' => 10,
        'cr_225' => 25,
        'cr_600' => 100
    );
	
	public static $INTERVAL_OPTION_MINS = array(1, 5, 10, 30, 60 );
	public static $PERIODE_OPTION_HOURS =  array(1, 3, 6, 12, 24, 48, 72, 168 );
	
	public static $INTERVAL_OPTION_MINS_RENDERER = array(
		1 => array('available_periodes' => array(1, 3), 'show_as_hour' => false), 
		5 => array('available_periodes' => array(1, 3, 6, 12), 'show_as_hour' => false), 
		10 => array('available_periodes' => array(1, 3, 6, 12, 24), 'show_as_hour' => false),
		30 => array('available_periodes' => array(1, 3, 6, 12, 24, 48), 'show_as_hour' => false), 
		60 => array('available_periodes' => array(3, 6, 12, 24, 48, 72, 168), 'show_as_hour' => true) 
	);
	
	public static $PERIODE_OPTION_HOURS_RENDERER = array(
		1 => array('show_as_day' => false), 
		3 => array('show_as_day' => false), 
		6 => array('show_as_day' => false), 
		12 => array('show_as_day' => false), 
		24 => array('show_as_day' => false), 
		48 => array('show_as_day' => false), 
		72 => array('show_as_day' => false), 
		168 => array('show_as_day' => true)
	);
	
	public static function addSettings($data){
		
		
		$new_id = \Core\DBConn::insert(  self::DEFAULT_DB_CONFIG, 
                                            'bump_settings', 
                                            $data);
		
		return $new_id;
	}
	
	public static function getSettings($user_id, $ads_id, $status){
		
		$sql = "SELECT * FROM bump_settings WHERE status = ". $status ." AND user_id = ". $user_id. " AND ads_id = ". $ads_id;
		$bump_settings = \Core\DBConn::selectRow(self::DEFAULT_DB_CONFIG, $sql);
		
		return $bump_settings;
	}
	
	public static function getLastBump($user_id, $ads_id){
		
		$sql = "SELECT is_one_time, date, bump_settings_id FROM bumps_log WHERE user_id = ". $user_id. " AND ads_id = ". $ads_id . " ORDER BY id DESC LIMIT 1";
		$bump_settings = \Core\DBConn::selectRow(self::DEFAULT_DB_CONFIG, $sql);
		
		return $bump_settings;
	}
	
	public static function cancelSettings($ads_id){
		
		$new_id = \Core\DBConn::update(self::DEFAULT_DB_CONFIG, 
                                            'bump_settings',
											array('status' => self::BUMPS_SETTINGS_CANCELED),
                                            'ads_id = '. $ads_id . ' AND status = '. self::BUMPS_SETTINGS_ACTIVE);
		
		return $new_id;
		
	}
	
	public static function addBump($user_id, $ads_id, $is_one_time = 0, $bumps_settings_id = 0){
		
		$mysqli = \Core\DBConn::getConnection(self::DEFAULT_DB_CONFIG, TRUE);
		
		try{
			//$mysqli->begin_transaction();
						
			$data_log = array(
				'user_id' => $user_id,
				'ads_id' => $ads_id,
				'is_one_time' => $is_one_time,
				'bump_settings_id' => $bumps_settings_id
			);
			
			$new_id = \Core\DBConn::insert( self::DEFAULT_DB_CONFIG, 
                                            'bumps_log', 
                                            $data_log);
			
			$affected_ads_row = \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
									'ads',
									array('date_bumped' => time()),
									"id = $ads_id AND user_id = $user_id AND status = 'active' ");
			
			if(!$affected_ads_row){
				throw new \Exception('Unexpected error 1');
			}
			
			$affected_user_row = \Core\DBConn::execute(  self::DEFAULT_DB_CONFIG,
									'UPDATE users SET credits = credits - '. self::BUMP_COST. ' WHERE id = ' . $user_id . ' AND date_banned = 0'									
								);
			
			if(!$affected_user_row){
				throw new \Exception('Unexpected error 2');
			}
			//\Core\Cache::cleanAll();
			//$mysqli->commit();
		}
		catch(\Exception $e)
		{
			//$mysqli->rollback();
			echo $e->getMessage();
		}
		return $new_id;
	}
	
    public static function getPackFromPack($price){
        foreach(self::$CREDIT_OPTIONS as $option => $amount){
            if($amount >= $price){
                $credits = str_replace('cr_','',$option);
               return array('credits' => $credits, 'price' => $amount ); 
            }
        }
        return false;
    }
}
?>
