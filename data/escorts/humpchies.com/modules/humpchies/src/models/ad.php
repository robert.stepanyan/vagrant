<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to interact with ad related tasks. Such tasks might be
 * but are not restricted to: add a new ad, delete an ad, edit an ad, etc.
 */

Namespace Modules\Humpchies;

final class AdModel{
    // CONSTANTS
    // =========

    const AD_PACKAGE_ACTIVE_STATUS = 2;
    const AD_PACKAGE_PENDING_STATUS = 1;

    // Number of items in lists
    const LIST_SIZE_DEFAULT_PC                  = 15;                                   // Default PC list size
    const LIST_SIZE_DEFAULT_MOBILE              = 15;                                   // Default Mobile list size

    // Database
    const DEFAULT_DB_CONFIG                     = '\Modules\Humpchies\Db\Default';      // The config file to use by default
    const DEFAULT_DB_TABLE                      = 'ads';                                // The default table to use

    // Input (forms) minimum /maximum sizes
    const MIN_LEN_TITLE                         = 3;                                    // Title minimum length
    const MAX_LEN_TITLE                         = 64;                                   // Title maximum length

    const MIN_PRICE                             = 0;                                    // Minimum price
    const MAX_PRICE                             = 9999;                                 // Maximum price

    const MIN_LEN_PRICE_CONDITIONS              = 0;                                    // Price conditions minimum length
    const MAX_LEN_PRICE_CONDITIONS              = 32;                                   // Price conditions maximum length

    const MIN_LEN_SEARCH_QUERY                  = 3;                                    // Search string minimum length
    const MAX_LEN_SEARCH_QUERY                  = 30;                                   // Search string maximum length

    const MIN_LEN_DESCRIPTION                   = 3;                                    // Description minimum length
    const MAX_LEN_DESCRIPTION                   = 1000;                                 // Description maximum length

    // Image dimensions: HEIGHTS
    const AD_IMAGE_HEIGHT_PORTRAIT              = 390;                                  // Portrait
    const AD_IMAGE_HEIGHT_LANDSCAPE             = 173;                                  // Landscape
    const AD_IMAGE_HEIGHT_SQUARED               = 260;                                  // Squared

    // Image dimensions: WIDTHS
    const AD_IMAGE_WIDTH                        = 260;                                  // All images have the same width

    // Errors
    const ERROR_AD_DELETION_FAILED              = 'Modules.Humpchies.AdModel.ErrorAdDeletionFailed';
    const ERROR_AD_DELETION_TAG_FAILED          = 'Modules.Humpchies.AdModel.ErrorAdDeletionTagFailed';
    const ERROR_AD_REPOST_FAILED                = 'Modules.Humpchies.AdModel.ErrorAdRepostFailed';
    const ERROR_AD_STOP_FAILED                  = 'Modules.Humpchies.AdModel.ErrorAdStopFailed';
    const ERROR_DESCRIPTION_IS_DUPLICATED       = 'Modules.Humpchies.AdModel.ErrorDescriptionIsDuplicated';             // Description already exists
    const ERROR_DESCRIPTION_IS_EMPTY            = 'Modules.Humpchies.AdModel.ErrorDescriptionIsEmpty';                  // Description is empty
    const ERROR_DESCRIPTION_IS_TOO_SHORT        = 'Modules.Humpchies.AdModel.ErrorDescriptionIsTooShort';               // Description is too short
    const ERROR_DESCRIPTION_IS_TOO_LONG         = 'Modules.Humpchies.AdModel.ErrorDescriptionIsTooLong';                // Description is too long
    const ERROR_MOBILITY_IS_INVALID             = 'Modules.Humpchies.AdModel.ErrorMobilityIsInvalid';                   // Mobility is Invalid
    const ERROR_PRICE_IS_INVALID                = 'Modules.Humpchies.AdModel.ErrorPriceIsInvalid';                      // Invalid donation amount
    const ERROR_PRICE_CONDITIONS_IS_EMPTY       = 'Modules.Humpchies.AdModel.ErrorPriceConditionsIsEmpty';              // Title is empty
    const ERROR_PRICE_CONDITIONS_IS_TOO_SHORT   = 'Modules.Humpchies.AdModel.ErrorPriceConditionsIsTooShort';           // Title is too short
    const ERROR_PRICE_CONDITIONS_IS_TOO_LONG    = 'Modules.Humpchies.AdModel.ErrorPriceConditionsIsTooLong';            // Title is too long
    const ERROR_SEARCH_QUERY_IS_EMPTY           = 'Modules.Humpchies.AdModel.ErrorSearchQueryIsEmpty';                  // Search string is empty
    const ERROR_SEARCH_QUERY_IS_INVALID         = 'Modules.Humpchies.AdModel.ErrorSearchQueryIsInvalid';                // Search string is empty
    const ERROR_SEARCH_QUERY_IS_TOO_SHORT       = 'Modules.Humpchies.AdModel.ErrorSearchQueryIsTooShort';               // Search string is too short
    const ERROR_SEARCH_QUERY_IS_TOO_LONG        = 'Modules.Humpchies.AdModel.ErrorSearchQueryIsTooLong';                // Search string is too long
    const ERROR_TITLE_IS_EMPTY                  = 'Modules.Humpchies.AdModel.ErrorTitleIsEmpty';                        // Title is empty
    const ERROR_TITLE_IS_TOO_SHORT              = 'Modules.Humpchies.AdModel.ErrorTitleIsTooShort';                     // Title is too short
    const ERROR_TITLE_IS_TOO_LONG               = 'Modules.Humpchies.AdModel.ErrorTitleIsTooLong';                      // Title is too long
    const ERROR_UNKNOWN                         = 'Modules.Humpchies.AdModel.ErrorUnknown';
    const NO_ERROR                              = 'Modules.Humpchies.AdModel.NoError';



    const ERROR_AD_ID_IS_INVALID                = 0x4;                                  // Invalid ID
    const ERROR_USER_ID_IS_INVALID              = 0x5;                                  // Invalid user ID
    const ERROR_USER_IS_BANNED                  = 0x6;                                  // The user is banned
    const ERROR_CITY_IS_INVALID                 = 0x7;                                  // Invalid city


    const ERROR_UNABLE_TO_ACTIVATE_AD           = 0x16;                                 // Not able to activate ad
    const DEFAULT_LIST_SIZE_PC                  = 25;                                

    private static $VipAdIds = [];
    private static $PremiumAdIds = [];
    private static $searchKeywords = [
        'pipe' => ['fellation', 'pipe']
    ];
    
    public static $pricesIntervals = [
        '<100'    => '<100 CAD',
        '100-200' => '100-200 CAD',
        '200-300' => '200-300 CAD',
        '>300'    => '300+ CAD'
    ];
    
    

    /**
     * Because this class is untended to be a singleton we block access to its constructor.
     * Therefore the class can only be instantiated from within itself.
     */
    private function __construct()
    {}

    public static function getMoreAdsByUserId($user_id) {

    }


    /**
     * Disable object copying
     */
    private function __clone()
    {}

    /**
     * Disable object serialization
     */
    private function __wakeup()
    {}

    /**
     * Disable object deserialization
     */
    private function __sleep()
    {}

    /**
     * @return array
     */
    public static function getVipAdIds() {
        return self::$VipAdIds;
    }

    /**
     * @param array $VipAdIds
     */
    public static function setVipAdIds($VipAdIds) {
        self::$VipAdIds = $VipAdIds;
    }

    /**
     * @return array
     */
    public static function getPremiumAdIds() {
        return self::$PremiumAdIds;
    }

    /**
     * @param array $PremiumAdIds
     */
    public static function setPremiumAdIds($PremiumAdIds) {
        self::$PremiumAdIds = $PremiumAdIds;
    }
    
    public static function userOwnsAd($adID, $userID){
          
        $result =  \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG,
        'SELECT id
        FROM ads
        WHERE  id = ' . $adID . ' AND user_id = ' . $userID,
        FALSE);  
        return !!($result['id']);
    }

    /**
     * put your comment there...
     *
     * @param mixed $phone
     * @return array
     */
    public static function checkIfPhoneBlacklisted($phone){
        $phone_n = preg_replace('/[^0-9]/', '', $phone);

        return \Core\DBConn::selectRow(
            self::DEFAULT_DB_CONFIG,
            'SELECT
                name
            FROM 
                blacklisted_phones
            WHERE 
                name = \'' . $phone_n . '\'',
            TRUE);
    }    
    public static function checkIfEmailBlacklisted($email){
         $email=trim(strtolower($email));

        //Check if valida email
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
            return true;    
        }
        $domain = end(explode('@', $email));
    
        $whitelist= \Core\DBConn::selectRow(
            self::DEFAULT_DB_CONFIG,
            'SELECT
                domain
            FROM 
                domain_whitelist
            WHERE 
                domain = \'' . $domain . '\'',
            TRUE);
        return !$whitelist;   
    }

    /**
     * Todo:
     *
     *   1.- restrict fields
     *   2.- restrict user
     *
     * @param mixed $fields
     * @param mixed $images
     * @param mixed $cover_image_index
     *
     * @return string
     */
    public static function create(&$fields, &$images = NULL, $cover_image_index = 0)
    {
        //echo "create ad";
        //var_dump($fields);
//        //die();
        $new_ad_id = \Core\DBConn::insert(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $fields);
       // die();
        if($new_ad_id !== FALSE)
            return $new_ad_id;

        $last_db_error = \Core\DBConn::getLastError(self::DEFAULT_DB_CONFIG, TRUE);

        if(strpos($last_db_error, 'Duplicate entry \'') !== FALSE && (strrpos($last_db_error, 'for key \'description\'', -21) !== FALSE || strrpos($last_db_error, 'for key 2', -9)))
            return self::ERROR_DESCRIPTION_IS_DUPLICATED;
        else
            return self::ERROR_UNKNOWN;
    }
    
    public static function report($fields){
        $new_ad_id = \Core\DBConn::insert(  self::DEFAULT_DB_CONFIG,
            'reported',
            $fields);

        if($new_ad_id !== FALSE)
            return $new_ad_id;
        return 0;
    }

    /***
     * put your comment there...
     *
     * @param mixed $id
     * @param mixed $fields
     * @param mixed $images
     * @param mixed $cover_image_index
     */
    public static function edit($id, $user_id, $fields)
    {
        $affected_rows = \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $fields,
            "id = $id AND user_id = $user_id AND status IN('active', 'inactive', 'paused')");

        if($affected_rows)
            return TRUE;

        $last_db_error = \Core\DBConn::getLastError(self::DEFAULT_DB_CONFIG, TRUE);

        if(empty($last_db_error))
            return TRUE;

        if(strpos($last_db_error, 'Duplicate entry \'') !== FALSE && (strrpos($last_db_error, 'for key \'description\'', -21) !== FALSE || strrpos($last_db_error, 'for key 2', -9)))
            return self::ERROR_DESCRIPTION_IS_DUPLICATED;
        else
            return self::ERROR_UNKNOWN;
    }
    
    public static function changeVerificationStatus($id, $user_id)
    {
        $affected_rows = \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            array('verification_status' => 'updated'),
            "id = $id AND user_id = $user_id AND status IN('active', 'inactive', 'paused')");

        if($affected_rows)
            return TRUE;

        $last_db_error = \Core\DBConn::getLastError(self::DEFAULT_DB_CONFIG, TRUE);

        if(empty($last_db_error))
            return TRUE;

        if(strpos($last_db_error, 'Duplicate entry \'') !== FALSE && (strrpos($last_db_error, 'for key \'description\'', -21) !== FALSE || strrpos($last_db_error, 'for key 2', -9)))
            return self::ERROR_DESCRIPTION_IS_DUPLICATED;
        else
            return self::ERROR_UNKNOWN;
    }
    
    public static function changePackCity($id, $user_id, $city)
    {
        $affected_rows = \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
            'order_packages',
            ['city' => $city],
            "ads_id = $id");

        if($affected_rows)
            return TRUE;

        $last_db_error = \Core\DBConn::getLastError(self::DEFAULT_DB_CONFIG, TRUE);

        if(empty($last_db_error))
            return TRUE;

        if(strpos($last_db_error, 'Duplicate entry \'') !== FALSE && (strrpos($last_db_error, 'for key \'description\'', -21) !== FALSE || strrpos($last_db_error, 'for key 2', -9)))
            return self::ERROR_DESCRIPTION_IS_DUPLICATED;
        else
            return self::ERROR_UNKNOWN;
    }
    
    public static function ban($id, $user_id){
        $fields['status'] = 'banned';
        $affected_rows = \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $fields,
            "id = $id AND user_id = $user_id AND status IN('active', 'inactive', 'paused')");

        if($affected_rows)
            return TRUE;

    }
    
    public static function banUserAds( $user_id){
        $fields['status'] = 'banned';
        $affected_rows = \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $fields,
            " user_id = $user_id AND status IN('active', 'inactive', 'paused')");

        if($affected_rows)
            return TRUE;

    }

    /***
     * put your comment there...
     *
     * @param mixed $id
     * @param mixed $user_id
     * @param mixed $fields
     */
    public static function editRevision($id, $user_id, $fields, $status, $originl_ad = [])
    {
        
        // if only the city is updated, just add the changes directly, otherwise send to revision
        $base_ad = array_intersect_key($originl_ad, $fields);
        
        $differences = array_diff_assoc($base_ad, $fields);
        
        unset($differences['location']);
        unset($differences['city']);
        unset($differences['city_id']);
        unset($differences['region_id']);
        unset($differences['price_min']);
        //unset($fields['price_min']);
        unset($differences['price_max']);
        //unset($fields['price_max']);
        
        //if(\Core\Utils\Session::getCurrentUserID() == 63661) {
            unset($differences['price']);    
            unset($differences['price_conditions']);    
        //}
        
        if(!empty($differences)) {
            $fields['approved_status'] = 0;
            $is_updated = '1';
        } else {
            $fields['approved_status'] = 1;
            $is_updated = '0';
        }
        
        $_table = 'ads_revision';
        $fields['status'] = $status;
        //$fields['approved_status'] = 0;

        $data = \Core\DBConn::select( self::DEFAULT_DB_CONFIG,
            'SELECT count(*) as \'r_count\'
            FROM ' . $_table . '
            WHERE ads_id = ' . $id . ' AND user_id = ' . $user_id . ' AND status IN(\'active\', \'inactive\', \'paused\')',
            FALSE);

        if( $data[0]['r_count'] > 0 ){
            $affected_rows = \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
                'ads_revision',
                $fields,
                "ads_id = $id AND user_id = $user_id AND status IN('active', 'inactive', 'paused')");
        } else {
            $fields['ads_id'] = $id;
            $fields['user_id'] = $user_id;
            $fields['date_added'] = time();
            $affected_rows = \Core\DBConn::insert(  self::DEFAULT_DB_CONFIG,
                'ads_revision',
                $fields);
        }

        if($affected_rows && self::setRevisionStatus($id, $is_updated, $fields))
            return TRUE;

        $last_db_error = \Core\DBConn::getLastError(self::DEFAULT_DB_CONFIG, TRUE);

        if(empty($last_db_error))
            return TRUE;

        if(strpos($last_db_error, 'Duplicate entry \'') !== FALSE && (strrpos($last_db_error, 'for key \'description\'', -21) !== FALSE || strrpos($last_db_error, 'for key 2', -9)))
            return self::ERROR_DESCRIPTION_IS_DUPLICATED;
        else
            return self::ERROR_UNKNOWN;
    }
    
    public static function adVerificationCreate($fields){
          $new_ad_id = \Core\DBConn::insert(  self::DEFAULT_DB_CONFIG,
            "ad_verification",
            $fields);

        if($new_ad_id !== FALSE)
            return $new_ad_id;

    }
   
    public static function getVerificationLastStatus($adID){
        $sql = "SELECT verification_status FROM ad_verification WHERE ad_id = " . intval($adID) . " ORDER BY id DESC LIMIT 1";
        return \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG, $sql , FALSE);
        
    }
    
    public static function getVerificationLast($adID){
        $sql = "SELECT  ad_id, user_id, images, verification_status FROM ad_verification WHERE ad_id = " . intval($adID) . " ORDER BY id DESC LIMIT 1";
        return \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG, $sql , FALSE);
        
    }

    public static function setRevisionStatus( $ad_id, $rev_status, $fields = false){
        
        $dump = array('is_updated' => $rev_status, 
                  'date_revision' => time()); 
        if($fields){
            $dump['title_new'] = $fields['title'];
            $dump['description_new'] = $fields['description'];
            $dump['city_new'] = $fields['city'];
            $dump['category_new'] = $fields['category'];
            $dump['city_id_new'] = $fields['city_id'];
            $dump['region_id_new'] = $fields['region_id'];
            
            $dump['city'] = $fields['city'];
            $dump['city_id'] = $fields['city_id'];
            $dump['region_id'] = $fields['region_id'];
            $dump['location'] = $fields['location'];
            $dump['price_min'] = $fields['price_min'];
            $dump['price_max'] = $fields['price_max'];
            
        }
       // \Core\library\log::debug($dump, "dump");            
        return \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $dump,
            "id = $ad_id");
    }

    public static function getRevisionData($ad_id, $user_id)
    {
        return \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG,
        'SELECT *
        FROM ads_revision
        WHERE approved_status = 0 AND ads_id = ' . $ad_id . ' AND user_id = ' . $user_id,
        FALSE);
    }

    public static function getAdIsUpdatedStatus($ad_id, $user_id)
    {
        return \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG,
        'SELECT is_updated
        FROM ads
        WHERE id = ' . $ad_id . ' AND user_id = ' . $user_id,
        FALSE);
    }
    
    public static function getAdStatus($oldAd)
    {
       
        return \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG,
        'SELECT user_id, verification_status
        FROM ads
        WHERE id = ' . $oldAd ,
        FALSE);
    }
    
    public static function getAdPhoneValidation ($AdId, $userID)
    {
       
        return \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG,
        'SELECT id,phone, validation_id, dialing_number, phone_validated, status
        FROM ads
        WHERE id = ' . $AdId . " and `user_id` = " . $userID ,
        FALSE);
    }
    
    public static function getAdPhoneValidationFromRevision ($AdId, $userID)
    {
       
        return \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG,
        'SELECT ads_id as id, phone, validation_id, dialing_number, phone_validated 
            FROM ads_revision WHERE 
            ads_id = ' . $AdId . " and `user_id` = " . $userID ,
        FALSE);
    }
    
     public static function getAdInfo($key)
    {   
        return \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG,
        'SELECT id as ad_id, user_id 
        FROM ads
        WHERE validation_id = \'' . $key . '\' '  ,
        FALSE);
    }
           
     public static function getRevisionAdInfo($key)
    {  
        return \Core\DBConn::selectRow( self::DEFAULT_DB_CONFIG,
        'SELECT ads_id as ad_id, user_id 
        FROM ads_revision
        WHERE validation_id = \'' . $key . '\' '  ,
        FALSE);
    }
       

    /***
     * put your comment there...
     *
     * @param mixed $ad_id
     * @param mixed $user_id
     */
    public static function delete($ad_id, $user_id)
    {
        if(\Core\DBConn::delete(self::DEFAULT_DB_CONFIG,
                self::DEFAULT_DB_TABLE,
                'id = ' . $ad_id . ' AND user_id = ' . $user_id) === 1)
            return self::NO_ERROR;

        return self::ERROR_AD_DELETION_FAILED;
    }


    public static function changePremiumcity($order_package_id, $CityName)
    {
        $CityName = addslashes($CityName);
        $ad_id = addslashes($ad_id);

        if(\Core\DBConn::execute(self::DEFAULT_DB_CONFIG,
                "UPDATE order_packages opa
                                INNER JOIN cities c ON c.name = '".$CityName."'
                                INNER JOIN zones z on z.id = c.zone_id
                                set opa.zone = z.name, opa.city = '".$CityName."' where opa.id = ".$order_package_id) === true)
            return self::NO_ERROR;

        return self::ERROR_UNKNOWN;
    }

    /***
     * put your comment there...
     *
     * @param mixed $ad_id
     * @param mixed $user_id
     */
    public static function tagForDeletion($ad_id, $user_id)
    {
        if(\Core\DBConn::update(self::DEFAULT_DB_CONFIG,
                self::DEFAULT_DB_TABLE,
                array('status' => 'delete', 'deletedbycron' => 1),
                'id = ' . $ad_id . ' 
                                AND 
                                user_id = ' . $user_id) === 1)
            return self::NO_ERROR;

        return self::ERROR_AD_DELETION_TAG_FAILED;
    }

    /***
     * put your comment there...
     *
     * @param mixed $ad_id
     * @param mixed $user_id
     */
    public static function stop($ad_id, $user_id)
    {
        if(\Core\DBConn::update(self::DEFAULT_DB_CONFIG,
                self::DEFAULT_DB_TABLE,
                array(  'date_released' => 0, 'status' => 'paused'),
                'id = ' . $ad_id . ' 
                                AND 
                                status = \'active\' 
                                AND 
                                user_id = ' . $user_id) === 1)
            return self::NO_ERROR;

        return self::ERROR_AD_STOP_FAILED;
    }

    /***
     * put your comment there...
     *
     * @param mixed $ad_id
     * @param mixed $user_id
     */
    public static function repost($ad_id, $user_id) {
        
        $arrUpdate = ['date_released' => time(), 'status' => 'active'];
        
         // check if user has comments
        $no_comments = \Modules\Humpchies\CommentsModel::getUserCommentsCount( $user_id, 1);
        if($no_comments > 0) {
            $arrUpdate['has_comments'] = 1;    
        }
        
        if(\Core\DBConn::update(self::DEFAULT_DB_CONFIG,
                self::DEFAULT_DB_TABLE,
                $arrUpdate,
                'id = ' . $ad_id . ' 
                                AND 
                                status IN(\'active\', \'inactive\', \'paused\') 
                                AND 
                                user_id = ' . $user_id) === 1)
            return self::NO_ERROR;

        return self::ERROR_AD_REPOST_FAILED;
    }
    
    /**
     * @param null $category_name
     * @param null $region
     * @param null $city_name
     * @param int $page_number
     * @param int $page_size
     * @return array     // TEMPORARY TILL AD LISTING DESIGN RELEASE
     */
    public static function getListOfActiveAdsByCategoryAndCityV2($category_name = NULL, $region = NULL, $city_name = NULL, $filters = NULL, $page_number = 1, $page_size = self::LIST_SIZE_DEFAULT_PC) {
        $where = "";
        $vipField = ' 0 as vip';
        $vipOrdering = $premiumOrdering = '';
        $premiumField = ' 0 as premium';
        $bump_ordering = '';
        $innerJoin = '';
        

        if (!empty($city_name) || !empty($category_name)){
            $bump_ordering = ' date_bumped > UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL 24 HOUR)) DESC,';
        }

        if (!empty($category_name)){
            $where .= "AND category = '$category_name'";
        }
        
        if (!empty($region)) {
            
            $region_details = \Modules\Humpchies\RegionModel::getRegionDetails($region);
            $where .= " AND region_id = " . $region_details['id'];
        }
       
        if (!empty($filters['location']) && in_array($filters['location'], ['incalls', 'outcalls'])){
            $where .= " AND location IN ('" . $filters['location'] . "', 'both')";
        }
        
        if (!empty($filters['real_photos']) && $filters['real_photos'] == 1){
            $where .= " AND  `verification_status` = 'verified'";
        }
        
        if (!empty($filters['has_comments']) && $filters['has_comments'] == 1){
            $where .= " AND  `blocked_comments` = 0 AND  `has_comments` = 1";
        }
        
        if (!empty($filters['accepts_comments']) && $filters['accepts_comments'] == 1){
            $where .= " AND  `blocked_comments` = 0";
        }
        
        // filter by price
        if(!empty($filters['prices'])) {
            
            $where .= " AND (`price_min` > 0 OR `price_max` > 0)";
            $pricesIntervals =  explode(",", $filters['prices']);
            
            // if all are selected, just make sure to display the ads with prices set
            if(count($pricesIntervals) < count(self::$pricesIntervals)) {
                
                $wherePrice = [];
                
                if(in_array('<100', $pricesIntervals)) {
                    $wherePrice[] = " (`price_min` < 100 OR `price_max` < 100)";    
                }
                
                if(in_array('100-200', $pricesIntervals)) {
                    $wherePrice[] = " (`price_min` BETWEEN 100 AND 200 OR `price_max` BETWEEN 100 AND 200)";    
                }
                
                if(in_array('200-300', $pricesIntervals)) {
                    $wherePrice[] = " (`price_min` BETWEEN 200 AND 300 OR `price_max` BETWEEN 300 AND 400)";    
                }
                
                if(in_array('>300', $pricesIntervals)) {
                    $wherePrice[] = " (`price_min` > 300 OR `price_max` > 300)";    
                }
                
                $where .= ' AND (' . implode(' OR ', $wherePrice) . ')';
            }  
        }
        
        
        $phone = $filters['phone1'] . $filters['phone2']. $filters['phone3'];
        $partial_phone = [$filters['phone1'], $filters['phone2'], $filters['phone3']];
        
        if($phone != '' && is_numeric($phone)) {
            $where .= " AND A.`phone` ";
            $where .= (strlen($phone) == 10)? ' = '.$phone : " LIKE  '%" . implode('%', array_filter($partial_phone)) . "%'";
        }
        
        switch(true) {
            
            case (!empty($city_name)):
            $CityName = \Modules\Humpchies\CityModel::parseCityWithAdsCode($city_name, $category_name);
            $where .= " AND city = '$CityName' ";
                break;
            
            case (empty($city_name) && !empty($region)):
                break;    
                
            default:
                break;
            
        }
    
        $wherevip=[];
    
        if (!empty(self::$VipAdIds)){
            $vipIds = implode(',',array_reverse(self::$VipAdIds));
            $vipField = " IF(FIND_IN_SET(A.id,'{$vipIds}'), 1, 0) as vip ";
            $wherevip[] = " A.id IN({$vipIds}) ";
            $vipOrdering = " FIELD(A.id,{$vipIds}) DESC, ";
        }

        if (!empty(self::$PremiumAdIds)){
            $premiumIds = implode(',',array_reverse(self::$PremiumAdIds));
            $premiumField = " IF(FIND_IN_SET(A.id,'{$premiumIds}'), 1, 0) as premium ";
            $wherevip[] = " A.id IN({$premiumIds}) ";
            $premiumOrdering = " FIELD(A.id,{$premiumIds}) DESC, ";
        }
    
        if(!empty($wherevip) && empty($filters) ){
                $where .= " OR (" . implode(" OR ", $wherevip ) ." ) ";     
        }
           
        $paginate = ($page_size * ($page_number - 1)) . "," . $page_size;
        
        // origina; sql
        $sql =  "
        SELECT
          A.id,
          images,
          date_released,
          category,
          city,
          title,
          description,
          date_bumped,
          `verification_status`,
          {$vipField},
          {$premiumField}
        FROM
          ads A
        {$innerJoin}
        WHERE
          A.`status` = 'active'
          AND city <> ''
          
          
          {$where}
        ORDER BY
            {$vipOrdering}
            {$premiumOrdering}
            {$bump_ordering}    
            date_released DESC
        LIMIT {$paginate}";
        
        
        if(!empty($filters['services'])) { // we have services, rewrite sql
            //var_dump("SERVICES+++++++++++++++++".$services);
            $servicesIds = \Modules\Humpchies\TagModel::findTagIdBySlugList($filters['services']);
            //var_dump($servicesIds); 
            
            if($servicesIds) {
                
                $sql = "
                    SELECT
                          A.id,
                          A.images,
                          A.date_released,
                          A.category,
                          A.city,
                          A.title,
                          A.description,
                          A.date_bumped,
                          A.`verification_status`,
                          {$vipField},
                          {$premiumField}
                        FROM ads_tags AT
                            LEFT JOIN ads A ON A.id = AT.ad_id
                            LEFT JOIN tags T ON T.id = AT.tag_id    
                        {$innerJoin}
                        WHERE
                            A.`status` = 'active' 
                            AND A.city<>'' 
                            {$where}
                            AND AT.tag_id IN ({$servicesIds})
                        GROUP BY A.id
                        ORDER BY
                            {$vipOrdering}
                            {$premiumOrdering}
                            {$bump_ordering}    
                            date_released DESC
                        LIMIT {$paginate}";                
            }  
        }
        
        //var_dump($sql);
        //echo "<!-- " . $sql . "-->";
        /* CACHING ISSUE AND date_released <= " . \Core\Utils\Time::getCommonTimeStamp() . "*/
        $Result = [
            'ads'           => [],
            'premium_ads'   => [],
            'vip_ads'       => []
        ];
        
        $Data = \Core\Utils\Data::findBySQL(self::DEFAULT_DB_CONFIG,
            $sql,
            FALSE,
            TRUE,
            array('enable' => false)
        );
        
        foreach ($Data['recordset'] as $ad){
            if ($ad['vip'] == 1){
                $Result['vip_ads'][] = $ad;
            }elseif ($ad['premium'] == 1){
                $Result['premium_ads'][] = $ad;
            }else{
                $Result['ads']['recordset'][] = $ad;
            }
        }
        $Result['ads']['count']=$Data['count'];
        return $Result;
    }    
    
    
    /**
     * @param null $category_name
     * @param null $region
     * @param null $city_name
     * @param int $page_number
     * @param int $page_size
     * @return array
     */
    public static function getListOfActiveAdsByCategoryAndCityV3($category_name = NULL, $region = NULL, $city_name = NULL, $location = NULL, $services = NULL, $page_number = 1, $page_size = self::LIST_SIZE_DEFAULT_PC)
    {
        $where = "";
        $vipField = ' 0 as vip';
        $vipOrdering = $premiumOrdering = '';
        $premiumField = ' 0 as premium';
        $bump_ordering = '';

        if (!empty($city_name) || !empty($category_name)){
            $bump_ordering = ' date_bumped > UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL 24 HOUR)) DESC,';
        }

        if (!empty($category_name)){
            $where .= "AND category = '$category_name'";
        }
        
        if (!empty($region)) {
            
            $region_details = \Modules\Humpchies\RegionModel::getRegionDetails($region);
            $where .= " AND region_id = " . $region_details['id'];
        }
        
        
        if (!empty($location) AND in_array($location, ['incalls', 'outcalls'])){
            $where .= " AND location IN ('$location', 'both')";
        }

        switch(true) {
            
            case (!empty($city_name)):
                $CityName = \Modules\Humpchies\CityModel::parseCityWithAdsCode($city_name, $category_name);
                $where .= " AND city = '$CityName' ";
                break;
            
            case (empty($city_name) && !empty($region)):
                break;    
            
            default:
                break;
            
        }
        
        
        
        
       // if (!empty($city_name)){
//            $CityName = \Modules\Humpchies\CityModel::parseCityWithAdsCode($city_name, $category_name);
//            $where .= " AND city = '$CityName' ";
//        } 
        $wherevip=[];
        if (!empty(self::$VipAdIds)){
            $vipIds = implode(',',array_reverse(self::$VipAdIds));
            $vipField = " IF(FIND_IN_SET(A.id,'{$vipIds}'), 1, 0) as vip ";
            $wherevip[] = " A.id IN({$vipIds}) ";
            $vipOrdering = " FIELD(A.id,{$vipIds}) DESC, ";
        }

        if (!empty(self::$PremiumAdIds)){
            $premiumIds = implode(',',array_reverse(self::$PremiumAdIds));
            $premiumField = " IF(FIND_IN_SET(A.id,'{$premiumIds}'), 1, 0) as premium ";
            $wherevip[] = " A.id IN({$premiumIds}) ";
            $premiumOrdering = " FIELD(A.id,{$premiumIds}) DESC, ";
        }
        if(!empty($wherevip) && empty($location) ){
                $where .= " OR (" . implode(" OR ", $wherevip ) ." ) ";     
        }
           
        $paginate = ($page_size * ($page_number - 1)) . "," . $page_size;
        
        // origina; sql
        $sql =  "
        SELECT
          A.id,
          images,
          date_released,
          category,
          city,
          title,
          description,
          date_bumped,
          `verification_status`,
          {$vipField},
          {$premiumField}
        FROM
          ads A
        WHERE
          `status` = 'active'
          AND city <> ''
          
          
          {$where}
        ORDER BY
            {$vipOrdering}
            {$premiumOrdering}
            {$bump_ordering}    
            date_released DESC
        LIMIT {$paginate}";
        
        
        if(!empty($services)) { // we have services, rewrite sql
            //var_dump("SERVICES+++++++++++++++++".$services);
            $servicesIds = \Modules\Humpchies\TagModel::findTagIdBySlugList($services);
            //var_dump($servicesIds); 
            
            if($servicesIds) {
                
                $sql = "
                    SELECT
                          A.id,
                          A.images,
                          A.date_released,
                          A.category,
                          A.city,
                          A.title,
                          A.description,
                          A.date_bumped,
                          A.`verification_status`,
                          {$vipField},
                          {$premiumField}
                        FROM ads_tags AT
                            LEFT JOIN ads A ON A.id = AT.ad_id
                            LEFT JOIN tags T ON T.id = AT.tag_id    
                        WHERE
                            A.`status` = 'active' 
                            AND A.city<>'' 
                            {$where}
                            AND AT.tag_id IN ({$servicesIds})
                        GROUP BY A.id
                        ORDER BY
                            {$vipOrdering}
                            {$premiumOrdering}
                            {$bump_ordering}    
                            date_released DESC
                        LIMIT {$paginate}";                
            }  
        }
        
        
        

        //echo "<!-- " . $sql . "-->";
        /* CACHING ISSUE AND date_released <= " . \Core\Utils\Time::getCommonTimeStamp() . "*/
        $Result = [];
        $Data = \Core\Utils\Data::findBySQL(self::DEFAULT_DB_CONFIG,
            $sql,
            FALSE,
            TRUE,
            array('enable' => false)
        );
        
        foreach ($Data['recordset'] as $ad){
            if ($ad['vip'] == 1){
                $Result['vip_ads'][] = $ad;
            }elseif ($ad['premium'] == 1){
                $Result['premium_ads'][] = $ad;
            }else{
                $Result['ads']['recordset'][] = $ad;
            }
        }
        $Result['ads']['count']=$Data['count'];
        return $Result;
    }    
    
    public static function getOrderOfActiveAdsByCategoryAndCity($category_name = NULL, $city_name = NULL, $start = 0, $limit = 1000)
    {
        $where = "";
        $vipField = ' 0 as vip';
        $vipOrdering = $premiumOrdering = '';
        $premiumField = ' 0 as premium';
        $bump_ordering = '';

        if (!empty($city_name) || !empty($category_name)){
            $bump_ordering = 'date_bumped > UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL 24 HOUR)) DESC, ';
        }

        if (!empty($category_name)){
            $where .= "AND category = '$category_name'";
        }
        

        $CityName = \Modules\Humpchies\CityModel::parseCityWithAdsCode($city_name, $category_name);
      
        $where .= " AND city = '$CityName' ";

        if (!empty(self::$VipAdIds)){
            $vipIds = implode(',', array_reverse(self::$VipAdIds));
            $vipField = " IF(FIND_IN_SET(id, '{$vipIds}'), 1, 0) as vip ";
            $where .= " OR id IN({$vipIds}) ";
            $vipOrdering = " FIELD(id,{$vipIds}) DESC, ";
        }

        if (!empty(self::$PremiumAdIds)){
            $premiumIds = implode(',', array_reverse(self::$PremiumAdIds));
            $premiumField = " IF(FIND_IN_SET(id, '{$premiumIds}'), 1, 0) as premium ";
            $where .= " OR id IN ({$premiumIds}) ";
            $premiumOrdering = " FIELD (id, {$premiumIds}) DESC, ";
        }
          $count = false;
         if($start == 0 ){
             $count = true;
         }
        
        $sql =  "SELECT id FROM ads WHERE `status` = 'active'
          {$where}
        ORDER BY
            {$vipOrdering}
            {$premiumOrdering}
            {$bump_ordering}    
            date_released DESC
            limit $start, $limit
        ";

       // echo $sql;
       
        $Result = [];
        $Data = \Core\Utils\Data::findBySQL(self::DEFAULT_DB_CONFIG,
            $sql,
            FALSE,
            true,
            array('enable' => false)
        );
       
        $recordset = [];
        if(isset($Data['recordset'])){
               
            foreach($Data['recordset'] as $record){
                $recordset[] = $record['id'];
            }
            $Data['recordset'] = $recordset;
        }    
        
        if(!isset($Data['recordset'])){
            return ['recordset' => $Data, "count" => false];
        }
 
        
        return $Data;
    }    
      
    public static function getListOfActiveAdsByVerified( $page_number = 1, $page_size = self::LIST_SIZE_DEFAULT_PC){
        
        $where = "";
        $vipField = ' 0 as vip';
        $vipOrdering = $premiumOrdering = '';
        $premiumField = ' 0 as premium';
		$bump_ordering = '';

		if (!empty($city_name) || !empty($category_name)){
			$bump_ordering = 'date_bumped DESC,';
		}

        if (!empty(self::$VipAdIds)){
            $vipIds = implode(',',array_reverse(self::$VipAdIds));
            $vipField = " IF(FIND_IN_SET(id,'{$vipIds}'), 1, 0) as vip ";
            $where .= " OR id IN({$vipIds}) ";
            $vipOrdering = " FIELD(id,{$vipIds}) DESC, ";
        }

        if (!empty(self::$PremiumAdIds)){
            $premiumIds = implode(',',array_reverse(self::$PremiumAdIds));
            $premiumField = " IF(FIND_IN_SET(id,'{$premiumIds}'), 1, 0) as premium ";
            $where .= " OR id IN({$premiumIds}) ";
            $premiumOrdering = " FIELD(id,{$premiumIds}) DESC, ";
        }

        $paginate = ($page_size * ($page_number - 1)) . "," . $page_size;
        $sql =  "
        SELECT
          id,
          images,
          date_released,
          category,
          city,
          title,
          description,
          date_bumped,
          `verification_status`,
          {$vipField},
          {$premiumField}
        FROM
          ads
        WHERE
          `status` = 'active'
          AND city <> ''
          AND `verification_status` = 'verified'
		  
          
          {$where}
        ORDER BY
            {$vipOrdering}
            {$premiumOrdering}
			{$bump_ordering}	
            date_released DESC
        LIMIT {$paginate}";

        //die($sql);/* CACHING ISSUE AND date_released <= " . \Core\Utils\Time::getCommonTimeStamp() . "*/
        $Result = [];
        $Data = \Core\Utils\Data::findBySQL(self::DEFAULT_DB_CONFIG,
            $sql,
            FALSE,
            TRUE,
			array('enable' => false)
		);
		
        foreach ($Data['recordset'] as $ad){
            if ($ad['vip'] == 1){
                $Result['vip_ads'][] = $ad;
            }elseif ($ad['premium'] == 1){
                $Result['premium_ads'][] = $ad;
            }else{
                $Result['recordset'][] = $ad;
            }
        }
        $Result['count']=$Data['count'];
        return $Result;
    }
       
     public static function getListOfActiveAdsByRadius($cities, $page_number = 1, $page_size = self::LIST_SIZE_DEFAULT_PC)
    {
        $where = "";
        $vipField = ' 0 as vip';
        $vipOrdering = $premiumOrdering = '';
        $premiumField = ' 0 as premium';
        $bump_ordering = '';

        if (!empty($city_name) || !empty($category_name)){
            $bump_ordering = 'date_bumped DESC,';
        }

        $cities = "'" . implode("','", $cities) ."'";
            $where .= " AND city in  ($cities) ";
        

        if (!empty($city_name)){
            $CityName = \Modules\Humpchies\CityModel::parseCityWithAdsCode($city_name, $category_name);
            $where .= " AND city = '$CityName' ";
        }

        if (!empty(self::$VipAdIds)){
            $vipIds = implode(',',array_reverse(self::$VipAdIds));
            $vipField = " IF(FIND_IN_SET(id,'{$vipIds}'), 1, 0) as vip ";
            $where .= " OR id IN({$vipIds}) ";
            $vipOrdering = " FIELD(id,{$vipIds}) DESC, ";
        }

        if (!empty(self::$PremiumAdIds)){
            $premiumIds = implode(',',array_reverse(self::$PremiumAdIds));
            $premiumField = " IF(FIND_IN_SET(id,'{$premiumIds}'), 1, 0) as premium ";
            $where .= " OR id IN({$premiumIds}) ";
            $premiumOrdering = " FIELD(id,{$premiumIds}) DESC, ";
        }

        $paginate = ($page_size * ($page_number - 1)) . "," . $page_size;
        $sql =  "
        SELECT
          id,
          images,
          date_released,
          category,
          city,
          title,
          description,
          {$vipField},
          {$premiumField}
        FROM
          ads
        WHERE
          `status` = 'active'
          AND city <> ''
          AND date_released <= " . \Core\Utils\Time::getCommonTimeStamp() . "
          {$where}
        ORDER BY
            {$vipOrdering}
            {$premiumOrdering}
            {$bump_ordering}    
            date_released DESC
        LIMIT {$paginate}";
       

        $Result = [];
        $Data = \Core\Utils\Data::findBySQL(self::DEFAULT_DB_CONFIG,
            $sql,
            FALSE,
            TRUE,
            array('enable' => false)
        );
       
        
        foreach ($Data['recordset'] as $ad){
            if ($ad['vip'] == 1){
                $Result['vip_ads'][] = $ad;
            }elseif ($ad['premium'] == 1){
                $Result['premium_ads'][] = $ad;
            }else{
                $Result['ads']['recordset'][] = $ad;
            }
        }
        $Result['ads']['count']=$Data['count'];
        return $Result;
    }

    public static function getVipAdsByCategoryAndZone($zone = NULL, $category_name = NULL)
    {
        $where = empty($zone) ? '' : ' AND op.zone = \'' . $zone . '\' ';

        if($zone == \Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO){
			if(empty($category_name)){
				$where .= " AND ( op.category = 'main-plus' OR op.category = 'main' )";
			}
			else{
				$where .= " AND ( ( op.category = 'main-plus' AND a.category = '{$category_name}' )  OR op.category = '{$category_name}')";
			}
		}
       // @file_put_contents('/home/shadybyte/web/viper/logs/log2.txt', 'SELECT
//                                                    a.id,
//                                                    a.images,
//                                                    a.date_released,
//                                                    a.category,
//                                                    a.city,
//                                                    a.title,
//                                                    a.description
//                                            FROM ads a
//                                            INNER JOIN order_packages op ON op.ads_id = a.id 
//                                            INNER JOIN packages p ON p.id = op.package_id AND p.is_premium = 0
//                                            WHERE
//                                                a.status = \'active\' AND op.status = 2
//                                                AND a.city <> \'\'
//                                                '. $where . '
//                                            ORDER BY
//                                                p.fixed_position ASC');
        //return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,

        return \Core\DBConn::select(self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    a.id,
                                                    a.images,
                                                    a.date_released,
                                                    a.category,
                                                    a.city,
                                                    a.title,
                                                    a.description
                                            FROM ads a
											INNER JOIN order_packages op ON op.ads_id = a.id 
											INNER JOIN packages p ON p.id = op.package_id AND p.is_premium = 0
                                            WHERE
												a.status = \'active\' AND op.status = 2
												AND a.city <> \'\'
												'. $where . '
                                            ORDER BY
												p.fixed_position ASC',
            FALSE
        );
    }
       
    public static function getVipAdsByCategoryAndRegion($region_id = NULL, $category_name = NULL) {
        
        $where = empty($region_id) ? '' : ' AND a.region_id = ' . $region_id;
        
        if(empty($category_name)){
            $where .= " AND ( op.category = 'main-plus' OR op.category = 'main' )";
        }
        else{
            $where .= " AND ( ( op.category = 'main-plus' AND a.category = '{$category_name}' )  OR op.category = '{$category_name}')";
        }
        
        //var_dump( 'SELECT
//                                                    a.id,
//                                                    a.images,
//                                                    a.date_released,
//                                                    a.category,
//                                                    a.city,
//                                                    a.title,
//                                                    a.description
//                                            FROM ads a
//                                            INNER JOIN order_packages op ON op.ads_id = a.id 
//                                            INNER JOIN packages p ON p.id = op.package_id AND p.is_premium = 0
//                                            WHERE
//                                                a.status = \'active\' AND op.status = 2
//                                                AND a.city <> \'\'
//                                                '. $where . '
//                                            ORDER BY
//                                                p.fixed_position ASC'); exit();
        //return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,

        return \Core\DBConn::select(self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    a.id,
                                                    a.images,
                                                    a.date_released,
                                                    a.category,
                                                    a.city,
                                                    a.title,
                                                    a.description
                                            FROM ads a
                                            INNER JOIN order_packages op ON op.ads_id = a.id 
                                            INNER JOIN packages p ON p.id = op.package_id AND p.is_premium = 0
                                            WHERE
                                                a.status = \'active\' AND op.status = 2
                                                AND a.city <> \'\'
                                                '. $where . '
                                            ORDER BY
                                                p.fixed_position ASC',
            FALSE
        );
    }
    
    /**
     * @param null $zone
     * @param null $category_name
     * @return array
     */
    public static function getVipAdsByCategoryAndZoneIds($zone = NULL, $category_name = NULL) {
        $VipAds = self::getVipAdsByCategoryAndZone($zone, $category_name);
        $ids = [];
        if(!empty($VipAds)){
            foreach ($VipAds as $vipAd){
                array_push($ids, $vipAd['id']);
            }
        }
        self::$VipAdIds = $ids;
        return $ids;
    }
    
    public static function getVipAdsByCategoryAndRegionIds($region_id = NULL, $category_name = NULL) {
        $VipAds = self::getVipAdsByCategoryAndRegion($region_id, $category_name);
        $ids = [];
        if(!empty($VipAds)){
            foreach ($VipAds as $vipAd){
                array_push($ids, $vipAd['id']);
            }
        }
        self::$VipAdIds = $ids;
        return $ids;
    }    

    public static function getPremiumAdsByCategoryAndZone($zone = NULL, $category_name = NULL)
    {

        $where = empty($zone) ? '' : ' AND op.zone = \'' . $zone . '\' ';

        if($zone == \Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO){
            if(empty($category_name)){
				$where .= " AND ( op.category = 'main-plus' OR op.category = 'main' )";
			}
			else{
				$where .= " AND ( ( op.category = 'main-plus' && a.category = '{$category_name}' )  OR op.category = '{$category_name}')";
			}
        }

        return \Core\DBConn::select(self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    a.id,
                                                    a.images,
                                                    a.date_released,
                                                    a.category,
                                                    a.city,
                                                    a.title,
                                                    a.description
                                            FROM ads a
                                            INNER JOIN order_packages op ON op.ads_id = a.id 
                                            INNER JOIN packages p ON p.id = op.package_id AND p.is_premium = 1
                                            WHERE
                                                a.status = \'active\' AND op.status = 2
                                                AND a.city <> \'\'
                                                '. $where .
												'ORDER BY date_bumped DESC, RAND()',
            FALSE
        );
    }
    
    public static function getPremiumAdsByCity( $city = NULL)
    {
        $zone = \Modules\Humpchies\Utils::getDefinedZoneForCity($city);
        $category_name = null;
        $where = empty($city) ? '' : ' AND op.zone = \'' . $city . '\' ';
        //$where = empty($city) ? '' : ' AND a.city = \'' . $city . '\' ';
        if($zone)
            $where = empty($zone) ? '' : ' AND op.zone = \'' . $zone . '\' ';

        if($zone == \Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO){
            if(empty($category_name)){
                $where .= " AND ( op.category = 'main-plus' OR op.category = 'main' )";
            }
            else{
                $where .= " AND ( ( op.category = 'main-plus' && a.category = '{$category_name}' )  OR op.category = '{$category_name}')";
            }
        }
        
        
        /*
        'SELECT  a.id, a.images, a.date_released, a.category, a.city,  a.title, a.description 
             FROM ads a
             INNER JOIN order_packages op ON op.ads_id = a.id 
             INNER JOIN packages p ON p.id = op.package_id AND p.is_premium = 1
             WHERE
                a.status = \'active\' AND op.status = 2
                AND a.city <> \'\'
        
        
        */
       
        return  \Core\DBConn::select(self::DEFAULT_DB_CONFIG,
        'SELECT 
            a.id,
            GROUP_CONCAT(op.status) as p_statuses,
            GROUP_CONCAT(op.date_activated) as activation_dates,
            GROUP_CONCAT(op.expiration_date) as expiration_dates,
            a.images,
            a.date_released,
            a.category,
            a.city,
            title,
            description,
            a.status,
            a.user_id,
            a.fake_status,
            op.id as p_id,
            op.status as package_status,
            a.verification_status,
            a.is_updated                         
        FROM ads a    
        LEFT JOIN order_packages op
                ON a.id = op.ads_id AND op.status in (1,2)        
        WHERE
                a.status = \'active\'
                AND a.city <> \'\' AND op.status = 2   
                '. $where  ."
        GROUP BY a.id        
        ",
            FALSE
        );
    }

    /**
     * @param null $zone
     * @param null $category_name
     * @return array
     */
    public static function getPremiumAdsByCategoryAndZoneIds($zone = NULL, $category_name = NULL) {

        $PremiumAds = self::getPremiumAdsByCategoryAndZone($zone, $category_name);

        $ids = [];
        if(!empty($PremiumAds)){
            foreach ($PremiumAds as $premiumAd){
                array_push($ids, $premiumAd['id']);
            }
        }
        self::$PremiumAdIds = $ids;
        return $ids;
    }

    public static function getPremiumAdsByCategoryAndCity($city = NULL, $category_name = NULL)
    {
        $supportedCities = CityModel::getCitiesWithAds($category_name);
        if (!empty($city)) {
            $city = $supportedCities[$city];
        }
        $city = !empty($city) ? addslashes($city) : null;
        $where = empty($city) ? '' : ' AND op.city = \'' . $city . '\' ';

		if(empty($category_name)){
			$where .= " AND ( op.category = 'main-plus' OR op.category = 'main' )";
		}
		else{
			$where .= " AND ( ( op.category = 'main-plus' && a.category = '{$category_name}' )  OR op.category = '{$category_name}')";
		}

        return \Core\DBConn::select(self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    a.id,
                                                    a.images,
													a.date_bumped,
                                                    a.date_released,
                                                    a.category,
                                                    a.city,
                                                    a.title,
                                                    a.description
                                            FROM ads a
                                            INNER JOIN order_packages op ON op.ads_id = a.id 
                                            INNER JOIN packages p ON p.id = op.package_id AND p.is_premium = 1
                                            WHERE
                                                a.status = \'active\' AND op.status = 2
                                                AND a.city <> \'\'
                                                '. $where .
												' ORDER BY 
													a.date_bumped DESC,	
													RAND()',
            FALSE
        );
    }
   
    public static function getPremiumAdsByCategoryAndRegion($region_id = NULL, $category_name = NULL)
    {
        //$supportedCities = CityModel::getCitiesWithAds($category_name);
//        if (!empty($city)) {
//            $city = $supportedCities[$city];
//        }
//        $city = !empty($city) ? addslashes($city) : null;
        
        if($region_id == 1) // montreal metro area
            $where =  ' AND op.zone = \'' . \Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO . '\' ';
        else
            $where = empty($region_id) ? '' : ' AND a.region_id = ' . $region_id;

		
        if(empty($category_name)){
			$where .= " AND ( op.category = 'main-plus' OR op.category = 'main' )";
		}
		else{
			$where .= " AND ( ( op.category = 'main-plus' && a.category = '{$category_name}' )  OR op.category = '{$category_name}')";
		}

        return \Core\DBConn::select(self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    a.id,
                                                    a.images,
													a.date_bumped,
                                                    a.date_released,
                                                    a.category,
                                                    a.city,
                                                    a.title,
                                                    a.description
                                            FROM ads a
                                            INNER JOIN order_packages op ON op.ads_id = a.id 
                                            INNER JOIN packages p ON p.id = op.package_id AND p.is_premium = 1
                                            WHERE
                                                a.status = \'active\' AND op.status = 2
                                                AND a.city <> \'\'
                                                '. $where .
												' ORDER BY 
													a.date_bumped DESC',
            FALSE
        );
    }

    /**
     * @param null $city
     * @param null $category_name
     * @return array
     */
    public static function getPremiumAdsByCategoryAndCityIds($city = null, $category_name = null) {
        $PremiumAds = self::getPremiumAdsByCategoryAndCity($city, $category_name);
        $ids = [];
        if(!empty($PremiumAds)){
            foreach ($PremiumAds as $premiumAd){
                array_push($ids, $premiumAd['id']);
            }
        }
        self::$PremiumAdIds = $ids;
        return $ids;
    }
    
    public static function getPremiumAdsByCategoryAndRegionId($region_id = null, $category_name = null) {
        $PremiumAds = self::getPremiumAdsByCategoryAndRegion($region_id, $category_name);
        $ids = [];
        if(!empty($PremiumAds)){
            foreach ($PremiumAds as $premiumAd){
                array_push($ids, $premiumAd['id']);
            }
        }
        
        //\Core\library\log::debug($ids, "PREMIUM IDS");
        
        self::$PremiumAdIds = $ids;
        return $ids;
    }
    
    
    public static function get4RandomAdsByCity( $city = NULL)
    {
        $zone = \Modules\Humpchies\Utils::getDefinedZoneForCity($city);
        $category_name = null;
        $where = empty($city) ? '' : ' AND op.zone = \'' . $city . '\' ';
        $where = empty($city) ? '' : ' AND a.city = \'' . $city . '\' ';
       
       // if($zone == \Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO){
//            if(empty($category_name)){
//                $where .= " AND ( op.category = 'main-plus' OR op.category = 'main' )";
//            }
//            else{
//                $where .= " AND ( ( op.category = 'main-plus' && a.category = '{$category_name}' )  OR op.category = '{$category_name}')";
//            }
//        }
      
        return  \Core\DBConn::select(self::DEFAULT_DB_CONFIG,
        'SELECT 
            a.id,
            GROUP_CONCAT(op.status) as p_statuses,
            GROUP_CONCAT(op.date_activated) as activation_dates,
            GROUP_CONCAT(op.expiration_date) as expiration_dates,
            a.images,
            a.date_released,
            a.category,
            a.city,
            title,
            description,
            a.status,
            a.user_id,
            a.fake_status,
            op.id as p_id,
            op.status as package_status,
            a.verification_status,
            a.is_updated                         
        FROM ads a    
        LEFT JOIN order_packages op
                ON a.id = op.ads_id AND op.status in (1,2)        
        WHERE
                a.status = \'active\'
                AND a.city <> \'\'   
                '. $where  ."
        GROUP BY a.id ORDER BY RAND() LIMIT 4       
        ",
            FALSE
        );
    }
        
    /***
     * put your comment there...
     *
     */
    public static function getListOfCategoriesAndCitiesWithAds()
    {
        return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    category,
                                                    city
                                            FROM
                                                    ads
                                            WHERE
                                                    status = \'active\'
                                                    AND
                                                    city <> \'\'
                                            GROUP BY
                                                    category, 
                                                    city',
            FALSE,
            FALSE);
    }

    /***
     * put your comment there...
     *
     * @param mixed $page_size
     * @param mixed $fields
     * @return []
     */
    public static function getListOfLatestAds($page_size = self::DEFAULT_LIST_SIZE, $fields = array('id','images','date_released','category','city','title','description','verification_status'))
    {
        return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    ' . implode(',', $fields) .'
                                            FROM
                                                    ads
                                            WHERE
                                                    status = \'active\'
                                                    AND
                                                    city <> \'\'
                                                    AND
                                                    date_released <= ' . \Core\Utils\Time::getCommonTimeStamp() . '
                                            ORDER BY
                                                    date_released DESC
                                            LIMIT 0,' . $page_size,
            FALSE,
            FALSE);
    }

    /***
     * put your comment there...
     *
     * @param mixed $ad_id
     * @return []
     */
    public static function getDetailsOfActiveAd($ad_id)
    {
        return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    a.id,
                                                    a.type,
                                                    a.location,
                                                    a.price,
                                                    a.price_conditions,
                                                    a.title,
                                                    a.description,
                                                    a.date_released,
                                                    a.city,
                                                    a.phone,
                                                    a.category,
                                                    a.fake_status,
                                                    a.images,
                                                    p.id as p_id,
                                                    op.package_status,
                                                    a.verification_status,
                                                    a.phone_validated,
                                                    a.myfans_url,
                                                    DATE_FORMAT(a.validate_date, "%m/%d/%Y") as validate_date,
                                                    user_id
                                            FROM ads a
                                            LEFT JOIN( SELECT status AS package_status, ads_id, package_id, expiration_date
                                                    FROM order_packages op
                                                    WHERE status = 2 ) AS op ON op.ads_id = a.id
                                            LEFT JOIN packages p ON p.id = op.package_id
                                            WHERE 
                                                    a.id = ' . $ad_id . '
                                                    AND 
                                                    a.status = \'active\'',
            FALSE,
            FALSE,
            array('expire' => 60));
    }
       
    public static function getCityCategOfAd($ad_id)
    {
        return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT  a.city, a.category FROM ads a  WHERE a.id = ' . $ad_id . ' ',
            FALSE,
            FALSE,
            array('expire' => 60));
    }
    
    public static function getDetailsOfSimpleAd($ad_id)
    {
        return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    a.id,
                                                    a.type,
                                                    a.location,
                                                    a.price,
                                                    a.price_min,
                                                    a.price_max,
                                                    a.price_conditions,
                                                    a.title,
                                                    a.description,
                                                    a.date_released,
                                                    a.city,
                                                    a.phone,
                                                    a.category,
                                                    a.fake_status,
                                                    a.images,
                                                    p.id as p_id,
                                                    op.package_status,
                                                    a.verification_status,
                                                    a.phone_validated,
                                                    a.myfans_url,
                                                    a.status,
                                                    a.attached_video,
                                                    a.blocked_comments,
                                                    a.has_comments,
                                                    DATE_FORMAT(a.validate_date, "%m/%d/%Y") as validate_date,
                                                    user_id,
                                                    p.period
                                            FROM ads a
                                            LEFT JOIN( SELECT status AS package_status, ads_id, package_id, expiration_date
                                                    FROM order_packages op
                                                    WHERE status = 2 ) AS op ON op.ads_id = a.id
                                            LEFT JOIN packages p ON p.id = op.package_id
                                            WHERE 
                                                    a.id = ' . $ad_id,
            FALSE,
            FALSE,
            array('expire' => 60));
    }
        
    public static function getDetailsOfSimpleAdNewAD($ad_id)
    {
        return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    a.id,
                                                    a.type,
                                                    a.location,
                                                    a.price,
                                                    a.price_min,
                                                    a.price_max,
                                                    a.price_conditions,
                                                    a.title,
                                                    a.description,
                                                    a.date_released,
                                                    a.city,
                                                    a.phone,
                                                    a.category,
                                                    a.fake_status,
                                                    a.images,
                                                    p.id as p_id,
                                                    op.package_status,
                                                    a.verification_status,
                                                    a.phone_validated,
                                                    a.myfans_url,
                                                    a.status,
                                                    a.attached_video,
                                                    a.blocked_comments,
                                                    a.has_comments,
                                                    DATE_FORMAT(a.validate_date, "%m/%d/%Y") as validate_date,
                                                    user_id,
                                                    p.period
                                            FROM ads a
                                            LEFT JOIN( SELECT status AS package_status, ads_id, package_id, expiration_date
                                                    FROM order_packages op
                                                    WHERE status = 1 ) AS op ON op.ads_id = a.id
                                            LEFT JOIN packages p ON p.id = op.package_id
                                            WHERE 
                                                    a.id = ' . $ad_id,
            FALSE,
            FALSE,
            array('expire' => 60));
    }
    public static function getDetails4Link($ad_id)
    {
        return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    a.id,
                                                    a.title,
                                                    a.city,
                                                    a.category
                                            FROM ads a
                                            WHERE 
                                                    a.id = ' . $ad_id,
            FALSE,
            FALSE,
            array('expire' => 60));
    }
      
    /***
     * put your comment there...
     *
     * @param mixed $ad_id
     * @return []
     */
    public static function getDetailsOfAd($ad_id)
    {
        return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    a.id,
                                                    type,
                                                    location,
                                                    price,
                                                    price_conditions,
                                                    title,
                                                    description,
                                                    date_released,
                                                    city,
                                                    phone,
                                                    category,
                                                    images,
                                                    c.id as city_id,
                                                    c.zone_id,
                                                    z.name as zone_name,
                                                    verification_status,
                                                    a.status
                                            FROM 
                                                    ads a
                                            INNER JOIN cities c on c.`name` = `a`.`city`
                                            INNER JOIN zones z on z.id = c.zone_id
                                            WHERE 
                                                    a.id = ' . addslashes($ad_id),  FALSE,     FALSE,  array('expire' => 60));
    }

    /***
     * put your comment there...
     *
     * @param mixed $user_id
     * @param mixed $page_number
     * @param mixed $page_size
     * @return []
     */
    public static function getListOfAdsByUser($user_id, $page_number = 1, $page_size = self::DEFAULT_LIST_SIZE_PC, $status = '\'active\',\'paused\', \'new\',', $filter=array()) // \'inactive\',
    {
        $where = '';
        if(isset($filter['myads']) && $filter['myads'] == 'yes'){
            $own = " and 1 ";
        }else{
            $own = " and 0 ";
        }
        if(isset($filter['myads'])){
            unset($filter['myads']);   
        }
        if (!empty($filter)){
            $where = ' AND '.implode(' AND ',$filter);
        }
       
        // LEFT JOIN ads_revision AR ON a.id = AR.ads_id AND AR.`status` =\'active\' and AR.`approved_status`= 0  
        $result = \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT
                        a.id,
                        GROUP_CONCAT(op.status) as p_statuses,
                        GROUP_CONCAT(op.date_activated) as activation_dates,
                        GROUP_CONCAT(op.expiration_date) as expiration_dates,
                        a.images,
                        a.date_released,
                        if(a.is_updated = \'1\' ' . $own . ', a.category_new, a.category) AS category,
                        if(a.is_updated = \'1\' ' . $own . ', a.city_new, a.city) AS city,
                        if(a.is_updated = \'1\' ' . $own . ', a.title_new, a.title) as title,
                        if(a.is_updated = \'1\' ' . $own . ', a.description_new, a.description) as description,
                        a.status,
                        a.user_id,
                        a.fake_status,
                         a.no_visits,
                        op.id as p_id,
                        op.status as package_status,
                        a.verification_status,
                        a.is_updated,
                        op.package_id                                            
                        FROM
                        ' . self::DEFAULT_DB_TABLE . ' a
                LEFT JOIN order_packages op
                ON a.id = op.ads_id AND op.status in (1,2)
                WHERE
                        a.user_id = ' .  $user_id . '
                        AND
                        a.status IN('.$status.')
                        '.$where.'
                GROUP BY a.id
                ORDER BY
                        a.date_released DESC,
                        a.date_added DESC
                LIMIT ' . ($page_size * ($page_number - 1)) . ',' . $page_size,
            FALSE,
            TRUE,
            array('enable' => FALSE));  
        
        return $result;
    }

    /***
     * put your comment there...
     *
     * @param mixed $keywords
     * @param mixed $page_number
     * @param mixed $page_size
     * @return []
     */
    public static function search(&$keywords, $page_number = 1, $page_size = self::DEFAULT_LIST_SIZE_PC, $city = null)
    {
        if(in_array($keywords, array_keys(self::$searchKeywords))){
           $keywordMatchSearch = [];
           
           foreach(self::$searchKeywords[$keywords] as $keyword){
               $keywordMatchSearch[] = "'+" . $keyword . "'";
           }
           $keywordMatchSearch = implode("", $keywordMatchSearch) . " IN BOOLEAN MODE";
        }else{
            $keywordMatchSearch = "'" . $keywords . "'";
        }
        
        // check city here                                    
        $where = '';
        if(!empty($city)) {
            $city_details = \Modules\Humpchies\CityModel::getCityDetails($city); 
            $where .= " AND city_id = " . $city_details["id"];
            //var_dump($city_details);exit("city nt fucking empty lol");
                                            
        }
        $results =  \Core\Utils\Data::findBySQL(	self::DEFAULT_DB_CONFIG,
            'SELECT
                                            	    id,
                                            	    images,
                                            	    phone,
													date_bumped,
                                            	    date_released,
                                            	    category,
                                            	    city,
                                            	    title,
                                            	    description,
                                                    verification_status,
                                                    
                                                    ROUND(MATCH(title, description) AGAINST(' . $keywordMatchSearch . '), 5) as relevancy
                                    	    FROM
                                            	    ' . self::DEFAULT_DB_TABLE . '
                                    	    WHERE
                                            	    MATCH(title,description) AGAINST(' . $keywordMatchSearch . ')
                                            	    AND
                                            	    status = \'active\'
                                            	    AND
                                                    date_released > 0
                                                    AND 
                                                    date_released <= ' . \Core\Utils\Time::getCommonTimeStamp() 
                                                    . $where . '
                                    	    ORDER BY
                                                    MATCH(title, description) AGAINST(' . $keywordMatchSearch . ') desc,
													date_bumped DESC,
                                                    date_released DESC
                                    	    LIMIT ' . ($page_size * ($page_number - 1)) . ',' . $page_size,
            FALSE,
            TRUE);
            
        if($page_number == 1){
            $varsa['keyword'] = $keywords; 
            $varsa['first_searched'] = date("Y-m-d H:i:s"); 
            $varsa['results'] = $results['count'];
            $varsa['total_searches'] = 1;
            if(\Core\Utils\Device::isPC()== 'pc'){
               $varsa['desktop_searches'] = 1; 
               $varsa['mobile_searches'] = 0;
               $update =  ", `desktop_searches` = `desktop_searches` + 1";
            }else{
               $varsa['desktop_searches'] = 0; 
               $varsa['mobile_searches'] = 1;
               $update =  ", `mobile_searches` = `mobile_searches` + 1"; 
            }
            $onDuplicate = " `total_searches` = `total_searches` + 1, `results` = " .$results['count'] . $update ;
            \Core\DBConn::insertIgnore(self::DEFAULT_DB_CONFIG,"searches", $varsa, $onDuplicate );  
        }
        return $results;    
            
    }
    
    public static function searchPhone($phone_number, $page_number = 1, $page_size = self::DEFAULT_LIST_SIZE_PC, $city = null) {
    
        // check city here                                    
        $where = '';
        if(!empty($city)) {
            $city_details = \Modules\Humpchies\CityModel::getCityDetails($city); 
            $where .= " AND city_id = " . $city_details["id"];
            //var_dump($city_details);exit("city nt fucking empty lol");
            
        }
        
        
        $results =  \Core\Utils\Data::findBySQL(    self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    id,
                                                    images,
                                                    phone,
                                                    date_bumped,
                                                    date_released,
                                                    category,
                                                    city,
                                                    title,
                                                    description,
                                                    verification_status
                                            FROM
                                                    ' . self::DEFAULT_DB_TABLE . '
                                            WHERE
                                                    `phone` = ' . $phone_number . '
                                                    AND
                                                    status = \'active\'
                                                    AND
                                                    date_released > 0
                                                    AND 
                                                    date_released <= ' . \Core\Utils\Time::getCommonTimeStamp() 
                                                    . $where . '
                                            ORDER BY
                                                    date_bumped DESC,
                                                    date_released DESC
                                            LIMIT ' . ($page_size * ($page_number - 1)) . ',' . $page_size,
            FALSE,
            TRUE);
            
        if($page_number == 1){
            $varsa['keyword'] = $phone_number; 
            $varsa['first_searched'] = date("Y-m-d H:i:s"); 
            $varsa['results'] = $results['count'];
            $varsa['total_searches'] = 1;
            if(\Core\Utils\Device::isPC()== 'pc'){
               $varsa['desktop_searches'] = 1; 
               $varsa['mobile_searches'] = 0;
               $update =  ", `desktop_searches` = `desktop_searches` + 1";
            }else{
               $varsa['desktop_searches'] = 0; 
               $varsa['mobile_searches'] = 1;
               $update =  ", `mobile_searches` = `mobile_searches` + 1"; 
            }
            $onDuplicate = " `total_searches` = `total_searches` + 1, `results` = " .$results['count'] . $update ;
            \Core\DBConn::insertIgnore(self::DEFAULT_DB_CONFIG,"searches", $varsa, $onDuplicate );  
        }
        return $results;    
            
    }
    
    public static function checkDuplicateDescription($description, $id = 0){
        
        $values =['id' => $id, "description" => $description]; 
        $duplicate = \Core\DBConn::selectRowClean( self::DEFAULT_DB_CONFIG,
        'SELECT description
        FROM ads
        WHERE id != :id AND description = \':description\'',
        $values,
        FALSE);
        return  !!($duplicate['description']);
    }

    /***
     * put your comment there...
     *
     * @param mixed $title
     */
    public static function isTitleInputValid(&$title)
    {
        switch(($title_validity = \Core\Utils\Security::isStringInputValid($title, self::MAX_LEN_TITLE, self::MIN_LEN_TITLE)))
        {
            case \Core\Utils\Security::NO_ERROR:
                return self::NO_ERROR;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_EMPTY:
                return self::ERROR_TITLE_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_SHORT:
                return self::ERROR_TITLE_IS_TOO_SHORT;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_LONG:
                return self::ERROR_TITLE_IS_TOO_SHORT;
                break;
            default:
                return $title_validity;
                break;
        }
    }

    /***
     * put your comment there...
     *
     * @param mixed $description
     */
    public static function isDescriptionInputValid(&$description, $id = 0 )
    {
        
        if( \Modules\Humpchies\AdModel::checkDuplicateDescription($description, $id)){
            return self::ERROR_DESCRIPTION_IS_DUPLICATED;    
        }
        //$description = addslashes($description); 
        switch(($description_validity = \Core\Utils\Security::isStringInputValid($description, self::MAX_LEN_DESCRIPTION, self::MIN_LEN_DESCRIPTION)))
        {
            case \Core\Utils\Security::NO_ERROR:
                return self::NO_ERROR;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_EMPTY:
                return self::ERROR_DESCRIPTION_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_SHORT:
                return self::ERROR_DESCRIPTION_IS_TOO_SHORT;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_LONG:
                return self::ERROR_DESCRIPTION_IS_TOO_LONG;
                break;
            default:
                return $description_validity;
                break;
        }
    
    }

    /***
     * put your comment there...
     *
     * @param mixed $donation
     */
    public static function isPriceInputValid(&$price)
    {
        $price_validity = \Core\Utils\Security::isIntegerInputValid($price, self::MIN_PRICE, self::MAX_PRICE);

        switch($price_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
            case \Core\Utils\Security::ERROR_INTEGER_IS_EMPTY:
                return self::NO_ERROR;
                break;
            case \Core\Utils\Security::ERROR_INTEGER_IS_INVALID:
                return self::ERROR_PRICE_IS_INVALID;
                break;
            default:
                return $price_validity;
        }
    }

    /***
     * put your comment there...
     *
     * @param mixed $donation_details
     */
    public static function isPriceConditionsInputValid(&$price_conditions)
    {
        switch(($price_conditions_validity = \Core\Utils\Security::isStringInputValid($price_conditions, self::MAX_LEN_PRICE_CONDITIONS, self::MIN_LEN_PRICE_CONDITIONS)))
        {
            case \Core\Utils\Security::NO_ERROR:
            case \Core\Utils\Security::ERROR_STRING_IS_EMPTY:
                return self::NO_ERROR;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_LONG:
                return self::ERROR_PRICE_CONDITIONS_IS_TOO_LONG;
                break;
            default:
                return $price_conditions_validity;
                break;
        }
    }

    /**
     * put your comment there...
     *
     * @param a $search_query
     */
    public static function isSearchQueryInputValid(&$search_query)
    {
        $search_query = \Core\Utils\Security::htmlToText( $search_query);

        if(empty($search_query))
            return self::ERROR_SEARCH_QUERY_IS_INVALID;

        switch(($search_query_validity = \Core\Utils\Security::isStringInputValid($search_query, self::MAX_LEN_SEARCH_QUERY, self::MIN_LEN_SEARCH_QUERY)))
        {
            case \Core\Utils\Security::NO_ERROR:
                return self::NO_ERROR;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_EMPTY:
                return self::ERROR_SEARCH_QUERY_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_SHORT:
                return self::ERROR_SEARCH_QUERY_IS_TOO_SHORT;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_LONG:
                return self::ERROR_SEARCH_QUERY_IS_TOO_LONG;
                break;
            default:
                return $search_query_validity;
                break;
        }
    }

    /**
     * put your comment there...
     *
     * @param mixed $ids
     * @return []
     */
    public static function getVIPAds($ids)
    {
        return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    id,
                                                    images,
                                                    date_released,
                                                    category,
                                                    city,
                                                    title,
                                                    description,
                                                    status,
                                                    verification_status
                                            FROM
                                                    ' . self::DEFAULT_DB_TABLE . '
                                            WHERE
                                                    id IN(' . implode(',', $ids) . ')
                                                    AND
                                                    status = \'active\'
                                                    AND
                                                    city <> \'\'
                                                    AND
                                                    date_released <= ' . \Core\Utils\Time::getCommonTimeStamp() .'
                                            ORDER BY
                                                    FIELD(id, ' . implode(',', $ids) . ')',
            FALSE,
            FALSE);
    }

    /**
     * put your comment there...
     *
     * @param mixed $id
     * @return []
     */
    public static function getAdDetailsForDeletion(&$id)
    {
        return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    id,
                                                    city,
                                                    category,
                                                    title
                                            FROM 
                                                    ads 
                                            WHERE 
                                                    id = ' . $id,
            TRUE,
            FALSE,
            array('enable' => FALSE));
    }

    public static function getAdDetailsForEdition(&$ad_id, $user_id)
    {       
        return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT
                                                    a.id,
                                                 a.type,
                                                 a.location,
                                                 a.price,
                                                 a.price_conditions,
                                                 a.title,
                                                 a.description,
                                                 a.date_released,
                                                 a.city,
                                                 a.city_id,
                                                 a.region_id,
                                                 a.phone,
                                                 a.category,
                                                 a.images,
                                                 a.price_min,
                                                 a.price_max,
                                                 a.status as ad_status,
                                                 a.myfans_url,
												 op.id as order_package_id,
                                                 op.status as package_status,
                                                 phone_country_iso,
                                                 phone_country_code,
                                                 attached_video,
                                                 date_bumped
                                                FROM
                                                    ads a
                                                LEFT JOIN order_packages op on op.ads_id = a.id AND op.`status` in (1,2)
                                                    WHERE a.id = ' . $ad_id .' AND a.user_id = ' . $user_id .' ',
            TRUE,
            FALSE,
            array('enable' => FALSE));
    }

    public static function bannByUserId($user_id)
    {
        if(\Core\Utils\Security::isIdInputValid($user_id) !== \Core\Utils\Security::NO_ERROR)
            return self::ERROR_USER_ID_IS_INVALID;

        return \Core\DBConn::update(    self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            array('status' => 'banned'),
            "user_id = $user_id");
    }

    public static function phoneHasAds($phone)
    {
        $recordset =  \Core\Utils\Data::findBySQL(  self::DEFAULT_DB_CONFIG,
            'SELECT
                                                            count(*) as \'count\'
                                                    FROM 
                                                            ads
                                                    WHERE 
                                                            phone = \'' . $phone . '\'
                                                            AND
                                                            status IN(\'active\', \'inactive\')',
            TRUE,
            FALSE,
            array('enable' => FALSE));

        return ((int)$recordset[0]['count'] > 0);
    }

    public function EscortActivitylog($action_slug, $params = null)
    {
        $user = 9999;

        $action_id = \Core\DBConn::selectRow(self::DEFAULT_DB_CONFIG, 'SELECT id FROM backend_activity_actions WHERE slug = "' . $action_slug . '"');

        if ( ! $action_id ) return false; // throw new Exception('Action does not exist in database');
        $data = array('backend_user_id' => $user, 'action_id' => $action_id['id']);

        if ( ! is_null($params) ) {
            $data['params'] = serialize($params);
        }

        return \Core\DBConn::insert(    self::DEFAULT_DB_CONFIG,
            'backend_activity_log',
            $data);
    }

    public static function canUpdateAd($userId, $adId) {
        $result = \Core\DBConn::selectRow(self::DEFAULT_DB_CONFIG, "
        select 1 from ads where id = {$adId} and user_id = {$userId}  
        ");
        return !empty($result);
    }

    public static function getUserId() {

    }
    
    public static function updateNoVisits($ads_ids) {
        if(empty($ads_ids))
            return;
            
        return \Core\DBConn::execute(self::DEFAULT_DB_CONFIG,
            "UPDATE ". self::DEFAULT_DB_TABLE ." SET `no_visits` = `no_visits` + 1 WHERE `id` IN (" . implode(',', $ads_ids) . ")" );
    }
    
    public static function updateNoImpressions($ad_id) {
        
        // add some check here maybe
        return \Core\DBConn::execute(self::DEFAULT_DB_CONFIG,
            "UPDATE ". self::DEFAULT_DB_TABLE ." SET `no_impressions` = `no_impressions` + 1 WHERE `id` = " . $ad_id);
    }       
    
    public static function getPremiumAds(){
        $redisKey = "PremiumAdsList";
        $premium = \Core\library\Redis::get($redisKey);
        //var_dump($premium);
        if($premium){
            return unserialize($premium);
        }
        
        $premium = \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT ads_id FROM order_packages WHERE `status` = 2',
            TRUE,
            FALSE,
            array('enable' => FALSE));
        
        $premium = array_column($premium,'ads_id');
        \Core\library\Redis::set($redisKey, serialize($premium));   
     
        //var_dump($premium);
           return $premium;    
        
    }

    /**
     * Get ad detail list.
     * 
     * @param int [] $adIdList The ad ID list.
     * @return array
     */
    public static function getDetailsByIdList($adIdList)
    {
        $result = \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT
                id,
                user_id,
                type,
                title,
                category,
                city,
                images               
            FROM ads
                WHERE id IN (' . implode(',', $adIdList) . ')'
        );
        return $result ? $result : array();
    }
      public static function getTotalAdsFromUser($userID)
    {
        $result = \Core\DBConn::select( self::DEFAULT_DB_CONFIG,
            'SELECT
                count(*) As cate              
            FROM ads
                WHERE `status` != \'delete\' AND user_id =  ' . intval($userID), true 
        );
        var_dump($result);
        return $result ? $result : array();
    }  
    
    public static function getTotalAdsFromUserforRedirect($userID)
    {
        $result = \Core\DBConn::select( self::DEFAULT_DB_CONFIG,
            'SELECT
                count(*) As cate              
            FROM ads
                WHERE `status` != \'delete\' AND `status` != \'inactive\' AND `status` != \'banned\' AND user_id =  ' . intval($userID), true 
        );
        
        return $result ? $result : array();
    }
    
    public static function phoneValidateLog(&$fields)
    {
        $adinfo = self::getAdInfo($fields['verification_id']);
        //var_dump($adinfo);
        if(!is_array($adinfo)){
            $adinfo = self::getRevisionAdInfo($fields['verification_id']);  
            //var_dump($adinfo);
            //echo "----";  
        }
        $fields['ad_id']    = $adinfo['ad_id'];
        $fields['user_id']  = $adinfo['user_id'];
       // var_dump($fields);
        $new_ad_id = \Core\DBConn::insert(  self::DEFAULT_DB_CONFIG,
            "phone_validation_callbacks",
            $fields);
        // die();
        if($new_ad_id !== FALSE)
            return $new_ad_id;

        $last_db_error = \Core\DBConn::getLastError(self::DEFAULT_DB_CONFIG, TRUE);
                                       
    }
    
    public static function adPhoneValidate( $fields, $validationID)
    {
        $fields['validate_date'] = date("Y-m-d"); 
        $affected_rows = \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $fields,
            "`validation_id` = '$validationID' ");

        if($affected_rows){
            return TRUE;
        }else{
        $affected_rows = \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
            "ads_revision",
            $fields,
            "`validation_id` = '$validationID' ");
    
        }

        $last_db_error = \Core\DBConn::getLastError(self::DEFAULT_DB_CONFIG, TRUE);

        if(empty($last_db_error))
            return TRUE;
    }
    
    public static function addUserVideo(&$fields)
    {
        $new_ad_id = \Core\DBConn::insert(  self::DEFAULT_DB_CONFIG,
            "user_videos",
            $fields);
        // die();
        if($new_ad_id !== FALSE)
            return $new_ad_id;
    }
    
    public static function getUserVideo($userid)
    {
        //echo 'SELECT video_name,user_id,id FROM user_videos WHERE `status` = 0 and user_id=' . intval($userid);
         $videos = \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT id, video_name, user_id FROM user_videos WHERE `status` = 0 and user_id=' . intval($userid),
            TRUE,
            FALSE,
            array('enable' => FALSE));
            return $videos;
            //var_dump($videos);
    }
    public static function getUserVideobyID($userid, $videoId)
    {
        //echo 'SELECT video_name,user_id,id FROM user_videos WHERE `status` = 0 and user_id=' . intval($userid);
         $videos = \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT id, video_name, user_id FROM user_videos WHERE `status` = 0 and id ='. intval($videoId).' and user_id=' . intval($userid),
            TRUE,
            FALSE,
            array('enable' => FALSE));
            return $videos;
            //var_dump($videos);
    }
    public static function deleteUserVideobyID($userid, $videoid)
    {
           $affected_rows = \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
            "user_videos",
            array('status' => '9'),
            "id = $videoid AND user_id = $userid");

        if($affected_rows)
            return TRUE;
    }
    
    
    /**
     * @param null $user_id
     * @param int $page_number
     * @param int $page_size
     * @return array
     */
    public static function getListOfAdsFavorites($user_id = null, $page_number = 1, $page_size = 25) {
        
        $where = " UF.user_id = ". $user_id;
        $vipField = ' 0 as vip';
        $vipOrdering = $premiumOrdering = '';
        $premiumField = ' 0 as premium';
        $bump_ordering = '';

        if (!empty($city_name) || !empty($category_name)){
            $bump_ordering = ' date_bumped > UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL 24 HOUR)) DESC,';
        }

        $wherevip=[];
        
        //if($user_id == 86523 || $user_id == 245459) {
//             \Core\library\log::debugUser(self::$VipAdIds, "VIP ADS IDS");   // 86523   245459
//        }
        
        
        
        if (!empty(self::$VipAdIds)) {
            $vipIds = implode(',',array_reverse(self::$VipAdIds));
            $vipField = " IF(FIND_IN_SET(A.id,'{$vipIds}'), 1, 0) as vip ";
            //$wherevip[] = " A.id IN({$vipIds}) ";
            $vipOrdering = " FIELD(A.id,{$vipIds}) DESC, ";
        }

        if (!empty(self::$PremiumAdIds)){
            $premiumIds = implode(',',array_reverse(self::$PremiumAdIds));
            $premiumField = " IF(FIND_IN_SET(A.id,'{$premiumIds}'), 1, 0) as premium ";
            //$wherevip[] = " A.id IN({$premiumIds}) ";
            $premiumOrdering = " FIELD(A.id,{$premiumIds}) DESC, ";
        }
        
        //if($user_id == 86523 || $user_id == 245459) {
//             \Core\library\log::debugUser(self::$PremiumAdIds, "PREMIUM ADS IDS");   // 86523   245459
//        }
        
        if(!empty($wherevip)){
                $where .= " OR (" . implode(" OR ", $wherevip ) ." ) ";     
        }
           
        $paginate = ($page_size * ($page_number - 1)) . "," . $page_size;

        $sql =  "
        SELECT
          A.id,
          images,
          date_released,
          category,
          city,
          title,
          description,
          date_bumped,
          `verification_status`,
          A.`status`,
          {$vipField},
          {$premiumField}
        FROM 
            user_favorites UF 
        LEFT JOIN ads A ON A.id = UF.ad_id
        WHERE
          
          {$where}
        ORDER BY
            {$vipOrdering}
            {$premiumOrdering}
            {$bump_ordering}    
            date_released DESC
        LIMIT {$paginate}";
        
        //if($user_id == 86523 || $user_id == 245459) {
//             \Core\library\log::debugUser($sql, "FAVOTIES QUERY");   // 86523   245459   SELECT * FROM `user_favorites` WHERE user_id=245459 LIMIT 1000;
//        }
        
        
        $Result = [];
        $Data = \Core\Utils\Data::findBySQL(self::DEFAULT_DB_CONFIG,
            $sql,
            FALSE,
            TRUE,
            array('enable' => false)
        );
        
        //if($user_id == 86523 || $user_id == 245459) {
//             \Core\library\log::debugUser($Data, "FAVOTIES QUERY RESULT");   // 86523   245459
//        }
        
        foreach ($Data['recordset'] as $ad){
            if ($ad['vip'] == 1){
                $Result['vip_ads'][] = $ad;
            }elseif ($ad['premium'] == 1){
                $Result['premium_ads'][] = $ad;
            }else{
                $Result['ads']['recordset'][] = $ad;
            }
        }
        
        //if($user_id == 86523 || $user_id == 245459) {
//             \Core\library\log::debugUser($Result, "FAVOTIES QUERY RESULT SORTED");   // 86523   245459
//        }
        
        $Result['ads']['count'] = $Data['count'];
        return $Result;
    }
}
