<?php
Namespace Modules\Humpchies;

final class TagModel{
    
    const DEFAULT_DB_CONFIG                         = '\Modules\Humpchies\Db\Default';
    
    /**
     * Because this class is untended to be a singleton we block access to its constructor.
     * Therefore the class can only be instantiated from within itself.
     */
    private function __construct()
    {}
    
    /**
     * Disable object copying
     */
    private function __clone()
    {}

    /**
     * Disable object serialization
     */
    private function __wakeup()
    {}

    /**
     * Disable object deserialization
     */
    private function __sleep()
    {}

    
    public static function getTagsList($language = "fr") {
        
        //return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG, 'SELECT id, `'. $language .'` FROM `tags WHERE 1 = 1', FALSE, FALSE, ['enable' => true]);
        
        $redisKey = "TagsList-%s";
        $tagsList = \Core\library\Redis::get(sprintf($redisKey, $language));
        $tagsList = false;
        //var_dump($tagsList);   
        if($tagsList){
            return unserialize($tagsList);
        }
        //var_dump('SELECT id, `'. $language .'` FROM tags WHERE 1');
        $tagsList = \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT id, `'. $language .'` as name FROM tags WHERE 1',
            TRUE,
            FALSE,
            array('enable' => FALSE));
        
        array_walk($tagsList, function(&$tag) {
          $tag['slug'] = preg_replace("`[^a-z0-9-.]+`is", "-", strtolower($tag["name"]));
        });
        
        \Core\library\Redis::set(sprintf($redisKey, $language), serialize($tagsList));   
     
        //var_dump($tagsList);
        return $tagsList;    
        
    }

    
    
    public static function findTagIdBySlugList($slugs) {
        
        $result = [];
        $slugArr = explode(',', $slugs);
        
        $lang =  (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';
        $all_tags = self::getTagsList($lang);
        
        if(!empty($slugArr)) {
            
            foreach($slugArr as $slug) {
                foreach($all_tags as $index => $tag) {
                    if($tag['slug'] == $slug) 
                    $result[] = $tag['id'];
                }    
            }
            
            return implode(",", $result);
        }
        
        return FALSE;
    }
    
}
