<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to interact with ad image related tasks. Such tasks might
 * but are not restricted to: add a new ad image, delete an ad image, etc.
 */

Namespace Modules\Humpchies;

final class AdLocationModel{

    // CONSTANTS
    // =========
     //Db configs
    const DEFAULT_DB_CONFIG = '\Modules\Humpchies\Db\Default';
    const DEFAULT_DB_TABLE = 'ads_location';

    // LOCAITOM Statuses
    const STATUS_SHOW = 0;
    const STATUS_HIDE = 1; 

    const ERROR_UNKNOWN         = 'Modules.Humpchies.AdLocationModel.ErrorUnknown'; //Some error occurrence on creation
    const NO_ERROR              = 'Modules.Humpchies.AdLocationModel.NoError';

    static $radius = 20;
    static $maxRadius = 2000;
    static $unit   = "mi"; //mi -miles km - kilometers

   static $yandexUrl = "https://static-maps.yandex.ru/1.x/?lang=en_US";
    static $mapBoxUrl        = "https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/";
    //static $mapBoxToken      = "pk.eyJ1IjoiZ2lnaWtlbiIsImEiOiJja2YyZXNncWcxMjF5MnFxZHEza202eWVrIn0.4t8DEWW4vaIMxbujBhj3Zg";
    static $mapBoxToken      = "pk.eyJ1IjoibWF4d2lydGhtYXBzIiwiYSI6ImNrbWFiNm5yaDFxMG8ycWtubDhvaXF6eGkifQ.zn5zSZgAcTS0Ksa7PT12jw";
    static $mapBoxTokenGeneric      = "pk.eyJ1IjoibWF4d2lydGhtYXBzIiwiYSI6ImNrbWFhdW5zODFwd3kydW53d2d0NWZoaGUifQ.nH3Ulwv11zAmQjVWK2AwMg";
    
    /**
     * Because this class is untended to be a singleton we block access to its constructor.
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}

    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}

    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}

    public static function create($fields) {
        //var_dump($fields);exit();
        $new_location_id = \Core\DBConn::insert(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $fields);
            
        if($new_location_id !== FALSE) {
            \Core\library\Redis::del("ad_locations"); 
            
            // ad photos
            $map_coords = self::getLocationInfo($fields['`ad_id`']);
            self::saveLocationImg($map_coords);
            
            return $new_location_id;
        } else
            return self::ERROR_UNKNOWN;
            
        
    }
   
    public static function createV2($fields) {
        //var_dump($fields);exit();
        $new_location_id = \Core\DBConn::insert(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $fields);
        
        //\Core\library\log::debug($new_location_id, "ADDED A NEW LOCATION");
            
        if($new_location_id !== FALSE) {
            \Core\library\Redis::del("ad_locations"); 
            
            // ad photos
            $map_coords = self::getLocationInfo($fields['`ad_id`']);
           
            //\Core\library\log::debug($map_coords, "MAP COORDS");
            self::saveLocationImgV2($map_coords);
            
            return $new_location_id;
        } else
            return self::ERROR_UNKNOWN;
            
        
    }
   
    public static function edit($ad_id, $user_id, $orig_fields, $update_fileds) {
        
        // update location if needed
        $fields = [];
        
        if($update_fileds['maps_lat'] !== $orig_fields['lat'])
            $fields['`lat`'] = $update_fileds['maps_lat'];   
        
        if($update_fileds['maps_long'] !== $orig_fields['long'])
            $fields['`long`'] = $update_fileds['maps_long'];  
        
        if($update_fileds['maps_city'] !== $orig_fields['current_city_name'])
            $fields['`current_city_name`'] = $update_fileds['maps_city'];
        
        if(isset($update_fileds["hide_map"]) && $update_fileds["hide_map"] == 'yes' && $orig_fields["hide_map"] == 0)   // hide map, it was visible before
            $fields['`hide_map`'] = 1;
        
        if(isset($update_fileds["hide_map"]) && $update_fileds["hide_map"] == 'no' && $orig_fields["hide_map"] == 1)   // show map, it was hidden before
            $fields['`hide_map`'] = 0;
        
        if(!isset($update_fileds["hide_map"]) && $orig_fields["hide_map"] == 1)   // show map, it was hidden before
            $fields['`hide_map`'] = 0;
        
        if(!isset($update_fileds["hide_map"]) && $orig_fields["hide_map"] == 1)   // show map, it was hidden before
            $fields['`hide_map`'] = 0;
        
        if(isset($update_fileds["city_id"]) && $orig_fields["ad_city_id"] !== $update_fileds["city_id"])   // show map, it was hidden before
            $fields['`ad_city_id`'] = $update_fileds['city_id'];
        
        if(isset($update_fileds["city"]) && $orig_fields["ad_city_name"] !== $update_fileds["city"])   // show map, it was hidden before
            $fields['`ad_city_name`'] = $update_fileds['city'];
        
        
        //var_dump($fields);exit();
        if(empty($fields))  // no updates
            return true;
        
        $affected_rows = \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $fields,
            "ad_id = $ad_id AND user_id = $user_id");
        
        if($affected_rows) {
            
            // ad photos
            self::saveLocationImg($orig_fields,$update_fileds["hide_map"],$update_fileds['maps_lat'],$update_fileds['maps_long']);
            
            \Core\Utils\Data::updateCacheBySql( self::DEFAULT_DB_CONFIG,
            'SELECT `id`, `ad_id`, `lat`, `long`, `hide_map`, `current_city_name`, `ad_city_id`, `ad_city_name`, `location_photo`, `location_photo_sat` FROM ads_location WHERE `ad_id` = ' . $ad_id,
            FALSE,
            FALSE);
            
             \Core\library\Redis::del("ad_locations");
            return TRUE;                             
        }
            
        $last_db_error = \Core\DBConn::getLastError(self::DEFAULT_DB_CONFIG, TRUE);

        if(empty($last_db_error))
            return TRUE;
        else
            return self::ERROR_UNKNOWN;
    }
    
    
    public static function editV2($ad_id, $user_id, $orig_fields, $update_fileds) {
        
        // update location if needed
        $fields = [];
        
        if($update_fileds['maps_lat'] !== $orig_fields['lat'])
            $fields['`lat`'] = $update_fileds['maps_lat'];   
        
        if($update_fileds['maps_long'] !== $orig_fields['long'])
            $fields['`long`'] = $update_fileds['maps_long'];  
        
        if($update_fileds['maps_city'] !== $orig_fields['current_city_name'])
            $fields['`current_city_name`'] = $update_fileds['maps_city'];
        
        if(isset($update_fileds["hide_map"]) && $update_fileds["hide_map"] == 'yes' && $orig_fields["hide_map"] == 0)   // hide map, it was visible before
            $fields['`hide_map`'] = 1;
        
        if(isset($update_fileds["hide_map"]) && $update_fileds["hide_map"] == 'no' && $orig_fields["hide_map"] == 1)   // show map, it was hidden before
            $fields['`hide_map`'] = 0;
        
        if(!isset($update_fileds["hide_map"]) && $orig_fields["hide_map"] == 1)   // show map, it was hidden before
            $fields['`hide_map`'] = 0;
        
        if(!isset($update_fileds["hide_map"]) && $orig_fields["hide_map"] == 1)   // show map, it was hidden before
            $fields['`hide_map`'] = 0;
        
        if(isset($update_fileds["city_id"]) && $orig_fields["ad_city_id"] !== $update_fileds["city_id"])   // show map, it was hidden before
            $fields['`ad_city_id`'] = $update_fileds['city_id'];
        
        if(isset($update_fileds["city"]) && $orig_fields["ad_city_name"] !== $update_fileds["city"])   // show map, it was hidden before
            $fields['`ad_city_name`'] = $update_fileds['city'];
        
        
        //var_dump($fields);exit();
        if(empty($fields))  // no updates
            return true;
        

        // if($ad_id == 1928191) {
        //     \Core\library\log::debug($fields, "MAP FIELDS");    
        // }

        $affected_rows = \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $fields,
            "ad_id = $ad_id AND user_id = $user_id");
        
        // if($ad_id == 1928191) {
        //     \Core\library\log::debug($affected_rows, "AFFECTED ROWS");    
        // }

        if($affected_rows) {
            
            // ad photos
            self::saveLocationImgV2($orig_fields,$update_fileds["hide_map"],$update_fileds['maps_lat'],$update_fileds['maps_long']);
            
            \Core\Utils\Data::updateCacheBySql( self::DEFAULT_DB_CONFIG,
                'SELECT `id`, `ad_id`, `lat`, `long`, `hide_map`, `current_city_name`, `ad_city_id`, `ad_city_name`, `location_photo`, `location_photo_sat` FROM ads_location WHERE `ad_id` = ' . $ad_id,
                FALSE,
                FALSE);
            
            \Core\library\Redis::del("ad_locations");
            return TRUE;                             
        }
            
        $last_db_error = \Core\DBConn::getLastError(self::DEFAULT_DB_CONFIG, TRUE);

        if(empty($last_db_error))
            return TRUE;
        else
            return self::ERROR_UNKNOWN;
    }
    
    public static function getLocationInfo($ad_id)     {   
        $location_data = \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT `id`, `ad_id`, `lat`, `long`, `hide_map`, `current_city_name`, `ad_city_id`, `ad_city_name`, `location_photo`, `location_photo_sat` FROM ads_location WHERE `ad_id` = ' . $ad_id,
            FALSE,
            FALSE,
            ['enable' => true]);
            
        if(!$location_data)
            return false;
        else
            return $location_data[0];       
    }
    
   
    public static function getLocationNear($latitude, $longitude, $distance, $page_number = 1, $page_size = null, &$total_ads = 0, $get_near = false) {
        
       //var_dump(\Core\library\Redis::exists("ad_locations")); exit();
       //\Core\library\Redis::del("ad_locations"); 
       
       if(\Core\library\Redis::exists("ad_locations") == 0 ) {
              self::setLocationNear($latitude, $longitude);
       }
        
       $results = \Core\library\Redis::geoRadius("ad_locations", $longitude, $latitude, $distance, self::$unit);
      
       if(!empty($results)) {
           foreach($results as &$result) {
                $result = unserialize($result);
           }
           
           if($page_size != null) {
               
               $total_ads = count($results);
               $paginate = ($page_size * ($page_number - 1));
               // return only paginated result
               $paginated_data = array_slice($results, $paginate, $page_size);
               $Result = [];
               foreach ($paginated_data as $ad){
                    if ($ad['vip'] == 1){
                        $Result['vip_ads'][] = $ad;
                    }elseif ($ad['premium'] == 1){
                        $Result['premium_ads'][] = $ad;
                    }else{
                        $Result['ads']['recordset'][] = $ad;
                    }
                }
                $Result['ads']['count']= $total_ads;
                return $Result;
           
           } else {
               
                $Result = array(
                    'status' => 'have_results',
                    'count' => count($results),
                    //'count' => 1,
                );
                return $Result;
           }
           
               
           return $results;
               
       } elseif($get_near) {   // we have no results, we much increase the range
            while(empty($results) && $distance <= self::$maxRadius) {
                $distance = $distance*2; // increase distance    
                $results = \Core\library\Redis::geoRadius("ad_locations", $longitude, $latitude, $distance, self::$unit);
                //var_dump($results);
           
                if(!empty($results)) {
           
                    $data = unserialize($results[0]);
                    return  [
                        'status' => 'nearest',
                        'city' => $data['city'],
                        'lat' => $data['lat'],
                        'lng' => $data['long']
                    ];
                    var_dump($return);
                    break; // leave loop
                }
            }  
           
           return false;
       } 
      
       return false;
    }
    
    
    public static function setLocationNear($latitude, $longitude) {
        
        if (!empty(\Modules\Humpchies\AdModel::$VipAdIds)){
            $vipIds = implode(',',array_reverse(self::$VipAdIds));
            $vipField = ", IF(FIND_IN_SET(A.id,'{$vipIds}'), 1, 0) as vip ";
            //$where .= " OR id IN({$vipIds}) ";
            //$vipOrdering = " FIELD(id,{$vipIds}) DESC, ";
        }

        if (!empty(\Modules\Humpchies\AdModel::$PremiumAdIds)){
            $premiumIds = implode(',',array_reverse(self::$PremiumAdIds));
            $premiumField = ", IF(FIND_IN_SET(A.id,'{$premiumIds}'), 1, 0) as premium ";
            //$where .= " OR id IN({$premiumIds}) ";
            //$premiumOrdering = " FIELD(id,{$premiumIds}) DESC, ";
        }
        
        
        $sql = "SELECT AL.id as location_id, AL.ad_id, AL.lat, AL.long,
                       A.id, A.title, A.description, A.`type`, A.`status`, A.images,
                       A.date_released,
                       A.category,
                       A.city,
                       A.title,
                       A.description,
                       A.date_bumped,
                       A.`verification_status`
                       {$vipField}
                       {$premiumField}
                  FROM ads_location AL
                        LEFT JOIN ads A ON A.id = AL.ad_id 
                  WHERE A.STATUS = 'active' AND AL.hide_map = 0 AND AL.lat > 0   
                  #HAVING DISTANCE < 30 
                  #ORDER BY DISTANCE 
                  #LIMIT 0 , 20; ";
       
      //var_dump($sql);  exit();
       $Data = \Core\Utils\Data::findBySQL(self::DEFAULT_DB_CONFIG,
            $sql,
            FALSE,
            TRUE,
            array('enable' => false)
        );
      
       if($Data['count'] > 0) {
             
           foreach($Data["recordset"] as $loc) {
              \Core\library\Redis::geoadd("ad_locations", $loc['long'], $loc['lat'], serialize($loc));
          }
       }        
       return false;     
    }
    
    public static function saveLocationImg($map_coords, $hide_map = 0, $current_lat = 0, $current_lng = 0) {
    
        $arrUpdate = [];     // nothing to update
       // var_dump($current_lat);
//        var_dump($current_lng);
//        var_dump($hide_map);
//        var_dump($map_coords);   exit();
        // check if photo was added, if not save photo
        if($hide_map == 0 || $hide_map == 'no') {
            
            $old_lat = floatval($map_coords['lat']);
            $old_long = floatval($map_coords['long']);
            
            switch(true) {
              
              case ($current_lat == 0 && $current_lng == 0):
                $lat  = $old_lat;
                $long = $old_long;
              break; 
              
              case ($old_lat != $current_lat || $old_long != $current_lng): 
                $lat  = $current_lat;
                $long = $current_lng;  
              break; 
              default:
                $lat = 0;
                $long = 0;
              break; 
            }
           
            if($lat != 0 && $long != 0) {
               
                // add normal photo
               $url = self::$yandexUrl . "&ll=$long,$lat&z=15&pt=$long,$lat,pm2dbm&l=map";
               $resulted_image =  file_get_contents($url);
               
               if($resulted_image) {
               
                   $image_name  = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_path') . 'locations/map-'.$map_coords['ad_id'].time().'.jpg';
                   file_put_contents($image_name, $resulted_image);
                   $arrUpdate['location_photo'] = 'map-'.$map_coords['ad_id'].time().'.jpg';    
               }
               
               // add sattelite photo
               $url_sat = self::$yandexUrl . "&ll=$long,$lat&z=15&pt=$long,$lat,pm2dbm&l=map,skl,trf,sat";
               $resulted_image_sat =  file_get_contents($url_sat);
               if($resulted_image_sat) {
               
                   $image_name  = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_path') . 'locations/map-sat-'.$map_coords['ad_id'].time().'.jpg';
                   file_put_contents($image_name, $resulted_image_sat);
                   $arrUpdate['location_photo_sat'] = 'map-sat-'.$map_coords['ad_id'].time().'.jpg';
               }
           }
        }
            
        if(empty($arrUpdate))  // no updates
            return false;
        
        // remove current photos
        if($map_coords['location_photo'] != null)
            unlink(\Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_path') . 'locations/' . $map_coords['location_photo']);
        
        if($map_coords['location_photo_sat'] != null)
            unlink(\Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_path') . 'locations/' . $map_coords['location_photo_sat']);
        
        $affected_rows = \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $arrUpdate,
            "id = " . $map_coords['id']);
        
        if($affected_rows) {
            
            \Core\Utils\Data::updateCacheBySql( self::DEFAULT_DB_CONFIG,
            'SELECT `id`, `ad_id`, `lat`, `long`, `hide_map`, `current_city_name`, `ad_city_id`, `ad_city_name`, `location_photo`, `location_photo_sat` FROM ads_location WHERE `ad_id` = ' . $map_coords["ad_id"],
            FALSE,
            FALSE);
            
            \Core\library\Redis::del("ad_locations");
            //var_dump($arrUpdate);exit();
            return $arrUpdate; // return photos                            
        }
    }
    
    public static function saveLocationImgV2($map_coords, $hide_map = 0, $current_lat = 0, $current_lng = 0) {
    
        $arrUpdate = [];     // nothing to update
        //\Core\library\log::debug($hide_map, "HIDE MAP");
        
        // check if photo was added, if not save photo
        if($hide_map == 0 || $hide_map == 'no') {
            
            $old_lat = floatval($map_coords['lat']);
            $old_long = floatval($map_coords['long']);
            
            switch(true) {
              
              case ($current_lat == 0 && $current_lng == 0):
                $lat  = $old_lat;
                $long = $old_long;
              break; 
              
              case ($old_lat != $current_lat || $old_long != $current_lng): 
                $lat  = $current_lat;
                $long = $current_lng;  
              break; 
              default:
                $lat = 0;
                $long = 0;
              break; 
            }
            
            //\Core\library\log::debug($lat, "LAT");
            //\Core\library\log::debug($long, "LONG");
           
            if($lat != 0 && $long != 0) {
               
                // add normal photo
               $url = self::$mapBoxUrl . "geojson(%7B%22type%22%3A%22Point%22%2C%22coordinates%22%3A%5B$long%2C$lat%5D%7D)/$long,$lat,12/700x300?access_token=" . self::$mapBoxTokenGeneric;
               //geojson(%7B%22type%22%3A%22Point%22%2C%22coordinates%22%3A%5B-73.571504074097%2C45.503189990622%5D%7D)/-73.571504074097,45.503189990622,12/640x300?access_token=pk.eyJ1IjoiZ2lnaWtlbiIsImEiOiJja2YyZXNncWcxMjF5MnFxZHEza202eWVrIn0.4t8DEWW4vaIMxbujBhj3Zg
              //\Core\library\log::debug($url, "URL");
               $resulted_image =  file_get_contents($url);
              // \Core\library\log::debug($resulted_image, "RESULTED IMAGE");

               // if($map_coords['ad_id'] == 1928191) {
               //      \Core\library\log::debug($url, "RESULTED IMAGE");
               //      \Core\library\log::debug($resulted_image, "RESULTED IMAGE");
               //  }

               if($resulted_image) {
                   
                   $image_filename = 'map-'.$map_coords['ad_id'].time().'.jpg';   
                   $image_name  = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_path') . 'locations/'.$image_filename;
                   //\Core\library\log::debug($image_name, "IMAGE PATH");
                   file_put_contents($image_name, $resulted_image);
                   $arrUpdate['location_photo'] = $image_filename;
               }
           }
        }
        

        // if($map_coords['ad_id'] == 1928191) {
        //     \Core\library\log::debug($arrUpdate, "ARRUPDATE");
        // }

        if(empty($arrUpdate))  // no updates
            return false;
        
        // remove current photos
        if($map_coords['location_photo'] != null)
            unlink(\Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_path') . 'locations/' . $map_coords['location_photo']);
        
        $affected_rows = \Core\DBConn::update(  self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $arrUpdate,
            "id = " . $map_coords['id']);
        

        // if($map_coords['ad_id'] == 1928191) {
        //     \Core\library\log::debug($affected_rows, "AFFECTED ROWS");
        // }

        if($affected_rows) {
            
            \Core\Utils\Data::updateCacheBySql( self::DEFAULT_DB_CONFIG,
            'SELECT `id`, `ad_id`, `lat`, `long`, `hide_map`, `current_city_name`, `ad_city_id`, `ad_city_name`, `location_photo`, `location_photo_sat` FROM ads_location WHERE `ad_id` = ' . $map_coords["ad_id"],
            FALSE,
            FALSE);
            
            \Core\library\Redis::del("ad_locations");
            //var_dump($arrUpdate);exit();
            return $arrUpdate; // return photos                            
        }
    }
    
   
}
?>