<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to interact with Categories.
 */

Namespace Modules\Humpchies;

final class CategoryModel{
  
    // SINGLETONS
    // ==========
    private static $supported_categories            = NULL;
    
    // CONSTANTS
    // =========
    const CATEGORY_ESCORT                           = 'escort';
    const CATEGORY_ESCORT_AGENCY                    = 'escort-agency';
    const CATEGORY_EROTIC_MASSAGE                   = 'erotic-massage';
    const CATEGORY_EROTIC_MASSAGE_PARLOR            = 'erotic-massage-parlor'; 
    
    // ERROR MESSAGES
    // ==============
    const ERROR_CATEGORY_IS_INVALID                 = 'Modules.Humpchies.CategoryModel.ErrorCategoryIsInvalid';
    const ERROR_CATEGORY_IS_EMPTY                   = 'Modules.Humpchies.CategoryModel.ErrorCategoryIsEmpty';
    const NO_ERROR                                  = 'Modules.Humpchies.CategoryModel.NoError';

    static $label = [
        'en-ca' => [
            'escort'                => 'Escorts',
            'escort-agency'         => 'Agencies',
            'erotic-massage'        => 'Erotic Massages',
            'erotic-massage-parlor' => 'Erotic Massage Parlors'
            ],
        'fr-ca' => [
            'escort'                => 'Escortes',
            'escort-agency'         => 'Agences',
            'erotic-massage'        => 'Massages &Eacute;rotiques',
            'erotic-massage-parlor' => 'Salons de Massage &Eacute;rotique'
        ]
            
    ];
    
    /**
    * Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}
    
    public static function getSupportedCategories()
    {
        if(self::$supported_categories !== NULL)
            return self::$supported_categories;
            
        return (self::$supported_categories = array(self::CATEGORY_ESCORT,
                                                    self::CATEGORY_ESCORT_AGENCY,
                                                    self::CATEGORY_EROTIC_MASSAGE,
                                                    self::CATEGORY_EROTIC_MASSAGE_PARLOR));
    }

    public static function isCategoryInputValid(&$category_code)
    {
        if(empty($category_code))
            return self::ERROR_CATEGORY_IS_EMPTY;
        elseif(@in_array($category_code, self::getSupportedCategories()))
            return self::NO_ERROR;
            
        $category_code = NULL;
        return self::ERROR_CATEGORY_IS_INVALID;    
    }
	
	public static function isPaymentCategoryValid($category)
	{
		$supported_category_types = array_merge(array('main', 'main-plus'), self::getSupportedCategories());
		
		if(in_array($category, $supported_category_types)){
			return true;
		}
		else{
			return false;
		}
	}		
    
    public static function getCategoryLabel($category, $lang) {
        
        return self::$label[$lang][$category];
    }	
}
?>
