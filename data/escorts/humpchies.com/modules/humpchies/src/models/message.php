<?php

Namespace Modules\Humpchies;

/**
 * Class MessageModel
 * @package Modules\Humpchies
 */
final class MessageModel
{
    // Number of items in lists
    const MESSAGE_LIST_SIZE = 15;
    const CHAT_MESSAGE_LIST_SIZE = 50;

    // Database
    const DEFAULT_DB_CONFIG = '\Modules\Humpchies\Db\Default';         // The config file to use by default
    const DEFAULT_DB_TABLE = 'message';                                // The default table to use
    const ORDER_DESC = 'DESC';
    const ORDER_ASC = 'ASC';
    const MIN_ID_VALUE = 1;
    const MAX_ID_VALUE = 2147483647;

    // Cache keys
    const KEY_COUNT_UNREAD_MESSAGES = __CLASS__ . '::countUnreadMessages';
    const KEY_GET_ONE_MESSAGE = __CLASS__ . '::getOneMessage';
    const KEY_GET_TIME_PERIODS = __CLASS__ . '::getTimePeriods';

    // Cache expire [seconds]
    const TTL_COUNT_UNREAD_MESSAGES = 300;
    const TTL_GET_ONE_MESSAGE = 300;
    const TTL_GET_TIME_PERIODS = 300;

    // Time periods
    const TIME_YEARS_AGO = 'years ago';
    const TIME_ONE_YEAR = 'one year';
    const TIME_MONTHS_AGO = 'months ago';
    const TIME_ONE_MONTH = 'one month';
    const TIME_WEEKS_AGO = 'weeks ago';
    const TIME_ONE_WEEK = 'one week';
    const TIME_DAYS_AGO = 'days ago';
    const TIME_YESTERDAY = 'yesterday';
    const TIME_YESTERDAY_AT = 'yesterday at';
    const TIME_HOURS_AGO = 'hours ago';
    const TIME_ONE_HOUR_AGO = 'one hour ago';
    const TIME_MINUTES_AGO = 'minutes ago';
    const TIME_ONE_MINUTE_AGO = 'one minute ago';
    const TIME_SECONDS_AGO = 'seconds ago';
    const TIME_ONE_SECOND_AGO = 'one second ago';
    const TIME_JUST_NOW = 'just now';

    // Used to translate time periods
    private static $timePeriods = array(
        self::TIME_YEARS_AGO => '[TIME-YEARS-AGO]',
        self::TIME_ONE_YEAR => '[TIME-ONE-YEAR-AGO]',
        self::TIME_MONTHS_AGO => '[TIME-MONTHS-AGO]',
        self::TIME_ONE_MONTH => '[TIME-ONE-MONTH-AGO]',
        self::TIME_WEEKS_AGO => '[TIME-WEEKS-AGO]',
        self::TIME_ONE_WEEK => '[TIME-ONE-WEEK-AGO]',
        self::TIME_DAYS_AGO => '[TIME-DAYS-AGO]',
        self::TIME_YESTERDAY => '[TIME-YESTERDAY]',
        self::TIME_YESTERDAY_AT => '[TIME-YESTERDAY-AT]',
        self::TIME_HOURS_AGO => '[TIME-HOURS-AGO]',
        self::TIME_ONE_HOUR_AGO => '[TIME-ONE-HOUR-AGO]',
        self::TIME_MINUTES_AGO => '[TIME-MINUTES-AGO]',
        self::TIME_ONE_MINUTE_AGO => '[TIME-ONE-MINUTE-AGO]',
        self::TIME_SECONDS_AGO => '[TIME-SECONDS-AGO]',
        self::TIME_ONE_SECOND_AGO => '[TIME-ONE-SECOND-AGO]',
        self::TIME_JUST_NOW => '[TIME-JUST-NOW]',
    );

    /**
     * Because this class is untended to be a singleton we block access to its constructor.
     * Therefore the class can only be instantiated from within itself.
     */
    private function __construct()
    {
    }

    /**
     * Disable object copying
     */
    private function __clone()
    {
    }

    /**
     * Disable object serialization
     */
    private function __wakeup()
    {
    }

    /**
     * Disable object deserialization
     */
    private function __sleep()
    {
    }

    /**
     * Get translated time periods.
     * 
     * @return array
     */
    private static function getTimePeriods()
    {
        static $translated = false;
        
        if (!$translated) {

            // get from cache
            $cacheKey = self::getCacheKey(self::KEY_GET_TIME_PERIODS, \Core\Utils\Session::getLocale());
            $cached = @unserialize(\Core\library\Redis::get($cacheKey));
            if ($cached !== false) {
                self::$timePeriods = $cached;
            } else {
                $text = implode($delimiter = '#PART#', self::$timePeriods);
                \Core\Utils\Text::translateBufferTokens($text,
                    \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'),
                    \Core\Utils\Session::getLocale(),
                    \Core\Utils\Config::getConfigValue('\Application\Settings::locales_module',
                        FALSE));
                $elements = array_combine(array_keys(self::$timePeriods), explode($delimiter, $text));
                if ($elements) {
                    self::$timePeriods = $elements;
                } else {
                    self::$timePeriods = array_combine(array_keys(self::$timePeriods), array_keys(self::$timePeriods));
                }
                // write to cache
                \Core\library\Redis::set($cacheKey, serialize(self::$timePeriods), self::TTL_GET_TIME_PERIODS);
            }
            
            $translated = true;
        }

        return self::$timePeriods;
    }

    /**
     * Get unique chat ID.
     *
     * @param int $adId The AD ID.
     * @param int $fromUserId The user ID that want to send the message.
     * @param int $toUserId The user ID that will receive the message.
     * @return string
     */
    public static function getChatId($adId, $fromUserId, $toUserId)
    {
        return md5($adId . '-' . min($fromUserId, $toUserId) . '-' . max($fromUserId, $toUserId));
    }

    /**
     * Send a new message (@param int $adId The AD ID.
     * @param int $fromUserId The user ID that want to send the message.
     * @param int $toUserId The user ID that will receive the message.
     * @param string $message
     * @return int|null
     * @see self::replyTo if you want to send a reply).
     *
     */
    public static function sendMessage($adId, $fromUserId, $toUserId, $message) {
        $chatId = self::getChatId($adId, $fromUserId, $toUserId);
        $message =  \Core\Utils\Security::htmlToText($message);
        
//        var_dump( array(
//                'ad_id' => $adId,
//                'from_user_id' => $fromUserId,
//                'to_user_id' => $toUserId,
//                'chat_id' => $chatId,
//                'message' => trim(filter_var($message, FILTER_SANITIZE_STRING))
//            ));
//            die();
        $messageId = \Core\DBConn::insert(
            self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            array(
                'ad_id' => $adId,
                'from_user_id' => $fromUserId,
                'to_user_id' => $toUserId,
                'chat_id' => $chatId,
                'message' => mb_convert_encoding($message, 'HTML-ENTITIES', "UTF-8")
            )
        );

        $result = $messageId ? $messageId : null;

        if ($result) {
            self::onUpdateUserMessages($toUserId);
            self::onUpdateChat($chatId);
        }

        return $result;
    }

    /**
     * Test if the user can send a message to another user.
     *
     * @param int $fromUserId The user ID that want to send the message.
     * @param int $toUserId The user ID that will receive the message.
     * @return bool
     */
    public static function canSendMessage($fromUserId, $toUserId)
    {
        return ($fromUserId != $toUserId)
            && !self::isBlockedUser($fromUserId, $toUserId) // blocked by `$toUserId`
            && !self::isBlockedUser($toUserId, $fromUserId); // blocked user
    }

    /**
     * Get one message by chat ID.
     *
     * @param string $chatId The unique chat ID.
     * @return array|null
     */
    public static function getOneMessage($chatId)
    {
        // get from cache
        $cacheKey = self::getCacheKey(self::KEY_GET_ONE_MESSAGE, $chatId);
        $cached = @unserialize(\Core\library\Redis::get($cacheKey));
        if ($cached !== false) {
            return $cached;
        }

        $result = \Core\DBConn::selectRowClean(
            self::DEFAULT_DB_CONFIG,
            "
            SELECT
                *
            FROM 
                " . self::DEFAULT_DB_TABLE . "
            WHERE
                chat_id = ':chat_id'
            LIMIT 1
            ",
            array(
                'chat_id' => $chatId
            ));

        $return = $result ? $result : null;

        // write to cache
        \Core\library\Redis::set($cacheKey, serialize($return), self::TTL_GET_ONE_MESSAGE);

        return $return;
    }

    /**
     * Test if user ID can see the message.
     *
     * @param int $userId
     * @param array $message
     * @return bool
     */
    public static function canAccessMessage($userId, $message)
    {
        
        return ((int)$message['from_user_id'] == $userId)
            || ((int)$message['to_user_id'] == $userId);
    }

    /**
     * Get other user ID from message.
     * 
     * @param int $userId
     * @param array $message
     * @return mixed
     */
    public static function getOtherUserFromMessage($userId, $message)
    {
        return ($message['from_user_id'] != $userId)
            ? (int)$message['from_user_id']
            : (int)$message['to_user_id'];
    }
    
    /**
     * Reply to a message from a chat.
     *
     * @param int $userId The user ID that want to send the reply.
     * @param array $chatMessage The message to send reply for.
     * @param string $message
     * @return int|null
     */
    public static function replyTo($userId, $chatMessage, $message)
    {
        $toUserId = self::getOtherUserFromMessage($userId, $chatMessage);
        $message =  \Core\Utils\Security::htmlToText($message);
        $messageId = \Core\DBConn::insert(
            self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            array(
                'ad_id' => $chatMessage['ad_id'],
                'from_user_id' => $userId,
                'to_user_id' => $toUserId,
                'chat_id' => $chatMessage['chat_id'],
                'message' => mb_convert_encoding($message, 'HTML-ENTITIES', "UTF-8")
            )
        );

        $result = $messageId ? $messageId : null;

        if ($result) {
            self::onUpdateUserMessages($toUserId);
            self::onUpdateChat($chatMessage['chat_id']);
        }

        return $result;
    }

    /**
     * Test if the user can reply to a message from another user.
     *
     * @param int $userId The user ID that want to send the message.
     * @param array $chatMessage The message to send reply for.
     * @return bool
     */
    public static function canReplyTo($userId, $chatMessage)
    {
        $toUserId = self::getOtherUserFromMessage($userId, $chatMessage);
        
        return (
                ($chatMessage['from_user_id'] == $userId)
                || ($chatMessage['to_user_id'] == $userId)
            )
            && !self::isBlockedUser($userId, $toUserId) // blocked by `$toUserId`
            && !self::isBlockedUser($toUserId, $userId); // blocked user
    }

    /**
     * Get cache key.
     *
     * @param string $key
     * @return string
     */
    private static function getCacheKey($key)
    {
        return implode('|', func_get_args());
    }

    /**
     * Count the number of unread messages.
     *
     * @param int $userId The user ID to count unread messages for.
     * @return int
     */
    public static function countUnreadMessages($userId)
    {
        // get from cache
        $cacheKey = self::getCacheKey(self::KEY_COUNT_UNREAD_MESSAGES, $userId);
        $cached = @unserialize(\Core\library\Redis::get($cacheKey));
        if ($cached !== false) {
            return (int)$cached;
        }

        $result = \Core\DBConn::selectRowClean(
            self::DEFAULT_DB_CONFIG,
            '
            SELECT
                COUNT(id) AS unread 
            FROM 
                ' . self::DEFAULT_DB_TABLE . '
            WHERE
                to_user_id = :to_user_id
                AND date_read IS NULL
                AND `status` = "visible"
            ',
            array(
                'to_user_id' => $userId
            ));

        $return = $result ? (int)$result['unread'] : 0;

        // write to cache
        \Core\library\Redis::set($cacheKey, serialize($return), self::TTL_COUNT_UNREAD_MESSAGES);

        return $return;
    }

    /**
     * Mark all messages of a chat as read.
     *
     * @param int $userId The user ID to mark messages as read.
     * @param string $chatId The unique chat ID.
     * @param int $minId Minimum ID value to start with.
     * @param int $maxId Maximum ID value to end with.
     * @return bool
     */
    public static function markAsRead($userId, $chatId, $minId = self::MIN_ID_VALUE, $maxId = self::MAX_ID_VALUE)
    {
        $where = array('1 = 1');
        if ($minId > self::MIN_ID_VALUE) {
            $where[] = 'id >= ' . (int)$minId;
        }
        if ($maxId < self::MAX_ID_VALUE) {
            $where[] = 'id <= ' . (int)$maxId;
        }

        $result = (bool)\Core\DBConn::execute(
            self::DEFAULT_DB_CONFIG,
            'UPDATE 
                ' . self::DEFAULT_DB_TABLE . '
            SET 
                date_read=NOW() 
            WHERE
                ' . implode('AND', $where) . '
                AND chat_id = ' . \Core\DBConn::escape(self::DEFAULT_DB_CONFIG, $chatId, TRUE) . '
                AND to_user_id = ' . (int)$userId . '
                AND date_read IS NULL
            '
        );

        if ($result) {
            self::onUpdateUserMessages($userId);
        }

        return $result;
    }

    /**
     * Clear cache on update user messages.
     *
     * @param int $userId
     */
    private static function onUpdateUserMessages($userId)
    {
        $cacheKey = self::getCacheKey(self::KEY_COUNT_UNREAD_MESSAGES, $userId);
        \Core\library\Redis::del($cacheKey);
    }

    /**
     * Test if a user was blocked by another.
     *
     * @param int $userId The user ID to check if was blocked by `$blockedByUserId`.
     * @param int $blockedByUserId The user ID who blocked `$userId`.
     * @return bool
     */
    public static function isBlockedUser($userId, $blockedByUserId)
    {
        $result = \Core\DBConn::selectRowClean(
            self::DEFAULT_DB_CONFIG,
            '
            SELECT
                id
            FROM 
                message_blocked
            WHERE
                user_id = :user_id
                AND blocked_user_id = :blocked_user_id
            LIMIT 1    
            ',
            array(
                'user_id' => $blockedByUserId,
                'blocked_user_id' => $userId
            )
        );
        return (bool)$result;
    }

    /**
     * Return an indexed array user ID => 1 for the blocked users.
     *
     * @param int $userId User that blocked other users.
     * @param int[] $userIdList An user ID list to test against.
     * @return array
     */
    public static function getBlockedUserList($userId, $userIdList)
    {
        $result = \Core\DBConn::select(self::DEFAULT_DB_CONFIG,
            '
            SELECT
                blocked_user_id,
                1 AS blocked
            FROM
                message_blocked
            WHERE
                user_id = ' . (int)$userId . '
                AND blocked_user_id IN (' . implode(',', $userIdList) . ')'
        );
        return $result ? array_column($result, 'blocked', 'blocked_user_id') : array();
    }

    /**
     * Block user.
     *
     * @param int $userId The user ID that want to block user `$blockedUserId`.
     * @param int $blockedUserId The user ID of the blocked user.
     * @return int|null
     */
    public static function blockUser($userId, $blockedUserId)
    {
        $messageBlockedId = \Core\DBConn::insert(
            self::DEFAULT_DB_CONFIG,
            'message_blocked',
            array(
                'user_id' => $userId,
                'blocked_user_id' => $blockedUserId
            )
        );
        return $messageBlockedId ? $messageBlockedId : null;
    }

    /**
     * Block user.
     *
     * @param int $userId The user ID that want to block user in `$message`.
     * @param array $message The message to block the user from.
     * @return int|null
     */
    public static function blockUserByMessage($userId, $message)
    {
        $blockedUserId = self::getOtherUserFromMessage($userId, $message);
        return self::blockUser($userId, $blockedUserId);
    }

    /**
     * Unblock user.
     *
     * @param int $userId The user ID that want to unblock user `$blockedUserId`.
     * @param int $blockedUserId The user ID of the blocked user.
     * @return bool
     */
    public static function unblockUser($userId, $blockedUserId)
    {
        return \Core\DBConn::delete(
                self::DEFAULT_DB_CONFIG,
                'message_blocked',
                'user_id = ' . (int)$userId . ' AND blocked_user_id = ' . (int)$blockedUserId
            ) > 0;
    }

    /**
     * Unblock user.
     *
     * @param int $userId The user ID that want to unblock user in `$message`.
     * @param array $message The message to unblock the user from.
     * @return int|null
     */
    public static function unblockUserByMessage($userId, $message)
    {
        $blockedUserId = self::getOtherUserFromMessage($userId, $message);
        return self::unblockUser($userId, $blockedUserId);
    }

    /**
     * Prepare values for SQL.
     *
     * @param int $startId
     * @param int $count
     * @param string $order
     * @param int $limitResults
     */
    private static function prepareQueryLimits(&$startId, &$count, &$order, $limitResults = 1000)
    {
        $startId = min(max((int)$startId, self::MIN_ID_VALUE), self::MAX_ID_VALUE);
        $count = min(max((int)$count, 1), $limitResults);
        if (!in_array($order, array(self::ORDER_ASC, self::ORDER_DESC))) {
            $order = self::ORDER_DESC;
        }
    }

    /**
     * Get paginated messages.
     *
     * @param int $userId The user ID to get messages for.
     * @param int $startId The ID of the message to start with (for pagination).
     * @param int $count How many messages to return.
     * @param string $order The order to consider (self::ORDER_DESC to take messages less or equal than `$startId`).
     * @return array
     */
    public static function getMessages($userId, $startId = self::MAX_ID_VALUE, $count = self::MESSAGE_LIST_SIZE, $order = self::ORDER_DESC)
    {
        self::prepareQueryLimits($startId, $count, $order);
        switch ($order) {
            case self::ORDER_DESC:
                $where = 'M.id <= ' . $startId;
                break;
            case self::ORDER_ASC:
                $where = 'M.id >= ' . $startId;
                break;
        }
        $result = \Core\DBConn::select(self::DEFAULT_DB_CONFIG,
            '
            SELECT
                M.*,
                T.unread
            FROM
                ' . self::DEFAULT_DB_TABLE . ' M
                INNER JOIN (
                    SELECT 
                        MAX(id) AS id,
                        SUM(IF(date_read IS NULL AND to_user_id = ' . (int)$userId . ', 1, 0)) AS unread
                    FROM 
                        ' . self::DEFAULT_DB_TABLE . '                        
                    WHERE
                        from_user_id = ' . (int)$userId . '
                        OR to_user_id = ' . (int)$userId . '
                    GROUP BY chat_id
                ) T ON (T.id = M.id)
            WHERE
                ' . $where . '
            ORDER BY id ' . $order . '
            LIMIT ' . $count
        );
        return $result ? $result : array();
    }

    /**
     * Simulate big list (@param int $userId The user ID to get messages for.
     * @param int $startId The ID of the message to start with (for pagination).
     * @param int $count How many messages to return.
     * @param string $order The order to consider (self::ORDER_DESC to take messages less or equal than `$startId`).
     * @return array
     * @throws \Exception
     * @see `getMessages`), can be used for testing.
     *
     */
    public static function _getMessages($userId, $startId = self::MAX_ID_VALUE, $count = self::MESSAGE_LIST_SIZE, $order = self::ORDER_DESC)
    {
        $result = array();
        if ($startId == self::MAX_ID_VALUE) {
            $startId = 500;
        }
        $counting = ($order == self::ORDER_DESC) ? -1 : 1;
        for ($i = 0; $i < $count; $i++) {
            $date = new \DateTime();
            $date->setTime(mt_rand(0, 23), mt_rand(0, 59), mt_rand(0, 59));
            $date->setDate(mt_rand(date('Y') - 1, date('Y')), mt_rand(0, 11), mt_rand(0, 27));
            $result[] = array(
                'id' => $startId,
                'ad_id' => 1,
                'from_user_id' => $userId,
                'to_user_id' => 1,
                'chat_id' => md5($startId),
                'message' => $startId . ' Some message ' . time(),
                'created' => $date->format('Y-m-d H:i:s'),
                'date_read' => null,
                'unread' => rand(0, 9),
                'user' => 'Username',
                'colour' => rand(0, 1) ? (rand(0, 1) ? '#' . rand(0, 9) . rand(0, 9) . rand(0, 9) : 'image') : 'hart',
                'avatar' => chr(64 + rand(0, 9)),
                'intro' => 'Intro message here',
            );
            $startId += $counting;
            if (($startId < 400) || ($startId > 550)) {
                break;
            }
        }
        return $result;
    }

    /**
     * Get paginated chat messages.
     *
     * @param int $userId The user ID to get chat messages for.
     * @param string $chatId The unique chat ID.
     * @param int $startId The ID of the message to start with (for pagination).
     * @param int $count How many messages to return.
     * @param string $order The order to consider (self::ORDER_DESC to take messages less or equal than `$startId`).
     * @return array
     */
    public static function getChatMessages($userId, $chatId, $startId = self::MAX_ID_VALUE, $count = self::CHAT_MESSAGE_LIST_SIZE, $order = self::ORDER_DESC)
    {
        self::prepareQueryLimits($startId, $count, $order);
        if ($order === self::ORDER_ASC) {
            $where = 'id >= ' . $startId;
        } else {
            $where = 'id <= ' . $startId;
        }
        
        
        $result = \Core\DBConn::select(self::DEFAULT_DB_CONFIG,
            '
            SELECT
                *
            FROM
                ' . self::DEFAULT_DB_TABLE . '
            WHERE
                ' . $where . '
                AND chat_id = ' . \Core\DBConn::escape(self::DEFAULT_DB_CONFIG, $chatId, TRUE) . '
                AND (
                    from_user_id = ' . (int)$userId . '
                    OR to_user_id = ' . (int)$userId . '
                )
            ORDER BY id ' . $order . '
            LIMIT ' . $count
        );
        return $result ? $result : array();
    }

    /**
     * Simulate big list (@param int $userId The user ID to get chat messages for.
     * @param string $chatId The unique chat ID.
     * @param int $startId The ID of the message to start with (for pagination).
     * @param int $count How many messages to return.
     * @param string $order The order to consider (self::ORDER_DESC to take messages less or equal than `$startId`).
     * @return array
     * @throws \Exception
     * @see `getChatMessages`), can be used for testing.
     *
     */
    public static function _getChatMessages($userId, $chatId, $startId = self::MAX_ID_VALUE, $count = self::CHAT_MESSAGE_LIST_SIZE, $order = self::ORDER_DESC)
    {
        $result = array();
        if ($startId == self::MAX_ID_VALUE) {
            $startId = 500;
        }
        $counting = ($order == self::ORDER_DESC) ? -1 : 1;
        for ($i = 0; $i < $count; $i++) {
            $date = new \DateTime();
            $date->setTime(mt_rand(0, 23), mt_rand(0, 59), mt_rand(0, 59));
            $date->setDate(mt_rand(date('Y') - 1, date('Y')), mt_rand(0, 11), mt_rand(0, 27));
            $result[] = array(
                'id' => $startId,
                'ad_id' => 1,
                'from_user_id' => ($rand = rand(0, 1)) ? $userId : 1,
                'to_user_id' => rand(0, 1) ? 1 : $userId,
                'chat_id' => $chatId,
                'message' => $startId . ' Some message ' . time(),
                'created' => $date->format('Y-m-d H:i:s'),
                'date_read' => null,
            );
            $startId += $counting;
            if (($startId < 350) || ($startId > 600)) {
                break;
            }
        }
        return $result;
    }

    /**
     * Delete chat messages.
     *
     * @param string $chatId Delete chat messages.
     * @return bool
     */
    public static function deleteChat($chatId)
    {
        $oneMessage = self::getOneMessage($chatId);

        $result = \Core\DBConn::delete(
                self::DEFAULT_DB_CONFIG,
                self::DEFAULT_DB_TABLE,
                'chat_id = ' . \Core\DBConn::escape(self::DEFAULT_DB_CONFIG, $chatId)
            ) > 0;

        if ($result) {
            if ($oneMessage) {
                self::onUpdateUserMessages($oneMessage['from_user_id']);
                self::onUpdateUserMessages($oneMessage['to_user_id']);
            }
            self::onUpdateChat($chatId);
        }

        return $result;
    }

    /**
     * Clear cache on delete chat.
     *
     * @param string $chatId
     */
    private static function onUpdateChat($chatId)
    {
        $cacheKey = self::getCacheKey(self::KEY_GET_ONE_MESSAGE, $chatId);
        \Core\library\Redis::del($cacheKey);
    }

    /**
     * Add date string to a message.
     *
     * @param array $message
     * @throws \Exception
     */
    public static function addDateString(&$message)
    {
        $timePeriod = self::getTimePeriods();
        $time = \DateTime::createFromFormat('Y-m-d H:i:s', $message['created']);
        $dateDiff = $time->diff(new \DateTime());
        $message['chat_time'] = $time->format('d/m/Y H:i');
        if ($dateDiff->y > 1) {
            $message['message_time'] = $dateDiff->y . ' ' . $timePeriod[self::TIME_YEARS_AGO];
            return;
        }
        if ($dateDiff->y == 1) {
            $message['message_time'] = $timePeriod[self::TIME_ONE_YEAR];
            return;
        }
        $message['chat_time'] = $time->format('d/m H:i');
        if ($dateDiff->m > 1) {
            $message['message_time'] = $dateDiff->m . ' ' . $timePeriod[self::TIME_MONTHS_AGO];
            return;
        }
        if ($dateDiff->m == 1) {
            $message['message_time'] = $timePeriod[self::TIME_ONE_MONTH];
            return;
        }
        $weeks = floor($dateDiff->d / 7);
        if ($weeks > 1) {
            $message['message_time'] = $weeks . ' ' . $timePeriod[self::TIME_WEEKS_AGO];
            return;
        }
        if ($weeks == 1) {
            $message['message_time'] = $timePeriod[self::TIME_ONE_WEEK];
            return;
        }
        if ($dateDiff->d > 1) {
            $message['message_time'] = $dateDiff->d . ' ' . $timePeriod[self::TIME_DAYS_AGO];
            return;
        }
        if ($dateDiff->d == 1) {
            $message['message_time'] = $timePeriod[self::TIME_YESTERDAY];
            $message['chat_time'] = $timePeriod[self::TIME_YESTERDAY_AT] . ' ' . $time->format('H:i');
            return;
        }
        $message['chat_time'] = $time->format('H:i');
        if ($dateDiff->h > 1) {
            $message['message_time'] = $dateDiff->h . ' ' . $timePeriod[self::TIME_HOURS_AGO];
            return;
        }
        if ($dateDiff->h == 1) {
            $message['message_time'] = $timePeriod[self::TIME_ONE_HOUR_AGO];
            return;
        }
        if ($dateDiff->i > 1) {
            $message['message_time'] = $dateDiff->i . ' ' . $timePeriod[self::TIME_MINUTES_AGO];
            return;
        }
        if ($dateDiff->i == 1) {
            $message['message_time'] = $timePeriod[self::TIME_ONE_MINUTE_AGO];
            return;
        }
        if ($dateDiff->s > 1) {
            $message['message_time']
                = $message['chat_time']
                = $dateDiff->s . ' ' . $timePeriod[self::TIME_SECONDS_AGO];
            return;
        }
        if ($dateDiff->s == 1) {
            $message['message_time']
                = $message['chat_time']
                = $timePeriod[self::TIME_ONE_SECOND_AGO];
            return;
        }
        $message['message_time']
            = $message['chat_time']
            = $timePeriod[self::TIME_JUST_NOW];
    }   
}
