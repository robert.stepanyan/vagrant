<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to interact with ad image related tasks. Such tasks might
 * but are not restricted to: add a new ad image, delete an ad image, etc.
 */

Namespace Modules\Humpchies;

final class AdTagModel{

    // CONSTANTS
    // =========
    const DEFAULT_DB_CONFIG       = '\Modules\Humpchies\Db\Default';
    const DEFAULT_DB_TABLE        = 'ads_tags';

    
    private static $redisKey      = "TagsAd-%d-%s";
    
    
    /**
     * Because this class is untended to be a singleton we block access to its constructor.
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}

    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}

    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}

    // add tags
    
    public static function create($ad_id, $tags = []) {
        
        if(!empty($tags)) {
            
            foreach($tags as $tag_id) {
            
                if(!is_numeric($tag_id))
                    continue;
                
                $fields = [
                    'ad_id' => $ad_id,
                    'tag_id' => $tag_id,
                ];
                
                $ad_tag = \Core\DBConn::insert(  self::DEFAULT_DB_CONFIG,
                    self::DEFAULT_DB_TABLE,
                    $fields);    
            }
        }
        return true;
    }
    
    
    // edit
    //
    public static function edit($ad_id, $tags = []) {
        
        // remove all existing tags and empty redis
        \Core\DBConn::delete(self::DEFAULT_DB_CONFIG, self::DEFAULT_DB_TABLE, "ad_id = {$ad_id}");
        
         \Core\library\Redis::del(sprintf(self::$redisKey, $ad_id, 'en'));    
         \Core\library\Redis::del(sprintf(self::$redisKey, $ad_id, 'fr'));    
        // recreate ad tags
        
        return self::create($ad_id, $tags);
        
    }
    
 
    public static function getAdTags($ad_id, $language = "fr") {
        
        //return \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG, 'SELECT id, `'. $language .'` FROM `tags WHERE 1 = 1', FALSE, FALSE, ['enable' => true]);
        //var_dump($redisKey);exit();
        $adTagsList = \Core\library\Redis::get(sprintf(self::$redisKey, $ad_id, $language));
        //$adTagsList = false;
        //var_dump($adTagsList);   
        if($adTagsList){
            return unserialize($adTagsList);
        }

        //var_dump('SELECT id, `'. $language .'` FROM tags WHERE 1');
        $adTagsList = \Core\Utils\Data::findBySQL( self::DEFAULT_DB_CONFIG,
            'SELECT tag_id, `'. $language .'` as name FROM ads_tags AT LEFT JOIN tags T ON T.id = AT.tag_id WHERE AT.ad_id = '.$ad_id,
            TRUE,
            FALSE,
            array('enable' => FALSE));
        
        \Core\library\Redis::set(sprintf(self::$redisKey, $ad_id, $language), serialize($adTagsList));   
     
        //var_dump($adTagsList);
        return $adTagsList;    
        
    }
}
?>