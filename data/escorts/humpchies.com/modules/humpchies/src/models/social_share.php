<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that allow us to interact with user related tasks. Such tasks might be
 * but are not restricted to: add a new user, retrieve a username, delete a
 * user, etc.
 */

Namespace Modules\Humpchies;

final class SocialShareModel{
    
    // CONSTANTS
    // =========
    const DEFAULT_DB_CONFIG                         = '\Modules\Humpchies\Db\Default';
    const DEFAULT_DB_TABLE                          = 'social_shares';
    
    const PLATFORM_FACEBOOK = 1;
    const PLATFORM_TWITTER 	= 2;
    
    const TYPE_LISTING 	= 1;
    const TYPE_VIEW 	= 2;

    public static $platforms 	= array('facebook' => self::PLATFORM_FACEBOOK , 'twitter' => self::PLATFORM_TWITTER);
    public static $types 		= array('listing' => self::TYPE_LISTING , 'view' => self::TYPE_VIEW);
    /**
    * Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}
   
  
    public static function create($platform, $type)
    {
    	$date = date("Y-m-d");
        $data = array( 
            'platform' => $platform,
            'type' => $type,
            'date' => $date,
        );

        $id = \Core\DBConn::insert(
            self::DEFAULT_DB_CONFIG,
            self::DEFAULT_DB_TABLE,
            $data
        );

        if($id !== FALSE){
            return $id;
        }

        return false;
    }

    public static function update($platform, $type)
    {
    	$affected_rows = \Core\DBConn::execute(  self::DEFAULT_DB_CONFIG,
    		"UPDATE social_shares SET count = count + 1 where platform = $platform AND type = $type AND date(date) = date(now())"
            );

        if($affected_rows){
            return true;
        }

        return false;
    }



    public static function updateOrCreate($platform, $type)
    {
    	$result = \Core\DBConn::selectRow(self::DEFAULT_DB_CONFIG, "
        select 1 from social_shares where platform = {$platform} and type = {$type} and date(date) = date(now()) 
        ");

        if(!empty($result)){
        	self::update($platform, $type);
        } else{
        	self::create($platform, $type);
        }

        return true;
    }
}   
?>