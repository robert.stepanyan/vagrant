<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * to support an API client.
 */

Namespace Modules\Humpchies;

final class ApiClient{
    
    /**
    * Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}
    
    private static function getData(            $uri, 
                                                $parameters){
                                            
        $api_call_context_params = \Core\Utils\HMAC::generateRequestContext(  \Core\Utils\Config::getConfigValue('\Application\Api::access_key_id'),
                                                                                    \Core\Utils\Config::getConfigValue('\Application\Api::secret_key'),
                                                                                    ($host = \Core\Utils\Config::getConfigValue('\Application\Api::host')),
                                                                                    $uri,
                                                                                    array(),
                                                                                    http_build_query($parameters),
                                                                                    array('Content-type' => 'application/x-www-form-urlencoded'));

        return \Core\Utils\Data::getURLData("http://$host$uri", $api_call_context_params, array('enable' => FALSE));
    }
    
    public static function authenticateUser(    $email,
                                                $password){
                                                    
        return self::getData(                   '/v1/userAuthenticate/', 
                                                array(  'email'             => $email,
                                                        'password'          => $password)); 
    }
    
    public static function getActiveAds(        $category_name = NULL, 
                                                $city_name = NULL, 
                                                $page_number = 1, 
                                                $page_size = \Modules\Humpchies\AdModel::DEFAULT_LIST_SIZE_PC){
                                                    
        return self::getData(                   '/v1/adListActive/', 
                                                array(  'category_name '    => $category_name,
                                                        'city_name'         => $city_name,
                                                        'page_number'       => $page_number,
                                                        'page_size'         => $page_size));                                               
    }
}
?>