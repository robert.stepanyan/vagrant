<?php

/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class is the default class in the humpchies project,
 * as such it also contains generic or not data structure specific methods.
 */
namespace Modules\Humpchies;

class PaymentController extends \Core\Controller
{
    static $paymentMethods= ['card','bitcoin','eth'];
    public static function index()
    {
        $data['currency'] = 'CAD';
        $data['prefilled'] = isset($_GET['prefilled']) ? $_GET['prefilled'] : null;

        // Payment Form Submitted => Validation
        if (isset($_SESSION['errors']) && !empty($_SESSION['errors'])) {
            $data['errors'] = $_SESSION['errors'];
            unset($_SESSION['errors']);
        }

        // Got Response From MMG => Setting the result
        if (isset($_SESSION['payment_result'])) {
            $data['payment_result'] = $_SESSION['payment_result'];
            unset($_SESSION['payment_result']);
        }


        if (is_numeric($data['prefilled']) && $data['prefilled'] > 0) {
            $data['prefilled'] = (float)$_GET['prefilled'];
        } else {
            $data['prefilled'] = 0;
        }

        if (isset($_SESSION['prefilled_money'])) {
            $data['prefilled'] = $_SESSION['prefilled_money'];
            unset($_SESSION['prefilled_money']);
        }

        self::$data = $data;

        // Sorry for bad versionning (bcs of Cloudfront)
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/payment_v4.css?v=3">
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile') . '/payment_v1.js?v=2"></script>');

    }

    public static function process()
    {
        // validating data
        $errors = array();

        // amount
        if (empty($_POST['amount'])) {
            $errors['amount'] = 'required';
            $amount = '';
        } else {
            $amount = stripslashes($_POST['amount']);
        }

        if (!is_numeric($amount) || $amount <= 0) {
            $errors['amount'] = 'invalid amount';
        }

        // lang
        $lang = (!empty($_POST['lang'])) ? $_POST['lang'] : '';

        // Showing form again
        if (count($errors) > 0) {
            $_SESSION['errors'] = $errors;
            self::redirect('/payment');
            die;
        }

        try {
            $mmbBill = new \Core\library\MmgBillAPI();
            $hosted_url = $mmbBill->getHostedPageUrl($amount, time(), 'https://www.humpchies.com/payment/callback');

            header('Location:' . $hosted_url);

        } catch (Exception $e) {
            self::addMessage('Unexpected error, please try again or contact Administrator if you see the error again', 'error');
            self::redirect('/payment');
        }

    }

    public static function callback()
    {
        $payment_result = [];
        
         // Additional headers
        $sHeaders = 'MIME-Version: 1.0' . "\r\n";
        $sHeaders .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $sHeaders .= 'From: Humpchies <webmaster@humpchies.com>' . "\r\n";

        \Core\Utils\SendMail::send("webmaster@humpchies.com", 'payment callback 1', print_R($_REQUEST, true), $sHeaders);  
        
        if (isset($_REQUEST['txn_status']) and $_REQUEST['txn_status'] == 'APPROVED') {
            $payment_result['status'] = $_REQUEST['txn_status'];
            $payment_result['merchant'] = $_REQUEST['mid'];
            $payment_result['ti_mmg'] = $_REQUEST['ti_mmg'];
            $payment_result['ti_mer'] = $_REQUEST['ti_mer'];

            if ($_REQUEST['mmg_errno'] != '0000')
                $payment_result['error'] = $_REQUEST['mmg_errno'];

        } else {
            $payment_result['status'] = 'REJECTED';
        }

        $_SESSION['payment_result'] = $payment_result;
        self::redirect('/payment');
    }
    
    public static function bitcoincallback()
    {
        $payment_result = [];
        
        //$raw = file_get_contents('php://input');
        //var_dump($raw);
        //var_dump($request);
        $orderID = intval($_GET['id']);
        $ch = curl_init();
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Set the url
        curl_setopt($ch, CURLOPT_URL,\Modules\Humpchies\PaymentModel::BITCOIN_CHECK_URL . \Modules\Humpchies\PaymentModel::BITCOIN_TOKEN . "/" . $orderID);
        // Execute
        $raw=curl_exec($ch);
        // Closing
        curl_close($ch);
        
        $request = json_decode($raw, true);
        
        $shoppping_card = \Modules\Humpchies\ShoppingcartModel::getbyID( \Core\Utils\Session::getCurrentUserID(), $request['orderId'] );
        //var_dump($shoppping_card);
        //die();
        if(empty($shoppping_card['category']))$shoppping_card['category']='Main';

        $unserilized_data = unserialize($shoppping_card['data']);
        $shoppping_card['city'] = $unserilized_data['premium_cities'];
        $shoppping_card['request'] = $request;
        $shoppping_card['credit'] = str_replace('cr_', '', $unserilized_data['credit']);
        $shoppping_card['is_bump'] = ($shoppping_card['ad_id'])?0 : 1;
        self::$data = $shoppping_card;
        $newad = \Modules\Humpchies\AdModel::getDetailsOfAd( $shoppping_card['ad_id'] );
        if($newad[0]['status'] == 'new'){
             header("Location: " . \Core\Utils\Navigation::urlFor('ad', 'confirmation', array('id' => $shoppping_card['ad_id']), false, true)); 
        }
    }

    public static function sc_callback()
    {
        $ti_mer = addslashes($_GET['ti_mer']);
        $is_bump = preg_match('|CR|', $_GET['ti_mer']);
        $success = ($_GET['txn_status'] == "APPROVED")? true : false;
        
        preg_match('|Z(.+?)Z(.+)|', $ti_mer, $parsed_data);
        $user_id  = $parsed_data[1];
        $hash  = $parsed_data[2];  

        $shoppping_card = \Modules\Humpchies\ShoppingcartModel::get( $user_id, $hash );
        if(empty($shoppping_card['category']))$shoppping_card['category']='Main';

        $unserilized_data = unserialize($shoppping_card['data']);
        $shoppping_card['city'] = $unserilized_data['premium_cities'];
        $shoppping_card['credit'] = str_replace('cr_', '', $unserilized_data['credit']);
        $shoppping_card['is_bump'] = $is_bump;
        self::$data = $shoppping_card;
        $newad = \Modules\Humpchies\AdModel::getDetailsOfAd( $shoppping_card['ad_id'] );
        $urlang =($unserilized_data['lang'] != 'fr-ca')?"":"/fr-ca";    
        //var_dump($newad[0]['status']);
        if($is_bump && $shoppping_card['ad_id']){
            if($success){
                header("Location: " . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname').$urlang . "/ad/confirmation-bump-success?id=".$shoppping_card['ad_id']); 
           
            }else{
                header("Location: " . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname').$urlang . "/ad/confirmation-bump-fail?id=".$shoppping_card['ad_id']); 
            }
            die();
        }else{ 
            if($newad[0]['status'] == 'new'){
               //die("Location: " . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname').$urlang . "/ad/confirmation-payment-success?id=".$shoppping_card['ad_id']);
                if($success){
                    header("Location: " . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'). $urlang . "/ad/confirmation-payment-success?id=".$shoppping_card['ad_id']);  
                }else{
                    header("Location: " . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'). $urlang . "/ad/confirmation-payment-fail?id=".$shoppping_card['ad_id']); 
                }
               die();
            } 
        }
        header("Location: " . $urlang . \Core\Utils\Navigation::urlFor('ad', 'confirmation', array('id' => $shoppping_card['ad_id']), false, true));
    }


    public static function send_result_email()
    {
        $payment_result = (isset($_POST['payment_result']) && !empty($_POST['payment_result'])) ? unserialize($_POST['payment_result']) : [];
        // $to = 'webmaster@humpchies.com';
        // $to = 'humpchies@gmail.com';
        $to = 'humpchiescom@gmail.com';
        
        $email_title = 'Payment on Humpchies';
        $payment_result['email'] = isset($_POST['email']) ? $_POST['email'] : '- - -';
        $payment_result['Id of the add'] = isset($_POST['id-of-add']) ? $_POST['id-of-add'] : '- - -';

        if (\Core\Utils\Session::getCurrentUserID()) {
            $payment_result['User'] = \Core\Utils\Session::getCurrentUserID();
        } else {
            $payment_result['User'] = 'Was not authenticated';
        }

        ob_start(null, 0, PHP_OUTPUT_HANDLER_CLEANABLE ^ PHP_OUTPUT_HANDLER_REMOVABLE);
        require_once(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Payment\Emails::payment_result', \Core\View::MODELESS));
        $parsed_email_contents = ob_get_clean();
        
        // Additional headers
        $sHeaders = 'MIME-Version: 1.0' . "\r\n";
        $sHeaders .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $sHeaders .= 'From: Humpchies <webmaster@humpchies.com>' . "\r\n";

        if(\Core\Utils\SendMail::send($to, $email_title, $parsed_email_contents, $sHeaders)) {   
          self::addMessage('[LABEL-EMAIL-SENT-SUCCESS]');
        }
        else{
          self::addMessage('[LABEL-EMAIL-SENT-FAIL]', 'error');
        }


        self::redirect(\Core\Utils\Navigation::urlFor('payment'));

    }

    public static function self_checkout(){

        \Core\Controller::setExtraHead('
                                        <link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/self_checkout_v3.css?v=195">
                                        <link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') .  '/new_modal.css?v=1.55">
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile') . '/self_checkout_v3.js?v=4.92"></script>');


        if (!$current_user = \Core\Utils\Session::getCurrentUserID())self::redirect(\Core\Utils\Navigation::urlFor('payment'));

        $data['ad_id'] = isset($_GET['ad_id']) ? $_GET['ad_id'] : null;
        
        $ad =  \Modules\Humpchies\AdModel::getDetailsOfAd($data['ad_id']);
       
        $data['category'] = isset($ad[0]['category']) ? $ad[0]['category'] : null;
        $data['city'] = isset($ad[0]['city']) ? $ad[0]['city']: null;

        $data['zone'] = \Modules\Humpchies\Utils::getDefinedZoneForCity($data['city']);
        
        $data['user_id'] = $current_user;
        $data['HumpchiesCategoryOptions'] = \Modules\Humpchies\CategoryModel::getSupportedCategories();
       // if($current_user == 63661){
//            $data['available_packages'] = \Modules\Humpchies\PaymentModel::get_unavailable_packages();    
//        }else{
            $data['available_packages'] = \Modules\Humpchies\PaymentModel::get_available_packages();
            
//        }
        $data['ongoing_package'] = \Modules\Humpchies\PaymentModel::get_ongoing_package($data['ad_id']);
        
        $data['lang'] = substr(\Core\Utils\Session::getLocale(),0,2);
               
        self::$data = $data;

    }

    public static function bumps(){

		if (!$current_user = \Core\Utils\Session::getCurrentUserID())self::redirect(\Core\Utils\Navigation::urlFor('payment'));
		
		\Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/self_checkout_v3.css?v=195">
									<script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
									<script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile') . '/bumps.js?v=196"></script>');
		$data['user_id'] = $current_user;
		$data['ad_id'] = isset($_GET['ad_id']) ? intval($_GET['ad_id']) : null;
		$data['every_min'] = 1;
		$data['for_hour'] = 1;
		$data['every_min_unit'] = '1 [LABEL-MINUTES]';
		$data['for_hour_unit'] =  '1 [LABEL-HOUR]';
		if(!\Modules\Humpchies\AdModel::canUpdateAd($current_user, $data['ad_id'])){
			self::redirect(\Core\Utils\Navigation::urlFor('ad/my-listings'));
		}
        self::setHeaderClass("has_breadcrumb");
		
		$data['bump_settings'] = \Modules\Humpchies\BumpsModel::getSettings($current_user, $data['ad_id'], \Modules\Humpchies\BumpsModel::BUMPS_SETTINGS_ACTIVE);
		$data['last_bump'] = \Modules\Humpchies\BumpsModel::getLastBump($current_user, $data['ad_id']);
		
		if($data['bump_settings']){
			$data['bump_settings']['bumps_left'] = $data['bump_settings']['bumps_total'] - $data['bump_settings']['bumps_count'];
			$data['every_min'] = $data['bump_settings']['interval_sec'] / 60;
			$data['for_hour'] = $data['bump_settings']['periode_sec'] / 3600;
			if(!$data['every_min']) $data['every_min'] = 1;
            
			$every_min_show_as_hour = \Modules\Humpchies\BumpsModel::$INTERVAL_OPTION_MINS_RENDERER[$data['every_min']]['show_as_hour'];
			if($every_min_show_as_hour){
				$data['every_min_unit'] = ($data['every_min'] / 60). ' [LABEL-HOUR]';
			}
			else{
				$data['every_min_unit'] = $data['every_min'] == 1 ?  $data['every_min'] . ' [LABEL-MINUTE]' : $data['every_min'] . ' [LABEL-MINUTES]';
			}
			
			$for_hour_show_as_day = \Modules\Humpchies\BumpsModel::$PERIODE_OPTION_HOURS_RENDERER[$data['for_hour']]['show_as_day'];
			if($for_hour_show_as_day){
				$data['for_hour_unit'] = $data['for_hour'] / 24 . ' [LABEL-DAYS]';
			}
			else{
				$data['for_hour_unit'] = $data['for_hour'] == 1 ?  $data['for_hour']. ' [LABEL-HOUR]' :  $data['for_hour']. ' [LABEL-HOURS]';
			}
		}
        
		self::$data = $data;
	}

    public static function bumps_purchase(){
		
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$errors = array();
            
			$amount = \Modules\Humpchies\BumpsModel::$CREDIT_OPTIONS[$_POST['credits']];
			try {
				if( !isset($amount) ){
					throw new \Exception('No such credit plan');
				}
                if(!in_Array($_POST['payment_method'], self::$paymentMethods)){
                    die(json_encode(array('status' => 'fail', 'errors' =>  'Payment Method Undefined')));
                }
			    $payment_method = $_POST['payment_method'];
                $amount = ($payment_method != "card" )? \Modules\Humpchies\PaymentModel::credit_price($amount) : $amount;
				$data = array(
					'hash' => base_convert(time(), 10, 36),
					'ip' => \Core\Utils\Device::getDeviceIp(),
					'user_id' => \Core\Utils\Session::getCurrentUserID(),
                    'ad_id'   => $_POST['ad_id'],
					'is_mobile' => \Core\Utils\Device::isMobile() ? 1 : 0,
                    'payment_processor' => $payment_method,
					'amount' => $amount
				);
		   
			
				$data['data'] = serialize(array('credit' => $_POST['credits'], 'lang' => \Core\Utils\Session::getLocale() )) ;
				$shopping_cart_id = \Modules\Humpchies\ShoppingcartModel::create($data);
                $lang = substr(\Core\Utils\Session::getLocale(),0,2);
                      
				if(empty($errors) && is_int($shopping_cart_id)){
                    if($payment_method == 'bitcoin'){
                      //die(json_encode(array('status' => $status, 'errors' =>  $errors, 'url' => $hosted_url)));
                      $hosted_url =  \Modules\Humpchies\PaymentModel::BITCOIN_URL . \Modules\Humpchies\PaymentModel::BITCOIN_TOKEN . "/" . $shopping_cart_id ;  
                      $status = 1;
                      $pp = 'bitcoin';
                      
                    }elseif($payment_method == 'eth'){
                      //die(json_encode(array('status' => $status, 'errors' =>  $errors, 'url' => $hosted_url)));
                      $hosted_url =  \Modules\Humpchies\PaymentModel::BITCOIN_URL . 'eth/'. \Modules\Humpchies\PaymentModel::BITCOIN_TOKEN . "/" . $shopping_cart_id ;  
                      $status = 1;
                      $pp = 'eth';
                      
                    }else{
                        
                        $status = 1;
                        $mmbBill = new \Core\library\MmgBillAPI();
                        if(\Core\Utils\Session::getCurrentUserID() == 63661){
                            $hosted_url = $mmbBill->getHostedPageUrl( $data['amount'],'CRZ' . $data['user_id']. 'Z' . $data['hash'], 'https://www.humpchies.com/payment/sc-callback', true );
                        }else{
                            $hosted_url = $mmbBill->getHostedPageUrl( $data['amount'],'CRZ' . $data['user_id']. 'Z' . $data['hash'], 'https://www.humpchies.com'.
                            \Core\Utils\Navigation::urlFor('payment', 'sc-callback') );
                        } 
                        $pp = 'card';   
                    }
                     
					
				}else{
					$status = 0;
				}
			} catch (\Exception $e) {
				$errors[] = $e->getMessage();
				//$errors[] = "Unexpected error, please try again or contact Administrator if you see the error again";
			}	

			die(json_encode(array('status' => $status, 'errors' =>  $errors, 'pp' => $pp, 'a' => $data['amount'] , 'lang' => $lang, 'url' => $hosted_url)));
		}
		else{
            
             $lang = substr(\Core\Utils\Session::getLocale(),0,2);
            //die(\Core\Utils\Session::getLocale());
			if (!$current_user = \Core\Utils\Session::getCurrentUserID())self::redirect(\Core\Utils\Navigation::urlFor('payment'));
			$data['user_id'] = $current_user;
            $data['current_credits'] = \Modules\Humpchies\UserModel::getCredits($current_user);
			$data['lang'] = substr(\Core\Utils\Session::getLocale(),0,2);
						
			\Core\Controller::setExtraHead('
                <link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/self_checkout_v3.css?v=195">
                <link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') . '/new_modal.css?v=1.55">
				<script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
				<script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile') . '/bumps.js?v=197"></script>');
			self::$data = $data;
		}
    }

    public static function one_time_bump(){
		
		$errors = array();
		
		try{
			if (!$current_user = \Core\Utils\Session::getCurrentUserID()){
				$errors[] = 'Your session has expired please log in again';
			}

			$ads_id = intval($_POST['ad_id']);
			
			if(!\Modules\Humpchies\AdModel::canUpdateAd($current_user, $ads_id)){
				throw new \Exception('Unexpected error please contact support');
			}
			$credits = \Modules\Humpchies\UserModel::getCredits($current_user);
			
			if($credits < \Modules\Humpchies\BumpsModel::BUMP_COST){
				throw new \Exception('[ERROR-CREDITS-NOT-ENOUGHT]');
			}
            $ad_details = \Modules\Humpchies\AdModel::getDetailsOfSimpleAd($ads_id);
            
            if($ad_details[0]["status"] != 'new'){
			    $new_id = \Modules\Humpchies\BumpsModel::addBump($current_user, $ads_id, 1 );
			
                if($new_id) {
                    // send mail
                    // =============================================    
                    
                    
                    if($ad_details) {
                        
                        $category   = \Modules\Humpchies\CategoryModel::getCategoryLabel($ad_details[0]['category'], \Core\Utils\Session::getLocale());
                        $city       = $ad_details[0]['city'];
                        $ad_id      = $ads_id;
                        $list_url   = \Modules\Humpchies\AdController::getDetailsURL($ad_details[0]['category'], \Core\Utils\Text::stringToUrl($ad_details[0]['city']), $ad_details[0]['id'], $ad_details[0]['title']);
                        $email_title = NULL;
                        $user =  \Core\Utils\Session::getAuthenticatedSessionValues();
                        
                        ob_start(NULL, 0, PHP_OUTPUT_HANDLER_CLEANABLE ^ PHP_OUTPUT_HANDLER_REMOVABLE);
                        require_once(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Emails::bump', \Core\View::LOCALIZED_MODE));
                        $parsed_email_contents = ob_get_clean();
                        \Core\Utils\SendMail::send($user['email'], $email_title, $parsed_email_contents, true);
                    
                        
                       // if($user['username'] == 'testme') {
    //                        \Core\library\log::debug($user['email'], "SEND MAILT TO ");
    //                        \Core\Utils\SendMail::send('florentina.opris@cmdnetwork.ro', $email_title, $parsed_email_contents, true); // send to me as well 
    //                    }
                        
                    }
                        
                }
            }else{
                \Modules\Humpchies\BumpsModel::cancelSettings($ads_id);
                $data = array(
                        'user_id' => $current_user,
                        'ads_id' => $ads_id,
                        'interval_sec' => ($interval * 60),
                        'periode_sec' =>  ($periode * 3600),
                        'status' => \Modules\Humpchies\BumpsModel::BUMPS_SETTINGS_ACTIVE,
                        'bumps_total' => 1,
                        'bumps_count' => 0, // Instant Bump 
                        'update_date' => date("Y-m-d H:i:s")
                );
                $bumps_settings_id = \Modules\Humpchies\BumpsModel::addSettings($data);
            }
			
		}
		catch (\Exception $e) {
			$errors[] = $e->getMessage();
			die(json_encode(array('status' => 0, 'errors' =>  $errors)));
		}
		
        die(json_encode(array('status' => 1, "ad_status"=> $ad_details[0]['status']) ));
     }

    public static function stop_bumping(){
		$errors = array();
		
		try{
			if (!$current_user = \Core\Utils\Session::getCurrentUserID()){
				$errors[] = 'Your session has expired please log in again';
			}

			$ads_id = intval($_POST['ad_id']);
			$interval = intval($_POST['bump_time']);
			$periode = intval($_POST['for']) ;

			if(!\Modules\Humpchies\AdModel::canUpdateAd($current_user, $ads_id)){
				throw new \Exception('Unexpected error');
			}
					
			\Modules\Humpchies\BumpsModel::cancelSettings($ads_id);
			
		}
		catch (\Exception $e) {
			$errors[] = $e->getMessage();
			die(json_encode(array('status' => 0, 'errors' =>  $errors)));
		}
		
        die(json_encode(array('status' => 1) ));
    }

    public static function bump_save_settings(){
		$errors = array();
		
		try{
			if (!$current_user = \Core\Utils\Session::getCurrentUserID()){
				$errors[] = 'Your session has expired please log in again';
			}

			$ads_id = intval($_POST['ad_id']);
			$interval = intval($_POST['bump_time']);
			$periode = intval($_POST['for']) ;

			if(!\Modules\Humpchies\AdModel::canUpdateAd($current_user, $ads_id)){
                
				throw new \Exception('Unexpected error');
			}
			if(!in_array($interval, \Modules\Humpchies\BumpsModel::$INTERVAL_OPTION_MINS) || !in_array($periode, \Modules\Humpchies\BumpsModel::$PERIODE_OPTION_HOURS)){
                            die(json_encode(array('status' => 2, 'errors' =>  ['Wrong options selected'])));
			}
			$total_credits = ( $periode * 60 ) / $interval + 1;
			$credits = \Modules\Humpchies\UserModel::getCredits($current_user);
			
            $difference = $total_credits - $credits;
            $ad_details = \Modules\Humpchies\AdModel::getDetailsOfSimpleAd($ads_id);
            $instantBump =($ad_details[0]['status'] == 'new')? 0 : 1; 
			if($credits < $total_credits){            
                $message = (\Core\Utils\Session::getLocale() == 'fr-ca')? 'Vous avez besoin de '. $difference . '  cr&eacute;dits en plus pour acheter cette option de bumps. S\'il vous plait acheter un nouveau paquet de cr&eacute;dits.' : 'You need '. $difference . ' more credits to get this feature. Please buy more through our shop!';
                if(!$instantBump){
                    
                        \Modules\Humpchies\BumpsModel::cancelSettings($ads_id);
                        $data = array(
                        'user_id' => $current_user,
                        'ads_id' => $ads_id,
                        'interval_sec' => ($interval * 60),
                        'periode_sec' =>  ($periode * 3600),
                        'status' => \Modules\Humpchies\BumpsModel::BUMPS_SETTINGS_ACTIVE,
                        'bumps_total' => $total_credits,
                        'bumps_count' => $instantBump, // Instant Bump 
                        'update_date' => date("Y-m-d H:i:s")
                    );
                    $bumps_settings_id = \Modules\Humpchies\BumpsModel::addSettings($data);
                    $pack = \Modules\Humpchies\BumpsModel::getPackFromPack($difference);
                    die(json_encode(array('status' => 3, 'errors' => [$message], 'credits' => $difference, 'pack'=> $pack['credits'], 'amount' => $pack['price'] )));
                }
                
                die(json_encode(array('status' => 3, 'errors' => [$message], 'credits' => $difference)));
			}
            \Modules\Humpchies\BumpsModel::cancelSettings($ads_id);
            
			$data = array(
				'user_id' => $current_user,
				'ads_id' => $ads_id,
				'interval_sec' => ($interval * 60),
				'periode_sec' =>  ($periode * 3600),
				'status' => \Modules\Humpchies\BumpsModel::BUMPS_SETTINGS_ACTIVE,
				'bumps_total' => $total_credits,
				'bumps_count' => $instantBump, // Instant Bump 
				'update_date' => date("Y-m-d H:i:s")
			);
			$bumps_settings_id = \Modules\Humpchies\BumpsModel::addSettings($data);
			//instant Bump 
            if($ad_details[0]["status"] != 'new'){
			    $new_id = \Modules\Humpchies\BumpsModel::addBump($current_user, $ads_id, 0, $bumps_settings_id );
            }
            if($new_id) {
                // send mail
                // =============================================    
                
                if($ad_details) {

                    $category   = \Modules\Humpchies\CategoryModel::getCategoryLabel($ad_details[0]['category'], \Core\Utils\Session::getLocale()) ;
                    $city       = $ad_details[0]['city'];
                    $ad_id      = $ads_id;
                    $list_url   = \Modules\Humpchies\AdController::getDetailsURL($ad_details[0]['category'], \Core\Utils\Text::stringToUrl($ad_details[0]['city']), $ad_details[0]['id'], $ad_details[0]['title']);
                    $email_title = NULL;
                    $user =  \Core\Utils\Session::getAuthenticatedSessionValues();
                    
                    ob_start(NULL, 0, PHP_OUTPUT_HANDLER_CLEANABLE ^ PHP_OUTPUT_HANDLER_REMOVABLE);
                    require_once(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Emails::bump', \Core\View::LOCALIZED_MODE));
                    $parsed_email_contents = ob_get_clean();
                    \Core\Utils\SendMail::send($user['email'], $email_title, $parsed_email_contents, true);
                    
                }   
            }
		}
		catch (\Exception $e) {
			$errors[] = $e->getMessage();
			die(json_encode(array('status' => 0, 'errors' =>  $errors)));
		}
        die(json_encode(array('status' => 1, "ad_status"=> $ad_details[0]['status']) ));
    }

    public static function shoping_cart($return = false) {

        if ($_SERVER['REQUEST_METHOD'] !== 'POST') die();
        $errors = array();
        $data = array();
     
        $data['hash'] = base_convert(time(), 10, 36);
        $data['ip'] = \Core\Utils\Device::getDeviceIp();

        $data['ad_id'] = intval($_POST['ad_id']);
        $data['user_id'] = intval($_POST['user_id']);
       
        $data['category'] = addslashes(str_replace('mtl_', '', $_POST['category'] ));
        $data['is_mobile'] =  ( \Core\Utils\Device::isMobile() ? 1 : 0 ) ;
        $lang = substr(\Core\Utils\Session::getLocale(),0,2);
		
		
		if(!\Modules\Humpchies\CategoryModel::isPaymentCategoryValid($data['category'])){
			$errors['package'] = 'Category unavailable';
		}
				
        // check package
        $available_packages = \Modules\Humpchies\PaymentModel::get_available_packages_ids();
        if(!in_array($_POST['package'], array_column($available_packages, 'id'))){
           $errors['package'] = 'Package unavailable';
        }else{
            $data['package_id'] = $_POST['package'];
        }

        // Amount
        $data['amount'] = \Modules\Humpchies\PaymentModel::get_packages_price( $data['package_id'], $data['category'] );
        $data['amount'] = $data['amount']['price']; 
        if($_POST['payment_method'] == 'bitcoin'){
            $data['amount'] =  \Modules\Humpchies\PaymentModel::get_bitcoin_price($data['package_id']); //\Modules\Humpchies\PaymentModel::BITCOIN_MULTIPLIER;
            $data['payment_processor'] = 'bitcoin'; 
        }
        if($_POST['payment_method'] == 'eth'){
            $data['amount'] =  \Modules\Humpchies\PaymentModel::get_bitcoin_price($data['package_id']); //\Modules\Humpchies\PaymentModel::BITCOIN_MULTIPLIER;
            $data['payment_processor'] = 'eth'; 
            
        }

		if( $data['amount'] <= 0){
			$errors['package'] = 'Amount problem';
		}
		
        // City and Zone 
        $ad = \Modules\Humpchies\AdModel::getDetailsOfAd( $data['ad_id'] );
      
        if( $ad[0]['city']  ){
           $data['city_id'] = $ad[0]['city_id'];
        }else{
           $errors['package'] = 'Package City problem';
        }

        if( !$ad[0]['zone_name']  ){
           $ad[0]['zone_name'] = '';
        }
        @file_get_contents('https://www.google-analytics.com/collect/?v=1&t=event&tid=UA-17889379-2&cid=d1e189e9-2b53-47b5-a7c0-bdab314dc4c7&el=payment_' . $data['amount'] );
     
        $data['data'] = serialize(array('premium_cities' => $ad[0]['city'], 'package' => $_POST['package'], 'lang' => \Core\Utils\Session::getLocale(), 'zone_name' => $ad[0]['zone_name'] )) ;
        $new_ad_id = \Modules\Humpchies\ShoppingcartModel::create($data);

        if(empty($errors) && is_int($new_ad_id)){
            $status = 1;
			try {
                 if($_POST['payment_method'] == 'bitcoin'){
                      //die(json_encode(array('status' => $status, 'errors' =>  $errors, 'url' => $hosted_url)));
                      $hosted_url = \Modules\Humpchies\PaymentModel::BITCOIN_URL. \Modules\Humpchies\PaymentModel::BITCOIN_TOKEN . "/" . $new_ad_id ;  
                      $status = 1;
                      $pp = 'bitcoin';
                      
                 }elseif($_POST['payment_method'] == 'eth'){
                      //die(json_encode(array('status' => $status, 'errors' =>  $errors, 'url' => $hosted_url)));
                      $hosted_url = \Modules\Humpchies\PaymentModel::BITCOIN_URL . 'eth/' . \Modules\Humpchies\PaymentModel::BITCOIN_TOKEN . "/" . $new_ad_id ;  
                      $status = 1;
                      $pp = 'eth';
                      
                 }else{
                    $pp = 'card';
                    $mmbBill = new \Core\library\MmgBillAPI();
                    
                    if(\Core\Utils\Session::getCurrentUserID() == 63661){
                        $hosted_url = $mmbBill->getHostedPageUrl( $data['amount'],'SCZ' . $data['user_id']. 'Z' . $data['hash'], 'https://www.humpchies.com/payment/sc-callback', true );    
                    }else{
                        $hosted_url = $mmbBill->getHostedPageUrl( $data['amount'],'SCZ' . $data['user_id']. 'Z' . $data['hash'], 'https://www.humpchies.com/payment/sc-callback' );
                    }
                 }
                            
            } catch (\Exception $e) {
                self::addMessage('Unexpected error, please try again or contact Administrator if you see the error again', 'error');
                self::redirect('/payment');
            }
        }else{
            $status = 0;
        }
        if ($return){
            return array('status' => $status, 'errors' =>  $errors, 'url' => $hosted_url);
        }
        die(json_encode(array('status' => $status, 'errors' =>  $errors,  'pp' => $pp, 'a' => $data['amount'] , 'lang' => $lang, 'url' => $hosted_url)));
    }

	public static function get_package_price() {

        if ($_SERVER['REQUEST_METHOD'] !== 'POST') die('unauthorized request');
        if(!intval($_POST['package'])){die('numeric');}

        $package_id = $_POST['package'];
        $category = addslashes(str_replace('mtl_', '', $_POST['category'] ));
				
		if(!\Modules\Humpchies\CategoryModel::isPaymentCategoryValid($category)){
			die('category invalid');
		}
		
        $price = \Modules\Humpchies\PaymentModel::get_packages_price($package_id, $category);
        if($price === false){
            $price['price'] = 0;
        }
        if($_POST['payment_method'] != 'card'){
            $price['price'] = \Modules\Humpchies\PaymentModel::get_bitcoin_price($package_id);  //*= \Modules\Humpchies\PaymentModel::BITCOIN_MULTIPLIER; 
        }
        
        die(json_encode(array('status' => 1, 'price'=> ceil($price['price'])) ));
	}
    
    public static function bitcoin_info(){
       
        \Core\Controller::setExtraHead('
                                        <link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/self_checkout_v3.css?v=195">
                                        <link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') .  '/new_modal.css?v=1.55">
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile') . '/self_checkout_v3.js?v=4.8"></script>');
        //die("BULA");
        $data['lang'] = substr(\Core\Utils\Session::getLocale(),0,2);
               
        self::$data = $data;
    }
    
    public static function mid(){
       //mid=M1601&ts=d&ti_mer=SCZ63661Zqw6ody&txn_type=SALE&cu_num=124&amc=2400&surl=https://www.humpchies.com/payment/sc-callback&c2=humpchies.com&sh=ec9cee25f08c40405bc3af4575d6b2893de161354e40f1b9999bb33ef3732636
       //$mid = $_GET['mid'];
//       $ts= $_GET['ts'];
//       $ts= $_GET['ts'];
//       //die("BULAAAAA");
        if(\Core\Utils\Session::getCurrentUserID() != 63661){
            die("NOPE");
        }
        $data['ptoken'] = $_GET['sh']; 
        $data['mer'] = $_GET['ti_mer']; 
        $data['amc'] = $_GET['amc']; 
        $data['transID'] = substr($_GET['sh'], -8); 
        self::$data = $data;
    }
}

?>
