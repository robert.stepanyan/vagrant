<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class is the default class in the humpchies project,
 * as such it also contains generic or not data structure specific methods.
 */

Namespace Modules\Humpchies;

class GenericController extends \Core\Controller{

    /**
    * This is the site's privacy policy
    * 
    * @returns NULL 
    */
    public static function privacyPolicy()
    {
        // Set some SEO values
        // ===================
        self::setTitle('[TITLE-PRIVACY-POLICY]');                       // Title
        self::setKeywords('[KEYWORDS-PRIVACY-POLICY]');                 // Keywords
        self::setDescription('[DESCRIPTION-PRIVACY-POLICY]');           // Description
        self::setHeaderClass("has_breadcrumb");
    }
    
    /**
    * This is the terms and conditions page of the site
    * 
    * @returns NULL
    */
    public static function termsAndConditions()
    {
        // Set some SEO values
        // ===================
        self::setTitle('[TITLE-TERMS-AND-CONDITIONS]');                 // Title
        self::setKeywords('[KEYWORDS-TERMS-AND-CONDITIONS]');           // Keywords
        self::setDescription('[DESCRIPTION-TERMS-AND-CONDITIONS]');     // Description 
        self::setHeaderClass("has_breadcrumb");
    }
    
    /**
    * This is the advertising information page.
    * 
    * @returns NULL
    */
    public static function advertising()
    {
        // Set some SEO values
        // ===================
        self::setTitle('[TITLE-ADVERTISING]');                          // Title
        self::setKeywords('[KEYWORDS-ADVERTISING]');                    // Keywords
        self::setDescription('[DESCRIPTION-ADVERTISING]');              // Description
        self::setHeaderClass("has_breadcrumb");
    }
    
    /**
    * This is the captcha image used all over the site.
    * It is in fact a wrapper around:
    * 
    * \Core\Utils\Security::outputCaptcha()
    * 
    * @returns NULL
    */
    public static function captcha()
    {
        // call the captcha builder... 
        \Core\Utils\Security::outputCaptcha();
    }
    
    /**
    * This is the HTML site map of the site
    * 
    * @returns NULL
    */
    public static function sitemap()
    {
        // Set some SEO values
        // ===================
        self::setTitle('[TITLE-SITEMAP]');                              // Title
        self::setKeywords('[KEYWORDS-SITEMAP]');                        // Keywords
        self::setDescription('[DESCRIPTION-SITEMAP]');                  // Description
        
        // Set non-dynamic URLs
        self::$data['root_url'] = \Core\Utils\Navigation::urlFor('/');
        self::$data['terms_and_conditions_url'] = \Core\Utils\Navigation::urlFor('terms-and-conditions');
        self::$data['privacy_policy_url'] = \Core\Utils\Navigation::urlFor('privacy-policy');
        self::$data['advertising_url'] = \Core\Utils\Navigation::urlFor('advertising');
        self::$data['contact_us_url'] = \Core\Utils\Navigation::urlFor('contact-us');
        self::$data['rss_url'] = \Core\Utils\Navigation::urlFor('rss');
        self::$data['login_url'] = \Core\Utils\Navigation::urlFor('user', 'login');
        self::$data['signup_url'] = \Core\Utils\Navigation::urlFor('user', 'signup');
        self::$data['request_password_reset_url'] = \Core\Utils\Navigation::urlFor('user', 'request-password-reset');
        
        // Grab a list of all categories and cities with ads
        self::$data['categories_and_cities'] = \Modules\Humpchies\AdModel::getListOfCategoriesAndCitiesWithAds();
        
        // Create an accumulator... we'll use it to clean up the list from the previous statement...
        self::$data['categories_cities'] = array();
        
        // Create another accumulator... We'll use it to store a list and details about cities with ads...
        self::$data['cities_with_ads'] = array();
        
        // Loop through each category and city
        foreach(self::$data['categories_and_cities'] as $record_row)
        {
            // Grab the city name... to later on use it to build some of the values for each combination of city and category.
            self::$data['cities_with_ads'][] = $record_row['city_name'] = (substr(\Core\Utils\Session::getLocale(), 0, 2) === 'en' ? \Core\Utils\Text::translateDiacriaticsAndLigatures($record_row['city']) : \Core\Utils\Security::htmlEncodeText($record_row['city']));
            
            /**
            * These are the actual value we'll be using to build the GUI... we separate them 
            * into categories... then each element in the category has the city name, the url
            * which targets            
            */
            self::$data['categories_cities'][$record_row['category']][] = array('city_name' => $record_row['city_name'],
                                                                                'city_list_url' => \Modules\Humpchies\AdController::getListURL($record_row['category'], \Core\Utils\Text::stringToUrl($record_row['city'])),
                                                                                'city_list_href_title' => \Modules\Humpchies\AdController::getListHrefTitle($record_row['category'], $record_row['city_name']));
        }

        // tag memory used for garbage collection... 
        unset(self::$data['categories_and_cities']);
        
        // Compress the list of cities with ads: remove duplicates.
        self::$data['cities_with_ads'] = array_unique(self::$data['cities_with_ads']);
        
        // Loop though each city with ads and build the items needed to build the GUI
        foreach(self::$data['cities_with_ads'] as $city_name)
        {
            self::$data['cities'][] = array('city_name' => $city_name,
                                            'city_list_url' => \Modules\Humpchies\AdController::getListURL(NULL, \Core\Utils\Text::stringToUrl($city_name)),
                                            'city_list_href_title' => \Modules\Humpchies\AdController::getListHrefTitle(NULL, $city_name));
        }
        
        // tag memory used for garbage collection... 
        unset(self::$data['cities_with_ads']);
    }
    
    /**
    * This is the site's XML sitemap for SEO
    * 
    * @returns NULL
    */
    public static function xmlSitemap()
    {
        self::$data['categories_and_cities'] = \Modules\Humpchies\AdModel::getListOfCategoriesAndCitiesWithAds();
        self::$data['urls'] = array();
        
        foreach(self::$data['categories_and_cities'] as $record_row)     
            self::$data['urls'][$record_row['category']][] = \Modules\Humpchies\AdController::getListURL($record_row['category'], \Core\Utils\Text::stringToUrl($record_row['city']));

        unset(self::$data['categories_and_cities']);
        
        header("Content-Type: text/xml;charset=ISO-8859-1");    
    }

    /**
     * The robots file
     */
    public static function robots()
    {
        $protocol = \Core\Utils\Device::getProtocol();
        $hostname = \Core\Utils\Config::getConfigValue('\Application\Settings::hostname');

        echo "User-agent: *\n";
        echo "sitemap: $protocol$hostname/sitemap.xml\n";
	echo "Disallow: /user\n";
	echo "Disallow: /fr-ca/user/login\n";
	echo "Disallow: /contact-us\n";
	echo "Disallow: /site-map\n";
	echo "Disallow: /privacy-policy\n";
	echo "Disallow: /terms-and-conditions\n";
        echo "Disallow: /fr-ca/contact-us\n";
        echo "Disallow: /fr-ca/terms-and-conditions\n";
        echo "Disallow: /fr-ca/privacy-policy\n";
        echo "Disallow: /fr-ca/advertising\n";
        echo "Disallow: /advertising\n";
		
        header("Content-Type: text/plain; charset=utf-8", true);
        header('Expires: Thu, 19 Nov 1981 08:52:00 GMT', true);
        header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0', true);
        header('Pragma: no-cache', true);
    }
    
    public static function bitcoin_info(){
       
        \Core\Controller::setExtraHead('
                                        <link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/self_checkout_v3.css?v=195">
                                        <link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') .  '/new_modal.css?v=1.55">
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile') . '/self_checkout_v3.js?v=4.8"></script>');
        //die("BULA");
        $data['lang'] = substr(\Core\Utils\Session::getLocale(),0,2);
               
        self::$data = $data;
    }
    public static function ennews1(){
          echo file_get_contents("/home/shadybyte/web/viper/modules/humpchies/views/generic/newsletter/news1-en.html");
          die();
    }
    public static function frnews1(){
          echo file_get_contents("/home/shadybyte/web/viper/modules/humpchies/views/generic/newsletter/news1-fr.html");
          die();
    }
}
