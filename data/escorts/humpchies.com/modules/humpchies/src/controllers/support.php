<?php

Namespace Modules\Humpchies;

/**
 * Class SupportController
 * @package Modules\Humpchies
 *
 *
 */
class SupportController extends \Core\Controller{

    const ERROR_MESSAGE_IS_EMPTY = "Modules.Humpchies.SupportController.MessageIsEmpty";
    const ERROR_MESSAGE_IS_TOO_SHORT = "Modules.Humpchies.SupportController.MessageIsTooShort";
    const ERROR_MESSAGE_IS_TOO_LONG = "Modules.Humpchies.SupportController.MessageIsTooLong";

    const ERROR_SUBJECT_IS_EMPTY = "Modules.Humpchies.SupportController.SubjectIsEmpty";
    const ERROR_SUBJECT_IS_TOO_SHORT = "Modules.Humpchies.SupportController.SubjectIsTooShort";
    const ERROR_SUBJECT_IS_TOO_LONG = "Modules.Humpchies.SupportController.SubjectIsTooLong";

    const ERROR_IMAGE_ERROR_UNHANDLED = 'Modules.Humpchies.SupportController.ErrorUnhandled';
    const ERROR_IMAGE_SIZE_IS_TOO_BIG = 'Modules.Humpchies.SupportController.ErrorImageSizeIsTooBig';
    const ERROR_IMAGE_TYPE_IS_INVALID = 'Modules.Humpchies.SupportController.ErrorImageTypeIsInvalid';

    const ERROR_UNKNOWN = 'Modules.Humpchies.SupportController.ErrorUnknown';

    const NO_ERROR = 'Modules.Humpchies.SupportController.NoError';

    /**
     * @var array $AllowedExt
     */
    public static $AllowedExt = array(
        'jpg', 'jpeg', 'jpe', 'gif', 'png', 'bmp'
    );

    /**
     * @var array $AllowedTypes
     */
    public static $AllowedTypes = array(
        'image/jpg', 'image/jpeg', 'image/jpe', 'image/gif', 'image/png', 'image/bmp'
    );

    
    static $shortMonths = [
        'fr-ca' => ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Août', 'Sept', 'Oct', 'Nov', 'Déc'],
        'en-ca' => ['Jan', 'Feb', 'Mar', 'Aps', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    ];



    public static function index_ma() {
        self::disableCache();
        $user_id = \Core\Utils\Session::getCurrentUserID();
        if (!$user_id){
            self::redirect('/users/login');
        }
        \Core\Controller::setExtraHead('
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'pc/support.js?v=2.2"></script>'
        );
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="'
            .
            \Modules\Humpchies\Utils::getCDNPath('css')
            .
            (\Core\Utils\Device::isPC() ? 'pc' : ('mobile/touch'))
            .
            '/support.css?v=8.9">');

        // Set some page values...
        self::setTitle('[LABEL-POST-AD]');
        self::setDescription('[LABEL-POST-AD]');

        self::$data['openTickets'] = \Modules\Humpchies\SupportModel::getOpenTickets($user_id);
        self::prepareTicketsForUI(self::$data['openTickets']);
        self::$data['closedTickets'] = \Modules\Humpchies\SupportModel::getClosedTickets($user_id);
        self::prepareTicketsForUI(self::$data['closedTickets']);
        
        
        self::setHeaderClass("has_breadcrumb");
        
        
    }

    public static function create_ma() {
        self::disableCache(); 
        $user_id = \Core\Utils\Session::getCurrentUserID();
        if (!$user_id){
            self::redirect(\Core\Utils\Navigation::urlFor('user', 'login'));
        }

        \Core\Controller::setExtraHead('
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'pc/support.js?v=2.2"></script>'
        );

        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="'
            .
            \Modules\Humpchies\Utils::getCDNPath('css')
            .
            (\Core\Utils\Device::isPC() ? 'pc' : ('mobile/touch'))
            .
            '/support.css?v=8.9">');

        // Set some page values...
        self::setTitle('[LABEL-POST-AD]');                      // Title
        self::setDescription('[LABEL-POST-AD]');                // Description

        self::setHeaderClass("has_breadcrumb");
        
        if (!empty($_POST)){
            self::$data = $data = array(
                'subject'=> !empty($_POST['subject']) ? trim($_POST['subject']) : '',
                'message'=>!empty($_POST['message']) ? trim($_POST['message']) : '',
            );
            $_POST = array();
            $images = self::getUploadedFiles('attached');

            $validity = self::validateNew($data, $images);
            switch ($validity){
                case self::NO_ERROR:
                    $data['status'] = \Modules\Humpchies\SupportModel::STATUS_TICKET_OPENED;
                    $data['status_progress'] = \Modules\Humpchies\SupportModel::STATUS_PROGRESS_NEW_UNREAD;
                    $data['user_id'] = $user_id;
                    $attached = array();
                    if (!empty($images)){

                        foreach ($images as $image){
                            $row  = \Core\Utils\Image::save($image['tmp_name'], 'attached', $image['ext']);
                            if (!is_array($row)){
                                self::addMessage("[ERROR_IMAGE_ERROR_UNHANDLED]", 'error');
                                return false;
                            }
                            $row['file_name'] = $image['name'];
                            array_push($attached, $row);
                        }
                    }

                    $data['attached'] = $attached;

                    $ticketResult = \Modules\Humpchies\SupportModel::create($data);
                    if (is_numeric($ticketResult)){
                        self::addMessage("[SUCCESS-TICKET-CREATED]");
                        self::redirect(\Core\Utils\Navigation::urlFor('support'));
                    }else{
                        self::addMessage("[ERROR-UNHANDLED]");
                    }
                    break;
                case self::ERROR_MESSAGE_IS_EMPTY:
                    self::addMessage("[ERROR-SUPPORT-TICKET-MESSAGE-IS-EMPTY]", 'error');
                    break;
                case self::ERROR_MESSAGE_IS_TOO_SHORT:
                    self::addMessage("[ERROR-SUPPORT-TICKET-MESSAGE-IS-TOO-SHORT]", 'error');
                    break;
                case self::ERROR_MESSAGE_IS_TOO_LONG:
                    self::addMessage("[ERROR-SUPPORT-TICKET-MESSAGE-IS-TOO-LONG]", 'error');
                    break;
                case self::ERROR_SUBJECT_IS_EMPTY:
                    self::addMessage("[ERROR-SUPPORT-TICKET-SUBJECT-IS-EMPTY]", 'error');
                    break;
                case self::ERROR_SUBJECT_IS_TOO_SHORT:
                    self::addMessage("[ERROR-SUPPORT-TICKET-SUBJECT-IS-TOO-SHORT]", 'error');
                    break;
                case self::ERROR_SUBJECT_IS_TOO_LONG:
                    self::addMessage("[ERROR-SUPPORT-TICKET-SUBJECT-IS-TOO-LONG]", 'error');
                    break;
                case self::ERROR_IMAGE_TYPE_IS_INVALID:
                    self::addMessage("[ERROR-SUPPORT-TICKET-INVALID-FILE-TYPE]", 'error');
                    break;
                case self::ERROR_IMAGE_SIZE_IS_TOO_BIG:
                    self::addMessage("[ERROR-SUPPORT-TICKET-FILE-IS-BIG]", 'error');
                    break;
                case self::ERROR_IMAGE_ERROR_UNHANDLED:
                    self::addMessage("[ERROR_IMAGE_ERROR_UNHANDLED]", 'error');
                    break;
                case self::ERROR_UNKNOWN:
                    self::addMessage("[ERROR-UNHANDLED]", 'error');
                    break;
                default:
                    self::addMessage("[ERROR-SOMETHING-WENT-WRONG]", 'error');
                    break;
            }
        }
    }

    public static function ticket_ma() {
        self::disableCache();
        $user_id = \Core\Utils\Session::getCurrentUserID();
        if (!$user_id){
            self::redirect(\Core\Utils\Navigation::urlFor('user', 'login'));
        }

        $ticketId = !empty($_GET['id']) ? intval($_GET['id']) : null;

        if ($ticketId < 1){
            self::addMessage("please specify ticket", 'error');
            self::redirect(\Core\Utils\Navigation::urlFor('support'));
        }

        $ticket = \Modules\Humpchies\SupportModel::getTicketById($ticketId, $user_id);
        if (empty($ticket['issue'])){
            self::addMessage("Ticket not found", 'error');
            self::redirect(\Core\Utils\Navigation::urlFor('support'));
        }

        // update is read
        \Modules\Humpchies\SupportModel::update(['is_read' => 1], 'id = '. $ticketId);

        $ticketComments  =  \Modules\Humpchies\SupportModel::getCommentsByTicketId($ticketId);

        self::prepareTicketForUI($ticket);
        self::prepareTicketCommentsForUI($ticketComments);

        self::$data['ticket'] = $ticket;
        self::$data['ticketComments'] = $ticketComments;

        self::setHeaderClass("has_breadcrumb");
        
        \Core\Controller::setExtraHead('
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'pc/support.js?v=2.2"></script>'
        );

        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="'
            .
            \Modules\Humpchies\Utils::getCDNPath('css')
            .
            (\Core\Utils\Device::isPC() ? 'pc' : ('mobile/touch'))
            .
            '/support.css?v=8.9">');

        // Set some page values...
        self::setTitle('[LABEL-POST-AD]');
        self::setDescription('[LABEL-POST-AD]');

    }

    public static function reply_ma() {
        
        header('Content-Type: text/html; charset=utf-8');
        self::disableCache();
        $user_id = \Core\Utils\Session::getCurrentUserID();
        $success = 0;
        $message = '';
        $commentDetails = array();
        if (!$user_id){
            $message = "please login first";
        }

        if (!empty($_POST) && empty($message)){
            $data = array(
                'comment' => !empty($_POST['message']) ? trim($_POST['message']) : '',
                'ticket_id' => !empty($_POST['ticket_id']) ? intval($_POST['ticket_id']) : null,
                'user_id' => $user_id,
            );

            \Core\library\log::debug($data, "REPLY DATA");

            $_POST=array();
            $images = self::getUploadedFiles('attached');
            $validity = self::validateReply($data, $images);

            switch ($validity){
                case self::NO_ERROR:
                    $attached = array();
                    if (!empty($images)){
                        foreach ($images as $image){
                            $row  = \Core\Utils\Image::save($image['tmp_name'], 'attached', $image['ext']);
                            if (!is_array($row)){
                                self::addMessage("[ERROR_IMAGE_ERROR_UNHANDLED]", 'error');
                                return false;
                            }
                            $row['file_name'] = $image['name'];
                            array_push($attached, $row);
                        }
                    }

                    $data['attached'] = $attached;
                    unset($data['user_id']);
                    $ticketResult = \Modules\Humpchies\SupportModel::addReply($data);
                    if (is_numeric($ticketResult)){
                        $message = "[SUCCESS-REPLY-ADDED]";
                        $success = 1;
                        $commentDetails = \Modules\Humpchies\SupportModel::getCommentById($ticketResult);
                        self::prepareTicketCommentForUI($commentDetails);
                    }else{
                        $message = "[ERROR-UNHANDLED]";
                    }
                    break;
                case self::ERROR_MESSAGE_IS_EMPTY:
                    $message = "[ERROR-SUPPORT-TICKET-MESSAGE-IS-EMPTY]";
                    break;
                case self::ERROR_MESSAGE_IS_TOO_SHORT:
                    $message = "[ERROR-SUPPORT-TICKET-MESSAGE-IS-TOO-SHORT]";
                    break;
                case self::ERROR_MESSAGE_IS_TOO_LONG:
                    $message = "[ERROR-SUPPORT-TICKET-MESSAGE-IS-TOO-LONG]";
                    break;
                case self::ERROR_IMAGE_TYPE_IS_INVALID:
                    $message = "[ERROR-SUPPORT-TICKET-INVALID-FILE-TYPE]";
                    break;
                case self::ERROR_IMAGE_SIZE_IS_TOO_BIG:
                    $message = "[ERROR-SUPPORT-TICKET-FILE-IS-BIG]";
                    break;
                case self::ERROR_IMAGE_ERROR_UNHANDLED:
                    $message = "[ERROR_IMAGE_ERROR_UNHANDLED]";
                    break;
                case self::ERROR_UNKNOWN:
                    $message = "[ERROR-UNHANDLED]";
                    break;
                default:
                    $message = "[ERROR-SOMETHING-WENT-WRONG]";
                    break;
            }
        }

        \Core\Utils\Text::translateBufferTokens($message,
            \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'),
            \Core\Utils\Session::getLocale(),
            \Core\Utils\Config::getConfigValue('\Application\Settings::locales_module',
                FALSE));

        // echo json_encode(array(
        //     'success'=>$success,
        //     'message'=>$message,
        //     'details'=>$commentDetails,
        // ));
        self::redirect(\Core\Utils\Navigation::urlFor('support', 'ticket', array('id' => $data['ticket_id'])));
        die;
    }

    /**
     * @param $tickets
     */
    private static function prepareTicketsForUI(&$tickets) {

        foreach ($tickets as &$ticket){
            
            $month_index = date('m', $ticket['creation_date']) - 1; 
            $month = self::$shortMonths[\Core\Utils\Session::getLocale()][$month_index];
            $ticket['creation_date'] =  date('d', $ticket['creation_date']) . ' ' . $month . ' ' . date("Y, H:i", $ticket['creation_date']);

            $ticket['resolve_date'] = date("Y m d H:i:s", $ticket['resolve_date']);
            if (strlen($ticket['subject']) > 150){
                $ticket['subject'] =  substr($ticket['subject'], 0, 147).'...';
            }
        }
    }

    /**
     * @param $ticket
     */
    private static function prepareTicketForUI(&$ticket){
        
        $month_index = date('m', $ticket['issue']['creation_date']) - 1; 
        $month = self::$shortMonths[\Core\Utils\Session::getLocale()][$month_index];
        $ticket['issue']['creation_date'] =  date('d', $ticket['issue']['creation_date']) . ' ' . $month . ' ' . date("Y, H:i", $ticket['issue']['creation_date']);
        //$ticket['issue']['creation_date'] = date("Y m d H:i:s", $ticket['issue']['creation_date']);
        $ticket['issue']['resolve_date'] = date("Y m d H:i:s", $ticket['issue']['resolve_date']);

        foreach ($ticket['attach'] as &$file){
            $file['url'] = \Core\Utils\Image::getUrl($file);
        }
    }

    /**
     * @param $comments
     */
    private static function prepareTicketCommentsForUI(&$comments){

        foreach ($comments as &$comment){
            self::prepareTicketCommentForUI($comment);
        }
    }

    /**
     * @param $comments
     */
    private static function prepareTicketCommentForUI(&$comment){
        if (!empty($comment['attached_files'])){
            foreach ($comment['attached_files'] as &$attached_file) {
                $attached_file['url'] = \Core\Utils\Image::getUrl($attached_file);
            }
        }else{
            $comment['attached_files']=array();
        }
    }

    private static function validateNew(&$fields, &$images) {
        // ---------------------------REMOVE UNSUPPORTED FILES--------------------------------------
        $images = array_intersect_key($images, array(0           => NULL,
            1           => NULL,
            2           => NULL,
            3           => NULL,
            4           => NULL));
        // Validate and sanitize all fields...
        // ===================================
        $subject_validity = \Modules\Humpchies\SupportModel::isSubjectInputValid($fields['subject']);
        $message_validity = \Modules\Humpchies\SupportModel::isMessageInputValid($fields['message']);

        $images_validity = \Core\Utils\Security::areFileInputValid($images, self::$AllowedTypes, self::$AllowedExt);

        switch ($subject_validity){
            case  \Modules\Humpchies\SupportModel::NO_ERROR :
                break;
            case \Modules\Humpchies\SupportModel::ERROR_SUBJECT_IS_EMPTY :
                return self::ERROR_SUBJECT_IS_EMPTY;
            case \Modules\Humpchies\SupportModel::ERROR_SUBJECT_IS_TOO_SHORT :
                return self::ERROR_SUBJECT_IS_TOO_SHORT;
            case \Modules\Humpchies\SupportModel::ERROR_SUBJECT_IS_TOO_LONG :
                return self::ERROR_SUBJECT_IS_TOO_LONG;
            default:
                return self::ERROR_UNKNOWN;
        }

        switch ($message_validity){
            case  \Modules\Humpchies\SupportModel::NO_ERROR :
               break;
            case \Modules\Humpchies\SupportModel::ERROR_MESSAGE_IS_EMPTY :
                return self::ERROR_MESSAGE_IS_EMPTY;
            case \Modules\Humpchies\SupportModel::ERROR_MESSAGE_IS_TOO_SHORT :
                return self::ERROR_MESSAGE_IS_TOO_SHORT;
            case \Modules\Humpchies\SupportModel::ERROR_MESSAGE_IS_TOO_LONG :
                return self::ERROR_MESSAGE_IS_TOO_SHORT;
            default:
                return self::ERROR_UNKNOWN;
        }

        switch ($images_validity){
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_IMAGE_SIZE_IS_TOO_BIG:
                return self::ERROR_IMAGE_SIZE_IS_TOO_BIG;
            case \Core\Utils\Security::ERROR_IMAGE_ERROR_UNHANDLED:
                return self::ERROR_IMAGE_ERROR_UNHANDLED;
            case \Core\Utils\Security::ERROR_IMAGE_TYPE_IS_INVALID:
                return self::ERROR_IMAGE_TYPE_IS_INVALID;
            default:
                return self::ERROR_UNKNOWN;
        }

        $fields['status'] = \Modules\Humpchies\SupportModel::STATUS_TICKET_OPENED;

        return self::NO_ERROR;
    }

    private static function validateReply(&$fields, &$images) {
        // ---------------------------REMOVE UNSUPPORTED FILES--------------------------------------
        $images = array_intersect_key($images, array(0           => NULL,
            1           => NULL,
            2           => NULL,
            3           => NULL,
            4           => NULL));
        // Validate and sanitize all fields...
        // ===================================
        $message_validity = \Modules\Humpchies\SupportModel::isMessageInputValid($fields['comment']);
        $ticket_validity = \Modules\Humpchies\SupportModel::isTicketIdValid($fields['ticket_id'], $fields['user_id']);

        $images_validity = \Core\Utils\Security::areFileInputValid($images, self::$AllowedTypes, self::$AllowedExt);

        switch ($ticket_validity){
            case \Modules\Humpchies\SupportModel::NO_ERROR:
                break;
            case \Modules\Humpchies\SupportModel::ERROR_UNKNOWN:
                return self::ERROR_UNKNOWN;
            default:
                return self::ERROR_UNKNOWN;
        }

        switch ($message_validity){
            case  \Modules\Humpchies\SupportModel::NO_ERROR :
               break;
            case \Modules\Humpchies\SupportModel::ERROR_MESSAGE_IS_EMPTY :
                return self::ERROR_MESSAGE_IS_EMPTY;
            case \Modules\Humpchies\SupportModel::ERROR_MESSAGE_IS_TOO_SHORT :
                return self::ERROR_MESSAGE_IS_TOO_SHORT;
            case \Modules\Humpchies\SupportModel::ERROR_MESSAGE_IS_TOO_LONG :
                return self::ERROR_MESSAGE_IS_TOO_SHORT;
            default:
                return self::ERROR_UNKNOWN;
        }

        switch ($images_validity){
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_IMAGE_SIZE_IS_TOO_BIG:
                return self::ERROR_IMAGE_SIZE_IS_TOO_BIG;
            case \Core\Utils\Security::ERROR_IMAGE_ERROR_UNHANDLED:
                return self::ERROR_IMAGE_ERROR_UNHANDLED;
            case \Core\Utils\Security::ERROR_IMAGE_TYPE_IS_INVALID:
                return self::ERROR_IMAGE_TYPE_IS_INVALID;
            default:
                return self::ERROR_UNKNOWN;
        }

        return self::NO_ERROR;
    }

    /**
     * @param $inputName
     * @return array
     */
    private static function getUploadedFiles($inputName){
        $uploadedFiles = array();

        if (empty($inputName) || !is_string($inputName)){
            return $uploadedFiles;
        }

        if (empty($_FILES[$inputName])){
            return $uploadedFiles;
        }

        $FileList = $_FILES[$inputName];
        foreach ($FileList['name'] as $i => $imageName){
            if (is_file($FileList['tmp_name'][$i])){
                $uploadedFiles[$i] = array(
                    'name'=>$imageName,
                    'size'=>$FileList['size'][$i],
                    'type'=>$FileList['type'][$i],
                    'tmp_name'=>$FileList['tmp_name'][$i],
                    'error'=>$FileList['error'][$i],
                );
            }
        }
        return $uploadedFiles;
    }
}
