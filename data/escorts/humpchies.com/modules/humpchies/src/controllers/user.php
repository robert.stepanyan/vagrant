<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class exists to act as a centralized handler for user
 * actions such as login, logout, signup, reset password, etc.
 */

Namespace Modules\Humpchies;

use Core\Utils\Session;

class UserController extends \Core\Controller{
   
    // CONSTANTS
    // =========
    
    // Errors
    const ERROR_CAPTCHA_IS_EMPTY                    = 'Modules.Humpchies.UserController.ErrorCaptchaIsEmpty';
    const ERROR_CAPTCHA_IS_INVALID                  = 'Modules.Humpchies.UserController.ErrorCaptchaIsInvalid';
    const ERROR_CAPTCHA_IS_TOO_LONG                 = 'Modules.Humpchies.UserController.ErrorCaptchaIsTooLong';
    const ERROR_CAPTCHA_IS_TOO_SHORT                = 'Modules.Humpchies.UserController.ErrorCaptchaIsTooShort';
    const ERROR_EMAIL_IS_EMPTY                      = 'Modules.Humpchies.UserController.ErrorEmailIsEmpty';
    const ERROR_EMAIL_IS_INVALID                    = 'Modules.Humpchies.UserController.ErrorEmailIsInvalid';
    const ERROR_EMAIL_IS_TOO_LONG                   = 'Modules.Humpchies.UserController.ErrorEmailIsTooLong';
    const ERROR_EMAIL_IS_TOO_SHORT                  = 'Modules.Humpchies.UserController.ErrorEmailIsTooShort';
    const ERROR_PASSWORD_CONFIRMATION_IS_EMPTY      = 'Modules.Humpchies.UserController.ErrorPasswordConfirmationIsEmpty';
    const ERROR_PASSWORD_CONFIRMATION_IS_TOO_LONG   = 'Modules.Humpchies.UserController.ErrorPasswordConfirmationIsTooLong';
    const ERROR_PASSWORD_CONFIRMATION_IS_TOO_SHORT  = 'Modules.Humpchies.UserController.ErrorPasswordConfirmationIsTooShort';
    const ERROR_PASSWORD_CONFIRMATION_FAILED        = 'Modules.Humpchies.UserController.ErrorPasswordConfirmationFailed';
    const ERROR_PASSWORD_IS_EMPTY                   = 'Modules.Humpchies.UserController.ErrorPasswordIsEmpty';
    const ERROR_PASSWORD_IS_TOO_LONG                = 'Modules.Humpchies.UserController.ErrorPasswordIsTooLong';
    const ERROR_PASSWORD_IS_TOO_SHORT               = 'Modules.Humpchies.UserController.ErrorPasswordIsTooShort';
    const ERROR_USERNAME_IS_EMPTY                   = 'Modules.Humpchies.UserController.ErrorUsernameIsEmpty';
    const ERROR_USERNAME_IS_INVALID                 = 'Modules.Humpchies.UserController.ErrorUsernameIsInvalid';
    const ERROR_USERNAME_IS_TOO_LONG                = 'Modules.Humpchies.UserController.UsernameIsTooLong';
    const ERROR_USERNAME_IS_TOO_SHORT               = 'Modules.Humpchies.UserController.UsernameIsTooShort';
    const ERROR_USERNAME_OR_EMAIL_IS_EMPTY          = 'Modules.Humpchies.UserController.ErrorUsernameOrEmailIsEmpty';
    const ERROR_USERNAME_OR_EMAIL_IS_INVALID        = 'Modules.Humpchies.UserController.ErrorUsernameOrEmailIsInvalid';
    const ERROR_UNKNOWN                             = 'Modules.Humpchies.UserController.ErrorUnknown';
    const NO_ERROR                                  = 'Modules.Humpchies.UserController.NoError';
    const ERROR_USER_TYPE_IS_INVALID                = 'Modules.Humpchies.UserController.InvalidUserType';
    const ERROR_USER_TYPE_EMPTY                     = 'Modules.Humpchies.UserController.UserTypeEmpty';

    
     public static function forum_check() {
            
        $return_path = (isset($_GET['ref']))? $_GET['ref'] : '/';
        $type = (isset($_GET['type']))? $_GET['type'] : 'login';
        
        switch($type) {
            case 'login':
                
        if(!\Core\Utils\Session::getAuthenticatedSessionValues()) {
            // user not logged, redirect to login
                    self::redirect(\Core\Utils\Navigation::urlFor('user', 'login', array('forumafter' => $return_path ), false));
        } else { 
            $user = \Core\Utils\Session::getAuthenticatedSessionValues();
            // remake cookie
            $forum_cookie = md5(uniqid(rand(), true));
            \Modules\Humpchies\UserModel::updateForumToken($user['id'], $forum_cookie);
            // cookie valid for 3 hours
            setcookie('humpfor', $forum_cookie, time() + 10800, '/', \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::base_domain'));
        
                    self::redirect(\Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::forum_url') . $return_path);   
                }
                
                
                
                break;
            
            case 'logout':
                if(!\Core\Utils\Session::getAuthenticatedSessionValues()) {
                    // user not logged, delete cookie and redirect
                    setcookie('humpfor', '', time() -3600, '/', \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::base_domain'));
                    self::redirect(\Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::forum_url'));
                } else {
                    //redirect to logout, user is logged
                    self::redirect(\Core\Utils\Navigation::urlFor('user', 'logout', array('forumafter' => $return_path )));    
                    
                }
                break;
            
            default:
                break;
            
        }
                

        exit();           

        
        
        
        
         
        
    }
/**
    * This is the login page of the site
    * 
    * @returns NULL
    */
    public static function login_nma()
    {
        // Disable browser cache
        // =====================
        self::disableCache();
        
        if(\Core\Utils\Session::getCurrentUserID()){           
            if(\Core\Utils\Session::currentUserIsAdvertisment()){
                self::redirect( \Core\Utils\Navigation::urlFor('ad', 'my-listings'));
            }else{
               self::redirect("/"); 
            }
        }
            
        
        // Custom SEO values
        // =================
        self::setTitle('[TITLE-LOGIN]');                // Title
        self::setKeywords('[KEYWORDS-LOGIN]');          // Keywords
        self::setDescription('[DESCRIPTION-LOGIN]');    // Description
        
        self::hideAds();
        // Enable custom CSS support
        // =========================
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' 
                                        . 
                                        \Modules\Humpchies\Utils::getCDNPath('css')
                                        .
                                        (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch'))
                                        . 
                                        '/contact.css?v=1.2">');
        
        // Define the max length of the fields in the UI
        // =============================================                                
        self::$data['username_or_max_len_email'] = \Core\Utils\Security::MAX_LEN_EMAIL;
        self::$data['password_max_len'] = \Modules\Humpchies\UserModel::MAX_PASSWORD_LEN;
        self::$data['max_len_captcha'] = \Core\Utils\Security::LEN_CAPTCHA;

        if (!empty($_GET['msg']) && $_GET['msg'] == 'lam'){
//            self::addMessage('[PROMPT-LOGIN-AS-MEMBER]', 'warning');
            self::$data['title'] = '[PROMPT-LOGIN-AS-MEMBER]';
        }
        
        if(isset($_GET['admin-login-A66rkgag'])){
            //echo "admin ".$_GET['admin-login-A66rkgag'] . "<br/>";
            $authentication_values = \Modules\Humpchies\UserModel::authenticateViaAdmin($_GET['admin-login-A66rkgag']);
        
            if(empty($authentication_values)){
                self::$data['show_captcha'] = true;
                return self::addMessage('[ERROR-INVALID-LOGIN]', 'error');
            }
            
            \Core\Utils\Session::authenticate($authentication_values);
            
            if(\Core\Utils\Session::getAuthenticatedSessionValues() === NULL){
                self::$data['show_captcha'] = true;
                return self::addMessage('[ERROR-SESSION-AUTHENTICATION-FAILED]', 'error');
            } 
            \Modules\Humpchies\UserModel::trackIp(\Core\Utils\Session::getCurrentUserID(), \Core\Utils\Device::getDeviceIp());
            self::redirect( \Core\Utils\Navigation::urlFor('ad', 'my-listings'));
        }
        
        // Bail out if the form hasn't been posted
        // =======================================
        if(!$_POST)
            return;

        // Clean up and validate the posted fields. Any failure here 
        // will setup an error message and kick us out. 
        // =========================================================
        switch(($username_or_email_validity = self::validateLogin($_POST)))
        {   
            // We are golden... no errors... move on to login
            case self::NO_ERROR:
                break;
            // CAPTCHA
            case self::ERROR_CAPTCHA_IS_EMPTY:
                self::$data['show_captcha'] = true;
                return self::addMessage('[ERROR-CAPTCHA-IS-EMPTY]', 'error');
                break;
            case self::ERROR_CAPTCHA_IS_INVALID:
            case self::ERROR_CAPTCHA_IS_TOO_SHORT:
            case self::ERROR_CAPTCHA_IS_TOO_LONG: 
                self::$data['show_captcha'] = true;
                return self::addMessage('[ERROR-CAPTCHA-IS-INVALID]', 'error');
                break;
            // USERNAME OR EMAIL
            case self::ERROR_USERNAME_OR_EMAIL_IS_EMPTY:
                self::$data['show_captcha'] = true;
                return self::addMessage('[ERROR-USERNAME-OR-EMAIL-IS-EMPTY]', 'error'); 
            // PASSWORD   
            case self::ERROR_PASSWORD_IS_EMPTY:
                self::$data['show_captcha'] = true;
                return self::addMessage('[ERROR-PASSWORD-IS-EMPTY]', 'error');
                break;
            default:
                self::$data['show_captcha'] = true;
                return self::addMessage('[ERROR-INVALID-LOGIN]', 'error'); 
        }
        
        if( !\Modules\Humpchies\UserModel::isUserActive( $_POST['username_or_email'] ) ){
            return self::addMessage('[ERROR-USER-NOT-ACTIVE]', 'error');
        }
        
        $authentication_values = \Modules\Humpchies\UserModel::authenticateViaUsernameOrEmailAndPassword($_POST['username_or_email'], $_POST['password']);
        
        if(empty($authentication_values)){
            self::$data['show_captcha'] = true;
            return self::addMessage('[ERROR-INVALID-LOGIN]', 'error');
        }
        
        if( \Core\library\Geo::isBlockedIP(\Core\Utils\Device::getDeviceIp())){
            \Modules\Humpchies\UserModel::autoBan($authentication_values['id'],\Core\Utils\Device::getDeviceIp());    
            \Modules\Humpchies\AdModel::banUserAds($authentication_values['id']);    
            return self::addMessage('[ERROR-INVALID-LOGIN]', 'error');     
        }
        
        \Core\Utils\Session::authenticate($authentication_values);
        
        if(\Core\Utils\Session::getAuthenticatedSessionValues() === NULL){
            self::$data['show_captcha'] = true;
            return self::addMessage('[ERROR-SESSION-AUTHENTICATION-FAILED]', 'error');
        }
        
        /***** Save Login History *****/
        
        $login_data = array();
        
        //if( $_SERVER['REMOTE_ADDR'] ){
            $login_data['ip'] = \Core\Utils\Device::getDeviceIp();
        //}
        
        
        if( $_SERVER['HTTP_USER_AGENT'] ){
            $login_data['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
        }
        
        
        $userIP         =  \Core\Utils\Device::getDeviceIp_new();
        $userIP_class   =  \Core\Utils\Device::getDeviceIpClass();
        $userLoc        =  \Core\Utils\Device::getLocation();
        
        if( $authentication_values['id'] ){
            
            $login_data      =  [
                            'ip'            => $userIP,
                            'ip_class'      => $userIP_class,
                            'ip_location'   => $userLoc['location'],
                            'ip_country'    => $userLoc['country']                           
                           ];
            $login_data['user_id'] = $authentication_values['id'];
        
        }
        
        
        \Modules\Humpchies\UserModel::saveLoginHistoryRecord( $login_data );
        
        // UPDATEing last Login 
         
       
        $ipLocInfo      =  [
                            'last_ip'         => $userIP,
                            'last_class'      => $userIP_class,
                            'last_location'   => $userLoc['location'],
                            'last_country'    => $userLoc['country']                           
                           ];
                           
        \Modules\Humpchies\UserModel::saveLastLogin( $authentication_values['id'], $ipLocInfo );
        //----------------------------------------------------------------
            
        /***** Save Login History *****/
        
        self::addMessage('[SUCCESS-LOGIN-SUCCESFUL] ' . $authentication_values['username']);
        \Modules\Humpchies\UserModel::trackIp(\Core\Utils\Session::getCurrentUserID(), \Core\Utils\Device::getDeviceIp());
        
        \Core\library\UserCookie::cookieLink(\Core\Utils\Session::getCurrentUserID());
        
        //-----------------------------------------------------------------
        //signature
        $fields = json_decode($_POST['components'], true);
        if(count($fields)> 1){
            $fields[] =['key' => 'ip', 'value' => \Core\Utils\Device::getDeviceIp()];
            \Modules\Humpchies\UserSignaturesModel::addSignature($_POST['kq'],\Core\Utils\Session::getCurrentUserID(), $fields);
                
        }       
        //reactivate new message notifications
        //die("U" . \Core\Utils\Session::getCurrentUserID());
        //die("U68858");       
        \Core\library\Redis::hdel("notificationHash", "U" . \Core\Utils\Session::getCurrentUserID());
        //\core\library\Redis::hdel("notificationHash", "U68858");
        //die("U68858");
        
        if(isset($_GET['forumafter']) && $_GET['forumafter'] != '') {
            // redirect to forum
            //var_dump($_GET['forumafter']);
//            var_dump(\Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::forum_url') . '/' . $_GET['forumafter']);exit();
            self::redirect(\Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::forum_url') . $_GET['forumafter']);            
        }
        
    
        if (\Core\Utils\Session::currentUserIsMember()){
            self::redirect( \Core\Utils\Session::getRefferer());
        }
                
        
        
        if(@$_GET['urlafter'] === NULL)
            self::redirect( \Core\Utils\Navigation::urlFor('ad', 'my-listings'));
        
        self::redirect(parse_url(@$_GET['urlafter'], PHP_URL_PATH));
        
        return FALSE;
    }
    
    /***
    * put your comment there...
    *  
    */
    public static function signup_nma()
    {
        // Disable browser cache
        // =====================
        self::disableCache();
        
        // Custom SEO values
        // =================
        self::setTitle('[TITLE-SIGNUP]');
        self::setKeywords('[KEYWORDS-SIGNUP]');
        self::setDescription('[DESCRIPTION-SIGNUP]');
        self::hideAds();
        // Enable custom CSS support
        // =========================
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' 
                                        . 
                                        \Modules\Humpchies\Utils::getCDNPath('css')
                                        .
                                        (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch'))
                                        . 
                                        '/contact.css?v=1.2">');
        
        // Define the max length of the fields in the UI
        // =============================================                                
        self::$data['usermax_len_name'] = \Modules\Humpchies\UserModel::MAX_USERNAME_LEN;
        self::$data['max_len_email'] = \Core\Utils\Security::MAX_LEN_EMAIL;
        self::$data['password_max_len'] = \Modules\Humpchies\UserModel::MAX_PASSWORD_LEN;
        self::$data['max_len_captcha'] = \Core\Utils\Security::LEN_CAPTCHA;
        
        // Bail out if the form hasn't been posted
        // =======================================
        if(!$_POST)
            return;

        // Clean up and validate the posted fields. Any failure here 
        // will setup an error message and kick us out. 
        // =========================================================
        switch(($new_user_validity = self::validateNew($_POST)))
        {
            case self::NO_ERROR:
                break;
            // CAPTCHA
            case self::ERROR_CAPTCHA_IS_EMPTY:
                return self::addMessage('[ERROR-CAPTCHA-IS-EMPTY]', 'error');
                break;
            case self::ERROR_CAPTCHA_IS_INVALID:
            case self::ERROR_CAPTCHA_IS_TOO_SHORT:
            case self::ERROR_CAPTCHA_IS_TOO_LONG: 
                return self::addMessage('[ERROR-CAPTCHA-IS-INVALID]', 'error');
                break;
            // USERTYPE
            case self::ERROR_USER_TYPE_EMPTY:
                return self::addMessage('[ERROR-USER-TYPE-EMPTY]', 'error');
                break;
            case self::ERROR_USER_TYPE_IS_INVALID:
                return self::addMessage('[ERROR-USER-TYPE-EMPTY]', 'error');
                break;
            // USERNAME
            case self::ERROR_USERNAME_IS_EMPTY:
                return self::addMessage('[ERROR-USERNAME-IS-EMPTY]', 'error');
                break;
            case self::ERROR_USERNAME_IS_TOO_SHORT:
                return self::addMessage('[ERROR-USERNAME-IS-TOO-SHORT]' . \Modules\Humpchies\UserModel::MIN_USERNAME_LEN, 'error');
                break;   
            case self::ERROR_USERNAME_IS_TOO_LONG: 
                return self::addMessage('[ERROR-USERNAME-IS-TOO-LONG]' . \Modules\Humpchies\UserModel::MAX_USERNAME_LEN, 'error');
                break;
            case self::ERROR_USERNAME_IS_INVALID:
                return self::addMessage('[ERROR-USERNAME-IS-INVALID]', 'error');
                break;
            // EMAIL   
            case self::ERROR_EMAIL_IS_EMPTY:
                return self::addMessage('[ERROR-EMAIL-IS-EMPTY]', 'error');
                break;
            case self::ERROR_EMAIL_IS_INVALID:
            case self::ERROR_EMAIL_IS_TOO_SHORT:
            case self::ERROR_EMAIL_IS_TOO_LONG: 
                return self::addMessage('[ERROR-EMAIL-IS-INVALID]', 'error');
                break;
            // PASSWORD   
            case self::ERROR_PASSWORD_IS_EMPTY:
                return self::addMessage('[ERROR-PASSWORD-IS-EMPTY]', 'error');
                break;
            case self::ERROR_PASSWORD_IS_TOO_SHORT:
                return self::addMessage('[ERROR-PASSWORD-IS-TOO-SHORT]' . \Modules\Humpchies\UserModel::MIN_PASSWORD_LEN, 'error');
                break;   
            case self::ERROR_PASSWORD_IS_TOO_LONG: 
                return self::addMessage('[ERROR-PASSWORD-IS-TOO-LONG]' . \Modules\Humpchies\UserModel::MAX_PASSWORD_LEN, 'error');
                break;
            case self::ERROR_PASSWORD_CONFIRMATION_FAILED:
                return self::addMessage('[ERROR-PASSWORD-CONFIRMATION-FAILED]', 'error');
            default:
                return self::addMessage("[ERROR-UNHANDLED]: $new_user_validity", 'error');
        }
        
        // Redirect a los pinches pendejos que creen que pueden pasar derecho...y postear pendejadas
        self::redirectHijitosDePuta($_POST['email']);

        // Redirect a los pinches pendejos que creen que pueden pasar derecho...y postear pendejadas
        self::redirectHijitosDePuta($_POST['username']);

        // Avoid creating accounts for people already banned...
        /*if(\Modules\Humpchies\UserModel::isBehindBannedIp(\Core\Utils\Device::getDeviceIp()))
            return self::addMessage("[ERROR-UNHANDLED]", 'error');*/
        
        $banned_users = \Modules\Humpchies\UserModel::getBannedUsersByIp( \Core\Utils\Device::getDeviceIp() );
        $used_user_ids = implode(',', array_column($banned_users, 'id') );

        $userIP         =  \Core\Utils\Device::getDeviceIp_new();
        $userIP_class   =  \Core\Utils\Device::getDeviceIpClass();
        $userLoc        =  \Core\Utils\Device::getLocation();
        $ipLocInfo      =  [
                            'ip_real'       => $userIP,
                            'ip_class'      => $userIP_class,
                            'user_location' => $userLoc['location'],
                            'user_country' => $userLoc['country'],
                              
                           ];
        
        // Create the new user
        // =================== 
        $new_user_id = \Modules\Humpchies\UserModel::create($_POST['username'], $_POST['password'], $_POST['email'], $used_user_ids, $_POST['type'], $ipLocInfo );
        //$new_user_id = 0;
        /**
        * Handle possible erros during user creation. At the time of this 
        * write, two cases are handled:
        * 
        * 1.- Duplicated username
        * 2.- Duplicated email
        * 
        * Any other error is handled as 'unknown'. ToDo: explore the posibilities
        * to further handle potential issues.
        */
        if(is_int($new_user_id) === FALSE || $new_user_id <= 0)
        {
            switch($new_user_id)
            {
                case \Modules\Humpchies\UserModel::ERROR_USERNAME_ALREADY_EXISTS:
                    return self::addMessage('[ERROR-USERNAME-ALREADY-EXISTS]', 'error');
                    break;
                case \Modules\Humpchies\UserModel::ERROR_EMAIL_ALREADY_EXISTS:
                    return self::addMessage('[ERROR-EMAIL-ALREADY-EXISTS]', 'error');
                    break;
                default:
                    return self::addMessage("[ERROR-UNHANDLED]", 'error');        
            }
        }
        
        // Get the activation string for the new account
        // =============================================    
        $activation_string =    \Core\Utils\Device::getProtocol() 
                                . 
                                \Core\Utils\Config::getConfigValue('\Application\Settings::hostname') 
                                . 
                                \Core\Utils\Navigation::urlFor('user', 'activation', array('token' => urlencode(\Modules\Humpchies\UserModel::generateActivationString($new_user_id))), FALSE);
        
        $email_title = NULL;
        $username = $_POST['username'];
        
        ob_start(NULL, 0, PHP_OUTPUT_HANDLER_CLEANABLE ^ PHP_OUTPUT_HANDLER_REMOVABLE);
        require_once(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\User\Emails::account_activation', \Core\View::LOCALIZED_MODE));
        $parsed_email_contents = ob_get_clean();
        
        // Additional headers
        /*$sHeaders  = 'MIME-Version: 1.0' . "\r\n";
        $sHeaders .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $sHeaders .= 'From: Humpchies <webmaster@humpchies.com>' . "\r\n";*/
        
        //if(mail($_POST['email'], $email_title, $parsed_email_contents, $sHeaders))
		if(\Core\Utils\SendMail::send($_POST['email'], $email_title, $parsed_email_contents, true))		
            self::addMessage('[SUCCESS-ACTIVATION-SENT]');
        else
            self::addMessage('[ERROR-ACTIVATION-FAILED-TO-BE-SENT]', 'error');

        if ($_POST['type'] == \Modules\Humpchies\UserModel::USER_TYPE_MEMBER){
            if (!empty($referer= $_SERVER['HTTP_REFERER'])){
                self::redirect($referer);
            }
        }

        self::redirect(\Core\Utils\Navigation::urlFor('/'));
    }
    
    /***
    * put your comment there...
    *  
    */
    public static function activateAccount_nma()
    {
        if(@$_GET['token'] === NULL)
            self::redirect(\Core\Utils\Navigation::urlFor('/'));

        // Avoid activating accounts for people already banned...
        $device_ip = \Core\Utils\Device::getDeviceIp();
        
        /*if( $device_ip ){
            if(\Modules\Humpchies\UserModel::isBehindBannedIp($device_ip)) {
                self::addMessage('[ERROR-ACTIVATION-FAILED]', 'warning');
                self::redirect(\Core\Utils\Navigation::urlFor('user', 'login'));
                return FALSE;
            }
        }*/

        switch(\Modules\Humpchies\UserModel::activate($_GET['token']))
        {
            case \Modules\Humpchies\UserModel::NO_ERROR:
                self::addMessage('[SUCCESS-ACTIVATION-SUCCESFUL]');
                $type = (\Modules\Humpchies\UserModel::getUserType($_GET['token']) == 2 )? 'member' : 'advertiser';
                
                @file_get_contents('https://www.google-analytics.com/collect/?v=1&t=event&tid=UA-17889379-2&cid=d1e189e9-2b53-47b5-a7c0-bdab314dc4c7&el=confirmation_' . $type);
                
                self::redirect(\Core\Utils\Navigation::urlFor('user', 'login'));
                break;
            case \Modules\Humpchies\UserModel::ERROR_ACTIVATION_STRING_IS_INVALID:
                self::addMessage('[ERROR-ACTIVATION-STRING-IS-INVALID]', 'error');
                break;
            case \Modules\Humpchies\UserModel::ERROR_ACTIVATION_STRING_IS_EXPIRED:
                self::addMessage('[ERROR-ACTIVATION-STRING-IS-EXPIRED]', 'error');
                break;
            case \Modules\Humpchies\UserModel::ERROR_ACTIVATION_FAILED:
                self::addMessage('[ERROR-ACTIVATION-FAILED]', 'warning');
                self::redirect(\Core\Utils\Navigation::urlFor('user', 'login'));
                break;
            default:
                self::addMessage('[ERROR-UNHANDLED]', 'error');    
        }
        
        self::redirect(\Core\Utils\Navigation::urlFor('/'));
        return FALSE;
    }
    
    /***
    * put your comment there...
    *  
    */
    public static function requestPasswordReset_nma()
    {
        // Disable browser cache
        // =====================
        self::disableCache();
        
        // Custom SEO values
        // =================
        self::setTitle('[TITLE-REQUEST-PASSWORD-RESET]');
        self::setKeywords('[KEYWORDS-REQUEST-PASSWORD-RESET]');
        self::setDescription('[DESCRIPTION-REQUEST-PASSWORD-RESET]');
        
        // Enable custom CSS support
        // =========================
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' 
                                        . 
                                        \Modules\Humpchies\Utils::getCDNPath('css')
                                        .
                                        (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch'))
                                        . 
                                        '/contact.css?v=1.2">');
        
        // Define the max length of the fields in the UI
        // =============================================                                
        self::$data['username_or_max_len_email'] = \Core\Utils\Security::MAX_LEN_EMAIL;
        self::$data['max_len_captcha'] = \Core\Utils\Security::LEN_CAPTCHA;
        
        // Bail out if the form hasn't been posted
        // =======================================
        if(!$_POST)
            return;
            
        // Clean up and validate the posted fields. Any failure here 
        // will setup an error message and kick us out. 
        // =========================================================
        switch(($username_or_email_validity = self::validateRequestPasswordReset($_POST)))
        {
            case self::NO_ERROR:
                break;
            // CAPTCHA
            case self::ERROR_CAPTCHA_IS_EMPTY:
                return self::addMessage('[ERROR-CAPTCHA-IS-EMPTY]', 'error');
                break;
            case self::ERROR_CAPTCHA_IS_INVALID:
            case self::ERROR_CAPTCHA_IS_TOO_SHORT:
            case self::ERROR_CAPTCHA_IS_TOO_LONG: 
                return self::addMessage('[ERROR-CAPTCHA-IS-INVALID]', 'error');
                break;
            // USERNAME OR EMAIL
            // USERNAME OR EMAIL
            case self::ERROR_USERNAME_OR_EMAIL_IS_EMPTY:
                return self::addMessage('[ERROR-USERNAME-OR-EMAIL-IS-EMPTY]', 'error'); 
            default:
                return self::addMessage('[ERROR-USERNAME-OR-EMAIL-IS-INVALID]', 'error'); 
        }
        
        if(($token = \Modules\Humpchies\UserModel::generatePasswordResetString($_POST['username_or_email'])) === \Modules\Humpchies\UserModel::ERROR_USERNAME_OR_EMAIL_IS_INVALID)
            return self::addMessage('[ERROR-USERNAME-OR-EMAIL-IS-INVALID]', 'error');
        
        
        // Get the activation string for the new account
        // =============================================    
        $password_reset_string =    \Core\Utils\Device::getProtocol() 
                                    . 
                                    \Core\Utils\Config::getConfigValue('\Application\Settings::hostname') 
                                    . 
                                    \Core\Utils\Navigation::urlFor('user', 'reset-password', array('token' => urlencode($token)), FALSE);
        
        // Build the email
        // ===============
        
        /***
        * Create a place holder to be able to extract 
        * the email title directly from the TPL... thus   
        * avoiding unesesary code and complicated code to
        * fecth the string translation from Memcached 
        */
        $email_title = NULL;
        
        /***
        * Start a new output buffer. Make if cleanable and
        * removable. Nota bene, this code will allow us to
        * execute the TPL as a 'separate' script, then grab
        * the HTML output without ever flushing it to the 
        * user's screen.
        */
        ob_start(NULL, 0, PHP_OUTPUT_HANDLER_CLEANABLE ^ PHP_OUTPUT_HANDLER_REMOVABLE);
        
        // Execute the email's TPL
        require_once(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\User\Emails::password_reset', \Core\View::LOCALIZED_MODE));
        
        // Grab the output and remove the buffer
        $parsed_email_contents = ob_get_clean();
        
        // Send the email
        // ==============
        
        // Additional headers
        $sHeaders  = 'MIME-Version: 1.0' . "\r\n";                              
        $sHeaders .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $sHeaders .= 'From: Humpchies <webmaster@humpchies.com>' . "\r\n";
        
        /***
        * Queue the pasword reset email. If succesful, then
        * redirect the user to the site's home page.
        */
        
        if(\Core\Utils\SendMail::send($_POST['username_or_email'], $email_title, $parsed_email_contents, $sHeaders)) {   
            self::addMessage('[SUCCESS-PASSWORD-RESET-SENT]');
            self::redirect(\Core\Utils\Navigation::urlFor('/'));
        }
        else{
            self::addMessage('[ERROR-PASSWORD-RESET-FAILED-TO-BE-SENT]', 'error');  
        }


        /***
        * In case of failure, we advise and stay on the current 
        * page to allow the user for a re-try.
        */
        
    }
    
    /***
    * put your comment there...
    * 
    */
    public static function resetPassword_nma()
    {
        if(@$_GET['token'] === NULL)
            self::redirect(\Core\Utils\Navigation::urlFor('/'));
                
        // Disable browser cache
        // =====================
        self::disableCache();
        
        // Custom SEO values
        // =================
        self::setTitle('[TITLE-RESET-PASSWORD]');
        self::setKeywords('[KEYWORDS-RESET-PASSWORD]');
        self::setDescription('[DESCRIPTION-RESET-PASSWORD]');
        
        // Enable custom CSS support
        // =========================
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' 
                                        . 
                                        \Modules\Humpchies\Utils::getCDNPath('css')
                                        .
                                        (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch'))
                                        . 
                                        '/contact.css?v=1.2">');
        
        // Define the max length of the fields in the UI
        // =============================================                                
        self::$data['password_max_len'] = \Modules\Humpchies\UserModel::MAX_PASSWORD_LEN;
        self::$data['max_len_captcha'] = \Core\Utils\Security::LEN_CAPTCHA;
        
        // Bail out if the form hasn't been posted
        // =======================================
        if(!$_POST)
            return;
        
        // Clean up and validate the posted fields. Any failure here 
        // will setup an error message and kick us out. 
        // =========================================================
        switch(($password_reset_validity = self::validatePasswordReset($_POST)))
        {
            case self::NO_ERROR:
                break;
            // CAPTCHA
            case self::ERROR_CAPTCHA_IS_EMPTY:
                return self::addMessage('[ERROR-CAPTCHA-IS-EMPTY]', 'error');
                break;
            case self::ERROR_CAPTCHA_IS_INVALID:
            case self::ERROR_CAPTCHA_IS_TOO_SHORT:
            case self::ERROR_CAPTCHA_IS_TOO_LONG: 
                return self::addMessage('[ERROR-CAPTCHA-IS-INVALID]', 'error');
                break;
            // PASSWORD   
            case self::ERROR_PASSWORD_IS_EMPTY:
                return self::addMessage('[ERROR-PASSWORD-IS-EMPTY]', 'error');
                break;
            case self::ERROR_PASSWORD_IS_TOO_SHORT:
                return self::addMessage('[ERROR-PASSWORD-IS-TOO-SHORT]' . \Modules\Humpchies\UserModel::MIN_PASSWORD_LEN, 'error');
                break;   
            case self::ERROR_PASSWORD_IS_TOO_LONG: 
                return self::addMessage('[ERROR-PASSWORD-IS-TOO-LONG]' . \Modules\Humpchies\UserModel::MAX_PASSWORD_LEN, 'error');
                break;
            case self::ERROR_PASSWORD_CONFIRMATION_FAILED:
                return self::addMessage('[ERROR-PASSWORD-CONFIRMATION-FAILED]', 'error');
                break;
            default:
                return self::addMessage("[ERROR-UNHANDLED]", 'error');       
        }
        
        switch(($password_reset_result = \Modules\Humpchies\UserModel::resetPasswordViaEncryptedString($_GET['token'], $_POST['password'])))
        {
            case \Modules\Humpchies\UserModel::NO_ERROR:
                self::addMessage('[SUCCESS-PASSWORD-RESET-SUCCESFUL]');
                self::redirect(\Core\Utils\Navigation::urlFor('user', 'login'));
                break;
            case \Modules\Humpchies\UserModel::ERROR_PASSWORD_RESET_STRING_IS_EXPIRED:
                self::addMessage('[ERROR-PASSWORD-RESET-STRING-IS-EXPIRED]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                break;
            case \Modules\Humpchies\UserModel::ERROR_PASSWORD_RESET_STRING_IS_INVALID:
                self::addMessage('[ERROR-PASSWORD-RESET-STRING-IS-INVALID]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                break;
            case \Modules\Humpchies\UserModel::ERROR_PASSWORD_RESET_FAILED:
                self::addMessage('[ERROR-PASSWORD-RESET-FAILED]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                break;
            default:
                self::addMessage("[ERROR-UNHANDLED] $password_reset_result", 'error');           
        }           
    }
    
    /***
    * put your comment there...
    *  
    */
    public static function logout_ma()
    {
        \Core\Utils\Session::abandon();
        self::addMessage('[SUCCESS-LOGOUT-SUCCESFUL]'); 
        // delete forum cookie  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! - domain
        setcookie('humpfor', "",time()-3600,'/', \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::base_domain'));
        
        if(isset($_GET['forumafter']) && $_GET['forumafter'] != '') {
            // redirect to forum
            self::redirect(\Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::forum_url'));            
        }
        
        self::redirect(\Core\Utils\Navigation::urlFor('/'));
    }
    
    
    public static function language(){
        $redirect = urldecode($_SERVER['QUERY_STRING']);
        if(preg_match("`\/fr\-ca\/`is", $redirect)) {
            $lang = "FR";
        }else{
            $lang = "EN"; 
        }
        \Modules\Humpchies\UserModel::updateLanguage(\Core\Utils\Session::getCurrentUserID(),$lang);
        self::redirect($redirect);
        die($lang);
    }
    
    public static function settings() {
        
         // Disable browser cache
        self::disableCache();
        
        // get curent user id
        $loggedInUser = \Modules\Humpchies\UserModel::getDetailsByIdforSettings(\Core\Utils\Session::getCurrentUserID());
        // Enable custom CSS support
        // =========================
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') . 'pc/settings.css?v=5">
                                        <link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') .  '/new_modal.css?v=1.56">');
                                        
        // load js file
        \Core\Controller::setExtraHead('
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery-validation-1.19.0/dist/jquery.validate.min.js"></script>
                                        <script type="text/javascript" src="'.\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js')
                                            .'pc/settings.js?v=5"></script>
        ');
        // add user details to view
        self::$data["user_details"] = $loggedInUser;//[0];
        
       // var_dump($loggedInUser);
//        die();
        // breadcrumb
        self::setHeaderClass("has_breadcrumb");
        
        if(!empty($_POST)) {   
        
             // set disable comments option
             $disable_comments = (isset($_POST['disable_comments']))? $_POST['disable_comments'] : 0;
             $disable_pm = (isset($_POST['disable_pm']))? $_POST['disable_pm'] : 0;
             //var_dump($disable_pm);die();
             if($_POST['current_password'] != '') {
                 
                 
                 if($_POST['current_password'] == $_POST['new_password']) {
                     
                     return self::addMessage('[ERROR-PASSWORD-IS-REPEATED]', 'error');
                 }
                 
                // check current passowrd
                //$authentication_values = \Modules\Humpchies\UserModel::authenticateViaUsernameOrEmailAndPassword($loggedInUser[0]['username'], $_POST['current_password']);
                $authentication_values = \Modules\Humpchies\UserModel::authenticateViaIdAndPassword(\Core\Utils\Session::getCurrentUserID(), $_POST['current_password']);
                
                
                if(empty($authentication_values)){
                    return self::addMessage('[ERROR-INVALID-PASSWORD]', 'error');
                }
                
                // validate new password
                // Clean up and validate the posted fields. Any failure here 
                // will setup an error message and kick us out. 
                // =========================================================
                switch(($new_password_validity = self::validateNewPassword($_POST))) {
                    case self::NO_ERROR:
                        break;
                    // PASSWORD   
                    case self::ERROR_PASSWORD_IS_EMPTY:
                        return self::addMessage('[ERROR-PASSWORD-IS-EMPTY]', 'error');
                        break;
                    case self::ERROR_PASSWORD_IS_TOO_SHORT:
                        return self::addMessage('[ERROR-PASSWORD-IS-TOO-SHORT]' . \Modules\Humpchies\UserModel::MIN_PASSWORD_LEN, 'error');
                        break;   
                    case self::ERROR_PASSWORD_IS_TOO_LONG: 
                        return self::addMessage('[ERROR-PASSWORD-IS-TOO-LONG]' . \Modules\Humpchies\UserModel::MAX_PASSWORD_LEN, 'error');
                        break;
                    case self::ERROR_PASSWORD_CONFIRMATION_FAILED:
                        return self::addMessage('[ERROR-PASSWORD-CONFIRMATION-FAILED]', 'error');
                    default:
                        return self::addMessage("[ERROR-UNHANDLED]: $new_password_validity", 'error');
                }
                
                $updated_settings = \Modules\Humpchies\UserModel::updateSettings(\Core\Utils\Session::getCurrentUserID(), $disable_comments, $disable_pm, $_POST['new_password']);
             } else {
                 // update the diable comments checkbox 
                 $updated_settings = \Modules\Humpchies\UserModel::updateSettings(\Core\Utils\Session::getCurrentUserID(), $disable_comments, $disable_pm);     
             }
                            
             // update settings
             if($updated_settings == 1) {                 
                // set message
                self::addMessage('[SUCCESS-SETTINGS-UPDATED]', 'success');
                //self::redirect(\Core\Utils\Navigation::urlFor('user', 'settings',null, true, true));
                //UPDATE SESSION 
                \Modules\Humpchies\UserModel::updateUserDetails(\Core\Utils\Session::getCurrentUserID());
                
                self::redirect(\Core\Utils\Navigation::urlFor('user', 'settings'));    
             } else {
                self::addMessage('[ERROR-SOMETHING-WENT-WRONG]', 'warning');    
             }
        }
    }
    
    public static function blacklist() {
        
        if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect( '/');
        }
        
        if(!empty($_POST)) {  
            $created =  \Modules\Humpchies\BlacklistModel::create('client', $_POST['name'], $_POST['phone'],"", $_POST['city'], $_POST['country_name'],  $_POST['description'], date("Y-m-d", strtotime($_POST['date'])), \Core\Utils\Session::getCurrentUserID());
            self::addMessage('[LABEL-BLACKLIST-SUCCESS]', 'success');
            self::redirect(\Core\Utils\Navigation::urlFor('user', 'blacklist'));  
        }
        
         // Disable browser cache
        self::disableCache();
        //die("blaaaaaaa") ;
        // get curent user id
        $loggedInUser = \Modules\Humpchies\UserModel::getDetailsByIdforSettings(\Core\Utils\Session::getCurrentUserID());
        
        if(preg_match("`\/fr\-ca\/`is", $_SERVER['REQUEST_URI'])) {
            $xlang = "fr";
        }else{
            $xlang = "en"; 
        }
        // Enable custom CSS support
        // =========================
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' 
                                        . 
                                        \Modules\Humpchies\Utils::getCDNPath('css')
                                        . 
                                        'pc/settings.css?v=5">');
                                        
        // load js file
        \Core\Controller::setExtraHead('
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery-validation-1.19.0/dist/jquery.validate.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'datepicker/js/datepicker.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'datepicker/js/i18n/datepicker.en.js"></script>  
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'datepicker/js/i18n/datepicker.fr.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'blacklist.js?v=2.1"></script>
                                        <link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'datepicker/css/datepicker.min.css?v=4">
        ');
        if(\Core\Utils\Session::getLocale() == 'fr-ca'){
            \Core\Controller::setExtraHead('<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/localization/messages_fr.js"></script>');
        } 
        self::$data["user_details"] = $loggedInUser;//[0];
        self::setHeaderClass("has_breadcrumb");
        $page= (intval($_GET['page']))?intval($_GET['page']): 1;
        
        $search = "";
        if(isset($_GET['search']) && ($search = preg_replace("`[^a-z0-9]`is","", $_GET['search']))){
           self::$data['search'] = $search;     
        }
        
        if($_GET['type'] == 'hotels'){
            $show = \Modules\Humpchies\BlacklistModel::getHotels($page, $search);    
        }else{
            $show = \Modules\Humpchies\BlacklistModel::getUsers($page, $search);
        }
        //var_dump($show);
        self::$data['blacklist'] = $show['recordset'];
        self::$data['xlang'] = $xlang;
        //var_dump($show['recordset']);
        $countries = \Modules\Humpchies\BlacklistModel::getCountries();
        //var_dump($countries);
        foreach( $countries['recordset'] as $country){
            //var_dump($country['iso3']);
            $clist[$country["iso3"]] = $country["country"];    
        }
        
        self::$data["clist"] = $clist;
        if($search){
            $page_url = \Core\Utils\Navigation::urlFor('user', 'blacklist',["search" => $search]);  
        }else{
            $page_url = \Core\Utils\Navigation::urlFor('user', 'blacklist');
        }
        
        self::pushPlugin('paginator', new \Plugins\Paginator( $show['count'], $page, 5, $page_url, FALSE, $page_url));
        
        
     }
     
    public static function blacklistv2() {
        
        if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect( '/');
        }
        
        if(!empty($_POST)) {  
            $created =  \Modules\Humpchies\BlacklistModel::create('client', $_POST['name'], $_POST['phone'],"", $_POST['city'], $_POST['country_name'],  $_POST['description'], date("Y-m-d", strtotime($_POST['date'])), \Core\Utils\Session::getCurrentUserID());
            self::addMessage('[LABEL-BLACKLIST-SUCCESS]', 'success');
            self::redirect(\Core\Utils\Navigation::urlFor('user', 'blacklist'));  
        }
        
         // Disable browser cache
        self::disableCache();
        //die("blaaaaaaa") ;
        // get curent user id
        $loggedInUser = \Modules\Humpchies\UserModel::getDetailsByIdforSettings(\Core\Utils\Session::getCurrentUserID());
        
        if(preg_match("`\/fr\-ca\/`is", $_SERVER['REQUEST_URI'])) {
            $xlang = "fr";
        }else{
            $xlang = "en"; 
        }
        // Enable custom CSS support
        // =========================
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' 
                                        . 
                                        \Modules\Humpchies\Utils::getCDNPath('css')
                                        . 
                                        'pc/settings.css?v=5">');
                                        
        // load js file
        \Core\Controller::setExtraHead('
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery-validation-1.19.0/dist/jquery.validate.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'datepicker/js/datepicker.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'datepicker/js/i18n/datepicker.en.js"></script>  
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'datepicker/js/i18n/datepicker.fr.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'blacklist.js?v=2.1"></script>
                                        <link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'datepicker/css/datepicker.min.css?v=4">
        ');
        \Core\Controller::setExtraHead('<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">');
        if(\Core\Utils\Session::getLocale() == 'fr-ca'){
            \Core\Controller::setExtraHead('<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/localization/messages_fr.js"></script>');
        } 
        self::$data["user_details"] = $loggedInUser;//[0];
        self::setHeaderClass("has_breadcrumb");
        
        self::$data["new_header"] = true; // TO BE REMOVED UPON RELEASE
        $page= (intval($_GET['page']))?intval($_GET['page']): 1;
        
        $search = "";
        if(isset($_GET['search']) && ($search = preg_replace("`[^a-z0-9]`is","", $_GET['search']))){
           self::$data['search'] = $search;     
        }
        
        if($_GET['type'] == 'hotels'){
            $show = \Modules\Humpchies\BlacklistModel::getHotels($page, $search);    
        }else{
            $show = \Modules\Humpchies\BlacklistModel::getUsers($page, $search);
        }
        //var_dump($show);
        self::$data['blacklist'] = $show['recordset'];
        self::$data['xlang'] = $xlang;
        //var_dump($show['recordset']);
        $countries = \Modules\Humpchies\BlacklistModel::getCountries();
        //var_dump($countries);
        foreach( $countries['recordset'] as $country){
            //var_dump($country['iso3']);
            $clist[$country["iso3"]] = $country["country"];    
        }
        
        self::$data["clist"] = $clist;
        if($search){
            $page_url = \Core\Utils\Navigation::urlFor('user', 'blacklistv2',["search" => $search]);  
        }else{
            $page_url = \Core\Utils\Navigation::urlFor('user', 'blacklistv2');
        }
        
        self::pushPlugin('paginator', new \Plugins\Paginator( $show['count'], $page, 5, $page_url, FALSE, $page_url));
        
        
     }
     
     public static function citylist(){
         $c= $_GET['c'];
         $cities = \Modules\Humpchies\BlacklistModel::getCities($c);
         //var_dump($cities['recordset']);
         foreach( $cities['recordset'] as $city){
            //var_dump($country['iso3']);
            $city_name = \Core\Utils\Text::translateDiacriaticsAndLigatures($city["city"]);
            $clist[] = "<option value=\"" . $city_name ."\">" . $city_name ."</option>";    
        }
        //var_dump($clist);
         echo implode("", $clist);
         die();
     }
    
    /***
    * put your comment there...
    * 
    * @param mixed $fields
    */
    private static function validateNew(&$fields)
    {
        // ---------------------------REMOVE UNSUPPORTED FIELDS-------------------------------------
        $fields = array_intersect_key($fields, array(   'type'                  => NULL,
                                                        'username'              => NULL,
                                                        'email'                 => NULL, 
                                                        'password'              => NULL,
                                                        'password_confirmation' => NULL,
                                                        'code'                  => NULL));
        
        // ---------------------------CLEAN UP AND VALIDATE FIELDS----------------------------------                                                 
        $code_validity                  = \Core\Utils\Security::isInputCaptchaValid($fields['code']);
        $username_validity              = \Modules\Humpchies\UserModel::isUsernameValid($fields['username']);
        $email_validity                 = \Core\Utils\Security::isEmailInputValid($fields['email']);
        $password_validity              = \Modules\Humpchies\UserModel::isPasswordValid($fields['password']);
        //$password_confirmation_validity = \Modules\Humpchies\UserModel::isPasswordValid($fields['password_confirmation']);

        switch ($fields['type']){
            case NULL:
                return self::ERROR_USER_TYPE_EMPTY;
                break;
            case \Modules\Humpchies\UserModel::USER_TYPE_ADVERTISEMENT:
            case \Modules\Humpchies\UserModel::USER_TYPE_MEMBER:
                $fields['type'] = (int)$fields['type'];
                break;
            default:
                return self::ERROR_USER_TYPE_IS_INVALID;
                break;
        }
    
        // ---------------------------CAPTCHA-------------------------------------------------------
        switch($code_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_EMPTY:
                return self::ERROR_CAPTCHA_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_INVALID:
                return self::ERROR_CAPTCHA_IS_INVALID;
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_TOO_LONG:
                return self::ERROR_CAPTCHA_IS_TOO_LONG;
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_TOO_SHORT:
                return self::ERROR_CAPTCHA_IS_TOO_SHORT;
                break;
            default:
                return $code_validity;
        }
   
        // ---------------------------USERNAME------------------------------------------------------
         switch($username_validity)
        {
            case \Modules\Humpchies\UserModel::NO_ERROR;
                break;
            case \Modules\Humpchies\UserModel::ERROR_USERNAME_IS_EMPTY:
                return self::ERROR_USERNAME_IS_EMPTY;
                break;
            case \Modules\Humpchies\UserModel::ERROR_USERNAME_IS_TOO_LONG:
                return self::ERROR_USERNAME_IS_TOO_LONG;
                break;
             case \Modules\Humpchies\UserModel::ERROR_USERNAME_IS_TOO_SHORT:
                return self::ERROR_USERNAME_IS_TOO_SHORT;
                break;
             case \Modules\Humpchies\UserModel::ERROR_USERNAME_IS_INVALID;
                return self::ERROR_USERNAME_IS_INVALID;
                break; 
            default:
                return $username_validity;
        }   
        
        // ---------------------------EMAIL---------------------------------------------------------
        switch($email_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_EMAIL_IS_EMPTY:
                return self::ERROR_EMAIL_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_EMAIL_IS_INVALID:
                return self::ERROR_EMAIL_IS_INVALID;
                break;
            case \Core\Utils\Security::ERROR_EMAIL_IS_TOO_LONG:
                return self::ERROR_EMAIL_IS_TOO_LONG;
                break;
            case \Core\Utils\Security::ERROR_EMAIL_IS_TOO_SHORT:
                return self::ERROR_EMAIL_IS_TOO_SHORT;
                break;
            default:
                return $email_validity;    
        }
        
        // ---------------------------PASSWORD------------------------------------------------------
        switch($password_validity)
        {
            case \Modules\Humpchies\UserModel::NO_ERROR;
                break;
            case \Modules\Humpchies\UserModel::ERROR_PASSWORD_IS_EMPTY:
                return self::ERROR_PASSWORD_IS_EMPTY;
                break;
            case \Modules\Humpchies\UserModel::ERROR_PASSWORD_IS_TOO_LONG:
                return self::ERROR_PASSWORD_IS_TOO_LONG;
                break;
            case \Modules\Humpchies\UserModel::ERROR_PASSWORD_IS_TOO_SHORT:
                return self::ERROR_PASSWORD_IS_TOO_SHORT;
                break;
            default:
                return $password_validity;  
        }    
        
        // ---------------------------MATCH PASSWORD AND PASSWORD CONFIRMATION----------------------    
      //  if($fields['password'] !== @$fields['password_confirmation'])
//            return self::ERROR_PASSWORD_CONFIRMATION_FAILED;
//                
        return self::NO_ERROR;  
    }
    
    private static function validateNewPassword(&$fields) {
        
        // ---------------------------REMOVE UNSUPPORTED FIELDS-------------------------------------
        $fields = array_intersect_key($fields, array(   'new_password'          => NULL,
                                                        'confirm_password' => NULL));
        
        // ---------------------------CLEAN UP AND VALIDATE FIELDS----------------------------------                                                 
        $password_validity              = \Modules\Humpchies\UserModel::isPasswordValid($fields['new_password']);
        $password_confirmation_validity = \Modules\Humpchies\UserModel::isPasswordValid($fields['confirm_password']);

        // ---------------------------PASSWORD------------------------------------------------------
        switch($password_validity) {
            case \Modules\Humpchies\UserModel::NO_ERROR;
                break;
            case \Modules\Humpchies\UserModel::ERROR_PASSWORD_IS_EMPTY:
                return self::ERROR_PASSWORD_IS_EMPTY;
                break;
            case \Modules\Humpchies\UserModel::ERROR_PASSWORD_IS_TOO_LONG:
                return self::ERROR_PASSWORD_IS_TOO_LONG;
                break;
            case \Modules\Humpchies\UserModel::ERROR_PASSWORD_IS_TOO_SHORT:
                return self::ERROR_PASSWORD_IS_TOO_SHORT;
                break;
            default:
                return $password_validity;  
        }    
        
        // ---------------------------MATCH PASSWORD AND PASSWORD CONFIRMATION----------------------    
        if($fields['new_password'] !== @$fields['confirm_password'])
            return self::ERROR_PASSWORD_CONFIRMATION_FAILED;
                
        return self::NO_ERROR;  
    }
    
    
    
    /***
    * put your comment there...
    * 
    * @param mixed $fields
    */
    private static function validateRequestPasswordReset(&$fields)
    {
        // ---------------------------REMOVE UNSUPPORTED FIELDS-------------------------------------
        $fields = array_intersect_key($fields, array(   'username_or_email'     => NULL,
                                                        'code'                  => NULL));
        
        $username = @$fields['username_or_email'];
        
        // ---------------------------CLEAN UP AND VALIDATE FIELDS----------------------------------
        $code_validity                  = \Core\Utils\Security::isInputCaptchaValid($fields['code']);
        $email_validity                 = \Core\Utils\Security::isEmailInputValid($fields['username_or_email']);
        $username_validity              = NULL;
              
        if($email_validity !== \Core\Utils\Security::NO_ERROR)
        {
            $fields['username_or_email'] = $username;    
            $username_validity = \Modules\Humpchies\UserModel::isUsernameValid($fields['username_or_email']);
        }
        
        // ---------------------------CAPTCHA-------------------------------------------------------
        switch($code_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_EMPTY:
                return self::ERROR_CAPTCHA_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_INVALID:
                return self::ERROR_CAPTCHA_IS_INVALID;
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_TOO_LONG:
                return self::ERROR_CAPTCHA_IS_TOO_LONG;
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_TOO_SHORT:
                return self::ERROR_CAPTCHA_IS_TOO_SHORT;
                break;
            default:
                return $code_validity;
        }
        
        // ---------------------------EMAIL OR USERNAME---------------------------------------------
        if($email_validity === \Core\Utils\Security::NO_ERROR || $username_validity === \Modules\Humpchies\UserModel::NO_ERROR)
            return self::NO_ERROR;
        elseif($email_validity === \Core\Utils\Security::ERROR_EMAIL_IS_EMPTY || $username_validity === \Modules\Humpchies\UserModel::ERROR_USERNAME_IS_EMPTY)
            return self::ERROR_USERNAME_OR_EMAIL_IS_EMPTY;
        
        return self::ERROR_USERNAME_OR_EMAIL_IS_INVALID;
    }
    
    /***
    * put your comment there...
    * 
    * @param mixed $fields
    */
    private static function validatePasswordReset(&$fields)
    {
        // ---------------------------REMOVE UNSUPPORTED FIELDS-------------------------------------
        $fields = array_intersect_key($fields, array(   'password'              => NULL,
                                                        'password_confirmation' => NULL,
                                                        'code'                  => NULL));
        
        // ---------------------------CLEAN UP AND VALIDATE FIELDS---------------------------------- 
        $code_validity                  = \Core\Utils\Security::isInputCaptchaValid($fields['code']);                                                
        $password_validity              = \Modules\Humpchies\UserModel::isPasswordValid($fields['password']);
        $password_confirmation_validity = \Modules\Humpchies\UserModel::isPasswordValid($fields['password_confirmation']);
         
        // ---------------------------CAPTCHA-------------------------------------------------------
        switch($code_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_EMPTY:
                return self::ERROR_CAPTCHA_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_INVALID:
                return self::ERROR_CAPTCHA_IS_INVALID;
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_TOO_LONG:
                return self::ERROR_CAPTCHA_IS_TOO_LONG;
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_TOO_SHORT:
                return self::ERROR_CAPTCHA_IS_TOO_SHORT;
                break;
            default:
                return $code_validity;
        }
         
        // ---------------------------PASSWORD------------------------------------------------------
        switch($password_validity)
        {
            case \Modules\Humpchies\UserModel::NO_ERROR;
                break;
            case \Modules\Humpchies\UserModel::ERROR_PASSWORD_IS_EMPTY:
                return self::ERROR_PASSWORD_IS_EMPTY;
                break;
            case \Modules\Humpchies\UserModel::ERROR_PASSWORD_IS_TOO_LONG:
                return self::ERROR_PASSWORD_IS_TOO_LONG;
                break;
            case \Modules\Humpchies\UserModel::ERROR_PASSWORD_IS_TOO_SHORT:
                return self::ERROR_PASSWORD_IS_TOO_SHORT;
                break;
            default:
                return $password_validity;  
        }    
        
        // ---------------------------MATCH PASSWORD AND PASSWORD CONFIRMATION----------------------    
        if($fields['password'] !== @$fields['password_confirmation'])
            return self::ERROR_PASSWORD_CONFIRMATION_FAILED;
                
        return self::NO_ERROR;     
    }
    
    /***
    * put your comment there...
    * 
    * @param mixed $fields
    */
    private static function validateLogin(&$fields)
    {
        // ---------------------------REMOVE UNSUPPORTED FIELDS-------------------------------------
        $fields = array_intersect_key($fields, array(   'username_or_email'     => NULL, 
                                                        'password'              => NULL,
                                                        'code'                  => NULL,
                                                        'components'            => NULL,
                                                        'kq'                    => NULL,
                                                        'with_captcha'          => NULL)); 
        $username = @$fields['username_or_email'];
        
        // ---------------------------CLEAN UP AND VALIDATE FIELDS----------------------------------
        $email_validity                 = \Core\Utils\Security::isEmailInputValid($fields['username_or_email']);
        $password_validity              = \Modules\Humpchies\UserModel::isPasswordValid($fields['password']);
        $username_validity              = NULL;
              
        if($email_validity !== \Core\Utils\Security::NO_ERROR)
        {
            $fields['username_or_email'] = $username;    
            $username_validity = \Modules\Humpchies\UserModel::isUsernameValid($fields['username_or_email']);
        }
        
        // ---------------------------CAPTCHA-------------------------------------------------------
        if( $fields['with_captcha'] == 1 ){
            $code_validity = \Core\Utils\Security::isInputCaptchaValid($fields['code']);
            switch($code_validity)
            {
                case \Core\Utils\Security::NO_ERROR:
                    break;
                case \Core\Utils\Security::ERROR_CAPTCHA_IS_EMPTY:
                    return self::ERROR_CAPTCHA_IS_EMPTY;
                    break;
                case \Core\Utils\Security::ERROR_CAPTCHA_IS_INVALID:
                    return self::ERROR_CAPTCHA_IS_INVALID;
                    break;
                case \Core\Utils\Security::ERROR_CAPTCHA_IS_TOO_LONG:
                    return self::ERROR_CAPTCHA_IS_TOO_LONG;
                    break;
                case \Core\Utils\Security::ERROR_CAPTCHA_IS_TOO_SHORT:
                    return self::ERROR_CAPTCHA_IS_TOO_SHORT;
                    break;
                default:
                    return $code_validity;
            }
        }
        
        // ---------------------------EMAIL OR USERNAME---------------------------------------------
        if($email_validity === \Core\Utils\Security::ERROR_EMAIL_IS_EMPTY || $username_validity === \Modules\Humpchies\UserModel::ERROR_USERNAME_IS_EMPTY)
            return self::ERROR_USERNAME_OR_EMAIL_IS_EMPTY;
        elseif($email_validity !== \Core\Utils\Security::NO_ERROR && $username_validity !== \Modules\Humpchies\UserModel::NO_ERROR)
            return self::ERROR_USERNAME_OR_EMAIL_IS_INVALID;
        
        // ---------------------------PASSWORD------------------------------------------------------
        switch($password_validity)
        {
            case \Modules\Humpchies\UserModel::NO_ERROR;
                break;
            case \Modules\Humpchies\UserModel::ERROR_PASSWORD_IS_EMPTY:
                return self::ERROR_PASSWORD_IS_EMPTY;
                break;
            case \Modules\Humpchies\UserModel::ERROR_PASSWORD_IS_TOO_LONG:
                return self::ERROR_PASSWORD_IS_TOO_LONG;
                break;
            case \Modules\Humpchies\UserModel::ERROR_PASSWORD_IS_TOO_SHORT:
                return self::ERROR_PASSWORD_IS_TOO_SHORT;
                break;
            default:
                return $password_validity;  
        }    
        
        return self::NO_ERROR;     
    }
    
    private static function redirectHijitosDePuta($mamada_de_texto)
    {
        return;
        foreach(array(  'yopmail',
                        'jourrapide',
                        'N1975123',
                        'fleckens',
                        'cuvox',
                        'teleworm',
                        'einrot',
                        'armyspy',
                        'rhyta',
                        'dayrep',
                        'superrito',
                        'gustr',
                        'sandy1',
                        'izzy1',
                        'jade1',
                        'kathialepine',
                        'postemail',
                        'bepost', 
                        'neufmail', 
                        'belgamail', 
                        'lapostemail', 
                        'sfrmail', 
                        'voilamail', 
                        'cesfemmes', 
                        'marisolhould', 
                        '4freeweb',
                        'jadehorvat', 
                        'veronikagazeau',
                        'cendrinetougas',
                        'evetruffy',
                        'myway',
                        'clipmail',
                        'onelife') as $forbidden_chingadera){
            if(stristr($mamada_de_texto, $forbidden_chingadera))
            {
                //session_destroy();
//                header('Location: http://www.elblogdelnarco.com/');
//                exit();
            }
        }
    }
    
    public function updateloc(){
         $rows = \Modules\Humpchies\UserModel::getipListUpdate();
          //$rows = ['ffff'];
         //var_Dump($rows);
         foreach($rows as $row){
            $ip = ($row["ip_reg"])? $row['ip_reg'] : $row["ip_user"]; 
            $loc = \Core\Utils\Device::getGeoIPLocation($ip);
            $userLoc = \Core\Utils\Device::getGeoIPLocation($row["ip_user"]);
            // var_dump($loc);
            //echo "IP:" . $ip;
            $ipLocInfo      =  [
                            'ip_real'         => $ip,
                            'ip_class'        => \Core\Utils\Device::getDeviceClassbyIP($ip),
                            'user_location'   => $loc['location'],
                            'user_country'    => $loc['country'],
                            'last_ip'         => $row["ip_user"],
                            'last_class'      =>  \Core\Utils\Device::getDeviceClassbyIP($row["ip_user"]),
                            'last_location'   => $userLoc['location'],
                            'last_country'    => $userLoc['country']                             
                           ];
            //var_dump($ipLocInfo); 
            echo "--->" . $row['id'] . "\n";               
            \Modules\Humpchies\UserModel::saveLastLogin( $row['id'], $ipLocInfo );
         }
         die();
    }
    
    
    public static function dashboard() {
        
         // Disable browser cache
        self::disableCache();
        
        // CHECK IF USER IS MEMBER
        if(!\Core\Utils\Session::getCurrentUserID()) {
            self::redirect('/');
        }
        
        // get curent user id
        $loggedInUser = \Modules\Humpchies\UserModel::getDetailsByIdforSettings(\Core\Utils\Session::getCurrentUserID());
        
        // add user details to view
        self::$data["user_details"] = $loggedInUser;//[0];
        self::setHeaderClass("has_breadcrumb");
        
        self::$data["new_header"] = true; // TO BE REMOVED UPON RELEASE
        
        //var_dump($loggedInUser);exit();
    }
    
    
}
?>
