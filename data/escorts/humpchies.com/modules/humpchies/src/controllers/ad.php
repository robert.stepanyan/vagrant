<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class is the default class in the humpchies project,
 * as such it also contains generic or not data structure specific methods.
 */

Namespace Modules\Humpchies;

use Core\Utils\Image;

class AdController extends \Core\Controller{

    // ===========
    // CONSTANTS |
    // ===========  
//
    // BOX LENGTHS
    // ===========
    const MAX_LEN_LIST_ITEM_DESCRIPTION__PC     = 225;                                                                  // The maximum description len for a list item on PC
    const MAX_LEN_LIST_ITEM_DESCRIPTION_MOBILE  = 75;                                                                   // The maximum description len for a list item on Mobile
    const MAX_LEN_LIST_ITEM_DESCRIPTION_SMALL   = 70;                                                                   // The maximum description len for a list item on Mobile
    const MAX_LEN_LIST_ITEM_TITLE_MOBILE        = 20;                                                                   // The maximum title len for a list item on Mobile

    // ERRORS
    // ======
    const ERROR_CATEGORY_IS_INVALID             = 'Modules.Humpchies.AdController.ErrorCategoryIsInvalid';              // Invalid Category
    const ERROR_CITY_IS_INVALID                 = 'Modules.Humpchies.AdController.ErrorCityIsInvalid';                  // Invalid city                                                                                   
    const ERROR_DESCRIPTION_IS_DUPLICATED       = 'Modules.Humpchies.AdController.ErrorDescriptionIsDuplicated';        // Description is empty
    const ERROR_DESCRIPTION_IS_EMPTY            = 'Modules.Humpchies.AdController.ErrorDescriptionIsEmpty';             // Description is empty
    const ERROR_DESCRIPTION_IS_TOO_LONG         = 'Modules.Humpchies.AdController.ErrorDescriptionIsTooLong';           // Description is too long
    const ERROR_DESCRIPTION_IS_TOO_SHORT        = 'Modules.Humpchies.AdController.ErrorDescriptionIsTooShort';          // Description is too short
    const ERROR_IMAGE_ERROR_UNHANDLED           = 'Modules.Humpchies.AdController.ErrorUnhandled';
    const ERROR_IMAGE_SIZE_IS_TOO_BIG           = 'Modules.Humpchies.AdController.ErrorImageSizeIsTooBig';
    const ERROR_IMAGE_TYPE_IS_INVALID           = 'Modules.Humpchies.AdController.ErrorImageTypeIsInvalid';
    const ERROR_IMAGE_RESAMPLING_FAILED         = 'Modules.Humpchies.AdController.ErrorImageResamplingFailed';
    const ERROR_LOCATION_IS_INVALID             = 'Modules.Humpchies.AdController.ErrorLocationIsInvalid';              // Location is Invalid
    const ERROR_PAGE_NUMBER_IS_INVALID          = 'Modules.Humpchies.AdController.ErrorPageNumberIsInvalid';            // Invalid page number
    const ERROR_PHONE_IS_EMPTY                  = 'Modules.Humpchies.AdController.ErrorPhoneIsEmpty';                   // Phone number is empty
    const ERROR_PHONE_IS_INVALID                = 'Modules.Humpchies.AdController.ErrorPhoneIsInvalid';                 // Phone number is inavlid
    const ERROR_PRICE_CONDITIONS_IS_EMPTY       = 'Modules.Humpchies.AdController.ErrorPriceConditionsIsEmpty';         // Price conditions is empty
    const ERROR_PRICE_CONDITIONS_IS_TOO_LONG    = 'Modules.Humpchies.AdController.ErrorPriceConditionsIsTooLong';       // Price conditions is too long
    const ERROR_PRICE_CONDITIONS_IS_TOO_SHORT   = 'Modules.Humpchies.AdController.ErrorPriceConditionsIsTooShort';      // Price conditions is too short
    const ERROR_PRICE_IS_INVALID                = 'Modules.Humpchies.AdController.ErrorPriceIsInvalid';                 // Invalid price amount
    const ERROR_SEARCH_QUERY_IS_EMPTY           = 'Modules.Humpchies.AdController.ErrorSearchQueryIsEmpty';             // Search query is empty
    const ERROR_SEARCH_QUERY_IS_INVALID         = 'Modules.Humpchies.AdController.ErrorSearchQueryIsInvalid';           // Search query is empty
    const ERROR_SEARCH_QUERY_IS_TOO_LONG        = 'Modules.Humpchies.AdController.ErrorSearchQueryIsTooLong';           // Search query is too long
    const ERROR_SEARCH_QUERY_IS_TOO_SHORT       = 'Modules.Humpchies.AdController.ErrorSearchQueryIsTooShort';          // Search query is too short
    const ERROR_TITLE_IS_EMPTY                  = 'Modules.Humpchies.AdController.ErrorTitleIsEmpty';                   // Title is empty
    const ERROR_TITLE_IS_TOO_LONG               = 'Modules.Humpchies.AdController.ErrorTitleIsTooLong';                 // Title is too long
    const ERROR_TITLE_IS_TOO_SHORT              = 'Modules.Humpchies.AdController.ErrorTitleIsTooShort';                // Title is too short
    const ERROR_USER_ID_IS_INVALID              = 'Modules.Humpchies.AdController.ErrorUserIDIsInvalid';                // Invalid user id
    const ERROR_USER_IS_NOT_ALLOWED             = 'Modules.Humpchies.AdController.ErrorUserIsNotAllowed';               // User is not allowed

    const ERROR_NOT_SUPPORTED_COUNTRY           = 'Modules.Humpchies.AdController.ErrorNotSupportedCountry';
    const ERROR_404_NOT_FOUND                   = 'Modules.Humpchies.AdController.ErrorNotFound';
    const ERROR_MISSING_ACCESS_KEY              = 'Modules.Humpchies.AdController.ErrorMissingAccessKey';
    const ERROR_INVALID_ACCESS_KEY              = 'Modules.Humpchies.AdController.ErrorInvalidAccessKey';
    const ERROR_INVALID_API_FUNCTION            = 'Modules.Humpchies.AdController.ErrorInvalidApiFunction';
    const ERROR_NO_PHONE_NUMBER_PROVIDED        = 'Modules.Humpchies.AdController.ErrorPhoneNumberNotProvided';
    const ERROR_NON_NUMERIC_PHONE_NUMBER_PROVIDED = 'Modules.Humpchies.AdController.ErrorNonNumericPhoneNumberProvided';
    const ERROR_INVALID_COUNTRY_CODE           = 'Modules.Humpchies.AdController.ErrorInvalidCountryCode';
    const ERROR_USAGE_LIMIT_REACHED            = 'Modules.Humpchies.AdController.ErrorUserLimitReached';
    const ERROR_HTTPS_ACCESS_RESTRICTED        = 'Modules.Humpchies.AdController.ErrorHttpsAccessRestricted';
    const ERROR_INACTIVE_USER                  = 'Modules.Humpchies.AdController.ErrorInactiveUser';
    const ERROR_UNKNOWN                        = 'Modules.Humpchies.AdController.ErrorUnknown';

    const NO_ERROR                              = 'Modules.Humpchies.AdController.NoError';                             // Life is beautiful: no errors
    
    static $noadCity     = null;
    static $noadCategory = null;

    // FORMS
    // =====
    const MAX_LEN_PHONE_ON_FORM                 = 14;                                                                   // The maximum length of a phone number when posted in a form i.e (234) 789-2345

    // SEO
    // ===
    const MAX_LEN_HEAD_DESCRIPTION              = 156;                                                                  // Maximum length of an ad <HEAD> description

    // VALIDATION
    // ==========
    const DELETE_KEY                            = '45ad45edf5a12e9f587ad019e78d30ad';                                   // A key to create a validation token for ad deletion

    // FLOOD CONTROL
    // =============
    const MIN_AD_POST_TIME_SPAN                 = 60;
    const MIN_AD_POST_TIME_SPAN_INITIAL_PERIOD  = 600;                                                                 // 10 minutes
    const AD_POST_INITIAL_RESTRICTION_PERIOD    = 172800;
    const REDIS_CITY_PREMIUM                    = "premium::%s";
    const REDIS_CITY_SHOWN                      = "premiumShow::%s";
    const REDIS_CITY_CATEGORY_ORDER             = "ordercity::%s::%s";

    public static function comments(){
        
       // var_dump(\Modules\Humpchies\CommentsVoteModel::getUserVote(86523, 77));
       // var_dump(\Modules\Humpchies\CommentsVoteModel::getUserVote(86523, 78));
//        var_dump(\Modules\Humpchies\CommentsVoteModel::create(86523, 75, 1928164, 'down'));
//        var_dump(\Modules\Humpchies\CommentsVoteModel::create(86523, 74, 1928164, 'up'));
//        
           \Core\Controller::setExtraHead('
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/comments_vote.js?v=1.08"></script>
                                        '); 
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . 'new_modal.css?v=1.07">');
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) .'/list.css?v=1.12">');
        \Core\Controller::setExtraHead('<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">');
        
        // ==================================================== //
        
        // SEO VALUES
        // ==========

        // Build the page title...
        $listsize= 25;
        $page_number = ($_GET['page'] === NULL ? 1 : (int)$_GET['page']);
        $page_url = 'comments';
        
        self::setTitle("Comment page ". $page_number);  
        self::setHeaderClass("has_breadcrumb");     // has breadcrumb
         
        $data = \Modules\Humpchies\CommentsModel::getComments($page_number, $listsize);        
        $comments = [];
        //var_dump($data);
        foreach ($data["recordset"] as $record){
           $comment = [];
           $comment['id'] = $record['id'];
           $comment['up'] = $record['up'];
           $comment['down'] = $record['down'];
           $comment['ad_id'] = $record['ad_id'];
           $comment['comment'] = \Core\Utils\Security::htmlEncodeText($record['comment']);
           $comment['title'] = \Core\Utils\Security::htmlEncodeText($record['title']);
           $comment['user'] = \Core\Utils\Security::htmlEncodeText($record['username']);
           $comment['date'] = date("d M Y", strtotime($record['date']));
           if($record['hash']){
               $category = substr($record['ad_id'], 0, 2) . DIRECTORY_SEPARATOR . substr($record['ad_id'], 2). DIRECTORY_SEPARATOR;
               $comment['image'] = \Core\Utils\Device::getProtocol() .
                \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_images') . $category  . 'img-' . $record['hash'].".jpg";  
           } else{
               $comment['image'] = "https://gui.humpchies.com/gui_ads/filler.jpg";
           }
           
           $comment['link'] = self::getDetailsURL($record['category'], \Core\Utils\Text::stringToUrl($record['city']),$record['ad_id'], $record['title']);
           $comment['urlCity'] = self::getListURL($record['category'], \Core\Utils\Text::stringToUrl($record['city']));
           $comment['city'] =  \Core\Utils\Text::stringToUrl($record['city']) ;
           
           $comment['category_name'] =  '[LABEL-' . strtoupper($record['category'])  . ']';
           $comment['category_url'] = self::getListURL($record['category']);
           $comment['category_href_title'] = '[HREF-TITLE-' . strtoupper($record['category']) . '-PART-1] [LABEL-MONTREAL]';
           
           $comments[] = $comment;
        }                                                                       // The current page number
        //var_dump($comments);
        self::$data['list'] = $comments;
        self::$data["user_votes"] = \Modules\Humpchies\CommentsVoteModel::getVotesComenntsIds(\Core\Utils\Session::getCurrentUserID());
        
        self::pushPlugin('paginator', new \Plugins\Paginator( $data['count'], $page_number, $listsize, $page_url, FALSE, $page_url));
    }
    
    public static function browse() {   
        if(isset($_GET['trg']) && $_GET['trg'] == 1) {
            $geoLocation = \Core\Utils\Session::getGeoLocationNew();
           // $db = new \Core\Ip2loc\Database('/code/core/src/ip2loc/data/IP-COUNTRY-REGION-CITY-LATITUDE-LONGITUDE.BIN', \Core\Ip2loc\Database::FILE_IO);
//
//            $records = $db->lookup('8.8.8.8', \Core\Ip2loc\Database::ALL);
//            $fields['ip']           = $records['ipAddress']; 
//            $fields['ipVersion']    = $records['ipVersion']; 
//            $fields['ipNumber']     = $records['ipNumber']; 
//            $fields['latitude']     = $records['latitude']; 
//            $fields['longitude']    = $records['longitude']; 
//            $fields['countryName']  = $records['longitude']; 
//            $fields['countryCode']  = $records['countryCode']; 
//            $fields['cityName']     = $records['cityName']; 
//            $fields['regionName']   = $records['regionName'];
//            echo "<pre>";
            var_dump($geoLocation);
//            echo "</pre>";
//            //var_dump($geoLocation->subdivisions, $geoLocation->city->names->en, $_GET['city']);
            die();
        }

        $geoloc = \Core\Utils\Session::getGeoLocation();
        /**
         * Since they will be heavily used down the road, validate the GET parameters.
         * Any error at this point will trigger a 404. This is because we'd either
         * have an invalid category, or an invalid city, or an invalid page number,
         * or something shady might be going on.
         */
        $browse_validity = self::validateBrowse($_GET);
        $_SESSION['goback'] = $_SERVER['REQUEST_URI'];
        switch($browse_validity) {
            // Yes it is! Simply move on to retrieve the list of ads
            case self::NO_ERROR:
                break;

            // No is NOT! Throw a 404, and advice calling context of failure
            default:
                \Core\Router::throw404();
                return FALSE;
        }

        // LOAD THE NEW HEADER
        //self::$data["new_header"] = true;
        
        // process get params
        $list_category = isset($_GET['category'])? $_GET['category'] : NULL;
        $list_region = isset($_GET['region'])? $_GET['region'] : NULL;
        $list_city = isset($_GET['city'])? $_GET['city'] : NULL;
        
        
        
        // Get the current page number... if null then this is page number 1
        $page_number = ($_GET['page'] === NULL ? 1 : (int)$_GET['page']);
        self::$data['show_filter'] = true;
        self::$data['price_intervals'] = \Modules\Humpchies\AdModel::$pricesIntervals;
        
        // check if we have any filters
        if($_POST) {
            
            // update session
            $_SESSION["filters"]['services']         = (isset($_POST['services']) ? $_POST['services'] : '');
            $_SESSION["filters"]['prices']           = (isset($_POST['prices']) ? $_POST['prices'] : '');
            $_SESSION["filters"]['location']         = (isset($_POST['location']) ? $_POST['location'] : '');
            $_SESSION["filters"]['phone1']           = (isset($_POST['phone1']) ? $_POST['phone1'] : '');
            $_SESSION["filters"]['phone2']           = (isset($_POST['phone2']) ? $_POST['phone2'] : '');
            $_SESSION["filters"]['phone3']           = (isset($_POST['phone3']) ? $_POST['phone3'] : '');
            $_SESSION["filters"]['accepts_comments'] = ((isset($_POST['accepts_comments']) && $_POST['accepts_comments'] == 1)? 1 : '');
            $_SESSION["filters"]['has_comments']     = ((isset($_POST['has_comments']) && $_POST['has_comments'] == 1)? 1 : '');
            $_SESSION["filters"]['real_photos']     = ((isset($_POST['real_photos']) && $_POST['real_photos'] == 1)? 1 : '');
            
            $page_number = 1; // reset page
        }
        
        if(!$_POST && $_GET['page'] === NULL) { // reset session when page is simply reloaded
            $_SESSION["filters"]['services'] = '';
            $_SESSION["filters"]['prices'] = '';      
            $_SESSION["filters"]['location'] = '';   
            $_SESSION["filters"]['accepts_comments'] = '';   
            $_SESSION["filters"]['has_comments'] = '';   
            $_SESSION["filters"]['real_photos'] = '';   
            $_SESSION["filters"]['phone1'] = '';   
            $_SESSION["filters"]['phone2'] = '';   
            $_SESSION["filters"]['phone3'] = '';   
        }
        
        self::$data['location_filter']          =  $_SESSION["filters"]['location'];
        self::$data['services_filter']          =  $_SESSION["filters"]['services'];                                                                                                                                                                                                                                                                                                                                                                 
        self::$data['prices_filter']            =  $_SESSION["filters"]['prices'];                                                                                                                                                                                                                                                                                                                                                                  
        self::$data['has_comments_filter']      =  $_SESSION["filters"]['has_comments'];                                                                                                                                                                                                                                                                                                                                                                 
        self::$data['accepts_comments_filter']  =  $_SESSION["filters"]['accepts_comments'];                                                                                                                                                                                                                                                                                                                                                                 
        self::$data['real_photos_filter']       =  $_SESSION["filters"]['real_photos'];                                                                                                                                                                                                                                                                                                                                                                 
        self::$data['phone1_filter']            =  $_SESSION["filters"]['phone1'];                                                                                                                                                                                                                                                                                                                                                                 
        self::$data['phone2_filter']            =  $_SESSION["filters"]['phone2'];                                                                                                                                                                                                                                                                                                                                                                 
        self::$data['phone3_filter']            =  $_SESSION["filters"]['phone3'];                                                                                                                                                                                                                                                                                                                                                                 
        // =================================//
        
        $lang =  (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';
        self::$data["tags"] = \Modules\Humpchies\TagModel::getTagsList($lang);
       
        // Setup VIPs and Premims
        self::setupVIPAds($list_category);
        self::setupPremiumAds($list_category);
        
        //$list_size = 1;
        $list_size = 25;

        $GroupedAds = \Modules\Humpchies\AdModel::getListOfActiveAdsByCategoryAndCityV2($list_category, $list_region, $list_city, $_SESSION['filters'], $page_number, $list_size);
        /*}*/
        self::$data['ads'] = $GroupedAds['ads'];
        self::$data['vip_ads'] = $GroupedAds['vip_ads'];
        self::$data['premium_ads'] = $GroupedAds['premium_ads'];

        // If no ads are found, throw a 404, and advice calling context of failure
        if(!self::$data['ads'] && !self::$data['vip_ads'] && !self::$data['premium_ads']) {
            \Core\Router::throw404();
            return FALSE;
        }
    
        if(is_array(self::$data['vip_ads'])){
            foreach(self::$data['vip_ads'] as &$ad) {
                self::prepareListAdForGUI($ad);
            }   
        }
        
        if(is_array(self::$data['premium_ads'])){
            foreach(self::$data['premium_ads'] as &$ad) {
                self::prepareListAdForGUI($ad);
            }
        }

        // Get the basic url of the current list
        $list_url = self::getListURL($list_category, $list_city, FALSE, $list_region);
        // Get the hash of the page
        $hash = [];
        array_walk($_SESSION["filters"], function($value, $key) use (&$hash) {
            if($value != ''){
                $hash[] =   $key . '=' . $value; 
        }
        });
        $hashString = (!empty($hash))? '#' . implode('&', $hash) : '';

        
        // Get the root url of the current list
        $list_root_url = ($list_category === NULL && $list_city === NULL  && $list_region === NULL ? '/' : $list_url);
        //$list_root_url =  $list_url;

        // Build and push a page paginator plugin...
        self::pushPlugin('paginator', new \Plugins\Paginator( self::$data['ads']['count'], $page_number, $list_size, $list_url, FALSE, $list_root_url, $hashString));

        //***** DISPLAY CITIES *** //
        /*$cities_zones = \Modules\Humpchies\CityModel::getCitiesByZones();
        reset($cities_zones);
        $selected_area = key($cities_zones);
        $selected_city = $_GET['city'];
        $selected_city_name = '[LABEL-SELECT-CITY]';
        
        array_walk($cities_zones, function ($area_cities, $key) use ($selected_city, &$selected_area, &$selected_city_name) {
            
            if(isset($area_cities[$selected_city])) {
                $selected_area = $key;
                $selected_city_name = $area_cities[$selected_city];                
            }
            return $selected_area;
        });
        */
        $cities_zones = \Modules\Humpchies\CityModel::getCitiesRegions();
        
        reset($cities_zones);
        $selected_area = (isset($_GET['region']))? $_GET['region'] : key($cities_zones);
        $selected_city = $list_city;
        $selected_city_name =  (isset($_GET['region']))? '[LABEL-ALL-CITIES]' : '[LABEL-SELECT-CITY]';
        
        array_walk($cities_zones, function ($area_cities, $key) use ($selected_city, &$selected_area, &$selected_city_name) {
            
            $city_in_area = array_search($selected_city, array_column($area_cities['cities'], 'slug'));
            if($city_in_area !== false) {
                $selected_area = $key;
                $city_details = \Modules\Humpchies\CityModel::getCityDetails($selected_city);
                $selected_city_name =   $city_details['name'];               
            }
            return $selected_area;
        });
        
        self::$data["cities_with_zones"] = $cities_zones;
        self::$data["current_city_code"] = $selected_city;
        self::$data["current_city_name"] = $selected_city_name;
        self::$data["current_area_code"] = $selected_area;
        self::$data['category_code']     = $list_category;
        self::$data['region']            = isset($_GET['region'])? $_GET['region'] : '';

        
        \Core\Controller::setExtraHead('
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/filters_modal.js?v=1.22"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/filters_box.js?v=1.18"></script>
                                        ');
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . 'new_modal.css?v=1.16">');
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) .'/list.css?v=1.16">');
        
        // ==================================================== //
        
        // SEO VALUES
        // ==========

        // Build the page title...
        self::setTitle(self::getListTitle(  $list_category,                                                                              // Use the passed category code
            \Modules\Humpchies\CityModel::parseCityWithAdsCode($list_city, $list_category, FALSE),    // Parse the city into a humanly readable value
            $page_number));                                                                                 // The current page number

        /***
         * At this point we'll build a few things got SEO. These items are
         * Keywords, description and in page text. Nevertheless, we'd want
         * these values to be dynamic and relevant to the key contents of
         * the page. This implies making the SEO values sensitive to the
         * cities and categories found in the ads to build a set of values
         * that truly depict what's on the page.
         */
        $category_names = NULL;                             // This variable will be used to accumulate the category names
        $city_names = NULL;                                 // This variable will be used to accumulate the city names
        $categories_and_cities = NULL;                      // This varible will be used to format and accumulate category and city names to be used later for keywords and in page text
        $categories_and_cities_for_in_page_text = NULL;     // This variable will be used to format and accumulate category and city names to be used later exclusivelly in the in page text

        $visited_ads = [];
        // Loop though each ad in the list...
        foreach(self::$data['ads']['recordset'] as &$ad)
        {
            // add the ids to update the visits counter
            array_push($visited_ads, $ad['id']);
            
            // Parse the ads into humanly readable values...
            self::prepareListAdForGUI($ad);

            // Grab and accumulate the ad's category name...
            $category_names[] = $ad['category_name'];

            // Grab and accumulate the ad's city name...
            $city_names[] = $ad['city_name'];

            // Build and accumulate a string that contains the ad's category and city name separated by a space...
            $categories_and_cities[] = $ad['category_name'] . ' ' . $ad['city_name'];

            // Build and accumulate a string that contains the ad's category and city name with a conjunctiom in between them...
            $categories_and_cities_for_in_page_text[] = $ad['category_name'] . ' [LABEL-IN] ' . $ad['city_name'];
        }
        // update visits counter for all the ads displayed
        
        /**
         * There might be repeated categories and cities in the ads. Therefore,
         * clean up the acumulators to make the values inside them unique...
         */
        $category_names = array_unique($category_names);
        $city_names = array_unique($city_names);
        $categories_and_cities = array_unique($categories_and_cities);
        $categories_and_cities_for_in_page_text = array_unique($categories_and_cities_for_in_page_text);

        // Comma separate the categories and cities combinations
        $categories_and_cities = implode(',', $categories_and_cities);

        /**
         * If after filtering there are 2 or more categories... add commas and
         * a conjunction for the last category name
         */
        if(count($category_names) > 1) {
            $temp = array_pop($category_names);                                             // Grab the last category name
            $category_names = implode(', ', $category_names) . ' [LABEL-AND] ' . $temp;     // Glue everything together, add commas and a conjunction for the last category name
        } else
            $category_names = $category_names[0];

        /**
         * If after filtering there are 2 or more cities... add commas and
         * a conjunction for the last city name
         */
        if(count($city_names) > 1) {
            $temp = array_pop($city_names);                                                 // Grab the last city name
            $city_names = implode(', ', $city_names) . ' [LABEL-AND] ' . $temp;             // Glue everything together, add commas and a conjunction for the last city name
        } else
            $city_names = $city_names[0];

        /**
         * If after filtering there are 2 or more categories and cities for
         * in page text... add commas and a conjunction for the last category
         * and city combination
         */
        if(count($categories_and_cities_for_in_page_text) > 1) {
            // Grab the last category and city combination...
            $temp = array_pop($categories_and_cities_for_in_page_text);

            // Glue everything together, add commas and a conjunction for the last category and city combination...
            $categories_and_cities_for_in_page_text = implode(', ', $categories_and_cities_for_in_page_text) . ' [LABEL-AND] ' . $temp;
        } else
            $categories_and_cities_for_in_page_text = $categories_and_cities_for_in_page_text[0];

        // Ony generate dynamic descriptions for pages other than the home page
        self::$data['city_name'] = $city_names;
        if(FALSE === empty(\Core\Router::getParsedURL()->public_controller_name) && empty(\Core\Router::getParsedURL()->public_action_name)) {
            // Assemble the page description...
            self::setDescription('[PREFIX-LIST-DESCRIPTION] ' . $category_names . ' [SUFFIX-LIST-DESCRIPTION] ' . $city_names);
        }

        // Assemble the in page's in page text
        self::setInPageText('[IN-PAGE-TEXT-LIST-PART-1] ' . self::getDescription() . '[IN-PAGE-TEXT-LIST-PART-2] ' . $categories_and_cities_for_in_page_text . '[IN-PAGE-TEXT-LIST-PART-3]');
        
        // breadcrumbz
         if($list_region !== NULL) {
             $region =  \Modules\Humpchies\RegionModel::getRegionDetails($list_region);
             self::$data["breadcrumb"]['region'] = ($list_city == 'montreal')? 'Montreal' : $region["name"];
            self::setHeaderClass("has_breadcrumb");
        }    
        if($list_city !== NULL) {
            self::$data["breadcrumb"]['city'] = ($list_city == 'montreal')? 'Montreal' : $city_names;
            self::setHeaderClass("has_breadcrumb");
        }
        
        if($list_category) {
            // get categ name and url
            self::$data["breadcrumb"]['category_url'] = self::getListURL($list_category, NULL);
            self::$data["breadcrumb"]['category_title'] = '[LABEL-' . strtoupper($_GET['category'])  . ']';
            
            if($list_city !== NULL) {
                // get city name
                $city_names = ($list_city == 'montreal')? 'Montreal' : $city_names;
                self::$data["breadcrumb"]['city'] = self::$data["breadcrumb"]['category_title'] . ' [IN-PAGE-TEXT-AD-DETAILS-PART-3] ' .$city_names;
            }            
            self::setHeaderClass("has_breadcrumb");
        }
        
        // get meta description
        switch(true) {
            
            case ($list_region !== null):
                $lang =  (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';
                self::$data["custom_description"] = \Modules\Humpchies\SeoDescriptionModel::getSEODescriptionRegion($list_region, $page_number, $lang);
                break;
            
            case ($list_city !== null):
                $lang =  (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';
                self::$data["custom_description"] = \Modules\Humpchies\SeoDescriptionModel::getSEODescriptionCity($list_city, $page_number, $lang);

                break;    
            
            default: 
                break;    
        }
        // make sure maxmimum banner is not doubled on mobile
        self::$data['max_banner'] = true;
    }
    
    
    
    
    public static function browse_v2() {   
                                     
        
        if(isset($_GET['trg']) && $_GET['trg'] == 1) {
            $geoLocation = \Core\Utils\Session::getGeoLocation();
            var_dump($geoLocation->subdivisions, $geoLocation->city->names->en, $_GET['city']);
            die;
        }

        $geoloc = \Core\Utils\Session::getGeoLocation();
        /**
         * Since they will be heavily used down the road, validate the GET parameters.
         * Any error at this point will trigger a 404. This is because we'd either
         * have an invalid category, or an invalid city, or an invalid page number,
         * or something shady might be going on.
         */
        $browse_validity = self::validateBrowse($_GET);
        $_SESSION['goback'] = $_SERVER['REQUEST_URI'];
        switch($browse_validity) {
            // Yes it is! Simply move on to retrieve the list of ads
            case self::NO_ERROR:
                break;

            // No is NOT! Throw a 404, and advice calling context of failure
            default:
                \Core\Router::throw404();
                return FALSE;
        }

        // LOAD THE NEW HEADER
        self::$data["new_header"] = true;
        
        // Get the current page number... if null then this is page number 1
        $page_number = ($_GET['page'] === NULL ? 1 : (int)$_GET['page']);
        self::$data['show_filter'] = true;
        self::$data['price_intervals'] = \Modules\Humpchies\AdModel::$pricesIntervals;
        //var_dump(\Modules\Humpchies\AdModel::$pricesIntervals);
        
        // check if we have any filters
        if($_POST) {
            
            // update session
            $_SESSION["filters"]['services']         = (isset($_POST['services']) ? $_POST['services'] : '');
            $_SESSION["filters"]['prices']           = (isset($_POST['prices']) ? $_POST['prices'] : '');
            $_SESSION["filters"]['location']         = (isset($_POST['location']) ? $_POST['location'] : '');
            $_SESSION["filters"]['phone1']           = (isset($_POST['phone1']) ? $_POST['phone1'] : '');
            $_SESSION["filters"]['phone2']           = (isset($_POST['phone2']) ? $_POST['phone2'] : '');
            $_SESSION["filters"]['phone3']           = (isset($_POST['phone3']) ? $_POST['phone3'] : '');
            $_SESSION["filters"]['accepts_comments'] = ((isset($_POST['accepts_comments']) && $_POST['accepts_comments'] == 1)? 1 : '');
            $_SESSION["filters"]['has_comments']     = ((isset($_POST['has_comments']) && $_POST['has_comments'] == 1)? 1 : '');
            $_SESSION["filters"]['real_photos']     = ((isset($_POST['real_photos']) && $_POST['real_photos'] == 1)? 1 : '');
            
            $page_number = 1; // reset page
        }
        
        if(!$_POST && $_GET['page'] === NULL) { // reset session when page is simply reloaded
            $_SESSION["filters"]['services'] = '';   
            $_SESSION["filters"]['prices'] = '';   
            $_SESSION["filters"]['location'] = '';   
            $_SESSION["filters"]['accepts_comments'] = '';   
            $_SESSION["filters"]['has_comments'] = '';   
            $_SESSION["filters"]['real_photos'] = '';   
            $_SESSION["filters"]['phone1'] = '';   
            $_SESSION["filters"]['phone2'] = '';   
            $_SESSION["filters"]['phone3'] = '';   
        }
        
        self::$data['location_filter']          =  $_SESSION["filters"]['location'];
        self::$data['services_filter']          =  $_SESSION["filters"]['services'];                                                                                                                                                                                                                                                                                                                                                                 
        self::$data['prices_filter']            =  $_SESSION["filters"]['prices'];                                                                                                                                                                                                                                                                                                                                                                 
        self::$data['has_comments_filter']      =  $_SESSION["filters"]['has_comments'];                                                                                                                                                                                                                                                                                                                                                                 
        self::$data['accepts_comments_filter']  =  $_SESSION["filters"]['accepts_comments'];                                                                                                                                                                                                                                                                                                                                                                 
        self::$data['real_photos_filter']       =  $_SESSION["filters"]['real_photos'];                                                                                                                                                                                                                                                                                                                                                                 
        self::$data['phone1_filter']            =  $_SESSION["filters"]['phone1'];                                                                                                                                                                                                                                                                                                                                                                 
        self::$data['phone2_filter']            =  $_SESSION["filters"]['phone2'];                                                                                                                                                                                                                                                                                                                                                                 
        self::$data['phone3_filter']            =  $_SESSION["filters"]['phone3'];                                                                                                                                                                                                                                                                                                                                                                 
        // =================================//
        
        $lang =  (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';
        self::$data["tags"] = \Modules\Humpchies\TagModel::getTagsList($lang);
       
        // Setup VIPs and Premims
        self::setupVIPAds($_GET['category']);
        self::setupPremiumAds($_GET['category']);
        
        //$list_size = 1;
        $list_size = 25;

        $GroupedAds = \Modules\Humpchies\AdModel::getListOfActiveAdsByCategoryAndCityV2($_GET['category'], $_GET['region'], $_GET['city'], $_SESSION['filters'], $page_number, $list_size);
        
        /*}*/
        self::$data['ads'] = $GroupedAds['ads'];
        self::$data['vip_ads'] = $GroupedAds['vip_ads'];
        self::$data['premium_ads'] = $GroupedAds['premium_ads'];

        // If no ads are found, throw a 404, and advice calling context of failure
        if(!self::$data['ads'] && !self::$data['vip_ads'] && !self::$data['premium_ads']) {
            \Core\Router::throw404();
            return FALSE;
        }
    
        if(is_array(self::$data['vip_ads'])){
            foreach(self::$data['vip_ads'] as &$ad) {
                self::prepareListAdForGUI($ad);
            }   
        }
        
        if(is_array(self::$data['premium_ads'])){
            foreach(self::$data['premium_ads'] as &$ad) {
                self::prepareListAdForGUI($ad);
            }
        }

        // Get the basic url of the current list
        $list_url = self::getListURLV2($_GET['category'], $_GET['city'], FALSE, $_GET['region']);
        // Get the hash of the page
        // Get the hash of the page
        $hash = [];
        array_walk($_SESSION["filters"], function($value, $key) use (&$hash) {
            if($value != ''){
                $hash[] =   $key . '=' . $value; 
        }
        });
        $hashString = (!empty($hash))? '#' . implode('&', $hash) : '';
        
        // Get the root url of the current list
        //$list_root_url = ($_GET['category'] === NULL && $_GET['city'] === NULL  && $_GET['region'] === NULL ? '/' : $list_url);
        $list_root_url =  $list_url;

        // Build and push a page paginator plugin...
        self::pushPlugin('paginator', new \Plugins\Paginator( self::$data['ads']['count'], $page_number, $list_size, $list_url, FALSE, $list_root_url, $hashString));

        //***** DISPLAY CITIES *** //
        /*$cities_zones = \Modules\Humpchies\CityModel::getCitiesByZones();
        reset($cities_zones);
        $selected_area = key($cities_zones);
        $selected_city = $_GET['city'];
        $selected_city_name = '[LABEL-SELECT-CITY]';
        
        array_walk($cities_zones, function ($area_cities, $key) use ($selected_city, &$selected_area, &$selected_city_name) {
            
            if(isset($area_cities[$selected_city])) {
                $selected_area = $key;
                $selected_city_name = $area_cities[$selected_city];                
            }
            return $selected_area;
        });
        */
        $cities_zones = \Modules\Humpchies\CityModel::getCitiesRegions();
        
        reset($cities_zones);
        $selected_area = (isset($_GET['region']))? $_GET['region'] : key($cities_zones);
        $selected_city = $_GET['city'];
        $selected_city_name =  (isset($_GET['region']))? '[LABEL-ALL-CITIES]' : '[LABEL-SELECT-CITY]';
        
        array_walk($cities_zones, function ($area_cities, $key) use ($selected_city, &$selected_area, &$selected_city_name) {
            
            $city_in_area = array_search($selected_city, array_column($area_cities['cities'], 'slug'));
            if($city_in_area !== false) {
                $selected_area = $key;
                $city_details = \Modules\Humpchies\CityModel::getCityDetails($selected_city);
                $selected_city_name =   $city_details['name'];               
            }
            return $selected_area;
        });
        
        self::$data["cities_with_zones"] = $cities_zones;
        self::$data["current_city_code"] = $selected_city;
        self::$data["current_city_name"] = $selected_city_name;
        self::$data["current_area_code"] = $selected_area;
        self::$data['category_code'] =     $_GET['category'];
        self::$data['region'] =            $_GET['region'];

        
        \Core\Controller::setExtraHead('
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/filters_modal_v2.js?v=1.18"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/filters_box_v2.js?v=1.19"></script>
                                        ');
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . 'new_modal.css?v=1.13">');
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) .'/list_v2.css?v=1.15">');
        
        // ==================================================== //
        
        // SEO VALUES
        // ==========

        // Build the page title...
        self::setTitle(self::getListTitle(  $_GET['category'],                                                                              // Use the passed category code
            \Modules\Humpchies\CityModel::parseCityWithAdsCode($_GET['city'], $_GET['category'], FALSE),    // Parse the city into a humanly readable value
            $page_number));                                                                                 // The current page number

        /***
         * At this point we'll build a few things got SEO. These items are
         * Keywords, description and in page text. Nevertheless, we'd want
         * these values to be dynamic and relevant to the key contents of
         * the page. This implies making the SEO values sensitive to the
         * cities and categories found in the ads to build a set of values
         * that truly depict what's on the page.
         */
        $category_names = NULL;                             // This variable will be used to accumulate the category names
        $city_names = NULL;                                 // This variable will be used to accumulate the city names
        $categories_and_cities = NULL;                      // This varible will be used to format and accumulate category and city names to be used later for keywords and in page text
        $categories_and_cities_for_in_page_text = NULL;     // This variable will be used to format and accumulate category and city names to be used later exclusivelly in the in page text

        $visited_ads = [];
        // Loop though each ad in the list...
        foreach(self::$data['ads']['recordset'] as &$ad)
        {
            // add the ids to update the visits counter
            array_push($visited_ads, $ad['id']);
            
            // Parse the ads into humanly readable values...
            self::prepareListAdForGUI($ad);

            // Grab and accumulate the ad's category name...
            $category_names[] = $ad['category_name'];

            // Grab and accumulate the ad's city name...
            $city_names[] = $ad['city_name'];

            // Build and accumulate a string that contains the ad's category and city name separated by a space...
            $categories_and_cities[] = $ad['category_name'] . ' ' . $ad['city_name'];

            // Build and accumulate a string that contains the ad's category and city name with a conjunctiom in between them...
            $categories_and_cities_for_in_page_text[] = $ad['category_name'] . ' [LABEL-IN] ' . $ad['city_name'];
        }
        // update visits counter for all the ads displayed
        
        /**
         * There might be repeated categories and cities in the ads. Therefore,
         * clean up the acumulators to make the values inside them unique...
         */
        $category_names = array_unique($category_names);
        $city_names = array_unique($city_names);
        $categories_and_cities = array_unique($categories_and_cities);
        $categories_and_cities_for_in_page_text = array_unique($categories_and_cities_for_in_page_text);

        // Comma separate the categories and cities combinations
        $categories_and_cities = implode(',', $categories_and_cities);

        /**
         * If after filtering there are 2 or more categories... add commas and
         * a conjunction for the last category name
         */
        if(count($category_names) > 1) {
            $temp = array_pop($category_names);                                             // Grab the last category name
            $category_names = implode(', ', $category_names) . ' [LABEL-AND] ' . $temp;     // Glue everything together, add commas and a conjunction for the last category name
        } else
            $category_names = $category_names[0];

        /**
         * If after filtering there are 2 or more cities... add commas and
         * a conjunction for the last city name
         */
        if(count($city_names) > 1) {
            $temp = array_pop($city_names);                                                 // Grab the last city name
            $city_names = implode(', ', $city_names) . ' [LABEL-AND] ' . $temp;             // Glue everything together, add commas and a conjunction for the last city name
        } else
            $city_names = $city_names[0];

        /**
         * If after filtering there are 2 or more categories and cities for
         * in page text... add commas and a conjunction for the last category
         * and city combination
         */
        if(count($categories_and_cities_for_in_page_text) > 1) {
            // Grab the last category and city combination...
            $temp = array_pop($categories_and_cities_for_in_page_text);

            // Glue everything together, add commas and a conjunction for the last category and city combination...
            $categories_and_cities_for_in_page_text = implode(', ', $categories_and_cities_for_in_page_text) . ' [LABEL-AND] ' . $temp;
        } else
            $categories_and_cities_for_in_page_text = $categories_and_cities_for_in_page_text[0];

        // Ony generate dynamic descriptions for pages other than the home page
        self::$data['city_name'] = $city_names;
        if(FALSE === empty(\Core\Router::getParsedURL()->public_controller_name) && empty(\Core\Router::getParsedURL()->public_action_name)) {
            // Assemble the page description...
            self::setDescription('[PREFIX-LIST-DESCRIPTION] ' . $category_names . ' [SUFFIX-LIST-DESCRIPTION] ' . $city_names);
        }

        // Assemble the in page's in page text
        self::setInPageText('[IN-PAGE-TEXT-LIST-PART-1] ' . self::getDescription() . '[IN-PAGE-TEXT-LIST-PART-2] ' . $categories_and_cities_for_in_page_text . '[IN-PAGE-TEXT-LIST-PART-3]');
        
        // breadcrumbz
         if($_GET['region'] !== NULL) {
             $region =  \Modules\Humpchies\RegionModel::getRegionDetails($_GET['region']);
             self::$data["breadcrumb"]['region'] = ($_GET['city'] == 'montreal')? 'Montreal' : $region["name"];
            self::setHeaderClass("has_breadcrumb");
        }    
        if($_GET['city'] !== NULL) {
            self::$data["breadcrumb"]['city'] = ($_GET['city'] == 'montreal')? 'Montreal' : $city_names;
            self::setHeaderClass("has_breadcrumb");
        }
        
        if($_GET['category'] !== NULL) {
            // get categ name and url
            self::$data["breadcrumb"]['category_url'] = self::getListURL($_GET['category'], NULL);
            self::$data["breadcrumb"]['category_title'] = '[LABEL-' . strtoupper($_GET['category'])  . ']';
            
            if($_GET['city'] !== NULL) {
                // get city name
                $city_names = ($_GET['city'] == 'montreal')? 'Montreal' : $city_names;
                self::$data["breadcrumb"]['city'] = self::$data["breadcrumb"]['category_title'] . ' [IN-PAGE-TEXT-AD-DETAILS-PART-3] ' .$city_names;
            }            
            self::setHeaderClass("has_breadcrumb");
        }
        
        // get meta description
        switch(true) {
            
            case $_GET["region"] !== null:
                $lang =  (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';
                self::$data["custom_description"] = \Modules\Humpchies\SeoDescriptionModel::getSEODescriptionRegion($_GET["region"], $page_number, $lang);
                break;
            
            case $_GET["city"] !== null:
                $lang =  (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';
                self::$data["custom_description"] = \Modules\Humpchies\SeoDescriptionModel::getSEODescriptionCity($_GET["city"], $page_number, $lang);

                break;    
            
            default: 
                break;    
        }

    }
    
    public static function locate() {                                    
                                      
        \Core\Controller::setExtraHead('<script src="https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.js"></script>');    
        \Core\Controller::setExtraHead('<link href="https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.css" rel="stylesheet" />'); 
        \Core\Controller::setExtraHead('<script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'setup_mapbox_location.js?v=0.2"></script>');
         // hide ads
        \Core\Controller::hideAds();
    }
    
    public static function locate_v2()
    {                                    
        
        
       
        \Core\Controller::setExtraHead('<script type="text/javascript" src="https://api-maps.yandex.ru/2.1/?lang=fr_CA&amp;apikey=a2322142-06fa-4970-a82e-2543c4763bcb"></script>');    
        \Core\Controller::setExtraHead('
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'setup_yandex_location_new.js?v=0.11"></script>'
        );
        
//        exit("here");
        //AdLocationModel::getLocationNear(37,-122);
        // exit("here");
        //self::setHeaderClass("has_breadcrumb");
         // hide ads
        \Core\Controller::hideAds();
    }
    
    public static function json_markers() {
        
        $ads_list = false;
        
        if(isset($_GET['lat']) && isset($_GET['long'])) {
        
            $distance = (isset($_GET['dist']) && intval($_GET['dist']) > 0)? intval($_GET['dist']) : 20;
            // get list of ads by location on map
            $ads_list = AdLocationModel::getLocationNear(floatval($_GET['lat']), floatval($_GET['long']), $distance, 1, null, $total_ads, true);
           
        }
        //if($ads_list) {
//             foreach($ads_list as &$ad) {
//             
//                // Parse the ads into humanly readable values...
//                self::prepareListAdForGUI($ad);  
//                
//                $ad['title'] = mb_convert_encoding($ad['title'], 'UTF-8', 'UTF-8');
//                $ad['description'] = mb_convert_encoding($ad['description'], 'UTF-8', 'UTF-8');
//                
//                               
//             }    
//        } 
        
       // $Result = array(
//            'status' => 'have_results',
//            'count' => count($ads_list),
//            //'count' => 1,
//        );
        header('Content-Type: application/json');
        //var_dump(json_encode($Result, true));
        echo json_encode($ads_list, true);
        //echo json_last_error_msg();
        
        exit();
    }
             
    public static function nearby()
    {                                    

        /**
         * Since they will be heavily used down the road, validate the GET parameters.
         * Any error at this point will trigger a 404. This is because we'd either
         * have an invalid category, or an invalid city, or an invalid page number,
         * or something shady might be going on.
         */
        $browse_validity = self::validateNearby($_GET);

        // Is the browse request valid?
        switch($browse_validity) {
            // Yes it is! Simply move on to retrieve the list of ads
            case self::NO_ERROR:
                break;

            // No is NOT! Throw a 404, and advice calling context of failure
            default:
                \Core\Router::throw404();
                return FALSE;
        }

        // Get the current page number... if null then this is page number 1
        $page_number = ($_GET['page'] === NULL ? 1 : (int)$_GET['page']);
        self::$data['distance_filter'] = (($_GET['distance'] === NULL) ? '20' : $_GET['distance']);
        
       
        // Setup VIPs and Premims
        self::setupVIPAds();
        self::setupPremiumAds();
        
        $list_size = 25;
                
       // $GroupedAds = \Modules\Humpchies\AdModel::getListOfActiveAdsByCategoryAndCityV3($_GET['category'], $_GET['region'], $_GET['city'], $_GET['location'], $page_number, $list_size);
        $GroupedAds = \Modules\Humpchies\AdLocationModel::getLocationNear(floatval($_GET['lat']), floatval($_GET['lng']), self::$data['distance_filter'],$page_number, $list_size, $total_ads);
       //var_dump($GroupedAds);
       // var_dump(AdLocationModel::getLocationNear(floatval($_GET['lat']), floatval($_GET['lng']), self::$data['distance_filter'],$page_number, 2));
        /*}*/
       self::$data['ads'] = $GroupedAds['ads'];
       self::$data['vip_ads'] = $GroupedAds['vip_ads'];
       self::$data['premium_ads'] = $GroupedAds['premium_ads'];
                
        // If no ads are found, throw a 404, and advice calling context of failure
        if(!self::$data['ads'] && !self::$data['vip_ads'] && !self::$data['premium_ads'])
        {
            \Core\Router::throw404();
            return FALSE;
        }
                               

        if(is_array(self::$data['vip_ads'])){
            foreach(self::$data['vip_ads'] as &$ad) {
                self::prepareListAdForGUI($ad);
             }    
        }
        
        if(is_array(self::$data['premium_ads'])){
            foreach(self::$data['premium_ads'] as &$ad) {
                self::prepareListAdForGUI($ad);
            }
        }
        
        // Get the basic url of the current list
        //$list_url =  \Core\Utils\Navigation::urlFor('ad', 'nearby', array('lat' => floatval($_GET['lat']), 'lng' => floatval($_GET['lng'])));
        $list_url =  "/ad/nearby/lat/" . floatval($_GET['lat']) . "/lng/" . floatval($_GET['lng']);

        if(self::$data['distance_filter'] != '') {
            $list_url .= '?distance=' . self::$data['distance_filter'];    
        }

        // Build and push a page paginator plugin...
        self::pushPlugin('paginator', new \Plugins\Paginator( self::$data['ads']['count'], $page_number, $list_size, $list_url, FALSE, $list_url));

        
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . '/modal.css?v=3.13">');
        
        // ==================================================== //
        
        // SEO VALUES
        // ==========

        // Build the page title...
        //self::setTitle(self::getListTitle(  $_GET['category'],                                                                              // Use the passed category code
        //    \Modules\Humpchies\CityModel::parseCityWithAdsCode($_GET['city'], $_GET['category'], FALSE),    // Parse the city into a humanly readable value
        //    $page_number));                                                                                 // The current page number

        /***
         * At this point we'll build a few things got SEO. These items are
         * Keywords, description and in page text. Nevertheless, we'd want
         * these values to be dynamic and relevant to the key contents of
         * the page. This implies making the SEO values sensitive to the
         * cities and categories found in the ads to build a set of values
         * that truly depict what's on the page.
         */
        
        $visited_ads = [];
        // Loop though each ad in the list...
        foreach(self::$data['ads']["recordset"] as &$ad)
        {
            // add the ids to update the visits counter
            array_push($visited_ads, $ad['id']);
        
            // Parse the ads into humanly readable values...
            self::prepareListAdForGUI($ad);

            // Grab and accumulate the ad's category name...
            $category_names[] = $ad['category_name'];

            // Grab and accumulate the ad's city name...
            $city_names[] = $ad['city_name'];

            // Build and accumulate a string that contains the ad's category and city name separated by a space...
            $categories_and_cities[] = $ad['category_name'] . ' ' . $ad['city_name'];

            // Build and accumulate a string that contains the ad's category and city name with a conjunctiom in between them...
            $categories_and_cities_for_in_page_text[] = $ad['category_name'] . ' [LABEL-IN] ' . $ad['city_name'];
    }
        // update visits counter for all the ads displayed
        //\Modules\Humpchies\AdModel::updateNoVisits($visited_ads);
    
        /**
         * There might be repeated categories and cities in the ads. Therefore,
         * clean up the acumulators to make the values inside them unique...
         */
        $category_names = array_unique($category_names);
        $city_names = array_unique($city_names);
        $categories_and_cities = array_unique($categories_and_cities);
        $categories_and_cities_for_in_page_text = array_unique($categories_and_cities_for_in_page_text);

        // Comma separate the categories and cities combinations
        $categories_and_cities = implode(',', $categories_and_cities);

        /**
         * If after filtering there are 2 or more categories... add commas and
         * a conjunction for the last category name
         */
        if(count($category_names) > 1)
        {
            $temp = array_pop($category_names);                                             // Grab the last category name
            $category_names = implode(', ', $category_names) . ' [LABEL-AND] ' . $temp;     // Glue everything together, add commas and a conjunction for the last category name
        }
        else
            $category_names = $category_names[0];

        /**
         * If after filtering there are 2 or more cities... add commas and
         * a conjunction for the last city name
         */
        if(count($city_names) > 1)
        {
            $temp = array_pop($city_names);                                                 // Grab the last city name
            $city_names = implode(', ', $city_names) . ' [LABEL-AND] ' . $temp;             // Glue everything together, add commas and a conjunction for the last city name
        }
        else
            $city_names = $city_names[0];

        /**
         * If after filtering there are 2 or more categories and cities for
         * in page text... add commas and a conjunction for the last category
         * and city combination
         */
        if(count($categories_and_cities_for_in_page_text) > 1)
        {
            // Grab the last category and city combination...
            $temp = array_pop($categories_and_cities_for_in_page_text);

            // Glue everything together, add commas and a conjunction for the last category and city combination...
            $categories_and_cities_for_in_page_text = implode(', ', $categories_and_cities_for_in_page_text) . ' [LABEL-AND] ' . $temp;
        }
        else
            $categories_and_cities_for_in_page_text = $categories_and_cities_for_in_page_text[0];

        // Ony generate dynamic descriptions for pages other than the home page
        if(FALSE === empty(\Core\Router::getParsedURL()->public_controller_name) && empty(\Core\Router::getParsedURL()->public_action_name))
        {
            // Assemble the page description...
            self::setDescription(   '[PREFIX-LIST-DESCRIPTION] '
                .
                $category_names
                .
                ' [SUFFIX-LIST-DESCRIPTION] '
                .
                $city_names);

        }

        // Assemble the in page's in page text
        self::setInPageText(    '[IN-PAGE-TEXT-LIST-PART-1] '
            .
            self::getDescription()
            .
            '[IN-PAGE-TEXT-LIST-PART-2] '
            .
            $categories_and_cities_for_in_page_text
            .
            '[IN-PAGE-TEXT-LIST-PART-3]');
    
        // breadcrumbz
        self::setHeaderClass("has_breadcrumb");
    }

    /**
     * This is the search page
     *
     * @returns NULL
     */
    public static function search()
    {
        /**
         * Since they will be heavily used down the road, validate the GET parameters.
         * Any error at this point will trigger a different action. The errors we are
         * set to handle are an empty query, a query too short, a query too long or
         * an invalid page number. Anything else might indicate that something shady
         * is going on.
         */
        $search_validity = self::validateSearch($_GET);

        // Is the search request valid?
        switch($search_validity)
        {
            // Yes it is! Simply move on to retrieve the list of ads
            case self::NO_ERROR:
                break;

            // No is NOT! handle errors: Search query string is invalid... advise user, reach the home page and advice calling context of failure
            case self::ERROR_SEARCH_QUERY_IS_INVALID:
                self::addMessage('[ERROR-SEARCH-QUERY-IS-INVALID]', 'error');
                self::redirect('/');
                return FALSE;
                break;
            // Search query string is empty... advise user, reach the home page and advice calling context of failure
            case self::ERROR_SEARCH_QUERY_IS_EMPTY:
                self::addMessage('[ERROR-SEARCH-QUERY-IS-EMPTY]', 'error');
                self::redirect('/');
                return FALSE;
                break;

            // Search query is too long... advise user, reach the home page and advice calling context of failure
            case self::ERROR_SEARCH_QUERY_IS_TOO_LONG:
                self::addMessage('[ERROR-SEARCH-QUERY-IS-TOO-LONG] ' . \Modules\Humpchies\AdModel::MAX_LEN_SEARCH_QUERY . ' [LABEL-CHARACTERS].', 'error');
                self::redirect('/');
                return FALSE;
                break;

            // Search query is too short... advise user, reach the home page and advice calling context of failure
            case self::ERROR_SEARCH_QUERY_IS_TOO_SHORT:
                self::addMessage('[ERROR-SEARCH-QUERY-IS-TOO-SHORT] ' . \Modules\Humpchies\AdModel::MIN_LEN_SEARCH_QUERY . ' [LABEL-CHARACTERS].', 'error');
                self::redirect('/');
                return FALSE;
                break;

            // Page number is invalid... throw a 404
            case self::ERROR_PAGE_NUMBER_IS_INVALID:
                \Core\Router::throw404();
                return FALSE;

            // Unhandled error... advise user, reach the home page and advice calling context of failure
            default:
                self::addMessage("[ERROR-UNHANDLED]: $search_validity", 'error');
                self::redirect('/');

                return FALSE;
                break;
        }
        $_SESSION['goback'] = $_SERVER['REQUEST_URI'];
        // Get the current page number... if null then this is page number 1
        $page_number = ($_GET['page'] === NULL ? 1 : (int)$_GET['page']);

        // Get the number of items to diplay per page...
        $list_size = 18;

        // Before conducting the search, look for redirects//if there are any, redirect to that page
        $search_redirect = \Modules\Humpchies\SearchesModel::getSearchDetails($_GET['query']);
//        var_dump($search_redirect);
        if(!empty($search_redirect) && $search_redirect[0]["redirect"] != '' && strpos("$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", $search_redirect[0]["redirect"]) === false){
            self::redirect($search_redirect[0]["redirect"]);    
        }
        
        // check if phone number format
        $search_term = preg_replace("/[^a-zA-Z0-9]/", "", $_GET['query']);
        if(is_numeric($search_term) && strlen($search_term) >= 10) {
           self::$data['ads'] = \Modules\Humpchies\AdModel::searchPhone($search_term, $page_number, $list_size, $_GET["city"]);
        } else {
            // Conduct the search...
            self::$data['ads'] = \Modules\Humpchies\AdModel::search($_GET['query'], $page_number, $list_size, $_GET["city"]);    
        }
        
    
        // If the search is unsuccesful, advise the user, reach the home page and advice the calling context about the failure
        if(self::$data['ads'] === FALSE)
        {
            self::addMessage('[ERROR-SEARCH-QUERY-NO-RESULTS]', 'warning');
            self::redirect('/');
            return FALSE;

            // Make sure we are running the search using a query string...
        }
        elseif(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY) === NULL)
        {
            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => $_GET['query']), FALSE));
            return FALSE;
        }

        if($_GET["city"] !== null) {
            $search_url = \Core\Utils\Navigation::urlFor('ad', 'search', array('city' => $_GET['city']));
            $search_url .= "?query=" . $_GET['query'];   
        } else {
            $search_url = \Core\Utils\Navigation::urlFor('ad', 'search',  array('query' => $_GET['query']), FALSE);    
        }
        
        // Create and push a paginator...
        self::pushPlugin(   'paginator',
            new \Plugins\Paginator( self::$data['ads']['count'],
                $page_number,
                $list_size,
                $search_url,
                FALSE));
        
        
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . '/modal.css?v=3.13">');
        
        // ==================================================== //
        // SEO VALUES
        // ==========

        // Build the title...
        $title =   "[TITLE-SEARCH]: " . $_GET['query'];
        
        if($_GET["city"]) {
            $city_details = \Modules\Humpchies\CityModel::getCityDetails($_GET["city"]);
            $title .= "[SEARCH-IN]" .$city_details["name"]; 
            
            //var_dump($city_details);
            //var_dump("omfg there is a city here");
            
        }
        
        self::setTitle(($title . ($page_number === 1 ? '' : " - [LABEL-PAGE] $page_number")));
        
        // breadcrumb
        self::$data['breadcrumb']["search"] = $_GET['query'];
        self::setHeaderClass("has_breadcrumb");
        
        /***
         * At this point we'll build a few things for SEO. These items are
         * Keywords, description and in page text. Nevertheless, we'd want
         * these values to be dynamic and relevant to the key contents of
         * the page. This implies making the SEO values sensitive to the
         * cities and categories found in the ads to build a set of values
         * that truly depict what's on the page.
         */
        $category_names = NULL;                             // This variable will be used to accumulate the category names
        $city_names = NULL;                                 // This variable will be used to accumulate the city names
        $categories_and_cities = NULL;                      // This varible will be used to format and accumulate category and city names to be used later for keywords and in page text
        $categories_and_cities_for_in_page_text = NULL;     // This variable will be used to format and accumulate category and city names to be used later exclusivelly in the in page text

        // Loop though each ad in the list...
        $visited_ads = [];
        foreach(self::$data['ads']['recordset'] as &$ad)
        {
            // add the ids to update the visits counter
            array_push($visited_ads, $ad['id']);
            
            // Parse the ads into humanly readable values...
            self::prepareListAdForGUI($ad);

            // Grab and accumulate the ad's category name...
            $category_names[] = $ad['category_name'];

            // Grab and accumulate the ad's city name...
            $city_names[] = $ad['city_name'];

            // Build and accumulate a string that contains the ad's category and city name separated by a space...
            $categories_and_cities[] = $ad['category_name'] . ' ' . $ad['city_name'];

            // Build and accumulate a string that contains the ad's category and city name with a conjunctiom in between them...
            $categories_and_cities_for_in_page_text[] = $ad['category_name'] . ' [LABEL-IN] ' . $ad['city_name'];
        }
        
        // update visits counter for all the ads displayed
        //\Modules\Humpchies\AdModel::updateNoVisits($visited_ads);

        /**
         * There might be repeated categories and cities in the ads. Therefore,
         * clean up the acumulators to make the values inside them unique...
         */
        $category_names = array_unique($category_names);
        $city_names = array_unique($city_names);
        $categories_and_cities = array_unique($categories_and_cities);
        $categories_and_cities_for_in_page_text = array_unique($categories_and_cities_for_in_page_text);


        // Comma separate the categories and cities combinations
        $categories_and_cities = implode(',', $categories_and_cities);

        /**
         * If after filtering there are 2 or more categories... add commas and
         * a conjunction for the last category name
         */
        if(count($category_names) > 1)
        {
            $temp = array_pop($category_names);                                             // Grab the last category name
            $category_names = implode(', ', $category_names) . ' [LABEL-AND] ' . $temp;     // Glue everything together, add commas and a conjunction for the last category name
        }
        else
            $category_names = $category_names[0];

        /**
         * If after filtering there are 2 or more cities... add commas and
         * a conjunction for the last city name
         */
        if(count($city_names) > 1)
        {
            $temp = array_pop($city_names);                                                 // Grab the last city name
            $city_names = implode(', ', $city_names) . ' [LABEL-AND] ' . $temp;             // Glue everything together, add commas and a conjunction for the last city name
        }
        else
            $city_names = $city_names[0];

        /**
         * If after filtering there are 2 or more categories and cities for
         * in page text... add commas and a conjunction for the last category
         * and city combination
         */
        if(count($categories_and_cities_for_in_page_text) > 1)
        {
            // Grab the last category and city combination...
            $temp = array_pop($categories_and_cities_for_in_page_text);

            // Glue everything together, add commas and a conjunction for the last category and city combination...
            $categories_and_cities_for_in_page_text = implode(', ', $categories_and_cities_for_in_page_text) . ' [LABEL-AND] ' . $temp;
        }
        else
            $categories_and_cities_for_in_page_text = $categories_and_cities_for_in_page_text[0];

        // Build the description...
        self::setDescription(   $base_title
            .
            ' [LABEL-IN] '
            .
            $category_names
            .
            ' [SUFFIX-LIST-DESCRIPTION] '
            .
            $city_names
            .
            ($page_number === 1 ? '' : " - [LABEL-PAGE] $page_number"));

        // Build the keywords...
        self::setKeywords($categories_and_cities);

        // Build the in page text...
        self::setInPageText(    '[IN-PAGE-TEXT-SEARCH-PART-1] '
            .
            self::getDescription()
            .
            '[IN-PAGE-TEXT-SEARCH-PART-2] '
            .
            $categories_and_cities_for_in_page_text
            .
            '[IN-PAGE-TEXT-SEARCH-PART-3]');
    }    
    
    public static function verified()
    {
  
        // Get the current page number... if null then this is page number 1
        $page_number = ($_GET['page'] === NULL ? 1 : (int)$_GET['page']);

        // Get the number of items to diplay per page...
        $list_size = 18;

        // Conduct the search...
        self::$data['ads'] = \Modules\Humpchies\AdModel::getListOfActiveAdsByVerified( $page_number, $list_size);
    
        // If the search is unsuccesful, advise the user, reach the home page and advice the calling context about the failure
        if(self::$data['ads'] === FALSE)
        {
            self::addMessage('[ERROR-SEARCH-QUERY-NO-RESULTS]', 'warning');
            self::redirect('/');
            return FALSE;

            // Make sure we are running the search using a query string...
        }
        // Create and push a paginator...
        self::pushPlugin(   'paginator',
            new \Plugins\Paginator( self::$data['ads']['count'],
                $page_number,
                $list_size,
                \Core\Utils\Navigation::urlFor('ad', 'verified', array('query' => $_GET['query']), FALSE),
                FALSE));

        // Create and push a generic city list
        //self::pushPlugIn('city_list', new \Plugins\HumpchiesCityList());

//        $cities_zones = \Modules\Humpchies\CityModel::getCitiesByZones();
        $cities_zones = \Modules\Humpchies\CityModel::getCitiesRegions();
        reset($cities_zones);
        $selected_area = key($cities_zones);
        $selected_city = '';
        $selected_city_name = '[LABEL-SELECT-CITY]';
        self::$data["cities_with_zones"] = $cities_zones;
        self::$data["current_city_code"] = $selected_city;
        self::$data["current_city_name"] = $selected_city_name;
        self::$data["current_area_code"] = $selected_area;
        self::$data['category_code'] =     $_GET['category'];
        
        \Core\Controller::setExtraHead('
                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'cities_modal.js?v=1.13"></script>');
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . '/modal.css?v=3.13">');
        
        // ==================================================== //
        
        
        
        // SEO VALUES
        // ==========

        // Build the title...
        self::setTitle((($base_title = "[TITLE-VERIFIED] " ) . ($page_number === 1 ? '' : " - [LABEL-PAGE] $page_number")));

        /***
         * At this point we'll build a few things for SEO. These items are
         * Keywords, description and in page text. Nevertheless, we'd want
         * these values to be dynamic and relevant to the key contents of
         * the page. This implies making the SEO values sensitive to the
         * cities and categories found in the ads to build a set of values
         * that truly depict what's on the page.
         */
        $category_names = NULL;                             // This variable will be used to accumulate the category names
        $city_names = NULL;                                 // This variable will be used to accumulate the city names
        $categories_and_cities = NULL;                      // This varible will be used to format and accumulate category and city names to be used later for keywords and in page text
        $categories_and_cities_for_in_page_text = NULL;     // This variable will be used to format and accumulate category and city names to be used later exclusivelly in the in page text
       
     
        // Loop though each ad in the list...
        $visited_ads = [];
        foreach(self::$data['ads']['recordset'] as &$ad)
        {
            // add the ids to update the visits counter
            array_push($visited_ads, $ad['id']);
            
            // Parse the ads into humanly readable values...
            self::prepareListAdForGUI($ad);

            // Grab and accumulate the ad's category name...
            $category_names[] = $ad['category_name'];

            // Grab and accumulate the ad's city name...
            $city_names[] = $ad['city_name'];

            // Build and accumulate a string that contains the ad's category and city name separated by a space...
            $categories_and_cities[] = $ad['category_name'] . ' ' . $ad['city_name'];

            // Build and accumulate a string that contains the ad's category and city name with a conjunctiom in between them...
            $categories_and_cities_for_in_page_text[] = $ad['category_name'] . ' [LABEL-IN] ' . $ad['city_name'];
        }
        
        // update visits counter for all the ads displayed
        //\Modules\Humpchies\AdModel::updateNoVisits($visited_ads);

        /**
         * There might be repeated categories and cities in the ads. Therefore,
         * clean up the acumulators to make the values inside them unique...
         */
        $category_names = array_unique($category_names);
        $city_names = array_unique($city_names);
        $categories_and_cities = array_unique($categories_and_cities);
        $categories_and_cities_for_in_page_text = array_unique($categories_and_cities_for_in_page_text);


        // Comma separate the categories and cities combinations
        $categories_and_cities = implode(',', $categories_and_cities);

        /**
         * If after filtering there are 2 or more categories... add commas and
         * a conjunction for the last category name
         */
        if(count($category_names) > 1)
        {
            $temp = array_pop($category_names);                                             // Grab the last category name
            $category_names = implode(', ', $category_names) . ' [LABEL-AND] ' . $temp;     // Glue everything together, add commas and a conjunction for the last category name
        }
        else
            $category_names = $category_names[0];

        /**
         * If after filtering there are 2 or more cities... add commas and
         * a conjunction for the last city name
         */
        if(count($city_names) > 1)
        {
            $temp = array_pop($city_names);                                                 // Grab the last city name
            $city_names = implode(', ', $city_names) . ' [LABEL-AND] ' . $temp;             // Glue everything together, add commas and a conjunction for the last city name
        }
        else
            $city_names = $city_names[0];

        /**
         * If after filtering there are 2 or more categories and cities for
         * in page text... add commas and a conjunction for the last category
         * and city combination
         */
        if(count($categories_and_cities_for_in_page_text) > 1)
        {
            // Grab the last category and city combination...
            $temp = array_pop($categories_and_cities_for_in_page_text);

            // Glue everything together, add commas and a conjunction for the last category and city combination...
            $categories_and_cities_for_in_page_text = implode(', ', $categories_and_cities_for_in_page_text) . ' [LABEL-AND] ' . $temp;
        }
        else
            $categories_and_cities_for_in_page_text = $categories_and_cities_for_in_page_text[0];

        // Build the description...
        self::setDescription(   $base_title
            .
            ' [LABEL-IN] '
            .
            $category_names
            .
            ' [SUFFIX-LIST-DESCRIPTION] '
            .
            $city_names
            .
            ($page_number === 1 ? '' : " - [LABEL-PAGE] $page_number"));

        // Build the keywords...
        self::setKeywords($categories_and_cities);

        // Build the in page text...
        self::setInPageText(    '[IN-PAGE-TEXT-SEARCH-PART-1] '
            .
            self::getDescription()
            .
            '[IN-PAGE-TEXT-SEARCH-PART-2] '
            .
            $categories_and_cities_for_in_page_text
            .
            '[IN-PAGE-TEXT-SEARCH-PART-3]');
    }

    /**
     * This function displays the details of a particular ad.
     *
     * @returns NULL
     */
    public static function show() {

        if( \Core\Utils\Security::isIdInputValid($_GET['id']) !== \Core\Utils\Security::NO_ERROR            // Validate the id as being an integer >= 1
            ||
            //(self::$data['ad'] = \Modules\Humpchies\AdModel::getDetailsOfActiveAd($_GET['id'])) === FALSE)  // Make sure the ID actually returns an ad
             (self::$data['ad'] = \Modules\Humpchies\AdModel::getDetailsOfSimpleAd($_GET['id'])) === FALSE)  // Make sure the ID actually returns an ad
        {
            if(isset($_GET['category']) && isset($_GET['city'])){
                self::$noadCity     = $_GET['city'];
                self::$noadCategory = $_GET['category'];
            } else {
                self::$noadCity     = null;
                self::$noadCategory = null;
            }   
            
            if(isset($_GET['category']) && isset($_GET['city'])){
                $getCloseAds = self::getCloseAds(intval($_GET['id']), $_GET['category'], $_GET['city']);
            }   
            
            if($getCloseAds['next']){
                $ad = \Modules\Humpchies\AdModel::getDetails4Link($getCloseAds['next']);
                $ad = $ad[0];
                self::redirect(self::getDetailsURL($ad['category'], \Core\Utils\Text::stringToUrl($ad['city']), $ad['id'], $ad['title'])); 
                //$ad = \Modules\Humpchies\AdModel::getDetails4Link($getCloseAds['next']);
//                self::redirect(self::getDetailsURL($ad['category'], \Core\Utils\Text::stringToUrl($ad['city']), $ad['id'], $ad['title']));
            }
            
            \Core\Router::throw404();                                                                   // Failure of any of the above conditions triggers a 404
            return FALSE;                                                                               // we then advise the calling context of the failure here
        }
        
        /**
         * The code to retrieve the ad uses '\Core\Data::findBySQL'. The findBySQL method
         * makes a call to \Core\dbConn::select. The select method executes a generic query
         * which against the database. This call retrieves its results as an array of
         * associative arrays. This means that the results come back in the form of a 2
         * dimentional array. Since in this case we are retrieving only 1 item, we need to
         * 'shift' that item to access it as a 1 dimention relational array.
         */
        //self::$data['ad']["chat_id"] = self::$data['ad']["id"];
        //self::$data['ad']["chat_title"] = self::$data['ad']["title"];
        self::$data['ad'] = array_shift(self::$data['ad']);
        self::$data['ad']["orig_title"] =  self::$data['ad']["title"];
        self::$data['videolocation'] = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::videos')."temp/". self::$data['ad']['user_id']."/";
        self::$data['attached_video'] = self::$data['ad']["attached_video"];
        self::$data['go_back'] = ($_SESSION['goback'])?$_SESSION['goback']:"/";
        
        self::$data["user_votes"] = \Modules\Humpchies\CommentsVoteModel::getVotesComenntsIds(\Core\Utils\Session::getCurrentUserID());
        
        $getCloseAds = self::getCloseAds(intval($_GET['id']), $_GET['category'], $_GET['city']);

        if($getCloseAds['prev']){
            $ad = \Modules\Humpchies\AdModel::getDetails4Link($getCloseAds['prev']);
            $ad = $ad[0];
            $getCloseAds['backurl'] =self::getDetailsURL($ad['category'], \Core\Utils\Text::stringToUrl($ad['city']), $ad['id'], $ad['title']);
           
        }
        if($getCloseAds['next']){
            $ad = \Modules\Humpchies\AdModel::getDetails4Link($getCloseAds['next']);
            $ad = $ad[0];
            $getCloseAds['nexturl'] =self::getDetailsURL($ad['category'], \Core\Utils\Text::stringToUrl($ad['city']), $ad['id'], $ad['title']);
        }
        
        if(\Core\Utils\Session::getAuthenticatedSessionValues() !== FALSE && \Core\Utils\Session::currentUserIsMember()) {  
            self::$data['user_favorites'] = \Modules\Humpchies\UserFavoriteModel::getUserFavoritesList(\Core\Utils\Session::getCurrentUserID());
        }
        
        self::$data['ad']['buttons'] = $getCloseAds;
        /* TO BE FIXED */
        // get ad tags
        $lang =  (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';
        self::$data["services"] = \Modules\Humpchies\AdTagModel::getAdTags($_GET['id'], $lang);
        
        if(! is_null(self::$data['ad']['p_id']) && in_array(self::$data['ad']['p_id'], array(19, 20, 21, 22, 23, 24, 25, 26)) ) {
            self::$data['ad']['is_premium'] = ( \Modules\Humpchies\AdModel::AD_PACKAGE_ACTIVE_STATUS == self::$data['ad']['package_status'] );
        }

        $filter = array(
            'a.id != '.self::$data['ad']['id']
        );
        
       
        if(self::$data['ad']['is_premium'] ||  in_array(self::$data['ad']['p_id'], self::getVIPZoneAdIds()) || (self::$data['ad']['p_id'] && self::$data['ad']['p_id'] < 19) ){
          
          self::$data['ads'] = \Modules\Humpchies\AdModel::getListOfAdsByUser(self::$data['ad']['user_id'],1,4,"'active'", $filter);
          self::$data['ads'] = array_shift(self::$data['ads']);  
        } else {
            self::$data['ads'] =  self::getMoreAds( self::$data['ad']['city']);
        }
        
         if( count(self::$data['ads'])== 0){
             self::$data['ads'] =  \Modules\Humpchies\AdModel::get4RandomAdsByCity( self::$data['ad']['city']);
         }
            
        self::$data['advertiser'] = \Modules\Humpchies\UserModel::getDetailsById(self::$data['ad']['user_id']);
        
        if(is_array(self::$data['ads'])) { 
            foreach(self::$data['ads'] as &$ad) {
                // Parse the ads into humanly readable values...
                self::prepareListAdForGUI($ad);
            }
        }
        
        $page_number = ($_GET['page'] === NULL ? 1 : (int)$_GET['page']);
        $list_size = 5;
        
        $Comments =\Modules\Humpchies\CommentsModel::getActiveCommentsByUserId(self::$data['ad']['user_id'], $page_number, $list_size);
        
        self::$data['comments'] = $Comments['recordset'];
        
          // Get the basic url of the current page
        $page_url = self::getDetailsURL(self::$data['ad']['category'], \Core\Utils\Text::stringToUrl(self::$data['ad']['city']), self::$data['ad']['id'], self::$data['ad']['title']);
       
        // Build and push a page paginator plugin...
        self::pushPlugin('paginator', new \Plugins\Paginator( $Comments['count'], $page_number, $list_size, $page_url, FALSE, $page_url));
        
//        self::$data['comments']=\Modules\Humpchies\CommentsModel::getActiveCommentsByUserId(self::$data['ad']['user_id']);
        //self::$data['comments'] = !empty(self::$data['comments']) ?  array_shift(self::$data['comments']) : array();
        if(is_array(self::$data['comments'])){
            foreach(self::$data['comments'] as $key =>  &$comment) {
                // Prepare url for ads
                $comment["ad_url"] = self::getDetailsURL($comment['ad_category'], \Core\Utils\Text::stringToUrl($comment['ad_city']), $comment['ad_id'], $comment['ad_title']);
            }
        }   

        $from_user = \Core\Utils\Session::getCurrentUserID();
        self::$data['user_has_pending_comment'] = \Modules\Humpchies\CommentsModel::hasPendingComment(self::$data['ad']['user_id'],$from_user);
        self::$data['redirect'] = empty(\Core\Utils\Session::getCurrentUserID());
        self::$data['is_member'] = \Core\Utils\Session::currentUserIsMember();

        // if the user is the advertiser who created the add, he/she can add a comment to it as well
        if($from_user == self::$data['ad']['user_id']) {
            self::$data['is_member'] = true;
        }
        
        /**
         * Make sure we are running the ad from a single URL... To do so, we grab the current
         * request URI, then build the official URL of the ad... compare them both and voila!
         * If the don't match redirect the request to the proper URL, and advise the calling
         * context about the failure - to prevent down stream code from executing. Even though
         * the redirection would call exit().
         */
        $current_req = str_replace("?".\Core\Utils\Device::getServerVarsAndHeaders('query_string'), '', \Core\Utils\Device::getServerVarsAndHeaders("request_uri") );
        if($current_req !== ($expected_ad_uri = self::getDetailsURL(self::$data['ad']['category'], \Core\Utils\Text::stringToUrl(self::$data['ad']['city']), self::$data['ad']['id'], self::$data['ad']['title'])))
        {
            self::redirect($expected_ad_uri);
            return FALSE;
        }

        
        // Parse the ad fields into humanly readable information...
        self::prepareAdForGUI(self::$data['ad']);
        
        // Whether or not this ad is a VIP add
        self::$data['ad']['vip'] = in_array(self::$data['ad']['id'], self::getVIPZoneAdIds());
        if(self::$data['ad']['p_id'] && self::$data['ad']['p_id'] < 19){
              self::$data['ad']['vip'] = true;
        }
        // Add chat related data
        $fromUserId = \Core\Utils\Session::getCurrentUserID();
        $toUserId = self::$data['ad']['user_id'];
        $chatId = \Modules\Humpchies\MessageModel::getChatId(self::$data['ad']['id'], $fromUserId, $toUserId);

        self::$data['chat'] = array(
            'isLoggedIn' => $fromUserId > 0,
            'canSendMessage' => ($fromUserId > 0)
                && \Modules\Humpchies\MessageModel::canSendMessage($fromUserId, $toUserId),
            'haveOpenedChat' => (bool)\Modules\Humpchies\MessageModel::getOneMessage($chatId),
            'chatId' => $chatId,
        );
        

        // SEO VALUES
        // ==========

        // Title
        self::setTitle(self::$data['ad']['title']);

        // Description
        self::setDescription(self::$data['ad']['seo_head_description']);

        // Keywords
        self::setKeywords('[LABEL-' . strtoupper(self::$data['ad']['category']) . '-SINGULAR]' .' ' . self::$data['ad']['city_name']);

        // In page text
        self::setInPageText(    '[IN-PAGE-TEXT-AD-DETAILS-PART-1] '
            .
            self::getTitle()
            .
            '[IN-PAGE-TEXT-AD-DETAILS-PART-2] '
            .
            '[LABEL-' . strtoupper(self::$data['ad']['category']) . '] '
            .
            '[IN-PAGE-TEXT-AD-DETAILS-PART-3] '
            .
            self::$data['ad']['city_name']
            .
            ' [IN-PAGE-TEXT-AD-DETAILS-PART-4]');

        // set breadcrumb class
        self::setHeaderClass("has_breadcrumb");
        
        // SLIMBOX SUPPORT
        // ===============
        \Core\Controller::setExtraHead('<meta property="og:image" content="' . self::$data['ad']['images']['main']['src'] . '">
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'fancybox/jquery.fancybox.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'pc/comments.js?v=1.06"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'modal.js?v=1.15"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'favorites.js?v=1.01"></script>
        
                                        <link rel="stylesheet" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'fancybox/jquery.fancybox.min.css" type="text/css" media="screen" />
                                        <style>.fancybox-navigation { position: static !important; }</style>');
        /*<script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'f/slimbox2.js"></script>
        <link rel="stylesheet" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . 'slimbox/default.' . \Core\Utils\Session::getLocale() . '.1.0.0.css" type="text/css" media="screen" />');*/
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="'
            .
            \Core\Utils\Device::getProtocol() .
            \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css')
            .
            (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch'))
            .
            '/details_view.css?v=3.3">');
            \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="'
            .
            \Core\Utils\Device::getProtocol() .
            \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css')
            .
            '/modal.css?v=3.13">');
            
        //MAPBOX
             \Core\Controller::setExtraHead('<script src="https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.js"></script>');    
            \Core\Controller::setExtraHead('<link href="https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.css" rel="stylesheet" />');    
            \Core\Controller::setExtraHead('<script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'show_mapbox.js?v=1.01"></script>'); 
        
        // comments votes
        \Core\Controller::setExtraHead('
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/comments_vote.js?v=1.07"></script>
                                        '); 

        // get map information
        $map_coords = \Modules\Humpchies\AdLocationModel::getLocationInfo($_GET['id']);
        // check if photo was added, if not save photo
        if($map_coords && $map_coords['hide_map'] == 0) {
            
            $lat = floatval($map_coords['lat']);
            $long = floatval($map_coords['long']);

            if($map_coords['location_photo'] == null) {

                // fetch photos
                if(\Core\Utils\Session::getCurrentUserID() == 63661) {
                    $arrPhotos = \Modules\Humpchies\AdLocationModel::saveLocationImgV2($map_coords);
                } else {
                    $arrPhotos = \Modules\Humpchies\AdLocationModel::saveLocationImg($map_coords);    
                }
                if($arrPhotos) {
                    $map_coords['location_photo']     = $arrPhotos['location_photo'];
                    $map_coords['location_photo_sat'] = $arrPhotos['location_photo_sat'];
                }
            }
        }
        self::$data['map_coords'] = $map_coords;
    }

    
    public static function show_v2() {
        
        self::$data['ad'] = \Modules\Humpchies\AdModel::getDetailsOfSimpleAd($_GET['id']);
        
        if( \Core\Utils\Security::isIdInputValid($_GET['id']) !== \Core\Utils\Security::NO_ERROR || self::$data['ad'] === FALSE) { 
        
            if(isset($_GET['category']) && isset($_GET['city'])){
                self::$noadCity     = $_GET['city'];
                self::$noadCategory = $_GET['category'];
            } else {
                self::$noadCity     = null;
                self::$noadCategory = null;
            }      
            
            if(isset($_GET['category']) && isset($_GET['city'])){
                $getCloseAds = self::getCloseAds(intval($_GET['id']), $_GET['category'], $_GET['city']);
            }   
            
            if($getCloseAds['next']){
                $ad = \Modules\Humpchies\AdModel::getDetails4Link($getCloseAds['next']);
                $ad = $ad[0];
                self::redirect(self::getDetailsURL($ad['category'], \Core\Utils\Text::stringToUrl($ad['city']), $ad['id'], $ad['title'])); 
                //$ad = \Modules\Humpchies\AdModel::getDetails4Link($getCloseAds['next']);
//                self::redirect(self::getDetailsURL($ad['category'], \Core\Utils\Text::stringToUrl($ad['city']), $ad['id'], $ad['title']));
            }
            
            \Core\Router::throw404();                                                                   // Failure of any of the above conditions triggers a 404
            return FALSE;                                                                               // we then advise the calling context of the failure here
        }
        
        self::$data["new_header"] = true; // TO BE REMOVED UPON RELEASE
        
        // add details to the ad to be used 
        self::$data['ad']["chat_id"] = self::$data['ad']["id"];
        self::$data['ad']["chat_title"] = self::$data['ad']["title"];
        self::$data['ad'] = array_shift(self::$data['ad']);
        self::$data['ad']["orig_title"] =  self::$data['ad']["title"];
        self::$data['videolocation'] = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::videos')."temp/". self::$data['ad']['user_id']."/";
        self::$data['attached_video'] = self::$data['ad']["attached_video"];
        self::$data['go_back'] = ($_SESSION['goback'])?$_SESSION['goback']:"/";
        
        $getCloseAds = self::getCloseAds(intval($_GET['id']), $_GET['category'], $_GET['city']);

        if($getCloseAds['prev']){
            $ad = \Modules\Humpchies\AdModel::getDetails4Link($getCloseAds['prev']);
            $ad = $ad[0];
            $getCloseAds['backurl'] =self::getDetailsURLV2($ad['category'], \Core\Utils\Text::stringToUrl($ad['city']), $ad['id'], $ad['title']);
        }
        
        if($getCloseAds['next']){
            $ad = \Modules\Humpchies\AdModel::getDetails4Link($getCloseAds['next']);
            $ad = $ad[0];
            $getCloseAds['nexturl'] =self::getDetailsURLV2($ad['category'], \Core\Utils\Text::stringToUrl($ad['city']), $ad['id'], $ad['title']);
        }
        
        if(\Core\Utils\Session::getAuthenticatedSessionValues() !== FALSE && \Core\Utils\Session::currentUserIsMember()) {  
            self::$data['user_favorites'] = \Modules\Humpchies\UserFavoriteModel::getUserFavoritesList(\Core\Utils\Session::getCurrentUserID());
        }
        self::$data['ad']['buttons'] = $getCloseAds;
        
       
        
        // get ad tags
        $lang =  (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';
        self::$data["services"] = \Modules\Humpchies\AdTagModel::getAdTags($_GET['id'], $lang);
        
        
        // GET MORE ADS DEPENING ON AD STATUS
        if(! is_null(self::$data['ad']['p_id']) && in_array(self::$data['ad']['p_id'], array(19, 20, 21, 22)) ) {
            self::$data['ad']['is_premium'] = ( \Modules\Humpchies\AdModel::AD_PACKAGE_ACTIVE_STATUS == self::$data['ad']['package_status'] );
        }

        $filter = array(
            'a.id != '.self::$data['ad']['id']
        );
        
       
        if(self::$data['ad']['is_premium'] ||  in_array(self::$data['ad']['p_id'], self::getVIPZoneAdIds()) || (self::$data['ad']['p_id'] && self::$data['ad']['p_id'] < 19) ){
          
            self::$data['ads'] = \Modules\Humpchies\AdModel::getListOfAdsByUser(self::$data['ad']['user_id'],1,4,"'active'", $filter);
            self::$data['ads'] = array_shift(self::$data['ads']);  
        } else {
            self::$data['ads'] =  self::getMoreAds( self::$data['ad']['city']);
        }
        
        if( count(self::$data['ads'])== 0){
             self::$data['ads'] =  \Modules\Humpchies\AdModel::get4RandomAdsByCity( self::$data['ad']['city']);
        }
            
        self::$data['advertiser'] = \Modules\Humpchies\UserModel::getDetailsById(self::$data['ad']['user_id']);
         
        if(is_array(self::$data['ads'])) { 
            foreach(self::$data['ads'] as &$ad) {
                // Parse the ads into humanly readable values...
                self::prepareListAdForGUI($ad);
            }
        }
        
        // get comments
        $page_number = ($_GET['page'] === NULL ? 1 : (int)$_GET['page']);
        $list_size = 5;
        $Comments =\Modules\Humpchies\CommentsModel::getActiveCommentsByUserId(self::$data['ad']['user_id'], $page_number, $list_size);
        self::$data['comments'] = $Comments['recordset'];
        
        // Get the basic url of the current page
        $page_url = self::getDetailsURL(self::$data['ad']['category'], \Core\Utils\Text::stringToUrl(self::$data['ad']['city']), self::$data['ad']['id'], self::$data['ad']['title']);

        self::pushPlugin('paginator', new \Plugins\Paginator( $Comments['count'], $page_number, $list_size, $page_url, FALSE, $page_url));
        
        if(is_array(self::$data['comments'])){
            foreach(self::$data['comments'] as $key =>  &$comment) {
                // Prepare url for ads
                $comment["ad_url"] = self::getDetailsURL($comment['ad_category'], \Core\Utils\Text::stringToUrl($comment['ad_city']), $comment['ad_id'], $comment['ad_title']);
            }
        }   

        $from_user = \Core\Utils\Session::getCurrentUserID();
        self::$data['user_has_pending_comment'] = \Modules\Humpchies\CommentsModel::hasPendingComment(self::$data['ad']['user_id'],$from_user);
        self::$data['redirect'] = empty(\Core\Utils\Session::getCurrentUserID());
        self::$data['is_member'] = \Core\Utils\Session::currentUserIsMember();
        
        // if the user is the advertiser who created the add, he/she can add a comment to it as well
        if($from_user == self::$data['ad']['user_id']) {
            self::$data['is_member'] = true;
        }
        
        /**
         * Make sure we are running the ad from a single URL... To do so, we grab the current
         * request URI, then build the official URL of the ad... compare them both and voila!
         * If the don't match redirect the request to the proper URL, and advise the calling
         * context about the failure - to prevent down stream code from executing. Even though
         * the redirection would call exit(). - IMPORTANT!!!!!!!
         */
        //$current_req = str_replace("?".\Core\Utils\Device::getServerVarsAndHeaders('query_string'), '', \Core\Utils\Device::getServerVarsAndHeaders("request_uri") );
//        if($current_req !== ($expected_ad_uri = self::getDetailsURL(self::$data['ad']['category'], \Core\Utils\Text::stringToUrl(self::$data['ad']['city']), self::$data['ad']['id'], self::$data['ad']['title'])))
//        {
//            self::redirect($expected_ad_uri);
//            return FALSE;
//        }

        
        // Parse the ad fields into humanly readable information...
        self::prepareAdForGUI(self::$data['ad']);
        
        // Whether or not this ad is a VIP add
        self::$data['ad']['vip'] = in_array(self::$data['ad']['id'], self::getVIPZoneAdIds());
        if(self::$data['ad']['p_id'] && self::$data['ad']['p_id'] < 19){
              self::$data['ad']['vip'] = true;
        }
        // Add chat related data
        $fromUserId = \Core\Utils\Session::getCurrentUserID();
        $toUserId = self::$data['ad']['user_id'];
        $chatId = \Modules\Humpchies\MessageModel::getChatId(self::$data['ad']['id'], $fromUserId, $toUserId);

        self::$data['chat'] = array(
            'isLoggedIn' => $fromUserId > 0,
            'canSendMessage' => ($fromUserId > 0)
                && \Modules\Humpchies\MessageModel::canSendMessage($fromUserId, $toUserId),
            'haveOpenedChat' => (bool)\Modules\Humpchies\MessageModel::getOneMessage($chatId),
            'chatId' => $chatId,
        );
        

        // SEO VALUES
        // ==========

        // Title
        self::setTitle(self::$data['ad']['title']);

        // Description
        self::setDescription(self::$data['ad']['seo_head_description']);

        // Keywords
        self::setKeywords('[LABEL-' . strtoupper(self::$data['ad']['category']) . '-SINGULAR]' .' ' . self::$data['ad']['city_name']);

        // In page text
        self::setInPageText(    '[IN-PAGE-TEXT-AD-DETAILS-PART-1] '
            .
            self::getTitle()
            .
            '[IN-PAGE-TEXT-AD-DETAILS-PART-2] '
            .
            '[LABEL-' . strtoupper(self::$data['ad']['category']) . '] '
            .
            '[IN-PAGE-TEXT-AD-DETAILS-PART-3] '
            .
            self::$data['ad']['city_name']
            .
            ' [IN-PAGE-TEXT-AD-DETAILS-PART-4]');

        // set breadcrumb class
        self::setHeaderClass("has_breadcrumb");
        
        // SLIMBOX SUPPORT
        // ===============
        \Core\Controller::setExtraHead('<meta property="og:image" content="' . self::$data['ad']['images']['main']['src'] . '">
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'fancybox/jquery.fancybox.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'pc/comments_v2.js?v=1.07"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'modal_v2.js?v=1.14"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'favorites.js?v=1.01"></script>
        
                                        <link rel="stylesheet" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'fancybox/jquery.fancybox.min.css" type="text/css" media="screen" />
                                        <style>.fancybox-navigation { position: static !important; }</style>');
        /*<script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'f/slimbox2.js"></script>
        <link rel="stylesheet" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . 'slimbox/default.' . \Core\Utils\Session::getLocale() . '.1.0.0.css" type="text/css" media="screen" />');*/
        
           
            \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . 'new_modal.css?v=1.12">');
        //MAPBOX
        
        if(\Core\Utils\Session::getCurrentUserID() == 63661){
             \Core\Controller::setExtraHead('<script src="https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.js"></script>');    
            \Core\Controller::setExtraHead('<link href="https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.css" rel="stylesheet" />');    
            \Core\Controller::setExtraHead('<script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'show_mapbox.js?v=1.01"></script>'); 
        }    

        // get map information
        $map_coords = \Modules\Humpchies\AdLocationModel::getLocationInfo($_GET['id']);
        // check if photo was added, if not save photo
        if($map_coords && $map_coords['hide_map'] == 0) {
            
            $lat = floatval($map_coords['lat']);
            $long = floatval($map_coords['long']);

            if($map_coords['location_photo'] == null) {

                // fetch photos
                if(\Core\Utils\Session::getCurrentUserID() == 63661) {
                    $arrPhotos = \Modules\Humpchies\AdLocationModel::saveLocationImgV2($map_coords);
                } else {
                    $arrPhotos = \Modules\Humpchies\AdLocationModel::saveLocationImg($map_coords);    
                }
                if($arrPhotos) {
                    $map_coords['location_photo']     = $arrPhotos['location_photo'];
                    $map_coords['location_photo_sat'] = $arrPhotos['location_photo_sat'];
                }
            }
        }
        self::$data['map_coords'] = $map_coords;
    }
    
    /**
     * This function is used to build the site's RSS
     *
     * @returns NULL
     */
    public static function rss()
    {
        // Grab the latest list of ads...
        self::$data['ads'] = \Modules\Humpchies\AdModel::getListOfLatestAds(\Modules\Humpchies\AdModel::LIST_SIZE_DEFAULT_PC, array('id','title','description','date_released', 'category', 'city'));

        // Throw a 404 if we are unable to retrieve any ads... don't forget to advise the calling context...
        if(self::$data['ads'] === FALSE)
        {
            \Core\Router::throw404();
            return FALSE;
        }

        // Set some values needed to build the RSS
        // =======================================
        self::$data['title']            = '[TITLE-RSS]';                                                            // The RSS title
        self::$data['description']      = '[DESCRIPTION-RSS]';                                                      // The RSS description
        self::$data['language']         = \Core\Utils\Session::getLocale();                                         // The RSS language
        self::$data['pub_date']         = @date(DATE_RSS, \Core\Utils\Time::getCommonTimeStamp());                  // The last publication date
        self::$data['last_build_date']  = self::$data['pub_date'];                                                  // The last build date. Same as the last publish date
        self::$data['link']             =   \Core\Utils\Device::getProtocol()                                       // The absolute root of the site
            .
            \Core\Utils\Config::getConfigValue('\Application\Settings::hostname');

        // Loop through each ad and prepare it for the RSS...
        foreach(self::$data['ads'] as &$ad)
            self::prepareListAdForXML($ad);

        // Set the MIME type and character encoding
        header("Content-Type: application/rss+xml;charset=ISO-8859-1");
    }

    /**
     * This the 404 page of the site
     *
     * @returns NULL
     */
    public static function notFound() {
        // It is the responsability of the 404 implementation to set the 404 header....
        header("HTTP/1.0 404 Not Found");

        // Set the page title...
        self::setTitle('[TITLE-404]');

        // Set the page description
        self::setDescription('[DESCRIPTION-404]');
       
        //var_dump($_GET['category']);

        // Fetch the first page of ads...
        self::$data['ads'] = \Modules\Humpchies\AdModel::getListOfActiveAdsByCategoryAndCityV3(self::$noadCategory, null,self::$noadCity, null, 1, 25);

         
        // If no ads are found simply bail out...
        if(self::$data['ads'] === FALSE)
            return;

        // Create and push a list of cities with ads...
        self::pushPlugIn('city_list', new \Plugins\HumpchiesCityList());


        // Loop though each of the ads, and parse them into humanly readable values
        foreach(self::$data['ads']['recordset'] as &$ad)
            self::prepareListAdForGUI($ad);
    }

    public static function favorites() {                                    
    
        // CHECK IF USER IS MEMBER
        if(!\Core\Utils\Session::currentUserIsMember()) {
            self::redirect('/');
        }
        
        $browse_validity = self::validateBrowse($_GET);

        // Is the browse request valid?
        switch($browse_validity) {
            // Yes it is! Simply move on to retrieve the list of ads
            case self::NO_ERROR:
                break;

            // No is NOT! Throw a 404, and advice calling context of failure
            default:
                \Core\Router::throw404();
                return FALSE;
        }

        // Get the current page number... if null then this is page number 1
        $page_number = ($_GET['page'] === NULL ? 1 : (int)$_GET['page']);
        
        // Setup VIPs and Premims
        self::setupVIPAds();
        self::setupPremiumAds();
        
        $list_size = 25;
        $GroupedAds = \Modules\Humpchies\AdModel::getListOfAdsFavorites(\Core\Utils\Session::getCurrentUserID(), $page_number, $list_size);

        self::$data['ads'] = $GroupedAds['ads'];
        self::$data['vip_ads'] = $GroupedAds['vip_ads'];
        self::$data['premium_ads'] = $GroupedAds['premium_ads'];

        // If no ads are found, throw a 404, and advice calling context of failure
        if(!self::$data['ads'] && !self::$data['vip_ads'] && !self::$data['premium_ads']) {
            \Core\Router::throw404();
            return FALSE;
        }
      
        if(is_array(self::$data['vip_ads'])) {
            foreach(self::$data['vip_ads'] as &$ad) {
                self::prepareListAdForGUI($ad);
            }   
        }
        
        if(is_array(self::$data['premium_ads'])) {
            foreach(self::$data['premium_ads'] as &$ad) {
                self::prepareListAdForGUI($ad);
            }
        }

        // Get the basic url of the current list
        $list_url = '/favorites/listing';
        // Build and push a page paginator plugin...
        self::pushPlugin('paginator', new \Plugins\Paginator( self::$data['ads']['count'], $page_number, $list_size, $list_url, FALSE, $list_url));
        
        \Core\Controller::setExtraHead('
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'favorites.js?v=1.01"></script>
                                        ');

        foreach(self::$data['ads']['recordset'] as &$ad) {
            self::prepareListAdForGUI($ad);
        } 
        
        self::setHeaderClass("has_breadcrumb"); 
    }
    
    
    /**
     * put your comment there...
     *
     * @returns NULL
     */
    public static function browseMyListings_ma($type = "'active', 'new', 'paused'")
    {

        if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect( '/');
        }
        // Disable browser cache...
        self::disableCache();
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/self_checkout_v3.css?v=55">'
            . '<script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (/*\Core\Utils\Device::isPC() ? 'pc' : */'mobile') . '/ads.js?v=15.3"></script>
            <link rel="stylesheet" href="' . \Modules\Humpchies\Utils::getCDNPath('css') .'tingle.css">
            <script src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') .'tingle.js"></script>
             <script src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') .'pc/mylist.js?v=1.04"></script>
        ');
        
        if(!\Core\Utils\Device::isPC()) {
             \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') . '/mobile/touch/my_ads.css?v=7">'
            );    
        }
        
        $debug= isset($_GET['debugLiviu'])? $_GET['debugLiviu'] : '';
        if($debug == "server"){
            $loc = \Core\Utils\Session::getGeoLocation();
            var_dump(\Core\Utils\Session::getGeoLocation());
            echo $loc->city->names->en."<br>"; 
            echo $loc->country->iso_code."<br>"; 
            echo $loc->country->names->en."<br>"; 
            echo $loc->subdivisions[0]->names->en."<br>"; 
            die();
        }

        // Validate the current request to my ads
        $browse_my_ads_validity = self::validateBrowseMyListings($_GET);

        // Handle errors accordingly
        switch($browse_my_ads_validity)
        {
            // We are golden... no errors... retrieve the user's ads
            case self::NO_ERROR:
                break;

            // Page number is invalid... throw 404
            case self::ERROR_PAGE_NUMBER_IS_INVALID:
                \Core\Router::throw404();
                break;
                return FALSE;

            // |Somehting else went wrong... advise user and redirect to the root of the site
            default:
                self::addMessage("[ERROR-UNHANDLED]: $browse_my_ads_validity", 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                return FALSE;
        }

        // Set some page values...
        self::setTitle('[LABEL-MY-ADS]');                   // Title
        self::setDescription('[LABEL-MY-ADS]');             // Description
        self::setKeywords('[LABEL-MY-ADS]');                // Keywords
        self::setHeaderClass("has_breadcrumb");
        
        // Get the current page number... if null then this is page number 1
        $page_number = ($_GET['page'] === NULL ? 1 : (int)$_GET['page']);

        // Get the number of items to diplay per page...
        $list_size = (\Core\Utils\Device::isPC() ? \Modules\Humpchies\AdModel::LIST_SIZE_DEFAULT_PC : \Modules\Humpchies\AdModel::LIST_SIZE_DEFAULT_MOBILE);
        
        $list_filter = array('myads' => 'yes');
        // Retrive the ads... if no ads are found for the current user... advise him/her and redirect him/her to the ad creation page
        if((self::$data['ads'] = \Modules\Humpchies\AdModel::getListOfAdsByUser(\Core\Utils\Session::getCurrentUserID(),
                $page_number,
                $list_size, $type, $list_filter)) === FALSE)
        {
            
            self::addMessage('[ERROR-MY-LISTINGS-IS-EMPTY]', 'warning');
            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'create'));
            return FALSE;
        }
        if($debug == 'hiddenddump'){
            var_dump(self::$data['ads']['recordset']);
            die();
        }
       
        self::$data['ticketActions'] = \Modules\Humpchies\SupportModel::hasTicketActions(\Core\Utils\Session::getCurrentUserID());

        $active_ads_count = 0;
        $exp_ads_count = 0;

        foreach (self::$data['ads']['recordset'] as $loop_ad) {
            switch ($loop_ad['status']) {
                case 'new':
                case 'active':
                    $active_ads_count++;
                    break;

                case 'inactive':
                case 'delete':
                case 'paused':
                case 'banned':
                    $exp_ads_count++;
                    break;
            }
        }
        self::$data['active_ads_count'] =  $active_ads_count;
        self::$data['exp_ads_count'] = $exp_ads_count;
        // Create and push a paginator
        self::pushPlugin('paginator',
            new \Plugins\Paginator( self::$data['ads']['count'],
                $page_number,
                $list_size,
                \Core\Utils\Navigation::urlFor('ad', 'my-listings'),
                TRUE));

        // Loop theough each ad and parse it into GUI usable values...
        foreach(self::$data['ads']['recordset'] as &$ad)
            self::prepareListAdForUser($ad);
    }

    public static function paPopup() {
        self::disableCache();
    }

    public static function browseMyActiveListings_ma()
    {
        AdController::browseMyListings_ma("'active', 'new'");
    }

    public static function browseMyExpiredListings_ma()
    {
        AdController::browseMyListings_ma("'paused'");
    }
    
    public static function phone_validation(){

         if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect( '/');
        }
        $user_id = \Core\Utils\Session::getCurrentUserID();

        if( !\Modules\Humpchies\UserModel::isUserActiveById( $user_id ) ){
            \Core\Utils\Session::abandon();
            self::addMessage('[SUCCESS-LOGOUT-SUCCESFUL]');
            self::redirect(\Core\Utils\Navigation::urlFor('/'));
        }
        // Disable browser cache
        self::disableCache();

        if  (\Modules\Humpchies\UserModel::isBehindBannedIp(\Core\Utils\Device::getDeviceIp())) {
            //self::handlePreviouslyBanned(\Core\Utils\Session::getCurrentUserID());
        }

        // Set some page values...
        self::setTitle('[LABEL-POST-AD]');                      // Title
        self::setDescription('[LABEL-POST-AD]');                // Description
        self::setHeaderClass("has_breadcrumb");
        // Enable custom CSS support
        // =========================
        \Core\Controller::setExtraHead('
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'pc/temp_ad.js?v=3.3"></script>'
        );
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="'
            .
            \Modules\Humpchies\Utils::getCDNPath('css')
            .
            (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch'))
            .
            '/post.css?v=5">
                                        <link media="all" rel="stylesheet" type="text/css" href="');


        // flag the max lengths of the fields that support this attribute...
        
        $phoneInfo = \Modules\Humpchies\AdModel::getAdPhoneValidation(intval($_GET['ad']), $user_id);
        if($phoneInfo['status'] != 'new'){
            $phoneInfo2 = \Modules\Humpchies\AdModel::getAdPhoneValidationFromRevision(intval($_GET['ad']), $user_id);
            $phoneInfo['revision'] = "no";
            if(isset($phoneInfo2['phone']) && $phoneInfo['phone'] != $phoneInfo2['phone']){
                $phoneInfo = array_merge($phoneInfo, $phoneInfo2);
                $phoneInfo['revision'] = "yes"; 
            }
            
        } 
        self::$data['ad'] = $phoneInfo;
        
    }
    
    public static function createMaV2() {
 
        \Core\Router::getParsedURL()->layout_settings['footer_mode']=\Core\View::HIDDEN_MODE;
        \Core\Router::getParsedURL()->layout_settings['action_mode']=\Core\View::DEVICE_MODE;

        if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect( '/');
        }
        $user_id = \Core\Utils\Session::getCurrentUserID();

        if( !\Modules\Humpchies\UserModel::isUserActiveById( $user_id ) ){
            \Core\Utils\Session::abandon();
            self::addMessage('[SUCCESS-LOGOUT-SUCCESFUL]');
            self::redirect(\Core\Utils\Navigation::urlFor('/'));
        }
        // Disable browser cache
        self::disableCache();
        $total_ads = \Modules\Humpchies\AdModel::getTotalAdsFromUserforRedirect($user_id);
        if (\Core\Cache::get(\Core\Utils\Device::getDeviceIp()) !== FALSE && intval($total_ads[0]['cate'])) {
             
                self::addMessage('[ERROR-AD-POST-WAIT-TIME] ' . ($minutes = (self::getAdPostTimeSpan() / 60)) . ' [LABEL-MINUTE]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
                return;
           
        } elseif (\Modules\Humpchies\UserModel::isBehindBannedIp(\Core\Utils\Device::getDeviceIp())) {
            //self::handlePreviouslyBanned(\Core\Utils\Session::getCurrentUserID());
        }

        // Set some page values...
        self::setTitle('[LABEL-POST-AD-V2]');                      // Title
        self::setDescription('[LABEL-POST-AD]');                // Description
        self::setHeaderClass("has_breadcrumb");
        
        // Enable custom CSS support
        \Core\Controller::setExtraHead('
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery-validation-1.19.0/dist/jquery.validate.min.js"></script>
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile/touch')).'/create_ad_new.js?v=1.06"></script>
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'pc/lant_uploader.js?v=0.09"></script>
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'intl-tel-input-new/build/js/intlTelInput.min.js" charset="utf-8"></script>
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'intl-tel-input-new/build/js/index.js?v=3.3" charset="utf-8"></script>
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'pc/temp_ad.js?v=3.3"></script>
            '
        );
       /* \Core\Controller::setExtraHead('
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'setup_map.js?v=0.5"></script>'
        );*/
        // MAPBOX
        \Core\Controller::setExtraHead('<script src="https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.js"></script>');    
        \Core\Controller::setExtraHead('<link href="https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.css" rel="stylesheet" />');    
        \Core\Controller::setExtraHead('<script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'setup_mapbox.js?v=1.01"></script>'); 
        
            
        if(\Core\Utils\Session::getLocale() == 'fr-ca'){
            \Core\Controller::setExtraHead('<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/localization/messages_fr.js"></script>');
        } 
        
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile/touch').'/create_ad_new.css?v=1.04">');
        
        \Core\Controller::setExtraHead('
            <link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/post.css?v=5">
            <link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('js').'intl-tel-input-new/build/css/intlTelInput.min.css">'
        );
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . '/modal.css?v=3.16">');
        
        // flag the max lengths of the fields that support this attribute...
        self::$data['max_len_title']                = \Modules\Humpchies\AdModel::MAX_LEN_TITLE;
        self::$data['max_len_price']                = strlen((string)\Modules\Humpchies\AdModel::MAX_PRICE);
        self::$data['max_len_price_conditions']     = \Modules\Humpchies\AdModel::MAX_LEN_PRICE_CONDITIONS;
        self::$data['max_len_phone']                = self::MAX_LEN_PHONE_ON_FORM;
        self::$data['videolocation']                = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::videos')."temp/";
        self::$data['imageslocation']                = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_images');

        self::$data['available_packages'] = \Modules\Humpchies\PaymentModel::get_available_packages();
        self::$data['user_videos'] = \Modules\Humpchies\AdModel::getUserVideo(\Core\Utils\Session::getCurrentUserID());

        // get tags
        //if(\Core\Utils\Session::getCurrentUserID() == 63661){ // only testme
            $lang =  (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';
            self::$data["tags"] = \Modules\Humpchies\TagModel::getTagsList($lang);
            //var_dump(self::$data["tags"]); 
        //}
        
        // Bail out if the form hasn't been posted
        // =======================================
        if(!$_POST)
        {
            //self::pushPlugIn('city_options', new \Plugins\HumpchiesCityOptions());
            self::pushPlugIn('category_options', new \Plugins\HumpchiesCategoryOptions());
            self::pushPlugIn('location_options', new \Plugins\HumpchiesLocationOptions());
            self::$data['added_tags'] = [];
            return;
        } else {
            self::$data['added_tags']  = $_POST['tags'];            
        }
        $originalPostData =  $_POST;
        $cleanphone = $_POST['phone'];
        $postedImages = array();
        $mainImgIndex =  $_POST['main_img'];
        $processSelfCheckout =  !empty($_POST['pr_sch']) && $_POST['pr_sch'] == 'pr_sch';
        $package= $_POST['package'];
        $category= $_POST['mtl_category'];
        unset($_POST['e_i']);
        unset($_POST['main_img']);
        unset($_POST['mtl_category']);
        unset($_POST['package']);
        
        foreach ($_FILES['images']['name'] as $i => $imageName){
            if (is_file($_FILES['images']['tmp_name'][$i])){
                $postedImages[$i] = array(
                    'name'=>$imageName,
                    'size'=>$_FILES['images']['size'][$i],
                    'type'=>$_FILES['images']['type'][$i],
                    'tmp_name'=>$_FILES['images']['tmp_name'][$i],
                    'error'=>$_FILES['images']['error'][$i],
                    'is_main' => (int)($i == $mainImgIndex)
                );
            }
        }
        
        if($_POST['addprice']) {
            
            switch(true) {
                case isset($_POST['price_min']) && $_POST['price_min'] > 0 :
                    $_POST['title'] .= " \$" . $_POST['price_min'];
                    break;
                case isset($_POST['price_max']) && $_POST['price_max'] > 0 :
                    $_POST['title'] .= " \$" . $_POST['price_max'];
                    break;
                case isset($_POST['price']) && $_POST['price'] > 0 :
                    $_POST['title'] .= " \$" . $_POST['price'];
                    break;
                default: 
                    break;
            }
            
            //$_POST['title'] .= " \$" . $_POST['price'];
        }

        $new_ad_validity = self::validateNewV2($user_id, $_POST, $postedImages);

        //self::pushPlugIn('city_options', new \Plugins\HumpchiesCityOptions(\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME, $_POST[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME]));
        self::pushPlugIn('category_options', new \Plugins\HumpchiesCategoryOptions(\Plugins\HumpchiesCategoryOptions::DEFAULT_SELECT_NAME, $_POST[\Plugins\HumpchiesCategoryOptions::DEFAULT_SELECT_NAME]));
        self::pushPlugIn('location_options', new \Plugins\HumpchiesLocationOptions(\Plugins\HumpchiesLocationOptions::DEFAULT_SELECT_NAME, $_POST[\Plugins\HumpchiesLocationOptions::DEFAULT_SELECT_NAME]));

        if( is_array($new_ad_validity) && $new_ad_validity['dimention'] === 'error' ){
            self::addMessage($new_ad_validity['msg'], 'error');
            return;
        }

        // Validate the data entered for the new ad...
        switch($new_ad_validity)
        {
            // We are GOLDEN! no errors...move on to insert the new ad into the DB
            case self::NO_ERROR:
                break;

            /**
             * Hum!... something shady is going on...the city options are provided
             * via a select. Yet, the user has managed to fuck it up... I wonder
             * how... nevermind that ... kick the user out of here.
             */
            case self::ERROR_CITY_IS_INVALID:
                self::addMessage('[ERROR-CITY-IS-INVALID]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                return FALSE;
                break;

            // There is no title for this ad...
            case self::ERROR_TITLE_IS_EMPTY:
                self::addMessage('[ERROR-TITLE-IS-EMPTY]', 'error');
                return;
                break;

            // The title is too long...
            case self::ERROR_TITLE_IS_TOO_LONG:
                self::addMessage('[ERROR-TITLE-IS-TOO-LONG] ' . \Modules\Humpchies\AdModel::MAX_LEN_TITLE . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The title is too short...
            case self::ERROR_TITLE_IS_TOO_SHORT:
                self::addMessage('[ERROR-TITLE-IS-TOO-SHORT] ' . \Modules\Humpchies\AdModel::MIN_LEN_TITLE . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The description is empty
            case self::ERROR_DESCRIPTION_IS_EMPTY:
                self::addMessage('[ERROR-DESCRIPTION-IS-EMPTY]', 'error');
                return;
                break;

            // The description is too long...
            case self::ERROR_DESCRIPTION_IS_TOO_LONG:
                self::addMessage('[ERROR-DESCRIPTION-IS-TOO-LONG] ' . \Modules\Humpchies\AdModel::MAX_LEN_DESCRIPTION . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The description is too short
            case self::ERROR_DESCRIPTION_IS_TOO_SHORT:
                self::addMessage('[ERROR-DESCRIPTION-IS-TOO-SHORT] ' . \Modules\Humpchies\AdModel::MIN_LEN_DESCRIPTION . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The description is too long...
            case self::ERROR_DESCRIPTION_IS_DUPLICATED:
                self::addMessage('[ERROR-DESCRIPTION-IS-DUPLICATED] ', 'error');
                return;
                break;    
            

            // The price is invalid
            case self::ERROR_PRICE_IS_INVALID: // . \Modules\Humpchies\AdModel::MAX_PRICE
                self::addMessage('[ERROR-PRICE-IS-INVALID] ', 'error');
                return;
                break;

            // Price conditions are too long...
            case self::ERROR_PRICE_CONDITIONS_IS_TOO_LONG:
                self::addMessage('[ERROR-PRICE-CONDITIONS-IS-TOO-LONG] ' . \Modules\Humpchies\AdModel::MAX_LEN_PRICE_CONDITIONS . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            /**
             * Hum!... again...something shady is going on...the category options
             * are provided via a select. Yet, the user has managed to fuck it
             * up... I wonder how... nevermind that ... kick the user out of here.
             */
            case self::ERROR_CATEGORY_IS_INVALID:
                self::addMessage('[ERROR-CATEGORY-IS-INVALID]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                return FALSE;
                break;

            // The phone number is empty
            case self::ERROR_PHONE_IS_EMPTY:
                self::addMessage('[ERROR-PHONE-IS-EMPTY]', 'error');
                return;
                break;

            // The phone number is empty...
            case self::ERROR_PHONE_IS_INVALID:
                self::addMessage('[ERROR-PHONE-IS-INVALID]', 'error');
                return;
                break;

            // The phone number not from CA...
            case self::ERROR_NOT_SUPPORTED_COUNTRY:
                self::addMessage('[ERROR-PHONE-NOT-SUPPORTED-COUNTRY]', 'error');
                return;
                break;

            /**
             * Hum!... yet again...something shady is going on...the category options
             * are provided via a select. Yet, the user has managed to fuck it
             * up... I wonder how... nevermind that ... kick the user out of here.
             */
            case self::ERROR_LOCATION_IS_INVALID:
                self::addMessage('[ERROR-LOCATION-IS-INVALID]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                return FALSE;
                break;

            /**
             * Hum!... yet again...something shady is going on...the user id is
             * retrieved from the session. Somwhoe the user has managed to fuck
             * it up. This indicates that something shady is going...kick
             * the user out of here!
             */
            case self::ERROR_USER_ID_IS_INVALID:
                \Core\Utils\Session::abandon();
                self::addMessage('[ERROR-USER-ID-IS-INVALID]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                break;

            // The user is banned...
            case self::ERROR_USER_IS_NOT_ALLOWED:
                \Core\Utils\Session::abandon();
                self::addMessage('[ERROR-USER-IS-NOT-ALLOWED]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                break;

            // One or more images are too big...
            case self::ERROR_IMAGE_SIZE_IS_TOO_BIG;
                self::addMessage("$postedImages [ERROR-IMAGE-IS-TOO-BIG]", 'error');
                return;
                break;

            // The image type is invalid...
            case self::ERROR_IMAGE_TYPE_IS_INVALID;
                self::addMessage("$postedImages [ERROR-IMAGE-TYPE-IS-INVALID]", 'error');
                return;
                break;

            // Failed to resample the image... might want to further break down this error, support exists already
            case self::ERROR_IMAGE_RESAMPLING_FAILED;
                self::addMessage("$postedImages [ERROR-IMAGE-RESAMPLING-FAILED]", 'error');
                return;
                break;

            // Something went wrong with the upload....
            case self::ERROR_IMAGE_ERROR_UNHANDLED;
                self::addMessage("[ERROR_IMAGE_ERROR_UNHANDLED]: $postedImages", 'error');
                return;
                break;

            // Unhandled error...
            default:
                self::addMessage("[ERROR-UNHANDLED]: $new_ad_validity", 'error');
                return;
        }

        // post city_id and region_id
        $city_details =  \Modules\Humpchies\CityModel::getCityDetails($_POST['city']);
        //var_dump($city_details);
        if(!empty($city_details)) {
            
            $_POST['city_id'] = $city_details['id'];
            $_POST['region_id'] = $city_details['region_id'];
        }

        $tmp_city_code = $_POST[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME];
        $_POST[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME] = \Modules\Humpchies\CityModel::parseSupportedCityCode($_POST[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME]);

        $_POST['user_id'] = $user_id;
        $_POST['date_added'] = time();
        $_POST['ad_ip']     = ip2long(self::userIP());
        $geo = \Core\library\Geo::getLocation(self::userIp());
        $_POST['ad_country']  = $geo['country'];
        $_POST['ad_location'] = $geo['city']. ", " . $geo['zone'] . ", ". $geo['countryName'] ;

        $user_auto_approval = \Modules\Humpchies\UserModel::getUserAutoApprovalById( $user_id );
        if( $user_auto_approval['auto_approval'] ) {
            $_POST['status'] = 'active';
            $_POST['auto_approved'] = 1;
        }

        if( isset($_POST['phone']) && !empty($_POST['phone']) ){
            if( \Modules\Humpchies\AdModel::checkIfPhoneBlacklisted( $_POST['phone'] ) ){
                $_POST['status'] = 'banned';
            }
        }
       $userDetails = \Modules\Humpchies\UserModel::getDetailsById($user_id) ;          
       if( \Modules\Humpchies\AdModel::checkIfEmailBlacklisted($userDetails[0]['email']) ){
            $_POST['status'] = 'banned';
            $_POST['comment'] = 'Banned by Email '. $userDetails[0]['email'];
        }
    
        
        unset($_POST['addprice']);
        
        // VALIDATE PHONE WHEN DONE 
       if($_POST['validate_phone']){
           $cleanphone= preg_replace("/[^0-9]/is","",$cleanphone);
            if(!empty($originalPostData['phone_country_code'])){ 
            $phone = (preg_match("`(^|\\+)" . $originalPostData['phone_country_code'] . "`is",$cleanphone))? $cleanphone : $originalPostData['phone_country_code'] . $cleanphone;
               
            }else{
                $phone = "1" . $_POST['phone'];
            }   
            //echo("|".$phone."|");
           // die();
            $phoneValidation = \Core\library\checkMobi::validationCall("+" . $phone);
            $_POST['validation_id'] = $phoneValidation['id'];
            $_POST['dialing_number'] = $phoneValidation['dialing_number'];
            unset($_POST['validate_phone']);
            unset($_POST['video-upload']);
            //var_dump($phoneValidation);
//            die();
           
        }
        
         // make sure location is never empty
        if(!in_array($_POST['location'], ['incalls','outcalls','both','unknown'])) {
            $_POST['location'] =  'incalls';
        }
//        
        // Create the new Ad
        // =================
        //var_dump($_POST);
//        die();
        $rotator = $_POST['rotator'];
        unset($_POST["rotator"]);
        $_POST['vpn'] = (\Core\library\checkVpn::isVpn(\Core\Utils\Device::getDeviceIp_new()))? "Y" : "N";
        // $_POST['vpn'] = (\Core\library\checkVpn::isVpn("5.62.24.31"))? "Y" : "N";
       
       
        $attached_video = \Modules\Humpchies\AdModel::getUserVideobyID(\Core\Utils\Session::getCurrentUserID(), $_POST['attached_video']);
        $_POST['attached_video'] = $attached_video[0]['video_name'];
        
       // update has_comments and blocked_comments 
       $no_comments = \Modules\Humpchies\CommentsModel::getUserCommentsCount( $user_id, 1);
       if($no_comments > 0) {
            $_POST['has_comments'] = 1;    
       }
       $advertiser = \Modules\Humpchies\UserModel::getDetailsById($user_id);
       $_POST['blocked_comments'] = $advertiser[0]['disable_comments'];
        
        $new_ad_id = \Modules\Humpchies\AdModel::create($_POST);
        unset($_POST['vpn']);
        $_POST['rotator'] = $rotator;
        if(isset($_POST['auto_approved'])) {
            unset($_POST['auto_approved']);
        }

        if(isset($_POST['status']) && $_POST['status'] == 'active') {
            unset($_POST['status']);
        }

        /**
         * Handle possible erros during ad creation. At the time of this
         * write, one case is handled:
         *
         * Duplicated description
         *
         * Any other error is handled as 'unknown'. ToDo: explore the posibilities
         * to further handle potential issues.
         */
        if(is_int($new_ad_id) === FALSE || $new_ad_id <= 0)
        {
            $_POST[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME] = $tmp_city_code;

            switch($new_ad_id)
            {
                case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_DUPLICATED:
                    self::addMessage('[ERROR-DESCRIPTION-IS-DUPLICATED]', 'error');
                    return;
                    break;
                default:
                    return self::addMessage("[ERROR-UNHANDLED] ", 'error');
            }
        }

        $_POST['id'] = $new_ad_id;

        //if(empty($postedImages))
        //    $postedImages = array(array('tmp_name' => \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_path') . 'filler.jpg'));

        $saving_images_result = \Modules\Humpchies\AdImageModel::savePostedImagesV2($_POST, $postedImages);

        unset($postedImages['data']);
        unset($_POST['id']);
        unset($_POST['ad_ip']);
        unset($_POST['ad_country']);
        unset($_POST['ad_location']);
        unset($_POST['attached_video']);

        switch($saving_images_result)
        {
            case \Modules\Humpchies\AdImageModel::NO_ERROR:
                break;
            case \Modules\Humpchies\AdImageModel::ERROR_SOME_IMAGES_COULD_NOT_BE_SAVED:
                \Core\DBConn::delete(\Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG, \Modules\Humpchies\AdModel::DEFAULT_DB_TABLE, "id = $new_ad_id");
                self::addMessage('[ERROR-AD-CREATION-FAILED]. [ERROR-SOME-IMAGES-FAILED-TO-BE-SAVED]', 'error');
                return;
            default:
                \Core\DBConn::delete(\Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG, \Modules\Humpchies\AdModel::DEFAULT_DB_TABLE, "id = $new_ad_id");
                self::addMessage("[ERROR-AD-CREATION-FAILED]: $saving_images_result", 'error');
                return;
        }
        // ----------------- SAVE LOCATION ---------------------- //
        $location_fields = [
            '`ad_id`'             => $new_ad_id,
            '`user_id`'           => \Core\Utils\Session::getCurrentUserID(),
            '`ad_city_id`'        => $_POST['city_id'],
            '`ad_city_name`'      => $_POST['city'],
            '`lat`'               => isset($originalPostData['maps_lat'])? floatval($originalPostData['maps_lat']) : '',
            '`long`'              => isset($originalPostData['maps_long'])? floatval($originalPostData['maps_long']) : '',
            '`current_city_name`' => isset($originalPostData['maps_city'])? $originalPostData['maps_city'] : '',
            '`hide_map`'          => (isset($originalPostData['hide_map']) && $originalPostData['hide_map'] == 'yes')? 1 : 0
        ];
        if(isset($originalPostData['maps_long']) && floatval($originalPostData['maps_lat']) != 0 && isset($originalPostData['maps_lat'])) {
            $ad_location_result = \Modules\Humpchies\AdLocationModel::createV2($location_fields);
        }
        
        
        // save TAGS
       //var_dump("ORIGINAL POST");
       //var_dump($originalPostData);
       //if(\Core\Utils\Session::getCurrentUserID() == 63661){ // only testme
        \Modules\Humpchies\AdTagModel::create($new_ad_id, $originalPostData['tags']);
       //}
        
        // should i remove ad is location does not save properly???
        //switch($ad_location_result)
//        {
//            case \Modules\Humpchies\AdLocationModel::ERROR_UNKNOWN:
//                \Core\DBConn::delete(\Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG, \Modules\Humpchies\AdModel::DEFAULT_DB_TABLE, "id = $new_ad_id");
//                self::addMessage("[ERROR-AD-CREATION-FAILED]: $saving_images_result", 'error');   // ??
//                return;
//            default:
//                break;
//        }
        
        

        $updateFields = NULL;

        if (\Modules\Humpchies\UserModel::isNew($user_id)) {
            $updateFields = $postedImages;
        } else {
            $updateFields = array_merge($postedImages, array('date_released' => time())); /*'status' => ( $_POST['status'] == 'banned' ) ? 'banned' : 'active',*/
        }

        if (\Core\DBConn::update(\Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG, \Modules\Humpchies\AdModel::DEFAULT_DB_TABLE, $updateFields, "id = $new_ad_id") === FALSE) {
            self::addMessage("[ERROR-AD-ACTIVATION-FAILED]", 'warning');
                self::redirect('/ad/my-listings');
            return FALSE;
        }
            
        \Core\Cache::save(\Core\Utils\Device::getDeviceIp(), 1, self::getAdPostTimeSpan());
        \Modules\Humpchies\UserModel::trackIp(\Core\Utils\Session::getCurrentUserID(), \Core\Utils\Device::getDeviceIp());
        \Modules\Humpchies\UserModel::trackPhone(\Core\Utils\Session::getCurrentUserID(), $_POST['phone']);
        /*if ($processSelfCheckout){
            $postdata = array(
                'user_id' => $user_id,
                'ad_id' => $new_ad_id,
                'category' => $category,
                'package' => $package,
            );
            $_POST = $postdata;
            $response = \Modules\Humpchies\PaymentController::shoping_cart(true);
            if ($response['status'] == 1){
                self::redirect($response['url']);
                return false;
            }else{
                //var_dump($postdata['package']);
                self::addMessage($response['errors']['package'].$postdata['package'], 'warning');
            }

        }*/
        
        if($phoneValidation['dialing_number']){
             self::redirect(\Core\Utils\Navigation::urlFor('ad', 'phone-validation', ["ad" => $new_ad_id ], true, true));
        }else{
            if(($user_id == 63661 || 1) &&  $_POST['status'] != 'banned'  ){
                self::redirect(\Core\Utils\Navigation::urlFor('ad', 'confirmation-v2', array('id' => $new_ad_id), false, true));
            }else{
                self::redirect(\Core\Utils\Navigation::urlFor('ad', 'confirmation', array('id' => $new_ad_id), false, true));
                
            }
            //self::addMessage('[SUCCESS-AD-CREATION-SUCCESFUL]');
            //self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings', ["ad" => $new_ad_id ], true, true));
        return FALSE;

    }
     }
   
    public static function createMaV3()  {
 
        \Core\Router::getParsedURL()->layout_settings['footer_mode']=\Core\View::HIDDEN_MODE;
        \Core\Router::getParsedURL()->layout_settings['action_mode']=\Core\View::DEVICE_MODE;

        if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect( '/');
        }
        $user_id = \Core\Utils\Session::getCurrentUserID();

        if( !\Modules\Humpchies\UserModel::isUserActiveById( $user_id ) ){
            \Core\Utils\Session::abandon();
            self::addMessage('[SUCCESS-LOGOUT-SUCCESFUL]');
            self::redirect(\Core\Utils\Navigation::urlFor('/'));
        }
        // Disable browser cache
        self::disableCache();
        $total_ads = \Modules\Humpchies\AdModel::getTotalAdsFromUser($user_id);
        if (\Core\Cache::get(\Core\Utils\Device::getDeviceIp()) !== FALSE && intval($total_ads[0]['cate'])) {
             
             
            self::addMessage('[ERROR-AD-POST-WAIT-TIME] ' . ($minutes = (self::getAdPostTimeSpan() / 60)) . ' [LABEL-MINUTE]', 'error');
            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
            return;
           
            //self::addMessage('[ERROR-AD-POST-WAIT-TIME] ' . ($minutes = (self::getAdPostTimeSpan() / 60)) . ' [LABEL-MINUTE]', 'error');
            //self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
           // return;
        } elseif (\Modules\Humpchies\UserModel::isBehindBannedIp(\Core\Utils\Device::getDeviceIp())) {
            //self::handlePreviouslyBanned(\Core\Utils\Session::getCurrentUserID());
        }

        // Set some page values...
        self::setTitle('[LABEL-POST-AD-V2]');                      // Title
        self::setDescription('[LABEL-POST-AD]');                // Description
        self::setHeaderClass("has_breadcrumb");
        
        // Enable custom CSS support
        // ========================= <link media="all" rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        \Core\Controller::setExtraHead('
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery-validation-1.19.0/dist/jquery.validate.min.js"></script>
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile/touch')).'/create_ad_new.js?v=1.02"></script>
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'pc/lant_uploader.js?v=0.09"></script>
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'intl-tel-input-new/build/js/intlTelInput.min.js" charset="utf-8"></script>
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'intl-tel-input-new/build/js/index.js?v=3.3" charset="utf-8"></script>
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'pc/temp_ad.js?v=3.3"></script>
            '
        );
       /* \Core\Controller::setExtraHead('
            <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'setup_map.js?v=0.5"></script>'
        );*/
        
        if(\Core\Utils\Session::getLocale() == 'fr-ca'){
            \Core\Controller::setExtraHead('<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/localization/messages_fr.js"></script>');
        } 
        
        //if(\Core\Utils\Session::getCurrentUserID() == 63661){ // only testme
            \Core\Controller::setExtraHead('<script type="text/javascript" src="https://api-maps.yandex.ru/2.1/?lang=fr_CA&amp;apikey=a2322142-06fa-4970-a82e-2543c4763bcb"></script>');    
            \Core\Controller::setExtraHead('<script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'setup_yandex_new.js?v=1.00"></script>'); 
        //}    
        
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile/touch').'/create_ad_new.css?v=1.01">');
        
        \Core\Controller::setExtraHead('
            <link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('css') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/post.css?v=5">
            <link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('js').'intl-tel-input-new/build/css/intlTelInput.min.css">'
        );
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . '/modal.css?v=3.16">');
        
        // flag the max lengths of the fields that support this attribute...
        self::$data['max_len_title']                = \Modules\Humpchies\AdModel::MAX_LEN_TITLE;
        self::$data['max_len_price']                = strlen((string)\Modules\Humpchies\AdModel::MAX_PRICE);
        self::$data['max_len_price_conditions']     = \Modules\Humpchies\AdModel::MAX_LEN_PRICE_CONDITIONS;
        self::$data['max_len_phone']                = self::MAX_LEN_PHONE_ON_FORM;
        self::$data['videolocation']                = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::videos')."temp/";
        self::$data['imageslocation']                = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_images');

        self::$data['available_packages'] = \Modules\Humpchies\PaymentModel::get_available_packages();
        self::$data['user_videos'] = \Modules\Humpchies\AdModel::getUserVideo(\Core\Utils\Session::getCurrentUserID());

        // get tags
        //if(\Core\Utils\Session::getCurrentUserID() == 63661){ // only testme
            $lang =  (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';
            self::$data["tags"] = \Modules\Humpchies\TagModel::getTagsList($lang);
            //var_dump(self::$data["tags"]); 
        //}
        
        // Bail out if the form hasn't been posted
        // =======================================
        if(!$_POST)
        {
            //self::pushPlugIn('city_options', new \Plugins\HumpchiesCityOptions());
            self::pushPlugIn('category_options', new \Plugins\HumpchiesCategoryOptions());
            self::pushPlugIn('location_options', new \Plugins\HumpchiesLocationOptions());
            return;
        }
        $originalPostData =  $_POST;
        $cleanphone = $_POST['phone'];
        $postedImages = array();
        $mainImgIndex =  $_POST['main_img'];
        $processSelfCheckout =  !empty($_POST['pr_sch']) && $_POST['pr_sch'] == 'pr_sch';
        $package= $_POST['package'];
        $category= $_POST['mtl_category'];
        unset($_POST['e_i']);
        unset($_POST['main_img']);
        unset($_POST['mtl_category']);
        unset($_POST['package']);
        
        foreach ($_FILES['images']['name'] as $i => $imageName){
            if (is_file($_FILES['images']['tmp_name'][$i])){
                $postedImages[$i] = array(
                    'name'=>$imageName,
                    'size'=>$_FILES['images']['size'][$i],
                    'type'=>$_FILES['images']['type'][$i],
                    'tmp_name'=>$_FILES['images']['tmp_name'][$i],
                    'error'=>$_FILES['images']['error'][$i],
                    'is_main' => (int)($i == $mainImgIndex)
                );
            }
        }
        
        if($_POST['addprice'] && $_POST['price']){
            $_POST['title'] .= " \$" . $_POST['price'];
        }

        $new_ad_validity = self::validateNewV2($user_id, $_POST, $postedImages);

        //self::pushPlugIn('city_options', new \Plugins\HumpchiesCityOptions(\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME, $_POST[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME]));
        self::pushPlugIn('category_options', new \Plugins\HumpchiesCategoryOptions(\Plugins\HumpchiesCategoryOptions::DEFAULT_SELECT_NAME, $_POST[\Plugins\HumpchiesCategoryOptions::DEFAULT_SELECT_NAME]));
        self::pushPlugIn('location_options', new \Plugins\HumpchiesLocationOptions(\Plugins\HumpchiesLocationOptions::DEFAULT_SELECT_NAME, $_POST[\Plugins\HumpchiesLocationOptions::DEFAULT_SELECT_NAME]));

        if( is_array($new_ad_validity) && $new_ad_validity['dimention'] === 'error' ){
            self::addMessage($new_ad_validity['msg'], 'error');
            return;
        }

        // Validate the data entered for the new ad...
        switch($new_ad_validity)
        {
            // We are GOLDEN! no errors...move on to insert the new ad into the DB
            case self::NO_ERROR:
                break;

            /**
             * Hum!... something shady is going on...the city options are provided
             * via a select. Yet, the user has managed to fuck it up... I wonder
             * how... nevermind that ... kick the user out of here.
             */
            case self::ERROR_CITY_IS_INVALID:
                self::addMessage('[ERROR-CITY-IS-INVALID]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                return FALSE;
                break;

            // There is no title for this ad...
            case self::ERROR_TITLE_IS_EMPTY:
                self::addMessage('[ERROR-TITLE-IS-EMPTY]', 'error');
                return;
                break;

            // The title is too long...
            case self::ERROR_TITLE_IS_TOO_LONG:
                self::addMessage('[ERROR-TITLE-IS-TOO-LONG] ' . \Modules\Humpchies\AdModel::MAX_LEN_TITLE . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The title is too short...
            case self::ERROR_TITLE_IS_TOO_SHORT:
                self::addMessage('[ERROR-TITLE-IS-TOO-SHORT] ' . \Modules\Humpchies\AdModel::MIN_LEN_TITLE . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The description is empty
            case self::ERROR_DESCRIPTION_IS_EMPTY:
                self::addMessage('[ERROR-DESCRIPTION-IS-EMPTY]', 'error');
                return;
                break;

            // The description is too long...
            case self::ERROR_DESCRIPTION_IS_TOO_LONG:
                self::addMessage('[ERROR-DESCRIPTION-IS-TOO-LONG] ' . \Modules\Humpchies\AdModel::MAX_LEN_DESCRIPTION . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The description is too short
            case self::ERROR_DESCRIPTION_IS_TOO_SHORT:
                self::addMessage('[ERROR-DESCRIPTION-IS-TOO-SHORT] ' . \Modules\Humpchies\AdModel::MIN_LEN_DESCRIPTION . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The description is too long...
            case self::ERROR_DESCRIPTION_IS_DUPLICATED:
                self::addMessage('[ERROR-DESCRIPTION-IS-DUPLICATED] ', 'error');
                return;
                break;    
            

            // The price is invalid
            case self::ERROR_PRICE_IS_INVALID: // . \Modules\Humpchies\AdModel::MAX_PRICE
                self::addMessage('[ERROR-PRICE-IS-INVALID] ', 'error');
                return;
                break;

            // Price conditions are too long...
            case self::ERROR_PRICE_CONDITIONS_IS_TOO_LONG:
                self::addMessage('[ERROR-PRICE-CONDITIONS-IS-TOO-LONG] ' . \Modules\Humpchies\AdModel::MAX_LEN_PRICE_CONDITIONS . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            /**
             * Hum!... again...something shady is going on...the category options
             * are provided via a select. Yet, the user has managed to fuck it
             * up... I wonder how... nevermind that ... kick the user out of here.
             */
            case self::ERROR_CATEGORY_IS_INVALID:
                self::addMessage('[ERROR-CATEGORY-IS-INVALID]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                return FALSE;
                break;

            // The phone number is empty
            case self::ERROR_PHONE_IS_EMPTY:
                self::addMessage('[ERROR-PHONE-IS-EMPTY]', 'error');
                return;
                break;

            // The phone number is empty...
            case self::ERROR_PHONE_IS_INVALID:
                self::addMessage('[ERROR-PHONE-IS-INVALID]', 'error');
                return;
                break;

            // The phone number not from CA...
            case self::ERROR_NOT_SUPPORTED_COUNTRY:
                self::addMessage('[ERROR-PHONE-NOT-SUPPORTED-COUNTRY]', 'error');
                return;
                break;

            /**
             * Hum!... yet again...something shady is going on...the category options
             * are provided via a select. Yet, the user has managed to fuck it
             * up... I wonder how... nevermind that ... kick the user out of here.
             */
            case self::ERROR_LOCATION_IS_INVALID:
                self::addMessage('[ERROR-LOCATION-IS-INVALID]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                return FALSE;
                break;

            /**
             * Hum!... yet again...something shady is going on...the user id is
             * retrieved from the session. Somwhoe the user has managed to fuck
             * it up. This indicates that something shady is going...kick
             * the user out of here!
             */
            case self::ERROR_USER_ID_IS_INVALID:
                \Core\Utils\Session::abandon();
                self::addMessage('[ERROR-USER-ID-IS-INVALID]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                break;

            // The user is banned...
            case self::ERROR_USER_IS_NOT_ALLOWED:
                \Core\Utils\Session::abandon();
                self::addMessage('[ERROR-USER-IS-NOT-ALLOWED]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                break;

            // One or more images are too big...
            case self::ERROR_IMAGE_SIZE_IS_TOO_BIG;
                self::addMessage("$postedImages [ERROR-IMAGE-IS-TOO-BIG]", 'error');
                return;
                break;

            // The image type is invalid...
            case self::ERROR_IMAGE_TYPE_IS_INVALID;
                self::addMessage("$postedImages [ERROR-IMAGE-TYPE-IS-INVALID]", 'error');
                return;
                break;

            // Failed to resample the image... might want to further break down this error, support exists already
            case self::ERROR_IMAGE_RESAMPLING_FAILED;
                self::addMessage("$postedImages [ERROR-IMAGE-RESAMPLING-FAILED]", 'error');
                return;
                break;

            // Something went wrong with the upload....
            case self::ERROR_IMAGE_ERROR_UNHANDLED;
                self::addMessage("[ERROR_IMAGE_ERROR_UNHANDLED]: $postedImages", 'error');
                return;
                break;

            // Unhandled error...
            default:
                self::addMessage("[ERROR-UNHANDLED]: $new_ad_validity", 'error');
                return;
        }

        // post city_id and region_id
        $city_details =  \Modules\Humpchies\CityModel::getCityDetails($_POST['city']);
//        var_dump($city_details);
        if(!empty($city_details)) {
            
            $_POST['city_id'] = $city_details['id'];
            $_POST['region_id'] = $city_details['region_id'];
        }

        $tmp_city_code = $_POST[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME];
        $_POST[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME] = \Modules\Humpchies\CityModel::parseSupportedCityCode($_POST[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME]);

        $_POST['user_id'] = $user_id;
        $_POST['date_added'] = time();
        $_POST['ad_ip']     = ip2long(self::userIP());
        $geo = \Core\library\Geo::getLocation(self::userIp());
        $_POST['ad_country']  = $geo['country'];
        $_POST['ad_location'] = $geo['city']. ", " . $geo['zone'] . ", ". $geo['countryName'] ;

        $user_auto_approval = \Modules\Humpchies\UserModel::getUserAutoApprovalById( $user_id );
        if( $user_auto_approval['auto_approval'] ) {
            $_POST['status'] = 'active';
            $_POST['auto_approved'] = 1;
        }

        if( isset($_POST['phone']) && !empty($_POST['phone']) ){
            if( \Modules\Humpchies\AdModel::checkIfPhoneBlacklisted( $_POST['phone'] ) ){
                $_POST['status'] = 'banned';
            }
        }
        
        unset($_POST['addprice']);
        
        // VALIDATE PHONE WHEN DONE 
       if($_POST['validate_phone']){
           $cleanphone= preg_replace("/[^0-9]/is","",$cleanphone);
            if(!empty($originalPostData['phone_country_code'])){ 
            $phone = (preg_match("`(^|\\+)" . $originalPostData['phone_country_code'] . "`is",$cleanphone))? $cleanphone : $originalPostData['phone_country_code'] . $cleanphone;
               
            }else{
                $phone = "1" . $_POST['phone'];
            }   
            //echo("|".$phone."|");
           // die();
            $phoneValidation = \Core\library\checkMobi::validationCall("+" . $phone);
            $_POST['validation_id'] = $phoneValidation['id'];
            $_POST['dialing_number'] = $phoneValidation['dialing_number'];
            unset($_POST['validate_phone']);
            unset($_POST['video-upload']);
            //var_dump($phoneValidation);
//            die();
           
        }
          
         // make sure location is never empty
        if(!in_array($_POST['location'], ['incalls','outcalls','both','unknown'])) {
            $_POST['location'] =  'incalls';
        }
//        
        // Create the new Ad
        // =================
        //var_dump($_POST);
//        die();
        $rotator = $_POST['rotator'];
        unset($_POST["rotator"]);
        //$_POST['vpn'] = (\Core\library\checkVpn::isVpn(\Core\Utils\Device::getDeviceIp_new()))? "Y" : "N";
       // $_POST['vpn'] = (\Core\library\checkVpn::isVpn("5.62.24.31"))? "Y" : "N";
       
       
        $attached_video = \Modules\Humpchies\AdModel::getUserVideobyID(\Core\Utils\Session::getCurrentUserID(), $_POST['attached_video']);
        $_POST['attached_video'] = $attached_video[0]['video_name'];
        
        // accepts comments and has_comments fields
       $no_comments = \Modules\Humpchies\CommentsModel::getUserCommentsCount( $user_id, 1);
       if($no_comments > 0) {
            $_POST['has_comments'] = 1;    
       }
       $advertiser = \Modules\Humpchies\UserModel::getDetailsById($user_id);
       $_POST['blocked_comments'] = $advertiser[0]['disable_comments'];
        $new_ad_id = \Modules\Humpchies\AdModel::create($_POST);
        unset($_POST['vpn']);
        $_POST['rotator'] = $rotator;
        if(isset($_POST['auto_approved'])) {
            unset($_POST['auto_approved']);
        }

        if(isset($_POST['status']) && $_POST['status'] == 'active') {
            unset($_POST['status']);
        }

        /**
         * Handle possible erros during ad creation. At the time of this
         * write, one case is handled:
         *
         * Duplicated description
         *
         * Any other error is handled as 'unknown'. ToDo: explore the posibilities
         * to further handle potential issues.
         */
        if(is_int($new_ad_id) === FALSE || $new_ad_id <= 0)
        {
            $_POST[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME] = $tmp_city_code;

            switch($new_ad_id)
            {
                case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_DUPLICATED:
                    self::addMessage('[ERROR-DESCRIPTION-IS-DUPLICATED]', 'error');
                    return;
                    break;
                default:
                    return self::addMessage("[ERROR-UNHANDLED] ", 'error');
            }
        }

        $_POST['id'] = $new_ad_id;

        //if(empty($postedImages))
        //    $postedImages = array(array('tmp_name' => \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_path') . 'filler.jpg'));

        $saving_images_result = \Modules\Humpchies\AdImageModel::savePostedImagesV2($_POST, $postedImages);

        unset($postedImages['data']);
        unset($_POST['id']);
        unset($_POST['ad_ip']);
        unset($_POST['ad_country']);
        unset($_POST['ad_location']);
        unset($_POST['attached_video']);

        switch($saving_images_result)
        {
            case \Modules\Humpchies\AdImageModel::NO_ERROR:
                break;
            case \Modules\Humpchies\AdImageModel::ERROR_SOME_IMAGES_COULD_NOT_BE_SAVED:
                \Core\DBConn::delete(\Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG, \Modules\Humpchies\AdModel::DEFAULT_DB_TABLE, "id = $new_ad_id");
                self::addMessage('[ERROR-AD-CREATION-FAILED]. [ERROR-SOME-IMAGES-FAILED-TO-BE-SAVED]', 'error');
                return;
            default:
                \Core\DBConn::delete(\Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG, \Modules\Humpchies\AdModel::DEFAULT_DB_TABLE, "id = $new_ad_id");
                self::addMessage("[ERROR-AD-CREATION-FAILED]: $saving_images_result", 'error');
                return;
        }
        // ----------------- SAVE LOCATION ---------------------- //
        $location_fields = [
            '`ad_id`'             => $new_ad_id,
            '`user_id`'           => \Core\Utils\Session::getCurrentUserID(),
            '`ad_city_id`'        => $_POST['city_id'],
            '`ad_city_name`'      => $_POST['city'],
            '`lat`'               => isset($originalPostData['maps_lat'])? floatval($originalPostData['maps_lat']) : '',
            '`long`'              => isset($originalPostData['maps_long'])? floatval($originalPostData['maps_long']) : '',
            '`current_city_name`' => isset($originalPostData['maps_city'])? $originalPostData['maps_city'] : '',
            '`hide_map`'          => (isset($originalPostData['hide_map']) && $originalPostData['hide_map'] == 'yes')? 1 : 0
        ];
        if(isset($originalPostData['maps_long']) && floatval($originalPostData['maps_lat']) != 0 && isset($originalPostData['maps_lat'])) {
            $ad_location_result = \Modules\Humpchies\AdLocationModel::create($location_fields);
        }
        
        
        // save TAGS
       //var_dump("ORIGINAL POST");
       //var_dump($originalPostData);
       //if(\Core\Utils\Session::getCurrentUserID() == 63661){ // only testme
        \Modules\Humpchies\AdTagModel::create($new_ad_id, $originalPostData['tags']);
       //}
        
        // should i remove ad is location does not save properly???
        //switch($ad_location_result)
//        {
//            case \Modules\Humpchies\AdLocationModel::ERROR_UNKNOWN:
//                \Core\DBConn::delete(\Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG, \Modules\Humpchies\AdModel::DEFAULT_DB_TABLE, "id = $new_ad_id");
//                self::addMessage("[ERROR-AD-CREATION-FAILED]: $saving_images_result", 'error');   // ??
//                return;
//            default:
//                break;
//        }
        
        

        $updateFields = NULL;

        if (\Modules\Humpchies\UserModel::isNew($user_id)) {
            $updateFields = $postedImages;
        } else {
            $updateFields = array_merge($postedImages, array('date_released' => time())); /*'status' => ( $_POST['status'] == 'banned' ) ? 'banned' : 'active',*/
        }

        if (\Core\DBConn::update(\Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG, \Modules\Humpchies\AdModel::DEFAULT_DB_TABLE, $updateFields, "id = $new_ad_id") === FALSE) {
                self::addMessage("[ERROR-AD-ACTIVATION-FAILED]", 'warning');
                self::redirect('/ad/my-listings');
                return FALSE;
            }
            
        \Core\Cache::save(\Core\Utils\Device::getDeviceIp(), 1, self::getAdPostTimeSpan());
        \Modules\Humpchies\UserModel::trackIp(\Core\Utils\Session::getCurrentUserID(), \Core\Utils\Device::getDeviceIp());
        \Modules\Humpchies\UserModel::trackPhone(\Core\Utils\Session::getCurrentUserID(), $_POST['phone']);
        /*if ($processSelfCheckout){
            $postdata = array(
                'user_id' => $user_id,
                'ad_id' => $new_ad_id,
                'category' => $category,
                'package' => $package,
            );
            $_POST = $postdata;
            $response = \Modules\Humpchies\PaymentController::shoping_cart(true);
            if ($response['status'] == 1){
                self::redirect($response['url']);
                return false;
            }else{
                //var_dump($postdata['package']);
                self::addMessage($response['errors']['package'].$postdata['package'], 'warning');
            }

        }*/
        
        if($phoneValidation['dialing_number']){
             self::redirect(\Core\Utils\Navigation::urlFor('ad', 'phone-validation', ["ad" => $new_ad_id ], true, true));
        }else{
            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'confirmation', array('id' => $new_ad_id), false, true));
            //self::addMessage('[SUCCESS-AD-CREATION-SUCCESFUL]');
            //self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings', ["ad" => $new_ad_id ], true, true));
            return FALSE;

        }
     }

    public static function confirmation_ma(){
       
        \Core\Router::getParsedURL()->layout_settings['footer_mode']=\Core\View::HIDDEN_MODE;
        \Core\Router::getParsedURL()->layout_settings['action_mode']=\Core\View::DEVICE_MODE;

        if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect( '/');
        }
        $user_id = \Core\Utils\Session::getCurrentUserID();

        if( !\Modules\Humpchies\UserModel::isUserActiveById( $user_id ) ){
            \Core\Utils\Session::abandon();
            self::addMessage('[SUCCESS-LOGOUT-SUCCESFUL]');
            self::redirect(\Core\Utils\Navigation::urlFor('/'));
        }
        // Disable browser cache
        self::disableCache();

       // if (\Core\Cache::get(\Core\Utils\Device::getDeviceIp()) !== FALSE) {
//            self::addMessage('[ERROR-AD-POST-WAIT-TIME] ' . ($minutes = (self::getAdPostTimeSpan() / 60)) . ' [LABEL-MINUTE]', 'error');
//            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
//            return;
//        } elseif (\Modules\Humpchies\UserModel::isBehindBannedIp(\Core\Utils\Device::getDeviceIp())) {
//            //self::handlePreviouslyBanned(\Core\Utils\Session::getCurrentUserID());
//        }

        // Set some page values...
        self::setTitle('[LABEL-POST-AD-V2]');                      // Title
        self::setDescription('[LABEL-POST-AD]');                // Description

        // Enable custom CSS support
        // ========================= <link media="all" rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        \Core\Controller::setExtraHead('
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile/touch')).'/create_ad.js?v=0.24"></script>
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'pc/lant_uploader.js?v=0.09"></script>
        '
        );
        \Core\Controller::setExtraHead('
        <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
        <link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile/touch').'/create_ad.css?v=0.08">
        ');


        // flag the max lengths of the fields that support this attribute...
        self::$data['max_len_title']                = \Modules\Humpchies\AdModel::MAX_LEN_TITLE;
        self::$data['max_len_price']                = strlen((string)\Modules\Humpchies\AdModel::MAX_PRICE);
        self::$data['max_len_price_conditions']     = \Modules\Humpchies\AdModel::MAX_LEN_PRICE_CONDITIONS;
        self::$data['max_len_phone']                = self::MAX_LEN_PHONE_ON_FORM;
        self::$data['adid']                         = $_GET['id'];

        self::$data['available_packages'] = \Modules\Humpchies\PaymentModel::get_available_packages();     
    }
     
    public static function confirmation2_ma(){
       
        \Core\Router::getParsedURL()->layout_settings['footer_mode']=\Core\View::HIDDEN_MODE;
        \Core\Router::getParsedURL()->layout_settings['action_mode']=\Core\View::DEVICE_MODE;

        if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect( '/');
        }
        $user_id = \Core\Utils\Session::getCurrentUserID();

        if( !\Modules\Humpchies\UserModel::isUserActiveById( $user_id ) ){
            \Core\Utils\Session::abandon();
            self::addMessage('[SUCCESS-LOGOUT-SUCCESFUL]');
            self::redirect(\Core\Utils\Navigation::urlFor('/'));
        }
        $adID = intval($_GET['id']);
        
        if(!\Modules\Humpchies\AdModel::userOwnsAd($adID, $user_id)){
            self::addMessage('[ERROR-AD-USER-ACCESS-DENIED]');
            self::redirect( '/ad/my-listings');
        }
        // Disable browser cache
        self::disableCache();

       // if (\Core\Cache::get(\Core\Utils\Device::getDeviceIp()) !== FALSE) {
//            self::addMessage('[ERROR-AD-POST-WAIT-TIME] ' . ($minutes = (self::getAdPostTimeSpan() / 60)) . ' [LABEL-MINUTE]', 'error');
//            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
//            return;
//        } elseif (\Modules\Humpchies\UserModel::isBehindBannedIp(\Core\Utils\Device::getDeviceIp())) {
//            //self::handlePreviouslyBanned(\Core\Utils\Session::getCurrentUserID());
//        }

        // Set some page values...
        self::setTitle('[LABEL-POST-AD-V2]');                      // Title
        self::setDescription('[LABEL-POST-AD]');                // Description

        // Enable custom CSS support
        // ========================= <link media="all" rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        \Core\Controller::setExtraHead('
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
    '
        );
        \Core\Controller::setExtraHead('
        <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
        <link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile/touch').'/create_ad.css?v=0.08">
        ');


        // flag the max lengths of the fields that support this attribute...
        self::$data['max_len_title']                = \Modules\Humpchies\AdModel::MAX_LEN_TITLE;
        self::$data['max_len_price']                = strlen((string)\Modules\Humpchies\AdModel::MAX_PRICE);
        self::$data['max_len_price_conditions']     = \Modules\Humpchies\AdModel::MAX_LEN_PRICE_CONDITIONS;
        self::$data['max_len_phone']                = self::MAX_LEN_PHONE_ON_FORM;
        self::$data['adid']                         = intval($_GET['id']);
        self::$data['ad'] = \Modules\Humpchies\AdModel::getDetailsOfSimpleAd(intval($_GET['id']));

        self::$data['available_packages'] = \Modules\Humpchies\PaymentModel::get_available_packages();     
    }
    
    public static function confirmation_payment_success_ma(){
       
        \Core\Router::getParsedURL()->layout_settings['footer_mode']=\Core\View::HIDDEN_MODE;
        \Core\Router::getParsedURL()->layout_settings['action_mode']=\Core\View::DEVICE_MODE;

        if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect( '/');
        }
        $user_id = \Core\Utils\Session::getCurrentUserID();

        if( !\Modules\Humpchies\UserModel::isUserActiveById( $user_id ) ){
            \Core\Utils\Session::abandon();
            self::addMessage('[SUCCESS-LOGOUT-SUCCESFUL]');
            self::redirect(\Core\Utils\Navigation::urlFor('/'));
        }
        $adID = intval($_GET['id']);
        
        if(!\Modules\Humpchies\AdModel::userOwnsAd($adID, $user_id)){
            self::addMessage('[ERROR-AD-USER-ACCESS-DENIED]');
            self::redirect( '/ad/my-listings');
        }
        // Disable browser cache
        self::disableCache();

       // if (\Core\Cache::get(\Core\Utils\Device::getDeviceIp()) !== FALSE) {
//            self::addMessage('[ERROR-AD-POST-WAIT-TIME] ' . ($minutes = (self::getAdPostTimeSpan() / 60)) . ' [LABEL-MINUTE]', 'error');
//            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
//            return;
//        } elseif (\Modules\Humpchies\UserModel::isBehindBannedIp(\Core\Utils\Device::getDeviceIp())) {
//            //self::handlePreviouslyBanned(\Core\Utils\Session::getCurrentUserID());
//        }

        // Set some page values...
        self::setTitle('[LABEL-POST-AD-V2]');                      // Title
        self::setDescription('[LABEL-POST-AD]');                // Description

        // Enable custom CSS support
        // ========================= <link media="all" rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        \Core\Controller::setExtraHead('
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
    '
        );
        \Core\Controller::setExtraHead('
        <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
        <link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile/touch').'/create_ad.css?v=0.08">
        ');
        //$ad = \Modules\Humpchies\AdModel::getDetailsOfSimpleAdNewAD(intval($_GET['id']));
//        var_dump($_GET['id']);
//        var_dump($ad);
//        die();
        self::$data['adid']  = intval($_GET['id']);
        self::$data['ad'] = \Modules\Humpchies\AdModel::getDetailsOfSimpleAdNewAD(intval($_GET['id']));     
    }
    public static function confirmation_payment_fail_ma(){
       
        \Core\Router::getParsedURL()->layout_settings['footer_mode']=\Core\View::HIDDEN_MODE;
        \Core\Router::getParsedURL()->layout_settings['action_mode']=\Core\View::DEVICE_MODE;

        if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect( '/');
        }
        $user_id = \Core\Utils\Session::getCurrentUserID();

        if( !\Modules\Humpchies\UserModel::isUserActiveById( $user_id ) ){
            \Core\Utils\Session::abandon();
            self::addMessage('[SUCCESS-LOGOUT-SUCCESFUL]');
            self::redirect(\Core\Utils\Navigation::urlFor('/'));
        }
        $adID = intval($_GET['id']);
        
        if(!\Modules\Humpchies\AdModel::userOwnsAd($adID, $user_id)){
            self::addMessage('[ERROR-AD-USER-ACCESS-DENIED]');
            self::redirect( '/ad/my-listings');
        }
        // Disable browser cache
        self::disableCache();

       // if (\Core\Cache::get(\Core\Utils\Device::getDeviceIp()) !== FALSE) {
//            self::addMessage('[ERROR-AD-POST-WAIT-TIME] ' . ($minutes = (self::getAdPostTimeSpan() / 60)) . ' [LABEL-MINUTE]', 'error');
//            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
//            return;
//        } elseif (\Modules\Humpchies\UserModel::isBehindBannedIp(\Core\Utils\Device::getDeviceIp())) {
//            //self::handlePreviouslyBanned(\Core\Utils\Session::getCurrentUserID());
//        }

        // Set some page values...
        self::setTitle('[LABEL-POST-AD-V2]');                      // Title
        self::setDescription('[LABEL-POST-AD]');                // Description

        // Enable custom CSS support
        // ========================= <link media="all" rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        \Core\Controller::setExtraHead('
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
    '
        );
        \Core\Controller::setExtraHead('
        <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
        <link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile/touch').'/create_ad.css?v=0.08">
        ');


        // flag the max lengths of the fields that support this attribute...
        self::$data['max_len_title']                = \Modules\Humpchies\AdModel::MAX_LEN_TITLE;
        self::$data['max_len_price']                = strlen((string)\Modules\Humpchies\AdModel::MAX_PRICE);
        self::$data['max_len_price_conditions']     = \Modules\Humpchies\AdModel::MAX_LEN_PRICE_CONDITIONS;
        self::$data['max_len_phone']                = self::MAX_LEN_PHONE_ON_FORM;
        self::$data['adid']                         = intval($_GET['id']);
        self::$data['ad'] = \Modules\Humpchies\AdModel::getDetailsOfSimpleAd(intval($_GET['id']));

        self::$data['available_packages'] = \Modules\Humpchies\PaymentModel::get_available_packages();     
    }
    
    public static function confirmation_bump_success_ma(){
       
        \Core\Router::getParsedURL()->layout_settings['footer_mode']=\Core\View::HIDDEN_MODE;
        \Core\Router::getParsedURL()->layout_settings['action_mode']=\Core\View::DEVICE_MODE;

        if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect( '/');
        }
        $user_id = \Core\Utils\Session::getCurrentUserID();

        if( !\Modules\Humpchies\UserModel::isUserActiveById( $user_id ) ){
            \Core\Utils\Session::abandon();
            self::addMessage('[SUCCESS-LOGOUT-SUCCESFUL]');
            self::redirect(\Core\Utils\Navigation::urlFor('/'));
        }
        $adID = intval($_GET['id']);
        
        if(!\Modules\Humpchies\AdModel::userOwnsAd($adID, $user_id)){
            self::addMessage('[ERROR-AD-USER-ACCESS-DENIED]');
            self::redirect( '/ad/my-listings');
        }
        // Disable browser cache
        self::disableCache();

       // if (\Core\Cache::get(\Core\Utils\Device::getDeviceIp()) !== FALSE) {
//            self::addMessage('[ERROR-AD-POST-WAIT-TIME] ' . ($minutes = (self::getAdPostTimeSpan() / 60)) . ' [LABEL-MINUTE]', 'error');
//            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
//            return;
//        } elseif (\Modules\Humpchies\UserModel::isBehindBannedIp(\Core\Utils\Device::getDeviceIp())) {
//            //self::handlePreviouslyBanned(\Core\Utils\Session::getCurrentUserID());
//        }

        // Set some page values...
        self::setTitle('[LABEL-POST-AD-V2]');                      // Title
        self::setDescription('[LABEL-POST-AD]');                // Description

        // Enable custom CSS support
        // ========================= <link media="all" rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        \Core\Controller::setExtraHead('
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
    '
        );
        \Core\Controller::setExtraHead('
        <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
        <link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile/touch').'/create_ad.css?v=0.08">
        ');


        // flag the max lengths of the fields that support this attribute...
        self::$data['max_len_title']                = \Modules\Humpchies\AdModel::MAX_LEN_TITLE;
        self::$data['max_len_price']                = strlen((string)\Modules\Humpchies\AdModel::MAX_PRICE);
        self::$data['max_len_price_conditions']     = \Modules\Humpchies\AdModel::MAX_LEN_PRICE_CONDITIONS;
        self::$data['max_len_phone']                = self::MAX_LEN_PHONE_ON_FORM;
        self::$data['adid']                         = intval($_GET['id']);
        self::$data['ad'] = \Modules\Humpchies\AdModel::getDetailsOfSimpleAd(intval($_GET['id']));

        self::$data['available_packages'] = \Modules\Humpchies\PaymentModel::get_available_packages();     
    }
    public static function confirmation_bump_fail_ma(){
       
        \Core\Router::getParsedURL()->layout_settings['footer_mode']=\Core\View::HIDDEN_MODE;
        \Core\Router::getParsedURL()->layout_settings['action_mode']=\Core\View::DEVICE_MODE;

        if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect( '/');
        }
        $user_id = \Core\Utils\Session::getCurrentUserID();

        if( !\Modules\Humpchies\UserModel::isUserActiveById( $user_id ) ){
            \Core\Utils\Session::abandon();
            self::addMessage('[SUCCESS-LOGOUT-SUCCESFUL]');
            self::redirect(\Core\Utils\Navigation::urlFor('/'));
        }
        $adID = intval($_GET['id']);
        
        if(!\Modules\Humpchies\AdModel::userOwnsAd($adID, $user_id)){
            self::addMessage('[ERROR-AD-USER-ACCESS-DENIED]');
            self::redirect( '/ad/my-listings');
        }
        // Disable browser cache
        self::disableCache();

       // if (\Core\Cache::get(\Core\Utils\Device::getDeviceIp()) !== FALSE) {
//            self::addMessage('[ERROR-AD-POST-WAIT-TIME] ' . ($minutes = (self::getAdPostTimeSpan() / 60)) . ' [LABEL-MINUTE]', 'error');
//            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
//            return;
//        } elseif (\Modules\Humpchies\UserModel::isBehindBannedIp(\Core\Utils\Device::getDeviceIp())) {
//            //self::handlePreviouslyBanned(\Core\Utils\Session::getCurrentUserID());
//        }

        // Set some page values...
        self::setTitle('[LABEL-POST-AD-V2]');                      // Title
        self::setDescription('[LABEL-POST-AD]');                // Description

        // Enable custom CSS support
        // ========================= <link media="all" rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        \Core\Controller::setExtraHead('
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
    '
        );
        \Core\Controller::setExtraHead('
        <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
        <link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile/touch').'/create_ad.css?v=0.08">
        ');


        // flag the max lengths of the fields that support this attribute...
        self::$data['max_len_title']                = \Modules\Humpchies\AdModel::MAX_LEN_TITLE;
        self::$data['max_len_price']                = strlen((string)\Modules\Humpchies\AdModel::MAX_PRICE);
        self::$data['max_len_price_conditions']     = \Modules\Humpchies\AdModel::MAX_LEN_PRICE_CONDITIONS;
        self::$data['max_len_phone']                = self::MAX_LEN_PHONE_ON_FORM;
        self::$data['adid']                         = intval($_GET['id']);
        self::$data['ad'] = \Modules\Humpchies\AdModel::getDetailsOfSimpleAd(intval($_GET['id']));

        self::$data['available_packages'] = \Modules\Humpchies\PaymentModel::get_available_packages();     
    }
    
        
    public static function verify_ma(){
       
        \Core\Router::getParsedURL()->layout_settings['footer_mode']=\Core\View::HIDDEN_MODE;
        \Core\Router::getParsedURL()->layout_settings['action_mode']=\Core\View::DEVICE_MODE;

        if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect( '/');
        }
        $user_id = \Core\Utils\Session::getCurrentUserID();
        $adID = intval($_GET['id']);

        if( !\Modules\Humpchies\UserModel::isUserActiveById( $user_id ) ){
            \Core\Utils\Session::abandon();
            self::addMessage('[SUCCESS-LOGOUT-SUCCESFUL]');
            self::redirect(\Core\Utils\Navigation::urlFor('/'));
        }
        if(!\Modules\Humpchies\AdModel::userOwnsAd($adID, $user_id)){
            self::addMessage('[ERROR-AD-USER-ACCESS-DENIED]');
            self::redirect( '/ad/my-listings');
        }
        if( \Core\Utils\Security::isIdInputValid($_GET['id']) !== \Core\Utils\Security::NO_ERROR
            ||
            ($ad_record = \Modules\Humpchies\AdModel::getAdDetailsForEdition($_GET['id'], $user_id)) === FALSE)
        {
            self::addMessage('[ERROR-AD-ID-IS-INVALID]', 'error');
            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
            return;
        }
        
        $lastStatus = \Modules\Humpchies\AdModel::getVerificationLastStatus($_GET['id']);
        if(!empty($lastStatus) && $lastStatus['verification_status'] != 'rejected' ){
            self::addMessage('[ERROR-AD-VERIFICATION-PENDING]', 'error');
            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
            return;
            
        }
        $ad_record = array_shift($ad_record);
        
        // Disable browser cache
        self::disableCache();
        
        // Set some page values...
        self::setTitle('[LABEL-POST-AD-V2]');                      // Title
        self::setDescription('[LABEL-POST-AD]');                // Description
        self::setHeaderClass("has_breadcrumb");
        
        // Enable custom CSS support
        // ========================= <link media="all" rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        \Core\Controller::setExtraHead('
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery-validation-1.19.0/dist/jquery.validate.min.js"></script>
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile/touch')).'/create_ad.js?v=0.24"></script>
         <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'pc/lant_uploader.js?v=0.16"></script>
        '
        );
        \Core\Controller::setExtraHead('
        <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
        <link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile/touch').'/create_ad.css?v=0.09">
         <link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') .'juploader.css?v=0.09">
        ');


        // flag the max lengths of the fields that support this attribute...
        self::$data['max_len_title']                = \Modules\Humpchies\AdModel::MAX_LEN_TITLE;
        self::$data['max_len_price']                = strlen((string)\Modules\Humpchies\AdModel::MAX_PRICE);
        self::$data['max_len_price_conditions']     = \Modules\Humpchies\AdModel::MAX_LEN_PRICE_CONDITIONS;
        self::$data['max_len_phone']                = self::MAX_LEN_PHONE_ON_FORM;
        self::$data['adid']                         = $adID;
        self::$data['images'] = AdImageModel::getImagesWeb($ad_record['id']);
        self::$data['title'] = $ad_record['title'];
        
        if(empty(self::$data['images']) ){
            self::addMessage('[ERROR-AD-VERIFICATION-NOPIC]', 'error');
            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
            return;
            
        }
        
        if(!$_POST){
            //ho "NOPOST";
            return;
        }
        // save verification Information 
        $varsa = array();
        $varsa['request_date'] = date("Y-m-d H:i:s"); 
        $varsa['verification_status'] = "requested"; 
        $varsa['ad_id'] = $adID; 
        $varsa['user_id'] = $user_id; 
        if($_POST['existsAd'] == 'yes'){
            $varsa['old_id'] = $_POST['old_ad'];
        }else{
            $images = explode(",", $_POST['uploadedFiles']);
            $postDir =  \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_safe') . "temp/";
            $safeDir =  \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_safe') . "safe/";
             
            $savedImages = array();
            foreach($images as $image){
                $Iimage =  preg_replace("`[^a-z0-9_.]`is", "", $image);
                //echo $postDir . $Iimage."<br>"; 
                if( file_exists($postDir . $Iimage) &&  is_file($postDir . $Iimage) && copy($postDir . $Iimage, $safeDir . $Iimage)){
                    $savedImages[] = $Iimage;
                    unlink($postDir . $Iimage);   
                }
            }
            $varsa['images'] = implode(",", $savedImages);
        }
        \Modules\Humpchies\AdModel::adVerificationCreate($varsa);
        $editPOST['verification_status'] = 'pending';   
                     
        $edit_add_result = \Modules\Humpchies\AdModel::edit($adID, $user_id, $editPOST);
        
        self::addMessage('[AD-VERIFY-SEND-CONFIRMATION]');
        self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
    }
    
    public static function uploadSafeDocuments(){
         if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect( '/');
        }
        $user_id = \Core\Utils\Session::getCurrentUserID();

        if( !\Modules\Humpchies\UserModel::isUserActiveById( $user_id ) ){
            \Core\Utils\Session::abandon();
            self::addMessage('[SUCCESS-LOGOUT-SUCCESFUL]');
            self::redirect(\Core\Utils\Navigation::urlFor('/'));
        }
        // Disable browser cache
        self::disableCache();
        $images = $_FILES['private'];
        $postDir =  \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::ads_safe') . "temp/";
        //$file=['filename'=> "file_" . rand(1000,2000) . "jpg", 'status' => 'success', 'adid' => $_POST['adid'] ]; 
        $uploadedFiles = array();

        foreach($images['tmp_name'] as $key => $image){
            $file = explode(".", $images['name'][$key]);
            $ext = array_pop($file);
               
            $new_name = $_POST['adid'] . "_" . md5(implode(".",$file).rand(1234, 100000));
            $new_name =  preg_replace("`[^a-z0-9_.]`is", "", $new_name);  
            $jpgName = $new_name . ".jpg";
            $new_name = $new_name  . ".". $ext;
            
            move_uploaded_file($image, $postDir . $new_name);
            self::imgResize($postDir . $new_name, 800, 600);
            $uploadedFiles[] = ['filename' => $jpgName ];
        }
        echo json_encode($uploadedFiles);
        die();
    }
    
    public static function checkold(){
        if(!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect( '/');
        }
        
        $user_id = \Core\Utils\Session::getCurrentUserID();

        if( !\Modules\Humpchies\UserModel::isUserActiveById( $user_id ) ){
            \Core\Utils\Session::abandon();
            self::addMessage('[SUCCESS-LOGOUT-SUCCESFUL]');
            self::redirect(\Core\Utils\Navigation::urlFor('/'));
        }

        $oldAd = intval($_POST['old']);
        // Disable browser cache
        self::disableCache();
        
        $result = \Modules\Humpchies\AdModel::getAdStatus($oldAd);
        
        if($result){
            //var_dump($result);
            //die();
            if($result["user_id"] != $user_id){
                $error = 'The ID you have added is not linked to your account';
            }elseif($result["verification_status"] != 'verified'){
                $error = 'This ad is not verified. Please enter another';
            }else{
                $error = '';
            }           
        }else{
            $error = "There is no ad with this ID.";
        }
        
        $varsa = array();
        
        if($error){
            $varsa['status'] = 'error';   
            $varsa['error']  = $error;   
        }else{
            $varsa['status'] = 'success';    
        }
                
        echo json_encode($varsa);
        die();
    }
    
    public static function editMaV2(){
       
        \Core\Router::getParsedURL()->layout_settings['footer_mode']=\Core\View::HIDDEN_MODE;
        \Core\Router::getParsedURL()->layout_settings['action_mode']=\Core\View::DEVICE_MODE;
        
        if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect('/');
        }
        // Disable browser cache

        self::disableCache();

        // Set some page values...
        self::setTitle('[LABEL-EDIT-AD]');                      // Title
        self::setDescription('[LABEL-EDIT-AD]');                // Description
        self::setHeaderClass("has_breadcrumb");
        // Enable custom CSS support
        // =========================
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="'
            .
            \Modules\Humpchies\Utils::getCDNPath('css')
            .
            (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch'))
            .
            '/post.css?v=5">');

        \Core\Controller::setExtraHead('
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery-validation-1.19.0/dist/jquery.validate.min.js"></script>
        <script type="text/javascript" src="'.\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') .'/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'intl-tel-input/js/intlTelInput.min.js"  charset="utf-8"></script>
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'intl-tel-input/js/index.js?v=3.1"  charset="utf-8"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.2/jquery.ui.touch-punch.min.js"></script>'.
        '<script type="text/javascript" src="'.\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js')
            .(\Core\Utils\Device::isPC() ? 'pc' : ('/mobile' . '/touch')).'/edit_ad_new.js?v=1.04"></script>
        ');
        \Core\Controller::setExtraHead('
            <link media="all" rel="stylesheet" type="text/css" href="'. \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch')) . '/edit_ads.css?v=4">
            <link media="all" rel="stylesheet" type="text/css" href="' . \Modules\Humpchies\Utils::getCDNPath('js').'intl-tel-input/css/intlTelInput.min.css" charset="utf-8">
                                        ');
        \Core\Controller::setExtraHead('
            <link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile/touch').'/create_ad_new.css?v=1.04">
        ');
        
        if(\Core\Utils\Session::getLocale() == 'fr-ca'){
            \Core\Controller::setExtraHead('<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/localization/messages_fr.js"></script>');
        }
        
        // MAPBOX
        \Core\Controller::setExtraHead('<script src="https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.js"></script>');    
        \Core\Controller::setExtraHead('<link href="https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.css" rel="stylesheet" />');    
        \Core\Controller::setExtraHead('<script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'setup_mapbox.js?v=1.01"></script>'); 
        
         \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . '/modal.css?v=3.16">');
        
        $user_id = \Core\Utils\Session::getCurrentUserID();

        if( \Core\Utils\Security::isIdInputValid($_GET['id']) !== \Core\Utils\Security::NO_ERROR
            ||
            ($ad_record = \Modules\Humpchies\AdModel::getAdDetailsForEdition($_GET['id'], $user_id)) === FALSE)
        {
            self::addMessage('[ERROR-AD-ID-IS-INVALID]', 'error');
            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
            return;
        }
      
        // get map information
        self::$data["map_coords"] = \Modules\Humpchies\AdLocationModel::getLocationInfo($_GET['id']);
      
        // get tags
        //if(\Core\Utils\Session::getCurrentUserID() == 63661){ // only testme
            $lang =  (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';
            self::$data["tags"] = \Modules\Humpchies\TagModel::getTagsList($lang);
            self::$data["selected_tags"] = \Modules\Humpchies\AdTagModel::getAdTags($_GET['id'], $lang);
        //}
        
        $isbumped = ( $ad_record[0]['date_bumped'] && $ad_record[0]['date_bumped'] > time()- 24 * 60 * 60)? true : false;
        self::$data[0]['bumped'] = $isbumped;
        unset($ad_record[0]['date_bumped']);
        $ispremium = $ad_record[0]['order_package_id'];
        
        self::$data['ispremium'] = $ispremium;
        
        $ad_record = array_shift($ad_record);
        $monitorAdInitial = [];
        $monitorAdInitial['location']           = $ad_record['location'];  
        $monitorAdInitial['price']              = $ad_record['price'];  
        $monitorAdInitial['price_conditions']   = $ad_record['price_conditions'];  
        $monitorAdInitial['title']              = $ad_record['title'];  
        $monitorAdInitial['description']        = $ad_record['description'];  
        $monitorAdInitial['phone']              = $ad_record['phone']; 
        $monitorAdInitial['myfans_url']         = $ad_record['myfans_url']; 
        $monitorAdInitial['category']           = $ad_record['category'];  

        $ad_is_updated = \Modules\Humpchies\AdModel::getAdIsUpdatedStatus($_GET['id'], $user_id);
        $original_ad = $ad_record;
        
        if($ad_is_updated['is_updated']) {
            $ad_record_revision = \Modules\Humpchies\AdModel::getRevisionData($_GET['id'], $user_id);

            if($ad_record_revision) {
                unset($ad_record_revision['id']);
                unset($ad_record_revision['ads_id']);
                unset($ad_record_revision['date_added']);
                unset($ad_record_revision['status']);
                unset($ad_record_revision['user_id']);
                unset($ad_record_revision['approved_status']);

                $ad_record = array_merge($ad_record, $ad_record_revision);
            }
        }

        $should_update = TRUE;

        if(!$_POST) {
            $_POST= $ad_record;
            $should_update = FALSE;
        }
        $added_images = 0;
        if(isset($_POST['pic_changed'])){
            $added_images = intval($_POST['pic_changed']);
            unset($_POST['pic_changed']);
        }
        $city_slug = $_POST['city']; // save the selected city slug
        self::pushPlugIn('category_options', new \Plugins\HumpchiesCategoryOptions(\Plugins\HumpchiesCategoryOptions::DEFAULT_SELECT_NAME, $_POST[\Plugins\HumpchiesCategoryOptions::DEFAULT_SELECT_NAME]));
        self::pushPlugIn('location_options', new \Plugins\HumpchiesLocationOptions(\Plugins\HumpchiesLocationOptions::DEFAULT_SELECT_NAME, $_POST[\Plugins\HumpchiesLocationOptions::DEFAULT_SELECT_NAME]));

        $_POST[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME] = \Modules\Humpchies\CityModel::getCitiesSlugNames()[$_POST[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME]];
        
        // flag the max lengths of the fields that support this attribute...
        self::$data['max_len_title']                = \Modules\Humpchies\AdModel::MAX_LEN_TITLE;
        self::$data['max_len_price']                = strlen((string)\Modules\Humpchies\AdModel::MAX_PRICE);
        self::$data['max_len_price_conditions']     = \Modules\Humpchies\AdModel::MAX_LEN_PRICE_CONDITIONS;
        self::$data['max_len_phone']                = self::MAX_LEN_PHONE_ON_FORM;
        self::$data['images'] = AdImageModel::getImagesWeb($ad_record['id']);
        self::$data['ad_id'] = $ad_record['id'];
        self::$data['max_image_count'] = 5;
        self::$data['user_videos'] = \Modules\Humpchies\AdModel::getUserVideo(\Core\Utils\Session::getCurrentUserID());
        self::$data['selected_video'] = $ad_record['attached_video'];
        self::$data['image_count'] = (self::$data['images'])?count(self::$data['images']):0;
        self::$data['videolocation']                = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::videos')."temp/";
        self::$data['imageslocation']                = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_images');
        
        if($should_update === FALSE) {
            return;
        }

        $attached_video = $_POST['attached_video'];
        unset($_POST['attached_video']);
        $originalPostData = $_POST;
        
        if($_POST['addprice']) {
            // remove old price
            $_POST['title'] = str_replace(" \$" . $ad_record['price_min'], "", $_POST['title']);
            $_POST['title'] = str_replace(" \$" . $ad_record['price_max'], "", $_POST['title']);
            $_POST['title'] = str_replace(" \$" . $ad_record['price'], "", $_POST['title']);
           // var_dump($_POST['title']);exit();
            switch(true) {
                case isset($_POST['price_min']) && $_POST['price_min'] > 0 :
                    $_POST['title'] .= " \$" . $_POST['price_min'];
                    break;
                case isset($_POST['price_max']) && $_POST['price_max'] > 0 :
                    $_POST['title'] .= " \$" . $_POST['price_max'];
                    break;
                case isset($_POST['price']) && $_POST['price'] > 0 :
                    $_POST['title'] .= " \$" . $_POST['price'];
                    break;
                default: 
                    break;
            }
            
            //$_POST['title'] .= " \$" . $_POST['price'];
        }
        
        unset($_POST['addprice']);

$rotator = $_POST['rotator'];
        unset($_POST["rotator"]);
        $rotator_images = $_POST['imageids'];
        unset($_POST["imageids"]);
        \Modules\Humpchies\AdImageModel::rotateImages($rotator_images,$rotator, intval($_GET['id']));
        $edit_ad_validity = self::validateEdit($user_id, $_POST, $_GET['id']);

        // Validate the data entered for the new ad...
        switch($edit_ad_validity) {
            // We are GOLDEN! no errors...move on to update ad in the DB
            case self::NO_ERROR:
                break;

            // There is no title for this ad...
            case self::ERROR_TITLE_IS_EMPTY:
                self::addMessage('[ERROR-TITLE-IS-EMPTY]', 'error');
                return;
                break;

            // The title is too long...
            case self::ERROR_TITLE_IS_TOO_LONG:
                self::addMessage('[ERROR-TITLE-IS-TOO-LONG] ' . \Modules\Humpchies\AdModel::MAX_LEN_TITLE . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The title is too short...
            case self::ERROR_TITLE_IS_TOO_SHORT:
                self::addMessage('[ERROR-TITLE-IS-TOO-SHORT] ' . \Modules\Humpchies\AdModel::MIN_LEN_TITLE . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The description is empty
            case self::ERROR_DESCRIPTION_IS_EMPTY:
                self::addMessage('[ERROR-DESCRIPTION-IS-EMPTY]', 'error');
                return;
                break;

             case self::ERROR_DESCRIPTION_IS_DUPLICATED:
                self::addMessage('[ERROR-DESCRIPTION-IS-DUPLICATED]', 'error');
                return;
                break;

            // The description is too long...
            case self::ERROR_DESCRIPTION_IS_TOO_LONG:
                self::addMessage('[ERROR-DESCRIPTION-IS-TOO-LONG] ' . \Modules\Humpchies\AdModel::MAX_LEN_DESCRIPTION . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The description is too short
            case self::ERROR_DESCRIPTION_IS_TOO_SHORT:
                self::addMessage('[ERROR-DESCRIPTION-IS-TOO-SHORT] ' . \Modules\Humpchies\AdModel::MIN_LEN_DESCRIPTION . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The price is invalid
            case self::ERROR_PRICE_IS_INVALID: //. \Modules\Humpchies\AdModel::MAX_PRICE
                self::addMessage('[ERROR-PRICE-IS-INVALID] ' , 'error');
                return;
                break;

            // Price conditions are too long...
            case self::ERROR_PRICE_CONDITIONS_IS_TOO_LONG:
                self::addMessage('[ERROR-PRICE-CONDITIONS-IS-TOO-LONG] ' . \Modules\Humpchies\AdModel::MAX_LEN_PRICE_CONDITIONS . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The phone number is empty
            case self::ERROR_PHONE_IS_EMPTY:
                self::addMessage('[ERROR-PHONE-IS-EMPTY]', 'error');
                return;
                break;

            // The phone number is invalid...
            case self::ERROR_PHONE_IS_INVALID:
                self::addMessage('[ERROR-PHONE-IS-INVALID]', 'error');
                return;
                break;

            // The phone number not from CA...
            case self::ERROR_NOT_SUPPORTED_COUNTRY:
                self::addMessage('[ERROR-PHONE-NOT-SUPPORTED-COUNTRY]', 'error');
                return;
                break;

            /**
             * Hum!... something shady is going on...the locations options
             * are provided via a select. Yet, the user has managed to fuck it
             * up... I wonder how... nevermind that ... kick the user out of here.
             */
            case self::ERROR_LOCATION_IS_INVALID:
                self::addMessage('[ERROR-LOCATION-IS-INVALID]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                return FALSE;
                break;

            /**
             * Hum!... yet again...something shady is going on...the user id is
             * retrieved from the session. Somwhow the user has managed to fuck
             * it up. This indicates that something shady is going...kick
             * the user out of here!
             */
            case self::ERROR_USER_ID_IS_INVALID:
                \Core\Utils\Session::abandon();
                self::addMessage('[ERROR-USER-ID-IS-INVALID]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                break;

            // The user is banned...
            case self::ERROR_USER_IS_NOT_ALLOWED:
                \Core\Utils\Session::abandon();
                self::addMessage('[ERROR-USER-IS-NOT-ALLOWED]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                break;

            // Unhandled error...
            default:
                self::addMessage("[ERROR-UNHANDLED]: $edit_ad_validity", 'error');
                return;
        }
        
        if(! is_null($ad_record['package_status']) && $ad_record['city'] != $_POST['city']){
            $city_change_status = \Modules\Humpchies\AdModel::changePremiumcity($ad_record['order_package_id'], $_POST['city']);
            if($city_change_status == \Modules\Humpchies\AdModel::ERROR_UNKNOWN){
                return self::addMessage("[ERROR-UNHANDLED] ", 'error');
            }
        }
        
        if( isset($_POST['phone']) && !empty($_POST['phone']) ){
            if( \Modules\Humpchies\AdModel::checkIfPhoneBlacklisted( $_POST['phone'] ) ){
                $_POST['status'] = 'banned';
            }
        } else {
            // VALIDATE PHONE WHEN DONE 
            if($_POST['validate_phone']) {
                $phone = (preg_match("`(^|\\+)" . $_POST['phone_country_code'] . "`is", $_POST['phone']))? $_POST['phone'] : $_POST['phone_country_code'] . $_POST['phone'];
                $phoneValidation = \Core\library\checkMobi::validationCall($phone);
                $_POST['validation_id'] = $phoneValidation['id'];
                $_POST['dialing_number'] = $phoneValidation['dialing_number'];          
                unset($_POST['validate_phone']);
            }
        }
        
        $user_auto_approval = \Modules\Humpchies\UserModel::getUserAutoApprovalById( $user_id );
        
        $monitorAdFinal = [];
        $monitorAdFinal['location']         = $_POST['location'];  
        $monitorAdFinal['price']            = $_POST['price'];  
        $monitorAdFinal['price_conditions'] = $_POST['price_conditions'];  
        $monitorAdFinal['title']            = $_POST['title'];  
        $monitorAdFinal['description']      = $_POST['description']; 
        $monitorAdFinal['phone']            = $_POST['phone'];  
        $monitorAdFinal['myfans_url']       = $_POST['myfans_url'];
        $monitorAdFinal['category']         = $_POST['category'];
        if($monitorAdFinal['phone'] != $monitorAdInitial['phone'] ){
             $_POST['phone_validated'] = "N";
        }
        
        $changes = array_diff_assoc($monitorAdFinal, $monitorAdInitial); 
        
         if( isset($_POST['phone']) && !empty($_POST['phone']) ){
            if( \Modules\Humpchies\AdModel::checkIfPhoneBlacklisted( $_POST['phone'] ) ){
                //$_POST['status'] = 'banned';
                \Modules\Humpchies\AdModel::edit($ad_record['id'], $user_id, ["status" => 'banned', 'comment' => 'attemp change blacklisted phone number: '. $_POST['phone'] ]);
                self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings', null, true, true));
            }
        }
        
        //if( (count($changes) || $ad_is_updated['is_updated'] || $added_images)  && !$user_auto_approval['auto_approval'] && !$isbumped && !$ispremium){
        if( (count($changes) || $ad_is_updated['is_updated'] || $added_images)  && !$user_auto_approval['auto_approval']){
 
            $_POST['revision_ip']= ip2long(self::userIP());
            $geo = \Core\library\Geo::getLocation(self::userIp());
            $_POST['rev_country']       = $geo['country'];
            $_POST['rev_country_id']    = \Core\library\Geo::getCountryID($geo['country']);
            $_POST['rev_location']      = $geo['city']. ", " . $geo['zone'] . ", ". $geo['countryName'] ;
        
            $city_details =  \Modules\Humpchies\CityModel::getCityDetails($city_slug);
            if(!empty($city_details)) {
                
                $_POST['city_id'] = $city_details['id'];
                $_POST['region_id'] = $city_details['region_id'];
                $originalPostData['city_id'] = $city_details['id'];
            }
//            var_dump($_POST);exit();
            $_POST['vpn'] = (\Core\library\checkVpn::isVpn(\Core\Utils\Device::getDeviceIp_new()))? "Y" : "N";
            $edit_add_result = \Modules\Humpchies\AdModel::editRevision($ad_record['id'], $user_id, $_POST, $ad_record['ad_status'], $original_ad);
            unset($_POST['vpn']);
            $is_revision = true;
        } else {
            
            $city_details =  \Modules\Humpchies\CityModel::getCityDetails($city_slug);
            if(!empty($city_details)) {
                
                $_POST['city_id'] = $city_details['id'];
                $_POST['region_id'] = $city_details['region_id'];
                $originalPostData['city_id'] = $city_details['id'];
            }

            $_POST['date_released'] = time();
          
            // make sure location is never empty
            if(!in_array($_POST['location'], ['incalls','outcalls','both','unknown'])) {
                $_POST['location'] =  'incalls';
            }
            
            $edit_add_result = \Modules\Humpchies\AdModel::edit($ad_record['id'], $user_id, $_POST);
            unset($_POST['date_released']);
            $is_revision = false;
            if($ispremium){
                \Modules\Humpchies\AdModel::changePackCity($ad_record['id'], $user_id, $_POST['city']);
            }
        }
        
        $attached_video = \Modules\Humpchies\AdModel::getUserVideobyID(\Core\Utils\Session::getCurrentUserID(), $attached_video);
        if(!empty($attached_video[0]['video_name'])){
            $attached_video = $attached_video[0]['video_name'];
            \Modules\Humpchies\AdModel::edit($ad_record['id'], $user_id, ["attached_video" => $attached_video]);
        }

        if($edit_add_result !== TRUE) {
            switch($edit_add_result) {
                case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_DUPLICATED:
                    self::addMessage('[ERROR-DESCRIPTION-IS-DUPLICATED]', 'error');
                    return;
                    break;
                default:
                    return self::addMessage("[ERROR-UNHANDLED] ", 'error');
            }
        }

        // update location if needed
        if(!self::$data["map_coords"]) {
            
            // ----------------- SAVE LOCATION ---------------------- //
            $hide_map_check =  isset($originalPostData['hide_map_check']) && $originalPostData['hide_map_check'] == 'yes'? 1 : 0;
            $hide_map       =  isset($originalPostData['hide_map']) && $originalPostData['hide_map'] == 'yes'? 1 : 0;
            
            $location_fields = [
                '`ad_id`'             => $ad_record['id'],
                '`user_id`'           => \Core\Utils\Session::getCurrentUserID(),
                '`ad_city_id`'        => $originalPostData['city_id'],
                '`ad_city_name`'      => $originalPostData['city'],
                '`lat`'               => isset($originalPostData['maps_lat'])? floatval($originalPostData['maps_lat']) : '',
                '`long`'              => isset($originalPostData['maps_long'])? floatval($originalPostData['maps_long']) : '',
                '`current_city_name`' => isset($originalPostData['maps_city'])? $originalPostData['maps_city'] : '',
                '`hide_map`'          => ($hide_map_check || $hide_map)? 1 : 0
            ];
            if(isset($originalPostData['maps_long']) && floatval($originalPostData['maps_lat']) != 0 && isset($originalPostData['maps_lat'])) {
                $ad_location_result = \Modules\Humpchies\AdLocationModel::createV2($location_fields);
            }
        
        }  else {
        
            if(isset($originalPostData['maps_long']) && floatval($originalPostData['maps_lat']) != 0 && isset($originalPostData['maps_lat'])) {
                
                $hide_map_check =  isset($originalPostData['hide_map_check']) && $originalPostData['hide_map_check'] == 'yes'? 1 : 0;
                $hide_map       =  isset($originalPostData['hide_map']) && $originalPostData['hide_map'] == 'yes'? 1 : 0;
                $originalPostData['hide_map'] =  ($hide_map_check || $hide_map)? 'yes' : 'no';

                $updated_map = \Modules\Humpchies\AdLocationModel::editV2($ad_record['id'], $user_id, self::$data["map_coords"], $originalPostData); 
            }   
        }
        if( isset($_POST['phone']) && !empty($_POST['phone']) ){
            if( \Modules\Humpchies\AdModel::checkIfPhoneBlacklisted( $_POST['phone'] ) ){
                //$_POST['status'] = 'banned';
                \Modules\Humpchies\AdModel::edit($ad_record['id'], $user_id, ["status" => 'banned', 'comment' => 'attemp change blacklisted phone number: '. $_POST['phone']]);
            }
        }
        
         // save TAGS
        //if(\Core\Utils\Session::getCurrentUserID() == 63661){ // only testme
            \Modules\Humpchies\AdTagModel::edit($ad_record['id'], $originalPostData['tags']);
        //}
        
        if(($new_ad_record = \Modules\Humpchies\AdModel::getAdDetailsForEdition($ad_record['id'], $user_id)) === FALSE) {
            self::addMessage('[ERROR-AD-ID-IS-INVALID]', 'error');
            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings', null, true, true));
            return;
        }

        $new_ad_record = array_shift($new_ad_record);

        \Modules\Humpchies\UserModel::trackIp(\Core\Utils\Session::getCurrentUserID(), \Core\Utils\Device::getDeviceIp());
        \Modules\Humpchies\UserModel::trackPhone(\Core\Utils\Session::getCurrentUserID(), $new_ad_record['phone']);
       
       if($phoneValidation['dialing_number']){
             self::redirect(\Core\Utils\Navigation::urlFor('ad', 'phone-validation', ["ad" => $ad_record['id'] ], true, true));
        } else {
            if($is_revision) { 
            self::addMessage('[SUCCESS-EDIT-REVISION-ADDED-SUCCESFUL]', (self::getMessages() === FALSE ? 'success' : 'warning'));
            } else {
             self::addMessage('[SUCCESS-AD-EDITION-SUCCESFUL]', (self::getMessages() === FALSE ? 'success' : 'warning'));     
        }

        self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings',null, true, true));
    }
    }

    public static function editMaV3(){
       
        \Core\Router::getParsedURL()->layout_settings['footer_mode']=\Core\View::HIDDEN_MODE;
        \Core\Router::getParsedURL()->layout_settings['action_mode']=\Core\View::DEVICE_MODE;
        
        if (!\Core\Utils\Session::currentUserIsAdvertisment()){
            self::redirect('/');
        }
        // Disable browser cache

        self::disableCache();

        // Set some page values...
        self::setTitle('[LABEL-EDIT-AD]');                      // Title
        self::setDescription('[LABEL-EDIT-AD]');                // Description
        self::setHeaderClass("has_breadcrumb");
        // Enable custom CSS support
        // =========================
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="'
            .
            \Modules\Humpchies\Utils::getCDNPath('css')
            .
            (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch'))
            .
            '/post.css?v=5">');

        \Core\Controller::setExtraHead('
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery-validation-1.19.0/dist/jquery.validate.min.js"></script>
        <script type="text/javascript" src="'.\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') .'/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'intl-tel-input/js/intlTelInput.min.js"  charset="utf-8"></script>
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'intl-tel-input/js/index.js?v=3.1"  charset="utf-8"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.2/jquery.ui.touch-punch.min.js"></script>'.
        /*
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'intl-tel-input/js/intlTelInput.min.js"></script>
        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'intl-tel-input/js/index.js?v=3.1"></script>
        */
        '<script type="text/javascript" src="'.\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js')
            .(\Core\Utils\Device::isPC() ? 'pc' : ('/mobile' . '/touch')).'/edit_ad_new.js?v=1.02"></script>
        ');
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="'
            .
            \Core\Utils\Device::getProtocol() .
            \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css')
            .
            (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch'))
            .
            '/edit_ads.css?v=4">
            <link media="all" rel="stylesheet" type="text/css" href="'
            .
            \Modules\Humpchies\Utils::getCDNPath('js').'intl-tel-input/css/intlTelInput.min.css" charset="utf-8">
                                        ');
        \Core\Controller::setExtraHead('        <link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . (\Core\Utils\Device::isPC() ? 'pc' : 'mobile/touch').'/create_ad_new.css?v=1.01">
        ');
        
        if(\Core\Utils\Session::getLocale() == 'fr-ca'){
            \Core\Controller::setExtraHead('<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/localization/messages_fr.js"></script>');
        }
        
        //if(\Core\Utils\Session::getCurrentUserID() == 63661){ // only testme
            \Core\Controller::setExtraHead('<script type="text/javascript" src="https://api-maps.yandex.ru/2.1/?lang=fr_CA&amp;apikey=a2322142-06fa-4970-a82e-2543c4763bcb"></script>');    
            \Core\Controller::setExtraHead('<script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'setup_yandex_new.js?v=1.00"></script>'); 
        //}
         \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . '/modal.css?v=3.16">');
        
        $user_id = \Core\Utils\Session::getCurrentUserID();

        if( \Core\Utils\Security::isIdInputValid($_GET['id']) !== \Core\Utils\Security::NO_ERROR
            ||
            ($ad_record = \Modules\Humpchies\AdModel::getAdDetailsForEdition($_GET['id'], $user_id)) === FALSE)
        {
            self::addMessage('[ERROR-AD-ID-IS-INVALID]', 'error');
            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
            return;
        }
      
        // get map information
        self::$data["map_coords"] = \Modules\Humpchies\AdLocationModel::getLocationInfo($_GET['id']);
        //var_dump(self::$data["map_coords"]);
      
        // get tags
        //if(\Core\Utils\Session::getCurrentUserID() == 63661){ // only testme
            $lang =  (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';
            self::$data["tags"] = \Modules\Humpchies\TagModel::getTagsList($lang);
            self::$data["selected_tags"] = \Modules\Humpchies\AdTagModel::getAdTags($_GET['id'], $lang);
            //var_dump(self::$data["selected_tags"]);
            //var_dump(array_column(self::$data["selected_tags"], 'tag_id'));
        //}
        $isbumped = ( $ad_record[0]['date_bumped'] && $ad_record[0]['date_bumped'] > time()- 24 * 60 * 60)? true : false;
        self::$data[0]['bumped'] = $isbumped;
        unset($ad_record[0]['date_bumped']);
        $ispremium = $ad_record[0]['order_package_id'];
        
        self::$data['ispremium'] = $ispremium;
        
        $ad_record = array_shift($ad_record);
        $monitorAdInitial = [];
        $monitorAdInitial['location']           = $ad_record['location'];  
        $monitorAdInitial['price']              = $ad_record['price'];  
        $monitorAdInitial['price_conditions']   = $ad_record['price_conditions'];  
        $monitorAdInitial['title']              = $ad_record['title'];  
        $monitorAdInitial['description']        = $ad_record['description'];  
        $monitorAdInitial['phone']              = $ad_record['phone'];  
        $monitorAdInitial['myfans_url']         = $ad_record['myfans_url']; 
        $monitorAdInitial['category']           = $ad_record['category'];  

        $ad_is_updated = \Modules\Humpchies\AdModel::getAdIsUpdatedStatus($_GET['id'], $user_id);
        $original_ad = $ad_record;
        
        if($ad_is_updated['is_updated']) {
            $ad_record_revision = \Modules\Humpchies\AdModel::getRevisionData($_GET['id'], $user_id);

            if($ad_record_revision) {
                unset($ad_record_revision['id']);
                unset($ad_record_revision['ads_id']);
                unset($ad_record_revision['date_added']);
                unset($ad_record_revision['status']);
                unset($ad_record_revision['user_id']);
                unset($ad_record_revision['approved_status']);

                $ad_record = array_merge($ad_record, $ad_record_revision);
            }
        }

        $should_update = TRUE;

        if(!$_POST) {
            $_POST= $ad_record;
            $should_update = FALSE;
        }
        $added_images = 0;
        if(isset($_POST['pic_changed'])){
            $added_images = intval($_POST['pic_changed']);
            unset($_POST['pic_changed']);
        }
        $city_slug = $_POST['city']; // save the selected city slug
        //self::pushPlugIn('city_options', new \Plugins\HumpchiesCityOptions(\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME, $_POST[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME], true));
        self::pushPlugIn('category_options', new \Plugins\HumpchiesCategoryOptions(\Plugins\HumpchiesCategoryOptions::DEFAULT_SELECT_NAME, $_POST[\Plugins\HumpchiesCategoryOptions::DEFAULT_SELECT_NAME]));
        self::pushPlugIn('location_options', new \Plugins\HumpchiesLocationOptions(\Plugins\HumpchiesLocationOptions::DEFAULT_SELECT_NAME, $_POST[\Plugins\HumpchiesLocationOptions::DEFAULT_SELECT_NAME]));

        $_POST[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME] = \Modules\Humpchies\CityModel::getCitiesSlugNames()[$_POST[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME]];
        
        // flag the max lengths of the fields that support this attribute...
        self::$data['max_len_title']                = \Modules\Humpchies\AdModel::MAX_LEN_TITLE;
        self::$data['max_len_price']                = strlen((string)\Modules\Humpchies\AdModel::MAX_PRICE);
        self::$data['max_len_price_conditions']     = \Modules\Humpchies\AdModel::MAX_LEN_PRICE_CONDITIONS;
        self::$data['max_len_phone']                = self::MAX_LEN_PHONE_ON_FORM;
        self::$data['images'] = AdImageModel::getImagesWeb($ad_record['id']);
        self::$data['ad_id'] = $ad_record['id'];
        self::$data['max_image_count'] = 5;
        self::$data['user_videos'] = \Modules\Humpchies\AdModel::getUserVideo(\Core\Utils\Session::getCurrentUserID());
        self::$data['selected_video'] = $ad_record['attached_video'];
        self::$data['image_count'] = (self::$data['images'])?count(self::$data['images']):0;
        self::$data['videolocation']                = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::videos')."temp/";
        self::$data['imageslocation']                = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_images');
        if($should_update === FALSE)
        {
            return;
        }

        if(\Modules\Humpchies\UserModel::isBehindBannedIp(\Core\Utils\Device::getDeviceIp()))
        {
            //self::handlePreviouslyBanned(\Core\Utils\Session::getCurrentUserID());
        }
        $attached_video = $_POST['attached_video'];
        unset($_POST['attached_video']);
        
        $rotator = $_POST['rotator'];
        unset($_POST["rotator"]);
        $rotator_images = $_POST['imageids'];
        unset($_POST["imageids"]);
        \Modules\Humpchies\AdImageModel::rotateImages($rotator_images,$rotator, intval($_GET['id']));
        
        $originalPostData = $_POST;
        
        $edit_ad_validity = self::validateEdit($user_id, $_POST, $_GET['id']);

        // Validate the data entered for the new ad...
        switch($edit_ad_validity)
        {
            // We are GOLDEN! no errors...move on to update ad in the DB
            case self::NO_ERROR:
                break;

            // There is no title for this ad...
            case self::ERROR_TITLE_IS_EMPTY:
                self::addMessage('[ERROR-TITLE-IS-EMPTY]', 'error');
                return;
                break;

            // The title is too long...
            case self::ERROR_TITLE_IS_TOO_LONG:
                self::addMessage('[ERROR-TITLE-IS-TOO-LONG] ' . \Modules\Humpchies\AdModel::MAX_LEN_TITLE . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The title is too short...
            case self::ERROR_TITLE_IS_TOO_SHORT:
                self::addMessage('[ERROR-TITLE-IS-TOO-SHORT] ' . \Modules\Humpchies\AdModel::MIN_LEN_TITLE . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The description is empty
            case self::ERROR_DESCRIPTION_IS_EMPTY:
                self::addMessage('[ERROR-DESCRIPTION-IS-EMPTY]', 'error');
                return;
                break;

             case self::ERROR_DESCRIPTION_IS_DUPLICATED:
                self::addMessage('[ERROR-DESCRIPTION-IS-DUPLICATED]', 'error');
                return;
                break;

            // The description is too long...
            case self::ERROR_DESCRIPTION_IS_TOO_LONG:
                self::addMessage('[ERROR-DESCRIPTION-IS-TOO-LONG] ' . \Modules\Humpchies\AdModel::MAX_LEN_DESCRIPTION . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The description is too short
            case self::ERROR_DESCRIPTION_IS_TOO_SHORT:
                self::addMessage('[ERROR-DESCRIPTION-IS-TOO-SHORT] ' . \Modules\Humpchies\AdModel::MIN_LEN_DESCRIPTION . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The price is invalid
            case self::ERROR_PRICE_IS_INVALID: //. \Modules\Humpchies\AdModel::MAX_PRICE
                self::addMessage('[ERROR-PRICE-IS-INVALID] ' , 'error');
                return;
                break;

            // Price conditions are too long...
            case self::ERROR_PRICE_CONDITIONS_IS_TOO_LONG:
                self::addMessage('[ERROR-PRICE-CONDITIONS-IS-TOO-LONG] ' . \Modules\Humpchies\AdModel::MAX_LEN_PRICE_CONDITIONS . ' [LABEL-CHARACTERS]', 'error');
                return;
                break;

            // The phone number is empty
            case self::ERROR_PHONE_IS_EMPTY:
                self::addMessage('[ERROR-PHONE-IS-EMPTY]', 'error');
                return;
                break;

            // The phone number is invalid...
            case self::ERROR_PHONE_IS_INVALID:
                self::addMessage('[ERROR-PHONE-IS-INVALID]', 'error');
                return;
                break;

            // The phone number not from CA...
            case self::ERROR_NOT_SUPPORTED_COUNTRY:
                self::addMessage('[ERROR-PHONE-NOT-SUPPORTED-COUNTRY]', 'error');
                return;
                break;

            /**
             * Hum!... something shady is going on...the locations options
             * are provided via a select. Yet, the user has managed to fuck it
             * up... I wonder how... nevermind that ... kick the user out of here.
             */
            case self::ERROR_LOCATION_IS_INVALID:
                self::addMessage('[ERROR-LOCATION-IS-INVALID]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                return FALSE;
                break;

            /**
             * Hum!... yet again...something shady is going on...the user id is
             * retrieved from the session. Somwhow the user has managed to fuck
             * it up. This indicates that something shady is going...kick
             * the user out of here!
             */
            case self::ERROR_USER_ID_IS_INVALID:
                \Core\Utils\Session::abandon();
                self::addMessage('[ERROR-USER-ID-IS-INVALID]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                break;

            // The user is banned...
            case self::ERROR_USER_IS_NOT_ALLOWED:
                \Core\Utils\Session::abandon();
                self::addMessage('[ERROR-USER-IS-NOT-ALLOWED]', 'error');
                self::redirect(\Core\Utils\Navigation::urlFor('/'));
                break;

            // Unhandled error...
            default:
                self::addMessage("[ERROR-UNHANDLED]: $edit_ad_validity", 'error');
                return;
        }

        //$_POST['city'] = mb_convert_encoding($_POST['city'], "UTF-8", "Windows-1252" );



        if(! is_null($ad_record['package_status']) && $ad_record['city'] != $_POST['city']){
            $city_change_status = \Modules\Humpchies\AdModel::changePremiumcity($ad_record['order_package_id'], $_POST['city']);
            if($city_change_status == \Modules\Humpchies\AdModel::ERROR_UNKNOWN){
                return self::addMessage("[ERROR-UNHANDLED] ", 'error');
            }
        }
        
        if( isset($_POST['phone']) && !empty($_POST['phone']) ){
            if( \Modules\Humpchies\AdModel::checkIfPhoneBlacklisted( $_POST['phone'] ) ){
                $_POST['status'] = 'banned';
                $_POST['comment'] = 'attemp change blacklisted phone number: '. $_POST['phone'];
            }
        }else{
        // VALIDATE PHONE WHEN DONE 
            if($_POST['validate_phone']){
                $phone = (preg_match("`(^|\\+)" . $_POST['phone_country_code'] . "`is", $_POST['phone']))? $_POST['phone'] : $_POST['phone_country_code'] . $_POST['phone'];
                $phoneValidation = \Core\library\checkMobi::validationCall($phone);
                $_POST['validation_id'] = $phoneValidation['id'];
                $_POST['dialing_number'] = $phoneValidation['dialing_number'];          
                unset($_POST['validate_phone']);
            }
        }
        
        $user_auto_approval = \Modules\Humpchies\UserModel::getUserAutoApprovalById( $user_id );
        
        $monitorAdFinal = [];
        $monitorAdFinal['location']         = $_POST['location'];  
        $monitorAdFinal['price']            = $_POST['price'];  
        $monitorAdFinal['price_conditions'] = $_POST['price_conditions'];  
        $monitorAdFinal['title']            = $_POST['title'];  
        $monitorAdFinal['description']      = $_POST['description']; 
        $monitorAdFinal['phone']            = $_POST['phone'];  
        $monitorAdFinal['myfans_url']       = $_POST['myfans_url'];
        $monitorAdFinal['category']         = $_POST['category'];
        if($monitorAdFinal['phone'] != $monitorAdInitial['phone'] ){
             $_POST['phone_validated'] = "N";
        }
        
        $changes = array_diff_assoc($monitorAdFinal, $monitorAdInitial); 
         
        if( (count($changes) || $ad_is_updated['is_updated'] || $added_images)  && !$user_auto_approval['auto_approval'] && !$isbumped && !$ispremium){
            //if($ad_record['verification_status'] == 'verified' ){
//                $_POST['verification_status'] = 'updated';   
//            }        
            $_POST['revision_ip']= ip2long(self::userIP());
            $geo = \Core\library\Geo::getLocation(self::userIp());
            $_POST['rev_country']       = $geo['country'];
            $_POST['rev_country_id']    = \Core\library\Geo::getCountryID($geo['country']);
            $_POST['rev_location']      = $geo['city']. ", " . $geo['zone'] . ", ". $geo['countryName'] ;
        
            $city_details =  \Modules\Humpchies\CityModel::getCityDetails($city_slug);
//            var_dump($city_details);
            if(!empty($city_details)) {
                
                $_POST['city_id'] = $city_details['id'];
                $_POST['region_id'] = $city_details['region_id'];
                $originalPostData['city_id'] = $city_details['id'];
            }
//            var_dump($_POST);exit();
            $_POST['vpn'] = (\Core\library\checkVpn::isVpn(\Core\Utils\Device::getDeviceIp_new()))? "Y" : "N";
            $edit_add_result = \Modules\Humpchies\AdModel::editRevision($ad_record['id'], $user_id, $_POST, $ad_record['ad_status'], $original_ad);
            unset($_POST['vpn']);
            $is_revision = true;
        } else {
            //if($ad_record['verification_status'] == 'verified' ){
//                $_POST['verification_status'] = 'updated';   
//            }
            
            $city_details =  \Modules\Humpchies\CityModel::getCityDetails($city_slug);
    //        var_dump($city_details);
            if(!empty($city_details)) {
                
                $_POST['city_id'] = $city_details['id'];
                $_POST['region_id'] = $city_details['region_id'];
                $originalPostData['city_id'] = $city_details['id'];
            }
            $_POST['date_released'] = time();
            
            // make sure location is never empty
            if(!in_array($_POST['location'], ['incalls','outcalls','both','unknown'])) {
                $_POST['location'] =  'incalls';
            }
            
            $edit_add_result = \Modules\Humpchies\AdModel::edit($ad_record['id'], $user_id, $_POST);
            unset($_POST['date_released']);
            $is_revision = false;
            if($ispremium){
                \Modules\Humpchies\AdModel::changePackCity($ad_record['id'], $user_id, $_POST['city']);
            }
        }
        
        $attached_video = \Modules\Humpchies\AdModel::getUserVideobyID(\Core\Utils\Session::getCurrentUserID(), $attached_video);
        
        if(!empty($attached_video[0]['video_name'])){
            $attached_video = $attached_video[0]['video_name'];
            \Modules\Humpchies\AdModel::edit($ad_record['id'], $user_id, ["attached_video" => $attached_video]);
        }

        /**
         * Handle possible erros during ad edition. At the time of this
         * write, one case is handled:
         *
         * Duplicated description
         *
         * Any other error is handled as 'unknown'. ToDo: explore the posibilities
         * to further handle potential issues.
         */
        if($edit_add_result !== TRUE)
        {
            switch($edit_add_result)
            {
                case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_DUPLICATED:
                    self::addMessage('[ERROR-DESCRIPTION-IS-DUPLICATED]', 'error');
                    return;
                    break;
                default:
                    return self::addMessage("[ERROR-UNHANDLED] ", 'error');
            }
        }

        // update location if needed
        if(!self::$data["map_coords"]) {
            
            // ----------------- SAVE LOCATION ---------------------- //
            $hide_map_check =  isset($originalPostData['hide_map_check']) && $originalPostData['hide_map_check'] == 'yes'? 1 : 0;
            $hide_map       =  isset($originalPostData['hide_map']) && $originalPostData['hide_map'] == 'yes'? 1 : 0;
            
            $location_fields = [
                '`ad_id`'             => $ad_record['id'],
                '`user_id`'           => \Core\Utils\Session::getCurrentUserID(),
                '`ad_city_id`'        => $originalPostData['city_id'],
                '`ad_city_name`'      => $originalPostData['city'],
                '`lat`'               => isset($originalPostData['maps_lat'])? floatval($originalPostData['maps_lat']) : '',
                '`long`'              => isset($originalPostData['maps_long'])? floatval($originalPostData['maps_long']) : '',
                '`current_city_name`' => isset($originalPostData['maps_city'])? $originalPostData['maps_city'] : '',
                '`hide_map`'          => ($hide_map_check || $hide_map)? 1 : 0
            ];
            if(isset($originalPostData['maps_long']) && floatval($originalPostData['maps_lat']) != 0 && isset($originalPostData['maps_lat'])) {
                $ad_location_result = \Modules\Humpchies\AdLocationModel::create($location_fields);
            }
        }  else {
            if(isset($originalPostData['maps_long']) && floatval($originalPostData['maps_lat']) != 0 && isset($originalPostData['maps_lat'])) {
                
                $hide_map_check =  isset($originalPostData['hide_map_check']) && $originalPostData['hide_map_check'] == 'yes'? 1 : 0;
                $hide_map       =  isset($originalPostData['hide_map']) && $originalPostData['hide_map'] == 'yes'? 1 : 0;
                $originalPostData['hide_map'] =  ($hide_map_check || $hide_map)? 'yes' : 'no';
                
                //var_dump($originalPostData);exit(); 
                $updated_map = \Modules\Humpchies\AdLocationModel::edit($ad_record['id'], $user_id, self::$data["map_coords"], $originalPostData); 
            }   
        }
        
        
         // save TAGS
        //if(\Core\Utils\Session::getCurrentUserID() == 63661){ // only testme
            //var_dump("ORIGINAL POST");
            //var_dump($originalPostData);
            \Modules\Humpchies\AdTagModel::edit($ad_record['id'], $originalPostData['tags']);
        //}
        
        if(($new_ad_record = \Modules\Humpchies\AdModel::getAdDetailsForEdition($ad_record['id'], $user_id)) === FALSE)
        {
            self::addMessage('[ERROR-AD-ID-IS-INVALID]', 'error');
            self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings', null, true, true));
            return;
        }

        $new_ad_record = array_shift($new_ad_record);

        \Modules\Humpchies\UserModel::trackIp(\Core\Utils\Session::getCurrentUserID(), \Core\Utils\Device::getDeviceIp());
        \Modules\Humpchies\UserModel::trackPhone(\Core\Utils\Session::getCurrentUserID(), $new_ad_record['phone']);
       
       if($phoneValidation['dialing_number']){
             self::redirect(\Core\Utils\Navigation::urlFor('ad', 'phone-validation', ["ad" => $ad_record['id'] ], true, true));
       }else{
        if( $is_revision){ 
            self::addMessage('[SUCCESS-EDIT-REVISION-ADDED-SUCCESFUL]', (self::getMessages() === FALSE ? 'success' : 'warning'));
        }else{
             self::addMessage('[SUCCESS-AD-EDITION-SUCCESFUL]', (self::getMessages() === FALSE ? 'success' : 'warning'));     
        }
            
        self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings',null, true, true));
    }
    }
    
    /**
     * put your comment there...
     *
     * @param mixed $absolut_path_to_original_image
     * @param mixed $absolut_path_to_replica_image
     * @param mixed $replica_image_width
     */
    public static function resampleJPEGImage($absolut_path_to_original_image, $absolut_path_to_replica_image, $replica_image_width = NULL, $force_destination_directory_creation = FALSE)
    {
        // Get the height and width of the original image
        list($original_image_width, $original_image_height) = @getimagesize($absolut_path_to_original_image);

        // Validate the height and width of the original image: both must be greater than 0
        if($original_image_height <= 0){
            return false;//self::ERROR_INVALID_IMAGE_HEIGHT;
        }elseif($original_image_width <= 0){
            return false;//self::ERROR_INVALID_IMAGE_WIDTH;
        }

        if($replica_image_width === NULL)
            $replica_image_width = $original_image_width;

        /**
         * Get the orientation of original image. Based on the original orientaion of the image
         * define the height of our replica image. Nota bene: calculations based on 3:2 aspect ratio
         */
        if(($original_image_orientation = ($original_image_width/$original_image_height)) < 1)      // Less than 1 => width < height: Portrait
            $replica_height = (($replica_image_width / 2) * 3);
        elseif($original_image_orientation > 1)                                                     // More than 1 => width > height: Landscape
            $replica_height = (($replica_image_width / 3) * 2);
        else                                                                                        // Exactly 1 => wicth is equal to height: Square
            $replica_height = $replica_image_width;

        /**
         * Adjust height or width so that their values allow us to resample the image proportionally
         */
        if(($replica_image_width/$replica_height) > $original_image_orientation)
        {
            $height_for_resampling = ($replica_image_width/$original_image_orientation);
            $width_for_resampling = $replica_image_width;
        } else {
            $width_for_resampling = ($replica_height*$original_image_orientation);
            $height_for_resampling = $replica_height;
        }

        $process = imagecreatetruecolor(round($width_for_resampling), round($height_for_resampling));

        $image_for_resampling = @imagecreatefromjpeg($absolut_path_to_original_image);

        if($image_for_resampling === FALSE){
            return false;//self::ERROR_INVALID_IMAGE;
        }

        imagecopyresampled($process, $image_for_resampling, 0, 0, 0, 0, $width_for_resampling, $height_for_resampling, $original_image_width, $original_image_height);
        $replica_image = @imagecreatetruecolor($replica_image_width, $replica_height) or die('Error initialization GD');;
        imagecopyresampled($replica_image, $process, 0, 0, (($width_for_resampling/2)-($replica_image_width/2)), (($height_for_resampling/2)-($replica_height/2)), $replica_image_width, $replica_height, $replica_image_width, $replica_height);

        imagedestroy($process);
        imagedestroy($image_for_resampling);

        if($force_destination_directory_creation === TRUE && is_dir(dirname($absolut_path_to_replica_image)) === FALSE && @mkdir(dirname($absolut_path_to_replica_image), 0777, TRUE) === FALSE){
            return false;//self::ERROR_UNABLE_TO_CREATE_RESAMPLING_DIR;
        }

        return imagejpeg($replica_image, $absolut_path_to_replica_image, 100);
    }

    public static function preview_ma()
    {
        /**
         * Validate the ad ID being passed. If the id is invalid or the id doesn't match any ads
         * bail out!
         */
        // if( \Core\Utils\Security::isIdInputValid($_GET['id']) !== \Core\Utils\Security::NO_ERROR            // Validate the id as being an integer >= 1
        //     ||
        //     (self::$data['ad'] = \Modules\Humpchies\AdModel::getDetailsOfActiveAd($_GET['id'])) === FALSE)  // Make sure the ID actually returns an ad
        //     {
        //         \Core\Router::throw404();                                                                   // Failure of any of the above conditions triggers a 404
        //         return FALSE;                                                                               // we then advise the calling context of the failure here
        //     }

        /**
         * The code to retrieve the ad uses '\Core\Data::findBySQL'. The findBySQL method
         * makes a call to \Core\dbConn::select. The select method executes a generic query
         * which against the database. This call retrieves its results as an array of
         * associative arrays. This means that the results come back in the form of a 2
         * dimentional array. Since in this case we are retrieving only 1 item, we need to
         * 'shift' that item to access it as a 1 dimention relational array.
         */
        self::$data['ad'] = array_shift(self::$data['ad']);

        /**
         * Make sure we are running the ad from a single URL... To do so, we grab the current
         * request URI, then build the official URL of the ad... compare them both and voila!
         * If the don't match redirect the request to the proper URL, and advise the calling
         * context about the failure - to prevent down stream code from executing. Even though
         * the redirection would call exit().
         */

        if(\Core\Utils\Device::getServerVarsAndHeaders('request_uri') !== ($expected_ad_uri = self::getDetailsURL(self::$data['ad']['category'], \Core\Utils\Text::stringToUrl(self::$data['ad']['city']), self::$data['ad']['id'], self::$data['ad']['title'])))
        {
            self::redirect($expected_ad_uri);
            return FALSE;
        }

        // Parse the ad fields into humanly readable information...
        self::prepareAdForGUI(self::$data['ad']);

        // Whether or not this ad is a VIP add
        self::$data['ad']['vip'] = in_array(self::$data['ad']['id'], self::getVIPZoneAdIds());

        // SEO VALUES
        // ==========

        // Title
        self::setTitle(self::$data['ad']['title']);

        // Description
        self::setDescription(self::$data['ad']['seo_head_description']);

        // Keywords
        self::setKeywords('[LABEL-' . strtoupper(self::$data['ad']['category']) . '-SINGULAR]' .' ' . self::$data['ad']['city_name']);

        // In page text
        self::setInPageText(    '[IN-PAGE-TEXT-AD-DETAILS-PART-1] '
            .
            self::getTitle()
            .
            '[IN-PAGE-TEXT-AD-DETAILS-PART-2] '
            .
            '[LABEL-' . strtoupper(self::$data['ad']['category']) . '] '
            .
            '[IN-PAGE-TEXT-AD-DETAILS-PART-3] '
            .
            self::$data['ad']['city_name']
            .
            ' [IN-PAGE-TEXT-AD-DETAILS-PART-4]');

        // SLIMBOX SUPPORT
        // ===============
        \Core\Controller::setExtraHead('<meta property="og:image" content="' . self::$data['ad']['images']['main']['src'] . '">
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'jquery/jquery-1.11.2.min.js"></script>
                                        <script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'fancybox/jquery.fancybox.min.js"></script>
                                        <link rel="stylesheet" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'fancybox/jquery.fancybox.min.css" type="text/css" media="screen" />
                                        <style>.fancybox-navigation { position: static !important; }</style>');
        /*<script type="text/javascript" src="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::js') . 'slimbox/slimbox2.js"></script>
        <link rel="stylesheet" href="' . \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::css') . 'slimbox/default.' . \Core\Utils\Session::getLocale() . '.1.0.0.css" type="text/css" media="screen" />');*/

    }

    public static function delete_ma()
    {
        self::disableCache();

        // Custom SEO values
        // =================
       /* self::setTitle('[DELETE-AD-TITLE]');

        // Enable custom CSS support
        // =========================
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="'
            .
            \Modules\Humpchies\Utils::getCDNPath('css')
            .
            (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch'))
            .
            '/contact.css?v=1">');

        self::$data['cancellation_url'] = \Core\Utils\Navigation::urlFor('ad', 'my-listings');
        self::$data['token']            = md5(@$_GET['id'] . self::DELETE_KEY);

        // Bail out if the form hasn't been posted
        // =======================================
        if(!$_POST)
            return;

        if(\Core\Utils\Security::isIdInputValid($_GET['id']) !== \Core\Utils\Security::NO_ERROR || $_POST['token'] !== md5($_GET['id'] . self::DELETE_KEY))
            self::addMessage('[ERROR-AD-ID-IS-INVALID]', 'error');
*/
        if(\Modules\Humpchies\AdModel::tagForDeletion($_GET['id'], \Core\Utils\Session::getCurrentUserID()) === \Modules\Humpchies\AdModel::NO_ERROR){
            self::addMessage('[SUCCESS-AD-DELETION-SUCCESFUL]');
            \Modules\Humpchies\AdModel::EscortActivitylog('delete_advertisement', array('id' => $_GET['id'],'user' => \Core\Utils\Session::getCurrentUserID(), 'ip' => \Core\Utils\Device::getDeviceIp() ));


        }else
            self::addMessage('[ERROR-AD-DELETION-FAILED]', 'error');
         $total_ads = \Modules\Humpchies\AdModel::getTotalAdsFromUser(\Core\Utils\Session::getCurrentUserID());
        if (\Core\Cache::get(\Core\Utils\Device::getDeviceIp()) !== FALSE && intval($total_ads[0]['cate'])== 0 ) {
             \Core\Cache::delete(\Core\Utils\Device::getDeviceIp());
        }
        self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
        return;
    }
    
    public static function report()
    {
        self::disableCache();
        header('Content-Type: application/json');
        $user_id = \Core\Utils\Session::getCurrentUserID();
//        if(!$user_id){
//            echo json_encode(['error' => "You are not logged in"]);   
//            die();
//        }
        $ad = \Modules\Humpchies\AdModel::getDetailsOfActiveAd(intval($_POST['adid']));
        if(!$ad){
            echo json_encode(['error' => '[LABEL-REPORT-UERROR]']);   
            die();
        }
        $ad= $ad[0];
       
        //$more = ($_POST['type'] == 4 && $_POST['more'] ) ? $_POST['more']:"";
        $fields = [ 'user_id'       => $ad['user_id'], 
                    'ad_id'         => $ad['id'], 
                    'report_type'   => $_POST['type'], 
                    'report_more'   =>  mb_convert_encoding($_POST['more'], 'HTML-ENTITIES', "UTF-8"), 
                    'reporter_ip'   => ($user_id)? ip2long(self::userIP()) : ip2long($_SERVER['REMOTE_ADDR']), 
                    'reporter_id'   => (!$user_id)? 0 : $user_id, 
                    'report_date'   => time(),
                    'reporter_email'   => ($_POST['email'] ) ? $_POST['email']:""
                    ];
                    
        if( \Modules\Humpchies\AdModel::report($fields)){
             echo json_encode(['success' => '[LABEL-REPORT-SUCCESS]']);         
        }else{
            echo json_encode(['error' => '[LABEL-REPORT-ERROR]']);       
        }
        die();
        return;
    }

    public static function repost_ma()
    {
        if(\Modules\Humpchies\UserModel::isBehindBannedIp(\Core\Utils\Device::getDeviceIp()))
        {
            //self::handlePreviouslyBanned(\Core\Utils\Session::getCurrentUserID());
        }

        self::disableCache();

        if(\Core\Cache::get(\Core\Utils\Device::getDeviceIp()) !== FALSE)
            self::addMessage('[ERROR-AD-POST-WAIT-TIME] ' . ($minutes = (self::getAdPostTimeSpan() / 60)) . ' [LABEL-MINUTE]', 'error');
        elseif(\Core\Utils\Security::isIdInputValid($_GET['id']) !== \Core\Utils\Security::NO_ERROR)
            self::addMessage('[ERROR-AD-ID-IS-INVALID]', 'error');
        elseif(\Modules\Humpchies\UserModel::isAllowed(\Core\Utils\Session::getCurrentUserID()) !== TRUE)
            self::addMessage('[ERROR-ACCOUNT-BANNED]', 'error');
        elseif(\Modules\Humpchies\AdModel::repost($_GET['id'], \Core\Utils\Session::getCurrentUserID()))
        {
            self::addMessage('[SUCCESS-AD-REPOST-SUCCESFUL]');
            \Core\Cache::save(\Core\Utils\Device::getDeviceIp(), 1, self::getAdPostTimeSpan());
            \Modules\Humpchies\UserModel::trackIp(\Core\Utils\Session::getCurrentUserID(), \Core\Utils\Device::getDeviceIp());
        }
        else
            self::addMessage('[ERROR-AD-REPOST-FAILED]', 'error');

        self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
        return;
    }

    public static function stop_ma()
    {
        if(\Modules\Humpchies\UserModel::isBehindBannedIp(\Core\Utils\Device::getDeviceIp()))
        {
            //self::handlePreviouslyBanned(\Core\Utils\Session::getCurrentUserID());
        }

        self::disableCache();

        if(\Core\Utils\Security::isIdInputValid($_GET['id']) !== \Core\Utils\Security::NO_ERROR)
            self::addMessage('[ERROR-AD-ID-IS-INVALID]', 'error');
        elseif(\Modules\Humpchies\AdModel::stop($_GET['id'], \Core\Utils\Session::getCurrentUserID()) === \Modules\Humpchies\AdModel::NO_ERROR)
            self::addMessage('[SUCCESS-AD-STOP-SUCCESFUL]');
        else
            self::addMessage('[ERROR-AD-STOP-FAILED]', 'error');

        self::redirect(\Core\Utils\Navigation::urlFor('ad', 'my-listings'));
        return;
    }

    /**
     * put your comment there...
     *
     * @param mixed $category_code
     * @param mixed $city_code
     * @param string $root_is_index
     *
     * @returns mixed
     */
    public static function getListURL($category_code = NULL, $city_code = NULL, $root_is_index = FALSE, $region_code = NULL)
    {
        /**
         * Truth table:
         *
         *   x = category    y = city        f(x)
         *   0               0               /ad/listing or customs root url
         *   0               1               /ad/listing/city/city_code
         *   1               0               /ad/listing/category/category_code
         *   1               1               /ad/listing/category/category_code/city/city_code
         */
        switch((($x = ($category_code === NULL ? 0 : 1)) + ($y = ($city_code === NULL ? 0 : 2)) + ($z = ($region_code === NULL ? 0 : 4))))
        {
            // Case 3: Get URL with Category and region
            case 5:
                return \Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => $category_code, 'region' => $region_code));
                break;
            // Case 3: Get URL with region
            case 4:
                return \Core\Utils\Navigation::urlFor('ad', 'listing', array('region' => $region_code));
                break;
            
            // Case 3: Get URL with Category and City
            case 3:
                return \Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => $category_code, 'city' => $city_code));
                break;
            // Case 2: get URL with City
            case 2:
                return \Core\Utils\Navigation::urlFor('ad', 'listing', array('city' => $city_code));
                break;
            // Case 1: get URL with Category
            case 1:
                return \Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => $category_code));
                break;
            // Default: return a basic ad listing to show all ads
            default:
                if(FALSE === $root_is_index)
                    return \Core\Utils\Navigation::urlFor('ad', 'listing');
                else
                    return \Core\Utils\Navigation::urlFor('/');

                break;
        }
    }
    
    
     public static function getListURLV2($category_code = NULL, $city_code = NULL, $root_is_index = FALSE, $region_code = NULL) {
         
        switch((($x = ($category_code === NULL ? 0 : 1)) + ($y = ($city_code === NULL ? 0 : 2)) + ($z = ($region_code === NULL ? 0 : 4)))) {
            // Case 3: Get URL with Category and region
            case 5:
                return \Core\Utils\Navigation::urlFor('ad', 'listing-v2', array('category' => $category_code, 'region' => $region_code));
                break;
            // Case 3: Get URL with region
            case 4:
                return \Core\Utils\Navigation::urlFor('ad', 'listing-v2', array('region' => $region_code));
                break;
            
            // Case 3: Get URL with Category and City
            case 3:
                return \Core\Utils\Navigation::urlFor('ad', 'listing-v2', array('category' => $category_code, 'city' => $city_code));
                break;
            // Case 2: get URL with City
            case 2:
                return \Core\Utils\Navigation::urlFor('ad', 'listing-v2', array('city' => $city_code));
                break;
            // Case 1: get URL with Category
            case 1:
                return \Core\Utils\Navigation::urlFor('ad', 'listing-v2', array('category' => $category_code));
                break;
            // Default: return a basic ad listing to show all ads
            default:
                if(FALSE === $root_is_index)
                    return \Core\Utils\Navigation::urlFor('ad', 'listing-v2');
                else
                    return \Core\Utils\Navigation::urlFor('ad', 'listing-v2');

                break;
        }
    }
    
    
    
    

    public static function getRegionUrl($category_code = NULL, $region_code)  {
        
        if($category_code === NULL)
            return \Core\Utils\Navigation::urlFor('ad', 'listing', array('region' => $region_code));
        
        
        return \Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => $category_code, 'region' => $region_code));
    }
    
    public static function getRegionUrlV2($category_code = NULL, $region_code)  {
        
        if($category_code === NULL)
            return \Core\Utils\Navigation::urlFor('ad', 'listing-v2', array('region' => $region_code));
        
        return \Core\Utils\Navigation::urlFor('ad', 'listing-v2', array('category' => $category_code, 'region' => $region_code));
    }
    
    
    /**
     * put your comment there...
     *
     * @param mixed $category_code
     * @param mixed $city_code
     * @return mixed
     */
    public static function getListTitle($category_code = NULL, $city_name = NULL, $page_number = 1)
    {
        /**
         * Truth table:
         *
         *   a = category    b = city    c = page is home    f(x)
         *   0               0           0                   Home page
         *   0               0           1                   Inner listing page
         *   0               1           0                   Home city page
         *   0               1           1                   Inner city page
         *   1               0           0                   Home category page
         *   1               0           1                   Inner category page
         *   1               1           0                   Home category by city page
         *   1               1           1                   Inner category by city page
         */

        $a = ($category_code === NULL ? 0 : 4);
        $b = ($city_name === NULL ? 0 : 2);
        $c = ($page_number === 1 ? 0 : 1);

        if($b)
        {
            $city_name = (  substr(\Core\Utils\Session::getLocale(), 0, 2) === 'en'
                ?
                \Core\Utils\Text::translateDiacriaticsAndLigatures($city_name)
                :
                \Core\Utils\Security::htmlEncodeText($city_name));
        }

        switch($a + $b + $c)
        {
            // Inner category by city page
            case 7:
                return '[PREFIX-LIST-TITLE] [LABEL-FOR] [LABEL-' . strtoupper($category_code) . "] [SUFFIX-LIST-TITLE] $city_name - [LABEL-PAGE] $page_number ";//- humpchies.com";
                break;
            // Home category by city page
            case 6:
                return '[PREFIX-LIST-TITLE] [LABEL-FOR] [LABEL-' . strtoupper($category_code) . "] [SUFFIX-LIST-TITLE] $city_name ";//- humpchies.com";
                break;
            // Inner category page
            case 5:
                return '[PREFIX-LIST-TITLE] [LABEL-FOR] [LABEL-' . strtoupper($category_code) . "] [LABEL-AT] [LABEL-QUEBEC] - [LABEL-PAGE] $page_number ";//- humpchies.com";
                break;
            // Home category page
            case 4:
                return '[PREFIX-LIST-TITLE] [LABEL-FOR] [LABEL-' . strtoupper($category_code) . '] [LABEL-AT] [LABEL-QUEBEC] ';//- humpchies.com';
                break;
            // Inner city page
            case 3:
                return "[PREFIX-LIST-TITLE] [SUFFIX-LIST-TITLE] $city_name - [LABEL-PAGE] $page_number ";//- humpchies.com";
                break;
            // Home city page
            case 2:
                return "[PREFIX-LIST-TITLE] [SUFFIX-LIST-TITLE] $city_name ";//- humpchies.com";
                break;
            // Inner listing page
            case 1:
                return \Core\Controller::getTitle() . " - [LABEL-PAGE] $page_number - humpchies.com";
                break;
            // Home page
            default:
                return \Core\Controller::getTitle() . ' - humpchies.com';
        }
    }

    /**
     * This function generates a tokenaized string representing the title to be used on all links
     * pointing to the details of an ad. NOTA BENE: this function does not execute any validation
     * and/or sanitaization. This is simply a concatenating rutine. Therefore, YOU are responsible
     * of making sure that the passed values are SAFE and in the PROPER FORMAT.
     *
     * @param string $category_name  humanly readable value representing the name of the ad's category
     * @param string $city_name a humanly readable value representing the name of the city of the ad
     * @param string $title a humanly readable value representing the title of the ad
     *
     * @returns string A tokenized value containning the title to be used on links pointing to the
     * details of an ad
     */
    public static function getTokenizedDetailsHrefTitle($category_name, $city_name, $title)
    {
        return  '[HREF-TITLE-AD-DETAILS-PART-1] '
            .
            $category_name
            .
            ' [HREF-TITLE-AD-DETAILS-PART-2] '
            .
            $city_name
            .
            ' [HREF-TITLE-AD-DETAILS-PART-3] '
            .
            $title;
    }

    /**
     * This function grabs the raw data of an ad that is to be used in a list,
     * and formats it to be consumable on the GUI. A list ad is one that is part
     * of a multi item recorsed. For example, search, ads by city, ads by
     * category, etc. Consumable implies:
     *
     * 1.- Removing potential security threats by sanitizing user inputed text
     * 2.- Generating SEO values
     * 3.- Generating links and their titles
     * 4.- Parsing data from DB abstract values into humanly readable ones
     * 5.- Generating image URLs
     *
     * @param array $ad The list ad needed to be parsed
     * @returns NULL
     */
    private static function prepareListAdForGUI(&$ad)
    {

        // Parse the images into values that are consumable by the GUI...
//      $ad['all_images'] = \Modules\Humpchies\AdImageModel::getAdHubImagesWebDetails($ad);
        $ad['all_images'] = \Modules\Humpchies\AdImageModel::getAdHubImagesWebListV2($ad);
        
        
//        var_dump($ad);
        // Parse the date released of the ad into a human readable value
        $ad['date_released'] = \Modules\Humpchies\Utils::getTimeDifferenceMessage($ad['date_released']);

        // Parse the ad's category into a human readable value
        $ad['category_name'] =  '[LABEL-' . strtoupper($ad['category'])  . ']';

        // Retrieve the thumb details in web format: src (Absolut CDN path of the ad's thumb), height and width
//        $ad['thumb_info'] = \Modules\Humpchies\AdImageModel::getMainThumbWebDetails($ad);
        $ad['thumb_info'] = \Modules\Humpchies\AdImageModel::getMainThumbWebDetailsForGuiV2($ad);
//        var_dump($ad['thumb_info']); die;
        /**
         * Parse the ad's city into a human readable value. If the language of the page is English
         * we remove all accents as English doesn't use any. Otherwise, we encode special characters
         * that might be present in the name of the city. Like �, �, etc. in French.
         */
        $ad['city_name'] = (substr(\Core\Utils\Session::getLocale(), 0, 2) === 'en' ? \Core\Utils\Text::translateDiacriaticsAndLigatures($ad['city']) : \Core\Utils\Security::htmlEncodeText($ad['city']));

        /**
         * Get the relative URL to access the details of the ad. The URL pattern in the string file
         * supports two parameters: [ID] the id of the ad, and [LABEL-TITLE] the title of the ad.
         */
        $ad['url'] = self::getDetailsURL($ad['category'], \Core\Utils\Text::stringToUrl($ad['city']), $ad['id'], $ad['title']);

        /**
         * Get the relative URL to access the list of all ads under the same category as the current
         * ad. The URL pattern supports 1 parameter: CATEGORY_CODE which is the category code as
         * stored in the database.
         */
        $ad['category_url'] = self::getListURL($ad['category']);

        /**
         * Get the title to be used on the link to the list of ads under the same category as the
         * current ad. The title string supports one parameter: CATEGORY_NAME which is the name of the
         * category as stored in the database only in upper case
         */
        $ad['category_href_title'] = '[HREF-TITLE-' . strtoupper($ad['category']) . '-PART-1] [LABEL-MONTREAL]';

        /**
         * Get the relative URL to access the list of all ads under the same category and city as
         * the current ad. The URL pattern supports 2 tokens: CATEGORY_CODE which is the category
         * code of the current ad as stored in the DB, and CITY_NAME which is the URL optimized
         * city name of the current ad.
         */
        $ad['category_url_w_city'] = self::getListURL($ad['category'], \Core\Utils\Text::stringToUrl($ad['city']));

        /**
         * Get the title to be used on the links to the list of ads under the same category and city
         * as the current ad. The title string supports two tokens: CATEGORY_CODE which is the category
         * code of the current ad as stored in the DB, and CITY_NAME which is the name of the city
         * for the current ad in a human readable format.
         */
        $ad['category_href_w_city_title'] = '[HREF-TITLE-' . strtoupper($ad['category']) . '-PART-1] ' . $ad['city_name'];

        /**
         * A bit of security:
         *
         * Upon ad submission we clean up as much as possible the user input. Nevertheless,
         * shit happens and a handy individual might be able to inject HTML code into the
         * title or description. In an attempt to prevent this code from executing if present,
         * we clean up the title and description for the GUI.
         */

        // Clean the title
        $ad['title'] = \Core\Utils\Security::htmlEncodeText($ad['title']);
        $ad['title_full'] = \Core\Utils\Security::htmlEncodeText($ad['title']);

        // Trim the title to a suitable length if on a mobile device...
        if(($request_device_is_pc = \Core\Utils\Device::isPC()) === FALSE && strlen($ad['title']) > self::MAX_LEN_LIST_ITEM_TITLE_MOBILE)
            $ad['title'] = substr($ad['title'], 0, self::MAX_LEN_LIST_ITEM_TITLE_MOBILE);

        // Clean the description
        $ad['description' ] = \Core\Utils\Security::htmlEncodeText($ad['description']);

        // Trim the description to a suitable length...
        $ad['description' ] = (strlen($ad['description']) > ($request_device_is_pc ? self::MAX_LEN_LIST_ITEM_DESCRIPTION__PC : self::MAX_LEN_LIST_ITEM_DESCRIPTION_MOBILE) ? substr($ad['description'], 0, ($request_device_is_pc ? self::MAX_LEN_LIST_ITEM_DESCRIPTION__PC : self::MAX_LEN_LIST_ITEM_DESCRIPTION_MOBILE)) . '...' : $ad['description']);

        /**
         * Get the title to be used in the links pointing to the details of the ad. The title pattern
         * supports 3 tokens: CATEGORY_NAME a humanly readable value representing the name of the ad's
         * category, CITY_NAME a humanly readable value representing the name of the city for which the
         * ad is applicable, and TITLE a humanly readable value representing the title of the ad.
         */
        $ad['href_title'] = self::getTokenizedDetailsHrefTitle($ad['category_name'], $ad['city_name'], $ad['title']);

    }

    /**
     * This function parses an array containning an ad's fields for the RSS
     * feed.
     *
     * @param mixed $ad The array containning the ad's fields
     * @returns NULL
     */
    private static function prepareListAdForXML(&$ad)
    {
        // Parse the date released of the ad into an RSS readable value
        $ad['date_released'] = @date(DATE_RSS, $ad['date_released']);

        /**
         * Get the relative URL to access the details of the ad. The URL pattern in the string file
         * supports two parameters: [ID] the id of the ad, and [LABEL-TITLE] the title of the ad.
         */
        $ad['url'] = self::getDetailsURL($ad['category'], \Core\Utils\Text::stringToUrl($ad['city']), $ad['id'], $ad['title'], TRUE);

        // Get and clean the title
        $ad['title'] = \Core\Utils\Security::htmlEncodeText($ad['title'], TRUE);

        // Get and clean the description
        $ad['description' ] = \Core\Utils\Security::htmlEncodeText($ad['description'], TRUE);
    }

    /**
     * This function parses an array representing an ad in a list of ads
     * containning the ads for a particular user. It formats the contents
     * of the array to render them humanly readable. It also ads all
     * nesesary URLs.
     *
     * @param array $ad An array representing the fields of an ad in a list of ads for a particular user
     * @returns NULL
     */
    private static function prepareListAdForUser(&$ad)
    {
        // Parse the date released of the ad into a human readable value
        $ad['date_released'] = ((int)$ad['date_released'] === 0 ? 0 : ((int)$ad['date_released'] > time() ? '[LABEL-PENDING]' : \Modules\Humpchies\Utils::getTimeDifferenceMessage($ad['date_released'])));

        // Parse the ad's category into a human readable value
        $ad['category_name'] =  '[LABEL-' . strtoupper($ad['category'])  . ']';

        // Retrieve the thumb details in web format: src (Absolut CDN path of the ad's thumb), height and width
//        $ad['thumb_info'] = \Modules\Humpchies\AdImageModel::getMainThumbWebDetails($ad);
        $ad['thumb_info'] = \Modules\Humpchies\AdImageModel::getMainThumbWebDetailsv2($ad);

        /**
         * Parse the ad's city into a human readable value. If the language of the page is English
         * we remove all accents as English doesn't use any. Otherwise, we encode special characters
         * that might be present in the name of the city. Like �, �, etc. in French.
         */
        $ad['city_name'] = (substr(\Core\Utils\Session::getLocale(), 0, 2) === 'en' ? \Core\Utils\Text::translateDiacriaticsAndLigatures($ad['city']) : \Core\Utils\Security::htmlEncodeText($ad['city']));

        // Set the URL which will allow the deletion of this ad...
        $ad['delete_url'] = \Core\Utils\Navigation::urlFor('ad', 'delete', array('id' => $ad['id']));

        // Set th URL which will allow the edition of the current ad...
        $ad['edit_url'] = \Core\Utils\Navigation::urlFor('ad', 'edit', array('id' => $ad['id']));

        // Set th URL which will allow the preview of the current ad...
        $ad['preview_url'] = \Core\Utils\Navigation::urlFor('ad', 'preview', array('id' => $ad['id']));

        // The the URL which will allow the user to repost the current ad...
        if($ad['status'] === 'active' || $ad['status'] === 'inactive' || $ad['status'] === 'paused')
            $ad['repost_url'] = \Core\Utils\Navigation::urlFor('ad', 'repost', array('id' => $ad['id']));

        // The the URL which will allow the user to stop the current ad...
        if($ad['status'] === 'active')
            $ad['stop_url'] = \Core\Utils\Navigation::urlFor('ad', 'stop', array('id' => $ad['id']));


        // Set up package data
        if(! is_null($ad['p_id'])) {

            $ad['p_statuses'] = explode(',', $ad['p_statuses']);
            $ad['activation_dates'] = explode(',', $ad['activation_dates']);
            $ad['expiration_dates'] = explode(',', $ad['expiration_dates']);

            $index_of_active_package = array_search( \Modules\Humpchies\AdModel::AD_PACKAGE_ACTIVE_STATUS, array_reverse($ad['p_statuses']) );

            if($index_of_active_package === FALSE) {
                $index_of_active_package = array_search( \Modules\Humpchies\AdModel::AD_PACKAGE_PENDING_STATUS, array_reverse($ad['p_statuses']) );
                $ad['premium_status'] = \Modules\Humpchies\AdModel::AD_PACKAGE_PENDING_STATUS;
            } else {
                $ad['premium_status'] = \Modules\Humpchies\AdModel::AD_PACKAGE_ACTIVE_STATUS;
            }

            $ad['package'] = [
                // 'id' => $ad['p_id'],
                'status' => $ad['p_statuses'][$index_of_active_package],
                'start' => $ad['activation_dates'][$index_of_active_package],
                'end' => $ad['expiration_dates'][$index_of_active_package],
                'is_premium' => ($ad['p_statuses'][$index_of_active_package] == \Modules\Humpchies\AdModel::AD_PACKAGE_ACTIVE_STATUS),
                'is_vip'    => ($ad['package_id'] > 0  && $ad['package_id'] <= 12)
            ];

        }
    }

    /**
     * This function formats an array containning the database fields that make out
     * an ad, and renders them humanly readable. It also creates all related URLs
     * required by the GUI.
     *
     * @param array $ad The array containing the database fields for a particular ad
     * @returns NULL
     */
    private static function prepareAdForGui(&$ad)
    {
        // Parse the date released of the ad into a human readable value
        $ad['date_released'] = \Modules\Humpchies\Utils::getTimeDifferenceMessage($ad['date_released']);

        // Parse the phone number into a human readabe value
        $ad['phone'] = \Core\Utils\Text::formatPhoneNumberForGUI($ad['phone']);

        // Format the Price into a human readable value... if no price given simply say is not available
        $ad['price'] = ($ad['price'] > 0 ? '$' . $ad['price'] : '[LABEL-NA]');

        // Format the price conditions into a human readable value... if not conditions given simply say is not available
        $ad['price_conditions'] = ($ad['price_conditions'] !== '' ? $ad['price_conditions'] : '[LABEL-NA]');

        // Parse the location into a human redable value... if location is unknown simply say is not available
        $ad['location'] = ($ad['location'] === \Modules\Humpchies\LocationModel::LOCATION_UNKNOWN ? '[LABEL-NA]' : ($ad['location'] == ''? '[LABEL-INCALLS]' : '[LABEL-' . strtoupper($ad['location']) . ']'));

        // Parse the ad's city... if the current language is English remove all accents and shit... Othewise HTML encode the special characters
        $ad['city_name'] = (substr(\Core\Utils\Session::getLocale(), 0, 2) === 'en' ? \Core\Utils\Text::translateDiacriaticsAndLigatures($ad['city']) : \Core\Utils\Security::htmlEncodeText($ad['city']));

        // Parse the category into a label which will later be translate into the proper language on PreRender
        $ad['category_name'] =  '[LABEL-' . strtoupper($ad['category']) . ']';

        // Parse the images into values that are consumable by the GUI...
//        $ad['images'] = \Modules\Humpchies\AdImageModel::getAdHubImagesWebDetails($ad);
        $ad['images'] = \Modules\Humpchies\AdImageModel::getAdHubImagesWebDetailsV2($ad);

        // If there is more than 1 image... indicate so by setting the following flag...
        $ad['has_thumbs'] = isset($ad['images']['thumbs']);

        // Get the URL to access all ads under the current ad's category...
        $ad['category_url'] = self::getListURL($ad['category'], NULL);

        // Get the link's title of the URL to access all ads under the current ad's category...
        $ad['category_href_title'] = self::getListHrefTitle($ad['category']);

        // Get the URL to access all ads under the current ad's category and city...
        $ad['category_url_w_city'] = self::getListURL($ad['category'], \Core\Utils\Text::stringToUrl($ad['city']));

        // Get the link's title of the URL to access all ads under the current ad's category and city...
        $ad['category_href_w_city_title'] = self::getListHrefTitle($ad['category'], $ad['city_name']);
        $ad['category_city_crumb'] = $ad['category_name'] . ' [IN-PAGE-TEXT-AD-DETAILS-PART-3] ' .$ad['city_name'];

        // Generate a link back to the list that brought the user to the current ad... or to the home page if the user landed straight here
        $language_options = \Core\Utils\Session::getLocaleOptions();

        if( isset($_SERVER['HTTP_REFERER'])
            &&
            parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) === \Core\Utils\Config::getConfigValue('\Application\Settings::hostname')
            &&
            $_SERVER['HTTP_REFERER'] !== $language_options[0][1])
        {
            $ad['back_link_url'] = $_SERVER['HTTP_REFERER'];
            $ad['back_link_title'] = '[HREF-TITLE-BACK-TO-LIST]';
            $ad['back_link_label'] = '[LABEL-BACK-TO-LIST]';
        }
        else
        {
            $ad['back_link_url'] = \Core\Utils\Navigation::urlFor('/');
            $ad['back_link_title'] = '[HREF-TITLE-ROOT]';
            $ad['back_link_label'] = '[LABEL-HOME]';
        }

        $ad['url'] = \Core\Utils\Device::getServerVarsAndHeaders('request_uri');

        // ====================
        // Security check point
        // ====================

        // Optimize the title for SEO and make sure is safe to display
        $ad['title'] = self::getDetailsTitle($ad['category'], $ad['city_name'], \Core\Utils\Security::htmlEncodeText($ad['title']));

        // Make sure the description is safe to display
        $ad['description' ] = \Core\Utils\Security::htmlEncodeText($ad['description']);

        // Optimize and store the SEO HEAD description
        $ad['seo_head_description'] = (strlen($ad['description' ]) > self::MAX_LEN_HEAD_DESCRIPTION ? trim(substr($ad['description' ], 0, self::MAX_LEN_HEAD_DESCRIPTION)) . '...'  : $ad['description']);

        // Turn new lines in the description into HTML breaks
        $ad['description'] = nl2br($ad['description']);

        // Make sure the phone number is safe to display
        $ad['phone'] = \Core\Utils\Security::htmlEncodeText($ad['phone']);

        // MAke sure the price is safe to display
        if($ad['price'] !== '[LABEL-NA]')
            $ad['price'] = \Core\Utils\Security::htmlEncodeText($ad['price']);

        // Make sure the price conditions are safe to display
        if($ad['price_conditions'] !== '[LABEL-NA]')
            $ad['price_conditions'] = \Core\Utils\Security::htmlEncodeText($ad['price_conditions']);

        // Make sure the location is safe to display
        if($ad['location'] !== '[LABEL-NA]')
            $ad['location'] = \Core\Utils\Security::htmlEncodeText($ad['location']);
    }

    /**
     * This function produces the link's title of the URL to access all ads under
     * a particular category and/or city
     *
     * @param string $category_code The code of the category
     * @param string $city_name The humanly readable city name
     * @returns string A tokenized string which will later be translated on PreRender
     */
    public static function getListHrefTitle($category_code = NULL, $city_name = NULL)
    {
        switch((($x = ($category_code === NULL ? 0 : 1)) + ($y = ($city_name === NULL ? 0 : 2))))
        {
            // Case 3: Get Tilte for an href with Category and City
            case 3:
                return '[HREF-TITLE-' . strtoupper($category_code) . '-PART-1] ' . $city_name;
                break;
            // Case 2: Get Tilte for an href with City only
            case 2:
                return '[HREF-TITLE-GENERIC-AD-LINK-PART-1] ' . $city_name;
                break;
            // Case 1: Get Tilte for an href with Category only
            case 1:
                return '[HREF-TITLE-' . strtoupper($category_code) . '-PART-1] [LABEL-MONTREAL]';
                break;
            // Default: Generic title including generic city
            default:
                return '[HREF-TITLE-GENERIC-AD-LINK-PART-1] [LABEL-MONTREAL]';
                break;
        }
    }

    /**
     * This function produces the absolut or relative URL of a particular ad.
     * It is in fact a wrapper around \Core\utils\Navigation::urlFor
     *
     * @param int $id The ID of the ad
     * @param string $title The title of the ad as retrieved from the database
     * @param bool $absolute Whether we want the url to be absolute. default is FALSE. i.e we want it relative.
     * @returns string A  relative or absolute path to access an ad
     */
    public static function getDetailsURL($category, $city, $id, $title, $absolute = FALSE)
    {
        return \Core\Utils\Navigation::urlFor(  'ad',
            'details',
            array(  'category' => $category,
                'city' => $city,
                'id' => $id,
                'title' => \Core\Utils\Text::stringToUrl($title)),
            TRUE,
            $absolute);
    }
    
    public static function getDetailsURLV2($category, $city, $id, $title, $absolute = FALSE)
    {
        return \Core\Utils\Navigation::urlFor(  'ad',
            'details-v2',
            array(  'category' => $category,
                'city' => $city,
                'id' => $id,
                'title' => \Core\Utils\Text::stringToUrl($title)),
            TRUE,
            $absolute);
    }

    /**
     * This function produces a tokenized string which will then
     * be translated during PreRender and turn into the title
     * of a particular ad's page.
     *
     * @param string $category_code The code of the category of the ad
     * @param string $city_name The humanly readable city name of the ad
     * @param string $title A SANATIZED string representing the title of the ad
     * @returns string A tokenized string
     */
    private static function getDetailsTitle($category_code, $city_name, $title)
    {
        return  '[PREFIX-AD-DETAILS-TITLE]'
            .
            '[LABEL-' . strtoupper($category_code) . '-SINGULAR] '
            .
            '[SUFFIX-AD-DETAILS-TITLE] '
            .
            $city_name
            .
            ': '
            .
            $title;
    }

    /**
     * This function validates the user input posted to create a new ad.
     *
     * @param int $user_id The ID of the user requesting to post a new ad
     * @param array $fields The contents of $_POST
     * @param array $images The contents of $_FILES
     * @returns string NO_ERROR constant on success. One of the ERROR constants on failure.
     */
    private static function validateNew($user_id, &$fields, &$images = NULL)
    {
        // ---------------------------REMOVE UNSUPPORTED FIELDS-------------------------------------
        $fields = array_intersect_key($fields, array(   'city'              => NULL,
            'title'             => NULL,
            'description'       => NULL,
            'price'             => NULL,
            'price_conditions'  => NULL,
            'category'          => NULL,
            'phone'             => NULL,
            'phone_country_iso' => NULL,
            'phone_country_code'=> NULL,
            'validate_phone'    => NULL,
            'location'          => NULL,
            'myfans_url'        => NULL));

        // ---------------------------REMOVE UNSUPPORTED FILES--------------------------------------
        $images = array_intersect_key($images, array(   0           => NULL,
            1           => NULL,
            2           => NULL,
            3           => NULL,
            4           => NULL));
//        if(preg_match("`^ 40`is", $fields['phone'] ) && strlen($fields['phone'])== 12){
//            echo "---->" ;
//        }    
//        die($fields['phone']);
        if(preg_match("`^ 40`is", $fields['phone'] )  && strlen($fields['phone'])== 12){
            $fields['phone'] = substr($fields['phone'], 3);
            $phone_validity = \Core\Utils\Security::NO_ERROR;
        }
        else{
            $phone_validity                 = \Core\Utils\Security::isPhoneInputValid($fields['phone']);  
        }
        
         // Validate and sanitize all fields...
        // ===================================
        $city_validity                  = \Modules\Humpchies\CityModel::isSupportedCityInputValid($fields[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME]);
        $title_validity                 = \Modules\Humpchies\AdModel::isTitleInputValid($fields['title']);                                                          // Title
        $description_validity           = \Modules\Humpchies\AdModel::isDescriptionInputValid($fields['description']);                                              // Description
        $price_validity                 = \Modules\Humpchies\AdModel::isPriceInputValid($fields['price']);
        $price_conditions_validity      = \Modules\Humpchies\AdModel::isPriceConditionsInputValid($fields['price_conditions']);                                     // Price conditions                                                                                         // Category
        $category_validity              = \Modules\Humpchies\CategoryModel::isCategoryInputValid($fields[\Plugins\HumpchiesCategoryOptions::DEFAULT_SELECT_NAME]);
                                                                      // Phone
        $location_validity              = \Modules\Humpchies\LocationModel::isLocationInputValid($fields[\Plugins\HumpchiesLocationOptions::DEFAULT_SELECT_NAME]);
        $images_validity                = \Core\Utils\Security::areJPEGImagesInputValid($images);
        $user_validity                  = \Core\Utils\Security::isIdInputValid($user_id);
       // var_dump(trim(filter_var($fields['description'], FILTER_SANITIZE_STRING)));
//        die();
        
        
        // ---------------------------CITY-------------------------------------------------------
        if($city_validity !== \Modules\Humpchies\CityModel::NO_ERROR)
            return self::ERROR_CITY_IS_INVALID;

        // ---------------------------TITLE------------------------------------------------------

        switch($title_validity)
        {
            case \Modules\Humpchies\AdModel::NO_ERROR:
                break;
            case \Modules\Humpchies\AdModel::ERROR_TITLE_IS_EMPTY:
                return self::ERROR_TITLE_IS_EMPTY;
                break;
            case \Modules\Humpchies\AdModel::ERROR_TITLE_IS_TOO_LONG:
                return self::ERROR_TITLE_IS_TOO_LONG;
                break;
            case \Modules\Humpchies\AdModel::ERROR_TITLE_IS_TOO_SHORT:
                return self::ERROR_TITLE_IS_TOO_SHORT;
                break;
            default:
                return $title_validity;
                break;
        }

        // ---------------------------DESCRIPTION------------------------------------------------
        switch($description_validity)
        {
            case \Modules\Humpchies\AdModel::NO_ERROR:
                break;
            case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_EMPTY:
                return self::ERROR_DESCRIPTION_IS_EMPTY;
                break;
            case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_TOO_LONG:
                return self::ERROR_DESCRIPTION_IS_TOO_LONG;
                break;
            case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_TOO_SHORT:
                return self::ERROR_DESCRIPTION_IS_TOO_SHORT;
                break;
            case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_DUPLICATED:
                return self::ERROR_DESCRIPTION_IS_DUPLICATED;
                break;
            default:
                return $description_validity;
                break;
        }

        // ---------------------------PRICE------------------------------------------------------
        if($price_validity !== \Modules\Humpchies\AdModel::NO_ERROR)
            return self::ERROR_PRICE_IS_INVALID;

        // ---------------------------PRICE CONDITIONS-------------------------------------------
        switch($price_conditions_validity)
        {
            case \Modules\Humpchies\AdModel::NO_ERROR:
                break;
            case \Modules\Humpchies\AdModel::ERROR_PRICE_CONDITIONS_IS_TOO_LONG:
                return self::ERROR_PRICE_CONDITIONS_IS_TOO_LONG;
                break;
            default:
                return $price_conditions_validity;
                break;
        }

        // ---------------------------CATEGORY---------------------------------------------------
        if($category_validity !== \Modules\Humpchies\CategoryModel::NO_ERROR)
            return self::ERROR_CATEGORY_IS_INVALID;

        // ---------------------------PHONE------------------------------------------------------
        switch($phone_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_PHONE_IS_EMPTY;
                return self::ERROR_PHONE_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_PHONE_IS_INVALID;
                return self::ERROR_PHONE_IS_INVALID;
                break;
            case \Core\Utils\Security::ERROR_PHONE_IS_TOO_SHORT;
            case \Core\Utils\Security::ERROR_PHONE_IS_TOO_LONG;
                return self::ERROR_PHONE_IS_INVALID;
                break;          
            default:
                return $phone_validity;
                break;
        }

//        $phone_validity = \Core\library\PhoneValidator::validate($fields['phone']);
//        switch($phone_validity)
//        {
//            case \Core\library\PhoneValidator::NO_ERROR:
//                break;
//            case \Core\library\PhoneValidator::ERROR_PHONE_IS_INVALID;
//                return self::ERROR_PHONE_IS_INVALID;
//                break;
//            case \Core\library\PhoneValidator::ERROR_NOT_SUPPORTED_COUNTRY;
//                return self::ERROR_NOT_SUPPORTED_COUNTRY;
//                break;
//            case \Core\library\PhoneValidator::ERROR_404_NOT_FOUND;
//                return self::ERROR_404_NOT_FOUND;
//                break;
//            case \Core\library\PhoneValidator::ERROR_MISSING_ACCESS_KEY;
//                return self::ERROR_MISSING_ACCESS_KEY;
//                break;
//            case \Core\library\PhoneValidator::ERROR_INVALID_ACCESS_KEY;
//                return self::ERROR_INVALID_ACCESS_KEY;
//                break;
//            case \Core\library\PhoneValidator::ERROR_INVALID_API_FUNCTION;
//                return self::ERROR_INVALID_API_FUNCTION;
//                break;
//            case \Core\library\PhoneValidator::ERROR_NO_PHONE_NUMBER_PROVIDED;
//                return self::ERROR_NO_PHONE_NUMBER_PROVIDED;
//                break;
//            case \Core\library\PhoneValidator::ERROR_NON_NUMERIC_PHONE_NUMBER_PROVIDED;
//                return self::ERROR_NON_NUMERIC_PHONE_NUMBER_PROVIDED;
//                break;
//            case \Core\library\PhoneValidator::ERROR_INVALID_COUNTRY_CODE;
//                return self::ERROR_INVALID_COUNTRY_CODE;
//                break;
//            case \Core\library\PhoneValidator::ERROR_USAGE_LIMIT_REACHED;
//                return self::ERROR_USAGE_LIMIT_REACHED;
//                break;
//            case \Core\library\PhoneValidator::ERROR_HTTPS_ACCESS_RESTRICTED;
//                return self::ERROR_HTTPS_ACCESS_RESTRICTED;
//                break;
//            case \Core\library\PhoneValidator::ERROR_INACTIVE_USER;
//                return self::ERROR_INACTIVE_USER;
//                break;
//            case \Core\library\PhoneValidator::ERROR_UNKNOWN;
//                return self::ERROR_UNKNOWN;
//                break;
//            default:
//                return $phone_validity;
//                break;
//        }

        // ---------------------------LOCATION---------------------------------------------------
        if($location_validity !== \Modules\Humpchies\LocationModel::NO_ERROR)
            return self::ERROR_LOCATION_IS_INVALID;

        // ---------------------------USER-------------------------------------------------------
        if($user_validity !== \Core\Utils\Security::NO_ERROR)
            return self::ERROR_USER_ID_IS_INVALID;
        elseif(\Modules\Humpchies\UserModel::isAllowed($user_id) === FALSE)
            return self::ERROR_USER_IS_NOT_ALLOWED;

        // ---------------------------IMAGES-----------------------------------------------------
        switch($images_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_IMAGE_SIZE_IS_TOO_BIG:
                return self::ERROR_IMAGE_SIZE_IS_TOO_BIG;
                break;
            case \Core\Utils\Security::ERROR_IMAGE_TYPE_IS_INVALID:
                return self::ERROR_IMAGE_TYPE_IS_INVALID;
                break;
            case \Core\Utils\Security::ERROR_IMAGE_ERROR_UNHANDLED:
                return self::ERROR_IMAGE_ERROR_UNHANDLED;
                break;
            case \Core\Utils\Security::ERROR_IMAGE_RESAMPLING_FAILED:
                return self::ERROR_IMAGE_RESAMPLING_FAILED;
                break;
            default:
                return $images_validity;
                break;
        }
       
        
        return self::NO_ERROR;
    }

    private static function validateNewV2($user_id, &$fields, &$images = NULL)
    {
        // ---------------------------REMOVE UNSUPPORTED FIELDS-------------------------------------
        $fields = array_intersect_key($fields, array(   'city'              => NULL,
            'title'             => NULL,
            'description'       => NULL,
            'price'             => NULL,
            'price_min'         => NULL,
            'price_max'         => NULL,
            'price_conditions'  => NULL,
            'category'          => NULL,
            'phone'             => NULL,
            'phone_country_iso' => NULL,
            'phone_country_code'=> NULL,
            'rotator'           => NULL,
            'validate_phone'    => NULL,
            'attached_video'    => NULL,
            'location'          => NULL));

        // ---------------------------REMOVE UNSUPPORTED FILES--------------------------------------
        $images = array_intersect_key($images, array(   0           => NULL,
            1           => NULL,
            2           => NULL,
            3           => NULL,
            4           => NULL));
        // Validate and sanitize all fields...
        // ===================================
        $city_validity                  = \Modules\Humpchies\CityModel::isSupportedCityInputValid($fields[\Plugins\HumpchiesCityOptions::DEFAULT_SELECT_NAME]);
        $title_validity                 = \Modules\Humpchies\AdModel::isTitleInputValid($fields['title']);                                                          // Title
        $description_validity           = \Modules\Humpchies\AdModel::isDescriptionInputValid($fields['description']);                                              // Description
        $price_validity                 = \Modules\Humpchies\AdModel::isPriceInputValid($fields['price']);
        $price_min_validity             = \Modules\Humpchies\AdModel::isPriceInputValid($fields['price_min']);
        $price_max_validity             = \Modules\Humpchies\AdModel::isPriceInputValid($fields['price_max']);
        $price_conditions_validity      = \Modules\Humpchies\AdModel::isPriceConditionsInputValid($fields['price_conditions']);                                     // Price conditions                                                                                         // Category
        $category_validity              = \Modules\Humpchies\CategoryModel::isCategoryInputValid($fields[\Plugins\HumpchiesCategoryOptions::DEFAULT_SELECT_NAME]);
        $phone_validity                 = \Core\Utils\Security::isPhoneInputValid($fields['phone']);                                                                // Phone
        $location_validity              = \Modules\Humpchies\LocationModel::isLocationsInputValid($fields['location']);
        $images_validity                = \Core\Utils\Security::areJPEGImagesInputValid($images);
        $user_validity                  = \Core\Utils\Security::isIdInputValid($user_id);


        // ---------------------------CITY-------------------------------------------------------
        if($city_validity !== \Modules\Humpchies\CityModel::NO_ERROR)
            return self::ERROR_CITY_IS_INVALID;

        // ---------------------------TITLE------------------------------------------------------

        switch($title_validity)
        {
            case \Modules\Humpchies\AdModel::NO_ERROR:
                break;
            case \Modules\Humpchies\AdModel::ERROR_TITLE_IS_EMPTY:
                return self::ERROR_TITLE_IS_EMPTY;
                break;
            case \Modules\Humpchies\AdModel::ERROR_TITLE_IS_TOO_LONG:
                return self::ERROR_TITLE_IS_TOO_LONG;
                break;
            case \Modules\Humpchies\AdModel::ERROR_TITLE_IS_TOO_SHORT:
                return self::ERROR_TITLE_IS_TOO_SHORT;
                break;
            default:
                return $title_validity;
                break;
        }

        // ---------------------------DESCRIPTION------------------------------------------------
        switch($description_validity)
        {
            case \Modules\Humpchies\AdModel::NO_ERROR:
                break;
            case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_EMPTY:
                return self::ERROR_DESCRIPTION_IS_EMPTY;
                break;
            case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_TOO_LONG:
                return self::ERROR_DESCRIPTION_IS_TOO_LONG;
                break;
            case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_TOO_SHORT:
                return self::ERROR_DESCRIPTION_IS_TOO_SHORT;
                break;
            case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_DUPLICATED:
                return self::ERROR_DESCRIPTION_IS_DUPLICATED;
                break;
            default:
                return $description_validity;
                break;
        }

        // ---------------------------PRICE------------------------------------------------------
        if($price_validity != \Modules\Humpchies\AdModel::NO_ERROR){
            return self::ERROR_PRICE_IS_INVALID;
        }

        if($price_min_validity != \Modules\Humpchies\AdModel::NO_ERROR){
            return self::ERROR_PRICE_IS_INVALID;
        }
        
        if($price_max_validity != \Modules\Humpchies\AdModel::NO_ERROR){
            return self::ERROR_PRICE_IS_INVALID;
        }
        // ---------------------------PRICE CONDITIONS-------------------------------------------
        switch($price_conditions_validity)
        {
            case \Modules\Humpchies\AdModel::NO_ERROR:
                break;
            case \Modules\Humpchies\AdModel::ERROR_PRICE_CONDITIONS_IS_TOO_LONG:
                return self::ERROR_PRICE_CONDITIONS_IS_TOO_LONG;
                break;
            default:
                return $price_conditions_validity;
                break;
        }

        // ---------------------------CATEGORY---------------------------------------------------
        if($category_validity !== \Modules\Humpchies\CategoryModel::NO_ERROR)
            return self::ERROR_CATEGORY_IS_INVALID;
            //var_dump($phone_validity);
        // ---------------------------PHONE------------------------------------------------------
        switch($phone_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                $fields['phone_country_iso'] = \Core\Utils\Phone::getCountryIso($fields['phone']);
                $fields['phone_country_code'] = \Core\Utils\Phone::getCountryCode($fields['phone']);
                break;
            case \Core\Utils\Security::ERROR_PHONE_IS_EMPTY;
                return self::ERROR_PHONE_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_PHONE_IS_INVALID;
                return self::ERROR_PHONE_IS_INVALID;
                break;
            case \Core\Utils\Security::ERROR_PHONE_IS_TOO_SHORT;
            case \Core\Utils\Security::ERROR_PHONE_IS_TOO_LONG;
                return self::ERROR_PHONE_IS_INVALID;
                break;      
            default:
                return $phone_validity;
                break;
        }

//        $phone_validity = \Core\library\PhoneValidator::validate($fields['phone']);
//        switch($phone_validity)
//        {
//            case \Core\library\PhoneValidator::NO_ERROR:
//                break;
//            case \Core\library\PhoneValidator::ERROR_PHONE_IS_INVALID;
//                return self::ERROR_PHONE_IS_INVALID;
//                break;
//            case \Core\library\PhoneValidator::ERROR_NOT_SUPPORTED_COUNTRY;
//                return self::ERROR_NOT_SUPPORTED_COUNTRY;
//                break;
//            case \Core\library\PhoneValidator::ERROR_404_NOT_FOUND;
//                return self::ERROR_404_NOT_FOUND;
//                break;
//            case \Core\library\PhoneValidator::ERROR_MISSING_ACCESS_KEY;
//                return self::ERROR_MISSING_ACCESS_KEY;
//                break;
//            case \Core\library\PhoneValidator::ERROR_INVALID_ACCESS_KEY;
//                return self::ERROR_INVALID_ACCESS_KEY;
//                break;
//            case \Core\library\PhoneValidator::ERROR_INVALID_API_FUNCTION;
//                return self::ERROR_INVALID_API_FUNCTION;
//                break;
//            case \Core\library\PhoneValidator::ERROR_NO_PHONE_NUMBER_PROVIDED;
//                return self::ERROR_NO_PHONE_NUMBER_PROVIDED;
//                break;
//            case \Core\library\PhoneValidator::ERROR_NON_NUMERIC_PHONE_NUMBER_PROVIDED;
//                return self::ERROR_NON_NUMERIC_PHONE_NUMBER_PROVIDED;
//                break;
//            case \Core\library\PhoneValidator::ERROR_INVALID_COUNTRY_CODE;
//                return self::ERROR_INVALID_COUNTRY_CODE;
//                break;
//            case \Core\library\PhoneValidator::ERROR_USAGE_LIMIT_REACHED;
//                return self::ERROR_USAGE_LIMIT_REACHED;
//                break;
//            case \Core\library\PhoneValidator::ERROR_HTTPS_ACCESS_RESTRICTED;
//                return self::ERROR_HTTPS_ACCESS_RESTRICTED;
//                break;
//            case \Core\library\PhoneValidator::ERROR_INACTIVE_USER;
//                return self::ERROR_INACTIVE_USER;
//                break;
//            case \Core\library\PhoneValidator::ERROR_UNKNOWN;
//                return self::ERROR_UNKNOWN;
//                break;
//            default:
//                return $phone_validity;
//                break;
//        }

        // ---------------------------LOCATION---------------------------------------------------
        if($location_validity !== \Modules\Humpchies\LocationModel::NO_ERROR)
            return self::ERROR_LOCATION_IS_INVALID;

        // ---------------------------USER-------------------------------------------------------
        if($user_validity !== \Core\Utils\Security::NO_ERROR)
            return self::ERROR_USER_ID_IS_INVALID;
        elseif(\Modules\Humpchies\UserModel::isAllowed($user_id) === FALSE)
            return self::ERROR_USER_IS_NOT_ALLOWED;

        // ---------------------------IMAGES-----------------------------------------------------
        switch($images_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_IMAGE_SIZE_IS_TOO_BIG:
                return self::ERROR_IMAGE_SIZE_IS_TOO_BIG;
                break;
            case \Core\Utils\Security::ERROR_IMAGE_TYPE_IS_INVALID:
                return self::ERROR_IMAGE_TYPE_IS_INVALID;
                break;
            case \Core\Utils\Security::ERROR_IMAGE_ERROR_UNHANDLED:
                return self::ERROR_IMAGE_ERROR_UNHANDLED;
                break;
            case \Core\Utils\Security::ERROR_IMAGE_RESAMPLING_FAILED:
                return self::ERROR_IMAGE_RESAMPLING_FAILED;
                break;
            default:
                return $images_validity;
                break;
        }

        return self::NO_ERROR;
    }

    private static function validateEdit($user_id, &$fields, $id = 0)
    {
        // ---------------------------REMOVE UNSUPPORTED FIELDS-------------------------------------
        $fields = array_intersect_key($fields, array(   'city'              => NULL,
            'title'             => NULL,
            'description'       => NULL,
            'category'          => NULL,
            'price'             => NULL,
            'price_min'         => NULL,
            'price_max'         => NULL,
            'price_conditions'  => NULL,
            'phone'             => NULL,
            'phone_country_iso' => NULL,
            'phone_country_code'=> NULL,
            'validate_phone'    => NULL,
            'location'          => NULL,
            'myfans_url'        => NULL));

        // Validate and sanitize supported fields
        // ======================================
        $title_validity                 = \Modules\Humpchies\AdModel::isTitleInputValid($fields['title']);                                                          // Title
        $description_validity           = \Modules\Humpchies\AdModel::isDescriptionInputValid($fields['description'], $id);                                              // Description
        $price_validity                 = \Modules\Humpchies\AdModel::isPriceInputValid($fields['price']);                                                          // Price
        $price_min_validity            = \Modules\Humpchies\AdModel::isPriceInputValid($fields['price_min']);                                                          // Price
        $price_max_validity            = \Modules\Humpchies\AdModel::isPriceInputValid($fields['price_max']);                                                          // Price
        $price_conditions_validity      = \Modules\Humpchies\AdModel::isPriceConditionsInputValid($fields['price_conditions']);                                     // Price conditions                                                                                         // Category
        //$phone_validity                 = \Core\Utils\Security::isPhoneInputValid($fields['phone']);                                                                // Phone
        $location_validity              = \Modules\Humpchies\LocationModel::isLocationInputValid($fields[\Plugins\HumpchiesLocationOptions::DEFAULT_SELECT_NAME]);  // Location
        $user_validity                  = \Core\Utils\Security::isIdInputValid($user_id);                                                                           // User ID
        
        if(preg_match("`^ 40`is", $fields['phone'] )  && strlen($fields['phone'])== 12){
            $fields['phone'] = substr($fields['phone'], 3);
            $phone_validity = \Core\Utils\Security::NO_ERROR;
        }
        else{
            $phone_validity                 = \Core\Utils\Security::isPhoneInputValid($fields['phone']);  
        }
        // ---------------------------TITLE------------------------------------------------------
        switch($title_validity)
        {
            case \Modules\Humpchies\AdModel::NO_ERROR:
                break;
            case \Modules\Humpchies\AdModel::ERROR_TITLE_IS_EMPTY:
                return self::ERROR_TITLE_IS_EMPTY;
                break;
            case \Modules\Humpchies\AdModel::ERROR_TITLE_IS_TOO_LONG:
                return self::ERROR_TITLE_IS_TOO_LONG;
                break;
            case \Modules\Humpchies\AdModel::ERROR_TITLE_IS_TOO_SHORT:
                return self::ERROR_TITLE_IS_TOO_SHORT;
                break;
            default:
                return $title_validity;
                break;
        }

        // ---------------------------DESCRIPTION------------------------------------------------
        switch($description_validity)
        {
            case \Modules\Humpchies\AdModel::NO_ERROR:
                break;
            case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_EMPTY:
                return self::ERROR_DESCRIPTION_IS_EMPTY;
                break;
            case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_TOO_LONG:
                return self::ERROR_DESCRIPTION_IS_TOO_LONG;
                break;
            case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_TOO_SHORT:
                return self::ERROR_DESCRIPTION_IS_TOO_SHORT;
                break;
            case \Modules\Humpchies\AdModel::ERROR_DESCRIPTION_IS_DUPLICATED:
                return self::ERROR_DESCRIPTION_IS_DUPLICATED;
                break;
            default:
                return $title_validity;
                break;
        }

        // ---------------------------PRICE------------------------------------------------------
        if($price_validity !== \Modules\Humpchies\AdModel::NO_ERROR)
            return self::ERROR_PRICE_IS_INVALID;
        
        if($price_min_validity !== \Modules\Humpchies\AdModel::NO_ERROR)
            return self::ERROR_PRICE_IS_INVALID;
        
        if($price_max_validity !== \Modules\Humpchies\AdModel::NO_ERROR)
            return self::ERROR_PRICE_IS_INVALID;
        // ---------------------------PRICE CONDITIONS-------------------------------------------
        switch($price_conditions_validity)
        {
            case \Modules\Humpchies\AdModel::NO_ERROR:
                break;
            case \Modules\Humpchies\AdModel::ERROR_PRICE_CONDITIONS_IS_TOO_LONG:
                return self::ERROR_PRICE_CONDITIONS_IS_TOO_LONG;
                break;
            default:
                return $price_conditions_validity;
                break;
        }

        // ---------------------------PHONE------------------------------------------------------
        switch($phone_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_PHONE_IS_EMPTY;
                return self::ERROR_PHONE_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_PHONE_IS_INVALID;
                return self::ERROR_PHONE_IS_INVALID;
                break;
            case \Core\Utils\Security::ERROR_PHONE_IS_TOO_SHORT;
            case \Core\Utils\Security::ERROR_PHONE_IS_TOO_LONG;
                return self::ERROR_PHONE_IS_INVALID;
                break;    
            default:
                return $phone_validity;
                break;
        }

//        $phone_validity = \Core\library\PhoneValidator::validate($fields['phone']);
//        switch($phone_validity)
//        {
//            case \Core\library\PhoneValidator::NO_ERROR:
//                break;
//            case \Core\library\PhoneValidator::ERROR_PHONE_IS_INVALID;
//                return self::ERROR_PHONE_IS_INVALID;
//                break;
//            case \Core\library\PhoneValidator::ERROR_NOT_SUPPORTED_COUNTRY;
//                return self::ERROR_NOT_SUPPORTED_COUNTRY;
//                break;
//            case \Core\library\PhoneValidator::ERROR_404_NOT_FOUND;
//                return self::ERROR_404_NOT_FOUND;
//                break;
//            case \Core\library\PhoneValidator::ERROR_MISSING_ACCESS_KEY;
//                return self::ERROR_MISSING_ACCESS_KEY;
//                break;
//            case \Core\library\PhoneValidator::ERROR_INVALID_ACCESS_KEY;
//                return self::ERROR_INVALID_ACCESS_KEY;
//                break;
//            case \Core\library\PhoneValidator::ERROR_INVALID_API_FUNCTION;
//                return self::ERROR_INVALID_API_FUNCTION;
//                break;
//            case \Core\library\PhoneValidator::ERROR_NO_PHONE_NUMBER_PROVIDED;
//                return self::ERROR_NO_PHONE_NUMBER_PROVIDED;
//                break;
//            case \Core\library\PhoneValidator::ERROR_NON_NUMERIC_PHONE_NUMBER_PROVIDED;
//                return self::ERROR_NON_NUMERIC_PHONE_NUMBER_PROVIDED;
//                break;
//            case \Core\library\PhoneValidator::ERROR_INVALID_COUNTRY_CODE;
//                return self::ERROR_INVALID_COUNTRY_CODE;
//                break;
//            case \Core\library\PhoneValidator::ERROR_USAGE_LIMIT_REACHED;
//                return self::ERROR_USAGE_LIMIT_REACHED;
//                break;
//            case \Core\library\PhoneValidator::ERROR_HTTPS_ACCESS_RESTRICTED;
//                return self::ERROR_HTTPS_ACCESS_RESTRICTED;
//                break;
//            case \Core\library\PhoneValidator::ERROR_INACTIVE_USER;
//                return self::ERROR_INACTIVE_USER;
//                break;
//            case \Core\library\PhoneValidator::ERROR_UNKNOWN;
//                return self::ERROR_UNKNOWN;
//                break;
//            default:
//                return $phone_validity;
//                break;
//        }

        // ---------------------------LOCATION---------------------------------------------------
        if($location_validity !== \Modules\Humpchies\LocationModel::NO_ERROR)
            return self::ERROR_LOCATION_IS_INVALID;

        // ---------------------------USER-------------------------------------------------------
        if($user_validity !== \Core\Utils\Security::NO_ERROR)
            return self::ERROR_USER_ID_IS_INVALID;
        elseif(\Modules\Humpchies\UserModel::isAllowed($user_id) === FALSE)
            return self::ERROR_USER_IS_NOT_ALLOWED;

        return self::NO_ERROR;
    }

    /**
     * This function validates the parameters passed to a request to browse ads
     *
     * @param array $parameters The contents of GET
     * @returns NO_ERROR constant on success. One of the ERROR constants on failure
     */
    private static function validateBrowse(&$parameters)
    {
        // ---------------------------REMOVE UNSUPPORTED PARAMETERS------------------------------
        $parameters = array_intersect_key($parameters, array(   'category'          => NULL,
            'city'              => NULL,
            'page'              => NULL,
            'activate-pm'       => NULL,
            'region'            => NULL
            ));

        // Validate and sanitize all parameters...
        // =======================================
        $category_validity      = \Modules\Humpchies\CategoryModel::isCategoryInputValid($parameters['category']);
        //$city_validity          = \Modules\Humpchies\CityModel::isCityWithAdsInputValid($parameters['city'], $parameters['category']);
        $city_validity          = \Modules\Humpchies\CityModel::NO_ERROR;
        $page_number_validity   = \Core\Utils\Security::isIntegerInputValid($parameters['page']);
        
        // ---------------------------CATEGORY---------------------------------------------------
        switch($category_validity)
        {
            case \Modules\Humpchies\CategoryModel::ERROR_CATEGORY_IS_EMPTY:
            case \Modules\Humpchies\CategoryModel::NO_ERROR;
                break;
            default:
                return self::ERROR_CATEGORY_IS_INVALID;
        }

        // ---------------------------CITY-------------------------------------------------------
        switch($city_validity)
        {
            case \Modules\Humpchies\CityModel::ERROR_CITY_IS_EMPTY:
            case \Modules\Humpchies\CityModel::NO_ERROR;
                break;
            default:
                return self::ERROR_CATEGORY_IS_INVALID;
        }

        // ---------------------------PAGE NUMBER------------------------------------------------
        switch($page_number_validity)
        {
            case \Core\Utils\Security::ERROR_INTEGER_IS_EMPTY:
            case \Core\Utils\Security::NO_ERROR;
                break;
            default:
                return self::ERROR_PAGE_NUMBER_IS_INVALID;
        }

        return self::NO_ERROR;
    }

    /**
     * This function validates and sanitizes the values passed to self::search
     *
     * @param array $parameters The contents of $_GET
     * @returns self::NO_ERROR on success, any of the ERROR constants on failure
     */
    private static function validateSearch(&$parameters)
    {
        // ---------------------------REMOVE UNSUPPORTED PARAMETERS------------------------------
        $parameters = array_intersect_key($parameters, array(   'query'             => NULL,
            'city'             => NULL,
            'page'              => NULL));

        // Validate and sanitize all parameters...
        // =======================================
        $search_query_validity  = \Modules\Humpchies\AdModel::isSearchQueryInputValid($parameters['query']);
        $page_number_validity   = \Core\Utils\Security::isIntegerInputValid($parameters['page']);

        // ---------------------------SEARCH QUERY-----------------------------------------------
        switch($search_query_validity)
        {
            case \Modules\Humpchies\AdModel::NO_ERROR:
                break;
            case \Modules\Humpchies\AdModel::ERROR_SEARCH_QUERY_IS_INVALID:
                return self::ERROR_SEARCH_QUERY_IS_INVALID;
                break;
            case \Modules\Humpchies\AdModel::ERROR_SEARCH_QUERY_IS_EMPTY:
                return self::ERROR_SEARCH_QUERY_IS_EMPTY;
                break;
            case \Modules\Humpchies\AdModel::ERROR_SEARCH_QUERY_IS_TOO_LONG:
                return self::ERROR_SEARCH_QUERY_IS_TOO_LONG;
                break;
            case \Modules\Humpchies\AdModel::ERROR_SEARCH_QUERY_IS_TOO_SHORT:
                return self::ERROR_SEARCH_QUERY_IS_TOO_SHORT;
                break;
            default:
                return $search_query_validity;
                break;
        }

        // ---------------------------PAGE NUMBER------------------------------------------------
        switch($page_number_validity)
        {
            case \Core\Utils\Security::ERROR_INTEGER_IS_EMPTY:
            case \Core\Utils\Security::NO_ERROR;
                break;
            default:
                return self::ERROR_PAGE_NUMBER_IS_INVALID;
        }

        return self::NO_ERROR;
    }

    private static function validateBrowseMyListings(&$parameters)
    {
        // ---------------------------REMOVE UNSUPPORTED PARAMETERS------------------------------
        $parameters = array_intersect_key($parameters, array('page' => NULL));

        // Validate and sanitize all parameters...
        // =======================================
        $page_number_validity = \Core\Utils\Security::isIntegerInputValid($parameters['page']);

        // ---------------------------PAGE NUMBER------------------------------------------------
        switch ($page_number_validity) {
            case \Core\Utils\Security::ERROR_INTEGER_IS_EMPTY:
            case \Core\Utils\Security::NO_ERROR;
                break;
            default:
                return self::ERROR_PAGE_NUMBER_IS_INVALID;
        }

        return self::NO_ERROR;
    }

     private static function validateNearby(&$parameters)
    {
        // ---------------------------REMOVE UNSUPPORTED PARAMETERS------------------------------
        $parameters = array_intersect_key($parameters, array( 
            'lat'               => NULL,
            'lng'               => NULL,
            'distance'          => NULL,
            'page'              => NULL,
            ));

        // Validate and sanitize all parameters...
        // =======================================
        $page_number_validity   = \Core\Utils\Security::isIntegerInputValid($parameters['page']);
        $distance_validity      = \Core\Utils\Security::isIntegerInputValid($parameters['distance']);
       
        // ---------------------------PAGE NUMBER------------------------------------------------
        switch($page_number_validity)
        {
            case \Core\Utils\Security::ERROR_INTEGER_IS_EMPTY:
            case \Core\Utils\Security::NO_ERROR;
                break;
            default:
                return self::ERROR_PAGE_NUMBER_IS_INVALID;
        }
        
        switch($distance_validity)
        {
            case \Core\Utils\Security::ERROR_INTEGER_IS_EMPTY:
            case \Core\Utils\Security::NO_ERROR;
                break;
            default:
                return self::ERROR_PAGE_NUMBER_IS_INVALID;
        }

        return self::NO_ERROR;
    }
    
    
    private static function getAdPostTimeSpan()
    {
        $account_date_created = \Core\Utils\Session::getCurrentUserCreationDate();

        if(!$account_date_created || (time() - $account_date_created) <= self::AD_POST_INITIAL_RESTRICTION_PERIOD)
            return self::MIN_AD_POST_TIME_SPAN_INITIAL_PERIOD;

        return self::MIN_AD_POST_TIME_SPAN;
    }

    private static function handlePreviouslyBanned($user_id)
    {
        \Modules\Humpchies\UserModel::bann($user_id);
        \Modules\Humpchies\AdModel::bannByUserId($user_id);

        session_destroy();
        header('Location: /');
        exit();
    }

    /**
     * Looks for applicable VIP Ads. If found, they will be appended to the current
     * Controller context.
     *
     * @param string $category one of the category constants in Modules\Humpchies\CategoryModel
     * @return bool TRUE if VIPs were found, and set. False otherwise.
     */
    private static function setupVIPAds($category = NULL)
    {
        // Assume we are unable to retrieve any VIPs
        $vipAdIds = NULL;


        // Get VIPs by the user's city choice. Fixme: using the get without country validation might break when we go world wide
        if(isset($_GET['city'])) {
            $vipAdIds = self::getCityVIPs($_GET['city'], $category);

        }
        
        if(isset($_GET['region'])) {
            $vipAdIds = self::getCityVIPs('', $category, $_GET['region']);

        }

        /*
         * Either the users hasn't chosen a city, or the city chosen by the user
         * isn't meant to display VIPs. Find out if the Geo Location of the user
         * is supposed to show VIPs
         */
        if(!$vipAdIds){
            $geoLocation = \Core\Utils\Session::getGeoLocation();

            // Disregard invalid Geo Locations
            if(\Core\Utils\Session::ERROR_GEO_LOCATION_IS_INVALID === $geoLocation) {
                return FALSE;
            }

            // Disregard Geo Locations Outside of Quebec
            $userIsInQuebec = FALSE;

            // Disregard anything for which we are unable to retrieve a state or province
            if(NULL === @$geoLocation->subdivisions) {
                return FALSE;
            }

            foreach($geoLocation->subdivisions as $subdivision) {
                if('QC' === $subdivision->iso_code && 'CA' === $geoLocation->country->iso_code) {
                    $userIsInQuebec = TRUE;
                }
            }

            if(!$userIsInQuebec) {
                return FALSE;
            }

            // Disregard Geo Locations for which a city could not be found
            if(NULL === @$geoLocation->city->names->en) {
                return FALSE;
            }

            // Get the applicable Geo Targeted VIPs
            $vipAdIds = self::getCityVIPs($geoLocation->city->names->en, $category);


        }

        /*
         * Stop: No VIPs were found either by the user's explicit city choice
         * or by Geo Targeting
         */

        if (!$vipAdIds) {
            return FALSE;
        }

        //if(isset($_COOKIE['vip_test'])) {
//			self::$data['vip_ads'] = $vipAdIds;


        /*}
        else{
            // Fetch VIP Ads
            self::$data['vip_ads'] = \Modules\Humpchies\AdModel::getVIPAds($vipAdIds);
        }*/
        // Stop: for some reason, we are unable to harvest the data
//        if(!self::$data['vip_ads']) {
//            return FALSE;
//        }

        /*
         * We are in business: We found all the components needed to display VIP ads
         * Prepare VIP ads for GUI... Parse the ads into humanly readable values...
         */
//        foreach(self::$data['vip_ads'] as &$ad) {
//            self::prepareListAdForGUI($ad);
//        }

        return TRUE;
    }
    /**
     * Looks for applicable premium Ads. If found, they will be appended to the current
     * Controller context.
     *
     * @param string $category one of the category constants in Modules\Humpchies\CategoryModel
     * @return bool TRUE if premiums were found, and set. False otherwise.
     */
    private static function setupPremiumAds($category = NULL)
    {
        // Assume we are unable to retrieve any premiums
        $premiumAdIds = NULL;

        // Get premiums by the user's city choice. Fixme: using the get without country validation might break when we go world wide
        if(isset($_GET['city'])) {
            $premiumAdIds = self::getCityPremiums($_GET['city'], $category);
        
        } elseif(isset($_GET['region'])) {
            $premiumAdIds = self::getCityPremiums('', $category, $_GET['region']);
        
        } else {

            /*
             * Either the users hasn't chosen a city, or the city chosen by the user
             * isn't meant to display Premiums. Find out if the Geo Location of the user
             * is supposed to show Premiums
             */

            $geoLocation = \Core\Utils\Session::getGeoLocation();
			// FOR LOCAL TESTING
            //$geoLocation = new \stdClass();
			//$geoLocation->city->names->en = 'montreal';
			if(\Core\Utils\Session::ERROR_GEO_LOCATION_IS_INVALID === $geoLocation) {
                return FALSE;
            }
            // Get the applicable Geo Targeted Premiums
            $premiumAdIds = self::getZonePremiums($geoLocation->city->names->en, $category);
        }

        /*
         * Stop: No VIPs were found either by the user's explicit city choice
         * or by Geo Targeting
         */

        if (!$premiumAdIds) {
            return FALSE;
        }

        //if(isset($_COOKIE['vip_test'])) {
//            self::$data['premium_ads'] = $premiumAdIds;



        // Stop: for some reason, we are unable to harvest the data
//        if(!self::$data['premium_ads']) {
//            return FALSE;
//        }

        /*
         * We are in business: We found all the components needed to display Premium ads
         * Prepare Premium ads for GUI... Parse the ads into humanly readable values...
         */

//        foreach(self::$data['premium_ads'] as &$ad) {
//            self::prepareListAdForGUI($ad);
//        }

        return TRUE;
    }

    /**
     * @param $cityName
     * @param $category
     * @return array|bool|mixed
     */
    private static function getCityVIPs($cityName, $category, $region = NULL)
    {
        
        switch(true) {
            
            case $cityName != '':
                $zoneName = \Modules\Humpchies\Utils::getDefinedZoneForCity($cityName);   

        if(!$zoneName) {
            return FALSE;
        }
                // return all vips by category and zone id
                return \Modules\Humpchies\AdModel::getVipAdsByCategoryAndZoneIds($zoneName, $category); 
                 
                break;
            
            case (!empty($region) && str_replace('-', "_", $region) == \Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO):    
                $zoneName = \Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO;
                // return all vips by category and zone id
                return \Modules\Humpchies\AdModel::getVipAdsByCategoryAndZoneIds($zoneName, $category); 
                
                break;
            
            case (!empty($region) && $region == 'centre-du-quebec'):    
                $zoneName = \Modules\Humpchies\Utils::GEO_ZONE_QUEBEC_CENTRE;
                // return all vips by category and zone id
                return \Modules\Humpchies\AdModel::getVipAdsByCategoryAndZoneIds($zoneName, $category); 
                
                break;
            
             case (!empty($region) ):    
                $zoneName = false;
                // get region id
                $region_details = \Modules\Humpchies\RegionModel::getRegionDetails($region);
                //$cities = \Modules\Humpchies\CityModel::getAllCities($region_details['id']);
                
                return \Modules\Humpchies\AdModel::getVipAdsByCategoryAndRegionIds($region_details['id'], $category); 
                
                
//                var_dump($cities); exit();
                break;
            
            default:
                return FALSE;
                break;    
            
        }
        
        
        
       // if($cityName != '') {     // selected city
//            $zoneName = \Modules\Humpchies\Utils::getDefinedZoneForCity($cityName);    
//        } else
//        
//        // only motreal metro area
//        if(!empty($region) && str_replace('-', "_", $region) == \Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO) {
//              $zoneName = \Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO;
//        }
//        
//        // any other region, now mtl metro
        
        
//        var_dump($zoneName);exit();

       // if(!$zoneName) {
//            return FALSE;
//        }
//        //if(isset($_COOKIE['vip_test'])) {
//        return \Modules\Humpchies\AdModel::getVipAdsByCategoryAndZoneIds($zoneName, $category);
        //}
        //switch($zoneName) {
//            case \Modules\Humpchies\Utils::GEO_ZONE_EASTERN_TOWNSHIPS:
//                return self::getVIPZoneAdIds(\Modules\Humpchies\Utils::GEO_ZONE_EASTERN_TOWNSHIPS);
//                break;
//            case \Modules\Humpchies\Utils::GEO_ZONE_QUEBEC_CENTRE:
//                return self::getVIPZoneAdIds(\Modules\Humpchies\Utils::GEO_ZONE_QUEBEC_CENTRE);
//                break;
//            case \Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO:
//                if(\Modules\Humpchies\CategoryModel::CATEGORY_ESCORT === $category){
//                    return self::getVIPZoneAdIds(\Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO . '_category_' . \Modules\Humpchies\CategoryModel::CATEGORY_ESCORT);
//                }elseif(\Modules\Humpchies\CategoryModel::CATEGORY_ESCORT_AGENCY === $category){
//                    return self::getVIPZoneAdIds(\Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO . '_category_' . \Modules\Humpchies\CategoryModel::CATEGORY_ESCORT_AGENCY);
//                }elseif(\Modules\Humpchies\CategoryModel::CATEGORY_EROTIC_MASSAGE === $category){
//                    return self::getVIPZoneAdIds(\Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO . '_category_' . \Modules\Humpchies\CategoryModel::CATEGORY_EROTIC_MASSAGE);
//                }elseif(\Modules\Humpchies\CategoryModel::CATEGORY_EROTIC_MASSAGE_PARLOR === $category){
//                    return self::getVIPZoneAdIds(\Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO . '_category_' . \Modules\Humpchies\CategoryModel::CATEGORY_EROTIC_MASSAGE_PARLOR);
//                }else{
//                    return self::getVIPZoneAdIds(\Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO);
//                }
//                break;
//            default:
//                return FALSE;
//
//        }
    }

    private static function getCityPremiums($cityName, $category, $region = NULL)
    {
        if($cityName != '')
        return \Modules\Humpchies\AdModel::getPremiumAdsByCategoryAndCityIDS($cityName, $category);
        else {
            $region_details = \Modules\Humpchies\RegionModel::getRegionDetails($region);
            return \Modules\Humpchies\AdModel::getPremiumAdsByCategoryAndRegionId($region_details['id'], $category);
        }    
    }

    private static function getZonePremiums($cityName, $category)
    {
        $zoneName = \Modules\Humpchies\Utils::getDefinedZoneForCity($cityName);
        if (!$zoneName){
            return [];
        }

        return \Modules\Humpchies\AdModel::getPremiumAdsByCategoryAndZoneIds($zoneName, $category);
    }


    /**
     * @param null $zone
     * @return array|mixed
     */
    private static function getVIPZoneAdIds($zone = NULL)
    {
        $vipZones = \Core\Utils\Config::getConfigValue("\Modules\Humpchies\Vips::zones");

        if(NULL !== $zone) {
            return @$vipZones[$zone];
        }

        $returnValues = array();

        foreach($vipZones as $key => $value) {
            $returnValues = array_merge($returnValues, $value);
        }

        return $returnValues;
    }

    public static function photos_ma() {
        self::disableCache();
        $user_id = \Core\Utils\Session::getCurrentUserID();
        $message ='';
        $success = 1;
        //var_dump($user_id);
         
        if (!$user_id){
            $message = "[ERROR-SOMETHING-WENT-WRONG]";
            $success = 0;
        }
        
        if (empty($_POST)){
            $message = "[ERROR-SOMETHING-WENT-WRONG]";
            $success = 0;
            $action = '';
        }else{
            $action = $_POST['a'];
        }

        $photoIds = $_POST['photo_id'];
        $ad_id= $_POST['ad_id'];

        $canUpdate = \Modules\Humpchies\AdModel::canUpdateAd($user_id, $ad_id);
        if (!$canUpdate){
            $action = '';
        }
        
       
        
        $result = array(
            'success' => $success,
            'error' => $message,
        );
        switch ($action) {
            case "upload":
                $images = \Modules\Humpchies\AdImageModel::getImages($ad_id);
                if(! $images){
                   $images = []; 
                }
                
                $adFields = array('id'=>$ad_id);
                $postedImages = array();
                foreach ($_FILES['images']['name'] as $i => $imageName){
                    $postedImages[$i] = array(
                        'name'=>$imageName,
                        'size'=>$_FILES['images']['size'][$i],
                        'type'=>$_FILES['images']['type'][$i],
                        'tmp_name'=>$_FILES['images']['tmp_name'][$i],
                        'error'=>$_FILES['images']['error'][$i],
                    );
                }
                
                $images_validity = \Core\Utils\Security::areJPEGImagesInputValid($postedImages);
                if (count($images) + count($postedImages) > 5){
                    $message = "[ERROR-IMAGES-MAX-COUNT-LIMITED]";
                    $success = 0;
                }

                switch($images_validity)
                {
                    case \Core\Utils\Security::NO_ERROR:
                        break;
                    case \Core\Utils\Security::ERROR_IMAGE_SIZE_IS_TOO_BIG:
                        $message = "[ERROR-IMAGE-IS-TOO-BIG]";
                        $success = 0;
                        break;
                    case \Core\Utils\Security::ERROR_IMAGE_TYPE_IS_INVALID:
                        $message = "[ERROR-IMAGE-TYPE-IS-INVALID]";
                        $success = 0;
                        break;
                    case \Core\Utils\Security::ERROR_IMAGE_ERROR_UNHANDLED:
                        $message = "[ERROR-IMAGE-RESAMPLING-FAILED]";
                        $success = 0;
                        break;
                    case \Core\Utils\Security::ERROR_IMAGE_RESAMPLING_FAILED:
                        $message = "[ERROR_IMAGE_ERROR_UNHANDLED]";
                        $success = 0;
                        break;
                    default:
                        if (is_array($images_validity) && !empty($images_validity['msg'])){
                            $message = $images_validity['msg'];
                        }else{
                            $message = "[ERROR-SOMETHING-WENT-WRONG]";
                        }
                        $success = 0;
                        break;
                }

                if ($message){
                    break;
                }

                $user_auto_approval = \Modules\Humpchies\UserModel::getUserAutoApprovalById( $user_id );
                if(!intval($user_auto_approval['auto_approval'])){
                    $saving_images_result = \Modules\Humpchies\AdImageModel::savePostedImagesV2($adFields, $postedImages, 1);
                } else {
            
                    $saving_images_result = \Modules\Humpchies\AdImageModel::savePostedImagesV2($adFields, $postedImages);
                }
               
                $ad_id = $_POST['ad_id'];
                
                
                unset($_POST['ad_id']);

                switch($saving_images_result)
                {
                    case \Modules\Humpchies\AdImageModel::NO_ERROR:
                        $result['images'] =  $postedImages['data'];

                        $result['count'] = count($images);
                        unset($postedImages['data']);
                        \Core\Cache::save(\Core\Utils\Device::getDeviceIp(), 1, self::getAdPostTimeSpan());
                        \Modules\Humpchies\UserModel::trackIp($user_id, \Core\Utils\Device::getDeviceIp());
                        
                        $editPOST['verification_status'] = 'updated';   
                        
                        $edit_add_result = \Modules\Humpchies\AdModel::edit($ad_id, $user_id, $editPOST);
                        $last = \Modules\Humpchies\AdModel::getVerificationLast($ad_id);
                        $verificationFields = array(
                          'ad_id' => $ad_id,
                          'user_id' => $user_id,
                          'verification_date' => date("Y-m-d H:i:s"),
                          'images'  => $last['images']
                        );
                        if($last['verification_status']=='approved'){
                            \Modules\Humpchies\AdModel::adVerificationCreate($verificationFields);
                        }
                        break;
                    case \Modules\Humpchies\AdImageModel::ERROR_SOME_IMAGES_COULD_NOT_BE_SAVED:
                        $message = '[ERROR-SOME-IMAGES-FAILED-TO-BE-SAVED]';
                        $success = 0;
                        break;
                    default:
                        $message = "[ERROR-SOME-IMAGES-FAILED-TO-BE-SAVED]: $saving_images_result";
                        $success = 0;
                        break;
                }

                break;
            case "sort":
                if (empty($photoIds)){
                    $message = "[ERROR-CHOOSE-IMAGE]";
                    break;
                }
                if (!AdImageModel::canProcessImages($photoIds, $ad_id)){
                    $message = "[ERROR-SOMETHING-WENT-WRONG]";
                    $success = 0;
                    break;
                }
                AdImageModel::reorder($photoIds);
                // make first image main
                AdImageModel::setMain($photoIds[0], $ad_id);
                
                
                break;
            case "set_main":
                if (empty($photoIds)){
                    $message = "[ERROR-CHOOSE-IMAGE]";
                    $success = 0;
                    break;
                }
                if (count($photoIds) > 1){
                    $message = "[ERROR-CHOOSE-IMAGE]";
                    $success = 0;
                    break;
                }
                if (!AdImageModel::canProcessImages($photoIds, $ad_id)){
                    $message = "[ERROR-SOMETHING-WENT-WRONG]";
                    $success = 0;
                    break;
                }
                AdImageModel::setMain($photoIds, $ad_id);
                break;
            case "delete":
                
                // clear array
                $photoIds = array_filter($photoIds, function($val) {
                    return is_numeric($val);
                });
                
                
                if (empty($photoIds)){
                    $message = "[ERROR-CHOOSE-IMAGE]";
                    $success = 0;
                    break;
                }
                $can_process = AdImageModel::canProcessImages($photoIds, $ad_id);
                
                
                if (!AdImageModel::canProcessImages($photoIds, $ad_id)){
                    $message = "[ERROR-SOMETHING-WENT-WRONG]";
                    $success = 0;
                    break;
                }

                \Modules\Humpchies\AdImageModel::delete($photoIds, $ad_id);
                $mainImage = \Modules\Humpchies\AdImageModel::getMainImage($ad_id);
                $result['main'] = $mainImage['id'];
                $images = \Modules\Humpchies\AdImageModel::getImages($ad_id);
                $result['count'] = ($images)? count($images) : 0;
                break;
            default:
                $message = "[ERROR-SOMETHING-WENT-WRONG]";
                $success = 0;
                break;
        }
        if ($message){
            \Core\Utils\Text::translateBufferTokens($message,
                \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'),
                \Core\Utils\Session::getLocale(),
                \Core\Utils\Config::getConfigValue('\Application\Settings::locales_module',
                    FALSE));
        }
        $result['error'] = $message;
        $result['success'] = $success;
        echo  json_encode($result); die;
    }

    public function addSocialShare()
    {
        if (empty($_POST)){
            echo  json_encode(array('status' => 'error')); die;
        }else{
            $platform   = \Modules\Humpchies\SocialShareModel::$platforms[$_POST['platform']];
            $type   = \Modules\Humpchies\SocialShareModel::$types[$_POST['type']];
            if($platform && $type){
                \Modules\Humpchies\SocialShareModel::updateOrCreate($platform, $type);
                echo  json_encode(array('status' => 'success')); die;
            }else{
                echo  json_encode(array('status' => 'error')); die;
            }
        }
    }
    private static function userIP(){
        //$ip = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
        
        if(filter_var($_SERVER["HTTP_X_FORWARDED_FOR"], FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE)){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR']; 
            return $ip;
        }
        if(filter_var($_SERVER['HTTP_INCAP_CLIENT_IP'], FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE)){
            $ip = $_SERVER['HTTP_INCAP_CLIENT_IP']; 
            return $ip;
        } 
         
        if(filter_var($_SERVER["REMOTE_ADDR"], FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE)){
            $ip = $_SERVER['REMOTE_ADDR']; 
            return $ip;
        }    
        
        //return "24.114.84.237"; //to be removed on live sever 
    }
    
    public static function getCityPremiumAds($city){
        $list = \Core\library\Redis::get(sprintf(self::REDIS_CITY_PREMIUM, $city));
        //var_dump(sprintf(self::REDIS_CITY_PREMIUM, $city));
       
        if(!$list){
            $list = \Modules\Humpchies\AdModel::getPremiumAdsByCity($city);
            \Core\library\Redis::set(sprintf(self::REDIS_CITY_PREMIUM, $city), serialize($list), 1800);
            self::updateCityShownAds($city, $list);
            
        }else{
            $list = unserialize($list); 
        }
        return $list;
    }
    
    public static function updateCityShownAds($city, $list){
        if(!$list) return false;
        $redisKey = sprintf(self::REDIS_CITY_SHOWN, $city);
        
        $makeExpire = (!\Core\library\Redis::exists($redisKey))? true : false;
        
        foreach($list as $ad){
            \Core\library\Redis::zadd($redisKey, "I" . $ad['id'], 1, ['NX']);
        }    
        if($makeExpire){
            \Core\library\Redis::ttl($redisKey, 86400);
        }
    } 
    
    public static function getMoreAds($city){
        
        $redisPremium = sprintf(self::REDIS_CITY_PREMIUM, $city); 
        $redisAdsShow = sprintf(self::REDIS_CITY_SHOWN  , $city); 
        
        
        $list = self::getCityPremiumAds($city);
        
        if(!\Core\library\Redis::exists($redisAdsShow)){
            self::updateCityShownAds($city, $list);
        }
        $showList = \Core\library\Redis::zRange($redisAdsShow, 0,3); 
        if(!is_array($showList)){
            $showList = [];
        }
        self::updateAdsCount($city, $showList);
        array_walk($showList, function(&$ad) {
            $ad = str_replace('I','',$ad);
        });
       
        if(is_array($showList) && is_array($list)){

            $modeAds = array_filter($list, function($item) use($showList){
                return (in_array($item['id'], $showList));    
            }); 
            
            if(count($modeAds) < 4){
                 $additionals =  array_filter($list, function($item) use($showList){
                    return (!in_array($item['id'], $showList));    
                 });
                 
                 $contor = 0 ;
                 $filler = [];
                 while( (count($additionals) > (4 - count($modeAds))) && count($modeAds) < 4 && $contor < 5  && count($additionals) > 0 ){
                    $contor ++;
                    $fill_ad = rand(0, $additionals);
                    $modeAds[] = $additionals[$fill_ad]; 
                    unset($additionals[$fill_ad]);
                    $additionals = array_values($additionals);
                 } 
            }
        
        }
        return $modeAds;
       // var_dump($modeAds);
//        die();         
    }
   
    public static function updateAdsCount($city, $showList){
        
        $redisAdsShow = sprintf(self::REDIS_CITY_SHOWN  , $city);
        foreach($showList as $id){
            \Core\library\Redis::zadd($redisAdsShow, $id, 1, ['INCR']);
        }    
        
    }
    
    public static function ajaxcheck(){
        $phone = \Modules\Humpchies\AdModel::getAdPhoneValidation(intval($_GET['id']), \Core\Utils\Session::getCurrentUserID()); 
        $result  = ['valid' => $phone['phone_validated']];
       // $result  = ['valid' => "Y"];
        echo json_encode($result); 
        die();
    }
    
    private static function imgResize($file, $width, $height){
        $fisier = explode(".", $file);
        $ext = end($fisier);
        array_pop($fisier);
        $fisier = implode(".", $fisier);
        switch(strtolower($ext)){
            case "jpeg": $img = imagecreatefromjpeg($file); break;
            case "jpg" : $img = imagecreatefromjpeg($file); break;
            case "png" : $img = imagecreatefrompng($file); break;
            case "gif" : $img = imagecreatefromgif($file); break;
            
        }
        //echo $file;
//        var_dump(getimagesize($file));
        list($width_orig, $height_orig) = getimagesize($file);

        $ratio_orig = $width_orig/$height_orig;

        if ($width/$height > $ratio_orig) {
           $width = intval($height*$ratio_orig);
        } else {
           $height = intval($width/$ratio_orig);
        }

        // Resample
        $image_p = imagecreatetruecolor($width, $height);
        imagecopyresampled($image_p, $img, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        // Output
        imagejpeg($image_p, $fisier . ".jpg", 100);
        return $fisier . ".jpg";
    }
    
    private static function getCloseAds($id, $category, $city) {
        
        $redisKey = sprintf(self::REDIS_CITY_CATEGORY_ORDER, $category, $city);
        
        //$orderList = \Core\library\Redis::del($redisKey);
        $orderList = \Core\library\Redis::get($redisKey);
       
        
        if(!empty($orderList)){
            $orderList = json_decode($orderList);
        }else{
            $orderList = ['recordset' => [], 'count' => false]; 
        }
        $remake = false;
        
        if(!empty($orderList['recordset']) && !in_array($id, $orderList['recordset']) && max($orderList['recordset']) < $id ){
            $orderList = ['recordset' => [], 'count' => false];
            \Core\library\Redis::del($redisKey);
        }
        
        while( !in_array($id, $orderList['recordset'])){
         
           $start = intval(count($orderList['recordset']));
            
           $adsList = \Modules\Humpchies\AdModel::getOrderOfActiveAdsByCategoryAndCity($category, $city, $start, 1000);
        
           if($adsList['count']){
               $orderList["count"] = $adsList['count'];
           }
           
           $orderList["recordset"] = array_merge($orderList["recordset"], $adsList["recordset"]);
           
           if($orderList["count"] <= count($orderList["recordset"])){
               break;
           }
        }
        //echo "-------------";
          //var_dump($orderList); 
//          die();
        if($remake){
            \Core\library\Redis::set($redisKey, json_encode($orderList));
            \Core\library\Redis::ttl($redisKey, 3600);
        }
        
        if(!in_array($id, $orderList['recordset'])){
            return false;    
        }
       // echo $id."----";
        $loc = array_search($id, $orderList['recordset'], false);

        $prev = ($loc)?$orderList['recordset'][$loc - 1] : false;
        $next = ($loc < $orderList["count"] )?$orderList['recordset'][$loc + 1] : false;
        return ['prev' => $prev, 'next' => $next ];
        
       // if(empty($orderList) || !in_array($id, $orderList['recordset'])){
//            if(isset($orderList['recordset'])){
//                $start = count($orderList['recordset']);
//                $adsList = \Modules\Humpchies\AdModel::getOrderOfActiveAdsByCategoryAndCity($category, $city, $start, 1000) ;
//                $orderList["recordset"] = array_merge($orderList["recordset"], $adsList);
//            }else{
//                $start = 0;
//                $orderList = \Modules\Humpchies\AdModel::getOrderOfActiveAdsByCategoryAndCity($category, $city, 0, 1000);
//                
//                \Core\library\Redis::set($redisKey, json_encode($orderList));
//                \Core\library\Redis::ttl($redisKey, 3600);
//                
//            }         
//        }
        
        //$adsList = \Modules\Humpchies\AdModel::getOrderOfActiveAdsByCategoryAndCity($category, $city, 0, 1000) ;
         
        var_dump($orderList);
        
        die();
    }
    
    private static function fillAds( $category, $city, $last, $total){
         $start = intval($last);
         $adsList = \Modules\Humpchies\AdModel::getOrderOfActiveAdsByCategoryAndCity($category, $city, $start, 1000) ;
         $orderList["recordset"] = array_merge($orderList["recordset"], $adsList);
         
         
    }
    
    public static function video_upload(){
        
       $videos_path_temp  = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::videos_path')."temp/"; 
       $videos_path_logs  = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::videos_path')."log/"; 
       $videos_location  = \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::videos'); 
       //$file = "/code/applications/gui.humpchies.com/videos/temp20200928090th112332file_example_MOV_1920_2_2MB.mov";
       
       $user = \Core\Utils\Session::getCurrentUserID();
       if(!is_dir($videos_path_temp.$user)){
          mkdir($videos_path_temp.$user); 
       }
       if(!empty($_FILES['videoupload'])){ 
     
            // File upload configuration 
            //$targetDir = "uploads/"; 
            $allowTypes = array('mp4', 'webm', 'flv', 'vob', 'ogg', 'avi', 'mov', 'wmv', 'rm', 'm4p', 'm4v', 'mpg', 'mp2', 'mpeg', 'mpe', 'mpv'); 
             
            $fileName = basename($_FILES['videoupload']['name']);
            $randName = date("YmdHIS") . rand(100000,200000);
            $tempName =  $randName ."-". $fileName; 
           // die($videos_path_temp . $user . "/" . $tempName); 
            // Check whether file type is valid 
            //$fileType = pathinfo($_FILES['file']['tmp_name'], PATHINFO_EXTENSION); 
            //if(in_array($fileType, $allowTypes)){ 
                // Upload file to the server 
                if(move_uploaded_file($_FILES['videoupload']['tmp_name'], $videos_path_temp . $user . "/" . $tempName)){ 
                    $upload = 'ok'; 
                    
                    exec("ffmpeg -ss 00:00:01 -i " . $videos_path_temp . $user . "/" . $tempName . "   -vframes 1 " . $videos_path_temp. $user . "/" . $tempName .".jpg > /dev/null 2>&1 < /dev/null");
                    //exec("ffmpeg -ss 00:00:01 -i " . $videos_path_temp . $user . "/" . $tempName . "   -vframes 1 " . $videos_path_temp. $user . "/" . $tempName .".jpg ");
                    
                    $fields = [
                        "user_id"       => $user, 
                        "video_name"    => $tempName, 
                        "video_size"    => $_FILES['videoupload']['size'], 
                        "added"         => date("Y-m-d H:i:s")
                    ];
                    
                    $videoID = \Modules\Humpchies\AdModel::addUserVideo($fields);
                    
                    echo json_encode(['movie'=> $videos_location . "temp/" . $user . "/" . $tempName, 'vid' => $videoID ]);
                }else{
                    echo "MUMU";
                } 
            //} 
        }else{
            echo "MOMO2";
        }
        
       die();  
    }
    
    public static function video_delete(){
       $user = \Core\Utils\Session::getCurrentUserID();
       if(\Modules\Humpchies\AdModel::deleteUserVideobyID($user,$_GET['id'])){
           echo json_encode(['status' => 'success', 'deleted' => $_GET['id']]);
       }else{
           echo json_encode(['status' => 'error']);
       }
       die(); 
    }
}
?>
