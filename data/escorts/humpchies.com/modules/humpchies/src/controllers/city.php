<?php

Namespace Modules\Humpchies;

class CityController extends \Core\Controller{
    
    public static function search(){
        
        $result = [];
        $category_code = ($_POST['category_code'] != '')? $_POST['category_code'] : null;
        if(isset($_POST['keyword']) && $_POST['keyword'] != '') {
            $search_term = $_POST['keyword'];    
            $cities = \Modules\Humpchies\CityModel::getAllCities(null, $search_term); 
            
            if($cities) {
                foreach($cities as $key => $city_details) {
                    
                    $cities[$key]["city_url"] = \Modules\Humpchies\AdController::getListURL($category_code, $city_details['slug']);
                     
                } 
                $result = $cities;
                
            }
                
        }
        
        echo json_encode($result); 
        die();
    }
    
    
    public static function search_new(){
        
        $result = [];
        $category_code = ($_POST['category_code'] != '')? $_POST['category_code'] : null;
        $version = (isset($_POST['version']) && $_POST['version'] != '')? $_POST['version'] : null;
        
        if(isset($_POST['keyword']) && $_POST['keyword'] != '') {
            //$search_term = $_POST['keyword'];    
            $search_term = preg_replace("/[^a-zA-Z -]/", "", $_POST['keyword']);
            $final_search_term = preg_replace('/\bst\b/i', 'saint', strtolower($search_term));
            
            $cities = \Modules\Humpchies\CityModel::getAllCities(null, $final_search_term); 
            
            if($cities) {
                foreach($cities as $key => $city_details) {
                    if($version == 2)
                        $cities[$key]["city_url"] = \Modules\Humpchies\AdController::getListURLV2($category_code, $city_details['slug']);
                    else
                    $cities[$key]["city_url"] = \Modules\Humpchies\AdController::getListURL($category_code, $city_details['slug']);
                } 
                $result = $cities;
                
            }
            if(isset($_POST['region']) && $_POST['region'] == 1) {
                
                $regions =  \Modules\Humpchies\RegionModel::getSearchRegions($final_search_term);
                if($regions) {
                    foreach($regions as $key => $region_details) {
                        if($version == 2)
                            $region_details["city_url"] = \Modules\Humpchies\AdController::getRegionUrlV2($category_code, $region_details['slug']);
                        else    
                        $region_details["city_url"] = \Modules\Humpchies\AdController::getRegionUrl($category_code, $region_details['slug']);
                       
                        $region_details["is_region"] = true;
                        
                        array_push($result, $region_details);
                    }
                } 
                //var_dump($result);  
            }
            
                
        }
        //\Core\library\log::debug($result, "CITY SEARCH RESULT");
        //header('Content-Type: application/json');
        echo json_encode($result);
        //\Core\library\log::debug(json_encode($result), "CITY SEARCH RESULT");
        exit();
        
        //echo json_encode($result); 
//        die();
    }
    
    
}
?>
