<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: his class exists to act as a centralized handler for
 * redirections needed across the site to handle legacy URLS as we move
 * frameworks and sites.
 */

Namespace Modules\Humpchies;

class RedirectionController extends \Core\Controller{

    public static function adDetails()
    {
        if( \Core\Utils\Security::isIdInputValid($_GET['id']) !== \Core\Utils\Security::NO_ERROR            // Validate the id as being an integer >= 1
            || 
            (self::$data['ad'] = \Modules\Humpchies\AdModel::getDetailsOfActiveAd($_GET['id'])) === FALSE)  // Make sure the ID actually returns an ad
            {
                \Core\Router::throw404();                                                                   // Failure of any of the above conditions triggers a 404
                return FALSE;                                                                               // we then advise the calling context of the failure here    
            } 
    
        self::$data['ad'] = array_shift(self::$data['ad']);
        
        self::redirect(self::getRedirectionLocalizedBaseURL() . ltrim(\Modules\Humpchies\AdController::getDetailsURL(self::$data['ad']['category'], \Core\Utils\Text::stringToUrl(self::$data['ad']['city']), self::$data['ad']['id'], self::$data['ad']['title']), '/'));
    }
    
    public static function frenchSearch()
    {
        self::redirect(self::getRedirectionLocalizedBaseURL() . 'ad/search?query=' . $_GET['mots_clef'] . (@$_GET['page'] === NULL ? '' : '&page=' . $_GET['page']));    
    }
    
    public static function englishSearch()
    {
        self::redirect(self::getRedirectionLocalizedBaseURL() . 'ad/search?query=' . $_GET['query'] . (@$_GET['page'] === NULL ? '' : '&page=' . $_GET['page']));    
    }
    
    public static function adListingFrench()
    { 
        $categories = array('escort'                    => 'escorte', 
                            'escort-agency'             => 'agence-escorte', 
                            'erotic-massage'            => 'massage-erotique', 
                            'erotic-massage-parlor'     => 'salon-massage-erotique');
                            
        if(($category = array_search($_GET['categorie'], $categories)) === FALSE)
        {
            \Core\Router::throw404();                                                            
            return FALSE;
        }
            
        $base_url_for_redirection = self::getRedirectionLocalizedBaseURL() . 'ad/listing/category/' . $category;
        
        $x = (@$_GET['ville'] === NULL ? 0 : 1);
        $y = (@$_GET['page'] === NULL ? 0 : 2);
        
        switch(($x + $y))
        {
            // City and page
            case 3:
                if( \Modules\Humpchies\CityModel::isCityWithAdsInputValid($_GET['ville'], $category) !== \Modules\Humpchies\CityModel::NO_ERROR 
                    ||
                    \Core\Utils\Security::isIntegerInputValid($_GET['page']) !== \Core\Utils\Security::NO_ERROR)
                {
                    \Core\Router::throw404();                                                            
                    return FALSE;
                }
                
                $base_url_for_redirection .= '/city/' . $_GET['ville'] . '/page/' . $_GET["page"];
                    
                break;
                
            // Page only
            case 2:
                if(\Core\Utils\Security::isIntegerInputValid($_GET['page']) !== \Core\Utils\Security::NO_ERROR)
                {
                    \Core\Router::throw404();                                                            
                    return FALSE;
                }
                
                $base_url_for_redirection .= '/page/' . $_GET["page"];
                
                break;
                
            // City only
            case 1:
                if(\Modules\Humpchies\CityModel::isCityWithAdsInputValid($_GET['ville'], $category) !== \Modules\Humpchies\CityModel::NO_ERROR)
                {
                    \Core\Router::throw404();                                                            
                    return FALSE;
                }
                
                $base_url_for_redirection .= '/city/' . $_GET['ville'];
                
                break;
            default:
                break;
        }
        
        self::redirect($base_url_for_redirection); 
    }
    
    public static function adListingEnglish()
    {                  
        if(in_array($_GET['category'], array('escort', 'escort-agency', 'erotic-massage', 'erotic-massage-parlor')) === FALSE)
        {
            \Core\Router::throw404();                                                            
            return FALSE;
        }
            
        $base_url_for_redirection = self::getRedirectionLocalizedBaseURL() . 'ad/listing/category/' . $_GET['category'];
        
        $x = (@$_GET['city'] === NULL ? 0 : 1);
        $y = (@$_GET['page'] === NULL ? 0 : 2);
        
        switch(($x + $y))
        {
            // City and page
            case 3:
                if( \Modules\Humpchies\CityModel::isCityWithAdsInputValid($_GET['city'], $category) !== \Modules\Humpchies\CityModel::NO_ERROR 
                    ||
                    \Core\Utils\Security::isIntegerInputValid($_GET['page']) !== \Core\Utils\Security::NO_ERROR)
                {
                    \Core\Router::throw404();                                                            
                    return FALSE;
                }
                
                $base_url_for_redirection .= '/city/' . $_GET['city'] . '/page/' . $_GET['page'];
                    
                break;
                
            // Page only
            case 2:
                if(\Core\Utils\Security::isIntegerInputValid($_GET['page']) !== \Core\Utils\Security::NO_ERROR)
                {
                    \Core\Router::throw404();                                                            
                    return FALSE;
                }
                
                $base_url_for_redirection .= '/page/' . $_GET['page'];
                
                break;
                
            // City only
            case 1:
                if(\Modules\Humpchies\CityModel::isCityWithAdsInputValid($_GET['city'], $category) !== \Modules\Humpchies\CityModel::NO_ERROR)
                {
                    \Core\Router::throw404();                                                            
                    return FALSE;
                }
                
                $base_url_for_redirection .= '/city/' . $_GET['city'];
                
                break;
            default:
                break;
        }
        
        self::redirect($base_url_for_redirection); 
    }

    public static function fullListing()
    {
        $base_url_for_redirection = self::getRedirectionLocalizedBaseURL() . 'ad/listing';
        
        if(@$_GET['page'] !== NULL )
        {
            if(\Core\Utils\Security::isIntegerInputValid($_GET['page']) !== \Core\Utils\Security::NO_ERROR)
            {
                \Core\Router::throw404();                                                            
                return FALSE;   
            }
            else
            {
                $base_url_for_redirection .= '/page/' . $_GET['page'];   
            }
        }
        
        self::redirect($base_url_for_redirection);    
    }
    
    public static function siteRoot()
    {
        self::redirect(self::getRedirectionLocalizedBaseURL());   
    }
    
    public static function myAds()
    {
        self::redirect(self::getRedirectionLocalizedBaseURL() . 'ad/my-listings');    
    }
    
    public static function repostAd()
    {
        self::siteRoot();  
    }
    
    public static function stopAd()
    {
        self::siteRoot();  
    }
    
    public static function postAd()
    {
        self::redirect(self::getRedirectionLocalizedBaseURL() . 'ad/create');   
    }
    
    public static function siteMap()
    {
        self::redirect(self::getRedirectionLocalizedBaseURL() . 'site-map');    
    }
    
    public static function rss()
    {
        self::redirect(self::getRedirectionLocalizedBaseURL() . 'rss');    
    }
    
    public static function contactUs()
    {
        self::redirect(self::getRedirectionLocalizedBaseURL() . 'contact-us');    
    }
    
    public static function signUp()
    {
        self::redirect(self::getRedirectionLocalizedBaseURL() . 'user/signup');    
    }
    
    public static function accountActivation()
    {
        self::siteRoot();    
    }
    
    public static function login()
    {
        self::redirect(self::getRedirectionLocalizedBaseURL() . 'user/login');    
    }
    
    public static function logout()
    {
        \Core\Utils\Session::abandon();
        self::redirect(self::getRedirectionLocalizedBaseURL() . 'user/logout');    
    }
    
    public static function captcha()
    {
        self::redirect(self::getRedirectionLocalizedBaseURL() . 'captcha.gif');    
    }
    
    public static function bingSiteAuthXml()
    {
        self::redirect(self::getRedirectionBaseURL() . 'BingSiteAuth.xml');   
    }
    
    public static function googleVerification()
    {
        self::redirect(self::getRedirectionBaseURL() . 'google1da93fedf20afa03.html');    
    }
    
    public static function meta()
    {
        self::redirect(self::getRedirectionBaseURL() . 'meta.txt');    
    }
    
    public static function robots()
    {
        self::redirect(self::getRedirectionBaseURL() . 'robots.txt');    
    }
    
    public static function siteMapXml()
    {
        self::redirect(self::getRedirectionBaseURL() . 'sitemap.xml');    
    }

    public static function index()
    {
        self::siteRoot();
    }
    
    public static function notFound()
    {
        self::siteRoot();
    }

    /***
    * put your comment there...
    * 
    */
    public static function privacy()
    {
        self::redirect(self::getRedirectionLocalizedBaseURL() . 'privacy-policy');
    }
    
    /***
    * put your comment there...
    * 
    */
    public static function termsOfService()
    {
        self::redirect(self::getRedirectionLocalizedBaseURL() . 'terms-and-conditions');
    }
    
    /**
    * put your comment there...
    * 
    */
    private static function getRedirectionBaseURL()
    {
        $base_url = \Core\Utils\Config::getConfigValue('\Application\Settings::redirection_base_url');
        
        return ($base_url === NULL ? '/' : $base_url);  
    }        
    
    /**
    * put your comment there...
    * 
    */
    private static function getRedirectionLocalizedBaseURL()
    {
        $base_url = \Core\Utils\Config::getConfigValue('\Application\Settings::redirection_localized_base_url');
        
        return ($base_url === NULL ? '/' : $base_url);  
    }     
}
?>