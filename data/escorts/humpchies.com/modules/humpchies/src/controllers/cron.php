<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class is used to run CRON jobs related to humpchies.
 */

Namespace Modules\Humpchies;

class CronController{
    
    /**
    * This function deletes old(90 days) ads from humpchies... 5 ads at the time.                                                      
    */
    public static function tagOldAdsForDeletion()
    {
		
        //return;
		$premium_orders_concated = \Core\DBConn::select(  \Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG,
                                                    'SELECT
														GROUP_CONCAT(ads_id) as premium_ids
                                                    FROM 
														order_packages    
                                                    WHERE 
                                                        status = 2
													GROUP BY status	
                                                    ', TRUE);
		$premium_ids = '';
		if($premium_orders_concated[0]['premium_ids']){
			$premium_ids = '  AND a.id NOT IN (   '. $premium_orders_concated[0]['premium_ids'] . ')';
		}
		//var_dump($premium_ids);
		// Pick up the 10 oldest ads...
        $records_to_delete = \Core\DBConn::select(  \Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG,
                                                    'SELECT
														a.id,
														a.user_id
                                                    FROM 
														ads a
                                                    WHERE
                                                        a.date_added < ' . (string)(time() - 7776000) . ' and a.date_released < ' . (string)(time() - 7776000) . ' AND a.status = "active" '. $premium_ids . '
                                                    ORDER BY
                                                            a.date_added ASC
                                                    LIMIT 0, 20', TRUE);
 
        // Bail out if no records found for deletion...                                            
        if($records_to_delete === FALSE)  
        {
            echo 'No ads to delete...';
            exit();
        }
        
        // Loop through each old ad...
        foreach($records_to_delete as $ad_record)
        {
            echo "Tagging record id {$ad_record['id']}... for deletion\n";

			if(\Modules\Humpchies\AdModel::tagForDeletion($ad_record['id'], $ad_record['user_id']) === \Modules\Humpchies\AdModel::NO_ERROR)
                echo "Record succesfully tagged for deletion...\n";
            else
                echo 'Failed to tag ad for deletion: ' . \Core\DBConn::getLastError(\Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG, TRUE) . "...\n";
        }
    }

    /**
     * This function deletes deleted ads from humpchies... 25 ads at the time.
     */
    public static function deleteDeletedAds()
    {
        // Pick up the 25 of the oldest deleted ads...
        $records_to_delete = \Core\DBConn::select(  \Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG,
                                                    'SELECT
                                                            id,
                                                            city,
                                                            category,
                                                            title
                                                    FROM 
                                                            ads
                                                    WHERE
                                                        status = \'delete\'
                                                    ORDER BY
                                                            date_added ASC
                                                    LIMIT 0, 25', TRUE);

        // Bail out if no records found for deletion...
        if($records_to_delete === FALSE)
        {
            echo 'No ads to delete...';
            exit();
        }

        // Loop through each old ad...
        foreach($records_to_delete as $ad_record)
        {
            echo "Deleting record id {$ad_record['id']}...\n";

            // Get the ads's image directory...
            $images_directory = dirname(\Modules\Humpchies\AdImageModel::getImagePath($ad_record, 0));

            echo "Images directory: $images_directory...\n";

            // Delete all ad images...
            foreach(glob($images_directory . '/*') as $file)
            {
                echo "Deleting image: $file...\n";
                echo "Result: " . unlink($file) . "\n";
            }

            echo "Deleting images directory...\n";

            // Delete the ad images' directory...
            if(rmdir($images_directory) === TRUE)
                echo "Images directory succesfully deleted...\n";

            // Delete the ad from the DB...
            if(\Core\DBConn::delete(\Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG, \Modules\Humpchies\AdModel::DEFAULT_DB_TABLE, 'id = ' . $ad_record['id']) === 1)
                echo "Record succesfully deleted...\n";
            else
                echo "Failed to delete record: {\Core\DBConn::getLastError(\Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG, TRUE)}...\n";
        }
    }

    public static function deleteExpiredNonActivatedUserAccounts()
    {
		// 1 day 
        $rowsAffected = \Core\DBConn::update(   \Modules\Humpchies\UserModel::DEFAULT_DB_CONFIG,
                                                'users',
                                                array('date_banned' => '28'),
                                                'date_created < (UNIX_TIMESTAMP() - 86400) AND date_activated = 0 AND type = '.\Modules\Humpchies\UserModel::USER_TYPE_ADVERTISEMENT);

        echo "Rows affected: $rowsAffected\n";
    }

    public static function deleteStalledAccounts()
    {
		// 30 days 
        $rowsAffected = \Core\DBConn::update(   \Modules\Humpchies\UserModel::DEFAULT_DB_CONFIG,
                                                'users',
                                                array('date_banned' => '28'),
                                                'phone IS NULL AND date_created < (UNIX_TIMESTAMP() - 2592000 ) AND date_grace < UNIX_TIMESTAMP() AND date_banned = 0 AND type = '.\Modules\Humpchies\UserModel::USER_TYPE_ADVERTISEMENT);

        echo "Rows affected: $rowsAffected\n";
    }

    public static function getNewestPostInfo()
    {
        $affectedRecords = \Core\DBConn::select(  \Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG,
            "select 
                                                          users.id as 'user_id', 
                                                          users.phone as 'phone', 
                                                          count(users.id) as 'ad_count',
                                                          users.password as 'password',
                                                          users.phone as 'phone',
                                                          users.ip as 'ip'
                                                    from 
                                                          ads
                                                          inner join users on ads.user_id = users.id
                                                    where 
                                                          ads.status = 'new'
                                                          and
                                                          users.date_created >= (UNIX_TIMESTAMP() - (86400 * 10))
                                                    group by users.id      
                                                    order by ads.user_id DESC, ads.date_added;",
            TRUE);

        $userIds = null;

        foreach($affectedRecords as $adRecord) {
            /**
             * date_banned:
             *
             * 1 general
             * 2 stalled
             * 3 theft
             * 4 invalid phone
             * 5 male
             * 6 reputation
             * 7 explicit
             * 8 flooding
             * 9 fake pics
             * 10 fraud
             * 11 copyright
             * 12 shady
             * 14 promotes other sites
             * 15 Dicks
             * 16 Illegal shit
             * 17 Impersonation
             * 18 Remote
             * 19 potential minor
             * 20 potential human trafficking
             * 21 dushbag
             * 22 no initial pics
             * 23 Bypassing emojis
             * 24 Bypassing image checkup by orientation
             * 25 dushbag images
             * 26 disrespect
             * 27 Blacklisted ( Socially wrong - old name )
             */

            echo "Request:\n";
            echo str_pad('User ID', 10);
            echo str_pad('Phone ', 15);
            echo str_pad('Ad Count', 5);
            echo "\n";
            echo str_pad($adRecord['user_id'], 10);
            echo str_pad($adRecord['phone'], 15);
            echo str_pad($adRecord['ad_count'], 5);
            echo "\n\nRelated users:\n";

            $relatedUsers = \Core\DBConn::select(  \Modules\Humpchies\UserModel::DEFAULT_DB_CONFIG,
                                                    "select
                                                          *
                                                    from
                                                          users
                                                    where
                                                          password = '{$adRecord['password']}'
                                                          or 
                                                          phone = '{$adRecord['phone']}'
                                                          or
                                                          ip = {$adRecord['ip']}
                                                    order by
                                                          id ASC;",
                                                    TRUE);

            if ($relatedUsers) {
                echo str_pad('User ID', 11);
                echo str_pad('Username', 16);
                echo str_pad('Password', 35);
                echo str_pad('Email', 30);
                echo str_pad('Phone', 15);
                echo str_pad('Ip', 18);
                echo str_pad('Date Created', 22);
                echo str_pad('Date Activated', 22);
                echo str_pad('Date Banned', 11);
                echo "\n";

                foreach($relatedUsers as $relatedUser) {
                    echo str_pad($relatedUser['id'], 11);
                    echo str_pad($relatedUser['username'], 16);
                    echo str_pad($relatedUser['password'], 35);
                    echo str_pad($relatedUser['email'], 30);
                    echo str_pad($relatedUser['phone'], 15);
                    echo str_pad(long2ip($relatedUser['ip']), 18);
                    echo str_pad(date('Y-m-d H:i:s', $relatedUser['date_created']), 22);
                    echo str_pad(date('Y-m-d H:i:s', $relatedUser['date_activated']), 22);
                    echo str_pad($relatedUser['date_banned'], 11);
                    echo "\n";
                }
            } else {
                echo "n/a";
            }

            echo "\n\nRelated ads:\n";
            $userAds = \Core\DBConn::select(  \Modules\Humpchies\AdModel::DEFAULT_DB_CONFIG,
                                                    "select
                                                          id,
                                                          thumb_md5,
                                                          phone, 
                                                          city, 
                                                          category,
                                                          substring(title, 1, 25) as 'title', 
                                                          substring(replace(description, '\r\n', ' '), 1, 75) as 'description'
                                                    from
                                                          ads
                                                    where
                                                          user_id = '{$adRecord['user_id']}'
                                                    order by
                                                          id DESC;",
                                                    TRUE);

            echo str_pad('ID', 11);
            echo str_pad('Phone', 15);
            echo str_pad('City', 20);
            echo str_pad('Category', 15);
            echo str_pad('Title', 28);
            echo str_pad('Description', 75);
            echo "\n";

            foreach ($userAds as $ad) {
                echo str_pad($ad['id'] . ('b944a4e591ff62a08d691efba012fdff' === $ad['thumb_md5'] ? '*' : ''), 11);
                echo str_pad($ad['phone'], 15);
                echo str_pad($ad['city'], 20);
                echo str_pad($ad['category'], 15);
                echo str_pad($ad['title'], 28);
                echo str_pad($ad['description'], 75);
                echo "\n";
            }

            echo "\n\n";
        }
    }
}?>
