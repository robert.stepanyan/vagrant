<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class is the default class in the humpchies project,
 * as such it also contains generic or not data structure specific methods.
 */

Namespace Modules\Humpchies;

use Core\Utils\Image;

class FavoriteController extends \Core\Controller{

    const NO_ERROR                              = 'Modules.Humpchies.AdController.NoError';                             // Life is beautiful: no errors
    
    public static function add() {
        self::disableCache();
        $user_id = \Core\Utils\Session::getCurrentUserID();
        $message ='';
        $success = 1;
         
        if (!$user_id){
            $message = "[ERROR-SOMETHING-WENT-WRONG]";
            $success = 0;
        }
        
        if (empty($_GET)){
            $message = "[ERROR-SOMETHING-WENT-WRONG]";
            $success = 0;
            
        } else {
            $ad_id = $_GET['id'];
        }

        $user_favorite =  \Modules\Humpchies\UserFavoriteModel::create($user_id, $ad_id);
        
        if(!$user_favorite) {
            $message = "[ERROR-SOMETHING-WENT-WRONG]";
            $success = 0;    
        }
        
        $result = array(
            'success' => $success,
            'error' => $message,
        );
        
        if ($message){
            \Core\Utils\Text::translateBufferTokens($message,
                \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'),
                \Core\Utils\Session::getLocale(),
                \Core\Utils\Config::getConfigValue('\Application\Settings::locales_module',
                    FALSE));
        }
        $result['error'] = $message;
        $result['success'] = $success;
        echo  json_encode($result); die;
    }

    public static function remove() {
        self::disableCache();
        $user_id = \Core\Utils\Session::getCurrentUserID();
        $message ='';
        $success = 1;
         
        if (!$user_id){
            $message = "[ERROR-SOMETHING-WENT-WRONG]";
            $success = 0;
        }
        
        if (empty($_GET)){
            $message = "[ERROR-SOMETHING-WENT-WRONG]";
            $success = 0;
            
        } else {
            $ad_id = $_GET['id'];
        }

        $user_favorite =  \Modules\Humpchies\UserFavoriteModel::delete($user_id, $ad_id);
        
        if(!$user_favorite) {
            $message = "[ERROR-SOMETHING-WENT-WRONG]";
            $success = 0;    
        }
        
        $result = array(
            'success' => $success,
            'error' => $message,
        );
        
        if ($message){
            \Core\Utils\Text::translateBufferTokens($message,
                \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'),
                \Core\Utils\Session::getLocale(),
                \Core\Utils\Config::getConfigValue('\Application\Settings::locales_module',
                    FALSE));
        }
        $result['error'] = $message;
        $result['success'] = $success;
        echo  json_encode($result); die;
    }
}
?>
