<?php

Namespace Modules\Humpchies;


class CommentsController extends \Core\Controller
{

    const MAX_COMMENT_MESSAGE_LENGTH = 500;
    const MIN_COMMENT_MESSAGE_LENGTH = 10;

    const ERROR_MEMBER_ID_IS_INVALID = 'Modules.Humpchies.CommentsController.MemberIdIsInvalid';
    const ERROR_AD_ID_IS_INVALID = 'Modules.Humpchies.CommentsController.AdIdIsInvalid';
    const ERROR_COMMENT_IS_EMPTY = 'Modules.Humpchies.CommentsController.ErrorCommentIsEmpty'; // Comment text is empty
    const ERROR_COMMENT_IS_TOO_SHORT = 'Modules.Humpchies.CommentsController.ErrorCommentIsTooShort'; // Comment text is too short
    const ERROR_COMMENT_IS_TOO_LONG = 'Modules.Humpchies.CommentsController.ErrorCommentIsTooLong'; // Comment text is too long
    const ERROR_UNKNOWN = 'Modules.Humpchies.CommentsController.ErrorUnknown'; //Some error occurrence on creation
    const NO_ERROR = 'Modules.Humpchies.CommentsController.NoError';



    public static function create_ma() {
        if (!\Core\Utils\Session::getCurrentUserID()) {
            self::redirect('/');
        }

        $from_user = \Core\Utils\Session::getCurrentUserID();

        // Disable browser cache
        self::disableCache();
        $data = array();
        $data['comment'] =$_POST['text'];
        $data['ad_id'] =$_POST['ad_id'];
        $data['from_user'] =$from_user;

        $validity = self::validateNew($data);
        $response = array(
            'success' => 0
        );

        switch ($validity){
            case self::NO_ERROR:
                $adDetails = \Modules\Humpchies\AdModel::getDetailsOfActiveAd($data['ad_id']);
                $adDetails = array_shift($adDetails);
                $data['user_id'] = $adDetails['user_id'];
                $data['date'] = date('Y-m-d H:i:s');
                
                 // only members and advertisers who made the add can post a comment                
                if(!\Core\Utils\Session::currentUserIsMember() && \Core\Utils\Session::getCurrentUserID() != $adDetails['user_id']) {
                    $message = "[ERROR-SOMETHING-WENT-WRONG]";
                }
                
                
                break;
            case self::ERROR_MEMBER_ID_IS_INVALID:
                $message = "[ERROR-SOMETHING-WENT-WRONG]";
                break;
            case self::ERROR_AD_ID_IS_INVALID:
                $message = "[ERROR-SOMETHING-WENT-WRONG]";
                break;
            case self::ERROR_COMMENT_IS_EMPTY:
                $message = "[ERROR-COMMENT-IS-EMPTY]";
                break;
            case self::ERROR_COMMENT_IS_TOO_LONG:
                $message = "[ERROR-COMMENT-IS-TOO-LONG]";
                break;
            case self::ERROR_COMMENT_IS_TOO_SHORT:
                $message = "[ERROR-COMMENT-IS-TOO-SHORT]";
                break;
            default:
                $message ="[ERROR-SOMETHING-WENT-WRONG]";
                break;
        }

        if (!empty($message)){
            \Core\Utils\Text::translateBufferTokens($message,
                \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'),
                \Core\Utils\Session::getLocale(),
                \Core\Utils\Config::getConfigValue('\Application\Settings::locales_module',
                    FALSE));
            $response['message'] = $message;
            die(json_encode($response));
        }

        $save_result = \Modules\Humpchies\CommentsModel::create($data);

        switch ($save_result){
            case \Modules\Humpchies\CommentsModel::ERROR_UNKNOWN:
                $message ="[ERROR-SOMETHING-WENT-WRONG]";
                break;
            case intval($save_result) > 0:
                $authenticatedSessionValues = \Core\Utils\Session::getAuthenticatedSessionValues();
                $response['success'] = 1;
                $response['details'] = array();
                $response['details']['member_name'] = $authenticatedSessionValues['username'];
                $response['details']['date'] = strtotime($data['date']);
                $response['details']['text'] = $data['comment'];
                $message = "[SUCCESS-COMMENT-CREATED]";
                break;
            default:
                $message = "[ERROR-SOMETHING-WENT-WRONG]";
                break;

        }

        \Core\Utils\Text::translateBufferTokens($message,
            \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'),
            \Core\Utils\Session::getLocale(),
            \Core\Utils\Config::getConfigValue('\Application\Settings::locales_module',
                FALSE));
        $response['message'] = $message;
        die(json_encode($response));
    }

    
    public static function vote_ma() {
        
        if (!\Core\Utils\Session::getCurrentUserID()) {
            self::redirect('/');
        }
        $response = array(
            'success' => 0,
            'message' => ''
        );
        
        // only members can vote
        if(!\Core\Utils\Session::currentUserIsMember()) {
             $message = "[ERROR-VOTE-NOT-ALLOWED]";
        }
        
        $user_id = \Core\Utils\Session::getCurrentUserID();
        
        // Disable browser cache
        self::disableCache();
        $data = array();
        
        $data['comment_id'] = $_POST['comment_id'];
        $data['ad_id']      = $_POST['ad_id'];
        $data['vote']       = $_POST['vote'];

       // \Core\library\log::debug(json_encode($data), "SEND VOTE DATA");
        $validity = self::validateVote($data);
        switch ($validity){
            case self::NO_ERROR:
                break;
            default:
                $message ="[ERROR-SOMETHING-WENT-WRONG]";
                break;
        }
        
         // check if the user already voted 10 times today
        $totalTodayVotes = \Modules\Humpchies\CommentsVoteModel::getTodayVotes($user_id);
        $todayVotes = array_filter($totalTodayVotes, function($k) use ($data) {  // remove the vote for the current comment
            return $k != $data['comment_id'];
        }, ARRAY_FILTER_USE_KEY);
        
        //\Core\library\log::debug(count($todayVotes), "TOTAL VOTES");
        
        if(count($todayVotes) >= 10)  { // more than 10 votes
             $message = "[ERROR-VOTED-TOO-MANY-TIMES]";
        }
        
        if (!empty($message)){
            \Core\Utils\Text::translateBufferTokens($message,
                \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'),
                \Core\Utils\Session::getLocale(),
                \Core\Utils\Config::getConfigValue('\Application\Settings::locales_module',
                    FALSE));
            $response['message'] = $message;
            die(json_encode($response));
        }
       
        // check if the user already voted for this comment
        // if yes, mark that u must -1 the other type of vote
        // like $response['up'] = -1;
        $removeVote = false;
        $userVote =  \Modules\Humpchies\CommentsVoteModel::getUserVote($user_id, $data['comment_id']);
        
        if($userVote == $data['vote']) {
            // do not vote anymore, do nothing
            $response['success'] = 1; 
            die(json_encode($response));
        }
        
        if($userVote) {
            $removeVote = $userVote;   
            $response[$removeVote] = -1; 
        }
        
        //\Core\library\log::debug($removeVote, "user already voted");
        
        // add new vote
        // \Modules\Humpchies\CommentsModel::updateVotes( 77,  'up');
        $commentUpdated = \Modules\Humpchies\CommentsModel::updateVotes( $data['comment_id'],  $data['vote'], $removeVote);
        $save_result = \Modules\Humpchies\CommentsVoteModel::create($user_id, $data['comment_id'], $data['ad_id'], $data['vote']);
        //var_dump($save_result);
        
        switch ($save_result){
            case intval($save_result) > 0:
                $response['success'] = 1;    
                $response[$data['vote']] = 1;
                
                $message = "[SUCCESS-COMMENT-VOTED]";
                
                break;
            default:
                $message = "[ERROR-SOMETHING-WENT-WRONG]";
                break;

        }

        \Core\Utils\Text::translateBufferTokens($message,
            \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'),
            \Core\Utils\Session::getLocale(),
            \Core\Utils\Config::getConfigValue('\Application\Settings::locales_module',
                FALSE));
        $response['message'] = $message;
        die(json_encode($response));
    }

    
    
    
    /**
     * @param $fields
     * @return string
     */
    private static function validateNew(&$fields) {

        $fields = array_intersect_key($fields, array(
                'comment' => NULL,
                'ad_id' => NULL,
                'from_user' => NULL,
            )
        );
        $member_validity = \Core\Utils\Security::isIdInputValid($fields['from_user']);
        $ad_validity = \Core\Utils\Security::isIdInputValid($fields['ad_id']);
        $comment_text_validity = \Modules\Humpchies\CommentsModel::isCommentTextInputValid($fields['comment']);

        // ---------------------------USER-------------------------------------------------------
        if($member_validity !== \Core\Utils\Security::NO_ERROR){
            return self::ERROR_MEMBER_ID_IS_INVALID;
        }

        // ---------------------------USER-------------------------------------------------------
        if($ad_validity !== \Core\Utils\Security::NO_ERROR){
            return self::ERROR_AD_ID_IS_INVALID;
        }


        // ---------------------------Comment Text------------------------------------------------
        switch ($comment_text_validity) {
            case \Modules\Humpchies\CommentsModel::NO_ERROR:
                break;
            case \Modules\Humpchies\CommentsModel::ERROR_COMMENT_IS_EMPTY:
                return self::ERROR_COMMENT_IS_EMPTY;
                break;
            case \Modules\Humpchies\CommentsModel::ERROR_COMMENT_IS_TOO_LONG:
                return self::ERROR_COMMENT_IS_TOO_LONG;
                break;
            case \Modules\Humpchies\CommentsModel::ERROR_COMMENT_IS_TOO_SHORT:
                return self::ERROR_COMMENT_IS_TOO_SHORT;
                break;
            default:
                return $comment_text_validity;
                break;
        }

        return self::NO_ERROR;
    }

    
    private static function validateVote(&$fields) {

        $fields = array_intersect_key($fields, array(
                'comment_id' => NULL,
                'ad_id' => NULL,
                'vote' => NULL,
            )
        );
        $comment_validity = \Core\Utils\Security::isIdInputValid($fields['comment_id']);
        $ad_validity = \Core\Utils\Security::isIdInputValid($fields['ad_id']);
        

        // --------------------------- COMMENT ID --------------------------- //
        if($comment_validity !== \Core\Utils\Security::NO_ERROR){
            return self::ERROR_UNKNOWN;
        }

        // --------------------------- AD ID --------------------------- //
        if($ad_validity !== \Core\Utils\Security::NO_ERROR) {
            return self::ERROR_AD_ID_IS_INVALID;
        }
        // --------------------------- VOTE --------------------------- //
        if(!in_array($fields['vote'], ['up', 'down'])) {
            return self::ERROR_UNKNOWN;    
        }
        
        return self::NO_ERROR;
    }
    
    
}