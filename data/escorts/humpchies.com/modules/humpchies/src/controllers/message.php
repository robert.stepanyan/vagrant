<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class contains the buisness logic of the humpchies'
 * messaging system.
 */

Namespace Modules\Humpchies;

class MessageController extends \Core\Controller{
    
    // ===========
    // CONSTANTS |
    // ===========
    
    // Name lengths
    // ============
    const MAX_LEN_NAME                          = 32;
    const MIN_LEN_NAME                          = 2;
    
    // Subject lengths
    // ===============
    const MAX_LEN_SUBJECT                       = 32;
    const MIN_LEN_SUBJECT                       = 2;
    
    // Message lengths
    // ===============
    const MAX_LEN_MESSAGE                       = 1024;
    const MIN_LEN_MESSAGE                       = 2;     
    
    // Errors
    // ======
    const ERROR_ISSUE_TYPE_IS_INVALID           = 'Modules.Humpchies.MessageController.ErrorIssueTypeIsInvalid';
    const ERROR_ISSUE_TYPE_IS_EMPTY             = 'Modules.Humpchies.MessageController.ErrorIssueTypeIsEmpty';
    const ERROR_CAPTCHA_IS_EMPTY                = 'Modules.Humpchies.MessageController.ErrorCaptchaIsEmpty';
    const ERROR_CAPTCHA_IS_INVALID              = 'Modules.Humpchies.MessageController.ErrorCaptchaIsInvalid';
    const ERROR_CAPTCHA_IS_TOO_LONG             = 'Modules.Humpchies.MessageController.ErrorCaptchaIsTooLong';
    const ERROR_CAPTCHA_IS_TOO_SHORT            = 'Modules.Humpchies.MessageController.ErrorCaptchaIsTooShort';
    const ERROR_EMAIL_IS_EMPTY                  = 'Modules.Humpchies.MessageController.ErrorEmailIsEmpty';
    const ERROR_REEMAIL_IS_EMPTY                = 'Modules.Humpchies.MessageController.ErrorReEmailIsEmpty';
    const ERROR_USERNAME_IS_EMPTY               = 'Modules.Humpchies.MessageController.ErrorUsernameIsEmpty';
    const ERROR_EMAIL_IS_INVALID                = 'Modules.Humpchies.MessageController.ErrorEmailIsInvalid';
    const ERROR_EMAIL_IS_TOO_LONG               = 'Modules.Humpchies.MessageController.ErrorEmailIsTooLong';
    const ERROR_EMAIL_IS_TOO_SHORT              = 'Modules.Humpchies.MessageController.ErrorEmailIsTooShort';
    const ERROR_MESSAGE_IS_EMPTY                = 'Modules.Humpchies.MessageController.ErrorMessageIsEmpty';
    const ERROR_MESSAGE_IS_TOO_LONG             = 'Modules.Humpchies.MessageController.ErrorMessageIsTooLong';
    const ERROR_MESSAGE_IS_TOO_SHORT            = 'Modules.Humpchies.MessageController.ErrorMessageIsTooShort';
    const ERROR_NAME_IS_EMPTY                   = 'Modules.Humpchies.MessageController.ErrorNameIsEmpty';
    const ERROR_NAME_IS_TOO_LONG                = 'Modules.Humpchies.MessageController.ErrorNameIsTooLong';
    const ERROR_NAME_IS_TOO_SHORT               = 'Modules.Humpchies.MessageController.ErrorNameIsTooShort';
    const ERROR_SUBJECT_IS_EMPTY                = 'Modules.Humpchies.MessageController.ErrorSubjectIsEmpty';
    const ERROR_SUBJECT_IS_TOO_LONG             = 'Modules.Humpchies.MessageController.ErrorSubjectIsTooLong';
    const ERROR_SUBJECT_IS_TOO_SHORT            = 'Modules.Humpchies.MessageController.ErrorSubjectIsTooShort'; 
    const ERROR_UNKNOWN                         = 'Modules.Humpchies.MessageController.ErrorUnknown';
    const ERROR_EMAIL_DOES_NOT_MATCH            = 'Modules.Humpchies.MessageController.ErrorEmailDoesntMatch';
    const NO_ERROR                              = 'Modules.Humpchies.MessageController.NoError'; 
    
    /**
    * This function fullfils the requirements of the Contact Us
    * page of the site.
    * 
    * @returns NULL
    */
    public static function contactUs()
    {
		// Disable cache
        self::disableCache();
        
        // Custom SEO values
        self::setTitle('[TITLE-CONTACT-US]');
        self::setKeywords('[KEYWORDS-CONTACT-US]');
        self::setDescription('[DESCRIPTION-CONTACT-US]');
        self::setHeaderClass("has_breadcrumb");
        
        // Enable custom CSS support
        // =========================
        \Core\Controller::setExtraHead('<link media="all" rel="stylesheet" type="text/css" href="' 
                                        . 
                                        \Modules\Humpchies\Utils::getCDNPath('css')
                                        .
                                        (\Core\Utils\Device::isPC() ? 'pc' : ('mobile' . '/touch'))
                                        . 
                                        '/contact.css?v=1.2">');
        
        // Set the max lengths of the form fields                                
        self::$data['max_len_name'] = self::MAX_LEN_NAME;                       // name
        self::$data['max_len_email'] = \Core\Utils\Security::MAX_LEN_EMAIL;     // Email
        self::$data['max_len_subject'] = self::MAX_LEN_SUBJECT;                 // Subject
        self::$data['max_len_captcha'] = \Core\Utils\Security::LEN_CAPTCHA;     // Captcha
        
        // Bail out if the form hasn't been posted
        if(!$_POST)
            return;
		
     // Validate the data posted on the contact us page...
        switch(($message_validity = self::validateContactUs($_POST)))
        {   
            // No error took place, just on...
            case self::NO_ERROR:
                break;
            // CAPTCHA
            case self::ERROR_ISSUE_TYPE_IS_INVALID:
                return self::addMessage('[ERROR-SELECT-ISSUE-TYPE]', 'error');
                break;  
            case self::ERROR_ISSUE_TYPE_IS_EMPTY:
                return self::addMessage('[ERROR-SELECT-ISSUE-TYPE]', 'error');
                break;
            // CAPTCHA
            case self::ERROR_CAPTCHA_IS_EMPTY:
                return self::addMessage('[ERROR-CAPTCHA-IS-EMPTY]', 'error');
                break;
            case self::ERROR_CAPTCHA_IS_INVALID:
            case self::ERROR_CAPTCHA_IS_TOO_SHORT:
            case self::ERROR_CAPTCHA_IS_TOO_LONG: 
                return self::addMessage('[ERROR-CAPTCHA-IS-INVALID]', 'error');
                break;
            // NAME
            case self::ERROR_NAME_IS_EMPTY:
                return self::addMessage('[ERROR-NAME-IS-EMPTY]', 'error');
                break;
            case self::ERROR_NAME_IS_TOO_SHORT:
                return self::addMessage('[ERROR-NAME-IS-TOO-SHORT]' . self::MIN_LEN_NAME, 'error');
                break;   
            case self::ERROR_NAME_IS_TOO_LONG: 
                return self::addMessage('[ERROR-NAME-IS-TOO-LONG]' . self::MAX_LEN_NAME, 'error');
                break;
            // EMAIL   
            case self::ERROR_EMAIL_IS_EMPTY:
                return self::addMessage('[ERROR-EMAIL-IS-EMPTY]', 'error');
                break;
             case self::ERROR_USERNAME_IS_EMPTY:
                return self::addMessage('[ERROR-USERNAME-IS-EMPTY]', 'error');
                break;
            case self::ERROR_EMAIL_IS_INVALID:
            case self::ERROR_EMAIL_IS_TOO_SHORT:
            case self::ERROR_EMAIL_IS_TOO_LONG: 
                return self::addMessage('[ERROR-EMAIL-IS-INVALID]', 'error');
                break;
            // SUBJECT   
            case self::ERROR_SUBJECT_IS_EMPTY:
                return self::addMessage('[ERROR-SUBJECT-IS-EMPTY]', 'error');
                break;
            case self::ERROR_SUBJECT_IS_TOO_SHORT:
                return self::addMessage('[ERROR-SUBJECT-IS-TOO-SHORT]' . self::MIN_LEN_SUBJECT, 'error');
                break;   
            case self::ERROR_SUBJECT_IS_TOO_LONG: 
                return self::addMessage('[ERROR-SUBJECT-IS-TOO-LONG]' . self::MAX_LEN_SUBJECT, 'error');
                break;
            // MESSAGE
            case self::ERROR_MESSAGE_IS_EMPTY:
                return self::addMessage('[ERROR-MESSAGE-IS-EMPTY]', 'error');
                break;
            case self::ERROR_MESSAGE_IS_TOO_SHORT:
                return self::addMessage('[ERROR-MESSAGE-IS-TOO-SHORT]' . self::MIN_LEN_MESSAGE, 'error');
                break;   
            case self::ERROR_MESSAGE_IS_TOO_LONG: 
                return self::addMessage('[ERROR-MESSAGE-IS-TOO-LONG]' . self::MAX_LEN_MESSAGE, 'error');
                break;       
            case self::ERROR_EMAIL_DOES_NOT_MATCH: 
                return self::addMessage('[ERROR-EMAIL-DOESNT-MATCH]' , 'error');
                break;       
            default:
                return self::addMessage('[ERROR-UNHANDLED]: ' . $message_validity, 'error');
                break; 
        }

        $throttleKey = 'msg_' . \Core\Utils\Device::getDeviceIp();

        switch ($_POST['issue_type']) {
            case 1:
                $email_to = 'report@humpchies.com';
                break;
            case 2:
                $email_to = 'sales@humpchies.com';
                break;
            case 3:
                $email_to = 'tech@humpchies.com';
                break;
            case 4:
                $email_to = 'webmaster@humpchies.com';
                break;
            
            default:
                # code...
                break;
        }

        if (strcasecmp('Alien.dtches@yahoo.ca', $_POST["email"]) === 0
            ||
            strcasecmp('leonidechampagne@hotmail.comp', $_POST["email"]) === 0
            ||
            strcasecmp('daviddaout@outlook.com', $_POST["email"]) === 0
            ||
            strcasecmp('leonidechampagne@hotmail.com', $_POST["email"]) === 0
            ||
            strcasecmp('tony-champagne@hotmail.com', $_POST["email"]) === 0
            ||
            strcasecmp('pierettechampagne@hotmail.com', $_POST["email"]) === 0
            ||
            strcasecmp('tonychampagne8@gmail.com', $_POST["email"]) === 0
            ||
            strcasecmp('Deslandes_mathieu@hotmail.com', $_POST["email"]) === 0
            ||
            strcasecmp('Pamela@hotmail.com', $_POST["email"]) === 0
            ||
            strcasecmp('Melodielafontaine@yahoo.com', $_POST["email"]) === 0
            ||
            strcasecmp('valeriepater666@gmail.com', $_POST["email"]) === 0
            ||
            strcasecmp('andreia514@hotmail.fr', $_POST["email"]) === 0
            ||
            strcasecmp('milk137@hotmail.com', $_POST["email"]) === 0
            ||
            strcasecmp('petherbridge@usol.com', $_POST["email"]) === 0
            ||
            strcasecmp('Dayboyd064@gmail.com', $_POST["email"]) === 0
            ||
            strcasecmp('lilyhammer96@hotmail.com', $_POST["email"]) === 0
            ||
            in_array(
                \Core\Utils\Device::getDeviceIp(),
                [
                    '91.221.57.129',
                    '185.127.25.68',
                    '185.195.25.111',
                    '92.63.103.241',
                    '79.124.59.194',
                    '142.44.154.169',
                    '46.161.9.61',
                    '46.161.9.18',
                    '46.161.9.68',
                    '178.93.59.192',
                    '185.220.101.21',
                    '171.25.193.20',
                    '176.10.99.200',
                    '185.220.101.33',
                    '23.236.167.200',
                    '104.129.18.179',
                    '104.129.18.180',
                    '104.129.18.181',
                    '155.94.250.39',
                    '185.104.184.119',
                    '192.42.116.24',
                    '171.25.193.77',
                    '51.255.174.180',
                    '195.181.160.70',
                    '93.115.86.9',
                    '178.159.37.146',
                    '76.68.208.145',
                    '185.144.78.73',
                    '178.159.37.53'
                ]
            )
            ||
            \Core\Cache::get($throttleKey) !== false
            ||
            \Modules\Humpchies\Utils::isRu()
        ) {
            unset($_POST);
            self::addMessage('[SUCCESS-MESSAGE-SENT]');
            return;
        }
        
        // At this point we are golden. No errors have ocurred, lets set some email headers
        $sHeaders  = 'MIME-Version: 1.0' . "\r\n";                                          // MIME version
        $sHeaders .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";                // MIME type and character set
        $sHeaders .= 'Sender: webmaster@humpchies.com' . "\r\n";                            // From Us
        $sHeaders .= 'From: ' . $_POST["name"] . ' <' . $_POST["email"] . '>' . "\r\n";     // On behalf of

        $loggedInUser = $geoLocation = $varsAndHeaders = 'n/a';
        $loggedInUserId = 0;

        if (\Core\Utils\Session::getCurrentUserID()) {
            $loggedInUser = \Modules\Humpchies\UserModel::getDetailsById(\Core\Utils\Session::getCurrentUserID());

            if (is_array($loggedInUser) && 1 === count($loggedInUser)) {
                $loggedInUser = $loggedInUser[0];
                $loggedInUserId = $loggedInUser['id'];
                $loggedInUser = print_r($loggedInUser, true);
            }
        }

        if (\Core\Utils\Session::getGeoLocation()) {
            $tmpGeoLocation = \Core\Utils\Session::getGeoLocation();

            $geoLocation = "City => {$tmpGeoLocation->city->names->en}";

            foreach($tmpGeoLocation->subdivisions as $subdivision) {
                $geoLocation .= " Subdivision => {$subdivision->iso_code}";
            }

            $geoLocation .= " Country => {$tmpGeoLocation->country->iso_code}";
        }

        if(\Core\Utils\Device::getServerVarsAndHeaders()) {
            $varsAndHeaders = \Core\Utils\Device::getServerVarsAndHeaders();

            if (is_array($varsAndHeaders)) {
                $varsAndHeaders = print_r($varsAndHeaders, true);
            }
        }

        \Modules\Humpchies\LogEmailsModel::create(addslashes($_POST['name']), addslashes($_POST['email']), addslashes($_POST['subject']), addslashes($_POST['message']), $loggedInUserId, $geoLocation);

        $_POST["message"] .= "<br><br><br><br>";
        $_POST["message"] .= "Username:" . $_POST['username'];
       // $_POST["message"] .= "Geo Location: $geoLocation<br>";
//        $_POST["message"] .= "Logged in user: $loggedInUser<br>";
//        $_POST["message"] .= "Vars & Headers: $varsAndHeaders<br>";

        // Queue the email... advise the user of success of failure...
        //
        if ( \Core\Utils\SendMail::send($email_to, $_POST["subject"], $_POST["message"], true, $_POST["email"], $_POST["name"]) === TRUE) {
            
            self::addMessage('[SUCCESS-MESSAGE-SENT]');
            // \Core\Cache::save($throttleKey, 1, 1800);
        }
        else
            self::addMessage('[ERROR-MESSAGE-FAILED-TO-BE-SENT]', 'error');
        
        // Unset the post... 
        unset($_POST);
    }
    
    /**
    * This function validates the request parameters for the contcat us page
    * 
    * @param array $fields The value of $_GET
    * @returns NO_ERROR on success, and of the ERRORS on failure
    */
    private static function validateContactUs(&$fields)
    {
        
        // Clean up all fields
        $code_validity        = \Core\Utils\Security::isInputCaptchaValid($fields['code']);
        $name_validity        = \Core\Utils\Security::isStringInputValid($fields['name'], self::MAX_LEN_NAME, self::MIN_LEN_NAME);
        $username_validity    = \Core\Utils\Security::isStringInputValid($fields['username'], self::MAX_LEN_NAME, self::MIN_LEN_NAME);
        $email_validity       = \Core\Utils\Security::isEmailInputValid($fields['email']);
        $reemail_validity     = \Core\Utils\Security::isEmailInputValid($fields['reemail']);
        $subject_validity     = \Core\Utils\Security::isStringInputValid($fields['subject'], self::MAX_LEN_SUBJECT, self::MIN_LEN_SUBJECT); 
        $message_validity     = \Core\Utils\Security::isStringInputValid($fields['message'], self::MAX_LEN_MESSAGE, self::MIN_LEN_MESSAGE);  
        $issue_type_validity  = \Core\Utils\Security::isIntegerInputValid($fields['issue_type'], 1, 4);  

    // ---------------------------ISSUE TYPE-------------------------------------------------------

        switch($issue_type_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_INTEGER_IS_EMPTY:
                return self::ERROR_ISSUE_TYPE_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_INTEGER_IS_INVALID:
                return self::ERROR_ISSUE_TYPE_IS_INVALID;
                break;
            default:
                return self::ERROR_UNKNOWN;
        }
        // ---------------------------CAPTCHA-------------------------------------------------------

        switch($code_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_EMPTY:
                return self::ERROR_CAPTCHA_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_INVALID:
                return self::ERROR_CAPTCHA_IS_INVALID;
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_TOO_LONG:
                return self::ERROR_CAPTCHA_IS_TOO_LONG;
                break;
            case \Core\Utils\Security::ERROR_CAPTCHA_IS_TOO_SHORT:
                return self::ERROR_CAPTCHA_IS_TOO_SHORT;
                break;
            default:
                return self::ERROR_UNKNOWN;
        }

        // ---------------------------NAME----------------------------------------------------------
         switch($name_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_EMPTY:
                return self::ERROR_NAME_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_LONG:
                return self::ERROR_NAME_IS_TOO_LONG;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_SHORT:
                return self::ERROR_NAME_IS_TOO_SHORT;
                break;
            default:
                return self::ERROR_UNKNOWN;
        }   
        
        // -------------------------USERNAME----------------------------------------------------------
         switch($username_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_EMPTY:
                return self::ERROR_USERNAME_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_LONG:
                return self::ERROR_USERNAME_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_SHORT:
                return self::ERROR_USERNAME_IS_EMPTY;
                break;
            default:
                return self::ERROR_UNKNOWN;
        }   
        
        // ---------------------------EMAIL---------------------------------------------------------
        switch($email_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_EMAIL_IS_EMPTY:
                return self::ERROR_EMAIL_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_EMAIL_IS_INVALID:
                return self::ERROR_EMAIL_IS_INVALID;
                break;
            case \Core\Utils\Security::ERROR_EMAIL_IS_TOO_LONG:
                return self::ERROR_EMAIL_IS_TOO_LONG;
                break;
            case \Core\Utils\Security::ERROR_EMAIL_IS_TOO_SHORT:
                return self::ERROR_EMAIL_IS_TOO_SHORT;
                break;
            default:
                return self::ERROR_UNKNOWN;    
        }
        
        if($fields['email'] != $fields['reemail']){
            return self::ERROR_EMAIL_DOES_NOT_MATCH;
        }
        
        // ---------------------------SUBJECT-------------------------------------------------------
        switch($subject_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_EMPTY:
                return self::ERROR_SUBJECT_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_LONG:
                return self::ERROR_SUBJECT_IS_TOO_LONG;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_SHORT:
                return self::ERROR_SUBJECT_IS_TOO_SHORT;
                break;
            default:
                return self::ERROR_UNKNOWN;  
        }    

        // ---------------------------MESSAGE-------------------------------------------------------
        switch($message_validity)
        {
            case \Core\Utils\Security::NO_ERROR:
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_EMPTY:
                return self::ERROR_MESSAGE_IS_EMPTY;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_LONG:
                return self::ERROR_MESSAGE_IS_TOO_LONG;
                break;
            case \Core\Utils\Security::ERROR_STRING_IS_TOO_SHORT:
                return self::ERROR_MESSAGE_IS_TOO_SHORT;
                break;
            default:
                return self::ERROR_UNKNOWN;  
        }   

        return self::NO_ERROR;  
    }
}
?>
