<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This class encapsulates utilitarian functions and methods
 * that are used system wise to manipulate CACHE; MEMCACHE that is.
 */

Namespace Modules\Humpchies;

final class Utils{

    // GEO Zones
    // =========
    const GEO_ZONE_MONTREAL_METRO               = 'montreal_metro';
    const GEO_ZONE_QUEBEC_CENTRE                = 'quebec_centre';
    const GEO_ZONE_EASTERN_TOWNSHIPS            = 'eastern_townships';
    const GEO_ZONE_ONTARIO                      = 'ontario';
    const GEO_ZONE_OTTAWA_METRO                 = 'ottawa_metro';
    const GEO_ZONE_NEW_BRUNSWICK                = 'new_brunswick';

    // Singletons
    // ==========
    private static $cityZones                   = NULL;
    private static $isMontrealMetro             = NULL;
    
    /**
    * Because this class is untended to be a singleton we block access to its constructor. 
    * Therefore the class can only be instantiated from within itself.
    */
    private function __construct()
    {}
    
    /**
    * Disable object copying
    */
    private function __clone()
    {}

    /**
    * Disable object serialization
    */
    private function __wakeup()
    {}
    
    /**
    * Disable object deserialization
    */
    private function __sleep()
    {}

    public static function getCDNPath($sub_directory = 'css')
    {   ///////enable singleton to speed things up!!!!
        return \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue("\Modules\Humpchies\Cdn::$sub_directory");
    }
    
    public static function getMobileAlternate()
    {
        return  \Core\Utils\Device::getProtocol() 
                . 
                \Core\Utils\Config::getConfigValue('\Application\Settings::mobile_hostname') 
                . 
                \Core\Utils\Session::getLocale(TRUE, TRUE)
                .
                $_SERVER['REQUEST_URI'];   
    }
    
    /**
    * @desc return a string containing the difference in 2 dates with the appropriate unit (weeks, days, hours, etc...). Calculated by doing time2 - time1
    *
    * @param int the first date
    * @param int the second date
    *
    * @return string containing our difference
    */
    public static function getTimeDifferenceMessage($timestamp)
    {
        $basic_message = NULL;
                                                                                                                      
        switch(($time_difference = abs(time() - $timestamp)))
        {
            case ($time_difference === 0):
                $basic_message = '[LABEL-JUST-NOW]';
                break;
            case ($time_difference === 1):
                $basic_message = '1 [LABEL-SECOND]';
                break;
            case ($time_difference < 60):
                $basic_message = "$time_difference [LABEL-SECONDS]";
                break;
            case ($time_difference < 120):
                $basic_message = '1 [LABEL-MINUTE]';
                break;
            case ($time_difference  < 3600):
                $basic_message = floor(($time_difference / 60)) . ' [LABEL-MINUTES]';
                break;
            case ($time_difference  < 7200):
                $basic_message = '1 [LABEL-HOUR]';
                break;
            case ($time_difference  < 86400):
                $basic_message = floor(($time_difference / 3600)) . ' [LABEL-HOURS]';
                break;
            case ($time_difference  < 172800):
                $basic_message = '1 [LABEL-DAY]';
                break;
            case ($time_difference  < 604800):
                $basic_message = floor(($time_difference / 86400)) . ' [LABEL-DAYS]';
                break;
            case ($time_difference  < 1209600):
                $basic_message = '1 [LABEL-WEEK]';
                break;
            case ($time_difference  < 2592000):
                $basic_message = floor(($time_difference / 604800)) . ' [LABEL-WEEKS]';
                break;
            case ($time_difference  < 5184000):
                $basic_message = '1 [LABEL-MONTH]';
                break;
            case ($time_difference  < 31586000):
                $basic_message = floor(($time_difference / 2592000)) . ' [LABEL-MONTHS]';
                break;
            case ($time_difference  < 63072000):
                $basic_message = '1 [LABEL-YEAR]';
                break;
            default:
                $basic_message = floor(($time_difference / 31586000)) . ' [LABEL-YEARS]';
                break;
        }
        
        return "[PREFIX-TIME-DIFFERENCE] $basic_message [SUFFIX-TIME-DIFFERENCE]";
    }

    /**
     * @return bool|null
     */
    public static function sessionIsInMontrealMetroArea()
    {
        if(NULL !== self::$isMontrealMetro) {
            return self::$isMontrealMetro;
        }

        $geoZone = NULL;

        // Get VIPs by the user's city choice. Fixme: using the get without country validation might break when we go world wide
        if(isset($_GET['city'])) {
            $geoZone = self::getDefinedZoneForCity($_GET['city']);
        }
        
        //var_dump($_GET['region']);exit();
        if(isset($_GET['region']) && str_replace('-', "_", $_GET['region']) == self::GEO_ZONE_MONTREAL_METRO) {
            $geoZone = self::GEO_ZONE_MONTREAL_METRO;
        }
        

        if($geoZone && self::GEO_ZONE_MONTREAL_METRO === $geoZone) {
            return (self::$isMontrealMetro = TRUE);
        }

        // Find out if we are in the Montreal Metro Area by Geo Location
        $geoLocation = \Core\Utils\Session::getGeoLocation();

        // Disregard invalid Geo Locations
        if(\Core\Utils\Session::ERROR_GEO_LOCATION_IS_INVALID === $geoLocation) {
            return (self::$isMontrealMetro = FALSE);
        }

        // Disregard Geo Locations Outside of Quebec
        $userIsInQuebec = FALSE;

        // Disregard anything for which we are unable to retrieve a state or province
        if(NULL === @$geoLocation->subdivisions) {
            return (self::$isMontrealMetro = FALSE);
        }

        foreach($geoLocation->subdivisions as $subdivision) {
            if('QC' === $subdivision->iso_code && 'CA' === $geoLocation->country->iso_code) {
                $userIsInQuebec = TRUE;
            }
        }

        if(!$userIsInQuebec) {
            return (self::$isMontrealMetro = FALSE);
        }

        // Disregard Geo Locations for which a city could not be found
        if(NULL === @$geoLocation->city->names->en) {
            return (self::$isMontrealMetro = FALSE);
        }

        $geoZone = self::getDefinedZoneForCity($geoLocation->city->names->en);

        if($geoZone && self::GEO_ZONE_MONTREAL_METRO === $geoZone) {
            return (self::$isMontrealMetro = TRUE);
        }

        return (self::$isMontrealMetro = FALSE);
    }

    /**
     * Identifies the user's country as US or not
     *
     * @return bool
     */
    public static function isUs()
    {
        $geoLocation = \Core\Utils\Session::getGeoLocation();

        if(\Core\Utils\Session::ERROR_GEO_LOCATION_IS_INVALID === $geoLocation) {
            return FALSE;
        }

        return 'US' === $geoLocation->country->iso_code;
    }

    /**
     * Identifies the user's country as RU or not
     *
     * @return bool
     */
    public static function isRu()
    {
        $geoLocation = \Core\Utils\Session::getGeoLocation();

        if(\Core\Utils\Session::ERROR_GEO_LOCATION_IS_INVALID === $geoLocation) {
            return FALSE;
        }

        return 'RU' === $geoLocation->country->iso_code;
    }

    /**
     * @return bool
     */
    public static function isFrancophoneGeo()
    {
        $geoLocation = \Core\Utils\Session::getGeoLocation();

        if(\Core\Utils\Session::ERROR_GEO_LOCATION_IS_INVALID === $geoLocation) {
            return FALSE;
        }

        switch($geoLocation->country->iso_code) {
            case 'FR':
                return TRUE;
                break;
            case 'CA':
                foreach($geoLocation->subdivisions as $subdivision) {
                    if('QC' === $subdivision->iso_code) {
                        return TRUE;
                    }
                }
                return FALSE;
                break;
            default:
                return FALSE;
        }
    }

    /**
     * Given a city name, it will try to retrieve the Geographical zone where it belongs
     *
     * @param string $cityName The name of the city
     * @return bool|string The name of the Geo Zone. False if no association with the city is found
     */
    public static function getDefinedZoneForCity($cityName)
    {
        $cityName = strtolower($cityName);

        if(NULL !== self::$cityZones && NULL !== @self::$cityZones[$cityName]) {
            return self::$cityZones[$cityName];
        }

        $definedZones = self::getGeoZoneDefinitions();

        foreach($definedZones as $zoneName => $definedCities) {
            foreach($definedCities as $definedCity) {
                if($definedCity === $cityName) {
                    return (self::$cityZones[$cityName] = $zoneName);
                }
            }
        }

        return (self::$cityZones[$cityName] = FALSE);
    }

    /**
     * Returns cities grouped by Geographical zones
     *
     * @return array A multidimentional array containing the zone name and the cities within it
     */
    public static function getGeoZoneDefinitions() {

        return array(
            self::GEO_ZONE_EASTERN_TOWNSHIPS => array(
                'sherbrooke'            // Eastern Townships
            ),                          // =================
            self::GEO_ZONE_QUEBEC_CENTRE => array(
                'drummondville',        // Centre du Quebec + Saint-Hyacinthe
                'becancour',            // ==================================
                'kingsey-falls',
                'nicolet',
                'plessisville',
                'princeville',
                'saint-cyrille-de-wendover',
				'saint-georges',
                'saint-germain-de-grantham',
                'victoriaville',
                'warwick',
                'riviere-du-loup',
                'saint-hyacinthe',
                'quebec'
                ),
            self::GEO_ZONE_MONTREAL_METRO => array(
                'contrecoeur',         // South Shore
                'sainte-julie',        // ===========
                'varennes',
                'beloeil',
                'carignan',
                'chambly',
                'mont-saint-hilaire',
                'otterburn-park',
                'saint-basile-le-grand',
                'brossard',
                'greenfield-park',
                'lemoyne',
                'longueuil',
                'saint-hubert',
                'saint-Lambert',
                'candiac',
                'chateauguay',
                'delson',
                'la-prairie',
                'lery',
                'mercier',
                'saint-constant',
                'sainte-catherine',
                'blainville',          // North Shore
                'bois-des-filion',     // ===========
                'boisbriand',
                'deux-montagnes',
                'lorraine',
                'mirabel',
                'oka',
                'laval',
                'pointe-calumet',
                'rosemere',
                'saint-eustache',
                'saint-joseph-du-Lac',
                'sainte-anne-des-plaines',
                'sainte-marthe-sur-le-lac',
                'sainte-therese',
                'charlemagne',
                'l-assomption',
                "l'assomption",
                'mascouche',
                'repentigny',
                'saint-sulpice',
                'terrebonne',
                'joliette',
                'montreal',            // Montreal
                'montreal-est',        // ========
                'montreal-ouest',
                'montreal-nord',
                'beaconsfield',
                'kirkland',
                'pointe-claire',
                'anjou',
                "baie-d'urfe",
                'baie-d-urfe',
                'dollard-des-ormeaux',
                'dorval',
                'lachine',
                'lasalle',
                'mont-royal',
                'cote-saint-luc',
                'pierrefonds',
                'roxboro',
                'saint-leonard',
                'verdun',
                'westmount',
                'saint-laurent',
                'pointe-aux-trembles',
                'outremont',
                'ahuntsic-cartierville'
            )
        );
    }
}
?>