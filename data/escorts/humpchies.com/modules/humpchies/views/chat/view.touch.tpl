<nav id="breadcrumb">
    <ul>
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <li>[LABEL-CHAT]</li>
    </ul>
</nav>

<div class="wrapper_content">
    <div class="content_box" style="margin:0!important">
        <div class="content_box_body">
            <div class="chat-main" data-messages="<?=count($messages)?>" data-command="<?= \Core\Utils\Navigation::urlFor('chat', 'command') ?>">
                <div class="chat-left">
                    <ul>
                        <li class="template" id="msg-${chatId}" data-id="${messageId}" data-chat="${chatId}"
                            data-blocked="${blocked}" data-unread="${unread}" data-colour="${colour}" data-intro="${intro}" tabindex="0">
                            <div>
                                <img src="${image}">
                                <div class="fa avatar" style="background-color:${colour}">${avatar}</div>
                                <div class="unread">${unread}</div>
                            </div>
                            <div>
                                <span>${title}</span>
                                <span>${time}</span>
                                <span>[LABEL-BLOCKED]</span>
                            </div>
                        </li>
                        <?php foreach ($messages as $index => $message) : ?>
                        <li id="msg-<?=$message['chat_id']?>" data-id="<?=$message['id']?>"
                            data-chat="<?=$message['chat_id']?>" data-blocked="<?=$message['blocked']?1:0?>"
                            data-unread="<?=$message['unread']?>" data-colour="<?=$message['colour']?>"
                            data-intro="<?=$message['intro']?>" data-link="<?=$message['url']?>"
                            class="<?=$selected==$message['chat_id']?'selected':''?>" tabindex="0">
                            <div>
                                <img src="<?=$message['image']?>">
                                <div class="fa avatar" style="background-color:<?=$message['colour']?>"><?=$message['avatar']?></div>
                                <div class="unread"><?=$message['unread']?></div>
                            </div>
                            <div>
                                <span><?=$message['title']?></span>
                                <span><?=$message['message_time']?></span>
                                <span>[LABEL-BLOCKED]</span>
                            </div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="chat-right" data-blocked="<?=$blocked?>" data-isblocked="<?=$isBlocked?>">
                    <div id="top-placeholder">
                        <div class="top">
                            <span>
                                <span id="btn-back" class="fa fa-arrow-left"></span>
                                <img src="">
                                <div class="fa avatar" style="background-color:transparent"></div>
                                <em></em>
                            </span>
                            <div id="chat-menu" class="fa btn-menu <?=\Core\Utils\Session::getLocale()?>">
                                <a class="btn-block" id="chat-block-user"><span class="fa fa-ban"></span>[LABEL-BLOCK-USER]</a>
                                <a class="btn-unblock"
                                   id="chat-unblock-user"><span class="fa fa-ban"></span>[LABEL-UNBLOCK-USER]</a>
                                <a class="btn-delete" id="chat-delete-conversation"><span class="fa fa-trash"></span>[LABEL-DELETE-CONVERSATION]</a>
                            </div>
                        </div>
                    </div>
                    <div class="center" tabindex="0">
                        <div id="msg-${messageId}" data-id="${messageId}" data-reply="${reply}"
                             class="message template">
                            <span>${message}</span>
                            <em>${time}</em>
                        </div>
                        <div class="intro" style='cursor:pointer'>[LABEL-CONVERSATION-FOR]<span style='font-weight:bold'></span></div>
                        <div>
                            <?php foreach ($chatMessages as $message) : ?>
                            <?php if(!$message["hidden"]):?>
                            <div id="chat-<?=$message['id']?>" data-id="<?=$message['id']?>"
                                 data-reply="<?=$message['is_reply']?>" class="message">
                                <span><?=$message['message']?></span>
                                <em><?=$message['chat_time']?></em>
                            </div>
                            <?php endif;?>    
                            <?php endforeach; ?>
                        </div>
                        <div class="blocked-by">[LABEL-INFO-BLOCKED-BY-USER]</div>
                        <div class="blocked">[LABEL-INFO-BLOCKED-USER]</div>
                    </div>
                    <div class="bottom">
                        <div>
                            <input id="chat-message" type="text" placeholder="[LABEL-MESSAGE-PLACEHOLDER]"
                                   maxlength="500">
                        </div>
                        <button class="fa fa-send btn-send" id="chat-reply">
                    </div>
                </div>
            </div>
            <div class="chat-empty" data-messages="<?=count($messages)?>">
                [LABEL-EMPTY-CHAT]
            </div>
        </div>
    </div>
</div>

<div id="block-user-modal" class="modal-pm">
    <!-- Modal content -->
    <div class="modal-pm-content">
        <div class="modal-pm-header">
            [LABEL-BLOCK-USER-QUESTION]
        </div>
        <div class="modal-pm-body">
            [LABEL-BLOCK-USER-ARE-YOU-SURE]
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left">
                <input type="button" class='btn-orange modal-pm-close' value="[LABEL-CANCEL]"/>
            </div>
            <div class="modal-pm-right">
                <input id="button-block-user-modal" type="button" class='btn-red' value="[LABEL-BUTTON-YES-BLOCK]"/>
            </div>
        </div>
    </div>
</div>

<div id="block-user-confirm-modal" class="modal-pm">
    <!-- Modal content -->
    <div class="modal-pm-content">
        <div class="modal-pm-header">
            [LABEL-USER-BLOCKED]
        </div>
        <div class="modal-pm-body">
            [LABEL-USER-BLOCK-CONFIRM]
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left">
                <a href="<?=\Core\Utils\Navigation::urlFor('/')?>" class="btn-orange" title="[LABEL-HOMEPAGE]">[LABEL-HOMEPAGE]</a>
            </div>
            <div class="modal-pm-right">
                <a href="<?=\Core\Utils\Navigation::urlFor('chat', 'view')?>" class="btn-orange" title="[LABEL-CHAT-LIST]">[LABEL-CHAT-LIST]</a>
            </div>
        </div>
    </div>
</div>

<div id="delete-chat-modal" class="modal-pm">
    <!-- Modal content -->
    <div class="modal-pm-content">
        <div class="modal-pm-header">
            [LABEL-DELETE-CONVERSATION]
        </div>
        <div class="modal-pm-body">
            [LABEL-DELETE-CONVERSATION-ARE-YOU-SURE]
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left">
                <input type="button" class='btn-orange modal-pm-close' value="[LABEL-CANCEL]"/>
            </div>
            <div class="modal-pm-right">
                <input id="button-delete-chat-modal" type="button" class='btn-red' value="[LABEL-BUTTON-YES-DELETE]"/>
            </div>
        </div>
    </div>
</div>

<div id="delete-chat-confirm-modal" class="modal-pm">
    <!-- Modal content -->
    <div class="modal-pm-content">
        <div class="modal-pm-header">
            [LABEL-DELETE-CONVERSATION-CONFIRM]
        </div>
        <div class="modal-pm-body">
            [LABEL-DELETE-CONVERSATION-DONE]
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left">
                <a href="<?=\Core\Utils\Navigation::urlFor('/')?>" class="btn-orange" title="[LABEL-HOMEPAGE]">[LABEL-HOMEPAGE]</a>
            </div>
            <div class="modal-pm-right">
                <a href="<?=\Core\Utils\Navigation::urlFor('chat', 'view')?>" class="btn-orange" title="[LABEL-CHAT-LIST]">[LABEL-CHAT-LIST]</a>
            </div>
        </div>
    </div>
</div>
