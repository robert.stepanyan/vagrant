<nav id="breadcrumb">
    <ul>
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <li>[LABEL-CHAT]</li>
    </ul>
</nav>

<div class="wrapper_content_full">
    <h1>[LABEL-CHAT]</h1>
    <div class="chat-main" data-messages="<?=count($messages)?>" data-command="<?= \Core\Utils\Navigation::urlFor('chat', 'command') ?>">
        <div class="chat-left">
            <ul>
                <li class="template" id="msg-${chatId}" data-id="${messageId}" data-chat="${chatId}"
                    data-blocked="${blocked}" data-unread="${unread}" data-colour="${colour}" data-intro="${intro}" tabindex="0">
                    <div>
                        <img src="${image}">
                        <div class="fa avatar" style="background-color:${colour}">${avatar}</div>
                        <div class="unread">${unread}</div>
                    </div>
                    <div>
                        <span>${title}</span>
                        <span>${time}</span>
                        <span>[LABEL-BLOCKED]</span>
                    </div>
                </li>
                <?php foreach ($messages as $index => $message) : ?>
                <li id="msg-<?=$message['chat_id']?>" data-id="<?=$message['id']?>"
                    data-chat="<?=$message['chat_id']?>" data-blocked="<?=$message['blocked']?1:0?>"
                    data-unread="<?=$message['unread']?>" data-colour="<?=$message['colour']?>"
                    data-intro="<?=$message['intro']?>" data-link="<?=$message['url']?>"
                    class="<?=$selected==$message['chat_id']?'selected':''?>" tabindex="0">
                    <div>
                        <img src="<?=$message['image']?>">
                        <div class="fa avatar" style="background-color:<?=$message['colour']?>"><?=$message['avatar']?></div>
                        <div class="unread"><?=$message['unread']?></div>
                    </div>
                    <div>
                        <span><?=$message['title']?></span>
                        <span><?=$message['message_time']?></span>
                        <span>[LABEL-BLOCKED]</span>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="chat-right" data-blocked="<?=$blocked?>" data-isblocked="<?=$isBlocked?>">
            <div class="top">
                <span><span>[LABEL-CONVERSATION-FOR]</span> <em></em></span>
                <a class="btn-delete" id="chat-delete-conversation"><span class="fa fa-trash"></span>[LABEL-DELETE-CONVERSATION]</a>
                <a class="btn-block" id="chat-block-user"><span class="fa fa-ban"></span>[LABEL-BLOCK-USER]</a>
                <a class="btn-unblock" id="chat-unblock-user"><span class="fa fa-ban"></span>[LABEL-UNBLOCK-USER]</a>
            </div>
            <div class="center" tabindex="0">
                <div id="msg-${messageId}" data-id="${messageId}" data-reply="${reply}"
                     class="message template">
                    <span>${message}</span>
                    <em>${time}</em>
                </div>
                <div class="intro"></div>
                <div>
                    <?php foreach ($chatMessages as $message) : ?>
                     <?php if(!$message["hidden"]):?>
                    <div id="chat-<?=$message['id']?>" data-id="<?=$message['id']?>"
                         data-reply="<?=$message['is_reply']?>" class="message">
                        <span><?=$message['message']?></span>
                        <em><?=$message['chat_time']?></em>
                    </div>
                     <?php endif;?>   
                    <?php endforeach; ?>
                </div>
                <div class="blocked-by">[LABEL-INFO-BLOCKED-BY-USER]</div>
                <div class="blocked">[LABEL-INFO-BLOCKED-USER]</div>
            </div>
            <div class="bottom">
                <div>
                    <input id="chat-message" type="text" placeholder="[LABEL-MESSAGE-PLACEHOLDER]"
                           maxlength="500">
                </div>
                <input class="btn-send" id="chat-reply" type="button" value="[LABEL-SEND]" >
            </div>
        </div>
    </div>
    <div class="chat-empty" data-messages="<?=count($messages)?>">
        <br><br><br><span style="line-height: 30px;">[LABEL-EMPTY-CHAT]</span>
    </div>
</div>

<div id="block-user-modal" class="modal-pm">
    <!-- Modal content -->
    <div class="modal-pm-content">
        <div class="modal-pm-header">
            [LABEL-BLOCK-USER-QUESTION]
        </div>
        <div class="modal-pm-body">
            [LABEL-BLOCK-USER-ARE-YOU-SURE]
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left">
                <input type="button" class='btn-orange modal-pm-close' value="[LABEL-CANCEL]"/>
            </div>
            <div class="modal-pm-right">
                <input id="button-block-user-modal" type="button" class='btn-red' value="[LABEL-BUTTON-YES-BLOCK]"/>
            </div>
        </div>
    </div>
</div>

<div id="block-user-confirm-modal" class="modal-pm">
    <!-- Modal content -->
    <div class="modal-pm-content">
        <div class="modal-pm-header">
            [LABEL-USER-BLOCKED]
        </div>
        <div class="modal-pm-body">
            [LABEL-USER-BLOCK-CONFIRM]
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left">
                <a href="<?=\Core\Utils\Navigation::urlFor('/')?>" class="btn-orange" title="[LABEL-HOMEPAGE]">[LABEL-HOMEPAGE]</a>
            </div>
            <div class="modal-pm-right">
                <a href="<?=\Core\Utils\Navigation::urlFor('chat', 'view')?>" class="btn-orange" title="[LABEL-CHAT-LIST]">[LABEL-CHAT-LIST]</a>
            </div>            
        </div>
    </div>
</div>

<div id="delete-chat-modal" class="modal-pm">
    <!-- Modal content -->
    <div class="modal-pm-content">
        <div class="modal-pm-header">
            [LABEL-DELETE-CONVERSATION]
        </div>
        <div class="modal-pm-body">
            [LABEL-DELETE-CONVERSATION-ARE-YOU-SURE]
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left">
                <input type="button" class='btn-orange modal-pm-close' value="[LABEL-CANCEL]"/>
            </div>
            <div class="modal-pm-right">
                <input id="button-delete-chat-modal" type="button" class='btn-red' value="[LABEL-BUTTON-YES-DELETE]"/>
            </div>            
        </div>
    </div>
</div>

<div id="delete-chat-confirm-modal" class="modal-pm">
    <!-- Modal content -->
    <div class="modal-pm-content">
        <div class="modal-pm-header">
            [LABEL-DELETE-CONVERSATION-CONFIRM]
        </div>
        <div class="modal-pm-body">
            [LABEL-DELETE-CONVERSATION-DONE]
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left">
                <a href="<?=\Core\Utils\Navigation::urlFor('/')?>" class="btn-orange" title="[LABEL-HOMEPAGE]">[LABEL-HOMEPAGE]</a>
            </div>
            <div class="modal-pm-right">
                <a href="<?=\Core\Utils\Navigation::urlFor('chat', 'view')?>" class="btn-orange" title="[LABEL-CHAT-LIST]">[LABEL-CHAT-LIST]</a>
            </div>
        </div>
    </div>
</div>
