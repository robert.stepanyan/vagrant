<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen in order
 * to verify the user's mayority of age.
 */

$ageVerifiedCookieName = 'ageVerified';

if (isset($_COOKIE[$ageVerifiedCookieName]) && 1 === (int)$_COOKIE[$ageVerifiedCookieName]) {
    return;
}

require_once(
            \Core\View::getFullyQualifiedViewPath('Modules\Humpchies\Verification::splash',
            \Core\View::LOCALIZED_DEVICE_MODE));?>

<script type="text/javascript">
    var ageVerifiedCookieName = '<?=$ageVerifiedCookieName?>';
    var layout = document.getElementById("layout");

    layout.classList.toggle('scroll-disabled');

    if ('1' === getCookieValue(ageVerifiedCookieName)) {
        tagAgeVerified();
    }

    function getCookieValue(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');

        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }

        return null;
    }

    function tagAgeVerified(){
        layout.classList.remove('scroll-disabled');

        var elems = document.querySelectorAll(".popup");

        for(var i = elems.length - 1;i >= 0; i--)
            elems[i].parentNode.removeChild(elems[i]);

        if ('1' !== getCookieValue(ageVerifiedCookieName)) {
            document.cookie = ageVerifiedCookieName + '=1; path=/;';
        }
    }

</script>