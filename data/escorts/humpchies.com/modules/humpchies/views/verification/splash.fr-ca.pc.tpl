<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen in order
 * to verify the user's mayority of age in French for the PC version of
 * the site.
 */
?>
<div class="modal-backdrop popup"></div>
<div class="humpchies-modal popup">
    <div class="modal-wrapper">
        <div class="modal-content">
            <div class="modal-body">
                <a class="language" title="<?=$language_options[0][2]?>" href="<?=$language_options[0][1]?>"><?=$language_options[0][0]?></a>
                <h2 class="modal-heading">
                    Adultes seulement!
                </h2>
                <p>
                    <strong>ATTENTION:</strong>&nbsp;Ce site contient de la nudit&eacute;, du contenu sexuel
                    explicite et un langage adulte.
                </p>
                <p>
                    Ce site Web doit &ecirc;tre acc&eacute;d&eacute; seulement par des personnes qui ont l'&acirc;ge
                    l&eacute;gal dans l'endroit physique d'o&ugrave; le site Web est acc&eacute;d&eacute;.
                    Toute utilisation non autoris&eacute;e de ce site peut enfreindre la loi nationale,
                    f&eacute;d&eacute;rale et / ou &eacute;trang&egrave;re.
                </p>
                <p>
                    Nous ne cr&eacute;ons ni ne produisons aucun contenu figurant sur notre site Web; Tout le contenu
                    publi&eacute; sur notre site Web doit &ecirc;tre conforme &agrave; nos normes d'&acirc;ge et
                    de contenu.
                </p>
                <p>
                    Nous NE tol&eacute;rons PAS la pornographie juv&eacute;nile ou les mineurs faisant de la
                    publicit&eacute; ou utilisant notre site.
                </p>
                <p>
                    Ce site utilise des cookies. En continuant &agrave; naviguer sur le site, vous acceptez notre
                    utilisation de cookies.
                </p>

            </div>
            <div class="modal-footer">
                <div class="access">
                    <a onclick="tagAgeVerified()" type="button" class="btn btn-enter" >[BTN-CAPTION-ENTER]</a>
                    <a href="https://www.google.com" class="btn btn-leave">[BTN-CAPTION-LEAVE]</a>                            
                </div>
                <div class="custom-links">
                  <a onclick="tagAgeVerified()" href="<?=\Core\Utils\Navigation::urlFor('terms-and-conditions')?>" title="[HREF-TITLE-TERMS-AND-CONDITIONS]">
                            [LABEL-TERMS-AND-CONDITIONS]
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>  