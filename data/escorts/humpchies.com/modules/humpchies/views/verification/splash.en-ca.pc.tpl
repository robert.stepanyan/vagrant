<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen in order
 * to verify the user's mayority of age in English for the PC version of
 * the site.
 */
?>
<div class="modal-backdrop popup"></div>
<div class="humpchies-modal popup">
    <div class="modal-wrapper">
        <div class="modal-content">
            <div class="modal-body">
                <a class="language" title="<?=$language_options[0][2]?>" href="<?=$language_options[0][1]?>"><?=$language_options[0][0]?></a>
                <h2 class="modal-heading">
                    Adults only!
                </h2>
                <p>
                    <strong>WARNING:</strong>&nbsp;This website contains nudity, explicit sexual
                    content and adult language.
                </p>
                <p>This Website should be accessed only by people who are
                    of legal age in the physical location from where the website is being accessed.
                    By accessing this website, you agree that you are of legal age and also agree
                    the our Terms &amp; Conditions. Any unauthorized use of this site may violate state,
                    federal and/or foreign law.</p>
                <p>
                    While we do not create nor produce any content listed on our website;
                    all content posted on our website must comply with our age and content standards.
                    We DO NOT tolerate child pornography or minors
                    advertising or utilizing our site.
                </p>
                <p>
                    This site uses cookies. By continuing to browse the site you are agreeing to our use of cookies.
                </p>
            </div>
            <div class="modal-footer">
                <div class="access">
                    <a onclick="tagAgeVerified()" type="button" class="btn btn-enter" >[BTN-CAPTION-ENTER]</a>
                    <a href="https://www.google.com" class="btn btn-leave">[BTN-CAPTION-LEAVE]</a>                            
                </div>
                <div class="custom-links">
                    <a onclick="tagAgeVerified()" href="<?=\Core\Utils\Navigation::urlFor('terms-and-conditions')?>" title="[HREF-TITLE-TERMS-AND-CONDITIONS]">
                            Terms &amp; Conditions
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>