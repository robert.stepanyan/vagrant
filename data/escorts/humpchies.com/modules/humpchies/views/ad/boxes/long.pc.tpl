<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents one ad and its details in a list of ads.
 */
 ?>
 
 
<div>
    <?php if (true === @$ad['vip']) { ?>
        <span class="vip_flag"></span><?php }?>
    <?php if (true === @$ad['premium']) { ?>
        <span class="premium_flag"></span>  <?php } ?>
</div>
<article class="user_content_list_element_box <?php echo (($ad['date_bumped'] + (60*60*24)) - time() > 0)? 'more_images' : '';?>">
    <div class="user_content_list_element_box_thumb">
        <div class="<?=($ad['thumb_info']['height'] === 180) ? 'thumb_portrait' : (($ad['thumb_info']['height'] === 120) ? 'thumb_square' : 'thumb_landscape')?>">
            <a href="<?=$ad['url']?>" title="<?=$ad['href_title']?>">
                <img src="<?=$ad['thumb_info']['src']?>" width="<?=$ad['thumb_info']['width']?>" height="<?=$ad['thumb_info']['height']?>" alt="<?=$ad['title']?>" />
            </a>
        </div>
        <?php 
         if($ad['date_bumped'] + (60*60*24) - time() > 0){
        $no_thumbs = 0;?>
        <?php if(!empty($ad['all_images']['thumbs'])):?>
            
             <?php   foreach(array(\Core\Utils\Image::ORIENTATION_LANDSCAPE, \Core\Utils\Image::ORIENTATION_PORTRAIT, \Core\Utils\Image::ORIENTATION_SQUARE) as $thumb_orientation)
                    {
                        if(isset($ad['all_images']['thumbs'][$thumb_orientation]))
                        {
                            foreach($ad['all_images']['thumbs'][$thumb_orientation] as $thumb)
                            {?>
                                <?php if($no_thumbs > 2 ) break;
                                $no_thumbs++;?>
                                <div class="thumb_square_small">
                                    <a href="<?=$ad['url']?>" title="<?=$ad['href_title']?>">
                                        <span class="small_thumb" style="background:url('<?=$thumb['href']?>'); background-size:cover;"  title="<?=$ad['title']?>"></span> 
                                    </a>       
                                </div>
            <?php           }    
                        }    
                    }?>
        <?php endif;?>
        <?php for ($i = $no_thumbs; $i < 2; $i++):?>
        <div class="thumb_square_small">
            <a href="<?=$ad['url']?>" title="<?=$ad['href_title']?>">
                <span class="small_thumb" style="background:url('https://gui.humpchies.com/gui_ads/filler.jpg'); background-size:cover;"  title="<?=$ad['title']?>"></span> 
            </a>       
        </div>
        <?php endfor;
        }
        ?>
        
    </div>
    <div class="user_content_list_element_box_header">
        <h2 class="title">
            <a href="<?=$ad['url']?>" title="<?=$ad['href_title']?>">
                <?=$ad['title']?>
            </a>
        </h2>
    </div>
    
    <div class="user_content_list_element_box_content">
        <p class="description_box">
            <?=$ad['description']?>
        </p>
        <section>
            <article>
                <ul>
                    <li>
                        <i class="fa fa-th-list"></i>
                        <span rel-data="[LABEL-CATEGORY]: "></span>
                        <?php if(0):?>
                            <div class="bg_labels category_<?php echo (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';?>">
                                <span>&nbsp;</span>
                            </div>
                        <?php endif;?>    
                        <a href="<?=$ad['category_url']?>" title="<?=$ad['category_href_title']?>" >
                            <?=$ad['category_name']?>
                        </a>
                    </li>
                    <li>
                        <i class="fa fa-map-marker"></i>
                        
                       <span rel-data="[LABEL-CITY]:&nbsp;"></span>
                        <a href="<?=$ad['category_url_w_city']?>" title="<?=$ad['category_href_w_city_title']?>" >
                            <?=$ad['city_name']?>
                        </a>
                    </li>
                    <li>
                        <i class="fa fa-calendar"></i>
                        <span rel-data="[LABEL-POSTED]:&nbsp;"></span>
                        <?=$ad['date_released']?>
                    </li>
                </ul>
            </article>
            <article>
                <? if($ad['id'] != 1579901): ?>
                <div class="social" style="display: flex; flex-direction: row; align-items: center; justify-content: flex-start;">
                    <div class="title" style="width: auto; margin-right: 15px; display: flex; align-items: center; align-content: center;">                    
                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                        <span rel-data="[LABEL-SHARE]"></span>
                        
                    </div>
                    <a href="https://twitter.com/share?url=<?php echo rawurlencode(\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'). $ad['url']);?>" rel="nofollow" target="_blank">
                        <i class="fa fa-twitter" aria-hidden="true" onclick="addSocialShare('twitter', 'listing');"></i>
                    </a>
                 
                </div>
                <? endif; ?>
                <a href="<?=$ad['url']?>" title="<?=$ad['href_title']?>" class="button">
                    <span rel-data="[LABEL-DETAILS]"></span>
                    
                </a>
            </article>
        </section>
        <?php if( ($ad['date_bumped'] + (60*60*24)) - time() > 0):?>
             <svg class="svg-arrow" 
             xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink"
             width="20px" height="27px" preserveAspectRatio="none" viewBox="0 0 30 50">
            <path fill-rule="evenodd"  fill="rgb(253, 152, 39)"
             d="M14.000,12.000 L14.000,20.000 L6.000,20.000 L6.000,12.000 L-0.000,12.000 L10.000,-0.000 L20.000,12.000 L14.000,12.000 ZM14.000,24.000 L6.000,24.000 L6.000,22.000 L14.000,22.000 L14.000,24.000 ZM14.000,27.000 L6.000,27.000 L6.000,26.000 L14.000,26.000 L14.000,27.000 Z"/>
            </svg>
        <?php endif;?>    
    </div>
    <?php if( $ad['verification_status'] == "verified") {?>
    <div class="real-photos">[LABEL-REAL-PHOTO]</div>
    <? } ?>
</article>
