<div class="location-filters_container">
    <div id="filter-container" class="location-filter-box">
        
        <!-- setup number of active filters -->
        <?php 
            $no_filters = 0;
            $no_filters += ($location_filter != '')? 1 : 0; // availability
            $no_filters += ($has_comments_filter == 1)? 1 : 0; // has comments
            $no_filters += ($accepts_comments_filter == 1)? 1 : 0; // accepts_ comments
            $no_filters += ($real_photos_filter == 1)? 1 : 0; // real photos
            $no_filters += ($display_location != '')? 1 : 0; // city/region
            $no_filters += ($services_filter != '')? count(explode(',', $services_filter)) : 0; // city/region
            $no_filters += ($prices_filter != '')? count(explode(',', $prices_filter)) : 0; // prices
            $no_filters += ($phone1_filter != '' || $phone2_filter != '' || $phone3_filter != '')? 1 : 0; // phone
            $total_filters =  count($tags) + 5;
        ?>
        
        <h3> 
            [LOCATION-LABEL] &amp; [FILTERS-LABEL]
            <?php echo ($no_filters > 0) ?  "({$no_filters}/{$total_filters})" : '';?>
        </h3>
        <div class="filter-icon <?php echo ($no_filters > 0) ? 'active' : '';?>"><i class="fa fa-filter"></i></div>
    </div>
</div>



<div id="filters_box" class="modal_full" style="display: none;">
    <div class="header_box">
        <a href="javascript:void(0)" id="close_filters"><i class="fa fa-arrow-left"></i></a> 
        <h3> [LOCATION-LABEL] &amp; [FILTERS-LABEL] </h3>
        <a href="javascript: void(0)" class="clear-all">[FILTERS-CLEAR] <i class="fa fa-trash"></i></a>
    </div>
    <div class="body_container">
        <form action="" method="POST" id="filters_form">
            <input type="hidden" name="location" id="location" value="<?php echo $location_filter;?>" />                         
            <input type="hidden" name="services" id="services" value="<?php echo $services_filter;?>" />  
            <input type="hidden" name="prices" id="prices" value="<?php echo $prices_filter;?>" />  
            <input type="hidden" name="has_comments" id="has_comments" value="<?php echo $has_comments_filter;?>" />                         
            <input type="hidden" name="real_photos" id="real_photos" value="<?php echo $real_photos_filter;?>" />                         
            <input type="hidden" name="accepts_comments" id="accepts_comments" value="<?php echo $accepts_comments_filter;?>" /> 
            <input type="hidden" name="phone1" id="phone1" value="<?php echo $phone1_filter;?>" />                         
            <input type="hidden" name="phone2" id="phone2" value="<?php echo $phone2_filter;?>" />                         
            <input type="hidden" name="phone3" id="phone3" value="<?php echo $phone3_filter;?>" />                      
            <input type="hidden" name="city_slug" id="city_slug" value="" />
                
            <div class="body_box">
                
                <div id="location-container" class="white-box">
                    <i class="fa fa-map-marker"></i> 
                    <p class="city-name-box">
                        [LABEL-SELECT]&nbsp;[LOCATION-LABEL]
                        <?php if($display_location != ''):?>
                            <span class="city_name selected">
                                <?php echo $display_location;?>
                            </span>
                        <?php endif;?>
                    </p> 
                </div>
                
                <div id="prices_select" class="white-box">
                    <i class="fa fa-money-bill"></i>
                    <p>
                        [LABEL-PRICE-NEW]
                        <?php if($prices_filter != ''):?>
                            <span class="prices_name selected">
                                <?php $price_list = [];?>
                                <?php foreach($price_intervals as $key => $price):?>
                                    <?php if(in_array($key, explode(",", $prices_filter))):?>
                                        <?php $price_list[] = $price;?>
                                    <?php endif;?>
                                <?php endforeach;?> 
                                <?php echo implode(", ", $price_list);?>   
                            </span>
                        <?php endif;?>
                    </p>
                    <i class="fa fa-plus"></i> 
                </div>
                
                <div id="availability_select" class="white-box">
                    <i class="fa fa-bed"></i> 
                    <p>
                        [LABEL-LOCATION-AVAILABLE]
                        <?php if($location_filter != ''):?>
                            <span class="availability_name selected">
                                <?php echo $location_filter == "incalls"? '[LABEL-LOCATION-INCALL]' : ($location_filter == "outcalls"? '[LABEL-LOCATION-OUTCALL]' : '[LABEL-LOCATION-ALL]');?>
                            </span>
                        <?php endif;?>
                    </p>
                    <i class="fa fa-plus"></i> 
                </div>
                <div id="services_select" class="white-box">
                    <div class="services_icon"></div>
                    <p>
                        [LABEL-SELECT-SERVICES]
                        <?php if($services_filter != ''):?>
                            <span class="services_name selected">
                                <?php $tag_list = [];?>
                                <?php foreach($tags as $tag):?>
                                    <?php if(in_array($tag['slug'], explode(",", $services_filter))):?>
                                        <?php $tag_list[] = $tag['name'];?>
                                    <?php endif;?>
                                <?php endforeach;?> 
                                <?php echo implode(", ", $tag_list);?>   
                            </span>
                        <?php endif;?>
                    </p>
                    <i class="fa fa-plus"></i> 
                </div>
                
                
                <div id="additional_select" class="white-box">
                    <i class="fa fa-filter"></i>
                    <p>
                        [FILTERS-MORE]
                        <span class="additional_name selected">
                            <?php $additional_list = []; 
                            
                            $additional_list[] = ($has_comments_filter == 1)? '[HAS-COMMENTS-LABEL]' : '';
                            $additional_list[] = ($accepts_comments_filter == 1)? '[ACCEPTS-COMMENTS-LABEL]' : '';
                            $additional_list[] = ($real_photos_filter == 1)? '[LABEL-REAL-PHOTO]' : '';
                            
                            $phone = [$phone1_filter, $phone2_filter, $phone3_filter];
                            $additional_list[] = implode('.', array_filter($phone));
                            
                            echo implode(", ", array_filter($additional_list));
                            ?>   
                        </span>
                       
                    </p>
                    <i class="fa fa-plus"></i> 
                </div>
            </div>
        <input type="submit" id="search-btn" class="apply-filters" value="[LABEL-APPLY-FILTERS]" />
        </form>
    </div> 
</div>


<div id="location_box" class="modal_full" style="display: none;">
    <div class="header_box">
        <a href="javascript:void(0)" id="close_location"><i class="fa fa-arrow-left"></i></a> 
        <h3> [LABEL-CITY-TITLE] </h3>
    </div>
    <div class="body_container">
        
        <div class="suggest-container">
            <input type="hidden" name="lang" id="lang" value="<?php echo \Core\Utils\Session::getLocale();?>" />
            <div class="form-label-group">
                <input type="text" id="search_box_city" name="search_box_city" class="form-control <?php echo ($display_location != '')? 'focused' : '';?>" placeholder="[INPUT-SEARCH-CITY]" value="<?php echo $display_location;?>">
                <label for="search_box_city">[LABEL-SELECT]&nbsp;[LOCATION-LABEL]</label>
              </div>

            <input type="hidden" name="category_code_city" id="category_code_city" value="<?php echo $category_code;?>" />
            <ul id="suggesstion-box-city" class="city_select city"></ul>
        </div>
            <div class="body_box">
                
                <ul class="dropdown_zones">        
                    <?php foreach($cities_with_zones as $key => $cities):?>
                        <li class="zone_select <?php echo $current_area_code == $key ? "active": '';?>">
                            <span class="region_title"><?php echo $cities['region']['name'];?></span>
                            <ul class="dropdown_cities" style="display: <?php echo $current_area_code == $key ? 'block': 'none';?>;">
                                <li>
                                    <a href="javascript:void(0)" class="region-box" data-title="<?php echo $cities['region']['name'];?>" data-slug="<?php echo $key;?>" data-url="<?=\Modules\Humpchies\AdController::getRegionUrlV2($category_code, $key);?>">
                                        [LABEL-ALL-CITIES]
                                    </a>
                                </li> 
                                <?php foreach($cities['cities'] as $newkey => $city_details): ?>
                                    <li>
                                        <a href="javascript:void(0)" class="suggested-city-location" data-slug="<?php echo $city_details['slug'];?>" data-url="<?=\Modules\Humpchies\AdController::getListURLV2($category_code, $city_details['slug'])?>" title="<?=\Modules\Humpchies\AdController::getListHrefTitle(@$category_code, $city_details['name'])?>">
                                            <?php echo $city_details['name']?>
                                        </a>
                                    </li>
                                <?php endforeach;?>   
                            </ul>
                        </li>
                    
                    <?php endforeach;?>
                </ul>     
            </div>
        
    </div> 
</div>

<div id="availability_filter" class="modal_full" style="display: none;">
    <div class="header_box">
        <a href="javascript:void(0)" id="close_availability_filter"><i class="fa fa-arrow-left"></i></a> 
        <h3> [LABEL-LOCATION-AVAILABLE] </h3>
        <a href="javascript: void(0)" class="clear-all" id="clear_availability_filter">[FILTERS-CLEAR] <i class="fa fa-trash"></i></a>
    </div>
    <div class="body_container">
        <div class="body_box">
            <div class="radio-group">      
                <input type="radio" id="location_all" name="location_filters" value="all" data-label="[LABEL-LOCATION-ALL]" <?php echo $location_filter == "all"? 'checked' : '';?> />
                <label for="location_all">[LABEL-LOCATION-ALL]</label>
                <br/>    
                <input type="radio" id="location_incall" name="location_filters" value="incalls" data-label="[LABEL-LOCATION-INCALL]" <?php echo $location_filter == "incalls"? 'checked' : '';?> />
                <label for="location_incall">[LABEL-LOCATION-INCALL]</label>
                <br/>
                <input type="radio" id="location_outcalls" name="location_filters" value="outcalls" data-label="[LABEL-LOCATION-OUTCALL]" <?php echo $location_filter == "outcalls"? 'checked' : '';?> />
                <label for="location_outcalls">[LABEL-LOCATION-OUTCALL]</label>
                
          </div>
          <input type="submit" id="availability-btn" class="apply-filters" value="[LABEL-APPLY-FILTERS]" />
        </div>
    </div>
</div>

<div id="services_filter" class="modal_full" style="display: none;">
    <div class="header_box">
        <a href="javascript:void(0)" id="close_services_filter"><i class="fa fa-arrow-left"></i></a> 
        <h3> [LABEL-SERVICES] </h3>
        <a href="javascript: void(0)" class="clear-all" id="clear_services_filter">[FILTERS-CLEAR] <i class="fa fa-trash"></i></a>
    </div>
    <div class="body_container">
        <div class="body_box">
            <div class="checkbox-group">      
               <?php foreach($tags as $tag):?>
                    <input class="styled-checkbox" name="selected_services[]" id="services-<?php echo $tag['slug'];?>" type="checkbox" value="<?php echo $tag['slug'];?>" data-label="<?php echo $tag['name'];?>" <?php echo in_array($tag['slug'], explode(",", $services_filter))? 'checked' : '';?> />
                    <label for="services-<?php echo $tag['slug'];?>"><?php echo $tag['name'];?></label>
                    <br/>
               <?php endforeach;?>
                
          </div>
          <input type="submit" id="services-btn" class="apply-filters" value="[LABEL-APPLY-FILTERS]" />
        </div>
    </div>
</div>


<div id="prices_filter" class="modal_full" style="display: none;">
    <div class="header_box">
        <a href="javascript:void(0)" id="close_prices_filter"><i class="fa fa-arrow-left"></i></a> 
        <h3> [LABEL-PRICE-NEW] </h3>
        <a href="javascript: void(0)" class="clear-all" id="clear_prices_filter">[FILTERS-CLEAR] <i class="fa fa-trash"></i></a>
    </div>
    <div class="body_container">
        <div class="body_box">
            <div class="checkbox-group">      
               <?php foreach($price_intervals as $key => $price):?>
                    <input class="styled-checkbox" name="selected_prices[]" id="prices-<?php echo $key;?>" type="checkbox" value="<?php echo $key;?>" data-label="<?php echo $price;?>" <?php echo in_array($key, explode(",", $prices_filter))? 'checked' : '';?> />
                    <label for="prices-<?php echo $key;?>"><?php echo $price;?></label>
                    <br/>
               <?php endforeach;?>
                
          </div>
          <input type="submit" id="prices-btn" class="apply-filters" value="[LABEL-APPLY-FILTERS]" />
        </div>
    </div>
</div>


<div id="additional_filter" class="modal_full" style="display: none;">
    <div class="header_box">
        <a href="javascript:void(0)" id="close_additional_filter"><i class="fa fa-arrow-left"></i></a> 
        <h3> [FILTERS-MORE] </h3>
        <a href="javascript: void(0)" class="clear-all" id="clear_additional_filter">[FILTERS-CLEAR] <i class="fa fa-trash"></i></a>
    </div>
    <div class="body_container">
        <div class="body_box">
            
            <!-- add phone here -->
            <div class="form-group bordered">
                <h3 style="margin-bottom: 10px;">[LABEL-TELEPHONE]</h3>   
                <div class="inline-inputs">
                    <input type="tel" name="selected_phone1" id="selected_phone1" class="form-control phone_section white-box" data-value="phone1" maxlength="3" placeholder="000" value="<?php echo $phone1_filter;?>" style="width: 47px;" />
                    <input type="tel" name="selected_phone2" id="selected_phone2" class="form-control phone_section white-box" data-value="phone2" maxlength="3" placeholder="000" value="<?php echo $phone2_filter;?>"  style="width: 47px;"/>
                    <input type="tel" name="selected_phone3" id="selected_phone3" class="form-control phone_section white-box" data-value="phone3" maxlength="4" placeholder="0000" value="<?php echo $phone3_filter;?>" style="width: 56px;" />
                </div>
            </div>
            
            <div class="checkbox-group">      
                
                <input class="styled-checkbox" name="selected_real_photos" id="selected_real_photos" type="checkbox" value="real_photos" data-label="[LABEL-REAL-PHOTO]" <?php echo $real_photos_filter == 1? 'checked' : '';?> />
                <label for="selected_real_photos">[LABEL-REAL-PHOTO]</label>
                 <br/> 
                <input class="styled-checkbox" name="selected_has_comments" id="selected_has_comments" type="checkbox" value="has_comments" data-label="[HAS-COMMENTS-LABEL]" <?php echo $has_comments_filter == 1? 'checked' : '';?> />
                    <label for="selected_has_comments">[HAS-COMMENTS-LABEL]</label>
                    <br/>
                    <input class="styled-checkbox" name="selected_accepts_comments" id="selected_accepts_comments" type="checkbox" value="accepts_comments" data-label="[ACCEPTS-COMMENTS-LABEL]" <?php echo $accepts_comments_filter == 1? 'checked' : '';?> />
                    <label for="selected_accepts_comments">[ACCEPTS-COMMENTS-LABEL]</label>
               
                 
            </div>
            
          <input type="submit" id="additional-btn" class="apply-filters" value="[LABEL-APPLY-FILTERS]" />
        </div>
    </div>
</div>