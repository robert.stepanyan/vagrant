<div class="important">
    <?php if (!empty($ticketActions)) : ?>
    <p>[LABEL-NEW-IMPORTANT-MESSAGE]</p>
    <ul>
        <?php foreach($ticketActions as $ticket) : ?>
        <li><p>[LABEL-TICKET-NUMBER]: #<?= $ticket['id']; ?> <a href="<?=\Core\Utils\Navigation::urlFor('support', 'ticket', array('id'=>$ticket['id']))?>">[LABEL-READ]</a></p></li>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>
</div>