<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents one ad and its details in a list of ads.
 */

 ?>

<div class="ad_element_box">
    <div class="box sidebar">
        <div class="<?=($ad['thumb_info']['height'] === 180) ? 'thumb_portrait' : (($ad['thumb_info']['height'] === 120) ? 'thumb_square' : 'thumb_landscape')?>">
            <a href="<?=$ad['url']?>" title="<?=$ad['href_title']?>">
                <img src="<?=$ad['thumb_info']['src']?>" width="83>" alt="<?=$ad['title']?>" />
            </a>
        </div>
    </div>
    <div class="box content">
        <div class="user_content_list_element_box_header">
            <h2 class="title">
                <a href="<?=$ad['url']?>" title="<?=$ad['href_title']?>">
                    <?=(strlen($ad['title']) > 24) ? substr($ad['title'],0,23) .'...': $ad['title'] ?>
                </a>
            </h2>
            <?php if (true === @$ad['vip']) { ?>
            <span class="vip_flag">VIP <i class="fa fa-star"></i></span>
            <?php }?>
            <?php if (true === @$ad['premium']) { ?>
            <span class="vip_flag" style="    top: 20px;    left: 454px;">Premium</span>  <?php } ?>
        </div>
        <div class="user_content_list_element_box_content">
            <p class="description_box">
                <?=substr($ad['description'],0,65)?>
            </p>
            <section>
                <article class="clearfix">
                    <ul>
                        <li>
                            <i class="fa fa-th-list"></i>
                            <span rel-data="[LABEL-CATEGORY]: "></span>
                            <a href="<?=$ad['category_url']?>" title="<?=$ad['category_href_title']?>" >
                                <?=$ad['category_name']?>
                            </a>
                        </li>
                        <li>
                            <i class="fa fa-map-marker"></i>
                            <span rel-data="[LABEL-CITY]: "></span>
                        
                            <a href="<?=$ad['category_url_w_city']?>" title="<?=$ad['category_href_w_city_title']?>" >
                                <?=$ad['city_name']?>
                            </a>
                        </li>                                        
                        <li>
                            <i class="fa fa-calendar"></i>
                            <span rel-data="[LABEL-POSTED]:&nbsp;"></span>
                        
                            <?=$ad['date_released']?>
                        </li>
                    </ul>  
                    <? if($ad['id'] != 1579901): ?>
                    <article class="share" style="display: none">
                        <nav class="social">
                            <div class="container_box">
                                <div class="container_box_drawer" onclick="function open(){classList.toggle('open')}; open()">
                                    <a href="javascript:void 0;"><i class="share" aria-hidden="true"></i></a>
                                    <a class="share-link" href="https://twitter.com/share?url=<?php echo rawurlencode(\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'). $ad['url'])?>" rel="nofollow" target="_blank">
                                        <i class="fa fa-twitter" aria-hidden="true" onclick="addSocialShare('twitter', 'listing');"></i>
                                    </a>
                                </div>
                            </div>
                        </nav>
                    </article>
                    <? endif; ?>
                </article>
            </section>
        </div>
    </div>
</div>