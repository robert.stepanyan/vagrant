<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents one ad and its details in a list of ads.
 */

 ?>

<li class="ad_element_box clearfix">
    <div class="box sidebar">
        <div class="<?=($ad_m['thumb_info']['height'] === 180) ? 'thumb_portrait' : (($ad_m['thumb_info']['height'] === 120) ? 'thumb_square' : 'thumb_landscape')?>">
            <a href="<?=$ad_m['url']?>" title="<?=$ad_m['href_title']?>">
                <img src="<?=$ad_m['thumb_info']['src']?>" alt="<?=$ad_m['title']?>" />
            </a>
        </div>
    </div>
    <div class="box content">
        <div class="user_content_list_element_box_header">
            <h2 class="title">
                <a href="<?=$ad_m['url']?>" title="<?=$ad_m['href_title']?>">
                    <?=$ad_m['title']?>
                </a>
            </h2>
            <?php if (true === @$ad_m['vip']) { ?>
            <span class="vip_flag"> <i class="fa fa-star"></i></span>
            <?php }?>
            <?php if (true === @$ad_m['premium']) { ?>
            <span class="vip_flag premium" style="    top: 20px;    left: 454px;"></span>  <?php } ?>
        </div>
        <div class="user_content_list_element_box_content">
            <p class="description_box" style="color:#999;">
                <?=substr($ad_m['description'],0,65)?>
            </p>
            <section>
                <article>
                    <ul >
                        <li>
                            <i class="fa fa-th-list"></i>
                            <span class="bg_labels category_<?php echo (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';?>"></span>
                            <a href="<?=$ad_m['category_url']?>" title="<?=$ad_m['category_href_title']?>" >
                                <?=$ad_m['category_name']?>
                            </a>
                        </li>
                        <li>
                            <i class="fa fa-map-marker"></i>
                            <span class="bg_labels city_<?php echo (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';?>"></span>
                            <a href="<?=$ad_m['category_url_w_city']?>" title="<?=$ad_m['category_href_w_city_title']?>" >
                                <?=$ad_m['city_name']?>
                            </a>
                        </li>
                        <li>
                            <i class="fa fa-calendar"></i>
                            <span class="bg_labels posted_<?php echo (\Core\Utils\Session::getLocale() == 'fr-ca')? 'fr' : 'en';?>"></span>
                            <?=$ad_m['date_released']?>
                        </li>
                    </ul>
                   <? if($ad_m['id'] != 1579901): ?>
                    <article class="share">
                        <nav class="social">
                            <div class="container_box">
                                <div class="container_box_drawer" onclick="function open(){classList.toggle('open')}; open()">
                                    <a href="javascript:void 0;"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                    <a class="share-link" href="https://twitter.com/share?url=<?php echo rawurlencode(\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'). $ad_m['url'])?>" rel="nofollow" target="_blank">
                                        <i class="fa fa-twitter" aria-hidden="true" onclick="addSocialShare('twitter', 'listing');"></i>
                                    </a>
                                   
                                </div>
                            </div>
                        </nav>
                    </article>
                    <? endif; ?>
                </article>
            </section>
        </div>
    </div>
</li>