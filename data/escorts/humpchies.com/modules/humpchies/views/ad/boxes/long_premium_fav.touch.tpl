<?php if(@$ad['status'] == 'active'):?>
<div class="add_list_box <?php echo (($ad['date_bumped'] + (60*60*24)) - time() > 0)? 'more_images' : '';?>">
    <div class="add_list_box_thumb">
        <div class="<?=($ad['thumb_info']['height'] === 180) ? 'thumb_portrait' : (($ad['thumb_info']['height'] === 120) ? 'thumb_square' : 'thumb_landscape')?>">
            <a href="<?=$ad['url']?>" title="<?=$ad['href_title']?>">
                <img src="<?=$ad['thumb_info']['src']?>" width="80" height="<?=($ad['thumb_info']['height'] === 180) ? 120 : (($ad['thumb_info']['height'] === 120) ? 80 : 53)?>" alt="<?=$ad['title']?>" />
            </a>
        </div>
        
        <?php 
         if($ad['date_bumped'] + (60*60*24) - time() > 0){
        $no_thumbs = 0;?>
        <?php if(!empty($ad['all_images']['thumbs'])):?>
            
             <?php   foreach(array(\Core\Utils\Image::ORIENTATION_LANDSCAPE, \Core\Utils\Image::ORIENTATION_PORTRAIT, \Core\Utils\Image::ORIENTATION_SQUARE) as $thumb_orientation)
                    {
                        if(isset($ad['all_images']['thumbs'][$thumb_orientation]))
                        {
                            foreach($ad['all_images']['thumbs'][$thumb_orientation] as $thumb)
                            {?>
                                <?php 
                                 if($no_thumbs > 2 ) break;
                                $no_thumbs++;?>
                                <div class="thumb_square_small">
                                    <a href="<?=$ad['url']?>" title="<?=$ad['href_title']?>">
                                        <span class="small_thumb" style="background:url('<?=$thumb['href']?>'); background-size:cover;"  title="<?=$ad['title']?>"></span> 
                                    </a>       
                                </div>
            <?php           }    
                        }    
                    }?>
        <?php endif;?>
        <?php for ($i = $no_thumbs; $i < 2; $i++):?>
        <div class="thumb_square_small">
            <a href="<?=$ad['url']?>" title="<?=$ad['href_title']?>">
                <span  class="small_thumb" style="background:url('https://gui.humpchies.com/gui_ads/filler.jpg'); background-size:cover;"  title="<?=$ad['title']?>"></span> 
            </a>       
        </div>
        <?php endfor;
        }
        ?>
        
    </div>
    <div class="add_list_box_data_vip">
        <div class="add_list_box_header">
            <h2>
                <a href="<?=$ad['url']?>" title="<?=$ad['href_title']?>">
                    <?=$ad['title']?>
                </a>
            </h2>
        </div>
        <div class="add_list_box_content_vip">
            <p class="description description_box">
                <?=$ad['description']?>
            </p>
            <p>
                <i class="fa fa-th-list"></i>
                <span class="spans" rel-data="[LABEL-CATEGORY]: "></span>
                &nbsp;
                <a href="<?=$ad['category_url']?>" title="<?=$ad['category_href_title']?>" >
                    <?=$ad['category_name']?>
                </a> 
            </p>
            <p>
                <i class="fa fa-map-marker"></i>
                <span class="spans" rel-data="[LABEL-CITY]:&nbsp;"></span>
                &nbsp;
                <a href="<?=$ad['category_url_w_city']?>" title="<?=$ad['category_href_w_city_title']?>" >
                    <?=$ad['city_name']?>
                </a>
                <span class="vip_flag premium" style="font-size: 12px;right: -28px;top: -8px;"> </span>
            </p>
            <?php if(0):?>
                <p>
                    <i class="fa fa-calendar"></i>
                    <span class="spans" rel-data="[LABEL-POSTED]:&nbsp;"></span>
                    &nbsp;
                    <?=$ad['date_released']?>
                </p>
            <?php endif;?>
            <div class="social remove-favorite" style="display: flex; justify-content: flex-end; margin-right:10px;">
                <div class="title" style="width: auto; margin-right: 10px; display: flex; align-items: center; align-content: center;">                     
                    <span class="spans" rel-data="[LABEL-REMOVE-FAV]"></span>
                </div>
                <a href="javascript:void(0)" data-id="<?php echo $ad['id'];?>" class="btn-remove-fav">
                     <i style="background: none;width: auto;color: #ff9900;font-size: 12px; margin-right: 0;" class="fa fa-heart" aria-hidden="true" ></i>
                </a>
            </div>
        </div>
    </div>
</div>
<?php else: ?>
    <article class="add_list_box" style="display: flex; align-items: center;flex-direction: column;min-height: 70px;">
        <p style="margin: 0 10px;padding-top: 24px;">[NO-FAVORITES]</p>
        <div class="social remove-favorite" style="display: flex; flex-direction: row; align-items: center; justify-content: flex-start; margin-top:0">
            <div class="title" style="width: auto; margin-right: 10px; display: flex; align-items: center; align-content: center;">                     
                <span class="spans" rel-data="[LABEL-REMOVE-FAV]"></span>
            </div>
            <a href="javascript:void(0)" data-id="<?php echo $ad['id'];?>" class="btn-remove-fav">
                <i style="background: none;width: auto;color: #ff9900;font-size: 12px; margin-right: 0;" class="fa fa-heart" aria-hidden="true" ></i>
            </a> 
        </div> 
    </article>
<?php endif;?>