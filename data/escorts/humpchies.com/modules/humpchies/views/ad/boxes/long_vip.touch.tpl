<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents one ad and its details in a list of ads for mobile.
 */
?>
<div class="add_list_box <?php echo (($ad['date_bumped'] + (60*60*24)) - time() > 0)? 'more_images' : '';?>" onclick="location.href='<?php echo $ad['url']?>'" title="<?=$ad['href_title']?>">
    <div class="add_list_box_thumb">
        <div class="<?=($ad['thumb_info']['height'] === 180) ? 'thumb_portrait' : (($ad['thumb_info']['height'] === 120) ? 'thumb_square' : 'thumb_landscape')?>">
            <a href="<?=$ad['url']?>" title="<?=$ad['href_title']?>">
                <img src="<?=$ad['thumb_info']['src']?>" width="80" height="<?=($ad['thumb_info']['height'] === 180) ? 120 : (($ad['thumb_info']['height'] === 120) ? 80 : 53)?>" alt="<?=$ad['title']?>" />
            </a>
        </div>
        <?php 
          if($ad['date_bumped'] + (60*60*24) - time() > 0){
        $no_thumbs = 0;?>
        <?php if(!empty($ad['all_images']['thumbs'])):?>
            
             <?php   foreach(array(\Core\Utils\Image::ORIENTATION_LANDSCAPE, \Core\Utils\Image::ORIENTATION_PORTRAIT, \Core\Utils\Image::ORIENTATION_SQUARE) as $thumb_orientation)
                    {
                        if(isset($ad['all_images']['thumbs'][$thumb_orientation]))
                        {
                            foreach($ad['all_images']['thumbs'][$thumb_orientation] as $thumb)
                            {?>
                                <?php 
                                 if($no_thumbs > 2 ) break;
                                $no_thumbs++;?>
                                <div class="thumb_square_small">
                                    <a href="<?=$ad['url']?>" title="<?=$ad['href_title']?>">
                                        <span class="small_thumb" style="background:url('<?=$thumb['href']?>'); background-size:cover;"  title="<?=$ad['title']?>"></span> 
                                    </a>       
                                </div>
            <?php           }    
                        }    
                    }?>
        <?php endif;?>
        <?php for ($i = $no_thumbs; $i < 2; $i++):?>
        <div class="thumb_square_small">
            <a href="<?=$ad['url']?>" title="<?=$ad['href_title']?>">
                <span  class="small_thumb" style="background:url('https://gui.humpchies.com/gui_ads/filler.jpg'); background-size:cover;"  title="<?=$ad['title']?>"></span> 
            </a>       
        </div>
        <?php endfor; 
        }?>
        
    </div>
    <div class="add_list_box_data_vip">
        <div class="add_list_box_header">
            <h2>
                <a href="<?=$ad['url']?>" title="<?=$ad['href_title']?>">
                    <?=$ad['title']?>
                </a>
            </h2>
        </div>
        <div class="add_list_box_content_vip">
            <p class="description description_box">
                <?=$ad['description']?>
            </p>
            <p>
                <i class="fa fa-th-list"></i>
                <span class="spans" rel-data="[LABEL-CATEGORY]: "></span>
                &nbsp;
                <a href="<?=$ad['category_url']?>" title="<?=$ad['category_href_title']?>" >
                    <?=$ad['category_name']?>
                </a> 
            </p>
            <p>
                <i class="fa fa-map-marker"></i>
                <span class="spans" rel-data="[LABEL-CITY]:&nbsp;"></span>
                &nbsp;
                <a href="<?=$ad['category_url_w_city']?>" title="<?=$ad['category_href_w_city_title']?>" >
                    <?=$ad['city_name']?>
                </a>
                <span class="vip_flag"> <i class="fa fa-star"></i></span>            
            </p>
            <p>
                <i class="fa fa-calendar"></i>
                <span class="spans" rel-data="[LABEL-POSTED]:&nbsp;"></span>
                &nbsp;
                <?=$ad['date_released']?>
            </p>
            
            <?php if(($ad['date_bumped'] + (60*60*24)) - time() > 0):?>
                 <svg class="svg-arrow" 
                 xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink"
                 width="20px" height="27px" preserveAspectRatio="none" viewBox="0 0 30 50">
                <path fill-rule="evenodd"  fill="rgb(253, 152, 39)"
                 d="M14.000,12.000 L14.000,20.000 L6.000,20.000 L6.000,12.000 L-0.000,12.000 L10.000,-0.000 L20.000,12.000 L14.000,12.000 ZM14.000,24.000 L6.000,24.000 L6.000,22.000 L14.000,22.000 L14.000,24.000 ZM14.000,27.000 L6.000,27.000 L6.000,26.000 L14.000,26.000 L14.000,27.000 Z"/>
                </svg>
            <?php else:?>
                <? if($ad['id'] != 1579901 && $ad['verification_status'] != "verified"): ?>
            <article class="share">
                <nav class="social">
                    <div class="container_box">
                        <div class="container_box_drawer" onclick="function open(){classList.toggle('open')}; open()">
                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                                <a href="https://twitter.com/share?url=<?php echo rawurlencode(\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'). $ad['url'])?>" rel="nofollow" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true" onclick="addSocialShare('twitter', 'listing');"></i>
                            </a>
                           
                        </div>
                    </div>
                </nav>
            </article>
        <? endif; ?>
             <? endif; ?>
             <?php if( $ad['verification_status'] == "verified") {?>
                        <span class="real-photos" style ='left:auto; right:0px'>[LABEL-REAL-PHOTO] </span>
            <? } ?> 
        </div>
    </div>
</div>
