
<div class="govazd">
    <div class="pic"> 
         <img class="<?=($ad['thumb_info']['height'] === 180) ? 'thumb_portrait' : (($ad['thumb_info']['height'] === 120) ? 'thumb_square' : 'thumb_landscape')?>" src="<?=$ad['thumb_info']['src']?>" width="<?=$ad['thumb_info']['width']?>" height="<?=$ad['thumb_info']['height']?>" alt="<?=$ad['title']?>" /> 
       <?php if( $ad['verification_status'] == "verified") {?>
        <div class="real-photos">[LABEL-REAL-PHOTO]</div>
       <? } ?>
    </div>
    <div class="content">
        <div class="title"><?=$ad['title']?> ( #<?= $ad['id'] ?> ) 
            <?php if($ad['fake_status'] == 1): ?>
                <span class="fake-pics">
                     [LABEL-FAKE-PICS] 
                </span>
            <?php endif; ?>        
        </div>
        <div class="sub-title">
            <strong>[LABEL-CATEGORY]:</strong>&nbsp;<?=$ad['category_name']?>&nbsp;|&nbsp;
            <strong>[LABEL-CITY]:</strong>&nbsp;<?=$ad['city_name']?> &nbsp;
            <?php  echo '|&nbsp;[LABEL-NO-VIEWS]:&nbsp; ' . (($ad['no_visits'] > 0) ? $ad['no_visits'] : 0) . ' &nbsp;';?>
             <?php if($ad['status'] === 'new'){?>
                [LABEL-PENDING]
                <?php }
                    elseif($ad['is_updated'] == '1' ){
                    ?>
                    <span style='color:#ff0000; font-size: 14px;' >
                       |&nbsp;[LABEL-AWAITING-REVISION-ATENTION]
                    </span>
                    <?php
                    }elseif($ad['status'] == "inactive" ){?>
                |&nbsp;[LABEL-INACTIVE]
                
                <?php }else{
                        if($ad['date_released']){
                            echo "|&nbsp;" . $ad['date_released'];
                        }
                }?>
            &nbsp;
            <?php if(isset($ad['status']) && $ad['status'] === "paused"): ?>
            
                <?php echo "|&nbsp;" . $ad['status'];
                    if(isset($ad['premium_status'])) {
                        echo ' | [PREFIX-PREMIUM]<b> &nbsp; ' . date("m-d-Y", strtotime($ad['package']['end'])) . '</b> &nbsp;';
                    }
                ?>
           
        <?php endif; ?>
        </div>
        <div class="desc description_box">
             <?=$ad['description']?>
        </div>
        <div class="paid-buttons">
           
            <? if($ad['status'] == 'active'): ?>
			
				<?/* if(\Core\Utils\Session::getCurrentUserID() == 63661 ):*/ ?>
					<div class="bump">
						<a href="<?=\Core\Utils\Navigation::urlFor('payment', 'bumps')?>?ad_id=<?= $ad['id'] ?>">[LABEL-BUMP]&nbsp;<i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></a>
					</div>
				<?/* endif */?>
				<div class="premium">
                <? if(isset($ad['package']) && $ad['premium_status']): ?>
                    <a>
                        <div class="top-up static">
                            <?php if($ad['premium_status'] == 2){ 
                                if($ad['package_id'] < 12){
                            ?> 
                                [PREFIX-VIP]  <?= date("m-d-Y", strtotime($ad['package']['end'])) ?>
                            <?php }else {?>
                                [PREFIX-PREMIUM]  <?= date("m-d-Y", strtotime($ad['package']['end'])) ?>
                                
                            <?php  }
                            }elseif($ad['premium_status'] == 1){ ?>
                                [PREFIX-PREMIUM-PENDING]
                            <?php } ?>
                        </div>
                    </a> 
                <? else: ?>
                    <a href="<?=\Core\Utils\Navigation::urlFor('payment', 'checkout')?>?ad_id=<?= $ad['id'] ?>&city=<?= $ad['city'] ?>">
                        <div class="top-up">
                        [LABEL-TOPUP]&nbsp;<i class="fa fa-star" aria-hidden="true"></i>
                        </div>
                    </a> 
                <? endif; ?>
                 </div>
            <? endif; ?>
            
        </div>
        <?php 
         if($ad['underRevision'] && $ad['is_updated'] && 0) {?>
            <div class ='revision'>[LABEL-AWAITING-REVISION-ATENTION]</div>
        <?php } ?>
    </div>
    <div class="buttons ">
        <? if(@$ad['repost_url']): ?>
            <div><a href="<?=$ad['repost_url']?>"><span>[LABEL-REPOST]</span><i class="fa fa-refresh"></i></a></div>
        <? else: ?>
            <div><a class="disabled" href="#"><span>[LABEL-REPOST]</span><i class="fa fa-refresh"></i></a></div>
        <? endif;?>

        <?if(@$ad['stop_url'] && (!isset($ad['premium_status']) || isset($ad['premium_status']) || $ad['status'] != 'active') ) :?>
             <div><a href="<?=$ad['stop_url']?>"><span>[LABEL-STOP]</span><i class="fa fa-toggle-on"></i></a></div>
        <? else:?> 
             <div><a class="<?= ( $ad['status'] == 'new'  ?  'disabled' : '' ) ?>" href="<?=$ad['repost_url']?>"><span>[LABEL-START]</span><i class="fa fa-toggle-off"></i></a></div>
        <? endif; ?>
        
        <?php if (@$ad['status'] !== 'new'): ?>
                <div><a href="<?=$ad['edit_url']?>"><span>[LABEL-EDIT]</span><i class="fa fa-edit"></i></a></div>
              
        <?php else: ?>
                <div><a class="disabled" href="#"><span>[LABEL-EDIT]</span><i class="fa fa-edit"></i></a></div>
               
        <?php endif; ?>
        <?php if($ad['thumb_info']['real']){ //TODO HADCODED ID
        ?>
                    <?php if($ad['verification_status']=='verified'){
                    ?>
                    <div style='color:#009A14; justify-content: space-between;'><span>[LABEL-VERIFIED]</span><i class="fa fa-check"></i></div>
                    
                    <?php
                    }else{
                    ?>
                    <div><a href="<?php echo \Core\Utils\Navigation::urlFor('ad', 'verify'); ?>?id=<?=$ad['id']?>"><span>[LABEL-VERIFY]</span><i class="fa fa-check"></i></a></div>
                    <?php
                    }
                    ?>
        <?php } ?>
        
        <? if(!isset($ad['premium_status']) || $ad['status'] != 'active'): ?>
          <div> <a class="delete " href="<?=$ad['delete_url']?>"><span>[LABEL-DELETE]</span><i class="fa fa-trash-o"></i></a></div>           
        <? endif; ?>
    </div>
    
    
</div>
