<?php 
    $startDate = strtotime("2021-06-30");
    $endDate = strtotime("2021-07-03");
    if(0 && \Core\Utils\Session::getCurrentUserID() == 63661 || (time() >= $startDate && time() < $endDate)): // only testme
?>
                                                                                                                        
<div id="july-special">
    <div class="inner">
        <div class="content-box">    
            <h2>[LABEL-JULY-TITLE-MOB]</h2>
            <p><strong>[LABEL-JULY-TEXT-A-MOB]</strong>&nbsp;[LABEL-JULY-TEXT-B-MOB]</p>
            <ul>
                <li>[LABEL-JULY-LIST-A-MOB]</li>
                <li>[LABEL-JULY-LIST-B-MOB]</li>
            </ul>
        </div>
        <?php if(0):?>
            <div class="actions-box">
                <a href="#" class="btn-orange">Buy premium</a>    
                <a href="<?=\Core\Utils\Navigation::urlFor('payment', 'bumps-purchase')?>" class="btn-black">Buy credits</a>    
            </div>
        <?php endif;?>
    </div>
</div>
<?php endif;?>
<?php if(!isset($_COOKIE['ebook-special'])):?>
<div id="ebook-special">
    
    <div class="inner">
        <div class="title-box">
            <h2>[LABEL-EBOOK-TITLE-A]&nbsp;[LABEL-EBOOK-TITLE-B]</h2>
        </div>
        <div class="content-box">    
           
            <a href="https://www.escortbook.com/signup?utm_source=humpchies&utm_medium=bannermob&utm_campaign=bannermob" target="_blank" class="btn-orange">[BTN-GO-EBOOK]</a>
             <a href="javscript:void(0)" id="hide-special" class="btn-outline">[BTN-HIDE-EBOOK]</a>
        </div>
    </div>
</div>
<?php endif;?>


<?php if(0):?>
<div id="holiday-special">
    
    <div class="inner">
        <div class="title-box">
            <h2>[LABEL-HOLIDAY-TITLE]</h2>
            <a href="javascript:void(0)" id="close-holiday"><i class="fa fa-times-circle"></i></a>
        </div>
        <div class="content-box">    
            <p>[LABEL-HOLIDAY-DESC]</p>
            <div class="details">
                <ul>
                    <li>[LABEL-HOLIDAY-OPTION-1]</li>
                    <li>[LABEL-HOLIDAY-OPTION-2]</li>
                </ul>
                <a href="<?=\Core\Utils\Navigation::urlFor('payment', 'bumps-purchase')?>" class="btn-orange">[LABEL-HOLIDAY-CTA]</a>
            </div>
        </div>
    </div>
</div>
<?php endif;?>