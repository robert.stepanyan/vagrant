<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents one ad and its details in a list of ads that belong to the
 * user himself.
 */
?>

<style>
    /*.user_content_list_element_box_head_name_status {
        padding: 10px;
        padding-right: 5px;
        white-space: nowrap;
        margin-left: auto;
    }*/
    
    .user_content_list_element_box_head_title {
        padding-bottom: 15px;
        color: white;
    }
    
    .user_content_list_element_box_body {
        
    }
    
    
    .ad_details {
        padding-top: 5px;
        color: gray;
        font-size: 13px;
        
    }
</style>

<div class="user_content_list_element_box" style="position: relative;">
    
            
            <?php
                $rep_url = '';
                $stp_url = '';
                $edt_url = '';
                $del_url = '';
                
                if(@$ad['repost_url']) {
                    $rep_url = $ad['repost_url'];
                }
                if(@$ad['stop_url'] && (!isset($ad['premium_status']) || isset($ad['premium_status']) || $ad['status'] != 'active') ) {
                      $stp_url = $ad['stop_url'];
                   } else {
                        $stp_url = $ad['repost_url'];
                   }
                         
                if (@$ad['status'] !== 'new') {
                    $edt_url = $ad['edit_url'];
                }
                
                
                
                $gov_status = '';
                if($ad['status'] == 'active'){
                    if(isset($ad['package']) && $ad['premium_status']) {
                        $gov_status = $ad['premium_status'];
                    } else {
                        $gov_status = \Core\Utils\Navigation::urlFor('payment', 'checkout')."?ad_id=" . $ad['id'] . "&city=" . $ad['city'];
                    }
                }
                
                if(!isset($ad['premium_status']) || $ad['status'] != 'active'){
                    $del_url = $ad['delete_url'];
                }
            
                
            ?>
            
            
            
     <div class="user_content_list_element_box_body pa_popup" data-photo = '<?=$ad["thumb_info"]["src"]?>' data-repost = '<?=$rep_url ?>' data-del = '<?=$del_url ?>'
          data-stop = '<?=$stp_url ?>' data-edit = '<?=$edt_url ?>' data-id = '<?=$ad["id"] ?>' data-premstatus = '<?=$gov_status ?>' data-title = '<?=$ad["title"] ?>'
          data-category = '<?=$ad["category_name"] ?>' data-city = '<?=$ad["city_name"] ?>' data-lang = '<?= \Core\Utils\Session::getLocale() ?>' 
          data-status = '<?=$ad["status"] ?>' data-verify = '<?=$ad["verification_status"] ?>' data-real="<?php echo ($ad['thumb_info']['real'])? 'real' : '';?>">
         
        <div class="user_content_list_element_box_body_pic main_pic">
            <div class="<?=($ad['thumb_info']['height'] === 180) ? 'thumb_portrait' : (($ad['thumb_info']['height'] === 120) ? 'thumb_square' : 'thumb_landscape')?>">
                <img src="<?=$ad['thumb_info']['src']?>" width="100%" height="124" alt="<?=$ad['title']?>" />
            </div>
          
        </div>
        <div class="user_content_list_element_box_body_content list_mob_ad">
            <div class="user_content_list_element_box_head_title">
                <div class="title_box">
                    <?=( strlen($ad['title']) > 27 ? substr($ad['title'], 0, 27 ).'...' : $ad['title']) ?>
                    <div>#<?= $ad['id'] ?></div>
                </div>    
                <div class="ad_details">
                    <strong>IN:</strong>&nbsp;<?=$ad['category_name']?>, <?=$ad['city_name']?>
                    <div class="user_content_list_element_box_city">
                        <strong>[LABEL-NO-VIEWS]:</strong>&nbsp;<?php echo (($ad['no_visits'] > 0) ? $ad['no_visits'] : 0)?>
                    </div>
                <?php if(isset($ad['status'])): ?>
                    <div class="user_content_list_element_box_head_name_status">
                        <?php 
                            if(isset($ad['premium_status'])) {
                                echo '[PREFIX-PREMIUM]<b> &nbsp; ' . date("m-d-Y", strtotime($ad['package']['end'])) . '</b> &nbsp;';
                            }
                        ?>
                    </div>
                <?php endif; ?>
                
                    <div class="user_content_list_element_box_head_date">
                      <?php 
                            if($ad['date_released'] !== 0){
                                echo $ad['date_released'];
                            }
                            ?>
            </div>
              <?php 
                 if( $ad['is_updated']) {?>
                    <div class ='revision' style="color:#ff0000; width: 149px;">[LABEL-AWAITING-REVISION-ATENTION]</div>
                <?php } ?>  
                </div>
                <?php if(0):?>
                    <!-- this section is no longer visible -->
                    <div class="user_content_list_element_box_cat"><strong>[LABEL-CATEGORY]:</strong>&nbsp;<?=$ad['category_name']?> </div>
                    <div class="user_content_list_element_box_city"><strong>[LABEL-CITY]:</strong>&nbsp;<?=$ad['city_name']?></div>
                <?php endif;?>
                
                
             
                <div class="right-arrow-get-in">Options <i class="fa fa-ellipsis-v"></i></div>      
        </div>
                       
        </div>
            
                 
    </div>
    <?php if( $ad['verification_status'] == "verified") {?>
                <div class="real-photos-left">[LABEL-REAL-PHOTO]</div>
             <? } ?>    
                 
</div>
           
