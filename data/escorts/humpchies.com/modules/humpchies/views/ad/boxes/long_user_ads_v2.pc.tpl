<?php
    $custom_class = '';
    switch(true) {
        case (@$ad_more['vip']):
            $custom_class = "vip";
            break;
        
        case (@$ad_more['premium']):
            $custom_class = "premium";
            break;
        
        case (($ad_more['date_bumped'] + (60*60*24)) - time() > 0):
            $custom_class = "bump";
            break;       
    }
?>

<article class="user_content_list_element_box <?php echo $custom_class;?>">

    <div class="user_content_list_element_box_thumb">
        <div class="thumb_layout <?=($ad_more['thumb_info']['height'] === 180) ? 'thumb_portrait' : (($ad_more['thumb_info']['height'] === 120) ? 'thumb_square' : 'thumb_landscape')?>">
            <a href="<?=$ad_more['url']?>" title="<?=$ad_more['href_title']?>">
                <span class="image" style="background-image:url(<?=$ad_more['thumb_info']['src']?>);"></span> 
            </a>
        </div>
        <?php 
            if($custom_class != '') {
                $no_thumbs = 0;
                
                if(!empty($ad_more['all_images']['thumbs'])) {
            
                    foreach(array(\Core\Utils\Image::ORIENTATION_LANDSCAPE, \Core\Utils\Image::ORIENTATION_PORTRAIT, \Core\Utils\Image::ORIENTATION_SQUARE) as $thumb_orientation) {
                        if(isset($ad_more['all_images']['thumbs'][$thumb_orientation]))
                        {
                            foreach($ad_more['all_images']['thumbs'][$thumb_orientation] as $thumb)
                            {?>
                                <?php if($no_thumbs >= 2 ) break;
                                $no_thumbs++;?>
                                <div class="thumb_square_small">
                                    <a href="<?=$ad_more['url']?>" title="<?=$ad_more['href_title']?>">
                                        <span class="small_thumb" style="background:url('<?=$thumb['href']?>'); background-size:cover;"  title="<?=$ad_more['title']?>"></span> 
                                    </a>       
                                </div>
            <?php           }    
                        }    
        }?>
        <?php };?>
        <?php for ($i = $no_thumbs; $i < 2; $i++):?>
        <div class="thumb_square_small">
            <a href="<?=$ad_more['url']?>" title="<?=$ad_more['href_title']?>">
                <span class="small_thumb" style="background:url('https://gui.humpchies.com/gui_ads/filler.jpg'); background-size:cover;"  title="<?=$ad_more['title']?>"></span> 
            </a>       
        </div>
        <?php endfor;
        }
        ?>
        
    </div>
    
    <div class="user_content_list_element_box_content">
        
        <h2 class="title">
            <a href="<?=$ad_more['url']?>" title="<?=$ad_more['href_title']?>">
                <?=$ad_more['title']?>
            </a>
        </h2>
        <div class="type-location">
            <a href="<?=$ad_more['category_url']?>" title="<?=$ad_more['category_href_title']?>" >
                <?=$ad_more['category_name']?>
            </a>
            <span rel-data="[LABEL-IN]&nbsp;"></span>
            <a href="<?=$ad_more['category_url_w_city']?>" title="<?=$ad_more['category_href_w_city_title']?>" >
                <?=$ad_more['city_name']?>
            </a>
        </div>
        
        <p class="description-box">
            <?=$ad_more['description']?>
        </p>
        
        <div class="bottom-container">
            <span><?=$ad_more['date_released']?></span>
            <a href="<?=$ad_more['url']?>" title="<?=$ad_more['href_title']?>" class="button">
                <span rel-data="[LABEL-DETAILS]"></span>
            </a>   
        </div>
        
        <?php if( $custom_class == 'bump'):?>
             <svg class="svg-arrow" 
             xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink"
             width="20px" height="27px" preserveAspectRatio="none" viewBox="0 0 30 50">
            <path fill-rule="evenodd"  fill="rgb(253, 152, 39)"
             d="M14.000,12.000 L14.000,20.000 L6.000,20.000 L6.000,12.000 L-0.000,12.000 L10.000,-0.000 L20.000,12.000 L14.000,12.000 ZM14.000,24.000 L6.000,24.000 L6.000,22.000 L14.000,22.000 L14.000,24.000 ZM14.000,27.000 L6.000,27.000 L6.000,26.000 L14.000,26.000 L14.000,27.000 Z"/>
            </svg>
        <?php endif;?>    
    </div>
    <?php if( $ad_more['verification_status'] == "verified") {?>
        <div class="real-photos">[LABEL-REAL-PHOTO]</div>
    <? } ?>
</article>
