<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents the site's ad creation page.
 */

 
  $city_list = \Modules\Humpchies\CityModel::getCitiesRegions();
 ?>
<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li><?=\Core\Controller::getTitle()?></li>
       
        
    </ul>    
 </nav>



<div class="wrapper_content">
    <div class="content_box">
        <div class="content_box_header">
            <h2 class="title"><?=\Core\Controller::getTitle()?></h2>
        </div>
        <div class="content_box_body">
            <div class="post_box">
                <form id="post_pad" method="post" enctype="multipart/form-data">
                    <div class="field">
                        <label>*[LABEL-CITY]:</label>
                        <div>                                    
                            <select name="city" data-must-be="">
                                 <?php foreach($city_list as $key => $list):?>
                                    <optgroup label="<?php echo $list['region']['name'];?>">
                                        
                                        <?php foreach($list['cities'] as $key => $city):?>
                                            <option value="<?php echo $city['slug'];?>" <?php echo (isset($_POST['city']) && $city['slug'] == $_POST['city'])? 'selected' : '';?>><?php echo $city['name'];?></option>
                                        <?php endforeach;?>
                                    </optgroup>    
                                 <?php endforeach;?>   
                             
                             
                             </select>   
                        </div>
                    </div>
                    <div class="field">
                        <label>*[LABEL-TITLE]:</label>
                        <div>
                            <input class="text" name="title" type="text" maxlength="<?=$max_len_title?>" value="<?=@$_POST['title']?>" />
                        </div>
                        
                    </div>
                    <div class="field"><label>&nbsp;</label><div style="border: 1px solid #333;line-height: 14px;color: #fff;padding: 10px;width: 278px;text-align: justify;">[LABEL-PRICE-TITLE]</div></div>
                    <div class="field">
                        <label>*[LABEL-DESCRIPTION]:</label>
                        <div>
                            <textarea name="description"  maxlength="999"><?=@$_POST["description"]?></textarea>
                        </div>
                    </div>
                    <div class="field">
                        <label>[LABEL-PRICE]:</label>
                        <div>
                            <input class="text" name="price" type="text" maxlength="<?=$max_len_price?>" value="<?=@$_POST['donation']?>" />
                        </div>
                    </div>
                    
                    <div class="field">
                        <label></label>
                        <div>
                        
                           <input type="checkbox" name='addprice' id='addprice' checked="checked" value='yes'/> &nbsp;[LABEL-ADD-PRICE]
                           
                        </div>
                    </div>
                    <div class="field">
                        <label>[LABEL-PRICE-CONDITIONS]:</label>
                        <div>
                            <input class="text" name="price_conditions" type="text" maxlength="<?=$max_len_price_conditions?>" value="<?=@$_POST['price_conditions']?>" />
                        </div>
                    </div>
                    <div class="field">
                        <label>*[LABEL-CATEGORY]:</label>
                        <div>
                            <?php \Core\Controller::renderPlugin('category_options', NULL, \Core\View::MODELESS);?>
                        </div>
                    </div>
                    <div class="field">
                        <label for="myfans_url" class="myfans_url">[LABEL-4MYFANS]:</label>
                        <div>
                            <input class="text" id="myfans_url" name="myfans_url" type="text"  value="<?=@$_POST['myfans_url']?>" />
                            <small>[NEW-4MYFANS]</small>
                        </div> 
                    </div>
                    
                    <div class="field">
                        <label for="phone" class="phone">*[LABEL-TELEPHONE]:</label>
                        <div>
                            <input class="text" id="phone" name="phone" type="tel" maxlength="<?=$max_len_phone?>" value="<?=(isset($_POST["phone"]) ? preg_replace('/\d{3}/', '$0-', str_replace('-', null, $_POST["phone"]), 2) : (!empty($_SESSION['user']['phone']) ? preg_replace('/\d{3}/', '$0-', str_replace('-', null, $_SESSION['user']['phone']), 2) : ''))?>" />
                            <input id="phone_country_iso" name="phone_country_iso" type="hidden">
                            <input id="phone_country_code" name="phone_country_code" type="hidden">
                        </div>
                    </div>
                    <?php 
                    if(\Core\utils\session::getCurrentUserID() == 81062 ){ ?>
                    <div class="field">
                        <label ></label>
                        <div>
                             <input type="checkbox" name='validate_phone' id='validate_phone' value='yes'/> &nbsp;[LABEL-VALIDATE-TELEPHONE]
                        </div>
                    </div>
                    <?php } ?>
                    <div class="field">
                        <label>[LABEL-LOCATION]:</label>
                        <div>
                            <?php \Core\Controller::renderPlugin('location_options', NULL, \Core\View::MODELESS);?>
                        </div>
                    </div>
                    <div class="field"><label>&nbsp;</label><div style="border: 1px solid #333;line-height: 14px;color: #fff;padding: 10px;width: 278px;text-align: justify;">[LABEL-TITLE-VERIFY-AD]</div></div>
                    <div class="field">
                        <label>[LABEL-PICTURES]:</label>
                        <div>
                            <input type="file" name="images[]" onchange="ValidateSize(this)"><br>
                            <input type="file" name="images[]" onchange="ValidateSize(this)"><br>
                            <input type="file" name="images[]" onchange="ValidateSize(this)"><br>
                            <input type="file" name="images[]" onchange="ValidateSize(this)"><br>
                            <input type="file" name="images[]" onchange="ValidateSize(this)">
                        </div>
                    </div>
                    <div class="field">
                        <label>&nbsp;</label>
                        <div><input class="button" type="submit" value="[BTN-CAPTION-NEW-LISTING]" /></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$(function() {  
    $("textarea[maxlength]").bind('input propertychange', function() {  
        var maxLength = $(this).attr('maxlength');  
        if ($(this).val().length > maxLength) {  
            $(this).val($(this).val().substring(0, maxLength));  
        }  
    })  
});
</script>