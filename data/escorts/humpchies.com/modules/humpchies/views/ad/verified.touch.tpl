<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents a list of search ads on Mobile.
 */
?>
<div class="wrapper">
    <div class="content_box">
        <div class="content_box_header">
            <h2 class="title"><?=\Core\Controller::getTitle()?></h2>
        </div>
        <div class="content_box_body">
            <?php

                  //require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_forum', \Core\View::DEVICE_MODE));
                    //http://jira.sceon.am/browse/HUMP-173  eros banner must be always 9 position no matter vip prem or regular listing
                   $eros_banner_impression = false;
                   $edir_banner_impression = false;
                   $forum_banner_impression = false;
                   $laboite_banner_impression = false;
                   $xxxception_banner_impression = true;
                   $escortbook_banner_impression = false;
                   
                   $procop_indexes = array('1', '2');
                    shuffle($procop_indexes);

                    if(@$vip_ads)
                    {
                        $ad_count = 1;
                        $mid = (int)floor((count($vip_ads) / 2));

                        foreach($vip_ads as $ad)
                        {
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_vip', \Core\View::DEVICE_MODE));

                            if($ad_count === $mid)
                            {
                                require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_' . array_pop($procop_indexes), \Core\View::DEVICE_MODE));
                            }
                            
                            if($ad_count === 1 && !$laboite_banner_impression) {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_laboite', \Core\View::DEVICE_MODE));
                               $laboite_banner_impression = true;
                            }
                            
                            if($ad_count === 4) { // xxxception for montreal, else forum
                               if(TRUE === \Modules\Humpchies\Utils::sessionIsInMontrealMetroArea() && $xxxception_banner_impression) {
                                 require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_xxxception', \Core\View::DEVICE_MODE));
                                 $xxxception_banner_impression = true;
                               
                               } else {
                                 require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_forum', \Core\View::DEVICE_MODE));
                                 $forum_banner_impression = true; 
                               }
                            }
                            
                            if($ad_count === 7 && !$forum_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_forum', \Core\View::DEVICE_MODE));
                               $forum_banner_impression = true;
                            }
                            
                            if($ad_count === 9)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_eros', \Core\View::DEVICE_MODE));
                                $eros_banner_impression = true;
                            }
                            
                            if($ad_count === 12)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_edir', \Core\View::DEVICE_MODE));
                                $edir_banner_impression = true;
                            }
                            
                            if($ad_count === 15 && !$escortbook_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_escortbook', \Core\View::DEVICE_MODE));
                               $escortbook_banner_impression = true;
                            }

                            $ad_count ++;
                        }
                    }

                    $procop_indexes = array('1', '2');
                    shuffle($procop_indexes);
                    if(@$premium_ads)
                    {

                        $ad_count = 1;
                        $mid = (int)floor((count($premium_ads) / 2));

                        foreach($premium_ads as $ad)
                        {
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_premium', \Core\View::DEVICE_MODE));

                            if($ad_count === $mid)
                            {
                                require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_' . array_pop($procop_indexes), \Core\View::DEVICE_MODE));
                            }

                            if($ad_count === 1 && !$laboite_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_laboite', \Core\View::DEVICE_MODE));
                               $laboite_banner_impression = true;
                            }
                           
                            if($ad_count === 4) {
                                if(TRUE === \Modules\Humpchies\Utils::sessionIsInMontrealMetroArea() && !$xxxception_banner_impression) {
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_xxxception', \Core\View::DEVICE_MODE));
                                    $xxxception_banner_impression = true;
                                } elseif(!$forum_banner_impression) {
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_forum', \Core\View::DEVICE_MODE));
                                    $forum_banner_impression = true; 
                                }
                            }
                            
                            if($ad_count === 7 && !$forum_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_forum', \Core\View::DEVICE_MODE));
                               $forum_banner_impression = true;
                            }
                            
                            if($ad_count === 9 && !$eros_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_eros', \Core\View::DEVICE_MODE));
                               $eros_banner_impression = true;
                            }
                            
                            if($ad_count === 12 && !$edir_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_edir', \Core\View::DEVICE_MODE));
                               $edir_banner_impression = true;
                            }
                            
                            if($ad_count === 15 && !$escortbook_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_escortbook', \Core\View::DEVICE_MODE));
                               $escortbook_banner_impression = true;
                            }

                            $ad_count ++;
                        }
                    }

                    $procop_indexes = array('1', '2');
                    shuffle($procop_indexes);

                    if(@$ads)
                    {
                        $ad_count = 1;

                        foreach($ads['recordset'] as $ad)
                        {
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long'));

                            switch($ad_count)
                            {
                                
                                case 1:   // only Montreal metro area
                                    if(!$laboite_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_laboite', \Core\View::DEVICE_MODE));
                                    }
                                    break;
                                    
                               
                                case 3:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_' . array_pop($procop_indexes), \Core\View::DEVICE_MODE));
                                    break;
                               
                               
                                 case 4:
                                    if(TRUE === \Modules\Humpchies\Utils::sessionIsInMontrealMetroArea() && !$xxxception_banner_impression) {
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_xxxception', \Core\View::DEVICE_MODE));
                                    } elseif(!$forum_banner_impression) {
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_forum', \Core\View::DEVICE_MODE));
                                        $forum_banner_impression = true;
                                    }
                                    break;  
                               
                               
                               
                                 case 7:
                                    if(!$forum_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_forum', \Core\View::DEVICE_MODE));
                                    }
                                    break;
                                    
                                case 6:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_' . array_shift($procop_indexes), \Core\View::DEVICE_MODE));
                                    break;
                                case 9:
                                    if(!$eros_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_eros', \Core\View::DEVICE_MODE));    
                                    }                                    
                                    break;
                                //case 9:
                                //    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_3', \Core\View::DEVICE_MODE));
                                //    break;
                                case 12:
                                    if(!$edir_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_edir', \Core\View::DEVICE_MODE));    
                                    }                                    
                                    break;
                                    
                                case 15:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_4', \Core\View::DEVICE_MODE));
                                    break;
                                case 18:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_5', \Core\View::DEVICE_MODE));
                                    break;
                                    
                                case 21:
                                     if(!$escortbook_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_escortbook', \Core\View::DEVICE_MODE));
                                     }
                                    break;    
                                default:
                                    break;
                            }

                            $ad_count ++;
                        }
                    }?>
        </div>
        <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::LOCALIZED_DEVICE_MODE);?>
    </div>
</div>