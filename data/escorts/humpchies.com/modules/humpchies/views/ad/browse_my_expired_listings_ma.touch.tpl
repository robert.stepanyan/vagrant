<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents his/her list of ads or Mobile.
 */
?>


<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i><a/></li>
        <li> <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>">[LABEL-MY-ADS]<a/></li>
        <li>[LABEL-EXPIRED-ADS]</li>
    </ul>    
</nav>

<div class="wrapper">
    <?/* if(\Core\Utils\Session::getCurrentUserID() !== 63661): */?>
        <div class="credits-buy">
           <div class="credits">
               <i class="fa fa-money"></i>
               <span>[LABEL-YOUR-CREDITS]: <?= number_format(\Modules\Humpchies\UserModel::getCredits(\Core\Utils\Session::getCurrentUserID())) ?></span>
           </div>
           <a href="<?=\Core\Utils\Navigation::urlFor('payment', 'bumps-purchase')?>">
               [LABEL-BUY] <i class="fa fa-cart-plus"></i>
           </a>

       </div>
    <?/* endif;*/?>
    <div class="content_box my-ads-box">
       <!-- <div class="content_box_header">
            <h2 class="title"><?=\Core\Controller::getTitle()?>
        </div> -->
        <?php if(0):?>
        <div  class="filter-my-ads">
            <a class = "disactive" href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-active-listings')?>"><span> [LABEL-ACTIVE-ADS]</span></a> &nbsp;
            <a  href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-expired-listings')?>"><span> [LABEL-EXPIRED-ADS]</span></a>
        </div>
        <?php endif;?>
        <?php require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::support_ticket_action', \Core\View::DEVICE_MODE));?>
        <div class="filter_ads_box">
            <div class="filter-container">
            <h2>[LABEL-FILTER-ADS]</h2>
            <div class="selection_box">
                <span class="current">[LABEL-EXPIRED-ADS]</span>
                </div>
            </div>
                <ul class="select_options">
                    <li><a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>"><span>[LABEL-LOCATION-ALL]</span></a></li> 
                    <li><a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-active-listings')?>"><span>[LABEL-ACTIVE-ADS]</span></a></li>    
                </ul>    
       </div>
        
<!--        <div class="ad_options">
           Tap on ad for options
       </div> -->
        <div class="content_box_body">
            <h2> [LABEL-ACTIVE-ADS] </h2>
            <?php  
             if(@$ads)
                        foreach($ads['recordset'] as $ad)
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_my_ads', \Core\View::DEVICE_MODE));?>
        </div>
        <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::LOCALIZED_DEVICE_MODE);?>
    </div>
</div>