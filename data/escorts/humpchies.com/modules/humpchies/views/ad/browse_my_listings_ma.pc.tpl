<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li>[LABEL-MY-ADS]</li>
    </ul>    
</nav>

<!-- add custom banner here -->
<?php 
        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::browse_banner', \Core\View::DEVICE_MODE));
?>





<div class="wrapper_content_full">
    <?php require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::support_ticket_action', \Core\View::DEVICE_MODE));?>
    <div  class="list-tabs <?= ($active_ads_count == 0 ? 'disabled' : '' ) ?>">
        <a class="tab" href="<?= ( $active_ads_count == 0 || $exp_ads_count == 0  ? '#' : \Core\Utils\Navigation::urlFor('ad', 'my-active-listings')) ?>">
            <div ><span> [LABEL-ACTIVE-ADS]</span></div>
        </a>
        <a class="tab inactive <?= ( $active_ads_count == 0 || $exp_ads_count == 0  ? 'disabled' : '' ) ?>" href="<?= ( $exp_ads_count == 0 ? '#' : \Core\Utils\Navigation::urlFor('ad', 'my-expired-listings')) ?>">
            <div ><span> [LABEL-EXPIRED-ADS]</span></div>
        </a>
    </div>
    <div class="govazds-wrapper">
        <?php   if(@$ads)
                    foreach($ads['recordset'] as $ad)
                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_my_ads', \Core\View::DEVICE_MODE));?>
    </div>
    <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::DEVICE_MODE);?>
</div>
