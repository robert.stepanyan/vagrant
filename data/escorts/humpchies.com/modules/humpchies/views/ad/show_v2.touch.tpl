<?php
$chatAdID = $ad['id'];
$chatAdTitle = $ad['title'] ;

$custom_class = '';
switch(true) {
    case (@$ad['vip']):
        $custom_class = "vip";
        break;
    
    case (@$ad['premium']):
        $custom_class = "premium";
        break;
    
    default:
        break;     
}
?>


<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i><a/></li>
        <?php if(0):?><li> <a href="<?php echo $ad['category_url'];?>"><?php echo $ad['category_name'];?></a></li><?php endif;?>
        <li> <a href="<?php echo $ad['category_url_w_city'];?>"><?php echo $ad['city'];?></a></li>
        <li style="height: 17px;"> [LABEL-CURRENT-AD]</li>
    </ul>    
 </nav>

<div class="wrapper_content">
    <?php if($ad['status'] == 'active'):?>
    <div class="content_box" style="margin:0;">
        <div class="content_box_body details_grid <?php echo $custom_class;?>">
                
            <div class="gallery">
                <div class="user_main_photo" style="background-image: url(<?=$ad['images']['main']['src']?>)">
                    <div class="photo_gallery">
                        <?=($ad['has_thumbs'] ? '<a href="' . $ad['images']['main']['href'] . '" data-fancybox="gallery" rel="lightbox[photo]" title="' . $ad['title'] . '">' : NULL)?>
                            <span></span>
                        <?=($ad['has_thumbs'] ? '</a>' : NULL)?>
                    </div>
                </div>
                
                <div class="user_secondary_photos">
                     <?php 
                         $totalphoto = 1;
                         $totalphoto += isset($ad['images']['thumbs'][\Core\Utils\Image::ORIENTATION_LANDSCAPE])? count($ad['images']['thumbs'][\Core\Utils\Image::ORIENTATION_LANDSCAPE]) :0;
                         $totalphoto += isset($ad['images']['thumbs'][\Core\Utils\Image::ORIENTATION_PORTRAIT])?count($ad['images']['thumbs'][\Core\Utils\Image::ORIENTATION_PORTRAIT]) : 0 ;
                         $totalphoto += isset($ad['images']['thumbs'][\Core\Utils\Image::ORIENTATION_SQUARE])? count($ad['images']['thumbs'][\Core\Utils\Image::ORIENTATION_SQUARE]): 0;
                         
                         $total_video = 0;
                         if(\Core\Utils\Session::getCurrentUserID() == 63661 && $attached_video){
                            $total_video = 1;    
                         }
                         
                         if ($totalphoto > 1 || $total_video == 1 ) {
                        ?>
                        <div class="photo_legends">
                                <?php echo ($totalphoto > 1)? '<p><i class="fa fa-camera" style="font-size: 19px;"></i> &nbsp;' . $totalphoto . ' photos' . '</p>' :'';?>
                                <?php echo ($total_video == 1)? '<p><i class="fa fa-video" style="font-size: 19px;"></i> &nbsp;' . $total_video . ' video' . '</p>' :'';?>
                                            </div>
                      <?php } ?>
                    
                </div>
                <div align="center">
                    <?php  
                    $totalphoto = 0;   
                    foreach(array(\Core\Utils\Image::ORIENTATION_LANDSCAPE, \Core\Utils\Image::ORIENTATION_PORTRAIT, \Core\Utils\Image::ORIENTATION_SQUARE) as $thumb_orientation) :?>
                        <?php if(isset($ad['images']['thumbs'][$thumb_orientation])): ?>
                            <?php foreach($ad['images']['thumbs'][$thumb_orientation] as $thumb) :?>
                                <?php $totalphoto++; ?>
                                <a href="<?=$thumb['href']?>" data-fancybox="gallery" rel="lightbox[photo]"
                                   title="<?=$ad['title']?>"> </a>
                            <?php endforeach;?>
                        <?php endif;?>
                    <?php endforeach;?>
                    <?php if(\Core\Utils\Session::getCurrentUserID() == 63661 && $attached_video): ?>
                        <video width="100%" controls="" controlslist="nodownload" id="movie-load" style="display:none;" rel="lightbox[photo]"  data-fancybox="gallery">
                            <source src="<?php echo $videolocation.$attached_video?>" type="video/mp4">
                        </video>
                    <?php endif;?>
                </div>
                
                
            </div>
            <?php if (@$ad['vip']) { ?>
                <span class="vip_flag">VIP </span>
            <?php }?>
            <?php if( $ad['is_premium'] ){ ?>
                <span class="premium_flag">Premium </span>  
            <?php } ?>
            <?php if( $ad['verification_status'] == "verified") { ?>
                <span class="real-photos"  style="bottom: 0px;">[LABEL-REAL-PHOTO] </span>
            <? } ?>
            
               
            <?php if($ad['fake_status']): ?>
                <div class="fake-pics-wrapper">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                    <h6 class="fake-pics">[LABEL-AD-FAKE-PICS]</h6>
                </div>
            <?php endif; ?>
                
            <div class="user_content_details">
                <div class="content-header">
                    <h2 class="title"><?=$ad['orig_title']?></h2> 
                    <div class="right">
                        <div class='buttons'>
                            <a href="<?php echo $ad['buttons']['backurl'];?>"  class="previous" <?php if ($ad['buttons']['backurl']==''){ echo 'style="visibility:hidden"';}?>>
                               <i class="fa fa-arrow-circle-left"></i> 
                            </a>
                            <a href="<?php echo $ad['buttons']['nexturl'];?>" class="next">
                                <i class="fa fa-arrow-circle-right" style="margin-left:5px;"></i>
                            </a>
                        </div>
                        <p>
                            <?=$ad['date_released']?>
                        </p>    
                    </div>
                       
                </div>
                <div class="separator"></div>
                
                <p class="description"> <?=$ad['description']?></p>
                
                <!-- private message -->
                <?php if ($ad['user_id'] != 4617 && $advertiser[0]["disable_pm"] != 1 ) { ?>
                    <?php if ($chat['isLoggedIn']) { ?>
                        <?php if ($chat['haveOpenedChat']) { ?>                                                                                                  
                            <a href="<?= \Core\Utils\Navigation::urlFor('chat', 'view', array('id' =>  $chat['chatId'])) ?>" class="btn btn-orange-outline m-20"><i class="fa fa-comments"></i> [BTN-PRIVATE-MESSAGE]</a>
                        <?php } else { ?>
                            <a href="javascript:void(0);" id="send-message-button" class="btn btn-orange-outline m-20 <?= $chat['canSendMessage'] ? '' : 'disabled' ?>"><i class="fa fa-comments"></i> [BTN-PRIVATE-MESSAGE]</a>
                        <?php } ?>
                    <?php } else { ?>
                        <a  href="<?= \Core\Utils\Navigation::urlFor('user', 'login') ?>" class="btn btn-orange-outline m-20"><i class="fa fa-comments"></i> [BTN-PRIVATE-MESSAGE]</a>
                    <?php } ?>
                <?php } ?>
                
                <div class="data-container">
                    <ul>
                        <li>
                            <span>[LABEL-TELEPHONE]:</span>
                            <div>
                                <span class="icons">
                                    <?php if($ad['phone_validated'] == 'Y'): ?>
                                        <i class="fa fa-check green"></i>
                                    <?php endif;?>
                                    <i class="fa fa-phone-volume purple"></i>
                                    <i class="fab fa-whatsapp green"></i>
                                </span>

                                <a href="tel:<?php echo $formated_phone;?>" onclick="gtag('event', 'touch',{'event_category' : 'PhoneClick','event_label' : '<?php echo $ad['phone'];?>' });"><?php echo $ad['phone'];?></a>
                                
                            </div>
                        </li>
                        
                        <li>
                            <span>[LABEL-LOCATION]:</span>

                            <?php echo ($ad['location'] == '')? 'Incalls' : ucfirst($ad['location']);?>
                        </li>
                        <li>
                            <span>[LABEL-CATEGORY]:</span>

                            <a href="<?=$ad['category_url']?>">
                                <?=$ad['category_name']?>
                            </a>
                        </li>
                        <li>
                            <span>[LABEL-CITY]:</span>

                            <a href="<?=$ad['category_url_w_city']?>">
                                <?=$ad['city_name']?>
                            </a>
                        </li>
                    
                        <?php if($ad['price_min'] > 0 || $ad['price_max'] > 0 ): ?>
                            <li>
                                <span>[LABEL-PRICE-NEW]:</span>
                                <?php echo ( $ad['price_min'] > 0)? $ad['price_min'] .'$/30 min,' : '';?>
                                <?php echo ( $ad['price_max'] > 0)? $ad['price_max'] .'$/h' : '';?>
                            </li>
                        <?php endif;?>
                    
                        <li class="end">
                            <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE) : ?>
                                <div class="favorite"><a href="<?=\Core\Utils\Navigation::urlFor('user', 'login')?>" id="btn_favorite"><i class="fas fa-heart"></i>&nbsp;[LABEL-ADD-FAV]</a></div>
                            <?php else:?>
                                <input type="hidden" name="no_favorites" id="no_favorites" value="<?php echo count($user_favorites);?>" />    
                                <div class="favorite">
                                    <a href="javascript:void(0)" id="btn_add_favorite" data-ut="<?php echo (\Core\Utils\Session::currentUserIsAdvertisment())? '1' : '2';?>" data-id="<?php echo $ad['id'];?>" <?php echo (in_array($ad['id'], $user_favorites))? 'style="display:none"' : 'style="display:block"';?>><i class="fas fa-heart"></i>&nbsp;[LABEL-ADD-FAV]</a>
                                </div>    
                                
                                <div class="favorite">
                                    <a href="javascript:void(0)" id="btn_remove_favorite"  data-ut="<?php echo (\Core\Utils\Session::currentUserIsAdvertisment())? '1' : '2';?>" data-id="<?php echo $ad['id'];?>" <?php echo (!in_array($ad['id'], $user_favorites))? 'style="display:none"' : 'style="display:block"';?>><i class="fas fa-heart"></i>&nbsp;[LABEL-ADDED-FAV]</a>
                                </div> 
                                
                            <?php endif;?>
                        </li>
                        
                        <?php if (strtolower(parse_url(\Core\Utils\Device::getServerVarsAndHeaders('http_referer'), PHP_URL_HOST)) == strtolower(\Core\Utils\Device::getServerVarsAndHeaders('http_host'))):?>
                            <li class="end">
                                <a onclick="gtag('event', 'click', {
                                            'event_category' : 'BackToAds',
                                            'event_label' : 'BackToAdsDesktop',
                                        'event_callback': function(){return;}
                                      });" href="<?php echo $go_back;?>">
                                    <i class="fa fa-step-backward" aria-hidden="true"></i>
                                    &nbsp;
                                    <?=$ad['back_link_label']?>
                                </a>
                            </li>
                        <?php endif;?>
                                                
                        <li class="end">
                            <a href="javascript:void(0);" title="report" id ="report-button">[LABEL-REPORT-TITLE]</a>
                        </li>
                    </ul>
                </div>
                <div class="separator"></div>
                
                <?php if($services !== false) :?>
                    <div id="services">
                        <div class="services_box">
                            <?php foreach($services as $service):?>
                                <span class="service"><i class="fa fa-check-circle"></i> <?php echo $service["name"];?></span>
                            <?php endforeach;?>    
                        </div>
                    </div>
                    <div class="separator" style="margin-top:10px;"></div>
                <?php endif;?>
                    
                <?php if($map_coords && $map_coords['hide_map'] == 0 && $map_coords['lat'] != 0) : ?>
                    <?php $path = \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_images') . 'locations/';?> 
                    
                    <div style="position:relative; display:block">
                        <div id="simple_map" style="background-image:url('<?php echo $path . $map_coords['location_photo'];?>'); display:block; "></div>
                        <?php if (\Core\Utils\Session::getCurrentUserID() == 63661):?>
                            <a href="javascript:void(0)" id="toggle_interactive" class="btn_map" style="right: 10px; padding: 6px 10px; border-radius: 4px; font-size: 13px">Interactive Map</a>
                        <?php endif;?>
                        
                        <input type="hidden" name="show_map" id="show_map" value="yes" />
                        <div id="map" style="height:0px; width: 100%;margin:0;" <?php echo ($map_coords && $map_coords['lat'] != 0)? 'data-lat="'.$map_coords['lat'].'" data-long="'.$map_coords['long'].'"' : 'data-city="' . $map_coords['ad_city_name'] . '" data-country="Canada"' ;?>></div>
                    </div>
                    <div class="separator"></div>
                <?php else:?>
                    <input type="hidden" name="show_map" id="show_map" value="no" />
                <?php endif;?>
                
            </div> 
        </div>
    </div>
    
    <section class="comments-container">
        <?php if($ad["blocked_comments"] == 1):?>
            <article>
                <p style="" class="blocked-comments" data-text="[LABEL-USER-BLOCKED-COMMENTS]"></p>
            </article> 
            <div class="separator"></div>   
        <?php else:?>
            <article>
                <h3>[LABEL-COMMENTS-FOR-THIS-USER]:</h3>
                <?php if(!empty($comments)) : ?>
                    <ul class="comments">
                        <?php foreach($comments as $comment) : ?>
                        <li class="comment">
                            <p class="comment-msg"><?php echo $comment['comment']; ?></p>
                            <div class="comment-data">
                                
                                <?php if ($comment['user_id'] == $comment['from_user']): ?>
                                    <p>
                                        <span class="adv-reply">[LABEL-COMMENTS-ADVERTISER-REPLY]</span>
                                        <span><?php echo $comment['date']?></span>
                                    </p>
                                <?php elseif($comment['ad_status'] == 'active'): ?>
                                    <a href="<?php echo $comment['ad_url'];?>"><?php echo $comment['ad_title'];?></a> 
                                    <p>
                                        <span class="member-name"><?php echo $comment['member_name']?></span>
                                        <span><?php echo $comment['date']?></span>  
                                    </p>                                     
                                <?php else: ?>
                                    <span><?php echo $comment['ad_title'];?></span> 
                                    <p>
                                        <span class="member-name"><?php echo $comment['member_name']?></span>
                                        <span><?php echo $comment['date']?></span>
                                    </p>                                      
                                
                                <?php endif; ?>    
                             </div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                    
                    <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::LOCALIZED_DEVICE_MODE);?> 
                <?php endif; ?>
            </article>
            <article class="add-comment-area" >
                 <a href="javascript:void(0);" id="btn-write-comment" class="btn btn-orange-outline" <?php echo $redirect ? ' data-rdr="1"' : ( (!$is_member) ? ' data-alert="1"': '' ); ?>>[BTN-WRITE-COMMENT]</a> 
                                
                <?php if(\Core\Utils\Session::currentUserIsMember() && $user_has_pending_comment) :?>
                    <p style="">[LABEL-COMMENT-MODERATION]</p>
                <?php else:?>
                    <p></p> 
                <?php endif; ?>
                <div id="comment-box" style="display: none;">
                    <p class="error" id="err-msg" style="display: none"></p>
                    <textarea name="comment" id="comment" placeholder="[LABEL-WRITE-MESSAGE]" rows="5"></textarea>
                    <input type="hidden" name="" id="ad_id" value="<?=$ad['id']?>">
                    
                    <a href="javascript:void(0);" class="btn btn-orange" id="save-comment" <?php echo $redirect ? ' data-rdr="1"' : ( (!$is_member) ? ' data-alert="1"': '' ); ?> style="margin: 20px 0 20px auto;">[LABEL-POST-COMMENT]</a>
                </div>
            </article> 
        <?php endif;?>    
    </section>
    <div class="separator"></div>   
    <p class="alert_description description">
        [DESCRIPTION-AD-ALERT]
    </p>
    <div class="separator"></div>   
    <?php if(@$ads) :?>
        <div class="content_box" style="margin:0;">
            <div class="content_box_body details_grid">
                <h3 class="title" style="margin: 0 20px 20px;"><?php echo ($custom_class != '')? '[LABEL-MORE-ADS-USER]' : '[LABEL-MORE-ADS-CITY]';?></h3>
           
                <?php
                    foreach($ads as $ad_more) {
                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_user_ads_v2', \Core\View::DEVICE_MODE));
                    }
                ?>
                <div class="separator"></div>
            </div>
        </div>
    <?php endif; ?>
    
                
    <?php else:?>
    <div class="content_box" style="margin:0;">
        <div class="content_box_body details_grid">
            <div class="alert_description">
                <p>[INACTIVE-AD-TEXT]&nbsp;<a href="<?php echo $ad['category_url_w_city'];?>"><?php echo $ad['city_name'];?></a> </p>
            </div>
            <div class="separator"></div>  
            <?php if(@$ads) :?>
                <h3 class="title" style="margin: 0 20px 20px;"><?php echo ($custom_class != '')? '[LABEL-MORE-ADS-USER]:' : '[LABEL-MORE-ADS-CITY]:';?></h3>
                <?php
                    foreach($ads as $ad_more) {
                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_user_ads_v2', \Core\View::DEVICE_MODE));
                    }
                ?>
                <div class="separator"></div>  
            <?php endif; ?>
        </div>
    </div>
   
          
    <?php endif;?>
            
            
     
</div>

<!-- The Modal -->
<div id="report-profile" class="modal-white-mob" style="display: none;">

    <div class="modal-content">
        <div class="modal-header">
            <h2>[LABEL-REPORT-TITLE]</h2>
            <a href="javascript:void(0)" id="close_report" class="close-modal"><i class="fa fa-times"></i></a>
        </div>
    
        <div class="modal-body report">
            
            <div class="box">
                 <p class="full">
                    <input type="radio" id="report_1" name="report_form" value='1' checked />
                    <label for="report_1" class="xreport"></label>
                 </p>
                 <p class="full">
                    <input type="radio" id="report_2" name="report_form" value='2' />
                    <label for="report_2" class="xreport"></label>
                 </p>
                 <p class="full">
                    <input type="radio" id="report_3" name="report_form" value='3' />
                    <label for="report_3" class="xreport"></label>
                 </p>
                 <p class="full">
                    <input type="radio" id="report_4" name="report_form" value='4' />
                    <label for="report_4" class="xreport-more"></label>
                 </p>
            </div> 
            
            <input type="hidden" id='adID' value ="<?=$chatAdID?>" />
            
            <div id='report-other' class="form-group white" style="display: block;">
                <textarea id='report-other-text' class="form-control white" placeholder="[LABEL-REPORT-Z]"></textarea>
            </div>
             
            <?php if(!\Core\Utils\Session::getCurrentUserID() ){ ?>
                <div class="form-group white">
                    <input type="text" name="email_address" id="email" class="form-control" placeholder="[LABEL-REPORT-F]" />
                    <div class="input-error" id="error_email" style="display: none;">[LABEL-REPORT-ERROR-EMAIL]</div>
                </div>
            <?php }?>
            <div class="form-group white">
                <div class="input-error" id="error_general" style="display:none;">[LABEL-REPORT-ERROR]</div>
            </div>
        </div>
        <div class="modal-body success" style="display: none;">[LABEL-REPORT-SUCCESS]</div>
        
        <div class="modal-footer report">
            <a href="javascript: void(0)" class="close-modal">[LABEL-CANCEL]</a>
            <a href="javascript: void(0)" class="red" id="send-report">[LABEL-SEND-REPORT]</a>
            
        </div>
    </div>

</div>
<script>
    var rep_texts = new Array();
    rep_texts[1] = "[LABEL-REPORT-A]";
    rep_texts[2] = "[LABEL-REPORT-B]";
    rep_texts[3] = "[LABEL-REPORT-C]";
    rep_texts[4] = "[LABEL-REPORT-D]";
    
    var more_placeholder = "[LABEL-REPORT-Z]";
    var other_placeholder = "[LABEL-REPORT-E]";
</script>

<?php if ($chat['canSendMessage']) { ?>

<div id="send-message-modal" class="modal-white-mob" style="display: none;">
    <div class="modal-content">
        
        <div class="modal-header">
            <h2>[LABEL-SEND-MESSAGE-TITLE] <?=$chatAdTitle?> </h2>
            <a href="javascript:void(0)"class="modal-pm-close close-modal"><i class="fa fa-times"></i></a>
        </div>
        <div class="modal-body">
            <div class="form-group white">
                <input type="hidden" id='send-message-ad-id' value="<?=$chatAdID?>" />
                <textarea id='send-message-body' placeholder="[LABEL-MESSAGE-PLACEHOLDER]" maxlength="500"></textarea>
                <div class="modal-error">
                    <span id="send-message-error-not-sent" class="input-error" style="display: none;">[ERROR-SEND-MESSAGE-NOT-SENT]</span>
                    <span id="send-message-error-required" class="input-error" style="display: none;">[ERROR-SEND-MESSAGE-REQUIRED]</span>
                    <span id="send-message-error-max-length" class="input-error" style="display: none;">[ERROR-SEND-MESSAGE-MAX-LENGTH]</span>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="javascript: void(0)" class="modal-pm-close">[LABEL-CANCEL]</a>
            <a href="javascript: void(0);" class="red" id="send-message">[BTN-SEND-MESSAGE]</a>
        </div>
    </div>
</div>

<div id="send-message-sent-modal" class="modal-white-mob" style="display: none;">
    <div class="modal-content">
        <div class="modal-header">
            <h2>[TITLE-MESSAGE-SENT]</h2> 
            <a href="javascript:void(0)"class="modal-pm-close close-modal"><i class="fa fa-times"></i></a>
        </div>
        
        <div class="modal-body success">[LABEL-MESSAGE-SENT]</div>
        <div class="modal-footer">
            <a href="javascript: void(0)" class="modal-pm-close">[LABEL-CLOSE]</a>
            <a href="<?=\Core\Utils\Navigation::urlFor('chat', 'view')?>" class="red" title="[LABEL-CHAT-LIST]">[LABEL-CHAT-LIST]</a>
        </div>
    </div>
</div>

<?php } // can send message ?>

