
<div class="create-ad" style="padding:0px 30px 20px 30px">
    <div class="content_box" >
        <div class="content_box_header" style="margin: 30px auto;margin-bottom: 0px; text-align:center; display:block">
            <img src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>confirmation.png" border="0" width="65" height="65" alt="" style="margin: 2px auto;"/>
    <div class="clear"></div>
            <div class="notice-message" style="width: 100%;font-size: 15px;">[CONFIRMATION-THANK-YOU]</div>     
            <div class="notice-message border-top" style="width: 100%;font-size: 15px;">[CONFIRMATION-VERIFICATION]</div>     
            
            <a class="button-orange-outline button back-list" href='<?php echo \Core\Utils\Navigation::urlFor('ad', 'my-listings'); ?>'>[CONFIRMATION-FINISH] </a> 
            <a class="button-orange button continue" href='<?php echo \Core\Utils\Navigation::urlFor('ad', 'verify'); ?>?id=<?php echo $adid; ?>'>[LABEL-VERIFY] </a>
            
        </div>
      
    </div>
</div>            

