<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders XML code to the user's screen that
 * represents the site's RSS feed.
 */
?>
<?='<?xml version="1.0"?>'?>
    <rss version="2.0">
        <channel>
            <title><?=$title?></title>
            <link><?=$link?></link>
            <description><?=$description?></description>
            <language><?=$language?></language>
            <pubDate><?=$pub_date?></pubDate>
            <lastBuildDate><?=$last_build_date?></lastBuildDate>
            <?php   if(@$ads)
                    {
                        foreach($ads as $ad)
                        {?>
                        <item>
                            <title><?=$ad['title']?></title>
                            <link><?=$ad['url']?></link>
                            <description><?=$ad['description']?></description>
                            <pubDate><?=$ad['date_released']?></pubDate>
                            <guid><?=$ad['url']?></guid>
                        </item>  
            <?php       }
                    }?>
        </channel>
    </rss>