<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents a list of ads for the PC.
 */
?>
<style>

.custom_description {
    color: #999;
    text-align: justify;
}
.custom_description h3 {
    color: #b7b7b7;
    margin-top: 20px;
    margin-bottom: 10px;
}
</style>
<?php if(isset($breadcrumb)):?>
<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <?php switch(true) {
        
            case ( isset($breadcrumb['city']) && isset($breadcrumb['category_url'])):
        ?>
            <li> <a href="<?php echo $breadcrumb['category_url'];?>"><?php echo $breadcrumb['category_title'];?></a></li>
            <li><?php echo $breadcrumb['city'];?></li>
        <?php                                                         
            break;    
            case ( !isset($breadcrumb['city']) && isset($breadcrumb['category_url'])):
        ?>
            <li> <?php echo $breadcrumb['category_title'];?></li>
         <?php                                                         
            break;    
            case ( isset($breadcrumb['city']) && !isset($breadcrumb['category_url'])):
        ?>
            <li><?php echo $breadcrumb['city'];?></li>
        <?php                                                         
            break;
            case ( isset($breadcrumb['search'])): 
        ?>
            <li>'<?php echo $breadcrumb['search'];?>'</li>
        <?php                                                         
            break;
            case ( isset($breadcrumb['region'])): 
        ?>
            <li><?php echo $breadcrumb['region'];?></li>
        <?php                                                         
            break; 
        } ?>    
        
        
    </ul>    
 </nav>
 <?php endif;?>

 <h2 class="stylized-title">
    <span><?=\Core\Controller::getTitle()?></span>
</h2>
       
 <div class="wrapper_content">
    <div class="content_box">
        
        <div style="display:flex; justify-content: space-between;">
            
            <?php // cities here ?>
            
            
            <div style="display: flex;">
                <?php if($cities_with_zones != null):?>
                <a href="javascript:void(0)" id="cities_list" title="" style="margin-right:20px;"><i class="fa fa-map-marker"></i> [LABEL-SELECT-CITY-BTN]</a>
                <?php endif;?>
            
            <?php if(isset($show_filter) && $show_filter):?>
                    <form id="location_form" method="get" style="min-width: 140px;">
                    <label class="custom-select" for="location">
                        <select name="location" id="location" onchange="this.form.submit();">
                        <option value="">[LABEL-LOCATION-AVAILABLE]</option>
                        <option value="all" <?php echo $location_filter == "all"? 'selected' : '';?>>[LABEL-LOCATION-ALL]</option>
                        <option value="incalls" <?php echo $location_filter == "incalls"? 'selected' : '';?>>[LABEL-LOCATION-INCALL]</option>
                        <option value="outcalls" <?php echo $location_filter == "outcalls"? 'selected' : '';?>>[LABEL-LOCATION-OUTCALL]</option>
                    </select>
                    </label>
                </form>    
            <?php endif;?>    
            </div>       
        </div>
        
        
        <div class="content_box_body">
            <?php  
            
            $laboite_banner_impression = false;
                
            if(@$vip_ads)
                    {
                        $ad_count = 1;
                        $mid = (int)floor((count($vip_ads) / 2));

                        foreach($vip_ads as $ad)
                        {
                            $ad['vip'] = true;
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long', \Core\View::DEVICE_MODE));

                            if($ad_count === $mid && !$laboite_banner_impression)
                            {
                                //require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_1', \Core\View::DEVICE_MODE));
                                require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_laboite', \Core\View::DEVICE_MODE));
                                $laboite_banner_impression = true;
                            }

                            $ad_count ++;
                        }
                    }

                    if(@$premium_ads)
                    {
                        $ad_count = 1;
                        $mid = (int)floor((count($premium_ads) / 2));

                        foreach($premium_ads as $ad)
                        {
                            $ad['premium'] = true;
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long', \Core\View::DEVICE_MODE));

                            if($ad_count === $mid && !$laboite_banner_impression)
                            {
                                //require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_1', \Core\View::DEVICE_MODE));
                                require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_laboite', \Core\View::DEVICE_MODE));
                                $laboite_banner_impression = true;
                            }

                            $ad_count ++;
                        }
                    }

                    if(@$ads)
                    {
                        $ad_count = 1;

                        foreach($ads['recordset'] as $ad)
                        {
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long', \Core\View::DEVICE_MODE));

                            switch($ad_count)
                            {
                                case 4:
                                    //require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_1', \Core\View::DEVICE_MODE));
                                    if(!$laboite_banner_impression)
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_laboite', \Core\View::DEVICE_MODE));
                                    break;
                                case 8:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_2', \Core\View::DEVICE_MODE));
                                    break;
                                //case 12:
                                //    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_3', \Core\View::DEVICE_MODE));
                                //    break;
                                //case 16:
                                //   require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_4', \Core\View::DEVICE_MODE));
                                //    break;
                                //case 20:
                                //    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_5', \Core\View::DEVICE_MODE));
                                //    break;
                                case 12:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_escortbook', \Core\View::DEVICE_MODE));
                                    break;
                                default:
                                    break;
                            }

                            $ad_count ++;
                        }
                    }?>
            <?php if(intval($ads['count']) == 0):?>
                <p style="margin-top:20px;">[ERROR-NO-ADS]</p>
            <?php endif;?>
        </div>
    </div>
    <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::DEVICE_MODE);?>
    
    <?php if (isset($custom_description) && $custom_description != ''):?>
        <div class="custom_description" style="margin-top: 60px;">
            <?php echo $custom_description;?>
        </div>
    <?php endif;?>
</div>