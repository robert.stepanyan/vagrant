<div class="create-ad">
    <div class="content_box">
        <div class="content_box_header">
            <h2 class="title" style="font-weight: 300;">[LABEL-TITLE-CREATE-AD]</h2>
            <h2 id="tips" style="color: #f90; cursor: pointer;">Tips <i class="fa fa-chevron-down" style="margin-left: 5px;"></i></h2>
        </div>
        <div class="tips-container" style="display: none;">
            [LABEL-TIPS-CREATE-AD]
            <div class="mb-20"></div>
        </div>  
        <div class="content_box_body">
            <div class="form-wrapper">
            </div>
        </div>
    </div>
</div>            