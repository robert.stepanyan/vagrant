<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li>Favorites</li>
        
    </ul>    
</nav>
<div class="wrapper">
    <div class="content_box">
        <div class="content_box_header">
            <div>
                <h2 class="title">[TITLE-FAVORITES]</h2>
                <p>[DESC-FAVORITES]</p> 
                    
            </div>        
        </div>
        <div class="content_box_body">
            <?php
                if(@$vip_ads) {
                        foreach($vip_ads as $ad) {
                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_vip_fav', \Core\View::DEVICE_MODE));
                               }
                            }

                if(@$premium_ads) {
                    foreach($premium_ads as $ad) {
                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_premium_fav', \Core\View::DEVICE_MODE));
                            }
                            }
                           
                if(@$ads) {
                    foreach($ads['recordset'] as $ad) {
                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_fav')); 
                                }
                            }
            ?>
        
        <?php if(intval($ads['count']) == 0):?>
            <p style="margin-top:20px;">[ERROR-NO-ADS]</p>
        <?php endif;?>
        </div>
        <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::LOCALIZED_DEVICE_MODE);?>
        
    </div>
</div>