<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents a list of jis/her ads.
 */
?>

<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i><a/></li>
        <li> <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>">[LABEL-MY-ADS]<a/></li>
        <li>[LABEL-ACTIVE-ADS]</li>
    </ul>    
</nav>


<div class="wrapper_content_full">
    <div class="govazds-wrapper">
        
        <div  class="ad-tabs">
            <a class="tab active" href="#">
                <div><span>[LABEL-ACTIVE-ADS]</span></div>
            </a>
            <a class="tab disabled" href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-expired-listings')?>">
                <div><span>[LABEL-EXPIRED-ADS]</span></div>
            </a>
        </div>
        
      
        <div class="content_box_body" style="padding: 10px 0 20px 0">
            <?php   if(@$ads)
                        foreach($ads['recordset'] as $ad)
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_my_ads', \Core\View::DEVICE_MODE));?>
        </div>
        <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::DEVICE_MODE);?>
    </div>
</div>