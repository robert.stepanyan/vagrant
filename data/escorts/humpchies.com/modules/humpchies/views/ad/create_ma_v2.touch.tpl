<?php
 $city_list = \Modules\Humpchies\CityModel::getCitiesRegions();
?>
<style>
.video-preview.selected:before {
    content: "[LABEL-VIDEO-SELECTED]";
}
</style>

<nav id="breadcrumb" style="top:60px;">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li><?=\Core\Controller::getTitle()?></li>
    </ul>    
 </nav>


 <?php if (!\Modules\Humpchies\AdModel::getListOfAdsByUser(\Core\Utils\Session::getCurrentUserID())):?>
    <input type="hidden" id="show_tips" name="show_tips" value="yes" />
 <?php endif;?>
<div class="create-ad">
    <div class="content_box">
        <div class="content_box_header">
            <h2 class="title">[LABEL-TITLE-CREATE-AD]</h2>
            <h2 id="tips">
                <img src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>lightbulb-solid.svg" style="width: 10px; margin-right: 5px;" />
                [LABEL-TIPS] 
            </h2>
        </div>
        <div class="tips-container" style="display: none;padding: 0 30px;">
            [LABEL-TIPS-CREATE-AD]
            <div class="mb-20"></div>
        </div> 
        <div class="content_box_body">
            <div class="form-wrapper">
                <form id="post_pad" method="post" enctype="multipart/form-data">
                    <div class="row mb-20 bg-gray">
                        
                        <div class="field mb-20">
                                <div>
                                    <label>[LABEL-CITY]<span class="required">*</span></label>
                                    <div class=" mt-5">
                                        <label class="custom-select" style="background: none;">
                                            <select name="city" id="city" data-must-be="">
                                                <option value=''>[LABEL-SELECT-CITY]</option>
                                        <?php foreach($city_list as $key => $list):?>
                                            <optgroup label="<?php echo $list['region']['name'];?>">
                                                
                                                <?php foreach($list['cities'] as $key => $city):?>
                                                        <option value="<?php echo $city['slug'];?>" <?php echo (isset($_POST['city']) && $city['slug'] == $_POST['city'])? 'selected' : '';?> data-lat="<?php echo $city["lat"];?>" data-lng="<?php echo $city["lng"];?>"><?php echo $city['name'];?></option>
                                                <?php endforeach;?>
                                            </optgroup>    
                                        <?php endforeach;?>
                                        </select>
                                        </label>    
                                    </div>
                                </div>
                            </div>

                        <div class="field mb-20">
                                <label for="title">[LABEL-TITLE]<span class="required">*</span></label>
                                <div class="mt-5">
                                    <input class="text" id="title" name="title" type="text" placeholder=" "
                                           maxlength="<?=$max_len_title?>" value="<?=@$_POST['title']?>"/>
                                </div>
                            <div class="mt-5 form-attention">[LABEL-PRICE-TITLE]</div>
                            </div>

                        <div class="field mb-20">
                                <label for="description">[LABEL-DESCRIPTION]<span class="required">*</span></label>
                                <div class="mt-5">
                                <textarea placeholder=" " name="description"
                                          id="description"><?=@$_POST["description"]?></textarea>
                                </div>
                            </div>
                            <input type="hidden" id="myfans_url" name="myfans_url" />
                            <?php if(0):?>
                            <div class="field mb-20">
                                <label for="myfans_url" class="myfans_url">[LABEL-4MYFANS]:</label>
                                <div class="mt-5">
                                    <input class="text" id="myfans_url" name="myfans_url" type="text" placeholder=" "  value="<?=@$_POST['myfans_url']?>" />
                                    <div class="mt-5" style="border: 1px solid #333; line-height: 14px;color: #fff;padding: 10px;">[NEW-4MYFANS]</div>
                                </div> 
                            </div>
                            <?php endif;?>    
                        
                        
                        <div class="field mb-20">
                                <label for="price_min" class="price_min">[LABEL-PRICE-30] (CAD):</label>
                                <div class="mt-5">
                                    <input class="text" id="price_min" name="price_min" type="text" placeholder=" "  maxlength="<?=$max_len_price?>"  value="<?=@$_POST['price_min']?>" />
                                </div> 
                            </div>
                        <div class="field mb-20">
                            <label for="price">[LABEL-PRICE-60] (CAD):</label>
                            <div class="mt-5">
                                <input class="text" name="price_max" id="price_max" type="text" placeholder=" "  maxlength="<?=$max_len_price?>" value="<?=@$_POST['price_max']?>"/>
                            </div>
                            <div class="custom-control-container mt-5">
                                <label class="custom-control custom-checkbox" for="addprice">
                                    <input type="checkbox" name="addprice" id='addprice' class="custom-control-input" value="yes" checked="checked" />
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-label" style="width: max-content"> [LABEL-ADD-PRICE]</span>
                                </label>
                            </div>
                        </div>
                        
                        <div class="field mb-20">
                                <label for="phone" class="phone">[LABEL-TELEPHONE]<span
                                            class="required">*</span></label>
                                <div class="mt-5">
                                    <input class="text" id="phone" name="phone" type="tel" placeholder=" "
                                           maxlength="<?=$max_len_phone?>" value="<?=(isset($_POST[" phone"]) ?
                                    preg_replace('/\d{3}/', '$0-', str_replace('-', null, $_POST["phone"]), 2) :
                                    (!empty($_SESSION['user']['phone']) ? preg_replace('/\d{3}/', '$0-',
                                    str_replace('-',
                                    null, $_SESSION['user']['phone']), 2) : ''))?>" />
                                    <input id="phone_country_iso" name="phone_country_iso" type="hidden" value="<?php echo (isset($_POST['phone_country_iso']) && $_POST['phone_country_iso'] != '')? $_POST['phone_country_iso'] : 'ca'?>">
                                    <input id="phone_country_code" name="phone_country_code" type="hidden">
                                </div>
                                   <?php 
                                    if(\Core\utils\session::getCurrentUserID() ==  81062  /*63661 */ ){ ?>
                                    <div class="custom-control-container mt-5">
                                        <label class="custom-control custom-checkbox" for="validate_phone">
                                        <div>
                                             <input type="checkbox" name='validate_phone' id='validate_phone' value='yes' class="custom-control-input"/> 
                                              <span class="custom-control-indicator"></span>
                                              <span class="custom-control-label" style="width: 500px;">[LABEL-VALIDATE-TELEPHONE]</span>
                                              </label>
                                        </div>
                                    </div>
                                    <?php } ?>
                            </div>
                        <div class="field mb-20">
                                <div class="mt-5">
                                    <label>[LABEL-CATEGORY]<span class="required">*</span></label>
                                    <div class="mt-5">
                                        <label class="custom-select">
                                        <?php \Core\Controller::renderPlugin('category_options', NULL, \Core\View::MODELESS);?>
                                        </label>    
                                    </div>
                                </div>
                            </div>
                            <div class="field pl-15">
                                <label>[LABEL-LOCATION]</label>
                                <div class="mt-5">
                                    <label class="custom-select">
                                    <?php \Core\Controller::renderPlugin('location_options', NULL, \Core\View::MODELESS);?>
                                    </label>    
                                </div>
                                    </div>

                        
                                </div>

                    <?php if(!empty($tags)):?>
                    <div class="tags-container">
                            <h2 class="stylized-title"><span>[LABEL-SERVICES]</span></h2>  
                      <ul class="ks-cboxtags">
                        <?php foreach($tags as $tag):?>
                            <li>
                                        <input name='tags[]' type="checkbox" id="tag<?php echo $tag['id'];?>" value="<?php echo $tag['id'];?>" <?php echo (in_array($tag['id'], $added_tags))? 'checked = "checked"' : '';?>>
                                <label for="tag<?php echo $tag['id'];?>"><?php echo $tag['name'];?></label>
                            </li>
                        <?php endforeach;?>

                      </ul>

                    </div>
                    <?php endif;?>
                    
                    <div class="row mb-20">
                        <h2 class="stylized-title"><span>[LABEL-PICTURES](max. 5)</span></h2> 
                        <h3 class="mb-20"><span class="title">[LABEL-TIPS]!&nbsp;</span> [LABEL-TITLE-VERIFY-AD]</h3>
                        
                    
                        <fieldset class="field">
                            <div class="pictures-container">
                                <div id="preview-default"></div>
                                <div class="img-preview" id="preview_images">

                                </div>
                            </div>
                            <input type="hidden" name="main_img" id="main-img">
                            <input type="hidden" name="rotator[0]" id="rotator-0" value="0">
                            <input type="hidden" name="rotator[1]" id="rotator-1" value="0">
                            <input type="hidden" name="rotator[2]" id="rotator-2" value="0">
                            <input type="hidden" name="rotator[3]" id="rotator-3" value="0">
                            <input type="hidden" name="rotator[4]" id="rotator-4" value="0">
                            <input id="images-0" type="file" accept="image/jpeg, imgae/jpg;capture=camera" name="images[]" style="width: 0; height: 0">
                            <input id="images-1" type="file" accept="image/jpeg, imgae/jpg;capture=camera" name="images[]" style="width: 0; height: 0">
                            <input id="images-2" type="file" accept="image/jpeg, imgae/jpg;capture=camera" name="images[]" style="width: 0; height: 0">
                            <input id="images-3" type="file" accept="image/jpeg, imgae/jpg;capture=camera" name="images[]" style="width: 0; height: 0">
                            <input id="images-4" type="file" accept="image/jpeg, imgae/jpg;capture=camera" name="images[]" style="width: 0; height: 0">
                            <div class="legend">
                                <button type="button" class="btn-select_pics">[LABEL-SELECT-PICS-UPLOAD]</button>
                            </div>
                       
                        </fieldset>
                        </div>
                    
                    <div class="location-container">
                        <h2 class="stylized-title"><span>[LOCATION-TITLE]</span></h2> 
                        
                        <input type="hidden" name="maps_lat" id="maps_lat" value="" />
                        <input type="hidden" name="maps_long" id="maps_long" value="" />
                        <input type="hidden" name="maps_city" id="maps_city" value="Montreal" />
                        <input type="hidden" name="hide_map" id='hide_map' value="yes" />
                        <input type="hidden" name="hide_map_check" id='hide_map_check' value="yes" />
                        <div id="map" class="map-container">
                            <!-- add cta for showing map -->
                            <a href="javascript:void(0)" id="show_map">[LOCATION-SHOW-BTN]</a>
                    </div>
                         
                        <div class="location-actions">
                            <a href="javascript:void(0)" id="remove_map" style="display:none;">[LOCATION-REMOVE-MAP] <i class="fa fa-trash"></i> </a>
                            <span class="notice add-success" style="display:none;">[LOCATION-NOTICE-ADD]</span>
                            <span class="notice remove-success" style="display:none;">[LOCATION-NOTICE-REMOVE]</span>
                            
                            </div>
                        
                        <div class="map_warning">
                            <span class="title">[LOCATION-WARNING-1]&nbsp;</span>[LOCATION-WARNING-2]
                                <br/>
                                [LOCATION-WARNING-3]
                            </div>
                        
                        </div>
                      <?php  
                $user_id = \Core\Utils\Session::getCurrentUserID();
                if($user_id == 63661  ){
            ?>
                    
                    <div class="video-block row mb-20">
                        <h2 class="stylized-title"><span>[LABEL-VIDEO-GALLERY]</span></h2>        
                            
                        <div class="field p-10-30 mb-20">
                                <div class="videos-container">
                                <div class="gallery_videos" id="gallery_videos">
                                       <?php
                                      
                                       foreach($user_videos as $video){ 
                                        $adresa =  $video['user_id']."/". $video['video_name'];
                                       ?>
                                       
                                       <div class="video-preview" rel-movie="<?php echo $videolocation;?><?php echo $adresa; ?>" rel-id="<?php echo $video["id"]; ?>" style="background-image: url(<?php echo $videolocation;?><?php echo $adresa; ?>.jpg);"><div class="video-remove"></div><div class="video-select">[LABEL-VIDEO-SELECT]</div></div>
                                       <?php }?>
                                    </div>
                                </div>
                                <div class="progress-bar"></div>
                                <input type="hidden" name="attached_video" id="attached-video">
                                <input id="video-upload" type="file" accept=".mov,video/mp4" name="videoupload" style="width: 0; height: 0">
                                <div class="legend">
                                    <button type="button" class="btn-select_video">[LABEL-VIDEO-UPLOAD]</button>
                                <a href="javascript:remove_selection()" style="margin-left: 10px;"> [LABEL-VIDEO-UNSELECT]</a>
                                </div>
                                
                            </div>
                        <h3>
                            <span class="title">[LABEL-TIPS]!&nbsp;</span> [LABEL-VIDEO-HELP]
                            <br/>
                     [LABEL-VIDEO-NOTE]
                        </h3>
                    </div>
                    
                    <?php } 
                    ?>
                    
                       
                    <div class="row bottom-container">
                            <div class="field">
                            <label class="label-free_ad">
                                <input class="free-ad" type="submit" value="[BTN-CAPTION-NEW-LISTING-FREE]"/></label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="tips-modal" class="modal-pm-gray">
    <!-- Modal content -->
    <div class="modal-pm-content">
        
        <div class="modal-pm-header">
            <h2>[LABEL-TIPS]</h2>
            <a href="javascript:void(0)" class="modal-close"><i class="fa fa-close"></i></a>
        </div>
        <div class="modal-pm-body">
            [LABEL-TIPS-CREATE-AD]
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left" style="margin: 0 auto;">
                <input type="button" class='btn-orange modal-close' value="[LABEL-CLOSE]"/>
            </div>
        </div>
    </div>
</div>
<div id="play-video-modal" class="play-video-modal modal-pm-gray">
    <!-- Modal content -->
    <div class="modal-pm-content">
        
        <div class="modal-pm-header">
            <div class="fa fa-close video-modal-close"></div>
            <h2>Preview Video</h2>
        </div>
        <div class="modal-pm-body">
           <video width="100%" controls controlsList="nodownload" id='movie-load'>
             
              Your browser does not support the video tag.
           </video> 
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left" style="margin: 0 auto;">
                <input type="button" class='btn-orange modal-close video-modal-close' value="[LABEL-CLOSE]"/>
            </div>
        </div>
    </div>
</div>

