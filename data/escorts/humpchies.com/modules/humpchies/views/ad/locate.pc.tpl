<?php $city_list = \Modules\Humpchies\CityModel::getCitiesRegions(); ?>
<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li>[LOCATION-CRUMB]</li>
    </ul>    
 </nav>
 <div class="wrapper_content">
    <div class="content_box">
        <h2 class="title">[LOCATION-CRUMB]</h2>
        
        <div class="content_box_body">
            
            <p>To show ads close to you, please activate your device location or select a city from the list below and drag the PIN to the appropiate location:</p>
            
            <div class="field mb-15">
                <div class=" mt-5">
                    <label class="custom-select">
                        <select name="city_map" id="city_map" data-must-be="">
                        <option value=''>[LABEL-SELECT-CITY]</option>
                        <?php foreach($city_list as $key => $list):?>
                            <optgroup label="<?php echo $list['region']['name'];?>">
                                <?php foreach($list['cities'] as $key => $city):?>
                                    
                                    <option value="<?php echo $city['slug'];?>" <?php echo (isset($_POST['city']) && $city['slug'] == $_POST['city'])? 'selected' : '';?>><?php echo $city['name'];?></option>
                                <?php endforeach;?>
                            </optgroup>    
                        <?php endforeach;?>
                        </select>
                    </label>    
                </div>
            </div>
            
    <div id="map" class="mt-5" style="height:calc(100vh - 190px); width: 100%; margin-top:35px;" data-city="Montreal" data-country="Canada"></div>
                    
        </div>
        
    </div>    
        
</div>


