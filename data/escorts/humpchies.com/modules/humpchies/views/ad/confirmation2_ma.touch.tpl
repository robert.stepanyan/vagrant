 <style type="text/css">
 .checked-text{
    margin-bottom: 20px;
    line-height: 21px;

    text-align: left;
    font-family: "Roboto";
    font-size: 16px;
    padding-top: 12px;
    margin-left: 30px;
    margin-right:30px;
    
 }
 .grey-separator{
     width:100%;
     height:1px;
     border-bottom:1px solid #707070;
 }
 .payments{
     display:flex;
     justify-content: center;
     margin-top: 20px;
         flex-wrap: wrap;
 }
 .payment-item{
     width: 315px;
     border: 1px solid #FF9900;
     border-radius: 5px;
     text-align: left;
     padding: 21px;
     margin: 39px 15px;
     font-family: "Roboto";
     
 }
 .premium:before, .bump:before{
    content: "";
    background-image:url("<?=Modules\Humpchies\Utils::getCDNPath('images')?>premium-star2.png");
    content: "";
    width: 59px;
    height: 58px;
    position: absolute;
    top: -38px;
    left: 125px;
    display: block;
 }
 .bump:before{
    content: "";
    background-image:url("<?=Modules\Humpchies\Utils::getCDNPath('images')?>bump-up2.png");
 }
 .payment-item-title{
     color:#FF9900;
     font-size: 16px;
     text-align: center;
     padding: 7px 15px 15px;
 }
 .payment-item-options {
    line-height: 18px;
    font-size: 14px;
    font-family: "roboto";
    color: #A4A4A4;
}
a.button-submit{
    background-color: #FF9900;
    border: 0px;
    width: 100%;
    padding: 15px;
    font-size: 16px;
    border-radius: 5px;
    margin-top: 22px;
    display: block;
    color:#000;
    text-align: center;
}
a.button-submit:hover{
    background-color: #e58900;    
}
a.lame-link {
    display: inline-block;
    width: 80%;
    border: 1px solid #FF9900;
    padding: 10px;
    border-radius: 5px;
    font-size: 14px;
    font-family: "Roboto";
    margin-top: 31px;
} 

.galben{
    color:#ff9900;
} 
.lame{
    margin-bottom : 50px;
    
}

 </style>
<div class="create-ad">
    
    <div class="content_box" >
        <div  class='grey-separator' style=""></div>
        <div class="content_box_header" style="margin: 30px auto;margin-bottom: 0px; text-align:center; display:block">
            <div style="display:flex;margin: 10px ; margin-left: 46px;    margin-bottom: 20px;">
            <img src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>checked_white.svg" border="0" width="65" height="65" alt="" style="margin: 2px auto;"/>
            <div class="checked-text"><?= \Core\Utils\Text::translateTextWithVars("[NEW-AD-SUCCESS-MESSAGE]", array("%id%" => $adid, '%city%' => $ad[0]['city'] ))?> </div>
        </div>
        <div  class='grey-separator' style=""></div>    
        <div class="payments">
            <div class='payment-item premium'>
                <div class="payment-item-title"> Premium </div>
                <div class="payment-item-options"> [NEW-AD-PAYMENT-DESCRIPTION] </div>
                <div class="payment-item-action"> <a class='button-submit' href="<?=\Core\Utils\Navigation::urlFor('payment', 'checkout',['ad_id' => $adid,city => $ad[0]['city'] ], false)?>" >[NEW-AD-PAYMENT-BUTTON]</a>
                </div> 
            </div>   
            <div class='payment-item bump'>
                <div class="payment-item-title"> Bump </div>
                <div class="payment-item-options"> [NEW-AD-BUMP-DESCRIPTION]. </div>
                <div class="payment-item-action"><a class='button-submit' href="<?=\Core\Utils\Navigation::urlFor('payment', 'bumps',['ad_id' => $adid], false)?>">[NEW-AD-BUMP-BUTTON]</a>
                </div> 
            </div>    
        </div>
        <div  class='grey-separator' style=""></div> 
        <div class='lame'>
            <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings');?>" class="lame-link"> [NEW-AD-CONTINUE-FREE]</a>
        </div>
    </div>
</div>            
