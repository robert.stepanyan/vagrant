<?php

 $chatAdID = $ad['id'];
 $chatAdTitle = $ad['title'] ;
?>

<style>
    li.comment {    margin-bottom: 20px;}

#report-other{
    display: block;
}

</style>

<?php
    $custom_class = '';
    switch(true) {
        case (@$ad['vip']):
            $custom_class = "vip";
            break;
        
        case (@$ad['premium']):
            $custom_class = "premium";
            break;
        
        default:
            break;     
    }
?>


<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li> <a href="<?php echo $ad['category_url'];?>"><?php echo $ad['category_name'];?></a></li>
        <li> <a href="<?php echo $ad['category_url_w_city'];?>"><?php echo $ad['category_city_crumb'];?></a></li>
        <li> [LABEL-CURRENT-AD]</li>
    </ul>
    
     <div class='buttons'>
        <a href="<?php echo $ad['buttons']['backurl'];?>"  class="previous" <?php if ($ad['buttons']['backurl']==''){ echo 'style="visibility:hidden"';}?>>
           <i class="fa fa-arrow-circle-left" style="margin-right: 5px;"></i> [LABEL-PREVIOUS-AD]
        </a>
        <a href="<?php echo $ad['buttons']['nexturl'];?>" class="next">
            [LABEL-NEXT-AD] <i class="fa fa-arrow-circle-right" style="margin-left: 5px;"></i>
        </a>
    </div>
        
 </nav>

  
<div class="wrapper_content">
   
    <div class="content_box">
        
        <?php if($ad['status'] == 'active'):?>
             
            <?php if($ad['fake_status']): ?>
                <h6 class="fake-pics"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;[LABEL-AD-FAKE-PICS]</h6>
            <?php endif; ?>
            
            <div class="content_box_body details_grid  <?php echo $custom_class;?>">
                <div class="user_content_box">  
                
                    <div class="gallery">
                        <div class="user_main_photo" style="background-image: url(<?=$ad['images']['main']['src']?>)">
                            <div class="photo_gallery">
                                <?=($ad['has_thumbs'] ? '<a href="' . $ad['images']['main']['href'] . '" data-fancybox="gallery" rel="lightbox[photo]" title="' . $ad['title'] . '">' : NULL)?>
                                    <span></span>
                                <?=($ad['has_thumbs'] ? '</a>' : NULL)?>
                            </div>
                        </div>
                        <div class="user_secondary_photos">
                            <?php   foreach(array(\Core\Utils\Image::ORIENTATION_LANDSCAPE, \Core\Utils\Image::ORIENTATION_PORTRAIT, \Core\Utils\Image::ORIENTATION_SQUARE) as $thumb_orientation) {
                                        if(isset($ad['images']['thumbs'][$thumb_orientation])) {
                                            foreach($ad['images']['thumbs'][$thumb_orientation] as $thumb) {
                            ?>
                                               <a href="<?=$thumb['href']?>" rel="lightbox[photo]" data-fancybox="gallery" title="<?=$ad['title']?>" class="secondary_image"  style="background-image: url(<?=$thumb['href']?>)"></a>
                            <?php           }    
                                        }    
                                    }?>
                            <?php if(\Core\Utils\Session::getCurrentUserID() == 63661 && $attached_video):?>
                                <a data-fancybox href="#movie-load"><span class="playpause" style="display: block;"></span></a>
                                <video width="100%" controls="" controlslist="nodownload" id="movie-load" style="display:none;" rel="lightbox[photo]"  data-fancybox="gallery">
                                    <source src="<?php echo $videolocation.$attached_video?>" type="video/mp4">
                                </video>
                            <?php endif;?>
                        </div>
                    
                    
                    
                        <?php if (@$ad['vip']) { ?>
                            <span class="vip_flag">VIP <i class="fa fa-crown" style="margin-left: 10px;"></i></span>
                        <?php }?>
                        <?php if( $ad['is_premium'] ){ ?>
                            <span class="premium_flag">Premium <i class="fa fa-crown"></i></span>  
                        <?php } ?>
                        <?php if( $ad['verification_status'] == "verified") {?>
                            <span class="real-photos">[LABEL-REAL-PHOTO] <i class="fa fa-check-circle"></i></span>
                        <? } ?>
                    
                    </div>
                    
                 
                    <div class="user_content_details"> 
                        
                        <h2 class="title">
                            <?php echo $ad['orig_title']; ?>
                        </h2>
                 
                        <p class="description">
                            <?php echo $ad['description']; ?>
                        </p>
                        
                        <?php if($services !== false) :?>
                            <div id="services">
                                <h3>[LABEL-SERVICES]</h3>
                                <div class="services_box">
                                    <?php foreach($services as $service):?>
                                        <span class="service"><i class="fa fa-check-circle"></i> <?php echo $service["name"];?></span>
                                    <?php endforeach;?>    
                                </div>
                            </div>
                        <?php endif;?>
                            
                        <?php if($map_coords && $map_coords['hide_map'] == 0 && $map_coords['location_photo'] != '') : ?>
                            
                            <?php $path = \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_images') . 'locations/';?> 
                            <div class="map-box">
                                <div id="simple_map" style="background-image:url('<?php echo $path . $map_coords['location_photo'];?>'); display:block; "></div>
                                <?php if(\Core\Utils\Session::getCurrentUserID() == 63661):?>
                                    <a href="javascript:void(0)" id="toggle_interactive" class="btn_map" style="right: 10px; padding: 6px 10px; border-radius: 4px; font-size: 13px">Interactive Map</a>
                                <?php endif;?>
                                
                                <input type="hidden" name="show_map" id="show_map" value="yes" />
                                <div id="map" style="height:0; width: 100%;margin: 0;" <?php echo ($map_coords && $map_coords['lat'] != 0)? 'data-lat="'.$map_coords['lat'].'" data-long="'.$map_coords['long'].'"' : '' ;?>></div>
                            </div>
                           
                        <?php else:?>
                            <input type="hidden" name="show_map" id="show_map" value="no" />
                        <?php endif;?>
                           
                    </div>
                </div>
            </div>
            
            <div class="content_box_body details_grid">
                <div class="user_content_box">
                    <div class="user_content_details">
                    <section class="comments-box">
                        <?php if($ad["blocked_comments"] == 1): ?>
                            <article>
                                <h3 class="blocked-comments" data-text="[LABEL-USER-BLOCKED-COMMENTS]"></h3>
                            </article>    
                        <?php else: ?>
                                
                            <article>
                                <?php if(!empty($comments)): ?>
                                    <h3>[LABEL-COMMENTS-FOR-THIS-USER]</h3>
                                    <ul class="comments">
                                        <?php foreach($comments as $comment): ?>
                                        <li class="comment">
                                            <p class="comment-msg"><?php echo $comment['comment']; ?></p>
                                            <div class="comment-data">
                                                
                                                <?php if ($comment['user_id'] == $comment['from_user']): ?>
                                                    <p>
                                                        <span class="adv-reply">[LABEL-COMMENTS-ADVERTISER-REPLY]</span>
                                                        <span><?php echo $comment['date']?></span>
                                                    </p>
                                                <?php elseif($comment['ad_status'] == 'active'): ?>
                                                    <a href="<?php echo $comment['ad_url'];?>"><?php echo $comment['ad_title'];?></a> 
                                                    <p>
                                                        <span class="member-name"><?php echo $comment['member_name']?></span>
                                                        <span><?php echo $comment['date']?></span>  
                                                    </p>                                     
                                                <?php else: ?>
                                                    <span><?php echo $comment['ad_title'];?></span> 
                                                    <p>
                                                        <span class="member-name"><?php echo $comment['member_name']?></span>
                                                        <span><?php echo $comment['date']?></span>
                                                    </p>                                      
                                                
                                                <?php endif; ?>    
                                             </div>
                                        </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php else: ?>
                                    <h3>[LABEL-NO-COMMENTS-FOR-THIS-USER]</h3>
                                <?php endif; ?>
                            </article>
                            
                            <?php if( $ad["blocked_comments"] == 0 && !empty($comments)): ?>
                               
                                <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::DEVICE_MODE); ?>
                                    
                            <?php endif;?>
                            
                            <article class="add-comment-area">
                                
                                <a href="javascript:void(0);" id="btn-write-comment" class="btn btn-orange-outline" <?php echo $redirect ? ' data-rdr="1"' : ( (!$is_member) ? ' data-alert="1"': '' ); ?>>[BTN-WRITE-COMMENT]</a> 
                                
                                
                                <?php if(\Core\Utils\Session::currentUserIsMember() && $user_has_pending_comment): ?>
                                    <p style="">[LABEL-COMMENT-MODERATION]</p>
                                <?php else:?>
                                    <p></p>                                
                                <?php endif; ?>
                                <div id="comment-box" style="display: none;">
                                    <p class="error" id="err-msg" style="display: none"></p>
                                    <textarea name="comment" id="comment" placeholder="[LABEL-WRITE-MESSAGE]" rows="5"></textarea>
                                    <input type="hidden" name="" id="ad_id" value="<?php echo $ad['id']; ?>">
                                    
                                    <a href="javascript:void(0);" class="btn btn-orange" id="save-comment" <?php echo $redirect ? ' data-rdr="1"' : ( (!$is_member) ? ' data-alert="1"': '' ); ?> style="margin: 20px 0 20px auto;">[LABEL-POST-COMMENT]</a>
                                    
                                </div>
                            </article>
                        <?php endif;?>    
                        <div class="lds-dual-ring-cntr"><div class="lds-dual-ring"></div></div>
                    </section>
                </div>
            </div>
        </div>

               
        
        <?php else:?>
            <div class="content_box_body details_grid" style="margin-top: 60px;">
                <div class="user_content_box">
                    <div class="user_content_details" style="padding: 10px 0;">
                        <p class="alert_description" style="border: none;">[INACTIVE-AD-TEXT]&nbsp;<a href="<?php echo $ad['category_url_w_city'];?>"><?php echo $ad['city_name'];?></a></p>
                    </div>
                </div>
                <div style="clear:both;"></div>
            </div>
        <?php endif;?>
           
        <?php echo "<!-- freeads " . print_R($freeads, true)."-->"; ?>
        <?php if(@$ads) :?>

        <div class="content_box_body">
            <h3 class="title"><?php echo ($custom_class != '')? '[LABEL-MORE-ADS-USER]' : '[LABEL-MORE-ADS-CITY]';?></h3> 
              
            <?php   
                foreach($ads as $ad_more) {
                    if($ad_more['id']) {
                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_user_ads_v2', \Core\View::DEVICE_MODE));
                    }
                }
            ?>
            
        </div>
        <?php endif; ?>
    </div>
</div>

<div class="wrapper_column details">
    <?php if($ad['status'] == 'active'): ?>
        <div class="phone-area">
            <div class="icons">
                <span>
                    <i class="fa fa-phone"></i>
                    <i class="fa fa-phone-volume"></i>
                    <i class="fab fa-whatsapp"></i>
                </span>
                <p>
                    <?php echo $ad['phone']; ?>
                    <?php if($ad['phone_validated'] == 'Y'): ?>
                        <i class="fa fa-check"></i>
                    <?php endif;?>
                </p>
            </div>
            <p class="txt">[PHONE-TEXT]</p>
        </div>
        <!-- private message -->
        <?php if ($ad['user_id'] != 4617 && $advertiser[0]["disable_pm"] != 1 ) { ?>
            <?php if ($chat['isLoggedIn']) { ?>
                <?php if ($chat['haveOpenedChat']) { ?>    
                    <a href="<?= \Core\Utils\Navigation::urlFor('chat', 'view', array('id' =>  $chat['chatId'])) ?>" class="btn btn-orange-outline"><i class="fa fa-comments"></i> [BTN-PRIVATE-MESSAGE]</a>
                <?php } else { ?>
                    <a href="javascript:void(0);" id="send-message-button" class="btn btn-orange-outline <?= $chat['canSendMessage'] ? '' : 'disabled' ?>"><i class="fa fa-comments"></i> [BTN-PRIVATE-MESSAGE]</a>
                <?php } ?>
            <?php } else { ?>
                <a  href="<?= \Core\Utils\Navigation::urlFor('user', 'login') ?>" class="btn btn-orange-outline"><i class="fa fa-comments"></i> [BTN-PRIVATE-MESSAGE]</a>
            <?php } ?>
        <?php } ?>
        
        <div class="data-container">
            <ul>
                <li>
                    <span>[LABEL-LOCATION]</span>

                    <?php echo ($ad['location'] == '')? 'Incalls' : ucfirst($ad['location']);?>
                </li>
                <li>
                    <span>[LABEL-CATEGORY]</span>

                    <a href="<?=$ad['category_url']?>">
                        <?=$ad['category_name']?>
                    </a>
                </li>
                <li>
                    <span>[LABEL-CITY]</span>

                    <a href="<?=$ad['category_url_w_city']?>">
                        <?=$ad['city_name']?>
                    </a>
                </li>
                <?php if($ad['price_min'] > 0 || $ad['price_max'] > 0 ): ?>
                    <li>
                        <span>[LABEL-PRICE-NEW]:</span>
                        <?php echo ( $ad['price_min'] > 0)? $ad['price_min'] .'$/30 min,' : '';?>
                        <?php echo ( $ad['price_max'] > 0)? $ad['price_max'] .'$/h' : '';?>
                    </li>
                <?php endif;?>
                
                <li>
                    <span>[LABEL-POSTED]</span>

                    <?=$ad['date_released']?>
                </li>
                
                <li class="end">
                    
                    <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE) : ?>
                        <div class="favorite"><a href="<?=\Core\Utils\Navigation::urlFor('user', 'login')?>" id="btn_favorite"><i class="fas fa-heart"></i>&nbsp;[LABEL-ADD-FAV]</a></div>
                    <?php else:?>
                        <input type="hidden" name="no_favorites" id="no_favorites" value="<?php echo count($user_favorites);?>" />    
                        <div class="favorite">
                            <a href="javascript:void(0)" id="btn_add_favorite" data-ut="<?php echo (\Core\Utils\Session::currentUserIsAdvertisment())? '1' : '2';?>" data-id="<?php echo $ad['id'];?>" <?php echo (in_array($ad['id'], $user_favorites))? 'style="display:none"' : 'style="display:block"';?>><i class="fas fa-heart"></i>&nbsp;[LABEL-ADD-FAV]</a>
                        </div>    
                        
                        <div class="favorite">
                            <a href="javascript:void(0)" id="btn_remove_favorite"  data-ut="<?php echo (\Core\Utils\Session::currentUserIsAdvertisment())? '1' : '2';?>" data-id="<?php echo $ad['id'];?>" <?php echo (!in_array($ad['id'], $user_favorites))? 'style="display:none"' : 'style="display:block"';?>><i class="fas fa-heart"></i>&nbsp;[LABEL-ADDED-FAV]</a>
                        </div> 
                        
                    <?php endif;?>
                </li>
                
                <?php if (strtolower(parse_url(\Core\Utils\Device::getServerVarsAndHeaders('http_referer'), PHP_URL_HOST)) == strtolower(\Core\Utils\Device::getServerVarsAndHeaders('http_host'))):?>
                    <li class="end">
                        <a onclick="gtag('event', 'click', {
                                    'event_category' : 'BackToAds',
                                    'event_label' : 'BackToAdsDesktop',
                                'event_callback': function(){return;}
                              });" href="<?php echo $go_back;?>">
                            <i class="fa fa-step-backward" aria-hidden="true"></i>
                            &nbsp;
                            <?=$ad['back_link_label']?>
                        </a>
                    </li>
                <?php endif;?>
                                        
                <li class="end">
                    <a href="javascript:void(0);" title="report" id ="report-button"><i class="fa fa-bug"></i>&nbsp; [LABEL-REPORT-TITLE]</a>
                </li>
                
            </ul>
        </div>
    <?php endif;?>
    
    <?php   
        // load right area banners   
        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::right_banners', \Core\View::DEVICE_MODE));
    ?>
    <p class="alert_description" rel-data ='[DESCRIPTION-AD-ALERT]'></p>
</div>



<!-- The Modal -->
<div id="report-profile" class="modal-white" style="display: none;">

    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <h2 class="filter-title">[LABEL-REPORT-TITLE]</h2>
            <a href="javascript:void(0)" id="close_report" class="close-modal"><i class="fa fa-times"></i></a>
        </div>
        <div class="modal-body report">
            <section class="availability">
                <div class="box">
                     <p class="full">
                        <input type="radio" id="report_1" name="report_form" value='1' checked />
                        <label for="report_1" class="xreport"></label>
                     </p>
                     <p class="full">
                        <input type="radio" id="report_2" name="report_form" value='2' />
                        <label for="report_2" class="xreport"></label>
                     </p>
                     <p class="full">
                        <input type="radio" id="report_3" name="report_form" value='3' />
                        <label for="report_3" class="xreport"></label>
                     </p>
                     <p class="full">
                        <input type="radio" id="report_4" name="report_form" value='4' />
                        <label for="report_4" class="xreport-more"></label>
                     </p>
                </div> 
            </section>
            
            <input type="hidden" id='adID' value ="<?=$chatAdID?>" >
            <div id='report-other' class="form-group white">
                <textarea id='report-other-text' class="form-control white" placeholder="[LABEL-REPORT-Z]"></textarea>
            </div>
            <?php if(!\Core\Utils\Session::getCurrentUserID() ){ ?>
                <div class="form-group white">
                    <input type="text" name="email_address" id="email" class="form-control" placeholder="[LABEL-REPORT-F]" />
                <div class="input-error" id="error_email" style="display: none;">[LABEL-REPORT-ERROR-EMAIL]</div>
                </div>
            <?php }?>
            <div class="form-group white">
            <div class="input-error" id="error_general" style="display:none;">[LABEL-REPORT-ERROR]</div>
        </div>
        </div>
        <div class="modal-body success" style="display: none;">
            [LABEL-REPORT-SUCCESS]
        </div>
        <div class="modal-footer report">
            <div class="modal-right">
                <div class="clear-container"><a href="javascript: void(0)" class="close-modal">[LABEL-CANCEL]</a></div>
                <input type="button" class="btn-orange small" value="[LABEL-SEND-REPORT]" id="send-report"/>
            </div>
        </div>
    </div>
</div>
<script>
    var rep_texts = new Array();
    rep_texts[1] = "[LABEL-REPORT-A]";
    rep_texts[2] = "[LABEL-REPORT-B]";
    rep_texts[3] = "[LABEL-REPORT-C]";
    rep_texts[4] = "[LABEL-REPORT-D]";
    var more_placeholder = "[LABEL-REPORT-Z]";
    var other_placeholder = "[LABEL-REPORT-E]";
</script>

<?php if ($chat['canSendMessage']) { ?>

<div id="send-message-modal" class="modal-white" style="display: none;">
    <!-- Modal content -->
    <div class="modal-content">
        
        <div class="modal-header">
            <h2>[LABEL-SEND-MESSAGE-TITLE] <?=$chatAdTitle?> </h2>
            <a href="javascript:void(0)"class="modal-pm-close close-modal"><i class="fa fa-times"></i></a>
        </div>
        <div class="modal-body">
            <div class="form-group white">
                <input type="hidden" id='send-message-ad-id' value="<?=$chatAdID?>" />
                <textarea id='send-message-body' placeholder="[LABEL-MESSAGE-PLACEHOLDER]" maxlength="500"></textarea>
            <div class="modal-error">
                    <span id="send-message-error-not-sent" class="input-error" style="display: none;">[ERROR-SEND-MESSAGE-NOT-SENT]</span>
                    <span id="send-message-error-required" class="input-error" style="display: none;">[ERROR-SEND-MESSAGE-REQUIRED]</span>
                    <span id="send-message-error-max-length" class="input-error" style="display: none;">[ERROR-SEND-MESSAGE-MAX-LENGTH]</span>
            </div>
        </div>
            </div>
        <div class="modal-footer">
            <div class="modal-right">
                <div class="clear-container"><a href="javascript: void(0)" class="modal-pm-close">[LABEL-CANCEL]</a></div>
                <input id="send-message" type="button" class="btn-orange small" value="[BTN-SEND-MESSAGE]"/>
            </div>
        </div>
    </div>
</div>

<div id="send-message-sent-modal" class="modal-white" style="display: none;">
    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <h2>[TITLE-MESSAGE-SENT]</h2> 
            <a href="javascript:void(0)"class="modal-pm-close close-modal"><i class="fa fa-times"></i></a>
        </div>
        <div class="modal-body">
            <h3 class="success">[LABEL-MESSAGE-SENT]</h3>
        </div>
        <div class="modal-footer">
            <div class="modal-right">
                <div class="clear-container"><a href="javascript: void(0)" class="modal-pm-close">[LABEL-CLOSE]</a></div>
                <a href="<?=\Core\Utils\Navigation::urlFor('chat', 'view')?>" class="btn-orange small" title="[LABEL-CHAT-LIST]">[LABEL-CHAT-LIST]</a>
            </div>
        </div>
    </div>
</div>

<?php } // can send message ?>
