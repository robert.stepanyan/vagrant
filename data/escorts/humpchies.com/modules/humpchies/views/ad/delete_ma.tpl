<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents a prompt to confrim ad deletion from the user.
 */
?>
<center>
    <div class="contact_box">
        <h2>
            [PROMPT-DELETE-AD]
        </h2>
        <h1>
            <a href="<?=$cancellation_url?>">[PROMPT-NO-DELETE-AD]</a>
        </h1>
        <form id="sign_up_form" method="post">
            <input type="hidden" name="token" value="<?=$token?>">
            <div class="field">
                <div>
                    <input class="button" type="submit" value="[BTN-CAPTION-DELETE-AD]" />
                </div>
            </div>
        </form>
    </div>
</center>