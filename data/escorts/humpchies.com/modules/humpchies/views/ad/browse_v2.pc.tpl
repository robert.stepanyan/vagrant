<?php $display_location == '';
    
    $no_filters = 0;
    $no_filters += ($location_filter != '')? 1 : 0; // availability
    $no_filters += ($has_comments_filter == 1)? 1 : 0; // has comments
    $no_filters += ($accepts_comments_filter == 1)? 1 : 0; // accepts_ comments
    $no_filters += ($real_photos_filter == 1)? 1 : 0; // real photos
    $no_filters += ($display_location != '')? 1 : 0; // city/region
    $no_filters += ($services_filter != '')? count(explode(',', $services_filter)) : 0; // services
    $no_filters += ($prices_filter != '')? count(explode(',', $prices_filter)) : 0; // prices
    $no_filters += ($phone1_filter != '' || $phone2_filter != '' || $phone3_filter != '')? 1 : 0; // phone
    $total_filters =  count($tags) + 5;  
?>

<?php if(isset($breadcrumb)):?>
<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <?php switch(true) {
        
            case ( isset($breadcrumb['city']) && isset($breadcrumb['category_url'])):
                $display_location  = $city_name;
        ?>
        
            <li> <a href="<?php echo $breadcrumb['category_url'];?>"><?php echo $breadcrumb['category_title'];?></a></li>
            <li><?php echo $breadcrumb['city'];?></li>
        <?php                                                         
            break;    
            case ( isset($breadcrumb['region']) && isset($breadcrumb['category_url'])):
                $display_location  = $breadcrumb['region'];
        ?>
            <li> <a href="<?php echo $breadcrumb['category_url'];?>"><?php echo $breadcrumb['category_title'];?></a></li>
            <li><?php echo $breadcrumb['region'];?></li>
        
        <?php                                                         
            break;    
            case ( !isset($breadcrumb['city']) && isset($breadcrumb['category_url'])):
        ?>
            <li> <?php echo $breadcrumb['category_title'];?></li>
         <?php                                                         
            break;    
            case ( isset($breadcrumb['city']) && !isset($breadcrumb['category_url'])):
                $display_location  = $breadcrumb['city'];
        ?>
            <li><?php echo $breadcrumb['city'];?></li>
        <?php                                                         
            break;
            case ( isset($breadcrumb['search'])): 
        ?>
            <li>'<?php echo $breadcrumb['search'];?>'</li>
        <?php                                                         
            break;
            case ( isset($breadcrumb['region'])):
                $display_location = $breadcrumb['region']; 
        ?>
            <li><?php echo $breadcrumb['region'];?></li>
        <?php                                                         
            break; 
        } ?>    
        
        
    </ul>    
 </nav>
 <?php endif;?>

<?php if(!isset($hide_filters)):?> 
<div id="filter-container">
    <div class="location-container">
        <input type="hidden" name="lang" id="lang" value="<?php echo \Core\Utils\Session::getLocale();?>" />
        <div class="form-label-group">
            <input type="text" id="search_city" name="search_city" class="form-control" value="<?php echo $display_location;?>" autocomplete="off" />
            <label for="search_box_city">[LABEL-SEARCH-CITY]</label>
        </div>
        <input type="hidden" name="category_city" id="category_city" value="<?php echo $category_code;?>">
        
        <ul id="suggestion-city" class="city_select city"></ul>
    </div>
    <form action="" method="POST" id="filters_form" data-services="" data-availability="">
        
        <div class="search-filters">
            <input type="hidden" name="location" id="location" value="<?php echo $location_filter;?>" />                         
            <input type="hidden" name="prices" id="prices" value="<?php echo $prices_filter;?>" />                         
            <input type="hidden" name="services" id="services" value="<?php echo $services_filter;?>" />                         
            <input type="hidden" name="has_comments" id="has_comments" value="<?php echo $has_comments_filter;?>" />                         
            <input type="hidden" name="real_photos" id="real_photos" value="<?php echo $real_photos_filter;?>" />                         
            <input type="hidden" name="accepts_comments" id="accepts_comments" value="<?php echo $accepts_comments_filter;?>" />                         
            <input type="hidden" name="phone1" id="phone1" value="<?php echo $phone1_filter;?>" />                         
            <input type="hidden" name="phone2" id="phone2" value="<?php echo $phone2_filter;?>" />                         
            <input type="hidden" name="phone3" id="phone3" value="<?php echo $phone3_filter;?>" />
            <input type="hidden" name="city_slug" id="city_slug" value="" />
            
            <div class="filters-box">
                <div class="filter-icon <?php echo ($no_filters > 0)? 'active' : '';?>"><i class="fa fa-filter"></i></div>
                <div class="content">
                    <p>[LABEL-MORE-FILTERS]</p>
                    <div class="filters_box" id="selected_filters">
                            <!-- add location filter and input -->
                            <?php if($location_filter != '' && $location_filter != 'all'):?>
                                <p> <?php echo $location_filter == "incalls"? '[LABEL-LOCATION-INCALL]' : '[LABEL-LOCATION-OUTCALL]';?>
                                    <a href="javascript:void(0)" id="remove_location" class="delete_filter availability"><i class="fa fa-times"></i> </a>
                                </p>
                            <?php endif;?>  

                            <!-- add tags filters  -->
                            <?php if($services_filter != ''):?>
                                <?php foreach($tags as $tag):?>
                                    <?php if(in_array($tag['slug'], explode(",", $services_filter))):?>
                                        <p> <?php echo $tag['name'];?>
                                            <a href="javascript:void(0)" id="remove_<?php echo $tag['slug'];?>" data-value="<?php echo $tag['slug'];?>" class="delete_filter service"><i class="fa fa-times"></i> </a>
                                        </p>
                                    <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>  
                            <?php if($prices_filter != ''):?>
                                <?php foreach($price_intervals as $key => $price):?>
                                    <?php if(in_array($key, explode(",", $prices_filter))):?>
                                        <p> <?php echo $price;?>
                                            <a href="javascript:void(0)" id="remove_<?php echo $key;?>" data-value="<?php echo $key;?>" class="delete_filter price"><i class="fa fa-times"></i> </a>
                                        </p>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>  
                        
                        <?php if($has_comments_filter == 1 ):?>
                            <p> [HAS-COMMENTS-LABEL] 
                                <a href="javascript:void(0)" id="remove_has_comments" data-value="has_comments" class="delete_filter additional"><i class="fa fa-times"></i> </a>
                            </p>
                        <?php endif;?>
                        
                        <?php if($accepts_comments_filter == 1 ):?>
                            <p> [ACCEPTS-COMMENTS-LABEL]
                                <a href="javascript:void(0)" id="remove_accepts_comments" data-value="accepts_comments" class="delete_filter additional"><i class="fa fa-times"></i> </a>
                            </p>
                        <?php endif;?> 
                        
                        <?php if($real_photos_filter == 1 ):?>
                            <p> [LABEL-REAL-PHOTO]
                                <a href="javascript:void(0)" id="remove_real_photos" data-value="real_photos" class="delete_filter additional"><i class="fa fa-times"></i> </a>
                            </p>
                        <?php endif;?> 
                    
                        <?php
                             $phone = [$phone1_filter, $phone2_filter, $phone3_filter];
                             $display_phone = implode('.', array_filter($phone));
                             if($display_phone != ''):
                        ?>    
                            <p> <?php echo $display_phone;?>
                                <a href="javascript:void(0)" id="remove_phones" class="delete_filter additional_phone"><i class="fa fa-times"></i></a>
                            </p>
                        <?php endif;?>
                        
                            </div>
                </div> 
                <a id="filters-mod" href="javascript:void(0)"><i class="fa fa-pencil-alt"></i></a>
            </div> 
            <a href="javascript:void(0)" id="search-filters-btn">[BTN-FILTER]</a>
    </div>
    </form>  
</div>
<?php endif;?>

<h2 class="stylized-title">
    <span><?=\Core\Controller::getTitle()?></span>
</h2>
                     
<div class="wrapper_content">
    <div class="content_box">
        <div class="content_box_body">
            <?php  
            
            $laboite_banner_impression = false;
                
            if(@$vip_ads)
                    {
                        $ad_count = 1;
                        $mid = (int)floor((count($vip_ads) / 2));

                        foreach($vip_ads as $ad)
                        {
                            $ad['vip'] = true;
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_v2', \Core\View::DEVICE_MODE));

                            if($ad_count === $mid)
                            {
                                //require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_1', \Core\View::DEVICE_MODE));
                                require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_laboite', \Core\View::DEVICE_MODE));
                                $laboite_banner_impression = true;
                            }

                            $ad_count ++;
                        }
                    }

                    if(@$premium_ads)
                    {
                        $ad_count = 1;
                        $mid = (int)floor((count($premium_ads) / 2));

                        foreach($premium_ads as $ad)
                        {
                            $ad['premium'] = true;
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_v2', \Core\View::DEVICE_MODE));

                            if($ad_count === $mid && !$laboite_banner_impression)
                            {
                                //require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_1', \Core\View::DEVICE_MODE));
                                require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_laboite', \Core\View::DEVICE_MODE));
                                $laboite_banner_impression = true;
                            }

                            $ad_count ++;
                        }
                    }

                    if(@$ads)
                    {
                        $ad_count = 1;

                        foreach($ads['recordset'] as $ad)
                        {
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_v2', \Core\View::DEVICE_MODE));

                            switch($ad_count)
                            {
                                case 4:
                                    //require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_1', \Core\View::DEVICE_MODE));
                                    if(!$laboite_banner_impression)
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_laboite', \Core\View::DEVICE_MODE));
                                    break;
                                case 8:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_2', \Core\View::DEVICE_MODE));
                                    break;
                                //case 12:
                                //    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_3', \Core\View::DEVICE_MODE));
                                //    break;
                                //case 16:
                                //   require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_4', \Core\View::DEVICE_MODE));
                                //    break;
                                //case 20:
                                //    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_5', \Core\View::DEVICE_MODE));
                                //    break;
                                case 12:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_escortbook', \Core\View::DEVICE_MODE));
                                    break;
                                default:
                                    break;
                            }

                            $ad_count ++;
                        }
                    }?>
            <?php if(intval($ads['count']) == 0):?>
                <?php if($no_filters > 0): ?>
                    <p class="no-ads">[ERROR-NO-ADS-FILTERS]</p>
                <?php else:?>
                    <p class="no-ads">[ERROR-NO-ADS]</p>
                <?php endif;?>
            <?php endif;?>
        </div>
    </div>
    <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::DEVICE_MODE);?>
    
    <?php if (isset($custom_description) && $custom_description != ''):?>
        <div class="custom_description" style="margin-top:50px;">
            <?php echo $custom_description;?>
        </div>
    <?php endif;?>
</div>



<div id="filters-modal" class="modal-white" style="display: none;">
    <!-- Modal content -->
    <div class="modal-content">
        
        <div class="modal-header">
            <i class="fa fa-filter"></i>
            <h2 class="filter-title <?php echo ($no_filters > 0)? 'active' : '';?>">[FILTERS-CURRENT]</h2>
            <a href="javascript:void(0)" id="close_filters"><i class="fa fa-times"></i></a>
        </div>
        <div class="modal-body">
            
            <section class="availability">
                <h3>[LABEL-PRICE-NEW]</h3>
                <div class="box">
                    <?php foreach($price_intervals as $key => $price):?>
                        <p>
                            <input class="styled-checkbox" name="selected_prices[]" id="prices-<?php echo $key;?>" type="checkbox" value="<?php echo $key;?>" data-label="<?php echo $price;?>" <?php echo in_array($key, explode(",", $prices_filter))? 'checked' : '';?> />
                            <label for="prices-<?php echo $key;?>"><?php echo $price;?></label>
                        </p>
                    <?php endforeach;?>
                </div> 
            </section>
            
            <section class="availability">
                <h3>[LABEL-LOCATION-AVAILABLE]</h3>
                <div class="box">
                    <p>
                        <input type="radio" id="location_all" name="location_filters" value="all" data-label="[LABEL-LOCATION-ALL]" <?php echo $location_filter == "all"? 'checked' : '';?> />
                        <label for="location_all">[LABEL-LOCATION-ALL]</label>
                      </p>
                      <p>
                        <input type="radio" id="location_incall" name="location_filters" value="incalls" data-label="[LABEL-LOCATION-INCALL]" <?php echo $location_filter == "incalls"? 'checked' : '';?> />
                        <label for="location_incall">[LABEL-LOCATION-INCALL]</label>
                      </p>
                      <p>
                        <input type="radio" id="location_outcalls" name="location_filters" value="outcalls" data-label="[LABEL-LOCATION-OUTCALL]" <?php echo $location_filter == "outcalls"? 'checked' : '';?> />
                        <label for="location_outcalls">[LABEL-LOCATION-OUTCALL]</label>
                      </p>
                </div> 
            </section>
            <section class="services">
                <div class="box" id="services_box">
                    <?php foreach($tags as $tag):?>
                        <p>
                            <input class="styled-checkbox" name="selected_services[]" id="services-<?php echo $tag['slug'];?>" type="checkbox" value="<?php echo $tag['slug'];?>" data-label="<?php echo $tag['name'];?>" <?php echo in_array($tag['slug'], explode(",", $services_filter))? 'checked' : '';?> />
                            <label for="services-<?php echo $tag['slug'];?>"><?php echo $tag['name'];?></label>
                        </p>
                    <?php endforeach;?>
                </div>
            </section>
            
            <section id="more_filters">
                <div class="box bordered">
                    <p class="box-3">
                        <input class="styled-checkbox" name="selected_real_photos" id="selected_real_photos" type="checkbox" value="real_photos" data-label="[LABEL-REAL-PHOTO]" <?php echo $real_photos_filter == 1? 'checked' : '';?> />
                        <label for="selected_real_photos">[LABEL-REAL-PHOTO]</label>
                    </p>
                    <p class="box-3">
                        <input class="styled-checkbox" name="selected_has_comments" id="selected_has_comments" type="checkbox" value="has_comments" data-label="[HAS-COMMENTS-LABEL]" <?php echo $has_comments_filter == 1? 'checked' : '';?> />
                        <label for="selected_has_comments">[HAS-COMMENTS-LABEL]</label>
                    </p>
                    <p class="box-3">
                        <input class="styled-checkbox" name="selected_accepts_comments" id="selected_accepts_comments" type="checkbox" value="accepts_comments" data-label="[ACCEPTS-COMMENTS-LABEL]" <?php echo $accepts_comments_filter == 1? 'checked' : '';?> />
                        <label for="selected_accepts_comments" style="white-space: nowrap;">[ACCEPTS-COMMENTS-LABEL]</label>
                    </p>
                    
                    
                </div> 
                
                <!-- add phone here -->
                <div class="form-group white">
                    <h3 style="margin-bottom: 10px;">[LABEL-TELEPHONE]</h3>
                    <div class="box multiple ">
                        <p style="width: 47px;"><input type="text" name="selected_phone1" id="selected_phone1" class="form-control phone_section" data-value="phone1" maxlength="3" placeholder="000" value="<?php echo $phone1_filter;?>" /></p>
                        <p style="width: 47px;"><input type="text" name="selected_phone2" id="selected_phone2" class="form-control phone_section" data-value="phone2" maxlength="3" placeholder="000" value="<?php echo $phone2_filter;?>" /></p>
                        <p style="width: 56px;"><input type="text" name="selected_phone3" id="selected_phone3" class="form-control phone_section" data-value="phone3" maxlength="4" placeholder="0000" value="<?php echo $phone3_filter;?>" /></p>
                    </div>
                </div>
            </section>
        </div>
        <div class="modal-footer">
            <div class="modal-right">
                <div class="clear-container"><a href="javascript: void(0)" class="clear-form">[FILTERS-CLEAR]</a> </div>
                <input type="button" class='btn-orange' id="filters-apply" value="[LABEL-APPLY-FILTERS]"/>
            </div>
        </div>
    </div>
</div>