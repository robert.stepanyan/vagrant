<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents a list of ads for the PC.
 */
?>
<h2 class="stylized-title">
    <span><?=\Core\Controller::getTitle()?></span>
</h2>
<div class="wrapper_content">
    <div class="content_box">
        
        <div class="content_box_body">
            <?php  if(@$vip_ads)
                    {
                        $ad_count = 1;
                        $mid = (int)floor((count($vip_ads) / 2));

                        foreach($vip_ads as $ad)
                        {
                            $ad['vip'] = true;
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long', \Core\View::DEVICE_MODE));

                            if($ad_count === $mid)
                            {
                                require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_1', \Core\View::DEVICE_MODE));
                            }

                            $ad_count ++;
                        }
                    }

                    if(@$premium_ads)
                    {
                        $ad_count = 1;
                        $mid = (int)floor((count($premium_ads) / 2));

                        foreach($premium_ads as $ad)
                        {
                            $ad['premium'] = true;
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long', \Core\View::DEVICE_MODE));

                            if($ad_count === $mid)
                            {
                                require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_1', \Core\View::DEVICE_MODE));
                            }

                            $ad_count ++;
                        }
                    }

                    if(@$ads)
                    {
                        $ad_count = 1;

                        foreach($ads['recordset'] as $ad)
                        {
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long', \Core\View::DEVICE_MODE));

                            switch($ad_count)
                            {
                                case 4:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_1', \Core\View::DEVICE_MODE));
                                    break;
                                case 8:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_2', \Core\View::DEVICE_MODE));
                                    break;
                                case 12:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_3', \Core\View::DEVICE_MODE));
                                    break;
                                case 16:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_4', \Core\View::DEVICE_MODE));
                                    break;
                                case 20:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::620x127_5', \Core\View::DEVICE_MODE));
                                    break;
                                default:
                                    break;
                            }

                            $ad_count ++;
                        }
                    }?>
        </div>
    </div>
    <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::DEVICE_MODE);?>
</div>