<?php
    $email_title = "Nouveau bump pour votre annonce #{$ad_id}";
?>


<html>
    <head>
        <title><?php echo $email_title;?></title>
        <style type="text/css">
            body {
                color: #fff;
                font-family: Tahoma, Arial;
                font-size: 14px;
                line-height: 1.2;
                text-align: left;
                background: #161616;
                margin: 0;
                padding: 0;
            }
            a:link, a:visited {
                color: #ff9900;
                font-weight: inherit;
                text-decoration: none!important;
            }
            a:hover, a:active {
                color: #ffffff;
                font-weight: inherit;
                text-decoration: none!important;
            }
            hr{
                color: #403f3f;
                height: 0;
                width: 100%;
                border: none;
                border-top: 1px solid #403f3f;
            }
            p {
                padding: 0 20px;
            }
            img {
                margin: 10px 20px;
            }
            h2 {
                font-weight: normal;
                font-size: 18px;
                margin: 20px 20px 35px 20px;
            }
        </style>
    </head>
    <body>
        
        <img src="https://gui.humpchies.com/images/pc/humnew.png" alt="Humpchies" style="height: 45px; margin: 15px;">
        <hr/>
        <h2 style="margin-bottom: 35px!important;"> <?php echo $email_title;?></h2>

        <p>Bonjour,</p>
        <p>
            Votre annonce #<?php echo $ad_id;?> a justement re&ccedil;u un bump dans la cat&eacute;gorie: <span style="font-weight: bold!important"><?php echo $category;?></span>, ville: <span style="font-weight: bold!important"><?php echo $city;?></span>.
            <br/>
            <a href="https://humpchies.com<?php echo $list_url;?>" target="_blank" style="text-decoration: none!important;font-size: 14px;width: 110px; background: #FF9900; border-radius: 4px; color: #000; display: block; padding: 13px 0; text-align: center; line-height: 16px;margin-top: 25px!important;margin-bottom: 25px!important;">Voir l'annonce</a>
        </p>

        <p>
            Meilleures salutations,
            <br/>
            L'&eacute;quipe Humpchies
        </p>
        <hr/>
        
    </body>
</html>