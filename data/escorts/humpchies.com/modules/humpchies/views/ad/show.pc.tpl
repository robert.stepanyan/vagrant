<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents one ad and all of its details. This is the Ad Hub page.
 */
 $chatAdID = $ad['id'];
 $chatAdTitle = $ad['title'] ;
?>

<style>
    div.user_content_photo {}
    div.user_content_photo > div { width: 100% !important; padding: 0 !important; height: 100% !important; }
    div.user_content_photo > div > a { display:block !important; width: 100% !important; height:100% !important; background-color: #111 !important; }
    div.user_content_photo > div span { display: inline-block !important; height: 100% !important; vertical-align: middle !important; }
    div.user_content_photo > div img { position: static !important; vertical-align: middle !important; display: inline-block !important; }
    .buttons { display: flex;justify-content: space-between; margin: 11px 0 16px 0; font-weight: bold;}
    
    .buttons > .previous {  padding: 10px; background-color: #222; border-radius: 4px; color: #ff9900;  text-transform: uppercase; cursor: pointer; display:inline-block:}
    .buttons > .next {  padding: 10px; background-color: #222; border-radius: 4px; color: #ff9900;  text-transform: uppercase; cursor: pointer; display:inline-block:}
    
    li.comment {    margin-bottom: 20px;}
    .video {
        width: 100%;
        border: 0px;
        outline: none;
    }
.user_video{
    display:block;
    width:auto;
    position:relative;
    clear:both;
    padding:10px;
}
.playpause {
   /* background-image: url(https://gui.humpchies.com/images/playmovie.png);   */
    background-repeat: no-repeat;
    height:39px;
    position: absolute;
    left: 9%;
    right: 0%;
    top: 0%;
    bottom: 0%;
    margin: auto;
    background-size: contain;
    background-position: center;
    position: absolute;
}

.gallery {
    display: flex;
}

.user_main_photo {
    max-width: 330px;
    margin: 10px;
    min-width: 330px;
    max-height: 384px;
    overflow: hidden;
    min-height: 384px;
    background-size: cover;
    background-position: top center;
}
.user_secondary_photos {
    display: flex;
    flex-wrap: wrap;
}
.secondary_image {
    margin: 10px;
    /* padding: 10px; */
    display: block;
    max-width: 125px;
    max-height: 190px;
    min-width: 125px;
    background-size: cover;
    background-position: center;
    /* min-height: 239px;*/
    
}

#services {
    padding: 20px 10px 5px 10px;
    border-top: 1px solid #3E3E3E;
    border-bottom: 1px solid #3E3E3E;
    margin-bottom: 20px;
}
#services h3 {
    font-size: 16px;
    color: #fff;
    font-weight: normal;
    margin: 0 0 20px 0;
}

#services .services_box {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    align-content: center;
}

#services .services_box .service {
    width: 33%;
    font-size: 15px;
    color: #AFAFAF;
    margin-bottom: 15px;
    display: flex;
}
#services .services_box .service i {
    font-size: 21px;
    color: #fff;
    margin-right: 7px;
}
#report-other{
    display: block;
}

</style>


<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li> <a href="<?php echo $ad['category_url'];?>"><?php echo $ad['category_name'];?></a></li>
        <li> <a href="<?php echo $ad['category_url_w_city'];?>"><?php echo $ad['category_city_crumb'];?></a></li>
        <li> <?php echo $ad['orig_title'];?></li>
    </ul>    
 </nav>

  
<div class="wrapper_content">
    <div class='buttons'>
        <a href="<?php echo $ad['buttons']['backurl'];?>"  class="previous" <?php if ($ad['buttons']['backurl']==''){ echo 'style="visibility:hidden"';}?>>
           <i class="fa fa-chevron-left" style="margin-right: 5px;"></i> [LABEL-PREVIOUS-AD]
        </a>
        <a href="<?php echo $ad['buttons']['nexturl'];?>" class="next">
            [LABEL-NEXT-AD] <i class="fa fa-chevron-right" style="margin-left: 5px;"></i>
        </a>
    </div>
    <div class="content_box">
        
        <?php if($ad['status'] == 'active'):?>
    
    
        <h2 class="title">
            <?php if( $ad['is_premium'] ){ ?>
                <span class="__premium">PREMIUM</span>
            <?php } ?>
            <?= $ad['title']?>
        </h2>
        
        
        <div style="position: relative;">
            <?php if (true === @$ad['vip']) { ?>
                <span class="vip_flag"></span><?php }?>
            <?php if( $ad['is_premium'] ){ ?>
                <span class="premium_flag"></span>  <?php } ?>
        </div>

        <?php if($ad['fake_status']): ?>
            <h6 class="fake-pics"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;[LABEL-AD-FAKE-PICS]</h6>
        <?php endif; ?>
        <div class="content_box_body details_grid">
            <div class="user_content_box">  
            <?php  
                $user_id = \Core\Utils\Session::getCurrentUserID();
                if($user_id == 63661 ){
            ?>
                <div class="gallery">
                    <div class="user_main_photo" style="background-image: url(<?=$ad['images']['main']['src']?>)">
                        <?php
                            $_orientation = '';
                            if( $ad['images']['main']['height'] > $ad['images']['main']['width'] ){
                                $_orientation = 'photo_portrait';
                            } elseif( $ad['images']['main']['height'] < $ad['images']['main']['width'] ){
                                $_orientation = 'photo_landscape';
                            } else {
                                $_orientation = 'photo_square';
                            }
                        ?>
                        <div class="<?=$_orientation?>"><?php //($ad['images']['main']['height'] === 390) ? 'photo_portrait' : (($ad['images']['main']['height'] === 260) ? 'photo_square' : 'photo_landscape') ?>
                            <?=($ad['has_thumbs'] ? '<a href="' . $ad['images']['main']['href'] . '" data-fancybox="gallery" rel="lightbox[photo]" title="' . $ad['title'] . '">' : NULL)?>
                                <span></span>
                               
                            <?=($ad['has_thumbs'] ? '</a>' : NULL)?>
                        </div>
                        
                          <?php if( $ad['verification_status'] == "verified") {?>
                            <span class="real-photos">[LABEL-REAL-PHOTO] </span>
                            <? } ?>
                    </div>
                    <div class="user_secondary_photos">
                        <?php   foreach(array(\Core\Utils\Image::ORIENTATION_LANDSCAPE, \Core\Utils\Image::ORIENTATION_PORTRAIT, \Core\Utils\Image::ORIENTATION_SQUARE) as $thumb_orientation)
                                {
                                    if(isset($ad['images']['thumbs'][$thumb_orientation]))
                                    {
                                        foreach($ad['images']['thumbs'][$thumb_orientation] as $thumb)
                                        {?>
                                           <a href="<?=$thumb['href']?>" rel="lightbox[photo]" data-fancybox="gallery" title="<?=$ad['title']?>" class="secondary_image"  style="background-image: url(<?=$thumb['href']?>)">
                                           </a>
                                          
                                           
                        <?php           }    
                                    }    
                                }?>
                    </div>
                </div>
                <?php if($attached_video){
                ?>
                <div class="user_video">
                    <video class='video' width="100%"  controls="" controlslist="nodownload" id="movie-load">
                        <source src="<?=$videolocation.$attached_video?>" type="video/mp4"></video>
                    <div class="playpause"></div>
                </div>
                
                
             <?php 
             }
             }else{
             // to be removedd after video is implemented
             ?>
             
             <div class="user_content_photo">
                    <?php
                        $_orientation = '';
                        if( $ad['images']['main']['height'] > $ad['images']['main']['width'] ){
                            $_orientation = 'photo_portrait';
                        } elseif( $ad['images']['main']['height'] < $ad['images']['main']['width'] ){
                            $_orientation = 'photo_landscape';
                        } else {
                            $_orientation = 'photo_square';
                        }
                    ?>
                    <div class="<?=$_orientation?>"><?php //($ad['images']['main']['height'] === 390) ? 'photo_portrait' : (($ad['images']['main']['height'] === 260) ? 'photo_square' : 'photo_landscape') ?>
                        <?=($ad['has_thumbs'] ? '<a href="' . $ad['images']['main']['href'] . '" data-fancybox="gallery" rel="lightbox[photo]" title="' . $ad['title'] . '">' : NULL)?>
                            <span></span>
                            <img src="<?=$ad['images']['main']['src']?>" width="100%" alt="<?=$ad['title']?>" />
                        <?=($ad['has_thumbs'] ? '</a>' : NULL)?>
                    </div>
                    
                      <?php if( $ad['verification_status'] == "verified") {?>
                        <span class="real-photos">[LABEL-REAL-PHOTO] </span>
                        <? } ?>
                </div>
                <div class="user_content_thumbs">
                    <?php   foreach(array(\Core\Utils\Image::ORIENTATION_LANDSCAPE, \Core\Utils\Image::ORIENTATION_PORTRAIT, \Core\Utils\Image::ORIENTATION_SQUARE) as $thumb_orientation)
                            {
                                if(isset($ad['images']['thumbs'][$thumb_orientation]))
                                {
                                    foreach($ad['images']['thumbs'][$thumb_orientation] as $thumb)
                                    {?>
                                        <div class="user_content_thumb">
                                            <div class="<?=($thumb_orientation === \Core\Utils\Image::ORIENTATION_LANDSCAPE ? 'thumb_landscape' : ($thumb_orientation === \Core\Utils\Image::ORIENTATION_PORTRAIT ? 'thumb_portrait' : 'thumb_square'))?>">
                                                <a href="<?=$thumb['href']?>" rel="lightbox[photo]" data-fancybox="gallery" title="<?=$ad['title']?>">
                                                        <img src="<?=$thumb['src']?>" width="<?=$thumb['width']?>" height="<?=$thumb['height']?>" alt="<?=$ad['title']?>" />
                                                </a>
                                            </div>
                                        </div> 
                    <?php           }    
                                }    
                            }?>
                </div>
             
             <?php }?>
                
                <div class="user_content_details">
                        <p class="description" style="color: #AFAFAF; font-size: 15px; line-height: 20px;">
                        <?=$ad['description']?>
                    </p>
                        <?php if($services !== false) :?>
                        <div id="services">
                            <h3>[LABEL-SERVICES]</h3>
                            <div class="services_box">
                                <?php foreach($services as $service):?>
                                    <span class="service"><i class="fa fa-check-circle"></i> <?php echo $service["name"];?></span>
                                <?php endforeach;?>    
                            </div>
                        </div>
                        <?php endif;?>
                    <section>
                        <article>
                            <ul>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <span>[LABEL-TELEPHONE]:</span>

                                    <?=$ad['phone']?> <?php if($ad['phone_validated']=='Y') {?><i class="fa fa-check" style="color: lime;    background: none;  font-weight: bold;"></i>
                                    <?php } ?>
                                   
                                    <!-- advertiser <?php //var_dump($advertiser[0]); ?> -->
                                    <?php if ($ad['user_id'] != 4617 && $advertiser[0]["disable_pm"] != 1 ) { ?>
                                        <?php if ($chat['isLoggedIn']) { ?>
                                            <?php if ($chat['haveOpenedChat']) { ?>    
                                                &nbsp; <a href="<?= \Core\Utils\Navigation::urlFor('chat', 'view', array('id' =>  $chat['chatId'])) ?>" class="pm">PM<span class="fa fa-send"></a>
                                            <?php } else { ?>
                                                &nbsp; <a href="#" id="send-message-button" class="pm <?= $chat['canSendMessage'] ? '' : 'disabled' ?>">PM<span class="fa fa-send"></span></a>
                                            <?php } ?>
                                        <?php } else { ?>
                                            &nbsp; <a  href="<?= \Core\Utils\Navigation::urlFor('user', 'login') ?>" class="pm">PM<span class="fa fa-send"></a>
                                        <?php } ?>
                                    <?php } ?>
                                  
                                     <?php if($ad['phone_validated'] == 'Y') {?>
                                    <div style="margin-left: 26px;color:lime">[LABEL-PHONE-CONFIRMED] <?=$ad['validate_date']?></div>
                                    <?php } ?>
                                </li>
                                <?php if($ad['myfans_url'] != ''):?>
                                    <li>
                                        <i class="fa fa-link"></i>
                                        <span>[LABEL-4MYFANS]</span>
                                        <?php if (!preg_match("~^(?:f|ht)tps?://~i", $ad['myfans_url'])) {
                                            $ad['myfans_url'] = "https://" . $ad['myfans_url'];
                                        }
                                        ?>
                                        <a href="<?=$ad['myfans_url']?>" target="_blank ">
                                            <?= preg_replace('#(^https?:\/\/(w{3}\.)?)|(\/$)#', '', $ad['myfans_url']); ?>    
                                        </a>
                                    </li>
                                <?php endif;?>
                                <li>
                                    <i class="fa fa-car"></i>
                                    <span>[LABEL-LOCATION]:</span>

                                        <?php echo ($ad['location'] == '')? 'Incalls' : ucfirst($ad['location']);?>
                                </li>
                                <li>
                                    <i class="fa fa-th-list"></i>
                                    <span>[LABEL-CATEGORY]:</span>

                                    <a href="<?=$ad['category_url']?>">
                                        <?=$ad['category_name']?>
                                    </a>
                                </li>
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <span>[LABEL-CITY]:</span>

                                    <a href="<?=$ad['category_url_w_city']?>">
                                        <?=$ad['city_name']?>
                                    </a>
                                </li>
                                    <?php if($ad['price_min'] > 0 || $ad['price_max'] > 0 ): ?>
                                        <li>
                                            <i class="fa fa-money"></i>
                                            <span>[LABEL-PRICE-NEW]:</span>
                                            <?php echo ( $ad['price_min'] > 0)? $ad['price_min'] .'$/30 min, ' : '';?>
                                            <?php echo ( $ad['price_max'] > 0)? $ad['price_max'] .'$/h' : '';?>
                                        </li>
                                    <?php endif;?>
                                    
                                <li>
                                    <i class="fa fa-calendar"></i>
                                    <span>[LABEL-POSTED]:</span>

                                    <?=$ad['date_released']?>
                                </li>
                            </ul>
                        </article>
                        <article>
                            <? if($ad['id'] != 1579901): ?>
                                <div class="social" style="display: flex; flex-direction: row; align-items: center; justify-content: flex-start; margin-bottom:0">
                                    <div class="title" style="width: auto; margin-right: 10px; display: flex; align-items: center; align-content: center;"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>[LABEL-SHARE]
                                </div>
                                    <a href="https://twitter.com/share?url=<?php echo rawurlencode(\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'). $ad['url']) ?>" rel="nofollow" target="_blank" style="display: flex;align-items: center;align-content: center;justify-content: center;">
                                    <i class="fa fa-twitter" aria-hidden="true" onclick="addSocialShare('twitter', 'view');"></i>
                                </a>
                            </div>
                             <? endif ?>
                                
                                
                                <input type="hidden" name="no_favorites" id="no_favorites" value="<?php echo (isset($user_favorites))? count($user_favorites) : 0;?>" />
                                    <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE) : ?>
                                        <div class="favorite"><a href="<?=\Core\Utils\Navigation::urlFor('user', 'login')?>" id="btn_favorite"><i class="fa fa-heart-o"></i>&nbsp;[LABEL-ADD-FAV]</a></div>
                                    <?php else:?>
                                        
                                        <div class="favorite">
                                            <a href="javascript:void(0)" id="btn_add_favorite" data-ut="<?php echo (\Core\Utils\Session::currentUserIsAdvertisment())? '1' : '2';?>" data-id="<?php echo $ad['id'];?>" <?php echo (in_array($ad['id'], $user_favorites))? 'style="display:none"' : 'style="display:block"';?>><i class="fa fa-heart-o"></i>&nbsp;[LABEL-ADD-FAV]</a>
                                        </div>    
                                        
                                        <div class="favorite">
                                            <a href="javascript:void(0)" id="btn_remove_favorite"  data-ut="<?php echo (\Core\Utils\Session::currentUserIsAdvertisment())? '1' : '2';?>" data-id="<?php echo $ad['id'];?>" <?php echo (!in_array($ad['id'], $user_favorites))? 'style="display:none"' : 'style="display:block"';?>><i class="fa fa-heart"></i>&nbsp;[LABEL-ADDED-FAV]</a>
                                        </div> 
                                        
                                        <?php if(\Core\Utils\Session::currentUserIsAdvertisment()) :?>
                                        <?php else:?>
                                        
                                        <?php endif;?>
                                        
                                    <?php endif;?>
                                
                                
                                
                            <ul>
                                <?php if (strtolower(parse_url(\Core\Utils\Device::getServerVarsAndHeaders('http_referer'), PHP_URL_HOST)) == strtolower(\Core\Utils\Device::getServerVarsAndHeaders('http_host'))):?>
                                <li>
                                        <a onclick="gtag('event', 'click', {
                                                    'event_category' : 'BackToAds',
                                                    'event_label' : 'BackToAdsDesktop',
                                                    'event_callback': function(){return;}
                                                  });" href="<?php echo $go_back;?>">
                                        <i class="fa fa-step-backward" aria-hidden="true"></i>
                                        &nbsp;
                                        <?=$ad['back_link_label']?>
                                    </a>
                                </li>
                                <?php endif;?>
                                    
                                <li>
                                    <a  href="javascript:;" title="report" id = 'report-button' >
                                        <j class="fa fas fa-bug"></j>&nbsp; [LABEL-REPORT-TITLE]
                                    </a>
                                </li>
                                    

                            </ul>
                       
                        </article>
                            
                            
                    </section>
                        <?php if($map_coords && $map_coords['hide_map'] == 0 && $map_coords['location_photo'] != '') : ?>
                            
                                <?php $path = \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_images') . 'locations/';?> 
                                
                                <div style="margin-bottom:20px; position:relative; display:block">
                                    <div id="simple_map" style="background-image:url('<?php echo $path . $map_coords['location_photo'];?>'); display:block; "></div>
                                <?php if(\Core\Utils\Session::getCurrentUserID() == 63661):?>
                                    <a href="javascript:void(0)" id="toggle_interactive" class="btn_map" style="right: 10px; padding: 6px 10px; border-radius: 4px; font-size: 13px">Interactive Map</a>
                                <?php endif;?>
                            
                                <?php 
                                    $lat = floatval($map_coords['lat']);
                                    $lng = floatval($map_coords['long']);
                                    ?>
                                <input type="hidden" name="show_map" id="show_map" value="yes" />
                                <div id="map" style="height:0; width: 100%;margin: 0;" <?php echo ($map_coords && $map_coords['lat'] != 0)? 'data-lat="'.$map_coords['lat'].'" data-long="'.$map_coords['long'].'"' : '' ;?>></div>
                            
                            </div>
                            
                        <?php else:?>
                            <input type="hidden" name="show_map" id="show_map" value="no" />
                        <?php endif;?>
                    <p class="alert_description" rel-data ='[DESCRIPTION-AD-ALERT]'>

                    </p>
                </div>
            </div>
        </div>
        
        <div class="content_box_body details_grid">
            <div class="user_content_box">
                <div class="user_content_details">
                    <section class="comments-box">
                            <?php if($ad["blocked_comments"] == 1):?>
                        <article>
                                    <p style="" class="blocked-comments" data-text="[LABEL-USER-BLOCKED-COMMENTS]"></p>
                            </article>    
                        <?php else:?>
                            
                            <article>
                                
                            <?php if(!empty($comments)) : ?>
                            <p style="">[LABEL-COMMENTS-FOR-THIS-USER]</p>
                            <ul class="comments">
                                <?php foreach($comments as $comment) : ?>
                                <li class="comment">
                                    <span class="member-name"><?= $comment['member_name']?></span>|<span ><?=$comment['date']?></span>|
                                    <? if ($comment['user_id'] == $comment['from_user']):?>
                                        <span>[LABEL-COMMENTS-ADVERTISER-REP]</span>
                                            <? elseif($comment['ad_status'] == 'active'): ?>
                                        <span>[LABEL-COMMENTS-FOR-AD]</span> <a href="<?=$comment['ad_url'];?>"><?=$comment['ad_title'];?></a>                                        
                                            <? else: ?>
                                                <span>[LABEL-COMMENTS-FOR-AD]</span> <?=$comment['ad_title'];?>                                       
                                            
                                                <? endif; ?>
                                                    
                                    <p class="comment-msg"><?= $comment['comment']?></p>
                                                
                                                <div class="comment-score" style="margin-top:10px;">
                                                    <div class="up">
                                                        <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE) : ?>
                                                            <a title="[HREF-TITLE-LOGIN]" href="<?=\Core\Utils\Navigation::urlFor('user', 'login')?>">
                                                                <i class="fa fa-thumbs-up"></i>
                                                            </a>
                                                        <?php else: ?>
                                                            <a href="javascript:void(0)" class="thumbsup <?php echo (isset($user_votes[$comment['id']]) && $user_votes[$comment['id']] == 'up')? 'active' : '';?>" data-comment="<?php echo $comment['id'];?>" data-ad="<?php echo $comment['ad_id'];?>"><i class="fa fa-thumbs-up"></i> </a>
                                                        <?php endif;?>
                                                        <span class="score" id="up-<?php echo $comment['id'];?>"><?php echo $comment['up'];?></span>
                                                    </div>
                                                    <div class="down">
                                                        <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE) : ?>
                                                            <a title="[HREF-TITLE-LOGIN]" href="<?=\Core\Utils\Navigation::urlFor('user', 'login')?>">
                                                                <i class="fa fa-thumbs-down"></i>
                                                            </a>
                                                        <?php else: ?>
                                                            <a href="javascript:void(0)" class="thumbsdown <?php echo (isset($user_votes[$comment['id']]) && $user_votes[$comment['id']] == 'down')? 'active' : '';?>" data-comment="<?php echo $comment['id'];?>" data-ad="<?php echo $comment['ad_id'];?>"><i class="fa fa-thumbs-down"></i></a>
                                                        <?php endif;?>
                                                        <span class="score" id="down-<?php echo $comment['id'];?>"><?php echo $comment['down'];?></span>
                                                    </div>
                                                </div>
                                                
                                </li>
                                <?php endforeach; ?>
                            </ul>
                            <?php else: ?>
                            <p style="">[LABEL-NO-COMMENTS-FOR-THIS-USER]</p>
                            <?php endif; ?>
                        </article>
                        <article class="add-comment-area" >
                            <?php if(\Core\Utils\Session::currentUserIsMember() && $user_has_pending_comment) :?>
                                <p style="">[LABEL-COMMENT-MODERATION]</p>
                            <?php else:?>
                                <p></p>                                
                            <?php endif; ?>
                            <p class="error" id="err-msg" style="display: none"></p>
                            <textarea name="comment" id="comment" placeholder="[LABEL-WRITE-MESSAGE]" rows="5"></textarea>
                            <input type="hidden" name="" id="ad_id" value="<?=$ad['id']?>">
                            <div>
                                <input type="button" value="[LABEL-POST-COMMENT]" class="btn" id="save-comment"<?php echo $redirect ? ' data-rdr="1"' : ( (!$is_member) ? ' data-alert="1"': '' ); ?>>
                            </div>
                            
                        </article>
                        <?php endif;?>    
                        <div class="lds-dual-ring-cntr"><div class="lds-dual-ring"></div></div>
                    </section>
                </div>
            </div>
        </div>

            <?php if( $ad["blocked_comments"] == 0  && !empty($comments)) : ?>
            <div style="margin-top:20px;">
                <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::DEVICE_MODE);?>
            </div>    
        <?php endif;?>    
       
        <?php else:?>
            <div class="content_box_body details_grid" style="margin-top: 60px;">
                <div class="user_content_box">
                    <div class="user_content_details" style="padding: 10px 0;">
                        <p class="alert_description" style="border: none;">[INACTIVE-AD-TEXT]&nbsp;<a href="<?php echo $ad['category_url_w_city'];?>"><?php echo $ad['city_name'];?></a></p>
                    </div>
                </div>
                <div style="clear:both;"></div>
            </div>
        <?php endif;?>
           
        <?php if(@$ads) :?>

        <div>
            <div  class="box-wrapper clearfix">
                <p class="title">[LABEL-MORE-ADS-FROM-THIS-USER]</p>
                <?php   
                        foreach($ads as $ad)
                        {
                            if($ad['id']){
                                require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_user_ads', \Core\View::DEVICE_MODE));
                            }
                        }
                ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>

<style>
    h2.title { line-height: 24px !important; }
    .__premium { padding: 4px 7px; margin-right: 10px; background: #ff9900; border-radius: 4px; color: #333;
        text-shadow: -1px 0 1px #fff; }
</style>

<!-- The Modal -->
<div id="report-profile" class="modal_report">

    <!-- Modal content -->
    <div class="modal-content_report">
        <div class="modal-header_report">
            <span class="close_report">&times;</span>
            <h2>[LABEL-REPORT-TITLE]</h2>
        </div>
        <div class="modal-body_report">
            
            <ol>
                <li><label for='report_1' class="xreport">  <input type="radio" id='report_1' name='report_form' value='1' checked><span></span></label></li>
                <li><label for='report_2' class="xreport">  <input type="radio" id='report_2' name='report_form' value='2'><span></span></label></li>
                <li><label for='report_3' class="xreport">  <input type="radio" id='report_3' name='report_form' value='3'> <span></span></label></li>
                <li><label for='report_4' class="xreport-more">  <input type="radio" id='report_4' name='report_form' value='4'> <span></span></label></li>
            </ol>
            <input type="hidden" id='adID' value ="<?=$chatAdID?>" >
            <div id='report-other'>
                <textarea id='report-other-input' placeholder="[LABEL-REPORT-Z]"></textarea>
            </div>
            <?php if(!\Core\Utils\Session::getCurrentUserID() ){ ?>
                <input type="text" name="email_address" id="email_address" placeholder="[LABEL-REPORT-F]" />
                <div class="input-error" id="error_email" style="display: none;">[LABEL-REPORT-ERROR-EMAIL]</div>
            <?php }?>
            <div class="input-error" id="error_general" style="display:none;">[LABEL-REPORT-ERROR]</div>
        </div>
        <div class="modal-body-success" style="display: none;">
            [LABEL-REPORT-SUCCESS]
        </div>
        <div class="modal-footer_report">
            <input type="button" class='btn' value="[LABEL-SEND-REPORT]" id="send-report"/>
        </div>
    </div>
</div>
<script>
    var rep_texts = new Array();
    rep_texts[1] = "[LABEL-REPORT-A]";
    rep_texts[2] = "[LABEL-REPORT-B]";
    rep_texts[3] = "[LABEL-REPORT-C]";
    rep_texts[4] = "[LABEL-REPORT-D]";
    var more_placeholder = "[LABEL-REPORT-Z]";
    var other_placeholder = "[LABEL-REPORT-E]";
</script>

<?php if ($chat['canSendMessage']) { ?>

<div id="send-message-modal" class="modal-pm-gray">
    <!-- Modal content -->
    <div class="modal-pm-content">
        
        <div class="modal-pm-header">
        <div class="fa fa-close modal-pm-close"></div>
            <h2>[LABEL-SEND-MESSAGE-TITLE] <?=$chatAdTitle?> </h2>
        </div>
        <div class="modal-pm-body">
            <input type="hidden" id='send-message-ad-id' value="<?=$chatAdID?>">
            <textarea id='send-message-body' placeholder="[LABEL-MESSAGE-PLACEHOLDER]"
                      maxlength="500"></textarea>
            <div class="modal-error">
                <span id="send-message-error-not-sent">[ERROR-SEND-MESSAGE-NOT-SENT]</span>
                <span id="send-message-error-required">[ERROR-SEND-MESSAGE-REQUIRED]</span>
                <span id="send-message-error-max-length">[ERROR-SEND-MESSAGE-MAX-LENGTH]</span>
            </div>
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left">
                <input type="button" class='btn-red modal-pm-close' value="[LABEL-CANCEL]"/>
            </div>
            <div class="modal-pm-right">
                <input id="send-message" type="button" class='btn-orange' value="[BTN-SEND-MESSAGE]"/>
            </div>
        </div>
    </div>
</div>

<div id="send-message-sent-modal" class="modal-pm-gray">
    <!-- Modal content -->
    <div class="modal-pm-content">
        <div class="modal-pm-header">
            <div class="fa fa-close modal-pm-close"></div>
            <h2>[TITLE-MESSAGE-SENT]</h2> 
        </div>
        <div class="modal-pm-body">
            <h3>[LABEL-MESSAGE-SENT]</h3>
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left">
                <input type="button" class='btn-red modal-pm-close' value="[LABEL-CLOSE]"/>
            </div>
            <div class="modal-pm-right">
                <a href="<?=\Core\Utils\Navigation::urlFor('chat', 'view')?>" class="btn-orange" title="[LABEL-CHAT-LIST]">[LABEL-CHAT-LIST]</a>
            </div>
        </div>
    </div>
</div>

<?php } // can send message ?>
