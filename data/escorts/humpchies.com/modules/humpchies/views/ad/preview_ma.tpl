<?php
/**
 * Copyright (c) MAX, webmaster@humpchies.com
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This is the ad preview page.
 */?>
 
<style>
    div.user_content_photo {}
    div.user_content_photo > div { width: 100% !important; padding: 0 !important; height: 100% !important; }
    div.user_content_photo > div > a { display:block !important; width: 100% !important; height:100% !important; background-color: #111 !important; }
    div.user_content_photo > div span { display: inline-block !important; height: 100% !important; vertical-align: middle !important; }
    div.user_content_photo > div img { position: static !important; vertical-align: middle !important; display: inline-block !important; }
</style>

<div class="wrapper_content">
    <div class="content_box">
        <h2 class="title"><?= $ad['title']?></h2>
        <?php if (true === @$ad['vip']) { ?>
            <span class="vip_flag">VIP <i class="fa fa-star"></i></span> 
        <?php } ?>
        <div class="content_box_body details_grid">
            <div class="user_content_box">
                <div class="user_content_photo">
                    <?php
                        $_orientation = '';
                        if( $ad['images']['main']['height'] > $ad['images']['main']['width'] ){
                            $_orientation = 'photo_portrait';
                        } elseif( $ad['images']['main']['height'] < $ad['images']['main']['width'] ){
                            $_orientation = 'photo_landscape';
                        } else {
                            $_orientation = 'photo_square';
                        }
                    ?>
                    <div class="<?=$_orientation?>"><?php //($ad['images']['main']['height'] === 390) ? 'photo_portrait' : (($ad['images']['main']['height'] === 260) ? 'photo_square' : 'photo_landscape') ?>
                        <?=($ad['has_thumbs'] ? '<a href="' . $ad['images']['main']['href'] . '" rel="lightbox[photo]" data-fancybox="gallery" title="' . $ad['title'] . '">' : NULL)?>
                            <img src="<?=$ad['images']['main']['src']?>" width="100%" alt="<?=$ad['title']?>" />
                        <?=($ad['has_thumbs'] ? '</a>' : NULL)?>
                       
                    </div>
                </div>
                <div class="user_content_thumbs">
                    <?php   foreach(array(\Core\Utils\Image::ORIENTATION_LANDSCAPE, \Core\Utils\Image::ORIENTATION_PORTRAIT, \Core\Utils\Image::ORIENTATION_SQUARE) as $thumb_orientation)
                            {
                                if(isset($ad['images']['thumbs'][$thumb_orientation]))
                                {
                                    foreach($ad['images']['thumbs'][$thumb_orientation] as $thumb)
                                    {?>
                                        <div class="user_content_thumb">
                                            <div class="<?=($thumb_orientation === \Core\Utils\Image::ORIENTATION_LANDSCAPE ? 'thumb_landscape' : ($thumb_orientation === \Core\Utils\Image::ORIENTATION_PORTRAIT ? 'thumb_portrait' : 'thumb_square'))?>">
                                                <a href="<?=$thumb['href']?>" rel="lightbox[photo]" data-fancybox="gallery" title="<?=$ad['title']?>">
                                                    <img src="<?=$thumb['src']?>" width="<?=$thumb['width']?>" height="<?=$thumb['height']?>" alt="<?=$ad['title']?>" />
                                                </a>
                                            </div>
                                        </div> 
                    <?php           }    
                                }    
                            }?>
                </div>
                <div class="user_content_details">
                    <p class="description">
                        <?=$ad['description']?>
                    </p>
                    <section>
                        <article>
                            <ul>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <span>[LABEL-TELEPHONE]:</span>

                                    <?=$ad['phone']?>
                                </li>
                                <li>
                                    <i class="fa fa-car"></i>
                                    <span>[LABEL-LOCATION]:</span>

                                    <?=$ad['location']?>
                                </li>
                                <li>
                                    <i class="fa fa-th-list"></i>
                                    <span>[LABEL-CATEGORY]:</span>

                                    <a href="<?=$ad['category_url']?>">
                                        <?=$ad['category_name']?>
                                    </a>
                                </li>
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <span>[LABEL-CITY]:</span>

                                    <a href="<?=$ad['category_url_w_city']?>">
                                        <?=$ad['city_name']?>
                                    </a>
                                </li>
                                <li>
                                    <i class="fa fa-calendar"></i>
                                    <span>[LABEL-POSTED]:</span>

                                    <?=$ad['date_released']?>
                                </li>
                            </ul>
                        </article>
                        <article>
                             <? if($ad_m['id'] != 1579901): ?>
                            <div class="social">
                                <div class="title"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>[LABEL-SHARE]
                                </div>
                                
                                <a href="https://twitter.com/share?url=<?php echo rawurlencode(\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'). $ad['url'])?>" rel="nofollow" target="_blank">
                                    <i class="fa fa-twitter" aria-hidden="true" onclick="addSocialShare('twitter', 'view');"></i>
                                </a>
                               
                            </div>
                            <? endif; ?>
                            <ul>
                                <li>
                                    <a onclick="history.go(-1); return false;" href="#">
                                        <i class="fa fa-step-backward" aria-hidden="true"></i>
                                        &nbsp;
                                        <?=$ad['back_link_label']?>
                                    </a>
                                </li>
                            </ul>
                        </article>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
