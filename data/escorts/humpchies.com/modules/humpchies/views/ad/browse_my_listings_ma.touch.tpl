
<?php

        $statuses = [];
        foreach($ads['recordset'] as $ad) {
        
            if(!in_array($ad['status'], $statuses, TRUE)) {
                $statuses[$ad['status']] = [];
                
            }
        }
        foreach($ads['recordset'] as $ad) {
            foreach($statuses as $status => $item) {
                if($status == $ad['status'] && !@in_array($ad,  $status, TRUE)) {
                    $statuses[$status][] = $ad;
                }
            }
        }
?>

<!-- add custom holiday banner here -->
<?php 
        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::browse_banner', \Core\View::DEVICE_MODE));
?>
<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i><a/></li>
        <li>[LABEL-MY-ADS]</li>
    </ul>    
</nav>
<div class="wrapper">
    <?/* if(\Core\Utils\Session::getCurrentUserID() !== 63661): */?>
        <div class="credits-buy">
             <div class="credits">
                 <i class="fa fa-money"></i>
             <span>[LABEL-YOUR-CREDITS]: <?= number_format(\Modules\Humpchies\UserModel::getCredits(\Core\Utils\Session::getCurrentUserID())) ?></span>
             </div>
             <a href="<?=\Core\Utils\Navigation::urlFor('payment', 'bumps-purchase')?>">
                 [LABEL-BUY] <i class="fa fa-cart-plus"></i>
             </a>
        </div>
    <?/* endif; */?>
    <div class="content_box my-ads-box">
        <?php require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::support_ticket_action', \Core\View::DEVICE_MODE));?>
      <?php if(0):?><div  class="filter-my-ads">
           <a class="<?= ($active_ads_count == 0 ? 'disactive' : '' ) ?>" href="<?= ( $active_ads_count == 0 || $exp_ads_count == 0 ? '#' : \Core\Utils\Navigation::urlFor('ad', 'my-active-listings')) ?>">
            <span> [LABEL-ACTIVE-ADS]</span></a> &nbsp;
           <a class="<?= ($exp_ads_count == 0 ? 'disactive' : '' ) ?>" href="<?= ( $active_ads_count == 0 || $exp_ads_count == 0 ? '#' : \Core\Utils\Navigation::urlFor('ad', 'my-expired-listings')) ?>">
            <span> [LABEL-EXPIRED-ADS]</span></a>
        </div>
 <div class="ad_options">
           Tap on ad for options
       </div> <?php endif;?>
       
       <div class="filter_ads_box">
            <div class="filter-container">
            <h2>[LABEL-FILTER-ADS]</h2>
            <div class="selection_box">
                <span class="current">[LABEL-LOCATION-ALL]</span>
                </div>
            </div>    
                <ul class="select_options">
                    <li><a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-active-listings')?>"><span>[LABEL-ACTIVE-ADS]</span></a></li>    
                    <li><a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-expired-listings')?>"><span>[LABEL-EXPIRED-ADS]</span></a></li>
            </ul>
       </div>
       
       
        <div class="content_box_body">
            
            <?php   
                if(@$ads) {
                  foreach($statuses as $status => $items) {
                     
                        switch($status){
                            case 'new':
                                echo '<h2> [LABEL-M-MODERATION-ADS] </h2>';
                                break;
                            case 'active':
                                echo '<h2>[LABEL-M-ACTIVE-ADS]</h2>';
                                break;
                            case 'paused':
                                echo '<h2>[LABEL-M-PAUSED-ADS]</h2>';
                                break;
                            case 'inactive':
                                echo '<h2>[LABEL-M-INACTIVE-ADS]</h2>';
                                break;
                            case 'deleted':
                                echo '<h2>[LABEL-M-DELETED-ADS]</h2>';
                                break;
                            default:
                                break;
                        }
                     foreach($statuses[$status] as $ad) {
                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_my_ads', \Core\View::DEVICE_MODE));
                        }
                    }
                }
            ?>
        </div>
        <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::LOCALIZED_DEVICE_MODE);?>
    </div>
    
   
            <div id="mobile-post-id"><a href="<?=\Core\Utils\Navigation::urlFor('ad', 'create')?>">[LABEL-POST-AD] <i class="fa fa-plus-circle"></i></a></div>
   
</div>
