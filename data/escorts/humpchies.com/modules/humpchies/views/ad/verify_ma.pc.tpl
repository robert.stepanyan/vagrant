 <style >
 .centered , .previewLargeGallery {
  position: fixed;
  top: 50%;
  left: 50%;
  margin-top: -50px;
  margin-left: -100px;
  border-radius: 4px;
  cursor: pointer;
}
.previewLargeGallery img{
   border-radius: 4px; 
}
.backgroundLargeGallery{
  position: fixed;
  top:0px;
  left:0px;
  right:0px;
  bottom:0px;
  background-color:rgba(0, 0, 0, 0.8);
  cursor: pointer;
}
.spangold{
    color:#ff9900;
}
 </style>
 
 <nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li> <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>">[LABEL-MY-ADS]</a></li>
        <li>[AD-VERIFY-TITLE]</li>
    </ul>    
</nav>
<div class="create-ad">
    <form id='verification-form' method="post">
    <div class="content_box"  style="font-size: 14px;">
        <h1 class="h1title"> [AD-VERIFY-TITLE] #<?php echo $adid; ?> <?=$title ?></h1>    
       <div class="content_box_header " style="background-color:#0e0e0e; padding:30px 20px; text-align: left;border-left: 10px solid #ff9900;border-top-left-radius: 7px;border-bottom-left-radius: 7px;">
           <img src="https://gui.humpchies.com/images/info.png" style="margin-right: 20px;margin-left:10px;"/>
           <div> [AD-VERIFY-TEXT]</div>
        </div>
        <div class ='footer-verify' style="display: flex;  margin-top: 20px;">
             <div class ='fleft'>
                 <input type='hidden' name='adid' id='adid' value="<?php echo $adid; ?>" />
                 <input type='hidden' name='uploadedFiles' id='uploaded-files' value="" />
                <input type='file' name='private[]' multiple id='private' style='width:0px;height:0px;'  accept="image/*" />
                <!-- <div class='button-orange button upload-private' >Upload Private Pictures</div>   -->
             </div>
            
        </div>
        <div class ='upload-gallery-preview' style='display:block'>
            <div class ='preview-title'  style="color: #fff;">
                [AD-VERIFY-VERIFICATION-PICTURES] 
            </div>
            <div class='gallery-content' id='gallery-content'>
                <span id="upload-private2" style="display: inline-block;min-width:130px;width:130px;height:140px; margin:24px 0 24px 24px; cursor:pointer; padding-top:20px">
                    <img src="https://gui.humpchies.com/images/upload.png" border="0" width="211" height="197" style="max-width: 110px; max-height:96px;" alt="upload images">
                </span>
            </div>
        </div>
        <div class ='footer-verify' style="display: flex;  margin-top: 20px;">
             <div class ='fleft' style="width: 62%">
                 <a href="<?php echo \Core\Utils\Navigation::urlFor('ad', 'my-listings'); ?>" class='button-orange-outline button '>[AD-VERIFY-SKIP]</a>
             </div>
             <div class='fright' style="width: 38%">    
                <a class="button-orange button continue" id='submitVerification' href='#' style="margin-right: 0px;text-align: center;padding-right: 67px;min-width: calc(100% - 15px);">[AD-VERIFY-SEND] </a>
             </div>
        </div>
        <div class ='upload-gallery-preview' style='display:block'>
            <div class ='preview-title' style="color: #fff;">
                [AD-VERIFY-GALLERY] 
            </div>
            <div class='gallery-content'>
            <?php foreach((array)$images as $img) {?>
                <div class="gallery_item" data-index="0" style="background-image:url('<?=$img['href']; ?>')">
                </div>
            <?php }?>
            </div>
        </div>                           
    </div>
    </form> 
</div>  