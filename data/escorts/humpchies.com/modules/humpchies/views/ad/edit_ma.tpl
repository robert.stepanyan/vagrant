<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This is the ad edit page.
 */
  $city_list = \Modules\Humpchies\CityModel::getCitiesRegions();
 ?>
 <nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i><a/></li>
        <li> <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>">[LABEL-MY-ADS]<a/></li>
        <li><?=\Core\Controller::getTitle()?> #<?=@$_GET['id']?></li>
    </ul>    
</nav>

<div class="wrapper_content">
    <div class="lds-dual-ring-cntr">
        <div class="lds-dual-ring"></div>
    </div>
    <div class="content_box">
        <div class="content_box_header">
            <h2 class="title"><?=\Core\Controller::getTitle()?> #<?=@$_GET['id']?></h2>  
        </div>
        <div class="content_box_body">
            <div class="post_box">
                <form id="post_pad" method="post" enctype="multipart/form-data">
                    <div class="field">
                        <label>*[LABEL-CITY]:</label>
                        <div>                                  
                            <select name="city" data-must-be="<?php echo $_POST['city'];?>">
                                 <?php foreach($city_list as $key => $list):?>
                                    <optgroup label="<?php echo $list['region']['name'];?>">
                                        
                                        <?php foreach($list['cities'] as $key => $city):?>
                                            <option value="<?php echo $city['slug'];?>" <?php echo (isset($_POST['city_id']) && $city['id'] == $_POST['city_id'])? 'selected' : '';?>><?php echo $city['name'];?></option>
                                        <?php endforeach;?>
                                    </optgroup>    
                                 <?php endforeach;?>   
                             
                             
                             </select>
                        </div>
                    </div>
                    <div class="field">
                        <label>*[LABEL-TITLE]:</label>
                        <div>
                            <input class="text" name="title" type="text" maxlength="<?=$max_len_title?>" value="<?=@$_POST['title']?>" />
                        </div>
                    </div>
                     <div class="field"><label>&nbsp;</label><div style="border: 1px solid #333;line-height: 14px;color: #fff;padding: 10px;width: 278px;text-align: justify;">[LABEL-PRICE-TITLE]</div></div>
                    <div class="field">
                        <label>*[LABEL-DESCRIPTION]:</label>
                        <div>
                            <textarea name="description"  maxlength="950" ><?=@$_POST["description"]?></textarea>
                        </div>
                    </div>
                    <div class="field">
                        <label>[LABEL-PRICE]:</label>
                        <div>
                            <input class="text" name="price" type="text" maxlength="<?=$max_len_price?>" value="<?=@$_POST['price']?>" />
                        </div>
                    </div>
                    <div class="field">
                        <label>[LABEL-PRICE-CONDITIONS]:</label>
                        <div>
                            <input class="text" name="price_conditions" type="text" maxlength="<?=$max_len_price_conditions?>" value="<?=@$_POST['price_conditions']?>" />
                        </div>
                    </div>
                    
                    <div class="field">
                        <label for="myfans_url" class="myfans_url">[LABEL-4MYFANS]:</label>
                        <div>
                            <input class="text" id="myfans_url" name="myfans_url" type="text"  value="<?=@$_POST['myfans_url']?>" />
                            <small>[NEW-4MYFANS]</small>
                        </div> 
                    </div>
                    
                    <div class="field">
                        <label for="phone">*[LABEL-TELEPHONE]:</label>
                        <div>
                            <input class="text" id="phone" name="phone" type="text" maxlength="<?=$max_len_phone?>" value="<?=(isset($_POST["phone"]) ? preg_replace('/\d{3}/', '$0-', str_replace('-', null, $_POST["phone"]), 2) : (!empty($_SESSION['user']['phone']) ? preg_replace('/\d{3}/', '$0-', str_replace('-', null, $_SESSION['user']['phone']), 2) : ''))?>" />
                            <input id="phone_country_iso" name="phone_country_iso" type="hidden" value="<?=@$_POST['phone_country_iso']?>">
                            <input id="phone_country_code" name="phone_country_code" type="hidden">
                        </div>
                    </div>
                      <?php 
                    if(\Core\utils\session::getCurrentUserID() == 63661 /*81062*/ ){ ?>
                    <div class="field">
                        <label ></label>
                        <div>
                             <input type="checkbox" name='validate_phone' id='validate_phone' value='yes'/> &nbsp;[LABEL-VALIDATE-TELEPHONE]
                        </div>
                    </div>
                    <?php } ?>
                    <div class="field">
                        <label>[LABEL-LOCATION]:</label>
                        <div>
                            <?php \Core\Controller::renderPlugin('location_options', NULL, \Core\View::MODELESS);?>
                        </div>
                    </div>
                    <div class="field">
                        <label>&nbsp;</label>
                        <div><input class="button" type="submit" value="[LABEL-SAVE-CHANGES]" /></div>
                    </div>
                </form>
                <br/><br/><br/><br/><br/><br/><br/>
               <div style="border: 1px solid #333;line-height: 14px;color: #fff;padding: 10px;text-align: justify; margin-bottom:20px">[LABEL-TITLE-VERIFY-AD]</div>
                <div id="photos">
                    <div class="photos_container">
                        <?php if ($image_count < $max_image_count) { ?>
                        <div class="upload_cont mb-10 clearfix">
                            <form id="img-upload" action="/ad/photos?a=upload" enctype="multipart/form-data"
                                  method="post" class="mb-10">
                                <input type="hidden" name="ad_id" id="ad_id" value="<?= $ad_id ?>">
                                <input type="hidden" name="img_count" id="img_count" value="<?=$image_count?>">
                                <div id="custom-fileinput" class="custom-fileinput clearfix" style='cursor:pointer'>
                                    <span class="lbl" style="cursor:pointer" id='label-choose' onclick="">[LABEL-CHOOSE-FILES]</span>
                                    <input type="text" disabled placeholder="[LABEL-NO-FILES-CHO0SEN]" class="lbl-placeholder" title="">
                                </div>
                                <input type="file" name="images[]" class="file-input" accept="image/jpeg" multiple
                                       id="images" title="" style="position: absolute;
    /* margin-top: -52px; */
    left: 0px;
    top: 2px;
    opacity: 0;
    display: block;width:100px">
                                <button type="submit" class="button">[BTN-UPLOAD]</button>
                            </form>
                            <p id ="error-msg" class="error-msg"></p>
                            <div id="preview_images" class="mb-20"></div> 
                            <div id="log_preview_images" class="mb-20"></div>
                        </div>
                        <?php } else { ?>
                        <input type="hidden" name="ad_id" id="ad_id" value="<?= $ad_id ?>">
                        <?php } ?>
                        <p class="img-header">[LABEL-PUBLIC-PHOTOS]</p>
                        <ul class="images mb-10 clearfix">
                            <?php  if (is_array($images)){
                            foreach($images as $i => $image) { ?>
                            <li class="image<?=$image['is_main'] ? ' main':''?>">
                                <div class="wrapper">
                                    <img src="<?= $image['href'] ?>" width="180" title="" alt=""/>
                                    <div class="action">
                                        <span class="move"></span>
                                        <span class="main">
                                            <label class="custom-control custom-checkbox" for="p-<?=$i?>">
                                                <input type="checkbox" class="custom-control-input" id="p-<?=$i?>" name="photo_id" value="<?=$image['id']?>">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description"></span>
                                            </label>
                                        </span>
                                    </div>
                                </div>
                            </li>
                            <?php }} ?>
                        </ul>
                        <div class="action-buttons clearfix">
                            <button class="set-main" style="cursor: pointer; z-index:1000">[BTN-SET-MAIN]</button>
                            <button class="delete"  style="cursor: pointer; z-index:1000" >[BTN-DELETE]</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
