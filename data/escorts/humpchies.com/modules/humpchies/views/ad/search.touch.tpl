<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents a list of search ads on Mobile.
 */
?>
<style>

.custom_description {
    color: #999;
    text-align: justify;
}
.custom_description h3 {
    color: #b7b7b7;
    margin-top: 20px;
    margin-bottom: 10px;
}
</style>
<?php if(isset($breadcrumb)):?>
<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <?php switch(true) {
        
            case ( isset($breadcrumb['city']) && isset($breadcrumb['category_url'])):
        ?>
            <li> <a href="<?php echo $breadcrumb['category_url'];?>"><?php echo $breadcrumb['category_title'];?></a></li>
            <li><?php echo $breadcrumb['city'];?></li>
        <?php                                                         
            break;    
            case ( !isset($breadcrumb['city']) && isset($breadcrumb['category_url'])):
        ?>
            <li> <?php echo $breadcrumb['category_title'];?></li>
         <?php                                                         
            break;    
            case ( isset($breadcrumb['city']) && !isset($breadcrumb['category_url'])):
        ?>
            <li><?php echo $breadcrumb['city'];?></li>
        <?php                                                         
            break;
            case ( isset($breadcrumb['search'])): 
        ?>
            <li>'<?php echo $breadcrumb['search'];?>'</li>
        <?php                                                         
            break;
            case ( isset($breadcrumb['region'])): 
        ?>
            <li><?php echo $breadcrumb['region'];?></li>
        <?php                                                         
            break; 
        } ?>  
        
    </ul>    
 </nav>
 <?php endif;?>
  

<div class="wrapper">
    <div class="content_box">
        <div class="content_box_header">
            <h2 class="title"><?=\Core\Controller::getTitle()?></h2>
            <div style="display:flex; align-items: center; margin-top: 20px; justify-content: center;">
                <?php if($cities_with_zones != null):?>
                <a href="javascript:void(0)" id="cities_list" title="" style="margin-right:20px;"><i class="fa fa-map-marker"></i> [LABEL-SELECT-CITY-BTN]</a>
                <?php endif;?>
                <?php if(isset($show_filter) && $show_filter):?>
            <form id="location_form" method="get">
                        <label class="custom-select" for="location">
                        <select name="location" id="location"  onchange="this.form.submit();">
                    <option value="">[LABEL-LOCATION-AVAILABLE]</option>
                    <option value="all" <?php echo $location_filter == "all"? 'selected' : '';?>>[LABEL-LOCATION-ALL]</option>
                    <option value="incalls" <?php echo $location_filter == "incalls"? 'selected' : '';?>>[LABEL-LOCATION-INCALL]</option>
                    <option value="outcalls" <?php echo $location_filter == "outcalls"? 'selected' : '';?>>[LABEL-LOCATION-OUTCALL]</option>
                </select>
                        </label>
                    </form> 
            <?php endif;?>
            </div>        
        </div>
        <div class="content_box_body">
            <?php
                   //require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_forum', \Core\View::DEVICE_MODE));
                  
                    //http://jira.sceon.am/browse/HUMP-173  eros banner must be always 9 position no matter vip prem or regular listing
                   $eros_banner_impression = false;
                   $edir_banner_impression = false;
                   $avalon_banner_impression = false;
                   $forum_banner_impression = false;
                   $laboite_banner_impression = false;
                   $xxxception_banner_impression = true;
                   $escortbook_banner_impression = false;
                   // $montrealxxxtase_banner_impression = false;
                   $sexytel_banner_impression = false;
                   
                   $procop_indexes = array('1', '2');
                    shuffle($procop_indexes);

                    if(@$vip_ads)
                    {
                        $ad_count = 1;
                        $mid = (int)floor((count($vip_ads) / 2));

                        foreach($vip_ads as $ad) {
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_vip', \Core\View::DEVICE_MODE));

                            if($ad_count === $mid) { // juicy ads
                                require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_' . array_pop($procop_indexes), \Core\View::DEVICE_MODE));
                            }

                            if($ad_count === 1 && !$laboite_banner_impression) {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_laboite', \Core\View::DEVICE_MODE));
                               $laboite_banner_impression = true;
                            }
                            
                            if($ad_count === 4) { // xxxception for montreal, else forum
                               if(TRUE === \Modules\Humpchies\Utils::sessionIsInMontrealMetroArea() && !$xxxception_banner_impression) {
                                 require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_xxxception', \Core\View::DEVICE_MODE));
                                 $xxxception_banner_impression = true;
                               
                               } else {
                                 require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_sexytel', \Core\View::DEVICE_MODE));
                                 $sexytel_banner_impression = true; 
                               }
                            }
                            
                            if($ad_count === 7 && !$sexytel_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_sexytel', \Core\View::DEVICE_MODE));
                               $sexytel_banner_impression = true;
                            }
                            
                            
                            
                            if($ad_count === 9)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_eros', \Core\View::DEVICE_MODE));
                                $eros_banner_impression = true;
                            }
                            
                             if($ad_count === 12)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_edir', \Core\View::DEVICE_MODE));
                                $edir_banner_impression = true;
                            }
                            
                            // if($ad_count === 15)
                            //{
                            //   require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_avalon', \Core\View::DEVICE_MODE));
                            ///   $avalon_banner_impression = true;
                            //}
                            
                            if($ad_count === 15 && !$escortbook_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_escortbook', \Core\View::DEVICE_MODE));
                               $escortbook_banner_impression = true;
                            }
                            //if($ad_count === 17 && !$montrealxxxtase_banner_impression)
                            //{
                            //   require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_montrealxxxtase', \Core\View::DEVICE_MODE));
                            //   $montrealxxxtase_banner_impression = true;
                            //}
                            
                            if($ad_count === 22 && !$forum_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_forum', \Core\View::DEVICE_MODE));
                               $forum_banner_impression = true;
                            }
                            
                            
                            $ad_count ++;
                        }
                    }

                    $procop_indexes = array('1', '2');
                    shuffle($procop_indexes);
                    if(@$premium_ads)
                    {

                        $ad_count = 1;
                        $mid = (int)floor((count($premium_ads) / 2));

                        foreach($premium_ads as $ad)
                        {
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_premium', \Core\View::DEVICE_MODE));

                            if($ad_count === $mid)
                            {
                                require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_' . array_pop($procop_indexes), \Core\View::DEVICE_MODE));
                            }
                            
                            if($ad_count === 1 && !$laboite_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_laboite', \Core\View::DEVICE_MODE));
                               $laboite_banner_impression = true;
                            }
                           
                            if($ad_count === 4) {
                                if(TRUE === \Modules\Humpchies\Utils::sessionIsInMontrealMetroArea() && !$xxxception_banner_impression) {
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_xxxception', \Core\View::DEVICE_MODE));
                                    $xxxception_banner_impression = true;
                                } elseif(!$sexytel_banner_impression) {
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_sexytel', \Core\View::DEVICE_MODE));
                                    $sexytel_banner_impression = true; 
                                }
                            }
                            
                            if($ad_count === 7 && !$sexytel_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_sexytel', \Core\View::DEVICE_MODE));
                               $sexytel_banner_impression = true;
                            }
                            
                            
                            
                            if($ad_count === 9 && !$eros_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_eros', \Core\View::DEVICE_MODE));
                               $eros_banner_impression = true;
                            }
                            
                            if($ad_count === 12 && !$edir_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_edir', \Core\View::DEVICE_MODE));
                               $edir_banner_impression = true;
                            }
                            
                            // if($ad_count === 15 && !$avalon_banner_impression)
                            //{
                            //   require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_avalon', \Core\View::DEVICE_MODE));
                            //   $avalon_banner_impression = true;
                            //}
                            
                             if($ad_count === 15 && !$escortbook_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_escortbook', \Core\View::DEVICE_MODE));
                               $escortbook_banner_impression = true;
                            }
                            
                            //if($ad_count === 17 && !$montrealxxxtase_banner_impression)
                            //{
                            //   require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_montrealxxxtase', \Core\View::DEVICE_MODE));
                            //   $montrealxxxtase_banner_impression = true;
                            //}

                            if($ad_count === 22 && !$forum_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_forum', \Core\View::DEVICE_MODE));
                               $forum_banner_impression = true;
                            }
                            
                            
                            $ad_count ++;
                        }
                    }

                    $procop_indexes = array('1', '2');
                    shuffle($procop_indexes);
                    if(@$ads)
                    {
                        $ad_count = 1;

                        foreach($ads['recordset'] as $ad)
                        {
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long'));

                            switch($ad_count)
                            {
                                
                                 case 1:   // only Montreal metro area
                                    if(!$laboite_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_laboite', \Core\View::DEVICE_MODE));
                                    }
                                    break;
                                    
                               
                                case 3:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_' . array_pop($procop_indexes), \Core\View::DEVICE_MODE));
                                    break;
                               
                               
                                 case 4:
                                    if(TRUE === \Modules\Humpchies\Utils::sessionIsInMontrealMetroArea() && !$xxxception_banner_impression) {
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_xxxception', \Core\View::DEVICE_MODE));
                                    } elseif(!$sexytel_banner_impression) {
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_sexytel', \Core\View::DEVICE_MODE));
                                        $sexytel_banner_impression = true;
                                    }
                                    break;  
                               
                               
                               
                                 case 7:
                                    if(!$sexytel_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_sexytel', \Core\View::DEVICE_MODE));
                                    }
                                    break;
                               
                                 
                               
                                case 6:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_' . array_shift($procop_indexes), \Core\View::DEVICE_MODE));
                                    break;
                                 case 9:
                                    if(!$eros_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_eros', \Core\View::DEVICE_MODE));    
                                    }                                    
                                    break;
                                 //case 9:
                                 //   require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_3', \Core\View::DEVICE_MODE));
                                 //   break;
                                case 12:
                                    if(!$edir_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_edir', \Core\View::DEVICE_MODE));    
                                    }                                    
                                    break;
                                
                                case 15:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_4', \Core\View::DEVICE_MODE));
                                    break;
                                case 18:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_5', \Core\View::DEVICE_MODE));
                                    break;
                                 //case 17:
                                 //   require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_montrealxxxtase', \Core\View::DEVICE_MODE));
                                 //   break;
                                
                                case 21:
                                     if(!$escortbook_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_escortbook', \Core\View::DEVICE_MODE));
                                     }
                                    break;
                                
                                case 23:
                                    if(!$forum_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_forum', \Core\View::DEVICE_MODE));
                                    }
                                    break;
                                
                                default:
                                    break;
                            }

                            $ad_count ++;
                        }
                    }?>
        
        <?php if(intval($ads['count']) == 0):?>
            <p style="margin-top:20px;">[ERROR-NO-ADS]</p>
        <?php endif;?>
        </div>
        <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::LOCALIZED_DEVICE_MODE);?>
        
        <?php if (isset($custom_description) && $custom_description != ''):?>
            <div class="custom_description" style="margin:20px 15px 0 15px;">
                <?php echo $custom_description;?>
            </div>
        <?php endif;?>
    </div>
</div>