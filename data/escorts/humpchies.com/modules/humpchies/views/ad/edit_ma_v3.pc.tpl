<style>
    .rotate {
        background: #f4f4f4;
        padding: 5px;
        border-bottom-left-radius: 4px;
        border-top-right-radius: 4px;
        width: 44px;
        margin-left: auto;
        margin-bottom: 15px;
        position: absolute;
        top: 0px;
        right: 0px;
    }

    .rotate .cw {
        width: 14px;
        float: left;
        margin-right: 5px;
        opacity: 0.7;
    }

    .rotate .cw:hover {
        opacity: 1;
    }

    .rotate .ccw {
        width: 14px;
        -webkit-transform: scaleX(-1);
        transform: scaleX(-1);
        opacity: 0.7;
    }

    .rotate .ccw:hover {
         opacity: 1;
    }


</style>

<?php
    $city_list = \Modules\Humpchies\CityModel::getCitiesRegions();
?>
<style>
    .video-preview.selected:before {
        content: "[LABEL-VIDEO-SELECTED]";
    }
</style>
<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i><a/></li>
        <li> <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>">[LABEL-MY-ADS]</a></li>
        <li><?=\Core\Controller::getTitle()?> #<?=@$_GET['id']?></li>
    </ul>    
</nav>

<div class="create-ad">
    <div class="content_box">
        <div class="content_box_header">
            <h2 class="title"><?=\Core\Controller::getTitle()?> #<?=@$_GET['id']?></h2>
        </div>
        <div class="content_box_body">
            <div class="form-wrapper">
                <form id="post_pad" method="post" enctype="multipart/form-data" class="mb-20">
                    <div class="row mb-20">
                        <div class="col-12 bg-gray mb-20">
                            <div class="col-4 pull-left pr-30">
                                <div class="field mb-20">

                                    <div class="field mb-20">
                                        <label>[LABEL-CITY]<span class="required">*</span></label>
                                        <div class=" mt-5">
                                            <label class="custom-select">
                                                <select name="city" id="city" data-must-be="<?php echo $_POST['city'];?>">
                                                     <?php foreach($city_list as $key => $list):?>
                                                        <optgroup label="<?php echo $list['region']['name'];?>">
                                                            
                                                            <?php foreach($list['cities'] as $key => $city):?>
                                                                <?php if((isset($_POST['city_id']) && $city['id'] == $_POST['city_id'])){
                                                                    $selected_city = $city['name'];
                                                                }?>
                                                                <option value="<?php echo $city['slug'];?>" <?php echo (isset($_POST['city_id']) && $city['id'] == $_POST['city_id'])? 'selected' : '';?>><?php echo $city['name'];?></option>
                                                            <?php endforeach;?>
                                                        </optgroup>    
                                                     <?php endforeach;?>
                                                </select>
                                            </label>    
                                        </div>
                                    </div>
                                </div>

                                <div class="field mb-20">
                                    <label for="phone" class="phone">[LABEL-TELEPHONE]<span class="required">*</span></label>
                                    <div class="mt-5">
                                        <input class="text" id="phone" name="phone" type="text" maxlength="<?=$max_len_phone?>" value="<?=(isset($_POST["phone"]) ? preg_replace('/\d{3}/', '$0-', str_replace('-', null, $_POST["phone"]), 2) : (!empty($_SESSION['user']['phone']) ? preg_replace('/\d{3}/', '$0-', str_replace('-', null, $_SESSION['user']['phone']), 2) : ''))?>" />
                                        <input id="phone_country_iso" name="phone_country_iso" type="hidden" value="<?php echo (isset($_POST['phone_country_iso']) && $_POST['phone_country_iso'] != '')? $_POST['phone_country_iso'] : 'ca'?>">
                                        <input id="phone_country_code" name="phone_country_code" type="hidden">
                                    </div>
                                </div>

                                <div class="field mb-20">
                                    <div class="mt-5">
                                        <label>[LABEL-CATEGORY]<span class="required">*</span></label>
                                        <div class="mt-5">
                                            <label class="custom-select">
                                                <?php \Core\Controller::renderPlugin('category_options', NULL, \Core\View::MODELESS);?>
                                            </label>    
                                        </div>
                                    </div>
                                </div>
                                <div class="field mb-20">
                                    <label for="price">[LABEL-PRICE]</label>
                                    <div class="mt-5">
                                        <input class="text" name="price" id="price" type="text" placeholder=" " maxlength="<?=$max_len_price?>" value="<?=@$_POST['price']?>" />
                                    </div>
                                    <div class="custom-control-container mt-5">
                                        <label class="custom-control custom-checkbox" for="addprice">
                                            <input type="checkbox" name="addprice" id='addprice' class="custom-control-input" value="yes" <?= @$_POST["addprice"] == 'yes' ? 'checked':'';?> />
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-label">[LABEL-ADD-PRICE]</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="field mb-20">
                                    <label for="price_conditions">[LABEL-PRICE-CONDITIONS]</label>
                                    <div class="mt-5">
                                        <input class="text" name="price_conditions" id="price_conditions" placeholder=" " type="text" maxlength="<?=$max_len_price_conditions?>" value="<?=@$_POST['price_conditions']?>" />
                                    </div>
                                </div>
                                <div class="field">
                                    <label>[LABEL-LOCATION]</label>
                                    <div class="mt-5">
                                        <label class="custom-select">
                                            <?php \Core\Controller::renderPlugin('location_options', NULL, \Core\View::MODELESS);?>
                                        </label>    
                                    </div> 
                                </div>
                            </div>
                            <div class="col-8 pull-left">
                                <div class="field mb-20">
                                    <label for="title">[LABEL-TITLE]<span class="required">*</span></label>
                                    <div class="mt-5">
                                        <input class="text" id="title" name="title" type="text" placeholder=" " maxlength="<?=$max_len_title?>" value="<?=@$_POST['title']?>" />
                                    </div>
                                    <div class="mt-5 form-attention">[LABEL-PRICE-TITLE]</div>
                                </div>
                                <div class="field">
                                    <label for="description">[LABEL-DESCRIPTION]<span class="required">*</span></label>
                                    <div class="mt-5">
                                        <textarea placeholder=" " name="description" id="description" style="height: 400px;"><?=@$_POST["description"]?></textarea>
                                    </div>
                                </div>
                                <input type="hidden" id="myfans_url" name="myfans_url" />
                                <?php if(0):?>  
                                    <div class="field ">
                                        <label for="myfans_url" class="myfans_url">[LABEL-4MYFANS]:</label>
                                        <div class="mt-5" style="width:60%">
                                            <input class="text" id="myfans_url" name="myfans_url" type="text" placeholder=" " value="<?=@$_POST['myfans_url']?>" />
                                            <div class="mt-5" style="border: 1px solid #333; line-height: 14px;color: #fff;padding: 10px;">[NEW-4MYFANS]</div>
                                        </div> 
                                    </div>
                                <?php endif;?>
                            </div>
                            
                        </div>
                        
                        <?php if(!empty($tags)):?>
                            <div class="tags-container  mb-20">
                              <h2 class="stylized-title"><span>[LABEL-SERVICES]</span></h2>
                              <ul class="ks-cboxtags">
                                <?php $selected =  array_column((array)$selected_tags, 'tag_id');?>
                                <?php foreach($tags as $tag):?>
                                    <li>
                                        <input name='tags[]' type="checkbox" id="tag<?php echo $tag['id'];?>" value="<?php echo $tag['id'];?>" <?php echo (in_array($tag['id'], $selected))? 'checked = "checked"' : '';?>/>
                                        <label for="tag<?php echo $tag['id'];?>"><?php echo $tag['name'];?></label>
                                    </li>
                                <?php endforeach;?>
                                
                              </ul>

                            </div>
                        <?php endif;?>
                        
                        <div class="location-container">
                            <h2 class="stylized-title"><span>[LOCATION-TITLE]</span></h2> 
                            <div class="map_warning">          
                                <span class="title">[LOCATION-WARNING-1]&nbsp;</span>[LOCATION-WARNING-2]
                                <br/>
                                [LOCATION-WARNING-3]
                            </div>
                            
                            <input type="hidden" name="pic_changed" id="pic_changed" value="0" />
                            <input type="hidden" name="hide_map_check" id="hide_map_check" value="<?php echo ($map_coords && $map_coords['hide_map'] == 1)? 'yes' : 'no'?>" />
                            <input type="hidden" name="hide_map" id="hide_map" value="<?php echo ($map_coords && $map_coords['hide_map'] == 1)? 'yes' : 'no'?>" />
                            
                            <input type="hidden" name="maps_lat" id="maps_lat" value="<?php echo ($map_coords && $map_coords['lat'] != 0)? $map_coords['lat'] : '';?>" />
                            <input type="hidden" name="maps_long" id="maps_long" value="<?php echo ($map_coords && $map_coords['long'] != 0)? $map_coords['long'] : '';?>" />
                            <input type="hidden" name="maps_city" id="maps_city" value="<?php echo ($map_coords && $map_coords['ad_city_name'] != '')? $map_coords['ad_city_name'] : $selected_city;?>" />
                            
                            <div id="map" class="map-container" <?php echo ($map_coords && $map_coords['lat'] != 0)? 'data-lat="'.$map_coords['lat'].'" data-long="'.$map_coords['long'].'"' : 'data-city="' . $selected_city . '" data-country="Canada"' ;?>>
                                <!-- add cta for showing map -->
                                <a href="javascript:void(0)" id="show_map">[LOCATION-SHOW-BTN]</a>
                            </div>
                            <div class="location-actions">
                                <a href="javascript:void(0)" id="remove_map" style="display:<?php echo ($map_coords && $map_coords['hide_map'] == 1)? 'none' : 'flex'?>">[LOCATION-REMOVE-MAP] <i class="fa fa-trash"></i> </a>
                                <span class="notice add-success" style="display:none;">[LOCATION-NOTICE-ADD]</span>
                                <span class="notice remove-success" style="display:none;">[LOCATION-NOTICE-REMOVE]</span>
                            </div> 
                        
                        </div>
                        
                    </div>
                    <input type="hidden" name="attached_video" id="attached-video" value="<?php echo $selected_video?>">
                    <input type="hidden" name="rotator[0]" id="rotator-0" value="0">
                    <input type="hidden" name="rotator[1]" id="rotator-1" value="0">
                    <input type="hidden" name="rotator[2]" id="rotator-2" value="0">
                    <input type="hidden" name="rotator[3]" id="rotator-3" value="0">
                    <input type="hidden" name="rotator[4]" id="rotator-4" value="0"> 
                    <input type="hidden" name="imageids[0]" id="imageids-0" value="0">
                    <input type="hidden" name="imageids[1]" id="imageids-1" value="0">
                    <input type="hidden" name="imageids[2]" id="imageids-2" value="0">
                    <input type="hidden" name="imageids[3]" id="imageids-3" value="0">
                    <input type="hidden" name="imageids[4]" id="imageids-4" value="0">           
                </form>
                
                <div id="photos">
                    <h2 class="stylized-title"><span>[LABEL-PICTURES](max. 5)</span></h2> 
                    <h3><span class="title">[LABEL-TIPS]!&nbsp;</span> [LABEL-TITLE-VERIFY-AD]</h3>
                    <div class="photos_container">
                        <?php if($image_count == 0):?>
                            <div id="preview-default"></div>
                        <?php endif;?>
                                                 
                        <div class="action-buttons clearfix mb-20" style="<?php echo ($image_count == 0)? 'display: none;' : '';?>">
                            <button class="set-main" style="cursor: pointer; z-index:1000">[BTN-SET-MAIN]</button>
                            <button class="delete"  style="cursor: pointer; z-index:1000" >[BTN-DELETE]</button>
                        </div>
                        
                        <ul class="images mb-20 clearfix">
                            <?php $key = -1; foreach($images as $i => $image) {
                                $key++;
                            ?>
                            <li class="image <?=$image['is_main'] ? ' main':''?>">
                                <div class="wrapper">
                                    <div class="image-box" style="background-image:url('<?= $image['href'] ?>');" zzkey="<?php echo $key;?>" zid ="<?php echo $i; ?>" ></div>
                                    <div class="action">
                                        <span class="move"></span>
                                        <span class="main">
                                            <label class="custom-control custom-checkbox" for="p-<?=$i?>">
                                                <input type="checkbox" class="custom-control-input" id="p-<?=$i?>" name="photo_id" value="<?=$image['id']?>">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description"></span>
                                            </label>
                                        </span>

                                    </div>
                                    <span class="rotate">
                                            <img src="https://gui.humpchies.com/images/cw.svg" class="cw" />
                                            <img src="https://gui.humpchies.com/images/cw.svg" class="ccw" />
                                    </span>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                        <input type="hidden" id="image_count" name="image_count" value="<?php echo $image_count;?>" /> 
                        
                        <form id="img-upload" action="/ad/photos?a=upload" enctype="multipart/form-data"  method="post" class="mb-10">
                            <input type="hidden" name="ad_id" id="ad_id" value="<?= $ad_id ?>">
                            <input type="file" name="images[]" class="file-input" accept="image/jpeg;capture=camera" id="images"  style=" opacity: 0;  display: none;">
                            <button type="submit" class="button" style="opacity:0; display:none;">[BTN-UPLOAD]</button>
                            <div class="field">
                                <div class="legend">
                                    <button type="button" class="btn-select_pics <?php echo $image_count == 5? 'disabled' : '';?>">[LABEL-SELECT-PICS-UPLOAD]</button>
                                </div>
                        </div>
                        </form>
                        <p id ="error-msg" class="error-msg"></p>
                    </div>
                </div>
                
                <?php  
                    $user_id = \Core\Utils\Session::getCurrentUserID();
                    if($user_id == 63661  ){
                ?>
                <div class="row video-section mb-20">
                    <h2 class="stylized-title"><span>[LABEL-VIDEO-GALLERY]</span></h2> 
                    <h3>
                        <span class="title">[LABEL-TIPS]!&nbsp;</span> [LABEL-VIDEO-HELP]
                        <br/>
                        [LABEL-VIDEO-NOTE]
                    </h3> 
                     
                    <div class="field">
                        <div class="videos-container">
                            <div class="gallery_videos p-30" id="gallery_videos">
                               <?php
                              
                               foreach($user_videos as $video){ 
                                $adresa =  $video['user_id']."/". $video['video_name'];
                                $class = ($selected_video == $video['video_name'] )? " selected ":" ";
                               ?>
                               
                               <div class="video-preview <?php echo $class; ?>" rel-movie="<?php echo $videolocation;?><?php echo $adresa; ?>" rel-id="<?php echo $video["id"]; ?>" style="background-image: url(<?php echo $videolocation;?><?php echo $adresa; ?>.jpg);"><div class="video-remove"></div><div class="video-select">[LABEL-SELECT-ACCOUNT-TYPE]</div></div>
                               <?php }?>
                            </div>
                        </div>
                        <div class="progress-bar"></div>
                       <input id="video-upload" type="file" accept=".mov,video/mp4" name="videoupload" style="width: 0; height: 0">
                        <div class="legend">
                            <button type="button" class="btn-select_video">Upload Video</button>
                            <a href="javascript:remove_selection()" style="margin-left: 40px;"> [LABEL-VIDEO-UNSELECT]</a>
                        </div>
                        
                    </div>
                </div>
                <?php 
                }
                ?>   
                <div class="row bottom-container">
                    <div class="field">
                         <label class="label-free_ad">&nbsp;<a href="javascript:void(0)" id="forum-submit" class="free-ad">[LABEL-SAVE-CHANGES]</a></label>
                    </div>
               </div>
            </div>
        </div>
    </div>
</div><div id="play-video-modal" class="play-video-modal modal-pm-gray">
    <!-- Modal content -->
    <div class="modal-pm-content">
        
        <div class="modal-pm-header">
            <div class="fa fa-close video-modal-close"></div>
            <h2>Preview Video</h2>
        </div>
        <div class="modal-pm-body">
           <video width="800" height="400" controls controlsList="nodownload" id='movie-load'>
             
              Your browser does not support the video tag.
           </video> 
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left">
                <input type="button" class='btn-orange modal-close video-modal-close' value="[LABEL-CLOSE]"/>
            </div>
        </div>
    </div>
</div>
