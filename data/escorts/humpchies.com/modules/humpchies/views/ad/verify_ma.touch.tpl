 <style >
 .spangold{
    color:#ff9900;
}
</style>
 <nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i><a/></li>
        <li> <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>">[LABEL-MY-ADS]<a/></li>
        <li>[AD-VERIFY-TITLE]</li>
    </ul>    
</nav>

<div class="create-ad">
    <form id='verification-form' method="post">
    <div class="content_box"  style="font-size: 14px; padding:20px">
        <h1 class="h1title"> [AD-VERIFY-TITLE] #<?php echo $adid; ?> <?=$title ?></h1>    
        <input type='hidden' name='adid' id='adid' value="<?php echo $adid; ?>" />
        <input type='hidden' name='uploadedFiles' id='uploaded-files' value="" />
       
         <div class="content_box_header " style="background-color:#212121; padding:30px 20px; text-align: left;border-left: 10px solid #ff9900;border-top-left-radius: 7px;border-bottom-left-radius: 7px; display:block">
            <img src="https://gui.humpchies.com/images/info.png" style="margin: 0px auto;margin-bottom: 30px;"/>
           <div>[AD-VERIFY-TEXT]</div>
        </div>
        <div class ='upload-gallery-preview' style='display:block'>
            <div class ='preview-title'>
               [AD-VERIFY-VERIFICATION-PICTURES] 
            </div>
            <div class='gallery-content' id='gallery-content'>
            </div>
        </div>
        <div class ='footer-verify'>
            <input type='file' name='private[]' multiple id='private' style='width:0px;height:0px;'  accept="image/*" />
            <div class='button-orange-outline button upload-private' > [AD-VERIFY-UPLOAD]</div>
            <a class="button-orange button continue" href='#' id="submitVerification" style="position:fixed; bottom:0px; left:0px; right:0px; border-radius: 0px; margin:0px">[BTN-CONTINUE] </a>
        </div>
         <div class ='skip-verify-files' style="margin:0px;"> <a href="<?php echo \Core\Utils\Navigation::urlFor('ad', 'my-listings'); ?>">[AD-VERIFY-SKIP]</a> </div>
    </div>
     <div class ='upload-gallery-preview' style='display:block; margin:20px; margin-bottom:60px'>
            <div class ='preview-title'>
                [AD-VERIFY-GALLERY] 
            </div>
            <div class='gallery-content'>
            <?php foreach($images as $img) {?>
                <div class="gallery_item" data-index="0" style="background-image:url('<?=$img['href']; ?>')">
                </div>
            <?php }?>
            </div>
        </div>          
    </form> 
</div>            
