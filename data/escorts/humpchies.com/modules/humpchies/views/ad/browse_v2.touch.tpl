<?php $display_location == '';?>
<?php if(isset($breadcrumb)):?>
<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <?php switch(true) {
        
            case ( isset($breadcrumb['city']) && isset($breadcrumb['category_url'])):
                $display_location  = $city_name;
        ?>
            <li> <a href="<?php echo $breadcrumb['category_url'];?>"><?php echo $breadcrumb['category_title'];?></a></li>
            <li><?php echo $breadcrumb['city'];?></li>
        <?php                                                         
            break;    
            case ( isset($breadcrumb['region']) && isset($breadcrumb['category_url'])):
                $display_location  = $breadcrumb['region'];
        ?>
            <li> <a href="<?php echo $breadcrumb['category_url'];?>"><?php echo $breadcrumb['category_title'];?></a></li>
            <li><?php echo $breadcrumb['region'];?></li>
        <?php
            break;
            case ( !isset($breadcrumb['city']) && isset($breadcrumb['category_url'])):
        ?>
            <li> <?php echo $breadcrumb['category_title'];?></li>
         <?php                                                         
            break;    
            case ( isset($breadcrumb['city']) && !isset($breadcrumb['category_url'])):
                $display_location  = $city_name;
        ?>
            <li><?php echo $breadcrumb['city'];?></li>
        <?php                                                         
            break;
            case ( isset($breadcrumb['search'])): 
        ?>
            <li>'<?php echo $breadcrumb['search'];?>'</li>
        <?php                                                         
            break;
            case ( isset($breadcrumb['region'])): 
                $display_location = $breadcrumb['region']; 
        ?>
            <li><?php echo $breadcrumb['region'];?></li>
        <?php                                                         
            break; 
        } ?>  
        
    </ul>    
 </nav>
 <?php endif;?>
  

<div class="wrapper">
    <div class="content_box">
        <div class="content_box_header">
            <h2 class="title"><?=\Core\Controller::getTitle()?></h2>
            
            <?php if(!isset($hide_filters)):?> 
            
            <div class="category-filter" id="category-filter">
                <?php 
                    $category = '';
                    switch($_GET['category']) {
                    
                        case 'escort':
                            $category = '[LABEL-ESCORT]';
                            break;
                        case 'escort-agency':
                            $category = '[LABEL-ESCORT-AGENCY]';
                            break;
                        case 'erotic-massage':
                            $category = '[LABEL-EROTIC-MASSAGE]';
                            break;
                        case 'erotic-massage-parlor':
                            $category = '[LABEL-EROTIC-MASSAGE-PARLOR]';
                            break;    
                        default:
                            $category = '[LABEL-SELECT-CATEGORY]';    
                            break;
                }?>
                <div class="category-filter-box">
                    <h3><?php echo $category;?></h3>
                    <i class="fa fa-angle-down"></i>
                </div>
                <div class="list" id="categories-list">
                    <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'listing')?>" title="">[LABEL-ALL-CATEGORIES]</a>
                        <a title="[HREF-TITLE-ESCORT-PART-1] <?=(@$city_name === NULL ? '[LABEL-MONTREAL]' : $city_name)?>" href="<?=\Core\Utils\Navigation::urlFor('ad', 'listing-v2', array('category' => 'escort'))?>" <?php echo (@$_GET['category'] === 'escort') ? 'class="active"' : '';?>>
                        [LABEL-ESCORT]
                    </a>
                         <a title="[HREF-TITLE-ESCORT-AGENCY-PART-1] <?=(@$city_name === NULL ? '[LABEL-MONTREAL]' : $city_name)?>" href="<?=\Core\Utils\Navigation::urlFor('ad', 'listing-v2', array('category' => 'escort-agency'))?>" <?php echo (@$_GET['category'] === 'escort-agency') ? 'class="active"' : '';?>>
                        [LABEL-ESCORT-AGENCY]
                    </a>
                        <a title="[HREF-TITLE-EROTIC-MASSAGE-PART-1] <?=(@$city_name === NULL ? '[LABEL-MONTREAL]' : $city_name)?>" href="<?=\Core\Utils\Navigation::urlFor('ad', 'listing-v2', array('category' => 'erotic-massage'))?>" <?php echo (@$_GET['category'] === 'erotic-massage') ? 'class="active"' : '';?>>
                        [LABEL-EROTIC-MASSAGE]
                    </a>
                        <a title="[HREF-TITLE-EROTIC-MASSAGE-PARLOR-PART-1] <?=(@$city_name === NULL ? '[LABEL-MONTREAL]' : $city_name)?>" href="<?=\Core\Utils\Navigation::urlFor('ad', 'listing-v2', array('category' => 'erotic-massage-parlor'))?>" <?php echo (@$_GET['category'] === 'erotic-massage-parlor') ? 'class="active"' : '';?>>
                        [LABEL-EROTIC-MASSAGE-PARLOR]
                    </a>        
                </div>
            </div>
            
            <?php require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::browse_filters_v2', \Core\View::DEVICE_MODE)); ?>
            <?php endif;?>    
        
            
        </div>    
        <div class="content_box_body">
            <?php
                   //require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_forum', \Core\View::DEVICE_MODE));
                   
                    //http://jira.sceon.am/browse/HUMP-173  eros banner must be always 9 position no matter vip prem or regular listing
                   $eros_banner_impression = false;
                   $edir_banner_impression = false;
                   $avalon_banner_impression = false;
                   $forum_banner_impression = false;
                   $laboite_banner_impression = true;         // hide
                   $xxxception_banner_impression = true;      // hide
                   $escortbook_banner_impression = false;
                   // $montrealxxxtase_banner_impression = false;
                   $sexytel_banner_impression = false;
                   
                   $procop_indexes = array('1', '2');
                    shuffle($procop_indexes);

                    if(@$vip_ads)
                    {
                        $ad_count = 1;
                        $mid = (int)floor((count($vip_ads) / 2));

                        foreach($vip_ads as $ad) {
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_v2', \Core\View::DEVICE_MODE));

                            if($ad_count === $mid) { // juicy ads
                                require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_' . array_pop($procop_indexes), \Core\View::DEVICE_MODE));
                            }

                            if($ad_count === 1 && !$laboite_banner_impression) {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_laboite', \Core\View::DEVICE_MODE));
                               $laboite_banner_impression = true;
                            }
                            
                            if($ad_count === 4) { // xxxception for montreal, else forum
                               if(TRUE === \Modules\Humpchies\Utils::sessionIsInMontrealMetroArea() && !$xxxception_banner_impression) {
                                 require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_xxxception', \Core\View::DEVICE_MODE));
                                 $xxxception_banner_impression = true;
                               
                               } else {
                                 require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_sexytel', \Core\View::DEVICE_MODE));
                                 $sexytel_banner_impression = true; 
                               }
                            }
                            
                            if($ad_count === 7 && !$sexytel_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_sexytel', \Core\View::DEVICE_MODE));
                               $sexytel_banner_impression = true;
                            }
                            
                            
                            
                            if($ad_count === 9)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_eros', \Core\View::DEVICE_MODE));
                                $eros_banner_impression = true;
                            }
                            
                             if($ad_count === 12)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_edir', \Core\View::DEVICE_MODE));
                                $edir_banner_impression = true;
                            }
                            
                            // if($ad_count === 15)
                            //{
                            //   require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_avalon', \Core\View::DEVICE_MODE));
                            ///   $avalon_banner_impression = true;
                            //}
                            
                            if($ad_count === 15 && !$escortbook_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_escortbook', \Core\View::DEVICE_MODE));
                               $escortbook_banner_impression = true;
                            }
                            //if($ad_count === 17 && !$montrealxxxtase_banner_impression)
                            //{
                            //   require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_montrealxxxtase', \Core\View::DEVICE_MODE));
                            //   $montrealxxxtase_banner_impression = true;
                            //}
                            
                            if($ad_count === 22 && !$forum_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_forum', \Core\View::DEVICE_MODE));
                               $forum_banner_impression = true;
                            }
                            
                            
                            $ad_count ++;
                        }
                    }

                    $procop_indexes = array('1', '2');
                    shuffle($procop_indexes);
                    if(@$premium_ads)
                    {

                        $ad_count = 1;
                        $mid = (int)floor((count($premium_ads) / 2));

                        foreach($premium_ads as $ad)
                        {
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_v2', \Core\View::DEVICE_MODE));

                            if($ad_count === $mid)
                            {
                                require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_' . array_pop($procop_indexes), \Core\View::DEVICE_MODE));
                            }
                            
                            if($ad_count === 1 && !$laboite_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_laboite', \Core\View::DEVICE_MODE));
                               $laboite_banner_impression = true;
                            }
                           
                            if($ad_count === 4) {
                                if(TRUE === \Modules\Humpchies\Utils::sessionIsInMontrealMetroArea() && !$xxxception_banner_impression) {
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_xxxception', \Core\View::DEVICE_MODE));
                                    $xxxception_banner_impression = true;
                                } elseif(!$sexytel_banner_impression) {
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_sexytel', \Core\View::DEVICE_MODE));
                                    $sexytel_banner_impression = true; 
                                }
                            }
                            
                            if($ad_count === 7 && !$sexytel_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_sexytel', \Core\View::DEVICE_MODE));
                               $sexytel_banner_impression = true;
                            }
                            
                            
                            
                            if($ad_count === 9 && !$eros_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_eros', \Core\View::DEVICE_MODE));
                               $eros_banner_impression = true;
                            }
                            
                            if($ad_count === 12 && !$edir_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_edir', \Core\View::DEVICE_MODE));
                               $edir_banner_impression = true;
                            }
                            
                            // if($ad_count === 15 && !$avalon_banner_impression)
                            //{
                            //   require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_avalon', \Core\View::DEVICE_MODE));
                            //   $avalon_banner_impression = true;
                            //}
                            
                             if($ad_count === 15 && !$escortbook_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_escortbook', \Core\View::DEVICE_MODE));
                               $escortbook_banner_impression = true;
                            }
                            
                            //if($ad_count === 17 && !$montrealxxxtase_banner_impression)
                            //{
                            //   require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_montrealxxxtase', \Core\View::DEVICE_MODE));
                            //   $montrealxxxtase_banner_impression = true;
                            //}

                            if($ad_count === 22 && !$forum_banner_impression)
                            {
                               require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_forum', \Core\View::DEVICE_MODE));
                               $forum_banner_impression = true;
                            }
                            
                            
                            $ad_count ++;
                        }
                    }

                    $procop_indexes = array('1', '2');
                    shuffle($procop_indexes);
                    if(@$ads)
                    {
                        $ad_count = 1;

                        foreach($ads['recordset'] as $ad)
                        {
                            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_v2'));

                            switch($ad_count)
                            {
                                
                                 case 1:   // only Montreal metro area
                                    if(!$laboite_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_laboite', \Core\View::DEVICE_MODE));
                                    }
                                    break;
                                    
                               
                                 case 3:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_' . array_pop($procop_indexes), \Core\View::DEVICE_MODE));
                                    break;
                               
                               
                                 case 4:
                                    if(TRUE === \Modules\Humpchies\Utils::sessionIsInMontrealMetroArea() && !$xxxception_banner_impression) {
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_xxxception', \Core\View::DEVICE_MODE));
                                    } elseif(!$sexytel_banner_impression) {
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_sexytel', \Core\View::DEVICE_MODE));
                                        $sexytel_banner_impression = true;
                                    }
                                    break;  
                               
                               
                               
                                 case 7:
                                    if(!$sexytel_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_sexytel', \Core\View::DEVICE_MODE));
                                    }
                                    break;
                               
                                 
                               
                                 case 6:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_' . array_shift($procop_indexes), \Core\View::DEVICE_MODE));
                                    break;
                                 case 9:
                                    if(!$eros_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_eros', \Core\View::DEVICE_MODE));    
                                    }                                    
                                    break;
                                 //case 9:
                                 //   require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_3', \Core\View::DEVICE_MODE));
                                 //   break;
                                 case 12:
                                    if(!$edir_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_edir', \Core\View::DEVICE_MODE));    
                                    }                                    
                                    break;
                                
                                 case 15:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_4', \Core\View::DEVICE_MODE));
                                    break;
                                 case 18:
                                    require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_5', \Core\View::DEVICE_MODE));
                                    break;
                                 //case 17:
                                 //   require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_montrealxxxtase', \Core\View::DEVICE_MODE));
                                 //   break;
                                
                                case 21:
                                     if(!$escortbook_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_escortbook', \Core\View::DEVICE_MODE));
                                     }
                                    break;
                                
                                case 23:
                                    if(!$forum_banner_impression){
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::347x121_forum', \Core\View::DEVICE_MODE));
                                    }
                                    break;
                                
                                default:
                                    break;
                            }

                            $ad_count ++;
                        }
                    }?>
        
        <?php if(intval($ads['count']) == 0):?>
            
            <?php 
                $no_filters = 0;
                $no_filters += ($location_filter != '')? 1 : 0; // availability
                $no_filters += ($has_comments_filter == 1)? 1 : 0; // has comments
                $no_filters += ($accepts_comments_filter == 1)? 1 : 0; // accepts_ comments
                $no_filters += ($real_photos_filter == 1)? 1 : 0; // real photos
                $no_filters += ($display_location != '')? 1 : 0; // city/region
                $no_filters += ($services_filter != '')? count(explode(',', $services_filter)) : 0; // city/region
                $no_filters += ($prices_filter != '')? count(explode(',', $prices_filter)) : 0; // prices
                $no_filters += ($phone1_filter != '' || $phone2_filter != '' || $phone3_filter != '')? 1 : 0; // phone
                $total_filters =  count($tags) + 5;
            
                if($no_filters > 0):
            ?>
                <p class="no-ads">[ERROR-NO-ADS-FILTERS]</p>
            <?php else:?>
                <p class="no-ads">[ERROR-NO-ADS]</p>
            <?php endif;?> 
                           
        <?php endif;?>
        </div>
        <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::LOCALIZED_DEVICE_MODE);?>
        
        <?php if (isset($custom_description) && $custom_description != ''):?>
            <div class="custom_description" style="margin:20px 15px 0 15px;">
                <?php echo $custom_description;?>
            </div>
        <?php endif;?>
    </div>
</div>