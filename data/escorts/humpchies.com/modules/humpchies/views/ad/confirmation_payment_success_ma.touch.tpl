<style>
 .checked-text{
    margin-bottom: 20px;
    line-height: 21px;
    width: 389px;
    text-align: center;
    font-family: "Roboto";
    font-size: 16px;
    padding-top: 12px;
    
 }
 .grey-separator{
     width:100%;
     height:1px;
     border-bottom:1px solid #707070;
 }
 .payments{
     display:flex;
     justify-content: center;
     margin-top: 20px;
 }
 .payment-item{
     width: 315px;
     border: 1px solid #FF9900;
     border-radius: 5px;
     text-align: left;
     padding: 21px;
     margin: 39px 15px;
     font-family: "Roboto";
 }
 .premium:before, .bump:before{
    content: "";
    background-image:url("<?=Modules\Humpchies\Utils::getCDNPath('images')?>premium-star.png");
    content: "";
    width: 59px;
    height: 58px;
    position: absolute;
    top: -38px;
    left: 125px;
    display: block;
 }
 .bump:before{
    content: "";
    background-image:url("<?=Modules\Humpchies\Utils::getCDNPath('images')?>bump-up.png");
 }
 .payment-item-title{
     color:#FF9900;
     font-size: 16px;
     text-align: center;
     padding: 7px 15px 15px;
 }
 .payment-item-options {
    line-height: 18px;
    font-size: 14px;
    font-family: "roboto";
    color: #A4A4A4;
}
a.button-submit{
    background-color: #FF9900;
    border: 0px;
    width: 100%;
    padding: 15px;
    font-size: 16px;
    border-radius: 5px;
    margin-top: 22px;
    display: block;
    color:#000;
    text-align: center;
}
a.button-submit:hover{
    background-color: #e58900;    
}
a.lame-link {
    display: inline-block;
    width: 40%;
    border: 1px solid #FF9900;
    padding: 10px;
    border-radius: 5px;
    font-size: 14px;
    font-family: "Roboto";
    margin-top: 31px;
} 

.lame a, .lame div {
    margin: 20px;
    padding: 10px;
    font-size: 14px;
}
.lame{
    
    justify-content: center;
}
 </style>
<div class="create-ad">
    <div class="content_box" >
        <div  class='grey-separator' style=""></div>
        <div class="content_box_header" style="margin: 30px auto;margin-bottom: 0px; text-align:center; display:block">
            <div style="margin: 10px;     margin-bottom: 20px;">
            <img src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>checked_white.svg" border="0" width="65" height="65" alt="" style="margin: 2px auto;"/>
            <div class="checked-text">[LABEL-PAYMENT-SUCCESS]<br>                                                                                                  
                <?= \Core\Utils\Text::translateTextWithVars("[LABEL-PAYMENT-SUCCESS-DESC-NEW]",['%days%'=> $ad[0]['period']]); ?>
            </div>
        </div>
        <div  class='grey-separator' style=""></div>    
        
        
        <div class='lame' >
           <div>[LABEL-PAYMENT-SUCCESS-OTHER]</div> <a href="<?php echo \Core\Utils\Navigation::urlFor('ad', 'verify'); ?>?id=<?php echo $adid; ?>" class="lame-link" style='width:auto'> [AD-VERIFY-TITLE]</a>  <a href="/ad/my-listings" class="lame-link" style='width:auto'> [LABEL-GO-TO-ADS]</a>
        </div>
    </div>
</div>            
