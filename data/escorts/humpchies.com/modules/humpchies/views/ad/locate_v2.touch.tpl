 <div id="activate_gps">
    <div style="display:flex; margin-bottom:40px;">
        <img src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>gps-icon.svg" alt="">
        <h1>[LOCATION-TITLE]</h1>
    </div>
    <p style="text-align: left; font-size: 16px; line-height:22px; padding: 0 0 10px 0;">[LOCATION-DESCR]</p>
    <div class="stylized-title"><span> [LOCATION-OR]</span> </div>
    <a id="show_map" href="javascript:void(0);">Select a location on the map</a>
</div>
            
 <div class="wrapper" id="maps-location" style="min-height: calc(100vh - 70px);">
    <div class="content_box" style="min-height: calc(100vh - 70px);margin-bottom: 0;">
        <div class="content_box_body" style="min-height: calc(100vh - 70px); padding: 0;">
            
            
            
            <div id="map" class="mt-5" style="height:calc(100vh - 70px); width: 100%; margin-top:0px;" data-city="Montreal" data-country="Canada"></div>
            
            <div class="field mb-20 p-15-30 bg-gray">
                <a id="show_list" class="disabled" href="javascript:void();">[LOCATION-BTN-SHOW]&nbsp;<span id="ads_count"></span></a>
            </div>
            <div id="no_results" style="margin: 20px; text-align: center;display: none;" >
                <p style="font-size:15px;">[LOCATION-NO-RESULTS-A]
                <a href="javascript:void(0);" id="show_near">[LOCATION-NO-RESULTS-B]&nbsp;<span id="city_name"></span></a></p> 
            </div>
        </div>            
    </div>
 </div>
