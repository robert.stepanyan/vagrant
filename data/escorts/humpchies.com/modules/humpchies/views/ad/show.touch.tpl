<?php
 $chatAdID = $ad['id'];
?>  
<style type="text/css">
 .buttons { display: flex;justify-content: space-between; margin: 11px 0 16px 0; font-weight: bold;}
    
    .buttons > .previous {  padding: 10px; background-color: #222; border-radius: 4px; color: #ff9900;  text-transform: uppercase; cursor: pointer; display:inline-block:}
    .buttons > .next {  padding: 10px; background-color: #222; border-radius: 4px; color: #ff9900;  text-transform: uppercase; cursor: pointer; display:inline-block:}
        
    #services {
    padding: 20px 0 5px 0px;
    border-top: 1px solid #3E3E3E;
    border-bottom: 1px solid #3E3E3E;
    margin-bottom: 20px;
}
#services h3 {
    font-size: 16px;
    color: #fff;
    font-weight: normal;
    margin: 0 0 20px 0;
}

#services .services_box {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    align-content: center;
}

#services .services_box .service {
    width: 50%;
    font-size: 15px;
    color: #AFAFAF;
    margin-bottom: 15px;
    display: flex;
}
#services .services_box .service i {
    font-size: 21px;
    color: #fff;
    margin-right: 7px;
}

.ad_details_box .ad_details_box_content p i {
    border-radius: 16px;
    color: #999;
    height: 22px;
    line-height: 21px;
    text-align: center;
    width: 22px;
    background: #333;
    margin-right: 5px;
    font-size: 17px;
    margin-bottom: 5px;
}
.ad_details_box .ad_details_box_content p .label {
    font-size: 14px;
}
#report-other{
    display: block;
}
</style>
<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i><a/></li>
        <li> <a href="<?php echo $ad['category_url'];?>"><?php echo $ad['category_name'];?></a></li>
        <li> <a href="<?php echo $ad['category_url_w_city'];?>"><?php echo $ad['city'];?></a></li>
        <li style="height: 11px;"> <?php echo $ad['orig_title'];?></li>
    </ul>    
 </nav>

<div class="wrapper_content">
 <div class='buttons'>
        <a href="<?php echo $ad['buttons']['backurl'];?>"  class="previous" <?php if ($ad['buttons']['backurl']==''){ echo 'style="visibility:hidden"';}?>>
           <i class="fa fa-chevron-left" style="margin-right: 5px;"></i> [LABEL-PREVIOUS-AD]
        </a>
        <a href="<?php echo $ad['buttons']['nexturl'];?>" class="next">
            [LABEL-NEXT-AD] <i class="fa fa-chevron-right" style="margin-left: 5px;"></i>
        </a>
    </div>
    <div class="content_box">
        <div class="content_box_header">
            <h2 class="title">
                <?php if( $ad['is_premium'] ){ ?>
                    <span class="__premium">PREMIUM</span>
                <?php } ?>
                <?=$ad['title']?>
            </h2>
            <?php if($ad['fake_status']): ?>
                <div class="fake-pics-wrapper">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                    <h6 class="fake-pics">[LABEL-AD-FAKE-PICS]</h6>
                </div>
            <?php endif; ?>
        </div>
        <div class="content_box_body" >
            <?php if($ad['status'] == 'active'):?>
            <div class="ad_details_box">
                <div class="ad_details_box_photo" style="width: fit-content">
                    <?=($ad['has_thumbs'] ? '<a style="dispaly:inline-block" href="' . $ad['images']['main']['href'] . '" data-fancybox="gallery" rel="lightbox[photo]" title="' . $ad['title'] . '">' : NULL)?>
                        <img src="<?=$ad['images']['main']['src']?>" width="260" alt="<?=$ad['title']?>" /> <?php /* width="<?=$ad['images']['main']['width']?>" height="<?=$ad['images']['main']['height']?>" */ ?>
                    <?=($ad['has_thumbs'] ? '</a>' : NULL)?>
                    
                     <?php 
                     $totalphoto = 1;
                     $totalphoto += isset($ad['images']['thumbs'][\Core\Utils\Image::ORIENTATION_LANDSCAPE])? count($ad['images']['thumbs'][\Core\Utils\Image::ORIENTATION_LANDSCAPE]) :0;
                     $totalphoto += isset($ad['images']['thumbs'][\Core\Utils\Image::ORIENTATION_PORTRAIT])?count($ad['images']['thumbs'][\Core\Utils\Image::ORIENTATION_PORTRAIT]) : 0 ;
                     $totalphoto += isset($ad['images']['thumbs'][\Core\Utils\Image::ORIENTATION_SQUARE])? count($ad['images']['thumbs'][\Core\Utils\Image::ORIENTATION_SQUARE]): 0;
                     if ($totalphoto > 1){
                    ?>
                    <div class="photo_legends" style="osition: relative;right: 0;
    color: #fff;
    z-index: 1;
    background: linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0.65) 63%,rgba(0,0,0,0.65) 100%);
    font-size: 14px;
    padding: 8px 14px;
    left: 0px;
    text-align: right;
    margin-top: -32px;
    display: flex;
    align-items: flex-end;
    justify-content: flex-end;"><i class="fa fa-camera" style='font-size: 19px;'></i> &nbsp; <?= $totalphoto ?> photos
                    </div>
                    <?php } ?>
                     <?php if( $ad['verification_status'] == "verified") {?>
                        <span class="real-photos"  style="bottom: 0px;">[LABEL-REAL-PHOTO] </span>
                     <? } ?>
                </div>
                <div align="center">
                    <?php  
                    //var_dump($ad['images']['thumbs']);
                     $totalphoto = 0;   
                     foreach(array(\Core\Utils\Image::ORIENTATION_LANDSCAPE, \Core\Utils\Image::ORIENTATION_PORTRAIT, \Core\Utils\Image::ORIENTATION_SQUARE) as $thumb_orientation) :?>
                        <?php if(isset($ad['images']['thumbs'][$thumb_orientation])): ?>
                            <?php foreach($ad['images']['thumbs'][$thumb_orientation] as $thumb) :?>
                                <?php $totalphoto++; ?>
                                <a href="<?=$thumb['href']?>" data-fancybox="gallery" rel="lightbox[photo]"
                                   title="<?=$ad['title']?>"> </a>&nbsp;&nbsp;&nbsp;
                            <?php endforeach;?>
                        <?php endif;?>
                    <?php endforeach;?>
                   
                </div>
                 <?php  
                $user_id = \Core\Utils\Session::getCurrentUserID();
                if($user_id == 63661 && $attached_video ){
            ?>
                 <div class="user_video">
                    <video class='video' width="100%"  controls="" controlslist="nodownload" id="movie-load">
                        <source src="<?=$videolocation.$attached_video?>" type="video/mp4"></video>
                    <div class="playpause"></div>
                </div>
            <?php 
                }
            ?>
                <div class="ad_details_box_content">
                        <p class="description" style="color: #AFAFAF; font-size: 15px; line-height: 20px; font-weight: normal;">
                        <?=$ad['description']?>
                    </p>
                        
                        <?php if($services !== false) :?>
                        <div id="services">
                            <h3>[LABEL-SERVICES]</h3>
                            <div class="services_box">
                                <?php foreach($services as $service):?>
                                    <span class="service"><i class="fa fa-check-circle"></i> <?php echo $service["name"];?></span>
                                <?php endforeach;?>    
                            </div>
                        </div>
                        <?php endif;?>
                        
                    <div>
                        <p>
                            <i class="fa fa-phone"></i>
                            <span class="label">[LABEL-TELEPHONE]:</span>
                            &nbsp;
                            <?php
                                 $formated_phone = preg_replace("/[^0-9\+]/", "", $ad['phone']);
                            ?>
                            <?php if($ad['phone_validated']=='Y') {?><i class="fa fa-check" style="color: lime;    background: none;  font-weight: bold;"></i>
                            <?php } ?>
                            <a href="tel:<?php echo $formated_phone;?>" onclick="gtag('event', 'touch',{'event_category' : 'PhoneClick','event_label' : '<?php echo $ad['phone'];?>' });"><?php echo $ad['phone'];?></a>

                             <?php if ($ad['user_id'] != 4617  && $advertiser[0]["disable_pm"] != 1 ) { ?>
                            <?php if ($chat['isLoggedIn']) { ?>
                            <?php if ($chat['haveOpenedChat']) { ?>
                            &nbsp; <a href="<?= \Core\Utils\Navigation::urlFor('chat', 'view', array('id' =>  $chat['chatId'])) ?>" class="pm">PM<span class="fa fa-send"></a>
                            <?php } else { ?>
                            &nbsp; <a href="#" id="send-message-button" class="pm <?= $chat['canSendMessage'] ? '' : 'disabled' ?>">PM<span class="fa fa-send"></span></a>
                            <?php } ?>
                            <?php } else { ?>
                            &nbsp; <a  href="<?= \Core\Utils\Navigation::urlFor('user', 'login') ?>" class="pm">PM<span class="fa fa-send"></a>
                            <?php } ?>
                            <?php } ?>
                            <?php if($ad['phone_validated'] == 'Y') {?>
                                <div style="color:lime; margin:10px 0px 10px 0px">[LABEL-PHONE-CONFIRMED] <?=$ad['validate_date']?></div>
                            <?php } ?>
                        </p>
                        <?php if($ad['myfans_url'] != ''):?>
                            <p>
                                <i class="fa fa-link"></i>
                                <span class="label">[LABEL-4MYFANS]:</span>
                                &nbsp;
                                <?php if (!preg_match("~^(?:f|ht)tps?://~i", $ad['myfans_url'])) {
                                    $ad['myfans_url'] = "https://" . $ad['myfans_url'];
                                }
                                ?>
                                <a href="<?=$ad['myfans_url']?>" target="_blank" >
                                    <?= preg_replace('#(^https?:\/\/(w{3}\.)?)|(\/$)#', '', $ad['myfans_url']); ?>  
                                </a>
                            </p>
                        <?php endif;?>
                        <p>
                            <i class="fa fa-car"></i>
                            <span class="label">[LABEL-LOCATION]:</span>
                            &nbsp;
                                <?php echo ($ad['location'] == '')? 'Incalls' : ucfirst($ad['location']);?>
                        </p>
                        <p>
                            <i class="fa fa-th-list"></i>
                            <span class="label">[LABEL-CATEGORY]:</span>
                            &nbsp;
                            <a href="<?=$ad['category_url']?>" title="<?=$ad['category_href_title']?>" >
                                <?=$ad['category_name']?>
                            </a>
                        </p>
                        <p>
                            <i class="fa fa-map-marker"></i>
                            <span class="label">[LABEL-CITY]:</span>
                            &nbsp;
                            <a href="<?=$ad['category_url_w_city']?>" title="<?=$ad['category_href_w_city_title']?>" >
                                <?=$ad['city_name']?>
                            </a>
                        </p>
                            
                            <?php if($ad['price_min'] > 0 || $ad['price_max'] > 0 ): ?>
                                <p>
                                    <i class="fa fa-money"></i>
                                    <span class="label">[LABEL-PRICE-NEW]:</span>
                                    &nbsp;
                                    <?php echo ( $ad['price_min'] > 0)? $ad['price_min'] .'$/30 min, ' : '';?>
                                    <?php echo ( $ad['price_max'] > 0)? $ad['price_max'] .'$/h' : '';?>
                                </p>
                            <?php endif;?>
                            
                            
                        <p>
                            <i class="fa fa-calendar"></i>
                            <span class="label">[LABEL-POSTED]:</span>
                            &nbsp;
                            <?=$ad['date_released']?>
                        </p>
                         <? if($ad['id'] != 1579901): ?>
                        <article class="share">
                            <nav class="social">
                                <div class="container_box">
                                    <div class="container_box_drawer" onclick="function open(){classList.toggle('open')}; open()">
                                        <a href="javascript:void 0;"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                        <a class="share-link" href="https://twitter.com/share?url=<?php echo rawurlencode(\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'). $ad['url'])?>" rel="nofollow" target="_blank">
                                            <i class="fa fa-twitter" aria-hidden="true" onclick="addSocialShare('twitter', 'view');"></i>
                                        </a>
                                       
                                    </div>
                                </div>
                            </nav>
                        </article>
                         <? endif ?>
                            
                            
                                <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE) : ?>
                                    <div class="favorite" style="position: absolute;right: 0;bottom: 35px;"><a href="<?=\Core\Utils\Navigation::urlFor('user', 'login')?>" id="btn_favorite"><i class="fa fa-heart-o"></i>&nbsp;[LABEL-ADD-FAV]</a></div>
                                <?php else:?>
                                    
                                    <div class="favorite" style="position: absolute;right: 0;bottom: 35px;">
                                        <a href="javascript:void(0)" id="btn_add_favorite" data-ut="<?php echo (\Core\Utils\Session::currentUserIsAdvertisment())? '1' : '2';?>" data-id="<?php echo $ad['id'];?>" <?php echo (in_array($ad['id'], $user_favorites))? 'style="display:none"' : 'style="display:block"';?>><i class="fa fa-heart-o"></i>&nbsp;[LABEL-ADD-FAV]</a>
                                    </div>    
                                    
                                    <div class="favorite" style="position: absolute;right: 0;bottom: 35px;">
                                        <a href="javascript:void(0)" id="btn_remove_favorite"  data-ut="<?php echo (\Core\Utils\Session::currentUserIsAdvertisment())? '1' : '2';?>" data-id="<?php echo $ad['id'];?>" <?php echo (!in_array($ad['id'], $user_favorites))? 'style="display:none"' : 'style="display:block"';?>><i class="fa fa-heart"></i>&nbsp;[LABEL-ADDED-FAV]</a>
                                    </div> 
                                    
                                    <?php if(\Core\Utils\Session::currentUserIsAdvertisment()) :?>
                                    <?php else:?>
                                    
                                    <?php endif;?>
                                    
                                <?php endif;?>
                             
                             
                             
                    </div>
                        
                        <?php if($map_coords && $map_coords['hide_map'] == 0 && $map_coords['lat'] != 0) : ?>
                            
                           
                                <?php $path = \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_images') . 'locations/';?> 
                                
                                <div style="margin:20px 0 ; position:relative; display:block">
                                    <div id="simple_map" style="background-image:url('<?php echo $path . $map_coords['location_photo'];?>'); display:block; "></div>
                                <?php if(\Core\Utils\Session::getCurrentUserID() == 63661):?>
                                    <a href="javascript:void(0)" id="toggle_interactive" class="btn_map" style="right: 10px; padding: 6px 10px; border-radius: 4px; font-size: 13px">Interactive Map</a>
                                <?php endif;?>
                                    <?php 
                                        $lat = floatval($map_coords['lat']);
                                        $lng = floatval($map_coords['long']);
                                        ?>
                                    <input type="hidden" name="show_map" id="show_map" value="yes" />
                                    <div id="map" style="height:0px; width: 100%;margin:0;" <?php echo ($map_coords && $map_coords['lat'] != 0)? 'data-lat="'.$map_coords['lat'].'" data-long="'.$map_coords['long'].'"' : 'data-city="' . $map_coords['ad_city_name'] . '" data-country="Canada"' ;?>></div>
                                </div>
                            
                        <?php else:?>
                            <input type="hidden" name="show_map" id="show_map" value="no" />
                        <?php endif;?>
                        
                    <p class="alert_description description">
                        [DESCRIPTION-AD-ALERT]
                    </p>
                
                </div>
            </div>
            <div class="ad_details_box comment-area">
                
                <div class="user_content_details">
                    <section class="comments-box">
                            <?php if($ad["blocked_comments"] == 1):?>
                            <article>
                                    <p style="" class="blocked-comments" data-text="[LABEL-USER-BLOCKED-COMMENTS]"></p>
                            </article>    
                        <?php else:?>
                        <article>
                            <p>[LABEL-COMMENTS-FOR-THIS-USER]</p>
                            <?php if(!empty($comments)) : ?>
                            <ul class="comments">
                                <?php foreach($comments as $comment) : ?>
                                <li class="comment">
                                                <div class="comment-header">
                                                    <div class="comment-title">
                                    <span class="member-name"><?= $comment['member_name']?></span>|<span ><?=$comment['date']?></span>
                                    <? if ($comment['user_id'] == $comment['from_user']):?>
                                         |<span>[LABEL-COMMENTS-ADVERTISER-REP]</span>
                                            <? elseif($comment['ad_status'] == 'active'): ?>
                                                            |<span>[LABEL-COMMENTS-FOR-AD] </span> <a href="<?=$comment['ad_url'];?>"><?=$comment['ad_title'];?></a>
                                             <? else: ?>
                                                            |<span>[LABEL-COMMENTS-FOR-AD] </span> <?=$comment['ad_title'];?>
                                    <? endif;?>    
                                                    </div> 
                                                    
                                                </div> 
                                    <p class="comment-msg"><?= $comment['comment']?></p>
                                                <div class="comment-score" style="width:auto;">
                                                        <div class="up">
                                                            <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE) : ?>
                                                                <a title="[HREF-TITLE-LOGIN]" href="<?=\Core\Utils\Navigation::urlFor('user', 'login')?>">
                                                                    <i class="fa fa-thumbs-up"></i>
                                                                </a>
                                                            <?php else: ?>
                                                                <a href="javascript:void(0)" class="thumbsup <?php echo (isset($user_votes[$comment['id']]) && $user_votes[$comment['id']] == 'up')? 'active' : '';?>" data-comment="<?php echo $comment['id'];?>" data-ad="<?php echo $comment['ad_id'];?>"><i class="fa fa-thumbs-up"></i> </a>
                                                            <?php endif;?>
                                                            <span class="score" id="up-<?php echo $comment['id'];?>"><?php echo $comment['up'];?></span>
                                                        </div>
                                                        <div class="down">
                                                            <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE) : ?>
                                                                <a title="[HREF-TITLE-LOGIN]" href="<?=\Core\Utils\Navigation::urlFor('user', 'login')?>">
                                                                    <i class="fa fa-thumbs-down"></i>
                                                                </a>
                                                            <?php else: ?>
                                                                <a href="javascript:void(0)" class="thumbsdown <?php echo (isset($user_votes[$comment['id']]) && $user_votes[$comment['id']] == 'down')? 'active' : '';?>" data-comment="<?php echo $comment['id'];?>" data-ad="<?php echo $comment['ad_id'];?>"><i class="fa fa-thumbs-down"></i></a>
                                                            <?php endif;?>
                                                            <span class="score" id="down-<?php echo $comment['id'];?>"><?php echo $comment['down'];?></span>
                                                        </div>
                                                    </div>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                                    
                                    <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::LOCALIZED_DEVICE_MODE);?> 
                            <?php endif; ?>
                        </article>
                        <article class="add-comment-area" >
                            <?php if(\Core\Utils\Session::currentUserIsMember() && $user_has_pending_comment) :?>
                                <p style="">[LABEL-COMMENT-MODERATION]</p>
                            <?php else:?>
                                <p></p> 
                            <?php endif; ?>
                            <p class="error" id="err-msg" style="display: none"></p>
                            <textarea name="comment" id="comment" placeholder="[LABEL-WRITE-MESSAGE]" rows="5"></textarea>
                            <input type="hidden" name="" id="ad_id" value="<?=$ad['id']?>">
                            <div>
                                <input type="button" value="[LABEL-POST-COMMENT]" class="btn" style="-webkit-appearance: none;" id="save-comment"<?php echo $redirect ? ' data-rdr="1"' : ( (!$is_member) ? ' data-alert="1"': '' ); ?>>
                            </div>
                        </article>
                        <?php endif;?>    
                    </section>
                </div>
                <?php if(@$ads) :?>
                

                <div>
                    <ul  class="box-wrapper">
                        <li class="title">[LABEL-MORE-ADS-FROM-THIS-USER]</li>
                        <?php

                            foreach($ads as $ad_m) {
                                require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_user_ads', \Core\View::DEVICE_MODE));
                            }
                        ?>
                    </ul>
                </div>
                <?php endif; ?>
            </div>
                
            <?php else:?>
                <div class="ad_details_box" style="min-height: auto;">
                        <div class="ad_details_box_content">
                            <p class="alert_description">[INACTIVE-AD-TEXT]&nbsp;<a href="<?php echo $ad['category_url_w_city'];?>"><?php echo $ad['city_name'];?></a></p>
                        </div>
                    <div style="clear:both;"></div>
                </div>
                <div class="ad_details_box comment-area">
                    <?php if(@$ads) :?>
                    

                        <div>
                            <ul  class="box-wrapper" style="margin-top: 0;">
                                <li class="title">[LABEL-MORE-ADS-FROM-THIS-USER]</li>
                                <?php

                                    foreach($ads as $ad_m) {
                                        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\Ad\Boxes::long_user_ads', \Core\View::DEVICE_MODE));
                                    }
                                ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>    
            <?php endif;?>
            
            
            
        </div>
        <div class="content_box_footer">
            <footer class="links">
                
                 <?php if (strtolower(parse_url(\Core\Utils\Device::getServerVarsAndHeaders('http_referer'), PHP_URL_HOST)) == strtolower(\Core\Utils\Device::getServerVarsAndHeaders('http_host'))):?>
                <section>                
                        <a onclick="gtag('event', 'click', {
                                                    'event_category' : 'BackToAds',
                                                    'event_label' : 'BackToAdsMobile',
                                                    'event_callback': function(){window.location.href='https://www.humpchies.com/<?php echo $go_back;?>';}
                                                  }); " href="<?php echo $go_back;?>" title="<?=$ad['back_link_title']?>" >
                        <?=$ad['back_link_label']?>
                    </a>
                </section>
                 <?php endif;?>
                     
                
                <section>                
                    <a  href="javascript:;" title="report" id = 'report-button' >
                        [LABEL-REPORT-TITLE]
                    </a>
                </section>
                
            </footer>
        </div>
    </div>
</div>

<style>
    h2.title { line-height: 27px !important; }
    .__premium { padding: 4px 7px; margin-right: 10px !important; background: #ff9900; border-radius: 4px; color: #333;
        text-shadow: -1px 0 1px #fff; }
</style>
<!-- The Modal -->
<div id="report-profile" class="modal_report">

  <!-- Modal content -->
  <div class="modal-content_report">
    <div class="modal-header_report">
      <span class="close_report">&times;</span>
      <h2>[LABEL-REPORT-TITLE]</h2>
    </div>
    <div class="modal-body_report">
      <ol>
        <li><label for='report_1' class="xreport">  <input type="radio" id='report_1' name='report_form' value='1' checked><span></span></label></li>
        <li><label for='report_2' class="xreport">  <input type="radio" id='report_2' name='report_form' value='2'><span></span></label></li>
        <li><label for='report_3' class="xreport">  <input type="radio" id='report_3' name='report_form' value='3'> <span></span></label></li>
        <li><label for='report_4' class="xreport-more">  <input type="radio" id='report_4' name='report_form' value='4'> <span></span></label></li>
      </ol>
      <input type="hidden" id='adID' value ="<?=$chatAdID?>" >
      <div id='report-other'>
        <textarea id='report-other-input' placeholder="[LABEL-REPORT-Z]"></textarea>
      </div>  
      <?php if(!\Core\Utils\Session::getCurrentUserID() ){ ?>
            <input type="text" name="email_address" id="email_address" placeholder="[LABEL-REPORT-F]" />
            <div class="input-error" id="error_email" style="display: none;">[LABEL-REPORT-ERROR-EMAIL]</div>
        <?php }?>
        <div class="input-error" id="error_general" style="display:none;">[LABEL-REPORT-ERROR]</div> 
    </div>
    <div class="modal-body-success" style="display: none;">
        [LABEL-REPORT-SUCCESS]
    </div>
    <div class="modal-footer_report">
      <input type="button" class='btn' value="[LABEL-SEND-REPORT]" id="send-report" style="-webkit-appearance: none;"/>
    </div>
  </div>

</div>
<script>
    var rep_texts = new Array();
    rep_texts[1] = "[LABEL-REPORT-A]";
    rep_texts[2] = "[LABEL-REPORT-B]";
    rep_texts[3] = "[LABEL-REPORT-C]";
    rep_texts[4] = "[LABEL-REPORT-D]";
    
    var more_placeholder = "[LABEL-REPORT-Z]";
    var other_placeholder = "[LABEL-REPORT-E]";
</script>

<?php if ($chat['canSendMessage']) { ?>

<div id="send-message-modal" class="modal-pm-gray">
    <!-- Modal content -->
    <div class="modal-pm-content">
        
        <div class="modal-pm-header">
        <div class="fa fa-close modal-pm-close"></div>
            <h2>[LABEL-SEND-MESSAGE-TITLE] <?=$ad['title']?></h2>
        </div>
        <div class="modal-pm-body">
            <input type="hidden" id='send-message-ad-id' value="<?=$chatAdID?>">
            <textarea id='send-message-body' placeholder="[LABEL-MESSAGE-PLACEHOLDER]"
                      maxlength="500"></textarea>
            <div class="modal-error">
                <span id="send-message-error-not-sent">[ERROR-SEND-MESSAGE-NOT-SENT]</span>
                <span id="send-message-error-required">[ERROR-SEND-MESSAGE-REQUIRED]</span>
                <span id="send-message-error-max-length">[ERROR-SEND-MESSAGE-MAX-LENGTH]</span>
            </div>
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left">
                <input type="button" class='btn-red modal-pm-close' value="[LABEL-CANCEL]"/>
            </div>
            <div class="modal-pm-right">
                <input id="send-message" type="button" class='btn-orange' value="[BTN-SEND-MESSAGE]" style="-webkit-appearance: none;"/>
            </div>
        </div>
    </div>
</div>

<div id="send-message-sent-modal" class="modal-pm">
    <!-- Modal content -->
    <div class="modal-pm-content">
        <div class="modal-pm-header">
            <div class="fa fa-close modal-pm-close"></div>
            <h2>[TITLE-MESSAGE-SENT]</h2>
            
        </div>
        <div class="modal-pm-body">
            <h3>[LABEL-MESSAGE-SENT]</h3>
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left">
                <input type="button" class='btn-red modal-pm-close' value="[LABEL-CLOSE]" style="-webkit-appearance: none;"/>
            </div>
            <div class="modal-pm-right">
                <a href="<?=\Core\Utils\Navigation::urlFor('chat', 'view')?>" class="btn-orange" title="[LABEL-CHAT-LIST]">[LABEL-CHAT-LIST]</a>
            </div>
        </div>
    </div>
</div>

<?php } // can send message ?>

