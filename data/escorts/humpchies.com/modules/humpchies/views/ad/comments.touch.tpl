
 <nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li>[LABEL-COMMENTS]</li>
    </ul>    
</nav>
 
 <div class="wrapper_content">
    <div class="content_box comments">
        <div class="content_box_header">
            <h2 class="title">[LABEL-COMMENTS]</h2>
        </div>
        <?php
            foreach($list as $item){
        ?>
        <div class="comment-list-ad">
             
            <div class="comment-list-subtitle">
                <span class="comment-name"><?php echo $item['user'] ?></span>&nbsp; [LABEL-COMMENT-ON] &nbsp;<?php echo $item['date'] ?> [LABEL-COMMENT-FOR-AD] <br>"<?php echo ucfirst($item['title']) ?>"
            </div> 
            
            <div class="comment-ad-content">
                
                
                <div class="comment-list-image" style="background-image:url('<?php echo $item['image'] ?>')">
                </div>
                <div class="comment-list-text">
                   
                    <div class="comment-list-body">
                        <?php echo $item['comment'] ?>
                    </div> 
                   
                    <div class="comment-score" style="margin-top: 10px; justify-content: flex-start;">
                <div class="up">
                    <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE) : ?>
                        <a title="[HREF-TITLE-LOGIN]" href="<?=\Core\Utils\Navigation::urlFor('user', 'login')?>">
                            <i class="fa fa-thumbs-up"></i>
                        </a>
                    <?php else: ?>
                        <a href="javascript:void(0)" class="thumbsup <?php echo (isset($user_votes[$item['id']]) && $user_votes[$item['id']] == 'up')? 'active' : '';?>" data-comment="<?php echo $item['id'];?>" data-ad="<?php echo $item['ad_id'];?>"><i class="fa fa-thumbs-up"></i> </a>
                    <?php endif;?>
                    <span class="score" id="up-<?php echo $item['id'];?>"><?php echo $item['up'];?></span>
                </div>
                <div class="down">
                    <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE) : ?>
                        <a title="[HREF-TITLE-LOGIN]" href="<?=\Core\Utils\Navigation::urlFor('user', 'login')?>">
                            <i class="fa fa-thumbs-down"></i>
                        </a>
                    <?php else: ?>
                        <a href="javascript:void(0)" class="thumbsdown <?php echo (isset($user_votes[$item['id']]) && $user_votes[$item['id']] == 'down')? 'active' : '';?>" data-comment="<?php echo $item['id'];?>" data-ad="<?php echo $item['ad_id'];?>"><i class="fa fa-thumbs-down"></i></a>
                    <?php endif;?>
                    <span class="score" id="down-<?php echo $item['id'];?>"><?php echo $item['down'];?></span>
                </div>
            </div>
            
                </div>
                
                </div>
                   
                   
             <div class="bottom-actions">
                    <div class="actions-left">
                        <a href="<?=$item['category_url']?>" title="<?=$item['category_href_title']?>" class="city-links">
                                <span style="color: #ff9900!important;"><?=$item['category_name']?></span>
                            </a>
                        <a href="<?php echo $item['urlCity'] ?>" class="city-links">&nbsp;<span class="spans" rel-data="[LABEL-IN]&nbsp;"></span><span style="color: #ff9900!important;"><?php echo ucfirst($item['city']) ?></span></a>
                    </div>
                    <div class="actions-right">
                        <a class="action-link" href=" <?php echo $item['link'] ?>">
                             [LABEL-COMMENT-VIEW-AD]
                        </a>
                    </div>
                </div>
                    
        </div>
        <?php
        }
        ?>

                 <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::LOCALIZED_DEVICE_MODE);?> 
         
        
    </div>
 </div>
 