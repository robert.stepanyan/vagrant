<nav id="breadcrumb" class="mb-30">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li> Terms and conditions</li>
    </ul>
</nav> 
<h2 class="stylized-title custom"><span>Terms and conditions</span></h2>
<div class="wrapper_content">
    <div class="content_box staticpage">
        
        <div class="content_box_body">
            <div class="event_list_box">
                <h2>1. ACCEPTANCE</h2>
                <p>
                    By using and/or visiting the Humpchies website (collectively, including but not limited to all Content, Uploads and User Submissions available through <?=\Core\Utils\Config::getConfigValue('\Application\Settings::hostname')?>, "Humpchies", the website) you agree to the terms and conditions contained herin and the terms and conditions of Humpchies' privacy policy incorporated herin, and all future amendments and modifications (collectively referred to as the "Agreement").  By entering, you agree to be bound by these terms and conditions.  If you do not agree to be bound the terms and conditions contained herein, then do not use Humpchies.com.
                </p>
                <p>
                    The terms and conditions of this Agreement are subject to change by Humpchies at any time in its sole discretion and you agree be bound by all modifications, changes and/or revisions.  If you do not accept to be bound by any and all modifications, changes and/or revisions of this agreement, you many not use Humpchies.com.
                </p>
                <p>
                    The terms and conditions contained herein apply to all users of Humpchies whether a 'visitor' or a 'member' and you are only authorized to use Humpchies.com if you agree to abide by all applicable laws and be legally bound by the terms and conditions of this Agreement.
                </p>
                <h2>2. DESCRIPTION</h2>
                <p>
                    The Humpchies website allows for uploading, sharing and general viewing various types of content allowing registered and unregistered users to share and view visual depictions of adult content, including sexually explicit images.  In addition, Humpchies contains images, information and other materials posted/uploaded by users.  Humpchies allows its users to view the Content and Website subject to the terms and conditions of this Agreement.
                </p>
                <p>
                    The Humpchies website may also contain certain links to third party websites which are in no way owned or controlled by Humpchies.  Humpchies assumes no responsibility for the content, privacy policies, practices of any and all third party websites.  Humpchies cannot censor or edit the content of third party sites.  You acknowledge that Humpchies will not be liable for any and all liability arising for your use of any third-party website.
                </p>
                <p>
                    <?=\Core\Utils\Config::getConfigValue('\Application\Settings::hostname')?> is for your personal use and shall not be used for any commercial endeavor except those specifically endorsed or approved by <?=\Core\Utils\Config::getConfigValue('\Application\Settings::hostname')?>.  Any illegal and/or unauthorized use of Humpchies is prohibited including but not limited to collecting usernames and e-mail addresses for sending unsolicited emails or unauthorized framing or linking to the Humpchies website is prohibited.
                </p>
                <h2>3. ACCESS</h2>
                <p>
                    In order to use this website, you affirm that you are at least eighteen (18) year of age and/or over the age of majority in the jurisdiction you reside and from which you access the website where the age of majority is greater than eighteen (18) years of age. If you are under the age of 18 and/or under the age of majority in the jurisdiction you reside and from which you access the website, then you are not permitted to use the website.
                </p>
                <h2>4. CONDUCT</h2>
                <p>
                    You acknowledge and agree that you shall be responsible for your own user submissions and the consequences of posting, uploading, publishing transmitting or other making them available on Humpchies.  You agree that you shall not (nor others using your account) post, upload, publish, transmit or make available in any way on Humpchies content which is illegal, unlawful, harassing, harmful, threatening, tortuous, abusive, defamatory, obscene, libelous, invasive of one's privacy including but not limited to personal information, hateful, racial.  You also agree that you shall not post, upload, publish, transmit or make available in any way on Humpchies software containing viruses or any other computer code, files, or programs designed to destroy, interrupt, limit the functionality of, or monitor, or persistently reside in or on any computer software or hardware or telecommunications equipment.  You agree that you shall not (nor others using your account) post, upload, publish, transmit or make available in any way on Humpchies content which is intentionally or unintentionally violating  any applicable local, state, national, or international law, or any regulations or laws having the force of law where you reside and elsewhere, including but not limited to any laws or regulations relating to securities, privacy, and export control; engage in, promote, You agree that you shall not (nor others using your account) post, upload, publish, transmit or make available in any way on Humpchies content  depicting illegal activities, promote or depict physical harm or injury against any group or individual, or promote or depict any act of cruelty to animals; You agree not to use Humpchies in any way that exposes Humpchies  to criminal or civil liability.
                </p>
                <p>
                    You agree that Humpchies shall have the right to determine in its sole and unfettered discretion, what action shall be taken in the event of any discovered or reported violation of the terms and conditions contained herein.
                </p>
                <h2>5. INTELLECTUAL PROPERTY  </h2>
                <p>
                    The Content contained on Humpchies with the exception of User Submissions including but not limited to the text, software, scripts, graphics, music, videos, photos, sounds, interactive features and trademarks, service marks and logos contained therein, are owned by and/or licensed to Humpchies, subject to copyright and other intellectual property rights under United States, Canada and foreign laws and international conventions. Content on the Website is provided to you AS IS for your information and personal use only and may not be used, copied, reproduced, distributed, transmitted, broadcast, displayed, sold, licensed, or otherwise exploited for any other purposes whatsoever without the prior written consent of the respective owners. Humpchies reserves all rights not expressly granted in and to the Website and the Content. You agree to not engage in the use, copying, or distribution of any of the Content other than expressly permitted herein, including any use, copying, and/or distribution of User Submissions of third parties obtained through the Website for any commercial purposes. If you download or print a copy of the Content for personal use, you must retain all copyright and other proprietary notices contained therein. You agree not to disable, circumvent, or otherwise interfere with security related features of the Humpchies or features that prevent or restrict use or copying of any Content or enforce limitations on use of the Humpchies Website or the Content therein.
                </p>
                <h2>6. USER SUBMISSIONS</h2>
                <p>
                    Humpchies permits the submission of ads, profiles and other communications and the hosting, sharing and publishing of such user submissions.  You understand that whether or not such User Submissions are published and/or uploaded, Humpchies does not guarantee any confidentiality with respect to any submissions.
                </p>
                <p>
                    Humpchies allows/permits you to link to materials on the Website for personal, non-commercial purposes only. Humpchies reserves the right to discontinue any aspect of the Humpchies website at any time.
                </p>
                <p>
                    You shall be solely responsible for any and all of your own User Submissions and the consequences of posting, uploading and publishing them.  Furthermore, with User Submissions, you affirm, represent and/or warrant that:
                </p>
                <ol>
                    <li>You own or retain the necessary licenses, rights, consents, and permissions to use and authorize Humpchies to use all trademarks, copyrights, trade secrets, patents, or other proprietary rights in and to any and all User Submissions to enable inclusion and use of the User Submissions in the manner contemplated by the Website and these Terms of Service; and </li>
                    <li>You will not post, or allow anyone else to post, any material that depicts any person under the age of 18 years and you have inspected and are maintaining written documentation sufficient to confirm that all subjects of your submissions are, in fact, over the age of 18 years.</li>
                    <li>You have the written consent, release, and/or permission of each and every identifiable individual person in the User Submission to use the name or likeness of each and every such identifiable individual person to enable inclusion and use of the User Submissions in the manner contemplated by the Website and these Terms of Service. For clarity, you retain all of your ownership rights in your User Submissions. By submitting the User Submissions to Humpchies, you hereby grant Humpchies a worldwide, non-exclusive, royalty-free, sublicenseable and transferable license to use, reproduce, distribute, prepare derivative works of, display, and perform the User Submissions in connection with the Humpchies Website and Humpchies's (and its successor's) business, including without limitation for promoting and redistributing part or all of the Humpchies Website (and derivative works thereof) in any media formats and through any media channels. You also hereby grant each user of the Humpchies Website a non-exclusive license to access your User Submissions through the Website, and to use, reproduce, distribute, prepare derivative works of, display and perform such User Submissions as permitted through the functionality of the Website and under these Terms of Service. The foregoing license granted by you terminates once you remove or delete a User Submission from the Humpchies Website.</li>
                </ol>
                <p>In submitting material (ads, images, profiles or other communication), you further agree that you shall not:</p>
                <ol>
                    <li>Submit material that is copyrighted, protected by trade secret or otherwise subject to third party proprietary rights, including privacy and publicity rights, unless you are the owner of such rights or have permission from their rightful owner to post the material and to grant Humpchies all of the license rights granted herein; </li>
                    <li>Publish falsehoods or misrepresentations that could damage Humpchies or any third party; </li>
                    <li>Submit material that is obscene, illegal, unlawful, , defamatory, libelous, harassing, hateful, racially or ethnically offensive, or encourages conduct that would be considered a criminal offense, give rise to civil liability, violate any law, or is otherwise inappropriate; </li>
                    <li>Impersonate another person. Humpchies does not endorse any User Submission or any opinion, recommendation, or advice expressed therein, and Humpchies expressly disclaims any and all liability in connection with User Submissions. Humpchies does not permit copyright infringing activities and infringement of intellectual property rights on its Website, and Humpchies will remove all Content and User Submissions if properly notified that such Content or User Submission infringes on another's intellectual property rights. Humpchies reserves the right to remove Content and User Submissions without prior notice or delay. Humpchies will also terminate a User's access to its Website, if they are determined to be an infringer.  While pornographic and adult content are accepted, Humpchies also reserves the right to decide in its sole and unfettered discretion, whether Content or a User Submission is appropriate and complies with these Terms of Service for violations other than copyright infringement and violations of intellectual property law, such as, but not limited to, obscene or defamatory material, or excessive length. Humpchies may remove such User Submissions and/or terminate a User's access for uploading such material in violation of these Terms of Service at any time, without prior notice and at its sole discretion.</li>
                </ol>
                <p>
                    You understand and acknowledge that when using Humpchies.com, you will be exposed to User Submissions from a variety of sources, and that Humpchies is not responsible for the accuracy, usefulness, safety, or intellectual property rights of or relating to such User Submissions. You further understand and acknowledge that you may be exposed to User Submissions that are inaccurate, offensive, indecent, or objectionable, and you agree to waive, and hereby do waive, any legal or equitable rights or remedies you have or may have against Humpchies with respect thereto, and agree to indemnify and hold Humpchies, its Owners, affiliates, operators, and/or licensors, harmless to the fullest extent allowed by law regarding all matters related to your use of the website.
                </p>
                <p>
                    You agree that Humpchies may at its sole discretion have the right to refuse to publish, remove, or block access to any User Submission that is available via the Website or other Humpchies network or services at any time, for any reason, or for no reason at all, with or without notice.
                </p>
                <p>
                    Humpchies provides its website as a service to its users. However, Humpchies assumes no responsibility whatsoever for monitoring the Humpchies Services for inappropriate content or conduct. If at any time Humpchies chooses, in its sole discretion, to monitor the Humpchies Services, however, Humpchies assumes no responsibility for the content, no obligation to modify or remove any inappropriate content, and no responsibility for the conduct of the User submitting any such content. Humpchies may review and delete any User Submissions that, in its sole judgment, violates this Agreement or may be otherwise offensive or illegal, or violate the rights, harm, or threaten the safety of any User or person not associated with the Website (collectively "Inappropriate User Submissions"). You are solely responsible for the User Submissions that you make visible on the Website or any other material or information that you transmit or share with other Users or unrelated third-parties via the Website.
                </p>
                <p>
                    You further understand, acknowledge, agree and specifically authorize Humpchies to use, reuse, post, publish or upload any User Submissions on any other website owned or controlled by Humpchies or on any website with whom Humpchies has an agreement with respect to User Submissions content. Humpchies reserves the right to determine as its discretion on what basis would a User submissions might be shared with other websites (rating, number of views, user reviews, etc.).
                </p>
                <h2>
                    7. ACCOUNT TERMINATION POLICY
                </h2>
                <ol>
                    <li>A user's access to Humpchies will be terminated if, under appropriate conditions, the User is determined to infringe repeatedly.</li>
                    <li>Humpchies reserves the right to decide whether Content or if a User's Submission is appropriate and complies with these Terms and Conditions in regards to violations other than copyright infringement or privacy law, such as, but not limited to, hate crimes, pornography, obscene or defamatory material, or excessive length. Humpchies may remove such User Submissions and/or terminate a User's access for uploading such material in violation of these Terms and Conditions at any time, without prior notice and at its sole discretion.</li>
                </ol>
                <h2>8. POLICY</h2>
                <p>
                    Humpchies abides by a ZERO TOLERANCE policy relating to any illegal content.  Child Pornography, Child Exploitation, bestiality, rape, torture, snuff, death and/or any other type of obscene and/or illegal material shall not be tolerated by Humpchies. Humpchies shall not condone child pornography and will cooperate with all governmental agencies that seek those who produce child pornography or exploit children.
                </p>
                <h2>9. FEES</h2>
                <p>
                    You acknowledge that Humpchies reserves the right to charge for Humpchies services and to change its fees from time to time in its discretion.  Further more, in the event Humpchies terminates your rights to use the website because of a breach of this Agreement, you shall not be entitled to the refund of any unused portion of subscription fees.
                </p>
                <h2>10. WARRANTIES</h2>
                <p>
                    You represent and warrant that all of the information provided by you to Humpchies to participate in the Humpchies website is accurate and current and you have all necessary right, power, and authority to enter into this Agreement and to perform the acts required of you hereunder.
                </p>
                <p>
                    As a condition to using Humpchies, you must agree to the terms of Humpchies' privacy policy and its modifications. You acknowledge and agree that the technical processing and transmission of the Website, including your User Submissions, may involve transmissions over various networks; and changes to conform and adapt to technical requirements of connecting networks or devices. You further acknowledge and agree that other data collected and maintained by Humpchies with regard to its users may be disclosed in accordance with the Humpchies Privacy Policy.
                </p>
                <h2>11. WARRANTY DISCLAIMER</h2>
                <p>
                    YOU AGREE THAT YOUR USE OF THE HUMPCHIES WEBSITE SHALL BE AT YOUR SOLE RISK. TO THE FULLEST EXTENT PERMITTED BY LAW, HUMPCHIES, ITS OFFICERS, DIRECTORS, EMPLOYEES, AND AGENTS DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, IN CONNECTION WITH THE WEBSITE AND YOUR USE THEREOF. HUMPCHIES MAKES NO WARRANTIES OR REPRESENTATIONS ABOUT THE ACCURACY OR COMPLETENESS OF THIS SITE'S CONTENT OR THE CONTENT OF ANY SITES LINKED TO THIS SITE AND ASSUMES NO LIABILITY OR RESPONSIBILITY FOR ANY
                </p>
                <ol>
                    <li>ERRORS, MISTAKES, OR INACCURACIES OF CONTENT, </li>
                    <li>PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF OUR WEBSITE, </li>
                    <li>ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN,</li>
                    <li>ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM OUR WEBSITE, (IV) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE WHICH MAY BE TRANSMITTED TO OR THROUGH OUR WEBSITE BY ANY THIRD PARTY, AND/OR </li>
                    <li>ANY ERRORS OR OMISSIONSIN ANY CONTENT OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE HUMPCHIES WEBSITE.
                    HUMPCHIES DOES NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY A THIRD PARTY THROUGH THE HUMPCHIES WEBSITE OR ANY HYPERLINKED WEBSITE OR FEATURED IN ANY BANNER OR OTHER ADVERTISING, AND HUMPCHIES WILL NOT BE A PARTY TO OR IN ANY WAY BE RESPONSIBLE FOR MONITORING ANY TRANSACTION BETWEEN YOU AND THIRD-PARTY PROVIDERS OF PRODUCTS OR SERVICES. AS WITH THE PURCHASE OF A PRODUCT OR SERVICE THROUGH ANY MEDIUM OR IN ANY ENVIRONMENT, YOU SHOULD USE YOUR BEST JUDGMENT AND EXERCISE CAUTION WHERE APPROPRIATE.
                    THE FOREGOING LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION.
                    YOU SPECIFICALLY ACKNOWLEDGE THAT HUMPCHIES SHALL NOT BE LIABLE FOR USER SUBMISSIONS OR THE DEFAMATORY, OFFENSIVE, OR ILLEGAL CONDUCT OF ANY THIRD PARTY AND THAT THE RISK OF HARM OR DAMAGE FROM THE FOREGOING RESTS ENTIRELY WITH YOU. </li>
                </ol>
                <h2>12. LIMITATION OF LIABILITY</h2>
                <p>
                    IN NO EVENT SHALL HUMPCHIES, ITS OFFICERS, DIRECTORS, EMPLOYEES, OR AGENTS, BE LIABLE TO YOU FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES WHATSOEVER RESULTING FROM ANY:
                </p>
                <ol>
                    <li>ERRORS, MISTAKES, OR INACCURACIES OF CONTENT, </li>
                    <li>PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF OUR WEBSITE, </li>
                    <li>ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN, </li>
                    <li>ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM OUR WEBSITE, </li>
                    <li>ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE, WHICH MAY BE TRANSMITTED TO OR THROUGH OUR WEBSITE BY ANY THIRD PARTY, AND/OR </li>
                    <li>ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF YOUR USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE HUMPCHIES WEBSITE, WHETHER BASED ON WARRANTY, CONTRACT, TORT, OR ANY OTHER LEGAL THEORY, AND WHETHER ORNOT THE COMPANY IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
                    THE FOREGOING LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION.
                    YOU SPECIFICALLY ACKNOWLEDGE THAT HUMPCHIES SHALL NOT BE LIABLE FOR USER SUBMISSIONS OR THE DEFAMATORY, OFFENSIVE, OR ILLEGAL CONDUCT OF ANY THIRD PARTY AND THAT THE RISK OF HARM OR DAMAGE FROM THE FOREGOING RESTS ENTIRELY WITH YOU.</li>
                </ol>
                <h2>13. INDEMNITY</h2>
                <p>
                    You agree to defend, indemnify and hold harmless Humpchies, its parent corporation, officers, directors, employees and agents, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees) arising from:
                </p>
                <ol>
                    <li>Your use of and access to the Humpchies Website; </li>
                    <li>Your violation of any term of these Terms of Service; </li>
                    <li>Your violation of any third party right, including without limitation any copyright, property, or privacy right; or </li>
                    <li>Any claim that one of your User Submissions caused damage to a third party. This defense and indemnification obligation will survive these Terms of Service and your use of the Humpchies Website. </li>
                </ol>
                <p>
                    You affirm that you are either more than 18 years of age or an emancipated minor, or possess legal parental or guardian consent, and are fully able and competent to enter into the terms, conditions, obligations, affirmations, representations, and warranties set forth in these Terms of Service, and to abide by and comply with these Terms and Conditions contained herein.
                </p>
                <h2>14.  ASSIGNMENT</h2>
                <p>
                    The Terms and Conditions contained herein and any rights and licenses granted hereunder, may not be transferred or assigned by you, but may be assigned by Humpchies without restriction.
                </p>
                <p>
                    If any term, clause or provision of the agreement is held invalid or unenforceable by a court of competent jurisdiction, such invalidity shall not affect the validity or operation of any term, clause or provision and such invalid term, clause or provision shall be deemed to be severed from this Agreement.
                </p>    
            </div>
        </div>
    </div>
</div>