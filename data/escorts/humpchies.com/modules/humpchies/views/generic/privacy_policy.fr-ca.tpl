<nav id="breadcrumb" class="mb-30">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li>Politique de confidentialit&eacute;</li>
    </ul>
</nav> 
<h2 class="stylized-title custom"><span>Politique de confidentialit&eacute;</span></h2>
<div class="wrapper_content">
    <div class="content_box staticpage">
        <div class="content_box_body">
            <div class="event_list_box">
                <p>
                    Ce document d&eacute;taille les informations importantes concernant l'utilisation et la divulgation des donn&eacute;es de l'utilisateur recueillies sur www.humpchies.com (Humpchies).
                <p>
                <p>
                    La s&eacute;curit&eacute; de vos donn&eacute;es est tr&egrave;s importante chez Humpchies et comme tel nous prenons toutes les mesures appropri&eacute;es pour limiter le risque qu'il peut &ecirc;tre perdu, endommag&eacute; ou mal utilis&eacute;.
                </p>
                <p>
                    Ce site limite express&eacute;ment et strictement ses privil&egrave;ges de membre et / ou affichage aux adultes de 18 ans et plus ou ayant atteint l'&acirc;ge de la majorit&eacute; dans leur communaut&eacute;. Toutes les personnes qui ne r&eacute;pondent pas &agrave; ces crit&egrave;res sont strictement interdits d'acc&eacute;der ou de visualiser le contenu de ce site. Nous ne cherchons pas sciemment ou collectons aucune information personnelle ou de donn&eacute;es de personnes qui n`ont pas atteint l'&acirc;ge de la majorit&eacute;. 
                </p>
                <h2>Les donn&eacute;es recueillies</h2>
                <ul class="bullet">
                    <li>Renseignements personnels:</li>
                        <ul class="bullet">
                            <li>Non-inscrits utilisateurs peuvent parcourir les profils et annonces sans enregistrement et sans aucune information collect&eacute;es et trait&eacute;es. Cependant l'adresse IP du visiteur sera enregistr&eacute;e dans le cas o&ugrave; il y a un d&eacute;tournement d'informations et / ou contenu.</li>
                            <li>Membres enregistr&eacute;s: L'inscription est obligatoire pour le posting d` annonces, et d'acc&eacute;der &agrave; un certain nombre d'autres fonctionnalit&eacute;s. Les renseignements personnels suivants sont demand&eacute;s au moment de l'inscription: nom d'utilisateur (obligatoire), et adresse e-mail (obligatoire). Les informations personnelles suppl&eacute;mentaires, telles que le num&eacute;ro de t&eacute;l&eacute;phone, l'&eacute;tat de la relation et l'orientation sexuelle peut &ecirc;tre ajout&eacute; sur une base volontaire apr&egrave;s l'inscription, pour les membres int&eacute;ress&eacute;s &agrave; identifier et potentiellement en contact avec les autres membres r&eacute;pondant &agrave; des crit&egrave;res sp&eacute;cifiques. Toutes ces donn&eacute;es &agrave; l'exception de l'adresse &eacute;lectronique et l'adresse IP devient une information accessible au public.</li>
                        </ul>
                    <li>Contenu t&eacute;l&eacute;charg&eacute; sur le site: Tous les renseignements personnels, annonce ou contenu du profil que vous divulguez volontairement en ligne devient accessible au public et peuvent &ecirc;tre collect&eacute;es et utilis&eacute;es par d'autres.</li>
                    <li>Cookies: Lorsque vous visitez Humpchies, nous pouvons envoyer un ou plusieurs cookies sur votre ordinateur qui identifie de mani&egrave;re unique votre session de navigation. Humpchies utilise &agrave; la fois des cookies de session et des cookies persistants. Si vous retirez votre cookie persistant, certaines fonctionnalit&eacute;s du site pourraient ne pas fonctionner correctement.</li>
                    <li>Information du fichier journal: Lorsque vous visitez Humpchies, nos serveurs enregistrent automatiquement certaines informations que votre navigateur envoie comme votre requ&ecirc;te web, l'adresse IP, le type de navigateur, la langue du navigateur, l'URL de r&eacute;f&eacute;rence, le type de plateforme, noms de domaine et la date et l'heure de votre demande.</li>
                    <li>Emails: Si vous nous contactez, nous pouvons garder une trace de cette correspondance.</li>
                </ul>
                <h2>UTILISATIONS</h2>
                <ul class="bullet">
                    <li>Vos informations personnellement identifiables soumis &agrave; Humpchiessont utilis&eacute;es pour fournir &agrave; l'utilisateur les fonctionnalit&eacute;s du site web et des fonctionnalit&eacute;s personnalis&eacute;es sp&eacute;ciales.</li>
                    <li>Votre nom d'utilisateur choisi (pas votre adresse e-mail) est affich&eacute; &agrave; d'autres utilisateurs &agrave; c&ocirc;t&eacute; du contenu que vous t&eacute;l&eacute;chargez, y compris les annonces, les profils, les commentaires, les messages que vous envoyez par la poste priv&eacute;e Humpchies, etc. D'autres utilisateurs peuvent vous contacter par des messages priv&eacute;s.</li>
                    <li>Les annonces ou les profils que vous soumettez &agrave; Humpchies peuvent &ecirc;tre redistribu&eacute;s &agrave; travers les canaux Internet et d'autres m&eacute;dias, et peuvent &ecirc;tre consult&eacute;s par le grand public.</li>
                    <li>Nous n&rsquo;utilisons pas votre adresse e-mail ou d'autres informations personnellement identifiables pour envoyer des messages commerciaux ou de marketing sans votre consentement.</li>
                    <li>Nous pouvons utiliser votre adresse e-mail sans autre consentement &agrave; des fins de non-commercialisation ou administratives (comme vous informer des changements de site web cl&eacute;s ou &agrave; des fins de service &agrave; la client&egrave;le).</li>
                    <li>OPT-IN ET COMMUNICATION UTILISATEUR - Abonn&eacute; express&eacute;ment et sp&eacute;cifiquement reconna&icirc;t et convient que son adresse e-mail ou d'autres moyens de communication avec l'abonn&eacute; peuvent &ecirc;tre utilis&eacute;s pour lui envoyer des offres, des informations ou d'autres e-mails &agrave; vocation commerciale ou d'autres moyens de communication. Plus pr&eacute;cis&eacute;ment, certaines offres peuvent &ecirc;tre pr&eacute;sent&eacute;es &agrave; l'abonn&eacute; via des campagnes d'e-mail ou autres moyens de communication avec l'option d'exprimer la pr&eacute;f&eacute;rence de l'abonn&eacute; soit en cliquant ou en saisissant &laquo;accepter&raquo; (alternativement &laquo;oui&raquo;) ou &laquo;d&eacute;clin&raquo; (alternativement &laquo;non&raquo; ). En s&eacute;lectionnant ou en cliquant sur &quot;Valider&quot; ou &quot;oui&quot;, l'abonn&eacute; indique que l'abonn&eacute; &quot;CHOISIT-IN&quot; &agrave; cette offre et ainsi accepte et consent que les renseignements personnels de l'abonn&eacute;, y compris son adresse e-mail et des donn&eacute;es peut &ecirc;tre utilis&eacute; pour cette question ou communiqu&eacute;es &agrave; des tiers &quot;.</li>
                    <li>OPT-OUT ET COMMUNICATION UTILISATEUR - Abonn&eacute; express&eacute;ment et sp&eacute;cifiquement reconna&icirc;t et accepte que son adresse e-mail ou d'autres moyens de communication avec l'abonn&eacute; peuvent &ecirc;tre utilis&eacute;s pour envoyer lui offre, d'informations ou d'autres e-mails &agrave; vocation commerciale ou d'autres moyens de communication. Plus pr&eacute;cis&eacute;ment, d'autres offres peuvent &ecirc;tre pr&eacute;sent&eacute;es &agrave; l'abonn&eacute; via des campagnes d'e-mail ou autres moyens de communication avec une pr&eacute;f&eacute;rence ou un choix de pr&eacute;s&eacute;lection. Si l'abonn&eacute; ne d&eacute;coche pas la pr&eacute;f&eacute;rence pr&eacute;-s&eacute;lectionn&eacute;e de choix (ce est &agrave; dire &quot;opt-out&quot; de l'offre), le site peut transf&eacute;rer des informations de profil personnel de l'abonn&eacute; au service tiers ou fournisseur de contenu qui fait l'offre. Si l'abonn&eacute; d&eacute;s&eacute;lectionner la pr&eacute;f&eacute;rence pr&eacute;-s&eacute;lectionn&eacute;, aucune information personnelle sur l'abonn&eacute; peuvent &ecirc;tre divulgu&eacute;s &agrave; un service tiers ou fournisseur de contenu.</li>
                    <li>Nous analysons les informations de trafic utilisateur agr&eacute;g&eacute;es pour aider &agrave; rationaliser nos activit&eacute;s de marketing et les op&eacute;rations d'h&eacute;bergement et d'am&eacute;liorer la qualit&eacute; de l'exp&eacute;rience utilisateur Humpchies.</li>
                </ul>
                <h2>DIVULGATION DE RENSEIGNEMENTS</h2>
                <ul class="bullet">
                    <li>Humpchies peut publier des donn&eacute;es pour se conformer &agrave; une obligation l&eacute;gale, ou en vue de faire respecter nos conditions d'utilisation et d'autres accords; ou pour prot&eacute;ger les droits, la propri&eacute;t&eacute; ou la s&eacute;curit&eacute; de Humpchies ou nos abonn&eacute;s ou autres. Cela comprend l'&eacute;change d'informations avec d'autres entreprises et organisations, y compris les autorit&eacute;s polici&egrave;res et gouvernementales aux fins de protection contre la fraude ou tout autre type d'activit&eacute; ill&eacute;gale ou non identifi&eacute;e dans les conditions de service. Ceci est la politique de Humpchies, chaque fois que possible et autoris&eacute;e par la loi, pour vous informer rapidement sur une obligation de fournir des donn&eacute;es &agrave; des tiers.</li>
                    <li>Si vous t&eacute;l&eacute;charger d&eacute;lib&eacute;r&eacute;ment sur Humpchiesdu mat&eacute;riel ill&eacute;galnous transmettrons toutes les informations disponibles &agrave; toutes les autorit&eacute;s concern&eacute;es et ce, sans pr&eacute;avis. Nous ne partageons pas vos informations personnelles (telles que le nom ou l'adresse e-mail) avec d'autres, des soci&eacute;t&eacute;s tierces pour leur utilisation commerciale ou de marketing sans votre consentement ou sauf dans le cadre d'un programme ou une fonction sp&eacute;cifique pour lequel vous aurez la possibilit&eacute; de opt-in ou opt-out.</li>
                </ul>
                <h2>S&Eacute;CURIT&Eacute;</h2>
                <p>    Lorsque nous vous avons donn&eacute; (ou si vous avez choisi un mot de passe) qui vous permet d'acc&eacute;der &agrave; certaines parties de notre site, vous &ecirc;tes responsable du maintien de ce mot de passe confidentiel. Nous vous demandons de ne pas partager votre mot de passe avec personne.
                </p>
                <p>
                    Malheureusement, la transmission d'informations via Internet n&rsquo;est pas totalement s&eacute;curis&eacute;e. Humpchies utilise commercialement des garanties physiques, techniques et de gestion raisonnables pour pr&eacute;server l'int&eacute;grit&eacute; et la s&eacute;curit&eacute; de vos informations personnelles. Nous ne pouvons pas, cependant, assurer ou garantir la s&eacute;curit&eacute; des informations que vous transmettez &agrave; Humpchies et vous le faites &agrave; vos propres risques.
                </p>
                
                <h2>VOS DROITS</h2>
                
                <p>    Vous avez le droit d'acc&egrave;s et de rectification de vos donn&eacute;es en le faisant directement sur le site ou en nous demandant de le faire via le section Contactez-nous. Si vous avez des questions suppl&eacute;mentaires, puis se il vous pla&icirc;t &eacute;crivez-nous &agrave; webmaster@www.humpchies.com. 
                </p>
            </div>
        </div>
    </div>
</div>