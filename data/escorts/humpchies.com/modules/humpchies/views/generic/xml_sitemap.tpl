<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders XML code to the user's screen that
 * represents the site's XML sitemap for SEO.
 */
?>
<?='<?xml version="1.0"?>'?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?=($base_url = \Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname'))?></loc>
    </url>
    <url>
        <loc><?=($base_url . \Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => 'escort')))?></loc>
    </url>
    <?php   if(@$urls['escort']){
                foreach($urls['escort'] as &$escort_uri){?>
                    <url>
                        <loc><?=$base_url . $escort_uri?></loc>
                    </url>         
    <?php       }
            }?>
    <url>
        <loc><?=($base_url . \Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => 'escort-agency')))?></loc>
    </url>
    <?php   if(@$urls['escort-agency']){
                foreach($urls['escort-agency'] as &$escort_agency_uri){?>
                    <url>
                        <loc><?=$base_url . $escort_agency_uri?></loc>
                    </url>         
    <?php       }
            }?>
    <url>
        <loc><?=($base_url . \Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => 'erotic-massage')))?></loc>
    </url>
    <?php   if(@$urls['erotic-massage']){
                foreach($urls['erotic-massage'] as &$erotic_massage_uri){?>
                    <url>
                        <loc><?=$base_url . $erotic_massage_uri?></loc>
                    </url>         
    <?php       }
            }?>
    <url>
        <loc><?=($base_url . \Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => 'erotic-massage-parlor')))?></loc>
    </url>
    <?php   if(@$urls['erotic-massage-parlor']){
                foreach($urls['escort-agency'] as &$erotic_massage_parlor_uri){?>
                    <url>
                        <loc><?=$base_url . $erotic_massage_parlor_uri?></loc>
                    </url>         
    <?php       }
            }?>
</urlset>