<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents the site's Site Map
 */
?>
<h2 class="stylized-title custom"><span>[LABEL-SITE-MAP]</span></h2>
<div class="wrapper_content">
    <div class="content_box staticpage">
        <div class="content_box_body">
            <div class="event_list_box">
                <ul class="bullet">
                    <li>
                        <a href="<?=$root_url?>" title="[HREF-TITLE-ROOT]">
                            [LABEL-HOME]
                        </a>
                        <?php   if(@$cities){?>
                            <br />
                            <ul class="bullet">
                                <?php   foreach($cities as &$city_details){?>
                                    <li>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="<?=$city_details['city_list_url']?>" title="<?=$city_details['city_list_href_title']?>">
                                            <?=$city_details['city_name']?>
                                        </a>
                                    </li>
                                <?php   }?>
                            </ul>
                        <?php   }?>
                    </li>
                    <li>
                        <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => 'escort'))?>" title="[HREF-TITLE-ESCORT-PART-1][LABEL-MONTREAL]">
                            [LABEL-ESCORT]
                        </a>
                        <?php   if(@$categories_cities['escort']){?>
                            <br />
                            <ul class="bullet">
                                <?php   foreach($categories_cities['escort'] as &$city_details){?>
                                    <li>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="<?=$city_details['city_list_url']?>" title="<?=$city_details['city_list_href_title']?>">
                                            <?=$city_details['city_name']?>
                                        </a>
                                    </li>
                                <?php   }?>
                            </ul>
                        <?php   }?>
                    </li> 
                    <li>
                        <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => 'escort-agency'))?>" title="[HREF-TITLE-ESCORT-AGENCY-PART-1][LABEL-MONTREAL]">
                            [LABEL-ESCORT-AGENCY]
                        </a>
                        <?php   if(@$categories_cities['escort-agency']){?>
                            <br />
                            <ul class="bullet">
                                <?php   foreach($categories_cities['escort-agency'] as &$city_details){?>
                                    <li>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="<?=$city_details['city_list_url']?>" title="<?=$city_details['city_list_href_title']?>">
                                            <?=$city_details['city_name']?>
                                        </a>
                                    </li>
                                <?php   }?>
                            </ul>
                        <?php   }?>
                    </li> 
                    <li>
                        <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => 'erotic-massage'))?>" title="[HREF-TITLE-EROTIC-MASSAGE-PART-1][LABEL-MONTREAL]">
                            [LABEL-EROTIC-MASSAGE]
                        </a>
                        <?php   if(@$categories_cities['erotic-massage']){?>
                            <br />
                            <ul class="bullet">
                                <?php   foreach($categories_cities['erotic-massage'] as &$city_details){?>
                                    <li>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="<?=$city_details['city_list_url']?>" title="<?=$city_details['city_list_href_title']?>">
                                            <?=$city_details['city_name']?>
                                        </a>
                                    </li>
                                <?php   }?>
                            </ul>
                        <?php   }?>
                    </li> 
                    <li>
                        <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => 'erotic-massage-parlor'))?>" title="[HREF-TITLE-EROTIC-MASSAGE-PARLOR-PART-1][LABEL-MONTREAL]">
                            [LABEL-EROTIC-MASSAGE-PARLOR]
                        </a>
                        <?php   if(@$categories_cities['erotic-massage-parlor']){?>
                            <br />
                            <ul class="bullet">
                                <?php   foreach($categories_cities['erotic-massage-parlor'] as &$city_details){?>
                                    <li>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="<?=$city_details['city_list_url']?>" title="<?=$city_details['city_list_href_title']?>">
                                            <?=$city_details['city_name']?>
                                        </a>
                                    </li>
                                <?php   }?>
                            </ul>
                        <?php   }?>
                    </li>
                    <li>
                        <a href="<?=$login_url?>" title="[HREF-TITLE-LOGIN]">
                            [LABEL-LOGIN]
                        </a>
                    </li>
                    <li>
                        <a href="<?=$signup_url?>" title="[HREF-TITLE-SIGN-UP]">
                            [LABEL-SIGN-UP]
                        </a>
                    </li>
                    <li>
                        <a href="<?=$request_password_reset_url?>" title="[HREF-TITLE-REQUEST-PASSWORD-RESET]">
                            [LABEL-RESET-PASSWORD]
                        </a>
                    </li>
                    <li>
                        <a href="<?=$terms_and_conditions_url?>" title="[HREF-TITLE-TERMS-AND-CONDITIONS]">
                            [LABEL-TERMS-AND-CONDITIONS]
                        </a>
                    </li> 
                    <li>
                        <a href="<?=$privacy_policy_url?>" title="[HREF-TITLE-PRIVACY-POLICY]">
                            [LABEL-PRIVACY-POLICY]
                        </a>
                    </li>
                    <li>
                        <a href="<?=$advertising_url?>" title="[HREF-TITLE-ADVERTISING]">
                            [LABEL-ADVERTISING]    
                        </a>
                    </li>
                    <li>
                        <a href="<?=$contact_us_url?>" title="[HREF-TITLE-CONTACT-US]">
                            [LABEL-CONTACT-US]
                        </a>
                    </li>
                    <li>
                        <a href="<?=$rss_url?>" title="[HREF-TITLE-RSS]">
                            RSS
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>