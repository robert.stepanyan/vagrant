<nav id="breadcrumb" class="mb-30">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li> Privacy Policy</li>
    </ul>
</nav> 
<h2 class="stylized-title custom"><span>Privacy Policy</span></h2>
<div class="wrapper_content">
    <div class="content_box staticpage">
        <div class="content_box_body"> 
            <p>This document details important information regarding the use and disclosure of User Data collected at <?=\Core\Utils\Config::getConfigValue('\Application\Settings::hostname')?> (Humpchies).</p>
            <p>The security of your Data is very important to Humpchies and as such we take all appropriate steps to limit the risk that it may be lost, damaged or misused.</p>
            <p>This site expressly and strictly limits its membership and/or viewing privileges to adults 18 years of age and over or having attained the age of majority in their community. All persons who do not meet its criteria are strictly forbidden from accessing or viewing the contents of this Site. We do not knowingly seek or collect any personal information or data from persons who have not attained the age of majority.</p>
            <h2>DATA COLLECTED</h2>
            <ul class="bullet">
                <li>Personal Information: <br>
                    <ul class="bullet">
                        <li>Non-Registered users can browse ads and profiles without registering and without any information being collected and processed. However the visitor's IP address will be recorded in the event that there is any misappropriation of information and/or content.</li>
                        <li>Registered Members: Registration is required for porsting ads, and accessing a number of other features. The following personal information is requested at the time of registration: username (required), and email address (required). Additional personal information, such as telephone number, relationship status and sexual orientation may be added on a voluntary basis after registration, for members interested in identifying and potentially contacting other members meeting specific criteria. All this data with the exception of the email address and IP address becomes publicly accessible information.</li>
                    </ul>
                </li>
                <li>Content Uploaded to the site: Any personal information, ad or profile content that you voluntarily disclose online becomes publicly available and can be collected and used by others.</li>
                <li>Cookies: When you visit Humpchies, we may send one or more cookies to your computer that uniquely identifies your browser session. Humpchies uses both session cookies and persistent cookies. If you remove your persistent cookie, some of the site's features may not function properly.</li>
                <li>Log File Information: When you visit Humpchies, our servers automatically record certain information that your web browser sends such as your web request, IP address, browser type, browser language, referring URL, platform type, domain names and the date and time of your request.</li>
                <li>Emails: If you contact us, we may keep a record of that correspondence. </li>
            </ul>
            <h2>USES</h2>
            <ul class="bullet">
                <li>Your Personally identifiable information submitted to Humpchies is used to provide to the user the website's features and special personalized features. </li>
                <li>Your chosen username (not your email address) is displayed to other Users alongside the content you upload, including ads, profiles, comments, the messages you send through the Humpchies private mail, etc. Other Users can contact you through, private messages. </li>
                <li>Any ads or profiles that you submit to Humpchies may be redistributed through the internet and other media channels, and may be viewed by the general public. </li>
                <li>We do not use your email address or other personally identifiable information to send commercial or marketing messages without your consent. </li>
                <li>We may use your email address without further consent for non-marketing or administrative purposes (such as notifying you of key website changes or for customer service purposes). </li>
                <li>OPT-IN AND USER COMMUNICATION - Subscriber's expressly and specifically acknowledges and agrees that his email address or other means of communicating with subscriber may be used to send him offers, information or any other commercially oriented emails or other means of communications. More specifically, some offers may be presented to the subscriber via email campaigns or other means of communications with the option to express the subscriber's preference by either clicking or entering "accept" (alternatively "yes") or "decline" (alternatively "no"). By selecting or clicking the "accept" or "yes", the subscriber indicates that the subscriber "OPTS-IN" to that offer and thereby agrees and assents that the subscriber's personal information, including its email address and data may be used for that matter or disclosed to third-parties."</li>
                <li>OPT-OUT AND USER COMMUNICATION - Subscriber's expressly and specifically acknowledges and agrees that his email address or other means of communicating with subscriber may be used to send him offers, information or any other commercially oriented emails or other means of communications. More specifically, other offers may be presented to the subscriber via email campaigns or other means of communications with a pre-selected preference or choice. If the subscriber does not deselect the pre-selected preference of choice (i.e. "OPT-OUT" of the offer) then the site may transfer the subscriber's personal profile information to the third-party service or content provider making the offer. If the subscriber deselects the pre-selected preference then no personal information about the subscriber may be disclosed to any third-party service or content provider.</li>
                <li>We analyze aggregated user traffic information to help streamline our marketing and hosting operations and to improve the quality of the Humpchies user-experience. </li>
            </ul>
            <h2>DISCLOSURE OF INFORMATION</h2>
            <ul class="bullet">
                <li>if under duty to do so Humpchies may release data to comply with any legal obligation, or in order to enforce our Terms Of Service and other agreements; or to protect the rights, property or safety of Humpchies or our subscribers or others. This includes exchanging information with other companies and organizations including the police and governmental authorities for the purposes of protection against fraud or any other kind of illegal activity whether or not identified in the Terms Of Service. It is Humpchies' policy, whenever possible and legally permissible, to promptly notify you upon an obligation to supply data to any third party. </li>
                <li>Should you deliberately upload any illegal material Humpchies shall forward all available information to all relevant authorities  and this without notice. We do not share your personally identifiable information (such as name or email address) with other, third-party companies for their commercial or marketing use without your consent or except as part of a specific program or feature for which you will have the ability to opt-in or opt-out. </li>
            </ul>
            <h2>SECURITY</h2>
            <p>Where we have given you (or where you have chosen a password) which enables you to access certain parts of our Site, you are responsible for keeping this password confidential. We ask you not to share your password with anyone. </p>
            <p>Unfortunately, the transmission of information via the Internet is not completely secure. Humpchies uses commercially reasonable physical, managerial and technical safeguards to preserve the integrity and security of your personal information. We cannot, however, ensure or warrant the security of any information you transmit to Humpchies and you do so at your own risk. </p>
            <h2>YOUR RIGHTS</h2>
            <p>You are entitled to access and correct your Data by doing so directly on the website or by requesting us to do so via the Contact us section. If you have additional questions then please write to us at webmaster@<?=\Core\Utils\Config::getConfigValue('\Application\Settings::hostname')?>.</p>
        </div>
    </div>
</div>