<nav id="breadcrumb" class="mb-30">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li> Advertising</li>
    </ul>
</nav> 
<h2 class="stylized-title custom"><span>Advertising</span></h2>

<div class="wrapper_content">
    <div class="content_box staticpage">
        <div class="content_box_body"> 
            <h2>
                Don't miss out!
            </h2>
            <p>
                Humpchies is growing fast! We are currently serving several million page views a month. Our site is optimized to help our users find what they are looking for faster than any other related Website. Users of our site, average 17 page views over a period of 10 minutes per visit. What does this mean to you? Opportunity!
            </p>
            <h2>
                How does it work?
            </h2>
            <p>
                Things are pretty simple with us. Nevertheless, first things first, Humpchies is 100% FREE!! That's right! FREE, Zero, Rien, Nada! You can post as many ads as you want, whenever you want!! You can stop them and repost them as much as you like. Here is what you need to do to start using Humpchies:
            </p>
                    <ul class="bullet">
                        <li>
                            <a title="Sign-up to Humpchies" href="<?=\Core\Utils\Navigation::urlFor('user', 'signup')?>">Sign-up</a>&nbsp;for a FREE account
                        </li>
                        <li>
                            An email will follow after you sign-up. Follow the instructions in it to activate your account.
                        </li>
                        <li>
                            Once your account has been activated,&nbsp;<a title="Login to Humpchies" href="<?=\Core\Utils\Navigation::urlFor('user', 'login')?>">Login</a>&nbsp;to Humpchies.
                        </li>
                        <li>
                            Click on the&nbsp;<a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>">My Ads</a>&nbsp;link at the top to start using Humpchies. Or simply click on the&nbsp;<a title="Post New Ad on Humpchies" href="<?=\Core\Utils\Navigation::urlFor('ad', 'create')?>">Post New Ad [ + ]</a>&nbsp;link to quickly post an new ad on Humpchies.
                        </li>
                    </ul>
            <p>    Whether you use a Personal Computer, a Mac, and iPhone, a Tablet or any other device. All these features are available from your favorite device, 24/7/365, always fast and most importantly 100% FREE. 
            </p>
            <h2>
                What about a banner?
            </h2>
            <p>
                Banners are great! Don't have one? We'll make you one... FREE! Currently our prime spots are taken. But we can offer you 300x60 spaces on the right column of the PC Website. A better opportunity are the 300x250 spaces available in the header and footer on Mobile.&nbsp;<a href="/contact-us" title="Contact us">Contact us</a>&nbsp;for more information. 
            </p>
            <p>
                Cheers!
            </p>
            <br><br>
            <p>
                The Webmaster
            </p>
        </div>
    </div>
</div>