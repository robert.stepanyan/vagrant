<nav id="breadcrumb" class="mb-30">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li> Publicit&eacute;</li>
    </ul>
</nav> 
<h2 class="stylized-title custom"><span>Publicit&eacute;</span></h2>

<div class="wrapper_content">
    <div class="content_box staticpage">
        <div class="content_box_body"> 
            <h2>
                Ne manquez pas le train!
            </h2>
            <p>
                Humpchies est en pleine croissance! Nous avons plusieurs millions de pages vues par mois. Notre site est optimis&eacute; pour aider nos utilisateurs &agrave; trouver ce qu'ils cherchent plus rapidement que tout autre site Web. Les utilisateurs de notre site, regardent en moyenne 17 pages par visite pour une dur&eacute;e de 10 minutes. Qu'est-ce que cela signifie pour vous? Une opportunit&eacute;!
            </p>
            <h2>
                Comment &ccedil;a marche?
            </h2>
            <p>
                Les choses sont assez simples avec nous. N&eacute;anmoins, le plus important , Humpchies est 100% GRATUIT !! C`est &ccedil;a! GRATUIT, Zero, Rien, Nada! Vous pouvez poster autant d'annonces que vous voulez, quand vous voulez !! Vous pouvez les arr&ecirc;ter et reposter autant que vous le souhaitez. Voici ce que vous devez faire pour commencer &agrave; utiliser Humpchies:
            </p>
                    <ul class="bullet">
                        <li>
                             <a title="Inscrivez-vous sur Humpchies" href="<?=\Core\Utils\Navigation::urlFor('user', 'signup')?>">Inscrivez-vous</a>&nbsp;pour un compte GRATUIT
                        </li>
                        <li>
                            Un courriel vous sera envoy&eacute; apr&egrave;s votre inscription. Suivez les instructions pour activer votre compte.
                        </li>
                        <li>
                             Une fois que votre compte a &eacute;t&eacute; activ&eacute;,&nbsp;<a title="Connectez-vous sur Humpchies" href="<?=\Core\Utils\Navigation::urlFor('user', 'login')?>">Connectez-vous</a>&nbsp;sur Humpchies.
                        </li>
                        <li>
                            Cliquez sur le lien&nbsp;<a title="Mes annonces sur Humpchies" href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>">Mes annonces</a>&nbsp;en haut pour commencer &agrave; utiliser Humpchies. Ou cliquez simplement sur le lien&nbsp;<a title="Ajouter une annonce" href="<?=\Core\Utils\Navigation::urlFor('ad', 'create')?>">Ajouter une annonce [ + ]</a>&nbsp;pour ajouter rapidement une nouvelle annonce sur Humpchies.
                        </li>
                    </ul>
            <p>        Que vous utilisiez un ordinateur personnel, un Mac, iPhone, une tablette ou tout autre appareil. Toutes ces fonctionnalit&eacute;s sont disponibles &agrave; partir de votre appareil pr&eacute;f&eacute;r&eacute;, 24/7/365, toujours rapide et surtout 100% GRATUIT.
            </p>
            <h2>
                Que diriez-vous d`une banni&egrave;re?
            </h2>
            <p>
                Les banni&egrave;res sont super! Vous n`en avez pas une? Nous vous ferons une ... GRATUIT! Actuellement, nos endroits privil&eacute;gi&eacute;s sont d&eacute;j&agrave; sous contrat mais nous pouvons vous offrir des emplacements VIP, ou des emplacements pour banni&egrave;res 300x60 sur la colonne de droite du site Web sur l` interface PC. Une meilleure occasion sont les emplacements pour banni&egrave;re 300x250 dans l'en-t&ecirc;te et pied de page sur Mobile.&nbsp;<a title="Contactez-nous" href="<?=\Core\Utils\Navigation::urlFor('contact-us')?>">Contactez-nous</a>&nbsp;pour plus d'informations.
            </p>
            <p>
                Merci!
            </p>
            <br><br>
            <p>
                Le Webmestre
            </p>
        </div>
    </div>
</div>