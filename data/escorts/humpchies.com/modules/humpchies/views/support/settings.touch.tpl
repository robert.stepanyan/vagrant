<div class="wrapper_content_full">
    <div class="" id="change_pass">
        <h1>Settings</h1>
        <div class="checkbox-container">
            <input type="checkbox" id="disable_comments" name="disable_comments" value="1">
            <label for="disable_comments">Block comments</label>
        </div>
        <form id="reset-pass" class="ticket-form" action="" method="post" enctype="multipart/form-data">
            <div class="field">
                <div>
                    <label for="current_password">Current Password <span class="req">*</span></label>
                    <input id="current_password" class="text" type="password" name="current_password" value="">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="field">
                <div>
                    <label for="new_password">New Password <span class="req">*</span></label>
                    <input id="new_password" class="text" type="password" name="new_password" value="">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="field">
                <div>
                    <label for="confirm_password">Confirm New Password <span class="req">*</span></label>
                    <input id="confirm_password" class="text" type="password" name="confirm_password" value="">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="field actions">
                <button type="submit" class="button">Change password</button>
            </div>
        </form>
    </div>
</div>