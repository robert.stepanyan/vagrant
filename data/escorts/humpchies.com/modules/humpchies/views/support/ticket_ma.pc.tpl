<?php

$issue = $ticket['issue'];
$attachedFiles = $ticket['attach'];

?>

<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li> <a href="<?=\Core\Utils\Navigation::urlFor('support')?>">[LABEL-SUPPORT]</a></li>
        <li>[LABEL-TICKET-NUMBER]: #<?= $issue['id']; ?></li>
    </ul>    
</nav>
<h2 class="stylized-title"><span>[LABEL-TICKET-NUMBER]: #<?= $issue['id']; ?></span></h2>  

<div class="wrapper_content_full">
    <div class="govazds-wrapper">
        <div class="content_box_body"  style="margin: 25px 0;">
            <div class="row clearfix">
                <div id="chat" class="tab-content active">
                    <ul class="tickets table">
                        <li class="heading">
                            <div class="row-f">
                                <p class="col-3 mb-20">Date: <?= $issue['creation_date']?></p>
                                <p class="col-3 mb-20" style="text-align: right;">[LABEL-TICKET-NUMBER]: #<?= $issue['id']; ?></p>
                                <p class="col-12 mb-10"><span class="strong">[LABEL-SUBJECT]:</span><?= $issue['subject']; ?></p>
                            </div>
                            <div class="col-12 mb-10">
                                <span class="strong">[LABEL-MESSAGE]:</span>
                                <?= $issue['message']?>
                            </div>
                            <?php if(!empty($attachedFiles)) : ?>
                                <?php foreach($attachedFiles as $attached) : ?>
                                    <div class="row-f">
                                        <p class="plr-20"><a href="<?=$attached['url']?>" target="_blank"><?= $attached['file_name']?></a></p>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </li>
                       
                    </ul>
                    <div style="height: 20px"></div>
                    <div id="ticketComments">
                        <?php if(!empty($ticketComments)) : ?>
                            <?php foreach($ticketComments as $ticketComment): ?>
                                <ul class="tickets <?=($ticketComment['bu_username'] ? 'by-admin' : 'by-user' )?>">
                                    <li class="heading">
                                        <div class="row-f">
                                            <p><?= $ticketComment['comment']?></p>
                                        </div>
                                        <?php if(!empty($ticketComment['attached_files'])) : ?>
                                            <div class="row-f attach-container">
                                                <?php foreach($ticketComment['attached_files'] as $attached) : ?>
                                                    <p>
                                                        <a href="<?=$attached['url']?>" target="_blank"><?= $attached['file_name']?></a>
                                                    </p>
                                                <?php endforeach; ?>
                                            </div>
                                        <?php endif; ?>
                                    </li>
                                    <li class="no-padding">
                                            <i class="author"> <?= $ticketComment['comment_date']?></i>
                                    </li>
                                </ul>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <p>[LABEL-NO-COMMENTS-YET]</p>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if($issue['status'] == \Modules\Humpchies\SupportModel::STATUS_TICKET_OPENED): ?>
        <div id="ticket-form-a">
            <p id="error-msg" class="error hidden"></p>
            <form action="<?=\Core\Utils\Navigation::urlFor('support', 'reply')?>" class="ticket-form" id="add-replay" enctype="multipart/form-data" method="post">
                <input type="hidden" name="ticket_id" id="ticket_id" value="<?= $issue['id']?>">
                <div class="field no-overflow">
                    <div class="send-container no-overflow">
                        <div id="custom-fileinput">
                            <span class="lbl"><i class="fa fa-paperclip" aria-hidden="true"></i></span>
                            <div class="file-preview">
                                <div class="image"></div>
                                <div class="file-remove"><i class="fa fa-trash" aria-hidden="true"></i></div>
                            </div>
                    </div>
                        <textarea id="message" class="text chat" name="message"></textarea>
                        <button type="submit" class="send-message"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                        <input type="file" name="attached[]" id="attached" multiple>
                </div>
                </div>
            </form>
            <div class="lds-dual-ring-cntr"><div class="lds-dual-ring"></div></div>
        </div>
    <?php endif; ?>
</div>