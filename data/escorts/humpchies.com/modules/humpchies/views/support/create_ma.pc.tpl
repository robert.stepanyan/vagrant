<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li> <a href="<?=\Core\Utils\Navigation::urlFor('support')?>">[LABEL-SUPPORT]</a></li>
        <li>[LABEL-NEW-TICKET]</li>
    </ul>    
</nav>
<h2 class="stylized-title"><span>[LABEL-NEW-TICKET]</span></h2> 

<div class="wrapper_content_full">
    <div class="content_box" style="margin: 25px 0;">
    <div class="" id="ticket-form-a" style="position: relative;">
            <p style="font-size: 15px;">
                [LABEL-SUPPORT-DESC]
        </p>
        <form class="ticket-form" action="" method="post" enctype="multipart/form-data">
            <div class="field">
                <div>
                        <label for="subject">[LABEL-SUBJECT] <span class="req">*</span></label>
                    <input id="subject" class="text" type="text" name="subject" value="">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="field">
                <div>
                        <label for="message">[LABEL-WRITE-MESSAGE] <span class="req">*</span></label>
                        <textarea id="message" class="text" name="message" rows="4" style="min-height: 100px; height: auto;"></textarea>
                </div>
            </div>
            <div class="field actions">
                <div id="custom-fileinput" class="custom-fileinput clearfix">
                    <span class="lbl">[LABEL-CHOOSE-FILES]</span>
                    <input type="text" disabled placeholder="[LABEL-NO-FILES-CHO0SEN]" class="lbl-placeholder" title="">
                </div>
                <input type="file" name="attached[]" id="attached" multiple>

                    <button type="submit" class="button">[LABEL-NEW]</button>
            </div>
        </form>


        <div class="bodky-medium"></div>
    </div>
    </div>
</div>