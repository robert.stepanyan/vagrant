<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li>[LABEL-SUPPORT]</li>
    </ul>    
</nav>
<div class="wrapper_content_full" style="margin-bottom: 56px;">
    <div class="govazds-wrapper">
        <div class="support-tabs">
            <a class="tab active" href="#open-tickets">
                <div><span>[LABEL-OPEN-TICKETS]</span></div>
            </a>
            <a class="tab disabled" href="#closed-tickets">
                <div><span>[LABEL-CLOSED-TICKETS]</span></div>
            </a>
        </div>
        <div class="content_box_body">
            <div class="row clearfix">
                <div class="tab-content active" id="open-tickets">
                    <ul class="tickets table">
                        <?php if(empty($openTickets)): ?>
                        <li style="font-size:15px;">[LABEL-NO-OPEN-TICKETS]</li>
                        <?php else: ?>
                        <?php foreach($openTickets as $i => $ticket) : ?>
                        <a href="<?=\Core\Utils\Navigation::urlFor('support', 'ticket', array('id'=>$ticket['id']))?>">
                            <li class="ticket-row <?php echo ($ticket['status_progress'] == 3 && $ticket['is_read'] == 0)? 'new-ticket' : '';?>">
                                <div class="row-f">
                                   
                                    <div class="date"><?= $ticket['creation_date']?></div>
                                    <div class="content-details" style="max-width: calc(100vw - 173px)">
                                        <span><?php echo $ticket['subject'];?></span>
                                    </div>
                                    <div class="action">></div>
                                </div>
                            </li>
                        </a>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="tab-content" id="closed-tickets">
                    <ul class="tickets table">
                        <?php if(empty($closedTickets)): ?>
                        <li style="font-size:15px;">[LABEL-NO-CLOSED-TICKETS]</li>
                        <?php else: ?>
                        <?php foreach($closedTickets as $i => $ticket) : ?>
                        <a href="<?=\Core\Utils\Navigation::urlFor('support', 'ticket', array('id'=>$ticket['id']))?>">
                            <li class=" ticket-row">
                                <div class="row-f">
                                    <div class="date"><?= $ticket['creation_date']?></div>
                                    <div class="content-details" style="max-width: calc(100vw - 173px)">
                                        <span><?php echo $ticket['subject'];?></span>
                                    </div>
                                    <div class="action">></div>
                                </div>
                            </li>
                        </a>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div> <a class="button sticky-button" href="<?=\Core\Utils\Navigation::urlFor('support', 'create-ticket')?>" style="padding:20px 0px;"><i class="fa fa-comments"></i>&nbsp;[LABEL-NEW-TICKET]</a>