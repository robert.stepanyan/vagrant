<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li>[LABEL-SUPPORT]</li>
    </ul>    
</nav>

<div class="wrapper_content_full">
    <div class="govazds-wrapper">
        <div class="content_box_header">
            <h2 style="font-size: 16px; font-weight: 400;">[LABEL-SUPPORT]</h2>
            <a class="button right" href="<?=\Core\Utils\Navigation::urlFor('support', 'create-ticket')?>"><i class="fa fa-comments"></i>&nbsp;[LABEL-NEW-TICKET]</a>
        </div>
        <div class="content_box_body"  style="margin: 25px 0;">
        <div class="clearfix mb-10">
                
        </div>
        <div class="support-tabs">
            <a class="tab active" href="#open-tickets">
                <div><span>[LABEL-OPEN-TICKETS]</span></div>
            </a>
            <a class="tab disabled" href="#closed-tickets">
                <div><span>[LABEL-CLOSED-TICKETS]</span></div>
            </a>
        </div>
        <div class="content_box_body" style="padding: 10px 0 20px 0">
            <div class="row clearfix">
                <div class="tab-content active" id="open-tickets">
                    <ul class="tickets table">
                        <?php if(empty($openTickets)): ?>
                                <li style="font-size:15px;">[LABEL-NO-OPEN-TICKETS]</li>
                        <?php else: ?>
                            <?php foreach($openTickets as $i => $ticket) : ?>
                                <a href="<?=\Core\Utils\Navigation::urlFor('support', 'ticket', array('id'=>$ticket['id']))?>">
                                        <li class="ticket-row <?= ( $ticket['status_progress'] == 3 && $ticket['is_read'] == 0 ? 'new-ticket' : '' ) ?>">
                                        <div class="row-f">
                                                <div class="date"><?= $ticket['creation_date']?></div>
                                                <div class="content-details">
                                                    <span><?= $ticket['subject']?></span>
                                                    <span class="action">[LABEL-FILTER-ADS]</span> 
                                            </div>
                                                
                                        </div>
                                    </li>
                                </a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="tab-content" id="closed-tickets">
                    <ul class="tickets table">
                        <?php if(empty($closedTickets)): ?>
                                <li style="font-size:15px;">[LABEL-NO-CLOSED-TICKETS]</li>
                        <?php else: ?>
                            <?php foreach($closedTickets as $i => $ticket) : ?>
                                <a href="<?=\Core\Utils\Navigation::urlFor('support', 'ticket', array('id'=>$ticket['id']))?>">
                                    <li class="ticket-row">
                                        <div class="row-f">
                                                <div class="date"><?= $ticket['creation_date']?></div>
                                                <div class="content-details">
                                                    <span><?= $ticket['subject']?></span>
                                                    <span class="action">[LABEL-FILTER-ADS]</span> 
                                                </div>
                                                
                                        </div>
                                    </li>
                                </a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>