<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents the site's 'Empty Layout' header.
 */
?>
<!DOCTYPE html>
<html id="humpchies" lang="<?=\Core\Utils\Session::getLocale()?>">
    <head>
        <?php require_once(\Core\View::getFullyQualifiedViewPath('Modules\Humpchies\Google::tracking_js', \Core\View::MODELESS));?>
        <title><?=\Core\Controller::getTitle()?></title>
        <meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
        <link rel="shortcut icon" href="<?=Modules\Humpchies\Utils::getCDNPath('images')?>favicon.ico">
        <link media="all" rel="stylesheet" type="text/css" href="<?=Modules\Humpchies\Utils::getCDNPath('css')?>pc/default2.css">
        <?=\Core\Controller::getExtraHead()?>
    </head>
    <body>
        <div id="layout">
            <div id="header">
                <div id="header_logo_block">
                    <a title="[HREF-TITLE-ROOT]" href="/">
                        <img width="300" height="120" alt="Logo - Humpchies" src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>pc/humpchies_logo.png">
                    </a>
                </div>
                <div id="header_user_block">
                    &nbsp;
                </div>
                <div id="header_menu_block">
                    &nbsp;    
                </div>
            </div>
            <div id="body">
            <?php require_once(\Core\View::getFullyQualifiedViewPath('\Core\Default::systemMessages', \Core\View::MODELESS))?>