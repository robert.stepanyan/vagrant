<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents the site's 'Empty Layout' Mobile header.
 */
?>
<!DOCTYPE html>
<html id="humpchies" lang="<?=\Core\Utils\Session::getLocale()?>">
    <head>
        <?php require_once(\Core\View::getFullyQualifiedViewPath('Modules\Humpchies\Google::tracking_js', \Core\View::MODELESS));?>
        <title><?=\Core\Controller::getTitle()?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
        <link rel="shortcut icon" href="<?=\Modules\Humpchies\Utils::getCDNPath('images')?>favicon.ico">
        <link href="<?=\Modules\Humpchies\Utils::getCDNPath('css')?>mobile/touch/default3.css" rel="stylesheet" type="text/css" media="all" />
        <?=\Core\Controller::getExtraHead()?>
    </head>
<body>
    <div id="layout">
        <div id="header">
            <div class="wrapper">
                <div id="header_logo_block">
                    <a href="/" title="[HREF-TITLE-ROOT]">
                        <img src="<?=\Modules\Humpchies\Utils::getCDNPath('images')?>pc/humpchies_logo.png" width="150" height="60" alt="Logo - Humpchies" />
                    </a>
                </div>
            </div>
            <?php require_once(\Core\View::getFullyQualifiedViewPath('\Core\Default::systemMessages', \Core\View::MODELESS))?>
        </div>
        <div id="body">