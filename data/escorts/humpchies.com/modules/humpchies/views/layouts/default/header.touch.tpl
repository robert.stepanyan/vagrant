<?php
// Grag the page's language options.... 
$language_options = \Core\Utils\Session::getLocaleOptions();
?>

<!DOCTYPE html>
<html id="humpchies" lang="<?=\Core\Utils\Session::getLocale()?>">
<head>

    <?php
        $city = !empty($_GET['city']) ? $_GET['city'] : '';
        $city_ttl = ucfirst($city);
        if(!empty($city_ttl)) {
            $meta_description = \Core\Controller::dynamicTerms(\Core\Utils\Session::getLocale(), 'DESCRIPTION-DEFAULT');
            $meta_description =  \Core\Controller::doTranslate($meta_description, $city_ttl);
        } else {
            $meta_description = \Core\Controller::getDescription();
            $city_ttl = 'Quebec';
        }
    ?>
    <?php require_once(\Core\View::getFullyQualifiedViewPath('Modules\Humpchies\Google::tracking_js', \Core\View::MODELESS));?>
    <?php $current_page = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
    <title><?=\Core\Controller::getTitle()?></title>
    <meta name="description" content="<?=$meta_description?>" />
    <link rel="alternate" href="<?=\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname') . $_SERVER['REQUEST_URI']?>" hreflang="<?=\Core\Utils\Session::getLocale()?>" />
    <link rel="alternate" href="<?=$language_options[0][1]?>" hreflang="<?=$language_options[0][3]?>" />
    <meta name="RATING" content="RTA-5042-1996-1400-1577-RTA" />
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="alternate" type="application/rss+xml" title="[HREF-TITLE-RSS]" href="/rss">
    <meta property="og:title" content="<?=\Core\Controller::getTitle()?>" >
    <meta property="og:description" content="<?=\Core\Controller::getDescription()?>">
    <meta property="og:site_name" content="Humpchies">
    <meta property="og:url" content="<?=$current_page;?>" />
    <meta property="og:country-name" content="Canada">
    <?php
        if( isset($ad) && is_array( $ad ) ){
        
        } else { ?>
    <meta property="og:image" content="https://d1d6tr4qe1zigi.cloudfront.net/images/pc/popup.jpg" />
        <?php }
    ?>
    <meta property="og:type" content="website">
    <link rel="dns-prefetch" href="//www.google-analytics.com">
    <link rel="dns-prefetch" href="<?=Modules\Humpchies\Utils::getCDNPath('')?>">
    <link rel="dns-prefetch" href="<?=\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_escort')?>">
    <link rel="dns-prefetch" href="<?=\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_escort-agency')?>">
    <link rel="dns-prefetch" href="<?=\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_erotic-massage')?>">
    <link rel="dns-prefetch" href="<?=\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_erotic-massage-parlor')?>">
    <link rel="dns-prefetch" href="//adserver.juicyads.com">
    <link rel="dns-prefetch" href="//js.juicyads.com">
    <link rel="dns-prefetch" href="//maxcdn.bootstrapcdn.com">
    <link rel="shortcut icon" href="<?=\Modules\Humpchies\Utils::getCDNPath('images')?>favicon.ico">
    
    <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
    
    <?php if(isset($new_header) && $new_header):?>
        <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/font-awesome-line-awesome/css/all.min.css">
        
        <link media="all" rel="stylesheet" type="text/css" href="<?=Modules\Humpchies\Utils::getCDNPath('css')?>mobile/touch/default_v3.css?v=1.14">
    <?php else:?>
        <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
        <link media="all" rel="stylesheet" type="text/css" href="<?=Modules\Humpchies\Utils::getCDNPath('css')?>mobile/touch/default_new.css?v=1.16">
    <?php endif;?>
    
    <link media="all" rel="stylesheet" type="text/css" href="<?=Modules\Humpchies\Utils::getCDNPath('css')?>modal.css?v=3.13">
    <link media="all" rel="stylesheet" type="text/css" href="<?=Modules\Humpchies\Utils::getCDNPath('css')?>noty.css?v=3.1.4">
    
    <script type="text/javascript" src="<?=\Modules\Humpchies\Utils::getCDNPath('js')?>jquery/jquery-1.11.2.min.js"></script>
    <?=\Core\Controller::getExtraHead()?>
    <?php require_once(\Core\View::getFullyQualifiedViewPath('Modules\Humpchies\Pop::under', \Core\View::MODELESS));?>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fingerprintjs2/2.1.0/fingerprint2.js"></script>   
    <script async type="text/javascript" src="<?=\Modules\Humpchies\Utils::getCDNPath('js')?>mobile/touch/default_new.js?v=1.12"></script>
    <script type="text/javascript" src="<?=\Modules\Humpchies\Utils::getCDNPath('js')?>noty.min.js?v=3.1.4"></script>
<style>
    .full_height {
        min-height: 100%;
        height: auto;
    }      
</style>     
</head>
<body>
    <div id="layout" class="<?php echo \Core\Controller::getHeaderClass();?> <?php echo (isset($_COOKIE['Sfb']) && $_COOKIE['Sfb'] == 1)? 'safe_browsing' : '' ;?>">
        <div id="header" class="<?php echo (\Core\Utils\Session::currentUserIsMember())? 'member': '';?>">
            <div class="wrapper" id="header_wrapper">
                <section  class="right-menu-container" id="mainMenu">
                    <div class="header-box">
                        <a class="open-button" id="showRight">
                            <i class="fa fa-bars"></i>
                            <i class="fa fa-times-circle"></i>
                            <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() !== FALSE) : ?>
                                <?php $unreadMessages = \Modules\Humpchies\MessageModel::countUnreadMessages(\Core\Utils\Session::getCurrentUserID()); ?>
                                <?php if ($unreadMessages) : ?>
                                    <span class="unreadMessages"><?=$unreadMessages?></span>
                                <?php endif; ?>
                            <?php endif; ?>
                        </a>
                        
                        <a href="<?=\Core\Utils\Navigation::urlFor('/')?>" class="hump_logo">
                            <img alt="Logo - Humpchies" src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>humpchies-logo.svg" />
                        </a>
                        
                        <div id="search-button">
                            <i class="fa fa-search"></i>
                            <i class="fa fa-times-circle"></i>
                        </div>
                    </div>
                    
                    <nav class="wrapper regular-position" id="right-menu">
                        
                        <article class="btns account">
                            <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE):?>
                                <a title="[HREF-TITLE-SIGN-UP]" href="<?=\Core\Utils\Navigation::urlFor('user', 'signup')?>"><i class="fa fa-sign-in"></i>[LABEL-SIGN-UP]</a>
                                <a title="[HREF-TITLE-LOGIN]" href="<?=\Core\Utils\Navigation::urlFor('user', 'login')?>"><i class="fa fa-user-plus"></i>[LABEL-LOGIN]</a>
                            <?php endif;?>
                            <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() !== FALSE) : ?>
                            
                                <?php if(\Core\Utils\Session::currentUserIsAdvertisment()):?>
                                    <a href="<?= \Core\Utils\Navigation::urlFor('ad', (\Core\Utils\Session::getCurrentUserID() == 63661) ? 'create-v2' : 'create')?>" class="full"><i class="fa fa-plus-circle"></i>[LABEL-POST-AD]</a>
                                <?php endif; ?>
                                <a href="javascript:void(0)" id="open-account"><i class="fa fa-user"></i>[LABEL-ACCOUNT]</a>
                                <a title="[HREF-TITLE-MY-CHAT]" href="<?=\Core\Utils\Navigation::urlFor('chat', 'view')?>" style="position:relative">
                                    <i class="fa fa-envelope"></i>
                                    [LABEL-MY-CHAT]
                                    <?php if ($unreadMessages) : ?>
                                    <span class="notice"><?=$unreadMessages?></span>
                                    <?php endif; ?>
                                </a>
                                
                            <?php endif;?>     
                        </article>
                        
                        <div class="account-menu">
                            <article class="btns">
                                <? if(\Core\Utils\Session::getCurrentUserID() !== 63661 && \Core\Utils\Session::currentUserIsAdvertisment()): ?>
                                    <a href="<?=\Core\Utils\Navigation::urlFor('payment', 'bumps-purchase')?>" class="full">
                                        <i class="fa fa-cart-plus"></i>
                                        [LABEL-BUY-MORE-CREDITS]
                                    </a>
                                <? endif ?>
                                <?php if(\Core\Utils\Session::currentUserIsAdvertisment()) :?>
                                    <a title="[HREF-TITLE-MY-ADS]" href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>"><i class="fa fa-folder-open"></i>[LABEL-MY-ADS]</a>
                                <?php endif;?>
                                
                                <a title="[HREF-TITLE-MY-SETTINGS]" href="<?=\Core\Utils\Navigation::urlFor('user', 'settings')?>"><i class="fa fa-cog"></i>[LABEL-SETTINGS]</a>
                                
                                <?php if(\Core\Utils\Session::currentUserIsMember()) :?>
                                    <a title="[TITLE-FAVORITES]" href="<?=\Core\Utils\Navigation::urlFor('favorites', 'listing')?>"><i class="fa fa-heart"></i>[TITLE-FAVORITES]</a>
                                <?php endif; ?>
                                
                                <?php if(\Core\Utils\Session::currentUserIsAdvertisment()) :?>
                                    <a title="[LABEL-BLACKLIST]" href="<?=\Core\Utils\Navigation::urlFor('user', 'blacklist')?>"><i class="fa fa-user-times"></i>[LABEL-BLACKLIST]</a>
                                <?php endif; ?>
                            
                                <a title="[LABEL-SUPPORT]" href="<?=\Core\Utils\Navigation::urlFor('support')?>"><i class="fa fa-comments"></i>[LABEL-SUPPORT]</a>

                                <a title="[HREF-TITLE-LOGOUT]" href="<?=\Core\Utils\Navigation::urlFor('user', 'logout')?>"><i class="fa fa-sign-out"></i>[LABEL-LOGOUT]</a>
                            </article>
                        </div>
                        
                        <article class="links">
                            <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'mature'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Mature'">Mature</a>
                            <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'anal'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Anal'">Anal</a>
                            <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'independante'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Independante'">Independante</a>
                            <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'milf'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'MILF'">MILF</a>
                            <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'avale'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Avale'">Avale</a>
                            <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'asiatique'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Asiatique'">Asiatique</a>
                            <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'bbw'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'BBW'">BBW</a>
                            <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'latina'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Latina'">Latina</a>
                            <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'domination'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Domination'">Domination</a>
                            <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'pipe'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Pipe'">Pipe</a>
                            <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'gros seins'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Gros Seins'">Gros Seins</a>
                            
                            
                            <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'comments')?>" title="[LABEL-COMMENTS]">[LABEL-COMMENTS]</a>  
                            <?php if(0):?>  
                            <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'verified', array(), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Gros Seins'">[LABEL-REAL-PHOTO]</a>    
                            <?php endif;?>    
                        </article>
                        
                        <article class="btns">
                            <a title="" href="<?=\Core\Utils\Navigation::urlFor('ad', 'locate')?>">
                                <i class="fa fa-map"></i>[LABEL-ESCORT] GPS
                            </a>
                            <a  href="javascript:void(0);" id="safe_browsing">[LABEL-SAFE-BROWSING]</a>
                        </article>
                        
                    
                        <article class="language" style="display: flex;align-items: flex-start;min-height: 150px;">
                             
                            <a href="https://twitter.com/HumpchiesC" target="_blank" title="Twitter" style="display: flex; align-items: center; align-content: center; color: #fff;">
                               <img src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>twitter-logo-mob.png" alt="" style="margin-right:5px;border: none;width: 33px;height: 27px;border-radius: 0;"/>
                               Twitter
                            </a>

                             
                             <?php
                                switch (\Core\Utils\Session::getLocale()) {
                                    case 'en-ca':
                                        $lang_class = 'flag-fr';
                                        break;
                                    case 'fr-ca':
                                        $lang_class = 'flag-en';
                                        break;
                                }

                                $lang_url =  (\Core\Utils\Session::getCurrentUserID())? "/user/language?" . urlencode($language_options[0][1]) : $language_options[0][1];
                             ?>   
                             <a href="<?php echo $lang_url;?>">
                                <img src="<?=Modules\Humpchies\Utils::getCDNPath('images') . $lang_class . '.png'?>" />
                                <?php echo $language_options[0][0]?>
                             </a>
                             
                        </article>
                    
                        <?php if(0):?>
                        <article class="main-nav">
                            <?php if(!\Core\Utils\Session::currentUserIsMember()){ ?>
                            <a class="new-ad" href="<?=\Core\Utils\Navigation::urlFor('ad', (\Core\Utils\Session::getCurrentUserID() == 63661) ? 'create-v2' : 'create')?>" title="[HREF-TITLE-AD-POST]">[LABEL-POST-AD]</a>
                            <?php } ?>
                            <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE){?>
                            
                            <?php }else{
                            if(!\Core\Utils\Session::currentUserIsMember()){
                            ?>
                            <a title="[HREF-TITLE-MY-ADS]" href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>">[LABEL-MY-ADS]</a>
                            <?php }}?>

                           
                            <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() !== FALSE) : ?>
                            <a title="[HREF-TITLE-MY-CHAT]" href="<?=\Core\Utils\Navigation::urlFor('chat', 'view')?>" style="position:relative">[LABEL-MY-CHAT]</a>
                            <?php endif; ?>
                            

                            <?php if(\Core\Utils\Session::getCurrentUserID() == 63661 || \Core\Utils\Session::getCurrentUserID() == 86282) : ?>
                                <a title=""  href="<?=\Core\Utils\Navigation::urlFor('support')?>">[LABEL-SUPPORT]</a>
                            <?php endif; ?>
                            
                             <?php if(\Core\Utils\Session::currentUserIsAdvertisment()) :?>
                                <a title="[LABEL-BLACKLIST-CLIENTS]" href="<?=\Core\Utils\Navigation::urlFor('user', 'blacklist')?>">[LABEL-BLACKLIST-CLIENTS]</a>
                                <?php endif; ?>    
                            <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE){?>
                            <?php }else{?>
                            <?php $userInfo = \Core\Utils\Session::getAuthenticatedSessionValues(); ?>
                            
                            <a title="[HREF-TITLE-MY-SETTINGS]" href="<?=\Core\Utils\Navigation::urlFor('user', 'settings')?>">[LABEL-SETTINGS]</a>
                            <a title="[HREF-TITLE-LOGOUT]" href="<?=\Core\Utils\Navigation::urlFor('user', 'logout')?>">[LABEL-LOGOUT]&nbsp;(<?php echo $userInfo['username'];?>)</a>
                            <?php }?>       
                              
                            
                        </article>
                        <?php endif;?>

                                          
                    </nav>
                   
                </section>
            </div>
            

            

            <?php
                require_once(\Core\View::getFullyQualifiedViewPath('\Core\Default::systemMessages', \Core\View::MODELESS));
                
               
            ?>
        </div>
<div id="search-form">
    <form id="header_search" method="get" action="<?=\Core\Utils\Navigation::urlFor('ad', 'search')?>">
        
        <div class="search-suggest">
            <div class="form-label-group">
                <input type="text" id="search_city_header" name="search_city_name" class="form-control" value="" placeholder="[INPUT-SEARCH-CITY]" />
                <label for="search_city_header">[LABEL-SELECT]&nbsp;[LOCATION-LABEL]</label>
            </div>
            <input type="hidden" name="category_search_city" id="category_search_city" value="<?php echo (isset($category_code)? $category_code : '');?>">
            <ul id="suggesstion-search-city" class="city_select city"></ul>
        </div>
        <input type="hidden" name="city" id="search_city_slug" value="" />
        <div class="form-label-group keyword">
            <input type="text" id="search_city" name="query" class="form-control" value="" />
            <label for="search_city">[LABEL-SEARCH-TXT]</label>
        </div>
        
    </form> 
</div>        
<?php if(isset($cities_with_zones) && $cities_with_zones != null):?>
<div id="cities_zones" class="modal_cities">

    <!-- Modal content -->
    <div class="modal-content_cities">
        <div class="modal-header_cities">
            <span class="close_report">&times;</span>
            <div style="clear:both"></div>
            <h2>[LABEL-SEARCH-CITY]</h2>
            <input type="text" class="form-control" id="search-box" placeholder="[INPUT-SEARCH-CITY]" />
            <input type="hidden" name="category_code" id="category_code" value="<?php echo $category_code;?>" />
            <ul id="suggesstion-box" class="city_select city"></ul>
            
            <div style="clear:both; height:15px;"></div>
            <h2>[LABEL-SELECT-CITY]</h2>
        </div>
        <div class="modal-body_cities">
             <div>
             <ul class="city_select area">
                <li>
                    <a href="#" class="selected_area"><?php echo $cities_with_zones[$current_area_code]['region']["name"];?></a> <!-- area name -->
                    <i class="fa fa-chevron-down"></i>

                    <ul class="dropdown select_area">
                        <?php foreach($cities_with_zones as $key => $cities):?>
                            <li class="<?php echo $key;?>"><?php echo $cities['region']['name'];?></li>
                        <?php endforeach;?>
                    </ul>
                </li>
             </ul>       
             </div>
             <div>
             <ul class="city_select city" data-txt="[LABEL-SELECT-CITY]">
                <li>
                    <a href="#" class="selected_city"><?php echo $current_city_name;?></a> <!-- city name -->
                    <i class="fa fa-chevron-down"></i>
                    
                    
                    <?php foreach($cities_with_zones as $key => $cities):?>
                        <ul class="dropdown cities_lists <?php echo $current_area_code == $key ? $key : $key . ' hidden';?>">
                            <li><a href="<?=\Modules\Humpchies\AdController::getRegionUrl($category_code, $key);?>">[LABEL-ALL-CITIES]</a></li>
                            <?php foreach($cities['cities'] as $newkey => $city_details): ?>
                                <li class="<?php echo $city_details['slug'];?>" data-areacode="<?php echo $key;?>" data-areaname="<?php echo $cities["region"]['name'];?>">
                                    <a href="<?=\Modules\Humpchies\AdController::getListURL($category_code, $city_details['slug'])?>" title="<?=\Modules\Humpchies\AdController::getListHrefTitle(@$category_code, $city_details['name'])?>">
                                        <?php echo $city_details['name']?>
                                    </a>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    <?php endforeach;?>
                </li>
             </ul> 
             </div>           
        </div>
        
    </div>

</div>

<?php endif;?>    

<?php if(!\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE) : ?>
    <input type="hidden" id="show_alert" name="show_alert" value="yes" />
<?php endif;?>
<div id="alert-modal" class="modal-pm-gray">
    <!-- Modal content -->
    <div class="modal-pm-content">
        
        <div class="modal-pm-header" style="padding-top: 20px;">
            <div class="fa fa-close modal-close"></div>
            <h2 style="color: #dc0000; font-size: 18px; text-align: center;">[LABEL-PHISHING-ALERT]</h2>
        </div>
        <div class="modal-pm-body" style="padding:10px 20px; font-size: 13px; line-height: 16px; text-align: left;">
            [LABEL-PHISHING-ALERT-TXT]
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left">
                <input id="modal-close" type="button" class='btn-orange modal-close' value="[LABEL-CLOSE]"/>
            </div>
        </div>
    </div>
</div>


<?php if(0 && \Core\Utils\Session::currentUserIsAdvertisment()) : ?>
    <?php 
        $startDate = strtotime("2021-10-29 00:00:00");
        $endDate = strtotime("2021-10-31 23:59:59");
        if(\Core\Utils\Session::getCurrentUserID() == 63661 || (time() >= $startDate && time() <= $endDate)): // only testme  86525
    ?>
        <input type="hidden" id="show_halloween" name="show_halloween" value="yes" />
    <?php endif;?>
<?php endif;?>


<div id="halloween-modal" class="modal-halloween">
    <!-- Modal content -->
    <div class="modal-content">
        
        <div class="modal-header">
            
            <h2>[LABEL-HALLOWEEN-TITLE]</h2>
        </div>
        <div class="modal-body">
            <ul>
                <li>
                    <i class="fa fa-check-circle"></i>
                    [HALLOWEEN-LIST-A]
                </li>
                <li>
                    <i class="fa fa-check-circle"></i>
                    [HALLOWEEN-LIST-B]
                </li>
            </ul>
            <p>[HALLOWEEN-DESCRIPTION]</p>
        </div>
        <div class="modal-footer">
            <input id="halloween-modal-close" type="button" class='btn-orange' value="[LABEL-CLOSE-HALLOWEEN]"/>
        </div>
    </div>
</div>





<div id="body" style="<?php if (!isset($parsedURL) || !isset($parsedURL->layout_settings['no_min_height']) || ($parsedURL->layout_settings['no_min_height'] != true)) : ?>min-height:500px<?php endif; ?>">
    
        <?php
         if (! \Core\Utils\Session::getCurrentUserID() && !\Core\Controller::showAds() ){
                   require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x250_1', \Core\View::DEVICE_MODE));
                }
        ?>