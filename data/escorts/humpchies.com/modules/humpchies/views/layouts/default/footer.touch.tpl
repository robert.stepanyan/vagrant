<?php

if(!\Core\Controller::showAds() && !isset($max_banner))
            require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x250_2', \Core\View::DEVICE_MODE));
\Core\Controller::renderPlugin('city_list', NULL, \Core\View::DEVICE_MODE);
$action_name  = Core\Router::getParsedURL()->public_action_name;

?>
</div>
<?php if (!isset($parsedURL) || !isset($parsedURL->layout_settings['no_footer']) || ($parsedURL->layout_settings['no_footer'] != true)) : ?>
<div id="footer">
    <div class="wrapper_container">
        
        <div class="grey-border rta-verified">
            <a href="http://rtalabel.org/?content=validate&rating=RTA-5042-1996-1400-1577-RTA&ref=<?=\Core\Utils\Config::getConfigValue('\Application\Settings::hostname')?>" target="_blank" rel="nofollow">
                <img src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>88x31_RTA-5042-1996-1400-1577-RTA-B-VERIFIED.gif" alt="[LABEL-RTA-VERIFIED]">
            </a>
        </div>
                
        <nav class="footer-menu">
            <ul>
                <li>
                    <a href="<?=\Core\Utils\Navigation::urlFor('terms-and-conditions')?>">
                        [LABEL-TERMS-AND-CONDITIONS]
                    </a>
                </li>
                <li>
                    <a href="<?=\Core\Utils\Navigation::urlFor('privacy-policy')?>">
                        [LABEL-PRIVACY-POLICY]
                    </a>
                </li>
                <li>
                    <a href="<?=\Core\Utils\Navigation::urlFor('advertising')?>" title="[HREF-TITLE-ADVERTISING]">
                        [LABEL-ADVERTISING]
                    </a>
                </li>
                <li>
                    <a href="<?=\Core\Utils\Navigation::urlFor('rss')?>" title="[HREF-TITLE-RSS]">
                        RSS
                    </a>
                </li>
                <li>
                    <a href="<?=\Core\Utils\Navigation::urlFor('contact-us')?>">
                        [LABEL-CONTACT-US]
                    </a>
                </li>
                <li>
                    <a href="<?=\Core\Utils\Navigation::urlFor('site-map')?>" title="[HREF-TITLE-SITE-MAP]">
                        [LABEL-SITE-MAP]
                    </a>
                </li>
            </ul>
        </nav>
        
        
        
        <div class="footer-text">
            <p> <?=\Core\Controller::getInPageText()?> [LABEL-FOOTER-COPYRIGHT-TEXT] <?=@date('Y')?></p>
            <p> [LABEL-PAYMENT-FOOTER-RIGHT]</p>
        </div>
        
            </div>
</div>
<?php endif; // no_footer ?>



      
        <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() !== FALSE && ($action_name == 'my-expired-listings' ||  $action_name == 'my-active-listings' || $action_name == 'my-listings' )  ): ?>
            <div id="mobile-post-id"><a href="<?=\Core\Utils\Navigation::urlFor('ad', 'create')?>">[LABEL-POST-AD]</a></div>
        <?php endif; ?>

        </div>
        <?php require_once(\Core\View::getFullyQualifiedViewPath('Modules\Humpchies\Verification::prompt', \Core\View::MODELESS));?>
        
        <script type="text/javascript" src="<?=\Modules\Humpchies\Utils::getCDNPath('js')?>social_share_counter.js"></script>
    </body>
</html>
