
<?php if (!isset($parsedURL) || !isset($parsedURL->layout_settings['no_wrapper_column']) || ($parsedURL->layout_settings['no_wrapper_column'] != true)) : ?>
<div class="wrapper_column">
                    <?php   
        // load right area banners   
        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::right_banners', \Core\View::DEVICE_MODE));
                    ?>
</div>
<?php endif; ?>
</div>
<?php require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::728x90_1', \Core\View::DEVICE_MODE));?>
<!-- start footer -->            
<div id="footer">
    
    <div class="footer-logo">
        <a title="[HREF-TITLE-ROOT]" href="<?=\Core\Utils\Navigation::urlFor('/')?>" class="hump_logo">
            <img alt="Logo - Humpchies" src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>new_logo.png">
        </a>
                </div>
    
                <div class="wrapper_container">
        
        <nav class="footer-menu">
                        <ul>
                            <li>
                                <a href="<?=\Core\Utils\Navigation::urlFor('terms-and-conditions')?>">
                                    [LABEL-TERMS-AND-CONDITIONS]
                                </a>
                            </li>
                            <li>
                                <a href="<?=\Core\Utils\Navigation::urlFor('privacy-policy')?>">
                                    [LABEL-PRIVACY-POLICY]
                                </a>
                            </li>
                            <li>
                                <a href="<?=\Core\Utils\Navigation::urlFor('advertising')?>" title="[HREF-TITLE-ADVERTISING]">
                                    [LABEL-ADVERTISING]
                                </a>
                            </li>
                            <li>
                    <a href="https://twitter.com/HumpchiesC" target="_blank" title="Twitter" style="display: flex; align-items: center; align-content: center;">
                       <img src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>twitter-logo.png" alt="" style="margin-right:5px;"/>
                       Twitter
                    </a>
                </li>
                <li>
                                <a href="<?=\Core\Utils\Navigation::urlFor('rss')?>" title="[HREF-TITLE-RSS]">
                        RSS
                                </a>
                            </li>
                            <li>
                                <a href="<?=\Core\Utils\Navigation::urlFor('contact-us')?>">
                        [LABEL-CONTACT-US]
                                </a>
                            </li>
                            <li>
                                <a href="<?=\Core\Utils\Navigation::urlFor('site-map')?>" title="[HREF-TITLE-SITE-MAP]">
                       [LABEL-SITE-MAP]
                                </a>
                            </li>
                        </ul>
        </nav>
                        
        <div class="grey-border rta-verified">
                         <a href="http://rtalabel.org/?content=validate&rating=RTA-5042-1996-1400-1577-RTA&ref=<?=\Core\Utils\Config::getConfigValue('\Application\Settings::hostname')?>" target="_blank" rel="nofollow">
                            <img src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>88x31_RTA-5042-1996-1400-1577-RTA-B-VERIFIED.gif" alt="[LABEL-RTA-VERIFIED]">
                        </a>
                    </div>
        
        <div class="footer-text">
            <p> <?=\Core\Controller::getInPageText()?> [LABEL-FOOTER-COPYRIGHT-TEXT] <?=@date('Y')?></p>
            <p> [LABEL-PAYMENT-FOOTER-RIGHT]</p>
                    </div>
        
                </div>
    
</div>
<!-- end footer -->
        
</div>
<a href="#" class="back-to-top"><i class="fa fa-arrow-circle-up"></i> Back to Top</a>
<?php require_once(\Core\View::getFullyQualifiedViewPath('Modules\Humpchies\Verification::prompt', \Core\View::MODELESS));?>
<script type="text/javascript" src="<?=\Modules\Humpchies\Utils::getCDNPath('js')?>social_share_counter.js"></script>
<script type="text/javascript">
            jQuery(document).ready(function() {
                var offset = 220;
                var duration = 500;
                jQuery(window).scroll(function() {
                    if (jQuery(this).scrollTop() > offset) {
                jQuery('.back-to-top').css('display','flex');
                    } else {
                        jQuery('.back-to-top').fadeOut(duration);
                    }
                });
                
                jQuery('.back-to-top').click(function(event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
</script>
</body>
</html>