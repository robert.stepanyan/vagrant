<!DOCTYPE html>
<html id="humpchies" lang="<?=\Core\Utils\Session::getLocale()?>">
<head>
    <?php
        $city = !empty($_GET['city']) ? $_GET['city'] : '';
        $city_ttl = ucfirst($city);
        if(!empty($city_ttl)) {
            $meta_description = \Core\Controller::dynamicTerms(\Core\Utils\Session::getLocale(), 'DESCRIPTION-DEFAULT');
            $meta_description =  \Core\Controller::doTranslate($meta_description, $city_ttl);
        } else {
            $meta_description = \Core\Controller::getDescription();
            $city_ttl = 'Quebec';
        }
    ?>
    <?php require_once(\Core\View::getFullyQualifiedViewPath('Modules\Humpchies\Google::tracking_js', \Core\View::MODELESS));?>
    <?php $current_page = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
    <title><?=\Core\Controller::getTitle()?></title>
    <meta content="<?=$meta_description?>" name="description">
    <link rel="alternate" href="<?=\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Application\Settings::hostname') . $_SERVER['REQUEST_URI']?>" hreflang="<?=\Core\Utils\Session::getLocale()?>" />
    <link rel="alternate" href="<?=$language_options[0][1]?>" hreflang="<?=$language_options[0][3]?>" />
    <meta name="RATING" content="RTA-5042-1996-1400-1577-RTA" />
    <meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
    <link rel="alternate" type="application/rss+xml" title="[HREF-TITLE-RSS]" href="/rss">
    <meta property="og:title" content="<?=\Core\Controller::getTitle()?>" >
    <meta property="og:description" content="<?=\Core\Controller::getDescription()?>">
    <meta property="og:site_name" content="Humpchies">
    <meta property="og:url" content="<?=$current_page;?>" />
    <meta property="og:country-name" content="Canada">
    <meta property="og:type" content="website">
    <?php
        if( isset($ad) && is_array( $ad ) ){
        
        } else { ?>
            <meta property="og:image" content="https://d1d6tr4qe1zigi.cloudfront.net/images/pc/popup.jpg" />
        <?php }
    ?>
    <link rel="dns-prefetch" href="//www.google-analytics.com">
    <link rel="dns-prefetch" href="<?=Modules\Humpchies\Utils::getCDNPath('')?>">
    <link rel="dns-prefetch" href="<?=\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_escort')?>">
    <link rel="dns-prefetch" href="<?=\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_escort-agency')?>">
    <link rel="dns-prefetch" href="<?=\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_erotic-massage')?>">
    <link rel="dns-prefetch" href="<?=\Core\Utils\Device::getProtocol() . \Core\Utils\Config::getConfigValue('\Modules\Humpchies\Cdn::gui_ads_erotic-massage-parlor')?>">
    <link rel="dns-prefetch" href="//adserver.juicyads.com">
    <link rel="dns-prefetch" href="//js.juicyads.com">
    <link rel="shortcut icon" href="<?=Modules\Humpchies\Utils::getCDNPath('images')?>favicon.ico">
    <link media="all" rel="stylesheet" type="text/css" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
    <link media="all" rel="stylesheet" type="text/css" href="<?=Modules\Humpchies\Utils::getCDNPath('css')?>pc/default_v2.css?v=1.08">
    <link media="all" rel="stylesheet" type="text/css" href="<?=Modules\Humpchies\Utils::getCDNPath('css')?>modal.css?v=3.13">
    
    <script type="text/javascript" src="<?=\Modules\Humpchies\Utils::getCDNPath('js')?>jquery/jquery-1.11.2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@beta/dist/js.cookie.min.js"></script>
    <?=\Core\Controller::getExtraHead()?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fingerprintjs2/2.1.0/fingerprint2.js"></script>   
    <script type="text/javascript" src="<?=\Modules\Humpchies\Utils::getCDNPath('js')?>pc/default_v2.js?v=1.08"></script>
    <style>
        .back-to-top {
            position: fixed;
            bottom: 2em;
            right: 0px;
            text-decoration: none;
            color: #000000;
            font-size: 12px;
            padding: 1em;
            display: none;
        }

        .back-to-top:hover {    
            background-color: rgba(135, 135, 135, 0.50);
        }
    </style>
</head>

<body>
<?php require_once(\Core\View::getFullyQualifiedViewPath('Modules\Humpchies\Pop::under', \Core\View::MODELESS));?>

<div id="layout" class="<?php echo (isset($_COOKIE['Sfb']) && $_COOKIE['Sfb'] == 1)? 'safe_browsing' : '' ;?>">
<div class="safe_box">
                   <p>[LABEL-SAFE-BROWSING]</p>
                   <label class="switch">
                        <input type="checkbox" id="safe_browsing" <?php echo (isset($_COOKIE['Sfb']) && $_COOKIE['Sfb'] == 1)? 'checked' : '' ;?>>
                        <span>
                            <em></em>
                            <strong></strong>
                        </span>
                    </label>
</div>    
                
    <div id="header" class="no-overflow">
                
        <div class="header-section">
            <div class="inner-section">
                <div id="header_logo_block">
                    <div class="logo-container"> 
                <a title="[HREF-TITLE-ROOT]" href="<?=\Core\Utils\Navigation::urlFor('/')?>" class="hump_logo">
                            <img alt="Logo - Humpchies" src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>new_logo.png">
                </a>
                        
                <div class="language-selection text_link">
                    <div class="lang-current">
                                <?php 
                                    switch (\Core\Utils\Session::getLocale()) {
                            case 'en-ca':
                                $lang_class = 'lang-fr';
                                break;
                            case 'fr-ca':
                                $lang_class = 'lang-en';
                                break;
                            }
                             
                                    $lang_url =  (\Core\Utils\Session::getCurrentUserID())? "/user/language?" . urlencode($language_options[0][1]) : $language_options[0][1];
                        ?>
                                <a href="<?php echo $lang_url;?>"><span class="<?php echo $lang_class ?>"></span></a>
                   </div>
                </div>
                            </div>
                         </div>
                    
            <div id="header_menu_block">
                <ul id="header_menu">
                    <li>
                        <a title="[HREF-TITLE-ESCORT-PART-1]<?=(@$city_name === NULL ? '[LABEL-MONTREAL]' : $city_name)?>" href="<?=\Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => 'escort'))?>">
                            [LABEL-ESCORT]
                        </a>
                    </li>
                    <li>
                        <a title="[HREF-TITLE-ESCORT-AGENCY-PART-1]<?=(@$city_name === NULL ? '[LABEL-MONTREAL]' : $city_name)?>" href="<?=\Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => 'escort-agency'))?>">
                            [LABEL-ESCORT-AGENCY]
                        </a>
                    </li>
                    <li>
                        <a title="[HREF-TITLE-EROTIC-MASSAGE-PART-1]<?=(@$city_name === NULL ? '[LABEL-MONTREAL]' : $city_name)?>" href="<?=\Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => 'erotic-massage'))?>">
                            [LABEL-EROTIC-MASSAGE]
                        </a>
                    </li>
                    <li>
                        <a title="[HREF-TITLE-EROTIC-MASSAGE-PARLOR-PART-1]<?=(@$city_name === NULL ? '[LABEL-MONTREAL]' : $city_name)?>" href="<?=\Core\Utils\Navigation::urlFor('ad', 'listing', array('category' => 'erotic-massage-parlor'))?>">
                            [LABEL-MASSAGE-PARLORS]
                        </a>
                    </li>
                        <?php $unreadMessages = \Modules\Humpchies\MessageModel::countUnreadMessages(\Core\Utils\Session::getCurrentUserID()); ?>
                        <li class="post_button user-menu-trigger" id="account-trigger">
                            <a href="javascript:void(0)">
                                <i class="fa fa-user"></i>
                                [LABEL-ACCOUNT]
                            </a>
                            
                            <?php if ($unreadMessages) : ?>
                                <span class="notice" id="noticeUread"><?=$unreadMessages?></span>
                            <?php endif; ?>
                            
                            <div class="user-menu-container">
                                <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE) : ?>
                                    <a title="[HREF-TITLE-LOGIN]" href="<?=\Core\Utils\Navigation::urlFor('user', 'login')?>">
                                        <i class="fa fa-sign-in"></i>
                                        [LABEL-LOGIN]
                                    </a>
                                    <a title="[HREF-TITLE-SIGN-UP]" href="<?=\Core\Utils\Navigation::urlFor('user', 'signup')?>">
                                        <i class="fa fa-user-plus"></i>
                                        [LABEL-SIGN-UP]
                                    </a>
                                <?php endif;?>
                                
                                <?php if(\Core\Utils\Session::getAuthenticatedSessionValues() !== FALSE) : ?>
                                    
                                    <? if(\Core\Utils\Session::getCurrentUserID() !== 63661 && \Core\Utils\Session::currentUserIsAdvertisment()): ?>
                                        <div class="credits-current-amount">[LABEL-YOUR-CREDITS]:<?= \Modules\Humpchies\UserModel::getCredits(\Core\Utils\Session::getCurrentUserID()) ?></div>
                                        <a href="<?=\Core\Utils\Navigation::urlFor('payment', 'bumps-purchase')?>">
                                            <i class="fa fa-cart-plus"></i>
                                            [LABEL-BUY-MORE-CREDITS]
                                        </a>
                                    <? endif ?>
                                    <?php if(\Core\Utils\Session::currentUserIsAdvertisment()) :?>
                                        <a title="[HREF-TITLE-MY-ADS]" href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>"><i class="fa fa-folder-open"></i>[LABEL-MY-ADS]</a>
                                    <?php endif;?>
                                    
                                    <a title="[HREF-TITLE-MY-CHAT]" href="<?=\Core\Utils\Navigation::urlFor('chat', 'view')?>" style="position:relative">
                                        <i class="fa fa-envelope"></i>
                                        [LABEL-MY-CHAT]
                                        <?php if ($unreadMessages) : ?>
                                        <span class="notice"><?=$unreadMessages?></span>
                                        <?php endif; ?>
                                    </a>
                                   
                                        <a title="[HREF-TITLE-MY-SETTINGS]" href="<?=\Core\Utils\Navigation::urlFor('user', 'settings')?>"><i class="fa fa-cog"></i>[LABEL-SETTINGS]</a>
                                    
                                    <?php if(\Core\Utils\Session::currentUserIsAdvertisment()) :?>
                                        <a title="[LABEL-BLACKLIST-CLIENTS]" href="<?=\Core\Utils\Navigation::urlFor('user', 'blacklist')?>"><i class="fa fa-user-times"></i>[LABEL-BLACKLIST-CLIENTS]</a>
                                    <?php endif; ?>
                                
                                    <?php if(\Core\Utils\Session::currentUserIsMember()) :?>
                                        <a title="[TITLE-FAVORITES]" href="<?=\Core\Utils\Navigation::urlFor('favorites', 'listing')?>"><i class="fa fa-heart"></i>[TITLE-FAVORITES]</a>
                                    <?php endif; ?>
                                
                                    <a title="[HREF-TITLE-LOGOUT]" href="<?=\Core\Utils\Navigation::urlFor('user', 'logout')?>"><i class="fa fa-sign-out"></i>[LABEL-LOGOUT]</a>
                                <?php endif;?>
                                
                            
                            </div>
                        </li>
                        
                    <?php if(\Core\Utils\Session::currentUserIsAdvertisment()):?>
                            <li class="post_button">
                               <a href="<?= \Core\Utils\Navigation::urlFor('ad', (\Core\Utils\Session::getCurrentUserID() == 63661) ? 'create-v2' : 'create')?>"><i class="fa fa-plus-circle"></i>[LABEL-NEW-AD]</a>
                    </li>
                    <?php endif; ?>
                        <li class="post_button small">
                        <a href="javascript:void(0)" id="search-btn"><i class="fa fa-search"></i></a>
                    </li>
                </ul>
            </div>
            <?php if (!isset($parsedURL) || !isset($parsedURL->layout_settings['no_horizontal_menu']) || ($parsedURL->layout_settings['no_horizontal_menu'] != true)) : ?>
        
        </div>
        </div>
        <div id="search-form">
            <div class="inner-section">
            <div class="search-header">
                    <h3>[LABEL-SEARCH-TITLE]</h3>
                    <a href="javascript:void(0)" id="close-search-form"><i class="fa fa-times-circle"></i></a>           
            </div>
            <form id="header_search" method="get" action="<?=\Core\Utils\Navigation::urlFor('ad', 'search')?>">
                
                <div class="search-suggest">
                    <div class="form-label-group">
                        <input type="text" id="search_city_header" name="search_city_name" class="form-control" value="" />
                        <label for="search_box_city">[LABEL-SELECT]&nbsp;[LOCATION-LABEL]</label>
                    </div>
                    <input type="hidden" name="category_search_city" id="category_search_city" value="<?php echo $category_code;?>">
                    <ul id="suggesstion-search-city" class="city_select city"></ul>
                </div>
                <input type="hidden" name="city" id="search_city_slug" value="" />
                <div class="form-label-group keyword">
                        <input type="text" id="query" name="query" class="form-control" value="" />
                        <label for="search_box_city">[LABEL-SEARCH-TXT]</label>
                </div>
                
            </form> 
        </div>
        </div>
        <div class="top_search_box_header inner-section">
            
               <!--  [LABEL-TOP-SEARCHES]: -->
                <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'mature'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Mature'">Mature</a>
                <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'anal'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Anal'">Anal</a>
                <?php if(0):?><a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'creampie'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Creampie'">Creampie</a><?php endif;?>                
                <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'independante'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Independante'">Independante</a>                
                 <?php if(0):?><a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'fellation'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Fellation'">Fellation</a> <?php endif;?>              
                <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'milf'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'MILF'">MILF</a>
                <?php if(0):?><a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'avale'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Avale'">Avale</a><?php endif;?>
                <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'asiatique'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Asiatique'">Asiatique</a>                
                <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'bbw'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'BBW'">BBW</a>
                <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'latina'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Latina'">Latina</a>
                <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'fontaine'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Fontaine'">Fontaine</a>                
                <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'domination'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Domination'">Domination</a>                 
                <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'pipe'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Pipe'">Pipe</a>
                <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'search', array('query' => 'gros seins'), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Gros Seins'">Gros Seins</a>
                <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'verified', array(), FALSE)?>" title="[HREF-TITLE-TOP-SEARCHES] <?=$city_ttl ?> 'Gros Seins'">[LABEL-REAL-PHOTO]</a>
            </div>
            <?php else: // no_horizontal_menu ?>
            <div style="border-top:1px solid #f90;height:1px;line-height:1px;margin:15px 0 10px 0"></div>
            <?php endif; // no_horizontal_menu ?>
        
        </div>
        <div id="body">

        <?php require_once(\Core\View::getFullyQualifiedViewPath('\Core\Default::systemMessages', \Core\View::MODELESS))?>
        


<!-- The Modal -->
<?php if($cities_with_zones != null):?>
<div id="cities_zones" class="modal_cities" style="display:none;">

    <!-- Modal content -->
    <div class="modal-content_cities">
        <div class="modal-header_cities">
            <span class="close_report">&times;</span>
            <div style="clear:both"></div>
            <h2>[LABEL-SEARCH-CITY]</h2>
            <input type="text" class="form-control" id="search-box" placeholder="[INPUT-SEARCH-CITY]" />
            <input type="hidden" name="category_code" id="category_code" value="<?php echo $category_code;?>" />
            <ul id="suggesstion-box" class="city_select city"></ul>
            
            <div style="clear:both; height:15px;"></div>
            <h2>[LABEL-SELECT-CITY]</h2>
        </div>
        <div class="modal-body_cities">
             <div>
             <ul class="city_select area">
                <li>
                    <a href="#" class="selected_area"><?php echo $cities_with_zones[$current_area_code]['region']["name"];?></a> <!-- area name -->
                    <i class="fa fa-chevron-down"></i>
                    <ul class="dropdown select_area">
                        <?php foreach($cities_with_zones as $key => $cities):?>
                            <li class="<?php echo $key;?>"><?php echo $cities['region']['name'];?></li>
                        <?php endforeach;?>
                    </ul>
                </li>
             </ul>       
             </div>
             <div>
             <ul class="city_select city" data-txt="[LABEL-SELECT-CITY]">
                <li>
                    <a href="#" class="selected_city"><?php echo $current_city_name;?></a> <!-- city name -->
                    <i class="fa fa-chevron-down"></i>
                    
                    <?php foreach($cities_with_zones as $key => $cities):?>
                        <ul class="dropdown cities_lists <?php echo $current_area_code == $key ? $key : $key . ' hidden';?>">
                            <li><a href="<?=\Modules\Humpchies\AdController::getRegionUrl($category_code, $key);?>">[LABEL-ALL-CITIES]</a></li>
                            <?php foreach($cities['cities'] as $newkey => $city_details): ?>
                                <li class="<?php echo $city_details['slug'];?>" data-areacode="<?php echo $key;?>" data-areaname="<?php echo $cities["region"]['name'];?>">
                                    <a href="<?=\Modules\Humpchies\AdController::getListURL($category_code, $city_details['slug'])?>" title="<?=\Modules\Humpchies\AdController::getListHrefTitle(@$category_code, $city_details['name'])?>">
                                        <?php echo $city_details['name']?>
                                    </a>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    <?php endforeach;?>
                </li>
             </ul> 
             </div>           
        </div>
        
    </div>

</div>

<?php endif;?>        

<?php if(!\Core\Utils\Session::getAuthenticatedSessionValues() === FALSE) : ?>
    <input type="hidden" id="show_alert" name="show_alert" value="yes" />
<?php endif;?>
<div id="alert-modal" class="modal-pm-gray">
    <!-- Modal content -->
    <div class="modal-pm-content">
        
        <div class="modal-pm-header" style="padding-top: 20px;">
            <div class="fa fa-close modal-close"></div>
            <h2 style="color: #dc0000; font-size: 18px; text-align: center;">[LABEL-PHISHING-ALERT]</h2>
        </div>
        <div class="modal-pm-body" style="padding:10px 20px; font-size: 13px; line-height: 16px; text-align: left;">
            [LABEL-PHISHING-ALERT-TXT]
        </div>
        <div class="modal-pm-footer">
            <div class="modal-pm-left">
                <input type="button" class='btn-orange modal-close' value="[LABEL-CLOSE]"/>
            </div>
        </div>
    </div>
</div>