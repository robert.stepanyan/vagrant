<nav id="breadcrumb" class="mb-30">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li> [LABEL-CONTACT-US]</li>
    </ul>
</nav> 
<h2 class="stylized-title custom"><span>[LABEL-CONTACT-US]</span></h2>

<div class="wrapper_content">
    <div class="content_box">
        <div class="content_box_body user-login">
                <form id="contact_form" method="post">
                    <div class="field">
                        <label>[LABEL-NAME]:</label>
                        <input class="form-control" name="name" type="text" value="<?=(isset($_POST["name"]) ? $_POST["name"] : '')?>" maxlength="<?=$max_len_name?>" />
                    </div>
                    <div class="field">
                        <label>[LABEL-EMAIL]:</label>
                        <input class="form-control" name="email" type="text" value="<?=(isset($_POST["email"]) ? $_POST["email"] : '')?>" maxlength="<?=$max_len_email?>" />
                        </div>
                    
                    <div class="field" style="margin-bottom:15px;"><div class='note'>[LABEL-CONTACT-N0TICE]</div></div>
                    
                    <div class="field">
                        <label>[LABEL-REEMAIL]:</label>
                        <input class="form-control" name="reemail" type="text" value="<?=(isset($_POST["reemail"]) ? $_POST["reemail"] : '')?>" maxlength="<?=$max_len_email?>" />
                    </div>
                    
                    <div class="field">
                        <label>[LABEL-USERNAME]:</label>
                        <input class="form-control" name="username" type="text" value="<?=(isset($_POST["username"]) ? $_POST["username"] : '')?>" maxlength="<?=$max_len_email?>" />
                    </div>
                    
                    <div class="field">
                        <label>[LABEL-ISSUE-TYPE]:</label>
                        <select class="form-control" name="issue_type" >
                                <option value="">---</option>
                                <option value="1">[OPTION-ISSUE-TYPE-1]</option>
                                <option value="2">[OPTION-ISSUE-TYPE-2]</option>
                                <option value="3">[OPTION-ISSUE-TYPE-3]</option>
                                <option value="4">[OPTION-ISSUE-TYPE-4]</option>
                            </select>
                        
                        </div>
                    
                    <div class="field">
                        <label>[LABEL-SUBJECT]:</label>
                        <input class="form-control" name="subject" type="text" value="<?=(isset($_POST["subject"]) ? $_POST["subject"] : '')?>" maxlength="<?=$max_len_subject?>" />
                    </div>
                    
                    <div class="field">
                        <label>[LABEL-MESSAGE]:</label>
                        <textarea name="message" class="form-control"><?=(isset($_POST["message"]) ? $_POST["message"] : '')?></textarea>
                        </div>
                    
                    <div class="field" style="margin-bottom: 15px;">
                        <label>[LABEL-CAPTCHA]:</label>
                        <div>
                            <span class="code"><img src="/captcha.gif"></span>
                        </div>
                    </div>
                    
                    <div class="field">
                        <label>[LABEL-TYPE-CAPTCHA]:</label>
                        <input class="form-control" name="code" type="text" value="" maxlength="<?=$max_len_captcha?>" />
                    </div>
                    
                    <div class="field">
                        <div style="width: fit-content;">
                            <input class="button" type="submit" value="[BTN-CAPTION-SEND-MESSAGE]" />
                        
                            <a href="https://twitter.com/HumpchiesC" target="_blank" title="Twitter" style="display: flex;align-items: center;align-content: center;justify-content: center;margin-top: 10px;color: #fff;">
                               <img src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>twitter-logo.png" alt="" style="margin-right:5px;"/>
                               Twitter
                            </a>
                        </div>
                        
                        
                
                    </div>
                </form>
            
        </div>
    </div>
</div>
