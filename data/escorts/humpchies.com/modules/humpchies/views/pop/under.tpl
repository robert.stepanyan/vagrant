<?php
/**
 *          __              __      __          __
 *    _____/ /_  ____ _____/ /_  __/ /_  __  __/ /____
 *   / ___/ __ \/ __ `/ __  / / / / __ \/ / / / __/ _ \
 *  (__  ) / / / /_/ / /_/ / /_/ / /_/ / /_/ / /_/  __/
 * /____/_/ /_/\__,_/\__,_/\__, /_.___/\__, /\__/\___/.com
 *                        /____/      /____/
 *
 * Copyright (c) Ivan Manzanilla, webmaster@shadybyte.com
 *
 * This program is NOT free software; you can NOT redistribute it and/or
 * modify it, unless explicitly authorized by Ivan Manzanilla.
 *
 * DESCRIPTION: This file renders HTML code to the user's screen that
 * represents the site's pop-under.
 */

 return;

 $ageVerifiedCookieName = 'ageVerified';

 if (!(isset($_COOKIE[$ageVerifiedCookieName]) && 1 === (int)$_COOKIE[$ageVerifiedCookieName])) {
    return;
 }

 require_once(
        \Core\View::getFullyQualifiedViewPath('Modules\Humpchies\Pop::under_js',
        \Core\View::DEVICE_MODE));?>
