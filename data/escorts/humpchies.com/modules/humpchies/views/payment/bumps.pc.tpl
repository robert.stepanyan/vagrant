<style>
 .checked-text{
    margin-bottom: 20px;
    line-height: 21px;
    width: 389px;
    text-align: left;
    font-family: "Roboto";
    font-size: 16px;
    padding-top: 12px;
	color:#fff;
    
 }
 .grey-separator{
     width:100%;
     height:1px;
     border-bottom:1px solid #707070;
 }
 .payments{
     display:flex;
     justify-content: center;
     margin-top: 20px;
 }
 .payment-item{
     width: 315px;
     border: 1px solid #FF9900;
     border-radius: 5px;
     text-align: left;
     padding: 21px;
     margin: 39px 15px;
     font-family: "Roboto";
 }
 .premium:before, .bump:before{
    content: "";
    background-image:url("<?=Modules\Humpchies\Utils::getCDNPath('images')?>premium-star.png");
    content: "";
    width: 59px;
    height: 58px;
    position: absolute;
    top: -38px;
    left: 125px;
    display: block;
 }
 .bump:before{
    content: "";
    background-image:url("<?=Modules\Humpchies\Utils::getCDNPath('images')?>bump-up.png");
 }
 .payment-item-title{
     color:#FF9900;
     font-size: 16px;
     text-align: center;
     padding: 7px 15px 15px;
 }
 .payment-item-options {
    line-height: 18px;
    font-size: 14px;
    font-family: "roboto";
    color: #A4A4A4;
}
a.button-submit{
    background-color: #FF9900;
    border: 0px;
    width: 100%;
    padding: 15px;
    font-size: 16px;
    border-radius: 5px;
    margin-top: 22px;
    display: block;
    color:#000;
    text-align: center;
}
a.button-submit:hover{
    background-color: #e58900;    
}
a.lame-link {
    display: inline-block;
    width: 40%;
    border: 1px solid #FF9900;
    padding: 10px;
    border-radius: 5px;
    font-size: 14px;
    font-family: "Roboto";
    margin-top: 31px;
} 

.lame a, .lame div {
    margin: 20px;
    padding: 10px;
    font-size: 14px;
}
.lame{
    display:flex;
    justify-content: center;
}
.galben{
    color:#ff9900;
}
 </style>

 <nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i><a/></li>
        <li> <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>">[LABEL-MY-ADS]<a/></li>
        <li>[LABEL-BUMP-EXTRA-VISIBILITY]</li>
    </ul>    
</nav>
<div class="wrapper_content_full">
  <div class="lds-dual-ring"></div>
	<div class="backurl"><a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>">[LABEL-RETURN-BACK]</a></div>
	<div class="create-ad" style="display:none">
    
        <div class="content_box" >
            <div  class='grey-separator' style=""></div>
            <div class="content_box_header" style="margin: 30px auto;margin-bottom: 0px; text-align:center; display:block">
                <div style="display:flex;width: 55%;margin: 10px 24%;     margin-bottom: 20px;">
                    <img src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>checked_white.svg" border="0" width="65" height="65" alt="" style="margin: 2px auto;"/>
                    <div class="checked-text">[NEW-AD-BUMPS-SUCCESS-TITLE]<br/> 
                    <?=\Core\Utils\Text::translateTextWithVars( '[NEW-AD-BUMP-DESCRIPTION-BUMP]',['%adid%' => $ad_id]) ?>
                    
                    </div>
                </div>
                <div  class='grey-separator' style=""></div>    
            
            
                <div class='lame' >
                   <div>[LABEL-PAYMENT-SUCCESS-OTHER]</div> <a href="<?php echo \Core\Utils\Navigation::urlFor('ad', 'verify'); ?>?id=<?php echo $ad_id; ?>" class="lame-link" style='width:auto'> [AD-VERIFY-TITLE]</a>  <a href="/ad/my-listings" class="lame-link" style='width:auto'> [LABEL-GO-TO-ADS]</a>
                </div>
            </div>
        </div>
    </div>
    <div class="bummped-ad" style="display:none">
    
		<div class="content_box" >
			<div  class='grey-separator' style=""></div>
			<div class="content_box_header" style="margin: 30px auto;margin-bottom: 0px; text-align:center; display:block">
				<div style="display:flex;width: 55%;margin: 10px 24%;     margin-bottom: 20px;">
					<img src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>checked_white.svg" border="0" width="65" height="65" alt="" style="margin: 2px auto;"/>
					<div class="checked-text">[NEW-AD-BUMPS-SUCCESS-TITLE]<br/> 
					
                    <?=\Core\Utils\Text::translateTextWithVars( '[DIRECT-BUMPS-SUCCESS-TITLE]',['%adid%' => $ad_id]) ?>
                    
					</div>
				</div>
				<div  class='grey-separator' style=""></div>    
			</div>
		</div>
	</div>         
	
	<div class="missing-credits" style="display:none">
    
		<div class="content_box" >
			<div  class='grey-separator' style=""></div>
			<div class="content_box_header" style="margin: 30px auto;margin-bottom: 0px; text-align:center; display:block">
				<div style="display:flex;width: 55%;margin: 10px 24%;     margin-bottom: 20px;">
					<img src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>payment-error.png" border="0" width="89" height="83" alt="" style="margin: 2px auto;"/>
					<div class="checked-text">[NEW-AD-BUMPS-BUY]			
					</div>
				</div>
				<div  class='grey-separator' style=""></div>    
			
			
				<div class='lame' >
				   <a href="/payment/bumps-purchase?ad_id=<?= $ad_id ?>" class="lame-link" style='width:auto'> [NEW-AD-BUMPS-BUY-OTHER]</a>  <a href="javascript:shortcutpay()" class="button-submit" style='width:auto'> [LABEL-HOLIDAY-CTA] </a> 
				</div>
			</div>
		</div>
	</div>  
	
	
	<div class="form-wrapper ">
    <div class="form-container">
        <div class="form-header">
            <h2  class="form-header-line1">[LABEL-BUMP-EXTRA-VISIBILITY]</h2>
            <div class="form-header-line2">
				<h4 id="what-bump-means" class="yellow"><i class="fa fa-question-circle" aria-hidden="true"></i>&nbsp;[LABEL-WHAT-BUMP-MEAN]</h4>
            </div>
        </div>
		<div class="arrow-up" style="display: none"></div>   
       
        <div class="what-bump-means" style="display: none">[BUMP-MEANING]</div>
        <hr class="form-seperator">

        <form id="bump-form" action="<?=\Core\Utils\Navigation::urlFor('payment', 'bumps-save')?>">
            <input type="hidden" name="user_id" value="<?= $user_id ?>">
            <input type="hidden" name="ad_id" id='ad_id' value="<?= $ad_id ?>">

            <div class="form-settings-blocks">
                <div class="form-settings-block">
					<h2 class="title-onetime">[LABEL-BUMP-AD-TITLE]  &nbsp;<span class="ad-id">#<?= $ad_id ?></span></h2>
					<div class="nopadding-left padding-20">
                        <div class="block">
                           <h4>[LABEL-BUMP-AD-DESC]</h4>
						   <div id='one-time-error' class="errors"></div> 
                           <button id="onetime-bump" data-url="<?= \Core\Utils\Navigation::urlFor('payment/bumps-onetime') ?>" type="submit" class="button">[LABEL-BUMP-AD-ONETIME]&nbsp;<i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
                
                <div class="form-settings-block">
                    <h2 class="title-multiple">[LABEL-BUMP-AD-MULTIPLE]</h2>
					<div class="nopadding-right padding-20">
                         <div class="form-inputs">
                            <div class="inline">
                                <div class="category">
                                    <div class="label-wrapper"><label>[LABEL-BUMP-SETTINGS]</label></div>
                                    <div class="dropdown">
                                        <div class="select">
                                            <span><?= $every_min_unit ?></span>
                                            <i class="fa fa-chevron-down"></i>
                                        </div>
                                        <input type="hidden" name="bump_time" value="<?= $every_min ?>">
                                        <ul class="dropdown-menu bump-settings">
										<? foreach( \Modules\Humpchies\BumpsModel::$INTERVAL_OPTION_MINS_RENDERER as $value => $option):	
											if($option['show_as_hour']){
												$interval_text = ($value / 60). ' [LABEL-HOUR]';
											}
											else{
												$interval_text = ($value == 1) ? $value . ' [LABEL-MINUTE]' : $value . ' [LABEL-MINUTES]';
											} ?>
											<li value="<?= $value ?>" data-hours="<?= implode(',', $option['available_periodes']) ?>"><?= $interval_text ?></li>
										<? endforeach; ?>

										</ul>
                                    </div>
                                </div>
                                
                                <div class="package">
                                    <div class="label-wrapper"><label>[LABEL-BUMP-FOR]</label></div>
                                    <div class="dropdown">
                                        <div class="select">
                                          <span>[LABEL-NEXT-LES] <?= $for_hour_unit ?></span>
                                          <i class="fa fa-chevron-down"></i>
                                        </div>
                                        <input type="hidden" name="for" value="<?= $for_hour ?>">
                                        <ul class="dropdown-menu">
											<? foreach( \Modules\Humpchies\BumpsModel::$PERIODE_OPTION_HOURS_RENDERER as $value => $option):	
											if($option['show_as_day']){
												$periode_text = ($value / 24). ' [LABEL-DAYS]';
											}
											else{
												$periode_text = ($value == 1) ? $value . ' [LABEL-HOUR]' : $value . ' [LABEL-HOURS]';
											} 
											
											if(in_array($value, \Modules\Humpchies\BumpsModel::$INTERVAL_OPTION_MINS_RENDERER[$every_min]['available_periodes'])){
												$show = true;
											}
											else{
												$show = false;
											}
											?>
																				
											<li value="<?= $value ?>" class="bump-for-<?= $value ?> bumps-hour " <?= $show ? 'style="display: list-item"' : '' ?> >[LABEL-NEXT] <?= $periode_text ?></li>
											
											<? endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="city"> 
                                <div class="label-wrapper">
                                    <p><span class="bump-count">1</span>&nbsp;bump =&nbsp;<span class="bump-credit">1</span>&nbsp;[LABEL-YOUR-CREDIT], [LABEL-DESC-2]</p>
                                    <p>[LABEL-DESC-3]:&nbsp;<span id="bump-credits"> <?= ($bump_settings === false) ? 61 : $bump_settings['bumps_total'] ?> </span></p>
                                    <p>[LABEL-YOUR-CREDITS]: &nbsp; <span class="galben"><?= \Modules\Humpchies\UserModel::getCredits(\Core\Utils\Session::getCurrentUserID()) ?></span></p>
                                </div>
                            </div>
							<div id='settings-error' class="errors"></div> 
                            <div class="payment-line">
                              <? if($bump_settings === false): ?>
									<button id="bump-settings-submit" type="submit" class="button">[LABEL-BUMP-SUBMIT-BUTTON]&nbsp;<i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></button>
                                <? else: ?>
									<button id="bump-settings-submit" type="submit" class="button">[LABEL-BUMP-EDIT-BUTTON]&nbsp;<i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></button>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="bump-status-text">
						<? if($last_bump === false): ?>
							<p><?= \Core\Utils\Text::translateTextWithVars( '[BUMP-STATUS-2]', array('%ad_id%'=> $ad_id) ) ?><br>[BUMP-STATUS-2-B]</p>
						<? elseif($bump_settings === false && $last_bump !== false ): ?>
                            <p><?= \Core\Utils\Text::translateTextWithVars( '[BUMP-STATUS-1]', array('%ad_id%'=> $ad_id, '%date%'=> $last_bump['date'] ) ) ?></p>
						<? elseif($bump_settings !== false): ?>
                            <p><?= \Core\Utils\Text::translateTextWithVars( '[BUMP-STATUS-3]', array('%x%' => $every_min_unit, '%y%' => $for_hour_unit) ) ?></p>
    					    <p><?= \Core\Utils\Text::translateTextWithVars( '[BUMP-STATUS-4]', array( '%count%' => $bump_settings['bumps_left'], '%date%' => $bump_settings['update_date'] ) ) ?></p>
                        <? endif; ?>
                    </div>

					<? if($bump_settings !== false): ?>
                        <div id="stop-bumping" class="stop-bumping"><i class="fa fa-times-circle-o" aria-hidden="true"></i>&nbsp;[LABEL-STOP-BUMPING]</div>
					<? endif; ?>
                </div>
            </div>
        </form>        
        </div>
    </div>
</div>
