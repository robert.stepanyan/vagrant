<style type="text/css">
h3 {
    margin-top: 29px;
}
</style>
<?php
if($lang =='fr') {?>
<div class="wrapper_content" style="line-height:20px;">
    <div class="content_box">
        <div class="content_box_body">
    <h2>Page d'information Bitcoin</h2>
 
<h3>Qu'est-ce que le bitcoin?</h3>
<p>bitcoin est l'une des  4000 cryptomonnaies qui existent depuis leur invention en 2008. La seule diff&eacute;rence est que le bitcoin est la cryptomonnaie la plus populaire sur le march&eacute;, parmi les investisseurs et les courtiers, tandis que les autres ont un volume de transactions insignifiant.</p>
<p>Comme monnaie digitale, le Bitcoin ne d&eacute;pend pas d'une banque centrale et n'est li&eacute; &agrave; aucune r&eacute;glementation gouvernementale. Les transactions effectu&eacute;es avec une cryptomonnaie comme le Bitcoin sont v&eacute;rifi&eacute;es par cryptographie et enregistr&eacute;es dans un grand livre public appel&eacute; blockchain.
Les paiements effectu&eacute;s avec Bitcoin vous offrent une discr&eacute;tion et une s&eacute;curit&eacute; accrue.</p>
<h3>Qui l'a cr&eacute;&eacute; et pourquoi?</h3>
<p>Bitcoin a &eacute;t&eacute; cr&eacute;&eacute; en 2009 par une personne ou un groupe de personnes agissant sous le pseudonyme de Satoshi Nakamoto. L'identit&eacute; de la personne ou des personnes derri&egrave;re Bitcoin reste un myst&egrave;re, mais ce qui est certain, c'est le fait qu'avec le temps, Bitcoin a commenc&eacute; &agrave; attirer beaucoup d'attention et a &eacute;t&eacute; mis en place comme nouveau moyen de paiement par les grandes entreprises.</p>

<p>La plus importante cryptomonnaie du moment, le Bitcoin a &eacute;t&eacute; cr&eacute;&eacute; pour &ecirc;tre autonome, s&eacute;par&eacute; par toute sorte d'autorit&eacute; ou d'interf&eacute;rence gouvernementale et non influenc&eacute; par l'erreur humaine, &eacute;tant g&eacute;r&eacute; par une autorit&eacute; d&eacute;centralis&eacute;e.</p>

<h3>Quand est-il devenu populaire?</h3>
<p>Bitcoin a &eacute;t&eacute; pr&eacute;sent&eacute; au grand public en 2009, mais les gens ne comprenaient pas vraiment la technologie qui se cache derri&egrave;re Bitcoin jusqu'&agrave; r&eacute;cemment. Mais, au fil du temps, il a gagn&eacute; en popularit&eacute; dans le monde entier, comme un moyen moderne et meilleur d'effectuer des paiements.</p>
<p> plus en plus d'entreprises ont commenc&eacute; &agrave; utiliser Bitcoin comme m&eacute;thode de paiement, en raison de la s&eacute;curit&eacute; accrue et du niveau de discr&eacute;tion plus &eacute;lev&eacute; qu'il offre &agrave; ses utilisateurs. Beaucoup pensent que Bitcoin a un grand avenir devant lui, les chiffres montrant une augmentation constante de la valeur, parce que les gens se tournent vers cette m&eacute;thode plus rapide et plus s&ucirc;re d'accepter et d'effectuer des paiements.</p>
<h3>Comment obtenir du bitcoin? Comment transformer de l'argent en btc ou de la btc en argent?</h3>
<p>Il existe trois principaux moyens d'obtenir du Bitcoin : acheter du Bitcoin sur un march&eacute; boursier, accepter du Bitcoin comme paiement pour des biens et des services et extraire du Bitcoin.</p>
<p>La transformation de Bitcoin en esp&egrave;ces ou d'esp&egrave;ces en Bitcoin peut se faire par l'interm&eacute;diaire d'un &eacute;change de devises cryptographiques de confiance. Vous pouvez y choisir de transformer le Bitcoin en une monnaie fiduciaire de votre choix ou de le convertir en une autre cryptomonnaie comme l'Ethereum. Utilisez une bourse qui prend en charge les &eacute;changes de fiat &agrave; crypto, cr&eacute;ez un compte et envoyez le Bitcoin de votre portefeuille num&eacute;rique au portefeuille Web existant sur votre compte d'&eacute;change de crypto.</p>
<p>Une autre m&eacute;thode de conversion de l'argent liquide en crypto, ou l'inverse, consiste &agrave; utiliser une passerelle de paiement ou une application de point de vente. Le service que vous choisissez d'utiliser recevra alors le Bitcoin qui vous a &eacute;t&eacute; pay&eacute; et d&eacute;posera l'&eacute;quivalent dans la devise de votre choix, sur votre compte bancaire professionnel.</p>
<p>Vous pouvez &eacute;galement opter pour une plate-forme de n&eacute;gociation. Il vous suffit de cr&eacute;er un compte et de d&eacute;finir le montant de Bitcoin que vous choisissez de vendre, ainsi que le type de paiement pr&eacute;f&eacute;r&eacute; (paiements en esp&egrave;ces en personne ou virements bancaires). Vous envoyez le Bitcoin de votre portefeuille num&eacute;rique sur un compte bloqu&eacute; s&eacute;curis&eacute; et, une fois le paiement convenu, il sera lib&eacute;r&eacute; et envoy&eacute; au portefeuille.</p>
<p>Parmi les meilleures bourses de cryptage du march&eacute;, on trouve Coinbase, Binance, Bisq et eToro. Ils pr&eacute;l&egrave;vent tous des frais par change, mais si vous les cumulez, ils sont presque toujours inf&eacute;rieurs aux frais suppl&eacute;mentaires que vous devez payer avec les devises fiduciaires.</p>
<h3>Comment payer ou conserver le bitcoin?</h3>
<p>La premi&egrave;re chose &agrave; faire lorsque vous voulez payer et &ecirc;tre pay&eacute; en cryptomonnaies est de trouver un porte-monnaie dans lequel vous pourrez ranger votre monnaie num&eacute;rique. Ce portefeuille num&eacute;rique fera alors office de compte bancaire et vous permettra de garder, de payer et de recevoir du Bitcoin.</p>
<p>Les portefeuilles cryptographiques sont de deux types: les portefeuilles froids et les portefeuilles chauds. Les portefeuilles froids sont ceux qui ne sont pas connect&eacute;s &agrave; l'internet, tandis que les portefeuilles chauds sont connect&eacute;s en permanence &agrave; l'internet.</p>
<p>Le principal avantage des portefeuilles froids est qu'ils vous offrent une s&eacute;curit&eacute; plus &eacute;lev&eacute;e que les portefeuilles chauds. Le portefeuille chaud est id&eacute;al pour garder de petites sommes d'argent et effectuer des paiements quotidiens, tandis que le portefeuille froid est pr&eacute;f&eacute;r&eacute; lorsque l'on parle de grosses sommes d'argent et d'une s&eacute;curit&eacute; accrue. Cette derni&egrave;re version n'est pas id&eacute;ale pour un usage quotidien.</p>
<p>Les portefeuilles Web, comme ceux des sites Web ou des navigateurs, et les portefeuilles de bureau qui sont install&eacute;s sur un ordinateur portable/ordinateur et sont connect&eacute;s en permanence &agrave; l'Internet, sont des portefeuilles chauds, tandis que les portefeuilles mat&eacute;riels, comme les cl&eacute;s USB, sont des portefeuilles froids.
Les meilleurs portefeuilles froids sur le march&eacute; sont le Ledger, le Trezor et le Keepkey, tandis que lorsque nous parlons de portefeuilles chauds, nous parlons d'Electrum, d'Exodus et de Mycelium.</p>
<p>Souvenez-vous que poss&eacute;der un Bitcoin signifie avoir une adresse et une cl&eacute; priv&eacute;e qui vous permet de crypter des signatures num&eacute;riques. Sans cl&eacute; priv&eacute;e, vous n'avez plus acc&egrave;s &agrave; vos bitcoins, alors gardez-les en lieu s&ucirc;r!</p>
<h3>Comment puis-je payer sur humpchies avec &ccedil;a?</h3>
<p>Comme mentionn&eacute; dans la page de paiement, vous pouvez payer de 2 mani&egrave;res:</p>
<p>Directement &agrave; un distributeur automatique de bitcoin en utilisant seulement votre carte ou de l'argent liquide (voir la page de paiement pour les d&eacute;tails et la carte)
Utiliser votre propre portefeuille hardware ou software;</p>
<h3>Quel est le processus exactement?</h3>
<p>Tout comme pour les paiements par carte, s&eacute;lectionnez le paquet de cr&eacute;dits ou l'annonce que vous souhaitez promouvoir. Lorsque vous choisissez le mode de paiement, s&eacute;lectionnez Bitcoin. Vous remarquerez que le prix va changer. Proc&eacute;dez au paiement et vous serez redirig&eacute; vers une page de paiement de notre partenaire de traitement Bitcoin,&nbsp;<a href="https://coiniverse.ch"> coiniverse.ch</a></p>
<p>&agrave; ce stade, vous serez montr&eacute;:</p>
<ul>
    <li style="padding-left:20px">   -une adresse de bitcoin o&ugrave; il vous est demand&eacute; d'envoyer le bitcoin</li>
    <li style="padding-left:20px">   -un montant en bitcoin, puisque le bitcoin a une valeur &eacute;lev&eacute;e, la conversion de CAD en CTB ressemblera &agrave; environ 0,000xxxx.</li>
    <li style="padding-left:20px">    -un code QR</li>
</ul>
<p>De plus, deux m&eacute;thodes de paiement vous seront pr&eacute;sent&eacute;es:</p>
<p>Le paiement par les distributeurs de Bitcoin - ceux-ci ressemblent aux distributeurs des banques, mais ils vous permettent de transf&eacute;rer du Bitcoin &agrave; l'adresse d'un destinataire sans en poss&eacute;der vous-m&ecirc;me - vous payez simplement avec votre carte de d&eacute;bit/cr&eacute;dit ou en comptant.</p>
<p>Une fois que vous avez atteint un distributeur de bitcoin, il vous suffit de suivre les instructions &agrave; l'&eacute;cran, mais assurez-vous de connaître l'adresse du destinataire (indiqu&eacute;e sur la page de paiement) et le montant &agrave; payer.</p>
<p>Pour savoir si vous pouvez payer aux distributeurs automatiques de bitcoin, veuillez consulter la liste des distributeurs disponibles dans votre r&eacute;gion &agrave; l'adresse suivante : &nbsp;<a href='https://coinatmradar.com/bitcoin-atm-near-me/' target="_blank">https://coinatmradar.com/bitcoin-atm-near-me/</a></p>
<p>Paiement par portefeuille (hardware ou software); pour payer par cette m&eacute;thode, il suffit d'acc&eacute;der &agrave; votre portefeuille de bitcoin, de vous assurer que vous avez assez de bitcoin pour payer puis de trouver l'option "Envoyer", de coller exactement l'adresse de Bitcoin et de coller le montant exact indiqu&eacute;  dans la page de paiement et confirmez ensuite l'envoi.  Veuillez vous assurer que l'adresse &agrave; laquelle vous envoyez et le montant correspondent parfaitement &agrave; ceux qui vous sont indiqu&eacute;s dans la page de paiement de coiniverse. Si vous n'avez pas encore de portefeuille et que vous ne savez pas comment vous en procurer un, veuillez lire ci-dessus la page o&ugrave; nous d&eacute;crivons les types de portefeuilles.</p>
<h3>Pourquoi les prix sont-ils plus &eacute;lev&eacute;s que pour le paiement par carte?</h3>
<p>Malheureusement, les paiements par bitcoin entraînent des frais plus &eacute;lev&eacute;s qui doivent &ecirc;tre couverts par un prix plus &eacute;lev&eacute;. De nos jours, l'anonymat co&ucirc;te cher et nous devons nous conformer aux r&egrave;gles du r&eacute;seau.</p>
<h3>Pourquoi faut-il tant de temps pour confirmer mon paiement?</h3>
<p>Une fois que vous avez effectu&eacute; une transaction &agrave; partir de votre portefeuille, elle est envoy&eacute;e dans la chaîne de blocage pour &ecirc;tre confirm&eacute;e. Selon les frais que vous avez choisi de payer, cela peut arriver t&ocirc;t ou tard. En g&eacute;n&eacute;ral, cela prend de 5 minutes &agrave; maximum 1h. Ne vous inqui&eacute;tez pas, notre syst&egrave;me v&eacute;rifie r&eacute;guli&egrave;rement l'&eacute;tat de votre transaction et il activera automatiquement votre option d&egrave;s que la chaîne de blocage aura renvoy&eacute; un statut "confirm&eacute;". Aucune action n'est requise de votre part apr&egrave;s l'envoi du paiement.</p>
<p>Toutefois, si vous pensez que quelque chose s'est mal pass&eacute;, n'h&eacute;sitez pas &agrave; nous contacter en nous indiquant le num&eacute;ro de transaction, votre nom d'utilisateur et le num&eacute;ro d'annonce.</p>
<h3>Ma banque ou ma famille sauront-elles que j'ai pay&eacute; sur des humpchies?</h3>
<p>La seule chose qui sera visible est l'adresse de votre portefeuille dans le blockchain, mais il n'y a pas de nom ou d'autres donn&eacute;es personnelles associ&eacute;es. Vous ne serez qu'une adresse de bitcoin parmi les millions d'adresses de bitcoin qui transf&egrave;rent des bitcoins chaque jour.</p>
</div>
</div>
</div>
<?php }else{
?>
<div class="wrapper_content" style="line-height:20px;">
    <div class="content_box">
        <div class="content_box_body">
        <h2>Bitcoin Info Page</h2>

<h3>What is bitcoin?</h3>
<p>Bitcoin is one of the 4.000 cryptocurrencies in existence since their invention in 2008. The only difference is that Bitcoin is the most popular cryptocurrency on the market, among investors and brokers, while the rest have insignificant trading volume.</p>
<p>As a digital currency, Bitcoin does not depend on a central bank and it is not tied to any Government regulations. The transactions made with a cryptocurrency such as Bitcoin are being verified using cryptography and then recorded in a public ledger called blockchain.</p>
<p>Payments done with Bitcoin are offering you discretion and increased safety.</p>
<h3>Who created it and why?</h3>
<p>Bitcoin was created in 2009 by a person or a group of persons acting under the pseudonym Satoshi Nakamoto. The identity of the person or persons behind Bitcoin is still a mystery, but what is a certainty is the fact that, in time, Bitcoin has started to attract a lot of attention and has been implemented as a new payment method by big businesses.</p>
<p>The most important cryptocurrency of the moment, the Bitcoin was created to stand alone, separated by any kind of government authority or interference and not influenced by human error, being operated by a decentralized authority.</p>
 <h3>When did it become popular?</h3>
<p>Bitcoin was introduced to the general public in 2009, but people didn't really understand the technology behind Bitcoin until recently. But, over time it gained more and more popularity all over the word, as a modern and better way of doing payments.</p>
<p>More and more businesses have started to use Bitcoin as a payment method, because of the increased safety and the higher level of discretion it offered to its users. Many think that Bitcoin has a bright future ahead, the numbers showing a constant rise in value, because people are turning to this faster and safer method of accepting and operating payments.</p>
<h3>How to get bitcoin? How to turn cash into btc or btc into cash?</h3>
<p>There are three main ways of getting Bitcoin: buying Bitcoin on an exchange, accepting Bitcoin as payment for goods and services and mining for Bitcoin.</p>
<p>Turning Bitcoin into cash or cash into Bitcoin can be done through a trusted cryptocurrency exchange. There, you can choose to turn Bitcoin into a fiat currency of your choosing or convert it into another cryptocurrency such as Ethereum. Use an exchange that supports fiat to crypto exchanges, create an account and send the Bitcoin from your digital wallet to the web wallet existing on your crypto exchange account.</p>
<p>Another method of converting cash into crypto, or the other way around, is through a payment gateway or a point-of-sale app. The service you choose to use will then receive the Bitcoin paid to you and deposit the equivalent in your chosen currency, in your business bank account.</p>
<p>You can also opt for a trading platform. All you need to do is create an account and set the amount of Bitcoin you choose to sell, and the payment type preferred (in-person cash payments or bank transfers). You are sending the Bitcoin from your digital wallet into a secure blind escrow account and after the payment has been agreed it will be released and sent to the wallet.</p>
<p>Some of the best crypto exchanges on the market are Coinbase, Binance, Bisq, and eToro. They all take a fee per exchange, but if you add it up it is almost always lower than the added fees you have to pay with fiat currencies.</p>
<h3>How to pay or hold bitcoin?</h3>
<p>The first thing you need to take care of when wanting to pay and get paid in cryptocurrencies is finding a wallet to store your digital currency in. This digital wallet will then act as your bank account, using it to store, pay and receive Bitcoin.</p>
<p>The crypto wallets are of two types: cold wallets and hot wallets. The cold wallets are the ones that are not connected to the internet, while the hot wallets are permanently connected to the internet.</p>
<p>The main advantage of cold wallets is that it offers you higher safety than the hot wallet. The hot wallet is great for storing small amounts of money and making daily payments, while the cold wallets are preferred when we talk about larger amounts of money and an increased security. This last version is not ideal for daily use.</p>
<p>Web wallets, such as website wallets or browser wallets, and desktop wallets that are installed on a laptop/computer and are permanently hooked to the Internet, are hot wallets, while hardware wallets, such as USB sticks are cold wallets.</p>
<p>Best cold wallets on the market are Ledger, Trezor and Keepkey, while when we talk about hot wallets we talk about Electrum, Exodus and Mycelium.</p>
<p>Keep in mind that owning Bitcoin means having an address and a private key that allows you to encrypt digital signatures. Without a private key you no longer have access to your Bitcoin, so make sure you keep it in a secure place!</p>

<h3>How can i pay on humpchies with it?</h3>

<p>As mentioned in the payment page, you can pay in 2 ways:</p>
<p>Directly from a bitcoin teller machine using only your card or cash (see the payment page for details and map)</p>
<p>Using your own hardware or software wallet; </p>
          

 <h3>What exactly is the process?</h3>

 <p>Just like in the case of card payments, select the credits package or the ad you want to upgrade. When you choose the payment method, select Bitcoin. You will notice that the price will change. Proceed with the payment and you will be redirected to a payment page of our bitcoin processing partner, coiniverse.ch</p>

<p>At this point you will be shown:</p>
<p><ul>
<li style="padding-left:20px">-a bitcoin address where you are asked to send the bitcoin</li>
<li style="padding-left:20px">-an amount in bitcoin, since bitcoin has a high value, the conversion from CAD to BTC will look something like 0,000xxxx.</li>
<li style="padding-left:20px">-a QR code</li>
</ul>
</p>
<p>Additionally, you will be presented with 2 payment methods:</p>

<p>Payment by Bitcoin teller machines- these look similar with the bank ATMs, only that they allow you to transfer bitcoin to a recipient bitcoin address without actually owning any Bitcoin yourself-you simply pay with your debit/credit card or with cash.</p>
<p>Once you reach a bitcoin machine, simply follow the instructions on the screen but make sure you know the recipient address (shown in the payment page) and the amount to pay.</p>
<p>To know if you can pay by bitcoin teller machines, please see you have any in the area here:&nbsp; <a href='https://coinatmradar.com/bitcoin-atm-near-me/' target='_black'>https://coinatmradar.com/bitcoin-atm-near-me/</a></p>

<p>Payment by wallet (hardware of software); to pay using this method, simply access your bitcoin wallet, make sure you have enough bitcoin to pay then find the option to "Send", paste exactly the Bitcoin address and paste the exact amount shown  in the payment page and then confirm the sending. Please make sure that the address you are sending at and the amount match perfectly the ones shown to you in the coiniverse payment page. If you don't have a wallet yet and you are not sure how to get one, please read above in the page, where we describe the types of wallets.</p>


<h3>Why are the prices higher than for the card payment?</h3>

<p>Unfortunately, bitcoin payments do bring a higher level of fees that need to be covered by a higher price. Nowadays, anonymity costs and we need to comply with the rules of the network.</p>
 

<h3>Why does it take so long to confirm my payment?</h3>

<p>Once you complete a transaction from your wallet, it is sent in the blockchain to be confirmed. Depending on the fees you've chosen to pay, this can happen sooner or later. Usually, it takes from 5 minutes to max. 1h. Do not worry, our system checks regularly to verify the status of your transaction and it will activate your option automatically as soon as the blockchain returns a "confirmed" status. There is no action needed from your part after you sent the payment.</p>
<p>However, if you suspect that something went wrong, feel free to contact us with the transaction id, your user id, and ad id.</p>

<h3>Will my bank or family know i paid on humpchies?</h3>

<p>The only thing that will be visible is your wallet address in the blockchain but there is no name or other personal data associated with it. You will be just a bitcoin address among millions of bitcoin addresses transferring bitcoin each day.</p>

        
        
        </div>
    </div>
</div>    
<?php
} 
?>