<div class="content">

    <? if(\Core\Controller::getMessages() !== FALSE): ?>

        <div class="payment-form-wrapper">
            <p class="center">
                <a href="/" class="back-link">&larr; [LABEL-RETURN-BACK]</a>
            </p>
        </div>

    <? else: ?>

        <div class="heading">
            <?php if(!empty($payment_result)): ?>
                <h4 class="title">[IN-PAGE-TEXT-PAY-HERE]</h4>
            <?php endif; ?>
        </div>

        <div class="payment-form-wrapper">

            <div class="form-wrapper">

                <?php if(!empty($payment_result)): ?>

                <form action="/payment/send-result-email" method="POST" clas="payment-feedback-form">

                    <a href="/payment" class="back-link">&lt; [LABEL-RETURN-BACK]</a>

                    <?php if($payment_result['status'] == "APPROVED"): ?>
                        <p class="payment-wrapper__text--bottom mt-4">[PROMPT-PAYMENT-SUCCESS] </p>
                    <?php else: ?>
                        <p class="payment-wrapper__text--bottom mt-4">[PROMPT-PAYMENT-FAIL]</p>
                    <?php endif; ?>


                    <?php if($payment_result['status'] == "APPROVED"): ?>

                    <div class="payment-feedback-form__input-container">
                        <div class="fancy-form-wrapper w-30">
                            <div class="form-group fancy-form">
                                <div class="md-form">
                                    <input autocomplete="off" required type="email" name="email" id="email-input" class="form-control pay-validate">
                                    <label for="email-input">[LABEL-YOUR-EMAIL]<i class="far for-valid-email fa-check-circle"></i></i></label>
                                </div>
                            </div>
                        </div>
                        <div class="fancy-form-wrapper w-70">
                            <div class="form-group fancy-form">
                                <div class="md-form">
                                    <input autocomplete="off" required type="text" name="id-of-add"  id="id-of-add" class="form-control pay-validate">
                                    <label for="id-of-add">[LABEL-USER-ID]<i class="far for-valid-email fa-check-circle"></i></i></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="payment_result" value="<?= htmlentities(serialize($payment_result)); ?>">
                    <button class="buy-now-btn mail-sender">[LABEL-SEND-EMAIL]</button>

                    <?php endif; ?>

                </form>

                
                <?php else: ?>
                    <h3 style="color: #fff; text-align: center; font-size: 18px; font-weight: normal;">[IN-PAGE-TEXT-PAYMENT-FORM-TOP]</h3>
                    <form action="/payment/process" method="POST" id="paymentForm" class="payment-form m-t-30" novalidate="novalidate">
                        <?php if(isset($errors)): ?>
                            <div class='alert alert-danger'>
                                <ul>
                                    <?php foreach($errors as $error) ?>
                                        <li><?= $error ?></li>
                                    <?php ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <div class="form-wrapper" style="padding-bottom: 0;">

                        <? if(empty($prefilled)): ?>
                            <div class="amount-wrapper clearfix">
                                <input type="number" min="1" max="10000" id="amount-input" name="amount">
                                <span class="currency"><?= $currency ?></span>
                            </div>
                        <? else: ?>
                            <div class="prefilled-text">
                                <span>
                                    <?= $prefilled ?></span>
                                <span class="prefilled-currency"><?= $currency ?></span>

                                <input type="hidden"  min="1" max="10000" id="amount-input" name="amount" value="<?= $prefilled ?>" />
                            </div>
                        <? endif; ?>

                        <button type="submit" class=" buy-now-btn clearfix" id="buyBtn">[LABEL-PAY-NOW]</button>

                        <input type="hidden" name="lang" value="">
                        <input type="hidden" name="page_name" value="ed.php">
                        <input type="hidden" name="payment_gateway" id="paymentGateway" value="ecardon">
                    
                    </form>
                    <p class="card-text payment-wrapper__text--bottom mt-4"  style="text-align: center; font-size: 16px;">[IN-PAGE-TEXT-PAYMENT-FORM-BOTTOM] </p>
                    <?php endif; ?>
                </div>


            
        </div>

    <? endif; ?>
</div>


<div class="footer">
    <div class="left-panel">
        <p class="title">[LABEL-COMPLIANCE]</p>
        <br>
        <p class="text">
        [LABEL-PAYMENT-FOOTER-LEFT]
        </p>
    </div>

    <div class="right-panel">
        <p class="title">[LABEL-SC-COMPANY-DETAILS] </p>
        <br>
        <p class="text">
        [LABEL-PAYMENT-FOOTER-RIGHT]
        </p>
    </div>
</div>


