<style type="text/css">
.payment-line{
    margin-bottom: 20px !important;
}
</style>
<div class="wrapper_content">
    <?/* if(\Core\Utils\Session::getCurrentUserID() !== 63661): */?>
        <div class="credits-buy">
             <span>[LABEL-YOUR-CREDITS]: <?= number_format(\Modules\Humpchies\UserModel::getCredits(\Core\Utils\Session::getCurrentUserID())) ?></span>
             <a href="<?=\Core\Utils\Navigation::urlFor('payment', 'bumps-purchase')?>">
                 <i class="fa fa-cart-plus"></i>
                 [LABEL-BUY]
             </a>
        </div>
    <?/* endif; */?>
  <div class="lds-dual-ring"></div>
	 <?php if(0):?><div class="backurl"><a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>"><i class="fa fa-arrow-left"></i>&nbsp; [LABEL-RETURN-BACK]</a></div><?php endif;?>
    <div class="form_wrapper sc-form-container mob_sc_wrapper">
    <div class="form_container">
      <? if($ongoing_package === false): ?>
      <div class="title_container">
        <h2>[LABEL-UPGRADE-AD]   &nbsp;<span class="ad_id">#<?= $ad_id ?></span></h2>
        <h4>[LABEL-UPGRADE-AD-SUB]</h4>
      </div>
      <div class="row clearfix">
        
            <form id="sc-form" class="sc-form-mobile" action="<?=\Core\Utils\Navigation::urlFor('payment', 'shoping-cart')?>">
            <input type="hidden" name="user_id" value="<?= $user_id ?>">
            <input type="hidden" name="ad_id" value="<?= $ad_id ?>">

           <div class="label-wrapper"><label>[LABEL-CATEGORY]</label></div>
           <div class="dropdown">
                <div class="select">
                    <span><?php echo $zone== 'montreal_metro'? '[LABEL-MONTREAL-MAIN]' : '[LABEL-OTHER-MAIN]';?></span>
                    <i class="fa fa-chevron-down"></i>
                </div>
                <input type="hidden" name="category" value="mtl_main">
                <ul class="dropdown-menu">
                    <li value="mtl_main"><?php echo $zone== 'montreal_metro'? '[LABEL-MONTREAL-MAIN]' : '[LABEL-OTHER-MAIN]';?></li>
                    <? 
						switch ($category) {
							case "escort":
								$main_plus_txt = '[LABEL-MONTREAL-ESCORTS]';	
								echo '<li value="mtl_escort">[LABEL-MONTREAL-ESCORTS]</li>';
								break;
							case "escort-agency":
								$main_plus_txt = '[LABEL-MONTREAL-ESCORT-AGENCIES]';	
								echo '<li value="mtl_escort-agency">[LABEL-MONTREAL-ESCORT-AGENCIES]</li>';
								break;
							case "erotic-massage":
								$main_plus_txt = '[LABEL-MONTREAL-MASSAGE]';	
								echo ' <li value="mtl_erotic-massage">[LABEL-MONTREAL-MASSAGE]</li>';
								break;
							case "erotic-massage-parlor":
								$main_plus_txt = '[LABEL-MONTREAL-PARLOR]';	
								echo '<li value="mtl_erotic-massage-parlor">[LABEL-MONTREAL-PARLOR]</li>';
								break;
						}
					?>
					<li value="mtl_main-plus"><?php echo $zone== 'montreal_metro'? '[LABEL-MONTREAL-MAIN]' : '[LABEL-OTHER-MAIN]';?> + <?= $main_plus_txt ?></li>
                </ul>
            </div>
           
          <div class="label-wrapper"><label>[LABEL-CITY]</label></div>
            <div class="dropdown">
                <div class="select">
                  <span><?= $city ?></span>
                  <i class="fa fa-chevron-down"></i>
                </div>
                <input type="hidden" name="city" value="">
                <ul class="dropdown-menu">
                    <li><?= $city ?></li>
                </ul>
            </div>
          
         

          <div class="label-wrapper"><label>[LABEL-PACKAGE-OPTION]</label></div>
            <div class="package">
                        
                        <div class="dropdown">
                            <div class="select">
                              <span><?= $available_packages[0]['name'] ?></span>
                              <i class="fa fa-chevron-down"></i>
                            </div>
                            <input type="hidden" name="package" value="19">
                            <ul class="dropdown-menu">
                              <? 
                                foreach($available_packages as $available_package){
                                  echo ' <li  value="'.$available_package["id"].'">'.$available_package["name"].' [LABEL-DAYS]'.'</li>';
                                } 
                              ?>
                             
                            </ul>
                        </div>
            </div>
            <?php
            if(Core\Utils\Session::getCurrentUserID() == 63661 || 1 ){ 
          ?>

          <div class="radio-group">
            <div style="padding-bottom: 20px">
            [LABEL-PAYMENT-METHOD]: &nbsp; &nbsp;
            </div>
            <div>
                <input type="radio" id="payment_method_card" name="payment_method" value="card" checked="checked">
                <label for="payment_method_card"> [LABEL-CARD]</label>           
                <input type="radio" id="payment_method_bitcoin" name="payment_method" value="bitcoin">
                <label for="payment_method_bitcoin"> Bitcoin</label>
                 <input type="radio" name="payment_method" id="payment_method_eth" value="eth">
                 <label for="payment_method_eth"> Ethereum</label>
            </div>
           </div>
           <?php
           }else{
           ?>
            <input type="hidden" name="payment_method" value="card">
            <?php
           }
           ?>
          <!--<div class="city"> 
                    <div class="label-wrapper">
                        <label>[LABEL-CITY]:</label><span class="location"><i aria-hidden="true" class="fa fa-map-marker"></i><?= $city ?></span>
                    </div>
                </div> -->
           <div class="errors">
            
          </div> 
          <div class="payment-line"> 
            <span id="price" class="mk_prem_blnc">Total: <span id="amount">24</span> CAD</span>
            <button id="sc-form-submit" class="button make_ad_premium" type="submit" style="display: block;">[LABEL-MAKE-PREMIUM]<i class="fa fa-star" style="margin-left: 10px;"></i></button>
            
        </div>
          
         
        </form>
         <div style="font-size: 14px; color:#fff; line-height:22px;margin-bottom:60px;padding: 0px 41px;" id='bit-note'><h2>[LABEL-BITCOIN-TITLE]</h2>
        <span class ="note">[LABEL-BITCOIN-NOTE]</span>
        </div>
    </div>
     <? else: ?>
         <div class="title_container">
          <h2>[LABEL-YOU-ALREADY-PAID]</h2>
        </div>
      <? endif; ?>
  </div>
</div>
</div>
<form method="POST" name="criptoForm" id="bitform" action=""><input type="hidden" name="accountId" value="<?= $user_id ?>"><input type="hidden" name="adId" value="<?= $ad_id ?>"><input type="hidden" name="amount" id='bitamount' value="0" /><input type="hidden" name="language" id='bitlang' value="<?php echo $lang; ?>" /></form>
