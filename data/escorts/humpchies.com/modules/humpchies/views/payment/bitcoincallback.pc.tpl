<style>
  .form_wrapper{
    text-align: center;margin-bottom: 50px;
  }
   .form_wrapper h2{
    font-size: 20px;
    font-family: segoe ui;
    font-weight: 100;
    margin-top: 50px;
    margin-bottom: 50px;
    padding: 0px 110px;
  }
  .sc-button{
      background: #ff9900;
      font-weight: bold!important;
      border: none;
      padding: 11px 10px 10px 10px;
      outline: none;
      cursor: pointer;
      color: #000!important;
      line-height: 3rem;
      border-radius: 4px; 
      font-size: 1.1em;
      -webkit-transition: all 0.30s ease-in-out;
      -moz-transition: all 0.30s ease-in-out;
      -ms-transition: all 0.30s ease-in-out;
      transition: all 0.30s ease-in-out;
      text-transform: uppercase;border-radius: 4px;
  }
  .back-to-credits{
    margin-right: 5px;
  }
</style>
<div class="wrapper_content">
  <div class="form_wrapper">
  <? if(isset($request) && $request['status'] == 1): ?>
      <? if($is_bump == 1): ?>
        <h2>[LABEL-PAYMENT-SUCCESS-BUMP]</h2>
        <h2><?= \Core\Utils\Text::translateTextWithVars( '[LABEL-PAYMENT-SUCCESS-DESC-BUMP]', array('%count%' => $credit) ) ?></h2>
        <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>" class="sc-button" role="button">[LABEL-GO-TO-ADS]</a>
      <? else: ?>
         <h2>[LABEL-PAYMENT-SUCCESS]</h2>
         <h2><?= \Core\Utils\Text::translateTextWithVars( '[LABEL-PAYMENT-SUCCESS-DESC]', array('%amount%' => $amount, '%id%' => $ad_id, '%title%' => $title, '%category%' => $category, '%city%' => $city) ) ?></h2>
         <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>" class="sc-button" role="button">[LABEL-BACK-TO-ADS]</a>
      <? endif; ?>
  <? else: ?>
     <?php if(in_array($request['status'],[0,3,4]) ){ ?>
          <? if($is_bump == 1): ?>
            <h2>[LABEL-PAYMENT-WAIT-BUMP]</h2>
            <h2>[LABEL-PAYMENT-WAIT-DESC-BUMP]</h2>
             <a href="<?=\Core\Utils\Navigation::urlFor('payment', 'bumps-purchase')?>" class="sc-button back-to-credits" role="button">[LABEL-BACK-TO-CREDITS]</a>
             <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>" class="sc-button" role="button">[LABEL-GO-TO-ADS]</a>
          <? else: ?>
            <h2>[LABEL-PAYMENT-WAIT]</h2>
            <h2>[LABEL-PAYMENT-WAIT-DESC]</h2>
            
             <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>" class="sc-button" role="button">[LABEL-BACK-TO-ADS]</a> 
          <? endif; ?>
     <?php }else{ ?>   
         <? if($is_bump == 1): ?>
            <h2>[LABEL-PAYMENT-FAIL-BUMP]</h2>
            <h2>[LABEL-PAYMENT-FAIL-DESC-BUMP]</h2>
             <a href="<?=\Core\Utils\Navigation::urlFor('payment', 'bumps-purchase')?>" class="sc-button back-to-credits" role="button">[LABEL-BACK-TO-CREDITS]</a>
             <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>" class="sc-button" role="button">[LABEL-GO-TO-ADS]</a>
          <? else: ?>
            <h2>[LABEL-PAYMENT-FAIL]</h2>
            <h2>[LABEL-PAYMENT-FAIL-DESC]</h2>
            <p>Error: <?= $_GET['mmg_errno'] ?></p>
             <a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>" class="sc-button" role="button">[LABEL-BACK-TO-ADS]</a> 
          <? endif; ?>
     <?php } ?>     
  <? endif; ?>
  </div>
</div>
