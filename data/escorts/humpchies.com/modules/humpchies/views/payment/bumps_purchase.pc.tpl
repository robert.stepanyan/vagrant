<style type="text/css">
.radio-grup {
    color: #f90;
    padding: 20px 0px;
    font-size: 16px;
}
.radio-grup input{
    margin:0px 5px 0px 10px;
}
.bitcoin{
    display:none;
}
#layout, #body {
    min-height: 600px;
}
.radio-group label{
    margin-right:15px;
}
.checked-text{
    margin-bottom: 20px;
    line-height: 21px;
    width: 389px;
    text-align: left;
    font-family: "Roboto";
    font-size: 16px;
    padding-top: 12px;
    
 }
 .grey-separator{
     width:100%;
     height:1px;
     border-bottom:1px solid #707070;
 }
 .payments{
     display:flex;
     justify-content: center;
     margin-top: 20px;
 }
 .payment-item{
     width: 315px;
     border: 1px solid #FF9900;
     border-radius: 5px;
     text-align: left;
     padding: 21px;
     margin: 39px 15px;
     font-family: "Roboto";
 }
 .premium:before, .bump:before{
    content: "";
    background-image:url("<?=Modules\Humpchies\Utils::getCDNPath('images')?>premium-star.png");
    content: "";
    width: 59px;
    height: 58px;
    position: absolute;
    top: -38px;
    left: 125px;
    display: block;
 }
 .bump:before{
    content: "";
    background-image:url("<?=Modules\Humpchies\Utils::getCDNPath('images')?>bump-up.png");
 }
 .payment-item-title{
     color:#FF9900;
     font-size: 16px;
     text-align: center;
     padding: 7px 15px 15px;
 }
 .payment-item-options {
    line-height: 18px;
    font-size: 14px;
    font-family: "roboto";
    color: #A4A4A4;
}
a.button-submit{
    background-color: #FF9900;
    border: 0px;
    width: 100%;
    padding: 15px;
    font-size: 16px;
    border-radius: 5px;
    margin-top: 22px;
    display: block;
    color:#000;
    text-align: center;
}
a.button-submit:hover{
    background-color: #e58900;    
}
a.lame-link {
    display: inline-block;
    width: 40%;
    border: 1px solid #FF9900;
    padding: 10px;
    border-radius: 5px;
    font-size: 14px;
    font-family: "Roboto";
    margin-top: 31px;
} 

.lame a, .lame div {
    margin: 20px;
    padding: 10px;
    font-size: 14px;
}
.lame{
    display:flex;
    justify-content: center;
}

</style>

<div class="create-ad" style="display:none">
    
    <div class="content_box" >
        <div  class='grey-separator' style=""></div>
        <div class="content_box_header" style="margin: 30px auto;margin-bottom: 0px; text-align:center; display:block">
            <div style="display:flex;width: 55%;margin: 10px 24%;     margin-bottom: 20px;">
            <img src="<?=Modules\Humpchies\Utils::getCDNPath('images')?>checked_white.svg" border="0" width="65" height="65" alt="" style="margin: 2px auto;"/>
            <div class="checked-text">Payment successful!                                                                                                  
                Congratulations, your payment was successful and your Premium package {days} will start as soon as your ad is approved.
            </div>
        </div>
        <div  class='grey-separator' style=""></div>    
        
        
        <div class='lame' >
           <div>In the meantime, you can:</div> <a href="<?php echo \Core\Utils\Navigation::urlFor('ad', 'verify'); ?>?id=<?php echo $adid; ?>" class="lame-link" style='width:auto'> VERIFY YOUR AD</a>  <a href="/ad/my-listings" class="lame-link" style='width:auto'> GO TO MY ADS</a>
        </div>
    </div>
</div>            
</div>
<div class="wrapper_content_full">
  <div class="lds-dual-ring"></div>
	<div class="backurl"><a href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>">[LABEL-RETURN-BACK]</a></div>
	<div class="bumps-purchase-wrapper">
    <div class="row">
      <div class="col">
        <h2>[LABEL-BUMP-CREDITS-PURCHASE]</h2>
        <p class="white">[BUMP-CREDIT-DESC]</p>
        <hr>
         <h2>[LABEL-BUMP-CREDITS-CUR-BALANCE]</h2>
         <h2><span id="current-balance"><?= $current_credits ?></span>&nbsp;[LABEL-CREDITS]</h2>
         <p>[LABEL-BUMP-CREDITS-NEW-BALANCE]:<br><?= $current_credits ?> +&nbsp;<span class="selected-credits">20</span>&nbsp;=&nbsp;<span class="total-bumps"><?= $current_credits + 20 ?></span>&nbsp;[LABEL-CREDITS]</p>
      </div>
      <div class="col dark">
          <h2>[LABEL-BUMP-CREDITS-SELECT]</h2>
          <p class="white">[BUMP-CREDIT-DESC-2]</p>
          <form id="bumps-purchase" action="<?=\Core\Utils\Navigation::urlFor('payment', 'bumps-purchase')?>">
          <?php
            if(Core\Utils\Session::getCurrentUserID() == 63661 || 1){ 
          ?>
           <div class="radio-group">
            Payment Method:
            <br/><br/>
            <input type="radio" name="payment_method" id="payment_method_card" value="card" checked="checked">
            <label for="payment_method_card"> Card</label>
            <input type="radio" name="payment_method" id="payment_method_bitcoin" value="bitcoin">
            <label for="payment_method_bitcoin"> Bitcoin</label>
            <input type="radio" name="payment_method" id="payment_method_eth" value="eth">
            <label for="payment_method_eth"> Ethereum</label>
            
           </div>
           <?php
           }else{
           ?>
            <input type="hidden" name="payment_method" value="card">
           <?php
           }
           ?>
            <input type="hidden" name="user_id" value="<?= $user_id ?>">
            <input type="hidden" name="ad_id" value="<?= $_GET['ad_id'] ?>">

            <div class="form-wrapper">
				<div class="dropdown">
                <div class="select">
                <?php
                     if( 0){  //-- changes
                ?>
                  <span>20 + 5 [LABEL-CREDITS]: 20 cad</span>

                  <i class="fa fa-chevron-down"></i>
                </div>
                <input type="hidden" name="credits" value="cr_25">
                
                <?php
                    }else{
                ?>
                  <span>20 [LABEL-CREDITS]: 20 cad</span>
      
                  <i class="fa fa-chevron-down"></i>
                </div>
                <input type="hidden" name="credits" value="cr_20">
                <?php }?>
                <ul class="dropdown-menu">
					<? 
                    if( 0){
                        foreach(Modules\Humpchies\BumpsModel::$CREDIT_OPTIONS_special as $option => $amount){
                    ?>
                            <li value="<?=$option?>" class="card"> <?=(str_replace('cr_','',$option) - Modules\Humpchies\BumpsModel::$bonus[$option])?> +<?php echo \Modules\Humpchies\BumpsModel::$bonus[$option]?> Credits : <?= $amount ?> cad</li>
                            <li value="<?= $option ?>" class="bitcoin"> <?=(str_replace('cr_','',$option) - Modules\Humpchies\BumpsModel::$bonus[$option])?> +<?php echo \Modules\Humpchies\BumpsModel::$bonus[$option]?>  Credits: <?= \Modules\Humpchies\PaymentModel::credit_price($amount)?> cad</li>
                    <?php
                        }    
                    }else{
                    foreach(Modules\Humpchies\BumpsModel::$CREDIT_OPTIONS as $option => $amount){?>
						<? $credits = str_replace('cr_','',$option); ?>
                        <li value="<?= $option ?>" class="card"> <?= $credits ?> Credits: <?= $amount ?> cad</li>
						<li value="<?= $option ?>" class="bitcoin"> <?= $credits ?> Credits: <?= \Modules\Humpchies\PaymentModel::credit_price($amount)?> cad</li>
					<? } }?>
				</ul>
				</div>
            <div>
              <button id="bumps-purchase-submit" type="submit" class="button">[LABEL-BUMP-CREDITS-PURCHASE]&nbsp;<i class="fa fa-shopping-cart"></i></button>
            </div>
            </div>
            <div class="errors"></div>
          </form>
      </div>
    </div>
  </div>
</div>
<form method="POST" name="criptoForm" id="bitform" action=""><input type="hidden" name="accountId" value="<?= $user_id ?>"><input type="hidden" name="amount" id='bitamount' value="0" /><input type="hidden" name="language" id='bitlang' value="<?php echo $lang; ?>" /></form>
