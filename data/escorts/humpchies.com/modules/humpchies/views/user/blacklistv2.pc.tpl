<style>
    div.user_content_photo {}
    div.user_content_photo > div { width: 100% !important; padding: 0 !important; height: 100% !important; }
    div.user_content_photo > div > a { display:block !important; width: 100% !important; height:100% !important; background-color: #111 !important; }
    div.user_content_photo > div span { display: inline-block !important; height: 100% !important; vertical-align: middle !important; }
    div.user_content_photo > div img { position: static !important; vertical-align: middle !important; display: inline-block !important; }
    .buttons { display: flex;justify-content: space-between; margin: 11px 0 16px 0; font-weight: bold;}
    
    .buttons > .previous {  padding: 10px; background-color: #222; border-radius: 4px; color: #ff9900;  text-transform: uppercase; cursor: pointer; display:inline-block:}
    .buttons > .next {  padding: 10px; background-color: #222; border-radius: 4px; color: #ff9900;  text-transform: uppercase; cursor: pointer; display:inline-block:}
   
    input[type="date"]::-webkit-calendar-picker-indicator {
        color: #f90;
    }
   
    #description{
        width: calc(100% - 23px);
        height: 245px;
        background-color: #222;
        border-color: #fff;
        resize: none;
        outline: none;
        padding: 10px;
        border-radius: 5px;
        font-size: 14px;
    }
    .addblacklist .text{
        width:100% !important;
        background: transparent !important;
    }
    .addblacklist {
        padding: 10px;
        background: #222;
        border-radius: 5px;
        font-size:14px;
        line-height: 20px;
        font-family: "roboto";
    }
    .addtitle {
        font-size: 18px;
        color:#fff;
        margin-bottom:25px;
        text-align: right;
        
    }
    .button {
        width: 50px;
        width: 82px;
        cursor: pointer;
        cursor: pointer;
        user-select: none;
        background-color: #f90 !important;
        border-radius: 8rem;
        height: 36px;
        font-size: 14px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: black;
        text-transform: uppercase;
        padding: 0px 10px;
        margin-left: 5px;
        float: right;
        margin-top: 17px;
        display: inline-block;
    }
    .button-outline {
        width: 82px;
        cursor: pointer;
        user-select: none;
        background-color: transparent;
        border-radius: 8rem;
        height: 36px;
        font-size: 14px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: #f90;
        text-transform: uppercase;
        padding: 0px 10px;
        margin-left: 5px;
        float: right;
        margin-top: 17px;
        display: inline-block;
        border: #f90 1px solid;
        margin-left: 50px;
    }
    .country-layout {
        display: flex;
        align-content: space-between;
        margin-bottom:20px;
    }
    
    .country-layout > div:first-child {
        margin-right: 15px;   
    }
    
    .country-layout .field {
        min-width: 287px;
        
    }
    .custom-select {
       background: none; 
    }
    
    .custom-select option {
        background: #222;
        color: #fff;
    }
    .custom-select select {
        border: 1px solid #ff9000;
        outline: none;
        background: none;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        border-radius: 4px;
        margin: 0;
        display: block;
        width: 100%;
        padding: 9px 25px 9px 15px;
        font-size: 14px;
        color: #ff9000;
        height: 46px;
    }
    .custom-select:after {
        position: absolute;
        right: -7px;
        top: 15px;
        width: 50px;
        height: 100%;
        line-height: 38px;
        content: "\f184";
        text-align: center;
        color: #ff9000;
        font-size: 20px;
        border-left: 0;
        z-index: -1;
        font: normal normal normal 14px/1 FontAwesome;
    }
    input#search {
        height: 18px;
        line-height: 18px;
        padding: 10px;
        background: #fff;
        border-radius: 4px;
        width: 355px;
        padding-right: 30px;
        font-size: 16px;
        color: #000;
    }
    
    .item {
        border-bottom: #000 1px solid;
        padding: 20px 0px;
        line-height: 16px;
    }
    .item-head {
        margin-bottom: 20px;
    }
    span.name {
        color: #ff9900;
        margin-right: 15px;
    }
    .content_box {
        padding: 20px;
        background: #222;
        margin-top:20px;
    }
    .pagination-content{
        margin-top:20px;
        height: 35px;
    }
    .search-button{
        width: 70px;
        height: 37px;
        border-radius: 4px;
        margin-left: 10px;
        background: #f90 !important;

    }
      .required{
            margin-left: 20px;
            color:#ff0000;
        }
    .search-container {
        display: flex;
    }
    @media only screen and (max-width: 600px) {
        .country-layout {
            display: block;
            
        }
        .country-layout .field {    
            height: 75px;
            min-width: auto;
        }
        .country-layout > div:first-child {
            margin-right: 0px;   
        }
      
 
    }
    .black-list-title{
        display:flex;
        font-size: 24px;
        color:#fff; 
        margin-bottom: 15px;      
    }
    .black-list-title img{
        margin-right: 6px;
    }
    .make-entry{
        vertical-align: middle;
        margin-top: 11px;
        margin-left: 5px;
    }
    
    .add-entry img{
        width:20px;
    }
    .add-entry{
        color:#ff9900;
        display:flex;
        margin-left:15px;
        font-size: 16px;
        font-weight: 300;
        font-family: Roboto;
        text-transform: uppercase;
        width:190px;
        cursor:pointer;
    }
    .add-entry:hover{
        color:#fff;    
    }
    
    .search-container-new{
        width: calc(100% - 393px);
        text-align: right;
    }
  .button-search {
    position: absolute;
    right: 3px;
    width: 32px;
    height: 32px;
    background: #ff9900;
    top: 3px;
    border-radius: 5px;
    min-width: 32px;
    min-height: 32px;
    padding: 0px;
    color: #fff !important;
    font-weight: 500;
    font-size: 16px;
}

.row {
    display: flex;
    color:#AFAFAF;
    font-size: 16px;
    min-height: 30px;
}
.row.header{
    font-weight: 500;
    border-bottom: 2px solid #585757;
}
.column-0 {
    width: 130px;
}
.column-1 {
    width: 150px;
}
.column-2 {
    width: 150px;
}
.column-3{
        width: 150px;
}
.column-4 {
    width: 418px;
}

.list-body .row{
    padding: 20px 0;
     border-bottom: 1px solid #585757;    
}

</style>

<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <?php if($search) { ?>
        <li> <a href="<?php echo \Core\Utils\Navigation::urlFor('user', 'blacklist');?>">[LABEL-BLACKLIST-CLIENTS]</a></li>
        <li> [LABEL-BLACKLIST-CLIENTS] <?='&laquo;&nbsp;' . $search . "&nbsp;&raquo;" ?></li>
        <?php }else{ ?>
        <li> [LABEL-BLACKLIST-CLIENTS]</li>
        <?php } ?>
    </ul>    
 </nav>

  

     <div class="addblacklist">
        <div class="Info mb-15">
        <div class="black-list-title">
            <img src="http://gui.humpchies.com/images/path2097.svg"/> 
            <div style="margin-top: 10px; width:158px">Client Blacklist</div> 
            <div class="add-entry" id="add">
                <img src="http://gui.humpchies.com/images/group271.svg"> 
                <div class="make-entry">make an Entry</div>
            </div>
            
                <div class="search-container-new">
                <form method="GET">
                    <input type='text' name="search" value="" placeholder = "[LABEL-SEARCH-PLACEHOLDER]" id='search'/>
                    <button type="submit"  class ='button-search' id ='search' ><i class="fas fa-search"></i></button>
                 </form>    
                </div>
           
        </div>
        <div id="info-detalii">
            <p>Sometimes we are getting feedbacks from escorts which are complaining about rude, drunk or aggressive clients - or clients which don't want to pay or are stealing money. Therefore we decided to create a client blacklist which is accesabile for all agencies and escorts. You can simply browse the list or search specific information using the search form below.</p>
            <p>If yourself have made bad experience of any kind, help others to avoid meeting those bad clients and add as much information as possible to the black list.</p>
            <p><a href="hideme()"> IT, HIDE THIS IN THE FUTURE.</a></p> 
        </div>
        </div>
        <form name="createblacklist" method="POST" id='form-show' style="display: none;">
            <div class="country-layout">
                <div class="field mb-15" >
                    <label for="country">[LABEL-COUNTRY]</label>
                    <div class=" mt-5">
                        <label class="custom-select">
                        <select name="country" id="country" data-must-be="">
                            <option value=''>[LABEL-SELECT-COUNTRY]</option>
                            <?php foreach($clist as $key => $list):?>
                                <option value="<?php echo $key ?>"> <?=$list ?> </option>      
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <input type="hidden" name="country_name" id="country_name">
                <div class="field mb-15">
                    <label for="city">[LABEL-CITY]</label>
                    <div class=" mt-5">
                        <label class="custom-select" >
                        <select name="city" id="city" data-must-be="" disabled="disabled">
                            <option value=''></option>
                           
                        </select>
                        </label>
                    </div>
                </div>
            
            </div>
            
            <div class="field mb-15">
                <label for="name">[LABEL-NAME]<span class="required">*</span></label>
                <input name="name" id = "name" class="text" required/>
            </div> 
            <div class="country-layout">
                <div class="field mb-15" >
                       <label for="date"> Date</label>
                <input name="date"  id ="date"  type='text' class='datepicker-here text' data-language='<?php echo $xlang; ?>' readonly="true"/>
                </div>
                <div class="field mb-15">
                    <label for="phone">[LABEL-TELEPHONE]<span class="required">*</span></label>
                    <input type="tel" name="phone" id = "phone" class="text" maxlength="10" required/>
                </div>
            </div>
            <div class="field mb-15">
                <label for="description">[LABEL-BLACKLIST-COMMENT]<span class="required">*</span></label>
                <div class="mt-5">
                    <textarea name="description" id = "description"  style='height: 135px;' required/>
                    </textarea>
                </div>
            </div>
            <div class="field mb-15">
                <input type="submit" value="[LABEL-SEND]"  style="max-width: 83px; padding-top: 4px; padding-bottom: 4px; min-width: 83px; height: 30px;  border-radius: 4px;  text-align: center;  margin-top: 0px; background: #f90; color: #000; -webkit-appearance:none"/>
                <input type="reset" value='[LABEL-CLOSE]'  id='close'  style=" max-width: 83px; padding-top: 4px; padding-bottom: 4px; min-width: 83px; height: 30px;  border-radius: 4px;  text-align: center;  margin-top: 0px; border:1px solid #f90; background: transparent; margin-left :30px; color:#f90">
            </div>
            
        </form>
    </div>
 <div class="wrapper_content" style="width: calc(100% - 20px);">
    <div class="content_box" style="padding:10px;">
       <div class="list">
            <div class="row header">
                <div class="column-0">Name</div>
                <div class="column-1">Date</div>
                <div class="column-2">City</div>
                <div class="column-3">Phone</div>
                <div class="column-4">Description</div>
            </div>
       </div>
       
       <div class="list-body">
            <?php foreach($blacklist as $k => $row) { ?>
            <div class="row">
                <div class="column-0"><?php echo $row['name']?></div>
                <div class="column-1"><?=date("m/d/Y", strtotime($row['date']))?></div>
                <div class="column-2"><?=$row['country']?></span>,&nbsp;<span class="gri"><?=$row['city']?></span></div>
                <div class="column-3"><?=$row['phone']?></div>
                <div class="column-4"> <?=nl2br(\Core\Utils\Security::htmlEncodeText($row['description']))?></div>
            </div>
              
            <?php } ?>
       </div>
   
   </div>
        
    <?php \Core\Controller::renderPlugin('paginator', NULL, \Core\View::DEVICE_MODE);?>    
    </div>
 </div>
