<script>
      var components = {fonts: {extendedJsFonts: true}};
    if (window.requestIdleCallback) {
    requestIdleCallback(function () {
        Fingerprint2.get(function (components) {
          console.log(components) ;
           var values = components.map(function (component) { return component.value });
           var murmur = Fingerprint2.x64hash128(values.join(''), 31);
           console.log(murmur);
           console.log(JSON.stringify(components));
          document.getElementById("components").value = JSON.stringify(components);
          document.getElementById("cq").value = murmur;
          
        })
    })
} else {
    setTimeout(function () {
        Fingerprint2.get(function (components) {
          console.log(components) ;
          var values = components.map(function (component) { return component.value });
          var murmur = Fingerprint2.x64hash128(values.join(''), 31);
          document.getElementById("components").value = JSON.stringify(components);
          document.getElementById("cq").value = murmur;
        })  
    }, 500);
}
    </script>
<nav id="breadcrumb" class="mb-30">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li> <?php echo $title ? $title: '[LABEL-LOGIN]';?></li>
    </ul>
</nav>    
<h2 class="stylized-title"><span><?php echo $title ? $title: '[LABEL-LOGIN]';?></span></h2>
    
<div class="wrapper_content">
    <div class="content_box">

        <div class="content_box_body user-login">
            
                <form id="login_form" method="post">
                    <div class="field">
                    <label>[LABEL-USERNAME] [LABEL-OR] [LABEL-EMAIL]:</label>
                    <input name="username_or_email" type="text" class="form-control" maxlength="<?=$username_or_max_len_email?>" value="<?=(isset($_POST['username_or_email']) ? $_POST["username_or_email"] : '')?>" />
                    </div>
                
                    <div class="field">
                    <label>[LABEL-PASSWORD]:</label>
                    <input name="password" type="password" class="form-control" maxlength="<?=$password_max_len?>" value="<?=(isset($_POST['password']) ? $_POST['password'] : '')?>" />
                    </div>
                    
                
                <?php if($show_captcha ): ?>
                        <input name='with_captcha' value='1' type='hidden' />
                    <div class="field __captcha" style="margin-bottom: 15px;">
                            <label><p>[LABEL-CAPTCHA]:</p></label>
                            <div>
                                <span class="code">
                                    <img src="/captcha.gif">
                                </span>
                            </div>  
                        </div>
                    
                        <div class="field __captcha">
                        <label>[LABEL-TYPE-CAPTCHA]:</label>
                        <input class="form-control" name="code" type="text" value="" maxlength="<?=$max_len_captcha?>" />
                        </div>
                    <?php endif; ?>
                    
                    <div class="field __btn">
                        <div>
                            <input class="button" type="submit" value="[BTN-CAPTION-LOGIN]" />
                        </div>
                    </div>
                    <input type="hidden" name='components' id='components'> 
                    <input type="hidden" name='kq' id='cq'> 
                </form>
                <p>
                    <ul class="bullet">
                        <li>
                            <a title="[HREF-TITLE-REQUEST-PASSWORD-RESET]" href="<?=\Core\Utils\Navigation::urlFor('user', 'request-password-reset')?>">[PROMPT-REQUEST-PASSWORD-RESET]</a>    
                        </li>
                        <li>
                            [PROMPT-SIGN-UP]&nbsp;<a title="[HREF-TITLE-SIGN-UP]" href="<?=\Core\Utils\Navigation::urlFor('user', 'signup')?>">[LABEL-SIGN-UP]!</a>
                        </li>
                    </ul>
                </p>
           
        </div>
    </div>
</div>
