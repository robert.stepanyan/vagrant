<nav id="breadcrumb" class="mb-30">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li> [LABEL-RESET-PASSWORD]</li>
    </ul>
</nav> 
 <h2 class="stylized-title"><span>[LABEL-RESET-PASSWORD]</span></h2>

<div class="wrapper_content">
    <div class="content_box">
        
        <div class="content_box_body user-login">
            
                <form id="sign_up_form" method="post">
                    <div class="field">
                        <label>[LABEL-USERNAME] [LABEL-OR] [LABEL-EMAIL]:</label>
                    <input class="form-control" name="username_or_email" type="text" maxlength="<?=$username_or_max_len_email?>" value="<?=(isset($_POST["username"]) ? $_POST["username"] : '')?>" />
                        </div>
                
                <div class="field" style="margin-bottom: 15px;">
                        <label>[LABEL-CAPTCHA]:</label>
                        <div>
                            <span class="code">
                                <img src="/captcha.gif">
                            </span>
                        </div>
                    </div>
                
                    <div class="field">
                        <label>[LABEL-TYPE-CAPTCHA]:</label>
                    <input class="form-control" name="code" type="text" value="" maxlength="<?=$max_len_captcha?>" />
                    
                    </div>
                
                    <div class="field">
                        <div>
                            <input class="button" type="submit" value="[BTN-CAPTION-REQUEST-PASSWORD-RESET]" />
                        </div>
                    </div>
                </form>
                <p>
                    <ul class="bullet">
                        <li>
                            [PROMPT-SIGN-UP]&nbsp;<a title="[HREF-TITLE-SIGN-UP]" href="<?=\Core\Utils\Navigation::urlFor('user', 'signup')?>">[LABEL-SIGN-UP]!</a>
                        </li>
                    </ul>
                </p>
            
        </div>
    </div>
</div>