<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li>[LABEL-SETTINGS]</li>
    </ul>    
</nav>
<div class="wrapper_content_full">
    <div class="" id="change_pass">

        <h1>[LABEL-SETTINGS]</h1>


        <div class="info">
            <p style="font-size: 14px;color: #fff;border-bottom: 1px solid #403f3f;border-top: 1px solid #403f3f; padding: 15px 0;margin: 15px 0;">[LABEL-SETTINGS-INFO]: 
                <span style="font-weight: bold; margin-left: 10px;"><?php echo $user_details['username'];?> (<?php echo \Core\Utils\Session::getCurrentUserID();?>)</span></p>
        </div>

        <form id="settings-form" class="settings-form" action="" method="post" enctype="multipart/form-data">
            <?php if(Core\Utils\Session::currentUserIsAdvertisment()):?>     
                <?php if ($user_details['disable_comments'] < 2):?>
                <div class="checkbox-group">      
                   <input class="styled-checkbox" name="disable_comments" id="disable_comments" type="checkbox" value="1" <?php echo (($user_details['disable_comments'] == 1)? 'checked' : ''); ?> />
                    <label for="disable_comments" style="color:#fff; font-size:16px;font-weight: normal;">[LABEL-BLOCK-COMMENTS]</label>
            </div>
                <?php else:?>
                    <input type="hidden"  name="disable_comments" id="disable_comments" value="2" />
                <?php endif;?>
                <br/>
               
                <div class="checkbox-group">
                    <input type="checkbox" class="styled-checkbox" id="disable_pm" name="disable_pm" value="1" <?php echo (($user_details['disable_pm'] == 1)? 'checked' : ''); ?> />
                    <label for="disable_pm" style="color:#fff; font-size:16px;font-weight: normal;">[LABEL-BLOCK-PM]</label>
            </div>
                <br/>
            <?php endif;?>
            <div class="field">
                <div>
                    <label for="current_password">[LABEL-PASSWORD]</label>
                    <input id="current_password" class="text" type="password" name="current_password" value="" style="width: 100%;">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="field">
                <div>
                    <label for="new_password">[LABEL-NEW-PASSWORD]</label>
                    <input id="new_password" class="text" type="password" name="new_password" value="" style="width: 100%;">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="field">
                <div>
                    <label for="confirm_password">[LABEL-NEW-PASSWORD-CONFIRMATION]</label>
                    <input id="confirm_password" class="text" type="password" name="confirm_password" value="" style="width: 100%;">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="field actions">
                <button type="submit" class="button">[BTN-UPDATE]</button>
            </div>
        </form>

    </div>
</div>