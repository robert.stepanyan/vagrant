<style>
    div.user_content_photo {}
    div.user_content_photo > div { width: 100% !important; padding: 0 !important; height: 100% !important; }
    div.user_content_photo > div > a { display:block !important; width: 100% !important; height:100% !important; background-color: #111 !important; }
    div.user_content_photo > div span { display: inline-block !important; height: 100% !important; vertical-align: middle !important; }
    div.user_content_photo > div img { position: static !important; vertical-align: middle !important; display: inline-block !important; }
    .buttons { display: flex;justify-content: space-between; margin: 11px 0 16px 0; font-weight: bold;}
    
    .buttons > .previous {  padding: 10px; background-color: #222; border-radius: 4px; color: #ff9900;  text-transform: uppercase; cursor: pointer; display:inline-block:}
    .buttons > .next {  padding: 10px; background-color: #222; border-radius: 4px; color: #ff9900;  text-transform: uppercase; cursor: pointer; display:inline-block:}
   
    input[type="date"]::-webkit-calendar-picker-indicator {
        color: #f90;
    }
   
    #description{
        width: calc(100% - 23px);
        height: 245px;
        background-color: #222;
        border-color: #fff;
        resize: none;
        outline: none;
        padding: 10px;
        border-radius: 5px;
        font-size: 14px;
    }
    .addblacklist .text{
        width:100% !important;
        background: transparent !important;
    }
    .addblacklist {
        padding: 10px;
        border-radius: 5px;
    }
    .addtitle {
        font-size: 18px;
        color:#fff;
        margin-bottom:25px;
        text-align: right;
        
    }
    .button {
        width: 50px;
        width: 82px;
        cursor: pointer;
        cursor: pointer;
        user-select: none;
        background-color: #f90 !important;
        border-radius: 8rem;
        height: 36px;
        font-size: 14px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: black;
        text-transform: uppercase;
        padding: 0px 10px;
        margin-left: 5px;
        float: right;
        margin-top: 17px;
        display: inline-block;
    }
    .button-outline {
        width: 82px;
        cursor: pointer;
        user-select: none;
        background-color: transparent;
        border-radius: 8rem;
        height: 36px;
        font-size: 14px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: #f90;
        text-transform: uppercase;
        padding: 0px 10px;
        margin-left: 5px;
        float: right;
        margin-top: 17px;
        display: inline-block;
        border: #f90 1px solid;
        margin-left: 50px;
    }
    .country-layout {
        display: flex;
        align-content: space-between;
        margin-bottom:20px;
    }
    
    .country-layout > div:first-child {
        margin-right: 15px;   
    }
    
    .country-layout .field {
        min-width: 287px;
        
    }
    .custom-select {
       background: none; 
    }
    
    .custom-select option {
        background: #222;
        color: #fff;
    }
    .custom-select select {
        border: 1px solid #ff9000;
        outline: none;
        background: none;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        border-radius: 4px;
        margin: 0;
        display: block;
        width: 100%;
        padding: 9px 25px 9px 15px;
        font-size: 14px;
        color: #ff9000;
        height: 46px;
    }
    .custom-select:after {
        position: absolute;
        right: -7px;
        top: 15px;
        width: 50px;
        height: 100%;
        line-height: 38px;
        content: "\f184";
        text-align: center;
        color: #ff9000;
        font-size: 20px;
        border-left: 0;
        z-index: -1;
        font: normal normal normal 14px/1 FontAwesome;
         font-family: "Font Awesome 5 Free"
    }
    input#search {
    line-height: 18px;
    padding: 10px;
    border-radius: 4px;
    width: 100%;
    margin-bottom: 20px;
    font-size: 16px;
    height: 24px;
    padding-right: 47px;
    }
    
    .search_button{
        max-width: 44px;
        padding-top: 3px;
        padding-left: 5px;
        min-width: 34px;
        height: 34px;
        border-radius: 4px;
        text-align: center;
        margin-top: 0px;
        -webkit-appearance: none;
        position: absolute;
        right: 15px;
        top: 5px;
        bottom: 5px;
        width: 25px;
        float: none;
        display: block;
        font-size: 16px;
        left: auto;
        min-height: 34px;
    }
    
    .item {
        border-bottom: #000 1px solid;
        padding: 20px 0px;
        line-height: 16px;
    }
    .item-head {
        margin-bottom: 20px;
    }
    span.name {
        color: #ff9900;
        margin-right: 15px;
    }
    .content_box {
        padding: 10px;
    }
    .pagination-content{
        margin-top:20px;
        height: 35px;
    }
    .search-button{
        width: 70px;
        height: 37px;
        border-radius: 4px;
        margin-left: 10px;
        background: #f90 !important;
        color: #000;
    }
      .required{
            margin-left: 20px;
            color:#ff0000;
        }
    .search-container {
        display: flex;
        padding: 0 10px;
    }
    @media only screen and (max-width: 600px) {
        .country-layout {
            display: block;
            
        }
        .country-layout .field {    
            height: 75px;
            min-width: auto;
        }
        .country-layout > div:first-child {
            margin-right: 0px;   
        }
      
 
    }
    .black-list-title{
        display:flex;
        font-size: 24px;
        color:#fff; 
        padding: 10px; 
    }
    .black-list-title img{
        margin-right: 6px;
    }
    .make-entry{
        vertical-align: middle;
        margin-top: 5px;
        margin-left: 5px;
    }
    
    .add-entry img{
        width:20px;
    }
    .add-entry{
        color:#ff9900;
        display:flex;
        margin-left:15px;
        font-size: 16px;
        font-weight: 300;
        font-family: Roboto;
        text-transform: uppercase;
        width:190px;
    }
    
    div#info-detalii {
        padding: 20px 10px;
    }
    
    div#add {
    height: 44px;
    margin: 10px;
    background: #ff9900;
    border-radius: 5px;
    text-align: center;
    font-size: 16px;
    color: #000;
    margin-bottom: 20px;
}
.entry img{
    display: inline-block;
    vertical-align: -4px;
    margin-right: 9px;
}

.entry-text {
    display: inline-block;
    line-height: 44px;
}

.section-container {
    border-bottom: 2px solid #2C2E30;
    margin-bottom: 20px;
}

form#form-show {
    padding: 10px;
}

.item-row {
    display: flex;
    font-size: 14px;
    color: #AFAFAF;
    line-height: 24px;
}

.item-label {
    width: 30%;
}

.item-value {
    width: 70%;
    text-align: right;
}


.normal {
    font-size: 14px;
    line-height: 20px;
    margin-top: 20px;
    color: #AFAFAF;
}
.item {
    border-bottom: #2C2E30 2px solid;
    padding: 0px;
    line-height: 16px;
    padding-bottom: 20px;
}
</style>

<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <?php if($search) { ?>
        <li> <a href="<?php echo \Core\Utils\Navigation::urlFor('user', 'blacklist');?>">[LABEL-BLACKLIST-CLIENTS]</a></li>
        <li> [LABEL-BLACKLIST-CLIENTS] <?='&laquo;&nbsp;' . $search . "&nbsp;&raquo;" ?></li>
        <?php }else{ ?>
        <li> [LABEL-BLACKLIST-CLIENTS]</li>
        <?php } ?>
    </ul>    
 </nav>

  
 <div class="section-container">
    <div class="black-list-title"><img src="http://gui.humpchies.com/images/path2097.svg"/> Client Blacklist</div>
 </div>
  <div class="section-container ">

        <div id="info-detalii"  style="font-size: 16px;line-height: 20px;color: #AFAFAF;">
            <p>Sometimes we are getting feedbacks from escorts which are complaining about rude, drunk or aggressive clients - or clients which don't want to pay or are stealing money. Therefore we decided to create a client blacklist which is accesabile for all agencies and escorts. You can simply browse the list or search specific information using the search form below.</p>

            <p>If yourself have made bad experience of any kind, help others to avoid meeting those bad clients and add as much information as possible to the black list.</p>

            <p><a href="hideme()"> IT, HIDE THIS IN THE FUTURE.</a></p>
  
        </div>
  </div>
    <div class="section-container">
        <div class="entry" id="add" style=""><img src="http://gui.humpchies.com/images/path2124.svg"><div class="entry-text"> Make Entry</div></div>
            
            <form name="createblacklist" method="POST" id='form-show' style="display: none;">
                <div class="country-layout">
                    <div class="field mb-15" >
                        <label for="country">[LABEL-COUNTRY]</label>
                        <div class=" mt-5">
                            <label class="custom-select">
                            <select name="country" id="country" data-must-be="">
                                <option value=''>[LABEL-SELECT-COUNTRY]</option>
                                <?php foreach($clist as $key => $list):?>
                                    <option value="<?php echo $key ?>"> <?=$list ?> </option>      
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="country_name" id="country_name">
                    <div class="field mb-15">
                        <label for="city">[LABEL-CITY]</label>
                        <div class=" mt-5">
                            <label class="custom-select" >
                            <select name="city" id="city" data-must-be="" disabled="disabled">
                                <option value=''></option>
                               
                            </select>
                            </label>
                        </div>
                    </div>
                
                </div>
                
                <div class="field mb-15">
                    <label for="name">[LABEL-NAME]<span class="required">*</span></label>
                    <input name="name" id = "name" class="text" required/>
                </div> 
                <div class="country-layout">
                    <div class="field mb-15" >
                           <label for="date"> Date</label>
                    <input name="date"  id ="date"  type='text' class='datepicker-here text' data-language='<?php echo $xlang; ?>' readonly="true"/>
                    </div>
                    <div class="field mb-15">
                        <label for="phone">[LABEL-TELEPHONE]<span class="required">*</span></label>
                        <input type="tel" name="phone" id = "phone" class="text" maxlength="10" required/>
                    </div>
                </div>
                <div class="field mb-15">
                    <label for="description">[LABEL-BLACKLIST-COMMENT]<span class="required">*</span></label>
                    <div class="mt-5">
                        <textarea name="description" id = "description"  style='height: 135px;' required/>
                        </textarea>
                    </div>
                </div>
                <div class="field mb-15">
                    <input type="submit" value="[LABEL-SEND]"  style="max-width: 83px; padding-top: 4px; padding-bottom: 4px; min-width: 83px; height: 30px;  border-radius: 4px;  text-align: center;  margin-top: 0px; background: #f90; color: #000; -webkit-appearance:none"/>
                    <input type="reset" value='[LABEL-CLOSE]'  id='close'  style=" max-width: 83px; padding-top: 4px; padding-bottom: 4px; min-width: 83px; height: 30px;  border-radius: 4px;  text-align: center;  margin-top: 0px; border:1px solid #f90; background: transparent; margin-left :30px; color:#f90">
                </div>
            
        </form>
    </div>
     <div class="search-form section-container ">
    <form method="GET">
    <div class="search-container">
        <input type='text' name="search" value="" placeholder = "[LABEL-SEARCH-PLACEHOLDER]" id='search'/>
        <button type="submit"  class="button search_button" id="search" ><i class="fas fa-search"></i></button>
    </div>
    </form>
      </div>
    <div class="content_box">
        <div class="content_box_body">
            
            <div class="list-body">
                <?php foreach($blacklist as $k => $row) { ?>
                    <div class="item">
                            <div class="item-row">
                                <div class="item-label">Name</div>
                                <div class="item-value"><?php echo $row['name']?></div>
                            </div>
                            <div class="item-row">
                                <div class="item-label">Date</div>
                                <div class="item-value"><?=date("m/d/Y", strtotime($row['date']))?></div>
                            </div>
                            <div class="item-row">
                                <div class="item-label">City</div>
                                <div class="item-value"><?=$row['country']?>,&nbsp;<?=$row['city']?></div>   
                            </div>
                            <div class="item-row">
                                <div class="item-label">Phone</div>
                                <div class="item-value"><?=$row['phone']?></div>   
                            </div>
                        <div class="normal">
                            <?=nl2br(\Core\Utils\Security::htmlEncodeText($row['description']))?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="pagination-content">
        <?php 
       
            \Core\Controller::renderPlugin('paginator', NULL, \Core\View::LOCALIZED_DEVICE_MODE);
      
         ?>
        </div>
    </div>
</div>