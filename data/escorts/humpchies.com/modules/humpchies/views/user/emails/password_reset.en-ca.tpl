<?php
/*
+---------------------------------------------------------------------------+
| Humpchies English-US Password Reset Email v1                              |
| ============================================                              |
|                                                                           |
| Copyright (c) Jorge Ivan Manzanilla                                       |
|                                                                           |
|    Email:             ivan_manzanilla@videotron.ca                        |
|    Phone:             514-882-2800                                        |
|    Mail Address:      574 Beaurepaire                                     |
|                       Beaconsfield, Quebec, Canada                        |
|                       H9W 3E5                                             |
|                                                                           |
| This program is NOT free software; you can NOT redistribute it and/or     |
| modify it, unless explicitly authorized by Jorge Ivan Manzanilla.         |
|                                                                           |
| This program is distributed in the hope that it will be useful, but       |
| WITHOUT ANY WARRANTY; without even the implied warranty of                |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      |
|                                                                           |
| DESCRIPTION: This file contains the body of the email sent to users who   |
| forgot their password, and that are requesting a password reset in        |
| English.                                                                  |
|                                                                           |
| HISTORY:                                                                  |
|                   November 10, 2014: [y_manzanilla] Creation... Putos!!   |
|                                                                           |
+---------------------------------------------------------------------------+
*/

/**
* This variable should be declared within the calling context of this file.
* It will be used as the subject of the email for which this file will be used.
* Note that error supression is on, should this variable not be declared no error 
* will be generated.
*/
@$email_title = "Instructions to reset your Humpchies' password";

/**
* Note that the calling context needs to declare and set variable $password_reset_string
* This variable is the tokenized URL that the user needs to access to be able to 
* reset his/her password. Failure to support this varibale will render this email
* useless. 
*/
?>
<html>
    <head>
        <title>Password Reset</title>
        <style type="text/css">
            body {
                color: #666666;
                font-family: Tahoma, Arial;
                font-size: 12px;
                line-height: 1.2;
                text-align: left;
                background: #000000;
            }
            a:link, a:visited {
                color: #ff9900;
                font-weight: inherit;
                text-decoration: none;
            }
            a:hover, a:active {
                color: #ffffff;
                font-weight: inherit;
                text-decoration: none;
            }
            hr{
                color: #ff9900;   
            }
        </style>
    </head>
    <body>
        <p>
            <img src="http://gui-cdn.humpchies.com/images/pc/humpchies_logo.png">
            <hr>
        </p>
        <p>
            Hello,
        </p>
        <p>
            A request was made to our systems for a password reset on your account. To reset your password
            please click on the link here below:
        </p> 
        <p>
            <?=@$password_reset_string?>
        </p>
        <p>
            If you are unable to click on the link, please copy it and paste it on the navigation bar of 
            your favorite browser. If you did not request a password reset, please reply to this email and
            advise the Webmaster.
        </p>
        <p>Cheers!</p>
        <p>
            The Webmaster
        </p>
        <hr>
    </body>
</html>