<?php
/*
+---------------------------------------------------------------------------+
| Humpchies English-US Account Activation Email v1                          |
| ================================================                          |
|                                                                           |
| Copyright (c) Jorge Ivan Manzanilla                                       |
|                                                                           |
|    Email:             ivan_manzanilla@videotron.ca                        |
|    Phone:             514-882-2800                                        |
|    Mail Address:      574 Beaurepaire                                     |
|                       Beaconsfield, Quebec, Canada                        |
|                       H9W 3E5                                             |
|                                                                           |
| This program is NOT free software; you can NOT redistribute it and/or     |
| modify it, unless explicitly authorized by Jorge Ivan Manzanilla.         |
|                                                                           |
| This program is distributed in the hope that it will be useful, but       |
| WITHOUT ANY WARRANTY; without even the implied warranty of                |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      |
|                                                                           |
| DESCRIPTION: This file contains the body of the email sent to users upon  |
| sign-up and to prompt them to activate their account in English.          | 
|                                                                           |
| HISTORY:                                                                  |
|                   November 10, 2014: [y_manzanilla] Creation... Putos!!   |
|                                                                           |
+---------------------------------------------------------------------------+
*/

/**
* This variable should be declared within the calling context of this file.
* It will be used as the subject of the email for which this file will be used.
* Note that error supression is on, should this variable not be declared no error 
* will be generated.
*/
@$email_title = "Welcome to Humpchies!";

/**
* Note that the calling context needs to declare and set two variables:
* 
* $username and $activation_string
*
* The $username variable as its name implies is the username associated with 
* with the newly created account and is used to greet the user. The $activation_string 
* contains the tokenized URL that the user needs to access to be able to 
* activate his/her account. Failure to support this varibale will render this email
* useless. 
*/
?>
<html>
    <head>
        <title>Welcome to Humpchies!</title>
        <style type="text/css">
            body {
                color: #666666;
                font-family: Tahoma, Arial;
                font-size: 12px;
                line-height: 1.2;
                text-align: left;
                background: #000000;
            }
            a:link, a:visited {
                color: #ff9900;
                font-weight: inherit;
                text-decoration: none;
            }
            a:hover, a:active {
                color: #ffffff;
                font-weight: inherit;
                text-decoration: none;
            }
            hr{
                color: #ff9900;   
            }
        </style>
    </head>
    <body>
        <p>
            <img src="http://gui-cdn.humpchies.com/images/pc/humpchies_logo.png">
            <hr>
        </p>
        <p>
            Hello <?=@$username?>,
        </p>
        <p>
            Welcome to Humpchies! Thank you for your preference, and for taking the time signup to our Website. Please activate your account
            by clicking on the link here below:
        </p> 
        <p>
            <?=@$activation_string?>
        </p>
        <p>
            If you are unable to click on the link, please copy it and paste it on the navigation bar of 
            your favorite browser.
        </p>
        <p>Enjoy!</p>
        <p>
            The Webmaster
        </p>
        <hr>
    </body>
</html>