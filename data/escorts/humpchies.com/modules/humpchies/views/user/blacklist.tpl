<style>
    div.user_content_photo {}
    div.user_content_photo > div { width: 100% !important; padding: 0 !important; height: 100% !important; }
    div.user_content_photo > div > a { display:block !important; width: 100% !important; height:100% !important; background-color: #111 !important; }
    div.user_content_photo > div span { display: inline-block !important; height: 100% !important; vertical-align: middle !important; }
    div.user_content_photo > div img { position: static !important; vertical-align: middle !important; display: inline-block !important; }
    .buttons { display: flex;justify-content: space-between; margin: 11px 0 16px 0; font-weight: bold;}
    
    .buttons > .previous {  padding: 10px; background-color: #222; border-radius: 4px; color: #ff9900;  text-transform: uppercase; cursor: pointer; display:inline-block:}
    .buttons > .next {  padding: 10px; background-color: #222; border-radius: 4px; color: #ff9900;  text-transform: uppercase; cursor: pointer; display:inline-block:}
   
    input[type="date"]::-webkit-calendar-picker-indicator {
        color: #f90;
    }
   
    #description{
        width: calc(100% - 23px);
        height: 245px;
        background-color: #222;
        border-color: #fff;
        resize: none;
        outline: none;
        padding: 10px;
        border-radius: 5px;
        font-size: 14px;
    }
    .addblacklist .text{
        width:100% !important;
        background: transparent !important;
    }
    .addblacklist {
        padding: 35px;
        background-color: #222;
    }
    .addtitle {
        font-size: 18px;
        color:#fff;
        margin-bottom:25px;
        text-align: right;
        
    }
    .button {
        width: 50px;
        width: 82px;
        cursor: pointer;
        cursor: pointer;
        user-select: none;
        background-color: #f90 !important;
        border-radius: 8rem;
        height: 36px;
        font-size: 14px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: black;
        text-transform: uppercase;
        padding: 0px 10px;
        margin-left: 5px;
        float: right;
        margin-top: 17px;
        display: inline-block;
    }
    .button-outline {
        width: 82px;
        cursor: pointer;
        user-select: none;
        background-color: transparent;
        border-radius: 8rem;
        height: 36px;
        font-size: 14px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: #f90;
        text-transform: uppercase;
        padding: 0px 10px;
        margin-left: 5px;
        float: right;
        margin-top: 17px;
        display: inline-block;
        border: #f90 1px solid;
        margin-left: 50px;
    }
    .country-layout {
        display: flex;
        align-content: space-between;
        margin-bottom:20px;
    }
    
    .country-layout > div:first-child {
        margin-right: 15px;   
    }
    
    .country-layout .field {
        min-width: 287px;
        
    }
    .custom-select {
       background: none; 
    }
    
    .custom-select option {
        background: #222;
        color: #fff;
    }
    .custom-select select {
        border: 1px solid #ff9000;
        outline: none;
        background: none;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        border-radius: 4px;
        margin: 0;
        display: block;
        width: 100%;
        padding: 9px 25px 9px 15px;
        font-size: 14px;
        color: #ff9000;
        height: 46px;
    }
    .custom-select:after {
        position: absolute;
        right: -7px;
        top: 15px;
        width: 50px;
        height: 100%;
        line-height: 38px;
        content: "\f184";
        text-align: center;
        color: #ff9000;
        font-size: 20px;
        border-left: 0;
        z-index: -1;
        font: normal normal normal 14px/1 FontAwesome;
    }
    input#search {
        height: 18px;
        line-height: 18px;
        padding: 10px;
        background: #333;
        border-radius: 4px;
        width: calc(100% - 90px);
        margin-bottom: 20px;
    }
    
    .item {
        border-bottom: #000 1px solid;
        padding: 20px 0px;
        line-height: 16px;
    }
    .item-head {
        margin-bottom: 20px;
    }
    span.name {
        color: #ff9900;
        margin-right: 15px;
    }
    .content_box {
        padding: 20px;
        background: #222;
        margin-top:20px;
    }
    .pagination-content{
        margin-top:20px;
        height: 35px;
    }
    .search-button{
        width: 70px;
        height: 37px;
        border-radius: 4px;
        margin-left: 10px;
        background: #f90 !important;
        color: #000;
    }
      .required{
            margin-left: 20px;
            color:#ff0000;
        }
    .search-container {
        display: flex;
    }
    @media only screen and (max-width: 600px) {
        .country-layout {
            display: block;
            
        }
        .country-layout .field {    
            height: 75px;
            min-width: auto;
        }
        .country-layout > div:first-child {
            margin-right: 0px;   
        }
      
 
    }
</style>

<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <?php if($search) { ?>
        <li> <a href="<?php echo \Core\Utils\Navigation::urlFor('user', 'blacklist');?>">[LABEL-BLACKLIST-CLIENTS]</a></li>
        <li> [LABEL-BLACKLIST-CLIENTS] <?='&laquo;&nbsp;' . $search . "&nbsp;&raquo;" ?></li>
        <?php }else{ ?>
        <li> [LABEL-BLACKLIST-CLIENTS]</li>
        <?php } ?>
    </ul>    
 </nav>

  
 <div class="wrapper_content">
     <div class="addblacklist">
        <div class="Info mb-15" style="margin-bottom: 45px;">
        [LABEL-BLACKLIST-INFO]
            <div class ='button mb-15' id ='add' style="padding-top: 5px;padding-left: 12px; min-width: 43px;height: 21px;border-radius: 4px;text-align: center;">
                 + [LABEL-ADD]
            </div>
        </div>
        <form name="createblacklist" method="POST" id='form-show' style="display: none;">
            <div class="country-layout">
                <div class="field mb-15" >
                    <label for="country">[LABEL-COUNTRY]</label>
                    <div class=" mt-5">
                        <label class="custom-select">
                        <select name="country" id="country" data-must-be="">
                            <option value=''>[LABEL-SELECT-COUNTRY]</option>
                            <?php foreach($clist as $key => $list):?>
                                <option value="<?php echo $key ?>"> <?=$list ?> </option>      
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <input type="hidden" name="country_name" id="country_name">
                <div class="field mb-15">
                    <label for="city">[LABEL-CITY]</label>
                    <div class=" mt-5">
                        <label class="custom-select" >
                        <select name="city" id="city" data-must-be="" disabled="disabled">
                            <option value=''></option>
                           
                        </select>
                        </label>
                    </div>
                </div>
            
            </div>
            
            <div class="field mb-15">
                <label for="name">[LABEL-NAME]<span class="required">*</span></label>
                <input name="name" id = "name" class="text" required/>
            </div> 
            <div class="country-layout">
                <div class="field mb-15" >
                       <label for="date"> Date</label>
                <input name="date"  id ="date"  type='text' class='datepicker-here text' data-language='<?php echo $xlang; ?>' readonly="true"/>
                </div>
                <div class="field mb-15">
                    <label for="phone">[LABEL-TELEPHONE]<span class="required">*</span></label>
                    <input type="tel" name="phone" id = "phone" class="text" maxlength="10" required/>
                </div>
            </div>
            <div class="field mb-15">
                <label for="description">[LABEL-BLACKLIST-COMMENT]<span class="required">*</span></label>
                <div class="mt-5">
                    <textarea name="description" id = "description"  style='height: 135px;' required/>
                    </textarea>
                </div>
            </div>
            <div class="field mb-15">
                <input type="submit" value="[LABEL-SEND]"  style="max-width: 83px; padding-top: 4px; padding-bottom: 4px; min-width: 83px; height: 30px;  border-radius: 4px;  text-align: center;  margin-top: 0px; background: #f90; color: #000; -webkit-appearance:none"/>
                <input type="reset" value='[LABEL-CLOSE]'  id='close'  style=" max-width: 83px; padding-top: 4px; padding-bottom: 4px; min-width: 83px; height: 30px;  border-radius: 4px;  text-align: center;  margin-top: 0px; border:1px solid #f90; background: transparent; margin-left :30px; color:#f90">
            </div>
            
        </form>
    </div>
    
   
    <div class="content_box">
        <h2 class="title">[LABEL-BLACKLIST-CLIENTS] <?php if($search) {  echo '&laquo;&nbsp;' . $search . "&nbsp;&raquo;"; } ?></h2>
        <div class="content_box_body">
            <form method="GET">
            <div class="search-container">
                <input type='text' name="search" value="" placeholder = "[LABEL-SEARCH-PLACEHOLDER]" id='search'/>
                <input type="submit" value="[LABEL-SEARCH-BUTTON]" class ='button' id ='search' style=" max-width: 102px; padding-top: 10px; padding-left: 3px; min-width: 91px; height: 38px;  border-radius: 4px;  text-align: center;  margin-top: 0px;-webkit-appearance:none">
            </div>
            </form>
            <div class="list-body">
                <?php foreach($blacklist as $k => $row) { ?>
                    <div class="item">
                        <div class="item-head">
                            <span class="name"><?php echo $row['name']?></span>&nbsp;<span class="gri"><?=date("m/d/Y", strtotime($row['date']))?></span><br/>
                            <span class="gri"><?=$row['country']?></span>,&nbsp;<span class="gri"><?=$row['city']?></span>&nbsp;/&nbsp;<span class="gri"><?=$row['phone']?></span>
                        </div>
                        <div class="normal">
                            <?=nl2br(\Core\Utils\Security::htmlEncodeText($row['description']))?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="pagination-content">
        <?php 
        if(\Core\Utils\Device::isPC()){
            \Core\Controller::renderPlugin('paginator', NULL, \Core\View::DEVICE_MODE);
        }else{
            \Core\Controller::renderPlugin('paginator', NULL, \Core\View::LOCALIZED_DEVICE_MODE);
        } 
         ?>
        </div>
    </div>
</div>