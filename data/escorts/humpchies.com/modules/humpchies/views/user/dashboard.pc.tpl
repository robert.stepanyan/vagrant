<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li>Dashboard</li>
    </ul>    
</nav>

<div class="wrapper_content_full">
    
        <h2 class="centered-title">Welcome to your private area, <?php echo $user_details['username'];?>!</h2>
       
       
        <div class="dashboard-boxes">
            <?php if(Core\Utils\Session::currentUserIsAdvertisment()):?>
                <a class="box" href="<?= \Core\Utils\Navigation::urlFor('ad', (\Core\Utils\Session::getCurrentUserID() == 63661) ? 'create-v2' : 'create')?>"><i class="fa fa-plus-circle"></i>[LABEL-NEW-AD]</a>
                <a class="box" title="[HREF-TITLE-MY-ADS]" href="<?=\Core\Utils\Navigation::urlFor('ad', 'my-listings')?>"><i class="fa fa-edit"></i>[LABEL-MY-ADS]</a>
                <a class="box" title="[HREF-TITLE-MY-ADS]" href="<?=\Core\Utils\Navigation::urlFor('payment', 'bumps-purchase')?>"><i class="fa fa-rocket"></i>Promote Ads</a>
                <a class="box" title="[HREF-TITLE-MY-CHAT]" href="<?=\Core\Utils\Navigation::urlFor('chat', 'view')?>">
                    <i class="fas fa-comments"></i>
                    [LABEL-MY-CHAT]
                    <?php if ($unreadMessages) : ?>
                        <span class="notice"><?=$unreadMessages?></span>
                    <?php endif; ?>
                </a>
                <a class="box" title="[HREF-TITLE-MY-SETTINGS]" href="<?=\Core\Utils\Navigation::urlFor('user', 'settings')?>"><i class="fa fa-cog"></i>[LABEL-SETTINGS]</a>
                <a class="box" title="[LABEL-BLACKLIST-CLIENTS]" href="<?=\Core\Utils\Navigation::urlFor('user', 'blacklist')?>"><i class="fa fa-user-times"></i>[LABEL-BLACKLIST-CLIENTS]</a>                
            <?php else :?>
                <a class="box" title="[HREF-TITLE-MY-CHAT]" href="<?=\Core\Utils\Navigation::urlFor('chat', 'view')?>">
                    <i class="fas fa-comments"></i>
                    [LABEL-MY-CHAT]
                    <?php if ($unreadMessages) : ?>
                        <span class="notice"><?=$unreadMessages?></span>
                    <?php endif; ?>
                </a>
                <a class="box" title="[HREF-TITLE-MY-SETTINGS]" href="<?=\Core\Utils\Navigation::urlFor('user', 'settings')?>"><i class="fa fa-cog"></i>[LABEL-SETTINGS]</a>
                <a class="box" title="[TITLE-FAVORITES]" href="<?=\Core\Utils\Navigation::urlFor('favorites', 'listing')?>"><i class="fa fa-heart"></i>[TITLE-FAVORITES]</a>                    
            <?php endif;?>
        </div>
   
        <h2 class="stylized-title centered m-20-0">
    <a title="[HREF-TITLE-LOGOUT]" href="<?=\Core\Utils\Navigation::urlFor('user', 'logout')?>"><span class="red"><i class="fa fa-power-off"></i>[LABEL-LOGOUT]</span></a>

</h2>    
<div class="c-50"></div>        
<div class="c-50"></div>        
    
</div>