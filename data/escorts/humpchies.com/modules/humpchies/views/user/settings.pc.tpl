<nav id="breadcrumb">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li>[LABEL-SETTINGS]</li>
    </ul>    
</nav>

<h2 class="stylized-title"><span>[LABEL-SETTINGS]</span></h2>

<div class="wrapper_content_full">
    <div id="change_pass" class="content_box" style="margin-top:30px;">

        <div class="info">
            <p style="font-size: 14px;color: #fff;border-bottom: 1px solid #403f3f;padding-bottom: 15px;margin-bottom: 15px;">[LABEL-SETTINGS-INFO]: 
                <span style="font-weight: bold; margin-left: 10px;"><?php echo $user_details['username'];?> (<?php echo \Core\Utils\Session::getCurrentUserID();?>)</span></p>
        </div>

        <form id="settings-form" class="settings-form" action="" method="post" enctype="multipart/form-data">
             <?php if(Core\Utils\Session::currentUserIsAdvertisment()):?>
             <?php if ($user_details['disable_comments'] < 2):?>
            <div class="checkbox-container">
                <input type="checkbox" id="disable_comments" name="disable_comments" value="1" <?php echo (($user_details['disable_comments'] == 1)? 'checked' : ''); ?> />
                <label for="disable_comments">[LABEL-BLOCK-COMMENTS]</label>
            </div>
             <?php else :?>
                <input type="hidden"  name="disable_comments" id="disable_comments" value="2" />
             <?php endif;?>
           <div class="checkbox-container">
                <input type="checkbox" id="disable_pm" name="disable_pm" value="1" <?php echo (($user_details['disable_pm'] == 1)? 'checked' : ''); ?> />
                <label for="disable_pm">[LABEL-BLOCK-PM]</label>
            </div>
            <?php endif;?>
           
            <div class="field">
                <div>
                    <label for="current_password">[LABEL-PASSWORD]</label>
                    <input id="current_password" class="text" type="password" maxlength="<?=$password_max_len?>" name="current_password" value="">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="field">
                <div>
                    <label for="new_password">[LABEL-NEW-PASSWORD]</label>
                    <input id="new_password" class="text" type="password" maxlength="<?=$password_max_len?>" name="new_password" value="">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="field">
                <div>
                    <label for="confirm_password">[LABEL-NEW-PASSWORD-CONFIRMATION]</label>
                    <input id="confirm_password" class="text" type="password" maxlength="<?=$password_max_len?>" name="confirm_password" value="">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="field actions">
                <button type="submit" class="button">[BTN-UPDATE]</button>
            </div>
        </form>

    </div>
</div>