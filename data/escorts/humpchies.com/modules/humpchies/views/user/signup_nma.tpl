<nav id="breadcrumb" class="mb-30">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li> [LABEL-SIGN-UP]</li>
    </ul>
</nav>

<h2 class="stylized-title"><span>[LABEL-SIGN-UP]</span></h2>

<div class="wrapper_content">
    <div class="content_box">
        
        <div class="content_box_body user-login">
                <form id="sign_up_form" method="post">
                <div class="field bordered">
                        <label for="type">[LABEL-ACCOUNT-TYPE]:</label>
                        <div class='radious'>
                            <label  class="container-signup">
                            [LABEL-ADVERTISEMENT]
                             <input type="radio" value="1"  name='type'  <?=(isset($_POST["type"]) && $_POST["type"] == 1 ? 'checked': '')?> style="margin-right: 10px;" > <span class="checkmark"></span></label>
                            <label class="container-signup">
                             [LABEL-MEMBER] <input type="radio" value="2"  name='type' <?=(isset($_POST["type"]) && $_POST["type"] == \Modules\Humpchies\UserModel::USER_TYPE_MEMBER ? 'checked': '')?> style="margin-right: 10px;" > <span class="checkmark"></span> </label>
                           
                        </div>
                    </div>
                    <div class="field">
                        <label>[LABEL-USERNAME]:</label>
                    <input class="form-control" name="username" type="text" maxlength="<?=$usermax_len_name?>" value="<?=(isset($_POST["username"]) ? $_POST["username"] : '')?>" />
                    </div>
                
                    <div class="field">
                        <label>[LABEL-EMAIL]:</label>
                    <input class="form-control" name="email" type="text" maxlength="<?=$max_len_email?>" value="<?=(isset($_POST["email"]) ? $_POST["email"] : '')?>" />
                    </div>
                
                    <div class="field">
                        <label>[LABEL-PASSWORD]:</label>
                    <input class="form-control" name="password" type="password" maxlength="<?=$password_max_len?>" value="<?=(isset($_POST["password"]) ? $_POST["password"] : '')?>" />
                    </div>
                    <!--<div class="field">
                        <label>[LABEL-PASSWORD-CONFIRMATION]:</label>
                        <div>
                            <input class="text" name="password_confirmation" type="password" maxlength="<?=$password_max_len?>" value="<?=(isset($_POST["password_confirmation"]) ? $_POST["password_confirmation"] : '')?>" />
                        </div>
                    </div>
                    -->
                <div class="field" style="margin-bottom: 15px;">
                        <label>[LABEL-CAPTCHA]:</label>
                        <div>
                            <span class="code">
                                <img src="/captcha.gif">
                            </span>
                        </div>
                    </div>
               
                    <div class="field">
                        <label>[LABEL-TYPE-CAPTCHA]:</label>
                    <input class="form-control" name="code" type="text" value="" maxlength="<?=$max_len_captcha?>" />
                    </div>
                
                    <div class="field">
                        <div>
                            <input class="button" type="submit" value="[BTN-CAPTION-SIGN-UP]" />
                        </div>
                    </div>
                
                </form>
                <p>
                    <ul class="bullet">
                        <li>
                            <a title="[HREF-TITLE-REQUEST-PASSWORD-RESET]" href="<?=\Core\Utils\Navigation::urlFor('user', 'request-password-reset')?>">[PROMPT-REQUEST-PASSWORD-RESET]</a>    
                        </li>
                    </ul>
                </p>
            
        </div>
    </div>
</div>
