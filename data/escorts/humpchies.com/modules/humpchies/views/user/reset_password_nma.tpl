<nav id="breadcrumb" class="mb-30">
    <ul>
        <li> <a href="/"><i class="fa fa-home"></i></a></li>
        <li> [LABEL-RESET-PASSWORD]</li>
    </ul>
</nav> 
 <h2 class="stylized-title"><span>[LABEL-RESET-PASSWORD]</span></h2>
 
<div class="wrapper_content">
    <div class="content_box">
        <div class="content_box_body user-login">
            
            
                <form id="sign_up_form" method="post">
                    <div class="field">
                        <label>[LABEL-NEW-PASSWORD]:</label>
                    <input class="form-control" name="password" type="password" maxlength="<?=$password_max_len?>" value="<?=(isset($_POST["password"]) ? $_POST["password"] : '')?>" />
                    </div>
                
                    <div class="field">
                        <label>[LABEL-NEW-PASSWORD-CONFIRMATION]:</label>
                    <input class="form-control" name="password_confirmation" type="password" maxlength="<?=$password_max_len?>" value="<?=(isset($_POST["password_confirmation"]) ? $_POST["password_confirmation"] : '')?>" />
                        </div>
                
                <div class="field" style="margin-bottom: 15px;">
                        <label>[LABEL-CAPTCHA]:</label>
                        <div>
                            <span class="code">
                                <img src="/captcha.gif">
                            </span>
                        </div>
                    </div>
                    <div class="field">
                        <label>[LABEL-TYPE-CAPTCHA]:</label>
                    <input class="form-control" name="code" type="text" value="" maxlength="<?=$max_len_captcha?>" />
                    </div>
                
                    <div class="field">
                        <div>
                            <input class="button" type="submit" value="[BTN-CAPTION-RESET-PASSWORD]" />
                        </div>
                    </div>
                </form>
            
        </div>
    </div>
</div>