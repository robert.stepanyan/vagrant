<?php $config = array('zones' => array( \Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO => array(
                                            '1112088',      // Allison #1
                                            '1115355',      // Katia
                                            '905567',       // Alessandra
                                            '1133728',      // Godiva #1
                                            '1055069',      // Maxxximum XXX
                                            '1053129',      // Fantasme XXX
                                            '758009',       // Indy
                                            '1133733',      // Godiva --
                                            '1098695',      // Myriam
                                            '1120104',      // Nadya
                                            '1127978',      // Jay
                                            '1099283'       // Chealsy
                                        ),
                                        \Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO . '_category_' . \Modules\Humpchies\CategoryModel::CATEGORY_ESCORT => array(
                                            '1117976',      // Allison #2
                                            '1123470',      // Katia
                                            '898620',       // Gabrielle
                                            '1133728',      // Godiva #2
                                            '1119872',      // San Dra
                                            '1120104',      // Nadya
                                            '1127978',      // Jay
                                            '1127987'       // Jay/Mel
                                        ),
                                        \Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO . '_category_' . \Modules\Humpchies\CategoryModel::CATEGORY_ESCORT_AGENCY => array(
                                            '1133728',      // Godiva #3
                                            '1120104',      // Nadya
                                            '1127416'       // Sweet Montreal
                                        ),
                                        \Modules\Humpchies\Utils::GEO_ZONE_MONTREAL_METRO . '_category_' . \Modules\Humpchies\CategoryModel::CATEGORY_EROTIC_MASSAGE => array(
                                            '898620'        // Gabrielle
                                        ),
                                        \Modules\Humpchies\Utils::GEO_ZONE_QUEBEC_CENTRE => array(
                                            '1132685'       // Bobby Paradis Drummondville
                                        ),
                                        \Modules\Humpchies\Utils::GEO_ZONE_EASTERN_TOWNSHIPS => array(
                                            '1129864'       // Random
                                        )
                                    )
                                );