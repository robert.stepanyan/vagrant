<?php
// Sorry for this dumb checking (fast solution)
// This helped to solve dummy bug: when users enter from Canada (Try with VPN)
// Without this double check, all cities after the selected
// were marked as selected too
// ---------------------------------------------
$found_selected_item = false;
// ---------------------------------------------

?>

<select name="<?=$select_name?>" data-must-be="<?= $current_city_code ?>">
    <?php   foreach($cities as $city_code => &$city_name){
                
                if($to_english)
                    //$city_name = \Core\Utils\Text::translateDiacriaticsAndLigatures($city_name);
                
                $selected = '';
                 
                if( !$encoding_utf8 ){
                    if( $city_code === $current_city_code ){
                        $selected = 'selected';
                    }
                } else {
                    if( $city_name === $current_city_code ){
                        $selected = 'selected';
                    }
                }
                
                ?>
                
                <option value="<?=$city_code?>" <?= $found_selected_item === false ? $selected : '' ?> ><?= $city_name ?></option>
    <?php
           if($selected === 'selected') $found_selected_item = true;
    ?>

    <?php  }?>
</select>