<?php
Namespace Plugins;

class HumpchiesCityOptions extends \Core\Plugin{
    
    const DEFAULT_SELECT_NAME = 'city';
    
    public function __construct($select_name = self::DEFAULT_SELECT_NAME, $current_city_code = NULL, $encoding_utf8 = false)
    {   
        $this->data['cities'] = \Modules\Humpchies\CityModel::getCitiesSlugNames();
        
        $this->data['current_city_code'] = $current_city_code;
        $this->data['select_name'] = $select_name;
        $this->data['encoding_utf8'] = $encoding_utf8;
        
        $session_language = $page_language = \Core\Utils\Session::getLocale();
        $this->data['to_english'] = ($session_language{0}.$session_language{1} === 'en');
    }
}
?>