<?php
Namespace Plugins;

class HumpchiesLocationOptions extends \Core\Plugin{

    const DEFAULT_SELECT_NAME = 'location';
    
    public function __construct($select_name = self::DEFAULT_SELECT_NAME, $current_location_code = NULL)
    {   
        $this->data['locations'] = \Modules\Humpchies\LocationModel::getSupportedLocations();
        $this->data['current_location_code'] = $current_location_code;
        $this->data['select_name'] = $select_name;
    }
}
?>