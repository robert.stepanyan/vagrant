<select name="<?=$select_name?>">
    <?php   foreach($locations as $location_code){
                $selected = ($location_code === $current_location_code ? 'selected' : '');?>
                <option value="<?=$location_code?>" <?=$selected?> ><?='[LABEL-' . strtoupper($location_code) . ']'?></option>
    <?php   }?>                                
</select>