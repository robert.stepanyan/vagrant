<?php
Namespace Plugins;

class Paginator extends \Core\Plugin{
    
    private static $standard_url = NULL;

    public function __construct($total_items, $page_num, $num_per_page, $url, $asPath = true, $first_page_url = NULL, $hash = null)
    {
        
        $this->data['page_num'] = (int)$page_num;
        $this->data['total_pages'] = (int)@(floor($total_items / $num_per_page) + (($total_items % $num_per_page) == 0 ? 0 : 1));
        $this->data['first_page_url'] = ($first_page_url === NULL ? $url : ($hash != '')? $first_page_url . '?page=1' . $hash : $first_page_url);
        $this->data['last_page_url'] = ((int)($this->data['total_pages'] - $this->data['page_num']) === 0 ? $this->data['first_page_url'] : self::getPageURL($url, $this->data['page_num'], $asPath, ($this->data['total_pages'] - $this->data['page_num']), $hash));

        $this->data['url_without_page_number'] = $url;
        $this->data['url_as_path'] = $asPath;
        $this->data['hash'] = $hash;

        $this->data['left_url'] = FALSE;
        $this->data['right_url'] = FALSE;
        $this->data['left_url_minus_one'] = FALSE;
        $this->data['right_url_plus_one'] = FALSE;
        
        if($this->data['total_pages'] <= 1)
            return;

        for($x = 1; $x <= 2; $x++)
        {
            if($this->data['right_url' . ($x === 2 ? '_plus_one' : NULL)] === FALSE && (($this->data['page_num'] + $x) <= $this->data['total_pages']) && (($this->data['page_num'] + $x) > 1 ))
                $this->data['right_url' . ($x === 2 ? '_plus_one' : NULL)] = self::getPageURL($url, $this->data['page_num'], $asPath, $x, $hash);

            if($this->data['left_url'] === FALSE && (($this->data['page_num'] - $x) === 1))
                  $this->data['left_url'] = $this->data['first_page_url'];
            else if($this->data['left_url_minus_one'] === FALSE && (($this->data['page_num'] - ($x + 1)) === 0))
                  $this->data['left_url_minus_one'] = $this->data['first_page_url'];
            elseif($this->data['left_url' . ($x === 2 ? '_minus_one' : NULL)] === FALSE && (($this->data['page_num'] - $x) > 1) && (($this->data['page_num'] - $x) < $this->data['total_pages']))
                $this->data['left_url' . ($x === 2 ? '_minus_one' : NULL)] = self::getPageURL($url, $this->data['page_num'], $asPath, -$x, $hash);
        } 
    }
    
    public static function getPageURL($url, $current_page_number, $as_path, $steps = 1, $hash = null)
    {
        if(self::$standard_url !== NULL) {
            $complete_url =  str_replace('[PAGE_NUMBER]', ($current_page_number + ($steps)), self::$standard_url);
            return $complete_url . $hash;
        }
        $url_parts = parse_url(preg_replace(array("/\/page\/[0-9]+/", "/page=[0-9]+/", "/&&/", "/\?&/", "/&$/", "/\/\//"), array('/', '', '&', '?', '', '/') , $url));

        if($as_path)
            $url_parts['path'] .= (substr($url_parts['path'], -1, 1) !== '/' ? '/' : NULL) .'page/[PAGE_NUMBER]';
        elseif(isset($url_parts['query']))
            $url_parts['query'] .= '&page=[PAGE_NUMBER]';
        else
            $url_parts['query'] = 'page=[PAGE_NUMBER]';
        
        $complete_url = str_replace('[PAGE_NUMBER]', ($current_page_number + ($steps)), (self::$standard_url =  implode('?', $url_parts)));
        
        return $complete_url . $hash;
    }
}
?>