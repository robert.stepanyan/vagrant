<?php if($total_pages > 1) : ?>
    <ul class="pagination <?= $page_num ?> <?= $total_pages ?>">
    
    <li>
        <a href="<?=$left_url?>" <?=(!$left_url ? ' class="disabled"' : '')?>>
            <i class="fa fa-arrow-left"></i>
        </a>
    </li>
    <?php   $index = (int)floor(($page_num - 1) / 4);

            for($x = 1; $x <= 5; $x++) {
                $pageNumber = (($index * 4) + $x);

                if($pageNumber <= $total_pages) { ?>
    <li>
        <a href="<?=(1 === $pageNumber ? $first_page_url : \Plugins\Paginator::getPageURL($url_without_page_number, $pageNumber, $url_as_path, 0, $hash))?>" <?=($pageNumber === $page_num ? ' class="active"' : '')?>>
            <?=$pageNumber?>
        </a>
    </li>
    <?php       }
            } ?>
    <li>
        <a href="<?=$right_url?>" <?=(!$right_url ? ' class="disabled"' : '')?>>
            <i class="fa fa-arrow-right"></i>
        </a>
    </li>
    
    </ul>
<?php endif; ?>
