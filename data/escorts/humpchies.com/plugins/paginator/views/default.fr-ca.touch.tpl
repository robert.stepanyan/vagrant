<?php if($total_pages > 1) : ?>
<div class="content_box_footer">
    <div class="pages">
        <?=($left_url ? '<a href="'.$left_url.'">' : '')?>
        &laquo;Pr&eacute;c&eacute;dent&nbsp;&nbsp;|
        <?=($left_url ? '</a>' : '')?>
        &nbsp;&nbsp;Page&nbsp;&nbsp;<?=$page_num?>&nbsp;&nbsp;de&nbsp;&nbsp;<?=$total_pages?>&nbsp;&nbsp;
        <?=($right_url ? '<a href="'.$right_url.'">' : '')?>
        |&nbsp;&nbsp;Suivant&raquo;
        <?=($right_url ? '</a>' : '')?>
    </div>
</div>
<?php endif;?>