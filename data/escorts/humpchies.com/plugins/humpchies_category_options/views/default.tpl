<select name="<?=$select_name?>">
    <?php   foreach($categories as $category_code){
                $selected = ($category_code === $current_category_code ? 'selected' : '');?>
                <option value="<?=$category_code?>" <?=$selected?> ><?='[LABEL-' . strtoupper($category_code) . ']'?></option>
    <?php   }?>                                
</select>