<?php
Namespace Plugins;

class HumpchiesCategoryOptions extends \Core\Plugin{

    const DEFAULT_SELECT_NAME = 'category';
        
    public function __construct($select_name = self::DEFAULT_SELECT_NAME, $current_category_code = NULL)
    {   
        $this->data['categories'] = \Modules\Humpchies\CategoryModel::getSupportedCategories();
        $this->data['current_category_code'] = $current_category_code;
        $this->data['select_name'] = $select_name;
    }
}
?>