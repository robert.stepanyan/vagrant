<?php
Namespace Plugins;

class HumpchiesCityList extends \Core\Plugin{
    
    public function __construct($current_city_code = NULL, $category_code = NULL)
    {
        $this->data['current_city_code'] = $current_city_code;
        $this->data['category_code'] = $category_code;

        $session_language = \Core\Utils\Session::getLocale();
        $this->data['to_english'] = ($session_language{0}.$session_language{1} === 'en');

        if(($this->data['cities'] = \Modules\Humpchies\CityModel::getCitiesWithAds($category_code)) === FALSE)
        {
            $this->data = NULL;
            return;
        }
    }
}
?>