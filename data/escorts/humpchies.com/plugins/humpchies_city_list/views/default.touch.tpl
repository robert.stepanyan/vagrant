<br>
<div class="wrapper">
    <div class="content_box">
        <div class="content_box_header">
            <h2 class="title">
                <?=($category_code === NULL ? '[LABEL-ESCORT] & [LABEL-EROTIC-MASSAGE]' : '[LABEL-' . strtoupper($category_code) . ']')?> [LABEL-BY-CITY]
            </h2>
        </div>
        <div class="content_box_body">
            <ul class="cities_list">
            <?php  foreach($cities as $city_code => &$city_name){

                            if($to_english)
                                $city_name = \Core\Utils\Text::translateDiacriaticsAndLigatures($city_name);
                            else
                                $city_name = \Core\Utils\Security::htmlEncodeText($city_name);
                                
                            if($city_code !== $current_city_code){?>
                                <li>
                                    <a href="<?=\Modules\Humpchies\AdController::getListURL($category_code, $city_code)?>" title="[HREF-TITLE-GENERIC-AD-LINK-PART-1]<?=$city_name?>">
                                        <?=$city_name?>
                                    </a>
                                </li>
                 <?php      }
                            else{?>
                                <li>
                                        <?=$city_name?>
                                </li> 
                            
                 <?php      }
                        }?>
            </ul>
        </div>
    </div>
</div>