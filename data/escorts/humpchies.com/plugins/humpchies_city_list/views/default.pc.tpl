<?php   

        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x250_3', \Core\View::DEVICE_MODE));
        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x250_4', \Core\View::DEVICE_MODE));
        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x250_5', \Core\View::DEVICE_MODE));
        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_1', \Core\View::DEVICE_MODE));
        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_2', \Core\View::DEVICE_MODE));
        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_3', \Core\View::DEVICE_MODE));
        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_4', \Core\View::DEVICE_MODE));
        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x100_5', \Core\View::DEVICE_MODE));
        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x250_eros', \Core\View::DEVICE_MODE));
        require(\Core\View::getFullyQualifiedViewPath('\Modules\Humpchies\govazd\Zones::300x250_edir', \Core\View::DEVICE_MODE));
?>
<div class="cities_list">
    <h2 class="title">
        <i class="fa fa-filter" aria-hidden="true"></i><?=($category_code === NULL ? '[LABEL-ESCORT] [LABEL-AND] [LABEL-EROTIC-MASSAGE]' : '[LABEL-' . strtoupper($category_code) . ']')?> [LABEL-BY-CITY]
    </h2>
    <ul>
         <?php  if($cities){
             foreach($cities as $city_code => &$city_name){
                        if($to_english)
                            $city_name = \Core\Utils\Text::translateDiacriaticsAndLigatures($city_name);
                        else
                            $city_name = \Core\Utils\Security::htmlEncodeText($city_name);
                        if($city_code !== $current_city_code){?>
                            <li>
                                <a href="<?=\Modules\Humpchies\AdController::getListURL($category_code, $city_code)?>" title="<?=\Modules\Humpchies\AdController::getListHrefTitle(@$category_code, $city_name)?>">
                                    <?=$city_name?>
                                </a>
                            </li>
             <?php      }
                        else{?>
                            <li>
                                    <?=$city_name?>
                            </li>

            <?php }
                }   
        }?>
    </ul>
</div>
