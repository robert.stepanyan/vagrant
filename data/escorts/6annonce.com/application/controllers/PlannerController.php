<?php

class PlannerController extends Zend_Controller_Action {
	private $_model;
	private $user;
		
	public function init() 
	{
		$this->user = $this->view->user = Model_Users::getCurrent();
		$this->view->layout()->setLayout('private-v2');
		
		if ( ! $this->user ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		
		$this->_model = new Model_Planner();
	}
	
	public function indexAction() 
	{
		$r_m = $this->view->r_m = date('n') . '-' . date('Y');
		$this->view->m = date('n');
		$this->view->y = date('Y');
		
		if ($this->user->isAgency())
		{
			$events = null;
			$events_info = null;
			$ag_id = $this->user->getAgency()->id;
			$this->view->escorts = Model_Escorts::getAgencyEscorts($ag_id);
		}
		else
		{
			$events = $this->_model->getEvents($this->user->id);
			$ret = $this->_model->getCurrentMonthEventsInfo($this->user->id, null, $r_m);
			$events_info = $ret['items'];
			$events_info_counts = $ret['counts'];
		}
		
		if ($events)
		{
			$ret = $this->_generateEvents($events, date('n'));

			$this->view->appointment = $ret['appointment'];
			$this->view->holiday = $ret['holiday'];
			$this->view->other = $ret['other'];
			
			$this->view->current_days = $current_days = $ret['current_days'];
			$this->view->events_info = $events_info;
			$this->view->events_info_counts = $events_info_counts;
		}
	}
	
	public function ajaxGetAction() 
	{
		$this->view->layout()->disableLayout();
		$this->view->r_m = $r_m = trim($this->_request->r_m);
		
		if ($r_m)
		{
			$r_parts = explode('-', $r_m);
			
			if (count($r_parts) == 2)
			{
				$m = intval($r_parts[0]);
				$y = intval($r_parts[1]);
				
				if ($m == 0 || $m > 12)
					$r_m = NULL;
				else if ($y < 2000 || $y > 2050)
					$r_m = NULL;
			}
			else
				$r_m = NULL;
		}
		
		if (!$r_m)
			$r_m = $this->view->r_m = date('n') . '-' . date('Y');
		
		if ($this->user->isAgency())
		{
			$events = null;
			$events_info = null;
			$ag_id = $this->user->getAgency()->id;
			$this->view->escorts = Model_Escorts::getAgencyEscorts($ag_id);
			$escort_id = intval($this->_request->escort_id);
			
			if ($this->_model->isOwnerAgency($escort_id, $ag_id))
			{
				$events = $this->_model->getEvents($this->user->id, $escort_id);
				$ret = $this->_model->getCurrentMonthEventsInfo($this->user->id, $escort_id, $r_m);
				$events_info = $ret['items'];
				$events_info_counts = $ret['counts'];
				$this->view->escort_id = $escort_id;
			}
		}
		else
		{
			$events = $this->_model->getEvents($this->user->id);
			$ret = $this->_model->getCurrentMonthEventsInfo($this->user->id, NULL, $r_m);
			$events_info = $ret['items'];
			$events_info_counts = $ret['counts'];
		}
		
		$cur_month = explode('-', $r_m);
		$this->view->m = $cur_month[0];
		$this->view->y = $cur_month[1];
		
		if ($events)
		{
			$ret = $this->_generateEvents($events, $cur_month[0]);

			$this->view->appointment = $ret['appointment'];
			$this->view->holiday = $ret['holiday'];
			$this->view->other = $ret['other'];
			
			$this->view->current_days = $current_days = $ret['current_days'];
			$this->view->events_info = $events_info;
			$this->view->events_info_counts = $events_info_counts;
		}
	}
	
	public function ajaxAddAction() 
	{
		$this->view->layout()->disableLayout();
		
		$type = intval($this->_request->type);
		
		if (!in_array($type, array(1, 2, 3))) 
			$type = 1;
		
		$date = $this->_request->date;
		$hour = $this->_request->hour;
		$minute = $this->_request->minute;
		$free_text = $this->_request->free_text;
		$alert = intval($this->_request->alert);
		
		if (!in_array($alert, array(1, 2))) 
			$alert = 2;
		
		$alert_type = $this->_request->alert_type;
		
		if ($alert_type)
			foreach ($alert_type as $k => $v)
				if (!in_array(intval($v), array(1, 2)))
					unset($alert_type[$k]);
		
		if ($alert_type && !in_array(1, $alert_type) && !in_array(2, $alert_type))
			$alert_type = NULL;
		
		$alert_time = intval($this->_request->alert_time);
		$alert_time_unit = intval($this->_request->alert_time_unit);
		
		if (!in_array($alert_time_unit, array(1, 2, 3))) 
			$alert_time_unit = 1;
		
		if ($this->_request->isPost())
		{
			$validator = new Cubix_Validator();
			
			if (!$date)
				$validator->setError('date', 'Required');
			else 
			{
				$date = date('Y-m-d', $this->_request->date) . ' ' . $hour . ':' . $minute . ':00';
			}
			
			if (!strlen($free_text))
				$validator->setError('free_text', 'Required');
			
			if ($alert == 1)
			{
				if (is_null($alert_type))
					$validator->setError('alert_type', 'Required');
				else
					$alert_type = implode (',', $alert_type);
				
				if (!$alert_time)
					$validator->setError('alert_time', 'Required');
			}
			else
			{
				$alert_type = NULL;
				$alert_time = NULL;
				$alert_time_unit = NULL;
			}
			
			if ($this->user->isAgency())
			{
				$escort_id = intval($this->_request->escort_id);
				$ag_id = $this->user->getAgency()->id;
			
				if (!$this->_model->isOwnerAgency($escort_id, $ag_id))
				{
					die;
				}
			}
			
			if ( $validator->isValid() ) {
				$data = array(
					'user_id' => $this->user->id,
					'type' => $type,
					'date' => $date,
					'text' => $free_text,
					'alert' => $alert,
					'alert_type' => $alert_type,
					'alert_time' => $alert_time,
					'alert_time_unit' => $alert_time_unit,
					'application_id' => Cubix_Application::getId()
				);
				
				if ($this->user->isEscort())
				{
					$escort = $this->user->getEscort();
					$data['escort_id'] = $escort->id;
				}
				elseif ($this->user->isAgency())
				{
					$data['escort_id'] = $escort_id;
				}
				
				$this->_model->add($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function ajaxGetEventAction() 
	{
		$this->view->layout()->disableLayout();
		
		$date = $this->_request->date;
		$escort_id = $this->_request->escort_id;
		
		if ($escort_id)
		{
			$ag_id = $this->user->getAgency()->id;
			
			if (!$this->_model->isOwnerAgency($escort_id, $ag_id))
			{
				return;
			}
		}
		else
			$escort_id = NULL;
		
		$d_parts = explode('/', $date);
		
		if (count($d_parts) != 3)
		{
			return;
		}
		
		$m = $d_parts[0];
		$d = $d_parts[1];
		$y = $d_parts[2];
		
		if (strlen($m) == 1) $m = '0' . $m;
		if (strlen($d) == 1) $d = '0' . $d;
				
		$d = $y . '-' . $m . '-' . $d;
		$this->view->info = $this->_model->getEventsInfo($d, $this->user->id, $escort_id);
	}
	
	public function ajaxRemoveAction()
	{
		$id = intval($this->_request->id);
		
		if (!$id) die;
		
		$is_owner = $this->_model->isOwner($this->user->id, $id);
		
		if (!$is_owner) die;
		
		$this->_model->remove($id);
		
		die;
	}
	
	public function ajaxEditAction()
	{
		$this->view->layout()->disableLayout();
		
		$id = intval($this->_request->id);
		
		if (!$id) die;
		
		$is_owner = $this->_model->isOwner($this->user->id, $id);
		
		if (!$is_owner) die;
		
		$this->view->item = $item = $this->_model->get($id);
		
		$type = intval($this->_request->type);
		
		if (!in_array($type, array(1, 2, 3))) 
			$type = 1;
		
		$date = $this->_request->date;
		$hour = $this->_request->hour;
		$minute = $this->_request->minute;
		$free_text = $this->_request->free_text;
		$alert = intval($this->_request->alert);
		
		if (!in_array($alert, array(1, 2))) 
			$alert = 2;
		
		$alert_type = $this->_request->alert_type;
		
		if ($alert_type)
			foreach ($alert_type as $k => $v)
				if (!in_array(intval($v), array(1, 2)))
					unset($alert_type[$k]);
		
		if ($alert_type && !in_array(1, $alert_type) && !in_array(2, $alert_type))
			$alert_type = NULL;
		
		$alert_time = intval($this->_request->alert_time);
		$alert_time_unit = intval($this->_request->alert_time_unit);
		
		if (!in_array($alert_time_unit, array(1, 2, 3))) 
			$alert_time_unit = 1;
		
		if ($this->_request->isPost())
		{
			$validator = new Cubix_Validator();
			
			if (!$date)
				$validator->setError('date', 'Required');
			else 
			{
				$date = date('Y-m-d', $this->_request->date) . ' ' . $hour . ':' . $minute . ':00';
			}
			
			if (!strlen($free_text))
				$validator->setError('free_text', 'Required');
			
			if ($alert == 1)
			{
				if (is_null($alert_type))
					$validator->setError('alert_type', 'Required');
				else
					$alert_type = implode (',', $alert_type);
				
				if (!$alert_time)
					$validator->setError('alert_time', 'Required');
			}
			else
			{
				$alert_type = NULL;
				$alert_time = NULL;
				$alert_time_unit = NULL;
			}
			
			if ( $validator->isValid() ) {
				$data = array(
					'id' => $id,
					'type' => $type,
					'date' => $date,
					'text' => $free_text,
					'alert' => $alert,
					'alert_type' => $alert_type,
					'alert_time' => $alert_time,
					'alert_time_unit' => $alert_time_unit
				);
								
				$this->_model->edit($data);
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	private function _generateEvents($events, $cur_month)
	{
		$days = array();
		$appointment = array();
		$holiday = array();
		$other = array();
		$current_days = array();

		foreach ($events as $e)
		{
			$day = date("n/j/Y", strtotime($e['date']));
			$month = date("n", strtotime($e['date']));
			$c_d = date("j", strtotime($e['date']));
			
			if ($month == $cur_month)
			{
				if (!in_array($c_d, $current_days))
					$current_days[] = $c_d;
			}

			if (!isset($days[$day]))
			{
				$days[$day] = $e['type'];
			}
		}

		foreach ($days as $d => $t)
		{
			switch ($t)
			{
				case PLANNER_TYPE_APPOINTMENT:
					$appointment[] = "'" . $d . "'";
					break;
				case PLANNER_TYPE_HOLIDAY:
					$holiday[] = "'" . $d . "'";
					break;
				case PLANNER_TYPE_OTHER:
					$other[] = "'" . $d . "'";
					break;
			}
		}

		$appointment = implode(',', $appointment);
		$holiday = implode(',', $holiday);
		$other = implode(',', $other);
		
		return array('appointment' => $appointment, 'holiday' => $holiday, 'other' => $other, 'current_days' => $current_days);
	}
}

?>
