<?php

class PrivateV2Controller extends Zend_Controller_Action
{
	/**
	 * @var Model_Escort_Profile
	 */
	protected $profile;

	/**
	 * @var Model_EscortItem
	 */
	protected $escort;

	/**
	 * @var Model_AgencyItem
	 */
	protected $agency;

	/**
	 * @var Zend_Session_Namespace
	 */
	protected $session;

	public function init()
	{
		$this->_request->setParam('no_tidy', true);
		
		$anonym = array();

		$this->user = Model_Users::getCurrent();

		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}

		$this->view->layout()->setLayout('private-v2');

		$this->defines = $this->view->defines = Zend_Registry::get('defines');

		$this->client = Cubix_Api::getInstance();

		if ( $this->user->isAgency() ) {
			$this->agency = $this->view->agency = $this->user->getAgency();

			$this->agencyDashboardAction();
			$this->view->layout()->enableLayout();
		}
		else if ( $this->user->isEscort() ) {
			$this->escort = $this->view->escort = $this->user->getEscort();
		}		

		$this->view->user = $this->user;
		
		$this->view->escort = $this->escort;
	}

	protected $_c = 0;

	protected $steps = array('biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times', 'prices', 'contact-info', 'finish');
	protected $_posted = false;

	public function indexAction()
	{


        
		if ( $this->user->isAgency() ) {
			$this->_helper->viewRenderer->setScriptAction('agency-index');

			$nav = array();
			$nav[] = array('id' => 'agency-profile', 'title' => __('pv2_btn_agency_profile'), 'link' => 'private-v2-agency-profile');
			$nav[] = array('id' => 'profile', 'title' => __('pv2_btn_add_escort_profile'), 'link' => 'private-v2-profile');
			$nav[] = array('id' => 'models', 'title' => __('pv2_btn_manage_models'), 'link' => 'private-v2-escorts');
			$nav[] = array('id' => 'tours', 'title' => __('pv2_btn_city_tours'), 'link' => 'private-v2-tours');
			$nav[] = array('id' => 'verification', 'title' => __('pv2_btn_100p_verified'), 'link' => '100p-verify');
			$nav[] = array('id' => 'myreviews', 'title' => __('pv2_btn_my_escorts_reviews'), 'link' => 'private-v2-escorts-reviews');
			$nav[] = array('id' => 'premium', 'title' => __('pv2_btn_premium_girls'), 'link' => 'private-v2-premium');
			$nav[] = array('id' => 'support', 'title' => 'Support', 'link' => 'private-v2-support');
			$nav[] = array('id' => 'settings', 'title' => __('pv2_btn_settings'), 'link' => 'private-v2-settings');
			$nav[] = array('id' => 'logout', 'title' => __('pv2_btn_logout'), 'link' => 'signout');

			$this->view->navigation = $nav;

			$this->view->escorts = $this->agency->getEscorts();
		}
		else if ( $this->user->isEscort() ) {
			$this->_helper->viewRenderer->setScriptAction('escort-index');

			// Determine the mode depending on if user has profile
			$this->mode = (($this->user->hasProfile()) ? 'update' : 'create');

			$nav = array();

            if(!$this->escort->is_suspicious){
                $nav[] = array('id' => 'profile', 'title' => __('pv2_btn_add_edit_profile'), 'link' => 'private-v2-profile');
                $nav[] = array('id' => 'photos', 'title' => __('pv2_btn_add_edit_photos'), 'link' => 'private-v2-plain-photos');
            }
			$nav[] = array('id' => 'tours', 'title' => __('pv2_btn_set_tours'), 'link' => 'private-v2-tours');
			$nav[] = array('id' => 'verification', 'title' => __('pv2_btn_100p_verified'), 'link' => '100p-verify');
			//$nav[] = array('id' => 'premium', 'title' => 'Go Premium', 'link' => 'private-v2-premium');
			if (!Model_Reviews::hasProduct($this->escort->id, Model_EscortsV2::PRODUCT_NO_REVIEWS))
				$nav[] = array('id' => 'myreviews', 'title' => __('pv2_btn_my_reviews'), 'link' => 'private-v2-escort-reviews');
			$nav[] = array('id' => 'settings', 'title' => __('pv2_btn_settings'), 'link' => 'private-v2-settings');
			$nav[] = array('id' => 'support', 'title' => 'Support', 'link' => 'private-v2-support');
			$nav[] = array('id' => 'logout', 'title' => __('pv2_btn_logout'), 'link' => 'signout');
			
			$this->view->navigation = $nav;
		}
		else if ( $this->user->isMember() ) {

			$is_premium = false;
			if ( isset($this->user->member_data) ) {
				$is_premium = $this->user->member_data['is_premium'];
			}

			$this->_helper->viewRenderer->setScriptAction('member-index');

			// Determine the mode depending on if user has profile
			$this->mode = (($this->user->hasProfile()) ? 'update' : 'create');

			$nav = array();
			$nav[] = array('id' => 'profile', 'title' => __('pv2_btn_member_profile'), 'link' => 'private-v2-member-profile');

			if ( ! $is_premium )
				$nav[] = array('id' => 'upgradepremium', 'title' => __('pv2_btn_up_to_premium'), 'link' => 'private-v2-upgrade-premium');

			$nav[] = array('id' => 'myfavourites', 'title' => __('pv2_btn_my_favorites'), 'link' => 'favorites');
			$nav[] = array('id' => 'myreviews', 'title' => __('pv2_btn_my_reviews'), 'link' => 'private-v2-reviews');
			$nav[] = array('id' => 'settings', 'title' => __('pv2_btn_settings'), 'link' => 'private-v2-settings');
			$nav[] = array('id' => 'support', 'title' => 'Support', 'link' => 'private-v2-support');
			$nav[] = array('id' => 'logout', 'title' => __('pv2_btn_logout'), 'link' => 'signout');
			
			$this->view->navigation = $nav;
		}
	}


  /* Grigor Update */

    public function profileStatusAction(){
        if ( $this->user->isEscort() ) {
            
            $escort = $this->user->getEscort();
			$escort_id = $escort->id;

            $client = Cubix_Api_XmlRpc_Client::getInstance();

            $action = $this->_request->getParam('act');
            $client->call('Escorts.profileStatus', array($escort_id,$action));
            $this->_redirect($this->view->getLink('private-v2'));
            
        }
    }

    public function profileDeleteAction(){
        if ( $this->user->isEscort() ) {
            $this->view->layout()->disableLayout();
            $escort = $this->user->getEscort();
			$escort_id = $escort->id;
           
            if ( $this->_request->isPost() ) {

                $validator = new Cubix_Validator();


                $req = $this->_request;


//                


                if (!trim($req->comment))
                    $validator->setError('comment', 'Required');



                if ( $validator->isValid() ) {
                    $leaving_reason = $req->comment;
                    $del_hash = Cubix_Salt::generateSalt($escort->showname);

                    $client = Cubix_Api_XmlRpc_Client::getInstance();
                    $client->call('Escorts.addDelData', array($escort_id,$del_hash,$leaving_reason));

                    Cubix_Email::sendTemplate('escort_delete', $this->user->email, array(
                        'del_hash' => $del_hash,
                        'email' => $this->user->email,
                        'showname' => $escort->showname
                    ));
                }

                
                die(json_encode($validator->getStatus()));
            }
        }
    }

    public function confirmDeletionAction(){
        if ( $this->user->isEscort() ) {

            $escort = $this->user->getEscort();
			$escort_id = $escort->id;

            $hash = $this->_getParam('hash');
            $model = new Model_EscortsV2();
            
            $us = $model->getById($escort_id);

            if ( $model->checkhash($escort_id, $hash) ) {
                $client = Cubix_Api_XmlRpc_Client::getInstance();
                $client->call('Escorts.deleteV2', array($escort_id));
                $this->_forward('signout', 'private');
            }

        }
        $this->_response->setRedirect($this->view->getLink());
    }
    /* Grigor Update */

	public function agencyDashboardAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$agency_id = $this->agency['id'];
		if ( $req->agency_id ) {
			$agency_id = $req->agency_id;
		}

		$page = 1;
		if ( $req->de_page ) {
			$page = intval($req->de_page);
		}

		if ( $page < 0 ) {
			$page = 1;
		}

		$per_page = 10;
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$cache = Zend_Registry::get('cache');

		$cache_key =  'v2_agency_escorts_' . $this->agency['id'] . '_page_' . $page . $per_page;
		if ( ! $escorts = $cache->load($cache_key) ) {
			$escorts = $escorts = $client->call('Agencies.getNotActiveEscorts', array($agency_id, $page, $per_page));
			$cache->save($escorts, $cache_key, array(), 300);
		}

		$d_escorts = $escorts['result'];
		$d_escorts_count = $escorts['count'];

		$this->view->dash_escorts = $d_escorts;
		$this->view->dash_escorts_count = $d_escorts_count;
		$this->view->dash_agency_id = $agency_id;
		$this->view->dash_page = $page;
		$this->view->dash_per_page = $per_page;

	}

	public function upgradeAction()
	{
		
	}

	public function settingsAction()
	{
		if ( $this->user->isAgency() ) {
			$this->_helper->viewRenderer->setScriptAction('agency-settings');
		}
		else if ( $this->user->isEscort() ) {
			$this->_helper->viewRenderer->setScriptAction('escort-settings');
		}
		else {
			$this->_helper->viewRenderer->setScriptAction('member-settings');
		}

		if ( $this->user->isMember() ) {
			$this->view->data = $this->user->getData(array(
				'recieve_newsletters'
			));
		}
		else {
			$this->view->data = $this->user->getData(array(
				'country_id', 'city_id', 'email', 'recieve_newsletters'
			));
		}		

		if ( $this->_request->isPost() ) {
			
			$validator = new Cubix_Validator();

			$form = new Cubix_Form_Data($this->_request);
			if ( $this->user->isMember() ) {
				
				$save_password = $this->_getParam('password');
				$save_newsletters = $this->_getParam('newsletters');

				$form->setFields(array(
					'password' => '',
					'new_password' => '',
					'new_password_2' => '',
					'recieve_newsletters' => ''
				));
			}
			else {

				$save_profile = $this->_getParam('profile');
				$save_password = $this->_getParam('password');
				$save_newsletters = $this->_getParam('newsletters');

				$form->setFields(array(
					'country_id' => 'int-nz',
					'city_id' => 'int-nz',
					'email' => '',
					'password' => '',
					'new_password' => '',
					'new_password_2' => '',
					'recieve_newsletters' => ''
				));
			}
			$data = $form->getData();

			switch ( true ) {
				case ! is_null($save_profile):
					if ( ! strlen($data['email']) ) {
						// $validator->setError('email', 'Required');
					}
					elseif ( ! Cubix_Validator::validateEmailAddress($data['email']) ) {
						$validator->setError('email', 'Invalid email address');
					}
					elseif ( Cubix_Api::getInstance()->call('existsByEmail', array($data['email'], $this->user->getId())) ) {
						$validator->setError('email', 'Email already exists');
					}

					$model = new Cubix_Geography_Countries();

					if ( ! is_null($data['country_id']) && ! $model->exists($data['country_id']) ) {
						$data['country_id'] = null;
					}

					if ( ! is_null($data['city_id']) ) {
						if ( is_null($data['country_id']) || ! $model->hasCity($data['country_id'], $data['city_id']) ) {
							$data['city_id'] = null;
						}
					}
					
					if ( $validator->isValid() ) {
						$emails = array(
							'old' => $this->user->email,
							'new' => $data['email']
						);

						$this->user->updateData(array(
							'country_id' => $data['country_id'],
							'city_id' => $data['city_id'],
							'email' => $data['email']
						));
						$this->view->data = $this->user->getData(array(
							'country_id', 'city_id', 'email', 'recieve_newsletters'
						));
												
						//newsletter email log
						Cubix_Api::getInstance()->call('addNewlstterEmailLog', array($this->user->id, $this->user->user_type, 'edit', $emails));
						//
					}
					else {
						$this->view->data = $data;
					}

					break;
				case ! is_null($save_password):
					$password = $this->_getParam('old_password');
					
					$new_password = $this->_getParam('new_password');
					$new_password_2 = $this->_getParam('new_password_2');

					if ( $new_password !== $new_password_2 ) {
						$validator->setError('new_password', 'Passwords doesn\'t match');
					}
					else if ( strlen($new_password) > 0 ) {
						try {
							$this->user->updatePassword($password, $new_password);
						}
						catch ( Exception $e ) {
							$validator->setError('old_password', $e->getMessage());
						}
					}

					break;
				case ! is_null($save_newsletters):
					$flag = (bool) $this->_getParam('newsletters');

					$this->user->updateRecieveNewsletters($flag);
					$this->view->data = $this->user->getData(array(
						'country_id', 'city_id', 'email', 'recieve_newsletters'
					));

					break;
			}

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();
				$this->view->errors = $status['msgs'];
			}
		}
	}

	public function addReviewAction()
	{
		if ($this->user->user_type != 'member')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			if (isset($this->_request->escort_id) && intval($this->_request->escort_id) > 0)
			{
				$escort_id = $this->view->escort_id = intval($this->_request->escort_id);

				if (Model_Reviews::hasProduct($escort_id, Model_EscortsV2::PRODUCT_NO_REVIEWS))
				{
					$this->_redirect($this->view->getLink());
					return;
				}
				else
				{
					$lng = Cubix_I18n::getLang();
					list($showname, $cities) = Cubix_Api::getInstance()->call('getEscortDetailsForReviews', array($escort_id, $lng));

					if (!$showname)
					{
						$this->_forward('error', 'error', 'default', array('error_msg' => 'Wrong URL.'));
						return;
					}

					$this->view->showname = $showname;
					$this->view->cities = $cities;
					$this->view->username = $this->user->username;
				}
			}
			else
			{
				$this->_forward('error', 'error', 'default', array('error_msg' => 'Wrong URL.'));
				return;
			}

			if ($this->_request->isPost())
			{
				$validator = new Cubix_Validator();

				$req = $this->_request;

				if (!$req->m_date)
					$validator->setError('m_date', 'Required');

				if (!$req->meeting_city)
					$validator->setError('meeting_city', 'Required');

				if (!$req->meeting_place)
					$validator->setError('meeting_place', 'Required');

				if (!trim($req->duration))
					$validator->setError('duration', 'Required');

				if (!$req->duration_unit)
					$validator->setError('duration_unit', 'Required');

				if (!trim($req->price))
					$validator->setError('price', 'Required');

				if (!$req->currency)
					$validator->setError('currency', 'Required');

				if ($req->looks_rate == '-1')
					$validator->setError('looks_rate', 'Required');

				if ($req->services_rate == '-1')
					$validator->setError('services_rate', 'Required');

				if (!$req->s_kissing)
					$validator->setError('s_kissing', 'Required');

				if (!$req->s_blowjob)
					$validator->setError('s_blowjob', 'Required');

				if (!$req->s_cumshot)
					$validator->setError('s_cumshot', 'Required');

				if (!$req->s_69)
					$validator->setError('s_69', 'Required');

				if (!$req->s_anal)
					$validator->setError('s_anal', 'Required');

				if (!$req->s_sex)
					$validator->setError('s_sex', 'Required');

				if (!$req->s_multiple_times_sex)
					$validator->setError('s_multiple_times_sex', 'Required');

				if (!$req->s_breast)
					$validator->setError('s_breast', 'Required');

				if (!$req->s_attitude)
					$validator->setError('s_attitude', 'Required');

				if (!$req->s_conversation)
					$validator->setError('s_conversation', 'Required');

				if (!$req->s_availability)
					$validator->setError('s_availability', 'Required');

				if (!$req->s_photos)
					$validator->setError('s_photos', 'Required');

				if (!trim($req->t_user_info))
					$validator->setError('t_user_info', 'Required');

				if (!trim($req->t_meeting_date))
					$validator->setError('t_meeting_date', 'Required');

				if ($req->hrs == '-1')
					$validator->setError('hrs', 'Required');

				if ($req->min == '-1')
					$validator->setError('hrs', 'Required');

				if (!trim($req->t_meeting_duration))
					$validator->setError('t_meeting_duration', 'Required');

				if (!trim($req->t_meeting_place))
					$validator->setError('t_meeting_place', 'Required');

				if (!trim($req->t_comments))
					$validator->setError('t_comments', 'Required');

				$captcha = Cubix_Captcha::verify($this->_request->recaptcha_response_field);

				$captcha_errors = array(
					'invalid-site-public-key' => 'We weren\'t able to verify the public key.',
					'invalid-site-private-key' => 'We weren\'t able to verify the private key.',
					'invalid-request-cookie' => 'The challenge parameter of the verify script was incorrect.',
					'incorrect-captcha-sol' => 'The CAPTCHA solution was incorrect.',
					'verify-params-incorrect' => 'The parameters to /verify were incorrect, make sure you are passing all the required parameters.',
					'invalid-referrer' => 'reCAPTCHA API keys are tied to a specific domain name for security reasons.',
					'recaptcha-not-reachable' => 'reCAPTCHA never returns this error code. A plugin should manually return this code in the unlikely event that it is unable to contact the reCAPTCHA verify server.'
				);

				//var_dump($captcha);

				if ( strlen($captcha) > 5 ) {
					$validator->setError('captcha', $this->view->t('wrong_secure_text') /* $captcha_errors[$captcha]*/);
				}

				$t_user_info = trim($req->t_user_info);
				$t_meeting_date = trim($req->t_meeting_date);
				$t_meeting_time = $req->hrs . ':' . $req->min;
				$t_meeting_duration = trim($req->t_meeting_duration);
				$t_meeting_place = trim($req->t_meeting_place);

				if ($validator->isValid())
				{
					if ($_SERVER["HTTP_X_FORWARDED_FOR"] != "")
					{
					   // for proxy
					   $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
					   //$proxy = $_SERVER["REMOTE_ADDR"];
					}
					else
					{
						// for normal user
						$ip = $_SERVER["REMOTE_ADDR"];
						//$proxy = '';
					}

					$req->setParam('ip', $ip);
					$req->setParam('user_id', $this->user->id);

					list($sms_unique, $phone_to) = Cubix_Api::getInstance()->call('addReview', array($this->getRequest()->getParams()));

					if (strlen(trim($phone_to)) > 0)
					{
						$text = $this->view->t('review_sms_template', array(
							'user_info' => $t_user_info,
							'date' => $t_meeting_date,
							'meeting_place' => $t_meeting_place,
							'duration' => $t_meeting_duration,
							'time' => $t_meeting_time,
							'unique_number' => $sms_unique
						));

						$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());

						$phone_from = $originator;
						$sms_id = Cubix_Api::getInstance()->call('outbox', array($escort_id, $phone_to, $phone_from, $text));

						$sms = new Cubix_SMS(SMS_USERKEY, SMS_PASSWORD);
						
						$sms->setOriginator($originator);
						$sms->addRecipient($phone_to, (string)$sms_id);
						$sms->setBufferedNotificationURL(SMS_BufferedNotificationURL);
						$sms->setDeliveryNotificationURL(SMS_DeliveryNotificationURL);
						$sms->setNonDeliveryNotificationURL(SMS_NonDeliveryNotificationURL);

						$sms->setContent($text);
						
						if (1 != $result = $sms->sendSMS())
						{

						}
					}

					$this->_redirect($this->view->getLink('private-v2-reviews'));
				}
				else
				{
					$status = $validator->getStatus();
					$this->view->errors = $status['msgs'];
					$this->view->review = $req;
				}
			}
		}
	}

	public function reviewsAction()
	{
		if ($this->user->user_type != 'member')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			$lng = Cubix_I18n::getLang();
			$this->view->items = Cubix_Api::getInstance()->call('getReviewsForMember', array($this->user->id, $lng));
		}
	}

	public function escortReviewsAction()
	{
		if ($this->user->user_type != 'escort')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			$lng = Cubix_I18n::getLang();
			$this->view->items = Cubix_Api::getInstance()->call('getReviewsForEscort', array($this->user->id, $lng));
		}
	}

	public function addReviewCommentAction()
	{
		$this->view->layout()->disableLayout();

		if ($this->user->user_type != 'escort' && $this->user->user_type != 'agency')
			//$this->_redirect($this->view->getLink('private-v2'));
			die();
		else
		{
			if (isset($this->_request->review_id) && intval($this->_request->review_id) > 0)
			{
				$this->view->review_id = $rev_id = $this->_request->review_id;
				$user_id = $this->user->id;
				$do = false;

				if ($this->user->user_type == 'escort')
				{
					if (Cubix_Api::getInstance()->call('checkEscortReview', array($user_id, $rev_id)))
					{
						$do = true;
					}
					else
						die;
				}
				else
				{
					if (Cubix_Api::getInstance()->call('checkEscortReviewForAgency', array($user_id, $rev_id)))
					{
						$do = true;
					}
					else
						die;
				}

				if ($do)
				{
					if ($this->_request->isPost())
					{
						$validator = new Cubix_Validator();

						if (!$this->_request->comment)
							$validator->setError('comment', 'Required');

						if ($validator->isValid())
						{
							Cubix_Api::getInstance()->call('addReviewComment', array($rev_id, $this->_request->comment));
							$result['status'] = 'success';
							$result['signin'] = true;
							echo json_encode($result);
							die;
						}
						else
						{
							$status = $validator->getStatus();
							$this->view->errors = $status['msgs'];
							if ( ! is_null($this->_getParam('ajax')) ) {
								echo(json_encode($status));
								ob_flush();
								die;
							}
						}
					}
				}
			}
			else
				die;
		}
	}

	public function escortsReviewsAction()
	{
		if ($this->user->user_type != 'agency')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			$lng = Cubix_I18n::getLang();
			$this->view->items = Cubix_Api::getInstance()->call('getReviewsForAgencyEscorts', array($this->user->id, $lng));
		}
	}

	public function memberProfileAction()
	{
		$client = new Cubix_Api_XmlRpc_Client();
   
		if ( $this->user )
		{
			$user_data = $client->call('Users.getById', array($this->user->id));
			$this->view->user_data = new Model_UserItem($user_data);
			$this->view->user = $this->user;
		}

		if ( $this->_request->isPost() )
		{
			$data = array();

			$validator = new Cubix_Validator();

			$data = $this->_validateUserProfile($validator);

			$this->view->user_data = new Model_UserItem($data);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];

				return;
			}

			try {
				$result = $client->call('Users.updateProfile', array($this->user->id, $data));
				//newsletter email log
				$emails = array(
					'old' => $this->user->email,
					'new' => $data['email']
				);
				Cubix_Api::getInstance()->call('addNewlstterEmailLog', array($this->user->id, $this->user->user_type, 'edit', $emails));
				//
			}
			catch (Exception $e) {
				var_dump($client->getLastResponse()); die;
			}

			if ( isset($result['error']) ) {
				print_r($result['error']); die;
			}
			else {
				if ( $result !== false ) {
					$this->view->saved = true;
					//echo nl2br(print_r($result, true));
				}
			}
		}
	}

	public function _validateUserProfile(Cubix_Validator $validator)
	{
		$req = $this->_request;

		$email = $req->email;
		if ( ! strlen($email) ) {
			$validator->setError('email', 'Email is required !');
		}
		$data['email'] = $email;

		//Location
		$country = $req->country_id;

		if ( ! $country ) {
			$validator->setError('country_id', 'Country is required !');
		}
		$data['country_id'] = $country;


		$city = $req->city_id;
		if ( ! $city ) {
			$city = null;
		}
		$data['city_id'] = $city;

		return $data;
	}

	protected function _validateAgency(Cubix_Validator $validator)
	{
		$req = $this->_request;

		$defines = Zend_Registry::get('defines');
		$client = new Cubix_Api_XmlRpc_Client();

		$data = array();

		$showname = trim($req->agency_name);
		if ( ! strlen($showname) ) {
			$validator->setError('agency_name', 'Required');
		}
		elseif ( ! preg_match('/^[^-][-_a-z0-9]+$/i', $showname) ) {
			$validator->setError('agency_name', 'Must begin with letter or number and must contain only alphanumeric characters');
		}
		$data['name'] = $showname;

		// Working times block
		$wds = (array) $req->work_days;
		$work_times = array();
		foreach ( $wds as $d => $nil ) {
			if ( ! ($req->work_times_from[$d]) || ! ($req->work_times_to[$d]) ) {
				$validator->setError('work_times_' . $d, 'Select time interval');
			}

			$work_times[$d] = array('from' => $req->work_times_from[$d], 'to' => $req->work_times_to[$d]);
		}
		$data['working_times'] = $work_times;

		$available_24_7 = $req->available_24_7;
		if ( ! $available_24_7 ) {
			$available_24_7 = null;
		}
		$data['available_24_7'] = $available_24_7;


		// Contacts block
		$contact_phone = $req->phone;
		if ( ! $contact_phone ) {

		}
		$data['phone'] = $contact_phone;

		$phone_instructions = $req->phone_instr;
		if ( ! $phone_instructions ) {

		}
		$data['phone_instructions'] = $phone_instructions;

		$contact_email = $req->email;
		if ( ! $contact_email ) {

		}
		$data['email'] = $contact_email;

		$contact_web = $req->web;
		if ( ! $contact_web ) {

		}
		$data['web'] = $contact_web;

		//Location
		$country = $req->country_id;
		if ( ! $country ) {

		}
		$data['country_id'] = Cubix_Application::getById()->country_id;

		$region = $req->region_id;
		if ( ! $region ) {

		}
		$data['region_id'] = $region;

		$city = $req->city_id;
		if ( ! $city ) {

		}
		$data['city_id'] = $city;

		if ( strlen($data['email']) && $client->call('Application.isDomainBlacklisted', array($data['email'])) ) {
			$validator->setError('email', 'Domain is blacklisted');
		}

		if ( strlen($data['web']) && $client->call('Application.isDomainBlacklisted', array($data['web'])) ) {
			$validator->setError('web', 'Domain is blacklisted');
		}

		/* // Sales person
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		$sales_user_id = intval($req->sales_user_id);
		if ( ! $sales_user_id ) $sales_user_id = null;

		if ( ! is_null($sales_user_id) ) {
			if ( ! $client->call('Users.isSalesValid', array($sales_user_id)) ) {
				$sales_user_id = null;
			}
		}
		$data['sales_user_id'] = $sales_user_id; */

		return $data;
	}

	public function agencyProfileAction()
	{
		$client = new Cubix_Api_XmlRpc_Client();
  
		if ( $this->user->isAgency() )
		{
			$agency_data = $client->call('Agencies.getByUserId', array($this->user->id));
			$agency_id = $agency_data['id'];
			$agency_data = array_merge($agency_data, $client->call('Agencies.getInfo', array($agency_data['name'])));

			$this->view->sales_persons = $client->call('Users.getSalesPersons');
			$this->view->agency_data = new Model_AgencyItem($agency_data);
			
		}

		$this->view->user = $this->user;

		if ( $this->_request->isPost() ) {

			$data = array();

			$validator = new Cubix_Validator();

			$data = $this->_validateAgency($validator);

			/* // Assign escort to a sales person
			$client->call('Users.assignAgency', array($data['sales_user_id'], $agency_id));
			unset($data['sales_user_id']); */
			
			$data['application_id'] = $agency_data['application_id'];

			$this->view->agency_data = new Model_AgencyItem($data);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];

				return;
			}

			try {
				$result = $client->call('Agencies.updateAgencyProfile', array($agency_id, $data));
			}
			catch (Exception $e) {
				var_dump($client->getLastResponse()); die;
			}

			if ( isset($result['error']) ) {
				print_r($result['error']); die;
			}
			else {
				if ( $result !== false ) {
					$this->view->saved = true;
					echo nl2br(print_r($result, true));
				}
			}
		}
	}

	public function agencyLogoAction()
	{
		$this->view->layout()->disableLayout();

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isAgency() )
		{
			$agency = $this->user->getAgency();
			$this->view->agency = $agency;

			if ( $this->_request->isPost() )
			{

				if ( $this->_request->a )
				{
					try {
						if ( ! isset($_FILES['logo']) || ! isset($_FILES['logo']['name']) || ! strlen($_FILES['logo']['name']) ) {
							$this->view->uploadError = 'Please select a photo to upload';
							return;
						}
						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($_FILES['logo']['tmp_name'], 'agencies', $agency->application_id, strtolower(@end(explode('.', $_FILES['logo']['name']))));

						$agency_data['logo_hash'] = $image['hash'];
						$agency_data['logo_ext'] = $image['ext'];

						$result = $client->call('Agencies.updateAgencyLogo', array($agency->id, $agency_data));

						$agency = $this->user->getAgency();
						$this->view->agency = $agency;

					} catch (Exception $e) {
						$this->view->uploadError = $e->getMessage();
					}
				}
				else if ( $this->_request->d ) {
					$agency_data['logo_hash'] = null;
					$agency_data['logo_ext'] = null;
					try {
						$client->call('Agencies.updateAgencyLogo', array($agency->id, $agency_data));

						$agency = $this->user->getAgency();
						$this->view->agency = $agency;
					} catch (Exception $e) {
						$this->view->uploadError = $e->getMessage();
					}
				}
			}
		}
	}

	public function toursAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->is_agency = $this->user->isAgency();

		$escort_id = intval($this->_getParam('escort'));
		if ( $escort_id > 0  && !is_null($this->_getParam('info')) ) {
			$model = new Model_Escorts();
			$escort = $model->getById($escort_id);

			if ( ! $escort ) {
				die(json_encode(null));
			}
			else {
				die(json_encode(array(
					'photo' => $escort->getMainPhoto()->getUrl('agency_p100', true),
					'link' => $this->view->getLink('profile', array('showname' => $escort->showname))
				)));
			}
		}

		if ( $this->user->isAgency() ) {
			$this->_helper->viewRenderer->setScriptAction('agency-tours');
			$this->view->escorts = $this->agency->getEscorts();
		}
		else {
			$this->_helper->viewRenderer->setScriptAction('escort-tours');
			$this->escort = $this->view->escort = $this->user->getEscort();
		}
	}

	public function ajaxToursAction($data = array())
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('no_tidy', true);

		$this->view->is_agency = $this->user->isAgency();
		$this->view->tours = array();
        /* Update Grigor */
		$getall = false;
        $agency_id = 0;
        /* Update Grigor */
		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
            /* Update Grigor */
            $agency =  $this->user->getAgency();
            $agency_id = $agency->id;
           
            if($escort_id == -1){
                $getall = true;
            }
            /* Update Grigor */

			if ( ! $this->agency->hasEscort($escort_id) && !$getall) return;
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}

		if ( is_null($escort_id) ) {
			return;
		}
         /* Update Grigor */
		$tours = $this->client->call('getEscortTours', array($escort_id, Cubix_I18n::getLang(), $agency_id));
        $this->view->tours = $tours[0];
       
        /* Update Grigor */


//		echo json_encode(array_merge(array(
//			'data' => $this->client->call('getEscortTours', array($escort_id, Cubix_I18n::getLang())),
//		), $data));
//		die;
	}

	public function editTourAction()
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('no_tidy', true);

		$tour = null;
		if ( ($tour_id = intval($this->_getParam('id'))) > 0 ) {
			$tour = $this->client->call('getEscortTour', array($tour_id, $this->_request->lang_id));
		}
		$this->view->tour = $tour;

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( $tour ) $escort_id = $tour['escort_id'];
			if ( ! $this->agency->hasEscort($escort_id) ) {
				die('Permission denied!');
			}
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
			if ( $tour && $escort_id != $tour['escort_id'] ) {
				die('Permission denied!');
			}
		}
		
		if ( ! $escort_id ) die;


		$model = new Model_Escorts();
		$this->view->escort = $escort = $model->getById($escort_id);

		if ( $this->_request->isPost() ) {
			$form = new Cubix_Form_Data($this->_request);
			$form->setFields(array(
				'id' => 'int-nz',
				'country_id' => 'int-nz',
				'city_id' => 'int-nz',
				'date_from' => 'int',
				'date_to' => 'int',
				'phone' => '',
				'email' => ''
			));
			$i_data = $form->getData();

			$validator = new Cubix_Validator();

			// <editor-fold defaultstate="collapsed" desc="Tour Dates Overlap Validation">
			$tours = $this->client->call('getEscortTours', array($escort_id, $this->_request->lang_id));
			$data['tours_tmp'] = $tours[0];
			if ( count($data['tours_tmp']) ) {
				foreach ( $data['tours_tmp'] as $t_k => $tr ) {
					unset($data['tours_tmp'][$t_k]['country']);
					unset($data['tours_tmp'][$t_k]['city']);
					$data['tours_tmp'][$t_k]['date_from'] = strtotime(date('d-m-Y', $data['tours_tmp'][$t_k]['date_from']));
					$data['tours_tmp'][$t_k]['date_to'] = strtotime(date('d-m-Y', $data['tours_tmp'][$t_k]['date_to']));
				}
			}
			
			$today = strtotime(date('d-m-Y', time()));
			
			if ( $i_data['date_to'] < $today ) {
				$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
			}

			if ( $i_data['date_from'] > $i_data['date_to'] ) {
				$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
			}

			if ( $validator->isValid() ) {
				foreach ( $data['tours_tmp'] as $t ) {
					if ( ($t['date_from'] >= $i_data['date_from'] && $t['date_from'] < $i_data['date_to']) ||
							($t['date_to'] > $i_data['date_from'] && $t['date_to'] <= $i_data['date_to']) ) {
						$found = true;

						$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
						break;
					}
				}
				unset($data['tours_tmp']);
			}
			// </editor-fold>

			// <editor-fold defaultstate="collapsed" desc="Other Fields Validation">
			$model = new Cubix_Geography_Countries();
			if ( ! $model->exists($i_data['country_id']) ) {
				$validator->setError('country_id', 'Invalid country');
			}

			if ( ! $model->hasCity($i_data['country_id'], $i_data['city_id']) ) {
				$validator->setError('city_id', 'Invalid city');
			}

			if ( ! $i_data['date_from'] || ! $i_data['date_to'] ) {
				$validator->setError('date_from', 'Invalid date range');
			}

			if ( $i_data['email'] ) {
				if ( ! Cubix_Validator::validateEmailAddress($i_data['email']) ) {
					$validator->setError('email', 'Invalid email address');
				}
				/*elseif ( Cubix_Application::isDomainBlacklisted($i_data['email']) ) {
					$validator->setError('email', 'Domain is blacklisted');
				}*/
			}
			// </editor-fold>

			if ( $validator->isValid() ) {
				if ( ! $tour ) {
					$this->client->call('addEscortTour', array($escort_id, $i_data));
				}
				else {
					$this->client->call('updateEscortTour', array($escort_id, $tour_id, $i_data));
				}
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function removeToursAction()
	{
		$tours = (array) $this->_getParam('tours');
		foreach ( $tours as $tour_id ) {
			$tour_id = intval($tour_id);
			
			$tour = $this->client->call('getEscortTour', array($tour_id, $this->_request->lang_id));
			if ( ! $tour ) continue;

			if ( $this->user->isAgency() ) {
				if ( ! $this->agency->hasEscort($tour['escort_id']) ) {
					continue; // wrong owner
				}
			}
			elseif ( $this->user->isEscort() ) {
				if ( $this->escort->getId() != $tour['escort_id'] ) {
					continue; // wrong owner
				}
			}
			
			$this->client->call('removeEscortTour', array($tour['escort_id'], $tour['id']));
		}

		die;
	}

	public function ajaxToursAddAction()
	{
		$this->_request->setParam('no_tidy', true);

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( ! $this->agency->hasEscort($escort_id) ) die;
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}

		if ( is_null($escort_id) ) die;

		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'id' => 'int-nz',
			'country_id' => 'int-nz',
			'city_id' => 'int-nz',
			'date_from' => 'int',
			'date_to' => 'int',
			'phone' => '',
			'email' => ''
		));
		$i_data = $form->getData();

		$validator = new Cubix_Validator();

		$tours = $this->client->call('getEscortTours', array($escort_id, $this->_request->lang_id));
		$data['tours_tmp'] =  $tours[0];
		if ( count($data['tours_tmp']) ) {
			foreach ( $data['tours_tmp'] as $t_k => $tr ) {
				unset($data['tours_tmp'][$t_k]['country']);
				unset($data['tours_tmp'][$t_k]['city']);
			}
		}
		$data['tours_tmp'][] = $i_data;

		$today = strtotime(date('d-m-Y', time()));

		foreach ( $data['tours_tmp'] as $i1 => $tour ) {
			//list($id, $country_id, $city_id, $date_from, $date_to, $phone, $email) = $tour;
			$id = $tour['id'];
			$country_id = $tour['country_id'];
			$city_id = $tour['city_id'];
			$date_from = $tour['date_from'];
			$date_to = $tour['date_to'];
			$phone = $tour['phone'];
			
			if ( $id == $i_data['id'] ) continue;

			if ( $date_to < $today ) {
				$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}

			if ( $date_from > $date_to ) {
				$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}

			$found = false;

			foreach ( $data['tours_tmp'] as $i2 => $t1 ) {
				if ( $i1 == $i2 ) continue;

				//list($nil, $nil, $d_f, $d_t, $phone) = $t1;
				$d_f = $t1['date_from'];
				$d_t = $t1['date_to'];

				if ( ($d_f >= $date_from && $d_f < $date_to) || ($d_t > $date_from && $d_t <= $date_to) ) {
					$found = true;

					$validator->setError('tours', 'Some date intervals are invalid or overlapped');
					break;
				}
			}

			if ( $found ) {
				break;
			}
		}
		unset($data['tours_tmp']);



		$model = new Cubix_Geography_Countries();
		if ( ! $model->exists($i_data['country_id']) ) {
			$validator->setError('country', 'Invalid country');
		}

		if ( ! $model->hasCity($i_data['country_id'], $i_data['city_id']) ) {
			$validator->setError('city', 'Invalid city');
		}

		if ( ! $i_data['date_from'] || ! $i_data['date_to'] ) {
			$validator->setError('date', 'Invalid date range');
		}

		if ( $i_data['email'] ) {
			if ( ! Cubix_Validator::validateEmailAddress($i_data['email']) ) {
				$validator->setError('email', 'Invalid email address');
			}/*
			else if (Cubix_Application::isDomainBlacklisted($i_data['email']) ) {
				$validator->setError('email', 'Domain is blacklisted');
			}*/
		}

		if ( $validator->isValid() ) {
			$this->client->call('addEscortTour', array($escort_id, $i_data));
		}
		else {
			die(json_encode($validator->getStatus()));
		}

		$this->ajaxToursAction(array('status' => 'success'));
	}

	public function ajaxToursRemoveAction()
	{
		$this->_request->setParam('no_tidy', true);

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( ! $this->agency->hasEscort($escort_id) ) die;
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}

		$tour_id = intval($this->_getParam('id'));

		$this->client->call('removeEscortTour', array($escort_id, $tour_id));

		$this->ajaxToursAction(array('status' => 'success'));
	}

	private function _loadPhotos()
	{
		$photos = 

		$public_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, false));
		$nil = null;
		$private_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, true));

		return $this->view->photos = array_merge($public_photos, $private_photos);
	}

	public function photosAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->user = $this->user;

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort'));

			if ( 1 > $escort_id ) die;

			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}

			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}

		$this->view->escort = $escort;
		$photos = $this->_loadPhotos();

		$photos_ids = array();
		foreach ( $photos as $photo ) {
			$photo_ids[] = intval($photo['id']);
		}

		$action = $this->_getParam('a');

		$photos_min_count = Cubix_Application::getById(Cubix_Application::getId())->min_photos;

		$this->view->is_private = $is_private = (bool) intval($this->_getParam('is_private'));

		if ( ! is_null($action) ) {
			if ( 'set-main' == $action ) {
				$ids = $this->_getParam('photo_id');
				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = 'Please select at least one photo';
				}

				$photo_id = reset($ids);
				if ( ! in_array($photo_id, $photo_ids) ) {
					return $this->view->actionError = 'Invalid id of one of the photos';
				}
				
				$photo = new Model_Escort_PhotoItem(array('id' => $photo_id));
				$result = $photo->setMain();

				$this->_loadPhotos();
			}
			elseif ( 'delete' == $action ) {
				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = 'Please select at least one photo';
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						return $this->view->actionError = 'Invalid id of one of the photos';
					}
				}

				$photo = new Model_Escort_Photos();
				$result = $photo->remove($ids);

//				// If count of photos is smaller than required, deactivate escort
//				$photos_count = $escort->getPhotosCount();
//				if ( $photos_count < $photos_min_count ) {
//					if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_ACTIVE))) ) {
//						$client->call('Escorts.removeStatusBit', array($escort_id, array(
//							Model_Escorts::ESCORT_STATUS_ACTIVE
//						)));
//					}
//
//					if ( ! $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS))) ) {
//						$client->call('Escorts.setStatusBit', array($escort_id, array(
//							Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS
//						)));
//					}
//				}

				$this->_loadPhotos();
			}
			elseif ( 'upload' == $action ) {
				try {
					//echo json_encode(array('text' => 'ddd'));die;
					if ( ! isset($_FILES['Filedata']) || ! isset($_FILES['Filedata']['name']) || ! strlen($_FILES['Filedata']['name']) ) {
						$this->view->uploadError = 'Please select a photo to upload';
						return;
					}

					// Save on remote storage
					$images = new Cubix_Images();
					$image = $images->save($_FILES['Filedata']['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $_FILES['Filedata']['name']))));

					$image = new Cubix_Images_Entry($image);
					$image->setSize('sthumb');
					$image->setCatalogId($escort->id);
					$image_url = $images->getUrl($image);


					$photo_arr = array(
						'escort_id' => $escort->id,
						'hash' => $image->getHash(),
						'ext' => $image->getExt(),
						'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD
					);

					if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_PROFILE))) ) {
						$photo_arr['is_approved'] = 1;
					}
					
					$photo = new Model_Escort_PhotoItem($photo_arr);

					$model = new Model_Escort_Photos();
					$photo = $model->save($photo);

//					// If count of photos is smaller than required, deactivate escort
//					$photos_count = $escort->getPhotosCount();
//					if ( $photos_count < $photos_min_count ) {
//						if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_ACTIVE))) ) {
//							$client->call('Escorts.removeStatusBit', array($escort_id, array(
//								Model_Escorts::ESCORT_STATUS_ACTIVE
//							)));
//						}
//
//
//						if ( ! $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS))) ) {
//							$client->call('Escorts.setStatusBit', array($escort_id, array(
//								Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS
//							)));
//						}
//					}
//					// Otherwise activate escort
//					else {
//						//echo "cool";
//						if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS))) ) {
//							$client->call('Escorts.removeStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS)));
//						}
//
//						if ( ! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED)) &&
//							! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_OWNER_DISABLED)) )
//						{
//							if (
//								(! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_IS_NEW)) &&
//								! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_NO_PROFILE)) &&
//								! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_PROFILE_CHANGED)))
//							) {
//								$client->call('Escorts.setStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_ACTIVE));
//							}
//						}
//					}

					$this->view->newPhoto = $photo;

					echo json_encode(array(
						'status' => 1,
						'name' => "success"
					));
					die;

				} catch ( Exception $e ) {
					echo json_encode(array(
						'status' => 0,
						'error' => $e->getMessage()
					));
					die;
					$this->view->uploadError = $e->getMessage();
				}
			}
			elseif ( 'set-adj' == $action ) {
				$photo_id = intval($this->_getParam('photo_id'));

				if ( ! in_array($photo_id, $photo_ids) ) {
					die(json_encode(array('error' => 'An error occured')));
				}

				$photo = new Model_Escort_PhotoItem(array(
					'id' => $photo_id
				));

				try {
					$hash = $photo->getHash();
					$result = array(
						'x' => intval($this->_getParam('x')),
						'y' => intval($this->_getParam('y')),
						'px' => floatval($this->_getParam('px')),
						'py' => floatval($this->_getParam('py'))
					);
					$photo->setCropArgs($result);

					// Crop All images
					$size_map = array(
						'backend_thumb' => array('width' => 150, 'height' => 205),
						'medium' => array('width' => 225, 'height' => 300),
						'thumb' => array('width' => 150, 'height' => 200),
						'nlthumb' => array('width' => 120, 'height' => 160),
						'sthumb' => array('width' => 76, 'height' => 103),
						'lvthumb' => array('width' => 75, 'height' => 100),
						'agency_p100' => array('width' => 90, 'height' => 120),
						't100p' => array('width' => 117, 'height' => 97)
					);
					$conf = Zend_Registry::get('images_config');

					get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
					// echo $conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;

					$catalog = $escort_id;
					$a = array();
					if ( is_numeric($catalog) ) {
						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}
					}
					else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
						array_shift($a);
						$catalog = $a[0];

						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}

						$parts[] = $a[1];
					}

					$catalog = implode('/', $parts);

					foreach($size_map as $size => $sm) {
						get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
					}
				}
				catch ( Exception $e ) {
					die(json_encode(array('error' => 'An error occured')));
				}

				die(json_encode(array('success' => true)));
			}
			elseif ( 'make' == substr($action, 0, 4) ) {
				$type = substr($action, 5); if ( ! in_array($type, array('public', 'private')) ) die;
				
				$ids = $this->_getParam('photo_id');
				
				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = 'Please select at least one photo';
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						return $this->view->actionError = 'Invalid id of one of the photos';
					}
				}

				foreach ( $ids as $id ) {
					$photo = new Model_Escort_PhotoItem(array('id' => $id));
					$photo->make($type == 'private' ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD);
				}
				
				$this->_loadPhotos();
			}
			elseif ( 'sort' == $action ) {
				$ids = $this->_getParam('photo_id');
				
				if ( ! is_array($ids) || ! count($ids) ) {
					die('Please select at least one photo');
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						die('Invalid id of one of the photos');
					}
				}

				$model = new Model_Escort_Photos();
				$model->reorder($ids);

				die;
			}

//			if ( 'upload' != $action ) {
//				$this->view->layout()->disableLayout();
//				$this->_helper->viewRenderer->setNoRender(true);
//				die;
//			}
		}
	}

	public function plainPhotosAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->user = $this->user;

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort'));

			if ( 1 > $escort_id ) die;

			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}

			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}

		$this->view->escort = $escort;
		$photos = $this->_loadPhotos();

		$photos_ids = array();
		foreach ( $photos as $photo ) {
			$photo_ids[] = intval($photo['id']);
		}

		$action = $this->_getParam('a');

		$photos_min_count = Cubix_Application::getById(Cubix_Application::getId())->min_photos;

		$this->view->is_private = $is_private = (bool) intval($this->_getParam('is_private'));

		if ( ! is_null($action) ) {
			if ( 'set-main' == $action ) {
				$ids = $this->_getParam('photo_id');
				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = 'Please select at least one photo';
				}

				$photo_id = reset($ids);
				if ( ! in_array($photo_id, $photo_ids) ) {
					return $this->view->actionError = 'Invalid id of one of the photos';
				}

				$photo = new Model_Escort_PhotoItem(array('id' => $photo_id));
				$result = $photo->setMain();

				$this->_loadPhotos();
			}
			elseif ( 'delete' == $action ) {
				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = 'Please select at least one photo';
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						return $this->view->actionError = 'Invalid id of one of the photos';
					}
				}

				$photo = new Model_Escort_Photos();
				$result = $photo->remove($ids);

				$this->_loadPhotos();
			}
			elseif ( 'upload' == $action ) {
				
					$set_photo = false;

					$new_photos = array();
					$upload_errors = array();

					foreach ( $_FILES as $i => $file )
					{
						try {
							if ( ! isset($file['name']) || ! strlen($file['name']) ) {
								continue;
							}
							else {
								$set_photo = true;
							}

							// Save on remote storage
							$images = new Cubix_Images();
							$image = $images->save($file['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $file['name']))));

							$image = new Cubix_Images_Entry($image);
							$image->setSize('sthumb');
							$image->setCatalogId($escort->id);
							$image_url = $images->getUrl($image);


							$photo_arr = array(
								'escort_id' => $escort->id,
								'hash' => $image->getHash(),
								'ext' => $image->getExt(),
								'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD
							);

							if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_PROFILE))) ) {
								$photo_arr['is_approved'] = 1;
							}

							$photo = new Model_Escort_PhotoItem($photo_arr);

							$model = new Model_Escort_Photos();
							$photo = $model->save($photo);

							$new_photos[] = $photo;						
						} catch ( Exception $e ) {
							$upload_errors[$i] = $file['name'] . ' (' . $e->getMessage() . ')';
						}					
				}

				if ( ! $set_photo ) {
					$this->view->uploadError = 'Please select a photo to upload';
				}

				$this->view->newPhotos = $new_photos;
				$this->view->uploadErrors = $upload_errors;
			}
			elseif ( 'set-adj' == $action ) {
				$photo_id = intval($this->_getParam('photo_id'));

				if ( ! in_array($photo_id, $photo_ids) ) {
					die(json_encode(array('error' => 'An error occured')));
				}

				$photo = new Model_Escort_PhotoItem(array(
					'id' => $photo_id
				));

				try {
					$hash = $photo->getHash();
					$result = array(
						'x' => intval($this->_getParam('x')),
						'y' => intval($this->_getParam('y')),
						'px' => floatval($this->_getParam('px')),
						'py' => floatval($this->_getParam('py'))
					);
					$photo->setCropArgs($result);

					// Crop All images
					$size_map = array(
						'backend_thumb' => array('width' => 150, 'height' => 205),
						'medium' => array('width' => 225, 'height' => 300),
						'thumb' => array('width' => 150, 'height' => 200),
						'nlthumb' => array('width' => 120, 'height' => 160),
						'sthumb' => array('width' => 76, 'height' => 103),
						'lvthumb' => array('width' => 75, 'height' => 100),
						'agency_p100' => array('width' => 90, 'height' => 120),
						't100p' => array('width' => 117, 'height' => 97)
					);
					$conf = Zend_Registry::get('images_config');

					get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
					// echo $conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;

					$catalog = $escort_id;
					$a = array();
					if ( is_numeric($catalog) ) {
						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}
					}
					else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
						array_shift($a);
						$catalog = $a[0];

						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}

						$parts[] = $a[1];
					}

					$catalog = implode('/', $parts);

					foreach($size_map as $size => $sm) {
						get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
					}
				}
				catch ( Exception $e ) {
					die(json_encode(array('error' => 'An error occured')));
				}

				die(json_encode(array('success' => true)));
			}
			elseif ( 'make' == substr($action, 0, 4) ) {
				$type = substr($action, 5); if ( ! in_array($type, array('public', 'private')) ) die;

				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = 'Please select at least one photo';
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						return $this->view->actionError = 'Invalid id of one of the photos';
					}
				}

				foreach ( $ids as $id ) {
					$photo = new Model_Escort_PhotoItem(array('id' => $id));
					$photo->make($type == 'private' ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD);
				}

				$this->_loadPhotos();
			}
			elseif ( 'sort' == $action ) {
				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					die('Please select at least one photo');
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						die('Invalid id of one of the photos');
					}
				}

				$model = new Model_Escort_Photos();
				$model->reorder($ids);

				die;
			}

//			if ( 'upload' != $action ) {
//				$this->view->layout()->disableLayout();
//				$this->_helper->viewRenderer->setNoRender(true);
//				die;
//			}
		}
	}

	public function escortsAction()
	{
		if ( ! $this->user->isAgency() ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}

	//	$this->view->escorts = $escorts = $this->agency->getEscorts();

        
 

		$this->view->is_active = (bool) $this->_getParam('is_active');


		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->_request->isPost() ) {
			$action = $this->_getParam('a');

			if ( $this->user->isEscort() ) {
				$escort = $this->user->getEscort();
				$escort_id = $escort->id;
			}
			else {
				$escort_id = intval($this->_getParam('escort_id'));

				if ( 0 == $escort_id ) $escort_id = null;

				if ( ! is_null($escort_id) && ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
					die;
				}

				$model = new Model_Escorts();
				$escort = $model->getById($escort_id);
			}

			switch ( $action ) {
				case 'activate':
					$this->view->actionError = '';
					if ( $escort->status & Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED ) {
						$this->view->actionError .= 'The escort is deactivated by administration!<br/>';
					}

					if ( $escort->status & Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS ) {
						$this->view->actionError .= 'You cannot activate this escort, not enough photos!<br/>';
					}


					if ( $escort->status & Model_Escorts::ESCORT_STATUS_IS_NEW || $escort->status & Model_Escorts::ESCORT_STATUS_NO_PROFILE ) {
						$this->view->actionError .= 'The profile of this escort has not been approved yet!<br/>';
					}

					if ( ! strlen($this->view->actionError) ) {
						$client->call('Escorts.activate', array($escort_id));
						
						$this->view->escorts = $escorts = $this->agency->getEscorts();
					}
					
					break;
				case 'deactivate':
					$client->call('Escorts.deactivate', array($escort_id));

					$this->view->escorts = $escorts = $this->agency->getEscorts();

					break;
				case 'delete':
					// TODO: escort products deletion. Premium package assigned escorts error.
					$client->call('Escorts.deleteV2', array($escort_id));
					$this->view->escorts = $escorts = $this->agency->getEscorts();
					break;
				default:
					
			}
		}

        $per_page = 9;
        $cur_page = 1;

        $escorts_a = $this->agency->getEscortsPerPage($cur_page, $per_page, Model_Escorts::ESCORT_STATUS_ACTIVE); //32 = Active
        $escorts_i = $this->agency->getEscortsPerPage($cur_page,$per_page);//default inactive

        $this->view->escorts_a_count = $escorts_a['escorts_count'];


        $this->view->escorts_i_count = $escorts_i['escorts_count'];

        $this->view->escorts_per_page = $per_page;
        $this->view->current_page = $cur_page;

        $this->view->escorts_active = $escorts_a['escorts'];
        $this->view->escorts_inactive = $escorts_i['escorts'];


	}

	private function _getPremiumList()
	{
		return Cubix_Api::getInstance()->call('premium_getAgencyPremiumEscorts', array($this->agency->id, $this->view->lang()));
	}

	public function premiumAction()
	{
		if ( $this->user->isAgency() ) {
			$this->_helper->viewRenderer->setScriptAction('agency-premium');
			

			$this->view->escorts = $this->_getPremiumList();
		}
		elseif ( $this->user->isEscort() ) {
			$this->_helper->viewRenderer->setScriptAction('escort-premium');
		}
	}

	public function ajaxPremiumSwitchAction()
	{
		// Require user to be an agency
		if ( ! $this->user->isAgency() ) die;

		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'from_escort_id' => 'int-nz',
			'to_escort_id' => 'int-nz',
			'city_ids' => 'arr-int'
		));
		$data = $form->getData();

		// If escort does not have any of these escorts
		if ( ! $this->agency->hasEscort($data['from_escort_id']) ||
				! $this->agency->hasEscort($data['to_escort_id']) ) {
			die;
		}

		$client = Cubix_Api::getInstance();
		$result = $client->call('premium_switchActivePackages', array($data['from_escort_id'], $data['to_escort_id'], $data['city_ids']));

		die(json_encode(array('data' => $this->_getPremiumList(), 'status' => 'success')));
	}

    /* Update Grigor */
    public function ajaxGetAgencyEscortsAction(){
        if ( ! $this->user->isAgency() ) die;

        $this->view->layout()->disableLayout();

        $page = $this->_getParam('cpage');
        $isActive = $this->_getParam('isActive');
        $per_page = 9;
        $status = null;
        $view = "ajax-get-agency-escorts";

        if($isActive){
           $status =  1;
           
        }

        $client = new Cubix_Api_XmlRpc_Client();
		$escorts = $client->call('Agencies.getEscortsByStatus', array($this->agency->id,$page,$per_page,$status));

        $cur_page = $page;

        $this->view->escorts_count = $escorts['escorts_count'];
        $this->view->escortStatus = $status;

        $this->view->escorts_per_page = $per_page;
        $this->view->current_page = $cur_page;

        $this->view->escorts = $escorts['escorts'];


        $this->_helper->viewRenderer->setScriptAction($view);


    }


    public function ajaxEscortsDoAction(){
        $action = $this->_getParam('a');
        $client = new Cubix_Api_XmlRpc_Client();


        $escort_id = intval($this->_getParam('escort_id'));

        if ( 0 == $escort_id ) $escort_id = null;

        if ( ! is_null($escort_id) && ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
            die;
        }

        $model = new Model_Escorts();
        $escort = $model->getById($escort_id);

        $return['success'] = false;
        $return['message'] = '';

        
        if($escort){
            switch ( $action ) {
                case 'activate':

                    if ( $escort->status & Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED ) {
                        $return['message'] .= 'The escort is deactivated by administration!<br/>';
                    }

                    if ( $escort->status & Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS ) {
                        $return['message'] .= 'You cannot activate this escort, not enough photos!<br/>';
                    }


                    if ( $escort->status & Model_Escorts::ESCORT_STATUS_IS_NEW || $escort->status & Model_Escorts::ESCORT_STATUS_NO_PROFILE ) {
                        $return['message'] .= 'The profile of this escort has not been approved yet!<br/>';
                    }



                    if ( ! strlen($return['message']) ) {
                        $client->call('Escorts.activate', array($escort_id));

                        $return['success'] = true;
                    }

                    break;
                case 'deactivate':
                    $client->call('Escorts.deactivate', array($escort_id));

                    $return['success'] = true;

                    break;
                case 'delete':
                    // TODO: escort products deletion. Premium package assigned escorts error.
                    $client->call('Escorts.deleteV2', array($escort_id));
                    $return['success'] = true;
                   // $this->view->escorts = $escorts = $this->agency->getEscorts();
                    break;
                default:

            }
        }

        die(json_encode($return));
    }
    /* Update Grigor */


	public function ajaxEscortsAction()
	{
		// Require user to be an agency
		if ( ! $this->user->isAgency() ) die;

		$escorts = Cubix_Api::getInstance()->call('premium_getAgencyNonPremiumEscorts', array($this->agency->id));
        
		die(json_encode($escorts));
	}

	public function ajaxEscortCitiesAction()
	{
		// Require user to be an agency
		if ( ! $this->user->isAgency() ) die;

		$escort_id = intval($this->_getParam('escort_id'));

		// If this escort does not belong to this agency
		if ( ! $this->agency->hasEscort($escort_id) ) die;

		$cities = Cubix_Api::getInstance()->call('premium_getEscortCities', array($escort_id, $this->view->lang()));
		$limit = Cubix_Api::getInstance()->call('premium_getEscortCitiesLimit', array($escort_id));

		die(json_encode(array('cities' => $cities, 'limit' => $limit)));
	}

	public function getBubbleTextAction()
	{
		if ( ! $this->user->isAgency() && ! $this->user->isEscort() ) {
			die();
		}

		$req = $this->_request;

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($req->escort_id);
		}

		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$bubble = $client->call('Escorts.getBubbleText', array($escort_id));

		die(json_encode($bubble));
	}

	public function addBubbleTextAction()
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}
		}

		$req = $this->_request;

		$text = strip_tags(mb_substr(stripslashes($req->text), 0, 1000, 'UTF-8'));

		$data = array('escort_id' => $escort_id, 'text' => $text);
		if ( strlen($text) > 0 ) {
			$data = array_merge($data, array('status' => 1));
		}
		else {
			$data = array_merge($data, array('status' => 0));
		}
		
		$status = $client->call('Escorts.addBubbleText', array($data));

		echo $status;

		die;
	}

	public function existsByShownameAction()
	{
		$model = new Model_EscortsV2();

		$req = $this->_request;

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort'));

			if ( $escort_id > 0 ) {
				if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
					die;
				}
			}
		}

		//$escort_id = $req->getParam('escort_id', false);
		$showname = $req->getParam('showname', false);

		$status = $model->existsByShowname($showname, $escort_id);

		if ( $status ) {
			$stat = 'true';
		}
		else {
			$stat = 'false';
		}

		die(json_encode(array('status' => $stat)));
	}


    public function escortActivePackageAction()
	{

        $this->view->layout()->disableLayout();

        $escort_id  = $this->_request->getParam('escort_id');
        $isActive       = $this->_request->getParam('isActive');
        
        if ( $this->user->isAgency() AND $this->agency->hasEscort($escort_id) ) {
			
            
			$package = $this->_getEscortActivePackage($escort_id);
            $this->view->package = $package;
            $this->view->isActive = $isActive;
		}

		
	}


    private function _getEscortActivePackage($escort_id){
        return Cubix_Api::getInstance()->call('getEscortActivePackage', array($escort_id,Cubix_I18n::getLang()));
    }
}
