<?php

class AgenciesController extends Zend_Controller_Action
{
	public static $linkHelper;
	
	public function init()
	{
		self::$linkHelper = $this->view->getHelper('GetLink');
	}
	
	public function indexAction()
	{
		   
	}

	public function showAction()
	{
		if ( ! is_null($this->_getParam('ajax')) ) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}
		else {
			$this->view->layout()->setLayout('main-simple');
		}

		$agency_slug = $this->_getParam('agencyName');
		$agency_id = intval($this->_getParam('agency_id'));

		$client = new Cubix_Api_XmlRpc_Client();
		$agency = $client->call('Agencies.getInfo', array($agency_slug, $agency_id));

		if ( ! $agency ) {
			// 404
			die;
		}
		elseif ( isset($agency['error']) ) {
			print_r($agency['error']); die;
		}

		$agency = $this->view->agency = new Model_AgencyItem($agency);

		/* --> Extract page from `req` parameter */
		$req = ltrim($this->_getParam('req'), '/');
		$req = explode('_', $req);

		$page = 1;
		$param_name = $req[0];

		if ( $param_name == 'page' ) {
			$page = $req[1];
		}

		if ( $page < 1 ) $page = 1;
		/* <-- */

		$config = Zend_Registry::get('escorts_config');
		
		$filter = array('e.agency_id = ?' => $agency->id);
		
		$count = 0;
		$escorts = Model_Escort_List::getFiltered($filter, 'last-modified', $page, $config['perPage'], $count);

		if ( isset($escorts[0]) && $escorts[0] ) {
			$this->view->agency_last_mod_date = $escorts[0]->date_last_modified;
		}
		
		$this->view->escorts = $escorts;
		$this->view->count = $count;
		$this->view->params = array('page' => $page);

		$this->view->no_paging = true;
	}	
}
