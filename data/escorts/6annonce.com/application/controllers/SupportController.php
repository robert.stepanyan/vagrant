<?php

class SupportController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_request->setParam('no_tidy', true);

		$anonym = array();

		$this->user = Model_Users::getCurrent();
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}

		$this->view->layout()->setLayout('main-simple');

		$this->view->user = $this->user;

		$this->client = Cubix_Api::getInstance();
		$this->model = new Model_Support();
	}

	public function indexAction()
	{
		$this->ajaxTicketsListAction();
		$this->view->layout()->enableLayout();
	}

	public function ticketAction()
	{
		$req = $this->_request;
		$ticket_id = intval($this->_request->getParam('id'));

		$user = $this->user;

		$data = array('user_id' => $user->id, 'ticket_id' => $ticket_id);

		if ( ! $this->model->isUserTicket($data) ) {
			$this->_response->setRedirect($this->view->getLink('private-v2-support'));
			return;
		}
		
		$ticket_data = $this->model->get($ticket_id);
		$this->view->ticket = $ticket = $ticket_data['ticket'];
		$attach_files = $ticket_data['attach'];
		
		$document_model = new Cubix_Documents();
		foreach($attach_files as &$attach){
			$url = $document_model->getUrl(new Cubix_Images_Entry(array(
					'application_id' => Cubix_Application::getId(),
					'catalog_id' => 'attached',
					'hash' => $attach['hash'],
					'ext' => $attach['ext']
				)));
			$attach['file_url'] = $url;
		}
				
		if (!$ticket['is_read'])
		{
			$this->model->read($ticket_id);
		}

		$this->ajaxCommentsListAction();
		$this->view->attached_files = $attach_files;
		$this->view->layout()->enableLayout();
	}

	public function ajaxTicketsListAction()
	{
		$this->view->layout()->disableLayout();
		if ( $this->_request->lang ) {
			$this->_request->setParam('lang_id', $this->_request->lang);
		}

		$user = $this->user;

		$tickets = $this->model->getAll($user->id);

		if ( count($tickets['opened']) > 0 ) {
			foreach($tickets['opened'] as $k => $ticket) {
				$tickets['opened'][$k]['comments'] = $this->model->getComments($ticket['id']);
			}
		}
		
		$this->view->tickets = $tickets;
		//print_r($tickets);
	}

	public function ajaxTicketFormAction()
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('lang_id', $this->_request->lang);
		//$this->view->sales = $this->user->getSalesAdminPersons();
		$sales_info = $this->user->getSalesPerson();
		$this->view->sales_user_id = $sales_info['id'];
	}

	public function ajaxAddTicketAction()
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('lang_id', $this->_request->lang);
		$req = $this->_request;
		$user = $this->user;
		
		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'attach' => 'arr-int',
			'sale_person' => 'int-nz',
			'subject' => 'notags|special',
			'message'=> 'xss'
		);
			
		$form->setFields($fields);
		$data = $form->getData();
		
		$backend_user_id = $data['sale_person'];
		$subject = $data['subject'];
		$message = $data['message'];
		$attaches = $data['attach'] ? $data['attach'] : NULL;
		//if ( magic_quotes_gpc() ) {
			//$subject = stripslashes($subject);
			//$message = stripslashes($message);
		//}
		
		$validator = new Cubix_Validator();

		if ( ! strlen($subject) ) {
			$validator->setError('subject', 'Subject Required!');
		}

		if ( ! strlen($message) ) {
			$validator->setError('message', 'Message Required!');
		}
		/*if ( intval($backend_user_id) < 1  ) {
			$validator->setError('sale_person', 'Sale Person Required!');
		}*/

		if ( $validator->isValid() ) {

			$data_ticket = array(
				'user_id' => $user->id,
				'subject' => $subject,
				'message' => $message,
				'status' => Model_Support::STATUS_TICKET_OPENED,
				'application_id' => Cubix_Application::getId(),
				'backend_user_id' => $backend_user_id,
				'status_progress' => Model_Support::STATUS_NEW_UNREAD
			);
			$data = array('ticket' => $data_ticket,'attach' => $attaches );
			
			$ticket_id = $this->model->save($data);
			
			$sales = $this->user->getSalesPersonById($backend_user_id);
			
			Cubix_Email::sendTemplate('support_ticket_submited', $user->email, array(
				'username' => $user->username
			));
			
			$conf = Zend_Registry::get('feedback_config');
			$email = $conf['emails']['support'];

			if ( $sales ) {
				if ( strlen($sales['email']) ) {
					$email = $sales['email'];
				}
			}

			$id = $user->id;
			
			if ($user->user_type == 'escort')
			{
				$m = new Model_EscortsV2();
				$id = $m->getByUserId($id)->id;
			}
			
			/*Cubix_Email::sendTemplate('admin_support_ticket_submited', $email, array(
				'user_id' => $id,
				'type' => $user->user_type,
				'username' => $user->username,
				'ticket_id' => $ticket_id,
				'subject' => $subject,
				'message' => $message
			));*/
			
			$req->setParam('ticket_id', $ticket_id);
			$this->ajaxSuccessAction();
			$this->_helper->viewRenderer->setScriptAction('/ajax-success');
			return;
		}

		die(json_encode($validator->getStatus()));
	}


	public function ajaxSuccessAction()
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('lang_id', $this->_request->lang);
		$req = $this->_request;

		$this->view->ticket_id = $req->getParam('ticket_id');
	}

	public function ajaxAddCommentAction()
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('lang_id', $this->_request->lang);
		$req = $this->_request;
		$user = $this->user;

		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'id' => 'int-nz',
			'message'=> 'xss',
			'attach' => 'arr-int'
		);
			
		$form->setFields($fields);
		$data = $form->getData();
		
		$ticket_id = $data['id'];
		$message = $data['message'];
		$attaches = $data['attach'] ? $data['attach'] : NULL;
		//if ( magic_quotes_gpc() ) {
			//$message = stripslashes($message);
		//}

		$validator = new Cubix_Validator();

		if ( ! strlen($message) ) {
			$validator->setError('message', 'Message Required!');
		}
		
		$data = array('user_id' => $user->id, 'ticket_id' => $ticket_id);
		if ( ! $this->model->isUserTicket($data) || $this->model->isClosed($ticket_id)) {
			die(json_encode( array( 'status'=> 'redirect')));
			
		}
		
		if ( $validator->isValid() ) {

			$data_ticket = array(
				'ticket_id' => $ticket_id,
				'comment' => $message,
			);
			$data = array('ticket' => $data_ticket,'attach' => $attaches );
			$this->model->addComment($data);


			//$sales = $this->user->getSalesPerson();			
			$client = new Cubix_Api_XmlRpc_Client();
			$sales = $client->call('Support.getSalesInfo', array($ticket_id));
			
			$conf = Zend_Registry::get('feedback_config');
			$email = $conf['emails']['support'];

			if ( $sales ) {
				if ( strlen($sales['email']) ) {
					$email = $sales['email'];
				}
			}

			Cubix_Email::sendTemplate('admin_support_ticket_comment_added', $email, array(
				'username' => $user->username,
				'ticket_id' => $ticket_id,
				'message' => $message
			));
		}

		die(json_encode($validator->getStatus()));
	}

	public function ajaxCommentsListAction()
	{
		$this->view->layout()->disableLayout();
		$document_model = new Cubix_Documents();
		if ( $this->_request->lang ) {
			$this->_request->setParam('lang_id', $this->_request->lang);
		}

		$user = $this->user;

		$ticket_id = $this->_request->id;

		$comments = $this->model->getComments($ticket_id);
		foreach($comments as &$comment){
			if($comment["attached_files"]){
				foreach($comment["attached_files"] as &$attach){
					$url = $document_model->getUrl(new Cubix_Images_Entry(array(
							'application_id' => Cubix_Application::getId(),
							'catalog_id' => 'attached',
							'hash' => $attach['hash'],
							'ext' => $attach['ext']
						)));
					$attach['file_url'] = $url;
				}
			}
		}
		$this->view->comments = $comments;
	}
	
	public function addAttachedFilesAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$docs = new Cubix_Documents();
		$error = false;
		$extension = end(explode(".", $_FILES["Filedata"]["name"]));
		
		if (!in_array($extension, $docs->allowed_ext  ) )
		{
			$error = 'Not allowed file extension.';
		}

		if ($error) {

			$return = array(
				'status' => '0',
				'error' => $error
			);

		} else {

			// Save on remote storage
			
			$doc = $docs->save($_FILES['Filedata']['tmp_name'], 'attached', $escort->application_id, $extension);
			
			$attach_arr = array(
				'file_name' => $_FILES["Filedata"]["name"],
				'hash' => $doc['hash'],
				'ext' => $doc['ext'],
				
			);
			$attached_id = $this->model->addAttached($attach_arr);
			
			$return = array(
				'status' => '1',
				'id' => $attached_id
			);
		}
		
		echo json_encode($return);
	}
}