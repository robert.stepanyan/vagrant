<?php

class OnlineBillingController extends Zend_Controller_Action
{
	public static $linkHelper;

	const SECRET_PREFIX = 'A7W9DC';
	const GATEWAY_URL = 'https://www.6payment.com/payment';
	const CURRENCY = 'EUR';
	/**
	 * @var Zend_Session_Namespace
	 */
	protected $_session;
	
	const STATUS_PENDING  = 1;
	const STATUS_ACTIVE   = 2;
	const STATUS_EXPIRED  = 3;
	const STATUS_CANCELLED = 4;
	const STATUS_UPGRADED = 5;
	const STATUS_SUSPENDED = 6;

	public static $STATUS_LABELS = array(
		self::STATUS_PENDING  => 'pending',
		self::STATUS_ACTIVE   => 'active',
		self::STATUS_EXPIRED  => 'expired',
		self::STATUS_CANCELLED => 'cancelled',
		self::STATUS_UPGRADED => 'upgraded',
		self::STATUS_SUSPENDED => 'suspended'
	);

	public function init()
	{
		$this->_request->setParam('no_tidy', true);
		$this->view->layout()->setLayout('private-v2');
		
		$anonym = array();
		
		$this->user = Model_Users::getCurrent();
		
		
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		
		if ( ! $this->user->isAgency() && ! $this->user->isEscort() ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}
		
		$this->view->headTitle('Private Area', 'PREPEND');

		$this->_session = new Zend_Session_Namespace('online_billing');
		
		$this->client = new Cubix_Api_XmlRpc_Client();
	}
	
	public function indexAction()
	{
		
	}
	
	public function listAction()
	{
		$payment_methods = array('master', 'visa');
		$payment_method = $this->view->payment_method = $this->_request->payment_method;
		
		if ( $this->user->isEscort() ) {
			$escort_package = $this->client->call('OnlineBilling.checkIfHasPaidPackage', array($this->user->escort_data['escort_id']));
			
			$has_package = false;
			
			if ( $escort_package ) {
				$this->view->addScriptPath($this->view->getScriptPath('online-billing'));
				
				$this->_helper->viewRenderer->setScriptAction("already-has-package");
			}
		}
		
		if ( $this->user->isAgency() ) {
			$user_type = USER_TYPE_AGENCY;
			$gender = null;
		} else {
			$escort_id = $this->user->escort_data['escort_id'];
			
			$user_type = USER_TYPE_SINGLE_GIRL;
			
			$m_escorts = new Model_EscortsV2();
			$escort = $m_escorts->get($escort_id);
			$gender = $escort->gender;
		}
		
		$this->view->packages_list = $this->client->call('OnlineBilling.getPackagesList', array($user_type, $gender));
		$this->view->gender = $gender;
		$this->view->user_type = $user_type;	
		
		$this->ajaxGetShoppingCartAction();
		
		$errors = array();
		$total_amount = 0;
		if ( $this->_request->isPost() ) {
			
			/*if ( ! in_array($payment_method, $payment_methods) ) {
				$errors[] = 'online_billing_err_wrong_pament_method_selected';
			}*/
			
			if ( ! count($this->view->cart_packages) ) {
				$errors[] = 'online_billing_err_no_package_selected';
			}
			
			$found_count = 0;
			if ( count($this->view->cart_packages) ) {
				foreach ( $this->view->cart_packages as $package ) {
					
					$data = unserialize($package['data']);
					if ( isset($data['premium_cities']) && $data['premium_cities'] ) {
						$found_count++;
					}
					
					$total_amount += $package['price'];
					
					if ( isset($package['optional_products']) ) {
						foreach ( $package['optional_products'] as $opt_product ) {
							$total_amount += $opt_product['price'];
						}
					}
				}
				
				if ( $found_count < count($this->view->cart_packages) ) {
					$errors[] = 'online_billing_err_select_premium_cities';
				}
			}
			
			if ( ! count($errors) ) {
				$this->view->render_form = true;
				
				$this->view->amount = number_format($total_amount, 2);
				$this->view->currency = self::CURRENCY;
				$this->view->reference = 'SC-' . $this->user->id;
				$this->view->sha1 = sha1(self::SECRET_PREFIX . $this->view->amount . $this->view->currency . $this->view->reference);
				$this->view->gateway_url = self::GATEWAY_URL;
				
				$config = Zend_Registry::get('system_config');
				
				//if ( $payment_method == 'visa' ) {
					$epg_payment = new Model_EpgGateway();
					$token = $epg_payment->getTokenForAuth($this->view->reference, $total_amount, 'http://www.6annonce.com/online-billing/epg-response');
					$this->client->call('OnlineBilling.storeToken', array($token, $this->user->id));
					$this->view->use_epg = true;
					$this->view->redirect_url = $epg_payment->getPaymentGatewayUrl($token);
				//}
			}
		}
		
		$this->view->errors = $errors;		
		$this->view->layout()->enableLayout();
	}
	
	public function ajaxAddPackageAction()
	{
		$this->view->layout()->disableLayout();
		
		$user_type_map = array(USER_TYPE_AGENCY, USER_TYPE_SINGLE_GIRL);
		$gender_map = array(GENDER_FEMALE, GENDER_TRANS, GENDER_TRANS);
		
		$package_id = $this->view->package_id = (int) $this->_request->package_id;
		$gender = $this->view->gender = (int) $this->_request->gender;
		$user_type = $this->view->user_type = (int) $this->_request->user_type;
		
		$agency_id = null;
		if ( $this->user->isAgency() ) {
			$agency_id = $this->user->agency_data['agency_id'];
		}
		
		if ( $user_type == USER_TYPE_SINGLE_GIRL && !in_array($gender, $gender_map) ) die(')))');
		if ( !in_array($user_type, $user_type_map) ) die(')))');
		
		$this->view->addScriptPath($this->view->getScriptPath('online-billing'));
		
		if ( $user_type == USER_TYPE_AGENCY ) {
			
			$this->view->escorts = $this->client->call('OnlineBilling.getAgencyEscortsByPackage', array($package_id, $agency_id));
			
			$this->_helper->viewRenderer->setScriptAction("add-package-agency");
		} else if ( $user_type == USER_TYPE_SINGLE_GIRL ) {
			
			$m_escorts = new Model_EscortsV2();
			$this->view->escort = $m_escorts->get($this->user->escort_data['escort_id']);
			
			$this->_helper->viewRenderer->setScriptAction("add-package-single");
		}
		
		$optional_products = $this->client->call('OnlineBilling.getOptionalProducts', array($package_id));
		
		if ( count($optional_products) ) {
			foreach ( $optional_products as $i => $product ) {
				$optional_products[$i]['name'] = Cubix_I18n::translate($product['name']);
			}
		}
		
		$this->view->optional_products = json_encode($optional_products);		
		
		
		
		if ( $this->_request->isPost() ) {
						
			$optional_products = $this->_request->optional_products;
			$escort_ids = $this->_request->escort_ids;
			
			if ( ! count($escort_ids) ) {
				die(json_encode(array('status' => 'error'))); exit;
			}
			
			foreach( $escort_ids as $escort_id ) {
				$data[] = array(
					'user_id' => $this->user->id,
					'escort_id' => $escort_id,
					'agency_id' => $agency_id,
					'package_id' => $package_id,
					'data' => ( isset($optional_products[$escort_id]) ) ? serialize(array('optional_products' => $optional_products[$escort_id])) : serialize(array())
				);
			}
			
			$status = $this->client->call('OnlineBilling.addToShoppingCart', array($data));
			
			if ( !is_array($status) )
				die(json_encode(array('status' => 'success')));
			else
				die(json_encode(array('status' => 'error')));
		}
	}
	
	public function ajaxGetShoppingCartAction()
	{
		$this->view->layout()->disableLayout();
		
		if ( $this->user->isAgency() ) {
			$user_type = USER_TYPE_AGENCY;
		} else {
			$user_type = USER_TYPE_SINGLE_GIRL;
		}
		
		$this->view->cart_packages = $this->client->call('OnlineBilling.getShoppingCart', array($this->user->id, $user_type));
	}
	
	public function ajaxRemoveFromShoppingCartAction()
	{
		$this->view->layout()->disableLayout();
		$shopping_cart_id = (int) $this->_request->shopping_cart_id;
		
		if ( ! $shopping_cart_id ) die;
		
		$this->client->call('OnlineBilling.removeFromShoppingCart', array($this->user->id, $shopping_cart_id));
	}
	
	public function ajaxChangeCityAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->shopping_cart_id = $shopping_cart_id = (int) $this->_request->shopping_cart_id;
		
		if ( ! $shopping_cart_id ) die;
		
		$item = $this->client->call('OnlineBilling.getShoppingCartItem', array($shopping_cart_id, $this->user->id));
		
		if ( ! $item ) die;
		
		$m_escorts = new Model_EscortsV2();
		$this->view->escort = $m_escorts->get($item['escort_id']);
		
		$this->view->json_data = json_encode(unserialize($item['data']));
		
		$this->view->working_locations = $this->client->call('OnlineBilling.getWorkingLocations', array($this->view->escort->id));
		$this->view->selected_premium_cities = $item['premium_cities'];
		
		if ( $this->_request->isPost() ) {
			
			$premium_cities = $this->_request->premium_cities;
			
			if ( ! count($premium_cities) ) {
				die(json_encode(array('status' => 'error', 'error' => Cubix_I18n::translate('select_premium_city'))));
			}
			
			$found = false;
			foreach ( $this->view->working_locations as $location ) {
				if ( in_array($location['id'], $premium_cities) ) {
					$found = true;
				}
			}
			
			if ( ! $found ) {
				die(json_encode(array('status' => 'error', 'error' => Cubix_I18n::translate('city_is_not_in_working_locations'))));
			}
			
			$status = $this->client->call('OnlineBilling.addPremiumCity', array($shopping_cart_id, $this->user->id, $premium_cities));
			
			if ( $status )
				die(json_encode(array('status' => 'success')));
			else
				die(json_encode(array('status' => 'error')));
			
		}
	}
	
	public function orderHistoryAction()
	{
		if ( $this->user->isAgency() ) {
			$user_type = USER_TYPE_AGENCY;
		} else {			
			$user_type = USER_TYPE_SINGLE_GIRL;
		}
		$this->view->user_type = $user_type;
		$this->ajaxGetOrderHistoryAction();
		
		$this->view->layout()->enableLayout();
	}
	
	public function ajaxGetOrderHistoryAction()
	{
		$this->view->layout()->disableLayout();
		
		$per_page = 10;
		$sort_map = array('asc', 'desc');
		$fields_map = array('order_date', 'package_title', 'showname', 'total_price', 'status');
				
		$page = (int) $this->_getParam('page', 1);
		if ( $page < 1 ) $page = 1;
		
		$ord_dir = $this->_getParam('ord_dir', 'desc');		
		if ( ! in_array($ord_dir, $sort_map) ) {
			$ord_dir = $sort_map[0];
		}
		
		$ord_field = $this->_getParam('ord_field', 'order_date');
		if ( ! in_array($ord_field, $fields_map) ) {
			$ord_field = $fields_map[0];
		}
		
		$data = array(
			'page' => $page,
			'per_page' => $per_page,
			'order_field' => $ord_field,
			'order_dir' => $ord_dir,
		);
		
		$orders = $this->client->call('OnlineBilling.getOrderHistory', array($this->user->id, $data));		
		
		foreach ( $orders['result'] as $k => $package ) {
			$orders['result'][$k]['package_status_text'] = self::$STATUS_LABELS[$package['package_status']];			
		}
		
		$this->view->orders = $orders['result'];
		$this->view->page = $page;
		$this->view->ord_dir = $ord_dir;
		$this->view->ord_field = $ord_field;
		
		$this->view->count = $orders['count'];
	}
	
	/*public function checkoutAction()
	{
		$this->view->layout()->disableLayout();
		
		
	}*/
	
	public function successfulPaymentAction()
	{
		
	}
	
	public function unsuccessfulPaymentAction()
	{
		
	}
	
	public function ajaxShowAdPlaceAction()
	{
		$this->view->layout()->disableLayout();
		
		$this->view->package_id = (int) $this->_request->package_id;
	}
        
    public function epgResponseAction()
	{
		try {
			$this->view->layout()->disableLayout();

			$req = $this->_request;

			$token = $req->Token;
			if ( ! $token ) $this->_redirect('/');

			$epg_payment = new Model_EpgGateway();
			$result = $epg_payment->getTransactionStatus($token);
var_dump($result);die;
			if ( $result['ResultStatus'] == "OK" && $result['TransactionResult'] == "OK" ) {
				$this->_redirect($this->view->getLink('ob-successful'));
			} else {
				$this->_redirect($this->view->getLink('ob-unsuccessful'));
			}
		} catch(Exception $ex) {
			throw $ex;
		}
	}
	
	public function epgGotdResponseAction()
	{
		$this->view->layout()->disableLayout();
		
		$req = $this->_request;
		
		$token = $req->Token;
		if ( ! $token ) $this->_redirect('/');
		
		$epg_payment = new Model_EpgGateway();
		$result = $epg_payment->getTransactionStatus($token);
		
		if ( $result['ResultStatus'] == "OK" && $result['TransactionResult'] == "OK" ) {
			$this->_redirect($this->view->getLink('private-v2-gotd-success'));
		} else {
			$this->_redirect($this->view->getLink('private-v2-gotd-failure'));
		}
	}
}
