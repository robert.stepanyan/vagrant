<?php

class BlMembersController extends Zend_Controller_Action {
	protected $client;
	protected $user;
	
	public function init()
	{
		$anonym = array();

		$this->user = Model_Users::getCurrent();
		$this->client = Cubix_Api_XmlRpc_Client::getInstance();


		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}

		if ( ! $this->user->isEscort() ) {
			$this->_redirect($this->view->getLink(''));
		}		
	}
	
	public function indexAction()
	{
		
	}
	
	public function addBlacklistCommentAction()
	{
		$this->view->layout()->disableLayout();
		
		$escort = $this->user->getEscort();
		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'captcha' => '',
			'comment' => 'notags|special',
			'user_id' => 'int-nz'
		);
		$data->setFields($fields);
		$data = $data->getData();
		
		$this->view->data = $data;
		
		$m_user = new Model_Members();
		$this->view->username = $m_user->getUsernameById($data['user_id']);;
		
		
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['comment']) ) {
				$validator->setError('f_comment', '');
			}
			
			if ( ! $data['captcha'] ) {
				$validator->setError('captcha', '');
			} else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', '');
				}
			}
		
		
			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				die(json_encode($result));
			} else {
				$this->client->call('Members.addBlacklistedComment', array($escort->id, $data['user_id'], $data['comment']));
				
				//NOtify node server 
				$query = http_build_query(array('user_id' => $data['user_id']));
				file_get_contents(Cubix_Application::getById()->url . '/node-chat/add-to-blacklist?' . $query);
				
				$result['msg'] = '
					<p style="padding:0 10px; margin-top: 100px; width: 330px; font-size: 14px"><span class="strong">' . __('blacklist_commen_added') . '</span></p>
				';
				die(json_encode($result));
			}
		}
	}
	
	public function getBlacklistedMembersAction()
	{
		$this->view->layout()->disableLayout();
		
		$page = $this->_request->page;
		$per_page = 3;
		$username = $this->_request->username;
		
		if ( ! $page ) $page = 1;
		
		$this->view->username = $username;
		$this->view->page = $page;
		$this->view->per_page = $per_page;
		
		
		$result = $this->client->call('Members.getBlacklistedMembers', array($username, $page, $per_page));
		
		$this->view->bl_members = $result['data'];
		$this->view->count = $result['count'];
	}
	
	public function getBlacklistedMemberCommentsAction()
	{
		$this->view->layout()->disableLayout();
		
		$m_users = new Model_Users();
		
		$user_id = $this->_request->user_id;
		$page = $this->_request->page;
		$per_page = 3;
		
		$user = $m_users->getById($user_id);
		
		if ( ! $page ) $page = 1;
		
		$this->view->page = $page;
		$this->view->per_page = $per_page;
		
		$result = $this->client->call('Members.getBlacklistedMemberComments', array($user_id, $page, $per_page));

		$this->view->username = $user->username;
		$this->view->user_id = $user->id;
		$this->view->bl_comments = $result['data'];
		$this->view->count = $result['count'];
	}
}

?>
