<?php

class Api_ClassifiedAdsController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;

	protected $_debug = true;

	protected $_log;

	/**
	 * @var Cubix_Api
	 */
	protected $_client;

	public function init()
	{
		$this->_db = Zend_Registry::get('db');
		$this->_client = Cubix_Api::getInstance();

		$this->view->layout()->disableLayout();
	}

	/**
	 * @var Cubix_Cli
	 */
	private $_cli;

	public function syncAction()
	{
		ini_set('memory_limit', '1024M');

		$errors = array();

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/a6-classified-ads-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/a6-classified-ads-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		$cli->out('Fetching \'classified_ads\' table dump from api...');
		$table_dump = $this->_client->call('classified.getAdsTableDump', array());
		
		if ( $table_dump ) {
			try {
				$cli->out('Dropping \'classified_ads\' table ...');
				$this->_db->query('DROP TABLE IF EXISTS classified_ads');
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not drop classified_ads table', 'exception' => $e);
			}
			
			try {
				$cli->out('Creating \'classified_ads\' table ...');
				$this->_db->query($table_dump);
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not create classified_ads table', 'exception' => $e);
			}
		}
		
		$cli->out('Fetching \'classified_ads_images\' table dump from api...');
		$table_dump = $this->_client->call('classified.getAdsImagesTableDump', array());
		
		if ( $table_dump ) {
			try {
				$cli->out('Dropping \'classified_ads_images\' table ...');
				$this->_db->query('DROP TABLE IF EXISTS classified_ads_images');
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not drop classified_ads_images table', 'exception' => $e);
			}
			
			try {
				$cli->out('Creating \'classified_ads_images\' table ...');
				$this->_db->query($table_dump);
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not create classified_ads_images table', 'exception' => $e);
			}
		}
		
		$cli->out('Fetching \'classified_ads_cities\' table dump from api...');
		$table_dump = $this->_client->call('classified.getAdsCitiesTableDump', array());
		
		if ( $table_dump ) {
			try {
				$cli->out('Dropping \'classified_ads_cities\' table ...');
				$this->_db->query('DROP TABLE IF EXISTS classified_ads_cities');
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not drop classified_ads_cities table', 'exception' => $e);
			}
			
			try {
				$cli->out('Creating \'classified_ads_cities\' table ...');
				$this->_db->query($table_dump);
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not create classified_ads_cities table', 'exception' => $e);
			}
		}
		
		$start = 0; $limit = 100;
		do {
			
			$cli->colorize('green')->out('Fetching ads data from api (max 100)...')->reset();
			$data = $this->_client->call('classified.getAds', array($start++ * $limit, $limit));
			
			$count = count($data);
			
			if ( $count ) {
				foreach ( $data as $i => $res ) {
					$cli->out('[' . ($i+1) . '/' . $count . '] Inserting ad: ' . $res['id'])->reset();

					$this->_db->insert('classified_ads', $res);
				}
			}
			
		} while ( count($data) > 0 );
		
		$start = 0; $limit = 100;
		do {
			
			$cli->colorize('green')->out('Fetching ads images data from api (max 100)...')->reset();
			$data = $this->_client->call('classified.getAdsImages', array($start++ * $limit, $limit));
			
			$count = count($data);
			
			if ( $count ) {
				foreach ( $data as $i => $res ) {
					$cli->out('[' . ($i+1) . '/' . $count . '] Inserting ad image: ' . $res['id'])->reset();

					$this->_db->insert('classified_ads_images', $res);
				}
			}
			
		} while ( count($data) > 0 );
		
		$start = 0; $limit = 100;
		do {
			
			$cli->colorize('green')->out('Fetching ads Cities data from api (max 100)...')->reset();
			$data = $this->_client->call('classified.getAdsCities', array($start++ * $limit, $limit));
			
			$count = count($data);
			
			if ( $count ) {
				foreach ( $data as $i => $res ) {
					$cli->out('[' . ($i+1) . '/' . $count . '] Inserting ad city: ' . $res['id'])->reset();

					$this->_db->insert('classified_ads_cities', $res);
				}
			}
			
		} while ( count($data) > 0 );
		
		exit;
	}
	
	public function syncLatestActionsAction()
	{
		ini_set('memory_limit', '1024M');

		$errors = array();

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/6a-latest-actions-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/6a-latest-actions-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		$cli->out('Fetching \'latest_actions\' table dump from api...');
		$table_dump = $this->_client->call('classified.getLatestActionsTableDump', array());
		
		if ( $table_dump ) {
			try {
				$cli->out('Dropping \'latest_actions\' table ...');
				$this->_db->query('DROP TABLE IF EXISTS latest_actions');
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not drop latest_actions table', 'exception' => $e);
			}
			
			try {
				$cli->out('Creating \'latest_actions\' table ...');
				$this->_db->query($table_dump);
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not create latest_actions table', 'exception' => $e);
			}
		}
		
		$start = 0; $limit = 100;
		do {
			
			$cli->colorize('green')->out('Fetching ads data from api (max 100)...')->reset();
			$data = $this->_client->call('classified.getLatestActions', array($start++ * $limit, $limit));
			
			$count = count($data);
			
			if ( $count ) {
				foreach ( $data as $i => $res ) {
					$cli->out('[' . ($i+1) . '/' . $count . '] Inserting action: ' . $res['id'])->reset();

					$this->_db->insert('latest_actions', $res);
				}
			}
			
		} while ( count($data) > 0 );
		
		$cache = Zend_Registry::get('cache');		
		$cache_key = 'latest_action_date_6a';
		
		$cache->save(time(), $cache_key, array());
		
		
		$cli->out('Fetching \'latest_actions_details\' table dump from api...');
		$table_dump = $this->_client->call('classified.getLatestActionsDetailsTableDump', array());
		
		if ( $table_dump ) {
			try {
				$cli->out('Dropping \'latest_actions_details\' table ...');
				$this->_db->query('DROP TABLE IF EXISTS latest_actions_details');
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not drop latest_actions_details table', 'exception' => $e);
			}
			
			try {
				$cli->out('Creating \'latest_actions_details\' table ...');
				$this->_db->query($table_dump);
			}
			catch ( Exception $e ) {
				$errors[] = array('title' => 'Could not create latest_actions_details table', 'exception' => $e);
			}
		}
		
		$start = 0; $limit = 100;
		do {
			
			$cli->colorize('green')->out('Fetching latest actions details data from api (max 100)...')->reset();
			$data = $this->_client->call('classified.getLatestActionsDetails', array($start++ * $limit, $limit));
			
			$count = count($data);
			
			if ( $count ) {
				foreach ( $data as $i => $res ) {
					$cli->out('[' . ($i+1) . '/' . $count . '] Inserting action: ' . $res['id'])->reset();

					$this->_db->insert('latest_actions_details', $res);
				}
			}
			
		} while ( count($data) > 0 );
		
		
		exit;
	}	
}