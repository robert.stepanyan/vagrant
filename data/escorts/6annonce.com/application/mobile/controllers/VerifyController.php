<?php

class Mobile_VerifyController extends Zend_Controller_Action
{
	/**
	 * @var Model_Escorts
	 */
	public $model;
	
	/**
	 * @var Model_EscortItem
	 */
	public $escort;
	
	/**
	 * @var Model_VerifyRequests
	 */
	public $verifyRequests;
	
	public function init()
	{
		$this->view->layout()->setLayout('mobile-private');
		
		$this->user = Model_Users::getCurrent();
		
		if ( ! $this->user ) {
			$this->_response->setRedirect($this->view->getLink('signin'));
			return;
		}
		
		$this->view->setScriptPath('../application/mobile/views/scripts/private');
		
		//$this->view->sitePath()->prepend($this->view->t('100_verification'), $this->view->getLink('100p-verify'));
		
		$this->verifyRequests = new Model_VerifyRequests();
		
		$this->model = new Model_Escorts();
		
		$escort_id = intval($this->_getParam('id'));
		
		if ( $escort_id )
		{
			$m_agencies = new Model_Agencies();
			$agency_id = $m_agencies->getByUserId($this->user->id);

			$this->escort = $this->view->escort = $this->model->getById($escort_id);
			
			if ( ! $this->escort->isInAgency($agency_id->id) )
				$this->_response->setRedirect($this->view->getLink('100p-verify'));
		}
		else
		{
			$this->escort = $this->view->escort = $this->model->getByUserId($this->user->id);
		}
	}
	
	
	protected function _check()
	{
		// Checks if there is an active request
		
		if ( $this->escort->isVerified() ) {
			$this->_forward('confirmed');
		}
		
		if ( $this->activeRequest = $this->escort->getVerifyRequest() ) {
			$this->view->request = $this->activeRequest;
			
			if ( $this->activeRequest->isPending() ) {
				$this->_forward('pending');
			}
			elseif ( $this->activeRequest->isConfirmed() ) {
				$this->_forward('confirmed');
			}
			
			return TRUE;
		}
		
		return FALSE;
	}
	
	public function pendingAction()
	{
		$this->_helper->viewRenderer('idcard-pending');
	}
	
	public function confirmedAction()
	{
		$this->_helper->viewRenderer('idcard-confirmed');
	}
	
	public function indexAction()
	{
		if ( ! $this->user ) {
			$this->_response->setRedirect($this->view->getLink('signin'));
			return;
		}
		
		if ( ! $this->user->isAgency() || $this->_getParam('id') )
		{
			$this->view->is_agency = false;
			
			if ( $this->_check() ) {
				return;
			}
		}
		else 
		{
			$this->view->is_agency = true;
			$escorts = $this->user->getAgency()->getEscorts(array(), $count);
			
			foreach ($escorts as $k => $escort)
			{
				$escort = new Model_EscortItem($escort);
				
				if ( $escort->isVerified() || is_null($escort['photo_hash']) )
				{					
					unset($escorts[$k]);
				}
			}
			//var_dump($escorts); die;
			$this->view->escorts = $escorts;
		}
		
		if ( $this->_request->isPost() ) {
			$method = $this->_getParam('method');
			
			if ( ! in_array($method, array('webcam', 'idcard')) ) {
				die;
			}
			
			( intval($this->_getParam('id')) ) ? $escort_id = '?id=' . intval($this->_getParam('id')) : '';			
			
			$this->_response->setRedirect($this->view->getLink('100p-verify-' . $method) . $escort_id);
		}
	}
	
	public function webcamAction()
	{
		if ( $this->_check() ) {
			return;
		}
		
		//$this->view->sitePath()->append($this->view->t('verify_webcam_session'), $this->view->getLink('100p-verify-webcam'));
		
		$data = new Cubix_Form_Data($this->_request);
		$fields = array(
			'dates' => '',
			'times' => '',
			'comment' => 'notags|special',
			'soft' => '',
			'soft_username' => 'special',
			'inform_method' => 'int',
			'email' => '',
			'phone' => ''
		);
		$data->setFields($fields);
		$data = $data->getData();
		
		$this->view->data = $data;
		
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			
			$data['dates'] = array_slice($data['dates'], 0, 3);
			$data['times'] = array_slice($data['times'], 0, 3);
			
			$selected_dates = array();
			foreach ( $data['dates'] as $i => $date ) {
				if ( $date ) {
					$selected_dates[$i] = $date;
				}
			}
			
			if ( count($selected_dates) < 2 ) {
				$validator->setError('dates', 'Please select at least 2 dates');
			}
			else {
				foreach ( $selected_dates as $index => $date ) {
					if ( $data['dates'][$index] < time() ) {
						$validator->setError('dates', 'Please provide dates in the future');
					}
				}
			}
			
			if ( $validator->isValid('dates') ) {
				foreach ( $selected_dates as $index => $date ) {
					if ( ! isset($data['times'][$index]) || ! $data['times'][$index] ) {
						$validator->setError('times', 'Please select corresponding times');
					}
				}
			}
			
			if ( ! $data['soft'] || ! in_array($data['soft'], array('skype', 'msn')) ) {
				$validator->setError('soft', 'Please select software you\'d like to use');
			}
			
			if ( ! strlen($data['soft_username']) ) {
				$validator->setError('soft_username', 'Username is required');
			}
			
			if ( ! in_array($data['inform_method'], array(Model_VerifyRequests::INFORM_EMAIL, Model_VerifyRequests::INFORM_PHONE)) ) {
				$validator->setError('inform_method', 'Select the way you want to be informed');
			}
			else {
				switch ( $data['inform_method'] ) {
					case Model_VerifyRequests::INFORM_EMAIL:
						if ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
							$validator->setError('email', 'A valid email address is required');
						}
					break;
					case Model_VerifyRequests::INFORM_PHONE:
						if ( ! preg_match('/.+/', $data['phone']) ) {
							$validator->setError('phone', 'A valid phone number is required');
						}
					break;
				}
			}
			
			if ( ! $validator->isValid() ) {
				$result = $validator->getStatus();
				
				$this->view->errors = $result['msgs'];
				return;
			}
			
			$item = new Model_VerifyRequest(array(
				'type' => Model_VerifyRequests::TYPE_WEBCAM,
				'escort_id' => $this->escort->id,
				'comment' => $data['comment'],
				'soft' => $data['soft'],
				'soft_username' => $data['soft_username'],
				'inform_method' => $data['inform_method'],
				'phone' => $data['phone'],
				'email' => $data['email']
				// status default PENDING
			));
			
			$timestamps = $this->_processDates($data);
			
			foreach ( $timestamps as $i => $timestamp ) {
				// that's safe, cause taken from `mktime`
				$item->{'date_' . ($i + 1)} = /*new Zend_Db_Expr('FROM_UNIXTIME(' . */$timestamp/* . ')')*/;
			}
			
			$this->verifyRequests->save($item);
			
			$this->_helper->viewRenderer->setScriptAction('webcam-success');
		}
	}
	
	protected function _processDates($data)
	{
		$timestamps = array();
		
		foreach ( $data['dates'] as $i => $date ) {
			if ( $date > time() && $data['times'][$i] ) {
				list($hours, $minutes) = explode(':', $data['times'][$i]);
				
				$timestamps[$i] = mktime($hours, $minutes, 0, date('m', $date), date('d', $date), date('Y', $date));
			}
		}
		
		return $timestamps;
	}
	
	public function idcardAction()
	{
		if ( $this->_check() ) {
			return;
		}
		
		//$this->view->sitePath()->append($this->view->t('verify_picture_upload'), $this->view->getLink('100p-verify-idcard'));
		
		$session = new Zend_Session_Namespace('verify_idcard');
		
		if ( $session->step == 'overview' ) {
			$this->_forward('idcard-overview');
		}
		
		if ( ! is_array($session->photos) ) {
			$session->photos = array();
		}
		
		/*$data = new Cubix_Form_Data($this->getRequest());
		$data->setFields(array(
			'inform_method' => 'int',
			'email' => 'special',
			'phone' => 'special'
		));
		$this->view->data = $data = $data->getData();*/
		$this->view->data['photos'] = array();
		
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			
			/*if ( ! in_array($data['inform_method'], array(Model_VerifyRequests::INFORM_EMAIL, Model_VerifyRequests::INFORM_PHONE)) ) {
				$validator->setError('inform_method', 'Select the way you want to be informed');
			}
			else {
				switch ( $data['inform_method'] ) {
					case Model_VerifyRequests::INFORM_EMAIL:
						if ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
							$validator->setError('email', 'A valid email address is required');
						}
					break;
					case Model_VerifyRequests::INFORM_PHONE:
						if ( ! preg_match('/.+/', $data['phone']) ) {
							$validator->setError('phone', 'A valid phone number is required');
						}
					break;
				}
			}*/
			
			$photos = $this->_processFiles($validator);
			$photos = array_merge($photos, $session->photos);
			$this->view->data['photos'] = $photos;
			
			if ( ! $validator->isValid() ) {
				$result = $validator->getStatus();
				
				$this->view->errors = $result['msgs'];
				return;
			}
			
			$session->photos = $photos;
			//$session->inform_method = $data['inform_method'];
			//$session->email = $data['email'];
			//$session->phone = $data['phone'];
			
			$session->step = 'overview';
			
			// A workaround, see that idcarOverviewAction checks the request for being post
			$_SERVER['REQUEST_METHOD'] = 'GET';
			
			$this->_forward('idcard-overview');
		}
	}
	
	public function idcardOverviewAction()
	{
		if ( $this->_check() ) {
			return;
		}
		
		$session = new Zend_Session_Namespace('verify_idcard');
		
		if ( $session->step != 'overview' ) {
			die;
		}
		
		// Get photos from session to attach url and comment later
		$photos = $session->photos;
		
		// Get comments from the request
		$comments = $this->_getParam('comment', array());
		if ( ! is_array($comments) ) $comments = array();
		
		// Attach url and comment to each photo uploaded in prev step
		$images = new Cubix_Images();
		foreach ( $photos as $i => $photo ) {
			$photos[$i]['url'] = $images->getServerUrl(new Cubix_Images_Entry(array(
				'catalog_id' => $this->escort->id . '/verify',
				'hash' => $photo['hash'],
				'ext' => $photo['ext'],
				'size' => 't100p',
				'application_id' => $this->escort->getApplicationId()
			)));
			
			$photos[$i]['comment'] = isset($comments[$i]) ? strip_tags(htmlspecialchars(substr(trim($comments[$i]), 0, 512))) : '';
		}
		
		$this->view->photos = $photos;
		
		if ( $this->_request->isPost() ) {
			$item = new Model_VerifyRequest(array(
				'type' => Model_VerifyRequests::TYPE_IDCARD,
				'escort_id' => $this->escort->id
				//'inform_method' => $session->inform_method,
				//'phone' => $session->phone,
				//'email' => $session->email
				// status default PENDING
			));
			
			$item = $this->verifyRequests->save($item);
			
			foreach ( $photos as $photo ) {
				$item->addPhoto(array(
					'hash' => $photo['hash'],
					'ext' => $photo['ext'],
					'comment' => $photo['comment']
				));
			}
			
			$session->unsetAll();
			
			$this->_helper->viewRenderer->setScriptAction('idcard-success');
		}
	}
	
	protected function _processFiles(Cubix_Validator $validator)
	{
		$indexes = array(0, 1, 2, 3);
		
		$photos = array();
		
		$images = new Cubix_Images();
		$config = Zend_Registry::get('images_config');
		
		foreach ( $indexes as $index ) {
			if (
				! isset($_FILES['pic']['name'][$index]) ||
				! isset($_FILES['pic']['tmp_name'][$index]) ||
				! strlen($_FILES['pic']['name'][$index]) ||
				! strlen($_FILES['pic']['tmp_name'][$index])
			) {
				continue;
			}
			
			$ext = explode('.', $_FILES['pic']['name'][$index]);
			$ext = strtolower(end($ext));
			
			$file = $_FILES['pic']['tmp_name'][$index];
			
			try {
				if (!in_array( $ext , $config['allowedExts'])){
					throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
				}
				$photos[$index] = $images->save($file, $this->escort->id . '/verify', $this->escort->application_id, $ext);
				$photos[$index]['name'] = $_FILES['pic']['name'][$index];
				//var_dump($photos);die;
			}
			catch (Exception $e) {
				$error = '';
				//var_dump($e->getMessage());die;
				switch ( $e->getCode() ) {
					case Cubix_Images::ERROR_IMAGE_INVALID:
						$error = $e->getMessage();
						break;
					case Cubix_Images::ERROR_IMAGE_TOO_BIG:
						$error = $e->getMessage();
						break;
					case Cubix_Images::ERROR_IMAGE_TOO_SMALL:
						$error = $e->getMessage();
						break;
					case Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED:
						$error = $e->getMessage();
						break;
					default:
						$error = 'An unexpected error occured when uploading file';
				}
				
				$validator->setError('photo_' . ($index + 1), $error);
				
				continue;
			}
		}
		
		if ( ! count($photos) ) {
			$validator->setError('photos', 'Please upload at least one photo');
		}
		
		return $photos;
	}
}
