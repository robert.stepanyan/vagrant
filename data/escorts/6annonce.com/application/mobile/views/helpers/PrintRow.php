<?php

class Zend_View_Helper_PrintRow
{
	public function printRow($label, $value)
	{

		if ( ! $value ) {
			return '';
		}


		return '<tr><td>' . $label . ':</td><th >' . $value . '</th></tr>';
	}
}
