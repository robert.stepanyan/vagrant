<?php

class Model_Agencies extends Cubix_Model
{
	protected $_table = 'agencies';
	protected $_itemClass = 'Model_AgencyItem';
	
	public static function hit($agency_id)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$result = $client->call('Agencies.updateHitsCount', array($agency_id));
		
		return $result;
	}
	
	/*public function getIdByName($name)
	{
		$sql = "
			SELECT id
			FROM agencies a
			WHERE name = ?
		";
		
		$agency = parent::_fetchRow($sql, $name);
		
		if ( $agency )
			return $agency->id;
		
		return null;
	}*/
	
	public function getIdByName($name)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$result = $client->call('Agencies.getByName', array($name));
		
		return $result;
	}
	
	public function get($agency_id)
	{
		$sql = "
			SELECT a.id, a.user_id, a.name, a.phone, a.phone_instructions, a.email, a.web, a.last_modified, a.logo_hash, a.logo_ext, u.application_id, u.date_registered AS creation_date, a.hit_count
			FROM agencies a
			LEFT JOIN users u ON u.id = a.user_id
			WHERE a.id = ?
		";
		
		return parent::_fetchRow($sql, $agency_id);
	}
	
	public function getByUserId($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$agency = $client->call('Agencies.getByUserId', array($user_id));
		
		$agency = new Model_AgencyItem($agency);
		
		return $agency;
	}
	
	public function getAll()
	{
		
	}

	public function save($agency)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$agency_id = $client->call('Agencies.save', array($agency));
		$agency->setId($agency_id);
		
		return $agency;
	}
}
