<?php

class Model_Pinboard extends Cubix_Model
{
	public function getPosts($user_type, $filter, $page, $per_page)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Pinboard.getPosts', array($user_type, $filter, $page, $per_page));
	}
	
	public function getMyPostsCount($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Pinboard.getMyPostsCount', array($user_id));
	}
	
	public function getMyRepliedPostsCount($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Pinboard.getMyRepliedPostsCount', array($user_id));
	}
	
	public function getMyPostsRepliesCount($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Pinboard.getMyPostsRepliesCount', array($user_id));
	}
	
	public function getMyPosts($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Pinboard.getMyPosts', array($user_id));
	}
	
	public function closePost($user_id, $post_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Pinboard.closePost', array($user_id, $post_id));
	}
	
	public function openPost($user_id, $post_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Pinboard.openPost', array($user_id, $post_id));
	}
	
	public function addPost($user_id, $user_type, $title, $post, $vis, $alert)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Pinboard.addPost', array($user_id, $user_type, $title, $post, $vis, $alert));
	}
	
	public function getPost($user_id, $post_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Pinboard.getPost', array($user_id, $post_id));
	}
	
	public function editPost($user_id, $post_id, $title, $post, $vis, $alert)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Pinboard.editPost', array($user_id, $post_id, $title, $post, $vis, $alert));
	}
	
	public function addReply($user_id, $user_type, $post_id, $reply)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Pinboard.addReply', array($user_id, $user_type, $post_id, $reply));
	}
	
	public function getMyReplies($user_id, $filter)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Pinboard.getMyReplies', array($user_id, $filter));
	}
	
	public function getMyPostsReplies($user_id, $filter)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Pinboard.getMyPostsReplies', array($user_id, $filter));
	}
	
	public function seeMyReplies($post_id, $user_id, $user_type, $page, $per_page)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Pinboard.seeMyReplies', array($post_id, $user_id, $user_type, $page, $per_page));
	}
	
	public function seeMyPostsReplies($post_id, $user_type, $page, $per_page)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Pinboard.seeMyPostsReplies', array($post_id, $user_type, $page, $per_page));
	}
}
