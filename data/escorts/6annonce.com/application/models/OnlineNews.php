<?php

class Model_OnlineNews extends Cubix_Model
{
	public static function getOnlineNews()
	{
		$cache_key = Cubix_Application::getId() . '_online_news_'.Cubix_I18n::getLang();
		
		if ( ! $online_news = self::cache()->load($cache_key) ) {
				
				$online_news = self::db()->fetchRow("SELECT	".Cubix_I18n::getTblField('subject') . " as subject, title, UNIX_TIMESTAMP(DATE(start_date)) AS start_date
													FROM online_news");
				self::cache()->save($online_news, $cache_key, array(), 300);
		}
		return $online_news;
	}
	
}
