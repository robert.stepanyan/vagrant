<?php

class Model_AgencyItem extends Cubix_Model_Item
{
	public function hasEscort($escort_id)
	{
		return Cubix_Api::getInstance()->call('hasAgencyEscort', array($this->getId(), $escort_id));
	}

	public function getEscorts($params = array(), &$escorts_count = null, $ex_escort_id = null)
	{
		/*if ( ! isset($params['filter']) ) {
			$params['filter'] = array();
		}
		$params['filter']['e.agency_id = ?'] = $this->id;

		if ( $ex_escort_id )
		{
			$params['filter']['e.id <> ?'] = $ex_escort_id;
		}*/


		$model = new Model_EscortsV2();
		if ( ! isset($params['filter']) ) {
			$params['filter'] = array();
		}
		$params['filter']['e.agency_id = ?'] = $this->id;

		unset($params['limit']);

//		$count = 0;
//		$escorts = $model->getAll($params, $count);
//
//		$escorts_count = $count;
//
//		return $escorts;

		
		$client = new Cubix_Api_XmlRpc_Client();
		$escorts = $client->call('Agencies.getActiveEscorts', array($this->id));
		//var_dump($escorts);die;
		//$model = new Model_Escorts();
		//$escorts = $model->getAll($params, $count);
		

		return $escorts;
	}
	
	public function getEscortsWithPackages()
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$escorts = $client->call('Agencies.getActiveEscortsWithPackages', array($this->id));
		
		return $escorts;
	}

    public function getEscortsPerPage($page = 1, $per_page = 9,$status = null, $is_susp = false , $order = null){
        

        $client = new Cubix_Api_XmlRpc_Client();

		$escorts = $client->call('Agencies.getEscortsByStatus', array($this->id,$page,$per_page,$status, $is_susp, null,$order));
        return $escorts;
    }

	
	public function getWorkTimes()
	{
		/*$is_open = $this->_adapter->query('
			SELECT
				IF ((
					awt.day_index = ' . date('N') . '
					AND awt.time_from <= ' . date('G') . '
					AND awt.time_to >= ' . date('G') . '
				) OR (
					awt.day_index = ' . date('N') . '
					AND awt.time_from <= ' . date('G') . '
					AND awt.time_from > awt.time_to
				) OR (
					awt.day_index = ' . ( 1 == date('N') ? 7 : date('N') - 1 ) . '
					AND awt.time_to >= ' . date('G') . '
					AND awt.time_from  > awt.time_to 
				), 1, 0) AS is_open
			FROM agency_working_times awt
			WHERE awt.agency_id = ? AND awt.day_index = ?
		', array($this->getId(), date('N')))->fetch()->is_open;
		
		
		$data = $this->_adapter->query('
			SELECT awt.day_index, awt.time_from, awt.time_to
			FROM agency_working_times awt
			WHERE awt.agency_id = ?
		', $this->getId())->fetchAll();
		
		$result = array();
		foreach ( $data as $row ) {
			$result[$row->day_index] = array(
				'from' => $row->time_from,
				'to' => $row->time_to
			);
		}*/
		
		$ret = array('is_open' => $this->is_open, 'working_times' => $this->working_times);
		
		return $ret;
	}
	
	public function getLogoUrl($from_server = false)
	{
		if ( $this->logo_hash ) {
			$images_model = new Cubix_Images();

			if ( $from_server ) {
				return $images_model->getServerUrl(new Cubix_Images_Entry(array(
					'application_id' => $this->application_id,
					'catalog_id' => 'agencies',
					'size' => 'agency_thumb',
					'hash' => $this->logo_hash,
					'ext' => $this->logo_ext
				)));
			}
			else {
				return $images_model->getUrl(new Cubix_Images_Entry(array(
					'application_id' => $this->application_id,
					'catalog_id' => 'agencies',
					'size' => 'agency_thumb',
					'hash' => $this->logo_hash,
					'ext' => $this->logo_ext
				)));
			}
		}
		
		return NULL;
	}
	
	public function getEscortsForGotd()
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$escorts = $client->call('Agencies.getActiveEscortsForGotd', array($this->id));

		return $escorts;
	}
}
