<?php

class Model_Activity
{
	/**
	 * @var Model_Activity
	 */
	protected static $_instance;
	
	const COOKIE_NAME = 'cookie_id';
	const COOKIE_LIFETIME = 365; // Days
	
	/**
	 * @var string
	 */
	protected $_cookie_id;
	
	/**
	 * @var Model_Activity_Client
	 */
	protected $_client;
	
	/**
	 * The list of available loggable actions
	 *
	 * @var array
	 */
	protected $_actions;
	
	/**
	 * @return Model_Activity
	 */
	public static function getInstance()
	{
		if ( empty(self::$_instance) ) {
			self::$_instance = new self();
		}
		
		return self::$_instance;
	}
	
	protected function __construct()
	{
		Zend_Session::start();
		
		$cookie_id = isset($_COOKIE[self::COOKIE_NAME]) ? $_COOKIE[self::COOKIE_NAME] : null;
		
		if ( ! preg_match('#^[a-f0-9]{64}#', $cookie_id) ) {
			$hash = (md5(rand(rand(1000, 9999), rand(1000, 9999)) * microtime(true))) . (md5(rand(rand(1000, 9999), rand(1000, 9999)) * microtime(true)));
			setcookie(self::COOKIE_NAME, $hash, strtotime('+' . self::COOKIE_LIFETIME . ' day'), '/', '.' . Cubix_Application::getById()->host);
			$cookie_id = $_COOKIE[self::COOKIE_NAME] = $hash;
		}
		
		$this->_cookie_id = $cookie_id;
		
		$client = Model_Activity_Client::getByCookieId($cookie_id);
		
		if ( is_null($client) ) {
			$client = new Model_Activity_Client();
			
			$client->setCookieId($cookie_id);
			
			$client->setHttpUserAgent(Model_Activity_Utils::getHttpUserAgent());
			$client->setHttpVia(Model_Activity_Utils::getHttpVia());
			$client->setClientIp(Model_Activity_Utils::getClientIp());
			$client->setProxyIp(Model_Activity_Utils::getProxyIp());
			
			$client->save();
		}
		
		$client->updateAccessTime();
		
		$client->setSessionId(Zend_Session::getId());
		
		$session = $client->getSession();
		
		if ( is_null($session) ) {
			$session = new Model_Activity_Client_Session();
			
			$session->setClientId($client->getData()->id);
			
			$session->setSessionId(Zend_Session::getId());
			
			$user = Model_Users::getCurrent();
			if ( ! is_null($user) ) {
				$session->setUserId($user->id);
			}
			
			$session->save();
		}
		
		$session->updateAccessTime();
		
		/*$GLOBALS['ACTIVITY'] = array(
			'client' => $client->getData(),
			'session' => $session->getData()
		);*/
		
		$this->_client = $client;
	}
	
	/**
	 * @return Model_Activity_Client
	 */
	public function getClient()
	{
		return $this->_client;
	}
	
	public function log($action_slug, array $params = array())
	{
		$db = Zend_Registry::get('db');
		
		if ( empty($this->_actions) ) {
			$actions = $db->query('
				SELECT id, slug FROM activity_actions
			')->fetchAll();
			
			foreach ( $actions as $i => $action ) {
				unset($actions[$i]);
				$actions[$action->slug] = $action->id;
			}
			
			$this->_actions = $actions;
		}
		
		if ( ! isset($this->_actions[$action_slug]) ) {
			throw new Exception('The action with slug `' . $action_slug . '` does not exist');
		}
		
		$action_id = $this->_actions[$action_slug];
		
		if ( ! count($params) ) {
			$params = null;
		}
		else {
			$params = serialize($params);
		}
		
		$db->insert('activity_log', array(
			'session_id' => $this->_client->getSession()->getData()->id,
			'action_id' => $action_id,
			'params' => $params
		));
	}
	
	public static function updateSplashStats($type)
	{
		$db = Zend_Registry::get('db');
		
		$map = array(1, 2);
		
		if ( !in_array($type, $map) ) return false;
		
		$db->insert('splash_screen_stats', array('status' => $type));
	}
}
