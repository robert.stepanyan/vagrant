<?php

class Model_Feedback extends Cubix_Model
{
	public function addFeedback($data)
	{
		$client = Cubix_Api::getInstance();
        
        $feedbackdata = array(
            "name" => @$data['name'],
            "email" => @$data['email'],
            "to_addr" => @$data['to_addr'],
            "escort_showname" => @$data['to_name'],
            "message" => @$data['message'],
            "escort_id" => @$data['to'],
            "flag" => @$data['flag'],
			"status" => @$data['status'],
            "application_id" => @$data['application_id']
        );
		
		// ip
		if(isset($_SERVER['HTTP_CLIENT_IP']))
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_X_CLIENT']))
			$ip = $_SERVER['HTTP_X_CLIENT'];
		else if(isset($_SERVER['REMOTE_ADDR']))
			$ip = $_SERVER['REMOTE_ADDR'];
		else
			$ip = '';

		$feedbackdata['ip'] = $ip;
		//
		
		// city
		$loc = Cubix_Geoip::getClientLocation($ip);
		$city = null;
		
		if (is_array($loc))
			$city = $loc['city'];
		
		$feedbackdata['city'] = $city;
		//
        
		$comment = $client->call('addFeedback', array($feedbackdata) );
	}
}
