<?php

class Model_Activity_Utils
{
	public static function getHttpUserAgent()
	{
		$user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? substr($_SERVER['HTTP_USER_AGENT'], 0, 255) : null;
		
		return $user_agent;
	}
	
	public static function getHttpVia()
	{
		$via = isset($_SERVER['HTTP_VIA']) ? substr($_SERVER['HTTP_VIA'], 0, 255) : null;
		
		return $via;
	}
	
	public static function getClientIp()
	{
		$xff = isset($_SERVER['X-Forwarded-For']) ? $_SERVER['X-Forwarded-For'] : null;
		
		if ( ! is_null($xff) ) {
			list($client_ip) = explode(',', $xff);
			$client_ip = trim($client_ip);
			
			if ( ! self::checkIp($client_ip) ) {
				return null;
			}
			
			return $client_ip;
		}
		
		// Maybe for full security check this field???
		return $_SERVER['REMOTE_ADDR'];
	}
	
	public static function getProxyIp()
	{
		$xff = isset($_SERVER['X-Forwarded-For']) ? $_SERVER['X-Forwarded-For'] : null;
		
		if ( is_null($xff) ) {
			return null;
		}
		
		// Maybe for full security check this field???
		return $_SERVER['REMOTE_ADDR'];
	}
	
	public static function checkIp($ip)
	{
		$ip_parts = explode('.', $ip);
		$error = false;
		
		if ( count($ip_parts) != 4 ) {
			$error = true;
		}
		else {
			foreach ( $ip_parts as $part ) {
				$part = intval($part);
				
				if ( $part < 0 || $part > 255 ) {
					
					$error = true;
					break;
				}
			}
		}
		
		return ! $error;
	}
}
