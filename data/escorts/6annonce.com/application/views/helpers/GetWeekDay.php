<?php

class Zend_View_Helper_GetWeekDay
{
	public function getWeekDay($day_index, $lang_id = 'en_US', $short = false)
	{
		switch ($lang_id) {
			case 'en':
				$lang_id = 'en_US';
			break;
			case 'de':
				$lang_id = 'de_AT';
			break;
			case 'it':
				$lang_id = 'it_IT';
			break;
			case 'fr':
				$lang_id = 'fr_FR';
			break;
			case 'gr':
				$lang_id = 'gr_GR';
			break;
		}

		$day_index++;
		if ( $day_index == 8 ) $day_index = 1;

		$date = new Zend_Date($day_index - 1, Zend_Date::WEEKDAY_DIGIT, $lang_id);

		if ($short)
			$day = ucwords($date->get(Zend_Date::WEEKDAY_SHORT));
		else
			$day = ucwords($date->get(Zend_Date::WEEKDAY));

		return $day;
	}
}
