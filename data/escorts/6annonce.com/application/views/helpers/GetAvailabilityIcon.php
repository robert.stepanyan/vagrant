<?php

class Zend_View_Helper_GetAvailabilityIcon
{
	public function getAvailabilityIcon($available_for)
	{
		//$DEFINITIONS = Zend_Registry::get('defines');
		$icon = '';
		
		switch ($available_for)
		{
	        case AVAILABLE_SAUNA:
	        case AVAILABLE_SALON:
	        case AVAILABLE_APARTMENT:
	        case AVAILABLE_INCALL:
	            $icon = "<img src='/img/icon_incall3.gif' alt='' />";
	            break;
	
	        case AVAILABLE_LOFT:
	        case AVAILABLE_OUTCALL:
	             $icon = "<img src='/img/icon_outcall3.gif' alt='' />";
	             break;
	
	        case AVAILABLE_INCALL_OUTCALL:
	            $icon = "<img src='/img/icon_incall3.gif' alt='' /> <img src='/img/icon_outcall3.gif' />";
	            break;
	    }
	    
	    return $icon;
	}
}
