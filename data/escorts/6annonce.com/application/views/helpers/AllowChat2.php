<?php

class Zend_View_Helper_AllowChat2
{
	public function allowChat2()
	{
		//return false;
		$request = Zend_Controller_Front::getInstance()->getRequest();
		
		if ( $request->getParam('chatVersion') != 2 ) 
		{
			return false;
		}
		
		$user = Model_Users::getCurrent();
		
		if ($user)
		{
			if ($user->user_type == 'agency')
			{
				return false;
			}
		}
		
		return true;
	}
}

