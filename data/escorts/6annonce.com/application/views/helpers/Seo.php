<?php

class Zend_View_Helper_Seo extends Zend_View_Helper_Abstract
{
	public function seo($entity, $primary_id, $tpl_data = array(), $auto_meta = false)
	{		
		$cache = Zend_Registry::get('cache');
		$cache_key = 'seo_inst_' . preg_replace("#\-#", "_", $entity) . "_" . (int)$primary_id;
		
		if ( ! $data = $cache->load($cache_key) ) {
			$data = Model_Seo_Instances::get($entity, $primary_id);
			
			$cache->save($data, $cache_key, array());
		}
		
		$tpl_data = array_merge(array(
			'app_title' => Cubix_Application::getById()->title
		), $tpl_data);
		
		foreach ( (array) $data as $key => $value ) {
			foreach ( $tpl_data as $tpl_var => $tpl_value ) {
				$data->$key = str_replace('%' . $tpl_var . '%', $tpl_value, $data->$key);
			}
		}
		
		if ( $auto_meta && $data ) {
			if ( isset($data->title) && strlen($data->title) ) {
				$this->view->headTitle($data->title, 'SET');
			}
			
			if ( isset($data->keywords) && strlen($data->keywords) ) {
				$this->view->headMeta()->appendName('keywords', $data->keywords);
			}
			
			if ( isset($data->description) && strlen($data->description) ) {
				$this->view->headMeta()->appendName('description', $data->description);
			}
		}
		
		return $data;
	}
}
