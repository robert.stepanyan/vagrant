<?php

class Zend_View_Helper_MergeHeadScript extends Zend_View_Helper_Abstract
{
    public function mergeHeadScript()
    {
        return $this;
    }

    public function __toString()
    {
        $mergePaths = array('/index/js-init', '/index/js-init-exact');
        $mergedUrlPath = '/index/js';

        $html = $this->view->headScript()->__toString();

        $merge = array();

        $html = preg_replace_callback('(<script(?<attrs>\s+[^>]+)>.*</script>)i', function ($match) use (&$merge, $mergePaths) {
            $matches = array();
            preg_match_all('((?<name>[a-z]+)=([\"\'])(?<value>.+?)\2)i', $match['attrs'], $matches);

            $attrs = array();
            foreach ($matches['name'] as $index => $name) {
                $value = $matches['value'][$index];
                $attrs[$name] = html_entity_decode($value);
            }

            if (!isset($attrs['src'])) {
                return $match[0];
            }

            $uri = parse_url($attrs['src']);

            if (isset($uri['path']) && !in_array($uri['path'], $mergePaths)) {
                return $match[0];
            }

            if (isset($uri['query'])) {
                $params = array();
                parse_str($uri['query'], $params);
                foreach ($params as $param => $value) {
                    if ($param == 'view') {
                        if (isset($merge[$param])) {
                            $merge[$param][] = $value;
                        }
                        else {
                            $merge[$param] = array($value);
                        }
                    }
                    else {
                        if (isset($merge[$param])) {
                            unset($merge[$param]);
                        }
                        $merge[$param] = $value;
                    }
                }
            }

            return '';
        }, $html);

        if (isset($merge['view'])) {
			$merge['view'] = array_reverse($merge['view']);
            $merge['view'] = implode(',', $merge['view']);
        }

        $url = $mergedUrlPath;
        if (count($merge) > 0) {
            $url .= '?' . urldecode(http_build_query($merge));
        }

        // merged version
        $html .= "\n" . '<script type="text/javascript" src="' . htmlentities($url) . '"></script>' . "\n";

        return $html;
    }
}
