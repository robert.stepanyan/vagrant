function Config() {
	var config = {
		production : {
			common : {
				applicationId : 2,
				listenPort : 8888,
				host: 'www.6annonce.com',
				authPath : '/get_user_info.php',
				sessionCacheTime: 1000 * 60 * 10, //10 minutes
				sessionCheckInterval : 1 * 60 * 60 * 1000, //1 hour
				imagesHost : 'http://pic.6annonce.com/6annonce.com/',
				avatarSize : 'lvthumb',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusLsdf8A9',
				blWordsLifeTime : 12 * 60 * 60 * 1000, //12 hours
				redisBlWordsDb: 0,
				redisBlWordsKey: 'bl-words-6a',
				blUsersLifeTime : 12 * 60 * 60 * 1000 //12 hours
			},
			db : {
				user:     'chat',
				database: '6annonce',
				password: 'KcFTjFg35CRGb2A2Fb77IOa4r',
				host:     '127.0.0.1'
			},
			memcache : {
				host : '95.183.48.38',
				port : 11211
			}
		},
		development: {
			common : {
				applicationId : 2,
				listenPort : 8888,
				host: 'www.6annonce.com.dev',
				authPath: '/get_user_info.php',
				sessionCacheTime: 1000 * 60 * 10, //10 minutes
				sessionCheckInterval : 1 * 60 * 60 * 1000, //1 hour
				imagesHost : 'http://pic.6annonce.com/6annonce.com/',
				avatarSize : 'lvthumb',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusLsdf8A9',
				blWordsLifeTime : 12 * 60 * 60 * 1000, //12 hours
				redisBlWordsDb: 0,
				redisBlWordsKey: 'bl-words-6a',
				blUsersLifeTime : 12 * 60 * 60 * 1000 //12 hours
			},
			db : {
				user:     'sceon',
				database: '6annonce.com_backend',
				password: '123456',
				host:     '192.168.0.1'
			},
			memcache : {
				host : '88.86.102.33',
				port : 11211
			}
		}
	};
	
	this.get = function(type) {
		if ( type == 'development' ) {
			return config.development;
		} else {
			return config.production;
		}
	}
}

function get(type) {
	conf = new Config();
	return conf.get(type);
}



exports.get = get;
