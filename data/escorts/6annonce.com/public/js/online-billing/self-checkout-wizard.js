Cubix.SelfCheckoutWizard = {}

Cubix.SelfCheckoutWizard.userType;
Cubix.SelfCheckoutWizard.url = 'online-billing-v2/wizard';
Cubix.SelfCheckoutWizard.overlay;
Cubix.SelfCheckoutWizard.loader;
Cubix.SelfCheckoutWizard.inProcess = false;
Cubix.SelfCheckoutWizard.width = 664;
Cubix.SelfCheckoutWizard.Price = 0;
Cubix.SelfCheckoutWizard.mooniformInstance = 0;

Cubix.SelfCheckoutWizard.OptProductsPrices = {};

Cubix.SelfCheckoutWizard.addCitiesCount;
Cubix.SelfCheckoutWizard.packWithPremCities;
Cubix.SelfCheckoutWizard.selectProductError;
	
Cubix.SelfCheckoutWizard.open = function(el) {
	if ( Cubix.SelfCheckoutWizard.inProcess ) return false;

	Cubix.SelfCheckoutWizard.overlay = new Cubix.Overlay(document.body, {has_loader: false, opacity: 0.5, color: '#000'});
	Cubix.SelfCheckoutWizard.overlay.disable();
	
	var x_offset = 100
		y_offset = -50;
	
	var container = new Element('div', {'class': 'self-checkout-wizard-wrapper'}).setStyles({
		left: window.getWidth() / 2 - Cubix.SelfCheckoutWizard.width / 2 - x_offset,
		top:  el.getPosition().y + y_offset,
		opacity: 0,
        position: 'absolute',
        'z-index': 101,
        background: '#fff'
	}).inject(document.body);
	
	Cubix.SelfCheckoutWizard.inProcess = true;

	new Request({
		url: Cubix.SelfCheckoutWizard.url,
		method: 'get',
		onSuccess: function (resp) {
			Cubix.SelfCheckoutWizard.inProcess = false;
			container.set('html', resp);
			
			Cubix.SelfCheckoutWizard.mooniformInstance = new Mooniform(container.getElements('select, input[type="checkbox"], input[type="radio"]'));
			
			container.setStyle('opacity', 1);
			
			Cubix.SelfCheckoutWizard.initEvents()
			
			Cubix.SelfCheckoutWizard.loader = new Cubix.Overlay(container, {loader: _st('loader-small.gif'), position: '50%', no_relative: true});
		}
	}).send();
	
	
}

Cubix.SelfCheckoutWizard.initEvents = function()
{
	$$('.self-checkout-wizard-wrapper .close, .self-checkout-wizard-wrapper .cancel-btn, .self-checkout-wizard-wrapper .cancel-btn-small').addEvent('click', function() {
		$$('.self-checkout-wizard-wrapper').destroy();
		Cubix.SelfCheckoutWizard.overlay.enable();
	});
	
	
	$('package').addEvent('change', function() {
		var optContainer = $$('.self-checkout-wizard-wrapper .optional-products')[0];
		optContainer.empty();
		$('premium-city').set('value', '').setProperty('disabled', 'disabled');
		$$('.optional-products-container').setStyle('display', 'none');
		$$('.selected-cities')[0].empty();
		$$('.self-checkout-wizard-wrapper span.error').set('html', '');
		
		if ( ! this.get('value') ) {
			Cubix.SelfCheckoutWizard.updateMooniform();
			return;
		}
		
		Cubix.SelfCheckoutWizard.loader.disable();
		
		var optProductUrl = 'online-billing-v2/get-optional-products?package_id=' + this.get('value');
		if ( Cubix.SelfCheckoutWizard.userType == 'agency' ) {
			optProductUrl += '&escort_id=' + $('escort').getSelected()[0].get('value')
		}
		new Request.JSON({
			url: optProductUrl,
			method: 'get',
			onSuccess: function (resp) {
				resp.optional_products.each(function(pr, i) {
					var lbl = new Element('label');
					new Element('input', {
						value: pr.id,
						type: 'checkbox',
						name: 'opt_products[]'
					}).inject(lbl);

					new Element('span', {
						html: pr.name
					}).inject(lbl);

					lbl.inject(optContainer);
					new Element('div', {'class' : 'clear'}).inject(optContainer);
					
					if ( pr.price ) {
						Cubix.SelfCheckoutWizard.OptProductsPrices[pr.id] = pr.price;
					}
				});

				resp.additional_cities.each(function(pr, i) {
					var lbl = new Element('label');
					new Element('input', {
						type: 'radio',
						name: 'opt_product_prem_city',
						value: pr.id
					}).inject(lbl);

					new Element('span', {
						html: pr.name
					}).inject(lbl);

					lbl.inject(optContainer);
					new Element('div', {'class' : 'clear'}).inject(optContainer);
					
					if ( pr.price ) {
						Cubix.SelfCheckoutWizard.OptProductsPrices[pr.id] = pr.price;
					}
				});

				if ( resp.additional_cities.length || resp.isPackageWithPremCities ) {
					$('premium-city').removeProperty('disabled');
				}
				if ( resp.additional_cities.length + resp.optional_products.length > 0 ) {
					$$('.optional-products-container').setStyle('display', 'block');
				}
				
				Cubix.SelfCheckoutWizard.updateMooniform();
				
				Cubix.SelfCheckoutWizard.initOptProductsEvents();
				
				Cubix.SelfCheckoutWizard.UpdatePrice();
				Cubix.SelfCheckoutWizard.loader.enable();
			}
		}).send();
	});
	
	$$('.self-checkout-wizard-wrapper .btn-add').addEvent('click', function() {
		if ( $('premium-city').get('disabled') ) return;
		
		if ( ! $('premium-city').get('value') ) return;
		
		if ( $('prem-city-' +  $('premium-city').get('value')) ) {
			$('prem-city-' +  $('premium-city').get('value')).highlight('#f8e7e7');
			return;
		}
		
		var selectedCitiesCount = $$('.self-checkout-wizard-wrapper .prem-city-row').length,
			allowedCities = 0,
			packageId = $('package').getSelected()[0].get('value');
			
		packageId = packageId.split('-')[0];
		
		if ( Cubix.SelfCheckoutWizard.packWithPremCities.indexOf(packageId) ) {
			allowedCities += 1;
		}
		
		if ( $$('input[name="opt_product_prem_city"]:checked').length )  {
			selectedProduct = $$('input[name="opt_product_prem_city"]:checked')[0].get('value');
			
			if ( Cubix.SelfCheckoutWizard.addCitiesCount[selectedProduct]  ) {
				allowedCities += Cubix.SelfCheckoutWizard.addCitiesCount[selectedProduct];
			}
		}
		
		if ( selectedCitiesCount >= allowedCities ) {
			$('premium-city').getParent('.row').getPrevious('span.error').set('html', Cubix.SelfCheckoutWizard.selectProductError);
			return;
		}
		
		var cont = new Element('div', {
			id: 'prem-city-' +  $('premium-city').get('value'),
			'class': 'prem-city-row'
		});
		var removeBtn = new Element('span', {'class': 'remove-city'}).inject(cont);
		new Element('span', {'class': 'city-name', 'html': $('premium-city').getSelected().get('html')[0]}).inject(cont);
		new Element('input', {
			type: 'hidden',
			name: 'premium_cities[]',
			value: $('premium-city').get('value'),
			'class': 'premium-cities'
		}).inject(cont);
		
		removeBtn.addEvent('click', function() {
			this.getParent('div').destroy();
		});
		
		$('premium-city').set('value', '');
		
		cont.inject($$('.selected-cities')[0]);
		Cubix.SelfCheckoutWizard.updateMooniform();
	});
	
	$$('.self-checkout-wizard-wrapper form').set('send', {
		method: 'post',
		onSuccess: function(response) {
			response = JSON.decode(response);
			
			$$('.self-checkout-wizard-wrapper span.error').set('html', '');
			if ( response.status == 'error' ) {
				for(name in response.msgs) {
					$$('.self-checkout-wizard-wrapper [name="' + name + '"]')[0].getParent('.row').getPrevious('span.error').set('html', response.msgs[name]);
				}
			} else {
				if ( Cubix.SelfCheckoutWizard.userType == 'escort' ) {
					window.location = response.url;return;
				} else {
					Cubix.SelfCheckoutWizard.addToCart(response.cart_id);
				}
			}
			
			Cubix.SelfCheckoutWizard.loader.enable();
		}
	});
	
	$$('.checkout-btn').addEvent('click', function() {
		Cubix.SelfCheckoutWizard.Send();
	});
	
	
	/*-->only for agency*/
	
	if ( $('escort') ) {
		$('escort').addEvent('change', function() {
			var optContainer = $$('.self-checkout-wizard-wrapper .optional-products')[0];
			optContainer.empty();
			$('package').empty().setProperty('disabled', 'disabled');
			$('premium-city').set('value', '').empty().setProperty('disabled', 'disabled');
			$$('.price')[0].set('html', '0.00');
			
			new Element('option', {
				value: '',
				html: '---'
			}).inject($('package'));
			
			new Element('option', {
				value: '',
				html: '---'
			}).inject($('premium-city'));
			
			$$('.optional-products-container').setStyle('display', 'none');
			$$('.selected-cities')[0].empty();
			$$('.self-checkout-wizard-wrapper span.error').set('html', '');
			
			
			if ( ! this.get('value') ) {
				Cubix.SelfCheckoutWizard.updateMooniform();
				return;
			}
			
			Cubix.SelfCheckoutWizard.loader.disable();
			new Request.JSON({
				url: 'online-billing-v2/get-escort-data?escort_id=' + this.get('value'),
				method: 'get',
				onSuccess: function(resp) {
					resp.packages.each(function(pr, i) {
						new Element('option', {
							value: pr.id + '-' + pr.price,
							html: pr.name
						}).inject($('package'));
					});
					$('package').removeProperty('disabled');
					
					resp.cities.each(function(pr, i) {
						new Element('option', {
							value: pr.id,
							html: pr.title
						}).inject($('premium-city'));
					});
					
					$('package').removeProperty('disabled');
					
					Cubix.SelfCheckoutWizard.updateMooniform();
					Cubix.SelfCheckoutWizard.loader.enable();
				}
			}).send();
		});
	}
	
	if ( $('add-to-cart') ) {
		$('add-to-cart').addEvent('click', function() {
			Cubix.SelfCheckoutWizard.Send();
		});
	}
	
	$$('.checkout-btn-small').addEvent('click', function() {
		Cubix.SelfCheckoutWizard.loader.disable();
		new Request.JSON({
			url: 'online-billing-v2/checkout',
			method: 'get',
			onSuccess: function(response) {
				
				if ( response.status == 'error' ) {
					$$('.self-checkout-wizard-wrapper .error.cart')[0].set('html', response.msgs['cart']);
				} else {
					window.location = response.url;return;
				}
				Cubix.SelfCheckoutWizard.loader.enable();
				
			}
		}).send();
	});
	/*<--only for agency*/
}

Cubix.SelfCheckoutWizard.initOptProductsEvents = function()
{
	$$('.optional-products-container input').addEvent('change', function(el) {
		Cubix.SelfCheckoutWizard.UpdatePrice();
	});
}

Cubix.SelfCheckoutWizard.Send = function() {
	Cubix.SelfCheckoutWizard.loader.disable();
	$$('.self-checkout-wizard-wrapper form').send();
}

Cubix.SelfCheckoutWizard.UpdatePrice = function()
{
	var packagePrice = $('package').getSelected()[0].get('value').split('-')[1];
	
	var price = packagePrice.toInt();
	
	$$('.optional-products-container input:checked').each(function(el) {
		if ( Cubix.SelfCheckoutWizard.OptProductsPrices[el.get('value')] ) {
			price += Cubix.SelfCheckoutWizard.OptProductsPrices[el.get('value')].toInt();
		}
	});
	
	$$('.self-checkout-wizard-wrapper .price-block .price').set('html', price.toFixed(2));
	$$('.self-checkout-wizard-wrapper .price-block').highlight('#f8e7e7');
}

Cubix.SelfCheckoutWizard.addToCart = function(cartId)
{
	var showname = $('escort').getSelected()[0].get('html'),
		packageName = $('package').getSelected()[0].get('html'),
		cost = $$('.self-checkout-wizard-wrapper .price-block .price')[0].get('html'),
		premCities = $$('.city-name').get('html');
	
		
	tr = new Element('tr');
	var removeBtn = new Element('a', {'class' : 'remove', 'rel' : cartId});
	
	removeBtn.inject(new Element('td').inject(tr));
	new Element('td', {html: showname}).inject(tr);
	new Element('td', {html: packageName}).inject(tr);
	new Element('td', {html: premCities.join(', ')}).inject(tr);
	new Element('td', {html: cost, 'class': 'last'}).inject(tr);
	
	tr.inject($$('.self-checkout-wizard-wrapper table tbody')[0])
	
	totalPrice = $$('.total-price')[0].get('html').toInt() + cost.toInt();
	$$('.total-price')[0].set('html', totalPrice.toFixed(2));
	
	$('escort').getSelected().destroy();
	$('escort').set('value', '').fireEvent('change');
	
	removeBtn.addEvent('click', function() {
		var tr = this.getParent('tr');
		price = tr.getElement('td.last').get('html').toInt();
		
		totalPrice = $$('.total-price')[0].get('html').toInt() - price;
		
		Cubix.SelfCheckoutWizard.loader.disable();
		new Request.JSON({
			url: 'online-billing-v2/remove-from-cart?cart_id=' + this.get('rel'),
			method: 'get',
			onSuccess: function(resp) {
				tr.destroy();
				$$('.total-price')[0].set('html', totalPrice.toFixed(2));
				Cubix.SelfCheckoutWizard.loader.enable();
				
				new Element('option', {html: resp.escort.showname + ' #' + resp.escort.id, value: resp.escort.id}).inject($('escort'));
			}
		}).send();
	});
}

Cubix.SelfCheckoutWizard.updateMooniform = function()
{
	Cubix.SelfCheckoutWizard.mooniformInstance.lookup($$('input[type="checkbox"], input[type="radio"]'));
	Cubix.SelfCheckoutWizard.mooniformInstance.update();
}