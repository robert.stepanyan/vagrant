/* --> Feedback */
Cubix.ChangeCityPopup = {};

Cubix.ChangeCityPopup.inProcess = false;
Cubix.ChangeCityPopup.url = '';
Cubix.ChangeCityPopup.one_city_allowed_error = '';

Cubix.ChangeCityPopup.Show = function (box_height, box_width, el) {
	if ( Cubix.ChangeCityPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();

	var y_offset = -200;
	var x_offset = 100;

	var container = new Element('div', { 'class': 'online-billing-popup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2 - x_offset,
		top:  el.getPosition().y + y_offset,
		opacity: 0,
        position: 'absolute',
        'z-index': 101,
        background: '#fff'
	}).inject(document.body);
	
	Cubix.ChangeCityPopup.inProcess = true;

	new Request({
		url: Cubix.ChangeCityPopup.url + el.get('href'),
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.ChangeCityPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'online-billing-popup-close-btn'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.online-billing-popup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
			
			if ( container.getElements('.remove-city') ) {
				container.getElements('.remove-city').addEvent('click', function(e){
					e.stop();

					this.getParent('div').destroy();
					$$('.online-billing-popup-wrapper').getElements('.error')[0].setStyle('display', 'none');
				});
			}
			
			
			if ( container.getElements('.btn-add')[0] ) {
			
				var add_btn = container.getElements('.btn-add')[0];
				var selected_cities_list = container.getElements('.selected-cities')[0];
				var selbox_cities = container.getElement('select[name=cities]');
				var opt_products = JSON.decode(container.getElements('.json-data')[0].get('html'));
				
				var OPTIONAL_PRODUCT_ADDITIONAL_CITY   = '9';
				var OPTIONAL_PRODUCT_CITY_PREMIUM_SPOT = '5';

				

				add_btn.addEvent('click', function(e){
					e.stop();

					var selected = selbox_cities.getSelected();

					if ( selected.get('value')[0] == 0 ) {
						selbox_cities.highlight('#f8e7e7');
						//alert('Please select city!');
						return false;
					}

					var hidden_city_ids = container.getElements('.hidden-city-ids');
					var already_in_list = false;
					hidden_city_ids.each(function(input){
						if ( input.get('value') == selected.get('value')[0] ) {
							input.getParent('div').highlight('#f8e7e7');
							already_in_list = true;
						}
					});
					
					if ( already_in_list ) {
						return false;
					}

					/*Additional city product validation*/
					var allowed_cities_count = 1;

					if ( opt_products.optional_products ) {
						if ( opt_products.optional_products.contains(OPTIONAL_PRODUCT_ADDITIONAL_CITY) ) {
							allowed_cities_count += 3;
						}
					}
					
					if ( hidden_city_ids.length >= allowed_cities_count ) {
						$$('.online-billing-popup-wrapper').getElements('.error')[0].setStyle('display', 'block');
						$$('.online-billing-popup-wrapper').getElements('.error')[0].set('html', Cubix.ChangeCityPopup.one_city_allowed_error/*'You can add only ' + allowed_cities_count + ' city! Please select "Additional city" product for more cities.'*/);
						return false;
					}
					
					/*Additional city product validation*/

					var div = new Element('div').inject(selected_cities_list);

					var remove_city = new Element('span', {
						'class':'remove-city'
					}).inject(div);
					
					var city_name = new Element('span', {
						'class':'city-name',
						'html':selected.get('html')[0]
					}).inject(div);
					
					var hidden = new Element('input', {
						'class':'hidden-city-ids',
						'type':'hidden',
						'value':selected.get('value')[0],
						'name': 'premium_cities[]'
					}).inject(div);
					
					var div_clear = new Element('div', {
						'class':'clear'
					}).inject(div);

					remove_city.addEvent('click', function(e){
						e.stop();

						this.getParent('div').destroy();
						$$('.online-billing-popup-wrapper').getElements('.error')[0].setStyle('display', 'none');
					});
				});
			}			
			
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.ChangeCityPopup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
		}
	}).send();		
	
	return false;
}

Cubix.ChangeCityPopup.Send = function (e) {
	e.stop();
	
	var overlay = new Cubix.Overlay($$('.online-billing-popup-wrapper')[0], { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();

	this.set('send', {
		onSuccess: function (resp) {

			resp = JSON.decode(resp);

			$$('.online-billing-popup-wrapper').getElements('.error')[0].set('html', '');
			$$('.online-billing-popup-wrapper').getElements('.error')[0].setStyle('display', 'none');

			overlay.enable();

			if ( resp.status == 'error' ) {
				$$('.online-billing-popup-wrapper').getElements('.error')[0].setStyle('display', 'block');
				$$('.online-billing-popup-wrapper').getElements('.error')[0].set('html', resp.error);

			}
			else if ( resp.status == 'success' ) {
                $$('.online-billing-popup-wrapper').destroy();
				$$('.overlay').destroy();
				
				Cubix.ShoppingCart.load();
			}
		}.bind(this)
	});

	this.send();
}