Cubix.BlacklistCommentPopup = {};


Cubix.BlacklistCommentPopup.inProcess = false;

Cubix.BlacklistCommentPopup.url = '/bl-members/add-blacklist-comment';
Cubix.BlacklistCommentPopup.Width = 405;
Cubix.BlacklistCommentPopup.Height = 327;

Cubix.BlacklistCommentPopup.Show = function (userId) {
	if ( Cubix.BlacklistCommentPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', 'z_index' : 10001 });
	page_overlay.disable();

	var y_offset = 30;

	var container = new Element('div', { 'class': 'blacklist-popup bl-add-comment'}).setStyles({
		left: window.getWidth() / 2 - Cubix.BlacklistCommentPopup.Width / 2,
		top:  window.getHeight() / 2 - Cubix.BlacklistCommentPopup.Height / 2 + window.getScroll().y - y_offset + 130,
		opacity: 0,
        position: 'absolute',
        'z-index': 10002,
        background: '#fff',
		width: Cubix.BlacklistCommentPopup.Width,
		height: Cubix.BlacklistCommentPopup.Height
	}).inject(document.body);
	
	Cubix.BlacklistCommentPopup.inProcess = true;

	new Request({
		url: Cubix.BlacklistCommentPopup.url + '?user_id=' + userId,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.BlacklistCommentPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'bl-popup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.blacklist-popup').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.BlacklistCommentPopup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
			
			var f_m_d = ' ...';
			
			$$('.textinput, .textareainput').addEvent('click', function() {
				var self = this;
				
				if (self.hasClass('defaultText'))
				{
					self.removeClass('defaultText');
					self.set('value', '');
				}
			});
			
			$$('textareainput').addEvent('blur', function() {
				var self = this;
				
				if (self.get('value').length == 0)
				{
					self.addClass('defaultText');
					
					if (self.hasClass('f-m'))
						self.set('value', f_m_d);
				}
			});
			
			$$('.f-m').addEvent('keyup', function() {
				var self = this;
				
				$('message').set('value', self.get('value'));
			});
		}
	}).send();
	
	return false;
};

Cubix.BlacklistCommentPopup.Send = function (e) {
	e.stop();
	var overlay = new Cubix.Overlay($$('.blacklist-popup')[0], { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();
	function getErrorElement(el) {
		var error = el.getPrevious('.error-profile');
		if ( error ) return error;
		var target = el;
		return new Element('div', { 'class': 'error-profile' }).inject(target, 'before');
	}
	this.set('send', {
		onSuccess: function (resp) {
			this.getElements('.error-profile').destroy();
			resp = JSON.decode(resp);
			overlay.enable();
			
			$$('.blacklist-popup .invalid').removeClass('invalid');
			if ( resp.status == 'error' ) {
				var c = false;
				for ( var field in resp.msgs ) {
					var input = this.getElement('*[name=' + field + ']');
					input.addClass('invalid');
					if ( field == 'captcha' ) {
						c = true;
					}
				}
				
				var img = this.getElement('img'),
					src = img.get('src');
				
				src = src.substr(0, src.indexOf('?'));
				src = src + '?' + Math.random();
				
				img.set('src', src);
				$('f-captcha').set('value', '');
			}
			else if ( resp.status == 'success' ) {
				this.getParent().set('html', resp.msg);
			}
		}.bind(this)
	});

	this.send();
};


Cubix.BlacklistMembersPopup = {};


Cubix.BlacklistMembersPopup.inProcess = false;

Cubix.BlacklistMembersPopup.url = '/bl-members/get-blacklisted-members';
Cubix.BlacklistMembersPopup.Width = 750;

Cubix.BlacklistMembersPopup.container;
Cubix.BlacklistMembersPopup.search_input_text = 'Search by username';

Cubix.BlacklistMembersPopup.Show = function () {
	if ( $$('.bl-members').length ) return;
	
	if ( Cubix.BlacklistMembersPopup.inProcess ) return false;

	var y_offset = 150;

	
	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', 'z_index' : 10001 });
	page_overlay.disable();

	var container = new Element('div', { 'class': 'blacklist-popup bl-members'}).setStyles({
		left: window.getWidth() / 2 - Cubix.BlacklistMembersPopup.Width / 2,
		top:  window.getHeight() / 2 + window.getScroll().y - y_offset,
		opacity: 0,
		position: 'absolute',
		'z-index': 10002,
		background: '#fff',
		width: Cubix.BlacklistMembersPopup.Width
		//height: Cubix.BlacklistMembersPopup.Height
	}).inject(document.body);
	
	new Element('div', {
		'class' : 'content'
	}).inject(container);
	
	var close_btn = new Element('div', {
		html: '',
		'class': 'bl-popup-close-btn-v2'
	}).inject(container);
	
	close_btn.addEvent('click', function() {
		$$('.bl-members').destroy();
		page_overlay.enable();
	});
	
	Cubix.BlacklistMembersPopup.container = container;
	Cubix.BlacklistMembersPopup.Load(false, 1);
	
	return false;
};

Cubix.BlacklistMembersPopup.Load = function (username, page) {
	var ov = new Cubix.Overlay(Cubix.BlacklistMembersPopup.container.getElement('.content'), {has_loader: true, oapcity: 0.5, color: '#000'});
	ov.disable();
	
	var url = Cubix.BlacklistMembersPopup.url;
	if ( username && username.length ) {
		url += '?username=' + username;
	} else {
		url += '?page=' + page;
	}
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {
			Cubix.BlacklistMembersPopup.container.getElement('.content').set('html', resp);
			Cubix.BlacklistMembersPopup.container.tween('opacity', '1');
			
			Cubix.BlacklistMembersPopup.InitSearchInput($('bl-search'));
			
		}
	}).send();
}

Cubix.BlacklistMembersPopup.InitSearchInput = function (input) {
	Cubix.BlacklistMembersPopup.input = input;
	this.timer = null;
	
	Cubix.BlacklistMembersPopup.input.addEvent('focus', function(){
		if ( this.get('value') == Cubix.BlacklistMembersPopup.search_input_text ) {
			this.set('value', '');
			this.removeClass('def-text');
		}	
	});
	Cubix.BlacklistMembersPopup.input.addEvent('blur', function(){
		if ( ! this.get('value').length ) {
			this.set('value', Cubix.BlacklistMembersPopup.search_input_text);
			this.addClass('def-text');
		}
	});
	
	Cubix.BlacklistMembersPopup.input.addEvents({
		keyup: function () {
			$clear(this.timer);
			this.timer = setTimeout('Cubix.BlacklistMembersPopup.InitSearchInput.KeyUp(Cubix.BlacklistMembersPopup.input)', 500);
		}.bind(this)
	});
}

Cubix.BlacklistMembersPopup.InitSearchInput.KeyUp = function (input) {
	
	
	var value = ( input.get('value').length && input.get('value') != Cubix.BlacklistMembersPopup.search_input_text ) ? input.get('value') : null;
	
	Cubix.BlacklistMembersPopup.Load(value, 1);
}

Cubix.BlacklistMemberCommentsPopup = {};


Cubix.BlacklistMemberCommentsPopup.inProcess = false;

Cubix.BlacklistMemberCommentsPopup.url = '/bl-members/get-blacklisted-member-comments';
Cubix.BlacklistMemberCommentsPopup.Width = 750;

Cubix.BlacklistMemberCommentsPopup.container;

Cubix.BlacklistMemberCommentsPopup.Show = function (userId) {
	if ( $$('.bl-member-comments').length ) return;
	
	if ( Cubix.BlacklistMemberCommentsPopup.inProcess ) return false;

	var y_offset = 150;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', 'z_index' : 10002 });
	page_overlay.disable();

	var container = new Element('div', { 'class': 'blacklist-popup bl-member-comments'}).setStyles({
		left: window.getWidth() / 2 - Cubix.BlacklistMemberCommentsPopup.Width / 2,
		top:  window.getHeight() / 2 + window.getScroll().y - y_offset,
		opacity: 0,
		position: 'absolute',
		'z-index': 10002,
		background: '#fff',
		width: Cubix.BlacklistMemberCommentsPopup.Width
	}).inject(document.body);
	
	new Element('div', {
		'class' : 'content'
	}).inject(container);
	
	var close_btn = new Element('div', {
		html: '',
		'class': 'bl-popup-close-btn-v2'
	}).inject(container);
	
	close_btn.addEvent('click', function() {
		$$('.bl-member-comments').destroy();
		page_overlay.enable();
	});
	
	Cubix.BlacklistMemberCommentsPopup.container = container;
	Cubix.BlacklistMemberCommentsPopup.Load(userId, 1);
	
	return false;
};

Cubix.BlacklistMemberCommentsPopup.Load = function (userId, page) {
	var ov = new Cubix.Overlay(Cubix.BlacklistMemberCommentsPopup.container.getElement('.content'), {has_loader: true, oapcity: 0.5, color: '#000'});
	ov.disable();
	
	var url = Cubix.BlacklistMemberCommentsPopup.url + '?user_id=' + userId + ' &page=' + page;
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {
			Cubix.BlacklistMemberCommentsPopup.container.getElement('.content').set('html', resp);
			Cubix.BlacklistMemberCommentsPopup.container.tween('opacity', '1');
		}
	}).send();
}