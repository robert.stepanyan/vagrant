/* --> Feedback */
Cubix.SaPopup = {};


Cubix.SaPopup.inProcess = false;

Cubix.SaPopup.url = '';

Cubix.SaPopup.Show = function (box_height, box_width) {
	if ( Cubix.SaPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();

	var y_offset = 150;

	var container = new Element('div', { 'class': 'SaPopup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0
	}).inject(document.body);
	
	Cubix.SaPopup.inProcess = true;

	new Request({
		url: Cubix.SaPopup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.SaPopup.inProcess = false;
			container.set('html', resp);

			container.getElements('.btn_cancel').addEvent('click', function(e){
				e.stop();
				
				$$('.SaPopup-wrapper').destroy();
				page_overlay.enable();
			});
			
			container.getElements('.btn_ok').addEvent('click', function(e){
				e.stop();
				
				$('psignup-form').submit();
			});

			var close_btn = new Element('div', {
				html: '',
				'class': 'SaPopup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.SaPopup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
		}
	}).send();
		
	
	return false;
}