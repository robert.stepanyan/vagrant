window.addEvent('domready',function() {

	if ( ! Cubix.Escorts ) return;

	$$('.big-red-btn-wrapper').hide();
	
	var finish = false, page = 1, req = new Request({
		method: 'get',
		url: Cubix.Escorts.GetRequestUrl('').replace(/^\/escorts/, '/').replace(/^\/\//, '/') + '&main_premium_spot=1',
		data: {page: 1},
		onFailure: function() {
			$$('.spinner').destroy();
		},
		onSuccess: function (resp) {
			$$('.spinner').destroy();
			ss.options.min = window.getScrollSize().y - window.getSize().y - 150;
			var target = $$('.escorts')[0];
			target.getElements('.spinner').destroy();
			
			var rows = new Element('div', {html: resp}).getElements('.row');
			if ( rows.length == 0 ) {finish = true;$$('.big-red-btn-wrapper').show();}
			rows.each(function (row) {
				row.inject(target);
			});

			Cubix.PhotoRoller.Init();
			Cubix.EscortHover.Init();
			Cubix.Escorts.ComRevIcons();
			$$(".wrap .diamond ,.wrap .premium, .wrap .p100s, .wrap .new").addEvent("click", function(event) {
				var sib = this.getNext(["a"]);
				var link = sib.getProperty("href");
				window.location = link;
			});
		}
	});

	window.addEvent('escortsFilterChange', function () {page = 1;finish = false});

	var ss = new ScrollSpy({
		min: window.getScrollSize().y - window.getSize().y - 2000,
		onScroll: function (p) {
			if ( req.running || finish ) return;
			if ( p.y > window.getScrollSize().y - window.getSize().y - 2000 ) {
				if ( $$('.escorts')[0].getElements('.row').length > 0 ) {
					var hash = document.location.hash.substring(1);
					var cityReg = /city_[a-z]{2}_([a-z-]+)/i;
					var cityInfo = Cubix.Escorts.GetRequestUrl('').match(cityReg);
					var url = "";
					if(cityInfo){
						url = Cubix.Escorts.GetRequestUrl('').replace(/^\/\//, '/') + '&main_premium_spot=1';
					}
					else{
						url = Cubix.Escorts.GetRequestUrl(hash).replace(/^(\/en)?\/escorts/, '/').replace(/^\/\//, '/') + '&main_premium_spot=1';
					}
					req.options.url = url
					req.options.data.page = ++page;

					if ( ! $$('.big-red-btn-wrapper') ) return;

					$$('.escorts')[0].getElements('.row.last').removeClass('last');
					getSpinner().inject($$('.escorts')[0]);
					req.send();
					
				}
			}
		},
		container: window
	});

	var getSpinner = function () {
		
		var el = new Element('div', {'class': 'spinner'});
		new Element('span', {html: 'Loading more escorts...'}).inject(el);
		return el;
	};
});
