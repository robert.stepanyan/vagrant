window.addEvent('domready', function(){
	
	var username = $$('#login-box-v2 input[name=username]')[0];
	var password = $$('#login-box-v2 input[name=password]')[0];
	
	if ( $$('#login-box-v2 .lb-login-btn')[0] ) {
		$$('#login-box-v2 .lb-login-btn').addEvent('click', function(e){
			e.stop();
			
			var popup = this.getNext('div.lb-popup');
			
			popup.toggleClass('none');
			
			if ( ! popup.hasClass('none') ) {
				username.focus();
			}
		});
	}

	if ( username && password ) {

		username.addEvent('focus', function(){
			if ( this.get('value') == headerVars.tUsername ) {
				this.set('value', '');
			}			
		});
		username.addEvent('blur', function(){
			if ( ! this.get('value').length ) {
				this.set('value', headerVars.tUsername);
			}
		});		

		var initPassword = function(pass) {
			pass.addEvent('focus', function() {
				this.destroy();
				injectNewPassword();
			});
		};

		var injectNewPassword = function() {
			var pass = new Element('input', {
				name: 'password',
				type: 'password',
				'class': 'lb-password',
				value: ''
			}).inject(username, 'after');

			pass.addEvent('blur', function() {
				if ( ! this.get('value').length ) {
					this.destroy();
					var pass = new Element('input', {
						name: 'password',
						type: 'text',
						'class': 'lb-password',
						value: headerVars.tPassword
					});
					pass.inject(username, 'after');
					initPassword(pass);
				}
			});

			pass.focus();
			pass.focus();
			pass.focus();
		};

		initPassword(password);
	}
	
	var mobile_popup = $$('.mobile_popup')[0];
	
	$$('.mobile_banner').addEvent('click', function(e) {
		e.stop();
		
		if ( mobile_popup.hasClass('none') ) {
			mobile_popup.removeClass('none');
		}
		else {
			mobile_popup.addClass('none')
		}
	});

	$$('.mobile_popup_x_btn').addEvent('click', function(e) {
		e.stop();
		mobile_popup.addClass('none')
	});


	// XMAS CALENDAR -------------------->
	
	if($('xmas-calendar-icon')){
		
		var xmas_popup = $('xmas-calendar-popup');
		var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
		$('xmas-calendar-icon').addEvent('click', function(e) {
			e.stop();

			if ( xmas_popup.hasClass('none') ) {
				page_overlay.disable();
				xmas_popup.removeClass('none');
			}
			else {
				page_overlay.enable();
				xmas_popup.addClass('none');
			}
		});
		$$('.xmas-popup_x_btn, #xmas-calendar-popup .cancel').addEvent('click', function(e) {
			e.stop();
			page_overlay.enable();
			xmas_popup.addClass('none');
		});

		var xmasUsername = $$('#xmas-calendar-popup input[name=username]')[0];
		var xmasPassword = $$('#xmas-calendar-popup input[name=password]')[0];
		if ( xmasUsername && xmasPassword ) {

			xmasUsername.addEvent('focus', function(){
				if ( this.get('value') == headerVars.tUsername ) {
					this.set('value', '');
				}			
			});
			xmasUsername.addEvent('blur', function(){
				if ( ! this.get('value').length ) {
					this.set('value', headerVars.tUsername);
				}
			});		

			var initXmasPassword = function(pass) {
				pass.addEvent('focus', function() {
					this.destroy();
					injectXmasPassword();
				});
			};

			var injectXmasPassword = function() {
				var pass = new Element('input', {
					name: 'password',
					type: 'password',
					'class': 'password',
					value: ''
				}).inject(xmasUsername, 'after');

				pass.addEvent('blur', function() {
					if ( ! this.get('value').length ) {
						this.destroy();
						var pass = new Element('input', {
							name: 'password',
							type: 'text',
							'class': 'password',
							value: headerVars.tPassword
						});
						pass.inject(xmasUsername, 'after');
						initXmasPassword(pass);
					}
				});

				pass.focus();
				pass.focus();
				pass.focus();
			};

			initXmasPassword(xmasPassword);
		}
	}
	
	/*if ( ! headerVars.currentUser ) {
		$('fchat').addEvent('click', function(e){
			e.stop();
			Cubix.Popup.Show('500', '520');
		});
	} else {
		if ( headerVars.currentUser.user_type == 'escort' && ! headerVars.packageQueue ) {
			$('fchat').addEvent('click', function(e){
				e.stop();
				Cubix.ChatPopup.Show('100', '100', this, 110, 130);
			});
		} else {
			$('fchat').addEvent('click', function(e){
				e.stop();
				if ( headerVars.sessionId ) {
					window.open('http://chat.6annonce.com/chat7/chat.php?hash=' + headerVars.sessionId , '6annonce.com', 'height=620, width=820');
				}
			});
		}
	}*/
});