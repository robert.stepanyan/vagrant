/* --> Favorites */
Cubix.Pinboard = {};

Cubix.Pinboard.containerMain = 'posts';
Cubix.Pinboard.scrollWrap = 'pin-wrap';
Cubix.Pinboard.url = '/private/ajax-pinboard';
Cubix.Pinboard.myPostsCountUrl = '/private/ajax-pinboard-my-posts-count';
Cubix.Pinboard.myRepliesCountUrl = '/private/ajax-pinboard-my-replies-count';
Cubix.Pinboard.myPostsRepliesCountUrl = '/private/ajax-pinboard-my-posts-replies-count';
Cubix.Pinboard.myPostsUrl = '/private/ajax-pinboard-my-posts';
Cubix.Pinboard.myRepliesUrl = '/private/ajax-pinboard-my-replies';
Cubix.Pinboard.myRepliesFilterUrl = '/private/ajax-pinboard-my-replies-filter';
Cubix.Pinboard.myPostsRepliesUrl = '/private/ajax-pinboard-my-posts-replies';
Cubix.Pinboard.closePostUrl = '/private/ajax-pinboard-close-post';
Cubix.Pinboard.openPostUrl = '/private/ajax-pinboard-open-post';
Cubix.Pinboard.addPostUrl = '/private/ajax-pinboard-add-post';
Cubix.Pinboard.editPostUrl = '/private/ajax-pinboard-edit-post';
Cubix.Pinboard.addReplyUrl = '/private/ajax-pinboard-add-reply';
Cubix.Pinboard.allowScroll = true;

Cubix.Pinboard.Init = function () {
	Cubix.Pinboard.Show({page: 1}, false);
	Cubix.Pinboard.MyPostsCount();
	Cubix.Pinboard.MyRepliesCount();
	Cubix.Pinboard.MyPostsRepliesCount();
	
	return false;
};

Cubix.Pinboard.Show = function(data, allowScroll) {	
	var container = Cubix.Pinboard.containerMain;
		
	if ($('search_text').get('value'))
		data.search_text = $('search_text').get('value');
		
	var myScroll = new Fx.Scroll(window);
	var overlay = new Cubix.Overlay($(container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Pinboard.url,
		method: 'get',
		data: data,
		onSuccess: function (resp) {			
			$(container).set('html', resp);
			var first = 1 + ($('h-page').get('value') - 1) * $('h-per-page').get('value');
			var second = $('h-page').get('value') * $('h-per-page').get('value');
			var third = $('h-count').get('value');
			if (second > third) second = third;
			$('showing').set('html', first + '-' + second + ' ' + Cubix.Pinboard.of_text + ' ' + third);
			overlay.enable();
			
			if (allowScroll) {
				myScroll.toElement($(Cubix.Pinboard.scrollWrap));
			}
		}
	}).send();
	
	return false;
};

Cubix.Pinboard.MyRepliesReply = function(post_id) {	
	var h = '<div style="margin-top: 10px"><div id="err-reply-' + post_id + '" class="red"></div><textarea name="tx-reply" id="r-' + post_id + '" style="width: 512px;"></textarea></div><div class="post-acts"><a href="#" class="post-send" onclick="return Cubix.Pinboard.Send(' + post_id + ');">' + Cubix.Pinboard.send_text + '</a><a href="#" class="post-cancel" onclick="return Cubix.Pinboard.Cancel(' + post_id + ');">' + Cubix.Pinboard.cancel_text + '</a><div class="clear"></div></div><div class="clear"></div>';
	
	$('reply-wrap-' + post_id).set('html', h);
	
	if ( typeof(tinyMCE) !== 'undefined' ) {
		tinyMCE.init({
			mode : "exact",
			elements: "r-" + post_id,
			theme : "advanced",

			onchange_callback : function(inst){
				container.getElement('#r-' + post_id).set('value', inst.getContent());
			},

			plugins: "emotions,advhr,preview,paste",
			theme_advanced_toolbar_location	  : "top",
			theme_advanced_toolbar_align	  : "left",
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,undo,redo,|,pastetext,pasteword,fullscreen,emotions,preview",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3_add : "",
			remove_script_host : false,
			relative_urls : false,
			convert_urls : false
		});
	}
	
	return false;
};

Cubix.Pinboard.Cancel = function(post_id) {	
	$('reply-wrap-' + post_id).set('html', '');
	
	return false;
};

Cubix.Pinboard.Send = function(post_id) {	
	var data = {
		post_id: post_id,
		reply: $('r-' + post_id).get('value')
	};

	new Request({
		url: Cubix.Pinboard.addReplyUrl,
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			resp = JSON.decode(resp);
			var status = resp.status;

			if (status == 'ok')
			{
				$('reply-wrap-' + post_id).set('html', '');
				
				Cubix.Pinboard.MyRepliesCount();
				Cubix.Pinboard.MyPostsRepliesCount();
				
				Cubix.Pinboard.SeeMyPostsReplies(post_id, 1);
			}
			else if (status == 'error')
			{
				$('err-reply-' + post_id).set('html', '');

				var data = resp.data;

				if (data.reply)
				{
					$('err-reply-' + post_id).set('html', data.reply);
				}
			}
		}
	}).send();
		
	return false;
};

Cubix.Pinboard.SeeReplies = function(post_id, page) {
	var overlay = new Cubix.Overlay($('mrp'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: '/private/ajax-pinboard-see-replies?p=' + post_id + '&page=' + page,
		method: 'get',
		onSuccess: function (resp) {			
			$('see-replies-wrap-' + post_id).set('html', resp);
			
			overlay.enable();
		}
	}).send();
	
	return false;
};

Cubix.Pinboard.SeeMyPostsReplies = function(post_id, page) {
	var overlay = new Cubix.Overlay($('mrp'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: '/private/ajax-pinboard-see-my-posts-replies?p=' + post_id + '&page=' + page,
		method: 'get',
		onSuccess: function (resp) {			
			$('see-replies-wrap-' + post_id).set('html', resp);
			
			overlay.enable();
		}
	}).send();
	
	return false;
};

Cubix.Pinboard.MyPostsCount = function() {
	var overlay = new Cubix.Overlay($('menu'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Pinboard.myPostsCountUrl,
		method: 'get',
		onSuccess: function (resp) {			
			$('my-posts-count').set('html', resp);
			
			overlay.enable();
		}
	}).send();
};

Cubix.Pinboard.MyRepliesCount = function() {
	var overlay = new Cubix.Overlay($('menu'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Pinboard.myRepliesCountUrl,
		method: 'get',
		onSuccess: function (resp) {			
			$('my-replies-count').set('html', resp);
			
			overlay.enable();
		}
	}).send();
};

Cubix.Pinboard.MyPostsRepliesCount = function() {
	var overlay = new Cubix.Overlay($('menu'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Pinboard.myPostsRepliesCountUrl,
		method: 'get',
		onSuccess: function (resp) {			
			$('my-posts-replies-count').set('html', resp);
			
			overlay.enable();
		}
	}).send();
};

Cubix.Pinboard.MyPosts = function() {
	closeAllPopups();
	
	var overlay = new Cubix.Overlay($('menu'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Pinboard.myPostsUrl,
		method: 'get',
		onSuccess: function (resp) {			
			$('posts-wrap').set('html', resp);
			
			overlay.enable();
		}
	}).send();
	
	return false;
};

Cubix.Pinboard.MyReplies = function() {
	closeAllPopups();
	
	var overlay = new Cubix.Overlay($('menu'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Pinboard.myRepliesUrl,
		method: 'get',
		onSuccess: function (resp) {			
			$('posts-wrap').set('html', resp);
			
			overlay.enable();
			
			var overlay2 = new Cubix.Overlay($('container'), {
				has_loader: false,
				position: '50%',
				offset: {
					left: 0,
					top: -1
				},
				color: '#000',
				opacity: .38
			});

			overlay2.disable();
			
			var date_from = new DatePicker($('f-date-f'), {
				pickerClass: 'datepicker',
				positionOffset: { x: -10, y: 0 },
				allowEmpty: true,
				format: 'j M Y'
			});
			
			var date_to = new DatePicker($('f-date-t'), {
				pickerClass: 'datepicker',
				positionOffset: { x: -10, y: 0 },
				allowEmpty: true,
				format: 'j M Y'
			});
		}
	}).send();
	
	return false;
};

Cubix.Pinboard.MyRepliesFilter = function() {
	var data = {};
	
	if ($('f-text').get('value'))
		data.search_text = $('f-text').get('value');
	
	if ($('f-date-f').get('value'))
		data.date_from = $('f-date-f').get('value');
	
	if ($('f-date-t').get('value'))
		data.date_to = $('f-date-t').get('value');
	
	if ($('f-post-id').get('value'))
		data.post_id = $('f-post-id').get('value');
	
	if ($('f-show').get('value'))
		data.show_order = $('f-show').get('value');
	
	/*if ($('f-newest').get('checked'))
		data.newest = 1;*/
	
	var overlay = new Cubix.Overlay($('mrp'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Pinboard.myRepliesFilterUrl,
		method: 'get',
		data: data,
		onSuccess: function (resp) {			
			$('posts-body').set('html', resp);
			
			overlay.enable();
		}
	}).send();
	
	return false;
};

Cubix.Pinboard.MyPostsReplies = function() {
	closeAllPopups();
	
	var overlay = new Cubix.Overlay($('menu'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Pinboard.myPostsRepliesUrl,
		method: 'get',
		onSuccess: function (resp) {			
			$('posts-wrap').set('html', resp);
			
			overlay.enable();
			
			var overlay2 = new Cubix.Overlay($('container'), {
				has_loader: false,
				position: '50%',
				offset: {
					left: 0,
					top: -1
				},
				color: '#000',
				opacity: .38
			});

			overlay2.disable();
			
			var date_from = new DatePicker($('f-date-f'), {
				pickerClass: 'datepicker',
				positionOffset: { x: -10, y: 0 },
				allowEmpty: true,
				format: 'j M Y'
			});
			
			var date_to = new DatePicker($('f-date-t'), {
				pickerClass: 'datepicker',
				positionOffset: { x: -10, y: 0 },
				allowEmpty: true,
				format: 'j M Y'
			});
		}
	}).send();
	
	return false;
};

Cubix.Pinboard.MyPostsRepliesFilter = function() {
	var data = {};
	
	if ($('f-text').get('value'))
		data.search_text = $('f-text').get('value');
	
	if ($('f-date-f').get('value'))
		data.date_from = $('f-date-f').get('value');
	
	if ($('f-date-t').get('value'))
		data.date_to = $('f-date-t').get('value');
	
	if ($('f-post-id').get('value'))
		data.post_id = $('f-post-id').get('value');
	
	if ($('f-show').get('value'))
		data.show_order = $('f-show').get('value');
	
	/*if ($('f-newest').get('checked'))
		data.newest = 1;*/
	
	var overlay = new Cubix.Overlay($('mrp'), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: '/private/ajax-pinboard-my-posts-replies-filter',
		method: 'get',
		data: data,
		onSuccess: function (resp) {			
			$('posts-body').set('html', resp);
			
			overlay.enable();
		}
	}).send();
	
	return false;
};

Cubix.Pinboard.ClosePopup = function(key) {
	if (key == 'my-posts')
		$$('.my-posts-popup').destroy();
	else if (key == 'my-replies')
	{
		$$('.my-replies-popup').destroy();
		$$('.overlay').destroy();
	}
	else if (key == 'add-post')
	{
		$$('.add-post-popup').destroy();
		$$('.overlay').destroy();
	}
	else if (key == 'add-reply')
	{
		$$('.add-reply-popup').destroy();
		$$('.overlay').destroy();
	}
	
	return false;
};

Cubix.Pinboard.ClosePost = function(post_id) {
	if (confirm(Cubix.Pinboard.close_question))
	{
		var data = {
			post_id: post_id
		};
		
		new Request({
			url: Cubix.Pinboard.closePostUrl,
			method: 'post',
			data: data,
			onSuccess: function (resp) {
				Cubix.Pinboard.Show({page: 1}, false);
				Cubix.Pinboard.MyPostsCount();
				Cubix.Pinboard.MyPostsRepliesCount();
				Cubix.Pinboard.MyPosts();
			}
		}).send();
	}
	
	return false;
};

Cubix.Pinboard.OpenPost = function(post_id) {
	if (confirm(Cubix.Pinboard.open_question))
	{
		var data = {
			post_id: post_id
		};
		
		new Request({
			url: Cubix.Pinboard.openPostUrl,
			method: 'post',
			data: data,
			onSuccess: function (resp) {
				Cubix.Pinboard.Show({page: 1}, false);
				Cubix.Pinboard.MyPostsCount();
				Cubix.Pinboard.MyPostsRepliesCount();
				Cubix.Pinboard.MyPosts();
			}
		}).send();
	}
	
	return false;
};

Cubix.Pinboard.EditPost = function(post_id) {
	var overlay = new Cubix.Overlay($('container'), {
		has_loader: false,
		position: '50%',
		offset: {
			left: 0,
			top: -1
		},
		color: '#000',
		opacity: .38
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Pinboard.editPostUrl,
		method: 'get',
		data: {
			post_id: post_id
		},
		onSuccess: function (resp) {
			$('add-post-wrap').set('html', resp);
			
			if ( typeof(tinyMCE) !== 'undefined' ) {
				tinyMCE.init({
					mode : "exact",
					elements: "ap-post",
					theme : "advanced",
					
					onchange_callback : function(inst){
						container.getElement('#ap-post').set('value', inst.getContent());
					},

					plugins: "emotions,advhr,preview,paste",
					theme_advanced_toolbar_location	  : "top",
					theme_advanced_toolbar_align	  : "left",
					editor_selector : 'about-field',
					theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,undo,redo,|,pastetext,pasteword,fullscreen,emotions,preview",
					theme_advanced_buttons2 : "",
					theme_advanced_buttons3_add : "",
					remove_script_host : false,
					relative_urls : false,
					convert_urls : false
				});
			}
			
			$('add-post-btn').addEvent('click', function(e) {
				e.stop();
				
				var alert = $('alert').get('checked');
				var al_val = 0;
				
				if (alert) al_val = 1;
								
				var data = {
					post_id: $('ap-id').get('value'),
					title: $('ap-title').get('value'),
					post: $('ap-post').get('value'),
					vis: $$('input[name=vis]:checked').get('value'),
					alert: al_val
				};
				
				new Request({
					url: Cubix.Pinboard.editPostUrl,
					method: 'post',
					data: data,
					onSuccess: function (resp) {
						resp = JSON.decode(resp);
						var status = resp.status;
						
						if (status == 'ok')
						{
							$$('.add-post-popup').destroy();							
							overlay.enable();
							Cubix.Pinboard.MyPosts();
							Cubix.Pinboard.Show();
						}
						else if (status == 'error')
						{
							$('err-title').set('html', '');
							$('err-post').set('html', '');
							
							var data = resp.data;
														
							if (data.title)
							{
								$('err-title').set('html', data.title);
							}
							
							if (data.post)
							{
								$('err-post').set('html', data.post);
							}
						}
					}
				}).send();
			});
		}
	}).send();
	
	return false;
};

Cubix.Pinboard.AddPost = function() {
	closeAllPopups();
	
	var overlay = new Cubix.Overlay($('container'), {
		has_loader: false,
		position: '50%',
		offset: {
			left: 0,
			top: -1
		},
		color: '#000',
		opacity: .38
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Pinboard.addPostUrl,
		method: 'get',
		onSuccess: function (resp) {			
			$('add-post-wrap').set('html', resp);
			
			if ( typeof(tinyMCE) !== 'undefined' ) {
				tinyMCE.init({
					mode : "exact",
					elements: "ap-post",
					theme : "advanced",
					
					onchange_callback : function(inst){
						container.getElement('#ap-post').set('value', inst.getContent());
					},

					plugins: "emotions,advhr,preview,paste",
					theme_advanced_toolbar_location	  : "top",
					theme_advanced_toolbar_align	  : "left",
					editor_selector : 'about-field',
					theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,undo,redo,|,pastetext,pasteword,fullscreen,emotions,preview",
					theme_advanced_buttons2 : "",
					theme_advanced_buttons3_add : "",
					remove_script_host : false,
					relative_urls : false,
					convert_urls : false
				});
			}
			
			$('add-post-btn').addEvent('click', function(e) {
				e.stop();
				
				var alert = $('alert').get('checked');
				var al_val = 0;
				
				if (alert) al_val = 1;
				
				var data = {
					title: $('ap-title').get('value'),
					post: $('ap-post').get('value'),
					vis: $$('input[name=vis]:checked').get('value'),
					alert: al_val
				};
				
				new Request({
					url: Cubix.Pinboard.addPostUrl,
					method: 'post',
					data: data,
					onSuccess: function (resp) {
						resp = JSON.decode(resp);
						var status = resp.status;
						
						if (status == 'ok')
						{
							$$('.add-post-popup').destroy();							
							overlay.enable();
							Cubix.Pinboard.MyPostsCount();
						}
						else if (status == 'error')
						{
							$('err-title').set('html', '');
							$('err-post').set('html', '');
							
							var data = resp.data;
														
							if (data.title)
							{
								$('err-title').set('html', data.title);
							}
							
							if (data.post)
							{
								$('err-post').set('html', data.post);
							}
						}
					}
				}).send();
			});
		}
	}).send();
	
	return false;
};

Cubix.Pinboard.AddReply = function(post_id) {
	closeAllPopups();
	
	var overlay = new Cubix.Overlay($('container'), {
		has_loader: false,
		position: '50%',
		offset: {
			left: 0,
			top: -1
		},
		color: '#000',
		opacity: .38
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Pinboard.addReplyUrl,
		method: 'get',
		onSuccess: function (resp) {			
			$('add-reply-wrap-' + post_id).set('html', resp);
			
			if ( typeof(tinyMCE) !== 'undefined' ) {
				tinyMCE.init({
					mode : "exact",
					elements: "ar-reply",
					theme : "advanced",
					
					onchange_callback : function(inst){
						container.getElement('#ar-reply').set('value', inst.getContent());
					},

					plugins: "emotions,advhr,preview,paste",
					theme_advanced_toolbar_location	  : "top",
					theme_advanced_toolbar_align	  : "left",
					editor_selector : 'about-field',
					theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,undo,redo,|,pastetext,pasteword,fullscreen,emotions,preview",
					theme_advanced_buttons2 : "",
					theme_advanced_buttons3_add : "",
					remove_script_host : false,
					relative_urls : false,
					convert_urls : false
				});
			}
			
			$('add-reply-btn').addEvent('click', function(e) {
				e.stop();
								
				var data = {
					post_id: post_id,
					reply: $('ar-reply').get('value')
				};
				
				new Request({
					url: Cubix.Pinboard.addReplyUrl,
					method: 'post',
					data: data,
					onSuccess: function (resp) {
						resp = JSON.decode(resp);
						var status = resp.status;
						
						if (status == 'ok')
						{
							$$('.add-reply-popup').destroy();							
							overlay.enable();
							Cubix.Pinboard.MyRepliesCount();
							Cubix.Pinboard.MyPostsRepliesCount();
						}
						else if (status == 'error')
						{
							$('err-reply').set('html', '');
							
							var data = resp.data;
																					
							if (data.reply)
							{
								$('err-reply').set('html', data.reply);
							}
						}
					}
				}).send();
			});
		}
	}).send();
	
	return false;
};

function closeAllPopups()
{
	if ($defined($$('.my-posts-popup')))
	{
		$$('.my-posts-popup').destroy();
	}
	
	if ($defined($$('.my-replies-popup')))
	{
		$$('.my-replies-popup').destroy();
	}
	
	if ($defined($$('.add-post-popup')))
	{
		$$('.add-post-popup').destroy();
	}
	
	if ($defined($$('.add-reply-popup')))
	{
		$$('.add-reply-popup').destroy();
	}
}