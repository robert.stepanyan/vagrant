/* --> Feedback */
Cubix.ExtendPopup = {};


Cubix.ExtendPopup.inProcess = false;
Cubix.ExtendPopup.url = '';
Cubix.ExtendPopup.epg_url = '';

Cubix.ExtendPopup.Show = function (box_width, box_height, escort_id, package_id, opid) {
	if ( Cubix.ExtendPopup.inProcess ) return false;

	/*var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', z_index: 200 });
	page_overlay.disable();*/

	var y_offset = 0;
	var x_offset = 60;

	var container = new Element('div', { 'class': 'extend-popup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2 - x_offset,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0
	}).inject(document.body);
	
	Cubix.ExtendPopup.inProcess = true;
	new Request({
		url: Cubix.ExtendPopup.url + '?escort_id=' + escort_id + '&package_id=' + package_id + '&opid=' + opid,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.ExtendPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'extend-popup-close'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.extend-popup-wrapper').destroy();
				//page_overlay.enable();
			});

			container.tween('opacity', '1');
			
			container.getElements('a.cancel').addEvent('click', function(e){
				e.stop();
				
				$$('.extend-popup-wrapper').destroy();
				//page_overlay.enable();
			});
			
			var price = container.getElements('span.p')[0];
			container.getElements('select[name=extension]').addEvent('change', function(){
				price.set('html', this.getSelected().get('rel'));
			});
			container.getElements('select[name=extension]').fireEvent('change');
			
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.ExtendPopup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
		}
	}).send();
		
	
	return false;
}

Cubix.ExtendPopup.Send = function (e) {
	e.stop();
	
	var overlay = new Cubix.Overlay($$('.extend-popup-wrapper')[0], { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();

	this.get('send').removeEvents('success');
	this.set('send', {
		onSuccess: function (resp) {
			
			overlay.enable();
			
			var container = new Element('div', { 'class': 'extend-popup-cont'}).setStyles({
				opacity: 0,
				'display' : 'none'
			}).inject(document.body);
			container.set('html', resp);
			
			var page_overlay = new Cubix.Overlay($('page'), { loader: _st('loader-small.gif'), position: '50%', no_relative: true, opacity: 0.5, color: '#fff', z_index: 200 });
			page_overlay.disable();
			
			if ( container.getElements('input[name=url]')[0] ) {
				window.location = container.getElements('input[name=url]')[0].get('value');
			}
			
			$$('.extend-popup-wrapper')[0].destroy();
			
		}.bind(this)
	});
	
	this.send();
}