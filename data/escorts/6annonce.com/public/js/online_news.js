var onlineNewstimoutId;
window.addEvent('domready', function(){
	
	var boxText = $("online-news-text");
	var boxHeight = boxText.getHeight() +'px';
	
	boxText.setStyle('maxHeight','96px');
	
	$('online-news').fade('in');
	onlieNewstimoutId = setTimeout(closeOnlineNews, 60000);
	$$('#online-news .close').addEvent('click',closeOnlineNews);
	$$('#online-news .show-all').addEvent('click',function(){
		var self = this
		var effect = new Fx.Tween(boxText,  {
							duration: 1000,
							transition:  Fx.Transitions.Quart.easeInOut,
							onComplete: function(){
								self.fade('out');
							}
							
			});
			
			effect.start('maxHeight', boxHeight);
			return false;
	})
});

var closeOnlineNews = function(){
	$('online-news').fade('out');
	clearTimeout(onlineNewstimoutId);
	Cookie.write('closed_online_news',1,{duration: 1});
	return false;
}