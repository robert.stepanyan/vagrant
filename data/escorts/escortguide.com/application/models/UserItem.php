<?php

class Model_UserItem extends Cubix_Model_Item
{
	/**
	 *
	 * @var Cubix_Api
	 */
	protected $client;

	public function __construct($rowData)
	{
		parent::__construct($rowData);
		$this->client = Cubix_Api::getInstance();
	}

	public function isEscort()
	{
		return $this->user_type == 'escort';
	}
	
	public function isAgency()
	{
		return $this->user_type == 'agency';
	}
	
	public function isMember()
	{
		return $this->user_type == 'member';
	}
	
	public function hasProfile()
	{
		if ( ! $this->isEscort() ) return NULL;
		
//		$client = new Cubix_Api_XmlRpc_Client();
//		return $client->call('Escorts.hasProfile', array($this->getId()));

		$client = Cubix_Api::getInstance();
		return $client->call('hasEscortProfileV2', array($this->getId()));
	}
	
	public function getFavorites()
	{
		$filter = array('f.user_id = ?' => $this->getId());
		
		
		$page_num = 1;
		if ( isset($page[1]) && $page[1] ) {
			$page_num = $page[1];
		}

		$count = 0;
		$escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 100, $count);
				
		return array('escorts' => $escorts, 'count' => $count);
	}
	
	public function getEscort()
	{
		$model = new Model_Escorts();
		
		return $model->getByUserId($this->id);
	}
	
	public function getAgency()
	{
		$model = new Model_Agencies();
		
		return $model->getByUserId($this->id);
	}
	
	public function getSalesPerson()
	{
		static $sales_person;
		
		if ( empty($sales_person) ) {
			$sales_person = Cubix_Api_XmlRpc_Client::getInstance()->call('Users.getUserSalesPerson', array($this->getId()));
		}
		
		return $sales_person;
	}

	public function getSalesAdminPersons()
	{
		static $sales_persons;

		if ( empty($sales_persons) ) {
			$sales_persons = Cubix_Api_XmlRpc_Client::getInstance()->call('Users.getSalesAdminPersons');
		}

		return $sales_persons;
	}


	public function updateData($data)
	{
		$this->client->call('updateUserData', array($this->getId(), $data));
		$this->relogin();
	}

	public function updatePassword($password, $new_password)
	{
		$result = $this->client->call('updateUserPassword', array($this->getId(), $password, $new_password));

		if ( false === $result ) {
			throw new Exception('Invalid password');
		}
	}

	public function updateRecieveNewsletters($flag)
	{
		$this->client->call('updateUserRecieveNewsletters', array($this->getId(), $flag));
		$this->relogin();
	}

	public function relogin()
	{
		$model = new Model_Users();
		foreach ( $model->getById($this->getId()) as $key => $value ) {
			$this->$key = $value;
		}
	}

	/**
	 * Need this func for ability to serialize this object
	 *
	 * @author GuGo
	 */
	public function __wakeup()
	{
		$this->setAdapter(Model_Users::getAdapter());
		$this->client = Cubix_Api::getInstance();
	}
}
