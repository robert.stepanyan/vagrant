<?php

class Model_Escort_Photos extends Cubix_Model
{
	const STATUS_VERIFIED = 1;
	const STATUS_NOT_VERIFIED = 2;
	const STATUS_NORMAL = 3;
	
	const PHOTO_ROTATE_ALL = 1;
	const PHOTO_ROTATE_SELECTED = 2;
	const PHOTO_ROTATE_SELECTED_ONE = 3;
	
	protected $_table = 'escort_photos';
	protected $_itemClass = 'Model_Escort_PhotoItem';

	public function get($id)
	{
		$photo = self::getAdapter()->fetchRow('
			SELECT * FROM escort_photos WHERE id = ?
		', $id);
		
		if ( ! $photo ) return null;
		
		$photo->application_id = Cubix_Application::getId();
		
		return new Model_Escort_PhotoItem($photo);
	}
	
	public function remove($ids)
	{
		$user = Model_Users::getCurrent();
		
		if ( ! is_array($ids) ) $ids = array($ids);
		
		foreach ( $ids as $i => $id ) {
			$ids[$i] = intval($id);
			if ( ! $ids[$i] ) unset($ids[$i]);
		}
		
		$client = new Cubix_Api_XmlRpc_Client();
//		try {
			$result = $client->call('Escorts.removePhotos', array($ids, $user->sign_hash));
//		}
//		catch ( Exception $e ) {
//			echo $e; ob_flush(); die;
//		}
		
		if ( is_array($result) && isset($result['error']) ) {
			return array('error' => 'An error occured when removing photos');
		}
		
		if ( is_int($result) ) {
			$main_photo_id = $result;
			self::getAdapter()->update('escort_photos', array('is_main' => 1), self::quote('id = ?', $main_photo_id));
		}
		
		try {
			$this->_db->delete('escort_photos', 'id IN (' . implode(', ', $ids) . ')');
		}
		catch ( Exception $e ) {
			return array('error' => 'An error occured when removing photos');
		}
		
		return true;
	}
	
	public function save(Model_Escort_PhotoItem $item)
	{
		$user = Model_Users::getCurrent();
		
		$escort_status = $this->_db->fetchOne('SELECT verified_status FROM escorts WHERE id = ?', $item->escort_id);
		
		if ( Model_Escorts::VERIFIED_STATUS_VERIFIED == $escort_status || Model_Escorts::VERIFIED_STATUS_VERIFIED_RESET == $escort_status ) {
			$item->status = self::STATUS_NOT_VERIFIED;
			
			if ( Model_Escorts::VERIFIED_STATUS_VERIFIED_RESET != $escort_status ) {
				$this->_db->update('escorts', array('verified_status' => Model_Escorts::VERIFIED_STATUS_VERIFIED_RESET), $this->_db->quoteInto('id = ?', $item->escort_id));
			}
		}
		
		$max = intval(self::getAdapter()->fetchOne('
			SELECT MAX(ordering) FROM escort_photos WHERE escort_id = ?' . 
			( $item->type == ESCORT_PHOTO_TYPE_PRIVATE ? ' AND type = ' . ESCORT_PHOTO_TYPE_PRIVATE : ' AND type <> ' . ESCORT_PHOTO_TYPE_PRIVATE ) . '
		', $item->getEscortId()));
		
		if ( ! $max ) $max = 0;
		$max++;
		
		$item->ordering = $max;
		
		$client = new Cubix_Api_XmlRpc_Client();
		/*if ( 0 == self::getAdapter()->fetchOne('SELECT COUNT(id) FROM escort_photos WHERE escort_id = ? AND is_main = 1 AND type <> 3', $item->getEscortId()) &&
				$item->type <> ESCORT_PHOTO_TYPE_PRIVATE ) {
			$item->is_main = 1;
		}*/
		if ( 0 == $client->call('Escorts.hasMainPhoto', array($item->getEscortId())) &&
				$item->type <> ESCORT_PHOTO_TYPE_PRIVATE ) {
			$item->is_main = 1;
		}
		
		
		$id = $client->call('Escorts.addPhoto', array($item->getData(), $user->sign_hash));
		$id = intval($id);
		
		$item->id = $id;
		
		try {
			self::getAdapter()->insert('escort_photos', array_merge(array('id' => $item->id), $item->getData()));
		}
		catch ( Exception $e ) {
			
		}
		
		if ( ! $id ) {
			throw new Exception('An error occured when uploading your file');
		}
		
		return $this->get($id);
	}

	public function reorder($photos)
	{
		Cubix_Api::getInstance()->call('reorderEscortPhotos', array($photos));

		foreach ( $photos as $i => $photo_id ) {
			$p = new Model_Escort_PhotoItem(array('id' => $photo_id));
		}
	}
	
	public function getRotatablePicsByEscortId($escort_id)
	{
		$photos = array();
		$results = self::getAdapter()->fetchAll('
			SELECT SQL_CALC_FOUND_ROWS id , is_main FROM escort_photos WHERE is_rotatable = 1 AND status !='.self::STATUS_NOT_VERIFIED.' AND type !='. ESCORT_PHOTO_TYPE_PRIVATE .' AND type !='. ESCORT_PHOTO_TYPE_DISABLED .' AND escort_id = ? ORDER BY id
		', $escort_id);
		
		if ( ! $results ) return null;
		
		$count = parent::getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		
		$photos['results'] = $results;
		$photos['count'] = $count;
		
		return $photos;
	}
	
	public function setMainImageFront($id)
	{
				
		self::getAdapter()->query('UPDATE escorts e INNER JOIN escort_photos ep ON ep.escort_id = e.id SET e.photo_hash = ep.hash, e.photo_ext = ep.ext, e.photo_status = ep.status WHERE ep.id = ?', $id);
		
		$this->getAdapter()->query('
			UPDATE escort_photos ep
			INNER JOIN escort_photos ep1 ON ep.escort_id = ep1.escort_id
			SET ep.is_main = FALSE
			WHERE ep1.id = ?
		', $id);
		
		self::getAdapter()->query('UPDATE escort_photos SET is_main = TRUE WHERE id = ?', $id);
		
		return true;
	}
	
	
}
