<?php


class AnalyticsController extends Zend_Controller_Action {

		private $user;
	
		public function init()
		{

			 $this->user = Model_Users::getCurrent();
			 $this->_helper->layout()->disableLayout(); 
		  	 $this->_helper->viewRenderer->setNoRender(true);
		
		}

		public function setValueAction()
		{	
		
			if($this->_request->isXmlHttpRequest() && $this->_request->isPost() && isset($_POST['data']) && !empty($_POST['data']) && isset($_POST['filter']))
			{	
				session_start();
				if(!isset($_SESSION['user_data']))
				{ 
				$user_data = Cubix_Geoip::getClientLocation($ip);
				$_SESSION['user_data'] = $user_data;
				}
				else
				{	
					$user_data = $_SESSION['user_data'];
				}
				$app_id = Cubix_Application::getId();
				Model_Analytics::Insert($_POST['data'], $this->user['id'], $_POST['filter'], $user_data,$app_id);
			}
		}
		
		
			
		
		
	
}
