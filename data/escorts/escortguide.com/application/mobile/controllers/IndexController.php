<?php

class Mobile_IndexController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_helper->layout->setLayout('mobile-index');
        if(!$this->_getParam('escort_id')){
            $_SESSION['request_url'] = $_SERVER['REQUEST_URI'];
        }
       
		$this->view->static_page_key = 'main';
        	//die('Mobile version is temporary unavailable');http://m.6annonce.com/remove-from-favorites?=11490
	}

    public function indexAction()
	{
        $gender = GENDER_FEMALE;
        $is_agency = null;
        
        $query = '';
        $where = null;
        if($this->_getParam('q')){
            $query = ltrim($this->_getParam('q'), '/');
            $where = array('ct.slug LIKE ?' => '%'.$query.'%');
        }

        $all_cities = Model_Statistics::getCities(null, $gender, $is_agency, null, false,$where);

        if(!$this->_getParam('all')){
            $this->view->cities = array_slice($all_cities, 0, 24);
        }else{
            $this->view->cities = $all_cities;
            $this->view->is_all = true;
        }
        $this->view->query = $query;

        $this->view->menuHome = 1;
        $this->view->total_count = Model_Statistics::getTotalCount($gender, $is_agency, null, false);
	}


    private function _orderTitles($a,$b){
        if ( is_object($a) && is_object($b) ) {
			return strnatcmp($a->city_title, $b->city_title);
		}
		elseif ( is_array($a) && is_array($b) ) {
			return strnatcmp($a["city_title"], $b["city_title"]);
		}
		else return 0;
    }

    public function searchAction()
	{

        $this->view->defines = Zend_Registry::get('defines');

     
        $all_cities = Model_Statistics::getCities();
        $config = Zend_Registry::get('mobile_config');
        $all_zones = Model_Statistics::getZones();



        usort($all_cities, array(self, "_orderTitles"));

        $this->view->menuSearch = 1;
        $this->view->cities = $all_cities;
        $this->view->zones = $all_zones;
        

        if( $this->_getParam('search') )
		{
			$params['filter'] = array();
            $params['order'] = array();

			$this->view->params = $this->_request->getParams();
            
            if( $this->_getParam('order') ) {

                $order_direction = '';
                $order_field = 'date_registered';

                switch($this->_getParam('order')){
                    case '1':
                       $order_field = "date_last_modified";
                       break;
                    case '2':
                       $order_field = "e.showname ASC";
                       break;
                   case '3':
                       $order_field = "e.hit_count DESC";
                       break;
                   case '4':
                       $order_field = "e.is_new DESC";
                       break;
                   case '5':
                       $order_field = "e.date_last_modified DESC";
                       break;
                   case '6':
                       $order_field = "e.outcall_price ASC";
                       break;
                   case '7':
                       $order_field = "e.outcall_price DESC";
                       break;
                }

                if($this->_getParam('order_direction')){
                   $order_direction = $this->_getParam('order_direction');
                }
                $params['order'] = $order_field." ".$order_direction;

			}


            if($this->_getParam('page')){
                $params['limit']['page'] = $this->_getParam('page');
            }
            if($this->_getParam('verified')){
                $params['filter'] = array_merge($params['filter'], array(
					'e.verified_status = ?' => Model_VerifyRequests::STATUS_VERIFIED
				));
            }


			if( $this->_getParam('showname') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.showname LIKE ?' => "%" . trim($this->_getParam('showname')) . "%"
				));
			}


			if( $this->_getParam('gender') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.gender = ?' => $this->_getParam('gender')
				));
			}

			if( $this->_getParam('city') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ct.slug = ?' => $this->_getParam('city')
				));
			}

            if( $this->_getParam('zone')  ) {
				$params['filter'] = array_merge($params['filter'], array(
					'cz.slug = ?' => $this->_getParam('zone')
				));
			}

			if( $this->_getParam('available_for_incall') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.incall_type IS NOT NULL' => true
				));
			}
			if( $this->_getParam('available_for_outcall') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.outcall_type IS NOT NULL' => true
				));
			}

            if( $this->_getParam('french') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.nationality_id = 15' => true
				));
			}


			/* ------------ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<< --------------- */


            if( $this->_getParam('ethnicity') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.ethnicity IN (' . implode(',', $this->_getParam('ethnicity')) . ')' => array()
				));

			}

            if( $this->_getParam('hair_color') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.hair_color IN (' . implode(',', $this->_getParam('hair_color') ) . ')' => array()
				));
			}

			if( $this->_getParam('hair_length') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.hair_length IN (' . implode(',', $this->_getParam('hair_length') ) . ')' => array()
				));
			}

			if( $this->_getParam('eye_color') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.eye_color IN (' . implode(',', $this->_getParam('eye_color') ) . ')' => array()
				));
			}

            

            if( $this->_getParam('pubic_hair') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.pubic_hair = ?' => $this->_getParam('pubic_hair')
				));
			}



			if( $this->_getParam('drinking') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.drinking = ?' => $this->_getParam('drinking')
				));
			}
        

            $page = 1;
            $page_size = $config['escorts']['perPage'];
            if($this->_getParam('page')){
                $page = $this->_getParam('page');
            }

			$model = new Model_EscortsV2();
			$count = 0;
			$escorts = $model->getSearchAll($params, $count,$page,$page_size);
			

			if ( count( $escorts['data'] ) > 0 ){
				// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
				$sess_name = "prev_next";
				$sid = 'sedcard_paging_' . Cubix_Application::getId();
				$sess = new Zend_Session_Namespace($sid);

				$sess->{$sess_name} = array();
				foreach ( $escorts['data'] as $escort ) {
					//$sess->{$sess_name}[] = $escort->showname;
					$sess->{$sess_name}[] = $escort->id;
				}
				// </editor-fold>
			}
			


            $this->view->page = $page;
			$this->view->count = $escorts['count'];
			$this->view->escorts = $escorts['data'];
			$this->view->search_list = true;

            $this->_helper->viewRenderer->setScriptAction("escort-list");

		}
	}

    public function favoritesAction()
	{
		$page = 1;
        $page_size = 9;
        if($this->_getParam('page')){
            $page = $this->_getParam('page');
        }

		$this->user = Model_Users::getCurrent();
		if ( $this->user->user_type == 'member' ) {
			$this->view->is_member = 1;

			$model = new Model_Users();
			$escorts = $model->getCurrent()->getFavorites();

			$count = $escorts['count'];
			$this->view->escorts = $escorts['escorts'];
			
		}else{
			$cook_name = "favorites";
			$data = unserialize($_COOKIE[$cook_name]);
			if($data && count($data) > 0){
				foreach ( $data as $e ) {
					$ids[] = $e;
				}

				$filter = array('e.id IN (' . implode(', ', $ids) . ')' => array());

				$count = 0;
				$escorts = Model_Escort_List::getFiltered($filter, 'random', $page, $page_size, $count);


				$this->view->escorts = $escorts;

				if ( count( $escorts ) > 0 ){
					// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
					$sess_name = "prev_next";
					$sid = 'sedcard_paging_' . Cubix_Application::getId();
					$sess = new Zend_Session_Namespace($sid);

					$sess->{$sess_name} = array();
					foreach ( $escorts as $escort ) {
						$sess->{$sess_name}[] = $escort->showname;
					}
					// </editor-fold>
				}
			}
		}



        $this->view->page = $page;
		$this->view->count = $count;

        $this->view->menuFavorites = 1;

        $this->_helper->viewRenderer->setScriptAction("favorites-list");

	}

    

    public function escortsAction()
	{
        
        
	}

	public function gpsLocationAction()
	{	
		$req = $this->_request;
		
		if (isset($req->lat) && isset($req->long))
		{	
			 $config = Zend_Registry::get('mobile_config');
             $limit = $config['escorts']['perPage'];
			 $page = $req->page;
             $page = $this->view->page = isset($page) && is_numeric($page) ? intval($page) : 1;
			 $page = ($page - 1) * $limit;
			 
			 $model = new Model_EscortsV2();
			 
			 $this->view->lat = floatval($req->lat);
			 $this->view->long = floatval($req->long);
			 $escorts = $model->getEscortsByLocation($req->lat, $req->long, $limit, $page);
			 $this->view->count = $escorts['count'];
			 $this->view->escorts = $escorts['data'];
		}
		else
		{
			$this->view->error = 1;
		}
	}
	
    public function addToFavoritesAction(){
		$req = $this->_request;
        $escort_id = intval($req->escort_id);
        $cook_name = "favorites";
        $cook_value = array($escort_id);
        if ( ! isset( $_COOKIE[$cook_name]) ) {
            setcookie($cook_name, serialize($cook_value), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
        }
        else {
            $cook_data = unserialize($_COOKIE[$cook_name]);
            if ( is_array($cook_data) ) {
                if ( in_array($cook_value[0], $cook_data) ) {
                    $this->view->alredy_in_favorites = true;
                }
                else {
                    $new_data = array_merge($cook_data, $cook_value);
                    setcookie($cook_name, serialize($new_data), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
                }
            }
        }

		$this->user = Model_Users::getCurrent();

		if ( $this->user->user_type == 'member' ) {
			$user_id = $this->user->id;
			$escort_id = $this->_request->escort_id;
			$model = new Model_Escorts();
			$model->addToFavorites($user_id, $escort_id);
		}


		


        $this->_redirect($_SERVER['HTTP_REFERER']);
    }
    
    
    public function removeFromFavoritesAction(){
		$req = $this->_request;
        $escort_id = intval($req->escort_id);
        $cook_name = "favorites";
        $cook_value = array($escort_id);
        if ( isset( $_COOKIE[$cook_name]) ) {
            $cook_data = unserialize($_COOKIE[$cook_name]);
            if ( is_array($cook_data) ) {
                if ( in_array($cook_value[0], $cook_data) ) {
                    $new_data = array_diff($cook_data, $cook_value);
                    setcookie($cook_name, serialize($new_data), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
                }
            }
        }

		$this->user = Model_Users::getCurrent();

		if ( $this->user->user_type == 'member' ) {
			$user_id = $this->user->id;
			$escort_id = $this->_request->escort_id;
			$model = new Model_Escorts();
			$model->removeFromFavorites($user_id, $escort_id);
		}


        $this->_redirect($_SERVER['HTTP_REFERER']);
    }


    public function feedbackAction()
	{
		$this->_request->setParam('no_tidy', true);

		$menu = array(
            'bio' => array(
                'name' => 'bio',
                'class' => 'w15',
            ),
            'services' => array(
                'name' => 'services',
                'class' => 'w23',
            ),
            'rates' => array(
                'name' => 'rates',
                'class' => 'w18',
            ),
            'contacts' => array(
                'name' => 'contact',
                'class' => 'w24',
            ),
            'photos' => array(
                'name' => 'photos',
                'class' => 'w20',
            ));

		$this->view->menu = $menu;


		$model = new Model_EscortsV2();

        $this->_helper->layout->setLayout('mobile');



		// Fetch administrative emails from config
		$config = Zend_Registry::get('feedback_config');
		$site_emails = $config['emails'];


		// Get data from request
		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'to' => '',
			'name' => 'notags|special',
			'email' => '',
			'message' => 'notags|special'
		);
		$data->setFields($fields);
		$data = $data->getData();

		$this->view->data = $data;

		// If the to field is invalid, that means that user has typed by
		// hand, so it's like a hacking attempt, simple die, without explanation
      

		if ( ! is_numeric($data['to']) && ! in_array($data['to'], array_keys($site_emails)) ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}

		// If escort id was specified, fetch it's email and showname,
		// construct the result email template and send it to escort
		if ( is_numeric($data['to']) ) {
			$escort = $model->get($data['to']);
			$this->view->escort = $escort;

            
            
			// If the escort id is invalid that means that it is hacking attempt
			if ( ! $escort ) {
				header('HTTP/1.1 301 Moved Permanently');
                header('Location: /');
                die;
                return;
			}
            
			$data['to_addr'] = $escort->email;
			$data['to_name'] = $escort->showname;
			$data['username'] = $escort->username;
			$email_tpl = 'escort_feedback';

            $this->view->showname = $escort->showname;
		}

		// In order when the form was posted validate fields
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', '' /*'Email is required'*/);
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', '' /*'Wrong email format'*/);
			}

			if ( ! strlen($data['message']) ) {
				$validator->setError('message','' /*'Please type the message you want to send'*/);
			}

			
			$result = $validator->getStatus();
            
            if ( ! $validator->isValid() ) {
                $this->view->errors = $result['msgs'];
			}else{
               

                // Set the template parameters and send it to support or to an escort
                $tpl_data = array(
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'to_addr' => $data['to_addr'],
                    'message' => $data['message'],
                    'escort_showname' => $escort->showname,
                    'escort_id' => $escort->id
                );

                if ( isset($data['username']) ) {
                    $tpl_data['username'] = $data['username'];
                }
                
                Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? $data['email'] : null);

                $this->_redirect('escort/'.$escort->showname.'?success=true#contacts');

            }
		}
	}

	public function robotsAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->layout()->disableLayout();
		$model = new Model_Robots();

		$robots = $model->getMobile();

		header('Content-Type: text/plain; charset=UTF-8', true);
		echo $robots;
		/*echo trim("User-agent: *
Dissallow: /page/terms-and-conditions
Disallow: /favorites
Disallow: /en
Disallow: /search");*/
		die;
	}
	
	public function goAction()
	{
		$link = $_SERVER['QUERY_STRING'];
		if ( ! preg_match('#^http://#', $link) ) $link = 'http://' . $link;
		
		header('Location: ' . $link);
		die;
	}

	public function getCityzonesAction()
	{
		$this->view->layout()->disableLayout();

		$city_slug = $this->_request->city;

		$c_model = new Cubix_Geography_Cities();
		$city = $c_model->getBySlug( $city_slug );

		$cz_model = new Cubix_Geography_Cityzones();
		
		$all_cityzones = $cz_model->ajaxGetAll($city->id);

		$this->view->zones = $all_cityzones;
//		var_Dump($this->view->zones);
//		exit;
	}


	public function ajaxGetCitiesAction()
	{
		$this->view->layout()->disableLayout();

		$this->_request->setParam('no_tidy', true);

		$country_id = intval($this->_getParam('country_id'));
		$region_id = intval($this->_getParam('region_id'));

		$model = new Cubix_Geography_Cities();

		$this->view->regions = $model->ajaxGetAll($region_id, $country_id);
		
	}



	public function contactAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->_request->setParam('to', 'support');

		$model = new Model_EscortsV2();

		// Fetch administrative emails from config
		$config = Zend_Registry::get('feedback_config');
		$site_emails = $config['emails'];


		$is_ajax = ! is_null($this->_getParam('ajax'));

		// If the request is ajax, disable the layout
		if ( $is_ajax ) {
			$this->view->layout()->disableLayout();
		}

		// Get data from request
		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'to' => '',
			'about' => 'int',
			'name' => 'notags|special',
			'email' => '',
			'message' => 'notags|special',
			'captcha' => ''
		);
		$data->setFields($fields);
		$data = $data->getData();

		$this->view->data = $data;

		// If the to field is invalid, that means that user has typed by
		// hand, so it's like a hacking attempt, simple die, without explanation
		if ( ! is_numeric($data['to']) && ! in_array($data['to'], array_keys($site_emails)) ) {
			die;
		}

		// If escort id was specified, fetch it's email and showname,
		// construct the result email template and send it to escort
		if ( is_numeric($data['to']) ) {
			$escort = $model->get($data['to']);
			$this->view->escort = $escort;
			//var_dump($escort);die;
			// If the escort id is invalid that means that it is hacking attempt
			if ( ! $escort ) {
				die;
			}

			$data['to_addr'] = $escort->email;
			$data['to_name'] = $escort->showname;
			$data['username'] = $escort->username;
			$email_tpl = 'escort_feedback';
		}
		// Else get administrative email from configuration file
		else {
			$data['to_addr'] = $site_emails[$data['to']];
			$email_tpl = 'feedback_template';
		}

		// In order when the form was posted validate fields
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Email is required');
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			}

			if ( ! strlen($data['message']) ) {
				$validator->setError('message', 'Please type the message you want to send');
			}

			if ( ! strlen($data['captcha']) ) {
				$validator->setError('captcha', 'Captcha is required');
			}
			else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', 'Captcha is invalid');
				}
			}

			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				// If the request was ajax, send the validation result as json
				if ( $is_ajax ) {
					echo json_encode($result);
					die;
				}
				// Otherwise, render the phtml and show errors in it
				else {
					$this->view->errors = $result['msgs'];
					return;
				}
			}
			$sent_by = '';
			if ( $user = Model_Users::getCurrent() )
			{
				$sent_by = $user->user_type.' '.$user->username.' wrote a message';
			}
			if ( $data['about'] ) {
				$about_escort = $model->get($data['about']);
			}
			if ( ! $about_escort ) $about_escort = (object) array('id' => '', 'showname' => '');

			// Set the template parameters and send it to support or to an escort
			$tpl_data = array(
				'name' => $data['name'],
				'email' => $data['email'],
				'to_addr' => $data['to_addr'],
				'message' => $data['message'],
				'escort_showname' => $about_escort->showname,
				'escort_id' => $about_escort->id,
				'sent_by' => $sent_by
			);

			if ( isset($data['username']) ) {
				$tpl_data['username'] = $data['username'];
			}

			Cubix_Email::sendTemplate($email_tpl, $data['to_addr'] , $tpl_data, isset($escort) ? $data['email'] : null);

			// contact me log
			Cubix_Api::getInstance()->call('contactLog', array($data['to_addr'], $user ? $user->id : null));
			//

			if ( $is_ajax ) {
				if ( isset($escort) ) {
					$result['msg'] = '
						<p style="padding:0 10px"><span class="strong" style="color: #060">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>
					';
				}
				else {
					$result['msg'] = '
						<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>
					';
				}
				//<a href="#" onclick="return Cubix.Feedback.Show(this.getParent(\'.cbox-small-ii\'))">Click here</a> to hide this message</strong>
				echo json_encode($result);
				die;
			}
			else {
				$this->view->success = TRUE;
			}
		}
	}


	public function captchaAction()
	{

		$this->_request->setParam('no_tidy', true);

		$font = 'css/trebuc.ttf';

		$charset = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';

		$code_length = 5;

		$height = 30;
		$width = 90;
		$code = '';

		for ( $i = 0; $i < $code_length; $i++ )
		{
			$code = $code . substr($charset, mt_rand(0, strlen($charset) - 1), 1);
		}

		$rgb[0] = array(204,0,0);
		$rgb[1] = array(34,136,0);
		$rgb[2] = array(51,102,204);
		$rgb[3] = array(141,214,210);
		$rgb[4] = array(214,141,205);
		$rgb[5] = array(100,138,204);

		$font_size = $height * 0.4;

		$bg = imagecreatefrompng('img/bg_captcha.png');
		$image = imagecreatetruecolor($width, $height);
		imagecopy($image, $bg, 0, 0, 0, 0, imagesx($bg), imagesy($bg));
		$background_color = imagecolorallocate($image, 255, 255, 255);
		$noise_color = imagecolorallocate($image, 20, 40, 100);

		for($i = 0; $i<$code_length; $i++)
		{
			$A[] = rand(-20, 20);
			$C[] = rand(0, 5);
			$text_color = imagecolorallocate($image, $rgb[$C[$i]][0], $rgb[$C[$i]][1], $rgb[$C[$i]][2]);
			imagettftext($image, $font_size, $A[$i], 7 + $i * 15, 20 + rand(-3, 3), $text_color, $font , $code[$i]);
		}

		$session = new Zend_Session_Namespace('captcha');
		$session->captcha = strtolower($code);

		header('Content-Type: image/png');
		imagepng($image);
		imagedestroy($image);

		die;
	}

}
