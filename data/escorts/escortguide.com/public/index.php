<?php
//session_start();

//date_default_timezone_set('America/Los_Angeles');

$srv = 'unknown';
if ( preg_match('#\.([0-9]+)$#', $_SERVER['SERVER_ADDR'], $match) ) {
	switch ( (int) $match[1] ) {
		case 11:
			$srv = 'web1';
			break;
		case 51:
			$srv = 'pic';
			break;
		case 13:
			$srv = 'web3';
			break;
	}
}
header('Cubix-Server: ' . $srv);

//session_set_cookie_params(0, '/', '.6annonce.com');

$GLOBALS['SQLS'] = array();

if ( preg_match('#\.dev$#', $_SERVER['SERVER_NAME']) > 0 ) {
	define('IS_DEBUG', true);
}
else {
	define('IS_DEBUG', false);
}

$host = $_SERVER['HTTP_HOST'];
$host = explode('.',$host);

if( isset($_GET['full_version']) && ! empty($_GET['full_version']) ) {
    setcookie('full_version', 1, null, "/",  $_SERVER['HTTP_HOST'] );
}


if( count($host) > 0 ) {
    $prefix = array_shift($host);

    if( $prefix == "m" || $prefix == "mobile" ) {
        $parts = explode('/',$_SERVER['REQUEST_URI']);

		$useragent = $_SERVER['HTTP_USER_AGENT'];
        if( !preg_match('/android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
        {
			$uri = ( strlen($_SERVER['REQUEST_URI']) > 1 ) ? $_SERVER['REQUEST_URI'] : '';
				
			header('Location: http://www.escortguide.com' . $uri );
		}
		
        if ( isset($parts[1]) && $parts[1] == 'en' ) {
            unset($parts[1]);
            $url = implode('/', $parts);
            $_SERVER['REQUEST_URI'] = 'en/mobile' . $url;
        } else {
            $_SERVER['REQUEST_URI'] = 'mobile' . $_SERVER['REQUEST_URI'];
        }		
		
    } elseif ( !isset( $_COOKIE['full_version'] ) && !isset( $_GET['full_version'] ) ) {
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        if(preg_match('/android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
        {
			$uri = ( strlen($_SERVER['REQUEST_URI']) > 1 ) ? $_SERVER['REQUEST_URI'] : '';
				
			header('Location: http://m.escortguide.com' . $uri );
		}
    }
}


mb_internal_encoding('UTF-8');



$app_env = 'production';
if ( preg_match('#\.dev$#', $_SERVER['SERVER_NAME']) ) {
    $app_env = 'development';
}
elseif ( preg_match('#dev.sceon.(int|am)$#', $_SERVER['SERVER_NAME']) ) {
    $app_env = 'staging';
}
define('APPLICATION_ENV', $app_env);

function mb_str_replace($needle, $replacement, $haystack)
{
    $needle_len = mb_strlen($needle);
    $replacement_len = mb_strlen($replacement);
    $pos = mb_strpos($haystack, $needle);
    while ($pos !== false)
    {
        $haystack = mb_substr($haystack, 0, $pos) . $replacement
                . mb_substr($haystack, $pos + $needle_len);
        $pos = mb_strpos($haystack, $needle, $pos + $replacement_len);
    }
    return $haystack;
}

//die('We are not available right now, we will back in a moment!');

if ( ! function_exists('json_encode') ) {
	function json_encode($data) {
		return Zend_Json::encode($data);
	}
}

if ( ! function_exists('json_decode') ) {
	function json_decode($data) {
		return Zend_Json::decode($data);
	}
}

if ( isset($_POST['PHPSESSID']) ) {
	$sessid = $_POST['PHPSESSID'];
	$_COOKIE['PHPSESSID'] = $sessid;
	session_id($_POST['PHPSESSID']);
}

/*require '../../library/Cubix/Debug.php';
require '../../library/Cubix/Debug/Benchmark.php';

if ( isset($_REQUEST['benchmark']) ) Cubix_Debug::setDebug(true);
Cubix_Debug_Benchmark::setLogType(Cubix_Debug_Benchmark::LOG_TYPE_DB);
Cubix_Debug_Benchmark::setApplication('frontend');
Cubix_Debug_Benchmark::start($_SERVER['REQUEST_URI']);
*/

/**
 * Used to construct static server url
 *
 * This function will automatically decide which folder to prefix to the given
 * parameter by examining it's extension
 *
 * /img/ - png, gif, jpg
 * /js/ - js
 * /css/ - css
 */
function _st($object)
{
	$matches = array();
	if ( 0 == preg_match('#\.([a-z]{2,4})/?#', $object, $matches) ) {
		throw new Exception('Invalid object, expected a filename with extension');
	}

	$base_url = 'http://www.escortguide.com';
	if ( defined('IS_DEBUG') && IS_DEBUG ) {
		$base_url = 'http://' . $_SERVER['HTTP_HOST'];
	}
	array_shift($matches);
	list($ext) = $matches;

	switch ( $ext ) {
		case 'jpg':
		case 'gif':
		case 'png':
			$prefix = '/img/';
			break;
		case 'css':
			$prefix = '/css/';
			break;
		case 'js':
			$prefix = '/js/';
			break;
		case 'php':
			$prefix = '/';
			break;
		default:
			throw new Exception('Invalid object, unsupported extension "' . $ext . '"');
	}

	$url = $base_url . $prefix . $object;

	return $url;
}

/**
 **************************** FOR MOBILE VERSION ********************
 *
 * Used to construct static server url
 *
 * This function will automatically decide which folder to prefix to the given
 * parameter by examining it's extension
 *
 * /img/ - png, gif, jpg
 * /js/ - js
 * /css/ - css
 */
function _stm($object)
{
	$matches = array();
	if ( 0 == preg_match('#\.([a-z]{2,4})$#', $object, $matches) ) {
		throw new Exception('Invalid object, expected a filename with extension');
	}

	$base_url = 'http://www.escortguide.com';
	if ( defined('IS_DEBUG') && IS_DEBUG ) {
		$base_url = 'http://' . $_SERVER['HTTP_HOST'];
	}

	array_shift($matches);
	list($ext) = $matches;

	switch ( $ext ) {
		case 'jpg':
		case 'gif':
		case 'png':
			$prefix = '/m/i/';
			break;
		case 'css':
			$prefix = '/m/';
			break;
		case 'js':
			$prefix = '/m/js/';
			break;
		default:
			throw new Exception('Invalid object, unsupported extension "' . $ext . '"');
	}

	$url = $base_url . $prefix . $object;

	return $url;
}





//session_start();
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../../library'),
	realpath(APPLICATION_PATH . '/../../../usr/lib'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$mtime = microtime();
$mtime = explode(" ",$mtime);
$mtime = $mtime[1] + $mtime[0];
$GLOBALS['exec_start_time'] = $mtime;

// <editor-fold defaultstate="collapsed" desc="Output Cache">
	$frontendOptions = array(
		'lifetime' => null,
		'automatic_serialization' => true
	);

	$backendOptions = array(
		'servers' => array(
			array(
				'host' => 'localhost',
				'port' => '11211',
				'persistent' =>  true
			)
		),
	);

// <editor-fold defaultstate="collapsed" desc="Output Cache">
	define('USE_CACHE', false);

	/*$cache = Zend_Cache::factory('Output', 'Memcached', $frontendOptions, $backendOptions);

	global $frontend_cache;
	$frontend_cache = $cache;*/

	function _cs($key)
	{
		if ( defined('USE_CACHE') && ! USE_CACHE ) {
			return;
		}

		global $frontend_cache;

		$key = 'html_cache_' . $key;
		return $frontend_cache->start($key);
	}

	function _ce()
	{
		if ( defined('USE_CACHE') && ! USE_CACHE ) {
			return;
		}

		global $frontend_cache;

		return $frontend_cache->end();
	}
// </editor-fold>

	//error_reporting(E_ALL);
	//ini_set('display_errors', '1');

	try {
		$application->bootstrap()->run();
	}
	catch ( Exception $e ) {
		//echo $e->__toString(); die;
		$error = '[' . date('d.m.Y H:i:s') . ']: URL: ' . $_SERVER['REQUEST_URI'] . "\n";
		$error .= get_class($e) . "\n";
		$error .= $e->__toString();
		$error .= "\n------------------\n";
		file_put_contents('debug.log', $error, FILE_APPEND);
		echo 'Unexpected error!';
	}
	

/*
Cubix_Debug_Benchmark::end($_SERVER['REQUEST_URI']);

if ( Cubix_Debug::isDebug() ) {
	Cubix_Debug_Benchmark::log();
}
*/
