/* --> Feedback */
Cubix.Feedback = {};


Cubix.Feedback.inProcess = false;
Cubix.Feedback.wrapper = 'contactme_wrapper';

Cubix.Feedback.captcha = '';

Cubix.Feedback.Show = function (element, escort_id, direct) {
	if ( Cubix.Feedback.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();

	if ( ! element.overlay ) {
		element.overlay = new Cubix.Overlay(element, { loader: _st('loader-circular-tiny.gif'), position: '50%' });
	}
	
	if ( $defined($$('.' + Cubix.Feedback.wrapper)[0])) {
		$$('.' + Cubix.Feedback.wrapper).destroy();
		//return false;
	}
	
	var box_height = 498;
	var box_width = 375;
	
	/*y = document.body.getScrollSize().y - (element.getPosition().y + box_height);
	x = element.getPosition().x - (box_width / 2).toInt() + (element.getSize().x/2).toInt();
	
	if(y < 0)
		y_offset = - (box_height +  10);
	else
		y_offset = 25;
	
	if(x > 0)
		x_offset = - (box_width / 2).toInt() + (element.getSize().x/2).toInt();
	else
		x_offset = 0;
	
	var position = element.getPosition();
	var pos_left = position.x + x_offset + 'px';
	var pos_top = position.y + y_offset + 'px';*/

	var y_offset = 50;

	var container = new Element('div', { 'class': Cubix.Feedback.wrapper}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0
	}).inject(document.body);
	
	Cubix.Feedback.inProcess = true;
	element.overlay.disable();

	var url = Cubix.Feedback.url + '?to=support&about=' + ($defined(escort_id) ? escort_id : 'support') + '&ajax';
	if ( $chk(direct) && direct ) {
		url = Cubix.Feedback.url + '?to=' + escort_id + '&ajax';
	}

	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.Feedback.inProcess = false;
			container.set('html', resp);

			//container.setStyle('padding-top', '30px');
			//container.setStyle('position', 'relative');

			var close_btn = new Element('div', {
				html: '',
				'class': 'cm-close-btn'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.' + Cubix.Feedback.wrapper).destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
			
			var form = container.getElement('form');
			form.addEvent('submit', Cubix.Feedback.Send);
			form.set('action', form.get('action') + '?ajax');
			element.overlay.enable();

			//showRecaptcha($('recaptcha_div'));
		}
	}).send();
		
	
	return false;
}

function showRecaptcha(element) {

	if ( Cubix.Feedback.wrapper == 'contactme_wrapper' ) {
		RecaptchaOptions = {
			theme : 'white'
		};
	}
	else {
		RecaptchaOptions = {
			theme : 'red'
		};
	}

	Recaptcha.create("6LdcRr8SAAAAAJH8_-gPTzJlbVZV9RE2Kj7zekcw", element, RecaptchaOptions);
}

Cubix.Feedback.Send = function (e) {
	e.stop();
	
	var overlay = new Cubix.Overlay($$('.' + Cubix.Feedback.wrapper)[0], { loader: _st('loader-circular-tiny.gif'), position: '50%' });
	$$('.' + Cubix.Feedback.wrapper)[0].setStyle('position', 'absolute');
	overlay.disable();
	
	function getErrorElement(el) {
		var error = el.getNext('.error');
		if ( error ) return error;
		
		return new Element('div', { 'class': 'error' }).inject(el, 'after');
	}
	
	this.set('send', {
		onSuccess: function (resp) {
			resp = JSON.decode(resp);
			
			this.getElements('.error').destroy();
			this.getElements('.invalid').removeClass('invalid');
			
			if ( resp.status == 'error' ) {
				var c = false;
				for ( var field in resp.msgs ) {
					var input = this.getElement('*[name=' + field + ']');
					input.addClass('invalid');
					getErrorElement(input).set('html', resp.msgs[field]);

					if ( field == 'captcha' ) {
						c = true;
					}
				}
				
				
				// Regenerate new captcha
				var img = this.getElement('img'),
					src = img.get('src');
				
				src = src.substr(0, src.indexOf('?'));
				src = src + '?' + Math.random();
				
				img.set('src', src);
				$('f-captcha').set('value', '');

				/*Recaptcha.reload();

				if ( c ) {
					$('recaptcha_response_field').setStyle('background-color', '#FFF6F0');
				}
				else {
					$('recaptcha_response_field').setStyle('background-color', '#fff');
				}*/
			}
			else if ( resp.status == 'success' ) {
				this.getParent().set('html', resp.msg);

				var close_btn = new Element('div', {
					html: '',
					'class': 'cm-close-btn'
				}).inject($$('.' + Cubix.Feedback.wrapper)[0]);

				close_btn.addEvent('click', function() {
					$$('.' + Cubix.Feedback.wrapper).destroy();
					$$('.overlay').destroy();
				});

				//Recaptcha.destroy();
			}
			overlay.enable();
		}.bind(this)
	});
	
	this.send();
}
/* <-- */
