Cubix.PrivateMessaging = {};
Cubix.PrivateMessaging.isLoggedIn = false;

/* --> LocationHash */
Cubix.LocationHash = {};

Cubix.LocationHash.Set = function (hash) {
	if ( hash.length ) {
		document.location.hash = '#' + hash;
	}
	else {
		document.location.hash = '';
	}
	
	return false;
}

Cubix.LocationHash.Parse = function () {
	var hash = document.location.hash.substring(1);
	
	if ( ! hash.length ) {
		return {};
	}
	
	var params = hash.split(';');
	
	var filter = '';
	
	var not_array_params = ['page', 'city', 'action_type'];
	
	params.each(function (param) {
		var key_value = param.split('=');
		
		var key = key_value[0];
		var val = key_value[1];
		
		if ( val === undefined ) return;
		
		val = val.split(',');
				
		val.each(function(it) {
			filter += key + ( ( ! not_array_params.contains(key) ) ? '[]=' : '=' ) + it + '&';
		});
	});
	
	return filter;
}

Cubix.LocationHash.Make = function (params) {
	
	var hash = '';
	var sep = ';';
	var value_glue = ',';
	
	var places = $$('#la-header-wrp .header');
	
	var map = {};
	places.each(function(place) {
		var inputs = place.getElements('input[type=text]');
		inputs.append(place.getElements('input[type=hidden]'));
		//inputs.append(place.getElements('input[type=text]'));
		inputs.each(function(input) {
			if ( input.get('value').length ) {
				var index = input.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([input.get('value')]);
			}
		});
		
		var selects = place.getElements('select');
		selects.each(function(select) {
			
			if ( select.getSelected().get('value') != 0 ) {
				var index = select.get('name').replace('[]', '');
				if ( map[index] === undefined ) map[index] = new Array();
				map[index].append([select.getSelected().get('value')]);
			}
		});
	});
	
	map = $merge(map, params);
	
	for (var i in map) {
		hash += i + '=' + map[i].join(value_glue) + sep;
	}
	
	return hash;
}
/* <-- */

/* --> HashController */
Cubix.HashController = {
	_current: '',
	
	init: function () {
		setInterval('Cubix.HashController.check()', 100);
	},
	
	check: function () {
		var hash = document.location.hash.substring(1);
		
		if (hash != this._current) {
			this._current = hash;
					
			var data = Cubix.LocationHash.Parse();
			
			
			Cubix.LatestActions.Load(data);
			
			Cubix.LatestActions.LoadHeader(data);
		}
	}
};
/* <-- */

Cubix.Filter = {};
Cubix.Filter.Set = function (filter) {
		
	Cubix.LocationHash.Set(Cubix.LocationHash.Make(filter));

	return false;
}

Cubix.LatestActions = {};

Cubix.LatestActions.container = 'la-escorts-wrp';
Cubix.LatestActions.header_container = 'la-header-wrp';

Cubix.LatestActions.header_url = '';
Cubix.LatestActions.list_url = '';

Cubix.LatestActions.Load = function (data) {
	
	var url = Cubix.LatestActions.list_url;
	
	var overlay = new Cubix.Overlay($(Cubix.LatestActions.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'POST',
		data: data,
		onSuccess: function (resp) {
			
			$(Cubix.LatestActions.container).set('html', resp);
			
			overlay.enable();	
		}
	}).send();
	
	return false;
}

Cubix.LatestActions.LoadHeader = function (data) {
	
	var url = Cubix.LatestActions.header_url;
	
	var overlay = new Cubix.Overlay($(Cubix.LatestActions.header_container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'POST',
		data: data,
		onSuccess: function (resp) {			
			$(Cubix.LatestActions.header_container).set('html', resp);
			overlay.enable();
			
			Cubix.HashController.InitAll();			
		}
	}).send();
	
	return false;
}

Cubix.LatestActions.InitFilters = function(){
	$$('#la-header-wrp select').addEvent('change', function(e){
		e.stop();
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	});
};

Cubix.HashController.InitAll = function(){
	Cubix.HashController.init();
	
	Cubix.LatestActions.InitFilters();
};

window.addEvent('domready', function () {
	Cubix.HashController.InitAll();
	
	/*if ( ! Cubix.LocationHash.Parse().length ) {
		Cubix.LocationHash.Set(Cubix.LocationHash.Make());
	}*/
});
