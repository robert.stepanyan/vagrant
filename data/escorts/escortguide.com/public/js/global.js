var Cubix = {};

Cubix.Lang = {};

Cubix.ShowTerms = function (link) {
	window.open(link, 'showTerms', 'menubar=no, resizable=yes, status=no, scrollbars=yes, toolbar=no, width=640, height=480');
	return false;
}

var _log = function (data) {
	if ( console && console.log != undefined ) {
		console.log(data);
	}
}

var _st = function (object) {
	var parts = object.split('.');
	if ( parts.length < 2 ) {
		return _log('invalid parameter, must contain filename with extension');
	}

	var ext = parts[parts.length - 1];
	
	var base_url = 'http://st.beneluxxx.com',
		prefix = '';

	switch ( ext ) {
		case 'jpg':
		case 'gif':
		case 'png':
			prefix = '/img/';
			break;
		case 'css':
			prefix = '/css/';
			break;
		case 'js':
			prefix = '/js/';
			break;
		default:
			return _log('invalid extension "' + ext + '"');
	}

	url = base_url + prefix + object;

	return url;
}

// Taken from viewed-escorts.js
/* --> Viewed Escorts */
Cubix.Viewed = {};

Cubix.Viewed.url = ''; // Must be set from php
Cubix.Viewed.container = '';

Cubix.initMoreLessButton = function() {
	if ( ! $$('a.more-less-button').length ) return false;

	$$('a.more-less-button').addEvent('click', function(e){
		e.stop();

		var more_cities_box = $$('div.more-cities-box')[0];

		var curr_title = this.get('html');

		if ( this.hasClass('more') ) {
			more_cities_box.removeClass('none');
			this.removeClass('more').addClass('less');
		} else if ( this.hasClass('less') ) {
			more_cities_box.addClass('none');
			this.removeClass('less').addClass('more');
		}

		this.set('html', this.get('rel')).set('rel', curr_title);
	});
};

window.addEvent('domready', function() {

	Cubix.initMoreLessButton();

	/******************* support ***********************/
	if ($defined($('support_popup')))
	{
		if (Cookie.read('support_close') != 'done')
		{
			if ($('support_popup').hasClass('none'))
				$('support_popup').removeClass('none');
		}
		else
		{
			if (!$('support_popup').hasClass('none'))
				$('support_popup').addClass('none');
		}
		
		$('support_popup_x').addEvent('click', function(e) {
			e.stop();
			
			Cookie.write('support_close', 'done');
			
			$('support_popup').addClass('none');
		})
		
		if ($defined($$('.reg_pop')))
		{
			$$('.reg_pop').addEvent('click', function(e) {
				e.stop();

				Cubix.Popup.Show('500', '520');
			});
		}
	}
	/**********************************************/
	
	if ( Cookie.read('email_collecting') != 'done' && ! Cookie.read('ec_session_closed') && ! headerVars.signedIn && Cookie.read('ec_count') == 3 && ! $('splash') ) {
		Cubix.EmailCollectingPopup.url = '/index/email-collecting';
		Cubix.EmailCollectingPopup.Show(100, 570);
	}
	
	if ( ! $(Cubix.Viewed.container) || ! Cubix.Viewed.url ) return;
	
	new Request({
		url: Cubix.Viewed.url,
		method: 'get',
		onSuccess: function (resp) {
			if(resp.contains('rapper'))
			{
				$(Cubix.Viewed.container).setStyle('opacity', 0);
				var myFx = new Fx.Tween($(Cubix.Viewed.container), {
					duration: 400,
					onComplete: function() {
						$(Cubix.Viewed.container).set('html', resp);
						$(Cubix.Viewed.container).tween('opacity', 0, 1);
					}
				});
				myFx.start('height', '137');
			}
		}
	}).send();	
});

var resizeProfile = function() {
	if ( $$('.cTab')[0] ) {
	
		var leftSide = $('left');
		var profile = $('profile-container');
		var comments = $$('.cTab')[0];

		if ( ! profile.get('rel') ) {
			profile.set('rel', profile.getSize().y);
		}

		var commentsHeight = comments.setStyles({
			visibility: 'hidden',
			display: 'block'
		}).getHeight();

		comments.setStyles({
			visibility: null,
			display: null
		})	

		var profileHeight = 0;

		profileHeight = (profile.get('rel')*1) + (commentsHeight*1) - 4700;


		if ( leftSide.getSize().y > profileHeight ) {
			profile.setStyle('height', left.getSize().y);
		} else {
			profile.setStyle('height', profileHeight);
		}
	} else {
		var $leftHeight = $("left").getCoordinates().height;
		var $profileHeight = $("profile-container").getCoordinates().height - 4600;

		if ( $leftHeight > $profileHeight ) {
			$("profile-container").setStyle('height', $leftHeight);
		} else {
			$("profile-container").setStyle('height', $profileHeight);
		}
	}
};

var initGallery = function() {
	
	Slimbox.scanPage();
	
	/*$$("#gallery img, #gallery .lupa").removeEvents('click');
	$$("#gallery img, #gallery .lupa").addEvent("click", function() {
		
		if ( this.hasClass('noclick') ) return;
		
		if ( $('image') ) {
			$('image').destroy();
		}
		
		if ( this.hasClass('lupa') ) {
			var src = this.getPrevious('img').get('src');
		} else {
			var src = this.get('src');
		}
		
		if ( src.indexOf('_pp') != -1 )
			src = src.replace("_pp", "_orig");
		else {
			src = src.replace("_pl", "_orig");
		}
		
		im = new Image();
		im.src = src;
		im.onload = function () {
			
			h = im.height;
			w = im.width;

			oh = $(window).getSize().y;
			ow = $(window).getSize().x;

			if ( oh < h ) {
				origH = h;
				h = oh - 40;
				w = h * w / origH;
			}

			if ( ow < w ) {
				origW = w;
				w = ow - 40;
				h = h * w / origW;
			}

			leftt = (w + 20) / 2;
			topr = (h + 20) / 2;

			var divImage = new Element('div', {
				id: 'image',
				styles: {
					'margin-left': -leftt,
					'margin-top': -topr,
					'display': 'block',
					'opacity': '0'
				}
			}).inject($(document.body));

			new Element('a', {
				'html': 'x'
			}).inject(divImage);

			new Element('img', {
				'src': src,
				'width': w,
				'height': h
			}).addEvent('contextmenu', function(e) {
				e.stop();
			}).inject(divImage);

			divImage.addEvent("click", function() {
				$(this).destroy();
			});
			
			divImage.set('tween', { duration: 'short' }).fade('in');
		}.bind(this);
	});*/
};