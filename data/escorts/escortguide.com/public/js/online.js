/* --> Photos */
Cubix.Online = {};

Cubix.Online.container = 'online-widget';

Cubix.Online.Load = function (page) {
	if ( undefined == page || page < 1 ) page = 1;
	var url = '/ajax-online?page=' + page;
	Cubix.Online.Show(url);
	
	return false;
}

Cubix.Online.Show = function (url) {
	var overlay = new Cubix.Overlay($(Cubix.Online.container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			$(Cubix.Online.container).set('html', resp);
			overlay.enable();
            if ( resp.length == 0 ){
                $('online-widget').setStyle('display','none');
            }    
		}
	}).send();
}
