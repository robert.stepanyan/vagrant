var fs = require('fs');
var config = require('../configs/config.js').get();
function SettingsManager() {
	var settings = {};
	
	this.init = function()
	{
		fs.readFile(config.common.userSettingsBackupPath, function(err, data) {
			if ( err ) return;
			try {
			settings = JSON.parse(data);
			} catch(err) {
				console.log('error - ' + err);
			}
		});
	}
	
	this.addOpenDialog = function(userId, dialogId)
	{
		if ( typeof settings[userId] == 'undefined'  ) {
			settings[userId] = {};
		}
		if ( typeof settings[userId]['openDialogs'] == 'undefined' ) {
			settings[userId]['openDialogs'] = [];
		}
		
		var openDialogs = settings[userId]['openDialogs'];
		for(var i = 0; i <= openDialogs.length; i++) {
			if (openDialogs[i] == dialogId) {
				return;
			}
		}
		
		settings[userId]['openDialogs'].push(dialogId);
	}
	
	this.removeOpenDialog = function(userId, dialogId) 
	{
		if ( settings[userId] && settings[userId]['openDialogs'] ) {
			for(i in settings[userId]['openDialogs']) {
				if (settings[userId]['openDialogs'][i] == dialogId ) {
					settings[userId]['openDialogs'].splice(i, 1);
				}
			}
		}
	}
	
	this.getOpenDialogs = function(userId)
	{
		if ( settings[userId]['openDialogs'] && settings[userId]['openDialogs'].length > 50 ) {
			settings[userId]['openDialogs'] = [];
		}
		
		if ( typeof settings[userId] != 'undefined' && typeof settings[userId]['openDialogs'] != 'undefined' && settings[userId]['openDialogs'].length  ) {
			return settings[userId]['openDialogs']
		} else {
			return false;
		}
	}
	
	this.addActiveDialog = function(userId, dialogId)
	{
		var sett = this.get(userId);
		if (!sett['activeDialogs']) {
			sett['activeDialogs'] = [];
		}

		if (sett['activeDialogs'].indexOf(dialogId) !== -1) {
			return;
		}

		sett['activeDialogs'].push(dialogId);
		this.set(userId, sett);
	}
	
	this.removeActiveDialog = function(userId, dialogId)
	{
		if ( settings[userId] && settings[userId]['activeDialogs'] ) {	
			for(i in settings[userId]['activeDialogs']) {
				if (settings[userId]['activeDialogs'][i] == dialogId) {
					settings[userId]['activeDialogs'].splice(i, 1);
				}
			}
		}
	}
	
	this.getActiveDialogs = function(userId)
	{
		var sett = this.get(userId);
		if ( typeof sett['activeDialogs'] != 'undefined' ) {
			return sett['activeDialogs'];
		} else {
			 return [];
		}
	}

	this.resetActiveDialogs = function(userId)
	{
		var sett = this.get(userId);
		sett['activeDialogs'] = [];
		this.set(userId, sett);
	}
	
	//availability[1, 0]
	this.setAvailability = function(userId, availability) {
		if ( availability != 0 ) {
			availability = 1;
		} else {
			availability = 0;
		}
		 
		if ( typeof settings[userId] == 'undefined' ) {
			settings[userId] = {};
		}
		settings[userId]['availability'] = availability;
	}
	
	this.getAvailability = function(userId) {
		if ( typeof settings[userId] == 'undefined' || typeof settings[userId]['availability'] == 'undefined' ) {
			return 1;
		} else {
			return settings[userId]['availability'];
		}
	}
	
	this.setShowOnlyEscorts = function(userId, value) 
	{
		var sett = this.get(userId);
		sett['showOnlyEscorts'] = value;
		this.set(userId, sett);
	}
	
	this.getShowOnlyEscorts = function(userId) 
	{
		var sett = this.get(userId);
		if ( typeof sett['showOnlyEscorts'] != 'undefined' ) {
			return sett['showOnlyEscorts'];
		} else {
			return 0;
		}
	}
	
	this.setKeepListClosed = function(userId, value) 
	{
		var sett = this.get(userId);
		sett['keepListClosed'] = value;
		this.set(userId, sett);
	}
	
	this.setInvisibility = function(userId, value) 
	{
		var sett = this.get(userId);
		sett['invisibility'] = value;
		this.set(userId, sett);
	}
	
	this.getInvisibility = function(userId) 
	{
		var sett = this.get(userId);
		if ( typeof sett['invisibility'] != 'undefined' ) {
			return sett['invisibility'];
		} else {
			return 0;
		}
	}
	
	this.addBlockedUser = function(userId, blockedUserId)
	{
		var sett = this.get(userId);
		
		if ( typeof sett['blockedUsers'] == 'undefined' ) {
			sett['blockedUsers'] = [];
		}
		
		for(var i = 0; i <= sett['blockedUsers'].length; i++) {
			if ( sett['blockedUsers'][i] == blockedUserId ) {
				return;
			}
		}
		
		sett['blockedUsers'].push(blockedUserId);
	}
	
	this.removeBlockedUser = function(userId, blockedUserId) 
	{
		var sett = this.get(userId);
		
		if ( sett['blockedUsers'] ) {
			for(i in sett['blockedUsers']) {
				if (sett['blockedUsers'][i] == blockedUserId ) {
					sett['blockedUsers'].splice(i, 1);
				}
			}
		}
	}
	
	this.getBlockedUsers = function(userId)
	{
		var sett = this.get(userId);
		
		if ( typeof sett['blockedUsers'] != 'undefined' && sett['blockedUsers'].length  ) {
			return sett['blockedUsers']
		} else {
			return [];
		}
	}
	
	this.get = function(userId)
	{
		if ( typeof settings[userId] == 'undefined' ) {
			settings[userId] = {
				'blockedUsers': [],
				'activeDialogs': [],
				'openDialogs': []
			};
			
			return settings[userId];
		} else {
			return settings[userId];
		}
	}
	
	this.set = function(userId, sett)
	{
		if ( typeof settings[userId] == 'undefined' ) {
			settings[userId] = {};
		}
		settings[userId] = sett;
		
	}
	
	this.startSettingsBackup = function(callback)
	{
		
		setInterval(function() {
			fs.writeFile(config.common.userSettingsBackupPath, JSON.stringify(settings), function(err) {
				callback();
			});
		}, 60 * 60 * 1000);
	}
	
	this.init();
}

function get() {
	return new SettingsManager();
}

exports.get = get;