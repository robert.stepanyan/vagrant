var config = require('../configs/config.js').get(),
	app = require('http').createServer(handler).listen(8877, '127.0.0.1'),
	
	io = require('socket.io').listen(app, { log: false }),
	crypto = require('crypto'),
	url = require('url'),

	userManager = require("./userManager.js").get(),
	messageManager = require("./messageManager.js").get(),
	settingsManager = require("./settingsManager.js").get(),
	bridge = require("./bridge.js");


function handler(request, response) {
	var queryData = url.parse(request.url, true)
	
	var query = queryData.query;
	
	//Changing availability
	if ( queryData.pathname == '/node-chat/change-availability' ) {
		
		var uid = query.userId
		availability = query.availability,
		hash = query.hash;
		
		if ( ! uid || ! availability || ! hash || crypto.createHash('md5').update(uid + '-' + availability + '-' + config.common.key).digest("hex") != hash)  {
			response.writeHead(200, {"Content-Type": "text/plain"});
			response.end("sta=error");
			return;
		} 
		
		console.log('Changing availability for userId ' + uid + ' to ' + availability);
		
		settingsManager.setAvailability(uid, availability);
		
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("sta=ok");
	}
	
	//Get availability
	if ( queryData.pathname == '/node-chat/get-availability' ) {
		var uid = query.userId,
			hash = query.hash;
			
		if ( ! uid || crypto.createHash('md5').update(uid + '-' + config.common.key).digest("hex") != hash) {
			response.writeHead(200, {"Content-Type": "text/plain"});
			response.end("sta=error");
			return;
		}
		result =  JSON.stringify({availability : settingsManager.getAvailability(uid)});
		
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end(result);
	}
	
	//Get online escorts
	if ( queryData.pathname == '/node-chat/get-online-escorts' ) {
		userManager.getOnlineEscorts(function(userIds) {
			response.writeHead(200, {"Content-Type": "text/plain"});
			response.end(JSON.stringify(userIds));
		});
	}
}

settingsManager.startSettingsBackup(function() {
	console.log('User settings backup done');
});

userManager.startOfflineUsersCheck(function(userIds) {
	if ( userIds.length ) {
		io.sockets.emit('offline-users', userIds);
	}
});

messageManager.startBlackListedWordsSync(function() {
	console.log('Bl word sync is done.');
});

//Checking sessions. if session expired kick out user 
setInterval(function() {
	users = userManager.getUsersPrivateList();
	for( var userId in users ) {
		(function(userId) {
			bridge.getUserInfo(users[userId].info.sid, function(resp) {
					if ( ! resp.auth && users[userId] ) {
						for ( i = 0; i < users[userId].sockets.length; i++ ) {
							io.sockets.socket(users[userId].sockets[i]).disconnect('Loged off');
						}
						users[userId].sockets = [];
						userManager.addUserWithoutSockets(userId);
					}
			}, false);
		})(userId);
	}
}, config.common.sessionCheckInterval);

function start() {
	io.sockets.on('connection', function (socket) {
		socket.on('auth', function(data) {
			bridge.getUserInfo(data.sid, function(resp) {
				if ( ! resp.auth ) {
					socket.emit('auth-complete', {auth: false});
					socket.disconnect('unauthorized');
					return;
				}
				var userInfo = resp.data;
				userInfo.status = 'online';//Setting status to online
				userInfo.sid = data.sid;
				if ( userInfo.userType != 'escort' ) {
					userInfo.avatar = config.common.noPhotoUrl;
				}
				
				var availability = 1;
				if ( data.forceEnable ) {
					settingsManager.setAvailability(userInfo.userId, 1);
				} else {
					availability = settingsManager.getAvailability(userInfo.userId);
				}
				userInfo.settings = settingsManager.get(userInfo.userId);
				
				socket.emit('auth-complete', {auth: true, availability: availability, userInfo: userInfo});
				
				if ( availability ) {
					socket.userInfo = userInfo;
					
					var newUser = true;
					if ( userManager.getUser(userInfo.userId) ) {
						newUser = false;
					} 
					
					isPrivate = socket.userInfo.settings.invisibility ? true : false;
					
					userManager.addUser(socket.id, userInfo, isPrivate);
					
					//Emit new-user only if a new user. f.ex can login from another browser.
					if ( newUser && ! isPrivate ) {
						delete userInfo.settings;
						socket.broadcast.emit('new-user', userInfo);
					}

					
					var ou = userManager.getUsersPublicList();
					
					//Removing himself from list
					delete ou[userInfo.userId];
					
					var fakeUsers = [{"userId":1,"nickName":"Stephen Cooper","avatar":"https://robohash.org/etatquo.jpg?size=50x50&set=set1","status":"online"},{"userId":2,"nickName":"Benjamin Martin","avatar":"https://robohash.org/eaaliaslabore.bmp?size=50x50&set=set1","status":"online"},{"userId":3,"nickName":"Judy Hansen","avatar":"https://robohash.org/nequecupiditatequi.png?size=50x50&set=set1","status":"online"},{"userId":4,"nickName":"Wayne Watkins","avatar":"https://robohash.org/autemquiaipsa.jpg?size=50x50&set=set1","status":"offline"},{"userId":5,"nickName":"Frances Cruz","avatar":"https://robohash.org/facereeaab.png?size=50x50&set=set1","status":"online"},{"userId":6,"nickName":"Wanda Lane","avatar":"https://robohash.org/fugaautvitae.png?size=50x50&set=set1","status":"offline"},{"userId":7,"nickName":"Virginia Carroll","avatar":"https://robohash.org/explicabocumet.jpg?size=50x50&set=set1","status":"offline"},{"userId":8,"nickName":"Janet Morrison","avatar":"https://robohash.org/quasinonquisquam.png?size=50x50&set=set1","status":"offline"},{"userId":9,"nickName":"Donna Mason","avatar":"https://robohash.org/reiciendissequieaque.jpg?size=50x50&set=set1","status":"online"},{"userId":10,"nickName":"Cheryl Bailey","avatar":"https://robohash.org/necessitatibusdoloressint.png?size=50x50&set=set1","status":"online"},{"userId":11,"nickName":"Juan Stephens","avatar":"https://robohash.org/inciduntidaliquam.png?size=50x50&set=set1","status":"offline"},{"userId":12,"nickName":"Carlos Mccoy","avatar":"https://robohash.org/deseruntnatusperferendis.png?size=50x50&set=set1","status":"offline"},{"userId":13,"nickName":"Clarence Patterson","avatar":"https://robohash.org/perspiciatisdignissimosminus.jpg?size=50x50&set=set1","status":"offline"},{"userId":14,"nickName":"Jeffrey Murphy","avatar":"https://robohash.org/sedadrepellendus.png?size=50x50&set=set1","status":"offline"},{"userId":15,"nickName":"Judy Pierce","avatar":"https://robohash.org/modiipsavoluptas.bmp?size=50x50&set=set1","status":"offline"},{"userId":16,"nickName":"Louis Howard","avatar":"https://robohash.org/accusamusautipsa.png?size=50x50&set=set1","status":"offline"},{"userId":17,"nickName":"Christopher Grant","avatar":"https://robohash.org/quoscommodisimilique.jpg?size=50x50&set=set1","status":"online"},{"userId":18,"nickName":"Katherine Schmidt","avatar":"https://robohash.org/autemmolestiaequi.bmp?size=50x50&set=set1","status":"online"},{"userId":19,"nickName":"Randy Fowler","avatar":"https://robohash.org/aliasconsequunturquasi.bmp?size=50x50&set=set1","status":"offline"},{"userId":20,"nickName":"Shawn Davis","avatar":"https://robohash.org/quifugiatconsequatur.jpg?size=50x50&set=set1","status":"online"},{"userId":21,"nickName":"Albert Arnold","avatar":"https://robohash.org/laboriosamillummolestiae.bmp?size=50x50&set=set1","status":"online"},{"userId":22,"nickName":"Paul Flores","avatar":"https://robohash.org/consequaturdoloremducimus.bmp?size=50x50&set=set1","status":"online"},{"userId":23,"nickName":"Henry Porter","avatar":"https://robohash.org/istevoluptaset.bmp?size=50x50&set=set1","status":"offline"},{"userId":24,"nickName":"Michelle Ford","avatar":"https://robohash.org/voluptatenatusmolestiae.png?size=50x50&set=set1","status":"online"},{"userId":25,"nickName":"Richard Fisher","avatar":"https://robohash.org/namestaut.png?size=50x50&set=set1","status":"offline"},{"userId":26,"nickName":"Kelly Perry","avatar":"https://robohash.org/porroquiasequi.jpg?size=50x50&set=set1","status":"online"},{"userId":27,"nickName":"Deborah Baker","avatar":"https://robohash.org/voluptatesvoluptatumvoluptas.png?size=50x50&set=set1","status":"online"},{"userId":28,"nickName":"Jennifer Morrison","avatar":"https://robohash.org/enimdistinctiocorporis.png?size=50x50&set=set1","status":"offline"},{"userId":29,"nickName":"Peter Austin","avatar":"https://robohash.org/doloremminimaquasi.png?size=50x50&set=set1","status":"online"},{"userId":30,"nickName":"Raymond Hunt","avatar":"https://robohash.org/dolorumnecessitatibusab.png?size=50x50&set=set1","status":"online"},{"userId":31,"nickName":"Martha Stewart","avatar":"https://robohash.org/omnissequiest.png?size=50x50&set=set1","status":"online"},{"userId":32,"nickName":"Carl Berry","avatar":"https://robohash.org/molestiasnullafacilis.png?size=50x50&set=set1","status":"online"},{"userId":33,"nickName":"Dorothy Jacobs","avatar":"https://robohash.org/dolorecupiditatedolorem.jpg?size=50x50&set=set1","status":"offline"},{"userId":34,"nickName":"Kenneth Gardner","avatar":"https://robohash.org/placeatenimdolor.bmp?size=50x50&set=set1","status":"offline"},{"userId":35,"nickName":"Alice Harper","avatar":"https://robohash.org/quosdictasunt.bmp?size=50x50&set=set1","status":"online"},{"userId":36,"nickName":"Aaron Howell","avatar":"https://robohash.org/quoestlibero.jpg?size=50x50&set=set1","status":"online"},{"userId":37,"nickName":"Henry Mitchell","avatar":"https://robohash.org/optiomagnamvel.jpg?size=50x50&set=set1","status":"online"},{"userId":38,"nickName":"Stephanie Miller","avatar":"https://robohash.org/autmaximeimpedit.jpg?size=50x50&set=set1","status":"offline"},{"userId":39,"nickName":"Andrea Holmes","avatar":"https://robohash.org/quaeconsequaturomnis.jpg?size=50x50&set=set1","status":"online"},{"userId":40,"nickName":"Ernest Franklin","avatar":"https://robohash.org/dolorumquiporro.bmp?size=50x50&set=set1","status":"online"},{"userId":41,"nickName":"Earl Gilbert","avatar":"https://robohash.org/abplaceatlaboriosam.png?size=50x50&set=set1","status":"offline"},{"userId":42,"nickName":"Rachel Bailey","avatar":"https://robohash.org/perferendiseligendiet.jpg?size=50x50&set=set1","status":"online"},{"userId":43,"nickName":"Victor Ellis","avatar":"https://robohash.org/doloretomnis.png?size=50x50&set=set1","status":"offline"},{"userId":44,"nickName":"Dorothy Andrews","avatar":"https://robohash.org/delectusvoluptasvoluptatem.jpg?size=50x50&set=set1","status":"offline"},{"userId":45,"nickName":"Amy Patterson","avatar":"https://robohash.org/velitetid.jpg?size=50x50&set=set1","status":"offline"},{"userId":46,"nickName":"Scott Martinez","avatar":"https://robohash.org/doloremdoloresblanditiis.jpg?size=50x50&set=set1","status":"online"},{"userId":47,"nickName":"Maria Myers","avatar":"https://robohash.org/maioresdebitisut.bmp?size=50x50&set=set1","status":"online"},{"userId":48,"nickName":"Gerald Simpson","avatar":"https://robohash.org/necessitatibuseavoluptatem.bmp?size=50x50&set=set1","status":"online"},{"userId":49,"nickName":"Susan Hall","avatar":"https://robohash.org/namquosrecusandae.png?size=50x50&set=set1","status":"online"},{"userId":50,"nickName":"Jacqueline Ross","avatar":"https://robohash.org/numquamquibeatae.jpg?size=50x50&set=set1","status":"online"}];
					for(var i in fakeUsers) {
						if (typeof fakeUsers[i] === 'function') continue;
						ou[fakeUsers[i].userId] = fakeUsers[i];
					}
					socket.emit('online-users-list', ou);

					//Checking if has opened dialogs. 
					//if yes getting userInfo and emiting open-dialogs event
					var openDialogs = settingsManager.getOpenDialogs(userInfo.userId);
					var od = [];
					if ( openDialogs ) {
						userManager.getUsers(openDialogs, true, function(users) {
							for(var i = 0; i <= openDialogs.length- 1; i++) {
								userId = openDialogs[i];
								if ( users[userId] ) {
									od.push({
										'userId' : userId,
										'userInfo' : users[userId].info
									});
								}
							}
							activeDialogs = settingsManager.getActiveDialogs(userInfo.userId);
							socket.emit('open-dialogs', {dialogs : od, activeDialogs : activeDialogs});
						});
					}
					
					//Checking if has new messages. if yes emiting new-message event.
					messageManager.getNewMessagesCount(userInfo.userId, function(messages) {
						if ( messages && messages.length ) {
							userIds = [];
							//gathering user ids to call getUsers
							for ( var i = 0; i < messages.length; i++ ) {
								userIds.push(messages[i].userId);
							}
							//getting users and merging with messages
							userManager.getUsers(userIds, true, function(users) {
								for ( var i = 0; i < messages.length; i++ ) {
									if ( users[messages[i].userId] ) {
										messages[i].senderInfo = users[messages[i].userId].info;
									}
								}
								socket.emit('new-messages', messages);
							});
						}
					});
				} else {
					socket.disconnect('status:not-available');
				}
			});
		});
		
		socket.on('message-sent', function(messageData) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			

			var blockedUsers = settingsManager.getBlockedUsers(messageData.userId);
			
			if ( blockedUsers && blockedUsers.indexOf(socket.userInfo.userId) >= 0 ) {
				socket.emit('user-blocked-you', {dialogId : messageData.userId});
				
				return;
			}
			
			userManager.getUser(messageData.userId, true, function(user) {
				if ( user ) {
					
					var message = messageManager.clearMessage(messageData.message);
					
					if ( message.length > 0 ) {
						messageManager.checkMessage(message, function(res) {
							if ( res && res.length ) { // Message is in black listed words, don't send message. emit sender about it.
								socket.emit('bl-word', {dialogId : messageData.userId, word: res.join(', ')});
							} else {
								messageManager.storeMessage(socket.userInfo.userId, user.info.userId, message);
								sockets = user.sockets;

								newMessageData = {
									body : message,
									userId : socket.userInfo.userId,
									date : new Date().getTime()
								}
								for(var i = 0; i <= sockets.length - 1; i++) {
									io.sockets.socket(sockets[i]).emit('new-message', {message: newMessageData, senderData: socket.userInfo})
								}
							}
						});
					}
				}
			});
		});
		
		socket.on('dialog-opened', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			//showing todays history
			date = new Date();
			date.setHours(0);
			date.setMinutes(0);
			date.setSeconds(0);
			
			messageManager.getConversation(socket.userInfo.userId, data.userId, date, function(messages) {
				if ( messages.length ) {
					var senderInfo = userManager.getUser(data.userId, true, function(user) {
						socket.emit('message-history', {messages: messages, senderInfo : user.info});
					})
				}
				
			});
			
			settingsManager.addOpenDialog(socket.userInfo.userId, data.userId);

		});

		socket.on('dialog-closed', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager.removeOpenDialog(socket.userInfo.userId, data.userId);
			settingsManager.removeActiveDialog(socket.userInfo.userId, data.userId);
		});
		
		socket.on('dialog-activated', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager.addActiveDialog(socket.userInfo.userId, data.userId);
		});
		
		socket.on('dialog-deactivated', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			settingsManager.removeActiveDialog(socket.userInfo.userId, data.userId);
		});
		
		socket.on('availability-changed', function($availability) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			if ( $availability ) {
				settingsManager.setAvailability(socket.userInfo.userId, 1);
			} else {
				settingsManager.setAvailability(socket.userInfo.userId, 0);
				settingsManager.resetActiveDialogs(socket.userInfo.userId);
				//Emitting to all sockets about availability changed
				user = userManager.getUser(socket.userInfo.userId);
				
				if ( user ) {
					var sockets = [] ;
					for( var i = 0; i < user.sockets.length; i++ ) {
						sockets.push(user.sockets[i])	
					}

					for(var i = 0; i < sockets.length; i++) {
						io.sockets.socket(sockets[i]).emit('chat-off', {});
						io.sockets.socket(sockets[i]).disconnect('chat disabled');
					}
				}
			}
		});
		
		socket.on('change-options', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			switch(data.option) {
				case 'show-only-escorts' :
					settingsManager.setShowOnlyEscorts(socket.userInfo.userId, data.value);
					break;
				case 'keep-list-closed' :
					settingsManager.setKeepListClosed(socket.userInfo.userId, data.value);
					break;
				case 'invisibility' :
					settingsManager.setInvisibility(socket.userInfo.userId, data.value);
					
					if ( data.value ) {
						//If set invisibility = 1 emit users about his status and remove from public list
						socket.broadcast.emit('offline-users', [socket.userInfo.userId]);
						userManager.removeUserFromPublicList(socket.userInfo.userId);
					} else {
						socket.broadcast.emit('new-user', socket.userInfo);
						userManager.addUserToPublicList(socket.userInfo);
					}
					
					break;
			}
		});
		
		socket.on('block-user', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			settingsManager.addBlockedUser(socket.userInfo.userId, data.userId);
		});
		
		socket.on('unblock-user', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			settingsManager.removeBlockedUser(socket.userInfo.userId, data.userId);
		});
		
		socket.on('blocked-users', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			if ( typeof socket.userInfo.settings != 'undefined' && typeof socket.userInfo.settings.blockedUsers != 'undefined' ) {
				userManager.getUsers(socket.userInfo.settings.blockedUsers, true, function(blockedUsers) {
					var users = [];
					for(var userId in blockedUsers) {
						users.push(blockedUsers[userId].info);
					}

					socket.emit('blocked-users', users);
				});
			}
		});
		
		socket.on('conversation-read', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			messageManager.markAsRead(socket.userInfo.userId, data.userId);
		});
		
		socket.on('typing-start', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			user = userManager.getUser(data.userId);
			if ( user ) {
				for(var i = 0; i <= user.sockets.length - 1; i++) {
					io.sockets.socket(user.sockets[i]).emit('typing-start', {userId : socket.userInfo.userId});
				}
			}
		});
		
		socket.on('typing-end', function(data) {
			if ( typeof socket.userInfo == 'undefined' ) return;
			
			user = userManager.getUser(data.userId);
			if ( user ) {
				for(var i = 0; i <= user.sockets.length - 1; i++) {
					io.sockets.socket(user.sockets[i]).emit('typing-end', {userId : socket.userInfo.userId});
				}
			}
		});
		
		socket.on('chat-with', function(data) {
			userManager.getUser(data.userId, true, function(user) {
				if (!user) return;

				settingsManager.addActiveDialog(socket.userInfo.userId, data.userId);
				socket.emit('open-dialogs', {
					dialogs : [{userId: data.userId, userInfo : user.info}], 
					activeDialogs : [data.userId]});
			});
		});
		
		socket.on('disconnect', function() {
			if ( ! socket.userInfo ) return;
			userManager.removeSocket(socket.userInfo.userId, socket.id);
		});
	});
}

exports.start = start;
