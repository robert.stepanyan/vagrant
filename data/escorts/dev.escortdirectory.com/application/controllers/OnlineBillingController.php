<?php

class OnlineBillingController extends Zend_Controller_Action
{
    /**
     * @return Zend_Session_Namespace
     */
    protected static function _getSession()
    {
        return new Zend_Session_Namespace('advertise');
    }

    public function init() {
        self::_getSession()->setExpirationSeconds(60 * 20); // 20 minutes
    }

    public $packages = [
        'single' => [
            ['title' => 'vip', 'price' => 27, 'days' => 15, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 49, 'days' => 30, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 99, 'days' => 90, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 20, 'days' => 15, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 34, 'days' => 30, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 75, 'days' => 90, 'country_type' => 'B'],
            ['title' => 'premium', 'price' => 15, 'days' => 15, 'country_type' => 'A'],
            ['title' => 'premium', 'price' => 29, 'days' => 30, 'country_type' => 'A'],
            ['title' => 'premium', 'price' => 75, 'days' => 90, 'country_type' => 'A'],
            ['title' => 'premium', 'price' => 10, 'days' => 15, 'country_type' => 'B'],
            ['title' => 'premium', 'price' => 19, 'days' => 30, 'country_type' => 'B'],
            ['title' => 'premium', 'price' => 51, 'days' => 90, 'country_type' => 'B'],
        ],
        'bulk' => [
            ['title' => 'vip', 'price' => 80, 'days' => 15, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 140, 'days' => 30, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 250, 'days' => 90, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 59, 'days' => 15, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 95, 'days' => 30, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 200, 'days' => 90, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 159, 'days' => 15, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 275, 'days' => 30, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 580, 'days' => 90, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 117, 'days' => 15, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 185, 'days' => 30, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 380, 'days' => 90, 'country_type' => 'B'],
        ],
        'banner' => [
            ['title' => '30', 'price' => 50, 'days' => 90],
            ['title' => '90', 'price' => 120, 'days' => 90],
            ['title' => 'vip', 'price' => 30, 'days' => null ],
        ],
        'other' => [
            ['title' => 'vip', 'price' => 25, 'days' => 30, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 48, 'days' => 90, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 20, 'days' => 30, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 60, 'days' => 90, 'country_type' => 'A'],
        ]
    ];

    public function indexAction()
    {

    }

    public function advertiseAction()
    {
        $message = self::_getSession()->message;

        if(!empty($message)) {
            $this->view->message = $message;
            self::_getSession()->message = null;
        }

        $this->view->packages = $this->packages;
    }

    public function generateMmgLinkAction()
    {
        $this->view->layout()->disableLayout();
        $request = $this->_request;

        if (!$this->_request->isPost() && !isint($request->id)) die("Invalid Request");

        $mmgBill = new Model_MmgBillAPIV2();
        $id = $this->_getParam('id');
        $country_id = $this->_getParam('country');
        $city_id = $this->_getParam('city');

        $key = explode('-' , $id) [0];
        $index = explode('-' ,$id) [1];

        $package = $this->packages[$key][$index];
        $package['country'] = (new Model_Countries)->getCountryByID($country_id)->country_title;

        if($city_id) {
            $package['city'] = (new Model_Cities)->getById($city_id)->title;
        }

        self::_getSession()->package = $package;

        if(APPLICATION_ENV == 'production') {
            $postback_url = 'http://dev.escortdirectory.com';
        }else{
            $postback_url = 'http://dev.escortdirectory2.com.test';
        }

        $hosted_url = $mmgBill->getHostedPageUrl($package['price'], time(), $postback_url.$this->view->getLink('advertise-mmg-response'));

        if (filter_var($hosted_url, FILTER_VALIDATE_URL) === FALSE) {
            $status = 'not-valid-url';
        }else{
            $status = 'success';
        }

        die(json_encode(array('status' => $status, 'url' =>  $hosted_url, 'package' => $package)));
    }

    public function advertiseMmgResponseAction()
    {
        $request = $this->_request;
        $payment_result = [];
        $package = self::_getSession()->package;

        self::_getSession()->package = null;
        if(empty($package)) return $this->_redirect('/advertise');

        if (isset($request->txn_status) and $request->txn_status == 'APPROVED') {
            $payment_result['status'] = $request->txn_status;
            $payment_result['merchant'] = $request->mid;
            $payment_result['mmg'] = $request->ti_mmg;
            $payment_result['ti_mer'] = $request->ti_mer;
            $payment_result['amount'] = $package['price'];

            if ($request->mmg_errno != '0000')
                $payment_result['error'] = $request->mmg_errno;

        } else {
            $payment_result['status'] = 'REJECTED';
        }


        $this->view->payment_status = $payment_result['status'];
        $this->view->payment_result = $payment_result;
    }

    public function advertisePaymentEmailAction(){

        $request = $this->_request;

        $payment_result = (isset($request->payment_data) && !empty($request->payment_data)) ? unserialize($request->payment_data) : [];
        $payment_result['email'] = isset($request->email) ? $request->email : '- - -';
        $payment_result['Link'] = isset($request->link) ? $request->link : '- - -';

        $current_user = Model_Users::getCurrent();

        if(!empty($current_user)){

            if ( $current_user->user_type == 'agency' ) {
                $payment_result['User'] = $current_user->agency_data['agency_id'];
            }
            else if ( $current_user->user_type == 'escort' ) {
                $payment_result['User'] = $current_user->escort_data['escort_id'];
            }
        }else{
            $payment_result['User'] = 'Was not authenticated';
        }

        $payment_details_string = '<ul>';
        foreach($payment_result as $key => $val) {
            $payment_details_string .= "<li> <b> $key </b> $val </li>";
        }
        $payment_details_string .= '</ul>';

        if(APPLICATION_ENV == 'production') {
            $to = 'sales@escortdirectory.com';
        }else{
            $to = 'edhovhannisyan97@gmail.com';
        }

        $mail_sent = Cubix_Email::sendTemplate('payment_details', $to, array(
            'details' => $payment_details_string
        ));

        if($mail_sent){
            $message = [
                'type' => "success",
                'text' => __('email_send_success')
            ];
        }else{
            $message = [
                'type' => "danger",
                'text' => __('email_send_error')
            ];
        }

        self::_getSession()->message = $message;
        self::_getSession()->package = null;
        $this->_redirect('/advertise');
    }

}