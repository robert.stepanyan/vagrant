<?php
class VerifyController extends Zend_Controller_Action
{
	/**
	 * @var Model_Escorts
	 */
	public $model;

	/**
	 * @var Model_EscortItem
	 */
	public $escort;

	/**
	 * @var Model_VerifyRequests
	 */
	public $verifyRequests;

	public function init()
	{

		$cache = Zend_Registry::get('cache');
		$this->view->user = $this->user = Model_Users::getCurrent();

        if ( ! $this->user ) {

            if($this->getRequest()->isXmlHttpRequest()){
                header("HTTP/1.1 403 Forbidden");
            }else{
                $this->_redirect($this->view->getLink('signin'));
            }

            return;
        }

		$this->verifyRequests = new Model_VerifyRequests();
		$this->model = new Model_Escorts();
		$cache_key =  'v2_user_pva_' . $this->user->id;

		if ( $this->user->isAgency() ) {
			if ( ! $agency = $cache->load($cache_key) ){
				$agency = $this->user->getAgency();
				$cache->save($agency, $cache_key, array(), 300);
			}

			$this->agency = $this->view->agency = $agency;
			//$this->view->agency_escorts = Model_Escorts::getAgencyEscorts($agency->id);
		}
		else if ( $this->user->isEscort() ) {
			if ( ! $escort = $cache->load($cache_key) ) {
				$escort = $this->user->getEscort();
				$cache->save($escort, $cache_key, array(), 300);
			}

			$this->escort = $this->view->escort = $escort;
		}
	}

	protected function _check()
	{
		// Checks if there is an active request
	
		if ( $this->escort->isVerified() ) {
			//$this->_forward('confirmed');
			$this->_helper->viewRenderer->setScriptAction('confirmed');
		}

		if ( $this->activeRequest = $this->escort->getVerifyRequest() ) {
			$this->view->request = $this->activeRequest;
			if ( $this->activeRequest->isPending() ) {
				//$this->_forward('pending');
				$this->_helper->viewRenderer->setScriptAction('pending');
			}
			elseif ( $this->activeRequest->isConfirmed() ) {
				$this->_helper->viewRenderer->setScriptAction('confirmed');
				//$this->_forward('confirmed');
			}
		
			return TRUE;
		}

		$lastRequest = $this->escort->getLastRequest();
		$this->view->lastRequest = $lastRequest;
		return FALSE;
	}

	public function successAction()
	{
		$this->view->layout()->disableLayout();
	}

	public function startAction()
	{
        if ( $this->user->isEscort() ) {

            $escort = $this->escort = $this->user->getEscort();
            $escort_id = $this->view->escort_id = $escort->id;
            $this->view->showname = $escort->showname;

            $this->_check();
        }

		$this->view->layout()->disableLayout();
	}
	
	public function escortsAction()
	{
		$this->view->layout()->disableLayout();
		
		
		if ( ! $this->user->isAgency() ) die;

		$this->view->s_showname = '';
	
		
		$per_page = $this->_request->getParam('per_page', 10);
		if ( !$per_page ) {
			$per_page = 10;
		}

		$escorts_page = $this->view->escorts_page = $this->_getParam('escorts_page', 1);

		$data = $this->agency->getEscortsPerPage($escorts_page, $per_page, 1, false);
		
		$this->view->count = $data['escorts_count'];
		$this->view->escorts_per_page = $per_page;

		$this->view->escorts = $data['escorts'];
		$this->view->agency_id = $this->agency->getId();
        $this->view->current_page = $cur_page;

	}
	public function ajaxEscortsAction()
	{
		
		$this->view->layout()->disableLayout();
		
		if ( ! $this->user->isAgency() ) die("This page is available only for Agencies");

		$this->view->s_showname = '';

		$per_page = $this->_request->getParam('per_page', 10);
		if ( !$per_page ) {
			$per_page = 10;
		}
		
		if($this->_request->getParam('search_content')){
			$searched_word = $this->_request->getParam('search_content');
		}else{
			$searched_word = '';
		}
		
		$escorts_page = $this->view->escorts_page = $this->_getParam('escorts_page', 1);
		$data = $this->agency->getEscortsPerPage($escorts_page, $per_page, 1, false, $searched_word);
		$this->view->count = $data['escorts_count'];
		$this->view->escorts_per_page = $per_page;
		$this->view->did_search = true;

		$this->view->escorts = $data['escorts'];
		$this->view->searched_word = $searched_word;
		$this->view->agency_id = $this->agency->getId();
		$this->view->current_page = $this->_getParam('escorts_page', 1);
		$this->_helper->viewRenderer->setScriptAction('escorts');
		
	}
	
	public function idcardAction()
	{
		$this->view->layout()->disableLayout();
	
		if ( $this->user->isAgency() ) {
		
			$escort = $this->escort = $this->model->getById($_POST['escort_id']);
			$escort_id = $this->view->escort_id = $escort->id;
			$this->view->showname = $escort->showname;
			$this->_check();
		} else if ( $this->user->isEscort() ) {
			
			$escort = $this->escort = $this->user->getEscort();
			$escort_id = $this->view->escort_id = $escort->id;
			$this->view->showname = $escort->showname;

			$this->_check();
		}
	
		if(isset($_POST['choose_escort'])){
			return;
		}
		if ( $this->_request->isPost() ) {
			
			$hashes = $this->_request->hash;

			if (!is_array($hashes) && !is_null($hashes)) {
				$hashes = [$hashes];
			}

			$exts = $this->_request->ext;
			$comments = $this->_request->comment;
			
			$validator = new Cubix_Validator();
			
			if ( ! $escort_id ) {
				$validator->setError('escort_id', Cubix_I18n::translate('escort_id_required'));
			}

			if ( ! count($hashes) ) {
				$validator->setError('photos', Cubix_I18n::translate('id_card_photos_required'));
			}
			
			$lastRequest = $this->escort->getLastRequest();
				
			if($lastRequest){
				$reasons = explode(',', $lastRequest->reason_ids); //need video
				if(in_array(10, $reasons)){
					$hasVideo = 0;
					foreach ( $hashes as $hash ) {
						if($exts[$hash][0] == 'flv'){
							$hasVideo = 1;
						}
					}		
					if(!$hasVideo){
						$validator->setError('video', Cubix_I18n::translate('video_is_required'));	
					}
				}
			}

			if ( ! $validator->isValid() ) {
				die( json_encode($validator->getStatus() ));
			} else {
				
				$item = new Model_VerifyRequest(array(
					'type' => Model_VerifyRequests::TYPE_IDCARD,
					'escort_id' => $escort_id
					//'inform_method' => $session->inform_method,
					//'phone' => $session->phone,
					//'email' => $session->email
					// status default PENDING
				));
				
				$save_data = array(
					'type' => Model_VerifyRequests::TYPE_IDCARD,
					'escort_id' => $escort_id
				);

				$item = $this->verifyRequests->save($save_data);

				foreach ( $hashes as $hash ) {
					
					$is_video = 0;
					if($exts[$hash][0] == 'flv'){
						$is_video = 1;
					}

					$item->addPhoto(array(
						'hash' => $hash,
						'ext' => $exts[$hash][0],
						'comment' => $comments[$hash][0],
						'is_video' => $is_video,
					));
				}

				die(json_encode($validator->getStatus()));
			}
		}
	}

	public function uploadPhotosAction()
	{
		try {
			if($_SERVER['CONTENT_LENGTH'] > 1500000){
				echo json_encode(array(
					'status' => 0,
					'error' => Cubix_I18n::translate('sys_error_upload_img_to_large')
				));
				die;
			}
			$model = new Model_Escort_Photos();
			$response = $model->upload();
			$escort_id = (int) $_GET['escortid'];

            $images = new Cubix_Images();
            $image = $images->save($response['tmp_name'], $escort_id. '/verify', $this->app_id, strtolower(@end(explode('.', $response['name']))));
            $image = new Cubix_Images_Entry($image);
            $image->setCatalogId($escort_id. '/verify');
            $image_url = $images->getUrl($image);

			echo json_encode(array(
				'status' => 1,
				'name' => "success",
				'escort_id' => $escort_id,
				//'photo_id' => $photo->id,
				'photo_url' => $image_url,
				'hash' => $image['hash'],
				'ext' => $image['ext'],
			));
			die;

		} catch ( Exception $e ) {
			echo json_encode(array(
				'status' => 0,
				'error' => $e->getMessage()
			));
			die;
		}
	}
	public function uploadVideoAction()
	{
		$escort_id = (int) $_GET['escortid'];
		$config =  Zend_Registry::get('videos_config');
		
		try {
			$model = new Model_Escort_Photos();
			$response = $model->upload();

			if($response['finish'] && $response['error'] === 0 ){
					$video_ftp = new Cubix_VideosCommon();

					$video_hash = uniqid().'_idcard';
					$video = $response['tmp_name'];
					
					$video_model = new Cubix_ParseVideo($video,$escort_id,$config);
					
					$video_ext = explode('.', $response['name']);
					$video_ext = strtolower(end($video_ext));
					
					if($video_ext != 'flv'){
						$video = $video_model->ConvertToFlv();
						$video_ext = 'flv';
					}

					$name = $video_hash.'.'.$video_ext;
					if($video_ftp->_storeToPicVideo($video, $escort_id, $name)){
						echo json_encode(array(
							'status' => 1,
							'name' => "success",
							'escort_id' => $escort_id,
							'is_video' => 1,
							'hash' => $video_hash,
							'ext' => $video_ext
						));
						die;
					}
			}

		} catch ( Exception $e ) {
			echo json_encode(array(
				'status' => 0,
				'error' => $e->getMessage(),
                'unhandled_exception' => true
			));
			die;
		}

		Cubix_Videos::showResponse($response);
		die;	
	}
	
}
