<?php

class EscortsController extends Zend_Controller_Action
{
    public static $linkHelper;
    public static $mapper = NULL;

    public function init()
    {
        self::$linkHelper = $this->view->getHelper('GetLink');

    }

    private function setMapper(){

        $mapper = new Cubix_Filter_Mapper();

        $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'k', 'filter' => 'Keyword', 'field' => 'ek.keyword_id', 'filterField' => 'fd.keywords', 'queryJoiner' => 'OR', 'weight' => 10)) );
        $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'l', 'filter' => 'Language', 'field' => 'e.languages', 'filterField' => 'fd.language', 'queryJoiner' => 'OR', 'weight' => 20)) );
        $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'e', 'filter' => 'Ethnicity', 'field' => 'e.ethnicity', 'filterField' => 'fd.ethnicity', 'queryJoiner' => 'OR', 'weight' => 30)) );
        //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'n', 'filter' => 'Nationality', 'field' => 'e.nationality_id', 'filterField' => 'fd.nationality', 'queryJoiner' => 'OR', 'weight' => 40)) );
        $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'hc', 'filter' => 'HairColor', 'field' => 'e.hair_color', 'filterField' => 'fd.hair_color', 'queryJoiner' => 'OR', 'weight' => 50)) );
        //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'hl', 'filter' => 'HairLength', 'field' => 'e.hair_length', 'filterField' => 'fd.hair_length', 'queryJoiner' => 'OR', 'weight' => 60)) );
        $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'cs', 'filter' => 'CupSize', 'field' => 'e.cup_size', 'filterField' => 'fd.cup_size', 'queryJoiner' => 'OR', 'weight' => 70)) );
        $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'ec', 'filter' => 'EyeColor', 'field' => 'e.eye_color', 'filterField' => 'fd.eye_color', 'queryJoiner' => 'OR', 'weight' => 80)) );
        $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'ph', 'filter' => 'PubicHair', 'field' => 'e.pubic_hair', 'filterField' => 'fd.pubic_hair', 'queryJoiner' => 'OR', 'weight' => 90)) );
        //tatto
        //piercing
        $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'sf', 'filter' => 'ServiceFor', 'field' => 'e.sex_availability', 'filterField' => 'fd.service_for', 'queryJoiner' => 'OR', 'weight' => 120)) );
        $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'se', 'filter' => 'Service', 'field' => 'es.service_id', 'filterField' => 'fd.services', 'queryJoiner' => 'OR', 'weight' => 130)) );

        //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'ihr', 'filter' => 'IncallHotel', 'field' => 'e.incall_hotel_room', 'filterField' => 'fd.available_for_incall_hotel', 'queryJoiner' => 'OR', 'weight' => 140)) );
        $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'in', 'filter' => 'Incall', 'field' => 'e.incall_type', 'filterField' => 'fd.available_for_i', 'queryJoiner' => 'OR', 'weight' => 150)) );
        $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'out', 'filter' => 'Outcall', 'field' => 'e.outcall_type', 'filterField' => 'fd.available_for_o', 'queryJoiner' => 'OR', 'weight' => 160)) );
        $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'tp', 'filter' => 'Travel', 'field' => 'e.travel_place', 'filterField' => 'fd.available_for_travel', 'queryJoiner' => 'OR', 'weight' => 170)) );

        //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'a', 'filter' => 'Age', 'field' => 'e.age', 'filterField' => 'fd.age', 'weight' => 50)) );
        //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'h', 'filter' => 'Height', 'field' => 'e.height', 'filterField' => 'fd.height', 'weight' => 90)) );
        //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'w', 'filter' => 'Weight', 'field' => 'e.weight', 'filterField' => 'fd.weight', 'weight' => 100)) );
        //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'sm', 'filter' => 'Smoker', 'field' => 'e.is_smoking', 'filterField' => 'fd.smoker', 'queryJoiner' => 'OR', 'weight' => 120)) );
        //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'v', 'filter' => 'Verified', 'field' => 'e.verified_status', 'filterField' => 'fd.verified', 'queryJoiner' => 'AND', 'weight' => 140)) );

        self::$mapper = $mapper;

    }

    private function getCurrentFilter($existing_filters = array())
    {
        if(is_null(self::$mapper)){
            $this->setMapper();
        }

        $filter = new Cubix_FilterV2( array('mapper' => self::$mapper->getAll(), 'request' => $this->_request, 'existingFilters' => $existing_filters) );
        //var_dump($filter->makeQuery()); die;
        return $filter->makeQuery();
    }

    private function getPostFilters()
    {
        $req = $this->_request;
        $s_country = $req->getParam('country') ? (int)$req->getParam('country'): null;
        $s_region = $req->getParam('region') ? (int)$req->getParam('region'): null;
        $s_city = $req->getParam('city') ? (int)$req->getParam('city'): null;
        $s_name = $req->getParam('name', null);
        //$s_agency  = $req->getParam('agency');
        $s_agency_slug  = $req->getParam('agency_slug', null);
        $s_phone = $req->getParam('phone') ? "%" . (int)$req->getParam('phone') . "%" : null ;
        $s_age_from = $req->getParam('age_from') ? (int)$req->getParam('age_from'): null;
        $s_age_to = $req->getParam('age_to') ? (int)$req->getParam('age_to') : null;
        $s_orientation = (array) $req->getParam('orientation');
        $s_height_from = $req->getParam('height_from') ? (int)$req->getParam('height_from'): null;
        $s_height_to = $req->getParam('height_to') ? (int)$req->getParam('height_to'): null;
        $s_weight_from =  $req->getParam('weight_from') ? (int)$req->getParam('weight_from'): null;
        $s_weight_to = $req->getParam('weight_to') ? (int)$req->getParam('weight_to'): null;
        $s_currency = $req->getParam('s_currency', null);
        $s_price_from =  $req->getParam('price_from') ? (int)$req->getParam('price_from'): null;
        $s_price_to = $req->getParam('price_to') ? (int)$req->getParam('price_to'): null;
        $s_incall_outcall = $req->getParam('incall_outcall') ? (int)$req->getParam('incall_outcall'): null;
        $s_real_pics =  $req->getParam('s_real_pics') ? (int)$req->getParam('s_real_pics'): null;
        $s_verified_contact = $req->getParam('s_verified_contact') ? (int)$req->getParam('s_verified_contact'): null;
        $s_with_video = $req->getParam('s_with_video') ? (int)$req->getParam('s_with_video'): null;
        $s_pornstar =  $req->getParam('s_pornstar') ? (int)$req->getParam('s_pornstar'): null;
        //$s_available_for_travel = (int)$this->_request->getParam('s_available_for_travel');
        $s_is_online_now = $req->getParam('s_is_online_now') ? (int)$req->getParam('s_is_online_now'): null;
        $s_natural_photo = $req->getParam('s_natural_photo') ? (int)$req->getParam('s_natural_photo'): null;

        $s_filter_params = array(
            'cr.id = ?' => $s_country,
            'ct.id = ?' => $s_city,
            'ct.region_id = ?' => $s_region,
            'e.showname LIKE ?' => $s_name,
            'e.agency_slug = ?' => $s_agency_slug,
            'e.contact_phone_parsed LIKE ?' => $s_phone,
            'e.age >= ?' => $s_age_from,
            'e.age <= ?' => $s_age_to,
            'e.height >= ?' => $s_height_from,
            'e.height <= ?' => $s_height_to,
            'e.weight >= ?' => $s_weight_from,
            'e.weight <= ?' => $s_weight_to,
            'currency' => $s_currency,
            'price-from' => $s_price_from,
            'price-to' => $s_price_to,
            'incall-outcall' => $s_incall_outcall,
            'orientation' => $s_orientation,
            'e.verified_status = 2' => $s_real_pics,
            'e.last_hand_verification_date IS NOT NULL' => $s_verified_contact,
            'with-video' => $s_with_video,
            'e.is_pornstar = 1' => $s_pornstar,
            //'travel_place' => $s_available_for_travel,
            'is-online-now' => $s_is_online_now,
            'with-natural-photo'=> $s_natural_photo
        );

        foreach($s_filter_params as $key => $filter){
            if(is_null($filter) || (is_array($filter) && count($filter) == 0)){
                unset($s_filter_params[$key]);
            }
        }
        return $s_filter_params;
    }

    public function getFilterV2Action($existing_filters = array(), $p_top_category = null)
    {
        if ( ! is_null($this->_getParam('ajax'))){
            $this->view->layout()->disableLayout();
        }
        $current_filter = array();
        if(is_null(self::$mapper)){
            $this->setMapper();
        }

        if ( ! is_null($this->_getParam('ajax')) && ! is_null($this->_getParam('filter_params')) ) {
            $filter_params = (array) json_decode(base64_decode($this->_getParam('filter_params')));
            $p_top_category = (!empty($p_top_category)) ? $p_top_category : $this->_getParam('p_top_category');
            $is_upcomingtour =  $this->_getParam('is_upcomingtour');
            $is_tour = $this->_getParam('is_tour');

            if ($this->_getParam('with_data')){
                $post_params = $this->getPostFilters();
                $filter_params = array_merge($post_params, $filter_params);
                $current_filter = $this->getCurrentFilter();
            }

            $existing_filters = Model_Escort_List::getActiveFilter($filter_params, $current_filter, $is_upcomingtour, $is_tour);
            $filter_defines = Model_Escort_List::getFilterDefines($p_top_category, $is_upcomingtour, $is_tour);
        }

        $filter = new Cubix_FilterV2( array('mapper' => self::$mapper->getAll(), 'request' => $this->_request, 'existingFilters' => $existing_filters, 'filterDefines' => $filter_defines) );
        /*if($existing_filters){
			var_dump($filter->renderED());die;
        }*/

        //$this->view->count = $existing_filters['result']->cnt;
        $this->view->count = $existing_filters['result']->cnt;
        $this->view->filter = $filter;
//        $this->view->filter = $filter->renderED();

    }

    /**
     * @param array $args
     * @return array|void
     */
    public function _hardcode_vips_position(Array $args)
    {
        $country = $args['country'];
        $city = $args['city'];
        $vips = $args['vips'];

        if (empty($vips)) return;

        // Escort VIP reorder in spots
        // -------------------------------------------------------
        $escort_places_arr = Model_VipHardcodes::getAll();

        // Group "escort places", Set escort_id as identifier
        // -------------------------------------------------------
        $count_of_ads_in_this_area = 0;
        $escort_places = [];
        foreach ($escort_places_arr as $key => $place) {
            $escort_places[$place->escort_id] = $place;

            $country_matches = $place->country_id == $country;
            $city_matches = $place->city_id == $city;

            if (($country && $country_matches) || ($city && $city_matches)) {
                $count_of_ads_in_this_area++;
            }
        }

        // Creating empty array, this will be used
        // to store sed_cards inside, but in ordered position
        // -------------------------------------------------------------
        $ordered_vips = range(0, count($vips) - 1);
        // -------------------------------------------------------------

        // Put sed_cards into their hardcoded place
        // -------------------------------------------------------------
        foreach ($vips as $id => $escort) {
            $pp_data = $escort_places[$escort->id];
            if (!is_null($pp_data)) {

                // Check if current filtered country and city are in hardcoded list
                // -------------------------------------------
                $country_matches = $pp_data->country_id == $country;
                $city_matches = $pp_data->city_id == $city;
                // -------------------------------------------

                if ((($country && $country_matches) || ($city && $city_matches)) && count($vips) > $count_of_ads_in_this_area) {
                    $escort->hardcoded_id = $pp_data->agency_id;
                    $ordered_vips[$pp_data->place] = $escort;
                }
            }
        }
        // -------------------------------------------------------------

        // Filling up the ordered array with other VIP non hardcoded Ads
        // -------------------------------------------------------------
        foreach ($vips as $id => $escort)
            if (!isset($escort_places[$escort->id]))
                foreach ($ordered_vips as $key => $val)
                    if (is_numeric($val)) {
                        $ordered_vips[$key] = $escort;
                        break;
                    }
        // -------------------------------------------------------------

        // Shuffle Ads if their Agency is the same
        // -------------------------------------------------------------
        for ($i = 0; $i < count($ordered_vips); $i++) {
            for ($j = 0; $j < count($ordered_vips); $j++) {
                if (!is_null($ordered_vips[$i]->hardcoded_id) && $ordered_vips[$i]->hardcoded_id == $ordered_vips[$j]->hardcoded_id && (rand(0, 10) / 10) < 0.5) {
                    $temp = $ordered_vips[$i];
                    $ordered_vips[$i] = $ordered_vips[$j];
                    $ordered_vips[$j] = $temp;
                }
            }
        }

        // -------------------------------------------------------------
        $ordered_vips = array_filter($ordered_vips, function($x) {
            return !is_numeric($x);
        });

        return $ordered_vips;
    }

    public function indexAction()
    {
        //echo $route_name = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
        if($this->_request->pushstate_filters){
            Model_Escort_List::convertPushState($this->_request->pushstate_filters);
        }

        // <editor-fold defaultstate="collapsed" desc="If it is ajax request, disable the layout and render only list.phtml">
        if ( ! is_null($this->_getParam('ajax')) ) {
            $this->view->layout()->disableLayout();
            $this->_helper->viewRenderer->setScriptAction('list');
            $this->view->is_ajax = true;
        } else {
            setcookie("show_all_escorts", 0, 0, "/", "www." . preg_replace('#www.|test.#', '', $_SERVER['HTTP_HOST']));
            $_COOKIE['show_all_escorts'] = 0;
        }
        // </editor-fold>

        // SEARCH PARAMS
        //$s_showname = $this->view->s_showname = $this->_request->showname;
        $alert_id = (int)$this->_getParam('alert_id');
        if($alert_id && $alert_id > 0){
            $cur_user = Model_Users::getCurrent();
            if(!$cur_user || $cur_user->user_type != 'member'){
                $this->_redirect($this->view->getLink('signin'));
                exit();
            }
        }

        //autocomplate fields
        $ts_type = $this->view->ts_type = $this->_getParam('ts_type');
        $ts_slug = $this->view->ts_slug = $this->_getParam('ts_slug');

        $show_all_hidden_escorts = $this->view->show_all_hidden_escorts = (int) $this->_getParam('sahe', 0);

        $country = $this->view->country = $this->_getParam('country_id');
        $region = $this->view->region = $this->_getParam('region_id');
        $city = $this->view->city = $this->_getParam('city_id');
        $zone = $this->view->city = $this->_getParam('zone_id');

        $s_name = $this->view->s_name = $this->_getParam('name');
        //$s_agency = $this->view->s_agency = $this->_request->agency;
        $s_agency_slug = $this->view->s_agency_slug = $this->_getParam('agency_slug');
        $s_gender = $this->view->s_gender = $this->_getParam('gender', array());
        //$s_city_slug = $this->view->s_city_slug = $this->_request->city_slug;
        $s_phone = $this->view->s_phone = $this->_getParam('phone', null);
        $s_age_from = $this->view->s_age_from = (int)$this->_getParam('age_from');
        $s_age_to = $this->view->s_age_to = (int)$this->_getParam('age_to');
        $s_orientation = $this->view->s_orientation = (array) $this->_getParam('orientation');
        $s_keywords = $this->view->s_keywords = (array) $this->_getParam('k');
        $s_height_from = $this->view->s_height_from = (int)$this->_getParam('height_from');
        $s_height_to = $this->view->s_height_to = (int)$this->_getParam('height_to');
        $s_weight_from = $this->view->s_weight_from = (int)$this->_getParam('weight_from');
        $s_weight_to = $this->view->s_weight_to = (int)$this->_getParam('weight_to');
        $s_tatoo = $this->view->s_tatoo = (int)$this->_getParam('tatoo');
        $s_piercing = $this->view->s_piercing = (int)$this->_getParam('piercing');

        $s_ethnicity = $this->view->s_ethnicity = (array) $this->_getParam('e');
        $s_language = $this->view->s_language = (array) $this->_getParam('l');
        $s_nationality = $this->view->s_nationality = (array) $this->_getParam('n');
        $s_hair_color = $this->view->s_hair_color = (array)$this->_getParam('hc');
        $s_hair_length = $this->view->s_hair_length = (array)$this->_getParam('hl');
        $s_cup_size = $this->view->s_cup_size = (array) $this->_getParam('cs');
        $s_eye_color = $this->view->s_eye_color = (array)$this->_getParam('ec');
        $s_pubic_hair = $this->view->s_pubic_hair = (array) $this->_getParam('ph');

        $s_currency = $this->view->s_currency = $this->_getParam('s_currency', (!empty($_COOKIE['currency']) ? $_COOKIE['currency'] : Model_Countries::getGeoCurrency()));
        $s_price_from = $this->view->s_price_from = (int)$this->_getParam('price_from');
        $s_price_to = $this->view->s_price_to = (int)$this->_getParam('price_to');
        $s_incall_outcall = $this->view->s_incall_outcall = (int)$this->_getParam('incall_outcall');
        $s_sex_availability = $this->view->s_sex_availability = (array) $this->_getParam('sf');
        $s_service = $this->view->s_service = (array) $this->_getParam('se');

        //$s_incall_other = $this->view->s_incall_other = (int)$this->_request->in;
        //$s_outcall_other = $this->view->s_outcall_other = (int)$this->_request->out;
        $s_incall = $this->view->s_incall = (array)$this->_getParam('in');
        $s_incall_hotel_room = $this->view->s_incall_hotel_room = (array) $this->_getParam('ihr');
        $s_outcall = $this->view->s_outcall = (array)$this->_getParam('out');
        $s_travel_place = $this->view->s_travel_place = (array) $this->_getParam('tp');

        $s_real_pics = $this->view->s_real_pics = (int)$this->_getParam('s_real_pics');
        $s_verified_contact = $this->view->s_verified_contact = (int)$this->_getParam('s_verified_contact');
        $s_with_video = $this->view->s_with_video = (int)$this->_getParam('s_with_video');
        $s_pornstar = $this->view->s_pornstar = (int)$this->_getParam('s_pornstar');
        $s_available_for_travel = $this->view->s_available_for_travel = (int)$this->_getParam('s_available_for_travel');
        $s_is_online_now = $this->view->s_is_online_now = (int)$this->_getParam('s_is_online_now');
        $s_natural_photo = $this->view->s_natural_photo = (int)$this->_getParam('s_natural_photo');
        $this->view->sort = ($this->_getParam('sort')) ? $this->_getParam('sort') : $_COOKIE['sorting'];

        $this->view->filterBoxes = [
            ['name' => 's_real_pics', 'title' => 'quick_search_real_pics', 'isVisible' => 1],
            ['name' => 's_verified_contact', 'title' => 'quick_search_verified_contact', 'isVisible' => 1],
            ['name' => 's_with_video', 'title' => 'quick_search_video', 'isVisible' => 1],
            ['name' => 's_is_online_now', 'title' => 'quick_search_online', 'isVisible' => 1],
            ['name' => 's_available_for_travel', 'title' => 'available_for_travel', 'isVisible' => 1]
        ];

        $this->view->p_id = $this->_request->getParam('p_id');
        $this->view->page = $this->_request->getParam('page');
        $this->view->user_cleared_location_searchbar = $user_cleared_location_searchbar = $this->_request->getParam('is_location_cleared', 0);
        $this->view->hideOrdering = false;

        $p_top_category = $this->view->p_top_category = $this->_getParam('p_top_category', 'escorts');
        $p_country_slug = $this->view->p_country_slug = $this->_getParam('p_country_slug');
        $p_country_id = $this->view->p_country_id = (int) $this->_getParam('p_country_id');
        $escortType = $this->view->escortType = $this->_getParam('escortType', 'escort');

        $geoData = Cubix_Geoip::getClientLocation();

        if($p_country_slug && $p_country_id && !$country){
            $country = $this->view->country = $p_country_id;
        }

        $p_city_slug = $this->view->p_city_slug = $this->_getParam('p_city_slug');
        $p_city_id = $this->view->p_city_id = (int) $this->_getParam('p_city_id');

        if($p_city_slug && $p_city_id && !$country){
            $city = $this->view->city = $p_city_id;
            $country = $this->view->country = Cubix_Geography_Cities::getCountryIdBySlug($p_city_slug);
        }

        $looped = false; // This is used to detect if goto operator moved you back to START_FILTERING
        START_FILTERING: // Remember the point to make filtering again if needed

        $s_filter_params = array(
            'name' => ($s_name) ? $s_name . "%" : '',
            'agency-name' => $s_agency_slug,
            'gender' => is_array($s_gender) ? $s_gender : [$s_gender],
            'phone' => ($s_phone) ? "%" . $s_phone . "%" : '',
            'age-from' => $s_age_from,
            'age-to' => $s_age_to,
            'orientation' => $s_orientation,
            'keywords' => $s_keywords,
            'height-from' => $s_height_from,
            'height-to' => $s_height_to,
            'weight-from' => $s_weight_from,
            'weight-to' => $s_weight_to,
            'tatoo' => $s_tatoo,
            'piercing' => $s_piercing,
            'alert_id' => $alert_id,
            'ethnicity' => $s_ethnicity,
            'language' => $s_language,
            'nationality' => $s_nationality,
            'hair-color' => $s_hair_color,
            'hair-length' => $s_hair_length,
            'cup-size' => $s_cup_size,
            'eye-color' => $s_eye_color,
            'pubic-hair' => $s_pubic_hair,
            'currency' => $s_currency,
            'price-from' => $s_price_from,
            'price-to' => $s_price_to,
            'incall-outcall' => $s_incall_outcall,
            'sex-availability' => $s_sex_availability,
            'service' => $s_service,
            'incall' => $s_incall,
            'incall-hotel-room' => $s_incall_hotel_room,
            'outcall' => $s_outcall,
            'travel-place' => $s_travel_place,
            'real-pics' => $s_real_pics,
            'verified-contact' => $s_verified_contact,
            'with-video' => $s_with_video,
            'pornstar' => $s_pornstar,
            'travel_place' => $s_available_for_travel,
            'is-online-now' => $s_is_online_now,
            'show-all-hidden-escorts' => $show_all_hidden_escorts,
            'with-natural-photo'=> $s_natural_photo
        );

        if ( $ts_type == 'escort' ) {
            $s_filter_params['name'] = $ts_slug;
        } else if ( $ts_type == 'escort-see-all' ) {
            $s_filter_params['name'] = $ts_slug . "%";
        }

        $cache = Zend_Registry::get('cache');
        $this->view->cache = $cache;

        $static_page_key = 'main';
        $is_tour = false;
        $is_upcomingtour = false;
        $gender = empty($s_gender) ? GENDER_FEMALE : $s_gender;
        $is_agency = null;
        $is_new = false;
        $category = ESCORT_TYPE_ESCORT;

        $s_config = Zend_Registry::get('system_config');
        $e_config = Zend_Registry::get('escorts_config');
        $this->view->showMainPremiumSpot = $s_config['showMainPremiumSpot'];

        if ( ! $s_config['showMainPremiumSpot'] ) {
            $static_page_key = 'regular';
        }

        $filtered_location = [];
        $params = array(
            'sort' => 'random',
            'filter' => array(),
            'page' => 1
        );

        // If user searched for cityzone, then fetch its info
        // and fill in top searchbar
        // --------------------------------------
        if (!empty($zone)) {
            $zone_data = Model_Cityzones::getZoneCityCountryByID($zone);
            $filtered_location = array_merge($filtered_location, (array)$zone_data);
        }

        // If we have cityID, fetch all info about it
        // to fill in top searchbar
        // --------------------------------------
        if (!empty($city)) {
            $city_data = Model_Cities::getCountryRegionByCityID($city);
            $filtered_location = array_merge($filtered_location, (array)$city_data);
        }

        // If we have don't have cityID, but regionID,
        // then select all data about region
        // --------------------------------------
        if (!empty($region)) {
            $region_data = Model_Regions::getRegionCountryByID($region);
            $filtered_location = array_merge($filtered_location, (array)$region_data);
        }

        // If we have don't have cityID and regionID, but countryID,
        // then select all data about country
        // --------------------------------------
        if (!empty($country)) {
            $country_data = Model_Countries::getCountryByID($country);
            $filtered_location = array_merge($filtered_location,  (array)$country_data);
        }
        // --------------------------------------

        // Store the location which user filtered last time
        // to put it in comments page ( in the filter panel )
        // !UPDATE! Not only in comments page, in listing also
        // ---------------------------------------
        $location_session = new Zend_Session_Namespace('last-filtered-location');
        $location_session->setExpirationHops(60 * 60);
        // ---------------------------------------

        // This block of code fills locationbar and search parameters
        // with last time searched location
        // ---------------------------------------
        if(!empty($location_session->data) && empty($filtered_location) && !$user_cleared_location_searchbar) {
            $filtered_location = & $location_session->data;

            $city = $filtered_location['city_id'];
            $country = $filtered_location['country_id'];
            $region = $filtered_location['region_id'];
        }
        // ---------------------------------------

        // If users location is detected but user didnt make any location searches
        // Need to also check if user didnt clear himself the searchbar, otherwise this means he just doesnt want to see escorts from his location
        // then put that into searchbar
        // -------------------------------------
        if (!$user_cleared_location_searchbar && !empty($geoData) && isset($geoData['city']) && empty($filtered_location) && !$looped && empty($location_session->data)) {
            $locationFilledWithGeo = true;
            $modelCities = new Model_Cities();
            $geo_city = $this->view->geo_city =  $modelCities->getByGeoIp();
            $filtered_location = (array)$geo_city;

            $city = $geo_city['city_id'];
            $country = $geo_city['country_id'];
            $region = $geo_city['region_id'];
            $s_currency = $this->view->s_currency = Model_Countries::getGeoCurrency();
        }
        // -------------------------------------

        $location_session->data = $this->view->filtered_location = $filtered_location; // Store filtered location into session

        $is_active_filter = false;
        foreach( $s_filter_params as $i => $par ) {
            if ( is_array($par) ) {
                if( count($par) ) {
                    $is_active_filter = true;
                    break;
                }
            } else {
                if( $par ) {
                    $is_active_filter = true;
                    break;
                }
            }
        }

        if (!$is_active_filter)
        {
            if ($country || $city)
                $is_active_filter = true;
        }

        $this->view->is_active_filter = $is_active_filter;

        // Filtering escorts by their type (Agency, Independent ...)
        // ----------------------------------------------
        switch($escortType) {
            case 'independent-escorts':
                $this->view->searchEscortType = 'independent-escorts';
                $params['filter'][1] = array('field' => 'independent-escorts', 'value' => array());
                break;
            case 'agencies':
            case 'agency-escorts':
                $this->view->searchEscortType = 'agencies';
                $params['filter'][1] = array('field' => 'agencies', 'value' => array());
                break;
            default:
                $this->view->searchEscortType = 'escorts';
                break;
        }
        // ----------------------------------------------
        
        if ( $p_top_category) {
            switch( $p_top_category )
            {
                case 'newest':
                    $static_page_key = 'newest';
                    $is_agency = null;
                    $is_new = true;
                    $params['filter'][0] = array('field' => 'newest', 'value' => array());
                    continue;
                case 'newest-independent':
                    $static_page_key = 'newest-independent';
                    $is_agency = 0;
                    $is_new = true;
                    $params['filter'][0] = array('field' => 'newest-independent', 'value' => array());
                    continue;
                case 'newest-agency-escorts':
                    $static_page_key = 'newest-agency-escorts';
                    $is_agency = 1;
                    $is_new = true;
                    $params['filter'][0] = array('field' => 'newest-agency-escorts', 'value' => array());
                    continue;
                case 'escorts':
                    if(is_numeric($p_city_id) && $p_city_id != 0){
                        $static_page_key = 'cities-escorts';
                    }else{
                        $static_page_key = 'escorts';
                        if($this->_getParam('online_now')){
                            $static_page_key = 'online-escorts';
                        }
                    }
                    $params['filter'][] = array('field' => 'escorts', 'value' => array());
                    $this->view->searchCategory = GENDER_FEMALE . '_' . ESCORT_TYPE_ESCORT;
                    continue;
                case 'escorts-only':
                    $static_page_key = 'escorts-only';
                    $is_agency = 0;
                    $params['filter'][] = array('field' => 'escorts-only', 'value' => array());
                    continue;
                case 'independent-escorts':
                    $static_page_key = 'independent-escorts';
                    $is_agency = 0;
                    $params['filter'][] = array('field' => 'independent-escorts', 'value' => array());
                    continue;
                case 'agency-escorts':
                    $static_page_key = 'agency-escorts';
                    $params['filter'][] = array('field' => 'agency', 'value' => array());
                    continue;
                case 'agencies':
                    $static_page_key = 'agencies';
                    $is_agency = 1;
                    $params['filter'][] = array('field' => 'agencies', 'value' => array());
                    continue;
                case 'bdsm':
                    $static_page_key = 'bdsm';
                    $is_agency = null;
                    $category = ESCORT_TYPE_BDSM;
                    $params['filter'][0] = array('field' => 'bdsm', 'value' => array());
                    $this->view->searchCategory = GENDER_FEMALE . '_' . ESCORT_TYPE_BDSM;
                    continue;
                case 'bdsm-boys':
                    $static_page_key = 'bdsm';
                    $gender = GENDER_MALE;
                    $category = ESCORT_TYPE_BDSM;
                    $params['filter'][0] = array('field' => 'bdsm-boys', 'value' => array());
                    continue;
                case 'bdsm-trans':
                    $static_page_key = 'bdsm';
                    $gender = GENDER_TRANS;
                    $category = ESCORT_TYPE_BDSM;
                    $params['filter'][0] = array('field' => 'bdsm-trans', 'value' => array());
                    continue;
                case 'boys':
                    $static_page_key = 'boys';
                    $gender = GENDER_MALE;
                    $params['filter'][0] = array('field' => 'boys', 'value' => array());
                    $this->view->searchCategory = GENDER_MALE;
                    continue;
                /*case 'boys-trans':
                    $static_page_key = 'boys-trans';
                    $params['filter'][0] = array('field' => 'boys-trans', 'value' => array());
                    continue;
                case 'boys-heterosexual':
                    $static_page_key = 'boys-heterosexual';
                    $params['filter'][0] = array('field' => 'boys-heterosexual', 'value' => array());
                    continue;
                case 'boys-bisexual':
                    $static_page_key = 'boys-bisexual';
                    $params['filter'][0] = array('field' => 'boys-bisexual', 'value' => array());
                    continue;
                case 'boys-homosexual':
                    $static_page_key = 'boys-homosexual';
                    $params['filter'][0] = array('field' => 'boys-homosexual', 'value' => array());
                    continue;*/
                case 'trans':
                    $static_page_key = 'trans';
                    $gender = GENDER_TRANS;
                    $params['filter'][0] = array('field' => 'trans', 'value' => array());
                    $this->view->searchCategory = GENDER_TRANS;
                    continue;
                case 'citytours':
                    $static_page_key = 'citytours';
                    $this->view->hideOrdering = true;
                    $is_tour = true;
                    $gender = null;
                    $params['filter'][] = array('field' => 'tours', 'value' => array());
                    $this->view->searchCategory = GENDER_FEMALE . '_' . ESCORT_TYPE_ESCORT;
                    continue;
                case 'upcomingtours':
                    $static_page_key = 'upcomingtours';
                    $is_tour = true;
                    $is_upcomingtour = true;
                    $gender = null;
                    $params['filter'][] = array('field' => 'upcomingtours', 'value' => array());
                    continue;
                case 'regular':
                    $static_page_key = 'regular';
                    $params['filter'][] = array('field' => 'regular', 'value' => array());
                    continue;
                case 'massage':
                    $static_page_key = 'massage';
                    $gender = GENDER_FEMALE;
                    $category = ESCORT_TYPE_MASSAGE;
                    $params['filter'][0] = array('field' => 'massage', 'value' => array());
                    $this->view->searchCategory = GENDER_FEMALE . '_' . ESCORT_TYPE_MASSAGE;
                    continue;
                case 'massage-boys':
                    $static_page_key = 'massage-boys';
                    $gender = GENDER_MALE;
                    $category = ESCORT_TYPE_MASSAGE;
                    $params['filter'][0] = array('field' => 'massage-boys', 'value' => array());
                    continue;
                case 'massage-trans':
                    $static_page_key = 'massage-trans';
                    $gender = GENDER_TRANS;
                    $category = ESCORT_TYPE_MASSAGE;
                    $params['filter'][0] = array('field' => 'massage-trans', 'value' => array());
                    continue;
                case 'view-alert':
                    $static_page_key = 'view-alert';
                    $params['filter'][0] = array('field' => 'view-alert', 'value' => array());
                    continue;
                default:
                    $this->_response->setRawHeader('HTTP/1.1 404 Not Found');
                    $this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
                    return;
            }

        }

        // Check if its some of the static pages, to hide gender and other filters
        // -----------------------------------------------
        $fucking_pages = array('newest', 'newest-independent', 'newest-agency-escorts', 'online-escorts', 'city-tours', 'citytours');
        $this->view->hideMainFilters = (str_replace($fucking_pages, '', $static_page_key) != $static_page_key);
        // -----------------------------------------------

        $list_types = array('gallery', 'list');

        if ( isset($_COOKIE['list_type']) && $_COOKIE['list_type'] ) {
            $list_type = $_COOKIE['list_type'];

            if ( ! in_array($list_type, $list_types) ) {
                $list_type = $list_types[0];
            }

            $this->view->render_list_type = $list_type;
        } else {
            $list_type = $list_types[0];
            $this->view->render_list_type = $list_types[0];
        }

        if ( $city ) {
            //$params['city'] = $p_city_slug;
            $params['filter'][] = array('field' => 'city_id', 'value' => array($city));
        }

        if ( $region ) {
            $params['filter'][] = array('field' => 'region_id', 'value' => array($region));
        }

        if ( $country ) {
            //$params['country'] = $p_country_slug;
            $params['filter'][] = array('field' => 'country_id', 'value' => array($country));
        }

        $this->view->static_page_key = $static_page_key;

        $filter_params = array(
            'order' => 'e.date_registered DESC',
            'limit' => array('page' => 1),
            'filter' => array()
        );


        // <editor-fold defaultstate="collapsed" desc="Map parameters from url to SQL where clauses">
        $filter_map = array(
            'name' => 'e.showname LIKE ?',
            'agency-name' => 'e.agency_slug = ?',
            'phone' => 'e.contact_phone_parsed LIKE ?',
            'age-from' => 'e.age >= ?',
            'age-to' => 'e.age <= ?',
            'height-from' => 'e.height >= ?',
            'height-to' => 'e.height <= ?',
            'weight-from' => 'e.weight >= ?',
            'weight-to' => 'e.weight <= ?',
            'tatoo' => 'e.tatoo = ?',
            'piercing' => 'e.piercing = ?',
            'real-pics' => 'e.verified_status = 2',
            'with-video' => 'with-video',
            'verified-contact' => 'e.last_hand_verification_date IS NOT NULL',
            'pornstar' => 'e.is_pornstar = 1',
            'travel_place' => 'e.travel_place IN (1, 2, 3)',
            'incall-other' => 'e.incall_other IS NOT NULL AND e.incall_other <> ""',
            'outcall-other' => 'e.outcall_other IS NOT NULL AND e.outcall_other <> ""',
            'region' => 'r.slug = ?',
            'city' => 'ct.slug = ?',
            'city_id' => 'eic.city_id = ?',
            'region_id' => 'eic.region_id = ?',
            'country_id' => 'eic.country_id = ?',
            'country' => 'cr.slug = ?',
            'cityzone' => 'c.id = ?',
            'zone' => 'cz.slug = ?',
            'boys-heterosexual' => 'e.sex_orientation = ' . ORIENTATION_HETEROSEXUAL . ' AND e.gender = ' . GENDER_MALE,
            'boys-bisexual' => 'e.sex_orientation = ' . ORIENTATION_BISEXUAL . ' AND e.gender = ' . GENDER_MALE,
            'boys-homosexual' => 'e.sex_orientation = ' . ORIENTATION_HOMOSEXUAL . ' AND e.gender = ' . GENDER_MALE,
            'escorts' => 'eic.gender = ' . GENDER_FEMALE . ' AND eic.type = ' . ESCORT_TYPE_ESCORT,
            'escorts-only' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
            'agencies' => 'eic.is_agency = 1',
            'independent-escorts' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE . ' AND eic.type = ' . ESCORT_TYPE_ESCORT,
            'agency' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE . ' AND eic.type = ' . ESCORT_TYPE_ESCORT,
            'boys-trans' => '(eic.gender = ' . GENDER_MALE . ' OR eic.gender = ' . GENDER_TRANS . ')',
            'boys' => 'eic.gender = ' . GENDER_MALE . ' AND eic.type = ' . ESCORT_TYPE_ESCORT,
            'trans' => 'eic.gender = ' . GENDER_TRANS. ' AND eic.type = ' . ESCORT_TYPE_ESCORT,
            'bdsm' => 'eic.gender = ' . GENDER_FEMALE .' AND eic.type = '.ESCORT_TYPE_BDSM,

            'bdsm-boys' => 'eic.gender = ' . GENDER_MALE. ' AND eic.type = '.ESCORT_TYPE_BDSM,
            'bdsm-trans' => 'eic.gender = ' . GENDER_TRANS . ' AND eic.type = '.ESCORT_TYPE_BDSM,
            'newest' => 'eic.gender = ' . GENDER_FEMALE,
            'newest-independent' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
            'newest-agency-escorts' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE,
            'tours' => 'eic.is_tour = 1',
            'upcomingtours' => 'eic.is_upcoming = 1',
            'massage' => 'eic.gender = '. GENDER_FEMALE. ' AND eic.type = '.ESCORT_TYPE_MASSAGE ,
            'massage-boys' => 'eic.gender = '. GENDER_MALE. ' AND eic.type = '.ESCORT_TYPE_MASSAGE,
            'massage-trans' => 'eic.gender = '. GENDER_TRANS. ' AND eic.type = '.ESCORT_TYPE_MASSAGE,
            'is-online-now' => 'ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1'
        );


        // <editor-fold defaultstate="collapsed" desc="Get page from request parameters">
        if ( $this->_request->page ) {
            $params['page'] = $this->_request->page;
        }

        if ( $this->_request->p_page && ! $this->_request->page ) {
            $params['page'] = $this->_request->p_page;
        }

        $page = intval($params['page']);
        if ( $page == 0 ) {
            $page = 1;
        }
        $filter_params['limit']['page'] = $this->view->page = $page;
        // </editor-fold>

        foreach ( $params['filter'] as $i => $filter ) {
            if ( ! isset($filter_map[$filter['field']]) ) continue;

            $value = $filter['value'];

            $filter_params['filter'][$filter_map[$filter['field']]] = $value;
        }

        /*if ( $online_now ) {
            unset($filter_params['filter']['eic.gender = 1']);
        }*/
        // </editor-fold>

        foreach ( $s_filter_params as $i => $s_filter ) {
            //if ( ! isset($filter_map[$i]) ) continue;

            if ( $s_filter && isset($filter_map[$i])) {
                $filter_params['filter'][$filter_map[$i]] = $s_filter;
                unset($s_filter_params[$i]);
            }
            elseif(!$s_filter || ( is_array($s_filter) && count($s_filter) == 0)){
                unset($s_filter_params[$i]);
            }
        }

        //Currency rates log
        $default_currency = "USD";
        $current_currency = $this->_getParam('currency', $default_currency);

        if ( $current_currency != $_COOKIE['currency'] && $this->_getParam('s') ) {
            setcookie("currency", $current_currency, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            $_COOKIE['currency'] = $current_currency;
        }

        if ( isset($_COOKIE['currency']) ) {
            $current_currency = $_COOKIE['currency'];
            $this->_setParam('currency', $current_currency);
        }

        $this->view->current_currency = $current_currency;
        //Currency rates log

        // <editor-fold defaultstate="collapsed" desc="Sorting parameters SQL mappings">
        $default_sorting = "close-to-me";
        $current_sorting = $this->_getParam('sort', $default_sorting);

        if ( in_array($static_page_key, array('newest', 'newest-independent', 'newest-agency-escorts'))) {
            $sort_map = array(
                'newest' => 'date_registered DESC',
            );
            $this->view->hideAdvancedSearch = true;
            $current_sorting = 'newest';
        } else{
            $sort_map = array(
                'newest' => 'date_registered DESC',
                'last-contact-verification' => 'last_hand_verification_date DESC',
                'price-asc' => 'price-asc', // will be modified from escorts
                'price-desc' => 'price-desc', // will be modified from escorts
                'last_modified' => 'date_last_modified DESC',
                'last-connection' => 'refresh_date DESC',
                'close-to-me' => 'ordering DESC',
                'most-viewed' => 'hit_count DESC',
                'random' => 'ordering DESC',
                'alpha' => 'showname ASC',
                'age' => '-age DESC',
            );
        }

        if ( !array_key_exists($current_sorting, $sort_map) ) {
            $current_sorting = $default_sorting;
            $this->_setParam('sort', $default_sorting);
        }

        if ( $current_sorting != $_COOKIE['sorting'] && $this->_getParam('s') ) {
            setcookie("sorting", $current_sorting, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            setcookie("sortingMap", $sort_map[$current_sorting]['map'], strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            $_COOKIE['sorting'] = $current_sorting;
            $_COOKIE['sortingMap'] = $sort_map[$current_sorting]['map'];
        }

        if ( isset($_COOKIE['sorting']) ) {
            $current_sorting = $_COOKIE['sorting'];
            $this->_setParam('sort', $current_sorting);
        }

        if ( $current_sorting ) {
            $tmp = $sort_map[$current_sorting];
            unset($sort_map[$current_sorting]);
            $sort_map = array_merge(array( $current_sorting => $tmp), $sort_map);
        }

        if ( isset($sort_map[$params['sort']]) ) {
            $filter_params['order'] = $sort_map[$params['sort']];
            $this->view->sort_param = $params['sort'];
        }

        $this->view->sort_map = $sort_map;
        $this->view->current_sort = $current_sorting;
        $params['sort'] = $current_sorting;
        // </editor-fold>

        // $model = new Model_EscortsV2(); // we dont use it))

        $count = 0;

        // <editor-fold defaultstate="collapsed" desc="Cache key generation">
        $filters_str = '';


        $combined_filters = array_merge((array) $filter_params['filter'], (array) $s_filter_params);
        foreach ( $combined_filters as $k => $filter ) {
            if ( ! is_array($filter) ) {
                $filters_str .= $k . '_' . $filter;
            }
            else {
                $filters_str .= $k;
                foreach($filter as $f) {
                    $filters_str .= '_' . $f;
                }
            }
        }

        /*$where_query_str = '';
        if ( $where_query ) {
            if ( count($where_query) ) {
                foreach ( $where_query as $query ) {
                    if ( isset($query['filter_query']) && $query['filter_query'] ) {
                        $where_query_str .= $query['filter_query'];
                    }
                }
            }
        }*/

        // </editor-fold>

        $per_page_def = $e_config['perPage'];

        $this->view->per_page = $per_page = Model_Escort_List::HOMEPAGE_LISTING_SIZE; // $this->_getParam('per_page', $per_page_def);

        $cache_key = 'v2_' . Cubix_I18n::getLang() . '_' . $params['sort'] . '_' . $filter_params['limit']['page'] . '_' . $filters_str . $where_query_str. (string) $has_filter . $per_page . $p_top_category . $country . $city . (int) $is_upcomingtour . (int) $is_tour . $list_type;
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
        $this->view->params = $params;

        $this->view->is_main_page = $is_main_page = ($static_page_key == 'escorts' && ! $city && ! $country );
        $this->view->filter_params = $filter_params['filter'];

        // If we are on main premium spot forward to corresponding action

        if ( $static_page_key == 'happy-hours' ) {
            $this->_forward('happy-hours');
            return;
        }

        if ( $static_page_key == 'chat-online' ) {
            $this->_forward('chat-online');
            return;
        }

        /* Ordering by country from GeoIp */
        /*if ($params['sort'] == 'random')
        {
            $modelC = new Model_Countries();

            $ret = $modelC->getByGeoIp();

            if (is_array($ret))
            {
                $cache_key .= '_country_iso_' . $ret['country_iso'];
                $params['sort'] = $ret['ordering'];
            }
        }*/
        /**/

        foreach($s_filter_params as $k => $value ) {
            if ( is_array($value) ) {
                foreach( $value as $val ) {
                    $cache_key .= '_' . $val . '_';
                }
            } else {
                $cache_key .= '_' . $k . '_' . $value;
            }
        }

        $geo_cache = "";

        if ( $params['sort'] == 'close-to-me' ) {

            if ( strlen($geoData['country']) ) {
                $geo_cache = md5($geoData['latitude'] . $geoData['longitude']);
                $cache_key .= $geo_cache;
            }
        }

        $cache_key .= $this->view->render_list_type;
        $cache_key .= Cubix_Countries::blacklistCountryCacheKeyCase();
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
        $cache_key = sha1($cache_key);

        //$cache_key_count = $cache_key . '_count';

        //$count = $cache->load($cache_key_count);

        if ( $is_active_filter ) {
            $filter_params['filter']['show_all_agency_escorts'] = true;
        }

        $only_newest = false;
        if ( in_array($static_page_key, array('newest', 'newest-independent', 'newest-agency-escorts'))) {
            $params['sort'] = 'newest';
            $filter_params['filter']['e.is_new = 1'] = array();
            $only_newest = true;
        }

        //if ( ! $escorts = $cache->load($cache_key) ) {
        if ( isset($is_tour) && $is_tour ) {
            $df = $dt = 0;
            if($page == 1) $dt = 1;
            else $df = 1;

            if($_GET['edo']) {
                echo "<pre>";
                var_dump($filter_params['filter']);
                echo "</pre>";
            }

            $escorts = Model_Escort_List::getTours($is_upcomingtour, $filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $per_page, $count, $s_filter_params, $show_grouped, $geoData, $list_type, $current_currency, $df, $dt);

        } else {
            list($escorts, $_availableFilters) = Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $per_page, $count, 'regular_list', false, $s_filter_params, $show_grouped, $geoData, $list_type, $p_top_category, $city, $country, $current_currency, true);

            // Loop trough all available checkboxes and
            // remove those which have no result in listing
            // --------------------------------------
            foreach($_availableFilters as $k => $v)
                foreach($this->view->filterBoxes as $_k => $checkBox)
                    if($checkBox['name'] == $k) {
                        $this->view->filterBoxes[$_k]['isVisible'] = $v;
                        break;
                    }
            // --------------------------------------

            $this->view->no_cache = true;
        }

        if(empty($escorts['vips'])) unset($escorts['vips']);
        if(empty($escorts['premiums'])) unset($escorts['premiums']);

        // Now the SATANIC logic comes :) (EDIR-2222)
        // We will remove everything from location step by step (e.g. First remove city then region  ...) and
        // make search again and again untill we get at least 1 escort then continue
        // -----------------------------------
        if (count($escorts) < 1 &&  $locationFilledWithGeo) {

            $looped = true;

            if(isset($geoData['city'])) {
                unset($geoData['city'], $city);
                goto START_FILTERING;
            }

            if(isset($region)) {
                unset($region);
                goto START_FILTERING;
            }

            if(isset($geoData['country'])) {
                unset($geoData['country'], $geoData['country_iso'], $country);
                goto START_FILTERING;
            }
        }
        // -----------------------------------
        $this->view->found_escorts_count = $count;
        $this->view->global_cache_key = $cache_key . $_COOKIE['show_all_escorts'] . $current_currency;

        // Video Merge
        $escorts_video = array();
        if( ! empty($escorts) ) {
            $video_cache_key = "video_escort_list_" . Cubix_Application::getId() . $this->view->global_cache_key;

            if ( !$vescorts = $cache->load($video_cache_key) ) {
                $e_id = array();

                foreach($escorts as $e)
                {
                    if( !is_null($e->id)){
                        $e_id[] = $e->id;
                    }
                }
                if($escorts['vips']){
                    foreach($escorts['vips'] as $e)
                    {
                        $e_id[] = $e->id;
                    }
                }
                if($escorts['premiums']){
                    foreach($escorts['premiums'] as $e)
                    {
                        $e_id[] = $e->id;
                    }
                }
                $video = new Model_Video();
                $vescorts = $video->GetEscortsVideoArray($e_id);
                $cache->save($vescorts, $video_cache_key, array());
            }

            if(!empty($vescorts))
            {
                $app_id = Cubix_Application::getId();
                foreach($vescorts as &$v)
                {
                    $photo = new Cubix_ImagesCommonItem(array(
                        'hash'=> $v->hash,
                        'width'=> $v->width,
                        'height'=> $v->height,
                        'ext'=> $v->ext,
                        'application_id'=> $app_id
                    ));

                    $v->photo = $photo->getUrl('orig');
                    $escorts_video[$v->escort_id] = $v;
                }
                //$config = Zend_Registry::get('videos_config');
                //$host =  Cubix_Application::getById(Cubix_Application::getId())->host;
                //$this->view->video_rtmp = $config['Media'].'mp4:'.$host;
                //$this->view->video_source = $config['remoteUrl'].$host;
            }
        }

        // Hardcode position for VIP Ads, if they match to criteria
        // --------------------------------------------------
        if(!empty($escorts['vips'])) {
            $escorts['vips'] = $this->_hardcode_vips_position([
                'country' => $country,
                'city' => $city,
                'vips' => $escorts['vips']
            ]);
        }
        // --------------------------------------------------

        // Save for every escort, new individual "visit"
        // This means someone has checked her/his profile
        // ------------------------------------------
        $this->_store_visit_on_every_escort([
            'escorts' =>  array_merge((array) $escorts, (array) $escorts['vips'], (array) $escorts['premiums'] ),
            'city' => $city,
        ]);
        // ------------------------------------------

        // Add banner to escorts array
        // -------------------------------------
        if ( !empty($escorts['vips'])) {
            switch (count($escorts['vips'])) {
                case 1:
                case 2:
                    if ($page < 2) {
                        if(count($escorts['vips'])  == 1 && count($escorts['premiums'])) {
                            $escorts['premiums'][] = 'vip-banner';
                        }else{
                            $escorts['vips'][] = 'vip-banner';
                        }
                    }
                    $count++;
                    break;
                default:
                    if ($page < 2) {
                        array_splice($escorts['vips'], $s_config['vipbanner'], 0, 'vip-banner');
                    }
                    $count++;
                    break;
            }
        } elseif (!empty($escorts['premiums'])) {
            switch (count($escorts['premiums'])) {
                case 1:
                case 2:
                    if ($page < 2) {
                        if (count($escorts['premiums']) == 1) {
                            array_splice($escorts, 1, 0, 'vip-banner');
                        } else {
                            $escorts['premiums'][] = 'vip-banner';
                        }
                    }
                    $count++;
                    break;
                default:
                    if ($page < 2) {
                        array_splice($escorts['premiums'], $s_config['vipbanner'], 0, 'vip-banner');
                    }
                    $count++;
                    break;
            }
        } else {

            // Another task from Katie (EDIR-1895)
            // --------------------------------------
            if (count($escorts) > 0) {
                $vips_premiums = 0;
                if ($page < 2) {
                    array_splice($escorts, $s_config['vipbanner'] + $vips_premiums, 0, 'vip-banner');
                }
                $count++;
            }
            // --------------------------------------
        }
        // -------------------------------------

        $this->view->escorts_video = $escorts_video;
        $this->view->pagination_count = $count;
        $this->view->escorts = $escorts;
        $this->view->is_tour = ( !$is_upcomingtour && $is_tour) ? 1 : 0;
        $this->view->is_upcomingtour = $is_upcomingtour ? 1 : 0;
        $this->view->gender = $gender;
        $this->view->is_agency = $is_agency;
        $this->view->is_new = $is_new;
        $this->view->category = $category;

        /* watched escorts */
        if ($list_type != 'simple')
        {
            $escorts_buf = $escorts;
            $ids = array();
            $watched_escorts = array();

            if (isset($escorts_buf['premiums']))
            {
                foreach ($escorts_buf['premiums'] as $e)
                {
                    if(is_string($e)){
                        continue;
                    }
                    $ids[] = $e->id;
                }

                unset($escorts_buf['premiums']);
            }

            if (isset($escorts_buf['vips']))
            {
                foreach ($escorts_buf['vips'] as $e)
                {
                    if(is_string($e)){
                        continue;
                    }
                    $ids[] = $e->id;
                }

                unset($escorts_buf['vips']);
            }

            foreach ($escorts_buf as $e)
            {
                if(is_string($e)){
                    continue;
                }
                $ids[] = $e->id;
            }

            $ids = array_filter($ids);
            if ($ids)
            {
                $ids_str = trim(implode(',', $ids), ',');
                $user = Model_Users::getCurrent();
                $modelM = new Model_Members();

                if ($user)
                {
                    $escorts_watched_type = $user->escorts_watched_type;

                    if (!$escorts_watched_type) $escorts_watched_type = 1;

                    $res = $modelM->getFromWatchedEscortsForListing($ids_str, $escorts_watched_type, $user->id, session_id());
                }
                else
                {
                    $res = $modelM->getFromWatchedEscortsForListing($ids_str, WATCH_TYPE_PER_SESSION, null, session_id());
                }

                if ($res)
                {
                    foreach ($res as $r)
                    {
                        $watched_escorts[] = $r->escort_id;
                    }
                }
            }

            $this->view->watched_escorts = $watched_escorts;
        }
        /**/

        $this->view->has_filter = $has_filter;

        //$this->view->newest_escorts = $newest_escorts;

        $this->view->montly_price_vip = $this->get_montly_price('vip', $country, $p_top_category, $geoData);
        $this->view->montly_price_premium = $this->get_montly_price('premium', $country, $p_top_category, $geoData);
        $current_filter = $this->getCurrentFilter();
        $existing_filters = Model_Escort_List::getActiveFilter($filter_params['filter'], $current_filter, $is_upcomingtour, $is_tour);
        $this->view->lookingFor = (!$is_agency)? $gender : 'agencies';
        $this->view->interestedIn = $category;
        $this->getFilterV2Action($existing_filters);

        if ($this->getRequest()->getHeader('X-PJAX', false)){
            $escort_list = $this->view->render('escorts/list.phtml');
            die(mb_convert_encoding($escort_list, "UTF-8"));
        }

        if ( ! is_null($this->_getParam('ajax')) ) {
//			$current_filter = $this->getCurrentFilter();
//			$existing_filters = Model_Escort_List::getActiveFilter($filter_params['filter'], $current_filter, $is_upcomingtour, $is_tour);

//			$this->getFilterV2Action($existing_filters);

            $filter_body = $this->view->render('escorts/get-filter-v2.phtml');
            $escort_list = $this->view->render('escorts/list.phtml');

            $this->view->setScriptPath('../application/layouts');

            die(json_encode(array('count'=>$count,'filter_body' => $filter_body, 'escort_list' => mb_convert_encoding($escort_list, "UTF-8"))));
        }
    }

    // $advertise_type  - vip or premium
    // $country - 
    // $category- escort, massage, bdsm
    // A countries -  usa, canada, germany, french,  italy, uk
    // B countries -  all 
    public function get_montly_price($advertise_type, $country, $category, $geoData = false)
    {
        if($category == 'escorts'){
            if(!is_null($country)){
                // SELECTED COUNTRY
                if(in_array($country, array(68, 10, 24 , 23, 33, 67 ))){
                    // A COUNTRIES
                    if($advertise_type == 'vip'){
                        return 33;
                    }elseif($advertise_type == 'premium'){
                        return 25;
                    }
                }else{
                    // B COUNTRIES
                    if($advertise_type == 'vip'){
                        return 25;
                    }elseif($advertise_type == 'premium'){
                        return 17;
                    }
                }
            }else{
                // if country geo detected
                if( in_array( strtolower($geoData['country_iso']), array('us', 'ca', 'de' , 'fr', 'it', 'gb' )) ){
                    // A COUNTRIES
                    if($advertise_type == 'vip'){
                        return 33;
                    }elseif($advertise_type == 'premium'){
                        return 25;
                    }
                }else{
                    // B COUNTRIES
                    if($advertise_type == 'vip'){
                        return 25;
                    }elseif($advertise_type == 'premium'){
                        return 17;
                    }
                }
            }
        }elseif( in_array( $category, array( 'bdsm', 'massage' ) ) ){
            if(!is_null($country)){
                // SELECTED COUNTRY
                if(in_array($country, array(68, 10, 24 , 23, 33, 67 ))){
                    // A COUNTRIES
                    return 20;
                }else{
                    // B COUNTRIES
                    return 16;

                }
            }else{
                // if country geo detected
                if( in_array( strtolower($geoData['country_iso']), array('us', 'ca', 'de' , 'fr', 'it', 'gb' )) ){
                    // A COUNTRIES
                    return 20;

                }else{
                    // B COUNTRIES
                    return 16;
                }
            }
        }

    }


    private function _store_visit_on_every_escort($params) {

        if(empty($params['escorts'])) return false;

        // Filtered city
        // ----------------------
        $city = $params['city'];
        // ----------------------

        foreach($params['escorts'] as $_escort) {

            // Validating, because of vips and premiums,
            // there could be no vip or premium for some cities
            if( !empty($_escort) && is_object($_escort)) {
                // Store into redis
                // ------------------------------------
                Model_Analytics::new_escort_visit([
                    'escort' => $_escort->id,
                    'visitor' => Cubix_Geoip::getIP(),
                    'action-type' => Model_Analytics::ACTION_TYPE_LISTING // From which page was the view
                ]);
                // ------------------------------------

                // If the visitor, selected some city from the filters
                // and escort is from that city
                // ---------------------------------------------
                if (!empty($city) && $city == $_escort->city_id) {
                    Model_Analytics::new_escort_visit([
                        'escort' => $_escort->id,
                        'visitor' => Cubix_Geoip::getIP(),
                        'action-type' => Model_Analytics::ACTION_TYPE_CITY, // From which page was the view
                        'city' => $city
                    ]);
                }
                // ---------------------------------------------
            }
        }

    }

    public function profileV2Action()
    {
        if($_GET['ip_check'] == 1){
            var_dump(Cubix_Geoip::getIP());die;
        }
        //Currency rates log
        $default_currency = "USD";
        $current_currency = $this->_getParam('currency', $default_currency);

        if ( $current_currency != $_COOKIE['currency'] && $this->_request->s ) {
            setcookie("currency", $current_currency, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            $_COOKIE['currency'] = $current_currency;
        }

        if ( isset($_COOKIE['currency']) ) {
            $current_currency = $_COOKIE['currency'];
            $this->_request->setParam('currency', $current_currency);
        }

        $this->view->current_currency = $current_currency;
        //Currency rates log

        $this->_helper->viewRenderer->setScriptAction("profile-v2/profile");

        $this->view->is_profile = true;

        // redirect not active escorts to home page
        // -----------------------------------------------------
        $m = new Model_EscortsV2();

        if ($this->_getParam('f') != 'b' && !$m->getShownameById($this->_getParam('escort_id')))
        {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
        }
        // -----------------------------------------------------

        $showname = $this->_getParam('escortName');
        $escort_id = $this->_getParam('escort_id');

        $model = new Model_EscortsV2();

        $cache_key = 'v2_' . $showname . '_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

        $this->view->profile_cache_key = 'html_profile_' . $cache_key;

        $escort = $model->get($escort_id, $cache_key);


        $redirect = false;

        if ($escort->showname != $showname) {
            $redirect = true;
        }


        if ( ! $escort || isset($escort['error']) ) {
            $redirect = true;
        }

        // Save "VISIT" into storage (probably Redis)
        // ----------------------------------------
        Model_Analytics::new_escort_visit([
            'escort' => $escort->id,
            'visitor' => Cubix_Geoip::getIP(),
            'action-type' => Model_Analytics::ACTION_TYPE_PROFILE // From which page was the view
        ]);
        // ----------------------------------------

        $blockModel = new Model_BlockedCountries();
        if ( $blockModel->checkIp($escort->id) ) {
            $redirect = true;
        }


        if ( $redirect && ! $this->view->is_ajax ) {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
            return;
        } else if ( $redirect && $this->view->is_ajax ) {
            die;
        }

        if ( strlen($this->_getParam('f')) && $this->_getParam('f') == 'b' ) {
            $is_preview = true;
        }

        $this->view->is_preview = $is_preview;

        //bubble
        $bubble_cache_key = "escort_message_status_".$escort_id;
        $cache = Zend_Registry::get('cache');
        if ( ! $statusMessage = $cache->load($bubble_cache_key) ) {
            try {
                $client = Cubix_Api_XmlRpc_Client::getInstance();
                $statusMessage = $client->call('Escorts.getBubbleText', array($escort_id));
            }
            catch ( Exception $e ) {
                $statusMessage = false;
            }

            $cache->save($statusMessage, $bubble_cache_key, array());
        }
        $this->view->statusMessage = $statusMessage;
        //bubble

        $vacation = new Model_EscortV2Item( array('id' => $escort->id  ));
        $vac = $vacation->getVacation();
        $allowRevCom = $model->getRevComById($escort->id);
        $bl = $model->getBlockWebsite($escort->id);
        $follow_model = new Model_Follow();
        //$vconfig = Zend_Registry::get('videos_config');
        //$host =  Cubix_Application::getById(Cubix_Application::getId())->host;
        $video = new Model_Video();
        $escort_video = $video->GetEscortVideo($escort->id, 1, NULL, FALSE);

        if (!empty($escort_video[0]))
        {
            if(!empty($escort_video[0][1])) {
                $tmpVideo = &$escort_video[0][1];
            }else{
                $tmpVideo = &$escort_video[0][0];
            }

            $photo = new Cubix_ImagesCommonItem($tmpVideo);
            $this->view->video_id = $escort_video[1][0]->id;
            $this->view->photo_video = $photo->getUrl('orig');
            $this->view->galerry_video = $photo->getUrl('pl');
            $this->view->vwidth = $photo['width'];
            $this->view->vheight = $photo['height'];

            //$this->view->video =$vconfig['remoteUrl'].$host.'/'.$escort->id.'/'.$escort_video[1][0]->video;
        }

        $escort->block_website = $bl->block_website;
        $escort->check_website = $bl->check_website;
        $escort->bad_photo_history = $bl->bad_photo_history;
        $escort->disabled_reviews = $allowRevCom->disabled_reviews;
        $escort->disabled_comments = $allowRevCom->disabled_comments;
        $escort->vac_date_from = $vac->vac_date_from;
        $escort->vac_date_to = $vac->vac_date_to;
        $escort->is_vip = $escort->getIsVip();
        $this->view->escort_id = $escort->id;
        $this->view->escort = $escort;

        $this->view->add_phones = $model->getAdditionalPhones($escort->id);
        $this->view->apps = $model->getApps($escort->id);
        $this->view->natural_pic = $model->getNaturalPic($escort->id);
        $this->view->is_followed = $follow_model->checkIsFollowed("escort", $escort->id);
        $this->view->followers_count = $follow_model->getCount("escort", $escort->id);
        $this->view->DEFINITIONS = Zend_Registry::get('defines');
        $this->view->conf = Zend_Registry::get('escorts_config');
        $this->view->contacts = $escort->getContacts();

        $this->view->langs = Cubix_Application::getLangs();

        $bio = $escort->getBio();
        $bio->languages = $escort->getLangsSortedByLevel();
        $this->view->bio = $escort->arrangeBio($bio);
        $this->view->keywords = $escort->getKeywords();
        $this->view->disabled_rev = (isset($escort->disabled_reviews) &&  $escort->disabled_reviews == 1) ? true : false;
        $this->view->disabled_comm = (isset($escort->disabled_comments) &&  $escort->disabled_comments == 1) ? true : false;
        $this->view->current_user = Model_Users::getCurrent();
        $this->view->setcard_info = $escort->getSetcardInfo();
        $this->view->hand_verification_date = $escort->getHandVerificationDate();
        $this->view->profile_disabled = false;

        if ( isset($escort->escort_status) && $escort->escort_status != 32 ) {
            $this->view->profile_disabled = true;
        }

        $this->view->services = ( $is_preview ) ? $escort->getServicesForPreview() : $escort->getServices();
        $this->view->rates = ( $is_preview ) ? $escort->getRates() : $escort->getHHRates();
        $this->travel_rates = $escort->getTravelRates();
        $this->travel_conditions = $escort->getTravelConditions();
        $this->travel_p_methods = $escort->getTravelPaymentMethods();
        $this->hh_info = $escort->getHHRatesInfo();
        $this->work_times = $escort->getWorkTimes();

        if ( $escort->agency_id ) {
            $this->view->agency_contacts = ( $is_preview ) ? $escort->getAgencyContactsForPreview() : $escort->getAgencyContacts();
            $m_agencies = new Model_Agencies();

            $this->view->agency_details = $m_agencies->getLocalById($escort->agency_id);
        }

        $this->view->esc_contacts = ( $is_preview ) ? $escort->getEscortContactsForPreview() : $escort->getEscortContacts();
        $this->view->online_status = 0;

        $this->view->additional_cities = $escort->getAdditionalCities($this->view->contacts->base_city_id);

        $this->view->viewzones = $escort->getCityzones();
        $this->view->tours = $escort->getCitytours();

        $bt = $model->getBubbleText($escort->id);
        $this->view->bubble_text = $bt ? $bt->bubble_text : null;
        $this->view->travel_place = $model->getTravelPlace($escort->id);
        $this->view->galleries = $escort->getPhotoGalleries($escort->id);
        $this->galleryPhotosAction($escort);

        if ( $escort->agency_id )
            $this->agencyEscortsV2Action($escort->agency_id, $escort->id);

        $user = Model_Users::getCurrent();
        $this->view->user = $user;

        /* watched */
        $model_m = new Model_Members();
        $sess_id = session_id();
        $user_id = null;
        $cur_user = Model_Users::getCurrent();

        if ($cur_user)
        {
            $user_id = $cur_user->id;

            $has = $model_m->getFromWatchedEscortsForUser($sess_id, $escort_id, $user_id, date('Y-m-d'));

            if ($has)
            {
                $model_m->updateToWatchedEscortsForUser($sess_id, $escort_id, $user_id);
            }
            else
            {
                $model_m->addToWatchedEscorts($sess_id, $escort_id, $user_id);
            }
        }
        else
        {
            $has = $model_m->getFromWatchedEscorts($sess_id, $escort_id);

            if ($has)
            {
                $model_m->updateToWatchedEscorts($sess_id, $escort_id);
            }
            else
            {
                $model_m->addToWatchedEscorts($sess_id, $escort_id);
            }
        }
        /**/

        $this->view->escort_user_id = $model->getUserId($escort->id);
    }




    public static function _itemDecorator($item, $is_first = FALSE, $is_hidden = FALSE)
    {
        $has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);

        $classes = array();

        if ( $is_first ) $classes[] = 'first';
        if ( $has_childs ) $classes[] = 'sub';

        $link = '#';
        if ( ! $has_childs ) {
            $link = self::$linkHelper->getLink('escorts', array('filter' => $item->value, 'page' => NULL));
        }

        $title = $item->title;
        if ( $is_first && isset($item->title) ) {
            $title = $item->parent . ' - ' . $item->title;
        }

        $html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="' . $link . '"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ filter: ' . ($item->value ? "'" . $item->value . "'" : 'null') . ', page: null })"' : '' ) . '>' . $title . '</a>';

        if ( $is_first ) {
            $html = '<div class="input-w"><div class="i">' .
                $html .
                '</div></div>';
        }

        return $html;
    }

    public static function _sortItemDecorator($item, $is_first = FALSE, $is_hidden = FALSE)
    {
        $has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);

        $classes = array();

        if ( $is_first ) $classes[] = 'first';
        if ( $has_childs ) $classes[] = 'sub';

        $link = '#';
        if ( ! $has_childs ) {
            $link = self::$linkHelper->getLink('escorts', array('sort' => $item->value));
        }

        $title = $item->title;
        if ( $is_first && isset($item->title) ) {
            $title = $item->parent . ' - ' . $item->title;
        }

        $html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="' . $link . '"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ sort: \'' . $item->value . '\' })"' : '' ) . '>' . $title . '</a>';

        if ( $is_first ) {
            $html = '<div class="input-w"><div class="i">' .
                $html .
                '</div></div>';
        }

        return $html;
    }

    protected function _getFooterText($key = 'main')
    {
        $seo = $this->view->seo('home-page-' . $key, null, array());

        if ( $seo ) {
            if ( strlen($seo->footer_text) ) {
                return $seo->footer_text;
            }
        }

        return '';
    }

    /**
     * Chat Online Escorts Action
     */
    public function chatOnlineAction()
    {
        $action = 'index';
        if ( ! is_null($this->_getParam('ajax')) ) {
            $action = 'list';
        }
        $this->_helper->viewRenderer->setScriptAction($action);

        $req = $this->_getParam('req');
        $param = explode('/', $req);
        $page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

        $ids = '';

        if ($this->view->allowChat2())
        {
            $m_e = new Model_EscortsV2();
            $res = $m_e->getChatNodeOnlineEscorts(1, 1000);
            $ids = $res['ids'];
        }

        if (strlen($ids) > 0)
        {
            $filter = array(
                'e.id IN (' . $ids . ')'
            );

            $page_num = 1;
            if ( isset($page[1]) && $page[1] ) {
                $page_num = $page[1];
            }

            $count = 0;
            $escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 50, $count);
        }
        else
        {
            $count = 0;
            $escorts = array();
        }

        $this->view->count = $count;
        $this->view->escorts = $escorts;

        list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

        $this->view->chat_online = true;
    }

    /**
     * Happy Hours action
     */
    public function happyHoursAction()
    {
        $action = 'index';
        if ( ! is_null($this->_getParam('ajax')) ) {
            $action = 'list';
        }
        $this->_helper->viewRenderer->setScriptAction($action);

        $req = $this->_getParam('req');
        $param = explode('/', $req);
        $page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

        $filter = array('e.gender = ?' => 1, 'e.hh_is_active = ?' => 1);

        $page_num = 1;
        if ( isset($page[1]) && $page[1] ) {
            $page_num = $page[1];
        }

        $count = 0;
        $escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 10, $count);

        $this->view->count = $count;
        $this->view->escorts = $escorts;

        list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

        $this->view->hh_page = true;
    }

    /**
     * Main premium spot action
     */
    public function mainPageAction()
    {
        $action = 'index';
        if ( ! is_null($this->_getParam('ajax')) ) {
            $action = 'list';
        }
        $this->_helper->viewRenderer->setScriptAction($action);

        $escorts = Model_Escort_List::getMainPremiumSpot();

        $this->view->escorts = $escorts;
        $this->view->no_paging = false;

        list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();
        //$this->view->e_ids = Model_EscortsV2::getLateNightGirlsForMainPage();
        //print_r($this->view->e_ids);

        //////////////////////////////////////////////////////////////
        if ($this->view->allowChat2())
        {
            $model = new Model_EscortsV2();
            $res = $model->getChatNodeOnlineEscorts(1, 1000);
            $ids = $res['ids'];
            $this->view->escort_ids = explode(',', $ids);
        }
        /////////////////////////////////////////////////
    }

    /**
     * New escorts action
     */
    public function newListAction()
    {
        $this->view->layout()->setLayout('main-simple');

        $model = new Model_EscortsV2();

        $req = $this->_getParam('req');
        $param = explode('/', $req);
        $page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

        $this->view->country_id = $country_id = $this->_request->country_id;
        //print_r($country_id);
        $filter = array('e.gender = ?' => 1);

        if ( $country_id ) {
            $filter = array_merge($filter, array('e.country_id = ?' => $country_id));
        }

        $page_num = 1;
        if ( isset($page[1]) && $page[1] ) {
            $page_num = $page[1];
        }

        $count = 0;
        $escorts = Model_Escort_List::getFiltered($filter, 'newest', $page_num, 10, $count, 'new_list');

        if ( count($escorts) ) {
            foreach ( $escorts as $k => $escort ) {
                $cache_key = 'v2_' . $escort->showname . '_new_profile_' . Cubix_I18n::getLang() . '_page_' . $page_num . '_' . $escort->id;
                $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
                $escorts[$k]->profile = $model->get($escort->id, $cache_key);
            }
        }

        $this->view->count = $count;
        $this->view->escorts = $escorts;

        $this->view->new_list_cache_key = 'new_list_' . $cache_key;
    }

    public function ajaxBubbleAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $page = intval($this->_getParam('page', 1));
        if ( $page < 1 ) $page = 1;
        $per_page = 20;

        $c = $this->_request->c;

        if (!in_array($c, array('escorts', 'trans', 'boys', 'bdsm')))
            $c = 'escorts';

        echo $this->view->bubbleTextsWidget($c, $page, $per_page);
    }

    public function ajaxOnlineAction()
    {
        $page = intval($this->_getParam('page', 1));
        if ( $page < 1 ) $page = 1;
        $per_page = 5;
        $this->view->layout()->disableLayout();

        if ($this->view->allowChat2())
        {
            $model = new Model_EscortsV2();
            $results = $model->getChatNodeOnlineEscorts($page, $per_page);

            $this->view->escorts = $results['escorts'];
            $this->view->per_page = $per_page;
            $this->view->page = $page;
            $this->view->count = $results['count'];
        }
    }

    /**
     *
     * @param string $r
     */
    protected function _makeFilterArgument($r)
    {
        $param = explode('_', $r);

        if ( count($param) < 2 ) {
            switch( $r ) {
                case 'independents':
                case 'agency':
                case 'boys':
                case 'trans':
                case 'citytours':
                case 'upcomingtours':
                    return array('field' => $r, 'value' => array());
            }
        }

        $param_name = $param[0];
        array_shift($param);

        switch ( $param_name ) {
            case 'filter':
                $field = reset($param);
                array_shift($param);

                $value = array_slice($param, 0, 2);

                $filter = array('field' => $field, 'value' => $value , 'main' => TRUE);
                break;
            case 'region':
            case 'state':
                $filter = array('field' => 'region', 'value' => $param[1]);
                break;
            case 'city':
            case 'cityzone':
                $filter = array('field' => $param_name, 'value' => array($param[1]));
                break;
            case 'name':
                $filter = array('field' => 'name', 'value' => array('%' . $param[0] . '%'));
                break;
        }

        return $filter;
    }

    /**
     * Calculates filter counts for each
     *
     * @param integer $count
     * @param array $filterParams
     * @param Cubix_NestedMenu $menu
     * @param string $cacheKey
     */
    protected function _calcFilterCounts($count, $filterParams, Cubix_NestedMenu $menu, $cacheKey = null)
    {
        $model = new Model_EscortsV2();
        $defs = Zend_Registry::get('defines');

        $filterMap = array(
            'svc-anal' => 'e.svc_anal = ?',
            'svc-kissing' => 'e.svc_kissing = ?',
            'svc-kissing-with-tongue' => 'e.svc_kissing = ?',
            'svc-blowjob-with-condom' => 'e.svc_blowjob = ?',
            'svc-blowjob-without-condom' => 'e.svc_blowjob = ?',
            'svc-cumshot-in-face' => 'e.svc_cumshot = ?',
            'svc-cumshot-in-mouth-swallow' => 'e.svc_cumshot = ?',
            'svc-cumshot-in-mouth-spit' => 'e.svc_cumshot = ?',

            'age' => 'e.age BETWEEN ? AND ?',
            'ethnic' => 'e.ethnicity = ?',
            'height' => 'e.height > ? AND e.height < ?',
            'weight' => 'e.weight > ? AND e.weight < ?',
            'breast-size' => 'e.breast_size = ?',
            'hair-color' => 'e.hair_color = ?',
            'hair-length' => 'e.hair_length = ?',
            'eye-color' => 'e.eye_color = ?',
            'dress-size' => 'e.dress_size = ?',
            'shoe-size' => 'e.shoe_size = ?',
            'available-for' => 'e.availability = ?',
            'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
            'smoker' => 'e.is_smoker = ?',
            'language' => 'FIND_IN_SET(?, e.languages)',
            'now-open' => 'e.is_now_open',
            'region' => 'r.slug = ?',
            'city' => 'cc.slug = ?',
            'cityzone' => 'c.id = ?',
            'name' => 'e.showname LIKE ?',

            'new_arrivals' => 'e.gender = ' . GENDER_FEMALE,
            'independent' => 'e.agency_id IS NULL AND e.gender = ' . GENDER_FEMALE,
            'agency' => 'e.agency_id IS NOT NULL AND e.gender = ' . GENDER_FEMALE,
            'boys' => 'e.gender = ' . GENDER_MALE,
            'trans' => 'e.gender = ' . GENDER_TRANS,
            'girls' => 'e.gender = ' . GENDER_FEMALE,

            'tours' => 'e.is_on_tour = 1',
            'upcomingtours' => 'e.tour_date_from > CURDATE()'
        );

        $counts = array();

        $filterStructure = $defs['escorts_filter'];

        foreach ( $filterStructure as $i => $filter ) {
            $fake = false;
            if ( ! isset($filter['childs']) ) {
                $filter['childs'] = array($filter);
                $fake = true;
            }

            foreach ( $filter['childs'] as $j => $child ) {
                $f = $this->_makeFilterArgument('filter_' . $child['value']);

                $newFilter = $filterParams;

                if ( ! isset($filterMap[$f['field']]) ) continue;

                $value = $f['value'];

                if ( isset($f['main']) && $f['main'] ) {
                    if ( isset($child['internal_value']) ) {
                        $value = $child['internal_value'];
                    }
                }

                $newFilter['filter'][$filterMap[$f['field']]] = $value;

                $count = 0; // $model->getCount($newFilter);

                if ( $fake ) {
                    $filterStructure[$i]['title'] .= " ($count)";
                }
                else {
                    $filterStructure[$i]['childs'][$j]['title'] .= " ($count)";;
                }
            }
        }

        if ( ! is_null($cacheKey) ) {
            Zend_Registry::get('cache')->save($filterStructure, $cacheKey, array());
        }

        $menu->setStructure(array('childs' => $filterStructure));
    }

    public function profileAction()
    {
        $this->view->layout()->setLayout('profile');

        $this->view->is_profile = true;

        //////   redirect not active escorts to home page   ///////
        $m = new Model_EscortsV2();
        if ($this->_getParam('f') != 'b' && !$m->getShownameById($this->_getParam('escort_id')))
        {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
        }
        //////////////////////////////////////////////////////////

        $showname = $this->_getParam('escortName');
        $escort_id = $this->_getParam('escort_id');

        $model = new Model_EscortsV2();

        $cache_key = 'v2_' . $showname . '_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

        $this->view->profile_cache_key = 'html_profile_' . $cache_key;

        //$escort = $model->get($showname, $cache_key);
        $escort = $model->get($escort_id, $cache_key);

        if ($escort->showname != $showname)
        {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
        }

        if ( ! $escort || isset($escort['error']) )
        {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
            return;
        }

        /*if ( isset($escort->escort_status) && $escort->escort_status != 32 ) {
            $this->_forward('profile-disabled');
            $this->_request->setParam('city_slug', $escort->city_slug);
        }*/

        $blockModel = new Model_BlockedCountries();
        if ( $blockModel->checkIp($escort->id) ){
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /');
            die;
            return;
        }

        $vacation = new Model_EscortV2Item( array('id' => $escort->id  ));
        $vac = $vacation->getVacation();
        $add_esc_data = $model->getRevComById($escort->id);
        $bl = $model->getBlockWebsite($escort->id);
        $block_website = $bl->block_website;
        $check_website = $bl->check_website;

        $escort->block_website = $block_website;
        $escort->check_website = $check_website;
        $escort->disabled_reviews = $add_esc_data->disabled_reviews;
        $escort->disabled_comments = $add_esc_data->disabled_comments;
        $escort->vac_date_from = $vac->vac_date_from;
        $escort->vac_date_to = $vac->vac_date_to;
        $this->view->escort_id = $escort->id;
        $this->view->escort = $escort;

        if ( $escort->home_city_id ) {
            $m_city = new Cubix_Geography_Cities();
            $home_city = $m_city->get($escort->home_city_id);

            $m_country = new Cubix_Geography_Countries();
            $home_country = $m_country->get($home_city->country_id);

            $this->view->home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
        }

        $bt = $model->getBubbleText($escort->id);
        $this->view->bubble_text = $bt ? $bt->bubble_text : null;

        $this->view->travel_place = $model->getTravelPlace($escort->id);
        /* Last Review */
        $cache = Zend_Registry::get('cache');
        $cache_key = 'reviews_' . $escort->id;
        if ( ! $res = $cache->load($cache_key) ) {
            try {
                $res = $model->getEscortLastReview($escort->id);
            }
            catch ( Exception $e ) {
                $res = array(false, false);
            }

            $cache->save($res, $cache_key, array());
        }

        list($review, $rev_count) = $res;
        $this->view->review = $review;
        $this->view->rev_count = $rev_count;
        /* Last Review */

        $this->photosAction($escort);

        $scheduleModel = new Model_Schedule();
        $this->view->schedules = $scheduleModel->getEscortSchedule( $escort->id, true );


        if ( $escort->agency_id )
            $this->agencyEscortsAction($escort->agency_id, $escort->id);

        $user = Model_Users::getCurrent();
        $this->view->user = $user;

        $paging_key = $this->_getParam('from');
        $paging_keys_map = array(
            'last_viewed_escorts',
            'search_list',
            'regular_list',
            'new_list',
            'profile_disabled_list',
            'premiums',
            'right_premiums',
            'main_premium_spot',
            'tours_list',
            'tour_main_premium_spot',
            'up_tours_list',
            'up_tour_main_premium_spot'
        );

        if ( in_array($paging_key, $paging_keys_map) ) {
            $sid = 'sedcard_paging_' . Cubix_Application::getId();
            $ses = new Zend_Session_Namespace($sid);

            $ses_pages = $ses->{$paging_key};
            $criterias = $ses->{$paging_key . '_criterias'};

            if ( ! isset($criterias['first_page']) ) {
                $criterias['first_page'] = $criterias['page'];
            }
            if ( ! isset($criterias['last_page']) ) {
                $criterias['last_page'] = $criterias['page'];
            }

            $ses_index = 0;
            if ( is_array($ses_pages) ) {
                $ses_index = array_search($escort_id, $ses_pages);
            }

            if ( isset($ses_pages[$ses_index - 1]) ) {
                $this->view->back_showname = $model->getShownameById($ses_pages[$ses_index - 1]);
                $this->view->back_id = $back_id = $ses_pages[$ses_index - 1];
            } elseif ( $criterias['first_page'] > 1 ) { //Loading previous page escorts
                switch( $paging_key ) {
                    case 'regular_list' :
                        $prev_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
                        break;
                    case 'tours_list':
                        $prev_page_escorts = Model_Escort_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
                        break;
                }

                $p_escorts_count = count($prev_page_escorts);
                if ( count($p_escorts_count) ) {
                    $this->view->back_showname = $prev_page_escorts[$p_escorts_count - 1]->showname;
                    $this->view->back_id = $prev_page_escorts[$p_escorts_count - 1]->id;
                    //Adding previous page escorts to $ses_pages and set in session
                    $ids = array();
                    foreach($prev_page_escorts as $p_esc) {
                        $ids[] = $p_esc->id;
                    }

                    $criterias['first_page'] -= 1;
                    $ses->{$paging_key} = array_merge($ids, $ses_pages);
                    $ses->{$paging_key . '_criterias'} = $criterias;
                }
            }

            if ( isset($ses_pages[$ses_index + 1]) ) {
                $this->view->next_showname = $model->getShownameById($ses_pages[$ses_index + 1]);
                $this->view->next_id = $next_id = $ses_pages[$ses_index + 1];
            } else { //Loading next page escorts
                switch( $paging_key ) {
                    case 'regular_list' :
                        $next_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
                        break;
                    case 'main_premium_spot':
                        $next_page_escorts = array();
                        break;
                    case 'tours_list':
                        $next_page_escorts = Model_Escort_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
                        break;
                }

                if ( count($next_page_escorts) ) {
                    $this->view->next_showname = $next_page_escorts[0]->showname;
                    $this->view->next_id = $next_page_escorts[0]->id;
                    //Adding next page escorts to $ses_pages and set in session
                    foreach($next_page_escorts as $p_esc) {
                        $ses_pages[] = $p_esc->id;
                    }

                    $criterias['last_page'] += 1;
                    $ses->{$paging_key} = $ses_pages;
                    $ses->{$paging_key . '_criterias'} = $criterias;
                }
            }
            $this->view->paging_key = $paging_key;
        }

        /*list($prev, $next) = Model_Escort_List::getPrevNext($escort->showname);

        if ( ! is_null($prev) ) {
            $this->view->back_showname = $prev;
        }
        if ( ! is_null($next) ) {
            $this->view->next_showname = $next;
        }*/

        list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();
    }

    public function agencyEscortsV2Action($agency_id = null, $escort_id = null)
    {
        $config = Zend_Registry::get('escorts_config');

        if ( ! $agency_id )
        {
            $this->view->layout()->disableLayout();
            $agency_id = $this->view->agency_id = $this->_getParam('agency_id');
        }
        else
            $this->view->agency_id = $agency_id;

        if( ! $escort_id )
            $escort_id = $this->view->escort_id = $this->_getParam('escort_id');

        $page = intval($this->_getParam('agency_page'));
        if ( $page < 1 )
            $page = 1;

        $default_size = "s";
        $current_size = $this->_getParam('view_type', $default_size);

        if ( $current_size != $_COOKIE['ae-view-type'] && $this->_request->s ) {
            setcookie("ae-view-type", $current_size, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            $_COOKIE['ae-view-type'] = $current_size;
        }

        if ( isset($_COOKIE['ae-view-type']) ) {
            $current_size = $_COOKIE['ae-view-type'];
        }

        $small_per_page = 14;
        $large_per_page = 3;

        $per_page = ($current_size == "l") ? $large_per_page : $small_per_page;

        $params = array('e.agency_id = ?' => (int)$agency_id,'e.id <> ?' => (int)$escort_id/*, 'show_all_agency_escorts' => 1*/);

        $escorts_count = 0;
        $escorts = Model_Escort_List::getAgencyEscorts($params, 'newest', $page, $per_page, $escorts_count);

        if (count($escorts) ) {
            $this->view->a_escorts = $escorts;
            $this->view->a_escorts_count = $escorts_count;

            $this->view->agencies_page = $page;
            $this->view->agencies_per_page = $per_page;
        }

        $m_esc = new Model_EscortsV2();
        $agency = $m_esc->getAgency($escort_id);

        $this->view->agency = new Model_AgencyItem($agency);
        $this->view->is_ajax = $this->_getParam('ajax', false);
        $this->view->size = $current_size;
    }

    public function profileDisabledAction()
    {
        $model = new Model_EscortsV2();
        $count = 0;

        $this->view->no_layout_banners = true;
        $this->view->no_paging = true;
        $this->view->no_js = true;

        $params = array('e.gender = 1' => array(), 'ct.slug = ?' => array($this->_request->city_slug));
        $escorts = Model_Escort_List::getFiltered($params, 'random', 1, 50, $count);

        $this->view->count = $count;
        $this->view->escorts = $escorts;
    }

    public function agencyEscortsAction($agency_id = null, $escort_id = null)
    {
        $config = Zend_Registry::get('escorts_config');

        if ( ! $agency_id )
        {
            $this->view->layout()->disableLayout();
            $agency_id = $this->view->agency_id = $this->_getParam('agency_id');
        }
        else
            $this->view->agency_id = $agency_id;

        if( ! $escort_id )
            $escort_id = $this->view->escort_id = $this->_getParam('escort_id');

        $page = intval($this->_getParam('agency_page'));
        if ( $page < 1 )
            $page = 1;

        $params = array('e.agency_id = ?' => $agency_id,'e.id <> ?' => $escort_id);

        $escorts_count = 0;
        $escorts = Model_Escort_List::getFiltered($params, 'random', $page, $config['widgetPerPage'], $escorts_count);

        if (count($escorts) ) {
            $this->view->a_escorts = $escorts;
            $this->view->a_escorts_count = $escorts_count;

            $this->view->agencies_page = $page;
            $this->view->widgetPerPage = $config['widgetPerPage'];

            $m_esc = new Model_EscortsV2();
            $agency = $m_esc->getAgency($escort_id);

            $this->view->agency = new Model_AgencyItem($agency);
        }
    }

    public function photosV2Action($escort = null)
    {
        $config = Zend_Registry::get('escorts_config');

        $page = $this->_getParam('photo_page');

        $escort_id = $this->_getParam('escort_id');
        $mode = $this->_getParam('mode');

        if( $mode == 'ajax' ) {
            $this->view->layout()->disableLayout();
            $model = new Model_EscortsV2();
            $escort = $model->get($escort_id);
            $this->view->escort_id = $escort_id;
        }

        if ( ! $page )
            $page = 1;

        $count = 0;
        if ( $this->view->is_preview || $this->_getParam('is_preview') ) {
            $photos = $escort->getPhotosApi($page, $count, true, true);
        }
        else {
            $count = 100;
            $photos = $escort->getPhotos($page, $count, true, true, null, null, false, true);
        }
        $this->view->photos = $photos;
        $this->view->photos_count = $count;
        $this->view->photos_page = $page;

        $this->view->photos_perPage = $config['photos']['perPage'];
    }

    public function galleryPhotosAction($escort = null)
    {
        $config = Zend_Registry::get('escorts_config');
        $page = 1;
        $count = 0;
        $galery_id = $this->_getParam('gallery_id');

        if($this->_request->isXmlHttpRequest()){
            $this->view->layout()->disableLayout();
            $this->_helper->viewRenderer->setScriptAction("profile-v2/gallery-photos");
            $escort_id = intval($this->_getParam('escort_id'));
            $showname = $this->_getParam('showname');
            $cache_key = 'v2_' . $showname . '_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
            $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
            $model = new Model_EscortsV2();
            $escort = $model->get($escort_id,$cache_key);

            /*if ($escort->showname != $showname)
            {
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: /');
                die;
            }*/

            $city = new Cubix_Geography_Cities();
            $city = $city->get($escort->base_city_id);
            $this->_request->setParam('region', $city->region_slug);
            $this->view->base_city = $city;

            if ( ! $escort || isset($escort['error']) )
            {
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: /');
                die;
                return;
            }

            $blockModel = new Model_BlockedCountries();
            if ( $blockModel->checkIp($escort->id) ){
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: /');
                die;
                return;
            }
            $this->view->escort = $escort;
            $this->view->escort_id = $escort_id;

        }

        if ( $this->view->is_preview || $this->_getParam('is_preview') || $this->_getParam('f') == 'b' ) {
            $photos = $escort->getPhotosApi($page, $count, true, true, $galery_id);

        }
        else {
            $count = 100;
            $photos = $escort->getPhotos($page, $count, true, false, null, null, false, true, $galery_id);
        }
        $this->view->photos = $photos;
        $this->view->photos_count = $count;
        $this->view->photos_page = $page;
        $this->view->photos_perPage = $config['photos']['perPage'];

    }

    public function photosAction($escort = null)
    {
        $config = Zend_Registry::get('escorts_config');

        $page = $this->_getParam('photo_page');

        $escort_id = $this->_getParam('escort_id');
        $mode = $this->_getParam('mode');

        if( $mode == 'ajax' ) {
            $this->view->layout()->disableLayout();
            $model = new Model_EscortsV2();
            $escort = $model->get($escort_id);
            $this->view->escort_id = $escort_id;
        }

        if ( ! $page )
            $page = 1;

        $count = 0;
        $photos = $escort->getPhotos($page, $count, false);
        $this->view->photos = $photos;
        $this->view->photos_count = $count;
        $this->view->photos_page = $page;

        $this->view->photos_perPage = $config['photos']['perPage'];
    }

    public function privatePhotosAction($escort = null)
    {
        $config = Zend_Registry::get('escorts_config');

        $page = $this->_getParam('photo_page');
        $escort_id = $this->_getParam('escort_id');

        if( $escort_id )
        {
            $this->view->layout()->disableLayout();
            $model = new Model_EscortsV2();
            $escort = $model->get($escort_id);
            $this->view->escort_id = $escort_id;
        }

        if ( ! $page )
            $page = 1;

        $count = 0;
        $this->view->private_photos = $escort->getPhotos($page, $count, false, true);
        $this->view->private_photos_count = $count;
        $this->view->private_photos_page = $page;
        $this->view->private_photos_perPage = $config['photos']['perPage'];
    }

    public function viewedEscortsAction()
    {
        $this->view->layout()->disableLayout();
        $escort_id = intval($this->_getParam('escort_id'));

        if ( $escort_id )
        {
            self::_addToLatestViewedList($escort_id);
            $viewedEscorts = self::_getLatestViewedList(7, $escort_id);
        }
        else
            $viewedEscorts = self::_getLatestViewedList(7);

        if ( ! is_array($viewedEscorts) || count($viewedEscorts) == 0 )
            return false;

        $ve = array();

        $ids = array();

        foreach ( $viewedEscorts as $e ) {
            $ids[] = $e['id'];
        }

        $filter = array('e.id IN (' . implode(', ', $ids) . ')' => array(), 'show_all_agency_escorts' => array(), 'remove_package_query' => array());
        $order = 'FIELD(eic.escort_id, ' . implode(', ', $ids) . ')';

        $count = 0;
        $escorts = Model_Escort_List::getFiltered($filter, $order, 1, 7, $count);
//var_dump($ids);

        $this->view->escorts = $escorts;
        $this->view->count = $count;
    }

    public function lateNightGirlsAction()
    {
        $this->view->layout()->disableLayout();

        $page = intval($this->_request->l_n_g_page);

        if ($page < 1) $page = 1;

        list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls($page);

        $this->view->l_n_g_page = $page;
    }

    private function _getLateNightGirls($page = 1)
    {
        return Model_EscortsV2::getLateNightGirls($page);
    }

    private function _addToLatestViewedList($escort_id)
    {
        $count = 7; // actually we need 6
        $escort = array('id' => $escort_id);
        $sid = 'last_viewed_escorts_' . Cubix_Application::getId();

        $ses = new Zend_Session_Namespace($sid);

        // Checking if session exists
        // creating if not exists
        if ( false == (isset($ses->escorts) && is_array($ses->escorts)) )
        {
            $ses->escorts = array();
        }
        else
        {
            // Checking if escort exists in stack
            // removing escort from stack if exists
            foreach ($ses->escorts as $key => $item)
            {
                if ( $escort['id'] == $item['id'] )
                {
                    array_splice($ses->escorts, $key, 1);
                }
            }
        }

        // Pushing escort to the end of the stack
        array_unshift($ses->escorts, $escort);

        // Checking if
        if ( count($ses->escorts) > $count )
        {
            array_pop($ses->escorts);
        }
        //var_dump($ses->escorts);
    }

    // Getting latest viewed escorts list (stack)
    // as Array of Assoc
    public function _getLatestViewedList($max, $exclude = NULL)
    {
        $sid = 'last_viewed_escorts_' . Cubix_Application::getId();

        $ses = new Zend_Session_Namespace($sid);

        if ( ! is_array($ses->escorts) )
            return array();

        if ( $exclude )
        {
            $arr = $ses->escorts;
            foreach($arr as $key => $item)
            {
                if($exclude == $item['id'])
                {
                    array_splice($arr, $key, 1);
                }
            }
            $ret = array_slice($arr, 0, $max);

            return $arr;
        }

        $ret = array_slice($ses->escorts, 0, $max);

        return $ret;
    }

    public function searchAction()
    {
        $this->view->layout()->setLayout('main-simple');


        $this->view->quick_links = false;

        $this->view->defines = Zend_Registry::get('defines');

        if( $this->_getParam('search') )
        {
            $params['filter'] = array();
            $params['order'] = array();

            $this->view->params = $this->_request->getParams();

            if( $this->_getParam('order') ) {

                $order_direction = '';
                $order_field = 'date_registered';

                switch($this->_getParam('order')){
                    case 'lastmodified':
                        $order_field = "date_last_modified";
                        break;
                    case 'popularity':
                        $order_field = "hit_count";
                        break;
                }

                if($this->_getParam('order_direction')){
                    $order_direction = $this->_getParam('order_direction');
                }
                $params['order'] = $order_field." ".$order_direction;

            }


            if($this->_getParam('page')){
                $params['limit']['page'] = $this->_getParam('page');
            }

            if( $this->_getParam('publish') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'DATEDIFF(NOW(),e.date_registered) <= ?' =>  trim($this->_getParam('days'))
                ));
            }

            if( $this->_getParam('showname') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.showname LIKE ?' => "%" . trim($this->_getParam('showname')) . "%"
                ));
            }

            if( $this->_getParam('agency_slug') && $this->_getParam('agency') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.agency_slug LIKE ?' => "%" . trim($this->_getParam('agency_slug')) . "%"
                ));
            }

            if( $this->_getParam('gender') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.gender = ?' => $this->_getParam('gender')
                ));
            }

            if ( $this->_getParam('country_id') && $this->_getParam('country_id') ) {
                $params['filter'][] = '(e.country_id= ' . $this->_getParam('country_id') . ')'; //. ' OR ' . ' etc.continent_id= ' . Model_Countries::getContinentId($this->_getParam('country_id'))
            }

            if ( $this->_getParam('city_id') && $this->_getParam('city_id') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'ct.id = ?' => $this->_getParam('city_id')
                ));
            }
            /*if( $this->_getParam('city') &&  $this->_getParam('city_slug') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'cc.slug = ?' => $this->_getParam('city_slug')
                ));
            }*/

            if( $this->_getParam('phone') ) {
                $phone = preg_replace('/^\+/', '00',$this->_getParam('phone'));
                $phone = preg_replace('/[^0-9]+/', '', $phone);
                $params['filter'] = array_merge($params['filter'], array(
                    'e.contact_phone_parsed LIKE ?' => "%".$phone."%"
                ));
            }

            if( $this->_getParam('available_for_incall') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.incall_type IS NOT NULL' => true
                ));
            }
            if( $this->_getParam('available_for_outcall') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.outcall_type IS NOT NULL' => true
                ));
            }

            if( $this->_getParam('nationality') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.nationality_id = ?' => $this->_getParam('nationality')
                ));
            }

            if( $this->_getParam('saf') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'FIND_IN_SET( ?, e.sex_availability)' => $this->_getParam('saf')
                ));
            }

            if( $this->_getParam('price_from') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.outcall_price >= ?' => $this->_getParam('price_from')
                ));
            }

            if( $this->_getParam('price_to') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.outcall_price <= ?' => $this->_getParam('price_to')
                ));
            }

            if( $this->_getParam('words') )
            {
                if ($this->_getParam('words_type') == 1)
                {
                    if ($pos = strpos($this->_getParam('words'), ' ')) {
                        $sub = substr($this->_getParam('words'), 0, $pos);
                    }
                    else {
                        $sub = $this->_getParam('words');
                    }

                    $params['filter'] = array_merge($params['filter'], array(
                        'e.' . Cubix_I18n::getTblField('about') . ' = ?' =>  $sub
                    ));
                }
                elseif ($this->_getParam('words_type') == 2)
                {
                    $params['filter'] = array_merge($params['filter'], array(
                        'e.' . Cubix_I18n::getTblField('about') . ' LIKE ?' => "%" . $this->_getParam('words') . "%"
                    ));
                }
            }

            /* ------------ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<< --------------- */

            if( $this->_getParam('age_from') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.age >= ?' => $this->_getParam('age_from')
                ));
            }
            if( $this->_getParam('age_to') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.age <= ?' => $this->_getParam('age_to')
                ));
            }

            if( $this->_getParam('ethnicity') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.ethnicity = ?' => $this->_getParam('ethnicity')
                ));
            }

            if( $this->_getParam('hair_color') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.hair_color = ?' => $this->_getParam('hair_color')
                ));
            }

            if( $this->_getParam('hair_length') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.hair_length = ?' => $this->_getParam('hair_length')
                ));
            }

            if( $this->_getParam('eye_color') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.eye_color = ?' => $this->_getParam('eye_color')
                ));
            }

            if( $this->_getParam('height_from') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.height >= ?' => $this->_getParam('height_from')
                ));
            }
            if( $this->_getParam('height_to') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.height <= ?' => $this->_getParam('height_to')
                ));
            }

            if( $this->_getParam('dress_size') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.dress_size = ?' => $this->_getParam('dress_size')
                ));
            }

            if( $this->_getParam('shoe_size') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.shoe_size = ?' => $this->_getParam('shoe_size')
                ));
            }

            if( $this->_getParam('cup_size') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.cup_size = ?' => $this->_getParam('cup_size')
                ));
            }

            if( $this->_getParam('pubic_hair') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.pubic_hair = ?' => $this->_getParam('pubic_hair')
                ));
            }

            if( $this->_getParam('smoker') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.is_smoking = ?' => $this->_getParam('smoker')
                ));
            }

            if( $this->_getParam('drinking') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.is_drinking = ?' => $this->_getParam('drinking')
                ));
            }
            if( $this->_getParam('language') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'e.languages LIKE ?' => "%".$this->_getParam('language')."%"
                ));
            }
            /* Services */
            if( $this->_getParam('services') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'es.service_id = ?' => $this->_getParam('services')
                ));
            }
            /* Services */
            /* Working Times */
            if ( $this->_getParam('travel_date') ) {
                $params['filter'] = array_merge($params['filter'], array(
                    'DATE(sch.day) = ?' => date('Y-m-d', $this->_getParam('travel_date')),
                    'sch.type = ?'	=> Model_Schedule::AVAILABLE_DAY
                ));
            }

            if( $this->_getParam('day_index') ) {
                $dateTimes = array();
                $counter = 0;
                foreach($this->_getParam('day_index') as $day => $value){
                    $timeFrom = $this->_getParam('time_from');
                    $timeFromM = $this->_getParam('time_from_m');
                    $timeTo = $this->_getParam('time_to');
                    $timeToM = $this->_getParam('time_to_m');
                    $dateTimes[$counter]['day_index'] = $day;
                    $dateTimes[$counter]['time_from'] = $timeFrom[$day];
                    $dateTimes[$counter]['time_from_m'] = $timeFromM[$day];
                    $dateTimes[$counter]['time_to'] = $timeTo[$day];
                    $dateTimes[$counter]['time_to_m'] = $timeToM[$day];
                    $counter++;
                }
                $params['filter'] = array_merge($params['filter'], array(
                    'working_times = ?' => $dateTimes
                ));
            }

            /* Working Times */

            $page = 1;
            $page_size = 54;
            if($this->_getParam('page')){
                $page = $this->_getParam('page');
            }

            $model = new Model_EscortsV2();
            $count = 0;

            $escorts = $model->getSearchAll($params, $count,$page,$page_size);
            $x['times'] = $dateTimes;

            $this->view->data = $x;
            $this->view->page = $page;
            $this->view->count = $escorts['count'];
            $this->view->escorts = $escorts['data'];
            $this->view->search_list = true;
        }
    }

    public function gotmAction()
    {
        $this->view->layout()->setLayout('main-simple');
        $show_history = (bool) $this->_getParam('history');



        $page = intval($this->_getParam('page', 1));
        if ( $page < 1 ) $page = 1;
        $this->view->per_page = $per_page = 40;
        $this->view->page = $page;

        $result = Model_EscortsV2::getMonthGirls($page, $per_page, $show_history);



        $this->view->escorts = $escorts = $result['escorts'];
        $this->view->count = $result['count'];

        if ( ! $show_history ) {
            $this->view->gotm = Model_EscortsV2::getCurrentGOTM();
        }
        else {
            $this->_helper->viewRenderer->setScriptAction('gotm-history');
        }
    }

    /**
     * Voting Widget Action
     */
    public function voteAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $response = array('status' => null, 'desc' => '', 'html' => '');

        $showname = $this->_getParam('showname');
        $escort_id = $this->_getParam('escort_id');
        $model = new Model_EscortsV2();
        //$escort = $model->get($showname);
        $escort = $model->get($escort_id);

        $user = Model_Users::getCurrent();

        if ( ! $user ) {
            $response['status'] = 'error';
            $response['desc'] = 'not signed in';
        }
        elseif ( 'member' != $user->user_type ) {
            $response['status'] = 'error';
            $response['desc'] = 'only members are allowed to vote';
        }
        elseif ( ! $escort ) {
            $response['status'] = 'error';
            $response['desc'] = 'invalid escort showname';
        }
        else {
            try {
                $result = $escort->vote($user->id);

                if ( $result !== true ) {
                    $response['status'] = 'error';
                    $response['desc'] = 'member has already voted';
                }
                else {
                    $response['status'] = 'success';
                    $response['html'] = $this->view->votingWidget($escort, true);
                }
            }
            catch ( Exception $e ) {
                $response['status'] = 'error';
                $response['desc'] = 'unexpected error';
            }
        }

        echo json_encode($response);
    }

    public function bookMeAction()
    {
        $this->view->layout()->disableLayout();

        $escort_id = $this->view->escort_id = $this->_request->escort_id;
        $showname = $this->view->showname = $this->_request->showname;

        $model = new Model_EscortsV2();

        $cache_key = 'v2_' . $showname . '_profile_' . Cubix_I18n::getLang();
        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

        $this->view->escort = $model->get($showname, $cache_key);

        $errors = array();

        if ( $this->_request->isPost() ) {
            $name = $this->view->name = $this->_request->name;
            $surname = $this->view->surname = $this->_request->surname;
            $address = $this->view->address = $this->_request->address;
            $city = $this->view->city = $this->_request->city;
            $state = $this->view->state = $this->_request->state;
            $country = $this->view->country = intval($this->_request->country);
            $zip = $this->view->zip = $this->_request->zip;
            $phone = $this->view->phone = $this->_request->phone;
            $email = $this->view->email = $this->_request->email;
            $request_date = $this->view->request_date = $this->_request->request_date;
            $duration = $this->view->duration = $this->_request->duration;
            $duration_unit = $this->view->duration_unit = $this->_request->duration_unit;
            $message = $this->view->message = $this->_request->message;

            $contact = $this->view->contact = $this->_request->contact;
            $time = $this->view->time = $this->_request->time;

            if ( ! $name ) {
                $errors[] = 'name';
            }
            /*if ( ! $surname ) {
                $errors[] = 'surname';
            }
            if ( ! $address ) {
                $errors[] = 'address';
            }
            if ( ! $city ) {
                $errors[] = 'city';
            }
            if ( ! $state ) {
                $errors[] = 'state';
            }*/
            if ( ! $country ) {
                $errors[] = 'country';
            }
            /*if ( ! $zip ) {
                $errors[] = 'zip';
            }
            if ( ! $phone ) {
                $errors[] = 'phone';
            }
            if ( ! $email ) {
                $errors[] = 'email';
            }
            if ( ! $request_date ) {
                $errors[] = 'request_date';
            }
            if ( ! $duration ) {
                $errors[] = 'duration';
            }*/

            if ( ! $phone && ! $email ) {
                if ( ! $phone ) {
                    $errors[] = 'phone';
                }
                if ( ! $email ) {
                    $errors[] = 'email';
                }
            }

            if ( count($contact) == 0 ) {
                //$errors[] = 'contact';
                $this->view->contact = array();
            }
            if ( count($time) == 0 ) {
                //$errors[] = 'time';
                $this->view->time = array();
            }

            // Save data
            if ( count($errors) == 0 ) {
                $data = array(
                    'escort_id' => $escort_id,
                    'name' => $name,
                    'surname' => $surname,
                    'address' => $address,
                    'city' => $city,
                    'state' => $state,
                    'country_id' => $country,
                    'zip' => $zip,
                    'phone' => $phone,
                    'email' => $email,
                    'request_date' => $request_date ? $request_date : null,
                    'duration' => $duration,
                    'duration_unit' => $duration_unit,
                    'message' => $message,
                    'contact' => $contact,
                    'preferred_time' => $time,
                    'application_id' => Cubix_Application::getId()
                );

                Cubix_Api::getInstance()->call('bookEscort', array($data));

                //$prefLang = Cubix_Api::getInstance()->call('getPreferLang', array('id'=>$escort_id,'type'=>'escort'));

                Cubix_Email::sendTemplate('booking_request_v1', $email, array(
                    'username' => $name
                ));//,null,null,$prefLang

                $this->view->success = true;
            }
        }
        else {
            $this->view->contact = array();
            $this->view->time = array();
        }
        $this->view->errors = $errors;
    }

    public function ajaxTellFriendAction()
    {
        $this->view->layout()->disableLayout();
        $this->view->full_url = $url = $_SERVER['HTTP_REFERER'];
        if($this->_request->isPost()){

            // Get data from request
            $data = new Cubix_Form_Data($this->getRequest());

            $fields = array(
                'name' => 'notags|special',
                'email' => '',
                'friend_name' => 'notags|special',
                'friend_email' => '',
                'message' => 'notags|special',

            );
            $data->setFields($fields);
            $data = $data->getData();

            $validator = new Cubix_Validator();

            if ( ! strlen($data['name']) ) {
                $validator->setError('name', 'Your Name is required');
            }

            if ( ! strlen($data['email']) ) {
                $validator->setError('email', 'Your Email is required');
            }
            elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
                $validator->setError('email', 'Wrong email format');
            }
            if ( ! strlen($data['friend_name']) ) {
                $validator->setError('friend_name', 'Friend Name is required');
            }

            if ( ! strlen($data['friend_email']) ) {
                $validator->setError('friend_email', 'Friend Email is required');
            }
            elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['friend_email']) ) {
                $validator->setError('friend_email', 'Wrong email format');
            }

            $result = $validator->getStatus();

            if ( $validator->isValid() ) {

                // Set the template parameters and send it to support
                $email_tpl = 'send_to_friend';
                $tpl_data = array(
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'friend_name' => $data['friend_name'],
                    'friend_email' => $data['friend_email'],
                    'message' => $data['message'],
                    'application_id' => Cubix_Application::getId(),
                    'url' => $url
                );
                $data['application_id'] = Cubix_Application::getId();

                Cubix_Email::sendTemplate($email_tpl, $data['friend_email'], $tpl_data, $data['email']);

                $config = Zend_Registry::get('newsman_config');

                $list_id = $config['list_id'];
                $segment = $config['member'];
                $nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);

                $subscriber_id = $nm->subscribe($list_id, $data['email'],$data['name']);
                $is_added = $nm->bindToSegment($subscriber_id, $segment);
                $subscriber_id_friend = $nm->subscribe($list_id, $data['friend_email'],$data['friend_name']);
                $is_added = $nm->bindToSegment($subscriber_id_friend, $segment);

                die(json_encode($result));
            }	// Otherwise, render the phtml and show errors in it
            else {
                die(json_encode($result));

            }
        }
    }

    public function ajaxSuspPhotoAction()
    {
        $this->view->layout()->disableLayout();

        $this->view->id = intval($this->_request->escort_id);

        if ($this->_request->isPost())
        {
            // Get data from request
            $data = new Cubix_Form_Data($this->getRequest());

            $fields = array(
                'escort_id' => 'int',
                'link_1' => '',
                'link_2' => '',
                'link_3' => '',
                'comment' => 'notags|special',
                'captcha' => ''
            );
            $data->setFields($fields);
            $data = $data->getData();


            $validator = new Cubix_Validator();

            if (!strlen($data['link_1']))
            {
                $validator->setError('link_1', $this->view->t('photo_link_required'));
            }
            elseif (!$validator->isValidURL($data['link_1']))
            {
                $validator->setError('link_1', $this->view->t('photo_link_not_valid'));
            }

            if (strlen($data['link_2']) && !$validator->isValidURL($data['link_2']))
            {
                $validator->setError('link_2', $this->view->t('photo_link_not_valid'));
            }

            if (strlen($data['link_3']) && !$validator->isValidURL($data['link_3']))
            {
                $validator->setError('link_3', $this->view->t('photo_link_not_valid'));
            }

            if (! strlen($data['comment']))
            {
                $validator->setError('comment', $this->view->t('comment_required'));
            }

            if ( ! strlen($data['captcha']) ) {
                $validator->setError('captcha', 'Captcha is required');//
            }
            else {
                $session = new Zend_Session_Namespace('captcha');
                $orig_captcha = $session->captcha;

                if ( strtolower($data['captcha']) != $orig_captcha ) {
                    $validator->setError('captcha', 'Captcha is invalid');//Captcha is invalid
                }
            }

            $result = $validator->getStatus();

            if ($validator->isValid())
            {
                $data['application_id'] = Cubix_Application::getId();
                $data['status'] = SUSPICIOUS_PHOTOS_REQUEST_PENDING;

                $user = Model_Users::getCurrent();
                $user_id = null;

                if ($user)
                    $user_id = $user->id;

                $data['user_id'] = $user_id;

                unset($data['captcha']);
                $data['type'] = 0;
                $client = new Cubix_Api_XmlRpc_Client();
                $client->call('Users.addSuspPhoto', array($data));
            }

            die(json_encode($result));
        }
    }

    public function ajaxAddReportAction()
    {
        $this->view->layout()->disableLayout();

        $this->view->id = intval($this->_request->escort_id);
        $this->view->type = intval($this->_request->type);

        if ($this->_request->isPost())
        {
            // Get data from request
            $data = new Cubix_Form_Data($this->getRequest());

            $fields = array(
                'escort_id' => 'int',
                'email' => '',
                'link_1' => '',
                'link_2' => '',
                'link_3' => '',
                'type' => 'int',
                'comment' => 'notags|special',
                'captcha' => ''
            );
            $data->setFields($fields);
            $data = $data->getData();


            $validator = new Cubix_Validator();

            if (! strlen($data['comment']))
            {
                $validator->setError('comment', $this->view->t('comment_required'));
            }

            if (  strlen($data['email']) ) {
                if ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
                    $validator->setError('email', 'Wrong email format');
                }
            }


            if ( ! strlen($data['captcha']) ) {
                $validator->setError('captcha', 'Captcha is required');//
            }
            else {
                $session = new Zend_Session_Namespace('captcha');
                $orig_captcha = $session->captcha;

                if ( strtolower($data['captcha']) != $orig_captcha ) {
                    $validator->setError('captcha', 'Captcha is invalid');//Captcha is invalid
                }
            }

            $result = $validator->getStatus();

            if ($validator->isValid())
            {
                $data['application_id'] = Cubix_Application::getId();
                $data['status'] = SUSPICIOUS_PHOTOS_REQUEST_PENDING;

                $user = Model_Users::getCurrent();
                $user_id = null;

                if ($user)
                    $user_id = $user->id;

                $data['user_id'] = $user_id;

                unset($data['captcha']);

                $client = new Cubix_Api_XmlRpc_Client();

                $client->call('Users.addProblemReport', array('',$data['email'],'',$data['comment'],$data['escort_id'],$data['user_id']));
            }

            die(json_encode($result));
        }
    }

    public function ajaxReportProblemAction()
    {
        $this->view->layout()->disableLayout();
        $url = $_SERVER['HTTP_REFERER'];
        if($this->_request->isPost()){
            // Fetch administrative emails from config
            $config = Zend_Registry::get('feedback_config');
            $support_email = $config['emails']['support'];
            $email_tpl = 'report_problem';
            // Get data from request
            $data = new Cubix_Form_Data($this->getRequest());

            $fields = array(
                'escort_id' => 'int',
                'name' => 'notags|special',
                'email' => '',
                'message' => 'notags|special',

            );
            $data->setFields($fields);
            $data = $data->getData();

            $validator = new Cubix_Validator();

            if ( ! strlen($data['name']) ) {
                $validator->setError('name', 'Your Name is required');
            }

            if ( ! strlen($data['email']) ) {
                $validator->setError('email', 'Your Email is required');
            }
            elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
                $validator->setError('email', 'Wrong email format');
            }
            if ( ! strlen($data['message']) ) {
                $validator->setError('message','Please type the report you want to send');
            }

            $result = $validator->getStatus();

            if ( $validator->isValid() ) {

                // Set the template parameters and send it to support
                $tpl_data = array(
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'message' => $data['message'],
                    'application_id' => Cubix_Application::getId(),
                    'url' => $url
                );
                $data['application_id'] = Cubix_Application::getId();

                try{
                    Cubix_Email::sendTemplate($email_tpl, $support_email, $tpl_data, null);

                    $user_id = null;
                    $user = Model_Users::getCurrent();

                    if ($user && $user->user_type == 'member')
                        $user_id = $user->id;

                    $client = new Cubix_Api_XmlRpc_Client();
                    $client->call('Users.addProblemReport', array($data['name'], $data['email'], $data['message'], $data['escort_id'], $user_id));

                    $nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);
                    $subscriber_id = $nm->subscribe($list_id, $data['email'],$data['name']);
                    $is_added = $nm->bindToSegment($subscriber_id, $segment);

                }catch ( Exception $e ) {
                    die($e->getMessage());
                }
                die(json_encode($result));
            }	// Otherwise, render the phtml and show errors in it
            else {
                die(json_encode($result));

            }

        }
    }

    public function getFilterAction()
    {
        $params = $this->_getParam('json');
        $this->view->layout()->disableLayout();
        $menus = array('filter' => NULL, 'sort' => NULL);

        $defs = Zend_Registry::get('definitions');

        $filterStructure = $defs['escorts_filter'];

        $menus['filter'] = new Cubix_NestedMenu(array('childs' => $filterStructure));
        $menus['filter']->setId('filter-options');
        $menus['filter']->setSelected($menus['filter']->getByValue(NULL));

        $menus['sort'] = new Cubix_NestedMenu(array('childs' => $defs['escorts_sort']));
        $menus['sort']->setId('sort-options');
        $menus['sort']->setSelected($menus['sort']->getByValue('random'));
        $this->view->menus = $menus;
        $this->view->params = $params;

    }

    public function ajaxGetCitiesSearchAction()
    {
        $all_cities = Model_Statistics::getCities(null, null, null, null, null, null, null, $this->_request->getParam('city_search_name', null));

        $fn_order_title = create_function('$a, $b', '
			if ( is_object($a) && is_object($b) ) {
				return strnatcmp($a->city_title, $b->city_title);
			}
			elseif ( is_array($a) && is_array($b) ) {
				return strnatcmp($a["city_title"], $b["city_title"]);
			}
			else return 0;
		');

        usort($all_cities, $fn_order_title);
        foreach ( $all_cities as $k => $city ) {
            $all_cities[$k] = array(
                'type' => 'city',
                'group' => $this->view->t('cities'),
                'slug' => $city->city_slug,
                'city_id' => $city->city_base_id,
                'name' => $city->city_title . ' (' . $city->country_title . ')'
            );
        }

        die(json_encode($all_cities));
    }

    public function ajaxGetAgenciesSearchAction()
    {
        $all_agencies = Model_EscortsV2::getAllAgencies($this->_request->getParam('agency_search_name', null));

        foreach ( $all_agencies as $k => $agency ) {
            $all_agencies[$k] = array(
                'type' => 'agency',
                'group' => $this->view->t('agency'),
                'slug' => $agency->agency_slug,
                'name' => $agency->agency_name,
                'id' => $agency->agency_id
            );
        }

        die(json_encode($all_agencies));
    }

    public function ajaxGetTopSearchAction()
    {
        $count = 0;

        $filter_params['filter'] = array('e.showname LIKE ?' => $this->_request->getParam('top_search_name', null) . '%', 'show_all_agency_escorts' => true, 'premium_regular' => true);

        $all_escorts = Model_Escort_List::getFiltered($filter_params['filter'], 'alpha', 1, 20, $count, 'regular_list', false);

        $_all_escorts = array();


        if ( $count ) {
            $_all_escorts[0] = array(
                'type' => 'escort',
                'method' => 'see-all',
                'group' => $this->view->t('escorts'),
                'slug' => $this->_request->getParam('top_search_name', null),
                'name' => $this->_request->getParam('top_search_name', null) . ' (' . $count . ' / ' . Cubix_I18n::translate('see_all') . ')'
            );
        }

        foreach ( $all_escorts as $k => $escort ) {

            if(!$escort->id){continue;}
            $show_name = $escort->showname . ' (' . $escort->city . ' / ';

            if ( $escort->agency_id ) {
                $show_name .= Cubix_I18n::translate('agency') . ' ' . $escort->agency_name . ')';
            } else {
                $show_name .= Cubix_I18n::translate('independent') . ')';
            }



            $_all_escorts[$k+1] = array(
                'type' => 'escort',
                'group' => $this->view->t('escorts'),
                'escort_id' => $escort->id,
                'slug' => $escort->showname,
                'name' => $show_name,
            );
        }

        /*$all_cities = Model_Statistics::getCities(null, null, null, null, null, null, 5, $this->_request->getParam('top_search_name', null));


        $fn_order_title = create_function('$a, $b', '
			if ( is_object($a) && is_object($b) ) {
				return strnatcmp($a->city_title, $b->city_title);
			}
			elseif ( is_array($a) && is_array($b) ) {
				return strnatcmp($a["city_title"], $b["city_title"]);
			}
			else return 0;
		');

        usort($all_cities, $fn_order_title);
        foreach ( $all_cities as $k => $city ) {
            $all_cities[$k] = array(
                'type' => 'city',
                'group' => $this->view->t('cities'),
                'slug' => $city->city_slug,
                'name' => $city->city_title . ' (' . $city->escort_count . ')' . ' (' . $city->country_title . ')'
            );
        }

        $all_countries = Model_Statistics::getCountries(null, null, null, null, 5, $this->_request->getP$this->_requestaram('top_search_name', null));

        $fn_order_title = create_function('$a, $b', '
			if ( is_object($a) && is_object($b) ) {
				return strnatcmp($a->country_title, $b->country_title);
			}
			elseif ( is_array($a) && is_array($b) ) {
				return strnatcmp($a["country_title"], $b["country_title"]);
			}
			else return 0;
		');

        usort($all_countries, $fn_order_title);
        foreach ( $all_countries as $k => $country ) {
            $all_countries[$k] = array(
                'type' => 'country',
                'group' => $this->view->t('countries'),
                'slug' => $country->country_slug,
                'name' => $country->country_title . ' (' . $country->escort_count . ')'
            );
        }

        $merged = array_merge($_all_escorts, $all_countries, $all_cities);*/
        $merged = $_all_escorts;

        die(json_encode($merged));
    }

    public function ajaxGetFilterCitiesAction()
    {
        $req = $this->_request;
        if ( $req->isPost() ) {
            $gender = is_null($req->gender) ? null : intval($req->gender);
            $is_agency = is_null($req->is_agency) ? null : intval($req->is_agency);
            $is_tour = is_null($req->is_tour) ? null : intval($req->is_tour);
            $is_upcoming_tour = is_null($req->is_upcoming_tour) ? null : intval($req->is_upcoming_tour);
            $country_id = is_null($req->country_id) ? null : intval($req->country_id);
            $is_new = is_null($req->is_new) ? null : intval($req->is_new);
            $region_id = is_null($req->region_id) ? null : intval($req->region_id);
            $m_st = new Model_Statistics();

            $cities = $m_st->getEDCities($gender, $is_agency, $is_tour, $is_upcoming_tour, null, array(), null, $country_id, $is_new, false, $region_id);
            echo json_encode($cities);
        }

        die;
    }

    public function ajaxGetFilterRegionsAction()
    {
        if ( $this->_request->isPost() ) {
            $gender = $this->_request->gender;
            $is_agency = $this->_request->is_agency;
            $is_tour = $this->_request->is_tour;
            $is_upcoming_tour = $this->_request->is_upcoming_tour;
            $country_id = $this->_request->country_id;
            $is_new = $this->_request->is_new;

            $m_st = new Model_Statistics();

            $regions = $m_st->getEDRegions($gender, $is_agency, $is_tour, $is_upcoming_tour, null, array(), null, $country_id, $is_new);
            echo json_encode($regions);
        }

        die;
    }

    public function bannerPageAction()
    {
        $showname = $this->_getParam('escortName');
        $escort_id = $this->_getParam('escort_id');
        $gender = Zend_Registry::get('defines')['gender_options'];
        $client = Cubix_Api_XmlRpc_Client::getInstance();

        $model = new Model_EscortsV2();
        $escort_item = new Model_EscortV2Item(array('id' => $escort_id));
        $current_city = (object)$client->call('Escorts.getCityCountryInfo', array($escort_id))[0];

        $client = new Cubix_Api_XmlRpc_Client();

        $photo_count = 2;

        $cache_key = 'v2_' . $showname . '_' . $escort_id . '_profile_' . Cubix_I18n::getLang();
        $cache_key = preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

        $escort = $model->get($escort_id, $cache_key);


        //Check user status
        if(!$escort || !isset($escort->escort_status)){
            $escort = $client->call('Escorts.getProfileV2', array($escort_id, Cubix_I18n::getLang(), true));
            $escort = new Model_EscortV2Item($escort);

            if ($escort->escort_status != 32) {
                $this->view->disabled = true;
            }
        }elseif ($escort->escort_status != 32){
            $this->view->disabled = true;
        }

        $escort->agency = $model->getAgency($escort->id);
        $escort->city_info = $model->getCityCountryinfo($escort->id);
        //Check ip user
        $blockModel = new Model_BlockedCountries();
        if ($blockModel->checkIp($escort->id)) {
            $this->view->disabled = true;
        }

        $escort->city = $current_city->title;
        $escort->all_photos = $escort_item->getPhotos(1, $photo_count, false);

        $nearby_escorts = $model->getPopunderEscorts($current_city->iso, 4, $escort->id, 4,$current_city->id,false);

        foreach ($nearby_escorts as $key => $esc) {
            $nearby_escorts[$key]->escort_item = new Model_EscortV2Item(array('id' => $esc->id));
            $nearby_escorts[$key]->agency = $model->getAgency($esc->id);
        }
        //

        //Whatched escorts
        foreach ($nearby_escorts as $e) {
            $ids[] = $e->id;
        }

        $ids[] = $escort->id;

        if ($ids) {
            $ids_str = trim(implode(',', $ids), ',');
            $modelM = new Model_Members();

            $res = $modelM->getFromWatchedEscortsForListing($ids_str, WATCH_TYPE_PER_SESSION, null, session_id());

            if ($res) {
                foreach ($res as $r) {
                    $watched_escorts[] = $r->escort_id;
                }
            }
        }
        //

        $this->view->watched_escorts = $watched_escorts;
        $this->view->escort = $escort;
        $this->view->gender = $gender;
        $this->view->nearby_escorts = $nearby_escorts;

    }

    public static function calculateNewestEscortDays($date_activated){

        $dateActivated = new DateTime();
        $dateActivated->setTimestamp($date_activated);
        $current_date = new DateTime();

        $interval = $current_date->diff($dateActivated)->format('%a');
        if(!$interval){
            $interval = 'today';
        }
        $month_ago =__('month_ago');

        $sticker_text = '';

        switch($interval){
            case $interval == 'today': $sticker_text = strtoupper(__('new')) . ' ' . __('today');break;
            case $interval == 1: $sticker_text = strtoupper(__('new')) . ' ' . __('yesterday');break;
            case $interval < 6 : $sticker_text = strtoupper(__('new') ) . ' ' . $interval . ' days ago';break;
            case $interval <= 28: $sticker_text = $interval . ' days ago';break;
            case $interval > 29 && $interval <= 45: $sticker_text = '1 '. $month_ago;break;
            case $interval > 46 && $interval <= 75: $sticker_text = '2 '. $month_ago;break;
            case $interval > 76 && $interval <= 105: $sticker_text = '3 '. $month_ago;break;
            case $interval > 106 && $interval <= 136: $sticker_text = '4 '.$month_ago;break;
            case $interval > 137 && $interval <= 167: $sticker_text = '5 '.$month_ago;break;
            case $interval > 168 && $interval <= 198: $sticker_text = '6 ' .$month_ago;break;
            case $interval > 199 && $interval <= 229: $sticker_text = '7 '.$month_ago;break;
            case $interval > 230 && $interval <= 260: $sticker_text = '8 '.$month_ago;break;
            case $interval > 261 && $interval <= 291: $sticker_text = '9 '.$month_ago;break;
            case $interval > 292 && $interval <= 322: $sticker_text = '10 '.$month_ago;break;
            case $interval > 323 && $interval <= 353: $sticker_text = '11 '.$month_ago;break;
            case $interval > 353 : $sticker_text = __('more_one_year');break;
            default : $sticker_text = 0;
        }

        return $sticker_text;

    }
}