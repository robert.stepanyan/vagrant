<?php
class ReviewsController extends Zend_Controller_Action
{
	private $model;
	private $user;

	public function init()
	{

        $this->model = new Model_Reviews();
		$this->user = Model_Users::getCurrent();
	}

	public function indexAction()
	{
		$request = $this->_request;
        #print_r($request);die;
		$ord_field = 'add';
		$ord_dir = 'desc';

		$this->view->sort = $ord_field . '_' . $ord_dir;
		$this->view->page = $page = 1;

		$config = Zend_Registry::get('reviews_config');
		$this->view->per_page = $per_page = $config['perPage'];

		$filter = array();

		if ($request->mode == 'exact')
		{
			$filter['mode'] = $this->view->mode = 'exact';
			$filter['member'] = $this->view->member = $request->member;
		}

		switch ($ord_field)
		{
			case 'meet':
				$ord_field_v = 'r.meeting_date';
				break;
			case 'add':
			default:
				$ord_field_v = 'r.creation_date';
				break;
		}

		switch ($ord_dir)
		{
			case 'asc':
				$ord_dir_v = 'ASC';
				break;
			case 'desc':
			default:
				$ord_dir_v = 'DESC';
				break;
		}

        $modelCities = new Model_Cities();
        $geoData = $modelCities->getByGeoIp();

        START_FILTERING: // Remember the point to make filtering again if needed

        // If we detect user's geo position, fetch all info about it
        // to fill in top search-bar
        // --------------------------------------
        if (!empty($geoData['country_id'])) {
            $filter['country_id'] = $geoData['country_id'];
            $this->view->country = ['id' => $geoData['country_id'], 'title' => $geoData['country_title']];

            if(!empty($geoData['city_id'])) {
                $filter['city_id'] =  $geoData['city_id'];
                $this->view->city = ['id' => $geoData['city_id'], 'title' => $geoData['city_title']];
            }
        }
        // --------------------------------------

		$this->view->countries = $this->model->getReviewsCountCountry();

		$ret_revs = $this->model->getReviews($page, $per_page, $filter, $ord_field_v, $ord_dir_v);

		// When we have no data found because the search was made by user
        // geo position, then just remove city from search-bar, if the result is
        // still empty, then remove country, if result is still empty remove location search
        // REMEMBER! This is only when location is detected by GEO IP not by manual search
        // ------------------------------------------
		if(isset($ret_revs[0]) AND count($ret_revs[0]) === 0) {

		    if(isset($geoData['city_id'])) {
                unset($geoData['city_id'], $geoData['city_title'], $geoData['city_slug']);
                unset($this->view->city);
                goto START_FILTERING;
            }

            if(isset($geoData['country_id'])) {
                unset($geoData['country_id'], $geoData['country_title'], $geoData['country_slug']);
                unset($this->view->country, $filter['city_id'], $filter['country_id']);
                goto START_FILTERING;
            }
        }
        // ------------------------------------------

		foreach ( $ret_revs[0] as $i => $rev )
		{
			$photo_url = new Model_Escort_PhotoItem($rev);
			$p_u = new SplFileInfo($photo_url->getUrl('review_list_v3'));
			$ext = $p_u->getExtension();

			if (!strlen($ext)) {
				$ret_revs[0][$i]->photo_url = '';
			} else {
				$ret_revs[0][$i]->photo_url = $photo_url->getUrl('review_list_v3');
			}

            $ret_revs[0][$i]->escort_age = $this->model->getEscortAge($rev->escort_id);


		}

		list($items, $count) = $ret_revs;
		$this->view->items = $items;
		$this->view->count = $count;
	}

	public function ajaxListAction()
	{
		$this->view->layout()->disableLayout();
		$request = $this->_request;

		$sort = $this->view->sort = !empty($request->sort) ? $request->sort : 'add_desc';
		$sort_parts = explode('_', $sort);
		$ord_field = $sort_parts[0];
		$ord_dir = $sort_parts[1];

		if ($ord_field != 'meet') $ord_field = 'add';

		if ($ord_dir != 'asc') $ord_dir = 'desc';

		$this->view->sort = $ord_field . '_' . $ord_dir;

		$page = intval($request->page);
		if ($page < 1) $page = 1;
		$this->view->page = $page;

		$config = Zend_Registry::get('reviews_config');
		$per_page_def = $config['perPage'];
		$per_page = $this->_getParam('per_page', $per_page_def);
		$this->view->per_page = $per_page;

		$filter = array();

		$filter['country_id'] = intval($request->country_id);
		$filter['city_id'] = intval($request->city_id);
		$filter['showname'] = $request->showname;
		$filter['member'] = $request->member;
		$filter['meeting_date_f'] = intval($request->meeting_date_f);
		$filter['meeting_date_t'] = intval($request->meeting_date_t);
		$filter['date_added_f'] = intval($request->date_added_f);
		$filter['date_added_t'] = intval($request->date_added_t);
		$filter['gender_combined'] = intval($request->gender_combined);

		if ($request->gender)
		{
			if (is_array($request->gender))
				$filter['gender'] = implode(',', $request->gender);
			else
				$filter['gender'] = $request->gender;
		}
		else
			$filter['gender'] = null;


		switch ($ord_field)
		{
			case 'meet':
				$ord_field_v = 'r.meeting_date';
				break;
			case 'add':
			default:
				$ord_field_v = 'r.creation_date';
				break;
		}

		switch ($ord_dir)
		{
			case 'asc':
				$ord_dir_v = 'ASC';
				break;
			case 'desc':
			default:
				$ord_dir_v = 'DESC';
				break;
		}

		$ret_revs = $this->model->getReviews($page, $per_page, $filter, $ord_field_v, $ord_dir_v);

		foreach ( $ret_revs[0] as $i => $rev )
		{
			$photo_url = new Model_Escort_PhotoItem($rev);

			$p_u = new SplFileInfo($photo_url->getUrl('review_list_v3'));
			$ext = $p_u->getExtension();

			if (!strlen($ext)) {
				$ret_revs[0][$i]->photo_url = '';
			} else {
				$ret_revs[0][$i]->photo_url = $photo_url->getUrl('review_list_v3');
			}
		}

		list($items, $count) = $ret_revs;
		$this->view->items = $items;
		$this->view->count = $count;

		$this->_helper->viewRenderer->setScriptAction('list');
	}

//	public function addReviewAction()
//	{
//
//      if ($this->user->user_type != 'member')
//		{
//			$this->_redirect($this->view->getLink('dashboard'));
//			return;
//		}
//
//
//		$escort_id = $this->view->escort_id = intval($this->_request->escort_id);
//
//		if (!$escort_id)
//		{
//			$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
//			$this->_forward('error', 'error', null, array('error_msg' => 'Wrong URL.'));
//			return;
//		}
//
//		$dis_rev = $this->model->allowRevCom($escort_id);
//		$dis_rev = $dis_rev['disabled_reviews'];
//
//		if ($dis_rev)
//		{
//			$this->_redirect($this->view->getLink());
//			return;
//		}
//
//		$showname = $this->model->getEscortShowname($escort_id);
//
//		if (!$showname)
//		{
//			$this->_forward('error', 'error', null, array('error_msg' => 'Wrong URL.'));
//			return;
//		}
//
//		$this->view->showname = $showname;
//		$this->view->username = $this->user->username;
//
//		if ($this->_request->isPost())
//		{
//			$validator = new Cubix_Validator();
//			$req = $this->_request;
//
//			if (intval($req->m_date) == 0)
//				$validator->setError('m_date', 'Required');
//
//			if (!$req->country_id)
//				$validator->setError('country_id', 'Required');
//
//			if (!$req->city_id)
//				$validator->setError('city_id', 'Required');
//
//			if (!$req->meeting_place)
//				$validator->setError('meeting_place', 'Required');
//
//            //Check if Meeting place in "At Her Apartament" or "Her Hotel"
//            //Ticket EDIR 1170
//			if(in_array($req->meeting_place,array(12,13)) && !$req->incall_location_review)
//                $validator->setError('incall_location_review', 'Required');
//
//            if (!$req->quality)
//                $validator->setError('quality', 'Required');
//
//			if (!trim($req->duration))
//				$validator->setError('duration', 'Required');
//			elseif (!is_numeric($req->duration))
//				$validator->setError('duration', $this->view->t('must_be_numeric'));
//
//			if (!$req->duration_unit)
//				$validator->setError('duration_unit', 'Required');
//
//			if (!trim($req->price))
//				$validator->setError('price', 'Required');
//			elseif (!is_numeric($req->price))
//				$validator->setError('price', $this->view->t('must_be_numeric'));
//
//			if (!$req->currency)
//				$validator->setError('currency', 'Required');
//
//			if ($req->looks_rate == '-1')
//				$validator->setError('looks_rate', 'Required');
//
//			if ($req->services_rate == '-1')
//				$validator->setError('services_rate', 'Required');
//
//			if (!$req->s_kissing)
//				$validator->setError('s_kissing', 'Required');
//
//			if (!$req->s_blowjob)
//				$validator->setError('s_blowjob', 'Required');
//
//			if (!$req->s_cumshot)
//				$validator->setError('s_cumshot', 'Required');
//
//			if (!$req->s_69)
//				$validator->setError('s_69', 'Required');
//
//			if (!$req->s_anal)
//				$validator->setError('s_anal', 'Required');
//
//			if (!$req->s_sex)
//				$validator->setError('s_sex', 'Required');
//
//			if (!$req->s_multiple_times_sex)
//				$validator->setError('s_multiple_times_sex', 'Required');
//
//			if (!$req->review)
//				$validator->setError('review', 'Required');
//
//			if (!$req->s_breast)
//				$validator->setError('s_breast', 'Required');
//
//			/* if (!$req->s_attitude)
//				$validator->setError('s_attitude', 'Required');
// 			*/
//			if (!$req->s_conversation)
//				$validator->setError('s_conversation', 'Required');
//
//			if (!$req->s_availability)
//				$validator->setError('s_availability', 'Required');
//
//			if (!$req->s_photos)
//				$validator->setError('s_photos', 'Required');
//
//			if (!trim($req->t_user_info))
//				$validator->setError('t_user_info', 'Required');
//
//			if (!trim($req->t_meeting_date))
//				$validator->setError('t_meeting_date', 'Required');
//
//			if ($req->hrs == '-1')
//				$validator->setError('hrs', 'Required');
//
//			if ($req->min == '-1')
//				$validator->setError('min', 'Required');
//
//			if (!trim($req->t_meeting_duration))
//				$validator->setError('t_meeting_duration', 'Required');
//
//			if (!trim($req->t_meeting_place))
//				$validator->setError('t_meeting_place', 'Required');
//
//			if (!trim($req->t_comments))
//				$validator->setError('t_comments', 'Required');
//
//			/* $captcha = trim($req->captcha);
//
//			if ( ! strlen($captcha ) ) {
//				$validator->setError('captcha', 'Required');
//			}
//			else {
//				$session = new Zend_Session_Namespace('captcha');
//				$orig_captcha = $session->captcha;
//
//				if ( strtolower($captcha) != $orig_captcha ) {
//					$validator->setError('captcha', 'Captcha is invalid');
//				}
//				else
//				{
//					$session->unsetAll();
//				}
//			} */
//
//			$t_user_info = trim($req->t_user_info);
//			$t_meeting_date = trim($req->t_meeting_date);
//			$t_meeting_time = $req->hrs . ':' . $req->min;
//			$t_meeting_duration = trim($req->t_meeting_duration);
//			$t_meeting_place = trim($req->t_meeting_place);
//
//
//			if ($validator->isValid())
//			{
//				$ip = Cubix_Geoip::getIP();
//				$req->setParam('ip', $ip);
//				$req->setParam('user_id', $this->user->id);
//				$req->setParam('meeting_city', 'other');
//				$req->setParam('m_date', intval($req->m_date));
//
//				list($sms_unique, $phone_to, $mem_susp) = Cubix_Api::getInstance()->call('addReview', array($this->getRequest()->getParams()));
//				Cubix_Api::getInstance()->call('SyncNotifier', array($escort_id, 29 ,array('escort_id' => $escort_id) ));
//
//				if (strlen(trim($phone_to)) > 0 && !$mem_susp)
//				{
//					$text = Cubix_I18n::translate('review_sms_template', array(
//						'user_info' => $t_user_info,
//						'showname' => $showname,
//						'date' => $t_meeting_date,
//						'meeting_place' => $t_meeting_place,
//						'duration' => $t_meeting_duration,
//						'time' => $t_meeting_time,
//						'unique_number' => $sms_unique
//					));
//
//					$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());
//
//					$phone_from = $originator;
//					$sms_id = Cubix_Api::getInstance()->call('outbox', array($escort_id, $phone_to, $phone_from, $text, Cubix_Application::getId()));
//
//					//sms info
//					$config = Zend_Registry::get('system_config');
//					$sms_config = $config['sms'];
//
//					$SMS_USERKEY = $sms_config['userkey'];
//					$SMS_PASSWORD = $sms_config['password'];
//					$SMS_DeliveryNotificationURL = $sms_config['DeliveryNotificationURL'];
//					$SMS_NonDeliveryNotificationURL = $sms_config['NonDeliveryNotificationURL'];
//					$SMS_BufferedNotificationURL = $sms_config['BufferedNotificationURL'];
//
//					$sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);
//
//					$sms->setOriginator($originator);
//					$sms->addRecipient($phone_to, (string)$sms_id);
//					$sms->setBufferedNotificationURL($SMS_BufferedNotificationURL);
//					$sms->setDeliveryNotificationURL($SMS_DeliveryNotificationURL);
//					$sms->setNonDeliveryNotificationURL($SMS_NonDeliveryNotificationURL);
//
//					$sms->setContent($text);
//
//					if (false/*1 != $result = $sms->sendSMS()*/)
//					{
//
//					}
//					else
//					{
//						//Cubix_Api::getInstance()->call('smsSent', array($sms_unique));
//					}
//				}
//
//				$this->_redirect($this->view->getLink('dashboard'));
//			}
//			else
//			{
//				$status = $validator->getStatus();
//				$this->view->errors = $status['msgs'];
//				$this->view->review = $req;
//			}
//		}
//	}


    public function addReviewPopupAction() {
        $this->view->layout()->disableLayout();
    }


    public function addReviewAction()
    {
        $this->view->layout()->disableLayout();

        if ($this->user->user_type != 'member')
        {
            $this->_redirect($this->view->getLink('home'));
            return;
        }


        $escort_id = $this->view->escort_id = intval($this->_request->escort_id);

        if (!$escort_id)
        {
            $this->_response->setRawHeader('HTTP/1.1 404 Not Found');
            $this->_forward('error', 'error', null, array('error_msg' => 'Wrong URL.'));
            return;
        }



        $dis_rev = $this->model->allowRevCom($escort_id);
        $dis_rev = $dis_rev['disabled_reviews'];

        if ($dis_rev)
        {
            $this->_redirect($this->view->getLink());
            return;
        }


        $showname = $this->model->getEscortShowname($escort_id);



//       Open after done with other steps. This step often yields error, that is why is closed

//        if (!$showname)
//        {
//            $this->_forward('error', 'error', null, array('error_msg' => 'Wrong URL.'));
//            return;
//        }

//        step end



        $this->view->showname = $showname;
        $this->view->username = $this->user->username;

        if ($this->_request->isPost())
        {
            $validator = new Cubix_Validator();
            $req = $this->_request;


            if (intval($req->m_date) == 0)
                $validator->setError('m_date', __('m_date_required'));

//            Probably won't be necessary
//            if (!$req->country_id)
//                $validator->setError('country_id', 'Required');

            if (!$req->city_id)
                $validator->setError('city_id', __('city_required'));

            if (!$req->meeting_place)
                $validator->setError('meeting_place', __('meeting_place_required'));

            //Check if Meeting place in "At Her Apartament" or "Her Hotel"
            //Ticket EDIR 1170
//            Won't need this, since there is no more input for the description of options 12 & 13
//            if(in_array($req->meeting_place,array(12,13)) && !$req->incall_location_review)
//                $validator->setError('incall_location_review', 'Required');


            if (!$req->quality)
                $validator->setError('quality', __('quality_required'));

            if (!trim($req->duration))
                $validator->setError('duration', __('meeting_duration_required'));
            elseif (!is_numeric($req->duration))
                $validator->setError('duration', __('meeting_duration_invalid'));

            if (!$req->duration_unit)
                $validator->setError('duration_unit', __('duration_unit_required'));

            if (!trim($req->price))
                $validator->setError('price', __('price_required'));
            elseif (!is_numeric($req->price))
                $validator->setError('price', __('price_invalid'));

            if (!$req->currency)
                $validator->setError('currency', __('currency_required'));

            if ($req->looks_rate == '-1')
                $validator->setError('looks_rate', __('looks_rate_required'));

            if ($req->services_rate == '-1')
                $validator->setError('services_rate', __('services_rate_required'));

            if (!$req->s_kissing)
                $validator->setError('s_kissing', __('service_kissing_required'));

            if (!$req->s_blowjob)
                $validator->setError('s_blowjob', __('service_blowjob_required'));

            if (!$req->s_cumshot)
                $validator->setError('s_cumshot', __('service_cumshot_required'));

            if (!$req->s_69)
                $validator->setError('s_69', __('service_69_required'));

            if (!$req->s_anal)
                $validator->setError('s_anal', __('service_anal_required'));

            if (!$req->s_sex)
                $validator->setError('s_sex', __('service_sex_required'));

            if (!$req->s_multiple_times_sex)
                $validator->setError('s_multiple_times_sex',  __('service_sex_multiple_times_required'));

            if (!$req->review)
                $validator->setError('review', __('review_required'));

            if (!$req->s_breast)
                $validator->setError('s_breast', __('service_breast_required'));

            /* if (!$req->s_attitude)
                $validator->setError('s_attitude', 'Required');
             */
            if (!$req->s_conversation)
                $validator->setError('s_conversation', __('service_conversation_required'));

            if (!$req->s_availability)
                $validator->setError('s_availability', __('service_availability_required'));

            if (!$req->s_photos)
                $validator->setError('s_photos', __('service_photos_required'));

            if (!trim($req->t_user_info))
                $validator->setError('t_user_info', __('review_t_user_info_required'));

            if (!trim($req->t_meeting_date))
                $validator->setError('t_meeting_date', __('review_t_meeting_date_required'));

            if ($req->hrs == '-1')
                $validator->setError('hrs', __('hrs_required'));

            if ($req->min == '-1')
                $validator->setError('min', __('min_required'));

            if (!trim($req->t_meeting_duration))
                $validator->setError('t_meeting_duration', __('review_t_meeting_duration_required'));

            if (!trim($req->t_meeting_place))
                $validator->setError('t_meeting_place', __('review_t_meeting_place_required'));

            if (!trim($req->t_comments))
                $validator->setError('t_comments', __('review_t_comments_required'));

            /* $captcha = trim($req->captcha);

            if ( ! strlen($captcha ) ) {
                $validator->setError('captcha', 'Required');
            }
            else {
                $session = new Zend_Session_Namespace('captcha');
                $orig_captcha = $session->captcha;

                if ( strtolower($captcha) != $orig_captcha ) {
                    $validator->setError('captcha', 'Captcha is invalid');
                }
                else
                {
                    $session->unsetAll();
                }
            } */

            $t_user_info = trim($req->t_user_info);
            $t_meeting_date = trim($req->t_meeting_date);
            $t_meeting_time = $req->hrs . ':' . $req->min;
            $t_meeting_duration = trim($req->t_meeting_duration);
            $t_meeting_place = trim($req->t_meeting_place);



            if ($validator->isValid())
            {
                $ip = Cubix_Geoip::getIP();
                $req->setParam('ip', $ip);
                $req->setParam('user_id', $this->user->id);
                $req->setParam('meeting_city', 'other');
                $req->setParam('m_date', intval(strtotime($req->m_date)));



                list($sms_unique, $phone_to, $mem_susp) = Cubix_Api::getInstance()->call('addReview', array($this->getRequest()->getParams()));
                Cubix_Api::getInstance()->call('SyncNotifier', array($escort_id, 29 ,array('escort_id' => $escort_id) ));

                if (strlen(trim($phone_to)) > 0 && !$mem_susp)
                {
                    $text = Cubix_I18n::translate('review_sms_template', array(
                        'user_info' => $t_user_info,
                        'showname' => $showname,
                        'date' => $t_meeting_date,
                        'meeting_place' => $t_meeting_place,
                        'duration' => $t_meeting_duration,
                        'time' => $t_meeting_time,
                        'unique_number' => $sms_unique
                    ));

                    $originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());

//                    var_dump($phone_to, $sms_unique, $mem_susp);die;

                    $phone_from = $originator;
                    $sms_id = Cubix_Api::getInstance()->call('outbox', array($escort_id, $phone_to, $phone_from, $text, Cubix_Application::getId()));

                    //sms info
                    $config = Zend_Registry::get('system_config');
                    $sms_config = $config['sms'];

                    $SMS_USERKEY = $sms_config['userkey'];
                    $SMS_PASSWORD = $sms_config['password'];
                    $SMS_DeliveryNotificationURL = $sms_config['DeliveryNotificationURL'];
                    $SMS_NonDeliveryNotificationURL = $sms_config['NonDeliveryNotificationURL'];
                    $SMS_BufferedNotificationURL = $sms_config['BufferedNotificationURL'];

                    $sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);

                    $sms->setOriginator($originator);
                    $sms->addRecipient($phone_to, (string)$sms_id);
                    $sms->setBufferedNotificationURL($SMS_BufferedNotificationURL);
                    $sms->setDeliveryNotificationURL($SMS_DeliveryNotificationURL);
                    $sms->setNonDeliveryNotificationURL($SMS_NonDeliveryNotificationURL);

                    $sms->setContent($text);

                    if (false/*1 != $result = $sms->sendSMS()*/)
                    {

                    }
                    else
                    {
                        //Cubix_Api::getInstance()->call('smsSent', array($sms_unique));
                    }
                }

                die(json_encode(array(
                    'status' => 'success',
                )));
//                $this->_redirect($this->view->getLink('dashboard'));
            }
            else
            {
                $status = $validator->getStatus();
//                print_r($status['msgs']);die;
                die(json_encode(array(
                    'status' => 'error',
                    'msg' => $status['msgs'],


            )));
//                $this->view->errors = $status['msgs'];
//                $this->view->review = $req;
            }
        }
    }

	public function escortAction()
	{
		$request = $this->_request;
		$lng = Cubix_I18n::getLang();
		$this->view->lng = $lng;
		if ($request->mode == 'ajax')
		{
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setScriptAction('escort-list');
		}

		$sort = $request->sort;
		$sort_parts = explode('_', $sort);
		$ord_field = $sort_parts[0];
		$ord_dir = $sort_parts[1];

		if ($ord_field != 'meet') $ord_field = 'add';

		if ($ord_dir != 'asc') $ord_dir = 'desc';

		$this->view->sort = $ord_field . '_' . $ord_dir;

		$page = intval($request->page);
		if ($page < 1) $page = 1;
		$this->view->page = $page;

		$config = Zend_Registry::get('reviews_config');
		$per_page_def = $config['escortPerPage'];
		$per_page = $this->_getParam('per_page', $per_page_def);
		$this->view->per_page = $per_page;

		$filter = array();

		$filter['member'] = $request->member;
		$filter['meeting_date_f'] = intval($request->meeting_date_f);
		$filter['meeting_date_t'] = intval($request->meeting_date_t);
		$filter['date_added_f'] = intval($request->date_added_f);
		$filter['date_added_t'] = intval($request->date_added_t);
		$filter['escort_id'] = $escort_id = intval($request->escort_id);

		switch ($ord_field)
		{
			case 'meet':
				$ord_field_v = 'r.meeting_date';
				break;
			case 'add':
			default:
				$ord_field_v = 'r.creation_date';
				break;
		}

		switch ($ord_dir)
		{
			case 'asc':
				$ord_dir_v = 'ASC';
				break;
			case 'desc':
			default:
				$ord_dir_v = 'DESC';
				break;
		}

		$ret_revs = $this->model->getReviews($page, $per_page, $filter, $ord_field_v, $ord_dir_v);

		list($items, $count) = $ret_revs;
		$this->view->items = $items;
		$this->view->count = $count;

		$escort = $this->model->getEscortInfo($escort_id);
		$escort = new Model_EscortV2Item($escort);
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$allowRevCom = $client->call("Reviews.allowRevCom",array($escort_id));
		$escort->disabled_reviews = $allowRevCom['disabled_reviews'];
		$escort->disabled_comments = $allowRevCom['disabled_comments'];

		$this->view->escort = $escort;

		if (!$items && ($escort->status & ESCORT_STATUS_DELETED))
		{
			$this->_redirect($this->view->getLink('reviews'));
			return;
		}

		if (($escort->status & ESCORT_STATUS_DELETED) || ($escort->status & ESCORT_STATUS_OWNER_DISABLED)
			|| ($escort->status & ESCORT_STATUS_ADMIN_DISABLED))
		{
			$this->view->disable = true;
		}
	}


	public function moreEscortReviewAction()
    {

        $request = $this->_request;


        $escort_id = intval($request->escort_id);
//        $review_id = $this->view->review_id = intval($request->review_id);
        $escort = $this->model->getEscortInfo($escort_id);
        $escort = new Model_EscortV2Item($escort);
        $m_e = new Model_EscortsV2();
        $add_esc_data = $m_e->getRevComById($escort_id);
        $escort->disabled_reviews = $add_esc_data->disabled_reviews;
        $escort->disabled_comments = $add_esc_data->disabled_comments;

        $this->view->escort = $escort;
        $review_ids = $this->model->getEscortReviewIds($escort_id);
        $review_count = count($review_ids);
        $showname = $request->showname;

        if (!is_null($this->_getParam('ajax'))) {
//            $this->view->layout()->disableLayout();
//            $escort_list = $this->view->render('agencies/list.phtml');
            die(json_encode(array('reviews' => $review_ids)));
        }


    }




	public function escortReviewAction()
	{
	    $request = $this->_request;

        if (!is_null($this->_getParam('ajax'))) {
            $this->view->layout()->disableLayout();
        }

		$escort_id = intval($request->escort_id);
        $review_id = $this->view->review_id = intval($request->review_id);
        $escort = $this->model->getEscortInfo($escort_id);
        $escort = new Model_EscortV2Item($escort);
        $m_e = new Model_EscortsV2();
		$add_esc_data = $m_e->getRevComById($escort_id);
		$escort->disabled_reviews = $add_esc_data->disabled_reviews;
		$escort->disabled_comments = $add_esc_data->disabled_comments;

		$this->view->escort = $escort;
        $this->view->escort_age = $this->model->getEscortAge($escort_id);
		$item = $this->model->getEscortReview($escort_id, $review_id);


//        $this->view->item = $item;

		if($item){
			if(isset($item->quality)){
				$item->quality = self::getQualityName($item->quality);
			}

			$this->view->item = $item;

			if (!$item && ($escort->status & ESCORT_STATUS_DELETED))
			{
				$this->_redirect($this->view->getLink('reviews'));
				return;
			}

			if (($escort->status & ESCORT_STATUS_DELETED) || ($escort->status & ESCORT_STATUS_OWNER_DISABLED) || ($escort->status & ESCORT_STATUS_ADMIN_DISABLED))
			{
				$this->view->disable = true;
			}

			/* get other reviews */
			$filter = array();

			$filter['review_id_not'] = $review_id;
			$filter['escort_id'] = $escort_id = intval($request->escort_id);

			$ord_field_v = 'r.creation_date';
			$ord_dir_v = 'DESC';

			$ret_revs = $this->model->getReviews(1, 4, $filter, $ord_field_v, $ord_dir_v);

			list($items, $count) = $ret_revs;
			$this->view->items = $items;

			$rev_items = [];
			foreach ($items as $rev_it){
                if(isset($rev_it->quality)){
                    $rev_it->quality = self::getQualityName($rev_it->quality);
                }
			    $rev_items[] = $this->model->getEscortReview($escort_id, $rev_it->id);

            }
            $this->view->rev_items = $rev_items;
		}
		else{
			$review_ids = $this->model->getEscortReviewIds($escort_id);
			$review_count = count($review_ids);
			$showname = $request->showname;

			if( $review_count == 0){
				$this->_redirect($this->view->getLink('reviews'));
			}
			elseif($review_count == 1){
				$review_id = $review_ids[0]->id;
				$this->_redirect($this->view->getLink('escort-review', array('showname' => $showname, 'escort_id' => $escort_id, 'review_id' => $review_id)));
			}
			else{
				$this->_redirect($this->view->getLink('escort-reviews', array('showname' => $showname, 'escort_id' => $escort_id)));
			}
			return;
		}
	}

	public function profileReviewsAction()
	{
		$current_user = Model_Users::getCurrent();
		$this->view->user_type  =  $user_type = $current_user->user_type;

		$this->view->layout()->disableLayout();

		$this->view->escort_id = $escort_id = $this->_request->escort_id;
        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $allowRevCom = $client->call("Reviews.allowRevCom",array($escort_id));
        if ($allowRevCom['disabled_reviews']){die;}
		$ord_field_v = 'r.creation_date';
		$ord_dir_v = 'DESC';

		$page = intval($this->_request->page);
		if ($page < 1) $page = 1;
		$this->view->page = $page;

		$this->view->per_page = $per_page = 1;

		$filter = array();

		$filter['escort_id'] = intval($escort_id);

		$ret_revs = $this->model->getReviews($page, $per_page, $filter, $ord_field_v, $ord_dir_v);

		list($items, $count) = $ret_revs;
		$this->view->reviews = $items;
		$this->view->reviews_count = $count;
	}

	public function profileMemberReviewsAction()
	{
		$this->view->layout()->disableLayout();

		$this->view->user_id = $user_id = $this->_request->user_id;
		$this->view->username = $username = $this->_request->username;

		$ord_field_v = 'r.creation_date';
		$ord_dir_v = 'DESC';

		$page = intval($this->_request->page);
		if ($page < 1) $page = 1;
		$this->view->page = $page;

		$this->view->per_page = $per_page = 1;

		$filter = array();

		$filter['user_id'] = intval($user_id);

		$ret_revs = $this->model->getReviews($page, $per_page, $filter, $ord_field_v, $ord_dir_v);

		list($items, $count) = $ret_revs;
		$this->view->reviews = $items;
		$this->view->reviews_count = $count;
	}

	public function profileAgencyReviewsAction()
	{
		$this->view->layout()->disableLayout();

		$this->view->agency_id = $agency_id = $this->_request->agency_id;

		$ord_field_v = 'r.creation_date';
		$ord_dir_v = 'DESC';

		$page = intval($this->_request->page);
		if ($page < 1) $page = 1;
		$this->view->page = $page;

		$this->view->per_page = $per_page = 1;

		$filter = array();

		$filter['agency_id'] = intval($agency_id);

		$ret_revs = $this->model->getReviews($page, $per_page, $filter, $ord_field_v, $ord_dir_v);

		list($items, $count) = $ret_revs;
		$this->view->reviews = $items;
		$this->view->reviews_count = $count;
	}

	/*******************************/

	public function getRevComSetAction()
	{
		$this->view->layout()->disableLayout();

		if (!$this->user || $this->user->user_type == 'member')
			die;

		$this->view->user = $this->user;

		if ($this->user->isAgency())
		{
			$m_pl = new Model_Planner();
			$ag_id = $this->user->getAgency()->id;
			$this->view->escorts = $m_pl->getAgencyEscorts($ag_id);

			$this->view->dis_rev = 1;
			$this->view->dis_com = 1;
		}
		else
		{
			$e_id = $this->user->getEscort()->id;
			$ret = $this->model->allowRevCom($e_id);

			$this->view->dis_rev = $ret['disabled_reviews'];
			$this->view->dis_com = $ret['disabled_comments'];
		}
	}

	public function getRevComSetAgencyAction()
	{
		$this->view->layout()->disableLayout();

		if (!$this->user || $this->user->user_type == 'member' || $this->user->user_type == 'escort')
			die;

		$e_id = intval($this->_request->e_id);
		$ag_id = $this->user->getAgency()->id;

		if ($e_id)
		{
			$m_pl = new Model_Planner();
			$owner = $m_pl->isOwnerAgency($e_id, $ag_id);

			if ($owner)
			{
				$ret = $this->model->allowRevCom($e_id);

				$dis_rev = $ret['disabled_reviews'];
				$dis_com = $ret['disabled_comments'];

				echo json_encode(array('dis_rev' => $dis_rev, 'dis_com' => $dis_com));
			}
		}

		die;
	}

	public function setRevComSetAction()
	{
		$this->view->layout()->disableLayout();

		if ($this->_request->isPost())
		{
			if (!$this->user || $this->user->user_type == 'member')
				die;

			$act = $this->_request->act;

			if (!in_array($act, array('r', 'c')))
				die;

			$val = $this->_request->val;

			if (!in_array($val, array(0, 1)))
				die;

			if ($this->user->isAgency())
			{
				$e_id = intval($this->_request->e_id);
				$ag_id = $this->user->getAgency()->id;

				if ($e_id)
				{
					$m_pl = new Model_Planner();
					$owner = $m_pl->isOwnerAgency($e_id, $ag_id);

					if ($owner)
					{
						$this->model->setAllowRevCom($e_id, $act, $val);
					}
				}
			}
			else
			{
				$e_id = $this->user->getEscort()->id;
				$this->model->setAllowRevCom($e_id, $act, $val);
			}
		}

		die;
	}

	public function myReviewsAction()
	{
		$this->view->layout()->disableLayout();

		if (!$this->user)
		{
			if($this->getRequest()->isXmlHttpRequest()){
				header("HTTP/1.1 403 Forbidden");
			}else{
				$this->_redirect($this->view->getLink('signin'));
			}
			return;
		}

		$this->view->user = $this->user;
		$page = intval($this->_request->page);
		if ($page < 1) $page = 1;
		$this->view->page = $page;

		$config = Zend_Registry::get('reviews_config');
		$this->view->per_page = $per_page = $config['my_perPage'];

		$lng = Cubix_I18n::getLang();

		$commented = $this->_request->commented;
		if (!in_array($commented, array('yes', 'no'))) $commented = null;

		$date_added_f = intval($this->_request->date_added_f);
		if (!$date_added_f) $date_added_f = null;

		$date_added_t = intval($this->_request->date_added_t);
		if (!$date_added_t) $date_added_t = null;

		$meeting_date_f = intval($this->_request->meeting_date_f);
		if (!$meeting_date_f) $meeting_date_f = null;

		$meeting_date_t = intval($this->_request->meeting_date_t);
		if (!$meeting_date_t) $meeting_date_t = null;

		$country_id = intval($this->_request->country_id);
		if (!$country_id) $country_id = null;

		$city_id = intval($this->_request->city_id);
		if (!$city_id) $city_id = null;


		$filter = array(
			'city' => $this->_request->city,
			'commented' => $commented,
			'date_added_f' => $date_added_f,
			'date_added_t' => $date_added_t,
			'meeting_date_f' => $meeting_date_f,
			'meeting_date_t' => $meeting_date_t,
			'city_id' => $city_id,
			'country_id' => $country_id,
		);

		$sort_field = $this->_request->sort_field;
		if (!in_array($sort_field, array(
			'r.creation_date',
			'r.meeting_date'
		)))

		$sort_field = 'r.creation_date';

		$sort_dir = $this->_request->sort_dir;
		if (!in_array($sort_dir, array('asc', 'desc')))
			$sort_dir = 'desc';

		$this->view->sort_field = $sort_field;
		$this->view->sort_dir = $sort_dir;

		if ($this->user->user_type == 'member')
		{
			$filter['showname'] = $this->_request->showname;
			$this->view->filter = $filter;

			$res = $this->model->getReviewsForMember($this->user->id, $lng, $filter, $page, $per_page, $sort_field, $sort_dir);

			$countryCity = $this->model->getReviewsCountryCity($this->user->id, 'member', $lng, $filter);
			$countriesIds = $countryCity[0];
			$citiesIds = $countryCity[1];

			$this->view->items = $res[0];
			$this->view->count = $res[1];
		}
		elseif ($this->user->user_type == 'escort')
		{
			$filter['member'] = $this->_request->member;
			$this->view->filter = $filter;
			$escort_id = $this->user->getEscort()->id;

			$res = $this->model->getReviewsForEscort($escort_id, $lng, $filter, $page, $per_page, $sort_field, $sort_dir);

			$countryCity = $this->model->getReviewsCountryCity($escort_id, 'escort', $lng, $filter);
			$countriesIds = $countryCity[0];
			$citiesIds = $countryCity[1];

			$this->view->items = $res[0];
			$this->view->count = $res[1];

		}
		else
		{
			$filter['member'] = $this->_request->member;
			$escort_id = intval($this->_request->escort_id);
			if (!$escort_id) $escort_id = null;
			$filter['escort_id'] = $escort_id;
			$this->view->filter = $filter;

			$ag_id = $this->user->getAgency()->id;

			$res = $this->model->getReviewsForAgency($ag_id, $lng, $filter, $page, $per_page, $sort_field, $sort_dir);

			$countryCity = $this->model->getReviewsCountryCity($ag_id, 'agency', $lng, $filter);
			$countriesIds = $countryCity[0];
			$citiesIds = $countryCity[1];

			$this->view->items = $res[0];
			$this->view->count = $res[1];
			$this->view->escorts = $res[2];

			/* Pending reviews */
			// $pending = Cubix_Api::getInstance()->call('getPendingReviewsForAgencyEscorts', array($this->user->id));
			// $this->view->items_pending = $pending['reviews'];
			// $this->view->items_pending_count = $pending['count'];
		}

		$m_country = new Cubix_Geography_Countries();
		$m_city = new Cubix_Geography_Cities();

		$countries = array();
		$cities = array();
		if($countriesIds){
			foreach ($countriesIds as $country) {
				if(!in_array($country, $countries)){
					$countryObj = $m_country->getTitleById($country);
					$countries[$country] = $countryObj;
				}
			}
		}
		if($citiesIds){
			foreach ($citiesIds as $country_id => $countryCity) {
				foreach($countryCity as $cityId){
					if(!in_array($cityId, $cities)){
						$cityObj = $m_city->getTitleById($cityId);
		 				$cities[$country_id][$cityId]['title'] = $cityObj;
		 			}
				}
			}
		}

		$this->view->countries = $countries;
		$this->view->cities = $cities;
        $this->view->is_filtered = !is_null($this->_request->page);
	}

	public function reviewConfirmationAction(){
		$this->view->layout()->disableLayout();
		if ($this->user->user_type != 'escort' && $this->user->user_type != 'agency') die;

		if ($this->user->user_type == 'escort')
		{
			$escort_id = $this->user->getEscort()->id;

			$pending = Cubix_Api::getInstance()->call('getPendingReviewsForEscort', array($this->user->id, $escort_id));
			$this->view->items_pending = $pending['reviews'];
		}
		else
		{
			$pending = Cubix_Api::getInstance()->call('getPendingReviewsForAgencyEscorts', array($this->user->id));
			$this->view->items_pending = $pending['reviews'];
		}

		if ($this->_request->isPost())
		{
			$rev_id = $this->view->review_id = intval($this->_request->review_id);

			if (!$rev_id) die;

			if ($this->user->user_type == 'escort')
			{
				if (!Cubix_Api::getInstance()->call('checkEscortReview', array($this->user->id, $rev_id)))
				{
					die;
				}
			}
			else
			{
				if (!Cubix_Api::getInstance()->call('checkEscortReviewForAgency', array($this->user->id, $rev_id)))
				{
					die;
				}
			}

			$validator = new Cubix_Validator();

			if (!$this->_request->com)
				$validator->setError('com', 'Required');

			if ($validator->isValid())
			{
				Cubix_Api::getInstance()->call('addReviewComment', array($rev_id, $this->_request->com));
				echo json_encode(array('status' => 'ok'));
			}
			else
			{
				$status = $validator->getStatus();
				echo(json_encode($status));
			}

			die;
		}

	}
	public function addCommentAction()
	{
		$this->view->layout()->disableLayout();

		if ($this->user->user_type != 'escort' && $this->user->user_type != 'agency') die('Not availble for this account');

		$rev_id = $this->view->review_id = intval($this->_request->review_id);

		if (!$rev_id) die("Review was not found");

		if ($this->user->user_type == 'escort')
		{
			if (!Cubix_Api::getInstance()->call('checkEscortReview', array($this->user->id, $rev_id)))
			{
				die("Already replied");
			}
		}
		else
		{
			if (!Cubix_Api::getInstance()->call('checkEscortReviewForAgency', array($this->user->id, $rev_id)))
			{
                die("Already replied");
			}
		}

		if ($this->_request->isPost())
		{
			$validator = new Cubix_Validator();

			if (!$this->_request->com)
				$validator->setError('com', 'Required');

			if ($validator->isValid())
			{

				$client = new Cubix_Api_XmlRpc_Client();
				$review  = $client->call('Reviews.getReviewbyID', array($rev_id));
				Cubix_Email::sendTemplate('review_replied_to_member_v1', $review['author_email'], array(
					'showname'=> $review['showname'],
					'username'=> $review['author_username'],
					'comment'=>$this->_request->com,
					'link_to_sedcard' => APP_HTTP.'://'.$_SERVER['SERVER_NAME'].'/reviews/'.$review['showname'].'-'.$review['id'],
				));

			Cubix_Api::getInstance()->call('addReviewComment', array($rev_id, $this->_request->com));
				echo json_encode(array('status' => 'ok'));
			}
			else
			{
				$status = $validator->getStatus();
				echo(json_encode($status));
			}

			die;
		}
	}

	public function answerAction()
	{
		$this->view->layout()->disableLayout();

		if ($this->user->user_type != 'escort' && $this->user->user_type != 'agency') die;

		$review_id = intval($this->_request->review_id);
		$answer = intval($this->_request->answer);

		if (!in_array($answer, array(1, 2)))
			$answer = null;

		if ($review_id && !is_null($answer))
		{
			if ($this->user->user_type == 'escort')
			{
				$check = Cubix_Api::getInstance()->call('checkPendingReviewForEscort', array($this->user->id, $review_id));

				if ($check)
				{
					Cubix_Api::getInstance()->call('setPendingReviewForEscort', array($review_id, $answer));
					echo json_encode(array('status' => 'ok'));
				}
				else
				{
					echo json_encode(array('status' => 'error'));
				}
			}
			else
			{
				$check = Cubix_Api::getInstance()->call('checkPendingReviewForAgencyEscort', array($this->user->id, $review_id));
				if ($check)
				{
					Cubix_Api::getInstance()->call('setPendingReviewForEscort', array($review_id, $answer));
					echo json_encode(array('status' => 'ok'));
				}
				else
				{
					echo json_encode(array('status' => 'error'));
				}
			}
		}
		else
		{
			echo json_encode(array('status' => 'error'));
		}

		die;
	}

	public function getReviewsCountCityAction(){
		$country_id = (int)$_GET['country_id'];
		if(!$country_id){
			$country_id = false;
		}

		$cities = $this->model->getReviewsCountCity($country_id);
		echo(json_encode(array('data' => $cities)));
		die();
	}

	public static function getQualityName($id){
	    $name = '';
	    switch ($id){
            case "1" :$name = __('very_good');break;
            case "2" :$name = __('good');break;
            case "3" :$name = __('ok');break;
            case "4" :$name = __('bad');
        }
        return $name;
    }
}
