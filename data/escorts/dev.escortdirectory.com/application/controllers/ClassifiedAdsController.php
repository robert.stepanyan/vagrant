<?php

class ClassifiedAdsController extends Zend_Controller_Action
{
	protected $_session;

	public function init()
	{
        $this->_session = new Zend_Session_Namespace('classified-ads');
		$this->model = new Model_ClassifiedAds();

		$this->defines = $this->view->defines = Zend_Registry::get('defines');
		$this->cache = Zend_Registry::get('cache');
	}
	
	public function indexAction()
	{
        $this->view->page = $page = 1;

		$this->view->per_page = $per_page = 10;

		$this->view->sort = $sort = 'desc';
        $filter = array();

        $modelCities = new Model_Cities();
        $geoData = $modelCities->getByGeoIp();

        START_FILTERING: // Remember the point to make filtering again if needed

        // If we detect user's geo position, fetch all info about it
        // to fill in top search-bar
        // --------------------------------------
        if (!empty($geoData['country_id'])) {
            $filter['country_id'] = $geoData['country_id'];
            $this->view->country = ['id' => $geoData['country_id'], 'title' => $geoData['country_title']];

            if(!empty($geoData['city_id'])) {
                $filter['city_id'] =  $geoData['city_id'];
                $this->view->city = ['id' => $geoData['city_id'], 'title' => $geoData['city_title']];
            }
        }
        // --------------------------------------

		$cache_key = Cubix_Application::getId() . '_classified_ads_' . $page . '_' . $per_page . '_' . $sort . '_' . implode('_', $filter);
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		if ( ! $ret = $this->cache->load($cache_key) )
		{
			$ads = $this->model->getList($filter, $page, $per_page, $sort, $count);

			if ($ads)
				$ret = array('ads' => $ads, 'count' => $count);
			else
				$ret = null;

			$this->cache->save($ret, $cache_key, array(), 180); //3 min
		}

        // When we have no data found because the search was made by user
        // geo position, then just remove city from search-bar, if the result is
        // still empty, then remove country, if result is still empty remove location search
        // REMEMBER! This is only when location is detected by GEO IP not by manual search
        // ------------------------------------------
        if(isset($ads) AND count($ads) === 0) {

            if(isset($geoData['city_id'])) {
                unset($geoData['city_id'], $geoData['city_title'], $geoData['city_slug']);
                unset($this->view->city);
                goto START_FILTERING;
            }

            if(isset($geoData['country_id'])) {
                unset($geoData['country_id'], $geoData['country_title'], $geoData['country_slug']);
                unset($this->view->country, $filter['city_id'], $filter['country_id']);
                goto START_FILTERING;
            }
        }
        // ------------------------------------------

		$this->view->ads = $ret['ads'];
		$this->view->count = $ret['count'];

    }

	public function ajaxFilterAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$this->view->category = (int)$req->category;
		$this->view->country_id = (int)$req->country_id;
		$this->view->text = $req->text;
		$this->view->date_f = $req->date_f;
		$this->view->date_t = $req->date_t;
		$this->view->with_photo = $req->with_photo;
	}
	
	public function ajaxListAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$sort = $req->sort;

		if ($sort != 'asc') $sort = 'desc';

		$this->view->sort = $sort;

		$page = intval($req->page);
		if ($page < 1) $page = 1;
		$this->view->page = $page;

		$per_page_def = 10;
		$per_page = $this->_getParam('per_page', $per_page_def);
		$this->view->per_page = $per_page;
		
		$filter = array();
		
		$category = $req->category;
		$city = (int)$req->city_id;
		$country_id = (int)$req->country_id;
		$text = $req->text;
		$date_f = $req->date_f;
		$date_t = $req->date_t;
		$with_photo = $req->with_photo;
		
		if ( $category ) {
			$filter['category'] = $category;
		}
		
		if ( $city ) {
			$filter['city'] = $city;
		}

		if ( $country_id ) {
			$filter['country_id'] = $country_id;
		}
		
		if ( strlen($text) ) {
			$filter['text'] = $text;
		}

		if ( strlen($date_f) ) {
			$filter['date_f'] = $date_f;
		}

		if ( strlen($date_t) ) {
			$filter['date_t'] = $date_t;
		}

		if ( $with_photo == 1 ) {
			$filter['with_photo'] = 1;
		}

		$cache_key = Cubix_Application::getId() . '_classified_ads_' . $page . '_' . $per_page . '_' . $sort . '_' . implode('_', $filter);
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		if ( ! $ret = $this->cache->load($cache_key) )
		{
			$ads = $this->model->getList($filter, $page, $per_page, $sort, $count);

			if ($ads)
				$ret = array('ads' => $ads, 'count' => $count);
			else
				$ret = null;

			$this->cache->save($ret, $cache_key, array(), 180); //3 min
		}

		$this->view->ads = $ret['ads'];
		$this->view->count = $ret['count'];
	}

	public function adAction()
	{
		$this->view->layout()->disableLayout();

		$id = (int)$this->_request->ad_id;

		$this->view->ad = $ad = $this->model->get($id);

		$this->model->updateViewCount($id);

		if ( ! isset($this->view->ad->id) ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $this->view->getLink('classified-ads-index'));
			return;
		}

        // EDIR-2292
        // ------------------------------
		/* add more ads */
		//$this->view->per_page = $per_page = 4;
		//$this->view->page = $page = 1;
        //
		//
		// $filter = array('category' => $ad->category_id);
		//
		// $this->view->ads = $this->model->getList($filter, $page, $per_page, 'desc', $count, $id);
		// $this->view->count = 4;
        // ------------------------------
	}
	
	public function placeAdAction()
	{
		$this->view->layout()->disableLayout();

		$blackListModel = new Model_BlacklistedWords();
		$req = $this->_request;
		// $defines = $this->defines;
		// $plan = 'free';
		$data = array();
		$photos = array();
		$errors = array();
		if ( $req->isPost() ) 
		{
			try {
				$images = new Cubix_ImagesCommon();
				foreach ( $_FILES as $i => $file ) {
					if ( strlen($file['tmp_name']) ) {
						$photo = $images->save($file);
						if ( $photo && count($photos) < 3  ) {
							$photos[$photo['image_id']] = $photo;
						}
					}
				}
			} catch ( Exception $e ) {
				$errors['photos'][] = $e->getMessage();
			}

			$category = (int) $req->category;
			$city =  $req->city_id;   
			$phone = $req->phone;
			$email = $req->email;
			$duration = (int) $req->duration;
			$title = trim($req->title);
			$text = $req->text;

			if ( ! $category ) {
				$errors['category'] = 'Category is required!';
				// $errors['category'] = Cubix_I18n::translate('sys_error_required');
			}

			if ( ! $duration ) {
				$errors['duration'] = 'Duration is required!';
				// $errors['duration'] = Cubix_I18n::translate('sys_error_required');
			}

			if ( ! strlen($email) && ! strlen($phone) ) {
				$errors['phone_email'] = Cubix_I18n::translate('sys_error_phone_email_required');
			}

			if ( strlen($email) ) {
				$valid = new Cubix_Validator();
				
				if ( ! $valid->isValidEmail($email) ) {
					$errors['invalid_email'] = Cubix_I18n::translate('invalid_email');
				}				
			}

			if ( ! strlen($city) ) {
				$errors['city'] = 'City is required!';
				// $errors['city'] = Cubix_I18n::translate('sys_error_required');
			}

			if ( ! strlen($title) ) {
				$errors['title'] = 'Title is required!';
				// $errors['title'] = Cubix_I18n::translate('sys_error_required');
			}

			$_text = strip_tags(trim($text));
			$_text = str_replace('&nbsp;', '', $_text);
			$_text = preg_replace('/\s+/', '', $_text);

			
			if ( ! strlen($_text) ) {
				$errors['text'] = 'Description is required!';
				// $errors['text'] = Cubix_I18n::translate('sys_error_required');
			}
			else if ($bl_words = $blackListModel->checkWords($text, Model_BlacklistedWords::BL_TYPE_CLASSIFIED_ADS)){
				foreach($bl_words as $bl_word){
					$pattern = '/' . preg_quote($bl_word, '/') . '/';
					$text = preg_replace($pattern, '<abbr title="Blacklisted word" class = "black-listed" >' . $bl_word . '</abbr>', $text);
				}
				$errors['text'] = 'You can`t use word "'.$blackListModel->getWords().'"';
				
			}

			if ( count($errors) ) {
				$result = array(
					'status' => 'error',
					'msgs' => $errors
				);

				echo(json_encode($result));
				die;
			}

			$data = array(
				'category' => $category,
				'cities' => array($city),
				'phone' => $phone,
				'email' => $email,
				'duration' => $duration,
				'title' => $title,
				'text' => $text
			);
			
			$user = Model_Users::getCurrent();
			$data['user_id'] = null;
			if ( $user ) {
				$data['user_id'] = $user->id;
			}

			// $packages = $defines['classified_ad_packages_arr'];

			// if ( !in_array($plan, $packages) ) {
				// $this->_redirect($this->view->getLink('classified-ads-place-ad'));
			// }
			
			/************************* IP ****************************/
			$ip = Cubix_Geoip::getIP();
			
			$data['ip'] = $ip;
			/*********************************************************/

			$m_ads = new Model_ClassifiedAds();
			
			$save_data = array('data' => $data, 'photos' => $photos);
				
			$ad_id = $m_ads->save($save_data);		

			if ( is_array($ad_id) ) {
				$msgs = array(
					'text' => __('c_ads_error_text')
				);

				$result = array(
					'status' => 'error',
					'msgs' => $msgs
				);

				echo(json_encode($result));
				die;
			} else {
				$msgs = array(
					'text' => __('c_ads_success_text', array('ID' => $ad_id))
				);

				$result = array(
					'status' => 'success',
					'msgs' => $msgs
				);

				echo(json_encode($result));
				die;
			}
		}
	}
	
	public function ajaxRemovePhotoAction()
	{
		$image_id = (int) $this->_request->image_id;
		
		$status = array('status' => 'error');
		if ( isset($this->_session->photos[$image_id]) ) {
			unset($this->_session->photos[$image_id]);
			$status = array('status' => 'success');
		}
		
		die(json_encode($status));
	}
	
	public function successAction()
	{
		$this->view->id = (int) $this->_request->id;
	}
	
	public function errorAction()
	{
		
	}

	public function printAction()
	{
		$this->view->layout()->disableLayout();

		$ad_id = (int) $this->_request->id;

		$this->view->ad = $this->model->get($ad_id);
	}
}