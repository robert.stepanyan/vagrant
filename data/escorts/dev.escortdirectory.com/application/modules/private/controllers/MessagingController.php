<?php
class Private_MessagingController extends Zend_Controller_Action {
	
	/**
	 * @var Model_EscortItem
	 */
	protected $escort;

	/**
	 * @var Model_AgencyItem
	 */
	protected $agency;

	private $_model;

	private $member_model;

	private $agency_model;
	
	private $can_write_message = true;
	
	public function init() {
		$this->user = $this->view->user = Model_Users::getCurrent();
		$this->member_model = new Model_Members();
		$this->agency_model = new Model_Agencies();
		$this->view->layout()->setLayout('private-v2');
		
		$anonym = array();

		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			if($this->getRequest()->isXmlHttpRequest()){
				header("HTTP/1.1 403 Forbidden");
			}else{
				$this->_redirect($this->view->getLink('signin'));
			}
			return;
		}

		$cache = Zend_Registry::get('cache');
		$cache_key =  'v2_user_pva_' . $this->user->id;
		
		if ( $this->user->isAgency() ) {
			if ( ! $agency = $cache->load($cache_key) ) {
				$agency = $this->user->getAgency();
				$cache->save($agency, $cache_key, array(), 300);
			}

			$this->agency = $agency;
			$this->view->active_agency_escorts = $agency->getEscortsWithPackages();
			$this->view->all_agency_escorts = $agency->getEscorts();
			
			$escort_id = $this->_getParam('escort_id');
			$a_escort_id = $this->_getParam('a_escort_id');
			if ( $escort_id ) {

				$this->_model = new Cubix_PrivateMessagingEd('escort', $escort_id);
				
				if ( ! $this->_model->checkIfActiveHasPackage($escort_id) ) {
					$this->can_write_message = false;
				}
			} else {
				if ($a_escort_id && !$this->agency->hasEscort($a_escort_id)) {
					if ($this->getRequest()->isXmlHttpRequest()) {
						die(json_encode([
							'status' => 'error',
							'msgs' => __('cannot_write_message_to_user')
						]));
					} else {
						$this->_redirect($this->view->getLink('dashboard'));
					}
				} elseif ($a_escort_id && $this->agency->hasEscort($a_escort_id)) {
					$this->_model = new Cubix_PrivateMessagingEd('escort', $a_escort_id);
				} else {
					$this->_model = new Cubix_PrivateMessagingEd('agency', $this->agency->id);
				}
			}
		}
		else if ( $this->user->isEscort() ) {

			if ( ! $escort = $cache->load($cache_key) ) {
				$escort = $this->user->getEscort();
				$cache->save($escort, $cache_key, array(), 300);
			}

			$this->escort =  $escort;
			$this->_model = new Cubix_PrivateMessagingEd('escort', $escort->id);

			if ( ! $this->_model->checkIfActiveHasPackage($escort->id) ) {
				$this->can_write_message = false;
			}
		}
		else if ( $this->user->isMember() ) {
			if ( $this->user->status != 1 ) {
				$this->can_write_message = false;
			}
			$this->_model = new Cubix_PrivateMessagingEd('member', $this->user->id);
		}
		$this->view->can_write_message = $this->can_write_message;
		
		$this->view->unreadCount = $this->_model->getUnreadThreadsCount();
	}
	
	public function indexAction() {
		$this->view->layout()->disableLayout();
		
		$threads = $this->getThreadsAction();
		$this->contactsAction();

		$this->view->threads = $threads;
	}
	
	public function sendMessageAction()	{
		$validator = new Cubix_Validator();
		$participants = $this->_request->getParam('participants', array());
		$message_body = $this->_request->getParam('message');
		$message_body = stripslashes($message_body);
		$message_body = strip_tags($message_body);

		// if ( $this->user->isAgency() && ! $this->_getParam('escort_id') ) {
		// 	$validator->setError('agency-escort-id', 'Select escort');
		// }
		
		if ( strlen($participants) ) {
			$participants = explode(',', $participants);
		} else {
			$participants = array();
		}

		if ( ! count($participants) ) {
			$validator->setError('participants', 'Select participants');
		}
		
		if ( ! strlen($message_body) ) {
			$validator->setError('message-body', 'Message required');
		}
		
		if ( $validator->isValid() ) {
			foreach($participants as $k => $v) {
				$parts = explode('-', $v);
				if ( $parts[0] == 'member' ) {
					$participants[] = array('type' => 'member', 'user_id' => $parts[1], 'escort_id' => null);
				} elseif ( $parts[0] == 'escort' ) {
					$participants[] = array('type' => 'escort', 'user_id' => null, 'escort_id' => $parts[1]);
				} elseif ( $parts[0] == 'agency' ) {
					$participants[] = array('type' => 'agency', 'user_id' => null, 'escort_id' => $parts[1]);
				} elseif ( $parts[0] == 'agencyEscort' ) {
					$participants[] = array('type' => 'agencyEscort', 'user_id' => null, 'escort_id' => $parts[1], 'agency_id' => $parts[2]);
				} 
				unset($participants[$k]);
			}

			$this->_model->sendMessages($message_body, $participants);
		}
		
		die(json_encode($validator->getStatus()));
	}

	public function addPhotosAction() {
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$docs = new Cubix_Documents();
		$error = false;

        $participant = $this->_request->getParam('participant');
        $File = $_FILES['Filedata'];
        if (!$participant){
        	$error = 'Select Participant';
		}
        list($participant_type, $participant_id) = explode('-', $participant);
        if (!$participant_type || !$participant_id){
        	$error = 'Select Participant';
		}
//        image
		$extension = end(explode(".", $File["name"]));

		if (!in_array($extension, $docs->allowed_ext  ) )
		{
			$error = 'Not allowed file extension.';
		}

		if ($error) {

			$return = array(
				'status' => '0',
				'error' => $error
			);

		} else {
			// Save on remote storage
			$doc = $docs->save($File['tmp_name'], 'attached', Cubix_Application::getId(), $extension);
			if ($doc){
                $doc['name']= $File['name'];

                $this->_model->sendMessage('', $participant_type, $participant_id, false, array($doc));
                $doc['catalog_id'] = 'attached';
                $fileType = (in_array($doc['ext'], array('jpg','gif','png','bmp'))) ? 'image' : 'file';
                $return = array(
                    'status' => '1',
                    'file_url'=>$docs->getUrl(new Cubix_Images_Entry($doc)),
                    'file_type' => $fileType,
                    'file_name' => $doc['name'],
                    'sender' => __('me'),
                    'date' => date('Y-m-d | i:s', time()),
                );
			}else{
				$return= array(
					'status'=>0,
					'error'=>'Unable save file!',
				);
			}

		}

		echo json_encode($return);
	}

	public function sendMessageAjaxAction()	{
		$this->view->layout()->disableLayout();
		
		$this->_request->setParam('message', preg_replace('|<img src="/img/private/private-messaging/_emoticons/([0-9]+).png" class="emoticons">|', '^%${1}%^', $this->_request->message));

		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'participant' => '',
			'message' => 'notags|special',
			'escort_id' => 'int-nz'
		);
		$data->setFields($fields);
		$data = $data->getData();
		$this->view->data = $data;
		
		list($participant_type, $participant_id) = explode('-', $data['participant']);

		
		if ( $participant_type == 'escort' || $participant_type == 'agency' ) {
			$m_esc = new Model_EscortsV2();
			$this->view->showname = $m_esc->getShownameById($participant_id);
		} else if ( $participant_type == 'member' ) {
			$m_user = new Model_Users();
			$us = $m_user->getById($participant_id);
			$this->view->username = $us->username;
		}
		
		if ( $this->user->isAgency() ) {
			$this->view->agency_escorts = $this->agency->getEscortsWithPackages();
		}

		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			
			if ( ! $participant_type || ! $participant_id ) {
				$validator->setError('participant', 'Select Participant');
			}
			
			if ( ! strlen($data['message']) ) {
				$validator->setError('f-message', 'Message required');
			}

			// if ( $this->user->isAgency() && ! $data['escort_id'] ) {
			// 	$validator->setError('escort_id', 'Select escort');
			// }
			
			$result = $validator->getStatus();
			
			if ( ! $validator->isValid() ) {
				die(json_encode($result));
			} else {
				list($participant_type, $participant_id) = explode('-', $data['participant']);

				$result['sent-to-thread'] = $this->_model->sendMessage($data['message'], $participant_type, $participant_id, false);
				$this->addContactAction(true);
				
				$result['message'] = $data['message'];
				$result['sender'] = __('me');
				$result['date'] = date('Y-m-d', time()) . ' | ' . date('i:s', time());
				die(json_encode($result));
			}
		}
	}
	
	public function sendMessageFromProfileAction() {
		$this->view->layout()->disableLayout();
		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'participant'	=> '',
			'message' => 'notags|special',
			'escort_id' => 'int-nz'
		);
		$data->setFields($fields);
		$data = $data->getData();

		$this->view->data = $data;
		
		list($participant_type, $participant_id) = explode('-', $data['participant']);
        $username = '';
        $pm_is_blocked = 0;
		if ( $participant_type == 'escort' ) {
			$m_esc = new Model_EscortsV2();
			$esc = $m_esc->getById($participant_id);
			$pm_is_blocked = $esc->pm_is_blocked;
			$username = $this->view->showname = $esc->showname;
			
		} else if ( $participant_type == 'member' ) {
			$m_user = new Model_Users();
			$us = $m_user->getById($participant_id);
			$pm_is_blocked = $us->pm_is_blocked;
			$username = $this->view->username = $us->username;
		}else if ( $participant_type == 'agency' ) {
			$m_agency = new Model_Agencies();
			$agency = $m_agency->get($participant_id);
            $pm_is_blocked = $m_agency->getPmIsBlocked($participant_id);
			$username = $this->view->username = $agency->showname;
		}

        if ($this->user->pm_is_blocked || $pm_is_blocked){
			die(__('pm_disabled_by_user',array('username'=>($this->user->pm_is_blocked) ? 'You' : $username)));
        }

		if ( $this->user->isAgency() ) {
			$this->view->agency_escorts = $this->agency->getEscortsWithPackages();
		}
		
		if ( $this->_request->isPost() ) {

			$validator = new Cubix_Validator();
			
			//$blackListModel = new Model_BlacklistedWords();
			if ( ! $participant_type || ! $participant_id ) {
				$validator->setError('participant', 'Select Participant');
			}
			
			if ( ! strlen($data['message']) ) {
				$validator->setError('f-message', 'Message required');
			} /*elseif( $blackListModel->checkWords($data['message']) ) {
				$validator->setError('f-message', 'You can`t use words "'. $blackListModel->getWords() .'"');
			}*/
			
			/* if ( ! $data['captcha'] ) {
				$validator->setError('captcha', 'Captcha required');//Captcha is required
				}
			else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', 'Captcha is invalid');//Captcha is invalid
				}
			} */

			// if ( $this->user->isAgency() && ! $data['escort_id'] ) {
			// 	$validator->setError('escort_id', 'Select escort');
			// }
			
			$result = $validator->getStatus();
			
			if ( ! $validator->isValid() ) {
				die(json_encode($result));
			} else {
				/* if message from pinboard */
				/*$pinboard_post_id = intval($this->_request->pinboard_post_id);
				
				if ($pinboard_post_id)
				{
					$data['message'] = '<strong>PINBOARD #' . $pinboard_post_id . ':</strong> ' . $data['message'];
				}*/
				/****************************/
				
				list($participant_type, $participant_id) = explode('-', $data['participant']);
				$r = $this->_model->sendMessage($data['message'], $participant_type, $participant_id, false);
				$this->addContactAction(true);
				// var_dump($r); die();
				
				if($participant_type == 'escort'){
					Cubix_Email::sendTemplate( 'pm_received_escort_v1', $esc->email, 
						array(
							'showname' => $esc->showname,
							'message' => $data['message'],
							'username' => (isset($this->user->showname)?$this->user->showname:$this->user->username),
						)
					); 
				}elseif($participant_type == 'agency'){
					Cubix_Email::sendTemplate( 'pm_received_agency_v1', $agency->email, 
						array(
							'agency' => $agency->showname,
							'message' => $data['message'],
							'username' => (isset($this->user->showname)?$this->user->showname:$this->user->username),
						)
					); 
				}elseif($participant_type == 'member'){
					Cubix_Email::sendTemplate( 'pm_received_member_v1', $us->email, 
						array(
							'message' => $data['message'],
							'user' => $us->username,
							'username' => (isset($this->user->showname)?$this->user->showname:$this->user->username),
						)
					); 
				}
				
				
				$result['message'] = $data['message'];
				$result['sender'] = __('me');
				$result['date'] = date('Y-m-d', time()) . ' | ' . date('i:s', time());
				die(json_encode($result));
			}
		}
	}
	
	public function getThreadsAction() {
		$this->view->layout()->disableLayout();
		
		$a_escort_id = $this->_request->getParam('a_escort_id', null); // agency case
		
		$page = $this->_request->getParam('page', 1);
		$per_page = $this->_request->getParam('per_page');

		if ( ! $per_page ) {
			$per_page = 100;
		}

		$this->view->per_page = $per_page;

		$result = $this->_model->getThreads($page, $per_page, $a_escort_id);

		// Well logic was canceled by katy
		// ----------------------------------------------------------------------------------------
		//$user_id = null;
		//if($this->user->isEscort()) $user_id = $this->escort->id;
		//elseif($this->user->isMember()) $user_id = $this->user->id;
		//elseif($this->user->isAgency()) $user_id = $this->agency->id;

		// This just attaches to thread a new property
		// which helps to detect, whether the thread is originally send by this user or nah
		// --------------------------------------------
		//$result['data'] = $this->_model->updateThreadsPrimaryState($result['data'], $user_id);
		// --------------------------------------------

		// ----------------------------------------------------------------------------------------

		foreach ($result['data'] as &$thread) {

			$thread['body'] = preg_replace('/\^%([0-9]+)%\^/', '<img src="/img/private/private-messaging/_emoticons/${1}.png" class="emoticons">', $thread['body']);

			if ($thread['user_type'] == 'member'){
				$thread['photo_url'] = $thread['member_photo_hash'] ? 
				$this->member_model->getLogoUrl($thread['member_photo_hash'], $thread['member_photo_ext'], $thread['member_id'], 'thumb_cropper_ed') : '';
			} else if ($thread['user_type'] == 'escort') {
				$esort_id = $thread['escort_id'];
				$hash = $thread['escort_photo_hash'];
				$ext = $thread['escort_photo_ext'];

				$photo_item = new Model_Escort_PhotoItem(
					array(
						'application_id' => Cubix_Application::getId(),
						'escort_id' => $esort_id,
						'hash' => $hash,
						'ext' => $ext
					)
				);

				$thread['photo_url'] = $photo_item->getUrl('backend_smallest');
			} else if ($thread['user_type'] == 'agency') {
				if ($thread['agency_logo_hash']){
					$thread['photo_url'] = $thread['agency_logo_hash'] ? 
						$this->agency_model->getLogoUrl($thread['agency_logo_hash'], $thread['agency_logo_ext'], $thread['agency_id'], 'backend_smallest') :
							'';
				} elseif ($hash = $thread['escort_photo_hash']){
					$esort_id = $thread['escort_id'];
					$hash = $thread['escort_photo_hash'];
					$ext = $thread['escort_photo_ext'];

					$photo_item = new Model_Escort_PhotoItem(
						array(
							'application_id' => Cubix_Application::getId(),
							'escort_id' => $esort_id,
							'hash' => $hash,
							'ext' => $ext
						)
					);

					$thread['photo_url'] = $photo_item->getUrl('backend_smallest');
				}
			}

			if ($thread['escort_agency_id']) {
				$escort_agency = $this->agency_model->get($thread['escort_agency_id']);

				$thread['escort_agency_name'] = $escort_agency->showname;
			}
		}

		$this->view->threads = $result['data'];
		$this->view->count = $result['count'];
		$this->view->page = $page;
		$this->view->per_page = $per_page;

		return $result;
	}
	
	public function removeThreadAction() {
		$id = $this->_getParam('id');
		if ( ! $id ) die;
		
		$this->_model->removeThread($id);
		
		die;
	}
	
	public function blockThreadAction() {
		$this->view->layout()->disableLayout();

		// die(json_encode(array($this->_request->id, $this->_request->a_escort_id)));

		$id = $this->_getParam('id');
		$a_escort_id = $this->_getParam('a_escort_id', null);
		
		if ( ! $id ) die;
		
		$this->_model->blockThread($id, $a_escort_id);
		$this->removeContactAction(true);
		
		die;
	}
	
	public function unblockThreadAction() {
		$id = $this->_getParam('id');
		$a_escort_id = $this->_getParam('a_escort_id', null);
		
		if ( ! $id ) die;
		
		$this->_model->unblockThread($id, $a_escort_id);
		
		die;
	}
	
	public function getParticipantsAction()	{
		$search = $this->_request->getParam('search');
		$participants = $this->_model->getParticipants($search);
		
		$result = array();
		
		//$result[] = array('escort-15', 'Escorts', null, null);
		
		foreach($participants['escorts'] as $p) {
			$result[] = array('escort-' . $p['id'], $p['showname'] . '(escort)', null, null);
		}
		
		//$result[] = array('members', 'Members', null, null);
		
		foreach($participants['members'] as $p) {
			$result[] = array('member-' . $p['id'], $p['username'] . '(member)', null, null);
		}
		
		echo json_encode($result);die;
	}
	
	public function contactsAction() {
		$this->view->layout()->disableLayout();
		
		$result = $this->_model->getContacts(1, 10);
		
		$this->view->contacts = $result['data'];
		$this->view->contacts_count = $result['count'];
	}
	
	public function removeContactAction($isAjax) {
		if (!$isAjax) {
			$p = $this->_getParam('id');
			$p = explode('-', $p);
		} else {
			$p = array($this->_getParam('participant_type'), $this->_getParam('participant_id'));
		}

		list($type, $id) = $p;
		if ( ! $id || ! $type ) die;
		
		$this->_model->removeContact($id, $type);
		if (!$isAjax){
			if ( $this->_getParam('get_html') ) {
				$result = $this->_model->getContacts(1, 10);
				$this->view->contacts = $result['data'];
				die(json_encode(array('html' => $this->view->render('/private-messaging/contacts.phtml'))));
			}
			
			die;
		}
	}
	
	public function addContactAction($isAjax) {
		$response = array(
			'success' => true,
			'msg' => ''
		);

		$contacts = $this->_model->getContacts(1, 1000)['data'];

		if (count($contacts) >= 10) {
			$response['success'] = false;
			$response['msg'] = 'You can have only 10 contacts.';
			
			if (!$isAjax){
				die(json_encode($response));
			} else {
				return json_encode($response);
			}
		}
		if (!$isAjax){
			$p = $this->_getParam('id');
		} else {
			$p = $this->_getParam('participant');
		}
		$p = explode('-', $p);
		
		list($type, $id) = $p;

		if ( ! $id || ! $type ) die;

		foreach ($contacts as $contact) {
			if ($contact[$type . '_id'] == $id){
				return;
			}
		}

		$this->_model->addContact($id, $type);
		if (!$isAjax){
			die(json_encode($response));
		} else {
			return json_encode($response);
		}
	}
	
	public function getThreadAction() {
		$this->view->layout()->disableLayout();
		
		$this->view->id = $id = $this->_getParam('id');
		$this->view->escort_id = $escort_id = $this->_getParam('escort_id');
		
		if ( ! $id  ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}
		// if ( $this->user->isAgency() && ! $escort_id ) {
		// 	$this->_redirect($this->view->getLink('private-v2'));
		// }
		
		
		$result = $this->_model->getThread($id, null, null, 'asc');
		$thread = $result['data'];

		foreach ($thread as $i => $t) {

			$thread[$i]['body'] = preg_replace('/\^%([0-9]+)%\^/', '<img src="/img/private/private-messaging/_emoticons/${1}.png" class="emoticons">', $t['body']);
		}
		
		$participant = $this->_model->getThreadParticipant($id);
		
		$this->view->thread = $thread;
		$this->view->count = $result['count'];
		$this->view->page = $page;
		$this->view->per_page = $per_page;
		
		
		$participant_passive = false;

		if ( $participant['user_id'] ) {
			$participant_name = $participant['member_username'];
			$participant_id = 'member-' . $participant['user_id'];
			if ( $participant['status'] != 1 ) {
				$participant_passive = true;
			}
		} else {
			$participant_name = $participant['escort_showname'];
			$participant_id = 'escort-' . $participant['escort_id'];
			
			if ( ! $this->_model->checkIfActiveHasPackage($participant['escort_id']) ) {
				$participant_passive = true;
			}
			
		}
		$this->view->participant_passive = $participant_passive;
		$this->view->participant_name = $participant_name;
		$this->view->participant_id = $participant_id;


		// ---------------------------------------------
		$model = new Model_EscortsV2();
		$participant_escort = $model->get($participant['escort_id']);
		$participant_escort = new Model_EscortV2Item($participant_escort);

		$this->view->participant_age = $participant_escort->age;
		$this->view->participant_country = $participant_escort->country;
		$this->view->participant_city = $participant_escort->city;
		// ---------------------------------------------

		if ( $this->user->isEscort() ) {
			$this->view->escort = $this->escort;
		} elseif ( $this->user->isAgency()  ) {
			$escorts = new Model_Escorts();
			$this->view->escort = $escorts->getById($escort_id);
			$this->view->self_escort_id = $escort_id;
		}
		
		$this->view->thread_id = $id;
		
		$this->view->layout()->global_btn_back_url = $this->view->getLink('private-messaging');
	}
	
	public function notifyByEmailAction() {
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$notify = intval($this->_getParam('notify'));
		$this->_model->notifyByMail($this->user->id,$notify);
	}
	
	public function notifyByEmailTextAction() {
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$notify = intval($this->_getParam('notify'));
		$this->_model->notifyByMailText($this->user->id,$notify);
	}
	
	public function checkForNewMessagesAction() {
		$curr_user =  Model_Users::getCurrent();
		if (  ! $curr_user ) {
			die(json_encode(array(
				'result'	=> false,
				'message'	=> 'Not logged in'
			)));
		}
		
		$this->view->layout()->disableLayout();
		$count = $this->_model->getUnreadThreadsCount();
		
		die(json_encode(array(
			'result'	=> true,
			'message'	=> 'Ok',
			'count'	=> $count
		)));
	}

	public function pmNewMessagePopupAction() {
		$curr_user =  Model_Users::getCurrent();
		if (  ! $curr_user ) {
			die(json_encode(array(
				'result'	=> false,
				'message'	=> 'Not logged in'
			)));
		}
		$this->view->layout()->disableLayout();
		$this->view->pmCount = intval($this->_request->pmCount);
	}
}

?>
