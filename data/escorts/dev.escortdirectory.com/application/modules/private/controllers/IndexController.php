<?php

class Private_IndexController extends Zend_Controller_Action
{
	/**
	 * @var Model_Escort_Profile
	 */
	protected $profile;

	/**
	 * @var Model_EscortItem
	 */
	protected $escort;

	/**
	 * @var Model_AgencyItem
	 */
	protected $agency;

	/**
	 * @var Zend_Session_Namespace
	 */
	protected $session;

	public static $linkHelper;

	public function init()
	{

	    if($_GET['edo']) {
            $clear_cache = Model_Applications::clearCache();
            var_dump($clear_cache);die;
        }

		$cache = Zend_Registry::get('cache');
		self::$linkHelper = $this->view->getHelper('GetLink');

		$anonym = array();

		$this->user = Model_Users::getCurrent();
		$this->defines = $this->view->defines = Zend_Registry::get('defines');
		$this->client = Cubix_Api::getInstance();

		if (! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {

			if($this->getRequest()->isXmlHttpRequest()){
				header("HTTP/1.1 403 Forbidden");
			}else{
				$this->_redirect($this->view->getLink('signin'));
			}

			return;
		}

		$cache_key =  'v2_user_pva_' . $this->user->id;
		$this->view->layout()->setLayout('private/main');
		 
		if ( $this->user->isAgency() ) {

			if ( ! $agency = $cache->load($cache_key) ) {
				$agency = $this->user->getAgency();
				$cache->save($agency, $cache_key, array(), 300);
			}
			$this->agency = $this->view->agency = $agency;
			$this->view->has_added_any_escort = Model_AgencyItem::hasAddedAnyEscort($agency->id);

			//$this->agencyDashboardAction();
			//$this->view->layout()->enableLayout();
		} else if ( $this->user->isEscort() ) {

			if (! $escort = $cache->load($cache_key) ) {
				$escort = $this->user->getEscort();
				$cache->save($escort, $cache_key, array(), 300);
			}
			$this->escort = $this->view->escort = $escort;
			$is_profile_partially = ($this->escort->status & Model_Escorts::ESCORT_STATUS_PARTIALLY_COMPLETE);
			$this->mode = $this->view->mode = ( !$is_profile_partially ) ? 'update' : 'create';

			/* Grigor Update */
			if (
				$this->escort->status & Model_Escorts::ESCORT_STATUS_SUSPICIOUS_DELETED &&
				in_array($this->_request->getActionName(), array('tours', 'escort-reviews', 'settings', 'support', 'happy-hour', 'client-blacklist','plain-photos'))
			) { //'plain- photos',
				$this->_redirect($this->view->getLink('private'));
			}
			/* Grigor Update */

			
			$config = Zend_Registry::get('escorts_config');
			if ( 'create' == $this->mode ) {
				$steps = array('completeProfileStep','biographyStep', 'aboutMeStep', 'workingCitiesStep', 'servicesStep',
							'workingTimesStep','contactInfoStep','galleryStep', 'finishStep');
			}else{
				$steps = array('biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times');

				if ($config['profile']['rates'])
					$steps[] = 'prices';
				
				$steps[] = 'contact-info';
				$steps[] = 'gallery';
			}

		}

		$this->view->user = $this->user;
		$this->view->show_online_status = $this->user->getShowOnlineStatus();

		$this->view->steps = $steps;

	
	}

	protected $_c = 0;

	protected $steps = array('biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times', 'prices', 'contact-info', 'finish');
	protected $_posted = false;

	public function indexAction()
	{ 
		if ( ! is_null($this->_getParam('ajax')) ) {
			$this->view->layout()->disableLayout();
		}

		$modelSupport = new Model_Support();
		$supportUnreadsCount = $this->view->supportUnreadsCount = $modelSupport->getUnreadsCountV2($this->user->id);

		// $modelBlog = new Model_Blog();
		// $this->view->last_blog_post = $modelBlog->getLatestPost();
		if ( $this->user->isAgency() ) {
			
			if ( ! is_null($this->_getParam('ajax')) ) {
				$this->_helper->viewRenderer->setScriptAction('dashboard-agency');
			}
			
			$this->view->all_agency_escorts = $this->agency->getEscorts();

			// $this->view->layout()->enableLayout();

			$agencies_model = new Model_Agencies();
			$this->view->profile = Cubix_Api_XmlRpc_Client::getInstance()->call('Agencies.getAgencyProfileOverview', array($this->user->id));

			$this->view->counts = $agencies_model->getCountsById($this->agency->id);

			$private_messaging = new Cubix_PrivateMessaging('agency', $this->agency->id);
			$unread_threads_count = $private_messaging->getUnreadThreadsCount();

			$pending = Cubix_Api::getInstance()->call('getPendingReviewsForAgencyEscorts', array($this->user->id));
			$this->view->pending_reviews_count = $pending['count'];
		} else if ( $this->user->isEscort() ) {

			if ( ! is_null($this->_getParam('ajax')) ) {
				$this->_helper->viewRenderer->setScriptAction('dashboard-escort');
			}
			
			$is_profile_partially = ($this->escort->status & Model_Escorts::ESCORT_STATUS_PARTIALLY_COMPLETE);
			$this->mode = $this->view->mode = ($this->escort->hasProfile() && !$is_profile_partially && $this->step != 'finish') ? 'update' : 'create';

            // If the user is escort and did not verify the email
            // we show only the Verification STEPS
            // ----------------------------------
			if ($this->user->status == -1 && $this->user->isEscort()) {
                $this->mode = $this->view->mode = Model_Users::PA_MODE_LIMITED;
            }
            // ----------------------------------

            // if('create' == $this->mode) {
			// 	$hash = "#profile/completeProfile";
           	// 	return $this->_redirect($this->view->getLink('private') . $hash);
			// }
			
			$private_messaging = new Cubix_PrivateMessaging('escort', $this->escort->id);
			$unread_threads_count = $private_messaging->getUnreadThreadsCount();

			$model_escorts_v2 = new Model_EscortsV2();
			$escortItem = $model_escorts_v2->get($this->escort->id);
			
			// Profile Overview
			$this->view->additional_cities = $escortItem->getAdditionalCities($escortItem->base_city_id);
			$this->view->profile = Cubix_Api_XmlRpc_Client::getInstance()->call('Escorts.getEscortProfileOverview', array($this->escort->id));
			$this->view->add_phones = $model_escorts_v2->getAdditionalPhones($this->escort->id);
			$this->view->tours = $this->client->call('getEscortToursV2', array($this->escort->id, Cubix_I18n::getLang(), false,1, 1, 0));
			$this->view->active_package = Cubix_Api::getInstance()->call('getEscortActivePackage', array($this->escort->id,Cubix_I18n::getLang()));

			$pending = Cubix_Api::getInstance()->call('getPendingReviewsForEscort', array($this->user->id, $this->escort->id));
			$this->view->pending_reviews_count = $pending['count'];
			

		} else if ( $this->user->isMember() ) {

			if ( ! is_null($this->_getParam('ajax')) ) {
				$this->_helper->viewRenderer->setScriptAction('dashboard-member');
			}
			$follow_model = new Model_Follow();
			$this->view->follow_count = $follow_model->getMemberFollowingCount($this->user->id);

			$private_messaging = new Cubix_PrivateMessaging('member', $this->user->id);
			$unread_threads_count = $private_messaging->getUnreadThreadsCount();
			
			$modelMember = new Model_Members();
			$user_data = $modelMember->getMembersInfoByUsername($this->user->username);
			if($user_data['langs']){
				$user_data['langs'] = unserialize($user_data['langs']);
			}
			$this->view->member = $user_data;
			$this->view->avatar = $modelMember->getLogoUrl($user_data['hash'], $user_data['ext'], $this->user['member_data']['id'], 'pa_avatar');
		}

		/* TEMPORARY CLOSED
		// Forum Api
		 $forum = new Cubix_Forum2Api();
		$data = $forum->getLastTopicFirstPost();

		$post_data = json_decode($data);
		
		if ($post_data->post)
		{

			$this->view->last_post = $post_data->post;
			$this->view->last_post_topic_slug = $post_data->topic_slug;
		} 
		*/

		$this->view->signup_type = $this->_getParam('success');
		$this->view->unread_threads_count = $unread_threads_count;

	}

	public function last7DaysStatisticsAction() {

        $result = Model_Analytics::collect_escort_profile_statistics($this->escort->id, 7);
	    die(json_encode($result));
    }

	private function dateDiff($date_start, $date_end)
	{
	    $difference = $date_end - $date_start;
        $minutes = floor(($difference / 3600) * 60); // 3600 seconds in an hour

		return $minutes;
	}

	const HH_STATUS_ACTIVE  = 1;
	const HH_STATUS_PENDING = 2;
	const HH_STATUS_EXPIRED = 3;

	private function makeDate($week_day, $h_from, $h_to)
	{
		$month = date('m', $week_day);
		$day = date('d', $week_day);
		$year = date('Y', $week_day);

		$day_to = $day;
		if ( $h_to < $h_from ) {
			$day_to += 1;
		}

		return array('hh_date_from' => mktime($h_from, 0, 0, $month, $day, $year), 'hh_date_to' => mktime($h_to, 0, 0, $month, $day_to, $year));
	}

	public function happyHourAction()
	{
		$req = $this->_request;
		$data = $req->data;

		if ( $req->isPost() ) {
			$validator = new Cubix_Validator();
			$def_currency = Model_Applications::getDefaultCurrencyTitle();
			foreach ( $data as $escort_id => $d ) {

				$hh_status = Cubix_Api::getInstance()->call('getHappyHourStatus', array($escort_id));

				if ( ! isset($data[$escort_id]['hh_save']) ) {
					$data[$escort_id]['hh_save'] = 0;
				}

				/*if ( ! strlen($d['hh_motto']) )
					$validator->setError('err_motto', 'Required');*/

				if ( $hh_status == self::HH_STATUS_PENDING )
				{
					if ( ! isset($d['hh_week_day']) || ! isset($d['hh_hour_from']) || ! isset($d['hh_hour_to']) ) {
						$validator->setError('err_date', 'Date is Required');
					}
					else {
						$dates = $this->makeDate($d['hh_week_day'], $d['hh_hour_from'], $d['hh_hour_to']);
						$data[$escort_id]['hh_date_from'] = $dates['hh_date_from'];
						$data[$escort_id]['hh_date_to'] = $dates['hh_date_to'];
						$d['hh_date_from'] = $dates['hh_date_from'];
						$d['hh_date_to'] = $dates['hh_date_to'];


						$diff_minutes = $this->dateDiff($d['hh_date_from'], $d['hh_date_to']);

						if ( ! strlen($d['hh_date_from']) || ! strlen($d['hh_date_to']) )
							$validator->setError('err_date', 'Date is Required');
						else if ( $d['hh_date_to'] < $d['hh_date_from'] )
							$validator->setError('err_date', 'Date until must be bigger then Date From');
						else if ( $d['hh_date_from'] < time() )
							$validator->setError('err_date', 'Date must be in the future');
						else if ( ! $this->user->isAgency() && $diff_minutes > 300 )
							$validator->setError('err_date', 'The duration of happy hour could not be grater then 5 hour !');
					}

					if ( $this->user->isAgency() ) {
						$diff_hour = $diff_minutes / 60;
						$used_time = Cubix_Api::getInstance()->call('getAgencyUsedHHHours', array($this->agency->id, $escort_id));

						if ( $used_time + $diff_hour > 10 )
							$validator->setError('err_date', 'The duration of happy hour could not be grater then ' . (10 - $used_time) . ' hour !');
					}


					if ( isset($d['outcall_rates']) && count($d['outcall_rates']) > 0 ) {
						$has_hh_price = false;
						foreach ( $d['outcall_rates'] as $rate_id => $ir ) {
							if( strlen($ir) ) {

								if ( $d['old_outcall_rates'][$rate_id] < $ir + 10) {
									$validator->setError('err_hh_hour_outcall', 'please make sure to specify smaller price at least 10 '.$def_currency);
								}

								if ( ! $has_hh_price ) {
									$has_hh_price = true;
								}
							}
						}
						if ( ! $has_hh_price )
							$validator->setError('err_hh_hour_outcall', 'At least one "Happy Hour Rate" is Required for Outcall');
					}

					if ( isset($d['incall_rates']) && count($d['incall_rates']) > 0 ) {
						$has_hh_price = false;
						foreach ( $d['incall_rates'] as $rate_id => $ir ) {
							if( strlen($ir) ) {

								if ( $d['old_incall_rates'][$rate_id] < $ir + 10 ) {
									$validator->setError('err_hh_hour', 'please make sure to specify smaller price at least 10 '.$def_currency);
								}

								if ( ! $has_hh_price ) {
									$has_hh_price = true;
								}
							}
						}

						if ( ! $has_hh_price )
							$validator->setError('err_hh_hour', 'At least one "Happy Hour Rate" is Required for Incall');
					}
				}
			}

			if ( $validator->isValid() ) {
				//print_r($data); die;
				Cubix_Api::getInstance()->call('setHappyHour', array($data));
			}

			die(json_encode($validator->getStatus()));
		}

		if ( $this->user->isAgency() ) {
			$this->view->escorts = $escorts_a = $this->agency->getEscortsPerPage(1, 1000, Model_Escorts::ESCORT_STATUS_ACTIVE);
		}
	}

	public function happyHourResetAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$escort_id = intval($req->escort_id);

		$escort_data = Cubix_Api::getInstance()->call('resetHappyHour', array($escort_id));
		die;
	}

	public function happyHourFormAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		Cubix_I18n::setLang($req->lang_id);
		$escort_id = intval($req->escort_id);

		$hh_status = $this->view->hh_status = Cubix_Api::getInstance()->call('getHappyHourStatus', array($escort_id));

		$escort_data = Cubix_Api::getInstance()->call('getHappyHour', array($escort_id));

		if ( $hh_status == Model_EscortsV2::HH_STATUS_EXPIRED ) {
			if ( $escort_data['agency_id'] ) {
				$dateDiff = strtotime('+ 7 days', $escort_data['hh_date_to']) - time();
			}
			else {
				$dateDiff = strtotime('+ 3 days', $escort_data['hh_date_to']) - time();
			}
			$days = floor($dateDiff / (60*60*24));
			$this->view->activate_after_days = $days;
		}

		$this->view->escort_data = $escort_data;
		$this->view->defines = Zend_Registry::get('defines');
	}


  /* Grigor Update */

    public function profileStatusAction(){
        if ( $this->user->isEscort() ) {
            $escort = $this->user->getEscort();
			$escort_id = $escort->id;

            $client = Cubix_Api_XmlRpc_Client::getInstance();

            $action = $this->_request->getParam('act');
			
            $status = $client->call('Escorts.profileStatus', array($escort_id,$action));

            /* clear cache */
			$cache = Zend_Registry::get('cache');
			$cache_key =  'v2_user_pva_' . $this->user->id;
			$cache->remove($cache_key);
			/**/
			echo $status;
            die;
            //$this->_redirect($this->view->getLink('private-v2'));

        }
    }

    public function profileDeleteAction(){
        if ( $this->user->isEscort() ) {
            $this->view->layout()->disableLayout();
            $escort = $this->user->getEscort();
			$escort_id = $escort->id;

            if ( $this->_request->isPost() ) {

                $validator = new Cubix_Validator();

                $req = $this->_request;

				if (!trim($req->comment))
                    $validator->setError('comment', 'Required');

                if ( $validator->isValid() ) {
                    $leaving_reason = $req->comment;
                    $del_hash = Cubix_Salt::generateSalt($escort->showname);

                    $client = Cubix_Api_XmlRpc_Client::getInstance();
                    $client->call('Escorts.addDelData', array($escort_id,$del_hash,$leaving_reason));

                    Cubix_Email::sendTemplate('escort_delete', $this->user->email, array(
                        'del_hash' => $del_hash,
                        'email' => $this->user->email,
                        'showname' => $escort->showname
                    ));
                }


                die(json_encode($validator->getStatus()));
            }
        }
    }

    public function profileRestoreAction(){
        if ( $this->user->isEscort() ) {
            $this->view->layout()->disableLayout();
            $escort = $this->user->getEscort();
			$escort_id = $escort->id;

            $client = Cubix_Api_XmlRpc_Client::getInstance();
            $client->call('Escorts.restore', array($escort_id));

            $cache = Zend_Registry::get('cache');
            $cache_key =  'v2_user_pva_' . $this->user->id;
            $cache->remove($cache_key);

            $this->_response->setRedirect($this->view->getLink('private-v2'));
        }
    }

    public function confirmDeletionAction(){
        if ( $this->user->isEscort() ) {

            $escort = $this->user->getEscort();
			$escort_id = $escort->id;

            $hash = $this->_getParam('hash');
            $model = new Model_EscortsV2();

            $us = $model->getById($escort_id);

            if ( $model->checkhash($escort_id, $hash) ) {

                $client = Cubix_Api_XmlRpc_Client::getInstance();
                $res = $client->call('Escorts.deleteTemporary', array($escort_id));

                $res = json_decode($res);

                if( isset($res->success) && $res->success ){
                    $cache = Zend_Registry::get('cache');
                    $cache_key =  'v2_user_pva_' . $this->user->id;
                    $cache->remove($cache_key);

                    $this->_response->setRedirect($this->view->getLink('private-v2'));
                }

            }
        }
        $this->_response->setRedirect($this->view->getLink('private-v2'));
    }


//    public function restoreAction(){
//        if ( $this->user->isAgency() ) {
//			$escort_id = intval($this->_getParam('escort_id'));
//			if ( ! $this->agency->hasEscort($escort_id) ) {
//				die('Permission denied!');
//			}
//		}
//		elseif ( $this->user->isEscort() ) {
//			$escort_id = $this->escort->getId();
//		}
//		if ( ! $escort_id ) die;
//
//
//        $client = Cubix_Api_XmlRpc_Client::getInstance();
//
//        $client->call('Escorts.restore', array($escort_id));
//        exit;
//    }
//
//    public function undoDeletionAction(){
//        if ( $this->user->isEscort() ) {
//
//            $escort = $this->user->getEscort();
//			$escort_id = $escort->id;
//
//            $model = new Model_EscortsV2();
//
//            $us = $model->getById($escort_id);
//
//
//            $client = Cubix_Api_XmlRpc_Client::getInstance();
//            $client->call('Escorts.restore', array($escort_id));
//
//        }
//        $this->_response->setRedirect($this->view->getLink());
//    }
    /* Grigor Update */

	public function agencyDashboardAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$agency_id = $this->agency['id'];
		if ( $req->agency_id ) {
			$agency_id = $req->agency_id;
		}

		$page = 1;
		if ( $req->de_page ) {
			$page = intval($req->de_page);
		}

		if ( $page < 0 ) {
			$page = 1;
		}

		$per_page = 10;

		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$cache = Zend_Registry::get('cache');

		$cache_key =  'v2_agency_escorts_' . $this->agency['id'] . '_page_' . $page . $per_page;
		if ( ! $escorts = $cache->load($cache_key) ) {
			$escorts = $escorts = $client->call('Agencies.getNotActiveEscorts', array($agency_id, $page, $per_page));
			$cache->save($escorts, $cache_key, array(), 300);
		}

		$d_escorts = $escorts['result'];
		$d_escorts_count = $escorts['count'];

		$this->view->dash_escorts = $d_escorts;
		$this->view->dash_escorts_count = $d_escorts_count;
		$this->view->dash_agency_id = $agency_id;
		$this->view->dash_page = $page;
		$this->view->dash_per_page = $per_page;

	}

	public function upgradeAction()
	{
		$this->view->layout()->setLayout('private');

		$sess = new Zend_Session_Namespace('private');

		if ( $this->user && ! $this->user->isMember() ) {
			header('Location: /');
			die;
		}

		if ( ! $this->user && ! $sess->want_premium ) {
			header('Location: /' . Cubix_I18n::getLang() . '/private-v2/signin');
			die;
		}

		if ( $this->user ) {
			$this->view->user = $this->user;
		}
		elseif ( $sess->want_premium ) {
			$this->view->user = $this->user = $sess->want_premium;
		}

		if ( $this->_request->isPost() ) {
			$plan = (int) $this->_getParam('plan');
			switch ( $plan ) {
				case 2:
					$amount = 8995;
					break;
				default:
					$amount = 995;
			}
			$this->_redirect(Cubix_CGP::getPaymentUrl(array(
				'ref' => md5('m_' . $this->user->member_data['id']),
				'email' => $this->user->email,
				'amount' => $amount
			)));
		}
	}

	public function upgradeSuccessAction()
	{

	}

	public function upgradeFailedAction()
	{

	}

	public function changePasswordAction()
	{

		if ( $this->_request->isPost() ) {
			

			$validator = new Cubix_Validator();

			$form = new Cubix_Form_Data($this->_request);

			$form->setFields(array(
				'current_pass' => '',
				'new_pass' => '',
				'repeat_new_pass' => '',
			));

			$password = $this->_getParam('current_pass');

			$new_password = $this->_getParam('new_pass');
			$new_password_2 = $this->_getParam('repeat_new_pass');

			if ( $new_password == $this->user->username) {
				$validator->setError('new_pass', __('username_equal_password'));
			}else{
				if ( $new_password !== $new_password_2 ) {
					$validator->setError('new_pass', __('passwords_dont_match'));
					$validator->setError('repeat_new_pass', __('passwords_dont_match'));
				} else {

					if ( strlen($new_password) > 0 ) {
						try {
							$this->user->updatePassword($password, $new_password);
						}
						catch ( Exception $e ) {
							$validator->setError('current_pass', $e->getMessage());
						}
					} else {
						$validator->setError('current_pass', __('required_password'));
					}
				}
			}

			$status = $validator->getStatus();
			die(json_encode($status));
		} else {
			if ($this->_request->ajax) {
				$this->view->layout()->disableLayout();
			} else {
				$this->view->layout()->setLayout('main');
			}
		}

	}

	public function settingsAction()
	{
		$this->view->layout()->disableLayout();
		
		$this->view->langs = Cubix_Application::getLangs();
		$reviewModel = null;
		if ( $this->user->isAgency() ) {
		    $reviewModel = new Model_Reviews();
			$this->_helper->viewRenderer->setScriptAction('agency-settings');
		}
		else if ( $this->user->isEscort() ) {
		    $reviewModel = new Model_Reviews();
			$this->_helper->viewRenderer->setScriptAction('escort-settings');
		}
		else {
			$this->_helper->viewRenderer->setScriptAction('member-settings');
		}

		if ( $this->user->isMember() ) {
			$this->view->data = $this->user->getData(array(
				'recieve_newsletters', 'escorts_watched_type', 'lang','pm_is_blocked'
			));
		}
		else {
			$this->view->data = $this->user->getData(array(
				'email', 'recieve_newsletters', 'username', 'lang','pm_is_blocked'
			));
            $RevCom = $reviewModel->allowRevCom($this->user->id,'u');
            $this->view->data['allow_review'] = $RevCom['disabled_reviews'] ? $RevCom['disabled_reviews'] : 0;
            $this->view->data['allow_comment'] = $RevCom['disabled_comments'] ?$RevCom['disabled_comments'] : 0;
		}

		if (is_null($this->view->data['pm_is_blocked'])){
            $this->view->data['pm_is_blocked'] = 0;
        }

		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			$form = new Cubix_Form_Data($this->_request);


			if ( $this->user->isMember() ) {
				$save_newsletters = $this->_getParam('newsletters');
                $enable_pm = $this->_getParam('enable_pm');
				$escorts_watched_type = $this->_getParam('escorts_watched_type');
				$lang = $this->_getParam('prefered_lang');

				$form->setFields(array(
					'recieve_newsletters' => '',
					'escorts_watched_type' => '',
					'lang' => '',
				));

				$data = $form->getData();

				if(! is_null($save_newsletters)){
					$flag = (bool) $this->_getParam('newsletters');
                    $this->user->updateEnablePm($enable_pm);
					$this->user->updatePreferedLang($lang);
					$this->user->updateRecieveNewsletters($flag);
					$modelM = new Model_Members();
					$modelM->updateWatchedType($this->user->id, $escorts_watched_type);
					$user = Model_Users::getCurrent();
					$user->escorts_watched_type = $escorts_watched_type;
					Model_Users::setCurrent($user);
					$this->view->data = $this->user->getData(array(
						'recieve_newsletters', 'escorts_watched_type', 'lang','pm_is_blocked'
					));
				}
			}
			else {
				$save_newsletters = $this->_getParam('newsletters');
				$lang = $this->_getParam('prefered_lang');
                $enable_pm = $this->_getParam('enable_pm');
                $allow_review = $this->_getParam('allow_review');
                $allow_comment = $this->_getParam('allow_comment');
				$form->setFields(array(
					'recieve_newsletters' => '',
					'lang' => '',
				));

				$data = $form->getData();

				if(! is_null($save_newsletters)){
					$flag = (bool) $this->_getParam('newsletters');
                    $this->user->updateEnablePm($enable_pm);
                    if ($reviewModel){
                        $reviewModel->setAllowRevComByUserId($this->user->id,'r',$allow_review);
                        $reviewModel->setAllowRevComByUserId($this->user->id,'c',$allow_comment);
                        $this->view->data['allow_review'] = $allow_review;
                        $this->view->data['allow_comment'] = $allow_comment;
                    }
					$this->user->updatePreferedLang($lang);
					$this->user->updateRecieveNewsletters($flag);
					$this->view->data = $this->user->getData(array(
						'recieve_newsletters', 'lang','pm_is_blocked'
					));
				}
			}
			

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();
				$this->view->errors = $status['msgs'];
			}
		}
	}

	public function addReviewAction()
	{
		if ($this->user->user_type != 'member')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			if (isset($this->_request->escort_id) && intval($this->_request->escort_id) > 0)
			{
				$escort_id = $this->view->escort_id = intval($this->_request->escort_id);
				$model = new Model_EscortsV2();
				$esc = $model->getRevComById($escort_id);

				if ($esc->disabled_reviews)
				{
					$this->_redirect($this->view->getLink());
					return;
				}
				else
				{
					$lng = Cubix_I18n::getLang();
					list($showname, $cities) = Cubix_Api::getInstance()->call('getEscortDetailsForReviews', array($escort_id, $lng));

					if (!$showname)
					{
						$this->_forward('error', 'error', 'default', array('error_msg' => 'Wrong URL.'));
						return;
					}

					$this->view->showname = $showname;
					$this->view->cities = $cities;
					$this->view->username = $this->user->username;
				}
			}
			else
			{
				$this->_forward('error', 'error', 'default', array('error_msg' => 'Wrong URL.'));
				return;
			}

			if ($this->_request->isPost())
			{
				$validator = new Cubix_Validator();

				$req = $this->_request;

				if (intval($req->m_date) == 0)
					$validator->setError('m_date', 'Required');

				if ($req->meeting_city == 'other')
				{
					if (!$req->country_id)
						$validator->setError('country_id', 'Required');

					if (!$req->city_id)
						$validator->setError('city_id', 'Required');
				}

				if (!$req->meeting_place)
					$validator->setError('meeting_place', 'Required');

				if (!trim($req->duration))
					$validator->setError('duration', 'Required');
				elseif (!is_numeric($req->duration))
					$validator->setError('duration', $this->view->t('must_be_numeric'));

				if (!$req->duration_unit)
					$validator->setError('duration_unit', 'Required');

				if (!trim($req->price))
					$validator->setError('currency', 'Required');
				elseif (!is_numeric($req->price))
					$validator->setError('currency', $this->view->t('must_be_numeric'));

				/*if (!$req->currency)
					$validator->setError('currency', 'Required');*/

				if ($req->looks_rate == '-1')
					$validator->setError('looks_rate', 'Required');

				if ($req->services_rate == '-1')
					$validator->setError('services_rate', 'Required');

				if (!$req->s_kissing)
					$validator->setError('s_kissing', 'Required');

				if (!$req->s_blowjob)
					$validator->setError('s_blowjob', 'Required');

				if (!$req->s_cumshot)
					$validator->setError('s_cumshot', 'Required');

				if (!$req->s_69)
					$validator->setError('s_69', 'Required');

				if (!$req->s_anal)
					$validator->setError('s_anal', 'Required');

				if (!$req->s_sex)
					$validator->setError('s_sex', 'Required');

				if (!$req->s_multiple_times_sex)
					$validator->setError('s_multiple_times_sex', 'Required');

				if (!$req->s_breast)
					$validator->setError('s_breast', 'Required');

				if (!$req->s_attitude)
					$validator->setError('s_attitude', 'Required');

				if (!$req->s_conversation)
					$validator->setError('s_conversation', 'Required');

				if (!$req->s_availability)
					$validator->setError('s_availability', 'Required');

				if (!$req->s_photos)
					$validator->setError('s_photos', 'Required');

				if (!trim($req->t_user_info))
					$validator->setError('t_user_info', 'Required');

				if (!trim($req->t_meeting_date))
					$validator->setError('t_meeting_date', 'Required');

				if ($req->hrs == '-1')
					$validator->setError('hrs', 'Required');

				if ($req->min == '-1')
					$validator->setError('hrs', 'Required');

				if (!trim($req->t_meeting_duration))
					$validator->setError('t_meeting_duration', 'Required');

				if (!trim($req->t_meeting_place))
					$validator->setError('t_meeting_place', 'Required');

				if (!trim($req->t_comments))
					$validator->setError('t_comments', 'Required');

				//$captcha = Cubix_Captcha::verify($this->_request->recaptcha_response_field);

				/*$captcha_errors = array(
					'invalid-site-public-key' => 'We weren\'t able to verify the public key.',
					'invalid-site-private-key' => 'We weren\'t able to verify the private key.',
					'invalid-request-cookie' => 'The challenge parameter of the verify script was incorrect.',
					'incorrect-captcha-sol' => 'The CAPTCHA solution was incorrect.',
					'verify-params-incorrect' => 'The parameters to /verify were incorrect, make sure you are passing all the required parameters.',
					'invalid-referrer' => 'reCAPTCHA API keys are tied to a specific domain name for security reasons.',
					'recaptcha-not-reachable' => 'reCAPTCHA never returns this error code. A plugin should manually return this code in the unlikely event that it is unable to contact the reCAPTCHA verify server.'
				);*/

				//var_dump($captcha);

				/*if ( strlen($captcha) > 5 ) {
					$validator->setError('captcha', $this->view->t('wrong_secure_text')  $captcha_errors[$captcha]);
				}*/
				$captcha = trim($req->captcha);
				if ( ! strlen($captcha ) ) {
					$validator->setError('captcha', 'Required');
				}
				else {
					$session = new Zend_Session_Namespace('captcha');
					$orig_captcha = $session->captcha;

					if ( strtolower($captcha) != $orig_captcha ) {
						$validator->setError('captcha', 'Captcha is invalid');
					}
				}


				$t_user_info = trim($req->t_user_info);
				$t_meeting_date = trim($req->t_meeting_date);
				$t_meeting_time = $req->hrs . ':' . $req->min;
				$t_meeting_duration = trim($req->t_meeting_duration);
				$t_meeting_place = trim($req->t_meeting_place);

				if ($validator->isValid())
				{
					$ip = Cubix_Geoip::getIP();

					$req->setParam('ip', $ip);
					$req->setParam('user_id', $this->user->id);
					$req->setParam('m_date', intval($req->m_date));

					list($sms_unique, $phone_to, $mem_susp) = Cubix_Api::getInstance()->call('addReview', array($this->getRequest()->getParams()));
					Cubix_Api::getInstance()->call('SyncNotifier', array($escort_id, 29 ,array('escort_id' => $escort_id) ));
					if (strlen(trim($phone_to)) > 0 && !$mem_susp)
					{
						$text = $this->view->t('review_sms_template', array(
							'user_info' => $t_user_info,
							'showname' => $showname,
							'date' => $t_meeting_date,
							'meeting_place' => $t_meeting_place,
							'duration' => $t_meeting_duration,
							'time' => $t_meeting_time,
							'unique_number' => $sms_unique
						));

						$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());

						$phone_from = $originator;
						$sms_id = Cubix_Api::getInstance()->call('outbox', array($escort_id, $phone_to, $phone_from, $text, Cubix_Application::getId()));

						//sms info
						$config = Zend_Registry::get('system_config');
						$sms_config = $config['sms'];

						$SMS_USERKEY = $sms_config['userkey'];
						$SMS_PASSWORD = $sms_config['password'];
						$SMS_DeliveryNotificationURL = $sms_config['DeliveryNotificationURL'];
						$SMS_NonDeliveryNotificationURL = $sms_config['NonDeliveryNotificationURL'];
						$SMS_BufferedNotificationURL = $sms_config['BufferedNotificationURL'];

						$sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);

						$sms->setOriginator($originator);
						$sms->addRecipient($phone_to, (string)$sms_id);
						$sms->setBufferedNotificationURL($SMS_BufferedNotificationURL);
						$sms->setDeliveryNotificationURL($SMS_DeliveryNotificationURL);
						$sms->setNonDeliveryNotificationURL($SMS_NonDeliveryNotificationURL);

						$sms->setContent($text);

						if (1 != $result = $sms->sendSMS())
						{

						}
						else
						{
							Cubix_Api::getInstance()->call('smsSent', array($sms_unique));
						}
					}

					$this->_redirect($this->view->getLink('private-v2-reviews'));
				}
				else
				{
					$status = $validator->getStatus();
					$this->view->errors = $status['msgs'];
					$this->view->review = $req;
				}
			}
		}
	}

	public function reviewsAction()
	{
		if ($this->user->user_type != 'member')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			$lng = Cubix_I18n::getLang();
			$this->view->items = Cubix_Api::getInstance()->call('getReviewsForMember', array($this->user->id, $lng));
		}
	}

	public function escortReviewsAction()
	{
		if ($this->user->user_type != 'escort')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			$lng = Cubix_I18n::getLang();
			$results = Cubix_Api::getInstance()->call('getReviewsForEscort', array($this->user->id, $lng,$escort_id, $page, $per_page ));
			$this->view->items = $results['reviews'];
		}
	}

	public function addReviewCommentAction()
	{
		$this->view->layout()->disableLayout();

		if ($this->user->user_type != 'escort' && $this->user->user_type != 'agency')
			//$this->_redirect($this->view->getLink('private-v2'));
			die();
		else
		{
			if (isset($this->_request->review_id) && intval($this->_request->review_id) > 0)
			{
				$this->view->review_id = $rev_id = $this->_request->review_id;
				$user_id = $this->user->id;
				$do = false;

				if ($this->user->user_type == 'escort')
				{
					if (Cubix_Api::getInstance()->call('checkEscortReview', array($user_id, $rev_id)))
					{
						$do = true;
					}
					else
						die;
				}
				else
				{
					if (Cubix_Api::getInstance()->call('checkEscortReviewForAgency', array($user_id, $rev_id)))
					{
						$do = true;
					}
					else
						die;
				}

				if ($do)
				{
					if ($this->_request->isPost())
					{
						$validator = new Cubix_Validator();

						if (!$this->_request->comment)
							$validator->setError('comment', 'Required');

						if ($validator->isValid())
						{
							Cubix_Api::getInstance()->call('addReviewComment', array($rev_id, $this->_request->comment));
							$result['status'] = 'success';
							$result['signin'] = true;
							echo json_encode($result);
							die;
						}
						else
						{
							$status = $validator->getStatus();
							$this->view->errors = $status['msgs'];
							if ( ! is_null($this->_getParam('ajax')) ) {
								echo(json_encode($status));
								ob_flush();
								die;
							}
						}
					}
				}
			}
			else
				die;
		}
	}

	public function escortsReviewsAction()
	{
		if ($this->user->user_type != 'agency')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			$lng = Cubix_I18n::getLang();
			$results = Cubix_Api::getInstance()->call('getReviewsForAgencyEscorts', array($this->user->id, $lng, $page, $per_page ));
			$this->view->items = $results['reviews'];
			$this->view->escorts = $this->agency->getEscorts();
		}
	}

	public function memberProfileAction()
    {
        $this->view->layout()->disableLayout();
        $client = new Cubix_Api_XmlRpc_Client();
        $member_model = new Model_Members();
        if ( $this->user && $this->user->isMember() )
		{
			$user_data = $member_model->getMembersDataByUserId($this->user->id);
			if($user_data['langs']){
				$user_data['langs'] = unserialize($user_data['langs']);	
			}
			
			$this->view->avatar = $member_model->getLogoUrl($user_data['hash'], $user_data['ext'], $this->user['member_data']['id'], 'thumb_cropper_ed');
			
			$this->view->member_stats = $member_model->getMemberStats($this->user->id);
			$this->view->user_data = new Model_UserItem($user_data);
			$this->view->user = $this->user;
		}else {
            die("This page is available only for Members;");
        }
		
		if ( $this->_request->isPost() )
		{
			$data = array();

			$validator = new Cubix_Validator();
			
			$data = $this->_validateUserProfile($validator);
			
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();
				die(json_encode([
                    'success' => false,
                    'errors' => $status['msgs']
                ]));
			}

			try {
				$result = $client->call('Users.updateProfile', array($this->user->id, $data));
				//newsletter email log
				/*$emails = array(
					'old' => $this->user->email,
					'new' => $data['email']
				);
				Cubix_Api::getInstance()->call('addNewlstterEmailLog', array($this->user->id, $this->user->user_type, 'edit', $emails));*/
				//
			}
			catch (Exception $e) {
				var_dump($client->getLastResponse()); die;
			}

			if ( isset($result['error']) ) {
				print_r($result['error']); die;
			}
			else {
				die(json_encode(array('success' => 'true')));
			}
		}
	}

	private function _validateUserProfile(Cubix_Validator $validator)
	{
		$defines = Zend_Registry::get('defines');

		$req = $this->_request;

		$form = new Cubix_Form_Data($this->_request);

		$form->setFields(array(
			'gender' => 'int',
			'country_id' => 'int',
			'city_id' => 'int',
			'about_me' => 'notags|special',
			'show_gender' => 'int',
			'show_location' => 'int',
			'show_about_me' => 'int',
			'show_languages' => 'int',
			'show_interests' => 'int',
		));
		
		$data = $form->getData();

		if (! $data['gender'] ) {
			$validator->setError('gender', 'Gender is required !');
		}
		
		$langs = $this->_getParam('langs', array());

		if ( ! is_array($langs) ) $langs = array();

		$invalid_lang = false;
		foreach ( $langs as $lang_id => $val ) {
			$data['langs'][] = $val;

			if ( ! array_key_exists($val, $defines['language_options']) ) {
				$invalid_lang = true;
			}
		}

		if ( $invalid_lang ) {
			$validator->setError('language_error', Cubix_I18n::translate('sys_error_lang_is_invalid'));
		}

		$interests = $this->_getParam('interests');
		
		if ( ! is_array($interests) ) $interests = array();

		foreach ( $interests as $interest ) {
			$data['interests'][] = $interest;
		}

		if ( ! $data['interests'] ) {
			$data['interests'] = null;
		}else{
			$data['interests'] = serialize($data['interests']);
		}
		
		/*$email = $req->email;
		if ( ! strlen($email) ) {
			$validator->setError('email', 'Email is required !');
		}
		$data['email'] = $email;*/

		//Location
		
		/*if ( ! $data['country_id'] ) {
			$validator->setError('country_id', 'Country is required !');
		}*/
		
		if ( ! $data['country_id'] ) {
			$data['country_id'] = null;
		}
		
		if ( ! $data['city_id'] ) {
			$data['city_id'] = null;
		}

		if ( ! $data['langs'] ) {
			$data['langs'] = null;
		}else{
			$data['langs'] = serialize($data['langs']);
		}

		return $data;
	}

	
	public function memberPicAction()
	{
		$this->view->user = $this->user;
		$member_model = new Model_Members();
		$client = new Cubix_Api_XmlRpc_Client();
		
		$action = $this->_getParam('a');

		switch($action){
			case 'upload':
				try {
					$model = new Model_Escort_Photos();
					$response = $model->upload();

					if($response['error'] == 0 && $response['finish']){
						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($response['tmp_name'], 'members', $this->app_id, strtolower(@end(explode('.', $response['name']))),$this->user['member_data']['id']  );
						$image_url = $member_model->getLogoUrl($image['hash'],$image['ext'],$this->user['member_data']['id'], 'pa_mem_avatar');
						$new_id = $member_model->addLogo($this->user['member_data']['id'] , $image['hash'], $image['ext']);
						$response['photo_url'] = $image_url;
					}

					$model->showResponse($response);
					die;

				} catch ( Exception $e ) {
					echo json_encode(array(
						'finish' => 0,
						'error' => $e->getMessage()
					));
					die;
				}
				break;

			case 'delete':
				$member_model->removeLogo($this->user['member_data']['id']);
				die;
		}
		
		$this->view->escort = $escort;

	}
	
	public function memberFollowTop10Action()
	{
		$follow_model = new Model_Follow();
		$this->view->show_top10 = $follow_model->isTop10Public($this->user->member_data['id']);
		
		$this->memberFollowAction();
		$this->memberTop10Action();
	}
	
	public function memberFollowAction()
	{
		$this->view->layout()->disableLayout();
		$follow_model = new Model_Follow();
				
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'page' => 'int-nz',
			'showname' => 'notags|special'
		));
		
		$f_data = $form->getData();
		$f_data['page'] = $page = $f_data['page'] ? $f_data['page'] : 1;
		$f_data['per_page'] = $per_page = 20;
		$f_data['user_id'] = $this->user->id;	
		$f_data['lang'] = Cubix_I18n::getLang();
		$followed_users = $follow_model->getAll($f_data);
		$this->view->agency_model = new Model_AgencyItem();
		$this->view->followed_users = $followed_users['result'];
		$this->view->count = $followed_users['count'];
		$this->view->page = $page;
		$this->view->per_page = $per_page;
	}
	
	public function editFollowAction()
	{
		$this->view->layout()->disableLayout();
		$model = new Model_Follow();
		
		$req = $this->_request;
				
		if ( $req->isPost() ) {
			$data = new Cubix_Form_Data($this->getRequest());

			$fields = array(
				'follow_user_id' => 'int',
				'user_type' => '',
				'user_type_id' => 'int',
				'events' => 'arr-int',
				'cities' => 'arr-int',
				'short_comment' => 'notags|special',
				'follow_type' => 'int',
				'follow_email' => 'notags|special'
			);

			$data->setFields($fields);
			$post_data = $data->getData();
			
			if(!in_array($post_data['user_type'], array('escort',"agency"))){
				die;
			}
			
			$validator = new Cubix_Validator();
						
			if(count($post_data['events']) == 0){
				$validator->setError('user_type_id', Cubix_I18n::translate('follow_select_alert'));
			}
			
			if((in_array(FOLLOW_ESCORT_NEW_CITY,$post_data['events']) || in_array(FOLLOW_AGENCY_NEW_CITY , $post_data['events'] )) && count($post_data['cities']) == 0 ){
				$validator->setError('user_type_id', Cubix_I18n::translate('follow_select_city'));
			}
			
			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				echo(json_encode($result));	die;
			}
			else {
									
				$follow_data = array(
					'user_id' => $this->user->id,
					'type' => $post_data['user_type'],
					'type_id' => $post_data['user_type_id'],
					'short_comment' => $validator->removeEmoji($post_data['short_comment'])
				);
				
				$model->edit($post_data['follow_user_id'], $follow_data, $post_data['events'], $model->encode($post_data['cities']));
											
				echo json_encode($result);
				die;
			}
		}
		else{
			$model_cities = new Model_Cities();
			$follow_user_id = intval($req->follow_user_id);
			$id = $this->view->id = intval($req->id);
			$type = $this->view->type = $req->type;

			if ( ! $id || ! $type ) die;

			$follow_array = $model->getFollowEvents($follow_user_id);
			$this->view->follow_data = $follow_array['data'];
			$this->view->follow_events = $follow_array['events'];
			$this->view->follow_user_id = $follow_user_id;
			
			$city_event = ($type == "escort") ? FOLLOW_ESCORT_NEW_CITY : FOLLOW_AGENCY_NEW_CITY ;
			if(isset($follow_array['events'][$city_event]) && strlen($follow_array['events'][$city_event]['extra']) > 0){
				$cities_array = $model->decode($follow_array['events'][$city_event]['extra']);
				$cities = $model_cities->getByIds(implode(',', $cities_array));
			}
			else{
				$cities = array();
			}
			
			$this->view->cities = $cities;
			$contries_model = new Cubix_Geography_Countries();
			$this->view->countries = $contries_model->ajaxGetAll(false);
		}
	}
	
	public function removeFollowAction()
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'type_id' => 'int-nz',
			'type' => ''
		));
				
		$data = $form->getData();
		
		if(!in_array($data['type'], array('escort',"agency"))){
			die;
		}
		
		$follow_model = new Model_Follow();
		$follow_model->remove($this->user->id, $data['type_id'], $data['type']);
		$this->user->follow_list = $follow_model->getFollowList($this->user->id);
		Model_Users::setCurrent($this->user);
		die;
	}
	
	public function memberTop10Action()
	{
		$this->view->layout()->disableLayout();
		$follow_model = new Model_Follow();
		$this->view->top10 = $follow_model->getTop10($this->user->id);
		
	}
	
	public function sortTop10Action()
	{
		$ids = $this->_getParam('sort_id');
				
		if ( ! is_array($ids) || ! count($ids) ) {
			die;
		}
		
		$follow_model = new Model_Follow();
		$follow_model->sortTop10($this->user->id,$ids);
		die;
	}
	
	public function showTop10Action()
	{
		$show = intval($this->_getParam('show'));
		$follow_model = new Model_Follow();
		$follow_model->showTop10($this->user->member_data['id'], $show);
		die;
	}
	
	public function favoritesUpAction()
	{
		$this->view->layout()->disableLayout();
		
		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'id' => 'int-nz'
		);
		
		$form->setFields($fields);
		$data = $form->getData();
		
		$model = new Model_Follow();
		$model->favoritesMoveUp($this->user->id, $data['id']);

		/* recalculation rating */
		//$model->recalculateRating($data['escort_id']);
		/**/
						
		die(json_encode(array('success' => true)));
	}
	
	public function favoritesDownAction()
	{
		$this->view->layout()->disableLayout();
		
		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'id' => 'int-nz'
		);
		
		$form->setFields($fields);
		$data = $form->getData();
		
		$model = new Model_Follow();
		$model->favoritesMoveDown($this->user->id, $data['id']);

		/* recalculation rating */
		//$model->recalculateRating($data['escort_id']);
		/**/
						
		die(json_encode(array('success' => true)));
	}
	
	public function favoritesRemoveAction()
	{
		$this->view->layout()->disableLayout();
		
		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'id' => 'int-nz'
		);
		
		$form->setFields($fields);
		$data = $form->getData();
		
		$model = new Model_Follow();
		$model->removeFavorites($this->user->id, $data['id']);
		$model->reorderingTop10($this->user->id);
		
		die(json_encode(array('success' => true)));
	}

    protected function format_working_times()
    {
        foreach ($_POST['time_from'] as $key => $value) {
            $from = explode(':', $value);

            if(isset($from[0]) AND ! empty($from[0]))
                $_POST['time_from'][$key] = $from[0];

            if(isset($from[1]) AND ! empty($from[1]))
                $_POST['time_from_m'][$key] = $from[1];
        }

        foreach ($_POST['time_to'] as $key => $value) {
            $to = explode(':', $value);

            if(isset($to[0]) AND ! empty($to[0]))
                $_POST['time_to'][$key] = $to[0];

            if(isset($to[1]) AND ! empty($to[1]))
                $_POST['time_to_m'][$key] = $to[1];
        }

    }
	
	protected function _validateAgency(Cubix_Validator $validator, $agency_id)
	{
        $this->format_working_times();
        $req = $this->_request;

		$defines = Zend_Registry::get('defines');
		$client = new Cubix_Api_XmlRpc_Client();

		$data = array('working_times' => []);

		// Working times block

		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'available_24_7' => 'int-nz',
			'day_index' => 'arr-int',
			'time_from' => 'date',
			'time_from_m' => 'date',
			'time_to' => 'date',
			'time_to_m' => 'date'
		));

		$_data = $form->getData();

		for ( $i = 1; $i <= 7; $i++ ) {
			if ( isset($_data['day_index'][$i]) && isset($_data['time_from'][$i]) && isset($_data['time_to'][$i])  ) {

				$data['working_times'][] = array(
					'day_index' => $i,
					'time_from' => $_data['time_from'][$i],
					'time_from_m' => $_data['time_from_m'][$i],
					'time_to' => $_data['time_to'][$i],
					'time_to_m' => $_data['time_to_m'][$i],
				);
			}
		}


		$data['available_24_7'] = $_data['available_24_7'];

		// unset($data['day_index']);
		// unset($data['time_from']);
		// unset($data['time_from_m']);
		// unset($data['time_to']);
		// unset($data['time_to_m']);


		// $wds = (array) $req->work_days;
		// $work_times = array();
		// foreach ( $wds as $d => $nil ) {
		// 	if ( ! ($req->work_times_from[$d]) || ! ($req->work_times_to[$d]) ) {
		// 		$validator->setError('work_times_' . $d, 'Select time interval');
		// 	}

		// 	$work_times[$d] = array('from' => $req->work_times_from[$d], 'to' => $req->work_times_to[$d]);
		// }
		// $data['working_times'] = $work_times;

		// $available_24_7 = $req->available_24_7;
		// if ( ! $available_24_7 ) {
		// 	$available_24_7 = null;
		// }
		// $data['available_24_7'] = $available_24_7;

		// Contacts block


		$data['viber'] = $req->viber;
		$data['whatsapp'] = $req->whatsapp;

		$showname = preg_replace('/(\s)+/','$1', trim($req->agency_name));
		if ( ! strlen($showname) ) {
			$validator->setError('agency_name', __('showname_required'));
		}
		elseif ( ! preg_match('/^[^-][-_a-z0-9\s]+$/i', $showname) ) {
			$validator->setError('agency_name', 'Must begin with letter or number and must contain only alphanumeric characters');
		}
		$data['name'] = $showname;


		$contact_phone = trim($req->getParam('phone',''));
		$phone_prefix = $req->phone_prefix;
		$data['contact_phone_parsed'] = null;
        if ($phone_prefix  && !$contact_phone) {
            $validator->setError('phone', 'Please enter phone number');
        }
		if ($contact_phone ) {
			if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $contact_phone) ) {
				$validator->setError('phone', 'Invalid phone number');
			}
			elseif(preg_match("/^(\+|00)/", $contact_phone) ) {
				$validator->setError('phone', 'Please enter phone number without country calling code');
			}
			if (!$phone_prefix) {
				$validator->setError('phone_prefix', 'Country calling code Required');
			}
		}
		list($country_id,$phone_prefix,$ndd_prefix) = explode('-',$req->phone_prefix);
		unset($data['phone_prefix']);
		$data['phone_country_id'] = intval($country_id);
		$data['contact_phone_parsed'] = $data['phone'] = preg_replace('/[^0-9]+/', '', $contact_phone);
		$data['contact_phone_parsed'] = preg_replace('/^'.$ndd_prefix.'/', '', $data['contact_phone_parsed']);
		$data['contact_phone_parsed'] = ($data['contact_phone_parsed']) ? '00'.$phone_prefix.$data['contact_phone_parsed'] : null;

		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$results = array();
		if(!empty($data['contact_phone_parsed'])){
            $results = $client->call('Escorts.existsByPhone', array($data['contact_phone_parsed'], null, $agency_id));
        }


		$resCount = is_array($results) ? count($results) : 0;

		if($resCount > 0) {
			$validator->setError('phone', 'Phone already exists');
		}

		$phone_instructions = trim($req->getParam('phone_instr', ''));
		if ( ! $phone_instructions ) {

		}
		$data['phone_instructions'] = $phone_instructions;

		$contact_email = trim($req->getParam('email',''));
		if ($contact_email ) {
		    if(!$validator->validateEmailAddress($contact_email)){
                $validator->setError('email', 'Please enter valid E-mail address');
            }
		}
		$data['email'] = $contact_email;

		$contact_web = trim($req->getParam('web',''));
		if ($contact_web) {
		    if(!$validator->isValidURL($contact_web)){
                $validator->setError('web', 'Please enter valid Url');
            }
		}
		$data['web'] = $contact_web;

		//Location

		$country = $req->country_id;
		if ( ! $country ) {
			$country = null;
		}
		$data['country_id'] = $country;

		$region = $req->region_id;
		if ( ! $region ) {

		}
		$data['region_id'] = $region;

		$city = $req->city_id;
		if ( ! $city ) {
			$city = null;
		}
		$data['city_id'] = $city;

		$data['phone_to_all_escorts'] = $req->phone_to_all_escorts;

		if ( strlen($data['email']) && $client->call('Application.isDomainBlacklisted', array($data['email'])) ) {
			$validator->setError('email', 'Domain is blacklisted');
		}

		if ( strlen($data['web']) && $client->call('Application.isDomainBlacklisted', array($data['web'])) ) {
			$validator->setError('web', 'Domain is blacklisted');
		}


		// Languages block

		$langs = $this->_getParam('langs', array());
		if ( ! is_array($langs) ) $langs = array();

		$invalid_lang = false;
		foreach ( $langs as $lang_id => $level ) {
			$level = intval($level); if ( $level < 1 ) continue;
			$data['langs'][] = array('lang_id' => $lang_id, 'level' => $level);

			if ( ! array_key_exists($lang_id, $defines['language_options']) ) {
				$invalid_lang = true;
			}
		}

		if ( $invalid_lang ) {
			$validator->setError('language_error', Cubix_I18n::translate('sys_error_lang_is_invalid'));
		}


		// About block

		$security = new Cubix_Security();
		$i18n_data = Cubix_I18n::getValues($req->getParams(), 'about', '');
		foreach ( $i18n_data as $field => $value ) {
			$value = $security->clean_html($value);
			$i18n_data[$field] = $security->xss_clean($value);
			//$i18n_data[$field] = htmlspecialchars(strip_tags(trim($value)));
		}
		$data = array_merge( $data,$i18n_data);

		$data['about_services'] = $security->clean_html($req->about_services);
		$data['about_areas_covered'] = $security->clean_html($req->about_areas_covered);

		//about us
		/*$about_me_exists = false;
		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			if(strlen($data['about_' . $lng]) > 0){
				$about_me_exists = true;
				break;
			}
		}
		if ( !$about_me_exists ) {
			$validator->setError('about_me_text', 'About us text required');
		}*/


		return $data;
	}

	public function agencyProfileAction()
	{
		$this->view->layout()->disableLayout();

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isAgency() )
		{
			$agency_data_all = $client->call('Agencies.getByUserIdED', array($this->user->id));
			$agency_data = $agency_data_all['agency_data'];
			$agency_data['working_times_ed'] = $agency_data_all['working_times'];
			$agency_data['languages_ed'] = $agency_data_all['languages'];

			$agency_id = $agency_data['id'];
			$agency_slug = $agency_data['slug'];
			$phone_prefix = $agency_data['phone_country_id'];
			$agency_data = array_merge($agency_data, $client->call('Agencies.getInfo', array($agency_slug, $agency_id)));
			$countyModel = new Model_Countries();

			$this->view->countries = $countyModel->getPhoneCountries();
			$this->view->sales_persons = $client->call('Users.getSalesPersons');
			$this->view->phone_prefix_id = $phone_prefix;
 
		} else {
            die("This page is available only for Agency;");
        }

        $agencyModel = new Model_AgencyItem($agency_data);

		$this->view->user = $this->user;
		$this->view->agency_data = $agencyModel;
		$this->view->agency = $this->user->getAgency();
        $this->view->avatar = $agencyModel->getLogoUrlV2($agencyModel->logo_hash,$agencyModel->logo_ext);

		$phone_prfixes = $countyModel->getPhonePrefixs();
		if($agency_data['phone']){
			if(preg_match('/^(\+|00)/',trim($agency_data['phone'])))
			{
				$phone_prefix_id = NULL;
				$phone_number = preg_replace('/^(\+|00)/', '',trim($agency_data['phone']));
				foreach($phone_prfixes as $prefix)
				{
					if(preg_match('/^('.$prefix->phone_prefix.')/',$phone_number))
					{
						$phone_prefix_id = $prefix->id;
						$agency_data['phone'] = preg_replace('/^('.$prefix->phone_prefix.')/', '',$phone_number);
						BREAK;
					}

				}
				$this->view->phone_prefix_id = $phone_prefix_id;
			}
		}

		if ( $this->_request->isPost() ) {

			$data = array();

			$validator = new Cubix_Validator();
			$data = $this->_validateAgency($validator, $agency_id);

			/* // Assign escort to a sales person
			$client->call('Users.assignAgency', array($data['sales_user_id'], $agency_id));
			unset($data['sales_user_id']); */

			$phone_to_all_escorts = $data['phone_to_all_escorts'];
			unset($data['phone_to_all_escorts']);

			$data['application_id'] = $agency_data['application_id'];
			$this->view->phone_prefix_id = $data['phone_country_id'];
			$this->view->agency_data = new Model_AgencyItem($data);

			$added_escorts_count = Model_AgencyItem::hasAddedAnyEscort($agency_id);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				$result = $validator->getStatus();

                die(json_encode([
                    'status' => $result['status'],
                    'errors' => $result['msgs']
                ]));
			} else {

				try {
					// var_dump($data); die();
					$result = $client->call('Agencies.updateAgencyProfile', array($agency_id, $data));
					if($phone_to_all_escorts == 1 ){

						$agency_escorts = $client->call('Agencies.getAllEscorts', array($agency_id));
						$escort_data = array(
							'phone_country_id'	   => $data['phone_country_id'],
							'phone_number'         => $data['phone'],
							'contact_phone_parsed' => $data['contact_phone_parsed'],
							'phone_exists'         => $data['contact_phone_parsed']
						);

						foreach($agency_escorts as $escort){
							Cubix_Api::getInstance()->call('updateEscortProfileSimple', array( $escort['id'], $escort_data) );
						}
					}

					if ( $this->_request->email_to_all_escorts == 1 ) {
						$agency_escorts = $client->call('Agencies.getAllEscorts', array($agency_id));
						$escort_data = array(
							'email'	   => $data['email'],
						);

						foreach($agency_escorts as $escort){
							Cubix_Api::getInstance()->call('updateEscortProfileSimple', array( $escort['id'], $escort_data) );
						}
					}

					if ( $this->_request->website_to_all_escorts == 1 ) {
						$agency_escorts = $client->call('Agencies.getAllEscorts', array($agency_id));
						$escort_data = array(
							'website'	   => $data['web'],
						);

						foreach($agency_escorts as $escort){
							Cubix_Api::getInstance()->call('updateEscortProfileSimple', array( $escort['id'], $escort_data) );
						}
					}
				}
				catch (Exception $e) {
					var_dump($client->getLastResponse()); die;
				}

				if ( isset($result['error']) ) {
					print_r($result['error']); die;
				}
				else {
					if ( $result !== false ) {
						$this->view->saved = true;
						echo nl2br(print_r($result, true));
					}
				}

				$result = $validator->getStatus();
				die(json_encode([
				    'success' => ($result['status'] == 'success'),
                    'show_no_provider_popup' => empty($added_escorts_count),
                ]));
			}
		}

	}

	public function agencyLogoAction()
	{

		$this->view->layout()->disableLayout();

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isAgency() )
		{
			$agency = $this->user->getAgency();
			$this->view->agency = $agency;

			/*if ( $this->_request->isPost() )
			{*/

				if ( $this->_request->a == 'upload' ) {
					try {
						$model = new Model_Escort_Photos();
						$response = $model->upload();

						if($response['error'] == 0 && $response['finish']){
							// Save on remote storage
							$images = new Cubix_Images();
							$image = $images->save($response['tmp_name'], 'agencies', $agency->application_id, strtolower(@end(explode('.', $response['name']))));
							
							$agency_data['logo_hash'] = $image['hash'];
							$agency_data['logo_ext'] = $image['ext'];

							$result = $client->call('Agencies.updateAgencyLogo', array($agency->id, $agency_data));

							$agency = $this->user->getAgency();
							$this->view->agency = $agency;

							$logo_url = $agency->getLogoUrl(true, 'thumb_cropper_agency');
							$response['photo_url'] = $logo_url;
						}

						$model->showResponse($response);
						die;
					} catch (Exception $e) {
						echo json_encode(array(
								'finish' => 0,
								'error' => $e->getMessage()
							));
						die;
						//$this->view->uploadError = $e->getMessage();
					}

				} elseif ( 'set-adj' == $this->_request->a ) {

					$agency_id = intval($this->_getParam('agency'));

					$agency_data = $client->call('Agencies.get', array($agency_id));

					/*if ( ! in_array($photo_id, $photo_ids) ) {
						die(json_encode(array('error' => 'An error occured')));
					}

					$photo = new Model_Escort_PhotoItem(array(
						'id' => $photo_id
					));*/

					try {
						$hash = $agency_data['logo_hash'];
						$result = array(
							'x' => intval($this->_getParam('x')),
							'y' => intval($this->_getParam('y')),
							'px' => floatval($this->_getParam('px')),
							'py' => floatval($this->_getParam('py'))
						);

						$client->call('Agencies.updateAgencyLogo', array($agency_id, array('args' => serialize($result) ) ));
						//$photo->setCropArgs($result);

						// Crop All images
						$size_map = array(
							'thumb_ed_agency' => array('width' => 233, 'height' => 283),
							'thumb_ed_agency_xl' => array('width' => 312, 'height' => 404),
							'thumb_ed_agency_list' => array('width' => 226, 'height' => 226),
						);
						$conf = Zend_Registry::get('images_config');

						$px = $result['x'] / (float)$size_map['thumb_ed_agency']['width'];
						$py = $result['y'] / (float)$size_map['thumb_ed_agency']['height'];

						get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache_agency&app=" . Cubix_Application::getById()->host . "&aid=" . $agency_id . "&hash=" . $hash);

						foreach($size_map as $size => $sm) {
							get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/agencies/" . $hash . "_" . $size . ".jpg?args=" . ($sm['width'] * $px) . ":" . ($sm['height'] * $py));
						}
					}
					catch ( Exception $e ) {
						die(json_encode(array('error' => 'An error occured')));
					}

					die(json_encode(array('success' => true)));
				}
				elseif ( 'delete' == $this->_request->a ) {
					$agency_id = intval($agency->id);
					$client->call('Agencies.updateAgencyLogo', array($agency_id, array('args' => '', 'logo_hash' => '', 'logo_ext' => '')));
				}
				/*else if ( $this->_request->d ) {
					$agency_data['logo_hash'] = null;
					$agency_data['logo_ext'] = null;
					try {
						$client->call('Agencies.updateAgencyLogo', array($agency->id, $agency_data));

						$agency = $this->user->getAgency();
						$this->view->agency = $agency;
					} catch (Exception $e) {
						$this->view->uploadError = $e->getMessage();
					}
				}*/
			//}
		}
		die;
	}

	public function toursAction()
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('no_tidy', true);
		$this->view->is_agency = $this->user->isAgency();

		$escort_id = $this->view->escort_id = intval($this->_getParam('escort'));
		if ( $escort_id > 0  && !is_null($this->_getParam('info')) ) {
			$model = new Model_Escorts();
			$escort = $model->getById($escort_id);

			if ( ! $escort ) {
				die(json_encode(null));
			}
			else {
				die(json_encode(array(
					'photo' => $escort->getMainPhoto()->getUrl('agency_p100', true),
					'link' => $this->view->getLink('profile', array('showname' => $escort->showname, 'escort_id' => $escort_id))
				)));
			}
		}

		if ( $this->user->isAgency() ) {
			//$this->_helper->viewRenderer->setScriptAction('agency-tours');
			$this->view->escorts = $this->agency->getEscorts();
			$this->view->agency_escorts = Model_Escorts::getAgencyEscorts($this->agency->id);
		}
		else {
			$this->escort = $this->view->escort = $this->user->getEscort();
		}

		$this->_helper->viewRenderer->setScriptAction('escort-tours');

		$this->view->disable_tour_history = $this->client->call('getTourHistory', array($this->user->id));
	}

	public function toggleTourHistoryAction()
	{
		$this->view->layout()->disableLayout();
		$o_t_v = intval($this->_request->o_t_v);

		$o_t_v == 1 ? $o_t_v = 0 : $o_t_v = 1;

		$this->client->call('updateTourHistory', array($this->user->id, $o_t_v));
	}

	public function ajaxToursAction($data = array())
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('no_tidy', true);
        $this->_helper->viewRenderer->setScriptAction('ajax-tours');

		$is_old_tour = intval($this->_getParam('old_tour')) ? true : false;
		$page = (isset($this->_request->page) && intval($this->_request->page) > 0) ? intval($this->_request->page) : 1;
		$per_page = 20;
		$this->view->is_agency = $this->user->isAgency();
		$this->view->tours = array();
		$this->view->page = $page;
		$this->view->per_page = $per_page;
        $getall = false;
        $agency_id = 0;

		if ( $this->user->isAgency() ) {

			$this->view->agency_escorts = Model_Escorts::getAgencyEscorts($this->agency->id);


			$escort_id = intval($this->_getParam('escort_id'));
            /* Update Grigor */
            $agency =  $this->user->getAgency();
            $agency_id = $agency->id;

            if($escort_id == -1){
                $getall = true;
            }
      		if ( ! $this->agency->hasEscort($escort_id) && !$getall) return;
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}

		if ( is_null($escort_id) ) {
			return;
		}
		$this->view->sel_escort_id = $escort_id;
		/* Update Grigor */
		$tours = $this->client->call('getEscortTours', array($escort_id, Cubix_I18n::getLang(), $agency_id,$page, $per_page, $is_old_tour));
        $this->view->tours = $tours[0];
		$this->view->count = $tours[1];
//        if($is_old_tour){
//		    $this->_helper->viewRenderer->setScriptAction('ajax-old-tours');
//	    }

        /* Update Grigor */

    }
	public function ajaxReviewsAction()
	{
		$this->view->layout()->disableLayout();
		$lng = Cubix_I18n::getLang();
		$escord_id = intval($this->_request->getParam('escort_id'));
		if ($escord_id == -1){
			$results = Cubix_Api::getInstance()->call('getReviewsForAgencyEscorts', array($this->user->id, $lng, $page, $per_page ));
			$this->view->items = $results['reviews'];
		}
		else{

		$results = Cubix_Api::getInstance()->call('getReviewsForEscort', array($this->user->id, $lng,$escort_id, $page, $per_page ));
		$this->view->items = $results['reviews'];
		}
	}

	/*public function editTourAction()
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('no_tidy', true);

		$tour = null;
		if ( ($tour_id = intval($this->_getParam('id'))) > 0 ) {
			$tour = $this->client->call('getEscortTour', array($tour_id, $this->_request->lang_id));
		}
		$this->view->tour = $tour;

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( $tour ) $escort_id = $tour['escort_id'];
			if ( ! $this->agency->hasEscort($escort_id) ) {
				die('Permission denied!');
			}
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
			if ( $tour && $escort_id != $tour['escort_id'] ) {
				die('Permission denied!');
			}
		}

		if ( ! $escort_id ) die;


		$model = new Model_Escorts();
		$this->view->escort = $escort = $model->getById($escort_id);

		if ( $this->_request->isPost() ) {
			$form = new Cubix_Form_Data($this->_request);
			$form->setFields(array(
				'id' => 'int-nz',
				'country_id' => 'int-nz',
				'city_id' => 'int-nz',
				'date_from' => 'int',
				'date_to' => 'int',
				'phone' => '',
				'email' => ''
			));
			$i_data = $form->getData();

			$validator = new Cubix_Validator();

			// <editor-fold defaultstate="collapsed" desc="Tour Dates Overlap Validation">
			$tours = $this->client->call('getEscortTours', array($escort_id, $this->_request->lang_id));
			$data['tours_tmp'] = $tours[0];

			if ( count($data['tours_tmp']) ) {
				foreach ( $data['tours_tmp'] as $t_k => $tr ) {
					unset($data['tours_tmp'][$t_k]['country']);
					unset($data['tours_tmp'][$t_k]['city']);
				}
			}
			$data['tours_tmp'][] = $i_data;

			$today = strtotime(date('d-m-Y', time()));

			foreach ( $data['tours_tmp'] as $i1 => $tourr ) {

				//if ( ! isset($tourr['showname']) ) continue;

				//list($id, $country_id, $city_id, $date_from, $date_to, $phone, $email) = $tour;
				$id = $tourr['id'];
				$country_id = $tourr['country_id'];
				$city_id = $tourr['city_id'];
				$date_from = $tourr['date_from'];
				$date_to = $tourr['date_to'];
				$phone = $tourr['phone'];

				//if ( $id == $i_data['id'] ) continue;

				if ( $date_to < $today ) {
					$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
					break;
				}

				if ( $date_from > $date_to ) {
					$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
					break;
				}

				$found = false;

				foreach ( $data['tours_tmp'] as $i2 => $t1 ) {
					if ( $i1 == $i2 ) continue;

					//if ( ! isset($t1['showname']) ) continue;

					//list($nil, $nil, $d_f, $d_t, $phone) = $t1;
					$d_f = $t1['date_from'];
					$d_t = $t1['date_to'];

					if ( ($d_f >= $date_from && $d_f < $date_to) || ($d_t > $date_from && $d_t <= $date_to) ) {
						$found = true;

						$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
						break;
					}
				}

				if ( $found ) {
					break;
				}
			}
			unset($data['tours_tmp']);

			// </editor-fold>

			// <editor-fold defaultstate="collapsed" desc="Other Fields Validation">
			$model = new Cubix_Geography_Countries();
			if ( ! $model->exists($i_data['country_id']) ) {
				$validator->setError('country_id', 'Invalid country');
			}

			if ( ! $model->hasCity($i_data['country_id'], $i_data['city_id']) ) {
				$validator->setError('city_id', 'Invalid city');
			}

			$date_from = strtotime(date('d-m-Y', $i_data['date_from']));
			$date_to = strtotime(date('d-m-Y', $i_data['date_to']));

			$today = strtotime(date('d-m-Y', time()));
			if ( ! $i_data['date_from'] || ! $i_data['date_to'] ) {
				$validator->setError('date_from', 'Date required');
			}
			elseif($date_from > $date_to){
				$validator->setError('date_from', 'Invalid date range');
			}
			elseif($date_to <  $today){
				$validator->setError('date_from', 'Invalid date range');
			}

			if ( $i_data['email'] ) {
				if ( ! Cubix_Validator::validateEmailAddress($i_data['email']) ) {
					$validator->setError('email', 'Invalid email address');
				}

			}
			// </editor-fold>

			if ( $validator->isValid() ) {
				if ( ! $tour ) {
					$this->client->call('addEscortTour', array($escort_id, $i_data));
				}
				else {
					$this->client->call('updateEscortTour', array($escort_id, $tour_id, $i_data));
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}*/

	public function editTourAction()
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('no_tidy', true);

		$tour = null;
		if ( ($tour_id = intval($this->_getParam('id'))) > 0 ) {
			$tour = $this->client->call('getEscortTourV2', array($tour_id, $this->_request->lang_id));
		}
		$this->view->tour = $tour;

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_request->escort_id);
			if ( $tour ) $escort_id = $tour['escort_id'];

			if ( ! $this->agency->hasEscort($escort_id) ) {
				die('Permission denied!');
			}
			$this->view->is_agency = true;
			$this->view->agency_escorts = Model_Escorts::getAgencyEscorts($this->agency->id);
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
			if ( $tour && $escort_id != $tour['escort_id'] ) {
				die('Permission denied!');
			}
		}

		if ( ! $escort_id ) die;


		$model = new Model_Escorts();
		$this->view->escort = $escort = $model->getById($escort_id);

		if ( $this->_request->isPost() ) {
			$form = new Cubix_Form_Data($this->_request);
			$form->setFields(array(
				'id' => 'int-nz',
				'country_id' => 'int-nz',
				'city_id' => 'int-nz',
				'date_from' => '',
				'date_to' => '',
				'is_time_on' => 'int',
				'minutes_from' => '',
				'minutes_to' => '',
				'diff_time' => '',
				'phone' => '',
				'email' => '',
			));
			$i_data = $form->getData();

			$validator = new Cubix_Validator();

			$model = new Cubix_Geography_Countries();
			if ( ! $model->exists($i_data['country_id']) ) {
				$validator->setError('country_id', Cubix_I18n::translate('invalid_country'));
			}

			if ( ! $model->hasCity($i_data['country_id'], $i_data['city_id']) ) {
				$validator->setError('city_id', Cubix_I18n::translate('invalid_city'));
			}

			if ( $i_data['email'] ) {
				if ( ! Cubix_Validator::validateEmailAddress($i_data['email']) ) {
					$validator->setError('email', Cubix_I18n::translate('invalid_email'));
				}
				/*elseif ( Cubix_Application::isDomainBlacklisted($i_data['email']) ) {
					$validator->setError('email', 'Domain is blacklisted');
				}*/
			}

			if ( ! $i_data['date_from'] || ! $i_data['date_to'] ) {
				$validator->setError('date_from', Cubix_I18n::translate('date_required'));
			}
			if ( (! isset($i_data['minutes_from']) && isset($i_data['minutes_to'])) || ( isset($i_data['minutes_from']) && !isset($i_data['minutes_to'])) ) {
				$validator->setError('minutes_from', Cubix_I18n::translate('time_required'));
			}
			if ( $validator->isValid() ) {
				$tours = $this->client->call('getEscortToursV2', array($escort_id, $this->_request->lang_id));
				$data['tours_tmp'] = $tours[0];

				$i_data['diff_time'] = intval($i_data['diff_time']);
				$date_to = $i_data['date_to'] = strtotime($i_data['date_to']); // = strtotime(date('d-m-Y H:i:s', ($i_data['date_to'] / 1000)));
				$date_from = $i_data['date_from'] = strtotime($i_data['date_from']); //= strtotime(date('d-m-Y H:i:s', ($i_data['date_from'] / 1000)));

				$i_data['minutes_from'] = (is_null($i_data['minutes_from']) || $i_data['minutes_from'] == "" )  ? null : intval($i_data['minutes_from']);
				$i_data['minutes_to']   = (is_null($i_data['minutes_to']) || $i_data['minutes_to'] == "" )  ? null : intval($i_data['minutes_to']);

				if($i_data['is_time_on']){
					$date_to = $i_data['date_to'] + $i_data['minutes_to'] * 60;
					$date_from = $i_data['date_from'] + $i_data['minutes_from'] * 60;
				}
				else{
					$date_to = $i_data['date_to']; // += 60 * 60 * 24 - 1; // 23:59:59
				}

				unset($i_data['is_time_on']);
				unset($i_data['diff_time']);
				if ( count($data['tours_tmp']) ) {
					foreach ( $data['tours_tmp'] as $t_k => $tr ) {
						unset($data['tours_tmp'][$t_k]['country']);
						unset($data['tours_tmp'][$t_k]['city']);
						$data['tours_tmp'][$t_k]['date_from'] = is_null($data['tours_tmp'][$t_k]['minutes_from']) ? $data['tours_tmp'][$t_k]['date_from'] : $data['tours_tmp'][$t_k]['date_from'] + $data['tours_tmp'][$t_k]['minutes_from'] * 60;
						$data['tours_tmp'][$t_k]['date_to'] = is_null($data['tours_tmp'][$t_k]['minutes_to']) ? $data['tours_tmp'][$t_k]['date_to'] : $data['tours_tmp'][$t_k]['date_to'] + $data['tours_tmp'][$t_k]['minutes_to'] * 60;
					}
				}

				$today = strtotime(date("Y-m-d 00:00:00"));

				if ( $date_to < $today || $date_from < $today ) {
					$validator->setError('date_from', Cubix_I18n::translate('sys_error_tour_date_invalid_interval'));
				}
				elseif($date_from >= $date_to ){
					$validator->setError('date_from', 'Some date intervals are invalid or overlapped');
				}
				else{

					foreach ( $data['tours_tmp'] as $t ) {
						if ($t['id'] == $i_data['id']) continue;
						if ( ( $date_from >= $t['date_from'] && $date_from < $t['date_to']) ||
							 ( $date_to > $t['date_from'] && $date_to <= $t['date_to']) ||
							 ( $date_from < $t['date_from'] && $date_to > $t['date_to']) )
						{
							$validator->setError('date_from', Cubix_I18n::translate('sys_error_tour_date_invalid_interval'));
							break;
						}
					}
				}
			}
			unset($data['tours_tmp']);
			if ( $validator->isValid() ) {
				if ( ! $tour ) {
					$this->client->call('addEscortTour', array($escort_id, $i_data));
				}
				else {
                    $this->client->call('updateEscortTour', array($escort_id, $tour_id, $i_data));
				}
			}
			$response = $validator->getStatus();
			$response['errors'] = $response['msgs'];

			$response['additional'] = [
			    'today' => $today,
                'i_data' => $i_data,
                'before' => $before
            ];

			die(json_encode($response));
		}
	}

	public function removeToursAction()
	{
		$tours = (array) $this->_getParam('tours');
		foreach ( $tours as $tour_id ) {
			$tour_id = intval($tour_id);

			$tour = $this->client->call('getEscortTour', array($tour_id, $this->_request->lang_id));
			if ( ! $tour ) continue;

			if ( $this->user->isAgency() ) {
				if ( ! $this->agency->hasEscort($tour['escort_id']) ) {
					continue; // wrong owner
				}
			}
			elseif ( $this->user->isEscort() ) {
				if ( $this->escort->getId() != $tour['escort_id'] ) {
					continue; // wrong owner
				}
			}

			$this->client->call('removeEscortTour', array($tour['escort_id'], $tour['id']));
		}

		die;
	}

	public function ajaxToursAddAction()
	{
		$this->_request->setParam('no_tidy', true);

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( ! $this->agency->hasEscort($escort_id) ) die;
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}

		if ( is_null($escort_id) ) die;

		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'id' => 'int-nz',
			'country_id' => 'int-nz',
			'city_id' => 'int-nz',
			'date_from' => 'int',
			'date_to' => 'int',
			'phone' => '',
			'email' => ''
		));
		$i_data = $form->getData();

		$validator = new Cubix_Validator();

		$tours = $this->client->call('getEscortTours', array($escort_id, $this->_request->lang_id));
		$data['tours_tmp'] = $tours[0];
		if ( count($data['tours_tmp']) ) {
			foreach ( $data['tours_tmp'] as $t_k => $tr ) {
				unset($data['tours_tmp'][$t_k]['country']);
				unset($data['tours_tmp'][$t_k]['city']);
			}
		}
		$data['tours_tmp'][] = $i_data;

		$today = strtotime(date('d-m-Y', time()));

		foreach ( $data['tours_tmp'] as $i1 => $tour ) {
			//list($id, $country_id, $city_id, $date_from, $date_to, $phone, $email) = $tour;
			$id = $tour['id'];
			$country_id = $tour['country_id'];
			$city_id = $tour['city_id'];
			$date_from = $tour['date_from'];
			$date_to = $tour['date_to'];
			$phone = $tour['phone'];

			if ( $id == $i_data['id'] ) continue;

			if ( $date_to < $today ) {
				$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}

			if ( $date_from > $date_to ) {
				$validator->setError('tours', 'Some date intervals are invalid or overlapped');
				break;
			}

			$found = false;

			foreach ( $data['tours_tmp'] as $i2 => $t1 ) {
				if ( $i1 == $i2 ) continue;

				//list($nil, $nil, $d_f, $d_t, $phone) = $t1;
				$d_f = $t1['date_from'];
				$d_t = $t1['date_to'];

				if ( ($d_f >= $date_from && $d_f < $date_to) || ($d_t > $date_from && $d_t <= $date_to) ) {
					$found = true;

					$validator->setError('tours', 'Some date intervals are invalid or overlapped');
					break;
				}
			}

			if ( $found ) {
				break;
			}
		}
		unset($data['tours_tmp']);

		$model = new Cubix_Geography_Countries();
		if ( ! $model->exists($i_data['country_id']) ) {
			$validator->setError('country', 'Invalid country');
		}

		if ( ! $model->hasCity($i_data['country_id'], $i_data['city_id']) ) {
			$validator->setError('city', 'Invalid city');
		}

		if ( ! $i_data['date_from'] || ! $i_data['date_to'] ) {
			$validator->setError('date', 'Invalid date range');
		}

		if ( $i_data['email'] ) {
			if ( ! Cubix_Validator::validateEmailAddress($i_data['email']) ) {
				$validator->setError('email', 'Invalid email address');
			}/*
			else if (Cubix_Application::isDomainBlacklisted($i_data['email']) ) {
				$validator->setError('email', 'Domain is blacklisted');
			}*/
		}

		if ( $validator->isValid() ) {
			$this->client->call('addEscortTour', array($escort_id, $i_data));
		}
		else {
			die(json_encode($validator->getStatus()));
		}

		$this->ajaxToursAction(array('status' => 'success'));
	}

	public function ajaxToursRemoveAction()
	{
		$this->_request->setParam('no_tidy', true);

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( ! $this->agency->hasEscort($escort_id) ) die;
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}

		$tour_id = intval($this->_getParam('id'));

		$this->client->call('removeEscortTour', array($escort_id, $tour_id));

		$this->ajaxToursAction(array('status' => 'success'));
	}

	private function _loadPhotos()
	{
		$public_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, false));
		$private_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, true));

		return $this->view->photos = array_merge($public_photos, $private_photos);
	}


	public function photosAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->user = $this->user;

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort'));

			if ( 1 > $escort_id ) die('no escort id specified');

			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die('no escorts');
			}

			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}


		$photos = $this->_loadPhotos();

		$photo_ids = array();
		foreach ( $photos as $photo ) {
			$photo_ids[] = intval($photo['id']);
		}

		$action = $this->_getParam('a');

		$photos_min_count = Cubix_Application::getById(Cubix_Application::getId())->min_photos;

		$this->view->is_private = $is_private = (bool) intval($this->_getParam('is_private'));
		
		if ( ! is_null($action) ) {
			if ( 'set-main' == $action ) {
				try {
					$ids = $this->_getParam('photo_id');
                    if ( ! is_array($ids) || ! count($ids) ) {
						return $this->view->actionError = 'Please select at least one photo';
					}
					$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED;
					if(count($ids) == 1){
						$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED_ONE;
					}


                    $esc_photo_id = $client->call('Escorts.getApprovedPhoto', array($escort_id, implode(',',$ids)));

					$esc_private_photos = $client->call('Escorts.getEscortPhotosList', array($escort_id, true));

					foreach ($esc_private_photos as $i) {
					    if(in_array($i['id'], $ids )) {
                            throw new Exception('Sorry you can\'t set private photo as main');
                        }

                    }


                    if(!$esc_photo_id){
                        throw new Exception('Sorry you can\'t set not approved photo as main');

                    }




					$photo_id = reset($ids);
					if ( ! in_array($photo_id, $photo_ids) ) {
						return $this->view->actionError = 'Invalid id of one of the photos';
					}

					$photo = new Model_Escort_PhotoItem(array('id' => $photo_id ,'escort_id' => $escort_id));
					$photo->setRotatePics($ids);
					$result = $photo->setMain();
					$client->call('Escorts.setPhotoRotateType', array($escort_id, $rotate_type));
					$escort->photo_rotate_type = $rotate_type;

					//$this->_loadPhotos();

					echo json_encode(array(
						'status' => 1,
						'name' => "success"
					));
					die;

				} catch ( Exception $e ) {
					echo json_encode(array(
						'status' => 0,
						'error' => $e->getMessage()
					));
					die;
				}
			}
			elseif ( 'delete' == $action ) {

				try {
					$ids = $this->_getParam('photo_id');

					if ( ! is_array($ids) || ! count($ids) ) {
						return $this->view->actionError = 'Please select at least one photo';
					}

					foreach ( $ids as $id ) {
						if ( ! in_array($id, $photo_ids) ) {
							return $this->view->actionError = 'Invalid id of one of the photos';
						}
					}

					$photo = new Model_Escort_Photos();
					$result = $photo->remove($ids);

					echo json_encode(array(
						'status' => 1,
						'name' => "success",
						'main_photo_id' => $result
					));
					die;

				} catch ( Exception $e ) {
					echo json_encode(array(
						'status' => 0,
						'error' => $e->getMessage()
					));
					die;
				}
			}
			elseif ( 'upload' == $action ) {
				try {
					$model = new Model_Escort_Photos();
					$photo_count = $model->getEscortPhotoCount($escort_id);

					if ( $photo_count >= Model_Escort_Photos::MAX_PHOTOS_COUNT ){
						throw new Exception(Cubix_I18n::translate('sys_error_upload_img_count_too_much', array('count' => Model_Escort_Photos::MAX_PHOTOS_COUNT)), Cubix_Images::ERROR_IMAGE_COUNT_LIMIT_OVER);
					}
					$gallery_id = intval($this->_getParam('gallery_id'));
					$response = $model->upload();

					if($response['error'] == 0 && $response['finish']){
						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($response['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $response['name']))));

						$image = new Cubix_Images_Entry($image);
						/*$image->setSize('sthumb');
						$image->setCatalogId($escort->id);
						$image_url = $images->getUrl($image);*/

						$image_size = getimagesize($response['tmp_name']);
						$is_portrait = 0;
						if ( $image_size ) {
							if ( $image_size[0] < $image_size[1] ) {
								$is_portrait = 1;
							}
						}

						$photo_arr = array(
							'escort_id' => $escort->id,
							'gallery_id' => $gallery_id,
							'hash' => $image->getHash(),
							'ext' => $image->getExt(),
							'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD,
							'is_portrait' => $is_portrait,
							'width' => $image_size[0],
							'height' => $image_size[1],
							'creation_date' => date('Y-m-d H:i:s', time())
						);

						if ( /*$client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_PROFILE))) ||*/ $client->call('Escorts.isAutoApproval', array($escort_id)) ) {
							$photo_arr['is_approved'] = 1;
						}

						$photo = new Model_Escort_PhotoItem($photo_arr);

						$model = new Model_Escort_Photos();
						$photo = $model->save($photo);

						$response['photo_id'] = $photo->id;
						$response['photo_url'] = $photo->getUrl('thumb_cropper_ed');
						$response['is_main'] = $photo->is_main;
						$response['photo_type'] = $photo->type;
						$response['is_approved'] = $photo->is_approved;
						$response['args'] = unserialize($photo->args);

                        $response['hash'] = $photo->hash;
                        $response['ext'] = $photo->ext;
					}

                    if (in_array($response['error'], [1, 2, 3, 4, 6, 7, 8])) {
                        $response['error'] = __('seems_error');
                    }

					$model->showResponse($response);
					die;

				} catch ( Exception $e ) {
					echo json_encode(array(
						'finish' => 0,
						'error' => $e->getMessage()
					));
					die;

				}
			}
			elseif ( 'set-adj' == $action ) {
				$photo_id = intval($this->_getParam('photo_id'));

				if ( ! in_array($photo_id, $photo_ids) ) {
					die(json_encode(array('error' => 'An error occured')));
				}

				$photo = new Model_Escort_PhotoItem(array(
					'id' => $photo_id
				));

				try {
					$hash = $photo->getHash();
					$result = array(
						'x' => intval($this->_getParam('x')),
						'y' => intval($this->_getParam('y')),
						'px' => floatval($this->_getParam('px')),
						'py' => floatval($this->_getParam('py'))
					);
					$photo->setCropArgs($result);

					// Crop All images
					$size_map = array(
						'backend_thumb' => array('width' => 150, 'height' => 205),
						'medium' => array('width' => 225, 'height' => 300),
						'thumb' => array('width' => 150, 'height' => 200),
						'nlthumb' => array('width' => 120, 'height' => 160),
						'sthumb' => array('width' => 76, 'height' => 103),
						'lvthumb' => array('width' => 75, 'height' => 100),
						'agency_p100' => array('width' => 90, 'height' => 120),
						't100p' => array('width' => 117, 'height' => 97)
					);
					$conf = Zend_Registry::get('images_config');

					get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
					// echo $conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;

					$catalog = $escort_id;
					$a = array();
					if ( is_numeric($catalog) ) {
						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}
					}
					else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
						array_shift($a);
						$catalog = $a[0];

						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}

						$parts[] = $a[1];
					}

					$catalog = implode('/', $parts);

					foreach($size_map as $size => $sm) {
						get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
					}
				}
				catch ( Exception $e ) {
					die(json_encode(array('error' => 'An error occured')));
				}

				die(json_encode(array('success' => true)));
			}
			elseif ( 'set-rotate' == $action ) {
                $photo_id = intval($this->_getParam('photo_id'));
                $degree = intval($this->_getParam('degree'));
                $degree = $degree == 90 ? 90 : -90;
				if ( ! in_array($photo_id, $photo_ids) ) {
					die(json_encode(array('error' => 'An error occured')));
				}

                $photo = new Model_Escort_PhotoItem(array(
                    'id' => $photo_id
                ));

                try {
                    $photo_info = $photo->getHashExt();
                    $hash = $photo_info['hash'];
					$ext = $photo_info['ext'];

					if (!is_null($this->_getParam('hash')) && empty($hash)) {
						$photo_info['hash'] = $hash = $this->_getParam('hash');
					}
					if (!is_null($this->_getParam('extension')) && empty($ext)) {
						$photo_info['ext'] = $ext = $this->_getParam('extension');
					}

                    $conf = Zend_Registry::get('images_config');
                    $remoteUrl = str_replace('https://','http://', $conf['remote']['url']);
                    $remoteUrl = str_replace('https://','', $conf['remote']['url']);
                    $remoteUrl = $conf['remote']['url'];

                    $rotateUrl = $remoteUrl. "/get_image.php?a=rotate&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash . "&ext=". $ext . "&d=". $degree;
                    //get_headers($rotateUrl);

//					 $ch = curl_init($rotateUrl);
//                     curl_exec($ch);
//					 curl_close($ch);
//
                    $cacheUrl = $remoteUrl . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;
                    //get_headers($cacheUrl);
                    $result = array(
                        'x' => 0,
                        'y' => 0,
                        'px' => 0,
                        'py' => 0
                    );

                    $photo->setCropArgs($result);

                    $catalog = $escort_id;
                    $a = array();
                    if ( is_numeric($catalog) ) {
                        $parts = array();

                        if ( strlen($catalog) > 2 ) {
                            $parts[] = substr($catalog, 0, 2);
                            $parts[] = substr($catalog, 2);
                        }
                        else {
                            $parts[] = '_';
                            $parts[] = $catalog;
                        }
                    }
                    else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
                        array_shift($a);
                        $catalog = $a[0];

                        $parts = array();

                        if ( strlen($catalog) > 2 ) {
                            $parts[] = substr($catalog, 0, 2);
                            $parts[] = substr($catalog, 2);
                        }
                        else {
                            $parts[] = '_';
                            $parts[] = $catalog;
                        }

                        $parts[] = $a[1];
                    }

                    $catalog = implode('/', $parts);
					get_headers($rotateUrl . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_thumb_cropper.".$ext); 
                }
                catch ( Exception $e ) {
                    die(json_encode(array('error' => 'An error occured')));
                }

				die(json_encode(array('success' => true, 'rotateURL' => $rotateUrl)));
				
			}
			elseif ( 'make' == substr($action, 0, 4) ) {
				$type = substr($action, 5);
				if ( ! in_array($type, array('public', 'private')) ) {
					die;
				}

				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = 'Please select at least one photo';
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						return $this->view->actionError = 'Invalid id of one of the photos';
					}
				}

				foreach ( $ids as $id ) {
					$photo = new Model_Escort_PhotoItem(array('id' => $id));
					$photo->make($type == 'private' ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD);
				}

				echo json_encode(array(
					'status' => 1,
					'name' => "success"
				));
				die;
			}
			elseif ( 'sort' == $action ) {
				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					die('Please select at least one photo');
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						die('Invalid id of one of the photos');
					}
				}

				$model = new Model_Escort_Photos();
				$model->reorder($ids);

				die;
			}
			elseif ( 'rename' == $action ) {
				$gallery_name = $this->_getParam('gallery_name');
				$gallery_id = $this->_getParam('gallery_id');
				$return = $this->client->call('changeGalleryName', array($escort_id,$gallery_id, $gallery_name ));
				die($return);
			}
			elseif ( 'change-gallery' == $action ) {
				$ids = $this->_getParam('photo_id');
				$gallery_id = intval($this->_getParam('gallery_id'));
				$type_id = is_null($this->_getParam('type')) ? null : intval($this->_getParam('type'));
				
				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = 'Please select at least one photo';
				}
				
				foreach ( $ids as $id ) {
					$this->client->call('changeGallery', array($escort_id, $id, $gallery_id, $type_id  ));
				}

				die(json_encode(array('success' => true)));
			}
			elseif ('change-gallery-type' == substr($action, 0, 19)) {
				// die(json_encode(array('success' => true)));	
				$type = substr($action, 20);
				$gallery_id = intval($this->_getParam('gallery_id'));
				$gallery_fake_id = intval($this->_getParam('gallery_fake_id'));

				if ( ! in_array($type, array('public', 'private')) ) {
					die;
				}

				$status = ($type == 'public') ? 1 : 2;

				$this->client->call('setGalleryStatus', array($escort_id, $gallery_fake_id, $status));
				$gallery_photos_private = $this->client->call('getEscortPhotosList', array($escort_id, true, $gallery_id));
				$gallery_photos_public = $this->client->call('getEscortPhotosList', array($escort_id, false, $gallery_id));

				$gallery_photos = array_merge($gallery_photos_public, $gallery_photos_private);
				
				foreach ( $gallery_photos as $g_photo ) {
					$photo = new Model_Escort_PhotoItem(array('id' => $g_photo['id']));
					$photo->make($type == 'private' ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD);
				}

				die(json_encode(array('success' => true)));
			}
			$this->view->escort = $escort;
		}
	}

	public function plainPhotosAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->user = $this->user;

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort'));

			if ( 1 > $escort_id ) die;

			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}

			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}


		$photos = $this->_loadPhotos();

		$photos_ids = array();
		foreach ( $photos as $photo ) {
			$photo_ids[] = intval($photo['id']);
		}

		$action = $this->_getParam('a');

		$photos_min_count = Cubix_Application::getById(Cubix_Application::getId())->min_photos;

		$this->view->is_private = $is_private = (bool) intval($this->_getParam('is_private'));

		if ( 'set-main' == $action || ! is_null($this->_getParam('set_main')) ) {
			$ids = $this->_getParam('photo_id');
			//$rotate_type = $this->_getParam('rotate');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED;
			if(count($ids) == 1){
				$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED_ONE;
			}

			$photo_id = reset($ids);
			if ( ! in_array($photo_id, $photo_ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
			}

			$photo = new Model_Escort_PhotoItem(array('id' => $photo_id ,'escort_id' => $escort_id));
			$photo->setRotatePics($ids);
			$result = $photo->setMain();
			$client->call('Escorts.setPhotoRotateType', array($escort_id, $rotate_type));
			$escort->photo_rotate_type = $rotate_type;

			$this->_loadPhotos();
		}
		elseif ( 'delete' == $action || ! is_null($this->_getParam('delete')) ) {
			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}
			}

			$photo = new Model_Escort_Photos();
			$result = $photo->remove($ids);

			$this->_loadPhotos();
		}
		elseif ( 'upload' == $action || ! is_null($this->_getParam('upload')) ) {

				$set_photo = false;
				$config = Zend_Registry::get('images_config');
				$new_photos = array();
				$upload_errors = array();

				foreach ( $_FILES as $i => $file )
				{
					try {
						if ( ! isset($file['name']) || ! strlen($file['name']) ) {
							continue;
						}
						else {
							$set_photo = true;
						}

						$img_ext = strtolower(@end(explode('.', $file[name])));
						if (!in_array( $img_ext , $config['allowedExts'])){
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
						}

						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($file['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $file['name']))));

						$image = new Cubix_Images_Entry($image);
						$image->setSize('sthumb');
						$image->setCatalogId($escort->id);
						$image_url = $images->getUrl($image);

						$image_size = getimagesize($file['tmp_name']);
						$is_portrait = 0;
						if ( $image_size ) {
							if ( $image_size[0] < $image_size[1] ) {
								$is_portrait = 1;
							}
						}

						$photo_arr = array(
							'escort_id' => $escort->id,
							'hash' => $image->getHash(),
							'ext' => $image->getExt(),
							'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD,
							'is_portrait' => $is_portrait,
							'width' => $image_size[0],
							'height' => $image_size[1],
							'creation_date' => date('Y-m-d H:i:s', time())
						);

						if ( $client->call('Escorts.isPhotoAutoApproval', array($escort_id)) ) {
							$photo_arr['is_approved'] = 1;
						}
						/*else if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_PROFILE))) ) {
							$photo_arr['is_approved'] = 1;
						}*/

						$photo = new Model_Escort_PhotoItem($photo_arr);

						$model = new Model_Escort_Photos();
						$photo = $model->save($photo);

						$new_photos[] = $photo;
					} catch ( Exception $e ) {
						$upload_errors[$i] = $file['name'] . ' (' . $e->getMessage() . ')';
					}
			}

			if ( ! $set_photo ) {
				$this->view->uploadError = Cubix_I18n::translate('sys_error_select_photo');
			}

			$this->view->newPhotos = $new_photos;
			$this->view->uploadErrors = $upload_errors;
		}
		elseif ( 'set-adj' == $action ) {
			$photo_id = intval($this->_getParam('photo_id'));

			if ( ! in_array($photo_id, $photo_ids) ) {
				die(json_encode(array('error' => 'An error occured')));
			}

			$photo = new Model_Escort_PhotoItem(array(
				'id' => $photo_id
			));

			try {
				$hash = $photo->getHash();
				$result = array(
					'x' => intval($this->_getParam('x')),
					'y' => intval($this->_getParam('y')),
					'px' => floatval($this->_getParam('px')),
					'py' => floatval($this->_getParam('py'))
				);
				$photo->setCropArgs($result);

				// Crop All images
				$size_map = array(
					'backend_thumb' => array('width' => 150, 'height' => 205),
					'medium' => array('width' => 225, 'height' => 300),
					'thumb' => array('width' => 150, 'height' => 200),
					'nlthumb' => array('width' => 120, 'height' => 160),
					'sthumb' => array('width' => 76, 'height' => 103),
					'lvthumb' => array('width' => 75, 'height' => 100),
					'agency_p100' => array('width' => 90, 'height' => 120),
					't100p' => array('width' => 117, 'height' => 97)
				);
				$conf = Zend_Registry::get('images_config');

				get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
				// echo $conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;

				$catalog = $escort_id;
				$a = array();
				if ( is_numeric($catalog) ) {
					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}
				}
				else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
					array_shift($a);
					$catalog = $a[0];

					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}

					$parts[] = $a[1];
				}

				$catalog = implode('/', $parts);

				foreach($size_map as $size => $sm) {
					get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
				}
			}
			catch ( Exception $e ) {
				die(json_encode(array('error' => 'An error occured')));
			}

			die(json_encode(array('success' => true)));
		}
		elseif ( ! is_null($this->_getParam('make_private')) || ! is_null($this->_getParam('make_public')) ) {

			if ( ! is_null($this->_getParam('make_private')) ) {
				$type = 'private';
			} elseif ( ! is_null($this->_getParam('make_public')) ) {
				$type = 'public';
			} else {
				die;
			}

			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}
			}

			foreach ( $ids as $id ) {
				$photo = new Model_Escort_PhotoItem(array('id' => $id));
				$photo->make($type == 'private' ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD);
			}

			$this->_loadPhotos();
		}
		elseif ( 'sort' == $action ) {
			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				die(Cubix_I18n::translate('sys_error_select_at_least_on_photo'));
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					die(Cubix_I18n::translate('sys_error_invalid_id_photo'));
				}
			}

			$model = new Model_Escort_Photos();
			$model->reorder($ids);

			die;
		}
		$this->view->escort = $escort;
//			if ( 'upload' != $action ) {
//				$this->view->layout()->disableLayout();
//				$this->_helper->viewRenderer->setNoRender(true);
//				die;
//			}
	}

	public function videoAction()
	{
		$this->view->user = $this->user;
		$video_model = new Model_Video();
		$client = new Cubix_Api_XmlRpc_Client();
		$config =  Zend_Registry::get('videos_config');

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {

			$escort_id = intval($this->_getParam('escort'));
			if ( 1 > $escort_id ) die;
			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}
			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}

		$action = $this->_getParam('a');

		if ( 'delete' == $action || ! is_null($this->_getParam('delete')) ) {
			$video_id = intval($this->_getParam('video_id'));
			$video_model->Remove($video_id,$escort_id);
			die;
		}
		elseif ( 'upload' == $action || ! is_null($this->_getParam('upload')) ) {

			$host =  Cubix_Application::getById(Cubix_Application::getId())->host;
			$response = array(
				'error' => '',
				'finish' => FALSE
			);

			try {

				if($video_model->UserVideoCount($escort_id) < $config['VideoCount'])
				{
					$backend_video_count = $client->call('Application.UserVideoCount',array($escort_id));
					$check = Cubix_Videos::CheckVideoStatus($backend_video_count, $config['VideoCount'], true);

					if(!$check)
					{
						$video = new Cubix_Videos($escort_id,$config);
						$response = $video->getResponse();

						if($response['finish'] && $response['error'] === 0 ){

							$video->ConvertToMP4();
							//$video->AddMetaData();
							$image = $video->SaveImage();
							if(is_array($image))
							{
								$display_image = array();
								$display_image = $image[2];
								$display_image['application_id'] = Cubix_Application::getId();
								$photo = new Cubix_ImagesCommonItem($display_image);

								$width = $display_image["width"];
								$height = $display_image["height"];
								$image_url = $photo->getUrl('m330');

								if($video_data = $video->SaveVideoImageFtpFrontend($image,$client)){
									$response['vod'] = $config['Media'].$host.'/'.$escort_id.'/';
									$response['photo_url'] = $image_url;
									$response['video_width'] = $width;
									$response['video_height'] = $height;
									$response['video_name'] = $video_data['name'];
									$response['video_id'] = $video_data['id'];
								}
								else{
									$response['error'] = Cubix_I18n::translate('sys_error_while_upload');
								}
							}
							else{
								$response['error'] = $image;
							}
						}
					}
					else{
						$response['error'] = $check;
					}
				}
				else{
					$response['error'] = Cubix_I18n::translate('sys_error_upload_video_count',array('count' => $config['VideoCount']));
				}
			} catch ( Exception $e ) {
				 $response['error'] = $e->getMessage();

			}
			Cubix_Videos::showResponse($response);
			die;
		}
	}

	public function naturalPicAction()
	{
		$this->view->user = $this->user;
		$nat_model = new Model_NaturalPic();
		$config =  Zend_Registry::get('images_config');
		$client = new Cubix_Api_XmlRpc_Client();


		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;

		}
		else {

			$escort_id = intval($this->_getParam('escort'));

			if ( 1 > $escort_id ) die;

			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}

			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}

		$action = $this->_getParam('a');

		switch($action){
			case 'upload':
				try {
					$model = new Model_Escort_Photos();
					$response = $model->upload();

					if($response['error'] == 0 && $response['finish']){
						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($response['tmp_name'], $escort_id. '/natural-pics', $this->app_id, strtolower(@end(explode('.', $response['name']))));
						$image = new Cubix_Images_Entry($image);
						$image->setSize('nat_pic_thumb');
						$image->setCatalogId($escort_id. '/natural-pics');
						$image_url = $images->getUrl($image);


						$photo_arr = array(
							'escort_id' => $escort_id,
							'hash' => $image->getHash(),
							'ext' => $image->getExt(),
							'creation_date' => date('Y-m-d H:i:s', time())
						);
						$new_id = $nat_model->save($photo_arr);


						$response['photo_id'] = $new_id;
						$response['photo_url'] = $image_url;
					}

					$model->showResponse($response);
					die;

				} catch ( Exception $e ) {
					echo json_encode(array(
						'finish' => 0,
						'error' => $e->getMessage()
					));
					die;
				}
				break;

			case 'delete':
				$nat_model->remove($escort_id);
				die;
		}




		$this->view->escort = $escort;

	}

	public function escortsAction()
	{
		if ( ! $this->user->isAgency() ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}

		$this->view->s_showname = '';
		$this->view->is_active = (bool) $this->_getParam('is_active');

		$defs = Zend_Registry::get('definitions');
		$sort_filter_tructure  = $defs['agency_escorts_sort'];

		$sort_filter = new Cubix_NestedMenu(array('childs' => $sort_filter_tructure));
		$sort_filter->setId('sort-options');
		$sort_filter->setSelected( $sort_filter->getByValue('paid-package') );

		$page_filter_structure = $defs['agency_escorts_pages'];
		$per_page_filter = new Cubix_NestedMenu(array('childs' => $page_filter_structure));
		$per_page_filter->setId('per-page-options');
		$per_page_filter->setSelected( $per_page_filter->getByValue(9) );
		$client = new Cubix_Api_XmlRpc_Client();


		if ( $this->_request->isPost() ) {

			$action = $this->_getParam('a');

			if ( $this->user->isEscort() ) {
				$escort = $this->user->getEscort();
				$escort_id = $escort->id;
			}
			else {
				$escort_id = intval($this->_getParam('escort_id'));

				if ( 0 == $escort_id ) $escort_id = null;

				if ( ! is_null($escort_id) && ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
					die;
				}

				$model = new Model_Escorts();
				$escort = $model->getById($escort_id);
			}

			switch ( $action ) {
				case 'activate':
					$this->view->actionError = '';
					if ( $escort->status & Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED ) {
						$this->view->actionError .= 'The escort is deactivated by administration!<br/>';
					}

					if ( $escort->status & Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS ) {
						$this->view->actionError .= 'You cannot activate this escort, not enough photos!<br/>';
					}


					if ( $escort->status & Model_Escorts::ESCORT_STATUS_IS_NEW || $escort->status & Model_Escorts::ESCORT_STATUS_NO_PROFILE ) {
						$this->view->actionError .= 'The profile of this escort has not been approved yet!<br/>';
					}

					if ( ! strlen($this->view->actionError) ) {
						$client->call('Escorts.activate', array($escort_id));

						$this->view->escorts = $escorts = $this->agency->getEscorts();
					}

					break;
				case 'deactivate':
					$client->call('Escorts.deactivate', array($escort_id));

					$this->view->escorts = $escorts = $this->agency->getEscorts();

					break;
				case 'delete':
					// TODO: escort products deletion. Premium package assigned escorts error.
					$client->call('Escorts.deleteTemporary', array($escort_id));
					$this->view->escorts = $escorts = $this->agency->getEscorts();
					break;
				default:

			}
		}

        $per_page = 9;
        $cur_page = 1;

        $escorts_a = $this->agency->getEscortsPerPage($cur_page, $per_page, 1); //32 = Active
        $escorts_i = $this->agency->getEscortsPerPage($cur_page,$per_page);//default inactive
        $escorts_d = $this->agency->getEscortsPerPage($cur_page,$per_page,-1);//default inactive

        $this->view->escorts_a_count = $escorts_a['escorts_count'];
        $this->view->escorts_i_count = $escorts_i['escorts_count'];
        $this->view->escorts_d_count = $escorts_d['escorts_count'];

        $this->view->escorts_per_page = $per_page;
        $this->view->current_page = $cur_page;

        $this->view->escorts_active = $escorts_a['escorts'];
        $this->view->escorts_inactive = $escorts_i['escorts'];
        $this->view->escorts_deleted = $escorts_d['escorts'];
		$this->view->sort_filter = $sort_filter;
		$this->view->per_page_filter = $per_page_filter;


	}

	private function _getPremiumList()
	{
		return Cubix_Api::getInstance()->call('premium_getAgencyPremiumEscorts', array($this->agency->id, $this->view->lang()));
	}

	public function premiumAction()
	{
		if ( $this->user->isAgency() ) {
			$this->_helper->viewRenderer->setScriptAction('agency-premium');


			$this->view->escorts = $this->_getPremiumList();
		}
		elseif ( $this->user->isEscort() ) {
			$this->_helper->viewRenderer->setScriptAction('escort-premium');
		}
	}

	public function ajaxPremiumSwitchAction()
	{
		// Require user to be an agency
		if ( ! $this->user->isAgency() ) die;

		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'from_escort_id' => 'int-nz',
			'to_escort_id' => 'int-nz',
			'city_ids' => 'arr-int'
		));
		$data = $form->getData();

		// If escort does not have any of these escorts
		if ( ! $this->agency->hasEscort($data['from_escort_id']) ||
				! $this->agency->hasEscort($data['to_escort_id']) ) {
			die;
		}

		$client = Cubix_Api::getInstance();
		$result = $client->call('premium_switchActivePackages', array($data['from_escort_id'], $data['to_escort_id'], $data['city_ids']));

		die(json_encode(array('data' => $this->_getPremiumList(), 'status' => 'success')));
	}

    public function ajaxGetAgencyEscortsAction()
    {
        $steps = ['biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times',
            'prices', 'contact-info', 'gallery', 'video', 'natural-pic'];

        $this->view->steps = array_slice($steps, 0, -2);
        if ( $this->user->isAgency() && $this->mode == 'create') {
            $this->view->steps = array_slice($steps, 0, -3);
            $steps = array_slice($steps, 0, -3);
            $this->view->escort_profile_creating = true;
        }

        $this->view->layout()->disableLayout();

        if ( ! $this->user->isAgency() ) die;

        $this->view->s_showname = '';

        $client = new Cubix_Api_XmlRpc_Client();

        $active_tab = $this->view->active_tab = $this->_getParam('active_tab', 'active');

        if ( $this->_request->isPost() ) {
            $action = $this->_getParam('a');

            if ( $this->user->isEscort() ) {
                $escort = $this->user->getEscort();
                $escort_id = $escort->id;
            }
            else {
                $this->view->a_escort_id = $escort_id = intval($this->_getParam('escort_id'));

                if ( 0 == $escort_id ) $escort_id = null;

                if ( ! is_null($escort_id) && ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
                    die;
                }

                $model = new Model_Escorts();
                $escort = $model->getById($escort_id);
            }

            switch ( $action ) {
                case 'activate':
                    $this->view->actionError = '';
                    if ( $escort->status & Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED ) {
                        $this->view->actionError .= 'The escort is deactivated by administration!';
                    }

                    if ( $escort->status & Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS ) {
                        $this->view->actionError .= 'You cannot activate this escort, not enough photos!';
                    }


                    if ( $escort->status & Model_Escorts::ESCORT_STATUS_IS_NEW || $escort->status & Model_Escorts::ESCORT_STATUS_NO_PROFILE ) {
                        $this->view->actionError .= 'The profile of this escort has not been approved yet!';
                    }

                    if (! strlen($this->view->actionError) ) {
                        $client->call('Escorts.activate', array($escort_id));

                        //$this->view->escorts = $escorts = $this->agency->getEscorts();
                    }

                    break;
                case 'deactivate':
                    $client->call('Escorts.profileStatus', array($escort_id, 'disable'));
                    break;
                case 'delete':
                    // TODO: escort products deletion. Premium package assigned escorts error.
                    $client->call('Escorts.deleteTemporary', array($escort_id, true));
                    break;
                case 'restore':
                    // TODO: escort restore
                    $client->call('Escorts.restore', array($escort_id, TRUE));
                    // $this->view->escorts = $escorts = $this->agency->getEscorts();
                    break;
            }
        }

        $per_page = $this->_request->getParam('per_page', 18);
        if ( !$per_page ) {
            $per_page = 18;
        }

        $escorts_page = $this->view->escorts_page = $this->_getParam('escorts_page', 1);

        $sort = $this->view->sort = $this->_getParam('agency_sort', 'alpha');
        switch($sort) {
            case 'paid-package':
                $sort = 'p.name DESC';
                BREAK;
            case 'alpha':
                $sort = 'e.showname';
                BREAK;
            case 'escort-id':
                $sort = 'e.id';
                BREAK;
            case 'last-modified':
                $sort = 'e.date_last_modified';
                BREAK;
        }

        switch($active_tab) {
            case 'active':
                $data = $this->agency->getEscortsPerPage($escorts_page, $per_page, 1, false, false, $sort);
                break;
            case 'disabled':
                $data = $this->agency->getEscortsPerPage($escorts_page, $per_page, null, false, false, $sort);
                break;
            case 'deleted':
                $data = $this->agency->getEscortsPerPage($escorts_page, $per_page, -1, false, false, $sort);
                break;
        }

        $this->view->count = $data['escorts_count'];
        $this->view->escorts_per_page = $per_page;

        $this->view->escorts = $data['escorts'];
        $this->view->agency_id = $this->agency->getId();
        $this->view->current_page = $cur_page;

        $this->_helper->viewRenderer->setScriptAction('agency-escorts');
    }


    public function ajaxEscortsDoAction(){
        $action = $this->_getParam('a');
        $client = new Cubix_Api_XmlRpc_Client();


        $escort_id = intval($this->_getParam('escort_id'));

        if ( 0 == $escort_id ) $escort_id = null;

        if ( ! is_null($escort_id) && ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
            die;
        }

        $model = new Model_Escorts();
        $escort = $model->getById($escort_id);

        $return['success'] = false;
        $return['message'] = '';


        if($escort){

            switch ( $action ) {
                case 'activate':

                    if ( $escort->status & Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED ) {
                        $return['message'] .= 'The escort is deactivated by administration!<br/>';
                    }

                    if ( $escort->status & Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS ) {
                        $return['message'] .= 'You cannot activate this escort, not enough photos!<br/>';
                    }


                    if ( $escort->status & Model_Escorts::ESCORT_STATUS_IS_NEW || $escort->status & Model_Escorts::ESCORT_STATUS_NO_PROFILE ) {
                        $return['message'] .= 'The profile of this escort has not been approved yet!<br/>';
                    }



                    if ( ! strlen($return['message']) ) {
                        $client->call('Escorts.activate', array($escort_id));

                        $return['success'] = true;
                    }

                    break;
                case 'deactivate':
                    $client->call('Escorts.deactivate', array($escort_id));

                    $return['success'] = true;

                    break;
                case 'delete':
                    // TODO: escort products deletion. Premium package assigned escorts error.

                    $res = $client->call('Escorts.deleteTemporary', array($escort_id));
                    if($res){
                        $res = json_decode($res);

                        if ( !$res->success ) {
                            $return['message'] = $res->message;
                        }
                    }

                   // $this->view->escorts = $escorts = $this->agency->getEscorts();
                    break;
                case 'restore':
                    // TODO: escort restore
                    $client->call('Escorts.restore', array($escort_id));
                    $return['success'] = true;
                   // $this->view->escorts = $escorts = $this->agency->getEscorts();
                    break;
                default:

            }
        }

        die(json_encode($return));
    }
    /* Update Grigor */


	public function ajaxEscortsAction()
	{
		// Require user to be an agency
		if ( ! $this->user->isAgency() ) die;

		$escorts = Cubix_Api::getInstance()->call('premium_getAgencyNonPremiumEscorts', array($this->agency->id));

		die(json_encode($escorts));
	}

	public function ajaxEscortCitiesAction()
	{
		// Require user to be an agency
		if ( ! $this->user->isAgency() ) die;

		$escort_id = intval($this->_getParam('escort_id'));

		// If this escort does not belong to this agency
		if ( ! $this->agency->hasEscort($escort_id) ) die;

		$cities = Cubix_Api::getInstance()->call('premium_getEscortCities', array($escort_id, $this->view->lang()));
		$limit = Cubix_Api::getInstance()->call('premium_getEscortCitiesLimit', array($escort_id));

		die(json_encode(array('cities' => $cities, 'limit' => $limit)));
	}

	public function getBubbleTextAction()
	{
		if ( ! $this->user->isAgency() && ! $this->user->isEscort() ) {
			die();
		}

		$req = $this->_request;

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($req->escort_id);
		}

		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$bubble = $client->call('Escorts.getBubbleText', array($escort_id));

		die(json_encode($bubble));
	}

	public function addBubbleTextAction()
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}
		}

		$req = $this->_request;

		$text = strip_tags(mb_substr(stripslashes($req->text), 0, 1000, 'UTF-8'));

		$data = array('escort_id' => $escort_id, 'text' => $text);
		if ( strlen($text) > 0 ) {
			$model_bl_words = new Model_BlacklistedWords();
			if($model_bl_words->checkWords($text, Model_BlacklistedWords::BL_TYPE_BUBBLE_TEXT)){
				echo 'You can`t use word "'.$model_bl_words->getWords().'"';
				die;
			}
			$data = array_merge($data, array('status' => 1));
		}
		else {
			$data = array_merge($data, array('status' => 0));
		}

		// US, France set status pending
		$countries = new Cubix_Geography_Countries();
		$config_system = Zend_Registry::get('system_config');
		$bubble_text_status = $config_system['bubbleTextApprovation'];

		if($this->user->isAgency() || empty($escort)){ 
			$escort = $client->call('Escorts.getById', array($escort_id));
		}

		$country = $countries->get($escort->country_id);
		if(in_array($country->iso, ['us', 'fr'])) $bubble_text_status = 0;
		//

		$status = $client->call('Escorts.addBubbleText', array($data, $bubble_text_status));
		echo $status;

		die;
	}

	public function existsByShownameAction()
	{
		$model = new Model_EscortsV2();

		$req = $this->_request;

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort'));

			if ( $escort_id > 0 ) {
				if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
					die;
				}
			}
		}

		//$escort_id = $req->getParam('escort_id', false);
		$showname = $req->getParam('showname', false);

		$status = $model->existsByShowname($showname, $escort_id);

		if ( $status ) {
			$stat = 'true';
		}
		else {
			$stat = 'false';
		}

		die(json_encode(array('status' => $stat)));
	}


    public function escortActivePackageAction()
	{

        $this->view->layout()->disableLayout();

        $escort_id  = $this->_request->getParam('escort_id');
        $isActive       = $this->_request->getParam('isActive');

        if ( $this->user->isAgency() AND $this->agency->hasEscort($escort_id) ) {


			$package = $this->_getEscortActivePackage($escort_id);
            $this->view->package = $package;
            $this->view->isActive = $isActive;
		}


	}

    private function _getEscortActivePackage($escort_id){
        return Cubix_Api::getInstance()->call('getEscortActivePackage', array($escort_id,Cubix_I18n::getLang()));
    }

	public function clientBlacklistAction()
	{
		$req = $this->_request;

		$page = 1;
		$per_page = 10;
		if($this->_getParam('page')){
			$page = $this->_getParam('page');
		}

		$criteria = $this->view->criteria = $req->client_criterias;

		$params = array($criteria, $page, $per_page, $req->lang_id);
		$data = Cubix_Api::getInstance()->call('getBlacklistedClients', array($params));

		$result = $this->view->result = $data['clients'];
		$count = $this->view->count = $data['count'];
		$this->view->page = $page;
		$this->view->per_page = $per_page;
	}

	public function addClientToBlacklistAction()
	{
		$req = $this->_request;

		$validator = new Cubix_Validator();
		$data = array();

		if ( $req->isPost() )
		{
			$date = intval($req->date);
			$client_name = $req->client_name;
			$client_phone = $req->client_phone;
			$comment = $req->comment;
			$city_id = $req->city_id_h;
			if(!empty($req->attach)){
				$photos = $req->attach;
			}

			if ( ! $city_id ) {
				$city_id = null;
			}

			if ( ! $date ) {
				$validator->setError('date', 'Date is Required');
			}
			else if ( $date > time() ) {
				$validator->setError('date', 'Date is on Future');
			}

			if ( ! strlen($client_name) && ! strlen($client_phone) ) {
				$validator->setError('name_or_phone', 'Client Name or Phone is Required');
			}

			if ( ! strlen($comment) ) {
				$validator->setError('err_comment', 'Comment is Required');
			}

			if ( $validator->isValid() ) {

				$data = array(
					'application_id' => Cubix_Application::getId(),
					'user_id' => $this->user->id,
					'date' => $date,
					'client_name' => $client_name,
					'city_id' => $city_id,
					'client_phone' => $client_phone,
					'comment' => $comment
				);
				if($photos){
					$data['photos'] = $photos;
				}

				Cubix_Api::getInstance()->call('addClientToBlacklist', array($data));
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function attachedImageBlacklistAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$images = new Cubix_ImagesCommon();
		$error = false;
		$extension = end(explode(".", $_FILES["Filedata"]["name"]));

		if ($error) {

			$return = array(
				'status' => '0',
				'error' => $error
			);

		} else {
			if ( strlen($_FILES['Filedata']['tmp_name']) ) {
				$photo = $images->save($_FILES['Filedata']);

				$return = array(
					'status' => '1',
					'id' => $photo['image_id']
				);
			}
		}

		echo json_encode($return);
	}

	public function faqAction()
	{
		$lng = Cubix_I18n::getLang();

		$type = $this->user->user_type;

		$config = Zend_Registry::get('faq_config');
		$cache = Zend_Registry::get('cache');
		$cache_key = 'v2_faq_type_' . $type . '_lang_' . $lng;

		if ( ! $items = $cache->load($cache_key) )
		{
			$items = Cubix_Api::getInstance()->call('getFaqByType', array($type, $lng));

			$cache->save($items, $cache_key, array(), $config['cacheTime']);
		}

		$this->view->items = $items;
	}


	public function vipMemberCancelAction()
	{
		$this->view->layout()->disableLayout();

		$user_id = $this->user->id;
		$cancel = $this->_request->cancel;

		if ( $cancel ) {
			try {
				$updated = Cubix_Api::getInstance()->call('members.cancelPremium', array($user_id));
			}
			catch ( Exception $e ) {

			}

			$this->user['member_data']['is_premium'] = 0;
			$this->user['member_data']['is_recurring'] = 0;
			$this->user['member_data']['date_expires'] = null;

			Model_Users::setCurrent($this->user);
		}
	}

	public function ajaxEscortCommentsAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$com_model = new Model_Comments();
		$page = intval($req->getParam('page'));
		$per_page = intval($req->getParam('per_page'));
		if($req->getParam('is_agency')){
			$escort_id = $req->getParam('escort_id');
			$this->view->comments = $com_model->getCommentsByEscortIds(json_decode($escort_id), $page, $per_page, $count);
			$this->view->is_agency = 1;
		}
		else{
			$escort_id = intval($req->getParam('escort_id'));
			$this->view->comments = $com_model->getEscortComments($page,$per_page, $count, $escort_id);
			$this->view->is_agency = 0;
		}
		$this->view->escort_id = $escort_id;
		$this->view->comments_count = $count;
		$this->view->comments_per_page = $per_page;
		$this->view->comments_page = $page;
	}

	public function addReplyAction()
	{
		$config = Zend_Registry::get('escorts_config');
		$this->view->layout()->disableLayout();
		$user_id = $this->user->id;

		if ( $this->_request->isPost() ) {

			$validator = new Cubix_Validator();

			$req = $this->_request;

			if (!trim($req->comment))
				$validator->setError('comment', __('comment_requiered'));
			elseif(strlen(trim($req->comment)) < intval($config['comments']['textLength']))
				$validator->setError('comment',  __('comment_is_short',array('LENGTH' => $config['comments']['textLength'])));
			if ( $validator->isValid() ) {
				$reply_comment = $req->comment;
				$comment_id = $req->comment_id;
				$escort_id =  $req->escort_id;
				$data = array(
					'user_id' => $user_id,
					'user_type' => 'escort',
					'escort_id' => $escort_id,
					'status' => Model_Comments::COMMENT_NOT_APPROVED,
					'message' => $reply_comment,
					'is_reply_to' => $comment_id
				);
				$comment_model = new Model_Comments();
				$comment_model->addComment($data);
			  }
			die(json_encode($validator->getStatus()));
		}

    }

	public function scheduleAction(){
		$model = new Model_Schedule();
		$req = $this->_request;
		$escort = $this->user->getEscort();
		$escort_id = $escort->id;

		if ( $req->isPost() ) {
			$data = $req->selected_days;

			$model->updateSchedule($data, $escort_id);
		}

		$this->view->schedules = $model->getEscortSchedule( $escort_id );


	}

	public static function _sortItemDecorator($item, $is_first = FALSE, $is_hidden = FALSE)
	{
		$lng = Cubix_I18n::getLang();
		$has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);

		$classes = array();

		if ( $is_first ) $classes[] = 'first';
		if ( $has_childs ) $classes[] = 'sub';

		$link = '#';
		if ( ! $has_childs ) {
			$link = self::$linkHelper->getLink('escorts', array('sort' => $item->value));
		}

		$title = $item->title;
		if ( $is_first && isset($item->title) ) {
			$title = $item->parent . ' - ' . $item->title;
		}

		$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="#"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Agencyescort.Load({lang: \'' .$lng. '\', page: 1 , is_active: 1, sort:\'' . $item->value .'\'})"' : '' ) . '>' . $title . '</a>';

		if ( $is_first ) {
			$html = '<div class="input-w"><div class="i">' .
				$html .
			'</div></div>';
		}

		return $html;
	}

	public static function _perPageItemDecorator($item, $is_first = FALSE, $is_hidden = FALSE)
	{
		$lng = Cubix_I18n::getLang();
		$has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);

		$classes = array();

		if ( $is_first ) $classes[] = 'first';
		if ( $has_childs ) $classes[] = 'sub';

		$link = '#';
		if ( ! $has_childs ) {
			$link = self::$linkHelper->getLink('escorts', array('sort' => $item->value));
		}

		$title = $item->title;
		if ( $is_first && isset($item->title) ) {
			$title = $item->parent . ' - ' . $item->title;
		}

		$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="#"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Agencyescort.Load({lang: \'' .$lng. '\', page: 1 , is_active: 1, per_page:\'' . $item->value .'\'})"' : '' ) . '>' . $title . '</a>';

		if ( $is_first ) {
			$html = '<div class="input-w"><div class="i">' .
				$html .
			'</div></div>';
		}

		return $html;
	}

	public function ajaxAllRotatableAction(){

		try{
			$req = $this->_request;
			$escort_id = intval($req->escort);
			$model = new Model_Escorts();
			$this->escort = $model->getById($escort_id);
			$photos = $this->_loadPhotos();

			$first_valid_photo = null;
			foreach ( $photos as $photo ) {
				if( $photo['type'] != ESCORT_PHOTO_TYPE_PRIVATE &&  $photo['type'] != ESCORT_PHOTO_TYPE_DISABLED  && $photo['status'] != Model_Escort_Photos::STATUS_NOT_VERIFIED && is_null($first_valid_photo)){
					$first_valid_photo = intval($photo['id']);
				}
			}
			$photo = new Model_Escort_PhotoItem(array('id' => $first_valid_photo,'escort_id' => $escort_id));
			$photo->setRotatePics();
			$result = $photo->setMain();

			$client = Cubix_Api_XmlRpc_Client::getInstance();
            $client->call('Escorts.setPhotoRotateType', array($escort_id, Model_Escort_Photos::PHOTO_ROTATE_ALL));

		}
		catch ( Exception $e ) {
			die( $e->getMessage());
		}
		die(json_encode(array('main' => $first_valid_photo)));
	}

	public function getUrgentMessageAction()
	{
		$this->view->layout()->disableLayout();
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		if ($this->user->isEscort())
		{
			$escort_id = $this->user->getEscort()->id;
            $message = $client->call('Escorts.getUrgentMessageForEscort', array($escort_id));

			if ($message)
			{
				$this->view->message = $message;
				$this->view->type = 'escort';
			}
			else
				die;
		}
		elseif ($this->user->isAgency())
		{
			$user_id = $this->user->id;
			$messages = $client->call('Escorts.getUrgentMessageForAgency', array($user_id));

			if ($messages)
			{
				$this->view->message = $messages;
				$this->view->type = 'agency';
			}
			else
				die;
		}
		else
			die;
	}

	public function setUrgentMessageAction()
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		if ($this->user->isEscort())
		{
			$escort_id = $this->user->getEscort()->id;
			$client->call('Escorts.setUrgentMessageForEscort', array($escort_id));
		}
		elseif ($this->user->isAgency())
		{
			$user_id = $this->user->id;
			$client->call('Escorts.setUrgentMessageForAgency', array($user_id));
		}

		die;
	}

	public function getRejectedVerificationAction()
	{
		$this->view->layout()->disableLayout();
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		if ($this->user->isEscort())
		{
			$escort_id = $this->user->getEscort()->id;
            $ret = $client->call('Escorts.getRejectedVerificationForEscort', array($escort_id));

			if ($ret && is_array($ret))
			{
				$ids_arr = explode(',', $ret['reason_ids']);
				$message = '';

				foreach ($ids_arr as $id)
				{
					if ($id == 4)
						$message .= $ret['reason_text'] . '<br><br>';
					else
						$message .= $this->view->t('verify_reject_reason_' . $id) . '<br><br>';
				}

				$this->view->message = $message;
				$this->view->type = 'escort';
			}
			else
				die;
		}
		elseif ($this->user->isAgency())
		{
			$user_id = $this->user->id;
			$ret = $client->call('Escorts.getRejectedVerificationForAgency', array($user_id));

			if ($ret && is_array($ret))
			{
				$messages = array();

				foreach ($ret as $sh => $r)
				{
					$ids_arr = explode(',', $r['reason_ids']);
					$message = '';

					foreach ($ids_arr as $id)
					{
						if ($id == 4)
							$message .= $r['reason_text'] . '<br><br>';
						else
							$message .= $this->view->t('verify_reject_reason_' . $id) . '<br><br>';
					}

					$messages[$sh] = $message;
				}

				$this->view->message = $messages;
				$this->view->type = 'agency';
			}
			else
				die;
		}
		else
			die;
	}

	public function setRejectedVerificationAction()
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		if ($this->user->isEscort())
		{
			$escort_id = $this->user->getEscort()->id;
			$client->call('Escorts.setRejectedVerificationForEscort', array($escort_id));
		}
		elseif ($this->user->isAgency())
		{
			$user_id = $this->user->id;
			$client->call('Escorts.setRejectedVerificationForAgency', array($user_id));
		}

		die;
	}

	public function ajaxUpdateShowOnlineStatusAction()
	{
		$flag = (int) $this->_request->flag;

		if ( ! in_array($flag, array(0, 1)) ) die(')');

		if ( $flag == 1 ) {
			$flag = 0;
		} else {
			$flag = 1;
		}

		$this->user->updateShowOnline($flag);

		die;
	}

	public function updateWatchedTypeAction()
	{
		if ( $this->_request->isPost() ) {

			$validator = new Cubix_Validator();

			$escorts_watched_type = intval($this->_request->escorts_watched_type);

			if ($escorts_watched_type < 1) $escorts_watched_type = 1;

			$modelM = new Model_Members();
			$modelM->updateWatchedType($this->user->id, $escorts_watched_type);
			$user = Model_Users::getCurrent();
			$user->escorts_watched_type = $escorts_watched_type;
			Model_Users::setCurrent($user);

			$status = $validator->getStatus();
			die(json_encode($status));
		}

		die;
	}

	public function changePassAction()
	{

		if ($this->_request->ajax) {
			$this->view->layout()->disableLayout();
		} else {
			$this->view->layout()->setLayout('main');
		}

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->_request->isPost() ) {
			$pass = $this->_getParam('password');
			$conf_pass = $this->_getParam('password2');
			$hash = $this->_getParam('hash');
			$id = intval($this->_getParam('id'));
			$validator = new Cubix_Validator();
			if ( strlen($pass) < 6 ) {
				$validator->setError('password', __('password_invalid'));
			}
			elseif ( $pass != $conf_pass ) {
				$validator->setError('password2',  __('password_missmatch'));
			}
			elseif ( ! preg_match('/[a-f0-9]{32}/', $hash) ) {
				$validator->setError('hash',  'Invalid hash !');
			}
			$this->view->errors = array();
			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				$this->view->errors = $result['msgs'];
			}
			else{
				$client->call('Users.checkUpdatePassword', array($id, $hash, $pass));

				$this->view->ok = true;
			}
		}
		else
		{
			$hash = $this->_getParam('hash');
			$username = $this->_getParam('username');
			$error = false;
			if(!isset($username) || !isset($hash)){
				$error = true;
			}
			$username = substr($username, 0, 24);
			if ( strlen($username) < 6 ) {
				$error = true;
			}
			elseif ( ! preg_match('/^[-_a-z0-9]+$/i', $username) ) {
				$error = true;
			}
			elseif ( ! preg_match('/[a-f0-9]{32}/', $hash) ) {
				$error = true;
			}

			if(!$error)
			{
				$id = $client->call('Users.getByUsernameMailHash', array($username, $hash));
				if($id){
					$this->view->id = $id;
					$this->view->hash = $hash;

				}
				else{
					$error = true;
				}

			}
			if ($error){
				$this->_response->setRedirect($this->view->getLink('signin'));
			}
		}
	}
}
