<?php

/**
 * Class Controller_Helper_Acl
 *
 * @property Zend_Acl $acl
 */
class Controller_Helper_Acl
{
    public $acl;

    public function __construct()
    {
        $this->acl = new Zend_Acl();
    }

    public function setRoles()
    {
        $this->acl->addRole(new Zend_Acl_Role('guest'));
        $this->acl->addRole(new Zend_Acl_Role('escort'), 'guest');
        $this->acl->addRole(new Zend_Acl_Role('agency'), 'guest');
        $this->acl->addRole(new Zend_Acl_Role('member'), 'guest');
    }

    public function setResources()
    {
        // Adding resources action-wise
        // that means, these actions of "every controller" are going to get validated
        // for access for guest or editor or admin users (roles) etc.
        // setting up access control as per controller
        // i.e. access control will work for these controllers only.
        // Suppose I want to control the access of only 2 controllers,
        // for ex. for NewsController and JobBoardController
        // you can also fetch these values from DB as done for roles
        // as above in function setRoles().
        $this->acl->add(new Zend_Acl_Resource('index'));
        $this->acl->add(new Zend_Acl_Resource('error'));
        $this->acl->add(new Zend_Acl_Resource('account'));
        $this->acl->add(new Zend_Acl_Resource('agencies'));
        $this->acl->add(new Zend_Acl_Resource('comments'));
        $this->acl->add(new Zend_Acl_Resource('escorts'));
        $this->acl->add(new Zend_Acl_Resource('reviews'));
        $this->acl->add(new Zend_Acl_Resource('private_test'));

    }

    public function setPrivilages()
    {

        // Setting privilages for actions of all controllers
        // $this->acl->allow('guest',null, array('view', 'index'));
        // $this->acl->allow('editor',array('view','edit'));
        // $this->acl->allow('admin');

        // Setting privileges for actions as per particular controller
        // $this->acl->allow('<role>','<controller>', <array of controller actions>);
        // Privileges for guest users
        $this->acl->allow('guest', 'error');
        $this->acl->allow('guest', 'account');
        $this->acl->allow('guest', 'index');
//        $this->acl->deny('guest', 'private_test'); if not set it
        $this->acl->allow('guest', 'escorts');
        $this->acl->allow('guest', 'agencies');
        $this->acl->allow('guest', 'comments');
        $this->acl->allow('guest', 'reviews');


        //Privileges for members
        $this->acl->allow('member', 'private_test');

        //Privileges for escorts
        $this->acl->allow('escort', 'private_test');

        //Privileges for Agencies
        $this->acl->allow('agency', 'private_test');

    }

    public function setAcl()
    {
        // store acl object using Zend_Registry for future use. This is compulsory.
        Zend_Registry::set('acl', $this->acl);
    }
}