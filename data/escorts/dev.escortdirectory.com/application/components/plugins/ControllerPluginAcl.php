<?php

/**
 * Class Controller_Plugin_Acl
 */
class Controller_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{
    // Correct consecutive slashes in the URL.
    /*public function routeStartup(Zend_Controller_Request_Abstract $request)
    {
        $uri = $request->getRequestUri();
        $correctedUri = preg_replace('/\/{2,}/', '/', $uri);
        if ($uri != $correctedUri) {
            $request->setRequestUri($correctedUri);
        }
    }*/

    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        /* @var $acl Zend_Acl*/
        $acl= Zend_Registry::get('acl');

        $user= Model_Users::getCurrent();
        if(!$user)
        {
            $roleName='guest';
        }
        else
        {
            $roleName=$user->user_type;
        }

        $privilageName=$request->getActionName();
        $controllerName=$request->getControllerName();
        $moduleName = $request->getModuleName();
        if ($moduleName && $moduleName != 'default'){

            $controllerName = $moduleName.'_'.$controllerName;
        }
//        var_dump($acl->isAllowed($roleName,$controllerName,$privilageName)); die;
        if ($acl->has($controllerName)){
            if(!$acl->isAllowed($roleName,$controllerName,$privilageName))
            {
                $request->setModuleName('default');
                $request->setControllerName('Error');
                $request->setActionName('error');
                $request->setParam('error_type','permission_denied');
            }
        }
    }
}