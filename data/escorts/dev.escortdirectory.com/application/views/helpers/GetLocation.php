<?php 
class Zend_View_Helper_GetLocation {

	public function getLocation($latitude, $longitude){
		$conf = Zend_Registry::get('api_config');
		$api_key = $conf['googleMaps'];
		

		$geocoding_url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $latitude . ',' . $longitude . '&key=' . $api_key;
		
		$ch = curl_init($geocoding_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);

		curl_close($ch);

		return json_decode($result);
	}

}