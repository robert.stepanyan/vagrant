<?php

class Zend_View_Helper_AgencyEscorts extends Zend_View_Helper_Abstract
{
	public function agencyEscorts($escorts)
	{
		$this->view->a_escorts = $escorts;

		return $this->view->render('agencies/agency-escorts.phtml');
	}
}
