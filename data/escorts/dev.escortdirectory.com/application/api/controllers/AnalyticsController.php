<?php

class Api_AnalyticsController extends Zend_Controller_Action
{
    /**
     * @var Zend_Db_Adapter_Mysqli
     */
    protected $_db;

    /**
     * @var Cubix_Api
     */
    protected $_client;

    public function init()
    {
        // Getting DB instance
        // ---------------------------
        $this->_db = Zend_Registry::get('db');
        // ---------------------------

        // Prevent actions from trying to show any view
        // -------------------------------
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        // -------------------------------
    }

    /**
     * @var Cubix_Cli
     */
    private $_cli;

    /**
     * This action retrievers data from redis
     *  and stores them into DB (MySQL)
     */
    public function saveCachedViewsAction()
    {
        // Init cli
        // --------------------------------
        $cli = $this->_cli = new Cubix_Cli();
        $cli->clear();
        // --------------------------------

        $cli->out('Fetching cached views from Redis ...');
        // Getting the redis instance
        // -------------------------------
        $redis = Zend_Registry::get('redis');
        // -------------------------------

        // Loop trough every type of visit action
        // -----------------------------------------
        $types = ['profile-view', 'listing-view', 'city-view'];
        $prefix = Cubix_Application::getId();
        foreach ($types as $type) {

            // Getting specific keys from Redis
            // --------------------------
            $views = $redis->keys($prefix . ':' . $type . ':*');
            // --------------------------

            // Parse the key, and fill array with escort's data
            // -----------------------------------
            $escorts = [];
            foreach ($views as $view) {
                @list($app_id, $action, $escort_id, $visitor, $city) = explode(':', $view);
                $escorts[$escort_id] = isset($escorts[$escort_id]) ? intval($escorts[$escort_id]) + 1 : 1;
            }
            // -----------------------------------

            // Generating table name with prefix, plurals and others
            // ----------------------------------
            $table = 'escort_' . str_replace('-', '_', $type) . 's_per_week';
            // ----------------------------------

            $date = date('Y-m-d');

            // Printing to console
            // ---------------------------
            $cli->out('Inserting into table ' . $table);
            $cli->out('Escourts: ' . count($escorts));
            // ---------------------------

            foreach ($escorts as $id => $views_count) {

                // In db we store amount of views for every day
                // so this is just retrieving escort's current visit data
                // ------------------------------------------------------------
                $todays_record = $this->_db->fetchRow("SELECT id FROM $table WHERE escort_id = ? and `date` = ? " , [$id, $date]);
                // ------------------------------------------------------------

                // If cron didn't work today yet, insert new row
                // ---------------------------------
                if(empty($todays_record)) {
                    $row = [
                        'escort_id' => $id,
                        'views_count' => 1,
                        'date' => $date,
                    ];

                    // Only for city-view table we need to specify this
                    // ---------------------------------
                    if($type == 'city-view' ) {
                        $row['city_id'] = $city;
                    }
                    // ---------------------------------

                    // Inserting
                    // --------------------
                    $this->_db->insert($table, $row);
                    // --------------------

                }else{

                    // If cron already inserted today some date,
                    // we just increment amount of visits
                    // --------------------------------------
                    $this->_db->update($table, [
                        'views_count' => new Zend_Db_Expr('views_count + ' . $views_count),
                        'date' => $date
                    ], $this->_db->quoteInto('id = ?', $todays_record->id));
                    // --------------------------------------
                }
            }

            // Clear the data from Redis, that is now in DB
            // -----------------------------------------
            $redis->delete($views);
            // -----------------------------------------
        }
        // -----------------------------------------

        $cli->out('Done ! :)');
        return;
    }

    /**
     * This action removes views that are older than 7 days
     */
    public function removeOldViewsAction () {

        // Init cli
        // --------------------------------
        $cli = $this->_cli = new Cubix_Cli();
        $cli->clear();
        // --------------------------------

        // Printing out to the console
        // -------------------------
        $cli->out('Removing old views (older than 7 days)');
        // -------------------------

        // Loop trough every type of action
        // -------------------------------------------
        $types = ['profile-view', 'listing-view', 'city-view'];
        $prefix = Cubix_Application::getId();
        foreach ($types as $type) {

            // Generate table name, with prefix, plurals and others
            // -----------------------------------------------------
            $table = 'escort_' . str_replace('-', '_', $type) . 's_per_week';
            // -----------------------------------------------------

            // Remove every row, that is older than 7 days
            // -------------------------------
            $this->_db->query("DELETE FROM $table WHERE `date` < NOW() - INTERVAL 1 WEEK  ");
            // -------------------------------
        }
        // -------------------------------------------

        $cli->out('Done !');

        return;
    }


}