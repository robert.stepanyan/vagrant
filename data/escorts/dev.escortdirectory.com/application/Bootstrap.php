<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    public function __construct($application)
    {
        require '../application/models/Plugin/Activity.php';
        require '../application/components/helpers/ControllerHelperAcl.php';
        require '../application/components/plugins/ControllerPluginAcl.php';

        parent::__construct($application);

    }

    public function run()
    {

        //Zend_Layout::startMvc();

        // Initialize hooks to avoid code confusion
        Model_Hooks::init();

        parent::run();
    }

    protected function _initRoutes()
    {
        require '../../library/Cubix/Security/Plugin.php';
        require '../application/models/Plugin/I18n.php';

        $this->bootstrap('frontController');
        $front = $this->getResource('frontController');
        $front->addModuleDirectory(APPLICATION_PATH . '/modules');

        $router = $front->getRouter();

        $router->addRoute(
            'video_manifest',
            new Zend_Controller_Router_Route('manifest',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'manifest',
                )), array(

            )
        );

        $router->addRoute(
            'video',
            new Zend_Controller_Router_Route_Regex('^(video|Video)'
                ,
                array(
                    'module' => 'default',
                    'controller' => 'video',
                    'action' => 'index',
                    'lang_id' => '',

                ), array(
                    'lang_id' => '[a-z]{2}',
                )
            )
        );

        $router->addRoute(
            'video_upload',
            new Zend_Controller_Router_Route_Regex('^(video|Video)/upload',
                array(
                    'module' => 'default',
                    'controller' => 'video',
                    'action' => 'upload',
                    'lang_id' => '',

                ), array(
                    'lang_id' => '[a-z]{2}',
                )
            )
        );

        $router->addRoute(
            'account-actions',
            new Zend_Controller_Router_Route('/account/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'account',
                ))
        );

        $router->addRoute(
            'account-change-pass',
            new Zend_Controller_Router_Route_Regex('(([a-z]{2})/)?account/change-pass/([-_a-z0-9]+)/([a-f0-9]{32})',
                array(
                    'module' => 'default',
                    'controller' => 'account',
                    'action' => 'change-pass',
                    'lang_id' => null,
                ), array(
                    2 => 'lang_id',
                    3 => 'username',
                    4 => 'hash',
                ))
        );

        $router->addRoute(
            'account-activate',
            new Zend_Controller_Router_Route_Regex('(.+)?/account/activate/(.+)?/([a-f0-9]{32})',
                array(
                    'module' => 'default',
                    'controller' => 'account',
                    'action' => 'activate',
                ), array(
                    1 => 'user_type',
                    2 => 'email',
                    3 => 'hash',
                ))
        );

        $router->addRoute(
            'video_remove',
            new Zend_Controller_Router_Route_Regex('^(video|Video)/remove',
                array(
                    'module' => 'default',
                    'controller' => 'video',
                    'action' => 'remove',
                    'lang_id' => '',

                ), array(
                    'lang_id' => '[a-z]{2}',
                )
            )
        );
        $router->addRoute(
            'get_video_escorts',
            new Zend_Controller_Router_Route_Regex('^(video|Video)/getvideo',
                array(
                    'module' => 'default',
                    'controller' => 'video',
                    'action' => 'getvideo',
                    'lang_id' => '',

                ), array(
                    'lang_id' => '[a-z]{2}',
                )
            )
        );

        $router->addRoute(
            'home-page',
            new Zend_Controller_Router_Route(
                ':lang_id/*',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'index',
                    'lang_id' => '',
                ), array(
                    'lang_id' => '[a-z]{2}',
                )
            )
        );

        /*$router->addRoute(
        'escorts-filter-def',
        new Zend_Controller_Router_Route_Regex(
        '^escorts(/.+)?',
        array(
        'module' => 'default',
        'controller' => 'escorts',
        'action' => 'index'
        ),
        array(
        1 => 'req'
        )
        )
        );*/

        $router->addRoute(
            'escorts-filter-def',
            new Zend_Controller_Router_Route_Regex(
                // ATTENTION that all the categories that include "-" must be first EXAMPLE "BDSM-TRANS" then "TRANS" or REGEX login won't match
                '^(([a-z]{2})/)?(newest-independent|newest-agency-escorts|boys-homosexual|bdsm-trans|bdsm-boys|massage-trans|massage-boys|boys-heterosexual|agency-escorts|boys-bisexual|independent-escorts|escorts-only|newest|citytours|upcomingtours|escorts|bdsm|boys|trans|massage)(-(.+)-([0-9]+))?',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'index',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    5 => 'p_city_slug',
                    6 => 'p_city_id',
                )
            )
        );

        $router->addRoute(
            'view-alert',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?(view-alert)/([0-9]+)?',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'index',
                    'p_top_category' => 'view-alert',
                    'lang_id' => null,
                ),
                array(
                    4 => 'alert_id',
                )
            )
        );

        $router->addRoute(
            'escorts-filter-def-page',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?(newest-independent|newest-agency-escorts|newest|citytours|upcomingtours|escorts-only|escorts|independent-escorts|agency-escorts|bdsm-boys|bdsm-trans|bdsm|boys-heterosexual|boys-bisexual|boys-homosexual|boys|trans|massage-boys|massage-trans|massage)-([0-9]+)',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'index',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    4 => 'p_page',
                )
            )
        );

        $router->addRoute(
            'get-filter-v2',
            new Zend_Controller_Router_Route_Regex('(([a-z]{2})/)?escorts/get-filter-v2',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'get-filter-v2',
                ), array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'escorts-filter-city-page',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?(newest-independent|newest-agency-escorts|newest|citytours|upcomingtours|escorts-only|escorts|independent-escorts|agency-escorts|bdsm-boys|bdsm-trans|bdsm|boys-heterosexual|boys-bisexual|boys-homosexual|boys|trans|massage-boys|massage-trans|massage)(-(.+)-([0-9]+)-([0-9]+))?',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'index',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    5 => 'p_city_slug',
                    6 => 'p_city_id',
                    7 => 'p_page',
                )
            )
        );

        /* CLASSIFIED ADS */
        $router->addRoute(
            'classified-ads',
            new Zend_Controller_Router_Route(':lang_id/classified-ads/:action',
                array(
                    'module' => 'default',
                    'controller' => 'classified-ads',
                    'action' => 'index',
                )), array(
                'lang_id' => '[a-z]{2}',
            )
        );

        $router->addRoute(
            'classified-ads-def',
            new Zend_Controller_Router_Route('classified-ads/:action',
                array(
                    'module' => 'default',
                    'controller' => 'classified-ads',
                    'action' => 'index',
                )), array(

            )
        );

        $router->addRoute(
            'classified-ads-details',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?classified-ads/(.+)?/ad/([0-9]+)',
                array(
                    'module' => 'default',
                    'controller' => 'classified-ads',
                    'action' => 'ad',
                ),
                array(
                    2 => 'lang_id',
                    3 => 'slug',
                    4 => 'id',
                )
            )
        );

        $router->addRoute(
            'classified-ads-paging',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?classified-ads/([0-9]+)',
                array(
                    'module' => 'default',
                    'controller' => 'classified-ads',
                    'action' => 'index',
                ),
                array(
                    2 => 'lang_id',
                    3 => 'page',
                )
            )
        );

        $router->addRoute(
            'online-escorts',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?online-(escorts|independent-escorts|agency-escorts)(-(.+)-c([0-9]+)-([0-9]+))?',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'index',
                    'online_now' => 1,
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                )
            )
        );

        $router->addRoute(
            'online-escorts',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?online-(escorts|independent-escorts|agency-escorts)(-(.+)-c([0-9]+))?',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'index',
                    'online_now' => 1,
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                )
            )
        );

        $router->addRoute(
            'online-escorts-city-def',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?online-(escorts|independent-escorts|agency-escorts)(-(.+)-([0-9]+))?',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'index',
                    'online_now' => 1,
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                )
            )
        );


        $router->addRoute(
            'agencies-show-tool-tip-def',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?agencies/show-tool-tip',
                array(
                    'module' => 'default',
                    'controller' => 'agencies',
                    'action' => 'show-tool-tip',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'agencies-filter-def',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?(agencies)(-(.+)-([0-9]+))?',
                array(
                    'module' => 'default',
                    'controller' => 'agencies',
                    'action' => 'list',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    5 => 'p_city_slug',
                    6 => 'p_city_id',
                )
            )
        );

        $router->addRoute(
            'agencies-filter-def-page',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?(agencies)-([0-9]+)',
                array(
                    'module' => 'default',
                    'controller' => 'agencies',
                    'action' => 'list',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    4 => 'page',
                )
            )
        );

        $router->addRoute(
            'agencies-filter-city-page',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?(agencies)(-(.+)-([0-9]+)-([0-9]+))?',
                array(
                    'module' => 'default',
                    'controller' => 'agencies',
                    'action' => 'list',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    5 => 'p_city_slug',
                    6 => 'p_city_id',
                    7 => 'page',
                )
            )
        );

        $router->addRoute(
            'agencies-countries-filter-def',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?(agencies)(-(.+)-c([0-9]+))?',
                array(
                    'module' => 'default',
                    'controller' => 'agencies',
                    'action' => 'list',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    5 => 'p_country_slug',
                    6 => 'p_country_id',
                )
            )
        );

        $router->addRoute(
            'agencies-countries-filter-def-page',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?(agencies)(-(.+)-c([0-9]+)-([0-9]+))?',
                array(
                    'module' => 'default',
                    'controller' => 'agencies',
                    'action' => 'list',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    5 => 'p_country_slug',
                    6 => 'p_country_id',
                    7 => 'page',
                )
            )
        );

        $router->addRoute(
            'online-agencies',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?online-(agencies)',
                array(
                    'module' => 'default',
                    'controller' => 'agencies',
                    'action' => 'list',
                    'online_now' => 1,
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                )
            )
        );

        $router->addRoute(
            'escorts-countries-filter-def',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?(newest-independent|newest-agency-escorts|newest|citytours|upcomingtours|escorts-only|escorts|independent-escorts|agency-escorts|bdsm-boys|bdsm-trans|bdsm|boys-heterosexual|boys-bisexual|boys-homosexual|boys|trans|massage-boys|massage-trans|massage)(-(.+)-c([0-9]+))?',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'index',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    5 => 'p_country_slug',
                    6 => 'p_country_id',
                )
            )
        );

        $router->addRoute(
            'escorts-countries-filter-def-page',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?(newest-independent|newest-agency-escorts|newest|citytours|upcomingtours|escorts-only|escorts|independent-escorts|agency-escorts|bdsm-boys|bdsm-trans|bdsm|boys-heterosexual|boys-bisexual|boys-homosexual|boys|trans|massage-boys|massage-trans|massage)(-(.+)-c([0-9]+)-([0-9]+))?',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'index',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'p_top_category',
                    5 => 'p_country_slug',
                    6 => 'p_country_id',
                    7 => 'p_page',
                )
            )
        );

        $router->addRoute(
            'external-link',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?go',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'go',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'loadFilterCities',
            new Zend_Controller_Router_Route_Regex(
                'escorts/ajax-get-filter-cities',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'ajax-get-filter-cities',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'loadFilterRegions',
            new Zend_Controller_Router_Route_Regex(
                'escorts/ajax-get-filter-regions',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'ajax-get-filter-regions',
                    'lang_id' => null,
                )
            )
        );

        $router->addRoute(
            'links',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?links',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'links',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'bubbles',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?ajax-bubble',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'ajax-bubble',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'blog',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog',
                array(
                    'module' => 'default',
                    'controller' => 'blog',
                    'action' => 'index',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'blog-top10',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/top10',
                array(
                    'module' => 'default',
                    'controller' => 'blog',
                    'action' => 'top10',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'blog-details',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/(.+)?\-([0-9]+)',
                array(
                    'module' => 'default',
                    'controller' => 'blog',
                    'action' => 'details',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'slug',
                    4 => 'id',
                )
            )
        );

        $router->addRoute(
            'blog-category',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog-category/(.+)?\-([0-9]+)',
                array(
                    'module' => 'default',
                    'controller' => 'blog',
                    'action' => 'category',
                ),
                array(
                    2 => 'lang_id',
                    3 => 'slug',
                    4 => 'id',
                )
            )
        );

        $router->addRoute(
            'blog-archive',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/([.0-9]+)',
                array(
                    'module' => 'default',
                    'controller' => 'blog',
                    'action' => 'index',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'date',
                )
            )
        );

        $router->addRoute(
            'blog-ajax-posts',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/ajax-posts', //(/[0-9]+)?
                array(
                    'module' => 'default',
                    'controller' => 'blog',
                    'action' => 'ajax-posts',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'date',
                )
            )
        );

        $router->addRoute(
            'blog-ajax-comments',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/ajax-comments',
                array(
                    'module' => 'default',
                    'controller' => 'blog',
                    'action' => 'ajax-comments',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'blog-ajax-add-comment',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/ajax-add-comment',
                array(
                    'module' => 'default',
                    'controller' => 'blog',
                    'action' => 'ajax-add-comment',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'blog-ajax-add-reply',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/ajax-add-reply',
                array(
                    'module' => 'default',
                    'controller' => 'blog',
                    'action' => 'ajax-add-reply',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'blog-ajax-replies',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?blog/ajax-replies',
                array(
                    'module' => 'default',
                    'controller' => 'blog',
                    'action' => 'ajax-replies',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'last-viewed-escorts',
            new Zend_Controller_Router_Route_Regex(
                '^([a-z]{2})/escorts/viewed-escorts',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'viewed-escorts',
                ),
                array(
                    1 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'last-viewed-escorts-def',
            new Zend_Controller_Router_Route_Regex(
                '^escorts/viewed-escorts',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'viewed-escorts',
                )
            )
        );

        $router->addRoute(
            'reviews',
            new Zend_Controller_Router_Route('reviews/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'reviews',
                    'action' => 'index',
                ))
        );

        $router->addRoute(
            'reviews-lng',
            new Zend_Controller_Router_Route(':lang_id/reviews/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'reviews',
                    'action' => 'index',
                    'lang_id' => null,
                ),
                array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'reviews-escort',
            new Zend_Controller_Router_Route_Regex('/^(([a-z]{2})/)?reviews/(.+)?\-([0-9]+)',
                array(
                    'module' => 'default',
                    'controller' => 'reviews',
                    'action' => 'escort',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'showname',
                    4 => 'escort_id',
                )
            )
        );

        $router->addRoute(
            'reviews-escort-review',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?reviews/(.+)?\-([0-9]+)/([0-9]+)',
                array(
                    'module' => 'default',
                    'controller' => 'reviews',
                    'action' => 'escort-review',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'showname',
                    4 => 'escort_id',
                    5 => 'review_id',
                )
            )
        );

        $router->addRoute(
            'reviews-member',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?reviews/member/(.+)?',
                array(
                    'module' => 'default',
                    'controller' => 'reviews',
                    'action' => 'index',
                    'mode' => 'exact',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'member',
                )
            )
        );

        $router->addRoute(
            'latest-actions',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?latest-actions',
                array(
                    'module' => 'default',
                    'controller' => 'latest-actions',
                    'action' => 'index',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'latest-actions-ajax-get-details',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?latest-actions/ajax-get-details',
                array(
                    'module' => 'default',
                    'controller' => 'latest-actions',
                    'action' => 'ajax-get-details',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'latest-actions-ajax-list',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?latest-actions/ajax-list',
                array(
                    'module' => 'default',
                    'controller' => 'latest-actions',
                    'action' => 'ajax-list',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'latest-actions-ajax-header',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?latest-actions/ajax-header',
                array(
                    'module' => 'default',
                    'controller' => 'latest-actions',
                    'action' => 'ajax-header',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        /*PHOTO FEED*/
        $router->addRoute(
            'photo-feed',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?photo-feed',
                array(
                    'module' => 'default',
                    'controller' => 'photo-feed',
                    'action' => 'index',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'photo-feed-ajax-list',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?photo-feed/ajax-list',
                array(
                    'module' => 'default',
                    'controller' => 'photo-feed',
                    'action' => 'ajax-list',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'photo-feed-ajax-header',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?photo-feed/ajax-header',
                array(
                    'module' => 'default',
                    'controller' => 'photo-feed',
                    'action' => 'ajax-header',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'photo-feed-ajax-voting-box',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?photo-feed/ajax-voting-box/([0-9]+)/([0-9]+)',
                array(
                    'module' => 'default',
                    'controller' => 'photo-feed',
                    'action' => 'ajax-voting-box',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'escort_id',
                    4 => 'photo_id',
                ))
        );
        $router->addRoute(
            'photo-feed-ajax-more-photos',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?photo-feed/ajax-get-more-photos/([0-9]+)/([0-9]+)/([0-9]+)',
                array(
                    'module' => 'default',
                    'controller' => 'photo-feed',
                    'action' => 'ajax-get-more-photos',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'escort_id',
                    4 => 'exclude_photo_id',
                    5 => 'page',
                ))
        );

        $router->addRoute(
            'photo-feed-ajax-vote',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?photo-feed/ajax-vote/([0-9]+)/([0-9]+)/([0-9]+)',
                array(
                    'module' => 'default',
                    'controller' => 'photo-feed',
                    'action' => 'ajax-vote',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'escort_id',
                    4 => 'photo_id',
                    5 => 'rate',
                ))
        );
        /*PHOTO FEED*/

        $router->addRoute(
            'captcha',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?captcha',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'captcha',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'static-pages-def',
            new Zend_Controller_Router_Route_Regex(
                '^page/(.+)?',
                array(
                    'module' => 'default',
                    'controller' => 'static-page',
                    'action' => 'show',
                ),
                array(
                    1 => 'page_slug',
                )
            )
        );

        $router->addRoute(
            'escort-photos-def',
            new Zend_Controller_Router_Route_Regex(
                '^escorts/photos',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'photos',
                )
            )
        );

        $router->addRoute(
            'escort-private-photos-def',
            new Zend_Controller_Router_Route_Regex(
                '^escorts/private-photos',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'private-photos',
                )
            )
        );

        $router->addRoute(
            'agency-profile-def',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?agency/(.+)?\-([0-9]+)$',
                array(
                    'module' => 'default',
                    'controller' => 'agencies',
                    'action' => 'show',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'agencyName',
                    4 => 'agency_id',
                )
            )
        );

        $router->addRoute(
            'agency-escorts-list',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?agency/(.+)?\-([0-9]+)/escorts',
                array(
                    'module' => 'default',
                    'controller' => 'agencies',
                    'action' => 'escorts',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'agencyName',
                    4 => 'agency_id',
                )
            )
        );

        $router->addRoute(
            'agency-ajax-escorts',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?agencies/ajax-escorts',
                array(
                    'module' => 'default',
                    'controller' => 'agencies',
                    'action' => 'ajax-escorts',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'escort-profile-v2-def',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?escort/(.+)?\-([0-9]+)$',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'profile-v2',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'escortName',
                    4 => 'escort_id',
                )
            )
        );

        $router->addRoute(
            'bdn-page',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?bdn-page/(.+)?\-([0-9]+)$',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'banner-page',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'escortName',
                    4 => 'escort_id',
                )
            )
        );

        $router->addRoute(
            'geography',
            new Zend_Controller_Router_Route(':lang_id/geography/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'geography',
                    'action' => 'index',
                ), array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'geography-def',
            new Zend_Controller_Router_Route('geography/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'geography',
                    'action' => 'index',
                ))
        );

//        $router->addRoute(
        //            'private',
        //            new Zend_Controller_Router_Route(':lang_id/private/:action/*',
        //            array(
        //                'module' => 'default',
        //                'controller' => 'private',
        //                'action' => 'index'
        //            ), array(
        //                'lang_id' => '[a-z]{2}'
        //            ))
        //        );
        // $router->addRoute(
        //     'private-def',
        //     new Zend_Controller_Router_Route('private/:action/*',
        //     array(
        //         'module' => 'private',
        //         'controller' => 'index',
        //         'action' => 'index'
        //     ))
        // );

        // $router->addRoute(
        //     'private-actions',
        //     new Zend_Controller_Router_Route('private/:action/*',
        //     array(
        //         'module' => 'private',
        //         'controller' => 'index',
        //         'action' => 'index'
        //     ))
        // );

//        $router->addRoute(
        //            'privateV2Lng',
        //            new Zend_Controller_Router_Route(':lang_id/private-v2/:action/*',
        //            array(
        //                'module' => 'default',
        //                'controller' => 'private-v2',
        //                'action' => 'index',
        //                'lang_id' => ''
        //            ),
        //            array(
        //                'lang_id' => '[a-z]{2}'
        //            ))
        //        );

//        $router->addRoute(
        //            'privateV2',
        //            new Zend_Controller_Router_Route('private-v2/:action/*',
        //            array(
        //                'module' => 'default',
        //                'controller' => 'private-v2',
        //                'action' => 'index'
        //            ))
        //        );

        // $router->addRoute(
        //     'privateProfileLng',
        //     new Zend_Controller_Router_Route(':lang_id/private-v2/profile/:action/*',
        //     array(
        //         'module' => 'default',
        //         'controller' => 'profile',
        //         'action' => 'index',
        //         'lang_id' => ''
        //     ),
        //     array(
        //         'lang_id' => '[a-z]{2}'
        //     ))
        // );

        // $router->addRoute(
        //     'privateProfile',
        //     new Zend_Controller_Router_Route('private-v2/profile/:action/*',
        //     array(
        //         'module' => 'default',
        //         'controller' => 'profile',
        //         'action' => 'index'
        //     ))
        // );

        $router->addRoute(
            'robots',
            new Zend_Controller_Router_Route('/robots.txt',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'robots',
                ))
        );


        $router->addRoute(
            'privateV2Lng',
            new Zend_Controller_Router_Route(':lang_id/(private|dashboard)/:action/*',
                array(
                    'module' => 'private',
                    'controller' => 'index',
                    'action' => 'index',
                    'lang_id' => '',
                ),
                array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        // Private Area
        // ---------------------------------------------------------------
        $router->addRoute(
            'private-area-dashboard-lng',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?(dashboard|private)',
                array(
                    'module' => 'private',
                    'controller' => 'index',
                    'action' => 'index',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'privateV2Lng_new',
            new Zend_Controller_Router_Route_Regex(':lang_id/(private|dashboard)/',
                array(
                    'module' => 'private',
                    'controller' => 'index',
                    'action' => 'index',
                    'lang_id' => '',
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'private-with-lang',
            new Zend_Controller_Router_Route(':lang_id/private/:action/*',
                array(
                    'module' => 'private',
                    'controller' => 'index',
                ))
        );

        $router->addRoute(
            'private-without-lang',
            new Zend_Controller_Router_Route('/private/:action/*',
                array(
                    'module' => 'private',
                    'controller' => 'index',
                ))
        );

        $router->addRoute(
            'privateProfileLng',
            new Zend_Controller_Router_Route(':lang_id/private/profile/:action/*',
                array(
                    'module' => 'private',
                    'controller' => 'profile',
                    'action' => 'index',
                    'lang_id' => '',
                ),
                array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'privateProfile',
            new Zend_Controller_Router_Route('private/profile/:action/*',
                array(
                    'module' => 'private',
                    'controller' => 'profile',
                    'action' => 'index',
                ))
        );

        $router->addRoute(
            'private-video',
            new Zend_Controller_Router_Route('private/video/:action',
                array(
                    'module' => 'private',
                    'controller' => 'video',
                ))
        );

        $router->addRoute(
            'private-support',
            new Zend_Controller_Router_Route('private/support/:action',
                array(
                    'module' => 'private',
                    'controller' => 'support',
                ))
        );

        $router->addRoute(
            'private-verified-def',
            new Zend_Controller_Router_Route('private/verify/:action',
                array(
                    'module' => 'default',
                    'controller' => 'verify',
                    'action' => 'index',
                ))
        );
        $router->addRoute(
            'private-verified-lng',
            new Zend_Controller_Router_Route('private/:lang_id/verify/:action',
                array(
                    'module' => 'default',
                    'controller' => 'verify',
                    'action' => 'index',
                    'lang_id' => '',
                ), array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'private-tours',
            new Zend_Controller_Router_Route('private/tours/:action',
                array(
                    'module' => 'private',
                    'controller' => 'tours',
                ))
        );

        $router->addRoute(
            'private-billing',
            new Zend_Controller_Router_Route('private/billing/:action',
                array(
                    'module' => 'private',
                    'controller' => 'billing',
                ))
        );

        $router->addRoute(
            'private-billing-mmg',
            new Zend_Controller_Router_Route('/billing/:action',
                array(
                    'module' => 'private',
                    'controller' => 'billing',
                ))
        );
        // ---------------------------------------------------------------


        //

//        $router->addRoute(
        //            'private-activate',
        //            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?(.+)?/private/activate/(.+)?/([a-f0-9]{32})',
        //            array(
        //                'module' => 'default',
        //                'controller' => 'private',
        //                'action' => 'activate'
        //            ), array(
        //                2 => 'lang_id',
        //                3 => 'user_type',
        //                4 => 'email',
        //                5 => 'hash'
        //            ))
        //        );

        /*$router->addRoute(
        'private-activate-def',
        new Zend_Controller_Router_Route_Regex('(.+)?/private/activate/(.+)?/([a-f0-9]{32})',
        array(
        'module' => 'default',
        'controller' => 'private',
        'action' => 'activate'
        ), array(
        1 => 'user_type',
        2 => 'email',
        3 => 'hash'
        ))
        );*/

//        $router->addRoute(
        //            'private-change-pass',
        //            new Zend_Controller_Router_Route_Regex('(([a-z]{2})/)?private/change-pass/([-_a-z0-9]+)/([a-f0-9]{32})',
        //            array(
        //                'module' => 'default',
        //                'controller' => 'private',
        //                'action' => 'change-pass',
        //                'lang_id' => ''
        //            ), array(
        //                2 => 'lang_id',
        //                3 => 'username',
        //                4 => 'hash'
        //            ))
        //        );

//        $router->addRoute(
        //            'private-verified-def',
        //            new Zend_Controller_Router_Route('verify/:action',
        //            array(
        //                'module' => 'default',
        //                'controller' => 'verify',
        //                'action' => 'index'
        //            ))
        //        );
        $router->addRoute(
            'private-verified-lng',
            new Zend_Controller_Router_Route(':lang_id/verify/:action',
                array(
                    'module' => 'default',
                    'controller' => 'verify',
                    'action' => 'index',
                    'lang_id' => '',
                ), array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'city-alerts',
            new Zend_Controller_Router_Route('/city-alerts/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'city-alerts',
                    'action' => 'index',
                ))
        );
        $router->addRoute(
            'city-alerts-lng',
            new Zend_Controller_Router_Route(':lang_id/city-alerts/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'city-alerts',
                    'action' => 'index',
                    'lang_id' => '',
                ), array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'follow',
            new Zend_Controller_Router_Route('/follow/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'follow',
                    'action' => 'index',
                ))
        );

        $router->addRoute(
            'subscribe',
            new Zend_Controller_Router_Route('/subscribe/add',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'subscribe',
                ))
        );

        $router->addRoute(
            'follow-lng',
            new Zend_Controller_Router_Route(':lang_id/follow/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'follow',
                    'action' => 'index',
                    'lang_id' => '',
                ), array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'my-followers',
            new Zend_Controller_Router_Route('my-followers/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'my-followers',
                    'action' => 'index',
                ))
        );

        $router->addRoute(
            'my-followers-lng',
            new Zend_Controller_Router_Route(':lang_id/my-followers/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'my-followers',
                    'action' => 'index',
                    'lang_id' => '',
                ), array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'follow-confirm-email',
            new Zend_Controller_Router_Route_Regex('confirm-follow-email/(.+)?/([a-f0-9]{32})',
                array(
                    'module' => 'default',
                    'controller' => 'follow',
                    'action' => 'confirm-email',
                ), array(
                    1 => 'email',
                    2 => 'hash',
                ))
        );

        $router->addRoute(
            'feedback',
            new Zend_Controller_Router_Route(':lang_id/feedback',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'feedback',
                ), array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'feedback-def',
            new Zend_Controller_Router_Route('feedback',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'feedback',
                ))
        );

        $router->addRoute(
            'contact',
            new Zend_Controller_Router_Route(':lang_id/contact',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'contact',
                ), array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'contact-def',
            new Zend_Controller_Router_Route('contact',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'contact',
                ))
        );

        /* --> Support <-- */
        $router->addRoute(
            'support',
            new Zend_Controller_Router_Route(':lang_id/support/:action',
                array(
                    'module' => 'default',
                    'controller' => 'support',
                    'action' => 'index',
                ), array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'support-def',
            new Zend_Controller_Router_Route('support/:action',
                array(
                    'module' => 'default',
                    'controller' => 'support',
                    'action' => 'index',
                ))
        );
        /* <-- Support --> */

        $router->addRoute(
            'benchmark',
            new Zend_Controller_Router_Route('benchmark',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'benchmark',
                ))
        );

        $router->addRoute(
            'comments-ajax-list',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?comments/ajax-list',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'ajax-list',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'profile_comments',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?comments/profile-comments',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'profile-comments',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'profile_member_comments',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?comments/profile-member-comments',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'profile-member-comments',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'profile_agency_comments',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?comments/profile-agency-comments',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'profile-agency-comments',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'comments',
            new Zend_Controller_Router_Route(':lang_id/comments/ajax-show',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'ajax-show',
                )), array(
                'lang_id' => '[a-z]{2}',
            )
        );

        $router->addRoute(
            'comments-def',
            new Zend_Controller_Router_Route('comments/ajax-show',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'ajax-show',
                ))
        );

        $router->addRoute(
            'comments-v2',
            new Zend_Controller_Router_Route(':lang_id/comments-v2/ajax-show',
                array(
                    'module' => 'default',
                    'controller' => 'comments-v2',
                    'action' => 'ajax-show',
                )), array(
                'lang_id' => '[a-z]{2}',
            )
        );
        $router->addRoute(
            'comments-v2-def',
            new Zend_Controller_Router_Route('comments-v2/ajax-show',
                array(
                    'module' => 'default',
                    'controller' => 'comments-v2',
                    'action' => 'ajax-show',
                ))
        );

        $router->addRoute(
            'add-comment-v2',
            new Zend_Controller_Router_Route(':lang_id/comments-v2/ajax-add-comment',
                array(
                    'module' => 'default',
                    'controller' => 'comments-v2',
                    'action' => 'ajax-add-comment',
                )), array(
                'lang_id' => '[a-z]{2}',
            )
        );

        $router->addRoute(
            'add-agency-comment-v2',
            new Zend_Controller_Router_Route(':lang_id/comments-v2/ajax-add-agency-comment',
                array(
                    'module' => 'default',
                    'controller' => 'comments-v2',
                    'action' => 'ajax-add-agency-comment',
                )), array(
                'lang_id' => '[a-z]{2}',
            )
        );

        $router->addRoute(
            'add-comment-v2-def',
            new Zend_Controller_Router_Route('comments-v2/ajax-add-comment',
                array(
                    'module' => 'default',
                    'controller' => 'comments-v2',
                    'action' => 'ajax-add-comment',
                ))
        );

        $router->addRoute(
            'add-agency-comment-v2-def',
            new Zend_Controller_Router_Route('comments-v2/ajax-add-agency-comment',
                array(
                    'module' => 'default',
                    'controller' => 'comments-v2',
                    'action' => 'ajax-add-agency-comment',
                ))
        );

        $router->addRoute(
            'add-comment',
            new Zend_Controller_Router_Route(':lang_id/comments/ajax-add-comment',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'ajax-add-comment',
                )), array(
                'lang_id' => '[a-z]{2}',
            )
        );

        $router->addRoute(
            'add-comment-def',
            new Zend_Controller_Router_Route('comments/ajax-add-comment',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'ajax-add-comment',
                ))
        );

        $router->addRoute(
            'vote-comment',
            new Zend_Controller_Router_Route(':lang_id/comments-v2/ajax-vote',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'ajax-vote',
                )), array(
                'lang_id' => '[a-z]{2}',
            )
        );

        $router->addRoute(
            'vote-comment-def',
            new Zend_Controller_Router_Route('comments-v2/ajax-vote',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'ajax-vote',
                ))
        );

        $router->addRoute(
            'vote-comment-agency',
            new Zend_Controller_Router_Route(':lang_id/comments-v2/ajax-vote-agency',
                array(
                    'module' => 'default',
                    'controller' => 'comments-v2',
                    'action' => 'ajax-vote-agency',
                )), array(
                'lang_id' => '[a-z]{2}',
            )
        );

        $router->addRoute(
            'vote-comment-agency-def',
            new Zend_Controller_Router_Route('comments-v2/ajax-vote-agency',
                array(
                    'module' => 'default',
                    'controller' => 'comments-v2',
                    'action' => 'ajax-vote-agency',
                ))
        );

        $router->addRoute(
            'member-info',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?member/(.+)?',
                array(
                    'module' => 'default',
                    'controller' => 'members',
                    'action' => 'index',
                ), array(
                    2 => 'lang_id',
                    3 => 'username',

                )
            )
        );

        $router->addRoute(
            'member-not-found',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?member/not-found',
                array(
                    'module' => 'default',
                    'controller' => 'error',
                    'action' => 'index',
                    'error_type' => 'member-not-found',
                ), array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'member-info-actions-lng',
            new Zend_Controller_Router_Route(':lang_id/members/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'members',
                    'action' => 'index',
                    'lang_id' => '',
                ),
                array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'member-info-actions',
            new Zend_Controller_Router_Route('members/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'members',
                    'action' => 'index',
                ))
        );

        $router->addRoute(
            'newsletter-cron-def',
            new Zend_Controller_Router_Route_Regex(
                '^newsletter/email-sync-cron',
                array(
                    'module' => 'default',
                    'controller' => 'newsletter',
                    'action' => 'email-sync-cron',
                ),
                array(

                )
            )
        );

        // COMMENTS

        $router->addRoute(
            'comments-filter-def-page',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?(comments)-([0-9]+)',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'ajax-list',
                ),
                array(
                    4 => 'page',
                )
            )
        );

        //

        /* comment Subpage*/

        $router->addRoute(
            'subpagecomments',
            new Zend_Controller_Router_Route(':lang_id/comments',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'ajax-list',
                )), array(
                'lang_id' => '[a-z]{2}',
            )
        );

        $router->addRoute(
            'subpagecomments-def',
            new Zend_Controller_Router_Route('comments',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'ajax-list',
                ))
        );

        $router->addRoute(
            'subpagecomments-escort',
            new Zend_Controller_Router_Route(
                ':lang_id/escortcomments',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'get-escort-comments',
                ),
                array(
                    'lang_id' => '[a-z]{2}',
                )
            )
        );

        $router->addRoute(
            'subpagecomments-escort-def',
            new Zend_Controller_Router_Route_Regex(
                'escortcomments',
                array(
                    'module' => 'default',
                    'controller' => 'comments',
                    'action' => 'get-escort-comments',
                )
            )
        );
        /* Comments subpage*/

        $router->addRoute(
            'api-main',
            new Zend_Controller_Router_Route('api/:controller/:action/*',
                array(
                    'module' => 'api',
                    'controller' => 'index',
                    'action' => 'index',
                ))
        );

        $router->addRoute(
            'index-controller',
            new Zend_Controller_Router_Route('/index/:action',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'index',
                ))
        );

        $router->addRoute(
            'index-controller-lng',
            new Zend_Controller_Router_Route(':lang_id/index/:action',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'index',
                ),
                array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'online',
            new Zend_Controller_Router_Route('ajax-online',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'ajax-online',
                ))
        );

        $router->addRoute(
            'confirm-deletion-lang',
            new Zend_Controller_Router_Route_Regex('([a-z]{2})/private-v2/confirm-deletion/([a-f0-9]{10})',
                array(
                    'module' => 'default',
                    'controller' => 'private-v2',
                    'action' => 'confirm-deletion',
                ), array(
                    1 => 'lang_id',
                    2 => 'hash',
                ))
        );

        $router->addRoute(
            'confirm-deletion',
            new Zend_Controller_Router_Route_Regex('private-v2/confirm-deletion/([a-f0-9]{10})',
                array(
                    'module' => 'default',
                    'controller' => 'private-v2',
                    'action' => 'confirm-deletion',
                ), array(
                    1 => 'hash',
                ))
        );

        $router->addRoute(
            'get-gallery-photos',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?escorts/gallery-photos',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'gallery-photos',
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'ajax-tell-friend',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?escorts/ajax-tell-friend',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'ajax-tell-friend',
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'ajax-susp-photo',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?escorts/ajax-susp-photo',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'ajax-susp-photo',
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'ajax-add-report',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?escorts/ajax-add-report',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'ajax-add-report',
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'ajax-report-problem',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?escorts/ajax-report-problem',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'ajax-report-problem',
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'email-collecting-popup',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?index/email-collecting-popup',
                array(
                    'module' => 'default',
                    'controller' => 'index',
                    'action' => 'email-collecting-popup',
                ), array(
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'get-filter',
            new Zend_Controller_Router_Route_Regex('(([a-z]{2})/)?escorts/get-filter',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'get-filter',
                ), array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'get-sitemap',
            new Zend_Controller_Router_Route(
                '/sitemap',
                array(
                    'module' => 'default',
                    'controller' => 'sitemap',
                    'action' => 'index',
                )
            )
        );

        $router->addRoute(
            'render-sitemap',
            new Zend_Controller_Router_Route(
                '/sitemap/render',
                array(
                    'module' => 'default',
                    'controller' => 'sitemap',
                    'action' => 'render',
                )
            )
        );

        $router->addRoute(
            'agencies',
            new Zend_Controller_Router_Route_Regex(
                '^agencies',
                array(
                    'module' => 'default',
                    'controller' => 'agencies',
                    'action' => 'list',
                )
            )
        );

        $router->addRoute(
            'bdsm-agencies',
            new Zend_Controller_Router_Route_Regex(
                '(([a-z]{2})/)?bdsm-agencies',
                array(
                    'module' => 'default',
                    'controller' => 'agencies',
                    'action' => 'list',
                    'filter' => '2', // type of bdsm in db is 2
                    'lang_id' => null,

                ),
                array(
                    array('filter' => '\d+'),
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'massage-agencies',
            new Zend_Controller_Router_Route_Regex(
                '(([a-z]{2})/)?massage-agencies',
                array(
                    'module' => 'default',
                    'controller' => 'agencies',
                    'action' => 'list',
                    'filter' => '3', // type of massage in db is 3
                    'lang_id' => null,
                ),
                array(
                    array('filter' => '\d+'),
                    2 => 'lang_id',
                )
            )
        );

        $router->addRoute(
            'contacts',
            new Zend_Controller_Router_Route('/contacts/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'contacts',
                    'action' => 'index',
                ))
        );
        $router->addRoute(
            'contacts-lng',
            new Zend_Controller_Router_Route(':lang_id/contacts/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'contacts',
                    'action' => 'index',
                    'lang_id' => '',
                ),
                array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        /*$router->addRoute(
            'dashboard-def',
            new Zend_Controller_Router_Route('/dashboard/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'dashboard',
                    'action' => 'index',
                ))
        );

        $router->addRoute(
            'dashboard-lng',
            new Zend_Controller_Router_Route(':lang_id/dashboard/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'dashboard',
                    'action' => 'index',
                    'lang_id' => '',
                ),
                array(
                    'lang_id' => '[a-z]{2}',
                ))
        );*/

        $router->addRoute(
            'favorites',
            new Zend_Controller_Router_Route('/favorites/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'favorites',
                    'action' => 'index',
                ))
        );
        $router->addRoute(
            'favorites-lng',
            new Zend_Controller_Router_Route(':lang_id/favorites/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'favorites',
                    'action' => 'index',
                    'lang_id' => '',
                ),
                array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'alerts',
            new Zend_Controller_Router_Route('/alerts/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'alerts',
                    'action' => 'index',
                ))
        );
        $router->addRoute(
            'alerts-lng',
            new Zend_Controller_Router_Route(':lang_id/alerts/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'alerts',
                    'action' => 'index',
                    'lang_id' => '',
                ),
                array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'planner',
            new Zend_Controller_Router_Route('planner/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'planner',
                    'action' => 'index',
                ))
        );
        $router->addRoute(
            'planner-lng',
            new Zend_Controller_Router_Route(':lang_id/planner/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'planner',
                    'action' => 'index',
                    'lang_id' => '',
                ),
                array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        $router->addRoute(
            'private-messaging',
            new Zend_Controller_Router_Route('/private-messaging/:action/*',
                array(
                    'module' => 'private',
                    'controller' => 'messaging',
                    'action' => 'index',
                ))
        );
        $router->addRoute(
            'private-messaging-lng',
            new Zend_Controller_Router_Route(':lang_id/private-messaging/:action/*',
                array(
                    'module' => 'private',
                    'controller' => 'messaging',
                    'action' => 'index',
                    'lang_id' => '',
                ),
                array(
                    'lang_id' => '[a-z]{2}',
                ))
        );

        /** ONLINE BILLING V2 */
        $router->addRoute(
            'online-billing-v2',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?online-billing-v2/index',
                array(
                    'module' => 'default',
                    'controller' => 'online-billing-v2',
                    'action' => 'index',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'online-billing-v2-def',
            new Zend_Controller_Router_Route_Regex(
                '^(/)?online-billing-v2/index',
                array(
                    'module' => 'default',
                    'controller' => 'online-billing-v2',
                    'action' => 'index',
                ))
        );

        $router->addRoute(
            'online-billing-v2-checkout',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?online-billing-v2/checkout',
                array(
                    'module' => 'default',
                    'controller' => 'online-billing-v2',
                    'action' => 'checkout',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'online-billing-v2-checkout-def',
            new Zend_Controller_Router_Route_Regex(
                '^(/)?online-billing-v2/checkout',
                array(
                    'module' => 'default',
                    'controller' => 'online-billing-v2',
                    'action' => 'checkout',
                ))
        );

        $router->addRoute(
            'online-billing-mmg-postback',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?online-billing-v2/mmg-postback',
                array(
                    'module' => 'default',
                    'controller' => 'online-billing-v2',
                    'action' => 'mmg-postback',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'online-billing-mmg-postback-def',
            new Zend_Controller_Router_Route_Regex(
                '^(/)?online-billing-v2/mmg-postback',
                array(
                    'module' => 'default',
                    'controller' => 'online-billing-v2',
                    'action' => 'mmg-postback',
                ))
        );

        $router->addRoute(
            'epg-payment',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?payment',
                array(
                    'module' => 'default',
                    'controller' => 'online-billing',
                    'action' => 'payment',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'online-billing-v2-def',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?advertise',
                array(
                    'module' => 'default',
                    'controller' => 'online-billing',
                    'action' => 'advertise',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))
        );

        $router->addRoute(
            'prices-mmg-link',
            new Zend_Controller_Router_Route(
                'prices/generate-mmg-link',
                array(
                    'module' => 'default',
                    'controller' => 'online-billing',
                    'action' => 'generate-mmg-link'
                ))
        );

        $router->addRoute(
            'prices-mmg-response',
            new Zend_Controller_Router_Route(
                'advertise/mmg-response',
                array(
                    'module' => 'default',
                    'controller' => 'online-billing',
                    'action' => 'advertise-mmg-response'
                ))
        );

        $router->addRoute(
            'advertise-payment-email',
            new Zend_Controller_Router_Route(
                'advertise/payment-email',
                array(
                    'module' => 'default',
                    'controller' => 'online-billing',
                    'action' => 'advertise-payment-email'
                ))
        );

        $router->addRoute(
            'epg-payment-success',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?payment-success',
                array(
                    'module' => 'default',
                    'controller' => 'online-billing',
                    'action' => 'payment-success',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                ))

        );

        $router->addRoute(
            'client-hotel-blacklist-load',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?client-hotel-blacklist/(clients|hotels)',
                array(
                    'module' => 'default',
                    'controller' => 'client-hotel-blacklist',
                    'action' => 'load',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'type',
                )
            )
        );

        $router->addRoute(
            'client-hotel-blacklist-admin-warning',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?client-hotel-blacklist/admin-warning',
                array(
                    'module' => 'default',
                    'controller' => 'client-hotel-blacklist',
                    'action' => 'admin-warning',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'type',
                )
            )
        );

        $router->addRoute(
            'client-hotel-blacklist-add',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?client-hotel-blacklist/(clients|hotels)/add',
                array(
                    'module' => 'default',
                    'controller' => 'client-hotel-blacklist',
                    'action' => 'add',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'type',
                )
            )
        );

        $router->addRoute(
            'agency-escorts-v2-def',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?escorts/agency-escorts-v2',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'agency-escorts-v2',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                    3 => 'type',
                )
            )
        );

        $router->addRoute(
            'ajax-get-cities-search',
            new Zend_Controller_Router_Route_Regex(
                '^escorts/ajax-get-cities-search',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'ajax-get-cities-search',
                )
            )
        );

        $router->addRoute(
            'ajax-get-countries-search',
            new Zend_Controller_Router_Route_Regex(
                '^escorts/ajax-get-countries-search',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'ajax-get-countries-search',
                )
            )
        );

        $router->addRoute(
            'ajax-get-top-search',
            new Zend_Controller_Router_Route_Regex(
                '^escorts/ajax-get-top-search',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'ajax-get-top-search',
                )
            )
        );

        $router->addRoute(
            'ajax-get-agencies-search',
            new Zend_Controller_Router_Route_Regex(
                '^escorts/ajax-get-agencies-search',
                array(
                    'module' => 'default',
                    'controller' => 'escorts',
                    'action' => 'ajax-get-agencies-search',
                )
            )
        );

        $router->addRoute(
            'glossary',
            new Zend_Controller_Router_Route('glossary/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'glossary',
                    'action' => 'index',
                ))
        );

        $router->addRoute(
            'escorts-api',
            new Zend_Controller_Router_Route('escorts-api/:action/*',
                array(
                    'module' => 'default',
                    'controller' => 'escorts-api',
                    'action' => 'index',
                ))
        );

        $router->addRoute(
            'popunder',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?popunder',
                array(
                    'module' => 'default',
                    'controller' => 'popunder',
                    'action' => 'index',
                ),
                array(
                    2 => 'lang',
                )
            )
        );

        $router->addRoute(
            'popunder-escorts',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?popunder/escorts',
                array(
                    'module' => 'default',
                    'controller' => 'popunder',
                    'action' => 'escorts',
                ),
                array(
                    2 => 'lang',
                )
            )
        );

        $router->addRoute(
            'popunder-escorts-v2',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?popunder/escorts-v2',
                array(
                    'module' => 'default',
                    'controller' => 'popunder',
                    'action' => 'escorts-v2',
                ),
                array(
                    2 => 'lang',
                )
            )
        );

        $router->addRoute(
            'pundr-escorts',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?pundr/escorts',
                array(
                    'module' => 'default',
                    'controller' => 'popunder',
                    'action' => 'escorts',
                ),
                array(
                    2 => 'lang',
                )
            )
        );

        $router->addRoute(
            'pundr-escorts-v2',
            new Zend_Controller_Router_Route_Regex(
                '^(([a-z]{2})/)?pundr/escorts-v2',
                array(
                    'module' => 'default',
                    'controller' => 'popunder',
                    'action' => 'escorts-v2',
                ),
                array(
                    2 => 'lang',
                )
            )
        );

        /*REDIRECT*/
        /*$router->addRoute(
        'redirect-escorts-to-index',
        new Zend_Controller_Router_Route('escorts',
        array(
        'module' => 'default',
        'controller' => 'redirect',
        'action' => 'index'
        ))
        );*/

        $router->addRoute(
            'redirect-evaluations-to-reviews',
            new Zend_Controller_Router_Route_Regex('^(([a-z]{2})/)?evaluations/(.+)',
                array(
                    'module' => 'default',
                    'controller' => 'redirect',
                    'action' => 'reviews',
                    'lang_id' => null,
                ),
                array(
                    2 => 'lang_id',
                )
            )
        );

        // POPUNDERS OF cloudsrv3.com

        //POPUNDER DEF
        $pundr_plain_route_def = new Zend_Controller_Router_Route('*');
        $pundr_host_route_def = new Zend_Controller_Router_Route_Hostname('www.cloudsrv3.com',
            array(
                'module' => 'default',
                'controller' => 'popunder',
                'action' => 'escorts',
            ));

        $router->addRoute('pundr-domain', $pundr_host_route_def->chain($pundr_plain_route_def));

        //POPUNDER VERSION 1
        $pundr_plain_route_v1 = new Zend_Controller_Router_Route_Regex(
            '^(([a-z]{2})/)?escorts',
            array('action' => 'escorts'),
            array(2 => 'lang')
        );

        $pundr_host_route_v1 = new Zend_Controller_Router_Route_Hostname('www.cloudsrv3.com',
            array(
                'module' => 'default',
                'controller' => 'popunder',
            ));

        $router->addRoute('pundr-domain-v1', $pundr_host_route_v1->chain($pundr_plain_route_v1));

        //POPUNDER VERSION 2
        $pundr_plain_route_v2 = new Zend_Controller_Router_Route_Regex(
            '^(([a-z]{2})/)?escorts-v2',
            array('action' => 'escorts-v2'),
            array(2 => 'lang')
        );

        $pundr_host_route_v2 = new Zend_Controller_Router_Route_Hostname('www.cloudsrv3.com',
            array(
                'module' => 'default',
                'controller' => 'popunder',
            ));

        $router->addRoute('pundr-domain-v2', $pundr_host_route_v2->chain($pundr_plain_route_v2));

        // POPUNDERS END
    }

    protected function _initAutoload()
    {
        $moduleLoader = new Zend_Application_Module_Autoloader(array(
            'namespace' => '',
            'basePath' => APPLICATION_PATH,
        ));

        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('Cubix_');

        return $moduleLoader;
    }

    protected function _initDatabase()
    {
        $this->bootstrap('db');
        $db = $this->getResource('db');
        $db->setFetchMode(Zend_Db::FETCH_OBJ);

        Zend_Registry::set('db', $db);
        Cubix_Model::setAdapter($db);

        $db->query('SET NAMES `utf8`');
    }

    protected function _initConfig()
    {
        $images_configs = $this->getOption('images');
        $video_configs = $this->getOption('videos');
        $images_configs['remote']['serverUrl'] = APP_HTTP . '://' . $images_configs['remote']['serverUrl'];
        $images_configs['remote']['url'] = APP_HTTP . '://' . $images_configs['remote']['url'];
        $video_configs['Media'] = APP_HTTP . '://' . $video_configs['Media'];
        $video_configs['remoteUrl'] = APP_HTTP . '://' . $video_configs['remoteUrl'];

        Zend_Registry::set('AVOID_AUTH_CHECKING_ACTIONS', ['signin', 'index']);
        Zend_Registry::set('AUTOMATIC_ACTIONS', ['check-for-new-messages']);

        $useragent = $_SERVER['HTTP_USER_AGENT'];
        Zend_Registry::set('isMobile', preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)));

        Zend_Registry::set('images_config', $images_configs);
        Zend_Registry::set('videos_config', $video_configs);
        Zend_Registry::set('escorts_config', $this->getOption('escorts'));
        Zend_Registry::set('mobile_config', $this->getOption('mobile'));
        Zend_Registry::set('reviews_config', $this->getOption('reviews'));
        Zend_Registry::set('faq_config', $this->getOption('faq'));
        Zend_Registry::set('blog_config', $this->getOption('blog'));
        Zend_Registry::set('feedback_config', $this->getOption('feedback'));
        Zend_Registry::set('system_config', $this->getOption('system'));
        Zend_Registry::set('newsman_config', $this->getOption('newsman'));
        Zend_Registry::set('geo_config', $this->getOption('geo'));

        $helper = new Controller_Helper_Acl();
        $helper->setRoles();
        $helper->setResources();
        $helper->setPrivilages();
        $helper->setAcl();
    }

    protected function _initViewHelpers()
    {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view = $layout->getView();

        $view->doctype('XHTML1_STRICT');

        $view->headMeta()
        //->appendHttpEquiv('Content-Type', 'text/html; charset=utf-8')
            ->appendHttpEquiv('Cache-Control', 'no-cache');
        $view->headTitle()->setSeparator(' - ');
        $view->addHelperPath(APPLICATION_PATH . '/views/helpers');
    }

    protected function _initDefines()
    {

    }

    protected function _initCache()
    {
        $frontendOptions = array(
            'lifetime' => null,
            'automatic_serialization' => true,
        );

        $backendOptions = array(
            'servers' => array(
                array(
                    'host' => 'tcp://memcached:11211',
                    'port' => '0',
                    'persistent' => true,
                ),
            ),
        );

        if (defined('IS_DEBUG') && IS_DEBUG) {
            $cache = Zend_Cache::factory('Core', 'Blackhole', $frontendOptions, array());
        } else {
            $cache = Zend_Cache::factory(new Cubix_Cache_Core($frontendOptions), new Cubix_Cache_Backend_Memcached($backendOptions), $frontendOptions);
        }

        Zend_Registry::set('cache', $cache);


        $redis = new Redis();
        if(getenv('REDIS_HOST')){
            $redis->connect(getenv('REDIS_HOST'), getenv('REDIS_PORT'));
        }
        else{
            $redis->connect('127.0.0.1', 6379);
        }

        Zend_Registry::set('redis', $redis);

        /*************************************/
        /*$servers = array('host' => '172.16.1.51',  'port' => 11211);
    $memcache = new Memcache();
    $memcache->addServer($servers['host'], $servers['port']);

    Zend_Registry::set('cache_native', $memcache);*/
    }

    protected function _initBanners()
    {
        if (defined('IS_CLI') && IS_CLI) {
            return;
        }

        $banners = array();

        /*$cache = Zend_Registry::get('cache');

        $config = Zend_Registry::get('system_config');

        $banners['banners']['right_banners'] = array();
        $banners['banners']['links'] = array();

        if ( ! $banners = $cache->load('v2_ed_banners_cache') ) {
        for ($i = 0; $i < 5; $i++) {
        $banners['banners']['right_banners'][] = Cubix_Banners::GetBanner(1);
        }

        for ($i = 0; $i < 10; $i++) {
        $banners['banners']['right_banners'][] = Cubix_Banners::GetBanner(2);
        }

        for ($i = 0; $i < 100; $i++) {
        $banners['banners']['links'][] = Cubix_Banners::GetBanner(3);
        }

        $cache->save($banners, 'v2_ed_banners_cache', array(), $config['bannersCacheLifeTime']);
        }*/

        Zend_Registry::set('banners', $banners);
    }
    protected function _initApi()
    {
        $api_config = $this->getOption('api');
        Zend_Registry::set('api_config', $api_config);
        Cubix_Api_XmlRpc_Client::setApiKey($api_config['key']);
        Cubix_Api_XmlRpc_Client::setServer($api_config['server']);
    }

    protected function _initPlugins()
    {
        $objFront = Zend_Controller_Front::getInstance();
        $objFront->registerPlugin(new Controller_Plugin_Acl());
        return $objFront;

        // comment above 3 lines to remove ACL functionality
    }
}
