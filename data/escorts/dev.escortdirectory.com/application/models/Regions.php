<?php

class Model_Regions extends Cubix_Model
{
    protected $_table = 'regions';

    public static function getRegionCountryByID($id)
    {
        $lng = Cubix_I18n::getLang();

        $sql = 'SELECT
            c.id as country_id,
            r.id as region_id,
            
            c.title_' . $lng . ' as country_title,
            r.title_' . $lng . ' as region_title
        FROM
            regions r
        LEFT JOIN countries c on c.id = r.country_id
        WHERE
            r.id = ?';

        $result = self::db()->fetchAll($sql, $id);
        return isset($result[0]) ? $result[0] : null;
    }

}
