<?php

class Model_Video extends Cubix_Model {
    protected $_table = 'escorts';
    private $db;

    public $dimensions = array(
        "720" => array(
            'height' => 720, 'bitrate' => 1200
        ),
        "480" => array(
            'height' => 480, 'bitrate' => 600
        ),
        "360" => array(
            'height' => 360, 'bitrate' => 250
        )
    );

    public function __construct()
    {

        $this->db = Zend_Registry::get('db');
    }

    public function GetEscortVideo($escort_id, $limit, $agency_id = NULL, $private = TRUE)
    {

        $escort_id = $this->db->quote($escort_id);
        if(isset($agency_id) && is_numeric($agency_id))
        {	$check =  parent::_fetchRow("SELECT id FROM escorts WHERE id = ? AND agency_id = ?",array($escort_id, $agency_id));
            if(empty($check))
                return FALSE;
        }
        if($private)
        {
            $client = Cubix_Api_XmlRpc_Client::getInstance();
            return  $client->call('Application.GetVideos',array($escort_id,$limit));
        }
        else
        {

            $video = parent::_fetchAll("SELECT * FROM video WHERE escort_id = ? ORDER BY `date` DESC LIMIT 0,".$limit, array($escort_id));

            $photo = array();
            if(!empty($video))
            {
                $v_id = array();
                foreach ($video as $v)
                {
                    $v_id[] = $v->id;
                }
                $v_id =  implode(',', $v_id);
                $sql_image = "SELECT * FROM video_image  WHERE video_id IN($v_id) AND is_mid = 1";
                $photo =  parent::_fetchAll($sql_image);
            }
            return array($photo, $video);
        }
    }

    public function GetEscortsVideoArray(array $escorts)
    {
        if ( ! count($escorts) ) return array();

        $escorts = implode(',', $escorts);
        $sql = "SELECT v.id as video_id , video, hash, width, height, escort_id, ext FROM video_image AS img 
				INNER JOIN (SELECT video,escort_id,id FROM video WHERE escort_id IN($escorts) ORDER BY `date` DESC ) AS v 
				ON v.id = img.video_id 
				GROUP BY escort_id ORDER BY img.`date` DESC ";
        return parent::_fetchAll($sql);
    }

    public function Remove($video_id,$escort_id)
    {

        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $affected_video_id = $client->call('Application.RemoveVideo',array($escort_id,$video_id));
        if($affected_video_id)
            $this->db->query("DELETE FROM video WHERE escort_id = ? AND id = ? ",array($escort_id,$video_id ));
        return $affected_video_id;

    }

    public function GetAgencyEscort($agency_id)
    {
        $escorts =parent::_fetchAll("SELECT showname,id FROM  escorts WHERE agency_id=?  ",$agency_id);

        if(!empty($escorts))
        {
            $client = Cubix_Api_XmlRpc_Client::getInstance();
            $escorts=$client->call('Application.GetAgencyEscortVideo',array($agency_id,$escorts));
        }
        return $escorts;
    }

    public function GetAgencyEscorts($id,$escort_id)
    {
        $escort_id = $this->db->quote($escort_id);
        $escorts = parent::_fetchAll("SELECT showname,id FROM escorts WHERE agency_id='$id' AND id=$escort_id ");
        return empty($escorts)?FALSE:TRUE;
    }


    public function UserVideoCount($escort_id)
    {
        $escort_id = $this->db->quote($escort_id);
        $count= parent::_fetchRow("SELECT count(id) AS quantity FROM video WHERE escort_id=$escort_id  GROUP BY escort_id ");
        return $count->quantity;
    }

    public function getManifestData($escort_id, $video_id)
    {
        return parent::_fetchRow("SELECT video, height, width FROM video WHERE escort_id = ? AND id = ?", array($escort_id, $video_id));
    }
}
