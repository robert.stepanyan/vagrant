<?php

class Model_Support extends Cubix_Model
{
    protected $_table = 'support_tickets';
    protected $_itemClass = 'Model_SupportItem';

    const STATUS_TICKET_OPENED = 1;
    const STATUS_TICKET_CLOSED = 2;

    const STATUS_NEW_UNREAD = 1;
    const STATUS_NOT_REPLIED = 2;
    const STATUS_READ_REPLIED = 3;
    const STATUS_NEW_REPLY = 4;

    public function get($ticket_id)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $data = $client->call('Support.getAttached', array($ticket_id));

        return $data;
    }

    public function getStandart($ticket_id)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $data = $client->call('Support.get', array($ticket_id));

        return $data;
    }

    public function isUserTicket($data)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call('Support.isUserTicket', array($data));
    }

    public function isClosed($ticket_id)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call('Support.isClosed', array($ticket_id));
    }

    public function getAll($user_id, $params = [])
    {
        $client = new Cubix_Api_XmlRpc_Client();

        if (!isset($params['ot_page']) || empty($params['ot_page']))
            $params['ot_page'] = 1;

        if (!isset($params['ct_page']) || empty($params['ct_page']))
            $params['ct_page'] = 1;

        if (!isset($params['per_page']) || empty($params['per_page']))
            $params['per_page'] = 3;

        // ot_page = Open tickets page,
        // ct_page = Closed tickets page
        $result = [];

        $result['tickets']['open'] = $client->call('Support.getAllPaginated', array($user_id, $params['ot_page'], $params['per_page'], self::STATUS_TICKET_OPENED));
        $result['tickets']['closed'] = $client->call('Support.getAllPaginated', array($user_id, $params['ct_page'], $params['per_page'], self::STATUS_TICKET_CLOSED));

        return $result;
    }

    public function save($data)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $ticket_id = $client->call('Support.insertAttached', array($data));

        return $ticket_id;
    }

    public function saveStandart($data)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $ticket_id = $client->call('Support.insert', array($data));

        return $ticket_id;
    }

    public function addComment($data)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $data = $client->call('Support.addCommentAttached', array($data));

        return $data;
    }

    public function addCommentStandart($data)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $data = $client->call('Support.addComment', array($data));

        return $data;
    }

    public function getComments($ticket_id)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $data = $client->call('Support.getCommentsAttached', array($ticket_id));

        return $data;
    }

    public function read($ticket_id)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $client->call('Support.read', array($ticket_id));
    }

    public function comment_read($ticket_id)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $client->call('Support.comment_read', array($ticket_id));
    }

    public function getUnreadsCount($user_id)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $data = $client->call('Support.getUnreadsCount', array($user_id));

        return $data;
    }

    public function getUnreadsCountV2($user_id)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $data = $client->call('Support.getUnreadsCountV2', array($user_id));

        return $data;
    }

    public function addAttached($data)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $id = $client->call('Support.addAttached', array($data));

        return $id;
    }
}
