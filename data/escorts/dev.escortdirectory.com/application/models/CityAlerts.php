<?php

class Model_CityAlerts extends Cubix_Model
{
	public function getAll($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.getAll', array($user_id));
	}

	public function getAllV2($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.getAllV2', array($user_id));
	}
	
	public function remove($id, $user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.remove', array($id, $user_id));
	}
	
	public function removeV2($id, $user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.removeV2', array($id, $user_id));
	}

	public function suspend($id, $user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.suspend', array($id, $user_id));
	}

	public function resume($id, $user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.resume', array($id, $user_id));
	}

	public function add($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.add', array($data));
	}

	public function addV2($data)
	{
		$data['all_city'] = 0;
		if($data['cities'][0] == "-1"){
			$model = new Cubix_Geography_Cities();
			$cities = $model->getByCountryId($data['country_id']);
			$data['cities'] = $cities;
			$data['all_city'] = 1;  
		}
		$client = new Cubix_Api_XmlRpc_Client();
		$client->call('CityAlerts.addV2', array($data));
	}
	public function save($data)
	{
		$data['all_city'] = 0;
		if($data['cities'][0] == "-1"){
			$model = new Cubix_Geography_Cities();
			$cities = $model->getByCountryId($data['country_id']);
			$data['cities'] = $cities;
			$data['all_city'] = 1;  
		}
		$client = new Cubix_Api_XmlRpc_Client();
		$client->call('CityAlerts.save', array($data));
	}
	
	public function checkCityExists($city_id, $user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.checkCityExists', array($city_id, $user_id));
	}

	public function checkCityExistsV2($country, $cities, $user_id, $escort_type, $escort_gender,$id = false)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.checkCityExistsV2', array($country, $cities, $user_id, $escort_type, $escort_gender, $id));
	}

	public function getEventList($user_id){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.getEventList', array($user_id));
	}

	public function clearList($id, $user_id){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.clearList', array($id, $user_id));
	}

	public function getAlertEscorts($alert_id,$escort_id){
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('CityAlerts.getAlertEscorts', array($alert_id,$escort_id));
	}
}
