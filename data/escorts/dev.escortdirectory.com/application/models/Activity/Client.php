<?php

class Model_Activity_Client
{
	/**
	 * @var array
	 */
	protected $_data;
	
	/**
	 * @var string
	 */
	protected $_session_id;
	
	/**
	 * @var Model_Activity_Client_Session
	 */
	protected $_session;
	
	public function __construct()
	{
		$this->_data = array(
			
		);
	}
	
	public static function getByCookieId($cookie_id)
	{
		$db = Zend_Registry::get('db');
		
		$row = $db->query('
			SELECT 
				id, cookie_id, http_user_agent, http_via, client_ip, proxy_ip
			FROM activity_clients
			WHERE cookie_id = ?
		', $cookie_id)->fetch();
		
		if ( ! $row ) {
			return null;
		}
		
		$client = new Model_Activity_Client();
		$client->setId($row->id);
		$client->setCookieId($row->cookie_id);
		$client->setHttpUserAgent($row->http_user_agent);
		$client->setHttpVia($row->http_via);
		$client->setClientIp($row->client_ip);
		$client->setProxyIp($row->proxy_ip);
		
		return $client;
	}
	
	public function setId($id)
	{
		$this->_data['id'] = $id;
	}
	
	public function setCookieId($cookie_id)
	{
		if ( ! preg_match('#^[a-f0-9]{64}$#', $cookie_id) ) {
			throw new Exception('Wrong cookie_id supplied, allowed only a-f, 0-9 and length must be 64');
		}
		
		$this->_data['cookie_id'] = $cookie_id;
	}
	
	public function setHttpUserAgent($user_agent)
	{
		$user_agent = substr($user_agent, 0, 255);
		
		$this->_data['http_user_agent'] = $user_agent;
	}
	
	public function setHttpVia($via)
	{
		$via = substr($via, 0, 255);
		
		$this->_data['http_via'] = $via;
	}
	
	public function setClientIp($ip)
	{
		if ( ! Model_Activity_Utils::checkIp($ip) ) {
			throw new Exception('Wrong ip format');
		}
		
		$this->_data['client_ip'] = $ip;
	}
	
	public function setProxyIp($ip)
	{
		if ( is_null($ip) ) return;
		
		if ( ! Model_Activity_Utils::checkIp($ip) ) {
			throw new Exception('Wrong ip format');
		}
		
		$this->_data['proxy_ip'] = $ip;
	}
	
	public function save()
	{
		$db = Zend_Registry::get('db');
		
		if ( ! isset($this->_data['cookie_id']) ) {
			throw new Exception('Cookie_id is required');
		}
		
		$db->insert('activity_clients', $this->_data);
		
		$this->setId($db->lastInsertId());
	}
	
	public function updateAccessTime()
	{
		$db = Zend_Registry::get('db');
		
		if ( ! isset($this->_data['id']) ) {
			throw new Exception('Id is required');
		}
		
		$db->update('activity_clients', array(
			'date_last_access' => new Zend_Db_Expr('NOW()')
		), $db->quoteInto('id = ?', $this->_data['id']));
	}
	
	public function getData()
	{
		return (object) $this->_data;
	}
	
	public function setSessionId($session_id)
	{
		/*if ( ! preg_match('#^[a-f0-9]{32}$#', $session_id) ) {
			throw new Exception('Wrong session_id supplied, allowed only a-f, 0-9 and length must be 32');
		}*/
		
		$this->_session_id = $session_id;
	}
	
	/**
	 * @return Model_Activity_Client_Session
	 */
	public function getSession()
	{
		if ( isset($this->_session) ) {
			return $this->_session;
		}
		
		if ( ! isset($this->_data['id']) ) {
			throw new Exception('Id is required');
		}
		
		if ( ! isset($this->_session_id) ) {
			throw new Exception('Session_id is required');
		}
		
		$session = Model_Activity_Client_Session::getByClientSessionId(
			$this->_data['id'], $this->_session_id);
		
		if ( is_null($session) ) {
			return null;
		}
		
		return $this->_session = $session;
	}
}
