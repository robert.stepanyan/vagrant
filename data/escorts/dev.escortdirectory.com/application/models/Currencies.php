<?php

class Model_Currencies extends Cubix_Model
{
	static public function getAll()
	{
		$cache_key = Cubix_Application::getId() . '_currencies_list';
		
		if ( ! $currencies = self::cache()->load($cache_key) ) {
			$currencies = self::db()->fetchAll('
				SELECT *
				FROM currencies
				ORDER BY ordering ASC, is_default DESC
			');
			self::cache()->save($currencies, $cache_key, array(),
					self::config()->currencies->cache_lifetime);
		}

		return $currencies;
	}

	const GET_TITLE = 1;
	const GET_SYMBOL = 2;

	static public function getAllAssoc($part = self::GET_TITLE)
	{
		$key = 'id';
		switch ( $part ) {
			case self::GET_TITLE:
				$key = 'title';
				break;
			case self::GET_SYMBOL:
				$key = 'symbol';
				break;
		}

		$result = array();
		foreach ( self::getAll() as $c ) {
			$result[$c->id] = $c->$key;
		}

		return $result;
	}

	static public function getSelectedCurrencyRate($iso)
	{
		$cache_key = $iso . "_" . Cubix_Application::getId() . '_selected_currency_rate';
		
		if ( ! $currency_rate = self::cache()->load($cache_key) ) {
			$currency_rate = self::db()->fetchRow('
				SELECT iso, rate
				FROM exchange_rates
				WHERE iso = ?
			', array($iso));

			self::cache()->save($currency_rate, $cache_key, array(),
					self::config()->currencies->cache_lifetime);
		}

		return $currency_rate;
	}
}
