<?php

class Model_Reviews extends Cubix_Model
{
	public function getReviews($page, $per_page, $filter, $sort_field, $sort_dir)
	{
		if (isset($filter['agency_id']) && $filter['agency_id'] > 0)
		{
			$params = array('e.agency_id = ?' => $filter['agency_id']);

			$a_escorts = Model_Escort_List::getAgencyEscorts($params, 'newest', 1, 1000, $escorts_count);

			if ($a_escorts)
			{
				$ae_ids = array();

				foreach ($a_escorts as $ae)
				{
					$ae_ids[] = $ae->id;
				}

				$ae_ids_str = implode(',', $ae_ids);
			}
		}

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				r.escort_id, r.username, r.showname, UNIX_TIMESTAMP(r.creation_date) AS creation_date,
				UNIX_TIMESTAMP(r.meeting_date) AS meeting_date, r.looks_rating, r.services_rating,
				c.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				cn.' . Cubix_I18n::getTblField('title') . ' AS country_title,
				r.agency_name, r.photo_hash AS hash, r.photo_ext AS ext, r.application_id, r.status, r.review, r.id,
				r.m_count, r.e_count, r.user_id, c2.' . Cubix_I18n::getTblField('title') . ' AS e_city_title,
				cn2.' . Cubix_I18n::getTblField('title') . ' AS e_country_title
			FROM reviews r
			LEFT JOIN cities c ON c.id = r.city
			LEFT JOIN countries cn ON cn.id = r.country
			LEFT JOIN cities c2 ON c2.id = r.e_city_id
			LEFT JOIN countries cn2 ON cn2.id = r.e_country_id
			LEFT JOIN escorts e ON r.escort_id = e.id
			WHERE e.disabled_reviews != 1 and 1
		';

		$countSql = 'SELECT FOUND_ROWS()';

		$where = ' AND r.status = ? AND r.is_deleted = 0 ';

		if (isset($filter['agency_id']) && $filter['agency_id'] > 0 && strlen($ae_ids_str) > 0)
			$where .= ' AND r.escort_id IN (' . $ae_ids_str . ')';

		if (isset($filter['review_id_not']) && $filter['review_id_not'] > 0)
			$where .= ' AND r.id <> ' . $filter['review_id_not'];

		if (isset($filter['escort_id']) && $filter['escort_id'] > 0)
			$where .= ' AND r.escort_id = ' . $filter['escort_id'];

		if (isset($filter['user_id']) && $filter['user_id'] > 0)
			$where .= ' AND r.user_id = ' . $filter['user_id'];

		if (isset($filter['country_id']) && $filter['country_id'] > 0)
			$where .= ' AND r.country = ' . $filter['country_id'];

		if (isset($filter['city_id']) && $filter['city_id'] > 0)
			$where .= ' AND r.city = ' . $filter['city_id'];

		if (isset($filter['member']) && strlen($filter['member']))
			$where .= " AND r.username LIKE '" . $filter['member'] . "%'";

		if (isset($filter['showname']) && strlen($filter['showname']))
			$where .= " AND r.showname LIKE '" . $filter['showname'] . "%'";

		if (isset($filter['meeting_date_f']) && $filter['meeting_date_f'] > 0)
			$where .= ' AND DATE(r.meeting_date) >= "' . date('Y-m-d', $filter['meeting_date_f']) . '"';

		if (isset($filter['meeting_date_t']) && $filter['meeting_date_t'] > 0)
			$where .= ' AND DATE(r.meeting_date) <= "' . date('Y-m-d', $filter['meeting_date_t']) . '"';

		if (isset($filter['date_added_f']) && $filter['date_added_f'] > 0)
			$where .= ' AND DATE(r.creation_date) >= "' . date('Y-m-d', $filter['date_added_f']) . '"';

		if (isset($filter['date_added_t']) && $filter['date_added_t'] > 0)
			$where .= ' AND DATE(r.creation_date) <= "' . date('Y-m-d', $filter['date_added_t']) . '"';

		if (isset($filter['gender']) && strlen($filter['gender']))
			$where .= ' AND r.gender IN (' . $filter['gender'] . ')';

		if (isset($filter['gender_combined']) && $filter['gender_combined'] > 0)
			$where .= ' AND r.gender = ' . $filter['gender_combined'];

		$sql .= $where;

		$sql .= '
			GROUP BY r.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$items = $this->db()->query($sql, REVIEW_APPROVED)->fetchAll();
		$count = intval($this->db()->fetchOne($countSql));

		return array($items, $count);
	}

	public function allowRevCom($escort_id, $mode = 'e')
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Reviews.allowRevCom', array($escort_id, $mode));
	}

    public function getEscortReviews( $escort_id )
    {
        $sql = '
			SELECT SQL_CALC_FOUND_ROWS
				r.id, r.username, r.user_id, r.looks_rating, r.services_rating, r.is_fake_free, r.meeting_place, r.duration, r.duration_unit, r.price, r.currency,
				r.fuckometer, r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69, r.s_anal, r.s_anal, r.s_sex, r.s_attitude, r.s_conversation, r.s_breast, r.s_multiple_sex,
				r.s_availability, r.s_photos, r.services_comments, r.review, UNIX_TIMESTAMP(r.meeting_date) AS meeting_date, UNIX_TIMESTAMP(r.creation_date) AS creation_date
			FROM reviews r			
			WHERE r.status = 2 AND r.is_deleted = 0 AND r.escort_id = ?
			ORDER BY r.creation_date DESC
		';

        $countSql = 'SELECT FOUND_ROWS() AS count';

        $item = $this->_db->query($sql, array($escort_id))->fetchAll();
        $count = $this->_db->query($countSql)->fetch();
        $count = $count->count;

        return array($item, $count);
    }

	public function setAllowRevCom($escort_id, $act, $val)
	{
		if ($act == 'r')
		{
			$field = 'disabled_reviews';
		}
		else
		{
			$field = 'disabled_comments';
		}

		$this->db()->update('escorts', array($field => $val), $this->db()->quoteInto('id = ?', $escort_id));

		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Reviews.setAllowRevCom', array($escort_id, $act, $val));
	}

	public function setAllowRevComByUserId($userId, $act, $val)
	{
        if ($act == 'r')
        {
            $field = 'disabled_reviews';
        }
        else
        {
            $field = 'disabled_comments';
        }

        $this->db()->update('escorts', array($field => $val), $this->db()->quoteInto('user_id = ?', $userId));

		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Reviews.setAllowRevComByUserId', array($userId, $act, $val));
	}

	public function getEscortLastReview($escort_id)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				r.id, r.username, r.looks_rating, r.services_rating, r.is_fake_free, r.meeting_place, r.duration, r.duration_unit, r.price, r.currency,
				r.fuckometer, r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69, r.s_anal, r.s_anal, r.s_sex, r.s_attitude, r.s_conversation, r.s_breast,
				r.s_multiple_sex, r.s_availability, r.s_photos, r.services_comments, r.review, UNIX_TIMESTAMP(r.meeting_date) AS meeting_date,
				UNIX_TIMESTAMP(r.creation_date) AS creation_date, r.e_count AS count, r.showname, r.escort_id, r.user_id, r.m_count
			FROM reviews r
			WHERE r.status = ? AND r.is_deleted = 0 AND r.escort_id = ?
			ORDER BY r.creation_date DESC
			LIMIT 1
		';

		return $this->db()->query($sql, array(REVIEW_APPROVED, $escort_id))->fetch();
	}

	public function getEscortShowname($escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Reviews.getEscortShowname', array($escort_id));
	}

	public function getReviewsForMember($user_id, $lng, $filter, $page, $per_page, $sort_field, $sort_dir)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Reviews.getReviewsForMember', array($user_id, $lng, $filter, $page, $per_page, $sort_field, $sort_dir));
	}

	public function getReviewsForEscort($escort_id, $lng, $filter, $page, $per_page, $sort_field, $sort_dir)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Reviews.getReviewsForEscort', array($escort_id, $lng, $filter, $page, $per_page, $sort_field, $sort_dir));
	}

	public function getReviewsCountryCity($id,$type,$lang,$filter)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Reviews.getReviewsCountryCity', array($id, $type, $lang, $filter));
	}

	public function getReviewsForAgency($agency_id, $lng, $filter, $page, $per_page, $sort_field, $sort_dir)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Reviews.getReviewsForAgency', array($agency_id, $lng, $filter, $page, $per_page, $sort_field, $sort_dir));
	}

	public function getAgencyEscorts($agency_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Reviews.getAgencyEscorts', array($agency_id));
	}

	public function getEscortReview($escort_id, $r_id)
	{
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				UNIX_TIMESTAMP(r.creation_date) AS creation_date,
				c.' . Cubix_I18n::getTblField('title') . ' AS city_title, r.fuckometer, r.id,
				UNIX_TIMESTAMP(r.meeting_date) AS meeting_date, r.looks_rating, r.services_rating, r.user_id,
				cn.' . Cubix_I18n::getTblField('title') . ' AS country_title, r.review, r.services_comments,
				r.escort_comment, r.meeting_place, r.duration, r.duration_unit, r.price, cr.title AS currency,
				r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69, r.s_anal, r.s_sex, r.s_multiple_sex, r.s_breast,
				r.s_attitude, r.s_conversation, r.s_availability, r.s_photos, r.username, r.m_count, r.e_count,
				c2.' . Cubix_I18n::getTblField('title') . ' AS e_city_title,
				cn2.' . Cubix_I18n::getTblField('title') . ' AS e_country_title, cn.iso ,
				quality, location_review
			FROM reviews r
			LEFT JOIN cities c ON c.id = r.city
			LEFT JOIN countries cn ON cn.id = c.country_id
			LEFT JOIN cities c2 ON c2.id = r.e_city_id
			LEFT JOIN countries cn2 ON cn2.id = r.e_country_id
			LEFT JOIN currencies cr ON cr.id = r.currency
			WHERE r.status = ? AND r.is_deleted = 0 AND r.escort_id = ? AND r.id = ?
		';

		$item = $this->db()->query($sql, array(REVIEW_APPROVED, $escort_id, $r_id))->fetch();

		return $item;
	}

	public static function createCookieClientID($user_id)
    {
        $clientID = "";

        if (!isset($_COOKIE['clientID']))
        {
			$clientID = md5(microtime());
        }
        else
        {
            $clientID = $_COOKIE['clientID'];
        }

        if ($clientID != "")
        {
            setcookie("clientID", $clientID, time() + 31536000, "/"); // 31536000 = 60*60*24*365 = one year in seconds

			Cubix_Api::getInstance()->call('cookieClientID', array($user_id, $clientID));
        }
	}

	public function getEscortInfo($escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Reviews.getEscortInfo', array($escort_id));
	}

	public function getEscortAge($escort_id){
        $sql = "
			SELECT age
			FROM escorts
			WHERE id = ?
		";

        return $this->db()->fetchOne($sql, $escort_id);
    }

    public function getEscortGender($escort_id){
        $sql = "
			SELECT gender
			FROM escorts
			WHERE id = ?
		";

        return $this->db()->fetchOne($sql, $escort_id);
    }

	public function getReviewsCountCountry(){
		$lng = Cubix_I18n::getLang();

		$sql = '
			SELECT
				COUNT(DISTINCT(r.id)) AS review_count,
				c.id, c.title_' . $lng . ' AS title
			FROM countries c
			LEFT JOIN reviews r ON r.country = c.id
			GROUP BY c.id
		';

		return $this->db()->query($sql)->fetchAll();	
	}

	public function getReviewsCountCity($country_id){
		$lng = Cubix_I18n::getLang();

		$sql = '
			SELECT
				COUNT(DISTINCT(r.id)) AS review_count,
				c.id, c.title_' . $lng . ' AS title
			FROM cities c
			LEFT JOIN reviews r ON r.city = c.id
			WHERE c.country_id = ?
			GROUP BY c.id
		';

		return $this->db()->query($sql,$country_id)->fetchAll();	
	}
	
	public function getEscortReviewIds($escort_id)
	{	
		$sql = 'SELECT id FROM reviews r WHERE r.escort_id = ?';
		return $this->db()->fetchAll($sql, $escort_id);
	}
}
