<?php

class Model_Cities extends Cubix_Model 
{
	protected $_table = 'cities';

	public function getByCountry($id) 
	{
		$sql = '
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM cities c
			WHERE country_id = ' . $id . '
			ORDER BY title
		';

		return parent::_fetchAll($sql);
	}

    public static function getCountryRegionByCityID($id)
    {
        $lng = Cubix_I18n::getLang();

        $sql = 'SELECT
            ct.id as city_id,
            c.id as country_id,
            r.id as region_id,
            
            ct.slug as city_slug,
            c.slug as country_slug,
            ct.title_' . $lng . ' as city_title,
            c.title_' . $lng . ' as country_title,
            r.title_' . $lng . ' as region_title
        FROM
            cities ct
        LEFT JOIN countries c on c.id = ct.country_id
        LEFT JOIN regions r on ct.region_id = r.id
        WHERE
            ct.id = ?';

        $result = self::db()->fetchAll($sql, $id);
        return isset($result[0]) ? $result[0] : null;
    }

    public function getCountryIdById($id)
    {
        $sql = '
			SELECT country_id
			FROM cities c
			WHERE id = ' . $id . '
		';

        return parent::_fetchRow($sql);
    }

    public function getIdBySlug($slug)
    {
        $sql = '
			SELECT id
			FROM cities
			WHERE slug = "' . $slug . '"
		';

        return parent::_fetchRow( $sql );
    }

    public static function getCoordinatesById($id)
    {
        $sql = '
			SELECT id, latitude, longitude, country_id
			FROM cities
			WHERE id = ?
		';

        return self::db()->fetchRow($sql, $id);
    }
	
	public function getByCountries($ids) 
	{
		$ids_str = implode(',', $ids);
		$sql = '
			SELECT c.id, c.country_id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM cities c
			WHERE country_id IN (' . $ids_str . ')
			ORDER BY title
		';

		return parent::_fetchAll($sql);
	}
	
	public function getByIds($ids)
	{
		return parent::_fetchAll('SELECT id, ' . Cubix_I18n::getTblField('title') . ' as title FROM cities WHERE id IN (' . $ids . ') ORDER BY title ASC');
	}

	public function getAll()
	{
		return parent::_fetchAll('SELECT id, ' . Cubix_I18n::getTblField('title') . ' as title FROM cities ORDER BY title ASC');
	}

	public function getByGeoIp()
	{
		$geoData = Cubix_Geoip::getClientLocation();

		if (!is_null($geoData))
		{
			// Hard code Exception for Hong Kong
			if ( $geoData['country'] == "Hong Kong" ) {
				$geoData['city'] = "Hong Kong";
			}

			$log = false;

			if ( strlen($geoData['country']) ) {

				$country = parent::getAdapter()->fetchRow('SELECT id, slug, title_en AS title FROM countries WHERE title_geoip = ?', $geoData['country']);

				if ($country) {

                    $city = null;
					if ( strlen($geoData['city']) ) {
                        $sql = 'SELECT c.title_en, c.slug, c.id, r.id as region_id, r.title_en as region_title, r.slug as region_slug 
                                FROM cities c
                                LEFT JOIN regions r 
                                ON r.id = c.region_id WHERE c.title_geoip = ?';
                        $city = parent::getAdapter()->fetchRow($sql, $geoData['city']);
					}

                    return array(
                        'city_id' => (isset($city->id) ? $city->id : null),
                        'city_title' => (isset($city->title_en) ? $city->title_en : null),
                        'city_slug' => (isset($city->slug) ? $city->slug : null),
                        'country_id' => $country->id,
                        'country_slug' => $country->slug,
                        'country_title' => $country->title,
                        'region_id' => $city->region_id,
                        'region_title' => $city->region_title,
                        'region_slug' => $city->region_slug,
                    );
				} else {
					$log = true;
				}

			} else {
				$log = false;
			}

			if ($log)
			{
				$geoData['city'] = utf8_encode($geoData['city']);

				$c = parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM geoip_missing_cities WHERE city = ?', $geoData['city']);

				if ($c == 0)
					parent::getAdapter()->insert('geoip_missing_cities', array(
						'ip' => $geoData['ip'],
						'country' => $geoData['country'],
						'country_iso' => $geoData['country_iso'],
						'region' => $geoData['region'],
						'city' => $geoData['city'],
						'latitude' => $geoData['latitude'],
						'longitude' => $geoData['longitude']
					));

				return null;
			}
		}
		else
			return null;
	}
	
	public function getById($id)
	{
		return $this->db()->query('SELECT c.*, ' . Cubix_I18n::getTblField('c.title') . ' as title, ' . Cubix_I18n::getTblField('co.title') . ' as country_title FROM cities c INNER JOIN countries co ON co.id = c.country_id WHERE c.id = ?', $id)->fetch();
	}

    public static function getNearbyCityIds($geo_data)
    {
        $redis = Zend_Registry::get('redis');
        $key = $geo_data['country_iso'] .'_'. $geo_data['city'];
        if($ids = $redis->hGet('nearby_cities', $key)){
            return $ids;
        }
        else{

            self::db()->insert('geoip_missing_nearby_cities', array(
                'ip' => $geo_data['ip'],
                'country' => $geo_data['country'],
                'country_iso' => $geo_data['country_iso'],
                'region' => $geo_data['region'],
                'city' => $geo_data['city'],
                'latitude' => $geo_data['latitude'],
                'longitude' => $geo_data['longitude']
            ));

            $sql = '
				SELECT nearby_cities , ((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - latitude))/2), 2) +
							COS(RADIANS(latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - latitude))/2), 2) +
							COS(RADIANS(latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - longitude))/2), 2)
						))
						)
					)) AS distance
				FROM cities
				ORDER BY distance
				LIMIT 1
			';

            return self::db()->fetchOne($sql);
        }


    }
}
