<?php

/**
 * @storage = Redis
 */

class Model_Analytics extends Cubix_Model
{
    const ACTION_TYPE_PROFILE = 'profile-view';
    const ACTION_TYPE_CITY = 'city-view';
    const ACTION_TYPE_LISTING = 'listing-view';

    public static function getStore()
    {
        return Zend_Registry::get('redis');
    }

    public static function collect_escort_profile_statistics($escort_id, $last_n_days = null)
    {
        $result = [];
        $where = ['escort_id = ?'];
        $prefixes = ['listing_views', 'profile_views', 'city_views'];

        if(is_numeric($last_n_days)) {
            $where []= ' date > NOW() - INTERVAL ' . $last_n_days . ' DAY';
        }else{
            throw new \Exception('param: Last n Days must be valid integer');
        }

        foreach ($prefixes as $prefix) {
            $table = 'escort_' . $prefix . '_per_week';

            $fields = '';
            if($prefix == 'city_views') {
                $fields .= ' city_id, ';
            }

            $sql = "
                SELECT
                    escort_id, `date`, $fields
                    sum( views_count ) AS views_count 
                FROM
                    $table 
                WHERE ".implode(' AND', $where)."
            ";

            $result[$prefix] = self::db()->fetchRow($sql, $escort_id);
        }

        return $result;
    }

    public static function new_escort_visit(Array $params)
    {
        // First check if data is not empty
        // ---------------------------
        if (!empty($params['action-type']) && !empty($params['escort']) && !empty($params['visitor'])) {

            // Generating key for storage
            // -------------------------------
            $key = Cubix_Application::getId() . ':' . $params['action-type'] . ':' . $params['escort'] . ':' . $params['visitor'];
            // -------------------------------

            if ($params['action-type'] == self::ACTION_TYPE_CITY) {
                $key .= ':' . $params['city'];
            }


            // Put into @storage, if it was not set already
            // ----------------------------------------
            self::getStore()->setNx($key, 1);
            // ----------------------------------------

            return $key;
        }
        // ---------------------------

        return false;
    }

}