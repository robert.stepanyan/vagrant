<?php

define('LISTING_SIZE', Zend_Registry::get('isMobile') ? 44 : 45);

class Model_Escort_List extends Cubix_Model
{
	const HOMEPAGE_LISTING_SIZE = LISTING_SIZE;

	public static function getActiveFilter($filter, $where_query = array(), $is_upcomingtour = false, $is_tour = false)
	{
		if ($filter['show_all_agency_escorts']) {
			unset($filter['show_all_agency_escorts']);		
		}

		/*$full_text = "";
		if ( isset($filter['search_string']) && $filter['search_string'] ) {
			if ( isset($filter['search_string'][0]) && strlen($filter['search_string'][0]) ) {
				$full_text = " AND MATCH (search_string) AGAINST ('" . $filter['search_string'][0] . "' IN BOOLEAN MODE) ";
			}
		}*/
		unset($filter['search_string']);		

		if ( $is_tour ) {
			if ( isset($filter['eic.gender = 1']) ) unset($filter['eic.gender = 1']);

			if ( ! $is_upcomingtour ) {
				$filter['eic.is_tour = 1'] = array();
			}
			else {
				$filter['eic.is_upcoming = 1'] = array();
			}
		} 
		else {
			// Only escorts with base city (or city tour) will be shown in this list
			if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
				$filter['eic.is_base = 1'] = array();
			}
			else {
				$filter['eic.is_upcoming = 0'] = array();
			}
		}

		$join = " ";

		if ( isset($filter['incall-outcall']) && $filter['incall-outcall'] ) {
			if($filter['incall-outcall'] == 1){
				$filter['e.incall_type IS NOT NULL'] = array();
			}
			elseif($filter['incall-outcall'] == 2){
				$filter['e.outcall_type IS NOT NULL'] = array();
			}
		}
		unset($filter['incall-outcall']);

		if ( isset($filter['orientation']) && count($filter['orientation']) ) {			
			$filter['e.sex_orientation IN ( ' . implode(',', $filter['orientation']) . ' )'] = array();
		}
		unset($filter['orientation']);

		$current_currency = "USD";
		if ( isset($filter['currency']) && $filter['currency'] ) {
			$current_currency = $filter['currency'];
		}
		
		$has_price_filter = false;
		if ( $filter['currency'] || $filter['price-from'] || $filter['price-to'] ) {
			$currency_rate = Model_Currencies::getSelectedCurrencyRate($current_currency);
			$has_price_filter = $current_currency;
			$join .= " 
				INNER JOIN currencies cur ON cur.id = e.incall_currency 
				INNER JOIN exchange_rates er ON er.iso = cur.title 
			";
		}
		
		if ( isset($filter['price-from']) && $filter['price-from'] ) { 
			$filter['(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' >= ' . $filter['price-from']] = array();
		}
		
		if ( isset($filter['price-to']) && $filter['price-to'] ) {
			$filter['(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' <= ' . $filter['price-to']] = array();
		}
		
		unset($filter['currency']);
		unset($filter['price-from']);
		unset($filter['price-to']);

		if ( isset($filter['cr.id = ?']) ) {
			$join .= " INNER JOIN countries cr ON cr.id = e.country_id ";
		}

		if ( isset($filter['r.id = ?']) ) {
			$join .= " INNER JOIN regions r ON r.id = eic.region_id	";
		}

		if ( isset($filter['is-online-now']) && $filter['is-online-now'] ) {
			$filter['ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1'] = array();
		}
		
		if ( (isset($filter['is-online-now']) && $filter['is-online-now']) || isset($filter['ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1']) ) {
			$join .= " INNER JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id ";
		}
		unset($filter['is-online-now']);
		unset($filter['ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1']);

		if ( isset($filter['with-video']) && $filter['with-video'] ) {
			$filter['e.has_video = 1'] = array();
			//$join .= " INNER JOIN video v ON v.escort_id = e.id ";
		}
		unset($filter['with-video']);

		// Caching logic 
		
		ksort($filter);
		$cache = Zend_Registry::get('cache');
		$cache_filter = self::_generateCacheKey($filter);
		$cache_filter = preg_replace('#[^a-zA-Z0-9<>()=_]#', '_', $cache_filter);
		$cache_wq = '';
		
		foreach($where_query as $where_q){
			if(strlen($where_q['filter_query']) > 0 ){
				$cache_wq .= '_' . $where_q['filter_query'] . '_';
			}
		}
		
		$cache_wq = preg_replace('#[^a-zA-Z0-9<>()=_]#', '_', $cache_wq);
		$cache_key = 'active_filters_'.Cubix_Application::getId().'_';
				
		$cache_key .= sha1($cache_filter.'_'.$cache_wq);
		/*$filter_benchmark = array(
			'filter' => $cache_filter,
			'where_query' => $cache_wq,
			'cache_key' => $cache_key
		);*/
		if ( !$results = $cache->load($cache_key) ) {
			$where = self::getWhereClause($filter, true);
			//var_dump($where);die;

			if(is_null($where)){ 
				$where = ' 1 ';
			}

			$wqa = array();
			if ( count($where_query) ) {
				foreach( $where_query as $wq ) {

					if ( strlen($wq['query']) ) {
						$where_query = ' AND ' . $wq['filter_query'];

						$where .= $where_query;
						$wqa[] = array('filter_query' => $wq['filter_query'], 'query' => $wq['query'] );
					}
				}
			}

			$metadata = self::db()->describeTable('escort_filter_data');
			$column_names = array_keys($metadata);
			unset($column_names[0]);	

			$select = array('COUNT(*) as cnt ');
			foreach ( $column_names as $column ) {
				$select[] = 'SUM(' . $column . ') AS ' . $column;
			}
			
			$sql = '
				SELECT 
					' . implode(', ', $select) . ' 
				FROM ( SELECT fd.*
				FROM escort_filter_data fd
				INNER JOIN escorts_in_cities eic ON eic.escort_id = fd.escort_id
				INNER JOIN escorts e ON e.id = eic.escort_id
				INNER JOIN cities ct ON eic.city_id = ct.id
				' . $join . '
				' . (! is_null($where) ? 'WHERE ' . $where : '') . '
				GROUP BY fd.escort_id ) f
			';
			
			//var_dump($sql);die;
			
			// TO FORCE INNER JOIN currencies, exchange_rates tables
			if($has_price_filter){
				$filter['currency'] = $has_price_filter;
			}

            $results = array('result' => self::db()->fetchRow($sql), 'global_filter' => $filter , 'where_query' => $wqa, 'is_upcoming' => $is_upcomingtour, 'is_tour' => $is_tour);
            $cache->save($results, $cache_key);
		}
		/*else{
			$filter_benchmark['from_cache'] = 1;
		}
		self::db()->insert('filter_benchmark', $filter_benchmark);*/
		return $results;
	}
	
	public static function getFilterDefines($p_top_category, $is_upcomingtour, $is_tour)
	{
		$cache_key = 'defines_cache_key_' . Cubix_Application::getId(). '_lang_'. Cubix_I18n::getLang().'_'. preg_replace('#[^a-zA-Z0-9_]#', '_', $p_top_category);
		$cache = Zend_Registry::get('cache');
		if ( !$defined_values = $cache->load($cache_key) ) {
			$defs = Zend_Registry::get('definitions');
			$defined_values = $defs['escorts_filter_v2'];

			switch($p_top_category )
			{
				case 'newest':
					$filter["eic.gender = 1"] = array();
					continue;
				case 'escorts':
					$filter["eic.gender = 1 AND e.type = 1"] = array();
					continue;
				case 'agency-escorts':
					$filter["eic.gender = 1 AND e.type = 1"] = array();
					continue;
				case 'bdsm':
					$filter["eic.gender = 1 AND e.type = 2"] = array();
					continue;
				case 'bdsm-boys':
					$filter["eic.gender = 2 AND e.type = 2"] = array();
					continue;
				case 'bdsm-trans':
					$filter["eic.gender = 3 AND e.type = 2"] = array();
					continue;        
				case 'boys':
					$filter["eic.gender = 2 AND e.type = 1"] = array();
					continue;
				case 'trans':
					$filter["eic.gender = 3 AND e.type = 1"] = array();
					continue;
				case 'citytours':
					$filter["eic.is_tour = 1"] = array();
					continue;
				case 'upcomingtours':
					$filter["eic.is_upcoming = 1"] = array();
					continue;
				case 'massage':
					$filter["eic.gender = 1 AND e.type = 3"] = array();
					continue;
				case 'massage-boys':
					$filter["eic.gender = 2 AND e.type = 3"] = array();
					continue;
				case 'massage-trans':
					$filter["eic.gender = 3 AND e.type = 3"] = array();
					continue;
				default:
					$filter["eic.gender = 1"] = array();
			}	

			$exists_filters = self::getActiveFilter($filter, array(), $is_upcomingtour, $is_tour);
			$exists_filter_results = $exists_filters['result'];
			
			foreach($defined_values as $title => $list){
								
				if($title == 'available-for-incall' || $title == 'available-for-outcall'){
					$prefix = 'available_for_';
				}
				else{
					$prefix = str_replace('-','_',$title ) . '_';
				}
				
				foreach( $list as $k => $value ) {
					$key = $prefix . strtolower(str_replace('-','_', $value['value']));

					if ( !$exists_filter_results->$key) {
						unset($defined_values[$title][$k]);
					}
				}
			}
						
			$cache->save($defined_values, $cache_key);
		}
		return $defined_values;
			
	}		
			
	public static function getMainPremiumSpot()
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		$sql = '
			SELECT
				/* escorts table */
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
				e.rates, e.incall_type, e.outcall_type, e.is_new, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status, e.is_pornstar, e.last_hand_verification_date,
				e.products, e.slogan, e.date_last_modified,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.is_suspicious,e.comment_count, e.review_count,
				
				ct.title_' . $lng . ' AS city,

				/* seo_entity_instances table */
				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,

				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				/* misc */
				1 AS is_premium, ' . Cubix_Application::getId() . ' AS application_id, e.hh_is_active, e.is_online
			FROM escorts e
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = e.id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 2 AND se.slug = "escort")
			WHERE
				eic.gender = ' . GENDER_FEMALE . ' AND e.is_main_premium_spot = 1
			GROUP BY e.id
		';

		$cache = Zend_Registry::get('cache');
		$cache_key = 'main_premium_spot_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang();

		if ( ! $escorts = $cache->load($cache_key) ) {
			
			$escorts = self::db()->fetchAll($sql);
			
			/* seo heading escorts */
			if ($escorts)
			{
				$es = array();
				foreach ($escorts as $e)
				{
					$es[] = $e->id;
				}
				$es_str = implode(',', $es);

				$query = "SELECT e.id, sei.heading_escort_{$lng} as heading_escort
					FROM escorts e
					INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
					INNER JOIN seo_entity_instances sei ON sei.primary_id = eic.city_id
					INNER JOIN seo_entities se ON se.application_id = " . Cubix_Application::getId() . " AND se.slug = 'city' AND sei.entity_id = se.id
					WHERE e.id IN ({$es_str})
				";
				$result = self::db()->fetchAll($query);

				foreach ($result as $r)
				{				
					foreach ($escorts as $e)
					{
						if ($e->id == $r->id)
						{
							$e->heading_escort = $r->heading_escort;
							break;
						}
					}
				}
			}
			/**/

			$cache->save($escorts, $cache_key, array());
		}

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		shuffle($escorts);

		// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$sess->main_premium_spot = array();
		foreach ( $escorts as $escort ) {
			$sess->main_premium_spot[] = $escort->showname;
		}
		// </editor-fold>


		return $escorts;
	}

    public static function getFilteredWhereQuery($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $only_vip = false, $s_filter_params = array(), $is_grouped = false, $geo_data = array(), $list_type = 'simple', $city_id = null, $country_id = null, $current_currency = null)
    {
        $lng = Cubix_I18n::getLang();
        $sql_join = ' ';
        $fields = ' ';
        $is_premium = "eic.is_premium,";
        $OrderInactiveVipPremiums = '';
        if($filter['premium_regular']){
            unset($filter['premium_regular']);
        }
        // Remove VIP and premium escorts from normal listing
        elseif ( ! in_array($ordering, array('vips', 'premiums')) ) {
            $filter['eic.participate_in_listing = 1'] = array();
            unset($filter['eic.is_inactive = 0']);
            //$OrderInactiveVipPremiums = " eic.is_inactive ASC, eic.is_vip DESC, eic.is_premium DESC, ";
            $is_premium = "IF( eic.is_premium OR
	EXISTS (
		SELECT
			1
		FROM
			escorts_in_cities AS eic1
		WHERE
			eic1.escort_id = eic.escort_id
		AND eic1.is_premium = 1
	), 1, 0) AS is_premium,";
        }
        else {

            unset($filter['eic.is_premium = 0 AND eic.is_vip = 0']);
            unset($filter['(( e.package_id <> 103 AND e.package_id <> 104) OR e.package_id IS NULL)']);
            $filter['eic.is_inactive = 0'] = array();
        }
        // Remove VIP and premium escorts from normal listing

        if ( isset($filter['remove_package_query']) ) {
            unset($filter['eic.is_premium = 0 AND eic.is_vip = 0']);
            unset($filter['(( e.package_id <> 103 AND e.package_id <> 104) OR e.package_id IS NULL)']);
            unset($filter['remove_package_query']);
        }

        if ( isset($s_filter_params['currency']) && $s_filter_params['currency'] && ($s_filter_params['price-from'] || $s_filter_params['price-to']) ) {
            $current_currency = $s_filter_params['currency'];
        }

        $currency_rate = Model_Currencies::getSelectedCurrencyRate($current_currency);
        if ( isset($s_filter_params['price-from']) && $s_filter_params['price-from'] ) {
            $filter[] = '(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' >= ' . $s_filter_params['price-from'];
        }
        if ( isset($s_filter_params['price-to']) && $s_filter_params['price-to'] ) {
            $filter[] = '(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' <= ' . $s_filter_params['price-to'];
        }

        if (date('G') < 2)
            $date = 2;
        else
            $date = date('G');

        if ( 'price' == substr($ordering, 0, 5) ) {
            $filter[] = 'e.incall_price IS NOT NULL';
        }

        /* #### Advanced search sql
        No need to check s_filter_params as for vip and premium they are already added in filter array
        UPDATE (EDUARD): In this version we as someone mentioned above, it doesnt work as it should so I removed the condition
        */
        if ( TRUE or in_array($ordering, array('vips', 'premiums')) ) {
            if ( isset($s_filter_params['gender']) && count($s_filter_params['gender']) ) {
                unset($filter['eic.gender = 1']);
                $filter['eic.gender = ? '] = intval($s_filter_params['gender']);
            }

            if ( isset($s_filter_params['keywords']) && count($s_filter_params['keywords']) ) {
                $filter[] = 'ek.keyword_id IN ( ' . implode(',', $s_filter_params['keywords']) . ' )';
                $sql_join .= " INNER JOIN escort_keywords ek ON ek.escort_id = e.id ";
            }

            if ( isset($s_filter_params['orientation']) && count($s_filter_params['orientation']) ) {
                $filter[] = 'e.sex_orientation IN ( ' . implode(',', $s_filter_params['orientation']) . ' )';
            }

            if ( isset($s_filter_params['ethnicity']) && count($s_filter_params['ethnicity']) ) {
                $filter[] = 'e.ethnicity IN ( ' . implode(',', $s_filter_params['ethnicity']) . ' )';
            }

            if ( isset($s_filter_params['language']) && count($s_filter_params['language']) ) {
                $languagesQ = '';
                foreach ($s_filter_params['language'] as $value) {
                    $languagesQ .= " e.languages like '%".$value."%' OR";
                }
                $languagesQ = substr($languagesQ, 0, -2);
                $filter[] = " (".$languagesQ. " ) ";
            }

            if ( isset($s_filter_params['nationality']) && count($s_filter_params['nationality']) ) {
                $languagesQ = ' e.nationality_id IN(';
                foreach ($s_filter_params['nationality'] as $value) {
                    $languagesQ .= $value.",";
                }
                $languagesQ = substr($languagesQ, 0, -1);
                $languagesQ .= ") ";
                $filter[] = $languagesQ;
            }

            if ( isset($s_filter_params['hair-color']) && count($s_filter_params['hair-color']) ) {
                $filter[] = 'e.hair_color IN ( ' . implode(',', $s_filter_params['hair-color']) . ' )';
            }

            if ( isset($s_filter_params['hair-length']) && count($s_filter_params['hair-length']) ) {
                $filter[] = 'e.hair_length IN ( ' . implode(',', $s_filter_params['hair-length']) . ' )';
            }

            if ( isset($s_filter_params['cup-size']) && count($s_filter_params['cup-size']) ) {
                $filter[] = 'e.cup_size IN ( "' . implode('","', $s_filter_params['cup-size']) . '" )';
            }

            if ( isset($s_filter_params['eye-color']) && count($s_filter_params['eye-color']) ) {
                $filter[] = 'e.eye_color IN ( ' . implode(',', $s_filter_params['eye-color']) . ' )';
            }

            if ( isset($s_filter_params['pubic-hair']) && count($s_filter_params['pubic-hair']) ) {
                $filter[] = 'e.pubic_hair IN ( ' . implode(',', $s_filter_params['pubic-hair']) . ' )';
            }

            if ( isset($s_filter_params['sex-availability']) && count($s_filter_params['sex-availability']) ) {
                $service_forQ = '(';

                foreach ($s_filter_params['sex-availability'] as $value) {
                    $service_forQ .= " FIND_IN_SET('". $value."' ,e.sex_availability) OR";
                }
                $service_forQ = substr($service_forQ, 0, -2).") ";
                $filter[] = $service_forQ;

            }

            if ( isset($s_filter_params['service']) && count($s_filter_params['service']) ) {
                $filter[] = 'es.service_id IN ( ' . implode(',', $s_filter_params['service']) . ' )';
                $sql_join .= " INNER JOIN escort_services es ON es.escort_id = e.id ";
            }

            if ( isset($s_filter_params['incall-outcall']) && $s_filter_params['incall-outcall'] ) {
                if($s_filter_params['incall-outcall'] == 1){
                    $filter[] = 'e.incall_type IS NOT NULL';
                }
                elseif($s_filter_params['incall-outcall'] == 2){
                    $filter[] = 'e.outcall_type IS NOT NULL';
                }
            }

            if ( isset($s_filter_params['incall']) && count($s_filter_params['incall']) ) {
                $incall_string = '';
                foreach($s_filter_params['incall'] as $incall_val){
                    $value = explode('_', $incall_val);
                    $incall_string .= $value[1].',';
                }
                $incall_string = substr($incall_string, 0, -1);
                $filter[] = 'e.incall_type IN ( ' . $incall_string . ' )';
            }

            if ( isset($s_filter_params['incall-hotel-room']) && count($s_filter_params['incall-hotel-room']) ) {
                $filter[] = 'e.incall_hotel_room IN ( ' . implode(',', $s_filter_params['incall-hotel-room']) . ' )';
            }

            if ( isset($s_filter_params['outcall']) && count($s_filter_params['outcall']) ) {
                $outcall_string = '';
                foreach($s_filter_params['outcall'] as $incall_val){
                    $value = explode('_', $incall_val);
                    $outcall_string .= $value[1].',';
                }
                $outcall_string = substr($outcall_string, 0, -1);
                $filter[] = 'e.outcall_type IN ( ' . $outcall_string . ' )';
            }

            if ( isset($s_filter_params['travel-place']) && count($s_filter_params['travel-place']) ) {
                $filter[] = 'e.travel_place IN ( ' . implode(',', $s_filter_params['travel-place']) . ' )';
            }

            if ( (isset($s_filter_params['is-online-now']) && $s_filter_params['is-online-now']) || isset($filter['ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1']) ) {
                $filter[] = 'ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1';
            }
            unset($filter['ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1']);
            unset($filter['is-online-now']);

            if ( isset($filter['online-now']) && $filter['online-now'] ) {
                $filter[] = 'ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1';
            }
            unset($filter['online-now']);
        }

        if(in_array($ordering, array('vips', 'premiums'))) {
            /*if ( isset($s_filter_params['keywords']) && count($s_filter_params['keywords']) ) {
                $sql_join .= " INNER JOIN escort_keywords ek ON ek.escort_id = e.id ";
            }
            if ( isset($s_filter_params['service']) && count($s_filter_params['service']) ) {
                $sql_join .= " INNER JOIN escort_services es ON es.escort_id = e.id ";
            }*/

            if($ordering == 'vips'){
                $fields .= ', if(FIND_IN_SET(18, e.products) > 0, 1, 0 ) as is_vip_plus , if(FIND_IN_SET(19, e.products) > 0, 1, 0 ) as is_vip2';
            }
        }

        /* #### END advanced sql */


        $natural_photo = false;
        if (isset($s_filter_params['with-natural-photo']) && $s_filter_params['with-natural-photo'] ||
            isset($filter['with-natural-photo']) && $filter['with-natural-photo']) {
            $natural_photo = true;
            $sql_join .=  " INNER JOIN natural_pics n_pics ON n_pics.escort_id = e.id ";
        }

        unset($filter['with-natural-photo']);
        /*if ( isset($s_filter_params['with-video']) && $s_filter_params['with-video'] ) {
            $sql_join .= " INNER JOIN video v ON v.escort_id = e.id ";
        }*/
        /* #### new search sql */



        // Only escorts with base city (or city tour) will be shown in this list
        if ( ! $city_id && ! $country_id ) {
            $filter['eic.is_base = 1'] = array();
        } else {
            $filter[] = 'eic.is_upcoming = 0';
        }

        if ( $ordering == "premiums" || $ordering == "vips" ) {
            $del_val = "eic.is_base = 1";
            while(($key = array_search($del_val, $filter)) !== false) {
                unset($filter[$key]);
            }
        }


        $show_splited_agency_escorts = true;
        if ( isset($filter['show_all_agency_escorts']) ) {
            unset($filter['show_all_agency_escorts']);
            $show_splited_agency_escorts = false;
        }

        if ( in_array($ordering, array('vips', 'premiums')) ) {
            unset($filter['show_all_agency_escorts']);
            unset($filter['eic.should_display_agency_escort = 1']);
            $show_splited_agency_escorts = false;
        }

        if ( $show_splited_agency_escorts ) {
            $filter['eic.should_display_agency_escort = 1'] = array();
        }

        //$with_video = false;
        if ( isset($filter['with-video']) && $filter['with-video'] ) {
            $filter['e.has_video = 1'] = array();
            //$with_video = true;
            //$sql_join .= " INNER JOIN video v ON v.escort_id = e.id ";
        }
        unset($filter['with-video']);


        if(isset($s_filter_params['alert_id']) && $s_filter_params['alert_id'] > 0){
            $cur_user = Model_Users::getCurrent();
            if($cur_user && $cur_user->user_type == 'member'){
                $cityAlertModel =  new Model_CityAlerts();
                $escortIds = $cityAlertModel->getAlertEscorts($s_filter_params['alert_id'],$cur_user->id);

                if($escortIds){
                    $escortExist = array();
                    $in = 'e.id IN (';
                    foreach ($escortIds as $escortId) {
                        if(!in_array($escortId['escort_id'], $escortExist)){
                            $in .= $escortId['escort_id'].',';
                            $escortExist[] = $escortId['escort_id'];
                        }
                    }
                    $in = substr($in, 0, -1);
                    $in .= ')';
                    $filter[$in] = array();
                }
            }else{
                die();
            }
        }


        $where = self::getWhereClause($filter, true);
        $order = self::_mapSorting($ordering, $is_grouped, $list_type);

        /*if ( $with_video ) {
            $filter['with-video'] = 'with-video';
        }*/

        if($natural_photo){
            $filter['with-natural-photo'] = 'with-natural-photo';
        }

        $page = (int) $page;
        if ( $page < 1 ) $page = 1;
        $limit = ($page_size * ($page - 1)) . ', ' . $page_size;

        $group_by = " GROUP BY eic.escort_id ";
        $distance = ', NULL AS distance ';

        if ( $ordering == 'close-to-me' && count($geo_data) && !in_array($ordering, array('vips', 'premiums'))) {
            if (array_key_exists("latitude", $geo_data) && strlen($geo_data['latitude']) > 0 && array_key_exists("longitude", $geo_data) && strlen($geo_data['longitude']) > 0) {
                $distance = ',
					((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						))
						)
					)) AS distance
				';
            }
        }

        $fields .= $distance;
        is_null($where) ? $where = ' 1 ' . Cubix_Countries::blacklistCountryWhereCase() : $where .= Cubix_Countries::blacklistCountryWhereCase();

        $having = "";

        // Setup "is online" filter
        // -----------------------------
        if(isset($s_filter_params['is-online-now']) && $s_filter_params['is-online-now'] == '1') {
            $having .= ' is_login = 1 ';
        }
        // -----------------------------

        $sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar, e.last_hand_verification_date, e.is_inactive,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated, ct.slug AS city_slug, ct.id AS city_id, ct.title_' . $lng . ' AS city, e.rates,
				e.incall_type, e.outcall_type, e.is_new, cr.id AS country_id, cr.slug AS country_slug, cr.title_' . $lng . ' AS country, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_args, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
				e.travel_place, e.email, e.website, e.phone_instr, e.phone_instr_no_withheld, e.phone_instr_other,
				e.phone_country_id, e.disable_phone_prefix, e.phone_number_free AS phone_number,
				IF (ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE), 1, 0) AS is_login, e.allow_show_online,
				e.hit_count, e.slogan, e.incall_price, e.date_last_modified, '.$is_premium.' e.is_vip, e.hh_is_active,
				e.is_on_tour AS is_tour, e.tour_date_from, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city,
				e.is_suspicious, e.is_online, ct.id AS city_id, IF((e.about_'.$lng.' IS NULl OR e.about_'.$lng.' = ""), e.about_en, e.about_'.$lng.') AS about, e.agency_name,
				(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' AS incall_price_converted,
				e.agency_slug, e.agency_id, ulrt.refresh_date AS refresh_date ' . $fields . '
			FROM escorts_in_cities eic /*USE INDEX(by_googo3)*/
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
			/*LEFT JOIN countries crtt ON crtt.id = ctt.country_id*/
			' . $sql_join . '
			LEFT JOIN currencies cur ON cur.id = e.incall_currency
			LEFT JOIN exchange_rates er ON er.iso = cur.title

			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			' . $group_by . (! empty($having) ? ' HAVING ' . $having : '' ) . ' 
			ORDER BY ' .$OrderInactiveVipPremiums. $order . '
			LIMIT ' . $limit . '
		';


        $escorts = self::db()->fetchAll($sql);
        $count = self::db()->fetchOne('SELECT FOUND_ROWS()');




        return $escorts;
    }

    public static function getQueryFrom (&$filter, &$ordering = 'random', &$page = 1, &$page_size = 50, &$count = 0, $sess_name = false, &$only_vip = false, &$s_filter_params = array(), &$is_grouped = false, &$geo_data = array(), &$list_type = 'simple', &$city_id = null, &$country_id = null, &$current_currency = null, &$vipPremCount = 0,  &$differenceFrom = 0, &$differenceTo = 0)
    {
        $sql_join = ' ';
        $fields = ' ';
        //$is_premium = ",eic.is_premium";
        $OrderInactiveVipPremiums = '';
        if($filter['premium_regular']){
            unset($filter['premium_regular']);
        }
        // Remove VIP and premium escorts from normal listing
        elseif ( ! in_array($ordering, array('vips', 'premiums')) ) {
            $filter['eic.participate_in_listing = 1'] = array();
            unset($filter['eic.is_inactive = 0']);
            $OrderInactiveVipPremiums = " eic.is_inactive ASC, ";
            /*$is_premium = ",IF( eic.is_premium OR
	EXISTS (
		SELECT
			1
		FROM
			escorts_in_cities AS eic1
		WHERE
			eic1.escort_id = eic.escort_id
		AND eic1.is_premium = 1
	), 1, 0) AS is_premium";*/
        }
        else {

            unset($filter['eic.is_premium = 0 AND eic.is_vip = 0']);
            unset($filter['(( e.package_id <> 103 AND e.package_id <> 104) OR e.package_id IS NULL)']);
            $filter['eic.is_inactive = 0'] = array();
        }
        // Remove VIP and premium escorts from normal listing

        if ( isset($filter['remove_package_query']) ) {
            unset($filter['eic.is_premium = 0 AND eic.is_vip = 0']);
            unset($filter['(( e.package_id <> 103 AND e.package_id <> 104) OR e.package_id IS NULL)']);
            unset($filter['remove_package_query']);
        }

        if ( isset($s_filter_params['currency']) && $s_filter_params['currency'] && ($s_filter_params['price-from'] || $s_filter_params['price-to']) ) {
            $current_currency = $s_filter_params['currency'];
        }

        $currency_rate = Model_Currencies::getSelectedCurrencyRate($current_currency);
        $inner_join_rates = false;
        if ( isset($s_filter_params['price-from']) && $s_filter_params['price-from'] ) {
            $filter[] = '(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' >= ' . $s_filter_params['price-from'];
            $inner_join_rates = true;
        }
        if ( isset($s_filter_params['price-to']) && $s_filter_params['price-to'] ) {
            $filter[] = '(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' <= ' . $s_filter_params['price-to'];
            $inner_join_rates = true;
        }

        if($inner_join_rates){
            $sql_join .= " INNER JOIN currencies cur ON cur.id = e.incall_currency ";
            $sql_join .= " INNER JOIN exchange_rates er ON er.iso = cur.title ";
        }

        if (date('G') < 2)
            $date = 2;
        else
            $date = date('G');

        if ( 'price' == substr($ordering, 0, 5) ) {
            $filter[] = 'e.incall_price IS NOT NULL';
        }

        /* #### Advanced search sql
        No need to check s_filter_params as for vip and premium they are already added in filter array*/
        if ( ! in_array($ordering, array('vips', 'premiums')) ) {
            if ( isset($s_filter_params['gender']) && count($s_filter_params['gender']) ) {
                unset($filter['eic.gender = 1']);
                $filter['eic.gender = ? '] = intval($s_filter_params['gender']);
            }

            if ( isset($s_filter_params['keywords']) && count($s_filter_params['keywords']) ) {
                $filter[] = 'ek.keyword_id IN ( ' . implode(',', $s_filter_params['keywords']) . ' )';
                $sql_join .= " INNER JOIN escort_keywords ek ON ek.escort_id = e.id ";
            }

            if ( isset($s_filter_params['orientation']) && count($s_filter_params['orientation']) ) {
                $filter[] = 'e.sex_orientation IN ( ' . implode(',', $s_filter_params['orientation']) . ' )';
            }

            if ( isset($s_filter_params['ethnicity']) && count($s_filter_params['ethnicity']) ) {
                $filter[] = 'e.ethnicity IN ( ' . implode(',', $s_filter_params['ethnicity']) . ' )';
            }

            if ( isset($s_filter_params['language']) && count($s_filter_params['language']) ) {
                $languagesQ = '';
                foreach ($s_filter_params['language'] as $value) {
                    $languagesQ .= " e.languages like '%".$value."%' OR";
                }
                $languagesQ = substr($languagesQ, 0, -2);
                $filter[] = " (".$languagesQ. " ) ";
            }

            if ( isset($s_filter_params['nationality']) && count($s_filter_params['nationality']) ) {
                $languagesQ = ' e.nationality_id IN(';
                foreach ($s_filter_params['nationality'] as $value) {
                    $languagesQ .= $value.",";
                }
                $languagesQ = substr($languagesQ, 0, -1);
                $languagesQ .= ") ";
                $filter[] = $languagesQ;
            }

            if ( isset($s_filter_params['hair-color']) && count($s_filter_params['hair-color']) ) {
                $filter[] = 'e.hair_color IN ( ' . implode(',', $s_filter_params['hair-color']) . ' )';
            }

            if ( isset($s_filter_params['hair-length']) && count($s_filter_params['hair-length']) ) {
                $filter[] = 'e.hair_length IN ( ' . implode(',', $s_filter_params['hair-length']) . ' )';
            }

            if ( isset($s_filter_params['cup-size']) && count($s_filter_params['cup-size']) ) {
                $filter[] = 'e.cup_size IN ( "' . implode('","', $s_filter_params['cup-size']) . '" )';
            }

            if ( isset($s_filter_params['eye-color']) && count($s_filter_params['eye-color']) ) {
                $filter[] = 'e.eye_color IN ( ' . implode(',', $s_filter_params['eye-color']) . ' )';
            }

            if ( isset($s_filter_params['pubic-hair']) && count($s_filter_params['pubic-hair']) ) {
                $filter[] = 'e.pubic_hair IN ( ' . implode(',', $s_filter_params['pubic-hair']) . ' )';
            }

            if ( isset($s_filter_params['sex-availability']) && count($s_filter_params['sex-availability']) ) {
                $service_forQ = '(';

                foreach ($s_filter_params['sex-availability'] as $value) {
                    $service_forQ .= " FIND_IN_SET('". $value."' ,e.sex_availability) OR";
                }
                $service_forQ = substr($service_forQ, 0, -2).") ";
                $filter[] = $service_forQ;

            }

            if ( isset($s_filter_params['service']) && count($s_filter_params['service']) ) {
                $filter[] = 'es.service_id IN ( ' . implode(',', $s_filter_params['service']) . ' )';
                $sql_join .= " INNER JOIN escort_services es ON es.escort_id = e.id ";
            }

            if ( isset($s_filter_params['incall-outcall']) && $s_filter_params['incall-outcall'] ) {
                if($s_filter_params['incall-outcall'] == 1){
                    $filter[] = 'e.incall_type IS NOT NULL';
                }
                elseif($s_filter_params['incall-outcall'] == 2){
                    $filter[] = 'e.outcall_type IS NOT NULL';
                }
            }

            if ( isset($s_filter_params['incall']) && count($s_filter_params['incall']) ) {
                $incall_string = '';
                foreach($s_filter_params['incall'] as $incall_val){
                    $value = explode('_', $incall_val);
                    $incall_string .= $value[1].',';
                }
                $incall_string = substr($incall_string, 0, -1);
                $filter[] = 'e.incall_type IN ( ' . $incall_string . ' )';
            }

            if ( isset($s_filter_params['incall-hotel-room']) && count($s_filter_params['incall-hotel-room']) ) {
                $filter[] = 'e.incall_hotel_room IN ( ' . implode(',', $s_filter_params['incall-hotel-room']) . ' )';
            }

            if ( isset($s_filter_params['outcall']) && count($s_filter_params['outcall']) ) {
                $outcall_string = '';
                foreach($s_filter_params['outcall'] as $incall_val){
                    $value = explode('_', $incall_val);
                    $outcall_string .= $value[1].',';
                }
                $outcall_string = substr($outcall_string, 0, -1);
                $filter[] = 'e.outcall_type IN ( ' . $outcall_string . ' )';
            }

            if ( isset($s_filter_params['travel-place']) && count($s_filter_params['travel-place']) ) {
                $filter[] = 'e.travel_place IN ( ' . implode(',', $s_filter_params['travel-place']) . ' )';
            }

            /*if ( (isset($s_filter_params['is-online-now']) && $s_filter_params['is-online-now']) || $ordering == 'last-connection'  ) {
                $sql_join .= " INNER JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id ";
            }*/

            if ( isset($filter['is-online-now']) && $filter['is-online-now'] || isset($filter['ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1']) ) {
                /*$sql_join .= " INNER JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id ";*/
                $filter[] = 'ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1';
            }
            unset($filter['is-online-now']);
            unset($filter['ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1']);
        }
        else{
            if ( isset($s_filter_params['keywords']) && count($s_filter_params['keywords']) ) {
                $sql_join .= " INNER JOIN escort_keywords ek ON ek.escort_id = e.id ";
            }
            if ( isset($s_filter_params['service']) && count($s_filter_params['service']) ) {
                $sql_join .= " INNER JOIN escort_services es ON es.escort_id = e.id ";
            }

            if($ordering == 'vips'){
                $fields .= ', if(FIND_IN_SET(18, e.products) > 0, 1, 0 ) as is_vip_plus , if(FIND_IN_SET(19, e.products) > 0, 1, 0 ) as is_vip2';
            }
        }

        /* #### END advanced sql */


        $natural_photo = false;
        if (isset($s_filter_params['with-natural-photo']) && $s_filter_params['with-natural-photo'] ||
            isset($filter['with-natural-photo']) && $filter['with-natural-photo']) {
            $natural_photo = true;
            $sql_join .=  " INNER JOIN natural_pics n_pics ON n_pics.escort_id = e.id ";
        }

        unset($filter['with-natural-photo']);
        /*if ( isset($s_filter_params['with-video']) && $s_filter_params['with-video'] ) {
            $sql_join .= " INNER JOIN video v ON v.escort_id = e.id ";
        }*/
        /* #### new search sql */



        // Only escorts with base city (or city tour) will be shown in this list
        if ( ! $city_id ) {
            $filter['eic.is_base = 1'] = array();
        } else {
            $filter[] = 'eic.is_upcoming = 0';
        }

        if ( $ordering == "premiums" || $ordering == "vips" ) {
            $del_val = "eic.is_base = 1";
            while(($key = array_search($del_val, $filter)) !== false) {
                unset($filter[$key]);
            }
        }


        $show_splited_agency_escorts = true;
        if ( isset($filter['show_all_agency_escorts']) ) {
            unset($filter['show_all_agency_escorts']);
            $show_splited_agency_escorts = false;
        }

        if ( in_array($ordering, array('vips', 'premiums')) ) {
            unset($filter['show_all_agency_escorts']);
            unset($filter['eic.should_display_agency_escort = 1']);
            $show_splited_agency_escorts = false;
        }

        if ( $show_splited_agency_escorts ) {
            $filter['eic.should_display_agency_escort = 1'] = array();
        }

        //$with_video = false;
        if ( isset($filter['with-video']) && $filter['with-video'] ) {
            $filter['e.has_video = 1'] = array();
            //$with_video = true;
            //$sql_join .= " INNER JOIN video v ON v.escort_id = e.id ";
        }
        unset($filter['with-video']);


        if(isset($s_filter_params['alert_id']) && $s_filter_params['alert_id'] > 0){
            $cur_user = Model_Users::getCurrent();
            if($cur_user && $cur_user->user_type == 'member'){
                $cityAlertModel =  new Model_CityAlerts();
                $escortIds = $cityAlertModel->getAlertEscorts($s_filter_params['alert_id'],$cur_user->id);

                if($escortIds){
                    $escortExist = array();
                    $in = 'e.id IN (';
                    foreach ($escortIds as $escortId) {
                        if(!in_array($escortId['escort_id'], $escortExist)){
                            $in .= $escortId['escort_id'].',';
                            $escortExist[] = $escortId['escort_id'];
                        }
                    }
                    $in = substr($in, 0, -1);
                    $in .= ')';
                    $filter[$in] = array();
                }
            }else{
                die();
            }
        }
        $where = self::getWhereClause($filter, true);
        $order = self::_mapSorting($ordering, $is_grouped, $list_type);

        /*if ( $with_video ) {
            $filter['with-video'] = 'with-video';
        }*/

        if($natural_photo){
            $filter['with-natural-photo'] = 'with-natural-photo';
        }

        $page = (int) $page;
        if ( $page < 1 ) $page = 1;

        // This difference appears only when there is a page
        // where we have vip / premiums AND regular escorts
        // in this case we translate the point of selecting regular escorts
        // by amount of vip / premiums that are in that crossing page
        // --------------------------------------------
        $skip = ($page_size * ($page - 1)) - $differenceFrom;
        $take = $page_size - $differenceTo;
        $limit = $skip . ', ' . $take;
        // --------------------------------------------

        $group_by = " GROUP BY eic.escort_id ";
        $distance = ', NULL AS distance ';

        is_null($where) ? $where = ' 1 ' . Cubix_Countries::blacklistCountryWhereCase() : $where .= Cubix_Countries::blacklistCountryWhereCase();

        if ( $ordering == 'close-to-me' && count($geo_data) && is_null($city_id) && is_null($country_id) && !in_array($ordering, array('vips', 'premiums'))) {
            if (array_key_exists("latitude", $geo_data) && strlen($geo_data['latitude']) > 0 && array_key_exists("longitude", $geo_data) && strlen($geo_data['longitude']) > 0) {
                /*$distance = ',
					((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - eic.latitude))/2), 2) +
							COS(RADIANS(eic.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - eic.longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - eic.latitude))/2), 2) +
							COS(RADIANS(eic.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - eic.longitude))/2), 2)
						))
						)
					)) AS distance
				';*/

                $nearby_city_ids = Model_Cities::getNearbyCityIds($geo_data);
                if(strlen($nearby_city_ids) > 0){
                    $where .= ' AND eic.city_id IN ('.$nearby_city_ids.')';
                }
            }
        }

        if($ordering == 'price-asc' || $ordering == 'price-desc'){
            $sql_join .= " INNER JOIN currencies cur ON cur.id = e.incall_currency ";
            $sql_join .= " INNER JOIN exchange_rates er ON er.iso = cur.title";
            $fields .= ',(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' AS incall_price_converted';
        }
        $fields .= $distance;

        // For Newest escorts need to sort by add date
        // For everything else sort by last modification date
        // --------------------------------
        $orderLastModification = '';
        if($ordering != 'newest') {
            $orderLastModification = 'last_modified_date DESC, ';
            $fields .= ',CASE 
                    WHEN ulrt.refresh_date > e.date_last_modified THEN ulrt.refresh_date
                    ELSE e.date_last_modified
                end as last_modified_date';
        }
        // --------------------------------

        return array(
            $fields,
            $sql_join,
            $where,
            $group_by,
            $OrderInactiveVipPremiums,
            $orderLastModification,
            $order,
            $limit
        );
    }

    /*
     * This function is only and only for NEWEST escorts page
     * It is checking if there is any escort available in listing
     * for each checkbox in that page, and returns array of those checkboxes
     * E.X. IF there is no escort for with real pics this checkbox is going to be disabled
     */
    public static function selectExistingFilters ($filter, $ordering = 'random', $page = 1, $page_size = 50, $count = 0, $sess_name = false, $only_vip = false, $s_filter_params = array(), $is_grouped = false, $geo_data = array(), $list_type = 'simple', $city_id = null, $country_id = null, $current_currency = null, $vipPremCount = 0,  $differenceFrom = 0, $differenceTo = 0)
    {
        list(
            $fields,
            $sql_join,
            $where,
            $group_by,
            $OrderInactiveVipPremiums,
            $orderLastModification,
            $order,
            $limit
            ) = self::getQueryFrom($filter, $ordering, $page, $page_size, $count, $sess_name , $only_vip, $s_filter_params, $is_grouped, $geo_data, $list_type, $city_id , $country_id, $current_currency, $vipPremCount,  $differenceFrom, $differenceTo);

        $sql = '
			SELECT eic.escort_id as id
				 '. $fields .'
			FROM escorts_in_cities eic FORCE INDEX (for_selbyids_query)
			INNER JOIN escorts e ON e.id = eic.escort_id
			LEFT JOIN users_last_refresh_time ulrt on ulrt.escort_id = eic.escort_id
			' . $sql_join . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
		';

        $baseSQL = [];
        $checkboxFilters = [
            's_real_pics' => 'e.verified_status = 2',
            's_verified_contact' => 'e.last_hand_verification_date IS NOT NULL',
            's_with_video' => 'e.has_video = 1',
            's_is_online_now' => 'ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1',
            's_available_for_travel' => 'e.travel_place IN (1, 2, 3)'
        ];

        $result = [];
        foreach ($checkboxFilters as $k => $v) {
            if (isset($filter[$v])) {
                unset($checkboxFilters[$k]);
                $result[] = $k;
                continue;
            }
            $tmpSQL = $sql . ' AND ' . $v;
            $baseSQL[] = ' EXISTS(' . $tmpSQL . $group_by . ') AS ' . $k;
        }

        $baseSQL = 'SELECT ' . implode(', ', $baseSQL);
        $filters = self::db()->fetchRow($baseSQL);

        return $filters;
    }

    public static function getEscortIdsOfQuery($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $only_vip = false, $s_filter_params = array(), $is_grouped = false, $geo_data = array(), $list_type = 'simple', $city_id = null, $country_id = null, $current_currency = null, $vipPremCount = 0,  $differenceFrom = 0, $differenceTo = 0)
    {
        list(
            $fields,
            $sql_join,
            $where,
            $group_by,
            $OrderInactiveVipPremiums,
            $orderLastModification,
            $order,
            $limit
        ) = self::getQueryFrom($filter, $ordering, $page, $page_size, $count, $sess_name , $only_vip, $s_filter_params, $is_grouped, $geo_data, $list_type, $city_id , $country_id, $current_currency, $vipPremCount,  $differenceFrom, $differenceTo);

        $sql = '
			SELECT SQL_CALC_FOUND_ROWS
				eic.escort_id as id
				 '. $fields .'
			FROM escorts_in_cities eic FORCE INDEX (for_selbyids_query)
			INNER JOIN escorts e ON e.id = eic.escort_id
			LEFT JOIN users_last_refresh_time ulrt on ulrt.escort_id = eic.escort_id
			/*INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id*/
			' . $sql_join . '
			/*LEFT JOIN currencies cur ON cur.id = e.incall_currency
			LEFT JOIN exchange_rates er ON er.iso = cur.title*/

			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			' . $group_by . '
			ORDER BY ' .$OrderInactiveVipPremiums . $orderLastModification . $order . '
			LIMIT ' . $limit . '
		';

        if($_GET['edo']) {
            die($sql);
        }

        $escorts = self::db()->fetchAll($sql);
        $escort_ids = array();
        foreach($escorts as $escort){
            $escort_ids[] = $escort->id;
        }
        $count = self::db()->fetchOne('SELECT FOUND_ROWS()');

        return $escort_ids;
    }

    public static function getListingDataByIds($escort_ids, $current_currency, $city_id)
    {
        $currency_rate = Model_Currencies::getSelectedCurrencyRate($current_currency);
        $lng = Cubix_I18n::getLang();
        $where = '';
        if($city_id){
            $where = ' AND eic.city_id = '. $city_id;
        }

        $sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar, e.last_hand_verification_date, e.is_inactive,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated, ct.slug AS city_slug, ct.id AS city_id, ct.title_' . $lng . ' AS city, e.rates,
				e.incall_type, e.outcall_type, e.is_new, cr.id AS country_id, cr.slug AS country_slug, cr.title_' . $lng . ' AS country, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_args, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
				e.travel_place, e.email, e.website, e.phone_instr, e.phone_instr_no_withheld, e.phone_instr_other,
				e.phone_country_id, e.disable_phone_prefix, e.phone_number_free AS phone_number,
				IF (ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE), 1, 0) AS is_login, e.allow_show_online,
				e.hit_count, e.slogan, e.incall_price, e.date_last_modified, e.is_vip, e.hh_is_active,
				e.is_on_tour AS is_tour, e.tour_date_from, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city,
				e.is_suspicious, e.is_online, ct.id AS city_id, IF((e.about_'.$lng.' IS NULl OR e.about_'.$lng.' = ""), e.about_en, e.about_'.$lng.') AS about, e.agency_name,
				(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' AS incall_price_converted,
				e.agency_slug, e.agency_id, ulrt.refresh_date AS refresh_date
			FROM escorts_in_cities eic /*USE INDEX(by_googo3)*/
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
			LEFT JOIN currencies cur ON cur.id = e.incall_currency
			LEFT JOIN exchange_rates er ON er.iso = cur.title

			WHERE e.id IN ('. implode(',',$escort_ids) .')' . $where .'
			GROUP BY eic.escort_id
			/*ORDER BY */
		';
        $escorts = self::db()->fetchAll($sql);

        return $escorts;
    }

    public static function getFiltered($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $only_vip = false, $s_filter_params = array(), $is_grouped = false, $geo_data = array(), $list_type = 'simple', $p_top_category = 'escorts', $city_id = null, $country_id = null, $current_currency = null, $has_active_filter = false)
    {
        $count_vips = $count_premiums = 0;
        $vip_premium_escorts = array();
        $cache = Zend_Registry::get('cache');
        $regularStartPage = $page;
        $availableFilters = [];

        // To not hit the performance we just assume that there is impossible to have more than 90 (2 x 45 = two pages) VIP or premium escorts
        // That's why dont make this queries for pages greater than 2
        // -----------------------------------------
        $isPageForVipAllowed = true; // ($page <= 2);
        // -----------------------------------------

        if ( $isPageForVipAllowed && ! in_array($ordering, array('vips', 'premiums'))  && ! in_array($p_top_category, array('newest', 'newest-independent', 'newest-agency-escorts', 'citytours', 'upcomingtours', 'all-joined')) ) {

            $count_premiums = $count_vips = 0;
            $filter___p = $filter;

            $cache_tail = Cubix_Application::getId() .'_'.$p_top_category.'_'. $country_id . '_' . $city_id.'_'. $geo_data['country_iso'].'_'.$page;
            $cache_filter = self::_generateCacheKey(array_merge((array) $filter, (array) $s_filter_params));
            $cache_tail .= preg_replace('#[^a-zA-Z0-9<>=_]#', '_', $cache_filter);

            $filter___p[' eic.is_premium = 1 AND eic.is_vip = 0'] = array();
            if ( ! $city_id && ! $country_id ) {
                // National Premium
                //http://jira.sceon.am/browse/EDIR-1766 is_home argument added  just for this task to detect home

                //if($is_home){
                if($geo_data['country_iso'] && !$has_active_filter){
                    // 4 - Main premium spot
                    $filter___p['( FIND_IN_SET(4, e.products) > 0 OR ( cr.iso = ? AND FIND_IN_SET(15, e.products ) > 0 )) '] = $geo_data['country_iso'];
                }
                else{
                    $filter___p['FIND_IN_SET(4, e.products) > 0'] = array();
                }
                //}
            }
            elseif(!$city_id && $country_id){
                // 15 Country premium spot
                $filter___p['FIND_IN_SET(15, e.products) > 0'] = array();
            }
            else{
                // 5 City premium spot
                $filter___p['FIND_IN_SET(5, e.products) > 0'] = array();
            }


            $cache_key_premiums = sha1("premium_escorts_". $cache_tail);
            if ( ! $vip_premium_escorts['premiums'] = $cache->load($cache_key_premiums) ) {
                $vip_premium_escorts['premiums'] = self::getFilteredWhereQuery($filter___p, 'premiums', 0, self::HOMEPAGE_LISTING_SIZE, $count_premiums, false, false, $s_filter_params, false, array(), $list_type, $city_id, $country_id);
                $cache->save($vip_premium_escorts['premiums'], $cache_key_premiums, array());
            }else{
                $count_premiums = count($vip_premium_escorts['premiums']);
            }

            $filter___v = $filter;
            $filter___v['eic.is_vip = 1'] = array();

            if ( ! $city_id && ! $country_id ) {
                // 4 - Main premium spot
                //if($is_home){
                if($geo_data['country_iso']){

                    $filter___v['( FIND_IN_SET(4, e.products) > 0 OR ( cr.iso = ? AND FIND_IN_SET(15, e.products ) > 0 )) '] = $geo_data['country_iso'];
                }
                else{
                    $filter___v['FIND_IN_SET(4, e.products) > 0'] = array();
                }
                //}
            }
            elseif(!$city_id && $country_id){
                // 15 Country premium spot
                $filter___v['FIND_IN_SET(15, e.products) > 0'] = array();
            }
            else{
                // 5 City premium spot
                $filter___v['FIND_IN_SET(5, e.products) > 0'] = array();
            }

            $cache_key_vips = sha1("vip_escorts_". $cache_tail);

            if ( ! $vip_premium_escorts['vips'] = $cache->load($cache_key_vips) ) {
                $cells_left_for_vip = isset($vip_premium_escorts['premiums']) ? max(self::HOMEPAGE_LISTING_SIZE - count($vip_premium_escorts['premiums']), 0) : self::HOMEPAGE_LISTING_SIZE;
                $vip_premium_escorts['vips'] = self::getFilteredWhereQuery($filter___v, 'vips', 0, $cells_left_for_vip, $count_vips, false, false, $s_filter_params, false, array(), $list_type,  $city_id, $country_id);
                $cache->save($vip_premium_escorts['vips'], $cache_key_vips, array());
            }else{
                $count_vips = count($vip_premium_escorts['vips']);
            }

        }

        //VIP and Prems calculation
        $paid_escorts_count = 0;
        if(count($vip_premium_escorts['vips']) || count($vip_premium_escorts['premiums'])){
            $vip_banner = 1;
            $paid_escorts_count = count($vip_premium_escorts['vips']) + count($vip_premium_escorts['premiums']) + $vip_banner ;

            // Find the page where VIP / Premium ends and Regular starts
            // ----------------------------------------
            $cross = floor($paid_escorts_count / 45);
            // ----------------------------------------

            // This detects if in the page where vip ends and regular starts,
            // are vips and regular being shown at the same time, like 5 vip + 40 regular
            // ----------------------------------
            $isCrossMixed = $cross != ($paid_escorts_count / 45);
            // ----------------------------------

            // Translate the actual page for regular escorts relative to VIP/Premium escorts
            // ----------------------------------
            $regularStartPage = $page - $cross;
            // ----------------------------------

            $page_size = Model_Escort_List::HOMEPAGE_LISTING_SIZE;

        }elseif($page == 1){
            $vip_banner = 1;
            $page_size = Model_Escort_List::HOMEPAGE_LISTING_SIZE - $vip_banner;
        } else {
            $page_size = Model_Escort_List::HOMEPAGE_LISTING_SIZE;
        }

        if($isCrossMixed) $cross++;
        $regularStartPage = max($regularStartPage, 0);

        // Detect if in this page we should show Regular escorts
        // or maybe regular + vip
        // -----------------------------------------
        $typeOfPage = null;
        if($page <= $cross)
            if($isCrossMixed && $page == $cross)
                $typeOfPage = 'mixed';
            else
                $typeOfPage = 'vip';
        elseif($page == $cross && $isCrossMixed )
            $typeOfPage = 'mixed';
        else
            $typeOfPage = 'regular';
        // -----------------------------------------

        if($typeOfPage == 'mixed' || $typeOfPage == 'regular') {

            // If it is page where only regular are going to be visible
            // the we just remove vip and premiums,
            // we selected them only to know how many they are
            // --------------------------------------------
            if($typeOfPage == 'regular') {
                unset($vip_premium_escorts['vips'], $vip_premium_escorts['premiums']);
            }
            // --------------------------------------------

            // Check inside self::getEscortIdsOfQuery
            // this parameters are described inside
            // ---------------------------------------
            if ($isCrossMixed && $page == $cross) {
                $dt = ($paid_escorts_count % Model_Escort_List::HOMEPAGE_LISTING_SIZE);
                $df = 0;
            } elseif ($isCrossMixed) {
                $df = ($paid_escorts_count % Model_Escort_List::HOMEPAGE_LISTING_SIZE);
                $dt = 0;
            } else {
                $dt = $df = 0;
            }
            // ---------------------------------------

            if(in_array($p_top_category, array('newest', 'newest-independent', 'newest-agency-escorts'))) {
                $availableFilters = self::selectExistingFilters($filter, $ordering , $regularStartPage , $page_size, $count, $sess_name, $only_vip, $s_filter_params, $is_grouped , $geo_data, $list_type, $city_id, $country_id, $current_currency, $paid_escorts_count, $df, $dt);
            }

            //$escorts = self::getFilteredWhereQuery($filter, $ordering , $page , $page_size, $count, $sess_name, $only_vip, $s_filter_params, $is_grouped , $geo_data, $list_type, $city_id, $country_id, $current_currency );
            $escort_ids = self::getEscortIdsOfQuery($filter, $ordering , $regularStartPage , $page_size, $count, $sess_name, $only_vip, $s_filter_params, $is_grouped , $geo_data, $list_type, $city_id, $country_id, $current_currency, $paid_escorts_count, $df, $dt);

            if(count($escort_ids)){
                $escorts = self::getListingDataByIds($escort_ids, $current_currency, $city_id);
            }else{
                $escorts = array();
            }

        }else{
            $escorts = array();
        }

        // total count of all escorts
        $count += $count_vips + $count_premiums;

        $escorts_by_id = array();
        foreach ($escorts as $escort) {
            $escorts_by_id[$escort->id] = $escort;
        }

        $escorts = array();
        foreach ($escort_ids as $id) {
            array_push($escorts, $escorts_by_id[$id]);
        }

        $escorts = array_merge($vip_premium_escorts, $escorts);

        return [
          $escorts, $availableFilters
        ];
    }

   	public static function getAgencyEscorts($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0)
	{
		$lng = Cubix_I18n::getLang();

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;


		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$group_by = " GROUP BY eic.escort_id ";

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar, e.last_hand_verification_date,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated, ct.slug AS city_slug, ct.id AS city_id, ct.title_' . $lng . ' AS city, e.rates,
				e.incall_type, e.outcall_type, e.is_new, cr.id AS country_id, cr.slug AS country_slug, cr.title_' . $lng . ' AS country, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
				e.travel_place, e.email, e.website, e.phone_instr, e.phone_instr_no_withheld, e.phone_instr_other,
				e.phone_country_id, e.disable_phone_prefix, e.phone_number_free AS phone_number,
				IF (ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE), 1, 0) AS is_login, e.allow_show_online,
				e.hit_count, e.slogan, e.incall_price, e.date_last_modified, eic.is_premium, e.is_vip, e.hh_is_active,
				e.is_on_tour AS is_tour, e.tour_date_from, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city,
				e.is_suspicious, e.is_online, ct.id AS city_id, e.about_' . $lng . ' AS about, e.agency_name,
				e.agency_slug, e.agency_id
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id			
			LEFT JOIN escort_services es ON es.escort_id = e.id
			LEFT JOIN escort_keywords ek ON ek.escort_id = e.id
			LEFT JOIN currencies cur ON cur.id = e.incall_currency
			LEFT JOIN exchange_rates er ON er.iso = cur.title

			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			' . $group_by . '
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		';

		$escorts = self::db()->fetchAll($sql);

		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		return $escorts;
	}

    public static function getTours($is_upcoming, $filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $s_filter_params = array(), $is_grouped = false, $geo_data = array(), $list_type = 'simple', $current_currency = null, $differenceFrom = 0, $differenceTo = 0)
    {
        $lng = Cubix_I18n::getLang();
        $sql_join = ' ';

        /*EDIR-2237
        if ( isset($s_filter_params['gender']) && count($s_filter_params['gender']) ) {
            $filter[] = 'eic.gender IN ( ' . implode(',', $s_filter_params['gender']) . ' )';
        }*/

        if ( isset($s_filter_params['service']) && count($s_filter_params['service']) ) {
            $filter[] = 'es.service_id IN ( ' . implode(',', $s_filter_params['service']) . ' )';
        }

        if ( isset($filter['with-video']) && $filter['with-video'] ) {
            $filter['e.has_video = 1'] = array();
            //$join .= " INNER JOIN video v ON v.escort_id = e.id ";
        }
        unset($filter['with-video']);

        if ( isset($filter['eic.gender = 1']) ) unset($filter['eic.gender = 1']);

        if ( ! $is_upcoming ) {
            $filter[] = 'eic.is_tour = 1';
        }
        else {
            $filter[] = 'eic.is_upcoming = 1';
        }

        unset($filter['online-now']);

        if ( isset($filter['show_all_agency_escorts']) ) {
            unset($filter['show_all_agency_escorts']);
        }

        if ( isset($s_filter_params['with-natural-photo']) && $s_filter_params['with-natural-photo'] ) {
            $sql_join .= " INNER JOIN natural_pics n_pics ON n_pics.escort_id = e.id ";
        }

        $where = self::getWhereClause($filter, true);
        $order = self::_mapSorting($ordering, $is_grouped, $list_type);

        $page = (int) $page;
        if ( $page < 1 ) $page = 1;

        // This difference is the VIP banner
        // --------------------------------------------
        $skip = ($page_size * ($page - 1)) - $differenceFrom;
        $take = $page_size - $differenceTo;
        $limit = $skip . ', ' . $take;
        // --------------------------------------------

        $fields = ' , NULL AS distance ';
        if ( $ordering == 'close-to-me' && count($geo_data) ) {
            if (array_key_exists("latitude", $geo_data) && strlen($geo_data['latitude']) > 0 && array_key_exists("longitude", $geo_data) && strlen($geo_data['longitude']) > 0) {
                $fields = '
					 , ((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						))
						)
					)) AS distance
				';
            }
        }

        $currency_rate = Model_Currencies::getSelectedCurrencyRate($current_currency);

        $order = 'eic.is_vip DESC, eic.is_premium DESC,' . $order;

        is_null($where) ? $where = ' 1 ' . Cubix_Countries::blacklistCountryWhereCase() : $where .= Cubix_Countries::blacklistCountryWhereCase();

        $sql = '
			SELECT SQL_CALC_FOUND_ROWS
                case 
                    WHEN ulrt.refresh_date > e.date_last_modified THEN ulrt.refresh_date
                    ELSE e.date_last_modified
                end as last_modified_date,
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar, e.last_hand_verification_date,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated, ct.slug AS city_slug, ct.id AS city_id, ct.title_' . $lng . ' AS city, e.rates,
				e.incall_type, e.outcall_type, e.is_new, cr.id AS country_id, cr.slug AS country_slug, cr.title_' . $lng . ' AS country, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_args, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
				e.travel_place, e.email, e.website, e.phone_instr, e.phone_instr_no_withheld, e.phone_instr_other,
				e.phone_country_id, e.disable_phone_prefix, e.phone_number_free AS phone_number,
				e.hit_count, e.slogan, e.incall_price, e.date_last_modified, eic.is_premium, e.is_vip, e.is_inactive, e.hh_is_active,
				e.is_suspicious, e.is_online, ct.id AS city_id, e.about_' . $lng . ' AS about, e.agency_id, e.agency_name, e.agency_slug,
				(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' AS incall_price_converted,
				eic.is_tour ' .
            (! $is_upcoming ? ', e.tour_date_from, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city' : // if
                ($is_upcoming ? ', ut.tour_date_from, ut.tour_date_to, ctu.title_' . $lng . ' AS tour_city' : '')) // else
            . $fields . '
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			'.$sql_join.'
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id

			LEFT JOIN currencies cur ON cur.id = e.incall_currency
			LEFT JOIN exchange_rates er ON er.iso = cur.title
			
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . ($is_upcoming ? 'INNER JOIN upcoming_tours ut ON ut.id = eic.escort_id INNER JOIN cities ctu ON ctu.id = ut.tour_city_id' : 'LEFT JOIN cities ctt ON ctt.id = e.tour_city_id') . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY eic.is_inactive, last_modified_date DESC,' . $order . '
			LIMIT ' . $limit . '
		';

        if($_GET['edo']) {
            die($sql);
        }


        $escorts = self::db()->fetchAll($sql);
        $count = self::db()->fetchOne('SELECT FOUND_ROWS()');

        if ( count($escorts) && $is_grouped && $list_type == 'simple' ) {

            $grouped_escorts = array();
            foreach($escorts as $k => $escort) {

                $group_field = $escort->city;
                if ( $ordering == 'by-country' && ! isset($filter['ct.id = ?']) && ! isset($filter['cr.id = ?']) ) {
                    $group_field = $escort->country;
                }

                $no_group_sorting = array('newest', 'random', 'alpha', 'most-viewed');
                if ( in_array($ordering, $no_group_sorting) ) {
                    $group_field = '';
                } else {
                    $group_field = $group_field . ' ' . __('escorts');
                }

                $grouped_escorts['city_groups'][$group_field][] = $escort;

                if ( $ordering != 'close-to-me' && $ordering != 'newest' ) {
                    ksort($grouped_escorts['city_groups']);
                }

            }

            $escorts = $grouped_escorts;
        }
        return $escorts;
    }

   	private static function _mapSortingAgencies($param)
	{
		$map = array(
			'alpha' => 'cd.is_premium DESC, cd.club_name ASC',
			'random' => 'cd.is_premium DESC, RAND()',
			'last_modified' => 'cd.is_premium DESC, cd.last_modified DESC',
			'premium' => 'cd.is_premium DESC',
			'by-city' => 'ct.title_en ASC',
			'by-country' => 'cr.title_en ASC',
			'most-viewed' => 'cd.hit_count DESC',
			'close-to-me' => 'distance ASC',
		);

		$order = 'cd.club_name ASC';
		if ( isset($map[$param]) ) {
			$order = $map[$param];
		}
		elseif ( false !== strpos($param, 'FIELD') ) {
			$order = $param;
		}

		return $order;
	}

	private static function _mapSorting($param, $is_grouped = false, $view_type = 'simple')
	{
		$map = array(
			'close-to-me' => '/*e.is_vip DESC, eic.is_premium DESC,*/ distance ASC, eic.ordering DESC',
			'last-contact-verification' => 'e.last_hand_verification_date DESC',
			'last-connection' => 'ulrt.refresh_date DESC',


			'by-city' => '/*e.is_vip DESC, eic.is_premium DESC,*/ ct.title_en ASC, eic.ordering DESC',
			'by-country' => '/*e.is_vip DESC, eic.is_premium DESC,*/ cr.title_en ASC, eic.ordering DESC',
			'random' => '/*e.is_vip DESC, eic.is_premium DESC,*/ eic.ordering DESC',
			'alpha' => '/*e.is_vip DESC, eic.is_premium DESC,*/ e.showname ASC',
			'most-viewed' => '/*eic.is_premium DESC,*/ e.hit_count DESC',
			'newest' => 'COALESCE(e.date_activated, e.date_registered) DESC, e.id DESC',
			

			'premiums' => 'e.is_vip DESC, eic.is_premium DESC, eic.ordering DESC',
			'vips' => 'e.is_vip DESC, is_vip_plus DESC, is_vip2 DESC, eic.ordering DESC',
			'price-asc' => 'incall_price_converted ASC',
			'price-desc' => 'incall_price_converted DESC',

			'last_modified' => 'e.date_last_modified DESC',
			'age' => '-e.age DESC',
		);

		if ( $view_type == 'simple' ) {
			$map = array(
				'close-to-me' => 'distance ASC/*, eic.ordering DESC*/',
				'last-contact-verification' => 'e.last_hand_verification_date DESC',
				'last-connection' => 'ulrt.refresh_date DESC',

				'by-city' => '/*e.is_vip DESC, eic.is_premium DESC,*/ ct.title_en ASC, eic.ordering DESC',
				'by-country' => '/*e.is_vip DESC, eic.is_premium DESC,*/ cr.title_en ASC, eic.ordering DESC',
				'random' => '/*e.is_vip DESC, eic.is_premium DESC,*/ eic.ordering DESC',
				'random-all' => 'e.is_vip DESC, eic.is_premium DESC, eic.ordering DESC',
				'alpha' => '/*e.is_vip DESC, eic.is_premium DESC,*/ e.showname ASC',
				'most-viewed' => '/*eic.is_premium DESC,*/ e.hit_count DESC',
				'newest' => 'COALESCE(e.date_activated, e.date_registered) DESC',


				'premiums' => 'e.is_vip DESC, eic.is_premium DESC, eic.ordering DESC',
				'vips' => 'e.is_vip DESC, eic.ordering DESC',
				'price-asc' => 'incall_price_converted ASC',
				'price-desc' => 'incall_price_converted DESC',

				'last_modified' => 'e.date_last_modified DESC',
				'age' => '-e.age DESC',
			);
		}
		
		$order = 'e.ordering DESC';
		if ( isset($map[$param]) ) {
			$order = $map[$param];
		}
		elseif ( false !== strpos($param, 'FIELD') ) {
			$order = $param;
		}

		return $order;
	}

	private static function _generateCacheKey($filters)
	{
		$cache_key = '';
		foreach($filters as $k => $value ) {
            if ( is_array($value) ) {
				$cache_key .= $k;
                foreach( $value as $val ) {
                    $cache_key .= '_' . $val . '_';
                }
            } else {
                $cache_key .= $k. '_' . $k . '_' . $value;
            }
        }
		return $cache_key;
	}

	public static function getPrevNext($showname)
	{
		return array(null, null);
		// Retrieve criterias from session
		$sid = 'v2_sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$page = $sess->page;
		$page_size = $sess->page_size;
		$pages_count = $sess->pages_count;

		list($filter, $ordering) = $sess->criteria;
		foreach ( $sess->escorts as $i => $name ) {
			if ( $name == $showname ) {
				$prev_i = $i - 1;
				$next_i = $i + 1;

				break;
			}
		}

		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);

		if ( ! isset($prev_i) && ! isset($next_i) ) {
			return array(null, null);
		}

		// This is first escort, we need to fetch data from prev page
		if ( isset($prev_i) && (! isset($sess->escorts[$prev_i]) || $prev_i < 0) && $page > 1 ) {
			$sess->escorts = array_slice($sess->escorts, 0, $page_size * 2);

			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page - 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($escorts, $sess->escorts);
			
			$prev_i = $page_size - 1;
		}

		
		echo "Escorts after 'first' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);
		
		// If this last escort on page, we need to fetch data from next page
		if ( isset($next_i) &&  (! isset($sess->escorts[$next_i]) || $next_i > count($sess->escorts) - 1 ) && $page < $pages_count ) {
			if ( count($sess->escorts) >= $page_size * 3 ) {
				$sess->escorts = array_slice($sess->escorts, $page_size, $page_size * 2);
				$next_i -= $page_size;
			}
			
			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page + 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($sess->escorts, $escorts);
		}

		echo "Escorts after 'last' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);
		
		return array(isset($prev_i) ? $sess->escorts[$prev_i] : null, isset($next_i) ? $sess->escorts[$next_i] : null);
	}
	
	public static function getVIPEscorts($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $only_vip = false, $is_tour = false)
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}
		
		if ( $only_vip ) {
			$filter[] = 'FIND_IN_SET(17, e.products) > 0';

			if ( isset($filter['ct.slug = ?']) && ! $is_tour ) {
				$country_id = Model_Statistics::getCountryByCitySlug($filter['ct.slug = ?'][0]);
				$filter[] = 'e.country_id = ' . $country_id;
				unset($filter['ct.slug = ?']);
			}
		}
		
		// Only escorts with base city (or city tour) will be shown in this list
		if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
			$filter[] = 'eic.is_base = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 0';
		}

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;
		
		$j = "";
		$f = "";
		if ( isset($filter['f.user_id = ?']) ) {
			$j = " INNER JOIN favorites f ON f.escort_id = e.id ";
			$f = " , f.user_id AS fav_user_id ";
		}

		$order = str_replace('eic.is_premium', 'IF(eic.is_tour = 1, 0, eic.is_premium)', $order);
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,

				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,
				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				e.hit_count, e.slogan, e.incall_price,
				e.date_last_modified,
				/*IF(eic.is_tour = 1 AND eic.is_premium = 1, 0, eic.is_premium) AS is_premium,*/ 
				IF(eic.is_tour = 1, 0, eic.is_premium) AS is_premium,
				IF(FIND_IN_SET(17, e.products) > 0, 1, 0) AS is_vip,
				e.hh_is_active, e.is_suspicious, e.is_online ' . $f . ', ct.id AS city_id, e.about_' . $lng . ' AS about
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = e.country_id
			LEFT JOIN regions r ON r.id = eic.region_id
			' . $j . '
			LEFT JOIN escort_cityzones ecz ON ecz.escort_id = eic.escort_id
			LEFT JOIN cityzones cz ON cz.id = ecz.city_zone_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 2 AND se.slug = "escort")
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		'; 
		
		$escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		
		
		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		if ( $sess_name ) {
			// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->id;
			}
			
			$sess->{$sess_name . '_criterias'} = array(
				'filter'	=> $filter,
				'sort'	=> $ordering,
				'page'		=> $page
			);
			// </editor-fold>
		}

		return $escorts;
	}

	public static function getForJson()
	{
		$lng = 'en';

		$sql = '
			SELECT
				e.id,
				e.user_id,
				e.showname, 
				e.age,
				e.gender,
				e.sex_orientation,
				e.height,
				e.weight,

				UNIX_TIMESTAMP(e.date_registered) AS registered_at,
				e.last_hand_verification_date AS last_hand_verification_at,
				e.date_last_modified AS last_modified_at,
				UNIX_TIMESTAMP(e.date_activated) AS activated_at,

				cr.id AS country_id, 
				cr.slug AS country_slug, 
				cr.title_' . $lng . ' AS country_title,

				ct.id AS city_id,
				ct.slug AS city_slug,				
				ct.title_' . $lng . ' AS city_title,
				
				e.is_pornstar,
				e.incall_type, 
				e.outcall_type, 
				e.is_new,
				e.verified_status,
				e.allow_show_online,
				e.is_suspicious, 
				e.is_online,
				eic.is_premium, 
				e.is_vip, 
				e.hh_is_active,
				e.is_on_tour AS is_tour,

				e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
							
				e.incall_currency, 
				e.incall_price, 
				e.outcall_currency, 
				e.outcall_price, 

				e.comment_count, 
				e.review_count,

				e.email, 
				e.website, 
				e.phone_instr, 
				e.phone_instr_no_withheld, 
				e.phone_instr_other,
				e.phone_country_id, 
				e.disable_phone_prefix, 
				e.phone_number_free AS phone_number,
				
				e.hit_count, 
				e.slogan,
				
				e.tour_date_from, 
				e.tour_date_to, 
				ctt.id AS tour_city_id,
				ctt.slug AS tour_city_slug,
				ctt.title_' . $lng . ' AS tour_city_title,
				
				e.about_' . $lng . ' AS about,

				e.agency_id,
				e.agency_slug,
				e.agency_name,

				e.sex_availability AS services_for,
				e.languages			
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id			
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id			
			LEFT JOIN currencies cur ON cur.id = e.incall_currency
			LEFT JOIN exchange_rates er ON er.iso = cur.title
			WHERE 1
			AND eic.is_base = 1
			GROUP BY eic.escort_id
		';

		$escorts = self::db()->fetchAll($sql);

		$date_format = 'D M d Y H:i:s O';

		foreach ($escorts as $i => $escort) {
			$escort = new Model_EscortV2Item($escort);

			$escorts[$i]->country = array('id' => $escorts[$i]->country_id, 'slug' => $escorts[$i]->country_slug, 'title' => $escorts[$i]->country_title);
			unset($escorts[$i]->country_id);
			unset($escorts[$i]->country_slug);
			unset($escorts[$i]->country_title);

			$escorts[$i]->city = array('id' => $escorts[$i]->city_id, 'slug' => $escorts[$i]->city_slug, 'title' => $escorts[$i]->city_title);
			unset($escorts[$i]->city_id);
			unset($escorts[$i]->city_slug);
			unset($escorts[$i]->city_title);

			$escorts[$i]->agency = array('id' => $escorts[$i]->agency_id, 'slug' => $escorts[$i]->agency_slug, 'title' => $escorts[$i]->agency_name);
			unset($escorts[$i]->agency_id);
			unset($escorts[$i]->agency_slug);
			unset($escorts[$i]->agency_name);

			if ( $escorts[$i]->is_tour ) {
				$escorts[$i]->tour = array(
					'date' => array(
						'start' => date($date_format, strtotime($escorts[$i]->tour_date_from)), 
						'end' => date($date_format, strtotime($escorts[$i]->tour_date_to))
					), 
					'city' => array(
						'id' => $escorts[$i]->tour_city_id, 
						'slug' => $escorts[$i]->tour_city_slug, 
						'title' => $escorts[$i]->tour_city_title) 
				);
			} else {
				$escorts[$i]->tour = array(
					'date' => array(
						'start' => null, 
						'end' => null
					), 
					'city' => array(
						'id' => null, 
						'slug' => null,
						'title' => null)
				);
			}
			unset($escorts[$i]->tour_date_from);
			unset($escorts[$i]->tour_date_to);
			unset($escorts[$i]->tour_city_id);
			unset($escorts[$i]->tour_city_slug);
			unset($escorts[$i]->tour_city_title);

			$escorts[$i]->image_url = $escort->getMainPhoto()->getUrl('thumb_ed_v3');
			unset($escorts[$i]->photo_hash);
			unset($escorts[$i]->photo_ext);
			unset($escorts[$i]->photo_status);
			unset($escorts[$i]->application_id);
			
			$escorts[$i]->about = mb_convert_encoding(substr( strip_tags($escorts[$i]->about), 0, 450 ), "UTF-8" );

			$keywords = self::db()->fetchAll('SELECT keyword_id AS id FROM escort_keywords WHERE escort_id = ?', $escorts[$i]->id);
			$_keywords = array();
			foreach ($keywords as $key => $keyword) {
				$_keywords[] = $keyword->id;
			}
			$escorts[$i]->keywords = $_keywords;

			$services = self::db()->fetchAll('SELECT service_id AS id FROM escort_services WHERE escort_id = ?', $escorts[$i]->id);
			$_services = array();
			foreach ($services as $key => $service) {
				$_services[] = $service->id;
			}
			$escorts[$i]->services = $_services;

			$escorts[$i]->languages = explode(',', $escorts[$i]->languages);
			$escorts[$i]->services_for = explode(',', $escorts[$i]->services_for);

			$escorts[$i]->registered_at = date($date_format, $escorts[$i]->registered_at);
			if ( $escorts[$i]->last_hand_verification_at ) {
				$escorts[$i]->last_hand_verification_at = date($date_format, strtotime($escorts[$i]->last_hand_verification_at));
			} else {
				$escorts[$i]->last_hand_verification_at = null;
			}
			$escorts[$i]->last_modified_at = date($date_format, strtotime($escorts[$i]->last_modified_at));
			if ( $escorts[$i]->activated_at ) {
				$escorts[$i]->activated_at = date($date_format, $escorts[$i]->activated_at);
			} else {
				$escorts[$i]->activated_at = null;
			}
		}

		return $escorts;
	}
	
	public static function convertPushState($pushstate_filters)
	{
		$request = Zend_Controller_Front::getInstance()->getRequest();
		$push_state_array = explode(';',$pushstate_filters);
		array_pop($push_state_array);

		foreach($push_state_array as $filter){
			list($key, $value) = explode('=',$filter);

			$values = explode(',',$value);
			if(count($values) > 1){
				$value_array = array();
				foreach($values as $val){
					$value_array[] = $val;
				}
				$request->setParam($key, $value_array);
			}
			else{
				$request->setParam($key, $value);
			}
		}
	}		
}
