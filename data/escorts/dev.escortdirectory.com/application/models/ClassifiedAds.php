<?php

class Model_ClassifiedAds extends Cubix_Model
{
	public function getList($filter = array(), $page = 1, $perPage = 20, $ord_dir = 'desc', &$count = null, $exclude_id = null, $ordering = false, $geo_data = null)
	{
	    if(isset($filter['city_id'])) {
	        $filter['city'] = $filter['city_id'];
	        unset($filter['city_id']);
        }
		if( ! $page ) $page = 1;
		
		$limit = ' LIMIT ' . ($page - 1) * $perPage . ', ' . $perPage;
		$join = '';
		$where = '';
		$order = '';

		$where = '';
		$order = ' is_premium DESC, approvation_date ' . $ord_dir . ' ';
		
		if ( $exclude_id ) {
			$where .= parent::quote(' AND ca.id <> ?', $exclude_id);
		}

		if ( isset($filter['category']) && $filter['category'] ) {
			$where .= parent::quote(' AND ca.category_id = ?', $filter['category']);
		}
		
		if ( isset($filter['city']) && $filter['city'] ) {
			$where .= parent::quote(' AND cac.city_id = ?', $filter['city']);
		}

		if ( isset($filter['country_id']) && $filter['country_id'] ) {
			$where .= parent::quote(' AND cn.id = ?', $filter['country_id']);
		}

		if ( isset($filter['with_photo']) && $filter['with_photo'] == 1 ) {
			$join .= ' LEFT JOIN classified_ads_images cai ON ca.id = cai.ad_id ';
			$where .= parent::quote(' AND cai.id > ?', 0);
		}
		
		if ( isset($filter['text']) && $filter['text'] ) {
			$where .= parent::quote(' AND ca.search_text LIKE ?', '%' . $filter['text'] . '%');
		}

		if ( isset($filter['date_f']) && $filter['date_f'] ) {
			$where .= parent::quote(' AND DATE(ca.approvation_date) >= DATE(?)', date('Y-m-d', $filter['date_f']));
		}

		if ( isset($filter['date_t']) && $filter['date_t'] ) {
			$where .= parent::quote(' AND DATE(ca.approvation_date) <= DATE(?)', date('Y-m-d', $filter['date_t']));
		}

        $close_to_me = '';
        if( IS_MOBILE_DEVICE ) {
            $fields = ' NULL AS distance ';
            if ( $ordering != false && count($geo_data) ) {
                if (array_key_exists("latitude", $geo_data) && strlen($geo_data['latitude']) > 0 && array_key_exists("longitude", $geo_data) && strlen($geo_data['longitude']) > 0) {
                    $fields = '
                        ((2 * 6371 *
                            ATAN2(
                            SQRT(
                                POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - c.latitude))/2), 2) +
                                COS(RADIANS(c.latitude)) *
                                COS(RADIANS(' . $geo_data['latitude'] . ')) *
                                POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - c.longitude))/2), 2)
                            ),
                            SQRT(1-(
                                POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - c.latitude))/2), 2) +
                                COS(RADIANS(c.latitude)) *
                                COS(RADIANS(' . $geo_data['latitude'] . ')) *
                                POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - c.longitude))/2), 2)
                            ))
                            )
                        )) AS distance
                    ';
                }
            }
            $close_to_me =  ', ' . $fields;
//            echo '<span class="bibar" style="display: none;">';
//            echo $close_to_me;
//            echo '</span>';
//            print_r($geo_data); die;
        }
        // " . $close_to_me . "
		
		$sql = "
			SELECT SQL_CALC_FOUND_ROWS ca.*, c." . Cubix_I18n::getTblField('title') . " AS city_title, cn." . Cubix_I18n::getTblField('title') . " AS country_title" . $close_to_me . "
			FROM classified_ads ca
			INNER JOIN classified_ads_cities cac ON cac.ad_id = ca.id
			INNER JOIN cities c ON c.id = cac.city_id
			INNER JOIN countries cn ON cn.id = c.country_id {$join}
			WHERE 1 {$where}
			GROUP BY ca.id
			ORDER BY {$order}
			{$limit}
		";
        //echo $sql;die;

        $items = $this->getAdapter()->fetchAll($sql);
        $count = $this->getAdapter()->fetchOne('SELECT FOUND_ROWS()');

		if ( count($items) ) {
			foreach ( $items as $i => $item ) {
				$items[$i]->images = $this->getAdapter()->fetchAll('
					SELECT 
						id AS image_id, hash, ext, ' . Cubix_Application::getId() . ' AS application_id 
					FROM classified_ads_images
					WHERE ad_id = ?
				', array($item->id));
			}
		}
		
		return $items;
	}

	public function updateViewCount($ad_id)
	{
		$this->getAdapter()->query('UPDATE classified_ads SET view_count = view_count + 1 WHERE id = ?', array($ad_id));

		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$client->call('ClassifiedAds.updateViewCount', array($ad_id));
	}

	public function get($id)
	{
		$sql = "
			SELECT
				ca.*, c." . Cubix_I18n::getTblField('title') . " AS city_title, cn." . Cubix_I18n::getTblField('title') . " AS country_title, cac.city_id, c.country_id
			FROM classified_ads ca
			INNER JOIN classified_ads_cities cac ON cac.ad_id = ca.id
			INNER JOIN cities c ON c.id = cac.city_id
			INNER JOIN countries cn ON cn.id = c.country_id
			WHERE ca.id = ?
		";

		$ad = $this->getAdapter()->fetchRow($sql, array($id));

		$ad_sql = '
			SELECT
				id AS image_id, hash, ext, ' . Cubix_Application::getId() . ' AS application_id
			FROM classified_ads_images
			WHERE ad_id = ?
		';

		$ad->images = $this->getAdapter()->fetchAll($ad_sql, array($ad->id));

		return $ad;
	}

	public function save($data)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$ad_id = $client->call('ClassifiedAds.save', array($data));
		
		return $ad_id;
	}

	public function addFeedback($data)
	{
		$feedbackdata = array(
			"name" => @$data['name'],
			"email" => @$data['email'],
			"to_addr" => @$data['to_addr'],
			"message" => @$data['message'],
			"user_id" => @$data['user_id'],
			"ca_id" => @$data['ca_id'],
			"flag" => @$data['flag'],
			"status" => @$data['status'],
			"application_id" => @$data['application_id']
		);

		// ip
		$ip = Cubix_Geoip::getIP();

		$feedbackdata['ip'] = $ip;
		//

		// city
		$loc = Cubix_Geoip::getClientLocation($ip);
		$city = null;

		if (is_array($loc))
			$city = $loc['city'];

		$feedbackdata['city'] = $city;
		//
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$client->call('ClassifiedAds.addFeedback', array($feedbackdata));
	}
}
