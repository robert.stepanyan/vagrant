<?php

class Model_Follow extends Cubix_Model
{
	
	const FOLLOW_TYPE_AFTER_SIGNIN = 1;
	const FOLLOW_TYPE_AFTER_SIGNUP = 2;
	const FOLLOW_TYPE_EMAIL = 3;
	
	public function getAll($params)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.getAll', array($params));
	}

	public function getMemberFollowingCount ($member_id) {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call('Follow.getMemberFollowingCount', array($member_id));
    }
	
	/*public function getCount($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.getCount', array($user_id));
	}*/

	public function getFollowList($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.getFollowList', array($user_id));
	}
	
	public function getFollowEvents($follow_user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.getFollowEvents', array($follow_user_id));
	}
	
	public function remove($id, $type_id, $type)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.remove', array($id, $type_id, $type));
	}
	
	public function add($data, $events, $cities = null)
	{
        $client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.add', array($data, $events, $cities));
	}
	
	public function edit($follow_user_id, $data, $events, $cities)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.edit', array($follow_user_id, $data, $events, $cities));
	}
	
	public function checkEmailExists($email,$type, $type_id )
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.checkEmailExists', array($email,$type, $type_id));
	}
	
	public function checkUserExists($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.checkUserExists', array($user_id));
	}
	
	public function checkUserFollows($user_id,$type, $type_id )
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.checkUserFollows', array($user_id,$type, $type_id));
	}
	
	public function checkIsFollowed($type, $type_id)
	{
		$user = Model_Users::getCurrent();
		if($user && $user->user_type == "member"){
			if(in_array($type .'-'. $type_id,$user->follow_list)){
				return true;
			}
		}
		else{
			$follow_session = new Zend_Session_Namespace('follow_info');
			
			if($follow_session->follow && is_array($follow_session->follow)){
				foreach($follow_session->follow as $follow){
					if($follow['user_type'] == $type && $follow['user_type_id'] == $type_id){
						return true;
					}
				}
			}
		}
		return false;
		
	}
	
	public function checkPendingFollows($user_id, $check = false)
	{
		$session = new Zend_Session_Namespace('follow_info');
		if($session->follow){
			
			foreach($session->follow as $follow){
					
				if($check){
					if($this->checkUserFollows($user_id, $follow['user_type'], $follow['user_type_id'])){
						CONTINUE;
					}
				}
				
				$follow_data = array(
					'user_id' => $user_id,
					'type' => $follow['user_type'],
					'type_id' => $follow['user_type_id'],
				);
				
				$this->add($follow_data, $follow['events'], $this->encode($follow['cities']));
			}
			
			unset($session->follow);
		}
	}
	
	public function checkHasFollowsWithEmail($user_id, $email)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.checkHasFollowsWithEmail', array($user_id, $email));
	}
	
	public function getTop10($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.getTop10', array($user_id, Cubix_I18n::getLang()));
	}
	
	public function Top10($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.getTop10', array($user_id, Cubix_I18n::getLang()));
	}
	
	public function isTop10Public($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.isTop10Public', array($user_id, Cubix_I18n::getLang()));
	}
	
	public function getCount($type, $type_id )
	{
		$cache = Zend_Registry::get('cache');
		$cache_key = 'followers_count_' . $type . '_' . $type_id;
		if ( ! $followers_count = $cache->load($cache_key) ) {
			try {
				$client = new Cubix_Api_XmlRpc_Client();
				$followers_count = $client->call('Follow.getCount', array($type, $type_id));
			}
			catch ( Exception $e ) {
				$followers_count = 0;
			}
			$cache->save($followers_count, $cache_key, array(), 300);
		}
		return $followers_count;
	}
	
	public function sortTop10($user_id, $sort_ids)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.reorderTop10', array($user_id, $sort_ids));
	}
	
	public function favoritesMoveUp($user_id, $id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.favoritesMoveUp', array($user_id, $id));
	}
	
	public function favoritesMoveDown($user_id, $id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.favoritesMoveDown', array($user_id, $id));
	}
	
	public function removeFavorites($user_id, $id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.removeFavorites', array($user_id, $id));
	}
	
	public function reorderingTop10($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.reorderingTop10', array($user_id));
	}
	
	public function confirmEmail($email, $hash)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.confirmEmail', array($email, $hash));
	}
	
	public function checkEmailFollowerExists($email)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.checkEmailFollowerExists', array($email));
	}
	
	public function showTop10($member_id, $show)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.showTop10', array($member_id, $show));
	}
	
	public function getFollowers($filter)
	{
		$lng = Cubix_I18n::getLang();
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.getFollowers', array($filter,$lng));	
	}
	
	public function toggleFavorite($type, $type_id, $follower_id, $is_favorite)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.toggleFavorite', array($type, $type_id, $follower_id, $is_favorite));	
	}
	
	public function toggleNewsletter($type, $type_id, $follower_id, $newsletter)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.toggleNewsletter', array($type, $type_id, $follower_id, $newsletter));	
	}
	
	public function addComment($type, $type_id , $data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Follow.addComment', array($type, $type_id, $data['id'], $data['comment']));	
	}
	
	public function getCountries($type, $type_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$lang = Cubix_I18n::getLang();
		return $client->call('Follow.getCountries', array($type, $type_id, $lang));	
	}
	
	public function getCities($type, $type_id, $country_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$lang = Cubix_I18n::getLang();
		return $client->call('Follow.getCities', array($type, $type_id, $country_id, $lang));	
	}
	
	public function encode($array)
	{
		if(count($array) > 0){
			return "|". implode('||', $array). "|";
		}
		else{
			return '';
		}
	}
	
	public function decode($string)
	{
		$string1 = trim($string,'|');
		return explode('||', $string1 );
	}		
}
