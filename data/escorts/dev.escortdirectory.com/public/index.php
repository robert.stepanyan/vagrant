<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

// require_once 'Mobile_Detect.php';
// $detect = new Mobile_Detect;

header("HTTP/1.1 301 Moved Permanently"); 
header("Location: https://www.escortdirectory.com"); 
exit();

$cooks = array(
	'sorting',
	'sortingMap',
	'list_type',
	'ln',
);
foreach ($cooks as $c_id)
{
    setcookie($c_id, NULL, 0, "/", ".escortdirectory.com");
}

header('Content-type: text/html; charset=UTF-8');

$srv = 'unknown';
header('Cubix-Server: web1');

$GLOBALS['SQLS'] = array();

if ( preg_match('#\.test$#', $_SERVER['SERVER_NAME']) > 0 ) {
	define('IS_DEBUG', true);
}
else {
	define('IS_DEBUG', false);
}


$host = $_SERVER['HTTP_HOST'];
$host = explode('.',$host);

// define('IS_MOBILE_DEVICE', ($detect->isMobile() && !$detect->isTablet()) ? true : false);
define('IS_MOBILE_DEVICE', false);

mb_internal_encoding('UTF-8');


$app_env = 'production';
if ( preg_match('#\.test$#', $_SERVER['SERVER_NAME']) ) {
    $app_env = 'development';
}

define('APPLICATION_ENV', $app_env);
define('APP_DOMAIN', $_SERVER['HTTP_HOST']);

$http_protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https' : 'http';
define('APP_HTTP', $http_protocol);
/*define('APP_DOMAIN', preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));*/

function mb_str_replace($needle, $replacement, $haystack)
{
    $needle_len = mb_strlen($needle);
    $replacement_len = mb_strlen($replacement);
    $pos = mb_strpos($haystack, $needle);
    while ($pos !== false)
    {
        $haystack = mb_substr($haystack, 0, $pos) . $replacement
                . mb_substr($haystack, $pos + $needle_len);
        $pos = mb_strpos($haystack, $needle, $pos + $replacement_len);
    }
    return $haystack;
}

//die('We are not available right now, we will back in a moment!');

if ( ! function_exists('json_encode') ) {
	function json_encode($data) {
		return Zend_Json::encode($data);
	}
}

if ( ! function_exists('json_decode') ) {
	function json_decode($data) {
		return Zend_Json::decode($data);
	}
}

if ( isset($_POST['PHPSESSID']) ) {
	$sessid = $_POST['PHPSESSID'];
	$_COOKIE['PHPSESSID'] = $sessid;
	session_id($_POST['PHPSESSID']);
}

/*require '../../library/Cubix/Debug.php';
require '../../library/Cubix/Debug/Benchmark.php';

if ( isset($_REQUEST['benchmark']) ) Cubix_Debug::setDebug(true);
Cubix_Debug_Benchmark::setLogType(Cubix_Debug_Benchmark::LOG_TYPE_DB);
Cubix_Debug_Benchmark::setApplication('frontend');
Cubix_Debug_Benchmark::start($_SERVER['REQUEST_URI']);
*/

/**
 * Used to construct static server url
 *
 * This function will automatically decide which folder to prefix to the given
 * parameter by examining it's extension
 *
 * /img/ - png, gif, jpg
 * /js/ - js
 * /css/ - css
 */
function _st($object)
{
	$matches = array();
	if ( 0 == preg_match('#\.([a-z]{2,4})/?#', $object, $matches) ) {
		throw new Exception('Invalid object, expected a filename with extension');
	}

	//$base_url = 'http://www.escortdirectory.com';
	$base_url =  'https://dev.escortdirectory.com';


	if ( defined('IS_DEBUG') && IS_DEBUG ) {
		$base_url = 'http://' . $_SERVER['HTTP_HOST'];
	}

	array_shift($matches);
	list($ext) = $matches;

	switch ( $ext ) {
		case 'jpg':
		case 'gif':
		case 'png':
			$prefix = '/images/';
			break;
		case 'css':
			$prefix = '/css/';
			break;
		case 'js':
			$prefix = '/js/';
			break;
		case 'php':
			$prefix = '/';
			break;
		default:
			throw new Exception('Invalid object, unsupported extension "' . $ext . '"');
	}

	$url = $base_url . $prefix . $object;

	return $url;
}





// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../../library'),
	realpath(APPLICATION_PATH . '/../../../usr/lib'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$mtime = microtime();
$mtime = explode(" ",$mtime);
$mtime = $mtime[1] + $mtime[0];
$GLOBALS['exec_start_time'] = $mtime;

// <editor-fold defaultstate="collapsed" desc="Output Cache">
	$frontendOptions = array(
		'lifetime' => null,
		'automatic_serialization' => true
	);

	$backendOptions = array(
		'servers' => array(
			array(
				'host' => '172.16.0.11',
				'port' => '11212',
				'persistent' =>  true
			)
		),
	);

// <editor-fold defaultstate="collapsed" desc="Output Cache">
	define('USE_CACHE', false);

	$cache = Zend_Cache::factory('Output', 'Memcached', $frontendOptions, $backendOptions);

	global $frontend_cache;
	$frontend_cache = $cache;

	function _cs($key)
	{
		if ( defined('USE_CACHE') && ! USE_CACHE ) {
			return;
		}

		global $frontend_cache;

		$key = 'html_cache_' . $key;
		return $frontend_cache->start($key);
	}

	function _ce()
	{
		if ( defined('USE_CACHE') && ! USE_CACHE ) {
			return;
		}

		global $frontend_cache;

		return $frontend_cache->end();
	}
// </editor-fold>

	try {
        $application->bootstrap()->run();
    }
	catch ( Exception $e ) {
		var_dump($e);
		$error = '[' . date('d.m.Y H:i:s') . ']: URL: ' . $_SERVER['REQUEST_URI'] . "\n";
		$error .= $e->__toString();
		$error .= "\n------------------\n";
		file_put_contents('debug.log', $error, FILE_APPEND);
		echo 'Unexpected error!';
	}



//file_put_contents('sqls.log', $GLOBALS['SQLS'], FILE_APPEND | LOCK_EX);

/*
Cubix_Debug_Benchmark::end($_SERVER['REQUEST_URI']);

if ( Cubix_Debug::isDebug() ) {
	Cubix_Debug_Benchmark::log();
}
*/
