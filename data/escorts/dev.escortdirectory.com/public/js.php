<?php
$CACHE_OFFSET = 7 * 24 * 60 * 60;
$JS_LOCATION  = "js/combined/";
$CSS_LOCATION = "css/combined/";

function getMaxModifTime( $js_sources ){
    $max_modif_time = null;
    foreach( $js_sources as $source ){
        $m_time = @filemtime($source);
        if( is_null($max_modif_time) ){
            $max_modif_time = $m_time;
        }
        if( $m_time > $max_modif_time ){
            $max_modif_time = $m_time;
        }
    }
    return $max_modif_time;
}

function writeToFile( $js_sources, $file_path ){
    //ATTENTION !!! Minify class is changed by Vahag. CLIENT CACHE VALIDATION IS DISABLED.
    $output = Minify::serve('Files', array(
        'files'  => $js_sources
    ,'quiet' => true
        //,'lastModifiedTime' => $lastModified
    ,'encodeOutput' => false
    ));
    $js = $output['content'];

    //echo $js;

    $f = @fopen($file_path, 'w');

    if (!$f) {
        echo 'Problem while writing to file.' . $file_path ; die;
    } else {
        $bytes = fwrite($f, $js);
        fclose($f);
    }
}


require dirname(__FILE__) . '/min/config.php';

set_include_path($min_libPath . PATH_SEPARATOR . get_include_path());

require_once 'Minify.php';
//JS FILES

$js_sources = array(
    'js/cookies.min.js',
    'js/jquery.min.js',
    'js/popper.min.js',
    'js/bootstrap/index.js',
    'js/bootstrap/dropdown.js',
    'js/bootstrap/modal.js',
    'js/loadingoverlay.min.js',
    'js/popup.js',
    'js/popup-signin.js',
    'js/popup-follow.js',
    'js/popup-subscribe.js',
    "js/private/PrivateMessaging.js",
    'js/global.js',
);

$other_sources = array(
    'home-page' => array(
        'js/private/libs/bootstrap-notify.min.js',
        'js/jquery.appear.js',
        'js/home.js',
        'js/filter.js',
        'js/country.js',
        'js/bootstrap-multiselect.js',
        'js/jquery.pjax.js',
    ),
    'blog' => array(
        'js/private/libs/bootstrap-notify.min.js',
        'js/blog.js',
    ),
    'agency-list' => array(
        'js/bootstrap-multiselect.js',
        'js/country.js',
        'js/home.js',
        'js/jquery.pjax.js',
        'js/agency-list.js',
    ),
    'agency-profile' => array(
        'js/private/libs/bootstrap-notify.min.js',
        'js/agencie-profile.js',
        'js/bootstrap/tooltip.js',
        'js/bootstrap/collapse.js'
    ),
    'profile' => array(
        'js/private/libs/bootstrap-notify.min.js',
        'lib/datepair/bootstrap-datepicker.min.js',
        'js/bootstrap/carousel.js',
        'js/bootstrap/collapse.js',
        'js/bcSwipe.js',
        'js/country.js',
        'js/profile.js',
        'js/bootstrap-multiselect.js',
        'js/private/index.js'

    ),
    'member-profile' => array(
        'js/bootstrap/collapse.js',
        'js/bcSwipe.js',
        'js/country.js',
        'js/members.js',
        'js/bootstrap-multiselect.js',
        'js/private/index.js'
    ),

    'comments' => array(
        'js/private/libs/bootstrap-notify.min.js',
        'js/bootstrap-multiselect.js',
        'js/country.js',
        'js/home.js',
        'js/jquery.pjax.js',
        'js/comments.js',
    ),

    'private' => array(
        'js/bootstrap.min.js',
        'js/bootstrap-multiselect.js',
        'js/private/index.js',
        "js/private/support.js",
        'js/private/libs/routie.min.js',
        'js/private/libs/bootstrap-notify.min.js',
        'js/private/escort-profile.js',
        'js/private/app.js',
    ),

    'register' => array(
        'js/register.js',
        'js/jquery-ui.min.js'
    ),

    'contacts' => array(
        'js/contacts.js',
    ),

    'links' => array(),

    'review' => array(
        'js/reviews.js',
        'js/country.js',
    ),

    'prices' => array(
        'js/prices.js',
        'js/bootstrap/tab.js',
        'js/bootstrap/collapse.js',
    ),

    'classified-ads' => array(
        'js/country.js',
        'js/private/libs/bootstrap-notify.min.js',
        'js/bootstrap-multiselect.js',
        'js/bootstrap/collapse.js',
        'js/bootstrap/carousel.js',
        'js/bcSwipe.js',
        'js/adds.js',
    ),
);


$css_sources = array(
    'css/bootstrap/main.css',
    'css/private/bootstrap4-style.css',
    'css/flag-icon.min.css',
    'css/bootstrap/modal.css',
    'css/bootstrap/grid.css',
    'css/bootstrap/forms.css',
    'css/bootstrap/close.css',
    'css/bootstrap/alert.css',
    'css/popup.css',
    'css/global.css'
);
$css_other = array(
	'home-page' => array(
		'css/header.css',
		'css/footer.css',
		'css/homepage.css',
		'css/bootstrap-multiselect.css',
        'css/fontawesome.css',
		'css/autocomplete.css',
	),
	'blog' => array(
        'css/header.css',
        'css/footer.css',
        'css/blog.css',
        'css/blog-article.css',
        'css/fontawesome.css',
        'css/autocomplete.css',
    ),
	'agency-list' => array(
		'css/header.css',
		'css/footer.css',
		'css/fontawesome.css',
		'css/bootstrap-multiselect.css',
		'css/homepage.css',
		'css/autocomplete.css',
	),

    'agency-profile' => array(
        'css/header.css',
        'css/footer.css',
        'css/fontawesome.css',
        'css/homepage.css',
        'css/bootstrap/breadcrumb.css',
        'css/agencie-profile.css',
    ),
    'comments' => array(
        'css/header.css',
        'css/footer.css',
        'css/homepage.css',
        'css/bootstrap-multiselect.css',
        'css/fontawesome.css',
        'css/autocomplete.css',
        'css/comments.css',
    ),
    'profile' => array(
        'css/header.css',
        'css/footer.css',
        'css/fontawesome.css',
        'css/bootstrap/images.css',
        'css/bootstrap/carousel.css',
        'css/bootstrap/breadcrumb.css',
        'css/profile.css',
        'css/profile-review-modal.css',
        'css/bootstrap-multiselect.css',
        'css/private/bootstrap-datepicker.css',
        'css/autocomplete.css',
    ),

    'member-profile' => array(
        'css/header.css',
        'css/footer.css',
        'css/fontawesome.css',
        'css/bootstrap/images.css',
        'css/bootstrap/carousel.css',
        'css/bootstrap/breadcrumb.css',
        'css/member-profile.css',
        'css/profile.css',
        'css/profile-review-modal.css',
        'css/bootstrap-multiselect.css',
        'css/autocomplete.css',
    ),

    'private' => array(
        'css/fontawesome.css',
        'css/header.css',
        'css/footer.css',
        'css/private/chat.css',
        'css/private/common.css',
        'css/private/normalizer.css',
    ),

    'register' => array(
        'css/fontawesome.css',
        'css/header.css',
        'css/footer.css',
        'css/register.css'
    ),

    'links' => array(
        'css/header.css',
        'css/fontawesome.css',
        'css/footer.css',
        'css/bootstrap/images.css',
        'css/bootstrap/breadcrumb.css',

    ),

    'review' => array(
        'css/header.css',
        'css/footer.css',
        'css/fontawesome.css',
        'css/reviews.css',
        'css/autocomplete.css',
    ),

    'contacts' => array(
        'css/header.css',
        'css/footer.css',
        'css/fontawesome.css',
        'css/bootstrap/breadcrumb.css',
        'css/contacts.css',
    ),

    'prices' => array(
        'css/header.css',
        'css/footer.css',
        'css/flag-icon.min.css',
        'css/fontawesome.css',
        'css/bootstrap/nav.css',
        'css/bootstrap/breadcrumb.css',
        'css/prices.css',
    ),

    'classified-ads' => array(
        'css/header.css',
        'css/footer.css',
        'css/flag-icon.min.css',
        'css/fontawesome.css',
        'css/bootstrap/images.css',
        'css/bootstrap/carousel.css',
        'css/bootstrap/breadcrumb.css',
        'css/bootstrap-multiselect.css',
        'css/adds.css',
        'css/autocomplete.css',
    ),

);

$load_key = $_GET['load_key'];
$type = $_GET['type'];

/* Validate all get params */
$types = array('css', 'js');

if (!in_array($type, $types)) {
    die('Invalid type was specified');
}

if ($type == 'css') {
    if (!array_key_exists($load_key, $css_other)) {
        die('Invalid load_key was specified');
    }
} elseif ($type == 'js') {
    if (!array_key_exists($load_key, $other_sources)) {
        die('Invalid load_key was specified');
    }
}

/* Validate all get params */
$SOURCE_LOCATION = ($type == 'css') ? $CSS_LOCATION : $JS_LOCATION;
if (strlen($load_key)) {
    $SOURCE_LOCATION .= '_' . $load_key . '.' . $type;
} else {
    $SOURCE_LOCATION .= $type == 'js' ? '_scripts.js' : '_styles.css';
}

if ($type == 'js') {

    $sanitize_js_for = [
        'private' => ['bootstrap']
    ];

    // Sanitize and remove conflicted sources
    if (array_key_exists($load_key, $sanitize_js_for)) {

		foreach ($js_sources as $key => $source)
			foreach ($sanitize_js_for[$load_key] as $conflict_source)
				if (strpos($source, $conflict_source))
					unset($js_sources[$key]);
	}
	// 
	$_sources = array_merge($js_sources, (strlen($load_key) &&  isset($other_sources[$load_key]) ? $other_sources[$load_key] : array()));
} elseif ($type == 'css') {
	$_sources = array_merge($css_sources, (strlen($load_key) &&  isset($css_other[$load_key])) ? $css_other[$load_key] : array());
}

if (file_exists($SOURCE_LOCATION)) {
    $file_modif_time = @filemtime($SOURCE_LOCATION);

	if (getMaxModifTime($_sources) > $file_modif_time) {

		writeToFile($_sources, $SOURCE_LOCATION);
	} else {
		// no need to write again (cache issue)
    }
} else {
	writeToFile($_sources, $SOURCE_LOCATION);
}

$js = file_get_contents($SOURCE_LOCATION);

if ($type == 'js') {
	header('content-type: text/javascript; charset: UTF-8');
} elseif ($type == 'css') {
	header('content-type: text/css;');
}
//USER CACHE VALIDATION
if (getMaxModifTime($_sources) < $file_modif_time && $_SERVER['HTTP_CACHE_CONTROL'] == 'max-age=0' && array_key_exists("HTTP_IF_MODIFIED_SINCE", $_SERVER)) {
	header("HTTP/1.1 304 Not Modified");
} else {
	header('last-modified: ' . gmdate('D, d M Y H:i:s', getMaxModifTime($_sources)) . ' GMT');
	header('cache-control: max-age=0');
}
header('expires: ' . gmdate('D, d M Y H:i:s', time() + $CACHE_OFFSET) . ' GMT');
header('date: ' . gmdate('D, d M Y H:i:s', time()) . ' GMT');


ob_start('ob_gzhandler');
echo $js;
ob_flush();
