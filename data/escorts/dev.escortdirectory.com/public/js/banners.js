if(typeof Cubix == "undefined") Cubix = {};

$(function() {

    // Probably user has Ad blocker, so we just need to remove all empty
    // blocks for advertisements
    if(typeof zonesJsonBuffer == "undefined") {
        $('.ixs-banner-item').remove();
        return;
    }

    Cubix.bannerPosition = 0;
    Cubix.mainBanners =  Object.assign({}, zonesJsonBuffer.x323139);
    Cubix.initBanners();
    // Cubix.initProfileBanners();
});

Cubix.BANNERS_HOST = 'https://adzz.io/';

Cubix.initProfileBanners = function(){

    if(!$('countrik')){
        return true;
    }

    var cntryId = $('countrik').getAttribute('value'),
        cityId = $('city_ban').getAttribute('value');


    var listingBanners = new Object();
    var limit = 6;
    var forceFirstIter = 0;
    var phonesexslut = null;


    var isItalyFirstPage = ( cntryId === '33') ? true : false;
    var isCanadaFirstPage = ( cntryId === '10') ? true : false;
    var isNetherlandFirstPage = ( cntryId === '27') ? true : false;
    var isUsaFirstPage = ( cntryId === '68') ? true : false;
    var isThailandFirstPage = ( cntryId === '63') ? true : false;
    var isSwissFirstPage = ( cntryId === '62') ? true : false;
    var isFranceFirstPage = ( cntryId === '23') ? true : false;
    var isUnitedkingdomFirstPage = ( cntryId === '67') ? true : false;
    var isMemphisFirstPage = (cityId === '411') ? true : false;
    var isFrankfurtFirstPage = ( cityId === '197') ? true : false;

    Object.each(zonesJsonBuffer.x323230, function(value, key){

        if(	(isCanadaFirstPage && (value.cmp_id == 1076 || value.cmp_id == 1097 || value.cmp_id == 1156 || value.cmp_id == 1175 || value.cmp_id == 1181 )) ||
            (isUsaFirstPage && (value.cmp_id == 1090 || value.cmp_id == 1091 || value.cmp_id == 1096 || value.cmp_id == 1107 || value.cmp_id == 1103 || value.cmp_id == 1166)) ||
            (isItalyFirstPage && (value.cmp_id == 1093 || value.cmp_id == 1092 || value.cmp_id == 1099 || value.cmp_id == 1131)) ||
            (isNetherlandFirstPage && ( value.cmp_id == 1079 || value.cmp_id == 1110 || value.cmp_id == 1160 || value.cmp_id == 1196 )) ||
            (isThailandFirstPage && value.cmp_id == 1098) ||
            (isSwissFirstPage && (value.cmp_id == 1106 || value.cmp_id == 1219)) ||
            (isUnitedkingdomFirstPage && value.cmp_id == 1147) ||
            (isFranceFirstPage && value.cmp_id == 1104) ||
            (value.cmp_id == 1222) || (isMemphisFirstPage && value.cmp_id == 1225) || (isFrankfurtFirstPage && value.cmp_id == 1235) || (value.cmp_id == 1231)
        ){

            listingBanners[forceFirstIter] = value;

            forceFirstIter++;

            delete zonesJsonBuffer.x323230[key];
        }
    });

    if(isUsaFirstPage && phonesexslut){
        listingBanners[forceFirstIter] = phonesexslut;
        forceFirstIter++;
    }
    Object.each(zonesJsonBuffer.x323230, function(value, key){
        listingBanners[forceFirstIter] = value;
        forceFirstIter++;
    });

    // Object.each(zonesJsonBuffer.x36, function(value, key){
    //     listingBanners[forceFirstIter] = value;
    //     forceFirstIter++;
    // });
    // console.log( listingBanners);
    var target = ["_blank","_self","_new","_parent"];
    var size = [];

    if($('adv-space')){

        $$('#adv-space').each(function(e) {

            for (i = 0; i < limit; i++) {


                var aElement = new Element('a', {
                    'rel' : 'nofollow',
                    'class' : 'ixsHitBtn',
                    'target': target[ listingBanners[i].target],
                    'goto' : Cubix.BANNERS_HOST + 'hits.php?adv='+ listingBanners[i].user_id + '&cmp='+ listingBanners[i].cmp_id + '&z=' + listingBanners[i].id + '&b='+ listingBanners[i].banner_id + '&go=' + encodeURIComponent(listingBanners[i].link),
                    'href' : listingBanners[i].link
                });

                //size = listingBanners[pos].size.split("x");
                new Element('img', {
                    'alt': listingBanners[i].title,
                    'title': listingBanners[i].title,
                    'width': 360, //size[0]
                    'height' : 200, // size[1]
                    'src' : Cubix.BANNERS_HOST + 'uploads/' + listingBanners[i].filename
                }).inject(aElement);

                new Element('img', {
                    'class' : 'style',
                    'width': 1,
                    'height': 1,
                    'src' : Cubix.BANNERS_HOST + 'views.php?adv='+ listingBanners[i].user_id + '&cmp='+ listingBanners[i].cmp_id + '&z=' + listingBanners[i].id +'&b='+ listingBanners[i].banner_id
                }).inject(aElement);

                aElement.inject(e);




            }
        });


    }
}


Cubix.initBanners = function(){

    if(typeof zonesJsonBuffer == 'undefined') return;

    if(!$('body').hasClass('index')){
        return console.warn('Banners deactivated');
    }

    var path = window.location.search;
    var countryId = $('#country_id').val();
    if(countryId) {
        countryId = countryId.match(/\d/g);
        countryId = countryId.join("");
    }
    var cityId = $('#city_id').val();
    if(cityId) {
        cityId = cityId.match(/\d/g);
        cityId = cityId.join("");
    }

    if($('#region_id').val()){
        var stateId = $('#region_id').val();
    }

    var listingBanners = {};
    var limit = 3;
    var $_GET = Sceon.parseQueryString();
    var forceFirstIter = 0;
    var phonesexslut = null;
    var isFirstPage = $_GET['page'] == undefined || $_GET['page'] == 1;


    var isItalyFirstPage = ( countryId === '33'  && isFirstPage) ? true : false;
    var isUnitedkingdomFirstPage = ( countryId === '67'  && isFirstPage) ? true : false;
    var isCanadaFirstPage = ( countryId === '10' && isFirstPage) ? true : false;
    var isNetherlandFirstPage = ( countryId === '27' && isFirstPage) ? true : false;
    var isUsaFirstPage = ( countryId === '68' && isFirstPage) ? true : false;
    var isThailandFirstPage = ( countryId === '63' && isFirstPage) ? true : false;
    var isSwissFirstPage = ( countryId === '62' && isFirstPage) ? true : false;
    var isFranceFirstPage = ( countryId === '23' && isFirstPage) ? true : false;
    var isMemphisFirstPage = (cityId === '411' && isFirstPage) ? true : false;
    var isFrankfurtFirstPage = (cityId === '197' && isFirstPage) ? true : false;
    var isUaeFirstPage = (countryId === '66' && isFirstPage) ? true : false;
    var isLondonFirstPage = (cityId === '54' && isFirstPage) ? true : false;
    var islasVegasFirstPage = (cityId === '86' && isFirstPage) ? true : false;
    var isNewYorkFirstPage = (cityId === '325' && isFirstPage) ? true : false;
    var isHomePage = (window.location.pathname == '/'  && isFirstPage) ? true : false;
    var isNewYorkStateFirstPage = (stateId === '18' && isFirstPage) ? true : false;
    var isFloridaStateFirstPage = (stateId === '13' && isFirstPage) ? true : false;
    var isCaliforniaStateFirstPage = (stateId === '9' && isFirstPage) ? true : false;
    var isViennaFirstPage = (cityId === '586' && isFirstPage) ? true : false;
    var isMalasiaFirstPage = ( countryId === '40'  && isFirstPage) ? true : false;
    var isDubaiFirstPage = (cityId === '145' && isFirstPage) ? true : false;
    var isAmsterdamFirstPage = (cityId === '131' && isFirstPage) ? true : false;

    for(var key in zonesJsonBuffer.x323139) {
        var value = zonesJsonBuffer.x323139[key];

        if(	(isCanadaFirstPage && (value.cmp_id == 1076 || value.cmp_id == 1097 || value.cmp_id == 1156 || value.cmp_id == 1175 || value.cmp_id == 1181 || value.cmp_id == 1242 )) ||
            (islasVegasFirstPage && (value.cmp_id == 1300|| value.cmp_id == 1301)) ||
            (isUsaFirstPage && (value.cmp_id == 1090 || value.cmp_id == 1091 || value.cmp_id == 1096 || value.cmp_id == 1107 || value.cmp_id == 1103 || value.cmp_id == 1166 || value.cmp_id == 1258 || value.cmp_id == 1262|| value.cmp_id == 1307)) ||
            (isItalyFirstPage && (value.cmp_id == 1093 || value.cmp_id == 1092 || valuef.cmp_id == 1099 || value.cmp_id == 1131)) ||
            (isNetherlandFirstPage && ( value.cmp_id == 1079 || value.cmp_id == 1110 || value.cmp_id == 1160 || value.cmp_id == 1196 || value.cmp_id == 1197 || value.cmp_id == 1198 || value.cmp_id == 1334 || value.cmp_id == 1199 || value.cmp_id == 1200  )) ||
            (isThailandFirstPage && value.cmp_id == 1098) ||
            (isSwissFirstPage && (value.cmp_id == 1106 || value.cmp_id == 1219)) ||
            (isUnitedkingdomFirstPage && (value.cmp_id == 1147 || value.cmp_id == 1241)) ||
            (isFranceFirstPage && value.cmp_id == 1104) ||
            (isMemphisFirstPage && (value.cmp_id == 1225 || value.cmp_id == 1296)) ||
            (isFrankfurtFirstPage && value.cmp_id == 1235) ||
            (isUaeFirstPage && value.cmp_id == 1262) ||
            (isMalasiaFirstPage && (value.cmp_id == 1357 || value.cmp_id == 1358 || value.cmp_id == 1359)  ) ||
            (isUnitedkingdomFirstPage && value.cmp_id == 1284) ||
            (isLondonFirstPage && value.cmp_id == 1284) || ((isFloridaStateFirstPage || isCaliforniaStateFirstPage || isNewYorkStateFirstPage) && value.cmp_id == 1307) || (isViennaFirstPage && value.cmp_id == 1332) || (isDubaiFirstPage && value.cmp_id == 1349) ||
            (isViennaFirstPage && value.cmp_id == 1365) ||
            (isAmsterdamFirstPage && value.cmp_id == 1372)
        ){
            if(isUsaFirstPage && value.cmp_id == 1103){
                phonesexslut = value;
            }
            else{
                listingBanners[forceFirstIter] = value;
                Cubix.bannerPosition = 0;
                forceFirstIter++;
            }
            delete Cubix.mainBanners[key];
        }
    };

    if(isUsaFirstPage && phonesexslut){
        listingBanners[forceFirstIter] = phonesexslut;
        forceFirstIter++;
    }

    for(var key in Cubix.mainBanners) {
        var value = Cubix.mainBanners[key];

        listingBanners[forceFirstIter] = value;
        if(!isHomePage && value.cmp_id == 1027){
            delete listingBanners[forceFirstIter];
        } else {
            forceFirstIter++;
        }
    };

    for(var key in zonesJsonBuffer.x36) {
        var value = zonesJsonBuffer.x36[key];

        listingBanners[forceFirstIter] = value;
        forceFirstIter++;
    };

    var target = ["_blank","_self","_new","_parent"];
    var size = [];

    if($('.escort-item.ixs-banner-item.empty').length){

        var pos = Cubix.bannerPosition;
        var count = Object.values(listingBanners).length;

        $('.escort-item.ixs-banner-item.empty').each(function(e, iter) {

            // for (i = 0; i < limit; i++) {

                if (listingBanners[pos].cmp_id == 1027) {

                    var aElement = $('<a />', {
                        'class': 'ixsHitBtn',
                        'target': target[listingBanners[pos].target],
                        'goto': Cubix.BANNERS_HOST + 'hits.php?adv=' + listingBanners[pos].user_id + '&cmp=' + listingBanners[pos].cmp_id + '&z=' + listingBanners[pos].id + '&b=' + listingBanners[pos].banner_id + '&go=' + encodeURIComponent(listingBanners[pos].link),
                        'href': listingBanners[pos].link
                    });


                } else {
                    var aElement = $('<a />', {
                        'rel': 'nofollow',
                        'class': 'ixsHitBtn',
                        'target': target[listingBanners[pos].target],
                        'goto': Cubix.BANNERS_HOST + 'hits.php?adv=' + listingBanners[pos].user_id + '&cmp=' + listingBanners[pos].cmp_id + '&z=' + listingBanners[pos].id + '&b=' + listingBanners[pos].banner_id + '&go=' + encodeURIComponent(listingBanners[pos].link),
                        'href': listingBanners[pos].link
                    });
                }
                // }
                //size = listingBanners[pos].size.split("x");
                $('<img />', {
                    'alt': listingBanners[pos].title,
                    'title': listingBanners[pos].title,
                    //'width': 265, //size[0]
                    //'height' : 150, // size[1]
                    'src' : Cubix.BANNERS_HOST + 'uploads/' + listingBanners[pos].filename
                }).appendTo(aElement);

                // $('<img />', {
                //     'class' : 'style',
                //     'width': 1,
                //     'height': 1,
                //     'src' : Cubix.BANNERS_HOST + 'views.php?adv='+ listingBanners[pos].user_id + '&cmp='+ listingBanners[pos].cmp_id + '&z=' + listingBanners[pos].id +'&b='+ listingBanners[pos].banner_id
                // }).appendTo(aElement);

                aElement.appendTo($(this));
                $(this).removeClass('empty');


                if(pos == (count - 1)){
                    pos = 0;
                }
                else{
                    pos++;
                }

            // }
        });

        Cubix.bannerPosition = pos;
    }
}
