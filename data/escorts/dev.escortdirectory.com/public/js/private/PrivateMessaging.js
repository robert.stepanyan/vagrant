/*-->Private Messaging*/
PrivateMessaging = {
    is_sending: false,
    is_blocked: false,
    currentConversation : {
        interlocutor: null
    }
};

PrivateMessaging.Load = function () {

    var url = lang_id + '/private-messaging/index';
    var container = $('#ui-view');

    container.LoadingOverlay("show", {
        color: "rgba(239, 243, 249, 0.80)",
        zIndex: 1000
    });

    // ????? 
    //--------------------------------------------
    // if ( $defined($('thread-agency-escort-id')) && $('thread-agency-escort-id').get('value') ) {
    // 	url += '&a_escort_id=' + $('thread-agency-escort-id').get('value');
    // }
    //--------------------------------------------

    $.ajax({
        url: url,
        type: 'get',
        success: function (resp) {
            container.html(resp);
            $('body').addClass('chat-on');
            $('.user-area-escort > .wait-for-load').removeClass('wait-for-load');

            container.LoadingOverlay("hide");

            if (!$('#pm_disabled').length) {
                var xhr = PrivateMessaging.LoadThreads(container);
                // PrivateMessaging.LoadContacts();


                xhr.done(function() {
                    // Activate first available thread
                    // --------------------------------
                    var $conv = $('.conversation.single-contact-thread').first();
                    if($conv.length && Sceon.isMobile() == false) {

                        // Set the active tab to the first available thread
                        // --------------------------
                        //PrivateMessaging.set_visibility_filter($conv.data('visible-on'));
                        // --------------------------

                        $conv.trigger('click');

                    }

                    if($conv.length <= 0) {
                        $('.dialog-content-container .dialog-content').show();
                    }
                    // --------------------------------
                })

            }
            //overlayDashboard.enable();
        }
    });

    return false;
};

PrivateMessaging.set_visible_panel = function (active_panel) {

    if (active_panel == 'dialog') {
        $('.chat .dialog').addClass('show');
        $('.chat .conversations').removeClass('show');
    } else if (active_panel == 'conversations') {
        $('.chat .dialog').removeClass('show');
        $('.chat .conversations').addClass('show');
    }
};

PrivateMessaging.LoadThreads = function (page) {
    if (undefined == page || page < 1) {
        if ($('.private-messages .pages').length) {
            page = $('.private-messages .pages span').html();
        } else {
            page = 1;
        }
    }

    var container = $('.threads-container .contact-threads'),
        singleMessageContainer = $('.single-thread'),
        sendBtn = $('#send-btn'),
        messageTextField = $('input#message-text'),
        searchInput = $('.search-bar-container').find('input'),
        url = lang_id + '/private-messaging/get-threads';

    container.LoadingOverlay('show', {image: '/images/loading_spinner.gif'});

    return $.ajax({
        url: url,
        type: 'get',
        success: function (resp) {
            container.LoadingOverlay('hide');
            container.html(resp);

            if(PrivateMessaging.currentConversation.interlocutor) {
                $('.single-contact-thread[data-id='+PrivateMessaging.currentConversation.interlocutor+']').addClass('active');
            }

            // if($('.filter-btn.sent').hasClass('active'))
            //     PrivateMessaging.set_visibility_filter('sent');
            // else
            //     PrivateMessaging.set_visibility_filter('inbox');

            var singleThreads = container.find('.single-contact-thread');
            var unreadCount = container.find('.single-contact-thread.unread');
            var escortId = headerVars.currentUser.id;

            singleThreads.on('click', function () {
                singleThreads.removeClass('active');
                $(this).addClass('active');
                $('.new-message-text').removeClass('alert-danger');
                PrivateMessaging.set_visible_panel('dialog');
                PrivateMessaging.ShowThread($(this), escortId);
            });

            searchInput.on('keyup', function () {
                PrivateMessaging.searchByUserName($(this).val().trim());
            });

            sendBtn
                .off('click')
                .on('click', function (e) {
                    e.preventDefault();

                    if (!$('.new-message-text').val()) {
                        return;
                    }

                    if(PrivateMessaging.is_blocked) {
                        return;
                    }

                    PrivateMessaging.SendComposeMessage(singleMessageContainer);
                });

            messageTextField
                .off('keypress')
                .on('keypress', function (e) {
                    if (e.which !== 13 || !$('.new-message-text').val()) {
                        return;
                    }
                    e.preventDefault();

                    if(PrivateMessaging.is_blocked) {
                        return;
                    }

                    PrivateMessaging.SendComposeMessage(singleMessageContainer);
                });

            var removeConversation = $('.contact-threads').find('.single-contact-thread').find('.remove-conversation');

            if (removeConversation.length) {
                removeConversation.each(function (el) {
                    el.on('click', function (e) {
                        e.preventDefault();
                        var threadId = $(this).parents('.single-contact-thread').attr('data-id');
                        PrivateMessaging.RemoveThread(threadId, headerVars.currentUser.id);
                    });
                });
            }

            var selectThreadParticipantInfo = $('.action-btns').find('.user-name span').attr('data-participant');
            if (selectThreadParticipantInfo) {
                $('.contact-threads').find('.single-contact-thread').each(function (el) {
                    if ($(this).attr('data-participant') == selectThreadParticipantInfo) {
                        $(this).trigger('click');
                    }
                });

                if ($('.contact-threads').find('.single-contact-thread.selected.unread').length) {
                    $('.contact-threads').find('.single-contact-thread').trigger('click');
                }
            }

            $('.conversation-count-all').html(singleThreads.length + ' Conversations');
            $('.conversation-count-unread').html(unreadCount.length + ' Unread');
        }
    });

    return false;
};

PrivateMessaging.UpdateCurrentUser = function() {
    if(PrivateMessaging.currentConversation.interlocutor) {

        var params = {
            'show_loader': false
        };

        PrivateMessaging.ShowThread(
            $('.conversation[data-id="' + PrivateMessaging.currentConversation.interlocutor + '"]'),
            headerVars.currentUser.id,
            params
        );
    }
};

PrivateMessaging.scrollMessagesToEnd = function () {
    if(Sceon.isMobile()) {
        var y = document.querySelector('.dialog-content-container').scrollHeight;
        $('.dialog-content-container').animate({scrollTop: y * y}, 300);
    }else{
        var y = document.querySelector('.dialog-content').scrollHeight;
        $('.dialog-content').animate({scrollTop: y * y}, 300);
    }
};

PrivateMessaging.ShowThread = function (el, escortId, params = {}) {

    if (headerVars.is_agency) {
        escortId = null;
    }

    var container = $('.single-thread'),
        userAvatarUrl = el.find('.conversation-logo').css('background-image'),
        participantInfo = el.attr('data-participant'),
        agencyEscortId = el.attr('data-escort-id'),
        usernameContainer = $('.action-btns .name'),
        threadId = el.attr('data-id');

    el.addClass('active');
    PrivateMessaging.currentConversation.interlocutor = threadId;

    if(headerVars.currentUser.user_type == 'escort' && participantInfo.includes('member')) {
        $('#open-participand-profile-btn').hide();
    }else{
        $('#open-participand-profile-btn').show();
    }

    if(params.show_loader !== false) {
        $('.dialog').LoadingOverlay("show", {
            color: "rgba(239, 243, 249, 0.80)",
            zIndex: 1000
        });
    }

    var url = lang_id + '/private-messaging/get-thread?id=' + threadId;
    if (escortId) {
        url += '&escort_id=' + escortId;
    }

    $.ajax({
        url: url,
        type: 'get',
        success: function (resp) {
            $('.dialog-content-container .dialog-content').show();
            container.html(resp);

            $('.dialog').LoadingOverlay("hide");
            $('.dialog-top .avatar').css('background-image', userAvatarUrl);

            $('.action-btns .age').html($('#participant_age').val())
            $('.action-btns .country').html($('#participant_country').val())
            $('.action-btns .city').html($('#participant_city').val())

            if ($('.conversation.active').attr('data-blocked') == 'true') {
                $('.dialog').addClass('blocked');
            } else {
                $('.dialog').removeClass('blocked');
            }

            if (!$('#participant_country').val()) $('.action-btns .point').hide();
            else $('.action-btns .point').show();

            usernameContainer.html(el.find('.conversation-name').html());
            usernameContainer.attr('data-participant', participantInfo);
            if (agencyEscortId) {
                usernameContainer.attr('data-escort-id', agencyEscortId);
            }

            el.siblings('.single-contact-thread').removeClass('selected');
            el.addClass('selected');

            $('.new-message-text').attr('contenteditable', true);

            if(el.hasClass('unread')) {
                var current_count = Cookies.get('pm_new_messages');
                console.log("Count was: " + current_count)
                PrivateMessaging.updateUnreadThreadsCount(current_count - 1);
                el.removeClass('unread');
            }

            PrivateMessaging.scrollMessagesToEnd();
        }
    });
};

PrivateMessaging.LoadEmoticons = function () {
    if ($$('.emoticons-wrapper').length) return;

    var emoticonsCount = 60,
        url = '/img/private/private-messaging/_emoticons/',
        emoticonsArray = [];

    for (var i = emoticonsCount; i >= 1; i--) {
        emoticonsArray.push(url + i + '.png');
    }

    var emoticonsWrapper = new Element('div', {
        class: 'emoticons-wrapper hidden'
    });

    var emoticonsContainer = new Element('div', {
        class: 'emoticons-container'
    });

    for (var i = emoticonsArray.length - 1; i >= 0; i--) {
        emoticonIcon = new Element('span', {
            styles: {
                'background-image': 'url(' + emoticonsArray[i] + ')',
            }
        });

        emoticonIcon.addEvent('click', function (e) {
            e.stop();
            var backgroundImageUrl = this.getStyle('background-image'),
                imgUrl = backgroundImageUrl.match(/\((.*?)\)/)[1].replace(/('|")/g, ''),
                emoticonToImage = new Element('img', {
                    'src': imgUrl,
                    'class': 'emoticons'
                });
            emoticonToImage.inject($$('.new-message-text')[0]);
            PrivateMessaging.ToggleEmoticons();
        })

        emoticonIcon.inject(emoticonsContainer);
    }

    emoticonsContainer.inject(emoticonsWrapper);
    emoticonsWrapper.inject($$('.new-message-action-btns')[0]);

    new GeminiScrollbar({
        element: emoticonsContainer,
        autoshow: true
    }).create();

    $('emoticons-btn').addEvent('click', function (e) {
        e.stop();
        PrivateMessaging.ToggleEmoticons();
    });

    $$('.new-message-text').addEvent('keyup', function () {
        var url = '/img/private/private-messaging/_emoticons/',
            str = this.get('html').replace('&nbsp;').slice(-2),
            replaceStr = '';

        switch (str) {
            case ':)':
                replaceStr = '<img src="' + url + '6.png" class="emoticons">';
                break;
            case ':(':
                replaceStr = '<img src="' + url + '14.png" class="emoticons">';
                break;
            default:
                replaceStr = '';
        }

        if (replaceStr) {
            this.set('html', this.get('html').replace(str, replaceStr));
            this.set('html', this.get('html').replace('&nbsp;', ''));
            PrivateMessaging.setEnd(this);
        }
    });
};

PrivateMessaging.LoadSettings = function (threadEl) {
    if ($$('.settings-container')[0]) {
        $$('.settings-container')[0].dispose();
        $$('.cog-contacts')[0].removeEvents();

    }

    var settingsContainer = new Element('div', {
        'class': 'settings-container hidden'
    });

    var settingsListWrapper = new Element('ul', {
        'class': 'settings-list'
    });

    // Depending on functionality change, can be expended
    var arrMenuList = {
        'delete': 'Delete Conversation',
        'block': 'Block Messages',
        'unblock': 'Unblock Messages'
    };

    var isBlock = threadEl.get('data-blocked') == 'true';

    if (isBlock) {
        delete arrMenuList['block'];
    } else {
        delete arrMenuList['unblock'];
    }

    for (var menuItem in arrMenuList) {
        var item = new Element('li', {
            'class': menuItem + '-item',
            'html': '<a href="javascript:void(0)">' + arrMenuList[menuItem] + '</a>'
        });

        item.inject(settingsListWrapper);
    }

    settingsListWrapper.inject(settingsContainer);
    settingsContainer.inject($$('.cog-contacts')[0]);

    $$('.cog-contacts')[0].addEvent('click', function (e) {
        e.stop();
        PrivateMessaging.ToggleSettings();
    });

    $$('.settings-list .delete-item > a')[0].addEvent('click', function (e) {
        e.stop();

        var threadId = $$('.contact-threads .single-contact-thread.selected') ?
            $$('.contact-threads .single-contact-thread.selected')[0].get('data-id') :
            null;

        if (!threadId) return;

        PrivateMessaging.RemoveThread(threadId, headerVars.currentUser.id);
        PrivateMessaging.ToggleSettings();
    });

    if (!isBlock) {
        $$('.settings-list .block-item > a')[0].addEvent('click', function (e) {
            e.stop();

            threadId = $$('.contact-threads .single-contact-thread.selected') ?
                $$('.contact-threads .single-contact-thread.selected')[0].get('data-id') :
                null;
            blockedParticipantInfo = $$('.contact-threads .single-contact-thread.selected') ?
                $$('.contact-threads .single-contact-thread.selected')[0].get('data-participant') :
                null;
            _selfEscortId = $$('.contact-threads .single-contact-thread.selected') ?
                $$('.contact-threads .single-contact-thread.selected')[0].get('data-escort-id') :
                null;
            PrivateMessaging.BlockThread(threadId, _selfEscortId, blockedParticipantInfo);
        });
    } else {
        $$('.settings-list .unblock-item > a')[0].addEvent('click', function (e) {
            e.stop();

            threadId = $$('.contact-threads .single-contact-thread.selected') ?
                $$('.contact-threads .single-contact-thread.selected')[0].get('data-id') :
                null;
            PrivateMessaging.UnBlockThread(threadId, headerVars.currentUser.id);
        });
    }
};

PrivateMessaging.LoadContacts = function () {
    // var newPmOverlay = new Cubix.Overlay($('new-pm-btn'));
    // newPmOverlay.disable();

    // if ($('.contacts-container').length){
    // 	$$('.contacts-container')[0].dispose();
    // }

    // new Request({
    // 	url: lang_id + '/private-messaging/contacts',
    // 	onSuccess: function(resp){
    // 		var contactsContainer = new Element('div', {
    // 			'class': 'contacts-container hidden',
    // 			'html': resp
    // 		});

    // 		var additionalParticipantsContainer = new Element('div', {
    // 			'class': 'additional-participants-container hidden',
    // 			'html': resp
    // 		});

    // 		contactsContainer.inject($$('.new-pm-container')[0]);
    // 		additionalParticipantsContainer.inject($$('.add-to-contacts')[0]);

    // 		$('new-pm-btn').removeEvents();
    // 		$('new-pm-btn').addEvent('click', function(e){
    // 			e.stop();
    // 			PrivateMessaging.ToggleContacts();
    // 		});

    // 		$('contact-plus-icon').removeEvents();
    // 		$('contact-plus-icon').addEvent('click', function(e){
    // 			e.stop();
    // 			PrivateMessaging.ToggleAdditionalParticipants();
    // 		});

    // 		$$('.contacts-container .single-contact-wrapper').addEvent('click', function(e){
    // 			e.stop();
    // 			var participantInfo = this.get('data-value'),
    // 				participantUserName = this.get('html'),
    // 				presentConversations = $$('.contact-threads')[0].getElements('.single-contact-thread'),
    // 				created = false;

    // 				presentConversations.each(function(el){
    // 					if (el.get('data-participant') == participantInfo) {
    // 						el.fireEvent('click');
    // 						created = true;
    // 					}
    // 				});

    // 				if(!created){
    // 					$$('.single-thread')[0].set('html', '');

    // 					var pmThreadContainer =  new Element('div', {
    // 						'id': 'pm-thread-container',
    // 						'class': 'pm-popup'
    // 					});

    // 					var conversationBody = new Element('div', {
    // 						'class': 'body'
    // 					});

    // 					var conversation = new Element('div', {
    // 						'class': 'conversation'
    // 					});

    // 					conversation.inject(conversationBody);
    // 					conversationBody.inject(pmThreadContainer);
    // 					pmThreadContainer.inject($$('.single-thread')[0]);

    // 					$$('.new-message-text')[0].set('contenteditable', true);

    // 					new GeminiScrollbar({
    // 				    	element: $$('.single-thread')[0],
    // 				    	autoshow: true,
    // 				    	onResize: $$('.single-thread')[0].getElement('.conversation')
    // 				    }).create();

    // 				    PrivateMessaging.LoadEmoticons();

    // 					$('contact-plus-icon').removeClass('none');
    // 					$('cog-icon').removeClass('none');

    // 					var usernameContainer = $$('.action-btns .user-name span')[0],
    // 						participantInfo = this.get('data-value'),
    // 						avatarImg = $$('.action-btns .user-name img');

    // 					usernameContainer.set('html', this.get('html'));
    // 					usernameContainer.set('data-participant', participantInfo);
    // 					avatarImg.set('src', '/img/default-avatar.png');
    // 				}

    // 				PrivateMessaging.ToggleContacts();
    // 		});

    // 		$$('.additional-participants-container .single-contact-wrapper').addEvent('click', function(e){
    // 			var addedParticipantInfo = this.get('data-value'),
    // 				addedParticipantName = this.get('html');
    // 			PrivateMessaging.addConversationParticipants(addedParticipantName, addedParticipantInfo);
    // 		});

    // 		newPmOverlay.enable();
    // 	}
    // }).send();
};

PrivateMessaging.searchByUserName = function (needle) {

    var contactThreads = $('.contact-threads').find('.single-contact-thread');

    if (!needle) {
        contactThreads.each(function () {
            $(this).removeClass('d-none');
        });

        return;
    }

    contactThreads.each(function () {
        if (!~$(this).find('.conversation-name a').html().trim().indexOf(needle)) {
            $(this).addClass('d-none');
        } else {
            $(this).removeClass('d-none');
        }
    });
};

PrivateMessaging.setEnd = function (contentEditableElement) {
    var range, selection;

    if (document.createRange) {
        range = document.createRange();
        range.selectNodeContents(contentEditableElement);
        range.collapse(false);
        selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
    } else if (document.selection) {
        range = document.body.createTextRange();
        range.moveToElementText(contentEditableElement);
        range.collapse(false);
        range.select();
    }
};

PrivateMessaging.ToggleEmoticons = function (container) {
    var emoticonsContainer = $$('.emoticons-wrapper')[0];

    if (emoticonsContainer) {
        emoticonsContainer.toggleClass('hidden');

        if (!emoticonsContainer.hasClass('hidden')) {
            $(document).removeEvents().addEvent('click', function (e) {
                e.stopPropagation();
                emoticonsContainer.toggleClass('hidden');
                $(document).removeEvents();
            });
        } else {
            $(document).removeEvents();
        }
    }
};

PrivateMessaging.ToggleContacts = function () {
    var contactsContainer = $$('.contacts-container')[0];

    if (contactsContainer) {
        contactsContainer.toggleClass('hidden');

        if (!contactsContainer.hasClass('hidden')) {
            $(document).removeEvents().addEvent('click', function (e) {
                e.stopPropagation();
                contactsContainer.toggleClass('hidden');
                $(document).removeEvents();
            });
        } else {
            $(document).removeEvents();
        }
    }
};

PrivateMessaging.ToggleAdditionalParticipants = function () {
    var additionalParticipantsContainer = $$('.additional-participants-container')[0];

    if (additionalParticipantsContainer) {
        additionalParticipantsContainer.toggleClass('hidden');

        if (!additionalParticipantsContainer.hasClass('hidden')) {
            $(document).removeEvents().addEvent('click', function (e) {
                e.stopPropagation();
                additionalParticipantsContainer.toggleClass('hidden');
                $(document).removeEvents();
            });
        } else {
            $(document).removeEvents();
        }
    }
};

PrivateMessaging.ToggleSettings = function () {
    var settingsContainer = $$('.settings-container')[0];

    if (settingsContainer) {
        settingsContainer.toggleClass('hidden');

        if (!settingsContainer.hasClass('hidden')) {
            $(document).removeEvents().addEvent('click', function (e) {
                e.stopPropagation();
                settingsContainer.toggleClass('hidden');
                $(document).removeEvents();
            });
        } else {
            $(document).removeEvent();
        }
    }
};

PrivateMessaging.resetGroupSendForm = function () {
    $('selected-contacts-count').set('html', 0);
    $$('.section.send-form textarea').set('value', '');

    return false;
};

PrivateMessaging.addConversationParticipants = function (participantName, participantInfo) {
    var additionalRecepientsCount = $$('.additional-recepients-count')[0],
        additionalParticipantsWrapper = $$('.additional-participants-wrapper').length ?
            $$('.additional-participants-wrapper')[0] :
            new Element('div', {
                'class': 'additional-participants-wrapper'
            }),
        addedPaticipantContainer = new Element('div', {
            'data-participant': participantInfo,
            'html': participantName
        });

    if (!additionalRecepientsCount.contains(additionalParticipantsWrapper)) {
        additionalParticipantsWrapper.inject(additionalRecepientsCount);
    }

    addedPaticipantContainer.inject(additionalParticipantsWrapper);

    $$('additional-participants-container')
        .getElements('.single-contact-wrapper')
        .each(function (el) {
            if (el.get('data-value') == participantInfo) {
                el.addClass('none');
            }
        });
    if (additionalRecepientsCount.hasClass('none')) {
        additionalRecepientsCount.removeClass('none');
    }

};

// PrivateMessaging.sendGroupMessage = function() {
// 	var container = $$('.section.send-form')[0];

// 	var page_overlay = new Cubix.Overlay(container, {loader: _st('loader-small.gif')});
// 	page_overlay.disable();

// 	new Request.JSON({
// 		url: lang_id + '/private-messaging/send-message',
// 		method: 'POST',
// 		data: {
// 			participants:  $('selected-contacts').get('value'),
// 			message: $$('.section.send-form textarea')[0].get('value'),
// 			escort_id:  $$('.section.send-form select[name="escort_id"]').length ? $$('.section.send-form select[name="escort_id"]')[0].getSelected()[0].get('value') : ''
// 		},
// 		onSuccess: function (resp) {
// 			$$('.section.send-form')[0].getElements('.icon-error').addClass('none');

// 			if ( resp.status == 'error' ) {
// 				var errs = [];
// 				for(i in resp.msgs) {
// 					errs.push(resp.msgs[i]);
// 				}

// 				$$('.section.send-form')[0].getElements('.icon-error')[0].set('floatingtitle', errs.join(', ')).removeClass('none');
// 			} else {
// 				Sceon.showNotification('Your message has been successfully sent');
// 				PrivateMessaging.LoadThreads();
// 			}

// 			page_overlay.enable();
// 		}
// 	}).send();

// 	return false;
// };
PrivateMessaging.attacheFilesIsInited = false;
PrivateMessaging.attachFiles = function () {
    var onFileUpload = function (response) {
        var conversationWrapper = $$('.single-thread-container')[0].getElement('.conversation');
        var selfMessageContainer = new Element('div', {
            'class': 'self-message'
        });

        var selfInfoContainer = new Element('div', {
            'class': 'self-info clearfix'

        });

        var html = '';
        if (response.file_type === 'image') {
            html = '<img src="' + response.file_url + '" class="img-message" alt="' + response.file_name + '" title="' + response.file_name + '">'
        } else {
            html = '<a href="' + response.file_url + '" target="_blank" class="file-message"><span>File</span><span class="file-name">' + response.file_name + '</span></a>'
        }
        var msgBody = new Element('div', {
            'class': 'file-message',
            'html': html
        });

        var usernameWrapper = new Element('div', {
            'class': 'user-name',
            'html': response.sender
        });

        var dateWrapper = new Element('div', {
            'class': 'message-date',
            'html': response.date
        });

        var messageBodyContainer = new Element('div', {
            'class': 'message-container',
            'html': msgBody.get('html')
        });

        // avatarImg.inject(avatarWrapper);
        // avatarWrapper.inject(selfInfoContainer);
        usernameWrapper.inject(selfInfoContainer);

        selfInfoContainer.inject(selfMessageContainer);
        messageBodyContainer.inject(selfMessageContainer);

        selfMessageContainer.inject(conversationWrapper);
        dateWrapper.inject(selfMessageContainer);

        var containerScroll = $$('.single-thread .gm-scroll-view')[0];

        var containerScroller = new Fx.Scroll(containerScroll, {
            duration: 100,
            onComplete: function () {

            }
        }).toBottom();

        delete containerScroller;
    }

    var initAttachFiles = function () {
        // $$('a#attach-files-btn')[0].remove();
        var participant = $$('.action-btns').getElement('.user-name > span').get('data-participant')[0];
        if (!participant) {
            alert('Please select a conversation!');
            return;
        }
        var overlay = new Cubix.Overlay($$('.new-message-container')[0], {no_relative: true});
        var url = '/private-messaging/add-photos?participant=' + participant;
        var addPhotos = new FancyUpload3.Attach('attach-list', '#add-photos-btn', {//, #attach-file-2
            path: '/js/fancy/Swiff.Uploader.swf',
            url: url,
            data: "PHPSESSID=" + Cookie.read('PHPSESSID'),
            fileSizeMax: 2 * 1024 * 1024,
            verbose: false,
            fileListMax: 4,
            showPercent: true,
            typeFilter: {
                'Files (*.jpg, *.jpeg, *.gif, *.png, *.bmp)': '*.jpg; *.jpeg; *.gif; *.png; '
            },

            onSelectFail: function (files) {
                files.each(function (file) {
                    new Element('li', {
                        'class': 'file-invalid',
                        events: {
                            click: function () {
                                this.destroy();
                            }
                        }
                    }).adopt(
                        new Element('span', {html: file.validationErrorMessage || file.validationError})
                    ).inject(this.list, 'bottom');
                }, this);
            },

            onFileSuccess: function (file, response) {
                var json = new Hash(JSON.decode(response, true) || {});

                if (json.get('status') == '1') {
                    onFileUpload(json);
                    file.ui.element.destroy();
                } else {
                    file.ui.element.destroy();
                    new Element('li', {
                        'class': 'file-invalid',
                        events: {
                            click: function () {
                                this.destroy();
                            }
                        }
                    }).adopt(
                        new Element('span', {html: json.error})
                    ).inject(this.list, 'bottom');
                }
            },

            onFileError: function (file) {
                file.ui.cancel.set('html', 'Retry').removeEvents().addEvent('click', function () {
                    file.requeue();
                    return false;
                });

                new Element('span', {
                    html: file.errorMessage,
                    'class': 'file-error'
                }).inject(file.ui.cancel, 'after');
            },

            onFileRequeue: function (file) {
                file.ui.element.getElement('.file-error').destroy();

                file.ui.cancel.set('html', 'Cancel').removeEvents().addEvent('click', function () {
                    file.remove();
                    return false;
                });

                this.start();
            }
        });

        var addFiles = new FancyUpload3.Attach('attach-list', '#attach-files-btn', {//, #attach-file-2
            path: '/js/fancy/Swiff.Uploader.swf',
            url: url,
            data: "PHPSESSID=" + Cookie.read('PHPSESSID'),
            fileSizeMax: 2 * 1024 * 1024,
            verbose: false,
            fileListMax: 4,
            showPercent: true,
            typeFilter: {
                'Files (*.rtf, *.txt, *.pdf, *.doc, *.docx, *.rtf, *.ppt, *.pptx, *.xls, *.xlsx)': 'application/rtf; text/plain; application/pdf; application/msword;  application/vnd.openxmlformats-officedocument.wordprocessingml.document; *.rtf; application/vnd.ms-powerpoint; application/vnd.openxmlformats-officedocument.presentationml.presentation; application/vnd.ms-excel; application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            },

            onSelectFail: function (files) {
                files.each(function (file) {
                    new Element('li', {
                        'class': 'file-invalid',
                        events: {
                            click: function () {
                                this.destroy();
                            }
                        }
                    }).adopt(
                        new Element('span', {html: file.validationErrorMessage || file.validationError})
                    ).inject(this.list, 'bottom');
                }, this);
            },

            onFileSuccess: function (file, response) {
                var json = new Hash(JSON.decode(response, true) || {});
                if (json.get('status') == '1') {
                    onFileUpload(json);
                    file.ui.element.destroy();
                } else {
                    file.ui.element.destroy();
                    new Element('li', {
                        'class': 'file-invalid',
                        events: {
                            click: function () {
                                this.destroy();
                            }
                        }
                    }).adopt(
                        new Element('span', {html: json.error})
                    ).inject(this.list, 'bottom');
                }
            },

            onFileError: function (file) {
                file.ui.cancel.set('html', 'Retry').removeEvents().addEvent('click', function () {
                    file.requeue();
                    return false;
                });

                new Element('span', {
                    html: file.errorMessage,
                    'class': 'file-error'
                }).inject(file.ui.cancel, 'after');
            },

            onFileRequeue: function (file) {
                file.ui.element.getElement('.file-error').destroy();

                file.ui.cancel.set('html', 'Cancel').removeEvents().addEvent('click', function () {
                    file.remove();
                    return false;
                });

                this.start();
            }
        });
        PrivateMessaging.attacheFilesIsInited = true;
    };
    if (!this.attacheFilesIsInited) {
        initAttachFiles();
    }
};

PrivateMessaging.set_visibility_filter = function (filter) {

    $('.conversation-filter .filter-btn.active').removeClass('active');
    $('.conversation-filter .filter-btn.' + filter).addClass('active');

    $('.contact-threads').hide().fadeIn(300);

    $('.contact-threads .conversation:not([data-visible-on="' + filter + '"])').addClass('d-none');
    $('.contact-threads .conversation[data-visible-on="' + filter + '"]').removeClass('d-none');
}

PrivateMessaging.view_participant_profile = function () {
    var link = $('.single-contact-thread.active .link-to-profile').val();

    if (link) {
        window.open(link, "_blank")
    }
}

PrivateMessaging.initAddPhoto = function (container, userId) {
    var initEscortPassportPic = function () {
        var url = '/private-messaging/add-photos';
        var PassportPic = new MooUpload('add-photos-btn', {
            action: url,
            accept: 'image/*',
            method: 'auto',
            multiple: false,
            listview: false,
            maxfiles: 1,
            autostart: true,
            texts: {
                selectfile: 'Attach Photo'
            },
            onFileUpload: function (fileindex, response) {
                if (response.status == '1') {
                    var images_wrapper = container.getElements('div.verify-images-wrapper')[0];
                    var item = new Element('div', {
                        'class': 'item',
                        'id': 'otherpic-' + fileindex
                    }).inject(images_wrapper, 'top');
                    var removediv = new Element('div', {'class': 'remove-btn'}).inject(item, 'top');
                    var img = new Element('img', {'src': response.photo_url}).inject(item, 'top');
                    var hash = new Element('input', {
                        'type': 'hidden',
                        'name': 'hash',
                        'value': response.hash
                    }).inject(item, 'bottom');
                    var ext = new Element('input', {
                        'type': 'hidden',
                        'name': 'ext[' + response.hash + '][]',
                        'value': response.ext
                    }).inject(item, 'bottom');
                    var comment = new Element('textarea', {
                        'type': 'text',
                        'placeholder': 'add comment',
                        'class': 'txt',
                        'name': 'comment[' + response.hash + '][]',
                        'value': ''
                    }).inject(item, 'bottom');
                    //this.reposition();
                    $$('#filecontrol-passport-pic_btnAddfile').setStyle('background-color', '#9b9b9b');
                    $$('.remove-btn').addEvent('click', function (e) {
                        this.getParent().destroy();
                        PassportPic.filelist[fileindex - 1].checked = false;
                        $$('#filecontrol-passport-pic_btnAddfile').setStyle('background-color', '#2da3df');
                    });
                }
            },
            onLoad: function () {
                $$('#filecontrol-passport-pic_progresscont').setStyle('display', 'none');
            },
        });
    };

    initEscortPassportPic($('form-passport-pic'));
};

PrivateMessaging.block_thread = function () {
    var $active_conversation = $('.conversation.active'),
        selfEscortId = $active_conversation.attr('data-escort-id'),
        id = $active_conversation.attr('data-id'),
        participantInfo = $active_conversation.attr('data-participant');

    var participantType = participantInfo.split('-')[0],
        participantId = participantInfo.split('-')[1];

    url = lang_id + '/private-messaging/block-thread?id=' + id;
    $('.dialog').LoadingOverlay('show');

    // if (selfEscortId) {
    //     url += '&a_escort_id=' + selfEscortId;
    // }

    $.ajax({
        url: url,
        type: 'get',
        data: {
            participant_type: participantType,
            participant_id: participantId
        },
        success: function (resp) {
            var xhr = PrivateMessaging.LoadThreads();

            xhr.done(function() {
                $('.dialog').LoadingOverlay('hide');
                PrivateMessaging.ShowThread($('.conversation[data-id="' + id + '"]'), headerVars.currentUser.id);
            })
        }
    });
};

PrivateMessaging.delete_thread = function () {

    var $active_conversation = $('.conversation.active'),
        selfEscortId = $active_conversation.attr('data-escort-id'),
        id = $active_conversation.attr('data-id'),
        url = lang_id + '/private-messaging/remove-thread?id=' + id;

    // if ( typeof selfEscortId !== 'undefined' ) {
    //     url += '&escort_id=' + selfEscortId;
    // }

    // Set Default image on Dialog -> Avatar
    // ------------------------------------
    $('.dialog-top .avatar').css('background-image', 'url(/images/default-image.jpeg)');
    // ------------------------------------

    // Disable Actions
    // ------------------------------------
    $('.dialog').removeClass('show');
    // ------------------------------------

    // Remove All Messages
    // ------------------------------------
    $('.dialog-content').empty();
    // ------------------------------------

    // Remove Contact Block item from sidebar
    // ------------------------------------
    $('.conversation.active').remove();
    // ------------------------------------

    // Show no message page, if the last conversation was deleted
    // -------------------------------------
    if(!$('.conversation.single-contact-thread').length) {
        $('.empty-chat-container').addClass('d-flex');
        $('.filled-chat-wrapper').remove();
    }
    // -------------------------------------


    $.ajax({
        url: url,
        type: 'get',
        success: function (resp) {

        }
    });
};

PrivateMessaging.unblock_thread = function (id) {
    var $container = $('.conversations'),
        $active_conversation = $('.conversation.active'),
        selfEscortId = $active_conversation.attr('data-escort-id'),
        id = $active_conversation.attr('data-id'),
        url = lang_id + '/private-messaging/unblock-thread?id=' + id;
    // if ( typeof selfEscortId !== 'undefined' ) {
    //     url += '&a_escort_id=' + selfEscortId;
    // }
    $('.dialog').LoadingOverlay('show');

    $.ajax({
        url: url,
        type: 'get',
        success: function (resp) {
            var xhr = PrivateMessaging.LoadThreads();

            xhr.done(function () {
                $('.dialog').LoadingOverlay('hide');
                PrivateMessaging.ShowThread($('.conversation[data-id="' + id + '"]'), headerVars.currentUser.id);
            })
        }
    });
};

PrivateMessaging.SendComposeMessage = function (thread_container) {

    $('#message-text').focus();
    if($('#form-chat .dialog').hasClass('blocked')) {
        PrivateMessaging.is_blocked = true;
        setTimeout(function(){ 
            PrivateMessaging.is_blocked = false;
        }, 3000);
        return Notify.alert("danger", headerVars.dictionary.conversation_is_blocked);
    }

    if (PrivateMessaging.is_sending) return console.warn("Cannot sent multiple messages at the same time !!!")
    if ($('.conversation.active').length <= 0) return Notify.alert('warning', "Please select the participant to send message");

    if (headerVars.is_agency) {
        senderId = headerVars.currentUser.agency_data.agency_id
    } else if (headerVars.is_escort) {
        senderId = headerVars.currentUser.escort_data.escort_id
    } else if (headerVars.is_member) {
        senderId = headerVars.currentUser.member_data.member_id
    }

    var msgBody = $('.new-message-text').val();
    conversationWrapper = $('.single-thread-container').find('.conversation'),
        agencyEscortId = $('.conversation.active').attr('data-escort-id'),
        panel = $('.dialog-content-container');

    panel.LoadingOverlay('show');
    PrivateMessaging.is_sending = true;

    $.ajax({
        url: lang_id + '/private-messaging/send-message-ajax',
        type: 'POST',
        data: {
            message: msgBody,
            participant: $('.conversation.active').attr('data-participant'),
            a_escort_id: (agencyEscortId ? parseInt(agencyEscortId) : '')
        },
        dataType: "JSON",
        success: function (resp) {
            if (resp.status == 'success') {

                var date = new Date();
                var dateStr = date.getFullYear() + '-' + (("0" + (date.getMonth() + 1)).slice(-2)) + '-' + (("0" + (date.getDate())).slice(-2)) + ' | ' + date.getUTCHours() +  ':' + date.getUTCMinutes();

                $('.new-message-text').removeClass('alert-danger');
                var message_template = '' +
                    '<div class="message-right"> ' +
                    '<div class="message"> ' +
                    msgBody +
                    '<span class="message-date"><br>'+dateStr+'</span> ' +
                    '</div> ' +
                    '</div>';

                var $active_conversation = $('.contact-threads .conversation.active');

                // Edir 1929
                // -------------------------------------------
                // $active_conversation.attr('data-visible-on', 'sent')
                //     .find('.conversation-message').text(msgBody);
                // -------------------------------------------

                // if ($('.filter-btn.inbox').hasClass('active')) {
                //     $active_conversation.addClass('d-none')
                // }

                thread_container.append(message_template);
                thread_container[0].scrollTop = thread_container[0].scrollHeight;

                PrivateMessaging.scrollMessagesToEnd();

                $('.new-message-text').val('');
            } else {
                // $('.new-message-text').addClass('alert-danger');
                if (resp['msgs']) {
                    Notify.alert('danger', resp['msgs'])
                }
            }

        },
        error: function () {
            panel.LoadingOverlay('show');
            PrivateMessaging.is_sending = false;
        }

    }).done(function () {
        panel.LoadingOverlay('hide');
        PrivateMessaging.is_sending = false;
    });

    return false;
};

PrivateMessaging.RemoveThread = function (id, selfEscortId) {
    var contactThreads = $$('.contact-threads')[0],
        contactThreadsOverlay = new Cubix.Overlay(contactThreads, {loader: '/img/loader-pm.gif'});

    contactThreadsOverlay.disable();

    url = lang_id + '/private-messaging/remove-thread?id=' + id

    if (typeof selfEscortId !== 'undefined') {
        url += '&escort_id=' + selfEscortId;
    }

    new Request({
        url: url,
        method: 'get',
        onSuccess: function (resp) {
            contactThreadsOverlay.enable();
            PrivateMessaging.LoadThreads();
        }
    }).send();
};


PrivateMessaging.RemoveContact = function (id) {
    var url = lang_id + '/private-messaging/remove-contact?id=' + id;

    new Request({
        url: url,
        method: 'get',
        onSuccess: function (resp) {
            PrivateMessaging.LoadContacts();
        }
    }).send();
};

PrivateMessaging.AddContact = function (id) {
    var url = lang_id + '/private-messaging/add-contact?id=' + id;

    new Request.JSON({
        url: url,
        method: 'get',
        onSuccess: function (resp) {
            PrivateMessaging.LoadContacts();
            if (!resp.success) {
                return alert(resp.msg);
            }

        }
    }).send();
};

PrivateMessaging.updateUnreadThreadsCount = function (count) {
    if ($('.header-menu .menu-icon.message').length) {

        if (count == 0) {
            $('.header-menu .menu-icon.message .notice-bubble').remove();
            return;
        }

        if (!$('.header-menu .menu-icon.message .notice-bubble').length) {
            $('.header-menu .menu-icon.message').append('<span class="notice-bubble"></span>')
        }

        if (count > 9) {
            $('.header-menu .menu-icon.message .notice-bubble').html('9+');
        } else {
            $('.header-menu .menu-icon.message .notice-bubble').html(count);
        }

    } else {
        console.warn("Couldn't find place to write unread messages count");
    }

    return;
};