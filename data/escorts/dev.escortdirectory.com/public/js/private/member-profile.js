var MemberProfile = function () {

    var templates = {
        loading: function () {
            return "Loading ...";
        }
    };

    var files_in_process = {
        profile: false
    };

    function init() {
        bind_events();
        init_multiselect();
    }

    function submit_profile_form() {
        $('form.ajax-form').trigger('submit');
    }

    function reset_member_profile() {
        var url = location.hash.replace(/^#/, '');
        if (url != '') {
            APPLICATION.setup_url(url);
        }
    }

    function bind_events() {
        $('#country').on('change', on_country_changed);
        $('.image-trigger').on('click', trigger_file);
        $('#member-profile-pic-file').on('change', trigger_form);
        $('.member-photo-upload-form').on('submit', init_upload);
        $('.abort-uploading').on('click', abort_uploading_process);
        $('.close').on('click', hide_progress_bar);
        $('button.submit').on('click', submit_profile_form);
        $('.discard').on('click', reset_member_profile);
    }

    function trigger_file(e) {
        e.preventDefault();
        var $btn = $(this),
            $target = $($btn.data('target'));

        $target.trigger('click');
    }

    function trigger_form() {
        $('.member-photo-upload-form').trigger('submit');
    }

    function construct_dropdown(params) {
        try {
            var regions = JSON.parse(params.regions);
            var visible_html = ``;
            var data_html = ``;

            for (var title in regions) {

                var data_html_chunk = ``;
                var visible_html_chunk = ``;

                regions[title].forEach(function (city) {
                    data_html_chunk += `<option value="${city.id}">${city.title}</option>`;
                    visible_html_chunk += `<li data-val="${city.id}"><a href="javascript:void(0)">${city.title}</a></li>`;
                });

                if (title) {
                    data_html += `<optgroup label="${title}" >${data_html_chunk}</optgroup>`;
                    visible_html += `<ul class="sub-dropdown-menu"> <li class="mb-2 mt-2"> <b> ${title} </b> </li> ${visible_html_chunk}</ul>`;
                } else {
                    data_html += data_html_chunk;
                    visible_html += visible_html_chunk;
                }
            }

            params.select.html(data_html).val(null); // This is only <option> s
            params.ul.html(visible_html); // This one is for ul>li that is visible for user

        } catch (e) {
            console.warn(e.message)
        }
    }

    function on_country_changed() {

        var $country_filter = $('#countryFilter'),
            $country_select = $("#country"),
            $cities_select = $("#cities");

        $cities_select.empty();
        $cities_select.siblings('ul[data-type="citiesFilter"]').empty().append(templates.loading());

        $.ajax({
            url: '/geography/ajax-get-cities-grouped?json=true',
            data: {
                country_id: $country_select.val()
            },
            success: function (r) {

                construct_dropdown({
                    regions: r,
                    select: $cities_select,
                    ul: $cities_select.siblings('ul[data-type="citiesFilter"]'),
                })

            }
        });
    }

    function init_upload(e) {
        e.preventDefault();
        var $form = $(this),
            file_type = $form.data('type'),
            $progressBarWrapper = $('.' + file_type + '-pics-progress-bar'),
            $progressBar = $progressBarWrapper.find('.progress-bar'),
            $profileImg = $('#' + file_type + '-pic'),
            $errorBar = $progressBarWrapper.find('.process-status .text');

        if (files_in_process[file_type]) {
            return alert("File is uploading, please wait ...");
        }

        $progressBarWrapper.addClass('uploading').removeClass('stop');
        files_in_process[file_type] = true;
        $errorBar.html('');
        $progressBar.css('width', 0 + '%');
        $progressBarWrapper.show();

        var formdata = new FormData(this),
            $file = formdata.get('file'),
            $fileId = (new Date()).getTime().toString(36),
            total = 0;

        var upload_chunk = function ($file, $fileId, $start) {

            // Force Stop Recursion
            if (files_in_process[file_type] == 'aborted') {
                return files_in_process[file_type] = null;
            }

            var chunk,
                $chunkSize = 524288, // 0.5 MB
                total = $start + $chunkSize;

            if (total >= $file.size) {
                total = $file.size
            }

            if ($file.mozSlice) {
                // Mozilla based
                chunk = $file.mozSlice($start, total);
            } else if ($file.webkitSlice) {
                // Chrome, Safari, Konqueror and webkit based
                chunk = $file.webkitSlice($start, total);
            } else {
                // Opera and other standards browsers
                chunk = $file.slice($start, total);
            }

            files_in_process[file_type] = $.ajax({
                type: "POST",
                url: $form.attr('action'),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-File-Id', $fileId);
                    xhr.setRequestHeader('X-File-Name', $file.name);
                    xhr.setRequestHeader('X-File-Size', $file.size);
                    xhr.setRequestHeader('X-File-Resume', 1);
                },
                contentType: "application/json",
                data: chunk,
                dataType: ' json',
                processData: false,
                success: function (response) {

                    var $newStartpos = 100 * total / $file.size;
                    $progressBar.css('width', $newStartpos + '%');

                    if (!response.finish) {

                        if (!response.error) {

                            upload_chunk($file, $fileId, total)
                        } else {
                            files_in_process[file_type] = null;

                            $errorBar.html(response.error)
                                .parent()
                                .removeClass('text-success')
                                .addClass('text-danger');
                            $progressBarWrapper.addClass('stop').removeClass('uploading');
                        }
                    } else {
                        files_in_process[file_type] = null;

                        if (response.error) {
                            $errorBar.html(response.error)
                                .parent()
                                .removeClass('text-success')
                                .addClass('text-danger');
                            $progressBarWrapper.addClass('stop').removeClass('uploading');

                        } else {
                            files_in_process[file_type] = false;
                            $progressBarWrapper.addClass('stop').removeClass('uploading');
                            $progressBar.css('width', '0%');

                            $errorBar.html(headerVars.dictionary.photo_upload_success)
                                .parent()
                                .removeClass('text-danger')
                                .addClass('text-success');

                            $profileImg.css('background-image', 'url(' + response['photo_url'] + ')');
                        }
                    }
                }
            })

        };
        upload_chunk($file, $fileId, 0);
    }

    function abort_uploading_process() {
        var $btn = $(this),
            target = $btn.attr('data-abort-target'),
            $form = $('form[data-type="' + target + '"]'),
            $progressBarWrapper = $('.' + target + '-pics-progress-bar');

        $progressBarWrapper.hide();
        files_in_process[target] = 'aborted';

        if ($form.length) {
            $form.get(0).reset();
        }
    }

    function hide_progress_bar() {
        $(this).parents('.progress-wrapper').hide();
        var target = $(this).attr('data-file-type'),
            $form = $('form[data-type="' + target + '"]');

        files_in_process[target] = null;

        if ($form.length) {
            $form.get(0).reset();
        }
    }

    function init_multiselect() {

        $('.multiple-dropdown-box').each(function(){
            var labels = [];
            $(this).multiselect({
                onChange: function (option, checked, select) {
                    var index = labels.indexOf($(option).val());
                    if (index !== -1) {
                        labels.splice(index, 1);
                    } else {
                        labels.push($(option).val());
                    }
                },
                buttonText: function(options) {
                    var labels = [];
                    options.each(function () {
                        labels.push($(this).text())
                    });

                    if (options.length > 1) {
                        return labels[0] + ' +' + options.length;
                    } else {
                        return headerVars.dictionary.nothing_selected;
                    }
                }
            });
        })

    }

    return {
        init: init
    }
}();

$(document).ready(function () {
    MemberProfile.init();
})