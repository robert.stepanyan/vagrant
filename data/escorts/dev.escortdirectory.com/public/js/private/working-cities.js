var Working_Cities_page = (function () {

    function Working_Cities_page() { };

    Working_Cities_page.prototype.templates = {
        other_city_item: function (params) {
            return  '<div class="other-city justify-content-between">'+
                    '<div class="city-name">' +params.title+'<span class="text-grey"> ( ' + params.country_title +' )</span></div>'+
                    '<div class="city-remove text-grey d-flex align-items-center">'+
                        '<p class="uppercase">Remove</p>'+
                        '<i class="fa fa-times"></i>'+
                        '<input value="'+params.id+'" name="cities[]" type="hidden">'+
                    '</div>'+
                '</div>';
        },
        loading: function () {
            return headerVars.dictionary.loading + " ...";
        }
    }

    Working_Cities_page.prototype.init_multiselect = function() {
        
        $('.multiple-dropdown-box').multiselect({

            onChange: function (option, checked, select) {
                select = $(this.$select);
                var list = $(this.$ul)
                var labels = [];
                list.find('input:checked').each(function() {
                    labels.push($(this).val());
                })
    
                select.val(labels);
            },
            buttonText: function(options) {
                var labels = [];
                options.each(function () {
                    labels.push($(this).text())
                });

                if (options.length > 3) {
                    return labels[0] + ',' + labels[1] + ' +' + options.length;
                } else if (options.length >= 1) {
                    return labels.join(',');
                } else {
                    return headerVars.dictionary.nothing_selected;
                }
            }
        });
    }

    Working_Cities_page.prototype.event_handlers = {
        add_city: function () {

            var city = {
                title: $("#otherCitiesFilter").find(".dropdown-toggle-val").html(),
                country_title: $("#countryFilter").find(".dropdown-toggle-val").html(),
                id: $("#cities").val(),
            };

            // Validation
            if ($("#otherCitiesFilter").find(".dropdown-toggle-val").hasClass('pristine')) return console.info('No City selected ');
            if ($("#countryFilter").find(".dropdown-toggle-val").hasClass('pristine')) return console.info('No Country selected ');
            if ($('input[name="cities[]"][value="' + city.id + '"]').length > 0) return console.info('Already added');
            //

            // If you change this number, dont forget
            // to make changes in js-init.php too (for translation)
            // -------------------------------------------------
            if ($('input[name="cities[]"]').length >= 4) {
                return Notify.alert('danger', headerVars.dictionary.maxOtherCities);
            }
            // -------------------------------------------------

            var container = this.templates.other_city_item(city);
            $(".other-cities-container").append(container);
        },

        remove_city: function () {
            $(this).parents(".other-city").slideUp('slow', function () {
                $(this).remove();
            });
        },

        toggle_callings: function () {
            var self = $(this);

            $(this).parents('.calling_section').first().find('.dropdown, input:not([type="checkbox"]), select, textarea').each(function () {
                if($(this).parents('.dropdown-selection').hasClass('independent')) return;

                if (self.is(':checked'))
                    $(this).removeAttr('disabled')
                else
                    $(this).attr('disabled', true)
            })
        },

        country_changed: function () {

            var id = $('#country').val();
            this.helpers.ajaxBaseCities.call(this, id);
            this.helpers.ajaxCities.call(this, id);
            // ajaxCitiesZones(container, uni_els);
        },

        base_city_changed: function () {
            var id = $('#country').val();
            this.helpers.ajaxCities.call(this, id);
            // ajaxCitiesZones(id);
        },

        incall_type_changed: function() {
            if($(this).val() != 2){
                $('#starsFilter').parents('.dropdown').first().attr('disabled', true)
            }else{
                $('#starsFilter').parents('.dropdown').first().removeAttr('disabled')
            }
        }
    }

    Working_Cities_page.prototype.helpers = {

        construct_dropdown: function (params){

            var cities_count = 0;
            try {
                var regions = JSON.parse(params.regions);
                var visible_html = "";
                var data_html = "";

                for (var title in regions) {

                    var data_html_chunk = "";
                    var visible_html_chunk = "";

                    regions[title].forEach(function (city) {
                        cities_count++;
                        data_html_chunk += "<option value=\"" + city.id + "\">" + city.title + "</option>";
                        visible_html_chunk += "<li data-val=\"" + city.id + "\"><a href=\"javascript:void(0)\">" + city.title + "</a></li>";
                    });

                    if (title) {
                        data_html += "<optgroup label=\"" + title + "\" >" + data_html_chunk + "</optgroup>";
                        visible_html += "<ul class=\"sub-dropdown-menu\"> <li class=\"mb-2 mt-2\"> <b> " + title + " </b> </li> " + visible_html_chunk + "</ul>";
                    } else {
                        data_html += data_html_chunk;
                        visible_html += visible_html_chunk;
                    }
                }

                params.select.html(data_html).val(null); // This is only <option> s
                params.ul.html(visible_html); // This one is for ul>li that is visible for user
            } catch (e) {
                console.warn(e.message);
            }

            return {
                cities: cities_count
            };
        },
        ajaxBaseCities: function (country_id) {
            var self = this;

            $('#baseCityFilter').siblings('ul[data-type="baseCityFilter"]').empty().append(this.templates.loading());

            $('#base-city').empty();
            $('#baseCityFilter').find('.dropdown-toggle-val').addClass('pristine').html(headerVars.dictionary.nothing_selected_html)

            $('#cities').empty();
            $('#otherCitiesFilter').find('.dropdown-toggle-val').addClass('pristine').html(headerVars.dictionary.nothing_selected_html)
            $('#baseCityFilter').siblings('ul[data-type="otherCitiesFilter"]').empty().append(this.templates.loading());

            $.ajax({
                url: '/geography/ajax-get-cities-grouped?json=true',
                data: {
                    country_id: country_id
                },
                success: function (r) {

                    var result = self.helpers.construct_dropdown({
                        regions: r,
                        select: $('#base-city'),
                        ul: $('#baseCityFilter').siblings('ul[data-type="baseCityFilter"]'),
                    });

                    if(result.cities <= 1) {
                        $('#otherCitiesFilter').parent().attr('disabled', true);
                    }else{
                        $('#otherCitiesFilter').parent().removeAttr('disabled');
                    }
                }
            });
        },
        ajaxCities: function (id) {
            var self = this;
            // Clear other cities dropdown
            $('#cities').empty().val(null);
            $('#otherCitiesFilter').find('.dropdown-toggle-val').addClass('pristine').html(headerVars.dictionary.nothing_selected_html)
            $('#otherCitiesFilter').siblings('ul[data-type="otherCitiesFilter"]').empty().append(this.templates.loading());

            // Clear added other cities
            $('.other-cities-container').empty();
            $('#otherCitiesFilter').parents('.dropdown').first().removeAttr('disabled');
            $('.add-other-city-trigger').removeClass('disabled').removeAttr('disabled');

            $.ajax({
                url: '/geography/ajax-get-cities-grouped?json=1',
                data: {
                    country_id: id
                },
                success: function (r) {
                    var res = self.helpers.construct_dropdown({
                        regions: r,
                        select: $('#cities'),
                        ul: $('#otherCitiesFilter').siblings('ul[data-type="otherCitiesFilter"]')
                    });

                    if(res.cities <= 1) {
                        $('#otherCitiesFilter').parents('.dropdown').first().attr('disabled', true);
                        $('.add-other-city-trigger').addClass('disabled').attr('disabled', true);
                    }

                    // Let's hide duplicate option in other cities and base city
                    $('#otherCitiesFilter').siblings('ul[data-type="otherCitiesFilter"]').find('li').each(function () {
                        if ($(this).data('val') == $('#base-city').val())
                            $(this).hide();
                    })
                }
            })
        }
    }

    Working_Cities_page.prototype.bind_events = function () {

        if (!window.WORKING_CITIES_PAGE) {
            $(document)
                .on("click", "#add-city", this.event_handlers.add_city.bind(this))
                .on("click", ".city-remove", this.event_handlers.remove_city)
                .on("change", "#incall_check, #outcall_check", this.event_handlers.toggle_callings)
                .on('change', '#country', this.event_handlers.country_changed.bind(this))
                .on('change', '#base-city', this.event_handlers.base_city_changed.bind(this))
                .on('change', 'select[name=incall_type]', this.event_handlers.incall_type_changed)

        }

    }

    Working_Cities_page.prototype.init = function () {

        try {
            this.bind_events();
            this.init_multiselect();
        }catch(e) {
            console.error("Whoops there was an error", e);
		}
        return this;
    }

    return Working_Cities_page;
})();




$(function () {
    window.WORKING_CITIES_PAGE = (new Working_Cities_page()).init();
});