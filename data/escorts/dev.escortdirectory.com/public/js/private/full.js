var FullEditPage = function () {

    var params = {};
    var xhrs = {};

    function init(obj) {
        set_active_ancor(obj.section);
        if (params.escort == obj.escort) return;
        params = obj;

        abort_previous_requests();
        show_fake_cards();
        get_accordion_contents();
        bind_events();

        $(window).on('hashchange', function() {
            $('.card-content.fake').removeClass('page-placeholder-loading');
            abort_previous_requests();
        });
    }

    function show_fake_cards() {
        $('.card-content.real').hide().empty();
        $('.card-content.fake').show();
    }

    function abort_previous_requests() {
        try {
            for (var key in xhrs) {
                xhrs[key].abort();
            }
        } catch (e) {
            console.warn(e)
        }
    }

    function get_accordion_contents() {

        var steps = [
            {'page': 'biography'},
            {'page': 'gallery', 'cb': GALLERY_PAGE.init.bind(GALLERY_PAGE)},
            {'page': 'about-me', 'cb': APPLICATION.update_tinymce_editors.bind(APPLICATION)},
            {'page': 'languages'},
            {'page': 'working-cities'},
            {'page': 'services', 'cb': APPLICATION.update_tinymce_editors.bind(APPLICATION)},
            {'page': 'working-times'},
            {'page': 'prices'},
            {'page': 'contact-info', 'cb': init_multiselect},
        ];

        // var params = _get_URL_params();

        if (!params['escort']) return alert("Escort was not specified, please go to manage page and select Escort you want to edit.");

        set_active_ancor();

        steps.forEach(function (step) {
            xhrs[step.page] = $.ajax({
                url: lang_id + '/private/profile/full?escort=' + params.escort,
                type: "POST",
                data: {page: step.page, escort: params.escort},
                dataType: "JSON",
                success: function (resp) {
                    delete xhrs[step];
                    if (!resp['html'].length) {
                        $('[data-target="#collapse-' + resp['page'] + '"]').parents('.card').remove();
                        return $('#collapse-' + resp['page']).remove();
                    }

                    $('#collapse-' + resp['page'] + ' .card-content.real').html(resp['html']).show();
                    $('#collapse-' + resp['page'] + ' .card-content.fake').hide();
                    if (step.cb) step.cb();
                }
            });
        });
    }

    function _get_URL_params() {
        var params = {};
        var params_arr = window.location.hash.split('?');
        window.location.hash.split('?').forEach(function (item) {
            params[item.split('=')[0]] = item.split('=')[1]
        });

        return params;
    }

    function handle_section_scroll(e) {
        e.preventDefault();
        var target = $(this).attr('target-section');
        var params = _get_URL_params();

        window.location.hash = '#' + target + (params['escort'] ? '?escort=' + params['escort'] : '');
        set_active_ancor();
    }

    function keep_loadbar_into_view(keep) {

        var focus = function () {
            var ov = $('.loadingOverlay');
            var y = $(document).scrollTop();
            ov.css('background-position-y', (y + $(window).outerHeight() / 5) + "px")
        };

        if (keep) {
            $(window).on('scroll', focus).trigger('scroll');
        } else {
            $(window).off('scroll');
        }
    }

    function submit_all_forms(e) {
        e.preventDefault();
        var $container = $('#ui-view');
        var data = collect_data_from_every_form();
        data['page'] = 'all';
        $container.LoadingOverlay('show');
        /*
        for (var langid  in headerVars.langs) {
           data['about_'+langid] = tinyMCE.get('about-' + langid).getContent();
           data['additional_service_'+langid] = tinyMCE.get('additional-service-' + langid).getContent();
        }
        */

        $.ajax({
            url: lang_id + '/private/profile/full',
            method: 'post',
            data: data,
            dataType: 'json',
            success: function (resp) {
                $container.LoadingOverlay('hide');

                if (resp.success) {

                    $('.is-invalid').removeClass('is-invalid');
                    $('[id^=validation-]').html('');
                    $('#errors-container').hide();

                    Notify.alert('success', headerVars.dictionary.changes_saved);
                    return EscortProfile.ShowFull('/private/profile/full?escort=' + resp.escort_id);

                } else {
                    init_errors(resp.errors);
                }
            }
        });
    }

    function init_errors(errors) {
        var did_animation = false;
        $('#errors-container').show();
        $('[id^=validation-]')
            .html('')
            .parents('.visible-on-error')
            .hide();

        $.each(errors, function (key, value) {
            $("[data-error='" + key + "']").addClass('is-invalid');
            $('#validation-' + key)
                .html(value)
                .parents('.visible-on-error')
                .show();

            if (!did_animation && $('#vlaidation-' + key).length) {
                $('html, body').animate({
                    scrollTop: $('#validation-' + key).offset().top - 100
                }, 1000);

                did_animation = true;
            }
        });
    }

    function collect_data_from_every_form() {
        var $forms = $('.edit-full-page-content form');

        var data = $forms.serializeArray().reduce(function (obj, item) {
            if (item.name.includes('[')) {
                if (obj[item.name] && $.isArray(obj[item.name]))
                    obj[item.name].push(item.value);
                else
                    obj[item.name] = [item.value];
            } else {
                obj[item.name] = item.value;
            }
            return obj;
        }, {});

        return data;
    }

    function discard_changes(e) {
        e.preventDefault();
        $('#discard-full').trigger('click')
    }

    function bind_events() {
        $('.submit-all').on('click', submit_all_forms);
        $('.edit-full-page-content').on('click', 'form a.discard', discard_changes);
    }

    function set_active_ancor(section = null) {
        if(!section) section = params['section'];

        var $elms = $('.card-header');
        var anchor_activated = false;
        $elms.each(function () {
            if ($(this).attr('id').includes(section)) {
                if ($(this).attr('aria-expanded') != 'true') {
                    $(this).trigger('click');
                }
                var active_tab = $(this);
                var interval = setInterval(function () {
                    var y = active_tab.offset().top;
                    if (y > 0) {
                        clearInterval(interval);
                        $('html, body').animate({scrollTop: y}, 500);
                    }
                }, 100);
                anchor_activated = true;
            }

        });

        if (!anchor_activated)
            $elms.first().trigger('click')

    }

    function init_multiselect() {
        $('.multiple-dropdown-box').multiselect({

            onChange: function (option, checked, select) {
                select = $(this.$select);
                var list = $(this.$ul)
                var labels = [];
                list.find('input:checked').each(function () {
                    labels.push($(this).val());
                })

                select.val(labels);
            },
            buttonText: function(options) {
                var labels = [];
                options.each(function () {
                    labels.push($(this).text())
                });

                if (options.length > 3) {
                    return labels[0] + ',' + labels[1] + ' +' + options.length;
                } else if (options.length >= 1) {
                    return labels.join(',');
                } else {
                    return headerVars.dictionary.nothing_selected;
                }
            }
        });
    }


    return {
        init: init
    }
}();

$(function () {
    //FullEditPage.init();
});