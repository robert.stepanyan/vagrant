var Settings = {

    init: function () {
        this.initForm();
        this.initCountries();
        this.initNewsletters();
        this.initProfileStatus();
    },

    change_password: function () {

        event.preventDefault();
        var $form = $('#password-change-form'),
            url = lang_id + '/private/change-password?ajax=1',
            $container = $('#ui-view'),
            data = {},
            fields = $form.serializeArray();

        fields.forEach(function (item) {
            data[item.name] = item.value;
        });

        $container.LoadingOverlay("show", {
            color: "rgba(239, 243, 249, 0.80)",
            zIndex: 1000
        });

        $.ajax({
            type: "POST",
            data: data,
            url: url,
            dataType: "JSON",
            success: function (response) {

                var did_animation = false;
                $container.LoadingOverlay("hide");


                $('[id^=validation-]')
                    .html('')
                $('.is-invalid').removeClass('is-invalid');

                if (response.status == "success") {
                    $('#errors-container').hide();
                    Notify.alert('success', headerVars.dictionary.changes_saved);
                    $form[0].reset()
                } else {

                    $.each(response['msgs'], function (key, value) {

                        $('#validation-' + key)
                            .html(value)
                        $('[name=' + key + ']').addClass('is-invalid');

                        if (!did_animation && $('#vlaidation-' + key).length) {
                            $('html, body').animate({
                                scrollTop: $('#validation-' + key).offset().top - 100
                            }, 1000);

                            did_animation = true;
                        }
                    });

                }

                $container.LoadingOverlay('hide');
            }
        })
    },

    initProfileStatus: function () {

        $('#profile-status').on('click', function () {

            $(".profile-status").LoadingOverlay("show", {
                color: "rgba(239, 243, 249, 0.80)",
                zIndex: 1000
            });
            var action = $('#profile-status').attr('value');

            $.ajax({
                type: "GET",
                url: "private/profile-status?act=" + action,
                success: function (response) {
                    if (response) {
                        if (action == 'enable') {
                            $('#profile-status').val("disable");
                            $('.status-profile').text("active");
                            $('.active-block').find('.alert-warning').removeClass('alert-warning').addClass('alert-success');
                        } else {
                            $('#profile-status').val("enable");
                            $('.status-profile').text("owner disabled");
                            $('.active-block').find('.alert-success').removeClass('alert-success').addClass('alert-warning');
                        }
                    }
                },

                complete: function (e) {
                    $(".profile-status").LoadingOverlay("hide", true);
                }
            })
        })
    },

    initCountries: function () {
        $('select#country,select#city').select2({
            theme: 'bootstrap'
        });

        $("#country").change(function () {
            var country_id = $(this).val();

            $.ajax({
                url: '/geography/ajax-get-cities',
                type: 'POST',
                data: {
                    country_id: country_id
                },
                dataType: 'json',
                beforeSend: function () {
                    $('#profile-settings').LoadingOverlay("show", true);
                },
                success: function (data) {

                    var city_area = $('#city');

                    city_area.find('option').remove();

                    $.each(data['data'], function (key, city) {
                        $('<option/>', {
                            value: city.id,
                            html: city.title
                        }).appendTo(city_area);
                    });

                    $('#profile-settings').LoadingOverlay("hide", true);
                }
            });
        });
    },


    initForm: function () {

        $('#settings-form').submit(function (event) {
            event.preventDefault();

            var $container = $('#ui-view');
                $pm_btn = $('#privateMessage'),
                $review_btn = $('#allowReview'),
                $comments_btn = $('#allowComments'),
                $news_btn = $('#allowNewsletter');


            var data = {
                enable_pm: $pm_btn.is(":checked") ? $pm_btn.data('yes') : $pm_btn.data('no') ,
                allow_review: $review_btn.is(":checked") ? $review_btn.data('yes') : $review_btn.data('no') ,
                allow_comment: $comments_btn.is(":checked") ? $comments_btn.data('yes') : $comments_btn.data('no') ,
                newsletters: $news_btn.is(":checked") ? $news_btn.data('yes') : $news_btn.data('no') ,
                prefered_lang: $('#prefered_lang').val(),
            };

            // Only For Member-Users
            if($('#watch_types').length) data.escorts_watched_type = $('#watch_types').val();

            $.ajax({
                url: '/private/settings',
                type: 'POST',
                data: data,
                beforeSend: function () {
                    $container.LoadingOverlay("show", true);
                },
                success: function (data) {
                    $container.LoadingOverlay("hide", true);
                }
            });
        });

    },

    initNewsletters: function () {
        // $('input[name="newsletters"]').change(function () {
        //     $(this).closest('form').submit();
        // });
    }

};


$(document).ready(function () {
    Settings.init();
});