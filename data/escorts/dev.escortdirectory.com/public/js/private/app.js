var Application = (function () {

    function Application(params) {
        this.ajaxLoad = params.ajaxLoad;
        this.page404 = params.page404;
        this.mainContent = params.mainContent;
        this.isRunning = params.isRunning;
        this.urls_with_callback = params.urls_with_callback;
        this.requires_loader = params.requires_loader;
        this.navigation = params.navigation;
        this.fullscreen_pages = params.fullscreen_pages;
        this.rewrite_pages = params.rewrite_pages;
        this.request_prefixes = params.request_prefixes;
    }

    Application.prototype.load_css = function (styles) {
        $('.inner-css-file').remove();
        for (var i = 0; i < styles.length; i++) {
            var style = document.createElement('link');
            style.type = 'text/css';
            style.href = "/" + styles[i] + '?v='+Math.random();
            style.rel = 'stylesheet';
            style.className = 'inner-css-file';
            document.head.appendChild(style);
        }
    }

    Application.prototype.load_js = function (jsFiles) {
        $('.inner-js-file').remove();
        for (var i = 0; i < jsFiles.length; i++) {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.async = false;
            script.src = "/" + jsFiles[i] +'?v='+Math.random();
            script.className = 'inner-js-file';
            document.body.appendChild(script);
        }
    }

    Application.prototype.user_notification = function () {
        $.ajax({
            method: 'POST',
            url: 'private/user-notifications',
            dataType: 'html',
            data: {
                ajax: true
            },
            cache: true,

            success: function (responseText) {
                $('.user-notifications').html('');
                $('.user-notifications').append(responseText);
            },

            error: function () {

            },

        });
    }


    Application.prototype.inner_styles_routes = function (url) {
        var prime_url = url;
        if (~url.indexOf('tours')) {
            url = 'tours';
        } else if (~url.indexOf('profile') || ~this.request_prefixes['private/profile'].indexOf(url)) {
            url = 'profile';
        } else if (~url.indexOf('black-list') || ~url.indexOf('admin-warning')) {
            url = 'blacklist';
        } else if (~url.indexOf('upgrade-now')) {
            url = 'upgrade-now';
        } else if (~url.indexOf('messages')) {
            url = 'messages';
        } else if (~url.indexOf('manage-models')) {
            url = 'manage-models';
        }
        var styles = [];

        // Specially For FULL PAGE EDIT
        // -----------------------------------------------
        // var is_profile_page = this.request_prefixes['private/profile'].includes(prime_url.replace(/^\/+|\/+$/g, '').split('?')[0]);
        // if(typeof headerVars.currentUser != 'undefined' && headerVars.currentUser.user_type == 'agency' && is_profile_page) {
        //     url = 'edit-full';
        // }
        // -----------------------------------------------
        switch (url) {

            case 'tours':
                var styles = [
                    "css/private/responsive.bootstrap4.min.css",
                    'css/private/bootstrap-datepicker.css',
                ];
                break;
            case 'add-new':
                var styles = [
                    'css/bootstrap-multiselect.css',
                ];
            case 'edit-full':
                jsFiles = [
                    'css/bootstrap-multiselect.css',
                ];
            case 'manage-models':
                jsFiles = [
                    'css/bootstrap-multiselect.css',
                ];
            case 'profile':
                var styles = [
                    'lib/fancybox/jquery.fancybox.min.css',
                    'lib/datepair/jquery.timepicker.css',
                    'css/bootstrap-multiselect.css',
                ];
                break;
            case 'upgrade-now':
                var styles = [
                    "css/private/self-checkout.css",
                    "css/private/pikaday.css",
                ];
                break;
            case 'blacklist':
                var styles = [
                    'css/private/bootstrap-datepicker.css',
                ];
                break;
            case 'support':
                var styles = [
                    'css/private/lightbox.css'
                ];
                break;

        }

        // Second Segment
        if (~prime_url.indexOf('profile/biography')) {
            prime_url = 'biography';
        } else if (~prime_url.indexOf('about-me')) {
            prime_url = 'about-me';
        } else if (~prime_url.indexOf('languages')) {
            prime_url = 'languages';
        } else if (~prime_url.indexOf('manage-models')) {
            prime_url = 'agency-escorts';
        } else if (~prime_url.indexOf('agency-profile')) {
            prime_url = 'agency-profile';
        } else if (~prime_url.indexOf('verify') || ~prime_url.indexOf('verification')) {
            prime_url = 'verify';
        } else if (~prime_url.indexOf('my-profile') || ~prime_url.indexOf('I-follow') || ~prime_url.indexOf('city-alerts')) {
            prime_url = 'member';
        }

        switch (prime_url) {
            case 'biography':
                styles.push("css/private/biography.css");
                break;
            case 'agency-escorts':
                styles.push('css/private/user_area-agencie.css');
                break;
            case 'agency-profile':
                styles.push('css/private/user_area-agencie.css');
                break;
            case 'verify':
                styles.push('css/private/user_area-agencie.css');
                break;
            case 'member':
                styles.push('css/private/user_area-member.css');
                break;
            case 'upgrade-now':
                styles.push('css/private/bootstrap-datepicker.css');
                break;
        }

        this.load_css(styles);
    }

    Application.prototype.inner_js_routes = function (url) {
        var jsFiles = [];
        var prime_url = url;

        if (~url.indexOf('100-verification')) {
            url = '100-verification';
        } else if (~url.indexOf('my-profile')) {
            url = 'member-profile';
        } else if (~url.indexOf('agency-profile')) {
            url = "agency-profile";
        } else if (~url.indexOf('profile') || ~url.indexOf('add-new') || ~this.request_prefixes['private/profile'].indexOf(url)) {
            url = 'profile';
        } else if (~url.indexOf('tours')) {
            url = 'tours';
        } else if (~url.indexOf('my-reviews')) {
            url = 'reviews';
        } else if (~url.indexOf('black-list') || ~url.indexOf('admin-warning')) {
            url = 'blacklist';
        } else if (~url.indexOf('upgrade-now')) {
            url = 'upgrade-now';
        } else if (~url.indexOf('settings')) {
            url = 'settings';
        } else if (~url.indexOf('my-followers')) {
            url = 'followers'
        } else if (~url.indexOf('messages')) {
            url = "messages"
        } else if (~url.indexOf('manage-models')) {
            url = "agency-escorts";
        } else if (~url.indexOf('city-alerts')) {
            url = "city-alerts"
        } else if (~url.indexOf('I-follow')) {
            url = "member-follow"
        } else if (url.length <= 1) {
            url = "index"
        }
        switch (url) {
            case 'index':
                jsFiles = [
                    'js/private/dashboard.js'
                ];
                break;
            case 'agency-profile':
                jsFiles = [
                    'lib/datepair/datepair.js',
                    'lib/datepair/jquery.datepair.js',
                    'lib/datepair/jquery.timepicker.js',
                    "js/private/languages.js?v1",
                    "js/private/working-times.js?v1",
                    "js/private/profile-agency.js?v1",
                ];
                break;
            case 'member-follow':
                jsFiles = [
                    "js/private/member-follow.js",
                ];
                break;
            case 'upgrade-now':
                jsFiles = [
                    "lib/datepair/lib/moment.js",
                    "lib/datepair/lib/moment-timezone-with-data-2012-2022.min.js",
                    "lib/datepair/bootstrap-datepicker.min.js",
                    "js/private/self-checkout.js",
                ];
                break;
            case 'agency-escorts':
                jsFiles = [
                    "js/private/agency-escorts.js",
                ];
                break;
            case 'tours':
                jsFiles = [
                    'lib/sortable.js',
                    'lib/datepair/bootstrap-datepicker.min.js',
                    'lib/datepair/jquery.timepicker.js',
                    'lib/datepair/datepair.js',
                    'lib/datepair/jquery.datepair.js',
                    "js/private/profile-tours.js",
                ];
                break;
            case 'followers':
                jsFiles = [
                    'js/private/my-followers.js'
                ]
                break;
            case 'reviews':
                jsFiles = [
                    "js/private/reviews.js"
                ]
                break;
            case 'support':
                jsFiles = [
                    'js/private/libs/simpleUpload.min.js',
                ];
                break;
            case 'profile':
                jsFiles = [
                    "js/private/libs/select2.full.js",
                    "js/private/libs/moment.min.js",
                    "js/private/libs/daterangepicker.min.js",
                    'js/private/blueimp/load-image.all.min.js',
                    'js/private/blueimp/jquery.ui.widget.js',
                    'js/private/blueimp/jquery.fileupload.js?v2',
                    'js/private/blueimp/jquery.fileupload-process.js',
                    'js/private/blueimp/jquery.fileupload-validate.js',
                    'lib/sortable.js',
                    'js/private/libs/simpleUpload.min.js',
                    'js/private/profile-steps.js',
                ];
                break;
            case '100-verification':
                jsFiles = [
                    'js/private/libs/simpleUpload.min.js',
                    "js/private/verification.js"
                ];
                break;
            case 'show-escorts':
                jsFiles = [
                    'js/private/agency-escorts.js'
                ];
                break;

            case 'settings':
                jsFiles = [
                    'js/private/libs/select2.full.js',
                    'js/private/settings.js'
                ];
                break;

            case 'blacklist':
                jsFiles = [
                    'lib/datepair/bootstrap-datepicker.min.js',
                    'js/private/blacklist.js'
                ];
                break;

            case 'premium':
                jsFiles = [
                    'js/private/premium-models.js'
                ];
                break;

            case 'city-alerts':
                jsFiles = [
                    'js/private/city-alerts.js'
                ];
                break;
            case 'alerts':
                jsFiles = [
                    'js/private/libs/select2.full.js',
                    'js/private/member/profile-alerts.js',
                ];
                break;
            case 'member-profile':
                jsFiles = [
                    'js/private/libs/select2.full.js',
                    'js/private/member-profile.js'
                ];
                break;
            case 'happy-hour':
                jsFiles = [
                    'js/private/happy-hour.js'
                ];
                break;

        }

        // Specially For FULL PAGE EDIT
        // -----------------------------------------------
        var is_profile_page = this.request_prefixes['private/profile'].includes(prime_url.replace(/^\/+|\/+$/g, '').split('?')[0]);
        if(typeof headerVars.currentUser != 'undefined' && headerVars.currentUser.user_type == 'agency' && is_profile_page) {
            prime_url = 'manage-models';
        }
        // -----------------------------------------------

        // Second Segment
        // ------------------------------------------------
        var additional_js = [
            {
                keyword: 'manage-models',
                files: [
                    'js/private/libs/select2.full.js',
                    'js/private/biography.js',
                    'js/private/languages.js',
                    'js/private/working-cities.js',
                    'js/private/services.js',
                    'lib/datepair/datepair.js',
                    'lib/datepair/jquery.datepair.js',
                    'lib/datepair/jquery.timepicker.js',
                    'js/private/working-times.js',
                    'js/private/prices.js',
                    "js/private/contact-info.js",
                    'lib/fancybox/jquery.fancybox.min.js',
                    'js/private/libs/simpleUpload.min.js',
                    'lib/sortable.js',
                    'js/interact.js',
                    "js/private/gallery.js",
                    'js/private/full.js'
                ]
            },
            {
                keyword: 'biography',
                files: ['js/interact.js', 'js/private/biography.js']
            },
            {
                keyword: 'languages',
                files: ['js/private/languages.js']
            },
            {
                keyword: 'working-cities',
                files: ['js/private/working-cities.js']
            },
            {
                keyword: 'add-new',
                files: ['js/private/working-cities.js']
            },
            {
                keyword: 'services',
                files: ['js/private/services.js']
            },
            {
                keyword: 'working-times',
                files: ['lib/datepair/datepair.js', 'lib/datepair/jquery.datepair.js', 'lib/datepair/jquery.timepicker.js', 'js/private/working-times.js']
            },
            {
                keyword: 'prices',
                files: ['js/private/prices.js']
            },
            {
                keyword: 'contact-info',
                files: ["js/private/contact-info.js"]
            },
            {
                keyword: 'gallery',
                files: ['lib/fancybox/jquery.fancybox.min.js', 'js/private/libs/simpleUpload.min.js', 'lib/sortable.js', 'js/interact.js', "js/private/gallery.js"]
            }
        ];

        for (var i = 0; i < additional_js.length; i++) {
            var item = additional_js[i];
            if (~prime_url.indexOf(item.keyword)) {
                item.files.forEach(function (file) {
                    jsFiles.push(file);
                });
                break;
            }
        }
        // ------------------------------------------------

        this.load_js(jsFiles);
    };

    Application.prototype.get_prefix_for = function (url) {
        for (var key in this.request_prefixes) {
            var arr = this.request_prefixes[key];

            for (var i = 0; i < arr.length; i++) {
                if (arr[i].includes(url)) {
                    return '/' + key;
                }
            }
        }
        return '';
    };

    Application.prototype.load_page = function (url, area) {

        // Remove starting and ending slashes from url and split the url to query params and URN
        // -----------------------------
        var URI = url.replace(/^\/+|\/+$/g, '').split('?');
        var URN = URI[0];
        var query = typeof URI[1] != 'undefined' ? '?' + URI[1] : '';
        // -----------------------------

        // Some pages need to bo changed completely, that's why I show in url x but send request to y
        // ------------------------------
        if (this.rewrite_pages[URN]) URN = this.rewrite_pages[URN];
        // ------------------------------

        var keep_loader = this.requires_loader.some(function (item) {
            return window.location.href.includes(item)
        });

        if (!URN.length) URN = 'index';
        var prefix = this.get_prefix_for(URN);
        var request_url = prefix + '/' + URN;

        $.ajax({
            method: 'GET',
            url: lang_id + request_url + query,
            data: {
                ajax: true
            },
            cache: true,
            success: function (response) {
                try {
                    response = JSON.parse(response);
                    if (typeof response == 'object' && response['status'] == 'error') {
                        if (typeof response.redirect_to_signin !== undefined) {
                            window.location.href = response.redirect_to_signin;
                        }
                    }
                    if (typeof response == 'object') {
                        this.inner_menu_start(response, url);
                    }
                } catch (e) {
                    this.inner_menu_start(response, url);
                }

                if ($('body').hasClass('sidebar-mobile-show')) {
                    $("button.navbar-toggler").trigger("click");
                }

                if (!keep_loader) {
                    $('.main').LoadingOverlay("hide", true);
                    $('.user-area-escort > .wait-for-load').removeClass('wait-for-load');
                }

                // Setup Fullscreen page
                // -------------------------------------------------
                var is_page_fullscreem = this.fullscreen_pages.some(function (item) {
                    return url.includes(item);
                })
                if (!is_page_fullscreem) {
                    $('body').removeClass('chat-on');
                }
                // -------------------------------------------------

                // Setting up Greetings panel
                // ------------------------------------------
                this.set_greetings_panel();
                // -------------------------------------------------

                // Set current navigation item to active
                // -------------------------------------------------
                this.set_nav_active_item(url);
                // -------------------------------------------------


                this.isRunning = false;
                this.update_tinymce_editors();

            }.bind(this),

            error: function () {
                window.location.href = this.page404;
            },

        });
    }

    Application.prototype.set_greetings_panel = function (url) {
        if (url === undefined) url = window.location.href;
        var pages_without_greetings_title = ['my-reviews', 'upgrade-now'];
        var pages_without_greetings_panel = ['support'];

        $('.greetings-top-panel .section-title.active').removeClass('active');
        if (url.includes('profile')) {
            $('.greetings-top-panel .section-title.profile').addClass('active');
        } else if (pages_without_greetings_title.some( function(item) { return url.includes(item) } )) {
            $('.greetings-top-panel .section-title').removeClass('active');
        } else {
            $('.greetings-top-panel .section-title.default').addClass('active');
        }

        if(pages_without_greetings_panel.some( function(item) { return url.includes(item) } )) {
            $('.greetings-top-panel').hide();
        }else{
            $('.greetings-top-panel').show();
        }
    };

    Application.prototype.update_tinymce_editors = function () {
        var cssUpdatedForFirefox = false;

        tinymce.remove();
        tinymce.init({
            selector: '.tinymce-editor',
            toolbar: ['bold italic underline | link image'],
            forced_root_block : "",
            width: '100%',
            height: "185px",
            menubar: false,
            invalid_elements: '*[*]',
            extended_valid_elements: 'p, strong, b, em, span',
            force_p_newlines: false,
            setup: function (editor) {

                editor.on("change keyup", function (e) {
                    var id = editor.targetElm.getAttribute('id');
                    var $counterBadge = $('#mce-counter');
                    $counterBadge.text(editor.getContent({ format: 'text' }).length);

                    tinyMCE.triggerSave(); // updates all instances
                    $(editor.getElement()).trigger('change'); // for garlic to detect change
                });

                editor.on('keydown', function () {
                    var id = editor.targetElm.getAttribute('id');
                    var $counterBadge = $('#mce-counter');
                    $counterBadge.text(editor.getContent({ format: 'text' }).length);

                    if (!cssUpdatedForFirefox) {
                        $('#editor-container iframe').contents().find("head")
                            .append($("<style type='text/css' id='row-wrapper-style'>  body, body *{-ms-word-break: break-all  !important;word-break: break-all  !important;}  </style>"));
                        cssUpdatedForFirefox = true;
                    }
                })
            }
        });

    }
    Application.prototype.inner_menu_start = function (response, url) {
        window.location.hash = url;

        if (typeof response == 'object' && response.hasOwnProperty('data')) {
            this.mainContent.html(response['data']);
        } else {
            this.mainContent.html(response);
        }
        this.inner_js_routes(url);
        this.inner_styles_routes(url);
        // userNotification();
        $('html, body').animate({
            scrollTop: $("html").offset().top
        }, 1000);
    };

    Application.prototype.set_nav_active_item = function () {
        try {
            var _self = this;
            // Add class .active to current link - AJAX Mode off
            this.navigation.find('a').each(function () {

                var current_item_link = String($(this).attr('data-ajax-url')).split('?')[0];

                if (window.location.href.includes(current_item_link)) {
                    _self.navigation.find('li.active').removeClass('active');
                    var $parent_li = $(this).parents('li');
                    $parent_li.addClass('active');

                    // Adding Blue Background via ".active" class
                    if ($parent_li.parent().hasClass('submenu-list'))
                        $parent_li.parents('.toggler').addClass('active')

                    // Open dropdown if it wasn't open already
                    if (!$parent_li.parents('.toggler').hasClass('open') && $parent_li.parents('.toggler').find('.accordion-heading').length) {
                        $parent_li.parents('.toggler').find('.accordion-heading').get(0).click()
                    }

                }
            });

            $('#menuBar').find('.submenu-list').each(function () {
                if ($(this).find('li.active').length == 0) {
                    $(this).removeClass('show open');
                }
            })

        } catch (e) {
            console.warn(e.message)
        }
    }

    Application.prototype.resize_broadcast = function () {
        var timesRun = 0;
        var interval = setInterval(function () {
            timesRun += 1;
            if (timesRun === 5) {
                clearInterval(interval);
            }
            window.dispatchEvent(new Event('resize'));
        }, 62.5);
    }

    Application.prototype.setup_url = function (url, area) {
        if (this.isRunning) return false;

        this.isRunning = true;
        if (area) {
            this.mainContent = area;
        }

        $('body').attr('class', function(i, c){
            return c.replace(/(^|\s)profile-page-\S+/g, '');
        });
        if(url.length) {
            $('body').addClass('profile-page-' + url.replace(/\?.*/,''));
        }

        var has_callback = this.urls_with_callback.some(function (item) {
            if (url.includes(item.path)) {
                item.cb();
                return true;
            }
        });

        if (has_callback) {
            this.inner_js_routes(url);
            this.inner_styles_routes(url);
            return this.isRunning = false;
        }

        $(".main").LoadingOverlay("show", {
            color: "rgba(239, 243, 249, 0.80)",
            zIndex: 1000
        });
        $('.modal').modal('hide');

        this.load_page(url);
    }

    Application.prototype.event_handlers = {

        generate_page: function (e) {
            e.preventDefault();
            var target = $(e.currentTarget);
            $('.nav li .nav-link, .mob-pa-nav').removeClass('active');
            target.addClass('active');
            // var targeturl = target.attr('data-ajax-url').replace(/private\/?#?\/?/g, '').replace('#', '/');
            var targeturl = target.attr('data-ajax-url');
            routie(targeturl);
        },

        toggle_open_class: function () {
            $(this).addClass('open');
        },

        dropdown_toggler_click: function (e) {
            if ($(this).hasClass('nav-dropdown-toggle')) {
                $(this).parent().toggleClass('open');
                this.resize_broadcast();
            }
        },

        nav_link_click: function (e) {
            if (this.ajaxLoad) {
                e.preventDefault();
            }

            if ($(this).hasClass('nav-dropdown-toggle')) {
                $(this).parent().toggleClass('open');
                this.resize_broadcast();
            }
        },

        navbar_toggler_click: function () {
            if ($(this).hasClass('sidebar-toggler')) {
                $('body').toggleClass('sidebar-hidden');
                _self.resize_broadcast();
            }

            if ($(this).hasClass('sidebar-minimizer')) {
                $('body').toggleClass('sidebar-minimized');
                _self.resize_broadcast();
            }

            if ($(this).hasClass('aside-menu-toggler')) {
                $('body').toggleClass('aside-menu-hidden');
                _self.resize_broadcast();
            }

            if ($(this).hasClass('mobile-sidebar-toggler')) {

                $('body').toggleClass('sidebar-mobile-show');
                _self.resize_broadcast();

            }
        },

        close_sidebar: function () {
            $('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');
        },

        ajax_form_submit: function (e) {
            e.preventDefault();

            var clicked_button = $(document.activeElement);
            var $container = clicked_button.parents("form");
            if(!$container.length) $container = $('#ui-view');

            var action = (clicked_button.attr('value')) ? clicked_button.attr('value') : ':next';

            $container.LoadingOverlay("show", {
                color: "rgba(239, 243, 249, 0.80)",
                zIndex: 1000
            });

            $('input[name=then]').val(action);
            var values = $('form').serializeArray();
            var url = $(this).attr('action') ? $(this).attr('action') : 'private/profile/index?ajax=true';

            $.ajax({
                type: "POST",
                url: url,
                data: values,
                success: function (response, status, xhr) {
                    $('form.ajax-form').trigger("ajax-form-success", response);

                    var a = $('input[name="a"]').val();
                    var did_animation = false;
                    $('.is-invalid').removeClass('is-invalid');

                    try {
                        response = JSON.parse(response);
                        if (response.success) {

                            if (!response['prevent_popup']) {
                                Notify.alert('success', headerVars.dictionary.changes_saved);
                            }
                            $('[id^=validation-]').html('');
                            $('#errors-container').hide();
                            if (response.data) {
                                $('#ui-view').html(response.data);
                                $('html, body').animate({scrollTop: 0}, 1000);
                                APPLICATION.update_tinymce_editors();
                            }
                        } else {
                            if (typeof response.redirect_to_signin != "undefined") {
                                window.location.href = response.redirect_to_signin;
                            }
                            $('#errors-container').show();
                            $('[id^=validation-]')
                                .html('')
                                .parents('.visible-on-error')
                                .hide();

                            $.each(response['errors'], function (key, value) {
                                $("[data-error='" + key + "']").addClass('is-invalid');
                                $('#validation-' + key)
                                    .html(value)
                                    .parents('.visible-on-error')
                                    .show();

                                if (!did_animation && $('#vlaidation-' + key).length) {
                                    $('html, body').animate({
                                        scrollTop: $('#validation-' + key).offset().top - 100
                                    }, 1000);

                                    did_animation = true;
                                }
                            });

                        }

                    } catch (e) {
                        console.warn(e.message);
                        $('#ui-view').html(response);
                        Notify.alert('success', headerVars.dictionary.changes_saved);
                    } finally {
                        $container.LoadingOverlay("hide", true);
                    }
                }
            });
        },

        prevent_default: function (e) {
            e.preventDefault();
        }
    }

    Application.prototype.bind_events = function () {
        var _self = this;

        $(document).on('click', 'button[data-ajax-url],a[data-ajax-url],option[data-ajax-url]', this.event_handlers.generate_page.bind(this));
        $('.toggler').click(this.event_handlers.toggle_open_class);
        $('nav > ul.nav').on('click', 'a', this.event_handlers.dropdown_toggler_click.bind(this));
        this.navigation.on('click', 'a', this.event_handlers.nav_link_click);
        $('.navbar-toggler').click(this.event_handlers.navbar_toggler_click);
        $('.sidebar-close').click(this.event_handlers.close_sidebar);
        $('a[href="#"][data-top!=true]').click(this.event_handlers.prevent_default);
        $(document).off('submit', 'form.ajax-form').on('submit', 'form.ajax-form', this.event_handlers.ajax_form_submit);
    };

    Application.prototype.init = function () {

        // If Escort tries to accept a page other than complete profile
        // When Escort has LIMITED PA (this means email is not verified)
        // Then we redirect to complete profile page :)
        // -------------------------------------------
        if(headerVars.currentUser && headerVars.currentUser.status == -1 && !window.location.hash.includes('complete-profile-step')) {
            window.location.hash = '#complete-profile-step';
            return window.location.reload();
        }
        // -------------------------------------------

        try {
            this.bind_events();

            if (this.ajaxLoad) {
                var url = location.hash.replace(/^#/, '');
                if (url != '') {
                    this.setup_url(url);
                }
            }

            routie({
                'private *': function (url) {
                    var is_profile_page = this.request_prefixes['private/profile'].includes(url.replace(/^\/+|\/+$/g, '').split('?')[0]);
                    this.setup_url(url)

                    // if (typeof headerVars.currentUser != 'undefined' && headerVars.currentUser.user_type == 'agency' && is_profile_page) {
                    //     var query = ~url.indexOf('?') ? url.slice(url.indexOf('?')) : '';
                    //
                    //     // If escort was not specified and we are not inside 'Full Edit' page already
                    //     // ---------------------------------
                    //     if (!query.length && !$('.edit-full-page-content').length) this.setup_url('');
                    //     // ---------------------------------
                    //
                    //     var xhr = EscortProfile.ShowFull('private/profile/full' + query);
                    //
                    //     if (xhr) {
                    //         xhr.done(function () {
                    //             this.inner_js_routes(url);
                    //             this.inner_styles_routes(url);
                    //             this.update_tinymce_editors();
                    //         }.bind(this))
                    //     }
                    //
                    // } else {
                    //     this.setup_url(url)
                    // }
                }.bind(this)
            });

            // Activating the navigation
            $('nav').removeClass('inactive')

        } catch (e) {
            console.error("Whoops there was an Error !!!", e);
        }


        return this;
    }

    return Application
})();


// Init The Page
$(function () {

    var config = {
        ajaxLoad: false,
        page404: 'profile/index',
        mainContent: $('#ui-view'),
        isRunning: false,
        navigation: $('nav ul.nav-list:not([id^=submenu])'),
        rewrite_pages: {
            '100-verification': 'verify/start',
            'black-list-clients': 'client-hotel-blacklist/clients',
            'black-list-hotels': 'client-hotel-blacklist/hotels',
            'admin-warning': 'client-hotel-blacklist/admin-warning',
            'my-reviews': 'reviews/my-reviews',
            'upgrade-now': 'online-billing-v2/index',
            'my-profile': 'private/member-profile',
            'I-follow': 'private/member-follow-top10',
            'city-alerts': 'city-alerts/index',
            'agency-profile': 'private/agency-profile',
            'add-new': 'private/profile/biography-step',
            'manage-models': 'private/ajax-get-agency-escorts',
        },
        request_prefixes: {
            'private/profile': ['biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times', 'prices', 'contact-info', 'gallery', 'complete-profile-step'],
            'private': ['index', 'tours', '100-verification', 'settings']
        },
        fullscreen_pages: ['messages'],
        urls_with_callback: [
            {
                path: 'support',
                cb: Support.load.bind(null, null, '/support/ajax-tickets-list?ajax=1'),
            },
            {
                path: 'messages',
                cb: PrivateMessaging.Load
            }
        ],
        requires_loader: ['upgrade-now'],
    };

    window.APPLICATION = (new Application(config)).init();
})