var MemberFollow = function () {

    var startSearch = false;
    var templates = {
        city_item: function (params) {
            return `<div class="other-city justify-content-between">
                    <div class="city-name"> ${params.title}  <span class="text-grey">( ${params.country_title} )</span></div>
                    <div class="city-remove text-grey d-flex align-items-center">
                        <p class="uppercase">Remove</p>
                        <i class="fa fa-times"></i>
                        <input value="${params.id}" name="cities[]" type="hidden">
                    </div>
                </div>`;
        },
        loading: function () {
            return "Loading ...";
        }
    };

    function init() {
        bind_events();
        init_tooltips();
    }

    function add_city() {

        var city = {
            title: $("#citiesFilter").find(".dropdown-toggle-val").html(),
            country_title: $("#countryFilter").find(".dropdown-toggle-val").html(),
            id: $("#cities").val(),
        };

        // Validation
        if (!$("#cities").val()) return console.info('No City selected ');
        if (!$("#country").val()) return console.info('No Country selected ');
        if ($('input[name="cities[]"][value="' + city.id + '"]').length > 0) return console.info('Already added');
        //

        var container = templates.city_item(city);
        $("#city-locations").append(container);
    }

    function remove_city() {
        $(this).parents(".other-city").slideUp('slow', function () {
            $(this).remove();
        })
    }

    function load_follows(text, page = 1) {
        var url = lang_id + '/private/member-follow/';
        var $container = $('#follow-content');
        $container.LoadingOverlay('show');
        var data = {};

        data = {
            'page': page,
            'showname': text
        };

        $.ajax({
            url: url,
            type: 'post',
            data: data,
            success: function (resp) {
                $container.html(resp);
                $container.LoadingOverlay('hide');
            }
        })
    }

    function construct_dropdown(params) {
        try {
            var regions = JSON.parse(params.regions);
            var visible_html = ``;
            var data_html = ``;

            for (var title in regions) {

                var data_html_chunk = ``;
                var visible_html_chunk = ``;

                regions[title].forEach(function (city) {
                    data_html_chunk += `<option value="${city.id}">${city.title}</option>`;
                    visible_html_chunk += `<li data-val="${city.id}"><a href="javascript:void(0)">${city.title}</a></li>`;
                });

                if (title) {
                    data_html += `<optgroup label="${title}" >${data_html_chunk}</optgroup>`;
                    visible_html += `<ul class="sub-dropdown-menu"> <li class="mb-2 mt-2"> <b> ${title} </b> </li> ${visible_html_chunk}</ul>`;
                } else {
                    data_html += data_html_chunk;
                    visible_html += visible_html_chunk;
                }
            }

            params.select.html(data_html).val(null); // This is only <option> s
            params.ul.html(visible_html); // This one is for ul>li that is visible for user

        } catch (e) {
            console.warn(e.message)
        }
    }

    function on_country_changed() {

        var $country_select = $("#country"),
            $cities_select = $("#cities");

        $cities_select.empty();
        $cities_select.siblings('ul[data-type="citiesFilter"]').empty().append(templates.loading());
        $cities_select.siblings('.dropdown-toggle').find('.dropdown-toggle-val').html(headerVars.dictionary.nothing_selected_html);

        $.ajax({
            url: '/geography/ajax-get-cities-grouped?json=true',
            data: {
                country_id: $country_select.val()
            },
            success: function (r) {

                construct_dropdown({
                    regions: r,
                    select: $cities_select,
                    ul: $cities_select.siblings('ul[data-type="citiesFilter"]'),
                })

            }
        });
    }

    function init_tooltips() {
        $('.member-follow-page [data-toggle="tooltip"]').tooltip();
    }

    function remove_follow(e) {

        e.preventDefault();

        var dataType = $(this).data('type');
        var dataTypeId = $(this).data('type-id');
        var $container = $('#follow-content');
        $container.LoadingOverlay('show');

        $.ajax({
            url: '/private/remove-follow/',
            type: 'post',
            data: {'type': dataType, 'type_id': dataTypeId},
            success: function (resp) {
                $container.LoadingOverlay('hide');
                var currentPage = $container.find('.page-number.active').html();
                if (currentPage != 1) {
                    load_follows(null, (currentPage - 1));
                } else {
                    load_follows();
                }
            }
        })
    }

    function handle_page_change(e) {
        e.preventDefault();
        var page = $(this).attr('data-page');
        load_follows(null, page);
    }

    function follow_edit(e, params = {}) {
        e.preventDefault();

        var followUserId = params.followUserId || $(this).data('follow-id');
        var id = params.id || $(this).data('type-id');
        var type = params.type || $(this).data('type');
        var $container = $('#edit-follow-modal .modal-content');
        $container.LoadingOverlay('show');

        $.ajax({
            type: "GET",
            url: lang_id + '/private/edit-follow?follow_user_id=' + followUserId + '&id=' + id + '&type=' + type,
            success: function (resp) {
                $container.html(resp);
                $container.LoadingOverlay('hide');
            }
        })
    }

    function update_follow_data(e) {
        e.preventDefault();
        var $form = $(this);
        var data = $form.serializeArray().reduce(function (obj, item) {
            if (item.name.includes('[')) {
                if (obj[item.name] && $.isArray(obj[item.name]))
                    obj[item.name].push(item.value);
                else
                    obj[item.name] = [item.value];
            } else {
                obj[item.name] = item.value;
            }
            return obj;
        }, {});
        $form.LoadingOverlay('show');

        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: data,
            dataType: 'JSON',
            success: function(r) {
                if(r.status == 'success') {
                    $('#edit-follow-modal').modal('hide');
                    Notify.alert('success', headerVars.dictionary.changes_saved)
                }
            }
        }).done(function () {
            $form.LoadingOverlay('hide');
        })
    }

    function add_top10(id) {
        var ids = [id];

        $('.toggle-top-10.active').each(function() {
            ids.push($(this).parents('.escort-info').data('id'));
        });

        var url = lang_id + '/private/profile/sort-top10/';

        $.ajax({
            url: url,
            type: 'post',
            data: {sort_id: ids.splice(0,10)},
            success: function (resp) {
                console.log(resp)
            }
        });
    }

    function remove_top10(id) {
        var url = lang_id + '/private/profile/favorites-remove/';

        $.ajax({
            url: url,
            type: 'post',
            data: {id: id}
        });
    }

    function toggle_top_10(e) {
        e.preventDefault();
        var was_active = $(this).hasClass('active');
        var $parent = $(this).parents('.escort-info');
        var id = $parent.data('id');
        $(this).toggleClass('active');

        if(was_active) {
            $(this).find('a').tooltip('hide')
                .attr('data-original-title',  headerVars.dictionary.not_in_top_10)
                .tooltip('show');
            return remove_top10(id);
        }else{
            $(this).find('a').tooltip('hide')
                .attr('data-original-title',  headerVars.dictionary.in_top_10)
                .tooltip('show');
            return add_top10(id);
        }

    }

    function reset_follow_model(e) {
        follow_edit(e, {
            id: $('#edit-follow-modal').find('input[name=user_type_id]').val(),
            type: $('#edit-follow-modal').find('input[name=user_type]').val(),
            followUserId: $('#edit-follow-modal').find('input[name=follow_user_id]').val()
        })
    }

    function bind_events() {
        $('#follow-search').on('input', search);
        $('.member-follow-page')
            .on('click', '.remove-follow-trigger', remove_follow)
            .on('click', 'a[data-page]', handle_page_change)
            .on('click', '.edit-follow-trigger', follow_edit)
            .on('click', '.add-other-city-trigger', add_city)
            .on("click", ".city-remove", remove_city)
            .on('click', '.toggle-top-10', toggle_top_10)
            .on('click', '.reset-follow-modal', reset_follow_model)
            .on('change', '#country', on_country_changed);

        $('#edit-follow-modal form').on('submit', update_follow_data);
    }

    function search() {
        var $searchInput = $('#follow-search');
        if (startSearch) {
            clearTimeout(startSearch);
        }

        startSearch = setTimeout(function () {
            load_follows($searchInput.val(), 1);
        }, 1000);
    }

    return {
        init: init
    }
}();


$(function () {
    MemberFollow.init();
})