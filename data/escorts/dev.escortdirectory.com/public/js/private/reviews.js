'use_strict';

var Reviews_Page = function () {};

Reviews_Page.load = function (data) {

    $container = $("#ui-view");

    $container.LoadingOverlay("show", {
        color: "rgba(239, 243, 249, 0.80)",
        zIndex: 1000
    });

    $.ajax({
        url: '/reviews/my-reviews',
        type: "GET",
        data: data,
        success: function(response) {

            $container
                .LoadingOverlay("hide")
                .html(response);
        }
        
    })

    return false;
}

Reviews_Page.prototype.templates = {

}

Reviews_Page.prototype.event_handlers = {

    toggle_review: function (e) {
        e.preventDefault();
        var $parent = $(this).parents(".review-item");

        $parent.find(".extra-info-container")
            .toggleClass('open')
            .stop(true, true).slideToggle();

        if($parent.find(".extra-info-container").hasClass('open')) {
            $parent.find('.when-open').show();
            $parent.find('.when-closed').hide();
        } else {
            $parent.find('.when-open').hide();
            $parent.find('.when-closed').show();
        }
    },

    add_comment: function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).parents(".review-item").find(".comment-container").stop(true, true).slideToggle();
    },

    submit_new_comment: function (event) {
        event.preventDefault();
        var $btn = $(event.target),
            $form = $btn.parents('.comment-container').first().find('.comment-form'),
            id = $btn.data('id'),
            url = $form.attr('action'),
            data = $form.serializeArray(),
            $container = $("#ui-view");

        $container.LoadingOverlay("show", {
            color: "rgba(239, 243, 249, 0.80)",
            zIndex: 1000
        });

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: "JSON",
            success: function (response) {

                if (response.status == "error")
                    console.warn(response.msgs);
                else if (response.status == 'ok')
                    $form.slideUp();

                $container.LoadingOverlay('hide');
                $('#search-form').trigger('submit');
            }
        })
    },

    set_filter: function (event) {
        event.preventDefault();
        var $btn = $(event.target),
            page = $btn.data('page') || 1,
            data = {
                page: page,
            };

        if ($('#escort_id') && $.trim($('#escort_id').val()))
            data.escort_id = $('#escort_id').val();

        if ($('#showname') && $.trim($('#showname').val()))
            data.showname = $('#showname').val();

        if ($('#member') && $.trim($('#member').val()))
            data.member = $('#member').val();

        if ($('#city').val())
            data.city_id = $('#city').val();

        if ($('#p_country_id').val())
            data.country_id = $('#p_country_id').val();

        if ($('#commented').val())
            data.commented = $('#commented').val();

        if ($('#date_added_f').val())
            data.date_added_f = $('#date_added_f').val();

        if ($('#date_added_t').val())
            data.date_added_t = $('#date_added_t').val();

        var f_d = data.f_d;
        delete data.f_d;

        if (f_d && f_d.length > 0) {
            var arr = new Array();
            arr = f_d.split('|');

            var sort_field = arr[0];
            var sort_dir = '';

            if (arr[1] == undefined)
                sort_dir = 'desc';
            else
                sort_dir = arr[1];

            sort_dir = sort_dir.toLowerCase();

            if (sort_dir == 'asc') {
                sort_dir = 'desc';
            } else {
                sort_dir = 'asc';
            }

            data.sort_field = sort_field;
            data.sort_dir = sort_dir;
        }

        Reviews_Page.load(data);
    }
}

Reviews_Page.prototype.bind_events = function () {
    $(document)
        .on("click", ".view-trigger", this.event_handlers.toggle_review)
        .on("click", ".add-comment", this.event_handlers.add_comment)
        .on("click", ".submit-post-btn", this.event_handlers.submit_new_comment)
        .on("click", ".set-filter", this.event_handlers.set_filter)
        .on("submit", "#search-form", this.event_handlers.set_filter)
}

Reviews_Page.prototype.init = function () {
    this.bind_events();

    return this;
}

// Statics

$(function () {

    if (!window.REVIEWS_PAGE)
        window.REVIEWS_PAGE = (new Reviews_Page()).init();

})