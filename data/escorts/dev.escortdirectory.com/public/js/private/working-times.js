var Working_times_page = (function () {

    function Working_times_page() {};

    Working_times_page.prototype.event_handlers = {

        toggle_schedule_block: function () {

            if ($('#on_schedule_radio').is(':checked'))
                $('.weekdays-container').addClass("approved")
            else
                $('.weekdays-container').removeClass("approved")
        },
        toggle_schedule_day: function () {

            if ($(this).is(':checked')) {
                $(this).parents('.datepicker-row')
                    .find('input.time')
                    .removeAttr('disabled')
                    .eq(0).focus();
            } else {
                $(this).parents('.datepicker-row')
                    .find('input.time')
                    .attr('disabled', 'disabled')
                    .val('')
            }
        }

    }

    Working_times_page.prototype.helpers = {

        init_datepicker: function () {

            $('.datepicker-row .time').timepicker({
                'showDuration': true,
                'timeFormat': 'H:i',

            });

            $('.datepicker-row ').datepair({
                defaultTimeDelta: 1800000 // Half hour
            });
        }

    }


    Working_times_page.prototype.bind_events = function () {
        if (!window.WORKING_TIMES_PAGE){
            $(document).on("change", "#on_schedule_radio, #available_24_7_radio", this.event_handlers.toggle_schedule_block);
            $(document).on("change", "input[name^='day_index']", this.event_handlers.toggle_schedule_day);
        }

    }

    Working_times_page.prototype.init = function () {

        try {
            this.bind_events();
            this.helpers.init_datepicker();
        }catch(e) {
            console.error("Whoops there was an error", e);
		}

        return this;
    }

    return Working_times_page;
})();




$(function () {
    window.WORKING_TIMES_PAGE = (new Working_times_page()).init();
})