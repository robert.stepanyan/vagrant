var Dash = {
  init:function() { 
    this.initPagination();
    this.initDeleteprofile();
    this.initbubbleText();
    this.initProfileStatus();

  },

   initPagination:function(  ){
    var self = this;

    $('.dash-notifications .page-item .page-link').on('click', function(){
       $(".dashboard-notifications").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
     
        var  agency_id = $(this).attr('data-agency-id');
        var  page = $(this).attr('data-page');

        $.ajax({
         type: "GET",
         url: "private/agency-dashboard?de_page=" + page + "&agency_id=" + agency_id,
         
         success: function(response){
           $('.dashboard-notifications').html(response);
         },
         complete: function(e){
          $(".dashboard-notifications").LoadingOverlay("hide", true);
          self.init();
         }

     });// AJAX END<a class="page-link" data-agencyid="7960" href="#">2</a>
    })

  }, // initPagination END

  initbubbleText: function(){
    $('#agency_escorts').on('change', function(event) {
      $('.instant-live-messages-block').LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000 });
      
      $.ajax({
         type: "GET",
         url: "private/get-bubble-text?escort_id="+$(this).val(),
          success: function(response){
            if(response != false){
               response = JSON.parse(response);
               $('#bubble-text').val(response['text']); 
            }
         },
         complete: function(e){
          $('.instant-live-messages-block').LoadingOverlay("hide", true);
          $('#bubble-text').trigger('focus');
         }
      })

    });

    $('#instant-live-messages').submit(function(event) {
      event.preventDefault();

      $.ajax({
         type: "GET",
         url: "private/add-bubble-text",
         data: {
            escort_id: $('#agency_escorts').val(),
            text: $('#bubble-text').val()
         },
         success: function(response){
            if(response == 'one_per_day_limit'){
              $('#instant-live-messages-alerts').addClass('show').removeClass('d-none');
              setTimeout( function(){ $('#instant-live-messages-alerts').addClass('d-none').removeClass('show') }, 7000 );
            }else{
              $('#instant-live-messages-alert-success').addClass('show').removeClass('d-none');
              setTimeout( function(){ $('#instant-live-messages-alert-success').addClass('d-none').removeClass('show') }, 7000 );
            }
         },
      })

    });
  },


  initDeleteprofile: function(){
    $('#delete-escort-profile').on('click',  function(){
   
      $(".main").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
      $.ajax({
         type: "GET",
         url: "private/profile-delete",

          success: function(response){

            $('#inner-notifications').append(response);
            $('.delete-comment-form-modal').modal('show');
            $('.delete-comment-form-modal').on('hide.bs.modal', function(){
              $('#inner-notifications').html('');
            })

            $('.submit-btn-delete-comment').on('click', function(){
                var delete_comment = $('textarea[name="comment"]').val();
             
                $.ajax({

                  type: "POST",
                  url: "private/profile-delete?ajax=true",
                  data: {comment: delete_comment},
                  
                  success: function(response){
                    try {
                      response = JSON.parse(response);
                        if(typeof response =='object' && response['status'] == 'error' ){
                         $.each( response['msgs'], function( key, value ) {
                          $( "[name='"+key+"']" ).addClass('is-invalid');
                          $('#validation-'+key).html(value);
                        });
                       }

                       if(response['status'] == 'success'){
                          $('.delete-comment-form-modal .modal-body').html('<div class="alert alert-success">You have successfully submitted your delete inquire</div>');
                          $('.delete-comment-form-modal .submit-btn-delete-comment').remove();
                       }                    
                    }
                    catch (e) {
                      
                    }
                    finally{
                     
                    }
                  }

                })
            })

         },

         complete: function(e){
           $(".main").LoadingOverlay("hide", true);
         }
      })
   })
  },

  initProfileStatus: function(){
    $('#profile-status-top').on('click', function(){
        $(".main").LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000   });
        var action = $('#profile-status-top').attr('value');

        $.ajax({
         type: "GET",
         url: "private/profile-status?act=" + action,
         success: function(response){
            if(response){
              if(action == 'enable'){
                $('#profile-status-top').val("disable");
                $('#profile-status-top').find('.alert-warning').removeClass('alert-warning').addClass('alert-success');
                $('.top-status-block').hide();
              }else{
                  $('#profile-status-top').val("enable");
                  $('#profile-status-top').find('.alert-success').removeClass('alert-success').addClass('alert-warning');
                  $('.top-status-block').show();
              }
            }
         },
         complete: function(e){
           $(".main").LoadingOverlay("hide", true);
         }
      })
    })
  },


};

$(document).ready(function(){
  Dash.init();
}); 





