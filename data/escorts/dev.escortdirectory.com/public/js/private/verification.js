var Verification = {

    escort_id: $('input[name=escort_id]').val(),
    interval: null,
    isSearching: false,

    init: function () {
        // this.initUploader();
        this.initEscortsProfiles();
    },

    on_search_try: function () {

        if(Verification.isSearching) return console.warn("Already searching ...");
        if(Verification.interval) {
            clearInterval(Verification.interval);
        }

        Verification.interval = setTimeout(function() {
            Verification.verify_escorts_action( 1 );
        }, 500);

    },

    form_submit_handler: function () {
        event.preventDefault();
        var self = this,
            $form = $(event.target),
            $container = $("#ui-view");

        $container.LoadingOverlay("show", {
            color: "rgba(239, 243, 249, 0.80)",
            zIndex: 1000
        });

        $.ajax({
            url: $form.attr('action'),
            type: "POST",
            data: $form.serializeArray(),
            dataType: "JSON",
            success: function (response) {
                var did_animation = false;

                $('.is-invalid').removeClass('is-invalid');
                $('[id^=validation-]').html('');
                $('#errors-container').hide();

                if (response.status == 'success')
                    return Verification.load('/verify/success');
                else {
                    $('#errors-container').show();
                    $('.visible-on-error').hide();
                }

                $.each(response['msgs'], function (key, value) {

                    $('#validation-' + key)
                        .html(value)
                        .parents('.visible-on-error')
                        .show();

                    if (!did_animation && $('#validation-' + key).length) {
                        $('html, body').animate({
                            scrollTop: $('#validation-' + key).offset().top - 100
                        }, 1000);

                        did_animation = true;
                    }
                })

                $container.LoadingOverlay("hide", true);
            }
        })
    },

    addUploader: function () {
        var uploaders_count = $('#uploaders input[type=file]').length;

        if (uploaders_count > 9) return false;

        $('<div/>').append('<label class="custom-file my-1 w-75">Image no. ' + (uploaders_count + 1) + ' <input id="uploader-' + (uploaders_count + 1) + '" type="file" name="pic[' + uploaders_count + ']" class="custom-file-input"><span class="custom-file-control  form-control-file"></span> </label>').appendTo($('#uploaders'));
    },


    attach_file: function () {
        var $btn = $(event.target);
        var $file_input = $btn.siblings('input[type=file]');
        var $parent_row = $btn.parents('.picture-container');

        if ($file_input.attr("multiple")) {
            if ($parent_row.hasClass('havePicture')) {
                $parent_row.removeClass('havePicture').find('.additional-block').empty();
            }
        } else {

            if ($parent_row.hasClass('haveVideo')) {
                $parent_row
                    .removeClass('haveVideo')
                    .find('div.image')
                    .css('background-image', 'url()')
                    .end()
                    .find('.additional-block')
                    .remove();
            }
        }


        if ($file_input.length)
            $file_input.trigger('click');
        else {
            console.warn("Whoops!, File input was not found");
        }
    },

    init_video_upload: function () {
        var e_id = $('input[name="escort_id"]').val();
        var url = '/verify/upload-video?escortid=' + e_id,
            $file_input = $(event.target),
            block = $('<div class="additional-block mt-1"></div>'),
            progress_bar = $('<div class="progress-bar progress-bar-striped progress-bar-animated" style="width:0%"></div>'),
            id = $file_input.attr('id'),
            container = $file_input.parents('.video-container');

        block.append(progress_bar);
        container
            .find('.additional-block')
            .remove()
            .end()
            .append(block);

        $file_input.simpleUpload(url, {
            allowedExts: ["mov", "wmv", "ogg", "webm", "mp4"],
            allowedTypes: ["video/mp4", "video/webm", "video/ogg"],
            limit: 1,
            maxFileSize: 1500000,
            start: function (file) {
                container.removeClass('haveVideo')
            },
            progress: function (progress) {
                progress_bar.width(progress + "%");
            },
            success: function (response) {

                try {
                    response = JSON.parse(response);

                    if (response.status == "1") {
                        var template = $('' +
                            '<input type="hidden" name="hash" value="' + response.hash + '" />' +
                            '                            <input type="hidden" name="ext[' + response.hash + '][]" value="' + response.ext + '" />' +
                            '').appendTo(block);

                        container.addClass('haveVideo').find('.additional-block').append(template);
                    } else {
                        var text = response.error;
                        if ($.isNumeric(text) || text.length < 4) {
                            Notify.alert('danger', headerVars.dictionary.file_upload_error);
                        } else {
                            $('#uploads .errors').html('<p>' + text + '</p>');
                            $('html, body').animate({scrollTop: 0}, 1000);
                        }
                    }
                } catch (e) {
                    console.warn("Whoops! something went wrong. Here are the details " + e.message, "Response from server", response);
                } finally {
                    setTimeout(function () {
                        progress_bar.slideUp();
                    }, 1500);
                }
            },
            error: function (error) {
                block.remove();
                var text = error.message;

                $('#uploads .errors').html(text);
                $('html, body').animate({
                    scrollTop: 0
                }, 1000);

            }
        });
    },

    init_multiple_file_upload: function () {

        var url = '/verify/upload-photos?escortid=' + this.escort_id,
            $file_input = $(event.target),
            container = $file_input.parents('.picture-container');
        block = container.find('.additional-block'),
            progress_bar_wrapper = block.find('.progress-bar-wrapper'),
            id = $file_input.attr('id');

        var file_count = $file_input.get(0).files.length + container.find('.additional-block .picture-info').length;

        if (file_count > 2) {
            return Notify.alert("warning", headerVars.dictionary.maximum_alloed_file_count + ': 2')
        }

        var progress_bar = $('<div class="progress-bar progress-bar-striped progress-bar-animated" style="width:0%"></div>');
        progress_bar_wrapper.empty().append(progress_bar);

        $file_input.simpleUpload(url, {
            allowedExts: ["jpg", "jpeg", "png"],
            allowedTypes: ["image/jpeg", "image/png", "image/x-png", "image/jpg"],
            limit: 2,
            expect: "JSON",
            start: function (file) {

            },
            progress: function (progress) {
                progress_bar.width(progress + "%");
            },
            success: function (response) {
                if (response.status == "1") {
                    var image_url = response.photo_url ? response.photo_url : "/images/default-image.jpeg";

                    var template = $("" +
                        "<div class=\" d-inline-block mt-4 \">" +
                        "<div class=\"picture-info d-flex mr-2 mb-2\">" +
                        "<div class=\"image havePicture\"></div>" +
                        "</div>" +
                        //"<div class=\"mt-4 autocomplete-container flex-grow-1 unset-height\" autocomplete=\"off\">" +
                        //"<textarea name=\"comment[" + response.hash + "][]\" placeholder=\"Comment ...\" rows=\"3\"></textarea>" +
                        //"</div>" +
                        "<input type=\"hidden\" name=\"hash\" value=\"" + response.hash + "\" />" +
                        "<input type=\"hidden\" name=\"ext[" + response.hash + "][]\" value=\"" + response.ext + "\" />" +
                        "</div> " +
                        "").appendTo(block).find('.image').css('background-image', "url(" + image_url + ")").end();

                    container.find('.additional-block').append(template);

                    if (file_count >= 2) {
                        container.addClass('havePicture')
                    }
                } else {
                    var text = response.error;
                    $('#uploads .errors').html('<p>' + text + '</p>');
                    $('html, body').animate({scrollTop: 0}, 1000);
                }
                setTimeout(function () {
                    progress_bar.slideUp();
                }, 1500);
            },
            error: function (error) {
                //block.remove();
                var text = error.message;

                $('#uploads .errors').html(text);
                $('html, body').animate({
                    scrollTop: 0
                }, 1000);

            }
        })
    },

    init_single_file_upload: function () {
        var e_id = $('input[name="escort_id"]').val();
        var url = '/verify/upload-photos?escortid=' + e_id,
            $file_input = $(event.target),
            block = $('<div class="additional-block"></div>'),
            progress_bar = $('<div class="progress-bar progress-bar-striped progress-bar-animated" style="width:0%"></div>'),
            id = $file_input.attr('id'),
            container = $file_input.parents('.picture-container');


        if (container.hasClass('havePicture')) {
            container
                .removeClass('havePicture')
                .find('div.image')
                .css('background-image', 'url()')
                .end()
                .find('.additional-block')
                .remove();
        }

        block.append(progress_bar)
        container
            .find('.additional-block')
            .remove()
            .end()
            .append(block)


        $file_input.simpleUpload(url, {
            allowedExts: ["jpg", "jpeg", "png"],
            allowedTypes: ["image/jpeg", "image/png", "image/x-png", "image/jpg"],
            limit: 1,
            expect: "JSON",
            start: function (file) {
            },
            progress: function (progress) {
                progress_bar.width(progress + "%");
            },
            success: function (response) {
                if (response.status == "1") {
                    var image_url = response.photo_url ? response.photo_url : "/images/default-image.jpeg";


                    var template = $("" +
                        //"<div class=\"autocomplete-container flex-grow-1 unset-height\" autocomplete=\"off\">" +
                        //"<textarea name=\"comment[" + response.hash + "][]\" placeholder=\"Comment ...\" rows=\"3\"></textarea>" +
                        //"</div>" +
                        "<input type=\"hidden\" name=\"hash\" value=\"" + response.hash + "\" />" +
                        "<input type=\"hidden\" name=\"ext[" + response.hash + "][]\" value=\"" + response.ext + "\" />" +
                        "").appendTo(block);

                    container
                        .addClass('havePicture')
                        .find('div.image')
                        .css('background-image', 'url(' + image_url + ')')
                        .end()
                        .find('.additional-block')
                        .append(template);
                } else {
                    var text = response.error;
                    $('#uploads .errors').html('<p>' + text + '</p>');
                    $('html, body').animate({scrollTop: 0}, 1000);
                }
                setTimeout(function () {
                    progress_bar.slideUp();
                }, 1500)
            },
            error: function (error) {
                block.remove();
                var text = error.message;

                $('#uploads .errors').html(text);
                $('html, body').animate({
                    scrollTop: 0
                }, 1000);

            }
        });
    },

    verify_escorts_action: function (page) {

        var $container = $('#ui-view');
        $container.LoadingOverlay('show');

        $.ajax({
            url: lang_id + '/verify/ajax-escorts',
            type: 'post',
            data: {
                'per_page': 10,
                'escorts_page': page,
                'search_content': $('#not-verified-escort-search').val()
            },
            success: function (resp) {
                $container
                    .html(resp)
                    .LoadingOverlay('hide');
            }
        });

        return false;
    },

    load: function (url, escort_id) {

        //if(!Controls.confirmationChangeTab()) return false;

        //var url = lang_id + url;

        var data = {};

        if (typeof (escort_id) === 'undefined') {
            escort_id = 0;
        } else {
            data = {
                escort_id: escort_id,
                choose_escort: true
            };
        }

        var container = $("#ui-view");

        container.LoadingOverlay("show", {
            color: "rgba(239, 243, 249, 0.80)",
            zIndex: 1000
        });

        $.ajax({
            url: url,
            type: 'post',
            data: data,
            success: function (resp) {
                container.html(resp).LoadingOverlay("hide", true);

                // if( url.search('idcard') != -1 ){
                //     Verification.Init(container, escort_id);
                // }
            }
        });

        return false;
    },

    initUploader: function () {

        $('#verification-form').submit(function (event) {

            event.preventDefault();

            var data = new FormData(document.querySelector('#verification-form'));

            $("#ui-view").LoadingOverlay("show", true);
            $.ajax({
                url: '/private/verify/idcard',
                type: 'POST',
                processData: false,
                contentType: false,
                data: data,
                success: function (data) {
                    $('#ui-view').html(data).LoadingOverlay("hide", true);
                    Verification.init();
                }
            });
        });


        $('#overview-form').submit(function (event) {
            event.preventDefault();

            var data = $('#overview-form').serializeArray();
            $("#ui-view").LoadingOverlay("show", true);

            $.ajax({
                url: '/private/verify/idcard-overview',
                type: 'POST',
                data: data,
                success: function (data) {
                    $('#ui-view').html(data).LoadingOverlay("hide", true);
                    Verification.init();
                }
            });

        });


        $('body').on('change', '.custom-file-input', function () {
            $(this).next('.form-control-file').addClass("selected").html($(this).val());
        });
    },
    initEscortsProfiles: function () {
        $(window).resize(function () {
            if ($(window).width() < 567) {
                $('#verification_escorts a[data-ajax-url]').each(function () {
                    $(this).attr('data-ajax-url-disabled', $(this).attr('data-ajax-url'));
                    $(this).removeAttr('data-ajax-url');
                });
            } else {
                $('#verification_escorts a[data-ajax-url-disabled]').each(function () {
                    $(this).attr('data-ajax-url', $(this).attr('data-ajax-url-disabled'));
                    $(this).removeAttr('data-ajax-url-disabled');
                });
            }
        });

        $('.escort-thumb').click(function () {
            $('.escort-thumb').removeClass('selected_verify_escort');
            $(this).addClass('selected_verify_escort');
            $('#verify_escort_mobile').removeClass('btn-default').addClass('btn-success');
        });

        $('#verify_escort_mobile').click(function () {

            if ($(this).attr('data-ajax-url')) return false;

            var selected_escort = $('.selected_verify_escort');

            if (!selected_escort.length) return false;

            var url = $('.selected_verify_escort').find('a[data-ajax-url-disabled]').attr('data-ajax-url-disabled');
            $(this).attr('data-ajax-url', url);
            $(this).trigger('click');

        });

        $(window).trigger('resize');

    }

};


$(document).ready(function () {
    Verification.init();
});