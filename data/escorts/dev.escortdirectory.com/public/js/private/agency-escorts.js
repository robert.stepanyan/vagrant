var AgencyEscorts = function () {

    var $tabs;
    var $sort_select;
    var CURRENT_PAGE = 1;
    var escorts_per_page = 10;

    function init() {
        update_selectors();
    }
    
    function showEscort(params) {
        $('.manage-escort-tabs').fadeOut('slow', function() {
            $('.manage-specific-escort').fadeIn();
        });

        $('input[name="escort_id"]').val(params.escort);
        FullEditPage.init(params);
    }

    function backToModels() {
        $('.manage-specific-escort').fadeOut('slow', function () {
            $('.manage-escort-tabs').fadeIn();
        });
    }

    function update_selectors() {
        $tabs = $('.filter-selection a');
        $sort_select = $('#agency-escorts-sort');

        $('[data-toggle="tooltip"]').tooltip();
    }

    function update_escorts(action = null, escort_id = null, page = null) {
        if (!page) page = {};
        else {
            AgencyEscorts.CURRENT_PAGE = page.escorts_page;
        }

        if (event) {
            event.preventDefault();
        }

        var $container = $('#ui-view');
        $container.LoadingOverlay('show');

        try {
            $.ajax({
                url: lang_id + '/private/ajax-get-agency-escorts',
                type: 'post',
                data: Object.assign({
                    a: action,
                    escort_id: escort_id,
                    active_tab: $('.filter-selection a.active').attr('title'),
                    agency_sort: $sort_select.val(),
                    per_page: escorts_per_page
                }, page),
                success: function (resp) {
                    $('#left-menu li a').removeClass('active');
                    $('#left-menu .agency-escorts').addClass('active');
                    $container.html(resp);
                    bind_events();
                }
            }).done(function () {
                $container.LoadingOverlay('hide');
            })
        } catch (e) {
            console.log(e)
        }
    }

    function on_tab_click(e) {
        e.preventDefault();
        if ($(this).hasClass('active')) return;

        AgencyEscorts.CURRENT_PAGE = 1;
        $tabs.removeClass('active');
        $(this).addClass('active');
        update_escorts();
    }

    function on_sort_change() {
        update_escorts();
    }

    function bind_events() {
        update_selectors();

        $tabs.on('click', on_tab_click);
        $sort_select.on('change', on_sort_change);
    }

    return {
        init: init,
        update_escorts: update_escorts,
        CURRENT_PAGE: CURRENT_PAGE,
        showEscort: showEscort,
        backToModels: backToModels
    }
}();

// Init page
// --------------------------------------
$(document).ready(function () {
    AgencyEscorts.init();
});
// --------------------------------------

