// Created by Eduard 2019
var CityAlerts = function () {

    var RUNNING_XHR = {
        getting_countries: null,
    };
    var templates = {
        loading: function () {
            return "Loading..."
        }
    };

    function init() {
        bind_events();
        show();
    }

    function construct_dropdown($select) {
        var html = '';
        $select.find('option').each(function () {
            html += '<li data-val="' + $(this).val()  + '"><a href="javascript:void(0)">' + $(this).html() + '</a></li>'
        });
        $select.siblings('.dropdown-menu').html(html);
    }

    function load_alert_cities(country, container) {
        if(RUNNING_XHR['getting_countries']) RUNNING_XHR['getting_countries'].abort();
        var selected;
        var $country = $(country);
        var country_id = $country.val();

        if (!country_id.length) return;

        var $city_1 = $(container + ' .city_id_1');
        var $city_2 = $(container + ' .city_id_2');
        var $city_3 = $(container + ' .city_id_3');

        $city_1.empty()
            .siblings('.dropdown-menu').html(templates.loading()).end()
            .parent().find('.dropdown-toggle-val').html(headerVars.dictionary.nothing_selected_html);
        $city_2.empty()
            .siblings('.dropdown-menu').html(templates.loading()).end()
            .parent().find('.dropdown-toggle-val').html(headerVars.dictionary.nothing_selected_html);
        $city_3.empty()
            .siblings('.dropdown-menu').html(templates.loading()).end()
            .parent().find('.dropdown-toggle-val').html(headerVars.dictionary.nothing_selected_html);

        $city_2.append($("<option />", {value: '', html: '-'}));
        $city_3.append($("<option />", {value: '', html: '-'}));

        RUNNING_XHR['getting_countries'] = $.ajax({
            url: lang_id + '/geography/ajax-get-cities',
            data: {country_id: country_id},
            type: 'get',
            dataType: 'json',
            success: function (resp) {
                RUNNING_XHR['getting_countries'] = null;

                if(resp.data.length > 1) {
                    $city_1.append($("<option />", {value: '-1', html: searchVars.all_cities}));
                }

                resp.data.forEach(function (c) {
                    $city_1.append($("<option />", {
                        value: c.id,
                        html: c.title + '&nbsp;&nbsp;'
                    }));

                    $city_2.append($("<option />", {
                        value: c.id,
                        html: c.title + '&nbsp;&nbsp;'
                    }));

                    $city_3.append($("<option />", {
                        value: c.id,
                        html: c.title + '&nbsp;&nbsp;'
                    }));
                });
                construct_dropdown($city_1);
                construct_dropdown($city_2);
                construct_dropdown($city_3);
            }
        });
    }

    function show() {
        var $container = $('#my-alert-container');

        $.ajax({
            url: lang_id + '/city-alerts/ajax-get',
            type: 'get',
            success: function (resp) {
                $container.html(resp);
            }
        });
    }

    function add() {

        var $container = $('#ui-view');
        var $error_container = $('.add-error-msg');
        $container.LoadingOverlay('show');

        var data = collect_data_from('.add-city-alert');

        $.ajax({
            url: lang_id + '/city-alerts/add',
            type: 'post',
            data: data,
            dataType: "json",
            success: function (resp) {
                $container.LoadingOverlay('hide');

                if (!$error_container.hasClass('d-none'))
                    $error_container.addClass('d-none');

                if (resp.status == 1) {
                    show();
                } else if (resp.status == 0) {
                    var msgs = resp.message;

                    $error_container.html(msgs);
                    $error_container.removeClass('d-none');
                }
            }
        })
    }

    function removeAlert(e){
        e.preventDefault();
        var id = $(this).data('key');
        var $container = $(this).parents('.my-alert-container');
        $container.LoadingOverlay('show');
        $.ajax({
            url: lang_id + '/city-alerts/clear',
            method: 'post',
            data: {
                id: id
            },
            success: function (resp) {
                $container.LoadingOverlay('hide');
                if (resp.status) {
                    $container.remove()
                }
            }
        })
    }

    function on_country_changed() {
        load_alert_cities(this, $(this).attr('data-group'));
    }

    function suspend(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var $container = $('#ui-view');
        $container.LoadingOverlay('show');

        $.ajax({
            url: lang_id + '/city-alerts/suspend',
            type: 'post',
            data: {
                id: id
            },
            success: function (resp) {
                $container.LoadingOverlay('hide');
                show();
            }
        })
    }

    function resume(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var $container = $('#ui-view');
        $container.LoadingOverlay('show');

        $.ajax({
            url: lang_id + '/city-alerts/resume',
            type: 'post',
            data: {
                id: id
            },
            success: function (resp) {
                $container.LoadingOverlay('hide');
                show();
            }
        });
    }

    function remove(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var $container = $('#ui-view');
        $container.LoadingOverlay('show');

        $.ajax({
            url: lang_id + '/city-alerts/remove',
            type: 'post',
            data: {
                id: id
            },
            success: function (resp) {
                $container.LoadingOverlay('hide');
                show();
            }
        });
    }

    function collect_data_from(contianer) {
        var $container = $(contianer);
        var data = {
            city_id_1: $container.find('.city_id_1').val(),
            city_id_2: $container.find('.city_id_2').val(),
            city_id_3: $container.find('.city_id_3').val()
        };

        data.country = $container.find('.country_id').val();
        data.escort_type = $container.find('.escort_type').val();
        data.escort_gender = $container.find('.escort_gender').val();
        data.alert_subject = $container.find('.alert_subject').val();

        if ($container.find('.send_email').is(':checked'))
            data.send_email = 1;
        else
            data.send_email = 0;

        return data;
    }

    function save(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var $container = $('#edit-city-alert-modal-' + id);
        var $error_container = $('#edit-city-alert-modal-' + id + ' .add-error-msg');
        $container.LoadingOverlay('show');

        var data = collect_data_from('#edit-city-alert-modal-' + id);
        data.id = id;

        $.ajax({
            url: lang_id + '/city-alerts/save',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (resp) {
                $container.LoadingOverlay('hide');

                if (!$error_container.hasClass('d-none'))
                    $error_container.addClass('d-none');

                if (resp.status == 1) {
                    $container.modal('hide');
                    show();
                } else if (resp.status == 0) {
                    var msgs = resp.message;
                    $error_container.html(msgs);
                    $error_container.removeClass('d-none');
                }
            }
        })
    }

    function hide_duplicate_city() {
        var group = $(this).attr('data-group');
        var targets = [$(group).find('.city_id_1').val(), $(group).find('.city_id_2').val(), $(group).find('.city_id_3').val()];
        var except = '';
        switch (true) {
            case $(this).hasClass('city_id_1'):
                except = '.city_id_1';
                break;
            case $(this).hasClass('city_id_2'):
                except = '.city_id_2';
                break;
            case $(this).hasClass('city_id_3'):
                except = '.city_id_3';
                break;
        }

        // Mark disabled other two dropdowns if they have no items to select
        // ----------------------------------------------------------------
        var disableDropdownsWithoutItems = function() {
            for (var i = 1; i <= 3; i++) {
                var cls = '.city_id_' + i;

                var nothing_left = $(group).find(cls).siblings('.dropdown-menu').find('li[data-val]:not(.disabled)').length <= 0;
                if(nothing_left) {
                    $(group).find(cls).parents('.dropdown').attr('disabled', true);
                }else{
                    $(group).find(cls).parents('.dropdown').removeAttr('disabled');
                }
            }
        }
        // ----------------------------------------------------------------


        // Dont try to understand this logic
        // -----------------------------------------
        if( except == '.city_id_1' && $(group).find(except).val() == '-1' ) {
            $(group).find('.city_id_2').val(null).siblings('.dropdown-menu').find('li[data-val]').hide().addClass('disabled');
            $(group).find('.city_id_2').siblings('.dropdown-toggle').find('.dropdown-toggle-val').html(headerVars.dictionary.nothing_selected_html);

            $(group).find('.city_id_3').val(null).siblings('.dropdown-menu').find('li[data-val]').hide().addClass('disabled');
            $(group).find('.city_id_3').siblings('.dropdown-toggle').find('.dropdown-toggle-val').html(headerVars.dictionary.nothing_selected_html);

            return disableDropdownsWithoutItems();
        }else if(except != '.city_id_1' && $(group).find(except).val() && $(group).find('.city_id_1').val() == '-1'){
            $(group).find('.city_id_1').val(null).siblings('.dropdown-menu').find('li[data-val]').hide().addClass('disabled');
            $(group).find('.city_id_1').siblings('.dropdown-toggle').find('.dropdown-toggle-val').html(headerVars.dictionary.nothing_selected_html);

        }
        // -----------------------------------------

        var selector = ['.city_id_1', '.city_id_2', '.city_id_3'];
        selector.splice(selector.indexOf(except), 1);
        $(group).find(selector.join(',')).each(function () {
            $(this).siblings('.dropdown-menu').find('li[data-val]').each(function () {
                if (~targets.indexOf($(this).attr('data-val'))) {
                    $(this).hide().addClass('disabled');
                } else {
                    $(this).show().removeClass('disabled');
                }
            })
        });

        disableDropdownsWithoutItems();
    }

    function bind_events() {
        $('.add-alert-trigger').on('click', add);
        $(".remove-city-alert").on('click', removeAlert);
        $('.city-alerts-page')
            .on('click', '.suspend-alert-trigger', suspend)
            .on('click', '.resume-alert-trigger', resume)
            .on('click', '.remove-alert-trigger', remove)
            .on('click', '.save-alert-trigger', save)
            .on('change', '.country_id', on_country_changed)
            .on('change', '.city_id_1, .city_id_2, .city_id_3', hide_duplicate_city);
    }

    return {
        init: init
    }
}();


$(function () {
    CityAlerts.init();
});

// var CityAlerts = {
//     init:function () {
//         this.initForm();
//         this.initRemove();
//     },
//
//     initForm:function () {
//         $('select').select2({
//             theme:'bootstrap'
//         });
//
//         $('#city-alerts-form').submit(function (event) {
//            event.preventDefault();
//            var self = $(this);
//
//            //destroy old errors
//            $('.error-area').html(' ');
//
//            var data = $(this).serializeArray();
//
//            $.ajax({
//               url:'/private/city-alerts/add',
//               type:'POST',
//               data:data,
//               beforeSend:function () {
//                   self.closest('.card-body').LoadingOverlay('show',true);
//               },
//               success:function (resp) {
//
//                   try{
//                       resp = JSON.parse(resp);
//
//                       $.each(resp['msgs'],function (key, value) {
//                           $('#'+key).html(value);
//                       });
//
//
//                   }catch(err){
//
//                       $('#ui-view').html(resp);
//                       CityAlerts.init();
//
//                       Notify.alert('success',"Your change has been saved!");
//                   }
//
//                   self.closest('.card-body').LoadingOverlay('hide',true);
//
//               }
//            });
//
//
//         });
//     },
//
//     initRemove:function () {
//         $('.remove_city_alert').click(function () {
//             var self = $(this);
//             var row = self.closest('tr');
//             var id = self.attr('data-id');
//
//             $.ajax({
//                 url:'/private/city-alerts/remove',
//                 type:'POST',
//                 data:{id:id},
//                 async:false,
//                 success:function (resp) {
//                     row.remove();
//                 }
//             });
//         });
//     }
//
// };

