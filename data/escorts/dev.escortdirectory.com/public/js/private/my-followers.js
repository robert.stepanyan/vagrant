var Followers_Page = (function () {

	function Followers_Page(){}

	Followers_Page.prototype.event_handlers = {
	
		submit_followers_form: function(e) {
	
			e.preventDefault();
			var $form = $('#my-followers-form'),
				fields = $form.serializeArray(),
				$container = $('#ui-view'),
				$grid = $('#grid'),
				data = {},
				self = this;
	
			fields.forEach(function(item) {
				data[item.name] = item.value;
			});
	
			$container.LoadingOverlay("show", {
				color: "rgba(239, 243, 249, 0.80)",
				zIndex: 1000
			});
	
			$.ajax({
				type: "POST",
				url: $form.attr('action'),
				data: data,
				success: function(response) {
					$grid.html(response);
					$container
						.LoadingOverlay('hide')
						.find('input[name="page"]').val('')
	
					// self.init_grid();
				}
			})
	
			return false;
		},
	
		change_page: function(e) {
			
			e.preventDefault();
			var $btn = $(event.target),
				page = $btn.data('page');
	
			this.setFilter({page: page});
		}
	}
	
	Followers_Page.prototype.setFilter = function(filter){
		var $form = $('#my-followers-form');
		
		for(var key in filter){
			$form.find('input[name=' + key + ']').val(filter[key]);
		}
	
		$form.trigger('submit');
	};
	
	Followers_Page.prototype.bind_events = function () {

		if (!window.FOLLOWERS_PAGE){
			$(document)
				.on('submit', '#my-followers-form', this.event_handlers.submit_followers_form.bind(this))
				.on('click', '.pagination-link', this.event_handlers.change_page.bind(this))
		}
	}
	
	Followers_Page.prototype.init = function () {

		try {
            this.bind_events();
			this.setFilter({});
        }catch(e) {
            console.error("Whoops there was an error", e);
		}
		
		return this;
	}

	return Followers_Page;
})();



$(function () {
	window.FOLLOWERS_PAGE = (new Followers_Page()).init();
})
