var AgencyProfile = function () {

    var templates = {
        loading: function () {
            return "Loading ...";
        }
    };

    var files_in_process = {
        profile: false
    };

    function init() {
        bind_events();
    }

    function init_upload(e) {
        e.preventDefault();
        var $form = $(this),
            file_type = $form.data('type'),
            $progressBarWrapper = $('.' + file_type + '-pics-progress-bar'),
            $progressBar = $progressBarWrapper.find('.progress-bar'),
            $profileImg = $('#' + file_type + '-pic'),
            $errorBar = $progressBarWrapper.find('.process-status .text');

        if (files_in_process[file_type]) {
            return alert("File is uploading, please wait ...");
        }

        $progressBarWrapper.addClass('uploading').removeClass('stop');
        files_in_process[file_type] = true;
        $errorBar.html('');
        $progressBar.css('width', 0 + '%');
        $progressBarWrapper.show();

        var formdata = new FormData(this),
            $file = formdata.get('file'),
            $fileId = (new Date()).getTime().toString(36),
            total = 0;

        var upload_chunk = function ($file, $fileId, $start) {

            // Force Stop Recursion
            if (files_in_process[file_type] == 'aborted') {
                return files_in_process[file_type] = null;
            }

            var chunk,
                $chunkSize = 524288, // 0.5 MB
                total = $start + $chunkSize;

            if (total >= $file.size) {
                total = $file.size
            }

            if ($file.mozSlice) {
                // Mozilla based
                chunk = $file.mozSlice($start, total);
            } else if ($file.webkitSlice) {
                // Chrome, Safari, Konqueror and webkit based
                chunk = $file.webkitSlice($start, total);
            } else {
                // Opera and other standards browsers
                chunk = $file.slice($start, total);
            }

            files_in_process[file_type] = $.ajax({
                type: "POST",
                url: $form.attr('action'),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-File-Id', $fileId);
                    xhr.setRequestHeader('X-File-Name', $file.name);
                    xhr.setRequestHeader('X-File-Size', $file.size);
                    xhr.setRequestHeader('X-File-Resume', 1);
                },
                contentType: "application/json",
                data: chunk,
                dataType: ' json',
                processData: false,
                success: function (response) {

                    var $newStartpos = 100 * total / $file.size;
                    $progressBar.css('width', $newStartpos + '%');

                    if (!response.finish) {

                        if (!response.error) {

                            upload_chunk($file, $fileId, total)
                        } else {
                            files_in_process[file_type] = null;

                            $errorBar.html(response.error)
                                .parent()
                                .removeClass('text-success')
                                .addClass('text-danger');
                            $progressBarWrapper.addClass('stop').removeClass('uploading');
                        }
                    } else {
                        files_in_process[file_type] = null;

                        if (response.error) {
                            $errorBar.html(response.error)
                                .parent()
                                .removeClass('text-success')
                                .addClass('text-danger');
                            $progressBarWrapper.addClass('stop').removeClass('uploading');

                        } else {
                            files_in_process[file_type] = false;
                            $progressBarWrapper.addClass('stop').removeClass('uploading');
                            $progressBar.css('width', '0%');

                            $errorBar.html('')
                                .parent()
                                .removeClass('text-danger')
                                .hide();

                            $profileImg.css('background-image', 'url(' + response['photo_url'] + ')');
                        }
                    }
                }
            })

        };
        upload_chunk($file, $fileId, 0);
    }

    function construct_dropdown(params) {
        try {
            var regions = JSON.parse(params.regions);
            var visible_html = ``;
            var data_html = ``;

            for (var title in regions) {

                var data_html_chunk = ``;
                var visible_html_chunk = ``;

                regions[title].forEach(function (city) {
                    data_html_chunk += `<option value="${city.id}">${city.title}</option>`;
                    visible_html_chunk += `<li data-val="${city.id}"><a href="javascript:void(0)">${city.title}</a></li>`;
                });

                if (title) {
                    data_html += `<optgroup label="${title}" >${data_html_chunk}</optgroup>`;
                    visible_html += `<ul class="sub-dropdown-menu"> <li class="mb-2 mt-2"> <b> ${title} </b> </li> ${visible_html_chunk}</ul>`;
                } else {
                    data_html += data_html_chunk;
                    visible_html += visible_html_chunk;
                }
            }

            params.select.html(data_html).val(null); // This is only <option> s
            params.ul.html(visible_html); // This one is for ul>li that is visible for user

        } catch (e) {
            console.warn(e.message)
        }
    }

    function on_country_changed() {

        var $country_filter = $('#countryFilter'),
            $country_select = $("#country"),
            $cities_select = $("#cities");

        $cities_select.empty();
        $cities_select.parent().find('.dropdown-toggle-val').html(headerVars.dictionary.nothing_selected_html);
        $cities_select.siblings('ul[data-type="citiesFilter"]').empty().append(templates.loading());

        $.ajax({
            url: '/geography/ajax-get-cities-grouped?json=true',
            data: {
                country_id: $country_select.val()
            },
            success: function (r) {

                construct_dropdown({
                    regions: r,
                    select: $cities_select,
                    ul: $cities_select.siblings('ul[data-type="citiesFilter"]'),
                })

            }
        });
    }

    function trigger_file(e) {
        e.preventDefault();
        var $btn = $(this),
            $target = $($btn.data('target'));

        $target.trigger('click');
    }

    function trigger_form() {
        $('.agency-photo-upload-form').trigger('submit');
    }

    function abort_uploading_process() {
        var $btn = $(this),
            target = $btn.attr('data-abort-target'),
            $form = $('form[data-type="' + target + '"]'),
            $progressBarWrapper = $('.' + target + '-pics-progress-bar');

        $progressBarWrapper.hide();
        files_in_process[target] = 'aborted';

        if ($form.length) {
            $form.get(0).reset();
        }
    }

    function hide_progress_bar() {
        $(this).parents('.progress-wrapper').hide();
        var target = $(this).attr('data-file-type'),
            $form = $('form[data-type="' + target + '"]');

        files_in_process[target] = null;

        if ($form.length) {
            $form.get(0).reset();
        }
    }

    function reset_agency_profile() {
        var url = location.hash.replace(/^#/, '');
        if (url != '') {
            APPLICATION.setup_url(url);
        }
    }

    function submit_agency_profile(e) {
        e.preventDefault();
        var $form  = $('.agency-profile-page');
        $form.trigger('submit');
    }

    function after_form_submitted(e, data) {
        var hash = window.location.hash,
            user_id = headerVars.currentUser.id,
            cookie_key = 'dont_show_no_provider_popup_for_' + user_id;

        try {
            if(typeof data == 'string') {
                data = JSON.parse(data);
            }
        }catch (e) {
            console.warn(e);
            data = {};
        }

        if(hash == '#agency-profile') {
            if(data.show_no_provider_popup && !Cookies.get(cookie_key)) {
                $('#no_provider_popup').modal('show');
            }
        }
    }

    function skip_adding_provider(e) {
        e.preventDefault();

        var dont_show_again = $('#dont_show_again').is(':checked'),
            user_id = headerVars.currentUser.id,
            cookie_key = 'dont_show_no_provider_popup_for_' + user_id;

        if(dont_show_again) {
            Cookies.set(cookie_key, true);
        }
    }

    function bind_events() {
        $('#country').on('change', on_country_changed);
        $('.image-trigger').on('click', trigger_file);
        $('#agency-profile-pic-file').on('change', trigger_form);
        $('.agency-photo-upload-form').on('submit', init_upload);
        $('.abort-uploading').on('click', abort_uploading_process);
        $('.close').on('click', hide_progress_bar);
        $('.discard-profile-changes').on('click', reset_agency_profile);
        $('.submit-profile').on('click', submit_agency_profile);
        $('form.ajax-form').on('ajax-form-success', after_form_submitted);
        $('#skip-adding-provider').on('click', skip_adding_provider);
    }

    return {
        init: init
    };
}();

$(document).ready(function () {
    AgencyProfile.init();
}); 
