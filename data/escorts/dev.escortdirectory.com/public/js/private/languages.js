var Languages_page = (function () {

    function Languages_page() {};

    Languages_page.prototype.templates = {
        language_item: function (params) {
            return '<div class="my-language justify-content-between" data-id="'+params.id+'">'+
                        '<div class="language-name">' + params.name + '<span class="text-grey"> ( ' + params.level + ')</span></div>'+
                        '<div class="language-remove text-grey d-flex align-items-center">'+
                            '<p class="uppercase">REMOVE</p>'+
                            '<i class="fa fa-times"></i>'+
                        '</div>'+
                        '<input type="hidden" name="langs['  + params.id + ']" value="' +params.level_id +'" init-value="'+params.level_id+'" />'+
                    '</div>';
        }
    }

    Languages_page.prototype.event_handlers = {
        add_language: function () {

            var language = {
                name: $("#language_picker_holder").find(".dropdown-toggle-val").html(),
                id: $("#language_picker").val(),
                level: $("#language_level_holder").find(".dropdown-toggle-val").html(),
                level_id: $("#language_level").val()
            };

            // Validation
            if (!language.id || $("#language_picker_holder").find(".dropdown-toggle-val").hasClass('pristine')) return console.info('No Language selected ');
            if (!language.level_id || $("#language_level_holder").find(".dropdown-toggle-val").hasClass('pristine')) return console.info('No Language level selected ');
            if ($('input[name="langs[' + language.id + ']"]').length > 0) return console.info('Already added');
            //

            var container = this.templates.language_item(language);
            $(".my-languages-container").append(container);

            $("#language_picker_holder").find(".dropdown-toggle-val").html(headerVars.dictionary.nothing_selected_html);
            $("#language_level_holder").find(".dropdown-toggle-val").html(headerVars.dictionary.nothing_selected_html);

            $("#language_picker").val(null);
            $("#language_level").val(null);

            removeDropdownSelected($("#language_level"));
            removeDropdownSelected($("#language_picker"));

            $('#language_picker').parent().find('li[data-val="' + language.id + '"]').show().addClass('disabled');
        },
        remove_language: function () {
            var $container = $(this).parents(".my-language");

            $container.slideUp('slow', function () {
                var lng = $container.data('id');
                $('#language_picker').parent().find('li[data-val="' + lng + '"]').removeClass('disabled');
                $(this).remove();
            });

        }
    }

    Languages_page.prototype.bind_events = function () {

        if (!window.LANGUAGES_PAGE) {
            $(document).on("click", ".add-language-trigger", this.event_handlers.add_language.bind(this));
            $(document).on("click", ".language-remove", this.event_handlers.remove_language);
        }
    }

    Languages_page.prototype.init = function () {

        try {
            this.bind_events();
        }catch(e) {
            console.error("Whoops there was an error", e);
        }

        return this;
    }

    return Languages_page;
})();

$(function () {
    window.LANGUAGES_PAGE = (new Languages_page()).init();
})