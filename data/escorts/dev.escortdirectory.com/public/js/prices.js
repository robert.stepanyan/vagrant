$(document).ready(function(){

	$(window).on('hashchange', function (e) {
    	changeTabs();
	});

	$('li.pill').on('click', function() {
		if(location.hash === '#ads-banners') {
			$('.location-filter-panel').hide();
		}else{
			$('.location-filter-panel').show();
		}
	});

	function changeTabs(){

		var w = window.innerWidth;
		if(w > 767){
			if(location.hash){
				var pill = $('a[data-hash="'+ location.hash +'"]');
				pill.click();
			} else{
				$('a[data-hash="#ads-female-escorts"]').click()
			}
		}
		else{
			if(location.hash){
				$('.mobile-pill-trigger[data-hash="'+ location.hash +'"]').click();
			} 
		}

	}
	changeTabs();
	showContactForm = function(category, position, country, days, price){
		var form = $('#price-contact-form');
		form.find('input[name="category"]').val(category)
		form.find('input[name="position"]').val(position)
		form.find('input[name="country"]').val(country)
		form.find('input[name="days"]').val(days)
		form.find('input[name="price"]').val(price)

		$('#contact-form-modal').modal('show')
	}


	function construct_dropdown(params) {
		try {
			var regions = JSON.parse(params.regions);
			var visible_html = "";
			var data_html = "";

			for (var title in regions) {
				var data_html_chunk = "";
				var visible_html_chunk = "";
				regions[title].forEach(function (city) {
					data_html_chunk += "<option value=\"".concat(city.id, "\">").concat(city.title, "</option>");
					visible_html_chunk += "<li data-val=\"".concat(city.id, "\"><a href=\"javascript:void(0)\">").concat(city.title, "</a></li>");
				});

				if (title) {
					data_html += "<optgroup label=\"".concat(title, "\" >").concat(data_html_chunk, "</optgroup>");
					visible_html += "<ul class=\"sub-dropdown-menu\"> <li class=\"mb-2 mt-2\"> <b> ".concat(title, " </b> </li> ").concat(visible_html_chunk, "</ul>");
				} else {
					data_html += data_html_chunk;
					visible_html += visible_html_chunk;
				}
			}

			params.select.html(data_html).val(null); // This is only <option> s

			params.ul.html(visible_html); // This one is for ul>li that is visible for user
		} catch (e) {
			console.warn(e.message);
		}
	}

	function set_country_type_visibility_filter( ){
		var $country_select = $("#country");
		var a_countries = [68, 10, 24, 23, 33, 67];
		var countryID = parseInt($country_select.val());
		var type = a_countries.includes(countryID) ? 'A' : 'B';

		$('.price-item:not(.uncategorized)').removeClass('show');
		$('.price-item:not(.uncategorized)[data-type='+ type +']').addClass('show');
	}

	$('select#country').on('change', function() {
		var $country_select = $("#country"),
			$cities_select = $("#city");

		$cities_select.empty();
		$cities_select.siblings('ul[data-type="city-select"]').empty().append('Loading ...');
		$cities_select.siblings('.dropdown-toggle').find('.dropdown-toggle-val').html(headerVars.dictionary.nothing_selected_html);
		set_country_type_visibility_filter();

		$.ajax({
			url: '/geography/ajax-get-cities-grouped?json=true',
			data: {
				country_id: $country_select.val()
			},
			success: function (r) {

				construct_dropdown({
					regions: r,
					select: $cities_select,
					ul: $cities_select.siblings('ul[data-type="city-select"]'),
				});
			}
		});
	});

	var fetchingMmgUrl = false;

	$('.price-item .item-price, .price-item .buy-button').on('click', function (e) {
		e.preventDefault();

		if(fetchingMmgUrl) return;

		var id = $(this).parents('.price-item').data('id');
		var country = $('#country').val();
		var city = $('#city').val();
		var $btn = $(this);

		$('#country-select').removeClass('invalid');

		if(! $('[data-hash="#ads-banners"]').hasClass('active') ) {
			$('html, body').animate({
				scrollTop: $('#country-select').offset().top
			}, 600);
			if (!country) return $('#country-select').addClass('invalid');
		}

		$.ajax({
			url: '/prices/generate-mmg-link',
			type: "POST",
			data: {id: id, country: country, city:city},
			dataType: "JSON",
			beforeSend: function() {
				$btn.LoadingOverlay('show');
				fetchingMmgUrl = true;
			},
			success: function (resp) {
				fetchingMmgUrl = false;
				$btn.LoadingOverlay('hide');

				if (resp.status === 'success') {
					window.location.href = resp.url;
				} else {
					alert("Service unavailble");
				}
			}
		})
	});

	$('#price-contact-form').submit(function(e){
		e.preventDefault();
		$('#contact-form-modal .modal-content').LoadingOverlay('show');
		var url = $(this).attr('action');
		data= $(this).serialize();
		$.ajax({
			url: url,
			type: 'POST',
			data:data,
			success: function(response){
				$('#contact-form-modal .modal-content').LoadingOverlay('hide');
				response = JSON.parse(response);
				if(response.status == 'error'){
					$('.error-box').html(' ')
					for(var key in response.msgs){
						$('.error-box').append('<p>' + response.msgs[key] + '</p>');
					}
					$('.error-box').show();
					return false;
				}

				$('#contact-form-modal').modal('hide')
				$('.error-box').html(' ')
				$('.error-box').hide();
			},
			error: function(response){

			},
		})
	})

	$('.mobile-pill-trigger').click(function(){
		var tabId = $(this).data('id');
		$(tabId).slideToggle();
		$(this).find('i').toggle();

	});

	$('.pill a').click(function(){
		if(history.pushState) {
		    history.pushState(null, null, $(this).data('hash'));
		}
		else {
		    location.hash = $(this).data('hash');
		}
	})

	$(".dropdown-selection.single-option").find(".dropdown-menu > li > a").on("click",function(){
		var val = $(this).html();
		$(this).parents(".dropdown-selection").find(".single-option-select").val(val);
	});

	$(".dropdown-toggle").each(function(){
		removeDropdownSelected(this);
	});

	$(document).on("click", ".dropdown-menu li a", function () {
		if($(this).parents('.dropdown').hasClass('disabled')) return;
		if($(this).parent().hasClass('disabled') || $(this).hasClass('disabled')) return;

		var val = $(this).html();
		var type = $(this).parents(".dropdown-menu").attr("data-type");
		$("#" + type).find(".dropdown-toggle-val")
			.html(val)
			.removeClass('pristine');
		$(this).parents('div.dropdown').first().find('select').val($(this).parent().data('val')).trigger('change');
		removeDropdownSelected(this);

	});

	function removeDropdownSelected(obj){
		var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
		$(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
			$(this).show();
		});
		$(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
			if ($(this).find("a").html() == val) {
				$(this).hide();
			}
		});
	}

	$('select#country').change();
});