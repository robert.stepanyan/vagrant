$(document).ready(function () {

    $(".dropdown-selection.single-option").find(".dropdown-menu > li > a").on("click", function () {
        var val = $(this).attr('data-val');
        $(this).parents(".dropdown-selection").find(".single-option-select").val(val).trigger('change');
    });


    // window.history.pushState({html:$("#agencies").html()}, '', location.pathname);
    $(".escort-details").each(function () {

        var text = $(this).find("p").html();
        var link = $(this).parents(".escort-item").find("a").attr("href");
        var len = 284;

        if (typeof text != "undefined") {
            if ((text.length > text.substring(0, len).length)) {
                text = text.substring(0, len - 3) + '...' + '<a href="' + link + '">Read more</a>';
            }
        }

        // return text; 
        $(this).find("p").html(text);
    });

    if (!ismobile()) {

        $(".image-slider").not('.single').each(function () {
            var x = $(this).find(".image");
            $(this).find(".image-bullets").append("<span class='bullet active'></span>");
            for (i = 1; i < x.length; i++) {
                x[i].style.display = "none";
                $(this).find(".image-bullets").append("<span class='bullet'></span>");
            }
        });
        var timer;

        $(".image-slider").not('.single').hover(function () {
            var myIndex = 0;
            var x = $(this).find(".image");
            var dots = $(this).find(".bullet");

            var tick = $(this).data('tick');
            if(!tick) tick = 500;

            timer = setInterval(function () {
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                myIndex++;
                if (myIndex > x.length) {
                    myIndex = 1
                }
                x[myIndex - 1].style.display = "block";
                dots[myIndex - 1].className += " active";
            }, tick);
        }, function () {
            clearInterval(timer);
        });
    } else {

        $(".image-slider").not('.single').each(function () {
            var x = $(this).find(".image");
            for (i = 1; i < x.length; i++) {
                x[i].style.display = "none";
            }
        });
    }


    function removeDropdownSelected(obj) {
        var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").text().trim();
        $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function () {
            $(this).show();
        });
        $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function () {
            if ($(this).find("a").text().trim() == val) {
                $(this).hide();
            }
            // if ($(this).find("a").find('label').text() == val) {
            //     $(this).hide();
            // }
        });
    }

    $(".dropdown-toggle").each(function () {
        removeDropdownSelected(this);
    });

    $(".dropdown-menu li a").on("click", function (e) {
        var val = $(this).text().trim();
        var type = $(this).parents(".dropdown-menu").attr("data-type");
        $("#" + type).find(".dropdown-toggle-val").text(val);

        var $checkbox = $(this).find('input[type=radio]');
        if($checkbox.length) {
            $(this).parents('ul.dropdown-menu')
                .first()
                .find('input[type=radio]')
                .removeAttr('checked');
            $checkbox.attr('checked', true)
        }
        removeDropdownSelected(this);
    });

    // var labels = [];
    // if($('.multiple-dropdown-box').length){
    //     $('.multiple-dropdown-box').multiselect({
    //
    //         onChange: function(option, checked, select) {
    //             var index = labels.indexOf($(option).val());
    //             if (index !== -1) {
    //                 labels.splice(index, 1);
    //             } else {
    //                 labels.push($(option).val());
    //             }
    //             var self = this;
    //             setTimeout(function () {
    //                 if (checked && option.val()) {
    //                     if (!self.$ul.parent().hasClass('selected')) {
    //                         self.$ul.parent().addClass('selected')
    //                     }
    //                 } else {
    //                     self.$ul.parent().removeClass('selected')
    //                 }
    //             });
    //         },
    //         buttonText: function(obj) {
    //             if (labels.length === 0) {
    //                 return 'Nothing selected';
    //             }
    //             else if (labels.length >= 2) {
    //                 return labels[0]+ ', +' + (labels.length - 1);
    //             }
    //             else if(labels.length >= 1){
    //                 return labels;
    //             }
    //         }
    //     });
    // }

    if ($('.multiple-dropdown-box').length) {
        $('.multiple-dropdown-box').multiselect({
            numberDisplayed: 1,
            nonSelectedText: headerVars['dictionary']['noneSelected'],
            /*onDropdownShown: function(e) {
                var $dropdown = $(e.target).parent().find('.multiselect-container');

                var y1 = $dropdown.offset().top;
                var y2 = $('#advf-search-mobile').offset().top;
                var h = $dropdown.height();
                var epsilion = 35;

                if(y1 + h >= y2) {
                    console.log("Above");
                    $('.homepage .filters-mobile').animate({
                        scrollTop: $('.homepage .filters-mobile').scrollTop() + (y1 + h - y2) + epsilion
                    }, 1, function() {
                        setTimeout(function() {
                            $dropdown.addClass('show');
                        }, 50)
                    });
                }

                $dropdown.addClass('show');
                console.log($dropdown)
            },
            onDropdownHidden: function(e) {
                var $dropdown = $(e.target).parent().find('.multiselect-container');

                $dropdown.removeClass('show');
            },*/
            onChange: function (option, checked) {
                if (checked && option.val()) {
                    if (!this.$ul.parent().hasClass('selected')) {
                        this.$ul.parent().addClass('selected')
                    }
                } else {
                    this.$ul.parent().removeClass('selected')
                }
            },
            onInitialized: function ($select, $container) {
                if ($select && $select.val() && $select.val().length > 0) {
                    $container.addClass('selected')
                }
            },
            buttonText: function (options, select) {
                if (this.disabledText.length > 0 &&
                    (select.prop('disabled') || (options.length == 0 && this.disableIfEmpty))) {

                    return this.disabledText;
                } else if (options.length === 0) {
                    return this.nonSelectedText;
                } else if (this.allSelectedText &&
                    options.length === $('option', $(select)).length &&
                    $('option', $(select)).length !== 1 &&
                    this.multiple) {

                    if (this.selectAllNumber) {
                        return this.allSelectedText + ' (' + options.length + ')';
                    } else {
                        return this.allSelectedText;
                    }
                } else if (this.numberDisplayed != 0 && options.length > this.numberDisplayed) {
                    return options.first().text() + ', +' + (options.length - 1);
                } else {
                    var selected = '';
                    var delimiter = this.delimiterText;

                    options.each(function () {
                        var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).text();
                        selected += label + delimiter;
                    });

                    return selected.substr(0, selected.length - this.delimiterText.length);
                }
            }
        });
    }


    if ($('#countryInput-mobile').length || $('#countryInput').length) {
        if (ismobile()) {
            autocomplete(document.getElementById("countryInput-mobile"));
        } else {
            autocomplete(document.getElementById("countryInput"));
        }
    }



    $('.homepage').on("click", ".view-buttons", function () {
        $(".view-buttons").removeClass("active");
        $(this).addClass("active");
        var date = new Date();
        document.cookie = "list_type=;expires=1969-12-31T23:59:59.000Z";
        var exp = new Date(date.setFullYear(date.getFullYear() + 1));
        exp = date.toISOString();

        var cookie = "list_type=";
        if ($(this).data("type") === "view-squares") {
            $(".escorts-list").removeClass("view-list").addClass("d-flex view-squares");
            $(".escort-item").removeClass("d-flex");
            cookie += "gallery;expires=" + exp;
        } else {
            $(".escorts-list").removeClass("d-flex view-squares").addClass("view-list");
            $(".escort-item").addClass("d-flex");
            cookie += "list;expires=" + exp;
        }
        document.cookie = cookie;
    });

    $(".intentions-container-mobile").on("click", function () {
        $(".filters-mobile").stop(true, true).fadeToggle('slow', function() {
            if($(".filters-mobile").css('display') == 'none') {
                $('html,body').removeClass('no-more-scroll');
            }else{
                $('html,body').addClass('no-more-scroll');
            }
        });
    });

    $("#cancel-mobile").on("click", function () {
        $(".intentions-container-mobile").trigger("click");
    });

    $(".btn-dropdown-rounded").on("click", function () {
        $(this).parent(".d-flex").find(".btn-dropdown-rounded").removeClass("active");
        $(this).addClass("active");
    });

    $(".search-mobile").on("click", function () {
        var country = $("#countryInput-mobile").val();
        if (country !== "") {
            $("#myCountry-mobile-place").html(country);
        }else{
            $("#myCountry-mobile-place").html('City Country ...');
        }

        if ($(".lookingFor-mobile").find(".btn-dropdown-rounded").hasClass("active")) {
            $(".lookingFor-mobile-place").html($(".lookingFor-mobile").find(".btn-dropdown-rounded.active").html());
        }

        if ($(".interestedIn-mobile").find(".btn-dropdown-rounded").hasClass("active")) {
            $(".interestedIn-mobile-place").html($(".interestedIn-mobile").find(".btn-dropdown-rounded.active").html());
        }

        $(".intentions-container-mobile").trigger("click");
    });

    // $('body').on('click', '.pagination-container a.page', function (e) {
    //     e.preventDefault();
    //     var $link = $(this).data('rel');
    //     if ($link){
    //         $('body').LoadingOverlay("show", {color : 'rgba(244, 245, 247, 0.44)'});
    //         $.ajax({
    //             url: $link,
    //             beforeSend: function(){
    //
    //             },
    //             success:function (response) {
    //                 var data = JSON.parse(response);
    //                 window.history.pushState({html:data.escort_list}, '', $link);
    //                 $('#agencies').html(data.escort_list);
    //                 $('.agencies').LoadingOverlay("hide");
    //             }
    //         })
    //     }
    // });

    // window.onpopstate = function(e) {
    //     if(e.state){
    //         var html  = e.state.html;
    //         if (html && html.length){
    //             $('#agencies').html(html);
    //         }
    //         console.log(e.state);
    //     }
    //     console.log(e.originalEvent, e.state, e);
    //     console.log(history.state, history);
    // };
});


function ismobile() {
    var newWindowWidth = $(window).width();
    return (newWindowWidth < 768);
}