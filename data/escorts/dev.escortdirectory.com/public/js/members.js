var Members = (function () {

    function init() {
        load_reviews(1);
        load_comments(1);
        bind_events();
    }

    function load_reviews(page) {

        var $container = $('.p-review'),
            user_name = $('#user_name').val(),
            user_id = $('#user_id').val();

        if(!$container.length) return console.warn("Reviews container didn't found.");
        if(!user_id) return console.warn("UserID was not found.");


        var data = {
            user_id: user_id,
            username: user_name
        };

        if (page)
            data.page = page;

        $container.LoadingOverlay('show');

        $.ajax({
            url: '/reviews/profile-member-reviews',
            data: data,
            type: 'get',
            success: function (resp) {
                $container.html(resp);
                $container.LoadingOverlay('hide');
            }
        });
    }

    function load_comments(page) {

        var $container = $('.p-comment'),
            user_name = $('#user_name').val();
            user_id = $('#user_id').val();

        if(!$container.length) return console.warn("Comments container didn't found.");
        if(!user_id) return console.warn("UserID was not found.");

        var data = {
            user_id: user_id,
            username: user_name
        };

        if (page)
            data.page = page;

        $container.LoadingOverlay('show');

        $.ajax({
            url: '/comments/profile-member-comments',
            data: data,
            type: 'get',
            success: function (resp) {
                $container.html(resp);
                $container.LoadingOverlay('hide');
            }
        })
    }

    function toggle_sections(e) {
        e.stopPropagation();
        e.preventDefault();

        var $clicked = $(this),
            $target = $($clicked.attr('data-collapse-target'));

        if(!$target.length) return console.warn("Whoops, section was not found!");

        $target.toggle();
    }

    function bind_events() {
        $('.member-profile-page [data-collapse-target]').on('click', toggle_sections);
    }

    return {
        init: init
    }
})();

$(function () {
    Members.init();
});