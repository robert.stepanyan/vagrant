$(document).ready(function (e) {

    $(document).on('click', 'a[data-pjax="1"]', function(event) {

        // event.preventDefault();
        var url = $(this).attr('href');

        $.pjax.click(event, '#pjax-container',{
            // timeout: 99999000,
            timeout: 5000, //	ajax timeout in milliseconds after which a full refresh is forecd
            push: true,//	use pushState to add a browser history entry upon navigation
            replace: false,//	replace URL without adding browser history entry
            maxCacheLength: 20,//	maximum cache size for previous container contents
            // version: '',//a string or function returning the current pjax version
            scrollTo: 0,//	vertical position to scroll to after navigation. To avoid changing scroll position, pass false.
            type: "POST",//	see $.ajax
            dataType: "html",//	see $.ajax
            url: url
        })


    });

    $(document).on('submit', 'form[data-pjax="1"]', function (event) {
        var url = $(this).attr('action');

        $.pjax.submit(event, '#pjax-container',{
            timeout: 99999000,//	ajax timeout in milliseconds after which a full refresh is forced
            push: true,//	use pushState to add a browser history entry upon navigation
            replace: false,//	replace URL without adding browser history entry
            maxCacheLength: 20,//	maximum cache size for previous container contents
            scrollTo: 0,//	vertical position to scroll to after navigation. To avoid changing scroll position, pass false.
            // type: "get",//	see $.ajax
            dataType: "html",//	see $.ajax
            url: url
        })
    });
    $(document).on('pjax:beforeSend',function (xhr, options) {
        $('#comments_container').LoadingOverlay("show", {color : 'rgba(244, 245, 247, 0.44)'});
    });
    $(document).on('pjax:beforeReplace',function (ev, contents, options) {

    });
    $(document).on('pjax:error',function (xhr, textStatus, error, options) {
        $('#comments_container').LoadingOverlay("hide");
        console.log(error)
    });

    $(document).on('pjax:success',function (event, data, status, xhr, options) {
        $('#comments_container').LoadingOverlay("hide");
    });

    // $(".intentions-container-mobile").on("click",function(){
    //     $(".filters-mobile").stop(true, true).fadeToggle();
    // });

});