$( document ).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();

    // Bottom Panel
    // ------------------------------------------------------
    $('.profile-bottom-panel .pullbar').click(function() {
        $(this).parents('.profile-bottom-panel').toggleClass('open');
        $('body').toggleClass('profile-backdrop-visible');
    });

    $(document).on('click', '.profile-backdrop', function(){
        $('.profile-bottom-panel .pullbar').trigger('click');
    });
    // ------------------------------------------------------

    // Send Message Logic
    // ----------------------------------------------
    $('.send-message').on('click', function (e) {
        e.preventDefault();
        var participant = $('#participant').val();
        Sceon.private_message_popup(participant);
    });
    // ----------------------------------------------

    if (!ismobile()) {

        $(".image-slider").each(function(){
            var x = $(this).find(".image");
            $(this).find(".image-bullets").append("<span class='bullet active'></span>");
            for (i = 1; i < x.length; i++) {
               x[i].style.display = "none";  
               $(this).find(".image-bullets").append("<span class='bullet'></span>");
            }
        });
        var timer;

        $(".image-slider").hover(function(){
            var myIndex = 0;
            var x = $(this).find(".image");
            var dots = $(this).find(".bullet");
            timer = setInterval(function(){
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none"; 
                    dots[i].className = dots[i].className.replace(" active", ""); 
                }
                myIndex++;
                if (myIndex > x.length) {myIndex = 1}    
                x[myIndex-1].style.display = "block";
                dots[myIndex-1].className += " active";
           },500);
        },function() {
            clearInterval(timer);
        });

        if ($(".escorts-slider").find(".image").length > 7) {
            for (var i = 8; i <= $(".escorts-slider").find(".image").length; i++) {
                $(".escorts-slider").find(".image:nth-child("+ i +")").css("display","none");
            }
            $(".escorts-slider").find(".image:nth-child("+ 7 +")").addClass("last").append("<span>+" + ($(".escorts-slider").find(".image").length - 8) + "</span>");
        }

    } else {

        $(".image-slider").each(function(){
            var x = $(this).find(".image");
            for (i = 1; i < x.length; i++) {
               x[i].style.display = "none";  
            }
        });

        if ($(".escorts-slider").find(".image").length > 4) {
            for (var i = 5; i <= $(".escorts-slider").find(".image").length; i++) {
                $(".escorts-slider").find(".image:nth-child("+ i +")").css("display","none");
            }
            $(".escorts-slider").find(".image:nth-child("+ 4 +")").addClass("last").append("<span>+" + ($(".escorts-slider").find(".image").length - 5) + "</span>");
        }

        $(".section-title").parent(".box-section").on("click",function(){
            $(this).find(".mobile-container").stop(true, true).slideToggle();
            if ($(this).find(".section-title > i").hasClass("fa-plus")) {
                $(this).find(".section-title > i").removeClass("fa-plus").addClass("fa-minus");
            } else {
                $(this).find(".section-title > i").removeClass("fa-minus").addClass("fa-plus");
            }
        });

    }
    initPopups();
});

function ismobile(){
     var newWindowWidth = $(window).width();

        return (newWindowWidth < 768);
}

function initPopups(){
    var agency_id = $('#agency_id').val();

    $(document).on('click', '.follow-me-button', function(){
        followAdd(this, agency_id, 'agency');
    });

    $(document).on('click', '.unfollow-me-button', function(){
        followRemove(this, agency_id, 'agency');
    });

    if(headerVars.show_follow_success){
        // Popup.followSuccess('escort');
    }

}
function followAdd(el, id, type) {
    if(headerVars.currentUser && headerVars.is_member == false){
        Popup.actionNotAvailable();
    }
    else if(headerVars.is_member){

        var $container = $(el).parents('.buttons-container').first();
        $container.LoadingOverlay("show", {color : 'rgba(244, 245, 247, 0.44)'});

        var data = {
            user_type: type,
            user_type_id: id,
            follow_type: 0
        };

        $.ajax({
            url: lang_id + '/follow/add?id=' + id + '&type=' + type,
            type: 'POST',
            data: data,
            success: function (resp) {
                resp = JSON.parse(resp);
                if ( resp.status === 'success' ) {

                    var unfollowButton = '<a data-newletter-popup="off"  class="follow-button unfollow-me-button">' +
                        '<div class="btn btn-small btn-brown follow">UNFOLLOW</div></a>';

                    var mobile_unfollowButton = '<a class="action-button unfollow-me-button">UNFOLLOW</a>';

                    $('.follow-me-button').each(function () {
                        if ($(this).parent().hasClass('mobile')) {
                            $(this)
                                .before(mobile_unfollowButton)
                                .remove();
                        } else {
                            $(this)
                                .before(unfollowButton)
                                .remove();
                        }
                    });

                    var followers_count = $('.followers-number-container');
                    if(followers_count.length){
                        var followersCount = parseInt(followers_count.first().text());
                        followers_count.text(1 + followersCount);
                    }
                }
                $container.LoadingOverlay('hide');
            }
        });

    }
    else{
        showFollowModal(type, id);
    }

    return false;
}

function followRemove(el, id, type) {
    var $container = $(el).parents('.buttons-container').first();
    $container.LoadingOverlay("show", {color : 'rgba(244, 245, 247, 0.44)'});

    var data = {
        type: type,
        type_id: id
    };

    $.ajax({
        url: '/follow/remove',
        type: 'POST',
        data: data,
        success: function (resp) {

            var followButton = '<a data-newletter-popup="off" class="follow-button follow-me-button">'+
                '<div class="btn btn-small btn-brown follow">FOLLOW</div></a>';

            var mobile_followButton = '<a class="action-button follow-me-button">FOLLOW</a>';

            $('.unfollow-me-button').each(function () {
                if ($(this).parent().hasClass('mobile')) {
                    $(this)
                        .before(mobile_followButton)
                        .remove();
                } else {
                    $(this)
                        .before(followButton)
                        .remove();
                }
            });

            var followers_count = $('.followers-number-container');
            if(followers_count.length){
                var followersCount = parseInt(followers_count.first().text());
                if(followersCount > 0) {
                    followers_count.text(followersCount - 1);
                }
            }
            $container.LoadingOverlay('hide');
        }
    });
}