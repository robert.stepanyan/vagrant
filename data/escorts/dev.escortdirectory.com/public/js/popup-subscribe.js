$(document).ready(function(){

    $('#ajax-subscribe-form').submit(function(e){
		e.preventDefault();
		$('#subscribe-modal .modal-content').LoadingOverlay('show');
		var url = $(this).attr('action') 
		var email = $('#subscribe-email').val();
		$.ajax({
			url: url,
			type: 'POST',
			data:{
				email: email
			},
			success: function(response){
				response = JSON.parse(response);
				$('#subscribe-modal .modal-content').LoadingOverlay('hide');
				if(response.status == 'error'){
					$('.error-box').html(' ')
					for(var key in response.msgs){
						$('.error-box').append('<p>' + response.msgs[key] + '</p>');
					}
					$('.error-box').show();
					return false;
				}
				$('#subscribe-modal').modal('hide');
				$('#subscribe-success-modal').modal('show');
			},
			error: function(response){

			},
		})
	})
});