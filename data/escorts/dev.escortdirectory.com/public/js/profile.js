$( document ).ready(function() {

    // Carousel set clicked image as active in modal
    // ---------------------------------------
    $('.image-slider .image, .image-slider .pic-on-hover').click(function() {
        var id = $(this).data('id');

        $('#modalCarousel-gallery')
            .find('.carousel-item')
            .removeClass('active')
            .end()
            .find('.carousel-item[data-id="' + id + '"]')
            .addClass('active');

    });
    // ---------------------------------------

    // Bottom Panel
    // ------------------------------------------------------
    $('.profile-bottom-panel .pullbar').click(function() {
        $(this).parents('.profile-bottom-panel').toggleClass('open');
        $('body').toggleClass('profile-backdrop-visible');
    });

    $(document).on('click', '.profile-backdrop', function(){
        $('.profile-bottom-panel .pullbar').trigger('click');
    });
    // ------------------------------------------------------

    // PM
    // ------------------------------------------------------
    $('.send-message').on('click', function(e) {
        e.preventDefault();
        var participant = $('#participant').val();
        Sceon.private_message_popup(participant);
    });
    // ------------------------------------------------------

    // Comment
    // ------------------------------------------------------
    $('.add-comment').on('click', function(e){
        e.preventDefault();
        var escort_id = $('#escort_id').val();

        var commentId = $(this).attr('rel') ? $(this).attr('rel') : null ;
        Sceon.add_comment_popup(this, escort_id , commentId);
    });
    // ------------------------------------------------------

    // Follow, Unfollow
    // ------------------------------------------------------
    $(document).on('click', '.follow-me-button', function(e){
        e.preventDefault();
        var escort_id = $('#escort_id').val();
        followAdd(this, escort_id, 'escort');
    });

    $(document).on('click', '.unfollow-me-button', function(e){
        e.preventDefault();
        var escort_id = $('#escort_id').val();
        followRemove(this, escort_id, 'escort');
    });

    function followAdd(el, id, type) {
        if(headerVars.currentUser && headerVars.is_member == false){
            Popup.actionNotAvailable();
        }
        else if(headerVars.is_member){
            $container = $(el).parents('.button-container').first();
            $container.LoadingOverlay("show", {color : 'rgba(244, 245, 247, 0.44)'});

            var data = {
                user_type: type,
                user_type_id: id,
                follow_type: 0
            };

            $.ajax({
                url: lang_id + '/follow/add?id=' + id + '&type=' + type,
                type: 'POST',
                data: data,
                success: function (resp) {
                    resp = jQuery.parseJSON(resp);
                    if ( resp.status == 'success' ) {
                        var unfollowButton = '<a data-newletter-popup="off" class="follow-button unfollow-me-button">' +
                                    '<div class="btn btn-small btn-brown follow">UNFOLLOW</div></a>';

                        var mobile_unfollowButton = '<a class="action-button unfollow-me-button">UNFOLLOW</a>';

                        $('.follow-me-button').each(function () {
                            if ($(this).parent().hasClass('mobile')) {
                                $(this)
                                    .before(mobile_unfollowButton)
                                    .remove();
                            } else {
                                $(this)
                                    .before(unfollowButton)
                                    .remove();
                            }
                        });

                        var followers_count = $('.followers-number-container');
                        if(followers_count.length){
                            var followersCount = parseInt(followers_count.first().text());
                            followers_count.text(1 + followersCount);
                        }
                    }
                    $container.LoadingOverlay('hide');
                }
            });

        }
        else{
            showFollowModal(type, id);
        }
    };

    function followRemove(el, id, type) {
        var $container = $(el).parents('.button-container').first();
        $container.LoadingOverlay("show", {color : 'rgba(244, 245, 247, 0.44)'});

        var data = {
            type: type,
            type_id: id
        };

        $.ajax({
            url: '/follow/remove',
            type: 'POST',
            data: data,
            success: function (resp) {
                var followButton = '<a data-newletter-popup="off" class="follow-button follow-me-button">'+
                                    '<div class="btn btn-small btn-brown follow">FOLLOW</div></a>';

                var mobile_followButton = '<a class="action-button follow-me-button">FOLLOW</a>';

                $('.unfollow-me-button').each(function () {
                    if ($(this).parent().hasClass('mobile')) {
                        $(this)
                            .before(mobile_followButton)
                            .remove();
                    } else {
                        $(this)
                            .before(followButton)
                            .remove();
                    }
                });

                var followers_count = $('.followers-number-container');
                if(followers_count.length){
                    var followersCount = parseInt(followers_count.first().text());
                    if(followersCount > 0) {
                        followers_count.text(followersCount - 1);
                    }
                }

                $container.LoadingOverlay('hide');
            }
        });
    }
    // ------------------------------------------------------

    // Full Review
    // ------------------------------------------------------
    function fullReviewTrigger() {
        $(document).on("click", ".full-review-trigger", function(){
            var review_wrap = $("#modalShowReview");
            var review_id = $(this).data('id'),
            escort_id = $(this).data('escort-id'),
            escort_showname = $(this).data('escort-showname');

             $.ajax({
                 url: '/reviews/' + escort_showname + '-' + escort_id + '/' + review_id,
                 type: 'GET',
                 data: {'ajax': 1, 'escort_id':escort_id, 'review_id': review_id, 'showname': escort_showname },
                 beforeSend: function(){
                    $('.profile').LoadingOverlay('show');
                 },
                 success: function (resp) {
                    review_wrap.html(resp);
                    review_wrap.modal("show");

                    grading($('.looks'), 'looks');
                    grading($('.services'), 'service');

                    $('.profile').LoadingOverlay('hide');
                 }
             })
        });
    }
    function loadMore() {
        $(document).on('click', '.load_more', function (e) {
           e.preventDefault();
           if(!$('.additional_reviews').length){
               $(this).html('no more reviews').css('pointer-events', 'none');
               return false;
           }
           $(this).addClass('d-none');
           $('.more_wrapper').removeClass('d-none');
           $('.load_less').removeClass('d-none');
        });

        $(document).on('click', '.load_less', function (e) {
            e.preventDefault();
            $(this).addClass('d-none');
            $('.more_wrapper').addClass('d-none');
            $('.load_more').removeClass('d-none');
        });
    }

    fullReviewTrigger();
    loadMore();
    // ------------------------------------------------------

    // Load Reviews
    // ------------------------------------------------------
    var page = 1;
    loadReviews(page);
    $('.pgn').on('click', function(e){
        e.stopPropagation();
        page++;
        loadReviews(page);
    });

    function loadReviews(page) {
        if (!$('#p-review'))
        {
            return;
        }

        if (!$('#escort_id'))
        {
            return;
        }

        var escort_id = $('#escort_id').val();
        $('.reviews').LoadingOverlay("show", {color : 'rgba(244, 245, 247, 0.44)'});

        var data = {
            escort_id: escort_id
        };

        if (page)
            data.page = page;

        $.ajax({
            url: lang_id + '/reviews/profile-reviews',
            type: 'GET',
            data: data,
            success: function (resp) {
                if(page == 1) {
                    $('#p-review').html(resp);
                    $('#r_count').text($('#reviews_count').val());
                } else {
                    $('#p-review').append(resp);
                }

                grading($('.review-looks'), 'looks');
                grading($('.review-services'), 'service');

                var reviews_count = $('#reviews_count').val();
                var reviews_view_count = $('#p-review .box-container').length;

                if(reviews_count == reviews_view_count) {
                    $('.pgn').hide();
                }

                $('.reviews').LoadingOverlay('hide');
            }
        });
    };
    // ------------------------------------------------------

    // Load Comments
    // ------------------------------------------------------
    var page_com = 1;
    loadComments(page_com);
    $('.pgn-c').on('click', function(e){
        e.stopPropagation();
        page_com++;
        loadComments(page_com);
    });

    function loadComments(page_com) {
        if (!$('#p-comment'))
        {
            return;
        }

        if (!$('#escort_id'))
        {
            return;
        }

        var escort_id = $('#escort_id').val();

        $('.comments').LoadingOverlay("show", {color : 'rgba(244, 245, 247, 0.44)'});

        var data = {
            escort_id: escort_id
        };

        if (page_com)
            data.page = page_com;

        $.ajax({
            url: lang_id + '/comments/profile-comments',
            type: 'GET',
            data: data,
            success: function (resp) {
                if(page_com == 1) {
                    $('#p-comment').html(resp);
                    $('#c_count').text($('#comments_count').val());
                } else {
                    $('#p-comment').append(resp);
                }

                var comments_count = $('#comments_count').val();
                var comments_view_count = $('#p-comment .box-container').length;
                if(comments_count == comments_view_count) {
                    $('.pgn-c').hide();
                }

                $('.comments').LoadingOverlay('hide');
            }
        });
    };
    // ------------------------------------------------------

    if (!isMobile()) {
        if ($(".image-slider").find(".image").length > 5) {
            for (var i = 6; i <= $(".image-slider").find(".image").length; i++) {
                $(".image-slider").find(".image:nth-child("+ i +")").css("display","none");
            }
            $(".image-slider").find(".image:nth-child("+ 5 +")").addClass("last").append("<span>+" + ($(".image-slider").find(".image").length - 5) + "</span>");
        }
    } else {
        $(".image-slider").each(function(){
            var x = $(this).find(".image");
            $(this).find(".image-bullets").append("<span class='bullet active'></span>");
            for (i = 1; i < x.length; i++) {
               x[i].style.display = "none";
               $(this).find(".image-bullets").append("<span class='bullet'></span>");
            }
        });

        $(".section-title").parent(".box-section").on("click",function(){
            $(this).find(".mobile-container").stop(true, true).slideToggle();
            if ($(this).find(".section-title > i").hasClass("fa-plus")) {
                $(this).find(".section-title > i").removeClass("fa-plus").addClass("fa-minus");
            } else {
                $(this).find(".section-title > i").removeClass("fa-minus").addClass("fa-plus");
            }
        });

        focusSection();

        $('.carousel').bcSwipe({ threshold: 50 });

        $(".mobile-useless-menu-trigger").on("click",function(){
            $(".mobile-useless-menu").show();
        });

        $(".close-menu").on("click",function(){
            $(".mobile-useless-menu").hide();
        });
    }


    // Init Datepicker
    // ------------------------------------------------------
    function init_date_picker() {
        $('.date-picker').each(function() {
            $(this).datepicker({
                format: 'dd M yyyy',
                autoclose: true,
            });
        })
        .on('show', function() {
            $('.modal').scroll(function() {
                $('.date-picker').blur().datepicker("hide");
            });
        })
    }
    // ------------------------------------------------------

    // Reviews part
    $(".write_review").on("click",function(){

        var $modal = $("#modalCreateReview");

        // This dumb logic makes the modal static
        // so even if you click on backdrop, it wont close
        // -----------------------------------------------------
        var keyboard = false; // Prevent to close by ESC
        var backdrop = 'static'; // Prevent to close on click outside the modal

        if(typeof $modal.data('bs.modal') === 'undefined') { // Modal did not open yet
            $modal.modal({
                keyboard: keyboard,
                backdrop: backdrop
            });
        } else { // Modal has already been opened
            $modal.data('bs.modal')._config.keyboard = keyboard;
            $modal.data('bs.modal')._config.backdrop = backdrop;

            if(keyboard === false) {
                $modal.off('keydown.dismiss.bs.modal'); // Disable ESC
            } else { //
                $modal.data('bs.modal').escape(); // Resets ESC
            }
        }
        // -----------------------------------------------------

        var escort_id = $('.write_review').attr('data-escort-id');

        if(!headerVars.currentUser) {
            return Popup.login('#login-modal');
        }else {
            if(headerVars.is_member == true) {
                $.ajax({
                    url: '/reviews/add-review-popup/?escort_id=' + escort_id,
                    type: 'GET',
                    beforeSend: function(){
                        $modal.LoadingOverlay('show');
                    },
                    success: function (resp) {
                        $modal.modal("show");
                        $modal.html(resp);
                        $modal.LoadingOverlay('hide');
                        autocomplete(document.getElementById("countryInput"));
                        rerun();
                        submit();
                        reload();
                        init_date_picker();
                    }
                })
            }else{
                Popup.actionNotAvailable();
            }
        }
    });




   function reload() {
       $('.reload').on('click', function () {

           var escort_id = $('.write_review').attr('data-escort-id');
           $.ajax({
               url: '/reviews/add-review-popup/?escort_id=' + escort_id,
               type: 'GET',
               beforeSend: function(){
                   $('#modalCreateReview').LoadingOverlay('show');
               },
               success: function (resp) {
                   $('#modalCreateReview').html(resp);
                   $('#modalCreateReview').LoadingOverlay('hide');
                   autocomplete(document.getElementById("countryInput"));
                   rerun();
                   reload();
                   submit();
               }
           })
       });
   }



    function rerun() {
        var labels = [];
        $('.multiple-dropdown-box').multiselect({

            onChange: function(option, checked, select){
                var index = labels.indexOf($(option).val());
                if (index !== -1) {
                    labels.splice(index, 1);
                } else {
                    labels.push($(option).val());
                }
            },
            buttonText: function(obj) {
                if (labels.length === 0) {
                    return 'Nothing selected';
                }
                else if (labels.length >= 2) {
                    return labels[0]+ ', +' + (labels.length - 1);
                }
                else if(labels.length >= 1){
                    return labels;
                }
            }
        });

        function removeDropdownSelected(obj){
            var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
            $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
                $(this).show();
            });
            $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
                if ($(this).find("a").html() == val) {
                    $(this).hide();
                }
            });
        }

        $(".dropdown-toggle").each(function(){
            removeDropdownSelected(this);
        });

        $(".dropdown-menu li a").on("click", function(){
            var val = $(this).html(),
                dataVal = $(this).closest('li').attr('value');
            var type = $(this).parents(".dropdown-menu").attr("data-type");
            $("#" + type).find(".dropdown-toggle-val").html(val);
            $("#" + type).find(".dropdown-toggle-val").attr('data-val', dataVal);
            removeDropdownSelected(this);
        });


        $('.looks_rate li a').on('click', function () {
            var num = $(this).find('i.active').length;
            $('#filter001 .dropdown-toggle-val').attr('data-looks', num);
        });

        $('.services_rate li a').on('click', function () {
            var num = $(this).find('i.active').length;
            $('#filter002 .dropdown-toggle-val').attr('data-services', num);
        });






    }


    rerun();
    submit();
    reload();
   function submit() {


       $('.submit_review').on('click', function () {
           $('.err_message').html('');
           var escort_id = $('.write_review').attr('data-escort-id'),
               meeting_date = $('#meeting-date-input-basic').val(),
               city_id = $('#city_id').val(),
               meeting_place = $('#filter_9 .dropdown-toggle-val').attr('data-val'),
               quality = $('#filter03 .dropdown-toggle-val').attr('data-val'),
               duration = $('#duration').val(),
               duration_unit = $('#filter01 .dropdown-toggle-val').attr('data-val'),
               price = $('#price').val(),
               currency = $('#filter02 .dropdown-toggle-val').attr('data-val'),
               looks_rate = $('#filter001 .dropdown-toggle-val').attr('data-looks'),
               services_rate = $('#filter002 .dropdown-toggle-val').attr('data-services'),
               s_kissing = $('#filter_23 .dropdown-toggle-val').attr('data-val'),
               s_blowjob = $('#filter_24 .dropdown-toggle-val').attr('data-val'),
               s_cumshot = $('#filter_25 .dropdown-toggle-val').attr('data-val'),
               s_69 = $('#filter_26 .dropdown-toggle-val').attr('data-val'),
               s_anal = $('#filter_27 .dropdown-toggle-val').attr('data-val'),
               s_sex = $('#filter_29 .dropdown-toggle-val').attr('data-val'),
               s_multiple_times_sex = $('#filter_30 .dropdown-toggle-val').attr('data-val'),
               review = $('#description_meeting').val(),
               s_breast = $('#filter_28 .dropdown-toggle-val').attr('data-val'),
               s_conversation = $('#filter_31 .dropdown-toggle-val').attr('data-val'),
               s_availability = $('#filter_32 .dropdown-toggle-val').attr('data-val'),
               s_photos = $('#filter_33 .dropdown-toggle-val').attr('data-val'),
               t_user_info = $('#member_Input').val(),
               t_meeting_date = $('#meeting-date-input-trusted').val(),
               hrs = $('#filter_35 .dropdown-toggle-val').attr('data-val'),
               min = $('#filter_36 .dropdown-toggle-val').attr('data-val'),
               t_meeting_duration = $('#meetingDuration_Input').val(),
               t_meeting_place = $('#meetingPlace_Input').val(),
               t_comments = $('#commentsInput').val(),
               duration_unit_2 = $('#filter_34 .dropdown-toggle-val').attr('data-val'),
               services_comments = $('#description_services').val(),
               data = {
                   'm_date': meeting_date,
                   'city_id': city_id,
                   'meeting_place': meeting_place,
                   'quality': quality,
                   'duration': duration,
                   'duration_unit': duration_unit,
                   'price': price,
                   'currency': currency,
                   'looks_rate': looks_rate,
                   'services_rate': services_rate,
                   's_kissing': s_kissing,
                   's_blowjob': s_blowjob,
                   's_cumshot': s_cumshot,
                   's_69': s_69,
                   's_anal': s_anal,
                   's_sex': s_sex,
                   's_multiple_times_sex': s_multiple_times_sex,
                   's_breast': s_breast,
                   'review': review,
                   's_conversation': s_conversation,
                   's_availability': s_availability,
                   's_photos': s_photos,
                   't_user_info': t_user_info,
                   't_meeting_date': t_meeting_date,
                   'hrs': hrs,
                   'min': min,
                   't_meeting_duration': t_meeting_duration,
                   't_meeting_place': t_meeting_place,
                   't_comments': t_comments,
                   'duration_unit_2': duration_unit_2,
                   'services_comments': services_comments,
               };

          $.ajax({
               url: '/reviews/add-review/?escort_id=' + escort_id,
               type: 'POST',
               data: data,
               beforeSend: function () {
                   $('#modalCreateReview').LoadingOverlay('show');
               },
               success: function (resp) {
                   resp = JSON.parse(resp);
                   if (resp.status == 'error') {
                       $('.err_message').removeClass('d-none');
                       for (var key in resp.msg) {
                           $('.err_message').append("<span>" + resp.msg[key] + "</span>" + "<br>");
                        }

                       $('#modalCreateReview').LoadingOverlay('hide');
                       return false;
                   } else {
                       // location.reload();
                       $( ".close-modal" ).trigger( "click" );
                       $('#modalCreateReview').LoadingOverlay('hide');
                       $('.add-review-success').removeClass('d-none');
                       setTimeout(function () {
                           $('.add-review-success').addClass('d-none');
                       },3000)
                   }

               },


           })
       });
   };


    // End of Reviews part


});

// Video
// ------------------------------------------------------
function ajaxEscortListVideo(escort_id, video_id ,image, prop, from_slide)
{
    $.ajax({
        url: '/index/ajax-play-video?escort_id=' + escort_id + '&video_id=' + video_id,
        type: 'GET',
        dataType: "json",
        success: function (resp) {
            prop = prop.split('-');
            play(escort_id, image, prop[0], prop[1], true, resp, from_slide);
        }
    });
};

function play(escortId, image, width, height, auto, source, from_slide)
{
    if($(window).width() < 768) {
        var height = height/width*($(window).width()*80/100);
        var modal_width = '80%';
        var width = '100%';
        var isMobile = true;
    } else {
        var width = modal_width = width / height * 360;
        var height = 360;
        var isMobile = false;
    }

    if(from_slide && !isMobile) {
        $('#modalCarousel-gallery').modal('hide');
    }

    if(from_slide && isMobile) {
        $('#modal-video').css({"background-color": "#000"});
    }

    $('#video-jw').parent('.modal-content').css({"width": modal_width, "height": height, "margin": "0 auto"});
    $('#modal-video').modal('show');

    var player = jwplayer('video-jw').setup({
        height: height,
        width: width,
        image: image,
        logo: {
            file: "/images/video_logo.png?v2"
        },
        skin: {
            name: "bekle"
        },
        sources: source
    });

    player.on('ready', function () {
        player.play();
    });

    if(from_slide) {
        $('#modal-video').on('hidden.bs.modal', function (e) {
            jwplayer('video-jw').stop();
            $('#modalCarousel-gallery').modal('show');
        });
    } else {
        $('#modal-video').on('hidden.bs.modal', function (e) {
            jwplayer('video-jw').stop();
            $('#modalCarousel-gallery').modal('hide');
        });
    }
};
// ------------------------------------------------------

// grading
// ------------------------------------------------------
function grading(elm, data) {
    var pnt = 0;
    elm.each(function(i, el){
       if($(this).length){
            pnt = $(this).data(data);
            pnt = Math.round(pnt/2);
        }

        for(var j=0; j<pnt; j+=1) {
            $(this).find('i').eq(j).addClass('active');
        }
    });
}
// ------------------------------------------------------

function isMobile(){
    var newWindowWidth = $(window).width();
    return (newWindowWidth < 768);
}

function reportPhoto(escortId, photoId) {
    console.log(escortId);
    console.log(photoId);
    // Popup.reportPic(escort_id);
}

function blockMember(escortId, photoId) {
    console.log(escortId);
    console.log(photoId);
    // type = this.getAttribute('type');
    // Popup.reportEscort(escort_id,type);
}

function votePhoto(escortId, photoId) {
    console.log(escortId);
    console.log(photoId);
}

function focusSection (hash) {

    if(!hash) hash = window.location.hash.substr(1);

    if(hash.length) {

        var $secion = $('[data-section="'+hash+'"]');

        if($secion.length) {
            var h = $secion.offset().top;
            $('html, body').animate({scrollTop: h}, 300);

            $secion.trigger('click');
        }
    }
}
