var Popup = (function () {

    var templates = {

    };

    var send_login_request = function (e) {

        e.preventDefault();
        var $form = $(e.target),
            data = $(this).serializeArray();

        data.push({name: 'ajax', value: 'true'});
        $form.LoadingOverlay('show');

        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: data,
            dataType: "JSON",
            success: function (response) {

                if (typeof response == 'object' && response['status'] == 'error') {
                    $('.ajax-login-response').empty();
                    $.each(response['msgs'], function (key, value) {
                        $('.ajax-login-response').append('<div class=\"alert alert-danger\" >' + value + '</div>');
                    });
                }
                if (response['status'] == 'success') {
                    $('.ajax-login-response').html('<div class=\"alert alert-success\" >' + headerVars.dictionary.login_success + '</div>');
                    window.location.reload();
                } else {
                    $form.LoadingOverlay('hide');
                }

            }
        }).done(function () {
            $form.LoadingOverlay('hide');
        });
    };

    var login = function (modal_id) {
        if (modal_id === undefined) modal_id = '#myModal';
        $(modal_id)
            .find('.error-box').empty().hide().end()
            .modal('show');

    };

    var send_private_message = function (e) {

        e.preventDefault();
        var $form = $(e.target),
            $container = $form.parents('.modal-content'),
            data = $form.serializeArray();

        $container.LoadingOverlay('show');

        $.ajax({
            url: lang_id + $form.attr('action'),
            method: 'POST',
            data: data,
            dataType: 'JSON',
            success: function (response) {
                if (response.status == 'error') {
                    $('.error-box').empty();
                    $.each(response['msgs'], function (key, value) {
                        $('.error-box').append('<div class=\"alert alert-danger\" >' + value + '</div>');
                    });
                    $('.error-box').show();

                } else {
                    $('#pm-form').get(0).reset();
                    $('.error-box').hide();
                    $('.modal.show').modal('hide');
                    Notify.alert('success', headerVars.dictionary.message_sent_success);
                }
            }
        }).done(function () {
            $container.LoadingOverlay('hide');
        });
    };

   var pm = function (participant_id, modal_id) {

        var action = lang_id + '/private-messaging/send-message-from-profile';
        if (modal_id === undefined) modal_id = '#myModal';
        $(modal_id)
            .find('form')
                .find('input#participant')
                    .val(participant_id)
                    .end()
                .off('submit')
                .on('submit', send_private_message)
                .attr('action', action)
            .end()
            .modal('show');

    };

    var send_comment_xhr = function (e) {
        e.preventDefault();

        var $form = $(e.target),
            $container = $form.parents('.modal-content'),
            $error_box = $container.find('.error-box'),
            data = $form.serializeArray();

        $container.LoadingOverlay('show');

        $.ajax({
            url: lang_id + $form.attr('action'),
            method: 'POST',
            data: data,
            dataType: 'JSON',
            success: function (response) {
                if (response.status != 'success') {

                    $error_box.empty();
                    if($.isArray(response)) {
                        response.forEach(function(val) {
                            $error_box.append('<div class=\"alert alert-danger\" >' + val + '</div>');
                        });
                        $error_box.show();
                    }
                } else {
                    $form.get(0).reset();
                    grecaptcha.reset();
                    $error_box.hide();
                    $('.modal.show').modal('hide');
                    Notify.alert('success', headerVars.dictionary.your_comment_sent);
                }
            }
        }).done(function () {
            $container.LoadingOverlay('hide');
        });
    };

    var add_comment = function (pos_el,escortId, isReply) {

        var url = lang_id + '/comments/ajax-add-comment?escort_id=' + escortId;
        if(isReply){
            url += "&comment_id=" + isReply;
        }

        $('#add-comment-modal')
            .find('#add-comment-form')
                .attr('action', url)
                .off('submit')
                .on('submit', send_comment_xhr)
                .end()
            .modal('show');

    };

    var actionNotAvailable = function() {
        $('#action-not-availble-modal').modal('show');
    };

    return {
        login: login,
        pm: pm,
        actionNotAvailable: actionNotAvailable,
        add_comment: add_comment
    }

})();
