// var Cities = new Sceon.Filter.Cities($('#citySearch'), $('#matchedCities'));
//var Escort = new Sceon.Filter.Escorts($('.filters'));

var NewestEscortsPage = (function() {

    function init() {
        setCheckboxVisibilityFilters();
    }

    function setCheckboxVisibilityFilters() {
        var availableCheckboxes = $('#availableCheckboxes').val();

        if(availableCheckboxes) {
            try {
                availableCheckboxes = JSON.parse(availableCheckboxes);

                availableCheckboxes.forEach(function(meta) {
                    var $checkBox = $('#' + meta['name']);

                    if($checkBox.length && meta['isVisible'] == 1) {
                        $checkBox.parents('.x1-box').show();
                    }else{
                        $checkBox.parents('.x1-box').hide();
                    }
                });
            }catch(e) {
                console.log(e);
            }
        }
    }

    return {
        init: init,
        setCheckboxVisibilityFilters: setCheckboxVisibilityFilters
    }
})();

var Home_pagination = (function () {

    function get_static_page_key() {
        var static_page_key = '';
        var static_pages = ['citytours', 'online-escorts', 'newest'];

        static_pages.forEach(function (key) {
            if (window.location.href.includes(key) && !window.location.href.includes('sort=' + key)) {
                static_page_key = key;
            }
        });
        return static_page_key;
    }

    function Home_pagination() {
    };

    Home_pagination.prototype = {

        keep_loadbar_into_view: function (keep) {

            var focus = function () {
                var ov = $('.loadingoverlay');
                var y = $(document).scrollTop();
                var advSearch_height = $('#advanced-search-form .advanced-search-container-top').height();
                ov.css('background-position-y', (y + $(window).outerHeight() / 5 - advSearch_height) + "px")
            };

            if (keep) {
                $(window).on('scroll', focus);
                focus();
            } else {
                $(window).off('scroll');
            }
        },

        init: function () {
            this.set_pagination_logic();
            this.bind_events();
            this.init_current_page(); // This is only for specific pages
        },

        init_current_page: function() {
            var page = get_static_page_key();

            switch (page) {
                case 'newest':
                    NewestEscortsPage.init();
                    break;
            }
        },

        on_advanced_search_trigger: function (e) {
            e.stopPropagation();

            var is_mobile = Sceon.isMobile();
            var form_id = 'advanced-search-form';
            if (is_mobile)
                form_id = 'advanced-search-form-mobile';


            var $elm = $(this),
                $inp = $(this).find('input'),
                target_name = $inp.data('target'),
                $target = $('#' + form_id + ' input[name=' + target_name + ']');

            $target.val($inp.val());
            if (is_mobile)
                $('#advanced-search-form-mobile').trigger('submit');
            else
                $('#autocomplete-search').trigger('click');

        },

        resetForm: function () {
            var advancedSearchForm = $('#advanced-search-form');
            var advancedSearchFormMobile = $('#advanced-search-form-mobile');
            var filterCheckboxes = $('.filters .filter-container');

            [advancedSearchForm, advancedSearchFormMobile, filterCheckboxes].forEach(function ($container) {
                $container.find('.multiple-dropdown-box').multiselect('clearSelection');
                $container.find('input[type="checkbox"]:checked').prop('checked', false);
                $container.find('input[type="radio"]:checked').prop('checked', false);
                $container.find('input[type="text"]').val('');
            });
        },

        toggleAdvancedSearch: function (e = null) {

            return new Promise(function (resolve) {
                if (e) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }

                var txt = headerVars.dictionary.advanced_search;

                var btn = $(".advanced-search");
                btn.toggleClass('show');
                if (btn.hasClass('show')) {
                    txt = headerVars.dictionary.basic_search
                }
                btn.text(txt);
                $(".advanced-search-container").stop(true, true).slideToggle('slow', function () {
                    resolve();
                });

                if (ismobile()) {
                    $(".advanced-buttons").stop(true, true).toggleClass("active");

                    if ($(".advanced-search").hasClass('show')) {
                        $('.filters-mobile ').animate({scrollTop: 350}, 300)
                    }
                }
            });
        },

        cancel_advanced_search: function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            this.resetForm();
            this.toggleAdvancedSearch();
        },

        sorting_from_native_device: function (e) {
            e.stopPropagation();

            var $target = $('#advanced-search-form-mobile input[name=sort]');

            $target.val($(this).val());
            $('#advanced-search-form-mobile').trigger('submit');

        },

        bind_events: function () {
            $(document).on('click', '.advanced-search-trigger li', this.on_advanced_search_trigger);
            $(document).on('change', '#ordering-native-select', this.sorting_from_native_device);
            $("#reset,#reset-mob").on('click', this.resetForm);
            $(".advanced-search").on("click", this.toggleAdvancedSearch);
            $("#cancel").on("click", this.cancel_advanced_search.bind(this));
        },

        set_pagination_logic: function () {

            if (Sceon.isMobile()) {
                this.run_mobile(); // Pagination with scrolling
            } else {
                this.run_desktop(); // Simple pagination
            }
        },

        run_mobile: function () {

            $('.scroll-page').appear();
            var is_loading = false;

            $(document).on('appear', '.scroll-page', function (e, $affected) {

                if (is_loading) return console.warn("Getting next page escorts");

                var $form = $('form#advanced-search-form'),
                    data = get_mobile_form_data(),
                    url = generatePageLink().replace(/^\/|\/$/g, ''),
                    $active_link = $('.pagination a.active'),
                    page = $active_link.next('[data-pjax=1]').data('page');

                if (!page) return console.error("Page was not found");
                data.push({name: 'page', value: page});
                var static_page_key = get_static_page_key();

                $.pjax({
                    container: '#pjax-container .escorts-list',
                    timeout: 99999000, //	ajax timeout in milliseconds after which a full refresh is forced
                    push: true, //	use pushState to add a browser history entry upon navigation
                    replace: false, //	replace URL without adding browser history entry
                    maxCacheLength: 20, //	maximum cache size for previous container contents
                    scrollTo: false, //	vertical position to scroll to after navigation. To avoid changing scroll position, pass false.
                    type: "GET",
                    dataType: "html",
                    url: '/' + static_page_key + url,
                    data: data,
                    clear_container: false, // Append next page to previous
                    mutate_contents: function (contents) {

                        var old_pagination = $('#pjax-container').find('.pagination-container'),
                            new_pagination = $(contents).find('.pagination-container');

                        old_pagination.replaceWith(new_pagination)

                        return $(contents).find('.escorts-list').html();
                    }
                });

            });

            $(document).on({
                'pjax:beforeSend': function (xhr, options) {
                    $('.scroll-page').LoadingOverlay("show", {
                        color: 'rgba(244, 245, 247, 0.44)'
                    });

                    $('#pjax-container').LoadingOverlay("show", {
                        color: 'rgba(244, 245, 247, 0.44)'
                    });
                    is_loading = true;
                },

                'pjax:error': function (xhr, textStatus, error, options) {
                    $('.scroll-page').LoadingOverlay("hide");
                    $('#pjax-container').LoadingOverlay("hide");
                    console.error(error);
                    is_loading = false;
                },

                'pjax:success': function (event, data, status, xhr, options) {
                    $('.scroll-page').LoadingOverlay("hide");
                    $('#pjax-container').LoadingOverlay("hide");
                    is_loading = false;
                    Cubix.initBanners();
                }
            });

            function dataExists(data, key) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].name === key) {
                        return i;
                    }
                }
                return false;
            }

            function get_mobile_form_data() {
                var data = [];
                var useless = ['lookingFor', 'interestedIn', 'escortsType', 'countryInput-mobile', 'region_id', 'city_id', 'country_id', 'zone_id'];
                $('.filters-mobile').find('input[type="checkbox"]:checked, select[name], input[type="radio"]:checked, input[type=hidden], input[type=text]').not('[disabled]').each(function (key, input) {
                    var name = $(input).attr('name');
                    var value = $(this).val();
                    if (name && !useless.includes(name) && (value && value.length) && !dataExists(data, name)) {
                        if ($.isArray(value)) {
                            value.forEach(function (v) {
                                data.push({
                                    name: name,
                                    value: v
                                });
                            });
                        } else {
                            data.push({
                                name: name,
                                value: value
                            });
                        }
                    }
                });

                data.push({
                    name: 'is_location_cleared',
                    value: $('input[name="is_location_cleared"]').val()
                });

                return data;
            }

            function getGender() {
                var category = $(".lookingFor-mobile .btn-dropdown-rounded.active input[name='interestedIn']").val() || '';
                category = category.split('_');
                var result = {};

                if(!category[0]) {
                    return null;
                }

                result.title = category[0];
                result.id = $("input[name='interested_in'][checked]").data('id');

                return result;
            }

            function getEscortType() {
                var category = $(".interestedIn-mobile input[name='interestedIn'][checked]").not('[disabled]').val() || '';
                category = category.split('_');
                return category[1] || null;
            }

            function generatePageLink() {
                var gender = getGender();
                var escortType = getEscortType();

                var pTopCategory = $(".escortsType-mobile .btn-dropdown-rounded.active input[name='escortsType']").val();
                var static_page_key = get_static_page_key();

                var country_id = $('#country_id').val();
                var region_id = $('#region_id').val();
                var city_id = $('#city_id').val();
                var zone_id = $('#zone_id').val();

                var url = '';
                var queryParams = [];

                if (!static_page_key) {

                    var location_selected = (country_id || city_id || region_id || zone_id);

                    if (location_selected && !escortType) {
                        escortType = 'escorts';
                    }

                    if (gender && escortType) {
                        // if(location_selected) {
                        // 	url = gender;
                        // 	if (interestedIn_id) {
                        // 		queryParams.push('s_category=' + interestedIn_id)
                        // 	}
                        //
                        // }else{
                        url = (escortType == 'escorts' ? '' : escortType + '-') + gender.title;
                        // }

                    } else if (gender && !escortType) {
                        url = gender.title
                    } else if (!gender && escortType) {
                        url = escortType
                    }

                } else {
                    url = '';

                    if (gender) {
                        queryParams.push('gender=' + gender.id)
                    }
                    if (escortType) {
                        if (city_id || country_id) {
                            url += escortType;
                        } else {
                            queryParams.push('escortType=' + escortType)
                        }
                    }
                }

                if(pTopCategory) {
                    queryParams.push('escortType=' + pTopCategory);
                }

                if (city_id) {
                    url += '-' + $('#city_id').attr('data-slug') + '-' + city_id;
                } else if (country_id) {
                    url += '-' + $('#country_id').attr('data-slug') + '-c' + country_id;
                }

                if (region_id) {
                } else if (zone_id) {
                    queryParams.push('zone_id=' + zone_id);
                }                    queryParams.push('region_id=' + region_id);

                return (queryParams.length > 0) ? url + '?' + queryParams.join('&') : url;
            }

            $(document).on('submit', '#advanced-search-form-mobile', function (event) {
                event.preventDefault();
                var url = $(this).attr('action');
                var form = $(this);
                var data = get_mobile_form_data();
                var url = generatePageLink().replace(/^\/|\/$/g, '');
                var static_page_key = get_static_page_key();

                $.pjax.submit(event, '#pjax-container', {
                    timeout: 9999000, //	ajax timeout in milliseconds after which a full refresh is forced
                    push: true, //	use pushState to add a browser history entry upon navigation
                    replace: false, //	replace URL without adding browser history entry
                    maxCacheLength: 20, //	maximum cache size for previous container contents
                    scrollTo: 0, //	vertical position to scroll to after navigation. To avoid changing scroll position, pass false.
                    // type: "get",//	see $.ajax
                    dataType: "html", //	see $.ajax
                    url: lang_id + '/' + static_page_key + url,
                    method: 'GET',
                    data: data,
                });
                event.preventDefault();
            });
        },

        run_desktop: function () {
            var _self = this;
            $(document).on('click', 'a[data-pjax="1"]', function (event) {
                event.preventDefault();
                var page = $(this).data('page');
                if (!page) return console.error("Page was not found");

                $('.filters').find('#pager-input').val(page);
                $('#autocomplete-search').trigger('click');

            });

            function getSubmitUrl(form) {
                return form.attr('action');
            }

            function dataExists(data, key) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].name === key) {
                        return i;
                    }
                }
                return false;
            }

            function get_advanced_search_data() {
                var data = [];
                $('.filters .filter-container').find('input[type="checkbox"]:checked, select[name], input[type=text], input[type=hidden]').each(function (key, input) {
                    var name = $(input).attr('name');
                    var val = $(this).val();
                    if (name && val && val.length && !dataExists(data, name)) {
                        if ($.isArray(val)) {
                            val.forEach(function (v) {
                                data.push({
                                    name: name,
                                    value: v
                                });
                            });
                        } else {
                            data.push({
                                name: name,
                                value: val
                            });
                        }
                    }
                });

                data.push({
                    name: 'is_location_cleared',
                    value: $('input[name="is_location_cleared"]').val()
                });

                return data;
            }

            function on_escort_fetch_start(params = {}) {
                // This will keep loading gif image in center
                // even if user performs scroll while page is loading
                // -------------------------------------
                _self.keep_loadbar_into_view(true);
                // -------------------------------------

                // Show loading animation
                // --------------------------------------
                $('#pjax-container .escorts-list').LoadingOverlay("show", {
                    color: 'rgba(244, 245, 247, 0.44)'
                });
                // --------------------------------------

                new Promise(function (resolve, reject) {
                    // Close "advance search" if it was open
                    // -----------------------------------------
                    if ($('.advanced-search-container').is(':visible')) {
                        _self.toggleAdvancedSearch().then(resolve);
                    } else {
                        resolve();
                    }
                    // -----------------------------------------

                }).then(function () {
                    // After search scroll to frist row of ESCORTS LIST
                    // -------------------------------------
                    var y = typeof params['scroll-to'] != "undefined" ? params['scroll-to'] : 258;
                    $('html, body').animate({scrollTop: y}, 600);
                    // -------------------------------------
                })
            }

            function on_escort_fetch_end() {
                NewestEscortsPage.setCheckboxVisibilityFilters();
                $('#pjax-container .escorts-list').LoadingOverlay("hide");
                Cubix.initBanners();
            }

            $(document).on('pjax:beforeSend', function (xhr, options) {
                on_escort_fetch_start();
            });

            $(document).on('pjax:error', function (xhr, textStatus, error, options) {
                _self.keep_loadbar_into_view(false);
                $('#pjax-container .escorts-list').LoadingOverlay("hide");
                console.error(error);
            });

            $(document).on('pjax:success', function (event, data, status, xhr, options) {
                _self.keep_loadbar_into_view(false);
                on_escort_fetch_end();
            });

            function getGender() {
                var category = $(".intentions-container input[name='interested_in'][checked]").not('[disabled]').val() || '';
                category = category.split('_');
                var result = {};

                if(!category[0]) {
                    return null;
                }

                result.title = category[0];
                result.id = $("input[name='interested_in'][checked]").data('id');

                return result;
            }

            function getEscortType() {
                var category = $(".intentions-container input[name='interested_in'][checked]").not('[disabled]').val() || '';
                category = category.split('_');
                return category[1] || null;
            }

            function generatePageLink() {
                var gender = getGender();
                var escortType = getEscortType();

                var pTopCategory = $(".intentions-container input[name='escortType']:checked").not('[disabled]').val();
                var interestedIn_id = $(".intentions-container input[name='interested_in']:checked").not('[disabled]').data('id');
                var static_page_key = get_static_page_key();

                var country_id = $('#country_id').val();
                var region_id = $('#region_id').val();
                var city_id = $('#city_id').val();
                var zone_id = $('#zone_id').val();
                var url = '';
                var queryParams = [];

                if (!static_page_key) {

                    var location_selected = (country_id || city_id || region_id || zone_id);

                    if (location_selected && !escortType) {
                        escortType = 'escorts';
                    }

                    if (gender && escortType) {
                        // if(location_selected) {
                        // 	url = gender;
                        // 	if (interestedIn_id) {
                        // 		queryParams.push('s_category=' + interestedIn_id)
                        // 	}
                        //
                        // }else{
                        url = (escortType == 'escorts' ? '' : escortType + '-') + gender.title;
                        // }

                    } else if (gender && !escortType) {
                        url = gender.title
                    } else if (!gender && escortType) {
                        url = escortType
                    }

                } else {
                    url = '';

                    if (gender) {
                        queryParams.push('gender=' + gender.id)
                    }
                    if (escortType) {
                        if (city_id || country_id) {
                            url += escortType;
                        } else {
                            queryParams.push('escortType=' + escortType)
                        }
                    }
                }

                if(pTopCategory) {
                    queryParams.push('escortType=' + pTopCategory);
                }

                if (city_id) {
                    url += '-' + $('#city_id').attr('data-slug') + '-' + city_id;
                } else if (country_id) {
                    url += '-' + $('#country_id').attr('data-slug') + '-c' + country_id;
                }

                if (region_id) {
                } else if (zone_id) {
                    queryParams.push('zone_id=' + zone_id);
                }                    queryParams.push('region_id=' + region_id);

                return (queryParams.length > 0) ? url + '?' + queryParams.join('&') : url;
            }

            function get_static_page_key() {
                var static_page_key = '';
                var static_pages = ['citytours', 'online-escorts', 'newest'];

                static_pages.forEach(function (key) {
                    var wasInGet = window.location.href.includes('sort=' + key);
                    var wasInPath = window.location.pathname.includes(key);
                    if ( wasInPath ) {
                        static_page_key = key;
                    }
                });
                return static_page_key;
            }

            $("#autocomplete-search").on('click', function (e) {
                e.preventDefault();
                var new_link = generatePageLink().replace(/^\/|\/$/g, '');
                var data = get_advanced_search_data();
                var current_link = (location.pathname).replace(/^\/|\/$/g, '');

                var separator = new_link.includes('?') ? '&' : '?';

                if (e.originalEvent !== undefined) {
                    data.forEach(function (item) {
                        if (item.name === 'page')
                            item.value = 1;
                    })
                }

                var request_url = new_link + separator + $.param(data);
                var static_page_key = get_static_page_key();

                // In this case We need to just call PJAX,
                // Because the main filters were not changed
                // ----------------------------------------
                var from_question_mark = ~new_link.indexOf('?') ? new_link.indexOf('?') : new_link.length;
                var compareto = new_link.substring(0, from_question_mark).replace(/^\/|\/$/g, '');

                if (1 || static_page_key || current_link == compareto) {
                    $.pjax({
                        container: '#pjax-container',
                        timeout: 99999000, //	ajax timeout in milliseconds after which a full refresh is forced
                        push: true, //	use pushState to add a browser history entry upon navigation
                        replace: false, //	replace URL without adding browser history entry
                        maxCacheLength: 20, //	maximum cache size for previous container contents
                        scrollTo: false, //	vertical position to scroll to after navigation. To avoid changing scroll position, pass false.
                        type: "GET",
                        dataType: "html",
                        url: lang_id + '/' + static_page_key + request_url,
                    });
                } else {
                    // User changed the main filter, so need to switch to first page
                    // ----------------------------------
                    data.forEach(function (item) {
                        if (item.name === 'page') item.value = 1;
                    });
                    // ----------------------------------
                    var separator = new_link.includes('?') ? '&' : '?';
                    var request_url = new_link + separator + $.param(data);
                    window.location.href = '/' + static_page_key + request_url;

                    on_escort_fetch_start({
                        'scroll-to': 0
                    });
                }
                // ----------------------------------------

            });

            var types_of_fucking_actions = ['change', 'click'];
            types_of_fucking_actions.forEach(function(type) {
                $(document).on(type, '.will-trigger-desktop-search-on-' + type, function() {
                    $('.filters').find('#pager-input').val(1);
                    $('#autocomplete-search').trigger('click');
                });
            });
        }
    };

    return Home_pagination;
})();


$(document).ready(function () {
    if (!$('.btn[data-object="false"]').length) return false;

    var pagination = new Home_pagination();
    pagination.init();


});