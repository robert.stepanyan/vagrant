
$( document ).ready(function() {

    // For one fucking Dropdown
    // ---------------------------------------------------
    $(".dropdown-toggle").each(function () {
        removeDropdownSelected(this);
    });

    $(".dropdown-selection.single-option").find(".dropdown-menu > li > a").on("click", function () {
        if($(this).parents('.dropdown').hasClass('disabled')) return;

        var val = $(this).html();
        $(this).parents(".dropdown-selection").find(".single-option-select").val(val).trigger('change');
    });

    $(document).on("click", ".dropdown-menu li a", function () {
        if($(this).parents('.dropdown').hasClass('disabled')) return;
        if($(this).parent().hasClass('disabled') || $(this).hasClass('disabled')) return;

        var val = $(this).html();
        var type = $(this).parents(".dropdown-menu").attr("data-type");
        $("#" + type).find(".dropdown-toggle-val")
            .html(val)
            .removeClass('pristine');
        $(this).parents('div.dropdown').first().find('select').val($(this).parent().data('val')).trigger('change');
        removeDropdownSelected(this);

    });

    function removeDropdownSelected(obj) {
        var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
        $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function () {
            $(this).show();
        });
        $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function () {
            if ($(this).find("a").html() == val) {
                $(this).hide();
            }
        });
    }
    // ---------------------------------------------------

    $('.toggle_hidden').on('click', function () {
        if($(this).find('img').hasClass('tog_vis')) {
            $(this).find('img').attr('src', '/images/visible_password.png').removeClass('tog_vis');
            $('.pswd_cont input').attr('type', 'text');
        } else {
            $(this).find('img').attr('src', '/images/hidden_password.png').addClass('tog_vis');
            $('.pswd_cont input').attr('type', 'password');
        }

    });

    if($('#resend_email_text')){
        $('#resent_email').on('click', function () {
            $(this).css('pointer-events', 'none');
        })
        setTimeout(function () {

            $('#resend_email_text').remove();
            $('#resent_email').css('display','inline-block');
        },5000)
    };


    $('.ajax-login-form').submit( function(event) {
        event.preventDefault();
        $('.theform').LoadingOverlay("show", {color : 'rgba(244, 245, 247, 0.44)'});
        var data = $(this).serializeArray();
        data.push({name:'ajax',value:'true'});
        data.push({name:'ignore-unverified-email',value:true});

        $.ajax({
            url: '/account/signin/',
            type: 'POST',
            data: data,
            success: function(response){
                try {
                    response = JSON.parse(response);
                    if(typeof response =='object' && response['status'] == 'error' ){
                        $(".shaker input").css('border-color', '#f8d7da').effect( "shake", {times:3}, 500 );
                        $.each( response['msgs'], function( key, value ) {
                            $('.ajax-login-response').html('<div class=\"alert alert-danger\" >'+ value +'</div>');
                        });
                    }
                    if(response['status'] == 'success'){
                        $('.ajax-login-response').html('<div class=\"alert alert-success\" >'+ response['msgs']+'</div>');
                        $('#login_ajax').modal('hide');
                        window.location.href = response['url'].replace(/\/$/g, '');
                    } else{
                        $('.theform').LoadingOverlay('hide');
                    }
                }
                catch (e) {

                } finally {

                }


            }
        })

    });





});

