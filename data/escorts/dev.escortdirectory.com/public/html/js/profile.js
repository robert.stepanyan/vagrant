$( document ).ready(function() {

    $("#submenu-trigger").on("click",function(e){
        e.stopPropagation();
        $(".header-submenu-container").stop(true, true).slideToggle();
    });

     $('html').on("click",function(){
        if ($(".header-submenu-container").css("display") == "block") {
            $("#submenu-trigger").trigger("click");
        }
     });

    if (!ismobile()) {

        $(".escort-details").each(function(){
            
            text = $(this).find("p").html();
            link = $(this).parents(".escort-item").find("a").attr("href");
            len = 260;

            if((text.length > text.substring(0, len).length)){
                text = text.substring(0, len - 3) + '...' + '<a href="' + link + '">Read more</a>';
            }
            
            // return text; 
            $(this).find("p").html(text);
        });

        if ($(".image-slider").find(".image").length > 5) {
            for (var i = 6; i <= $(".image-slider").find(".image").length; i++) {
                $(".image-slider").find(".image:nth-child("+ i +")").css("display","none");
            }
            $(".image-slider").find(".image:nth-child("+ 5 +")").addClass("last").append("<span>+" + ($(".image-slider").find(".image").length - 5) + "</span>");
        }
    } else {
        $(".image-slider").each(function(){
            var x = $(this).find(".image");
            $(this).find(".image-bullets").append("<span class='bullet active'></span>");
            for (i = 1; i < x.length; i++) {
               x[i].style.display = "none";  
               $(this).find(".image-bullets").append("<span class='bullet'></span>");
            }
        });

        $(".section-title").parent(".box-section").on("click",function(){
            $(this).find(".mobile-container").stop(true, true).slideToggle();
            if ($(this).find(".section-title > i").hasClass("fa-plus")) {
                $(this).find(".section-title > i").removeClass("fa-plus").addClass("fa-minus");
            } else {
                $(this).find(".section-title > i").removeClass("fa-minus").addClass("fa-plus");
            }
        });

        $('.carousel').bcSwipe({ threshold: 50 });

        $(".mobile-useless-menu-trigger").on("click",function(){
            $(".mobile-useless-menu").stop(true,true).fadeToggle();
        });

        $(".close-menu").on("click",function(){
            $(".mobile-useless-menu-trigger").trigger("click");
        });
    }

});

function ismobile(){
     var newWindowWidth = $(window).width();

        return (newWindowWidth < 768);
}
    