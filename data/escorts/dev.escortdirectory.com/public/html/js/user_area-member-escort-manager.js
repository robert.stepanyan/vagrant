$( document ).ready(function() {

    $(".edit-trigger").on("click",function(){
        $("#modalEdit").modal("show");
    });

    $(".city-remove").on("click",function(){
        $(this).parents(".other-city").slideUp('slow',function(){
            $(this).remove();
        });
    });

    $(".add-other-city-trigger").on("click",function(){
        var country = $("#countryFilter").find(".dropdown-toggle-val").html();
        var otherCity = $("#otherCitiesFilter").find(".dropdown-toggle-val").html();

        if ((country != "Nothing selected") && (otherCity != "Nothing selected")) {

            var container = `<div class="other-city justify-content-between">
                                <div class="city-name">`+ otherCity +` <span class="text-grey">(`+ country +`)</span></div>
                                <div class="city-remove text-grey d-flex align-items-center">
                                    <p class="uppercase">remove</p>
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>`;

            $(".other-cities-container").append(container);

            $(".city-remove").on("click",function(){
                $(this).parents(".other-city").slideUp('slow',function(){
                    $(this).remove();
                });
            });
        }
    });
   
});

function ismobile(){
     var newWindowWidth = $(window).width();

        return (newWindowWidth < 768);
}

function removeDropdownSelected(obj){
    var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
    $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
        $(this).show();
    });
    $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
        if ($(this).find("a").html() == val) {
            $(this).hide();
        }
    });
}