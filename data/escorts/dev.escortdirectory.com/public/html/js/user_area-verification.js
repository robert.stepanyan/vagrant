$( document ).ready(function() {

    $(".dropdown-selection.single-option").find(".dropdown-menu > li > a").on("click",function(){
        var val = $(this).html();
        $(this).parents(".dropdown-selection").find(".single-option-select").val(val);
    });

    $(".dropdown-toggle").each(function(){
        removeDropdownSelected(this);
    });

    $(".dropdown-menu li a").on("click", function(){
        var val = $(this).html();
        var type = $(this).parents(".dropdown-menu").attr("data-type");
        $("#" + type).find(".dropdown-toggle-val").html(val);
        removeDropdownSelected(this);
    });

    $(".button-tap").on("click",function(){
        $(this).toggleClass("tapped");
    });

    $(".add-photo, .replace-photo").on("click",function(){
        var val = $(this).parents(".picture-container").attr("data-type");

        $("#"+val+"Picture").trigger("click");
    });

    $(".add-video, .replace-video").on("click",function(){
        var val = $(this).parents(".video-container").attr("data-type");

        $("#"+val).trigger("click");
    });

    $("#mainPicture, #secondPicture, #thirdPicture, #forthPicture, extraPicture").on("change",function(){
        var val = $(this).val();
        if (val != "") {
            $(this).parents(".picture-container").addClass("havePicture");
        }
    });

    $("#video").on("change",function(){
        var val = $(this).val();
        if (val != "") {
            $(this).parents(".video-container").addClass("haveVideo");
        }
    });

    $(".next-step").on("click",function(){
        var val = $(this).parents("section").attr("id").split('step').pop();
        var val = parseInt(val);
        var next = val + 1;
        if (stepIsValid(val)) {
            $("#step"+val).fadeOut(100,function(){
                $("#step"+next).fadeIn(150);
            });
        }
    });

    $(".previous-step").on("click",function(){
        var val = $(this).parents("section").attr("id").split('step').pop();
        var val = parseInt(val);
        var previous = val - 1;
        $("#step"+val).fadeOut(100,function(){
            $("#step"+previous).fadeIn(150);
        });
    });

    if ($("#submenu1").parent("li").hasClass("active")) {
        $("#submenu1").addClass("show");
    }

    if (ismobile()) {
        
        $("#submenu-trigger").on("click",function(e){
            e.preventDefault();
            e.stopPropagation();
            $(".user-area").toggleClass("mobile-opened");
            $(".header").toggleClass("fixed");
        });

        $('.left-section').click( function(e) {
            e.stopPropagation();
        });

        $("body").on("click",function(){
            $(".user-area").removeClass("mobile-opened");
            $(".header").removeClass("fixed");
        });
    }
});

function ismobile(){
     var newWindowWidth = $(window).width();

        return (newWindowWidth < 768);
}

function removeDropdownSelected(obj){
    var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
    $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
        $(this).show();
    });
    $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
        if ($(this).find("a").html() == val) {
            $(this).hide();
        }
    });
}

function stepIsValid(step){

    var errors=[];
    $(".errors-container > .box-section").empty();

    switch(step) {

      case 0:
            $(".errors-container").css("display","none");
            return true;
        break;
      case 1:

            if (errors.length == 0) {
                $(".errors-container").css("display","none");
                return true;
            } else {
                $(".errors-container").fadeIn();
                for (i=0; i < errors.length; i++) {
                    $(".errors-container > .box-section").append('<p>'+ (i+1) + '. ' + errors[i] + '</p>')
                }
            }
        break;
      case 2:

            if (errors.length == 0) {
                $(".errors-container").css("display","none");
                return true;
            } else {
                $(".errors-container").fadeIn();
                for (i=0; i < errors.length; i++) {
                    $(".errors-container > .box-section").append('<p>'+ (i+1) + '. ' + errors[i] + '</p>')
                }
            }
        break;
      case 3:

            if (errors.length == 0) {
                $(".errors-container").css("display","none");
                return true;
            } else {
                $(".errors-container").fadeIn();
                for (i=0; i < errors.length; i++) {
                    $(".errors-container > .box-section").append('<p>'+ (i+1) + '. ' + errors[i] + '</p>')
                }
            }
        break;
      case 4:

            if (errors.length == 0) {
                $(".errors-container").css("display","none");
                return true;
            } else {
                $(".errors-container").fadeIn();
                for (i=0; i < errors.length; i++) {
                    $(".errors-container > .box-section").append('<p>'+ (i+1) + '. ' + errors[i] + '</p>')
                }
            }
        break;
      case 5:

            if (errors.length == 0) {
                $(".errors-container").css("display","none");
                return true;
            } else {
                $(".errors-container").fadeIn();
                for (i=0; i < errors.length; i++) {
                    $(".errors-container > .box-section").append('<p>'+ (i+1) + '. ' + errors[i] + '</p>')
                }
            }
        break;
      default:
        // code block
    }

}
    