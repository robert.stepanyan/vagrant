$( document ).ready(function() {

    $("#submenu-trigger").on("click",function(e){
        e.stopPropagation();
        $(".header-submenu-container").stop(true, true).slideToggle();
    });

     $('html').on("click",function(){
        if ($(".header-submenu-container").css("display") == "block") {
            $("#submenu-trigger").trigger("click");
        }
     });

    $(".agency-details").each(function(){
        
        text = $(this).find("p").html();
        link = $(this).parents(".escort-item").find("a").attr("href");

        if (ismobile()) {
            len = 160;
        } else {
            len = 260;
        }

        if((text.length > text.substring(0, len).length)){
            text = text.substring(0, len - 3) + '...' + '<a href="' + link + '">Read more</a>';
        }
        
        // return text; 
        $(this).find("p").html(text);
    });

    if (!ismobile()) {

        $(".image-slider").each(function(){
            var x = $(this).find(".image");
            $(this).find(".image-bullets").append("<span class='bullet active'></span>");
            for (i = 1; i < x.length; i++) {
               x[i].style.display = "none";  
               $(this).find(".image-bullets").append("<span class='bullet'></span>");
            }
        });
        var timer;

        $(".image-slider").hover(function(){
            var myIndex = 0;
            var x = $(this).find(".image");
            var dots = $(this).find(".bullet");
            timer = setInterval(function(){
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none"; 
                    dots[i].className = dots[i].className.replace(" active", ""); 
                }
                myIndex++;
                if (myIndex > x.length) {myIndex = 1}    
                x[myIndex-1].style.display = "block";
                dots[myIndex-1].className += " active";
           },500);
        },function() {
            clearInterval(timer);
        });

        if ($(".escorts-slider").find(".image").length > 7) {
            for (var i = 8; i <= $(".escorts-slider").find(".image").length; i++) {
                $(".escorts-slider").find(".image:nth-child("+ i +")").css("display","none");
            }
            $(".escorts-slider").find(".image:nth-child("+ 7 +")").addClass("last").append("<span>+" + ($(".escorts-slider").find(".image").length - 8) + "</span>");
        }

    } else {

        $(".image-slider").each(function(){
            var x = $(this).find(".image");
            for (i = 1; i < x.length; i++) {
               x[i].style.display = "none";  
            }
        });

        if ($(".escorts-slider").find(".image").length > 4) {
            for (var i = 5; i <= $(".escorts-slider").find(".image").length; i++) {
                $(".escorts-slider").find(".image:nth-child("+ i +")").css("display","none");
            }
            $(".escorts-slider").find(".image:nth-child("+ 4 +")").addClass("last").append("<span>+" + ($(".escorts-slider").find(".image").length - 5) + "</span>");
        }
    }

});

function ismobile(){
     var newWindowWidth = $(window).width();

        return (newWindowWidth < 768);
}
    