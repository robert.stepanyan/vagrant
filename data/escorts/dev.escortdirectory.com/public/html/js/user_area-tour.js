$( document ).ready(function() {

    $(".dropdown-selection.single-option").find(".dropdown-menu > li > a").on("click",function(){
        var val = $(this).html();
        $(this).parents(".dropdown-selection").find(".single-option-select").val(val);
    });

    $(".dropdown-toggle").each(function(){
        removeDropdownSelected(this);
    });

    $(".dropdown-menu li a").on("click", function(){
        var val = $(this).html();
        var type = $(this).parents(".dropdown-menu").attr("data-type");
        $("#" + type).find(".dropdown-toggle-val").html(val);
        removeDropdownSelected(this);
    });

    $(".button-tap").on("click",function(){
        $(this).toggleClass("tapped");
    });

    $(".add-tour-trigger").on("click",function(){
        var city = $("#baseCityFilter").find(".dropdown-toggle-val").text();
        var country = $("#countryFilter").find(".dropdown-toggle-val").text();
        var startDate = $("#startTourDate").find(".dropdown-toggle-val").text();
        var endDate = $("#endTourDate").find(".dropdown-toggle-val").text();
        var phone = $("#phoneNumber").val();
        var email = $("#email").val();

        if ((city != "Nothing selected") && (country != "Nothing selected") && (startDate != "Nothing selected") && (endDate != "Nothing selected") && (phone != "") && (email != "")) {

            var container = `<div class="my-tour justify-content-between" style="display: none;">
                                <div class="tour-name">`+ city +` <span class="text-grey">(`+ country +`)</span> `+ startDate +` - `+ endDate +`, `+ phone +`, `+ email +`</div>
                                <div class="tour-remove text-grey d-flex align-items-center">
                                    <p class="uppercase">remove</p>
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>`;

            if ($(".my-tour").length == 0) {
                $(".tour-container-box").slideDown();
            }

            $(".my-tour-container").append(container);
            $(".my-tour-container").find(".my-tour:last").slideDown();

            $(".tour-remove").on("click",function(){
                var obj = $(this);

                if ($(".my-tour").length-1 == 0) {
                    $(".tour-container-box").slideUp('slow',function(){
                        obj.parents(".my-tour").remove();
                    });
                } else {
                    obj.parents(".my-tour").slideUp('slow',function(){
                        obj.parents(".my-tour").remove();
                    });
                }
            });
        }
    });

    $(".tour-remove").on("click",function(){
        $(this).parents(".my-tour").slideUp('slow',function(){
            $(this).remove();
        });
    });

    if ($("#submenu1").parent("li").hasClass("active")) {
        $("#submenu1").addClass("show");
    }

    if (ismobile()) {
        
        $("#submenu-trigger").on("click",function(e){
            e.preventDefault();
            e.stopPropagation();
            $(".user-area").toggleClass("mobile-opened");
            $(".header").toggleClass("fixed");
        });

        $('.left-section').click( function(e) {
            e.stopPropagation();
        });

        $("body").on("click",function(){
            $(".user-area").removeClass("mobile-opened");
            $(".header").removeClass("fixed");
        });
    }
});

function ismobile(){
     var newWindowWidth = $(window).width();

        return (newWindowWidth < 768);
}

function removeDropdownSelected(obj){
    var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
    $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
        $(this).show();
    });
    $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
        if ($(this).find("a").html() == val) {
            $(this).hide();
        }
    });
}
    