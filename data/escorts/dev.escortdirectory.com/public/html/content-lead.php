<?php 
if ( is_front_page() ) :  ?>
<article <?php post_class('content-lead'); ?>>
	<?php	
	$sticky = get_option( 'sticky_posts' );
	$args = array(
	'posts_per_page' => 3,
	'post__in'  => $sticky,
	'ignore_sticky_posts' => 1,
	);
	$query = new WP_Query( $args );
		$stiicky_posts = get_posts($args);
		$x = count($stiicky_posts);
		$num = 4 - $x;
		if(count($stiicky_posts)<4) {
			$args2 = array(
		'posts_per_page' => $num,
		'post__not_in' => get_option( 'sticky_posts' )
	);
	wp_reset_query();
	$query2 = new WP_Query( $args2 );
	$Not_stiicky_posts = get_posts($args2);
		}
	?>
	<div id="demo">
		<div id="owl-demo2" class="owl-carousel owl-theme">
		<?php foreach($stiicky_posts as $post){?>
		<div class="owl-item">
			<div class="cD" style="position:relative">
			       <a href="<?php echo get_permalink($post->ID);?>">
				       	 <img title="<?php echo get_the_title(get_post_thumbnail_id($post->ID)) ?>" alt="<?php  echo get_post_meta(get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true ); ?>"height="100%" width="100%" id="homepage-big-image" src="<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID));echo $url; ?>" alt="main-photo">
						<!--<div class="carousel-caption">-->
						<div class="divWithTxt" id="text1">
							 <h3 class="h3Custom"><?php echo mb_substr( ($post->post_title),0,250 ); ?></h3>
							 <span class="txtMainImage"><?php the_excerpt(); ?></span>
						</div>
					</a>
			</div>
		</div>
		 <?php } ?>
		 
		 <?php foreach($Not_stiicky_posts as $post){?>
		<div class="owl-item">
			<div class="cD" style="position:relative">
			       <a href="<?php echo get_permalink($post->ID);?>">
				       	 <img title="<?php echo get_the_title(get_post_thumbnail_id($post->ID)) ?>" alt="<?php  echo get_post_meta(get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true ); ?>"height="100%" width="100%" id="homepage-big-image" src="<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID));echo $url; ?>" alt="main-photo">
						<!--<div class="carousel-caption">-->
						<div class="divWithTxt" id="text1">
							 <h3 class="h3Custom"><?php echo mb_substr( ($post->post_title),0,250 ); ?></h3>
							 <span class="txtMainImage"><?php the_excerpt(); ?></span>
						</div>
					</a>
			</div>
		</div>
		 <?php } ?>
		 
		</div>
	</div>	  
</article>


<?php else :?>
    <article <?php post_class('content-lead'); ?>>
	<div class="content-thumb content-lead-thumb">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php
			if (has_post_thumbnail()) {
				the_post_thumbnail('content-single');
			} else {
				echo '<img class="mh-image-placeholder" src="' . get_template_directory_uri() . '/images/placeholder-content-single.jpg' . '" alt="No Picture" />';
			} ?>
		</a>
	</div>
	<?php mh_newsdesk_post_meta(); ?>
	<h3 class="entry-title content-lead-title">
		<a href="<?php echo get_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">
			<?php the_title(); ?>
		</a>
	</h3>
	<div class="content-lead-excerpt">
		<?php the_content(); ?>
		<?php mh_newsdesk_more(); ?>
	</div>
</article>
<?php endif; ?>