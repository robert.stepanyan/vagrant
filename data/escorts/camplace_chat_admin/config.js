module.exports = function(app, express){
	var config = this;
	
	app.configure(function(){
		app.use(express.bodyParser());
		app.use(express.cookieParser('shhhh, very secret'));
		app.use(express.session());
	});
	
	
	app.configure('development', function() {
		
		app.set('mysqlConnection', {
			host : 'localhost',
			user : 'chat',
			password : '8e3_eBd6cAE0',
			database : 'camplace_chat'
		});
		
		app.set('apiOptions', {
			host: '92.240.245.78',
			path : '/Api',
			secretCode : '6537AD5E7DDE8',
			accountId : 10241,
			tokenCacheTime : 1000 * 60 * 60 * 12 //12 hours
		});
		
		app.use(express.errorHandler({ 
			dumpExceptions: true, 
			showStack: true 
		}));
	});
	
	app.configure('production', function() {
		
		app.set('mysqlConnection', {
			host : 'localhost',
			user : 'chat',
			password : '8e3_eBd6cAE0',
			database : 'camplace_chat'
		});
		
		app.set('apiOptions', {
			host: 'scripts.camplace.com',
			path : '/Api',
			secretCode : '6537AD5E7DDE8',
			accountId : 10241,
			tokenCacheTime : 1000 * 60 * 60 * 12 //12 hours
		});
		
		app.use(express.errorHandler());
	});
	
	return config;
};