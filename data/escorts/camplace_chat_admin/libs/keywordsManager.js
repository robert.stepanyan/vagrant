function KeywordsManager(app) {
	
	var dbClient = app.get('mysqlClient');
	
	this.getKeywords  = function(filter, callback)
	{
		filter.sortName = filter.sortName || 'id';
		filter.sortOrder = filter.sortOrder || 'desc';
		filter.page = filter.page || 1;
		filter.perPage = filter.perPage || 15;
		
		var sql = 'SELECT id, keyword, type, is_maskable FROM keywords WHERE 1';
		var countSql = 'SELECT count(id) AS count FROM keywords WHERE 1';
		
		var where = ' ';
		var bind = [];
		
		
		if ( filter.keyword && filter.keyword.length ) {
			where += ' AND keyword LIKE ?';
			bind.push(filter.keyword + '%');
		}
		
		sql += where;
		countSql += where;
		
		sql += ' ORDER BY ' + filter.sortName + ' ' + filter.sortOrder;
		sql += ' LIMIT ' + (filter.page - 1) * filter.perPage + ', ' + filter.perPage;
		
		dbClient.fetchAll(sql, bind, function(result) {
			dbClient.fetchRow(countSql, bind, function(countResult) {
				countResult = countResult ? countResult : 0;
				callback(result, countResult.count);
			});
		});
	}
	
	this.remove = function(id, callback)
	{
		var sql = 'DELETE FROM keywords WHERE id = ?';
		
		dbClient.query(sql, [id], function(result) {
			callback();
		});
	}
	
	this.get = function(id, callback)
	{
		var sql = 'SELECT * FROM keywords WHERE id = ?';
		
		dbClient.query(sql, [id], function(error, result) {
			callback(result[0]);
		});
	}
	
	this.update = function(data, callback)
	{
		var sql = 'UPDATE keywords SET keyword = ?, type = ?, is_maskable = ? WHERE id = ?';
		
		dbClient.query(sql, [data.keyword, data.type, data.is_maskable, data.id], function(error, result) {
			if ( ! error ) {
				callback(result);
			} else {
				console.log(error);
			}
		});
	}
	
	this.add = function(data, callback)
	{
		var sql = 'INSERT INTO keywords (keyword, type, is_maskable, creation_date) VALUES(?, ?, ?, NOW())';
		dbClient.query(sql, [data.keyword, data.type, data.is_maskable], function(error, result) {
			if ( ! error ) {
				callback(result);
			} else {
				console.log(error);
			}
		});
	}
	
	
	this.getMatches = function(filter, callback)
	{
		filter.sortName = filter.sortName || 'cm.date';
		filter.sortOrder = filter.sortOrder || 'desc';
		filter.page = filter.page || 1;
		filter.perPage = filter.perPage || 15;
		
		var sql = 'SELECT km.id, km.sender_user_id, km.participant_user_id, km.keyword_id, k.keyword, cm.body AS message, DATE_FORMAT(cm.date, "%d %b %Y %H:%i") AS date, km.is_checked FROM keyword_matches AS km INNER JOIN keywords k ON k.id = km.keyword_id INNER JOIN chat_messages AS cm ON km.message_id = cm.id  WHERE 1';
		var countSql = 'SELECT count(km.id) AS count FROM keyword_matches AS km INNER JOIN keywords k ON k.id = km.keyword_id INNER JOIN chat_messages AS cm ON km.message_id = cm.id  WHERE 1';
		
		var where = ' ';
		var bind = [];
		
		
		if ( filter.keyword && filter.keyword.length ) {
			where += ' AND k.keyword LIKE ?';
			bind.push(filter.keyword + '%');
		}
		if ( filter.isChecked.length > 0 ) {
			where += ' AND km.is_checked = ?';
			bind.push(filter.isChecked);
		}
		
		if ( filter.dateFrom ) {
			where += ' AND UNIX_TIMESTAMP(cm.date) > ?';
			bind.push(filter.dateFrom.getTime() / 1000);
		}
		if ( filter.dateTo ) {
			where += ' AND UNIX_TIMESTAMP(cm.date) < ?';
			bind.push(filter.dateTo.getTime() / 1000);
		}
		
		sql += where;
		countSql += where;
		
		sql += ' ORDER BY ' + filter.sortName + ' ' + filter.sortOrder;
		sql += ' LIMIT ' + (filter.page - 1) * filter.perPage + ', ' + filter.perPage;
		
		dbClient.fetchAll(sql, bind, function(result) {
			dbClient.fetchRow(countSql, bind, function(countResult) {
				countResult = countResult ? countResult : 0;
				callback(result, countResult.count);
			});
		});
	}
	
	
	this.markMatchAsChecked = function(id, adminUserId, callback)
	{
		dbClient.query('UPDATE keyword_matches SET is_checked = 1, check_date = NOW(), admin_user_id = ? WHERE id IN (' + id + ')', [adminUserId], function(error, result) {
			if ( error ) console.log(error);
			callback();
		});
	}
	
	this.markMatchAsUnChecked = function(id, callback)
	{
		dbClient.query('UPDATE keyword_matches SET is_checked = 0 WHERE id = ?', [id], function(error, result) {
			if ( error ) console.log(error);
			callback();
		});
	}
	
	this.scanForKeywords = function(callback)
	{	
		var page = 1,
			perPage = 100,
			count;
			
		dbClient.query('SELECT last_check_date FROM last_check_date', [], function(err, res) {
			startCheckDate = res[0].last_check_date;
			dbClient.query('SELECT * FROM keywords', [], function(err, keywords) {
				for ( i in keywords ) {
					bla = function(keyword) {
					
						where = ' WHERE 1';
						bind = [];

						if ( startCheckDate && keyword.creation_date < startCheckDate ) {
							where += ' AND cm.date > ?';
							bind.push(startCheckDate);
						}


						if ( keyword.type == 'exact' ) {
							where += ' AND body REGEXP ?';
							bind.push(['[[:<:]]' + keyword.keyword + '[[:>:]]']);
						} else {
							where += ' AND body LIKE ?';
							bind.push(['%' + keyword.keyword + '%']);
						}


						dbClient.query('SELECT cm.id, cm.user_id, cm.thread_id, cm.body, cm.date, ctp.user_id AS participant_user_id FROM chat_messages cm INNER JOIN chat_threads_participants ctp ON ctp.thread_id = cm.thread_id AND ctp.user_id <> cm.user_id ' + where, bind, function(err, matches) {
							for(j in matches) {
								match = matches[j];

								dbClient.query('INSERT INTO keyword_matches (sender_user_id, participant_user_id, keyword_id, message_id, is_checked)  VALUES(?, ?, ?, ?, 0)', [match.user_id, match.participant_user_id, keyword.id, match.id]);
							}
							
							if ( keywords[keywords.length - 1].id == keyword.id ) {
								callback();
							}
						});


						
					}(keywords[i])
				}
				dbClient.query('UPDATE last_check_date SET last_check_date = NOW()');
			});
		});
	}
	
	this.getAdminCheckLog = function(filter, callback)
	{
		filter.sortName = filter.sortName || 'a.full_name';
		filter.sortOrder = filter.sortOrder || 'ASC';
		
		var sql = 'SELECT a.full_name AS AdminFullName, a.id AS AdminUserId, COUNT(km.id) AS Count FROM keyword_matches km INNER JOIN admins a ON a.id = km.admin_user_id  WHERE 1 ';
		
		var where = ' ';
		var bind = [];
		
		
		if ( filter.dateFrom ) {
			where += ' AND UNIX_TIMESTAMP(km.check_date) > ?';
			bind.push(filter.dateFrom.getTime() / 1000);
		}
		if ( filter.dateTo ) {
			where += ' AND UNIX_TIMESTAMP(km.check_date) < ?';
			bind.push(filter.dateTo.getTime() / 1000);
		}
		if ( filter.admin ) {
			where += ' AND a.id = ?';
			bind.push(filter.admin);
		}
		
		sql += where;
		sql += ' GROUP BY a.id ';
		sql += ' ORDER BY ' + filter.sortName + ' ' + filter.sortOrder;
		
		
		dbClient.fetchAll(sql, bind, function(result) {
			callback(result);
		});
	}
	
	function twoDigits(d) {
		if(0 <= d && d < 10) return "0" + d.toString();
		if(-10 < d && d < 0) return "-0" + (-1*d).toString();
		return d.toString();
	}
	
	Date.prototype.toMysqlFormat = function() {
		return this.getFullYear() + "-" + twoDigits(1 + this.getMonth()) + "-" + twoDigits(this.getDate()) + " " + twoDigits(this.getHours()) + ":" + twoDigits(this.getMinutes()) + ":" + twoDigits(this.getSeconds());
	};
}

module.exports = KeywordsManager;