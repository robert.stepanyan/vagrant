function UserManager(app) {
	var dbClient = app.get('mysqlClient');
	var bridge = app.get('bridge');
	
	var crypto = require('crypto');
	
	this.getAdminUserByUsernamePassword = function(username, password, callback) {
		password = crypto.createHash('md5').update(password).digest("hex");
		dbClient.fetchRow('SELECT id AS Id, username AS ChatName, email AS Email, full_name AS FullName, type AS Type FROM admins WHERE username = ? AND password = ?', [username, password], function(result) {
			callback(result);
		});
		
		return false;
	}
	
	this.getAdmins = function(callback) {
		dbClient.fetchAll('SELECT id AS Id, username AS ChatName, email AS Email, full_name AS FullName, type AS Type FROM admins', [], function(result) {
			callback(result);
		});
		
		return false;
	}
	
	this.getUser = function(userId, callback)
	{
		bridge.getChatUsers([userId], function(data) {
			if ( typeof data == 'undefined'  ) {
				return callback(false);
			}
			callback(getUserObject(data[0]));
		});
	}
	
	getUserObject = function(data)
	{
		user = {
			userId : data.UserId,
			nickName : data.Chatname,
			userType : data.IsModel ? 'escort' : 'member',
			displayName : data.DisplayName
		};
		
		return user;
	}
}


module.exports = UserManager;