function UserManager(app) {
	
	var dbClient = app.get('mysqlClient');
	
	this.getConversation = function(userId, participantId, dateFrom, dateTo, callback)
	{
		var bind = [], sql;
		getThreadId(userId, participantId, function(threadId) {
			if ( ! threadId ) return [];

			where = ' WHERE thread_id = ? ';
			bind.push(threadId);
			
			if ( dateFrom ) {
				where += ' AND date >= ? ';
				bind.push(dateFrom.toMysqlFormat())
			}
			
			if ( dateTo ) {
				where += ' AND date <= ? ';
				bind.push(dateTo.toMysqlFormat())
			}
			
			sql = 'SELECT user_id AS userId, body, UNIX_TIMESTAMP(date) date FROM chat_messages ' + where + ' ORDER BY id ASC';//getting timestamp in milliseconds
			
			dbClient.query(sql, bind, function(error, messages) {
				callback(messages);
			});
		});
	}
	
	this.getConversations  = function(filter, callback)
	{
		filter.sortName = filter.sortName || 'tp1.user_id';
		filter.sortOrder = filter.sortOrder || 'asc';
		filter.page = filter.page || 1;
		filter.perPage = filter.perPage || 15;
		
		var sql = 'SELECT tp1.user_id AS userId, tp2.user_id AS participantUserId, UNIX_TIMESTAMP(cm.date) AS date FROM chat_threads_participants tp1 INNER JOIN chat_threads_participants tp2 ON tp2.thread_id = tp1.thread_id INNER JOIN chat_messages cm ON cm.thread_id = tp1.thread_id WHERE tp1.user_id <> tp2.user_id';
		var countSql = 'SELECT count(tp1.user_id) AS count FROM chat_threads_participants tp1 INNER JOIN chat_threads_participants tp2 ON tp2.thread_id = tp1.thread_id WHERE tp1.user_id <> tp2.user_id';
		
		var where = ' ';
		var bind = [];
		
		if ( filter.userId ) {
			where += ' AND tp1.user_id = ?';
			bind.push(filter.userId);
		}
		
		if ( filter.username && filter.username.length ) {
			where += ' AND u1.username = ?';
			bind.push(filter.username);
		}
		
		if ( filter.participantUserId ) {
			where += ' AND tp2.user_id = ?';
			bind.push(filter.participantUserId);
		}
		
		if ( filter.participantUsername && filter.participantUsername.length ) {
			where += ' AND u2.username = ?';
			bind.push(filter.participantUsername);
		}
		if ( filter.dateFrom ) {
			sql += ' AND UNIX_TIMESTAMP(cm.date) > '  + filter.dateFrom.getTime() / 1000
		}
		if ( filter.dateTo ) {
			sql += ' AND UNIX_TIMESTAMP(cm.date) < '  + filter.dateTo.getTime() / 1000
		}
		
		
		sql += where;
		countSql += where;
		
		sql += ' GROUP BY tp1.thread_id ORDER BY ' + filter.sortName + ' ' + filter.sortOrder;
		sql += ' LIMIT ' + (filter.page - 1) * filter.perPage + ', ' + filter.perPage;
		
		dbClient.fetchAll(sql, bind, function(result) {
			dbClient.fetchRow(countSql, bind, function(countResult) {
				countResult = countResult ? countResult : 0;
				callback(result, countResult.count);
			});
		});
	}
	
	
	this.getBackupConversations  = function(filter, callback)
	{
		filter.sortName = filter.sortName || 'tp1.user_id';
		filter.sortOrder = filter.sortOrder || 'asc';
		filter.page = filter.page || 1;
		filter.perPage = filter.perPage || 15;
		
		var sql = 'SELECT tp1.user_id AS userId, tp2.user_id AS participantUserId, UNIX_TIMESTAMP(cm.date) AS date FROM chat_threads_participants tp1 INNER JOIN chat_threads_participants tp2 ON tp2.thread_id = tp1.thread_id INNER JOIN chat_messages_backup cm ON cm.thread_id = tp1.thread_id WHERE tp1.user_id <> tp2.user_id';
		var countSql = 'SELECT count(tp1.user_id) AS count FROM chat_threads_participants tp1 INNER JOIN chat_threads_participants tp2 ON tp2.thread_id = tp1.thread_id WHERE tp1.user_id <> tp2.user_id';
		
		var where = ' ';
		var bind = [];
		
		if ( filter.userId ) {
			where += ' AND tp1.user_id = ?';
			bind.push(filter.userId);
		}
		
		if ( filter.participantUserId ) {
			where += ' AND tp2.user_id = ?';
			bind.push(filter.participantUserId);
		}
		
		if ( filter.dateFrom ) {
			sql += ' AND UNIX_TIMESTAMP(cm.date) > '  + filter.dateFrom.getTime() / 1000
		}
		if ( filter.dateTo ) {
			sql += ' AND UNIX_TIMESTAMP(cm.date) < '  + filter.dateTo.getTime() / 1000
		}
		
		
		sql += where;
		countSql += where;
		
		//sql += ' GROUP BY tp1.thread_id ORDER BY ' + filter.sortName + ' ' + filter.sortOrder;
		sql += ' LIMIT ' + (filter.page - 1) * filter.perPage + ', ' + filter.perPage;
		
		dbClient.fetchAll(sql, bind, function(result) {
			callback(result, 1000);
			/*dbClient.fetchRow(countSql, bind, function(countResult) {
				countResult = countResult ? countResult : 0;
				callback(result, countResult.count);
			});*/
		});
	}
	
	this.getBackupConversation = function(userId, participantId, dateFrom, dateTo, callback)
	{
		var bind = [], sql;
		getThreadId(userId, participantId, function(threadId) {
			if ( ! threadId ) return [];

			where = ' WHERE thread_id = ? ';
			bind.push(threadId);
			
			if ( dateFrom ) {
				where += ' AND date >= ? ';
				bind.push(dateFrom.toMysqlFormat())
			}
			
			if ( dateTo ) {
				where += ' AND date <= ? ';
				bind.push(dateTo.toMysqlFormat())
			}
			
			sql = 'SELECT user_id AS userId, body, UNIX_TIMESTAMP(date) date FROM chat_messages_backup ' + where + ' ORDER BY id ASC';//getting timestamp in milliseconds
			
			dbClient.query(sql, bind, function(error, messages) {
				callback(messages);
			});
		});
	}
	
	function getThreadId(userIdFrom, userIdTo, callback)
	{
		dbClient.query(
			'SELECT tp1.thread_id AS threadId FROM chat_threads_participants tp1 INNER JOIN chat_threads_participants tp2 ON tp2.thread_id = tp1.thread_id AND tp2.user_id = ? WHERE tp1.user_id = ?',
			[userIdFrom, userIdTo],
			function(error, result) {
				if ( ! result || result.length == 0 ) {// If doesn't exist with this userIds pair insert
					return false;
				} else {
					callback(result[0].threadId);
				}
			}
		);
	}

	function twoDigits(d) {
		if(0 <= d && d < 10) return "0" + d.toString();
		if(-10 < d && d < 0) return "-0" + (-1*d).toString();
		return d.toString();
	}
	
	Date.prototype.toMysqlFormat = function() {
		return this.getFullYear() + "-" + twoDigits(1 + this.getMonth()) + "-" + twoDigits(this.getDate()) + " " + twoDigits(this.getHours()) + ":" + twoDigits(this.getMinutes()) + ":" + twoDigits(this.getSeconds());
	};
}

module.exports = UserManager;