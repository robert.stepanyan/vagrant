var config;
var http = require('http');
var querystring = require('querystring');



function Bridge(app) {
	var token = {};
	var _cache = {};
	
	config = app.get('apiOptions');
	
	
	this.getChatUsers = function(userIds, callback)
	{
		currentDate = new Date();
		/*if ( typeof _cache[userId] != 'undefined' && ( currentDate - _cache[userId]['date'] < config.common.sessionChacheTime ) ) {
			return callback(_cache[userId]['data']);
		}*/
		
		this.call('SimpleChatUsers', {userIds: userIds.join(',')}, function(result) {
			/*_cache[userId] = {
				date : new Date(),
				data : result
			};*/
			return callback(result);
		});
	}
	
	this.call = function(method, params, callback) {
		try {
			getToken( function(token) {
				var post_data = {
					'compilation_level' : 'ADVANCED_OPTIMIZATIONS',
					'output_format': 'json',
					'output_info': 'compiled_code',
					'warning_level' : 'QUIET',
					'token' : token
				};

				//Setting params
				for(i in params) {
					post_data[i] = params[i];
				}

				post_data = querystring.stringify(post_data);

				var post_options = {
					host: config.host,
					port: '80',
					path: config.path + '/' + method,
					method: 'POST',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'Content-Length': post_data.length
					}
				};
				
				var post_req = http.request(post_options, function(res) {
					res.setEncoding('utf8');

					var resp = '';
					res.on('data', function (chunk) {
						resp += chunk;
					}).on('end', function() {
						try {
							resp = JSON.parse(resp);
							callback(resp);
						} catch(err) {
							console.log(err);
						}
					});
				}).on("error", function(e) {
					console.log("Goto error: " + e.message);
				});

				post_req.write(post_data);
				post_req.end();
			});
		} catch(err) {
			console.log('Call func', err);
		}

		
	}
	
	getToken = function(callback)
	{
		try {
			if ( typeof token.token != 'undefined' && ( new Date() - token.date < config.tokenCacheTime ) ) {
				return callback(token.token);
			}
			var post_data = querystring.stringify({
				'compilation_level' : 'ADVANCED_OPTIMIZATIONS',
				'output_format': 'json',
				'output_info': 'compiled_code',
				'warning_level' : 'QUIET',
				'accountid' : config.accountId,
				'secretcode' : config.secretCode
			});

			var post_options = {
				host: config.host,
				port: '80',
				path: config.path + '/mobile/NewAccountTicket',
				method: 'POST',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Content-Length': post_data.length
				}
			};
			var post_req = http.request(post_options, function(res) {
				res.setEncoding('utf8');

				res.on('data', function (resp) {
					try {
						resp = JSON.parse(resp);
						token = {
							token : resp.Token,
							date : new Date()
						}
						callback(token.token);
					} catch(err) {
						console.log('Invalid response', resp);
					}
				})/*.on("error", function(e) {
					console.log("Goto error: " + e.message);
				})*/;
			});
			post_req.write(post_data);
			post_req.end();
		} catch(err) {
			console.log('getToken func', err);
		}
	}
	
	startclearCache = function()
	{
		setInterval(function() {
			_cache = {};
		}, 5 * 60 * 60 * 1000);//5 hours
	}
	
	//startclearCache();
}



exports.get = function(app)  {
	return new Bridge(app);
}
