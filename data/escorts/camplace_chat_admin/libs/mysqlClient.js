module.exports.get = function(app) {
	mysql = require('mysql');
	connections = app.get('mysqlConnection');
	
	var client = mysql.createConnection({
		user:     connections.user,
		database: connections.database,
		password: connections.password,
		host:     connections.host,
		insecureAuth: true
	});
	
	//Pinging server every hour to avoid connection timeout
	setInterval(function() {
		client.query('SELECT TRUE');
	}, 60 * 60 * 1000);
	
	client.fetchRow = function(sql, params, callback) {
		client.query(sql, params, function(err, result) {
			if ( err ) { console.log(err); return false}
			
			if (  ! result ) return false;
			
			callback(result[0]);
		}); 
	}
	
	client.fetchAll = function(sql, params, callback) {
		client.query(sql, params, function(err, result) {
			if ( err ) { console.log(err); return false}
			
			if (  ! result ) return false;
			
			callback(result);
		}); 
	}
	
	return client;
}


