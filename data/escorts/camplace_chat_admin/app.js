var express = require('express');
var expressLayouts = require('express-ejs-layouts');
var app = express();
 
var hbs = require('hbs');
var url = require('url');


var config = require('./config.js')(app, express);

app.set('views', __dirname + '/views');
app.use(express['static']('public'));


app.use(expressLayouts);
app.set('view engine', 'html');
app.set('layout', 'main-layout.html');

app.engine('html', require('ejs').__express);

var acl = {
	common: ['/login', '/logout', '/history', '/history/data', '/backup', '/history/backupData',
	'/history/conversation', '/history/backupConversation'],
	moderator : ['/keywordMatches', '/keywordMatches/data',	'/keywordMatches/markAsChecked', '/keywordMatches/markAsUnChecked'],
	support: [],
	superadmin: ['all']
};

app.use(function(req, res, next) {
	if ( req.session.user ) {
		res.locals.user = req.session.user;
		res.locals.originalUrl = req.originalUrl;
		res.locals.acl = acl.common.concat(acl[req.session.user.Type]);
	}
	next();
});

//Setting mysql client to the app
var mysqlClient = require('./libs/mysqlClient').get(app);
app.set('mysqlClient', mysqlClient);

//Setting bridge to the app
var bridge = require('./libs/bridge.js').get(app);
app.set('bridge', bridge);
 
 
 var UserManager = require('./libs/userManager');
 var MessageManager = require('./libs/messageManager');
 var KeywordsManager = require('./libs/keywordsManager');
 
 userManager = new UserManager(app);
 messageManager = new MessageManager(app);
 keywordsManager = new KeywordsManager(app);
 

function authenticate(username, password, fn) {
  userManager.getAdminUserByUsernamePassword(username, password, function(user) {
	  if ( ! user ) return fn(new Error('cannot find user'));
  
		if ( user ) {
			return fn(null, user);
		} else {
			fn(new Error('Invalid username or password'));  
		}
  });
}

function twoDigits(d) {
	if(0 <= d && d < 10) return "0" + d.toString();
	if(-10 < d && d < 0) return "-0" + (-1*d).toString();
	return d.toString();
}
	
function getDateString(date) {
	return date.getFullYear() + "-" + twoDigits(1 + date.getMonth()) + "-" + twoDigits(date.getDate()) + " " + twoDigits(date.getHours()) + ":" + twoDigits(date.getMinutes()) + ":" + twoDigits(date.getSeconds());
}

function restrict(req, res, next) {
	if (req.session.user) {
		var path = url.parse(req.url, true).pathname;
		var allowed = acl.common.concat(acl[req.session.user.Type]);
		
		if ( allowed.indexOf(path) == -1 && allowed.indexOf('all') == -1 ) {
			res.redirect('/login');
		}

		next();
	} else {
	  req.session.error = 'Access denied!';
	  res.redirect('/login');
	}
}


setInterval(function() {
	keywordsManager.scanForKeywords(function() {
		//console.log('Keywords scan done !!!');
	})
}, 8 * 60 * 60 * 1000);
 
app.get('/', function(req, res) {
   res.redirect('login');
});

app.get('/login', function(req, res) {
	var data = {
		title: 'PM Chat Admin',
		layout: 'login-layout',
		error: ''
	};
	res.render('login', data);
});
 
 
app.post('/login', function(req, res) {
	var data = {
		title: 'PM Chat Admin',
		layout: 'login-layout',
		error: ''
	};
	authenticate(req.body.username, req.body.password, function(err, user){
		if (user) {
			req.session.regenerate(function(){
				req.session.user = user;
				res.redirect('history');
			});
		} else {
			data.error = 'Wrong Credentials';
			res.render('login', data);
		}
	});
});

app.get('/logout', function(req, res){
	req.session.destroy(function(){
		res.redirect('/');
	});
});


app.get('/history', restrict, function(req, res){
	var data = {
		title: 'PM Chat Admin',
		activeMenu: 'history'
	};
	res.render('history', data);
});

app.get('/backup', restrict, function(req, res){
	var data = {
		title: 'PM Chat Admin',
		activeMenu: 'backup'
	};
	res.render('backup', data);
});


app.get('/history/backupData', restrict, function(req, res){
	var queryData = url.parse(req.url, true)
	var query = queryData.query;
	
	var filter = {
		page: query.page,
		perPage: query.rp,
		sortName: query.sortname,
		sortOrder: query.sortorder,
		userId: query.user_id,
		username: query.username,
		participantUserId: query.participant_user_id,
		participantUsername: query.participant_username,
		dateFrom: query.date_from.length ? new Date(query.date_from) : false,
		dateTo: query.date_to.length ? new Date(query.date_to) : false
	}
	
	messageManager.getBackupConversations(filter, function(conversations, count) {
		var result = {
			page: query.page,
			total: count,
			rows: []
		};
		for( var i = 0; i < conversations.length; i++) {
			var conv = conversations[i];
			var pairId = conv.userId + '-' + conv.participantUserId;
			conv.viewLink = '<a class="action-view" rel="/history/backupConversation?user_id=' + conv.userId + '&participant_user_id=' + conv.participantUserId + '" ></a>'
			
			result.rows.push({id: pairId, cell: conv});
		}
		
		res.end(JSON.stringify(result));
		
	});
	
});

app.get('/history/data', restrict, function(req, res){
	var queryData = url.parse(req.url, true)
	var query = queryData.query;
	
	var filter = {
		page: query.page,
		perPage: query.rp,
		sortName: query.sortname,
		sortOrder: query.sortorder,
		userId: query.user_id,
		username: query.username,
		participantUserId: query.participant_user_id,
		participantUsername: query.participant_username,
		dateFrom: query.date_from.length ? new Date(query.date_from) : false,
		dateTo: query.date_to.length ? new Date(query.date_to) : false
	}
	
	messageManager.getConversations(filter, function(conversations, count) {
		var result = {
			page: query.page,
			total: count,
			rows: []
		};
		for( var i = 0; i < conversations.length; i++) {
			var conv = conversations[i];
			var pairId = conv.userId + '-' + conv.participantUserId;
			conv.viewLink = '<a class="action-view" rel="/history/conversation?user_id=' + conv.userId + '&participant_user_id=' + conv.participantUserId + '" ></a>'
			
			result.rows.push({id: pairId, cell: conv});
		}
		
		res.end(JSON.stringify(result));
		
	});
	
});

app.get('/history/conversation', restrict, function(req, res) {
	var queryData = url.parse(req.url, true)
	var query = queryData.query;
	
	var userId = query.user_id,
		participantUserId = query.participant_user_id
		dateFrom = false,
		dateTo = false;
	
	if ( query.date_from && query.date_from.length ) {
		dateFrom = new Date(query.date_from);
	}
	
	if ( query.date_to && query.date_to.length ) {
		dateTo = new Date(query.date_to);
	}
	
	
	if ( ! userId || ! participantUserId ) {
		res.end('Provide user ids');
		return;
	}
	
	userManager.getUser(userId, function(user) {
		userManager.getUser(participantUserId, function(participantUser) {
			messageManager.getConversation(userId, participantUserId, dateFrom, dateTo, function(messages) {
				if ( userId == 10831 || participantUserId == 10831 ) {
					res.render('conversation', {layout: false, messages: [], user: user, participantUser: participantUser});
					return;
				}
				res.render('conversation', {layout: false, messages: messages, user: user, participantUser: participantUser});
			});
		});
	});
});



app.get('/history/backupConversation', restrict, function(req, res) {
	var queryData = url.parse(req.url, true)
	var query = queryData.query;
	
	var userId = query.user_id,
		participantUserId = query.participant_user_id
		dateFrom = false,
		dateTo = false;
	
	if ( query.date_from && query.date_from.length ) {
		dateFrom = new Date(query.date_from);
	}
	
	if ( query.date_to && query.date_to.length ) {
		dateTo = new Date(query.date_to);
	}
	
	
	if ( ! userId || ! participantUserId ) {
		res.end('Provide user ids');
		return;
	}
	
	userManager.getUser(userId, function(user) {
		userManager.getUser(participantUserId, function(participantUser) {
			messageManager.getBackupConversation(userId, participantUserId, dateFrom, dateTo, function(messages) {
				if ( userId == 10831 || participantUserId == 10831 ) {
					res.render('conversation', {layout: false, messages: [], user: user, participantUser: participantUser});
					return;
				}
				res.render('conversation', {layout: false, messages: messages, user: user, participantUser: participantUser});
			});
		});
	});
});

app.get('/keywords', restrict, function(req, res) {
	var data = {
		title: 'PM Chat Admin',
		activeMenu: 'keywords'
	};
	res.render('keywords/index', data);
});

app.get('/keywords/data', restrict, function(req, res) {
	var queryData = url.parse(req.url, true)
	var query = queryData.query;
	
	var filter = {
		page: query.page,
		perPage: query.rp,
		sortName: query.sortname,
		sortOrder: query.sortorder,
		keyword: query.keyword_f
	}
	
	keywordsManager.getKeywords(filter, function(keywords, count) {
		var result = {
			page: query.page,
			total: count,
			rows: []
		};
		for( var i = 0; i < keywords.length; i++) {
			var keyword = keywords[i];
			keyword.deleteLink = '<a class="action-delete" rel="/keywords/delete?id=' + keyword.id + '" ></a>'
			keyword.editLink = '<a class="action-edit" rel="/keywords/edit?id=' + keyword.id + '" ></a>'
			
			result.rows.push({id: keyword.id, cell: keyword});
		}
		
		res.end(JSON.stringify(result));
	});
	
});

app.get('/keywords/edit', restrict, function(req, res) {
	var queryData = url.parse(req.url, true)
	var query = queryData.query;
	
	var id = query.id;
	
	
	if ( ! id ) {
		res.end('Provide id');
		return;
	}
	
	keywordsManager.get(id, function(keyword) {
		res.render('keywords/edit', {layout: false, keyword: keyword});
	});
	
});

app.post('/keywords/edit', restrict, function(req, res) {
	
	var data = {
		id: req.body.id,
		keyword: req.body.keyword,
		type: req.body.type,
		is_maskable: req.body.is_maskable
	};
	
	var errors = [];
	
	if ( ! data.keyword.length ) {
		errors.push({name: 'keyword', msg: 'requred'});
	}
	
	if ( errors.length ) {
		res.end(JSON.stringify({success: false, errors: errors}));
	} else {
		keywordsManager.update(data, function() {
			res.end(JSON.stringify({success: true}));
		});
	}
});

app.get('/keywords/add', restrict, function(req, res) {
	res.render('keywords/add', {layout: false});
});

app.post('/keywords/add', restrict, function(req, res) {
	var data = {
		keyword: req.body.keyword,
		type: req.body.type,
		is_maskable: req.body.is_maskable ? 1 : 0
	};
	
	var errors = [];
	
	if ( ! data.keyword.length ) {
		errors.push({name: 'keyword', msg: 'requred'});
	}
	if ( ! data.type.length ) {
		errors.push({name: 'type', mst : 'requred'});
	}
	
	if ( errors.length ) {
		res.end(JSON.stringify({success: false, errors: errors}));
	} else {
		keywordsManager.add(data, function() {
			res.end(JSON.stringify({success: true}));
		});
	}
});


app.post('/keywords/delete', restrict, function(req, res) {
	var queryData = url.parse(req.url, true)
	var query = queryData.query;
	
	keywordsManager.remove(query.id, function() {
		res.end(JSON.stringify({status: 'success'}));
	});
});


app.get('/runKeywordChecker', restrict, function(req, res) {
	keywordsManager.scanForKeywords(function() {
		res.end('Done');
	});
});

app.get('/keywordMatches', restrict, function(req, res){
	var data = {
		title: 'PM Chat Admin',
		activeMenu: 'keywordMatches'
	};
	res.render('kyewordMatches/index', data);
});


app.get('/keywordMatches/data', restrict, function(req, res){
	var queryData = url.parse(req.url, true)
	var query = queryData.query;
	
	var filter = {
		dateFrom: query.date_from.length ? new Date(query.date_from) : false,
		dateTo: query.date_to.length ? new Date(query.date_to) : false,
		page: query.page,
		perPage: query.rp,
		sortName: query.sortname,
		sortOrder: query.sortorder,
		keyword: query.keyword_f,
		isChecked: query.is_checked
	}
	
	keywordsManager.getMatches(filter, function(matches, count) {
		
		var result = {
			page: query.page,
			total: count,
			rows: []
		};
		
		for( var i = 0; i < matches.length; i++) {
			var match = matches[i];
			
			match.viewLink = '<a class="action-view" title="View Conversation" rel="/history/conversation?user_id=' + match.sender_user_id + '&participant_user_id=' + match.participant_user_id + '" ></a>';
			if ( match.is_checked ) {
				match.checkUncheck = '<a class="action-button mark-as-unchecked" title="Mark As UnChecked" rel="/keywordMatches/markAsUnChecked?id=' + match.id + '" ></a>';
			} else {
				match.checkUncheck = '<a class="action-button mark-as-checked" data-id="' + match.id + '" title="Mark As Checked" rel="/keywordMatches/markAsChecked?id=' + match.id + '" ></a>';
			}
			match.is_checked = '<span class="' + ( match.is_checked ? 'checked' : 'not-Checked') + '">' + ( match.is_checked ? 'Checked' : 'Not Checked') + '</span>';
			
			result.rows.push({id: match.id, cell: match});
		}
		
		res.end(JSON.stringify(result));
	});
	
	
});

app.post('/keywordMatches/markAsChecked', restrict, function(req, res) {
	var queryData = url.parse(req.url, true)
	var query = queryData.query;
	
	keywordsManager.markMatchAsChecked(query.id, req.session.user.Id, function() {
		res.end(JSON.stringify({status: 'success'}));
	});
});


app.post('/keywordMatches/markAsUnChecked', restrict, function(req, res) {
	var queryData = url.parse(req.url, true)
	var query = queryData.query;
	
	keywordsManager.markMatchAsUnChecked(query.id, function() {
		res.end(JSON.stringify({status: 'success'}));
	});
});



app.get('/keywordMatchesLog', restrict, function(req, res){
	userManager.getAdmins(function(admins) {
		var data = {
			title: 'PM Chat Admin',
			activeMenu: 'keywordMatchesLog',
			admins: admins
		};
		res.render('kyewordMatchesLog/index', data);
	});
	
});


app.get('/keywordMatchesLog/data', restrict, function(req, res){
	var queryData = url.parse(req.url, true)
	var query = queryData.query;
	
	var filter = {
		dateFrom: query.date_from.length ? new Date(query.date_from) : false,
		dateTo: query.date_to.length ? new Date(query.date_to) : false,
		admin: query.admin
	}
	
	keywordsManager.getAdminCheckLog(filter, function(rows) {
		var result = {
			page: query.page,
			total: rows.length,
			rows: []
		};
		
		
		for( var i = 0; i < rows.length; i++) {
			var row = rows[i];
			
			result.rows.push({id: row.AdminUserId, cell: row});
		}
		
		res.end(JSON.stringify(result));
	});
	
	
});


app.get('/restricted', restrict, function(req, res){
  res.send('Wahoo! restricted area, click to <a href="/logout">logout</a>');
});

app.use(function(req, res, next){
  res.send(404, '<h1>404 NOT FOUND</h1>');
});

app.use(function(err, req, res, next){
	console.error(err.stack);
	res.send(500, '<h1>Something broke!</h1>');
});

app.listen(3000);