var express = require('express');
var expressLayouts = require('express-ejs-layouts')
var app = express();
 
var hbs = require('hbs');
var url = require('url');


var config = require('./config.js')(app, express);

app.set('views', __dirname + '/views');
app.use(express['static']('public'));


app.use(expressLayouts);
app.set('view engine', 'html');
app.set('layout', 'main-layout.html');

app.engine('html', require('ejs').__express);

//Setting mysql client to the app
var mysqlClient = require('./libs/mysqlClient').get(app);
app.set('mysqlClient', mysqlClient);

//Setting bridge to the app
var bridge = require('./libs/bridge.js').get(app);
app.set('bridge', bridge);

var UserManager = require('./libs/userManager');
var MessageManager = require('./libs/messageManager');
var KeywordsManager = require('./libs/keywordsManager');
 
messageManager = new MessageManager(app);
userManager = new UserManager(app);

var fs = require('fs');

exportAdminConversations = function(adminUserId, dateFrom, dateTo) {
	userManager.getUser(adminUserId, function(adminUser) {
		messageManager.getConversations({userId: adminUserId, page: 1, perPage: 10000}, function(result, count) {
			var totalLog = '';
			var cnt = 0;
			for(j = 0; j < result.length; j++) {
				var conv = result[j];
				userManager.getUser(conv.participantUserId, function(participantUser) {
					(function(participantUser) {
						var convLog = adminUser.nickName + ' - ' + participantUser.nickName + '\n';
						messageManager.getConversation(adminUser.userId, participantUser.userId, dateFrom, dateTo, function(messages) {
							for(var i = 0; i < messages.length; i++) {
								convLog += (messages[i].userId == adminUserId ? adminUser.nickName : participantUser.nickName) + ' ' + getTimeForMessage(messages[i].date) +'\n';
								convLog += messages[i].body + '\n';
							}

							totalLog += convLog + '\n\n\n';
							cnt++;

							if ( cnt == result.length ) {
								fs.writeFile(adminUserId + '.txt', totalLog, function(err) {
									console.log('done ' + adminUserId);
								});
							}
						});
					})(participantUser, conv);
				});
			}
		});
	});
}





var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
getTimeForMessage = function(date)
{
	var curDate = new Date();
	var date = new Date(date * 1000);

	hours = date.getHours();
	minutes = ('0' + date.getMinutes()).slice(-2);//two number format

	d = hours + ':' + minutes;

	if ( curDate.getDate() != date.getDate() ) {
		d += ' ' + date.getDate() + 'th ' + monthNames[date.getMonth()];
	}

	if ( curDate.getYear() != date.getYear() ) {
		d += ' ' + date.getYear();
	}

	return d;
}

 var ids = [553713, 553718, 553711, 553725];
 for ( var i = 0; i < ids.length; i++ ) {
	exportAdminConversations(ids[i], new Date('12/08/2014'));
 }
 