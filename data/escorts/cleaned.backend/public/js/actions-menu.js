var ActionsMenu = new Class({
	Implements: [Events, Options],

	btnEl: null,
	menuEl: null,

	options: {
		icon: 'details',
		mode: 'click',
		hideDelay: 300
	},

	actions: {},
	timer: null,

	initialize: function (actions, options) {
		this.setOptions(options || {});
		this.render(actions || []);
		this.initEvents();
	},

	_docClickEvent: function (e) {
		if ( e.target != this.menuEl && e.target != this.target ) {
			this.hide();
		}
	},

	onEnter: function (e) {
		$clear(this.timer);
	},

	onLeave: function (e) {
		if ( this.hideOnMouseLeave ) {
			this.timer = this.hide.delay(this.options.hideDelay, this);
		}
	},

	initEvents: function () {
		this.bind = {
			click: this._docClickEvent.bindWithEvent(this),
			enter: this.onEnter.bindWithEvent(this),
			leave: this.onLeave.bindWithEvent(this)
		};

		if ( this.options.mode == 'mouse' ) {
			$$([this.btnEl, this.menuEl]).addEvents({
				mouseenter: this.bind.enter,
				mouseleave: this.bind.leave
			});
		}
		
		document.addEvent('click', this.bind.click);
	},

	render: function (actions) {
		this.btnEl = new Element('div', {
			'class': 'cx-grid-actions-btn',
			styles: {
				display: 'none'
			}
		}).adopt(
			new Element('a', { 'class': 'sbtn icon-' + this.options.icon })
		).inject(document.body).addEvent('click', function () {
			if ( ! actions.length ) return;
			
			(actions[0].func.bind(this))(this.data);
		}.bind(this));

		this.menuEl = new Element('div', {
			'class': 'cx-grid-actions',
			styles: {
				display: 'none'
			}
		}).inject(document.body);

		actions.each(function (act) {
			var el = new Element('a', {
				'class': 'icon-' + act.icon + ' cx-grid-actions-action',
				html: act.caption
			}).inject(this.menuEl);

			var self = this;
			el.addEvents({
				mouseenter: function () {
					if ( ! this.disabled ) {
						this.addClass('active');
					}
				},
				mouseleave: function () {
					this.removeClass('active');
				},
				click: function (e) {
					if ( ! this.disabled ) {
						(act.func.bind(self))(self.data);
					}
					else {
						e.stop();
					}
				}
			});

			el.disable = function () {
				// this.setStyle('display', 'none');
				if ( ! this.disabled ) {
					this.setStyle('display', 'block');
					this.setStyle('background-image', this.getStyle('background-image').replace('.png', '.png,gray'));
					this.setStyle('background-color', this.getStyle('background-color') + '!important');
					this.addClass('disabled');
					this.disabled = true;
				}
			}.bind(el);

			el.enable = function () {
				if ( this.disabled ) {
					this.disabled = false;
					this.setStyle('display', 'block');
					this.setStyle('background-image', this.getStyle('background-image').replace(',gray', ''));
					this.removeClass('disabled');
				}
			}.bind(el);
			
			el.hide = function () {
				
					this.disabled = true;
					this.setStyle('display', 'none');
						
			}.bind(el);

			this.actions[act.name] = el;
		}.bind(this));
	},

	show: function (target, data, showButton) {
		this.hide();

		if ( $defined(showButton) && false == showButton ) {
			showButton = false;
			this.hideOnMouseLeave = false;
		}
		else {
			showButton = true;
			this.hideOnMouseLeave = true;
		}
		
		this.target = target;
		this.data = data;

		this.fireEvent('show', [data]);

		var display = false;
		for ( var name in this.actions ) {
			if ( ! this.actions[name].disabled ) {
				display = true;
			}
		}

		if ( ! display ) return;

		var co;
		if ( $type(this.target) == 'element' ) {
			co = target.getCoordinates(document.body);

			co.left -= 4;
			co.top -= 5;
			co.right = undefined;
			co.bottom = undefined;
		}
		else {
			co = target;
		}
		
		if ( showButton ) {
			this.btnEl.setStyles($merge(co, {
				display: 'block'
			}));

			var btnCo = this.btnEl.getCoordinates(document.body);
		}

		var h = $$('.cx-grid-actions')[0].setStyles({visibility: 'hidden', display: 'block'}).getHeight();
		$$('.cx-grid-actions')[0].setStyles({visibility: null, display: 'none'});
		
		this.menuEl.setStyles($merge(co, {
			top: co.top - h,
			left: co.left,
			width: null,
			height: null,
			bottom: null,
			right: null,
			display: 'block'
		}));
	},

	hide: function () {
		this.btnEl.setStyle('display', 'none');
		this.menuEl.setStyle('display', 'none');

		this.hideOnMouseLeave = (this.options.mode == 'mouse');

		this.fireEvent('hide');
	}
});