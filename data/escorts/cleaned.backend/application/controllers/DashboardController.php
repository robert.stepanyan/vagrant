<?php

class DashboardController extends Zend_Controller_Action
{
	/**
	 * @var Model_Dashboard
	 */
	public $dashboard;
	
	public function init()
	{
		/*for ( $f = 40; $f <= 100; $f += 1 ) {
			echo "$f = ";
			echo $r = Cubix_UnitsConverter::convert($f, 'kg', 'lbs');
			echo " = ";
			echo $r = Cubix_UnitsConverter::convert($r, 'lbs', 'kg') . "\n";
		}
		die;*/
		$this->dashboard = new Model_Dashboard();

		/*$status = new Cubix_EscortStatus(213);
		$status->setStatusBit(32);
		$status->save();
		var_dump($status->getStatusText($status->get())); die;*/
		//echo md5('Pa5sf0rt'); die;

		//var_dump(Cubix_UnitsConverter::convert("6'5", 'ftin', 'cm'));die;
		//var_dump(Cubix_UnitsConverter::convert("203", 'cm', 'ftin'));die;
	}
	
	public function tipDataAction()
	{
		$this->view->layout()->disableLayout();
		
		$user_id = $this->_request->user_id;

		$users_model = new Model_Users();

		if ( ! is_null($user_id) ) {
			$user = $users_model->get($user_id);

			if ( ! $user ) {
				die;
			}
		}

		if ( (isset($user) && $user->user_type == Model_Users::USER_TYPE_ESCORT ) ||
				! is_null($escort_id = $this->_getParam('escort_id')) ||
				! is_null($showname = $this->_getParam('showname')) ) {
			$model = new Model_Escorts();
			// var_dump(! is_null($escort_id) || ! is_null($showname) ? ($escort_id ? $escort_id : $showname) : $user->getId());
			if (!is_null($showname))
			{
				$ar = explode ('-', $showname);
				$escort_id = end($ar);
			}
			//$this->view->escort = ! is_null($escort_id) || ! is_null($showname) ? $model->get($escort_id ? $escort_id : $model->getIdByShowname($showname)->id) : $model->getByUserId($user->getId());
			$this->view->escort = ! is_null($escort_id) ? $model->get($escort_id) : $model->getByUserId($user->getId());
			$user = $users_model->get($this->view->escort->user_id);
			$this->_helper->viewRenderer->setScriptAction('tip-data-escort');
		}
		elseif ( $user->user_type == Model_Users::USER_TYPE_AGENCY ) {
			$model = new Model_Agencies();
			$this->view->agency = $model->getByUserId($user->getId());
			$this->_helper->viewRenderer->setScriptAction('tip-data-agency');
		}

		$this->view->user = $user;
	}
	
	public function indexAction()
	{	
		/*$db = Zend_Registry::get('db');
		$str = "<?php\nreturn array(\n";
		foreach ( $db->fetchAll('SELECT id, title_en AS title FROM nationalities ORDER BY title_en') as $n ) {
			$str .= "\t{$n->id} => '{$n->title}',\n";
		}
		$str .= ");\n";
		file_put_contents('../../markup/data/nationalities.php', $str);
		die;*/

//		$db = Zend_Registry::get('db');
//		$str = "<?php\nreturn array(\n";
//		foreach ( $db->fetchAll('SELECT id, title_en AS title FROM countries ORDER BY ordering DESC, title_en') as $n ) {
//			$str .= "\t{$n->id} => array('title' => '" . (str_replace('\'', '\\\'', $n->title)) . "', 'regions' => array()),\n";
//		}
//		$str .= ");\n";
//		file_put_contents('../../markup/data/countries.php', $str);
//
//
//		$db = Zend_Registry::get('db');
//		$str = "<?php\n";
//		foreach ( $db->fetchAll('SELECT country_id, id, title_en AS title FROM regions title_en') as $n ) {
//			$n->title = str_replace('\'', '\\\'', $n->title);
//			// $str = "if ( ! isset(\$COUNTRIES[{$n->country_id}]['regions']) ) \$COUNTRIES[{$n->id}]['regions'] = array();\n";
//			$str .= "\$COUNTRIES[{$n->country_id}]['regions'][{$n->id}] = array('cities' => array(), 'title' => '{$n->title}');\n";
//		}
//		$str .= "";
//		file_put_contents('../../markup/data/regions.php', $str);
//
//		$db = Zend_Registry::get('db');
//		$str = "<?php\n";
//		foreach ( $db->fetchAll('SELECT id, region_id, country_id, title_en AS title FROM cities ORDER BY country_id, region_id, title_en') as $n ) {
//			$n->title = str_replace('\'', '\\\'', $n->title);
//			$str .= "\$COUNTRIES[{$n->country_id}]['regions'][{$n->region_id}]['cities'][{$n->id}] = array('title' => '{$n->title}');\n";
//		}
//		$str .= "";
//		file_put_contents('../../markup/data/cities.php', $str);
//		die;
		
		$auth = Zend_Auth::getInstance();
		$admin = $auth->getIdentity();
		
		switch ( $admin->type )
		{ 
			case 'superadmin':
				$this->_forward('superadmin');
			break;
			case 'admin':
				$this->_forward('superadmin');
			break;
			case 'moderator plus':
				$this->_forward('superadmin');
			break;
			case 'moderator':
				$this->_forward('superadmin');
			break;
			case 'payments manager':
				$this->_forward('superadmin');
			break;
			case 'dash manager':
				$this->_forward('dashmanager');
			break;
			case 'sales manager':
				$this->_forward('salesmanager');
			break;
			case 'data entry':
				$this->_forward('dataentry');
			break;
			case 'data entry plus':
				$this->_forward('dataentry');
			break;
			case 'seo manager':
				$this->_forward('seomanager');
			break;
			default:
				$this->_forward('superadmin');
		}
		
		/*$client = new Cubix_Api_XmlRpc_Client();
		$agency = $client->call('Agencies.getEscorts', array($agency_id, $page, $config['widgetPerPage'], $escort_id));*/
		
		//$acl = Zend_Registry::get('acl');
		//echo $acl->isAllowed('sales manager', 'dashboard', 'index') ? "разрешен" : "запрещен";
	}

	public function salesmanagerAction()
	{
		//$this->billingInfoAction();
		
		/*$this->latestRegisteredEscortsAction();
		$this->notApprovedRevisionsAction();
		$this->verifyNotificationsAction();
		$this->notApprovedPhotosAction();*/
		
		$app_id = $this->_request->application_id;
		
		/*if ( $app_id == 16 ) {
			$this->phoneBillingEscortsAction();
		}*/
		
		//$this->view->statistics = $this->dashboard->getStatistics($app_id);
		
		$this->view->layout()->enableLayout();
	}

	public function seomanagerAction()
	{
		$this->_redirect('/seo/entities');
	}

	public function dataentryAction()
	{
		$this->_redirect('/escorts');
	}

	public function superadminAction()
	{
		//$this->dashboard->updateGeography();

		//$this->billingInfoAction();
		//$this->latestRegisteredEscortsAction();
		//$this->notApprovedRevisionsAction();
		//$this->verificationRequestsAction();
		//$this->verifyNotificationsAction();
		//$this->notApprovedPhotosAction();
		
		$app_id = $this->_request->application_id;

		if ( $app_id == APP_A6 || $app_id == APP_A6_AT ) {
			$this->phoneBillingEscortsAction();
			$this->view->gotd_booked_days = $this->dashboard->getGotdBookedDays();
		}
		
		//$this->view->statistics = $this->dashboard->getStatistics($app_id);
		
		$this->view->layout()->enableLayout();
	}
	
	public function dashmanagerAction()
	{
		//$this->dashboard->updateGeography();

		/*$this->latestRegisteredEscortsAction();
		$this->notApprovedRevisionsAction();
		$this->verificationRequestsAction();
		$this->verifyNotificationsAction();
		$this->notApprovedPhotosAction();*/
		
		$app_id = $this->_request->application_id;

		if ( $app_id == APP_A6 || $app_id == APP_A6_AT ) {
			$this->phoneBillingEscortsAction();
			$this->view->gotd_booked_days = $this->dashboard->getGotdBookedDays();
		}
		
		//$this->view->statistics = $this->dashboard->getStatistics($app_id);
		
		$this->view->layout()->enableLayout();
	}
	
	public function statisticsAction()
	{
		session_write_close();
		$this->view->layout()->disableLayout();

		$app_id = $this->_request->application_id;
		
		$this->view->be_user = $be_user = Zend_Auth::getInstance()->getIdentity();
		
		$this->view->statistics = $this->dashboard->getStatistics($app_id);
	}

	public function notApprovedPhotosAction()
	{
		session_write_close();
		$this->view->layout()->disableLayout();

		$app_id = $this->_request->application_id;
		$be_user = Zend_Auth::getInstance()->getIdentity();
		
		$cache = Zend_Registry::get('cache');
		$cache_key = 'not_approved_photos_' . $app_id . '_' . preg_replace('# #', '_', $be_user->type) . '_' . preg_replace('# |-#', '_', $be_user->username);
		
		if ( ! $items = $cache->load($cache_key) ) {
			$items = $this->dashboard->getNotApprovedPhotos($app_id, $be_user);
			
			$cache->save($items, $cache_key, array(), 5);
			//echo "not cache";
		}
				
		$this->view->not_approved_photos = $items;
		
		/********************* VIP ******************************/
		$this->view->vip_escorts = array();
		
		if (Sceon_Application::getId() == APP_6A)
		{
			if ($items)
			{
				$escs = array();
				
				foreach ($items as $item)
					$escs[] = $item->id;
				
				$ids = implode(',', $escs);
				$escorts = $this->dashboard->getVipVerifiedEscortsByIds($ids);
				$vip_escorts = array();

				if ($escorts)
				{
					foreach ($escorts as $e)
						$vip_escorts[] = $e->escort_id;
					
					$this->view->vip_escorts = $vip_escorts;
				}
			}
		}
	}
	
	public function onlineEscortsAction()
	{
		session_write_close();
		$this->view->layout()->disableLayout();

		$app_id = $this->_request->application_id;
		$be_user = Zend_Auth::getInstance()->getIdentity();
		
		$cache = Zend_Registry::get('cache');
		$cache_key = 'online_escorts_' . $app_id . '_' . preg_replace('# #', '_', $be_user->type) . '_' . preg_replace('# |-#', '_', $be_user->username);
		
		if ( /*! $items = $cache->load($cache_key)*/true ) {
			$cont = file_get_contents(Sceon_Application::getById()->url . '/index/o-e');
			$items = json_decode($cont);
			$cache->save($items, $cache_key, array(), 5);
		}
		
		$this->view->escorts = $items;
	}

	public function billingInfoAction()
	{
		session_write_close();
		$this->view->layout()->disableLayout();

		$bu_user = Zend_Auth::getInstance()->getIdentity();
		$app_id = $this->_request->application_id;
		
		$cache = Zend_Registry::get('cache');
		$cache_key_po = 'po_billing_info_' . $app_id . '_' . preg_replace('# #', '_', $be_user->type) . '_' . preg_replace('# |\-#', '_', $be_user->username);
		$cache_key_pro = 'pro_billing_info_' . $app_id . '_' . preg_replace('# #', '_', $be_user->type) . '_' . preg_replace('# |-#', '_', $be_user->username);
		$cache_key_pt = 'pt_billing_info_' . $app_id . '_' . preg_replace('# #', '_', $be_user->type) . '_' . preg_replace('# |-#', '_', $be_user->username);

		if ( ! $pending_orders = $cache->load($cache_key_po) ) {
			$pending_orders = $this->dashboard->getPendingOrders($bu_user);
			
			$cache->save($pending_orders, $cache_key_po, array(), 15);
			//echo "not cache";
		}
		
		if ( ! $payment_rejected_orders = $cache->load($cache_key_pro) ) {
			$payment_rejected_orders = $this->dashboard->getPaymentRejectedOrders($bu_user);
			
			$cache->save($payment_rejected_orders, $cache_key_pro, array(), 15);
			//echo "not cache";
		}
		
		if ( ! $pending_transfers = $cache->load($cache_key_pt) ) {
			$pending_transfers = $this->dashboard->getPendingTransfers($bu_user);
			
			$cache->save($pending_transfers, $cache_key_pt, array(), 15);
			//echo "not cache";
		}		
		

		$this->view->pending_orders_count = $pending_orders['count'];
		$this->view->payment_rejected_orders_count = $payment_rejected_orders['count'];
		$this->view->pending_transfers_count = $pending_transfers['count'];
	}
	
	public function notApprovedRevisionsAction()
	{
		session_write_close();
		
		$this->view->layout()->disableLayout();
		
		$app_id = $this->_request->application_id;
		$be_user = Zend_Auth::getInstance()->getIdentity();
		
		$cache = Zend_Registry::get('cache');
		$cache_key = 'revisions_escorts_' . $app_id . '_' . preg_replace('# #', '_', $be_user->type) . '_' . preg_replace('# |-#', '_', $be_user->username);
		
		if ( ! $revisions = $cache->load($cache_key) ) {
			$revisions = $this->dashboard->getNotApprovedRevisionsV2($app_id, $be_user);
			
			$cache->save($revisions, $cache_key, array(), 5);
		}	
		
		if ( $revisions['data'] )
		{
			$ret = array();
			foreach ( $revisions['data'] as $rev )
			{
				$ret[date("d M Y", strtotime($rev->date_updated)) . ' :: ' . $rev->sales_name][] = $rev;
			}
			
			$this->view->revisions = $ret;
		}
		
		$this->view->rev_count = $revisions['count']->rev_count;
		
		/********************* VIP ******************************/
		$this->view->vip_escorts = array();
		
		if (Sceon_Application::getId() == APP_6A)
		{
			if ($revisions['data'])
			{
				$escs = array();
				
				foreach ($revisions['data'] as $item)
					$escs[] = $item->id;
				
				$ids = implode(',', $escs);
				$escorts = $this->dashboard->getVipVerifiedEscortsByIds($ids);
				$vip_escorts = array();

				if ($escorts)
				{
					foreach ($escorts as $e)
						$vip_escorts[] = $e->escort_id;
					
					$this->view->vip_escorts = $vip_escorts;
				}
			}
		}
	}
	
	public function latestRegisteredEscortsAction()
	{
		session_write_close();
		
		$this->view->layout()->disableLayout();

		$app_id = $this->_request->application_id;
		$be_user = Zend_Auth::getInstance()->getIdentity();
		
		$cache = Zend_Registry::get('cache');
		$cache_key = 'latest_reg_escorts_' . $app_id . '_' . preg_replace('# #', '_', $be_user->type) . '_' . preg_replace('# |-#', '_', $be_user->username);

		if ( ! $escorts = $cache->load($cache_key) ) {
			$escorts = $this->dashboard->getLatestRegisteredEscortsV2($app_id, $be_user);
			
			$cache->save($escorts, $cache_key, array(), 5);
		}
		
		$this->view->latestRegisteredEscorts = $escorts;
	}
	
	public function phoneBillingEscortsAction()
	{
		$this->view->layout()->disableLayout();

		$app_id = $this->_request->application_id;

		$be_user = Zend_Auth::getInstance()->getIdentity();

		$this->view->phoneBillingEscorts = $this->dashboard->phoneBillingEscorts($app_id); 
	}
	
	public function verificationRequestsAction()
	{
		session_write_close();
		$this->view->layout()->disableLayout();

		$be_user = Zend_Auth::getInstance()->getIdentity();

		$idCard = $this->dashboard->getVerifyRequestsByType(Model_Verifications_Requests::TYPE_IDCARD, $be_user);
		$webCam = $this->dashboard->getVerifyRequestsByType(Model_Verifications_Requests::TYPE_WEBCAM, $be_user);
		
		$this->view->webCamCount = $webCam->count;
		$this->view->idCardCount = $idCard->count;
		
		unset($webCam->count);
		unset($idCard->count);
		
		$this->view->verifyRequests = $pendingRequest = array_merge($idCard->data, $webCam->data);
		
		/********************* VIP ******************************/
		$this->view->vip_escorts = array();
		
		if (Sceon_Application::getId() == APP_6A)
		{
			if ($pendingRequest)
			{
				$escs = array();
				
				foreach ($pendingRequest as $item)
					$escs[] = $item->escort_id;
				
				$ids = implode(',', $escs);
				$escorts = $this->dashboard->getVipVerifiedEscortsByIds($ids);
				$vip_escorts = array();

				if ($escorts)
				{
					foreach ($escorts as $e)
						$vip_escorts[] = $e->escort_id;
					
					$this->view->vip_escorts = $vip_escorts;
				}
			}
		}
	}
	
	public function verifyNotificationsAction()
	{
		session_write_close();
		$this->view->layout()->disableLayout();

		$be_user = Zend_Auth::getInstance()->getIdentity();

		$this->view->verifyNotifications = $items = $this->dashboard->getVerifyNotifications($be_user);
		
		/********************* VIP ******************************/
		$this->view->vip_escorts = array();
		
		if (Sceon_Application::getId() == APP_6A)
		{
			if ($items->data)
			{
				$escs = array();
				
				foreach ($items->data as $item)
					$escs[] = $item->id;
				
				$ids = implode(',', $escs);
				$escorts = $this->dashboard->getVipVerifiedEscortsByIds($ids);
				$vip_escorts = array();

				if ($escorts)
				{
					foreach ($escorts as $e)
						$vip_escorts[] = $e->escort_id;
					
					$this->view->vip_escorts = $vip_escorts;
				}
			}
		}
	}
	
	
	
	public function switchApplicationAction()
	{
		$user = Zend_Auth::getInstance()->getIdentity();
		$back_to = $this->_getParam('back_to');
		$application_id = $this->_getParam('global_application_id');
		
		if ( $user->type !== 'superadmin' && $user->type !== 'seo manager' && $application_id != $user->application_id ) {
			$this->_response->setRedirect($back_to);
			return;
		}
		
		setcookie('application_id', $application_id, time() + 2 * 7 * 24 * 60 * 60 /* two weeks */, '/');
		
		$this->_response->setRedirect($back_to);
	}

	public function fraudWarningsAction()
	{
		$page = (int) $this->_getParam('page');
		if ( $page < 1 ) $page = 1;
		$limit = 10;
		$this->view->page = $page;
		$this->view->limit = $limit;
	}
	
	public function dublicitiesAction()
	{
		$page = (int) $this->_getParam('page');
		$ip = $this->_getParam('ip');
		$member = $this->_getParam('member');
		$escort = $this->_getParam('escort');
		$agency = $this->_getParam('agency');
		if ( $page < 1 ) $page = 1;
		$limit = 10;
		$this->view->page = $page;
		$this->view->params = array('ip' => $ip, 'member' => $member, 'escort' => $escort, 'agency' => $agency);
		$this->view->limit = $limit;
	}
	
	public function dublicitiesViewAction()
	{
		$this->view->layout()->disableLayout();
		$m = new Model_Dublicities();
		
		$user_id = (int) $this->_getParam('user_id');
		
		$this->view->ip = Model_Users::getLastLogin($user_id)->ip;
		$this->view->users = $m->getForUser($user_id);
	}
	
	public function cookieDublicitiesAction()
	{
		$page = (int) $this->_getParam('page');
		$member = $this->_getParam('member');
		$escort = $this->_getParam('escort');
		$agency = $this->_getParam('agency');
		if ( $page < 1 ) $page = 1;
		$limit = 100;
		$this->view->page = $page;
		$this->view->params = array('member' => $member, 'escort' => $escort, 'agency' => $agency);
		$this->view->limit = $limit;
	}
}
