<?php

class Model_Geoip extends Cubix_Model
{
	public static function getClientLocation($ip = null)
    {
		if (!function_exists('geoip_record_by_name'))
			return null;
		
        if (!$ip)
        {
            if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if (isset($_SERVER['REMOTE_ADDR']))
                $ip = $_SERVER['REMOTE_ADDR'];
            else
                $ip = 'UNKNOWN';
        }

        $geoip = geoip_record_by_name($ip);

        $geoData = array(
			'country' => $geoip['country_name'],
			'country_iso' => $geoip['country_code'],
			'region' => $geoip['region'],
			'city' => $geoip['city'],
			'latitude' => $geoip['latitude'],
			'longitude' => $geoip['longitude']
		);

        return $geoData;
    }
}
