<?php

class Model_Menu_Item
{
	public $title = '';
	public $key = '';
	public $link = '';
	public $children = array();
	public $is_active = false;

	public function __construct($title, array $params = array())
	{
		$this->title = $title;

		foreach ( $params as $key => $value ) {
			$this->$key = $value;
		}
	}

	public function addChild(Model_Menu_Item $item)
	{
		$this->children[] = $item;
	}
}
