<?php

class Model_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{
	/**
	 * @var Zend_Auth
	 */
	protected $_auth;

	/**
	 * @var Model_Acl
	 */
	protected $_acl;
	
	public function __construct()
	{
		$this->_auth = Zend_Auth::getInstance();
		
		if ( ! $this->_auth->hasIdentity() ) {
			return;
		}
		
		$this->_acl = Model_Acl::getInstance();
		
		// Construct the ACL
		// $this->_acl->addRole('group-1');
		// $this->_acl->allow('group-1', null);
		
		$this->_acl->addResource('default/auth');
		$this->_acl->allow(null, 'default/auth');
		
		
		$db = Zend_Registry::get('db');
		
		$user_id = $this->_auth->getIdentity()->getId();
		$resources = $db->query('
			SELECT r.*
			FROM acl_users_roles ur
			INNER JOIN acl_resources r ON r.role_id = ur.role_id
			WHERE ur.backend_user_id = ?
		', $user_id)->fetchAll();
		
		foreach ( $resources as $res ) {
			extract((array) $res);
			
			$role = new Zend_Acl_Role('group-' . $role_id);
			
			if ( ! $this->_acl->hasRole($role) ) {
				$this->_acl->addRole($role);
			}
			
			if ( ! is_null($action) && ! is_null($controller) ) {
				$resource = $module . '/' . $controller . '/' . $action;
			}
			elseif ( is_null($action) && ! is_null($controller) ) {
				$resource = $module . '/' . $controller;
			}
			elseif ( is_null($action) && is_null($controller) ) {
				$resource = $module;
			}
			
			if ( ! $this->_acl->has($resource) ) {
				$this->_acl->addResource($resource);
			}
			
			$this->_acl->allow($role, $resource);
		}
	}
	
	/**
	 * @param Zend_Controller_Request_Abstract $request
	 */
	public function preDispatch($request)
	{
		if ( ! $this->_auth->hasIdentity() ) {
			return;
		}
		
		$resources = array();
		$resources[] = $request->getModuleName();
		$resources[] = $request->getControllerName();
		$resources[] = $request->getActionName();
		
		if ( ! $this->_acl->isAllowed(implode('/', $resources)) ) {
			$request->setModuleName('default');
			$request->setControllerName('error');
			$request->setActionName('denied');
			return;
		}
	}
}
