<?php
class Model_Users extends Cubix_Model
{
	protected $_table = 'users';
	protected $_itemClass = 'Model_UserItem';
	
	const USER_TYPE_MEMBER = 'member';
	const USER_TYPE_AGENCY = 'agency';
	const USER_TYPE_ESCORT = 'escort';

	public function get($id)
	{
		return parent::_fetchRow('
			SELECT id, user_type, username, email FROM users WHERE id = ?
		', array($id));
	}

	public function activate($user_id)
	{
		$this->_db->update('users', array(
			'activation_hash' => null,
			'status' => STATUS_ACTIVE
		), array(
			$this->_db->quoteInto('id = ?', $user_id)
		));

		return true;
	}
	
	public function setStatus($user_id,$status)
	{
		$this->_db->update('users', array(
			'status' => $status
		), array(
			$this->_db->quoteInto('id = ?', $user_id)
		));

		return true;
	}

	public function getAll($page, $per_page, $filter, $sort_field, $sort_dir, &$count)
	{
		$sql = '
			SELECT
				u.id,
				u.username,
				u.email,
				u.user_type,
				u.application_id,
				UNIX_TIMESTAMP(u.date_registered) AS creation_date,
				c.iso AS app_iso,
				u.status,
				u.activation_hash
			FROM users u
			INNER JOIN applications a ON a.id = u.application_id
			LEFT JOIN countries c ON c.id = a.country_id
			WHERE 1
		';

		$countSql = '
			SELECT COUNT(DISTINCT(u.id))
			FROM users u
			INNER JOIN applications a ON a.id = u.application_id
			LEFT JOIN countries c ON c.id = a.country_id
			WHERE 1
		';

		$where = '';

		
		if ( strlen($filter['application_id']) ) {
			$where .= self::quote('AND u.application_id = ?', $filter['application_id']);
		}

		/*if ( strlen($filter['not_verified']) ) {
			$where .= ' AND u.activation_hash IS NOT NULL AND status = -1 ';
		}*/
		if ( strlen($filter['status']) ) {
			$where .= self::quote('AND u.activation_hash IS NOT NULL AND status = ?', $filter['status']);
		}
		if ( strlen($filter['username']) ) {
			$where .= self::quote('AND u.username LIKE ?', $filter['username'] . '%');
		}

		if ( strlen($filter['email']) ) {
			$where .= self::quote('AND u.email LIKE ?', $filter['email'] . '%');
		}

		if ( strlen($filter['user_type']) ) {
			$where .= self::quote('AND u.user_type = ?', $filter['user_type']);
		}
		
		if ( strlen($filter['sales_user_id']) ) {
			$where .= self::quote('AND u.sales_user_id = ?', $filter['sales_user_id']);
		}

		$sql .= $where;
		$countSql .= $where;
		//echo $sql; die;

		$sql .= '
			GROUP BY u.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$count = intval($this->getAdapter()->fetchOne($countSql));

		return parent::_fetchAll($sql);
	}

	public function save(Model_UserItem $item)
	{
		unset($item['groups']);
		parent::save($item);

		// Set the new inserted id
		if ( ! $item->getId() ) {
			$item->setId(self::getAdapter()->lastInsertId());
		}

		return parent::getAdapter()->lastInsertId();
	}
	
	public function remove($id)
	{
		parent::remove(self::quote('id = ?', $id));
	}

	static public function can($action)
	{
		$auth = Zend_Auth::getInstance();
		$user = $auth->getIdentity();

		switch ( $action ) {
			case 'publish-seo-data':
				return in_array($user->type, array( 'moderator', 'moderator plus', 'seo manager', 'admin', 'superadmin'));
			case 'edit-escort-profile':
				return in_array($user->type, array( 'moderator', 'moderator plus', 'sales manager', 'payments manager', 'data entry','data entry plus', 'admin', 'superadmin'));
			case 'edit-escort-seo-data':
				return in_array($user->type, array( 'moderator', 'moderator plus', 'seo manager', 'seo copywriter', 'admin', 'superadmin'));
			case 'set-member-premium':
				return in_array($user->type, array('superadmin'));
		}
	}

	public static function getLastLogin($id)
	{
		return parent::getAdapter()->query('SELECT login_date, ip FROM users_last_login WHERE user_id = ? ORDER BY login_date DESC LIMIT 1', $id)->fetch();
	}

	public static function getLastLoginsCount($id)
	{
		return parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM users_last_login WHERE user_id = ?', $id);
	}

	public static function getLastLogins($id, $interval)
	{
		return parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM users_last_login WHERE user_id = ? AND DATE_ADD(login_date, INTERVAL -' . $interval . ' DAY)', $id);
	}

	public static function sendPostActivationEmail($user) {
		Cubix_Email::sendTemplate('activate_success', $user['email'], array(
			'username' => $user['username']
		));
	}
	public static function getUniqueIps($user_id)
	{
		return parent::getAdapter()->fetchAll('SELECT ip, MAX(login_date) AS last_date FROM users_last_login WHERE user_id = ? GROUP BY ip ORDER BY MAX(login_date) DESC', $user_id);
	}
	
	public static function checkNewSalesPerson($user_id, $sales_person)
	{
		return parent::getAdapter()->fetchOne('SELECT TRUE FROM users WHERE id = ? AND new_sales_user_id = ?', array($user_id, $sales_person));
	}
	
	public function getByEmail($email)
	{
		return parent::getAdapter()->query('
			SELECT u.id, u.user_type, e.id AS escort_id, a.id AS agency_id
			FROM users u 
			LEFT JOIN escorts e ON u.id = e.user_id
			LEFT JOIN agencies a ON u.id = a.user_id
			WHERE u.status <> -99 AND u.email = ?
			GROUP BY u.email
		', $email)->fetch();
	}
	
	public function setSystemDisabled($id, $status)
	{
		$this->_db->update('users', array('system_disabled' => $status), $this->_db->quoteInto('id = ?', $id));
	}
}
