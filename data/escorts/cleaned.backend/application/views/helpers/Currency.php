<?php

class Zend_View_Helper_Currency extends Zend_View_Helper_Abstract
{
	public function currency($what = 'symbol')
	{
		return Sceon_Application::getById($this->view->request()->application_id)->{'currency_' . $what};
	}
}
