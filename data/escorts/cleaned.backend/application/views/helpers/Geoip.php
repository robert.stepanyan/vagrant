<?php

class Zend_View_Helper_Geoip extends Zend_View_Helper_Abstract
{
	public function geoip($ip, $ip_count = NULL)
	{
		$str = '';

		if ($ip)
		{
			/**/
			$m_c = new Model_Countries();
			$c_id = Sceon_Application::getById()->country_id;
			$iso = array();
			
			if ($c_id)
			{
				$iso[] = strtoupper($m_c->getById($c_id)->iso);
			}
			else 
			{
				$all = $m_c->getAll();
				
				foreach ($all as $c)
				{
					$iso[] = strtoupper($c->iso);
				}
			}
			/**/
			
			$geoData = Model_Geoip::getClientLocation($ip);

			//if ($geoData['country_iso'] != 'FR')
			if (!in_array($geoData['country_iso'], $iso))
				$str .= '<span class="red">';

			$str .= $ip;
			if($ip_count){
				$str .= ' ( <span class="ip-opener" >'.$ip_count.'</span> ) ';
			}
			if ($geoData['country'])
				$str .= ', ' . $geoData['country'];

			if ($geoData['city'])
				$str .= ', ' . $geoData['city'];

			if ($geoData['latitude'])
				$str .= ', lat: ' . $geoData['latitude'];

			if ($geoData['longitude'])
				$str .= ', lon: ' . $geoData['longitude'];

			//if ($geoData['country_iso'] != 'FR')
			if (!in_array($geoData['country_iso'], $iso))
				$str .= '</span>';
		}

		return $str;
	}
}
