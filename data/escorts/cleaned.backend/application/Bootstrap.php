<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	public function run()
	{
		define('IN_BACKEND', 1);
		parent::run();
	}
	
	protected function _initRoutes()
	{
		require('../application/models/Plugin/Auth.php');
		require('../application/models/Plugin/Filter.php');

		$this->bootstrap('frontController');
		$front = $this->getResource('frontController');
		$router = $front->getRouter();
		
		$router->addRoute(
			'dashboard',
			new Zend_Controller_Router_Route(
				'',
				array(
					'module' => 'default',
					'controller' => 'dashboard',
					'action' => 'index'
				)
			)
		);
		
		$router->addRoute(
			'external-link',
			new Zend_Controller_Router_Route(
				'go',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'go'
				)
			)
		);
	}
	
	protected function _initConfig()
	{
		Zend_Registry::set('system_config', $this->getOption('system'));
		Zend_Registry::set('escorts_config', $this->getOption('escorts'));
		//Zend_Registry::set('feedback_config', $this->getOption('feedback'));
		//Zend_Registry::set('newsletter_config', $this->getOption('newsletter'));
		//Zend_Registry::set('statistics', $this->getOption('statistics'));
	}
	
	protected function _initAutoload()
	{
		$moduleLoader = new Zend_Application_Module_Autoloader(array(
			'namespace' => '',
			'basePath' => APPLICATION_PATH
		));

		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('Cubix_');
		$autoloader->registerNamespace('Sceon_');

		return $moduleLoader;
	}
	
	protected function _initDatabase()
	{
		$this->bootstrap('db');
		$db = $this->getResource('db');
		$db->setFetchMode(Zend_Db::FETCH_OBJ);

		Zend_Registry::set('db', $db);
		Cubix_Model::setAdapter($db);

		$db->query('SET NAMES `utf8`');
	}
	
	protected function _initViewHelpers()
	{
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();

		$view->doctype('XHTML1_STRICT');

		$view->headMeta()
			->appendHttpEquiv('Content-Type', 'text/html; charset=utf-8')
			->appendHttpEquiv('Cache-Control', 'no-cache');
		$view->headTitle()->setSeparator(' - ');
		$view->headTitle('EscortForum Backend');
	}
	
	
	
	protected function _initStorage()
	{
		Zend_Registry::set('images_config', $this->getOption('images'));
	}

	protected function _initCache()
	{
		
		$frontendOptions = array(
			'lifetime' => null,
			'automatic_serialization' => true
		);
		
		$backendOptions = array(
			'servers' => array(
				array(
					'host' => 'localhost',
					'port' => '11211',
					'persistent' =>  true
				)
			),
		);

		if (defined('APPLICATION_ENV') && (APPLICATION_ENV == 'staging' || APPLICATION_ENV == 'development') ) {
			$cache = Zend_Cache::factory('Core', 'Blackhole', array('automatic_serialization' => true));
		} else {
			$cache = Zend_Cache::factory(new Zend_Cache_Core($frontendOptions), new Zend_Cache_Backend_Memcached($backendOptions), $frontendOptions);
		}

		Zend_Registry::set('cache', $cache);
		
	}
	
	protected function _initDefines()
	{
		if ( ! is_callable('json_encode') ) {
			function json_encode($value) {
				return Zend_Json_Encoder::encode($value);
			}
			
			function json_decode($value)
			{
				return Zend_Json_Decoder::decode($value);
			}
		}
	}
}
