<?php

class Geography_View_Helper_Langs
{
	public function langs($ids = true)
	{
		return Cubix_I18n::getLangs($ids);
	}
}

