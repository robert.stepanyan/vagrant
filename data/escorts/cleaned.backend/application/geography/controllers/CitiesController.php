<?php

class Geography_CitiesController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Geography_Cities();
	}
	
	public function indexAction()
	{
	
	}
	
	public function dataAction()
	{
		$country_id = intval($this->_getParam('country_id'));
		$title = trim($this->_getParam('title'));
		$no_timezone = $this->_getParam('no_timezone');
		if ( ! $country_id ) {
			$country_id = null;
		}
		
		$region_id = intval($this->_getParam('region_id'));
		$page = $this->_getParam('page');
		$per_page = $this->_getParam('per_page');
		$sort_field = $this->_getParam('sort_field');
		$sort_dir = $this->_getParam('sort_dir');
		$count = 0;
		$sort_field = 'order_id'; $sort_dir = 'ASC';
		$data = $this->model->getAll($region_id, $country_id, $page, $per_page, $sort_field, $sort_dir, $count, $title,$no_timezone);
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}

	public function ajaxHomeCitiesSearchAction()
	{
		$name = trim($this->_getParam('city_search_name'));
		$lng = trim($this->_getParam('lng'));

		if ( ! strlen($name) ) {
			die(json_encode(array()));
		}
		$model = new Cubix_Geography_Cities();

		$cities = $model->ajaxGetAllCities($name, $lng);

		$tmp_cities = array();
		if ( count($cities) ) {
			foreach ( $cities as $i => $city ) {
				$tmp_cities[$i] = array('id' => $city->id, 'name' => $city->title . ' (' . $city->country_title . ')');
			}
		}

		echo json_encode($tmp_cities);
		die;
	}

	public function ajaxGetEscortCitiesAction()
	{
		$escort_id = intval($this->_getParam('escort_id'));

		$m_escorts = new Model_Escorts();
		$escort = $m_escorts->get($escort_id);

		$escort_cities = $escort->getCities();

		foreach( $escort_cities as $k => $esc_city ) {
			$escort_cities[$k]->is_base = 0;
			if ( $esc_city->id == $escort->city_id ) {
				$escort_cities[$k]->is_base = 1;
			}
		}
		
		die(json_encode($escort_cities));
	}
	
	public function ajaxAction()
	{
		$country_id = intval($this->_getParam('country_id'));
		$region_id = intval($this->_getParam('region_id'));
		
		echo json_encode(array(
			'data' => $this->model->ajaxGetAll($region_id, $country_id)
		));
		die;
	}
	
	public function createAction()
	{
		$country_id = intval($this->_getParam('country_id'));
		
		if ( ! $country_id ) {
			$country_id = null;
		}
		
		$this->view->country_id = $country_id;
		$this->view->region_id = $region_id = intval($this->_getParam('region_id'));
		$url_slug = $this->_getParam('url_slug');

		$m_timezones = new Cubix_Geography_TimeZones();
		$this->view->time_zones = $m_timezones->getAll();
		
		if ( $this->_request->isPost() ) {
			$titles = Cubix_I18n::getValues($this->_request->getParams(), 'title', '');
			
			$validator = new Cubix_Validator();

			$time_zone_id = intval($this->_getParam('time_zone_id'));

			if ( ! strlen($titles['title_en']) ) {
				$validator->setError('title_en', 'English title is required');
			}
			
			if ( $country_id && ! $region_id ) {
				$countries_model = new Model_Geography_Countries();
				if ( $countries_model->get($country_id)->has_regions ) {
					$validator->setError('region_id', 'Required');
				}
			}
			elseif ( ! $country_id ) {
				$validator->setError('country_id', 'Required');
			}
			
			if ( ! strlen($url_slug) ) {
				$validator->setError('url_slug', 'Url slug is required');
			}
			elseif ( preg_match('/[^a-zA-Z0-9-]+/', $url_slug) ) {
				$validator->setError('url_slug', 'Only alphanumeric symbols');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_Geography_CityItem(array_merge(
					array(
						'region_id' => $region_id,
						'country_id' => $country_id,
						'time_zone_id' => $time_zone_id,
						'url_slug'	=> $url_slug
					),
					$titles
				)));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function editAction()
	{
		$id = intval($this->_getParam('id'));

		$m_timezones = new Cubix_Geography_TimeZones();
		$this->view->time_zones = $m_timezones->getAll();

		if ( $this->_request->isPost() ) {
			$country_id = intval($this->_getParam('country_id'));
			$region_id = intval($this->_getParam('region_id'));
			$time_zone_id = intval($this->_getParam('time_zone_id'));
			$titles = Cubix_I18n::getValues($this->_request->getParams(), 'title', '');
			$url_slug = $this->_getParam('url_slug');
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($titles['title_en']) ) {
				$validator->setError('title_en', 'English title is required');
			}

			if ( ! $time_zone_id ) {
				$time_zone_id = null;
			}
			
			if ( $country_id && ! $region_id ) {
				$countries_model = new Model_Geography_Countries();
				if ( $countries_model->get($country_id)->has_regions ) {
					$validator->setError('region_id', 'Required');
				}
			}
			elseif ( ! $country_id ) {
				$validator->setError('country_id', 'Required');
			}
			
			if ( ! strlen($url_slug) ) {
				$validator->setError('url_slug', 'Url slug is required');
			}
			elseif ( preg_match('/[^a-zA-Z0-9-]+/', $url_slug) ) {
				$validator->setError('url_slug', 'Only alphanumeric symbols');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_Geography_CityItem(array_merge(
					array(
						'id' => $id,
						'region_id' => $region_id,
						'country_id' => $country_id,
						'time_zone_id' => $time_zone_id,
						'url_slug'		=> $url_slug
					),
					$titles
				)));
			}
			
			die(json_encode($validator->getStatus()));
		}
		else {
			$this->view->city = $this->model->get($id);
		}
	}
	
	public function ajaxGetCitiesGroupedAction()
	{
		$country_id = intval($this->_getParam('country_id'));
		$region_id = intval($this->_getParam('region_id'));
		
		//$model = new Cubix_Geography_Cities();
		
		$cities = $this->model->ajaxGetAll($region_id, $country_id);
		
		$regions = array();
		foreach ( $cities as $city ) {
			if ( ! isset($regions[$city->region_title]) ) {
				$regions[$city->region_title] = array();
			}
			$has_regions = $city->has_regions;
			$regions[$city->region_title][] = $city;
		}
		
		//ksort($regions);
		
		foreach ( $regions as $k => $region ) {
			usort($regions[$k], create_function('$a1, $a2', 'return ($a1["title"] == $a2["title"]) ? 1 : ($a1["title"] > $a2["title"] ? 1 : 0);'));
		}
		
		$is_json = ! is_null($this->_getParam('json'));
		if ( $is_json ) {
			die(json_encode($regions));
		}
		
		$html = '';

		if ( $has_regions )
		{
			foreach ( $regions as $title => $cities )
			{

				$html .= $has_regions ? '<optgroup label="' . $title . '">' : '';
				foreach ( $cities as $city )
				{
					$html .= '<option value="' . $city->id .'">' . $city->title . '</option>';
				}
				$html .= $has_regions ? '</optgroup>' : '';
			}
		}
		else
		{
			foreach ( $cities as $city )
			{
				$html .= '<option value="' . $city->id .'">' . $city->title . '</option>';
			}
		}
		
		echo $html;
		
		die;
	}
	
	public function removeAction()
	{
		$id = intval($this->_getParam('id'));
		
		$this->model->remove($id);
	}

	public function toggleFrenchAction()
	{
		$id = intval($this->_getParam('id'));
		$db = Zend_Registry::get('db');
		$db->update('cities', array('is_french' => new Zend_Db_Expr('NOT is_french')), array($db->quoteInto('id = ?', $id)));
	}
	
	public function syncAction()
	{
		$app_id = $this->_getParam('application_id');

		$app = Sceon_Application::getById($app_id);
		$url = 'http://www.' . $app->host;
		$sync_url = $url . '/api/dictionary/geography-plain';
		//echo $sync_url;die;
		$result = @file_get_contents($sync_url);

		if ( $result === false ) {
			die(json_encode(array('status' => 'error')));
		}

		header('Cubix-message: ' . $url . ' has been successfully syncronized');
		die(json_encode(array('status' => 'success', 'result' => $url, 'response' => $result)));
	}
	
	public function orderingAction()
	{
		$id = $this->_getParam('id');
		$dir = $this->_getParam('dir');
		
		if ( ! $id || ! $dir ) die;
		
		$this->model->changeOrder($id, $dir);
		
		die('Success');
	}
	
	public function setTimezoneAction()
	{
		if ( $this->_request->isPost() ) {
			$ids = unserialize($this->_getParam('ids'));
			$time_zone_id = intval($this->_getParam('time_zone_id'));
			$validator = new Cubix_Validator();
			if ( ! $time_zone_id ) {
				$validator->setError('time_zone_id', 'Required');
			}
			$this->model->setTimeZones($ids,$time_zone_id);
			die(json_encode($validator->getStatus()));
		}
		else{
			$ids = $this->_getParam('id');
			$this->view->ids = htmlentities(serialize($ids));
			$m_timezones = new Cubix_Geography_TimeZones();
			$this->view->time_zones = $m_timezones->getAll();
			
		}
	}
}

