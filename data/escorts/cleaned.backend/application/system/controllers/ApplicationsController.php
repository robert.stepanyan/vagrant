<?php

class System_ApplicationsController extends Zend_Controller_Action
{
	/**
	 * @var Model_System_Dictionary
	 */
	protected $model;
	
	public function init()
	{
		$this->model = new Model_System_Applications();
	}
	
	public function indexAction()
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		
		$current_app_id = intval($this->_getParam('application_id'));
		
		$filter = array();
		if ( $current_app_id != 0 )
		{
			$filter['a.id'] = $current_app_id;
		}
		
		$data = $this->model->getAll(
			$req->page,
			$req->per_page,  
			$req->sort_field, 
			$req->sort_dir,
			$filter,
			$count
		);
		
		echo json_encode(array('data' => $data, 'count' => $count));
		die;
	}
	
	public function editAction()
	{		
		$id = intval($this->_getParam('id'));
		
		if ( $this->_request->isPost() ) 
		{			
			$app_title = $this->_getParam('application_title');
			$host = $this->_getParam('host');
			$country_id = intval($this->_getParam('country_id'));
			$url = $this->_getParam('url');
			$min_photos = intval($this->_getParam('min_photos'));
			$max_working_cities = intval($this->_getParam('max_working_cities'));
			
			$validator = new Cubix_Validator();
						
			if ( ! strlen($app_title) ) {
				$validator->setError('application_title', 'Application title is required');
			}
			
			if ( ! strlen($host) ) {
				$validator->setError('host', 'Host is required');
			}
			
			if ( ! strlen($url) ) {
				$validator->setError('url', 'Url is required');
			}
			
			if ( $min_photos == 0 ) {
				$validator->setError('min_photos', 'Escort minimum photos count is required');
			}
			
			/*if ( $country_id == 0 ) {
				$validator->setError('country_id', 'Country is required');
			}*/
			
			if ( $max_working_cities == 0 ) {
				$validator->setError('max_working_cities', 'Max Working Cities is required');
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_System_ApplicationsItem(array(
					'id'         => $id,
					'title'      => $app_title,
					'host'       => $host,
					//'country_id' => $country_id,
					'url'        => $url,
					'min_photos' => $min_photos,
					'max_working_cities' => $max_working_cities
				)));
			}
			
			die(json_encode($validator->getStatus()));
		}
		else {
			$this->view->application = $this->model->get($id);
		}
	}
}
