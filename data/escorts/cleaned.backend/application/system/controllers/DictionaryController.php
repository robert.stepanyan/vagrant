<?php

class System_DictionaryController extends Zend_Controller_Action
{
	/**
	 * @var Model_System_Dictionary
	 */
	protected $model;
	
	public function init()
	{
		$this->model = new Model_System_Dictionary();
	}
	
	public function indexAction() 
	{
		
	}
	
	public function dataAction()
	{
		$req = $this->_request;
		$langs = Cubix_I18n::getLangs();
		
		$filter = array('id' => $req->id, 'value_en' => $req->value_en, 'value_it' => $req->value_it, 'value_fr' => $req->value_fr);
		
		$data = $this->model->getAll(
			$req->page,
			$req->per_page, 
			$filter, 
			$req->sort_field, 
			$req->sort_dir, 
			$count
		);
		
		foreach ( $data as $i => $d ) {
			foreach ( $langs as $lang ) {
				$data[$i]->{'value_' . $lang} = mb_substr($data[$i]->{'value_' . $lang}, 0, 300);
			}
		}
		
		$this->view->data = array(
			'data' => $data,
			'count' => $count
		);
	}
	
	public function createAction()
	{
		if ( $this->_request->isPost() )
		{
			$data = new Cubix_Form_Data($this->_request);
			$fields = array(
				'entry_id' => ''
			);
			$data->setFields($fields);
			$data = $data->getData();
			
			$values = Cubix_I18n::getValues($this->_request->getParams(), 'value', '');
			
			$validator = new Cubix_Validator();
			
			if ( ! strlen($data['entry_id']) ) {
				$validator->setError('entry_id', 'This field is required');
			}
			else {
				$item = $this->model->getById($data['entry_id']);

				if ( $item && $item->getId() == $data['entry_id'] ) {
					$validator->setError('entry_id', 'Choose another id, this one already exists');
				}
			}
			
			if ( $validator->isValid() ) {
				$this->model->save(new Model_System_DictionaryItem(array_merge($data, $values)));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}

	public function editAction()
	{
		$this->view->entry = $entry = $this->model->getById($this->_getParam('id'));
		
		if ( $this->_request->isPost() )
		{
			$data = Cubix_I18n::getValues($this->_request->getParams(), 'value', '');
						
			$this->model->save(new Model_System_DictionaryItem(array_merge(
				array('id' => $this->_getParam('id')), 
				$data
			)));
			
			$validator = new Cubix_Validator();
			die(json_encode($validator->getStatus()));
		}
	}
	
	public function uploadLogoAction()
	{
		$agency_id = intval($this->_getParam('agency_id'));
		
		try {
			$agency = $this->model->getById($agency_id);
			$agency = $agency['agency_data'];
			
			if ( ! $agency ) die('Wrong agency id was specified');
			
			// Save on remote storage
			$images = new Cubix_Images();
			$image = $images->save($_FILES['Filedata']['tmp_name'], 'agencies', $agency->application_id, strtolower(@end(explode('.', $_FILES['Filedata']['name']))));
			
			$agency->logo_hash = $image['hash'];
			$agency->logo_ext = $image['ext'];
			
			$this->model->setLogo($agency->id, $image);
			
			$result = array(
				'status' => 'success',
				'logo_url' => $agency->getLogoUrl()
			);
		} catch (Exception $e) {
			$result = array('status' => 'error', 'msg' => '');
			
			$result['msg'] = $e->getMessage();
		}
		
		die(json_encode($result));
	}

	public function removeAction()
	{
		$id = $this->_getParam('id');
		
		$this->model->remove($id);
		
		die;
	}
	
	public function exportAction()
	{ 
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename=dictionary.csv');
		
		$data = $this->model->getAllForExport();
		
		
		$fp = fopen('php://output', 'w');
		foreach ($data as $fields) {
			fputcsv($fp, $fields);
		}
		fclose($fp);
		
		die;
	}
}
