<?php

class Cubix_Debug_Benchmark
{
	protected static $_results = array();

	const LOG_TYPE_DB = 1;
	const LOG_TYPE_FILE = 2;

	protected static $_logType = self::LOG_TYPE_FILE;

	protected static $_application = 'default';

	protected static function _microtime()
	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float) $usec + (float) $sec);
	}

	public static function setApplication($application)
	{
		self::$_application = $application;
	}

	public static function setLogType($type)
	{
		self::$_logType = $type;
	}

	public static function start($name)
	{
		self::$_results[$name] = array('name' => $name, 'start' => self::_microtime());
	}
	
	public static function end($name)
	{
		if ( ! isset(self::$_results[$name]) ) trigger_error('No benchmark started with name', E_USER_ERROR);

		self::$_results[$name]['end'] = ($end = self::_microtime());
		self::$_results[$name]['time'] = round($end - self::$_results[$name]['start'], 5);
	}
	
	public static function get($name = null)
	{
		if ( is_null($name) ) return self::$_results;

		if ( ! isset(self::$_results[$name]) ) trigger_error('No benchmark started with name', E_USER_ERROR);

		return self::$_results[$name];
	}

	public static function log()
	{
		if ( self::LOG_TYPE_DB == self::$_logType ) {
			self::_logDb();
		}
		elseif ( self::LOG_TYPE_FILE == self::$_logType ) {
			self::_logFile();
		}
	}

	protected static function _logDb()
	{
		$db = Zend_Registry::get('db');

		$db->query('DELETE FROM debug_benchmark WHERE application = ?', self::$_application);
		foreach ( self::get() as $result ) {
			$db->insert('debug_benchmark', array(
				'application' => self::$_application,
				'label' => $result['name'],
				'time' => $result['time']
			));
		}
	}

	protected static function _logFile()
	{
		$data = '';

		foreach ( self::get() as $result ) {
			$data .= $result['name'] . "\n" . round($result['time'], 4) . "\n";
		}

		$data .= "---------------------------------------------------------\n\n";

		file_put_contents('benchmark.log', $data);
	}
}
