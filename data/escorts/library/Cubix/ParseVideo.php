<?php

class Cubix_ParseVideo {
   
	private $buffer;
    public $metaData;
    public $videoData;
    private $fileName;
    private $typeFlagsAudio;
    private $typeFlagsVideo;
	private $escort_id;
	private $config;
	private $error;
	public $approve = 0;
	public $pending = 0;
    private $VCidMap = array(
      2=>"Sorenson H.263",
      3=>"Screen Video",
      4=>"VP6",
      5=>"VP6 with Alpha channel",
    );      //Video Codec ID(s)

    private $ACidMap = array(
      "Linear PCM, platform endian",
      "ADPCM",
      "MP3",
      "Linear PCM, little endian",
      "Nellymoser 16-kHz Mono",
      "Nellymoser 8-kHz Mono",
      "Nellymoser",
      "G.711 A-law logarithmic PCM",
      "G.711 mu-law logarithmic PCM",
      "reserved",
      "AAC",
      "Speex",
      14=>"MP3 8-Khz",
      15=>"Device-specific sound"
    );      //Audio Codec ID(s)

/**
 *  CONSTRUCTOR : initialize class members
 *
 * @param string $flv : flv file path
 */
    public function  __construct($flv,$escort_id,$config) {
 
	  $this->fileName = $flv;
		$this->escort_id = $escort_id;
		$this->config = $config;
		$this->ffmpeg = $this->config['ffmpeg_path'];
		$this->metaData = array(
			"duration"=>null,
			"size"=>null,
			"framerate"=>null,
			"width"=>null,
			"height"=>null,
			"videodatarate"=>null,
			"audiodatarate"=>null,
			"audiodelay"=>null,
			"audiosamplesize"=>null,
			"audiosamplerate"=>null,
			"audiocodecid"=>null,
			"videocodecid"=>null,
			"version"=>null,
			"headersize"=>0
        );

		$this->error = $this->getVideoInfo();
		
	}
  
	public function getError()
	{
		return $this->error;
	}
	
	public function dumpMetaData(){
        echo "FLV Version: <strong>".$this->metaData["version"]."</strong><br />";
        echo "Duration : <strong>".$this->metaData["duration"]."</strong> Second(s) <br />";
        echo "File Size: <strong>".number_format(($this->metaData["size"]/pow(1024,2)) , 2)."</strong> MB<br />";
        echo "Width: <strong>".$this->metaData["width"]."</strong> Pixel(s)<br />";
        echo "Height: <strong>".$this->metaData["height"]."</strong> Pixel(s)<br />";
        echo "Framerate: <strong>".number_format($this->metaData["framerate"],2)."</strong> FPS<br />";
        echo "Video Data Rate: <strong>".number_format($this->metaData["videodatarate"])."</strong> Kbps<br />";
        echo "Audio Data Rate: <strong>".number_format($this->metaData["audiodatarate"])."</strong> Kbps<br />";
        echo "Audio Delay: <strong>".$this->metaData["audiodelay"]."</strong> Second(s)<br />";
        echo "Audio Codec ID: <strong>".$this->metaData["audiocodecid"]."</strong><br />";
        if(is_numeric($this->metaData["audiocodecid"])){
            echo "Audio Format: <strong>".$this->ACidMap[$this->metaData["audiocodecid"]]."</strong><br />";
        }
        echo "Video Codec ID: <strong>".$this->metaData["videocodecid"]."</strong><br />";
        if(is_numeric($this->metaData["videocodecid"])){
            echo "Video Format: <strong>".$this->VCidMap[$this->metaData["videocodecid"]]."</strong><br />";
        }
        echo "Header Size: <strong>".$this->metaData["headersize"]."</strong> Byte(s)<br />";
    }

    public function getMetaData(){
        if(!file_exists($this->fileName)){
            echo "Error! {$this->fileName} does not exist.<br />";
            return false;
        }
        if(!is_readable($this->fileName)){
            echo "Error! Could not read the file. Check the file permissions.<br />";
            return false;
        }
        $f = @fopen($this->fileName,"rb");
        if(!$f){
            echo "Unknown Error! Could not read the file.<br />";
            return;
        }
        $signature = fread($f,3);
        if($signature != "FLV"){
            echo "Error! Wrong file format.<br />";
            return false;
        }
        $this->metaData["version"] = ord(fread($f,1));
        $this->metaData["size"] = filesize($this->fileName);

        $flags = ord(fread($f,1));
        $flags = sprintf("%'04b", $flags);
        $this->typeFlagsAudio = substr($flags, 1, 1);
        $this->typeFlagsVideo = substr($flags, 3, 1);

        for ($i=0; $i < 4; $i++) {
            $this->metaData["headersize"] += ord(fread($f,1)) ;
        }

        $this->buffer = fread($f, 400);
        fclose($f);
	if(strpos($this->buffer, "onMetaData") === false){
            echo "Error! No MetaData Exists.<br />";
            return false;
        }  

        foreach($this->metaData as $k=>$v){
            $this->parseBuffer($k);
        }

        return $this->metaData;
    }


    private function parseBuffer($fieldName){
        $fieldPos = strpos($this->buffer, $fieldName);  //get the field position
        if($fieldPos !== false){
            $pos = $fieldPos + strlen($fieldName) + 1;  
            $buffer = substr($this->buffer,$pos);

            $d = "";
            for($i=0; $i < 8;$i++){
                $d .= sprintf("%08b", ord(substr($buffer,$i,1)));
            }

            $total = self::bin2Double($d);
            $this->metaData[$fieldName] = $total;
        }
    }


    public static function bin2Double($strBin){
            $sb = substr($strBin, 0, 1);    // first bit is sign bit
            $exponent = substr($strBin, 1, 11); // 11 bits exponent
            $fraction = "1".substr($strBin, 12, 52);    //52 bits fraction (1.F) 

            $s = pow(-1, bindec($sb));
            $dec = pow(2, (bindec($exponent) - 1023));  //Decode exponent

            if($dec == 2047){
                if($fraction == 0){
                    if($s==0){
                        echo "Infinity";
                    }else{
                        echo "-Infinity";
                    }
                }else{
                    echo "NaN";
                }
            }

            if($dec > 0 && $dec < 2047){
                $t = 1;
                for($i=1 ; $i <= 53; $i++){
                    $t += ((int)substr($fraction, $i, 1)) * pow(2, -$i);    //decode significant
                }
                $total = $s * $t * $dec ;
                return  $total;
            }
            return false;
    }
	
	public function getVideoInfo()
	{
		
		if(!isset($this->config) || !is_array($this->config))
				return FALSE;
		
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		if(file_exists($this->ffmpeg))
		{			
			ob_start();
			passthru("$this->ffmpeg -i ".$this->fileName." 2>&1");
			$video_info = ob_get_contents();
			ob_end_clean();

			if((strpos($video_info, 'bitrate:')===false) || (strpos($video_info, 'Video:')===false) || (strpos($video_info, 'Duration:')===false))
			return Cubix_I18n::translate('sys_error_upload_video_allowed_extensions',array('formats' => implode(', ', $this->config['allowedvideoExts'])));	

			$this->videoData['size'] = number_format((filesize($this->fileName)/pow(1024,2)),2);
			preg_match('/bitrate: ([0-9]+)/', $video_info, $match);
			$this->videoData['bitrate'] = $match[1];
			preg_match('/Stream #(?:[0-9\.]+)(?:.*)\: Video: (?P<videocodec>.*) (?P<width>[0-9]*)x(?P<height>[0-9]*)/', $video_info, $match);
			$this->videoData['height'] = $match['height'];
			$this->videoData['width'] =  $match['width'];
			preg_match("/Duration: ([\d]{2}):([\d]{2}):([\d]{2}).([\d]{1,2}), start/si", $video_info, $match);
			$this->videoData['duration'] = $match[1] * 3600 + $match[2] * 60 + $match[3];
			
			if(intval($this->videoData['height']) < 320 || intval($this->videoData['width']) < 320){
				return Cubix_I18n::translate('sys_error_upload_video_allowed_size',array('size' => 320));	
			}
			/*$video_size = number_format((filesize($this->fileName)/pow(1024,2)),2);
			if($video_size>intval($this->config['VideoMaxSize']))
			return Cubix_I18n::translate('sys_error_upload_video_allowed_max_size',array('size' =>$this->config['VideoMaxSize']));*/

			/*	
			Min Length
			if(!isset($this->config['VideoMinLength']) || intval($mins)<$this->config['VideoMinLength'])
			return Cubix_I18n::translate('sys_error_upload_video_allowed_min_length', array('length' =>  $this->config['VideoMinLength']));
			*/
			return null;
		}
		
	}

	public function CreateMobileVideo($width,$height)
	{		
		$temp = sys_get_temp_dir().'/escort/';
		$name = pathinfo($this->fileName,PATHINFO_FILENAME).'_m.mp4';
		
		$dir =$temp.$name; 	
		$ffmpeg =  $this->config;
		$size = $width.'X'.$height;
		$path = pathinfo($_SERVER['SCRIPT_FILENAME'],PATHINFO_DIRNAME);
		try{		
			exec("$ffmpeg -i $this->fileName -acodec libmp3lame -ab 128k -vcodec mpeg4 -b 1200k -mbd 2 -flags +mv4+aic -trellis 1 -cmp 2 -subcmp 2 -s $size -metadata title=X $dir >> $path/log_file.log"); 
			if(!$this->SaveFtpVideoBackend($dir,$name))
			{
				echo 'sys_error_upload_mobile_video';
			}
		}
		catch (Exception $e)
		{
			echo $e->getMessage();
		}
		//exec("/usr/bin/php /var/www/backend.6annonce.com/application/ffmpeg.php ".escapeshellarg(json_encode(array($this->fileName,$this->config['ffmpeg_path'],$this->escort_id,$width,$height)))." >> /var/www/backend.6annonce.com/application/log_file.log 2>&1 &");
		
	}
	
	public function SaveImage($ajax=FALSE)
	{	
		
		try{
			$dir = sys_get_temp_dir().'/escort/'.Cubix_Application::getById(Cubix_Application::getId())->host.'/'.$this->escort_id .'/';
			
			$dir.= uniqid().'/';

			if(!is_dir($dir)){
				mkdir($dir,0755,true);
			}	
			
			$img_count = isset($this->config['ImageCount']) && is_numeric($this->config['ImageCount'])?$this->config['ImageCount']:6;

			$interval= $this->videoData['duration'] / $img_count;
			$sum = 0;
			$size = $this->videoData['width'].'x'.$this->videoData['height'];
			
			for($i = 0; $sum < $this->videoData['duration'] - $interval; $i++)
			{	
				$sum = $interval * $i;
				exec("$this->ffmpeg -i $this->fileName -deinterlace -an -ss $sum -f mjpeg -t 1 -r 1 -y -s $size $dir".uniqid().".jpg "); 
			}	
			$images = new Cubix_ImagesCommon();
			$dir_handle = opendir($dir) or die("Unable to open $dir"); 
			$photo = array();	
			while ($filename = readdir($dir_handle))  
			{
				if(file_exists($dir.$filename) && $filename!='.' && $filename!='..' && getimagesize($dir.$filename))
				{	$img['tmp_name'] = $dir.$filename;
					$img['name'] = $filename;
					$photo[] = $images->save($img,$ajax);				
				}

			}
			/*$photo['width'] = $this->videoData['width'];
			$photo['height'] = $this->videoData['height'];*/
			
			
		}
		catch (Exception $e)
		{
			return $e->getMessage();
			//return 'sys_error_upload_video';
		}
		
		return $photo;			
		
	}
	
	public function SaveFtpVideoBackend($path,$name)
	{	
		
		$ftp = new Cubix_VideosCommon();
		return $ftp->_storeToPicVideo($path,$this->escort_id,$name);
	}
	
	public function SaveVideoImageFtpFrontend(array $image,$client,$ext='flv')
	{
		if(!empty($image))
		{
		    $width=$height=null;
			$name = uniqid().'.'.$ext;
            if (Cubix_Application::getId() == APP_ED){
                $width = $this->videoData['width'];
                $height = $this->videoData['height'];
                $hash = uniqid();
                $name = $hash.'_'.$height.'p.'.$ext;
            }
			$ftp = new Cubix_VideosCommon();	
			
			if($ftp->_storeToPicVideo($this->fileName, $this->escort_id,$name))
			{
                $data = array(
                    'escort_id'=>$this->escort_id,
                    'video'=>$hash,
                    'height' => $height,
                    'width' => $width
                );
				$video_id = $client->call('Application.saveVideoV2',array($data));

				$count = count($image);
				$video_img = array();
				for($i=0;$i<$count;$i++)
				{	if($image[$i]['image_id']!=0)
					{	
						$video_img['video_id'] = $video_id;
						$video_img['image_id'] = $image[$i]['image_id'];
						$client->call('Application.saveVideoImageV2',array($video_img));
					}
				}
		
				return array('id' => $video_id, 'name' => $name);
			}
		}
			return false;
	}
		
	public function CheckVideoStatus($video_count,$frontend=false)
	{	
		$video_count[0] = (array)$video_count[0];
		$video_count[1] = (array)$video_count[1];
		$pending = $approve = array('quantitiy'=>0);
		switch($video_count[0]['status'])
		{
			case 'pending':$pending = $video_count[0];
									break;
			case 'approve':$approve = $video_count[0];
									break;
		}
		if(isset($video_count[1]))
		{	
			switch($video_count[1]['status'])
			{
				case 'pending':$pending = $video_count[1];
										break;
				case 'approve':$approve = $video_count[1];
										break;
			}

		}
		$this->pending = $pending['quantitiy'];
		$this->approve = $approve['quantitiy'];
		if($frontend && $approve['quantitiy']>=$this->config['VideoCount'])
			return  Cubix_I18n::translate('sys_error_upload_video_pending',array('count' =>$this->config['VideoCount']));	
		if( ($pending['quantitiy']==($this->config['VideoCount']-1) && $approve['quantitiy']==1) || 
			($approve['quantitiy']==($this->config['VideoCount']-1) && $pending['quantitiy']==1) || 
			($pending['quantitiy']>=$this->config['VideoCount']) || ($approve['quantitiy']>=$this->config['VideoCount']) )
			if($frontend)
			return Cubix_I18n::translate('sys_error_upload_video_pending',array('count' =>$this->config['VideoCount'])).' '.Cubix_I18n::translate('wait_while_other_videos_checked');
			else
			return	Cubix_I18n::translate('sys_error_upload_video_pending',array('count' =>$this->config['VideoCount'])).' first check other videos.';
		else
		return false;
	}
	
	public function ConvertToFlv()
	{
        $pathInfo  = pathinfo($this->fileName);
        $path = $pathInfo['dirname'].DIRECTORY_SEPARATOR.$pathInfo['filename'];
		$ffmpeg = $this->config['ffmpeg_path'];
		exec("$ffmpeg -i $this->fileName -ar 22050 -ab 32k -f flv -b 800k  -y  $path.flv");
		$this->fileName =$path.'.flv';
		return $this->fileName;
	}
	
	public function ConvertToMP4()
	{
        $pathInfo  = pathinfo($this->fileName);
        $path = $pathInfo['dirname'].DIRECTORY_SEPARATOR.$pathInfo['filename'];
		$ffmpeg = $this->config['ffmpeg_path'];
		exec("$ffmpeg -i $this->fileName -vcodec libx264 -acodec libfdk_aac -b 1000k -maxrate 1000k -ab 196k {$path}.mp4 ");
		$this->fileName =$path.'.mp4';
		return $this->fileName;
	}
	
	public function ConvertToMP4test()
	{
		$path = explode('.', $this->fileName);
		$ffmpeg = $this->config['ffmpeg_path'];
		ob_start();
		
		//passthru("$ffmpeg -i $this->fileName -vcodec libx264 -level 21 -refs 2 -b 500k -threads 0 -s 640x360 -frame_size 160 $path[0].mp4 2>&1");
		
		//passthru("$ffmpeg -i $this->fileName -vcodec libx264 -threads 0 -r 25  -b 500k -acodec libmp3lame -ab 64k -s 720x406  $path[0].mp4 2>&1"); 
		//passthru("$ffmpeg -i $this->fileName -s 720x406 -y -strict experimental -acodec aac -ab 128k -ac 2 -ar 48000 -vcodec libx264 -g 48 -b 1000000 -threads 64  $path[0].mp4 2>&1");
		$video_info = ob_get_contents();
		ob_end_clean();
		var_dump($video_info);die;
		//exec("$ffmpeg -i $this->fileName -s 720x406 -y -strict experimental -acodec aac -ab 128k -ac 2 -ar 48000 -vcodec libx264 -vprofile main -g 48 -b 1000000 -threads 64  /tmp/escort/test.mp4 >> /tmp/escort/ffmpeg.log");
		//exec("$ffmpeg -i $this->fileName -s 720x406 -y -strict experimental -acodec aac -ab 128k -ac 2 -ar 48000 -vcodec libx264 -vprofile main -g 48 -b 1000000 -threads 64  $path[0].mp4");
		$this->fileName =$path[0].'.mp4';
		return $this->fileName;
	}
	
	public function AddMetaData($path='')
	{	
		$dir = sys_get_temp_dir().'/escort/';
		if(!is_dir($dir))
			mkdir($dir);
		$dir.=Cubix_Application::getById(Cubix_Application::getId())->host.'/';
		if(!is_dir($dir))
		mkdir($dir);
		$dir.=$this->escort_id .'/';
		if(!is_dir($dir))
			mkdir($dir);

		$file = $path !='' ? pathinfo($path,PATHINFO_FILENAME) : '';
		$dir = $dir.$file.'.flv';
		$yamdi = $this->config['yamdi_path'];
		//var_dump($dir, $this->fileName, $yamdi);die;
		exec("$yamdi -i $this->fileName -o $dir ");
		$this->fileName = $dir;
		return $this->fileName;
	}


    /**
     * @param $side
     */
    public function rotateOriginalTo($side = 1, $ext = 'mp4')
    {
        $pathInfo = pathinfo($this->fileName);
        $path = $pathInfo['dirname'] . DIRECTORY_SEPARATOR . $pathInfo['filename'];
        $ffmpeg = $this->config['ffmpeg_path'];
        exec("$ffmpeg -i $this->fileName -vf \"scale=960:720,transpose=$side\" {$path}_r$side.$ext ");
        $this->fileName = $path . "_r$side.$ext";
        return $this->fileName;
    }

	
	public function rotate($side)
	{
		$ffmpeg = $this->config['ffmpeg_path'];
		$new_filename = str_replace('.flv','_r.flv',$this->fileName );
		exec("$ffmpeg -y -i ".$this->fileName."  -ab 128k -ac 2 -vcodec flv1 -threads 0 -qscale:v 6 -vf 'transpose=".$side."' ". $new_filename);
		
		/*ob_start();
			//passthru("$ffmpeg -i ".$this->fileName." -qscale:v 5 -vf 'transpose=1' -ar 44100 -ab 96k -y ". $new_filename ." 2>&1");
			passthru("$ffmpeg -y -i ".$this->fileName." -c:v libx264  -crf 15  -c:a libmp3lame -b:a 192k -ac 2 ". $new_filename ." 2>&1");
		    //passthru("$ffmpeg -y -i ".$this->fileName." -vcodec libx264 -level 21 -refs 2 -b 500k -threads 0 -s 640x360 -frame_size 160 ". $new_filename ." 2>&1");
		$video_info = ob_get_contents();
		ob_end_clean();
		var_dump("$ffmpeg -y -i ".$this->fileName." -c:v libx264  -crf 15  -c:a libmp3lame -b:a 192k -ac 2 ". $new_filename ." 2>&1");
		var_dump($video_info);die;*/	
		$this->fileName = $new_filename;
		return $this->fileName;
	}		
}