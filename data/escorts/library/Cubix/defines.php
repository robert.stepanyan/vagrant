<?php

$app_id = Zend_Controller_Front::getInstance()->getRequest()->application_id;

if ( ! $app_id ) {
	$app_id = Cubix_Application::getId();
}

define('APP_EF',		1);
define('APP_6A',		2);
define('APP_EG_CO_UK',	5);
define('APP_SI',		6);
define('APP_EG',		7);
define('APP_EI',		9);
define('APP_AE',		11);
define('APP_A6',		16);
define('APP_A6_AT',		25);
define('APP_6B',		17);
define('APP_6C',		18);
define('APP_PC',		22);
define('APP_BL',		30);
define('APP_EM',		33);
define('APP_ED',		69);


define('PM_WESTERN_UNION',         1);
define('PM_MONEYGRAM',             2);
define('PM_CREDIT_CARD',           3);
define('PM_BANK_WIRE',             4);
define('PM_PAYPAL',                5);
define('PM_OTHER',                 6);
define('PM_CASH',                 7);

$DEFINITIONS['payment_method_options'] = array(
    PM_WESTERN_UNION    => 'Western Union',
    PM_MONEYGRAM		=> 'Moneygram',
    PM_CREDIT_CARD      => 'Credit Card',
    PM_BANK_WIRE		=> 'Bank Wire',
    PM_PAYPAL			=> 'PayPal',
    PM_CASH				=> 'Cash',
    PM_OTHER			=> Cubix_I18n::translate('other')
);

define('PERCENT', 1005);

define('ADMIN_TYPE_NORMAL',                 1);
define('ADMIN_TYPE_PAYMENT_MANAGER',        2);
define('ADMIN_TYPE_DATA_ENTRY',             3);
define('ADMIN_TYPE_SUPERUSER',              99); // superuser can be added only directly in db

$DEFINITIONS['ADMIN_TYPE'] = array(
    ADMIN_TYPE_NORMAL           => 'admin',
    ADMIN_TYPE_PAYMENT_MANAGER  => 'payment manager',
    ADMIN_TYPE_DATA_ENTRY       => 'data entry',
    ADMIN_TYPE_SUPERUSER		=> 'superadmin'
);

define('USER_STATUS_ACTIVE',                1);
define('USER_STATUS_NOT_VERIFIED',          -1);
define('USER_STATUS_DISABLED',              -4);
define('USER_STATUS_DELETED',               -5);
define('USER_STATUS_FRAUD',                 -6);

$DEFINITIONS['user_status_options'] = array(
	USER_STATUS_ACTIVE             => 'Active',
    USER_STATUS_DISABLED           => 'Disabled',
    USER_STATUS_NOT_VERIFIED       => 'Not verified',
    USER_STATUS_DELETED			   => 'Deleted'
    /*USER_STATUS_ACTIVE             => Cubix_I18n::translate('status_active'),
    USER_STATUS_DISABLED           => Cubix_I18n::translate('status_disabled'),
    USER_STATUS_NOT_VERIFIED       => Cubix_I18n::translate('status_not_verified')*/
);

define('AGENCY_STATUS_NO_PROFILE',          -1);
define('AGENCY_STATUS_OFFLINE',              0);
define('AGENCY_STATUS_ONLINE',               1);
define('AGENCY_STATUS_NOT_APPROVED',         2);


$DEFINITIONS['agency_status_options'] = array(
	AGENCY_STATUS_ONLINE              => 'Online',
	AGENCY_STATUS_OFFLINE             => 'Offline',
	AGENCY_STATUS_NOT_APPROVED        => 'Not Approved',
    AGENCY_STATUS_NO_PROFILE        => 'No Profile',
);

define('PROD_AVAILABLE_FOR_ALL',            1);
define('PROD_AVAILABLE_FOR_DOMESTIC',       2);
define('PROD_AVAILABLE_FOR_INTERNATIONAL',  3);

$DEFINITIONS['prod_available'] = array(
    PROD_AVAILABLE_FOR_ALL              => 'all',
    PROD_AVAILABLE_FOR_DOMESTIC         => 'domestic',
    PROD_AVAILABLE_FOR_INTERNATIONAL    => 'international'
);

define('ESCORT_STATUS_NO_PROFILE',		1);			// 000000000000001
define('ESCORT_STATUS_NO_ENOUGH_PHOTOS',	2);		// 000000000000010
define('ESCORT_STATUS_NOT_APPROVED',		4);		// 000000000000100
define('ESCORT_STATUS_OWNER_DISABLED',	8);			// 000000000001000
define('ESCORT_STATUS_ADMIN_DISABLED',	16);		// 000000000010000
define('ESCORT_STATUS_ACTIVE',			32);		// 000000000100000
define('ESCORT_STATUS_IS_NEW',			64);		// 000000001000000
define('ESCORT_STATUS_PROFILE_CHANGED',	128);		// 000000010000000
define('ESCORT_STATUS_DELETED',			256);		// 000000100000000
define('ESCORT_STATUS_FAKE_PICTURE_DISABLED',512);	// 000001000000000
define('ESCORT_STATUS_TEMPRARY_DELETED', 1024);		// 000010000000000
define('ESCORT_STATUS_SUSPICIOUS_DELETED',	2048);	// 000100000000000
define('ESCORT_STATUS_NEED_AGE_VERIFICATION',4096);	// 001000000000000
define('ESCORT_STATUS_OFFLINE',8192);		        // 010000000000000
define('ESCORT_STATUS_INACTIVE',16384);             // 100000000000000
define('ESCORT_STATUS_PARTIALLY_COMPLETE',32768);   // 1000000000000000
define('ESCORT_STATUS_IP_BLOCKED',65536);		    // 10000000000000000
define('ESCORT_STATUS_AWAITING_PAYMENT',131072);    // 100000000000000000

$DEFINITIONS['escort_status_options'] = array(
    ESCORT_STATUS_ACTIVE            => 'Active',
    ESCORT_STATUS_ADMIN_DISABLED	=> 'Admin Disabled',
    ESCORT_STATUS_OWNER_DISABLED	=> 'Owner Disabled',
    ESCORT_STATUS_IS_NEW			=> 'New',
    ESCORT_STATUS_PROFILE_CHANGED	=> 'Profile Changed',
    ESCORT_STATUS_NOT_APPROVED		=> 'Not Approved',
    ESCORT_STATUS_NO_ENOUGH_PHOTOS	=> 'No Enough Photos',
    ESCORT_STATUS_NO_PROFILE		=> 'No Profile',
    ESCORT_STATUS_DELETED			=> 'Deleted',
    ESCORT_STATUS_FAKE_PICTURE_DISABLED		=> 'Fake Picture Disabled',
	ESCORT_STATUS_TEMPRARY_DELETED  => 'Temporary Deleted',
	ESCORT_STATUS_SUSPICIOUS_DELETED		=> 'Suspicious Deleted',
    ESCORT_STATUS_OFFLINE	        => 'Offline',
    ESCORT_STATUS_INACTIVE          => 'Inactive',
    ESCORT_STATUS_IP_BLOCKED          => 'IP blacklisted',
);

$DEFINITIONS['escort_status_options_popup'] = array(
    ESCORT_STATUS_ACTIVE            => 'Active',
    ESCORT_STATUS_ADMIN_DISABLED	=> 'Admin Disabled',
    ESCORT_STATUS_OWNER_DISABLED	=> 'Owner Disabled',
    ESCORT_STATUS_OFFLINE	        => 'Offline',
    ESCORT_STATUS_INACTIVE          => 'Inactive',
);

if ($app_id == APP_ED){
    define('ESCORT_STATUS_AWAITING_PAYMENT', 131072);   // 100000000000000000
    $DEFINITIONS['escort_status_options'][ESCORT_STATUS_AWAITING_PAYMENT] = 'Awaiting Payment';
    $DEFINITIONS['escort_status_options_popup'][ESCORT_STATUS_AWAITING_PAYMENT] = 'Awaiting Payment';
}

define('STATUS_ACTIVE',                     1);
define('STATUS_NO_PHOTOS',                  -1);
define('STATUS_NO_PROFILE',                 -2);
define('STATUS_OWNER_DISABLED',             -3);
define('STATUS_ADMIN_DISABLED',             -4);
define('STATUS_WAITING_APPROVAL',           -5);
define('STATUS_NO_WORK_LOCATIONS',          -6);
define('STATUS_NO_LANGUAGES',               -7);
define('STATUS_NO_MAIN_PIC',                -8);
define('STATUS_EXPIRED',                    -9);
define('STATUS_DOMAIN_BLACKLISTED',         -10); // disabled because domain is blacklisted
define('STATUS_DELETED',                    -99);

$DEFINITIONS['status_options'] = array(
    STATUS_ACTIVE                   => 'Active',
    STATUS_NO_PHOTOS                => 'No photos',
    STATUS_NO_PROFILE               => 'No profile',
    STATUS_OWNER_DISABLED           => 'Owner disabled',
    STATUS_ADMIN_DISABLED           => 'Admin disabled',
    STATUS_WAITING_APPROVAL         => 'Not approved',
    STATUS_NO_WORK_LOCATIONS        => 'No working locations',
    STATUS_NO_LANGUAGES             => 'No languages',
    STATUS_NO_MAIN_PIC              => 'No main photo',
    STATUS_EXPIRED                  => 'Expired',
    STATUS_DOMAIN_BLACKLISTED       => 'Domain blacklisted',
    STATUS_DELETED                  => 'Deleted'
	/*STATUS_ACTIVE                   => Cubix_I18n::translate('status_active'),
    STATUS_NO_PHOTOS                => Cubix_I18n::translate('status_no_photos'),
    STATUS_NO_PROFILE               => Cubix_I18n::translate('status_no_profile'),
    STATUS_OWNER_DISABLED           => Cubix_I18n::translate('status_owner_disabled'),
    STATUS_ADMIN_DISABLED           => Cubix_I18n::translate('status_admin_disabled'),
    STATUS_WAITING_APPROVAL         => Cubix_I18n::translate('status_not_approved'),
    STATUS_NO_WORK_LOCATIONS        => Cubix_I18n::translate('status_no_working_locations'),
    STATUS_NO_LANGUAGES             => Cubix_I18n::translate('status_no_languages'),
    STATUS_NO_MAIN_PIC              => Cubix_I18n::translate('status_no_main_photo'),
    STATUS_EXPIRED                  => Cubix_I18n::translate('status_expired'),
    STATUS_DOMAIN_BLACKLISTED       => Cubix_I18n::translate('status_domain_blacklisted'),
    STATUS_DELETED                  => Cubix_I18n::translate('status_deleted')*/
);

define('ESCORT_NOT_VERIFIED',               1);
define('ESCORT_VERIFIED',                   2);
define('ESCORT_REQUESTED_REMOVE',           3);
define('ESCORT_CALLED_1',                   4);
define('ESCORT_CALLED_2',                   5);
define('ESCORT_CALLED_3',                   6);
define('ESCORT_INVALID_NUMBER',             7);
define('ESCORT_REQUESTED_REMOVE_WAVED',     8);
define('ESCORT_SELF_REGISTERED',            9);
define('ESCORT_CALLED_0',                  10);

$DEFINITIONS['ESCORT_VERIFICATION'] = array(
    ESCORT_NOT_VERIFIED             => 'not verified',
    ESCORT_VERIFIED                 => 'verified',
    ESCORT_REQUESTED_REMOVE         => 'requested remove',
    ESCORT_REQUESTED_REMOVE_WAVED   => 'requested remove, waved because new ads',
	ESCORT_CALLED_0                 => 'not called',
    ESCORT_CALLED_1                 => 'called 1 time',
    ESCORT_CALLED_2                 => 'called 2 times',
    ESCORT_CALLED_3                 => 'called 3 times',
    ESCORT_INVALID_NUMBER           => 'invalid number',
    ESCORT_SELF_REGISTERED          => 'self registered'
);

define('AGENCY_NOT_VERIFIED',               1);
define('AGENCY_VERIFIED',                   2);
define('AGENCY_INVALID_CONTACT',            3);
define('AGENCY_CALLED_1',                   4);
define('AGENCY_CALLED_2',                   5);
define('AGENCY_CALLED_0',                   6);

$DEFINITIONS['AGENCY_VERIFICATION'] = array(
    AGENCY_NOT_VERIFIED             => 'not verified',
    AGENCY_VERIFIED                 => 'verified',
    AGENCY_INVALID_CONTACT          => 'invalid contact',
    AGENCY_CALLED_0                 => 'not called',
    AGENCY_CALLED_1                 => 'called 1 time',
    AGENCY_CALLED_2                 => 'called 2 times'
    
);

// colors
define('COLOR_BLACK',                       1);
define('COLOR_WHITE',                       2);
define('COLOR_RED',                         3);
define('COLOR_BLOND',                       4);
define('COLOR_BROWN',                       5);
define('COLOR_GREEN',                       6);
define('COLOR_YELLOW',                      7);
define('COLOR_BLUE',                        8);
define('COLOR_GRAY',                        9);
define('COLOR_BRUNETTE',                    10);
define('COLOR_HAZEL',                       11);
define('COLOR_OTHER',                       12);

$DEFINITIONS['hair_color_options'] = array(
	COLOR_BLOND => Cubix_I18n::translate('color_blond'),
	COLOR_BROWN => Cubix_I18n::translate('color_light_brown'),
	COLOR_BRUNETTE => Cubix_I18n::translate('color_brunette'),
	COLOR_BLACK => Cubix_I18n::translate('color_black'),
	COLOR_RED => Cubix_I18n::translate('color_red'),
	COLOR_OTHER => Cubix_I18n::translate('color_other')
);

// hair length
define('LENGTH_VERY_SHORT',     1);
define('LENGTH_SHORT',          2);
define('LENGTH_MEDIUM',         3);
define('LENGTH_LONG',           4);
define('LENGTH_VERY_LONG',      5);

$DEFINITIONS['hair_length_options'] = array(
	LENGTH_VERY_SHORT => Cubix_I18n::translate('length_very_short'),
	LENGTH_SHORT => Cubix_I18n::translate('length_short'),
	LENGTH_MEDIUM => Cubix_I18n::translate('length_medium'),
	LENGTH_LONG => Cubix_I18n::translate('length_long'),
	LENGTH_VERY_LONG => Cubix_I18n::translate('length_very_long')
);

define('YES',                               1);
define('NO',                                2);
define('OCASIONALLY',                       3);
define('NOT_AVAILABLE',                     98);
define('DONT_KNOW',                         99);
define('NEGOTIABLE',                        101);

$DEFINITIONS['yes_no_options'] = array(
   YES                              => Cubix_I18n::translate('yes'),
   NO                               => Cubix_I18n::translate('no')
);

$DEFINITIONS['y_n_na_options'] = array(
   YES                              => Cubix_I18n::translate('yes'),
   NO                               => Cubix_I18n::translate('no'),
   NOT_AVAILABLE                    => Cubix_I18n::translate('not_available')
);

$DEFINITIONS['y_n_dn_options'] = array(
   YES                              => Cubix_I18n::translate('yes'),
   NO                               => Cubix_I18n::translate('no'),
   DONT_KNOW						=> Cubix_I18n::translate('dont_know')
);

$DEFINITIONS['y_n_oc_options'] = array(
   YES                              => Cubix_I18n::translate('yes'),
   NO                               => Cubix_I18n::translate('no'),
   OCASIONALLY						=> Cubix_I18n::translate('ocasionally')
);

$DEFINITIONS['y_n_neg_options'] = array(
   NO                               => Cubix_I18n::translate('no'),
   YES                              => Cubix_I18n::translate('yes'),
   NEGOTIABLE						=> Cubix_I18n::translate('negotiable')
);

$DEFINITIONS['eye_color_options'] = array(
	COLOR_BLACK => Cubix_I18n::translate('color_black'),
	COLOR_BROWN => Cubix_I18n::translate('color_brown'),
	COLOR_GREEN => Cubix_I18n::translate('color_green'),
	COLOR_BLUE => Cubix_I18n::translate('color_blue'),
	COLOR_GRAY => Cubix_I18n::translate('color_gray')
);

$DEFINITIONS['breast_size_options'] = array(1 => 'A', 'B', 'C', 'D', 'DD', 'F', 'FF', 'G', 'H', 'J' );

define('BREAST_TYPE_NATURAL',	1);
define('BREAST_TYPE_SILICONE',	2);

$DEFINITIONS['breast_type_options'] = array(
	BREAST_TYPE_NATURAL	   => Cubix_I18n::translate('breast_type_natural'),
	BREAST_TYPE_SILICONE   => Cubix_I18n::translate('breast_type_silicone'),
);

define('PUBIC_HAIR_SHAVED_COMPLETELY',		1);
define('PUBIC_HAIR_SHAVED_MOSTLY',			2);
define('PUBIC_HAIR_TRIMMED',				3);
define('PUBIC_HAIR_ALL_NATURAL',			4);

$DEFINITIONS['pubic_hair_options'] = array(
	PUBIC_HAIR_SHAVED_COMPLETELY	=> Cubix_I18n::translate('pubic_hair_shaved_completely'),
	PUBIC_HAIR_SHAVED_MOSTLY		=> Cubix_I18n::translate('pubic_hair_shaved_mostly'),
	PUBIC_HAIR_TRIMMED				=> Cubix_I18n::translate('pubic_hair_trimmed'),
	PUBIC_HAIR_ALL_NATURAL			=> Cubix_I18n::translate('pubic_hair_all_natural'),
);

$DEFINITIONS['tatoo_options'] = array(
   NO                               => Cubix_I18n::translate('no'),
   YES                              => Cubix_I18n::translate('yes')
);

$DEFINITIONS['piercing_options'] = array(
   NO                               => Cubix_I18n::translate('no'),
   YES                              => Cubix_I18n::translate('yes')
);
$DEFINITIONS['vaccinated_options'] = array(
   NO                               => Cubix_I18n::translate('no'),
   YES                              => Cubix_I18n::translate('yes')
);

// user type
define('USER_TYPE_SINGLE_GIRL',             1);
define('USER_TYPE_AGENCY',                  2);
define('USER_TYPE_MEMBER',                  3);
define('USER_TYPE_CLUB',                    4);

// gender
define('GENDER_FEMALE',                     1);
define('GENDER_MALE',                       2);
define('GENDER_TRANS',                      3);
define('GENDER_DOMINA',                     4);
define('GENDER_BDSM',                       5);

$DEFINITIONS['gender_options'] = array(
    GENDER_FEMALE                   => Cubix_I18n::translate('female'),
    GENDER_MALE                     => Cubix_I18n::translate('male'),
    GENDER_TRANS                    => Cubix_I18n::translate('trans'),
    //GENDER_DOMINA                   => Cubix_I18n::translate('domina')
);


// gender DUO for packages
define('GENDER_DUO_FEMALE',                     11);
define('GENDER_DUO_MALE',                       22);
define('GENDER_DUO_TRANS',                      33);
define('GENDER_DUO_DOMINA',                     44);
define('GENDER_DUO_BDSM',                       55);
$DEFINITIONS['gender_duo_options'] = array(
    GENDER_DUO_FEMALE                   => Cubix_I18n::translate('female_duo'),
    /*GENDER_DUO_MALE                     => Cubix_I18n::translate('male_duo'),
    GENDER_DUO_TRANS                    => Cubix_I18n::translate('trans_duo'),*/
);

define('PROFILE_TYPE_SINGLE',                    1);
define('PROFILE_TYPE_DUO',                       2);

$DEFINITIONS['profile_type_options'] = array(
    PROFILE_TYPE_SINGLE                   => Cubix_I18n::translate('profile_type_single'),
    PROFILE_TYPE_DUO                     => Cubix_I18n::translate('profile_type_duo'),
);

// Sexual Orientation
define('ORIENTATION_HETEROSEXUAL',           1);
define('ORIENTATION_BISEXUAL',               2);
define('ORIENTATION_HOMOSEXUAL',             3);


$DEFINITIONS['sexual_orientation_options'] = array(
    ORIENTATION_HETEROSEXUAL  => Cubix_I18n::translate('heterosexual'),
    ORIENTATION_BISEXUAL      => Cubix_I18n::translate('bisexual'),
    ORIENTATION_HOMOSEXUAL    => Cubix_I18n::translate('homosexual')
);

define('PHONE_INSTR_SMS_CALL',           1);
define('PHONE_INSTR_SMS_ONLY',           2);
define('PHONE_INSTR_NO_SMS',             3);

define('PHONE_INSTR_NO_WITHHELD',        1);

$DEFINITIONS['phone_instr_options'] = array(
    PHONE_INSTR_SMS_CALL  => Cubix_I18n::translate('phone_instr_sms_call'),
    PHONE_INSTR_SMS_ONLY  => Cubix_I18n::translate('phone_instr_sms_only'),
    PHONE_INSTR_NO_SMS    => Cubix_I18n::translate('phone_instr_no_sms')
);

$DEFINITIONS['phone_instr_no_withheld_options'] = array(
    PHONE_INSTR_NO_WITHHELD  => Cubix_I18n::translate('phone_instr_no_withheld')
);

$incall_types = array(
		'1' => 'private_apartment',
		'2' => 'hotel_room',
		'3' => 'club_studio',
		'4' => ''
	);
	$outcall_types = array(
		'1' => 'hotel_visits_only',
		'2' => 'home_visits_only',
		'3' => 'hotel_and_home_visits',
		'4' => ''
	);

define('AVAIL_INCALL_TYPE_PRIVATE_APARTMENT',       1);
define('AVAIL_INCALL_TYPE_HOTEL_ROOM',				2);
define('AVAIL_INCALL_TYPE_CLUB_STUDIO',				3);
define('AVAIL_INCALL_TYPE_OTHER',					4);

define('AVAIL_OUTCALL_TYPE_HOTEL_VISITS_ONLY',      1);
define('AVAIL_OUTCALL_TYPE_HOME_VISITS_ONLY',       2);
define('AVAIL_OUTCALL_TYPE_HOTEL_HOME_VISITS',      3);
define('AVAIL_OUTCALL_TYPE_OTHER',					4);

$DEFINITIONS['avail_incall_type_options'] = array(
    AVAIL_INCALL_TYPE_PRIVATE_APARTMENT  => Cubix_I18n::translate('private_apartment'),
    AVAIL_INCALL_TYPE_HOTEL_ROOM	=> Cubix_I18n::translate('hotel_room'),
    AVAIL_INCALL_TYPE_CLUB_STUDIO	=> Cubix_I18n::translate('club_studio'),
    AVAIL_INCALL_TYPE_OTHER			=> ''
);

$DEFINITIONS['avail_outcall_type_options'] = array(
    AVAIL_OUTCALL_TYPE_HOTEL_VISITS_ONLY  => Cubix_I18n::translate('hotel_visits_only'),
    AVAIL_OUTCALL_TYPE_HOME_VISITS_ONLY  => Cubix_I18n::translate('home_visits_only'),
    AVAIL_OUTCALL_TYPE_HOTEL_HOME_VISITS  => Cubix_I18n::translate('hotel_and_home_visits'),
    AVAIL_OUTCALL_TYPE_OTHER  => ''
);

define('KISSING_NO',                        11);
define('KISSING',                           12);
define('KISSING_WITH_TONGUE',               13);

define('BLOWJOB_NO',                        11);
define('BLOWJOB_WITH_CONDOM',               12);
define('BLOWJOB_WITHOUT_CONDOM',            13);

define('CUMSHOT_NO',                        11);
define('CUMSHOT_IN_MOUTH_SPIT',             12);
define('CUMSHOT_IN_MOUTH_SWALLOW',          13);
define('CUMSHOT_IN_FACE',                   14);

// metric system
define('METRIC_SYSTEM',                     1);
define('ROYAL_SYSTEM',                      2);


$DEFINITIONS['metric_system'] = array(
    METRIC_SYSTEM               => Cubix_I18n::translate('metric'),
    ROYAL_SYSTEM                => Cubix_I18n::translate('royal')
);

$DEFINITIONS['kissing_options'] = array(
	KISSING                         => Cubix_I18n::translate('yes'),
	KISSING_WITH_TONGUE             => Cubix_I18n::translate('kissing_with_tongue'),
	KISSING_NO                      => Cubix_I18n::translate('no')
);

$DEFINITIONS['blowjob_options'] = array(
	BLOWJOB_WITHOUT_CONDOM          => Cubix_I18n::translate('blowjob_without_condom'),
	BLOWJOB_WITH_CONDOM             => Cubix_I18n::translate('blowjob_with_condom'),
	BLOWJOB_NO                      => Cubix_I18n::translate('no')
);

$DEFINITIONS['cumshot_options'] = array(
    CUMSHOT_IN_MOUTH_SPIT           => Cubix_I18n::translate('cumshot_in_mouth_spit'),
    CUMSHOT_IN_MOUTH_SWALLOW        => Cubix_I18n::translate('cumshot_in_mouth_swallow'),
    CUMSHOT_IN_FACE                 => Cubix_I18n::translate('cumshot_in_face'),
	CUMSHOT_NO                      => Cubix_I18n::translate('no')
);

// time units
define('TIME_MINUTES',                      1);
define('TIME_HOURS',                        2);
define('TIME_DAYS',                         3);
define('TIME_OVERNIGHT',                    4);

$DEFINITIONS['time_unit_options'] = array(
    TIME_MINUTES                    => Cubix_I18n::translate('minutes'),
    TIME_HOURS                      => Cubix_I18n::translate('hours'),
    TIME_DAYS                       => Cubix_I18n::translate('days'),
    TIME_OVERNIGHT                  => Cubix_I18n::translate('time_overnight')
);

$DEFINITIONS['hours_days_options'] = array(
    TIME_HOURS                      => Cubix_I18n::translate('hours'),
    TIME_DAYS                       => Cubix_I18n::translate('days')
);

// ethnic
define('ETHNIC_WHITE',                          1);
define('ETHNIC_BLACK',                          2);
define('ETHNIC_ASIAN',                          3);
define('ETHNIC_LATIN',                          4);
define('ETHNIC_AFRICAN',                        5);
define('ETHNIC_INDIAN',                         6);
define('ETHNIC_ARABIAN',                        8);
define('ETHNIC_MIXED',                        9);
define('ETHNIC_CAUCASIAN',                        10);

$DEFINITIONS['ethnic_options'] = array(
	ETHNIC_ASIAN                    => Cubix_I18n::translate('ethnic_asian'),
	ETHNIC_BLACK                    => Cubix_I18n::translate('ethnic_black'),
    ETHNIC_WHITE                    => Cubix_I18n::translate('ethnic_white'),
    ETHNIC_LATIN                    => Cubix_I18n::translate('ethnic_latin'),
	ETHNIC_MIXED					=> Cubix_I18n::translate('ethnic_mixed'),
    ETHNIC_INDIAN                   => Cubix_I18n::translate('ethnic_indian'),
    ETHNIC_ARABIAN                   => Cubix_I18n::translate('ethnic_arabian'),
    ETHNIC_CAUCASIAN                   => Cubix_I18n::translate('ethnic_caucasian')

    // ETHNIC_AFRICAN                  => Cubix_I18n::translate('ethnic_african'),
    
);

define('AVAILABLE_INCALL',                  1);
define('AVAILABLE_OUTCALL',                 2);
define('AVAILABLE_INCALL_OUTCALL',          3);
define('AVAILABLE_SAUNA',                   4);
define('AVAILABLE_SALON',                   5);
define('AVAILABLE_APARTMENT',               6);
define('AVAILABLE_LOFT',                    7);

$DEFINITIONS['available_for']               = array(
	AVAILABLE_APARTMENT 			=> Cubix_I18n::translate('apartment'),
	AVAILABLE_INCALL 				=> Cubix_I18n::translate('incall'),
	AVAILABLE_OUTCALL 				=> Cubix_I18n::translate('outcall'),
	AVAILABLE_INCALL_OUTCALL 		=> Cubix_I18n::translate('incall_outcall'),
	AVAILABLE_SAUNA 				=> Cubix_I18n::translate('sauna'),
	AVAILABLE_SALON 				=> Cubix_I18n::translate('salon')
);

define('AVAILABILITY_INCALL_PRIVATE_APARTMENT', 1);
define('AVAILABILITY_INCALL_HOTEL_ROOM', 2);
define('AVAILABILITY_INCALL_CLUB_STUDIO', 3);
define('AVAILABILITY_INCALL_OTHER', 4);
define('AVAILABILITY_OUTCALL_HOTEL_VISITS', 1);
define('AVAILABILITY_OUTCALL_HOME_VISITS', 2);
define('AVAILABILITY_OUTCALL_HOTEL_HOME_VISITS', 3);
define('AVAILABILITY_OUTCALL_OTHER', 4);


$DEFINITIONS['filter_availability_options'] = array(
	'i_' . AVAILABILITY_INCALL_PRIVATE_APARTMENT => Cubix_I18n::translate('filter_incall_private_apartment'),
	'i_' . AVAILABILITY_INCALL_HOTEL_ROOM => Cubix_I18n::translate('filter_incall_hotel_room'),
	'i_' . AVAILABILITY_INCALL_CLUB_STUDIO => Cubix_I18n::translate('filter_incall_club_studio'),
	
	'o_' . AVAILABILITY_OUTCALL_HOTEL_VISITS => Cubix_I18n::translate('filter_outcall_hotel_visits'),
	'o_' . AVAILABILITY_OUTCALL_HOME_VISITS => Cubix_I18n::translate('filter_outcall_home_visits'),
	'o_' . AVAILABILITY_OUTCALL_HOTEL_HOME_VISITS => Cubix_I18n::translate('filter_outcall_hotel_home_visits'),
);

$DEFINITIONS['filter_availability_options_incall'] = array(    
    'i_' . AVAILABILITY_INCALL_PRIVATE_APARTMENT => Cubix_I18n::translate('filter_incall_private_apartment'),
    'i_' . AVAILABILITY_INCALL_HOTEL_ROOM => Cubix_I18n::translate('filter_incall_hotel_room'),
    'i_' . AVAILABILITY_INCALL_CLUB_STUDIO => Cubix_I18n::translate('filter_incall_club_studio'),
);
$DEFINITIONS['filter_availability_options_outcall'] = array(    
    'o_' . AVAILABILITY_OUTCALL_HOTEL_VISITS => Cubix_I18n::translate('filter_outcall_hotel_visits'),
    'o_' . AVAILABILITY_OUTCALL_HOME_VISITS => Cubix_I18n::translate('filter_outcall_home_visits'),
    'o_' . AVAILABILITY_OUTCALL_HOTEL_HOME_VISITS => Cubix_I18n::translate('filter_outcall_hotel_home_visits'),
);

if ($app_id == APP_ED)
{
	$DEFINITIONS['filter_availability_options_incall']['i_'. AVAILABILITY_INCALL_OTHER] = Cubix_I18n::translate('other');
	$DEFINITIONS['filter_availability_options_outcall']['o_'. AVAILABILITY_OUTCALL_OTHER ] = Cubix_I18n::translate('other');
	$DEFINITIONS['filter_availability_options']['i_'. AVAILABILITY_INCALL_OTHER] = Cubix_I18n::translate('other');
	$DEFINITIONS['filter_availability_options']['o_'. AVAILABILITY_OUTCALL_OTHER ] = Cubix_I18n::translate('other');
}

/* Travel Places EM */
define('TRAVEL_WORLDWIDE_I_USA', 1);
define('TRAVEL_WORLDWIDE_E_USA', 2);
define('TRAVEL_EUROPE', 3);
define('TRAVEL_NO_TRAVEL', 4);

$DEFINITIONS['travel_places'] = array(
    TRAVEL_NO_TRAVEL => Cubix_I18n::translate('travel_places_1'),
    TRAVEL_WORLDWIDE_I_USA => Cubix_I18n::translate('travel_places_2'),
    TRAVEL_WORLDWIDE_E_USA => Cubix_I18n::translate('travel_places_3'),
    TRAVEL_EUROPE => Cubix_I18n::translate('travel_places_4')
);


if ( $app_id == APP_ED ) {
    define('TRAVEL_NATIONWIDE', 5);
	$DEFINITIONS['travel_places'][TRAVEL_NATIONWIDE] = Cubix_I18n::translate('travel_places_5');
}
//

/* Travel Places PC */
define('WORLDWIDE', 1);
define('USA_AND_CANADA', 2);
define('USA_ONLY', 3);

$DEFINITIONS['travel_places_pc'] = array(
	WORLDWIDE => 'Worldwide',
	USA_AND_CANADA => 'USA and Canada',
	USA_ONLY => 'USA only'
);
//

define('SEXUALLY_AVAILABLE_FOR_MEN', 'men');
define('SEXUALLY_AVAILABLE_FOR_WOMEN', 'women');
define('SEXUALLY_AVAILABLE_FOR_COUPLES', 'couples');
define('SEXUALLY_AVAILABLE_FOR_TRANS', 'trans');
define('SEXUALLY_AVAILABLE_FOR_GAYS', 'gays');
define('SEXUALLY_AVAILABLE_FOR_PLUS2', 'plus2');

$DEFINITIONS['sex_availability_options'] = array(
	SEXUALLY_AVAILABLE_FOR_MEN => Cubix_I18n::translate('men'),
	SEXUALLY_AVAILABLE_FOR_WOMEN => Cubix_I18n::translate('women'),
	SEXUALLY_AVAILABLE_FOR_COUPLES => Cubix_I18n::translate('couples'),
	SEXUALLY_AVAILABLE_FOR_TRANS => Cubix_I18n::translate('trans'),
	SEXUALLY_AVAILABLE_FOR_GAYS => Cubix_I18n::translate('gays'),
	SEXUALLY_AVAILABLE_FOR_PLUS2 => Cubix_I18n::translate('plus2'),
);

define('KEYWORD_VIP_ESCORT',		1);
define('KEYWORD_CENTERFOLD',		2);
define('KEYWORD_MODEL_EX_MODEL',	3);
define('KEYWORD_XXX_PORN_STAR',		4);
define('KEYWORD_SUPER_BUSTY',		5);
define('KEYWORD_SUPER_BOOTY',		6);
define('KEYWORD_PETITE',			7);
define('KEYWORD_ALL_NATURAL',		8);
define('KEYWORD_MATURE',			9);
define('KEYWORD_COLLEGE_GIRL',		10);
define('KEYWORD_PROF_MISTRESS',		11);
define('KEYWORD_BBW',		        12);

$DEFINITIONS['keywords'] = array(
	KEYWORD_VIP_ESCORT      => Cubix_I18n::translate('vip_escort'),
	KEYWORD_CENTERFOLD      => Cubix_I18n::translate('centerfold'),
	KEYWORD_MODEL_EX_MODEL  => Cubix_I18n::translate('model_ex_model'),
	KEYWORD_XXX_PORN_STAR   => Cubix_I18n::translate('xxx_porn_star'),
	KEYWORD_SUPER_BUSTY     => Cubix_I18n::translate('super_busty'),
	KEYWORD_SUPER_BOOTY     => Cubix_I18n::translate('super_booty'),
	KEYWORD_PETITE          => Cubix_I18n::translate('petite'),
	KEYWORD_ALL_NATURAL     => Cubix_I18n::translate('all_natural'),
	KEYWORD_MATURE          => Cubix_I18n::translate('mature'),
	KEYWORD_COLLEGE_GIRL    => Cubix_I18n::translate('college_girl'),
	KEYWORD_PROF_MISTRESS   => Cubix_I18n::translate('prof_mistress'),
	KEYWORD_BBW             => 'BBW'
);

define('KEYWORD_ED_VIP_COMPANION',		1);
define('KEYWORD_ED_MUSCULAR',		    2);
define('KEYWORD_ED_MODEL_EX_MODEL',	    3);
define('KEYWORD_ED_OCCASIONAL',	        4);
define('KEYWORD_ED_SUPER_BUSTY',		5);
define('KEYWORD_ED_SUPER_BOOTY',		6);
define('KEYWORD_ED_PETITE',			    7);
define('KEYWORD_ED_ALL_NATURAL',		8);
define('KEYWORD_ED_COLLEGE_GIRL',		9);
define('KEYWORD_ED_BBW',		        10);
define('KEYWORD_ED_FITNESS_GIRL',		11);
define('KEYWORD_ED_MATURE',     		12);

$DEFINITIONS['keywords_ed'] = array(
	KEYWORD_ED_VIP_COMPANION    => Cubix_I18n::translate('vip_companion'),
	KEYWORD_ED_MUSCULAR         => Cubix_I18n::translate('muscular'),
	KEYWORD_ED_MODEL_EX_MODEL   => Cubix_I18n::translate('model_ex_model'),
	KEYWORD_ED_OCCASIONAL       => Cubix_I18n::translate('occasional'),
	KEYWORD_ED_SUPER_BUSTY      => Cubix_I18n::translate('super_busty'),
	KEYWORD_ED_SUPER_BOOTY      => Cubix_I18n::translate('super_booty'),
	KEYWORD_ED_PETITE           => Cubix_I18n::translate('petite'),
	KEYWORD_ED_ALL_NATURAL      => Cubix_I18n::translate('all_natural'),
	KEYWORD_ED_COLLEGE_GIRL     => Cubix_I18n::translate('college_girl'),
	KEYWORD_ED_BBW              => 'BBW',
	KEYWORD_ED_FITNESS_GIRL     => Cubix_I18n::translate('fitness_girl'),
	KEYWORD_ED_MATURE           => Cubix_I18n::translate('mature')
);

$DEFINITIONS['language_options'] = array(
	'en' => Cubix_I18n::translate('language_en'),
	'fr' => Cubix_I18n::translate('language_fr'),
	'de' => Cubix_I18n::translate('language_de'),
	'it' => Cubix_I18n::translate('language_it'),
	'pt' => Cubix_I18n::translate('language_pt'),
	'ru' => Cubix_I18n::translate('language_ru'),
	'es' => Cubix_I18n::translate('language_es'),
	'sa' => Cubix_I18n::translate('languages_sa'),
	//'be' => Cubix_I18n::translate('languages_be'),
	'bg' => Cubix_I18n::translate('languages_bg'),
	'cn' => Cubix_I18n::translate('languages_cn'),
	'cz' => Cubix_I18n::translate('languages_cz'),
	'hr' => Cubix_I18n::translate('languages_hr'),
	'nl' => Cubix_I18n::translate('languages_nl'),
	'fi' => Cubix_I18n::translate('languages_fi'),
	'gr' => Cubix_I18n::translate('languages_gr'),
	'hu' => Cubix_I18n::translate('languages_hu'),
	'in' => Cubix_I18n::translate('languages_in'),
	'jp' => Cubix_I18n::translate('languages_jp'),
	'lv' => Cubix_I18n::translate('languages_lv'),
	'pl' => Cubix_I18n::translate('languages_pl'),
	'ro' => Cubix_I18n::translate('languages_ro'),
	'tr' => Cubix_I18n::translate('language_tr'),
	'sk' => Cubix_I18n::translate('language_sk'),
	'sl' => Cubix_I18n::translate('language_sl'),
	'dk' => Cubix_I18n::translate('language_dk'),
	'no' => Cubix_I18n::translate('language_no'),
	'se' => Cubix_I18n::translate('language_se'),
);



if ($app_id == APP_BL) {
    $DEFINITIONS['language_options'] = array_merge($DEFINITIONS['language_options'], array(
        'lu' => Cubix_I18n::translate('language_lu'),
    ));
}



if ($app_id == APP_ED)
{
	$DEFINITIONS['language_options'] = array_merge($DEFINITIONS['language_options'], array(
		'hk' => Cubix_I18n::translate('language_hk'),
		'bd' => Cubix_I18n::translate('language_bd'),
		'kr' => Cubix_I18n::translate('language_kr'),
		'vn' => Cubix_I18n::translate('language_vn'),
		'ua' => Cubix_I18n::translate('language_ua'),
		'ir' => Cubix_I18n::translate('language_ir'),
		'np' => Cubix_I18n::translate('language_np'),
		'az' => Cubix_I18n::translate('language_az'),
		'am' => Cubix_I18n::translate('language_am'),
		'ne' => Cubix_I18n::translate('language_ne'),
		'mm' => Cubix_I18n::translate('language_mm'),
		'rs' => Cubix_I18n::translate('language_rs'),
		'th' => Cubix_I18n::translate('language_th'),
		'ng' => Cubix_I18n::translate('language_ng'),
		'pk' => Cubix_I18n::translate('language_pk'),
		'sv' => Cubix_I18n::translate('language_sv'),
		'no' => Cubix_I18n::translate('language_no'),
		'da' => Cubix_I18n::translate('language_da'),
		'et' => Cubix_I18n::translate('language_et'),
        'pk_si' => Cubix_I18n::translate('language_pk_si'),
        'id_jv' => Cubix_I18n::translate('language_id_jv'),
        'id_su' => Cubix_I18n::translate('language_id_su'),
        'in_hi' => Cubix_I18n::translate('language_in_hi'),
        'in_tl' => Cubix_I18n::translate('language_in_tl'),
        'in_mr' => Cubix_I18n::translate('language_in_mr'),
        'in_ta' => Cubix_I18n::translate('language_in_ta'),
        'in_gu' => Cubix_I18n::translate('language_in_gu'),
        'in_ml' => Cubix_I18n::translate('language_in_ml'),
        'in_kn' => Cubix_I18n::translate('language_in_kn'),
        'in_or' => Cubix_I18n::translate('language_in_or'),
        'in_pa' => Cubix_I18n::translate('language_in_pa'),
        'in_ma' => Cubix_I18n::translate('language_in_ma'),
        'in_aw' => Cubix_I18n::translate('language_in_aw'),
        'cn_ma' => Cubix_I18n::translate('language_cn_ma'),
        'cn_wu' => Cubix_I18n::translate('language_cn_wu'),
        'cn_mn' => Cubix_I18n::translate('language_cn_mn'),
        'cn_ji' => Cubix_I18n::translate('language_cn_ji'),
        'cn_xi' => Cubix_I18n::translate('language_cn_xi'),
        'cn_ha' => Cubix_I18n::translate('language_cn_ha'),
        'cn_ga' => Cubix_I18n::translate('language_cn_ga'),
        'af' => Cubix_I18n::translate('language_af'),
        'lx' => Cubix_I18n::translate('language_lx'),
	));
}

$DEFINITIONS['language_level_options'] = array(
	1 => '1 (' . Cubix_I18n::translate('basic') . ')',
	2 => '2',
	3 => '3',
	4 => '4 (' . Cubix_I18n::translate('fluent') . ')'
);

$_db = Zend_Registry::get('db');

$DEFINITIONS['currencies'] = array(
	2 => 'EUR',
	1 => 'USD',
	3 => 'GBP',
	4 => 'CHF'
);

$DEFINITIONS['currency_symbols'] = array(
	1 => '$',
	2 => '&euro;',
	3 => '&pound;',
	4 => 'CHF'
);

$DEFINITIONS['biography'] = array(
	METRIC_SYSTEM => array(
		'height' => range(135, 195),
		'weight' => range(40, 200),
		'dress_size' => array(1 => 'XS', 'S', 'M', 'L', 'XL', 'XXL'),
		'shoe_size' => range(34, 46)
	),
	ROYAL_SYSTEM => array(
		'height' => array(), //range(135, 195),
		'weight' => range(80, 264), //range(40, 200),
		'dress_size' => array(1 => 'XS', 'S', 'M', 'L', 'XL', 'XXL'),
		'shoe_size' => range(34, 46)
	)
);

for ( $ft = 4; $ft <= 6; $ft++ ) {
	for ( $in = 1; $in <= 11; $in++ ) {
		$DEFINITIONS['biography'][ROYAL_SYSTEM]['height'][] = $ft . '\' ' . $in . '"';
	}
}


define('FLIGHT_RESERVATION_ESCORT', 1);
define('FLIGHT_RESERVATION_CUSTOMER', 2);
define('FLIGHT_RESERVATION_NEGOTIABLE', 3);

$DEFINITIONS['flight_reservation_by'] = array(
	FLIGHT_RESERVATION_ESCORT => Cubix_I18n::translate('by_escort'),
	FLIGHT_RESERVATION_CUSTOMER => Cubix_I18n::translate('by_customer'),
	FLIGHT_RESERVATION_NEGOTIABLE => Cubix_I18n::translate('negotiable')
);

define('ESCORT_PHOTO_TYPE_HARD', 1);
define('ESCORT_PHOTO_TYPE_SOFT', 2);
define('ESCORT_PHOTO_TYPE_PRIVATE', 3);
define('ESCORT_PHOTO_TYPE_DISABLED', 4);
define('ESCORT_PHOTO_TYPE_ARCHIVED', 5);

$DEFINITIONS['escorts_filter'] = require_once('filter.php');
$DEFINITIONS['escorts_filter_v2'] = require_once('filter-v2.php');
$DEFINITIONS['escorts_sort'] = require_once('sort.php');
$DEFINITIONS['agency_escorts_sort'] = require_once('agency_escorts_sort.php');
$DEFINITIONS['agency_escorts_pages'] = require_once('agency_escorts_pages.php');
$DEFINITIONS['escorts_cities_filter'] = require_once('cities-filter.php');

// sms statuses
define('SMS_NOT_SENT',                      0); // failed to be sent, or sms is turned off
define('SMS_SENT',                          1); // sent but no reply received
define('SMS_CONFIRMED',                     2); // received a "yes" reply
define('SMS_REJECTED',                      3); // received a "no" reply
define('SMS_MISMATCH',                      4); // the reply was not understood

$DEFINITIONS['sms_status_options'] = array(
	SMS_NOT_SENT => Cubix_I18n::translate('sms_not_sent'),
	SMS_SENT => Cubix_I18n::translate('sms_sent'),
	SMS_CONFIRMED => Cubix_I18n::translate('sms_confirmed'),
	SMS_REJECTED => Cubix_I18n::translate('sms_rejected'),
	SMS_MISMATCH => Cubix_I18n::translate('sms_mismatch')
);

// newletters statuses
define('NEWSLETTER_PENDING',	1);
define('NEWSLETTER_SENDING',	2);
define('NEWSLETTER_SENT',		3);
define('NEWSLETTER_CANCELED',	4);

$DEFINITIONS['newsletter_status_options'] = array(
	NEWSLETTER_PENDING => 'Pending',
	NEWSLETTER_SENDING => 'Sending',
	NEWSLETTER_SENT => 'Sent',
	NEWSLETTER_CANCELED => 'Canceled'
);

//review remove interval
define('REVIEW_REMOVE_INTERVAL', 30); //in days

// reviews statuses
define('REVIEW_NOT_APPROVED',		1);
define('REVIEW_APPROVED',			2);
define('REVIEW_DISABLED',			3);
define('REVIEW_SYSTEM_DISABLED',	4);
define('REVIEW_NEED_MODIFICATION',	5);

define('REVIEW_REPLY_PENDING',  1);
define('REVIEW_REPLY_NEED_MODIFICATION', 2);
define('REVIEW_REPLY_DISABLED', 3);
define('REVIEW_REPLY_APPROVED', 4);

$DEFINITIONS['review_status_options'] = array(
	REVIEW_NOT_APPROVED => Cubix_I18n::translate('review_not_approved'),
	REVIEW_APPROVED => Cubix_I18n::translate('review_approved'),
	REVIEW_DISABLED => Cubix_I18n::translate('review_disabled'),
	REVIEW_SYSTEM_DISABLED => 'System Disabled',
	REVIEW_NEED_MODIFICATION => Cubix_I18n::translate('review_need_modification')
);

$DEFINITIONS['review_reply_status'] = array(
	REVIEW_REPLY_PENDING => "Pending",
	REVIEW_REPLY_NEED_MODIFICATION => 'Need Modification',
	REVIEW_REPLY_DISABLED => "Disabled",
	REVIEW_REPLY_APPROVED => "Approved"
);

define('REVIEW_DISABLE_TYPE_INTERNAL', 1);
//define('REVIEW_DISABLE_TYPE_EXTERNAL', 2);
//define('REVIEW_DISABLE_TYPE_MEETING_DENIED', 3);
define('REVIEW_DISABLE_TYPE_EXTERNAL_REQUESTED', 4);
define('REVIEW_DISABLE_TYPE_EXTERNAL_ADMIN', 5);

$DEFINITIONS['review_disable_types'] = array(
	REVIEW_DISABLE_TYPE_INTERNAL => 'Internal',
	//REVIEW_DISABLE_TYPE_EXTERNAL => 'External',
	//REVIEW_DISABLE_TYPE_MEETING_DENIED => 'Meeting denied',
	REVIEW_DISABLE_TYPE_EXTERNAL_REQUESTED => 'External By E. or M.',
	REVIEW_DISABLE_TYPE_EXTERNAL_ADMIN => 'External By Admin'
);

define('COMMENT_DISABLE_TYPE_INTERNAL', 1);
//define('COMMENT_DISABLE_TYPE_EXTERNAL', 2);
define('COMMENT_DISABLE_TYPE_EXTERNAL_REQUESTED', 4);
define('COMMENT_DISABLE_TYPE_EXTERNAL_ADMIN', 5);

$DEFINITIONS['comment_disable_types'] = array(
	COMMENT_DISABLE_TYPE_INTERNAL => 'Internal',
	//COMMENT_DISABLE_TYPE_EXTERNAL => 'External',
	COMMENT_DISABLE_TYPE_EXTERNAL_REQUESTED => 'External By E. or M.',
	COMMENT_DISABLE_TYPE_EXTERNAL_ADMIN => 'External By Admin'
);

define('MTS_NO_ONLY_ONCE',   1);
define('MTS_YES_MORE_TIMES',       2);
define('MTS_DONT_KNOW',		99);

$DEFINITIONS['mts_options'] = array(
	MTS_YES_MORE_TIMES => Cubix_I18n::translate('yes_more_times'),
	MTS_NO_ONLY_ONCE => Cubix_I18n::translate('no_only_once'),
	MTS_DONT_KNOW => Cubix_I18n::translate('dont_know')
);

//reviews suspicious
define('REVIEW_SUSPICIOUS_SAME_ESCORT_AND_IP',          1);
define('REVIEW_SUSPICIOUS_USER_IP_DUPLICITY',           2);
define('REVIEW_SUSPICIOUS_UNIQUE_COOKIE_ID_DUPLICITY',  3);

$DEFINITIONS['review_suspicious_options'] = array(
	REVIEW_SUSPICIOUS_SAME_ESCORT_AND_IP => Cubix_I18n::translate('same_escort_and_ip'),
	REVIEW_SUSPICIOUS_USER_IP_DUPLICITY => Cubix_I18n::translate('user_ip_duplicity'),
	REVIEW_SUSPICIOUS_UNIQUE_COOKIE_ID_DUPLICITY => Cubix_I18n::translate('cookie_id_duplicity')
);

//comments suspicious
define('COMMENT_SUSPICIOUS_SAME_ESCORT_AND_IP',          1);
define('COMMENT_SUSPICIOUS_USER_IP_DUPLICITY',           2);
define('COMMENT_SUSPICIOUS_UNIQUE_COOKIE_ID_DUPLICITY',  3);

$DEFINITIONS['comment_suspicious_options'] = array(
	COMMENT_SUSPICIOUS_SAME_ESCORT_AND_IP => Cubix_I18n::translate('same_escort_and_ip'),
	COMMENT_SUSPICIOUS_USER_IP_DUPLICITY => Cubix_I18n::translate('user_ip_duplicity'),
	COMMENT_SUSPICIOUS_UNIQUE_COOKIE_ID_DUPLICITY => Cubix_I18n::translate('cookie_id_duplicity')
);

// review meeting places
define('MEETING_AT_MY_FLAT_OR_HOUSE',       11);
define('MEETING_AT_HER_APARTMENT',          12);
define('MEETING_AT_HER_HOTEL',              13);
define('MEETING_AT_MY_HOTEL',               14);
define('MEETING_SOMEWHERE_ELSE',            15);

$DEFINITIONS['meeting_place_options'] = array(
    MEETING_AT_MY_FLAT_OR_HOUSE     => Cubix_I18n::translate('at_my_flat'),
    MEETING_AT_HER_APARTMENT        => Cubix_I18n::translate('at_her_apartament'),
    MEETING_AT_HER_HOTEL			=> Cubix_I18n::translate('her_hotel'),
    MEETING_AT_MY_HOTEL             => Cubix_I18n::translate('my_hotel'),
    MEETING_SOMEWHERE_ELSE          => Cubix_I18n::translate('somewhere_else'),
);

define('SEX_ENTHUSIASTIC',                  11);
define('SEX_PASSIVE',                       12);
define('SEX_ACTIVE',                        13);

$DEFINITIONS['sex_options'] = array(
    SEX_ENTHUSIASTIC		=> Cubix_I18n::translate('enthusiastic'),
	SEX_ACTIVE				=> Cubix_I18n::translate('active'),
    SEX_PASSIVE				=> Cubix_I18n::translate('passive')
);

define('ATTITUDE_FRIENDLY',                 11);
define('ATTITUDE_UNFRIENDLY',               12);
define('ATTITUDE_POSHY',                    13);
define('ATTITUDE_NORMAL',                   14);

$DEFINITIONS['attitude_options'] = array(
    ATTITUDE_FRIENDLY				=> Cubix_I18n::translate('friendly'),
	ATTITUDE_NORMAL					=> Cubix_I18n::translate('normal'),
    ATTITUDE_UNFRIENDLY				=> Cubix_I18n::translate('unfriendly'),
    ATTITUDE_POSHY					=> Cubix_I18n::translate('poshy'),
);

define('CONVERSATION_LANGUAGE_PROBLEMS',    11);
define('CONVERSATION_VERY_SIMPLE',          12);
define('CONVERSATION_NORMAL',               13);
define('CONVERSATION_INTELLIGENT',          14);

$DEFINITIONS['conversation_options'] = array(
	CONVERSATION_VERY_SIMPLE					=> Cubix_I18n::translate('very_simple'),
	CONVERSATION_INTELLIGENT					=> Cubix_I18n::translate('intelligent'),
	CONVERSATION_NORMAL							=> Cubix_I18n::translate('normal'),
	CONVERSATION_LANGUAGE_PROBLEMS				=> Cubix_I18n::translate('language_problems')
);

define('BREAST_REAL',                       11);
define('BREAST_SILICON',                    12);
define('BREAST_DONT_KNOW',                  99);

$DEFINITIONS['breast_options'] = array(
    BREAST_REAL				=> Cubix_I18n::translate('real'),
    BREAST_SILICON			=> Cubix_I18n::translate('silicon'),
    BREAST_DONT_KNOW		=> Cubix_I18n::translate('dont_know')
);

define('AVAILABILITY_EASY',                 11);
define('AVAILABILITY_NORMAL',               12);
define('AVAILABILITY_HARD',                 13);
define('AVAILABILITY_SUCKS',                14);

$DEFINITIONS['availability_options'] = array(
    AVAILABILITY_EASY				=> Cubix_I18n::translate('easy'),
    AVAILABILITY_NORMAL				=> Cubix_I18n::translate('normal'),
    AVAILABILITY_HARD				=> Cubix_I18n::translate('hard'),
    AVAILABILITY_SUCKS				=> Cubix_I18n::translate('sucks')
);

define('PHOTO_GENUINE',                     11);
define('PHOTO_FAKE',                        12);
define('PHOTO_HARD_TO_BELIEVE',             13);

$DEFINITIONS['photos_options'] = array(
    PHOTO_GENUINE				=> Cubix_I18n::translate('genuine'),
	PHOTO_HARD_TO_BELIEVE		=> Cubix_I18n::translate('hard_to_belive'),
    PHOTO_FAKE					=> Cubix_I18n::translate('fake')
);

// comments statuses
define('COMMENT_ACTIVE',                     1);
define('COMMENT_NOT_APPROVED',              -3);
define('COMMENT_DISABLED',					-4);
define('COMMENT_SYSTEM_DISABLED',			-5);

$DEFINITIONS['comment_status_options'] = array(
    COMMENT_ACTIVE				=> Cubix_I18n::translate('active'),
    COMMENT_NOT_APPROVED		=> Cubix_I18n::translate('not_approved'),
    COMMENT_DISABLED			=> Cubix_I18n::translate('disabled'),
    COMMENT_SYSTEM_DISABLED		=> 'System Disabled'
);

$DEFINITIONS['services'] = require(dirname(__FILE__) . '/services.php');


$DEFINITIONS['services_flat'] = array();
foreach ( $DEFINITIONS['services'] as $svcs ) {
	$DEFINITIONS['services_flat'] = array_merge($DEFINITIONS['services_flat'], $svcs);
}

$DEFINITIONS['phone_number_regexes'] = array(
	215 => '', // UK
	199 => '', // CH
	101 => '', // IT
);

$DEFINITIONS['dial_codes'] = require('dial_codes.php');

define('PHONE_INSTRUCTIONS_SMS_CALL', 1);
define('PHONE_INSTRUCTIONS_SMS_ONLY', 2);
define('PHONE_INSTRUCTIONS_NO_SMS', 3);
define('PHONE_INSTRUCTIONS_WHATSAPP_ONLY', 4);

$DEFINITIONS['phone_instructions'] = array(
	PHONE_INSTRUCTIONS_SMS_CALL => Cubix_I18n::translate('phone_instr_sms_call'),
	PHONE_INSTRUCTIONS_SMS_ONLY => Cubix_I18n::translate('phone_instr_sms_only'),
	PHONE_INSTRUCTIONS_NO_SMS => Cubix_I18n::translate('phone_instr_no_sms'),
);

$DEFINITIONS['beneluxxx_phone_instructions'] = array(
    PHONE_INSTRUCTIONS_SMS_CALL => Cubix_I18n::translate('phone_instr_sms_call'),
    PHONE_INSTRUCTIONS_SMS_ONLY => Cubix_I18n::translate('phone_instr_sms_only'),
    PHONE_INSTRUCTIONS_NO_SMS => Cubix_I18n::translate('phone_instr_no_sms'),
    PHONE_INSTRUCTIONS_WHATSAPP_ONLY => Cubix_I18n::translate('phone_instr_only_whatsapp')
);

/**
 * US Phone Number RegEx
 *  '/^(?:1(?:[. -])?)?(?:\((?=\d{3}\)))?([2-9]\d{2})'
 * .'(?:(?<=\(\d{3})\))? ?(?:(?<=\d{3})[.-])?([2-9]\d{2})'
 * .'[. -]?(\d{4})(?: (?i:ext)\.? ?(\d{1,5}))?$/'
 */

define('FUCKOMETER_TRUSED_USER_REVIEWS', 4);
define('TOTAL_FUCKOMETER_USER_MIN_REVIEWS', 1);

//sms info
define('SMS_USERKEY', 'GCK68SF3ZSIM');
define('SMS_PASSWORD', '658152');
define('SMS_DeliveryNotificationURL', 'http://backend.6annonce.com/sms/index/sms-callback?status=1&id=');
define('SMS_NonDeliveryNotificationURL', 'http://backend.6annonce.com/sms/index/sms-callback?status=2&id=');
define('SMS_BufferedNotificationURL', 'http://backend.6annonce.com/sms/index/sms-callback?status=3&id=');

/* Alert me */
define('ALERT_ME_NEW_COMMENT', 1);
define('ALERT_ME_MODIFICATION_ON_PROFILE', 2);
define('ALERT_ME_NEW_PICTURES', 3);
define('ALERT_ME_NEW_REVIEW', 4);
define('ALERT_ME_ANY_CHANGE', 5);
define('ALERT_ME_CITY', 6);

define('FOLLOW_ALERT_REVIEW', 1);
define('FOLLOW_ALERT_COMMENT', 2);
define('FOLLOW_ALERT_PINBOARD_ENTRY', 3);
define('FOLLOW_ALERT_PINBOARD_REPLAY', 4);

$DEFINITIONS['alert_me'] = array(
	ALERT_ME_NEW_COMMENT => Cubix_I18n::translate('alm_new_comment'),
	ALERT_ME_MODIFICATION_ON_PROFILE => Cubix_I18n::translate('alm_mod_profile'),
	ALERT_ME_NEW_PICTURES => Cubix_I18n::translate('alm_new_pictures'),
	ALERT_ME_NEW_REVIEW => Cubix_I18n::translate('alm_new_review'),
	ALERT_ME_ANY_CHANGE => Cubix_I18n::translate('alm_any_change'),
	ALERT_ME_CITY => Cubix_I18n::translate('alm_city')
);
//

/* Follow Events */
define('FOLLOW_ESCORT_NEW_COMMENT', 1);
define('FOLLOW_ESCORT_NEW_PROFILE', 2);
define('FOLLOW_ESCORT_NEW_PICTURE', 3);
define('FOLLOW_ESCORT_NEW_REVIEW', 4);
define('FOLLOW_ESCORT_NEW_PRICE', 5);
define('FOLLOW_ESCORT_NEW_CITY',6);

define('FOLLOW_AGENCY_NEW_FEMALE', 11);
define('FOLLOW_AGENCY_NEW_MALE', 12);
define('FOLLOW_AGENCY_NEW_TRANS', 13);
define('FOLLOW_AGENCY_NEW_COMMENT', 14);
define('FOLLOW_AGENCY_NEW_REVIEW', 15);
define('FOLLOW_AGENCY_NEW_CITY', 16);

$DEFINITIONS['follow_escort_events'] = array(
    FOLLOW_ESCORT_NEW_COMMENT => Cubix_I18n::translate('follow_new_comment'),
    FOLLOW_ESCORT_NEW_PROFILE => Cubix_I18n::translate('follow_modification_profile'),
    FOLLOW_ESCORT_NEW_PICTURE => Cubix_I18n::translate('follow_new_picture'),
    FOLLOW_ESCORT_NEW_REVIEW => Cubix_I18n::translate('follow_new_review'),
	FOLLOW_ESCORT_NEW_PRICE	=> Cubix_I18n::translate('follow_new_price'),
	FOLLOW_ESCORT_NEW_CITY => Cubix_I18n::translate('follow_visit_city'),
);

$DEFINITIONS['follow_agency_events'] = array(
    FOLLOW_AGENCY_NEW_FEMALE => Cubix_I18n::translate('follow_new_female_escort'),
    FOLLOW_AGENCY_NEW_MALE => Cubix_I18n::translate('follow_new_trans_escort'),
    FOLLOW_AGENCY_NEW_TRANS => Cubix_I18n::translate('follow_new_man_escort'),
    FOLLOW_AGENCY_NEW_COMMENT => Cubix_I18n::translate('follow_new_comment'),
	FOLLOW_AGENCY_NEW_REVIEW => Cubix_I18n::translate('follow_new_review'),
	FOLLOW_AGENCY_NEW_CITY => Cubix_I18n::translate('follow_new_city')
);

define('FOLLOW_NEW_CITY_CITY',1);
define('FOLLOW_NEW_CITY_TOUR',2);

/* Booking Contact */
define('BOOKIN_CONTACT_PHONE', 1);
define('BOOKIN_CONTACT_EMAIL', 2);
define('BOOKIN_CONTACT_NO_MATTER', 3);

$DEFINITIONS['booking_contact'] = array(
	BOOKIN_CONTACT_PHONE => 'By Phone',
	BOOKIN_CONTACT_EMAIL => 'By Email',
	BOOKIN_CONTACT_NO_MATTER => 'Doesn\'t matter'
);
//

/* Booking Preferred Time */
define('BOOKIN_PREFERRED_TIME_MORNING', 1);
define('BOOKIN_PREFERRED_TIME_AFTERNOON', 2);
define('BOOKIN_PREFERRED_TIME_EVENING', 3);
define('BOOKIN_PREFERRED_TIME_NO_MATTER', 4);

$DEFINITIONS['booking_preferred_time'] = array(
	BOOKIN_PREFERRED_TIME_MORNING => 'Morning',
	BOOKIN_PREFERRED_TIME_AFTERNOON => 'Afternoon',
	BOOKIN_PREFERRED_TIME_EVENING => 'Evening',
	BOOKIN_PREFERRED_TIME_NO_MATTER => 'Doesn\'t matter'
);
//

/* Feedbacks */
define('FEEDBACK_FLAG_SENT', 0);
define('FEEDBACK_FLAG_BLOCKED_BY_WORD', 1);
define('FEEDBACK_FLAG_BLOCKED_BY_EMAIL', 2);
define('FEEDBACK_FLAG_NOT_SEND', 3);

$DEFINITIONS['feedback_flags'] = array(
	FEEDBACK_FLAG_NOT_SEND => 'Not Send',
	FEEDBACK_FLAG_SENT => 'Sent',
	FEEDBACK_FLAG_BLOCKED_BY_WORD => 'Blocked By Word',
	FEEDBACK_FLAG_BLOCKED_BY_EMAIL => 'Blocked By Email'
);

define('FEEDBACK_STATUS_NOT_APPROVED', 1);
define('FEEDBACK_STATUS_APPROVED', 2);
define('FEEDBACK_STATUS_DISABLED', 3);

$DEFINITIONS['feedback_status'] = array(
	FEEDBACK_STATUS_NOT_APPROVED => 'Not Approved',
	FEEDBACK_STATUS_APPROVED => 'Approved',
	FEEDBACK_STATUS_DISABLED => 'Disabled'
);


/*--> Blacklisted member's comments */
define('BL_MEMBER_COMMENT_STATUS_NOT_APPROVED', 1);
define('BL_MEMBER_COMMENT_STATUS_APPROVED', 2);

$DEFINITIONS['bl_member_comments_status'] = array(
	BL_MEMBER_COMMENT_STATUS_NOT_APPROVED => 'Not Approved',
	BL_MEMBER_COMMENT_STATUS_APPROVED => 'Approved',
);

/*<-- Feedbacks */

/* Private messages */
define('PM_STATUS_NOT_APPROVED', 1);
define('PM_STATUS_APPROVED', 2);
define('PM_STATUS_DISABLED', 3);
$DEFINITIONS['pm_status'] = array(
	PM_STATUS_NOT_APPROVED => 'Not Approved',
	PM_STATUS_APPROVED => 'Approved',
	PM_STATUS_DISABLED => 'Disabled'
);

define('PM_FLAG_SENT', 0);
define('PM_FLAG_BLOCKED_BY_WORD', 1);
define('PM_FLAG_BLOCKED_BY_USER', 2);

$DEFINITIONS['pm_flags'] = array(
	PM_FLAG_SENT => 'Sent',
	PM_FLAG_BLOCKED_BY_WORD => 'Blocked By Word',
	PM_FLAG_BLOCKED_BY_USER => 'Blocked By User'
);
//

/* Club */
define('CLUB_ENTRANCE_NA', 0);
define('CLUB_ENTRANCE_FREE', 1);
define('CLUB_ENTRANCE_NOT_FREE', 2);
define('CLUB_WELLNESS_NA', 0);
define('CLUB_WELLNESS_YES', 1);
define('CLUB_WELLNESS_NO', 2);
define('CLUB_WELLNESS_FREE', 3);
define('CLUB_WELLNESS_NOT_FREE', 4);
define('CLUB_FOOD_NA', 0);
define('CLUB_FOOD_YES', 1);
define('CLUB_FOOD_NO', 2);
define('CLUB_FOOD_FREE', 3);
define('CLUB_FOOD_NOT_FREE', 4);
define('CLUB_OUTDOOR_NA', 0);
define('CLUB_OUTDOOR_YES', 1);
define('CLUB_OUTDOOR_NO', 2);

$DEFINITIONS['club_entrance_status'] = array(
	CLUB_ENTRANCE_NA => 'N/A',
	CLUB_ENTRANCE_FREE => Cubix_I18n::translate('free'),
	CLUB_ENTRANCE_NOT_FREE => Cubix_I18n::translate('with_cost'),
);
$DEFINITIONS['club_wellness_status'] = array(
	CLUB_WELLNESS_NA => 'N/A',
	CLUB_WELLNESS_YES => Cubix_I18n::translate('yes'),
	CLUB_WELLNESS_NO => Cubix_I18n::translate('no'),
	CLUB_WELLNESS_FREE => Cubix_I18n::translate('free'),
	CLUB_WELLNESS_NOT_FREE => Cubix_I18n::translate('with_cost'),
);
$DEFINITIONS['club_food_status'] = array(
	CLUB_FOOD_NA => 'N/A',
	CLUB_FOOD_YES => Cubix_I18n::translate('yes'),
	CLUB_FOOD_NO => Cubix_I18n::translate('no'),
	CLUB_FOOD_FREE => Cubix_I18n::translate('free'),
	CLUB_FOOD_NOT_FREE => Cubix_I18n::translate('with_cost'),
);
$DEFINITIONS['club_outdoor_status'] = array(
	CLUB_OUTDOOR_NA => 'N/A',
	CLUB_OUTDOOR_YES => Cubix_I18n::translate('yes'),
	CLUB_OUTDOOR_NO => Cubix_I18n::translate('no'),
);
//

/* reciprocal links */
define('RECIPROCAL_LINK_HAS_NOT_BANNER', 0);
define('RECIPROCAL_LINK_HAS_BANNER', 1);

$DEFINITIONS['reciprocal_links_status'] = array(
	RECIPROCAL_LINK_HAS_NOT_BANNER => 'Has Not Banner',
	RECIPROCAL_LINK_HAS_BANNER => 'Has Banner'
);
/**/

/* agency filter criteria */
define('AGENCY_FILTER_CRITERIA_CLUB', 1);
define('AGENCY_FILTER_CRITERIA_KONTAKTBAR', 2);
define('AGENCY_FILTER_CRITERIA_LAUFHAUS', 3);
define('AGENCY_FILTER_CRITERIA_NIGHTCLUB', 4);

$DEFINITIONS['agency_filter_criteria'] = array(
	AGENCY_FILTER_CRITERIA_CLUB => Cubix_I18n::translate('club_studio'),
	AGENCY_FILTER_CRITERIA_KONTAKTBAR => Cubix_I18n::translate('kontaktbar'),
	AGENCY_FILTER_CRITERIA_LAUFHAUS => Cubix_I18n::translate('laufhaus'),
	AGENCY_FILTER_CRITERIA_NIGHTCLUB => Cubix_I18n::translate('nightclub')
);
/**/

/* report problem status */
define('REPORT_PROBLEM_STATUS_PENDING', 1);
define('REPORT_PROBLEM_STATUS_CHECKED', 2);
define('REPORT_PROBLEM_STATUS_REPLIED', 3);

$DEFINITIONS['report_problem_status'] = array(
	REPORT_PROBLEM_STATUS_PENDING => 'Pending',
	REPORT_PROBLEM_STATUS_CHECKED => 'Checked',
	REPORT_PROBLEM_STATUS_REPLIED => 'Replied'
);
/**/

define('C_ADS_SHE_SEEKS',			1);
define('C_ADS_HE_SEEKS',			2);
define('C_ADS_COUPLE_SEEKS',		3);
define('C_ADS_TS_TV',				4);
define('C_ADS_JOBS',				5);
define('C_ADS_FLATS_PROPERTIES',	6);
define('C_ADS_MISCELLANEOUS',		7);
define('C_ADS_PARTY',				8);
define('C_ADS_GANG_BANG',			9);
define('C_ADS_PLACES',			    10);
define('C_ADS_MASSAGE',			    11);
define('C_ADS_BDSM',			    12);
define('C_ADS_MASSAGE',             13);
define('C_ADS_FETISH',              14);
define('C_ADS_GAY',			        15);

$DEFINITIONS['classified_ad_categories'] = array(
    C_ADS_SHE_SEEKS			=> Cubix_I18n::translate('she_seeks'),
    C_ADS_HE_SEEKS			=> Cubix_I18n::translate('he_seeks'),
    C_ADS_COUPLE_SEEKS      => Cubix_I18n::translate('couple_seeks'),
    C_ADS_TS_TV				=> Cubix_I18n::translate('ts_tv'),
    C_ADS_JOBS				=> Cubix_I18n::translate('jobs'),
    C_ADS_FLATS_PROPERTIES	=> Cubix_I18n::translate('flat_properties'),
    C_ADS_MISCELLANEOUS		=> Cubix_I18n::translate('miscellaneous')
);


if($app_id == APP_A6){
	$DEFINITIONS['classified_ad_categories'][C_ADS_MISCELLANEOUS] = Cubix_I18n::translate('diverse');
    $DEFINITIONS['classified_ad_categories'][C_ADS_FETISH] = Cubix_I18n::translate('fetish');
    $DEFINITIONS['classified_ad_categories'][C_ADS_GAY] = Cubix_I18n::translate('gay');
    $DEFINITIONS['classified_ad_categories'][C_ADS_GANG_BANG] = Cubix_I18n::translate('gang_bang');
}


if (in_array($app_id, array(APP_ED, APP_EG_CO_UK, APP_EG, APP_6C)))
{
    $DEFINITIONS['classified_ad_categories'][C_ADS_BDSM] = Cubix_I18n::translate('bdsm_fetish');
}

if (in_array($app_id, array(APP_EG_CO_UK)))
{
	$DEFINITIONS['classified_ad_categories'][C_ADS_MASSAGE] = Cubix_I18n::translate('massage');
}

$DEFINITIONS['classified_ad_categories_bl'] = array(
    C_ADS_SHE_SEEKS			=> Cubix_I18n::translate('she_seeks'),
    C_ADS_HE_SEEKS			=> Cubix_I18n::translate('he_seeks'),
    C_ADS_COUPLE_SEEKS      => Cubix_I18n::translate('couple_seeks'),
    C_ADS_TS_TV				=> Cubix_I18n::translate('ts_tv'),
    C_ADS_JOBS				=> Cubix_I18n::translate('jobs'),
    C_ADS_FLATS_PROPERTIES	=> Cubix_I18n::translate('flat_properties'),
    C_ADS_MISCELLANEOUS		=> Cubix_I18n::translate('miscellaneous'),
    C_ADS_PARTY				=> Cubix_I18n::translate('events_parties'),
    C_ADS_GANG_BANG			=> Cubix_I18n::translate('gang_bang')
);

$DEFINITIONS['classified_ad_categories_ed'] = array(
    C_ADS_SHE_SEEKS			=> Cubix_I18n::translate('she_seeks'),
    C_ADS_HE_SEEKS			=> Cubix_I18n::translate('he_seeks'),
    C_ADS_COUPLE_SEEKS      => Cubix_I18n::translate('couple_seeks'),
    C_ADS_TS_TV				=> Cubix_I18n::translate('ts_tv'),
    C_ADS_JOBS				=> Cubix_I18n::translate('jobs'),
    C_ADS_FLATS_PROPERTIES	=> Cubix_I18n::translate('flat_properties'),
    C_ADS_MISCELLANEOUS		=> Cubix_I18n::translate('miscellaneous'),
    C_ADS_PARTY				=> Cubix_I18n::translate('events_parties'),
    C_ADS_GANG_BANG			=> Cubix_I18n::translate('gang_bang'),
	C_ADS_PLACES			=> Cubix_I18n::translate('places'),
	C_ADS_MASSAGE			=> Cubix_I18n::translate('massage'),
	C_ADS_BDSM			    => Cubix_I18n::translate('bdsm_fetish')
);

define('C_ADS_DAYS_3',  3);
define('C_ADS_DAYS_7',	7);
define('C_ADS_DAYS_10',	10);
define('C_ADS_DAYS_15',	15);
define('C_ADS_DAYS_30',	30);
define('C_ADS_DAYS_100', 100);

$DEFINITIONS['classified_ad_durations'] = array(
    C_ADS_DAYS_3    => Cubix_I18n::translate('3_days'),
    C_ADS_DAYS_10	=> Cubix_I18n::translate('10_days'),
    C_ADS_DAYS_15   => Cubix_I18n::translate('15_days'),
    C_ADS_DAYS_30	=> Cubix_I18n::translate('30_days')
);

if ( $app_id == APP_BL ) {
    $DEFINITIONS['classified_ad_durations'][C_ADS_DAYS_100] = Cubix_I18n::translate('100_days');
}

if ( $app_id == APP_A6 ) {
    $DEFINITIONS['classified_ad_durations'][C_ADS_DAYS_7] = Cubix_I18n::translate('7_days');
}

define('C_ADS_PACKAGE_FREE',	'free');
define('C_ADS_PACKAGE_1_DAY',	1);
define('C_ADS_PACKAGE_3_DAY',	2);

$DEFINITIONS['classified_ad_packages_arr'] = array(
    C_ADS_PACKAGE_FREE,
    C_ADS_PACKAGE_1_DAY,
    C_ADS_PACKAGE_3_DAY
);

$DEFINITIONS['classified_ad_packages'] = array(
    C_ADS_PACKAGE_1_DAY => array('days' => 1, 'price' => 19.00),
    C_ADS_PACKAGE_3_DAY => array('days' => 3, 'price' => 49.00)
);

if($app_id == APP_A6){
    $DEFINITIONS['classified_ad_packages'] = array(
        C_ADS_PACKAGE_1_DAY => array('days' => 1, 'price' => 20.00),
        C_ADS_PACKAGE_3_DAY => array('days' => 3, 'price' => 50.00)
    );
}

$DEFINITIONS['latest_actions_titles'] = array(
	Cubix_LatestActions::ACTION_NEW_PHOTO		=> Cubix_I18n::translate('la_has_new_photo'),
    Cubix_LatestActions::ACTION_PROFILE_UPDATE	=> Cubix_I18n::translate('la_profile_update'),
	Cubix_LatestActions::ACTION_TOUR_UPDATE		=> Cubix_I18n::translate('la_tour_update'),
	Cubix_LatestActions::ACTION_NEW_REVIEW		=> Cubix_I18n::translate('la_new_review'),
	Cubix_LatestActions::ACTION_NEW_COMMENT		=> Cubix_I18n::translate('la_new_comment')
);

$DEFINITIONS['latest_actions_titles_a6'] = array(
    Cubix_LatestActions::ACTION_NEW_PHOTO       => Cubix_I18n::translate('la_has_new_photo'),
    Cubix_LatestActions::ACTION_PROFILE_UPDATE  => Cubix_I18n::translate('la_profile_update'),
    Cubix_LatestActions::ACTION_NEW_COMMENT     => Cubix_I18n::translate('la_new_comment')
);


if ($app_id == APP_BL) {
	define('GIRL_OF_THE_DAY', 16);
} elseif ($app_id == APP_EG_CO_UK) {
    define('GIRL_OF_THE_DAY', 18);
    define('GIRL_OF_THE_DAY_WITH_PACKAGE_PRICE', 5);
    define('GIRL_OF_THE_DAY_WITHOUT_PACKAGE_PRICE', 10);
    define('BOY_OF_THE_DAY_WITH_PACKAGE_PRICE', 5);
    define('BOY_OF_THE_DAY_WITHOUT_PACKAGE_PRICE', 9);
    $DEFINITIONS['free_packages'] = array(43, 44, 95, 105, 106);
}elseif ($app_id == APP_A6) {
    define('GIRL_OF_THE_DAY', 15);
    define('GIRL_OF_THE_DAY_WITH_PACKAGE_PRICE', 90);
    define('GIRL_OF_THE_DAY_WITHOUT_PACKAGE_PRICE', 90);
    define('BOY_OF_THE_DAY_WITH_PACKAGE_PRICE', 90);
    define('BOY_OF_THE_DAY_WITHOUT_PACKAGE_PRICE', 90);
    $DEFINITIONS['free_packages'] = array(26, 9, 15);
}elseif($app_id == APP_ED){
    define('GIRL_OF_THE_DAY', 16);
    define('GIRL_OF_THE_DAY_WITH_PACKAGE_PRICE', 8);
    define('GIRL_OF_THE_DAY_WITHOUT_PACKAGE_PRICE', 14);
    define('BOY_OF_THE_DAY_WITH_PACKAGE_PRICE', 8);
    define('BOY_OF_THE_DAY_WITHOUT_PACKAGE_PRICE', 14);
}else {
	define('GIRL_OF_THE_DAY', 15);
}

if ( $app_id == APP_A6 ) {
    define('VIDEO_OF_THE_DAY', 22);
}

if ( $app_id == APP_EF ) {
	define('PRODUCT_ED_LISTING', 17);
	define('PRODUCT_NO_ED_LISTING', 18);
} elseif ( $app_id == APP_6A ) {
	define('PRODUCT_ED_LISTING', 27);
	define('PRODUCT_NO_ED_LISTING', 26);
}

define('MILAN_EXPO_PRODUCT_ID', 20);

/* suspicious photos request */
define('SUSPICIOUS_PHOTOS_REQUEST_PENDING', 1);
define('SUSPICIOUS_PHOTOS_REQUEST_SUSPICIOUS', 2);
define('SUSPICIOUS_PHOTOS_REQUEST_NOT_SUSPICIOUS', 3);
define('SUSPICIOUS_PHOTOS_REQUEST_UNKNOWN', 4);
define('SUSPICIOUS_PHOTOS_REQUEST_UNDER_INVESTIGATION', 5);

$DEFINITIONS['suspicious_photos_requests'] = array(
	SUSPICIOUS_PHOTOS_REQUEST_PENDING => 'Pending',
	SUSPICIOUS_PHOTOS_REQUEST_SUSPICIOUS => 'Suspicious',
	SUSPICIOUS_PHOTOS_REQUEST_NOT_SUSPICIOUS => 'Not Suspicious',
	SUSPICIOUS_PHOTOS_REQUEST_UNKNOWN => 'Unknown',
	SUSPICIOUS_PHOTOS_REQUEST_UNDER_INVESTIGATION => 'Under Investigation'
);
/*****************************/

define('ERROR_PROFILE_DATA', 1);
define('CANNOT_OPEN_PHOTOS', 2);
define('REVIEW_AND_COMMENT', 3);

$DEFINITIONS['report_problems'] = array(
	
	ERROR_PROFILE_DATA=>array(Cubix_I18n::translate('error_profile_data'),0),
	CANNOT_OPEN_PHOTOS=>array(Cubix_I18n::translate('cannot_open_photos'),0),
	REVIEW_AND_COMMENT=>array(Cubix_I18n::translate('review_comments'),1)

);

define('ALERT_CITY_NON', 0);
define('ALERT_CITY_EMAIL', 1);
define('ALERT_CITY_SMS', 2);

$DEFINITIONS['alert_city_types'] = array(
	
	'ALERT_CITY_NON'=>ALERT_CITY_NON,
	'ALERT_CITY_EMAIL'=>ALERT_CITY_EMAIL,
	'ALERT_CITY_SMS'=>ALERT_CITY_SMS

);

define('ESCORT_ARRIVED', 1);
define('ESCORT_ON_TOUR', 2);
define('ESCORT_PLANNED', 3);
define('ESCORT_ADD_PHOTOS', 4);
define('ESCORT_NEW_REVIEW', 5);
define('ESCORT_NEW_COMMENT', 6);

$DEFINITIONS['alerts_about'] = array(
	
	ERROR_PROFILE_DATA=>Cubix_I18n::translate('ESCORT_ARRIVED'),
	ESCORT_ON_TOUR=>Cubix_I18n::translate('ESCORT_ON_TOUR'),
	ESCORT_PLANNED=>Cubix_I18n::translate('ESCORT_PLANNED'),
	ESCORT_ADD_PHOTOS=>Cubix_I18n::translate('ESCORT_ADD_PHOTOS'),
	ESCORT_NEW_REVIEW=>Cubix_I18n::translate('ESCORT_NEW_REVIEW'),
	ESCORT_NEW_COMMENT=>Cubix_I18n::translate('ESCORT_NEW_COMMENT'),

);

define('PINBOARD_STATUS_PENDING', 1);
define('PINBOARD_STATUS_APPROVED', 2);
define('PINBOARD_STATUS_DISABLED', 3);
define('PINBOARD_STATUS_CLOSED', 4);

$DEFINITIONS['pinboard_statues'] = array(	
	PINBOARD_STATUS_PENDING => Cubix_I18n::translate('pending'),
	PINBOARD_STATUS_APPROVED => Cubix_I18n::translate('approved'),
	PINBOARD_STATUS_DISABLED => Cubix_I18n::translate('disabled'),
	PINBOARD_STATUS_CLOSED => Cubix_I18n::translate('closed')
);

define('BLOG_STATUS_DISABLED', 0);
define('BLOG_STATUS_ACTIVE', 1);

$DEFINITIONS['blog_statues'] = array(	
	BLOG_STATUS_ACTIVE => Cubix_I18n::translate('active'),
	BLOG_STATUS_DISABLED => Cubix_I18n::translate('disabled')
);

define('BLOG_COMMENT_STATUS_PENDING', 1);
define('BLOG_COMMENT_STATUS_APPROVED', 2);
define('BLOG_COMMENT_STATUS_DISABLED', 3);

$DEFINITIONS['blog_comment_statues'] = array(	
	BLOG_COMMENT_STATUS_PENDING => Cubix_I18n::translate('pending'),
	BLOG_COMMENT_STATUS_APPROVED => Cubix_I18n::translate('approved'),
	BLOG_COMMENT_STATUS_DISABLED => Cubix_I18n::translate('disabled')
);

define('BLOG_CATEGORY_STATUS_PUBLISHED', 1);
define('BLOG_CATEGORY_STATUS_UNPUBLISHED', 0);

$DEFINITIONS['blog_category_statues'] = array(   
    BLOG_CATEGORY_STATUS_PUBLISHED => Cubix_I18n::translate('published'),
    BLOG_CATEGORY_STATUS_UNPUBLISHED => Cubix_I18n::translate('unpublished'),
);

define('BLOG_CATEGORY_LAYOUT_LEFT_BIG', 1);
define('BLOG_CATEGORY_LAYOUT_LEFT_SMALL', 2);
define('BLOG_CATEGORY_LAYOUT_RIGHT_BIG', 3);
define('BLOG_CATEGORY_LAYOUT_RIGHT_SMALL', 4);

$DEFINITIONS['blog_category_layouts'] = array(   
    BLOG_CATEGORY_LAYOUT_LEFT_BIG => "Left big (5 thumb)",
    BLOG_CATEGORY_LAYOUT_LEFT_SMALL => "Left small (2 thumb)",
    BLOG_CATEGORY_LAYOUT_RIGHT_BIG => "Right big (3 thumb)",
    BLOG_CATEGORY_LAYOUT_RIGHT_SMALL => "Right small (2 thumb)",
);


define('PLANNER_TYPE_APPOINTMENT', 1);
define('PLANNER_TYPE_HOLIDAY', 2);
define('PLANNER_TYPE_OTHER', 3);

$DEFINITIONS['planner_types'] = array(	
	PLANNER_TYPE_APPOINTMENT => Cubix_I18n::translate('appointment'),
	PLANNER_TYPE_HOLIDAY => Cubix_I18n::translate('holiday'),
	PLANNER_TYPE_OTHER => Cubix_I18n::translate('other')
);

define('PLANNER_ALERT_YES', 1);
define('PLANNER_ALERT_NO', 2);

$DEFINITIONS['planner_alerts'] = array(	
	PLANNER_ALERT_YES => Cubix_I18n::translate('yes'),
	PLANNER_ALERT_NO => Cubix_I18n::translate('no')
);

define('PLANNER_ALERT_TYPE_EMAIL', 1);
define('PLANNER_ALERT_TYPE_SMS', 2);

$DEFINITIONS['planner_alert_types'] = array(	
	PLANNER_ALERT_TYPE_EMAIL => Cubix_I18n::translate('email'),
	PLANNER_ALERT_TYPE_SMS => 'SMS'
);

define('PLANNER_ALERT_TIME_MINUTE', 1);
define('PLANNER_ALERT_TIME_HOUR', 2);
define('PLANNER_ALERT_TIME_DAY', 3);

$DEFINITIONS['planner_alert_time_unites'] = array(	
	PLANNER_ALERT_TIME_MINUTE => Cubix_I18n::translate('minute'),
	PLANNER_ALERT_TIME_HOUR => Cubix_I18n::translate('hour'),
	PLANNER_ALERT_TIME_DAY => Cubix_I18n::translate('day')
);

$DEFINITIONS['month_en'] = array(
	1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 
	7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'
);
$DEFINITIONS['week_en'] = array(0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday');

$DEFINITIONS['month_fr'] = array(
	1 => 'Janvier', 2 => 'Février', 3 => 'Mars', 4 => 'avril', 5 => 'Mai', 6 => 'Juin', 
	7 => 'Juillet', 8 => 'Août', 9 => 'Septembre', 10 => 'Octobre', 11 => 'Novembre', 12 => 'Décembre'
);
$DEFINITIONS['week_fr'] = array(0 => 'Dimanche', 1 => 'Lundi', 2 => 'Mardi', 3 => 'Mercredi', 4 => 'Jeudi', 5 => 'Vendredi', 6 => 'Samedi');

$DEFINITIONS['month_pt'] = array(
	1 => 'janeiro', 2 => 'fevereiro', 3 => 'março', 4 => 'abril', 5 => 'maio', 6 => 'junho', 
	7 => 'julho', 8 => 'agosto', 9 => 'setembro', 10 => 'outubro', 11 => 'novembro', 12 => 'dezembro'
);
$DEFINITIONS['week_pt'] = array(0 => 'domingo', 1 => 'segunda-feira', 2 => 'terça-feira', 3 => 'quarta-feira', 4 => 'quinta-feira', 5 => 'sexta-feira', 6 => 'sábado');

$DEFINITIONS['month_de'] = array(
	1 => 'Januar', 2 => 'Februar', 3 => 'März', 4 => 'April', 5 => 'Mai', 6 => 'Juni', 
	7 => 'Juli', 8 => 'August', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Dezember'
);
$DEFINITIONS['week_de'] = array(0 => 'Sonntag', 1 => 'Montag', 2 => 'Dienstag', 3 => 'Mittwoch', 4 => 'Donnerstag', 5 => 'Freitag', 6 => 'Samstag');

$DEFINITIONS['month_my'] = array(
	1 => 'Januari', 2 => 'Februari', 3 => 'Mac', 4 => 'April', 5 => 'Mei', 6 => 'Jun', 
	7 => 'Julai', 8 => 'Ogos', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Disember'
);
$DEFINITIONS['week_my'] = array(0 => 'Ahad', 1 => 'Isnin', 2 => 'Selasa', 3 => 'Rabu', 4 => 'Khamis', 5 => 'Jumaat', 6 => 'Sabtu');

$DEFINITIONS['month_cn'] = array(
	1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 
	7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'
);
$DEFINITIONS['week_cn'] = array(0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday');

$DEFINITIONS['month_nl'] = array(
	1 => 'januari', 2 => 'februari', 3 => 'maart', 4 => 'april', 5 => 'mei', 6 => 'juni', 
	7 => 'juli', 8 => 'augustus', 9 => 'september', 10 => 'oktober', 11 => 'november', 12 => 'december'
);
$DEFINITIONS['week_nl'] = array(0 => 'zondag', 1 => 'maandag', 2 => 'dinsdag', 3 => 'woensdag', 4 => 'donderdag', 5 => 'vrijdag', 6 => 'zaterdag');

$DEFINITIONS['month_es'] = array(
	1 => 'enero', 2 => 'febrero', 3 => 'marzo', 4 => 'abril', 5 => 'mayo', 6 => 'junio', 
	7 => 'julio', 8 => 'agosto', 9 => 'septiembre', 10 => 'octubre', 11 => 'noviembre', 12 => 'diciembre'
);
$DEFINITIONS['week_es'] = array(0 => 'domingo', 1 => 'lunes', 2 => 'martes', 3 => 'miércoles', 4 => 'jueves', 5 => 'viernes', 6 => 'sábado');

$DEFINITIONS['month_it'] = array(
	1 => 'gennaio', 2 => 'febbraio', 3 => 'marzo', 4 => 'aprile', 5 => 'maggio', 6 => 'giugno', 
	7 => 'luglio', 8 => 'agosto', 9 => 'settembre', 10 => 'ottobre', 11 => 'novembre', 12 => 'dicembre'
);
$DEFINITIONS['week_it'] = array(0 => 'domenica', 1 => 'lunedì', 2 => 'martedì', 3 => 'mercoledì', 4 => 'giovedì', 5 => 'venerdì', 6 => 'sabato');

$DEFINITIONS['month_ro'] = array(
	1 => 'ianuarie', 2 => 'februarie', 3 => 'martie', 4 => 'aprilie', 5 => 'mai', 6 => 'iunie', 
	7 => 'iulie', 8 => 'august', 9 => 'septembrie', 10 => 'octombrie', 11 => 'noiembrie', 12 => 'decembrie'
);
$DEFINITIONS['week_ro'] = array(0 => 'duminică', 1 => 'luni', 2 => 'marţi', 3 => 'miercuri', 4 => 'joi', 5 => 'vineri', 6 => 'sîmbătă');

$DEFINITIONS['month_ru'] = array(
	1 => 'январь', 2 => 'февраль', 3 => 'март', 4 => 'апрель', 5 => 'май', 6 => 'июнь', 
	7 => 'июль', 8 => 'август', 9 => 'сентябрь', 10 => 'октябрь', 11 => 'ноябрь', 12 => 'декабрь'
);
$DEFINITIONS['week_ru'] = array(0 => 'воскресенье', 1 => 'понедельник', 2 => 'вторник', 3 => 'среда', 4 => 'четверг', 5 => 'пятница', 6 => 'суббота');

define('CITY_ALERT_PERIOD_DAY', 1);
define('CITY_ALERT_PERIOD_WEEK', 2);

$DEFINITIONS['planner_alert_periods'] = array(	
	CITY_ALERT_PERIOD_DAY => Cubix_I18n::translate('day'),
	CITY_ALERT_PERIOD_WEEK => Cubix_I18n::translate('week')
);

define('MEMBER_RATING_TRUSTED',  1);
define('MEMBER_RATING_JUNIOR',   2);
define('MEMBER_RATING_SENIOR',   3);
define('MEMBER_RATING_DIAMOND',  4);
define('MEMBER_RATING_VIP',      5);
define('MEMBER_RATING_VIP_PLUS', 6);

$DEFINITIONS['member_ratings'] = array(
    MEMBER_RATING_TRUSTED   => Cubix_I18n::translate('member_rating_trusted'),
    MEMBER_RATING_JUNIOR    => Cubix_I18n::translate('member_rating_junior'),
    MEMBER_RATING_SENIOR    => Cubix_I18n::translate('member_rating_senior'),
    MEMBER_RATING_DIAMOND   => Cubix_I18n::translate('member_rating_diamond'),
    MEMBER_RATING_VIP       => Cubix_I18n::translate('member_rating_vip'),
    MEMBER_RATING_VIP_PLUS  => Cubix_I18n::translate('member_rating_vip_plus')
);

define('IDEA_POOL_STATUS_PENDING', 1);
define('IDEA_POOL_STATUS_APPROVED', 2);
define('IDEA_POOL_STATUS_REALIZED', 3);
define('IDEA_POOL_STATUS_DISABLED', 4);

$DEFINITIONS['idea_pool_status'] = array(
	IDEA_POOL_STATUS_PENDING => 'Pending',
	IDEA_POOL_STATUS_APPROVED => 'Approved',
	IDEA_POOL_STATUS_REALIZED => 'Realized',
	IDEA_POOL_STATUS_DISABLED => 'Disabled'
);

define('PRODUCT_BOOST_PROFILE', 19);

define('AGENCY_COMMENT_STATUS_PENDING', 9);
define('AGENCY_COMMENT_STATUS_ACTIVE', 1);
define('AGENCY_COMMENT_STATUS_DISABLED', 0);

$DEFINITIONS['agency_comment_status'] = array(
	AGENCY_COMMENT_STATUS_PENDING => 'Pending',
	AGENCY_COMMENT_STATUS_ACTIVE => 'Approved',
	AGENCY_COMMENT_STATUS_DISABLED => 'Disabled'
);

define('WATCH_TYPE_PER_SESSION', 1);
define('WATCH_TYPE_PER_DAY', 2);
define('WATCH_TYPE_PER_LAST_3_DAYS', 3);
define('WATCH_TYPE_PER_LAST_7_DAYS', 4);
define('WATCH_TYPE_PER_ALL_TIME', 5);

$DEFINITIONS['watch_types'] = array(
    WATCH_TYPE_PER_SESSION => Cubix_I18n::translate('per_session'),
    WATCH_TYPE_PER_DAY => Cubix_I18n::translate('per_day'),
    WATCH_TYPE_PER_LAST_3_DAYS => Cubix_I18n::translate('last_3_days'),
    WATCH_TYPE_PER_LAST_7_DAYS => Cubix_I18n::translate('this_week'),
    WATCH_TYPE_PER_ALL_TIME => Cubix_I18n::translate('all_time')
);

/* User login fail attempt count and time */
define('FAIL_LOGIN_CAPTCH_COUTN', 3);
define('FAIL_LOGIN_CAPTCH_MIN', 10);
define('FAIL_LOGIN_BLOCK_USER_COUNT', 8);
define('FAIL_LOGIN_BLOCK_USER_MIN', 1440);

/* ESCORT TYPES */
define('ESCORT_TYPE_ESCORT', 1);
define('ESCORT_TYPE_BDSM', 2);
define('ESCORT_TYPE_MASSAGE', 3);

$DEFINITIONS['ESCORT_TYPES'] = array(
    ESCORT_TYPE_ESCORT => 'Escort',
    ESCORT_TYPE_BDSM => 'BDSM',
    ESCORT_TYPE_MASSAGE => 'Massage'
);
//

$DEFINITIONS['EF_CAM_TOKEN_CURRENCIES'] = array(
    200  => 19.99,
	550  => 49.99,
	900  => 74.99,
	1875 => 149.99,
	3775 => 299.99,
	7575 => 599.99
);

$DEFINITIONS['AND6_CAM_TOKEN_CURRENCIES'] = array(
    200  => 19.99,
	550  => 49.99,
	900  => 74.99,
	1875 => 149.99,
	3775 => 299.99,
	7575 => 599.99
);

/*DISCOUNTS*/
define('DISCOUNT_FOR_EVERYONE', 1);
/*define('DISCOUNT_FOR_INDEPENDENTS_ESCORTS', 2);
define('DISCOUNT_FOR_AGENCY_ESCORTS', 3);*/
define('DISCOUNT_FOR_VERIFIED_PHOTOS', 4);
define('DISCOUNT_FOR_ACTIVE_VIDEO',	5);
define('DISCOUNT_FOR_CONFIRMED_PHONE_NUMBER',	6);

$DEFINITIONS['DISCOUNT_TYPES'] = array(
    DISCOUNT_FOR_EVERYONE	            => 'For Everyone',
    /*DISCOUNT_FOR_INDEPENDENTS_ESCORTS   => 'For Independents',
    DISCOUNT_FOR_AGENCY_ESCORTS         => 'For Agency Escorts',*/
    DISCOUNT_FOR_VERIFIED_PHOTOS        => 'Photos Verified',
    DISCOUNT_FOR_ACTIVE_VIDEO	        => 'Active Videos',
    DISCOUNT_FOR_CONFIRMED_PHONE_NUMBER	=> 'Phone Number Confirmed',
);
/**/

/* BUMPS hours */

$DEFINITIONS['BUMP_AVAILABLE_HOURS'] = array(
   'bump-option-1'   => 4, //bumpnow, and then bump every 4
   'bump-option-2'   => 8, //bumpnow, and then bump every 8
   'bump-option-3'   => 12, //bumpnow, and then bump every 12
   'bump-option-4'   => 12, //bumpnow, 10 am and 10 pm twice a day
   'bump-option-5'   => 24, //bumpnow and then everyday same time
   'bump-option-6'   => 24, //bumpnow it is just for showing user one time bump 
);
/**/

define('TOKEN_PAYMENT_PENDING', 0);
define('TOKEN_PAYMENT_PAYED_NOT_RECEIVED_CAMS', 1);
define('TOKEN_PAYMENT_PAYED_AND_RECEIVED_CAMS', 2);


/* Prospective clients groups (temporary solution for getting groups) */
$DEFINITIONS['PROSPECTIVE_CLIENT_GROUPS'] = array(
    1 => 'escort-advisor 3'
);


Zend_Registry::set('defines', $DEFINITIONS);


const SALES_CHANGE_SMS_IS_OLD_TRY = 0;
const SALES_CHANGE_SMS_NOT_VERIFIED = 1;
const SALES_CHANGE_PENDING = 2;
const SALES_CHANGE_APPROOVED = 3;
const SALES_CHANGE_DECLINED = 4;


const ESCORT_SKYPE_CALL_REQUESTS_PER_PAGE = 5;

define('ESCORT_CLICKED_PHONE_VERIFICATION', 1);
define('ESCORT_CLICKED_100_VERIFICATION', 2);
define('ESCORT_CLICKED_UPGRADE_NOW', 3);
if ($app_id == APP_EG_CO_UK) {
    define('BUBBLES_PER_PAGE_COUNT', 20);
}

define('SUPPORT_TICKET_SALES_CHANGE','Support ticket sales was reassigned');

define('SALES_CHANGED_BY_ESCORT_MODIFIER_ID',                2021);

/*PromoCode*/
define('PROMO_CODE_STATUS_ACTIVE', 1);
define('PROMO_CODE_STATUS_SPENT', 2);
/**/