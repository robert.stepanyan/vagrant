<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Cubix_2sms_Sms
{
	
	private $client;

    // Provider URL
    private $url = 'http://www.2sms.am/api/http/';
   
    // commands --------------------------------------------------------
    private $_commands = array(
                                'query'   => 'query',
                                'send'    => 'send',
                                'balance' => 'balance');

    private $apikey = '2966872697d008b1a354440068b1ef2c';
	
	const STATUS_UNKNOWN_RESPONSE = -10;
    const STATUS_MSG_EXPIRED = -5;
    const STATUS_NO_ROUTE = -4;
    const STATUS_MSG_UNKNOWN = -3;
    const STATUS_MSG_USER_CANCELED = -2;
    const STATUS_MSG_FAILED_TO_DELIVER = -1;
    const STATUS_MSG_QUEUED = 0;
    const STATUS_MSG_DELIVERED_TO_GATEWAY = 1;
    const STATUS_MSG_DELIVERED = 2;
	
    const API_KEY_EF = "2966872697d008b1a354440068b1ef2c"; //447624802546
    const API_KEY_6A = "6237119df642f26a26083788a1665611"; //447624802518
    const API_KEY_EM = "679f34061e1e2d0b549eca48d87ca265"; //447624802550
    const API_KEY_6ABR = "a7003600ed18f58c36c1938b6e9ab906"; //447624802552
    const API_KEY_AE = "d58394ca02338964ce5bd44e7fd79e1b"; //447624802553
    const API_KEY_BL = "18c70966583cadff5dbc4da780965aad"; //447624802560
    const API_KEY_EG = "17e5a695d4eb53b3ce65cdb189e19a26"; //447624802563
    const API_KEY_EG_CO_UK = "2621eeb9278b60c02e9cc56ea5e493d6"; //447624802564

    public function __construct($apikey = null)
    {
        // Checkin apikey
        if($apikey) {
            $this->apikey = $apikey;
        }


        // Initializing HTTP Client
        $this->client = new Zend_Http_Client($this->url);

        // Configs
        $config['maxredirexts'] = 2;
        $config['timeout'] = 30;
        $this->client->setConfig ($config);

        $this->client->setMethod(Zend_Http_Client::POST);
    }


    // Send request
    public function send($message, $sender_id, $to)
    {
       $this->resetClientParameters();

       // Command
       $this->client->setUri($this->url . $this->_commands[__FUNCTION__]);

	   if (strpos($sender_id, '00') === 0)
		{
			$sub = substr($sender_id, 2);
			$sender_id = '+' . $sub;
		}
	   
       // Setting post parameters
       $this->client->setParameterPost('message', $message);
       $this->client->setParameterPost('from', $sender_id);

		if ( is_array($to) ) {
			foreach ( $to as $i => $phone ) 
			{
				if (strpos($phone, '00') === 0)
				{
					$sub = substr($phone, 2);
					$to[$i] = '+' . $sub;
				}
			}
			$to = implode(',', $to);
		}
		else {
			if (strpos($to, '00') === 0)
			{
				$sub = substr($to, 2);
				$to = '+' . $sub;
			}
			$to = $to;
		}
	   
       //$to = (is_array($to)) ? implode(',', $to) : $to;

       $this->client->setParameterPost('to', $to);

       // Sending request
       $response = $this->client->request()
                                ->getBody();

       return json_decode($response);
    }



    /* Query message status */
    public function query($ids)
    {
        $this->resetClientParameters();
        $this->client->setUri($this->url . $this->_commands[__FUNCTION__]);

        $ids = (is_array($ids)) ? implode(',', $ids) : $ids;

        $this->client->setParameterPost('msgid', $ids);

       // Sending request
       $response = $this->client->request()
                                ->getBody();

       return json_decode($response);
    }



    private function resetClientParameters()
    {
        $this->client->resetParameters();
        $this->client->setParameterPost('apikey', $this->apikey);
	}
}

?>
