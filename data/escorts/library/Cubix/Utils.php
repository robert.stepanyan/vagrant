<?php

class Cubix_Utils
{
	public static function makeSlug($str)
	{
		$slug = preg_replace('/[^-a-z0-9]/i', '-', $str);
		while ( FALSE !== strpos($slug, '--') ) {
			$slug = str_replace('--', '-', $slug);
		}
		$slug = trim($slug, '-');
		$slug = strtolower($slug);
		
		return $slug;
	}

	static function arrToObj($array) {
		if ( ! is_array($array) ) {
			return $array;
		}

		$object = new stdClass();
		foreach ( $array as $name => $value ) {
			if ( ! empty($name) ) {
				$object->$name = self::arrToObj($value);
			}
		}

		return $object;
	}

	static function getDaysInBetween($start, $end) {
		$day = 86400; // Day in seconds
		$format = 'Y-m-d'; // Output format (see PHP date funciton)
		$sTime = /*strtotime(*/$start/*)*/; // Start as time
		$eTime = /*strtotime(*/$end/*)*/; // End as time
		$numDays = round(($eTime - $sTime) / $day);
		$days = array();

		// Return days
		return abs($numDays);
	}

	static function print_row($label_id, $value, $th_width = null, $is_bold = true) {
		global $VIEW;

		if ( ! $value ) {
			return '';
		}

		$bold_class = '';
		if ( ! $is_bold ) {
			$bold_class = 'class="no_bold"';
		}

		return '<tr><th ' . (($th_width) ? 'style="width:' . $th_width . 'px"' : '') . '>' . __($label_id) . ':</th><td ' . $bold_class . '>' . $value . '</td></tr>';
	}
	
	/*
    case 0 = Unknown
    case 1 = Offline
    case 2 = Online
    case 3 = Away
    case 4 = Not Available
    case 5 = Do Not Disturb
    case 6 = Invisible
    case 7 = Skype Me
	*/
	
	static function getSkypeStatus($username, $lang = "de", $img_type = "bigclassic")
	{
		$url = "http://mystatus.skype.com/".$username.".xml";
		$data = @file_get_contents($url);

		$status = array();
		if($data === false)
		{
		$status = array("num" =>0,
						"text"=>"http error"
					);
		if(isset($http_response_header)) $status["error_info"] = $http_response_header;
		}
		else
		{
		$pattern = '/xml:lang="NUM">(.*)</';
		preg_match($pattern,$data, $match);
		
		switch ($match[1]) {
			//online top priority
			case 2:
				$status["num"] = 5;
				break;
			case 3:
				$status["num"] = 4;
				break;
			case 5:
				$status["num"] = 3;
				break;
			default:	
			$status["num"] = 0;
		}
		//$status["num"] = $match[1];
		$pattern = '/xml:lang="' . $lang .'">(.*)</';
		preg_match($pattern,$data, $match);

		$status["text"]    = $match[1];
		$status["img_url"] = "http://mystatus.skype.com/".$img_type."/".$username;
		}
		return $status;
	}

	public static function dateToAge($timestamp)
	{

		if ( $timestamp === false || is_null($timestamp) || ! $timestamp ) return null;

		if (strpos($timestamp, '-') ) {
			$timestamp = strtotime($timestamp);
		}

		$age = DateTime::createFromFormat('d/m/Y', date('d/m/Y', $timestamp) )->diff(new DateTime('now'))->y;
		return $age;
		// https://sceonteam.atlassian.net/browse/EDIR-175
		// $age = ($timestamp < 0 ) ? ($curr_time + ($timestamp * -1)) : $curr_time - $timestamp;
		// $year = 60 * 60 * 24 * 365;
		
		// $age_in_years = floor($age / $year);

		// if ($age_in_years < 18) $age_in_years = 18;

		// return $age_in_years;
	}
	
	public static function urlMaker($link)
	{
		if ( ! preg_match('#^https?://#', $link) ) $link = 'http://' . $link;

		$link = preg_replace('|\?.+|', '', $link);
		
		return $link;
	}
	
	public static function crypter( $string, $secret_key, $secret_iv,  $action = 'e' ) {
		
		$output = false;
		$encrypt_method = "AES-256-CBC";
		$key = hash( 'sha256', $secret_key );
		$iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

		if( $action == 'e' ) {
			$output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
		}
		else if( $action == 'd' ){
			$output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
		}

		return $output;
	}
	
	public static function phoneGrinder($phone, $limit = 5)
	{
		$length = strlen($phone);
		$grindered_phones = array($phone);
		
		for($i = 1; $i <= $length - $limit; $i++){
			$grindered_phones[] = substr($phone, $i );
		}
		
		return $grindered_phones;
	}
	
	public static function HTML5_upload()
	{
        $headers = array();
        $headers['Content-Length'] = $_SERVER['CONTENT_LENGTH'];
        $headers['X-File-Id'] = $_SERVER['HTTP_X_FILE_ID'];
        $headers['X-File-Name'] = $_SERVER['HTTP_X_FILE_NAME'];
        $headers['X-File-Resume'] = $_SERVER['HTTP_X_FILE_RESUME'];
        $headers['X-File-Size'] = $_SERVER['HTTP_X_FILE_SIZE'];

        $response = array();
        $response['id'] = $headers['X-File-Id'];
        $response['name'] = basename($headers['X-File-Name']); 	// Basename for security issues
        $response['size'] = $headers['Content-Length'];
        $response['error'] = UPLOAD_ERR_OK;
        $response['finish'] = FALSE;

        // Is resume?
        $flag = (bool)$headers['X-File-Resume'] ? FILE_APPEND : 0;
        $filename = $response['id'].'_'.$response['name'];
        $response['upload_name'] = $filename;

        // Write file
        $file = sys_get_temp_dir().DIRECTORY_SEPARATOR.$response['upload_name'];

        if (file_put_contents($file, file_get_contents('php://input'), $flag) === FALSE){
            $response['error'] = UPLOAD_ERR_CANT_WRITE;
        }
        else
        {
            $response['file_size'] = filesize($file);
            $response['X-File-Size'] = $headers['X-File-Size'];
            if (filesize($file) == $headers['X-File-Size'])
            {
                $response['tmp_name'] = $file;
                $response['finish'] = TRUE;
            }
        }

        return $response;
	}

	public static function getDebugBacktrace()
	{
		$backtrace = debug_backtrace();
		array_shift($backtrace);

		$trace_list = array();
		foreach($backtrace as $i => $bt) {
			if ( ! isset($bt['class']) ) $bt['class'] = '';
			if ( ! isset($bt['type']) ) $bt['type'] = '';
			if ( ! isset($bt['file']) ) $bt['file'] = '';
			if ( ! isset($bt['line']) ) $bt['line'] = '';

			$trace_list[] =
				($i + 1) . ". " .
				str_replace('\\', '/', str_replace(APP_ROOT_PATH, '', $bt['file'])) . ", " .
				$bt['line'] . "\n" .
				$bt['class'] . $bt['type'] . $bt['function'];
		}

		$trace_list = implode("\n\n", $trace_list);
		return $trace_list;
	}		
}