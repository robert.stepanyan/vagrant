<?php

/**
 * The Cubix_Twispay_TwispayApi class implements methods to get the value
 * of `jsonRequest` and `checksum` that need to be sent by POST
 * when making a Twispay order and to decrypt the Twispay IPN response.
 */
class Cubix_Twispay_TwispayApi
{
    const  TYPE_TRANSACTION = 'transaction';
    const  TYPE_ORDER       = 'order';

    /**
     * @var Array
     */
    private static $config;

    /**
     * @param $values
     */
    public static function setConfig($values)
    {
        self::$config = $values;
    }

    /**
     * @param string $type
     * @param array $queryParams
     * @return mixed
     * @throws Exception
     */
    public static function search($type, $queryParams)
    {
        $twispayApiUrl = self::$config['api_url'];
        $twispayKey = self::$config['key'];

        if (!$twispayKey || !$twispayApiUrl) throw new Exception('Configuration missing');
        if (!in_array($type, [self::TYPE_ORDER, self::TYPE_TRANSACTION])) throw new Exception('Type doesnt exist');

        $queryString = http_build_query($queryParams);

        $ch = curl_init($twispayApiUrl . '/' . $type . '/?' . $queryString);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization:" . $twispayKey));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        return json_decode(curl_exec($ch), true);
    }

    /**
     * @param string $type
     * @param integer $id
     * @return mixed
     * @throws Exception
     */
    public static function get($type, $id)
    {
        $twispayApiUrl = self::$config['api_url'];
        $twispayKey = self::$config['key'];

        if (!$twispayKey || !$twispayApiUrl) throw new Exception('Configuration missing');
        if (!in_array($type, [self::TYPE_ORDER, self::TYPE_TRANSACTION])) throw new Exception('Type doesnt exist');

        $ch = curl_init($twispayApiUrl . '/' . $type . '/' . $id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization:" . $twispayKey));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        return json_decode(curl_exec($ch), true);
    }

    /**
     * Get the `jsonRequest` parameter (order parameters as JSON and base64 encoded).
     *
     * @param array $orderData The order parameters.
     *
     * @return string
     */
    public static function getBase64JsonRequest(array $orderData)
    {
        return base64_encode(json_encode($orderData));
    }

    /**
     * Get the `checksum` parameter (the checksum computed over the `jsonRequest` and base64 encoded).
     *
     * @param array $orderData The order parameters.
     * @param string $secretKey The secret key (from Twispay).
     *
     * @return string
     */
    public static function getBase64Checksum(array $orderData, $secretKey)
    {
        $hmacSha512 = hash_hmac('sha512', json_encode($orderData), $secretKey, true);
        return base64_encode($hmacSha512);
    }

    /**
     * Decrypt the IPN response from Twispay.
     *
     * @param string $encryptedIpnResponse
     * @param string $secretKey The secret key (from Twispay).
     *
     * @return array
     */
    public static function decryptIpnResponse($encryptedIpnResponse, $secretKey)
    {
        // get the IV and the encrypted data
        $encryptedParts = explode(',', $encryptedIpnResponse, 2);
        $iv = base64_decode($encryptedParts[0]);
        $encryptedData = base64_decode($encryptedParts[1]);

        // decrypt the encrypted data
        $decryptedIpnResponse = openssl_decrypt($encryptedData, 'aes-256-cbc', $secretKey, OPENSSL_RAW_DATA, $iv);

        # JSON decode the decrypted data
        return json_decode($decryptedIpnResponse, true, 4);
    }
}