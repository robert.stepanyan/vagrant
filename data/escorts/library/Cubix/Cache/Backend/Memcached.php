<?php

class Cubix_Cache_Backend_Memcached extends Zend_Cache_Backend_Memcached
{
	private $_key_prefix;
	
	public function setKeyPrefix($key_prefix)
	{
		if ( ! $key_prefix ) $key_prefix = Cubix_Application::getId();
		$this->_key_prefix = $key_prefix;
	}
	
	public function getIdPrefixed($id)
	{
		if ( ! $this->_key_prefix ) {
			$prefix = Cubix_Application::getId();
		}
		else {
			$prefix = $this->_key_prefix;
		}
		
		return $prefix . '_' . $id;
	}

    public function load($id, $doNotTestCacheValidity = false)
    {
		$id = $this->getIdPrefixed($id);
		return parent::load($id, $doNotTestCacheValidity);
    }

    public function test($id)
    {
		$id = $this->getIdPrefixed($id);
        return parent::test($id);
    }

    public function save($data, $id, $tags = array(), $specificLifetime = false)
    {
		$id = $this->getIdPrefixed($id);
        return parent::save($data, $id, $tags, $specificLifetime);
    }

    public function remove($id)
    {
		$id = $this->getIdPrefixed($id);
        return parent::remove($id);
    }
}
