<?php

abstract class Cubix_Filter_Abstract
{
	protected $_renderer;
	
	protected $_selected;
	
	protected $_mapperParam;
	
	protected $_existingFilters = array();
	
	protected $_filter_defines = array();

    protected $_isMobileView = false;
	
	abstract public function makeQuery();

    public function setIsMobileView($isMobileView)
    {
        $this->_isMobileView = $isMobileView;
    }

    public function getIsMobileView()
    {
        return $this->_isMobileView;
    }
	
	public function setSelected($value)
	{
		if ( is_null($value[0]) ) {
			$value = array();
		}
		
		if ( ! is_array($value) ) $value = array($value);
		
		$this->_selected = $value;
	}
	
	public function getSelected()
	{
		return $this->_selected;
	}
	
	public function setMapperParam(Cubix_Filter_Mapper_Parameter $param)
	{
		$this->_mapperParam = $param;
	}
	
	public function getMapperParam()
	{
		return $this->_mapperParam;
	}
	
	public function setExistingFilters($existing_filters)
	{
		$this->_existingFilters = $existing_filters;
	}
	
	public function getExistingFilters()
	{
		return $this->_existingFilters;
	}
	
	public function setFilterDefines($filter_defines)
	{
		$this->_filter_defines = $filter_defines;
	}
	
	public function getFilterDefines()
	{
		return $this->_filter_defines;
	}
	
	abstract public function getRenderer();
}