<?php

class Cubix_Filter_Custom_Travel extends Cubix_Filter_Multiple
{	
		
	public function getRenderer()
	{
		$existing_filter = $this->getExistingFilters();
		$filter_defines = $this->getFilterDefines();
		
		$data = array(
			'title' => Cubix_I18n::translate('travel'),
			'selected' => $this->getSelected(),
			'mapperParam' => $this->getMapperParam(),
			'existingFilters' => $existing_filter['result'],
			'filterDefines' => $filter_defines,
            'isMobileView' => $this->getIsMobileView(),
		);
		
		return new Cubix_Filter_Renderer_Travel($data);
	}
}
