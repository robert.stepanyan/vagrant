<?php

class Cubix_Filter_Custom_Incall extends Cubix_Filter_Multiple
{	
	public function makeQuery()
	{
		$queries = array();
		$filter_queries = array();
		$query = '';
		$filter_query = '';
		
		$this->_setQueryJoiner($this->getMapperParam()->getQueryJoiner());
		
		if ( count($this->getSelected()) ) {
			foreach( $this->getSelected() as $selected ) {
				$value = explode('_', $selected);
				// IF incall value
				if (preg_match('#i_#', $selected) ) {
					$queries[] = 'e.incall_type = ' . $value[1];
					$filter_queries[] = 'fd.available_for_i_' .  $value[1]  . ' = 1';
				} else {
					$queries[] = 'e.outcall_type = ' . $value[1];
					$filter_queries[] = 'fd.available_for_o_' .  $value[1]  . ' = 1';
				}
				
				/*if ( $selected ) {
					if ( $selected == 1 ) {
						$queries[] = 'e.outcall_type IS NOT NULL';
						$filter_queries[] = 'fd.available_for_' .  $selected  . ' = 1';
					} else {
						$queries[] = 'e.incall_type IS NOT NULL';
						$filter_queries[] = 'fd.available_for_' .  $selected  . ' = 1';
					}
				}*/
			}
			
			$query = implode(' ' . $this->_getQueryJoiner() . ' ', $queries);
			$filter_query = implode(' ' . $this->_getQueryJoiner() . ' ', $filter_queries);
			
			if ( $this->_getQueryJoiner() == self::QUERY_JOINER_OR ) {
				$query = ' ( ' . $query . ' ) ';
				$filter_query = ' ( ' . $filter_query . ' ) ';
			}
		}
		
		return array('query' => $query, 'filter_query' => $filter_query);
	}
	
	public function getRenderer()
	{
		$existing_filter = $this->getExistingFilters();
		$filter_defines = $this->getFilterDefines();
		
		$data = array(
			'title' => Cubix_I18n::translate('incall'),
			'selected' => $this->getSelected(),
			'mapperParam' => $this->getMapperParam(),
			'existingFilters' => $existing_filter['result'],
			'filterDefines' => $filter_defines,
            'isMobileView' => $this->getIsMobileView(),
		);
		
		return new Cubix_Filter_Renderer_Incall($data);
	}
}
