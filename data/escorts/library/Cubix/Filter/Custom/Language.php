<?php

class Cubix_Filter_Custom_Language extends Cubix_Filter_Multiple
{	
	public function makeQuery()
	{
		$queries = array();
		$filter_queries = array();
		$query = '';
		$filter_query = '';
		
		$this->_setQueryJoiner($this->getMapperParam()->getQueryJoiner());
		
		if ( count($this->getSelected()) ) {
			foreach( $this->getSelected() as $selected ) {
				if ( $selected ) {
					$queries[] = 'FIND_IN_SET(\'' . $selected . '\', ' . $this->getMapperParam()->getField() . ')';
					$filter_queries[] = $this->getMapperParam()->getFilterField() . '_' .  $selected  . ' = 1';
				}
			}
			
			$query = implode(' ' . $this->_getQueryJoiner() . ' ', $queries);
			$filter_query = implode(' ' . $this->_getQueryJoiner() . ' ', $filter_queries);
			
			if ( $this->_getQueryJoiner() == self::QUERY_JOINER_OR ) {
				$query = ' ( ' . $query . ' ) ';
				$filter_query = ' ( ' . $filter_query . ' ) ';
			}
		}
		
		return array('query' => $query, 'filter_query' => $filter_query);
	}
	
	public function getRenderer()
	{
		$existing_filter = $this->getExistingFilters();
		$filter_defines = $this->getFilterDefines();
		
		$data = array(
			'title' => Cubix_I18n::translate('language'),
			'selected' => $this->getSelected(),
			'mapperParam' => $this->getMapperParam(),
			'existingFilters' => $existing_filter['result'],
			'filterDefines' => $filter_defines,
            'isMobileView' => $this->getIsMobileView(),
		);
		
		return new Cubix_Filter_Renderer_Language($data);
	}
}
