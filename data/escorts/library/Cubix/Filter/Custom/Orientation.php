<?php

class Cubix_Filter_Custom_Orientation extends Cubix_Filter_Multiple
{	
	public function getRenderer()
	{
		$existing_filter = $this->getExistingFilters();
		$filter_defines = $this->getFilterDefines();
		
		if ( isset($existing_filter['global_filter']) ) {
			
			foreach( $existing_filter['where_query'] as $k => $wq ) {
				if ( preg_match("#fd.orientation#", $wq['filter_query']) ) {
					unset($existing_filter['where_query'][$k]);
					$existing_filter = Model_Escort_List::getActiveFilter($existing_filter['global_filter'], $existing_filter['where_query'], $existing_filter['is_upcoming'], $existing_filter['is_tour']);
				}
			}
		}
		
		$data = array(
			'title' => Cubix_I18n::translate('orientation'),
			'selected' => $this->getSelected(),
			'mapperParam' => $this->getMapperParam(),
			'existingFilters' => $existing_filter['result'],
			'filterDefines' => $filter_defines,
            'isMobileView' => $this->getIsMobileView(),
		);
		
		return new Cubix_Filter_Renderer_Orientation($data);
	}
}
