<?php

class Cubix_Filter_Simple_Exception extends Exception {}

class Cubix_Filter_Simple extends Cubix_Filter_Abstract
{
	public function makeQuery()
	{
		$queries = array();
		$filter_queries = array();
		$query = '';
		$filter_query = '';
		
		if ( count($this->getSelected()) > 1 ) {
			throw new Cubix_Filter_Simple_Exception('Invalid parameter specified for filter "' . $this->getMapperParam()->getFilter() . '"!');
		}
		
		if( count($this->getSelected()) ) {
			foreach( $this->getSelected() as $selected ) {
				if ( $selected ) {
					$queries[] = $this->getMapperParam()->getField() . ' = ' . $selected;
					$filter_queries[] = $this->getMapperParam()->getFilterField() . '_' . $selected . ' = 1';
				}
			}
			
			$query = implode(' AND ', $queries);
			$filter_query = implode(' AND ', $filter_queries);
		}
		
		return array('query' => $query, 'filter_query' => $filter_query);
	}
	
	public function getRenderer()
	{
		
	}
}
