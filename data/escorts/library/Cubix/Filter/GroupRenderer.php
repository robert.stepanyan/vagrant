<?php

class Cubix_Filter_GroupRenderer
{
	protected $_title;
	
	protected $_scriptName;
	
	protected $_scriptPath;

	protected $_visibleCount = 4;
	
	protected $_content = array();
	
	protected $_width;
	
	protected $_height;

	protected $_weight;
	
	protected $_view;
	
	protected $_groupId;
	
	public function __construct(array $options)
	{
		$this->_view = new Zend_View();
		
		if ( count($options) ) {
			$this->setOptions($options);
		}
	}
	
	public function setOptions(array $options)
	{
		foreach ( $options as $function => $value ) {
			$function_name = 'set' . ucfirst($function);
			
			if ( method_exists($this, $function_name) ) {
				$this->$function_name($value);
			}
		}
	}
	
	public function render()
	{
		$this->_view->setScriptPath($this->_scriptPath);
		
		$this->_view->title = $this->_title;
		$this->_view->width = $this->_width;
		$this->_view->height = $this->_height;
		$this->_view->content = $this->_content;
		$this->_view->id = $this->_groupId;
		$this->_view->visibleCount = $this->_visibleCount;
				
		return $this->_view->render($this->_scriptName);
	}
	
	public function setTitle($title)
	{
		$this->_title = $title;
	}
	
	public function getTitle()
	{
		return $this->_title;
	}

	public function setVisibleCount($count)
	{
		$this->_visibleCount = $count;
	}
	
	public function getVisibleCount()
	{
		return $this->_visibleCount;
	}
	
	public function setGroupId($id)
	{
		$this->_groupId = $id;
	}
	
	public function getGroupId()
	{
		return $this->_groupId;
	}
	
	public function setScriptName($script_name)
	{
		if ( ! preg_match('#.phtml#', $script_name) ) {
			$script_name = $script_name . '.phtml';
		}
		
		$this->_scriptName = $script_name;
	}
	
	public function getScriptName()
	{
		return $this->_scriptName;
	}
	
	public function getView()
	{
		return $this->_view;
	}
	
	public function setScriptPath($path)
	{
		$this->_scriptPath = $path;
	}
	
	public function setContent($content)
	{
		$this->_content[] = $content;
	}
	
	public function getContent()
	{
		return $this->_content;
	}
	
	public function setWidth($width)
	{
		$this->_width = $width;
	}
	
	public function getWidth()
	{
		return $this->_width;
	}
	
	public function setHeight($height)
	{
		$this->_height = $height;
	}
	
	public function getHeight()
	{
		return $this->_height;
	}

	public function setWeight($weight)
	{
		$this->_weight = $weight;
	}
	
	public function getWeight()
	{
		return $this->_weight;
	}
}