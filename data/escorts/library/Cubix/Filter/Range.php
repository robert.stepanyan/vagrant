<?php

class Cubix_Filter_Range_Exception extends Exception {}

class Cubix_Filter_Range extends Cubix_Filter_Abstract
{
	public function makeQuery()
	{
		$queries = array();
		$filter_queries = array();
		$query = '';
		$filter_query = '';
		
		if( count($this->getSelected()) ) {
			foreach( $this->getSelected() as $selected ) {
				list($from, $to) = explode('-', $selected);
				
				if ( ! $from || ! $to ) {
					throw new Cubix_Filter_Range_Exception('Invalid range was specified for filter "' . $this->getMapperParam()->getFilter() . '"!');
				}
				
				$queries[] = '(' . $this->getMapperParam()->getField() . ' >= ' . $from . ' AND ' . $this->getMapperParam()->getField() . ' <= ' . $to . ')';
				$filter_queries[] = $this->getMapperParam()->getFilterField() . '_' . $from . '_' . $to . ' = 1 ';
			}
			
			$query = implode(' OR ', $queries);
			$filter_query = implode(' OR ', $filter_queries);
			
			$query = ' ( ' . $query . ' ) ';
			$filter_query = ' ( ' . $filter_query . ' ) ';
		}
		
		return array('query' => $query, 'filter_query' => $filter_query);
	}
	
	public function getRenderer()
	{
		
	}
}
