<?php

class Cubix_Filter_Mapper
{
	protected $_params = array();
	
	public function setParam(Cubix_Filter_Mapper_Parameter $param)
	{
		$this->_params[$param->getWeight()] = $param;
	}
	
	public function getAll()
	{
		return $this->_params;
	}
}
