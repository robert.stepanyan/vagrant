<?php

class Cubix_Filter_PopupRenderer
{
	protected $_title;
	
	protected $_scriptName;
	
	protected $_scriptPath;
	
	protected $_content = array();
	
	protected $_width;

	protected $_isGroup = false;
	
	protected $_height;
	
	protected $_view;
	
	protected $_popupId;
	
	public function __construct(array $options)
	{
		$this->_view = new Zend_View();
		
		if ( count($options) ) {
			$this->setOptions($options);
		}
	}
	
	public function setOptions(array $options)
	{
		foreach ( $options as $function => $value ) {
			$function_name = 'set' . ucfirst($function);
			
			if ( method_exists($this, $function_name) ) {
				$this->$function_name($value);
			}
		}
	}
	
	public function render()
	{
		$this->_view->setScriptPath($this->_scriptPath);
		
		$this->_view->title = $this->_title;
		$this->_view->width = $this->_width;
		$this->_view->height = $this->_height;
		$this->_view->content = $this->_content;
		$this->_view->id = $this->_popupId;
		$this->_view->is_group = $this->_isGroup;
				
		return $this->_view->render($this->_scriptName);
	}
	
	public function setTitle($title)
	{
		$this->_title = $title;
	}
	
	public function getTitle()
	{
		return $this->_title;
	}
	
	public function setPopupId($id)
	{
		$this->_popupId = $id;
	}
	
	public function getPopupId()
	{
		return $this->_popupId;
	}

	public function setIsGroup($is_group)
	{
		$this->_isGroup = $is_group;
	}
	
	public function getIsGroup()
	{
		return $this->_isGroup;
	}
	
	public function setScriptName($script_name)
	{
		if ( ! preg_match('#.phtml#', $script_name) ) {
			$script_name = $script_name . '.phtml';
		}
		
		$this->_scriptName = $script_name;
	}
	
	public function getScriptName()
	{
		return $this->_scriptName;
	}
	
	public function getView()
	{
		return $this->_view;
	}
	
	public function setScriptPath($path)
	{
		$this->_scriptPath = $path;
	}
	
	public function setContent($content)
	{
		$this->_content[] = $content;
	}
	
	public function getContent()
	{
		return $this->_content;
	}
	
	public function setWidth($width)
	{
		$this->_width = $width;
	}
	
	public function getWidth()
	{
		return $this->_width;
	}
	
	public function setHeight($height)
	{
		$this->_height = $height;
	}
	
	public function getHeight()
	{
		return $this->_height;
	}
}