<?php

class Cubix_Filter_Mapper_Parameter
{
	private $_name;
	private $_filter;
	private $_group;
	private $_weight = 0;
	private $_groupWeight = 0;
	private $_field;
	private $_filterField;
	private $_queryJoiner;
	
	public function __construct(array $options = array())
	{
		if ( count($options) ) {
			$this->setOptions($options);
		}
	}
	
	private function setOptions(array $options)
	{
		foreach ( $options as $function => $value ) {
			$function_name = 'set' . ucfirst($function);
			
			if ( method_exists($this, $function_name) ) {
				$this->$function_name($value);
			}
		}
	}
	
	public function setName($name)
	{
		$this->_name = $name;
	}
	
	public function setField($field)
	{
		$this->_field = $field;
	}

	public function setGroup($group)
	{
		$this->_group = $group;
	}
	
	public function setFilterField($filter_field)
	{
		$this->_filterField = $filter_field;
	}
	
	public function setQueryJoiner($joiner)
	{
		$this->_queryJoiner = $joiner;
	}
	
	public function setFilter($filter)
	{
		$this->_filter = $filter;
	}
	
	public function setWeight($weight)
	{
		$this->_weight = $weight;
	}

	public function setGroupWeight($weight)
	{
		$this->_groupWeight = $weight;
	}
	
	public function getName()
	{
		return $this->_name;
	}
	
	public function getField()
	{
		return $this->_field;
	}

	public function getGroup()
	{
		return $this->_group;
	}
	
	public function getFilterField()
	{
		return $this->_filterField;
	}
	
	public function getQueryJoiner()
	{
		return $this->_queryJoiner;
	}
	
	public function getFilter()
	{
		return $this->_filter;
	}
	
	public function getWeight()
	{
		return $this->_weight;
	}

	public function getGroupWeight()
	{
		return $this->_groupWeight;
	}
}
