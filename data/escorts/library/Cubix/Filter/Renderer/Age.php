<?php

class Cubix_Filter_Renderer_Age extends Cubix_Filter_Renderer_Abstract
{
	public function __construct(array $options)
	{
		$defs = Zend_Registry::get('definitions');
		$defined_values = $defs['escorts_filter_v2']['age'];
		
		$options = $options + array('scriptName' => 'age', 'definedValues' => $defined_values, 'popupWidth' => 335, 'popupHeight' => null);
		parent::__construct($options);
	}	
		
	public function renderFull()
	{
		$this->setRenderFull(true);
		$this->setIsCollapsed(false);
		
		return parent::render();
	}
}