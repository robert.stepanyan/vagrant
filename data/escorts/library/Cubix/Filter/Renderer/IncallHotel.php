<?php

class Cubix_Filter_Renderer_IncallHotel extends Cubix_Filter_Renderer_Abstract
{
	public function __construct(array $options)
	{
		if(count($options['filterDefines']) > 0){
			$defined_values = $options['filterDefines']['available-for-incall-hotel'];
		}
		else{
			$defs = Zend_Registry::get('definitions');
			$defined_values = $defs['escorts_filter_v2']['available-for-incall-hotel'];
		}
		
		$options = $options + array('scriptName' => 'incall-hotel', 'definedValues' => $defined_values, 'popupWidth' => 450, 'popupHeight' => null);
		parent::__construct($options);
	}	
		
	public function renderFull()
	{
		$this->setRenderFull(true);
		$this->setIsCollapsed(false);
		
		return parent::render();
	}
}