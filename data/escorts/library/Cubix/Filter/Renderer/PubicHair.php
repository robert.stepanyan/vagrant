<?php

class Cubix_Filter_Renderer_PubicHair extends Cubix_Filter_Renderer_Abstract
{
	public function __construct(array $options)
	{
		if(count($options['filterDefines']) > 0){
			$defined_values = $options['filterDefines']['pubic-hair'];
		}
		else{
			$defs = Zend_Registry::get('definitions');
			$defined_values = $defs['escorts_filter_v2']['pubic-hair'];
		}
		
		$options = $options + array('scriptName' => 'pubic-hair', 'definedValues' => $defined_values, 'popupWidth' => 335, 'popupHeight' => null);
		parent::__construct($options);
	}	
		
	public function renderFull()
	{
		$this->setRenderFull(true);
		$this->setIsCollapsed(false);
		
		return parent::render();
	}
}