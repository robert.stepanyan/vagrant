<?php

abstract class Cubix_Filter_Renderer_Abstract
{
	protected $_title;
	
	protected $_selected;
	
	protected $_scriptName;

	protected $_isMobileView = false;

	protected $_view;
	
	protected $_visibleCount = 3;
	
	protected $_mapperParam;
	
	protected $_scriptPath;
	
	protected $_definedValues;

	protected $_isCollapsed = false;
	
	protected $_popupWidth;
	
	protected $_popupHeight;
	
	protected $_renderFull = false;
	
	protected $_showTitle = false;
	
	protected $_existingFilters = array();
	
	public function __construct(array $options = array())
	{
		$this->_view = new Zend_View();
		
		if ( count($options) ) {
			$this->setOptions($options);
		}
	}
	
	public function setOptions(array $options)
	{
		foreach( $options as $function => $value ) {
			$function_name = 'set' . ucfirst($function);
			
			if (method_exists($this, $function_name) ) {
				$this->$function_name($value);
			}
		}
	}
	
	/**
	 *
	 * @return Model_Escort_Filter_Renderer_Abstract
	 */
	public function render()
	{
		$this->_view->setScriptPath($this->_scriptPath);
		
		$this->_view->defined_values = $this->getDefinedValues();
		$this->_view->existing_filters = $this->getExistingFilters();
		
		$this->_view->title = $this->_title;
		$this->_view->show_title = $this->_showTitle;
		$this->_view->render_full = $this->_renderFull;
		$this->_view->is_collapsed = $this->_isCollapsed;
		$this->_view->selected = $this->_selected;
		$this->_view->visible_count = $this->_visibleCount;
		$this->_view->isMobileView = $this->_isMobileView;
		$this->_view->mapper_param = $this->_mapperParam;
		
		return $this->_view->render($this->_scriptName);
	}
	
	abstract public function renderFull();
	
	public function setShowTitle($is_show)
	{
		$this->_showTitle = $is_show;
	}
	
	public function setTitle($title)
	{
		$this->_title = $title;
	}
	
	public function getTitle()
	{
		return $this->_title;
	}

    /**
     * @param bool $isMobileView
     */
    public function setIsMobileView($isMobileView) {
        $this->_isMobileView = $isMobileView;
    }

    public function getIsMobileView() {
        return $this->_isMobileView;
    }


	public function setSelected($value)
	{
		if (!isset($value[0]) || is_null($value[0]) ) {
			$value = array();
		}
		
		if ( ! is_array($value) ) $value = array($value);
		
		$this->_selected = $value;
	}
	
	public function getSelected()
	{
		return $this->_selected;
	}
	
	public function setScriptName($script_name)
	{
		if ( ! preg_match('#.phtml#', $script_name) ) {
			$script_name = $script_name . '.phtml';
		}
		
		$this->_scriptName = $script_name;
	}
	
	public function getScriptName()
	{
		return $this->_scriptName;
	}
	
	public function getView()
	{
		return $this->_view;
	}
	
	public function setScriptPath($path)
	{
		$this->_scriptPath = $path;
	}

	public function setDefinedValues($defined_values)
	{
		$this->_definedValues = $defined_values;
	}
	
	public function getDefinedValues()
	{
		return $this->_definedValues;
	}
	
	public function setMapperParam(Cubix_Filter_Mapper_Parameter $param)
	{
		$this->_mapperParam = $param;
	}
	
	public function getMapperParam()
	{
		return $this->_mapperParam;
	}
	
	public function setVisibleCount($visible_count)
	{
		$this->_visibleCount = $visible_count;
	}
	
	public function getVisibleCount()
	{
		return $this->_visibleCount;
	}
	
	public function setIsCollapsed($is_collapsed)
	{
		$this->_isCollapsed = $is_collapsed;
	}
	
	public function getIsCollapsed()
	{
		return $this->_isCollapsed;
	}
	
	public function setPopupWidth($width)
	{
		$this->_popupWidth = $width;
	}
	
	public function getPopupWidth()
	{
		return $this->_popupWidth;
	}
	
	public function setPopupHeight($height)
	{
		$this->_popupHeight = $height;
	}
	
	public function getPopupHeight()
	{
		return $this->_popupHeight;
	}
	
	public function setRenderFull($is_full)
	{
		$this->_renderFull = $is_full;
	}
	
	public function setExistingFilters($existing_filters)
	{
		$this->_existingFilters = $existing_filters;
	}
	
	public function getExistingFilters()
	{
		return $this->_existingFilters;
	}
}