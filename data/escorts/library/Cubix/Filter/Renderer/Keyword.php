<?php

class Cubix_Filter_Renderer_Keyword extends Cubix_Filter_Renderer_Abstract
{
	public function __construct(array $options)
	{
		if(count($options['filterDefines']) > 0){
			$defined_values = $options['filterDefines']['keywords'];
		}
		else{
			$defs = Zend_Registry::get('definitions');
			$defined_values = $defs['escorts_filter_v2']['keywords'];
		}
		
		$options = $options + array('scriptName' => 'keyword', 'definedValues' => $defined_values, 'popupWidth' => 400, 'popupHeight' => null);
		parent::__construct($options);
	}	
		
	public function renderFull()
	{
		$this->setRenderFull(true);
		$this->setIsCollapsed(false);

		return parent::render();
	}
}