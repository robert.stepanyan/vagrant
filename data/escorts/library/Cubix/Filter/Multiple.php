<?php

class Cubix_Filter_Multiple extends Cubix_Filter_Abstract
{
	const QUERY_JOINER_AND = 'AND';
	const QUERY_JOINER_OR = 'OR';
	
	protected $_queryJoiner = self::QUERY_JOINER_OR;
	
	protected function _setQueryJoiner($joiner)
	{
		$joiners = array(self::QUERY_JOINER_AND, self::QUERY_JOINER_OR);
		
		if ( ! in_array($joiner, $joiners) ) {
			throw new Cubix_Filter_Multiple_Exception('Invalid query joiner was specified for filter "' . $this->getMapperParam()->getFilter() . '"!');
		}
		
		$this->_queryJoiner = $joiner;
	}
	
	protected function _getQueryJoiner()
	{
		return $this->_queryJoiner;
	}
	
	public function makeQuery()
	{
		$queries = array();
		$filter_queries = array();
		$query = '';
		$filter_query = '';
		
		$this->_setQueryJoiner($this->getMapperParam()->getQueryJoiner());
		
		if( count($this->getSelected()) ) {
			foreach( $this->getSelected() as $selected ) {
				if ( $selected ) {
					$queries[] = $this->getMapperParam()->getField() . ' = \'' . $selected . '\'';
					$filter_queries[] = $this->getMapperParam()->getFilterField() . '_' .  $selected  . ' = 1';
				}
			}
			
			$query = implode(' ' . $this->_getQueryJoiner() . ' ', $queries);
			$filter_query = implode(' ' . $this->_getQueryJoiner() . ' ', $filter_queries);
			
			if ( $this->_getQueryJoiner() == self::QUERY_JOINER_OR ) {
				$query = ' ( ' . $query . ' ) ';
				$filter_query = ' ( ' . $filter_query . ' ) ';
			}
		}
		
		return array('query' => $query, 'filter_query' => $filter_query);
	}
	
	public function getRenderer()
	{
		
	}
}
