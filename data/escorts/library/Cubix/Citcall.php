<?php

class Cubix_Citcall extends Cubix_Model
{
	private $api_key;
	private $base_url = 'https://gateway.citcall.com';
	private $version = '/v3';
	
	public function __construct($api_key)
	{		
		$this->api_key = $api_key;
	}
		
	public function asynccall($phone)
	{
		$gatway = substr($phone, 0, 3) == '+62' ? 2 : 4; // for indonesia 
		
		try{
			$action = '/asynccall';
			$url = $this->base_url . $this->version . $action ;
			$data = array(
				"msisdn" => $phone,
				"gateway" => $gatway
			);

			$content = json_encode($data);
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Authorization: Apikey ' . $this->api_key,
				'Content-Length: ' . strlen($content))
			);
			$response = curl_exec($curl);
			$err  = curl_error($curl);

			curl_close($curl);
			$ret = array();
			
			if ($err) {
				$ret['result'] = "Failed";
				$ret['error'] = $err;
			} else {
				$res = json_decode($response);
				$rc = $res->rc;
				if($rc == "00"){
					
					$ret['trx_id'] = $res->trxid;
					$ret['token'] = $res->token;
					$ret['msisdn'] = $res->msisdn;
					$ret['result'] = "Success";
				}else{
					$ret['result'] = "Failed";
					$ret['error'] = $rc;
				}
			}
		}catch(Exception $e){
			$ret['result'] = "Failed";
			$ret['error'] = $e->getMessage();
		}
		
		return $ret;
	}		
}
