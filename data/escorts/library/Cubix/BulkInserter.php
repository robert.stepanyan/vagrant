<?php

class Cubix_BulkInserter
{
	private $db;
	private $table;
	private $keys = '';
	private $values = array();
	
	public function __construct($db, $table)
	{
		$this->db = $db;
		$this->table = $table;
	}
	
	public function addKeys($keys)
	{
		$this->keys = implode(',', $keys);
	}
	
	public function addFields($fields)
	{
		$value_string = '(';
		foreach($fields as $field){
			switch ( gettype($field)) {
				case 'string':
					$value_string .= $this->db->quote($field);
					break;
				case 'NULL':
					$value_string .= "NULL";
					break;
				default:
					$value_string .= $field;
			}
			$value_string .= ',';		
		}
		
		$this->values[] = substr($value_string, 0, -1) . ')';
		unset($value_string);
	}
	
	public function insertBulk()
	{
		$values = implode(',', $this->values);
		$sql  = 'INSERT IGNORE INTO ' . $this->table . ' (' . $this->keys . ') VALUES '. $values;
		unset($this->values);
		$this->values = array();
		$this->db->query($sql);
		return true;
	}
	
	public function returnSql()
	{
		$values = implode(',', $this->values);
		$sql  = 'INSERT IGNORE INTO ' . $this->table . ' (' . $this->keys . ') VALUES '. $values;
		unset($this->values);
		$this->values = array();
			
		return $sql;
	}
	
}
