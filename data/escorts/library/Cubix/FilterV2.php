<?php

class Cubix_FilterV2_Exception extends Exception {} 

class Cubix_FilterV2
{
	protected $_mapper;
	
	protected $_request;
	
	protected $_existing_filters;
	
	protected $_filter_defines;
	protected $_is_mobile = false;

	const SCRIPT_PATH = '../application/views/scripts/filters';
	
	public function __construct(array $options)
	{
		if ( count($options) ) {
			$this->setOptions($options);
		}
	}
	
	public function setOptions(array $options)
	{
		foreach ( $options as $function => $value ) {
			$function_name = 'set' . ucfirst($function);
			
			if ( method_exists($this, $function_name) ) {
				$this->$function_name($value);
			}
		}
	}
	
	public function setMapper(array $mapper)
	{
		$this->_mapper = $mapper;
	}

	public function setIsMobile($isMobile)
	{
		$this->_is_mobile = $isMobile;
	}

	public function getMapper()
	{
		return $this->_mapper;
	}
	
	public function setExistingFilters($existing_filters)
	{
		$this->_existing_filters = $existing_filters;
	}
	
	public function getExistingFilters()
	{
		return $this->_existing_filters;
	}
	
	public function setRequest(Zend_Controller_Request_Abstract $request)
	{
		$this->_request = $request;
	}
	
	public function setFilterDefines($filter_defines)
	{
		$this->_filter_defines = $filter_defines;
	}
	
	public function getFilterDefines()
	{
		return $this->_filter_defines;
	}
		
	public function render()
	{
		$html = array();
		$collapsed_html = array();
		$popup_html = array();
		
		if ( count($this->_mapper) ) {
			$i = 1;

			$collapsed_popup = new Cubix_Filter_PopupRenderer(array('title' => Cubix_I18n::translate('others'), 'width' => 335, 'popupId' => 'f_other'));
			$collapsed_popup->setScriptPath(self::SCRIPT_PATH);
			$collapsed_popup->setScriptName('popup');

			foreach ( $this->_mapper as $mapper_param ) {

				$filter = $this->_factory($mapper_param->getFilter());
				
				$filter->setSelected($this->_request->{$mapper_param->getName()});
				$filter->setMapperParam($mapper_param);
				$filter->setExistingFilters($this->getExistingFilters());
				
				$renderer = $filter->getRenderer();
				$renderer->setScriptPath(self::SCRIPT_PATH);

				if ( $i > 7 ) {
					$renderer->setIsCollapsed(true);
					$collapsed_html[] = $renderer->render();
					
					$renderer->setShowTitle(true);
					$collapsed_popup->setContent($renderer->renderFull());
					
				} else {
					$normal = $renderer->render();
					$html[] = $normal;
					
					if ( strlen($normal) <= 25 ) {
						$i--;
					} 
					
					$normal_popup = new Cubix_Filter_PopupRenderer(array('title' => $renderer->getTitle(), 'popupId' => 'f_' . $renderer->getMapperParam()->getName(), 'width' => $renderer->getPopupWidth(), 'height' => $renderer->getPopupHeight()));
					$normal_popup->setScriptPath(self::SCRIPT_PATH);
					$normal_popup->setScriptName('popup');
					
					$normal_popup->setContent($renderer->renderFull());
					
					$popup_html[] = $normal_popup->render();
				}
				
				$i++;
			}
			
			$popup_html[] = $collapsed_popup->render();
		}
		
		return array('full' => $html, 'collapsed' => $collapsed_html, 'popup' => $popup_html);
	}

	public function renderV2()
	{
		$html = array();
		$grouped_html = array();
		$_grouped_html = array();
		$popup_html = array();
		
		if ( count($this->_mapper) ) {
			$i = 1;

			$collapsed_popup = array();

			foreach ( $this->_mapper as $mapper_param ) {

				$filter = $this->_factory($mapper_param->getFilter());
				
				$filter->setSelected($this->_request->{$mapper_param->getName()});
				$filter->setMapperParam($mapper_param);
				$filter->setExistingFilters($this->getExistingFilters());
				
				$renderer = $filter->getRenderer();
				$renderer->setScriptPath(self::SCRIPT_PATH);

				$renderer->setVisibleCount(4);

				// Render grouped options and popups
				if ( $mapper_param->getGroup() ) {					

					$group_title = preg_replace("/\s+/", "-", strtolower($mapper_param->getGroup()));

					if ( ! isset($grouped_html[$group_title])) {
						$grouped_html[$group_title] = new Cubix_Filter_GroupRenderer(array('title' => $group_title, 'width' => 335, 'groupId' => 'f_' . $group_title));
					}
					
					$grouped_html[$group_title]->setScriptPath(self::SCRIPT_PATH);
					$grouped_html[$group_title]->setScriptName('group');
					$grouped_html[$group_title]->setWeight($mapper_param->getGroupWeight());

					$renderer->setShowTitle(true);
					$renderer->setIsCollapsed(true);
					$grouped_html[$group_title]->setContent($renderer->render());

					
					
					if ( ! isset($collapsed_popup[$group_title])) {
						$collapsed_popup[$group_title] = new Cubix_Filter_PopupRenderer(array('title' => Cubix_I18n::translate($group_title), 'width' => 450, 'popupId' => 'f_' . $group_title));
					}
					
					$collapsed_popup[$group_title]->setScriptPath(self::SCRIPT_PATH);
					$collapsed_popup[$group_title]->setScriptName('popup');
					$collapsed_popup[$group_title]->setIsGroup(true);

					$renderer->setShowTitle(true);
					$collapsed_popup[$group_title]->setContent($renderer->render());

					
				} else { // Render not grouped options and popups
					$normal = $renderer->render();
					$html[] = $normal;
					
					if ( strlen($normal) <= 25 ) {
						$i--;
					} 
					
					$popup_width = $renderer->getPopupWidth();

					if ( $mapper_param->getFilter() == 'Service' ) {
						$popup_width = 1200;						
					}

					$normal_popup = new Cubix_Filter_PopupRenderer(array('title' => $renderer->getTitle(), 'popupId' => 'f_' . $renderer->getMapperParam()->getName(), 'width' => $popup_width, 'height' => $renderer->getPopupHeight()));
					$normal_popup->setScriptPath(self::SCRIPT_PATH);
					$normal_popup->setScriptName('popup');
					
					$normal_popup->setContent($renderer->renderFull());
					
					$popup_html[] = $normal_popup->render();
				}

				$i++;
			}

			foreach ($collapsed_popup as $col_popup) {
				$popup_html[] = $col_popup->render();
			}

			foreach ($grouped_html as $group_title => $gr_html) {
				$g_html = $gr_html->render();
				if ( strlen($g_html) > 30 ) {
					$_grouped_html[$gr_html->getWeight()] = array('title' => $group_title, 'html' => $g_html);
				}
			}
			ksort($_grouped_html);			
		}
		
		return array('full' => $html, 'grouped' => $_grouped_html, 'popup' => $popup_html);
	}
	
	public function renderED()
	{
		$html = array();
				
		if ( count($this->_mapper) ) {
			//$i = 1;
			
			foreach ( $this->_mapper as $mapper_param ) {
				
				$filter = $this->_factory($mapper_param->getFilter());
				$filter->setIsMobileView($this->_is_mobile);
				$filter->setSelected($this->_request->{$mapper_param->getName()});
				$filter->setMapperParam($mapper_param);
				$filter->setExistingFilters($this->getExistingFilters());
				$filter->setFilterDefines($this->getFilterDefines());
				$renderer = $filter->getRenderer();
				$renderer->setScriptPath(self::SCRIPT_PATH);
				$normal = $renderer->render();
				$html[$mapper_param->getFilter()] = $normal;
			}
		}
		
		return array('full' => $html);
	}
	
	public function renderEG()
	{
		$html = array();
		$grouped_html = array();
		$_grouped_html = array();
		$popup_html = array();
		$grouped_popup = array();
		
		if ( count($this->_mapper) ) {
			$i = 1;

			foreach ( $this->_mapper as $mapper_param ) {

				$filter = $this->_factory($mapper_param->getFilter());
				
				$filter->setSelected($this->_request->{$mapper_param->getName()});
				$filter->setMapperParam($mapper_param);
				$filter->setExistingFilters($this->getExistingFilters());
				
				$renderer = $filter->getRenderer();
				$renderer->setScriptPath(self::SCRIPT_PATH);

				$renderer->setVisibleCount(4);

				// Render grouped options and popups
				if ( $mapper_param->getGroup() ) {					

					$group_title = preg_replace("/\s+/", "-", strtolower($mapper_param->getGroup()));

					if ( ! isset($grouped_html[$group_title])) {
						$grouped_html[$group_title] = new Cubix_Filter_GroupRenderer(array('title' => $group_title, 'groupId' => $group_title));
					}
					
					$grouped_html[$group_title]->setScriptPath(self::SCRIPT_PATH);
					$grouped_html[$group_title]->setScriptName('group');
					$grouped_html[$group_title]->setWeight($mapper_param->getGroupWeight());

					$renderer->setShowTitle(true);
					$renderer->setIsCollapsed(true);
					$grouped_html[$group_title]->setContent($renderer->render());
				} else { // Render not grouped options and popups
					$normal = $renderer->render();
					$html[] = $normal;
					
					if ( strlen($normal) <= 25 ) {
						$i--;
					} 
				}
				
				if ( in_array( $mapper_param->getFilter(), array('Height','Weight','EyeColor'))) {
					$group_title = 'physical-appearance-other';
					if ( ! isset($grouped_popup[$group_title])) {
						$grouped_popup[$group_title] = new Cubix_Filter_PopupRenderer(array('title' => $renderer->getTitle(), 'popupId' => 'f_' . $renderer->getMapperParam()->getFilter()));
					}
					
					$grouped_popup[$group_title]->setScriptPath(self::SCRIPT_PATH);
					$grouped_popup[$group_title]->setScriptName('popup');
					$grouped_popup[$group_title]->setIsGroup(true);
					$grouped_popup[$group_title]->setContent($renderer->render());		
				}
				else{
					$normal_popup = new Cubix_Filter_PopupRenderer(array('title' => $renderer->getTitle(), 'popupId' => 'f_' . $renderer->getMapperParam()->getFilter()));
					$normal_popup->setScriptPath(self::SCRIPT_PATH);
					$normal_popup->setScriptName('popup');
					$normal_popup->setContent($renderer->renderFull());


					$popup_html[] = $normal_popup->render();
				}

				$i++;
			}
			
			foreach ($grouped_popup as $gr_popup) {
				$popup_html[] = $gr_popup->render();
			}
			
			foreach ($grouped_html as $group_title => $gr_html) {
				$g_html = $gr_html->render();
				if ( strlen($g_html) > 30 ) {
					$_grouped_html[$gr_html->getWeight()] = array('title' => $group_title, 'html' => $g_html);
				}
			}
			ksort($_grouped_html);
		}
		
		return array('full' => $html, 'grouped' => $_grouped_html, 'popup' => $popup_html);
	}
	
	public function makeQuery()
	{
		$queries = array();

		if ( count($this->_mapper) ) {
			foreach ( $this->_mapper as $mapper_param ) {
				$filter = $this->_factory($mapper_param->getFilter());
				
				$filter->setSelected($this->_request->{$mapper_param->getName()});
				$filter->setMapperParam($mapper_param);
				
				if ( $q = $filter->makeQuery() ) {
					$queries[] = $q;
				}
			}
		}

		return $queries;
	}

    /**
     * @param $filter
     * @return Cubix_Filter_Renderer_Abstract
     * @throws Cubix_FilterV2_Exception
     */
	protected function _factory($filter)
	{		
		$class_name = 'Cubix_Filter_Custom_' . $filter;
		
		if ( class_exists($class_name) ) {
			return new $class_name;
		}
		else {
			throw new Cubix_FilterV2_Exception('Filter class "' . $class_name . '" doesn\'t exist.');
		}
	}
}
