<?php

class Cubix_Images
{
	const ERROR_IMAGE_EXT_NOT_ALLOWED = 1;
	const ERROR_IMAGE_INVALID = 2;
	const ERROR_IMAGE_TOO_SMALL = 3;
	const ERROR_IMAGE_TOO_BIG = 4;
	const ERROR_IMAGE_COUNT_LIMIT_OVER = 5;
	
	/**
	 * @var Cubix_Images_Storage_Ftp
	 */
	protected $_storage;
	
	/**
	 * The host of the current application
	 *
	 * @var string
	 */
	protected $_application;
	
	/**
	 * Configuration from application init
	 *
	 * @var array
	 */
	protected $_config;
	
	public function __construct()
	{
		$this->_config = $config = Zend_Registry::get('images_config');
		$this->_storage = new Cubix_Images_Storage_Ftp($config['remote']['ftp']);
		$this->_application = Cubix_Application::getById(Cubix_Application::getId())->host;
	}
	
	/**
	 * Move temp photos to the remote machine
	 *
	 * @param string $file 
	 * @param string $catalog_id the same as escort id
	 * @return array
	 * @author GuGo
	 */
	public function saveVirtualPhotos($file, $catalog_id, $application_id = null, $ext_override = null, $hash)
	{
		// Create a random file name
		//$hash = md5(microtime(TRUE) * mt_rand(1, 1000000));
		$ext = is_null($ext_override) ? strtolower(@end(explode('.', $file))) : $ext_override;
		
		$remote = $this->_getFullPath(new Cubix_Images_Entry(array(
			'application_id' => $application_id,
			'catalog_id' => $catalog_id,
			'hash' => $hash,
			'ext' => $ext
		)));
		
		if ( ! is_null($application_id) ) {
			$application = Cubix_Application::getById($application_id)->host;
		}
		else {
			$application = $this->_application;
		}
		
		$dirs = array();
		$i = 0;
		$dir = $remote;
		while ( ($dir = dirname($dir)) != '/' . $application ) {
			$i++;
			$dirs[] = $dir;
			if ( $i > 100 ) die('error');
		}
		
		// Connect to ftp
		$this->_storage->connect();
		
		// Create directories if they does not exist
		$dirs = array_reverse($dirs);
		
		foreach ( $dirs as $dir ) {
			try {
				$this->_storage->makeDir($dir);
			} catch (Cubix_Images_Storage_Exception $e) {
				// Probably folder already exists
			}
		}
		
		$file_from = $this->_getFullPath(new Cubix_Images_Entry(array(
			'application_id' => $application_id,
			'catalog_id' => '0000',
			'hash' => $hash,
			'ext' => $ext
		)));
		
		$this->_storage->move($file_from, $remote);
		
		$this->_storage->disconnect();
	}
	
	
	/**
	 * Save the file on the remote machine
	 *
	 * @param string $file 
	 * @param string $catalog_id the same as escort id
	 * @return array
	 * @author GuGo
	 */
	public function save($file, $catalog_id, $application_id = null, $ext_override = null, $user_type_id = null, $hash = null, $no_validation = null)
	{
		
		// Create a random file name
		if ( ! $hash ) {
			$hash = md5(microtime(TRUE) * mt_rand(1, 1000000));
		}
		
		$ext = is_null($ext_override) ? strtolower(@end(explode('.', $file))) : strtolower($ext_override);
		
		$remote = $this->_getFullPath(new Cubix_Images_Entry(array(
			'application_id' => $application_id,
			'catalog_id' => $catalog_id,
			'hash' => $hash,
			'ext' => $ext
		)), $user_type_id);
		
		if ( ! is_null($application_id) ) {
			$application = Cubix_Application::getById($application_id)->host;
		}
		else {
			$application = $this->_application;
		}
		
		$dirs = array();
		$i = 0;
		$dir = $remote;
		
		while ( ($dir = dirname($dir)) != '/' . $application ) {
			$i++;
			$dirs[] = $dir;
			if ( $i > 100 ) die('error');
		}
		
		// Connect to ftp
		$this->_storage->connect();
		
		// Create directories if they does not exist
		$dirs = array_reverse($dirs);

		foreach ( $dirs as $dir ) {
			try {
				$this->_storage->makeDir($dir);
			} catch (Cubix_Images_Storage_Exception $e) {
				// Probably folder already exists
			}
		}
		
		try {
			$is_agency = false;
			if ( $catalog_id == 'agencies' ) {
				$is_agency = true;
			}

			// Resize image on the local server
			
			$file = $this->_resize($file, $ext, $application_id, $is_agency, $no_validation);
			
			// Save the local file on remote storage
			$this->_storage->store($file, $remote);
			$this->_storage->disconnect();
		} catch (Cubix_Images_Storage_Exception $e) {
			var_dump($e->getMessage());die;
			return false;
		}
		
		return array('hash' => $hash, 'ext' => $ext);
	}

	/**
	 * Copy the file on the remote machine
	 *
	 * @param string $file 
	 * @param string $catalog_id the same as escort id
	 * @return status int
	 * @author Onyx
	 */
	//public function copy($file, $catalog_id, $application_id = null, $ext_override = null, $agency_id = null, $hash = null, $no_validation = null)
	public function copy($hash,$ext,$catalog_id,$destCatalog_id, $newHash = null, $agency_id = null, $application_id = null)
	{
		$destHash = $hash;

		//Create a random file name
		if ( $newHash ) {
			$destHash = md5(microtime(TRUE) * mt_rand(1, 1000000));
		}
		
		$from = $this->_getFullPath(new Cubix_Images_Entry(array(
			'application_id' => $application_id,
			'catalog_id' => $catalog_id,
			'hash' => $hash,
			'ext' => $ext
		)), $agency_id);
		
		$to = $this->_getFullPath(new Cubix_Images_Entry(array(
			'application_id' => $application_id,
			'catalog_id' => $destCatalog_id,
			'hash' => $destHash,
			'ext' => $ext
		)), $agency_id);

		if ( ! is_null($application_id) ) {
			$application = Cubix_Application::getById($application_id)->host;
		}
		else {
			$application = $this->_application;
		}
		
		$dirs = array();
		$i = 0;
		$dir = $to;
		
		while ( ($dir = dirname($dir)) != '/' . $application ) {
			$i++;
			$dirs[] = $dir;
			if ( $i > 100 ) die('error');
		}
		
		// Connect to ftp
		$this->_storage->connect();
		
		// Create directories if they does not exist
		$dirs = array_reverse($dirs);
		
		foreach ( $dirs as $dir ) {
			try {
				$this->_storage->makeDir($dir);
			} catch (Cubix_Images_Storage_Exception $e) {
				// Probably folder already exists
			}
		}
		
		try {
			$this->_storage->copy($from, $to);

		} catch (Cubix_Images_Storage_Exception $e) {
			var_dump($e->getMessage());die;
			return false;
		}
		
		return array('hash' => $destHash);
	}
	
	public function remove($entries)
	{
		if ( ! is_array($entries) ) {
			$entries = array($entries);
		}
		
		if ( ! count($entries) ) return;
		
		$remote = $this->_getFullPath( $entries[0] );
		$path = dirname( $remote );
		
		try {
			$this->_storage->connect();
			$list = $this->_storage->getFiles($path);
			
			foreach ( $entries as $entry ) {
				foreach ( $list as $filename ) {
					if ( preg_match('/' . $entry->hash . '(_.+)?\.' . $entry->ext . '/', $filename) ) {
						$res = $this->_storage->delete($path.'/' . $filename);
						if( !$res ){
							return 'failed to remove file: 789456123';
						}
					}
				}
				return true;
			}
			
			$this->_storage->disconnect();
		}
		catch ( Exception $e ) {
			$this->_storage->disconnect();
			return $e;
		}
	}
	
	protected function _getFullPath(Cubix_Images_Entry $entry, $user_type_id = null)
	{
		$application = $this->_application;
		
		if ( isset($entry->application_id) ) {
			$application = Cubix_Application::getById($entry->application_id)->host;
		}

		$catalog = $entry->getCatalogId();
		$parts = array($catalog);

		$a = array();
		if ( is_numeric($catalog) ) {
			$parts = array();
			
			if ( strlen($catalog) > 2 ) {
				$parts[] = substr($catalog, 0, 2);
				$parts[] = substr($catalog, 2);
			}
			else {
				$parts[] = '_';
				$parts[] = $catalog;
			}
		}
		else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
			array_shift($a);
			$catalog = $a[0];
			
			$parts = array();
			
			if ( strlen($catalog) > 2 ) {
				$parts[] = substr($catalog, 0, 2);
				$parts[] = substr($catalog, 2);
			}
			else {
				$parts[] = '_';
				$parts[] = $catalog;
			}
			
			$parts[] = $a[1];
		}
		elseif ($catalog == 'agencies' && !is_null($user_type_id) && intval($user_type_id) > 0)
		{
			$parts = array();
			
			$parts[] = $catalog;
			$parts[] = $user_type_id;
		}
		
		elseif ($catalog == 'members' && !is_null($user_type_id) && intval($user_type_id) > 0)
		{
			$parts = array();
			
			$parts[] = $catalog;
			
			if ( strlen($user_type_id) > 2 ) {
				$parts[] = substr($user_type_id, 0, 2);
				$parts[] = substr($user_type_id, 2);
			}
			else {
				$parts[] = '_';
				$parts[] = $user_type_id;
			}
		}
		
		$catalog = implode('/', $parts);

		$path = '/' . $application . '/' . $catalog . '/' . $entry->getHash() . (isset($entry->size) ? '_' . $entry->getSize() : '') . '.' . $entry->getExt();
		
		return $path;
	}
	
	/**
	 * NO NEED OF THIS FUNC
	 * Downloads image from the server
	 *
	 * @param Cubix_Images_Entry $entry 
	 * @return binary
	 * @author GuGo
	 */
	public function get(Cubix_Images_Entry $entry)
	{
		
	}
	
	/**
	 * Returns url of image on the remote machine
	 * If the application id is not specified, current application will be used
	 *
	 * @param Cubix_Images_Entry $entry 
	 * @return string
	 * @author GuGo
	 */
	public function getUrl(Cubix_Images_Entry $entry)
	{
		return $this->_clusterizeUrl($entry,
			trim($this->_config['remote']['url'], '/') .
			$this->_getFullPath($entry)
		);
	}
	
	public function getAgencyUrl(Cubix_Images_Entry $entry, $agency_id)
	{
		return $this->_clusterizeUrl($entry,
			trim($this->_config['remote']['url'], '/') .
			$this->_getFullPath($entry, $agency_id)
		);
	}

	public function getMemberUrl(Cubix_Images_Entry $entry, $member_id = null)
	{
		
		return $this->_clusterizeUrl($entry,
			trim($this->_config['remote']['url'], '/') .
			$this->_getFullPath($entry, $member_id)
		);
	}
	
	public function getServerUrl(Cubix_Images_Entry $entry)
	{
		return $this->_clusterizeUrl($entry, 
			trim($this->_config['remote']['serverUrl'], '/') .
			$this->_getFullPath($entry)
		);
	}

	/**
	 * If there is a config for clustering image server this function will
	 * append a random number between 1 and specified one in config file
	 * If there is no config the url will not be affected
	 *
	 * @param string $url
	 * @return string The resulting url
	 */
	private function _clusterizeUrl($entry, $url)
	{
		if ( isset($this->_config['remote']['clusterize']) &&
				$this->_config['remote']['clusterize'] &&
				isset($this->_config['remote']['cluster_max']) &&
				($max = $this->_config['remote']['cluster_max']) > 0 ) {
			$url = preg_replace('#^(http://pic)\.#', '${1}' . $this->_decideClusterId($entry) . '.', $url);
		}

		return $url;
	}

	/**
	 * Will calculate the id of cluster for this entry
	 *
	 * @param Cubix_Images_Entry $entry
	 */
	private function _decideClusterId($entry)
	{
		$ascii = 0;
		for ( $i = 0; $i < strlen($entry->hash); ++$i ) $ascii += ord(substr($entry->hash, $i, 1));
		$base = ord('1') * strlen($entry->hash);
		$ascii -= $base;
		$max_ascii = ord('f') * strlen($entry->hash) - $base;
		$percent = $ascii / $max_ascii;
		return round($percent * $this->_config['remote']['cluster_max']);
	}
	
	protected function _resize($file, $ext_override = null, $application_id, $is_agency = false, $no_validation = null)
	{
		if ( ! is_file($file) ) {
			throw new Exception(Cubix_I18n::translate('sys_error_file_does_not_exist', array('file' => $file)));
		}

		// Extract the filename and extension from full file path
		$file_name = basename($file);
		$file_name = explode('.', $file_name);
		$file_ext = is_null($ext_override) ? strtolower(end($file_name)) : $ext_override;
		$file_name = implode('.', array_slice($file_name, 0, count($file_name) - 1));
		
		// Make sure the extension is allowed
		if ( ! in_array($file_ext, $this->_config['allowedExts']) ) {
			throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $this->_config['allowedExts']))), self::ERROR_IMAGE_EXT_NOT_ALLOWED);
		}

		// Make sure that the image is valid and get it's width, height and type
		$img_info = @getimagesize($file);

		list($width, $height, $image_type) = $img_info;

		if ( FALSE === $img_info ) {
            $message = Cubix_I18n::translate('sys_error_upload_img_only_images');
            //if (Cubix_Application::getId() == APP_EF) {
                $err = error_get_last();
                $message .= $file.' ## ' . var_export($err, true);
           // }
			throw new Exception($message, self::ERROR_IMAGE_INVALID);
		}
		
		$max_sidesize = $this->_config['maxImageSize'];
		$min_sidesize = $this->_config['minImageSize'];

		$new_profile_apps = array(APP_A6, APP_EF, APP_BL, APP_6A, APP_6B, APP_6C, APP_AE, APP_A6_AT, APP_EG_CO_UK, APP_EG, APP_EM);
		
		if ( ! $no_validation ) {
		
			if ( in_array($application_id, $new_profile_apps) ) {
				$p_min_width = 400;			
				$p_min_height = 600;			
				$l_min_width = 500;
				$l_min_height = 375;
				if ( $is_agency ) {
					$p_min_width = 179;			
					$p_min_height = 139;			
					$l_min_width = 179;
					$l_min_height = 139;
				}

				if ( $width < $height ) {
					if ( $width < $p_min_width || $height < $p_min_height ) {
						throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_min_a6', array('min_width' => $p_min_width, 'min_height' => $p_min_height, 'ratio' => Cubix_I18n::translate('portrait'))), self::ERROR_IMAGE_TOO_SMALL);
					}
				} else {
					if ( $width < $l_min_width || $height < $l_min_height ) {
						throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_min_a6', array('min_width' => $l_min_width, 'min_height' => $l_min_height, 'ratio' => Cubix_I18n::translate('landscape'))), self::ERROR_IMAGE_TOO_SMALL);
					}
				}
			} else {
				if ( $width < $min_sidesize || $height < $min_sidesize ) {
					throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_min', array('min_sidesize' => $min_sidesize)), self::ERROR_IMAGE_TOO_SMALL);
				}
			}

			/*if ( $width > $max_sidesize || $height > $max_sidesize ) {
				throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_max', array('max_sidesize' => $max_sidesize)), self::ERROR_IMAGE_TOO_BIG);
			}*/
			
		}
		
		// Get original file extension, it's size and quality from config
		$dest_bigside = $this->_config['origBigSide'];
		$dest_ext = $this->_config['destExt'];
		$dest_quality = $this->_config['origQuality'];
		
		// Get the gd function extension from image type constant
		$ext = $this->_gdExtFromImageInfo($image_type);
		$ext = $ext['ext'];
		
		// Load uploaded image
		$func = 'imagecreatefrom' . $ext;
		// 
		ini_set("gd.jpeg_ignore_warning", 1);
		$gd = @$func($file);
		
		if ( FALSE === $gd ) {
            $message = Cubix_I18n::translate('sys_error_upload_img_gd_error');
            if (Cubix_Application::getId() == APP_EF) {
                $err = error_get_last();
                $message .= ' ## ' . var_export($err, true);
            }
			throw new Exception($message);
		}
		
		// Calculate rational image size
		if ( $width < $height ) {
			$dest_width = $dest_bigside;
			$dest_height = $height * ($dest_width / $width);
		}
		else {
			$dest_height = $dest_bigside;
			$dest_width = $width * ($dest_height / $height);
		}
		
		if ( $dest_width > $width ) {
			$dest_width = $width;
		}
		
		if ( $dest_height > $height ) {
			$dest_height = $height;
		}
		
		$dest_width = intval($dest_width);
		$dest_height = intval($dest_height);
		
		// Resize the image
		$dst_gd = @imagecreatetruecolor($dest_width, $dest_height);
		@imagecopyresampled($dst_gd, $gd, 0, 0, 0, 0, $dest_width, $dest_height, $width, $height);
		
		// Get gd extension from destination file extension and save the resized image
		$ext = $this->_gdExtFromFileExt($dest_ext);
		$ext = $ext['ext'];
		$func = 'image' . $ext;
		
		//$dst_file = preg_replace('/\..+$/i', '_resized.' . $dest_ext, $file);
		$dst_file = $file;
		$func($dst_gd, $dst_file, ($ext == 'jpeg') ? $dest_quality : null);
		
		@imagedestroy($gd);
		@imagedestroy($dst_gd);
		
		return $dst_file;
	}
	
	protected function _watermark($file)
	{
		
	}
	
	protected function _gdExtFromFileExt($file_ext)
	{
		switch ($file_ext) {
			case 'jpg':
			case 'jpeg':
				$mime = 'image/jpeg';
				$ext = 'jpeg';
			break;
			case 'png':
				$mime = 'image/png';
				$ext = 'png';
			break;
			case 'gif':
				$mime = 'image/gif';
				$ext = 'gif';
			break;
		}
		
		if ( ! isset($mime) || ! isset($ext) ) {
			throw new Exception(Cubix_I18n::translate('sys_error_unknown_extension', array('extension' => $file_ext)), self::ERROR_IMAGE_EXT_NOT_ALLOWED);
		}
		
		return array('mime' => $mime, 'ext' => $ext);
	}
	
	protected function _gdExtFromImageInfo($image_type)
	{
		switch ($image_type) {
			case IMAGETYPE_JPEG:
				$mime = 'image/jpeg';
				$ext = 'jpeg';
			break;
			case IMAGETYPE_PNG:
				$mime = 'image/png';
				$ext = 'png';
			break;
			case IMAGETYPE_GIF:
				$mime = 'image/gif';
				$ext = 'gif';
			break;
		}
		
		if ( ! isset($mime) || ! isset($ext) ) {
			throw new Exception(Cubix_I18n::translate('sys_error_unknown_image_type', array('image_type' => $image_type)),self::ERROR_IMAGE_EXT_NOT_ALLOWED);
		}
		
		return array('mime' => $mime, 'ext' => $ext);
	}
	
	public function getPathWithSize($path, $size)
	{
		$parts = explode('/', $path);
		$file = array_pop($parts);
		$path = implode('/', $parts);
		$files_parts = explode('.', $file);
		$new_file = $files_parts[0] . '_' . $size . '.' . $files_parts[1];
		$path = $path . '/' . $new_file;
		
		return $path;
	}
	
	public function retreive($entry){
		
		try {
            $video_config =  Zend_Registry::get('videos_config');
            //shared folder for APP_EF only
            $temp_dir_path = null;
            if (!empty($video_config['temp_dir_path']) && is_dir($video_config['temp_dir_path']) && Cubix_Application::getId() == APP_EF){
                $temp_dir_path = $video_config['temp_dir_path'];
            }
			$this->_storage->connect();
			
			$file = $this->_getFullPath($entry);
			
			$tmp_dir = ($temp_dir_path) ? $temp_dir_path : sys_get_temp_dir();
			$unique = uniqid();
			$name = $unique.'.'.$entry->getExt();
			$dir = $tmp_dir.'/escort/';
			if(!is_dir($dir))
				mkdir($dir);

			$local = $dir.$name;
			$this->_storage->retreive($local, $file);
			$this->_storage->disconnect();
		}
		catch ( Exception $e ) {
			$this->_storage->disconnect();
		}
		
		return $local; 
	}
	
}
