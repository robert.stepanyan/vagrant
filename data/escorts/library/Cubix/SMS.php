<?php
/*
Copyright [2011] [tompkins@vadian.net]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

class Cubix_SMS
{

	public $version;
	public $statusCode;
	private $fields;
	private $UserKey;
	private $Password;
	private $ServiceUrl;
	private $Recipients;
	private $Originator;
	private $MessageText;
	private $DeferredDeliveryTime;
	private $FlashingSMS;
	private $TimeZone;
	private $URLBufferedMessageNotification;
	private $URLDeliveryNotification;
	private $URLNonDeliveryNotification;
	private $AffiliateId;

	private $TokenReference;
	private $TokenValidity;
	private $TokenMask;
	private $VerificationCode;
	private $TokenCaseSensitive;
	private $PhoneNumber;

	private $OriginatorUnlockCode;

	private $WapDescription;
	private $WapURL;

	public function __construct($u, $p)
	{
	    $APP_ID = Cubix_Application::getId();
		$this->Version = "09.08.2011";
		$this->UserKey = $u;
		$this->Password = $p;
		$this->ServiceUrl = "https://soap.aspsms.com/aspsmsx2.asmx/";
        if ( in_array($APP_ID, array(APP_EI, APP_BL, APP_EM, APP_EG_CO_UK))){
            $this->ServiceUrl = "http://soap.aspsms.com/aspsmsx2.asmx/";
        }
		$this->Recipients = array();
		$this->Originator = "";
		$this->MessageText = "";
		$this->DeferredDeliveryTime = "";
		$this->FlashingSMS = "";
		$this->TimeZone = "";
		$this->URLBufferedMessageNotification = "";
		$this->URLDeliveryNotification = "";
		$this->URLNonDeliveryNotification = "";
		$this->AffiliateId = "";

		$this->OriginatorUnlockCode = "";

		$this->TokenReference = "";
		$this->TokenValidity = "";
		$this->TokenMask = "";
		$this->VerificationCode = "";
		$this->TokenCaseSensitive = "";
		$this->PhoneNumber = "";

		$this->WapDescription = "";
		$this->WapURL = "";

	}

	private function ExecutePost($Action)
	{
        $fields_string= '';
		//url-ify the data for the POST
		foreach ($this->fields as $key => $value)
		{
			$fields_string .= $key . '=' . $value . '&';
		}
        $fields_string = rtrim($fields_string, '&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $this->ServiceUrl . $Action);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, count($this->fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		//execute post
		$result = curl_exec($ch);
		// Check the return value of curl_exec(), too
		if ($result === false) {
			var_dump(curl_error($ch), curl_errno($ch));die;
		}
		//close connection
		curl_close($ch);

		//parse results
		$doc = new DOMDocument();
		$doc->loadXML($result);
		return $this->parseResponse($doc->firstChild->textContent);

	}

	private function parseResponse($response){
	    $responseArr = explode(":", $response);
	    $this->statusCode = $responseArr[1];
        return $this->statusCode;
    }

	public function SendSMS()
	{

		foreach ($this->Recipients as $tracknr => $number) {
            // according to the docs multiple recipients must look like this: <NUMBER>:<TRACKNR>;<NUMBER>:<TRACKNR>
            $recipientList[] = "$number:$tracknr";
        }
		//set parameters
		$this->fields = array(
				'UserKey' => urlencode($this->UserKey),
				'Password' => urlencode($this->Password),
				'Recipients' => urlencode(implode(";", $recipientList)),
				'Originator' => urlencode($this->Originator),
				'MessageText' => urlencode($this->MessageText),
				'DeferredDeliveryTime' => urlencode($this->DeferredDeliveryTime),
				'FlashingSMS' => urlencode($this->FlashingSMS),
				'TimeZone' => urlencode($this->TimeZone),
				'URLBufferedMessageNotification' => urlencode($this->URLBufferedMessageNotification),
				'URLDeliveryNotification' => urlencode($this->URLDeliveryNotification),
				'URLNonDeliveryNotification' => urlencode($this->URLNonDeliveryNotification),
				'AffiliateId' => urlencode($this->AffiliateId),
		);
		//print_r($this->fields);die;

		return $this->ExecutePost("SendTextSMS");

	}

	public function SimpleWAPPush()
	{

		//set parameters
		$this->fields = array(
				'UserKey' => $this->UserKey,
				'Password' => $this->Password,
				'Recipients' => $this->Recipients,
				'Originator' => $this->Originator,
				'WapDescription' => utf8_encode($this->WapDescription),
				'WapURL' => utf8_encode($this->WapURL),
				'DeferredDeliveryTime' => $this->DeferredDeliveryTime,
				'FlashingSMS' => $this->FlashingSMS,
				'TimeZone' => $this->TimeZone,
				'URLBufferedMessageNotification' => $this->URLBufferedMessageNotification,
				'URLDeliveryNotification' => $this->URLDeliveryNotification,
				'URLNonDeliveryNotification' => $this->URLNonDeliveryNotification,
				'AffiliateId' => $this->AffiliateId,
		);
		//print_r($this->fields);

		return $this->ExecutePost("SimpleWAPPush");

	}

	public function SendTokenSMS()
	{

		foreach ($this->Recipients as $tracknr => $number) {
            // according to the docs multiple recipients must look like this: <NUMBER>:<TRACKNR>;<NUMBER>:<TRACKNR>
            $recipientList[] = "$number:$tracknr";
        }
        		
		//set parameters
		$this->fields = array(
				'UserKey' => urlencode($this->UserKey),
				'Password' => urlencode($this->Password),
				'Recipients' => urlencode(implode(";", $recipientList)),
				'Originator' => urlencode($this->Originator),
				'MessageData' => urlencode(utf8_encode($this->MessageText)),
				'TokenReference' => urlencode(utf8_encode($this->TokenReference)),
				'TokenValidity' => urlencode($this->TokenValidity),
				'TokenMask' => urlencode($this->TokenMask),
				'VerificationCode' => urlencode($this->VerificationCode),
				'TokenCaseSensitive' => urlencode($this->TokenCaseSensitive),
				'URLBufferedMessageNotification' => urlencode($this->URLBufferedMessageNotification),
				'URLDeliveryNotification' => urlencode($this->URLDeliveryNotification),
				'URLNonDeliveryNotification' => urlencode($this->URLNonDeliveryNotification),
				'AffiliateId' => urlencode($this->AffiliateId),
		);
		//print_r($this->fields);

		return $this->ExecutePost("SendTokenSMS");

	}

	public function VerifyToken()
	{

		//set parameters
		$this->fields = array(
				'UserKey' => urlencode($this->UserKey),
				'Password' => urlencode($this->Password),
				'PhoneNumber' => urlencode($this->PhoneNumber),
				'TokenReference' => urlencode(utf8_encode($this->TokenReference)),
				'VerificationCode' => urlencode($this->VerificationCode),
		);
		//print_r($this->fields);

		return $this->ExecutePost("VerifyToken");

	}

	public function CheckCredits()
	{

		//set parameters
		$this->fields = array(
				'UserKey' => urlencode($this->UserKey),
				'Password' => urlencode($this->Password),
		);
		return $this->ExecutePost("CheckCredits");
	}

	public function CheckOriginatorAuthorization()
	{

		//set parameters
		$this->fields = array(
				'UserKey' => urlencode($this->UserKey),
				'Password' => urlencode($this->Password),
				'Originator' => urlencode($this->Originator),
		);
		return $this->ExecutePost("CheckOriginatorAuthorization");
	}

	public function SendOriginatorUnlockCode()
	{

		//set parameters
		$this->fields = array(
				'UserKey' => urlencode($this->UserKey),
				'Password' => urlencode($this->Password),
				'Originator' => urlencode($this->Originator),
		);
		return $this->ExecutePost("SendOriginatorUnlockCode");
	}

	public function UnlockOriginator()
	{

		//set parameters
		$this->fields = array(
				'UserKey' => urlencode($this->UserKey),
				'Password' => urlencode($this->Password),
				'Originator' => urlencode($this->Originator),
				'OriginatorUnlockCode' => urlencode($this->OriginatorUnlockCode),
		);
		return $this->ExecutePost("UnlockOriginator");
	}

	public function InquireDeliveryNotifications($trn)
	{

		if ($trn != null)
		{
			$this->TransactionReferenceNumbers = $trn;
		}
		//set parameters
		$this->fields = array(
				'UserKey' => urlencode($this->UserKey),
				'Password' => urlencode($this->Password),
				'TransactionReferenceNumbers' => urlencode($this->TransactionReferenceNumbers),
		);

		return $this->ExecutePost("InquireDeliveryNotifications");
	}

	public function GetStatusCodeDescription($StatusCode)
	{
		//set parameters
		$this->fields = array(
				'StatusCode' => urlencode($StatusCode),
		);

		return $this->ExecutePost("GetStatusCodeDescription");
	}
	
	public function setOriginator($o)
  	{
    	$this->Originator = $o;
  	}
	
	public function setBufferedNotificationURL($url)
  	{
    	$this->URLBufferedMessageNotification = $url;
  	}

	public function setDeliveryNotificationURL($url)
  	{
    	$this->URLDeliveryNotification = $url;
	}

	public function setNonDeliveryNotificationURL($url)
  	{
		$this->URLNonDeliveryNotification = $url;
		
  	}

	public function addRecipient($r, $id = null)
  	{
    	$this->Recipients[$id] = $r;
	}
	
	public function setRecipient($r, $id = null)
	{
		$this->Recipients = array();
		$this->Recipients[$id] = $r;
	}
	
	/*private function _umlautFilter(&$string)
	{
		$ar = array("ä"=>"&#228;", "ö"=>"&#246;", "ü"=>"&#252;", "ß"=>"&#223;",
			"Ä"=>"&#196;", "Ö"=>"&#214;", "Ü"=>"&#220;", "À"=>"&#192;",
			"È"=>"&#200;", "É"=>"&#201;", "Ê"=>"&#202;", "è"=>"&#232;",
			"é"=>"&#233;", "ê"=>"&#234;", "à"=>"&#224;", "ï"=>"&#239;",
			"ù"=>"&#249;", "û"=>"&#251;", "ü"=>"&#252;", "ç"=>"&#231;",
			"Æ"=>"&#198;", "?"=>"&#330;", ""=>"&#338;", ""=>"&#339;",
			""=>"&#8364;","«"=>"&#171;", "»"=>"&#187;"
		);

		foreach($ar as $key=>$val)
		{
			$string = str_replace( $key, $val, $string );
		}
	}*/

	public function setContent($content)
  	{
		if(strlen($content) === 0){
			$this->_sendAlarm();
		}
		//$this->_umlautFilter($content);
		$this->MessageText = $content;
	}
	
	private function _sendAlarm()
	{
		$err_body = Cubix_Utils::getDebugBacktrace();
		Cubix_Email::send('badalyano@gmail.com', 'EMPTY SMS SEND', $err_body);
		return;
	}		
}