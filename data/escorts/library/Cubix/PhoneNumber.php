<?php


class Cubix_PhoneNumber
{

    const NO_MATCH    = 'The input does not match a phone number format';
    const UNSUPPORTED = 'The country provided is currently unsupported';
    const INVALID     = 'Invalid type given. String expected';



    /**
     * Phone Number Patterns
     *
     * @var array
     */
    protected static $phone = [];

    /**
     * ISO 3611 Country Code
     *
     * @var string
     */
    protected static $country;

    /**
     * Allow Possible Matches
     *
     * @var bool
     */
    protected static $allowPossible = false;

    /**
     * Allowed Types
     *
     * @var string[]
     */
    protected static $allowedTypes = [
        'general',
        'fixed',
        'tollfree',
        'personal',
        'mobile',
        'voip',
        'uan',
    ];


    /**
     * Load Pattern
     *
     * @param  string        $code
     * @return array[]|false
     */
    protected static function loadPattern($code)
    {
        if (! isset(static::$phone[$code])) {
            if (! preg_match('/^[A-Z]{2}$/D', $code)) {
                return false;
            }

            $file = __DIR__ . '/PhoneNumber/' . $code . '.php';
            if (! file_exists($file)) {
                return false;
            }

            static::$phone[$code] = include $file;
        }

        return static::$phone[$code];
    }


    /**
     * Returns true if and only if $value matches phone number format
     *
     * @param  string|null $value
     * @param  array|null  $context
     * @return bool
     */
    public static function isValid($value = null, $country, $context = null)
    {
        if (! is_scalar($value)) {
            return self::INVALID;
        }

        if (! $countryPattern = self::loadPattern(strtoupper($country))) {
            if (isset($context[$country])) {
                $country = $context[$country];
            }

            if (! $countryPattern = self::loadPattern(strtoupper($country))) {
                return self::UNSUPPORTED;
            }
        }

        $codeLength = strlen($countryPattern['code']);

        /*
         * Check for existence of either:
         *   1) E.123/E.164 international prefix
         *   2) International double-O prefix
         *   3) Bare country prefix
         */
        if (0 === strpos($value, '+' . $countryPattern['code'])) {
            $valueNoCountry = substr($value, $codeLength + 1);
        } elseif (0 === strpos($value, '00' . $countryPattern['code'])) {
            $valueNoCountry = substr($value, $codeLength + 2);
        } elseif (0 === strpos($value, $countryPattern['code'])) {
            $valueNoCountry = substr($value, $codeLength);
        }

        // check against allowed types strict match:
        foreach ($countryPattern['patterns']['national'] as $type => $pattern) {
            if (in_array($type, self::$allowedTypes, true)) {
                // check pattern:
                if (preg_match($pattern, $value)) {
                    return true;
                }

                if (isset($valueNoCountry) && preg_match($pattern, $valueNoCountry)) {
                    // this handles conditions where the country code and prefix are the same
                    return true;
                }
            }
        }

        return self::NO_MATCH;
    }

}