<?php

class Cubix_EscortCopier extends Cubix_Model
{
	static public function getSupportedApplications()
	{
		$applications = array(
			1 => (object) array(
				'id' => 1,
				'title' => 'escortforumit.xxx',
				'backend' => 'https://backend.escortforumit.xxx',
				'username' => 'copy-api',
				'password' => 'dfjg&*Tghsdbgfff565ghh',
				'sync_username'	=> 'sync-manager',
				'sync_password' => 'dfg89U&hnsfjgdfgfgfg'
			),			
			5 => (object) array(
				'id' => 5,
				'title' => 'escortguide.co.uk',
				'backend' => 'https://backend.escortguide.co.uk',
				'username' => 'copy-api-uk',
				'password' => 'dfjg&*Tghsdbgfff565ghh',
				'sync_username'	=> 'sync-manager',
				'sync_password' => 'dfg89U&hnsfjgdfgfgfg'
			),
			7 => (object) array(
				'id' => 7,
				'title' => 'escortguide.com',
				'backend' => 'http://backend.escortguide.com',
				'username' => 'copy-api',
				'password' => 'dfjg&*Tghsdbgfff565ghh',
				'sync_username'	=> 'sync-manager',
				'sync_password' => 'dfg89U&hnsfjgdfgfgfg'
			),
			9 => (object) array(
				'id' => 9,
				'title' => 'escortitalia.com',
				'backend' => 'http://backend.escortitalia.com',
				'username' => 'copy-api',
				'password' => 'dfjg&*Tghsdbgfff565ghh',
				'sync_username'	=> 'sync-manager',
				'sync_password' => 'dfg89U&hnsfjgdfgfgfg'
			),
			11 => (object) array(
				'id' => 11,
				'title' => 'asianescorts.com',
				'backend' => 'http://backoffice.asianescorts.com',
				'username' => 'copy-api',
				'password' => 'dfjg&*Tghsdbgfff565ghh',
				'sync_username'	=> 'sync-manager',
				'sync_password' => 'dfg89U&hnsfjgdfgfgfg'
			),
			16 => (object) array(
				'id' => 16,
				'title' => 'and6.ch',
				'backend' => 'http://backend.and6.ch',
				'username' => 'copy-api',
				'password' => 'dfjg&*Tghsdbgfff565ghh',
				'sync_username'	=> 'sync-manager',
				'sync_password' => 'dfg89U&hnsfjgdfgfgfg'
			),
			17 => (object) array(
				'id' => 17,
				'title' => '6anuncio.com',
				'backend' => 'http://backend.6anuncio.com',
				'username' => 'copy-api',
				'password' => 'dfjg&*Tghsdbgfff565ghh',
				'sync_username'	=> 'sync-manager',
				'sync_password' => 'dfg89U&hnsfjgdfgfgfg'
			),
			18 => (object) array(
				'id' => 18,
				'title' => '6classifieds.com',
				'backend' => 'http://backend.6classifieds.com',
				'username' => 'copy-api',
				'password' => 'dfjg&*Tghsdbgfff565ghh',
				'sync_username'	=> 'sync-manager',
				'sync_password' => 'dfg89U&hnsfjgdfgfgfg'
			),
			25 => (object) array(
				'id' => 25,
				'title' => 'and6.at',
				'backend' => 'http://backend.and6.at',
				'username' => 'copy-api',
				'password' => 'dfjg&*Tghsdbgfff565ghh',
				'sync_username'	=> 'sync-manager',
				'sync_password' => 'dfg89U&hnsfjgdfgfgfg'
			),
			30 => (object) array(
				'id' => 30,
				'title' => 'beneluxxx.com',
				'backend' => 'https://backend.beneluxxx.com',
				'username' => 'copy-api',
				'password' => 'dfjg&*Tghsdbgfff565ghh',
				'sync_username'	=> 'sync-manager',
				'sync_password' => 'dfg89U&hnsfjgdfgfgfg'
			),
			33 => (object) array(
				'id' => 33,
				'title' => 'escortmeetings.com',
				'backend' => 'http://backend.escortmeetings.com',
				'username' => 'copy-api',
				'password' => 'dfjg&*Tghsdbgfff565ghh',
				'sync_username'	=> 'sync-manager',
				'sync_password' => 'dfg89U&hnsfjgdfgfgfg'
			),
			69 => (object) array(
				'id' => 69,
				'title' => 'escortdirectory.com',
				'backend' => 'https://backend.escortdirectory.com',
				'username' => 'copy-api',
				'password' => 'dfjg&*Tghsdbgfff565ghh',
				'sync_username'	=> 'sync-manager',
				'sync_password' => 'dfg89U&hnsfjgdfgfgfg'
			)
		);
		return $applications;
	}

	static public function getTargetApplications()
	{
		$applications = self::getSupportedApplications();

		unset($applications[Cubix_Application::getId()]);

		return $applications;
	}
}

