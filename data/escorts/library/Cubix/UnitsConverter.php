<?php

class Cubix_UnitsConverter
{
	static public function convert($value, $from = 'lbs', $to = 'kg')
	{
		// Height converter
		if ( $from == 'ftin' && $to == 'cm' ) {
			$rx = '/^([0-9]+)(\s*\'\s*([0-9]+)\s*(\"|\'\')?\s*)?$/';
			if ( ! preg_match($rx, $value, $a) )
					return null;

			$feet = intval($a[1]);
			$inch = intval($a[3]);
			//return intval(($feet * 0.3) * 100 + ($inch * 2.54) / 10);
			return intval($feet * 30.48 + $inch * 2.54);
			/*$tmp =  ($feet * 12) + 3;
			return intval($tmp * 2.54);*/
		}
		elseif ( $from == 'cm' && $to == 'ftin' ) {
			$value = intval($value);
			$inch = $value / 2.54;
			$feet = intval($inch / 12);
			$inch -= $feet * 12;
			$inch = round($inch);
			if ( $inch > 11 ) {
				$feet += ($inch - 11);
				$inch = 0;
			}

			return $feet . '\' ' . $inch . '"';
		}

		// Weight converter
		if ( $from == 'lbs' && $to == 'kg' ) {
			return $value * 0.45359237;
		}
		elseif ( $from == 'kg' && $to == 'lbs' ) {
			return $value / 0.45359237;
		}
	}
	
	static public function filterQueryConvert($query, $from = 'lbs', $to = 'kg')
	{
		$vars = array();
		$patterns = array();
		$replacements = array();
		preg_match_all('/=\s+(\d+)([\s|\)])/',$query, $vars);
		
		foreach($vars[1] as $key => $var){
			$patterns[$key] = "/=\s+".$var."[\s|\)]/";
			$replacements[$key] =  "= ".self::convert($var, $from, $to);
		}

		foreach($vars[2] as $key => $var){
			$replacements[$key] .= $var;
		}
		return  preg_replace($patterns, $replacements, $query);
	}
}
