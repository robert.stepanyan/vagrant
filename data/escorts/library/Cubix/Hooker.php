<?php

class Cubix_Hooker
{
	protected $_hooks = array();

	public function notify($name, array $args = array())
	{
		$method = $this->_getHookMethod($name);

		foreach ( $this->_hooks as $hook ) {
			$func = array($hook, $method);
			
			if ( is_callable($func) ) {
				call_user_func_array($func, $args);
			}
		}
	}

	public function addHook(Cubix_Hooker_Hook $hook)
	{
		$hook->setHooker($this);
		$this->_hooks[] = $hook;
	}

	protected function _getHookMethod($name)
	{
		$method = str_replace('_', ' ', $name);
		$method = ucwords($method);
		$method = 'on' . str_replace(' ', '', $method);

		return $method;
	}
}
