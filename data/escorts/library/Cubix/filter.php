<?php

$services = require_once('services.php');

function ___clean_str($str)
{
	$str = strtolower($str);
	$str = str_replace(array(' ', '_'), '-', $str);
	while ( FALSE !== @strpos('--', $str) ) {
		$str = str_replace('--', '-', $str);
	}
	
	$str = str_replace(array('(', ')'), '', $str);
	
	return $str;
}

function clearServiceTitle($str)
{
	$str = strtolower($str);

	$str = preg_replace("#[^0-9a-z]#", '-', $str);

	$str = preg_replace("#\s+#", '-', $str);

	$str = ___clean_str($str);

	return $str;
}

//SERVICE OPTIONS
$service_options = array();

if ( count($services['common']) ) {
	foreach( $services['common'] as $i => $service ) {
		$service_options[] = array(
			'title' => $service,
			'value' => 'svc-' . clearServiceTitle($service),
			'internal_value' => $i
		);
	}
}
//print_r($service_options);
/*$service_options[] = array(
	'title' => Cubix_I18n::translate('kissing'),
	'value' => 'svc-kissing',
	'internal_value' => KISSING
);

$service_options[] = array(
	'title' => Cubix_I18n::translate('kissing_with_tongue'),
	'value' => 'svc-kissing-with-tongue',
	'internal_value' => KISSING_WITH_TONGUE
);

$service_options[] = array(
	'title' => Cubix_I18n::translate('blowjob_with_condom'),
	'value' => 'svc-blowjob-with-condom',
	'internal_value' => BLOWJOB_WITH_CONDOM
);

$service_options[] = array(
	'title' => Cubix_I18n::translate('blowjob_without_condom'),
	'value' => 'svc-blowjob-without-condom',
	'internal_value' => BLOWJOB_WITHOUT_CONDOM
);

$service_options[] = array(
	'title' => Cubix_I18n::translate('cumshot_in_face'),
	'value' => 'svc-cumshot-in-face',
	'internal_value' => CUMSHOT_IN_FACE
);

$service_options[] = array(
	'title' => Cubix_I18n::translate('cumshot_in_mouth_swallow'),
	'value' => 'svc-cumshot-in-mouth-swallow',
	'internal_value' => CUMSHOT_IN_MOUTH_SWALLOW
);

$service_options[] = array(
	'title' => Cubix_I18n::translate('cumshot_in_mouth_spit'),
	'value' => 'svc-cumshot-in-mouth-spit',
	'internal_value' => CUMSHOT_IN_MOUTH_SPIT
);*/
//SERVICE OPTIONS


// AGE OPTIONS
$age_options = array();

$age_options[] = array(
	'title' => '18-20 ' . Cubix_I18n::translate('years'),
	'value' => 'age_18_20'
);

$age_options[] = array(
	'title' => '21-24 ' . Cubix_I18n::translate('years'),
	'value' => 'age_21_24'
);

$age_options[] = array(
	'title' => '25-29 ' . Cubix_I18n::translate('years'),
	'value' => 'age_25_29'
);

$age_options[] = array(
	'title' => '30-35 ' . Cubix_I18n::translate('years'),
	'value' => 'age_30_35'
);

$age_options[] = array(
	'title' => '36-40 ' . Cubix_I18n::translate('years'),
	'value' => 'age_36_40'
);

$age_options[] = array(
	'title' => '41-45 ' . Cubix_I18n::translate('years'),
	'value' => 'age_41_45'
);

$age_options[] = array(
	'title' => '46-49 ' . Cubix_I18n::translate('years'),
	'value' => 'age_46_49'
);

$age_options[] = array(
	'title' => Cubix_I18n::translate('more_than_years', array('NUMBER' => 50)),
	'value' => 'age_50_120'
);

// ETHNIC OPTIONS
$ethnic_options = array();

foreach ( $DEFINITIONS['ethnic_options'] as $id => $ethnic ) {
	$ethnic_options[] = array(
		'title' => $ethnic,
		'value' => 'ethnic_' . ___clean_str($ethnic),
		'internal_value' => $id
	);
}

// ROYAL HEIGHT OPTIONS
$height_options = array();

$height_options[] = array(
	'title' => Cubix_I18n::translate('less_than', array('NUMBER' => '5\' 3')),
	'value' => 'height__160'
);

$height_options[] = array(
	'title' => '5\' 3" - 5\' 5"',
	'value' => 'height_160_165'
);

$height_options[] = array(
	'title' => '5\' 5" - 5\' 7"',
	'value' => 'height_166_170'
);

$height_options[] = array(
	'title' => '5\' 7" - 5\' 9"',
	'value' => 'height_171_175'
);

$height_options[] = array(
	'title' => '5\' 9" - 5\' 11"',
	'value' => 'height_176_180'
);

$height_options[] = array(
	'title' => '5\' 11" - 6\' 1"',
	'value' => 'height_181_185'
);

$height_options[] = array(
	'title' => Cubix_I18n::translate('more_than', array('NUMBER' => '6\' 1')),
	'value' => 'height_185_350'
);

// METRIC HEIGHT OPTIONS
$m_height_options = array();

$m_height_options[] = array(
	'title' => Cubix_I18n::translate('less_than', array('NUMBER' => '160')) . ' ' . Cubix_I18n::translate('cm'),
	'value' => 'height__160'
);

$m_height_options[] = array(
	'title' => '160 - 165 ' . Cubix_I18n::translate('cm'),
	'value' => 'height_160_165'
);

$m_height_options[] = array(
	'title' => '166 - 170 ' . Cubix_I18n::translate('cm'),
	'value' => 'height_166_170'
);

$m_height_options[] = array(
	'title' => '171 - 175 ' . Cubix_I18n::translate('cm'),
	'value' => 'height_171_175'
);

$m_height_options[] = array(
	'title' => '176 - 180 ' . Cubix_I18n::translate('cm'),
	'value' => 'height_176_180'
);

$m_height_options[] = array(
	'title' => '181 - 185 ' . Cubix_I18n::translate('cm'),
	'value' => 'height_181_185'
);

$m_height_options[] = array(
	'title' => Cubix_I18n::translate('more_than', array('NUMBER' => '185')) . ' ' . Cubix_I18n::translate('cm'),
	'value' => 'height_185_350'
);

//ROYAL WEIGHT OPTIONS
$weight_options = array();

$weight_options[] = array(
	'title' => Cubix_I18n::translate('less_than', array('NUMBER' => 100)) . ' ' . Cubix_I18n::translate('lbs'),
	'value' => 'weight__100'
);

$weight_options[] = array(
	'title' => '100-110 ' . Cubix_I18n::translate('lbs'),
	'value' => 'weight_100_110'
);

$weight_options[] = array(
	'title' => '110-120 ' . Cubix_I18n::translate('lbs'),
	'value' => 'weight_110_120'
);

$weight_options[] = array(
	'title' => '120-130 ' . Cubix_I18n::translate('lbs'),
	'value' => 'weight_120_130'
);

$weight_options[] = array(
	'title' => '130-140 ' . Cubix_I18n::translate('lbs'),
	'value' => 'weight_130_140'
);

$weight_options[] = array(
	'title' => '140-150 ' . Cubix_I18n::translate('lbs'),
	'value' => 'weight_140_150'
);

$weight_options[] = array(
	'title' => Cubix_I18n::translate('more_than', array('NUMBER' => 150)) . ' ' . Cubix_I18n::translate('lbs'),
	'value' => 'weight_150_250'
);

//METRIC WEIGHT OPTIONS
$m_weight_options = array();

/*$m_weight_options[] = array(
	'title' => Cubix_I18n::translate('less_than', array('NUMBER' => 100)) . ' ' . Cubix_I18n::translate('lbs'),
	'value' => 'weight__100'
);*/

$m_weight_options[] = array(
	'title' => '41-45 ' . Cubix_I18n::translate('kg'),
	'value' => 'weight_41_45'
);

$m_weight_options[] = array(
	'title' => '46-50 ' . Cubix_I18n::translate('kg'),
	'value' => 'weight_46_50'
);

$m_weight_options[] = array(
	'title' => '51-55 ' . Cubix_I18n::translate('kg'),
	'value' => 'weight_51_55'
);

$m_weight_options[] = array(
	'title' => '56-60 ' . Cubix_I18n::translate('kg'),
	'value' => 'weight_56_60'
);

$m_weight_options[] = array(
	'title' => '61-65 ' . Cubix_I18n::translate('kg'),
	'value' => 'weight_61_65'
);

$m_weight_options[] = array(
	'title' => '66-70 ' . Cubix_I18n::translate('kg'),
	'value' => 'weight_66_70'
);

$m_weight_options[] = array(
	'title' => '71-75 ' . Cubix_I18n::translate('kg'),
	'value' => 'weight_71_75'
);

$m_weight_options[] = array(
	'title' => Cubix_I18n::translate('more_than', array('NUMBER' => 76)) . ' ' . Cubix_I18n::translate('kg'),
	'value' => 'weight_76_250'
);

// BREAST SIZE OPTIONS
$breast_size_options = array();

foreach ( $DEFINITIONS['breast_size_options'] as $i => $breast_size ) {
	$breast_size_options[] = array(
		'title' => $breast_size,
		'value' => 'cup-size_' . $breast_size,
		'internal_value' => $breast_size
	);
}

// HAIR COLOR OPTIONS
$hair_color_options = array();

foreach ( $DEFINITIONS['hair_color_options'] as $color => $hair_color ) {
	$hair_color_options[] = array(
		'title' => $hair_color,
		'value' => 'hair-color_' . ___clean_str($hair_color),
		'internal_value' => $color
	);
}

// HAIR LENGTH OPTIONS
$hair_length_options = array();

foreach ( $DEFINITIONS['hair_length_options'] as $length => $hair_length ) {
	$hair_length_options[] = array(
		'title' => $hair_length,
		'value' => 'hair-length_' . ___clean_str($hair_length),
		'internal_value' => $length
	);
}

// EYE COLOR OPTIONS
$eye_color_options = array();

foreach ( $DEFINITIONS['eye_color_options'] as $color => $eye_color ) {
	$eye_color_options[] = array(
		'title' => $eye_color,
		'value' => 'eye-color_' . ___clean_str($eye_color),
		'internal_value' => $color
	);
}

/*
// DRESS SIZE OPTIONS
$dress_size_options = array();

for ( $i = 2; $i <= 20; $i += 2 ) {
	$dress_size_options[] = array(
		'title' => $i,
		'value' => 'dress-size_' . $i,
		'internal_value' => $i
	);
}

// SHOE SIZE OPTIONS
$shoe_size_options = array();

for ( $i = 5.0; $i <= 10.0; $i += 0.5 ) {
	$shoe_size_options[] = array(
		'title' => $i,
		'value' => 'shoe-size_' . $i,
		'internal_value' => $i
	);
}
*/

// AVAILABLE FOR OPTIONS
$available_for_options = array();

$available_for_options[] = array(
	'title' => Cubix_I18n::translate('outcall'),
	'value' => 'available-for-outcall',
	'internal_value' => array()
);

$available_for_options[] = array(
	'title' => Cubix_I18n::translate('incall'),
	'value' => 'available-for-incall',
	'internal_value' => array()
);

// SERVICE FOR OPTIONS
$service_for_options = array();

foreach ( $DEFINITIONS['sex_availability_options'] as $id => $opt ) {
	$service_for_options[] = array(
		'title' => $opt,
		'value' => 'service-for_' . ___clean_str($id),
		'internal_value' => $id,
	);
}

// SMOKER OPTIONS
$smoker_options = array();

$smoker_options[] = array(
	'title' => Cubix_I18n::translate('yes'),
	'value' => 'smoker_yes',
	'internal_value' => 1,
);

$smoker_options[] = array(
	'title' => Cubix_I18n::translate('no'),
	'value' => 'smoker_no',
	'internal_value' => 2
);

$smoker_options[] = array(
	'title' => Cubix_I18n::translate('ocasionally'),
	'value' => 'smoker_ocasionally',
	'internal_value' => 3
);

// LANGUAGE OPTIONS
$language_options = array();

foreach ( $DEFINITIONS['language_options'] as $id => $lang ) {
	$language_options[] = array(
		'title' => $lang,
		'value' => 'language_' . ___clean_str($id),
		'internal_value' => $id,
	);
}

if ( Zend_Registry::isRegistered('escorts_config') ) {
	$conf = Zend_Registry::get('escorts_config');
}

if ( $conf['useMetricSystem'] )
{
	$height = $m_height_options;
	$weight = $m_weight_options;
}
else
{
	$height = $height_options;
	$weight = $weight_options;
}

$filter = array(
	array(
		'title' => 'All',
		'childs' => NULL
	),
	/*SERVICES*/
	array(
		'title' => Cubix_I18n::translate('services'),
		'childs' => $service_options
	),
	/*SERVICES*/
	array(
		'title' => Cubix_I18n::translate('age'),
		'childs' => $age_options
	),
	array(
		'title' => Cubix_I18n::translate('ethnic'),
		'childs' => $ethnic_options
	),
	array(
		'title' => Cubix_I18n::translate('height'),
		'childs' => $height
	),
	array(
		'title' => Cubix_I18n::translate('weight'),
		'childs' => $weight
	),
	array(
		'title' => Cubix_I18n::translate('cup_size'),
		'childs' => $breast_size_options
	),
	array(
		'title' => Cubix_I18n::translate('hair_color'),
		'childs' => $hair_color_options
	),
	array(
		'title' => Cubix_I18n::translate('hair_length'),
		'childs' => $hair_length_options
	),
	array(
		'title' => Cubix_I18n::translate('eye_color'),
		'childs' => $eye_color_options
	),
	/*array(
		'title' => Cubix_I18n::translate('dress_size'),
		'childs' => $dress_size_options
	),
	array(
		'title' => Cubix_I18n::translate('shoe_size'),
		'childs' => $shoe_size_options
	),*/
	array(
		'title' => Cubix_I18n::translate('available_for'),
		'childs' => $available_for_options
	),
	array(
		'title' => Cubix_I18n::translate('service_for'),
		'childs' => $service_for_options
	),
	array(
		'title' => Cubix_I18n::translate('smoker'),
		'childs' => $smoker_options
	),
	array(
		'title' => Cubix_I18n::translate('language'),
		'childs' => $language_options
	),
	array(
		'title' => Cubix_I18n::translate('now_open'),
		'value' => 'now-open'
	)
);

if ( Cubix_Application::getId() == 9 ) {
	unset($filter[1]);
}

if ( Cubix_Application::getId() == 16 ) {
	$filter[0]['title'] = Cubix_I18n::translate('all_filter');
}

return $filter;
