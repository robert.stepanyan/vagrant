<?php

class Cubix_Debug
{
	protected static $_debug = false;

	protected static $_logFile = './debug.log';

	public static function setDebug($flag)
	{
		self::$_debug = $flag;
	}

	public static function isDebug()
	{
		return self::$_debug;
	}

	public static function log($message, $type = 'INFO')
	{
		$message = '[' . $type . ' ' . date('d.m.y H:i:s') . '] @' . gethostname() . ' ' . $message . "\n";
		
		file_put_contents(self::$_logFile, $message, FILE_APPEND);
	}

	public static function logException(Exception $e)
	{
		$log_file = '../logs/exceptions.log';
		$message = str_repeat('-', 80) . "\n";
		$message .= "Date: " . date('d.m.Y H:i:s') . "\n";
		$message .= $e->__toString();
		$message .= "\n" . str_repeat('-', 80) . "\n";
		file_put_contents($log_file, $message, FILE_APPEND);
	}
}

