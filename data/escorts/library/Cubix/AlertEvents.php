<?php

class Cubix_AlertEvents
{
	private static function _db()
	{
		return Zend_Registry::get('db');
	}

	public static function notify($escort_id, $event, array $data = array())
	{
		$date = new Zend_Db_Expr('NOW()');
		
		if (in_array($event, array(ALERT_ME_NEW_REVIEW, ALERT_ME_NEW_COMMENT)) && isset($data['set_date']) && $data['set_date'])
		{
			$date = $data['set_date'];
		}
		
		$data = array(
			'date' => $date,
			'escort_id' => $escort_id,
			'event' => $event,
			'data' => (count($data) > 0 ? json_encode($data) : null)
		);

		self::_db()->insert('alert_events', $data);
	}

	public static function addFollowAlert($user_id, $type, $event_id){

		self::_db()->insert('follow_alerts', array(
			'user_id' => $user_id,
			'type' => $type,
			'event_id' => $event_id,
			'date' => date('Y-m-d H:m:s'),
			'status' => 0
		));	
	}
}
