<?php

/**
 * Class Cubix_6B_SMSResponseReplay
 *
 * @package Cubix/6B
 *
 * @property-read array $replies
 */
class Cubix_6B_SMSResponseReply extends Cubix_6B_SMSResponseAbstract
{
    const STATUS_UNEXPECTED_ERROR = 0;
    const STATUS_REPLIES_RETURNED = 1;
    const STATUS_REQUEST_INTERVAl_ERROR = 2;
    protected $STATUS_MESSAGES = array(
        self::STATUS_UNEXPECTED_ERROR => "An exception ocurred during the request.",
        self::STATUS_REPLIES_RETURNED => "Replies returned.",
        self::STATUS_REQUEST_INTERVAl_ERROR => "One request allowed each 60 seconds.",
    );
    public $replies;

    /**
     * @var array
     */
    protected $keyValuePair = array(
        'Respostas' =>'replies',
        'StatusMensagem' =>'statusMessage',
        'Status' =>'status',
    );

    /**
     * @return array
     */
    public function getReplies() {
        return $this->replies;
    }

    public function setResponse($data) {
        parent::setResponse($data);
    }
}