<?php
/**
 * Class Cubix_6BSMS
 * Only for 6anuncio.com
 * @package Cubix/6B
 * @property  $user string username for auth
 * @property  $password string password for auth
 *
 *
 *#################################
 * USAGE EXAMPLES
 * 1. same message to all recipients
 * $sms = new Cubix_SMS6B($username, $password);
 * $dest = $sms->send_sms(array('17)996333355', '(17) 9 9 6 3 3 3 3 5 5', '17 9964.53355'), 'subject', 'message',date('d/MM/yyyy'));
 *
 * 2. Personalized Send
 * $sms = new Cubix_SMS6B($username, $password);
 * $sms->send_sms_ersonalized(
 *     array(
 *         array('celular' => "(17)996333355", 'mensagem' => "bom dia", 'data' => date("dd/MM/yyyy")),
 *         array('celular' => "17.99633.3355", 'assunto' => "ola", 'mensagem' => "boa tarde", 'data' => date("dd/MM/yyyy")),
 *         array('celular' => "17996333355", 'mensagem' => "oi")
 *     )
 * );
 *
 *
 */
class Cubix_6B_SMS
{
    private $user;
    private $password;
    private $requestMethod = "GET";
    private $url = "https://ws.smsdigital.com.br/";
    private $Invoke = "sms/envio";
    private $InvokePersonalized = "sms/envioPersonalizado";
    private $InvokeStatus = "sms/status";
    private $InvokeReply = "sms/resposta";
    private $InvokeBalance = "sms/saldo";

    private $InvokeNewUser= "sms/usuario";

    // Values indicating SMS delivery acknowledgment. Possible values:
    const NEW_USER_DO_NOT_RECEIVE_ACKNOWLEDGEMENT = 0;
    const NEW_USER_RECEIVE_ACKNOWLEDGEMENT_BY_SMS = 1;
    const NEW_USER_RECEIVE_ACKNOWLEDGEMENT_BY_EMAIL = 2;
    const NEW_USER_RECEIVE_ACKNOWLEDGEMENT_BY_SMS_AND_EMAIL = 3;

    public static $NEW_USER_SMS_DELIVERY_VALUES = array(
        self::NEW_USER_DO_NOT_RECEIVE_ACKNOWLEDGEMENT => 'Do not receive acknowledgement.',
        self::NEW_USER_RECEIVE_ACKNOWLEDGEMENT_BY_SMS => 'Receive acknowledgement by SMS(Incurs in change).',
        self::NEW_USER_RECEIVE_ACKNOWLEDGEMENT_BY_EMAIL => 'Receive acknowledgement by e-mail.',
        self::NEW_USER_RECEIVE_ACKNOWLEDGEMENT_BY_SMS_AND_EMAIL => 'Receive acknowledgement by SMS and e-mail (Incurs charge).',
    );

    //Values indicating replies. Possible values:
    const NEW_USER_DO_NOT_RECEIVE_REPLIES = 0;
    const NEW_USER_RECEIVE_REPLIES_BY_SMS = 1;
    const NEW_USER_RECEIVE_REPLIES_BY_EMAIL = 2;
    const NEW_USER_RECEIVE_REPLIES_BY_SMS_AND_EMAIL = 3;

    public static $NEW_USER_REPLIES_VALUES = array(
        self::NEW_USER_DO_NOT_RECEIVE_REPLIES => 'Do not receive replies.',
        self::NEW_USER_RECEIVE_REPLIES_BY_SMS => 'Recieve replies by SMS(Incurs charge).',
        self::NEW_USER_RECEIVE_REPLIES_BY_EMAIL => 'Receive replies by e-mail.',
        self::NEW_USER_RECEIVE_REPLIES_BY_SMS_AND_EMAIL => 'Receive replies by SMS and e-mail (Incurs charge).',
    );

    private static $Instance;

    public function __construct($username, $password) {
        if (empty($username) || empty($password)){
            throw new Exception("Username and password required");
        }
        $this->user = $username;
        $this->password = $password;
    }

    /**
     * @return Cubix_6B_SMS
     * @throws Zend_Exception
     * @throws Exception
     */
    public static function getInstance(){
        if (empty(self::$Instance)){
            $sys_conf = Zend_Registry::get('system_config');
            $conf = $sys_conf['sms_6b'];
            self::$Instance = new static($conf['user'],$conf['password']);
        }

        return self::$Instance;
    }



    /**
     * @param $method string
     * @param $params array
     * @param $responseClass string
     * @return Cubix_6B_SMSResponse|Cubix_6B_SMSResponseStatus|Cubix_6B_SMSResponseReply|Cubix_6B_SMSResponseBalance|Cubix_6B_SMSResponseUser|Cubix_6B_SMSResponseAbstract
     *
     * @throws Exception
     */
    private function invoke($method, $params, $responseClass) {
        //request data
        $requestParams = array(
            'http' => array(
                'header' => array('Content-type: application/json',
                    'Authorization: Basic ' . base64_encode($this->user . ':' . $this->password)
                ),
                'method' => $this->requestMethod,
                'content' => json_encode($params),
            ),
        );

        //make request
        $response = file_get_contents($this->url . $method, false, stream_context_create($requestParams));

        //create Response
        $responseObject = new $responseClass();
        if (!$responseObject instanceof Cubix_6B_SMSResponseAbstract) {
            throw  new Exception("Response class should be instance of Cubix_SMS6B_ResponseInterface got :" .get_class($responseObject));
        }
        $responseObject->setResponse(json_decode($response, true));
        return $responseObject;
    }

    /**
     * @param $recipients array|string (required) Array containing the mobile number recipients.
     *        All numbers must include the area code;
     *        all non numeric characters will be removed.
     * @param $message string Message body.
     * @param $subject string Message subject. Required only for short marketing
     * @param $date string Date (UTC -3, dd/MM/yyyy HH:mm) to send the messages.
     * @param $flash string Whether the message should be sent as flash.
     * @return Cubix_6B_SMSResponse
     *
     * @throws Exception
     */
    public function send_sms($recipients, $message, $subject, $date, $flash = null) {
        if (!is_array($recipients)){
            $recipients = array($recipients);
        }
        //request params
        $requestParams = array(
            'recipients' => $recipients,
            'message' => $message,
            'subject' => $subject,
            'date' => $date,
            'flash' => $flash
        );
        $this->requestMethod = 'POST';
        $requestParams = $this->normaliseRequestDataFor($requestParams);
        //request
        return $this->invoke($this->Invoke, $requestParams, 'Cubix_6B_SMSResponse');
    }

    /**
     * @param $recipients array    Array of arrays with the following properties:
     * 1. @subject Message subject for this recipient. Required for short marketing.
     * 2. @phone Mobile number recipient. Required. The number must include the area code; all non numeric characters will be removed.
     * 3. @date Date (UTC -3, d/M/Y H:i) to send this message. Optional.
     * 4. @message Message body for this recipient. Required.
     *
     * @return Cubix_6B_SMSResponse
     *
     * @throws Exception
     */
    public function send_sms_personalized($recipients) {
        //request params
        $requestParams = array('recipients' => $recipients);
        $requestParams= $this->normaliseRequestDataFor($requestParams,'InvokePersonalized');
        //set request method
        $this->requestMethod = 'POST';
        //request
        return $this->invoke($this->InvokePersonalized, $requestParams, 'Cubix_6B_SMSResponse');
    }

    /**
     * @param $identificationNumbers array Array containing the recipient identification numbers.
     * @return Cubix_6B_SMSResponseStatus
     * @throws Exception
     */
    public function get_status($identificationNumbers = array()) {
        //request params
        $sequence_sms = array('sequencias' => $identificationNumbers);
        $this->requestMethod = 'POST';
        //request
        return $this->invoke($this->InvokeStatus, $sequence_sms, 'Cubix_6B_SMSResponseStatus');
    }

    /**
     * @param $identificationNumber array|string Array containing the recipient identification numbers or .
     * @return Cubix_6B_SMSResponseReply
     * @throws Exception
     */
    public function get_reply($identificationNumber)  {
        $normalisedData = $this->normaliseRequestDataFor($identificationNumber,'InvokeReply');
        //request
        return $this->invoke($this->InvokeReply, $normalisedData, 'Cubix_6B_SMSResponseReply');
    }

    /**
     * Interval between requests 10 seconds
     *
     * @param $subUser string optional  If null, the balance of the authenticated user will be returned.
     * @return Cubix_6B_SMSResponseBalance
     * @throws Exception
     */
    public function get_balance($subUser = '') {

        $parametro = array('subUsuario' => $subUser);
        //request
        return $this->invoke($this->InvokeBalance, $parametro, 'Cubix_6B_SMSResponseBalance');
    }

    /**
     * Restrictions
     * Max number of salespeople for HOME franchises 1
     * Max number of salespeople for FULL franchises 6
     * Max number of user accounts for LIGHT franchises 2
     *
     * @param $login string required Username, must be at least 6 characters in length.
     * @param $password string required Password, must be at least 6 characters in length and not the same as the username.
     * @param $phone string required
     * @param $email string required
     * @param $name string required
     * @param $confirmation int required
     * @param $reply int required
     * @param $serviceUrl string optional
     * @param $signature string optional
     * @param $salesperson bool optional
     * @return Cubix_6B_SMSResponseUser
     *
     * @throws Exception
     */
    public function create_user($login, $password, $phone, $email, $name, $confirmation, $reply, $serviceUrl = null, $signature = null, $salesperson = null) {
        $parametros = array(
            'login' => $login,
            'senha' => $password,
            'celular' => $phone,
            'email' => $email,
            'nome' => $name,
            'confirmacao' => $confirmation,
            'resposta' => $reply
        );
        if (!empty($serviceUrl)) {
            $parametros['UrlWebService'] = $serviceUrl;
        }
        if (!empty($signature)) {
            $parametros['assinatura'] = $signature;
        }
        if (!empty($salesperson)) {
            $parametros['vendedor'] = $salesperson;
        }
        $this->requestMethod = "POST";
        //request
        return $this->invoke($this->InvokeNewUser, $parametros, "Cubix_6B_SMSResponseUser");
    }

    /**
     * @param $data array Array with request Data
     * @param string $for
     * @return array Normalised request data
     * @throws Exception
     */
    private function normaliseRequestDataFor($data, $for='Invoke'){
        $method = "normaliseRequestDataFor".ucfirst($for);
        if (!method_exists($this,$method)){
            throw new Exception("Method Cubix_6BSMS::{$method} does not exists");
        }
        return call_user_func([$this, $method],$data);
    }

    /**
     * @param $data
     * @return array
     */
    private function normaliseRequestDataForInvoke($data){
        $normalisedData = array();
        foreach ($data as $key => $value) {
            if (empty($value) ||  !$value){
                continue;
            }
            if ($key == 'recipients'){
                $normalisedData['destinatarios'] = filter_var_array($value,FILTER_SANITIZE_NUMBER_INT);
            }
            elseif ($key == 'subject'){
                $normalisedData['assunto'] = $this->_normaliseText($value);
            }
            elseif ($key == 'message'){
                $normalisedData['mensagem'] = $this->_normaliseText($value);
            }
            elseif ($key == 'date'){
                $normalisedData['data'] = $value;
            }
            elseif ($key == 'flash'){
                $normalisedData['flash'] = $value;
            }
        }

        return $normalisedData;
    }

    /**
     * @param $data
     * @return array
     * @throws Exception
     */
    private function normaliseRequestDataForInvokePersonalized($data){
        $normalisedData = array(
            'destinatarios' => array()
        );
        if (empty($data['recipients'])){
            throw new Exception("Recipients required");
        }

        if (!is_array($data['recipients'])){
            throw new Exception("Recipients must be an array");
        }

        foreach ($data['recipients'] as $index => $item) {
            $subject = trim($item['subject']);
            $message = trim($item['message']);
            $date = trim($item['date']);
            $phone = trim($item['phone']);
            if (!empty($subject)) {
                $normalisedData['destinatarios'][$index]['assunto'] = $this->_normaliseText($item['subject']);
            }
            if (empty($message)) {
                throw new Exception("Message body required");
            }
            $normalisedData['destinatarios'][$index]['mensagem'] = $this->_normaliseText($item['message']);
            if (!empty($date)) {
                $normalisedData['destinatarios'][$index]['data'] = trim($item['date']);
            }
            if (empty($phone)) {
                throw new Exception("Phone number required");
            }
            $phone = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
            $phone = str_replace('-', '', $phone);
            $phone = str_replace('+', '', $phone);
            $normalisedData['destinatarios'][$index]['celular'] = $phone;
        }

        return $normalisedData;
    }

    /**
     * @param $data
     * @return array
     * @throws Exception
     */
    private function normaliseRequestDataForInvokeReply($data){
        $normalisedData = array(
            'sequencias' => array()
        );
        if (empty($data['identificationNumber'])){
            $data['identificationNumber'] = array();
        }

        if (!is_array($data['identificationNumber'])){
            array_push($normalisedData['sequencias'], $data['identificationNumber']);
        }else{
            $normalisedData['sequencias'] = $data['identificationNumber'];
        }


        return $normalisedData;
    }

    /**
     * @param $string
     * @return mixed|string
     */
    private function _normaliseText($string) {

        $search = array(
            "¡", "¢", "£", "¤", "¥", "¦", "§", "©", "ª", "«", "¬", "­", "®", "¯", "°", "±", "²", "³", "´", "µ",
            "¶", "·", "¸", "¹", "º", "»", "¼", "½", "¾", "¿", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É",
            "Ê", "Ë", "Ì", "Í", "Î", "Ï", "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "×", "Ø", "Ù", "Ú", "Û", "Ü", "Ý",
            "Þ", "ß", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", "ð", "ñ",
            "ò", "ó", "ô", "õ", "ö", "÷", "ø", "ù", "ú", "û", "ü", "ý", "þ", "ÿ",
            );
        $replace = array(
            "&#161", "&#162", "&#163", "&#164", "&#165", "&#166", "&#167", "&#169", "&#170", "&#171",
            "&#172", "&#173", "&#174", "&#175", "&#176", "&#177", "&#178", "&#179", "&#180", "&#181",
            "&#182", "&#183", "&#184", "&#185", "&#186", "&#187", "&#188", "&#189", "&#190", "&#191",
            "&#192", "&#193", "&#194", "&#195", "&#196", "&#197", "&#198", "&#199", "&#200", "&#201",
            "&#202", "&#203", "&#204", "&#205", "&#206", "&#207", "&#208", "&#209", "&#210", "&#211",
            "&#212", "&#213", "&#214", "&#215", "&#216", "&#217", "&#218", "&#219", "&#220", "&#221",
            "&#222", "&#223", "&#224", "&#225", "&#226", "&#227", "&#228", "&#229", "&#230", "&#231",
            "&#232", "&#233", "&#234", "&#235", "&#236", "&#237", "&#238", "&#239", "&#240", "&#241",
            "&#242", "&#243", "&#244", "&#245", "&#246", "&#247", "&#248", "&#249", "&#250", "&#251",
            "&#252", "&#253", "&#254", "&#255",
        );

        $string = str_replace($search, $replace, $string);
        return trim($string);
    }

}
