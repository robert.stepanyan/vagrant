<?php

/**
 * Class Cubix_6B_SMSResponseStatus
 *
 * @package Cubix/6B
 *
 * @property $identificationNumbers array
 */
class Cubix_6B_SMSResponseStatus extends Cubix_6B_SMSResponseAbstract
{
    public $identificationNumbers;
    const STATUS_UNEXPECTED_ERROR = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_INTERVAl_ERROR = 2;
    protected $STATUS_MESSAGES = array(
        self::STATUS_UNEXPECTED_ERROR => "An exception ocurred during the request.",
        self::STATUS_SUCCESS => "Replies returned.",
        self::STATUS_INTERVAl_ERROR => "One request allowed each 60 seconds.",
    );
    protected $keyValuePair = array(
        'StatusSequencias' =>'identificationNumbers',
        'StatusMensagem' =>'statusMessage',
        'Status' =>'status'
    );

    private $_statusDescriptions = array(
        101 => "Send", //The message is in the queue to be sent.
        104 => "Error", //An error occurred during the comunication with the carrier and the message was not sent.
        105 => "Sent", //The message was sent, but the carrier have yet to acknowledge the delivery.
        106 => "Not delivered", //	The carrier could not send the message for techinical reasons, e.g.: number does not exist.
        107 => "Delivered", //The message was sent and the carrier acknowledged the deliver.
        108 => "Blacklist", //The message was not sent because the recipient was blacklisted. Incurs no charges.
        109 => "Invalid", //The message was not set because the recipient is either invalid, fixed telephone or nonexistent. Incurs no charges.
        112 => "Quota Exceeded", //	Max weekly quota of 5 messages per recipient exceeded. Incurs no charge.
        113 => "Delivered", //without acknowledgment	The message was sent to a carrier without delivery acknowledgment, e.g.: OI.
        114 => "Spam", //The message was considered spam. Incurs no charge.
        116 => "Duplicate", //The same message was sent to the same recipient during a period of 12 hours. Incurs no charge.
    );

    /**
     * @param $statusCode int
     * @return mixed
     */
    public function getStatusForSmsOutBox($statusCode) {
        $statusArray = [
            101 => 0,  // Model_SMS_SMS::SMS_STATUS_UNKNOWN
            104 => 2,  // Model_SMS_SMS::SMS_STATUS_FAILURE
            105 => 4,  // Model_SMS_SMS::SMS_STATUS_SENT
            106 => 5,  // Model_SMS_SMS::SMS_STATUS_NOT_DELIVERED
            107 => 1,  // Model_SMS_SMS::SMS_STATUS_SUCCESS
            108 => 6,  // Model_SMS_SMS::SMS_STATUS_BLACKLIST
            109 => 7,  // Model_SMS_SMS::SMS_STATUS_INVALID
            112 => 8,  // Model_SMS_SMS::SMS_STATUS_QUOTA_EXCEEDED
            113 => 9,  // Model_SMS_SMS::SMS_STATUS_NO_ACKNOWLEGDMENT
            114 => 10, // Model_SMS_SMS::SMS_STATUS_SPAM
            116 => 11  // Model_SMS_SMS::SMS_STATUS_DUPLICATE
        ];
        return $statusArray[$statusCode];
    }

    /**
     * @return mixed
     */
    public function getIdentificationNumbers() {
        return $this->identificationNumbers;
    }

    /**
     * @param $data
     */
    public function setResponse($data) {
        parent::setResponse($data);
        $identificationNumbers = array();
        if (!empty($this->identificationNumbers) && is_array($this->identificationNumbers)){
            foreach ($this->identificationNumbers as $index => $row) {
                $identificationNumbers[$index] = array();
                $identificationNumbers[$index]['identificationNumber'] = $row['Sequencia'];
                $identificationNumbers[$index]['statusDescription'] = $this->_statusDescriptions[$row['StatusCodigo']];
                $identificationNumbers[$index]['statusCode'] = $row['StatusCodigo'];
            }
        }
        $this->identificationNumbers = $identificationNumbers;
    }
}