<?php

/**
 * Class Cubix_6B_SMSResponseAbstract
 *
 * @package Cubix/6B
 *
 * @property-read integer $status
 * @property-read string $statusMessage
 */
abstract class Cubix_6B_SMSResponseAbstract implements Cubix_6B_SMSResponseInterface
{
    public $status,
        $statusMessage;

    protected $keyValuePair = array();
    protected $STATUS_MESSAGES = array();

    /**
     * @param $name string
     * @param $value mixed
     * @throws Exception
     */
    public function __set($name, $value) {
        $setter= 'set'.ucfirst($name);

        if (!property_exists($this, $name)){
            throw new Exception("Property ".get_class($this)."::{$name} does not exists");
        }

        if (!method_exists($this,$setter)){
            $getter = 'get'.ucfirst($name);
            if (method_exists($this,$getter)){
                throw new Exception("unable to set value. property ".get_called_class()."::{$name} is read only ");
            }else{
                throw new Exception("unable to set value to property ".get_called_class()."::{$name} no permission");
            }
        }
        $this->$setter($value);
    }

    /**
     * @param $name string
     * @return mixed
     * @throws Exception
     */
    public function __get($name) {
        if (!property_exists($this, $name)){
            throw new Exception("Property ".get_called_class()."::".$name." does not exists");
        }
        $getter = 'get'.ucfirst($name);
        if (!method_exists($this,$getter)){
            $setter = 'set'.ucfirst($name);
            if (method_exists($this,$setter)){
                throw new Exception("unable to set value. property ".get_called_class()."::{$name} is write only ");
            }else{
                throw new Exception("unable to set value to property ".get_called_class()."::{$name} no permission");
            }
        }
         return $this->$getter();
    }

    /**
     * is triggered when invoking inaccessible methods in an object context.
     *
     * @param $name string
     * @param $arguments array
     * @return mixed
     * @throws Exception
     */
    public function __call($name, $arguments) {
        if (!method_exists($this, $name)){
            throw new Exception("Method ".get_called_class()."::".$name."() does not exists ");
        }

        return call_user_func_array([get_called_class(), $name], $arguments);
    }

    /**
     * @param $name
     * @return bool
     */
    public function __isset($name) {
        $getter = "get" . $name;
        if (method_exists($this, $name)) {
            return $this->$getter() !== null;
        }
        return false;
    }

    /**
     * @param $name string
     * @throws Exception
     */
    public function __unset($name) {
        $setter = 'set' . $name;
        if (!method_exists($this, $setter)) {
            throw new Exception('Unsetting an unknown or read-only property: ' . get_class($this) . '::' . $name);
        }

        $this->$setter(null);
    }

    public function setResponse($data) {
        foreach ($data AS $key => $value){
            $property = !empty($this->keyValuePair[$key]) ? $this->keyValuePair[$key] : null;
            if (!$property){
                continue;
            }
            if (property_exists($this, $property)){
                if ($property == 'statusMessage'){
                    $this->{$property} = $this->STATUS_MESSAGES[$data['Status']];
                }else{
                    $this->{$property} = $value;
                }

            }
        }
    }

    /**
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getStatusMessage() {
        return $this->statusMessage;
    }
}