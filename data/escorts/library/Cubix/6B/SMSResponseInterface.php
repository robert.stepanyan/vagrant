<?php


interface Cubix_6B_SMSResponseInterface
{
    public function setResponse($data);

    public function getStatus();

    public function getStatusMessage();
}