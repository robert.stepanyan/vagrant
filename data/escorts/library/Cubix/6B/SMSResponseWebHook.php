<?php


/**
 * Class Cubix_6B_SMSResponseStatusWebHook
 *
 * @package Cubix/6B
 * @property $smsType string
 * @property string $smsTypeDescription
 * @property string $identificationNumber
 * @property int $statusCode
 * @property string $statusDescription
 * @property string $message
 * @property string $replyDate
 * @property string $phone
 */
class Cubix_6B_SMSResponseWebHook extends Cubix_6B_SMSResponseAbstract
{
    public
        $smsType, // Number code identifying the plataform:
        $smsTypeDescription, // Name of the platform in which the SMS was sent.
        $identificationNumber, //Identification number for SMS message sent.
        $statusCode, //Status for status
        $statusDescription, //Status description of SMS for status
        $message, // SMS message reply.
        $replyDate, //Reply date.
        $phone; //Recipient phone number.

    const STATUS_SEND = 101;
    const STATUS_ERROR = 104;
    const STATUS_SENT = 105;
    const STATUS_NOT_DELIVERED = 106;
    const STATUS_DELIVERED = 107;
    const STATUS_BLACKLIST = 108;
    const STATUS_INVALID = 109;
    const STATUS_QUOTA_EXCEEDED = 112;
    const STATUS_DELIVERED_WITHOUT_ACKNOWLEDGMENT = 113;
    const STATUS_SPAM = 114;
    const STATUS_DUPLICATE = 116;

    protected $keyValuePair = array(
        'TipoSMS' => 'smsType',
        'TipoSMSDescricao' => 'smsTypeDescription',
        'Sequencia' => 'identificationNumber',
        'Celular' => 'phone',
        'StatusCodigo' => 'statusCode',
        'StatusDescricao' => 'statusDescription',
        'Mensagem' =>'message',
        'Data' => 'replyDate',
        'StatusMensagem' => 'statusMessage',
        'Status' => 'status'
    );

    private $_statusDescriptions = array(
        self::STATUS_SEND => "Send", //The message is in the queue to be sent.
        self::STATUS_ERROR => "Error", //An error occurred during the comunication with the carrier and the message was not sent.
        self::STATUS_SENT => "Sent", //The message was sent, but the carrier have yet to acknowledge the delivery.
        self::STATUS_NOT_DELIVERED => "Not delivered", //	The carrier could not send the message for techinical reasons, e.g.: number does not exist.
        self::STATUS_DELIVERED => "Delivered", //The message was sent and the carrier acknowledged the deliver.
        self::STATUS_BLACKLIST => "Blacklist", //The message was not sent because the recipient was blacklisted. Incurs no charges.
        self::STATUS_INVALID => "Invalid", //The message was not set because the recipient is either invalid, fixed telephone or nonexistent. Incurs no charges.
        self::STATUS_QUOTA_EXCEEDED => "Quota Exceeded", //	Max weekly quota of 5 messages per recipient exceeded. Incurs no charge.
        self::STATUS_DELIVERED_WITHOUT_ACKNOWLEDGMENT => "Delivered without acknowledgment", //without acknowledgment	The message was sent to a carrier without delivery acknowledgment, e.g.: OI.
        self::STATUS_SPAM => "Spam", //The message was considered spam. Incurs no charge.
        self::STATUS_DUPLICATE => "Duplicate", //The same message was sent to the same recipient during a period of 12 hours. Incurs no charge.
    );

    public function setResponse($data) {
        parent::setResponse($data);
        if (!$this->isReplay()){
            $this->statusDescription = $this->_statusDescriptions[intval($this->statusCode)];
            $this->statusMessage = "Status returned";
        }else{
            $date = str_replace('/', '-', $this->replyDate);
            if ($time = strtotime($date)){
                $this->replyDate = date("Y-m-d H:i:s", $time);
            }else{
                $this->replyDate = date("Y-m-d H:i:s");
            }
            $this->statusMessage = "Reply returned";
        }
    }

    public function getStatusForSmsOutBox() {
        $statusArray = [
            self::STATUS_SEND => 0,  // Model_SMS_SMS::SMS_STATUS_UNKNOWN
            self::STATUS_ERROR => 2,  // Model_SMS_SMS::SMS_STATUS_FAILURE
            self::STATUS_SENT => 4,  // Model_SMS_SMS::SMS_STATUS_SENT
            self::STATUS_NOT_DELIVERED => 5,  // Model_SMS_SMS::SMS_STATUS_NOT_DELIVERED
            self::STATUS_DELIVERED => 1,  // Model_SMS_SMS::SMS_STATUS_SUCCESS
            self::STATUS_BLACKLIST => 6,  // Model_SMS_SMS::SMS_STATUS_BLACKLIST
            self::STATUS_INVALID => 7,  // Model_SMS_SMS::SMS_STATUS_INVALID
            self::STATUS_QUOTA_EXCEEDED => 8,  // Model_SMS_SMS::SMS_STATUS_QUOTA_EXCEEDED
            self::STATUS_DELIVERED_WITHOUT_ACKNOWLEDGMENT => 9,  // Model_SMS_SMS::SMS_STATUS_NO_ACKNOWLEGDMENT
            self::STATUS_SPAM => 10, // Model_SMS_SMS::SMS_STATUS_SPAM
            self::STATUS_DUPLICATE => 11  // Model_SMS_SMS::SMS_STATUS_DUPLICATE
        ];
        return $statusArray[$this->statusCode];
    }

    /**
     * @return bool
     */
    public function isReplay() {
        return !empty($this->message);
    }
    /**
     * @return mixed
     */
    public function getSmsType() {
        return $this->smsType;
    }

    /**
     * @return string
     */
    public function getSmsTypeDescription() {
        return $this->smsTypeDescription;
    }

    /**
     * @return string
     */
    public function getIdentificationNumber() {
        return $this->identificationNumber;
    }

    /**
     * @return int
     */
    public function getStatusCode() {
        return $this->statusCode;
    }

    /**
     * @return string
     */
    public function getStatusDescription() {
        return $this->statusDescription;
    }

    /**
     * @return mixed
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getReplyDate() {
        return $this->replyDate;
    }

    /**
     * @return mixed
     */
    public function getPhone() {
        return $this->phone;
    }



}