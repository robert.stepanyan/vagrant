<?php

/**
 * Class Cubix_6B_SMSResponse
 *
 * @package Cubix/6B
 *
 * @property $recipients
 *
 */
class Cubix_6B_SMSResponse extends Cubix_6B_SMSResponseAbstract
{
    const STATUS_UNEXPECTED_ERROR = 0;
    const STATUS_MESSAGE_SENT = 1;
    const STATUS_INSUFFICIENT_FUNDS = 2;
    const STATUS_MESSAGE_REQUIRED = 3;
    const STATUS_SUBJECT_REQUIRED = 4;
    const STATUS_RECIPIENTS_REQUIRED = 5;
    const STATUS_RECIPIENTS_EXCEEDED = 6;
    const STATUS_SHORT_MESSAGE_EXCEEDED = 7;

    protected $STATUS_MESSAGES = array(
        self::STATUS_UNEXPECTED_ERROR => "An exception occurred during the request",
        self::STATUS_MESSAGE_SENT => "Messages sent",
        self::STATUS_INSUFFICIENT_FUNDS => "Insufficient funds",
        self::STATUS_MESSAGE_REQUIRED => "Message body must not be null",
        self::STATUS_SUBJECT_REQUIRED => "For short marketing messages, parameter Assunto must not be null",
        self::STATUS_RECIPIENTS_REQUIRED => "Parameter Recipients must not be null",
        self::STATUS_RECIPIENTS_EXCEEDED => "Max recipients number (50000) exceeded",
        self::STATUS_SHORT_MESSAGE_EXCEEDED => "Short marketing messages must not exceeded 160 characters/Subject in short marketing messages must not exceed 20 characters",
    );
    public $recipients;

    /**
     * @var array
     */
    protected $keyValuePair = array(
        'Lote' =>'recipients',
        'StatusMensagem' =>'statusMessage',
        'Status' =>'status',
        'Destinatarios' =>'recipients',
    );


    /**
     * @return mixed
     */
    public function getRecipients() {
        return $this->recipients;
    }

    public function setResponse($data) {
        parent::setResponse($data);
        $recipients = array();
        if (is_array($this->recipients) && !empty($this->recipients)){
            foreach ($this->recipients as $index => $recipient) {
                $recipients[$index] = array();
                $recipients[$index]['phone'] = $recipient['Celular'];
                $recipients[$index]['identificationNumber'] = $recipient['Sequencia'];
            }
        }
        $this->recipients = $recipients;
    }

}