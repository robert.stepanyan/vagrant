<?php

/**
 * Class Cubix_6B_SMSResponseBalance
 *
 * @package Cunix/6B
 *
 * @property-read float $balance
 */
class Cubix_6B_SMSResponseBalance extends Cubix_6B_SMSResponseAbstract
{
    public $balance;
    public $balances;
    const STATUS_UNEXPECTED_ERROR = 0;
    const STATUS_REPLIES_RETURNED = 1;
    const STATUS_REQUEST_INTERVAl_ERROR = 2;
    const STATUS_INVALID_SUBUSER = 3;
    protected $STATUS_MESSAGES = array(
        self::STATUS_UNEXPECTED_ERROR => "An exception ocurred during the request.",
        self::STATUS_REPLIES_RETURNED => "Balance returned.",
        self::STATUS_REQUEST_INTERVAl_ERROR => "One request allowed each 10 seconds.",
        self::STATUS_INVALID_SUBUSER => "Invalid subuser.",
    );
    protected $keyValuePair = array(
        'Saldo' =>'balance',
        'Saldos' =>'balances',
        'StatusMensagem' =>'statusMessage',
        'Status' =>'status'
    );

    /**
     * @return float
     */
    public function getBalance() {
        return $this->balance;
    }

}