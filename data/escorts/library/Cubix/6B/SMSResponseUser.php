<?php


class Cubix_6B_SMSResponseUser extends Cubix_6B_SMSResponseAbstract
{
    protected $keyValuePair = array(
        'StatusMensagem' => 'statusMessage',
        'Status' => 'status'
    );
    const STATUS_UNEXPECTED_ERROR = 0;
    const STATUS_USER_CREATED = 1;
    const STATUS_QUOTA_EXCEED = 2;
    const STATUS_USERNAME_IN_USE = 3;
    const STATUS_MISSING_FIELDS = 4;
    const STATUS_INVALID_FIELDS = 5;

    protected $STATUS_MESSAGES = array(
        self::STATUS_UNEXPECTED_ERROR => "An exception occurred during the request",
        self::STATUS_USER_CREATED => "User created",
        self::STATUS_QUOTA_EXCEED => "Salesperson or user account quota exceed",
        self::STATUS_USERNAME_IN_USE => "Username already in use",
        self::STATUS_MISSING_FIELDS => "Required fields (Phone, Name, Email, Confirmation, Response, Login, Password) missing",
        self::STATUS_INVALID_FIELDS => "Phone, Login, Email or Password invalid"
    );
}