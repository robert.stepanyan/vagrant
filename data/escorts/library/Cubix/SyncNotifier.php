<?php

class Cubix_SyncNotifier
{
	const EVENT_ESCORT_CREATED = 1;
	const EVENT_ESCORT_UPDATED = 2;
	const EVENT_ESCORT_STATUS_UPDATED = 3;

	const EVENT_ESCORT_REV_CREATED = 7;
	const EVENT_ESCORT_REV_UPDATED = 8;
	const EVENT_ESCORT_REV_APPROVED = 9;
	const EVENT_ESCORT_REV_DECLINED = 10;

	const EVENT_ESCORT_PHOTO_ADDED = 4;
	const EVENT_ESCORT_PHOTO_DELETED = 5;
	const EVENT_ESCORT_PHOTO_UPDATED = 6;

	const EVENT_PACKAGE_ACTIVATED = 11;
	const EVENT_PACKAGE_DEACTIVATED = 12;

	const EVENT_ESCORT_TOUR_ADDED = 13;
	const EVENT_ESCORT_TOUR_UPDATED = 14;
	const EVENT_ESCORT_TOUR_DELETED = 15;

	const EVENT_ESCORT_BUBBLE_TEXT = 16;

	const EVENT_ESCORT_HAPPY_HOUR_ACTIVATED = 17;
	const EVENT_ESCORT_HAPPY_HOUR_DEACTIVATED = 18;

	const EVENT_ESCORT_TOUR_ACTIVATED = 19;
	const EVENT_ESCORT_TOUR_DEACTIVATED = 20;

	const EVENT_ESCORT_OPEN = 21;
	const EVENT_ESCORT_CLOSED = 22;

	const EVENT_ESCORT_SUSPICIOUS_UPDATED = 23;

	const EVENT_ESCORT_PACKAGE_EXPIRATION_UPDATED = 24;
	
	const EVENT_ESCORT_VIP_TOGGLED = 25;
	const EVENT_ESCORT_REVIEW_TOGGLED = 26;
	const EVENT_ESCORT_COMMENT_TOGGLED = 27;

	const EVENT_ESCORT_REVIEW_APPROVED = 28;
	const EVENT_ESCORT_REVIEW_NOT_APPROVED = 29;
	const EVENT_ESCORT_REVIEW_DISABLED = 30;
	const EVENT_ESCORT_REVIEW_DELETED = 31;
	const EVENT_ESCORT_REVIEW_NEED_MODIFICATION = 32;
	const EVENT_ESCORT_SHOW_GALLERY_TOGGLED = 33;
	
	
	const EVENT_ESCORT_VIDEO_ADDED = 34;
	const EVENT_ESCORT_VIDEO_DELETED = 35;
	const EVENT_ESCORT_VIDEO_UPDATED = 36;
	
	const EVENT_ESCORT_PHONE_CHANGED = 37;
	
	const EVENT_ED_LISTING_PRODUCT_ADDED = 38;
	const EVENT_ED_LISTING_PRODUCT_DELETED = 39;

	const EVENT_ESCORT_ORDER_TRANSFER = 40; //order trsanfer from 1 sales to another
	const EVENT_ESCORT_PROFILE_TRANSFER = 41; //profile trsanfer from 1 sales to another
	const EVENT_PREMIUM_CITY_CHANGED = 42;
	const EVENT_VOTD_EXPIRED = 43;
	const EVENT_VOTD_ACTIVATED = 44;
	const EVENT_ESCORT_ORDER_REMOVED = 45;

	const EVENT_ORDER_REMOVED = 45;

	const EVENT_VOTD_REACTIVATED = 46;

	const EVENT_SKYPE_CALL_ACTIVATED = 47;
	const EVENT_SKYPE_CALL_DEACTIVATED = 48;

	const EVENT_CAM_BOOKING_ACTIVATED = 49;
	const EVENT_CAM_BOOKING_DEACTIVATED = 50;
	
	const EVENT_PHONE_VERIFIED = 51;
	const EVENT_ESCORT_EXPIRATION_UPDATED = 52;

	const EVENT_ESCORT_PACKAGE_MODIFIED_UPDATED = 53;

    const EVENT_GOTD_EXPIRED = 54;
    const EVENT_GOTD_ACTIVATED = 55;

	public static $_events = array(
		self::EVENT_ESCORT_CREATED => 'escort created',
		self::EVENT_ESCORT_UPDATED => 'escort updated',
		self::EVENT_ESCORT_STATUS_UPDATED => 'escort status updated',

		self::EVENT_ESCORT_PHOTO_ADDED => 'escort photo added',
		self::EVENT_ESCORT_PHOTO_DELETED => 'escort photo deleted',
		self::EVENT_ESCORT_PHOTO_UPDATED => 'escort photo updated',

		self::EVENT_ESCORT_REV_CREATED => 'escort revision created',
		self::EVENT_ESCORT_REV_UPDATED => 'escort revision updated',
		self::EVENT_ESCORT_REV_APPROVED => 'escort revision approved',
		self::EVENT_ESCORT_REV_DECLINED => 'escort revision declined',

		self::EVENT_PACKAGE_ACTIVATED => 'package activated',
		self::EVENT_PACKAGE_DEACTIVATED => 'package deactivated',

		self::EVENT_ESCORT_TOUR_ADDED => 'tour added',
		self::EVENT_ESCORT_TOUR_UPDATED => 'tour updated',
		self::EVENT_ESCORT_TOUR_DELETED => 'tour deleted',

		self::EVENT_ESCORT_BUBBLE_TEXT => 'bubble text',

		self::EVENT_ESCORT_HAPPY_HOUR_ACTIVATED => 'happy hour activated',
		self::EVENT_ESCORT_HAPPY_HOUR_DEACTIVATED => 'happy hour deactivated',

		self::EVENT_ESCORT_TOUR_ACTIVATED => 'tour activated',
		self::EVENT_ESCORT_TOUR_DEACTIVATED => 'tour deactivated',

		self::EVENT_ESCORT_OPEN => 'escort open',
		self::EVENT_ESCORT_CLOSED => 'escort closed',

		self::EVENT_ESCORT_SUSPICIOUS_UPDATED => 'suspicious updated',

		self::EVENT_ESCORT_PACKAGE_EXPIRATION_UPDATED => 'package expiration date updated',
		
		self::EVENT_ESCORT_VIP_TOGGLED => 'escort VIP toggled',
		self::EVENT_ESCORT_REVIEW_TOGGLED => 'escort REVIEW toggled',
		self::EVENT_ESCORT_COMMENT_TOGGLED => 'escort COMMENT toggled',

		self::EVENT_ESCORT_REVIEW_APPROVED => 'Approved Review ',
		self::EVENT_ESCORT_REVIEW_NOT_APPROVED => 'Not Approved Review ',
		self::EVENT_ESCORT_REVIEW_DISABLED => 'Disabled Review ',
		self::EVENT_ESCORT_REVIEW_DELETED => 'Deleted Review ',
		self::EVENT_ESCORT_REVIEW_NEED_MODIFICATION => 'Need Modifiaction Review ',
		self::EVENT_ESCORT_SHOW_GALLERY_TOGGLED => 	'escort Show Gallery toggled',
		
		self::EVENT_ESCORT_VIDEO_ADDED => 'escort video added',
		self::EVENT_ESCORT_VIDEO_DELETED => 'escort video deleted',
		self::EVENT_ESCORT_VIDEO_UPDATED => 'escort video updated',
		
		self::EVENT_ESCORT_PHONE_CHANGED => 'phone number changed',
		
		self::EVENT_ED_LISTING_PRODUCT_ADDED => 'ED listing product added',
		self::EVENT_ED_LISTING_PRODUCT_DELETED => 'ED listing product deleted', 

		self::EVENT_ESCORT_ORDER_TRANSFER => 'order sales transfer',
		self::EVENT_ESCORT_PROFILE_TRANSFER => 'profile sales transfer',
		self::EVENT_PREMIUM_CITY_CHANGED => 'premium city changed',	
		
		self::EVENT_VOTD_EXPIRED => 'votd expired',	
		self::EVENT_VOTD_REACTIVATED => 'votd  re-activated',	

		self::EVENT_VOTD_ACTIVATED => 'votd activated',
		self::EVENT_ESCORT_ORDER_REMOVED => 'escort order removed',

		self::EVENT_SKYPE_CALL_ACTIVATED => 'skype call activated',
		self::EVENT_SKYPE_CALL_DEACTIVATED => 'skype call deactivated',

		self::EVENT_CAM_BOOKING_ACTIVATED => 'cam booking activated',
		self::EVENT_CAM_BOOKING_DEACTIVATED => 'cam booking deactivated',
		 
		self::EVENT_PHONE_VERIFIED => 'Phone number is verified',
        self::EVENT_ESCORT_EXPIRATION_UPDATED => 'Escort Expiration date was seted',

        self::EVENT_ESCORT_PACKAGE_MODIFIED_UPDATED => 'Escort Packages were edited',

        self::EVENT_GOTD_EXPIRED => 'gotd expired',
		self::EVENT_GOTD_ACTIVATED => 'gotd activated',

	);

	private static function _db()
	{
		return Zend_Registry::get('db');
	}

	public static function notify($escort_id, $event, array $data = array(),$sales_user_id = null)
	{
		if ( !array_key_exists($event, self::$_events) ) {
			throw new Exception('Invalid event type "' . $event . '"');
		}
		
		$data = array(
			'date' => new Zend_Db_Expr('NOW()'),
			'escort_id' => $escort_id,
			'event_type' => $event,
			'data' => (count($data) > 0 ? json_encode($data) : null),
			'comment' => !in_array($event, array(28,29,30,31,32)) ? self::$_events[$event] : self::$_events[$event].' ID '. $data['review_id']
		);
		
        if (!empty($sales_user_id) && intval($sales_user_id) > 0){
            $data['sales_user_id'] = $sales_user_id;
        }
        
		if ( Zend_Auth::getInstance()->hasIdentity() ) {
			$bo_user = Zend_Auth::getInstance()->getIdentity();
			$data['sales_user_id'] = $bo_user->id;
		}

		$ip = Cubix_Geoip::getIP();
		
		if ( $ip ) {
			$data['ip'] = $ip;
		}

		self::_db()->insert('journal', $data);
	}

	public static function getAffectedEscorts()
	{
		return self::_db()->fetchAll('
			SELECT DISTINCT(escort_id)
			FROM journal
			WHERE is_done = 0
		');
	}

	public static function markAsDone($escort_id)
	{
		// Force $escort_id to be array
		if ( ! is_array($escort_id) ) {
			$escort_id = array($escort_id);
		}

		// Convert all ids to integer
		foreach ( $escort_id as $i => $id ) {
			$escort_id[$i] = intval($id);
		}

		// We don't need duplicate ids
		array_unique($escort_id);

		// Break the whole array into small chunks to avoid sql query limit
		$escorts = array();
		$chunk = 50;
		$chunks = ceil(count($escort_id) / $chunk);
		for ( $i = 0; $i < $chunks; $i++ ) {
			$escorts[] = array_slice($escort_id, $i * $chunk, $chunk);
		}

		foreach ( $escorts as $ids ) {
			self::_db()->update('journal', array(
				'is_sync_done' => 1,
				'date_done' => new Zend_Db_Expr('NOW()')
			), 'escort_id IN (' . implode(', ', $ids) . ')');
		}
	}
	
	public static function notifyAgency($user_id, $agency_id, $comment)
	{		
		$data = array(
			'user_id' => $user_id,
			'agency_id' => $agency_id,
			'comment' => $comment
		);
		
		if ( Zend_Auth::getInstance()->hasIdentity() ) {
			$bo_user = Zend_Auth::getInstance()->getIdentity();
			$data['sales_user_id'] = $bo_user->id;
		}

		$ip = Cubix_Geoip::getIP();
		
		if ( $ip ) {
			$data['ip'] = $ip;
		}

		self::_db()->insert('journal_agency', $data);
	}
}
