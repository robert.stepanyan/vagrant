<?php

function _cubix_images_ftp_error_handler($a1, $a2, $a3, $a4)
{
	throw new Cubix_Images_Storage_Exception('PHP Error: ' . $a1 . ' ' . $a2 . ' ' . $a3 . ' ' . $a4);
}

class Cubix_Images_Storage_Ftp
{
	/**
	 * Contains host, port, login, password and the root dir of ftp server
	 *
	 * @var array
	 */
	protected $_opts = array();
	
	/**
	 * The ftp connection resource
	 *
	 * @var resource
	 */
	protected $_conn = FALSE;
	
	public function __construct(array $options = array())
	{
		
		$options = array_merge(array(
			'host' => 'localhost',
			'port' => 21,
			'login' => 'root',
			'password' => '',
			'root_dir' => '/',
			'passive' => false,
			'timeout' => 90
		), $options);
		
		$this->_opts = $options;
	}
	
	public function connect()
	{
		
		$this->_conn = ftp_connect($this->_opts['host'], $this->_opts['port'], $this->_opts['timeout']);
		
		if ( $this->_conn === FALSE ) {
			throw new Cubix_Images_Storage_Exception('Could not connect to ftp storage');
		}
		
		if ( ! ftp_login($this->_conn, $this->_opts['login'], $this->_opts['password']) ) {
			throw new Cubix_Images_Storage_Exception('Wrong credentials, could not login to ftp storage');
		}
		
		if ( $this->_opts['root_dir'] ) {
			if ( ftp_chdir($this->_conn, $this->_opts['root_dir'] ) ) {
				throw new Cubix_Images_Storage_Exception('Could not chdir to root dir, make sure the path exists');
			}
		}
		
		if ( $this->_opts['passive'] ) {
			if ( ! ftp_pasv($this->_conn, TRUE) ) {
				throw new Cubix_Images_Storage_Exception('Could not enter passive mode');
			}
		}
		
		return TRUE;
	}
	
	public function move($file_from, $file_to)
	{
		$command = "RNFR " . $file_from;
		ftp_raw($this->_conn, $command);
		
		$command = "RNTO " . $file_to;
		ftp_raw($this->_conn, $command);
	}
	
	/**
	 * Uploads a file from local server to remote ftp storage
	 *
	 * @param string $local 
	 * @param string $remote 
	 * @param FTP_BINARY|FTP_ASCII $mode
	 * @return bool
	 * @author GuGo
	 */
	public function store($local, $remote = null, $mode = FTP_BINARY)
	{
		set_error_handler('_cubix_images_ftp_error_handler', E_ALL);
		
		if ( ! is_file($local) ) {
			throw new Cubix_Images_Storage_Exception('The local file does not exist');
		}
		
		if ( is_null($remote) ) {
			$remote = basename($local);
		}
		
		if ( ! ftp_put($this->_conn, $remote, $local, $mode) ) {
			throw new Cubix_Images_Storage_Exception('Could not upload the file');
		}

		//$this->makeExecutable($remote);
		
		restore_error_handler();
		
		return TRUE;
	}
	
	/**
	 * Uploads a file from local server to remote ftp storage
	 *
	 * @param string $local 
	 * @param string $remote 
	 * @param FTP_BINARY|FTP_ASCII $mode
	 * @return bool
	 * @author GuGo
	 */
	public function copy($from, $to, $remote = null, $mode = FTP_BINARY)
	{
		set_error_handler('_cubix_images_ftp_error_handler', E_ALL);
		
		$tmp_dir = sys_get_temp_dir();
		$unique = uniqid();
		$tempFile = $tmp_dir."/".$unique."tmp"; 

		if(ftp_get($this->_conn, $tempFile, $from, $mode)){
			if ( ftp_put($this->_conn, $to, $tempFile, $mode) ) {
				unlink($tempFile);
			}else{
				throw new Cubix_Images_Storage_Exception('Could not upload the file');
			}
		}else{
        	throw new Cubix_Images_Storage_Exception("Can't copy file");	
		}
		
		restore_error_handler();
		
		return TRUE;
	}

	/**
	 * Downloads a file from remote ftp storage
	 *
	 * @param string $local 
	 * @param string $remotem 
	 * @param FTP_BINARY|FTP_ASCII $mdoe 
	 * @return bool
	 * @author GuGo
	 */
	public function retreive($local, $remote, $mode = FTP_BINARY)
	{
		if ( ! ftp_get($this->_conn, $local, $remote, $mode) ) {
			throw new Cubix_Images_Storage_Exception('Could not download the file');
		}
		
		return TRUE;
	}
	
	/**
	 * Deletes remote file on the ftp server
	 *
	 * @param string $remote 
	 * @return bool
	 * @author GuGo
	 */
	public function delete($remote)
	{
		if ( ! ftp_delete($this->_conn, $remote) ) {
			throw new Cubix_Images_Storage_Exception('Unable to delete remote file');
		}
		
		return TRUE;
	}
	
	/**
	 * Returns the list of items in the path
	 *
	 * @param string $remote 
	 * @return array
	 * @author GuGo
	 */
	public function getFiles($remote)
	{
		$list = ftp_nlist($this->_conn, trim($remote, '/') . '/');
		
		if ( $list === FALSE ) {
			throw new Cubix_Images_Storage_Exception('Error getting files list');
		}
		
		return $list;
	}
	
	/**
	 * Creates remote directory on the ftp server
	 *
	 * @param string $remote 
	 * @return void
	 * @author GuGo
	 */
	public function makeDir($remote)
	{
		if ( ! @ftp_mkdir($this->_conn, $remote) ) {
			throw new Cubix_Images_Storage_Exception('Unable to create remote directory');
		}
		
		$this->makeWritable($remote);
		
		return TRUE;
	}
	
	/**
	 * Chmods the target to 0777 
	 *
	 * @param string $remote 
	 * @return bool
	 * @author GuGo
	 */
	public function makeWritable($remote)
	{
		if ( ! @ftp_site($this->_conn, 'CHMOD 0777 ' . $remote) ) {
			throw new Cubix_Images_Storage_Exception('Could not chmod the remote file');
		}
		
		return TRUE;
	}

	/**
	 * Chmods the target to 0775
	 *
	 * @param string $remote
	 * @return bool
	 * @author GuGo
	 */
	public function makeExecutable($remote)
	{
		if ( ! @ftp_site($this->_conn, 'CHMOD 0775 ' . $remote) ) {
			throw new Cubix_Images_Storage_Exception('Could not chmod the remote file');
		}

		return TRUE;
	}
	
	/**
	 * Disconnects from ftp server
	 *
	 * @return bool
	 * @author GuGo
	 */
	public function disconnect()
	{
		if ( $this->_conn === FALSE ) {
			throw new Cubix_Images_Storage_Exception('Not connected to ftp storage');
		}
		
		ftp_close($this->_conn);
	}

	public function getFileSize($remote)
	{
		return ftp_size($this->_conn, $remote);
	}
}
