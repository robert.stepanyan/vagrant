<?php
/* only for 6anuncio.com */

class Cubix_SMS6B
{
	var $user;
	var $password;
	var $msg;
	var $externalkey;
	var $destination;
	var $hasError;


	public function __construct($u, $p)
  	{
    	$this->user = $u;
	    $this->password = $p;
	    $this->hasError = false;
	}

	public function setDestination($number, $id = null)
	{
		if (substr($number, 0, 4) == '0055')
			$number = substr($number, 4);
		else
			$this->hasError = true;

		$this->destination = $number;
		$this->externalkey = $id;
	}

	private function _umlautFilter(&$string)
	{
		$ar = array("ä"=>"&#228;", "ö"=>"&#246;", "ü"=>"&#252;", "ß"=>"&#223;",
			"Ä"=>"&#196;", "Ö"=>"&#214;", "Ü"=>"&#220;", "À"=>"&#192;",
			"È"=>"&#200;", "É"=>"&#201;", "Ê"=>"&#202;", "è"=>"&#232;",
			"é"=>"&#233;", "ê"=>"&#234;", "à"=>"&#224;", "ï"=>"&#239;",
			"ù"=>"&#249;", "û"=>"&#251;", "ü"=>"&#252;", "ç"=>"&#231;",
			"Æ"=>"&#198;", "?"=>"&#330;", ""=>"&#338;", ""=>"&#339;",
			""=>"&#8364;","«"=>"&#171;", "»"=>"&#187;"
		);

		foreach($ar as $key=>$val)
		{
			$string = str_replace( $key, $val, $string );
		}
	}

	public function setContent($msg)
  	{
		$this->_umlautFilter($msg);
		
    	$this->msg = urlencode($msg);
  	}

	public function send()
  	{
		$url = 'https://www.facilitamovel.com/api/simpleSend.ft?user=' . $this->user . '&password=' . $this->password . '&destinatario=' . $this->destination . '&msg=' . $this->msg;

	    if ($this->externalkey)
		    $url .= '&externalkey=' . $this->externalkey;

	    if (!$this->hasError)
	        $ret = file_get_contents($url);

	    return $ret;
  	}

	public function checkMessageStatus($externalkey)
	{
		$url = 'https://www.facilitamovel.com/api/dlrByExternalKey.ft?user=' . $this->user . '&password=' . $this->password . '&externalkey=' . $externalkey;

		$ret = file_get_contents($url);

		return $ret;
	}
}


