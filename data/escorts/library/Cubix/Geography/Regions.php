<?php

class Cubix_Geography_Regions extends Cubix_Model
{
	protected $_table = 'regions';
	
	protected $_itemClass = 'Cubix_Geography_RegionItem';
	
	/*public function getAll($country_id, $p, $pp, $sort_field, $sort_dir, &$count)
	{
		$count = self::getAdapter()->fetchOne('
			SELECT COUNT(id) FROM regions 
			WHERE country_id = ?
		', $country_id);
		
		return parent::_fetchAll('
			SELECT id, ' . Cubix_I18n::getTblField('title') . ', state FROM regions
			WHERE country_id = ?
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		', $country_id);
	}*/
	
	public function getRegionBySlug($slug)
	{
		$cache = Zend_Registry::get('cache');
		$new_cache_key = "region_cache_by_slug_" . $slug;
                $new_cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $new_cache_key);
		if ( ! $region = $cache->load($new_cache_key) ) {			
			$sql = 'SELECT id, ' . Cubix_I18n::getTblField('title') . ' AS title FROM regions WHERE slug = ?';		
			$region = parent::_fetchRow($sql, $slug);
			$cache->save($region, $new_cache_key, array());
		}
		
		return $region;
		
	}
	
	public function getTopRegions($gender = 1, $type = false, &$count = 0)
	{
		/*$regions = parent::_fetchAll("SELECT id, slug FROM regions");
		foreach($regions as $region)
		{
			$slug = str_replace('-', '_', $region->slug);
			parent::getAdapter()->query('UPDATE regions SET slug = ? WHERE id = ?', array($slug, $region->id));
		}
		die;*/

		$where = " AND e.gender = ? ";
		$country_id = Cubix_Application::getById()->country_id;
		if ( $type )
		{
			if ( $type == USER_TYPE_AGENCY )
				$where .= " AND e.agency_id IS NOT NULL ";
			else if ( $type == USER_TYPE_SINGLE_GIRL )
				$where .= " AND e.agency_id IS NULL ";
		}

		// <editor-fold defaultstate="collapsed" desc="6annonce.ch Specific Code">
//		if ( substr(Cubix_Application::getById()->host, 0, 11) == '6annonce.ch' ) {
//			$where .= ' AND r.is_french = 1 AND ct.is_french = 1';
//		}
		// </editor-fold>

		$base_sql = "
			SELECT
				r.id AS region_id, r.slug AS region_slug, r." . Cubix_I18n::getTblField('title') . " AS region_title, c.slug AS country_slug, c.iso AS country_iso,
				COUNT(e.id) as escort_count
			FROM escorts AS e

			INNER JOIN escort_cities ec ON ec.escort_id = e.id
			LEFT JOIN upcoming_tours ut ON ut.id = e.id
	    	INNER JOIN cities ct ON %cities_condition%
			INNER JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = r.country_id

			WHERE
				ct.country_id = ?
				{$where}
			GROUP BY r.id
		";

		$sql = "
	    	SELECT x.*, SUM(x.escort_count) AS escort_count FROM ((
			" . str_replace('%cities_condition%', "(ct.id = ec.city_id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_NATIONAL_LISTING . ", e.products) > 0 AND e.is_on_tour = 0)", $base_sql) . "
			) UNION (
			" . str_replace('%cities_condition%', "(IF (e.is_on_tour = 1, e.tour_city_id, ut.tour_city_id) = ct.id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_TOUR_ABILITY . ", e.products) > 0)", $base_sql) . "
			)) AS x
			GROUP BY x.region_id
	    ";
/*echo $sql;
echo $id . "<br/>";
echo $gender;die;*/
		
		return parent::_fetchAll($sql, array($country_id, $gender, $country_id /*, Cubix_Application::getById(Cubix_Application:: getId())->country_id*/, $gender));
		
		$countSql = "
			SELECT COUNT(DISTINCT(e.id)) AS escort_count FROM escorts AS e

			INNER JOIN escort_cities ec ON ec.escort_id = e.id
			/*INNER JOIN escort_products eprod ON eprod.escort_id = e.id*/

			INNER JOIN cities ct ON (ct.id = e.city_id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_NATIONAL_LISTING . ", e.products) > 0 AND e.is_on_tour = 0) OR (e.tour_city_id = ct.id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_TOUR_ABILITY . ", e.products) > 0 )
			INNER JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = r.country_id

			WHERE
				ct.country_id = ? {$where} AND e.is_on_tour = 0		
		";

		$count = parent::_fetchRow($countSql, array(Cubix_Application::getById(Cubix_Application::getId())->country_id, $gender))->escort_count;

		$sql = "
			SELECT COUNT(DISTINCT(e.id)) AS escort_count, r.id AS region_id, r.slug AS region_slug, r." . Cubix_I18n::getTblField('title') . " AS region_title, c.slug AS country_slug, c.iso AS country_iso FROM escorts AS e

			/*INNER JOIN escort_products eprod ON eprod.escort_id = e.id*/
			INNER JOIN escort_cities ec ON ec.escort_id = e.id
			
			INNER JOIN cities ct ON (ct.id = e.city_id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_NATIONAL_LISTING . ", e.products) > 0 AND e.is_on_tour = 0) OR (e.tour_city_id = ct.id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_TOUR_ABILITY . ", e.products) > 0 )
			INNER JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = r.country_id
			
			WHERE
				ct.country_id = ? {$where} AND e.is_on_tour = 0
			GROUP BY r.id
			ORDER BY region_title ASC
		";
		
		$regions = parent::_fetchAll($sql, array(Cubix_Application::getById(Cubix_Application::getId())->country_id, $gender));
		
		$config = Zend_Registry::get('escorts_config');
		
		if ( ! $config['showCities'] )
		{
			foreach ($regions as $region)
			{
				$region_cities[$region->region_id] = array('region' => $region);
			}
			
			return $region_cities;
		}
		
		$sql = "
			SELECT COUNT(DISTINCT(e.id)) AS escort_count, ct.is_important, ct.id AS city_id, ct.slug AS city_slug, ct." . Cubix_I18n::getTblField('title') . " AS city_title, r.id AS region_id FROM escorts AS e

			INNER JOIN escort_products eprod ON eprod.escort_id = e.id AND eprod.product_id = " . Model_Escorts::PRODUCT_NATIONAL_LISTING . "
			INNER JOIN escort_cities ec ON ec.escort_id = e.id
			
			INNER JOIN cities ct ON ct.id = ec.city_id OR e.tour_city_id = ct.id
			INNER JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = r.country_id
			
			WHERE
				ct.country_id = ? {$where}
			GROUP BY ct.id
			ORDER BY escort_count DESC
		";
		
		$cities = parent::_fetchAll($sql, array(Cubix_Application::getById(Cubix_Application:: getId())->country_id, $gender));
		
		$region_cities = array();
		
		foreach ($regions as $region)
		{
			$region_cities[$region->region_id] = array('region' => $region);
			
			foreach($cities as $city)
			{
				if($region->region_id == $city->region_id)
				{
					$region_cities[$region->region_id]['cities'][] = $city;
				}
			}
		}
		
		return $region_cities;
	}
	
	public function ajaxGetAll($country_id)
	{
		return parent::_fetchAll('
			SELECT id, ' . Cubix_I18n::getTblField('title') . ' AS title, slug FROM regions
			WHERE country_id = ?
			ORDER BY title ASC
		', $country_id);
	}
	
	public function getRegionsByCountryId($country_id, $excl_slug)
	{
		$cache = Zend_Registry::get('cache');
		$new_cache_key = "regions_cache_" . $country_id . $excl_slug;
		if ( ! $regions = $cache->load($new_cache_key) ) {
			$regions = parent::_fetchAll('
				SELECT id, ' . Cubix_I18n::getTblField('title') . ' AS title, slug FROM regions
				WHERE country_id = ? AND slug <> ?
				ORDER BY title ASC
			', array($country_id, $excl_slug));
			$cache->save($regions, $new_cache_key, array());
		}
		
		return $regions;
	}
	
	public function get($id)
	{
		$cache = Zend_Registry::get('cache');
		$new_cache_key = "region_cache_" . $id;
		if ( ! $region = $cache->load($new_cache_key) ) {
			$region = parent::_fetchRow('
				SELECT id, slug, country_id, ' . Cubix_I18n::getTblFields('title') . ', state FROM regions
				WHERE id = ?
			', $id);
			
			$cache->save($region, $new_cache_key, array());
		}
		
		return $region;
	}
}
