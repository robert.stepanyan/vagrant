<?php

class Cubix_Geography_CityzoneItem extends Cubix_Model_Item
{
	public function getCity()
	{
		$m = new Cubix_Geography_Cities();
		return $m->get($this->city_id);
	}
}
