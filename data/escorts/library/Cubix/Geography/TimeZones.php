<?php

class Cubix_Geography_TimeZones extends Cubix_Model
{
	protected $_table = 'time_zones';
	
	protected $_itemClass = 'Cubix_Geography_TimeZoneItem';
		
	public function getAll()
	{
		return parent::_fetchAll('SELECT id, title, shift FROM time_zones');
	}
}
