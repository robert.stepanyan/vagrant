<?php

class Cubix_Geography_Cities extends Cubix_Model
{
    const FILTER_AT_LEAST_ONE_ESCORT   = 1; // 00000000000001

	protected $_table = 'cities';
	
	protected $_itemClass = 'Cubix_Geography_CityItem';
	
	/*public function getAll($region_id, $country_id, $p, $pp, $sort_field, $sort_dir, &$count)
	{
		$count = self::getAdapter()->fetchOne('
			SELECT COUNT(id) FROM cities 
			WHERE ' . ($region_id ? 'region_id = ?' : 'country_id = ?') . '
		', $region_id ? $region_id : $country_id);
		
		return parent::_fetchAll('
			SELECT id, country_id, region_id, ' . Cubix_I18n::getTblField('title') . ' FROM cities
			WHERE ' . ($region_id ? 'region_id = ?' : 'country_id = ?') . '
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		', $region_id ? $region_id : $country_id);
	}*/

	static public function getTitleById($id)
	{
		return self::getAdapter()->fetchOne('
			SELECT ' . Cubix_I18n::getTblField('title') . ' AS title FROM cities WHERE id = ?
		', $id);
	}
	
	static public function getTitleBySlug($slug)
	{
		return self::getAdapter()->fetchOne('
			SELECT ' . Cubix_I18n::getTblField('title') . ' AS title FROM cities WHERE slug = ?
		', $slug);
	}

    static public function getCountryIdBySlug($slug)
    {
        return self::getAdapter()->fetchOne('
			SELECT country_id AS c_id FROM cities WHERE slug = ?
		', $slug);
    }

    static public function getCountryIdByCityId($city_id)
    {
        return self::getAdapter()->fetchOne('
			SELECT country_id AS c_id FROM cities WHERE id = ?
		', $city_id);
    }

    static public function getIdBySlug($slug)
    {
        return self::getAdapter()->fetchOne('
			SELECT id AS id FROM cities WHERE slug = ?
		', $slug);
    }

	static public function isFromApplicationCountry($city_id)
	{
		return self::getAdapter()->fetchOne('
			SELECT id FROM cities WHERE id = ? AND country_id = ?
		', array($city_id, Cubix_Application::getById()->country_id));
	}

	static public function isFromCountry($city_id, $country_id)
	{
		return self::getAdapter()->fetchOne('
			SELECT TRUE FROM cities WHERE id = ? AND country_id = ?
		', array($city_id, $country_id));
	}
	public function getCitiesByEscortID( $escort_id ){
		return self::getAdapter()->fetchAll('
			SELECT 
				c.id, c.' . Cubix_I18n::getTblFields('title') . ' AS title
			FROM escort_cities ec
			INNER JOIN cities c ON c.id = ec.city_id
			WHERE ec.escort_id = ?
		', array($escort_id));
	}
	public function getBySlug($slug)
	{
		$cache = Zend_Registry::get('cache');
		$new_cache_key = "city_cache_by_slug_" . $slug . Cubix_Application::getId() . Cubix_I18n::getLang();
		$new_cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $new_cache_key);
		if ( ! $city = $cache->load($new_cache_key) ) {			
			$sql = '
				SELECT
					c.id, c.slug AS city_slug, c.url_slug AS city_url_slug, cr.iso AS country_iso,
					c.region_id, c.' . Cubix_I18n::getTblField('title') . ' AS title, cr.' . Cubix_I18n::getTblField('title') . ' AS country_title,
					r.slug AS region_slug
				FROM
					cities c
				INNER JOIN countries cr ON cr.id = c.country_id
				LEFT JOIN regions r ON r.id = c.region_id
				WHERE c.slug = ?
			';

			// parent::_fetchRow('SET NAMES `utf8`');

			$city = parent::_fetchRow($sql, $slug);
			
			$cache->save($city, $new_cache_key, array());
		}
		
		return $city;
	}
	
	public function getByUrlSlug($url_slug)
	{
		$cache = Zend_Registry::get('cache');
		$new_cache_key = "city_cache_by_slug_" . $url_slug;
		$new_cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $new_cache_key);
		if ( ! $city = $cache->load($new_cache_key) ) {			
			$sql = '
				SELECT
					c.id, c.slug AS city_slug, cr.iso AS country_iso,
					c.region_id, c.' . Cubix_I18n::getTblField('title') . ' AS title,
					r.slug AS region_slug
				FROM
					cities c
				INNER JOIN countries cr ON cr.id = c.country_id
				LEFT JOIN regions r ON r.id = c.region_id
				WHERE c.url_slug = ?
			';

			$city = parent::_fetchRow($sql, $url_slug);
			
			$cache->save($city, $new_cache_key, array());
		}
		
		return $city;
	}
	
	public function getByRegionId($id, $gender = 1, $type = false)
	{
		$where = " AND e.gender = ? ";
		
		if ( $type )
		{
			if ( $type == USER_TYPE_AGENCY )
				$where .= " AND e.agency_id IS NOT NULL ";
			else if ( $type == USER_TYPE_SINGLE_GIRL )
				$where .= " AND e.agency_id IS NULL ";
		}

		// <editor-fold defaultstate="collapsed" desc="6annonce.ch Specific Code">
//		if ( substr(Cubix_Application::getById()->host, 0, 11) == '6annonce.ch' ) {
//			$where .= ' AND r.is_french = 1 AND ct.is_french = 1';
//		}
		// </editor-fold>

		$base_sql = "
			SELECT c.iso AS country_iso, ct.id, ct.slug, ct." . Cubix_I18n::getTblField('title') . " AS title, COUNT(e.id) as escort_count FROM escorts AS e

			INNER JOIN escort_cities ec ON ec.escort_id = e.id
			LEFT JOIN upcoming_tours ut ON ut.id = e.id 
	    	INNER JOIN cities ct ON %cities_condition%
			INNER JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = r.country_id

			WHERE
				r.id = ?
				/*AND e.country_id = ?*/ {$where}
			GROUP BY ct.id
		";

		$sql = "
	    	SELECT x.*, SUM(x.escort_count) AS escort_count FROM ((
			" . str_replace('%cities_condition%', "(ct.id = ec.city_id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_NATIONAL_LISTING . ", e.products) > 0 AND e.is_on_tour = 0)", $base_sql) . "
			) UNION (
			" . str_replace('%cities_condition%', "(IF (e.is_on_tour = 1, e.tour_city_id, ut.tour_city_id) = ct.id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_TOUR_ABILITY . ", e.products) > 0)", $base_sql) . "
			)) AS x
			GROUP BY x.id
			ORDER BY x.title ASC
	    ";
/*echo $sql;
echo $id . "<br/>";
echo $gender;*/
		return parent::_fetchAll($sql, array($id, $gender, $id /*, Cubix_Application::getById(Cubix_Application:: getId())->country_id*/, $gender));
	}
	
	public function getPopularCities($count, $gender = 1, $type = false)
	{
		static $cities;
		
		if ( ! empty($cities) ) {
			return $cities;
		}	
		
		$where = " AND e.gender = ? ";
		
		if ( $type )
		{
			if ( $type == USER_TYPE_AGENCY )
				$where .= " AND e.agency_id IS NOT NULL ";
			else if ( $type == USER_TYPE_SINGLE_GIRL )
				$where .= " AND e.agency_id IS NULL ";
		}

		// <editor-fold defaultstate="collapsed" desc="6annonce.ch Specific Code">
//		if ( substr(Cubix_Application::getById()->host, 0, 11) == '6annonce.ch' ) {
//			$where .= ' AND r.is_french = 1 AND ct.is_french = 1';
//		}
		// </editor-fold>

		$base_sql = "SELECT
	    		ct.id AS city_id, c.iso AS country_iso, c.slug AS country_slug, 
	    		ct.slug AS city_slug,
	    		ct." . Cubix_I18n::getTblField('title') . " AS city_title,
	    		COUNT(DISTINCT(e.id)) as escort_count
	    	FROM escorts AS e


			INNER JOIN escort_cities ec ON ec.escort_id = e.id
			LEFT JOIN upcoming_tours ut ON ut.id = e.id
	    	INNER JOIN cities ct ON %cities_condition%

			LEFT JOIN countries c ON c.id = ct.country_id

			WHERE
				ct.country_id = ? {$where}
			GROUP BY ct.id
		";
 
		$sql = "
	    	SELECT x.*, SUM(x.escort_count) AS escort_count FROM ((
			" . str_replace('%cities_condition%', "(ct.id = ec.city_id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_NATIONAL_LISTING . ", e.products) > 0 AND e.is_on_tour = 0)", $base_sql) . "
			) UNION (
			" . str_replace('%cities_condition%', "(IF (e.is_on_tour = 1, e.tour_city_id, ut.tour_city_id) = ct.id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_TOUR_ABILITY . ", e.products) > 0)", $base_sql) . "
			)) AS x
			GROUP BY x.city_id
			ORDER BY escort_count DESC
			LIMIT ?
	    ";

	   	$cities = parent::_fetchAll($sql, array(Cubix_Application::getById(Cubix_Application:: getId())->country_id, $gender, Cubix_Application::getById(Cubix_Application:: getId())->country_id, $gender, $count));
   
	    return $cities;
	}
	
	public function getTourCities($is_upcoming = false)
	{
		if( $is_upcoming ) {
			$sql = "
				SELECT
					cr.slug AS country_slug, cr.iso AS country_iso, c.id, c.slug AS city_slug, r.slug AS region_slug, " . Cubix_I18n::getTblField('c.title') . " AS city_title, " . Cubix_I18n::getTblField('r.title') . " AS region_title,
					COUNT(DISTINCT(ut.id)) AS escort_count
				FROM upcoming_tours ut
				INNER JOIN escorts e ON e.id = ut.id
				INNER JOIN cities c ON (c.id = ut.tour_city_id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_TOUR_ABILITY . ", e.products) > 0)/*c.id = ut.tour_city_id*/
				INNER JOIN regions r ON r.id = c.region_id
				INNER JOIN countries cr ON cr.id = c.country_id
				WHERE DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY /*AND e.gender = 1*/
				GROUP BY c.id
				ORDER BY city_title ASC
			";
		}
		else {
			$sql = "
				SELECT
					cr.slug AS country_slug, cr.iso AS country_iso, c.id, c.slug AS city_slug, r.slug AS region_slug, " . Cubix_I18n::getTblField('c.title') . " AS city_title, " . Cubix_I18n::getTblField('r.title') . " AS region_title,
					COUNT(DISTINCT(e.id)) AS escort_count
				FROM escorts e
				
				INNER JOIN cities c ON (c.id = e.tour_city_id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_TOUR_ABILITY . ", e.products) > 0)/*c.id = e.tour_city_id*/
				INNER JOIN regions r ON r.id = c.region_id
				INNER JOIN countries cr ON cr.id = c.country_id
				WHERE e.is_on_tour = 1 /*AND e.gender = 1*/
				GROUP BY c.id
				ORDER BY city_title ASC
			";
		}
		
		return parent::_fetchAll($sql);
	}
	
	/*public function ajaxGetAll($region_id, $country_id)
	{
		return parent::_fetchAll('
			SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, ' . Cubix_I18n::getTblField('r.title') . ' AS region_title FROM cities c
			LEFT JOIN regions r ON c.region_id = r.id
			WHERE ' . ($region_id ? 'c.region_id = ?' : '(c.country_id = ? OR r.country_id = ?)') . '
			GROUP BY c.id
			ORDER BY title ASC
		', $region_id ? $region_id : array($country_id, $country_id));
	}*/

    /**
     * Returns all the cities with matching criteria.
     *
     * @param int $region_id
     * @param int $country_id
     * @param array $filters
     * @return array
     */
	public function ajaxGetAll($region_id = 0, $country_id = 0, $filters = [])
	{
        $joins = $fieldsToSelect = $groupedFilters = '';

        foreach ($filters as $filter) {
            switch ($filter) {
                case self::FILTER_AT_LEAST_ONE_ESCORT:
                    $fieldsToSelect .= 'count(eic.escort_id) as escorts_count';
                    $groupedFilters .= 'escorts_count >= 1';
                    $joins          .= 'LEFT JOIN escorts_in_cities eic ON eic.city_id = c.id';
                    break;
            }
        }

        if (!empty($groupedFilters)) $groupedFilters = ' HAVING ' . $groupedFilters;
        if (!empty($fieldsToSelect)) $fieldsToSelect = ' , ' . $fieldsToSelect;

        $sql = '
			SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, ' . Cubix_I18n::getTblField('r.title') . ' AS region_title, c.region_id as region_id, co.has_regions, c.slug 
			' . $fieldsToSelect . '
			FROM cities c
			LEFT JOIN regions r ON r.id = c.region_id
			LEFT JOIN countries co ON co.id = c.country_id
			' . $joins . '
			WHERE c.country_id = ?
			GROUP BY c.id
			' . $groupedFilters . '
			ORDER BY title ASC
		';

        return parent::_fetchAll($sql, $country_id);
	}
	
	public function ajaxGetByregId($region_id, $country_id)
	{
		return parent::_fetchAll('
			SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, ' . Cubix_I18n::getTblField('r.title') . ' AS region_title, co.has_regions, c.slug FROM cities c
			LEFT JOIN regions r ON r.id = c.region_id
			LEFT JOIN countries co ON co.id = c.country_id
			WHERE c.country_id = ? AND c.region_id = ?
			GROUP BY c.id
			ORDER BY title ASC
		', array($country_id, $region_id) );
	}

	public function ajaxGetAllClassifiedAds($region_id, $country_id)
	{
		return parent::_fetchAll('
			SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, ' . Cubix_I18n::getTblField('r.title') . ' AS region_title, co.has_regions, c.slug
			FROM cities c
			LEFT JOIN regions r ON r.id = c.region_id
			LEFT JOIN countries co ON co.id = c.country_id
			INNER JOIN classified_ads_cities cac ON cac.city_id = c.id
			WHERE c.country_id = ?
			GROUP BY c.id
			ORDER BY title ASC
		', $country_id);
	}

	public function ajaxGetAllCities($city_name, $lng)
	{
		return parent::_fetchAll('
			SELECT 
				c.id, c.title_' . $lng . ' AS title, co.title_' . $lng . ' AS country_title
			FROM cities c
			INNER JOIN countries co ON co.id = c.country_id
			WHERE c.title_' . $lng . ' LIKE ?
			GROUP BY c.id
			ORDER BY title ASC
		', array($city_name . '%'));
	}

    /**
     * Selects cities that has at least one approved review
     *
     * @param int $country_id
     * @return mixed
     */
    public function ajaxGetAllReviews($country_id)
    {

        return parent::_fetchAll('
			SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, co.has_regions, c.slug FROM cities c
			INNER JOIN reviews r ON r.city = c.id
			INNER JOIN countries co ON co.id = c.country_id
			INNER JOIN escorts e on r.escort_id = e.id
			WHERE c.country_id = ?
			AND e.disabled_reviews != 1 
			AND r.status = '. REVIEW_APPROVED .'
			AND r.is_deleted = 0
			GROUP BY c.id
			ORDER BY title ASC
		', $country_id);
	}

    /**
     * Selects cities that has at least one approved comment
     *
     * @param int $country_id
     * @return mixed
     */
    public function ajaxGetAllComments($country_id)
    {
        return parent::_fetchAll('
			SELECT c.id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, co.has_regions, c.slug FROM cities c
			INNER JOIN comments cmt ON cmt.e_city_id = c.id
			INNER JOIN countries co ON co.id = c.country_id
            INNER JOIN escorts as e ON e.id = cmt.escort_id
			WHERE cmt.e_country_id = ?
			AND cmt.status = '. COMMENT_ACTIVE .'
            AND cmt.disabled_comments = 0
			GROUP BY c.id
			ORDER BY title ASC
		', $country_id);
    }

	public function ajaxSearch($name, $country_id)
	{
		$base_sql = "SELECT
	    		ct.id AS id, 
	    		r." . Cubix_I18n::getTblField('title') . " AS region_name, ct.slug AS slug,
	    		ct." . Cubix_I18n::getTblField('title') . " AS name,
	    		COUNT(DISTINCT(e.id)) as escort_count
	    	FROM escorts AS e


			INNER JOIN escort_cities ec ON ec.escort_id = e.id
			LEFT JOIN upcoming_tours ut ON ut.id = e.id
	    	INNER JOIN cities ct ON %cities_condition%

			INNER JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = r.country_id

			WHERE
				ct.country_id = ? AND ct.title_en LIKE ? AND e.gender = 1
			GROUP BY ct.id
		";

		$sql = "
	    	SELECT x.*, SUM(x.escort_count) AS escort_count FROM ((
			" . str_replace('%cities_condition%', "(ct.id = ec.city_id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_NATIONAL_LISTING . ", e.products) > 0 AND e.is_on_tour = 0)", $base_sql) . "
			) UNION (
			" . str_replace('%cities_condition%', "(IF (e.is_on_tour = 1, e.tour_city_id, ut.tour_city_id) = ct.id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_TOUR_ABILITY . ", e.products) > 0)", $base_sql) . "
			)) AS x
			GROUP BY x.id
			ORDER BY escort_count DESC
			LIMIT 30
	    ";



	   	$cities = parent::_fetchAll($sql, array($country_id, $name . '%', $country_id, $name . '%'));

	    return $cities;

		return parent::_fetchAll('
			SELECT
				c.id, c.slug, ' . Cubix_I18n::getTblField('c.title') . ' AS name, ' . Cubix_I18n::getTblField('r.title') . ' AS region_name, COUNT(DISTINCT(e.id)) AS escort_count
			FROM cities c
			INNER JOIN escort_cities ec ON ec.city_id = c.id
			INNER JOIN escorts e ON e.city_id = ec.city_id
			LEFT JOIN regions r ON r.id = c.region_id
			LEFT JOIN countries co ON co.id = c.country_id
			WHERE c.country_id = ? AND c.title_en LIKE ? AND e.gender = 1
			GROUP BY c.id
			ORDER BY name ASC
			LIMIT 30
		', array($country_id, $name . '%'));
	}

	public function ajaxSearchArray($country_id, $gender = 1)
	{
		$base_sql = "SELECT
	    		ct.id AS id,
	    		r." . Cubix_I18n::getTblField('title') . " AS region_name, ct.slug AS slug,
	    		ct." . Cubix_I18n::getTblField('title') . " AS name,
	    		COUNT(DISTINCT(e.id)) as escort_count
	    	FROM escorts AS e


			INNER JOIN escort_cities ec ON ec.escort_id = e.id
			LEFT JOIN upcoming_tours ut ON ut.id = e.id
	    	INNER JOIN cities ct ON %cities_condition%

			INNER JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = r.country_id

			WHERE
				ct.country_id = ? AND e.gender = " . $gender . "
			GROUP BY ct.id
		";

		$sql = "
	    	SELECT x.*, SUM(x.escort_count) AS escort_count FROM ((
			" . str_replace('%cities_condition%', "(ct.id = ec.city_id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_NATIONAL_LISTING . ", e.products) > 0 AND e.is_on_tour = 0)", $base_sql) . "
			) UNION (
			" . str_replace('%cities_condition%', "(IF (e.is_on_tour = 1, e.tour_city_id, ut.tour_city_id) = ct.id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_TOUR_ABILITY . ", e.products) > 0)", $base_sql) . "
			)) AS x
			GROUP BY x.id
			ORDER BY escort_count DESC
	    ";



	   	$cities = parent::_fetchAll($sql, array($country_id, $country_id));

	    return $cities;
	}

	public function get($id)
	{
		return parent::_fetchRow('
			SELECT 
				c.id, c.slug, c.url_slug, c.country_id, c.region_id, ' . Cubix_I18n::getTblFields('c.title') . ' AS title,
				cr.iso AS country_iso, r.slug AS region_slug, ' . Cubix_I18n::getTblFields('c.title') . '
			FROM cities c
			INNER JOIN countries cr ON cr.id = c.country_id
			LEFT JOIN regions r ON r.id = c.region_id
			WHERE c.id = ?
		', $id);
	}
	
	public function getRegionId($city_id)
	{
		return parent::_fetchRow('
			SELECT region_id FROM cities
			WHERE id = ?
		', $city_id);
	}

	public function getTopCities($gender = 1, $type = false, &$count = 0)
	{
		$where = " AND e.gender = ? ";
		$country_id = 71;
		if ( $type )
		{
			if ( $type == USER_TYPE_AGENCY ) {
				$where .= " AND e.agency_id IS NOT NULL ";
			}
			else if ( $type == USER_TYPE_SINGLE_GIRL ) {
				$where .= " AND e.agency_id IS NULL ";
			}
		}

		$base_sql = "
			SELECT
				COUNT(DISTINCT(e.id)) AS escort_count, ct.is_important, ct.id AS city_id,
				ct.slug AS city_slug, ct." . Cubix_I18n::getTblField('title') . " AS city_title,
				r.id AS region_id, c.iso AS country_iso
			FROM escorts AS e

			INNER JOIN escort_cities ec ON ec.escort_id = e.id
			LEFT JOIN upcoming_tours ut ON ut.id = e.id
	    	INNER JOIN cities ct ON %cities_condition%
			INNER JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = r.country_id

			WHERE
				ct.country_id = ?
				{$where}
			GROUP BY ct.id
		";

		$sql = "
	    	SELECT x.*, SUM(x.escort_count) AS escort_count FROM ((
			" . str_replace('%cities_condition%', "(ct.id = ec.city_id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_NATIONAL_LISTING . ", e.products) > 0 AND e.is_on_tour = 0)", $base_sql) . "
			) UNION (
			" . str_replace('%cities_condition%', "(IF (e.is_on_tour = 1, e.tour_city_id, ut.tour_city_id) = ct.id AND FIND_IN_SET(" . Model_Escorts::PRODUCT_TOUR_ABILITY . ", e.products) > 0)", $base_sql) . "
			)) AS x
			GROUP BY x.city_id
			ORDER BY escort_count DESC
			/*LIMIT 24*/
	    ";

		return parent::getAdapter()->fetchAll($sql, array($country_id, $gender, $country_id /*, Cubix_Application::getById(Cubix_Application:: getId())->country_id*/, $gender));

		/*$sql = "
			SELECT
				COUNT(DISTINCT(e.id)) AS escort_count, ct.is_important, ct.id AS city_id,
				ct.slug AS city_slug, ct." . Cubix_I18n::getTblField('title') . " AS city_title,
				r.id AS region_id, c.iso AS country_iso
			FROM escorts e

			INNER JOIN escort_products eprod ON eprod.escort_id = e.id
			INNER JOIN escort_cities ec ON ec.escort_id = e.id

			INNER JOIN cities ct ON (ct.id = ec.city_id AND eprod.product_id = " . Model_EscortsV2::PRODUCT_NATIONAL_LISTING . ") OR (e.tour_city_id = ct.id AND eprod.product_id = " . Model_EscortsV2::PRODUCT_TOUR_ABILITY . ")
			INNER JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = r.country_id

			WHERE
				ct.country_id = ? {$where}
			GROUP BY ct.id
			ORDER BY city_title ASC
		";

		$cities = parent::_fetchAll($sql, array(Cubix_Application::getById(Cubix_Application:: getId())->country_id, $gender));

		$sql = "
			SELECT SUM(x.count) FROM (
				SELECT
					COUNT(DISTINCT(e.id)) AS count
				FROM escorts e

				INNER JOIN escort_products eprod ON eprod.escort_id = e.id
				INNER JOIN escort_cities ec ON ec.escort_id = e.id

				INNER JOIN cities ct ON (ct.id = ec.city_id AND eprod.product_id = " . Model_EscortsV2::PRODUCT_NATIONAL_LISTING . ") OR (e.tour_city_id = ct.id AND eprod.product_id = " . Model_EscortsV2::PRODUCT_TOUR_ABILITY . ")
				INNER JOIN regions r ON r.id = ct.region_id
				INNER JOIN countries c ON c.id = r.country_id

				WHERE
					ct.country_id = ? {$where}
				GROUP BY ct.region_id
			) x
		";

		$count = parent::getAdapter()->fetchOne($sql, array(Cubix_Application::getById(Cubix_Application::getId())->country_id, $gender));

		return $cities;*/
	}

	public static function getByCountryId($country_id)
	{
		$sql = '
			SELECT id, ' . Cubix_I18n::getTblField('title') . ' AS title
			FROM cities
			WHERE country_id = ?
		';
		$result = parent::getAdapter()->fetchAll($sql, array($country_id));
		
		return $result;
	}
	
	public static function getSlugByOldSlug($old_slug)
	{
		$sql = '
			SELECT id, slug
			FROM cities
			WHERE slug_old = ?
		';
		$result = parent::getAdapter()->fetchRow($sql, array($old_slug));
		
		return $result;
	}

	public static function getSlugById($id)
	{
		$sql = '
			SELECT slug
			FROM cities
			WHERE id = ?
		';
		$result = parent::getAdapter()->fetchOne($sql, array($id));

		return $result;
	}

	public static function getByZoneSlug($zone_slug)
	{
		$sql = '
			SELECT
				cz.city_id AS id, ' . Cubix_I18n::getTblField('c.title') . ' AS title, c.slug AS city_slug
			FROM cityzones cz
			INNER JOIN cities c ON c.id = cz.city_id
			WHERE cz.slug = ?
		';
		$result = parent::getAdapter()->fetchRow($sql, array($zone_slug));

		return $result;
	}
	
	public static function getFakeCities()
	{
		$sql = '
			SELECT
				fc.id, ' . Cubix_I18n::getTblField('fc.title') . ' AS title, fc.slug AS slug, fc.zip
			FROM f_cities fc
			ORDER BY fc.title_en ASC
		';

		$result = parent::getAdapter()->fetchAll($sql);

		return $result;
	}
}
