<?php

class Cubix_Geography_CityItem extends Cubix_Model_Item
{
	public function getRegionTitle()
	{
		$model = new Cubix_Geography_Regions();
		$region = $model->get($this->region_id);
		
		if ( $region ) {
			return $region->{'title_' . Cubix_I18n::getLang()};
		}
		
		return '';
	}
}
