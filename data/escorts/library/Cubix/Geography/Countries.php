<?php

class Cubix_Geography_Countries extends Cubix_Model
{
    const FILTER_AT_LEAST_ONE_CITY   = 1; // 00000000000001
    const FILTER_AT_LEAST_ONE_ESCORT = 2; // 00000000000010

	protected $_table = 'countries';
	
	protected $_itemClass = 'Cubix_Geography_CountryItem';

	static public function getTitleById($id)
	{
		return self::getAdapter()->fetchOne('
			SELECT ' . Cubix_I18n::getTblField('title') . ' AS title FROM countries WHERE id = ?
		', $id);
	}

	static public function getTitleByCityId($id)
	{
		return self::getAdapter()->fetchOne('
			SELECT cn.' . Cubix_I18n::getTblField('title') . ' AS title
			FROM countries cn
			INNER JOIN cities ci ON ci.country_id = cn.id
			WHERE ci.id = ?
		', $id);
	}

	static public function getTitleBySlug($slug)
	{
		return self::getAdapter()->fetchOne('
			SELECT ' . Cubix_I18n::getTblField('title') . ' AS title FROM countries WHERE slug = ?
		', $slug);
	}

	static public function getTitleIsoBySlug($slug)
	{
		return self::getAdapter()->fetchRow('
			SELECT iso, ' . Cubix_I18n::getTblField('title') . ' AS title FROM countries WHERE slug = ?
		', $slug);
	}

    static public function getIdBySlug($slug)
    {
        return self::getAdapter()->fetchOne('
			SELECT id AS id FROM countries WHERE slug = ?
		', $slug);
    }

    static public function getSlugById($id)
    {
        return self::getAdapter()->fetchOne('
			SELECT slug AS co_slug FROM countries WHERE id = ?
		', $id);
    }

	static public function getTitleByIso($iso)
	{
		return self::getAdapter()->fetchRow('
			SELECT id, iso, ' . Cubix_I18n::getTblField('title') . ' AS title FROM countries WHERE iso = ?
		', $iso);
	}

	static public function getById($id)
	{
		return self::getAdapter()->fetchRow('
			SELECT cpc.id, cpc.iso, ' . Cubix_I18n::getTblField('cpc.title') . ' AS title FROM countries_phone_code cpc WHERE cpc.id = ?
		', $id);
	}

	/*public function getAll($p, $pp, $sort_field, $sort_dir, &$count)
	{
		$count = self::getAdapter()->fetchOne('
			SELECT COUNT(id) FROM countries
		');
		
		return parent::_fetchAll('
			SELECT id, ' . Cubix_I18n::getTblField('title') . ', iso FROM countries
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		');
	}*/

    /**
     * @param bool $must_have_regions
     * @param array $filters
     * @return array
     */
    public function ajaxGetAll($must_have_regions = TRUE, $filters = [])
    {
        $joins = $fieldsToSelect = $filtersStr = $groupedFilters = '';

        foreach ($filters as $filter) {
            switch ($filter) {
                case self::FILTER_AT_LEAST_ONE_CITY:
                    $fieldsToSelect .= 'c.cities_count';
                    $filtersStr     .= ' AND cities_count > 0';
                    break;
                case self::FILTER_AT_LEAST_ONE_ESCORT:
                    $fieldsToSelect .= 'count(eic.escort_id) as escorts_count';
                    $groupedFilters .= 'escorts_count >= 1';
                    $joins          .= 'LEFT JOIN escorts_in_cities eic ON eic.country_id = c.id';
                    break;
            }
        }

        if ($must_have_regions) $filtersStr .= ' AND has_regions = TRUE';

        if (!empty($groupedFilters)) $groupedFilters = ' HAVING ' . $groupedFilters;
        if (!empty($filtersStr)) $filtersStr = ' WHERE 1 ' . $filtersStr;
        if (!empty($fieldsToSelect)) $fieldsToSelect = ' , ' . $fieldsToSelect;

        $sql = '
			SELECT id, ' . Cubix_I18n::getTblField('title') . ' AS title, has_regions, slug 
            ' . $fieldsToSelect . '
			FROM countries c
			' . $joins . '
			' . $filtersStr . '
			GROUP BY c.id
            ' . $groupedFilters . '
			ORDER BY title ASC
		';

        return parent::_fetchAll($sql);
    }

	public function ajaxGetAllClassifiedAds($must_have_regions = true)
	{
		return parent::_fetchAll('
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' AS title, c.has_regions, c.slug
			FROM countries c
			INNER JOIN cities ct ON ct.country_id = c.id
			INNER JOIN classified_ads_cities cac ON cac.city_id = ct.id
			' . ($must_have_regions ? 'WHERE c.has_regions = TRUE' : '') . '
			GROUP BY c.id
			ORDER BY title ASC
		');
	}

    /**
     * Returns countries that have at least one review
     *
     * @param bool $must_have_regions
     * @return mixed
     */
	public function ajaxGetAllReviews($must_have_regions = true)
	{
		return parent::_fetchAll('
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' AS title, c.has_regions, c.slug
			FROM countries c
			INNER JOIN cities ct on ct.country_id = c.id
            INNER JOIN reviews r on r.city = ct.id
            INNER JOIN escorts e on r.escort_id = e.id
			' . ($must_have_regions ? 'WHERE c.has_regions = TRUE' : '') . '
			WHERE r.status = '. REVIEW_APPROVED .'
			AND r.is_deleted = 0
			AND e.disabled_reviews != 1 
			GROUP BY c.id
			ORDER BY title ASC
		');
	}

    /**
     * Returns countries that have at least one comment
     *
     * @param bool $must_have_regions
     * @return mixed
     */
    public function ajaxGetAllComments()
    {
        $sql = '
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' AS title, c.has_regions, c.slug
			FROM countries c
            INNER JOIN comments cmt on cmt.e_country_id = c.id
            INNER JOIN escorts as e ON e.id = cmt.escort_id
			WHERE cmt.status = '. COMMENT_ACTIVE .'
			AND cmt.disabled_comments = 0
			GROUP BY c.id
			ORDER BY title ASC
		';

        return parent::_fetchAll($sql);
    }

	//For beneluxxx only
	public function ajaxGetAllBlx($must_have_regions = true)
	{
		return parent::_fetchAll('
			SELECT id, ' . Cubix_I18n::getTblField('title') . ' AS title, has_regions FROM countries_all
			' . ($must_have_regions ? 'WHERE has_regions = TRUE' : '') . '
			ORDER BY title ASC
		');
	}
	
	public function get($id)
	{
		return parent::_fetchRow('
			SELECT id, ' . Cubix_I18n::getTblFields('title') . ', iso, has_regions FROM countries
			WHERE id = ?
		', $id);
	}

	public function hasCity($country_id, $city_id)
	{
		return parent::getAdapter()->fetchOne('
			SELECT TRUE FROM cities WHERE country_id = ? AND id = ?
		', array($country_id, $city_id));
	}

	public function exists($country_id)
	{
		return parent::getAdapter()->fetchOne('
			SELECT TRUE FROM countries WHERE id = ?
		', $country_id);
	}

	static public function getAll()
	{
		return self::db()->fetchAll('
			SELECT id, title_' . Cubix_I18n::getLang() . ' AS title FROM countries
		');
	}
	
	static public function getNationality($country_iso)
	{
		return self::db()->fetchRow('
			SELECT id, title_' . Cubix_I18n::getLang() . ' AS title
			FROM nationalities
			WHERE iso = ?
		', array($country_iso));
	}
}
