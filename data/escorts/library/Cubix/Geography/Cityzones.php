<?php

class Cubix_Geography_Cityzones extends Cubix_Model
{
	protected $_table = 'cityzones';
	
	protected $_itemClass = 'Cubix_Geography_CityzoneItem';
	
	/*public function getAll($city_id, $p, $pp, $sort_field, $sort_dir, &$count)
	{
		$count = self::getAdapter()->fetchOne('
			SELECT COUNT(id) FROM cityzones
			WHERE city_id = ?
		', $city_id);
		
		return parent::_fetchAll('
			SELECT id,  ' . Cubix_I18n::getTblField('title') . ' FROM cityzones
			WHERE city_id = ?
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ( ($p - 1) * $pp ) . ', ' . $pp . '
		', $city_id);
	}*/
	public function getZonesByCityId($id, $gender = 1, $type = false)
	{
		$where = " AND e.gender = ? ";

		if ( $type )
		{
			if ( $type == USER_TYPE_AGENCY )
				$where .= " AND e.agency_id IS NOT NULL ";
			else if ( $type == USER_TYPE_SINGLE_GIRL )
				$where .= " AND e.agency_id IS NULL ";
		}

		$sql = "
			SELECT
				c.iso AS country_iso, ct.id, ct.slug, ct.title_en AS title,
				COUNT(e.id) as escort_count,
				ctz.id AS zone_id, ctz.slug AS zone_slug, ctz.title_en AS zone_title
			FROM escorts AS e

			INNER JOIN escort_cityzones ez ON ez.escort_id = e.id

			INNER JOIN cityzones ctz ON ctz.id = ez.city_zone_id

			INNER JOIN cities ct ON ct.id = e.city_id
			INNER JOIN regions r ON r.id = ct.region_id
			INNER JOIN countries c ON c.id = r.country_id

			WHERE
				ctz.city_id = ? {$where}
			GROUP BY ctz.id
		";

		return parent::_fetchAll($sql, array($id/*, Cubix_Application::getById(Cubix_Application:: getId())->country_id*/, $gender));
	}
	
	public function ajaxGetAll($city_id)
	{
		return parent::_fetchAll('
			SELECT cz.id, ' . Cubix_I18n::getTblField('cz.title') . ' AS title, ' . Cubix_I18n::getTblField('c.title') . ' AS city_title, cz.slug as slug   FROM cityzones cz
			INNER JOIN cities c ON c.id = cz.city_id
			WHERE cz.city_id = ?
			ORDER BY title ASC
		', array($city_id));
	}

	public function ajaxGetAllByCitySlug($city_slug)
	{
		return parent::_fetchAll('
			SELECT cz.id, ' . Cubix_I18n::getTblField('cz.title') . ' AS title, ' . Cubix_I18n::getTblField('c.title') . ' AS city_title, cz.slug as slug   FROM cityzones cz
			INNER JOIN cities c ON c.id = cz.city_id
			WHERE c.slug = ?
			ORDER BY title ASC
		', array($city_slug));
	}
	
	public function get($id)
	{
		return parent::_fetchRow('
			SELECT cz.id, cz.city_id, r.id AS region_id, ct.id AS country_id, ' . Cubix_I18n::getTblFields('cz.title') . ' FROM cityzones cz
			LEFT JOIN cities c ON c.id = cz.city_id
			LEFT JOIN regions r ON r.id = c.region_id
			LEFT JOIN countries ct ON ct.id = c.country_id
			WHERE cz.id = ?
		', $id);
	}

	public function getBySlug($slug)
	{
		return parent::_fetchRow('
			SELECT cz.id, cz.city_id, r.id AS region_id, ct.id AS country_id, cz.' . Cubix_I18n::getTblField('title') . ' AS title FROM cityzones cz
			LEFT JOIN cities c ON c.id = cz.city_id
			LEFT JOIN regions r ON r.id = c.region_id
			LEFT JOIN countries ct ON ct.id = c.country_id
			WHERE cz.slug = ?
		', $slug);
	}

	public function getZoneInfo($id){
		return parent::_fetchRow('
			SELECT cz.id, ' . Cubix_I18n::getTblField('cz.title') . ' AS title, ' . Cubix_I18n::getTblField('c.title') . ' AS city_title FROM cityzones cz
			LEFT JOIN cities c ON c.id = cz.city_id
			LEFT JOIN regions r ON r.id = c.region_id
			LEFT JOIN countries ct ON ct.id = c.country_id
			WHERE cz.id = ?
		', $id);	
	}
}
