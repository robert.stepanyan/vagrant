<?php

interface Core_JsonRpc_Server_PluginInterface
{
	public function notify($about, array $params = array());

	public function setServer(JsonRpc_Server $server);
}

