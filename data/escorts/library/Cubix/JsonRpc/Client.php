<?php

class Cubix_JsonRpc_Client
{
	protected $_url;

	static protected $_id = 0;

	protected $_client;

	public function __construct($url)
	{
		$this->_url = $url;
		$this->_client = new Zend_Http_Client($url);
	}

	public function setClient(Zend_Http_Client $client)
	{
		$this->_client = $client;
	}

	public function getClient()
	{
		return $this->_client;
	}

	public function call($method, array $params = array())
	{
		//var_dump($this->_url);die;
		
		$request = array(
			'id' => ++self::$_id,
			'method' => $method,
			'params' => $params
		);

		$content = json_encode($request);
		$length = strlen($content);

		try {
			$this->_client->setRawData($content);
			$this->_client->request('POST');
			$result = $this->_client->getLastResponse()->getBody();
		}
		catch ( Exception $e ) {
			throw new Exception('Could not connect to rpc server');
		}

		$plain = $result;
		$result = json_decode($result);
		if ( ! $result ) {
			throw new Exception('Could not parse server response. Actual response: ' . $plain);
		}

		if ( ! empty($result->error) ) {
			throw new Exception('Got error from server: ' . $result->error);
		}

		return $result->result;
	}
}

