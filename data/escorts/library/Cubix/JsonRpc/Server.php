<?php

class Cubix_JsonRpc_Server
{
	protected $_request;
	protected $_response;

	protected $_methods = array();

	protected $_plugins = array();

	public function __construct()
	{
		$this->_request = new Cubix_JsonRpc_Request();
		$this->_response = new Cubix_JsonRpc_Response();
	}

	public function getResponse()
	{
		return $this->_response;
	}

	public function registerPlugin(JsonRpc_Server_PluginInterface $plugin)
	{
		$plugin->setServer($this);
		$this->_plugins[] = $plugin;
	}

	protected function _notify($about, array $params = array())
	{
		foreach ( $this->_plugins as $plugin ) {
			if ( true === $plugin->notify($about, $params) ) {
				return true;
			}
		}
		return false;
	}

	public function addModule(Cubix_JsonRpc_Module $module)
	{
		$interface = $module->getInterface();
		if ( ! count($interface) )
			return;
		foreach ( $interface as $method ) {
			$this->addMethod(array($module, $method));
		}
	}

	public function addMethod($callback, $alias = null)
	{
		if ( is_string($callback) ) {
			if ( null === $alias )
				$alias = $callback;
		}
		elseif ( is_array($callback) ) {
			if ( 2 != count($callback) )
				throw new InvalidArgumentException('Valid callback is required');

			if ( is_string($callback[0]) ) {
				if ( null === $alias )
					$alias = $callback[0];
			}
			elseif ( is_object($callback[0]) ) {
				if ( null === $alias ) {
					if ( method_exists($callback[0], 'getName') ) {
						$alias = call_user_func_array(array($callback[0], 'getName'), array()) . '.' . $callback[1];
					}
					else {
						$alias = get_class($callback[0]) . '.' . $callback[1];
					}
				}
			}
			else {
				throw new InvalidArgumentException('Valid callback is required');
			}
		}
		else {
			throw new InvalidArgumentException('Valid callback is required');
		}

		if ( ! is_callable($callback) ) {
			throw new InvalidArgumentException('Passed callback is not callable');
		}

		$this->_methods[$alias] = $callback;
	}

	public function dispatch($namespace, $method, array $params = array())
	{
		try {
			$ns = str_replace('-', ' ', $namespace);
			$ns = ucwords($ns);
			$ns = str_replace(' ', '.', $ns);
			
			$class = 'Model_' . $ns;

			if ( ! isset($this->_methods[$namespace . '.' . $method]) )
				throw new Cubix_JsonRpc_Server_Exception('Invalid method');
			
			$result = call_user_func_array($this->_methods[$namespace . '.' . $method], $params);
			$this->_response->setResult($result);
		}
		catch ( Exception $e ) {
			if ( ! $this->_notify('exceptionCaught', array('e' => $e)) ) {
				$this->_response->setError($e->__toString());
			}
		}

		$this->_response->setId($this->_request->getId());
		return $this->_response;
	}

	public function handle(Cubix_JsonRpc_Request $request = null)
	{
		if ( null === $request ) {
			$data = file_get_contents('php://input');
			$this->_request->parse($data);
		}
		else {
			$this->_request = $request;
		}

		return $this->dispatch($this->_request->getNamespace(), $this->_request->getMethod(), $this->_request->getParams());
	}
}

