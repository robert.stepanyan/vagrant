<?php

class Cubix_JsonRpc_Request extends ArrayObject
{
	public function __construct()
	{
		parent::__construct(array(), self::ARRAY_AS_PROPS);
	}
	
	public function getMethod()
	{
		return $this->method;
	}

	public function getNamespace()
	{
		return $this->namespace;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getParams()
	{
		return $this->params;
	}

	public function parse($json)
	{
		$json = json_decode($json, true);
		if ( null === $json ) {
			throw new Cubix_JsonRpc_Request_Exception('Invalid json data');
		}

		if ( ! isset($json['method']) ) {
			throw new Cubix_JsonRpc_Request_Exception('Method is required');
		}

		list($namespace, $method) = explode('.', $json['method']);

		$this->method = $method;
		$this->namespace = $namespace;

		if ( ! isset($json['params']) || ! is_array($json['params']) ) {
			throw new Cubix_JsonRpc_Request_Exception('Params is required and must be array');
		}

		$this->params = $json['params'];

		$this->id = isset($json['id']) ? $json['id'] : null;
	}
}

