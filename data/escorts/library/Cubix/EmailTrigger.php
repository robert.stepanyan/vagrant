<?php

class Cubix_EmailTrigger 
{
    private $api_client;
    protected $user_id;

    public function __construct($user_id, $trigger_type, $escort_id)
    {
        $this->user_id = $user_id;
        $this->trigger_type = $trigger_type;
        $this->escort_id = $escort_id;

        $this->api_client = new Cubix_Api_XmlRpc_Client(); 
    }

    public function addToDeferred($params = array())
    {
        return $this->api_client->call('EmailTrigger.add', array($this->user_id, $this->trigger_type, $params, $this->escort_id));
    }

    public function removeFromDeferred()
    {
        return $this->api_client->call('EmailTrigger.remove', array($this->user_id, $this->trigger_type, $this->escort_id));
    }
}