<?php

class Cubix_ForumApi
{
	//const API_URL = 'http://forum.escortforum.net.dev/webapi/?cmd=%cmd%&%params%';
	const API_URL = 'http://forum.escortforum.net/webapi/?cmd=%cmd%&%params%';
	const API_KEY = 'hello_cubix_dev_2011';

	protected function _request($cmd, $args = array())
	{
		set_time_limit(60);
		
		foreach ($args as $arg => $value) {
			$args[] = "$arg=$value";
			unset($args[$arg]);
		}

		$url = str_replace(array('%cmd%', '%params%'), array($cmd, implode('&', array_merge($args, array('key=' . self::API_KEY, 'sid=' . session_id())))), self::API_URL);

		list($nil, $host, $path, $query) = parse_url($url);

		$content = file_get_contents($url);
		
		/*
		if(preg_match('/^46\.70\./', $_SERVER['REMOTE_ADDR']))
		{
		    echo 'content: '. $content;
		    die;
		}
		*/

		return $content;
	}

	public function RegisterUser($user_id, $user_name, $email, $lng = "en") {
		$resp = $this->_request('RegisterUser', array('user_id' => $user_id, 'user_name' => $user_name, 'email' => $email, 'lng' => $lng));
		return ($resp != 'error');
	}

	public function Login($user_id, $user_name = null, $email = null, $lng = null, $is_premium = false) {
		$resp = $this->_request('Login', array('user_id' => $user_id, 'user_name' => $user_name, 'email' => $email, 'lng' => $lng, 'is_premium' => $is_premium));
		return ($resp != 'error');
	}

	public function LogOut() {
		$resp = $this->_request('LogOut', array());
		return ($resp != 'error');
	}

	public function SetPremium($status, $user_id) {
		$resp = $this->_request('SetPremium', array('status' => $status, 'user_id' => $user_id));
		return ($resp != 'error');
	}

	public function LoadForumActivity($array, $user_id_index) {
		if ( ! is_array($array) ) {
			return false;
		}

        // collect user ids
        $users = array();
        foreach ($array as $row) {
            $user_id = $row[$user_id_index];
            $users[$user_id] = 1;
        }

        $res = $this->_request('LoadForumActivity', array('uids' => implode(':', array_keys($users))));
        if ( $res == 'error' ) {
			return $array;
        }

        $res = @unserialize($res);
        if ( ! is_array($res) || ! count($res) ) {
			return $array;
        }

        // collect user posts data
        $user_posts = array();
        $user_last_post = array();
        foreach ($res as $row) {
            list($user_id, $posts, $last_post) = $row;
            $user_posts[$user_id] = $posts;
            $user_last_post[$user_id] = $last_post;
        }

        // fill array
        foreach ($array as $index => $row) {
            $user_id = $row[$user_id_index];
            $array[$index]['posts'] = $user_posts[$user_id];
            $array[$index]['last_post'] = $user_last_post[$user_id];
        }

        return $array;
	}

	public function GetForumPostsCount($user_id) {
		$resp = $this->_request('GetForumPostsCount', array('user_id' => $user_id));
		return @unserialize($resp);
	}

	public function DeleteUserPosts($user_id) {
		$resp = $this->_request('DeleteUserPosts', array('user_id' => $user_id));
		return ($resp != 'error');
	}

	public function UpdateEmail($user_id, $email) {
		$resp = $this->_request('UpdateEmail', array('user_id' => $user_id, 'email' => $email));

		return ($resp != 'error');
	}

	public function DeleteUser($user_id) {
		$resp = $this->_request('DeleteUser', array('user_id' => $user_id));
		return ($resp != 'error');
	}

	public function BannUser($user_id, $status) {
		$resp = $this->_request('BannUser', array('user_id' => $user_id, 'status' => $status ));
		return ($resp != 'error');
	}
}
