<?php


class Cubix_VotdCalendar {
	/**
	 * Array of Week Day Names
	 *
	 * @var array
	 */
	public $wday_names = false;
	private $now;
	private $dailyHtml = array();
	private $offset = 0;
	private $areaId;
	/**
	 * Constructor - Calls the setDate function
	 *
	 * @see setDate
	 * @param null|string $date_string
	 */
	
	function __construct( $date_string = null, $area_id = null ) {
		$this->setDate($date_string);
		$this->setArea($area_id);
	}
	/**
	 * Sets the date for the calendar
	 *
	 * @param null|string $date_string Date string parsed by strtotime for the calendar date. If null set to current timestamp.
	 */
	public function setDate( $date_string = null ) {
		if( $date_string ) {
			$this->now = getdate(strtotime($date_string));
		} else {
			$this->now = getdate();
		}
	}

	public function setArea( $area_id = null ) {
		if( $area_id ) {
			$this->areaId = $area_id;
		} else {
			$this->areaId = 0;
		}
	}
	/**
	 * Add a daily event to the calendar
	 *
	 * @param string      $html The raw HTML to place on the calendar for this event
	 * @param string      $start_date_string Date string for when the event starts
	 * @param null|string $end_date_string Date string for when the event ends. Defaults to start date
	 */
	public function addDailyHtml( $html, $start_date_string, $end_date_string = null ) {
		static $htmlCount = 0;
		$start_date = strtotime($start_date_string);
		if( $end_date_string ) {
			$end_date = strtotime($end_date_string);
		} else {
			$end_date = $start_date;
		}
		$working_date = $start_date;
		do {
			$tDate = getdate($working_date);
			$working_date += 86400;
			$this->dailyHtml[$tDate['year']][$tDate['mon']][$tDate['mday']][$htmlCount] = $html;
		} while( $working_date < $end_date + 1 );
		$htmlCount++;
	}
	/**
	 * Clear all daily events for the calendar
	 */
	public function clearDailyHtml() { $this->dailyHtml = array(); }
	/**
	 * Sets the first day of the week
	 *
	 * @param int|string $offset Day to start on, ex: "Monday" or 0-6 where 0 is Sunday
	 */
	public function setStartOfWeek( $offset ) {
		if( is_int($offset) ) {
			$this->offset = $offset % 7;
		} else {
			$this->offset = date('N', strtotime($offset)) % 7;
		}
	}
	/**
	 * Returns/Outputs the Calendar
	 *
	 * @param bool $echo Whether to echo resulting calendar
	 * @return string HTML of the Calendar
	 */
	public function show( $echo = true ) {

		if( $this->wday_names ) {
			$wdays = $this->wday_names;
		} else {
			$today = (86400 * (date("N")));
			$wdays = array();
			$wdays[] =	__('obv4_sunday');	
			$wdays[] =	__('obv4_monday');	
			$wdays[] =	__('obv4_tuesday');	
			$wdays[] =	__('obv4_wednesday');	
			$wdays[] =	__('obv4_thursday');	
			$wdays[] =	__('obv4_friday');	
			$wdays[] =	__('obv4_saturday');	
			// for( $i = 0; $i < 7; $i++ ) {
			// 	$wdays[] = strftime('%a', time() - $today + ($i * 86400));
			// }
		}

		$this->arrayRotate($wdays, $this->offset);

		$wday    = date('N', mktime(0, 0, 1, $this->now['mon'], 1, $this->now['year'])) - $this->offset;
		
		$no_days = cal_days_in_month(CAL_GREGORIAN, $this->now['mon'], $this->now['year']);

		$out  = '<div class="ob-votd-calendar-wrapper">';
		
		// < Calendar >

		$out .= '<table cellpadding="0" cellspacing="0" class="ob-votd-calendar">';
		$out .= str_repeat('<col/>', 7);
		
		// < Calendar next/current/previous months >
		// < Calendar weekdays >

		$out .= '<thead>';
		
		$out .= '<tr class="month-year-buttons">';
		
		$out .= '<th>';
		$out .= '<a href="#" data-month="';
		$out .= date('Y-m-d', strtotime(date('Y-m',mktime(0, 0, 1, $this->now['mon'], 1, $this->now['year'])) . " -1 month"));
		$out .= '" data-area-id="' . $this->areaId . '">&lsaquo;</a>';
		$out .= '</th>';
		
		$out .= '<th colspan="5">';
		$out .= date('F Y', mktime(0, 0, 1, $this->now['mon'], 1, $this->now['year']));
		$out .= '</th>';

		$out .= '<th>';
		$out .= '<a href="#" data-month="';
		$out .= date('Y-m-d', strtotime(date('Y-m',mktime(0, 0, 1, $this->now['mon'], 1, $this->now['year'])) . " +1 month"));
		$out .= '" data-area-id="' . $this->areaId . '">&rsaquo;</a>';
		$out .= '</th>';

		$out .= '</tr>';

		$out .= '<tr class="weekdays">';

		for ($i = 0; $i < 7; $i++) {
			$out .= '<th>' . $wdays[$i] . '</th>';
		}

		$out .= '</tr>'; 

		$out .= '</thead>';

		// </ Calendar weekdays >
		// </ Calendar next/current/previous months >

		// < Calendar dates >

		$out .= '<tbody>';
		
		$out .= '<tr>';
	
		$wday = ($wday + 7) % 7;

		if ($wday == 7) {
			$wday = 0;
		} else {
			$out .= str_repeat('<td>&nbsp;</td>', $wday);
		}
		
		$count = $wday + 1;

		for( $i = 1; $i <= $no_days; $i++ ) {

			$inactive_html_class = ((strtotime( $this->now['mon'] . '/' . $i . '/' . $this->now['year']) + + 60 * 60 * 24) < time()) ? 'inactive ' : '';

			
			$busy_html_class = isset($this->dailyHtml[$this->now['year']][$this->now['mon']][$i]) ?
				 implode(' ', $this->dailyHtml[$this->now['year']][$this->now['mon']][$i]) . ' ' : '';

			$today_html_class = (
				$i == $this->now['mday']
				&& $this->now['mon'] == date('n')
				&& $this->now['year'] == date('Y')
				) ? 'today ' : ''; 

			$out .= '<td class="' . $today_html_class . $busy_html_class . $inactive_html_class .'">';

			$datetime = mktime(0, 0, 1, $this->now['mon'], $i, $this->now['year']);

			$out .= '<time datetime="' . date('Y-m-d', $datetime) . '">' . $i . '</time>';

			$out .= "</td>";

			if( $count > 6 ) {
				$out .= "</tr>" . ($i != $count ? '<tr>' : '');
				$count = 0;
			}

			$count++;
		}

		$out .= ($count != 1 ? str_repeat('<td>&nbsp;</td>', 8 - $count) : '') . '</tr>';

		// </ Calendar dates >

		$out .= '</tbody>';
		
		$out .= '</table>';
		
		$out .= '</div>';

		// </ Calendar >

		if( $echo ) {
			echo $out;
		}

		return $out;
	}
	private function arrayRotate( &$data, $steps ) {
		$count = count($data);
		if( $steps < 0 ) {
			$steps = $count + $steps;
		}
		$steps = $steps % $count;
		for( $i = 0; $i < $steps; $i++ ) {
			array_push($data, array_shift($data));
		}
	}
}