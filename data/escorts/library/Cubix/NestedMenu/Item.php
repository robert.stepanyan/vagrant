<?php

class Cubix_NestedMenu_Item
{
	protected $_data = array();
	
	public function __construct(array $data = array())
	{
		foreach ( $data as $key => $value ) {
			$this->$key = $value;
		}
	}
	
	public function __set($key, $value)
	{
		$this->_data[$key] = $value;
	}
	
	public function __get($key)
	{
		return isset($this->_data[$key]) ? 
			$this->_data[$key] : NULL;
	}
	
	public function __isset($key)
	{
		return isset($this->_data[$key]);
	}
	
	public function __unset($key)
	{
		unset($this->_data[$key]);
	}
	
	public function getData()
	{
		return $this->_data;
	}
	
	public function isEqual(Cubix_NestedMenu_Item $item)
	{
		foreach ( $item->getData() as $key => $value ) {
			if ( ! isset($this->$key) || $this->$key !== $value ) {
				return FALSE;
			}
		}
		
		return TRUE;
	}
}
