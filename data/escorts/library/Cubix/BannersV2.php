<?php
class Cubix_BannersV2
{
	public static function GetFromApi($zone, $max_count = NULL)
	{
		$banners = array();
		
		$zone = intval($zone);
		
		if ($zone < 1)
			return $banners;
		
		$c = '';
		
		if (!is_null($max_count))
		{
			$max_count = intval($max_count);
			
			if ($max_count > 0)
			{
				$c = '&max_count=' . $max_count;
			}
		}
		
		try {
			$json_banners = file_get_contents('https://www.ixspublic.com/api.php?zone=' . $zone . $c);
			$banners = json_decode($json_banners);
		}
		// The ADS Server is down!
		catch ( Exception $e ) {
			echo("connection error. Zone ID: " . $zone . "\n");
			echo $e;
		}
		
		return $banners;
	}
	
	public static function GetBanners($zone, $limit = NULL)
	{
		$banners = self::GetFromApi($zone);
		
		if (!is_null($limit))
		{
			$limit = intval($limit);
			
			if ($limit > 0 && count($banners) > $limit)
			{
				$l = count($banners) - 1;
				$new_banners = array();
				
				while (count($new_banners) != $limit)
				{
					$rand = rand(0, $l);
					
					if (!isset($new_banners[$rand]))
						$new_banners[$rand] = $banners[$rand];
				}
				
				$banners = $new_banners;
			}
		}
		
		return $banners;
	}
}
