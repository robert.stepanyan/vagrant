<?php

class Cubix_Security_Plugin extends Zend_Controller_Plugin_Abstract
{
	public function routeStartup(Zend_Controller_Request_Abstract $request)
	{
		/*$excl_actions = array(
			'add-client-to-blacklist'
		);
		var_dump(Zend_Controller_Front::getInstance()->getRequest()->getActionName());
		if ( in_array($request->getActionName(), $excl_actions) ) {
			return true;
		}*/
		
		if ( ! isset($_COOKIE['client_id']) || ! preg_match('#^[0-9a-f]{32}$#', $_COOKIE['client_id']) ) {
			setcookie('client_id', $client_id = md5(rand(10, 900) * microtime(true)), strtotime('+10 year'), '/', 'www.' . preg_replace('#^www.#', '', $_SERVER['HTTP_HOST']));
		}
		else {
			$client_id = $_COOKIE['client_id'];
		}

		$post = $_POST;
		$get = $_GET;
		$cookie = $_COOKIE;
		
		$found = false;
		$malicious = array();
		foreach ( ($data = array('get' => $get, 'post' => $post, 'cookie' => $cookie)) as $type => $params ) {
			$malicious[$type] = $this->_getMaliciousParams($params);
			//var_dump($malicious);
			if ( count($malicious[$type]) > 0 )
				$found = true;
		}

		if ( ! $found ) return;

		$date = date('Y-m-d H:i:s');
		$user_id = null;

		$user = Model_Users::getCurrent();
		if ( $user ) $user_id = $user->id;
		
		$ip_addrs = $this->_getClientAddrs();
		$host_names = $this->_getHostNames($ip_addrs);

		$url = $_SERVER['REQUEST_URI'];

		$db = Zend_Registry::get('db');
		
		$db->insert('malicious_requests', array(
			'date_time' => $date,
			'logged_user_id' => $user_id,
			'client_id' => $client_id,
			'ip_addrs' => implode(', ', $ip_addrs),
			'host_names' => implode(', ', $host_names),
			'url' => $url,
			'data' => json_encode($data),
			'malicious' => json_encode($malicious)
		));


		die(json_encode(array('status' => 'malicious')));
	}

	private $_check_for = array('sql', 'html');

	private function _getMaliciousParams($array)
	{
		$result = array();
		foreach ( $array as $key => $value ) {
			$value = is_array($value) ? implode(' ', $value) : $value ;
			foreach ( $this->_check_for as $cf ) {
				$meth = '_checkFor_' . $cf;
				if ( $this->$meth($value) ) {
					if ( ! isset($result[$key]) ) {
						$result[$key] = array();
					}
					$result[$key][] = $cf;
				}
			}
		}
		return $result;
	}


	private function _checkFor_sql($value)
	{
		if ( @preg_match('/(aes_decrypt|load_file|union.+select|union.+delete|union.+update|sleep.*\(.*\)|truncate.+ table|drop.+ table|mysqldump|table_name|information_schema|sys_eval|sys_exec|sysdate\(|CCHAR\(|nslookup)/xi', $value) ) {
			return true;
		}
		
		return false;
	}

	private function _checkFor_html($value)
	{
		return false;
		if ( @preg_match('/((\%3C)|<)[^\n]+((\%3E)|>)/i', $value) ||
			@preg_match('/((\%3C)|<)((\%2F)|\/)*[a-z0-9\%]+((\%3E)|>)/ix', $value) ||
			@preg_match('/((\%3C)|<)((\%69)|i|(\%49))((\%6D)|m|(\%4D))((\%67)|g|(\%47))[^\n]+((\%3E)|>)/i', $value)
		) {
			return true;
		}

		return false;
	}

	private function _getClientAddrs()
	{
		$ips = array($_SERVER['REMOTE_ADDR']);
		if ( isset($_SERVER['HTTP_CLIENT_IP']) )
			$ips[] = $_SERVER['HTTP_CLIENT_IP'];
		if ( isset($_SERVER['X_FORWARDED_FOR']) )
			$ips[] = $_SERVER['X_FORWARDED_FOR'];
		return $ips;
	}

	private function _getHostNames(array $ips)
	{
		$hosts = array();

		foreach ( $ips as $ip ) {
			exec('nslookup ' . $ip, $op);

			// php is running on windows machine
			if ( substr(php_uname(), 0, 7) == 'Windows' ) {
				$hosts[] = substr($op[3], 6);
			}
			else {
				// on linux nslookup returns 2 diffrent line depending on
				// ip or hostname given for nslookup
				if ( strpos($op[4], 'name = ') > 0 )
					$hosts[] = substr($op[4], strpos($op[4], 'name =') + 7, -1);
				else
					$hosts[] = substr($op[4], strpos($op[4], 'Name:') + 6);
			}
		}

		return $hosts;
	}
}

