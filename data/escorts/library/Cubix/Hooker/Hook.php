<?php

class Cubix_Hooker_Hook
{
	/**
	 * @var Cubix_Hooker
	 */
	protected $_hooker;

	/**
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $_db;

	public function __construct()
	{
		$this->_db = Zend_Registry::get('db');
	}

	public function setHooker(Cubix_Hooker $hooker)
	{
		$this->_hooker = $hooker;
	}
}
