<?php

class Cubix_Geoip
{
	public static function getClientLocation($ip = null)
    {
        if (!function_exists('geoip_record_by_name') && !in_array(Cubix_Application::getId(), array(APP_ED, APP_EG_CO_UK, APP_EF, APP_A6)))
			return 0;

        if (!$ip)
        {
	        if (isset($_SERVER['HTTP_CF_CONNECTING_IP']))
		        $ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
            else if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])){ 
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				$ip_array = explode(',', $ip);
				$ip = trim($ip_array[0]);
			}
            else if (isset($_SERVER['REMOTE_ADDR']))
                $ip = $_SERVER['REMOTE_ADDR'];
            else
			{
                $ip = 'UNKNOWN';
				return false;
			}
        }

		
		if(APPLICATION_ENV == 'development'){
			return  array(
				'ip' => $ip,
				'country' => 'Gelderland',
				'country_iso' => strtolower('NL'),
				'region' => 'Gelderland test',
				'city' => 'Apeldoorn test',
				'latitude' => '52.21',
				'longitude' => '5.96944'
			);
		}
		elseif ( Cubix_Application::getId() == APP_ED || Cubix_Application::getId() == APP_EG_CO_UK || Cubix_Application::getId() == APP_BL || Cubix_Application::getId() == APP_EF || Cubix_Application::getId() == APP_A6) {
			if ( Zend_Registry::isRegistered('system_config') ) {
				$system = Zend_Registry::get('system_config');
				$ip2_path = $system['IP2Location_path'];
			} else {
				$config = Zend_Registry::get('config');
				$ip2_path = $config->IP2Location_path;
			}

			/*if(Cubix_Application::getId() == APP_EG_CO_UK ) {
			    echo "<pre style='display: none'>";
			    var_dump(Zend_Registry::isRegistered('system_config') );
			    var_dump($ip2_path);
			    var_dump($system);
                echo "</pre>";
            }*/
			
			$ip = $ip ? $ip : Cubix_Geoip::getIP();
			$geo_ip = new Cubix_IP2Location($ip2_path, Cubix_IP2Location::FILE_IO);
			$records = $geo_ip->lookup($ip, array(Cubix_IP2Location::ALL));
			
			$geoData = array(
				'ip' => $ip,
				'country' => $records['countryName'],
				'country_iso' => strtolower($records["countryCode"]),
				'region' => $records['regionName'],
				'city' => $records['cityName'],
				'latitude' => $records['latitude'],
				'longitude' => $records['longitude']
			);
		}
		else{
			$geoip = geoip_record_by_name($ip);

			if (!$geoip['country_name'] && !$geoip['country_code'] && !$geoip['region'] && !$geoip['city'] && !$geoip['latitude'] && !$geoip['longitude'])
				return $geoip;

			$geoData = array(
				'ip' => $ip,
				'country' => $geoip['country_name'],
				'country_iso' => $geoip['country_code'],
				'region' => $geoip['region'],
				'city' => $geoip['city'],
				'latitude' => $geoip['latitude'],
				'longitude' => $geoip['longitude']
			);
		}



		return $geoData;
    }

	public static function getIP()
	{
		$ip = '';

		if (isset($_SERVER['HTTP_CF_CONNECTING_IP']))
			$ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
		else if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if (isset($_SERVER['REMOTE_ADDR']))
			$ip = $_SERVER['REMOTE_ADDR'];

		$ip_arr = explode(',', $ip);
		return $ip_arr[0];
	}
}
