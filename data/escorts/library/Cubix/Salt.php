<?php

class Cubix_Salt
{
	public static function generateSalt($data)
	{
	    $salt = md5('~' . $data . '~' . microtime(true) . '~');
	
	    $salt = substr($salt, rand(0,22), 10);
	
	    return $salt;
	}

	public static function hashPassword($password, $salt)
	{
	    return md5($password . $salt);
	}

    public static function generateRandomString($length = 10) {
        $characters = '0123465789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
