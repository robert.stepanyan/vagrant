<?

class Cubix_Coinsome extends Cubix_Model
{
	const ORDER_PENDING = 0;
	const ORDER_PAID = 1;
	const ORDER_EXPIRED = 2;
	const ORDER_UNCONFIRMED = 3;
	const ORDER_SEMI_PAID = 4;
	
	private $domains = [
		APP_EF => 'https://www.coinsome.ch',
		APP_A6 => 'https://www.coinpass.ch'
	];
	
	private $tokens = [
		APP_EF => '82cc921c6a5c6707e1d6e6862ba3201a',
		APP_A6 => 'c45b5657c426613a2cce9b58e889a0c2'
	];
	
	
	
	public function __construct() {
		parent::__construct();
		$this->token = $this->tokens[$this->_app_id];
		$this->domain = $this->domains[$this->_app_id];
	}
	
	public function generateFormUrl($order_id){
		return  $this->domain. '/apps/'. $this->token  .'/'. $order_id;
	}
	
	public function getTransaction($order_id)
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->domain. '/check/'. $this->token ."/". $order_id);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  "Content-Type: application/json",
		));
		
		$responseData = curl_exec($ch);
		
		if(curl_errno($ch)) {
			return curl_error($ch);
		}
		curl_close($ch);
		return $responseData;
	}
	
	public function getOrder($order_id)
	{
		return $this->_db->fetchRow('SELECT user_id, payment_type, price, hash, gotd_orders_ids FROM coinsome_orders WHERE id = ?', array($order_id));
	}
	
	public function updateOrder($order_id, $status, $json_data)
	{
		$data = array(
			'status' => $status,
			'data' => $json_data
		);
		
		$this->_db->update('coinsome_orders', $data, $this->_db->quoteInto('id  = ?', $order_id));
	}
	
	public function saveLog($order_id, $status, $json_data)
	{
		$log_data = array(
			'order_id' => $order_id,
			'status' => $status,
			'data' => $json_data
		);
		
		$this->_db->insert('coinsome_orders_log', $log_data );
	}		
}