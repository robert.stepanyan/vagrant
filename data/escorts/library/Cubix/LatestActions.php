<?php

class Cubix_LatestActions
{
	const ACTION_NEW_PHOTO		= 1;
	const ACTION_PROFILE_UPDATE = 2;
	const ACTION_TOUR_UPDATE	= 3;
	const ACTION_NEW_REVIEW		= 4;
	const ACTION_NEW_COMMENT	= 5;
	

	public static $_actions = array(
		self::ACTION_NEW_PHOTO		=> 'new photo added',
		self::ACTION_PROFILE_UPDATE => 'escort profile updated',
		self::ACTION_TOUR_UPDATE	=> 'tour updated',
		self::ACTION_NEW_REVIEW		=> 'new review added',
		self::ACTION_NEW_COMMENT	=> 'new comment added'
	);
	
	public static $_actions_arr = array(
		self::ACTION_NEW_PHOTO,
		self::ACTION_PROFILE_UPDATE,
		self::ACTION_TOUR_UPDATE,
		self::ACTION_NEW_REVIEW,
		self::ACTION_NEW_COMMENT
	);
	
	private static function _db()
	{
		return Zend_Registry::get('db');
	}

	public static function addAction($escort_id, $action)
	{		
		if ( ! array_key_exists($action, self::$_actions) ) {
			throw new Exception('Invalid action type "' . $action . '"');
		}
		
		$data = array(
			'escort_id' => $escort_id,
			'action_type' => $action,
			'comment' => self::$_actions[$action]
		);
		
		if ( Zend_Auth::getInstance()->hasIdentity() ) {
			$bo_user = Zend_Auth::getInstance()->getIdentity();
			$data['sales_user_id'] = $bo_user->id;
		}

		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		$client->call('Escorts.addLatestAction', array($data));
	}
	
	public static function addActionDetail($escort_id, $action, $entity_id)
	{		
		if ( ! array_key_exists($action, self::$_actions) ) {
			throw new Exception('Invalid action type "' . $action . '"');
		}
		
		$data = array(
			'escort_id' => $escort_id,
			'action_type' => $action,
			'entity_id' => $entity_id
		);

		$client = Cubix_Api_XmlRpc_Client::getInstance();		
		$client->call('Escorts.addLatestActionDetail', array($data));
	}
	
	public static function addDirectAction($escort_id, $action)
	{
		if ( ! array_key_exists($action, self::$_actions) ) {
			throw new Exception('Invalid action type "' . $action . '"');
		}
		
		$data = array(
			'escort_id' => $escort_id,
			'action_type' => $action,
			'comment' => self::$_actions[$action]
		);
		
		if ( Zend_Auth::getInstance()->hasIdentity() ) {
			$bo_user = Zend_Auth::getInstance()->getIdentity();
			$data['sales_user_id'] = $bo_user->id;
		}
		
		self::_db()->insert('latest_actions', $data);
	}
	
	public static function addDirectActionDetail($escort_id, $action, $entity_id)
	{		
		if ( ! array_key_exists($action, self::$_actions) ) {
			throw new Exception('Invalid action type "' . $action . '"');
		}
		
		$data = array(
			'escort_id' => $escort_id,
			'action_type' => $action,
			'entity_id' => $entity_id
		);

		self::_db()->insert('latest_actions_details', $data);
	}
}
