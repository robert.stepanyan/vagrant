<?php


class Cubix_PrivateMessagingEd extends Cubix_Model {
	private $type;
	private $id;
	private $api_client;
	
	public function __construct($type, $id) {
		$this->type = $type;
		$this->id = $id;
		
		$this->api_client = new Cubix_Api_XmlRpc_Client();
	}

	public function getId()
    {
	    return $this->id;
    }
	
	public function sendMessage($body, $participant_type, $participant_id, $for_paids_only = true, $files = array())
	{

		$result = $this->api_client->call('PrivateMessagingEd.sendMessage', array($this->type, $participant_type, $this->id, $participant_id, $body, $for_paids_only,$files));

		return $result;
	}

	public function getSendersLastMessage($participantType, $participantId, $intervalMinutes = null)
    {
        $result = $this->api_client->call('PrivateMessagingEd.getSendersLastMessage', array($this->type, $this->id, $intervalMinutes));
        return $result;
    }

    public function getLastReplyInThread($participantType, $participantId, $intervalMinutes = null)
    {
        return $this->api_client->call('PrivateMessagingEd.getLastReplyInThread', array($this->type, $participantType, $this->id, $participantId));
    }

    public function getThreadLastMessage($participantType, $participantId, $notDeletedValidator = null)
    {
        $result = $this->api_client->call('PrivateMessagingEd.getThreadLastMessage', array($this->type, $participantType, $this->id, $participantId, $notDeletedValidator));
        return $result;
    }

    public function getThreadSentLastMessage($participantType, $participantId, $intervalMinutes = null)
    {
        $result = $this->api_client->call('PrivateMessagingEd.getThreadSentLastMessage', array($this->type, $participantType, $this->id, $participantId, $intervalMinutes));
        return $result;
    }

	public function updateThreadsPrimaryState($threads, $user_id) {

	    // Collect all thread ids, to filter messages
        // -------------------------------------
        $thread_ids = [];
        foreach	($threads as $key => $row) {
            $thread_ids[] = $row['id'];
        }
        // -------------------------------------

        // Getting first messages of threads
        // -------------------------------------
        $result = $this->api_client->call('PrivateMessagingEd.getThreadsPrimaryMessages', array($thread_ids));
        // -------------------------------------

        // Group messages by their thread-id
        // ---------------------------------
        $first_messages = [];
        foreach	($result as $key => $val) {
            $first_messages[$val['thread_id']] = $val;
        }
        // ---------------------------------

        // Attach to every thread it's primary state
        // P.S: primary state means, sent or received originally
        // (If this conversation is open by this user then it is "sent")
        // --------------------------------------------
        foreach ($threads as &$thread) {
            $thread['first_message'] = $first_messages[$thread['id']];
            $thread['was_open_by_user'] = in_array($user_id, [
                $thread['first_message']['user_id'],
                $thread['first_message']['escort_id'],
                $thread['first_message']['agency_id'],
            ]);
        }
        // --------------------------------------------

        return $threads;
    }
	
	public function sendMessages($body, $participants)
	{
		foreach($participants as $k => $participant) {
			$participants[$k]['type'] =  $participant['type'] = $participant['type'];
			if ( $participant['type'] == 'escort' ) {
				$participants[$k]['id'] = $participant['escort_id'];
			} else {
				$participants[$k]['id'] = $participant['user_id'];
			}
			unset($participants[$k]['escort_id']);
			unset($participants[$k]['user_id']);
		}
		$result = $this->api_client->call('PrivateMessagingEd.sendMessages', array($this->type, $this->id, $participants, $body));
	}
	
	public function getThreads($page = null, $per_page = null, $escort_id = null)
	{
		$result = $this->api_client->call('PrivateMessagingEd.getThreads', array($this->type, $this->id, $page, $per_page, $escort_id));
		return $result;
	}
	
	public function getThread($id, $page = null, $per_page = null, $order = 'desc')
	{
		$result = $this->api_client->call('PrivateMessagingEd.getThread', array($id, $this->type, $this->id, $page, $per_page, $order));
		
		return $result;
	}
	
	public function getThreadParticipant($id)
	{
		$result = $this->api_client->call('PrivateMessagingEd.getThreadParticipant', array($id, $this->type, $this->id));
		return $result;
	}
	
	public function removeThread($id)
	{
		$this->api_client->call('PrivateMessagingEd.removeThread', array($id, $this->type, $this->id));
		return true;
	}
	
	public function blockThread($id, $a_escort_id = null)
	{
		$this->api_client->call('PrivateMessagingEd.blockThread', array($id, $this->type, $this->id, $a_escort_id));
		return true;
	}
	
	public function unblockThread($id, $a_escort_id = null)
	{
		$this->api_client->call('PrivateMessagingEd.unblockThread', array($id, $this->type, $this->id, $a_escort_id));
		return true;
	}
	
	public function getParticipants($search)
	{
		$result = $this->api_client->call('PrivateMessagingEd.getParticipants', array($search, $this->type, $this->id, 10));
		return $result;
	}

    public function getParticipantsFilter($search, $page, $only_escorts = false, $only_members = false )
    {
        $result = $this->api_client->call('PrivateMessagingEd.getParticipantsFilter', array($search, $this->type, $this->id, 10, $page, $only_escorts, $only_members));

        return $result;
    }
	
	public function removeContact($id, $type)
	{
		$this->api_client->call('PrivateMessagingEd.removeContact', array($this->id, $this->type, $id, $type));
		return true;
	}
	
	public function addContact($id, $type)
	{
		$this->api_client->call('PrivateMessagingEd.addContact', array($this->id, $this->type, $id, $type));
		return true;
	}
	
	public function getContacts($page = null, $per_page = null)
	{
		$result = $this->api_client->call('PrivateMessagingEd.getContacts', array($this->type, $this->id, $page, $per_page));
		return $result;
	}
	
	public function checkIfActiveHasPackage($escort_id)
	{
		$result = $this->api_client->call('PrivateMessagingEd.checkIfActiveHasPackage', array($escort_id));
		return $result;
	}
	
	public function getUnreadThreadsCount()
	{
	    // return $this->id;
		return $this->api_client->call('PrivateMessagingEd.getUnreadThreadsCount', array($this->type, $this->id));
	}
	
	public function notifyByMail($user_id,$notify)
	{
		return $this->api_client->call('PrivateMessagingEd.notifyByMail', array($user_id,$notify));
	}
	
	public function notifyByMailText($user_id,$notify)
	{
		return $this->api_client->call('PrivateMessagingEd.notifyByMailText', array($user_id,$notify));
	}
	
	public function getNotifications($user_id)
	{
		return $this->api_client->call('PrivateMessagingEd.getNotifications', array($user_id));
	}

    //In PV we have two type of user escort and member
    //If it is independent or agency escort we take escort_id as identity
    //If member user_id
    public static function getIdentityField($type)
    {
        //return $type;
        switch ( $type ) {
            case 'escort':
                $field = 'escort_id';
                break;
            case 'agency':
                $field = 'agency_id';
                break;
            case 'member':
                $field = 'user_id';
        }

        return $field;
    }
	
}
