<?php
/*
 * Copyright (C) 2002-2007 Oliver Hitz <oliver@net-track.ch>
 *
 * $Id: SMS.inc,v 1.5 2007-09-18 14:23:13 oli Exp $
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

class Cubix_SMSOld
{
	var $userkey;
	var $password;
	var $originator;
	var $recipients;
	var $content;
	var $blinking;
	var $flashing;
	var $debug;
	var $mcc;
	var $mnc;
	var $logo;
	var $timeout;
  	var $servers;
  	var $deferred;
  	var $timezone;
  	var $notification;

	public function __construct($u, $p)
  	{
    	$this->userkey = $u;
	    $this->password = $p;
	    $this->recipients = array();
	    $this->blinking = false;
	    $this->flashing = false;
	    $this->originator = "";
	    $this->debug = 0;
	    $this->timeout = 2;
	    $this->notification = array();
	
	    $this->servers = array( "xml1.aspsms.com:5061",
				    "xml1.aspsms.com:5098",
				    "xml2.aspsms.com:5061",
				    "xml2.aspsms.com:5098" );
	}

	public function setTimeout($t)
  	{
		$this->timeout = $t;
  	}

	public function setOriginator($o)
  	{
    	$this->originator = $o;
  	}

	public function setDeferred($d)
  	{
    	$this->deferred = $d;
  	}

	public function setTimezone($t)
  	{
    	$this->timezone = $t;
  	}

	public function addRecipient($r, $id = null)
  	{
    	$this->recipients[] = array( "number" => $r, "transaction" => $id );
  	}

	public function setRecipient($r, $id = null)
	{
		$this->recipients = array(array( "number" => $r, "transaction" => $id ));
	}
	
	public function setMCC($mcc)
  	{
    	$this->mcc = $mcc;
  	}

	public function setMNC($mnc)
  	{
    	$this->mnc = $mnc;
  	}

	public function setLogo($logo)
  	{
    	$this->logo = $logo;
  	}
	
	private function _umlautFilter(&$string)
	{
		$ar = array("ä"=>"&#228;", "ö"=>"&#246;", "ü"=>"&#252;", "ß"=>"&#223;",
			"Ä"=>"&#196;", "Ö"=>"&#214;", "Ü"=>"&#220;", "À"=>"&#192;",
			"È"=>"&#200;", "É"=>"&#201;", "Ê"=>"&#202;", "è"=>"&#232;",
			"é"=>"&#233;", "ê"=>"&#234;", "à"=>"&#224;", "ï"=>"&#239;",
			"ù"=>"&#249;", "û"=>"&#251;", "ü"=>"&#252;", "ç"=>"&#231;",
			"Æ"=>"&#198;", "?"=>"&#330;", ""=>"&#338;", ""=>"&#339;",
			""=>"&#8364;","«"=>"&#171;", "»"=>"&#187;"
		);

		foreach($ar as $key=>$val)
		{
			$string = str_replace( $key, $val, $string );
		}
	}

	public function setContent($content, $parse = false)
  	{
		$this->_umlautFilter($content);
		
    	if ( $parse ) {
      		$this->content = "";
      		$in = false;
      
      		for ( $i = 0; $i < strlen($content); $i++ ) 
      		{
        		$c = $content[$i];
        		
        		if ( $c == "[" && ! $in ) {
          			$this->content .= "<blink>";
  	  				$in = true;
	  				$this->blinking = true;
        		} 
        		else if ( $c == "]" && $in ) {
          			$this->content .= "</blink>";
	  				$in = false;
        		} 
        		else {
          			$this->content .= $c;
        		}
      		}
    	} 
    	else {
      		$this->content = $content;
    	}
  	}

	public function setFlashing()
  	{
    	$this->flashing = true;
  	}

	public function setBufferedNotificationURL($url)
  	{
    	$this->notification["buffered"] = $url;
  	}

	public function setDeliveryNotificationURL($url)
  	{
    	$this->notification["delivery"] = $url;
  	}

	public function setNonDeliveryNotificationURL($url)
  	{
    	$this->notification["nondelivery"] = $url;
  	}

  	function getXML($content, $action)
  	{
    	$originator = "";
    	if ($this->originator != "") {
	      $originator = sprintf("  <Originator>%s</Originator>\r\n", $this->originator);
	    }

    	$recipients = "";
	    if (count($this->recipients) > 0) 
	    {
	    	foreach ($this->recipients as $re) 
	    	{
				if ($re["transaction"] != null) {
					$recipients .= sprintf("  <Recipient>\r\n".
							 "    <PhoneNumber>%s</PhoneNumber>\r\n".
							 "    <TransRefNumber>%s</TransRefNumber>\r\n".
							 "  </Recipient>\r\n",
							 htmlspecialchars($re["number"]),
							 htmlspecialchars($re["transaction"]));
				} 
				else {
					$recipients .= sprintf("  <Recipient>\r\n".
							 "    <PhoneNumber>%s</PhoneNumber>\r\n".
							 "  </Recipient>\r\n",
							 htmlspecialchars($re["number"]));
				}
	      	}
	    }

		$notify = "";
	    if (isset($this->notification["buffered"])) {
	    	$notify .= sprintf("  <URLBufferedMessageNotification>%s</URLBufferedMessageNotification>\r\n",
				htmlspecialchars($this->notification["buffered"]));
	    }
	    if (isset($this->notification["delivery"])) {
	    	$notify .= sprintf("  <URLDeliveryNotification>%s</URLDeliveryNotification>\r\n",
				htmlspecialchars($this->notification["delivery"]));
	    }
	    if (isset($this->notification["nondelivery"])) {
	    	$notify .= sprintf("  <URLNonDeliveryNotification>%s</URLNonDeliveryNotification>\r\n",
				 htmlspecialchars($this->notification["nondelivery"]));
	    }

    	if (isset($this->deferred)) {
      		$deferred = sprintf("  <DeferredDeliveryTime>%s</DeferredDeliveryTime>\r\n", $this->deferred);
    	} 
    	else {
      		$deferred = "";
    	}
    	if (isset($this->timezone)) {
      		$timezone = sprintf("  <TimeZone>%s</TimeZone>\r\n", $this->timezone);
    	} 
    	else {
      		$timezone = "";
    	}

	    return sprintf("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\r\n".
			   "<aspsms>\r\n".
			   "  <Userkey>%s</Userkey>\r\n".
			   "  <Password>%s</Password>\r\n".
			   "%s%s%s%s%s%s%s".
	                   "  %s\r\n".
			   "  <Action>%s</Action>\r\n".
			   "</aspsms>\n",
			   $this->userkey,
			   $this->password,
			   $originator,
			   $this->flashing ? "  <FlashingSMS>1</FlashingSMS>\r\n" : "",
			   $this->blinking ? "  <BlinkingSMS>1</BlinkingSMS>\r\n" : "",
			   $recipients,
			   $deferred,
			   $timezone,
			   $notify,
			   $content,
			   $action);
  	}

	public function setDebug()
	{
    	$this->debug = 1;
	}

	public function sendVCard($name, $phone)
	{
    	$content = sprintf("<VCard><VName>%s</VName><VPhoneNumber>%s</VPhoneNumber></VCard>", htmlspecialchars($name), htmlspecialchars($phone));
    	
    	return $this->send($this->getXML($content, "SendVCard"));
  	}

	public function sendSMS()
  	{ 
    	//return $this->send($this->getXML("<MessageData>".htmlspecialchars($this->content)."</MessageData>", "SendTextSMS"));
    	return $this->send($this->getXML("<MessageData>".  strip_tags($this->content)."</MessageData>", "SendTextSMS"));
  	}

	public function sendLogo()
  	{
    	$c = sprintf("<MCC>%s</MCC><MNC>%s</MNC><URLBinaryFile>%s</URLBinaryFile>", $this->mcc, $this->mnc, $this->logo);
		
    	return $this->send($this->getXML($c, "SendLogo"));
  	}

	public function showCredits()
  	{
    	if ( $this->send($this->getXML("", "ShowCredits")) == 1 ) {
    		return $this->result['Credits'];
    	}
  	}

	public function send($msg)
  	{
    	foreach ($this->servers as $server) 
    	{
      		list($host, $port) = explode(":", $server);
      		$result = $this->sendToServer($msg, $host, $port);
      		
      		if ($result == 1) {
				return $result;
      		}
    	}
    	
    	return 0;
  	}

  	function sendToServer($msg, $host, $port)
  	{
	    if ($this->debug) 
	    {
	    	print "<pre>";
	    	print nl2br(htmlentities($msg));
	    	print "</pre>";
	    	return 1;
	    }
    	else 
    	{
		    $errno = 0;
		    $errdesc = 0;
		    $fp = fsockopen($host, $port, $errno, $errdesc, $this->timeout);

		    if ($fp) 
			{
		    	fputs($fp, "POST /xmlsvr.asp HTTP/1.0\r\n");
		        fputs($fp, "Content-Type: text/xml\r\n");
		        fputs($fp, "Content-Length: ".strlen($msg)."\r\n");
		        fputs($fp, "\r\n");
		        fputs($fp, $msg);
		
		        $content = 0;
		        $reply = array();

		        while ( ! feof($fp) ) 
		        {
		        	$r = fgets($fp, 1024);
		      	  
		        	if ($content) {
		  	    		$reply[] = $r;
			  		} 
			  		else {
			    		if (trim($r) == "") {
			      			$content = 1;
			    		}
			  		}
		        }
				fclose($fp);
		        $this->parseResult(join("", $reply));
		        return $this->result["ErrorCode"];
			} 
		    else 
		    {
				$this->result["ErrorCode"] = 0;
		        $this->result["ErrorDescription"] = "Unable to connect to ".$host.":".$port;
		        
		        return 0;
			}
	    }
	}

	public function getErrorCode()
  	{
    	return $this->result["ErrorCode"];
  	}

  	public function getErrorDescription()
  	{
    	return $this->result["ErrorDescription"];
  	}

  	public function getCredits()
  	{
    	return $this->result["Credits"];
  	}

  	var $result = array();
  	var $nextResult = "";

  	function startElement($parser, $name, $attrs)
  	{
    	if ($name == "ErrorCode" || $name == "ErrorDescription" || $name == "Credits") {
      		$this->nextResult = $name;
    	}
  	}

  	function endElement($parser, $name)
  	{
    	$this->nextResult = "";
  	}

  	function characterData($parser, $data)
  	{
    	if ($this->nextResult != "") {
      		$this->result[$this->nextResult] .= $data;
    	}
  	}

  	function parseResult($result)
  	{
    	// Clear the result
    	$this->result = array("ErrorCode" => 0, "ErrorDescription" => "", "Credits" => "");

    	$p = xml_parser_create();
    	xml_parser_set_option($p, XML_OPTION_CASE_FOLDING, false);
    	xml_set_element_handler($p, array(&$this, "startElement"), array(&$this, "endElement"));
    	xml_set_character_data_handler($p, array(&$this, "characterData"));
	    
    	if (!xml_parse($p, $result, true)) {
	      $this->result["ErrorCode"] = 0;
	      $this->result["ErrorDescription"] = "Unable to parse result.";
	    }

	    xml_parser_free($p);
  	}
}


