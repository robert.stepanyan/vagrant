<?php

class Cubix_2000charge_Config
{
	private static $apiKey_a6_test = 'sk_test_s1Z4EhntuuqyzYqli7r69QNbZg4u20Buu8AUzzaN';
    private static $apiKey_a6 = 'sk_live_8UFr6iBa8z8IKXjsMPIWhHeAJYGpigA5P0Dm7CZz';
	//private static $apiKey_ef = 'sk_live_F3HIl0u7ADUiCcfJC7Hb8AA6EDa22v5BhyHC4rND';
    private static $apiKey_ef = 'sk_live_RxugCJ9pnp2d2y7eCeBAbtxZNCJZ5SyiawR1WwjP';
    private static $apiKey_eguk_test = 'sk_test_SbqtorJpGE0hB6K3RzI8533klHGi6deIS2Oecukq';
	private static $apiKey_eguk = 'sk_live_mBby0AuEkj0wHWEWdBi4mZVx0UHimr6doGPtQ1SG';
	
    public static function getApiKey()
    {
		$app_id = Cubix_Application::getId();
		if ($app_id == APP_EF ) {
			return self::$apiKey_ef;
		}
		elseif($app_id == APP_A6){
			return self::$apiKey_a6;
		}
        elseif($app_id == APP_EG_CO_UK){
            return self::$apiKey_eguk;
        }
		else{
			return self::$apiKey_a6_test;
		}
	}
    
    public static function setApiKey($apiKey)
    {
        self::$apiKey = $apiKey;
    }
    
    private static $apiUrl = 'https://api.2000charge.com/api';
    
    public static function getApiUrl()
    {
        return self::$apiUrl;
    }

    public static function setApiUrl($apiUrl)
    {
        self::$apiUrl = $apiUrl;
    }
}
