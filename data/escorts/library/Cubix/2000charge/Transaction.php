<?php

class Cubix_2000charge_Transaction
{
    const RESOURCE = 'transactions';
    
    public static function getAll()
    {
        $service = new Cubix_2000charge_Request;
        return $service->getAll(self::RESOURCE);
    }

    public static function get($code)
    {
        $service = new Cubix_2000charge_Request;
        return $service->get(self::RESOURCE, $code);
    }

    public static function post($data)
    {
        $service = new Cubix_2000charge_Request;
        return $service->post(self::RESOURCE, $data);
    }
}
