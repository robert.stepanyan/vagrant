<?php

class Cubix_Currency
{
	public static function getDefaultCurrency()
	{
		$db = Zend_Registry::get('db');

		return $db->fetchRow('SELECT id, title, symbol FROM currencies WHERE is_default = 1');
	}

	public static function getCurrency()
	{
		$db = Zend_Registry::get('db');
		$application_id = Cubix_Application::getId();

		return $db->fetchRow('SELECT c.id, c.title, c.symbol FROM currencies LEFT JOIN applications a ON a.currency_id = c.id WHERE a.id = ?', $application_id);
	}

	public static function getCurrencies()
	{
		$db = Zend_Registry::get('db');

		return $db->fetchAll('SELECT id, title, symbol FROM currencies');
	}
	
	public static function getCurrenciesArr()
	{
		$db = Zend_Registry::get('db');

		$cuurs = $db->fetchAll('SELECT id, title, symbol FROM currencies');
		
		$currencies = array();
		foreach ( $cuurs AS $curr ) {
			$currencies[$curr->id] = array('title' => $curr->title, 'symbol' => $curr->symbol);
		} 
		
		return $currencies;
	}
}
