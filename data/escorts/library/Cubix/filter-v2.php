<?php
// AGE OPTIONS
$age_options = array();

$age_options[] = array(
	'title' => '18-20',
	'value' => '18-20'
);

$age_options[] = array(
	'title' => '21-24',
	'value' => '21-24'
);

$age_options[] = array(
	'title' => '25-29',
	'value' => '25-29'
);

$age_options[] = array(
	'title' => '30-35',
	'value' => '30-35'
);

$age_options[] = array(
	'title' => '36-40',
	'value' => '36-40'
);

$age_options[] = array(
	'title' => '41-45',
	'value' => '41-45'
);

$age_options[] = array(
	'title' => '46-49',
	'value' => '46-49'
);

$age_options[] = array(
	'title' => '> 50',
	'value' => '50-100'
);

// ETHNIC OPTIONS
$ethnic_options = array();

foreach ( $DEFINITIONS['ethnic_options'] as $id => $ethnic ) {
	$ethnic_options[] = array(
		'title' => $ethnic,
		'value' => $id,//___clean_str($ethnic),
		'internal_value' => $id
	);
}

// ROYAL HEIGHT OPTIONS
$height_options = array();

$height_options[] = array(
	'title' => /*Cubix_I18n::translate('less_than', array('NUMBER' => '5\' 3'))*/ '< 5\' 3',
	'value' => '1-160'
);

$height_options[] = array(
	'title' => '5\' 3" - 5\' 5"',
	'value' => '160-165'
);

$height_options[] = array(
	'title' => '5\' 5" - 5\' 7"',
	'value' => '166-170'
);

$height_options[] = array(
	'title' => '5\' 7" - 5\' 9"',
	'value' => '171-175'
);

$height_options[] = array(
	'title' => '5\' 9" - 5\' 11"',
	'value' => '176-180'
);

$height_options[] = array(
	'title' => '5\' 11" - 6\' 1"',
	'value' => '181-185'
);

$height_options[] = array(
	'title' => /*Cubix_I18n::translate('more_than', array('NUMBER' => '6\' 1'))*/'> 6\' 1',
	'value' => '185-250'
);

// METRIC HEIGHT OPTIONS
$m_height_options = array();

$m_height_options[] = array(
	'title' => /*Cubix_I18n::translate('less_than', array('NUMBER' => '160')) . */'< 160' . Cubix_I18n::translate('cm'),
	'value' => '1-160'
);

$m_height_options[] = array(
	'title' => '160 - 165 ' . Cubix_I18n::translate('cm'),
	'value' => '160-165'
);

$m_height_options[] = array(
	'title' => '166 - 170 ' . Cubix_I18n::translate('cm'),
	'value' => '166-170'
);

$m_height_options[] = array(
	'title' => '171 - 175 ' . Cubix_I18n::translate('cm'),
	'value' => '171-175'
);

$m_height_options[] = array(
	'title' => '176 - 180 ' . Cubix_I18n::translate('cm'),
	'value' => '176-180'
);

$m_height_options[] = array(
	'title' => '181 - 185 ' . Cubix_I18n::translate('cm'),
	'value' => '181-185'
);

$m_height_options[] = array(
	'title' => /*Cubix_I18n::translate('more_than', array('NUMBER' => '185')) . */'> 185' . Cubix_I18n::translate('cm'),
	'value' => '185-250'
);

//ROYAL WEIGHT OPTIONS
$weight_options = array();

$weight_options[] = array(
	'title' => /*Cubix_I18n::translate('less_than', array('NUMBER' => 100)) . */'< 100' . Cubix_I18n::translate('lbs'),
	'value' => '1-100'
);

$weight_options[] = array(
	'title' => '100-110 ' . Cubix_I18n::translate('lbs'),
	'value' => '100-110'
);

$weight_options[] = array(
	'title' => '110-120 ' . Cubix_I18n::translate('lbs'),
	'value' => '110-120'
);

$weight_options[] = array(
	'title' => '120-130 ' . Cubix_I18n::translate('lbs'),
	'value' => '120-130'
);

$weight_options[] = array(
	'title' => '130-140 ' . Cubix_I18n::translate('lbs'),
	'value' => '130-140'
);

$weight_options[] = array(
	'title' => '140-150 ' . Cubix_I18n::translate('lbs'),
	'value' => '140-150'
);

$weight_options[] = array(
	'title' => /*Cubix_I18n::translate('more_than', array('NUMBER' => 150)) . */'> 150' . Cubix_I18n::translate('lbs'),
	'value' => '150-250'
);

//METRIC WEIGHT OPTIONS
$m_weight_options = array();

/*$m_weight_options[] = array(
	'title' => Cubix_I18n::translate('less_than', array('NUMBER' => 100)) . ' ' . Cubix_I18n::translate('lbs'),
	'value' => 'weight__100'
);*/

$m_weight_options[] = array(
	'title' => '41-45 ' . Cubix_I18n::translate('kg'),
	'value' => '41-45'
);

$m_weight_options[] = array(
	'title' => '46-50 ' . Cubix_I18n::translate('kg'),
	'value' => '46-50'
);

$m_weight_options[] = array(
	'title' => '51-55 ' . Cubix_I18n::translate('kg'),
	'value' => '51-55'
);

$m_weight_options[] = array(
	'title' => '56-60 ' . Cubix_I18n::translate('kg'),
	'value' => '56-60'
);

$m_weight_options[] = array(
	'title' => '61-65 ' . Cubix_I18n::translate('kg'),
	'value' => '61-65'
);

$m_weight_options[] = array(
	'title' => '66-70 ' . Cubix_I18n::translate('kg'),
	'value' => '66-70'
);

$m_weight_options[] = array(
	'title' => '71-75 ' . Cubix_I18n::translate('kg'),
	'value' => '71-75'
);

$m_weight_options[] = array(
	'title' => /*Cubix_I18n::translate('more_than', array('NUMBER' => 76)) . */'> 76' . Cubix_I18n::translate('kg'),
	'value' => '76-150'
);

// BREAST SIZE OPTIONS
$breast_size_options = array();

foreach ( $DEFINITIONS['breast_size_options'] as $i => $breast_size ) {
	$breast_size_options[] = array(
		'title' => $breast_size,
		'value' => $breast_size
	);
}

// HAIR COLOR OPTIONS
$hair_color_options = array();

foreach ( $DEFINITIONS['hair_color_options'] as $color => $hair_color ) {
	$hair_color_options[] = array(
		'title' => $hair_color,
		'value' => $color
	);
}

// HAIR LENGTH OPTIONS
$hair_length_options = array();

foreach ( $DEFINITIONS['hair_length_options'] as $length => $hair_length ) {
	$hair_length_options[] = array(
		'title' => $hair_length,
		'value' => $length
	);
}

// EYE COLOR OPTIONS
$eye_color_options = array();

foreach ( $DEFINITIONS['eye_color_options'] as $color => $eye_color ) {
	$eye_color_options[] = array(
		'title' => $eye_color,
		'value' => $color
	);
}

// AVAILABLE FOR OPTIONS
$available_for_options = array();
foreach ( $DEFINITIONS['filter_availability_options'] as $id => $av ) {
	
	if ( Cubix_Application::getId() == APP_EF || Cubix_Application::getId() == APP_6A ) {
		if ( $id == 'i_3' ) {
			continue;
		}
	} 
	
	$available_for_options[] = array(
		'title' => $av,
		'value' => $id
	);
}

$available_for_options_incall = array();
foreach ( $DEFINITIONS['filter_availability_options_incall'] as $id => $av ) {	
	$available_for_options_incall[] = array(
		'title' => $av,
		'value' => $id
	);
}
$available_for_options_outcall = array();
foreach ( $DEFINITIONS['filter_availability_options_outcall'] as $id => $av ) {	
	$available_for_options_outcall[] = array(
		'title' => $av,
		'value' => $id
	);
}

if ( Cubix_Application::getId() == APP_ED){
	foreach ( $DEFINITIONS['travel_places'] as $id => $av ) {	
		$travel_places[] = array(
			'title' => $av,
			'value' => $id
		);
	}
	
	for ( $i = 5; $i >= 1; $i-- ){
		$incall_hotel_rooms[] = array(
			'title' => Cubix_I18n::translate('pv2_label_hotel_room'),
			'value' => $i
		);
	}
}
/*$available_for_options = array();

$available_for_options[] = array(
	'title' => Cubix_I18n::translate('outcall'),
	'value' => '1'
);

$available_for_options[] = array(
	'title' => Cubix_I18n::translate('incall'),
	'value' => '2'
);*/

// SERVICE FOR OPTIONS
$service_for_options = array();

foreach ( $DEFINITIONS['sex_availability_options'] as $id => $opt ) {
	$service_for_options[] = array(
		'title' => $opt,
		'value' => ___clean_str($id)
	);
}

// SMOKER OPTIONS
$smoker_options = array();

$smoker_options[] = array(
	'title' => Cubix_I18n::translate('yes'),
	'value' => 1
);

$smoker_options[] = array(
	'title' => Cubix_I18n::translate('no'),
	'value' => 2
);

$smoker_options[] = array(
	'title' => Cubix_I18n::translate('ocasionally'),
	'value' => 3
);

// LANGUAGE OPTIONS
$language_options = array();

$k = 0;
foreach ( $DEFINITIONS['language_options'] as $id => $lang ) {
	$language_options[$k] = array(
		'title' => $lang,
		'value' => ___clean_str($id),
		'internal_value' => $id,
	);
	
	if ( (Cubix_Application::getId() == APP_EF && $id == 'it') || (Cubix_Application::getId() == APP_6B && $id == 'pt') ) {
		$key = $k;
	}
	$k++;
}

if ( (Cubix_Application::getId() == APP_EF || Cubix_Application::getId() == APP_6B ) && $key ) {
		
	$tmp = $language_options[$key];
	unset($language_options[$key]);

	$language_options = array_merge(array($tmp), $language_options);
}


if ( Zend_Registry::isRegistered('escorts_config') ) {
	$conf = Zend_Registry::get('escorts_config');
}

if ( $conf['useMetricSystem'] )
{
	$height = $m_height_options;
	$weight = $m_weight_options;
}
else
{
	$height = $height_options;
	$weight = $weight_options;
}

$services = require('services.php');
//SERVICE OPTIONS
$service_options = array();

if ( count($services['common']) ) {
	foreach( $services['common'] as $i => $service ) {
		$service_options[] = array(
			'title' => $service,
			'value' => $i,
			'group' => 'common'
		);
	}
}

if ( count($services['extra']) ) {
	foreach( $services['extra'] as $i => $service ) {
		$service_options[] = array(
			'title' => $service,
			'value' => $i,
			'group' => 'extra'	
		);
	}
}

if ( count($services['fetish']) ) {
	foreach( $services['fetish'] as $i => $service ) {
		$service_options[] = array(
			'title' => $service,
			'value' => $i,
			'group' => 'fetish'
		);
	}
}

if ( isset($services['virtual']) && count($services['virtual']) ) {
	foreach( $services['virtual'] as $i => $service ) {
		$service_options[] = array(
			'title' => $service,
			'value' => $i,
			'group' => 'fetish'
		);
	}
}

if ($services['massage'] && count($services['massage']) ) {
	foreach( $services['massage'] as $i => $service ) {
		$service_options[] = array(
			'title' => $service,
			'value' => $i,
			'group' => 'fetish'
		);
	}
}

if ($services['companion'] && count($services['companion']) ) {
	foreach( $services['companion'] as $i => $service ) {
		$service_options[] = array(
			'title' => $service,
			'value' => $i,
			'group' => 'fetish'
		);
	}
}

if ( Zend_Registry::isRegistered('db') ) {
	$db = Zend_Registry::get('db');
	$cache = Zend_Registry::get('cache');
	
	$cache_key = "nat_filter_v2_list_" . Cubix_Application::getId();
	if ( ! $nationalities = $cache->load($cache_key) ) {
		
		$nationalities = $db->fetchAll('SELECT * FROM nationalities');
		
		$cache->save($nationalities, $cache_key, array());
	}
	
	$nationality_options = array();
	
	$k = 0;
	foreach ( $nationalities as $n ) {
		$nationality_options[$k] = array(
			'title' => $n->{'title_' . Cubix_I18n::getLang()},
			'value' => $n->id
		);
			
		if ( Cubix_Application::getId() == APP_EF && $n->id == 22 ) {
			$key = $k;
		}
		
		if ( Cubix_Application::getId() == APP_6A && $n->id == 15 ) {
			$key = $k;
		}
		
		$k++;
	}
	
	if ( (Cubix_Application::getId() == APP_EF && $key) || (Cubix_Application::getId() == APP_6A && $key) ) {
		
		$tmp = $nationality_options[$key];
		unset($nationality_options[$key]);
		
		$nationality_options = array_merge(array($tmp), $nationality_options);
	}
}

$verified_options = array();

$verified_options[] = array(
	'title' => Cubix_I18n::translate('100_verified'),
	'value' => 2
);

// ETHNIC OPTIONS
$keywords_options = array();

if ( Cubix_Application::getId() == APP_ED) {
	$def_keywords = $DEFINITIONS['keywords_ed'];
}
else{
	$def_keywords = $DEFINITIONS['keywords']; 
}

foreach ( $def_keywords as $id => $keyword ) {
    $keywords_options[] = array(
        'title' => $keyword,
        'value' => $id,
        'internal_value' => $id
    );
}

$orientation_options = array();

foreach ( $DEFINITIONS['sexual_orientation_options'] as $id => $orientation ) {
    $orientation_options[] = array(
        'title' => $orientation,
        'value' => $id,
        'internal_value' => $id
    );
}

foreach ( $DEFINITIONS['pubic_hair_options'] as $id => $pubic_hair ) {
	$pubic_hair_options[] = array(
		'title' => $pubic_hair,
		'value' => $id
	);
}

$tatoo_options = array();
foreach ( $DEFINITIONS['tatoo_options'] as $id => $tatoo ) {
	$tatoo_options[] = array(
		'title' => $tatoo,
		'value' => $id
	);
}

$piercing_options = array();
foreach ( $DEFINITIONS['piercing_options'] as $id => $piercing ) {
	$piercing_options[] = array(
		'title' => $piercing,
		'value' => $id
	);
}

$filter = array(
	'services' => $service_options,
	'available-for' => $available_for_options,
	'available-for-incall' => $available_for_options_incall,
	'available-for-outcall' => $available_for_options_outcall,
	'service-for' => $service_for_options,
	'cup-size' => $breast_size_options,
	'age' => $age_options,
	'language' => $language_options,
	'ethnicity' => $ethnic_options,
	'hair-color' => $hair_color_options,
	'hair-length' => $hair_length_options,
	'height' => $height,
	'weight' => $weight,
	'm_height'	=> $m_height_options,
	'm_weight'	=> $m_weight_options,
	'eye-color' => $eye_color_options,
	'smoker' => $smoker_options,
	'nationality' => $nationality_options,
	'verified' => $verified_options,
	'orientation' => $orientation_options,
);

if ( in_array(Cubix_Application::getId(),array(APP_EG_CO_UK, APP_EG, APP_6C, APP_6B, APP_ED) )) {
    $filter['keywords'] = $keywords_options;
}

if ( Cubix_Application::getId() == APP_ED) {
	$filter['pubic-hair'] = $pubic_hair_options;
	$filter['tatoo'] = $tatoo_options;
	$filter['piercing'] = $piercing_options;
	$filter['available-for-incall-hotel'] = $incall_hotel_rooms;
    $filter['available-for-travel'] = $travel_places;
	
	unset($filter['age']);
	unset($filter['height']);
	unset($filter['m_height']);
	unset($filter['weight']);
	unset($filter['m_weight']);
}

return $filter;
