<?php
class Cubix_Captcha {

	static public function verify($captcha)
	{
		//$conf = Zend_Registry::get('config')->recaptcha;
		$private_key = '6LdcRr8SAAAAAJWfsNcIsH2abOMD6n7-deEYdxrd';//$conf->privateKey;
		$ip = Cubix_Geoip::getIP();
		$challenge = isset($_REQUEST['recaptcha_challenge_field']) ?
		$_REQUEST['recaptcha_challenge_field'] : null;

		$client = new Zend_Http_Client('http://www.google.com/recaptcha/api/verify');
		
		$client->setParameterPost(array(
			'privatekey' => $private_key,
			'remoteip' => $ip,
			'challenge' => $challenge,
			'response' => $captcha
		));

		$client->request('POST');
		
		list($status, $error) = explode("\n", $client->getLastResponse()->getBody());

		if ( 'true' == $status ) {
			return true;
		}
		else {
			return $error;
		}

		/*$descs = array(
			'invalid-site-public-key' => 'We weren\'t able to verify the public key.',
			'invalid-site-private-key' => 'We weren\'t able to verify the private key.',
			'invalid-request-cookie' => 'The challenge parameter of the verify script was incorrect.',
			'incorrect-captcha-sol' => 'The CAPTCHA solution was incorrect.',
			'verify-params-incorrect' => 'The parameters to /verify were incorrect, make sure you are passing all the required parameters.',
			'invalid-referrer' => 'reCAPTCHA API keys are tied to a specific domain name for security reasons.',
			'recaptcha-not-reachable' => 'reCAPTCHA never returns this error code. A plugin should manually return this code in the unlikely event that it is unable to contact the reCAPTCHA verify server.'
		);

		$exception = new Exception(isset($descs[$error]) ? $descs[$error] : 'Unknown error');
			throw $exception;*/
	}
}