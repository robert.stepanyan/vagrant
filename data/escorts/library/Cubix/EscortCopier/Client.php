<?php

class Cubix_EscortCopier_Client extends Cubix_JsonRpc_Client
{
	protected $_application_id;
	protected $_username;
	protected $_password;
	protected $_authenticated = false;
	
	public function getClient()
	{
		$client = parent::getClient();
		$client->setAuth('babazeo', 'GnaMer1a');
		return $client;
	}
	
	public function __construct($application_id, $for_sync = false)
	{
		$applications = Cubix_EscortCopier::getTargetApplications();
		if ( ! isset($applications[$application_id]) ) {
			throw new Exception('Invalid application id ' . $application_id);
		}

		$this->_application_id = $application_id;
		$config = $applications[$application_id];
		
		if ( $for_sync ) {
			$this->_username = $config->sync_username;
			$this->_password = $config->sync_password;
		} else {
			$this->_username = $config->username;
			$this->_password = $config->password;
		}
		parent::__construct($config->backend);
		
		//Creating client with custom timeout
		$client = new Zend_Http_Client($config->backend, array('timeout' => 90));
		$this->setClient($client);

		$jar = new Zend_Http_CookieJar();
		$this->getClient()->setCookieJar($jar);
	}
	
	public function authenticate()
	{
		$client = $this->getClient();
		$client->setUri($this->_url . '/auth');
		$client->setParameterPost('username', $this->_username);
		$client->setParameterPost('password', $this->_password);
		$client->setConfig(array('maxredirects' => 0));
		$client->request('POST');
		$redirect = $client->getLastResponse()->getHeader('location');

		if ( ! $redirect || '/auth' == substr($redirect, -5) ) {
			throw new Exception('Could not authenticate');
		}

		$this->authenticated = true;
	}

	public function call($method, array $params = array())
	{
		if ( ! $this->_authenticated ) {
			$this->authenticate();
		}

		$this->getClient()->setUri($this->_url . '/copy-listener');
		return parent::call($method, $params);
	}
}

