<?php

class Cubix_EscortCopier_JsonRpc_CopyListener extends Cubix_JsonRpc_Module
{
	const RESULT_ESCORT_ALREADY_TAKEN = 1;
	const RESULT_ESCORT_TAKEN_SUCCESSFULLY = 2;
	const RESULT_ESCORT_VALIDATION_ERROR = 4;
	const RESULT_ESCORT_PHOTO_SUCCESS = 5;
	const RESULT_ESCORT_PHOTO_FAIL = 6;
	const RESULT_ESCORT_DOESNT_EXIST = 7;
	const RESULT_ESCORT_OLD_PHOTO_REMOVED = 8;

	const RESULT_AGENCY_ALREADY_TAKEN = 91;
	const RESULT_AGENCY_TAKEN_SUCCESSFULLY = 92;
	const RESULT_AGENCY_ALREADY_EXISTS = 93;
	const RESULT_AGENCY_VALIDATION_ERROR = 94;
	const RESULT_AGENCY_LOGO_SUCCESS = 95;
	const RESULT_AGENCY_LOGO_FAIL = 96;
	const RESULT_AGENCY_DOESNT_EXIST = 97;


	public function getName()
	{
		return 'CopyListener';
	}

	public function getInterface()
	{
		return array(
			'takeCopyOfEscort', 'takeCopyOfEscortPhoto', 'removeOldEscortPhotos','takeCopyOfEscortStatuses',
			'takeCopyOfAgency', 'takeCopyOfAgencyLogo','takeCopyOfCities', 'takeCopyOfEscortBirthDate','takeCopyOfEscortKeywordsMessengers',
			'getMyUserId', 'activateTransitPackage', 'removeTransitPackage', 'ping'
		);
	}

	static public function getRequest()
	{
		return self::getFrontController()->getRequest();
	}

	static public function getFrontController()
	{
		return Zend_Controller_Front::getInstance();
	}

	public function takeCopyOfEscort($data, $for_sync = false)
	{
		$warnings = array();

		$new_agency = $this->getNewAgencyId($data['agency_id'], $data['application_id']);
		$new_agency_id = $new_agency->agency_id;
		$new_user_id = $new_agency->user_id;
		$data['new_user_id'] = $new_user_id;
		
		
		if ( $data['agency_id'] ) {
			if ( $new_agency_id  ) {
				$data['agency_id'] = $new_agency_id;
			}
			elseif ( $new_agency_id = $this->getAgencyIdByName($data['agency_name']) ) {
				$data['agency_id'] = $new_agency_id;
			}
			else {
				return array('result' => self::RESULT_AGENCY_DOESNT_EXIST, 'message' => 'There is no such agency, maybe copying failed before?');
			}

		}
		
		if ( $escort_id = $this->getNewEscortId($data['id'], $data['application_id']) ) {
			//return array('result' => self::RESULT_ESCORT_ALREADY_TAKEN, 'message' => 'Escort has already been copied');
		}
		
		
		
		$orig_escort_id = $data['id'];
		$orig_app_id = $data['application_id'];
		
		// make phone prefix
		$prefix = Model_Countries::byId($data['phone_country_id']);
		$prefix = $prefix->id . '-' . $prefix->phone_prefix . '-' . $prefix->ndd_prefix;

		$country_model = new Cubix_Geography_Countries();
		$country_id = null;
		if ( $data['country_slug'] ) {
			$country_id = $country_model->getIdBySlug($data['country_slug']);
		} else {
			$country = $country_model->get($data['country_id']);
			$country_id = $country->id;
		}

		if ( ! $country_id ) {
			$warnings[] = 'Application doesn\'t support escort\'s country';
		}
		
		//$warnings[] = 'Country has not been set, please do it manually';
		//$warnings[] = 'Please be warned that working locations and home city are not copied';
		
		// basic params mapping {{{
		$params = array(
			'application_id' => Cubix_Application::getId(),
			// *********************************************
			// ******** login & biography tab **************
			// *********************************************

			// login info
			'showname' => $data['showname'],
			'slogan' => $data['slogan'],
			'reg_email' => $data['reg_email'],

			// contact
			'phone_prefix' => $prefix,
			'phone_number' => $data['phone_number'],
			'phone_instr' => $data['phone_instr'],
			'phone_instr_no_withheld' => $data['phone_instr_no_withheld'],
			'phone_instr_other' => $data['phone_instr_other'],
			'email' => $data['email'],
			'website' => $data['website'],
			'club_name' => $data['club_name'],
			'street' => $data['street'],
			'street_no' => $data['street_no'],

			// biography part 1
			'ethnicity' => $data['ethnicity'],
			'nationality_id' => $data['nationality_id'],
			'gender' => $data['gender'],
			'age' => $data['birth_date'] ? $this->dateToAge($data['birth_date']) : null,
            'birth_date' => $data['birth_date'] ? $this->dateToAge($data['birth_date']) : null,
			'characteristics' => $data['characteristics'],
			
			// biography part 2
			'height' => $data['height'],
			'weight' => $data['weight'],
			'bust' => $data['bust'],
			'waist' => $data['waist'],
			'hip' => $data['hip'],
			'cup_size' => $data['cup_size'],
			'hair_color' => $data['hair_color'],
			'hair_length' => $data['hair_length'],
			'eye_color' => $data['eye_color'],
			'shoe_size' => $data['shoe_size'],
			'dress_size' => $data['dress_size'],
			'pubic_hair' => $data['pubic_hair'],
			'is_smoking' => $data['is_smoking'],
			'is_drinking' => $data['is_drinking'],
			'tatoo'	=> $data['tatoo'],
			'piercing'	=> $data['piercing'],
			'breast_type'	=> $data['breast_type'],
			
			// spoken languages
			'langs' => array(),

			// about

			// *****************************************
			// ********** services tab *****************
			// *****************************************

			// services offered for
			'sex_availability' => explode(',', $data['sex_availability']),

			// sexual orientation
			'sex_orientation' => $data['sex_orientation'],
			
			// additional 

			// services
			'services' => array(),
			'service_prices' => array(),
			'service_currencies' => array(),

			// *****************************************
			// ********** working tab ******************
			// *****************************************

			// working times
			'available_24_7' => $data['available_24_7'],
			'night_escort' => $data['night_escort'],
			'day_index' => array(),
			'time_from' => array(),
			'time_from_m' => array(),
			'time_to' => array(),
			'time_to_m' => array(),
			'night_escorts' => array(),

			// avilable for
			'incall' => $data['incall_type'] ? 1 : null,
			'incall_type' => $data['incall_type'],
			'incall_hotel_room' => $data['incall_hotel_room'],
			'incall_other' => $data['incall_other'],
			
			'outcall' => $data['outcall_type'] ? 1 : null,
			'outcall_type' => $data['outcall_type'],
			'outcall_other' => $data['outcall_other'],

			'zip' => $data['zip'],

			// locations
			//'home_city' => $data['home_city_id'],
			//'base_city_id' => $data['city_id'],
			'country_id'	=> $country_id,
			'locations' => array(),

			// vacation
			'vac_date_from' => $data['vac_date_from'],
			'vac_date_to' => $data['vac_date_to'],

			// ******************************************
			// ************ prices tab ******************
			// ******************************************
			
			// special rates / other
			'special_rates' => $data['special_rates'],

			// rates
			'rates' => array('incall' => array(), 'outcall' => array()),
		);

		if (!$escort_id)
        {
            $params['sales_user_id'] = $this->getMyUserId();
        }

		if (Cubix_Application::getId() == APP_ED){
			$params['type'] = 1;
			$params['mark_not_new'] = $data['mark_not_new'];
			
		}

		if (Cubix_Application::getId() == APP_ED && ! $for_sync) {
			$params['sales_user_id'] = 1;
		}

        if (in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK))) {
            if ($data['last_hand_verification_date'])
                $params['last_hand_verification_date'] = $data['last_hand_verification_date'];

            if ($data['keywords'])
                $params['keywords'] = $data['keywords'];

            if ($data['telegram'])
                $params['telegram'] = $data['telegram'];

            if ($data['viber'])
                $params['viber'] = $data['viber'];

            if ($data['whatsapp'])
                $params['whatsapp'] = $data['whatsapp'];

            if ($data['ssignal'])
                $params['ssignal'] = $data['ssignal'];

        }

		foreach($data['times'] as $i => $r) {
			$params['day_index'][$i + 1] = $r['day_index'] ;
			$params['time_from'][$i + 1] = $r['time_from'];
			$params['time_from_m'][$i + 1] = $r['time_from_m'];
			$params['time_to'][$i + 1] = $r['time_to'];
			$params['time_to_m'][$i + 1] = $r['time_to_m'];
			
			if ( $r['night_escorts'] ) {
				$params['night_escorts'][] = $i+1;
			}
		}
		
		// <editor-fold defaultstate="collapsed" desc="Mapping cities">
		if ( $data['working_locations'] && count($data['working_locations']) ) {
			
			$city_names = array();
			foreach($data['working_locations'] as $city) {
				$city_names[] = $city['title'];
			}
			
			$new_cities = $this->getCitiesByName($city_names, $country_id);//Cities with the new ids
			
			if ( count($data['working_locations']) != count($new_cities) ) {
				$missing_cities = array();
				foreach($city_names as $c_n) {
					if ( ! $new_cities[$c_n] ) {
						$missing_cities[] = $c_n;
					}
				}
				return array('result' => self::RESULT_ESCORT_VALIDATION_ERROR, 'message' => 'There were validation errors', 'errors' => array('Missing cities (' . implode(',', $missing_cities) . ').'));
			}
			
			$locations = array();
			$base_city_id = null;
			foreach($data['working_locations'] as $city) {
				if ( $new_cities[$city['title']] ) {
					$locations[] = $new_cities[$city['title']];
					if ( $city['is_main'] ) {
						$base_city_id = $new_cities[$city['title']];
					}
				}
			}
			
			$params['locations'] = $locations;
			$params['base_city_id'] = $base_city_id;
		}
        // </editor-fold>
		
		
		// }}}

		// advanced params mapping {{{

		// spoken languages
		foreach ( $data['langs'] as $lang ) {
			if ( Cubix_Application::getId() == APP_ED ) {
				switch($lang['level']) {
					case 1:
						$lang['level'] = 24;
						break;
					case 2:
						$lang['level'] = 49;
						break;
					case 3:
						$lang['level'] = 74;
						break;
					case 4:
						$lang['level'] = 100;
						break;
				}
			}elseif(Cubix_Application::getId() == APP_EG_CO_UK){

			    if(in_array($lang['level'],array(1,2,3,4))){

                }elseif ($lang['level'] > 1 && $lang['level'] <= 24){
                    $lang['level'] = 1;
                }elseif ($lang['level'] >= 24 && $lang['level'] <= 49){
                    $lang['level'] = 2;
                }elseif ($lang['level'] >= 50 && $lang['level'] <= 74){
                    $lang['level'] = 3;
                }elseif ($lang['level'] >= 75 && $lang['level'] <= 100){
                    $lang['level'] = 4;
                }

            }
			$params['langs'][] = $lang['lang_id'] . ':' . $lang['level'];
		}

		
		/* TOURS */
		if ( $data['tours'] ) {
			
			$missing_countries = array();
			$city_names = array();
			foreach($data['tours'] as $tour) {
				$city_names[] = $tour['city_title'];
			}
			
			$new_cities = $this->getCitiesByName($city_names, $country_id);//Cities with the new ids
			
			if ( count($data['tours']) != count($new_cities) ) {
				$missing_cities = array();
				foreach($city_names as $c_n) {
					if ( ! $new_cities[$c_n] ) {
						$missing_cities[] = $c_n;
					}
				}
				return array('result' => self::RESULT_ESCORT_VALIDATION_ERROR, 'message' => 'There were validation errors', 'errors' => array('Missing cities (' . implode(',', $missing_cities) . ').'));
			}
			
			foreach($data['tours'] as $tour) {
				$country_id = $country_model->getIdBySlug($tour['country_slug']);
				if ( $country_id ) {
					
					$tour['date_to'] = Cubix_Application::getId() == APP_ED ? strtotime(date('d-m-Y', $tour['date_to'])) : $tour['date_to'];
					$tour_data = $country_id. ':' .$new_cities[$tour['city_title']]. ':' .$tour['date_from']. ':' .$tour['date_to']. ':' .$tour['phone'];
					if( in_array(Cubix_Application::getId(), array(APP_EF, APP_6A, APP_BL, APP_AE, APP_6B, APP_6C, APP_EG, APP_EG_CO_UK))){
						$tour_data .= $tour['minutes_from']. ':' .$tour['minutes_to'];
					}
					$params['tours'][] = $tour_data;
					
				} else {
					$missing_countries[] = $tour['country_title'];
				}
			}
			
			if ( count($missing_countries) ) {
				return array('result' => self::RESULT_ESCORT_VALIDATION_ERROR, 'message' => 'There were validation errors', 'errors' => array('Missing countries (' . implode(',', $missing_countries) . ').'));
			}
		}
		/* TOURS */
		
		/*blocked countries*/
		if ( $data['block_countries'] ) {
			$block_countries_ids = array();
			$missing_countries = array();

			foreach($data['block_countries'] as $c) {
				$country_id = $country_model->getIdBySlug($c['slug']);
				if ( $country_id ) {
					$block_countries_ids[] = $country_id;
				} else {
					$missing_countries[] = $c['title'];
				}
			}

			if ( count($missing_countries) ) {
				$warnings[] = 'Missing countries (' . implode(',', $missing_countries) . ')';
				//return array('result' => self::RESULT_ESCORT_VALIDATION_ERROR, 'message' => 'There were validation errors', 'errors' => array('Missing countries (' . implode(',', $missing_countries) . ').'));
			}
			$params['block_countries'] = $block_countries_ids;
		}
		/*blocked countries*/
		
		
		
		// about
		//different logic for ed
		//ed has only en about_me text, if it's empty, fill it with another language
		if ( Cubix_Application::getId() == APP_ED && empty($data['about_en']) ) {
			foreach ( Cubix_I18n::getLangs() as $lang ) {
				if ( ! empty($data['about_' . $lang]) ) {
					$params['about_en'] = $data['about_' . $lang];
					BREAK;
				}
			}
			
		} else {
			foreach ( Cubix_I18n::getLangs() as $lang ) {
				$params['about_' . $lang] = ! empty($data['about_' . $lang]) ? $data['about_' . $lang] : '';
			}
		}
		

		// additional
		foreach ( Cubix_I18n::getLangs() as $lang ) {
			$params['additional_service_' . $lang] = ! empty($data['additional_service_' . $lang]) ? $data['additional_service_' . $lang] : '';
		}

		// services
		foreach ( $data['services'] as $service ) {
			$params['services'][] = $service['service_id'];
			$params['service_prices'][$service['service_id']] = $service['price'];
			$params['service_currencies'][$service['service_id']] = $service['currency_id'];
		}

		// locations
		//foreach ( $data['cities'] as $city ) {
		//	$params['locations'][] = $city['city_id'];
		//}

		// rates
		foreach ( $data['rates'] as $rate ) {
			$_rate = array();

			if ( isset($rate['type']) && $rate['type'] ) {
				$_rate['type'] = $rate['type'];
				$_rate['price'] = $rate['price'];
				$_rate['currency'] = $rate['currency_id'];
			}
			else {
				$_rate['time'] = $rate['time'];
				$_rate['unit'] = $rate['time_unit'];
				$_rate['price'] = $rate['price'];
				$_rate['currency'] = $rate['currency_id'];
			}

			$_rate = json_encode($_rate);

			if ( $rate['availability'] == 1 ) {
				$params['rates']['incall'][] = $_rate;
			}
			elseif ( $rate['availability'] == 2 ) {
				$params['rates']['outcall'][] = $_rate;
			}
		}
		
		// for agency and independant escorts
		if ( $data['agency_id'] ) {
			$params['agency_id'] = $data['agency_id'];
		}
		else {
			$params['username'] = $data['username'];
			$params['password'] = $this->makeNewPassword();
		}
		// }}}

		$request = new Zend_Controller_Request_Http();
		foreach ( $params as $param => $value ) {
			$request->setParam($param, $value);
		}
		
		if ( $for_sync ) {
			$request->setParam('sync_escort', 1);
		}
		
		if ( $for_sync && $escort_id ) {
			
			$request->setParam('id', $escort_id);
			$result = $this->doDispatch($request, 'default', 'escorts-v2', 'edit');
			
			if ( 'error' == $result->status ) {
				return array('result' => self::RESULT_ESCORT_VALIDATION_ERROR, 'message' => 'There were validation errors', 'errors' => $result->msgs);
			}
			
			return array('result' => self::RESULT_ESCORT_TAKEN_SUCCESSFULLY, 'message' => 'The escort has been successfully taken', 'warnings' => $warnings, 'id' => $escort_id);
		} else {
			
			$result = $this->doDispatch($request, 'default', 'escorts-v2', 'create');

			if ( 'error' == $result->status ) {
				return array('result' => self::RESULT_ESCORT_VALIDATION_ERROR, 'message' => 'There were validation errors', 'errors' => $result->msgs);
			}
			else {
				parent::db()->delete('copied_escorts', array(
					'orig_escort_id = ?' => $orig_escort_id,
					'orig_app_id = ?' => $orig_app_id
				));
				
				parent::db()->insert('copied_escorts', array(
					'escort_id' => $result->escort_id,
					'orig_app_id' => $orig_app_id,
					'orig_escort_id' => $orig_escort_id
				));
				return array('result' => self::RESULT_ESCORT_TAKEN_SUCCESSFULLY, 'message' => 'The escort has been successfully taken', 'warnings' => $warnings, 'id' => $result->escort_id);
			}
		}
		
//		return $result;
	}

	public function takeCopyOfEscortPhoto($escort_id, $photo, $application_id, $photo_url, $is_private)
	{
		if ( ! $new_escort_id = $this->getNewEscortId($escort_id, $application_id) ) {
			return array('result' => self::RESULT_ESCORT_DOESNT_EXIST, 'message' => 'There is no such escort');
		}

		/*if ($application_id == 69 || $application_id == 17) {
			$photo_url = str_replace('https://', "http://", $photo_url);
		}*/

		$http = new Zend_Http_Client($photo_url, array(
		'maxredirects' => 0,
		'timeout'      => 100));
		$http->setParameterGet('origano', 'true');

		try {
			$http->request('GET');
			$binary = $http->getLastResponse()->getBody();

//			if($escort_id == 71309){
//				return array('result' => self::RESULT_ESCORT_PHOTO_FAIL, 'message' => 'Failed to copy escort photo', 'error' => $binary);
//			}
			}
		catch ( Exception $e ) {
			return array('result' => self::RESULT_ESCORT_PHOTO_FAIL, 'message' => 'Could not get escort original photo', 'error' => $e->__toString());
		}

		$path = sys_get_temp_dir();
		$tmp_file = tempnam($path, 'ECEscortPhoto');
		file_put_contents($tmp_file, $binary);

		$_FILES['Filedata']['tmp_name'] = $tmp_file;
		$_FILES['Filedata']['name'] = $http->getUri()->getPath();

		$request = new Zend_Controller_Request_Http();
		$request->setParam('escort_id', $new_escort_id);
		$request->setParam('is_private', (bool) $is_private);
		$request->setParam('no_validation', (bool) 1);
		
		$result = $this->doDispatch($request, 'default', 'escorts-v2', 'upload-photo');
		@unlink($tmp_file);

		if ( 'success' == $result->status ) {
			$args = @unserialize($photo['args']);
			if ( $args ) {
				$request->setParam('photo_id', $result->id);
				$request->setParam('act', 'adjust');
				foreach ( $args as $key => $value ) {
					$request->setParam($key, $value);
				}
				$adjust_result = $this->doDispatch($request, 'default', 'escorts-v2', 'do-photos');
			}

			if ( $photo['is_main'] ) {
				$request->setParam('photos', array($result->id));
				$request->setParam('act', 'set-main');
				$adjust_result = $this->doDispatch($request, 'default', 'escorts-v2', 'do-photos');
			}

			return array('result' => self::RESULT_ESCORT_PHOTO_SUCCESS, 'message' => 'Escort photo has been successfully copied', 'image_url' => $result->photo->image_url);
		}
		else {
			return array('result' => self::RESULT_ESCORT_PHOTO_FAIL, 'message' => 'Failed to copy escort photo', 'error' => $result->msg);
		}
	}

	public function removeOldEscortPhotos($escort_id, $application_id)
	{
		
		if ( ! $new_escort_id = $this->getNewEscortId($escort_id, $application_id) ) {
			return array('result' => self::RESULT_ESCORT_DOESNT_EXIST, 'message' => 'There is no such escort');
		}
		
		parent::db()->delete('escort_photos', parent::db()->quoteInto('escort_id = ?', $new_escort_id));
		return array('result' => self::RESULT_ESCORT_OLD_PHOTO_REMOVED, 'message' => 'escort old photos are removed');		
	}		
	
	public function takeCopyOfAgency($data, $for_sync = false)
	{
		$new_agency = $this->getNewAgencyId($data['agency_data']['id'], $data['agency_data']['application_id']);
		$new_agency_id = $new_agency->agency_id;
		$new_user_id = $new_agency->user_id;
		
		if ( $new_agency_id ) {
			if ( ! $for_sync ) {
				return array('result' => self::RESULT_AGENCY_ALREADY_TAKEN, 'id' => $new_agency_id, 'message' => 'Agency has already been copied');
			}
		}
		elseif ( $agency_id = $this->getAgencyIdByName($data['agency_data']['name']) ) {
			return array('result' => self::RESULT_AGENCY_ALREADY_EXISTS, 'id' => $agency_id, 'message' => 'Agency with same name exists');
		}

		$agency_data = $data['agency_data'];

		$orig_agency_id = $agency_data['id'];
		$orig_app_id = $agency_data['application_id'];

		// country code fix by Artash {{{
		if ( $agency_data['phone'] ) {
			if ( preg_match('/^(\+|00)/', trim($agency_data['phone'])) ) {
				$countyModel = new Model_Countries();
				$phone_prfixes = $countyModel->getPhonePrefixs();
				$phone_prefix_id = null;
				$phone_number = preg_replace('/^(\+|00)/', '', trim($agency_data['phone']));
				foreach ( $phone_prfixes as $prefix ) {
					if ( preg_match('/^(' . $prefix->phone_prefix . ')/', $phone_number) ) {
						$phone_prefix = $prefix->id . '-' . $prefix->phone_prefix;
						$agency_data['phone'] = preg_replace('/^(' . $prefix->phone_prefix . ')/', '', $phone_number);
						break;
					}
				}
			}
			elseif ( $agency_data['phone_country_id'] ) {
				$phone_prefix = $agency_data['phone_country_id'] . '-';
			}
		}
		// }}}

		$params = array(
			'application_id' => Cubix_Application::getId(),
			'agency_name' => $agency_data['name'],
			'login' => $agency_data['username'],
			'password' => $this->makeNewPassword(),
			'email' => $agency_data['email'],
			'u_email' => $agency_data['u_email'],
			'web' => $agency_data['web'],
			'phone' => $agency_data['phone'],
			'phone_prefix' => isset($phone_prefix) ? $phone_prefix : null,
			'phone_instructions' => $agency_data['phone_instructions'],
			'status' => $agency_data['status'],
			'address' => $agency_data['address'],
			'latitude' => $agency_data['latitude'],
			'longitude' => $agency_data['longitude'],

			'sales_person_id' => $this->getMyUserId(),

			'available_24_7' => (bool) $agency_data['available_24_7'],
		);

		foreach ( Cubix_I18n::getLangs() as $lang ) {
			$params['about_' . $lang] = ! empty($agency_data['about_' . $lang]) ? $agency_data['about_' . $lang] : '';
		}

		foreach ( $data['working_hours'] as $work ) {
			if ( ! isset($params['work_times_from']) ) {
				$params['work_times_from'] = array();
			}

			if ( ! isset($params['work_times_to']) ) {
				$params['work_times_to'] = array();
			}

			$params['work_times_from'][$work['day_index']] = $work['time_from'];
			$params['work_times_to'][$work['day_index']] = $work['time_to'];
		}
		
		$request = new Zend_Controller_Request_Http();

		foreach ( $params as $param => $value ) {
			$request->setParam($param, $value);
		}
		
		if ( $for_sync && $new_agency_id ) {
			$request->setParam('id', $new_agency_id);
			$request->setParam('user_id', $new_user_id);
			$result = $this->doDispatch($request, 'default', 'agencies', 'edit');

			if ( 'error' == $result->status ) {
				return array('result' => self::RESULT_AGENCY_VALIDATION_ERROR, 'message' => 'There were validation errors', 'errors' => $result->msgs);
			}
			
			return array('result' => self::RESULT_AGENCY_TAKEN_SUCCESSFULLY, 'message' => 'The agency has been successfully taken', 'id' => $result->$new_agency_id);
		} else {
			$result = $this->doDispatch($request, 'default', 'agencies', 'create');

			if ( 'error' == $result->status ) {
				return array('result' => self::RESULT_AGENCY_VALIDATION_ERROR, 'message' => 'There were validation errors', 'errors' => $result->msgs);
			}
			else {
				parent::db()->insert('copied_agencies', array(
					'agency_id' => $result->new_id,
					'orig_app_id' => $orig_app_id,
					'orig_agency_id' => $orig_agency_id
				));
				return array('result' => self::RESULT_AGENCY_TAKEN_SUCCESSFULLY, 'message' => 'The agency has been successfully taken', 'id' => $result->new_id);
			}
		}
//		@unlink($tmp_file);
		
		
		

//		return $result;
	}

	public function doDispatch(Zend_Controller_Request_Http $request, $module, $controller, $action)
	{
		$request->setParam('request_mode', 'copy-listener');

		$request->setModuleName($module);
		$request->setControllerName($controller);
		$request->setActionName($action);

		$front = self::getFrontController();
		$dispatcher = $front->getDispatcher();

		$response = new Zend_Controller_Response_Http();
		$dispatcher->dispatch($request, $response);
		$plain = $response->getBody();
		$result = json_decode($plain);

		return $result;
	}

	public function takeCopyOfAgencyLogo($agency_id, $application_id, $logo_url)
	{
		$new_agency = $this->getNewAgencyId($agency_id, $application_id);
		$new_agency_id = $new_agency->agency_id;
//		$new_user_id = $new_agency->user_id;
		
		if ( ! $new_agency_id ) {
			return array('result' => self::RESULT_AGENCY_DOESNT_EXIST, 'message' => 'There is no such agency');
		}
        $http = new Zend_Http_Client($logo_url, array(
            'maxredirects' => 0,
            'timeout'      => 100));
        $http->setParameterGet('origano', 'true');
        try {
            $http->request('GET');
            $binary = $http->getLastResponse()->getBody();
            if ($http->getLastResponse()->getStatus() != 200){
                return array('result' => self::RESULT_AGENCY_LOGO_FAIL, 'message' => 'Could not get agency original logo', 'error' => "$logo_url?origano=true file not downloaded");
            }
        }
        catch ( Exception $e ) {
            return array('result' => self::RESULT_AGENCY_LOGO_FAIL, 'message' => 'Could not get agency original logo', 'error' => $e->__toString());
        }
		$ext = explode('.', $logo_url);
        $ext = end($ext);
		$ext = strtolower($ext);
        $cnf= Zend_Registry::get('videos_config');
        $path = !empty($cnf['temp_dir_path']) ? $cnf['temp_dir_path'] : sys_get_temp_dir();
		$tmp_file = tempnam($path, 'ECAgencyLogo_').'.'.$ext;

		$size = file_put_contents($tmp_file, $binary);
        $mimetypes = array(
            'image/png',
            'image/jpeg',
        );
        $type = mime_content_type($tmp_file);
        if (!in_array($type, $mimetypes)) {
            return array('result' => self::RESULT_AGENCY_LOGO_FAIL, 'message' => 'Failed to copy agency logo', 'error' => "invalid mime type: $type");
        }
		$request = new Zend_Controller_Request_Http();
		$request->setParam('agency_id', $new_agency_id);
		$request->setParam('file_name', $tmp_file);

        $fileName = explode('/', $tmp_file);
        $fileName = end($fileName);

        $_SERVER['CONTENT_LENGTH'] = filesize($tmp_file);
        $_SERVER['HTTP_X_FILE_ID'] = dechex(time());
        $_SERVER['HTTP_X_FILE_NAME'] = $fileName;
        $_SERVER['HTTP_X_FILE_RESUME'] = 1;
        $_SERVER['HTTP_X_FILE_SIZE'] = filesize($tmp_file);
		$result = $this->doDispatch($request, 'default', 'agencies', 'upload-logo');

        // remove tmp file not needed anymore
		unlink($tmp_file);
		if ( 'success' == $result->status ) {
			return array(
                    'result' => self::RESULT_AGENCY_LOGO_SUCCESS,
                    'message' => 'Agency logo has been successfully copied',
                    'image_url' => $result->logo_url,
                    'ext'=>$ext,
                    'size'=>$size,
                    'res'=>$result
            );
		}
		else {
			return array('result' => self::RESULT_AGENCY_LOGO_FAIL, 'message' => 'Failed to copy agency logo', 'error' => $result->msg);
		}
	}

	public function getNewEscortId($escort_id, $application_id)
	{
		return parent::db()->fetchOne('
			SELECT escort_id
			FROM copied_escorts
			WHERE orig_escort_id = ? AND orig_app_id = ?
		', array($escort_id, $application_id));
	}

	public function getNewAgencyId($agency_id, $application_id)
	{
		return parent::db()->fetchRow('
			SELECT ca.agency_id, a.user_id
			FROM copied_agencies ca
			INNER JOIN agencies a ON a.id = ca.agency_id
			WHERE orig_agency_id = ? AND orig_app_id = ?
		', array($agency_id, $application_id));
	}

	public function getAgencyIdByName($name)
	{
		return parent::db()->fetchOne('
			SELECT id
			FROM agencies
			WHERE name = ?
		', $name);
	}

	public function makeEscortRequestParams($data)
	{

	}

	public function makeAgencyRequestParams($data)
	{
		
	}

	public function getMyUserId()
	{
		return Zend_Auth::getInstance()->getIdentity()->id;
	}

	public function makeNewPassword($min_length = 6, $max_length = 10)
	{
		$password = '';
		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-*&?%=+';
		$length = rand($min_length, $max_length);
		for ( $i = 0; $i < $length; $i++ ) {
			$char = substr($chars, rand(0, strlen($chars) - 1), 1);
			$password .= $char;
		}
		return $password;
	}

	public function dateToAge($timestamp)
	{
		if ( $timestamp === false || is_null($timestamp) ) return null;
		list($year, $month, $day) = explode('-', date('Y-m-d', $timestamp));
		$year_diff  = date("Y") - $year;
		$month_diff = date("m") - $month;
		$day_diff   = date("d") - $day;
		if ($day_diff < 0 || $month_diff < 0)
			$year_diff--;
		return $year_diff;
	}
	
	
	public function getCitiesByName($names, $country_id = null)
	{
		$where = '';
		if($country_id){
			$where = ' AND country_id = '. $country_id;
		}
		
		$cities = parent::db()->fetchAll('
			SELECT c.id, title_en
			FROM cities c 
			WHERE c.title_en IN ("' . implode('","', $names) . '") ' . $where
		);
		$result = array();
		
		foreach($cities as $city) {
			$result[$city->title_en] = $city->id;
		}
		
		return $result;
	}

    public function getCitiesBySlugs($slugs)
    {
        $cities = parent::db()->fetchAll('
			SELECT c.id, c.slug
			FROM cities c 
			WHERE c.slug IN ("' . implode('","', $slugs) . '")
		');
        $result = array();

        foreach($cities as $city) {
            $result[$city->slug] = $city->id;
        }

        return $result;
    }
	
	public function activateTransitPackage($escort_data)
	{	
		$packages_model = new Model_Billing_Packages();
		$package = $packages_model->getByEscortId($escort_data['id']);
		
		$add_data = parent::db()->fetchRow('
			SELECT e.id, e.user_id, e.gender, e.agency_id
			FROM escorts e
			WHERE e.id = ?
		', array($escort_data['id']));
		
		
		
		if ( $package->status == Model_Packages::STATUS_ACTIVE && ! is_null($package->order_id)  ) return true;
		
		$system_order_id = $this->_getNewOrderId();
		$application_id = Cubix_Application::getId();
		$transit_package = 105;
		
		$order_data = array(
			'user_id'	=> $add_data->user_id,
			'backend_user_id'	=> $this->getMyUserId(), 
			'status' => Model_Billing_Orders::STATUS_PENDING,
			'activation_condition' => Model_Billing_Orders::CONDITION_AFTER_RECEIVING_PAYMENT_DETAILS,
			'order_date' => new Zend_Db_Expr('NOW()'),
			'payment_date'	=> new Zend_Db_Expr('NOW()'),
			'system_order_id' => $system_order_id,
			'price' => 0, // Temporary price. Will be updated after foreach
			'application_id' => $application_id
		);
		
		parent::db()->insert('orders', $order_data);
		$order_id = parent::db()->lastInsertId();
		
		$order_package_data = array(
			'order_id'			=> $order_id,
			'escort_id'			=> $escort_data['id'],
			'package_id'		=> $transit_package,
			'application_id'	=> $application_id,
			'base_period'		=> $escort_data['package_duration'],
			'period'			=> $escort_data['package_duration'],
			'activation_type'	=> Model_Packages::ACTIVATE_ASAP,
			'base_price'		=> 0,
			'price'				=> 0
			
		);

		parent::db()->insert('order_packages', $order_package_data);
		$order_package_id = parent::db()->lastInsertId();
		
		$escort_type = null;
		if ($application_id == APP_ED){
		    $escort_type = Model_EscortV2Item::getEscortTypeByID($escort_data['id']);
        }
		//Maybe need to refactor this part later
		$packages_model = new Model_Packages();
		$products = $packages_model->getProducts($transit_package, $application_id, $add_data->agency_id, $add_data->gender, null, null, $escort_type);

		if ( count($products) > 0 )
		{
			foreach( $products as $product )
			{
				$package_product_data = array(
					'order_package_id' => $order_package_id,
					'product_id' => $product->id,
					'is_optional' => 0,
					'price' => $product->price
				);
				parent::db()->insert('order_package_products', $package_product_data);
			}
		}
		
		
		if ( $escort_data['premium_cities'] && count($escort_data['premium_cities']) ) {
			$city_names = array();
			foreach($escort_data['premium_cities'] as $city) {
				$city_names[] = $city['title'];
			}
			
			$new_cities = $this->getCitiesByName($city_names);//Cities with the new ids
			
			foreach($escort_data['premium_cities'] as $city) {
				if ( $new_cities[$city['title']] ) {
					parent::db()->insert('premium_escorts', array('order_package_id' => $order_package_id, 'city_id' => $new_cities[$city['title']]));
				}
			}
		}
		
		return $order_package_id;
	}
	
	
	public function removeTransitPackage($escort_id, $application_id)
	{
		$packages_model = new Model_Billing_Packages();
		
		$new_escort_id = $this->getNewEscortId($escort_id, $application_id);
		$package = $packages_model->getTransitPackage($new_escort_id, 105);// transit package in ED
		
		if ( $package ) {
			$package->cancel('Canceled by sync manager.');
		}
		return array('status' => 'success');
	}
	
	public function ping()
	{
		sleep(18);
		return 'OK';
	}
	
	
	private function _generateOrderId()
	{
		$order_id = md5(microtime() * microtime() + microtime());
		$order_id = substr(strtoupper($order_id), 0, 8);

		return $order_id;
	}

	private function _getNewOrderId()
	{
		$m_orders = new Model_Billing_Orders();
		
		$order_id = $this->_generateOrderId();
		while( $m_orders->isOrderIdExist($order_id) ) {
			$order_id = $this->_generateOrderId();
		}

		return $order_id;
	}

    public function takeCopyOfCities($data, $for_sync = false)
    {
        $params = array(
            'region_id' => $data['region_id'],
            'slug' => $data['slug'],
            'title_pt' => $data['title_pt'],
            'title_es' => $data['title_es'],
            'title_fr' => $data['title_fr'],
            'title_ru' => $data['title_ru'],
            'title_gr' => $data['title_gr'],
            'title_en' => $data['title_en'],
            'title_it' => $data['title_it'],
            'title_de' => $data['title_de'],
            'internal_id' => $data['internal_id'],
            'is_important' => $data['is_important'],
            'is_french' => $data['is_french'],
            'time_zone_id' => $data['time_zone_id'],
            'url_slug' => $data['url_slug'],
            'order_id' => $data['order_id'],
            'title_geoip' => $data['title_geoip'],
            'iso' => $data['iso'],
        );

        $request = new Zend_Controller_Request_Http();

        foreach ( $params as $param => $value ) {
            $request->setParam($param, $value);
        }

        $result = $this->doDispatch($request, 'default', 'cron', 'cities');
        if ( 'error' == $result->status ) {
            return array('result' => 'error', 'message' => $result->message);
        }
        else {
            parent::db()->insert('copied_cities', array(
                'city_id' => $result->id,
                'title_en' => $result->data->title_en,
                'country_id' => $result->data->country_id
            ));
            return array('result' => self::RESULT_AGENCY_TAKEN_SUCCESSFULLY, 'message' => 'The city has been successfully transfered', 'id' => $result->id);
        }
    }

    public function takeCopyOfEscortBirthDate($escort_id,$birthdate)
    {
        $params = array(
            'escort_id' => $escort_id,
            'birth_date' => $birthdate
        );

        $request = new Zend_Controller_Request_Http();

        foreach ( $params as $param => $value ) {
            $request->setParam($param, $value);
        }

        $result = $this->doDispatch($request, 'default', 'cron', 'copy-birth-date');
        if ( 'error' == $result->status ) {
            return array('result' => 'error', 'message' => $result->message);
        }
        else {
            return array('result' => 'success', 'message' => 'The birth date has been successfully transfered', 'escort_id' => $result->escort_id);
        }
    }

    public function takeCopyOfEscortKeywordsMessengers($escort_id, $keywords, $messangers, $lastCheckDate)
    {
        $params = array(
            'escort_id' => $escort_id,
            'keywords' => $keywords,
            'messengers' => $messangers,
            'last_check_date' => $lastCheckDate
        );

        $request = new Zend_Controller_Request_Http();

        foreach ( $params as $param => $value ) {
            $request->setParam($param, $value);
        }

        $result = $this->doDispatch($request, 'default', 'cron', 'copy-keywords');
        if ( 'error' == $result->status ) {
            return array('result' => 'error', 'message' => $result->message);
        }
        else {
            return array('result' => 'success', 'message' => 'The keywords has been successfully transfered', 'escort_id' => $result->escort_id);
        }
    }

    public function takeCopyOfEscortStatuses($escort_id, $status)
    {
        $params = array(
            'escort_id' => $escort_id,
            'status' => $status,
        );

        $request = new Zend_Controller_Request_Http();

        foreach ( $params as $param => $value ) {
            $request->setParam($param, $value);
        }

        $result = $this->doDispatch($request, 'default', 'cron', 'copy-statuses');
        if ( 'error' == $result->status ) {
            return array('result' => 'error', 'message' => $result->message);
        }
        else {
            return array('result' => 'success', 'message' => $result->message, 'escort_id' => $result->escort_id);
        }
    }
}

