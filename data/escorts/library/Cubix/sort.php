<?php

return array(
	array(
		'title' => Cubix_I18n::translate('random'),
		'value' => 'random'
	),
	array(
		'title' => Cubix_I18n::translate('alphabetically'),
		'value' => 'alpha'
	),
	array(
		'title' => Cubix_I18n::translate('most_viewed'),
		'value' => 'most-viewed'
	),
	array(
		'title' => Cubix_I18n::translate('newest_first'),
		'value' => 'newest'
	),
	array(
		'title' => Cubix_I18n::translate('last_modified'),
		'value' => 'last-modified'
	),
	array(
		'title' => Cubix_I18n::translate('price') . ' &uarr;',
		'value' => 'price-asc'
	),
	array(
		'title' => Cubix_I18n::translate('price') . ' &darr;',
		'value' => 'price-desc'
	)
);
