<?php

class Cubix_Forum2Api
{	
	private $API_URL;
	private $API_KEY;
	private $DOMAIN;
	private $GIRLFORUM;

	public function __construct($girlforum = false) 
	{
		if (Cubix_Application::getId() == APP_6A)
		{
			if ($girlforum)
			{
				$this->API_URL = 'http://girlforum.6annonce.com/webapi/?cmd=%cmd%&%params%';
				$this->API_KEY = 'girlforum_6a_sceon_2012';
				$this->DOMAIN = '.6annonce.com';
			}
			else
			{
				$this->API_URL = 'http://forum.6annonce.com/webapi/?cmd=%cmd%&%params%';
				$this->API_KEY = 'forum_sceon_2012';
				$this->DOMAIN = '.6annonce.com';
			}
		}
		elseif (Cubix_Application::getId() == APP_EF)
		{
			if ($girlforum)
			{
				$this->API_URL = 'https://girlforum.escortforumit.xxx/webapi/?cmd=%cmd%&%params%';
				$this->API_KEY = 'girlforum_sceon_2012';
				$this->DOMAIN = '.escortforumit.xxx';
			}
			else
			{
				$this->API_URL = 'https://forum.escortforumit.xxx/webapi/?cmd=%cmd%&%params%';
				$this->API_KEY = 'forum2_cubix_2011';
				$this->DOMAIN = '.escortforumit.xxx';
			}
		}
		elseif (Cubix_Application::getId() == APP_BL)
		{
			if ($girlforum)
			{

			}
			else
			{
				$this->API_URL = 'https://forum.beneluxxx.com/webapi/?cmd=%cmd%&%params%';
				$this->API_KEY = 'forum_bl_sceon_2012';
				$this->DOMAIN = '.beneluxxx.com';
			}
		}
		elseif (Cubix_Application::getId() == APP_ED)
		{
			if ($girlforum)
			{

			}
			else
			{
				$this->API_URL = 'https://forum.escortdirectory.com/webapi/?cmd=%cmd%&%params%';
				$this->API_KEY = 'forum_ed_cubix_2016';
				$this->DOMAIN = '.escortdirectory.com';
			}
		}elseif (Cubix_Application::getId() == APP_EG_CO_UK)
        {
            if ($girlforum)
            {

            }
            else
            {
                $this->API_URL = 'https://forum.escortguide.co.uk/webapi/?cmd=%cmd%&%params%';
                $this->API_KEY = 'forum_uk_cubix_2021';
                $this->DOMAIN = '.escortguide.co.uk';
            }
        }

		$this->GIRLFORUM = $girlforum;

		if ($girlforum) {
			if (getenv('GIRLFORUM_API_URL')) {
				$this->API_URL = getenv('GIRLFORUM_API_URL');
			}
		}
		else {
			if (getenv('FORUM_API_URL')) {
				$this->API_URL = getenv('FORUM_API_URL');
			}
		}
	}

	protected function _request($cmd, $args = array())
	{
		set_time_limit(60);
		$user_name = $args['user_name'];
		foreach ($args as $arg => $value) {
			$args[] = "$arg=" . urlencode($value) . "";
			unset($args[$arg]);
		}

		$url = str_replace(array('%cmd%', '%params%'), array($cmd, implode('&', array_merge($args, array('key=' . $this->API_KEY, 'sid=' . session_id())))), $this->API_URL);
		/*if ( $cmd == "AddReviewAsTopicAndPost" ) {
			var_dump($url);
			die;
		}*/
		list($nil, $host, $path, $query) = parse_url($url);

		$content = file_get_contents($url);

		return $content;
	}

	public function RegisterUser($user_name, $email)
	{
		$resp = $this->_request('RegisterUser', array('user_name' => $user_name, 'email' => $email));
		return ($resp != 'error');
	}

	public function UpdateEmail($user_name, $email)
	{
		$resp = $this->_request('UpdateEmail', array('user_name' => $user_name, 'email' => $email));

		return ($resp != 'error');
	}

	public function DeleteUser($user_name)
	{
		$resp = $this->_request('DeleteUser', array('user_name' => $user_name));
		return ($resp != 'error');
	}

	public function BannUser($user_name, $status)
	{
		$resp = $this->_request('BannUser', array('user_name' => $user_name, 'status' => $status ));
		return ($resp != 'error');
	}

	public function Login($user_name = null, $email = null, $has_package = null)
	{
		if (Cubix_Application::getId() == APP_EG_CO_UK){
			$ip_address = '10.20.10.5';
		}
		else{
			$ip_address = $_SERVER['REMOTE_ADDR'];
		}
		
		$arr = array(
			'user_name' => $user_name,
			'email' => $email,
			'ip_address' => $ip_address,
			'user_agent' => $_SERVER['HTTP_USER_AGENT']
		);

		if (!is_null($has_package))
			$arr['has_package'] = $has_package;

		if ($this->GIRLFORUM)
			$arr['girl_forum'] = 1;

		$resp = $this->_request('Login', $arr);

		/*if ($user_name == 'cubix_test_agency2')
		{
			var_dump($resp);die;
			//var_dump($arr);die;
		}*/


		if ( $resp == 'error' ) return false;

		$resp = json_decode($resp);

		if ($this->GIRLFORUM)
		{
			setcookie('girl_session_id', $resp->session_id, 0, '/', $this->DOMAIN);
			$_SESSION['girlforum_session_id'] = $resp->session_id;
			$_SESSION['girlforum_member_key'] = $resp->member_key;
		}
		else
		{
			setcookie('session_id', $resp->session_id, 0, '/', $this->DOMAIN);
			$_SESSION['forum_session_id'] = $resp->session_id;
			$_SESSION['forum_member_key'] = $resp->member_key;
		}

		return ($resp != 'error');
	}

	public function LogOut()
	{
		if ($this->GIRLFORUM)
			$resp = $this->_request('LogOut', array('session_id' => $_SESSION['girlforum_session_id'], 'member_key' => $_SESSION['girlforum_member_key'], 'ip_address' => $_SERVER['REMOTE_ADDR'], 'user_agent' => $_SERVER['HTTP_USER_AGENT'], 'girl_forum' => 1));
		else
			$resp = $this->_request('LogOut', array('session_id' => $_SESSION['forum_session_id'], 'member_key' => $_SESSION['forum_member_key'], 'ip_address' => $_SERVER['REMOTE_ADDR'], 'user_agent' => $_SERVER['HTTP_USER_AGENT']));

        if (Cubix_Application::getId() == APP_EG_CO_UK)
        {
            $resp = json_decode($resp);
            setcookie('session_id', $resp->session_id, 0, '/', $this->DOMAIN);
            setcookie('session_id', $resp->session_id, 0, '/', 'forum'.$this->DOMAIN);
        }

        return ($resp != 'error');

    }

	public function getLastTopicFirstPost()
	{
		$resp = $this->_request('GetLastTopicFirstPost', array());

		return $resp;
	}

	public function getApprovedPostsCountOfMember($name)
	{
		$resp = $this->_request('GetApprovedPostsCountOfMember', array('name' => $name));

		return $resp;
	}

	public function addReviewAsTopicAndPost($title, $title_seo, $ip, $post, $review_id)
	{
		if (Cubix_Application::getId() == APP_BL)
		{
			$resp = $this->_request('AddReviewAsTopicAndPost', array('review_id' => $review_id, 'title' => $title, 'title_seo' => $title_seo, 'ip' => $ip, 'post' => $post));
		}

		return ($resp != 'error');
	}
}
