<?php

class Cubix_EscortStatus
{
	const ESCORT_STATUS_NO_PROFILE                  = 1;		// 00000000000001
	const ESCORT_STATUS_NO_ENOUGH_PHOTOS            = 2;		// 00000000000010
	const ESCORT_STATUS_NOT_APPROVED                = 4;		// 00000000000100
	const ESCORT_STATUS_OWNER_DISABLED              = 8;		// 00000000001000
	const ESCORT_STATUS_ADMIN_DISABLED              = 16;		// 00000000010000
	const ESCORT_STATUS_ACTIVE                      = 32;		// 00000000100000
	const ESCORT_STATUS_IS_NEW                      = 64;		// 00000001000000
	const ESCORT_STATUS_PROFILE_CHANGED             = 128;		// 00000010000000
	const ESCORT_STATUS_DELETED                     = 256;		// 00000100000000
	const ESCORT_STATUS_FAKE_PICTURE_DISABLED       = 512;		// 00001000000000
    const ESCORT_STATUS_TEMPRARY_DELETED            = 1024;		// 00001000000000
	const ESCORT_STATUS_SUSPICIOUS_DELETED          = 2048;		// 00010000000000
	const ESCORT_STATUS_NEED_AGE_VERIFICATION       = 4096;		// 00100000000000
	const ESCORT_STATUS_OFFLINE                     = 8192;		// 10000000000000
	const ESCORT_STATUS_INACTIVE                    = 16384;    // 100000000000000
    const ESCORT_STATUS_PARTIALLY_COMPLETE          = 32768;	// 1000000000000000
    const ESCORT_STATUS_IP_BLOCKED          		= 65536;	// 10000000000000000
    const ESCORT_STATUS_AWAITING_PAYMENT			= 131072;	// 100000000000000000

    const REVISION_STATUS_NOT_APPROVED	= 1;
	const REVISION_STATUS_APPROVED		= 2;
	const REVISION_STATUS_DECLINED		= 3;

	const ESCORT_STATUS_NO_PROFILE_TXT                  = 'NO PROFILE';
	const ESCORT_STATUS_NO_ENOUGH_PHOTOS_TXT            = 'NO ENOUGH PHOTOS';
	const ESCORT_STATUS_NOT_APPROVED_TXT                = 'NOT APPROVED';
	const ESCORT_STATUS_OWNER_DISABLED_TXT              = 'OWNER DISABLED';
	const ESCORT_STATUS_ADMIN_DISABLED_TXT              = 'ADMIN DISABLED';
	const ESCORT_STATUS_ACTIVE_TXT                      = 'ACTIVE';
	const ESCORT_STATUS_IS_NEW_TXT                      = 'NEW';
	const ESCORT_STATUS_PROFILE_CHANGED_TXT             = 'PROFILE CHANGED';
	const ESCORT_STATUS_DELETED_TXT                     = 'DELETED';
	const ESCORT_STATUS_FAKE_PICTURE_DISABLED_TXT       = 'FAKE PICTURE DISABLED';
	const ESCORT_STATUS_TEMPRARY_DELETED_TXT            = 'TEMPORARY DELETED';
	const ESCORT_STATUS_SUSPICIOUS_DELETED_TXT			= 'SUSPICIOUS DELETED';
	const ESCORT_STATUS_NEED_AGE_VERIFICATION_TXT       = 'NEED AGE VERIFICATION';
	const ESCORT_STATUS_OFFLINE_TXT                     = 'OFFLINE';
	const ESCORT_STATUS_INACTIVE_TXT                    = 'INACTIVE';
    const ESCORT_STATUS_PARTIALLY_COMPLETE_TXT          = 'WIZARD NOT COMPLETE';	// 1000000000000000
    const ESCORT_STATUS_IP_BLOCKED_TXT         			= 'IP BLOCKED';	// 1000000000000000
    const ESCORT_STATUS_AWAITING_PAYMENT_TXT			= 'AWAITING PAYMENT';

    private $_status;
	private $_user_status;
	private $_orig_status;
	private $_status_text = '';
	private $_escort_id;
	private $_sales_user_id;

	private $_need_check;
	
	private $_db;

	private $_changed = false;

	public function __construct($escort_id, $status = null, $reset = false, $need_check = true, $sales_user_id =null)
	{
		$this->_db = Zend_Registry::get('db');
		$this->_escort_id = $escort_id;
		if ( is_null($status) ) {
			$status = $this->_db->fetchOne('SELECT status FROM escorts where id = ?', array($escort_id));
		}
		
		$this->_status = $this->_orig_status = $status;
		
		if ( $reset ) {
			$this->_status = 0;
		}
		if (is_numeric($sales_user_id) && $sales_user_id > 0){
		    $this->_sales_user_id = intval($sales_user_id);
        }
		$this->_need_check = $need_check;
	}

	public function setStatusBit($status)
	{
		if ( ! is_array($status) ) $status = array($status);
        
		foreach ( $status as $bit ) {
			if ( ! $this->hasStatusBit($bit) ) {
				$this->_status = $this->_status | $bit;
				
				$this->_changed = true;
			}
		}
	}

	public function removeStatusBit($status)
	{
		if ( ! is_array($status) ) $status = array($status);
		
		foreach ( $status as $bit ) {
			if ( $this->hasStatusBit($bit) ) {
				$this->_status = $this->_status & ~$bit;

				$this->_changed = true;
			}
		}
	}

	public function hasStatusBit($status)
	{
		if ( ! is_array($status) ) $status = array($status);

		foreach ($status as $bit) {
			if((boolean)($this->_status & $bit)){
				return true;
			}
        }
		
		return false;
	}

	/**
	 *
	 * @author GooGo
	 * @param int $status
	 */
	public function setStatus($status)
	{
		switch ( $status ) {
			case self::ESCORT_STATUS_FAKE_PICTURE_DISABLED:
				$this->_user_status = USER_STATUS_DISABLED;
				$this->removeStatusBit(array(
					self::ESCORT_STATUS_ACTIVE,
					self::ESCORT_STATUS_OWNER_DISABLED,
                    self::ESCORT_STATUS_INACTIVE,
				));
				$this->setStatusBit(array(
					self::ESCORT_STATUS_ADMIN_DISABLED,
					self::ESCORT_STATUS_FAKE_PICTURE_DISABLED,
				));
				break;
			case self::ESCORT_STATUS_ACTIVE:
				$this->_user_status = USER_STATUS_ACTIVE;
				$this->setStatusBit(self::ESCORT_STATUS_ACTIVE);
				$this->removeStatusBit(array(
					self::ESCORT_STATUS_ADMIN_DISABLED,
					self::ESCORT_STATUS_OWNER_DISABLED,
					self::ESCORT_STATUS_FAKE_PICTURE_DISABLED,
					self::ESCORT_STATUS_SUSPICIOUS_DELETED,
                    self::ESCORT_STATUS_OFFLINE,
                    self::ESCORT_STATUS_INACTIVE,
                    self::ESCORT_STATUS_AWAITING_PAYMENT
				));
				break;
			case self::ESCORT_STATUS_ADMIN_DISABLED:
				$this->_user_status = USER_STATUS_DISABLED;
				$this->removeStatusBit(array(
					self::ESCORT_STATUS_ACTIVE,
					self::ESCORT_STATUS_OWNER_DISABLED,
					self::ESCORT_STATUS_FAKE_PICTURE_DISABLED,
                    self::ESCORT_STATUS_INACTIVE
				));
				$this->setStatusBit(self::ESCORT_STATUS_ADMIN_DISABLED);
				break;
			case self::ESCORT_STATUS_OWNER_DISABLED:
				$this->_user_status = USER_STATUS_ACTIVE;
				$this->removeStatusBit(array(
					self::ESCORT_STATUS_ACTIVE,
					self::ESCORT_STATUS_ADMIN_DISABLED,
					self::ESCORT_STATUS_FAKE_PICTURE_DISABLED,
                    self::ESCORT_STATUS_INACTIVE
				));
				$this->setStatusBit(self::ESCORT_STATUS_OWNER_DISABLED);
				break;
			case self::ESCORT_STATUS_DELETED:
				$this->_user_status = USER_STATUS_DELETED;
				$this->removeStatusBit(array(
					self::ESCORT_STATUS_ACTIVE,
					self::ESCORT_STATUS_AWAITING_PAYMENT
				));
				$this->setStatusBit(self::ESCORT_STATUS_DELETED);
				break;
			case self::ESCORT_STATUS_SUSPICIOUS_DELETED:
				$this->removeStatusBit(self::ESCORT_STATUS_ACTIVE);
				$this->setStatusBit(self::ESCORT_STATUS_SUSPICIOUS_DELETED);
				break;
			case self::ESCORT_STATUS_OFFLINE:
				$this->removeStatusBit(array(
					self::ESCORT_STATUS_ACTIVE,
					self::ESCORT_STATUS_INACTIVE
				));
				$this->setStatusBit(self::ESCORT_STATUS_OFFLINE);
				break;
			case self::ESCORT_STATUS_INACTIVE:
				$this->removeStatusBit(array(
                    self::ESCORT_STATUS_OWNER_DISABLED,
                    self::ESCORT_STATUS_ADMIN_DISABLED,
                    self::ESCORT_STATUS_FAKE_PICTURE_DISABLED,
                    self::ESCORT_STATUS_OFFLINE
                ));
				$this->setStatusBit(self::ESCORT_STATUS_INACTIVE);
				break;
			case self::ESCORT_STATUS_IP_BLOCKED:
				$this->removeStatusBit(array(
                    self::ESCORT_STATUS_OWNER_DISABLED,
					self::ESCORT_STATUS_ACTIVE,
                ));
				$this->setStatusBit(self::ESCORT_STATUS_IP_BLOCKED);
				$this->setStatusBit(self::ESCORT_STATUS_ADMIN_DISABLED);
				break;
			case self::ESCORT_STATUS_AWAITING_PAYMENT:
				$this->setStatusBit(self::ESCORT_STATUS_AWAITING_PAYMENT);
                $this->removeStatusBit(array(
					self::ESCORT_STATUS_ACTIVE,
                    self::ESCORT_STATUS_INACTIVE,
				));
				break;
		}
	}

	public function get()
	{
		return $this->_status;
	}

	private function checkPhotos()
	{
		$photos_count = $this->_getEscortPhotosCount($this->_escort_id);
		$photos_min_count = Cubix_Application::getById()->min_photos;
		
		//TEMPORARY SOLUTION TASK EDIR-505
		if ( Cubix_Application::getId() == APP_ED && $this->_escort_id > 27240) {
			if ( $photos_count < 3 ) {
				$this->setStatusBit(self::ESCORT_STATUS_NO_ENOUGH_PHOTOS);
				$this->removeStatusBit(self::ESCORT_STATUS_ACTIVE);
			}
			else {
				$this->removeStatusBit(self::ESCORT_STATUS_NO_ENOUGH_PHOTOS);

				if ( ! $this->hasStatusBit(self::ESCORT_STATUS_ADMIN_DISABLED) && ! $this->hasStatusBit(self::ESCORT_STATUS_OWNER_DISABLED) ) {
					if (
						(! $this->hasStatusBit(self::ESCORT_STATUS_IS_NEW) &&
						! $this->hasStatusBit(self::ESCORT_STATUS_NO_PROFILE) &&
						! $this->hasStatusBit(self::ESCORT_STATUS_PROFILE_CHANGED) &&
						! $this->hasStatusBit(self::ESCORT_STATUS_AWAITING_PAYMENT))
					) {
						$this->setStatusBit(self::ESCORT_STATUS_ACTIVE);
					}
				}
			}
		}
		else{
			if ( $photos_count < $photos_min_count ) {
				$this->setStatusBit(self::ESCORT_STATUS_NO_ENOUGH_PHOTOS);
				$this->removeStatusBit(self::ESCORT_STATUS_ACTIVE);
			}
			else {
				$this->removeStatusBit(self::ESCORT_STATUS_NO_ENOUGH_PHOTOS);

				if ( ! $this->hasStatusBit(self::ESCORT_STATUS_ADMIN_DISABLED) && ! $this->hasStatusBit(self::ESCORT_STATUS_OWNER_DISABLED) ) {
					if (
						(! $this->hasStatusBit(self::ESCORT_STATUS_IS_NEW) &&
						! $this->hasStatusBit(self::ESCORT_STATUS_NO_PROFILE) &&
						! $this->hasStatusBit(self::ESCORT_STATUS_PROFILE_CHANGED) &&
						! $this->hasStatusBit(self::ESCORT_STATUS_AWAITING_PAYMENT))
					) {
						$this->setStatusBit(self::ESCORT_STATUS_ACTIVE);
					}
				}
			}
		}
	}



	private function _getEscortPhotosCount($escort_id)
	{
		$sql = "
			SELECT COUNT(ep.id) as count
			FROM escort_photos ep
			WHERE ep.escort_id = ? AND ep.is_approved = 1 AND ep.type <> ? AND ep.type <> ? AND ep.type <> ? AND ep.retouch_status <> 1
		";

		$count = $this->_db->fetchOne($sql, array($escort_id, ESCORT_PHOTO_TYPE_PRIVATE, ESCORT_PHOTO_TYPE_DISABLED, ESCORT_PHOTO_TYPE_ARCHIVED));
		
		return $count;
	}

    private function checkDeletion(){
        if($this->hasStatusBit(self::ESCORT_STATUS_OFFLINE) || $this->hasStatusBit(self::ESCORT_STATUS_TEMPRARY_DELETED) || $this->hasStatusBit(self::ESCORT_STATUS_DELETED) || $this->hasStatusBit(self::ESCORT_STATUS_SUSPICIOUS_DELETED) ){
            $this->removeStatusBit(self::ESCORT_STATUS_ACTIVE);
        }
    }

	private function checkRevisions()
	{
		$revisions = $this->_getEscortRevisionsStatus($this->_escort_id);
		$rev_count = count($revisions);

		$has_active_rev = false;
		
		if ( $rev_count > 0 ) {
			foreach ( $revisions as $revision ) {
				if ( $revision->status == self::REVISION_STATUS_APPROVED ) {
					$has_active_rev = true;
				}
			}
		}

		if ( $rev_count == 0 ) {
			$this->setStatusBit(self::ESCORT_STATUS_IS_NEW);
			$this->setStatusBit(self::ESCORT_STATUS_NO_PROFILE);
			
			$this->removeStatusBit(self::ESCORT_STATUS_PROFILE_CHANGED);
			$this->removeStatusBit(self::ESCORT_STATUS_NOT_APPROVED);
		}

		if ( $rev_count == 1 && $revisions[0]->status == self::REVISION_STATUS_NOT_APPROVED ) {
			$this->setStatusBit(self::ESCORT_STATUS_IS_NEW);
			$this->setStatusBit(self::ESCORT_STATUS_NO_PROFILE);
			$this->setStatusBit(self::ESCORT_STATUS_PROFILE_CHANGED);
		}

		if ( $rev_count == 1 && ( $revisions[0]->status == self::REVISION_STATUS_APPROVED || $revisions[0]->status == self::REVISION_STATUS_DECLINED ) ) {
			$this->removeStatusBit(self::ESCORT_STATUS_IS_NEW);
			$this->removeStatusBit(self::ESCORT_STATUS_PROFILE_CHANGED);
			
			if ( $revisions[0]->status == self::REVISION_STATUS_APPROVED ) {
				$this->removeStatusBit(self::ESCORT_STATUS_NO_PROFILE);
			}
			elseif ( $revisions[0]->status == self::REVISION_STATUS_DECLINED ) {
				$this->setStatusBit(self::ESCORT_STATUS_NO_PROFILE);
			}
		}

		if ( $rev_count > 1 && $has_active_rev ) {
			$this->removeStatusBit(self::ESCORT_STATUS_IS_NEW);
			$this->removeStatusBit(self::ESCORT_STATUS_NO_PROFILE);
			$this->removeStatusBit(self::ESCORT_STATUS_PROFILE_CHANGED);
		}

		if ( $rev_count > 1 && ! $has_active_rev ) {
			$this->setStatusBit(self::ESCORT_STATUS_NO_PROFILE);
		}

		if ( $rev_count > 1 && $revisions[0]->status == self::REVISION_STATUS_NOT_APPROVED ) {
			$this->setStatusBit(self::ESCORT_STATUS_PROFILE_CHANGED);
			
			if ( ! $has_active_rev ) {
				$this->setStatusBit(self::ESCORT_STATUS_NO_PROFILE);
			}
		}

		if ( $this->hasStatusBit(self::ESCORT_STATUS_ADMIN_DISABLED) || $this->hasStatusBit(self::ESCORT_STATUS_OWNER_DISABLED) || $this->hasStatusBit(self::ESCORT_STATUS_NO_PROFILE) ) {
			$this->removeStatusBit(self::ESCORT_STATUS_ACTIVE);
		}
	}

	private function _getEscortRevisionsStatus($escort_id)
	{
		return $this->_db->fetchAll('
			SELECT status
			FROM profile_updates_v2
			WHERE escort_id = ?
			ORDER BY revision DESC
		', $escort_id, Zend_Db::FETCH_OBJ);
	}

	private function check()
	{
		$this->checkPhotos();
		$this->checkRevisions();
        $this->checkDeletion();
	}

	public function save()
	{
		if ( $this->_need_check ) {
			$this->check();
		}

		if ( $this->_changed ) {

			if ( Cubix_Application::getId() == APP_ED ) {
				if ( $this->_status & self::ESCORT_STATUS_ACTIVE ) {
					$this->_db->query('
						UPDATE escorts
						SET status_active_date = NOW()
						WHERE id = ?
					', array($this->_escort_id));					
				} else {
					$this->_db->query('
						UPDATE escorts
						SET status_active_date = NULL
						WHERE id = ?
					', array($this->_escort_id));	
				}
			}


            if ($this->_status == 0 || $this->_status == ESCORT_STATUS_PROFILE_CHANGED){
				$this->setStatusBit(self::ESCORT_STATUS_ACTIVE);
			}
			$this->_db->update('escorts', array(
				'status' => $this->_status
			), $this->_db->quoteInto('id = ?', $this->_escort_id));
			//escort_statuses table
			$this->saveStatus();
			
			if ( ! empty($this->_user_status) ) {
				// Do not disable user if it is agency escort
				$this->_db->query('
					UPDATE users u
					INNER JOIN escorts e ON e.user_id = u.id
					SET u.status = ?
					WHERE e.id = ? AND u.user_type = \'escort\'
				', array($this->_user_status, $this->_escort_id));
			}

			Cubix_SyncNotifier::notify($this->_escort_id, Cubix_SyncNotifier::EVENT_ESCORT_STATUS_UPDATED, array(
				'before' => $this->getStatusText($this->_orig_status) . ' (' . $this->_orig_status. ')',
				'after' => $this->getStatusText($this->_status) . ' (' . $this->_status. ')'
			), $this->_sales_user_id);

			$this->_changed = false;
			return true;
		}

		return false;
	}

	public function getStatusText($bit)
	{
		$status_text = '';

		if ( $bit & self::ESCORT_STATUS_NO_PROFILE ) {
			$status_text .= self::ESCORT_STATUS_NO_PROFILE_TXT;
		}
		if ( $bit & self::ESCORT_STATUS_NO_ENOUGH_PHOTOS ) {
			$status_text .= ', ' . self::ESCORT_STATUS_NO_ENOUGH_PHOTOS_TXT;
		}
		if ( $bit & self::ESCORT_STATUS_NOT_APPROVED ) {
			$status_text .= ', ' . self::ESCORT_STATUS_NOT_APPROVED_TXT;
		}
		if ( $bit & self::ESCORT_STATUS_OWNER_DISABLED ) {
			$status_text .= ', ' . self::ESCORT_STATUS_OWNER_DISABLED_TXT;
		}
		if ( $bit & self::ESCORT_STATUS_ADMIN_DISABLED ) {
			$status_text .= ', ' . self::ESCORT_STATUS_ADMIN_DISABLED_TXT;
		}
		if ( $bit & self::ESCORT_STATUS_ACTIVE ) {
			$status_text .= ', ' . self::ESCORT_STATUS_ACTIVE_TXT;
		}
		if ( $bit & self::ESCORT_STATUS_IS_NEW ) {
			$status_text .= ', ' . self::ESCORT_STATUS_IS_NEW_TXT;
		}
		if ( $bit & self::ESCORT_STATUS_PROFILE_CHANGED ) {
			$status_text .= ', ' . self::ESCORT_STATUS_PROFILE_CHANGED_TXT;
		}
		if ( $bit & self::ESCORT_STATUS_DELETED ) {
			$status_text .= ', ' . self::ESCORT_STATUS_DELETED_TXT;
		}
		if ( $bit & self::ESCORT_STATUS_TEMPRARY_DELETED ) {
			$status_text .= ', ' . self::ESCORT_STATUS_TEMPRARY_DELETED_TXT;
		}
		if ( $bit & self::ESCORT_STATUS_FAKE_PICTURE_DISABLED ) {
			$status_text .= ', ' . self::ESCORT_STATUS_FAKE_PICTURE_DISABLED_TXT;
		}
		if ( $bit & self::ESCORT_STATUS_OFFLINE ) {
			$status_text .= ', ' . self::ESCORT_STATUS_OFFLINE_TXT;
		}
		if ( $bit & self::ESCORT_STATUS_INACTIVE ) {
			$status_text .= ', ' . self::ESCORT_STATUS_INACTIVE_TXT;
		}

		if ( $bit & self::ESCORT_STATUS_PARTIALLY_COMPLETE ) {
			$status_text .= ', ' . self::ESCORT_STATUS_PARTIALLY_COMPLETE_TXT;
		}
		if ( $bit & self::ESCORT_STATUS_IP_BLOCKED ) {
			$status_text .= ', ' . self::ESCORT_STATUS_IP_BLOCKED_TXT;
		}
		if ( $bit & self::ESCORT_STATUS_AWAITING_PAYMENT ) {
			$status_text .= ', ' . self::ESCORT_STATUS_AWAITING_PAYMENT_TXT;
		}

		$status_text = trim($status_text, ',');

		return $status_text;
	}
	
	public function saveStatus()
	{
		$b_user = Zend_Auth::getInstance()->getIdentity();
		
		$status_data = array(
			'escort_id' => $this->_escort_id,
			'status' => $this->_status,
			'date' => new Zend_Db_Expr('NOW()'),
			'backend_user_id' => $b_user->id
		);
		$this->_db->insert('escort_statuses',$status_data);
	}		

}
