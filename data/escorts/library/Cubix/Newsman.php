<?php

class Cubix_Newsman extends Cubix_Model
{
	protected $_rpc_url = '';
	protected $_api_url = '';

	public function __construct($ip = "127.0.0.1")
	{
		$NEWSMAN_user_id = 61;
		$NEWSMAN_rpc_url = "https://ssl.nzsrv.com/rpc";
		//$API_key = "b46131f2cef5472f3e64be5d644f367e";
		$API_key = "ad6da04df34e56930d1a9d7b5b09aa48";

        $this->_rpc_url = $NEWSMAN_rpc_url . "/" . $NEWSMAN_user_id . "/" . $API_key;

        $NEWSMAN_api_url = "https://ssl.nzsrv.com/api/1.2/rest";
        $this->_api_url = $NEWSMAN_api_url . "/" . $NEWSMAN_user_id . "/" . $API_key . "/";
		$_SERVER["REMOTE_ADDR"] = $ip;
		//echo $this->_rpc_url;die;
	}
	
	
	/**
		method: ab.saveSubscribe
		params:
			list_id - int
			email - string (lowercase trim pls)
			firstname - string // utf-8
			lastname - string // utf-8
			ip - string (ip of subscriber $_SERVER["REMOTE_ADDR"]
			props - structure ($abonat_props)

		return:
			int - newsman subscriber_id of newly subscribed user
	*/
	public function subscribe($LIST_ID, $email, $firstname, $lastname = "", $props = array())
	{		
		/*$abonat_props = array(
			"age" => 28,
			"sex" => "m", // m = male, f = female (can be anything)
			"city" => "Bucharest",
			"from" => "top_right_form",
			"registered_username" => "catalin",
			"country" => "Romania"
		);*/
		$client = new Zend_XmlRpc_Client($this->_rpc_url);
		
		try {
			
			$subscriber_id = $client->call("ab.saveSubscribe", array($LIST_ID, $email, $firstname, $lastname, $_SERVER["REMOTE_ADDR"], $props));
			
			return $subscriber_id;
		} catch (Zend_XmlRpc_Client_FaultException $e) { 
			//$e->getCode() returns 404
			//var_dump($e->getMessage());
		} catch (Zend_Http_Client_Adapter_Exception $e) { 
			//$e->getCode() returns 404
			//var_dump($e->getMessage());
		}
	}
	
	/**
	method: ab.saveUnsubscribe
	params:
		list_id - int
		email - string (lowercase trim pls)
		ip - string (ip of user unsubscribing $_SERVER["REMOTE_ADDR"]
	
	return:
		int - newsman subscriber_id of unsubscribed user
	*/
	public function unsubscribe($LIST_ID, $email)
	{
		$client = new Zend_XmlRpc_Client($this->_rpc_url);
		
		try {
			$subscriber_id = $client->call("ab.saveUnsubscribe", array($LIST_ID, $email, $_SERVER["REMOTE_ADDR"]));
			
			return $subscriber_id;
		} catch (Zend_XmlRpc_Client_FaultException $e) { 
			// $e->getCode() returns 404
			//var_dump($e->getMessage());
		}
		catch (Zend_Http_Client_Adapter_Exception $e) { 
			// $e->getCode() returns 404
			//var_dump($e->getMessage());
		}
		
		/*$request = xmlrpc_encode_request("ab.saveUnsubscribe", array($LIST_ID, $email, $_SERVER["REMOTE_ADDR"]), array("encoding" => "utf-8"));
		$context = stream_context_create(array("http" => array("method" => "POST", "header" => "Content-Type: text/xml", "content" => $request)));
		$raw_output = file_get_contents($this->_rpc_url, false, $context);
		$output = xmlrpc_decode($raw_output);
		
		if (@xmlrpc_is_fault($output)) {
			print("ERROR\n");
			var_dump($output);
		} 
		else {
			$subscriber_id = $output;
			return $subscriber_id;
		}*/
	}
	
	
	/**
		method: sg.subscribeAbonat
		params:
			subscriber_id - int
			segment_id - int

		return:
			string - OK for all ok
	*/
	public function bindToSegment($subscriber_id, $segment_id)
	{
		$client = new Zend_XmlRpc_Client($this->_rpc_url);
		
		try {
			$client->call("sg.subscribeAbonat", array($subscriber_id, $segment_id));
			
			return true;
		} catch (Zend_XmlRpc_Client_FaultException $e) { 
			// $e->getCode() returns 404
			//var_dump($e->getMessage());
			return false;
		}
		catch (Zend_Http_Client_Adapter_Exception $e) { 
			// $e->getCode() returns 404
			//var_dump($e->getMessage());
			return false;
		}
		
		/*$request = xmlrpc_encode_request("sg.subscribeAbonat", array($subscriber_id, $segment_id));
		$context = stream_context_create(array("http" => array("method" => "POST", "header" => "Content-Type: text/xml", "content" => $request)));
		$raw_output = file_get_contents($this->_rpc_url, false, $context);
		$output = xmlrpc_decode($raw_output);
		
		if (@xmlrpc_is_fault($output))
		{
			return false;
		} else
		{
			return true;
		}*/
	}


    public function saveSubscribe($list_id, $email, $first_name = '', $last_name = '')
    {
        $params = array(
            'list_id' => $list_id,
            'email' => $email,
            'firstname' => $first_name,
            'lastname' => $last_name,
            'ip' => '192.168.0.1',
            'props' => ''
        );

        return $this->cURLcall('subscriber.saveSubscribe', $params);
    }

    public function saveUnsubscribe($list_id, $email)
    {
        $params = array(
            'list_id' => $list_id,
            'email' => $email,
            'ip' => '192.168.0.1',
        );

        return $this->cURLcall('subscriber.saveUnsubscribe', $params);
    }


    public function addToSegment($subscriber_id, $segment_id)
    {
        $params = array(
            'subscriber_id' => $subscriber_id,
            'segment_id' => $segment_id
        );

        return $this->cURLcall('subscriber.addToSegment', $params);
    }


    public function removeFromSegment($subscriber_id, $segment_id)
    {
        $params = array(
            'subscriber_id' => $subscriber_id,
            'segment_id' => $segment_id
        );

        return $this->cURLcall('subscriber.removeFromSegment', $params);
    }


	public function getSubscriberByEmail($list_id, $email)
	{
	    $params = array(
	        'list_id' => $list_id,
            'email' => $email
        );

        return $this->cURLcall('subscriber.getByEmail', $params);
	}


	protected function cURLcall($method, $params = array())
    {
        $url = $this->_api_url . $method . '.json?' . http_build_query($params);

        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            $result = curl_exec($ch);

            return json_decode($result, true);

        } catch (Zend_XmlRpc_Client_FaultException $e) {
//			var_dump($e->getMessage());
            return false;
        }
        catch (Zend_Http_Client_Adapter_Exception $e) {
//			var_dump($e->getMessage());
            return false;
        }
    }

}
