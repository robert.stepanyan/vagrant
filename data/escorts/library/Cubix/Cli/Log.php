<?php

class Cubix_Cli_Log_Exception extends Exception {}

class Cubix_Cli_Log
{
	private $_file;
	private $_fp;

	const INFO = 1;
	const ERROR = 2;

	public function __construct($file)
	{
		$this->_file = $file;
		$this->_open();
	}

	private function _open()
	{
		if ( ! $this->_fp = @fopen($this->_file, 'w+') ) {
			throw new Cubix_Cli_Log_Exception('Unable to open ' . $this->_file . ' for writing');
		}

		if ( ! @flock($this->_fp, LOCK_EX) ) {
			throw new Cubix_Cli_Log_Exception('Unable to lock ' . $this->_file . ' for writing');
		}
	}

	private function _close()
	{
		@flock($this->_fp, LOCK_UN);
		@fclose($this->_fp);
	}

	private function _write($text)
	{
		$text .= "\n";
		fputs($this->_fp, $text, strlen($text));
	}

	public function __destruct()
	{
		$this->_close();
	}


	
	public function log($text, $status = self::INFO)
	{
		$text = $this->_formatStatus($status) . $text . "\n";
		$this->_write($text);
	}

	public function error($text)
	{
		$this->log($text, self::ERROR);
	}

	public function info($text)
	{
		$this->log($text, self::INFO);
	}

	private function _formatStatus($status)
	{
		$str = '[' . date('d.m.y H:i:s') . ' ';

		switch ($status) {
			case self::INFO:
				$str .= 'INFO';
				break;
			case self::ERROR:
				$str .= 'ERROR';
				break;
			default:
				$str .= 'UNKNOWN';
		}
		$str .= '] ';

		return $str;
	}
}
