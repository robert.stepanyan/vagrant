<?php

class Cubix_CityAlertEvents
{
	private static function _db()
	{
		return Zend_Registry::get('db');
	}

	public static function notify($city_id, $escort_id, $agency_id)
	{
		$date = new Zend_Db_Expr('NOW()');
				
		$data = array(
			'date' => $date,
			'escort_id' => $escort_id,
			'agency_id' => $agency_id,
			'city_id' => $city_id,
			'application_id' => Cubix_Application::getId()
		);

		self::_db()->insert('city_alerts_events', $data);
	}
}
