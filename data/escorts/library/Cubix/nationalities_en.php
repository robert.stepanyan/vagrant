<?php

// WARNING: If you add another nationality,
// you must add its ISO code to the $NATIONALITIES in include/nationalities.inc.php.

$_NATS['al'] = "Albanian";
$_NATS['us'] = "American";
$_NATS['ar'] = "Argentinian";
$_NATS['au'] = "Australian";
$_NATS['at'] = "Austrian";
$_NATS['be'] = "Belgian";
$_NATS['br'] = "Brazilian";
$_NATS['gb'] = "British";
$_NATS['bg'] = "Bulgarian";
$_NATS['ca'] = "Canadian";
$_NATS['cn'] = "Chinese";
$_NATS['cz'] = "Czech";
$_NATS['dk'] = "Danish";
$_NATS['nl'] = "Dutch";
$_NATS['fr'] = "French";
$_NATS['de'] = "German";
$_NATS['gr'] = "Greek";
$_NATS['hu'] = "Hungarian";
$_NATS['in'] = "Indian";
$_NATS['ie'] = "Irish";
$_NATS['il'] = "Israeli";
$_NATS['it'] = "Italian";
$_NATS['jm'] = "Jamaican";
$_NATS['jp'] = "Japanese";
$_NATS['my'] = "Malaysian";
$_NATS['md'] = "Moldovian";
$_NATS['pl'] = "Polish";
$_NATS['pt'] = "Portuguese";
$_NATS['ro'] = "Romanian";
$_NATS['ru'] = "Russian";
$_NATS['sk'] = "Slovak";
$_NATS['es'] = "Spanish";
$_NATS['se'] = "Swedish";
$_NATS['ch'] = "Swiss";
$_NATS['th'] = "Thai";
$_NATS['ua'] = "Ukrainian";
$_NATS['ve'] = "Venezuelan";

$_NATS['by'] = "Belarussian";
$_NATS['ee'] = "Estonian";
$_NATS['fi'] = "Finnish";
$_NATS['hr'] = "Croatian";
$_NATS['kr'] = "Korean";
$_NATS['lt'] = "Lithuanian";
$_NATS['lv'] = "Latvian";
$_NATS['me'] = "Montenegrian";
$_NATS['mx'] = "Mexican";
$_NATS['no'] = "Norwegian";
$_NATS['ph'] = "Filipino";
$_NATS['rs'] = "Serbian";
$_NATS['sg'] = "Singaporean";
$_NATS['si'] = "Slovenian";
$_NATS['cu'] = "Cuban";
$_NATS['co'] = "Colombian";
$_NATS['pr'] = "Puerto Rican";
$_NATS['vn'] = "Vietnamese";
