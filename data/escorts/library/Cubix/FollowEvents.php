<?php

class Cubix_FollowEvents
{
	private static function _db()
	{
		return Zend_Registry::get('db');
	}

	public static function notify($type_id, $type, $event, $rel_data, $check = false)
	{
		if($check ){
			if(self::check($type_id, $type,$event, $rel_data['rel_id'])){
				return false;
			}
		}
		
		$date = new Zend_Db_Expr('NOW()');
		
		$data = array(
			'type' => $type,
			'type_id' => $type_id,
			'date' => $date,
			'event' => $event
		);

		$data = array_merge($data, $rel_data);
				
		self::_db()->insert('follow_events', $data);
	}
	
	private static function check($type_id, $type, $event, $rel_id)
	{
		$where = "";
		if(in_array($event, array(FOLLOW_ESCORT_NEW_PROFILE, FOLLOW_ESCORT_NEW_PICTURE,FOLLOW_ESCORT_NEW_CITY, FOLLOW_ESCORT_NEW_PRICE,FOLLOW_AGENCY_NEW_CITY ))){
			$where = " AND sent = 0";
		}
		$sql = "SELECT TRUE FROM follow_events WHERE type_id = ? AND type = ? AND rel_id = ? AND event = ? ". $where;
		return self::_db()->fetchOne($sql, array($type_id, $type, $rel_id, $event) );
	}
	
}
