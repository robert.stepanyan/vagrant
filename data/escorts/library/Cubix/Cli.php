<?php

class Cubix_Cli_Exception extends Exception {}

class Cubix_Cli
{
	public static function isRunning()
	{
		$pid_file = self::getPidFile();
		
		if ( is_file($pid_file) ) {
			$pid = intval(file_get_contents($pid_file));
			if ( $pid > 0 ) {
				$output = array();
				$cmd = "ps -p $pid";
				exec($cmd, $output);
				
				if ( count($output) >= 2 ) {
					
					/*$found = intval($output[0]);
					if ( $pid == $found ) {
						return true;
					}*/
					return true;
				}
			}
		}

		file_put_contents($pid_file, getmypid());
		return false;
	}
	
	public static function pidIsRunning($pid_file)
	{
		if ( is_file($pid_file) ) {
			$pid = intval(file_get_contents($pid_file));
			if ( $pid > 0 ) {
				$output = array();
				$cmd = "ps -p $pid";
				exec($cmd, $output);
				
				if ( count($output) >= 2 ) {
					
					return true;
				}
			}
		}

		return false;
	}

	private static $_pid_file = null;

	public static function setPidFile($file)
	{
		self::$_pid_file = $file;
	}

	public static function getPidFile()
	{
		return self::$_pid_file;
	}

	private $_fg_colors = array();
	
	private $_fg_color = null;
	
	public function __construct()
	{
		$this->_fg_colors['black'] = '0;30';
		$this->_fg_colors['dark_gray'] = '1;30';
		$this->_fg_colors['blue'] = '0;34';
		$this->_fg_colors['light_blue'] = '1;34';
		$this->_fg_colors['green'] = '0;32';
		$this->_fg_colors['light_green'] = '1;32';
		$this->_fg_colors['cyan'] = '0;36';
		$this->_fg_colors['light_cyan'] = '1;36';
		$this->_fg_colors['red'] = '0;31';
		$this->_fg_colors['light_red'] = '1;31';
		$this->_fg_colors['purple'] = '0;35';
		$this->_fg_colors['light_purple'] = '1;35';
		$this->_fg_colors['brown'] = '0;33';
		$this->_fg_colors['yellow'] = '1;33';
		$this->_fg_colors['light_gray'] = '0;37';
		$this->_fg_colors['white'] = '1;37';

		$this->_bg_colors['black'] = '40';
		$this->_bg_colors['red'] = '41';
		$this->_bg_colors['green'] = '42';
		$this->_bg_colors['yellow'] = '43';
		$this->_bg_colors['blue'] = '44';
		$this->_bg_colors['magenta'] = '45';
		$this->_bg_colors['cyan'] = '46';
		$this->_bg_colors['light_gray'] = '47';
	}

	

	public function setFgColor($alias = null)
	{
		if ( ! is_null($alias) && ! array_key_exists($alias, $this->_fg_colors) ) {
			throw new Cubix_Cli_Exception('Invalid foreground color alias ' . $alias);
		}

		$this->_fg_color = $alias;

		return $this;
	}
	
	public function setBgColor($alias = null)
	{
		
	}

	public function resetColor()
	{
		echo $this->_reset_color;
		return $this;
	}

	private function _getColor()
	{
		return (! is_null($this->_fg_color) ? "\033[" . $this->_fg_colors[$this->_fg_color] . "m" : '') .
			   (! is_null($this->_bg_color) ? "\033[" . $this->_bg_colors[$this->_fg_color] . "m" : '') ;
	}

	public function status($status, $col_width = 60)
	{
		$fg_color = $this->_fg_color;
		switch ( $status ) {
			case 'error':
				$this->jump(null, $col_width)->setFgColor('red')->out('[  error  ]');
				break;
			case 'success':
				$this->jump(null, $col_width)->setFgColor('green')->out('[ success ]');
				break;
		}
		$this->setFgColor($fg_color);
	}

	public function error($text)
	{
		$_fg_color = $this->_fg_color;
		$_bg_color = $this->_bg_color;

		$this->setFgColor('red')
				->out($text)
				->setFgColor($_fg_color)
				->setBgColor($_bg_color);
		@ob_flush();
		return $this;
	}

	public function success($text)
	{
		$_fg_color = $this->_fg_color;
		$_bg_color = $this->_bg_color;

		$this->setFgColor('green')
				->out($text)
				->setFgColor($_fg_color)
				->setBgColor($_bg_color);
		@ob_flush();
		return $this;
	}

	public function replacePrevLine($text, $width = 80)
	{
		echo "\033[1A\033M" . $this->_getColor() . $text . str_repeat(' ', ($_ = $width - strlen($text)) > 0 ? $_ : 0) . "\033D\n";
		$this->resetColor();
		@ob_flush();
		return $this;
	}

	public function move($line = null, $col = null)
	{
		if ( ! is_null($line) ) {
			echo "\033[" . $line . "H";
		}

		if ( ! is_null($col) ) {
			echo "\033[" . $col . "G";
		}
		
		return $this;
	}

	public function clear()
	{
		echo "\033[2J\033[H";
	}

	public function colored($text, $color)
	{
		$fg_color = $this->_fg_color;
		$this->setFgColor($color);
		$this->out($text);
		$this->setFgColor($fg_color);
	}




	public function reset()
	{
		echo "\033[0m";
		return $this;
	}

	public function out($text = '', $new_line = true)
	{
		echo $text;
		if ( $new_line ) {
			echo "\n";
		}

		@ob_flush();
		return $this;
	}

	public function colorize($alias)
	{
		if ( ! is_null($alias) && ! array_key_exists($alias, $this->_fg_colors) ) {
			throw new Cubix_Cli_Exception('Invalid foreground color alias ' . $alias);
		}
		$code = $this->_fg_colors[$alias];
		
		echo "\033[{$code}m";
		return $this;
	}

	public function prev()
	{
		echo "\033[1A";
		$this->column(1);
		return $this;
	}
	
	public function column($n)
	{
		echo "\033[{$n}G";
		return $this;
	}

	public function erase($count = null)
	{
		if ( null == $count ) {
			echo "\033[K";
		}
		else {
			echo "\033[{$count}X";
		}
		
		return $this;
	}
}
