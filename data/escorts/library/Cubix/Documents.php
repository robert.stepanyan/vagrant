<?php

class Cubix_Documents
{
		
	/**
	 * @var Cubix_Images_Storage_Ftp
	 */
	protected $_storage;
	
	/**
	 * The host of the current application
	 *
	 * @var string
	 */
	protected $_application;
	
	/**
	 * Configuration from application init
	 *
	 * @var array
	 */
	protected $_config;
	
	public $allowed_ext = array('rtf','doc','txt','pdf','jpg','gif','png','bmp','doc','docx','rtf','ppt','pptx','xls','xlsx');
	
	public function __construct()
	{
		$this->_config = $config = Zend_Registry::get('images_config');
		$this->_storage = new Cubix_Images_Storage_Ftp($config['remote']['ftp']);
		$this->_application = Cubix_Application::getById(Cubix_Application::getId())->host;
	}
	
	/**
	 * Save the file on the remote machine
	 *
	 * @param string $file 
	 * @param string $catalog_id the same as escort id
	 * @return array
	 * @author GuGo
	 */
	public function save($file, $catalog_id, $application_id = null, $ext_override = null)
	{
		
		// Create a random file name
		if ( ! $hash ) {
			$hash = md5(microtime(TRUE) * mt_rand(1, 1000000));
		}
		
		$ext = is_null($ext_override) ? strtolower(@end(explode('.', $file))) : $ext_override;
		if (!in_array($ext, $this->allowed_ext  ) )
		{
			 die('Not allowed file extension.');
		}
		$remote = $this->_getFullPath(new Cubix_Images_Entry(array(
			'application_id' => $application_id,
			'catalog_id' => $catalog_id,
			'hash' => $hash,
			'ext' => $ext
		)));
		
		if ( ! is_null($application_id) ) {
			$application = Cubix_Application::getById($application_id)->host;
		}
		else {
			$application = $this->_application;
		}
		
		$dirs = array();
		$i = 0;
		$dir = $remote;
		while ( ($dir = dirname($dir)) != '/' . $application ) {
			$i++;
			$dirs[] = $dir;
			if ( $i > 100 ) die('error');
		}
		
		// Connect to ftp
		$this->_storage->connect();
		
		// Create directories if they does not exist
		$dirs = array_reverse($dirs);
		
		foreach ( $dirs as $dir ) {
			try {
				$this->_storage->makeDir($dir);
			} catch (Cubix_Images_Storage_Exception $e) {
				// Probably folder already exists
			}
		}
		
		try {
			// Save the local file on remote storage
			$this->_storage->store($file, $remote);
			$this->_storage->disconnect();
		} catch (Cubix_Images_Storage_Exception $e) {
			return false;
		}
		
		return array('hash' => $hash, 'ext' => $ext);
	}
	
	public function remove($entries)
	{
		if ( ! is_array($entries) ) {
			$entries = array($entries);
		}
		
		if ( ! count($entries) ) return;
		
		$remote = $this->_getFullPath($entries[0]);
		$path = dirname($remote);
		
		try {
			$this->_storage->connect();
			$list = $this->_storage->getFiles($path);
			
			foreach ( $entries as $entry ) {
				foreach ( $list as $filename ) {
					if ( preg_match('/' . $entry->hash . '(_.+)?\.' . $entry->ext . '/', $filename) ) {
						$this->_storage->delete('/' . $filename);
					}
				}
			}
			
			$this->_storage->disconnect();
		}
		catch ( Exception $e ) {
			$this->_storage->disconnect();
		}
	}
	
	protected function _getFullPath(Cubix_Images_Entry $entry)
	{
		$application = $this->_application;
		
		if ( isset($entry->application_id) ) {
			$application = Cubix_Application::getById($entry->application_id)->host;
		}
				
		$catalog = $entry->getCatalogId();
		$parts = array($catalog);
		
		$a = array();
		if ( is_numeric($catalog) ) {
			$parts = array();
			
			if ( strlen($catalog) > 2 ) {
				$parts[] = substr($catalog, 0, 2);
				$parts[] = substr($catalog, 2);
			}
			else {
				$parts[] = '_';
				$parts[] = $catalog;
			}
		}
		else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
			array_shift($a);
			$catalog = $a[0];
			
			$parts = array();
			
			if ( strlen($catalog) > 2 ) {
				$parts[] = substr($catalog, 0, 2);
				$parts[] = substr($catalog, 2);
			}
			else {
				$parts[] = '_';
				$parts[] = $catalog;
			}
			
			$parts[] = $a[1];
		}
				
		$catalog = implode('/', $parts);

		$path = '/' . $application . '/' . $catalog . '/' . $entry->getHash() . (isset($entry->size) ? '_' . $entry->getSize() : '') . '.' . $entry->getExt();
		
		return $path;
	}
	
	/**
	 * Returns url of image on the remote machine
	 * If the application id is not specified, current application will be used
	 *
	 * @param Cubix_Images_Entry $entry 
	 * @return string
	 * @author GuGo
	 */
	public function getUrl(Cubix_Images_Entry $entry)
	{
		
		return $this->_clusterizeUrl($entry,
			trim($this->_config['remote']['url'], '/') .
			$this->_getFullPath($entry)
		);
	}
	
	public function getServerUrl(Cubix_Images_Entry $entry)
	{
		return $this->_clusterizeUrl($entry, 
			trim($this->_config['remote']['serverUrl'], '/') .
			$this->_getFullPath($entry)
		);
	}

	/**
	 * If there is a config for clustering image server this function will
	 * append a random number between 1 and specified one in config file
	 * If there is no config the url will not be affected
	 *
	 * @param string $url
	 * @return string The resulting url
	 */
	private function _clusterizeUrl($entry, $url)
	{
		if ( isset($this->_config['remote']['clusterize']) &&
				$this->_config['remote']['clusterize'] &&
				isset($this->_config['remote']['cluster_max']) &&
				($max = $this->_config['remote']['cluster_max']) > 0 ) {
			$url = preg_replace('#^(http://pic)\.#', '${1}' . $this->_decideClusterId($entry) . '.', $url);
		}

		return $url;
	}

	/**
	 * Will calculate the id of cluster for this entry
	 *
	 * @param Cubix_Images_Entry $entry
	 */
	private function _decideClusterId($entry)
	{
		$ascii = 0;
		for ( $i = 0; $i < strlen($entry->hash); ++$i ) $ascii += ord(substr($entry->hash, $i, 1));
		$base = ord('1') * strlen($entry->hash);
		$ascii -= $base;
		$max_ascii = ord('f') * strlen($entry->hash) - $base;
		$percent = $ascii / $max_ascii;
		return round($percent * $this->_config['remote']['cluster_max']);
	}
	
	public function getPathWithSize($path, $size)
	{
		$parts = explode('/', $path);
		$file = array_pop($parts);
		$path = implode('/', $parts);
		$files_parts = explode('.', $file);
		$new_file = $files_parts[0] . '_' . $size . '.' . $files_parts[1];
		$path = $path . '/' . $new_file;
		
		return $path;
	}
}
