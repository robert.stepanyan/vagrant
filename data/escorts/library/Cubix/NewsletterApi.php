<?php

class Cubix_NewsletterApi extends Cubix_Model
{
	protected $_url = '';
	protected $_username = '';
	protected $_usertoken = '';
	
	public function  __construct()
	{
		if (Cubix_Application::getId() == 1)
		{
			$this->_url = 'http://newsletter.escortforum.pw/xml.php';
			$this->_username = 'admin';
			$this->_usertoken = '95f7ac2e021cf55f8fdb6e38c917dd8f659e8fff';
		}
		elseif (Cubix_Application::getId() == 11)
		{
			$this->_url = 'http://newsletter.6annonce.com/xml.php';
			$this->_username = 'admin';
			$this->_usertoken = '97ad5a4935aec5d8821ed7e94814b61ee9f0adad';
		}
		elseif (Cubix_Application::getId() == 2)
		{
			$this->_url = 'http://newsletter.6annonce.com/xml.php';
			$this->_username = 'admin';
			$this->_usertoken = '97ad5a4935aec5d8821ed7e94814b61ee9f0adad';
		}
	}
	
	protected function _makeXML($params = array())
	{
		$ret = array();

		// validation
		if (!$params['requesttype'])
		{
			$ret['error'] = 'Param Request Type required.';

			return $ret;
		}

		if (!$params['requestmethod'])
		{
			$ret['error'] = 'Param Request Method required.';

			return $ret;
		}
		//

		$xml = '
			<xmlrequest>
				<username>' . $this->_username . '</username>
				<usertoken>' . $this->_usertoken . '</usertoken>
				<requesttype>' . $params['requesttype'] . '</requesttype>
				<requestmethod>' . $params['requestmethod'] . '</requestmethod>
				<details>
		';

		if ($params['details'])
		{
			foreach ($params['details'] as $k => $v)
			{
				if ($k == 'customfields')
				{
					$xml .= '<customfields>';

					foreach ($v as $kk => $vv)
					{
						$xml .= '<' . $kk . '>' . htmlspecialchars($vv) . '</' . $kk . '>';
					}

					$xml .= '</customfields>';
				}
				else
				{
					$xml .= '<' . $k . '>' . htmlspecialchars($v) . '</' . $k . '>';
				}
			}
		}

		$xml .= '
				</details>
			</xmlrequest>
		';

		$ret['xml'] = $xml;

		return $ret;
	}

	protected function _execXML($xml)
	{
		$ch = curl_init($this->_url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

		$result = curl_exec($ch);

		$ret = array();

		if ($result === false)
		{
			$ret['error'] = "Error performing request";
		}
		else
		{
			$xml_doc = simplexml_load_string($result);

			$ret['status'] = $xml_doc->status;

			if ($xml_doc->status == 'SUCCESS')
			{
				$ret['data'] = $xml_doc->data;
			}
			else
			{
				$ret['error'] = $xml_doc->errormessage;
			}
		}

		return $ret;
	}

	public function process($params)
	{
		$xml = $this->_makeXML($params);
		$ret = $this->_execXML($xml);

		return $ret;
	}
	
	public function getNewsletterLog()
	{
		return parent::getAdapter()->query('SELECT id, user_id, user_type, action, old_email, new_email FROM newsletter_email_log WHERE status = 0 ORDER BY date ASC')->fetchAll();
	}

	public function changeNewslettersLogStatus($ids, $status = 1)
	{
		return parent::getAdapter()->query('UPDATE newsletter_email_log SET status = ' . $status . ' WHERE id IN (' . $ids . ')');
	}
}
