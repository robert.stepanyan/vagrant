<?php

class Cubix_Eurobill_Callback_Exception extends Exception {}

class Cubix_Eurobill_Callback_UsernameRequired extends Cubix_Eurobill_Callback_Exception {}
class Cubix_Eurobill_Callback_UsernameDoesntExist extends Cubix_Eurobill_Callback_Exception {}
class Cubix_Eurobill_Callback_ActionRequired extends Cubix_Eurobill_Callback_Exception {}
class Cubix_Eurobill_Callback_ActionInvalid extends Cubix_Eurobill_Callback_Exception {}
class Cubix_Eurobill_Callback_MembershipPlanUnknown extends Cubix_Eurobill_Callback_Exception {}

class Cubix_Eurobill_Callback
{
	static private $_actions = array('activation', 'billing', 'reactivation', 'rebilling', 'expiration');
	static private $_plans = array(4, 5, 6);

	static public function db()
	{
		return Zend_Registry::get('db');
	}

	private $_params = array();

	private $_id;

	public function getId()
	{
		return $this->_id;
	}

	public function __construct(stdClass $params)
	{
		$this->_params = $params;
		$this->_storeRaw();
	}

	// <editor-fold defaultstate="collapsed" desc="(Set/get)-ers">
	public function get($name, $default = null)
	{
		return isset($this->_params->$name) ? $this->_params->$name : $default;
	}

	public function set($name, $value)
	{
		$this->_params->$name = $value;
	}
	// </editor-fold>

	private function _validate()
	{
		if ( ! $this->get('username') ) {
			throw new Cubix_Eurobill_Callback_UsernameRequired();
		}

		if ( ! $this->get('action') ) {
			throw new Cubix_Eurobill_Callback_ActionRequired();
		}
		elseif ( ! in_array($this->get('action'), self::$_actions) ) {
			throw new Cubix_Eurobill_Callback_ActionInvalid("Got `{$this->get('action')}`, but must be one of " . implode(', ', self::$_actions));
		}

		if ( ! in_array((int) $this->get('plan'), self::$_plans) ) {
			throw new Cubix_Eurobill_Callback_MembershipPlanUnknown("Got `{$this->get('plan')}`, but must be one of " . implode(', ', self::$_plans));
		}

		$this->_notify('validate', array($this));
	}

	private function _storeRaw()
	{
		$data = json_encode($this->_params);

		self::db()->insert('eurobill_callback', array(
			'date' => new Zend_Db_Expr('NOW()'),
			'raw' => $data
		));
		$this->_id = self::db()->lastInsertId();
	}

	public function setError($e)
	{
		self::db()->update('eurobill_callback', array(
			'error' => $e
		), self::db()->quoteInto('id = ?', $this->_id));
	}

	public function handle()
	{
		try {
			$this->_validate();
		}
		catch ( Exception $e ) {
			$this->setError($e->__toString());
			throw $e;
		}

//		$db = Zend_Registry::get('db');
//		$db->update('cgp_callback', array(
//			'transaction_id' => (int) $this->get('transaction')->id,
//			'ref' => $this->get('ref'),
//			'is_test' => (bool) $this->get('is_test'),
//			'status' => (int) $this->get('status'),
//			'status_desc' => $this->_statuses[(int) $this->get('status')],
//			'amount' => (int) $this->get('amount')
//		), $db->quoteInto('id = ?', $this->_id));

		if ( 'expiration' == $this->get('action') ) {
			$this->_notify('expiration', array($this));
		}
		else {
			$this->_notify('payment', array($this));
		}
	}

	// <editor-fold defaultstate="collapsed" desc="Hooking Realization">
	private $_hooks = array();

	public function registerHook($name, $callback)
	{
		if ( ! isset($this->_hooks[$name]) ) {
			$this->_hooks[$name] = array();
		}

		$this->_hooks[$name][] = $callback;
	}

	private function _notify($name, array $args = array())
	{
		if ( isset($this->_hooks[$name]) ) {
			foreach ( $this->_hooks[$name] as $callback ) {
				if ( false === call_user_func_array($callback, $args) ) {
					break;
				}
			}
		}
	}
	// </editor-fold>
}
