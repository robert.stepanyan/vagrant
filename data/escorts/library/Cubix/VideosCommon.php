<?php

class Cubix_VideosCommon
{
	const ERROR_IMAGE_EXT_NOT_ALLOWED = 1;
	const ERROR_IMAGE_INVALID = 2;
	const ERROR_IMAGE_TOO_SMALL = 3;
	const ERROR_IMAGE_TOO_BIG = 4;
	const ERROR_IMAGE_COUNT_LIMIT_OVER = 3;
	
	/**
	 * @var Cubix_Images_Storage_Ftp
	 */
	protected $_storage;
	
	/**
	 * The host of the current application
	 *
	 * @var string
	 */
	protected $_application;
	protected $_application_id;
	
	/**
	 * Configuration from application init
	 *
	 * @var array
	 */
	protected $_config;
	
	public function __construct()
	{
		$this->_config = Zend_Registry::get('videos_config');
		$this->_storage = new Cubix_Images_Storage_Ftp($this->_config['remote']['ftp']);
		$this->_application = Cubix_Application::getById(Cubix_Application::getId())->host;
		$this->_application_id = Cubix_Application::getId();

	}

	

	
	protected function _saveToDB($image_data)
	{		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$image_id = $client->call('Application.saveImageV2', array($image_data));
		if ( is_array($image_id) ) {
			throw new Exception($image_id['error']);
		}
		
		$image_data = array_merge($image_data, array('image_id' => $image_id, 'application_id' => $this->_application_id));	
		
		return $image_data;
	}
	
	public function _storeToPicVideo($video,$id,$name)
	{	$return=false;
		if(isset($this->_application_id))
		{	
			try{
				$this->_storage->connect();
			}
			catch (Exception $e)
			{
				var_dump($e->getMessage());die;
			}
			
			$path='/'.$this->_application;
			try{
				$this->_storage->makeDir($path);
			}
			catch (Exception $e)
			{
				
			}
			$path.='/'.$id;
			try{
				$this->_storage->makeDir($path);
			}
			catch (Exception $e)
			{
				
			}
			try{
				$return = $this->_storage->store($video, $path.'/'.$name);
			}
			catch (Exception $e)
			{
				var_dump($e->getMessage());die;
			}	
			$this->_storage->disconnect();
		}

		if(Cubix_Application::getId() == APP_ED) {
		    return [$path.'/'.$name, $return];
        }

		return $return;
	}
	
	public function _storeToPicVideoV2($videos,$id,$hash)
	{	$return = false;
		if(isset($this->_application_id))
		{		
			$this->_storage->connect();
			$path='/'.$this->_application;
			try{
				$this->_storage->makeDir($path);
			}
			catch (Exception $e)
			{
				
			}
			$path.='/'.$id;
			try{
				$this->_storage->makeDir($path);
			}
			catch (Exception $e)
			{
				
			}
			foreach($videos as $height => $video){
				$name = $hash."_".$height.".flv";
				$return = $this->_storage->store($video, $path.'/'.$name);
			}
			
			$this->_storage->disconnect();
		}
		return $return;
	}
	
	public function download($local, $remote)
	{
		$this->_storage->connect();
		try{
				$this->_storage->retreive($local, $remote);
		}
		catch (Exception $e)
		{
			var_dump($e->getMessage());die;
		}
		
		return TRUE;
	}

    public function retrive($video, $id) {
        $path='/'.$this->_application;
        $path.='/'.$id.'/';
        $remote = $path.$video->hash.'.'.$video->ext;
        $video_config =  Zend_Registry::get('videos_config');
        //shared folder for APP_EF only
        $temp_dir_path = null;
        if (!empty($video_config['temp_dir_path']) && is_dir($video_config['temp_dir_path']) && Cubix_Application::getId() == APP_EF){
            $temp_dir_path = $video_config['temp_dir_path'];
        }
        $tmp_dir = ($temp_dir_path) ? $temp_dir_path : sys_get_temp_dir();
        $unique = uniqid();
        $name = $unique.'.'.$video->ext;
        $dir = $tmp_dir.'/escort/';
        if(!is_dir($dir)){
            mkdir($dir);
        }
        $local = $dir.$name;
        $this->download($local, $remote);
        return $local;
	}

}
