<?php

abstract class Cubix_Model
{
	/**
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected static $_db;
	
	protected $_table;
	
	protected $_itemClass;
	protected $_app_id;

	public function __construct()
	{
		$this->_db = Zend_Registry::get('db');
		
		$app_id = Cubix_Application::getId();
		if ( is_null($app_id) ) {
			$app_id = Zend_Controller_Front::getInstance()->getRequest()->application_id;
		}

		$this->_app_id = $app_id;
	}
	
	public static function setAdapter(Zend_Db_Adapter_Abstract $adapter)
	{
		self::$_db = $adapter;
	}
	
	/**
	 * @return Zend_Db_Adapter_Abstract
	 */
	public static function getAdapter()
	{
		return self::$_db;
	}

	/**
	 * @return Zend_Db_Adapter_Abstract
	 */
	public static function db()
	{
		return self::$_db;
	}
	
	protected function _fetchAll($sql, $bind = array())
	{
		$result = self::getAdapter()->query($sql, $bind)->fetchAll(Zend_Db::FETCH_OBJ);

		if (isset($this) && ! empty($this->_itemClass) ) {
			foreach ( $result as $i => $row ) {
				$result[$i] = new $this->_itemClass($row);
				$result[$i]->setAdapter(self::getAdapter());
			}
		}
		
		return $result;
	}
	
	protected function _fetchRow($sql, $bind = array())
	{
		$result = self::getAdapter()->fetchRow($sql, $bind, Zend_Db::FETCH_OBJ);
		
		if ( ! $result ) {
			return NULL;
		}
		
		if (isset($this) && ! empty($this->_itemClass) ) {
			$item = new $this->_itemClass($result);
			$item->setAdapter(self::getAdapter());
			return $item;
		}
		
		return $result;
	}
	
	public function save($item)
	{
		$where = (array) $where;
		$id = $item->getId();
		$data = $item->getData();
		
		if ( ! is_null($id) ) {
			//var_dump($data);die;
			self::getAdapter()->update($this->_table, $data, self::quote('id = ?', $id));
		}
		else {
			self::getAdapter()->insert($this->_table, $data);
		}
	}
	
	public function remove($where)
	{
		$where = (array) $where;
		self::getAdapter()->delete($this->_table, $where);
	}
	
	public static function quote($expr, $value)
	{
		return self::getAdapter()->quoteInto($expr, $value);
	}

	public static function quoteMulti($expr, array $value)
	{
		$count = count($value);
		while ($count > 0) {
			if (strpos($expr, '?') !== false) {
				$expr = substr_replace($expr, self::getAdapter()->quote($value[$count - 1]), strpos($expr, '?'), 1);
			}
			--$count;
		}
		return $expr;
	}

	public static function getSql(array $sql, $calc_found_rows = false)
	{
		$where = null;
		if ( is_array($sql['where']) ) {
			$where = self::getWhereClause($sql['where']);
		}
		
		$tables = $sql['tables'];
		if ( is_array($tables) ) {
			$tables = implode(', ', $tables);
		}

		$sSQL = 'SELECT ' . (($calc_found_rows) ? ' SQL_CALC_FOUND_ROWS ' : '') . implode(', ', $sql['fields']) . "\n" . ' FROM ' . $tables . ' ' . "\n" .
				implode(' ' . "\n", $sql['joins']) . (! is_null($where) ? ' WHERE ' . $where : '' ) . "\n" .
				( (isset($sql['group']) && $sql['group'] !== false) ? ' GROUP BY ' . $sql['group'] : '' ) . "\n" .
				( (isset($sql['having']) && $sql['having'] !== false) ? ' HAVING ' . $sql['having'] : '' ) . "\n" .
				( (isset($sql['order']) && $sql['order'] !== false) ? ' ORDER BY ' . $sql['order'][0] . ' ' . $sql['order'][1] : '' ) . "\n" .
				( (isset($sql['page']) && $sql['page'] !== false) ? ' LIMIT ' . ($sql['page'][0] - 1) * $sql['page'][1] . ', ' . $sql['page'][1] : '' );

		return $sSQL;
	}

	public static function getWhereClause(array $array, $ignore_null = true)
	{
		foreach ( $array as $condition => $value ) {
			if ( ! defined('IN_BACKEND') || ! IN_BACKEND ) {
				if ( $ignore_null && is_null($value) && ! is_array($value) ) continue;
			}
			else {
				if ( ! $value && ! is_array($value) ) continue;
			}

			if ( is_int($condition) ) {
				$where[] = $value;
			}
			elseif ( ! is_array($value) ) {
				$where[] = self::quote($condition, $value);
			}
			elseif ( is_array($value) ) {
				$where[] = self::quoteMulti($condition, $value);
			}
		}

		if ( ! isset($where) || ! count($where) ) {
			return null;
		}

		return implode(' AND ', $where);
	}

	/**
	 *
	 * @return Zend_Cache_Backend_Memcached
	 */
	static public function cache()
	{
		return Zend_Registry::get('cache');
	}

	static public function config($section = 'system')
	{
		return Cubix_Utils::arrToObj(Zend_Registry::get($section . '_config'));
	}
	
	public static function translit($str)
	{
		$caracteres_especiais = array(
			'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 
			'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 
			'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 
			'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 
			'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
			'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 
			'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 
			'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 
			'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 
			'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f', 'ü' => 'u'
		);
		
		return strtr($str, $caracteres_especiais);
	}
}
