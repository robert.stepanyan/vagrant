<?php


class Cubix_PrivateMessaging extends Cubix_Model {
	private $type;
	private $id;
	private $api_client;
	
	public function __construct($type, $id) {
		$this->type = $type;
		$this->id = $id;
		
		$this->api_client = new Cubix_Api_XmlRpc_Client();
	}
	
	public function sendMessage($body, $participant_type, $participant_id, $for_paids_only = true, $check_blw = true)
	{
		$result = $this->api_client->call('PrivateMessaging.sendMessage', array($this->type, $participant_type, $this->id, $participant_id, $body, $for_paids_only, $check_blw ));
	}
	
	public function sendMessages($body, $participants)
	{
		foreach($participants as $k => $participant) {
			$participants[$k]['type'] =  $participant['type'] = $participant['type'];
			if ( $participant['type'] == 'escort' ) {
				$participants[$k]['id'] = $participant['escort_id'];
			} else {
				$participants[$k]['id'] = $participant['user_id'];
			}
			unset($participants[$k]['escort_id']);
			unset($participants[$k]['user_id']);
		}
		$result = $this->api_client->call('PrivateMessaging.sendMessages', array($this->type, $this->id, $participants, $body));
		
	}
	
	public function getThreads($page = null, $per_page = null, $escort_id = null)
	{
		$result = $this->api_client->call('PrivateMessaging.getThreads', array($this->type, $this->id, $page, $per_page, $escort_id));
		return $result;
	}
	
	public function getThread($id, $page = null, $per_page = null, $order = 'desc')
	{
		$result = $this->api_client->call('PrivateMessaging.getThread', array($id, $this->type, $this->id, $page, $per_page, $order));
		
		return $result;
	}
	
	public function getThreadParticipant($id)
	{
		$result = $this->api_client->call('PrivateMessaging.getThreadParticipant', array($id, $this->type, $this->id));
		return $result;
	}
	
	public function removeThread($id)
	{
		$this->api_client->call('PrivateMessaging.removeThread', array($id, $this->type, $this->id));
		return true;
	}
	
	public function blockThread($id, $a_escort_id = null)
	{
		$this->api_client->call('PrivateMessaging.blockThread', array($id, $this->type, $this->id, $a_escort_id));
		return true;
	}
	
	public function unblockThread($id, $a_escort_id = null)
	{
		$this->api_client->call('PrivateMessaging.unblockThread', array($id, $this->type, $this->id, $a_escort_id));
		return true;
	}
	
	public function getParticipants($search)
	{
		
		$result = $this->api_client->call('PrivateMessaging.getParticipants', array($search, $this->type, $this->id, 10));
		return $result;
	}
	
	public function removeContact($id, $type)
	{
		$this->api_client->call('PrivateMessaging.removeContact', array($this->id, $this->type, $id, $type));
		return true;
	}
	
	public function addContact($id, $type)
	{
		$this->api_client->call('PrivateMessaging.addContact', array($this->id, $this->type, $id, $type));
		return true;
	}
	
	public function getContacts($page = null, $per_page = null)
	{
		$result = $this->api_client->call('PrivateMessaging.getContacts', array($this->type, $this->id, $page, $per_page));
		return $result;
	}
	
	public function checkIfActiveHasPackage($escort_id)
	{
		$result = $this->api_client->call('PrivateMessaging.checkIfActiveHasPackage', array($escort_id));
		return $result;
	}
	
	public function getUnreadThreadsCount()
	{
		return $this->api_client->call('PrivateMessaging.getUnreadThreadsCount', array($this->type, $this->id));
	}
	
	public function notifyByMail($user_id,$notify)
	{
		return $this->api_client->call('PrivateMessaging.notifyByMail', array($user_id,$notify));
	}
	
	public function notifyByMailText($user_id,$notify)
	{
		return $this->api_client->call('PrivateMessaging.notifyByMailText', array($user_id,$notify));
	}
	
	public function getNotifications($user_id)
	{
		return $this->api_client->call('PrivateMessaging.getNotifications', array($user_id));
	}
	
}
