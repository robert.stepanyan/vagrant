<?php

class Cubix_CGP_Callback_Exception extends Exception {}

class Cubix_CGP_Callback_Exception_TransactionIdRequired extends Cubix_CGP_Callback_Exception {}
class Cubix_CGP_Callback_Exception_RefRequired extends Cubix_CGP_Callback_Exception {}
class Cubix_CGP_Callback_Exception_AmountRequired extends Cubix_CGP_Callback_Exception {}
class Cubix_CGP_Callback_Exception_AmountUnknown extends Cubix_CGP_Callback_Exception {}
class Cubix_CGP_Callback_Exception_CurrencyInvalid extends Cubix_CGP_Callback_Exception {}
class Cubix_CGP_Callback_Exception_StatusRequired extends Cubix_CGP_Callback_Exception {}
class Cubix_CGP_Callback_Exception_StatusInvalid extends Cubix_CGP_Callback_Exception {}
class Cubix_CGP_Callback_Exception_HashInvalid extends Cubix_CGP_Callback_Exception {}

class Cubix_CGP_Callback
{
	private $_params = array();

	private $_statuses = array(
		0 => 'Transaction in progress',
		100 => 'Authorization successful',
		150 => '3D secure status ‘Y’ (yes), waiting for 3D secure authentication',
		152 => '3D secure status ‘N’ (no)',
		154 => '3D secure status ‘U’ (unknown)',
		156 => '3D secure status ‘E’ (error)',
		200 => 'Transaction successful',
		210 => 'Recurring transaction successful',
		300 => 'Transaction failed',
		301 => 'Transaction failed due to anti fraud system',
		310 => 'Recurring transaction failed',
		350 => 'Transaction failed, time out for 3D secure authentication',
		400 => 'Refund to customer',
		410 => 'Chargeback by customer',
		700 => 'Transaction waits for user action'
	);

	public static $success_statuses = array(200, 210);
	public static $recurring_statuses = array(210,310);
	private $_id;

	public function getId()
	{
		return $this->_id;
	}

	/**
	 *
	 * @var Cubix_Api
	 */
	private $_client;

	public function __construct(stdClass $params)
	{
		$this->_params = $params;

		$this->_client = Cubix_Api::getInstance();

		$this->_storeRaw();
	}

	public function get($name, $default = null)
	{
		return isset($this->_params->$name) ? $this->_params->$name : $default;
	}

	public function set($name, $value)
	{
		$this->_params->$name = $value;
	}

	public function isTest()
	{
		return (bool) $this->get('is_test');
	}

	private function getVerifyHash()
	{
		$hash = '';
		if ( $this->isTest() ) {
			$hash .= 'TEST';
		}
		$hash .= $this->get('transaction')->id;
		$hash .= $this->get('currency');
		$hash .= $this->get('amount');
		$hash .= $this->get('ref');
		$hash .= $this->get('status');
		$hash .= Cubix_CGP::getSecurityHash();
		$hash = md5($hash);

		return $hash;
	}

	private function _validate()
	{
		if ( ! $this->get('transaction')->id ) {
			throw new Cubix_CGP_Callback_Exception_TransactionIdRequired();
		}

		if ( ! $this->get('ref') ) {
			throw new Cubix_CGP_Callback_Exception_RefRequired();
		}

		if ( ! $this->get('amount') ) {
			throw new Cubix_CGP_Callback_Exception_AmountRequired();
		}

		if ( ! in_array((int) $this->get('status'), array_keys($this->_statuses)) ) {
			throw new Cubix_CGP_Callback_Exception_StatusUnknown('It is ' . $this->get('status'));
		}

		/*if ( $this->getVerifyHash() != $this->get('hash') ) {
			throw new Cubix_CGP_Callback_Exception_HashInvalid("Must be " . $hash . ' instead of ' . $this->get('hash'));
		}*/

		$this->_notify('validate', array($this));
	}

	private function _storeRaw()
	{
		$data = json_encode($this->_params);

		$db = Zend_Registry::get('db');
		$db->insert('cgp_callback', array(
			'date' => new Zend_Db_Expr('NOW()'),
			'raw' => $data
		));
		$this->_id = $db->lastInsertId();
	}

	public function setError($e)
	{
		$db = Zend_Registry::get('db');

		$db->update('cgp_callback', array(
			'error' => $e
		), $db->quoteInto('id = ?', $this->_id));
	}

	public function handle()
	{
		try {
			$this->_validate();
		}
		catch ( Exception $e ) {
			$this->setError($e->__toString());
			throw $e;
		}

		$db = Zend_Registry::get('db');
		$db->update('cgp_callback', array(
			'transaction_id' => (int) $this->get('transaction')->id,
			'ref' => $this->get('ref'),
			'is_test' => (bool) $this->get('is_test'),
			'status' => (int) $this->get('status'),
			'status_desc' => $this->_statuses[(int) $this->get('status')],
			'amount' => (int) $this->get('amount')
		), $db->quoteInto('id = ?', $this->_id));

		if ( in_array($this->get('status'), self::$success_statuses) ) {
			$this->_notify('success', array($this));
		}
		else {
			$this->_notify('fail', array($this));
		}
	}

	private $_hooks = array();

	public function registerHook($name, $callback)
	{
		if ( ! isset($this->_hooks[$name]) ) {
			$this->_hooks[$name] = array();
		}

		$this->_hooks[$name][] = $callback;
	}

	private function _notify($name, array $args = array())
	{
		if ( isset($this->_hooks[$name]) ) {
			foreach ( $this->_hooks[$name] as $callback ) {
				if ( false === call_user_func_array($callback, $args) ) {
					break;
				}
			}
		}
	}

	static public function getIdByTransactionId($trans_id)
	{
		$db = Zend_Registry::get('db');
		return $db->fetchOne('SELECT id FROM cgp_callback WHERE transaction_id = ?', $trans_id);
	}
}
