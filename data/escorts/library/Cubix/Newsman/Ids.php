<?php

class Cubix_Newsman_Ids
{
	private $_ids = array(
		1 => array(
			78 => array(
				'member' => 116,
				'escort' => 118,
				'agency' => 117
			)
		),
		2 => array(
			79 => array(
				'member' => 122,
				'escort' => 121,
				'agency' => 120
			)
		),
		11 => array(
			97 => array(
				'member' => 219,
				'escort' => 220,
				'agency' => 221
			)
		),
		30 => array(
			107 => array(
				'member' => 269,
				'escort' => 270,
				'agency' => 271
			)
		),
		5 => array(
			109 => array(
				'member' => 261,
				'escort' => 262,
				'agency' => 260
			)
		),
		33 => array(
			98 => array(
				'member' => 265,
				'escort' => 266
			)
		),
        69 => array(
            331 => array(
                'member' => 47597,
                'escort' => 47598,
                'agency' => 57183
            )
        ),
		17 => array(
			147 => array(
				'member' => 536
			)
		),
		16 => array(
			251 => array(
				'member' => 819
			)
		),
		7 => array(
			315 => array(
				'member' => 1129
			)
		)
	);
	
	private $_conf = array(
		'user_id' => 61,
		'api_key' => 'ad6da04df34e56930d1a9d7b5b09aa48'
	);
	
	public function get($app_id)
	{
		return $this->_ids[$app_id];
	}
	
	public function getConf()
	{
		return $this->_conf;
	}
}