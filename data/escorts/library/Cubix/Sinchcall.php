<?php

class Cubix_Sinchcall extends Cubix_Model
{
	private $key;
	private $secret;
	private $url = 'https://verificationapi-v1.sinch.com/verification/v1/verifications';
	
	public function __construct($key,$secret)
	{		
		$this->key = $key;
		$this->secret = $secret;
	}
		
	public function asynccall($phone)
	{

		try{
			$url = $this->url;
			$data = array(
				"type" => 'number',
				"endpoint" => $phone,
			);

			$content = json_encode(array('identity' => $data,'method' => 'flashcall'));

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_USERPWD, $this->key . ':' . $this->secret);

            $headers = array();
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $response = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
                $err = curl_error($ch);
            }
            curl_close ($ch);

			$ret = array();
			
			if ($err) {
				$ret['result'] = "Failed";
				$ret['error'] = $err;
			} else {
				$res = json_decode($response);
				$rc = $res->flashCall->cliFilter;
				if($rc){
					$ret['result'] = "Success";
					$ret['token'] = $rc;
				}else{
					$ret['result'] = "Failed";
					$ret['error'] = $rc;
				}
			}
		}catch(Exception $e){
			$ret['result'] = "Failed";
			$ret['error'] = $e->getMessage();
		}
		
		return $ret;
	}		
}
