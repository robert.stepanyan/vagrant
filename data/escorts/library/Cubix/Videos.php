<?php

class Cubix_Videos {
   
	public $metaData;
    public $fileName;
	public $max_height;
	public $max_width;
    private $escort_id;
    private $VideoMinHeight = 360;
	private $config;
	private $response;
	private static $html5;
	public $dimensions = array(
		"720" => array(
			'height' => 720, 'bitrate' => 1200
		),
		"480" => array(
			'height' => 480, 'bitrate' => 600
		),
		"360" => array(
			'height' => 360, 'bitrate' => 250
		)
	);
	
	public $dimension_videos = array();
		   
	public function  __construct($escort_id, $config, $html5 = false) {
 
		$this->escort_id = $escort_id;
		$this->config = $config;
		self::$html5 = $html5;
		$this->ffmpeg = $this->config['ffmpeg_path'];

        if (Cubix_Application::getId() == APP_A6) {
            $this->dimensions['240'] = array(
                'height' => 240, 'bitrate' => 200
            );
            $this->VideoMinHeight = 240;
        }

		
		$this->metaData = array(
			"duration"=>null,
			"size"=>null,
			"framerate"=>null,
			"width"=>null,
			"height"=>null,
			"videodatarate"=>null,
			"audiodatarate"=>null,
			"audiodelay"=>null,
			"audiosamplesize"=>null,
			"audiosamplerate"=>null,
			"audiocodecid"=>null,
			"videocodecid"=>null,
			"version"=>null,
			"headersize"=>0
        );
		
		$this->response = $this->startUpload();
		
	}

	private function _tempPathForEscort($escort_id) {
		$path = array(
			isset($this->config['temp_dir_path']) ? $this->config['temp_dir_path'] : sys_get_temp_dir(),
			Cubix_Application::getById(Cubix_Application::getId())->host,
			'escort',
			$this->escort_id,
			uniqid()
		);

		$path = implode('/', $path);

		if (!is_dir($path)) {
			@mkdir($path, 0755, true);
		}	

		return $path;
	}
  
	public function getResponse()
	{
		return $this->response;
	}
	
	public function getVideoInfo()
	{

		if(!isset($this->config) || !is_array($this->config))
				return FALSE;
		
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		if(file_exists($this->ffmpeg))
		{			
			exec("$this->ffmpeg -i ".$this->fileName." 2>&1", $output);
			$video_info = implode('', $output);
			if((strpos($video_info, 'bitrate:')===false) || (strpos($video_info, 'Video:')===false) || (strpos($video_info, 'Duration:')===false)){
				//return Cubix_I18n::translate('sys_error_upload_video_allowed_extensions',array('formats' => implode(', ', $this->config['allowedvideoExts'])));
				//return $this->fileName;
				//return $video_info;
				return '(unexpected error occurred please try later)';
			}
			
			$this->videoData['size'] = number_format((filesize($this->fileName)/pow(1024,2)),2);
			preg_match('/bitrate: ([0-9]+)/', $video_info, $match);
			$this->videoData['bitrate'] = $match[1];
			preg_match('/Stream #(?:[0-9\.]+)(?:.*)\: Video: (?P<videocodec>.*) (?P<width>[0-9]{3,})x(?P<height>[0-9]{3,})/', $video_info, $match_size);
			$this->videoData['height'] = $match_size['height'];
			$this->videoData['width'] =  $match_size['width'];
			preg_match("/Duration: ([\d]{2}):([\d]{2}):([\d]{2}).([\d]{1,2}), start/si", $video_info, $match);
			$this->videoData['duration'] = $match[1] * 3600 + $match[2] * 60 + $match[3];
			preg_match("/rotate[\s]+: (?P<rotate>(0|90|180|270))/", $video_info, $match);
			$this->videoData['rotate'] = $match['rotate'];
			
			if(intval($this->videoData['height']) < $this->VideoMinHeight ){
				return Cubix_I18n::translate('sys_error_upload_video_allowed_size',array('size' => $this->VideoMinHeight));
			}
			
			if( in_array( $this->videoData['rotate'] , array(90, 270 ))){
				$this->videoData['height'] = $match_size['width'];
				$this->videoData['width'] =  $match_size['height'];
			}
			
			
			return 0;
		}
		return 0;
	}

	private function validate($file_data)
	{
		$ext = strtolower(pathinfo($file_data['name'],PATHINFO_EXTENSION));
		
		if(!in_array($ext,$this->config['allowedvideoExts']))
		{
			return Cubix_I18n::translate('sys_error_upload_video_allowed_extensions',array('formats' => implode(', ',$this->config['allowedvideoExts'])));
		}
		elseif($file_data['full_size'] > self::_convert_size($this->config['VideoMaxSize'])){
			return Cubix_I18n::translate('sys_error_upload_video_allowed_max_size',array('size' => $this->config['VideoMaxSize']));
		}
		else{
			return true;
		}
	}
	
	public function SaveImage($ajax=FALSE)
	{	
		
		try{
			$dir = $this->_tempPathForEscort($this->escort_id) . '/';
			
			$img_count = isset($this->config['ImageCount']) && is_numeric($this->config['ImageCount']) ? $this->config['ImageCount'] : 6;

			$interval = $this->videoData['duration'] / $img_count;
			$sum = 0;
			$size = $this->max_width.'x'.$this->max_height;
									
			for($i = 0; $sum < $this->videoData['duration'] - $interval; $i++)
			{	
				$sum = round($interval * $i);
				exec("$this->ffmpeg -i $this->fileName -deinterlace -an -ss $sum -f mjpeg -t 1 -r 1 -y -s $size $dir".uniqid().".jpg "); 
			}	
			$images = new Cubix_ImagesCommon();
			$dir_handle = opendir($dir) or die("Unable to open $dir"); 
			$photo = array();	
			while ($filename = readdir($dir_handle))  
			{
				if(file_exists($dir.$filename) && $filename!='.' && $filename!='..' && getimagesize($dir.$filename))
				{	
					$img['tmp_name'] = $dir.$filename;
					$img['name'] = $filename;
					$photo[] = $images->save($img,$ajax);				
				}

			}
			/*$photo['width'] = $this->videoData['width'];
			$photo['height'] = $this->videoData['height'];*/
			
			
		}
		catch (Exception $e)
		{
			return $e->getMessage();
			//return 'sys_error_upload_video';
		}
		
		return $photo;			
		
	}
	
	public function SaveFtpVideoBackend()
	{	
		$hash = uniqid();
		$name = $hash.'_'.$this->max_height.'p.mp4';
		$ftp = new Cubix_VideosCommon();
		if($ftp->_storeToPicVideo($this->fileName, $this->escort_id, $name)){
			return $hash;
		}
		else{
			return false;
		}
	}
	
	public function SaveVideoImageFtpFrontend(array $image,$client,$ext='mp4')
	{
		if(!empty($image))
		{	
			
			$hash = uniqid();
			$name = $hash.'_'.$this->max_height.'p.'.$ext;
			$ftp = new Cubix_VideosCommon();	
			
			if($ftp->_storeToPicVideo($this->fileName, $this->escort_id,$name))
			{
				$data = array(
					'escort_id' => $this->escort_id,
					'video' => $hash,
					'height' => $this->max_height,
					'width' => $this->max_width
				);
				$video_id = $client->call('Application.saveVideoV2',array($data));

				$count = count($image);
				$video_img = array();
				for( $i = 0; $i < $count; $i++)
				{	
					if($image[$i]['image_id'] != 0)
					{	
						$video_img['video_id'] = $video_id;
						$video_img['image_id'] = $image[$i]['image_id'];
						$client->call('Application.saveVideoImageV2',array($video_img));
					}
				}
		
				return array('id' => $video_id, 'name' => $hash);
			}
		}
			return false;
	}
		
	public static function CheckVideoStatus($video_count,$max_video, $frontend = false)
	{	
		$video_count[0] = (array)$video_count[0];
		$video_count[1] = (array)$video_count[1];
		$pending = $approve = array('quantitiy'=>0);
		switch($video_count[0]['status'])
		{
			case 'pending':$pending = $video_count[0];
									break;
			case 'approve':$approve = $video_count[0];
									break;
		}
		if(isset($video_count[1]))
		{	
			switch($video_count[1]['status'])
			{
				case 'pending':$pending = $video_count[1];
										break;
				case 'approve':$approve = $video_count[1];
										break;
			}

		}
		
		if($frontend && $approve['quantitiy'] >= $max_video)
			return  Cubix_I18n::translate('sys_error_upload_video_pending',array('count' => $max_video));	
		if( ($pending['quantitiy'] == ($max_video - 1) && $approve['quantitiy'] == 1) || 
			($approve['quantitiy'] == ($max_video - 1) && $pending['quantitiy'] == 1) || 
			($pending['quantitiy'] >= $max_video) || ($approve['quantitiy'] >= $max_video) )
			if($frontend)
				return Cubix_I18n::translate('sys_error_upload_video_pending',array('count' => $max_video)).' '.Cubix_I18n::translate('wait_while_other_videos_checked');
			else
				return	Cubix_I18n::translate('sys_error_upload_video_pending',array('count' => $max_video )).' first check other videos.';
		else
		return false;
	}
	
	public function ConvertToFlv()
	{
        $pathInfo  = pathinfo($this->fileName);
        $path = $pathInfo['dirname'].DIRECTORY_SEPARATOR.$pathInfo['filename'];
		$ffmpeg = $this->config['ffmpeg_path'];
		exec("$ffmpeg -i $this->fileName -ar 22050 -ab 32k -f flv -b 800k  -y  $path.flv");
		$this->fileName = $path.'.flv';
		return $this->fileName;
	}
	
	public function ConvertToMP4()
	{
        $pathInfo  = pathinfo($this->fileName);
        $path = $pathInfo['dirname'].DIRECTORY_SEPARATOR.$pathInfo['filename'];
		$ffmpeg = $this->config['ffmpeg_path'];
		
		foreach($this->dimensions as $dimension){
			if( $this->videoData['height'] < $dimension['height']){
				CONTINUE;
			}
			else{
				$this->max_height = $dimension['height'];
				//Odd number for width is not alloyable  
				$this->max_width = intval(($dimension['height'] * $this->videoData['width']) / ( $this->videoData['height'] * 2 ) ) * 2;
				$bitrate = $this->videoData['bitrate'] < $dimension['bitrate'] ? $this->videoData['bitrate'] : $dimension['bitrate'] ;
				// When *.mp4 file uploaded ffmpeg unable to convert video with same name
				//so output file should be renamed
				if(Cubix_Application::getId() == APP_EF){
					exec("$ffmpeg -i $this->fileName -s ".$this->max_width."x".$this->max_height." -vcodec libx264 -acodec libfdk_aac -b ".$bitrate."k -ab 128k {$path}_out.mp4");
				}
				elseif(in_array(Cubix_Application::getId(),array(APP_BL, APP_EG_CO_UK, APP_ED))){
					exec("$ffmpeg -i $this->fileName -s ".$this->max_width."x".$this->max_height." -vcodec libx264 -acodec libmp3lame -b:v ".$bitrate."k -b:a 128k {$path}_out.mp4");
					
				}elseif(Cubix_Application::getId() == APP_A6){
                    exec("$ffmpeg -i $this->fileName -s ".$this->max_width."x".$this->max_height." -vcodec libx264 -acodec libfdk_aac -b:v ".$bitrate."k -b:a 128k {$path}_out.mp4");
                }
				else{
					exec("$ffmpeg -i $this->fileName -s ".$this->max_width."x".$this->max_height." -threads 0 -vcodec libx264 -acodec libfaac -b ".$bitrate."k -ab 128k {$path}_out.mp4");
				}
				BREAK;
			}
		}
		$this->fileName = $path.'_out.mp4';
		return $this->fileName;
	}
	
	public function rotate($side)
	{
		$ffmpeg = $this->config['ffmpeg_path'];
		$new_filename = str_replace('.flv','_r.flv',$this->fileName );
		exec("$ffmpeg -y -i ".$this->fileName."  -ab 128k -ac 2 -vcodec flv1 -threads 0 -qscale:v 6 -vf 'transpose=".$side."' ". $new_filename);
		
		/*ob_start();
			//passthru("$ffmpeg -i ".$this->fileName." -qscale:v 5 -vf 'transpose=1' -ar 44100 -ab 96k -y ". $new_filename ." 2>&1");
			passthru("$ffmpeg -y -i ".$this->fileName." -c:v libx264  -crf 15  -c:a libmp3lame -b:a 192k -ac 2 ". $new_filename ." 2>&1");
		    //passthru("$ffmpeg -y -i ".$this->fileName." -vcodec libx264 -level 21 -refs 2 -b 500k -threads 0 -s 640x360 -frame_size 160 ". $new_filename ." 2>&1");
		$video_info = ob_get_contents();
		ob_end_clean();
		var_dump("$ffmpeg -y -i ".$this->fileName." -c:v libx264  -crf 15  -c:a libmp3lame -b:a 192k -ac 2 ". $new_filename ." 2>&1");
		var_dump($video_info);die;*/	
		$this->fileName = $new_filename;
		return $this->fileName;
	}
	
	/**
	 *
	 * Detect if upload method is HTML5
	 * 	 
	 * @return	boolean
	 * 	 	  	 
	 */
	public static function is_HTML5_upload()
	{
		if( self::$html5 ) return true;
		return empty($_FILES);
	}
	
	
	/**
	 *
	 * Upload a file using HTML4 or Flash method
	 * 
	 * @param		boolean	Return response to the script	 
	 * @return	array		Response
	 */
	public function HTML4_upload()
	{
	
		// Upload file using traditional method	
		$response = array();
		$container_index = '_tbxFile';
		foreach ($_FILES as $k => $file)
		{
			$response['key']         = (int)substr($k, strpos($k, $container_index) + strlen($container_index));
			$response['name']        = basename($file['name']);	// Basename for security issues
			$response['error']       = $file['error'];
			$response['size']        = $file['size'];
			$response['full_size']   = $file['size'];
			$response['upload_name'] = $file['name'];
			$response['finish']      = FALSE;

			// Detect upload errors
			$valid = $this->validate($response);
			if($valid !== true){
				$response['error'] = $valid;
			}
			elseif($response['error'] == 0){
				$this->fileName = $file['tmp_name'];
				$response['finish'] = TRUE;
			}
		}   
		
	    if($response['error'] == 0 && $response['finish']){
			$response['error'] = $this->getVideoInfo();
		}
				
	 	return $response;	
	}
	
	
	/**
	 * Upload a file using HTML5
	 * 
	 * @param		boolean	Return response to the script	 
	 * @return	array		Response
	 */
	public function HTML5_upload()
	{
		// Read headers
		$response = array();
		$headers 	= self::_read_headers();
		
		$response['id']    	= $headers['X-File-Id'];
		$response['name']  	= basename($headers['X-File-Name']); 	// Basename for security issues
		$response['size']  	= $headers['Content-Length'];
		$response['full_size']  	= $headers['X-File-Size'];
		$response['error'] 	= UPLOAD_ERR_OK; 
		$response['finish'] = FALSE;
	    
		// Detect upload errors
		$valid = $this->validate($response);
		
		if($valid !== true){
			$response['error'] = $valid;
		}
		else{
			
			// Firefox 4 sometimes sends a empty packet as last packet
				/*	       
			else if ($headers['Content-Length'] == 0)
				$response['error'] = UPLOAD_ERR_NO_FILE;
			*/	    	  
			// Is resume?	  
			$flag = (bool)$headers['X-File-Resume'] ? FILE_APPEND : 0;
			$response['name'] = preg_replace('#[^a-zA-Z0-9_.]#', '_', $response['name']);
			$temp_dir = isset($this->config['temp_dir_path']) ? $this->config['temp_dir_path'] : sys_get_temp_dir();
			$filename = $temp_dir . DIRECTORY_SEPARATOR . $response['id'] . '_' . $response['name'];
			$this->fileName = $filename;
			//$response['full_name'] = $filename;
			// Write file
			//$context = stream_context_create( array( 'http'=>array() ) );
			if( self::$html5 ){
				$_content = $this->realContentForMobile( file_get_contents('php://input') );
			} else {
				$_content = file_get_contents('php://input');
			}

			if (file_put_contents($filename, $_content, $flag) === FALSE){
				$response['error'] = UPLOAD_ERR_CANT_WRITE;
			}
			else
			{
				$response['sizes'] = array( filesize($filename), $headers['X-File-Size'] );
				// $response['temp_dir'] = $this->realContentForMobile( file_get_contents('php://input') );
				if (filesize($filename) == $headers['X-File-Size']){
					$response['finish'] = TRUE;
				}
			} 
		}

		if($response['error'] == 0 && $response['finish']){
			$response['error'] = $this->getVideoInfo();
		}
	  
	  return $response;
	}

	public function realContentForMobile( $string, $first_removing_lines = 4, $last_removing_line = 2 ){
		$bits = explode("\r\n", $string);

		for( $i = 0; $i < $first_removing_lines; $i += 1 ){
			array_shift($bits);
		}

		for( $i = 0; $i < $last_removing_line; $i += 1 ){
			array_pop($bits);
		}

		return implode( "\r\n", $bits );
//		$string =  implode("\r\n", array_slice(explode("\r\n", $string), $first_removing_lines));
//		$string = join("\r\n", array_slice(explode("\r\n", $string ), 0, -1));
//		return rtrim( join("\r\n", array_slice(explode("\r\n", $string ), 0, -1)), "\r" );
		//return ltrim( join("\n", array_slice(explode("\n", $string), 0, -1)), "\r" );
		//return trim( trim( join("\n", array_slice(explode("\n", $string), 0, -1)), "\n" ), "\r" );
	}
	
	
	/**
	 *	
	 * Detect the upload method and process the files uploaded
	 * 
	 * @param		string	Directory destination path	 	 	 	 	 
	 * @param		boolean	Return response to the script	 
	 * @return	array		Response
	 * 
	 */
	public function startUpload()
	{			
		return self::is_HTML5_upload() ? $this->HTML5_upload() : $this->HTML4_upload();		
	}	 		 	 	
	
	public static function showResponse($response)
	{
		// If is HTML5
		if(isset($response['id'])){
			header('Cache-Control: no-cache, must-revalidate');
			header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
			header('Content-type: application/json');
		}
		echo json_encode($response);
	}		
	
	/**
	 *
	 * Convert to bytes a information scale
	 * 
	 * @param		string	Information scale
	 * @return	integer	Size in bytes	 
	 *
	 */	 	 	 	 
	public static function _convert_size($val)
	{
		$val = trim($val);
	  $last = strtolower($val[strlen($val) - 2]);
	  
	  switch ($last) {
	    case 'g': $val *= 1024;
	 
	    case 'm': $val *= 1024;
	 
	    case 'k': $val *= 1024;
	  }
	 
	  return $val;
	}	
	
	
	private static function _read_headers()
	{
	
		// GetAllHeaders doesn't work with PHP-CGI
		if (function_exists('getallheaders')) 
		{
			$headers = getallheaders();
		}
		else 
		{
			$headers = array();
			$headers['Content-Length'] 	= $_SERVER['CONTENT_LENGTH'];
			$headers['X-File-Id'] 			= $_SERVER['HTTP_X_FILE_ID'];
			$headers['X-File-Name'] 		= $_SERVER['HTTP_X_FILE_NAME'];			
			$headers['X-File-Resume'] 	= $_SERVER['HTTP_X_FILE_RESUME'];
			$headers['X-File-Size'] 		= $_SERVER['HTTP_X_FILE_SIZE'];
		}

		return $headers;
		
	}
	
	
}