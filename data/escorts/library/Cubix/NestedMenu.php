<?php

class Cubix_NestedMenu
{
	/**
	 * @var Cubix_NestedMenu_Item
	 */
	protected $_root = array();
	
	/**
	 * @var Cubix_NestedMenu_Item
	 */
	protected $_selected;
	
	protected $_id = null;
	
	public function __construct(array $config = array())
	{
		$this->setStructure($config);
	}

	public function setStructure(array $structure)
	{
		$this->_root = $this->_process($structure);
	}

	protected function _process(array $item)
	{
		$item = new Cubix_NestedMenu_Item($item);
		
		if ( isset($item->childs) && is_array($item->childs) && count($item->childs) ) {
			$childs = array();
			
			foreach ( $item->childs as $child ) {
				$i = $this->_process($child);
				
				if ( isset($item->title) ) {
					$i->parent = $item->title;
				}
				
				$childs[] = $i;
			}
			
			$item->childs = $childs;
		}
		
		return $item;
	}
	
	public function setId($id)
	{
		$this->_id = $id;
	}
	
	public function getId()
	{
		return $this->_id;
	}
	
	public function setSelected(Cubix_NestedMenu_Item $item)
	{
		$this->_selected = $item;
	}
	
	public function getSelected()
	{
		return $this->_selected;
	}
	
	public function getByValue($value)
	{
		return $this->_getByValue($this->_root->childs, $value);
	}
	
	protected function _getByValue($items, $value)
	{
		foreach ( $items as $item ) {
			if ( isset($item->childs) && is_array($item->childs) && count($item->childs) ) {
				$result = $this->_getByValue($item->childs, $value);
				
				if ( ! is_null($result) ) {
					return $result;
				}
			}
			
			if ($value == $item->value) {
				return $item;
			}
		}
		
		return NULL;
	}
	
	public function render($decorator)
	{
		$html = '<li>';
		$html .= call_user_func($decorator, $this->_selected, TRUE);
		$html .= '<ul' . ( ! is_null($this->_id) ? ' id="' . $this->getId() . '"' : '' ) . '>';
		
		foreach ( $this->_root->childs as $item ) {
			$html .= $this->_render($item, $decorator);
		}
		
		$html .= '</ul>';
		$html .= '</li>';
		
		return $html;
	}
	
	protected function _render(Cubix_NestedMenu_Item $item, $decorator)
	{
		$hidden = FALSE;
		if ( ! empty($this->_selected) && $item->isEqual($this->_selected) )
			$hidden = TRUE;
		
		$html = '<li>';
		$html .= call_user_func($decorator, $item, FALSE, $hidden);
		
		if ( isset($item->childs) && is_array($item->childs) && count($item->childs) ) {
			$html .= '<ul>';
			foreach ( $item->childs as $child ) {
				$html .= $this->_render($child, $decorator);
			}
			$html .= '</ul>';
		}
		
		$html .= '</li>';
		
		return $html;
	}
}
