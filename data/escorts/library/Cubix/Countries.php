<?php

class Cubix_Countries extends Cubix_Model
{
	protected $_table = 'countries';
	protected $_itemClass = 'Cubix_CountryItem';

	public function getAll()
	{
		return $this->_fetchAll('SELECT id, ' . Cubix_I18n::getTblField('title') . ' FROM countries ORDER BY ' . Cubix_I18n::getTblField('title') . ' ASC');
	}

	public function get($id)
	{
		return $this->_fetchOne('SELECT id, title FROM countries WHERE id = ?', $id);
	}

	/* blacklist country checker */
	private static function _blacklistCountryId()
	{
		
		$country_table = "countries";
		if ( Cubix_Application::getId() == APP_BL || Cubix_Application::getId() == APP_A6 ) {
			$country_table = "countries_all";
		}

		$db = Zend_Registry::get('db');
		if(APPLICATION_ENV == 'development'){
			$country_iso = 'am';
		}
		elseif ( Cubix_Application::getId() == APP_EF ) {
//			$system = Zend_Registry::get('system_config');
			$geo_ip = Cubix_IP2Location::getInstance();
			$records = $geo_ip->lookup(Cubix_Geoip::getIP(), array(Cubix_IP2Location::COUNTRY_CODE));
			$country_iso = strtolower($records["countryCode"]);
			if($_GET['detect_iso'] == 1){
				var_dump($country_iso);die;
			}
		}
		else{
			$geoData = Cubix_Geoip::getClientLocation();
			// if( $_GET['test'] == 1 ){ print_r( $geoData ); die; }
			if (!$geoData) return null;
			if (!isset($geoData['country_iso']) || !$geoData['country_iso']) return null;
			$country_iso = $geoData['country_iso'];
		}
		$geo_id = $db->fetchOne('SELECT id FROM ' . $country_table . ' WHERE iso = ?', $country_iso);

		if (!$geo_id) return null;

		return $geo_id;
	}

	public static function blacklistCountryWhereCase()
	{
		$country_table = "countries";
		if ( Cubix_Application::getId() == APP_BL || Cubix_Application::getId() == APP_A6 ) {
			$country_table = "countries_all";
		}

		$db = Zend_Registry::get('db');

		$geo_id = self::_blacklistCountryId();

		if (!$geo_id) return '';

		$countries_count = $db->fetchOne('SELECT COUNT(*) FROM ' . $country_table);

		$fields_count = ceil($countries_count / 62);
		$w = array();

		for ($i = 1; $i <= $fields_count; $i++)
		{
			$w[] = 'FIND_IN_SET(' . $geo_id .', e.blacklisted_countries_' . $i . ') = 0';
		}

		$ws = implode(' AND ', $w);

		$where = ' AND ' . $ws . ' ';

		return $where;
	}

	public static function blacklistCountryCacheKeyCase()
	{
		$geo_id = self::_blacklistCountryId();

		if (!$geo_id) return '';

		$key = '_blacklist_country_' . $geo_id;

		return $key;
	}
}
