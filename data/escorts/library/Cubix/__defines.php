<?php

define('ADMIN_TYPE_NORMAL',                 1);
define('ADMIN_TYPE_PAYMENT_MANAGER',        2);
define('ADMIN_TYPE_DATA_ENTRY',             3);
define('ADMIN_TYPE_SUPERUSER',              99); // superuser can be added only directly in db

$DEFINITIONS['ADMIN_TYPE'] = array(
    ADMIN_TYPE_NORMAL           => 'admin',
    ADMIN_TYPE_PAYMENT_MANAGER  => 'payment manager',
    ADMIN_TYPE_DATA_ENTRY       => 'data entry',
    ADMIN_TYPE_SUPERUSER		=> 'superadmin'
);

define('USER_STATUS_ACTIVE',                1);
define('USER_STATUS_NOT_VERIFIED',          -1);
define('USER_STATUS_DISABLED',              -4);

$DEFINITIONS['user_status_options'] = array(
    USER_STATUS_ACTIVE             => Cubix_I18n::translate('status_active'),
    USER_STATUS_DISABLED           => Cubix_I18n::translate('status_disabled'),
    USER_STATUS_NOT_VERIFIED       => Cubix_I18n::translate('status_not_verified')
);

define('STATUS_ACTIVE',                     1);
define('STATUS_NO_PHOTOS',                  -1);
define('STATUS_NO_PROFILE',                 -2);
define('STATUS_OWNER_DISABLED',             -3);
define('STATUS_ADMIN_DISABLED',             -4);
define('STATUS_WAITING_APPROVAL',           -5);
define('STATUS_NO_WORK_LOCATIONS',          -6);
define('STATUS_NO_LANGUAGES',               -7);
define('STATUS_NO_MAIN_PIC',                -8);
define('STATUS_EXPIRED',                    -9);
define('STATUS_DOMAIN_BLACKLISTED',         -10); // disabled because domain is blacklisted
define('STATUS_DELETED',                    -99);

$DEFINITIONS['status_options'] = array(
    STATUS_ACTIVE                   => Cubix_I18n::translate('status_active'),
    STATUS_NO_PHOTOS                => Cubix_I18n::translate('status_no_photos'),
    STATUS_NO_PROFILE               => Cubix_I18n::translate('status_no_profile'),
    STATUS_OWNER_DISABLED           => Cubix_I18n::translate('status_owner_disabled'),
    STATUS_ADMIN_DISABLED           => Cubix_I18n::translate('status_admin_disabled'),
    STATUS_WAITING_APPROVAL         => Cubix_I18n::translate('status_not_approved'),
    STATUS_NO_WORK_LOCATIONS        => Cubix_I18n::translate('status_no_working_locations'),
    STATUS_NO_LANGUAGES             => Cubix_I18n::translate('status_no_languages'),
    STATUS_NO_MAIN_PIC              => Cubix_I18n::translate('status_no_main_photo'),
    STATUS_EXPIRED                  => Cubix_I18n::translate('status_expired'),
    STATUS_DOMAIN_BLACKLISTED       => Cubix_I18n::translate('status_domain_blacklisted'),
    STATUS_DELETED                  => Cubix_I18n::translate('status_deleted')
);

define('ESCORT_NOT_VERIFIED',               1);
define('ESCORT_VERIFIED',                   2);
define('ESCORT_REQUESTED_REMOVE',           3);
define('ESCORT_CALLED_1',                   4);
define('ESCORT_CALLED_2',                   5);
define('ESCORT_CALLED_3',                   6);
define('ESCORT_INVALID_NUMBER',             7);
define('ESCORT_REQUESTED_REMOVE_WAVED',     8);
define('ESCORT_SELF_REGISTERED',            9);

$DEFINITIONS['ESCORT_VERIFICATION'] = array(
    ESCORT_NOT_VERIFIED             => 'not verified',
    ESCORT_VERIFIED                 => 'verified',
    ESCORT_REQUESTED_REMOVE         => 'requested remove',
    ESCORT_REQUESTED_REMOVE_WAVED   => 'requested remove, waved because new ads',
    ESCORT_CALLED_1                 => 'called 1 time',
    ESCORT_CALLED_2                 => 'called 2 times',
    ESCORT_CALLED_3                 => 'called 3 times',
    ESCORT_INVALID_NUMBER           => 'invalid number',
    ESCORT_SELF_REGISTERED          => 'self registered'
);

// colors
define('COLOR_BLACK',                       1);
define('COLOR_WHITE',                       2);
define('COLOR_RED',                         3);
define('COLOR_BLOND',                       4);
define('COLOR_BROWN',                       5);
define('COLOR_GREEN',                       6);
define('COLOR_YELLOW',                      7);
define('COLOR_BLUE',                        8);
define('COLOR_GRAY',                        9);
define('COLOR_BRUNETTE',                    10);
define('COLOR_HAZEL',                       11);

$DEFINITIONS['hair_color_options'] = array(
	COLOR_BLACK => Cubix_I18n::translate('color_black'),
	COLOR_BLOND => Cubix_I18n::translate('color_blond'),
	COLOR_BROWN => Cubix_I18n::translate('color_brown'),
	COLOR_RED => Cubix_I18n::translate('color_red')
);

define('YES',                               1);
define('NO',                                2);
define('NOT_AVAILABLE',                     98);
define('DONT_KNOW',                         99);

$DEFINITIONS['yes_no_options'] = array(
   YES                              => Cubix_I18n::translate('answer_yes'),
   NO                               => Cubix_I18n::translate('answer_no')
);

$DEFINITIONS['y_n_na_options'] = array(
   YES                              => Cubix_I18n::translate('answer_yes'),
   NO                               => Cubix_I18n::translate('answer_no'),
   NOT_AVAILABLE                    => Cubix_I18n::translate('answer_not_available')
);

$DEFINITIONS['eye_color_options'] = array(
	COLOR_BLACK => Cubix_I18n::translate('color_black'),
	COLOR_BROWN => Cubix_I18n::translate('color_brown'),
	COLOR_GREEN => Cubix_I18n::translate('color_green'),
	COLOR_BLUE => Cubix_I18n::translate('color_blue'),
	COLOR_GRAY => Cubix_I18n::translate('color_gray')
);

$DEFINITIONS['breast_size_options'] = array('A', 'B', 'C', 'D', 'DD');

// user type
define('USER_TYPE_SINGLE_GIRL',             1);
define('USER_TYPE_AGENCY',                  2);
define('USER_TYPE_MEMBER',                  3);
define('USER_TYPE_CLUB',                    4);

// gender
define('GENDER_FEMALE',                     1);
define('GENDER_MALE',                       2);
define('GENDER_TRANS',                      3);
define('GENDER_DOMINA',                     4);

$DEFINITIONS['gender_options'] = array(
    GENDER_FEMALE                   => Cubix_I18n::translate('gender_female'),
    GENDER_MALE                     => Cubix_I18n::translate('gender_male'),
    GENDER_TRANS                    => Cubix_I18n::translate('gender_trans'),
    GENDER_DOMINA                   => Cubix_I18n::translate('gender_domina')
);

define('KISSING_NO',                        11);
define('KISSING',                           12);
define('KISSING_WITH_TONGUE',               13);

define('BLOWJOB_NO',                        11);
define('BLOWJOB_WITH_CONDOM',               12);
define('BLOWJOB_WITHOUT_CONDOM',            13);

define('CUMSHOT_NO',                        11);
define('CUMSHOT_IN_MOUTH_SPIT',             12);
define('CUMSHOT_IN_MOUTH_SWALLOW',          13);
define('CUMSHOT_IN_FACE',                   14);

// metric system
define('METRIC_SYSTEM',                     1);
define('ROYAL_SYSTEM',                      2);


$DEFINITIONS['metric_system'] = array(
    METRIC_SYSTEM               => Cubix_I18n::translate('measure_metric'),
    ROYAL_SYSTEM                => Cubix_I18n::translate('measure_royal')
);

$DEFINITIONS['kissing_options'] = array(
    KISSING_NO                      => Cubix_I18n::translate('kissing_no_kissing'),
    KISSING                         => Cubix_I18n::translate('kissing_yes'),
    KISSING_WITH_TONGUE             => Cubix_I18n::translate('kissing_with_tongue')
);

$DEFINITIONS['blowjob_options'] = array(
    BLOWJOB_NO                      => Cubix_I18n::translate('blowjob_no_blowjob'),
    BLOWJOB_WITH_CONDOM             => Cubix_I18n::translate('blowjob_with_condom'),
    BLOWJOB_WITHOUT_CONDOM          => Cubix_I18n::translate('blowjob_without_condom')
);

$DEFINITIONS['cumshot_options'] = array(
    CUMSHOT_NO                      => Cubix_I18n::translate('cumshot_no_cumshot'),
    CUMSHOT_IN_MOUTH_SPIT           => Cubix_I18n::translate('cumshot_in_mouth_spit'),
    CUMSHOT_IN_MOUTH_SWALLOW        => Cubix_I18n::translate('cumshot_in_mouth_swallow'),
    CUMSHOT_IN_FACE                 => Cubix_I18n::translate('cumshot_in_face')
);

// time units
define('TIME_MINUTES',                      1);
define('TIME_HOURS',                        2);
define('TIME_DAYS',                         3);
define('TIME_OVERNIGHT',                    4);

$DEFINITIONS['time_unit_options'] = array(
    TIME_MINUTES                    => Cubix_I18n::translate('time_minutes'),
    TIME_HOURS                      => Cubix_I18n::translate('time_hours'),
    TIME_DAYS                       => Cubix_I18n::translate('time_days'),
    TIME_OVERNIGHT                  => Cubix_I18n::translate('time_overnight')
);

// ethnic
define('ETHNIC_WHITE',                          1);
define('ETHNIC_BLACK',                          2);
define('ETHNIC_ASIAN',                          3);
define('ETHNIC_LATIN',                          4);
define('ETHNIC_AFRICAN',                        5);
define('ETHNIC_INDIAN',                         6);

$DEFINITIONS['ethnic_options'] = array(
    ETHNIC_WHITE                    => Cubix_I18n::translate('ethnic_white'),
    ETHNIC_BLACK                    => Cubix_I18n::translate('ethnic_black'),
    ETHNIC_ASIAN                    => Cubix_I18n::translate('ethnic_asian'),
    ETHNIC_LATIN                    => Cubix_I18n::translate('ethnic_latin'),
    ETHNIC_AFRICAN                  => Cubix_I18n::translate('ethnic_african'),
    ETHNIC_INDIAN                   => Cubix_I18n::translate('ethnic_indian')
);

define('AVAILABLE_INCALL',                  1);
define('AVAILABLE_OUTCALL',                 2);
define('AVAILABLE_INCALL_OUTCALL',          3);
define('AVAILABLE_SAUNA',                   4);
define('AVAILABLE_SALON',                   5);
define('AVAILABLE_APARTMENT',               6);
define('AVAILABLE_LOFT',                    7);

$DEFINITIONS['available_for']               = array(
	AVAILABLE_APARTMENT 			=> Cubix_I18n::translate('available_apartment'),
	AVAILABLE_INCALL 				=> Cubix_I18n::translate('available_incall'),
	AVAILABLE_OUTCALL 				=> Cubix_I18n::translate('available_outcall'),
	AVAILABLE_INCALL_OUTCALL 		=> Cubix_I18n::translate('available_incall_outcall'),
	AVAILABLE_SAUNA 				=> Cubix_I18n::translate('available_sauna'),
	AVAILABLE_SALON 				=> Cubix_I18n::translate('available_salon')
);

define('SEXUALLY_AVAILABLE_FOR_MEN', 'men');
define('SEXUALLY_AVAILABLE_FOR_WOMEN', 'women');
define('SEXUALLY_AVAILABLE_FOR_COUPLES', 'couples');
define('SEXUALLY_AVAILABLE_FOR_TRANS', 'trans');
define('SEXUALLY_AVAILABLE_FOR_GAYS', 'gays');
define('SEXUALLY_AVAILABLE_FOR_PLUS2', 'plus2');

$DEFINITIONS['sex_availability_options'] = array(
	SEXUALLY_AVAILABLE_FOR_MEN => Cubix_I18n::translate('sexually_men'),
	SEXUALLY_AVAILABLE_FOR_WOMEN => Cubix_I18n::translate('sexually_women'),
	SEXUALLY_AVAILABLE_FOR_COUPLES => Cubix_I18n::translate('sexually_couples'),
	SEXUALLY_AVAILABLE_FOR_TRANS => Cubix_I18n::translate('sexually_trans'),
	SEXUALLY_AVAILABLE_FOR_GAYS => Cubix_I18n::translate('sexually_gays'),
	SEXUALLY_AVAILABLE_FOR_PLUS2 => Cubix_I18n::translate('sexually_plus2'),
);

$DEFINITIONS['language_options'] = array(
	'en' => Cubix_I18n::translate('lng_en'),
	'fr' => Cubix_I18n::translate('lng_fr'),
	'de' => Cubix_I18n::translate('lng_de'),
	'it' => Cubix_I18n::translate('lng_it'),
	'pt' => Cubix_I18n::translate('lng_pt'),
	'ru' => Cubix_I18n::translate('lng_ru'),
	'es' => Cubix_I18n::translate('lng_es'),
	'gr' => Cubix_I18n::translate('lng_gr')
);

$DEFINITIONS['language_level_options'] = array(
	1 => '1 (' . Cubix_I18n::translate('level_basic') . ')',
	2 => '2',
	3 => '3',
	4 => '4 (' . Cubix_I18n::translate('level_fluent') . ')'
);

$_db = Zend_Registry::get('db');

$DEFINITIONS['currencies'] = array(
	1 => 'USD',
	2 => 'EUR',
	3 => 'GBP'
);

define('ESCORT_PHOTO_TYPE_HARD', 1);
define('ESCORT_PHOTO_TYPE_SOFT', 2);
define('ESCORT_PHOTO_TYPE_PRIVATE', 3);

$DEFINITIONS['escorts_filter'] = require_once('filter.php');
$DEFINITIONS['escorts_sort'] = require_once('sort.php');

// sms statuses
define('SMS_NOT_SENT',                      0); // failed to be sent, or sms is turned off
define('SMS_SENT',                          1); // sent but no reply received
define('SMS_CONFIRMED',                     2); // received a "yes" reply
define('SMS_REJECTED',                      3); // received a "no" reply
define('SMS_MISMATCH',                      4); // the reply was not understood

$DEFINITIONS['sms_status_options'] = array(
	SMS_NOT_SENT => Cubix_I18n::translate('sms_not_sent'),
	SMS_SENT => Cubix_I18n::translate('sms_sent'),
	SMS_CONFIRMED => Cubix_I18n::translate('sms_confirmed'),
	SMS_REJECTED => Cubix_I18n::translate('sms_rejected'),
	SMS_MISMATCH => Cubix_I18n::translate('sms_mismatch')
);

// newletters statuses
define('NEWSLETTER_PENDING',	1);
define('NEWSLETTER_SENDING',	2);
define('NEWSLETTER_SENT',		3);
define('NEWSLETTER_CANCELED',	4);

$DEFINITIONS['newsletter_status_options'] = array(
	NEWSLETTER_PENDING => 'Pending',
	NEWSLETTER_SENDING => 'Sending',
	NEWSLETTER_SENT => 'Sent',
	NEWSLETTER_CANCELED => 'Canceled'
);

define('ERROR_PROFILE_DATA', 1);
define('CANNOT_OPEN_PHOTOS', 2);
define('REVIEW_AND_COMMENT', 3);

$DEFINITIONS['report_problems'] = array(
	
	ERROR_PROFILE_DATA=> Cubix_I18n::translate('error_profile_data'),
	CANNOT_OPEN_PHOTOS=> Cubix_I18n::translate('cannot_open_photos'),
	REVIEW_AND_COMMENT=> Cubix_I18n::translate('review_comments')

);

Zend_Registry::set('defines', $DEFINITIONS);
