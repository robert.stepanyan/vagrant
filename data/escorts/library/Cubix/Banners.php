<?php
class Cubix_Banners
{
	protected static $_applications = array();

	public static function GetFunctions()
	{
		$conf = Zend_Registry::get('system_config');
		
		return '';

		$output = "
			<script type='text/javascript'>
				<!--// <![CDATA[
				var OA_channel = '';
				var OA_p=location.protocol=='https:'?'https:':'http:';
				var OA_r=Math.floor(Math.random()*99999999);
				if (!document.OA_used) document.OA_used = ',';
				function OA_adjs(z,n)
				{
				  if (z>-1) {
					var az=\"<\"+\"script language='JavaScript' type='text/javascript' \";
					az+=\"src='\"+OA_p+\"//ads.escortforum.net/www/delivery/ajs.php?n=\"+n+\"&zoneid=\"+z;
					az+=\"&source=\"+OA_channel+\"&exclude=\"+document.OA_used+\"&r=\"+OA_r;
					az+=\"&mmm_fo=\"+(document.mmm_fo)?'1':'0';
					if (document.context) az+= \"&context=\" + escape(document.context);
					if (window.location) az+=\"&loc=\"+escape(window.location);
					if (document.referrer) az+=\"&referer=\"+escape(document.referrer);
					az+=\"'><\"+\"/script>\";
					document.write(az);
				  }
				}
				function OA_adpop(z,n)
				{
				  if (z>-1) {
					var az=\"<\"+\"script language='JavaScript' type='text/javascript' \";
					az+=\"src='\"+OA_p+\"//ads.escortforum.net/www/delivery/apu.php?n=\"+n+\"&zoneid=\"+z;
					az+=\"&source=\"+OA_channel+\"&exclude=\"+document.OA_used+\"&r=\"+OA_r;
					if (window.location) az+=\"&loc=\"+escape(window.location);
					if (document.referrer) az+=\"&referer=\"+escape(document.referrer);
					az+=\"'><\"+\"/script>\";
					document.write(az);
					
				  }
				}
				// ]]> -->
			</script>
		";

		return $output;
	}

	protected static $_exclude_ids = array();

	protected static function _getClient()
	{
		$app = Cubix_Application::getId();
		
		if ( $app == 2 ) {
			$client = new Zend_XmlRpc_Client('http://ads.6annonce.com/delivery/axmlrpc.php', new Zend_Http_Client(null, array('timeout' => 2)));
		}
		elseif( $app == 1 ) {
			$client = new Zend_XmlRpc_Client('http://ads2.escortforumit.xxx/delivery/axmlrpc.php', new Zend_Http_Client(null, array('timeout' => 2)));
		}
		elseif( $app == 11 ) {
			$client = new Zend_XmlRpc_Client('http://ads.asianescorts.com/delivery/axmlrpc.php', new Zend_Http_Client(null, array('timeout' => 2)));
		}
		elseif( $app == 16 ) {
			$client = new Zend_XmlRpc_Client('http://ads.and6.ch/delivery/axmlrpc.php', new Zend_Http_Client(null, array('timeout' => 15)));
		}
		elseif( $app == 25 ) {
			$client = new Zend_XmlRpc_Client('http://ads.and6.at/delivery/axmlrpc.php', new Zend_Http_Client(null, array('timeout' => 10)));
		}
		elseif( $app == 17 ) {
			$client = new Zend_XmlRpc_Client('http://ads.6anuncio.com/delivery/axmlrpc.php', new Zend_Http_Client(null, array('timeout' => 2)));
		}
		elseif( $app == 18 ) {
			$client = new Zend_XmlRpc_Client('http://ads.6classifieds.com/delivery/axmlrpc.php', new Zend_Http_Client(null, array('timeout' => 2)));
		}
		elseif( $app == 30 ) {
			$client = new Zend_XmlRpc_Client('http://ads.beneluxxx.com/delivery/axmlrpc.php', new Zend_Http_Client(null, array('timeout' => 2)));
		}
		elseif( $app == 33 ) {
			$client = new Zend_XmlRpc_Client('http://ads.escortmeetings.com/delivery/axmlrpc.php', new Zend_Http_Client(null, array('timeout' => 2)));
		}
		elseif( $app == 69 ) {
			$client = new Zend_XmlRpc_Client('http://ads.escortdirectory.com/delivery/axmlrpc.php', new Zend_Http_Client(null, array('timeout' => 2)));
		}
		else {
			$client = new Zend_XmlRpc_Client('http://ads2.escortforum.net/delivery/axmlrpc.php', new Zend_Http_Client(null, array('timeout' => 2)));
		} 

		return $client;
	}

	protected static function _getBannerXmlRpc($zone)
	{
		
		$client = self::_getClient();

		if ( defined('IS_CLI') && IS_CLI ) {
			$_SERVER = array(
				'REMOTE_ADDR' => '127.0.0.1'
			);
		}
		
		$var_map = array(
			'remote_addr'       => 'REMOTE_ADDR',
			'remote_host'       => 'REMOTE_HOST',
			'request_uri'       => 'REQUEST_URI',
			'https'             => 'HTTPS',
			'server_name'       => 'SERVER_NAME',
			'http_host'         => 'HTTP_HOST',
			'accept_language'   => 'HTTP_ACCEPT_LANGUAGE',
			'referer'           => 'HTTP_REFERER',
			'user_agent'        => 'HTTP_USER_AGENT',
			'via'               => 'HTTP_VIA',
			'forwarded'         => 'HTTP_FORWARDED',
			'forwarded_for'     => 'HTTP_FORWARDED_FOR',
			'x_forwarded'       => 'HTTP_X_FORWARDED',
			'x_forwarded_for'   => 'HTTP_X_FORWARDED_FOR',
			'client_ip'         => 'HTTP_CLIENT_IP'
		);

		$remote_info = array();
		foreach ( $var_map as $var => $name ) {
			if (isset($_SERVER[$name])) {
				$remote_info[$var] = $_SERVER[$name];
			}
		}

		$remote_info['cookies'] = $_COOKIE;

		$context = array();
		if ( count(self::$_exclude_ids[$zone]) ) {
			foreach ( self::$_exclude_ids[$zone] as $id ) {
				$context[] = array('!=' => 'bannerid:' . $id);
			}
		}
		try {
		$banner = $client->call('openads.view', array(
			$remote_info,
			'zone:' . intval($zone),
			0,
			'',
			'',
			false,
			$context
		));

		}
		catch (Exception $e) {
			$resp = $client->getHttpClient()->getLastResponse();
			echo $resp->getBody();
			die;
		}
		
		//echo $banner['bannerid'] . ', ' . strlen($banner['html']) . ', ' . count(self::$_exclude_ids) . "\n";ob_flush();
		
		if ( $banner['bannerid'] && strlen($banner['bannerid']) ) {
			self::$_exclude_ids[$zone][] = $banner['bannerid'];
		} else {
			return "";
		}
		return $banner;
	}

	public static function GetBanner($zone)
	{
		$rand = rand(1000, 9999);

		if ( preg_match('#\.test$#', $_SERVER['HTTP_HOST']) ) {
			return '';
		}

		try {
			echo('getting banner ' . $zone);
			$banner = self::_getBannerXmlRpc($zone);
			echo ("\n"); ob_flush();
			//return $banner;
		}
		// The ADS Server is down!
		catch ( Exception $e ) {
			echo("connection error. Zone ID: " . $zone . "\n");
			echo $e;
			$banner = array('html' => '');
		}
		
		if ( isset($banner['html']) ) {
			$html = str_replace('\'', '"', $banner['html']);
			//$html = preg_replace('#<a href=#i', '<a rel="nofollow" href=', $html);

			$html = preg_replace('#document.write\("(.+?)"\);#', 'document.write(\'$1\');', $html);
			
			
			$html = preg_replace('#<iframe(.+?)width="?1"?(.+?)>(.*?)</iframe>(.+)$#i', '$4', $html);
			$html = preg_replace('#<iframe(.+?)width="?1px"?(.+?)>(.*?)</iframe>(.+)$#i', '$4', $html);
			
			/*if ( preg_match("#sportberserk#", $banner) ) {
				$html = "";
			}*/
			
			$html = preg_replace('#null#i', '', $html);
		} else { 
			$html = ""; 
		}
		
		return $html;
	}
}
