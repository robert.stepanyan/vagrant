<?php
class Cubix_I18n
{
	protected static $_dictionary;
	
	protected static $_dictionary_lng;
	
	protected static $_langs;

	public static function setLang($lang_id)
	{
		$req = Zend_Controller_Front::getInstance()->getRequest();

		$req->setParam('lang_id', $lang_id);
	}

	public static function getLang()
	{
		$req = Zend_Controller_Front::getInstance()->getRequest();

		if(!$req->lang_id) return 'en';
		return $req->lang_id;
	}

	static public function _getLangsByAppId($app_id)
	{
		self::$_langs = array();

        if ( empty(self::$_langs[$app_id]) ) {
			$db = Zend_Registry::get('db');
			$rows = $db->query('SELECT l.id, l.title FROM langs l
				INNER JOIN application_langs al ON al.lang_id = l.id
				WHERE al.application_id = ? ORDER BY al.is_default DESC, l.id', $app_id)->fetchAll(Zend_Db::FETCH_OBJ);

			if(empty($rows)) {
                Cubix_Debug::log("LANGS ARE EMPTY : "  . json_encode($rows), 'DEBUG');
            } else {
                self::$_langs[$app_id] = array();

                foreach ($rows as $lang) {
                    self::$_langs[$app_id][$lang->id] = $lang->title;
                }
            }
        }
	}

	public static function getLangs($ids = true)
	{
		$app_id = Zend_Controller_Front::getInstance()->getRequest()->application_id;
		if(empty($app_id)) $app_id = Cubix_Application::getId();


        if ( $app_id ) {
            self::_getLangsByAppId($app_id);
            $langs = self::$_langs[$app_id];

        }
		elseif ( empty(self::$_langs) ) {
			self::$_langs = array();
			$db = Zend_Registry::get('db');
			$cache = Zend_Registry::get('cache');
			
			$cache_key =  'application_get_langs_cache__' . (int) $ids . Cubix_Application::getId();
			if ( ! $db_langs = $cache->load($cache_key) ) {
				$db_langs = $db->query('SELECT id, title FROM langs')->fetchAll(Zend_Db::FETCH_OBJ);

				$cache->save($db_langs, $cache_key, array(), 21600); //6 hours cache
			}

			foreach ($db_langs as $lang) {
				self::$_langs[$lang->id] = $lang->title;
			}
			$langs = self::$_langs;
		}
		else {
			$langs = self::$_langs;
		}

		if ( isset($langs) && is_array($langs) ) {
			if ( $ids ) {
				return array_keys($langs);
			}

			return $langs;
		}
		else return array();
	}
	
	public static function getLangsByAppId($app_id)
	{	
		$db = Zend_Registry::get('db');
		
		$langs = $db->query('SELECT lang_id FROM application_langs WHERE application_id = ?', $app_id)->fetchAll();
				
		return $langs;
	}
	
	public static function getValues($array, $field, $default = null)
	{
		$new = array();
		
		foreach ( self::getLangs() as $lang_id ) {
			$key = $field . '_' . $lang_id;
			
			if ( isset($array[$key]) ) {
				$new[$key] = $array[$key];
			}
			else {
				$new[$key] = $default;
			}
		}
		
		return $new;
	}
	
	public static function getTblFields($field, $app_id = null, $as_array = false, $alias = null)
	{
		if ( is_null($app_id) ) {
			$langs = self::getLangs();
		}
		else {
			$langs = Cubix_Application::getLangs($app_id, true);
		}

		$fields = array();
		
		foreach ( $langs as $lang_id ) {
			if ( $alias ) {
				$fields[] = '' . $field . '_' . $lang_id . ' AS ' . $alias . '_' . $lang_id . '';
			}
			else {
				$fields[] = '' . $field . '_' . $lang_id . '';
			}
		}

		if ( $as_array ) {
			return $fields;
		}

		return implode(', ', $fields);
	}
	
	public static function getTblField($field,$lng=NULL)
	{	$language = isset($lng)?$lng:self::getLang();

	    if(empty($language)) $language = 'en';
		return $field . '_' .$language;
	}
	
	public static function init($lng=NULL)
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		if ( Zend_Registry::isRegistered('system_config') ) {
			$config = Zend_Registry::get('system_config');
						
			//$cache_file = APPLICATION_PATH . '/cache/dictionary_' . self::getLang() . '.inc.php';
			$language = isset($lng)?$lng:self::getLang();
			$cache_key = 'dictionary_app_' . Cubix_Application::getId() . '_lng_' .$language;

			$cache = Zend_Registry::get('cache');
			$LNG = array();

			if (! $LNG = $cache->load($cache_key) ) {
				$db = Zend_Registry::get('db');
				$tmp = array();

				// Filling up the default strings with english
				// -------------------------------------------
				if( Cubix_Application::getId() == 69) {
					try {
						$default_english = $db->query('SELECT id, ' . self::getTblField('value', 'en') . ' AS value FROM dictionary')->fetchAll();

						foreach ($default_english as $i => $item) {
							$tmp[$item->id] = $item->value;
						}
						
					} catch (Exception $e) {
						Cubix_Debug::log($e->getMessage());
					}
				}
				// -------------------------------------------
				$result = $db->query('
					SELECT id, ' . self::getTblField('value',$lng) . ' AS value FROM dictionary
				');
				$dictionary = $result->fetchAll();
				
				foreach ( $dictionary as $i => $item ) {
					if(!empty($item->value)) {
						$tmp[$item->id] = $item->value;
					}
				}

				$LNG = json_encode($tmp);
				$cache->save($LNG, $cache_key, array(), $config['dictionary']['cacheLifetime']);

				//var_dump('not_cache');
			}
			
			//var_dump(json_decode($LNG, true));die;
			/*if ( ! is_file($cache_file) || filesize($cache_file) == 0 || time() - filemtime($cache_file) > $config['dictionary']['cacheLifetime'] ) {
				// @unlink($cache_file);
				
				$db = Zend_Registry::get('db');
				$result = $db->query('
					SELECT id, ' . self::getTblField('value') . ' AS value FROM dictionary
				');
				
				$dictionary = $result->fetchAll();
				
				
					$lines = array();

					$lines[] = '<?php' . "\r\n";

					foreach ( $dictionary as $i => $item ) {
						$lines[] = '$LNG[\'' . $item->id . '\'] = "' . preg_replace('#("|\$)#', '\\\$1', $item->value) . '";' . "\r\n";
					}
					
					file_put_contents($cache_file, implode('', $lines));
				
			}*/
			
			//require($cache_file);
				if(!isset($lng))
				{
					self::$_dictionary = json_decode($LNG, true);
				}
				else
					self::$_dictionary_lng = json_decode($LNG, true);
				
		}
	}
	
	public static function translate($id, array $args = array())
	{
		$registry = Zend_Registry::getInstance();
		$feedback_config = isset($registry['feedback_config']) ? $registry['feedback_config'] : false;
		
		$LNG = self::$_dictionary;

		// For Ed need to remove the asterisks
        // --------------------------------------------
        if(Cubix_Application::getId() == 69) {
            $id = strtolower($id);
            $LNG = array_change_key_case($LNG, CASE_LOWER);

            // Added temporary asterisks for Anca to see the ids
            // --------------------------------------
            $value = isset($LNG[$id]) ? $LNG[$id] : $id;
            // --------------------------------------

        }else{
            $value = isset($LNG[$id]) ? $LNG[$id] : '*' . $id . '*';
        }
        // --------------------------------------------

        $app = Cubix_Application::getById();

		/*if ( ! isset($LNG[$id]) ) {
			$db = Zend_Registry::get('db');
			if ( ! $db->fetchOne('SELECT TRUE FROM dictionary WHERE id = ?', $id) ) {
				$db->insert('dictionary', array('id' => strtolower($id)));
			}
		}*/

		$args = array_merge(array(
			'sys_url' => 'http://www.' . $app->host,
			'sys_site_title' => $app->title,
			'sys_app_country_title' => $app->country_title
		), $args);
		
		if ( $feedback_config ) {
			$args['sys_contact_email'] = $feedback_config['emails']['support'];
		}
		
		$a = array();
		if ( preg_match_all('#%([-._a-zA-Z0-9]+)%#', $value, $a) ) {
			foreach ($a[1] as $arg) {
				if ( isset($args[$arg]) ) {
					$value = str_replace('%' . $arg . '%', $args[$arg], $value);
				}
				else {
					$value = str_replace('%' . $arg . '%', '', $value);
				}
			}
		}
		
		return $value;
	}
	
	public static	function TransByValue($val,$lng='en')
	{
		$val =  mb_convert_encoding($val,'UTF-8');
		$key =  array_search($val,self::$_dictionary);
		if(isset($key) && $key!='')
		{
			self::init ($lng);	
			return isset(self::$_dictionary_lng[$key])?self::$_dictionary_lng[$key]:$val;
		}
		return $val;
	}
	
	public static function translateByLng($lng, $id, $args = array())
	{
		self::init($lng);
		
		$value = isset(self::$_dictionary_lng[$id]) ? self::$_dictionary_lng[$id] : self::$_dictionary[$id];
		
		$a = array();
		if ( preg_match_all('#%([-._a-zA-Z0-9]+)%#', $value, $a) ) {
			foreach ($a[1] as $arg) {
				if ( isset($args[$arg]) ) {
					$value = str_replace('%' . $arg . '%', $args[$arg], $value);
				}
				else {
					$value = str_replace('%' . $arg . '%', '', $value);
				}
			}
		}
		
		return $value;
	}
}
	
	
function __($key, array $params = array())
{
	return Cubix_I18n::translate($key, $params);
}


function _shorten($string, $width = 200) {
  if(strlen($string) > $width) {
	$string = str_replace("\n", "", $string);  
    $string = wordwrap($string, $width);
    $string = mb_substr($string, 0, strpos($string, "\n"), 'UTF-8') . "...";
		

  }
  $string = addslashes($string);
  return $string;
}

function _shorten_word($string, $width = 200) {
  if(strlen($string) > $width) {
    $string = mb_substr($string, 0, $width, 'UTF-8') . "...";
  }

  return $string;
}