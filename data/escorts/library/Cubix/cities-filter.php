<?php
return array(
	array(
		'title' => 'All',
		'childs' => NULL,
		'value' => 'all'
	),
	array(
		'title' => Cubix_I18n::translate('geneva'),
		'value' => 'geneva',
		'city_id' => '47'
	),
	array(
		'title' => Cubix_I18n::translate('vaud'),
		'value' => 'vaud',
		'city_id' => '48'
	),
	array(
		'title' => Cubix_I18n::translate('neuchatel'),
		'value' => 'neuchatel',
		'city_id' => '49'
	),
	array(
		'title' => Cubix_I18n::translate('jura'),
		'value' => 'jura',
		'city_id' => '50'
	),
	array(
		'title' => Cubix_I18n::translate('fribourg'),
		'value' => 'fribourg',
		'city_id' => '51'
	),
);
