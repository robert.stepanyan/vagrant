<?php

class Cubix_BB2Html 
{	
	public function bb2html($text)
	{
		$bbcode = array(//"<", ">",
						"[list]", "[*]", "[/list]",
						"[img]", "[/img]",
						"[b]", "[/b]",
						"[u]", "[/u]",
						"[i]", "[/i]",
						'[color=', "[/color]",
						'[size=', "[/size]",
						'[url=', "[/url]",
						'[mail=', "[/mail]",
						"[code]", "[/code]",
						"[quote]", "[/quote]",
						']');
		$htmlcode = array(//"&lt;", "&gt;",
						"<ul>", "<li>", "</ul>",
						'<img src="', '">',
						"<b>", "</b>",
						"<u>", "</u>",
						"<i>", "</i>",
						'<span style="color:', "</span>",
						'<span style="font-size:', "</span>",
						'<a href="', "</a>",
						'<a href="mailto:', "</a>",
						"<code>", "</code>",
						"<table width=100% bgcolor=lightgray><tr><td bgcolor=white>", "</td></tr></table>",
						'">');
		$newtext = str_replace($bbcode, $htmlcode, $text);
		$newtext = nl2br($newtext);//second pass
		
		return $newtext;
	}
}

?> 