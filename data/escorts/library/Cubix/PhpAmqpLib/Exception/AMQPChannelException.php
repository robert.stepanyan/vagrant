<?php
//namespace PhpAmqpLib\Exception;

/**
 * @deprecated use AMQPProtocolChannelException instead
 */
class Cubix_PhpAmqpLib_Exception_AMQPChannelException extends Cubix_PhpAmqpLib_Exception_AMQPException
{
}
