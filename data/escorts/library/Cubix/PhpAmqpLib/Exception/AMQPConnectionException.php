<?php
//namespace PhpAmqpLib\Exception;

/**
 * @deprecated use AMQPProtocolConnectionException instead
 */
class Cubix_PhpAmqpLib_Exception_AMQPConnectionException extends Cubix_PhpAmqpLib_Exception_AMQPException
{
}
