<?php

class Cubix_Email
{
	public static function send($email, $subject, $body, $htmlEmail = false, $from = null, $from_name = null, $attachments = array())
	{
		if(APPLICATION_ENV == 'development'){
			return true;
		}
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		try {
			$client->call('Email.send', array($email, $subject, $body, $htmlEmail, $from, $from_name, Cubix_Application::getId(), $attachments));
		}
		catch (Exception $e) {
			return $e->getMessage();
		}
	}
		
	public static function sendTemplate($template_id, $to_addr, array $data = array(), $from_addr = null, $from_name = null, $lang = null, $smtp2 = false, $subject = null)
	{
		if(APPLICATION_ENV == 'development'){
			return true;
		}
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		
		if(Cubix_Application::getId() == APP_ED && empty($lang)){
			$lang = 'en';
		}
		if(Cubix_Application::getId() == APP_6B){
			$lang = 'pt';
		}

		if(!$lang){
			$lang = Cubix_I18n::getLang();
		}

		try {
			return $client->call('Email.sendTemplate', array($template_id, $to_addr, $data, Cubix_Application::getId(), $lang, $from_addr, $from_name,  $smtp2, $subject));
		}
		catch (Exception $e) {
			return $e->getMessage();
		}
	}
}
