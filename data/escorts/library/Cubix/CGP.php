<?php

class Cubix_CGP_Exception extends Exception {}

class Cubix_CGP_Recurring_Exception extends Cubix_CGP_Exception {}
class Cubix_CGP_Recurring_Transaction_Exception extends Cubix_CGP_Recurring_Exception {}

class Cubix_CGP
{
	static private function _config()
	{
		$config = (object) Zend_Registry::get('system_config');
		return (object) $config->gateway;
	}

	static public function isTest()
	{
		return (bool) self::_config()->is_test;
	}

	static public function getGatewayUrl()
	{
		return self::_config()->url;
	}

	static public function getSecurityHash()
	{
		return self::_config()->security_hash;
	}

	static public function getSiteId()
	{
		return (int) self::_config()->siteid;
	}

	static public function getCurrency()
	{
		return self::_config()->currency;
	}

	static public function getPaymentOption()
	{
		return 'creditcard';
	}

	static public function getAmount()
	{
		$amount = (double) self::_config()->amount;
		return round($amount * 100);
	}

	static public function makeHash($ref, $amount = null, $test = false)
	{
		$hash = '';
		if ( self::isTest() )
			$hash .= 'TEST';
		$hash .= self::getSiteId();
		$hash .= $amount;
		$hash .= $ref;
		$hash .= self::getSecurityHash();
		return md5($hash);
	}

	static public function getPaymentUrl(array $params = array())
	{
		$array = array();

		if ( self::isTest() ) {
			$array['test'] = 1;
		}

		$array['option'] = self::getPaymentOption();
		$array['siteid'] = self::getSiteId();
		$array['currency'] = self::getCurrency();
		if ( ! isset($params['amount']) ) {
			throw new Cubix_CGP_Exception('Amount is required');
		}
		$array['amount'] = $params['amount'];
		if ( ! isset($params['ref']) ) {
			throw new Cubix_CGP_Exception('Ref is required');
		}
		$array['ref'] = $params['ref'];
		if ( isset($params['description']) ) {
			$array['description'] = $params['description'];
		}
		$array['hash'] = self::makeHash($array['ref'], $array['amount']);

		if ( isset($params['email']) ) {
			$array['email'] = $params['email'];
		}

		$url = http_build_query($array);
		return self::getGatewayUrl() . '?' . $url;
	}

	static public function renderFormInputs(array $params = array())
	{
		$array = array();

		if ( self::isTest() ) {
			$array['test'] = 1;
		}

		$array['option'] = self::getPaymentOption();
		$array['siteid'] = self::getSiteId();
		$array['currency'] = self::getCurrency();
		$array['amount'] = self::getAmount();
		if ( ! isset($params['ref']) ) {
			throw new Cubix_CGP_Exception('Ref is required');
		}
		$array['ref'] = $params['ref'];
		if ( isset($params['description']) ) {
			$array['description'] = $params['description'];
		}
		$array['hash'] = self::makeHash($array['ref'], $array['amount']);

		if ( isset($params['email']) ) {
			$array['email'] = $params['email'];
		}

		foreach ( $array as $key => $value ) {
			$element = "<input type=\"hidden\" name=\"$key\" value=\"$value\" />";
			$array[$key] = $element;
		}

		return implode("\n", $array);
	}

	/**
	 * Needs to be called from backend
	 *
	 */
	static public function doRecurringRequest($site_id, $transaction_id, $amount, $ref)
	{
	 	$config = Zend_Registry::get('system_config');
		
		$security_hash = $config['gateway']['security_hash'];

		$hash = md5($site_id . $amount . $security_hash);

		$client = new Zend_Http_Client('http://www.cardgateplus.com/webapi/recurring');
		$client->setParameterPost('site_id', $site_id);
		$client->setParameterPost('transaction_id', $transaction_id);
		$client->setParameterPost('amount', $amount);
		$client->setParameterPost('reference', $ref);

		$client->setParameterPost('hash', $hash);
		
		$response = $client->request(Zend_Http_Client::POST);
		$xml = $response->getBody();

		$xml = simplexml_load_string($xml);
		
		if ( (int) $xml->response->errorNotification->errorNumber > 0 ) {
			throw new Cubix_CGP_Recurring_Exception(trim($xml->response->errorNotification->errorText),
					(int) $xml->response->errorNotification->errorNumber);
		}
		elseif ( (int) $xml->response->payment->paymentStatus == 210 ) {
			return (object) array(
				'transaction_id' => (int) $xml->response->payment->transactionID,
				'currency' => $xml->response->payment->paymentCurrency,
				'amount' => $xml->response->payment->paymentAmount,
				'status' => (int) $xml->response->payment->paymentStatus
			);
		}
		elseif ( (int) $xml->response->payment->paymentStatus == 310 ) {
			return (object) array(
				'transaction_id' => (int) $xml->response->payment->transactionID,
				'currency' => $xml->response->payment->paymentCurrency,
				'amount' => $xml->response->payment->paymentAmount,
				'status' => (int) $xml->response->payment->paymentStatus
			);
		}
	}
}
