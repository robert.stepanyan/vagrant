<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Cubix_Clickatell_Sms
{
    protected $user = 'bonsai';
    protected $password = 'batualla2011';
    protected $api_id = '3294998';

    protected  $recipients = array();
    protected  $message;
    protected  $template;
    protected  $from = 'EstateAm';
    protected  $unicode = 0;
    protected  $mo = '';
    protected  $callback = '';
    //protected  $session_id = '';
    protected  $cliMsgId = '';

    public function __construct(){
       // TODO
    }

	public function setCliMsgId($id) 
	{
		$this->cliMsgId = $id;
	}
	
	/*public function setSessionId($session_id) 
	{
		$this->session_id = $session_id;
	}*/
	
	public function setMo($originator) 
	{
		$this->mo = $originator;
	}
	
	public function setCallback($callback)
	{
		$this->callback = $callback;
	}
	
    public function addRecipients($recipients)
    {
        // validating
       if ( is_array($recipients) ) {
		 foreach ( $recipients as $i => $phone ) 
		 {
			if (strpos($phone, '00') === 0)
			{
				$sub = substr($phone, 2);
				$recipients[$i] = '+' . $sub;
			}
		  }
		  $this->recipients = implode(',', $recipients);
	   }
	   else {
		   if (strpos($recipients, '00') === 0)
			{
				$sub = substr($recipients, 2);
				$recipients = '+' . $sub;
			}
		   $this->recipients = $recipients;
	   }
    }


    public function setMessage($message)
    {
        $this->message = substr($message, 0, 160);
    }

    public function setTemplate($template)
    {
        $this->template = substr($template, 0, 160);
    }


    public function setFrom($from)
    {
		if (strpos($from, '00') === 0)
		{
			$sub = substr($from, 2);
			$from = '+' . $sub;
		}
		
        $this->from = $from;
    }

    public function setUnicode($flag)
    {
        if($flag)
            $this->unicode = 1;
        else
            $this->unicode = 0;
    }



    public function send()
    {
        // validating
        $this->validate();

		return true;

        // Checking the sending mode
        if($this->template) {
            $this->sendByTemplate();
            return true;
        } else {
            $this->sendCommonMessage();
            return true;
        }
    }


    private function validate()
    {
        // checking recipients
        if(strlen($this->recipients) == 0){
            throw new Exception('No recipients to send');
            return false;
        }

        // checking message or template
        if(($this->message || $this->template) == false) {
            throw new Exception('No message or template defined to send');
            return false;
        }

        return true;
    }


    private function sendByTemplate()
    {
        $success = 0;
        foreach($this->recipients as $recipient)
        {
            // $template = $this->template;
            // preparing template

            // print_r($recipient);


            $search = array('{fname}', '{lname}', '{mobile}', '{contract}');
            $replace = array($recipient->fname, $recipient->lname, $recipient->mobile, $recipient->contract);


            $message = str_replace($search, $replace, $this->template);

            // echo($message);
            // die();

            // sendind to user
            $this->sendMessageClickatell($message, array($recipient));
        }

    }


    private function sendCommonMessage()
    {
        return $this->sendMessageClickatell($this->message, $this->recipients);
    }



    private function sendMessageClickatell($message, $recipients)
    {
        // Creating HTTP Client object using Zend
        $client = new Zend_Http_Client();

        $client->setUri('http://api.clickatell.com/http/sendmsg');

        $client->setConfig(array(
                                 'maxredirects' => 0,
                                 'timeout'      => 30));

        //$recipients = $this->prepareRecipientsString($recipients);

        $client->setParameterGet(array(
            'user'  =>  $this->user,
            'password' => $this->password,
            'api_id'     => $this->api_id,
            'to' => $recipients,
            'text' => $message,
            'from' => $this->from,
            'unicode' => $this->unicode,
            'mo' => $this->mo,
            'callback' => $this->callback,
            'cliMsgId' => $this->cliMsgId
            //'session_id' => $this->session_id
        ));


         //print_r($client);


        $response = $client->request();

        return $response;
    }


    private function prepareRecipientsString($recipients)
    {
        $url = '';

        $array = array();
        foreach($recipients as $recipient){
            $array[] = $recipient->mobile;
        }

        return implode(',', $array);
    }


}


?>
