<?php

class Cubix_Api_Module
{
	/**
	 *
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected static $_db;
	
	protected static $_app;

	protected static $_config;

	public static function setDb(Zend_Db_Adapter_Mysqli $db)
	{
		self::$_db = $db;
	}

	public static function setApp($app)
	{
		self::$_app = $app;
	}

	public static function setConfig($config)
	{
		self::$_config = $config;
	}

	protected static function db()
	{
		return self::$_db;
	}

	protected static function app()
	{
		return self::$_app;
	}

	protected static function config()
	{
		return self::$_config;
	}
}
