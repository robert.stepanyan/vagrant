<?php

class Cubix_Api_Error
{
	public function __construct($error)
	{
		
	}

	public static function Exception2Array(Exception $e)
	{
		return array(
			'code' => $e->getCode(),
			'line' => $e->getLine(),
			'message' => $e->getMessage(),
			'trace' => $e->getTraceAsString(),
			'file' => $e->getFile()
		);
	}
}
