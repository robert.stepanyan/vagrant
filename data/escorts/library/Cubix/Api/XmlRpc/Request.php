<?php

class Cubix_Api_XmlRpc_Request extends Zend_XmlRpc_Request_Http
{
	/**
	 * @param int $code
	 * @param string $message
	 * @return Cubix_Api_XmlRpc_Request
	 */
	public function setFault($code = 404, $message = '')
	{
		$this->_fault = new Zend_XmlRpc_Fault($code, $message);
		
		return $this;
	}
}
