<?php

class Cubix_Api_XmlRpc_Server extends Zend_XmlRpc_Server
{
	public function setRequest($request)
	{
		$api_key = isset($_SERVER['HTTP_API_KEY']) ? $_SERVER['HTTP_API_KEY'] : null;
		
		if ( is_null($api_key) ) {
			return $request->setFault(999, 'No api key was specified');
		}
		
		$app = System_Api::getAppByApiKey($api_key);
		
		if ( is_null($app) ) {
			return $request->setFault(999, 'Invalid api key was specified');
		}
		
		Cubix_Application::setId($app->id);
		Zend_Registry::set('app', $app);
		
		return $request;
	}
}
