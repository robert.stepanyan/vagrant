<?php

class Cubix_Api_XmlRpc_Client
{
	protected static $_api_key;

	protected static $_instance;

	/**
	 *
	 * @return Cubix_Api
	 */
	public static function getInstance()
	{
		if ( empty(self::$_instance) ) {
			self::$_instance = new self;
		}

		return self::$_instance;
	}

	public static function setApiKey($api_key)
	{
		self::$_api_key = $api_key;
	}
	
	public static function getApiKey()
	{
		if ( empty(self::$_api_key) ) {
			$registry = Zend_Registry::getInstance();
			if ( isset($registry['api_key']) ) {
				return $registry['api_key'];
			}
			
			return null;
		}
		
		return self::$_api_key;
	}
	
	protected static $_server;
	
	public static function setServer($server)
	{
		self::$_server = $server;
	}
	
	public static function getServer()
	{
		if ( empty(self::$_server) ) {
			return null;
		}
		
		return self::$_server;
	}

	public function call($method, $params = array(), $timeout = 30)
	{
		//if ( $method == 'setUserSmsLng' || $method == 'getEscortVotingData' || $method == 'isInAlerts' || $method == 'isInFavorites' ) return false;
		$start_time = microtime(true);
		
		$is_v2 = (preg_match('#^(.+?)\.(.+?)$#', $method) > 0);

		$url = Cubix_Api_XmlRpc_Client::getServer() . '/?api_key=' . Cubix_Api_XmlRpc_Client::getApiKey() . '&v2=1';
		if ( ! preg_match('#^(http|https)://#', $url) ) $url = 'http://' . $url;
		$url .= '&method=' . $method;

		if ( defined('IS_DEBUG') && IS_DEBUG ) {
			if ( ! defined('APP_ROOT_PATH') ) define('APP_ROOT_PATH', realpath(APPLICATION_PATH));
//			$execution_time = $execution_end_time - $execution_start_time;
			$delim = str_repeat('#', 60) . "\n";

			$text = $delim.
				'API Call: ' . $url . "\n" . str_repeat('-', 60) . "\n\n";

			$backtrace = debug_backtrace();
			array_shift($backtrace);

			$trace_list = array();
			foreach($backtrace as $i => $bt) {

				if ( ! isset($bt['class']) ) $bt['class'] = '';
				if ( ! isset($bt['type']) ) $bt['type'] = '';
				if ( ! isset($bt['file']) ) $bt['file'] = '';
				if ( ! isset($bt['line']) ) $bt['line'] = '';

				$trace_list[] =
					($i + 1) . ". " .
					str_replace('\\', '/', str_replace(APP_ROOT_PATH, '', $bt['file'])) . ", " .
					$bt['line'] . "\n" .
					$bt['class'] . $bt['type'] . $bt['function'];
			}

			$trace_list = implode("\n\n", $trace_list);

			$text .= $trace_list . "\n" . $delim . "\n";

			$GLOBALS['SQLS'][] = $text;
		}

		/* --> Perform a POST request */
		if ( count($params) ) {
			if ( ! $is_v2 ) {
				$postdata = serialize($params);
			}
			else {
				$postdata = serialize($params);				
			}

			$opts = array('http' => array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $postdata,
				'timeout' => $timeout
			));
			
			$context  = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);

		}
		else {
			$context  = stream_context_create(array('http' => array('timeout' => $timeout)));
			$result = file_get_contents($url, false, $context);
		}
		/* <-- */

		if(defined('RABBIT_ON') && RABBIT_ON ){
		
			/* Log in RabbitMq */
			
			$rabbit = Zend_Registry::get('rabbit_mq');
			$channel = $rabbit['channel']; 
			$que_name = 'ha.api_calls';
			$end_time = microtime(true);
			$time = round($end_time - $start_time, 4);
			$bt = debug_backtrace(false);
			$location = 'File - ' .$bt[0]["file"] . ' | line - '. $bt[0]["line"];
			$rabbit_msg = array(
				'uid' => $rabbit['uniq_id'], 
				'api_class' => 'Cubix_Api_XmlRpc_Client',
				'host' => $_SERVER[HTTP_HOST],
				'ip' => $_SERVER['SERVER_ADDR'],
				'request_uri' => $_SERVER['REQUEST_URI'],
				'method' => $method,
				'exec_time' => $time,
				'location' => $location,
				'date' => date("Y-m-d H:i:s")
			);
			
			$msg = new Cubix_PhpAmqpLib_Message_AMQPMessage(json_encode($rabbit_msg));
			$channel->basic_publish($msg, '', $que_name);
			/* Log in RabbitMq END */
		}
		
		if ( false === $result ) {
			Cubix_Debug::log($result, 'ERROR');
			throw new Exception('Could not call method, probably server is unreachable!');
		}

		if ( ! $is_v2 ) {
			$data = unserialize($result);

			if ( false === $data ) {
				//var_dump($result);die;
				Cubix_Debug::log($result, 'ERROR');
				throw new Exception('Invalid response from API server, response has been logged');
			}
			
			if ( is_array($data) && isset($data['bool']) ) {
				$data = $data['bool'];
			}
		}
		else {			
			$data = unserialize($result);
			/*var_dump($method);
			var_dump($params);
			var_dump($result);*/

			if ( null == $data ) {
				Cubix_Debug::log($method . " - " . $result, 'ERROR');
				//throw new Exception('Invalid response from API server, response has been logged');
			}
			elseif ( is_array($data) && array_key_exists('__internal', $data) ) {
				Cubix_Debug::log($method . " - " . $result, 'ERROR');
				throw new Exception("Exception recieved from api\n" . $data['__internal']);
			}

			if ( is_array($data) && array_key_exists('__bool', $data) ) {
				$data = $data['__bool'];
			}
		}

		if ( isset($_GET['dump']) && $_GET['dump'] )
				file_put_contents('/var/log/xmlrpc-bbt2.log', date('d M, Y H:i:s') . ' - method (' . $url . ') - ' . "\n\n", FILE_APPEND);


		return $data;
	}
}
