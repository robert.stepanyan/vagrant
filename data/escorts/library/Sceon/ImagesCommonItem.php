<?php

class Sceon_ImagesCommonItem extends Cubix_Model_Item
{
	/**
	 * The image utility library
	 *
	 * @var Sceon_Images
	 */
	protected static $_images;
	
	protected $_app_id = null;
	
	public function __construct($rowData = array())
	{
		parent::__construct($rowData);
		
		if ( empty(self::$_images) ) {
			self::$_images = new Sceon_ImagesCommon();
		}
	}
		
	public function getUrl($size_name = null, $from_server = false)
	{
		$data = array(
			'application_id' => $this->application_id,
			'catalog_id' => $this->image_id,
			'hash' => $this->hash,
			'ext' => $this->ext
		);
		
		if ( $size_name )
		{
			$data['size'] = $size_name;
		}
		
		if ( $from_server )
			return self::$_images->getServerUrl(new Sceon_Images_Entry($data));
		else
			return self::$_images->getUrl(new Sceon_Images_Entry($data));
	}
}
