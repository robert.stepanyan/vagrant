<?php

class Sceon_I18n
{
	protected static $_dictionary;
	
	protected static $_langs;

	public static function setLang($lang_id)
	{
		$req = Zend_Controller_Front::getInstance()->getRequest();

		$req->setParam('lang_id', $lang_id);
	}

	public static function getLang()
	{
		$req = Zend_Controller_Front::getInstance()->getRequest();

		return $req->lang_id;
	}

	static public function _getLangsByAppId($app_id)
	{
		self::$_langs = array();
		if ( empty(self::$_langs[$app_id]) ) {
			$db = Zend_Registry::get('db');
			self::$_langs[$app_id] = array();
			foreach ($db->query('SELECT l.id, l.title FROM langs l
				INNER JOIN application_langs al ON al.lang_id = l.id
				WHERE al.application_id = ? ORDER BY al.is_default DESC, l.id', $app_id)->fetchAll(Zend_Db::FETCH_OBJ) as $lang) {
				self::$_langs[$app_id][$lang->id] = $lang->title;
			}
		}
	}

	public static function getLangs($ids = true)
	{
		$app_id = Zend_Controller_Front::getInstance()->getRequest()->application_id;
		if ( $app_id ) {
			self::_getLangsByAppId($app_id);
			$langs = self::$_langs[$app_id];
		}
		elseif ( empty(self::$_langs) ) {
			self::$_langs = array();
			$db = Zend_Registry::get('db');
			foreach ($db->query('SELECT id, title FROM langs')->fetchAll(Zend_Db::FETCH_OBJ) as $lang) {
				self::$_langs[$lang->id] = $lang->title;
			}
			$langs = self::$_langs;
		}
		else {
			$langs = self::$_langs;
		}

		if ( isset($langs) && is_array($langs) ) {
			if ( $ids ) {
				return array_keys($langs);
			}

			return $langs;
		}
		else return array();
	}
	
	public static function getLangsByAppId($app_id)
	{	
		$db = Zend_Registry::get('db');
		
		$langs = $db->query('SELECT lang_id FROM application_langs WHERE application_id = ?', $app_id)->fetchAll();
				
		return $langs;
	}
	
	public static function getValues($array, $field, $default = null)
	{
		$new = array();
		
		foreach ( self::getLangs() as $lang_id ) {
			$key = $field . '_' . $lang_id;
			
			if ( isset($array[$key]) ) {
				$new[$key] = $array[$key];
			}
			else {
				$new[$key] = $default;
			}
		}
		
		return $new;
	}
	
	public static function getTblFields($field, $app_id = null, $as_array = false, $alias = null)
	{
		if ( is_null($app_id) ) {
			$langs = self::getLangs();
		}
		else {
			$langs = Sceon_Application::getLangs($app_id, true);
		}
		
		$fields = array();
		
		foreach ( $langs as $lang_id ) {
			if ( $alias ) {
				$fields[] = '' . $field . '_' . $lang_id . ' AS ' . $alias . '_' . $lang_id . '';
			}
			else {
				$fields[] = '' . $field . '_' . $lang_id . '';
			}
		}

		if ( $as_array ) {
			return $fields;
		}

		return implode(', ', $fields);
	}
	
	public static function getTblField($field)
	{
		return $field . '_' . self::getLang();
	}
	
	public static function init()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		
		if ( Zend_Registry::isRegistered('system_config') ) {
			$config = Zend_Registry::get('system_config');
			//$cache_file = APPLICATION_PATH . '/cache/dictionary_' . self::getLang() . '.inc.php';
		
			$cache_key = 'dictionary_app_' . Sceon_Application::getId() . '_lng_' . self::getLang();
			
			$cache = Zend_Registry::get('cache');
			
			$LNG = array();
			
			if ( ! $LNG = $cache->load($cache_key) ) {
				$db = Zend_Registry::get('db');
				
				$result = $db->query('
					SELECT id, ' . self::getTblField('value') . ' AS value FROM dictionary
				');
				
				$dictionary = $result->fetchAll();
				
				$tmp = array();
				foreach ( $dictionary as $i => $item ) {
					$tmp[$item->id] = $item->value;
				}
				
				$LNG = json_encode($tmp);
				
				$cache->save($LNG, $cache_key, array(), $config['dictionary']['cacheLifetime']);
				//var_dump('not_cache');
			}
			
			//var_dump(json_decode($LNG, true));die;
			/*if ( ! is_file($cache_file) || filesize($cache_file) == 0 || time() - filemtime($cache_file) > $config['dictionary']['cacheLifetime'] ) {
				// @unlink($cache_file);
				
				$db = Zend_Registry::get('db');
				$result = $db->query('
					SELECT id, ' . self::getTblField('value') . ' AS value FROM dictionary
				');
				
				$dictionary = $result->fetchAll();
				
				
					$lines = array();

					$lines[] = '<?php' . "\r\n";

					foreach ( $dictionary as $i => $item ) {
						$lines[] = '$LNG[\'' . $item->id . '\'] = "' . preg_replace('#("|\$)#', '\\\$1', $item->value) . '";' . "\r\n";
					}
					
					file_put_contents($cache_file, implode('', $lines));
				
			}*/
			
			//require($cache_file);
			
			self::$_dictionary = json_decode($LNG, true);
		}
	}
	
	public static function translate($id, array $args = array())
	{
		$registry = Zend_Registry::getInstance();
		$feedback_config = isset($registry['feedback_config']) ? $registry['feedback_config'] : false;
		
		$LNG = self::$_dictionary;
		
		$value = isset($LNG[$id]) ? $LNG[$id] : '*' . $id . '*';
		
		$app = Sceon_Application::getById();
		
		$args = array_merge(array(
			'sys_url' => 'http://www.' . $app->host,
			'sys_site_title' => $app->title,
			'sys_app_country_title' => $app->country_title
		), $args);
		
		if ( $feedback_config ) {
			$args['sys_contact_email'] = $feedback_config['emails']['support'];
		}
		
		$a = array();
		if ( preg_match_all('#%([-._a-zA-Z0-9]+)%#', $value, $a) ) {
			foreach ($a[1] as $arg) {
				if ( isset($args[$arg]) ) {
					$value = str_replace('%' . $arg . '%', $args[$arg], $value);
				}
				else {
					$value = str_replace('%' . $arg . '%', '', $value);
				}
			}
		}
		
		return $value;
	}
}

function ___($key, array $params = array())
{
	return Sceon_I18n::translate($key, $params);
}

function __shorten($string, $width = 200) {
  if(strlen($string) > $width) {
    $string = wordwrap($string, $width);
    $string = mb_substr($string, 0, strpos($string, "\n"), 'UTF-8') . "...";
  }

  return $string;
}