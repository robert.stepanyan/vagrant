<?php

class Sceon_Application
{
	protected static $_applications = array();
	
	public static function getAll()
	{
		$db = Zend_Registry::get('db');
		
		return $db->query('SELECT a.id, a.title, a.host  FROM applications a')->fetchAll();
	}
	
	public static function getLangs($application_id = NULL, $ids = false)
	{
		static $_langs = array();
		
		if ( is_null($application_id) ) {
			$application_id = self::getId();
		}
		
		if ( ! isset($_langs[$application_id]) ) {
			$db = Zend_Registry::get('db');
			
			$langs = $db->query('
				SELECT l.id, l.title FROM application_langs al
				INNER JOIN langs l ON l.id = al.lang_id
				WHERE al.application_id = ?
			', $application_id)->fetchAll();
			
			$_langs[$application_id] = $langs;
		}
		
		$langs = $_langs[$application_id];
		
		if ( $ids ) {
			foreach ( $langs as $i => $lang ) {
				$langs[$i] = $lang['id'];
			}
		}
		
		return $langs;
	}
	
	public static function getByHost($host)
	{
		$db = Zend_Registry::get('db');
		
		return $db->query('
			SELECT a.id, a.title, a.host FROM applications a WHERE a.host = ?
		', $host)->fetch();
	}
	
	public function getPhoneNumber($application_id)
	{
		return self::getById($application_id)->phone_number;
	}
	
	public static function getById($id = NULL)
	{
		$db = Zend_Registry::get('db');
		
		if ( is_null($id) ) {
			$id = Sceon_Application::getId();
		}
		
		if ( ! isset(self::$_applications[$id]) ) {
			$cache = Zend_Registry::get('cache');

			$cache_key =  '11_application_get_by_id___' . $id;
			if ( ! $app = $cache->load($cache_key) ) {
				$app = $db->query('
					SELECT
						a.host, a.url, a.backend_url, a.title,
						a.phone_number, a.min_photos, a.currency_symbol, a.currency_id, a.currency_title,
						a.max_working_cities, a.measure_units
					FROM applications a
					WHERE a.id = ?
				', $id)->fetch(Zend_Db::FETCH_OBJ);

				$cache->save($app, $cache_key, array(), 21600); //6 hours cache
			}
			
			
			self::$_applications[$id] = $app;
		}
		
		return self::$_applications[$id];
	}

	public static function getGoogleId()
	{
		$db = Zend_Registry::get('db');

		
		$id = Sceon_Application::getId();
		

		
		$cache = Zend_Registry::get('cache');


		$cache_key =  '11_google_id___' . $id;
		if ( ! $app = $cache->load($cache_key) ) {
			$app = $db->fetchOne('
				SELECT
					a.google_id
				FROM applications a
				WHERE a.id = ?
			', $id);

			$cache->save($app, $cache_key, array());
		}

		return $app;
	}
	
	protected static $_app_id;
	
	public static function setId($id)
	{
		self::$_app_id = $id;
	}
	
	public static function getId()
	{		
		if ( Zend_Registry::isRegistered('system_config') ) {
			$config = Zend_Registry::get('system_config');
			
			if ( isset($config['application_id']) ) {
				return $config['application_id'];
			}
		}
		
		return self::$_app_id;
	}
	
	public static function isDomainBlacklisted($domain)
	{
		$db = Zend_Registry::get('db');
		
		$sql = "SELECT 1 FROM blacklisted_domains WHERE ( ? REGEXP CONCAT('^([a-z]+://)?[^/]*', domain, '(.*)$') ) OR ( ? REGEXP CONCAT('', domain, '$') ) OR ( ? REGEXP CONCAT('([a-z]+://)?[^/]*', domain, '') )";
		
		return (boolean) $db->query($sql, array($domain, $domain, $domain))->fetch();
	}

	public static function getDefaultLang($app_id = null)
	{
		static $def_langs = array();

		if ( is_null($app_id) ) {
			$app_id = self::getId();
		}

		// Static var cache to not overload memcached
		if ( isset($def_langs[$app_id]) ) {
			return $def_langs[$app_id];
		}

		$db = Zend_Registry::get('db');
		$cache = Zend_Registry::get('cache');

		$cache_key =  $app_id . '_application_get_default_lang__';
		if ( ! $def_lng = $cache->load($cache_key) ) {
			$def_lng = $db->fetchOne('SELECT lang_id FROM application_langs WHERE application_id = ? AND is_default = 1', $app_id);
			$cache->save($def_lng, $cache_key, array());
		}

		$def_langs[$app_id] = $def_lng;

		return $def_lng;
	}
}
