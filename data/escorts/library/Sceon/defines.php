<?php

define('ADMIN_TYPE_NORMAL',                 1);
define('ADMIN_TYPE_PAYMENT_MANAGER',        2);
define('ADMIN_TYPE_DATA_ENTRY',             3);
define('ADMIN_TYPE_SUPERUSER',              99); // superuser can be added only directly in db

$DEFINITIONS['ADMIN_TYPE'] = array(
    ADMIN_TYPE_NORMAL           => 'admin',
    ADMIN_TYPE_PAYMENT_MANAGER  => 'payment manager',
    ADMIN_TYPE_DATA_ENTRY       => 'data entry',
    ADMIN_TYPE_SUPERUSER		=> 'superadmin'
);

define('USER_STATUS_ACTIVE',                1);
define('USER_STATUS_NOT_VERIFIED',          -1);
define('USER_STATUS_DISABLED',              -4);
define('USER_STATUS_DELETED',               -5);

$DEFINITIONS['user_status_options'] = array(
	USER_STATUS_ACTIVE             => 'Active',
    USER_STATUS_DISABLED           => 'Disabled',
    USER_STATUS_NOT_VERIFIED       => 'Not verified',
    USER_STATUS_DELETED			   => 'Deleted'
    /*USER_STATUS_ACTIVE             => Sceon_I18n::translate('status_active'),
    USER_STATUS_DISABLED           => Sceon_I18n::translate('status_disabled'),
    USER_STATUS_NOT_VERIFIED       => Sceon_I18n::translate('status_not_verified')*/
);


define('ESCORT_STATUS_NO_PROFILE',		1);			// 000000001
define('ESCORT_STATUS_NO_ENOUGH_PHOTOS',	2);		// 000000010
define('ESCORT_STATUS_NOT_APPROVED',		4);		// 000000100
define('ESCORT_STATUS_OWNER_DISABLED',	8);			// 000001000
define('ESCORT_STATUS_ADMIN_DISABLED',	16);		// 000010000
define('ESCORT_STATUS_ACTIVE',			32);		// 000100000
define('ESCORT_STATUS_IS_NEW',			64);		// 001000000
define('ESCORT_STATUS_PROFILE_CHANGED',	128);		// 010000000
define('ESCORT_STATUS_DELETED',			256);		// 100000000
define('ESCORT_STATUS_FAKE_PICTURE_DISABLED',512);	// 1000000000
define('ESCORT_STATUS_TEMPRARY_DELETED', 1024);	// 1000000000
define('ESCORT_STATUS_SUSPICIOUS_DELETED',	2048);	// 100000000000

$DEFINITIONS['escort_status_options'] = array(
    ESCORT_STATUS_ACTIVE            => 'Active',
    ESCORT_STATUS_ADMIN_DISABLED	=> 'Admin Disabled',
    ESCORT_STATUS_OWNER_DISABLED	=> 'Owner Disabled',
    ESCORT_STATUS_IS_NEW			=> 'New',
    ESCORT_STATUS_PROFILE_CHANGED	=> 'Profile Changed',
    ESCORT_STATUS_NOT_APPROVED		=> 'Not Approved',
    ESCORT_STATUS_NO_ENOUGH_PHOTOS	=> 'No Enough Photos',
    ESCORT_STATUS_NO_PROFILE		=> 'No Profile',
    ESCORT_STATUS_DELETED			=> 'Deleted',
    ESCORT_STATUS_FAKE_PICTURE_DISABLED		=> 'Fake Picture Disabled',
	ESCORT_STATUS_TEMPRARY_DELETED  => 'Temporary Deleted',
	ESCORT_STATUS_SUSPICIOUS_DELETED		=> 'Suspicious Deleted'
);

$DEFINITIONS['escort_status_options_popup'] = array(
    ESCORT_STATUS_ACTIVE            => 'Active',
    ESCORT_STATUS_ADMIN_DISABLED	=> 'Admin Disabled',
    ESCORT_STATUS_OWNER_DISABLED	=> 'Owner Disabled'
);

define('STATUS_ACTIVE',                     1);
define('STATUS_NO_PHOTOS',                  -1);
define('STATUS_NO_PROFILE',                 -2);
define('STATUS_OWNER_DISABLED',             -3);
define('STATUS_ADMIN_DISABLED',             -4);
define('STATUS_WAITING_APPROVAL',           -5);
define('STATUS_NO_WORK_LOCATIONS',          -6);
define('STATUS_NO_LANGUAGES',               -7);
define('STATUS_NO_MAIN_PIC',                -8);
define('STATUS_EXPIRED',                    -9);
define('STATUS_DOMAIN_BLACKLISTED',         -10); // disabled because domain is blacklisted
define('STATUS_DELETED',                    -99);

$DEFINITIONS['status_options'] = array(
    STATUS_ACTIVE                   => 'Active',
    STATUS_NO_PHOTOS                => 'No photos',
    STATUS_NO_PROFILE               => 'No profile',
    STATUS_OWNER_DISABLED           => 'Owner disabled',
    STATUS_ADMIN_DISABLED           => 'Admin disabled',
    STATUS_WAITING_APPROVAL         => 'Not approved',
    STATUS_NO_WORK_LOCATIONS        => 'No working locations',
    STATUS_NO_LANGUAGES             => 'No languages',
    STATUS_NO_MAIN_PIC              => 'No main photo',
    STATUS_EXPIRED                  => 'Expired',
    STATUS_DOMAIN_BLACKLISTED       => 'Domain blacklisted',
    STATUS_DELETED                  => 'Deleted'
	/*STATUS_ACTIVE                   => Sceon_I18n::translate('status_active'),
    STATUS_NO_PHOTOS                => Sceon_I18n::translate('status_no_photos'),
    STATUS_NO_PROFILE               => Sceon_I18n::translate('status_no_profile'),
    STATUS_OWNER_DISABLED           => Sceon_I18n::translate('status_owner_disabled'),
    STATUS_ADMIN_DISABLED           => Sceon_I18n::translate('status_admin_disabled'),
    STATUS_WAITING_APPROVAL         => Sceon_I18n::translate('status_not_approved'),
    STATUS_NO_WORK_LOCATIONS        => Sceon_I18n::translate('status_no_working_locations'),
    STATUS_NO_LANGUAGES             => Sceon_I18n::translate('status_no_languages'),
    STATUS_NO_MAIN_PIC              => Sceon_I18n::translate('status_no_main_photo'),
    STATUS_EXPIRED                  => Sceon_I18n::translate('status_expired'),
    STATUS_DOMAIN_BLACKLISTED       => Sceon_I18n::translate('status_domain_blacklisted'),
    STATUS_DELETED                  => Sceon_I18n::translate('status_deleted')*/
);

define('ESCORT_NOT_VERIFIED',               1);
define('ESCORT_VERIFIED',                   2);
define('ESCORT_REQUESTED_REMOVE',           3);
define('ESCORT_CALLED_1',                   4);
define('ESCORT_CALLED_2',                   5);
define('ESCORT_CALLED_3',                   6);
define('ESCORT_INVALID_NUMBER',             7);
define('ESCORT_REQUESTED_REMOVE_WAVED',     8);
define('ESCORT_SELF_REGISTERED',            9);
define('ESCORT_CALLED_0',                  10);

$DEFINITIONS['ESCORT_VERIFICATION'] = array(
    ESCORT_NOT_VERIFIED             => 'not verified',
    ESCORT_VERIFIED                 => 'verified',
    ESCORT_REQUESTED_REMOVE         => 'requested remove',
    ESCORT_REQUESTED_REMOVE_WAVED   => 'requested remove, waved because new ads',
	ESCORT_CALLED_0                 => 'not called',
    ESCORT_CALLED_1                 => 'called 1 time',
    ESCORT_CALLED_2                 => 'called 2 times',
    ESCORT_CALLED_3                 => 'called 3 times',
    ESCORT_INVALID_NUMBER           => 'invalid number',
    ESCORT_SELF_REGISTERED          => 'self registered'
);

define('AGENCY_NOT_VERIFIED',               1);
define('AGENCY_VERIFIED',                   2);
define('AGENCY_INVALID_CONTACT',            3);
define('AGENCY_CALLED_1',                   4);
define('AGENCY_CALLED_2',                   5);
define('AGENCY_CALLED_0',                   6);

$DEFINITIONS['AGENCY_VERIFICATION'] = array(
    AGENCY_NOT_VERIFIED             => 'not verified',
    AGENCY_VERIFIED                 => 'verified',
    AGENCY_INVALID_CONTACT          => 'invalid contact',
    AGENCY_CALLED_0                 => 'not called',
    AGENCY_CALLED_1                 => 'called 1 time',
    AGENCY_CALLED_2                 => 'called 2 times'

);

// colors
define('COLOR_BLACK',                       1);
define('COLOR_WHITE',                       2);
define('COLOR_RED',                         3);
define('COLOR_BLOND',                       4);
define('COLOR_BROWN',                       5);
define('COLOR_GREEN',                       6);
define('COLOR_YELLOW',                      7);
define('COLOR_BLUE',                        8);
define('COLOR_GRAY',                        9);
define('COLOR_BRUNETTE',                    10);
define('COLOR_HAZEL',                       11);

$DEFINITIONS['hair_color_options'] = array(
	COLOR_BLOND => Sceon_I18n::translate('color_blond'),
	COLOR_BROWN => Sceon_I18n::translate('color_light_brown'),
	COLOR_BRUNETTE => Sceon_I18n::translate('color_brunette'),
	COLOR_BLACK => Sceon_I18n::translate('color_black'),
	COLOR_RED => Sceon_I18n::translate('color_red')
);

define('YES',                               1);
define('NO',                                2);
define('OCASIONALLY',                       3);
define('NOT_AVAILABLE',                     98);
define('DONT_KNOW',                         99);
define('NEGOTIABLE',                        101);

$DEFINITIONS['yes_no_options'] = array(
   YES                              => Sceon_I18n::translate('yes'),
   NO                               => Sceon_I18n::translate('no')
);

$DEFINITIONS['y_n_na_options'] = array(
   YES                              => Sceon_I18n::translate('yes'),
   NO                               => Sceon_I18n::translate('no'),
   NOT_AVAILABLE                    => Sceon_I18n::translate('not_available')
);

$DEFINITIONS['y_n_dn_options'] = array(
   YES                              => Sceon_I18n::translate('yes'),
   NO                               => Sceon_I18n::translate('no'),
   DONT_KNOW						=> Sceon_I18n::translate('dont_know')
);

$DEFINITIONS['y_n_oc_options'] = array(
   YES                              => Sceon_I18n::translate('yes'),
   NO                               => Sceon_I18n::translate('no'),
   OCASIONALLY						=> Sceon_I18n::translate('ocasionally')
);

$DEFINITIONS['y_n_neg_options'] = array(
   NO                               => Sceon_I18n::translate('no'),
   YES                              => Sceon_I18n::translate('yes'),
   NEGOTIABLE						=> Sceon_I18n::translate('negotiable')
);

$DEFINITIONS['eye_color_options'] = array(
	COLOR_BLACK => Sceon_I18n::translate('color_black'),
	COLOR_BROWN => Sceon_I18n::translate('color_brown'),
	COLOR_GREEN => Sceon_I18n::translate('color_green'),
	COLOR_BLUE => Sceon_I18n::translate('color_blue'),
	COLOR_GRAY => Sceon_I18n::translate('color_gray')
);

$DEFINITIONS['breast_size_options'] = array(1 => 'A', 'B', 'C', 'D', 'DD', 'F', 'FF', 'G');

define('PUBIC_HAIR_SHAVED_COMPLETELY',		1);
define('PUBIC_HAIR_SHAVED_MOSTLY',			2);
define('PUBIC_HAIR_TRIMMED',				3);
define('PUBIC_HAIR_ALL_NATURAL',			4);

$DEFINITIONS['pubic_hair_options'] = array(
	PUBIC_HAIR_SHAVED_COMPLETELY	=> Sceon_I18n::translate('pubic_hair_shaved_completely'),
	PUBIC_HAIR_SHAVED_MOSTLY		=> Sceon_I18n::translate('pubic_hair_shaved_mostly'),
	PUBIC_HAIR_TRIMMED				=> Sceon_I18n::translate('pubic_hair_trimmed'),
	PUBIC_HAIR_ALL_NATURAL			=> Sceon_I18n::translate('pubic_hair_all_natural'),
);

// user type
define('USER_TYPE_SINGLE_GIRL',             1);
define('USER_TYPE_AGENCY',                  2);
define('USER_TYPE_MEMBER',                  3);
define('USER_TYPE_CLUB',                    4);

// gender
define('GENDER_FEMALE',                     1);
define('GENDER_MALE',                       2);
define('GENDER_TRANS',                      3);
define('GENDER_DOMINA',                     4);

$DEFINITIONS['gender_options'] = array(
    GENDER_FEMALE                   => Sceon_I18n::translate('female'),
    GENDER_MALE                     => Sceon_I18n::translate('male'),
    GENDER_TRANS                    => Sceon_I18n::translate('trans'),
    //GENDER_DOMINA                   => Sceon_I18n::translate('domina')
);

// Sexual Orientation
define('ORIENTATION_HETEROSEXUAL',           1);
define('ORIENTATION_BISEXUAL',               2);
define('ORIENTATION_HOMOSEXUAL',             3);


$DEFINITIONS['sexual_orientation_options'] = array(
    ORIENTATION_HETEROSEXUAL  => Sceon_I18n::translate('heterosexual'),
    ORIENTATION_BISEXUAL      => Sceon_I18n::translate('bisexual'),
    ORIENTATION_HOMOSEXUAL    => Sceon_I18n::translate('homosexual')
);

define('PHONE_INSTR_SMS_CALL',           1);
define('PHONE_INSTR_SMS_ONLY',           2);
define('PHONE_INSTR_NO_SMS',             3);

define('PHONE_INSTR_NO_WITHHELD',        1);

$DEFINITIONS['phone_instr_options'] = array(
    PHONE_INSTR_SMS_CALL  => Sceon_I18n::translate('phone_instr_sms_call'),
    PHONE_INSTR_SMS_ONLY  => Sceon_I18n::translate('phone_instr_sms_only'),
    PHONE_INSTR_NO_SMS    => Sceon_I18n::translate('phone_instr_no_sms')
);

$DEFINITIONS['phone_instr_no_withheld_options'] = array(
    PHONE_INSTR_NO_WITHHELD  => Sceon_I18n::translate('phone_instr_no_withheld')
);

$incall_types = array(
		'1' => 'private_apartment',
		'2' => 'hotel_room',
		'3' => 'club_studio',
		'4' => ''
	);
	$outcall_types = array(
		'1' => 'hotel_visits_only',
		'2' => 'home_visits_only',
		'3' => 'hotel_and_home_visits',
		'4' => ''
	);

define('AVAIL_INCALL_TYPE_PRIVATE_APARTMENT',       1);
define('AVAIL_INCALL_TYPE_HOTEL_ROOM',				2);
define('AVAIL_INCALL_TYPE_CLUB_STUDIO',				3);
define('AVAIL_INCALL_TYPE_OTHER',					4);

define('AVAIL_OUTCALL_TYPE_HOTEL_VISITS_ONLY',      1);
define('AVAIL_OUTCALL_TYPE_HOME_VISITS_ONLY',       2);
define('AVAIL_OUTCALL_TYPE_HOTEL_HOME_VISITS',      3);
define('AVAIL_OUTCALL_TYPE_OTHER',					4);

$DEFINITIONS['avail_incall_type_options'] = array(
    AVAIL_INCALL_TYPE_PRIVATE_APARTMENT  => Sceon_I18n::translate('private_apartment'),
    AVAIL_INCALL_TYPE_HOTEL_ROOM	=> Sceon_I18n::translate('hotel_room'),
    AVAIL_INCALL_TYPE_CLUB_STUDIO	=> Sceon_I18n::translate('club_studio'),
    AVAIL_INCALL_TYPE_OTHER			=> ''
);

$DEFINITIONS['avail_outcall_type_options'] = array(
    AVAIL_OUTCALL_TYPE_HOTEL_VISITS_ONLY  => Sceon_I18n::translate('hotel_visits_only'),
    AVAIL_OUTCALL_TYPE_HOME_VISITS_ONLY  => Sceon_I18n::translate('home_visits_only'),
    AVAIL_OUTCALL_TYPE_HOTEL_HOME_VISITS  => Sceon_I18n::translate('hotel_and_home_visits'),
    AVAIL_OUTCALL_TYPE_OTHER  => ''
);

define('KISSING_NO',                        11);
define('KISSING',                           12);
define('KISSING_WITH_TONGUE',               13);

define('BLOWJOB_NO',                        11);
define('BLOWJOB_WITH_CONDOM',               12);
define('BLOWJOB_WITHOUT_CONDOM',            13);

define('CUMSHOT_NO',                        11);
define('CUMSHOT_IN_MOUTH_SPIT',             12);
define('CUMSHOT_IN_MOUTH_SWALLOW',          13);
define('CUMSHOT_IN_FACE',                   14);

// metric system
define('METRIC_SYSTEM',                     1);
define('ROYAL_SYSTEM',                      2);


$DEFINITIONS['metric_system'] = array(
    METRIC_SYSTEM               => Sceon_I18n::translate('metric'),
    ROYAL_SYSTEM                => Sceon_I18n::translate('royal')
);

$DEFINITIONS['kissing_options'] = array(
    KISSING_NO                      => Sceon_I18n::translate('no'),
    KISSING                         => Sceon_I18n::translate('yes'),
    KISSING_WITH_TONGUE             => Sceon_I18n::translate('kissing_with_tongue')
);

$DEFINITIONS['blowjob_options'] = array(
    BLOWJOB_NO                      => Sceon_I18n::translate('no'),
    BLOWJOB_WITH_CONDOM             => Sceon_I18n::translate('blowjob_with_condom'),
    BLOWJOB_WITHOUT_CONDOM          => Sceon_I18n::translate('blowjob_without_condom')
);

$DEFINITIONS['cumshot_options'] = array(
    CUMSHOT_NO                      => Sceon_I18n::translate('no'),
    CUMSHOT_IN_MOUTH_SPIT           => Sceon_I18n::translate('cumshot_in_mouth_spit'),
    CUMSHOT_IN_MOUTH_SWALLOW        => Sceon_I18n::translate('cumshot_in_mouth_swallow'),
    CUMSHOT_IN_FACE                 => Sceon_I18n::translate('cumshot_in_face')
);

// time units
define('TIME_MINUTES',                      1);
define('TIME_HOURS',                        2);
define('TIME_DAYS',                         3);
define('TIME_OVERNIGHT',                    4);

$DEFINITIONS['time_unit_options'] = array(
    TIME_MINUTES                    => Sceon_I18n::translate('minutes'),
    TIME_HOURS                      => Sceon_I18n::translate('hours'),
    TIME_DAYS                       => Sceon_I18n::translate('days'),
    TIME_OVERNIGHT                  => Sceon_I18n::translate('time_overnight')
);

$DEFINITIONS['hours_days_options'] = array(
    TIME_HOURS                      => Sceon_I18n::translate('hours'),
    TIME_DAYS                       => Sceon_I18n::translate('days')
);

// ethnic
define('ETHNIC_WHITE',                          1);
define('ETHNIC_BLACK',                          2);
define('ETHNIC_ASIAN',                          3);
define('ETHNIC_LATIN',                          4);
define('ETHNIC_AFRICAN',                        5);
define('ETHNIC_INDIAN',                         6);
define('ETHNIC_ARABIAN',                        8);
define('ETHNIC_MIXED',                        9);

$DEFINITIONS['ethnic_options'] = array(
	ETHNIC_ASIAN                    => Sceon_I18n::translate('ethnic_asian'),
	ETHNIC_BLACK                    => Sceon_I18n::translate('ethnic_black'),
    ETHNIC_WHITE                    => Sceon_I18n::translate('ethnic_white'),
    ETHNIC_LATIN                    => Sceon_I18n::translate('ethnic_latin'),
	ETHNIC_MIXED					=> Sceon_I18n::translate('ethnic_mixed'),
    ETHNIC_INDIAN                   => Sceon_I18n::translate('ethnic_indian'),
    ETHNIC_ARABIAN                   => Sceon_I18n::translate('ethnic_arabian')

    // ETHNIC_AFRICAN                  => Sceon_I18n::translate('ethnic_african'),

);

define('AVAILABLE_INCALL',                  1);
define('AVAILABLE_OUTCALL',                 2);
define('AVAILABLE_INCALL_OUTCALL',          3);
define('AVAILABLE_SAUNA',                   4);
define('AVAILABLE_SALON',                   5);
define('AVAILABLE_APARTMENT',               6);
define('AVAILABLE_LOFT',                    7);

$DEFINITIONS['available_for']               = array(
	AVAILABLE_APARTMENT 			=> Sceon_I18n::translate('apartment'),
	AVAILABLE_INCALL 				=> Sceon_I18n::translate('incall'),
	AVAILABLE_OUTCALL 				=> Sceon_I18n::translate('outcall'),
	AVAILABLE_INCALL_OUTCALL 		=> Sceon_I18n::translate('incall_outcall'),
	AVAILABLE_SAUNA 				=> Sceon_I18n::translate('sauna'),
	AVAILABLE_SALON 				=> Sceon_I18n::translate('salon')
);

define('AVAILABILITY_INCALL_PRIVATE_APARTMENT', 1);
define('AVAILABILITY_INCALL_HOTEL_ROOM', 2);
define('AVAILABILITY_INCALL_CLUB_STUDIO', 3);
define('AVAILABILITY_INCALL_OTHER', 4);
define('AVAILABILITY_OUTCALL_HOTEL_VISITS', 1);
define('AVAILABILITY_OUTCALL_HOME_VISITS', 2);
define('AVAILABILITY_OUTCALL_HOTEL_HOME_VISITS', 3);
define('AVAILABILITY_OUTCALL_OTHER', 4);


$DEFINITIONS['filter_availability_options'] = array(
	'i_' . AVAILABILITY_INCALL_PRIVATE_APARTMENT => Sceon_I18n::translate('filter_incall_private_apartment'),
	'i_' . AVAILABILITY_INCALL_HOTEL_ROOM => Sceon_I18n::translate('filter_incall_hotel_room'),
	'i_' . AVAILABILITY_INCALL_CLUB_STUDIO => Sceon_I18n::translate('filter_incall_club_studio'),

	'o_' . AVAILABILITY_OUTCALL_HOTEL_VISITS => Sceon_I18n::translate('filter_outcall_hotel_visits'),
	'o_' . AVAILABILITY_OUTCALL_HOME_VISITS => Sceon_I18n::translate('filter_outcall_home_visits'),
	'o_' . AVAILABILITY_OUTCALL_HOTEL_HOME_VISITS => Sceon_I18n::translate('filter_outcall_hotel_home_visits'),
);

define('SEXUALLY_AVAILABLE_FOR_MEN', 'men');
define('SEXUALLY_AVAILABLE_FOR_WOMEN', 'women');
define('SEXUALLY_AVAILABLE_FOR_COUPLES', 'couples');
define('SEXUALLY_AVAILABLE_FOR_TRANS', 'trans');
define('SEXUALLY_AVAILABLE_FOR_GAYS', 'gays');
define('SEXUALLY_AVAILABLE_FOR_PLUS2', 'plus2');

$DEFINITIONS['sex_availability_options'] = array(
	SEXUALLY_AVAILABLE_FOR_MEN => Sceon_I18n::translate('men'),
	SEXUALLY_AVAILABLE_FOR_WOMEN => Sceon_I18n::translate('women'),
	SEXUALLY_AVAILABLE_FOR_COUPLES => Sceon_I18n::translate('couples'),
	SEXUALLY_AVAILABLE_FOR_TRANS => Sceon_I18n::translate('trans'),
	SEXUALLY_AVAILABLE_FOR_GAYS => Sceon_I18n::translate('gays'),
	SEXUALLY_AVAILABLE_FOR_PLUS2 => Sceon_I18n::translate('plus2'),
);

define('KEYWORD_VIP_ESCORT',		1);
define('KEYWORD_CENTERFOLD',		2);
define('KEYWORD_MODEL_EX_MODEL',	3);
define('KEYWORD_XXX_PORN_STAR',		4);
define('KEYWORD_SUPER_BUSTY',		5);
define('KEYWORD_SUPER_BOOTY',		6);
define('KEYWORD_PETITE',			7);
define('KEYWORD_ALL_NATURAL',		8);
define('KEYWORD_MATURE',			9);
define('KEYWORD_COLLEGE_GIRL',		10);
define('KEYWORD_PROF_MISTRESS',		11);

$DEFINITIONS['keywords'] = array(
	KEYWORD_VIP_ESCORT => 'VIP Escort',
	KEYWORD_CENTERFOLD => 'Centerfold',
	KEYWORD_MODEL_EX_MODEL => 'Model/Ex-Model',
	KEYWORD_XXX_PORN_STAR => 'XXX Porn Star',
	KEYWORD_SUPER_BUSTY => 'Super Busty',
	KEYWORD_SUPER_BOOTY => 'Super Booty',
	KEYWORD_PETITE => 'Petite',
	KEYWORD_ALL_NATURAL => 'All Natural',
	KEYWORD_MATURE => 'Mature',
	KEYWORD_COLLEGE_GIRL => 'College Girl',
	KEYWORD_PROF_MISTRESS => 'Prof. Mistress'
);

$DEFINITIONS['language_options'] = array(
	'en' => Sceon_I18n::translate('language_en'),
	'fr' => Sceon_I18n::translate('language_fr'),
	'de' => Sceon_I18n::translate('language_de'),
	'it' => Sceon_I18n::translate('language_it'),
	'pt' => Sceon_I18n::translate('language_pt'),
	'ru' => Sceon_I18n::translate('language_ru'),
	'es' => Sceon_I18n::translate('language_es'),
	'sa' => Sceon_I18n::translate('languages_sa'),
	//'be' => Sceon_I18n::translate('languages_be'),
	'bg' => Sceon_I18n::translate('languages_bg'),
	'cn' => Sceon_I18n::translate('languages_cn'),
	'cz' => Sceon_I18n::translate('languages_cz'),
	'hr' => Sceon_I18n::translate('languages_hr'),
	'nl' => Sceon_I18n::translate('languages_nl'),
	'fi' => Sceon_I18n::translate('languages_fi'),
	'gr' => Sceon_I18n::translate('languages_gr'),
	'hu' => Sceon_I18n::translate('languages_hu'),
	'in' => Sceon_I18n::translate('languages_in'),
	'jp' => Sceon_I18n::translate('languages_jp'),
	'lv' => Sceon_I18n::translate('languages_lv'),
	'pl' => Sceon_I18n::translate('languages_pl'),
	'ro' => Sceon_I18n::translate('languages_ro'),
	'tr' => Sceon_I18n::translate('language_tr')
);

$DEFINITIONS['language_level_options'] = array(
	1 => '1 (' . Sceon_I18n::translate('basic') . ')',
	2 => '2',
	3 => '3',
	4 => '4 (' . Sceon_I18n::translate('fluent') . ')'
);

$_db = Zend_Registry::get('db');

$DEFINITIONS['currencies'] = array(
	2 => 'EUR',
	1 => 'USD',
	3 => 'GBP',
	4 => 'CHF'
);

$DEFINITIONS['currency_symbols'] = array(
	1 => '$',
	2 => '&euro;',
	3 => '&pound;',
	4 => 'CHF'
);

$DEFINITIONS['biography'] = array(
	METRIC_SYSTEM => array(
		'height' => range(135, 195),
		'weight' => range(40, 200),
		'dress_size' => array(1 => 'XS', 'S', 'M', 'L', 'XL', 'XXL'),
		'shoe_size' => range(34, 46)
	),
	ROYAL_SYSTEM => array(
		'height' => array(), //range(135, 195),
		'weight' => range(80, 264), //range(40, 200),
		'dress_size' => array(1 => 'XS', 'S', 'M', 'L', 'XL', 'XXL'),
		'shoe_size' => range(34, 46)
	)
);

for ( $ft = 4; $ft <= 6; $ft++ ) {
	for ( $in = 1; $in <= 11; $in++ ) {
		$DEFINITIONS['biography'][ROYAL_SYSTEM]['height'][] = $ft . '\' ' . $in . '"';
	}
}


define('FLIGHT_RESERVATION_ESCORT', 1);
define('FLIGHT_RESERVATION_CUSTOMER', 2);
define('FLIGHT_RESERVATION_NEGOTIABLE', 3);

$DEFINITIONS['flight_reservation_by'] = array(
	FLIGHT_RESERVATION_ESCORT => Sceon_I18n::translate('by_escort'),
	FLIGHT_RESERVATION_CUSTOMER => Sceon_I18n::translate('by_customer'),
	FLIGHT_RESERVATION_NEGOTIABLE => Sceon_I18n::translate('negotiable')
);

define('ESCORT_PHOTO_TYPE_HARD', 1);
define('ESCORT_PHOTO_TYPE_SOFT', 2);
define('ESCORT_PHOTO_TYPE_PRIVATE', 3);
define('ESCORT_PHOTO_TYPE_DISABLED', 4);
define('ESCORT_PHOTO_TYPE_ARCHIVED', 5);


define('MTS_NO_ONLY_ONCE',   1);
define('MTS_YES_MORE_TIMES',       2);
define('MTS_DONT_KNOW',		99);

$DEFINITIONS['mts_options'] = array(
	MTS_NO_ONLY_ONCE => Sceon_I18n::translate('no_only_once'),
	MTS_YES_MORE_TIMES => Sceon_I18n::translate('yes_more_times'),
	MTS_DONT_KNOW => Sceon_I18n::translate('dont_know')
);


// review meeting places
define('MEETING_AT_MY_FLAT_OR_HOUSE',       11);
define('MEETING_AT_HER_APARTMENT',          12);
define('MEETING_AT_HER_HOTEL',              13);
define('MEETING_AT_MY_HOTEL',               14);
define('MEETING_SOMEWHERE_ELSE',            15);

$DEFINITIONS['meeting_place_options'] = array(
    MEETING_AT_MY_FLAT_OR_HOUSE     => Sceon_I18n::translate('at_my_flat'),
    MEETING_AT_HER_APARTMENT        => Sceon_I18n::translate('at_her_apartament'),
    MEETING_AT_HER_HOTEL			=> Sceon_I18n::translate('her_hotel'),
    MEETING_AT_MY_HOTEL             => Sceon_I18n::translate('my_hotel'),
    MEETING_SOMEWHERE_ELSE          => Sceon_I18n::translate('somewhere_else'),
);

define('SEX_ENTHUSIASTIC',                  11);
define('SEX_PASSIVE',                       12);
define('SEX_ACTIVE',                        13);

$DEFINITIONS['sex_options'] = array(
    SEX_ENTHUSIASTIC		=> Sceon_I18n::translate('enthusiastic'),
    SEX_PASSIVE				=> Sceon_I18n::translate('passive'),
    SEX_ACTIVE				=> Sceon_I18n::translate('active')
);

define('ATTITUDE_FRIENDLY',                 11);
define('ATTITUDE_UNFRIENDLY',               12);
define('ATTITUDE_POSHY',                    13);
define('ATTITUDE_NORMAL',                   14);

$DEFINITIONS['attitude_options'] = array(
    ATTITUDE_FRIENDLY				=> Sceon_I18n::translate('friendly'),
    ATTITUDE_UNFRIENDLY				=> Sceon_I18n::translate('unfriendly'),
    ATTITUDE_POSHY					=> Sceon_I18n::translate('poshy'),
    ATTITUDE_NORMAL					=> Sceon_I18n::translate('normal')
);

define('CONVERSATION_LANGUAGE_PROBLEMS',    11);
define('CONVERSATION_VERY_SIMPLE',          12);
define('CONVERSATION_NORMAL',               13);
define('CONVERSATION_INTELLIGENT',          14);

$DEFINITIONS['conversation_options'] = array(
    CONVERSATION_LANGUAGE_PROBLEMS				=> Sceon_I18n::translate('language_problems'),
    CONVERSATION_VERY_SIMPLE					=> Sceon_I18n::translate('very_simple'),
    CONVERSATION_NORMAL							=> Sceon_I18n::translate('normal'),
    CONVERSATION_INTELLIGENT					=> Sceon_I18n::translate('intelligent')
);

define('BREAST_REAL',                       11);
define('BREAST_SILICON',                    12);
define('BREAST_DONT_KNOW',                  99);

$DEFINITIONS['breast_options'] = array(
    BREAST_REAL				=> Sceon_I18n::translate('real'),
    BREAST_SILICON			=> Sceon_I18n::translate('silicon'),
    BREAST_DONT_KNOW		=> Sceon_I18n::translate('dont_know')
);

define('AVAILABILITY_EASY',                 11);
define('AVAILABILITY_NORMAL',               12);
define('AVAILABILITY_HARD',                 13);
define('AVAILABILITY_SUCKS',                14);

$DEFINITIONS['availability_options'] = array(
    AVAILABILITY_EASY				=> Sceon_I18n::translate('easy'),
    AVAILABILITY_NORMAL				=> Sceon_I18n::translate('normal'),
    AVAILABILITY_HARD				=> Sceon_I18n::translate('hard'),
    AVAILABILITY_SUCKS				=> Sceon_I18n::translate('sucks')
);

define('PHOTO_GENUINE',                     11);
define('PHOTO_FAKE',                        12);
define('PHOTO_HARD_TO_BELIEVE',             13);

$DEFINITIONS['photos_options'] = array(
    PHOTO_GENUINE				=> Sceon_I18n::translate('genuine'),
    PHOTO_FAKE					=> Sceon_I18n::translate('fake'),
    PHOTO_HARD_TO_BELIEVE		=> Sceon_I18n::translate('hard_to_belive')
);

// comments statuses
define('COMMENT_ACTIVE',                     1);
define('COMMENT_NOT_APPROVED',              -3);
define('COMMENT_DISABLED',					-4);
define('COMMENT_SYSTEM_DISABLED',			-5);

$DEFINITIONS['comment_status_options'] = array(
    COMMENT_ACTIVE				=> Sceon_I18n::translate('active'),
    COMMENT_NOT_APPROVED		=> Sceon_I18n::translate('not_approved'),
    COMMENT_DISABLED			=> Sceon_I18n::translate('disabled'),
    COMMENT_SYSTEM_DISABLED		=> 'System Disabled'
);

$DEFINITIONS['services'] = require(dirname(__FILE__) . '/services.php');


$DEFINITIONS['services_flat'] = array();
foreach ( $DEFINITIONS['services'] as $svcs ) {
	$DEFINITIONS['services_flat'] = array_merge($DEFINITIONS['services_flat'], $svcs);
}

define('PHONE_INSTRUCTIONS_SMS_CALL', 1);
define('PHONE_INSTRUCTIONS_SMS_ONLY', 2);
define('PHONE_INSTRUCTIONS_NO_SMS', 3);

$DEFINITIONS['phone_instructions'] = array(
	PHONE_INSTRUCTIONS_SMS_CALL => Sceon_I18n::translate('phone_instr_sms_call'),
	PHONE_INSTRUCTIONS_SMS_ONLY => Sceon_I18n::translate('phone_instr_sms_only'),
	PHONE_INSTRUCTIONS_NO_SMS => Sceon_I18n::translate('phone_instr_no_sms')
);

/**
 * US Phone Number RegEx
 *  '/^(?:1(?:[. -])?)?(?:\((?=\d{3}\)))?([2-9]\d{2})'
 * .'(?:(?<=\(\d{3})\))? ?(?:(?<=\d{3})[.-])?([2-9]\d{2})'
 * .'[. -]?(\d{4})(?: (?i:ext)\.? ?(\d{1,5}))?$/'
 */

define('FUCKOMETER_TRUSED_USER_REVIEWS', 4);
define('TOTAL_FUCKOMETER_USER_MIN_REVIEWS', 1);

//sms info
define('SMS_USERKEY', 'GCK68SF3ZSIM');
define('SMS_PASSWORD', '658152');
define('SMS_DeliveryNotificationURL', 'http://backend.6annonce.com/sms/index/sms-callback?status=1&id=');
define('SMS_NonDeliveryNotificationURL', 'http://backend.6annonce.com/sms/index/sms-callback?status=2&id=');
define('SMS_BufferedNotificationURL', 'http://backend.6annonce.com/sms/index/sms-callback?status=3&id=');

/* Alert me */
define('ALERT_ME_NEW_COMMENT', 1);
define('ALERT_ME_MODIFICATION_ON_PROFILE', 2);
define('ALERT_ME_NEW_PICTURES', 3);
define('ALERT_ME_NEW_REVIEW', 4);
define('ALERT_ME_ANY_CHANGE', 5);
define('ALERT_ME_CITY', 6);

$DEFINITIONS['alert_me'] = array(
	ALERT_ME_NEW_COMMENT => Sceon_I18n::translate('alm_new_comment'),
	ALERT_ME_MODIFICATION_ON_PROFILE => Sceon_I18n::translate('alm_mod_profile'),
	ALERT_ME_NEW_PICTURES => Sceon_I18n::translate('alm_new_pictures'),
	ALERT_ME_NEW_REVIEW => Sceon_I18n::translate('alm_new_review'),
	ALERT_ME_ANY_CHANGE => Sceon_I18n::translate('alm_any_change'),
	ALERT_ME_CITY => Sceon_I18n::translate('alm_city')
);
//

/* Booking Contact */
define('BOOKIN_CONTACT_PHONE', 1);
define('BOOKIN_CONTACT_EMAIL', 2);
define('BOOKIN_CONTACT_NO_MATTER', 3);

$DEFINITIONS['booking_contact'] = array(
	BOOKIN_CONTACT_PHONE => 'By Phone',
	BOOKIN_CONTACT_EMAIL => 'By Email',
	BOOKIN_CONTACT_NO_MATTER => 'Doesn\'t matter'
);
//

/* Booking Preferred Time */
define('BOOKIN_PREFERRED_TIME_MORNING', 1);
define('BOOKIN_PREFERRED_TIME_AFTERNOON', 2);
define('BOOKIN_PREFERRED_TIME_EVENING', 3);
define('BOOKIN_PREFERRED_TIME_NO_MATTER', 4);

$DEFINITIONS['booking_preferred_time'] = array(
	BOOKIN_PREFERRED_TIME_MORNING => 'Morning',
	BOOKIN_PREFERRED_TIME_AFTERNOON => 'Afternoon',
	BOOKIN_PREFERRED_TIME_EVENING => 'Evening',
	BOOKIN_PREFERRED_TIME_NO_MATTER => 'Doesn\'t matter'
);
//

/* Travel Places EM */
define('TRAVEL_WORLDWIDE_I_USA', 1);
define('TRAVEL_WORLDWIDE_E_USA', 2);
define('TRAVEL_EUROPE', 3);

$DEFINITIONS['travel_places'] = array(
	TRAVEL_WORLDWIDE_I_USA => 'Worldwide (including USA)',
	TRAVEL_WORLDWIDE_E_USA => 'Worldwide (excluding USA)',
	TRAVEL_EUROPE => 'Europe only'
);
//

/* Travel Places PC */
define('WORLDWIDE', 1);
define('USA_AND_CANADA', 2);
define('USA_ONLY', 3);

$DEFINITIONS['travel_places_pc'] = array(
	WORLDWIDE => 'Worldwide',
	USA_AND_CANADA => 'USA and Canada',
	USA_ONLY => 'USA only'
);
//

/* Feedbacks */
define('FEEDBACK_FLAG_SENT', 0);
define('FEEDBACK_FLAG_BLOCKED_BY_WORD', 1);
define('FEEDBACK_FLAG_BLOCKED_BY_EMAIL', 2);
define('FEEDBACK_FLAG_NOT_SEND', 3);

$DEFINITIONS['feedback_flags'] = array(
	FEEDBACK_FLAG_NOT_SEND => 'Not Send',
	FEEDBACK_FLAG_SENT => 'Sent',
	FEEDBACK_FLAG_BLOCKED_BY_WORD => 'Blocked By Word',
	FEEDBACK_FLAG_BLOCKED_BY_EMAIL => 'Blocked By Email'
);

define('FEEDBACK_STATUS_NOT_APPROVED', 1);
define('FEEDBACK_STATUS_APPROVED', 2);
define('FEEDBACK_STATUS_DISABLED', 3);

$DEFINITIONS['feedback_status'] = array(
	FEEDBACK_STATUS_NOT_APPROVED => 'Not Approved',
	FEEDBACK_STATUS_APPROVED => 'Approved',
	FEEDBACK_STATUS_DISABLED => 'Disabled'
);

/* Private messages */
define('PM_STATUS_NOT_APPROVED', 1);
define('PM_STATUS_APPROVED', 2);
define('PM_STATUS_DISABLED', 3);
$DEFINITIONS['pm_status'] = array(
	PM_STATUS_NOT_APPROVED => 'Not Approved',
	PM_STATUS_APPROVED => 'Approved',
	PM_STATUS_DISABLED => 'Disabled'
);

define('PM_FLAG_SENT', 0);
define('PM_FLAG_BLOCKED_BY_WORD', 1);
define('PM_FLAG_BLOCKED_BY_USER', 2);

$DEFINITIONS['pm_flags'] = array(
	PM_FLAG_SENT => 'Sent',
	PM_FLAG_BLOCKED_BY_WORD => 'Blocked By Word',
	PM_FLAG_BLOCKED_BY_USER => 'Blocked By User'
);
//

/* Club */
define('CLUB_ENTRANCE_NA', 0);
define('CLUB_ENTRANCE_FREE', 1);
define('CLUB_ENTRANCE_NOT_FREE', 2);
define('CLUB_WELLNESS_NA', 0);
define('CLUB_WELLNESS_YES', 1);
define('CLUB_WELLNESS_NO', 2);
define('CLUB_WELLNESS_FREE', 3);
define('CLUB_WELLNESS_NOT_FREE', 4);
define('CLUB_FOOD_NA', 0);
define('CLUB_FOOD_YES', 1);
define('CLUB_FOOD_NO', 2);
define('CLUB_FOOD_FREE', 3);
define('CLUB_FOOD_NOT_FREE', 4);
define('CLUB_OUTDOOR_NA', 0);
define('CLUB_OUTDOOR_YES', 1);
define('CLUB_OUTDOOR_NO', 2);

$DEFINITIONS['club_entrance_status'] = array(
	CLUB_ENTRANCE_NA => 'N/A',
	CLUB_ENTRANCE_FREE => Sceon_I18n::translate('free'),
	CLUB_ENTRANCE_NOT_FREE => Sceon_I18n::translate('with_cost'),
);
$DEFINITIONS['club_wellness_status'] = array(
	CLUB_WELLNESS_NA => 'N/A',
	CLUB_WELLNESS_YES => Sceon_I18n::translate('yes'),
	CLUB_WELLNESS_NO => Sceon_I18n::translate('no'),
	CLUB_WELLNESS_FREE => Sceon_I18n::translate('free'),
	CLUB_WELLNESS_NOT_FREE => Sceon_I18n::translate('with_cost'),
);
$DEFINITIONS['club_food_status'] = array(
	CLUB_FOOD_NA => 'N/A',
	CLUB_FOOD_YES => Sceon_I18n::translate('yes'),
	CLUB_FOOD_NO => Sceon_I18n::translate('no'),
	CLUB_FOOD_FREE => Sceon_I18n::translate('free'),
	CLUB_FOOD_NOT_FREE => Sceon_I18n::translate('with_cost'),
);
$DEFINITIONS['club_outdoor_status'] = array(
	CLUB_OUTDOOR_NA => 'N/A',
	CLUB_OUTDOOR_YES => Sceon_I18n::translate('yes'),
	CLUB_OUTDOOR_NO => Sceon_I18n::translate('no'),
);
//

/* reciprocal links */
define('RECIPROCAL_LINK_HAS_NOT_BANNER', 0);
define('RECIPROCAL_LINK_HAS_BANNER', 1);

$DEFINITIONS['reciprocal_links_status'] = array(
	RECIPROCAL_LINK_HAS_NOT_BANNER => 'Has Not Banner',
	RECIPROCAL_LINK_HAS_BANNER => 'Has Banner'
);
/**/

/* agency filter criteria */
define('AGENCY_FILTER_CRITERIA_CLUB', 1);
define('AGENCY_FILTER_CRITERIA_KONTAKTBAR', 2);
define('AGENCY_FILTER_CRITERIA_LAUFHAUS', 3);
define('AGENCY_FILTER_CRITERIA_NIGHTCLUB', 4);

$DEFINITIONS['agency_filter_criteria'] = array(
	AGENCY_FILTER_CRITERIA_CLUB => Sceon_I18n::translate('club_studio'),
	AGENCY_FILTER_CRITERIA_KONTAKTBAR => Sceon_I18n::translate('kontaktbar'),
	AGENCY_FILTER_CRITERIA_LAUFHAUS => Sceon_I18n::translate('laufhaus'),
	AGENCY_FILTER_CRITERIA_NIGHTCLUB => Sceon_I18n::translate('nightclub')
);
/**/

/* report problem status */
define('REPORT_PROBLEM_STATUS_PENDING', 1);
define('REPORT_PROBLEM_STATUS_CHECKED', 2);
define('REPORT_PROBLEM_STATUS_REPLIED', 3);

$DEFINITIONS['report_problem_status'] = array(
	REPORT_PROBLEM_STATUS_PENDING => 'Pending',
	REPORT_PROBLEM_STATUS_CHECKED => 'Checked',
	REPORT_PROBLEM_STATUS_REPLIED => 'Replied'
);
/**/

define('C_ADS_SHE_SEEKS',			1);
define('C_ADS_HE_SEEKS',			2);
define('C_ADS_COUPLE_SEEKS',		3);
define('C_ADS_TS_TV',				4);
define('C_ADS_JOBS',				5);
define('C_ADS_FLATS_PROPERTIES',	6);
define('C_ADS_MISCELLANEOUS',		7);

define('C_ADS_PARTY',				8);
define('C_ADS_GANG_BANG',			9);

$DEFINITIONS['classified_ad_categories'] = array(
    C_ADS_SHE_SEEKS			=> Sceon_I18n::translate('she_seeks'),
    C_ADS_HE_SEEKS			=> Sceon_I18n::translate('he_seeks'),
    C_ADS_COUPLE_SEEKS      => Sceon_I18n::translate('couple_seeks'),
    C_ADS_TS_TV				=> Sceon_I18n::translate('ts_tv'),
    C_ADS_JOBS				=> Sceon_I18n::translate('jobs'),
    C_ADS_FLATS_PROPERTIES	=> Sceon_I18n::translate('flat_properties'),
    C_ADS_MISCELLANEOUS		=> Sceon_I18n::translate('miscellaneous')
);

$DEFINITIONS['classified_ad_categories_bl'] = array(
    C_ADS_SHE_SEEKS			=> Sceon_I18n::translate('she_seeks'),
    C_ADS_HE_SEEKS			=> Sceon_I18n::translate('he_seeks'),
    C_ADS_COUPLE_SEEKS      => Sceon_I18n::translate('couple_seeks'),
    C_ADS_TS_TV				=> Sceon_I18n::translate('ts_tv'),
    C_ADS_JOBS				=> Sceon_I18n::translate('jobs'),
    C_ADS_FLATS_PROPERTIES	=> Sceon_I18n::translate('flat_properties'),
    C_ADS_MISCELLANEOUS		=> Sceon_I18n::translate('miscellaneous'),
    C_ADS_PARTY				=> Sceon_I18n::translate('events_parties'),
    C_ADS_GANG_BANG			=> Sceon_I18n::translate('gang_bang')
);

define('C_ADS_DAYS_3',	3);
define('C_ADS_DAYS_10',	10);
define('C_ADS_DAYS_15',	15);
define('C_ADS_DAYS_30',	30);

$DEFINITIONS['classified_ad_durations'] = array(
    C_ADS_DAYS_3	=> Sceon_I18n::translate('3_days'),
    C_ADS_DAYS_10	=> Sceon_I18n::translate('10_days'),
    C_ADS_DAYS_15   => Sceon_I18n::translate('15_days'),
    C_ADS_DAYS_30	=> Sceon_I18n::translate('30_days')
);

define('C_ADS_PACKAGE_FREE',	'free');
define('C_ADS_PACKAGE_1_DAY',	1);
define('C_ADS_PACKAGE_3_DAY',	2);

$DEFINITIONS['classified_ad_packages_arr'] = array(
    C_ADS_PACKAGE_FREE,
    C_ADS_PACKAGE_1_DAY,
    C_ADS_PACKAGE_3_DAY
);

$DEFINITIONS['classified_ad_packages'] = array(
    C_ADS_PACKAGE_1_DAY	=> array('days' => 1, 'price' => 19.00),
    C_ADS_PACKAGE_3_DAY	=> array('days' => 3, 'price' => 49.00)
);


$DEFINITIONS['latest_actions_titles'] = array(
	Cubix_LatestActions::ACTION_NEW_PHOTO		=> Sceon_I18n::translate('la_has_new_photo'),
    Cubix_LatestActions::ACTION_PROFILE_UPDATE	=> Sceon_I18n::translate('la_profile_update'),
	Cubix_LatestActions::ACTION_TOUR_UPDATE		=> Sceon_I18n::translate('la_tour_update'),
	Cubix_LatestActions::ACTION_NEW_REVIEW		=> Sceon_I18n::translate('la_new_review'),
	Cubix_LatestActions::ACTION_NEW_COMMENT		=> Sceon_I18n::translate('la_new_comment')
);

$app_id = Zend_Controller_Front::getInstance()->getRequest()->application_id;
if ( $app_id == APP_BL ) {
	define('GIRL_OF_THE_DAY', 16);
} elseif($app_id == APP_EG_CO_UK){
    define('GIRL_OF_THE_DAY', 18);
    define('GIRL_OF_THE_DAY_WITH_PACKAGE_PRICE', 5);
    define('GIRL_OF_THE_DAY_WITHOUT_PACKAGE_PRICE', 10);
    define('BOY_OF_THE_DAY_WITH_PACKAGE_PRICE', 5);
    define('BOY_OF_THE_DAY_WITHOUT_PACKAGE_PRICE', 9);
     define('FREE_PACKAGES', [ 43, 44, 95, 105, 106 ]);
}elseif($app_id == APP_ED){
    define('GIRL_OF_THE_DAY', 16);
    define('GIRL_OF_THE_DAY_WITH_PACKAGE_PRICE', 8);
    define('GIRL_OF_THE_DAY_WITHOUT_PACKAGE_PRICE', 14);
    define('BOY_OF_THE_DAY_WITH_PACKAGE_PRICE', 8);
    define('BOY_OF_THE_DAY_WITHOUT_PACKAGE_PRICE', 14);
}else{
	define('GIRL_OF_THE_DAY', 15);
}

Zend_Registry::set('defines', $DEFINITIONS);
if ($app_id == APP_EG_CO_UK) {
    define('BUBBLES_PER_PAGE_COUNT', 20);
}

define('SUPPORT_TICKET_SALES_CHANGE','Support ticket sales was reassigned');

/*DISCOUNTS*/
define('DISCOUNT_FOR_EVERYONE', 1);
define('DISCOUNT_FOR_INDEPENDENTS_ESCORTS', 2);
define('DISCOUNT_FOR_AGENCY_ESCORTS', 3);
define('DISCOUNT_FOR_VERIFIED_PHOTOS', 4);
define('DISCOUNT_FOR_ACTIVE_VIDEO',	5);
define('DISCOUNT_FOR_CONFIRMED_PHONE_NUMBER',	6);

$DEFINITIONS['DISCOUNT_TYPES'] = array(
    DISCOUNT_FOR_EVERYONE	            => 'For Everyone',
    DISCOUNT_FOR_INDEPENDENTS_ESCORTS   => 'For Independents',
    DISCOUNT_FOR_AGENCY_ESCORTS         => 'For Agency Escorts',
    DISCOUNT_FOR_VERIFIED_PHOTOS        => 'Photos Verified',
    DISCOUNT_FOR_ACTIVE_VIDEO	        => 'Active Videos',
    DISCOUNT_FOR_CONFIRMED_PHONE_NUMBER	=> 'Phone Number Confirmed',
);

define('SALES_CHANGED_BY_ESCORT_MODIFIER_ID',                2021);
/**/


/*PromoCode*/
define('PROMO_CODE_STATUS_ACTIVE', 1);
define('PROMO_CODE_STATUS_SPENT', 2);
/**/