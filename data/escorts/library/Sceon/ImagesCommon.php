<?php

class Sceon_ImagesCommon
{
	const ERROR_IMAGE_EXT_NOT_ALLOWED = 1;
	const ERROR_IMAGE_INVALID = 2;
	const ERROR_IMAGE_TOO_SMALL = 3;
	const ERROR_IMAGE_TOO_BIG = 4;
	const ERROR_IMAGE_COUNT_LIMIT_OVER = 5;
	
	/**
	 * @var Sceon_Images_Storage_Ftp
	 */
	protected $_storage;
	
	/**
	 * The host of the current application
	 *
	 * @var string
	 */
	protected $_application;
	protected $_application_id;

	protected $_db;

	/**
	 * Configuration from application init
	 *
	 * @var array
	 */
	protected $_config;
	
	public function __construct($adapter = 'ftp')
	{
		$adapters = array('ftp', 'file');

		if ( ! in_array($adapter, $adapters) ) {
			throw new Exception('Wrong store adapter specified');
		}

		$this->_db = Zend_Registry::get('db');

		$this->_config = $config = Zend_Registry::get('images_config');

		if ( $adapter == 'ftp' ) {
			$this->_storage = new Sceon_Images_Storage_Ftp($config['remote']['ftp']);
		} else if ( $adapter == 'file' ) {
			$this->_storage = new Sceon_Images_Storage_File($config['local']['file']['path']);
		}

		$this->_application = Sceon_Application::getById(Sceon_Application::getId())->host;
		$this->_application_id = Sceon_Application::getId();
	}
	
	public function save($file)
	{
		if ( ! strlen($file['tmp_name']) ) return false;
		
		$image_size = getimagesize($file['tmp_name']);
		
		if ( ! $image_size ) {
			throw new Exception(Sceon_I18n::translate('sys_error_upload_img_invalid_image'));
		}
		
		$is_portrait = 0;
		if ( $image_size ) {
			if ( $image_size[0] < $image_size[1] ) {
				$is_portrait = 1;
			}
		}
		
		$img_ext = strtolower(@end(explode('.', $file[name])));
		
		if ( !in_array( $img_ext , $this->_config['allowedExts']) ) {
			throw new Exception(Sceon_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $this->_config['allowedExts']))), self::ERROR_IMAGE_EXT_NOT_ALLOWED);
		}
		
		$image_data = array(
			'hash' => $this->_getHash($file['tmp_name']),
			'ext' => $img_ext,
			'is_portrait' => $is_portrait,
			'width' => $image_size[0],
			'height' => $image_size[1]
		);
	
		$image = $this->_saveToDB($image_data);
		
		if ( is_array($image) ) {
			$this->_storeToPicServer($image, $file['tmp_name']);
		}
		
		return $image;
	}
	
	protected function _getHash($file_name)
	{
		return md5(microtime(TRUE) * mt_rand(1, 1000000));
	}
	
	protected function _saveToDB($image_data)
	{
		$this->_db->insert('images', $image_data);

		$image_id = $this->_db->lastInsertId();

		if ( ! $image_id ) {
			throw new Exception('Error inserting image in images table');
		}
		
		$image_data = array_merge($image_data, array('image_id' => $image_id, 'application_id' => $this->_application_id));	
		
		return $image_data;
	}
	
	protected function _storeToPicServer($image, $file)
	{		
		$remote = $this->_getFullPath(new Sceon_Images_Entry(array(
			'application_id' => $this->application_id,
			'catalog_id' => $image['image_id'],
			'hash' => $image['hash'],
			'ext' => $image['ext']
		)));
		
		if ( ! is_null($application_id) ) {
			$application = Sceon_Application::getById($application_id)->host;
		}
		else {
			$application = $this->_application;
		}
		
		$dirs = array();
		$i = 0;
		$dir = $remote;
		while ( ($dir = dirname($dir)) != '/' . $application ) {
			$i++;
			$dirs[] = $dir;
			if ( $i > 100 ) die('error');
		}
		
		// Connect to ftp
		$this->_storage->connect();
		
		// Create directories if they does not exist
		$dirs = array_reverse($dirs);
		
		foreach ( $dirs as $dir ) {
			try {
				$this->_storage->makeDir($dir);
			} catch (Sceon_Images_Storage_Exception $e) {
				//echo('Probably folder already exists');
			}
		}
		
		// Resize image on the local server
		if ( in_array($ext, $this->_config['allowedExts']) ) {
			$file = $this->_resize($file, $this->application_id);
		}
		// Save the local file on remote storage
		$this->_storage->store($file, $remote);
		$this->_storage->disconnect();
		
		return array('hash' => $hash, 'ext' => $ext);
	}
	
	protected function _getFullPath(Sceon_Images_Entry $entry)
	{
		$application = $this->_application;
		
		if ( isset($entry->application_id) ) {
			$application = Sceon_Application::getById($entry->application_id)->host;
		}
				
		$catalog = $entry->getHash();		
		
		$parts = array();

		if ( strlen($catalog) > 2 ) {
			$parts[] = substr($catalog, 0, 2);
			$parts[] = substr($catalog, 2, 2);
			$parts[] = substr($catalog, 4, 2);
		}		
		
		$catalog = implode('/', $parts);

		$path = '/' . $application . '/' . 'common/' . $catalog . '/' . $entry->getHash() . (isset($entry->size) ? '_' . $entry->getSize() : '') . '.' . $entry->getExt();
		
		return $path;
	}
	
	protected function _resize($file, $application_id)
	{
		if ( ! is_file($file) ) {
			throw new Exception(Sceon_I18n::translate('sys_error_file_does_not_exist', array('file' => $file)));
		}
		
		// Extract the filename and extension from full file path
		$file_name = basename($file);
		$file_name = explode('.', $file_name);
		$file_ext = strtolower(end($file_name));
		$file_name = implode('.', array_slice($file_name, 0, count($file_name) - 1));
		
		// Make sure the extension is allowed
		if ( ! in_array($file_ext, $this->_config['allowedExts']) ) {
			throw new Exception(Sceon_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $this->_config['allowedExts']))), self::ERROR_IMAGE_EXT_NOT_ALLOWED);
		}
		
		// Make sure that the image is valid and get it's width, height and type
		$img_info = @getimagesize($file);
		list($width, $height, $image_type) = $img_info;
		
		if ( FALSE === $img_info ) {
			throw new Exception(Sceon_I18n::translate('sys_error_upload_img_only_images'), self::ERROR_IMAGE_INVALID);
		}
		
		$max_sidesize = $this->_config['maxImageSize'];
		$min_sidesize = $this->_config['minImageSize'];

		if ( $width < $min_sidesize || $height < $min_sidesize ) {
			throw new Exception(Sceon_I18n::translate('sys_error_upload_img_size_min', array('min_sidesize' => $min_sidesize)), self::ERROR_IMAGE_TOO_SMALL);
		}
		
		if ( $width > $max_sidesize || $height > $max_sidesize ) {
			throw new Exception(Sceon_I18n::translate('sys_error_upload_img_size_max', array('max_sidesize' => $max_sidesize)), self::ERROR_IMAGE_TOO_BIG);
		}
		
		// Get original file extension, it's size and quality from config
		$dest_bigside = $this->_config['origBigSide'];
		$dest_ext = $this->_config['destExt'];
		$dest_quality = $this->_config['origQuality'];
		
		// Get the gd function extension from image type constant
		$ext = $this->_gdExtFromImageInfo($image_type);
		$ext = $ext['ext'];
		
		// Load uploaded image
		$func = 'imagecreatefrom' . $ext;
		
		$gd = @$func($file);
		
		if ( FALSE === $gd ) {
			throw new Exception(Sceon_I18n::translate('sys_error_upload_img_gd_error'));
		}
		
		// Calculate rational image size
		if ( $width < $height ) {
			$dest_width = $dest_bigside;
			$dest_height = $height * ($dest_width / $width);
		}
		else {
			$dest_height = $dest_bigside;
			$dest_width = $width * ($dest_height / $height);
		}
		
		if ( $dest_width > $width ) {
			$dest_width = $width;
		}
		
		if ( $dest_height > $height ) {
			$dest_height = $height;
		}
		
		$dest_width = intval($dest_width);
		$dest_height = intval($dest_height);
		
		// Resize the image
		$dst_gd = @imagecreatetruecolor($dest_width, $dest_height);
		@imagecopyresampled($dst_gd, $gd, 0, 0, 0, 0, $dest_width, $dest_height, $width, $height);
		
		// Get gd extension from destination file extension and save the resized image
		$ext = $this->_gdExtFromFileExt($dest_ext);
		$ext = $ext['ext'];
		$func = 'image' . $ext;
		
		//$dst_file = preg_replace('/\..+$/i', '_resized.' . $dest_ext, $file);
		$dst_file = $file;
		$func($dst_gd, $dst_file, ($ext == 'jpeg') ? $dest_quality : null);
		
		@imagedestroy($gd);
		@imagedestroy($dst_gd);
		
		return $dst_file;
	}
	
	protected function _gdExtFromFileExt($file_ext)
	{
		switch ($file_ext) {
			case 'jpg':
			case 'jpeg':
				$mime = 'image/jpeg';
				$ext = 'jpeg';
			break;
			case 'png':
				$mime = 'image/png';
				$ext = 'png';
			break;
			case 'gif':
				$mime = 'image/gif';
				$ext = 'gif';
			break;
		}
		
		if ( ! isset($mime) || ! isset($ext) ) {
			throw new Exception(Sceon_I18n::translate('sys_error_unknown_extension', array('extension' => $file_ext)));
		}
		
		return array('mime' => $mime, 'ext' => $ext);
	}
	
	protected function _gdExtFromImageInfo($image_type)
	{
		switch ($image_type) {
			case IMAGETYPE_JPEG:
				$mime = 'image/jpeg';
				$ext = 'jpeg';
			break;
			case IMAGETYPE_PNG:
				$mime = 'image/png';
				$ext = 'png';
			break;
			case IMAGETYPE_GIF:
				$mime = 'image/gif';
				$ext = 'gif';
			break;
		}
		
		if ( ! isset($mime) || ! isset($ext) ) {
			throw new Exception(Sceon_I18n::translate('sys_error_unknown_image_type', array('image_type' => $image_type)));
		}
		
		return array('mime' => $mime, 'ext' => $ext);
	}
	
	public function getUrl(Sceon_Images_Entry $entry)
	{
		return $this->_clusterizeUrl($entry,
			trim($this->_config['remote']['url'], '/') .
			$this->_getFullPath($entry)
		);
	}
	
	public function getServerUrl(Sceon_Images_Entry $entry)
	{
		return $this->_clusterizeUrl($entry, 
			trim($this->_config['remote']['serverUrl'], '/') .
			$this->_getFullPath($entry)
		);
	}
	
	/**
	 * If there is a config for clustering image server this function will
	 * append a random number between 1 and specified one in config file
	 * If there is no config the url will not be affected
	 *
	 * @param string $url
	 * @return string The resulting url
	 */
	private function _clusterizeUrl($entry, $url)
	{
		if ( isset($this->_config['remote']['clusterize']) &&
				$this->_config['remote']['clusterize'] &&
				isset($this->_config['remote']['cluster_max']) &&
				($max = $this->_config['remote']['cluster_max']) > 0 ) {
			$url = preg_replace('#^(http://pic)\.#', '${1}' . $this->_decideClusterId($entry) . '.', $url);
		}

		return $url;
	}

	/**
	 * Will calculate the id of cluster for this entry
	 *
	 * @param Sceon_Images_Entry $entry
	 */
	private function _decideClusterId($entry)
	{
		$ascii = 0;
		for ( $i = 0; $i < strlen($entry->hash); ++$i ) $ascii += ord(substr($entry->hash, $i, 1));
		$base = ord('1') * strlen($entry->hash);
		$ascii -= $base;
		$max_ascii = ord('f') * strlen($entry->hash) - $base;
		$percent = $ascii / $max_ascii;
		return round($percent * $this->_config['remote']['cluster_max']);
	}
}
