<?php

class Core_JsonRpc_Server_Plugin_ModelValidator implements Core_JsonRpc_Server_PluginInterface
{
	protected $_server = null;

	public function notify($about, array $params = array())
	{
		if ( 'exceptionCaught' != $about )
			return false;

		$exception = $params['e'];
		if ( ! $exception instanceof Core_Model_Validator_Exception )
			return false;

		$response = $this->_server->getResponse();
		$response->setResult(array(
			'validation' => array(
				'status' => 'error',
				'errors' => $exception->getErrors(),
			)
		));
		return true; // break notify cycle
	}

	public function setServer(Core_JsonRpc_Server $server)
	{
		$this->_server = $server;
	}
}

