<?php

class Core_View_Helper_Shortify extends Zend_View_Helper_Abstract
{
	public function shortify($string, $length = 80)
	{
		$suffix = '...';
		$string = html_entity_decode($string, ENT_COMPAT, 'UTF-8');
		$string = strip_tags($string, 'UTF-8');
		if ( mb_strlen($string, 'UTF-8') <= $length) $suffix = '';
		$string = mb_substr($string, 0, $length, 'UTF-8') . $suffix;
		return $string;
	}
}

