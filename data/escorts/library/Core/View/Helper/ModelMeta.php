<?php

class Core_View_Helper_ModelMeta extends Zend_View_Helper_Abstract
{
	public function modelMeta($model)
	{
		$html = '<div class="meta">';
		if ( isset($model->date_updated) && $model->date_updated ) {
			$html .= '<div class="updated">';
			$date = strtotime($model->date_updated);
			$html .= 'Updated on <strong>' . date('d M Y', $date) . '</strong> at <strong>' . date('H:i:s', $date) . '</strong>';
			$html .= '<br/>by <strong>' . $model->updated_by_name . '</strong>';
			$html .= '</div>';
		}
		if ( isset($model->date_created) && $model->date_created ) {
			$html .= '<div class="created">';
			$date = strtotime($model->date_created);
			$html .= 'Created on <strong>' . date('d M Y', $date) . '</strong> at <strong>' . date('H:i:s', $date) . '</strong>';
			$html .= '<br/>by <strong>' . $model->created_by_name . '</strong>';
			$html .= '</div>';
		}
		$html .= '<div class="clear"></div>';
		$html .= '</div>';
		return $html;
	}
}

