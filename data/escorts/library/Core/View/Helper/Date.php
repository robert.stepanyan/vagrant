<?php

class Core_View_Helper_Date extends Zend_View_Helper_Abstract
{
	public function date($date)
	{
		$timestamp = strtotime($date);
		if ( false === $date ) return null;
		return date('d/m/Y');
	}
}

