<?php

class Core_Model_Validator_Exception extends Exception
{
	protected $_errors;

	public function __construct($message = '', $errors = array())
	{
		parent::__construct($message);
		$this->_errors = $errors;
	}

	public function getErrors()
	{
		return $this->_errors;
	}
}
