<?php

class Core_Model_Validator
{
	protected $_model;

	public function __construct(Core_Model $model)
	{
		$this->_model = $model;
	}

	public function validate()
	{
		$errors = array();
		foreach ( array_keys($this->_model->getArrayCopy()) as $key ) {
			$method = 'valid_' . Core_Tools::capitalize($key, false);
			$callback = array($this, $method);
			if ( is_callable($callback) ) {
				$result = call_user_func_array($callback, array(&$this->_model->$key));
				if ( true !== $result ) {
					$errors[$key] = $result;
				}
			}
		}

		if ( count($errors) > 0 ) {
			throw new Core_Model_Validator_Exception('Validation failed', $errors);
		}

		return true;
	}
}

