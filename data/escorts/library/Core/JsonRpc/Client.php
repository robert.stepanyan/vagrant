<?php

class Core_JsonRpc_Client
{
	protected $_url;

	static protected $_id = 0;

	public function __construct($url)
	{
		$this->_url = $url;
	}

	public function call($method, array $params = array())
	{
		$request = array(
			'id' => ++self::$_id,
			'method' => $method,
			'params' => $params
		);

		$content = json_encode($request);
		$length = strlen($content);
		$options = array(
			'http' => array(
				'method' => 'POST',
				'header' => "Connection: close\r\nContent-Length: $length\r\nContent-Type: application/json\r\n",
				'content' => $content
			)
		);

		$context = stream_context_create($options);
		$result = file_get_contents($this->_url, false, $context);

		if ( false === $result ) {
			throw new Exception('Could not connect to rpc server');
		}

		$plain = $result;
		$result = json_decode($result);
		if ( ! $result ) {
			throw new Exception('Could not parse server response. Actual response: ' . $plain);
		}

		if ( ! empty($result->error) ) {
			throw new Exception('Got error from server: ' . $result->error);
		}

		return $result->result;
	}
}

