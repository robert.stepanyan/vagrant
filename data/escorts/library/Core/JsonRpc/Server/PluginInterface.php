<?php

interface Core_JsonRpc_Server_PluginInterface
{
	public function notify($about, array $params = array());

	public function setServer(Core_JsonRpc_Server $server);
}

