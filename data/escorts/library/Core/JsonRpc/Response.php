<?php

class Core_JsonRpc_Response extends ArrayObject
{
	public function __construct()
	{
		parent::__construct(array('id' => null, 'error' => null, 'result' => null), self::ARRAY_AS_PROPS);
	}
	
	public function setError($error)
	{ 
		$this->error = $error;
	}

	public function setResult($result)
	{
		$this->result = $result;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function toJson()
	{
		return json_encode($this->getArrayCopy());
	}

	public function send()
	{
		Zend_Controller_Front::getInstance()
			->getResponse()
			->setHeader('Content-Type', 'application/json')
			->setBody($this->toJson());
	}
}
