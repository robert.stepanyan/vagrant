<?php

class Core_JsonRpc_Request extends ArrayObject
{
	public function __construct()
	{
		parent::__construct(array(), self::ARRAY_AS_PROPS);
	}

	public function getMethod()
	{
		return $this->method;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getParams()
	{
		return $this->params;
	}

	public function parse($json)
	{
		$json = json_decode($json, true);
		if ( null === $json ) {
			throw new Core_JsonRpc_Request_Exception('Invalid json data');
		}

		if ( ! isset($json['method']) ) {
			throw new Core_JsonRpc_Request_Exception('Method is required');
		}

		$this->method = $json['method'];

		if ( ! isset($json['params']) || ! is_array($json['params']) ) {
			throw new Core_JsonRpc_Request_Exception('Params is required and must be array');
		}

		$this->params = $json['params'];

		$this->id = isset($json['id']) ? $json['id'] : null;
	}
}

