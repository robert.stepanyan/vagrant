<?php

class Core_I18n_Dictionary extends Core_Model
{
	private $_cache_file;

	private $_lang_id = 'en';

	public function __construct($lang_id = null)
	{
		$this->_cache_file = APP_ROOT . '/cache/dictionary_%s.inc.php';

		$this->_lang_id = null === $lang_id ? Core_I18n::getLang() : $lang_id;
	}

	public function load($section = null, $force_cache = false)
	{
		if ( isset($_REQUEST['translate']) ) {
			$this->_dict = array();
			return;
		}
		
		$config = self::config();
		$lifetime = intval($config->dictionary->lifetime);

		$cache_file = sprintf($this->_cache_file, Core_I18n::getLang());

		// If cache does not exist or is expired fetch everything from database
		// and save again to cache
		if ( ! is_file($cache_file) || (time() - filemtime($cache_file)) > $lifetime ) {
			$sql = 'SELECT id, value_' . $this->_lang_id . ' AS value FROM dictionary';
			$result = self::fetchAll($sql);

			$this->exchangeArray(array());
			$fp = fopen($cache_file, 'w+');
			flock($fp, LOCK_EX);
			fwrite($fp, '<? return array(' . PHP_EOL);
			foreach ( $result as $row ) {
				$this[$row->id] = $row->value;
				$value = str_replace('"', '\"', $row->value);
				fwrite($fp, "'{$row->id}' => \"{$value}\"," . PHP_EOL);
			}
			fwrite($fp, '); ?>');

			return;
		}
		else {
			$this->exchangeArray(require($cache_file));
		}
	}
}
