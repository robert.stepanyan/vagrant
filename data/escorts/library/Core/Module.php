<?php

abstract class Core_Module
{
	abstract static public function init(Zend_Controller_Front $front);
}
