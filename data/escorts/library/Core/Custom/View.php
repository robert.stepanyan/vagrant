<?php

class Core_Custom_View extends Zend_View
{
	public function setBasePath($path, $classPrefix = 'Zend_View')
	{
		$module = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();

		$path		 = preg_replace('#views$#', $module, $path);
		$path        = rtrim($path, '/');
		$path        = rtrim($path, '\\');
		$path       .= DIRECTORY_SEPARATOR;
		$classPrefix = rtrim($classPrefix, '_') . '_';
		$this->setScriptPath($path);
//		$this->setHelperPath($path . 'helpers', $classPrefix . 'Helper');
//		$this->setFilterPath($path . 'filters', $classPrefix . 'Filter');
		return $this;
	}

	public function addBasePath($path, $classPrefix = 'Zend_View')
	{
		$module = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();

		$path		 = preg_replace('#views$#', $module, $path);
		$path        = rtrim($path, '/');
		$path        = rtrim($path, '\\');
		$path       .= DIRECTORY_SEPARATOR;
		$classPrefix = rtrim($classPrefix, '_') . '_';
		$this->addScriptPath($path);
//		$this->addHelperPath($path . 'helpers', $classPrefix . 'Helper');
//		$this->addFilterPath($path . 'filters', $classPrefix . 'Filter');
		return $this;
	}
}
