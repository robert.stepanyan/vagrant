<?php

class Core_Tree extends Core_Model
{
	protected $_node_class = 'Core_Tree_Node';
	protected $_table = 'nodes';

	protected function _setTable($table)
	{
		$this->_table = $table;
	}

	protected function _setNodeClass($class)
	{
		$this->_node_class = $class;
	}

	public function getPath($id)
	{
		$result = self::db()->fetchAll('
			SELECT parent.id FROM ' . $this->_table . ' AS node, ' . $this->_table . ' AS parent
			WHERE node.lft >= parent.lft AND node.rgt <= parent.rgt AND node.id = ?
			ORDER BY parent.lft
		', $id);

		foreach ( $result as $i => $data )
			$result[$i] = new $this->_node_class($result);

		return $result;
	}

	public function getFull($root_id = null)
	{
		if ( $root_id ) {
			self::db()->query('SELECT lft, rgt FROM ' . $this->_table . ' WHERE id = ? INTO @lft, @rgt', $root_id);
		}

		$result = self::db()->fetchAll('
			SELECT
				node.id,
				(COUNT(parent.id) - 1) AS depth
			FROM ' . $this->_table . ' AS node
			INNER JOIN ' . $this->_table . ' AS parent ON node.lft >= parent.lft AND node.lft <= parent.rgt AND node.lft >= @lft AND node.rgt <= @rgt
			GROUP BY node.id
			ORDER BY node.lft;
		');

		foreach ( $result as $i => $data )
			$result[$i] = new $this->_node_class($data);

		return $result;
	}

	public function getFirstChild($node_id)
	{
		self::db()->query('SELECT lft, rgt FROM nodes WHERE id = ? INTO @lft, @rgt', $node_id);

		$result = self::db()->fetchRow('
			SELECT
				node.id
			FROM ' . $this->_table . ' AS node
			WHERE node.lft = @lft + 1
			GROUP BY node.id
			ORDER BY node.lft
		');

		if ( ! $data ) return null;

		return new $this->_node_class($result);
	}

	public function getRootNodes()
	{
		$result = self::db()->fetchAll('
			SELECT id, lft, rgt, depth FROM (
				SELECT
					node.id,
					node.lft,
					node.rgt,
					(COUNT(parent.id) - 1) as depth
				FROM ' . $this->_table . ' AS node
				INNER JOIN ' . $this->_table . ' AS parent ON node.lft >= parent.lft AND node.lft <= parent.rgt
				GROUP BY node.id
				ORDER BY node.lft
			) AS root_nodes WHERE depth = 0
		');

		foreach ( $result as $i => $data )
			$result[$i] = new $this->_node_class($data);

		return $result;
	}

	public function getRootNode()
	{
		$node = self::db()->fetchRow('
			SELECT id FROM (
				SELECT
					node.id,
					(COUNT(parent.id) - 1) as depth
				FROM ' . $this->_table . ' AS node
				INNER JOIN ' . $this->_table . ' AS parent ON node.lft >= parent.lft AND node.lft <= parent.rgt
				GROUP BY node.id
				ORDER BY node.lft
			) AS root_nodes WHERE depth = 0
			LIMIT 1
		');

		return new $this->_node_class($node);
	}
}
