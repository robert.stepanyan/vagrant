<?php

class Core_Controller_Plugin_I18n extends Zend_Controller_Plugin_Abstract
{
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		if ( ! $request->lang ) {
			$this->_response->setRedirect('/' . Core_I18n::getDefLang());
		}
		
		/*if ( ! $request->isDispatched() && $request->lang == Core_I18n::getDefLang() ) {
			$url = preg_replace('#^/' . Core_I18n::getDefLang() . '#', '', $_SERVER['REQUEST_URI']);
			if ( 0 == strlen($url) ) $url = '/';
			$this->_response->setRedirect($url, 301);
			$request->setDispatched();
			return;
		}*/
		
		Core_I18n::setLang($request->lang);

//		if ( isset($request->c) ) {
//			setcookie('_lang', $request->lang, strtotime('+1 year'), '/');
//		}
//		elseif ( $request->getModuleName() == 'default' &&
//				$request->getControllerName() == 'index' &&
//				$request->getActionName() == 'index' && $request->getCookie('_lang') ) {
//			if ( $request->lang !== $request->getCookie('_lang') ) {
//				Core_I18n::setLang($request->getCookie('_lang'));
//				$this->_response->setRedirect(_link(''));
//				$request->setDispatched();
//				return;
//			}
//		}

//		if ( ! $request->getServer('HTTP_REFERER') &&
//			'default:index:index' == $request->getModuleName() . ':' . $request->getControllerName() . ':' . $request->getActionName() ) {
//			$cookie = $request->getCookie('_lang');
//			if ( $cookie == Core_I18n::getDefLang() ) $cookie = null;
//			if ( $request->lang != $cookie ) {
//				//if ( $cookie == Core_I18n::getDefLang() ) $cookie = null;
//				$this->_response->setRedirect('/' . $cookie);
//				$request->setDispatched();
//				return;
//			}
//		}

		Core_I18n::init();
	}
}
