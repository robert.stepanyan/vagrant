<?php

class Core_Controller_Plugin_Auth extends Zend_Controller_Plugin_Abstract
{
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
//		$output = var_export($_REQUEST, true);
//		echo json_encode(array('status' => 'error', 'message' => $output));
//		die;

		if ( ! IS_CLI && ! Auth_User::getCurrent() && (
			array($request->getModuleName(), $request->getControllerName(), $request->getActionName()) !== array('auth', 'index', 'index')
			&& array($request->getModuleName(), $request->getControllerName(), $request->getActionName()) !== array('api', 'handling', 'category')
				) ) {
			$this->_response->setRedirect(_link('def', array('module' => 'auth')) . '?r=' . urlencode($_SERVER['REQUEST_URI']));
			return $request->setDispatched(true);
		}

		if ( $user = Auth_User::getCurrent() ) {
			$user->date_last_online = new Zend_Db_Expr('NOW()');
			$user->save();
		}
	}
}
