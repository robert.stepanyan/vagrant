<?php

class Core_Controller_Action extends Zend_Controller_Action
{
	public function preDispatch()
	{
		if ( $this->_isAjax() ) {
			$this->_disableLayout();
		}
	}

	protected function p($param, $default = null)
	{
		return $this->_getParam($param, $default);
	}

	protected function _disableLayout()
	{
		$this->_helper->layout->disableLayout();
	}

	protected function _disableView()
	{
		$this->_helper->viewRenderer->setNoRender();
	}

	protected function _setScriptAction($action)
	{
		$this->_helper->viewRenderer->setScriptAction($action);
	}

	protected function _isAjax()
	{
		return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
			('xmlhttprequest' == strtolower($_SERVER['HTTP_X_REQUESTED_WITH']))) || ! is_null($this->p('ajax'));
	}

	protected function _error($code)
	{
		$this->_forward('error', 'error', 'default', array('code' => $code));
	}

	protected function _json($data)
	{
		$this->_disableLayout();
		$this->_disableView();
		$data = json_encode($data);
		$this->_response->setHeader('Content-type', 'application/javascript; charset=utf-8');
		$this->_response->appendBody($data);
	}

	protected function _take(array $keys)
	{
		$data = array();
		foreach ( $keys as $k => $v ) {
			$data[$v] = trim($this->p(is_int($k) ? $v : $k));
		}
		return (object) $data;
	}

	protected function _takeGridParams(array $defaults)
	{
		$defaults = array_merge(array(
			'page' => 1,
			'per_page' => 10,
			'sort_field' => '',
			'sort_dir' => ''
		), $defaults);

		$params = array();

		$page = (int) $this->p('page', $default['page']);
		if ( $page < 1 ) $page = 1;

		$per_page = (int) $this->p('per_page', $default['per_page']);
		if ( $per_page < 10 ) $per_page = 10;

		$sort_field = $this->p('sort_field', $defaults['sort_field']);
		$sort_dir = $this->p('sort_dir', $defaults['sort_dir']);

		return array(
			'page' => $page,
			'per_page' => $per_page,
			'sort_field' => $sort_field,
			'sort_dir' => $sort_dir
		);
	}
}
