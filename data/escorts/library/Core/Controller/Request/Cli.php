<?php

class Core_Controller_Request_Cli extends Zend_Controller_Request_Abstract
{
	public function __construct($action, $controller, $module, array $params = array())
	{
		$this->setModuleName($module);
		$this->setControllerName($controller);
		$this->setActionName($action);
		$this->setParams($params);
	}
}
