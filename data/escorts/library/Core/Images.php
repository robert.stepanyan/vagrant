<?php

class Core_Images
{
	const ERROR_IMAGE_EXT_NOT_ALLOWED = 1;
	const ERROR_IMAGE_INVALID = 2;
	const ERROR_IMAGE_TOO_SMALL = 3;
	const ERROR_IMAGE_TOO_BIG = 4;
	
	/**
	 * @var Core_Images_Storage_Ftp
	 */
	protected $_storage;
	
	public function __construct()
	{
		$this->_storage = new Core_Images_Storage_File();
	}
	
	/**
	 * Save the file on the remote machine
	 *
	 * @param string $file 
	 * @param string $catalog_id the same as escort id
	 * @return array
	 * @author GuGo
	 */
	public function save($file, $ext_override = null)
	{
		// Create a random file name
		$hash = md5(microtime(TRUE) * mt_rand(1, 1000000));
		$ext = is_null($ext_override) ? strtolower(@end(explode('.', $file))) : $ext_override;
		$remote = $this->_getFullPath(new Core_Images_Entry(array(
			'catalog_id' => $catalog_id,
			'hash' => $hash,
			'ext' => $ext
		)));
		
		$dirs = array();
		$i = 0;
		$dir = $remote;
		
		while ( ($dir = str_replace(array('/', '\\'), '/', dirname($dir))) != '/' ) {
			$i++;
			$dirs[] = $dir;
			if ( $i > 100 ) die('error');
		}
		
		// Connect to ftp
		$this->_storage->connect();
		
		// Create directories if they does not exist
		$dirs = array_reverse($dirs);
		
		foreach ( $dirs as $dir ) {
			try {
				$this->_storage->makeDir($dir);
			} catch (Core_Images_Storage_Exception $e) {
				// Probably folder already exists
			}
		}
		
		// Resize image on the local server
		$file = $this->_resize($file, $ext_override);
		
		// Save the local file on remote storage
		$this->_storage->store($file, $remote);
		$this->_storage->disconnect();
		
		return new Media_Image(array('hash' => $hash, 'ext' => $ext));
	}
	
	public function remove($entries)
	{
		if ( ! is_array($entries) ) {
			$entries = array($entries);
		}
		
		if ( ! count($entries) ) return;
		
		$remote = $this->_getFullPath($entries[0]);
		$path = dirname($remote);
		
		try {
			$this->_storage->connect();
			$list = $this->_storage->getFiles($path);
			
			foreach ( $entries as $entry ) {
				foreach ( $list as $filename ) {
					if ( preg_match('/' . $entry->hash . '(_.+)?\.' . $entry->ext . '/', $filename) ) {
						$this->_storage->delete($path . '/' . $filename);
					}
				}
			}
			
			$this->_storage->disconnect();
		}
		catch ( Exception $e ) {
			$this->_storage->disconnect();
		}
	}
	
	static protected function _getFullPath(Core_Images_Entry $entry)
	{
		$hash = $entry->hash;

		$path = '/' . substr($hash, 0, 2) . '/' . substr($hash, 2, 2) . '/' . $entry->hash . ($entry->size ? '_' . $entry->size : '') . '.' . $entry->ext;
		
		return $path;
	}
	
	/**
	 * NO NEED OF THIS FUNC
	 * Downloads image from the server
	 *
	 * @param Core_Images_Entry $entry 
	 * @return binary
	 * @author GuGo
	 */
	public function get(Core_Images_Entry $entry)
	{
		
	}
	
	/**
	 * Returns url of image on the remote machine
	 * If the application id is not specified, current application will be used
	 *
	 * @param Core_Images_Entry $entry 
	 * @return string
	 * @author GuGo
	 */
	static public function getUrl(Core_Images_Entry $entry)
	{
		return trim(_config()->media->base_url, '/') . self::_getFullPath($entry);
	}

	protected function _resize($file, $ext_override = null)
	{
		if ( ! is_file($file) ) {
			throw new Exception("File '$file' does not exist");
		}
		
		// Extract the filename and extension from full file path
		$file_name = basename($file);
		$file_name = explode('.', $file_name);
		$file_ext = is_null($ext_override) ? strtolower(end($file_name)) : $ext_override;
		$file_name = implode('.', array_slice($file_name, 0, count($file_name) - 1));
		
		// Make sure the extension is allowed
		if ( ! in_array($file_ext, explode(',', _config()->media->allowed)) ) {
			throw new Exception("Allowed extensions are " . _config()->media->allowed, self::ERROR_IMAGE_EXT_NOT_ALLOWED);
		}
		
		// Make sure that the image is valid and get it's width, height and type
		$img_info = @getimagesize($file);
		list($width, $height, $image_type) = $img_info;
		
		if ( FALSE === $img_info ) {
			throw new Exception('Only images are allowed to be uploaded', self::ERROR_IMAGE_INVALID);
		}
		
		$max_sidesize = _config()->media->max_side;
		$min_sidesize = _config()->media->min_side;

		if ( $width > $max_sidesize || $height > $max_sidesize ) {
			throw new Exception('Please upload image smaller than ' . $max_sidesize . 'x' . $max_sidesize, self::ERROR_IMAGE_TOO_BIG);
		}
		
		// Get original file extension, it's size and quality from config
		$dest_bigside = _config()->media->orig_size;
		$dest_ext = _config()->media->orig_ext;
		$dest_quality = _config()->media->orig_quality;
		
		// Get the gd function extension from image type constant
		$ext = $this->_gdExtFromImageInfo($image_type);
		$ext = $ext['ext'];
		
		// Load uploaded image
		$func = 'imagecreatefrom' . $ext;
		
		$gd = @$func($file);
		
		if ( FALSE === $gd ) {
			throw new Exception('GD Error: Could not create from image');
		}
		
		// Calculate rational image size
		if ( $width < $height ) {
			$dest_width = $dest_bigside;
			$dest_height = $height * ($dest_width / $width);
		}
		else {
			$dest_height = $dest_bigside;
			$dest_width = $width * ($dest_height / $height);
		}
		
		if ( $dest_width > $width ) {
			$dest_width = $width;
		}
		
		if ( $dest_height > $height ) {
			$dest_height = $height;
		}
		
		$dest_width = intval($dest_width);
		$dest_height = intval($dest_height);
		
		// Resize the image
		$dst_gd = @imagecreatetruecolor($dest_width, $dest_height);
		@imagecopyresampled($dst_gd, $gd, 0, 0, 0, 0, $dest_width, $dest_height, $width, $height);
		
		// Get gd extension from destination file extension and save the resized image
		$ext = $this->_gdExtFromFileExt($dest_ext);
		$ext = $ext['ext'];
		$func = 'image' . $ext;
		
		//$dst_file = preg_replace('/\..+$/i', '_resized.' . $dest_ext, $file);
		$dst_file = $file;
		$func($dst_gd, $dst_file, ($ext == 'jpeg') ? $dest_quality : null);
		
		@imagedestroy($gd);
		@imagedestroy($dst_gd);
		
		return $dst_file;
	}
	
	protected function _watermark($file)
	{
		
	}
	
	protected function _gdExtFromFileExt($file_ext)
	{
		switch ($file_ext) {
			case 'jpg':
			case 'jpeg':
				$mime = 'image/jpeg';
				$ext = 'jpeg';
			break;
			case 'png':
				$mime = 'image/png';
				$ext = 'png';
			break;
			case 'gif':
				$mime = 'image/gif';
				$ext = 'gif';
			break;
		}
		
		if ( ! isset($mime) || ! isset($ext) ) {
			throw new Exception('Unknown extension ' . $file_ext);
		}
		
		return array('mime' => $mime, 'ext' => $ext);
	}
	
	protected function _gdExtFromImageInfo($image_type)
	{
		switch ($image_type) {
			case IMAGETYPE_JPEG:
				$mime = 'image/jpeg';
				$ext = 'jpeg';
			break;
			case IMAGETYPE_PNG:
				$mime = 'image/png';
				$ext = 'png';
			break;
			case IMAGETYPE_GIF:
				$mime = 'image/gif';
				$ext = 'gif';
			break;
		}
		
		if ( ! isset($mime) || ! isset($ext) ) {
			throw new Exception('Unknown image type ' . $image_type);
		}
		
		return array('mime' => $mime, 'ext' => $ext);
	}
}
