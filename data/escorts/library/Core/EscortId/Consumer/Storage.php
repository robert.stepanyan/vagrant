<?php

class Core_EscortId_Consumer_Storage extends Zend_OpenId_Consumer_Storage
{
	const TABLE_ASSOCIATION = 'consumer_association';
	const TABLE_DISCOVERY = 'consumer_discovery';
	const TABLE_NONCE = 'consumer_nonce';

	private $_db;
	
	public function __construct(Zend_Db_Adapter_Abstract $db = null)
	{
		if ( null === $db ) {
			$db = Core_Model::db();
		}
		$this->_db = $db;
	}

	public function addAssociation($url, $handle, $macFunc, $secret, $expires)
	{
		$secret = base64_encode($secret);
		$this->_db->insert(self::TABLE_ASSOCIATION, array(
			'url' => $url,
			'handle' => $handle,
			'mac_func' => $macFunc,
			'secret' => $secret,
			'expires' => new Zend_Db_Expr('FROM_UNIXTIME(' . $expires . ')')
		));
		return true;
	}

	public function getAssociation($url, &$handle, &$mac_func, &$secret, &$expires)
	{
		$this->_db->delete(self::TABLE_ASSOCIATION, $this->_db->quoteInto('expires < ?', time()));
		$result = $this->_db->fetchRow('
			SELECT
				handle, mac_func, secret, expires
			FROM ' . self::TABLE_ASSOCIATION . '
			WHERE url = ?
		', $url, Zend_Db::FETCH_NUM);
		if ( $result ) {
			list($handle, $mac_func, $secret, $expires) = $result;
			$secret = base64_decode($secret);
			return true;
		}
		return false;
	}

	public function getAssociationByHandle($handle, &$url, &$mac_func, &$secret, &$expires)
	{
		$this->_db->delete(self::TABLE_ASSOCIATION, $this->_db->quoteInto('expires < ?', time()));
		$result = $this->_db->fetchRow('
			SELECT
				url, mac_func, secret, expires
			FROM ' . self::TABLE_ASSOCIATION . '
			WHERE handle = ?
		', $handle, Zend_Db::FETCH_NUM);
		if ( $result ) {
			list($handle, $mac_func, $secret, $expires) = $result;
			$secret = base64_decode($secret);
			return true;
		}
		return false;
	}

	public function delAssociation($url)
	{
		$this->_db->delete(self::TABLE_ASSOCIATION, $this->_db->quoteInto('url = ?', $url));
		return true;
	}

	public function addDiscoveryInfo($id, $real_id, $server, $version, $expires)
	{
		$secret = base64_encode($secret);
		$this->_db->insert(self::TABLE_DISCOVERY, array(
			'id' => $id,
			'real_id' => $real_id,
			'server' => $server,
			'version' => $version,
			'expires' => $expires
		));
		return true;
	}

	public function getDiscoveryInfo($id, &$real_id, &$server, &$version, &$expires)
	{
		$this->_db->delete(self::TABLE_DISCOVERY, $this->_db->quoteInto('expires < ?', time()));
		$result = $this->_db->fetchRow('
			SELECT
				real_id, server, version, expires
			FROM ' . self::TABLE_DISCOVERY . '
			WHERE id = ?
		', $id, Zend_Db::FETCH_NUM);
		if ( $result ) {
			list($real_id, $server, $version, $expires) = $result;
			return true;
		}
		return false;
	}

	public function delDiscoveryInfo($id)
	{
		$this->_db->delete(self::TABLE_DISCOVERY, $this->_db->quoteInto('id = ?', $id));
		return true;
	}

	public function isUniqueNonce($provider, $nonce)
	{
		try {
			$this->_db->insert(self::TABLE_NONCE, array(
				'nonce' => $nonce,
				'created' => time()
			));
			return true;
		}
		catch ( Exception $e ) {
			return false;
		}
	}

	public function purgeNonces($date = null)
	{
		// TODO: Find the usage of this function and implement in right way
	}
}

