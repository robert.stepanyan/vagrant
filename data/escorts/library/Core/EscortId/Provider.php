<?php

class Core_EscortId_Provider extends Zend_OpenId_Provider
{
	protected $_extensions;

	public function __construct()
	{
		$storage = new Core_EscortId_Profile_Storage();
		$session = new Zend_Session_Namespace('escortid');
		$user = new Zend_OpenId_Provider_User_Session($session);
		
		$this->_extensions['ax'] = new Core_EscortId_Extension_AttributeExchange(null, null, $storage, $user, $session);
		$this->_extensions['ui'] = new Core_EscortId_Extension_UserInterface();

		parent::__construct(
			_mlink('default', 'provider', array('action' => 'login')),
			_mlink('default', 'provider', array('action' => 'trust')),
			$user,
			new Core_EscortId_Provider_Storage()
		);
		$this->setOpEndpoint(Zend_OpenId::absoluteUrl(_mlink('default', 'provider')));
	}

	public function getExtension($name = null)
	{
		if ( null === $name ) {
			return $this->_extensions;
		}

		return isset($this->_extensions[$name]) ?
			$this->_extensions[$name] : null;
	}
}

