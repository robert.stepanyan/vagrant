<?php

class Core_EscortId_Extension_AttributeExchange extends Zend_OpenId_Extension
{
	const NAMESPACE_1_0 = 'http://openid.net/srv/ax/1.0';

	// OpenId Draft AX Properties Schemas {{{
	const SCHEMA_NAMEPERSON_PREFIX = 'http://openid.net/schema/namePerson/prefix';
	const SCHEMA_NAMEPERSON_FIRST = 'http://openid.net/schema/namePerson/first';
	const SCHEMA_NAMEPERSON_LAST = 'http://openid.net/schema/namePerson/last';
	const SCHEMA_NAMEPERSON_MIDDLE = 'http://openid.net/schema/namePerson/middle';
	const SCHEMA_NAMEPERSON_SUFFIX = 'http://openid.net/schema/namePerson/suffix';
	const SCHEMA_NAMEPERSON_FRIENDLY = 'http://openid.net/schema/namePerson/friendly';
	const SCHEMA_PERSON_GUID = 'http://openid.net/schema/person/guid';
	const SCHEMA_BIRTHDATE = 'http://openid.net/schema/birthDate';
	const SCHEMA_BIRTHDATE_YEAR = 'http://openid.net/schema/birthDate/birthYear';
	const SCHEMA_BIRTHDATE_MONTH = 'http://openid.net/schema/birthDate/birthMonth';
	const SCHEMA_BIRTHDATE_DAY = 'http://openid.net/schema/birthDate/birthDay';
	const SCHEMA_GENDER = 'http://openid.net/schema/gender';
	const SCHEMA_LANGUAGE_PREF = 'http://openid.net/language/pref';
	const SCHEMA_CONTACT_PHONE_DEFAULT = 'http://openid.net/contact/phone/default';
	const SCHEMA_CONTACT_PHONE_HOME = 'http://openid.net/contact/phone/home';
	const SCHEMA_CONTACT_PHONE_BUSINESS = 'http://openid.net/contact/phone/business';
	const SCHEMA_CONTACT_PHONE_CELL = 'http://openid.net/contact/phone/cell';
	const SCHEMA_CONTACT_PHONE_FAX = 'http://openid.net/contact/phone/fax';

	const SCHEMA_CONTACT_POSTALADDRESS_HOME = 'http://openid.net/contact/postaladdress/home'; // street num, street name, apartment
	const SCHEMA_CONTACT_POSTALADDRESSADDITIONAL_HOME = 'http://openid.net/contact/postaladdressadditional/home';
	const SCHEMA_CONTACT_CITY_HOME = 'http://openid.net/contact/city/home';
	const SCHEMA_CONTACT_STATE_HOME = 'http://openid.net/contact/state/home'; // or province
	const SCHEMA_CONTACT_COUNTRY_HOME = 'http://openid.net/contact/country/home'; // country iso (2 letter) uppercase
	const SCHEMA_CONTACT_POSTALCODE_HOME = 'http://openid.net/contact/postalcode/home'; // or zip code ?

	const SCHEMA_CONTACT_POSTALADDRESS_BUSINESS = 'http://openid.net/contact/postaladdress/business'; // street num, street name, apartment
	const SCHEMA_CONTACT_POSTALADDRESSADDITIONAL_BUSINESS = 'http://openid.net/contact/postaladdressadditional/business';
	const SCHEMA_CONTACT_CITY_BUSINESS = 'http://openid.net/contact/city/business';
	const SCHEMA_CONTACT_STATE_BUSINESS = 'http://openid.net/contact/state/business'; // or province
	const SCHEMA_CONTACT_COUNTRY_BUSINESS = 'http://openid.net/contact/country/business'; // country iso (2 letter) uppercase
	const SCHEMA_CONTACT_POSTALCODE_BUSINESS = 'http://openid.net/contact/postalcode/business'; // or zip code ?

	const SCHEMA_CONTACT_IM_DEFAULT = 'http://openid.net/schema/contact/IM/default';
	const SCHEMA_CONTACT_IM_AIM = 'http://openid.net/schema/contact/IM/AIM';
	const SCHEMA_CONTACT_IM_ICQ = 'http://openid.net/schema/contact/IM/ICQ';
	const SCHEMA_CONTACT_IM_MSN = 'http://openid.net/schema/contact/IM/MSN';
	const SCHEMA_CONTACT_IM_YAHOO = 'http://openid.net/schema/contact/IM/Yahoo';
	const SCHEMA_CONTACT_IM_JABBER = 'http://openid.net/schema/contact/IM/Jabber';
	const SCHEMA_CONTACT_IM_SKYPE = 'http://openid.net/schema/contact/IM/Skype';

	const SCHEMA_CONTACT_INTERNET_EMAIL = 'http://openid.net/schema/contact/internet/email';
	const SCHEMA_CONTACT_WEB_DEFAULT = 'http://openid.net/schema/contact/web/default';
	const SCHEMA_CONTACT_WEB_BLOG = 'http://openid.net/schema/contact/web/blog';
	const SCHEMA_CONTACT_WEB_LINKEDIN = 'http://openid.net/schema/contact/web/Linkedin';
	const SCHEMA_CONTACT_WEB_AMAZON = 'http://openid.net/schema/contact/web/Amazon';
	const SCHEMA_CONTACT_WEB_FLICKR = 'http://openid.net/schema/contact/web/Flickr';
	const SCHEMA_CONTACT_WEB_DELICIOUS = 'http://openid.net/schema/contact/web/Delicious';

	const SCHEMA_COMPANY_NAME = 'http://openid.net/schema/company/name';
	const SCHEMA_COMPANY_TITLE = 'http://openid.net/schema/company/title';

	const SCHEMA_MEDIA_SPOKENNAME = 'http://openid.net/schema/media/spokenname';
	const SCHEMA_MEDIA_GREETING_AUDIO = 'http://openid.net/schema/media/greeting/audio';
	const SCHEMA_MEDIA_GREETING_VIDEO = 'http://openid.net/schema/media/greeting/video';
	const SCHEMA_MEDIA_BIOGRAPHY = 'http://openid.net/schema/media/biography';
	const SCHEMA_MEDIA_IMAGE = 'http://openid.net/schema/media/image'; // unspecified dimension
	const SCHEMA_MEDIA_IMAGE_16X16 = 'http://openid.net/schema/media/image/16x16';
	const SCHEMA_MEDIA_IMAGE_32X32 = 'http://openid.net/schema/media/image/32x32';
	const SCHEMA_MEDIA_IMAGE_48X48 = 'http://openid.net/schema/media/image/48x48';
	const SCHEMA_MEDIA_IMAGE_64X64 = 'http://openid.net/schema/media/image/64x64';
	const SCHEMA_MEDIA_IMAGE_80X80 = 'http://openid.net/schema/media/image/80x80';
	const SCHEMA_MEDIA_IMAGE_128X128 = 'http://openid.net/schema/media/image/128x128';
	const SCHEMA_MEDIA_IMAGE_160X120 = 'http://openid.net/schema/media/image/160x120';
	const SCHEMA_MEDIA_IMAGE_320X240 = 'http://openid.net/schema/media/image/320x240';
	const SCHEMA_MEDIA_IMAGE_640X480 = 'http://openid.net/schema/media/image/640x480';
	const SCHEMA_MEDIA_IMAGE_120X160 = 'http://openid.net/schema/media/image/120x160';
	const SCHEMA_MEDIA_IMAGE_240X320 = 'http://openid.net/schema/media/image/240x320';
	const SCHEMA_MEDIA_IMAGE_480X640 = 'http://openid.net/schema/media/image/480x640';
	const SCHEMA_MEDIA_IMAGE_FAVICON = 'http://openid.net/schema/media/image/favicon';
	const SCHEMA_TIMEZONE = 'http://openid.net/schema/timezone';
	// }}}

	const REQUEST_MODE_FETCH = 'fetch_request';
	const REQUEST_MODE_STORE = 'store_request';

	const RESPONSE_MODE_FETCH = 'fetch_response';
	const RESPONSE_MODE_STORE_SUCCESS = 'store_response_success';
	const RESPONSE_MODE_STORE_FAILURE = 'store_response_failure';

	protected $_attrs;
	protected $_updateUrl;
	protected $_storage;
	protected $_user;
	protected $_session;

	protected $_mode = self::REQUEST_MODE_FETCH;

	protected $_trusted;

	protected $_isSuccess;

	protected $_error;

	public function isSuccess()
	{
		return $this->_isSuccess;
	}

	public function getError()
	{
		return $this->_error;
	}

	public function __construct(array $attrs = null, $update_url = null, $storage = null, $user = null, $session = null)
	{
		if ( empty($attrs) ) {
			if ( ! empty($session->req_attrs) ) {
				$attrs = $session->req_attrs;
			}
		}

		if ( ! empty($session->req_mode) ) {
			$this->setMode($session->req_mode);
		}

		$this->_attrs = $attrs;

		$this->_updateUrl = $update_url;

		$this->_storage = $storage;
		if ( ! empty($this->_storage) ) {
			$this->_storage->setId($user->getLoggedInUser());
		}

		$this->_user = $user;
		$this->_session = $session;
	}

	public function getAttributes()
	{
		if ( is_array($this->_attrs) ) {
			return $this->_attrs;
		}
		
		return array();
	}

	public function setTrustedAliases($aliases)
	{
		$trusted = array();
		foreach ( $aliases as $trustedAlias ) {
			foreach ( $this->getAttributes() as $alias => $meta ) {
				if ( $alias == $trustedAlias ) {
					$trusted[] = $meta['type'];
				}
			}
		}
		$this->_trusted = $trusted;
	}

	public function setTrustedSchemas($schemas)
	{
		$this->_trusted = $schemas;
	}

	public function getTrustedSchemas()
	{
		return $this->_trusted;
	}

	public function getStorage()
	{
		return $this->_storage;
	}

	public function setMode($mode)
	{
		$this->_mode = $mode;
	}

	protected function _fetchRequest(&$params)
	{
		$required = $available = array();
		foreach ( $this->_attrs as $alias => $rule ) {
			$params['openid.ax.type.' . $alias] = $rule['type'];
			if ( ! empty($rule['count']) ) {
				$params['openid.ax.count.' . $alias] = (int) $rule['count'];
			}
			else {
				$params['openid.ax.count.' . $alias] = 'unlimited';
			}

			// collect required and if_available attributes
			if ( isset($rule['required']) && $rule['required'] ) {
				$required[] = $alias;
			}
			else {
				$if_available[] = $alias;
			}
		}
		
		if ( count($required) ) {
			$params['openid.ax.required'] = implode(',', $required);
		}
		if ( count($if_available) ) {
			$params['openid.ax.if_available'] = implode(',', $if_available);
		}

		if ( ! empty($this->_updateUrl) ) {
			$params['openid.ax.update_url'] = $this->_updateUrl;
		}
	}

	protected function _storeRequest(&$params)
	{
		$required = $available = array();
		foreach ( $this->_attrs as $attr => $value ) {
			$params['openid.ax.type.' . $attr] = $value['type'];
			
			$val = $value['value'];

			if ( is_array($val) && count($val) > 0 ) {
				$params['openid.ax.count.' . $attr] = count($val);
				$current = 0;
				foreach ( $val as $v ) {
					$current++;
					$params['openid.ax.value.' . $attr . '.' . $current] = $v;
				}
			}
			elseif ( is_string($val) ) {
				$params['openid.ax.value.' . $attr] = $val;
			}
		}
	}

	public function prepareRequest(&$params)
	{
		$params['openid.ns.ax'] = self::NAMESPACE_1_0;
		$params['openid.ax.mode'] = $this->_mode;

		if ( is_array($this->_attrs) && count($this->_attrs) > 0 ) {
			if ( self::REQUEST_MODE_FETCH == $this->_mode ) {
				$this->_fetchRequest($params);
			}
			else if ( self::REQUEST_MODE_STORE == $this->_mode ) {
				$this->_storeRequest($params);
			}
		}
		return true;
	}

	protected function _parseFetchRequest($params)
	{
		$attrs = array();
		foreach ( $params as $param => $value ) {
			if ( 'openid_ax_required' == $param ||
				'openid_ax_if_available' == $param ) {
				foreach ( explode(',', $value) as $alias ) {
					$alias = trim($alias);
					if ( ! strlen($alias) )
						continue;

					$attrs[$alias] = array();

					if ( 'openid_ax_required' == $param ) {
						$attrs[$alias]['required'] = true;
					}

					$key = 'openid_ax_type_' . $alias;
					if ( isset($params[$key]) ) {
						$attrs[$alias]['type'] = $params[$key];
					}

					$key = 'openid_ax_count_' . $alias;
					if ( isset($params[$key]) ) {
						$attrs[$alias]['count'] = (
							'unlimited' == $params[$key]
							? 'unlimited'
							: (int) $params[$key]
						);
					}
					else {
						$params[$key]['count'] = 1;
					}
					
				}
			}
			elseif ( 'openid_ax_update_url' == $param ) {
				$this->_updateUrl = $value;
			}
		}

		$this->_attrs = count($attrs) ? $attrs : null;
		$this->_session->req_attrs = $attrs;
	}

	protected function _parseStoreRequest($params)
	{
		$attrs = array();
		foreach ( $params as $param => $value ) {
			if ( 0  === strpos($param, 'openid_ax_type_') ) {
				$alias = substr($param, strlen('openid_ax_type_'));

				$attrs[$alias] = array();

				$key = 'openid_ax_type_' . $alias;
				if ( isset($params[$key]) ) {
					$attrs[$alias]['type'] = $params[$key];
				}

				$key = 'openid_ax_count_' . $alias;
				if ( isset($params[$key]) ) {
					$val = array();
					$count = $params[$key];
					for ( $i = 1; $i <= $count; ++$i ) {
						$key = 'openid_ax_value_' . $alias . '_' . $i;
						$val[] = $params[$key];
					}
					$attrs[$alias]['count'] = $count;
					$attrs[$alias]['value'] = $val;
				}
				else {
					$key = 'openid_ax_value_' . $alias;
					$attrs[$alias]['value'] = $params[$key];
				}
			}
			elseif ( 'openid_ax_update_url' == $param ) {
				$this->_updateUrl = $value;
			}
		}

		$this->_attrs = count($attrs) ? $attrs : null;
		$this->_session->req_attrs = $attrs;
	}

	public function parseRequest($params)
	{
		if ( isset($params['openid_ns_ax']) &&
			$params['openid_ns_ax'] === self::NAMESPACE_1_0 ) {
		}
		else {
			throw new Exception('Invalid schema for Attribute Exchange Extension');
		}

		$this->_mode = $params['openid_ax_mode'];
		$this->_session->req_mode = $this->_mode;
		if ( self::REQUEST_MODE_FETCH == $this->_mode ) {
			$this->_parseFetchRequest($params);
		}
		elseif ( self::REQUEST_MODE_STORE == $this->_mode ) {
			$this->_parseStoreRequest($params);
		}
		return true;
	}

	protected function _fetchResponse(&$params)
	{
		$params['openid.ax.mode'] = self::RESPONSE_MODE_FETCH;
		if ( is_array($this->_attrs) && count($this->_attrs) > 0 ) {
			if ( ! empty($this->_updateUrl) ) {
				$_params = $params;
				$params = array();
			}

			$avail = array();
			foreach ( $this->getTrustedSchemas() as $schema ) {
				foreach ( $this->getAttributes() as $alias => $rule ) {
					if ( $schema == $rule['type'] ) {
						$avail[$alias] = $rule['type'];
					}
				}
			}
			
			if ( count($avail) > 0 ) {
				foreach ( $avail as $alias => $type ) {
					$value = $this->getStorage()->getSchemaValue($type);
					if ( is_array($value) ) {
						$params['openid.ax.type.' . $alias] = $type;
						$params['openid.ax.count.' . $alias] = count($value);
						$current = 0;
						foreach ( $value as $val ) {
							$current++;
							$params['openid.ax.value.' . $alias . '.' . $current] = $val;
						}
					}
					elseif ( $value ) {
						$params['openid.ax.type.' . $alias] = $type;
						$params['openid.ax.value.' . $alias] = $value;
					}
				}
			}

			if ( isset($_params) ) {
				$tmp = $params;
				$params = $_params;
				$_params = $params + $tmp;

				// Generate new nonce to avoid double request
				$_params['openid.response_nonce'] = gmdate('Y-m-d\TH:i:s\Z') . uniqid();

				// Change openid.return_to to match upadte_url to pass the verification
				$_params['openid.return_to'] = $this->_updateUrl;

				// Fetch Association to Sign the request {{{
				$storage = new Core_EscortId_Provider_Storage();
				$storage->getAssociation($_params['openid.assoc_handle'], $macFunc, $secret, $expires);
				// }}}

				// Sign the request for consumer {{{
				$signed = '';
				$data = '';
				foreach ($_params as $key => $val) {
					if (strpos($key, 'openid.') === 0) {
						$key = substr($key, strlen('openid.'));
						if (!empty($signed)) {
							$signed .= ',';
						}
						$signed .= $key;
						$data .= $key . ':' . $val . "\n";
					}
				}
				$signed .= ',signed';
				$data .= 'signed:' . $signed . "\n";
				$_params['openid.signed'] = $signed;

				$_params['openid.sig'] = base64_encode(Zend_OpenId::hashHmac($macFunc, $data, $secret));
				// }}}
				$result = $this->_httpRequest($this->_updateUrl, 'POST', $_params);
			}
		}
	}

	protected function _storeResponse(&$params)
	{
		$this->_storage->load();
		foreach ( $this->getTrustedSchemas() as $schema ) {
			foreach ( $this->getAttributes() as $alias => $rule ) {
				if ( $rule['type'] == $schema ) {
					$this->_storage->setSchemaValue($rule['type'], $rule['value']);
				}
			}	
		}

		$result = $this->_storage->store();

		if ( true === $result ) {
			$params['openid.ax.mode'] = 'store_response_success';
		}
		else {
			$params['openid.ax.mode'] = 'store_response_failure';
			$params['openid.ax.error'] = $this->_storage->getError();
		}
	}

	public function prepareResponse(&$params)
	{
		$params['openid.ns.ax'] = self::NAMESPACE_1_0;
		
		if ( self::REQUEST_MODE_FETCH == $this->_mode ) {
			$this->_fetchResponse($params);
		}
		elseif ( self::REQUEST_MODE_STORE == $this->_mode ) {
			$this->_storeResponse($params);
		}
		unset($this->_session->req_mode);
		return true;
	}

	protected function _parseFetchResponse($params)
	{
		$attrs = array();
		foreach ( $params as $param => $value ) {
			if ( 0 === strpos($param, 'openid_ax_type_') ) {
				$alias = substr($param, strlen('openid_ax_type_'));
				$attrs[$alias] = array('type' => $value);
				
				$key = 'openid_ax_count_' . $alias;
				if ( isset($params[$key]) ) { // multiple values
					$count = $params[$key];
				}

				$key = 'openid_ax_value_' . $alias;
				if ( isset($count) ) {
					$attrs[$alias]['value'] = array();
					for ( $i = 1; $i <= $count; $i++ ) {
						$attrs[$alias]['value'][] = $params[$key . '_' . $i];
					}
				}
				else {
					$attrs[$alias]['value'] = $params[$key];
				}
			}
		}
		$this->_attrs = count($attrs) > 0 ? $attrs : null;
	}

	protected function _parseStoreResponse($params)
	{
		if ( self::RESPONSE_MODE_STORE_SUCCESS == $params['openid_ax_mode'] ) {
			$this->_isSuccess = true;
		}
		elseif ( self::RESPONSE_MODE_STORE_FAILURE == $params['openid_ax_mode'] ) {
			$this->_isSuccess = false;
			$this->_error = $params['openid_ax_error'];
		}
	}

	public function parseResponse($params)
	{
		$found = false;
		foreach ( $params as $param => $value ) {
			if ( $param == 'openid_ns_ax' ) {
				$found = true;
			}
		}
		if ( ! $found )
			return true; // this is not an AX response

		$this->_mode = $params['openid_ax_mode'];
		if ( self::RESPONSE_MODE_FETCH == $this->_mode ) {
			$this->_parseFetchResponse($params);
		}
		elseif ( in_array($this->_mode, array(self::RESPONSE_MODE_STORE_SUCCESS, self::RESPONSE_MODE_STORE_FAILURE)) ) {
			$this->_parseStoreResponse($params);
		}
		return true;
	}

	protected function _httpRequest($url, $method = 'GET', array $params = array(), &$status = null)
	{
		$client = new Zend_Http_Client(
			$url,
			array(
				'maxredirects' => 1,
				'timeout' => 10,
				'useragent' => 'Core_EscortId'
			)
		);
		if ( $method == 'POST' ) {
			$client->setMethod(Zend_Http_Client::POST);
			$client->setParameterPost($params);
		}
		elseif ( $method == 'GET' ) {
			$client->setMethod(Zend_Http_Client::GET);
			$client->setParameterGet($params);
		}

		try {
			$response = $client->request();
		}
		catch ( Exception $e ) {
			//throw new Exception('HTTP Request failed: ' . $e->getMessage());
			return false;
		}

		$status = $response->getStatus();
		$body = $response->getBody();
		if ( $status == 200 || ($status == 400 && ! empty($body)) ) {
			return $body;
		}
		else {
			// Invalid http reponse status
			return false;
		}
	}

	public function getTrustData(&$data)
	{
		$data[get_class()] = $this->getTrustedSchemas();
		return true;
	}

	public function checkTrustData($data)
	{
		$trusted = isset($data[get_class()]) ? $data[get_class()] : array();
		$this->setTrustedSchemas($trusted);

		$found = false;
		foreach ( $this->getAttributes() as $alias => $rule ) {
			$found = false;
			foreach ( $this->getTrustedSchemas() as $schema ) {
				if ( $rule['type'] == $schema ) {
					$found = true;
					break;
				}
			}
			if ( ! $found ) break;
		}

		return $found;
	}
}

