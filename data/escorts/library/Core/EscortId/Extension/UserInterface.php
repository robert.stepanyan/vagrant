<?php

class Core_EscortId_Extension_UserInterface extends Zend_OpenId_Extension
{
	const NAMESPACE_1_0 = 'http://specs.openid.net/extensions/ui/1.0';

	const MODE_POPUP = 'popup';

	protected $_mode;
	protected $_lang;

	public function setMode($mode)
	{
		$this->_mode = $mode;
	}

	public function getMode()
	{
		return $this->_mode;
	}

	public function setLang($lang)
	{
		$this->_lang = $lang;
	}

	public function getLang()
	{
		return $this->_lang;
	}

	public function __construct()
	{
	}

	public function prepareRequest(&$params)
	{
		if ( ! empty($this->_mode) ) {
			$params['openid.ns.ui'] = self::NAMESPACE_1_0;
			$params['openid.ui.mode'] = $this->_mode;
			$params['openid.ui.lang'] = $this->_lang;
		}
		return true;
	}

	public function parseRequest($params)
	{
		if ( isset($params['openid_ns_ui']) &&
			$params['openid_ns_ui'] === self::NAMESPACE_1_0 ) {
		}
		else {
			throw new Exception('Invalid schema for User Interface Extension');
		}

		$this->_mode = $params['openid_ui_mode'];
		$this->_lang = $params['openid_ui_lang'];
		return true;
	}

	public function prepareResponse(&$params)
	{
		return true;
	}

	public function parseResponse($params)
	{
		return true;
	}
}

