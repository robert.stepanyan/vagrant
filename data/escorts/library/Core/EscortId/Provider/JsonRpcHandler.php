<?php

class Core_EscortId_Provider_JsonRpcHandler extends Core_Model
{
	static protected $_provider;

	static protected function _getProvider()
	{
		if ( empty(self::$_provider) ) {
			self::$_provider = new Core_EscortId_Provider();
		}
		return self::$_provider;
	}

	static public function checkUsername($username)
	{
		$username = strtolower(trim($username));
		$id = Core_EscortId::username2id($username);

		return ! (bool) self::db()->fetchOne('
			SELECT TRUE
			FROM ' . Core_EscortId_Provider_Storage::TABLE_USER . '
			WHERE id = ?
		', $id);
	}

	static public function checkEmail($email)
	{
		$email = strtolower(trim($email));
		return ! (bool) self::db()->fetchOne('
			SELECT TRUE
			FROM ' . Core_EscortId_Profile_Storage::TABLE_USER_PROFILE . '
			WHERE email = ?
		', $email);
	}

	static public function register($data)
	{
		$id = Core_EscortId::username2id($data['username']);
		//self::_getProvider()->register($id, $data['password']);

		$profile = new Core_EscortId_Profile_Storage();
		$profile->setId($id);
		$profile->showname = $data['username'];
		$profile->contact_email = $data['email'];
		$profile->type = $data['type'];
		$result = $profile->store();
		return $profile->getError();

		return true;
	}
}

