<?php

class Core_EscortId_Provider_Storage extends Zend_OpenId_Provider_Storage
{
	const TABLE_ASSOCIATION = 'provider_association';
	const TABLE_USER = 'provider_user';
	const TABLE_USER_SITE = 'provider_user_site';
	
	private $_db;
	
	public function __construct(Zend_Db_Adapter_Abstract $db = null)
	{
		if ( null === $db ) {
			$db = Core_Model::db();
		}
		$this->_db = $db;
	}

	public function addAssociation($handle, $macFunc, $secret, $expires)
	{
		$secret = base64_encode($secret);
		$this->_db->insert(self::TABLE_ASSOCIATION, array(
			'handle' => $handle,
			'mac_func' => $macFunc,
			'secret' => $secret,
			'expires' => $expires
		));
		return true;
	}

	public function getAssociation($handle, &$mac_func, &$secret, &$expires)
	{
		$this->_db->delete(self::TABLE_ASSOCIATION, $this->_db->quoteInto('expires < ?', time()));
		$result = $this->_db->fetchRow('
			SELECT
				mac_func, secret, expires
			FROM ' . self::TABLE_ASSOCIATION . '
			WHERE handle = ?
		', $handle, Zend_Db::FETCH_NUM);
		if ( $result ) {
			list($mac_func, $secret, $expires) = $result;
			$secret = base64_decode($secret);
			return true;
		}
		return false;
	}

	public function delAssociation($handle)
	{
		$this->_db->delete(self::TABLE_ASSOCIATION, $this->_db->quoteInto('handle = ?', $handle));
		return true;
	}

	public function addUser($id, $password)
	{
		$this->_db->insert(self::TABLE_USER, array(
			'id' => $id,
			'password' => $password
		));
		return true;
	}

	public function hasUser($id)
	{
		return (bool) $this->_db->fetchOne('
			SELECT TRUE
			FROM ' . self::TABLE_USER . '
			WHERE id = ?
		', $id);
	}

	public function checkUser($id, $password)
	{
		return (bool) $this->_db->fetchOne('
			SELECT TRUE
			FROM ' . self::TABLE_USER . '
			WHERE id = ? AND password = ?
		', array($id, $password));
	}

	public function delUser($id)
	{
		$this->_db->delete(self::TABLE_USER, $this->_db->quoteInto('id = ?', $id));
		return true;
	}

	public function getTrustedSites($id)
	{
		$sites = $this->_db->fetchAll('
			SELECT site, trusted
			FROM ' . self::TABLE_USER_SITE . '
			WHERE id = ?
		', $id, Zend_Db::FETCH_NUM);
		$result = array();
		foreach ( $sites as $site ) {
			list($site, $trusted) = $site;
			$trusted = @json_decode($trusted);
			if ( is_object($trusted) )
				$trusted = (array) $trusted;
			if ( is_array($trusted) ) {
				$result[$site] = $trusted;
			}
		}
		return $result;
	}

	public function addSite($id, $site, $trusted)
	{
		$exists = $this->_db->fetchRow('
			SELECT TRUE
			FROM ' . self::TABLE_USER_SITE . '
			WHERE id = ? AND site = ?
		', array($id, $site), Zend_Db::FETCH_NUM);
		if ( null === $trusted ) { // the site must be delted
			$this->_db->delete(self::TABLE_USER_SITE, array(
				$this->_db->quoteInto('id = ?', $id),
				$this->_db->quoteInto('site = ?', $site)
			));
		}

		if ( $exists ) {
			$this->_db->update(self::TABLE_USER_SITE, array(
				'trusted' => $trusted === false ? false : json_encode($trusted)
			), array(
				$this->_db->quoteInto('id = ?', $id),
				$this->_db->quoteInto('site = ?', $site)
			));
		}
		else {
			$this->_db->insert(self::TABLE_USER_SITE, array(
				'id' => $id,
				'site' => $site,
				'trusted' => $trusted === false ? false : json_encode($trusted)
			));
		}
		return true;
	}
}

