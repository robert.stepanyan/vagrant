<?php

class Core_EscortId_Profile_Storage extends Core_EscortId_Extension_AttributeExchange_Storage
{
	protected $_table = 'provider_user_profile';

	// Custom Schemas for EscortID namespace {{{
	const SCHEMA_SHOWNAME = 'http://escortid.com/schema/showname';
	const SCHEMA_SLOGAN = 'http://escortid.com/schema/slogan';
	const SCHEMA_COUNTRY = 'http://escortid.com/schema/country';
	const SCHEMA_CONTACT_PHONE = 'http://escortid.com/schema/contact/phone';
	const SCHEMA_CONTACT_PHONE_CALLINGCODE = 'http://escortid.com/schema/contact/phone/callingCode';
	const SCHEMA_CONTACT_PHONE_INSTRUCTIONS = 'http://escortid.com/schema/contact/phone/instructions';
	const SCHEMA_CONTACT_PHONE_WITHHELD = 'http://escortid.com/schema/contact/phone/withheld';
	const SCHEMA_CONTACT_EMAIL = 'http://escortid.com/schema/contact/email';
	const SCHEMA_CONTACT_URL = 'http://escortid.com/schema/contact/url';
	const SCHEMA_CONTACT_CLUBNAME = 'http://escortid.com/schema/contact/clubName';
	const SCHEMA_CONTACT_ADDRESS_STREET = 'http://escortid.com/schema/contact/address/street';
	const SCHEMA_CONTACT_ADDRESS_APARTMENT = 'http://escortid.com/schema/contact/address/apartment';
	const SCHEMA_CONTACT_OTHER = 'http://escortid.com/schema/contact/other';
	const SCHEMA_BIOGRAPHY_ETHNICITY = 'http://escortid.com/schema/biography/ethnicity';
	const SCHEMA_BIOGRAPHY_NATIONALITY = 'http://escortid.com/schema/biography/nationality';
	const SCHEMA_BIOGRAPHY_GENDER = 'http://escortid.com/schema/biography/gender';
	const SCHEMA_BIOGRAPHY_BIRTHDATE = 'http://escortid.com/schema/biography/birthDate';
	const SCHEMA_BIOGRAPHY_CHARACTERISTICS = 'http://escortid.com/schema/biography/characteristics';
	const SCHEMA_PHYSICAL_HEIGHT = 'http://escortid.com/schema/physical/height';
	const SCHEMA_PHYSICAL_WEIGHT = 'http://escortid.com/schema/physical/weight';
	const SCHEMA_PHYSICAL_BUST = 'http://escortid.com/schema/physical/bust';
	const SCHEMA_PHYSICAL_WAIST = 'http://escortid.com/schema/physical/waist';
	const SCHEMA_PHYSICAL_HIP = 'http://escortid.com/schema/physical/hip';
	const SCHEMA_PHYSICAL_CUPSIZE = 'http://escortid.com/schema/physical/cupSize';
	const SCHEMA_PHYSICAL_HAIRCOLOR = 'http://escortid.com/schema/physical/hairColor';
	const SCHEMA_PHYSICAL_EYECOLOR = 'http://escortid.com/schema/physical/eyeColor';
	const SCHEMA_PHYSICAL_SHOESIZE = 'http://escortid.com/schema/physical/shoeSize';
	const SCHEMA_PHYSICAL_DRESSSIZE = 'http://escortid.com/schema/physical/dressSize';
	const SCHEMA_PHYSICAL_PUBICHAIR = 'http://escortid.com/schema/physical/pubicHair';
	const SCHEMA_PHYSICAL_SMOKING = 'http://escortid.com/schema/physical/smoking';
	const SCHEMA_PHYSICAL_DRINKING = 'http://escortid.com/schema/physical/drinking';
	const SCHEMA_LANGUAGES_ISO = 'http://escortid.com/schema/languages/iso';
	const SCHEMA_LANGUAGES_LEVEL = 'http://escortid.com/schema/languages/level';
	const SCHEMA_ABOUT_ISO = 'http://escortid.com/schema/about/iso';
	const SCHEMA_ABOUT_TEXT = 'http://escortid.com/schema/about/text';
	const SCHEMA_SERVICES_SERVICEFOR = 'http://escortid.com/schema/services/serviceFor';
	const SCHEMA_SERVICES_ORIENTATION = 'http://escortid.com/schema/services/orientation';
	const SCHEMA_SERVICES_ADDITIONAL_ISO = 'http://escortid.com/schema/services/additional/iso';
	const SCHEMA_SERVICES_ADDITIONAL_TEXT = 'http://escortid.com/schema/services/additional/text';
	const SCHEMA_SERVICES_OFFERS_ID = 'http://escortid.com/schema/services/offers/id';
	const SCHEMA_SERVICES_OFFERS_PRICE = 'http://escortid.com/schema/services/offers/price';
	const SCHEMA_SERVICES_OFFERS_CURRENCY = 'http://escortid.com/schema/services/offers/currency';
	const SCHEMA_WORKING_TIMES_ALL = 'http://escortid.com/schema/working/times/all';
	const SCHEMA_WORKING_TIMES_DAY = 'http://escortid.com/schema/working/times/day';
	const SCHEMA_WORKING_TIMES_FROM = 'http://escortid.com/schema/working/times/from';
	const SCHEMA_WORKING_TIMES_TO = 'http://escortid.com/schema/working/times/to';
	const SCHEMA_WORKING_AVAILABILITY_INCALL = 'http://escortid.com/schema/working/availability/incall';
	const SCHEMA_WORKING_AVAILABILITY_INCALL_ZIP = 'http://escortid.com/schema/working/availability/incall/zip';
	const SCHEMA_WORKING_AVAILABILITY_INCALL_HOTELROOMSTARS = 'http://escortid.com/schema/working/availability/incall/hotelRoomStars';
	const SCHEMA_WORKING_AVAILABILITY_INCALL_OTHERTEXT = 'http://escortid.com/schema/working/availability/incall/otherText';
	const SCHEMA_WORKING_AVAILABILITY_OUTCALL = 'http://escortid.com/schema/working/availability/outcall';
	const SCHEMA_WORKING_AVAILABILITY_OUTCALL_OTHERTEXT = 'http://escortid.com/schema/working/availability/outcall/otherText';
	const SCHEMA_WORKING_LOCATIONS = 'http://escortid.com/schema/working/locations';
	const SCHEMA_WORKING_LOCATIONS_HOME = 'http://escortid.com/schema/working/locations/home';
	const SCHEMA_WORKING_LOCATIONS_BASE = 'http://escortid.com/schema/working/locations/base';
	const SCHEMA_WORKING_VACATION_FROM = 'http://escortid.com/schema/working/vacation/from';
	const SCHEMA_WORKING_VACATION_TO = 'http://escortid.com/schema/working/vacation/to';
	const SCHEMA_RATES_INCALL_DURATION = 'http://escortid.com/schema/rates/incall/duration';
	const SCHEMA_RATES_INCALL_UNIT = 'http://escortid.com/schema/rates/incall/unit';
	const SCHEMA_RATES_INCALL_PRICE = 'http://escortid.com/schema/rates/incall/price';
	const SCHEMA_RATES_INCALL_CURRENCY = 'http://escortid.com/schema/rates/incall/currency';
	const SCHEMA_RATES_INCALL_OTHER = 'http://escortid.com/schema/rates/incall/other';
	const SCHEMA_RATES_INCALL_OTHER_PRICE = 'http://escortid.com/schema/rates/incall/other/price';
	const SCHEMA_RATES_INCALL_OTHER_CURRENCY = 'http://escortid.com/schema/rates/incall/other/currency';
	const SCHEMA_RATES_OUTCALL_DURATION = 'http://escortid.com/schema/rates/outcall/duration';
	const SCHEMA_RATES_OUTCALL_UNIT = 'http://escortid.com/schema/rates/outcall/unit';
	const SCHEMA_RATES_OUTCALL_PRICE = 'http://escortid.com/schema/rates/outcall/price';
	const SCHEMA_RATES_OUTCALL_CURRENCY = 'http://escortid.com/schema/rates/outcall/currency';
	const SCHEMA_RATES_OUTCALL_OTHER = 'http://escortid.com/schema/rates/outcall/other';
	const SCHEMA_RATES_OUTCALL_OTHER_PRICE = 'http://escortid.com/schema/rates/outcall/other/price';
	const SCHEMA_RATES_OUTCALL_OTHER_CURRENCY = 'http://escortid.com/schema/rates/outcall/other/currency';
	// }}}

	const TABLE_USER_PROFILE = 'provider_user_profile';

	private $_loaded = false;
	private $_id;

	const NAMESPACE_URL = 'http://escortid.com.dev';
	const SCHEMA_DEFINITION_FILE = 'profile.xsd';

	private $_schema_map = array();

	public function __construct($data = array())
	{
		parent::__construct($data);
		$this->_schema_map = array(
			// Mapping of internal aliases to schemas {{{
			'nickname' => Core_EscortId_Extension_AttributeExchange::SCHEMA_NAMEPERSON_FRIENDLY,
			'email' => Core_EscortId_Extension_AttributeExchange::SCHEMA_CONTACT_INTERNET_EMAIL,
			'website' => Core_EscortId_Extension_AttributeExchange::SCHEMA_CONTACT_WEB_DEFAULT,
			'phone' => Core_EscortId_Extension_AttributeExchange::SCHEMA_CONTACT_PHONE_DEFAULT,
			'address' => Core_EscortId_Extension_AttributeExchange::SCHEMA_CONTACT_POSTALADDRESS_HOME,
			'country' => Core_EscortId_Extension_AttributeExchange::SCHEMA_CONTACT_COUNTRY_HOME,
			'gender' => Core_EscortId_Extension_AttributeExchange::SCHEMA_GENDER,
			'birth_date' => Core_EscortId_Extension_AttributeExchange::SCHEMA_BIRTHDATE,

			'showname' => self::SCHEMA_SHOWNAME,
			'slogan' => self::SCHEMA_SLOGAN,
			'country' => self::SCHEMA_COUNTRY,
			'contact_phone' => self::SCHEMA_CONTACT_PHONE,
			'contact_phone_calling_code' => self::SCHEMA_CONTACT_PHONE_CALLINGCODE,
			'contact_phone_instructions' => self::SCHEMA_CONTACT_PHONE_INSTRUCTIONS,
			'contact_phone_withheld' => self::SCHEMA_CONTACT_PHONE_WITHHELD,
			'contact_email' => self::SCHEMA_CONTACT_EMAIL,
			'contact_url' => self::SCHEMA_CONTACT_URL,
			'contact_club_name' => self::SCHEMA_CONTACT_CLUBNAME,
			'contact_address_street' => self::SCHEMA_CONTACT_ADDRESS_STREET,
			'contact_address_apartment' => self::SCHEMA_CONTACT_ADDRESS_APARTMENT,
			'contact_other' => self::SCHEMA_CONTACT_OTHER,
			'biography_ethnicity' => self::SCHEMA_BIOGRAPHY_ETHNICITY,
			'biography_nationality' => self::SCHEMA_BIOGRAPHY_NATIONALITY,
			'biography_gender' => self::SCHEMA_BIOGRAPHY_GENDER,
			'biography_birth_date' => self::SCHEMA_BIOGRAPHY_BIRTHDATE,
			'biography_characteristics' => self::SCHEMA_BIOGRAPHY_CHARACTERISTICS,
			'physical_height' => self::SCHEMA_PHYSICAL_HEIGHT,
			'physical_weight' => self::SCHEMA_PHYSICAL_WEIGHT,
			'physical_bust' => self::SCHEMA_PHYSICAL_BUST,
			'physical_waist' => self::SCHEMA_PHYSICAL_WAIST,
			'physical_hip' => self::SCHEMA_PHYSICAL_HIP,
			'physical_cup_size' => self::SCHEMA_PHYSICAL_CUPSIZE,
			'physical_hair_color' => self::SCHEMA_PHYSICAL_HAIRCOLOR,
			'physical_eye_color' => self::SCHEMA_PHYSICAL_EYECOLOR,
			'physical_shoe_size' => self::SCHEMA_PHYSICAL_SHOESIZE,
			'physical_dress_size' => self::SCHEMA_PHYSICAL_DRESSSIZE,
			'physical_pubic_hair' => self::SCHEMA_PHYSICAL_PUBICHAIR,
			'physical_smoking' => self::SCHEMA_PHYSICAL_SMOKING,
			'physical_drinking' => self::SCHEMA_PHYSICAL_DRINKING,
			'languages_iso' => self::SCHEMA_LANGUAGES_ISO,
			'languages_level' => self::SCHEMA_LANGUAGES_LEVEL,
			'about_iso' => self::SCHEMA_ABOUT_ISO,
			'about_text' => self::SCHEMA_ABOUT_TEXT,
			'services_service_for' => self::SCHEMA_SERVICES_SERVICEFOR,
			'services_orientation' => self::SCHEMA_SERVICES_ORIENTATION,
			'services_additional_iso' => self::SCHEMA_SERVICES_ADDITIONAL_ISO,
			'services_additional_text' => self::SCHEMA_SERVICES_ADDITIONAL_TEXT,
			'services_offers_id' => self::SCHEMA_SERVICES_OFFERS_ID,
			'services_offers_price' => self::SCHEMA_SERVICES_OFFERS_PRICE,
			'services_offers_currency' => self::SCHEMA_SERVICES_OFFERS_CURRENCY,
			'working_times_all' => self::SCHEMA_WORKING_TIMES_ALL,
			'working_times_day' => self::SCHEMA_WORKING_TIMES_DAY,
			'working_times_from' => self::SCHEMA_WORKING_TIMES_FROM,
			'working_times_to' => self::SCHEMA_WORKING_TIMES_TO,
			'working_availability_incall' => self::SCHEMA_WORKING_AVAILABILITY_INCALL,
			'working_availability_incall_zip' => self::SCHEMA_WORKING_AVAILABILITY_INCALL_ZIP,
			'working_availability_incall_hotel_room_stars' => self::SCHEMA_WORKING_AVAILABILITY_INCALL_HOTELROOMSTARS,
			'working_availability_incall_other_text' => self::SCHEMA_WORKING_AVAILABILITY_INCALL_OTHERTEXT,
			'working_availability_outcall' => self::SCHEMA_WORKING_AVAILABILITY_OUTCALL,
			'working_availability_outcall_other_text' => self::SCHEMA_WORKING_AVAILABILITY_OUTCALL_OTHERTEXT,
			'working_locations' => self::SCHEMA_WORKING_LOCATIONS,
			'working_locations_home' => self::SCHEMA_WORKING_LOCATIONS_HOME,
			'working_locations_base' => self::SCHEMA_WORKING_LOCATIONS_BASE,
			'working_vacation_from' => self::SCHEMA_WORKING_VACATION_FROM,
			'working_vacation_to' => self::SCHEMA_WORKING_VACATION_TO,
			'rates_incall_duration' => self::SCHEMA_RATES_INCALL_DURATION,
			'rates_incall_unit' => self::SCHEMA_RATES_INCALL_UNIT,
			'rates_incall_price' => self::SCHEMA_RATES_INCALL_PRICE,
			'rates_incall_currency' => self::SCHEMA_RATES_INCALL_CURRENCY,
			'rates_incall_other' => self::SCHEMA_RATES_INCALL_OTHER,
			'rates_incall_other_price' => self::SCHEMA_RATES_INCALL_OTHER_PRICE,
			'rates_incall_other_currency' => self::SCHEMA_RATES_INCALL_OTHER_CURRENCY,
			'rates_outcall_duration' => self::SCHEMA_RATES_OUTCALL_DURATION,
			'rates_outcall_unit' => self::SCHEMA_RATES_OUTCALL_UNIT,
			'rates_outcall_price' => self::SCHEMA_RATES_OUTCALL_PRICE,
			'rates_outcall_currency' => self::SCHEMA_RATES_OUTCALL_CURRENCY,
			'rates_outcall_other' => self::SCHEMA_RATES_OUTCALL_OTHER,
			'rates_outcall_other_price' => self::SCHEMA_RATES_OUTCALL_OTHER_PRICE,
			'rates_outcall_other_currency' => self::SCHEMA_RATES_OUTCALL_OTHER_CURRENCY,
			// }}}
		);
	}

	public function getSupportedSchemas()
	{
		return array_values($this->_schema_map);
	}

	public function setId($id)
	{
		$this->_id = $id;
	}

	public function getSchemaValue($schema)
	{
		if ( ! $this->loaded ) {
			$this->load();
		}
		if ( $alias = array_search($schema, $this->_schema_map) ) {
			return $this[$alias];
		}
		return null;
	}

	public function setSchemaValue($schema, $value)
	{
		if ( $alias = array_search($schema, $this->_schema_map) ) {
			$this->$alias = $value;
			return true;
		}
		return false;
	}

	public function getSchemaGroups()
	{

	}

	public function getSchemaGroup($schema)
	{

	}

	public function load()
	{
		$profile = parent::db()->fetchRow('
			SELECT
				email, nickname, xml
			FROM ' . self::TABLE_USER_PROFILE . '
			WHERE id = ?
		', $this->_id);
		if ( ! $profile ) {
			$profile = array();
		}

		$this->fromXml($profile->xml);
	}

	public function store()
	{
		$result = $this->toXml();
		if ( is_string($result) ) {
			$data = array(
				'id' => $this->_id,
				'xml' => $result,
				'nickname' => $this->showname,
				'email' => $this->contact_email
			);

			if ( isset($this->type) ) {
				$data['type'] = $this->type;
			}
return $data;
			$this->exchangeArray($data);
			$this->save();
			return true;
		}
		else { // schema validation failed
			$this->_setError(implode("\n", $result));
			return false;
		}
	}

	protected function _setError($message)
	{
		$this->_error = $message;
	}

	public function getError()
	{
		return $this->_error;
	}

	// Method: fromXml - Fill this object via xml parsing {{{
	public function fromXml($xml)
	{
		$doc = new DOMDocument('1.0', 'UTF-8');
		$doc->loadXml($xml);

		$profile = $doc->childNodes->item(0);

		foreach ( $profile->childNodes as $node ) {
			switch ( $node->nodeName ) {
				case 'Showname':
					$this->nickname = $node->nodeValue;
					$this->showname = $node->nodeValue;
					break;
				case 'Slogan':
					$this->slogan = $node->nodeValue;
					break;
				case 'Country':
					$this->country = $node->nodeValue;
					break;
				case 'Contact':
					foreach ( $node->childNodes as $_node ) {
						switch ( $_node->nodeName ) {
							case 'Phone':
								$this->contact_phone = $_node->nodeValue;
								$this->contact_phone_calling_code = $_node->getAttribute('callingCode');
								$this->contact_phone_instructions = $_node->getAttribute('instructions');
								$this->contact_phone_withheld = $_node->getAttribute('withheld');
								$this->phone = $this->contact_phone_calling_code . $this->contact_phone;
								break;
							case 'Email':
								$this->email = $_node->nodeValue;
								$this->contact_email = $_node->nodeValue;
								break;
							case 'Url':
								$this->website = $_node->nodeValue;
								$this->contact_url = $_node->nodeValue;
								break;
							case 'ClubName':
								$this->contact_club_name = $_node->nodeValue;
								break;
							case 'Address':
								$address = array();
								foreach ( $_node->childNodes as $__node ) {
									switch ( $__node->nodeName ) {
										case 'Street':
											$address[0] = $__node->nodeValue;
											$this->contact_address_street = $__node->nodeValue;
											break;
										case 'Apartment':
											$address[1] = $__node->nodeValue;
											$this->contact_address_apartment = $__node->nodeValue;
											break;
									}
								}
								ksort($address);
								$this->address = implode(', ', $address);
								break;
							case 'Other':
								$this->contact_other = $_node->nodeValue;
								break;
						}
					}
					break;
				case 'Biography':
					foreach ( $node->childNodes as $_node ) {
						switch ( $_node->nodeName ) {
							case 'Ethnicity':
								$this->biography_ethnicity = $_node->nodeValue;
								break;
							case 'Nationality':
								$this->biography_nationality = $_node->nodeValue;
								break;
							case 'Gender':
								$this->gender = $_node->nodeValue;
								$this->biography_gender = $_node->nodeValue;
								break;
							case 'BirthDate':
								$this->birth_date = $_node->nodeValue;
								$this->biography_birth_date = $_node->nodeValue;
								break;
							case 'Characteristics':
								$this->biography_characteristics = $_node->nodeValue;
								break;
						}
					}
					break;
				case 'Physical':
					foreach ( $node->childNodes as $_node ) {
						switch ( $_node->nodeName ) {
							case 'Height':
								$this->physical_height = $_node->nodeValue;
								break;
							case 'Weight':
								$this->physical_weight = $_node->nodeValue;
								break;
							case 'Bust':
								$this->physical_bust = $_node->nodeValue;
								break;
							case 'Waist':
								$this->physical_waist = $_node->nodeValue;
								break;
							case 'Hip':
								$this->physical_hip = $_node->nodeValue;
								break;
							case 'CupSize':
								$this->physical_cup_size = $_node->nodeValue;
								break;
							case 'HairColor':
								$this->physical_hair_color = $_node->nodeValue;
								break;
							case 'EyeColor':
								$this->physical_eye_color = $_node->nodeValue;
								break;
							case 'ShoeSize':
								$this->physical_shoe_size = $_node->nodeValue;
								break;
							case 'DressSize':
								$this->physical_dress_size = $_node->nodeValue;
								break;
							case 'PubicHair':
								$this->physical_pubic_hair = $_node->nodeValue;
								break;
							case 'Smoking':
								$this->physical_smoking = $_node->nodeValue;
								break;
							case 'Drinking':
								$this->physical_drinking = $_node->nodeValue;
								break;
						}
					}
					break;
				case 'Languages':
					$this->languages_iso = array();
					$this->languages_level = array();
					foreach ( $node->childNodes as $_node ) {
						if ( $_node->nodeName != 'Language' ) continue;
						$this->languages_iso[] = $_node->getAttribute('iso');
						$this->languages_level[] = (int) $_node->getAttribute('level');
					}
					break;
				case 'About':
					$this->about_iso = array();
					$this->about_level = array();
					foreach ( $node->childNodes as $_node ) {
						if ( $_node->nodeName != 'Text' ) continue;
						$this->about_iso[] = $_node->getAttribute('iso');
						$this->about_text[] = $_node->nodeValue;
					}
					break;
				case 'Services':
					foreach ( $node->childNodes as $_node ) {
						switch ( $_node->nodeName ) {
							case 'ServiceFor':
								$this->services_service_for = explode(' ', $_node->nodeValue);
								break;
							case 'Orientation':
								$this->services_orientation = $_node->nodeValue;
								break;
							case 'Additional':
								$this->services_additional_iso = array();
								$this->services_additional_text = array();
								foreach ( $_node->childNodes as $__node ) {
									if ( $__node->nodeName != 'Text' ) continue;
									$this->services_additional_iso[] = $__node->getAttribute('iso');
									$this->services_additional_text[] = $__node->nodeValue;
								}
								break;
							case 'Offers':
								$this->services_offers_id = 
								$this->services_offers_price =
								$this->services_offers_currency = array();
								foreach ( $_node->childNodes as $__node ) {
									if ( $__node->nodeName != 'Service' ) continue;
									$this->services_offers_id[] = (int) $__node->getAttribute('id');
									$this->services_offers_price[] = (double) $__node->getAttribute('price');
									$this->services_offers_currency[] = $__node->getAttribute('currency');
								}
								break;
						}
					}
					break;
				case 'Working':
					foreach ( $node->childNodes as $_node ) {
						switch ( $_node->nodeName ) {
							case 'Times':
								$this->working_times_all = 'false';
								foreach ( $_node->childNodes as $__node ) {
									if ( $__node->nodeName == 'All' ) {
										$this->working_times_all = 'true';
										break;
									}

									if ( $__node->nodeName != 'Day' ) continue;
									if ( ! isset($this->working_times_day) ) {
										$this->working_times_day = array();
										$this->working_times_from = array();
										$this->working_times_to = array();
									}
									$this->working_times_day[] = (int) $__node->getAttribute('day');
									$this->working_times_from[] = $__node->getAttribute('from');
									$this->working_times_to[] = $__node->getAttribute('to');
								}
								break;
							case 'Availability':
								foreach ( $_node->childNodes as $__node ) {
									switch ( $__node->nodeName ) {
										case 'Incall':
											$zip = (int) $__node->getAttribute('zip');
											if ( $zip ) {
												$this->working_availability_incall_zip = $zip;
											}
											$option = $__node->childNodes->item(0);
											switch ( $option->nodeName ) {
												case 'PrivateApartment':
													$this->working_availability_incall = 'private_apartment';
													break;
												case 'HotelRoom':
													$this->working_availability_incall = 'hotel_room';
													$this->working_availability_incall_hotel_room_stars = (int) $option->getAttribute('stars');
													break;
												case 'Club':
													$this->working_availability_incall = 'club';
													break;
												case 'Other':
													$this->working_availability_incall = 'other';
													$this->working_availability_incall_other_text = $option->nodeValue;
													break;
											}
											break;
										case 'Outcall':
											$option = $__node->childNodes->item(0);
											switch ( $option->nodeName ) {
												case 'Hotel':
													$this->working_availability_outcall = 'hotel';
													break;
												case 'Home':
													$this->working_availability_outcall = 'home';
													break;
												case 'HotelHome':
													$this->working_availability_outcall = 'hotel_home';
													break;
												case 'Other':
													$this->working_availability_outcall = 'other';
													$this->working_availability_outcall_other_text = $option->nodeValue;
													break;
											}
											break;
									}
								}
								break;
							case 'Locations':
								$this->working_locations_home = (int) $_node->getAttribute('home');
								$this->working_locations = array();
								foreach ( $_node->childNodes as $__node ) {
									if ( $__node->nodeName != 'City' ) continue;
									$this->working_locations[] = (int) $__node->getAttribute('id');
									if ( 'true' == $__node->getAttribute('base') ) {
										$this->working_locations_base = (int) $__node->getAttribute('id');
									}
								}
								break;
							case 'Vacation':
								$this->working_vacation_from = $_node->getAttribute('from');
								$this->working_vacation_to = $_node->getAttribute('to');
								break;
						}
					}
					break;
				case 'Rates':
					foreach ( $node->childNodes as $_node ) {
						if ( ! in_array($_node->nodeName, array('Incall', 'Outcall')) ) continue;
						$key = 'rates_' . strtolower($_node->nodeName);
						
						foreach ( $_node->childNodes as $__node ) {
							if ( $__node->nodeName == 'Rate' ) {
								if ( ! isset($this->{$key . '_duration'}) ) {
									$this->{$key . '_duration'} = array();
									$this->{$key . '_unit'} = array();
									$this->{$key . '_price'} = array();
									$this->{$key . '_currency'} = array();
								}
								$this->{$key . '_duration'}[] = (int) $__node->getAttribute('duration');
								$this->{$key . '_unit'}[] = $__node->getAttribute('unit');
								$this->{$key . '_price'}[] = (double) $__node->getAttribute('price');
								$this->{$key . '_currency'}[] = $__node->getAttribute('currency');
							}
							elseif ( $__node->nodeName == 'Other' ) {
								foreach ( $__node->childNodes as $___node ) {
									switch ( $___node->nodeName ) {
										case 'AddtionalHour':
											$this->{$key . '_other'}[] = 'additional_hour';
											break;
										case 'Overnight':
											$this->{$key . '_other'}[] = 'overnight';
											break;
										case 'DinnerDate':
											$this->{$key . '_other'}[] = 'dinner_date';
											break;
										case 'Weekend':
											$this->{$key . '_other'}[] = 'weekend';
											break;
									}
									if ( ! isset($this->{$key . '_other'}) ) {
										$this->{$key . '_other'} = array();
										$this->{$key . '_other_price'} = array();
										$this->{$key . '_other_currency'} = array();
									}
									$this->{$key . '_other_price'}[] = (double) $___node->getAttribute('price');
									$this->{$key . '_other_currency'}[] = $___node->getAttribute('currency');
								}
							}
						}
					}
					break;
			}
		}
	}
	// }}}

	// Method: toXml - Serialize this object into xml string {{{
	public function toXml()
	{
		$doc = new DOMDocument('1.0', 'UTF-8');

		$profile = $doc->createElementNS(self::NAMESPACE_URL, 'Profile');
		$profile->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
		$profile->setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'schemaLocation', self::NAMESPACE_URL . ' ' . self::SCHEMA_DEFINITION_FILE);
		$doc->appendChild($profile);

		$showname = $doc->createElement('Showname', $this->showname);
		$profile->appendChild($showname);

		$slogan = $doc->createElement('Slogan', $this->slogan);
		$profile->appendChild($slogan);

		if ( $this->country ) {
			$country = $doc->createElement('Country', strtoupper($this->country));
			$profile->appendChild($country);
		}

		// Contact {{{
		$contact = $doc->createElement('Contact');
		$profile->appendChild($contact);

		if ( $this->contact_phone ) {
			$phone = $doc->createElement('Phone', $this->contact_phone);
			$phone->setAttribute('callingCode', $this->contact_phone_calling_code);
			$phone->setAttribute('instructions', $this->contact_phone_instructions);
			$phone->setAttribute('withheld', $this->withheld ? 'true' : 'false');
			$contact->appendChild($phone);
		}

		$email = $doc->createElement('Email', $this->contact_email);
		$contact->appendChild($email);

		$url = $doc->createElement('Url', $this->contact_url);
		$contact->appendChild($url);

		$clubName = $doc->createElement('ClubName', $this->contact_club_name);
		$contact->appendChild($clubName);

		$address = $doc->createElement('Address');
		$street = $doc->createElement('Street', $this->contact_address_street);
		$apt = $doc->createElement('Apartment', $this->contact_address_apartment);
		$address->appendChild($street);
		$address->appendChild($apt);
		$contact->appendChild($address);

		$other = $doc->createElement('Other', $this->contact_other);
		$contact->appendChild($other);
		// }}}

		// Biography {{{
		$biography = $doc->createElement('Biography');

		if ( $this->biography_ethnicity ) {
			$ethnicity = $doc->createElement('Ethnicity', $this->biography_ethnicity);
			$biography->appendChild($ethnicity);
		}

		if ( $this->biography_nationality ) {
			$nationality = $doc->createElement('Nationality', $this->biography_nationality);
			$biography->appendChild($nationality);
		}

		if ( $this->biography_gender ) {
			$gender = $doc->createElement('Gender', $this->biography_gender);
			$biography->appendChild($gender);
		}

		if ( $this->biography_birth_date ) {
			$birthDate = $doc->createElement('BirthDate', $this->biography_birth_date);
			$biography->appendChild($birthDate);
		}

		if ( $this->biography_characteristics ) {
			$characteristics = $doc->createElement('Characteristics', $this->biography_characteristics);
			//$biography->appendChild($characteristics);
		}

		if ( $biography->hasChildNodes() ) {
			$profile->appendChild($biography);
		}
		// }}}

		// Physical {{{
		$physical = $doc->createElement('Physical');

		if ( $this->physical_height ) {
			$height = $doc->createElement('Height', $this->physical_height);
			$physical->appendChild($height);
		}

		if ( $this->physical_weight ) {
			$weight = $doc->createElement('Weight', $this->physical_weight);
			$physical->appendChild($weight);
		}

		if ( $this->physical_bust ) {
			$bust = $doc->createElement('Bust', $this->physical_bust);
			$physical->appendChild($bust);
		}

		if ( $this->physical_waist ) {
			$waist = $doc->createElement('Waist', $this->physical_waist);
			$physical->appendChild($waist);
		}

		if ( $this->physical_hip ) {
			$hip = $doc->createElement('Hip', $this->physical_hip);
			$physical->appendChild($hip);
		}

		if ( $this->physical_cup_size ) {
			$bustSize = $doc->createElement('CupSize', $this->physical_cup_size);
			$physical->appendChild($bustSize);
		}

		if ( $this->physical_hair_color ) {
			$hairColor = $doc->createElement('HairColor', $this->physical_hair_color);
			$physical->appendChild($hairColor);
		}

		if ( $this->physical_eye_color ) {
			$eyeColor = $doc->createElement('EyeColor', $this->physical_eye_color);
			$physical->appendChild($eyeColor);
		}

		if ( $this->physical_shoe_size ) {
			$shoeSize = $doc->createElement('ShoeSize', $this->physical_shoe_size);
			$physical->appendChild($shoeSize);
		}

		if ( $this->physical_dress_size ) {
			$dressSize = $doc->createElement('DressSize', $this->physical_dress_size);
			$physical->appendChild($dressSize);
		}

		if ( $this->physical_pubic_hair ) {
			$pubicHair = $doc->createElement('PubicHair', $this->physical_pubic_hair);
			$physical->appendChild($pubicHair);
		}

		if ( $this->physical_smoking ) {
			$smoking = $doc->createElement('Smoking', $this->physical_smoking);
			$physical->appendChild($smoking);
		}

		if ( $this->physical_drinking ) {
			$drinking = $doc->createElement('Drinking', $this->physical_drinking);
			$physical->appendChild($drinking);
		}

		if ( $physical->hasChildNodes() ) {
			$profile->appendChild($physical);
		}
		// }}}

		// Languages {{{
		$languages = $doc->createElement('Languages');
		
		if ( is_array($this->languages_iso) ) {
			foreach ( $this->languages_iso as $i => $iso ) {
				$level = $this->languages_level[$i];

				$language = $doc->createElement('Language');
				$language->setAttribute('iso', strtoupper($iso));
				$language->setAttribute('level', (int) $level);
				$languages->appendChild($language);
			}
		}

		if ( $languages->hasChildNodes() ) {
			$profile->appendChild($languages);
		}
		// }}}

		// About {{{
		$about = $doc->createElement('About');

		if ( is_array($this->about_iso) ) {
			foreach ( $this->about_iso as $i => $iso ) {
				$text = $this->about_text[$i];
				
				$text = $doc->createElement('Text', $text);
				$text->setAttribute('iso', strtoupper($iso));
				$about->appendChild($text);
			}
		}

		if ( $about->hasChildNodes() ) {
			$profile->appendChild($about);
		}
		// }}}

		// Services {{{
		$services = $doc->createElement('Services');
		$profile->appendChild($services);

		if ( is_array($this->services_service_for) ) {
			$serviceFor = $doc->createElement('ServiceFor', implode(' ', $this->services_service_for));
			$services->appendChild($serviceFor);
		}

		if ( $this->services_orientation ) {
			$orientation = $doc->createElement('Orientation', $this->services_orientation);
			$services->appendChild($orientation);
		}

		$additional = $doc->createElement('Additional');

		if ( is_array($this->services_additional_iso) ) {
			foreach ( $this->services_additional_iso as $i => $iso ) {
				$text = $this->services_additional_text[$i];

				$text = $doc->createElement('Text', $text);
				$text->setAttribute('iso', strtoupper($iso));
				$additional->appendChild($text);
			}
		}

		if ( $additional->hasChildNodes() ) {
			$services->appendChild($additional);
		}

		$offers = $doc->createElement('Offers');

		if ( is_array($this->services_offers_id) ) {
			foreach ( $this->services_offers_id as $i => $id ) {
				$id = (int) $id;
				$price = (double) $this->services_offers_price[$i];
				$currency = $this->services_offers_currency[$i];

				$service = $doc->createElement('Service');
				$service->setAttribute('id', $id);
				$service->setAttribute('price', $price);
				$service->setAttribute('currency', $currency);
				$offers->appendChild($service);
			}
		}

		if ( $offers->hasChildNodes() ) {
			$services->appendChild($offers);
		}
		// }}}

		// Working {{{
		$working = $doc->createElement('Working');

		$times = $doc->createElement('Times');
		
		if ( 'true' == $this->working_times_all ) {
			$all = $doc->createElement('All');
			$times->appendChild($all);
		}
		else {
			if ( is_array($this->working_times_day) ) {
				foreach ( $this->working_times_day as $i => $day ) {
					$day_index = (int) $day;
					$from = $this->working_times_from[$i];
					$to = $this->working_times_to[$i];
					
					$day = $doc->createElement('Day');
					$day->setAttribute('day', $day_index);
					$day->setAttribute('from', $from);
					$day->setAttribute('to', $to);
					$times->appendChild($day);
				}
			}
		}

		if ( $times->hasChildNodes() ) {
			$working->appendChild($times);
		}

		$availability = $doc->createElement('Availability');
		$working->appendChild($availability);

		$incall = $doc->createElement('Incall');
		if ( (int) $this->working_availability_incall_zip ) {
			$incall->setAttribute('zip', (int) $this->working_availability_incall_zip);
		}

		switch ( $this->working_availability_incall ) {
			case 'private_apartment':
				$element = $doc->createElement('PrivateApartment');
				break;
			case 'hotel_room':
				$element = $doc->createElement('HotelRoom');
				$element->setAttribute('stars', (int) $this->working_availability_incall_hotel_room_stars);
				break;
			case 'club':
				$element = $doc->createElement('Club');
				break;
			case 'other':
				$element = $doc->createElement('Other', $this->working_availability_incall_other_text);
				break;
		}

		if ( isset($element) ) {
			$incall->appendChild($element);
			unset($element);
		}

		if ( $incall->hasChildNodes() ) {
			$availability->appendChild($incall);
		}

		$outcall = $doc->createElement('Outcall');

		switch ( $this->working_availability_outcall ) {
			case 'home':
				$element = $doc->createElement('Home');
				break;
			case 'hotel':
				$element = $doc->createElement('Hotel');
				break;
			case 'hotel_home':
				$element = $doc->createElement('HotelHome');
				break;
			case 'other':
				$element = $doc->createElement('Other', $this->working_availability_incall_other_text);
				break;
		}

		if ( isset($element) ) {
			$outcall->appendChild($element);
		}

		if ( $outcall->hasChildNodes() ) {
			$availability->appendChild($outcall);
		}

		$locations = $doc->createElement('Locations');
		if ( (int) $this->working_locations_home ) {
			$locations->setAttribute('home', (int) $this->working_locations_home);
		}

		if ( is_array($this->working_locations) ) {
			foreach ( $this->working_locations as $id ) {
				$id = (int) $id;
				$city = $doc->createElement('City');
				$city->setAttribute('id', $id);
				if ( (int) $this->working_locations_base == $id ) {
					$city->setAttribute('base', 'true');
				}
				$locations->appendChild($city);
			}
		}

		if ( $locations->hasChildNodes ) {
			$working->appendChild($locations);
		}

		if ( $this->working_vacation_from && $this->working_vacation_to ) {
			$vacation = $doc->createElement('Vacation');
			$vacation->setAttribute('from', $this->working_vacation_from);
			$vacation->setAttribute('to', $this->working_vacation_to);
			$working->appendChild($vacation);
		}

		if ( $working->hasChildNodes() ) {
			$profile->appendChild($working);
		}
		// }}}

		// Rates {{{
		$rates = $doc->createElement('Rates');
		$profile->appendChild($rates);

		// Incall {{{
		$incall = $doc->createElement('Incall');
		$rates->appendChild($incall);

		if ( isset($this->rates_incall_duration) ) {
			foreach ( $this->rates_incall_duration as $i => $duration ) {
				$duration = (int) $duration;
				$unit = $this->rates_incall_unit[$i];
				$price = $this->rates_incall_price[$i];
				$currency = $this->rates_incall_currency[$i];

				$rate = $doc->createElement('Rate');
				$rate->setAttribute('duration', $duration);
				$rate->setAttribute('unit', $unit);
				$rate->setAttribute('price', $price);
				$rate->setAttribute('currency', $currency);
				$incall->appendChild($rate);
			}
		}

		if ( isset($this->rates_incall_other) ) {
			foreach ( $this->rates_incall_other as $i => $other ) {
				if ( ! isset($other_element) )
					$other_element = $doc->createElement('Other');

				switch ( $other ) {
					case 'additional_hour':
						$element = $doc->createElement('AdditionalHour');
						break;
					case 'overnight':
						$element = $doc->createElement('Overnight');
						break;
					case 'dinner_date':
						$element = $doc->createElement('DinnerDate');
						break;
					case 'weekend':
						$element = $doc->createElement('Weekend');
						break;
					default: continue;
				}

				$price = $this->rates_incall_other_price[$i];
				$currency = $this->rates_incall_other_currency[$i];

				$element->setAttribute('price', $price);
				$element->setAttribute('currency', $currency);
				$other_element->appendChild($element);
			}
		}
		if ( isset($other_element) ) {
			$incall->appendChild($other_element);
			unset($other_element);
		}
		// }}}

		// Outcall {{{
		$outcall = $doc->createElement('Outcall');
		$rates->appendChild($outcall);

		if ( isset($this->rates_outcall_duration) ) {
			foreach ( $this->rates_outcall_duration as $i => $duration ) {
				$duration = (int) $duration;
				$unit = $this->rates_outcall_unit[$i];
				$price = $this->rates_outcall_price[$i];
				$currency = $this->rates_outcall_currency[$i];

				$rate = $doc->createElement('Rate');
				$rate->setAttribute('duration', $duration);
				$rate->setAttribute('unit', $unit);
				$rate->setAttribute('price', $price);
				$rate->setAttribute('currency', $currency);
				$outcall->appendChild($rate);
			}
		}

		if ( isset($this->rates_outcall_other) ) {
			foreach ( $this->rates_outcall_other as $i => $other ) {
				if ( ! isset($other_element) )
					$other_element = $doc->createElement('Other');

				switch ( $other ) {
					case 'additional_hour':
						$element = $doc->createElement('AdditionalHour');
						break;
					case 'overnight':
						$element = $doc->createElement('Overnight');
						break;
					case 'dinner_date':
						$element = $doc->createElement('DinnerDate');
						break;
					case 'weekend':
						$element = $doc->createElement('Weekend');
						break;
					default: continue;
				}

				$price = $this->rates_outcall_other_price[$i];
				$currency = $this->rates_outcall_other_currency[$i];

				$element->setAttribute('price', $price);
				$element->setAttribute('currency', $currency);
				$other_element->appendChild($element);
			}
		}
		if ( isset($other_element) )
			$outcall->appendChild($other_element);
		// }}}

		// }}}
		
		global $__xml_parsing_errors;
		$__xml_parsing_errors = array();
		$original = set_error_handler(create_function('$errno, $errstr, $errfile, $errline', 'global $__xml_parsing_errors;
			$__xml_parsing_errors[] = $errstr;
			return true;'));
		if ( false === $doc->schemaValidate(self::NAMESPACE_URL . '/' . self::SCHEMA_DEFINITION_FILE) ) {
			return $__xml_parsing_errors;
		}
		set_error_handler($original);

		return $doc->saveXml();
	}
	// }}}
}

