<?php

class Core_Tools
{
	static public function makeSlug($string)
	{
		$regex = '/[^\p{N}\p{L}\-\s]/u';
		$string = preg_replace($regex, ' ', $string);
		$string = trim(preg_replace('/[-\s]+/u', '-', $string), '-');
		$string = mb_strtolower($string, 'UTF-8');

		return $string;
	}

	static public function capitalize($string, $first_letter = false)
	{
		$string = preg_replace('#[-_]+#', ' ', $string);
		$string = ucwords($string);
		if ( ! $first_letter ) $string[0] = strtolower($string[0]);
		$string = preg_replace('#\s+#', '', $string);

		return $string;
	}
	
	/**
	 * WARNING!!! This is experimental functions!
	 * 
	 * @param type $string
	 * @return string 
	 */
	static public function guessLang($string)
	{
		$lang = _lang();
		if ( preg_match('#^[a-z]#iu', $string) ) {
			$lang = 'en';
		}
		elseif ( preg_match('/^[а-яА-Я]/i', $string) ) {
			$lang = 'ru';
		}
		elseif ( preg_match('/^[ա-ֆԱ-և]/u', $string) ) {
			$lang = 'am';
		}
		
		return $lang;
	}
}
