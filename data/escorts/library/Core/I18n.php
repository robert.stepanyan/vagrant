<?php

class Core_I18n_Exception_InvalidLangId extends Exception {}
class Core_I18n extends Core_Model
{
	/**
	 * @var Core_I18n_Dictionary
	 */
	static private $_dictionary;

	/**
	 * Specially hardcoded to avoid extra select
	 *
	 * @var array
	 */
	static private $_langs = array(1 => 'en');
	static private $_lang_ids = array('en' => 1);
	static private $_locales = array('en' => 'en_US');
	static private $_lang_titles = array(1 => 'English');

	static private $_def_lang = 'en';
	static private $_lang_id = 'en';

	static public function getLangs()
	{
		return self::$_langs;
	}

	static public function getLangTitle($lang_id)
	{
		return isset(self::$_lang_titles[$lang_id])
			? self::$_lang_titles[$lang_id]
			: null;
	}

	static public function setLang($lang_id = null)
	{
		if ( null === $lang_id ) $lang_id = self::getDefLang();

		if ( ! in_array($lang_id, self::getLangs()) ) {
			$lang_id = self::getDefLang();
		}

		self::$_lang_id = $lang_id;
		self::request()->setParam('lang', self::$_lang_id);
	}

	static public function getLang($lang_id = null)
	{
		if ( null === $lang_id )
			return self::$_lang_id;
		return isset(self::$_langs[$lang_id]) ? self::$_langs[$lang_id] : null;
	}

	static public function getLangId($lang_id = null)
	{
		$lang_id = null === $lang_id ? self::getLang() : $lang_id;
		return self::$_lang_ids[$lang_id];
	}

	static public function getDefLang()
	{
		return self::$_def_lang;
	}

	static public function getDefLangId()
	{
		return self::getLangId(self::getDefLang());
	}

	static public function getLocale($lang_id)
	{
		if ( ! isset(self::$_locales[$lang_id]) ) {
			throw new Core_I18n_Exception_InvalidLangId();
		}

		return self::$_locales[$lang_id];
	}

	/**
	 * @return Core_I18n_Dictionary
	 */
	static public function getDictionary()
	{
		return self::$_dictionary;
	}

	static public function setDictionary(Core_I18n_Dictionary $dictionary)
	{
		self::$_dictionary = $dictionary;
	}

	static public function init()
	{
		$config = self::config();

		self::$_dictionary = new Core_I18n_Dictionary();
		self::$_dictionary->load();
	}
}
