<?php

class Core_Model extends ArrayObject
{
	protected $_table = '';
	protected $_keys = array();

	public function __construct($data = array())
	{
		$data = (array) $data;
		$data = $this->extract($this->_keys, $data);
		parent::__construct($data, parent::ARRAY_AS_PROPS);
	}

	/**
	 *
	 * @return Zend_Db_Adapter_Mysqli
	 */
	static public function db()
	{
		return Zend_Registry::get('db');
	}

	/**
	 *
	 * @return Zend_Config_Xml
	 */
	static public function config()
	{
		return Zend_Registry::get('config');
	}

	static public function request()
	{
		return Zend_Controller_Front::getInstance()->getRequest();
	}
	
	public function save($data = array())
	{
		$data = (array) $data;
		$this->merge($this->extract($this->_keys, $data));

		if ( isset($this->id) ) {
			self::db()->update($this->_table, (array) $this, self::quote('id = ?', $this->id));
		}
		else {
			self::db()->insert($this->_table, (array) $this);
			$this->id = self::db()->lastInsertId();
		}
		return $this;
	}

	static public function loc_field($field)
	{
		$result = array();
		foreach ( Core_I18n::getLangs() as $id => $iso ) {
			$result[$id] = str_replace('%', $id, $field);
		}
		return implode(', ', $result);
	}

	static public function loc_join($table, $alias, $ref)
	{
		$result = array();
		foreach ( Core_I18n::getLangs() as $id => $iso ) {
			$result[$id] = 'INNER JOIN ' . $table . ' ' . str_replace('%', $id, $alias) . ' ON ' . str_replace('%', $id, $alias) . '.ref_id = ' . $ref . ' AND ' . str_replace('%', $id, $alias) . '.lang_id = ' . $id;
		}
		return implode("\n", $result);
	}
	
	static public function fetchAll($sql, $bind = array(), $mode = null)
	{
		return self::db()->fetchAll($sql, $bind, $mode);
	}

	static public function fetchRow($sql, $bind = array(), $mode = null)
	{
		return self::db()->fetchRow($sql, $bind, $mode);
	}

	static public function fetchOne($sql, $bind = array(), $mode = null)
	{
		return self::db()->fetchOne($sql, $bind, $mode);
	}

	static public function quote($expr, $value = null)
	{
		if ( is_null($value) ) {
			return self::db()->quote($expr);
		}

		return self::db()->quoteInto($expr, $value);
	}

	static public function exists($table, $criteria)
	{
		$sql = '
			SELECT TRUE
			FROM ' . $table . '
			WHERE ' . $criteria . '
		';
		return (int) self::db()->fetchOne($sql);
	}

	static public function field($field)
	{
		return $field . '_' . _lang();
	}

	static public function fields($field)
	{
		$fields = array();
		foreach ( Core_I18n::getLangs() as $id => $iso ) {
			$fields[] = $field . '_' . $iso;
		}
		return $fields;
	}

	public function extract(array $keys, $from = null)
	{
		if ( null === $from )
			$from = $this->getArrayCopy();
		$result = array();
		foreach ( $keys as $key ) {
			if ( array_key_exists($key, $from) ) {
				$result[$key] = $from[$key];
			}
		}
		return $result;
	}

	public function merge($data)
	{
		$this->exchangeArray(array_merge($this->getArrayCopy(), $data));
	}

	static public function date($date = null, $with_time = true, $format = null)
	{
		if ( null === $date ) $date = time();
		
		if ( null === $format ) {
			$format = 'Y-m-d';
			if ( $with_time ) {
				$format .= ' H:i:s';
			}
		}
		
		return date($format, $date);
	}

	static public function metaData($prefix)
	{
		$join = array(
			'LEFT JOIN bo_users bu_c ON bu_c.id = ' . $prefix . '.created_by',
			'LEFT JOIN bo_users bu_u ON bu_u.id = ' . $prefix . '.updated_by'
		);

		$field = array(
			$prefix . '.date_created', $prefix . '.date_updated', $prefix . '.created_by', $prefix . '.updated_by',
			'bu_c.username AS created_by_username', 'CONCAT(bu_c.first_name, " ", bu_c.last_name) AS created_by_name',
			'bu_u.username AS updated_by_username', 'CONCAT(bu_u.first_name, " ", bu_u.last_name) AS updated_by_name'
		);

		return array($join, $field);
	}
}
