<?php

class Core_EscortId
{
	const BASE_URL = 'http://www.escortid.com.dev';
	const ID_TEMPLATE = 'http://escortid.com.dev/account/{username}';

	static protected $_client;

	static public function getJsUrl()
	{
		return self::BASE_URL . '/js/escortid.js';
	}

	static protected function _getClient()
	{
		if ( empty(self::$_client) ) {
			self::$_client = new Core_JsonRpc_Client(self::BASE_URL . '/provider/rpc');
		}
		return self::$_client;
	}

	static public function checkUsername($username)
	{
		return self::_getClient()->call('checkUsername', array($username));
	}

	static public function checkEmail($email)
	{
		return self::_getClient()->call('checkEmail', array($email));
	}

	static public function register($username, $password, $type)
	{
		return self::_getClient()->call('register', array($username, $password, $type));
	}

	static public function username2id($username)
	{
		$id = str_replace('{username}', $username, self::ID_TEMPLATE);
		return $id;
	}
}

