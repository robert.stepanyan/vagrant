<?php

class Core_Images_Storage_File
{
	/**
	 * Contains host, port, login, password and the root dir of ftp server
	 *
	 * @var array
	 */
	protected $_opts = array();

	private $_root = '/var/www/media.downtown.int/www/media';

	/**
	 * The ftp connection resource
	 *
	 * @var resource
	 */
	protected $_conn = FALSE;

	public function __construct(array $options = array())
	{
		$this->_root = _config()->media->repository;
	}

	public function connect()
	{
		
	}

	/**
	 * Uploads a file from local server to remote ftp storage
	 *
	 * @param string $local
	 * @param string $remote
	 * @param FTP_BINARY|FTP_ASCII $mode
	 * @return bool
	 * @author GuGo
	 */
	public function store($local, $remote = null, $mode = FTP_BINARY)
	{
		return (bool) @copy($local, $this->_root . $remote);
	}

	/**
	 * Downloads a file from remote ftp storage
	 *
	 * @param string $local
	 * @param string $remotem
	 * @param FTP_BINARY|FTP_ASCII $mdoe
	 * @return bool
	 * @author GuGo
	 */
	public function retreive($local, $remote, $mdoe = FTP_BINARY)
	{
		return (bool) @copy($this->_root . $remote, $local);
	}

	/**
	 * Deletes remote file on the ftp server
	 *
	 * @param string $remote
	 * @return bool
	 * @author GuGo
	 */
	public function delete($remote)
	{
		return (bool) @unlink($this->_root . $remote);
	}

	/**
	 * Returns the list of items in the path
	 *
	 * @param string $remote
	 * @return array
	 * @author GuGo
	 */
	public function getFiles($remote)
	{
		$content = array();
		foreach ( new DirectoryIterator($this->_root . $remote) as $item ) {
			if ( $item->isFile() ) {
				$content[] = $item->getFilename();//trim($remote, PATH_SEPARATOR) . '/' . $item->getFilename();
			}
		}
		return $content;
	}

	/**
	 * Creates remote directory on the ftp server
	 *
	 * @param string $remote
	 * @return void
	 * @author GuGo
	 */
	public function makeDir($remote)
	{
		return (bool) @mkdir($this->_root . $remote);
	}

	/**
	 * Chmods the target to 0777
	 *
	 * @param string $remote
	 * @return bool
	 * @author GuGo
	 */
	public function makeWritable($remote)
	{
		return true;
	}

	/**
	 * Chmods the target to 0775
	 *
	 * @param string $remote
	 * @return bool
	 * @author GuGo
	 */
	public function makeExecutable($remote)
	{
		return true;
	}

	public function disconnect()
	{
		
	}

}
