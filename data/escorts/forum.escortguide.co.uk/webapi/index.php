<?php
//  error_reporting(-1);
// ini_set('display_errors', true);

    
$sess_dir = '/tmp/forum_sessions';
if ( ! is_dir($sess_dir) ) {
   mkdir($sess_dir);
   chmod(777, $sess_dir);
}

ini_set('session.save_handler', 'files');
ini_set('session.save_path', '/tmp/forum_sessions');



if($_GET['s_set']) {
    $_SESSION['test_value'] = date('y-m-d H:i:s');
    var_dump('Value has been set'); die;
}

if($_GET['s_get']) {
    var_dump('getting data from timeline of ' . $_SESSION['test_value']); die;
}



if($_GET['s_xuy']) {
    error_reporting(-1);
    ini_set('display_errors', true);

    session_start();
    $_SESSION['sessionid']  = session_id();
    $_SESSION['hello2']     = 'Hello World';
    $_SESSION['today']      = date('l');   // (lowercase 'L') shows full text day


    echo '<pre>'; // cosmetic line-spacing
      print_r( $_SESSION );
    echo '</pre>';
    die;


    phpinfo();
    die;
}

define('API_KEY', 'forum_uk_cubix_2021');

require_once 'init.inc.php';

$LOG->info('Got request (' . $_SERVER['REQUEST_URI'] . ')');

$key = isset($_GET['key']) ? $_GET['key'] : null;

if ( $key != API_KEY ) {
	$LOG->err('Wrong api key');
	die('error');
}

$sid = isset($_GET['sid']) ? $_GET['sid'] : null;

$cmd = isset($_GET['cmd']) ? $_GET['cmd'] : null;

if ( is_null($cmd) || strlen($cmd) == 0 ) {
	$LOG->err('No command has been specified');
	die('error');
}

if ( ! method_exists($CMDS, $cmd) ) {
	$LOG->err("the command '$cmd' is not supported by api");
	die('error');
}

$cmd = new ReflectionMethod($CMDS, $cmd);

$args = $log_args = array();

foreach ($cmd->getParameters() as $param) {
	$param = $param->name;
	$value = isset($_GET[$param]) ? $_GET[$param] : null;
	$args[] = $value;
	$log_args[] = "'" . $value . "'";
}

$LOG->info('Calling ' . $cmd->getName() . '(' . implode(', ', $log_args) . ')');
$result = $cmd->invokeArgs($CMDS, $args);

if ( $result === FALSE ) {
	$LOG->err('Result: ' . $result);
	die('error');
}

$LOG->info('Result: ' . $result);
echo $result;
