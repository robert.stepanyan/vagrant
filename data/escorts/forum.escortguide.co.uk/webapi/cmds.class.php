<?php

class WebApi_Commands
{
	const DOMAIN = '.escortguide.co.uk';
	const URL = 'https://forum.escortguide.co.uk/';

	public function RegisterUser($user_name, $email) 
	{
		global $DB;
		
		$member_id = $DB->fetchOne('SELECT member_id FROM members WHERE name = ?', $user_name);
		
		if ( $member_id ) 
		{
			$DB->delete('members', $DB->quoteInto('member_id = ?', $member_id));
			$DB->delete('profile_portal', $DB->quoteInto('pp_member_id = ?', $member_id));
			$DB->delete('pfields_content', $DB->quoteInto('member_id = ?', $member_id));
		}
				
		$DB->insert('members', array(
			'name' => $user_name,
			'member_group_id' => 3,
			'email' => $email,
			'joined' => time(),
			'allow_admin_mails' => 1,
			'language' => 1,
			'msg_show_notification' => 1,
			'members_auto_dst' => 1,
			'members_display_name' => $user_name,
			'members_seo_name' => $user_name,
			'members_l_display_name' => $user_name,
			'members_l_username' => $user_name,
			'members_profile_views' => 1,
			'member_banned' => 0,
			'member_uploader' => 'flash'
		));
		
		$id = $DB->lastInsertId();
		
		$DB->insert('profile_portal', array(
			'pp_member_id' => $id,
			'pp_setting_count_friends' => 1,
			'pp_setting_count_comments' => 1,
			'pp_setting_count_visitors' => 1
		));
		
		$DB->insert('pfields_content', array(
			'member_id' => $id
		));
	}
	
	public function UpdateEmail($user_name, $email) 
	{
		global $DB;
		
		$DB->update('members', array('email' => $email), $DB->quoteInto('name = ?', $user_name));
	}
	
	public function DeleteUser($user_name) 
	{
		global $DB;
		
		$member_id = $DB->fetchOne('SELECT member_id FROM members WHERE name = ?', $user_name);
		
		if ($member_id)
		{
			$DB->delete('members', $DB->quoteInto('member_id = ?', $member_id));
			$DB->delete('profile_portal', $DB->quoteInto('pp_member_id = ?', $member_id));
			$DB->delete('pfields_content', $DB->quoteInto('member_id = ?', $member_id));
		}
	}
	
	public function BannUser($user_name, $status) 
	{
		global $DB;
		
		if ($status == 1)
		{
			$member_group_id = 5;
			$member_banned = 1;
			$members_auto_dst = 0;
		}
		else
		{
			$member_group_id = 3;
			$member_banned = 0;
			$members_auto_dst = 1;
		}
				
		$DB->update('members', array(
			'member_group_id' => $member_group_id,
			'member_banned' => $member_banned,
			'members_auto_dst' => $members_auto_dst
		), $DB->quoteInto('name = ?', $user_name));
	}
	
	public function Login($user_name = null, $email = null) 
	{
		global $DB, $sid, $LOG;
		
		$sc = 'f45F7ndfDSA';
		$v = md5($user_name . $sc);
		
		$user = $DB->query('SELECT * FROM members WHERE name = ?', $user_name)->fetch();
	
		if ( ! $user ) {
			$this->RegisterUser($user_name, $email);
			$this->Login($user_name, $email);
			return TRUE;
		}
		else {
			$do = false;
			
			if (isset($_REQUEST['has_package']))
			{
				if ($_REQUEST['has_package'] == 1)
				{
					$r_p = '';
					$do = true;
				}
				elseif ($_REQUEST['has_package'] == 0)
				{
					$r_p = '1';
					$do = true;
				}
				
				if ($do)
				{
					if ($user['restrict_post'] != $r_p)
						$DB->update('members', array('restrict_post' => $r_p), $DB->quoteInto('member_id = ?', $user['member_id']));
				}
			}
						
			$url = self::URL . 'index.php?section=login&app=core&module=global&do=process';
			$params = array(
				'ips_username' => $user_name,
				'auth_key' => '880ea6a14ea49e853634fbdc5015a024',
				'cx_ip_address' => $_REQUEST['ip_address'],
				'cx_user_agent' => $_REQUEST['user_agent'],
				'v' => $v
			);
			
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			//curl_setopt($ch, CURLOPT_NOBODY, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS,  http_build_query($params));
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false );
			curl_setopt($ch, CURLOPT_MAXREDIRS, 3 );
			$result = curl_exec($ch);
			
			preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);
			$cookies = array();
			foreach($matches[1] as $item) {
				parse_str($item, $cookie);
				$cookies = array_merge($cookies, $cookie);
			}
						
			return json_encode(array(
				'session_id' => $cookie['session_id'], 
				'member_key' => md5($user['email'] . '&' . $user['member_login_key'] . '&' . $user['joined'])
			));
		}
		
		return TRUE;
	}
	
	
	public function LogOut() 
	{
		$url = self::URL . 'index.php?section=login&app=core&module=global&do=logout&k=880ea6a14ea49e853634fbdc5015a024';

        $params = array(
            'cx_ip_address' => $_REQUEST['ip_address'],
            'cx_user_agent' => $_REQUEST['user_agent']
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        //curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,  http_build_query($params));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false );
        curl_setopt($ch, CURLOPT_MAXREDIRS, 3 );
        $result = curl_exec($ch);

        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);
        $cookies = array();
        foreach($matches[1] as $item) {
            parse_str($item, $cookie);
            $cookies = array_merge($cookies, $cookie);
        }

        return json_encode(array(
            'session_id' => $cookie['session_id'],
        ));
	}
	
	/*public function GetApprovedPostsCountOfMember($name)
	{
		global $DB;
		
		$m_id = $DB->fetchOne('SELECT member_id FROM members WHERE name = ?', $name);
		$count = 0;
		
		if ($m_id)
		{
			$count = $DB->fetchOne('SELECT COUNT(pid) FROM posts WHERE author_id = ? AND queued = 0', $m_id);
		}
		
		return $count;
	}*/

	public function GetLastTopicFirstPost()
	{
		global $DB;

		$last_topic = $DB->query('SELECT tid, title_seo FROM topics ORDER BY tid DESC LIMIT 1')->fetch();

		$first_post = $DB->query('SELECT author_id, author_name, post, post_date, topic_id FROM posts WHERE topic_id = ? AND new_topic = 1', $last_topic['tid'])->fetch();

		return json_encode(array(
			'post' => $first_post,
			'topic_slug' => $last_topic['title_seo']
		));
	}

	/*public function AddReviewAsTopicAndPost($title, $title_seo, $ip, $post)
	{
		// this is for forum.beneluxxx.com ONLY
		global $DB;

		$member_id = 12396; // add topic and post via this member
		$member = $DB->query('SELECT members_display_name, members_seo_name FROM members WHERE member_id = ?', $member_id)->fetch();

		$forum_id = 8; // forum for reviews

		// add topic
		$DB->insert('topics', array(
			'title' => $title,
			'state' => 'open',
			'posts' => 1,
			'starter_id' => $member_id,
			'start_date' => time(),
			'last_poster_id' => $member_id,
			'last_post' => time(),
			'starter_name' => $member['members_display_name'],
			'last_poster_name' => $member['members_display_name'],
			'poll_state' => 0,
			'last_vote' => 0,
			'views' => 0,
			'forum_id' => $forum_id,
			'approved' => 1,
			'author_mode' => 1,
			'pinned' => 0,
			'title_seo' => $title_seo,
			'seo_last_name' => $member['members_seo_name']
		));

		// get added topic id
		$topic_id = $DB->lastInsertId();

		// add post
		$DB->insert('posts', array(
			'author_id' => $member_id,
			'author_name' => $member['members_display_name'],
			'use_sig' => 1,
			'use_emo' => 1,
			'ip_address' => $ip,
			'post_date' => time(),
			'post' => $post,
			'topic_id' => $topic_id,
			'new_topic' => 1,
			'post_key' => md5(time())
		));

		// get added post id
		$post_id = $DB->lastInsertId();

		// update topic first post id
		$DB->update('topics', array('topic_firstpost' => $post_id), $DB->quoteInto('tid = ?', $topic_id));

		// update forum data
		$DB->update('forums', array(
			'topics' => new Zend_Db_Expr('topics + 1'),
			'posts' => new Zend_Db_Expr('posts + 1'),
			'last_post' => time(),
			'last_poster_id' => $member_id,
			'last_poster_name' => $member['members_display_name'],
			'last_title' => $title,
			'last_id' => $topic_id,
			'newest_title' => $title,
			'newest_id' => $topic_id,
			'seo_last_title' => $title_seo,
			'seo_last_name' => $member['members_seo_name']
		), $DB->quoteInto('id = ?', $forum_id));

		// update member data
		$DB->update('members', array(
			'posts' => new Zend_Db_Expr('posts + 1'),
			'last_post' => time(),
			'last_activity' => time()
		), $DB->quoteInto('member_id = ?', $member_id));
	}*/
}

