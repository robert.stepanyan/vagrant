<?php

// load config file
require_once '../conf_global.php';

// connect to database
require_once 'Zend/Db.php';
$DB = Zend_Db::factory('Mysqli', array(
	'host'     => $INFO['sql_host'],
	'username' => $INFO['sql_user'],
	'password' => $INFO['sql_pass'],
	'dbname'   => $INFO['sql_database']
));

// MAIN DB CONNECTION GOES HERE <---------

// init logging
require_once 'Zend/Log.php';
require_once 'Zend/Log/Writer/Stream.php';
$LOG = new Zend_Log(new Zend_Log_Writer_Stream('./api.log'));

// init session handling
require_once 'Zend/Session/Namespace.php';

// load command handlers
require_once 'cmds.class.php';
$CMDS = new WebApi_Commands();
