<?php
session_start();

		$font = 'css/trebuc.ttf';

		$charset = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';

		$code_length = 5;

		$height = 30;
		$width = 90;
		$code = '';

		for ( $i = 0; $i < $code_length; $i++ )
		{
			$code = $code . substr($charset, mt_rand(0, strlen($charset) - 1), 1);
		}

		$rgb[0] = array(204,0,0);
		$rgb[1] = array(34,136,0);
		$rgb[2] = array(51,102,204);
		$rgb[3] = array(141,214,210);
		$rgb[4] = array(214,141,205);
		$rgb[5] = array(100,138,204);

		$font_size = $height * 0.4;

		$bg = imagecreatefrompng('img/bg_captcha.png');
		$image = imagecreatetruecolor($width, $height);
		imagecopy($image, $bg, 0, 0, 0, 0, imagesx($bg), imagesy($bg));
		$background_color = imagecolorallocate($image, 255, 255, 255);
		$noise_color = imagecolorallocate($image, 20, 40, 100);

		for($i = 0; $i<$code_length; $i++)
		{
			$A[] = rand(-20, 20);
			$C[] = rand(0, 5);
			$text_color = imagecolorallocate($image, $rgb[$C[$i]][0], $rgb[$C[$i]][1], $rgb[$C[$i]][2]);
			imagettftext($image, $font_size, $A[$i], 7 + $i * 15, 20 + rand(-3, 3), $text_color, $font , $code[$i]);
		}

		$_SESSION['captcha']= strtolower($code);

		header('Content-Type: image/png');
		imagepng($image);
		imagedestroy($image);

		die;

?>
