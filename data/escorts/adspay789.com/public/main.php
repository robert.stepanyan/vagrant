<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="payment-block text-right clearfix">
				<h2 class="clearfix">{{enter_custom_amount}}</h2>
				
				<form action="process.php" method="POST" id="paymentForm" class="payment-form m-t-30">
					<div class="amount-wrapper clearfix">
						<input type="text" name="amount" value=""/>
						<input type="hidden" name="lang" value=""/>
						<input type="hidden" name="payment_gateway" id="paymentGateway" value="mmgbill"/>
						<span class="currency">EUR</span>
					</div>

					<!--<div class="payment-option clearfix">
						<span class="checkbox">
							<span class="bullet"></span>
						</span>

						<label for="ecardon">
							<input type="radio" data-method="ecardon" id="ecardon" />
							Checkout with Ecardon (recommended)
						</label>
					</div>
					
					<div class="payment-option checked clearfix">
						<span class="checkbox">
							<span class="bullet"></span>
						</span>

						<label for="mmgbill">
							<input type="radio" checked="checked" data-method="mmgbill" id="mmgbill" />
							Checkout with MMGBILL (recommended)
						</label>
					</div>-->

                    <input type="hidden" data-method="mmgbill" id="mmgbill" />
					<button type="submit" class="buy-now-btn clearfix" id="buyBtn">{{buy_now}}</button>
				</form>
			</div>
		</div>
	</div>
	<div class="packages">
		<article class="row m-t-30">
			<section class="col-md-4">
				<div class="package m-b-30">
					<div class="row">
						<h3 class="package-title col-md-12 col-sm-6 col-xs-6">Diamond (30 {{days}})</h3>
						<div class="package-price col-md-12 col-sm-6 col-xs-6">518 EUR</div>
					</div>
				</div>
			</section>
			<section class="col-md-4">
				<div class="package m-b-30">
					<div class="row">
						<h3 class="package-title col-md-12 col-xs-6">Diamond (15 {{days}})</h3>
						<div class="package-price col-md-12 col-xs-6">311 EUR</div>
					</div>
				</div>
			</section>
			<section class="col-md-4">
				<div class="package m-b-30">
					<div class="row">
						<h3 class="package-title col-md-12 col-xs-6">Silver (30 {{days}})</h3>
						<div class="package-price col-md-12 col-xs-6">270 EUR</div>
					</div>
				</div>
			</section>
			<section class="col-md-4">
				<div class="package m-b-30">
					<div class="row">
						<h3 class="package-title col-md-12 col-xs-6">Silver (15 {{days}})</h3>
						<div class="package-price col-md-12 col-xs-6">165 EUR</div>
					</div>
				</div>
			</section>
			<section class="col-md-4">
				<div class="package m-b-30">
					<div class="row">
						<h3 class="package-title col-md-12 col-xs-6">Bronze (30 {{days}})</h3>
						<div class="package-price col-md-12 col-xs-6">170 EUR</div>
					</div>
				</div>
			</section>
			<section class="col-md-4">
				<div class="package m-b-30">
					<div class="row">
						<h3 class="package-title col-md-12 col-xs-6">Bronze (15 {{days}})</h3>
						<div class="package-price col-md-12 col-xs-6">100 EUR</div>
					</div>
				</div>
			</section>
		</article>
	</div>
</div>