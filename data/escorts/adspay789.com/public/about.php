<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="hero-page">

				<h2 class="page-title">About us</h2>
				<h3 class="light-text hightlighted">Imprint</h3>
				<p>
					<i class="fa fa-map-marker fa-2" aria-hidden="true"></i>
					AA Media Services GmbH 
					Industriestrasse 28, 9100 Herisau
				</p>
				<p><i class="fa fa-phone fa-2" aria-hidden="true"></i></span> CHE-389.533.593</p>

			</div>
		</div>
	</div>
</div>
