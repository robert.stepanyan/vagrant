<?
include('inc/common.php');
$token =  preg_replace('#[^a-zA-Z0-9.\-]#', '', $_GET['id']);
	
$error = true;
if ($token){
	$alt_payment = new Cube_EcardonGateway();
	$result = $alt_payment->getStatus($token);
	$result_dec = json_decode($result);
	$copy_examples = 'MerchantTransactionId, Holder, Amount etc.';
	if(isset($result_dec->result) && ($result_dec->result->code == '000.000.000' || $result_dec->result->code == '000.100.110')){
		$message = '<span class="ttu"> Your payment <b>was successful </b></span> Thank you very much! ';

		$responce = array(
			'Transaction Id' => $result_dec->descriptor,
			'Holder' => $result_dec->card->holder,
			'Amount' => $result_dec->amount,
			'Currency' => $result_dec->currency,
			'Payment Brand' => $result_dec->paymentBrand,
			'Date' => date('Y-m-d H:i:s', strtotime($result_dec->timestamp))   
		);
		$error = false;
	} 
	else {
		$message = '<span class="ttu">Error Transaction!</span><br/><br/>' . $result_dec->result->description ;
	}
}
else{
	$message = '<span class="ttu">Error Transaction!</span><br/><br/> Please Try Again ';
}

//$date = new DateTime($order->transaction->lastUpdate, new DateTimeZone('America/New_York'));
//$date->setTimezone(new DateTimeZone('Europe/Rome'));
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" media="screen" href="css/style.css"/>
		<link rel="icon" href="/images/favicon.ico" type="image/png" />
	</head>
	<body>
		<div id="main-wrapper">
			<? $page = 'success'; ?>
			<? include('header.php'); ?>
			<div id="container" class="centered-wrapper sm">
				<? if($error): ?>
				<h1 class="unsuccessful-header">An Error occured!</h1>
				<? else: ?>
				<h1 class="successful-header">Successful transaction!</h1>
				<? endif; ?>
				
				<div class="cont-sep"></div>
				<div class="successful-payment-container">
					<? if($error): ?>
						<h3>Your payment was unsuccessful. </h3>
						<p>Please contact our staff for details.</p>
					<? else: ?>
						<h3>Your payment was successful, thank you very much.</h3>
						<p>Please contact your sales person with the payment details</p>
						<ul>
							<? foreach($responce as $key => $value): ?>
								<li><?= $key ?>: <?= $value ?></li>
							<? endforeach; ?>
						</ul>
						
					<? endif; ?>
					<br>	
					<a class="bth ttu" href="/">back to home</a>
				</div>	
			</div>
			<? include('footer.php'); ?>
		</div>
	</body>
</html>