<header id="header" class="animated fadeInUp" <?= $page != 'index' ? 'class="sm"' : ''  ?>>
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-lg-offset-3 col-md-12">
				<nav class="main-nav">
					<ul>
						<li class="sep-tri"></li>
						<li class="pre-menu"><i></i></li>
						<li class="<?= 'index' == $page ? 'selected' : '' ?>">
							<a class="text-center" href="#">{{main}}</a>
						</li>
						<li class="sep"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
						<li class="<?= 'about' == $page ? 'selected' : '' ?>">
							<a class="text-center" href="#about">{{about_us}}</a>
						</li>
						<li class="sep"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
						<li class="<?= 'terms' == $page ? 'selected' : '' ?>">
							<a class="text-center" href="#terms">{{terms}}</a>
						</li>
						<li class="sep"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
						<?/*
                        <li class="<?= 'sites' == $page ? 'selected' : '' ?>">
							<a class="text-center" href="#sites">{{our_sites}}</a>
						</li>
						<li class="sep"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                        */?>
						<li class="<?= 'privacy' == $page ? 'selected' : '' ?>">
							<a class="text-center" href="#privacy">{{privacy}}</a>
						</li>
						<li class="<?= $_SERVER['SERVER_NAME'] == 'www.adspay-789.com' ? 'logo' : 'logo-old' ?>">
							<a class="text-center" href="#"></a>
						</li>
						<li class="lang">
							<a class="flag-en" href="javascript:void(0)">
								<img src="/images/en.svg" alt="EN" width="28" height="18">
							</a>
							<a class="flag-it" href="javascript:void(0)">
								<img src="/images/it.svg" alt="IT" width="28" height="18">
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<div class="row m-t-30 m-b-20">
			<div class="col-md-8 col-md-offset-4">
				<div class="row">
					<div class="col-md-6 title-block">
						<h1 class="text-right">{{slogan_block_row_1}}</h1>
						<!--<p class="subtitle text-right">{{slogan_block_row_2}}</p>-->
					</div>

					<div class="col-md-6 subtitle-block">
							{{slogan_block_row_3}} {{slogan_block_row_4}} {{slogan_block_row_5}}
					</div>
				</div>
			</div>			
		</div>
	</div>
</header>