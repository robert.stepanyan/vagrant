<?php include 'inc/common.php'; ?>

<?php

	$token =  preg_replace('#[^a-zA-Z0-9.\-]#', '', $_GET['id']);
	$error = true;

	if ($token){
		$alt_payment = new Cube_EcardonGateway();
		$result = $alt_payment->getStatus($token);
		$result_dec = json_decode($result);

		$copy_examples = 'MerchantTransactionId, Holder, Amount etc.';

		if (isset($result_dec->result) && ($result_dec->result->code == '000.000.000' || $result_dec->result->code == '000.100.110')) {

			$response = array(
				'Transaction Id' => $result_dec->descriptor,
				'Holder' => $result_dec->card->holder,
				'Amount' => $result_dec->amount,
				'Currency' => $result_dec->currency,
				'Payment Brand' => $result_dec->paymentBrand,
				'Date' => date('Y-m-d H:i:s', strtotime($result_dec->timestamp))   
			);

			$error = false;
		}
	} 
?>

 <div class="container">
 	<div class="row">
 		<div class="col-md-12">
 			<div class="hero-page">
				<?php if($error): ?>
					<h2 class="page-title">An Error occured!</h2>
				<?php else: ?>
					<h2 class="page-title">Successful transaction!</h2>
				<?php endif; ?>

				<?php if ($error): ?>
					<h3 class="light-text hightlighted">Your payment was unsuccessful. </h3>
					<p>Please contact our staff for details.</p>
				<?php else: ?>
					<h3 class="light-text hightlighted">Your payment was successful, thank you very much.</h3>
					<p>Please contact your sales person with the payment details.</p>
					<ul>
						<?php foreach($response as $key => $value): ?>
							<li><?= $key ?>: <?= $value ?></li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>

				<p><a href="#">Back to home</a></p>
			</div>
		</div>
	</div>
</div>