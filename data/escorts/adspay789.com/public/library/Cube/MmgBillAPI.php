<?php
class Cube_MmgBillAPI
{
	private $payment_link = 'https://secure.f5fin.com/payment/?';
	private $merchantId = ''; 
	private $secret_key = ''; 
	private $ts = 'd'; //Stands for dynamic system
	private $currencyCode = '978';
	 
	public function __construct()
	{
		//$this->merchantId = 'M1572';
		//$this->secret_key = 'qaRxBH6laojLkfkIbbUiRupyJLL0rcfv';
		$this->merchantId = 'M1524';
		$this->secret_key = 'GRBOyQdTHZZn862';
	}
	 
	public function getHostedPageUrl($amount, $transaction_id)
	{
		$c1 = 'adspay789:' . $amount;
		$amount = $amount * 100;
		$postback_url = 'http://www.adspay-789.com/callback_mmg.php';
		
		$array_request = array(
			'mid' => $this->merchantId,
			'ts' => $this->ts,
			'ti_mer' => $transaction_id,
			'txn_type' => 'SALE',
			'cu_num' => $this->currencyCode,
			'amc' => $amount,
			'c1' => $c1,
			'surl' => $postback_url
		);
		
		/* create the hash signature */
		$sh = $this->create_hash($array_request, $this->secret_key);

		/* create the payment link */
		$payment_link = $this->create_payment_link($array_request, $this->payment_link, $sh);
		
		return $payment_link;
	}
	 
	/* function to create the hash signature */
	private function create_hash ($array_request, $secret_key)
	{
		$hash_string = "";
		ksort($array_request);
		foreach ($array_request as $key => $value) {
			$hash_string .= "|" . $key . $value;
		}

		return hash_hmac("sha256", $hash_string, $secret_key, false);
	}

	/* function to create payment link */
	private function create_payment_link($array_request, $payment_link, $sh) 
	{
		foreach ($array_request as $key => $value) {
			$payment_link .= $key . "=" . $value . "&";
		}

		return $payment_link .= "sh=" . $sh;
	}
	
	
}

?>
