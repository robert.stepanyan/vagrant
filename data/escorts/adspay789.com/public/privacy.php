<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="hero-page">
				<h2 class="page-title">{{privacy}}</h2>

				<h3 class="light-text hightlighted">{{our_commitment_to_privacy}}</h3>
				<p>{{our_commitment_to_privacy_block_1}}</p>

				<h3 class="light-text hightlighted">{{the_information_we_collect}}</h3>
				<p>{{the_information_we_collect_block_1}}</p>
				
				<h3 class="light-text hightlighted">{{the_way_we_use_information}}</h3>
				<p>{{the_way_we_use_information_block_1}}</p>
				
				<h3 class="light-text hightlighted">{{our_commitment_to_data_security}}</h3>
				<p>{{our_commitment_to_data_security_block_1}}</p>
				
				<h3 class="light-text hightlighted">{{how_you_can_access_your_information}}</h3>
				<p>{{how_you_can_access_your_information_block_1}}</p>
				<p>{{how_you_can_access_your_information_block_2}}</p>
				<p>{{how_you_can_access_your_information_block_3}}</p>
			</div>
		</div>
	</div>
</div>