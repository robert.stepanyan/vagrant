<?php
// Permanent 301 redirection
//header("HTTP/1.1 301 Moved Permanently");
//header("Location: https://www.escortforumit.xxx/payment");
//exit();
?>
<?php include 'inc/common.php'; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link rel="icon" href="/images/favicon.ico" type="image/png" />
		<link rel="stylesheet" type="text/css" media="screen" href="css/main.css?v=2"/>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300|Roboto:300,400,700" rel="stylesheet">

		<link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" href="/css/animate.css" />
	</head>
	
	<body>
		<div id="mainWrapper">
			<div data-device="{{device}}" data-lang="{{lang}}" id="pageConf">
				<?php
	                session_start();

	                $token = $_SESSION['token'] ? $_SESSION['token'] : false;
	                unset($_SESSION['token']);
				?>

				<?php if($token): ?>
				<div class="ecardon-popup modal" >
					<script>
						var wpwlOptions = {
							onBeforeSubmitCard: function(){
								//card holder validtion
								if ( $(".wpwl-control-cardHolder").val() === "" )
								{             
									$(".wpwl-control-cardHolder").addClass("wpwl-has-error");
									$(".wpwl-control-cardHolder").after("<div class='wpwl-hint wpwl-hint-cardHolderError'>Invalid card holder</div>");
									$(".wpwl-button-pay").addClass("wpwl-button-error").attr("disabled", "disabled");
									return false;
								}
								else
									return true;
							}
						}
					</script>
					<script async src="https://oppwa.com/v1/paymentWidgets.js?checkoutId=<?= $token ?>"></script>
					<form action="https://www.adspay-789.com/#ecardonResponse/" class="paymentWidgets" data-brands="VISA MASTER"></form>
				</div>
				<?php endif; ?>

				<?php include('header.php'); ?>
				<main id="main">
					<div id="root" class="page">
						<?php include('main.php'); ?>
					</div>
					<div id="about" class="page" style="display: none">
						<?php include('about.php'); ?>
					</div>
					<div id="terms" class="page" style="display: none">
						<?php include('terms.php'); ?>
					</div>
					<!--<div id="sites" class="page" style="display: none">
						<?php /*include('sites.php'); */?>
					</div>-->
					<div id="privacy" class="page" style="display: none">
						<?php include('privacy.php'); ?>
					</div>
					<div id="ecardonResponse" class="page" style="display:none">
					</div>
					<div id="mmgResponse" class="page" style="display:none">
					</div>

				</main>
				<?php include('footer.php'); ?>
			</div>

			<?php if ($_SERVER['HTTP_HOST'] == 'www.adspay789.com'): ?>
	        <div class="redirect-modal modal modal-sm">
	            <div class="modal-content">
	                <h2>{{attention_new_domain}}</h2>
	                <p>
	                	{{adspay_new_address}}
	                	<a href="//www.adspay-789.com">www.adspay-789.com</a>
	                </p>
	                <p>{{click_on_the_button}}</p>
	                <p class="text-center">
	                    <a href="//www.adspay-789.com" class="btn btn-success">{{go_to_new_domain}}</a>
	                </p>
                    <p>
                        {{auto_redirect}} <span class="counter">5</span> {{seconds}}.
                    </p>
	             </div>
	        </div>
	        <?php endif; ?>
		</div>
	
		<!-- SCRIPTS -->
		<script type="text/javascript" src="/js/jquery-3.2.1.min.js?v=1" ></script>
		<script type="text/javascript" src="/css/bootstrap/js/bootstrap.min.js" ></script>
		<script type="text/javascript" src="/js/jquery.validate.js"></script>
		<script type="text/javascript" src="/js/pace.min.js"></script>
		<script type="text/javascript" src="/js/mustache.min.js"></script>
		<script type="text/javascript" src="/js/t.js?v=1"></script>
		<script type="text/javascript" src="/js/routie.js"></script>
		<script type="text/javascript" src="/js/app.js?v=8"></script>
	</body>
</html>
