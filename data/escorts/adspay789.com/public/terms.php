<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="hero-page">

				<h2 class="page-title">{{advertising_terms}}</h2>
				<p>{{terms_top}}</p>

				<h3 class="light-text hightlighted">{{advertising_terms}}</h3>
				<p>{{please_read_carefully}}</p>

				<ul class="common">
					<li>{{terms_row_1}}</li>
					<li>{{terms_row_2}}</li>
					<li>{{terms_row_3}}</li>
					<li>{{{terms_row_4}}}</li>
					<li>{{terms_row_5}}</li>
				</ul>

				<h3 class="light-text hightlighted">{{disclaimer_of_liability}}</h3>
				<p>{{disclaimer_block_1}}</p>
				<p>{{disclaimer_block_2}}</p>
				<p>{{disclaimer_block_3}}</p>
				<p>{{disclaimer_block_4}}</p>

				<h3 class="light-text hightlighted">{{governing_low}}</h3>
				<p>{{governing_low_block_1}}</p>

			</div>
		</div>
	</div>
</div>
