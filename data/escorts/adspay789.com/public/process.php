<?php
// die('<h1>Payment is temporarily unavailable</h1>');
include('inc/common.php');

// validating data
$errors = array();


// amount
if( empty($_POST['amount']) ) {

	$errors['amount'] = 'required';
	$amount = '';
} else {

	$amount = stripslashes($_POST['amount']);
}

if ( !is_numeric($amount) || $amount <= 0 ) {
	$errors['amount'] = 'invalid amount';
}


// lang
$lang = (!empty($_POST['lang'])) ? $_POST['lang'] : '';

$locales = array(
	'en'	=> 'en_US',
);

// Showing form again
if(count($errors) > 0) {
	include 'views/payment.phtml';
	die;
}
 
try 
{
	/*$hostUrl = $_SERVER['HTTP_HOST'];
	$findme   = 'www.';
	$pos = strpos($hostUrl, $findme);

	if ( $pos === false ){
		$hostUrl = "www.".$hostUrl;
	}
    $SuccessURL = 'http://'.$hostUrl.'/success.php';*/
	if($_POST['payment_gateway'] == 'ecardon'){
		$ref = md5(rand(100000, 1000000000));
		$TokenParams = array(
			'amount' => $amount,
			'descriptor' => $ref,
			'merchantTransactionId' => $ref
		);
		
		$alt_payment = new Cube_EcardonGateway($TokenParams);
		$token_data = $alt_payment->checkout();
		$token_data_dec = json_decode($token_data);
		
		if(isset($token_data_dec->result) && $token_data_dec->result->code == '000.200.100'){
			$token =  $token_data_dec->id;
						
			// Redirecting to Payment Gateway URL
			session_start();
			$_SESSION['token'] = $token;
			header("Location: /");
		}
	} elseif ( $_POST['payment_gateway'] == 'mmgbill' ) {
		$mmgBill = new Cube_MmgBillAPI();
		$hosted_url = $mmgBill->getHostedPageUrl($amount, time());
		
		header('Location:'. $hosted_url);
	} else {
		$nbx = new Cube_NetbanxAPI();
	
		$order = $nbx->create_order(
			array(
				'total_amount'	=> $amount * 100,
				'currency_code'	=> 'EUR',
				'merchant_ref_num'	=> md5(time()),
				'redirect'	=> array(
					array(
						'rel'	=> 'on_success',
						'returnKeys'	=> array('id'),
						'uri'	=> 'https://www.adspay789.com/success.php'
					)
				),
				'link'		=> array(
					array(
						'rel'	=> 'cancel_url',
						'uri'	=> 'https://www.adspay789.com'
					),
					array(
						'rel'	=> 'return_url',
						'uri'	=> 'https://www.adspay789.com'
					)
				),
				'locale'	=> $lang ? $locales[$lang] : 'it_IT'
			)
		);
		header('Location:'. $order->link[0]->uri);
	}
	
	
}
catch(Exception $e)
{
	//header('Location: /');
	
	die('Unexpected error, please try again or contact Administrator if you see the error again');
	print_r($e);
	exit;
	$errors['exception'] = 'Unexpected error, please try again or contact Administrator if you see the error again';
	include 'views/payment.phtml';
}

?>