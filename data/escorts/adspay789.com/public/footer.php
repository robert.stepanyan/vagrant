<footer id="footer" class="container m-t-80">
	<div class="row">
		<div class="uncol-md-10 text-left">
			<div class="copy">
				<p>
					{{footer_text}} &copy; Copyright {{date}} / AA Media Services GmbH
				</p>
			</div>
			<div class="tri"></div>
		</div>
		<div class="uncol-md-2">
			<div class="cards text-center">
				<img src="/images/mv.svg" alt="master visa cards" width="120">
			</div>
		</div>
	</div>
	<nav class="mobile-nav">
		<ul>
			<li class="menu-icon-wrapper">
				<a href="javascript:void(0)">
					<img src="/images/menu.svg" alt="">
				</a>
				<ul class="hamburger-menu">
					<li class="<?= 'index' == $page ? 'selected' : '' ?>">
						<a class="text-center" href="#">{{main}}</a>
						
					</li>
					<li class="<?= 'about' == $page ? 'selected' : '' ?>">
						<a class="text-center" href="#about">{{about_us}}</a>
					</li>
					<li class="<?= 'terms' == $page ? 'selected' : '' ?>">
						<a class="text-center" href="#terms">{{terms}}</a>
					</li>
					<li class="<?= 'sites' == $page ? 'selected' : '' ?>">
						<a class="text-center" href="#sites">{{our_sites}}</a>
					</li>
					<li class="<?= 'privacy' == $page ? 'selected' : '' ?>">
						<a class="text-center" href="#privacy">{{privacy}}</a>
					</li>
				</ul>
			</li>
			<li class="<?= $_SERVER['SERVER_NAME'] == 'www.adspay-789.com' ? 'logo' : 'logo-old' ?>">
				<a class="text-center" href="#"></a>
			</li>
			<li class="lang">
				<a class="flag-en" href="javascript:void(0)">
					<img src="/images/en.svg" alt="EN" width="28" height="18">
				</a>
				<a class="flag-it" href="javascript:void(0)">
					<img src="/images/it.svg" alt="IT" width="28" height="18">
				</a>
			</li>
		</ul>
		
	</nav>
</footer>