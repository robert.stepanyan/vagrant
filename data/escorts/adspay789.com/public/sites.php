<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="hero-page">

				<h2 class="page-title">Our sites</h2>

				<p>
					We run many internationally known advertising sites worldwide. <span class="hightlighted">www.adspay-789.com</span> offers only advertising for Italy. You still can contact us if you are interested in advertising solutions for different countries.
				</p>
				<h3 class="light-text hightlighted">We are locally available in:</h3>
				<div class="row">
					<div class="col-md-6">
						<ul class="styled">
							<li>United Kingdom</li>
							<li>Switzerland</li>
							<li>Italy</li>
							<li>United States</li>
							<li>Spain</li>
							<li>Greece</li>
							<li>Germany</li>
						</ul>
					</div>
					<div class="col-md-6">
						<a class="site-preview pull-right" href="http://www.escortforumit.xxx" target="_blank"></a>
						<p class="pull-right">
							<a href="http://www.escortforumit.xxx" target="_blank">escortforumit.xxx</a>
							Leading advertising site in Italy (Alexa Traffic Rank: 20.983)
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
