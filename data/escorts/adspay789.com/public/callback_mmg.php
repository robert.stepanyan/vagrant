<?php
include('inc/common.php');

$c1 = $_GET['c1'];
$c1 = explode(':', $c1);
$error = $_GET['txn_status'] != 'APPROVED';

?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link rel="icon" href="/images/favicon.ico" type="image/png" />
		<link rel="stylesheet" type="text/css" media="screen" href="css/main.css"/>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300|Roboto:300,400,700" rel="stylesheet">

		<link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" href="/css/animate.css" />
	</head>
	
	<body>
		<div id="mainWrapper">
			<div data-device="{{device}}" data-lang="{{lang}}" id="pageConf">
				<?php include('header-mmg.php'); ?>
				<main id="main">
					<div id="mmgResponse" class="page">
						<div class="container">
						 	<div class="row">
						 		<div class="col-md-12">
						 			<div class="hero-page">
										<?php if($error): ?>
											<h2 class="page-title">An Error occured!</h2>
										<?php else: ?>
											<h2 class="page-title">Successful transaction!</h2>
										<?php endif; ?>

										<?php if ($error): ?>
											<h3 class="light-text hightlighted">Your payment was unsuccessful. </h3>
											<p>Please contact our staff for details.</p>
										<?php else: ?>
											<h3 class="light-text hightlighted">Your payment was successful, thank you very much.</h3>
											<p>Please contact your sales person with the payment details.</p>
											<ul>
												<li>Transaction Id: <?= $_GET['ti_mmg'] ?></li>
												<li>Amount: <?= $c1[1] ?> EUR</li>
											</ul>
										<?php endif; ?>

										<p><a href="#">Back to home</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>

				</main>
				<?php include('footer.php'); ?>
			</div>

			
		</div>
	
		<!-- SCRIPTS -->
		<script type="text/javascript" src="/js/jquery-3.2.1.min.js?v=1" ></script>
		<script type="text/javascript" src="/css/bootstrap/js/bootstrap.min.js" ></script>
		<script type="text/javascript" src="/js/pace.min.js"></script>
		<script type="text/javascript" src="/js/mustache.min.js"></script>
		<script type="text/javascript" src="/js/t.js?v=1"></script>
		<script type="text/javascript" src="/js/routie.js"></script>
		<script type="text/javascript" src="/js/app-mmg.js?v=7"></script>
	</body>
</html>
