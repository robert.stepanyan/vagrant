<?php
include 'inc/common.php';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" media="screen" href="css/style.css"/>
		<link rel="icon" href="/images/favicon.ico" type="image/png" />
		<link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css" />
	</head>
	<style type="text/css">
		label.error {display:none !important}
	</style>
	<script type="text/javascript" src="/js/jquery-1.5.min.js" ></script>
	<script type="text/javascript" src="/js/jquery.validate.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#custom-amount-form").validate({
				rules: {
					amount: {
						required: true,
						number: true
					}
				}
			});
			
			$('.payment-option').click(function() {
				$('.payment-option').removeClass('checked');
				$(this).addClass('checked');
				
				$('#payment-gateway').val($(this).attr('data-method'));
			});
			
			if($('.overlay').length){
				$('.overlay').click(function(){
					$('.overlay').remove();
					$('.ecardon-popup').remove();
				});
			}
		});
	</script>
	<body>

        <?php if ($_SERVER['HTTP_HOST'] == 'www.adspay789.com'): ?>
            <script>
                $(document).ready(function () {
                    $('.close').click(function () {
                        $('#modal').hide();
                    });

                    $('.redirect_new_domain').click(function () {
                        window.location.href = '//www.adspay-789.com';
                    });

                    var time = 4;
                    var interval = setInterval(function () {
                        showCounter(time--);
                    }, 1000);

                    var showCounter = function (time) {
                        $('.show_counter').html(time);
                        if (time == 0) {
                            clearInterval(interval);
                            window.location.href = '//www.adspay-789.com';
                        }
                    };

                });
            </script>

            <div id="modal" class="new_domain_modal">
                <div class="modal-content">
                    <span class="close" >&times;</span>
                    <h2><?= __('attention_new_domain')?></h2>
                    <p class="new_address"><?= __('adspay_new_address')?> <a href="//www.adspay-789.com">www.adspay-789.com</a></p>
                    <p class="click_button_attention"><?= __('click_on_the_button')?></p>

                    <div class="text-center">
                        <button class="redirect_new_domain"><?= __('go_to_new_domain')?></button>
                    </div>

                    <div class="counter">
                        <p>
                            <?= __('auto_redirect')?>  <span class="show_counter"> 5 </span> <?= __('seconds')?>.
                        </p>

                    </div>
                 </div>
            </div>

        <?php endif; ?>

		<div id="main-wrapper" style="margin: 0px auto; display: block">
			<?php
                session_start();

                $token = $_SESSION['token'] ? $_SESSION['token'] : false;
                unset($_SESSION['token']);
			?>

			<?php if($token): ?>

			<div class="overlay"></div>
			<div class="ecardon-popup">
				<script src="https://oppwa.com/v1/paymentWidgets.js?checkoutId=<?= $token ?>"></script>
				<form action="https://www.adspay-789.com/ecardon_responce.php" class="paymentWidgets" data-brands="VISA MASTER"></form>
			</div>

			<?php endif; ?>

			<?php $page = 'index'; ?>

			<?php include('header.php'); ?>

			<div id="container" class="centered-wrapper">
				<div id="packages-container">
					<div class="package gold-premium">
						<div class="header">
							<span>GOLD PREMIUM</span>
						</div>
						<div class="package-body">
							<ul>
								<li class="duration">30 <?= __('days') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('national_listing') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('international_listing') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('girl_of_the_month') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('main_premium_spot') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('tour_ability') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('search') ?></li>
							</ul>
							<div class="clear"></div>
						</div>
					</div>
					<div class="package gold-premium-light">
						<div class="header">
							<span>GOLD PREMIUM LIGHT</span>
						</div>
						<div class="package-body">
							<ul>
								<li class="duration">15 <?= __('days') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('national_listing') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('international_listing') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('girl_of_the_month') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('main_premium_spot') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('tour_ability') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('search') ?></li>
							</ul>
							<div class="clear"></div>
						</div>
					</div>
					<div class="package silver">
						<div class="header">
							<span>Silver City</span>
						</div>
						<div class="package-body">
							<ul>
								<li class="duration">30 <?= __('days') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('national_listing') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('international_listing') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('girl_of_the_month') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('main_premium_spot_silver') ?></li>
								<li class="sep"></li>
								<li class="product"><?= __('tour_ability') ?></li>
								<li class="sep"></li>
							</ul>
							<div class="clear"></div>
						</div>
					</div>
					<div class="package customized">
						<div class="header">
							<span><?= __('customized') ?></span>
						</div>
						<div class="package-body">
							<form action="process.php" method="post" id="custom-amount-form">
								<input type="text" name="amount" value=""/>
								<input type="hidden" name="lang" value="<?= ! empty($_GET['lang']) ? $_GET['lang'] : '' ?>"/>
								<input type="hidden" name="payment_gateway" id="payment-gateway" value="ecardon"/>
								<ul>
									<li class="sep"></li>
									<li class="product"><?= __('enter_custom_amount') ?></li>
									<li class="sep"></li>
								</ul>
								<div class="payment-option-wrapper">
									<p>Please select one of these options below</p>
									<div class="payment-option checked" data-method="ecardon">checkout with <strong>Ecardon (recommended)</strong></div>
									<div class="clear"></div>
									<?/*<div class="payment-option " data-method="netbanx">checkout with <strong>Netbanx (alternative)</strong></div>
									<div class="clear"></div>*/?>
									<div class="payment-option" data-method="mmgbill">checkout with <strong>MMGBILL</strong> (alternative) </div>
									<div class="clear"></div>
								</div>
								<div class="clear"></div>
								<input type="submit" class="buy-now-btn" value="<?= strtoupper(__('buy_now')) ?>"/>
								<div class="clear"></div>
							</form>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<?php include('footer.php'); ?>
		</div>
	</body>
</html>
