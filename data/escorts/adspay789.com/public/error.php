<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<? include('..inc/common.php'); ?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" media="screen" href="css/style.css"/>
		<link rel="icon" href="/images/favicon.ico" type="image/png" />
	</head>
	<body>
		<div id="main-wrapper">
			<? include('header.php'); ?>
			<div id="container" class="centered-wrapper sm">
				<h1 class="successful-header">Error Occured!</h1>
				<div class="cont-sep"></div>
				<div class="successful-payment-container">
					<h3>Your payment was successful, thank you very much.</h3>
					<p>Please contact your sales person with the payment details</p>
					<a href="/" class="back-button">back to index</a>
				</div>
			</div>
			<? include('footer.php'); ?>
		</div>
	</body>
</html>