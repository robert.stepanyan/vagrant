<?
$dictionary = array(
	'main' => array(
		'it'	=> 'Principale',
		'en'	=> 'Main'
	),
	'about_us' => array(
		'it'	=> 'Su di noi',
		'en'	=> 'ABOUT US'
	),
	'terms' => array(
		'it'	=> 'Termini',
		'en'	=> 'Terms'
	),
	'our_sites' => array(
		'it'	=> 'I nostri siti',
		'en'	=> 'Our sites'
	),
	'privacy' => array(
		'it'	=> 'Privacy',
		'en'	=> 'Privacy'
	),
	'slogan_block_row_1' => array(
		'it'	=> 'Your advertising specialists!',
		'en'	=> 'Your advertising specialists!'
	),
	'slogan_block_row_2' => array(
		'it'	=> 'Offriamo una vasta gamma di pacchetti pubblicitari.',
		'en'	=> 'We offer a range of advertising packages.'
	),
	'slogan_block_row_3' => array(
		'it'	=> 'Sei stanca di pagare troppo per la tua pubblicità e non vedere buoni risultati?',
		'en'	=> 'Are you tired of paying too much for your advertising and not seeing good results?'
	),
	'slogan_block_row_4' => array(
		'it'	=> 'Noi abbiamo la soluzione giusta per voi! Offriamo una vasta gamma di pacchetti pubblicitari.',
		'en'	=> 'Then we have the right solution for you! We offer a range of advertising packages.'
	),
	'slogan_block_row_5' => array(
		'it'	=> 'Se i pacchetti non si adattano alle tue esigenze… nessun problema! Offriamo anche soluzioni personalizzate per qualsiasi tua esigenza. Per soluzioni personalizzate mettiti in contatto con il tuo rappresentante di vendita.',
		'en'	=> 'If the packages don\'t fit your needs - no problem. We also offer customized solutions for any demand. For customized solutions get in touch with your sales representative.'
	),
	'customized' => array(
		'it'	=> 'Personalizzato',
		'en'	=> 'Customized'
	),
	'buy_now' => array(
		'it'	=> 'Acquista adesso',
		'en'	=> 'BUY NOW'
	),
	'days' => array(
		'it'	=> 'Giorni',
		'en'	=> 'Days'
	),
	'international_listing' => array(
		'it'	=> 'Sezione Internazionali',
		'en'	=> 'International Listing'
	),
	'national_listing' => array(
		'it'	=> 'Sezione Nazionali',
		'en'	=> 'National Listing'
	),
	'girl_of_the_month' => array(
		'it'	=> 'Ragazza del mese',
		'en'	=> 'Girl of the month '
	),
	'main_premium_spot' => array(
		'it'	=> 'Premium in prima pagina',
		'en'	=> 'Main premium spot'
	),
	'tour_ability' => array(
		'it'	=> 'Possibilità di Tour',
		'en'	=> 'Tour ability'
	),
	'search' => array(
		'it'	=> 'Motore di ricerca',
		'en'	=> 'Search'
	),
	'main_premium_spot_silver' => array(
		'it'	=> 'Prima pagina della città scelta',
		'en'	=> 'Main premium spot'
	),
	'enter_custom_amount' => array(
		'it'	=> 'Per favore inserisci l\'ammontare in Euro che avete concordato con il vostro sales',
		'en'	=> 'Please enter the amount in EURO which you agreed with your sales person'
	),
	'advertising_terms' => array(
		'it'	=> 'Termini Pubblicitari',
		'en'	=> 'Advertising Terms'
	),
	'please_read_carefully' => array(
		'it'	=> 'Leggi attentamente i nostri termini pubblicitari. Ordinando qualsiasi confezione dal nostro sito accetti le nostre condizioni.',
		'en'	=> 'Please read carefully our advertising terms. By ordering any package from our site you agree with our conditions.'
	),
	'terms_row_1' => array(
		'it'	=> 'Quando invii il tuo ordine, verrai reindirizzato sul nostro provider esterno per i pagamenti.',
		'en'	=> 'When you submit your order, you will get redirected to our external payment provider.'
	),
	'terms_row_2' => array(
		'it'	=> 'Dopo aver effettuato con successo il pagamento potresti venire contattato dal nostro personale se avessimo bisogno di ulteriori dettagli.',
		'en'	=> 'After successful payment you may be contacted by our staff if we need more details.'
	),
	'terms_row_3' => array(
		'it'	=> 'Tutti gli ordini effettuati tramite il sito ' . $_SERVER['SERVER_NAME'] . ' non sono rimborsabili.',
		'en'	=> 'All orders done through ' . $_SERVER['SERVER_NAME'] . ' are non-refundable.'
	),
	'terms_row_4' => array(
		'it'	=> 'Tutte le informazioni verranno trattate in maniera confidenziale, leggi a proposito la nostra <a href="%link%">politica sulla privacy</a>.',
		'en'	=> 'All data are kept confidentially, pls read our <a href="%link%">privacy policy.</a>'
	),
	'terms_row_5' => array(
		'it'	=> 'Tutte le richieste verranno soddisfatte nel giro di 24-48 ore.',
		'en'	=> 'All requests will be answered within 24-48 hours.'
	),
	'disclaimer_of_liability' => array(
		'it'	=> 'Dichiarazione di non responsabilità',
		'en'	=> 'Disclaimer of liability'
	),
	'disclaimer_block_1' => array(
		'it'	=> 'Tutto il materiale presente sul sito è soltanto a titolo informativo e non fa riferimento a nessuna circostanza relativa a individui o enti. Le informazioni contenute nel sito non sono intese essere complete o esaurienti e potrebbero non risultare aggiornate, e quindi non dovrebbero essere considerate attendibili come tali. Non ci assumiamo o accettiamo nessuna responsabilità in relazione né alla disponibilità del sito internet né alle informazioni in esso contenute.',
		'en'	=> 'All material on the Site is for information only and does not address the circumstances of any particular individual or entity. The information on the Site is not intended to be comprehensive or complete and may not be up-to-date, and should not be relied on as such. We do not assume or accept any responsibility or liability in relation to either the availability of the Site or the information on it.'
	),
	'disclaimer_block_2' => array(
		'it'	=> 'Tutte le informazioni sono fornite così come risultano, senza garanzie di nessun genere e a vostro unico rischio. Non effettuiamo rappresentanze e ci esoneriamo da tutte le garanzie e condizioni sia espresse che implicite di qualunque genere in relazione alle informazioni contenute sul sito.',
		'en'	=> 'All information is provided "as is", without any warranties of any kind and at your sole risk. We make no representations and disclaim all express and implied warranties and conditions of any kind in relation to information on the Site.'
	),
	'disclaimer_block_3' => array(
		'it'	=> 'Sia che siate o meno avvisati della possibilità, né noi né nessuna altra parte coinvolta nella creazione, produzione, mantenimento o consegna del sito internet sarà considerato responsabile nei tuoi confronti per qualsiasi tipo di perdita o danno da te subito o rivendicato (o da qualsiasi persona in tuo nome) o da qualsiasi terza parte, che si manifesta in connessione con l\'uso, l\'inabilità ad utilizzare o il risultato dell\'utilizzo del sito e di qualsiasi altro sito internet collegato alla nostra pagina o al contenuto in esso presente, sia che tali perdite o danni risultino sofferti direttamente o indirettamente o siano immediati o conseguenti, e sia che si manifestino per negligenza, rottura del contratto o altro.',
		'en'	=> 'Whether or not advised of the possibility, neither we nor any other party involved in creating, producing, maintaining or delivering the Site shall be liable to you for any kind of loss or damage that may be suffered or claimed by you (or any person claiming under or through you) or to any third party, which arises in connection with the use of, inability to use or the results of use of the Site and any websites linked to the Site or the material on the Site or such other websites, whether such losses or damages are suffered directly or indirectly or are immediate or consequential and whether arising from negligence, breach of contract or otherwise.'
	),
	'disclaimer_block_4' => array(
		'it'	=> 'Niente in questa dichiarazione esclude o limita le nostre responsabilità per frode, per negligenza come causa di morte o infortuni personali, o per qualsiasi responsabilità verso le estensioni che non possono essere escluse o limitate sotto le leggi o i sistemi regolatori applicabili.',
		'en'	=> 'Nothing in this disclaimer excludes or limits our liability for fraud, for negligence causing death or personal injury, or for any liability to the extent that it cannot be excluded or limited under the applicable law or regulatory system.'
	),
	'governing_low' => array(
		'it'	=> 'Legge Governativa',
		'en'	=> 'Governing law'
	),
	'governing_low_block_1' => array(
		'it'	=> 'Questo sito e tutte le vertenze o altre questioni ad esso collegate saranno regolate dalla leggi vigenti nella città di Zurigo, Svizzera e legiferate dal tribunale della giurisdizione competente a Zurigo, Svizzera o in qualunque altra sede le parti possano concordare. Dal momento che non dovresti avere alcuna ragione per nutrire rimostranze nei nostri confronti, come disincentivo a dispute immotivate, accetti che in caso di una tua denuncia nei nostri confronti',
		'en'	=> 'This Site and all disputes or other matters arising out of it shall be governed by the laws of Zurich, Switzerland and dealt with by a court of competent jurisdiction in Zurich, Switzerland or such other location as the parties may agree. Since you should have no reason to have a grievance with us, as a disincentive to unwarranted litigation, you agree that if you sue us and don\'t win, you will pay all our costs, including reasonable fees for in-house and outside counsel.'
	),
	'privacy' => array(
		'it'	=> 'Privacy',
		'en'	=> 'Privacy'
	),
	'our_commitment_to_privacy' => array(
		'it'	=> 'Il nostro Impegno in Materia di Privacy',
		'en'	=> 'Our Commitment to Privacy'
	),
	'our_commitment_to_privacy_block_1' => array(
		'it'	=> 'La tua Privacy è importante per noi. Al fine di proteggere meglio la tua privacy ti forniamo questa informativa per spiegare le nostre pratiche sul trattamento delle informazioni online e le scelte che puoi fare su come queste informazioni vengono raccolte ed utilizzate. Per rendere questa informativa facile da trovare, la renderemo disponibile all\'interno della nostra homepage e in ogni punto dove potrebbe essere richiesto l\'inserimento di dati personali.',
		'en'	=> 'Your Privacy is important to us. To better protect your privacy we provide this notice explaining our online information practices and the choices you can make about the way your information is collected and used. To make this notice easy to find, we make it available an our homepage and at every point where personally identifiable information may be requested.'
	),
	'the_information_we_collect' => array(
		'it'	=> 'Le informazioni che Raccogliamo',
		'en'	=> 'The Information We Collect'
	),
	'the_information_we_collect_block_1' => array(
		'it'	=> 'Questa informativa viene applicata a tutte le informazioni raccolte o inviate all\'interno di questo sito internet. In alcune pagine, puoi ordinare prodotti/sottoscrizioni, effettuare richieste, e registrarti per ricevere del materiale. La tipologia di informazioni personali raccolte in queste pagine sono: Nome completo, Nome Utente, Indirizzo email',
		'en'	=> 'This notice applies to all information collected or submitted on this website. On some pages, you can order products/subscriptions, make requests, and register to receive materials. The types of personal information collected at these pages are: Fullname, Username, Email address.'
	),
	'the_way_we_use_information' => array(
		'it'	=> 'Il Modo in cui Utilizziamo le Informazioni',
		'en'	=> 'The Way We Use Information'
	),
	'the_way_we_use_information_block_1' => array(
		'it'	=> 'Utilizziamo le informazioni raccolte per la protezione dell\'accesso al nostro sistema (nome utente, password) e per la verifica utente (email).',
		'en'	=> 'We use collected information for protection of access to our system (username, password) and for user verification (email).'
	),
	'our_commitment_to_data_security' => array(
		'it'	=> 'Il nostro Impegno per la Sicurezza dei Dati',
		'en'	=> 'Our Commitment To Data Security'
	),
	'our_commitment_to_data_security_block_1' => array(
		'it'	=> 'Per impedire accessi non autorizzati, mantenere la precisione dei dati, e assicurare il corretto uso delle informazioni, abbiamo messo in atto delle appropriate procedure fisiche, elettroniche, e di gestione al fine di salvaguardare e rendere sicure le informazioni che raccogliamo online.',
		'en'	=> 'To Prevent unauthorized access, maintain data accuracy, and ensure the correct use of information, we have put in place appropriate physical, electronic, and managerial procedures to safeguard and secure the information we collect online.'
	),
	'how_you_can_access_your_information' => array(
		'it'	=> 'Come puoi Accedere o Correggere le Tue Informazioni',
		'en'	=> 'How You Can Access Or Correct Your Information'
	),
	'how_you_can_access_your_information_block_1' => array(
		'it'	=> 'Puoi accedere a tutte le tue informazioni di identificazione personale che raccogliamo online e che manteniamo tramite un database SQL. Utilizziamo questa procedura per meglio salvaguardare le tue informazioni.',
		'en'	=> 'You can access all your personally identifiable information that we collect online and maintain by SQL database. We use this procedure to better safeguard your information.'
	),
	'how_you_can_access_your_information_block_2' => array(
		'it'	=> 'Al fine di proteggere la tua privacy e la tua sicurezza, ti verrà inoltre richiesto di superare alcuni passaggi per verificare la tua identità prima di consentire l\'accesso o la modifica delle informazioni. Come Contattarci ',
		'en'	=> 'To protect your privacy and security, we will also take reasonalble steps to verify your identity before granting access or making corrections. How to Contact us'
	),
	'how_you_can_access_your_information_block_3' => array(
		'it'	=> 'Se dovessi avere altre domande o dubbi riguardanti queste politiche sulla privacy, inviaci un messaggio',
		'en'	=> 'Should you have other questions or concerns about these privacy policies, send us an message'
	),
	'footer_text' => array(
		'it'	=> 'AA Media Services GmbH, Industriestrasse 28, 9100 Herisau. AA Media Services GmbH ha sede in Svizzera, dove adempie al pagamento delle tasse per tutti i servizi.',
		'en'	=> 'AA Media Services GmbH, Industriestrasse 28, 9100 Herisau. AA Media Services GmbH is placed in Switzerland, where it pays taxes for all services.'
	),
	'terms_top' => array(
		'it'	=> 'Questo sito è regolato e gestito da AA Media Services GmbH, dalla sede svizzera.',
		'en'	=> 'This website is owned, regulated and operated by AA Media Services GmbH, from the Switzerland offices.'
	),
	'attention_new_domain' => array(
        'it'	=> 'Attenzione! Nuovo dominio!',
        'en'	=> 'Attention! New domain!'
	),
	'adspay_new_address' => array(
        'it'	=> 'Adspay789.com è stato trasferito su un nuovo indirizzo,',
        'en'	=> 'Adspay789.com moved to a new address,'
	),

	'click_on_the_button' => array(
        'it'	=> 'Clicca sul pulsante sottostante per visitare il nuovo dominio e acquistare il tuo pacchetto pubblicitario.',
        'en'	=> 'Click on the button below to visit the new domain and purchase your advertising package.'
	),
	'go_to_new_domain' => array(
        'en'	=> 'Go to new domain',
        'it'	=> 'Vai al nuovo dominio'
	),
    'auto_redirect' => array(
        'it'	=> 'Verrai automaticamente reindirizzato tra',
        'en'	=> 'You will be redirected automatically in'
    )
	,
	'seconds' => array(
        'it'	=> 'secondi',
        'en'	=> 'seconds'
	)
);
?>
