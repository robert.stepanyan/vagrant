<?
include('inc/common.php');
$nbx = new Cube_NetbanxAPI();
$id = $_GET['id'];
$order = $nbx->get_order($id);
$date = new DateTime($order->transaction->lastUpdate, new DateTimeZone('America/New_York'));
$date->setTimezone(new DateTimeZone('Europe/Rome'));
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" media="screen" href="css/style.css"/>
		<link rel="icon" href="/images/favicon.ico" type="image/png" />
	</head>
	<body>
		<div id="main-wrapper">
			<? $page = 'success'; ?>
			<? include('header.php'); ?>
			<div id="container" class="centered-wrapper sm">
				<h1 class="successful-header">Successful transaction!</h1>
				<div class="cont-sep"></div>
				<div class="successful-payment-container">
					<h3>Your payment was successful, thank you very much.</h3>
					<p>Please contact your sales person with the payment details</p>
					
					<ul class="common">
						<li>Address: <?= $order->billingDetails->street ?></li>
						<li>Zip: <?= $order->billingDetails->zip ?></li>
						<li>Country: <?= strtoupper($order->transaction->card->country) ?></li>
						<li>Transaction Id: <?= $order->transaction->confirmationNumber ?></li>
						<li>Amount: <?=  number_format($order->transaction->amount / 100, 2) ?></li>
						<li>Currency: <?=  $order->transaction->currencyCode ?></li>
						<li>Date: <?=  $date->format('Y-m-d H:i:s') ?></li>
					</ul>
					<a class="bth ttu" href="/">back to home</a>
				</div>
			</div>
			<? include('footer.php'); ?>
		</div>
	</body>
</html>