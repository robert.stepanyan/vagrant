<?php

class Api_SyncController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;

	protected $_debug = true;

	protected $_log;

	/**
	 * @var Cubix_Api
	 */
	protected $_client;

	public function init()
	{
		$this->_db = Zend_Registry::get('db');
		$this->_client = Cubix_Api::getInstance();

		$this->view->layout()->disableLayout();
	}

	/**
	 * @var Cubix_Cli
	 */
	private $_cli;

	public function diffAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/6a-diff-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/6a-diff-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		$start = 0; $limit = 100;
		do {
			// <editor-fold defaultstate="collapsed" desc="Fetch Data from API">
			$cli->out('Fetching data from api (max 100)...');
			$data = $this->_client->call('sync.getDiffEscorts', array($start++ * $limit, $limit));
			// </editor-fold>
//print_r($data); die;
			// IMPORTANT! This array will contain ids of escorts which were successuflly
			// removed or updated this array will be passed to api to mark those escorts
			// in journal as synchronized
			$success = array();

			// <editor-fold defaultstate="collapsed" desc="Process escorts which need to be removed">
			if ( count($data['removed']) > 0 ) {
				$cli->colorize('purple')
					->out('Need to remove ' . ($count = count($data['removed'])) . ' escorts...')
					->reset();
				foreach ( $data['removed'] as $i => $escort ) {
					$percent = round((($i + 1) / $count) * 100) . '%';
					// Shorthand variables
					list($escort_id, $showname) = $escort;

					try {
						$this->_removeEscort($escort_id);
						$success[] = $escort_id;
						$result = true;
					}
					catch ( Exception $e ) {
						$result = false;
						// If error occured just log the error and display output in red color
						$log->error("Unable to remove escort '$showname': " . $e->getMessage());
					}

					// Pretty output
					$cli->out('[' . ($i + 1) . '/' . $count . ']', false)
						->colorize($result ? 'green' : 'red')
						->out(' Remove ' . $showname . '...', false)
						->column(60)
						->reset()
						->out($percent, true);

				}
				
				$cli->out();
			}
			// </editor-fold>

			// <editor-fold defaultstate="collapsed" desc="Process escorts which need to be updated">
			if ( count($data['updated']) > 0 ) {
				$cli->colorize('purple')
						->out('Need to update ' . ($count = count($data['updated'])) . ' escorts...')
						->reset();
				foreach ( $data['updated'] as $i => $escort ) {
					$percent = round((($i + 1) / $count) * 100) . '%';
					// Shorthand variables
					$escort_id = $escort['profile']['id'];
					$showname = $escort['profile']['showname'];

					// <editor-fold defaultstate="collapsed" desc="Remove escort at first">
					try {
						$this->_removeEscort($escort_id);
						$result = true;
					}
					catch ( Exception $e ) {
						$result = false;
						$log->error("Unable to remove escort '$showname': " . $e->getMessage());
					}

					$cli->out('[' . ($i + 1) . '/' . $count . ']', false)
						->colorize($result ? 'green' : 'red')
						->out(' Remove ' . $showname . '...', false)
						->column(60)
						->reset()
						->out($percent, true);
					// </editor-fold>

					// If there is error removing error just ignore this one
					if ( ! $result ) continue;

					try {
						// Insert escort and all correspoinding data (services, cities, profiles, photos, etc.))
						$this->_insertEscort($escort);
						$success[] = $escort_id;
						$result = true;
					}
					catch ( Exception $e ) {
						$result = false;
						// Just log into file if something happend and ignore
						$log->error("Unable to insert escort '$showname': " . $e->getMessage());
					}

					// Pretty output
					$cli->out('[' . ($i + 1) . '/' . $count . ']', false)
						->colorize($result ? 'green' : 'red')
						->out(' Insert ' . $showname . '...', false)
						->column(60)
						->reset()
						->out($percent, true);
				}
				
				$cli->out();
			}
			// </editor-fold>

			// If there were successfull escort syncs
			if ( count($success) > 0 ) {
				// <editor-fold defaultstate="collapsed" desc="Finally mark escorts as sync done to prevent continious syncing">
				// We don't want any extra data to be passed to API
				$success = array_unique($success);
				// Pass to API to mark these escorts as sync done
				$this->_client->call('sync.markEscortsDone', array($success));
				// </editor-fold>

				// And clear the cache
			}
			else {
				$cli->colorize('yellow')->out('Nothing happend :)')->reset();
			}
		} while ( count($data['removed']) > 0 || count($data['updated']) > 0 );

		// At the end we need to repopulate cache table for optimized sqls
		// this table contains rows that describe in which city escort will be shown
		// and why is she there, because of upcoming, tour or just working city
		// also there are some reduntunt fields such as is_premium, is_agency,
		// gender and so on
		$cli->out()->out('Populating cache tables...', false);
		try {
			$this->_populateCacheTables();
			$cli->column(60)->colorize('green')->out('success');
		}
		catch ( Exception $e ) {
			$cli->column(60)->colorize('red')->out('error');
			$log->error($e->getMessage());
		}
		$cli->out();

		$clear_cache = file_get_contents(Cubix_Application::getById(Cubix_Application::getId())->url . "/api/escorts/clear-cache");
		$cli->reset()->out($clear_cache);

		exit;
	}

	private function _removeEscort($escort_id)
	{
		$this->_db->delete('escorts', $this->_db->quoteInto('id = ?', $escort_id));
		$this->_db->delete('escort_services', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_cities', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_cityzones', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_photos', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('upcoming_tours', $this->_db->quoteInto('id = ?', $escort_id));
		$this->_db->delete('escort_profiles', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('premium_cities', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_working_times', $this->_db->quoteInto('escort_id = ?', $escort_id));
//		$this->_db->delete('escorts_in_cities', $this->_db->quoteInto('escort_id = ?', $escort_id));
	}

	private function _populateCacheTables()
	{
		
		try {
			$this->_db->query('TRUNCATE escorts_in_cityzones');
			$sql = '
				INSERT INTO escorts_in_cityzones (escort_id, city_id, cityzone_id, gender, is_agency, is_tour, is_upcoming, is_premium)
				SELECT x.escort_id, x.id AS city_id, x.cityzone_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium
				FROM ((
					SELECT
						cz.city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
						EXISTS (SELECT 1 FROM premium_cities WHERE escort_id = e.id AND city_id = cz.id) AS is_premium
					FROM cityzones AS cz
					INNER JOIN escort_cityzones ecz ON ecz.city_zone_id = cz.id
					INNER JOIN escorts e ON e.id = ecz.escort_id
					INNER JOIN cities ct ON ct.id = cz.city_id
					WHERE ct.country_id = 199 /*AND e.is_on_tour = 0*/ AND FIND_IN_SET(1, e.products) > 0
				) UNION (
					SELECT
						ut.tour_city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM upcoming_tours ut
					INNER JOIN cities ct ON ct.id = ut.tour_city_id
					INNER JOIN escorts e ON e.id = ut.id
					INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id
					INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id AND cz.city_id = ct.id
					WHERE ct.country_id = 199 AND/* e.is_on_tour = 0 AND*/ FIND_IN_SET(7, e.products) > 0 AND
						DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY
				) UNION (
					SELECT
						e.tour_city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM escorts e
					INNER JOIN cities ct ON ct.id = e.tour_city_id
					INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id
					INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id AND cz.city_id = ct.id
					WHERE
						ct.country_id = 199 AND e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0
				)) x
			';
			$this->_db->query($sql);
		}
		catch ( Exception $e ) {
			throw new Exception("Unable to repopulate cache table 'escorts_in_cityzones'\nError: {$e->getMessage()}");
		}
		
		try {
			$this->_db->query('TRUNCATE escorts_in_cities');
			$sql = '
				INSERT INTO escorts_in_cities (escort_id, region_id, city_id, gender, is_agency, is_tour, is_upcoming, is_premium, is_base, ordering)
				SELECT x.escort_id, x.region_id, x.id AS city_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium, x.is_base, x.ordering
				FROM ((
					SELECT
						ct.id AS id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
						EXISTS (SELECT 1 FROM premium_cities WHERE escort_id = e.id AND city_id = ct.id) AS is_premium,
						(e.city_id = ct.id) AS is_base
					FROM cities AS ct
					INNER JOIN escort_cities ec ON ec.city_id = ct.id
					INNER JOIN escorts e ON e.id = ec.escort_id
					WHERE ct.country_id = 199 /*AND e.is_on_tour = 0*/ AND FIND_IN_SET(1, e.products) > 0
				) UNION (
					SELECT
						ut.tour_city_id AS id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
						e.is_tour_premium AS is_premium,
						FALSE AS is_base
					FROM upcoming_tours ut
					INNER JOIN cities ct ON ct.id = ut.tour_city_id
					INNER JOIN escorts e ON e.id = ut.id
					WHERE ct.country_id = 199 AND/* e.is_on_tour = 0 AND*/ FIND_IN_SET(7, e.products) > 0 AND
						DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY
				) UNION (
					SELECT
						e.tour_city_id AS id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
						e.is_tour_premium AS is_premium,
						TRUE AS is_base
					FROM escorts e
					INNER JOIN cities ct ON ct.id = e.tour_city_id
					WHERE
						ct.country_id = 199 AND e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0
				)) x
			';
			$this->_db->query($sql);
			
			$sql = '
				DELETE eic FROM escorts_in_cities AS eic
				WHERE ((SELECT e.is_on_tour FROM escorts e WHERE e.id = eic.escort_id) AND eic.is_tour = 0 AND eic.is_base = 0 AND eic.is_upcoming <> 1)
			';
			$this->_db->query($sql);			
			
		}
		catch ( Exception $e ) {
			throw new Exception("Unable to repopulate cache table 'escorts_in_cities'\nError: {$e->getMessage()}");
		}
	}

	private function _insertEscort($escort)
	{
		$cli = $this->_cli;
		$escort_id = $escort['profile']['id'];
		$showname = $escort['profile']['showname'];

		if ( isset($escort['products']) ) {
			$escort['profile']['products'] = implode(',', $escort['products']);
		}
		// Insert main escort row
		$this->_db->insert('escorts', $escort['profile']);

		// Insert escort services
		foreach ( $escort['services'] as $service ) {
			$service['escort_id'] = $escort_id;
			try {
				$this->_db->insert('escort_services', $service);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert service for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($service, true));
			}
		}

		// Insert escort working cities
		foreach ( $escort['cities'] as $city ) {
			$city = array('escort_id' => $escort_id, 'city_id' => $city);
			try {
				$this->_db->insert('escort_cities', $city);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert city for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($city, true));
			}
		}

		// Insert escort cityzones
		foreach ( $escort['cityzones'] as $cityzone ) {
			$cityzone = array('escort_id' => $escort_id, 'city_zone_id' => $cityzone);
			try {
				$this->_db->insert('escort_cityzones', $cityzone);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert cityzone for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($cityzone, true));
			}
		}

		// Insert escort photos
		foreach ( $escort['photos'] as $photo ) {
			$photo['escort_id'] = $escort_id;
			try {
				$this->_db->insert('escort_photos', $photo);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert photo for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($photo, true));
			}
		}

		// Insert escort upcoming tours
		foreach ( $escort['tours'] as $tour ) {
			$tour['id'] = $escort_id;
			try {
				$this->_db->insert('upcoming_tours', $tour);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert tour for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($tour, true));
			}
		}

		// Insert escort profile
		try {
			if ( isset($escort['snapshot']) ) {
				$snapshot = array('escort_id' => $escort_id, 'data' => serialize($escort['snapshot']));
				$this->_db->insert('escort_profiles', $snapshot);
			}
		}
		catch ( Exception $e ) {
			throw new Exception("Unable to insert profile for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($snapshot, true));
		}


		// Finally if escort has premium cities insert them
		if ( isset($escort['premiums']) ) {
			foreach ( $escort['premiums'] as $city ) {
				$city = array('escort_id' => $escort_id, 'city_id' => $city);
				try {
					$this->_db->insert('premium_cities', $city);
				}
				catch ( Exception $e ) {
					throw new Exception("Unable to insert premium city for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($city, true));
				}
			}
		}

		foreach ( $escort['times'] as $time ) {
			$time['escort_id'] = $escort_id;
			try {
				$this->_db->insert('escort_working_times', $time);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert working time for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($time, true));
			}
		}
	}

	public function reviewsAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/6a-reviews-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/6a-reviews-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Transfer Reviews">
		$chunk = 4000;
		$part = 0;
		$cli->out('Fetching reviews data from api (chunk size: ' . $chunk . ')...');
		$reviews = array();
		do {
			$cli->reset()->out('part #' . ($part + 1), false);
			$data = $this->_client->call('getEscortReviewsV2', array($part++ * $chunk, $chunk));
			if ( isset($data['error']) ) {
				$cli->column(60)->colorize('red')->out('error');
				$log->error('Error when fetching data from api: ' . $data['error']);
				continue;
			}

			$reviews = array_merge($reviews, $data['result']);
			$cli->column(60)->colorize('green')->out('success');
		} while ( count($data['result']) > 0 );
		$data = null; unset($data);
		$cli->reset()->out('Done fetching data (' . count($reviews) . ' reviews fetched).')->out();
		// </editor-fold>


		// <editor-fold defaultstate="collapsed" desc="Insert data into database">
		$cli->out('Truncate table `reviews`', false);
		try {
			$this->_db->query('TRUNCATE `reviews`');
			$cli->column(60)->colorize('green')->out('success');
		}
		catch ( Exception $e ) {
			$cli->column(60)->colorize('red')->out('error');
			$log->error('Error when truncating reviews table: ' . $e->getMessage());
			exit(1);
		}

		$cli->reset()->out('Inserting reviews...');
		$c = 0;
		foreach ( $reviews as $review ) {
			try {
				$this->_db->insert('reviews', $review);
				$c++;
			}
			catch ( Exception $e ) {
				$cli->colorize('red')->out('Could not insert review #' . $review['id'] . '')->reset();
				$log->error('Error when inserting review #' . $review['id'] . ': ' . $e->getMessage());
			}
		}
		$cli->out('Successfully inserted ' . $c . ' reviews.');
		// </editor-fold>

		exit(0);
	}

	public function commentsAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/6a-comments-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/6a-comments-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Transfer Comments">
		$chunk = 4000;
		$part = 0;
		$cli->out('Fetching comments data from api (chunk size: ' . $chunk . ')...');
		$comments = array();
		do {
			$cli->reset()->out('part #' . ($part + 1), false);
			$data = $this->_client->call('getCommentsV2', array($part++ * $chunk, $chunk));
			if ( isset($data['error']) ) {
				$cli->column(60)->colorize('red')->out('error');
				$log->error('Error when fetching data from api: ' . $data['error']);
				continue;
			}

			$comments = array_merge($comments, $data['result']);
			$cli->column(60)->colorize('green')->out('success');
		} while ( count($data['result']) > 0 );
		$data = null; unset($data);
		$cli->reset()->out('Done fetching data (' . count($comments) . ' comments fetched).')->out();
		// </editor-fold>


		// <editor-fold defaultstate="collapsed" desc="Insert data into database">
		$cli->out('Truncate table `comments`', false);
		try {
			$this->_db->query('TRUNCATE `comments`');
			$cli->column(60)->colorize('green')->out('success');
		}
		catch ( Exception $e ) {
			$cli->column(60)->colorize('red')->out('error');
			$log->error('Error when truncating comments table: ' . $e->getMessage());
			exit(1);
		}

		$cli->reset()->out('Inserting comments...');
		$c = 0;
		foreach ( $comments as $comment ) {
			try {
				if ( ! $comment['is_premium'] ) {
					$comment['is_premium'] = 0;
				}
				if ( ! $comment['comments_count'] ) {
					$comment['comments_count'] = 0;
				}
				$this->_db->insert('comments', $comment);
				$c++;
			}
			catch ( Exception $e ) {
				echo $e->getMessage();
				$cli->colorize('red')->out('Could not insert comment #' . $comment['id'] . '')->reset();
				$log->error('Error when inserting comment #' . $comment['id'] . ': ' . $e->getMessage());
			}
		}
		$cli->out('Successfully inserted ' . $c . ' comments.');
		// </editor-fold>

		exit(0);
	}

	public function chatOnlineCronAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/6a-chat-online-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/6a-chat-online-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		$online_users = @file_get_contents('http://www.6annonce.com:35555/roomonlineusers.js?roomid=1');

		// Reset all online users
		$this->_db->update('escorts', array('is_online' => 0), $this->_db->quoteInto('is_online = ?', 1));

		if ( strlen($online_users) > 0 ) {
			$online_users = substr($online_users, 20, (strlen($online_users) - 21));

			$online_users = json_decode($online_users);

			$usernames = array();
			if ( count($online_users) > 0 ) {
				foreach ( $online_users as $user ) {
					$usernames[] = $user->name;
					echo "Escort {$user->name} is online now ! \n\r";
				}
			}
			
			$this->_db->query('UPDATE escorts SET is_online = 1 WHERE username IN (\'' . implode('\',\'', $usernames) . '\')');
		}

		die;
	}
	
	public function photoRotateAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/6a-photo-rotate-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/6a-photo-rotate-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

	
		$chunk = 4000;
		$part = 0;
		$cli->out('Fetching escorts data from database (chunk size: ' . $chunk . ')...');
		
		$photos_model = new Model_Escort_Photos();
		do {
			$cli->reset()->out('part #' . ($part + 1), false);
			$escorts = $this->_db->fetchAll('SELECT id, showname from escorts LIMIT ' . $part++ * $chunk . ', ' . $chunk);
			foreach($escorts as $escort)
			{
				$photos = $photos_model->getRotatablePicsByEscortId($escort->id);
				
				if(!is_null($photos)){
					
					$next_main_image = 0;
					$rotate_done = 0;
					$i = 0;
					
					foreach($photos['results'] as $photo){
						$i++;
						if($i == 1){
							$first_photo_id = $photo->id;
						}
						if($next_main_image == 1){
							$photos_model->setMainImageFront($photo->id);
							$rotate_done = 1;
							BREAK;
						}
						if($photo->is_main == 1){
							$next_main_image = 1;
						}
						if($photos['count'] == $i && $rotate_done == 0 ){
							$photos_model->setMainImageFront($first_photo_id);
						}	
					}
				}
				
				
			}
			
		} while ($escorts);
		$cli->column(60)->colorize('green')->out('success');
		$cli->reset()->out('Rotating Main Images Done ')->out();
		unset($escorts);
		
		$clear_cache = file_get_contents(Cubix_Application::getById(Cubix_Application::getId())->url . "/api/escorts/clear-cache");
		$cli->reset()->out($clear_cache);
		
		exit(0);
	}

}

