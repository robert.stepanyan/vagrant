<?php

class Model_Members extends Cubix_Model
{
	public function save($member)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$member_id = $client->call('Members.saveMember', array( (array) $member));
		$member->setId($member_id);

		return $member;
	}
	
	public function getEscortAlerts($user_id, $escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.getEscortAlerts', array($user_id, $escort_id));
	}
	
	public function memberAlertsSave($user_id, $escort_id, $events, $extra = null)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.alertsSave', array($user_id, $escort_id, $events, $extra));
	}
	
	public function getAlerts($user_id)
	{
		//$client = new Cubix_Api_XmlRpc_Client();
		//return $client->call('Members.getAlerts', array($user_id));
		$client = Cubix_Api::getInstance();
		return $client->call('getMemberAlerts', array($user_id));
	}

	public function getMembersInfoByUsername($username)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.getMembersInfoByUsername', array($username));
	}
}
