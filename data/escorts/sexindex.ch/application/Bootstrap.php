<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	public function __construct($application)
	{
		require('../application/models/Plugin/Activity.php');

		parent::__construct($application);
	}

	public function run()
	{
		//Zend_Layout::startMvc();

		// Initialize hooks to avoid code confusion
		Model_Hooks::init();

		parent::run();
	}

	protected function _initRoutes()
	{
		require('../../library/Cubix/Security/Plugin.php');
		require('../application/models/Plugin/I18n.php');

		$this->bootstrap('frontController');
		$front = $this->getResource('frontController');

		$router = $front->getRouter();
		$router->removeDefaultRoutes();


        



		$router->addRoute(
			'home-page',
			new Zend_Controller_Router_Route(
				':lang_id/*',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index',
					'lang_id' => ''
				), array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'external-link',
			new Zend_Controller_Router_Route(
				'go',
				array(
					'module' => 'default',
					'controller' => 'index',
					'action' => 'go'
				)
			)
		);

		$router->addRoute(
			'captcha',
			new Zend_Controller_Router_Route(':lang_id/captcha',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'captcha'
			))
		);

		$router->addRoute(
			'captcha',
			new Zend_Controller_Router_Route(':lang_id/captcha',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'captcha'
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'captcha-def',
			new Zend_Controller_Router_Route('captcha',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'captcha'
			))
		);

		/*REDIRECT*/
		$router->addRoute(
			'agency-profile-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^agency.php(.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'agency'
				),
				array(
					1 => 'agencyId',
				)
			)
		);
		/*REDIRECT*/
//		$router->addRoute(
//			'escorts-filter-redirect',
//			new Zend_Controller_Router_Route_Regex(
//				'^escorts/(.+)?',
//				array(
//					'module' => 'default',
//					'controller' => 'redirect',
//					'action' => 'index'
//				),
//				array(
//					1 => 'req'
//				)
//			)
//		);

		$router->addRoute(
			'escorts-filter',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index'
				),
				array(
					1 => 'lang_id',
					2 => 'req'
				)
			)
		);

		$router->addRoute(
			'escorts-filter-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'index'
				),
				array(
					1 => 'req'
				)
			)
		);
		
		$router->addRoute(
			'escorts-redirect-to-main',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'main-page'
				)				
			),
			array(
				1 => 'lang'
			)
		);
		$router->addRoute(
			'def-escorts-redirect-to-main',
			new Zend_Controller_Router_Route_Regex(
				'^escorts',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'main-page'
				)				
			)
		);

		// <editor-fold defaultstate="collapsed" desc="Compatibility Redirects">
		$router->addRoute(
			'main-page-en-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/(en)/?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'main-page'
				),
				array(
					1 => 'lang'
				)
			)
		);

		$router->addRoute(
			'reviews-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^evaluations(/(en|it|de|fr))?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'reviews'
				),
				array(
					2 => 'lang'
				)
			)
		);

		/*$router->addRoute(
			'reviews-escort-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^evaluations/((en|it|de|fr)/)?(.+?)(/([0-9]+))?$',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'reviews-escort'
				),
				array(
					2 => 'lang',
					3 => 'showname',
					5 => 'page'
				)
			)
		);*/
		
		
		$router->addRoute(
			'escorts-filter-global-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/(en)(/(boys|trans|citytours|upcomingtours|nuove|agence|independantes))?(/(city|region)_fr_(.+?))?(/(incall|outcall))?(/([0-9]+))?$',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'geo'
				),
				array(
					1 => 'lang',
					3 => 'type',
					5 => 'geo',
					6 => 'geoval',
					8 => 'filter',
					10 => 'page'
				)
			)
		);

		/*$router->addRoute(
			'escorts-filter-global-redirect-require-page',
			new Zend_Controller_Router_Route_Regex(
				'^escorts(/(boys|trans|citytours|upcomingtours|nuove|agence|independantes))?(/(city|region)_fr_(.+?))?(/(incall|outcall))?(/([0-9]+))$',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'geo',
					'lang' => null
				),
				array(
					2 => 'type',
					4 => 'geo',
					5 => 'geoval',
					7 => 'filter',
					9 => 'page'
				)
			)
		);*/

		// </editor-fold>





		$router->addRoute(
			'static-pages',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/page/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'static-page',
					'action' => 'show'
				),
				array(
					1 => 'lang_id',
					2 => 'page_slug'
				)
			)
		);

		$router->addRoute(
			'static-pages-def',
			new Zend_Controller_Router_Route_Regex(
				'^page/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'static-page',
					'action' => 'show'
				),
				array(
					1 => 'page_slug'
				)
			)
		);

		$router->addRoute(
			'escort-photos',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts/photos',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'photos'
				), array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'escort-photos-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/photos',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'photos'
				)
			)
		);

		$router->addRoute(
			'escort-private-photos',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts/private-photos',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'private-photos'
				), array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'escort-private-photos-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/private-photos',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'private-photos'
				)
			)
		);

		$router->addRoute(
			'agency-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts/agency-escorts',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'agency-escorts'
				), array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'agency-escorts-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/agency-escorts',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'agency-escorts'
				)
			)
		);

		/*$router->addRoute(
			'agency-profile',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/agency/([^/]+)(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'show'
				),
				array(
					1 => 'lang_id',
					2 => 'agencyName',
					3 => 'req'
				)
			)
		);

		$router->addRoute(
			'agency-profile-def',
			new Zend_Controller_Router_Route_Regex(
				'^agency/([^/]+)(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'show'
				),
				array(
					1 => 'agencyName',
					2 => 'req'
				)
			)
		);*/
		
		/*REDIRECT*/
		$router->addRoute(
			'agency-profile-redirect2',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/agency/([^/]+)(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'agencies2'
				),
				array(
					1 => 'lang_id',
					2 => 'agencyName',
					3 => 'req'
				)
			)
		);

		$router->addRoute(
			'agency-profile-redirect2-def',
			new Zend_Controller_Router_Route_Regex(
				'^agency/([^/]+)(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'agencies2'
				),
				array(
					1 => 'agencyName',
					2 => 'req'
				)
			)
		);
		//
		
		$router->addRoute(
			'agency-profile',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/agency/(.+)?\-([0-9]+)(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'show'
				),
				array(
					1 => 'lang_id',
					2 => 'agencyName',
					3 => 'agency_id',
					4 => 'req'
				)
			)
		);

		$router->addRoute(
			'agency-profile-def',
			new Zend_Controller_Router_Route_Regex(
				'^agency/(.+)?\-([0-9]+)(/.+)?',
				array(
					'module' => 'default',
					'controller' => 'agencies',
					'action' => 'show'
				),
				array(
					1 => 'agencyName',
					2 => 'agency_id',
					3 => 'req'
				)
			)
		);

		/*REDIRECT*/
		$router->addRoute(
			'escort-profile-redirect2',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escort/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'escort2'
				),
				array(
					1 => 'lang_id',
					2 => 'showname'
				)
			)
		);

		$router->addRoute(
			'escort-profile-redirect2-def',
			new Zend_Controller_Router_Route_Regex(
				'^escort/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'escort2'
				),
				array(
					1 => 'showname'
				)
			)
		);
		//

		$router->addRoute(
			'escort-profile',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escort/(.+)?\-([0-9]+)$',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'profile'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'escort-profile-def',
			new Zend_Controller_Router_Route_Regex(
				'^escort/(.+)?\-([0-9]+)$',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'profile'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

		/*REDIRECT*/
		$router->addRoute(
			'escort-profile-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^escort/(.+?)/(it|fr|de|en)$',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'escort'
				),
				array(
					1 => 'showname',
					2 => 'lang'
				)
			)
		);

		/*REDIRECT*/
		$router->addRoute(
			'directory-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^escorte-directory/((en)|fr)-(.+)',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'directory'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'login-redirect',
			new Zend_Controller_Router_Route_Regex(
				'^login.php',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'login'
				)
			)
		);

		$router->addRoute(
			'last-viewed-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts/viewed-escorts',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'viewed-escorts'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'last-viewed-escorts-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/viewed-escorts',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'viewed-escorts'
				)
			)
		);

		$router->addRoute(
			'late-night-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/escorts/late-night-girls',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'late-night-girls'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'late-night-escorts-def',
			new Zend_Controller_Router_Route_Regex(
				'^escorts/late-night-girls',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'late-night-girls'
				)
			)
		);

		$router->addRoute(
			'escorts-search',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/search',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'search'
				), array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'escorts-search-def',
			new Zend_Controller_Router_Route_Regex(
				'^search',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'search'
				)
			)
		);

		$router->addRoute(
			'geography',
			new Zend_Controller_Router_Route(':lang_id/geography/:action/*',
			array(
				'module' => 'default',
				'controller' => 'geography',
				'action' => 'index'
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'geography-def',
			new Zend_Controller_Router_Route('geography/:action/*',
			array(
				'module' => 'default',
				'controller' => 'geography',
				'action' => 'index'
			))
		);

		$router->addRoute(
			'private',
			new Zend_Controller_Router_Route(':lang_id/private/:action/*',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'index'
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'private-def',
			new Zend_Controller_Router_Route('private/:action/*',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'index'
			))
		);

		

		$router->addRoute(
			'privateV2Lng',
			new Zend_Controller_Router_Route(':lang_id/private-v2/:action/*',
			array(
				'module' => 'default',
				'controller' => 'private-v2',
				'action' => 'index',
				'lang_id' => ''
			),
			array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'privateV2',
			new Zend_Controller_Router_Route('private-v2/:action/*',
			array(
				'module' => 'default',
				'controller' => 'private-v2',
				'action' => 'index'
			))
		);

		$router->addRoute(
			'privateProfileLng',
			new Zend_Controller_Router_Route(':lang_id/private-v2/profile/:action/*',
			array(
				'module' => 'default',
				'controller' => 'profile',
				'action' => 'index',
				'lang_id' => ''
			),
			array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'privateProfile',
			new Zend_Controller_Router_Route('private-v2/profile/:action/*',
			array(
				'module' => 'default',
				'controller' => 'profile',
				'action' => 'index'
			))
		);

		$router->addRoute(
			'splash',
			new Zend_Controller_Router_Route('/splash',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'splash'
			))
		);

		$router->addRoute(
			'robots',
			new Zend_Controller_Router_Route('/robots.txt',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'robots'
			))
		);

		$router->addRoute(
			'private-signup',
			new Zend_Controller_Router_Route_Regex('([a-z]{2})/private/signup-(member|vip-member|escort|agency)',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'signup'
			), array(
				1 => 'lang_id',
				2 => 'type'
			))
		);

		$router->addRoute(
			'private-signup-def',
			new Zend_Controller_Router_Route_Regex('private/signup-(member|vip-member|escort|agency)',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'signup'
			), array(
				1 => 'type'
			))
		);

		$router->addRoute(
			'private-remove-from-favorites',
			new Zend_Controller_Router_Route_Regex('([a-z]{2})/private/remove-from-favorites(.+)',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'remove-from-favorites'
			), array(
				1 => 'lang_id'
			))
		);

		$router->addRoute(
			'private-remove-from-favorites-def',
			new Zend_Controller_Router_Route_Regex('private/remove-from-favorites(.+)',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'remove-from-favorites'
			))
		);

		$router->addRoute(
			'private-add-to-favorites',
			new Zend_Controller_Router_Route_Regex('([a-z]{2})/private/add-to-favorites(.+)',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'add-to-favorites'
			), array(
				1 => 'lang_id'
			))
		);

		$router->addRoute(
			'private-add-to-favorites-def',
			new Zend_Controller_Router_Route_Regex('private/add-to-favorites(.+)',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'add-to-favorites'
			))
		);

		$router->addRoute(
			'private-activate',
			new Zend_Controller_Router_Route_Regex('([a-z]{2})/(.+)?/private/activate/(.+)?/([a-f0-9]{32})',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'activate'
			), array(
				1 => 'lang_id',
				2 => 'user_type',
				3 => 'email',
				4 => 'hash'
			))
		);

		$router->addRoute(
			'private-activate-def',
			new Zend_Controller_Router_Route_Regex('(.+)?/private/activate/(.+)?/([a-f0-9]{32})',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'activate'
			), array(
				1 => 'user_type',
				2 => 'email',
				3 => 'hash'
			))
		);
		
		$router->addRoute(
			'private-change-pass',
			new Zend_Controller_Router_Route_Regex('private/change-pass/([-_a-z0-9]+)/([a-f0-9]{32})',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'change-pass'
			), array(
				1 => 'username',
				2 => 'hash'
			))
		);

		$router->addRoute(
			'private-verified',
			new Zend_Controller_Router_Route(':lang_id/private/verify/:action',
			array(
				'module' => 'default',
				'controller' => 'verify',
				'action' => 'index'
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'private-verified-def',
			new Zend_Controller_Router_Route('private/verify/:action',
			array(
				'module' => 'default',
				'controller' => 'verify',
				'action' => 'index'
			))
		);

		$router->addRoute(
			'feedback',
			new Zend_Controller_Router_Route(':lang_id/feedback',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'feedback'
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'feedback-def',
			new Zend_Controller_Router_Route('feedback',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'feedback'
			))
		);

		$router->addRoute(
			'contact',
			new Zend_Controller_Router_Route(':lang_id/contact',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'contact'
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'contact-def',
			new Zend_Controller_Router_Route('contact',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'contact'
			))
		);


		/* --> Support <-- */
		$router->addRoute(
			'support',
			new Zend_Controller_Router_Route(':lang_id/support/:action',
			array(
				'module' => 'default',
				'controller' => 'support',
				'action' => 'index'
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'support-def',
			new Zend_Controller_Router_Route('support/:action',
			array(
				'module' => 'default',
				'controller' => 'support',
				'action' => 'index'
			))
		);
		/* <-- Support --> */




		$router->addRoute(
			'benchmark',
			new Zend_Controller_Router_Route('benchmark',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'benchmark'
			))
		);

		$router->addRoute(
			'comments',
			new Zend_Controller_Router_Route(':lang_id/comments/ajax-show',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'ajax-show'
			)), array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'comments-def',
			new Zend_Controller_Router_Route('comments/ajax-show',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'ajax-show'
			))
		);
		
		$router->addRoute(
			'getAlerts',
			new Zend_Controller_Router_Route(':lang_id/escorts/ajax-alerts',
			array(
				'module' => 'default',
				'controller' => 'escorts',
				'action' => 'ajax-alerts'
			)), array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'getAlerts-def',
			new Zend_Controller_Router_Route('escorts/ajax-alerts',
			array(
				'module' => 'default',
				'controller' => 'escorts',
				'action' => 'ajax-alerts'
			))
		);
		
		$router->addRoute(
			'alertsSave',
			new Zend_Controller_Router_Route(':lang_id/escorts/ajax-alerts-save',
			array(
				'module' => 'default',
				'controller' => 'escorts',
				'action' => 'ajax-alerts-save'
			)), array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'alertsSave-def',
			new Zend_Controller_Router_Route('escorts/ajax-alerts-save',
			array(
				'module' => 'default',
				'controller' => 'escorts',
				'action' => 'ajax-alerts-save'
			))
		);

		$router->addRoute(
			'add-comment',
			new Zend_Controller_Router_Route(':lang_id/comments/ajax-add-comment',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'ajax-add-comment'
			)), array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'add-comment-def',
			new Zend_Controller_Router_Route('comments/ajax-add-comment',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'ajax-add-comment'
			))
		);

		$router->addRoute(
			'vote-comment',
			new Zend_Controller_Router_Route(':lang_id/comments/ajax-vote',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'ajax-vote'
			)), array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'vote-comment-def',
			new Zend_Controller_Router_Route('comments/ajax-vote',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'ajax-vote'
			))
		);

		$router->addRoute(
			'reviews',
			new Zend_Controller_Router_Route(':lang_id/evaluations',
			array(
				'module' => 'default',
				'controller' => 'reviews',
				'action' => 'index'
			)), array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'reviews-def',
			new Zend_Controller_Router_Route('evaluations',
			array(
				'module' => 'default',
				'controller' => 'reviews',
				'action' => 'index'
			))
		);

		/*REDIRECT*/
		$router->addRoute(
			'reviews-escort-redirect2',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/evaluations/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'reviews-escort'
				),
				array(
					1 => 'lang_id',
					2 => 'showname'
				)
			)
		);

		$router->addRoute(
			'reviews-escort-redirect2-def',
			new Zend_Controller_Router_Route_Regex(
				'^evaluations/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'redirect',
					'action' => 'reviews-escort'
				),
				array(
					1 => 'showname'
				)
			)
		);
		//

		$router->addRoute(
			'reviews-escort',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/evaluations/(.+)?\-([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'escort'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'reviews-escort-def',
			new Zend_Controller_Router_Route_Regex(
				'^evaluations/(.+)?\-([0-9]+)',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'escort'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'reviews-member',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/evaluations/member/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'member'
				),
				array(
					1 => 'lang_id',
					2 => 'username'
				)
			)
		);

		$router->addRoute(
			'reviews-member-def',
			new Zend_Controller_Router_Route_Regex(
				'^evaluations/member/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'member'
				),
				array(
					1 => 'username'
				)
			)
		);

		$router->addRoute(
			'top-reviewers',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/evaluations/top-reviewers',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'top-reviewers'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'top-reviewers-def',
			new Zend_Controller_Router_Route_Regex(
				'^evaluations/top-reviewers',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'top-reviewers'
				),
				array(
					
				)
			)
		);

		$router->addRoute(
			'ajax-escorts-search',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/reviews/ajax-escorts-search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'ajax-escorts-search'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'ajax-escorts-search-def',
			new Zend_Controller_Router_Route_Regex(
				'^reviews/ajax-escorts-search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'ajax-escorts-search'
				),
				array(

				)
			)
		);

		$router->addRoute(
			'ajax-members-search',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/reviews/ajax-members-search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'ajax-members-search'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'ajax-members-search-def',
			new Zend_Controller_Router_Route_Regex(
				'^reviews/ajax-members-search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'ajax-members-search'
				),
				array(

				)
			)
		);

		$router->addRoute(
			'ajax-agencies-search',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/reviews/ajax-agencies-search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'ajax-agencies-search'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'ajax-agencies-search-def',
			new Zend_Controller_Router_Route_Regex(
				'^reviews/ajax-agencies-search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'ajax-agencies-search'
				),
				array(

				)
			)
		);

		$router->addRoute(
			'top-ladies',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/evaluations/top-ladies',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'top-ladies'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'top-ladies-def',
			new Zend_Controller_Router_Route_Regex(
				'^evaluations/top-ladies',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'top-ladies'
				),
				array(

				)
			)
		);

		$router->addRoute(
			'reviews-search',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/evaluations/search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'search'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'reviews-search-def',
			new Zend_Controller_Router_Route_Regex(
				'^evaluations/search',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'search'
				),
				array(

				)
			)
		);

		$router->addRoute(
			'reviews-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/evaluations/escorts',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'escorts'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'reviews-escorts-def',
			new Zend_Controller_Router_Route_Regex(
				'^evaluations/escorts',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'escorts'
				),
				array(

				)
			)
		);

		$router->addRoute(
			'reviews-agencies',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/evaluations/agencies',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'agencies'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'reviews-agencies-def',
			new Zend_Controller_Router_Route_Regex(
				'^evaluations/agencies',
				array(
					'module' => 'default',
					'controller' => 'reviews',
					'action' => 'agencies'
				),
				array(

				)
			)
		);

		$router->addRoute(
			'private-v2-reviews',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/private-v2/reviews',
				array(
					'module' => 'default',
					'controller' => 'private-v2',
					'action' => 'reviews'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'private-v2-reviews-def',
			new Zend_Controller_Router_Route_Regex(
				'^private-v2/reviews',
				array(
					'module' => 'default',
					'controller' => 'private-v2',
					'action' => 'reviews'
				),
				array(

				)
			)
		);

		$router->addRoute(
			'member-info',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?member/(.+)?',
				array(
					'module' => 'default',
					'controller' => 'members',
					'action' => 'index'
				), array(
					2 => 'lang_id',
					3 => 'username'

				)
			)
		);

		$router->addRoute(
			'member-info-actions-lng',
			new Zend_Controller_Router_Route(':lang_id/members/:action/*',
			array(
				'module' => 'default',
				'controller' => 'members',
				'action' => 'index',
				'lang_id' => ''
			),
			array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'member-info-actions',
			new Zend_Controller_Router_Route('members/:action/*',
			array(
				'module' => 'default',
				'controller' => 'members',
				'action' => 'index'
			))
		);

		$router->addRoute(
			'private-v2-alerts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/private-v2/alerts',
				array(
					'module' => 'default',
					'controller' => 'private-v2',
					'action' => 'alerts'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'private-v2-alerts-def',
			new Zend_Controller_Router_Route_Regex(
				'^private-v2/alerts',
				array(
					'module' => 'default',
					'controller' => 'private-v2',
					'action' => 'alerts'
				),
				array(

				)
			)
		);

		$router->addRoute(
			'newsletter-cron-def',
			new Zend_Controller_Router_Route_Regex(
				'^newsletter/email-sync-cron',
				array(
					'module' => 'default',
					'controller' => 'newsletter',
					'action' => 'email-sync-cron'
				),
				array(

				)
			)
		);

		

		

		/*$router->addRoute(
			'signInUp',
			new Zend_Controller_Router_Route(':lang_id/private/sign-in-up',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'sign-in-up'
			)), array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'signInUp-def',
			new Zend_Controller_Router_Route('private/sign-in-up',
			array(
				'module' => 'default',
				'controller' => 'private',
				'action' => 'sign-in-up'
			))
		);*/

		$router->addRoute(
			'api-main',
			new Zend_Controller_Router_Route('api/:controller/:action/*',
			array(
				'module' => 'api',
				'controller' => 'index',
				'action' => 'index'
			))
		);

		$router->addRoute(
			'index-controller',
			new Zend_Controller_Router_Route('/index/:action',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'index'
			))
		);

		$router->addRoute(
			'girl-of-month',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?girl-of-month(/history)?(/page_([0-9]+))?',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'gotm'
				), array(
					2 => 'lang_id',
					3 => 'history',
					5 => 'page'
				)
			)
		);



		$router->addRoute(
			'voting-widget',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escort/(.+?)\-([0-9]+)/vote',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'vote'
				),
				array(
					2 => 'lang_id',
					3 => 'showname',
					4 => 'escort_id'
				)
			)
		);


         /* update Grigor*/

        /* comment Subpage*/

        $router->addRoute(
			'subpagecomments',
			new Zend_Controller_Router_Route(':lang_id/comments',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'index'
			)), array(
				'lang_id' => '[a-z]{2}'
			)
		);

		$router->addRoute(
			'subpagecomments-def',
			new Zend_Controller_Router_Route('comments',
			array(
				'module' => 'default',
				'controller' => 'comments',
				'action' => 'index'
			))
		);

		$router->addRoute(
			'subpagecomments-escort',
			new Zend_Controller_Router_Route_Regex(
				':lang_id/escortcomments',
				array(
					'module' => 'default',
					'controller' => 'comments',
					'action' => 'get-escort-comments'
				),
				array(
					'lang_id' => '[a-z]{2}'
				)
			)
		);

		$router->addRoute(
			'subpagecomments-escort-def',
			new Zend_Controller_Router_Route_Regex(
				'escortcomments',
				array(
					'module' => 'default',
					'controller' => 'comments',
					'action' => 'get-escort-comments'
				)
			)
		);
        /* Comments subpage*/


        $router->addRoute(
			'bubbles',
			new Zend_Controller_Router_Route('ajax-bubble',
			array(
				'module' => 'default',
				'controller' => 'escorts',
				'action' => 'ajax-bubble'
			))
		);

        $router->addRoute(
			'online',
			new Zend_Controller_Router_Route('ajax-online',
			array(
				'module' => 'default',
				'controller' => 'escorts',
				'action' => 'ajax-online'
			))
		);

        $router->addRoute(
			'confirm-deletion-lang',
			new Zend_Controller_Router_Route_Regex('([a-z]{2})/private-v2/confirm-deletion/([a-f0-9]{10})',
			array(
				'module' => 'default',
				'controller' => 'private-v2',
				'action' => 'confirm-deletion'
			), array(
				1 => 'lang_id',
				2 => 'hash'
			))
		);

        $router->addRoute(
			'confirm-deletion',
			new Zend_Controller_Router_Route_Regex('private-v2/confirm-deletion/([a-f0-9]{10})',
			array(
				'module' => 'default',
				'controller' => 'private-v2',
				'action' => 'confirm-deletion'
			), array(
				1 => 'hash'
			))
		);

		$router->addRoute(
			'ajax-tell-friend',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/ajax-tell-friend',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-tell-friend'
				),
				array(
					2 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'ajax-report-problem',
			new Zend_Controller_Router_Route_Regex(
				'^(([a-z]{2})/)?escorts/ajax-report-problem',
				array(
					'module' => 'default',
					'controller' => 'escorts',
					'action' => 'ajax-report-problem'
				),
				array(
					2 => 'lang_id'
				)
			)
		);
		
		$router->addRoute(
			'get-filter',
			new Zend_Controller_Router_Route_Regex('(([a-z]{2})/)?escorts/get-filter',
			array(
				'module' => 'default',
				'controller' => 'escorts',
				'action' => 'get-filter'
			), array(
				2 => 'lang_id'
			))
		);
		
		// <editor-fold defaultstate="collapsed" desc="Gateway Callbacks">
		$router->addRoute('callbacks', new Zend_Controller_Router_Route(
			'callbacks/:action',
			array(
				'module' => 'default',
				'controller' => 'callbacks',
				'action' => 'index'
			)
		));
		// </editor-fold>


		// <editor-fold defaultstate="collapsed" desc="Mobile Version">
		$router->addRoute(
			'mobile',
			new Zend_Controller_Router_Route('mobile/:controller/:action/*',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'index'
			))
		);

        $router->addRoute(
			'def-mobile',
			new Zend_Controller_Router_Route(':lang_id/mobile/:controller/:action/*',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'index',
                'lang_id' => ''
				), array(
					'lang_id' => '[a-z]{2}'
				)
            )
		);
//
        $router->addRoute(
			'mobile-allcities',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/allcities',
				array(
					'module' => 'mobile',
					'controller' => 'index',
					'action' => 'index',
                    'all' => 1
				)
			)
		);
		
		$router->addRoute(
			'mobile-robots',
			new Zend_Controller_Router_Route('mobile/robots.txt',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'robots'
			))
		);

		$router->addRoute(
			'mobile-allcities-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/allcities',
				array(
					'module' => 'mobile',
					'controller' => 'index',
					'action' => 'index',
                    'all' => 1
				)
			)
		);


        $router->addRoute(
			'mobile-escorts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escorts(/.+)?',
				array(
					'module' => 'mobile',
					'controller' => 'escorts',
					'action' => 'index'
				),
				array(
					1 => 'lang_id',
					2 => 'req'
				)
			)
		);

		$router->addRoute(
			'mobile-escorts-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escorts(/.+)?',
				array(
					'module' => 'mobile',
					'controller' => 'escorts',
					'action' => 'index'
				),
				array(
					1 => 'req'
				)
			)
		);


        $router->addRoute(
			'mobile-escort-favorites',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/favorites',
				array(
					'module' => 'mobile',
					'controller' => 'index',
					'action' => 'favorites'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-favorites-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/favorites',
				array(
					'module' => 'mobile',
					'controller' => 'index',
					'action' => 'favorites'
				)
			)
		);


        $router->addRoute(
			'mobile-escort-search',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/search',
				array(
					'module' => 'mobile',
					'controller' => 'index',
					'action' => 'search'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-search-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/search',
				array(
					'module' => 'mobile',
					'controller' => 'index',
					'action' => 'search'
				)
			)
		);


        $router->addRoute(
			'mobile-escort-profile',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'escort'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-profile-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'escort'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

        $router->addRoute(
			'mobile-escort-services',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/services/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'services'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-services-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/services/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'services'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

        $router->addRoute(
			'mobile-escort-rates',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/rates/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'rates'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-rates-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/rates/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'rates'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

        $router->addRoute(
			'mobile-escort-contacts',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/contacts/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'contacts'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-contacts-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/contacts/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'contacts'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);


        $router->addRoute(
			'mobile-escort-tours',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/tours/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'tours'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-tours-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/tours/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'tours'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

        $router->addRoute(
			'mobile-static-pages',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/page/(.+)?',
				array(
					'module' => 'mobile',
					'controller' => 'page',
					'action' => 'show'
				),
				array(
					1 => 'lang_id',
					2 => 'page_slug'
				)
			)
		);

		$router->addRoute(
			'mobile-static-pages-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/page/(.+)?',
				array(
					'module' => 'mobile',
					'controller' => 'page',
					'action' => 'show'
				),
				array(
					1 => 'page_slug'
				)
			)
		);

        $router->addRoute(
			'mobile-escort-photo',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/photo/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'photo'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-photo-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/photo/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'photo'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);

		/* mobile-photos */
		$router->addRoute(
			'mobile-escort-photos',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/escort/photos/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'profile-photos'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-photos-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/escort/photos/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'profile-photos'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);
		/* escort photos end */


		
        $router->addRoute(
			'mobile-add-to-favorites',
			new Zend_Controller_Router_Route_Regex('([a-z]{2})/mobile/add-to-favorites(.+)?',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'add-to-favorites'
			), array(
				1 => 'lang_id'
			))
		);

		$router->addRoute(
			'mobile-add-to-favorites-def',
			new Zend_Controller_Router_Route_Regex('mobile/add-to-favorites(.+)?',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'add-to-favorites'
			))
		);

        $router->addRoute(
			'mobile-remove-from-favorites',
			new Zend_Controller_Router_Route_Regex('([a-z]{2})/mobile/remove-from-favorites(.+)?',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'remove-from-favorites'
			), array(
				1 => 'lang_id'
			))
		);

		$router->addRoute(
			'mobile-remove-from-favorites-def',
			new Zend_Controller_Router_Route_Regex('mobile/remove-from-favorites(.+)?',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'remove-from-favorites'
			))
		);

        $router->addRoute(
			'mobile-feedback',
			new Zend_Controller_Router_Route(':lang_id/mobile/feedback',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'feedback'
			), array(
				'lang_id' => '[a-z]{2}'
			))
		);

		$router->addRoute(
			'mobile-feedback-def',
			new Zend_Controller_Router_Route('mobile/feedback',
			array(
				'module' => 'mobile',
				'controller' => 'index',
				'action' => 'feedback'
			))
		);


		
		$router->addRoute(
			'mobile-escort-evaluations',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/evaluations/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'reviews'
				),
				array(
					1 => 'lang_id',
					2 => 'escortName',
					3 => 'escort_id'
				)
			)
		);

		$router->addRoute(
			'mobile-escort-evaluations-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/evaluations/(.+)?\-([0-9]+)',
				array(
					'module' => 'mobile',
					'controller' => 'profile',
					'action' => 'reviews'
				),
				array(
					1 => 'escortName',
					2 => 'escort_id'
				)
			)
		);


		$router->addRoute(
			'mobile-evaluations',
			new Zend_Controller_Router_Route_Regex(
				'^([a-z]{2})/mobile/evaluations',
				array(
					'module' => 'mobile',
					'controller' => 'escorts',
					'action' => 'reviews'
				),
				array(
					1 => 'lang_id'
				)
			)
		);

		$router->addRoute(
			'mobile-evaluations-def',
			new Zend_Controller_Router_Route_Regex(
				'^mobile/evaluations',
				array(
					'module' => 'mobile',
					'controller' => 'escorts',
					'action' => 'reviews'
				),
				array(
				)
			)
		);


		// </editor-fold>
	}

	protected function _initAutoload()
	{
		$moduleLoader = new Zend_Application_Module_Autoloader(array(
			'namespace' => '',
			'basePath' => APPLICATION_PATH
		));

		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('Cubix_');


		return $moduleLoader;
	}

	protected function _initDatabase()
	{
		$this->bootstrap('db');
		$db = $this->getResource('db');
		$db->setFetchMode(Zend_Db::FETCH_OBJ);

		Zend_Registry::set('db', $db);
		Cubix_Model::setAdapter($db);

		$db->query('SET NAMES `utf8`');
	}

	protected function _initConfig()
	{
		Zend_Registry::set('images_config', $this->getOption('images'));
		Zend_Registry::set('escorts_config', $this->getOption('escorts'));
        Zend_Registry::set('mobile_config', $this->getOption('mobile'));
		Zend_Registry::set('reviews_config', $this->getOption('reviews'));
		Zend_Registry::set('faq_config', $this->getOption('faq'));
		Zend_Registry::set('feedback_config', $this->getOption('feedback'));
		Zend_Registry::set('system_config', $this->getOption('system'));
		Zend_Registry::set('gateway_config', $this->getOption('gateway'));
		Zend_Registry::set('statistics', $this->getOption('statistics'));
	}

	protected function _initViewHelpers()
	{
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();

		$view->doctype('XHTML1_STRICT');

		$view->headMeta()
			->appendHttpEquiv('Content-Type', 'text/html; charset=utf-8')
			->appendHttpEquiv('Cache-Control', 'no-cache');
		$view->headTitle()->setSeparator(' - ');
		//$view->headTitle('EscortForum Backend');
		
	}

	protected function _initDefines()
	{

	}

	protected function _initCache()
	{
		$frontendOptions = array(
			'lifetime' => null,
			'automatic_serialization' => true
		);

		$backendOptions = array(
			'servers' => array(
				array(
					'host' => '172.16.0.11',
					'port' => '11211',
					'persistent' =>  true
				)
			),
		);

		if ( defined('IS_DEBUG') && IS_DEBUG ) {
			$cache = Zend_Cache::factory('Core', 'Blackhole', $frontendOptions, array());
		}
		else {
			$cache = Zend_Cache::factory(new Cubix_Cache_Core($frontendOptions), new Cubix_Cache_Backend_Memcached($backendOptions), $frontendOptions);
		}

		Zend_Registry::set('cache', $cache);
	}

	protected function _initBanners()
	{
		if ( defined('IS_CLI') && IS_CLI ) return;

		$banners = array();

		/*$cache = Zend_Registry::get('cache');

		$config = Zend_Registry::get('system_config');

		$banners['banners']['top_banners'] = array();
		$banners['banners']['right_banners'] = array();
		$banners['banners']['right_banners_10'] = array();

		if ( ! $banners = $cache->load('v2_banners_cache_sex') ) {
			for ($i = 0; $i < 4; $i++) {
				$banners['banners']['top_banners'][] = Cubix_Banners::GetBanner(11);
			}

			for ($i = 0; $i < 10; $i++) {
				$banners['banners']['right_banners'][] = Cubix_Banners::GetBanner(11);
			}		

			
			$banners['banners']['right_banners_120'][] = Cubix_Banners::GetBanner(10);
			
			$banners['banners']['right_banners_250'][] = Cubix_Banners::GetBanner(12);
			
			
	
			$cache->save($banners, 'v2_banners_cache_sex', array(), $config['bannersCacheLifeTime']);
		}*/

		Zend_Registry::set('banners', $banners);
	}

	protected function _initApi()
	{
		$api_config = $this->getOption('api');
		Zend_Registry::set('api_config', $api_config);
		Cubix_Api_XmlRpc_Client::setApiKey($api_config['key']);
		Cubix_Api_XmlRpc_Client::setServer($api_config['server']);
	}



}
