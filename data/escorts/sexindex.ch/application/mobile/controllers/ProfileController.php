<?php

class Mobile_ProfileController extends Zend_Controller_Action
{
	public function init()
	{
		//die('Mobile version is temporary unavailable');

        $menu = array(
            'bio' => array(
                'name' => 'bio',
                'class' => 'w15',
            ),
            'services' => array(
                'name' => 'services',
                'class' => 'w23',
            ),
            'rates' => array(
                'name' => 'rates',
                'class' => 'w18',
            ),
            'contacts' => array(
                'name' => 'contact',
                'class' => 'w24',
            ),
            'photos' => array(
                'name' => 'photos',
                'class' => 'w20',
            ));


        $cook_name = "favorites";
        $data = @unserialize($_COOKIE[$cook_name]);
        
		$this->_helper->layout->setLayout('mobile');
        $showname = $this->_getParam('escortName');
        $escort_id = $this->_getParam('escort_id');
        $model = new Model_EscortsV2();
		//$escort = $model->get($showname);
		$escort = $model->get($escort_id);

		$add_esc_data = $model->getRevComById($escort->id);

		$escort->disabled_reviews = $add_esc_data->disabled_reviews;
		$escort->disabled_comments = $add_esc_data->disabled_comments;

        if(!$escort->rates){
            unset($menu['rates']);
        }

        if( (!$escort->services) || (count($escort->services) == 1 && $escort->services[0]['service_id'] == 0)){
            unset($menu['services']);
        }

        if(!$escort->tours){
            unset($menu['tours']);
        }


        $isFavorite = false;

		if ( ! $escort || isset($escort['error']) )
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}

        if(is_array($data) && in_array($escort->id,$data)){
            $isFavorite = true;
        }
        

        $this->view->backUrl =  @$_SESSION['request_url'];

        $this->view->isFavorite = $isFavorite;
        $this->view->showname = $showname;
        $this->photosAction($escort);   
        $this->escort = $escort;
        $this->view->escort = $escort;
        $this->view->menu = $menu;
		$this->model = $model;
	}

    public function indexAction()
	{    
        
	}

    public function photosAction($escort = null)
	{
		$config = Zend_Registry::get('mobile_config');
		$escort_id = $this->_getParam('escort_id');
		$mode = $this->_getParam('mode');
		if( $mode == 'ajax' ) {
			$this->view->layout()->disableLayout();
			$model = new Model_EscortsV2();
			$escort = $model->get($escort_id);
			$this->view->escort_id = $escort_id;
		}
        $page = intval($this->_getParam('page'));

		if ( ! $page )
			$page = 1;
		$count = 0;    
		$photos = $escort->getPhotos($page, $count, true,false,$config);
      
        $this->view->photoCount = $count;
        $this->view->page = $page;
		$this->view->photos = $photos;       
	}

    public function photoAction(){
        $photo_id = intval($this->_getParam('photo_id'));
        $this->view->photo = $this->escort->getPhoto($photo_id);
    }



    public function profilePhotosAction(){
        $page = 1;
        if($this->_getParam('page')){
            $page = $this->_getParam('page');
        }

		$this->view->currentMenu = 'photos';
        $this->view->photos = $this->escort->getPhotos($page, $count, true,false,null,5);
		$this->view->count = $count;
		$this->view->page = $page;
    }


	public function reviewsAction(){
		$lng = Cubix_I18n::getLang();

		$request = $this->_request;
		$escortName = $request->escortName;

		if ( $request->ajax ) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}

		$config = Zend_Registry::get('reviews_config');

		$model = new Model_EscortsV2();
		$escort = $model->get($escortName);


		if (isset($request->page) && intval($request->page) > 0)
			$page = intval($request->page);
		else
			$page = 1;

		$perpage = 1;

		if (isset($request->perpage))
		{
			if ($request->perpage == 'all')
				$perpage = 99999;
			elseif (intval($request->perpage) > 0)
				$perpage = intval($request->perpage);
		}

		$cache = Zend_Registry::get('cache');
		$cache_key = 'v2_reviews_escort_' . $lng . '_page_' . $page . '_perpage_' . $perpage . '_escortName_' . $escortName;
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		if ( ! $store = $cache->load($cache_key) )
		{
			//$topLadies = Cubix_Api::getInstance()->call('getTopLadies', array($config['topLadiesCount'], $lng));
			$topLadies = Model_Reviews::getTopLadies($config['topLadiesCount']);
			//$ret = Cubix_Api::getInstance()->call('getEscortReviews', array($escortName, $page, $perpage, $lng));
			$ret = Model_Reviews::getEscortReviews($escortName, $page, $perpage);

			$store = array($ret, $topLadies);

			$cache->save($store, $cache_key, array(), $config['cacheTime']);
		}

		list($ret, $topLadies) = $store;
		list($items, $count) = $ret;

		$topEsc = array();
		$topEscK = array();
		foreach ($topLadies as $k => $escort2)
		{
			$topEsc[$k] = $escort2['escort_id'];
			$topEscK[$escort2['escort_id']] = $k;
		}

		$escort_id = $items[0]['escort_id'];

		if (in_array($escort_id, $topEsc))
			$this->view->inTop30 = $topEscK[$escort_id] + 1;

		$this->view->items = $items;
		$this->view->count = $count;
		$this->view->page = $page;
		if ($perpage == 99999) $perpage = 'all';
		$this->view->perpage = $perpage;
	}

    public function escortAction(){
		
		/************** Bio **************/

        $this->view->about = $this->escort->getAbout();        
        $this->view->currentMenu ='bio';

		$comment_model = new Model_Comments();
		$comments = $comment_model->getEscortComments(1, 10, $comment_count, $this->escort->id);

		if ( count($comments) > 0 ){
			 foreach ($comments as &$comment ){
				 
				if ( strlen($comment->message) > 190 ){
					$comment->message1 = mb_substr($comment->message,0,190);
					$comment->message2 = mb_substr($comment->message,190,(strlen($comment->message)-190) );
				}else{
					$comment->message1 = mb_substr($comment->message,0,190);
					$comment->message2 = false;
				}
			 }
		}

		$this->view->comments = $comments;

		/* Last Review */
		$cache = Zend_Registry::get('cache');
		$cache_key = 'm_reviews_' . $this->escort->id;
		if ( ! $res = $cache->load($cache_key) ) {
			try {
				$res = $this->model->getEscortLastReview($this->escort->id);
			}
			catch ( Exception $e ) {
				$res = array(false, false);
			}

			$cache->save($res, $cache_key, array());
		}

		list($review, $rev_count) = $res;
		
		$this->view->review = $review;
		$this->view->rev_count = $rev_count;
		/* Last Review */

		/************ Tours/Contacts ************/

		if($this->_getParam('success')){
            $this->view->message = "Your feedback has been successfully submitted";
        }

		$contacts = $this->escort->getContacts();

        $additional_cities = $this->escort->getAdditionalCities($contacts->base_city_id);
        $zones = $this->escort->getCityzones();
        $work_times = $this->escort->getWorkTimes();

        $this->view->contacts = $this->escort->getContacts();
        $this->view->work_times = $work_times;
        $this->view->zones = $zones;
        $this->view->additional_cities = $additional_cities;

		$_tours = $this->escort->getCitytours();

		$tours = array('current' => array(), 'upcoming' => array());
        if(count($_tours) > 0 && is_array($_tours)){
           foreach ( $_tours as $tour ) {
                if ( $tour->is_in_tour ) {
                    $tours['current'][] = $tour;
                    $seo_params['city'] = $tour->city_title;
                    $site_path_city = $tour->city_title;
                }
                else {
                    $tours['upcoming'][] = $tour;
                }
            }
        }
        $this->view->tours = $tours;

		/*************** Rates ***************/

		$this->view->rates = $this->escort->getHHRates();

		/*************** Services ***************/

		$this->view->services = $this->escort->getServices();

    }

    public function contactsAction(){
		$link = $this->view->getLink('profile-hash', array('showname' => $this->view->showname, 'type' => 'contacts' ) );
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: '.$link);
		die;
		return;
    }
    public function ratesAction(){
        $link = $this->view->getLink('profile-hash', array('showname' => $this->view->showname, 'type' => 'rates' ) );
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: '.$link);
		die;
		return;
    }
    public function toursAction(){
        $link = $this->view->getLink('profile-hash', array('showname' => $this->view->showname, 'type' => 'contacts' ) );
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: '.$link);
		die;
		return;
    }

    public function servicesAction(){
        $link = $this->view->getLink('profile-hash', array('showname' => $this->view->showname, 'type' => 'services' ) );
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: '.$link);
		die;
		return;
    }
	
    public function profileAction(){
       
    }

    public function addToFavoritesAction(){
		$req = $this->_request;
        $escort_id = intval($req->escort_id);
        $cook_name = "favorites";
        $cook_value = array($escort_id);
        if ( ! isset( $_COOKIE[$cook_name]) ) {
            setcookie($cook_name, serialize($cook_value), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
        }
        else {
            $cook_data = unserialize($_COOKIE[$cook_name]);
            if ( is_array($cook_data) ) {
                if ( in_array($cook_value[0], $cook_data) ) {
                    $this->view->alredy_in_favorites = true;
                }
                else {
                    $new_data = array_merge($cook_data, $cook_value);
                    setcookie($cook_name, serialize($new_data), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
                }
            }
        }
    }
}
