$( document ).ready(function() {

    $(".dropdown-selection.single-option").find(".dropdown-menu > li > a").on("click",function(){
        var val = $(this).html();
        $(this).parents(".dropdown-selection").find(".single-option-select").val(val);
    });

    $(".dropdown-toggle").each(function(){
        removeDropdownSelected(this);
    });

    $(".dropdown-menu li a").on("click", function(){
        var val = $(this).html();
        var type = $(this).parents(".dropdown-menu").attr("data-type");
        $("#" + type).find(".dropdown-toggle-val").html(val);
        removeDropdownSelected(this);
    });

    $(".button-tap").on("click",function(){
        $(this).toggleClass("tapped");
    });

    $(".show-form").on("click",function(){
        $(this).stop(true, true).fadeToggle("fast", function(){
            $(".form-container").stop(true, true).slideToggle();
        });
    });

    $(".hide-form").on("click",function(){
        $(".form-container").stop(true, true).slideToggle(function(){
            $(".show-form").stop(true, true).fadeToggle("fast");
        });
    });

    $(".add-to-blacklist").on("click",function(){
        var country = $("#countryFilter").find(".dropdown-toggle-val").html();
        var city = $("#cityFilter").find(".dropdown-toggle-val").html();
        var date = $("#dateFilter").find(".dropdown-toggle-val").html();
        var phone = $("#phoneNumber").val();
        var clientName = $("#clientName").val();
        var description = $("#descriptionInput").val();

        if ((country != "Nothing selected") && (city != "Nothing selected") && (date != "Nothing selected") && (phone != "") && (clientName != "") && (description != "")) {

            var container = `<div class="black-list-item">
                                <div class="d-flex">
                                    <p class="item-name">`+ clientName +`</p>
                                    <p class="text-grey ml-10">`+ date +`</p>
                                </div>
                                <p class="text-grey">`+ country +`, `+ city +`  /  `+ phone +`</p>
                                <div class="clear mb-10"></div>
                                <p>`+ description +`</p>
                            </div>`;

            $(".black-list-container").append(container);
            $("#countryFilter").find(".dropdown-toggle-val").html("Nothing selected");
            $("#cityFilter").find(".dropdown-toggle-val").html("Nothing selected");
            $("#dateFilter").find(".dropdown-toggle-val").html("Nothing selected");
            $("#phoneNumber").val("");
            $("#clientName").val("");
            $("#descriptionInput").val("");
        }
    });

    if ($("#submenu1").parent("li").hasClass("active")) {
        $("#submenu1").addClass("show");
    }

    if ($("#submenu2").parent("li").hasClass("active")) {
        $("#submenu2").addClass("show");
    }

    if ($("#submenu3").parent("li").hasClass("active")) {
        $("#submenu3").addClass("show");
    }

    if (ismobile()) {
        
        $("#submenu-trigger").on("click",function(e){
            e.preventDefault();
            e.stopPropagation();
            $(".user-area").toggleClass("mobile-opened");
            $(".header").toggleClass("fixed");
        });

        $('.left-section').click( function(e) {
            e.stopPropagation();
        });

        $("body").on("click",function(){
            $(".user-area").removeClass("mobile-opened");
            $(".header").removeClass("fixed");
        });
    }
});

function ismobile(){
     var newWindowWidth = $(window).width();

        return (newWindowWidth < 768);
}

function removeDropdownSelected(obj){
    var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
    $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
        $(this).show();
    });
    $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
        if ($(this).find("a").html() == val) {
            $(this).hide();
        }
    });
}
    