$( document ).ready(function() {

    $("#submenu-trigger").on("click",function(e){
        e.stopPropagation();
        $(".header-submenu-container").stop(true, true).slideToggle();
    });

     $('html').on("click",function(){
        if ($(".header-submenu-container").css("display") == "block") {
            $("#submenu-trigger").trigger("click");
        }
     });

    $(".escort-details").not(".uncut").each(function(){
        
        text = $(this).find("p").html();
        link = $(this).parents(".escort-item").find("a").attr("href");
        len = 284;

        if ($(this).parents(".agencies-list")) {
            len = 199;
        } else {
            len = 284;
        }

        if((text.length > text.substring(0, len).length)){
            text = text.substring(0, len - 3) + '...' + '<a href="' + link + '">Read more</a>';
        }
        
        // return text; 
        $(this).find("p").html(text);
    });


        // $(".escorts-slider").each(function(){
        //     var x = $(this).find(".image");
        //     for (i = 7; i < x.length; i++) {
        //        x[i].style.display = "none";  
        //     }
        // });

    if (!ismobile()) {

        $(".image-slider").each(function(){
            var x = $(this).find(".image");
            $(this).find(".image-bullets").append("<span class='bullet active'></span>");
            for (i = 1; i < x.length; i++) {
               x[i].style.display = "none";  
               $(this).find(".image-bullets").append("<span class='bullet'></span>");
            }
        });
        var timer;

        $(".image-slider").not(".single").hover(function(){
            var myIndex = 0;
            var x = $(this).find(".image");
            var dots = $(this).find(".bullet");
            timer = setInterval(function(){
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none"; 
                    dots[i].className = dots[i].className.replace(" active", ""); 
                }
                myIndex++;
                if (myIndex > x.length) {myIndex = 1}    
                x[myIndex-1].style.display = "block";
                dots[myIndex-1].className += " active";
           },500);
        },function() {
            clearInterval(timer);
        });

        if ($(".escorts-slider").find(".image").length > 8) {
            for (var i = 8; i <= $(".escorts-slider").find(".image").length; i++) {
                $(".escorts-slider").find(".image:nth-child("+ i +")").css("display","none");
            }
            $(".escorts-slider").find(".image:nth-child("+ 7 +")").addClass("last").append("<span>+" + ($(".escorts-slider").find(".image").length - 8) + "</span>");
        }

    } else {

        $(".image-slider").each(function(){
            var x = $(this).find(".image");
            for (i = 1; i < x.length; i++) {
               x[i].style.display = "none";  
            }
        });

        if ($(".escorts-slider").find(".image").length > 4) {
            for (var i = 5; i <= $(".escorts-slider").find(".image").length; i++) {
                $(".escorts-slider").find(".image:nth-child("+ i +")").css("display","none");
            }
            $(".escorts-slider").find(".image:nth-child("+ 4 +")").addClass("last").append("<span>+" + ($(".escorts-slider").find(".image").length - 5) + "</span>");
        }
    }

    function removeDropdownSelected(obj){
    	var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
    	$(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
    		$(this).show();
    	});
    	$(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
    		if ($(this).find("a").html() == val) {
    			$(this).hide();
    		}
    	});
    }

    $(".dropdown-menu[data-type='lookingFor']").find("a").on("click",function(){
        var val = $(this).html();
        if (val == "Agencies") {
            $(".content").addClass("agencies");
            $(".filters").slideUp();
            $(".escorts-container").hide();
            $(".agencies-container").show();
        } else {
            $(".content").removeClass("agencies");
            $(".filters").slideDown();
            $(".agencies-container").hide();
            $(".escorts-container").show();
        }
    });

    $(".lookingFor-mobile").find(".btn-dropdown-rounded").on("click",function(){
        var val = $(this).html();
        if (val == "Agencies") {
            $(".content").addClass("agencies");
            $(".escorts-container").hide();
            $(".agencies-container").show();
        } else {
            $(".content").removeClass("agencies");
            $(".agencies-container").hide();
            $(".escorts-container").show();
        }
    });

    $(".dropdown-selection.single-option").find(".dropdown-menu > li > a").on("click",function(){
        var val = $(this).html();
        $(this).parents(".dropdown-selection").find(".single-option-select").val(val);
    });

    $(".dropdown-toggle").each(function(){
        removeDropdownSelected(this);
    });

    $(".dropdown-menu li a").on("click", function(){
        var val = $(this).html();
        var type = $(this).parents(".dropdown-menu").attr("data-type");
        $("#" + type).find(".dropdown-toggle-val").html(val);
        removeDropdownSelected(this);
    });

    var labels = [];
    $('.multiple-dropdown-box').multiselect({ 
        
        onChange: function(option, checked, select){
       		var index = labels.indexOf($(option).val());
       		if (index !== -1) {
       			labels.splice(index, 1);
       		} else {
                labels.push($(option).val());
       		}
        },
        buttonText: function(obj) {
            if (labels.length === 0) {
                return 'Nothing selected';
            }
            else if (labels.length >= 2) {
                return labels[0]+ ', +' + (labels.length - 1);
            }
            else if(labels.length >= 1){
            	return labels;
            }
        }
    });

    $(".advanced-search").on("click",function(){
    	$(this).html() == "Advanced search" ? $(this).html('Basic search') : $(this).html('Advanced search');

        if (ismobile()) {
            $(".advanced-buttons").stop(true, true).toggleClass("active");
        }

    	$(".advanced-search-container").stop(true, true).slideToggle();
    });

    $("#cancel").on("click",function(){
    	$(".advanced-search").trigger("click");
    });

    $("#reset, .reset").on("click",function(){
    	var labels = [];
    	$(".advanced-search-container-top").find("input[type='text']").val("");
    });

    if (ismobile()) {
        autocomplete(document.getElementById("countryInput-mobile"), countries);    
    } else {
        autocomplete(document.getElementById("countryInput"), countries);
    }

	

    $(".view-buttons").on("click",function(){
        $(".view-buttons").removeClass("active");
        $(this).addClass("active");

        if ($(this).attr("data-type") == "view-squares") {
            $(".escorts-list").removeClass("view-list").addClass("d-flex view-squares");
            $(".escort-item").removeClass("d-flex");
        } else {
            $(".escorts-list").removeClass("d-flex view-squares").addClass("view-list");
            $(".escort-item").addClass("d-flex");
        }
    });

    $(".intentions-container-mobile").on("click",function(){
        $(".filters-mobile").stop(true, true).fadeToggle();
    });

    $("#cancel-mobile").on("click",function(){
        $(".intentions-container-mobile").trigger("click");
    });

    $(".btn-dropdown-rounded").on("click",function(){
        $(this).parent(".d-flex").find(".btn-dropdown-rounded").removeClass("active");
        $(this).addClass("active");
    });

    $(".search-mobile").on("click",function(){
        var country = $("#countryInput-mobile").val();
        if (country !== "") {
            $("#myCountry-mobile-place").html(country);
        }

        if ($(".lookingFor-mobile").find(".btn-dropdown-rounded").hasClass("active")) {
            $(".lookingFor-mobile-place").html($(".lookingFor-mobile").find(".btn-dropdown-rounded.active").html());
        }

        if ($(".interestedIn-mobile").find(".btn-dropdown-rounded").hasClass("active")) {
            $(".interestedIn-mobile-place").html($(".interestedIn-mobile").find(".btn-dropdown-rounded.active").html());
        }

        $(".intentions-container-mobile").trigger("click");
    });

});

function ismobile(){
     var newWindowWidth = $(window).width();

        return (newWindowWidth < 768);
}
    