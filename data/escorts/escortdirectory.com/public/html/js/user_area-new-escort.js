$( document ).ready(function() {

    $(".dropdown-selection.single-option").find(".dropdown-menu > li > a").on("click",function(){
        var val = $(this).html();
        $(this).parents(".dropdown-selection").find(".single-option-select").val(val);
    });

    $(".dropdown-toggle").each(function(){
        removeDropdownSelected(this);
    });

    $(".dropdown-menu li a").on("click", function(){
        var val = $(this).html();
        var type = $(this).parents(".dropdown-menu").attr("data-type");
        $("#" + type).find(".dropdown-toggle-val").html(val);
        removeDropdownSelected(this);
    });

    $(".button-tap").on("click",function(){
        $(this).toggleClass("tapped");
    });

    $(".add-language-trigger").on("click",function(){
        var lang = $("#languageFilter").find(".dropdown-toggle-val").text();
        var quality = $("#proficiencyFilter").find(".dropdown-toggle-val").text();

        if ((lang != "Nothing selected") && (quality != "Nothing selected")) {

            var container = `<div class="my-language justify-content-between">
                                <div class="language-name">`+ lang +` <span class="text-grey">(`+ quality +`)</span></div>
                                <div class="language-remove text-grey d-flex align-items-center">
                                    <p class="uppercase">remove</p>
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>`;

            if ($(".my-language").length == 0) {
                $(".languages-container-box").slideDown();
            }

            $(".my-languages-container").append(container);

            $(".language-remove").on("click",function(){
                var obj = $(this);

                if ($(".my-language").length-1 == 0) {
                    $(".languages-container-box").slideUp('slow',function(){
                        obj.parents(".my-language").remove();
                    });
                } else {
                    obj.parents(".my-language").slideUp('slow',function(){
                        obj.parents(".my-language").remove();
                    });
                }
            });
        }
    });

    $(".language-remove").on("click",function(){
        $(this).parents(".my-language").slideUp('slow',function(){
            $(this).remove();
        });
    });

    $(".city-remove").on("click",function(){
        $(this).parents(".other-city").slideUp('slow',function(){
            $(this).remove();
        });
    });

    $(".add-other-city-trigger").on("click",function(){
        var country = $("#countryFilter").find(".dropdown-toggle-val").html();
        var otherCity = $("#otherCitiesFilter").find(".dropdown-toggle-val").html();

        if ((country != "Nothing selected") && (otherCity != "Nothing selected")) {

            var container = `<div class="other-city justify-content-between">
                                <div class="city-name">`+ otherCity +` <span class="text-grey">(`+ country +`)</span></div>
                                <div class="city-remove text-grey d-flex align-items-center">
                                    <p class="uppercase">remove</p>
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>`;

            $(".other-cities-container").append(container);

            $(".city-remove").on("click",function(){
                $(this).parents(".other-city").slideUp('slow',function(){
                    $(this).remove();
                });
            });
        }
    });

    if ($("#submenu2").parent("li").hasClass("active")) {
        $("#submenu2").addClass("show");
    }

    $(".gallery-image:not(.existance)").on("click",function(){
        $(this).siblings("input").click();
    });

    $("input[type|='file']").on("change",function(e){
        e.preventDefault();
        var fileVal = $(this).val().split('\\').pop();
        if (fileVal != '') {
            $(this).siblings(".gallery-image").addClass("existance");
            $(this).siblings(".gallery-image").append(`
                <div class="image" style="background-image: url('pics/` + fileVal + `');"></div>
                <div class="remove-image"></div>
            `);

            $(".remove-image").on("click",function(){
                $(this).closest(".gallery-image").removeClass("existance").find(".image,.remove-image").remove();
                return false;
            });
        }
    });

    var progressBarValue = 10;

    $(".next-step").on("click",function(){
        var val = $(this).parents("section").attr("id").split('step').pop();
        var val = parseInt(val);
        var next = val + 1;
        if (stepIsValid(val)) {
            $("#step"+val).fadeOut(100,function(){
                $("#step"+next).fadeIn(150);
            });
            progressBarValue = progressBarValue + 15;
            $(".progress-bar").css("width",progressBarValue + "%");
        }
    });

    $(".previous-step").on("click",function(){
        var val = $(this).parents("section").attr("id").split('step').pop();
        var val = parseInt(val);
        var previous = val - 1;
        $("#step"+val).fadeOut(100,function(){
            $("#step"+previous).fadeIn(150);
        });
        progressBarValue = progressBarValue - 15;
        $(".progress-bar").css("width",progressBarValue + "%");
    });



    if (ismobile()) {
        
        $("#submenu-trigger").on("click",function(e){
            e.preventDefault();
            e.stopPropagation();
            $(".user-area").toggleClass("mobile-opened");
            $(".header").toggleClass("fixed");
        });

        $('.left-section').click( function(e) {
            e.stopPropagation();
        });

        $("body").on("click",function(){
            $(".user-area").removeClass("mobile-opened");
            $(".header").removeClass("fixed");
        });
    }
});

function ismobile(){
     var newWindowWidth = $(window).width();

        return (newWindowWidth < 768);
}

function removeDropdownSelected(obj){
    var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
    $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
        $(this).show();
    });
    $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
        if ($(this).find("a").html() == val) {
            $(this).hide();
        }
    });
}

function stepIsValid(step){

    var errors=[];
    $(".errors-container > .box-section").empty();

    switch(step) {

      case 0:
            $(".errors-container").css("display","none");
            return true;
        break;
      case 1:
            if ($("#nameInput").val() == "") {
                errors.push("Name is mandatory");
            }
            if ($("#countryFilter").find(".dropdown-toggle-val").text() == "Nothing selected") {
                errors.push("Country is mandatory");
            }
            if ($("#baseCityFilter").find(".dropdown-toggle-val").text() == "Nothing selected") {
                errors.push("City is mandatory");
            }
            if ($("#categoryFilter").find(".dropdown-toggle-val").text() == "Nothing selected") {
                errors.push("Category is mandatory");
            }
            if ($("#genderFilter").find(".dropdown-toggle-val").text() == "Nothing selected") {
                errors.push("Gender is mandatory");
            }
            if ($("#hairColorFilter").find(".dropdown-toggle-val").text() == "Nothing selected") {
                errors.push("Hair color is mandatory");
            }
            if ($("#eyeColorFilter").find(".dropdown-toggle-val").text() == "Nothing selected") {
                errors.push("Eye color is mandatory");
            }

            if (errors.length == 0) {
                $(".errors-container").css("display","none");
                return true;
            } else {
                $(".errors-container").fadeIn();
                for (i=0; i < errors.length; i++) {
                    $(".errors-container > .box-section").append('<p>'+ (i+1) + '. ' + errors[i] + '</p>')
                }
            }
            console.log(errors);
        break;
      case 2:
            if ($("#descriptionInput").val() == "") {
                errors.push("Description is mandatory");
            }
            if ($(".my-languages-container").find(".my-language").length == 0) {
                errors.push("Minimum a language is mandatory");
            }

            if (errors.length == 0) {
                $(".errors-container").css("display","none");
                return true;
            } else {
                $(".errors-container").fadeIn();
                for (i=0; i < errors.length; i++) {
                    $(".errors-container > .box-section").append('<p>'+ (i+1) + '. ' + errors[i] + '</p>')
                }
            }
            console.log(errors);
        break;
      case 3:
            if (($("#incall_check").is(":checked") == false) && ($("#outcall_check").is(":checked") == false)) {
                errors.push("Check one option is mandatory");
            }

            if (errors.length == 0) {
                $(".errors-container").css("display","none");
                return true;
            } else {
                $(".errors-container").fadeIn();
                for (i=0; i < errors.length; i++) {
                    $(".errors-container > .box-section").append('<p>'+ (i+1) + '. ' + errors[i] + '</p>')
                }
            }
            console.log(errors);
        break;
      case 4:
            if ($(".services-offered-for > .button-tap.tapped").length == 0) {
                errors.push("Check one option is mandatory");
            }

            if (errors.length == 0) {
                $(".errors-container").css("display","none");
                return true;
            } else {
                $(".errors-container").fadeIn();
                for (i=0; i < errors.length; i++) {
                    $(".errors-container > .box-section").append('<p>'+ (i+1) + '. ' + errors[i] + '</p>')
                }
            }
            console.log(errors);
        break;
      case 5:
            if (($("#email").val() == "") && ($("#phoneNumber").val() == "")) {
                errors.push("Email or phone is mandatory");
            }

            if (errors.length == 0) {
                $(".errors-container").css("display","none");
                return true;
            } else {
                $(".errors-container").fadeIn();
                for (i=0; i < errors.length; i++) {
                    $(".errors-container > .box-section").append('<p>'+ (i+1) + '. ' + errors[i] + '</p>')
                }
            }
            console.log(errors);
        break;
      default:
        // code block
    }

}
