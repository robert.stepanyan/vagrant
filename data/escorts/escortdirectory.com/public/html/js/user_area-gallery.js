$( document ).ready(function() {

    $(".dropdown-selection.single-option").find(".dropdown-menu > li > a").on("click",function(){
        var val = $(this).html();
        $(this).parents(".dropdown-selection").find(".single-option-select").val(val);
    });

    $(".dropdown-toggle").each(function(){
        removeDropdownSelected(this);
    });

    $(".dropdown-menu li a").on("click", function(){
        var val = $(this).html();
        var type = $(this).parents(".dropdown-menu").attr("data-type");
        $("#" + type).find(".dropdown-toggle-val").html(val);
        removeDropdownSelected(this);
    });

    $("body").on("click", function(){
        $(".settings-list").fadeOut();
    });

    $(".settings-trigger").on("click", function(e){
        e.stopPropagation();
        $(this).parents(".settings-container").find(".settings-list").stop(true, true).fadeToggle();
        
    });

    $(".add-photo-video").on("click", function(){
        $("#photoOrVideo").trigger("click");
    });

    $(".add-natural-photo").on("click", function(){
        $("#naturalPhoto").trigger("click");
    });

    if ($("#submenu1").parent("li").hasClass("active")) {
        $("#submenu1").addClass("show");
    }

    if (ismobile()) {
        
        $("#submenu-trigger").on("click",function(e){
            e.preventDefault();
            e.stopPropagation();
            $(".user-area").toggleClass("mobile-opened");
            $(".header").toggleClass("fixed");
        });

        $('.left-section').click( function(e) {
            e.stopPropagation();
        });

        $("body").on("click",function(){
            $(".user-area").removeClass("mobile-opened");
            $(".header").removeClass("fixed");
        });
    }

    var labels = [];
    $('.multiple-dropdown-box').multiselect({ 
        
        onChange: function(option, checked, select){
            var index = labels.indexOf($(option).val());
            if (index !== -1) {
                labels.splice(index, 1);
            } else {
                labels.push($(option).val());
            }
        },
        buttonText: function(obj) {
            if (labels.length === 0) {
                return 'Nothing selected';
            }
            else if (labels.length >= 2) {
                return labels[0]+ ', +' + (labels.length - 1);
            }
            else if(labels.length >= 1){
                return labels;
            }
        }
    });
});

function ismobile(){
     var newWindowWidth = $(window).width();

        return (newWindowWidth < 768);
}

function removeDropdownSelected(obj){
    var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
    $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
        $(this).show();
    });
    $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
        if ($(this).find("a").html() == val) {
            $(this).hide();
        }
    });
}
    