var MobileFilters = function () {

    var _xhrs = {
        fetchingNextPage: null
    };

    function isLocationSelected() {
        return $('#country_id').val() || $('#city_id').val() || $('#region_id').val() || $('#zone_id').val();
    }

    function applyFilters(filters = {}, pjaxParams = {}, pure = false) {

        var initialData = pure ? {} : GetParams.convertToObject();
        var data = Object.assign(initialData, filters);
        var isSearchAction = $('.escort-list-pagination-search');
        var old_pagination;
        if (isSearchAction)
        {
            old_pagination = $('.pagination-container');
        }else{
            old_pagination = $('#pjax-container').find('.pagination-container');
        }
        var url = (lang_id ? '/' + lang_id : lang_id) + window.location.pathname;
        _xhrs['fetchingNextPage'] = $.pjax(Object.assign({}, {
            scrollTo: false,
            container: '#pjax-container .escorts-list',
            timeout: 9999000, //	ajax timeout in milliseconds after which a full refresh is forced
            push: true, //	use pushState to add a browser history entry upon navigation
            replace: false, //	replace URL without adding browser history entry
            maxCacheLength: 20, //	maximum cache size for previous container contents
            dataType: "html", //	see $.ajax
            url: url,
            method: 'GET',
            data: data,
            mutate_contents: function (contents) {
                var new_pagination = $(contents).find('.pagination-container');

                old_pagination.replaceWith(new_pagination);

                var urlParams = new URLSearchParams(window.location.search);
                if (urlParams.get('advanced_search'))
                {
                    var count = getCookie('near_by_count');
                    if(urlParams.get('sort') !== 'close-to-me' && count ) {
                        $('.total-results h1').first().html(count+' escorts found Worldwide');
                    }else if(count){
                        $('.total-results h1').first().html(count+' escorts found close to you');
                    }
                }
                return $(contents).find('.escorts-list').html(); //.find('div.escorts.escorts').html();
            }
        }, pjaxParams));
    };

    var eventHandlers = {

        onNextPageAppear: function () {

            if (_xhrs['fetchingNextPage']) return console.warn("Getting next page escorts");

            var $active_link = $('.pagination a.active'),
                page = $active_link.next('[data-pjax=1]').data('page');

            if (!page) return;

            var filters = {page: page};
            var pjaxParams = {
                clear_container: false, // Append next page to previous
                scrollTo: false
            };
            applyFilters(filters, pjaxParams);
        },

        onSearchEnd: function () {
            _xhrs.fetchingNextPage = null;
            initMobilePagination();

        },

        onSearchFormSubmit: function (e) {

            if (window.location.pathname == '/') {
                e.preventDefault();

                // Temporary removing this blocks
                // ------------------------
                $('.secondary-breadcrumb-wrapper').hide();
                $('.cities-dictionary').hide();
                // $('.total-results').hide();
                // ------------------------

                var excludeInputs = ['countryInput-mobile'];
                var getParams = $("#advanced-search-form-mobile :input")
                    .filter(function (index, element) {
                        if (excludeInputs.includes($(element).attr('name'))) return false;

                        return $(element).val() != '';
                    })
                    .serialize();

                var filters = {sort: $('#ordering-native-select').val(), page: 1};
                var pjaxParams = {url: window.location.pathname + '?' + getParams, scrollTo: false};
                if (isLocationSelected()) {
                    filters.sort = 'recommended_escorts';
                    $('#ordering-native-select').val('recommended_escorts');
                }

                applyFilters(filters, pjaxParams, true);
            }
        }
    };

    function initMobilePagination(){
        $('.scroll-page').waypoint(function(direction) {
            if(direction == 'down') {

                eventHandlers.onNextPageAppear();
            }
        }, {
            offset: '30%'
        });
    }

    function bindEvents() {
        $(document)
            .on('pjax:beforeSend', eventHandlers.onSearchStart)
            .on('pjax:error', eventHandlers.onSearchEnd)
            .on('pjax:success', eventHandlers.onSearchEnd);

        $('#advanced-search-form-mobile').on('submit', eventHandlers.onSearchFormSubmit);
        initMobilePagination();
    };

    return {
        bindEvents: bindEvents
    }
}();

var Filters = function () {

    var _listingContainer;

    function init() {
        bindEvents();
        cacheComponents();
    }

    function cacheComponents() {
        _listingContainer = $('#pjax-container');
    };

    function resetPagination() {
        GetParams.set('page', '1');
    };

    function resetLocation() {
        GetParams.remove('country_id');
        GetParams.remove('city_id');
        GetParams.remove('zone_id');
        GetParams.remove('region_id');
    };

    function updateCurrentList(url = window.location.href) {

        $.pjax({
            container: '#pjax-container',
            timeout: 99999000, //	ajax timeout in milliseconds after which a full refresh is forced
            push: true, //	use pushState to add a browser history entry upon navigation
            replace: false, //	replace URL without adding browser history entry
            maxCacheLength: 20, //	maximum cache size for previous container contents
            scrollTo: false,  //	vertical position to scroll to after navigation. To avoid changing scroll position, pass false.
            type: "POST",
            dataType: "html",
            url: url,
        });
    };

    var eventHandlers = {

        onSortingChange: function () {
            var newSorting;

            if (Sceon.isMobile()) {
                newSorting = $(this).val();
            } else {
                newSorting = $(this).find('input').val();
            }

            if(newSorting == 'recommended_escorts') {
                resetLocation();
            }

            GetParams.set('sort', newSorting);
            resetPagination();
            updateCurrentList();
            var queryString = window.location.search;
            var urlParams = new URLSearchParams(queryString);
            if (urlParams.get('advanced_search') && urlParams.get('s_is_online_now'))
            {
                setTimeout(function (){var count = getCookie('near_by_count');
                    if(urlParams.get('sort') !== 'close-to-me' && count ) {
                        $('.total-results h1').first().html(count+' escorts found Worldwide');
                    }else if(count){
                        $('.total-results h1').first().html(count+' escorts found close to you');
                    }},1000);
            }
        },

        onSearchStart: function () {

            if (Sceon.isMobile() == false) {
                var y = _listingContainer.offset().top - 25;
               // $('html, body').animate({scrollTop: y}, 600);
            }

            _listingContainer.LoadingOverlay('show');
        },

        onSearchEnd: function () {
            if (Sceon.isMobile()){
                var country_id = $('#country_id').val();
                var city_id = $('#city_id').val();
            }

            GetParams.regenerate();
            _listingContainer.LoadingOverlay('hide');
            Cubix.initBanners(country_id, city_id);
            Sceon.lazyLoadImages();
        },

        onPageChange: function (e) {
            e.preventDefault();
            var page = $(this).data('page');
            GetParams.set('page', page);

            updateCurrentList();
        }
    };

    function bindEvents() {

        $(document)
            .on('click', '.advanced-search-trigger[data-type="sort-by"] > li', eventHandlers.onSortingChange)
            .on('change', '#ordering-native-select', eventHandlers.onSortingChange)
            .on('pjax:error', eventHandlers.onSearchEnd)
            .on('pjax:success', eventHandlers.onSearchEnd)
            .on('pjax:beforeSend', eventHandlers.onSearchStart)
            .on('click', '.pagination a.page-number:not(.active)', eventHandlers.onPageChange);

        if (Sceon.isMobile()) {
            MobileFilters.bindEvents();
        }
    };

    return {
        init: init
    };
}();

$(document).ready(function () {
    Filters.init();
});

var getCookie = function(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};