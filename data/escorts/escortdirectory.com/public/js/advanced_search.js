var AdvancedSearchPage = function () {

    function init() {
        initMultiselects();
        bindEvents();
        hideLoader();
    }

    var templates = {
        loading: function () {
            return headerVars.dictionary.loading + '...';
        }
    };

    function hideLoader() {
        $(".advanced_search_loading").hide();
    };

    function initMultiselects() {
        $('.multiple-dropdown-box').multiselect({

            onChange: function (option, checked, select) {
                select = $(this.$select);
                var list = $(this.$ul)
                var labels = [];
                list.find('input:checked').each(function () {
                    labels.push($(this).val());
                })

                select.val(labels);
            },
            buttonText: function (options) {
                var labels = [];
                options.each(function () {
                    labels.push($(this).text())
                });

                if (options.length > 3) {
                    return labels[0] + ',' + labels[1] + ' +' + options.length;
                } else if (options.length >= 1) {
                    return labels.join(',');
                } else {
                    return headerVars.dictionary.nothing_selected;
                }
            }

        });
        $('.multiple-dropdown-box').parents('.advanced-search-container').removeClass('d-none').addClass('d-flex');
        $('.multiple-dropdown-box').parents('.dropdown-selection').removeClass('d-none').addClass('d-flex');
    };

    function updateCurrentList(url = window.location.href) {

        $.pjax({
            container: '#pjax-container',
            timeout: 99999000, //	ajax timeout in milliseconds after which a full refresh is forced
            push: true, //	use pushState to add a browser history entry upon navigation
            replace: false, //	replace URL without adding browser history entry
            maxCacheLength: 20, //	maximum cache size for previous container contents
            scrollTo: false, //	vertical position to scroll to after navigation. To avoid changing scroll position, pass false.
            type: "POST",
            dataType: "html",
            url: url,
        });
    };

    function resetPagination() {
        GetParams.set('page', '1');
    };

    function constructDropdown(params) {
        var cities_count = 0;
        var exclude = params['exclude'] || [];

        try {
            var regions = JSON.parse(params.regions);
            var visible_html = '<input type=\"text\" class=\"form-control\ geolocation">';
            var data_html = "<option></option>";

            for (var title in regions) {

                var data_html_chunk = "";
                var visible_html_chunk = "";

                regions[title].forEach(function (city) {
                    if (exclude.includes(city.id)) return;

                    cities_count++;
                    data_html_chunk += "<option value=\"" + city.id + "\">" + city.title + "</option>";
                    visible_html_chunk += "<li class=\"searchable\" data-val=\"" + city.id + "\"><a href=\"javascript:void(0)\">" + city.title + "</a></li>";
                });

                if (title) {
                    data_html += "<optgroup label=\"" + title + "\" >" + data_html_chunk + "</optgroup>";
                    visible_html += "<ul class=\"sub-dropdown-menu city-ul-custom\"> <li class=\"mb-2 mt-2 states\"> <b> " + title + " </b> </li> " + visible_html_chunk + "</ul>";
                } else {
                    data_html += data_html_chunk;
                    visible_html += visible_html_chunk;
                }
            }

            params.select.html(data_html).val(null); // This is only <option> s
            params.ul.html(visible_html); // This one is for ul>li that is visible for user
        } catch (e) {
            console.warn(e.message);
        }

        return {
            cities: cities_count
        };
    };

    function generateUrlForCurrentFilters() {
        var getParams = $("#advanced-search-form :input")
            .filter(function (index, element) {
                return $(element).val() != '';
            })
            .serialize();
        return window.location.pathname + '?' + getParams;
    };

    var eventHandlers = {

        onSortingChange: function () {
            var newSorting = $(this).find('input').val();
            $('#sort').val(newSorting);

            resetPagination();

            var url = generateUrlForCurrentFilters();
            updateCurrentList(url);
        },

        onSearchStart: function () {
            $('html, body').animate({scrollTop: 500}, 600);
            $('#pjax-container').LoadingOverlay('show');
        },

        onSearchEnd: function () {
            var country_id = $('#filter-countries').val();
            var city_id = $('#filter-cities').val();
            GetParams.regenerate();
            $('#pjax-container').LoadingOverlay('hide');
            Cubix.initBanners(country_id, city_id);
            Sceon.lazyLoadImages();
        },

        onPageChange: function (e) {
            e.preventDefault();
            var page = $(this).data('page');
            $('#page').val(page);

            var url = generateUrlForCurrentFilters();
            updateCurrentList(url);
        },

        onAdvancedSearchFormReset: function () {
            $('#advanced-search-form').get(0).reset();
            $('.dropdown-menu > li').css('display', 'block');
            $('.multiselect-container input[type="checkbox"]').prop('checked', false);
            $('.multiselect-container li.active').removeClass('active');
            $('.multiselect-selected-text').text(headerVars.dictionary.nothing_selected);
            $('.multiselect.dropdown-toggle').attr('title', 'None selected');

            ['#age_from', '#age_to', '#incall_outcall', '#s_currency', '#escortType', '#interested_in'].forEach(function (id) {
                $(id).siblings('ul').find('li > a').first().trigger('click');
            });

            $('#filter-country .dropdown-toggle-val').html(headerVars.dictionary.country);
            $('#filter-cities-dropdown .dropdown-toggle-val').html(headerVars.dictionary.city);

            $('.single-option input[type="radio"]').prop('checked', false);
        },

        onAdvancedSearchButtonClick: function (e) {

            var url = generateUrlForCurrentFilters();
            e.preventDefault();

            resetPagination();
            updateCurrentList(url);
        },

        onDropdownItemClick: function () {
            var parent = $(this).closest('ul');
            if (parent.data('type') === 'filter-country' || parent.data('type') === 'filter-cities-dropdown' )
            {
                parent.children('input').val('');
                parent.children('li').each(function () {
                    $(this).show();
                });
            }

            var removeDropdownSelected = function (obj) {
                var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
                $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function () {
                    $(this).show();
                });
                $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function () {
                    if ($(this).find("a").html() == val) {
                        $(this).hide();
                    }
                });
            };

            var val = $(this).html(),
                value = $(this).closest('li').attr('data-val'),
                dataVal = $(this).closest('li').attr('value');

            $(this).parents('.dropdown-menu').find('input:checked').removeAttr('checked');
            $(this).closest('li').find('input').attr('checked', 'checked');

            var type = $(this).parents(".dropdown-menu").attr("data-type");
            $("#" + type).find(".dropdown-toggle-val").html(val);
            $("#" + type).find(".dropdown-toggle-val").attr('data-val', dataVal);
            $('#' + type).parent().find('select').val(value).trigger('change');

            removeDropdownSelected(this);
        },

        onDropdownKeyDown: function() {
            var searchIng = $(this).val();
            var states = $('li.states');
            if (searchIng && states)
            {
                states.hide();
            }else{
                states.show();
            }
            searchIng = new RegExp(searchIng.toUpperCase());
            $(this).parent('ul').find('li.searchable').each(function() {
               var searchIn = $(this).children('a').text().trim();
               searchIn = searchIn.toUpperCase();
               var states = $('li.states');
               if (!searchIng.test(searchIn))
               {
                   $(this).hide();
               }else{
                   $(this).show();
               }
           });

        },

        onFilterCountryChange: function () {
            var $country_select = $("#filter-countries"),
                $cities_select = $("#filter-cities");

            $cities_select.empty();
            $cities_select.siblings('ul').empty().append(templates.loading());
            $cities_select.siblings('a').first().html(headerVars.dictionary.city);

            $.ajax({
                url: '/geography/ajax-get-cities-grouped?json=true',
                data: {
                    country_id: $country_select.val(),
                    has_escorts: 1
                },
                success: function (r) {
                    var result = constructDropdown({
                        regions: r,
                        select: $cities_select,
                        ul: $cities_select.siblings('ul'),
                    });

                    if (result.cities < 1) {
                        $cities_select.parents('.dropdown-selection').addClass('disabled');
                    } else {
                        $cities_select.parents('.dropdown-selection').removeClass('disabled');
                    }

                }
            });
        },

        onListingTypeChange: function () {
            $(".view-buttons").removeClass("active");
            $(this).addClass("active");

            if ($(this).attr("data-type") == "view-squares") {
                $(".escorts-list").removeClass("view-list").addClass("d-flex view-squares");
                $(".escort-item").removeClass("d-flex");
                Cookies.set('list_type', 'gallery')
            } else {
                $(".escorts-list").removeClass("d-flex view-squares").addClass("view-list");
                $(".escort-item").addClass("d-flex");
                Cookies.set('list_type', 'list')
            }
        }

    };

    function bindEvents() {
        $(document)
            .on('click', '.advanced-search-trigger[data-type="sort-by"] > li', eventHandlers.onSortingChange)
            .on('pjax:error', eventHandlers.onSearchEnd)
            .on('pjax:success', eventHandlers.onSearchEnd)
            .on('pjax:beforeSend', eventHandlers.onSearchStart)
            .on('click', '.pagination a.page-number:not(.active)', eventHandlers.onPageChange)
            .on('click', '.dropdown-menu > li > a', eventHandlers.onDropdownItemClick)
            .on('click', '.dropdown-menu > ul > li > a', eventHandlers.onDropdownItemClick)
            .on('keyup', '.dropdown-menu > input', eventHandlers.onDropdownKeyDown)
            .on('click', '.view-buttons', eventHandlers.onListingTypeChange);

        $('#filter-countries').on('change', eventHandlers.onFilterCountryChange);

        $('.advanced-search-button').on('click', eventHandlers.onAdvancedSearchButtonClick);
        $('#reset').on('click', eventHandlers.onAdvancedSearchFormReset);

        $('a.geoloc-select').on('click',function () {
            var parent = $(this).next('ul');
            $('.geolocation').css('border-color','transparent');
            setTimeout(function(){parent.children('input').focus();},10);
        });


        $('.dropdown-toggle').on('click',function () {
            var $body = $('body');
            var ariaExpand = $(this).attr('aria-expanded');

            if ( ariaExpand === 'false' || ariaExpand === 'undefined' )
            {
                $body.addClass('freezed');
            }else{
                $body.removeClass('freezed');
            }
        });
        $(document).mouseup(function(e)
        {
            var container = $('.dropdown-toggle');

            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target))
            {
                $('body').removeClass('freezed');
            }
        });

        $.each($('button.dropdown-toggle'),function () {
           if(!$(this).attr('aria-expanded'))
           {
               $(this).attr('aria-expanded','false');
           }
        });

    };

    return {
        init: init,
        bindEvents: bindEvents,
    };
}();


$(function () {
    AdvancedSearchPage.init();
})