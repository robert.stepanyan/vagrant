function autocomplete(inp) {
    var Data = {
        "countries": {},
        "regions": {},
        "cities": {},
        "zones": {}
    };
    var dataLoaded = false;
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    if (inp) {
        var country_inp = createInput('country_id');
        var region_inp = createInput('region_id');
        var city_inp = createInput('city_id');
        var zone_inp = createInput('zone_id');
        var loader = initLoader(inp);
        inp.parentNode.insertBefore(country_inp, inp);
        inp.parentNode.insertBefore(region_inp, inp);
        inp.parentNode.insertBefore(city_inp, inp);
        inp.parentNode.insertBefore(zone_inp, inp);
        /* get autocomplete data on focus in*/
        inp.addEventListener('focusin', function (e) {
            inp.selectionStart = inp.value.length;
            initData();
        });
        /*execute a function when someone writes in the text field:*/

        var handle_oninput_event = function() {
            initData();
            var a, i, val = this.value.trim();
            /*close any already open lists of autocompleted values*/
            closeAllLists();
            if (!val || val.length < 2) {
                setValue('country_id', '');
                setValue('region_id', '');
                setValue('city_id', '');
                setValue('zone_id', '');
                $('input[name="is_location_cleared"]').val(1);
                return false;
            }
            $('input[name="is_location_cleared"]').val(0);
            currentFocus = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.style.maxHeight = 0;
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            /*for each item in the array...*/
           
            var res = matchList(val);
            
            if(Array.isArray(res) && res.length == 0 ){
               let is_mobile = Sceon.isMobile();
               $.ajax({
                url: '/index/city-search-log',
                data: {query: val, is_mob: is_mobile},
                method: 'post',
                    success: function (response) {
                        console.log(response);
                    }
                })
            }

            drawList(a, val, res);
            var self = this;
            $(a).animate({'max-height': '300px'}, 'slow', function (e) {
                self.dispatchEvent(new Event('autocomplete:listDrawed'))
            });
        }
        var _interval = null;

        inp.addEventListener("input", function (e) {
            if(_interval) clearInterval(_interval);
            _interval = setTimeout(handle_oninput_event.bind(this), 350);
        });

        /*execute a function presses a key on the keyboard:*/
        inp.addEventListener("keydown", function (e) {
            var list = document.getElementById(this.id + "autocomplete-list");
            var x;
            if (list) x = list.getElementsByTagName("div");
            if (e.keyCode == 40) {
                e.preventDefault();
                /*If the arrow DOWN key is pressed,
                increase the currentFocus variable:*/
                currentFocus++;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 38) {//up
                e.preventDefault();
                /*If the arrow UP key is pressed,
                decrease the currentFocus variable:*/
                currentFocus--;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 13) {
                /*If the ENTER key is pressed, prevent the form from being submitted,*/
                e.preventDefault();
                if (currentFocus > -1) {
                    /*and simulate a click on the "active" item:*/
                    if (x) x[currentFocus].click();
                }
            }
        });

        inp.addEventListener('autocomplete:listDrawed', function (e) {
            var list = document.getElementById(this.id + "autocomplete-list");
            var x;
            if (list) x = list.getElementsByTagName("div");
            if (x)
                currentFocus = 0;
            addActive(x);
        })
    }

    function initData() {
        if (!dataLoaded) {
            inp.setAttribute('disabled', true);
            loader.classList.add('loading');
            $.ajax({
                url: '/index/get-zone-city-country/',
                // async: false,
                success: function (response) {
                    var D = JSON.parse(response);
                    for (var i = 0; i < D.countries.length; i++) {
                        Data.countries[D.countries[i].iso] = D.countries[i]
                    }
                    for (i = 0; i < D.cities.length; i++) {
                        var k = D.cities[i].city_slug +'-' + D.cities[i].city_id;
                        Data.cities[k] = D.cities[i]
                    }
                    for (i = 0; i < D.regions.length; i++) {
                        Data.regions[D.regions[i].region_title] = D.regions[i]
                    }
                    Data.zones = D.zones;
                    d = Data;
                    dataLoaded = true;
                    loader.classList.remove('loading');
                    inp.removeAttribute('disabled');
                    inp.focus()
                }
            })
        }
    }

    function initLoader(inp){
        var loader = document.querySelector('span.auto-complete-loader');
        if(!loader){
            loader = document.createElement('span');
            loader.classList.add('auto-complete-loader');
        }
        inp.parentNode.insertBefore(loader, inp.nextSibling);
        return loader;
    }

    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
        // scroll to element
        $(x[currentFocus].parentNode).scrollTop(0);//set to top
        $(x[currentFocus].parentNode).scrollTop($('.autocomplete-active:first').offset().top-$(".autocomplete-items").height());
    }

    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }

    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }

    function createInput(id) {
        var input = document.getElementById(id);
        if (!input) {
            input = document.createElement('input');
            input.setAttribute('type', 'hidden');
            input.setAttribute('id', id);
            input.setAttribute('name', id);
        }
        return input;
    }

    function setValue(id, val, slug = null) {
        document.getElementById(id).value = val;
        document.getElementById(id).setAttribute('data-slug', slug);
    }


    function matchZones(text) {
        var list = [];
        var regex = new RegExp(text.trim(), 'gi');
        /*check if the item starts with the same letters as the text field value:*/
        for (var i  in Data.zones) {
            var title = Data.zones[i].zone_title;
            var m = title.match(regex);
            if (m && m.length) {
                var fullTitle = Data.zones[i].zone_title;
                var k = Data.zones[i].city_slug +'-'+Data.zones[i].city_id;
                if (Data.cities.hasOwnProperty(k)) {
                    fullTitle += ', ' + Data.cities[k].city_title;
                    var rKey = Data.cities[k].region_title;
                    var cKey = Data.cities[k].country_iso;
                    if (rKey && Data.regions.hasOwnProperty(rKey)) {
                        fullTitle += ', ' + rKey;
                    }
                    if (cKey && Data.countries.hasOwnProperty(cKey)) {
                        fullTitle += ', ' + cKey.toUpperCase();
                    }
                }
                var row = Object.assign({full_title: fullTitle, title: title, cssClass:'zone'}, Data.zones[i]);
                delete row.city_id;
                list.push(row)
            }
        }
        return list;
    }

    function matchCities(text) {
        var list = [];
        var regex = new RegExp(text.trim(), 'gi');
        /*check if the item starts with the same letters as the text field value:*/
        for (var i  in Data.cities) {
            var title = Data.cities[i].city_title;
            var m = title.match(regex);
            if (m && m.length) {
                var fullTitle = Data.cities[i].city_title;
                var rKey = Data.cities[i].region_title;
                var cKey = Data.cities[i].country_iso;
                // if (rKey && Data.regions.hasOwnProperty(rKey)) {
                //     fullTitle += ', <i>' + rKey +' (region)</i>';
                // }
                if (cKey && Data.countries.hasOwnProperty(cKey)) {
                    fullTitle += ', ' + cKey.toUpperCase();
                }
                var row = Object.assign({full_title: fullTitle, title: title, cssClass:'city'}, Data.cities[i]);
                list.push(row)
            }
        }
        return list;
    }

    function getCountryById(id) {
        var country = {};
        for (var i  in Data.countries) {
            if (Number(id) === Number(Data.countries[i].country_id)) {
                country = Data.countries[i];
                break
            }
        }
        return country
    }

    function matchRegions(text) {
        var list = [];
        var regex = new RegExp(text.trim(), 'gi');
        /*check if the item starts with the same letters as the text field value:*/
        for (var i  in Data.regions) {
            var title = Data.regions[i].region_title;
            var m = title.match(regex);
            if (m && m.length) {
                var fullTitle = Data.regions[i].region_title;
                var cKey = getCountryById(Data.regions[i].country_id);

                if (cKey) {
                    fullTitle += ', ' + cKey.country_title;
                }
                var row = Object.assign({country_id: cKey.country_id, country_slug: cKey.country_slug , full_title: fullTitle, title: title, cssClass:'region'}, Data.regions[i]);
                list.push(row)
            }
        }
        return list;
    }

    function matchCountries(text) {
        var list = [];
        var regex = new RegExp(text.trim(), 'gi');
        /*check if the item starts with the same letters as the text field value:*/
        for (var i  in Data.countries) {
            var title = Data.countries[i].country_title;
            var m = title.match(regex);
            if (m && m.length) {
                var row = Object.assign({full_title: title, title: title, cssClass:'country'}, Data.countries[i]);
                list.push(row)
            }
        }
        return list;
    }

    var rev_ads = false;

    function matchList(text) {
        var List = [];
        var listArr = text.trim().replace(/^[\,]{1,}|[\,]{1,}$/gm,'').split(',');
        listArr = listArr.filter(function (v) {
            return !!v;
        });
        // var firstPart = listArr[0];
        var citiesList = matchCities('');

        if($('#modalCreateReview').css('display') == 'block' || $('#modalAddAd').css('display') == 'block') {
            var zonesList = '';
            var regionsList = '';
            var countriesList = '';
        } else {
            var zonesList = matchZones('');
            var regionsList = matchRegions('');
            var countriesList = matchCountries('');
        }

        List = List.concat(countriesList, regionsList, citiesList, zonesList);

        // if(listArr.length > 1){
            List = List.filter(function(value, index){
                var p = document.createElement('p');
                p.innerHTML = value.full_title;
                var cleanText = p.innerText;
                var regex = new RegExp(text.trim(), 'gi');
                var m = cleanText.match(regex);
                return !!(m && m.length);
            });
        // }
        return List;
    }

    function getMarked(title, sub) {
        var regex = new RegExp(sub, "gi");
        return title.replace(regex, '<b>$&</b>')
    }

    function drawList(a, val, arr) {
        var b;
        for (var i = 0; i < arr.length; i++) {
            /*create a DIV element for each matching element:*/
            b = document.createElement("DIV");
            b.classList.add('autocomplete-item');
            b.classList.add(arr[i].cssClass);
            /*make the matching letters bold:*/
            b.innerHTML = '<small><i class="fas fa-map-marker-alt flag-place-icon"></i>' + getMarked(arr[i].full_title, val) + "</small>";
            var dataKey = {
                country_id: (arr[i].country_id) ? arr[i].country_id : '',
                region_id: (arr[i].region_id) ? arr[i].region_id : '',
                city_id: (arr[i].city_id) ? arr[i].city_id : '',
                zone_id: (arr[i].cityzone_id) ? arr[i].cityzone_id : '',
                country_slug: (arr[i].country_slug) ? arr[i].country_slug : '',
                city_slug: (arr[i].city_slug) ? arr[i].city_slug : '',
                region_slug: (arr[i].region_slug) ? arr[i].region_slug : '',
            };
            /*insert a input field that will hold the current array item's value:*/
            var HInp  = document.createElement('input');
            HInp.setAttribute('type', 'hidden');
            HInp.setAttribute('value', arr[i].full_title.replace('<i>','').replace(' (region)</i>',''));
            HInp.setAttribute('data-key', JSON.stringify(dataKey));
            b.appendChild(HInp);
            /*execute a function when someone clicks on the item value (DIV element):*/
            b.addEventListener("click", function (e) {
                /*insert the value for the autocomplete text field:*/

                var sort = $('.advanced-search-trigger li [name="sort"]').val();
                /*if(sort == 'close-to-me') {
                    $('.advanced-search-trigger li [name="sort"][value="recommended_escorts"]').parents('li').first().click();
                }*/

                var activeInp = this.getElementsByTagName("input")[0];
                inp.value = activeInp.value;
                var dataKey = JSON.parse(activeInp.dataset.key);
                setValue('country_id', dataKey.country_id, dataKey.country_slug);
                setValue('region_id', dataKey.region_id);
                setValue('city_id', dataKey.city_id, dataKey.city_slug);
                setValue('zone_id', dataKey.zone_id, dataKey.region_slug);
                /*close the list of autocompleted values,
                (or any other open lists of autocompleted values:*/
                closeAllLists();
                $(inp).trigger('onAfterSelect');
            });
            a.appendChild(b);
        }
        if(!arr.length) {
            b = document.createElement("DIV");
            b.classList.add('autocomplete-item', 'disabled');
            /*make the matching letters bold:*/
            b.innerHTML = '<small><i class="fas fa-dot-circle mr-2"></i>' + 'location not found' + "</small>";

            a.appendChild(b);
        }
    }

    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}