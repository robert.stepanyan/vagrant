$(document).ready(function(){

	showFollowModal = function(user_type, user_type_id){
		var form = $('#ajax-follow-form');
		form.find('input[name="user_type"]').val(user_type)
		form.find('input[name="user_type_id"]').val(user_type_id)

		$('#follow-modal')
			.find('.error-box').empty().hide().end()
			.modal('show')
	}

	$('#follow-modal .login-button').click(function(){
		$('#follow-modal').modal('hide');
		$('#login-modal').modal('show');
	});

    $('#ajax-follow-form').submit(function(e){
		e.preventDefault();
		$('#follow-modal .modal-content').LoadingOverlay('show');
		var url = $(this).attr('action');
		var user_type = $('#user_type').val();
		var user_type_id = $('#user_type_id').val();
		var follow_email = $('#follow-email').val();
		$.ajax({
			url: url,
			type: 'POST',
			data:{
				user_type: user_type,
				user_type_id: user_type_id,
				follow_type: 3,
				follow_email: follow_email
			},
			success: function(response){
				response = JSON.parse(response);
				$('#follow-modal .modal-content').LoadingOverlay('hide');
				if(response.status == 'error'){
					$('.error-box').html(' ');
					for(var key in response.msgs){
						$('.error-box').append('<p>' + response.msgs[key] + '</p>');
					}
					$('.error-box').show();
					return false;
				}
				$('#follow-modal').modal('hide');
				$('#follow-success-modal').modal('show');
			},
			error: function(response){

			},
		})
	})
});