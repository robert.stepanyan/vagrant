var ReviewsPage = (function () {

    var pending_requests = {
        getting_cities: null
    };
    var templates = {
        loading: function () {
            return "Loading ...";
        },
        mobile_loading: function mobile_loading() {
            return "<li class='disabled'><a><label>Loading ...</label></a></li>";
        }
    };

    var init = function () {
        bind_events();
        update_geo_filled_countries();
    };

    function update_geo_filled_countries() {

        if(Sceon.isMobile()) {
            var cities = $('#reviews-city-search-mobile').find('input[name="country_id"]');
            if(cities.length >= 1) {
                on_mobile_country_changed().then(function () {
                    if ( cities.length == 1 ) {
                        set_city_selected(cities.first().val());
                }
                })
            }
        }else{
            var cities = $('#reviews-search-city').find('option');
            if(cities.length >= 1) {
                on_country_changed().then(function () {
                    // If this happens, it means that city was set from GEO detector,
                    // this means it must be autoselected :))
                    // ----------------------------
                    if (cities.length == 1)
                        set_city_selected(cities.first().val());
                    // ----------------------------
                })
            }
        }
    };

    function bind_events () {
        $('#reviews-country, #reviews-search-country').on('change', on_country_changed);
    }

    function on_country_changed() {

        return new Promise(function (res, rej) {
            var $country_select = $("#reviews-search-country"),
                $cities_select = $("#reviews-search-city"),
                type = 'reviews-city-search-select';

            $cities_select.empty();
            $cities_select.siblings('ul[data-type="reviews-city-search-select"]').empty().append(templates.loading());
            $cities_select.siblings('.dropdown-toggle').find('.dropdown-toggle-val').html(headerVars.dictionary.all_cities);

            if(!$country_select.val()) {
                return rej();
            }

            $.ajax({
            url: '/geography/ajax-get-cities-reviews?json=true',
            data: {
                country_id: $country_select.val()
            },
            success: function (r) {
                construct_dropdown({
                    regions: r,
                    select: $cities_select,
                    ul: $cities_select.siblings('ul[data-type="reviews-city-search-select"]'),
                }).then(function() {
                    res();
                })
            }
        });

        });

    }

    function on_mobile_country_changed() {

        return new Promise(function(res, rej) {

            if (pending_requests.getting_cities) return console.log("Wait please");

            var $cities_dropdown = $("#reviews-city-search-mobile"),
                country_id = $('#reviews-country-search-mobile input:checked').val(),
                type = 'reviews-city-search-mobile';

            $cities_dropdown.find('ul').empty().append(templates.mobile_loading());
            $cities_dropdown.find('.dropdown-toggle-val').html(headerVars.dictionary.all_cities);

            if(!country_id) {
                return rej();
            }

            pending_requests.getting_cities = $.ajax({
                url: '/geography/ajax-get-cities-reviews??json=true',
                data: {
                    country_id: country_id
                },
                success: function (r) {
                    pending_requests.getting_cities = null
                    construct_mobile_dropdown({
                        regions: r,
                        dropdown: $cities_dropdown,
                    }).then(function() {
                        res();
                    })
                }
            }).done(function () {
                pending_requests.getting_cities = null
            });

        });
    }

    function set_city_selected(cityID) {
        $('.location-dropdown.city-dropdown li[data-val="'+cityID+'"] a').trigger('click');
        $('#reviews-city-search-mobile [name="country_id"][value="'+cityID+'"]').siblings('a').trigger('click');
    };

    function construct_dropdown(params) {
        return new Promise(function(res) {
            try {
                var cities = JSON.parse(params.regions);
                var visible_html = "<ul class=\"sub-dropdown-menu pl-1\" style=\"list-style: none\"> <li data-val=\"0\"><a href=\"javascript:void(0)\">All Cities</a></li>";
                var data_html = "";

                cities.data.forEach(function(city) {
                    data_html += "<option value=\"".concat(city.id, "\">").concat(city.title, "</option>");
                    visible_html += "<li data-val=\"".concat(city.id, "\"><a href=\"javascript:void(0)\">").concat(city.title, "</a></li>");
                });
                visible_html += "</ul> ";

                params.select.html(data_html).val(null); // This is only <option> s
                params.ul.html(visible_html); // This one is for ul>li that is visible for user

                if(cities.data.length === 1) {
                    set_city_selected(cities.data[0].id);
                }

            } catch (e) {
                console.warn(e.message);
            }finally {
                res();
            }
        });

    }

    function construct_mobile_dropdown(params) {
        return new Promise(function(res) {
            try {
                var cities = JSON.parse(params.regions);
                var visible_html = "<ul class=\"sub-dropdown-menu pl-1\" style=\"list-style: none\"> ";

                visible_html += `<li>
                    <label class="radio" title="${headerVars.dictionary.all_cities}">
                        <input type="radio" name="country_id" value=""> <a>${headerVars.dictionary.all_cities} </a>
                    </label>
                </li>`;

                cities.data.forEach(function(city) {
                    visible_html += `<li>
                    <label class="radio" title="${city.title}">
                        <input type="radio" name="country_id" value="${city.id}"> <a>${city.title} </a>
                    </label>
                </li>`;
                });
                visible_html += "</ul> ";

                params.dropdown.find('ul').html(visible_html); // This is only <option> s

                if(cities.data.length === 1) {
                    set_city_selected(cities.data[0].id);
                }
            } catch (e) {
                console.warn(e.message);
            } finally {
                res();
            }
        })

    }

    return {
        init: init,
        on_mobile_country_changed: on_mobile_country_changed
    }
})();


function grading(elm, data) {
    var pnt = 0;

    elm.each(function(i, el){
        if($(this).length){
            pnt  = $(this).data(data);
            pnt = Math.round(pnt/2);
        }

        for(var j=0; j<pnt; j+=1) {
            $(this).find('i').eq(j).addClass('active');
        }
    });
}

function setReadMoreTags( ) {
    $(".escort-details").each(function(){

        text = $(this).find("p").html();
        link = $(this).parents(".escort-item").find("a").attr("href") + '#reviews';
        len = 240;

        if (ismobile()) {
            len = 135;
        }

        if((text.length > text.substring(0, len).length)){
            text = text.substring(0, len - 3) + '...' + '<span class="read-mode-review">'+headerVars.dictionary.read_more+'</span>';
        }

        // return text;
        $(this).find("p").html(text);
    });
}

$( document ).ready(function() {

    // Init page's logic
    // -------------------------------
    ReviewsPage.init();
    // -------------------------------

    grading($('.looks'), 'looks');
    grading($('.service_rating'), 'service');
    setReadMoreTags();

    function removeDropdownSelected(obj){
        var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
        $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
            $(this).show();
        });
        $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
            if ($(this).find("a").html() == val) {
                $(this).hide();
            }
        });
    }

    $(".dropdown-toggle").each(function(){
        removeDropdownSelected(this);
    });

    $("body.reviews").on("click", ".dropdown-menu li a", function(){
        var val = $(this).html(),
            value = $(this).closest('li').attr('data-val'),
            dataVal = $(this).closest('li').attr('value');

        $(this).parents('.dropdown-menu').find('input:checked').removeAttr('checked');
        $(this).closest('li').find('input').attr('checked', 'checked');

        var type = $(this).parents(".dropdown-menu").attr("data-type");
        $("#" + type).find(".dropdown-toggle-val").html(val);
        $("#" + type).find(".dropdown-toggle-val").attr('data-val', dataVal);
        $('#' + type).parent().find('select').val(value).trigger('change');

        removeDropdownSelected(this);
    });

    $("body.reviews").on("click", "#reviews-country-search-mobile li a", ReviewsPage.on_mobile_country_changed);
    $("body.reviews").on('click', '.sort2.mobile li, .sort3 li', function() {
        Loader(1)
    });

    $(".advanced-search").on("click",function(){
        $(this).html() == "Advanced search" ? $(this).html('Basic search') : $(this).html('Advanced search');

        if (ismobile()) {
            $(".advanced-buttons").stop(true, true).toggleClass("active");
        }

        $(".advanced-search-container").stop(true, true).slideToggle();
    });

    $("#cancel").on("click",function(){
        $(".advanced-search").trigger("click");
    });

    if (ismobile()) {
        autocomplete(document.getElementById("countryInput-mobile"));

        $(".escort-item").on("click",function(){
            // $("#modalShowReview").modal("show");
        });
    } else {
        autocomplete(document.getElementById("countryInput"));
    }


    function getLocation(elm, data) {
        var  location = $(elm).filter(function() {
                return $(this).val().length;
            }),
            location_id = location.attr('id'),
            location_value = location.val();
            return (location_value)? data[location_id] = (location_value) : '';
    }


    $(".search-button-trigger").on("click", function () {
        var member = $('#member').val(),
            escort = $('#escort').val(),
            sort = $('#sort3 .dropdown-toggle-val').attr('data-val'),
            country_id = $('#reviews-search-country').val(),
            city_id = $('#reviews-search-city').val(),
            data = {'member': member, 'escort': escort, 'sort': sort, 'country_id' : country_id, 'city_id' : city_id };

        $.ajax({
            url:'/reviews/ajax-list',
            type: 'GET',
            data: data,
            beforeSend: function () {
                $('.escorts').LoadingOverlay('show');
            },
            success: function (resp) {
                $('#list').html(resp).LoadingOverlay('hide');
                triggerModalEvents();
                renderModal();
                setReadMoreTags();
            }
        })
    });

    $(".search-mobile").on("click", function () {
        var member = $('#memberName').val(),
            escort = $('#escortName').val(),
            gender = $('.interestedIn-mobile div.active').attr('data-value'),
            sort = $('#sort2 .dropdown-toggle-val').attr('data-val'),
            country_id = $('#reviews-country-search-mobile input[checked]').val(),
            city_id = $('#reviews-city-search-mobile input[checked]').val(),
            data = {'escort' : escort, 'member': member, 'gender': gender, 'sort': sort, country_id: country_id, city_id: city_id};

        var country = $('#reviews-country-search-mobile .dropdown-toggle-val').text();
        var city = $('#reviews-city-search-mobile .dropdown-toggle-val').text();

        var is_country_selected = country_id;
        var is_city_selected = city_id;

        if(is_country_selected) {
            var location = country;

            if(is_city_selected)
                location += ', ' + city;

            $('#myCountry-mobile-place').html(location);
        }

        $.ajax({
            url:'/reviews/ajax-list',
            type: 'GET',
            data: data,
            beforeSend: function () {
                $('.escorts').LoadingOverlay('show');
            },
            success: function (resp) {
                $('#list').html(resp).LoadingOverlay('hide');
                triggerModalEventsMobile();
                renderModal();
            }
        })
    });


    $(".view-buttons").on("click",function(){
        $(".view-buttons").removeClass("active");
        $(this).addClass("active");

        if ($(this).attr("data-type") == "view-squares") {
            $(".escorts-list").removeClass("view-list").addClass("d-flex view-squares");
            $(".escort-item").removeClass("d-flex");
        } else {
            $(".escorts-list").removeClass("d-flex view-squares").addClass("view-list");
            $(".escort-item").addClass("d-flex");
        }
    });

    $(".intentions-container-mobile").on("click",function(){
        $(".filters-mobile").stop(true, true).fadeToggle();
        $('body').toggleClass('no-more-scroll');
    });

    $("#cancel-mobile").on("click",function(){
        $(".intentions-container-mobile").trigger("click");
    });

    $(".btn-dropdown-rounded").on("click",function(){
        $(this).parent(".d-flex").find(".btn-dropdown-rounded").removeClass("active");
        $(this).addClass("active");
    });

    $(".search-mobile").on("click",function(){
        var country = $("#countryInput-mobile").val();
        if (country !== "") {
            $("#myCountry-mobile-place").html(country);
        }

        if ($(".lookingFor-mobile").find(".btn-dropdown-rounded").hasClass("active")) {
            $(".lookingFor-mobile-place").html($(".lookingFor-mobile").find(".btn-dropdown-rounded.active").html());
        }

        if ($(".interestedIn-mobile").find(".btn-dropdown-rounded").hasClass("active")) {
            $(".interestedIn-mobile-place").html($(".interestedIn-mobile").find(".btn-dropdown-rounded.active").html());
        }

        $(".intentions-container-mobile").trigger("click");
    });

     Loader = function(page) {
         var member, gender, sort, datum, country_id, city_id, escort_name;
         if(ismobile()) {
             escort_name = $('#escortName').val(),
             member = $('#memberName').val(),
             gender = $('.interestedIn-mobile div.active').attr('data-value'),
             sort = $('#sort2 .dropdown-toggle-val').attr('data-val'),
             country_id = $('#reviews-country-search-mobile input[checked]').val(),
             city_id = $('#reviews-city-search-mobile input[checked]').val(),
             datum = {'page': page, 'member': member, 'gender': gender, 'sort': sort, 'country_id' : country_id, 'city_id' : city_id, 'escort': escort_name};
             getLocation('.loc_id input[type="hidden"]', datum);
         } else {
             escort_name = $('#escort').val(),
             member = $('#member').val(),
             gender = $('#lookingFor .dropdown-toggle-val').attr('data-val'),
             sort = $('#sort3 .dropdown-toggle-val').attr('data-val'),
             country_id = $('#reviews-search-country').find(":selected").val(),
             city_id = $('#reviews-search-city').find(":selected").val(),
             datum = {'page':page, 'member': member, 'gender': gender, 'sort': sort, 'country_id' : country_id, 'city_id' : city_id, 'escort' : escort_name};
             getLocation('.location_id input[type="hidden"]', datum);
         }

        $.ajax({
            url:'/reviews/ajax-list',
            type: 'GET',
            data: datum,
            beforeSend: function () {
                $('.escorts').LoadingOverlay('show');
            },
            success: function (resp) {
                $('#list').html(resp).LoadingOverlay('hide');
                triggerModalEvents();
                renderModal();
                triggerModalEventsMobile();
                $('.to-the-moon').trigger('click');
                setReadMoreTags();
            }
        })
    }

    $('.sort3 li').on('click', function () {
        var member = $('#member').val(),
            gender = $('#lookingFor .dropdown-toggle-val').attr('data-val'),
            sort = $(this).attr('value'),
            data = {'member': member, 'gender': gender, 'sort': sort};
            getLocation('.location_id input[type="hidden"]', data);

        $.ajax({
            url:'/reviews/ajax-list',
            type: 'GET',
            data: data,
            beforeSend: function () {
                $('.escorts').LoadingOverlay('show');
            },
            success: function (resp) {
                $('#list').html(resp).LoadingOverlay('hide');
                triggerModalEvents();
                renderModal();
            }
        })
    });

    $('.sort2 li').on('click', function () {
        var member = $('#memberName').val(),
            escort = $('#escortName').val(),
            gender = $('.interestedIn-mobile div.active').attr('data-value'),
            sort = $(this).attr('value'),
            data = {'escort' : escort, 'member': member, 'gender': gender, 'sort': sort};
        getLocation('.loc_id input[type="hidden"]', data);
        $.ajax({
            url:'/reviews/ajax-list',
            type: 'GET',
            data: data,
            beforeSend: function () {
                $('.escorts').LoadingOverlay('show');
            },
            success: function (resp) {
                $('#list').html(resp).LoadingOverlay('hide');
                triggerModalEventsMobile();
                renderModal();
            }
        })
    });


    function triggerModalEvents(){
        var paginger = $('.pageStore').attr('data-paging');
        $('.total-results b').html(paginger);
        $(".full-review-trigger").on("click",function(){
            var review_wrap = $("#modalShowReview");
            var review_id = $(this).data('id'),
                escort_id = $(this).data('escort-id'),
                escort_showname = $(this).data('escort-showname');

            if(!review_id || !escort_id || !escort_showname) return;


            $.ajax({
                url: lang_id + '/reviews/' + escort_showname + '-' + escort_id + '/' + review_id,
                type: 'GET',
                data: {'ajax': 1, 'escort_id':escort_id, 'review_id': review_id, 'showname': escort_showname },
                beforeSend: function(){
                    review_wrap.modal("show").LoadingOverlay('show');
                },
                success: function (resp) {
                    review_wrap.html(resp).LoadingOverlay('hide');

                    renderModal();
                }
            })
        });
    }

    function triggerModalEventsMobile() {
        $('.mobile-full-review').on('click', function () {
            var review_wrap = $("#modalShowReview");
            var review_id = $(this).data('id'),
                escort_id = $(this).data('escort-id'),
                escort_showname = $(this).data('escort-showname');

            $.ajax({
                url: 'reviews/' + escort_showname + '-' + escort_id + '/' + review_id,
                type: 'GET',
                data: {'ajax': 1, 'escort_id':escort_id, 'review_id': review_id, 'showname': escort_showname },
                beforeSend: function(){
                    review_wrap.modal("show").LoadingOverlay('show');
                },
                success: function (resp) {
                    review_wrap.html(resp).LoadingOverlay('hide');
                    renderModal();
                }
            })
        });
    }

    triggerModalEventsMobile();

    triggerModalEvents();
});

function renderModal(){
    grading($('.looks'), 'looks');
    grading($('.services'), 'service');
    grading($('.service_rating'), 'service');

    $('.load_more').on('click', function () {
        if(!$('.additional_reviews').length){
            $(this).html('no more reviews').css('pointer-events', 'none');
            return false;
        }
        $(this).addClass('d-none');
        $('.more_wrapper').removeClass('d-none');
        $('.load_less').removeClass('d-none');
    });

    $('.load_less').on('click', function () {
        $(this).addClass('d-none');
        $('.more_wrapper').addClass('d-none');
        $('.load_more').removeClass('d-none');
    })
}

function ismobile(){
    var newWindowWidth = $(window).width();

    return (newWindowWidth < 768);
}


//Stop Propagation
//-------------------------------------------------------
$('.stop-propagation').on('click', function(e) {
    e.stopPropagation();
});
//-------------------------------------------------------