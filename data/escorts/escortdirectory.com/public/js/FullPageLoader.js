if(typeof headerVars == "object") {

        var FullPageLoader = function () {

        var STATES = {
            PAUSED: 'PAUSED',
            RUNNING: 'RUNNING',
            DESTROYED: 'DESTROYED'
        };

        var stateTexts = ['Loading ...'];
        var stateUpdateInterval = 2000;
        var _interval = null;
        var $container = null;
        var _state = STATES.PAUSED;

        function init() {
            var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
            if (params.hasOwnProperty('texts')) stateTexts = params.texts;
            if (params.hasOwnProperty('interval')) stateUpdateInterval = params.interval;
        }

        function show() {
            $container = $('<div />', {
                'class': 'fpl-wrapper'
            });
            $container.append(getTempalte());
            $('body').append($container);
            _state = STATES.RUNNING;
            setupTicker();
        }

        function getLoaderIcon() {
            return '<svg xmlns="http://www.w3.org/2000/svg" width="60" height="58" viewBox="0 0 135 140" fill="#fff">\n' + '    <rect y="18.3285" width="15" height="103.343" rx="6">\n' + '        <animate attributeName="height" begin="0.5s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite"/>\n' + '        <animate attributeName="y" begin="0.5s" dur="1s" values="10;15;20;25;30;35;40;45;50;0;10" calcMode="linear" repeatCount="indefinite"/>\n' + '    </rect>\n' + '    <rect x="30" y="30.8285" width="15" height="78.343" rx="6">\n' + '        <animate attributeName="height" begin="0.25s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite"/>\n' + '        <animate attributeName="y" begin="0.25s" dur="1s" values="10;15;20;25;30;35;40;45;50;0;10" calcMode="linear" repeatCount="indefinite"/>\n' + '    </rect>\n' + '    <rect x="60" width="15" height="53.343" rx="6" y="43.3285">\n' + '        <animate attributeName="height" begin="0s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite"/>\n' + '        <animate attributeName="y" begin="0s" dur="1s" values="10;15;20;25;30;35;40;45;50;0;10" calcMode="linear" repeatCount="indefinite"/>\n' + '    </rect>\n' + '    <rect x="90" y="30.8285" width="15" height="78.343" rx="6">\n' + '        <animate attributeName="height" begin="0.25s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite"/>\n' + '        <animate attributeName="y" begin="0.25s" dur="1s" values="10;15;20;25;30;35;40;45;50;0;10" calcMode="linear" repeatCount="indefinite"/>\n' + '    </rect>\n' + '    <rect x="120" y="18.3285" width="15" height="103.343" rx="6">\n' + '        <animate attributeName="height" begin="0.5s" dur="1s" values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite"/>\n' + '        <animate attributeName="y" begin="0.5s" dur="1s" values="10;15;20;25;30;35;40;45;50;0;10" calcMode="linear" repeatCount="indefinite"/>\n' + '    </rect>\n' + '</svg>';
        }

        function setupTicker() {
            if (_interval) clearInterval(_interval);
            _interval = setTimeout(function timeout() {
                updateState();
                setTimeout(timeout, stateUpdateInterval);
            }, stateUpdateInterval);
        }

        function destroy() {
            _state = STATES.DESTROYED;
            if (_interval) clearInterval(_interval);

            if (stateTexts.length) {
                stateUpdateInterval = 80;
                setupTicker();
            } else {
                if ($container) $container.remove();
            }
        }

        function updateState() {
            $container.find('.fpl-text').html(getCurrentStateText());

            if (stateTexts.length < 1 && _state === STATES.DESTROYED) {
                destroy();
            }
        }

        function getCurrentStateText() {
            return stateTexts.shift();
        }

        function getTempalte() {
            var visibleText = getCurrentStateText();
            var loaderIcon = getLoaderIcon();
            var html = "<div class=\"fpl-container\">".concat(loaderIcon, " <p class=\"fpl-text\">").concat(visibleText, "</p> </div>");
            return html;
        }

        return {
            init: init,
            show: show,
            destroy: destroy
        };
    }();

}
