var Blog = function (params = {}) {
    this.destructure(params);
    this.construct();
};

Blog.prototype = {

    isBigSliderRunning : false,

    construct: function () {
        this.bindEvents();
        this.showComments({'page' : 1, 'post_id' : $("[name=post_id]").val()});
        this.startMainSlider();
        this.init_current_page();
    },

    init_current_page: function () {
        var pageType = $('#page-type').val();

        switch (pageType) {
            case 'category':
                this.show({page: 1, category_id: $('#category_id').val()});
                break;
            default:
                return false;
        }
    },

    startMainSlider: function() {

        if(this.isBigSliderRunning) return false;
        else isBigSliderRunning = true;

        if($(".main-slider").length <= 0) return console.warn('Slider Not found!');

        $(".main-slider").not('.single').each(function () {
            var x = $(this).find(".image");
            $(this).find(".image-bullets").append("<span class='bullet active'></span>");
            for (i = 1; i < x.length; i++) {
                x[i].style.display = "none";
                $(this).find(".image-bullets").append("<span class='bullet'></span>");
            }
        });

        var interval = null;
        var myIndex = 0;

        var startSliding = function() {

            clearInterval(interval);
            var x = $(".main-slider").find(".image");
            var dots = $(".main-slider").find(".bullet");
            var tick = $(".main-slider").data('tick');

            var activate_slider = function (auto = true) {
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                    dots[i].className = dots[i].className.replace(" active", "");
                }

                if(auto)
                    myIndex++;

                if (myIndex > x.length) {
                    myIndex = 1
                }

                if(x[myIndex - 1])
                    x[myIndex - 1].style.display = "block";

                if(dots[myIndex - 1])
                    dots[myIndex - 1].className += " active";
            };

            $('.blog .main-slider .arrows button')
                .off('click')
                .on('click', function() {
                    var action = $(this).data('action');

                    if(action === 'next') {
                        myIndex++;
                    }else{
                        myIndex--;
                    }

                    if (myIndex > x.length) {
                        myIndex = 1
                    }

                    if (myIndex < 0) {
                        myIndex = x.length - 1;
                    }

                    activate_slider(false);
                });

            activate_slider();
            interval = setInterval(activate_slider, tick);
        };

        startSliding();

        $('.main-slider').hover(function () {
            clearInterval(interval);
        }, function () {
            startSliding();
        });
    },

    destructure: function(params) {
        this.containerPosts = $(params['containerPosts']);
        this.containerAddComment = $(params['containerAddComment']);
        this.containerComments = $(params['containerComments']);
    },

    onCommentFormSubmit: function (e) {
        var $btn = $(e.target),
            $form = $btn.parents('.comment-form');

        var data = $form.serializeArray();

        $.ajax({
            type: 'post',
            url: '/' + headerVars.lang_id + '/blog/ajax-add-comment',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $form.LoadingOverlay('show');
            },
            onError: function() {
                $form.LoadingOverlay('hide');
            },
            success: function (resp) {
                $form.LoadingOverlay('hide');

                if(resp.error) {
                    Blog.prototype.displayErrors($form, resp.error);
                }else if (resp.success) {
                    Notify.alert('success', headerVars.dictionary.your_comment_sent);
                    $form.get(0).reset();
                    Blog.prototype.resetErrors($form);
                }
            }
        });
    },

    onReplyFormSubmit: function(e) {
        e.preventDefault();

        var $form = $(e.target);
        var data = $form.serializeArray();

        $.ajax({
            type: 'post',
            url: '/' + headerVars.lang_id + '/blog/ajax-add-reply',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $form.LoadingOverlay('show');
            },
            onError: function() {
                $form.LoadingOverlay('hide');
            },
            success: function (resp) {
                $form.LoadingOverlay('hide');

                if(resp.error) {
                    Blog.prototype.displayErrors($form, resp.error);
                }else if (resp.success) {
                    Notify.alert('success', headerVars.dictionary.your_comment_sent);
                    $form.get(0).reset();
                    Blog.prototype.resetErrors($form);
                }
            }
        });
    },

    onReplyBtnClick: function(e) {
        var $btn = $(e.target),
            $form = $btn.parents('.comment-wrap').find('.reply-form');

        $('.reply-form').hide();
        $form.show();
    },

    showComments: function(data) {
        var $container = this.containerComments;

        $.ajax({
            url: lang_id + '/blog/ajax-comments',
            type: 'get',
            data: data,
            beforeSend: function() {
                $container.LoadingOverlay('show');
            },
            success: function (resp) {
                $container.html(resp);
                $container.LoadingOverlay('hide');
            }
        });
    },

    resetErrors: function(form) {
          form.find('.invalid').removeClass('invalid');
    },

    displayErrors: function($form, fields) {
        this.resetErrors($form);

        fields.forEach(function(name) {
            $form.find('[name='+name+']').addClass('invalid');
        });
    },

    show: function(data) {

        var $container = $('#posts-container');
        if(typeof data.category_id == "undefined") {
            data.category_id = $('#category_id').val();
        }

        $.ajax({
            url: lang_id + '/blog/ajax-posts',
            method: 'get',
            data: data,
            beforeSend: function() {
              $container.LoadingOverlay('show');
            },
            success: function (resp) {
                $container.html(resp);
                $container.LoadingOverlay('hide');
            }
        });
    },

    bindEvents: function () {
        $('.send-comment-btn').on('click', this.onCommentFormSubmit);

        $(document)
            .on('click', '.open-reply-form', this.onReplyBtnClick)
            .on('submit', '.reply-form', this.onReplyFormSubmit);

    },
};

$(function() {
    Sceon.BLOG = new Blog({
       containerPosts: '#posts-wrap',
       containerAddComment: '#c-form-wrap',
       containerComments: '#com-wrap'
   });
});