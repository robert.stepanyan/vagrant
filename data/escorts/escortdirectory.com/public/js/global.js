//
//  ███████╗███████╗ ██████╗ ██████╗ ██████╗ ████████╗    ██████╗ ██╗██████╗ ███████╗ ██████╗████████╗ ██████╗ ██████╗ ██╗   ██╗
//  ██╔════╝██╔════╝██╔════╝██╔═══██╗██╔══██╗╚══██╔══╝    ██╔══██╗██║██╔══██╗██╔════╝██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗╚██╗ ██╔╝
//  █████╗  ███████╗██║     ██║   ██║██████╔╝   ██║       ██║  ██║██║██████╔╝█████╗  ██║        ██║   ██║   ██║██████╔╝ ╚████╔╝
//  ██╔══╝  ╚════██║██║     ██║   ██║██╔══██╗   ██║       ██║  ██║██║██╔══██╗██╔══╝  ██║        ██║   ██║   ██║██╔══██╗  ╚██╔╝
//  ███████╗███████║╚██████╗╚██████╔╝██║  ██║   ██║       ██████╔╝██║██║  ██║███████╗╚██████╗   ██║   ╚██████╔╝██║  ██║   ██║
//  ╚══════╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═╝   ╚═╝       ╚═════╝ ╚═╝╚═╝  ╚═╝╚══════╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝   ╚═╝
//


// ------------------------------------


// This is temporary code
// Unregister Service workers for PWA
// ----------------------------------
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.getRegistrations()
        .then(function (registrations) {
            for (let registration of registrations) {
                if (registration.active.scriptURL == 'https://www.escortdirectory.com/pwabuilder-sw.js?v3') {
                    registration.unregister();
                }
            }
        });
}
// ----------------------------------


var Sceon = function () {

    function cyrb53(str, seed = 0) {
        let h1 = 0xdeadbeef ^ seed, h2 = 0x41c6ce57 ^ seed;
        for (let i = 0, ch; i < str.length; i++) {
            ch = str.charCodeAt(i);
            h1 = Math.imul(h1 ^ ch, 2654435761);
            h2 = Math.imul(h2 ^ ch, 1597334677);
        }
        h1 = Math.imul(h1 ^ h1 >>> 16, 2246822507) ^ Math.imul(h2 ^ h2 >>> 13, 3266489909);
        h2 = Math.imul(h2 ^ h2 >>> 16, 2246822507) ^ Math.imul(h1 ^ h1 >>> 13, 3266489909);
        return 4294967296 * (2097151 & h2) + (h1 >>> 0);
    };

    function checkForNewPms() {
        $.ajax({
            url: lang_id + '/private-messaging/check-for-new-messages',
            type: 'GET',
            dataType: 'json',
            success: function (resp) {
                if (resp.result) {
                    if (!Cookies.get('pm_new_messages') || resp.count != Cookies.get('pm_new_messages')) {
                        if (resp.count > Cookies.get('pm_new_messages')) {
                            if ($('.contact-threads').length && typeof PrivateMessaging != 'undefined') {
                                PrivateMessaging.LoadThreads();
                                PrivateMessaging.UpdateCurrentUser();
                            }
                        }
                        Cookies.set('pm_new_messages', resp.count)
                    }

                    if (typeof PrivateMessaging != 'undefined' && PrivateMessaging.updateUnreadThreadsCount) {
                        PrivateMessaging.updateUnreadThreadsCount(resp.count);
                    }
                }
            }
        });
        return;
    }

    function add_comment_popup(el, id, comment) {
        if (!headerVars.currentUser) {
            return Popup.login('#login-modal');
        } else {

            if(headerVars.is_member == true) {
                Popup.add_comment(el, id, comment);
            }else{
                Popup.actionNotAvailable();
            }
        }
    }

    function private_message_popup(participant_id) {
        if (!headerVars.currentUser) {
            return Popup.login('#login-modal');
        } else {
            return Popup.pm(participant_id, '#send-pm-modal');
        }
    }

    function private_email_popup(participant_id) {
            return Popup.private_email(participant_id, '#send-private-email-modal');
    }

    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    function debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }

    function isIOSChrome() {
        return window.navigator.userAgent.match("CriOS");
    }

    function isIOS() {
        return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    }

    function isMSIE() {

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        return (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./));
    }

    function isMobile() {
        var check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
        return check;
    }
    

    /**
     * @author Eduard Hovhannisyan :))
     * @returns {string}
     */
    function getLogoASCI() {
        return "\n            \u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2557\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2557 \u2588\u2588\u2588\u2588\u2588\u2588\u2557 \u2588\u2588\u2588\u2588\u2588\u2588\u2557 \u2588\u2588\u2588\u2588\u2588\u2588\u2557 \u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2557    \u2588\u2588\u2588\u2588\u2588\u2588\u2557 \u2588\u2588\u2557\u2588\u2588\u2588\u2588\u2588\u2588\u2557 \u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2557 \u2588\u2588\u2588\u2588\u2588\u2588\u2557\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2557 \u2588\u2588\u2588\u2588\u2588\u2588\u2557 \u2588\u2588\u2588\u2588\u2588\u2588\u2557 \u2588\u2588\u2557   \u2588\u2588\u2557\n            \u2588\u2588\u2554\u2550\u2550\u2550\u2550\u255D\u2588\u2588\u2554\u2550\u2550\u2550\u2550\u255D\u2588\u2588\u2554\u2550\u2550\u2550\u2550\u255D\u2588\u2588\u2554\u2550\u2550\u2550\u2588\u2588\u2557\u2588\u2588\u2554\u2550\u2550\u2588\u2588\u2557\u255A\u2550\u2550\u2588\u2588\u2554\u2550\u2550\u255D    \u2588\u2588\u2554\u2550\u2550\u2588\u2588\u2557\u2588\u2588\u2551\u2588\u2588\u2554\u2550\u2550\u2588\u2588\u2557\u2588\u2588\u2554\u2550\u2550\u2550\u2550\u255D\u2588\u2588\u2554\u2550\u2550\u2550\u2550\u255D\u255A\u2550\u2550\u2588\u2588\u2554\u2550\u2550\u255D\u2588\u2588\u2554\u2550\u2550\u2550\u2588\u2588\u2557\u2588\u2588\u2554\u2550\u2550\u2588\u2588\u2557\u255A\u2588\u2588\u2557 \u2588\u2588\u2554\u255D\n            \u2588\u2588\u2588\u2588\u2588\u2557  \u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2557\u2588\u2588\u2551     \u2588\u2588\u2551   \u2588\u2588\u2551\u2588\u2588\u2588\u2588\u2588\u2588\u2554\u255D   \u2588\u2588\u2551       \u2588\u2588\u2551  \u2588\u2588\u2551\u2588\u2588\u2551\u2588\u2588\u2588\u2588\u2588\u2588\u2554\u255D\u2588\u2588\u2588\u2588\u2588\u2557  \u2588\u2588\u2551        \u2588\u2588\u2551   \u2588\u2588\u2551   \u2588\u2588\u2551\u2588\u2588\u2588\u2588\u2588\u2588\u2554\u255D \u255A\u2588\u2588\u2588\u2588\u2554\u255D \n            \u2588\u2588\u2554\u2550\u2550\u255D  \u255A\u2550\u2550\u2550\u2550\u2588\u2588\u2551\u2588\u2588\u2551     \u2588\u2588\u2551   \u2588\u2588\u2551\u2588\u2588\u2554\u2550\u2550\u2588\u2588\u2557   \u2588\u2588\u2551       \u2588\u2588\u2551  \u2588\u2588\u2551\u2588\u2588\u2551\u2588\u2588\u2554\u2550\u2550\u2588\u2588\u2557\u2588\u2588\u2554\u2550\u2550\u255D  \u2588\u2588\u2551        \u2588\u2588\u2551   \u2588\u2588\u2551   \u2588\u2588\u2551\u2588\u2588\u2554\u2550\u2550\u2588\u2588\u2557  \u255A\u2588\u2588\u2554\u255D  \n            \u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2557\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2551\u255A\u2588\u2588\u2588\u2588\u2588\u2588\u2557\u255A\u2588\u2588\u2588\u2588\u2588\u2588\u2554\u255D\u2588\u2588\u2551  \u2588\u2588\u2551   \u2588\u2588\u2551       \u2588\u2588\u2588\u2588\u2588\u2588\u2554\u255D\u2588\u2588\u2551\u2588\u2588\u2551  \u2588\u2588\u2551\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2557\u255A\u2588\u2588\u2588\u2588\u2588\u2588\u2557   \u2588\u2588\u2551   \u255A\u2588\u2588\u2588\u2588\u2588\u2588\u2554\u255D\u2588\u2588\u2551  \u2588\u2588\u2551   \u2588\u2588\u2551   \n            \u255A\u2550\u2550\u2550\u2550\u2550\u2550\u255D\u255A\u2550\u2550\u2550\u2550\u2550\u2550\u255D \u255A\u2550\u2550\u2550\u2550\u2550\u255D \u255A\u2550\u2550\u2550\u2550\u2550\u255D \u255A\u2550\u255D  \u255A\u2550\u255D   \u255A\u2550\u255D       \u255A\u2550\u2550\u2550\u2550\u2550\u255D \u255A\u2550\u255D\u255A\u2550\u255D  \u255A\u2550\u255D\u255A\u2550\u2550\u2550\u2550\u2550\u2550\u255D \u255A\u2550\u2550\u2550\u2550\u2550\u255D   \u255A\u2550\u255D    \u255A\u2550\u2550\u2550\u2550\u2550\u255D \u255A\u2550\u255D  \u255A\u2550\u255D   \u255A\u2550\u255D   \n        ";
    }

    function parseQueryString(querystring) {
        if(typeof querystring == "undefined") querystring = window.location.href;
        // remove any preceding url and split
        querystring = querystring.substring(querystring.indexOf('?')+1).split('&');
        var params = {}, pair, d = decodeURIComponent;
        // march and parse
        for (var i = querystring.length - 1; i >= 0; i--) {
            pair = querystring[i].split('=');
            params[d(pair[0])] = d(pair[1] || '');
        }

        return params;
    }

    function lazyLoadImages() {
        $('[data-loading="lazy"]').lazy();
    }

    function preventDefault(e) {
        e.preventDefault();
    }

    function preventDefaultForScrollKeys(e) {
        // left: 37, up: 38, right: 39, down: 40,
        // spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
        var keys = {37: 1, 38: 1, 39: 1, 40: 1};

        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }

    // modern Chrome requires { passive: false } when adding event
    var supportsPassive = false;
    try {
        window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
            get: function () {
                supportsPassive = true;
            }
        }));
    } catch (e) {
    }

    var wheelOpt = supportsPassive ? {passive: false} : false;
    var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';
    var scrollElem = $('.content-section');
    // call this to Disable
    function disableScroll() {
        if( document.getElementsByClassName('content-section')[0]) {
            document.getElementsByClassName('content-section')[0].addEventListener('DOMMouseScroll', preventDefault, false); // older FF
            document.getElementsByClassName('content-section')[0].addEventListener(wheelEvent, preventDefault, wheelOpt); // modern desktop
            document.getElementsByClassName('content-section')[0].addEventListener('touchmove', preventDefault, wheelOpt); // mobile
            document.getElementsByClassName('content-section')[0].addEventListener('keydown', preventDefaultForScrollKeys, false);
        }
    }

    // call this to Enable
    function enableScroll() {
        if( document.getElementsByClassName('content-section')[0]){
            document.getElementsByClassName('content-section')[0].removeEventListener('DOMMouseScroll', preventDefault, false);
            document.getElementsByClassName('content-section')[0].removeEventListener(wheelEvent, preventDefault, wheelOpt);
            document.getElementsByClassName('content-section')[0].removeEventListener('touchmove', preventDefault, wheelOpt);
            document.getElementsByClassName('content-section')[0].removeEventListener('keydown', preventDefaultForScrollKeys, false);
        }
    }

    return {
        cyrb53: cyrb53,
        debounce: debounce,
        isIOSChrome: isIOSChrome,
        isIOS: isIOS,
        getLogoASCI: getLogoASCI,
        checkForNewPms: checkForNewPms,
        isMSIE: isMSIE,
        private_message_popup: private_message_popup,
        private_email_popup: private_email_popup,
        add_comment_popup:add_comment_popup,
        parseQueryString: parseQueryString,
        isMobile: isMobile,
        lazyLoadImages: lazyLoadImages,
        enableScroll: enableScroll,
        disableScroll: disableScroll,
    }

}();

var GetParams = (function () {

    var _url;
    var _queryString;
    var _searchParams;

    function regenerate() {
        _url = new URL(window.location.href);
        _queryString = _url.search;
        _searchParams = new URLSearchParams(_queryString);
    }

    /**
     * @param key
     * @param value
     */
    function set(key, value) {
        _searchParams.set(key, value);
        updateUrl();
    }

    /**
     *
     * @param key
     */
    function remove(key) {
        _searchParams.delete(key);
        updateUrl();
    }

    function updateUrl() { 
        var params = _searchParams.toString();
        var baseUrl = '/' + window.location.pathname.replace(/^[\/]+|[\/]+$/g, "");
        window.history.pushState(null, null, baseUrl + '?' + params);
    }

    function convertToObject() {
        var result = {}
        for (var entry of _searchParams.entries()) {
            result[entry[0]] = entry[1];
        }
        return result;
    }

    regenerate();

    return {
        set: set,
        convertToObject: convertToObject,
        regenerate: regenerate,
        remove: remove,
    }
})();

function checkHash() {
    var __elms =  $('.hash-section');

    var hash = document.location.hash.substring(1);

    if ( ! hash.length ) {
        hash = '';
    }

    __elms.each(function(e) {
        var __sizes = e.getSize(),
            __h = parseInt( __sizes.y ),
            __w_scr = $(window).getScroll().y,
            __interval = __w_scr + 200;

        if( __interval > e.getPosition().y ){
            if( __interval < e.getPosition().y + __h ){
                if(e.attr('data-hash') != hash){
                    var scr = document.body.scrollTop;
                    document.location.hash = '#' + e.get('data-hash');
                    document.body.scrollTop = scr;
                }
            }
        }

    });
}
// Addition to jquery
// --------------------------------------
$.fn.textWidth = function (text, font) {
    if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);
    $.fn.textWidth.fakeEl.text(text || this.val() || this.text() || this.attr('placeholder')).css('font', font || this.css('font'));
    return $.fn.textWidth.fakeEl.width();
};
// --------------------------------------


var Validators = function () {

    function init() {
        numbers();
        alpha_numeric();
        numeric_ranges();
    }

    function is_email(str) {
        var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        return re.test(String(str).toLowerCase());
    }

    function alpha_numeric() {
        $(document).on('keypress keydown keyup blur', '.alpha-numeric-only', function () {
            var regex = new RegExp("^[a-zA-Z0-9]+$");
            var btn = !event.charCode ? event.which : event.charCode;
            var key = String.fromCharCode(btn);
            var allowed = [8, 46];

            if (!allowed.includes(btn) && !regex.test(key)) {
                event.preventDefault();
                return false;
            }
        });
    }

    function numbers() {
        $(document).on('keypress keyup blur', '.numbers-only', function () {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        })
    }

    function numeric_ranges() {
        $(document).on('change', '[data-range-from], [data-range-to]', function () {

            var group = $(this).attr('data-range-from') || $(this).attr('data-range-to'),
                $from_elm = $('[data-range-from="' + group + '"]'),
                $to_elm = $('[data-range-to="' + group + '"]'),
                min = parseFloat($from_elm.val()),
                max = parseFloat($to_elm.val());

            $from_elm.find('option').each(function () {
                var val = parseFloat($(this).val());

                if (max && val >= max) {
                    $(this).attr('disabled', true);
                } else {
                    $(this).removeAttr('disabled');
                }
            });

            $from_elm.parent().find('ul.dropdown-menu').find('li a').each(function () {
                var val = $(this).parent().data('val');

                if (!val) val = $(this).find('input').val();

                val = parseFloat(val);

                if (max && val >= max) {
                    $(this).parent().addClass('disabled');
                } else {
                    $(this).parent().removeClass('disabled');
                }
            });

            $to_elm.find('option').each(function () {
                var val = parseFloat($(this).val());
                if (min && val <= min) {
                    $(this).attr('disabled', true);
                } else {
                    $(this).removeAttr('disabled');
                }
            });

            $to_elm.parent().find('ul.dropdown-menu').find('li a').each(function () {
                var val = $(this).parent().data('val');
                if (!val) val = $(this).find('input').val();

                val = parseFloat(val);


                if (min && val <= min) {

                    $(this).parent().addClass('disabled');
                } else {
                    $(this).parent().removeClass('disabled');
                }
            });

        })
    }

    return {
        init: init,
        is_email: is_email
    }
}();


$(document).ready(function () {

	/*if (document.referrer === "") {
		
		function backHijack(){ 
			console.log('scrolllll');
			document.removeEventListener('scroll', backHijack);
			var _url = location.toString().replace(location.hash, "");
		
			history.replaceState(null, document.title,  location.origin + "#!/history");
			history.pushState(null, document.title, _url);
			window.addEventListener("popstate", function () {
				if (location.hash === "#!/history") {
					history.replaceState(null, document.title, location.pathname);
					setTimeout(function () {
						location.replace("/");
					}, 0);
				}
			}, false);
			
		};
		document.addEventListener('scroll',backHijack); 
        
    }*/
    // Lazy loading of images
    // ---------------------------------
    Sceon.lazyLoadImages();
    // ---------------------------------

    // Print the logo into Console Panel
    // ------------------------------------
    console.log(Sceon.getLogoASCI());
    // ------------------------------------

    // Init Validations
    // ------------------------------------
    Validators.init();
    // ------------------------------------

    // Init location selector thing inside Header
    // --------------------------------------------
    if(typeof headerVars == "object") {
        LocationPicker.init();

        var _quickSearch = new QuickSearch(headerVars);
        _quickSearch.init();
    }


    // --------------------------------------------

    // Start the pm message getter
    //-----------------------------------------
    if (typeof headerVars != "undefined" && headerVars.currentUser) {
        Sceon.checkForNewPms();
        setInterval(Sceon.checkForNewPms, 30000);
    }
    //-----------------------------------------
    $('.language-switcher').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).find("ul").slideToggle();
    });

    $(document).on('click', '[data-replace-with]',  function(e) {
         var a = $(this);
         var b = $($(this).data('replace-with'));

         var a_Clone = a.clone();
         var b_Clone = b.clone();

         a.replaceWith(b_Clone);
         b.replaceWith(a_Clone);
    });

    $(".more_cities").on("click", function () {
        $(".cities-dictionary .cities-list-full").slideToggle(250);
        $(".more_arrow").toggleClass("hide");
    });

    $('.flag-row').on('click', function (e) {
        e.stopPropagation();
        e.preventDefault();
        var lng = $(this).find('a').data('lang');
        if (lng.length) {
            if((window.location.pathname).match('/escort/')) {
                var langs = Object.keys(headerVars.langs);
                var url = window.location.pathname;
                var url_filtered = url.split('/').filter(function (el) {
                  return el != '';
                });
                if(langs.includes(url_filtered[url_filtered.length-1])) {
                    url_filtered.pop();
                    window.location = '/' + url_filtered.join('/') + '/' + lng + '/';
                    return;
                }
            }
            var exp = new Date();
            exp.setFullYear(exp.getFullYear() + 1);
            exp = exp.toISOString();
            document.cookie = "ln=" + lng + "; expires=" + exp + "; path=/;domain=." + location.host + ";";
            window.location.reload();
        }
    });

    $('#native-language-picker').click(function(e) {
        e.stopPropagation();
    });

    $('#native-language-picker').on('change', function(e) {
        e.stopPropagation();
        var flag = $(this).prev();
        var lng = $(this).val();
        if (lng.length) {
            flag.removeClass();
            flag.addClass('flag-icon flag-icon-'+lng);
            if((window.location.pathname).match('/escort/')) {
                var langs = Object.keys(headerVars.langs);
                var url = window.location.pathname;
                var url_filtered = url.split('/').filter(function (el) {
                  return el != '';
                });
                if(langs.includes(url_filtered[url_filtered.length-1])) {
                    url_filtered.pop();
                    window.location = '/' + url_filtered.join('/') + '/' + lng + '/';
                    return;
                }
            }
            var exp = new Date();
            exp.setFullYear(exp.getFullYear() + 1);
            exp = exp.toISOString();
            document.cookie = "ln=" + lng + "; expires=" + exp + "; path=/;domain=." + location.host + ";";
            window.location.reload();
        }
    });

    $('.dropdown-menu[data-type="currentCurency"] li').on('click', function (e) {
        e.preventDefault();
        var currency = $(this).find('a').data('currency');
        if (currency.length) {
            Cookies.set('currency', currency);
            window.location.reload();
        }
    });

    $("#submenu-trigger").on("click", function (e) {
        e.stopPropagation();
        $(".header-submenu-container").stop(true, true)
            .slideToggle()
            .toggleClass('open');
    });

    $('html').on("click", function () {
        if ($(".header-submenu-container").css("display") == "block") {
            $("#submenu-trigger").trigger("click");
        }
    });

    $('.not-logged-in').click(function () {
        $('#login-modal').modal('show');
    });

    // Close any dropdown menu, if the user scrolls page
    // ---------------------------------------
    var hide_dropdowns = function () {$('.dropdown-menu.show').removeClass('show')};
    $(window).scroll(hide_dropdowns);
    $('.modal').scroll(hide_dropdowns);
    $('.homepage .filters-mobile').scroll(hide_dropdowns);
    // ---------------------------------------


    $('.agency-website-link').click(function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        window.open(href, '_blank');
    })
    $('.menu-search-item').click(function(e){
        $(".mobile-header-search").slideToggle(200);
        $(".mobile-search-block .search_word").focus();
    })
    var body = $('body');
    $(".advanced_search").on("click", function(){
        $(".mobile-header-search").slideUp(200);
        if (!body.hasClass('no-scroll'))
        {
            body.addClass('no-scroll'); //EDIR-322
        }
        $(".filters-mobile.mobile").css("display", "block");
    });

    $("#adv-search-mobile, #cancel-mobile").click(function () {
        if (body.hasClass('no-scroll'))
        {
            body.removeClass('no-scroll');
        }
    });

    var IS_IPHONE = navigator.userAgent.match(/iPhone/i) != null;

    var form = $('form#advanced-search-form-mobile');

    if(IS_IPHONE)
    {
        form.addClass('iphone-to-bottom');
    }else{
        form.addClass('mobile-to-bottom');
    }

});