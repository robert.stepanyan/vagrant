$(document).ready(function(){
	$('.toggle-password-field').on('click', function () {
        if($(this).find('img').hasClass('tog_vis')) {
            $(this).find('img').attr('src', '/images/visible_password.png').removeClass('tog_vis');
            $(this).prev('input[type="password"]').attr('type', 'text');
        } else {
            $(this).find('img').attr('src', '/images/hidden_password.png').addClass('tog_vis');
            $(this).prev('input[type="text"]').attr('type', 'password');
        }

    });

	var send_login_request = function (e) {

		e.preventDefault();
		var $form = $(e.target),
			$container = $form.parents('.modal-content'),
			data = $(this).serializeArray();

		data.push({name: 'ajax', value: 'true'});
		$container.LoadingOverlay('show');

		$.ajax({
			type: "POST",
			url: $form.attr('action'),
			data: data,
			dataType: "JSON",
			success: function (response) {

				if (typeof response == 'object' && response['status'] == 'error') {
					$('.error-box').empty();

					for(var key in response.msgs){
						$('.error-box').append('<p>' + response.msgs[key] + '</p>');
					}
					$('.error-box').show();

				}
				if (response['status'] == 'success') {
					$('.ajax-login-response').html('<div class=\"alert alert-success\" >' + headerVars.dictionary.login_success + '</div>');
					window.location.reload();
				} else {
					$container.LoadingOverlay('hide');
				}

			}
		}).done(function () {
			$container.LoadingOverlay('hide');
		});
	};

	$('#ajax-login-form').submit(send_login_request);
});