if (typeof headerVars == "object") {


    /**
     * @author Eduard Hovhannisyan
     * @constructor
     */
    var QuickSearch = function () {
        this._searchInterval = null;
        QuickSearch.combineLocationsForEasySearch();
    };

    /**
     * @type {{standardTexts: string[], syntezedLocationFields: null}}
     */
    QuickSearch.GLOBALS = {

        syntezedLocationFields: null,

        standardTexts: [
            headerVars.dictionary.applying_your_locaiton + '...',
            headerVars.dictionary.finding_best_matching_escorts + '...',
            headerVars.dictionary.collecting_escorts_data + '...',
            headerVars.dictionary.filtrating_beautiful_escorts + '...',
            headerVars.dictionary.preparing_to_redirect + '...',
            headerVars.dictionary.organizing_data + '...',
            headerVars.dictionary.researching + '...',
            headerVars.dictionary.almost_done + '...',
        ]
    };

    // Static methods
    // -------------------------------
    QuickSearch.validateDependencies = function () {
        if (typeof FullPageLoader == "undefined") return console.error("Dependency Missing! Please import FullPageLoader.js");
    };

    QuickSearch.fetchUrlToRedirect = function (location) {
        return $.ajax({
            type: 'POST',
            url: '/geography/get-city-page/',
            dataType: 'JSON',
            data: location
        });
    }

    QuickSearch.getMatchingLocations = function (query) {
        var result = [];
        var reg = new RegExp(query, 'gi');

        if (!query.length) return result;

        query = query.toLowerCase();

        for (let key in QuickSearch.GLOBALS.syntezedLocationFields) {

            if (key.toLowerCase().includes(query)) {
                if (QuickSearch.GLOBALS.syntezedLocationFields[key].escortsCount > 0) {
                    let title = key.replace(reg, function (str) {
                        return '<span style="color: green"><b>' + str + '</b></span>'
                    });

                    result.push({
                        title: title,
                        id: QuickSearch.GLOBALS.syntezedLocationFields[key].id,
                        type: QuickSearch.GLOBALS.syntezedLocationFields[key].type
                    });
                }
            }

            if (result.length >= 30) break;
        }
        return result;
    };

    QuickSearch.constructMatchedLocations = function (items) {

        var tpl = '<ul>';

        if (!items) return;

        items.forEach(function (item) {
            tpl += '<li data-type="' + item.type + '" data-id="' + item.id + '">' + item.title + '</li>';
        });

        tpl += '</ul>';

        return tpl;
    };

    QuickSearch.combineLocationsForEasySearch = function () {

        if (!QuickSearch.GLOBALS.syntezedLocationFields) {

            QuickSearch.GLOBALS.syntezedLocationFields = {};


            for (var countryId in headerVars.countries) {
                var country = headerVars.countries[countryId];
                var countryEscortsCount = country.escortsCount;

                QuickSearch.GLOBALS.syntezedLocationFields[country.title] = {id: countryId, type: 'country', 'escortsCount' : countryEscortsCount};

                if (country.regions) {
                    for (var regionId in country.regions) {
                        var region = country.regions[regionId];
                        var regionEscortsCount = region.escortsCount;

                        QuickSearch.GLOBALS.syntezedLocationFields[region.title] = {id: regionId, 'type': 'region', 'escortsCount' : regionEscortsCount};

                        for (var cityId in region.cities) {
                            var city = region.cities[cityId];
                            var cityEscortsCount = city.escortsCount;

                            QuickSearch.GLOBALS.syntezedLocationFields[city.title] = {id: cityId, 'type': 'city', 'escortsCount' : cityEscortsCount};
                        }
                    }
                } 
				if(country.cities){

                    for (var cityId in country.cities) {
                        var city = country.cities[cityId];
                        var escortsCount = city.escortsCount;

                        if(country.hasOwnProperty('iso') === false) continue;

                        if(QuickSearch.GLOBALS.syntezedLocationFields[city.title]) {
                            QuickSearch.GLOBALS.syntezedLocationFields[city.title + ', ' + country.iso.toUpperCase()] = {id: cityId, 'type': 'city', 'escortsCount' : escortsCount};
                        }else{
                            QuickSearch.GLOBALS.syntezedLocationFields[city.title] = {id: cityId, 'type': 'city', 'escortsCount' : escortsCount};
                        }
                    }
                }
            }
        }
    };

    QuickSearch.applyLocation = function (location = null) {

        // Generate random texts from standard messages
        // ------------------------
        var shuffled = QuickSearch.GLOBALS.standardTexts.sort(function () {
            return 0.5 - Math.random()
        });
        var selected = shuffled.slice(0, 2);
        // ------------------------

        FullPageLoader.init({
            'texts': selected.concat([
                headerVars.dictionary.few_more_steps + '...',
                headerVars.dictionary['done'] + '!',
            ]),
            'interval': 1500
        });
        FullPageLoader.show();


        QuickSearch.fetchUrlToRedirect(location)
            .done(function (resp) {

                if (resp.url) {
                    window.location.href = resp.url;
                } else {
                    FullPageLoader.destroy();
                    alert("This Location is not available yet.");
                }
            })
            .fail(function () {
                FullPageLoader.destroy();
                alert("This Location is not available yet.");
            });
    }
    // -------------------------------

    QuickSearch.prototype.init = function () {

        QuickSearch.validateDependencies();

        this.bindEvents();
    }

    /**
     * @param show
     * @param html
     */
    QuickSearch.prototype.updateResultList = function (show, html = null) {

        var $wrapper = $('#quick-search-result');
        if (show) {
            $wrapper.show();
        } else {
            $wrapper.hide();
        }

        $wrapper.html(html);
    }

    /**
     * @returns {string}
     */
    QuickSearch.detectCurrentPageCategory = function() {

        var category = 'escorts';
        var definedCategories = ['bdsm', 'citytours', 'massage'];

        var i = definedCategories.length;
        while (i--) {
            if(location.pathname.includes(definedCategories[i])) {
                category = definedCategories[i];
                break;
            }
        }

        return category;
    }

    /**
     * @type {{onSearchStart: QuickSearch.listeners.onSearchStart, onResultSelect: QuickSearch.listeners.onResultSelect}}
     */
    QuickSearch.prototype.listeners = {

        onResultSelect: function (e) {
            var selected = $(e.currentTarget);

            $('#quick-search-result').empty();
            $('#location-name-inp').val(selected.text());

            var location = {
                type: selected.data('type'),
                id: selected.data('id'),
                category : QuickSearch.detectCurrentPageCategory()
            }

            QuickSearch.applyLocation(location);
        },

        onSearchStart: function () {

            var self = this;

            if (self._searchInterval) {
                clearInterval(this._searchInterval);
            }

            self._searchInterval = setTimeout(function () {

                var query = $('#location-name-inp').val();
                var items = QuickSearch.getMatchingLocations(query);

                var html = QuickSearch.constructMatchedLocations(items);
                self.updateResultList(true, html);

            }, 300);
        },
    };

    /**
     * Binds all the events to dom elements
     */
    QuickSearch.prototype.bindEvents = function () {
        $('#quick-search-result').on('click', 'li', this.listeners.onResultSelect);
        $('#location-name-inp').on('input', this.listeners.onSearchStart.bind(this));
    };


// ---------------------------------------------
    var LocationPicker = function () {

        var $countryDropdown = null;
        var $cityDropdown = null;
        var $popoverBtn = null;
        var $popoverContainer = null;
        var $locationListingWrapper = null;

        function init() {

            if (typeof FullPageLoader == "undefined") return console.error("Dependency Missing! Please import FullPageLoader.js")

            generateRefs();
            fillDropdownWithCountries();
            fillMobileLocationsListing();
            setupDropdowns();
            bindEvents();
        }

        function generateRefs() {
            $locationListingWrapper = $('.locations-listing');
            $countryDropdown = $('.dropdown-my-location.country select');
            $cityDropdown = $('.dropdown-my-location.city select');
            $regionDropdown = $('.dropdown-my-location.region select');
            $popoverBtn = $('#my-location-btn');
            $popoverContainer = $('#my-location-picker-wrapper');
        }

        var Listeners = {
            onCountryChange: function () {
                var countryId = $countryDropdown.val();

                initCitiesDropdown([]); // to clear them
                initRegionsDropdown([]); // to clear them

                if (!headerVars.countries[countryId]) return;

                if (headerVars.countries[countryId].hasOwnProperty('regions') && headerVars.countries[countryId].regions)
                    return initRegionsDropdown(Object.values(headerVars.countries[countryId].regions));

                if (headerVars.countries[countryId].hasOwnProperty('cities') && headerVars.countries[countryId].cities)
                    initCitiesDropdown(Object.values(headerVars.countries[countryId].cities));
            },

            onRegionChange: function () {
                var countryId = $countryDropdown.val();
                var regionId = $regionDropdown.val();

                initCitiesDropdown(Object.values(headerVars.countries[countryId].regions[regionId].cities));
            },

            onLocationApply: function () {

                var location = getSelectedLocation();
                if (!location.id) return alert('Please select at least one country');

                // Generate random texts from standard messages
                // ------------------------
                var shuffled = QuickSearch.GLOBALS.standardTexts.sort(function () {
                    return 0.5 - Math.random()
                });
                var selected = shuffled.slice(0, 2);
                // ------------------------

                FullPageLoader.init({
                    'texts': selected.concat([
                        headerVars.dictionary.few_more_steps + '...',
                        headerVars.dictionary['done'] + '!',
                    ]),
                    'interval': 1500
                });
                FullPageLoader.show();

                QuickSearch.fetchUrlToRedirect(location)
                    .done(function (resp) {

                        if (resp.url) {
                            window.location.href = resp.url;
                        } else {
                            FullPageLoader.destroy();
                            alert("This Location is not available yet.");
                        }
                    })
                    .fail(function () {
                        FullPageLoader.destroy();
                        alert("This Location is not available yet.");
                    });
            },

            onLocationListCollapse: function (e) {
                e.stopPropagation();

                if ($(this).has('ul').length) {
                    $(this).parents('ul').first().find('li.collapsed').not($(this)).removeClass('collapsed');
                    $(this).toggleClass('collapsed');
                } else {

                    var location = {
                        id: $(this).data('id'),
                        type: $(this).data('type'),
                        category : QuickSearch.detectCurrentPageCategory()
                    };

                    QuickSearch.applyLocation(location);
                }
            },
        };

        function fillMobileLocationsListing() {

            var tpl = '<ul>';

            Object.values(headerVars.countries).sort(function (a, b) {
                return a.title > b.title
            }).forEach(function (country) {

                tpl += '<li data-type="country" data-id="' + country.id + '"><b>' + country.title + ' </b> <i class="fas fa-chevron-up"></i>';

                if (country.regions) {

                    tpl += '<ul>';

                    Object.values(country.regions).sort(function (a, b) {
                        return a.title > b.title
                    }).forEach(function (region) {

                        tpl += '<li data-type="region" data-id="' + region.id + '"><b>' + region.title + '</b> <i class="fas fa-chevron-up"></i>';
                        tpl += '<ul>';
                        if (region.cities) {
                            Object.values(region.cities).forEach(function (city) {
                                tpl += '<li data-type="city" data-id="' + city.id + '">' + city.title + '</li>';
                            });
                        }
                        tpl += '</ul>';
                        tpl += '</li>';
                    });

                    tpl += '</ul>';

                } else {

                    if (country.cities) {

                        tpl += '<ul>';
                        Object.values(country.cities).sort(function (a, b) {
                            return a.title > b.title
                        }).forEach(function (city) {
                            tpl += '<li data-type="city" data-id="' + city.id + '">' + city.title + '</li>';
                        });
                        tpl += '</ul>';
                    }

                }

                tpl += '</li>';

            });


            tpl += '</ul>';

            $locationListingWrapper.append(tpl);
        }

        function getSelectedLocation() {

            var location = {};

            if ($cityDropdown.val()) {
                location.type = 'city';
                location.id = $cityDropdown.val();
            } else if ($regionDropdown.val()) {
                location.type = 'region';
                location.id = $regionDropdown.val();
            } else if ($countryDropdown.val()) {
                location.type = 'country';
                location.id = $countryDropdown.val();
            }

            location.category = QuickSearch.detectCurrentPageCategory();

            return location;
        }

        function hidePicker() {
            $popoverContainer.hide();
        }

        function showPicker() {
            $popoverContainer.show();
            $('#location-name-inp').focus();
        }

        function togglePicker() {
            $popoverContainer.toggle();

            if ($popoverContainer.is(':visible')) {
                $('#location-name-inp').focus();
            }
        }

        function bindEvents() {
            $countryDropdown.on('select2:select', Listeners.onCountryChange);
            $regionDropdown.on('select2:select', Listeners.onRegionChange);
            $('.close-picker').on('click', hidePicker);
            $popoverBtn.on('click', togglePicker);
            $('.apply-my-location-btn').on('click', Listeners.onLocationApply);
            $('#my-location-picker-wrapper .locations-listing > ul li').on('click', Listeners.onLocationListCollapse);
        }

        function initRegionsDropdown(regions) {
            $regionDropdown.empty();

            if (regions.length < 1) {
                return $regionDropdown.parents('.form-group')
                    .hide();
            }

            $regionDropdown.append(new Option(headerVars.dictionary.nothing_selected, ''));

            regions.sort(function (a, b) {
                return a.title > b.title
            }).forEach(function (region) {
                $regionDropdown.append(new Option(region.title, region.id, false, false));
            });

            $regionDropdown.trigger('change')
                .parents('.form-group')
                .show();

            $cityDropdown.empty()
                .trigger('change')
                .parents('.form-group')
                .hide();

        }

        function initCitiesDropdown(cities) {
            $cityDropdown.empty();

            if (cities.length < 1) {
                return $cityDropdown.parents('.form-group')
                    .hide();
            }

            $cityDropdown.append(new Option(headerVars.dictionary.nothing_selected, ''));
            cities.sort(function (a, b) {
                return a.title > b.title
            }).forEach(function (city) {
                $cityDropdown.append(new Option(city.title, city.id, false, false));
            })

            $cityDropdown.parents('.form-group').show();
            $cityDropdown.trigger('change');
        }

        function fillDropdownWithCountries() {
            var tpl = '<option value="">' + headerVars.dictionary.nothing_selected + '</option>';

            for (var id in headerVars.countries) {
                var country = headerVars.countries[id];
                tpl += '<option value="' + country['id'] + '">' + country['title'] + '</option>';
            }

            $countryDropdown.html(tpl);
        }

        function setupDropdowns() {

            var params = {
                width: '100%'
            };

            $countryDropdown.select2(params);
            $cityDropdown.select2(params);
            $regionDropdown.select2(params);
        }

        return {
            init: init
        }
    }();


}