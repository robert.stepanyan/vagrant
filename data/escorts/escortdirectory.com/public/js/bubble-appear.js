'use strict';


function loadBubbleTexts(countryId) {
        $.ajax({
            url: '/ajax-bubble?page=1&per_page=8',
            type: 'GET',
            dataType: "html",
            data: {page: 1, country_id: countryId, ajax: true},
            success: function (resp) {
                $('#status-messages').append(resp);
            },
            complete: function () {
                $('.bubble-page').LoadingOverlay("hide", true);
            }
        });
}

$(document).ready(function () {
    var countryId = '';
    var elastiCountrySearch = $('.elastic-country');
    if (elastiCountrySearch && elastiCountrySearch.length > 0) {
        var elasticCountry = [];
        elastiCountrySearch.each(function (index, value) {
            elasticCountry.push($(this).val());
        });
        elasticCountry = $.unique(elasticCountry);
        if (elasticCountry.length <= 1)
        {
            countryId = elasticCountry[0];
        }
    } else {
        var pageCountry = $('#current-page-country').data('id');
        var userCoutry = $('#user_country_id').data('id');
        if (pageCountry) {
            countryId = pageCountry;
        } else {
            countryId = userCoutry;
        }
    }
    if (countryId && countryId !== '') {
        loadBubbleTexts(countryId);
    }
});