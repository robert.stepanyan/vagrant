;(function (window, document) {

    function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
            textbox.addEventListener(event, function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        });
    };

    function submitPaymentForm(data) {

        $('.payment-form__wrapper').addClass('processing');

        $.ajax({
            url: '/payment/checkout',
            data: data,
            type: 'post',
            success: function (response) {
                response = JSON.parse(response);

                $('.payment-form__wrapper').removeClass('processing');

                console.log(response);
                switch (response.status) {
                    case 'mmg_success':
                        return location.href = response.url;
                        break;

                    case 'error':
                        alert('Currently payment system is not available, please check back soon. ' + (response.error ? 'Details ' + response.error : ''));
                        break;
                }

            }
        });
    }


    document.addEventListener("DOMContentLoaded", function () {

        var numbersOnlyInputs = document.querySelectorAll("[data-validate='true'][data-accept='number']");

        numbersOnlyInputs.forEach(function ($element) {
            setInputFilter($element, function (value) {
                return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
            });
        });

        $('#pay-btn').on('click', function () {

            var data = {
                amount: $('#amount-inp').val(),
                paymentMethod: $('[name="paymentMethod"]:checked').val(),
            };

            if (data.amount <= 0) {
                return alert("Invalid Amount specified");
            }

            if (!['mmg'].includes(data.paymentMethod)) {
                return alert("Invalid PaymentMethod specified");
            }

            submitPaymentForm(data);
        });

    });

})(this, document);