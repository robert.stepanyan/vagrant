// Searchbar and Ordering logic
// ------------------------------------------------
var Adds = (function () {

    var escort_id = null;
    var pending_requests = {
        getting_cities: null
    };
    var templates = {
        loading: function () {
            return "Loading ...";
        },
        mobile_loading: function mobile_loading() {
            return "<li class='disabled'><a><label>Loading ...</label></a></li>";
        }
    };

    function init() {
        bind_events();
    }

    function sync_mobile_filters() {
        var city_id = $('#classified-city-search-mobile input:checked').val();
        $('.searchbar select[name="city_id"]').empty();

        if(city_id) {
            $('.searchbar select[name="city_id"]')
                .append("<option value='"+city_id+"'></option>")
                .val( city_id );
        }

        $('.searchbar select[name="country_id"]').val( $('.searchbar.mobile [name="country_id"]:checked').val() );
        $('.searchbar.desktop #category').val( $('.searchbar.mobile .category-mobile input:checked').val() );
        $('.searchbar.desktop #text').val( $('#text-mob').val() )
    }

    function set_filter(filter, isMobile = false) {
        
        if(isMobile) {
            sync_mobile_filters();
        }

        var hash = make_hash(filter);
        set_hash(hash);
        $(window).trigger('hashchange');
    }

    function set_hash(hash) {

        if (hash.length) {
            window.history.pushState(null, '', '?' + hash);
        } else {
            window.history.pushState(null, '', null);
        }
    }

    function make_hash(filter) {
        var hash = '';
        var sep = '&';
        var value_glue = ',';
        var map = {};

        var hidden_inputs = $('.searchbar input[type=hidden]');
        hidden_inputs.each(function (input) {
            if ($(this).attr('name') && $(this).attr('name')) {
                var index = $(this).attr('name');
                map[index] = $(this).val();
            }
        });

        var input_texts = $('.searchbar input[type=text]');
        input_texts.each(function (input) {
            if ($(this).val().length > 0 && $(this).attr('name')) {
                var index = $(this).attr('name');
                map[index] = $(this).val();
            }
        });

        var input_checkbox = $('.searchbar input[type=checkbox]:checked');
        input_checkbox.each(function (input) {
            if ($(this).val().length > 0 && $(this).attr('name')) {
                var index = $(this).attr('name');
                map[index] = $(this).val();
            }
        });

        var selects = $('.searchbar select');
        selects.each(function (input) {
            if ($(this).val() && $(this).attr('name')) {
                var index = $(this).attr('name');
                map[index] = $(this).val();
            }
        });

        map = Object.assign({}, map, filter);

        for (var i in map) {
            if(map[i] || map[i].length)
                hash += i + '=' + map[i] + sep;
        }

        return hash;
    }

    function parse_hash() {
        var hash = document.location.hash.substring(1),
            filter = '',
            params = hash.split(';'),
            not_array_params = ['page', 'category', 'city_id', 'text', 'per_page', 'country_id', 'date_f', 'date_t', 'with_photo', 'sort'];

        if (!hash.length) {
            return {};
        }

        params.forEach(function (param) {
            var key_value = param.split('=');
            var key = key_value[0];
            var val = key_value[1];

            if (val === undefined) return;

            if (key == 'text') {
                $('#text').val(val);
            } else if (key == 'country_id') {
                $('#country').val(val);
            } else if (key == 'city_id') {
                $('#city').val(val);
            } else if (key == 'zone_id') {
                $('#zone_id').val(val);
            } else if (key == 'zone_id') {
                $('#zone_id').val(val);
            } else if (key == 'category') {
                $('#category').val(val)
            } else if (key == 'sort') {
                $('#sort-inp').val(val)
            }

            val = val.split(',');

            val.forEach(function (it) {
                filter += key + ((!not_array_params.includes(key)) ? '[]=' : '=') + it + '&';
            });
        });

        return filter;
    }

    function load_ads(params) {

        var $container = $('#classified-list'),
            filters = parse_hash(),
            url = lang_id + '/classified-ads/ajax-list' + window.location.search;

        if ( filters.length ) {
            filters = Sceon.parseQueryString(filters);
        }

        $container.LoadingOverlay('show');

        $.ajax({
            url: url,
            type: 'get',
            //data: Object.assign({}, {escort_id: escort_id}, filters || {}),
            success: function (resp) {
                $container.html(resp);
                $container.LoadingOverlay('hide');
                $('.to-the-moon').trigger('click');
            }
        });
    }

    function on_hash_change() {
        var data = parse_hash();
        load_ads(data);
    }
    
    function sort_adds() {
        var sort = $(this).val();
        $('#sort-inp').val(sort);
        set_filter({page: 1, sort: sort});
    }

    function searchbar_country_updated() {
        var inputs = ['country_id', 'region_id', 'city_id', 'zone_id'];

        setTimeout(function() {
            inputs.forEach(function(id) {
                $('#' + id + '-inp').val( $('#' + id).val() );
            })
        }, 300)
    }

    function construct_dropdown(params) {
        try {
            var cities = JSON.parse(params.regions);
            var visible_html = "<ul class=\"sub-dropdown-menu pl-1\" style=\"list-style: none\"> ";
            var data_html = "";

            cities.data.forEach(function(city) {
                data_html += "<option value=\"".concat(city.id, "\">").concat(city.title, "</option>");
                visible_html += "<li data-val=\"".concat(city.id, "\"><a href=\"javascript:void(0)\">").concat(city.title, "</a></li>");
            });
            visible_html += "</ul> ";

            params.select.html(data_html).val(null); // This is only <option> s
            params.ul.html(visible_html); // This one is for ul>li that is visible for user
        } catch (e) {
            console.warn(e.message);
        }
    }

    function construct_mobile_dropdown(params) {
        try {
            var cities = JSON.parse(params.regions);
            var visible_html = "<ul class=\"sub-dropdown-menu pl-1\" style=\"list-style: none\"> ";

            cities.data.forEach(function(city) {
                visible_html += `<li>
                    <label class="radio" title="${city.title}">
                        <input type="radio" name="country_id" value="${city.id}"> <a>${city.title} </a>
                    </label>
                </li>`;
            });
            visible_html += "</ul> ";


            params.dropdown.find('ul').html(visible_html); // This is only <option> s
        } catch (e) {
            console.warn(e.message);
        }
    }

    function on_country_changed() {
        console.log($(this).data('type'))
        if( $(this).data('type') == 'searchbar' ) {
            var $country_select = $("#classified-search-country"),
                $cities_select = $("#classified-search-city"),
                type = 'classified-city-search-select';

            $cities_select.empty();
            $cities_select.siblings('ul[data-type="classified-city-search-select"]').empty().append(templates.loading());
            $cities_select.siblings('.dropdown-toggle').find('.dropdown-toggle-val').html(headerVars.dictionary.nothing_selected_html);

        }else{
            var $country_select = $("#classified-country"),
                $cities_select = $("#classified-city"),
                type = 'classified-city-select';


            $cities_select.empty();
            $cities_select.siblings('ul[data-type="'+type+'"]').empty().append(templates.loading());
            $cities_select.siblings('.dropdown-toggle').find('.dropdown-toggle-val').html(headerVars.dictionary.nothing_selected_html);
        }

        $.ajax({
            url: '/geography/ajax-get-cities-classified-ads?json=true',
            data: {
                country_id: $country_select.val(),
                type: type
            },
            success: function (r) {

                construct_dropdown({
                    regions: r,
                    select: $cities_select,
                    ul: $cities_select.siblings('ul[data-type="'+type+'"]'),
                })

            }
        });
    }
    
    function on_mobile_country_changed() {

        if(pending_requests.getting_cities)  pending_requests.getting_cities.abort();

        var $cities_dropdown = $("#classified-city-search-mobile"),
            type = 'classified-city-search-mobile';

        $cities_dropdown.find('ul').empty().append(templates.mobile_loading());
        $cities_dropdown.find('.dropdown-toggle-val').html(headerVars.dictionary.nothing_selected_html);

        pending_requests.getting_cities = $.ajax({
            url: '/geography/ajax-get-cities-classified-ads?json=true',
            data: {
                country_id: $('#classified-country-search-mobile input:checked').val()
            },
            success: function (r) {

                construct_mobile_dropdown({
                    regions: r,
                    dropdown: $cities_dropdown,
                })

            }
        }).done(function() {
            pending_requests.getting_cities = null
        });
    }

    function update_preview_content() {

        // Collecting all data from FORM
        //------------------------------------------------
        var $container = $('#modalAddAd .form-wrapper');

        var country = $container.find('#classified-country-select').html();
        var city = $container.find('#classified-city-select').html();
        var category = $('#classified-category').val() ? $container.find('#category-select').text() : '- - -';

        var phone = $container.find('#classified-phone').val() || '- - -';
        var email = $container.find('#classified-email').val() || '- - -';

        var title = $container.find('#classified-title').val() || '- - -';
        var text = $container.find('#classified-text').val() || '- - -';

        var images = '';

        $container.find('.add-images .image').each(function () {
            var elm = $(this).clone();
            elm.find('.remove-image').remove();
            images += elm[0].outerHTML;
        });
        //------------------------------------------------

        // Put data into preview wrapper
        //------------------------------------------------
        var $preview_container = $('#modalAddAd .preview-wrapper');

        $preview_container.find('.images').html(images);
        $preview_container.find('.category').html(category);
        $preview_container.find('.location').html([country, city].join(','));
        $preview_container.find('.phone a').html(phone);
        $preview_container.find('.ad-title').html(title);
        $preview_container.find('.ad-text').html(text);

        //------------------------------------------------

    }

    function toggle_searchbar() {
        var $searchbar = $('.searchbar .searchbar-wrapper');
        $searchbar.toggleClass('show');

        if($searchbar.hasClass('show')) {
            $('#countryInput').focus()
        }

    }

    function reset_searchbar() {
        $('.searchbar form').get(0).reset();
        $('.btn-dropdown-rounded.active').removeClass('active');
        $('#countryInput').val('');
        $('#text-mob').val('');

        $('#classified-country-search-mobile .multiselect-selected-text').html(headerVars.dictionary.nothing_selected_html);
        $('#classified-city-search-mobile .multiselect-selected-text').html(headerVars.dictionary.nothing_selected_html);

        if(Sceon.isMobile()) {
            $('#classified-country-search-mobile input:checked').removeAttr('checked');
            $('#classified-city-search-mobile input:checked').removeAttr('checked');
        }else{
            $('.searchbar [name="zone_id"]').val(null);
            $('.searchbar [name="city_id"]').val(null);
            $('.searchbar [name="region_id"]').val(null);
            $('.searchbar [name="country_id"]').val(null);
        }
    }

    function switch_modal_panel(e) {
        e.preventDefault();
        update_preview_content();
        $('#modalAddAd .modal-content').toggleClass('preview-ad');
    }

    function hide_mobile_searchbar() {
        $('.searchbar .searchbar-wrapper').removeClass('show');
        var country = $('#classified-country-search-mobile .dropdown-toggle-val').text();
        var city = $('#classified-city-search-mobile .dropdown-toggle-val').text();

        var is_country_selected = $('#classified-country-search-mobile input:checked').val();
        var is_city_selected = $('#classified-city-search-mobile input:checked').val();

        if(is_country_selected) {
            var location = country;

            if(is_city_selected)
                location += ', ' + city;

            $('#myCountry-mobile-place').html(location);
        }else {
            $('#myCountry-mobile-place').html(headerVars.dictionary.city_region_or_country);
        }


        $('body').removeClass('no-more-scroll');
        set_filter({page: 1}, true);
    }

    function bind_events() {
        $(window).on('hashchange', on_hash_change)

        $("#cancel-mobile").on("click", hide_mobile_searchbar);
        $('select[name=sort]').on('change', sort_adds);
        $('.searchbar #filter-country-input').on('change', searchbar_country_updated);
        $('#classified-country, #classified-search-country').on('change', on_country_changed);
        $('.preview-ad-btn').on('click', switch_modal_panel);
        $('.searchbar-trigger').on('click', toggle_searchbar);
        $('.dismiss-searchbar').on('click', reset_searchbar);
        $('.search-button-trigger.mobile').on('click', hide_mobile_searchbar);
        $('#classified-country-search-mobile input:radio').on('change', on_mobile_country_changed);
    }

    return {
        init: init,
        load_ads: load_ads,
        set_filter: set_filter
    }
})();
// ------------------------------------------------


$( document ).ready(function() {



    // Listen to changes of Url and
    // update the content
    // ------------------------------------
    Adds.init();
    // ------------------------------------

    // Thanks to this bunch of code,
    // the selected item in dropdown, goes to THE FUCKING SELECT
    // ------------------------------------
    $(document).on("click", ".dropdown-menu li a", function () {
        if($(this).parents('.dropdown').hasClass('disabled')) return;
        if($(this).parent().hasClass('disabled') || $(this).hasClass('disabled')) return;

        var val = $(this).html();
        var type = $(this).parents(".dropdown-menu").attr("data-type");
        $("#" + type).find(".dropdown-toggle-val")
            .html(val)
            .removeClass('pristine');

        $(this).parents('div.dropdown').first().find('select').val($(this).parent().data('val')).trigger('change');
    });
    // ------------------------------------


    $(".escort-details").each(function(){

        text = $(this).find("p").html();
        link = $(this).parents(".escort-item").find("a").attr("href");
        len = 293;

        var ad_id = $(this).data('id');
        var ad_slug = $(this).data('slug');

        if (ismobile()) {
            len = 135;
        }

        if((text.length > text.substring(0, len).length)){
            text = text.substring(0, len - 3) + '...';

            if (ismobile()) {
                text = text + '<a href="#" class="grey full-add-trigger" data-id="' + ad_id + '" data-slug="' + ad_slug + 
                '">Read more</a>';
            }
        }
        
        // return text; 
        $(this).find("p").html(text);
    });

    $(".ad-title").each(function(){
        
        text = $(this).html();
        len = 122;

        if (ismobile()) {
            len = 45;
        }

        if((text.length > text.substring(0, len).length)){
            text = text.substring(0, len - 3) + '...';
        }
        
        // return text; 
        $(this).html(text);
    });

    $(".upload-trigger").on("click",function(){
        $("#upload_materials").trigger("click");
    });

    function removeDropdownSelected(obj){
        var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
        $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
            $(this).show();
        });
        $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
            if ($(this).find("a").html() == val) {
                $(this).hide();
            }
        });
    }

    $(".dropdown-toggle").each(function(){
        removeDropdownSelected(this);
    });

    $('.classified-ads').on('click', ".dropdown-menu li a", function(){
        var val = $(this).html();
        var type = $(this).parents(".dropdown-menu").attr("data-type");
        $("#" + type).find(".dropdown-toggle-val").html(val);

        var $checkbox = $(this).find('input[type=radio]');
        if($checkbox.length) {
            $(this).parents('ul.dropdown-menu')
                .first()
                .find('input[type=radio]')
                .removeAttr('checked');
            $checkbox.attr('checked', true);
        }

        if(type == 'category-select') {
            $('#modalAddAd').find('#classified-category').val($(this).data('val'));
        }

        if(type == 'duration-select') {
            $('#modalAddAd').find('#classified-duration').val($(this).data('val'));
        }

        if($checkbox.length) {
            $checkbox.trigger('change');
        }

        removeDropdownSelected(this);
    });

    $(".advanced-search").on("click",function(){
        $(this).html() == headerVars.dictionary.advanced_search ? $(this).html(headerVars.dictionary.basic_search) : $(this).html('Advanced search');

        if (ismobile()) {
            $(".advanced-buttons").stop(true, true).toggleClass("active");
        }

        $(".advanced-search-container").stop(true, true).slideToggle();
    });

    $("#cancel").on("click",function(){
        $(".advanced-search").trigger("click");
    });

    
    $(".full-review-trigger").on("click",function(){
        $("#modalShowReview").modal("show");
    });

    // var labels = [];
    // $('.multiple-dropdown-box').multiselect({ 
        
    //     onChange: function(option, checked, select){
    //         var index = labels.indexOf($(option).val());
    //         if (index !== -1) {
    //             labels.splice(index, 1);
    //         } else {
    //             labels.push($(option).val());
    //         }
    //     },
    //     buttonText: function(obj) {
    //         if (labels.length === 0) {
    //             return 'Nothing selected';
    //         }
    //         else if (labels.length >= 2) {
    //             return labels[0]+ ', +' + (labels.length - 1);
    //         }
    //         else if(labels.length >= 1){
    //             return labels;
    //         }
    //     }
    // });


    $(".view-buttons").on("click",function(){
        $(".view-buttons").removeClass("active");
        $(this).addClass("active");

        if ($(this).attr("data-type") == "view-squares") {
            $(".escorts-list").removeClass("view-list").addClass("d-flex view-squares");
            $(".escort-item").removeClass("d-flex");
        } else {
            $(".escorts-list").removeClass("d-flex view-squares").addClass("view-list");
            $(".escort-item").addClass("d-flex");
        }
    });

    $(".intentions-container-mobile").on("click",function(){
        $(".filters-mobile").stop(true, true).fadeToggle();
        $('body').addClass('no-more-scroll');
    });


    $(".btn-dropdown-rounded").on("click",function(){
        $(this).parent(".d-flex").find(".btn-dropdown-rounded").removeClass("active");
        $(this).addClass("active");
    });

    $(".search-mobile").on("click",function(){
        var country = $("#filter-country-input-mobile").val();
        if (country !== "") {
            $("#myCountry-mobile-place").html(country);
        }

        if ($(".lookingFor-mobile").find(".btn-dropdown-rounded").hasClass("active")) {
            $(".lookingFor-mobile-place").html($(".lookingFor-mobile").find(".btn-dropdown-rounded.active").html());
        }

        if ($(".interestedIn-mobile").find(".btn-dropdown-rounded").hasClass("active")) {
            $(".interestedIn-mobile-place").html($(".interestedIn-mobile").find(".btn-dropdown-rounded.active").html());
        }

        $(".intentions-container-mobile").trigger("click");
    });

    if (ismobile()) {
        autocomplete(document.getElementById("filter-country-input-mobile"));   
    } else {
        autocomplete(document.getElementById("filter-country-input"));
    }

    ///
    ///
    ///

    modalCarousel = function() {
        /*$(document).on("click", ".modalCarousel-gallery-trigger", function (e) {

            var sources = $(this).find(".image-src");
            sources.each(function(i, el){
               var src = $(this).data("src");
               if(i == 0) {
                    var img = '<div class="carousel-item item active">' +
                        '<img class="img-responsive d-block w-100" src="' + src + '" alt="...">' +
                    '</div>';
                } else {
                    var img = '<div class="carousel-item item">' +
                        '<img class="img-responsive d-block w-100" src="' + src + '" alt="...">' +
                    '</div>';
                }

                $('.carousel-inner').append(img);
            });

            $('#modalCarousel-gallery').modal('show');

        });*/

        $('#modalCarousel-gallery').on('hidden.bs.modal', function (e) {
            $('.carousel-inner').html('');
        });
    }

    modalCarousel();

    fullAdd = function() {
        $(document).on("click", ".full-add-trigger", function(e){
            e.preventDefault();

            var ad_wrap = $("#modalShowAd");
            var ad_id = $(this).data('id');
            var ad_slug = $(this).data('slug');

             $.ajax({
                 url: '/classified-ads/ad',
                 type: 'GET',
                 data: {'ajax': 1, 'ad_id': ad_id, 'ad_slug': ad_slug },
                 beforeSend: function(){
                    $('.adds-page').LoadingOverlay('show');
                 },
                 success: function (resp) {
                    ad_wrap.html(resp);
                    ad_wrap.modal("show");
                    $('.adds-page').LoadingOverlay('hide');
                 }
             });
        });
    }

    fullAdd();

    contactTo = function() {
        $(document).on("click", ".contact-to-trigger", function(e){
            e.preventDefault();

            var data = {
                id: $(this).data('id'),
                type: 'c',
                ajax: 1,
                email: $("#modalShowAd").find("input[name=email]").val(),
                name: $("#modalShowAd").find("input[name=name]").val(),
                message: $("#modalShowAd").find("textarea[name=message]").val()
            };

             $.ajax({
                 url: '/contacts/contact-to',
                 type: 'POST',
                 data: data,
                 beforeSend: function(){
                    $('.adds-page').LoadingOverlay('show');
                 },
                 success: function (resp) {
                    $('.adds-page').LoadingOverlay('hide');
                    $('#modalShowAd').find('.error-box').html('');
                    $('#modalShowAd').find('.error-box').addClass('d-none');
                 
                    response = JSON.parse(resp);
                    if(response.status == 'error'){
                        $('#modalShowAd').find('.error-box').removeClass('d-none');
                        for(var key in response.msgs){
                            $('#modalShowAd').find('.error-box').append('<p>' + response.msgs[key] + '!</p>');
                        }
                    } else{
                        Notify.alert('success', headerVars.dictionary.message_sent_success);
                        $('#modalShowAd').modal('hide');
                    }
                 }
             });
        });
    }

    contactTo();

    loadMore = function() {
        $(document).on('click', '.load-more', function (e) {
            e.preventDefault();

           if(!$('.additional-ads').length){
               $(this).html('no more classified ads').css('pointer-events', 'none');
               return false;
           }

           $(this).addClass('d-none');
           $('.more-wrapper').removeClass('d-none');
           $('.load-less').removeClass('d-none');
        });

        $(document).on('click', '.load-less', function (e) {
            e.preventDefault();

            $(this).addClass('d-none');
            $('.more-wrapper').addClass('d-none');
            $('.load-more').removeClass('d-none');
        });
    }

    loadMore();

    loaderPagination = function(page) {
         var datum;
         if(ismobile()) {
             datum = {'page': page};
         } else {
             datum = {'page': page};
         }

        $.ajax({
            url:'/classified-ads/ajax-list',
            type: 'GET',
            data: datum,
            beforeSend: function () {
                $('.adds-page').LoadingOverlay('show');
            },
            success: function (resp) {
                $('#classified-list').html(resp);
                $('.total-results b').text($('#res_count').val());
                $('.adds-page').LoadingOverlay('hide');
            }
        })
    }

    autocomplete(document.getElementById("countryInput"));

    var filesList = [];

    addImage = function() {
        $("#modalAddAd").find("#upload_materials").on("change", function(e){
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                var name = this.files[0].name;

                filesList.push(this.files[0]);

                reader.onload = function (e) {
                    var result = e.target.result;

                    var imageHtml = '<div class="image" style="background-image: url(' + result + ');" data-name="' + name +'">' +
                    '<div class="remove-image"><div class="desktop">Remove</div>' +
                    '<div class="mobile d-flex align-items-center justify-content-center"><i class="fas fa-times"></i></div>' +
                    '</div></div><div class="clear"></div>';
                    $('#modalAddAd').find('.add-images').prepend(imageHtml);

                    toggleUpload();
                }

                reader.readAsDataURL(this.files[0]);
            }
        });
    }

    addImage();

    removeImage = function() {
        $(document).on("click", ".remove-image", function(){
            var name = $(this).parent(".image").data("name");

            $.each(filesList, function(i, el){
              if(el['name'] == name) {
                fix_index = i;
              }
            });
            filesList.splice(fix_index, 1);

            $(this).parent(".image").remove();

            toggleUpload();
        });
    }

    removeImage();

    toggleUpload = function() {
        if($("#modalAddAd .add-images").find(".image").length == 3) {
            $("#modalAddAd").find(".upload-trigger").removeClass('d-flex');
            $("#modalAddAd").find(".upload-trigger").hide();
        } else if($("#modalAddAd .add-images").find(".image").length < 3) {
            $("#modalAddAd").find(".upload-trigger").addClass('d-flex');
            $("#modalAddAd").find(".upload-trigger").show();
        }
    }

    submitAdd = function() {
        $("#classified-form").on("submit", function(e){
            e.preventDefault();

            var data = new FormData();
            $.each(filesList, function(i, file) {
                data.append('file-'+i, file);
            });

            data.append('category', $(this).find("select[name=classified-category]").val());
            data.append('duration', $(this).find("select[name=classified-duration]").val());
            data.append('phone', $(this).find("input[name=classified-phone]").val());
            data.append('email', $(this).find("input[name=classified-email]").val());
            data.append('title', $(this).find("input[name=classified-title]").val());
            data.append('text', $(this).find("textarea[name=classified-text]").val());
            data.append('country_id', $(this).find("select[name=country_id]").val());
            //data.append('region_id', $(this).find("input[name=region_id]").val());
            //data.append('zone_id', $(this).find("input[name=zone_id]").val());
            data.append('city_id', $(this).find("select[name=city_id]").val());

            $.ajax({
                url:'/classified-ads/place-ad',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST',
                beforeSend: function () {
                    $('.adds-page').LoadingOverlay('show');
                },
                success: function (resp) {
                    $('.adds-page').LoadingOverlay('hide');

                    clearAlertNotifications();

                    response = JSON.parse(resp);

                    if(response.status == 'error') {
                        $('#classified-form').find('.error-box').removeClass('d-none');
                        for(var key in response.msgs){
                            $('#classified-form').find('.error-box').append('<p>' + response.msgs[key] + '!</p>');
                        }
                    } else if(response.status == 'success') {
                        $('#classified-form').find('.success-box').removeClass('d-none');
                        $('.modal').modal('hide');
                        reset_form('#classified-form');
                        var html = '';
                        for(var key in response.msgs){
                            html += '<p>' + response.msgs[key] + '</p>'
                        }
                        Notify.alert('success', html);
                    }
                }
            });
        });
    };

    function reset_form(selector) {
        var $form = $(selector);

        $form.get(0).reset();
        $form.find('.add-images .image').remove();
        $('.dropdown-toggle-val').html(headerVars.dictionary.nothing_selected_html)
    }

    submitAdd();

    clearAlertNotifications = function() {
        $('#classified-form').find('.error-box').html('');
        $('#classified-form').find('.error-box').addClass('d-none');
        
        $('#classified-form').find('.success-box').html('');
        $('#classified-form').find('.success-box').addClass('d-none');
    }

    $('#modalAddAd').on('hidden.bs.modal', function (e) {
        clearAlertNotifications();
    });

    $('.search-button-trigger').on('click', function(e) {

    });

});

function ismobile(){
     var newWindowWidth = $(window).width();

        return (newWindowWidth < 768);
}
    