$(document).ready(function () {
    
    //Mobile advanced search select inputs reset
// **Start**

    $("#reset-all-filters-mob").click(function() {
            Notify.alert('success', headerVars.dictionary.reset_all_filters_message);
            $('.alert-success').css({"top": "0px", "font-size": "13px"});
            $('#advanced-search-form-mobile').get(0).reset();
            $('.multiselect-container input[type="checkbox"]').prop('checked', false);
            $('.multiselect-container li.active').removeClass('active');
            $('.multiselect.dropdown-toggle').attr('title', 'None selected');

            ['#common_services', '#age_to', '#incall_outcall', '#s_currency', '#escortType', '#interested_in'].forEach(function(id) {
                $(id).siblings('ul').find('li > a').first().trigger('click');
            });

            $('#common_services').parent().find('.multiselect-selected-text').html(headerVars.dictionary.common_services);
            $('#extra_services').parent().find('.multiselect-selected-text').html(headerVars.dictionary.extra_services);
            $('#fetish_services').parent().find('.multiselect-selected-text').html(headerVars.dictionary.fetish_services);
            $('#incall_outcall-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.where_to_meet);
            $('#currency-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.select_currency);
            $('#keywords-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.keywords);
            $('#language-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.language);
            $('#ethnicity-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.ethnicity);
            $('#hair_color-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.hair_color);
            $('#cup_size-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.cup_size);
            $('#eye_color-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.eye_color);
            $('#public_hair-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.public_hair);
            $('#services_for-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.service_for);
            $('#incall-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.incall);
            $('#outcall-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.outcall);
            $('#travel-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.travel);
            $('#escortType').parent().find('.multiselect-selected-text').html(headerVars.dictionary.all_escorts);
            $('#p_top_category').parent().find('.multiselect-selected-text').html(headerVars.dictionary.looking_for);
            $('#weight_from-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.select_weight);
            $('#height_from-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.select_height);
            $('#age_from-mob').parent().find('.multiselect-selected-text').html(headerVars.dictionary.select_age);
    });

//**End**

    $(".dropdown-selection.single-option").find(".dropdown-menu > li > a").on("click", function () {
        var val = $(this).attr('data-val');
        $(this).parents(".dropdown-selection").find(".single-option-select").val(val).trigger('change');
    });


    $(".escort-details").each(function () {

        var text = $(this).find("p").html();
        var link = $(this).parents(".escort-item").attr("href");
        var len = 284;

        if (typeof text != "undefined") {
            if ((text.length > text.substring(0, len).length)) {
                text = text.substring(0, len - 3) + '...' + '<a href="' + link + '">Read more</a>';
            }
        }

        $(this).find("p").html(text);
    });

    function removeDropdownSelected(obj) {
        var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").text().trim();
        $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function () {
            $(this).show();
        });
        $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function () {
            if ($(this).find("a").text().trim() == val) {
                $(this).hide();
            }
        });
    }

    $(".dropdown-toggle").each(function () {
        removeDropdownSelected(this);
    });

    $(".dropdown-menu li a").on("click", function (e) {
        var val = $(this).text().trim();
        var type = $(this).parents(".dropdown-menu").attr("data-type");
        $("#" + type).find(".dropdown-toggle-val").text(val);

        var $checkbox = $(this).find('input[type=radio]');
        if($checkbox.length) {
            $(this).parents('ul.dropdown-menu')
                .first()
                .find('input[type=radio]')
                .removeAttr('checked');
            $checkbox.attr('checked', true)
        }
        removeDropdownSelected(this);
    });

    if ($('.multiple-dropdown-box').length) {
        $('.multiple-dropdown-box').multiselect({
            numberDisplayed: 1,
            nonSelectedText: headerVars['dictionary']['noneSelected'],
            onChange: function (option, checked) {
                if (checked && option.val()) {
                    if (!this.$ul.parent().hasClass('selected')) {
                        this.$ul.parent().addClass('selected')
                    }
                } else {
                    this.$ul.parent().removeClass('selected')
                }
            },
            onInitialized: function ($select, $container) {
                if ($select && $select.val() && $select.val().length > 0) {
                    $container.addClass('selected')
                }
            },
            buttonText: function (options, select) {
                if (this.disabledText.length > 0 &&
                    (select.prop('disabled') || (options.length == 0 && this.disableIfEmpty))) {

                    return this.disabledText;
                } else if (options.length === 0) {
                    return this.nonSelectedText;
                } else if (this.allSelectedText &&
                    options.length === $('option', $(select)).length &&
                    $('option', $(select)).length !== 1 &&
                    this.multiple) {

                    if (this.selectAllNumber) {
                        return this.allSelectedText + ' (' + options.length + ')';
                    } else {
                        return this.allSelectedText;
                    }
                } else if (this.numberDisplayed != 0 && options.length > this.numberDisplayed) {
                    return options.first().text() + ', +' + (options.length - 1);
                } else {
                    var selected = '';
                    var delimiter = this.delimiterText;

                    options.each(function () {
                        var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).text();
                        selected += label + delimiter;
                    });

                    return selected.substr(0, selected.length - this.delimiterText.length);
                }
            }
        });
    }


    if ($('#countryInput-mobile').length || $('#countryInput').length) {
        if (ismobile()) {
            autocomplete(document.getElementById("countryInput-mobile"));
        } else {
            autocomplete(document.getElementById("countryInput"));
        }
    }



    $('.homepage').on("click", ".view-buttons", function () {
        $(".view-buttons").removeClass("active");
        $(this).addClass("active");
        var date = new Date();
        document.cookie = "list_type=;expires=1969-12-31T23:59:59.000Z";
        var exp = new Date(date.setFullYear(date.getFullYear() + 1));
        exp = date.toISOString();

        var cookie = "list_type=";
        if ($(this).data("type") === "view-squares") {
            $(".escorts-list").removeClass("view-list").addClass("d-flex view-squares");
            $(".escort-item").removeClass("d-flex");
            cookie += "gallery;expires=" + exp;
        } else {
            $(".escorts-list").removeClass("d-flex view-squares").addClass("view-list");
            $(".escort-item").addClass("d-flex");
            cookie += "list;expires=" + exp;
        }
        document.cookie = cookie;
    });

    $('body').on('click','.page-number', function(){
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    });

    $(".intentions-container-mobile").on("click", function () {
        $(".filters-mobile").stop(true, true).fadeToggle('slow', function() {
            if($(".filters-mobile").css('display') == 'none') {
                $('html,body').removeClass('no-more-scroll');
            }else{
                $('html,body').addClass('no-more-scroll');
            }
        });
    });

    $("#cancel-mobile").on("click", function () {
        $(".intentions-container-mobile").trigger("click");
        $(".filters-mobile").LoadingOverlay('show');
        window.location = document.referrer
    });

    $(".btn-dropdown-rounded").on("click", function () {
        $(this).parent(".d-flex").find(".btn-dropdown-rounded").removeClass("active");
        $(this).addClass("active");
    });

    $(".search-mobile").on("click", function () {
        $(".filters-mobile").css("display", "none");
        var country = $("#countryInput-mobile").val();
        if (country !== "") {
            $("#myCountry-mobile-place").html(country);
        }else{
            $("#myCountry-mobile-place").html('City Country ...');
        }

        if ($(".lookingFor-mobile").find(".btn-dropdown-rounded").hasClass("active")) {
            $(".lookingFor-mobile-place").html($(".lookingFor-mobile").find(".btn-dropdown-rounded.active").html());
        }

        if ($(".interestedIn-mobile").find(".btn-dropdown-rounded").hasClass("active")) {
            $(".interestedIn-mobile-place").html($(".interestedIn-mobile").find(".btn-dropdown-rounded.active").html());
        }

        $(".intentions-container-mobile").trigger("click");
    });

});


function ismobile() {
    var newWindowWidth = $(window).width();
    return (newWindowWidth < 768);
}

