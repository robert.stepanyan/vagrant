$(document).ready(function () {

    $(".dropdown-selection.single-option").find(".dropdown-menu > li > a").on("click", function () {
        if($(this).parents('.dropdown').hasClass('disabled')) return;

        var val = $(this).html();
        $(this).parents(".dropdown-selection").find(".single-option-select").val(val).trigger('change');
    });

    $(".dropdown-toggle").each(function () {
        removeDropdownSelected(this);
    });

    $(document).on('click', '.submit-trigger', function(){
        $(this).parents('form').trigger('submit', [{callback_value: 'back'}]);
    })

    $(document).on("click", ".dropdown-menu li a", function () {
        if($(this).parents('.dropdown').hasClass('disabled')) return;
        if($(this).parent().hasClass('disabled') || $(this).hasClass('disabled')) return;

        var val = $(this).html();
        var type = $(this).parents(".dropdown-menu").attr("data-type");
        $("#" + type).find(".dropdown-toggle-val")
                    .html(val)
                    .removeClass('pristine');
        $(this).parents('div.dropdown').first().find('select').val($(this).parent().data('val')).trigger('change');
        removeDropdownSelected(this);

    });

    $(".city-remove").on("click", function () {
        $(this).parents(".other-city").slideUp('slow', function () {
            $(this).remove();
        });
    });

    if ($("#submenu1").parent("li").hasClass("active")) {
        $("#submenu1").addClass("show");
    }


    $("#submenu-trigger").on("click", function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(".user-area").toggleClass("mobile-opened");
        $(".header").toggleClass("fixed");
    });

    $('.left-section').click(function (e) {
        e.stopPropagation();
    });

    $("body").on("click", function (event) {
        if(! $(event.target).hasClass('accordion-heading')){
            $(".user-area").removeClass("mobile-opened");
            $(".header").removeClass("fixed");
        }
    });
});

function ismobile() {
    var newWindowWidth = $(window).width();
    return (newWindowWidth < 768);
}

function removeDropdownSelected(obj) {
    var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
    $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function () {
        $(this).show();
    });
    $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function () {
        if ($(this).find("a").html() == val) {
            $(this).hide();
        }
    });
}

$.ajaxSetup({
    'complete': function (xhr, textStatus) {
        if(xhr.status == 403){
            alert("Session expired, please log in again");
            return window.location.href = '/account/signin/'
        }
    }
})
