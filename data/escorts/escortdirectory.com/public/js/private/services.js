var Services_page = (function () {
    function Services_page() {};

    Services_page.prototype.event_handlers = {

        currency_changed: function () {

            var type = $(this).data('type');
            var currency = {
                id: $(this).val(),
                title: $("#" + type + "-services-currency").find('.dropdown-toggle-val').html()
            }

            $('#' + type + "_services").find('span.currency-type-bar').each(function () {
                $(this)
                    .html(currency.title)
                    .siblings('.service_currencies_holder').val(currency.id)
            })
        },
        service_toggled: function () {
            $(this).parents('.service-list-item').find('input[name^="service_prices"]')
                .prop('disabled', !$(this).is(":checked"))
                .focus();
        },
        toggle_orientation: function () {
            $(this).parents('.buttons-tap-parent')
                .find('input[type="radio"]')
                .each(function () {
                    $(this).prop('checked', false)
                })

            $(this).parents('.buttons-tap-parent')
                .find('.tapped')
                .removeClass('tapped')

            $(this)
                .addClass('tapped')
                .find('input[type="radio"]')
                .prop("checked", true)
        },
        toggle_simple_taps: function () {
            $(this).toggleClass("tapped");

            var $checkbox = $(this).find('input[type="checkbox"]').first();
            $checkbox.prop('checked', !$checkbox.is(':checked'));
        },
    }

    Services_page.prototype.bind_events = function () {
        if (!window.SERVICES_PAGE) {
            $(document).on("change", ".currency-select", this.event_handlers.currency_changed);
            $(document).on("change", "input[type='checkbox'][name='services[]']", this.event_handlers.service_toggled);
            $(document).on("click", ".button-tap-single-value", this.event_handlers.toggle_orientation);
            $(document).on("click", ".working-cities-step-form .button-tap:not(.button-tap-single-value), .services-offered-for .button-tap", this.event_handlers.toggle_simple_taps);
        }
    }


    Services_page.prototype.init_multiselect = function () {

        $('.multiple-dropdown-box').multiselect({

            onChange: function (option, checked, select) {
                select = $(this.$select);
                var list = $(this.$ul)
                var labels = [];
                list.find('input:checked').each(function() {
                    labels.push($(this).val());
                })

                select.val(labels);
            },
            buttonText: function(options) {
                var labels = [];
                options.each(function () {
                    labels.push($(this).text())
                });

                if (options.length > 3) {
                    return labels[0] + ',' + labels[1] + ' +' + options.length;
                } else if (options.length >= 1) {
                    return labels.join(',');
                } else {
                    return headerVars.dictionary.nothing_selected;
                }
            }

        });
        $('.multiple-dropdown-box').parents('.dropdown-selection').first().removeClass('d-none').addClass('d-flex');



    }

    Services_page.prototype.init = function () {
        this.bind_events();
        this.init_multiselect();
        return this;
    }

    return Services_page;
})();


$(function () {
    window.SERVICES_PAGE = (new Services_page()).init();
})