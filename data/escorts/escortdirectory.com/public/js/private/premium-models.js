var PremiumModels = {
    escorts : null,

    init:function () {
        this.initEdit();
        this.initSave();
    },

    initEdit:function () {

        var self = this;

        this.LoadEscorts();

        $('.edit_escort_package').click(function () {
            var fromEscortId = $(this).attr('data-id');

            var options = '';
            self.escorts.forEach(function (escort) {
                options += '<option value="'+escort.id+'" >'+ escort.showname +'</option>';
            });

            var column = $('#showname-'+fromEscortId);
            var row = $('#'+fromEscortId);

            column.html(' ');

            $('<select/>',{id:'select-row-'+fromEscortId}).append(options).appendTo(column);

            $('select').select2({theme:'bootstrap'});

            row.find('.save_package_transfer').removeClass('d-none');
            row.find('.edit_escort_package').addClass('d-none');


        });
    },

    initSave:function () {

        var self = this;



        $('.save_package_transfer').click(function () {

            var fromEscortId = $(this).attr('data-id');
            var toEscortId = $('#select-row-'+fromEscortId).val();

            var data ={
                from_escort_id:fromEscortId,
                to_escort_id:toEscortId
            };

            $.ajax({
               url:'/private/ajax-premium-switch',
               type:'POST',
               data:data,
               beforeSend:function () {
                   $(".card-body").LoadingOverlay("show", true);
               },

               success:function (resp) {

                   $('#ui-view').html(resp);
                   $(".card-body").LoadingOverlay("hide", true);

                   Notify.alert('success','Your package successfuly changed !');
                   PremiumModels.init();

               }
            });

        });
    },

    LoadEscorts:function () {
        var self = this;

        $.ajax({
            url:'/private/ajax-escorts',
            type:'post',
            dataType:'json',
            async:false,
            success:function (escorts) {
                self.escorts = escorts;
            }

        });
    }
};

$(document).ready(function () {
   PremiumModels.init();
});