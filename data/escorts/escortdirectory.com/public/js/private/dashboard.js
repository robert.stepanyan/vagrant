var Dashboard = (function () {
    function Dashboard() {}

    Dashboard.prototype.init_bubble_text = function () {

        var $textarea = $('#bubble-text');

        if ($textarea.length) {

            $.ajax({
                url: '/private/get-bubble-text',
                type: "GET",
                dataType: "JSON",
                success: function (resp) {
                    $textarea.val(resp.text).trigger('keydown');
                }
            });

        } else {
            console.warn("Bubble text container not found !!!")
        }

        $textarea.on("focus blur keyup keydown input", function () {
            var maxlen = 200;
            var len = $textarea.val().length;
            var counter = $("#bubble-text-counter");
            counter.html(maxlen - len);
        });
    }

    Dashboard.prototype.fetch_analytic_statistics = function() {

        var $container = $('.last-7-days-container');
        if(!$container.length) return console.warn("Statistics container not found !");

        $.ajax({
            url: '/private/last-7-days-statistics',
            type: 'POST',
            dataType: 'JSON',
            success: function(resp) {
                try {
                    if(resp.listing_views) {
                        var n = new Intl.NumberFormat().format(resp.listing_views['views_count'] || 0);
                        $('#listing-views-count').text(n);
                    }

                    if(resp.profile_views) {
                        var n = new Intl.NumberFormat().format(resp.profile_views['views_count'] || 0);
                        $('#profile-views-count').text(n);
                    }

                    if(resp.city_views) {
                        var n = new Intl.NumberFormat().format(resp.city_views['views_count'] || 0);
                        $('#city-views-count').text(n);
                    }
                }catch(e) {

                }
            }
        })
    }

    Dashboard.prototype.event_handlers = {

        share_bubble_test: function () {
            event.preventDefault();
            
            var $container = $('#ui-view'),
                id = $("#agency_escorts").val(),
                $textarea = $('#bubble-text'),
                data = {text: $textarea.val()};

            $container.LoadingOverlay("show", true);

            if(id){
                data.escort_id = id;
            }

            $.ajax({
                url: '/private/add-bubble-text',
                data: data,
                type: 'POST',
                success: function (resp) {
                    $container.LoadingOverlay("hide", true);
                }
            });
        },

        toggle_profile_status: function (e) {
            e.preventDefault();
            var $container = $('#ui-view'),
                $btn = $(e.target),
                status = $btn.data('status');
            
            $container.LoadingOverlay('show', true);

            $.ajax({
                url: '/private/profile-status',
                method: 'POST',
                data: {
                    act: status
                },
                success: function (resp) {
                    $container.LoadingOverlay('hide', true);

                    $('.profile-status-bar').addClass('d-none').removeClass('d-flex');
                    $('.profile-status-' + status).removeClass('d-none').addClass('d-flex');
                }
            });
        },

        update_escorts_bubble_text: function(e) {
            
            var id = $("#agency_escorts").val(),
                $container = $('#ui-view'),
                $textarea = $('#bubble-text');

            $container.LoadingOverlay('show', true);

            if(id != 0){

                $.ajax({
                    url: 'private/get-bubble-text?escort_id=' + id,
                    method: "GET",
                    dataType: "JSON",
                    success: function(resp){

                        if (resp) {
                            $textarea.val(resp.text);
                        } else {
                            $textarea.val('');
                        }

                        $textarea.trigger('keydown');
                        $container.LoadingOverlay('hide');
                    }
                })
            }else{
                $textarea.val('');
            }
        }
    }

    Dashboard.prototype.bind_events = function () {

        $('#add-bubble-text').click(this.event_handlers.share_bubble_test);
        $('.profile-status-toggle').click(this.event_handlers.toggle_profile_status.bind(this));
        $("#agency_escorts").change(this.event_handlers.update_escorts_bubble_text.bind(this));
    }

    Dashboard.prototype.init = function () {

        if(headerVars.currentUser.user_type == 'escort') {
            this.fetch_analytic_statistics();
        }

        try {
            this.init_bubble_text();
            this.bind_events();
        }catch(e){
            console.error("Whoops there was an error !!!", e)
        }

        return this;
    }

    return Dashboard;
})();

$(function () {
    window.DASHBOARD_PAGE = (new Dashboard).init();
});

$('document').ready(function () {
    
    $('#halloweenModal').modal('show');

    setTimeout(function () {
        if ($('#enable').length > 0) {
            $('#enable').on('click', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                $.ajax({
                    method: 'get',
                    url: url,
                    success: function (resp) {
                        if (resp) {
                            location.reload();
                        }
                    },

                    error: function () {
                        alert('Something went wrong.');
                    },

                });
            })
        }

    }, 2000);
})