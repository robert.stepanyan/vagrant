'use strict';
var Cubix = Cubix || {};

Cubix.SelfCheckout = (function (global) {

    var Bootstrap = function () {};
    Bootstrap.prototype = (function () {
        var stateController;

        function run(userType, data) {
            stateController = new StateController(userType, data);
            new ViewRenderer(stateController);
        }

        function runFromCache(state) {
            stateController = state;
            new ViewRenderer(state);
        }

        return {
            run: run,
            runFromCache: runFromCache,
            getState: function() {
                return stateController;
            }
        };
    })();

    var StateController = function (userType, data) {
        return this.construct(userType, data);
    };

    StateController.prototype = {
        construct: function (userType, data) {
            //UK TIME
            // moment.tz.setDefault("Europe/London");

            this.userType = userType;
            this.pageView = 1;

            if (userType === 'escort') {
                this.userData = data.escort;
                this.premium_cities = data.working_locations;
                this.premium_packages = data.escort_packages;
                this.premium_country_change = data.premium_country_change;
                this.status_owner_disabled = 8;
            }

            if (userType === 'agency') {
                this.searchTerm = '';
                this.escortsPage = 1;
                this.escortsPerPage = 10;
                this.userData = data.agency;
                this.escorts = data.escorts.reduce(function (a, escort) {
                    a[escort.id] = escort;
                    return a;
                }, {});

                this.disabled_escorts = data.disabled_escorts.reduce(function (a, escort) {
                    a[escort.id] = escort;
                    return a;
                }, {});

                this.visibleEscorts = data.escorts;
                this.agencyDiscounts = data.agency_discounts.reduce(function (a, obj) {
                    var o = {};

                    o.range = [obj.value_from, obj.value_to];
                    o.discount = obj.discount;

                    return a = a.concat(o);
                }, []);
            }

            this.available_packages = data.available_packages;
            this.reason = data.reason;
            this.cart = {};
        },
    };

    var _draw_notification = function (reason, cb) {
        if(cb === undefined) cb = null;
        var $main_container = $('#ui-view');
           console.log(reason);
        switch (reason) {
            case 'no_packages_availble':
                $main_container.html('<div class="alert alert-danger" role="alert">This accounts credentials can not be used to buy new packages.</div><button data-ajax-url="/private/support#index" class="btn m-auto modal-button--checkout uppercase float-left px-5 py-3 active fixed-button">Support</button>')
                break;
            case 'owner-disabled':
                $main_container.html('<div class="alert alert-danger" role="alert">Your profile seems to be disabled. To upgrade the ad, please first Activate it.</div><button id="profile-status-activate" class="btn m-auto btn-blue btn-lg active fixed-button">Activate now</button>')
                $('#profile-status-activate').on('click', function () {
                    $(".main").LoadingOverlay("show", {
                        color: "rgba(239, 243, 249, 0.80)",
                        zIndex: 1000
                    });

                    $.ajax({
                        type: "GET",
                        url: "private/profile-status?act=enable",
                        success: function (response) {
                            if (response) {
                                $main_container.html('<div class="alert alert-success" role="alert">Your account has been successfully activated.</div><button onClick="window.location.reload()"  class="btn m-auto modal-button--checkout uppercase float-left px-5 py-3 active fixed-button">Upgrade now</button>')
                            } else {
                                $main_container.html('<div class="alert alert-danger" role="alert">Unexpected error please contact support</div>')
                            }
                        },
                        complete: function (e) {
                            $(".main").LoadingOverlay("hide", true);
                        }
                    })
                })
                break;
            case 'escort-gender':
                $main_container.html('<div class="alert alert-danger" role="alert">You can not buy a package. Please contact support</div><button data-ajax-url="/private/support#index" class="btn m-auto modal-button--checkout uppercase float-left px-5 py-3 active fixed-button">Support</button>')
                break;
            case 'complete_7_steps':
                $main_container.html('<div class="alert alert-danger" role="alert">Your ad seems to be missing mandatory information. Please complete your ad then, once the profile is approved, you can purchase a premium/vip option.</div><button data-ajax-url="/private/profile#index" class="btn m-auto modal-button--checkout uppercase float-left px-5 py-3 active fixed-button">Complete profile</button>')
                break;
            case 'escorts_no_found':
                $main_container.html('<div class="alert alert-danger" role="alert">'+headerVars.dictionary.sc_please_add_escorts+'</div><button data-ajax-url="#manage-models?active_tab=disabled" class="btn m-auto modal-button--checkout uppercase float-left active fixed-button">'+headerVars.dictionary.manage_models+'</button>')
                break;
            case 'all_escorts_from_us_fr':
                $main_container.html('<div class="alert alert-danger" role="alert">'+headerVars.dictionary.all_escorts_from_us_fr+'</div><button data-ajax-url="#manage-models?active_tab=disabled" class="btn m-auto modal-button--checkout uppercase float-left active fixed-button">'+headerVars.dictionary.manage_models+'</button>')
                break;
            case 'waiting_for_approval':
                $main_container.html('<div class="alert alert-danger" role="alert">Your profile is currently in moderation. As soon as our staff approves the changes, you can upgrade your ad.</div>')
                break;
            case 'is_inactive':
                $main_container.html('<div class="alert alert-danger" role="alert">Your ad is currently "Inactive" due to a long period of inactivity. If you are back to work, please <a href="/private/profile-status?act=activate" id="enable">"Activate"</a> the ad.</div>')
                break;

            default:
                console.log('nothing');
        }

        if(cb) cb();
    };

    var Cart = (function () {
        var instance;

        function createInstance() {
            var obj = {};
            obj.premium_packages = {};
            obj.dateCreated = Date.now();

            return obj;
        }

        return {
            getInstance: function () {
                if (!instance) {
                    instance = createInstance();
                }
                return instance;
            }
        };
    })();

    var CartItemBuilder = function () {
        return this.construct();
    };

    CartItemBuilder.prototype = {
        construct: function () {
            return this;
        },
        setAvailablePackages: function (premium_packages) {
            this.available_packages = premium_packages;

            return this;
        },

        setEscort: function (id) {
            this.escortId = id;

            return this;
        },

        setType: function (type) {

            this.type = type;

            return this;
        },

        setGender: function (gender) {
            this.gender = gender;
            return this;
        },
        setCategory: function (category) {
            this.category = category;
            return this;
        },

        setDuration: function (duration) {
            this.duration = duration;

            return this;
        },

        setActivationDate: function (date) {
            this.activationDate = date;

            return this;
        },

        setCountryGroup: function (countryGroup) {
            this.countryGroup = countryGroup;

            return this;
        },

        setBaseCityId: function (cityId) {
            this.baseCityId = cityId;

            return this;
        },

        setShowname: function (showname) {
            this.showname = showname;
        },

        setPremiumCities: function(premium_cities){
            this.premium_cities = premium_cities;
            return this;
        },

        getAmount: function (filterForAgency) {
            if(this.type == 'MonthlyPlus') {
                var _name = this.type + ' ' + this.countryGroup;
            } else {
                var _name = this.type + ' ' + this.duration + ' ' + this.countryGroup;
            }
            var _gender = this.gender;
            var _category = this.category;

            var premium_packages = this.available_packages.filter(function (p) {

                if (!filterForAgency){
                    if (p.name == _name && p.gender == _gender) {
                        return p;
                    }
                }else{
                    for(var i =0; i< p.package_prices.length; i++){
                        if (p.package_prices[i].gender == _gender && p.package_prices[i].category == _category && p.name == _name){
                            p.price = p.package_prices[i].price;
                            return p;
                        }
                    }
                }
            });

            if(typeof premium_packages[0] === 'undefined') {
                Notify.alert('warning', "No availble package");
                console.warn("No available package");
                return 0;
            }

            var amount = Number(premium_packages[0].price);
           /* if (this.premium_cities.length){
                amount += ((this.premium_cities.length - 1) * amount * 0.1) // add 10% of package price for each premium city expect base city
            }*/

            return amount;
        },

        getForCart: function (filterForAgency) {
            if(this.type == 'MonthlyPlus') {
                var _name = this.type + ' ' + this.countryGroup;
                this.duration = 30;
            } else {
                var _name = this.type + ' ' + this.duration + ' ' + this.countryGroup;
            }
            var _gender = this.gender;
            var _category = this.category;
            var premium_package = this.available_packages.filter(function (p) {
                if (!filterForAgency){
                    if (p.name == _name && p.gender == _gender) {
                        return p;
                    }
                }
                else{
                    for(var i =0; i< p.package_prices.length; i++){
                        if (p.package_prices[i].gender == _gender && p.package_prices[i].category == _category && p.name == _name){
                            p.price = p.package_prices[i].price;
                            return p;
                        }
                    }
                }
            });

            if(!premium_package[0]) return false;
            var price = premium_package[0] ? premium_package[0].price: null;
            var amount = Number(price);
            if (this.premium_cities.length && price){
                // amount += ((this.premium_cities.length - 1) * amount * 0.1) // add 10% of package price for each premium city expect base city
            }

            return {
                type: this.type,
                countryGroup: this.countryGroup,
                duration: this.duration,
                premium_cities: this.premium_cities,
                escortId: this.escortId,
                showname: this.showname,
                packageId: (premium_package[0] ? premium_package[0].id : null),
                activation_date: this.activationDate,
                price: amount
            };
        }
    };

    var ViewRenderer = function (state) {
        return this.construct(state);
    };

    ViewRenderer.prototype = {
        construct: function (state) {
            window.sc_state = state;
            var vr = this;
            vr.state = state;
            vr._draw();
        },

        _draw: function () {
            var vr = this;

            var $main_container = $('#ui-view');
            $main_container.html('')

            if (vr.state.userType === 'escort') {
                var escortView = vr._drawEscortView();
                $main_container.append(escortView);
                vr._bindEvents();
            } else if (vr.state.userType === 'agency') {
                var agencyView = vr._drawAgencyView();

                $main_container.append(agencyView);
                vr._bindAgencyEvents();
            }
        },

        _drawEscortView: function () {
            var vr = this;
            var $container = $('<div/>', {
                class: "self-checkout-container"
            });

            /** PAGE 1 */
            var $page1 = $('<div/>', {
                'data-component': 'page',
                'data-id': 1,
                'data-name': 'escortslist',
                'class': 'page',
                'css': {
                    'display': this.state.pageView !== 1 ? 'none' : 'block'
                }
            });

            var $headerBar = $('<div/>', {
                'data-component': 'bar',
                'data-name': 'headerbar',
                'class': 'section-subtitle uppercase'
            });

            var $title = $('<p/>', {
                'text': headerVars.dictionary.your_packages,
                'data-component': 'heading',
                'data-name': 'title',
                'class': 'title'
            });

            $headerBar.append($title);

            var $escortsList = $('<div/>', {
                'data-component': 'list',
                'data-name': 'escortslist',
                'style': 'background-color: #e9e9e9;',
                'class': 'escorts-list-wrapper d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap'

            });

            var $premium_packageItem = this._drawpremium_packageItem();
            $escortsList.append($premium_packageItem);

            $page1.append($headerBar, $escortsList);
            if(vr.state.premium_country_change != ''){
                $page1.append("<span class='w-100 sd' style='display: inline-block;width: 100%;color: red; background-color: #e9e9e9;padding: 0 21px 19px;'>"+ vr.state.premium_country_change +"</span>");
            }


            /** PAGE 2 */
            var $page2 = $('<div/>', {
                'data-component': 'page ',
                'data-id': 2,
                'data-name': 'cart',
                'class': 'page col-12 p-static box-section',
                'css': {
                    'display': this.state.pageView !== 2 ? 'none' : 'block'
                }
            });

            var $titleBar = $('<div/>', {
                'data-component': 'bar',
                'data-name': 'titlebar',
                'class': 'bar--inverted'
            });

            var $title = $('<div/>', {
                'text': 'Review your premium package',
                'data-component': 'heading',
                'data-name': 'title',
                'class': 'title',
            });
            $titleBar.append($title);

            var $cartTable = $('<table/>', {
                'data-component': 'table',
                'data-name': 'carttable',
                'class': 'table cart-review'
            });

            var $tableHead = $('<tr/>', {
                'data-component': 'thead',
                'data-name': 'tablehead',
                'class': 'head'
            });

            $.each(['Type', 'Country', 'Start Date', 'Duration', 'Price', ''], function (idx, el) {
                $tableHead.append($('<td/>', {
                    'data-component': 'td',
                    'data-name': 'colname',
                    'class': 'td columnHead' + el,
                    'text': el
                }))
            });

            var _buildTable = function (){
                if (vr.state.cart.premium_packages && Object.keys(vr.state.cart.premium_packages).length) {
                    $.each(vr.state.cart.premium_packages, function (idx, p) {

                        var $tableRow = $('<tr/>', {
                            'data-component': 'tr',
                            'data-name': 'tablerow row',
                            'class': 'body selected-escort'
                        });

                        $tableRow.append($('<td/>', {
                            'data-component': 'td',
                            'data-name': 'cartdata',
                            'class': 'td',
                            'text': p.type
                        }));

                        var country_title = (typeof escort != "undefined" ? escort['country_' + headerVars.lang_id] : vr.state.userData['country_' + headerVars.lang_id]);
                        $tableRow.append($('<td/>', {
                            'data-component': 'td',
                            'data-name': 'cartdata',
                            'class': 'td',
                            'text': country_title
                        }));

                        $tableRow.append($('<td/>', {
                            'data-component': 'td',
                            'data-name': 'cartdata',
                            'class': 'td',
                            'text': p.activation_date ? p.activation_date : 'ASAP'
                        }));

                        $tableRow.append($('<td/>', {
                            'data-component': 'td',
                            'data-name': 'cartdata',
                            'class': 'td',
                            'text': p.duration + ' Days'
                        }));

                        $tableRow.append($('<td/>', {
                            'data-component': 'td',
                            'data-name': 'cartdata',
                            'class': 'td',
                            'text': '€' + p.price
                        }));

                        $tableRow.append($('<td/>', {
                            'data-component': 'td',
                            'data-name': 'toolbox',
                            'class': 'action-buttons td d-flex justify-content-end actions',
                            'html': '<i class="button--edit far fa-edit  text-grey " data-id="' + idx + '">' +
                                '</i><i class="button--delete fas fa-trash  text-grey" data-id="' + idx + '"></i>'
                        }));

                        $cartTable.append($tableRow);
                    });
                }
            }

            $cartTable.append($tableHead);
            _buildTable();


            var $cartFooter = $('<div/>', {
                'data-component': 'footer',
                'data-name': 'cartfooter',
                'class': 'footer--cart d-flex justify-content-between',
            });

            if (this.state.cart.premium_packages && Object.keys(this.state.cart.premium_packages).length) {
                var totalAmount = Object.values(this.state.cart.premium_packages).reduce(function (a, el) {
                    a += +el.price;

                    return a;
                }, 0);

            }


            var $promoInput = $('<div/>', {
                class: 'promo-input',
                'html': '<input type="text" name="promo_code" class="promo-code form-control" id="promo-code" placeholder="Promo Code">'
            });

            var $promoCheck = $('<div/>', {
                class: 'promo-button',
                'html': '<button type="button" name="promo_check" class="promo-check btn btn-info" id="promo-code" >APPLY</button>'
            }).on('click', function() {
                var $promoCode = $('#promo-code').val();

                $.ajax({
                    url: '/online-billing-v2/check-promo-code',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        promo_code: $promoCode
                    },
                    success: function(resp) {
                        if (resp.status == 'success') {
                            var cartData = Object.values(vr.state.cart.premium_packages);

                            $('#promo-code').css('border', '3px solid lightgreen')
                            $('#promo-code').css('border-radius','3px');
                            if ($('.promo-value').length)
                            {
                                $('.promo-value').remove();
                            }
                            if (resp.promo_data.amount)
                            {
                                var cartTotal = 0;
                                $.each(cartData, function(index, el) {
                                    cartTotal += cartData[index].price
                                });
                                if (cartTotal > resp.promo_data.amount)
                                {
                                    var writeOffPricePerCartItem = resp.promo_data.amount/cartData.length;

                                    if (cartData.length > 1)
                                    {
                                        $.each(cartData, function(index, el) {
                                            cartData[index].price = cartData[index].price - writeOffPricePerCartItem;
                                        });
                                    }else{
                                        $.each(cartData, function(index, el) {
                                            cartData[index].price = ((cartData[index].price - writeOffPricePerCartItem) >= 0)?cartData[index].price - writeOffPricePerCartItem : 0;
                                        });
                                    }
                                }else{
                                    if ($('.promo-value').length)
                                    {
                                        $('.promo-value').remove();
                                    }
                                    $('#promo-code').css('border', '3px solid red');
                                    $('#promo-code').css('border-radius','3px');
                                    setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},200)
                                    setTimeout(() => {$('#promo-code').css('border', '3px solid red')},500)
                                    setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},700)
                                    setTimeout(() => {$('#promo-code').css('border', '3px solid red')},900)
                                    setTimeout(() => {alert("Cart total amount should exceed promo code amount")},1200)
                                    return;
                                }
                            }else if (resp.promo_data.percent)
                            {
                                $.each(cartData, function(index, el) {
                                    cartData[index].price = cartData[index].price - (cartData[index].price * resp.promo_data.percent)/100;
                                });
                            }else if(resp.promo_data.days){
                                var additionalDaysPerCartItem;
                                if (cartData.length > 1)
                                {
                                    if (resp.promo_data.days % cartData.length)
                                    {
                                        additionalDaysPerCartItem = resp.promo_data.days/cartData.length;
                                        $.each(cartData, function(index, el) {
                                            cartData[index].duration = cartData[index].duration + '+' + additionalDaysPerCartItem;
                                        });
                                    }else{
                                        additionalDaysPerCartItem = (resp.promo_data.days - 1)/cartData.length;
                                        $.each(cartData, function(index, el) {
                                            if (index === 1)
                                            {
                                                cartData[index].duration = cartData[index].duration + '+' + additionalDaysPerCartItem + 1;
                                            }else{
                                                cartData[index].duration = cartData[index].duration + '+' + additionalDaysPerCartItem;
                                            }
                                        });
                                    }
                                }else{
                                    $.each(cartData, function(index, el) {
                                        cartData[index].duration = cartData[index].duration + '+' + resp.promo_data.days;
                                    });
                                }
                            }
                            $cartTable.empty();

                            var $tableHead = $('<tr/>', {
                                'data-component': 'thead',
                                'data-name': 'tablehead',
                                'class': 'head'
                            });

                            $.each(['Type', 'Country', 'Start Date', 'Duration', 'Price', ''], function (idx, el) {
                                $tableHead.append($('<td/>', {
                                    'data-component': 'td',
                                    'data-name': 'colname',
                                    'class': 'td columnHead' + el,
                                    'text': el
                                }))
                            });

                            $cartTable.append($tableHead);
                            _buildTable();
                            var total_amount = Object.values(vr.state.cart.premium_packages).reduce(function(a, el) {
                                a += +el.price;

                                return a;
                            }, 0);

                            var $total_amount = $('<div/>', {
                                class: 'align-self-center',
                                'html': '<div class="total-amount-data">Total: <strong>€' + (total_amount ? (total_amount.toFixed(2)) : 0) + '</strong></div>'
                            });
                            $('div.total-amount-data').replaceWith($total_amount);
                            $('button.promo-check').attr('disabled','disabled');
                        }else{
                            if ($('.promo-value').length)
                            {
                                $('.promo-value').remove();
                            }
                            $('#promo-code').css('border', '3px solid red');
                            $('#promo-code').css('border-radius','3px');
                            setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},500)
                            setTimeout(() => {$('#promo-code').css('border', '3px solid red')},800)
                            setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},1000)
                        }
                    }
                });
            });

            var $promoSection = $('<div/>', {
                class: 'promo-container'
            });

            $promoSection.append($promoInput,$promoCheck);

            var $totalAmount = $('<div/>', {
                class: 'align-self-center',
                'html': '<div class="total-amount-data">Total: <strong>€' + (totalAmount ? (totalAmount.toFixed(2)) : 0) + '</strong></div>'
            });


            var $checkoutBtn = $('<button/>', {
                'data-component': 'button',
                'data-name': 'buy-button',
                'class': 'modal-button--checkout fixed-button py-3',
                'text': headerVars.dictionary.proceed_to_checkout
            }).on('click', function () {
                $checkoutBtn.prop('disabled', true);
                var cartData = Object.values(vr.state.cart.premium_packages);
                //convert date as needed for shopping card

                if (cartData[0].activation_date) {
                    var dateMoment = moment.utc(cartData[0].activation_date).format('YYYY-MM-DD');
                    cartData[0].activation_date = dateMoment;
                }
                var promoCode = $('input[name=promo_code]').val();

                $.ajax({
                    url: '/online-billing-v2/checkout',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        cart: cartData,
                        promo_code: promoCode
                    },
                    success: function (resp) {
                        if (resp.status == 'success') {
                            window.location.href = resp.url;
                        }
                        else if (resp.status == 'overlapped'){
                            Notify.alert('warning', headerVars.dictionary.overlapped_reason, 10000);
                        }
                    }
                });
            });

            $cartFooter.append($totalAmount, $promoSection , $checkoutBtn);

            $page2.append($titleBar, $cartTable, $cartFooter);
            var $footer_text = $('<div/>', {
                class: 'footer-notification w-100 box-section',
                html: '<div class="d-flex align-items-center flex-wrap">' +
                '<div class="image" style="background-image: url(\'/images/bankpic.png\'); background-size: cover; width: 110px; height: 35px; margin-right: 20px;"></div>' +
                '<div class="clear mb-10 mobile" style="min-width: 100%;"></div>' +
                '<p class="text-grey weight" style="font-style: italic; font-size: 14px;line-height: 16px;">' + headerVars.dictionary.sc_footer_title1 + '</p>' +
                '</div>' +
                '<div class="clear mb-20"></div>' +
                '<p class="text-grey" style="font-style: italic;font-size: 14px;line-height: 16px;">' + headerVars.dictionary.sc_footer_col1 + '</p>' +
                '<div class="clear mb-30"></div>' +
                '<p class="text-grey weight">' + headerVars.dictionary.sc_footer_title2 + '</p>' +
                '<div class="clear mb-10"></div><div class="text-grey">' + headerVars.dictionary.sc_footer_col2_listed + '</div>'
            });
            $container.append($page1, $page2, $footer_text);




            return $container;
        },

        _drawpremium_packageItem: function () {
            var $tr = $('<div/>', {
                'style': 'background-color: #e9e9e9;',
                class: 'box-section d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap w-100'
            });

            if (!this.state.premium_packages.length) {
                var $tdInfo = $('<div/>', {
                    html: "<p class='weight' style='font-size: 16px;line-height: 18px; margin-right: 60px;' >" + headerVars.dictionary.you_currently_have_a_basic_free + "</p>",
                    class: 'desc--row my-auto d-flex'
                });
            } else {
                var infopremium_packages = this.state.premium_packages.reduce(function (a, p) {

                    if (p.expiration_date == null) {
                        return ((a ? a + ' | ' : '') + '<span>Your <b>' + p.package_name + '</b> will be activated shortly.</span>');
                    } else {
                        return ((a ? a + ' | ' : '') + '<span>You have ' + p.package_name + '  until ' + moment.unix(p.expiration_date).format("D MMM YYYY") + '. </span>');
                    }

                }, '');

                var $tdInfo = $('<div/>', {
                    html: infopremium_packages,
                    class: 'desc--row my-auto d-flex'
                });
            }

            var $tdBtnWrapper1 = $('<div/>', {
                'data-component': 'td',
                'data-name': 'button-wrapper',
                'class': 'wrapper--button'
            });

            var $newpremium_packageBtn = $('<button/>', {
                'data-component': 'button',
                'data-name': 'purchase-button',
                'class': 'button--purchase fixed-button btn btn-blue btn-large btn-bold uppercase',
                'text': headerVars.dictionary.upgrade_now
            });

            $tdBtnWrapper1.append($newpremium_packageBtn);

            if (!this._canBuy(this.state.premium_packages)) {
                $tdBtnWrapper1.addClass('disabled');

                var $tooltip = $('<div/>', {
                    'class': 'tooltip',
                    'text': 'You already have a package. You cannot buy more.'
                });
                $tdBtnWrapper1.append($tooltip);

                $newpremium_packageBtn.addClass('disabled');
            }

            $tr.append($tdInfo, $tdBtnWrapper1);

            return $tr;
        },

        _drawAgencyView: function () {
            var vr = this;

            var $container = $('<div/>', {
                'class': ""
            });


            /** PAGE 1 */
            var $page1 = $('<div/>', {
                'data-component': 'page',
                'data-id': 1,
                'data-name': 'escortslist',
                'class': 'page',
                'css': {
                    'display': this.state.pageView !== 1 ? 'none' : 'block'
                }
            });

            var $headerBar = $('<div/>', {
                'data-component': 'bar',
                'data-name': 'headerbar',
                'class': 'bar box-section'
            });

            var $title = $('<div/>', {
                'text': 'Upgrade',
                'data-component': 'heading',
                'data-name': 'title',
                'class': 'section-subtitle uppercase'
            });

            $container.append($title);

            var $searchBar = $('<div/>', {
                'data-component': 'input',
                'data-name': 'searchbar',
                'class': 'search-bar',
                'html': '<div class="autocomplete-container d-flex flex-grow-1 mb-10" autocomplete="off">' +
                        '<input type="text" placeholder="Search" value="' + vr.state.searchTerm + '" />' +
                    '</div>'
            });

            var $sortBar = $('<div/>', {
                'data-component': 'select',
                'data-name': 'sortbar',
                'class': 'sort-bar d-flex',
                'html': '<div class="d-flex dropdown-selection dropdown-rates single-option justify-content-between align-items-center flex-fill">' +
                    '<div class="dropdown dropdown-sorting" data-error="type">' +
                        '<select class="single-option-select d-none" name="sort_by"></select>' +
                        '<a class="dropdown-toggle" id="sort_by_filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                            '<label>Sort By</label>' +
                            '<span class="dropdown-toggle-val uppercase"><span class="nothing-selected">Nothing Selected</span></span>' +
                        '</a>' +
                        '<ul class="dropdown-menu dropdown-custom dropdown-small dropdown-grey" data-type="sort_by_filter"></ul>' +
                    '</div>' +
                '</div>'
            });

            var $cartIcon = $('<i>', {
                'class': 'fa fa-cart-plus fa-2x go-to-cart px-3'
            }).on('click', function (e) {
                e.preventDefault();

                vr.state.pageView = 2;
                new ViewRenderer(vr.state);
            });
            var $divSeperator = $('<div/>',{
               'class' : 'div-seperator'
            });
            if (vr.state.cart.premium_packages && Object.keys(vr.state.cart.premium_packages).length) {
                $cartIcon.append($('<span>', {
                    text: Object.keys(vr.state.cart.premium_packages).length,
                    class: 'cart-packages-count'
                }));
            }

            var $sortOptions = ['Alphabetically', 'By escort ID', 'Latest modified', 'Newest first'].map(function (type) {
                return $('<option/>', {
                    'text': type,
                    'data-id': type.toLowerCase().replace(/ /g, '-')
                });
            });

            var $sortOptions_listed = ['Alphabetically', 'By escort ID', 'Latest modified', 'Newest first'].map(function (type) {
                return $('<li/>', {
                    'html': '<a href="javascript:void(0)">' + type + '</a>',
                    'data-val': type.toLowerCase().replace(/ /g, '-')
                });
            });
            $sortBar.find('select').append($sortOptions);
            $sortBar.find('ul').append($sortOptions_listed);
            $divSeperator.append($searchBar, $sortBar);
            $headerBar.prepend($cartIcon);
            $headerBar.append($divSeperator);

            var $escortsList = $('<div/>', {
                'data-component': 'list',
                'data-name': 'escortslist',
                'class': 'escorts-list-wrapper agency'
            });
            var term = vr.state.searchTerm;
            if (term) {
                vr.state.visibleEscorts = Object.values(vr.state.escorts).filter(function (escort) {
                    if (~escort.showname.toLowerCase().search(term.toLowerCase()) || ~escort.id.toString().search(term)) return escort;
                });
            } else {
                vr.state.visibleEscorts = Object.values(vr.state.escorts);
                vr.state.disabledEscorts = Object.values(vr.state.disabled_escorts);
            }

            $.each(vr.state.visibleEscorts, function (idx, escort) {
                var $escortItem = vr._drawEscortItem(escort);
                $escortsList.append($escortItem);
            });

            $.each(vr.state.disabledEscorts, function (idx, escort) {
                var $escortItem = vr._drawDisabledEscortItem(escort);
                $escortsList.append($escortItem);
            });

            $page1.append($headerBar, $escortsList);

            /** PAGE 2 */
            var $page2 = $('<div/>', {
                'data-component': 'page',
                'data-id': 2,
                'data-name': 'cart',
                'class': 'page box-section',
                'css': {
                    'display': this.state.pageView !== 2 ? 'none' : 'block'
                }
            });

            var $titleBar = $('<div/>', {
                'data-component': 'bar',
                'data-name': 'titlebar',
                'class': 'bar--inverted mb-4 bar--inverted mb-4 d-flex justify-content-between align-items-center'
            });

            var $title = $('<div/>', {
                'text': 'Review your premium package',
                'data-component': 'heading',
                'data-name': 'title',
                'class': 'title',
            });

            var $backToEscortList = $('<a/>', {
                'html': '<i class="fa fa-arrow-left d-md-none"></i><span class="d-none d-md-block">&larr; Back To Escorts</span>',
                'href': '#',
                'data-component': 'heading',
                'class': 'go-to-page px-3',
            }).on('click', function (e) {
                e.preventDefault();
                vr.state.pageView = 1;
                new ViewRenderer(vr.state);
            });
            $titleBar.append($backToEscortList, $title);

            var $cartTable = $('<div />', {
                'data-component': 'table',
                'data-name': 'carttable',
                'class': 'table agency-cart-review'
            });

            var $tableHead = $('<div/>', {
                'data-component': 'thead',
                'data-name': 'tablehead',
                'class': 'head desktop'
            });

            $.each(['Escort Name', 'Date', 'Duration', 'Price', ''], function (idx, el) {
                $tableHead.append($('<div/>', {
                    'data-component': 'td',
                    'data-name': 'colname',
                    'class': 'td columnHead' + el,
                    'text': el
                }))
            });

            var _buildAgencyTable = () => {
                if (vr.state.cart.premium_packages && Object.keys(vr.state.cart.premium_packages).length) {

                    $.each(vr.state.cart.premium_packages, function (idx, p) {

                        var $tableRow = $('<div/>', {
                            'data-id': p.escortId,
                            'id': p.escortId,
                            'data-component': 'tr',
                            'data-name': 'tablerow',
                            'class': 'body'
                        }).on('click', function (event) {
                            $.each($('.agency-cart-review tr td.action-buttons'), function (index, el) {
                                $(el).addClass('d-none');
                            });

                            if ($(this).hasClass('selected-escort')) {
                                $(this).removeClass('selected-escort');
                            } else {
                                $(this).addClass('selected-escort');
                                $(this).find('.action-buttons').removeClass('d-none');
                            }

                            $.each($('.agency-cart-review div').not('#' + $(this).data('id')), function (index, el) {
                                $(el).removeClass('selected-escort');
                            });

                        });;

                        $tableRow.append($('<div/>', {
                            'data-component': 'td',
                            'data-name': 'cartdata',
                            'class': 'td',
                            'html': '<b class="mobile">'+p.escort_name+':</b><span>'+p.showname+'</span>'
                        }));

                        $tableRow.append($('<div/>', {
                            'data-component': 'td',
                            'data-name': 'cartdata',
                            'class': 'td',
                            'html': '<b class="mobile">'+p.date+':</b><span>'+(p.activation_date ? p.activation_date : 'ASAP')+'</span>'
                        }));

                        $tableRow.append($('<div/>', {
                            'data-component': 'td',
                            'data-name': 'cartdata',
                            'class': 'td',
                            'html': '<b class="mobile">'+p.duration+':</b> <span>'+p.duration+' Days</span>'
                        }));

                        $tableRow.append($('<div/>', {
                            'data-component': 'td',
                            'data-name': 'cartdata',
                            'class': 'td',
                            'html': '<b class="mobile">'+p.price+':</b><span>€'+ p.price+'</span>'
                        }));

                        $tableRow.append($('<div/>', {
                            'data-component': 'td',
                            'data-name': 'toolbox ',
                            'data-actions-escort-id': p.escortId,
                            'class': 'td justify-content-end actions action-buttons td  justify-content-between actions',
                            'html': '<i class="button--edit far fa-edit  text-grey" data-id="' + idx + '">' +
                                '</i><i class="button--delete fas fa-trash  text-grey" data-id="' + idx + '"></i>'
                        }));



                        $cartTable.append($tableRow);
                    });
                } else {
                    var $tableRow = $('<div/>', {
                        'data-component': 'tr',
                        'data-name': 'tablerow',
                        'colspan': '',
                        'class': '',
                        'html': '<td class="text-center" colspan="5">Cart is empty</td>'
                    })
                    $cartTable.append($tableRow);
                }
            }

            $cartTable.append($tableHead);

            _buildAgencyTable();

            var $cartFooter = $('<div/>', {
                'data-component': 'footer',
                'data-name': 'cartfooter',
                'class': 'footer--cart d-flex justify-content-between align-items-start pt-5',
            });
            var totalAmount = 0;
            var totalAmount = 0;
            var discountedAmount = 0;
            var discountPct = 0;
            var discounts = vr.state.agencyDiscounts;
            var packageCnt = vr.state.cart.premium_packages ? Object.keys(vr.state.cart.premium_packages).length : 0;

            if (packageCnt) {
                if (discounts) {
                    $.each(discounts, function (index, el) {
                        if (el.range[1]) {
                            if (packageCnt >= el.range[0] && packageCnt < el.range[1]) {
                                discountPct = el.discount;
                                return false;
                            }
                        } else {
                            if (packageCnt >= el.range[0]) {
                                discountPct = el.discount;
                                return false;
                            }
                        }
                    });

                }
            }

            if (vr.state.cart.premium_packages && Object.keys(vr.state.cart.premium_packages).length) {
                discountedAmount = Object.keys(vr.state.cart.premium_packages).reduce(function (a, key) {
                    var el = vr.state.cart.premium_packages[key];
                    totalAmount += +el.price;
                    a += +el.price - (el.price * discountPct / 100);
                    return a;
                }, 0);
            }

            discountedAmount = discountedAmount.toFixed(2);
            var $promoInput = $('<div/>', {
                class: 'agency-promo-input',
                'html': '<input type="text" name="promo_code" class="agency-promo-code form-control" id="agency-promo-code" placeholder="Promo Code">'
            });

            var $promoCheck = $('<div/>', {
                class: 'agency-promo-button',
                'html': '<button type="button" name="promo_check" class="agency-promo-check btn btn-info" id="agency-promo-code" >APPLY</button>'
            }).on('click', function() {
                var $promoCode = $('#agency-promo-code').val();
                $.ajax({
                    url: 'online-billing-v2/check-promo-code',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        promo_code: $promoCode
                    },
                    success: function(resp) {
                        if (resp.status == 'success') {
                            var cartData = Object.values(vr.state.cart.premium_packages);
                            var discounted = 0;
                            $('#agency-promo-code').css('border', '3px solid lightgreen')
                            $('#agency-promo-code').css('border-radius','3px');
                            if ($('.promo-value').length)
                            {
                                $('.promo-value').remove();
                            }
                            if (resp.promo_data.amount)
                            {
                                var cartTotal = 0;
                                $.each(cartData, function(index, el) {
                                    cartTotal += parseInt(cartData[index].price);
                                });
                                if (cartTotal > resp.promo_data.amount)
                                {
                                    var writeOffPricePerCartItem = resp.promo_data.amount/cartData.length;
                                    discounted = resp.promo_data.amount;
                                    if (cartData.length > 1)
                                    {
                                        $.each(cartData, function(index, el) {
                                            cartData[index].price = parseInt(cartData[index].price) - writeOffPricePerCartItem;
                                        });
                                    }else{
                                        $.each(cartData, function(index, el) {
                                            cartData[index].price = ((parseInt(cartData[index].price) - writeOffPricePerCartItem) >= 0)?parseInt(cartData[index].price) - writeOffPricePerCartItem : 0;
                                        });
                                    }
                                }else{
                                    if ($('.promo-value').length)
                                    {
                                        $('.promo-value').remove();
                                    }
                                    $('#agency-promo-code').css('border', '3px solid red');
                                    $('#agency-promo-code').css('border-radius','3px');
                                    setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},200)
                                    setTimeout(() => {$('#promo-code').css('border', '3px solid red')},500)
                                    setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},700)
                                    setTimeout(() => {$('#promo-code').css('border', '3px solid red')},900)
                                    setTimeout(() => {alert("Cart total amount should exceed promo code amount")},1200)
                                    return;
                                }
                            }else if (resp.promo_data.percent)
                            {
                                $.each(cartData, function(index, el) {
                                    cartData[index].price = cartData[index].price - (cartData[index].price * resp.promo_data.percent)/100;
                                    discounted += cartData[index].price * resp.promo_data.percent/100;
                                });
                            }else if(resp.promo_data.days){
                                $.each(cartData, function(index, el) {
                                    cartData[index].duration = cartData[index].duration + '+' + resp.promo_data.days;
                                });
                            }
                            $cartTable.empty();

                            var $tableHead = $('<div/>', {
                                'data-component': 'thead',
                                'data-name': 'tablehead',
                                'class': 'head desktop'
                            });

                            $.each(['Escort Name', 'Date', 'Duration', 'Price', ''], function (idx, el) {
                                $tableHead.append($('<div/>', {
                                    'data-component': 'td',
                                    'data-name': 'colname',
                                    'class': 'td columnHead' + el,
                                    'text': el
                                }))
                            });

                            $cartTable.append($tableHead);
                            _buildAgencyTable();

                            var total_amount = Object.values(vr.state.cart.premium_packages).reduce(function(a, el) {
                                a += +el.price;

                                return a;
                            }, 0);

                            var $totalAmount = $('<div>', {
                                'html': '<p class="total-amount-data">Price:' + '<strong>€' + total_amount.toFixed(2) + '</strong></p>',
                                'class': 'total-amount-wrapper'
                            });

                            var $discountAmount = $('<div>', {
                                'html': '<p class="discount-amount-data">Discount: <strong>€' + discounted.toFixed(2) + '</strong></p>',
                                'class': 'discount-amount-wrapper'
                            });

                            var $toPayAmount = $('<div>', {
                                'html': '<p class="to-pay-amount-data">Total:' + ' <strong>€' + total_amount + '</strong></p>',
                                'class': 'to-pay-amount-wrapper'
                            });

                            $('p.total-amount-data-agency').replaceWith($totalAmount);
                            $('p.discount-amount-data-agency').replaceWith($discountAmount);
                            $('p.to-pay-amount-data-agency').replaceWith($toPayAmount);
                            $('button.agency-promo-check').attr('disabled','disabled');
                        }else{
                            if ($('.promo-value').length)
                            {
                                $('.promo-value').remove();
                            }
                            $('#promo-code').css('border', '3px solid red');
                            $('#promo-code').css('border-radius','3px');
                            setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},500)
                            setTimeout(() => {$('#promo-code').css('border', '3px solid red')},800)
                            setTimeout(() => {$('#promo-code').css('border', '3px solid transparent')},1000)
                        }
                    }
                });
            });

            var $promoSection = $('<div/>', {
                class: 'agency-promo-container'
            });

            $promoSection.append($promoInput,$promoCheck);
            /** Total Price El */

            var $totalAmount = $('<div>', {
                'html': '<p class="total-amount-data-agency">Price:' + '<strong>€' + totalAmount.toFixed(2) + '</strong></p>',
                'class': 'total-amount-wrapper'
            });

            var $discountAmount = $('<div>', {
                'html': '<p class="discount-amount-data-agency">Discount: <strong>€' + (totalAmount - discountedAmount).toFixed(2) + '</strong></p>',
                'class': 'discount-amount-wrapper'
            });

            var $toPayAmount = $('<div>', {
                'html': '<p class="to-pay-amount-data-agency">Total:' + ' <strong>€' + discountedAmount + '</strong></p>',
                'class': 'to-pay-amount-wrapper'
            });

            var $checkoutBtn = $('<button>', {
                'data-component': 'button',
                'data-name': 'buy-button',
                'class': 'modal-button--checkout fixed-button',
                'text': headerVars.dictionary.proceed_to_checkout
            }).on('click', function () {
                $checkoutBtn.prop('disabled', true);
                var packages = vr.state.cart.premium_packages;
                var promoCode = $('input[name=promo_code]').val();
                var cartData = Object.keys(packages).map(function (k) {
                    if (discountPct) {
                        packages[k].discount = discountPct;
                    }

                    return vr.state.cart.premium_packages[k];
                });

                $.each(cartData, function (index, el) {
                    if (cartData[index].activation_date) {
                        var dateMoment = moment.utc(cartData[index].activation_date).format('YYYY-MM-DD');
                        cartData[index].activation_date = dateMoment;
                    }
                });

                $.ajax({
                    url: '/online-billing-v2/checkout',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        cart: cartData,
                        promo_code: promoCode
                    },
                    success: function (resp) {
                        if (resp.status == 'success') {
                            window.location.href = resp.url;
                        }
                    }
                })
            });


            var $cartFooter_col1 = $('<div />')
            $cartFooter_col1.append($totalAmount, $discountAmount, $toPayAmount)

            $cartFooter.append($cartFooter_col1, $promoSection , $checkoutBtn);

            $page2.append($titleBar, $cartTable, $cartFooter);

            $container.append($page1, $page2);

            return $container;
        },

        _drawEscortItem: function (escort) {
            var inCart = false;
            var cartPackages = this.state.cart.premium_packages;
            if (cartPackages) {
                var inCartEscorts = Object.keys(cartPackages).map(function (idx) {
                    return cartPackages[idx].escortId;
                });

                var escortPackagePair = Object.keys(cartPackages).reduce(function (a, idx) {
                    a[cartPackages[idx].escortId] = idx;

                    return a;
                }, {});

                inCart = inCartEscorts.includes(escort.id.toString());
            }

            var $tr = $('<div/>', {
                'class': 'tr'
            });

            var $photo = $('<img/>', {
                'data-component': 'image',
                'data-name': 'image',
                'src': escort.photo,
                'css': {
                    'object-fit': 'cover',
                    'object-position': 'center right'
                },
                'class': inCart ? 'in-cart' : ''
            });
            console.log(escort)
            var $escortInfo = $('<div/>', {
                'data-component': 'td',
                'data-name': 'escortinfo',
                'class': 'info--escort',
                'html': '<p class="info--escort--showname"><span class="showname-container">' + escort.showname + '</span><br/><span class="text-grey id-container">(ID: #' + escort.id + ')</span></p><span class="text-grey city-container">(CITY: ' + escort.base_city_en + ')</span>'
            });

            if (!inCart) {
                if (!escort.escort_packages.length) {
                    if(escort.active_package && escort.active_package.package_name == 'Monthly Package') {
                        var $tdInfo = $('<div/>', {
                            'text': 'Has a monthly package.',
                            'class': 'desc--row my-auto d-flex'
                        });
                    } else {
                        var $tdInfo = $('<div/>', {
                            'text': 'You currently have a free basic ad with minimum visibility.',
                            'class': 'desc--row my-auto d-flex'
                        });
                    }
                } else {
                    var infoPackages = escort.escort_packages.reduce(function (a, p) {
                        if (p.expiration_date == null) {
                            return ((a ? a + ' | ' : '') + '<span> <b>' + p.package_name + '</b> will be activated shortly.</span>');
                        } else {
                            return ((a ? a + ' | ' : '') + '<span>Has ' + p.package_name + '  until ' + moment.unix(p.expiration_date).format("D MMM YYYY") + '. </span>');
                        }
                    }, '');

                    var $tdInfo = $('<div/>', {
                        'html': infoPackages,
                        'class': 'desc--row my-auto d-flex'
                    });
                }
            } else {
                var $tdInfo = $('<div>', {
                    'text': 'Is in your cart',
                    'class': 'desc--row in-cart align-center align-md-left'
                });
            }

            var $tdBtnWrapper1 = $('<div/>', {
                'data-component': 'td',
                'data-name': 'button-wrapper',
                'class': 'wrapper--button'
            });
            var $tdBtnWrapper2 = $tdBtnWrapper1.clone();

            var $newPackageBtn = null;
            var $movePackageBtn = null;


            if (inCart) {
                $newPackageBtn = $('<button/>', {
                    'data-component': 'button',
                    'data-name': 'purchase-button',
                    'title': 'Remove from the cart',
                    'data-id': escortPackagePair[escort.id],
                    'class': 'button--remove-from-cart button--remove',
                    'html': 'Remove'
                });
            } else {

                $newPackageBtn = $('<button/>', {
                    'data-component': 'button',
                    'data-name': 'purchase-button',
                    'data-id': escort.id,
                    'title': !escort.escort_packages.length ? 'Upgrade now' : 'This Escort is Upgraded',
                    'class': 'button--purchase inverted btn btn-blue-dark btn-small' + (!escort.escort_packages.length ? 'button--purchase inverted purchase-inverted' : 'button--purchase disabled purchase-disabled'),
                    'text': !escort.escort_packages.length ? headerVars.dictionary.upgrade_now : 'Upgraded'
                });


                if(headerVars.currentUser.user_type == 'agency' && escort.gender == 1 && escort.country_iso != 'us' && escort.active_package && escort.active_package.package_name != 'Minimum' && escort.active_package.package_name != 'Zero Package') {
                    var check_escorts_to = false;

                    $.each(this.state.escorts, function (id, esc) {
                        if(escort.id != esc.id && esc.gender == 1 && esc.country_iso != 'us' && !esc.escort_packages.length) {
                            if(!esc.active_package) {
                                check_escorts_to = true;
                            } else if(escort.active_package.package_name != esc.active_package.package_name && (esc.active_package.package_name == 'Minimum' || esc.active_package.package_name == 'Monthly Package')) {
                                check_escorts_to = true;
                            }
                        }
                    });

                    if(check_escorts_to) {
                        $movePackageBtn = $('<button/>', {
                            'data-component': 'button',
                            'data-name': 'move-package-button',
                            'data-id': escort.id,
                            'title': 'Move Package',
                            'class': 'button--move-package inverted btn btn-blue-dark btn-small inverted',
                            'text': 'Move Package',
                            'style': 'margin-left: 15px;'
                        });
                    }
                }
            }
            //$tdBtnWrapper2.append($newPackageBtn);
            var $secondXUY = $("<div />", {
                class: 'info-wrapper-bottom'
            }).append($escortInfo, $tdInfo, $newPackageBtn, $movePackageBtn);


            $tr.append($photo, $secondXUY, $tdBtnWrapper2);

            return $tr;
        },

        _drawDisabledEscortItem: function (escort) {

            var $tr = $('<div/>', {
                'class': 'tr'
            });

            var $photo = $('<img/>', {
                'data-component': 'image',
                'data-name': 'image',
                'src': escort.photo,
                'css': {
                    'object-fit': 'cover',
                    'object-position': 'center right',
                    'opacity' : 0.5
                },
                'class': ''
            });

            var $escortInfo = $('<div/>', {
                'data-component': 'td',
                'data-name': 'escortinfo',
                'class': 'info--escort',
                'html': '<p class="info--escort--showname"><span class="showname-container">' + escort.showname + '</span><br/><span class="text-grey id-container">(ID: #' + escort.id + ')</span></p>'
            });

            if (!escort.escort_packages.length) {
                if(escort.active_package && escort.active_package.package_name == 'Monthly Package') {
                    var $tdInfo = $('<div/>', {
                        'text': 'Has a monthly package.',
                        'class': 'desc--row my-auto d-flex'
                    });
                } else {
                    var $tdInfo = $('<div/>', {
                        'text': 'You currently have a free basic ad with minimum visibility.',
                        'class': 'desc--row my-auto d-flex'
                    });
                }
            } else {
                var infoPackages = escort.escort_packages.reduce(function (a, p) {
                    if (p.expiration_date == null) {
                        return ((a ? a + ' | ' : '') + '<span> <b>' + p.package_name + '</b> will be activated shortly.</span>');
                    } else {
                        return ((a ? a + ' | ' : '') + '<span>Has ' + p.package_name + '  until ' + moment.unix(p.expiration_date).format("D MMM YYYY") + '. </span>');
                    }
                }, '');

                var $tdInfo = $('<div/>', {
                    'html': infoPackages,
                    'class': 'desc--row my-auto d-flex'
                });
            }

            var $tdBtnWrapper1 = $('<div/>', {
                'data-component': 'td',
                'data-name': 'button-wrapper',
                'class': 'wrapper--button'
            });
            var $tdBtnWrapper2 = $tdBtnWrapper1.clone();

            var $movePackageBtn = null;

            if(headerVars.currentUser.user_type == 'agency' && escort.gender == 1 && escort.country_iso != 'us' && escort.active_package && escort.active_package.package_name != 'Minimum' && escort.active_package.package_name != 'Zero Package') {
                var check_escorts_to = false;

                $.each(this.state.escorts, function (id, esc) {
                    if(escort.id != esc.id && esc.gender == 1 && esc.country_iso != 'us' && !esc.escort_packages.length) {
                        if(!esc.active_package) {
                            check_escorts_to = true;
                        } else if(escort.active_package.package_name != esc.active_package.package_name && (esc.active_package.package_name == 'Minimum' || esc.active_package.package_name == 'Monthly Package')) {
                            check_escorts_to = true;
                        }
                    }
                });

                if(check_escorts_to) {
                    $movePackageBtn = $('<button/>', {
                        'data-component': 'button',
                        'data-name': 'move-package-button',
                        'data-id': escort.id,
                        'title': 'Move Package',
                        'class': 'button--move-package inverted btn btn-blue-dark btn-small inverted',
                        'text': 'Move Package'
                    });
                }
            }

            var $secondXUY = $("<div />", {
                class: 'info-wrapper-bottom'
            }).append($escortInfo, $movePackageBtn, $tdInfo);


            $tr.append($photo, $secondXUY, $tdBtnWrapper2);

            if($movePackageBtn) {
                return $tr;
            } else {
                return '';
            }

        },

        _bindEvents: function () {
            var vr = this;
            var date = new Date();
            var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            var endDate = new Date(date.getFullYear() + 10, date.getMonth(), date.getDate());
            $('.button--purchase:not(.disabled)').on('click', function () {
                var modalContent = vr._getModalContent(null, null);

                var $modal = new Modal({
                    close: true,
                    header: modalContent.$header,
                    body: modalContent.$body
                });
                $modal.show();

                var cartItem = vr._getCartItem($modal);
                $modal.find('.amount').html('€' + cartItem.getAmount(false));

                $('#premium_packageActivationDate')
                    .datepicker({
                        startDate: today,
                        endDate: endDate,
                        todayHighlight: true,
                        maxViewMode:'years',
                        format: {
                            toDisplay: function (date, format, language) {
                                var d = new Date(date);
                                return moment(d).format("D MMM YYYY");
                            },
                            toValue: function (date, format, language) {
                                var d = new Date(date);
                                return d.getTime();
                            }
                        }
                    })
                    .on('show', function(e){
                        $('label[for="psd_scheduled"]').click();
                    });

                $modal.find('.modal-button--amount').on('click', function (e) {
                    e.preventDefault();

                    var cartItem = vr._getCartItem($modal);

                    var itemForCart = cartItem.getForCart(false);

                    if(!itemForCart) {
                        if(Cubix.SelfCheckout.bootstrap.getState().reason)
                            Notify.alert('warning', Cubix.SelfCheckout.bootstrap.getState().reason);
                        return console.log("Canceled !");
                    }

                    var cartInstance = Cart.getInstance();
                    var hash = 'p' + Date.now().toString(16);
                    cartInstance.premium_packages[hash] = itemForCart;
                    vr.state.cart = cartInstance;
                    vr.state.pageView = 2;
                    $modal.hide();
                    new ViewRenderer(vr.state);
                });

                $modal.find('input[type="radio"]').on('change', function () {
                    var cartItem = vr._getCartItem($modal);
                    $modal.find('.amount').html('€' + cartItem.getAmount(false));
                });

                $modal.find('input[type="checkbox"]').on('change', function () {
                    var cartItem = vr._getCartItem($modal);
                    $modal.find('.amount').html('€' + cartItem.getAmount(false));
                });
            });


            $('.button--edit').on('click', function () {
                var premium_packageUid = $(this).data('id');
                var modalContent = vr._getModalContent(null, vr.state.cart.premium_packages[premium_packageUid]);

                var $modal = new Modal({
                    close: true,
                    header: modalContent.$header,
                    body: modalContent.$body
                });
                $modal.show();

                var cartItem = vr._getCartItem($modal);
                $modal.find('.amount').html('€' + cartItem.getAmount(false));

                $('#premium_packageActivationDate')
                    .datepicker({
                        startDate: today,
                        endDate: endDate,
                        todayHighlight: true,
                        maxViewMode: 'years',
                        format: {
                            toDisplay: function (date, format, language) {
                                var d = new Date(date);
                                return moment(d).format("D MMM YYYY");
                            },
                            toValue: function (date, format, language) {
                                var d = new Date(date);
                                return d.getTime();
                            }
                        }
                    })
                    .on('show', function(e){
                        $('label[for="psd_scheduled"]').click();
                    });

                $modal.find('.modal-button--amount').on('click', function (e) {
                    e.preventDefault();
                    var cartItem = vr._getCartItem($modal);
                    var itemForCart = cartItem.getForCart(false);
                    if(!itemForCart) return;

                    var cartInstance = Cart.getInstance();
                    cartInstance.premium_packages[premium_packageUid] = itemForCart;
                    vr.state.cart = cartInstance;
                    vr.state.pageView = 2;
                    $modal.hide();
                    new ViewRenderer(vr.state);
                });

                $modal.find('input[type="radio"]').on('change', function () {
                    var cartItem = vr._getCartItem($modal);
                    $modal.find('.amount').html('€' + cartItem.getAmount(false));
                });
                $modal.find('input[type="checkbox"]').on('change', function () {
                    var cartItem = vr._getCartItem($modal);
                    $modal.find('.amount').html('€' + cartItem.getAmount(false));
                });
            });

            $('.button--delete').on('click', function () {
                var premium_packageUid = $(this).data('id');
                delete vr.state.cart.premium_packages[premium_packageUid];
                if (Object.keys(vr.state.cart.premium_packages).length === 0) {
                    vr.state.pageView = 1;
                } else {
                    vr.state.pageView = 2;
                }

                new ViewRenderer(vr.state);
            });
        },

        _bindAgencyEvents: function () {
            var vr = this;
            var date = new Date();
            var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            var endDate = new Date(date.getFullYear() + 10, date.getMonth(), date.getDate());
            $('.button--purchase:not(.disabled)').on('click', function () {
                var escortId = $(this).data('id');
                var escort = vr.state.escorts[escortId];

                var modalContent = vr._getModalContent(escort, null);

                var $modal = new Modal({
                    close: true,
                    header: modalContent.$header,
                    body: modalContent.$body
                });
                $modal.show();

                if($modal.find('#pn_MonthlyPlus').prop('checked')) {
                    $modal.find('.duration').hide();
                }

                var cartItem = vr._getCartItem($modal);
                $modal.find('.amount').html('€' + cartItem.getAmount(true));

                $('#premium_packageActivationDate')
                    .datepicker({
                        startDate: today,
                        endDate: endDate,
                        todayHighlight: true,
                        maxViewMode: 'years',
                        format: {
                            toDisplay: function (date, format, language) {
                                var d = new Date(date);
                                return moment(d).format("D MMM YYYY");
                            },
                            toValue: function (date, format, language) {
                                var d = new Date(date);
                                return d.getTime();
                            }
                        }
                    })
                    .on('show', function(e){
                        $('label[for="psd_scheduled"]').click();
                    });

                $modal.find('.modal-button--amount').on('click', function (e) {
                    e.preventDefault();
                    var cartItem = vr._getCartItem($modal);
                    var itemForCart = cartItem.getForCart(true);
                    if(!itemForCart) return;

                    var cartInstance = Cart.getInstance();
                    var hash = 'p' + Date.now().toString(16);
                    cartInstance.premium_packages[hash] = itemForCart;
                    vr.state.cart = cartInstance;

                    $modal.hide();
                    new ViewRenderer(vr.state);

                    var infoModalContent = vr._getInfoModalContent();


                    var $modalMsg = new Modal({
                        header: infoModalContent.$header,
                        body: infoModalContent.$body
                    });

                    $modalMsg.find('.modal-button--continue').on('click', function () {
                        vr.state.pageView = 1;
                        $modalMsg.hide();
                        new ViewRenderer(vr.state);
                    });

                    $modalMsg.find('.modal-button--gotocart').on('click', function () {
                        vr.state.pageView = 2;
                        $modalMsg.hide();
                        new ViewRenderer(vr.state);
                    });

                    $modalMsg.show()
                });

                $modal.find('input[type="radio"]').on('change', function () {
                    if($modal.find('#pn_MonthlyPlus').prop('checked')) {
                        $modal.find('.duration').hide();
                        $modal.find('.premium-cities').hide();
                        $modal.find('input[type="checkbox"]:not([disabled])').prop('checked', false);
                    } else if($modal.find('.duration').is(":hidden")) {
                        $modal.find('.duration').show();
                        $modal.find('.premium-cities').show();
                    }
                    console.log($(this).attr('id'));
                    if ($(this).attr('id') === 'pn_VIP') {
                        $modal.find('.duration label span.d-md-none').text('VIP')
                    }
                    if ($(this).attr('id') === 'pn_Premium') {
                        $modal.find('.duration label span.d-md-none').text('Premium')
                    }
                    var cartItem = vr._getCartItem($modal);
                    $modal.find('.amount').html('€' + cartItem.getAmount(true));
                });

                $modal.find('input[type="checkbox"]').on('change', function () {
                    var cartItem = vr._getCartItem($modal);
                    $modal.find('.amount').html('€' + cartItem.getAmount(true));
                });
            });

            $('.button--move-package:not(.disabled)').on('click', function () {
                var escortId = $(this).data('id');
                var escort = vr.state.escorts[escortId];

                if(!escort) {
                    var escort = vr.state.disabled_escorts[escortId];
                }

                var modalContent = vr._getModalContentForMovePackage(escort);

                var $modal = new Modal({
                    close: true,
                    header: modalContent.$header,
                    body: modalContent.$body
                });
                $modal.show();

                $modal.find('.modal-button--move-package').on('click', function (e) {
                    e.preventDefault();

                    var from_escort_id = $modal.find('input[name="from_escort_id"]').val();
                    var to_escort_id = $modal.find('select[name="to_escort_id"]').val();
                    var city_ids = $modal.find('input[name="city_ids"]').val();

                    var movePackageData = {
                        from_escort_id: from_escort_id,
                        to_escort_id: to_escort_id,
                        city_ids: city_ids ? city_ids.split(',') : []
                    };

                    $modal.LoadingOverlay("show", {
                        color: "rgba(239, 243, 249, 0.80)",
                        zIndex: 3000
                    });

                    $.ajax({
                        url: '/online-billing-v2/move-package',
                        method: 'POST',
                        dataType: 'json',
                        data: movePackageData,
                        success: function (resp) {
                            if (resp.status) {
                                window.sc_state = null;
                                loadPage();
                                $modal.LoadingOverlay("hide", true);
                                $modal.hide();
                            } else {
                                $modal.LoadingOverlay("hide", true);
                                Notify.alert('warning', resp.message);
                            }
                        }
                    });

                });

            });

            $('.button--edit').on('click', function () {
                var premium_packageUid = $(this).data('id');
                var modalContent = vr._getModalContent(vr.state.escorts[vr.state.cart.premium_packages[premium_packageUid].escortId], vr.state.cart.premium_packages[premium_packageUid]);

                var $modal = new Modal({
                    close: true,
                    header: modalContent.$header,
                    body: modalContent.$body
                });
                $modal.show();

                if($modal.find('#pn_MonthlyPlus').prop('checked')) {
                    $modal.find('.duration').hide();
                }

                var cartItem = vr._getCartItem($modal);
                $modal.find('.amount').html('€' + cartItem.getAmount(true));

                $('#premium_packageActivationDate')
                    .datepicker({
                        startDate: today,
                        endDate: endDate,
                        todayHighlight: true,
                        maxViewMode: 'years',
                        format: {
                            toDisplay: function (date, format, language) {
                                var d = new Date(date);
                                return moment(d).format("D MMM YYYY");
                            },
                            toValue: function (date, format, language) {
                                var d = new Date(date);
                                return d.getTime();
                            }
                        }
                    })
                    .on('show', function(e){
                        $('label[for="psd_scheduled"]').click();
                    });

                $modal.find('.modal-button--amount').on('click', function (e) {
                    e.preventDefault();

                    var cartItem = vr._getCartItem($modal);
                    var itemForCart = cartItem.getForCart(true);
                    if(!itemForCart) return;

                    var cartInstance = Cart.getInstance();
                    cartInstance.premium_packages[premium_packageUid] = itemForCart;
                    vr.state.cart = cartInstance;
                    vr.state.pageView = 2;
                    $modal.hide();
                    new ViewRenderer(vr.state);
                });

                $modal.find('input[type="radio"]').on('change', function () {
                    if($modal.find('#pn_MonthlyPlus').prop('checked')) {
                        $modal.find('.duration').hide();
                        $modal.find('.premium-cities').hide();
                        $modal.find('input[type="checkbox"]:not([disabled])').prop('checked', false);
                    } else if($modal.find('.duration').is(":hidden")) {
                        $modal.find('.duration').show();
                        $modal.find('.premium-cities').show();
                    }
                    console.log($(this).attr('id'));
                    if ($(this).attr('id') === 'pn_VIP') {
                        $modal.find('.duration label span.d-md-none').text('VIP')
                    }
                    if ($(this).attr('id') === 'pn_Premium') {
                        $modal.find('.duration label span.d-md-none').text('Premium')
                    }

                    var cartItem = vr._getCartItem($modal);
                    $modal.find('.amount').html('€' + cartItem.getAmount(true));
                });

                $modal.find('input[type="checkbox"]').on('change', function () {

                    var cartItem = vr._getCartItem($modal);
                    $modal.find('.amount').html('€' + cartItem.getAmount(true));
                });
            });

            $('.button--delete').on('click', function () {
                var premium_packageUid = $(this).data('id');
                delete vr.state.cart.premium_packages[premium_packageUid];
                if (Object.keys(vr.state.cart.premium_packages).length === 0) {
                    vr.state.pageView = 1;
                } else {
                    vr.state.pageView = 2;
                }

                new ViewRenderer(vr.state);
            });

            $('.button--remove-from-cart').on('click', function () {
                var packageUid = $(this).data('id');
                delete vr.state.cart.premium_packages[packageUid];

                new ViewRenderer(vr.state);
            });

            $('.search-bar').find('input').on('input', function () {
                vr.state.searchTerm = $(this).val();
                new ViewRenderer(vr.state);
            });

            $('.sort-bar').find('select').on('change', function () {
                var selectedOpt = $(this).find(":selected").data('id');

                switch (selectedOpt) {
                    case 'alphabetically':
                        vr.state.visibleEscorts.sort(function (a, b) {
                            return ('' + a.showname).localeCompare(b.showname);
                        });
                        $('.escorts-list-wrapper.agency').html('')
                        $.each(vr.state.visibleEscorts, function (idx, escort) {
                            var $escortItem = vr._drawEscortItem(escort);
                            $('.escorts-list-wrapper.agency').append($escortItem);
                        });
                        break;
                    case 'by-escort-id':
                        vr.state.visibleEscorts.sort(function (a, b) {
                            return (a.id - b.id);
                        });

                        $('.escorts-list-wrapper.agency').html('')
                        $.each(vr.state.visibleEscorts, function (idx, escort) {
                            var $escortItem = vr._drawEscortItem(escort);
                            $('.escorts-list-wrapper.agency').append($escortItem);
                        });
                        break;
                    case 'latest-modified':
                        vr.state.visibleEscorts.sort(function (a, b) {
                            return new Date(b.date_last_modified) - new Date(a.date_last_modified);
                        });

                        $('.escorts-list-wrapper.agency').html('')
                        $.each(vr.state.visibleEscorts, function (idx, escort) {
                            var $escortItem = vr._drawEscortItem(escort);
                            $('.escorts-list-wrapper.agency').append($escortItem);
                        });
                        break;
                    case 'newest-first':
                        vr.state.visibleEscorts.sort(function (a, b) {
                            return new Date(b.date_registered) - new Date(a.date_registered);
                        });

                        $('.escorts-list-wrapper.agency').html('')
                        $.each(vr.state.visibleEscorts, function (idx, escort) {
                            var $escortItem = vr._drawEscortItem(escort);
                            $('.escorts-list-wrapper.agency').append($escortItem);
                        });
                        break;
                }
                vr._bindAgencyEvents();

                // vr.state.visibleEscorts.sort()
            });

            var $searchBar = $('.search-bar').find('input');

            if (vr.state.searchTerm) {
                $searchBar.focus();

                setTimeout(function () {
                    $searchBar.get(0).selectionStart = $searchBar.get(0).selectionEnd = 10000;
                }, 0);
            } else {
                //$searchBar.focus();
            }
        },

        _getInfoModalContent: function () {
            var vr = this;
            var $header = $('<h6/>', {
                'class': 'card-header',
                'text': 'Package added to a cart'
            });

            var modalTxt = '';
            var packageCnt = Object.keys(vr.state.cart.premium_packages).length;
            var modalTxt = '';

            if (vr.state.agencyDiscounts) {
                var discounts = vr.state.agencyDiscounts.sort(function (a, b) {
                    return a.range[0] > b.range[0] ? 1 : -1
                });
                for (var i = 0; i < discounts.length; i++) {
                    var el = discounts[i];

                    if (packageCnt < el.range[0]) {
                        modalTxt = '<h6 class="p-4">Buy ' + (el.range[0] - packageCnt) + ' more packages to get ' + el.discount + '% off.</h6>';
                        break;
                    }
                }
            }
            var $body = $('<div/>', {
                'class': 'card-body',
                'html': '<div>' + modalTxt + '</div>'
            });

            var $footer = $('<div/>', {
                'data-component': 'footer',
                'data-name': 'modalfooter',
                'class': 'footer d-flex justify-content-center py-3 flex-column align-items-center'
            });



            var $goToCartButton = $('<button/>', {
                'data-component': 'button',
                'data-name': 'gotocartbutton',
                'class': 'modal-button--gotocart btn btn-brown btn-large my-2',
                'text': 'Go to cart',
                'style': 'width:173px'
            });

            var $continueButton = $('<button/>', {
                'data-component': 'button',
                'data-name': 'continuebutton',
                'class': 'modal-button--continue btn btn-blue-dark btn-large my-2',
                'text': 'Continue shopping'
            });

            $footer.append($goToCartButton, $continueButton);
            $body.append($footer);

            return {
                $header: $header,
                $body: $body
            };
        },

        _getCartItem: function ($container) {
            var premium_packageType = $container.find('input[name="premium_package_name"]:checked').val();
            var premium_packageCities = [];

            // UPD: https://sceonteam.atlassian.net/browse/EDIR-875 discount disabled for task.
            $container.find('input[name="premium_cities[]"]:checked').each(function (i, el) {
                console.log($(el).val());
                premium_packageCities.push(Number($(el).val()))
            });
            
            var escortId = $container.find('input[name="escort_id"]').val();
            var premium_packageDuration = $container.find('input[name="premium_package_duration"]:checked').val();
            var premium_packageGender = $container.find('input[name="premium_package_gender"]').val();
            var premium_packageCategory = $container.find('input[name="premium_package_category"]').val();
            var packageBaseCity = $container.find('input[name="package_base_city"]').val();
            var escortShowname = $container.find('input[name="escort_showname"]').val();
            var packageCountryGroup = $container.find('input[name="package_country_group"]').val();

            var premium_packageActivationDate = $container.find('input[name="premium_package_start_date"]:checked').val();
            var activationDate = premium_packageActivationDate == 'scheduled' ? $('#premium_packageActivationDate').val() : null;

            var cartItem = new CartItemBuilder();


            cartItem
                .setAvailablePackages(this.state.available_packages)
                .setEscort(escortId)
                .setGender(premium_packageGender)
                .setCategory(premium_packageCategory)
                .setType(premium_packageType)
                .setDuration(premium_packageDuration)
                .setActivationDate(activationDate)
                .setCountryGroup(packageCountryGroup)
                .setPremiumCities(premium_packageCities)
                .setBaseCityId(packageBaseCity);

            if (escortShowname) {
                cartItem.setShowname(escortShowname);
            }

            return cartItem;
        },

        _getModalContent: function (escort, premium_package) {
            var vr = this;
            var showname = '';

            if (escort) {
                showname = '(' + escort.showname + ')';
            }

            var $header = $('<h6/>', {
                'class': 'modal-header mb-0',
                'text': !premium_package ? 'Set your Premium package' : 'Edit the Premium package ' + showname

            });

            var $body = $('<div/>', {
                'class': 'modal-body'
            });

            var $bodyRow1 = $('<tr/>', {
                'data-component': 'tr',
                'data-name': 'tablerow',
                'class': 'row'
            });

            var $bodyRow2 = $bodyRow1.clone().addClass('duration bordered-b pb-20 mt-0');
            var $bodyRowPremiumCities = $bodyRow1.clone().addClass('premium-cities bordered-b pb-20 mt-0');
            var $bodyRow3 = $bodyRow1.clone().addClass('flex-column flex-sm-row premium-package-datetime d-md-flex bordered-b pb-20');
            var $bodyRow4 = $bodyRow1.clone().addClass('paddingless d-flex d-none bordered-b mt-0 pb-20');

            var countryISO = escort ? escort.country_iso : vr.state.userData.country_iso;
            var aCounties = ~['gb', 'us', 'ca', 'de', 'fr', 'it'].indexOf(countryISO) ? 'A' : 'B';


            if (escort) {
                var inputs = '<input type="hidden" name="escort_showname" value="' + escort.showname + '" />';
            } else {
                var inputs = '';
            }

            var text = '<span class="block--country">For country: <strong>' + (escort ? escort['country_' + headerVars.lang_id] : vr.state.userData['country_' + headerVars.lang_id]) + '</strong></span>';

            $bodyRow4.append($('<div/>', {
                'data-component': 'td',
                'data-name': 'premium_packagedata',
                'class': 'td flex-row text-center col-md-12 mt-0',
                'html': '<input type="hidden" name="premium_package_gender" value="' + (escort ? escort.gender : vr.state.userData.gender) + '" />' +
                    '<input type="hidden" name="premium_package_category" value="' + (escort ? escort.type : vr.state.userData.type) + '" />' +
                    '<input type="hidden" name="escort_id" value="' + (escort ? escort.id : vr.state.userData.id) + '" />' +
                    '<input type="hidden" name="package_base_city" value="' + (escort ? escort.base_city_id : vr.state.userData.base_city_id) + '" />' +
                    '<input type="hidden" name="package_country_group" value="' + aCounties + '" />' +
                    text +
                    inputs
            }));

            var arr = ['Premium', 'VIP', 'MonthlyPlus'];
            if(headerVars.currentUser.user_type === 'escort' || Number(escort.gender) !== 1 || escort.country_iso === 'us') {
                arr.pop();
            }

            var $packageNamesRow = $("<div/>",{
                'data-component': 'td',
                'data-name': 'premium_packagedata',
                'class': 'td scheduled checkbox-container flex-row text-center col-md-12',
            });
            $.each(arr, function (idx, premium_packageName) {
                $packageNamesRow.append($("<span/>", {
                    'class':'pl-2 pr-2',
                    'html': '<input type="radio" id="pn_' + premium_packageName + '" name="premium_package_name" value="' + premium_packageName + '" /><label for="pn_' + premium_packageName + '">' + premium_packageName + '</label>'
                }));
            });
            $bodyRow4.append($packageNamesRow);
            if (premium_package) {
                $bodyRow4.find('#pn_' + premium_package.type).attr('checked', true);
            } else {
                $bodyRow4.find('input[type="radio"]').filter(":first").attr('checked', true);
            }


            var premiumCities = escort ? escort.working_locations : vr.state.premium_cities;
            $.each(premiumCities, function (idx, city) {
                var premiumCityShouldBeChecked = false;
                if (premium_package && premium_package.hasOwnProperty('premium_cities')) {
                    premiumCityShouldBeChecked = premium_package.premium_cities.indexOf(city.city_id) >= 0;
                }

                $bodyRowPremiumCities.append($('<div/>', {
                    'data-component': 'td',
                    'data-name': 'premium_packagedata',
                    'class': 'td  checkbox-container col-md-6 w-50'
                }).append($("<input/>",{
                        "type":'checkbox',
                        'id':'pc_'+idx,
                        'name':'premium_cities[]',
                        'value': city.city_id,
                        'disabled':(Number(city.is_base) === 1),
                        'checked':(Number(city.is_base) === 1 || premiumCityShouldBeChecked)
                    })).append($("<label/>",{
                        'for':'pc_'+idx,
                        'html':'<strong>&nbsp;&nbsp;'+city.title+'</strong>'
                    }))
                );
            });

           /* $bodyRowPremiumCities.append($("<p>", {
                'text': 'One city included, Extra cities cost more',
                'class': 'text-center col-md-12 mt-3'
            }));
*/
            $.each(['15', '30', '90'], function (idx, duration) {

                $bodyRow2.append($('<div/>', {
                    'data-component': 'td',
                    'data-name': 'premium_packagedata',
                    'class': 'td  checkbox-container col-md-4',
                    'html': '<input type="radio" id="pd_' + duration + '" name="premium_package_duration" value="' + duration + '" /><label for="pd_' + duration + '"><span class="d-md-none">Premium </span>' + duration + ' <span class="">Days</span></label>'
                }));

            });

            if (premium_package) {
                $bodyRow2.find('#pd_' + premium_package.duration).attr('checked', true);
            } else {
                $bodyRow2.find('input[type="radio"]').filter(':first').attr('checked', true);
            }


            $.each([{
                title: 'Starts now',
                subtitle: ' Or immediately after the current '+headerVars.dictionary.premium_package+' ends'
            }, {
                title: 'Scheduled',
                subtitle: 'Starts at selected date'
            }], function (idx, startDate) {
                if (startDate.title == 'Starts now') {

                }
                var sd = startDate.title.toLowerCase().replace(/ /g, '-');
                $bodyRow3.append($('<div/>', {
                    'data-component': 'td',
                    'data-name': 'premium_packagedata',
                    'class': 'td checkbox-container col-md-6',
                    'html': '<input type="radio" id="psd_' + sd + '" name="premium_package_start_date" value="' + sd + '"/>' +
                        '<label class="psd_' + sd + '" for="psd_' + sd + '">' + startDate.title + '<div class="text-muted">' + startDate.subtitle + '</span></label>' +
                        (startDate.title !== 'Starts now' ? '<input type="text" readonly="readonly" id="premium_packageActivationDate"/><div id="render-pikaday"></div>' : '')
                }));
            });

            if (premium_package) {
                $bodyRow3.find('#psd_' + (!premium_package.activationDate ? 'starts-now' : 'scheduled')).attr('checked', true);
            } else {
                $bodyRow3.find('input[type="radio"]').filter(':first').attr('checked', true);
            }

            var $footer = $('<div/>', {
                'data-component': 'footer',
                'data-name': 'modalfooter',
                'class': 'footer d-flex justify-content-between pt-5 pb-2'
            });

            var $amountTitle = $('<div/>', {
                'class': 'total-amount',
                'text': 'Total:'
            });

            var $amount = $('<div/>', {
                'data-component': 'block',
                'data-name': 'amount',
                'class': 'amount',
                'text': ''
            });

            var $buyButton = $('<button/>', {
                'data-component': 'button',
                'data-name': 'buy-button',
                'class': 'modal-button--amount btn btn-blue-dark btn-large ml-auto',
                'text': !premium_package ? 'ADD TO CART' : 'DONE'
            });

            $footer.append($amountTitle, $amount, $buyButton);

            var $bodyRow2Title = $('<h6/>', {
                'class': 'duration_title d-block d-md-none py-2',
                'text': 'Duration'
            });

            $body.append($bodyRow4, $bodyRow1, $bodyRow2Title, $bodyRow2, $bodyRowPremiumCities, $bodyRow3, $footer);

            return {
                $header: $header,
                $body: $body
            }
        },

        _getModalContentForMovePackage: function (escort) {
            var vr = this;

            var packageInfo = ' Assign ' + escort.active_package.package_name + ' to ';
            var $header = $('<h6/>', {
                'class': 'modal-header',
                'text': packageInfo
            });

            var $body = $('<div/>', {
                'class': 'modal-body'
            });

            var $bodyRow1 = $('<tr/>', {
                'data-component': 'tr',
                'data-name': 'tablerow',
                'class': 'row'
            });

            var $bodyRow2 = $bodyRow1.clone().addClass('paddingless d-flex d-none m-0');

            $bodyRow2.append($('<div/>', {
                'data-name': 'from_escort_id',
                'html':
                    '<input type="hidden" name="from_escort_id" value="' + escort.id + '" />'+
                    '<input type="hidden" name="city_ids" value="' + escort.premium_cities + '" />'
            }));

            var options = '';


            $.each(vr.state.escorts, function (id, esc) {
                if(escort.id != esc.id && esc.gender == 1 && esc.country_iso != 'us' && !esc.escort_packages.length &&
                   (!esc.active_package || (esc.active_package &&
                   escort.active_package.package_name != esc.active_package.package_name &&
                   (esc.active_package.package_name == 'Minimum' || esc.active_package.package_name == 'Monthly Package')))) {
                    options += '<option value="' + id + '">' + esc.showname + '</option>';
                }
            });

            $bodyRow1.append($('<div/>', {
                'data-name': 'to_escort_id',
                'class': 'col-md-6',
                'html': '<select name="to_escort_id" class="form-control">' + options + '</select>'
            }));

            var $moveButton = $('<button/>', {
                'data-component': 'button',
                'data-name': 'move-button',
                'class': 'col-md-4 offset-md-1 modal-button--move-package btn btn-blue-dark',
                'text': 'MOVE'
            });

            $bodyRow1.append($moveButton);

            $body.append($bodyRow1, $bodyRow2);

            return {
                $header: $header,
                $body: $body
            }
        },

        _canBuy: function (premium_packages) {
            var STATUS_PENDING = 1;
            var STATUS_ACTIVE = 2;

            if (premium_packages.length === 1 && premium_packages[0].status === STATUS_PENDING) {
                return false;
            }

            if (premium_packages.length === 1 && premium_packages[0].status === STATUS_ACTIVE) {
                return false;
            }

            if (premium_packages.length === 2) {
                return false;
            }

            return true;
        }
    }


    function Modal(params) {
        /**
         * params should contain { header, body }
         */
        return this.construct(params);
    };
    Modal.prototype = {
        construct: function (params) {
            this.params = params || {
                header: $('<div>'),
                body: $('<div>')
            };

            this.build().bindEvents();

            this.html.show = this._show;
            this.html.hide = this._hide;

            return this.html;
        },

        _buildOverlay: function () {
            return $('<div/>', {
                id: this.params.id || 'ModalOverlay',
                class: 'modal show',
                css: {
                    display: 'block'
                }
            });
        },

        _buildModal: function () {
            return $('<div/>', {
                class: 'modal-dialog'
            });
        },

        _buildCloseBtn: function () {
            return $('<div/>', {
                class: 'close-btn p-2',
                html: '&times;'
            });
        },

        build: function () {
            var Overlay = this._buildOverlay(),
                Modal = this._buildModal(),
                dialog = $('<div/>', {
                    class: "modal-dialog"
                });

            if (this.params.close) {
                this.closeBtn = this._buildCloseBtn();
                Modal.append(this.closeBtn);
            }

            Modal.append(this.params.header);
            Modal.append(this.params.body);
            Modal.wrapInner("<div class='modal-content'></div>");

            Overlay.append(Modal);

            this.Overlay = Overlay;

            $(document.body).append(Overlay);

            return this;
        },

        bindEvents: function () {
            var _self = this;

            if (this.closeBtn) {
                this.closeBtn.on('click', function () {
                    $('body').removeClass('modal-open');
                    _self.Overlay.remove();
                });
            }

            return this;
        },

        _show: function () {
            $('body').addClass('modal-open');
            this.find('.Modal').addClass('shown');
        },

        _hide: function () {
            $('body').removeClass('modal-open');
            this.remove();
        },

        get html() {
            return this.Overlay;
        }
    };


    function loadPage() {
        var $container = $('#ui-view');
        Cubix.SelfCheckout.bootstrap = new Bootstrap;

        if(window.sc_state != undefined) {
            $('.main').LoadingOverlay("hide", true);
            $container.LoadingOverlay("hide", true);
            $('.wait-for-load').removeClass("wait-for-load");

            return Cubix.SelfCheckout.bootstrap.runFromCache(window.sc_state);
        }

        $container.LoadingOverlay("show", {
            color: "rgba(239, 243, 249, 0.80)",
            zIndex: 1000
        });

        $.ajax({
            url: lang_id + '/online-billing-v2/index',
            dataType: 'json',
            success: function (data) {

                $('.wait-for-load').removeClass("wait-for-load");

                if(!data.available_packages.length) Notify.alert('warning', data.reason);

                if (data.user_type == 'escort') {
                    // button
                    if(data.escort.error || data.is_package_purchase_allowed == false){
                        if (data.escort.escort_status)
                        {
                            if ((data.escort.escort_status & 8) == 8) {
                                _draw_notification('owner-disabled');
                            }else if ((data.escort.escort_status & 128) == 128 && (data.escort.escort_status & 32) == 32 && data.escort.country_iso != 'us' && data.escort.country_iso != 'fr') {
                                Cubix.SelfCheckout.bootstrap.run(data.user_type, data);
                            }else if ((data.escort.escort_status & 67) == 67 && (data.escort.escort_status & 128) != 128) {
                                _draw_notification('complete_7_steps');
                            }else if ((data.escort.escort_status & 195) == 195) {
                                _draw_notification('waiting_for_approval');
                            }else if ((data.escort.escort_status & 2) == 2) {
                                _draw_notification('complete_7_steps');
                            }else if ((data.escort.escort_status & 128) == 128) {
                                _draw_notification('waiting_for_approval');
                            }else if ((data.escort.escort_status & 16384) == 16384) {
                                _draw_notification('is_inactive');
                            }else if ((data.escort.escort_status & 131072) == 131072){
                                Cubix.SelfCheckout.bootstrap.run(data.user_type, data);
                            }else{
                                _draw_notification('no_packages_availble');
                            }
                        }else{
                            _draw_notification('no_packages_availble');
                        }
                    }else if ((data.escort_status & 8) == 8) {
                        _draw_notification('owner-disabled');
                    } else if ((data.escort_status & 67) == 67 && (data.escort_status & 128) != 128) {
                        _draw_notification('complete_7_steps');
                    }else if ((data.escort_status & 195) == 195) {
                        _draw_notification('waiting_for_aproval');
                    }
                    else {
                        Cubix.SelfCheckout.bootstrap.run(data.user_type, data);
                    }
                } else if (data.user_type == 'agency') {
                    //if PHP's array converted to js object during JSON(encode/decode)
                    if (data.escorts.constructor.name === 'Object') {
                        data.escorts = Object.values(data.escorts)
                    }
                    //console.log(data, data.escorts, data.escorts.length);
                    if (data.escorts.length > 0) {
                        Cubix.SelfCheckout.bootstrap.run(data.user_type, data);
                    } else if(data.all_escorts_from_us_fr == 'all_escorts_from_us_fr'){
                        _draw_notification('all_escorts_from_us_fr');
                    }
                    else {
                        _draw_notification('escorts_no_found');
                    }

                }

                $('.main').LoadingOverlay("hide", true);
                $container.LoadingOverlay("hide", true);
            }
        })
    };

    return {
        load: loadPage
    };
})(window);
Cubix.SelfCheckout.load();
$('document').ready(function () {
    setTimeout(function () {
        if ($('#enable').length > 0) {
            $('#enable').on('click', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                $.ajax({
                    method: 'get',
                    url: url,
                    success: function (resp) {
                        if (resp) {
                            location.reload();
                        }
                    },

                    error: function () {
                        alert('Something went wrong.');
                    },

                });
            })
        }

    }, 2000);
})