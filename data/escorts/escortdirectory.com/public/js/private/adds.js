$( document ).ready(function() {

    $("#submenu-trigger").on("click",function(e){
        e.stopPropagation();
        $(".header-submenu-container").stop(true, true).slideToggle();
    });

     $('html').on("click",function(){
        if ($(".header-submenu-container").css("display") == "block") {
            $("#submenu-trigger").trigger("click");
        }
     });

     $(".remove-image").on("click",function(){
        $(this).parents(".image").fadeOut('slow',function(){
            $(this).parents(".image").remove();
        });
     });

    $(".escort-details").each(function(){
        
        text = $(this).find("p").html();
        link = $(this).parents(".escort-item").find("a").attr("href");
        len = 293;

        if (ismobile()) {
            len = 135;
        }

        if((text.length > text.substring(0, len).length)){
            text = text.substring(0, len - 3) + '...';

            if (ismobile()) {
                text = text + '<a href="#" class="grey" data-toggle="modal" data-target="#modalShowAd">Read more</a>';
            }
        }
        
        // return text; 
        $(this).find("p").html(text);
    });

    $(".ad-title").each(function(){
        
        text = $(this).html();
        len = 122;

        if (ismobile()) {
            len = 45;
        }

        if((text.length > text.substring(0, len).length)){
            text = text.substring(0, len - 3) + '...';
        }
        
        // return text; 
        $(this).html(text);
    });

    $(".upload-trigger").on("click",function(){
        $("#upload_materials").trigger("click");
    });

    function removeDropdownSelected(obj){
        var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
        $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
            $(this).show();
        });
        $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
            if ($(this).find("a").html() == val) {
                $(this).hide();
            }
        });
    }

    var labels = [];
    $('.multiple-dropdown-box').multiselect({ 
        
        onChange: function(option, checked, select){
            var index = labels.indexOf($(option).val());
            if (index !== -1) {
                labels.splice(index, 1);
            } else {
                labels.push($(option).val());
            }
        },
        buttonText: function(options) {
            var labels = [];
            options.each(function () {
                labels.push($(this).text())
            });

            if (options.length > 3) {
                return labels[0] + ',' + labels[1] + ' +' + options.length;
            } else if (options.length >= 1) {
                return labels.join(',');
            } else {
                return headerVars.dictionary.nothing_selected;
            }
        }
    });

    $(".dropdown-toggle").each(function(){
        removeDropdownSelected(this);
    });

    $(".dropdown-menu li a").on("click", function(){
        var val = $(this).html();
        var type = $(this).parents(".dropdown-menu").attr("data-type");
        $("#" + type).find(".dropdown-toggle-val").html(val);
        removeDropdownSelected(this);
    });

    $(".advanced-search").on("click",function(){
        $(this).html() == "Advanced search" ? $(this).html('Basic search') : $(this).html('Advanced search');

        if (ismobile()) {
            $(".advanced-buttons").stop(true, true).toggleClass("active");
        }

        $(".advanced-search-container").stop(true, true).slideToggle();
    });

    $("#cancel").on("click",function(){
        $(".advanced-search").trigger("click");
    });

    if (ismobile()) {
        autocomplete(document.getElementById("countryInput-mobile"), countries);   
    } else {
        autocomplete(document.getElementById("countryInput"), countries);
    }

    $(".intentions-container-mobile").on("click",function(){
        $(".filters-mobile").stop(true, true).fadeToggle();
    });

    $("#cancel-mobile").on("click",function(){
        $(".intentions-container-mobile").trigger("click");
    });

    $(".btn-dropdown-rounded").on("click",function(){
        $(this).parent(".d-flex").find(".btn-dropdown-rounded").removeClass("active");
        $(this).addClass("active");
    });

    $(".search-mobile").on("click",function(){
        var country = $("#countryInput-mobile").val();
        if (country !== "") {
            $("#myCountry-mobile-place").html(country);
        }

        if ($(".lookingFor-mobile").find(".btn-dropdown-rounded").hasClass("active")) {
            $(".lookingFor-mobile-place").html($(".lookingFor-mobile").find(".btn-dropdown-rounded.active").html());
        }

        if ($(".interestedIn-mobile").find(".btn-dropdown-rounded").hasClass("active")) {
            $(".interestedIn-mobile-place").html($(".interestedIn-mobile").find(".btn-dropdown-rounded.active").html());
        }

        $(".intentions-container-mobile").trigger("click");
    });

});

function ismobile(){
     var newWindowWidth = $(window).width();

        return (newWindowWidth < 768);
}
    