var Alert = {
    init:function() { 
    this.initCitySelect();
    this.initSave();
    this.initDelete();
    this.initAnyChange();
  },
  initCitySelect: function(){
    $('.select2').select2({
      theme: "bootstrap"
    });
  },

  initAnyChange: function(){
    $('.any-change').on('click',  function(event) {
        if(!$(this).is(':checked')){
          $(this).parents('form').find( ".event" ).prop('checked', false);
           $(this).parents('form').find( ".event" ).prop('disabled', false);
        }else{
          $(this).parents('form').find( ".event" ).prop('checked', true);
          $(this).parents('form').find( ".event" ).prop('disabled', true);
        }
    });
  },

  initSave: function(){
    $( "form" ).submit(function( event ) {
      event.preventDefault();
      var escortid = $(this).find('input[name=escort_id]').val();
      $('#escort-'+escortid).LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000 });
      
      var form = $(this).serializeArray();
      
      $.ajax({
        url: '/private/ajax-alerts-save',
        type: 'GET',
        data: form,
        success: function(res){
         Notify.alert('success','Your alerts successfully edited');
        },
        complete: function(){
          $('#escort-'+escortid).LoadingOverlay('hide');
        }
      })

    });
  },
  initDelete: function(){
    $('.remove-alert').on('click', function(event) {
      event.preventDefault();
      var escortid = $(this).data('id');
      $('#escort-'+escortid).LoadingOverlay("show", {color : "rgba(239, 243, 249, 0.80)", zIndex: 1000 });
      $.ajax({
        url: '/private/ajax-alerts-save',
        type: 'GET',
        data: {
          escort_id: $(this).data('id'),
          events: ''
        },
        success: function(res){
         Notify.alert('success','Your alert successfully deleted');
           $('#escort-'+escortid).LoadingOverlay('hide');
            APPLICATION.setup_url('alerts');
       
        },
        error: function(){
         // $('#escort-'+escortid).LoadingOverlay('hide');
         Notify.alert('error','Unexpected error');
        }
      })
    });
  }
}

$(document).ready(function(){
  Alert.init();
}); 