var Tours = {
    init: function () {
        var self = this;
        this.initTours();
        this.initHistorySwitcher();
        this.addEditTour();
        this.initDatepicker();


        $('#tour-escort').on('change', function () {
            self.initTours();
            if ($(this).val() != -1) {
                $('#addTour').removeClass('btn-secondary').addClass('btn-success');
            } else {
                $('#addTour').removeClass('btn-success').addClass('btn-secondary');
            }

        })

    },

    initTours: function () {
        var escortID = $('#escort_id').val();

        this.getTours(escortID, 0);
        if ($('#tours-history-switcher').val() == 0) {
            this.getTours(escortID, 1);
        }
    },

    removeTour: function (id) {
        $('.my-tour[data-tour-id="' + id + '"]').slideUp();
        $.ajax({
            url: '/private/remove-tours',
            data: {
                tours: id,
            }
        })
    },

    submitForm: function () {
        var self = this;
        event.preventDefault();

        if (headerVars.currentUser && headerVars.currentUser.user_type == 'agency' && !$('#escort_id').val()) {
            return Notify.alert('danger', headerVars.dictionary['dash_please_select_model'])
        }

        var email = $('#tour-email').val();
        if (email && !Validators.is_email(email)) {
            return Notify.alert('danger', 'Invalid Email')
        }

        $("main").LoadingOverlay("show", {
            color: "rgba(239, 243, 249, 0.80)",
            zIndex: 1000
        });

        var form = $(event.target);
        var values = form.serializeArray();
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: values,
            success: function (response, status, xhr) {
                var a = $('input[name="a"]').val();
                var did_animation = false;
                try {
                    response = JSON.parse(response);

                    if (response.status == 'success') {
                        $('.is-invalid').removeClass('is-invalid');
                        $('[id^=validation-]').html('');
                        $('#errors-container').hide();
                        self.initTours();

                        if (!response['prevent_popup']) {
                            Notify.alert('success', headerVars.dictionary.changes_saved);
                        }

                        form.get(0).reset();
                        form.find('.dropdown-toggle-val').html(headerVars.dictionary.nothing_selected_html);

                    } else {

                        $('#errors-container').show();
                        $('.visible-on-error').hide();

                        $.each(response['errors'], function (key, value) {
                            $("[data-error='" + key + "']").addClass('is-invalid');
                            $('#validation-' + key)
                                .html(value)
                                .parents('.visible-on-error')
                                .show();

                            if (!did_animation && $('#validation-' + key).length) {
                                $('html, body').animate({
                                    scrollTop: $('#validation-' + key).offset().top - 100
                                }, 1000);

                                did_animation = true;
                            }
                        });

                    }

                } catch (e) {
                    console.warn(e.message)
                    $('#ui-view').html(response);
                } finally {
                    $("#ui-view").LoadingOverlay("hide", true);
                    $(".main").LoadingOverlay("hide", true);
                    $('html, body').animate({
                        scrollTop: $(".main").offset().top
                    }, 1000);
                }
            }
        });

    },

        construct_dropdown: function (params){

            var cities_count = 0;
            var exclude = params['exclude'] || [];

            try {
                var regions = JSON.parse(params.regions);
                var visible_html = "";
                var data_html = "";

                for (var title in regions) {

                    var data_html_chunk = "";
                    var visible_html_chunk = "";

                    regions[title].forEach(function (city) {
                        if(exclude.includes(city.id)) return;

                        cities_count++;
                        data_html_chunk += "<option value=\"" + city.id + "\">" + city.title + "</option>";
                        visible_html_chunk += "<li data-val=\"" + city.id + "\"><a href=\"javascript:void(0)\">" + city.title + "</a></li>";
                    });

                    if (title) {
                        data_html += "<optgroup label=\"" + title + "\" >" + data_html_chunk + "</optgroup>";
                        visible_html += "<ul class=\"sub-dropdown-menu\"> <li class=\"mb-2 mt-2 d-block\"> <b> " + title + " </b> </li> " + visible_html_chunk + "</ul>";
                    } else {
                        data_html += data_html_chunk;
                        visible_html += visible_html_chunk;
                    }
                }

                params.select.html(data_html).val(null); // This is only <option> s
                params.ul.html(visible_html); // This one is for ul>li that is visible for user
            } catch (e) {
                console.warn(e.message);
            }

            return {
                cities: cities_count
            };
        },
    construct_cities_by_region_dropdown: function (params){

            var cities_count = 0;
            var exclude = params['exclude'] || [];

            try {
                var regions = JSON.parse(params.regions);
                var visible_html = "";
                var data_html = "";

                for (var title in regions) {

                    var data_html_chunk = "";
                    var visible_html_chunk = "";

                    regions[title].forEach(function (city) {
                        if(exclude.includes(city.id)) return;

                        cities_count++;
                        data_html_chunk += "<option value=\"" + city.id + "\">" + city.title + "</option>";
                        visible_html_chunk += "<li data-val=\"" + city.id + "\"><a href=\"javascript:void(0)\">" + city.title + "</a></li>";
                    });

                    data_html += data_html_chunk;
                    visible_html += visible_html_chunk;
                }

                params.select.html(data_html).val(null); // This is only <option> s
                params.ul.html(visible_html); // This one is for ul>li that is visible for user
            } catch (e) {
                console.warn(e.message);
            }

            return {
                cities: cities_count
            };
        },
    construct_region_dropdown: function (params){

            var cities_count = 0;
            var exclude = params['exclude'] || [];

            try {
                var regions = JSON.parse(params.regions);
                var visible_html = "";
                var data_html = "";

                for (var title in regions) {

                    data_html += "<option value=\"" + regions[title] + "\">" + title + "</option>";
                    visible_html += "<ul class=\"sub-dropdown-menu\"> <li class=\"mb-2 mt-2 d-block\"> <li data-val=\"" + regions[title] + "\"><a href=\"javascript:void(0)\">" + title + "</a></li></ul>";

                }

                params.select.html(data_html).val(null); // This is only <option> s
                params.ul.html(visible_html); // This one is for ul>li that is visible for user
            } catch (e) {
                console.warn(e.message);
            }

            return {
                cities: cities_count
            };
        },

    update_cities_dropdown: function () {
        var self = this;
        var id = $('#country-select').val();
        // Clear other cities dropdown
        $('#cities').empty().val(null);
        $('#regions').empty().val(null);

        if(id == 68){
            $('.other-regions').removeClass('d-none');
            $('#otherCitiesFilter').parents('.dropdown').attr("disabled", "disabled");
            $('#otherCitiesFilter').find('.dropdown-toggle-val').addClass('pristine').html(headerVars.dictionary.nothing_selected_html);
            $('#otherCitiesFilter').siblings('ul[data-type="otherCitiesFilter"]').empty();
            $('#otherRegionsFilter').find('.dropdown-toggle-val').addClass('pristine').html(headerVars.dictionary.nothing_selected_html);
            $('#otherRegionsFilter').siblings('ul[data-type="otherRegionsFilter"]').empty().append("Loading ...");
            $('#otherRegionsFilter').parents('.dropdown').first().removeAttr('disabled');
            $.ajax({
                url: '/geography/ajax-get-regions-grouped?json=1',
                data: {
                    country_id: id
                },
                success: function (r) {
                    self.construct_region_dropdown({
                        regions: r,
                        select: $('#regions'),
                        ul: $('#otherRegionsFilter').siblings('ul[data-type="otherRegionsFilter"]')
                    });

                    // Let's hide duplicate option in other cities and base city
                    $('#otherRegionsFilter').siblings('ul[data-type="otherRegionsFilter"]').find('li').each(function () {
                        if ($(this).data('val') == $('#base-city').val())
                            $(this).hide();
                    })
                }
            })
        }else{
            $('#otherRegionsFilter').parents('.dropdown').attr("disabled", "disabled");
            $('#otherRegionsFilter').find('.dropdown-toggle-val').addClass('pristine').html(headerVars.dictionary.nothing_selected_html);
            $('#otherRegionsFilter').siblings('ul[data-type="otherRegionsFilter"]').empty();
            $('#otherCitiesFilter').find('.dropdown-toggle-val').addClass('pristine').html(headerVars.dictionary.nothing_selected_html)
            $('#otherCitiesFilter').siblings('ul[data-type="otherCitiesFilter"]').empty().append("Loading ...");
            $('#otherCitiesFilter').parents('.dropdown').first().removeAttr('disabled')

            $.ajax({
                url: '/geography/ajax-get-cities-grouped?json=1',
                data: {
                    country_id: id
                },
                success: function (r) {
                    self.construct_dropdown({
                        regions: r,
                        select: $('#cities'),
                        ul: $('#otherCitiesFilter').siblings('ul[data-type="otherCitiesFilter"]')
                    });

                    // Let's hide duplicate option in other cities and base city
                    $('#otherCitiesFilter').siblings('ul[data-type="otherCitiesFilter"]').find('li').each(function () {
                        if ($(this).data('val') == $('#base-city').val())
                            $(this).hide();
                    })
                }
            })
        }

    },
    update_region_cities_dropdown: function () {
        var self = this;
        var id = $('#country-select').val();
        var region_id = $('#regions').val();
        // Clear other cities dropdown
        $('#cities').empty().val(null);
        $('#otherCitiesFilter').find('.dropdown-toggle-val').addClass('pristine').html(headerVars.dictionary.nothing_selected_html);
        $('#otherCitiesFilter').siblings('ul[data-type="otherCitiesFilter"]').empty().append("Loading ...");
        $('#otherCitiesFilter').parents('.dropdown').first().removeAttr('disabled');

        $.ajax({
            url: '/geography/ajax-get-cities-by-regions?json=1',
            data: {
                country_id: id,
                region_id: region_id
            },
            success: function (r) {
                self.construct_cities_by_region_dropdown({
                    regions: r,
                    select: $('#cities'),
                    ul: $('#otherCitiesFilter').siblings('ul[data-type="otherCitiesFilter"]')
                });

                // Let's hide duplicate option in other cities and base city
                $('#otherCitiesFilter').siblings('ul[data-type="otherCitiesFilter"]').find('li').each(function () {
                    if ($(this).data('val') == $('#base-city').val())
                        $(this).hide();
                })
            }
        })
    },

    initDatepicker: function () {

        var update_datepickers = function (hard = false) {
            if(hard) $('#date-picking-block .date').datepicker("destroy");

            var start = $('#date-from').val();
            var end = $('#date-to').val();

            if(start) {
                start = start.split('-');
                var parts = [];
                start.forEach(function(part) {
                    if(part.length <= 1) {
                        part = 0 + part;
                    }
                    parts.push(part)
                });
                start = parts.join('-');
            }

            if(end) {
                end = end.split('-');
                var parts = [];
                end.forEach(function(part) {
                    if(part.length <= 1) {
                        part = 0 + part;
                    }
                    parts.push(part)
                });
                end = parts.join('-');
            }

            var options1 = {
                'format': 'yyyy-m-d',
                'autoclose': true,
            };

            if(start ) {
                options1.startDate = new Date(start);
            }

            var options2 = {
                'format': 'yyyy-m-d',
                'autoclose': true,
            };

            if(end ) {
                options2.endDate = new Date(end);
            }

            $('#date-picking-block .date.start').datepicker(options2);
            $('#date-picking-block .date.end').datepicker(options1);

        };

        update_datepickers();

        $('#date-from, #date-to').change(function() {
            update_datepickers();
        });

        $('#date-picking-block').datepair({
            'defaultDateDelta': 1, // Half our
            parseDate: function (input) {
                var selector = input.getAttribute('data-anchor');
                var target = document.querySelector(selector);

                target.value = input.value;

                update_datepickers(true);
            },
        });

    },

    initHistorySwitcher: function () {
        var self = this;
        $('#tours-history-switcher').on('click', function (event) {
            var SwitcherValue = $(this);
            $.ajax({
                url: 'private/tours/toggle-tour-history',
                type: 'POST',
                data: {
                    o_t_v: SwitcherValue.val(),
                },
                success: function () {
                    APPLICATION.setup_url('tours/index/');
                }
            })
        });
    },

    addEditTour: function (tourID) {
        var self = this;
        $('#addTour').on('click', function (event) {
            var escortID = $('#tour-escort').find(":selected").val();
            if (escortID == -1) {
                $('#tour-escort').popover('show');
            } else {
                $('#tour-escort').popover('hide');
                $('.tours-block').LoadingOverlay("show", {
                    color: "rgba(239, 243, 249, 0.80)",
                    zIndex: 1000
                });
                self.getPopup(escortID);
            }
        });
    },

    saveTour: function (tour) {
        var self = this;
        $.ajax({
            'url': '/private/tours/edit-tour',
            type: 'POST',
            data: tour,
            success: function (response) {
                self.initTours();

                try {
                    response = JSON.parse(response);
                    if (typeof response == 'object' && response['status'] == 'success') {
                        $('.add-tour-form-modal').modal('hide');
                        Notify.alert('success', headerVars.dictionary.changes_saved);
                    } else {
                        if (typeof response == 'object' && response['status'] == 'error') {
                            $.each(response['msgs'], function (key, value) {
                                $("[name='" + key + "']").addClass('is-invalid');
                                $('#validation-' + key).html(value);
                            });
                        }
                    }
                } catch (e) {
                    Notify.alert('error', "Unexpected error when adding tour");
                }
            }
        })

    },

    getTours: function (escortid, history, page) {
        $('#escorts-tours').popover('hide');
        var self = this;
        history = typeof history !== 'undefined' ? history : 0;
        page = typeof page !== 'undefined' ? page : 1;

        if (typeof escortid == "undefined") {
            escortid = $('#tour-escort').find(":selected").val()
        }
        $('.other-regions').addClass('d-none');
        $('#tour-escort').popover('hide');
        var tours_container_block = $(".my-tour-container");
        var tours_history_container_block = $(".tours-history-block");

        var tours_container = $("#ajax-target");
        var tours_history_container = $("#ajax-target-old");

        $.ajax({
            'url': lang_id + '/private/ajax-tours',
            'type': 'POST',
            'data': {
                escort_id: escortid,
                old_tour: history,
                page: page
            },
            beforeSend: function () {
                if (history) {
                    tours_history_container_block.LoadingOverlay("show", {
                        color: "rgba(239, 243, 249, 0.80)"
                    });
                } else {
                    tours_container_block.LoadingOverlay("show", {
                        color: "rgba(239, 243, 249, 0.80)"
                    });
                }
            },
            success: function (data) {

                if (history) {
                    tours_history_container.html(data);
                    self.initPagination('.old-tours-pagination');
                    //var table = $('#escorts-tours-old').DataTable({"bInfo" : false, "bLengthChange": false });
                } else {

                    tours_container.html(data);
                    self.initPagination('.tours-pagination');
                    //var table = $('#escorts-tours').DataTable({"bInfo" : false, "bLengthChange": false });
                }
            },
            complete: function () {
                if (history) {
                    tours_history_container_block.LoadingOverlay("hide", true);
                } else {
                    self.initActions();
                    setTimeout(function () {
                        tours_container_block.LoadingOverlay("hide", true)
                    });
                    ;
                }
            }

        });
    },

    initPagination: function (pagination_wrapper) {
        var self = this;

        $(pagination_wrapper + ' .page-item .page-link').on('click', function (e) {
            e.preventDefault();

            var page = parseInt($(this).attr('data-page'));
            var isTourOld = parseInt($(pagination_wrapper).attr('data-is-tour-old'));
            var escortID = $('#tour-escort').find(":selected").val();
            self.getTours(escortID, parseInt(isTourOld), page);
        })
    },

    initActions: function () {
        var self = this
        // var table = $('#escorts-tours').DataTable();
        var selected_row = 0;
        $('.tours-list').on('click', '.tour', function () {
            if (this.id == 0) return false;
            $('#escorts-tours').popover('hide');
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                selected_row = 0;
                $('.delete-tour').addClass('disabled');
                $('.delete-tour.mobile').addClass('d-none');

                $('.edit-tour').addClass('disabled');
                $('.edit-tour.mobile').addClass('d-none');
            } else {
                $('.tours-list .tour').removeClass('selected');
                $(this).addClass('selected');
                selected_row = $(this);
                $('.delete-tour').removeClass('disabled');
                $('.delete-tour.mobile').removeClass('d-none');
                $('.edit-tour').removeClass('disabled');
                $('.edit-tour.mobile').removeClass('d-none');
            }
        });

        // DELETE
        $('.delete-tour').click(function () {
            if (selected_row != 0) {
                var tourdeleteConfirm = confirm("Are you sure you want to delete " + selected_row.data('showname') + "'s tour");
                if (tourdeleteConfirm == true) {
                    $.ajax({
                        url: '/private/tours/remove-tours',
                        data: {
                            tours: selected_row.attr('id'),
                        },
                        success: function (data) {
                            Notify.alert('success', "Tour has been successfully deleted!");
                            self.getTours();
                        },
                        error: function (data) {
                            alert('Unexpected error');
                        },
                    })
                }
            } else {
                $('#escorts-tours').popover('show');
            }
        });

        // EDIT
        $('.edit-tour').click(function () {
            if (selected_row != 0) {
                escortID = 0;
                $('.tours-block').LoadingOverlay("show", {
                    color: "rgba(239, 243, 249, 0.80)",
                    zIndex: 1000
                });
                self.getPopup(escortID, selected_row.attr('id'));
                $('.tours-block').LoadingOverlay("hide");
                selected_row.trigger('click');

            } else {
                $('#escorts-tours').popover('show');
            }
        });
    },
    getPopup: function (escortID, tourid) {
        var self = this;
        tourid = typeof tourid !== 'undefined' ? tourid : 0;

        var get = '';
        if (escortID != 0) {
            get += 'escort_id=' + escortID;
        } else if (tourid != 0) {
            get += 'id=' + tourid;
        } else {
            return false;
        }

        $.ajax({
            'url': '/private/tours/edit-tour?' + get,
            type: 'GET',
            success: function (data) {
                $('#inner-notifications').append(data);
                $('.add-tour-form-modal').modal('show');

                $('.input-daterange ').datepicker({
                    language: "en-GB",
                    startDate: new Date(),
                    todayHighlight: true,
                    autoclose: true,
                    format: 'dd M yyyy',
                });

                $('.tour-times-block .time').timepicker({
                    'showDuration': true,
                    'timeFormat': 'H:i'
                });

                $('.tour-times-block ').datepair();

                $('#tourtime').on('change', function (event) {
                    if (this.checked) {
                        $('.tour-times-block input').attr('readonly', false);
                    } else {
                        $('.tour-times-block input').val('');
                        $('.tour-times-block input').attr('readonly', true);
                    }
                });

                $('.submit-btn-add-tour').on('click', function (event) {
                    event.preventDefault();
                    var form = $('#add-tour-form').serializeArray();

                    self.saveTour(form);
                });

                $('.add-tour-form-modal').on('hidden.bs.modal', function () {
                    $('#inner-notifications').html('');
                })
            },
            complete: function () {
                $('.tours-block').LoadingOverlay('hide');
            }
        })

    }
};

$('.geolocation').on('keyup',function () {
    var searchIng = $(this).val();
    var states = $('li.states');
    if (searchIng && states)
    {
        states.hide();
    }else{
        states.show();
    }
    searchIng = new RegExp(searchIng.toUpperCase());
    $(this).parent('ul').find('li.searchable').each(function() {
        var searchIn = $(this).children('a').text().trim();
        searchIn = searchIn.toUpperCase();
        var states = $('li.states');
        if (!searchIng.test(searchIn))
        {
            $(this).hide();
        }else{
            $(this).show();
        }
    });

});
$('#country-dropdown').click(function () {
    var parent = $(this).next('ul');
    setTimeout(function(){parent.children('input').focus();},10);
});

$(document).ready(function () {
    Tours.init();
});