var Blacklist_Page = (function () {

    function Blacklist_Page() {};

    Blacklist_Page.prototype.load = function(type, clear_params, page) {
    
            event.preventDefault();
            var url = lang_id + '/client-hotel-blacklist/' + type,
                $container = $("#ui-view"),
                $form = $('#search-form'),
                data = {};
        
            if ( ! page ) {
                page = {};
            } else {
                page = {'page': page};
            }
        
            if ( $form.length && ! clear_params ) {
                data = {};
    
                $form.serializeArray().forEach(function(item) {
                    data[item.name] = item.value
                });
            }
    
            $container.LoadingOverlay("show", {
                color: "rgba(239, 243, 249, 0.80)",
                zIndex: 1000
            });
    
            $.ajax({
                url: url,
                type: "post",
                data: Object.assign({}, data, page),
                success: function(response) {
                    $container.html(response);
                    $container
                        .LoadingOverlay("hide")
                        .html(response);

                    $('.to-the-moon').trigger('click');
                }
                
            })
    }
    
    Blacklist_Page.prototype.helpers = {
    
        construct_dropdown: function (params) {

            try {
                var regions = JSON.parse(params.regions);
                var visible_html = '';
                var data_html = '';

                for (var title in regions) {

                    var data_html_chunk = '';
                    var visible_html_chunk = '';

                    regions[title].forEach(function (city) {
                        data_html_chunk += '<option value="' + city.id + '">' + city.title + '</option>';
                        visible_html_chunk += '<li data-val="' + city.id + '"><a href="javascript:void(0)">' + city.title + '</a></li>';
                    });

                    if (title) {
                        data_html += '<optgroup label="' + title + '" >' + data_html_chunk + '</optgroup>';
                        visible_html += '<ul class="sub-dropdown-menu"> <li class="mb-2 mt-2"> <b> ' + title + ' </b> </li> ' + visible_html_chunk + '</ul>';
                    } else {
                        data_html += data_html_chunk;
                        visible_html += visible_html_chunk;
                    }
                }

                params.select.html(data_html).val(null); // This is only <option> s
                params.ul.html(visible_html); // This one is for ul>li that is visible for user
            } catch (e) {
                console.warn(e.message);
            }
        },
    
        ajaxCities: function (id) {
            var self = this;
            // Clear other cities dropdown
            $('#city-dropdown').find('.dropdown-toggle-val').addClass('pristine').html(headerVars.dictionary.nothing_selected_html)
            $('#city-dropdown').siblings('ul[data-type="city-dropdown"]').empty().append(headerVars.dictionary.loading + " ...");
    
            $.ajax({
                url: '/geography/ajax-get-cities-grouped?json=1',
                data: {
                    country_id: id
                },
                success: function (r) {
                    self.helpers.construct_dropdown({
                        regions: r,
                        select: $('#cities'),
                        ul: $('#city-dropdown').siblings('ul[data-type="city-dropdown"]')
                    });
                }
            })
        }
    }
    
    Blacklist_Page.prototype.event_handlers = {
    
        pagination_activate: function() {
    
            event.preventDefault();
            var $btn = $(event.target),
                params = $btn.data(),
                clear_params = params['clearParams'],
                page = params['page'],
                type = params['type'];
            
            this.load(type, clear_params, page);
    
            return false;
        },
    
        search_form_submit: function() {
            
            event.preventDefault();
            var $form = $(event.target)
                type = $form.find('input[name="type"]').val();
            
            this.load(type, false);
            return false;
        },
    
        country_changed: function () {
    
            var id = $('#country-select').val();
            this.helpers.ajaxCities.call(this, id);
        },
    
        toggle_newlist_form: function() {
            var $form = $('#add-to-blacklist');
            $form.slideToggle();
        },
    
        hide_newlist_form: function() {
            var $form = $('#add-to-blacklist');
            $form.slideUp().get(0).reset();

            $form.find('.dropdown-toggle-val').html(headerVars.dictionary.nothing_selected_html)
        },
    
        new_blacklist_submit : function() {
    
            event.preventDefault();
            var $form = $('#add-to-blacklist'),
                $container = $('#ui-view'),
                url = $form.attr('action'),
                fields = $form.serializeArray();
    
            $container.LoadingOverlay("show", {
                color: "rgba(239, 243, 249, 0.80)",
                zIndex: 1000
            });
    
            var data = {};
            fields.forEach(function(item) {
                data[item.name] = item.value;
            });
    
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                success: function(response) {
                    var did_animation = false;
                    $container.LoadingOverlay("hide");
    
                    $('#errors-container').show();
                    $('[id^=validation-]')
                        .html('')
                        .parents('.visible-on-error')
                        .hide();
    
                    try{
                        response = JSON.parse(response);
    
                        if(response.status == "success"){
                            $('#errors-container').hide();
                            Notify.alert('success', headerVars.dictionary.blacklist_submit_success);
                            $form[0].reset()
                        }else{
                            
                            $.each(response['msgs'], function (key, value) {
                                
                                $('#validation-' + key)
                                    .html(value)
                                    .parents('.visible-on-error')
                                    .show();
                                
                                if( !did_animation && $('#vlaidation-' + key).length)
                                {
                                    $('html, body').animate({
                                        scrollTop: $('#validation-' + key).offset().top - 100
                                    }, 1000);
            
                                    did_animation = true;
                                }
                            });
                            
                        }
    
    
                    }catch(e){
                        console.warn(e.message);
                    }
                }
            })
    
            return false;
        }   
    }
    
    Blacklist_Page.prototype.bind_events = function () {
        $(document)
            .on('click', '.pagination-link', this.event_handlers.pagination_activate.bind(this))
            .on('submit', '#search-form', this.event_handlers.search_form_submit.bind(this))
            .on('click', '.show-form', this.event_handlers.toggle_newlist_form.bind(this))
            .on('click', '.hide-form', this.event_handlers.hide_newlist_form.bind(this))
            .on('submit', '#add-to-blacklist', this.event_handlers.new_blacklist_submit.bind(this))
            .on('change', '#country-select', this.event_handlers.country_changed.bind(this))
        
    }
    
    Blacklist_Page.prototype.init_datepicker = function() {
        $('.blacklist-page .date').datepicker({
            'format': 'm/d/yyyy',
            'autoclose': true,
        });
    }
    
    Blacklist_Page.prototype.init = function () {

        try {
            
            if (!window.BLACKLIST_PAGE) {
                this.bind_events();
                this.init_datepicker();
            }

        } catch (e) {
            console.error("Whooops, there was an Error !!!", e)
        }
        return this;
    
    }

    return Blacklist_Page;
})();


$(function () {
    window.BLACKLIST_PAGE = (new Blacklist_Page()).init();
})