EscortProfile = {};

EscortProfile.Send = function () {
    alert("SEND");
};

EscortProfile.Next = function () {
    alert("nextTab");
};

EscortProfile.SendFull = function () {
    alert("SendFull");
};

EscortProfile.scrollToElement = function (element) {

    var $element = $('[data-scroll-anchor="' + element + '"]');
    if (!$element.length) return console.warn("Couldn't scroll to element");

    $('html, body').animate({
        scrollTop: $element.offset().top - 50
    }, 300)
};

EscortProfile.inProcess = false;

EscortProfile.ShowFull = function (url) {

    // Getting section to scroll to it
    // -------------------------------------------------------
    var section = window.location.hash.slice(1).split('?')[0];
    // -------------------------------------------------------

    if (EscortProfile.inProcess) return false;
    // Already in this page -> just scroll to the right content
    // --------------------------------------------------------
    if (!$('.edit-full-page-content').length) {
        EscortProfile.inProcess = true;
        var $container = $('#ui-view');
        $container.LoadingOverlay('show');

        return $.ajax({
            url: url,
            type: 'get',
            success: function (resp) {

                EscortProfile.inProcess = false;
                $container.html(resp);
                EscortProfile.scrollToElement(section);
                $container.LoadingOverlay('hide');

                // $container.find('form .submit').on('click', EscortProfile.Send);
                // if ($container.find('form .next-tab')) {
                //     $container.find('form .next-tab').on('click', EscortProfile.nextTab);
                // }
                // $container.find('form .discard').on('click', EscortProfile.Show);
                // $container.find('.submit-all').on('click', EscortProfile.SendFull);

            }
        });
    }else{
        EscortProfile.scrollToElement(section);
    }
};

