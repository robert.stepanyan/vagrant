var Global = {};
Global.uploaded_files = [];
var Steps = {
    init: function () {
        this.initElements();

        return true;
    },

    initElements: function () {
        this.initFormSubmit();
        this.initResetForm();
        this.initBiography();
        this.initAboutMe();
        this.initWorkingCities();
        this.initWorkingTimes();
        this.initContactInfo();
        this.initGallery();
        this.initVideo();
        this.initPrices();
        this.initTimepairs();
        this.initDaysjs();
        this.initServices();
        this.initFinishStep();
    },

    initFinishStep: function() {

        var emailSendCountdown = 5;

        $('#resent_email').click(function() {

            if(emailSendCountdown != 5) return;

            var $elm = $(this);
            $elm.LoadingOverlay('show');

            $.ajax({
                type: 'GET',
                url: '/account/signup?fromPage=complete_profile&user_id=' + headerVars.currentUser.id,
                dataType: 'json',
                success: function(req) {

                    if(req.status == 'success') {
                        Notify.alert('success', req.msg);
                        $elm.attr('disabled', true);

                        var _interval = setInterval(function(){
                            emailSendCountdown -= 1;

                            if(emailSendCountdown <= 0) {
                                clearInterval(_interval);
                                emailSendCountdown = 5;
                                $elm.removeAttr('disabled');
                            }

                        }, 1000);
                    }else{
                        Notify.alert('error', 'Couldn\'t send email.')
                    }
                }
            }).done(function() {
                $elm.LoadingOverlay('hide');
            })
        });
    },

    initResetForm: function(){

        $(document).on('click', 'form a.discard', function (event) {
            var url = location.hash.replace(/^#/, '');
            if (url != '') {
                APPLICATION.setup_url(url);
            }
        });
    },
    initFormSubmit: function () {

        var self = this;

        $(document)
            .off('submit', 'form.ajax-form')
            .on('submit', 'form.ajax-form', function (event, data) {
                var $container = Sceon.isMobile() ? $('html') : $('main');

                if (data === undefined) {
                    data = null
                }
                event.preventDefault();
                $container.LoadingOverlay("show", {
                    color: "rgba(239, 243, 249, 0.80)",
                    zIndex: 1000
                });

                var clicked_button = $(document.activeElement);
                var action;

                if (data && data['callback_value'] == 'back')
                    action = 'back'
                else
                    action = (clicked_button.attr('value')) ? clicked_button.attr('value') : ':next';

                $('input[name=then]').val(action);
                var values = $('form').serializeArray();
                var url = $(this).attr('action') ? $(this).attr('action') : 'private/profile/index?ajax=true';
                $.ajax({
                    type: "POST",
                    url: url,
                    data: values,
                    success: function (response, status, xhr) {
                        var a = $('input[name="a"]').val();
                        var did_animation = false;
                        $('.is-invalid').removeClass('is-invalid');

                        try {
                            response = JSON.parse(response);
                            if (response.success) {

                                if (!response['prevent_popup']) {
                                    Notify.alert('success', headerVars.dictionary.changes_saved);
                                }

                                $('[id^=validation-]').html('')
                                $('#errors-container').hide();
                                if (response.data) {
                                    $('#ui-view').html(response.data)
                                }
                            } else {
                                if (typeof response.redirect_to_signin != "undefined") {
                                    window.location.href = response.redirect_to_signin;
                                }
                                $('#errors-container').show();
                                $('[id^=validation-]')
                                    .html('')
                                    .parents('.visible-on-error')
                                    .hide();

                                $.each(response['errors'], function (key, value) {
                                    $("[data-error='" + key + "']").addClass('is-invalid');
                                    $('#validation-' + key)
                                        .html(value)
                                        .parents('.visible-on-error')
                                        .show();

                                    if (!did_animation && $('#vlaidation-' + key).length) {
                                        $('html, body').animate({
                                            scrollTop: $('#validation-' + key).offset().top - 100
                                        }, 1000);

                                        did_animation = true;
                                    }
                                });

                            }

                            if (a === 'set-rotate' && response.success) {
                                var img = $('#photo-' + response.photo);
                                var src = img.attr('src');
                                var re = /((\?|\#).*)$/;
                                src = src.replace(re, "") + '?cache=' + (new Date()).getTime();
                                img.attr('src', src);
                            }

                        } catch (e) {
                            console.warn(e.message)
                            $('#ui-view').html(response);
                            self.initElements();
                            Notify.alert('success', headerVars.dictionary.changes_saved);
                        } finally {
                            $("#ui-view").LoadingOverlay("hide", true);
                            $container.LoadingOverlay("hide", true);

                            $('html, body').animate({
                                scrollTop: $(".main").offset().top
                            }, 1000);
                        }

                    }
                });
            });


        $('#profile-tabs-selectbox').change(function (event) {
            var target = $(this).find(":selected").attr('data-ajax-url').replace(/\/private#?\/?/g, '').replace('#', '/');
            APPLICATION.setup_url(target);
        });

        $('#profile-tabs-selectbox').select2({
            theme: 'bootstrap',
            dropdownCssClass: "profile-tabs-selectbox",
            minimumResultsForSearch: -1
        });

    },


    initTimepairs: function () {

        $.each($('tr[id^="datepair-"]'), function (key, val) {

            $('#' + val.id + ' .time').timepicker({
                'showDuration': true,
                'timeFormat': 'H:i'
            });

            $('#' + val.id).datepair();
        });
    },

    initDaysjs: function () {
        $.each($('tr[id^="datepair-"]'), function (key, val) {
            $('#' + val.id + ' .day-checkbox').on('click', function () {
                if ($(this).is(':checked')) {
                    $('#' + val.id + ' .start').attr('disabled', false);
                    $('#' + val.id + ' .end').attr('disabled', false);
                } else {
                    $('#' + val.id + ' .start').val('');
                    $('#' + val.id + ' .end').val('');
                    $('#' + val.id + ' .start').attr('disabled', true);
                    $('#' + val.id + ' .end').attr('disabled', true);
                };
            });
        })

        $("#reset-working-time").on('click', function () {
            $('.day-checkbox').prop('checked', false);
            $('.start').attr('disabled', true);
            $('.end').attr('disabled', true);
            $('.start').val('');
            $('.end').val('');
        });
    },

    initBiography: function () {

        if (!$(' #select2-2').length) return false;

        $(' #select2-2').select2({
            theme: "bootstrap"
        });

    },
    initAboutMe: function () {

        //if (!$("#about-me").length) return false;

        $("#selecting-language-knowledge").select2({
            templateResult: formatState,
            theme: "bootstrap"
        });

        $(document).on('click', '#editor-container.about-me-area .nav-item', function() {
            var lng = $(this).attr('cx:lng');
            var editor = tinyMCE.get('about-' + lng);
            $('#mce-counter').text(editor.getContent({format: 'text'}).length);
        });

        $('#add-language').click(function () {
            var selected_language = $("#selecting-language-knowledge").val();
            var language_level = $('input[name="lng-lev"]:checked').val();
            var language_level_text = $('input[name="lng-lev"]:checked').parent('label').text();
            var selected_language_text = $("#selecting-language-knowledge").select2('data')[0].text;

            if (selected_language && language_level) {
                if ($('input[name="langs[' + selected_language + ']"]').length) {
                    $('input[name="langs[' + selected_language + ']"]')
                        .val(language_level)
                        .parent()
                        .find('.lang-level-text')
                        .html(language_level_text);

                    return false;
                }

                var languages_area = $('#selected_languages');
                var new_lang_area = $('<div/>', {
                        id: selected_language
                    })
                    .appendTo(languages_area);

                $('<span/>').append('<img src="/img/private/flags/flag_' + selected_language + '-big.png" /> ' + selected_language_text + ' - <span class="lang-level-text">' + language_level_text + '</span>')
                    .append('<i class="fa fa-times remove-language" aria-hidden="true"></i>')
                    .appendTo(new_lang_area);
                $('<input/>', {
                        type: 'hidden',
                        value: language_level,
                        name: 'langs[' + selected_language + ']'
                    })
                    .appendTo(new_lang_area);

            }
        });

        $('body').on('click', '.remove-language', function () {
            $(this).closest('div').remove();
        });

        function formatState(state) {

            if (!state.id) {
                return state.text;
            }
            var baseUrl = "/img/private/flags/";
            var $state = $(
                '<span><img src="' + baseUrl + '/flag_' + state.element.value.toLowerCase() + '-big.png" class="" /> ' + state.text + '</span>'
            );
            return $state;
        };



        $('body').on('mouseup','.dropdown-toggle',function () {
            var $body = $('body');
            var ariaExpand = $(this).attr('aria-expanded');

            if (ariaExpand === 'false' || !ariaExpand) {
                $body.addClass('freezed');
            } else{
                $body.removeClass('freezed');
            }
        });

        $(document).mouseup(function(e)
        {
            var container = $('.dropdown-toggle');

            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target))
            {
                $('body').removeClass('freezed');
            }
        });
    },

    initWorkingCities: function () {
        if (!$("#working-cities").length) return false;

        $("#base_city,#cities").select2({
            theme: "bootstrap"
        });

        $('body').on('change', '#base_city', function () {
            var city_id = this.value;
            if (!city_id) return false;

            $('#cities option').each(function () {
                $(this).prop('disabled', '');

                if ($(this).is(':selected') && $(this).val() == city_id) {
                    $(this).prop('selected', '');
                }
            });

            $('#cities option[value=' + city_id + ']').prop('disabled', 'disabled');

            $('#cities').select2({
                theme: "bootstrap"
            });

        });

        $('#incall').change(changeInCall);

        $('#outcall').change(changeOutCall);

        $('#incall_area input[name="incall_type"]').change(changeInCallType);
        $('#incall_area input[name="incall_hotel_room"]').change(changeInCallTypeRoom);

        $('#outcall_area input[name="outcall_type"]').change(changeOutCallType);

        $('input[name="outcall_other"], input[name="incall_other"]').on("focusin", function () {
            var attr = $(this).attr('name');
            var id = "#" + attr.replace('_', '-');
            $(id).prop('checked', true);
            if (attr === "incall_other") {
                $('#incall_area dl dt').each(function () {
                    $(this).find('input').prop('checked', false);
                });
                if (!$('#incall').is(':checked')) {
                    $('#incall').prop('checked', true)
                }
            } else {
                if (!$('#outcall').is(':checked')) {
                    $('#outcall').prop('checked', true)
                }
            }
        });

        function changeInCall() {
            var checked = $(this).is(':checked');
            if (!checked) {
                $('#incall_area dl dt').each(function () {
                    $(this).find('input').prop('checked', checked).removeAttr('required');
                });
                $('#incall_area dt').each(function () {
                    $('#input[name="incall_type"]').prop('checked', checked);
                    $(this).find('input[name="incall_type"]').prop('checked', checked);
                });
                if (!$('#incall-other').is(':checked')) {
                    $('input[name="incall_other"]').val('');
                }
            }
        }

        function changeOutCall() {
            var checked = $(this).is(':checked');
            if (!checked) {
                $('#outcall_area dt').each(function () {
                    $(this).find('input[name="outcall_type"]').prop('checked', checked);
                });

                if (!$('#outcall-other').is(':checked')) {
                    $('input[name="outcall_other"]').val('');
                }
            }
        }

        function changeOutCallType() {
            if (!$('#outcall').is(':checked')) {
                $('#outcall').prop('checked', true)
            }
            if ($(this).val() != $('#outcall-other').val()) {
                $('input[name="outcall_other"]').val('');
            }
        }

        function changeInCallType() {
            var checked = $(this).is(':checked');
            if (checked) {
                var check_hotel_room = ($(this).val() == $('#hotel-room').val()) ? checked : !checked;
                if (!$('#incall').is(':checked')) {
                    $('#incall').prop('checked', true)
                }
                if (!check_hotel_room) {
                    $('#incall_area input[name="incall_hotel_room"]').each(function () {
                        if ($(this).is(':checked')) {
                            $(this).prop('checked', false)
                        }
                        $(this).removeAttr('required')
                    })
                } else {
                    $('#incall_area input[name="incall_hotel_room"]').each(function () {
                        $(this).attr('required', true)
                    })
                }
                if (($(this).val() != $('#incall-other').val())) {
                    $('input[name="incall_other"]').val('');
                }
            }
            // var check_hotel_room = ($(this).val() == $('#hotel-room').val()) ? checked : !checked;
            // var check_other = ($(this).val() == $('#incall-other').val()) ? checked : !checked;
            //
            // $('#incall_area dl dt').each(function () {
            //     $(this).find('input').prop('checked', !check_hotel_room);
            // });
            //
            // $('input[name="incall_other"]').prop('disabled', !check_other);
        }

        function changeInCallTypeRoom() {
            var checked = $(this).is(':checked');
            if (checked) {
                if (!$('#hotel-room').is(':checked')) {
                    $('#hotel-room').prop('checked', true)
                }

                if (!$('#incall').is(':checked')) {
                    $('#incall').prop('checked', true)
                }

                $('input[name="incall_other"]').val('');
            }
        }

    },
    initWorkingTimes: function () {
        if (!$('#working-times').length) return false;

        $('#working-times-submit').click(function () {
            $('.modal').modal('hide');
            $(this).closest('form').submit();
        });

        $('#available_24_7').click(function () {
            var disabled = $(this).is(':checked');

            $('input[name*="day_index"]').each(function () {
                $(this).closest('tr').find('input,select').prop('disabled', 1);
                $('#selectall').prop('disabled', 1);
            });

        });

        $('#on_schedule').click(function () {
            $('tr.work-times-disabled').each(function () {

                var dayCheckbox = $(this).find('input.day-checkbox').prop('disabled', 0);
                if (dayCheckbox.is(':checked')) {
                    dayCheckbox.prop('disabled', 0);
                    $(this).find('input').prop('disabled', 0);
                }
            });

            $('#selectall').prop('disabled', 0);
        });

        $('#return_vacation').click(function () {

            var escort_id = $(this).attr('data-id');
            var self = $(this);

            $.ajax({
                url: '/private/profile/ajax-return-from-vacation',
                type: 'POST',
                data: {
                    escort_id: escort_id
                },
                success: function (data) {
                    data = JSON.parse(data);
                    if (data.success) {
                        self.parent().remove();
                    }
                }
            });

        });

        // $('input[name*="day_index"]').each(function () {
        //     console.log('test');
        //     $(this).closest('tr').find('select').prop('disabled',!$(this).is(':checked'));

        // });


        // $('select').select2({theme: "bootstrap"});

    },

    initContactInfo: function () {
        if (!$('#contact-info').length) return false;
        $('#phone_prefix').select2({
            theme: "bootstrap"
        });
        $('#phone_prefix').on('select2:select', function (e) {
            $("#phone").unmask();
        });
    },
    initGallery: function () {

        $('#upload_photos').change(function (el) {

            var get_params = '?ajax=true&a=upload';
            if ($('input[name="in_step"]').length) {
                get_params += '&in_step=1';
            }

            if ($('input[name="escort"]').length) {
                get_params += '&escort=' + $('input[name="escort"]').val();
            }

            var filename = null;

            $(this).simpleUpload("/private/profile/gallery" + get_params, {

                allowedExts: ["jpg", "jpeg", "png"],
                allowedTypes: ["image/jpeg", "image/png", "image/x-png", "image/jpg"],
                limit: 50,
                maxFileSize: 5000000, //5MB in bytes

                start: function (file) {

                    if ($.inArray(file.name, Global.uploaded_files) == -1) {
                        filename = file.name;
                        Global.uploaded_files.push(file.name);
                    } else {
                        Notify.alert('danger', "<span>"+headerVars.dictionary.dont_upload_same_photo +"</span>");

                        return false;
                    }

                    this.block = $('<div class="block"></div>');
                    this.progressBar = $('<div class="progressBar"></div>');



                    this.block.append(this.progressBar);
                    $('#uploads').append(this.block);

                },

                progress: function (progress) {
                    //received progress
                    this.progressBar.width(progress + "%");
                },

                success: function (data) {
                    $('#ui-view').html(data);

                    Steps.initElements();

                    $("#ui-view").LoadingOverlay("hide", true);
                    $("main").LoadingOverlay("hide", true);
                },

                error: function (error) {

                    var index = Global.uploaded_files.indexOf(filename);
                    if(index >= 0) Global.uploaded_files.splice(index, 1);

                    //upload failed
                    this.progressBar.remove();
                    this.block.remove();
                    error = error.message;
                    var errorDiv = null;

                    if (!$('#error_uploader').length) {
                        errorDiv = $('<div class="error text-danger" id="error_uploader"></div>').text(error);
                        $('#uploads').append(errorDiv);
                    } else {
                        errorDiv = $('#error_uploader').html(error);
                    }

                },


            });

        });


        $('.gallery_buttons').click(function () {
            var value = $(this).val();
            if (value === 'make-private') {
                $('input[name="is_private"]').val("1");
            }
            $('input[name=a]').val($(this).attr('name'));
            // $('<input/>',{type:'hidden',name:$(this).attr('name'),value:$(this).val()}).appendTo('form:first-child');
        });

        $('button.rotate').click(function (e) {
            var d = $(this).hasClass('rotate-left') ? 90 : -90;
            $(this).siblings('input[type="checkbox"]').prop('checked', true);
            var value = $(this).val();
            $('input[name="a"]').val(value);
            $('input[name="degree"]').val(d);
        });

        $('.photos-switcher').click(function (e) {
            e.preventDefault();
            var value = $(this).attr('data-value');

            if (value === 'private') {
                $('#public_area').removeClass('d-none');
                $('#private_area').addClass('d-none');
            } else {
                $('#public_area').addClass('d-none');
                $('#private_area').removeClass('d-none');
            }

        });
    },
    Play: function (video, image, width, height, auto) {

        $('.video-modal')
            .modal()
            .find('.modal-dialog')
            .css({
                width: width + 'px',
                height: height + 'px'
            });

        jwplayer('video-modal').setup({
            height: height,
            width: '100%',
            image: image,
            autostart: auto,
            skin: {
                name: "bekle"
            },
            logo: {
                file: "/img/video/logo.png"
            },
            file: video
        });

        $(".video-modal").on("hidden.bs.modal", function () {
            $('#video-modal').html("");
        });
    },

    initVideo: function () {

        var __self = this;

        __self.formInput = $('#fileupload');
        __self.uploadVideo();

        $('#remove_video').click(function (e) {
            e.preventDefault();
            $('#video-block').LoadingOverlay('show', true);
            __self.removeVideo($(this).attr('data-id'));
            $('#video-block').LoadingOverlay('hide', true);
        });

        $('#video_area').click(function () {
            var config = $(this).find('#video_config');
            var image = config.attr('src');
            var url = config.attr('data-url');
            var video = config.attr('data-video');
            var height = config.attr('data-height');
            var width = config.attr('data-width');

            video = url + video + '_' + height + 'p.mp4';

            __self.Play(video, image, width, height, true);

        });
    },

    removeVideo: function (video_id) {
        if ($('input[name="escort"]').length) {
            var params = '?escort_id=' + $('input[name="escort"]').val();
        } else {
            var params = '';
        }
        if (Number.prototype.isNumeric(video_id)) {
            $.ajax({
                url: '/private/video/video' + params,
                type: 'POST',
                data: {
                    ajax: true,
                    video_id: video_id,
                    act: 'delete',
                    escort: $('input[name="escort"]').val()
                },
                success: function () {
                    if ($('button[name="finish-step"]').length) {
                        $('input[name="a"]').remove();
                        $('button[name="finish-step"]').click();
                    } else {
                        // var gallery_tab = $('#pills-gallery-tab');
                        // gallery_tab.attr('data-ajax-url',gallery_tab.attr('data-ajax-url')+'&'+ Math.round(Math.random() * 100000)).click();
                        $('button[name="hidden_button"]').click();
                    }

                    setTimeout(function () {
                        Notify.alert('success', 'Your video has been successfuly deleted !');
                    }, 1000);

                }
            });
        }
    },

    initPrices: function () {
        if (!$('#prices').length) return false;

        $('.unit').select2({
            theme: 'bootstrap'
        });

        $('.selected-currency').select2({
            theme: 'bootstrap'
        }).on("select2:opening", function (e) {
            e.preventDefault();
        });

        $('.btn-add-currency').click(function () {

            var type_rate = $(this).attr('data-type'); //outcall or incall

            var rate_area = $(this).closest('tr');

            var time = rate_area.find('.time');
            var unit = rate_area.find('.unit option:selected');
            var amount = rate_area.find('.amount');
            var currency = rate_area.find('.selected-currency option:selected');
            if (!isNormalInteger(time.val()) || !isNormalInteger(amount.val())) return false;

            var rate_object = {
                'time': time.val(),
                'unit': unit.val(),
                'price': amount.val(),
                'currency': currency.val()
            };


            if (!$('#times-' + time.val() + '-' + unit.val()).length) {
                $('<li/>', {
                        'id': 'times-' + time.val() + '-' + unit.val(),
                        "class": "list-group-item"
                    })
                    .append(time.val() + ' ' +
                        '<span class="unit_text">' + unit.text() + '</span>' +
                        '<span class="amount_text">' + amount.val() + '</span> ' +
                        '<span class="currency_text">' + currency.text() + '</span> ')
                    .append('<input type="hidden" name="rates[' + type_rate + '][]" value=\'' + JSON.stringify(rate_object) + '\'/>')
                    .append('<i class="fa fa-times remove-price pull-right" aria-hidden="true"></i>')
                    .appendTo('#selected_rates_' + type_rate);
            } else {
                var current_price = $('#selected_rates_' + type_rate + ' #times-' + time.val() + '-' + unit.val());
                current_price.find('.amount_text').html(amount.val());
                current_price.find('.currency_text').html(currency.text());
                current_price.find('input[type="hidden"]').val(JSON.stringify(rate_object));
                alertBackground(current_price);
            }


        });

        $('.add_price_by_types').click(function () {

            var type_rate = $(this).attr('data-type'); //outcall or incall

            var rate_area = $(this).closest('tr');

            var type = rate_area.find('.type').attr('data-type');
            var amount = rate_area.find('.amount');
            var currency = rate_area.find('.selected-currency option:selected');

            var rate_object = {
                'type': type,
                'price': amount.val(),
                'currency': currency.val()
            };

            if (!isNormalInteger(amount.val())) return false;


            if (!$('#selected_rates_' + type_rate + ' #times-' + type).length) {

                $('<li/>', {
                        'id': 'times-' + type,
                        "class": "list-group-item"
                    })
                    .append('<span class="unit_text">' + rate_area.find('.type').text() + '</span> ' +
                        '<span class="amount_text"> ' + amount.val() + '</span> ' +
                        '<span class="currency_text">' + currency.text() + '</span> ')
                    .append('<input type="hidden" name="rates[' + type_rate + '][]" value=\'' + JSON.stringify(rate_object) + '\'/>')
                    .append('<i class="fa fa-times remove-price pull-right" aria-hidden="true"></i>')
                    .appendTo('#selected_rates_' + type_rate);

            } else {
                var current_price = $('#selected_rates_' + type_rate + ' #times-' + type);
                current_price.find('.amount_text').html(amount.val());
                current_price.find('.currency_text').html(currency.text());
                current_price.find('input[type="hidden"]').val(JSON.stringify(rate_object));
                alertBackground(current_price);
            }

        });

        $('body').on('click', '.remove-price', function () {
            $(this).parent().remove();
        });


        function isNormalInteger(str) {
            var n = Math.floor(Number(str));
            return String(n) === str && n >= 0;
        }

        function alertBackground(elem) {
            elem.addClass('bg-warning');
            setTimeout(function () {
                elem.removeClass('bg-warning');
            }, 500);
        }
    },

    initServices: function () {
        if (!$('#services').length) return false;

        var porn_star_link = $('#pornstar_link');

        $('#keywords-4').change(function () {
            if ($(this).is(':checked')) {
                porn_star_link.removeClass('d-none');
                porn_star_link.addClass('d-block');
                porn_star_link.attr('required', 'required');
            } else {
                porn_star_link.removeClass('d-block');
                porn_star_link.addClass('d-none');
                porn_star_link.removeAttr('required');
            }
        });
    },
    uploadVideo: function () {
        var self = this;
        if ($('input[name="escort"]').length) {
            var params = '&escort_id=' + $('input[name="escort"]').val();
        } else {
            var params = '';
        }
        if (!this.formInput.length) return false;
        this.formInput.wrap('<form enctype="multipart/form-data"></form>');
        this.formInput.fileupload({
            dataType: 'text',
            url: "/private/video/video?act=upload" + params,
            maxChunkSize: 1000000, // ~100 KB
            loadVideoMaxFileSize: 50000000,
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.progress .progress-bar').css('width', progress + '%');
            },
            start: function () {
                $('.video-alerts').empty();
                var status_elm = $('.process-status'),
                    progress_block = $('.progress'),
                    progress_wrapper = $('.progress-block');
                progress_wrapper.show();
                progress_block.show();
                status_elm.text('Uploading...');
            },
            chunkdone: function (e, data) {
                var response = jQuery.parseJSON(data.result),
                    status_elm = $('.process-status'),
                    progress_bar = $('.progress').find('.progress-bar');

                self.calculatePercent(data.loaded, data.total, false);

                if (response.error === 0) {
                    self.calculatePercent(data.loaded, data.total);
                    if ((data.maxChunkSize + data.loaded > data.total) && (response.finish === false)) {
                        status_elm.addClass('in-uploading').text('Uploading...');
                        progress_bar.addClass('load');
                        self.calculatePercent(data.loaded, data.total, true);
                    } else if (response.finish === true) {
                        status_elm.removeClass('in-uploading').addClass('in-done').text('Upload Done. Conversion Please wait...');
                        progress_bar.removeClass('load');

                        if (response.error) {
                            $('.video-alerts').empty();
                            $('.video-alerts').html(response.error).removeClass('d-none');
                        } else {
                            if ($('button[name="finish-step"]').length) {
                                $('input[name="a"]').remove();
                                $('button[name="finish-step]"').click();
                            } else {
                                // var gallery_tab = $('#pills-gallery-tab');
                                // gallery_tab.attr('data-ajax-url',gallery_tab.attr('data-ajax-url')+'&'+ Math.round(Math.random() * 100000)).click();

                                //$('button[name="hidden_button"]').click();
                                //$('button[name="hidden_button"]').click();
                                $('.video-uploading').html('<div class="alert alert-success" role="alert">Your video has been uploaded and it\'s now being converted-please check in a few minutes</div>');
                            }
                        }
                        $('.video-block').LoadingOverlay('hide', true);
                    }
                } else {

                    $('.progress-percent').empty();
                    $('.video-alerts').empty().html(response.error).removeClass('d-none');
                    $('.progress').add('.progress-block').css('display', 'none');
                    $('.fileinput-button').removeClass('unactive');
                    status_elm.removeClass('in-uploading').text('');
                    progress_bar.removeClass('load');
                }
            }
        });
    },

    calculatePercent: function (current, size, done) {
        var perc_block = $('.progress-percent');
        if (!done) {
            var _perc = Math.ceil(((current * 100) / size) * 100) / 100;
            perc_block.html(_perc + '%');
        } else {
            perc_block.html('');
        }
    }

};




$(document).ready(function () {
    if( ! window.STEPS_INITIALIZED)
        window.STEPS_INITIALIZED = Steps.init();

}); // document READY
