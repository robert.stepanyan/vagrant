/**
 * Created by SceonDev on 20.10.2017.
 * Updated by Eduard on 2019 (Spring)
 */
var Support = {

    attachmentInProgress: false,
    commentAttachedFilesCount: 0,

    init: function () {
        // this.initSummernote();
        // this.loadTicket();
        // this.initForm();
    },

    load: function (event, url, params) {

        if(params == undefined) {
            params = {};
        }

        var e = event || window.event;
        if(e) e.preventDefault();

        if (!lang_id) lang_id = '';

        var url = lang_id + url;
        var container = $('#ui-view');

        // $$('.swiff-uploader-box').destroy();
        if (!params['secretly']) {
            container.LoadingOverlay("show", {
                color: "rgba(239, 243, 249, 0.80)",
                zIndex: 1000
            });
        }

        var data = {
            'ot_page': params['ot_page'],
            'ct_page': params['ct_page'],
        };

        $.ajax({
            url: url,
            type: 'get',
            data: data,
            success: function (resp) {
                container.html(resp);
                container.LoadingOverlay("hide", true);
                $('body').removeClass('chat-on');
                $('.user-area-escort > .wait-for-load').removeClass('wait-for-load');

                // Activate sidebar item
                // --------------------------------------
                if (typeof APPLICATION != "undefined") {
                    APPLICATION.set_nav_active_item();
                    APPLICATION.set_greetings_panel();
                }
                // --------------------------------------

                // Support.initAttach(container.getElements('.ticket-add')[0]);
                // Support.init(container);

                // container.getElements('a.subject').addEvent('click', function(e){
                //     e.stop();

                //     Support.SupportTicket(this.get('rel'));
                // });
            }
        });

        return false;
    },

    initSummernote: function (area) {

        var elem = area || $('#ui-view');
        elem.find('textarea').summernote({
            height: 100,
            disableDragAndDrop: true,
            disableResizeEditor: true,
            shortcuts: false,
            toolbar: [
                ['misc', ['undo', 'redo']],
                ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
            ],
            popover: {}
        });
    },

    loadTicket: function (e, ticket_id) {
        var _self = this;
        var $contiainer = $("#ui-view");

        e.preventDefault();

        $.ajax({
            url: '/support/ticket',
            type: 'GET',
            data: {id: ticket_id},
            beforeSend: function () {
                $contiainer.LoadingOverlay('show', true);
            },
            success: function (resp) {
                //$('body').addClass('chat-on');
                $('#all-tickets').hide();
                $('#preview-ticket').html(resp).removeClass('d-none');

                $contiainer.LoadingOverlay('hide', true);
                var $list_wrapper = $('#comments_wrapper');

                $list_wrapper[0].scrollTop = $list_wrapper[0].scrollHeight
            }
        });
    },

    close_ticket: function () {
        event.preventDefault();
        $('body').removeClass('chat-on');
        $('#all-tickets').show();
        $('#preview-ticket').addClass('d-none');
    },

    update_comments_list: function () {
        var id = $('#support-comment-form input[name="id"]').val();
        var $container = $('#comments_wrapper .sub-comments');

        $.ajax({
            url: '/support/ajax-comments-list?id=' + id,
            type: "POST",
            success: function(data) {
                $container.html(data);
                $container[0].scrollTop = $container[0].scrollHeight
            }
        });
    },

    set_visible_panel : function(active_panel) {
        if(active_panel == 'dialog') {
            $('.chat .dialog').addClass('show');
            $('.chat .conversations').removeClass('show');
        }else if(active_panel == 'conversations'){
            $('.chat .dialog').removeClass('show');
            $('.chat .conversations').addClass('show');
        }
    },

    send_ticket_comment: function (e) {
        e.preventDefault();

        var $form = $('form#support-comment-form');
        var $container = $('#ui-view');
        var data = $form.serializeArray();
        var $message = $('#ticket-comment-text');

        if (!$message.val()) {
            $message.focus();
            return false;
        } else {
            $message.val('');
        }

        $.ajax({
            url: '/support/ajax-add-comment',
            type: 'POST',
            data: data,
            beforeSend: function () {
                $container.LoadingOverlay('show');
            },
            success: function (resp) {
                try {
                    resp = JSON.parse(resp);
                    if (resp.status == 'error') {
                        var text = '<ul>';
                        for (field in data.msgs) {
                            text += "<li>" + data.msgs[field] + "</li>"
                        }
                        text += '</ul>';
                        Notify.alert(text)
                    } else if (resp.status == 'redirect') {
                        Support.close_ticket();
                    }else{
                        Support.update_comments_list();
                        $('.support-ticket-attached-files').empty();
                        $('#support-comment-files-meta').empty();
                        Support.commentAttachedFilesCount = 0;
                    }

                } catch (e) {
                    console.log(e)
                }

                Support.update_attached_files_count();
            }
        }).done(function () {
            $container.LoadingOverlay('hide');
        })
    },

    initBackButton: function () {
        $('.back_to_tickets_list').click(function () {
            if (CheckDevice.isMobile()) {
                $('#support-first-section').addClass('mobile-hide');
                $('#support-second-section').removeClass('mobile-hide');
            }
        });
    },

    initNewTicket: function () {
        var _self = this;
        $('.new_ticket').click(function () {
            $('#preview_ticket').html(' ');
            $('#add_ticket_form').fadeIn();
            if (CheckDevice.isMobile()) {
                $('#support-first-section').removeClass('mobile-hide');
                $('#support-second-section').addClass('mobile-hide');
            }
        });

        $('#attach_file').click(function () {
            $(this).closest('form').find('.attachment').click();
        });
    },

    update_attached_files_count: function() {
        $('.attached-files-count').html(Support.commentAttachedFilesCount + (Support.commentAttachedFilesCount > 9 ? '+' : ''));

        if (Support.commentAttachedFilesCount <= 0) $('.attached-files-count').hide();
        else $('.attached-files-count').show();
    },

    add_comment_attachment: function () {

        var $file_input = $('#support-comment-attachment'),
            $meta_container = $('#support-comment-files-meta'),
            $btn = $('#support-comment-attach-files'),
            $attached_files = $('.support-ticket-attached-files');

        var url = "/support/add-attached-files";
        $btn.LoadingOverlay('show');

        $file_input.simpleUpload(url, {
            name: 'Filedata',
            allowedExts: ["jpg", "jpeg", "png"],
            allowedTypes: ["image/jpeg", "image/png", "image/x-png", "image/jpg"],
            limit: 50,
            maxFileSize: 5000000, //50MB in bytes
            success: function (response) {
                try {
                    response = JSON.parse(response);

                    $btn.LoadingOverlay('hide');

                    if (response.status == 1) {
                        // var template = '<div class="w-100 p-3 d-flex justify-content-center align-items-center">' +
                        //     '<img class="img-thumbnail" src="' + response.attachment_url + '" alt="">' +
                        //     '</div>';
                        // $attached_files.prepend(template);

                        Support.commentAttachedFilesCount++;
                        $meta_container.append("<input type='hidden' name='attach[]' value='" + response.id + "'>");

                    } else {
                        Notify.alert('danger', response.error);
                    }

                } catch (e) {
                    console.warn(e);
                    Notify.alert("Some error occurred, please try again later.")
                }

                Support.update_attached_files_count();

            },

            error: function (error) {
                Notify.alert('danger', error.message);
                $btn.LoadingOverlay('hide');
            }
        });


        // if(form.attr('id') === 'add_ticket_form'){
        //     $('body').on('click','.close-icon', function () {

        //         var attachmentTicket = $('#attachment_ticket');
        //         var images = attachmentTicket.val();
        //         var imagesArray = images.split(',');
        //         var imageIndex = imagesArray.indexOf($(this).attr('data-id'));

        //         imagesArray.splice(imageIndex,1);
        //         images = imagesArray.join(',');
        //         attachmentTicket.val(images);

        //         $(this).parent('div').remove();

        //     });
        // }
    },

    add_ticket_attachment: function (params) {

        var selected_files_count = this.files.length;
        var already_attached_files_count = $('[name="attach[]"]').length;
        var total_count = selected_files_count + already_attached_files_count;
        var max_allowed_files_count = 5;

        if(total_count > max_allowed_files_count ) {
            return Notify.alert('danger',  headerVars.dictionary.maximum_alloed_file_count + ': ' + max_allowed_files_count);
        }

        var $file_input = $('#support-attachment'),
            $error_bar = $('.attachment_error'),
            $meta_container = $('#attach-files-meta'),
            $progress_bar = $('.upload-status .progress-bar'),
            $attached_files = $('#attached-files');

        var __self = $(this);
        var url = "/support/add-attached-files";

        $error_bar.html('');
        $progress_bar.parent().show();

        $file_input.simpleUpload(url, {

            name: 'Filedata',
            allowedExts: ["jpg", "jpeg", "png"],
            allowedTypes: ["image/jpeg", "image/png", "image/x-png", "image/jpg"],
            limit: max_allowed_files_count,
            maxFileSize: 2000000, //5MB in bytes

            start: function (_file) {
                $progress_bar.css('width', '0%');
            },

            progress: function (progress) {
                $progress_bar.css('width', progress + '%')
            },

            success: function (response) {
                response = JSON.parse(response);

                $progress_bar.parent().hide()
                if (response.status == 1) {

                    $meta_container.append("<input type='hidden' name='attach[]' value='" + response.id + "'>")


                    if (response.orig_name) {
                        var file_name = response.orig_name.length > 30 ? response.orig_name.substring(0, 30) + '...' : response.orig_name;
                        $attached_files.append("<li class='mb-3'><a target='blank' href='" + response.attachment_url + "' >" + file_name + "</a> <i class='fa fa-trash-alt close' onClick='Support.event_handlers.remove_attachment(" + response.id + ")'></i> </li>");
                    }

                } else {
                    $error_bar.html("<p class='text-danger'>" + response.error + "</p>");
                }

            },

            error: function (error) {

                var html = '<p class=\'text-danger\'>' + error.message + '</p>';
                $error_bar.hide().fadeIn(300).html(html);
            }
        });


        // if(form.attr('id') === 'add_ticket_form'){
        //     $('body').on('click','.close-icon', function () {

        //         var attachmentTicket = $('#attachment_ticket');
        //         var images = attachmentTicket.val();
        //         var imagesArray = images.split(',');
        //         var imageIndex = imagesArray.indexOf($(this).attr('data-id'));

        //         imagesArray.splice(imageIndex,1);
        //         images = imagesArray.join(',');
        //         attachmentTicket.val(images);

        //         $(this).parent('div').remove();

        //     });
        // }
    },

    event_handlers: {

        attach_file: function (input) {
            $(input).trigger('click');
        },
        remove_attachment: function (id) {
            var $elm = $(event.target),
                $parent = $elm.parents('li');

            $('#attach-files-meta input[value=' + id + ']').remove();
            $parent.remove();
        },

        form_submit: function () {

            event.preventDefault();

            var $error_bar = $('.attachment_error'),
                $form = $('#add-ticket-form'),
                data = $form.serializeArray(),
                $container = $('#ui-view');

            $.ajax({
                url: '/support/ajax-add-ticket',
                type: 'POST',
                data: data,

                beforeSend: function () {
                    $container.LoadingOverlay("show", true);
                },

                success: function (resp) {

                    $container.LoadingOverlay("hide", true);
                    try {
                        resp = JSON.parse(resp);
                        var errors_template = "";

                        if (resp.status == "success") {
                            Support.load( null,'/support/ajax-tickets-list?ajax=1/', {secretly: true});
                            $error_bar.html("");
                            Notify.alert("success", headerVars.dictionary.ticket_send_success)

                        } else {

                            $.each(resp['msgs'], function (key, value) {
                                errors_template += "<li>" + value + "</li>"
                            });

                            $error_bar.html("<div class='alert alert-danger'><ol class='m-0'>" + errors_template + "</ol></div>")
                        }

                    } catch (e) {
                        $container.html(resp);
                    }
                }
            });
        }
    }

};
