var Verification = {

    escort_id: $('input[name=escort_id]').val(),
    takenScreenshots: [],
    recorderVideo: null,
    isProcessingVideo: false,
    errors: {
      cameraDisabled: "We can't find your camera, that most probably means that it's either not connected broken or you don't have the proper webcam drivers. Requested device not found.",
        processingFailed: "Whoops, there was an error while trying to use your Camera / Microphone."
    },
    currentStep: 1,
    maxStep: 3,

    interval: null,
    isSearching: false,

    init: function () {
        // this.initUploader();
        this.initEscortsProfiles();
    },

    changeStep: function(to) {

        var $nextBtn = $('.stepper-meta button.next');
        var $prevBtn = $('.stepper-meta button.prev');
        var $finalBtn = $('.stepper-meta button.final-step-btn');

        var hasScreenshots = $('.screenshot-meta').length > 0;
        var hasVideo = $('.video-meta').length > 0;


        if (!to) {
            to = 'next';
        }

        if (to == 'next' && Verification.currentStep < Verification.maxStep) {

            if(Verification.currentStep == 1 && !hasVideo) {
                return Notify.alert('danger', headerVars.dictionary.id_card_video_required);
            } else if (Verification.currentStep == 2 && !hasScreenshots) {
                return Notify.alert('danger', headerVars.dictionary.id_card_min_2_screenshots);
            }

            Verification.currentStep++;
        }

        if (to == 'prev' && Verification.currentStep > 1) {
            Verification.currentStep--;
        }

        if (Verification.currentStep >= Verification.maxStep) {
            $nextBtn.hide().prop('disabled', true);
            $finalBtn.show();

        } else if (Verification.currentStep <= 1) {
            $nextBtn.show().prop('disabled', false);
            $prevBtn.prop('disabled', true);

            $finalBtn.hide();
        } else {
            $nextBtn.show().prop('disabled', false);
            $prevBtn.prop('disabled', false);

            $finalBtn.hide();
        }

        $('.stepper .step.active').removeClass('active');
        $('.stepper .step--' + Verification.currentStep).addClass('active');
        $('.current-step-number').text(Verification.currentStep);
        Verification.setStepsDoneByPercent(100 / Verification.maxStep * Verification.currentStep);
    },

    setStepsDoneByPercent: function(percent) {
        $(".progress").each(function () {

            var value = percent || 0;
            var left = $(this).find('.progress-left .progress-bar');
            var right = $(this).find('.progress-right .progress-bar');

            if (value > 0) {
                if (value <= 50) {
                    left.css('transform', 'rotate(0deg)')
                    right.css('transform', 'rotate(' + (value) / 100 * 360 + 'deg)')
                } else {
                    right.css('transform', 'rotate(180deg)')
                    left.css('transform', 'rotate(' + (value - 50) / 100 * 360 + 'deg)')
                }
            }

        })
    },

    renderTakenScreenshots: function (silent = false) {

        var html = ``;
        var removeButton = `
            <button type="button" class="remove">
                <i class="fa fa-times"></i>
                Remove
            </button>
        `;

        var loaderIcon = `
            <img width="30" src="/images/loading_spinner.gif" /> 
        `;
        Verification.takenScreenshots.forEach(function (item, index) {
            html += `
                    <div class="screenshots-list__item col-md-6 col-12" data-id="${item.id}">
                        <img src="${item.base64}" alt="">
                        
                        <input type="hidden" class="screenshot-meta" name="hash[]"  value="${item.hash}">
                        <input type="hidden" class="screenshot-meta" name="ext[${item.hash}][]" value="${item.ext}">
                        
                        <div class="d-flex ml-4 align-items-center flex-column">
                            <p class="text-grey">Screenshot ${index + 1}</p>
        
                            ${item.done ? removeButton : loaderIcon}
                        </div>
                    </div>`;
        });

        $('.screenshots-list__wrapper').html(html);

        var needScreensCount = 2 - Verification.takenScreenshots.length;
        if (needScreensCount && !silent) {
            Notify.alert('warning', needScreensCount + ' more screenshot(s) left!');
        }
    },

    startVideoRecording: function () {

        Verification.setStepsDoneByPercent(33);

        if (Sceon.isIOSChrome() && Sceon.isIOS()) {
            $('.browser-support-warning').show();
            $('#recorded-video').addClass('isiOSSafari');
        }

        try {

            $('#verify-by-video').hide();
            $('.video-render__wrapper').fadeIn();

            var $timer = $('.verify-option--video .timer-indicator');
            var $resetBtn = $('.verify-option--video .reset');
            var $playBtn = $('.verify-option--video .play-again');
            var $loader = $('.verify-option--video .loading');

            var recordedSeconds = 0;
            var timerInterval = null;
            var options = {
                controls: true,
                //width: 900/4,
                //height: 300,
                aspectRatio: "4:3",
                inactivityTimeout: 0,
                fluid: true,
                controlBar: {
                    volumePanel: false
                },
                plugins: {
                    record: {
                        audio: true,
                        video: true,
                        maxLength: 8,
                        debug: true
                    }
                }
            };

            // apply some workarounds for opera browser
            applyVideoWorkaround();

            var player = window.videoRecorder = videojs('video-recorder', options, function () {
                // print version information at startup
                var msg = 'Using video.js ' + videojs.VERSION +
                    ' with videojs-record ' + videojs.getPluginVersion('record') +
                    ' and recordrtc ' + RecordRTC.version;
            });

            // error handling
            player.on('deviceError', function () {
                Notify.alert('danger', Verification.errors.cameraDisabled)
            });

            player.on('error', function (element, error) {
                Notify.alert('danger', Verification.errors.processingFailed)
            });

            $resetBtn.on('click', function () {
                $timer.hide();
                $resetBtn.hide();
                $playBtn.hide();

                player.record().reset();

                $('#recorded-video').attr('src', '');
                $('.recorded-video__wrapper').hide();
                $('.video-render__wrapper').show();
            });

            $playBtn.on('click', function () {
                $('#recorded-video').get(0).play()
            });

            // user clicked the record button and started recording
            player.on('startRecord', function () {
                recordedSeconds = 8;
                $('.video-meta').remove();
                $loader.hide();

                $timer.text(recordedSeconds + ' sec').show();
                $resetBtn.hide();
                $playBtn.hide();

                timerInterval = setInterval(function () {
                    $timer.text((recordedSeconds--) + ' sec');

                    if (recordedSeconds < 0) {
                        clearInterval(timerInterval);
                    }
                }, 1000);
            });


            player.on('stopRecord', function (element, error) {
                console.error(error);

                clearInterval(timerInterval);
                $timer.hide();
            });


            // user completed recording and stream is available
            player.on('finishRecord', function () {
                // the blob object contains the recorded data that
                // can be downloaded by the user, stored on server etc.
                $loader.show();
                $('.video-render__wrapper').hide();

                Verification.sendVideoToServer(player.recordedData)
                    .done(function () {
                        $timer.hide();
                        $resetBtn.show();
                        $playBtn.show();
                        $loader.hide();
                    });
            });

            player.on('deviceReady', function () {
                // the blob object contains the recorded data that
                // can be downloaded by the user, stored on server etc.
                player.record().start();
            });

        } catch (e) {
            alert("Whoops! something went wrong " + e.message);
        }
    },

    startScreenshotTaking: function () {
        try {
            var options = {
                controls: true,
                controlBar: {
                    fullscreenToggle: false,
                    pictureInPictureToggle: false,
                    exitPictureInPicture: true,
                    volumePanel: false,
                },
                width: 900/4,
                height: 300,
                //aspectRatio: "4:3",
                inactivityTimeout: 0,
                fluid: true,
                plugins: {
                    record: {
                        image: true,
                        debug: true,
                        imageOutputType: 'dataURL',
                        imageOutputFormat: 'image/png',
                        imageOutputQuality: 0.92
                    }
                }
            };

            var player = window.screenshotRecorder = videojs('screenshot-recorder', options, function () {
                // print version information at startup
                var msg = 'Using video.js ' + videojs.VERSION +
                    ' with videojs-record ' + videojs.getPluginVersion('record');
                videojs.log(msg);
            });

            // error handling
            player.on('deviceError', function () {
                Notify.alert('danger', Verification.errors.cameraDisabled)
            });

            player.on('error', function (element, error) {
                Notify.alert('danger', Verification.errors.processingFailed)
            });

            // snapshot is available
            player.on('finishRecord', function () {

                if (Verification.takenScreenshots.length >= 2) {
                    return Notify.alert('warning', "Maximum 2 screenshots!");
                }

                // the blob object contains the image data that
                // can be downloaded by the user, stored on server etc.
                var image = {
                    id: Date.now(),
                    base64: player.recordedData
                };

                console.log(image);

                player.record().reset()

                Verification.takenScreenshots.push(image);
                Verification.sendScreenshotToServer(image);

                Verification.renderTakenScreenshots();
            });

            player.on('retry', function () {
                console.log('retry');
            });

            $(document).on('click', '.screenshots-list__wrapper .remove', function () {
                var id = $(this).parents('.screenshots-list__item').data('id');

                $(this).parents('.screenshots-list__item').fadeOut("fast", function () {
                    Verification.takenScreenshots = Verification.takenScreenshots.filter(function (item) {
                        return item.id != id;
                    });

                    Verification.renderTakenScreenshots();
                });
            });

        } catch (e) {
            alert("Whoops! something went wrong " + e.message);
        }
    },

    makeBlob: function (dataURL) {
        var BASE64_MARKER = ';base64,';
        if (dataURL.indexOf(BASE64_MARKER) == -1) {
            var parts = dataURL.split(',');
            var contentType = parts[0].split(':')[1];
            var raw = decodeURIComponent(parts[1]);
            return new Blob([raw], {type: contentType});
        }
        var parts = dataURL.split(BASE64_MARKER);
        var contentType = parts[0].split(':')[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;

        var uInt8Array = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], {type: contentType});
    },

    sendScreenshotToServer: function (image) {
        var e_id = $('input[name="escort_id"]').val();
        var url = '/verify/upload-photos?escortid=' + e_id;
        var fileId = (new Date()).getTime().toString(36);

        var blob = Verification.makeBlob(image.base64);
        $.ajax({
            url: url,
            type: 'POST',
            processData: false,
            contentType: 'application/octet-stream',
            data: blob,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-File-Id', fileId);
                xhr.setRequestHeader('X-File-Name', fileId + '.jpg');
                xhr.setRequestHeader('X-File-Size', blob.size);
            }
        })
            .done(function (response) {

                if (response.status == "1") {
                    var imageUrl = response.photo_url ? response.photo_url : "/images/default-image.jpeg";

                    Verification.takenScreenshots.map(function (item) {
                        if (item.id === image.id) {
                            item.done = true;
                            item.ext = response.ext;
                            item.hash = response.hash;
                        }
                        return item;
                    });

                    Verification.renderTakenScreenshots(true);

                } else {
                    Notify.alert("danger", "Error cannot upload the file");
                    console.log(response);

                    Verification.takenScreenshots = Verification.takenScreenshots.filter(function (item) {
                        return item.id != image.id;
                    });

                    Verification.renderTakenScreenshots(true);
                }

            })
            .fail(function () {
                Notify.alert("danger", "Error cannot upload the file");

                Verification.takenScreenshots = Verification.takenScreenshots.filter(function (item) {
                    return item.id != image.id;
                });

                Verification.renderTakenScreenshots(true);
            });
    },

    sendGalleryProofToServer: function (base64) {
        var e_id = $('input[name="escort_id"]').val();
        var url = '/verify/upload-photos?escortid=' + e_id;
        var fileId = (new Date()).getTime().toString(36);

        var blob = Verification.makeBlob(base64);
        return $.ajax({
            url: url,
            type: 'POST',
            processData: false,
            contentType: 'application/octet-stream',
            data: blob,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-File-Id', fileId);
                xhr.setRequestHeader('X-File-Name', fileId + '.jpg');
                xhr.setRequestHeader('X-File-Size', blob.size);
            }
        });
    },

    sendVideoToServer: function (blob) {
        var e_id = $('input[name="escort_id"]').val();
        // var rotate = Sceon.isIOS() ? '1' : '';
        var url = '/verify/upload-video?escortid=' + e_id /*+ '&rotate=' + rotate*/;
        var fileId = (new Date()).getTime().toString(36);

        return $.ajax({
            url: url,
            type: 'POST',
            processData: false,
            contentType: 'application/octet-stream',
            data: blob,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-File-Id', fileId);
                xhr.setRequestHeader('X-File-Name', fileId + '.mp4');
                xhr.setRequestHeader('X-File-Size', blob.size);
            }
        })
            .done(function (response) {

                if (response.status == "1") {
                    var template = $("" +
                        "<input type=\"hidden\" class='video-meta' name=\"hash[]\" value=\"" + response.hash + "\" />" +
                        "<input type=\"hidden\" class='video-meta' name=\"ext[" + response.hash + "][]\" value=\"" + response.ext + "\" />" +
                        "<input type='hidden' class='video-meta' name='has_video' value='1'>" +
                        "")

                    Verification.recorderVideo = {
                        path: 'https://video.escortdirectory.com' + response.path
                    };

                    $('.video-meta').remove();
                    $('.video-render__wrapper').append(template);

                    $('#recorded-video').attr('src', Verification.recorderVideo.path);
                    $('.recorded-video__wrapper').show();

                    Verification.isProcessingVideo = false;

                } else {
                    Notify.alert("danger", "Error cannot upload the file");
                    Verification.isProcessingVideo = false;
                }

            })
            .fail(function () {
                Notify.alert("danger", "Error cannot upload the file");
                Verification.isProcessingVideo = false;
            });
    },

    startGalleryProoveUploads: function() {

        $('.gallery-proof__block.block-add').on('click', function() {
            var html = `<div class="gallery-proof__block bordered">
                            <div class="btn-activator">
                                <i class="fas fa-upload"></i>
                            </div>
                            <input type="file" accept="image/*">
                        </div>`;

            if($('.gallery-proof__block.bordered').length >= 11) {
                return Notify.alert('warning', 'Maximum limit reached.')
            }

            $(this).after(html);
        });

        $('.galery-proof-list__wrapper').on('click', '.gallery-proof__block.bordered > .btn-activator', function () {
            var $inp = $(this).parent().find('input');

            if($inp) $inp.trigger('click');
        });

        $('.galery-proof-list__wrapper').on('change', 'input[type="file"]', function () {
            var input = this;

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var base64 = e.target.result;
                    var html = $(`
                        <div class="image-preview grayscale" style="background-image: url(${base64})">
                             <img width="50" src="/images/loading_spinner.gif" />
                        </div>
                    `);

                    $(input).before(html);

                    Verification.sendGalleryProofToServer(base64)
                        .done(function (response) {

                            if (response.status == "1") {
                                html.find('img').remove();
                                html.removeClass('grayscale');
                                html.append(`
                                    <i class="fa fa-trash"></i>
                                    <input type="hidden" name="hash[]"  value="${response.hash}">
                                    <input type="hidden" name="ext[${response.hash}][]" value="${response.ext}">
                                `);

                            } else {
                                Notify.alert("danger", "Error cannot upload the file");
                                html.remove();
                            }

                            html.find('i').on('click', function(){
                                if(confirm('Are you sure you want to delete ?')) {
                                    $(this).parents('.gallery-proof__block').remove();
                                }
                            })

                        })
                        .fail(function () {
                            Notify.alert("danger", "Error cannot upload the file");
                            html.remove();
                        });
                };

                reader.readAsDataURL(input.files[0]);
            }
        });
    },

    on_search_try: function () {

        if (Verification.isSearching) return console.warn("Already searching ...");
        if (Verification.interval) {
            clearInterval(Verification.interval);
        }

        Verification.interval = setTimeout(function () {
            Verification.verify_escorts_action(1);
        }, 500);

    },

    form_submit_handler: function () {
        event.preventDefault();
        var self = this,
            $form = $(event.target),
            $container = $("#ui-view");

        $container.LoadingOverlay("show", {
            color: "rgba(239, 243, 249, 0.80)",
            zIndex: 1000
        });

        $.ajax({
            url: $form.attr('action'),
            type: "POST",
            data: $form.serializeArray(),
            dataType: "JSON",
            success: function (response) {
                var did_animation = false;

                $('.is-invalid').removeClass('is-invalid');
                $('[id^=validation-]').html('');
                $('#errors-container').hide();

                if (response.status == 'success')
                    return Verification.load('/verify/success');
                else {
                    $('#errors-container').show();
                    $('.visible-on-error').hide();
                }

                $.each(response['msgs'], function (key, value) {

                    $('#validation-' + key)
                        .html(value)
                        .parents('.visible-on-error')
                        .show();

                    if (!did_animation && $('#validation-' + key).length) {
                        $('html, body').animate({
                            scrollTop: $('#validation-' + key).offset().top - 100
                        }, 1000);

                        did_animation = true;
                    }
                })

                $container.LoadingOverlay("hide", true);
            }
        })
    },

    addUploader: function () {
        var uploaders_count = $('#uploaders input[type=file]').length;

        if (uploaders_count > 9) return false;

        $('<div/>').append('<label class="custom-file my-1 w-75">Image no. ' + (uploaders_count + 1) + ' <input id="uploader-' + (uploaders_count + 1) + '" type="file" name="pic[' + uploaders_count + ']" class="custom-file-input"><span class="custom-file-control  form-control-file"></span> </label>').appendTo($('#uploaders'));
    },

    attach_file: function () {
        var $btn = $(event.target);
        var $file_input = $btn.siblings('input[type=file]');
        var $parent_row = $btn.parents('.picture-container');

        if ($file_input.attr("multiple")) {
            if ($parent_row.hasClass('havePicture')) {
                $parent_row.removeClass('havePicture').find('.additional-block').empty();
            }
        } else {

            if ($parent_row.hasClass('haveVideo')) {
                $parent_row
                    .removeClass('haveVideo')
                    .find('div.image')
                    .css('background-image', 'url()')
                    .end()
                    .find('.additional-block')
                    .remove();
            }
        }


        if ($file_input.length)
            $file_input.trigger('click');
        else {
            console.warn("Whoops!, File input was not found");
        }
    },

    init_video_upload: function () {
        var e_id = $('input[name="escort_id"]').val();
        var url = '/verify/upload-video?escortid=' + e_id,
            $file_input = $(event.target),
            block = $('<div class="additional-block mt-1"></div>'),
            success_message = (headerVars.dictionary.video_upload_success),
            progress_bar = $('<div class="progress-bar progress-bar-striped progress-bar-animated" style="width:0%"></div>'),
            id = $file_input.attr('id'),
            container = $file_input.parents('.video-container');

        block.append(progress_bar);
        container
            .find('.additional-block')
            .remove()
            .end()
            .append(block);

        var formdata = new FormData(document.getElementById('verify-form')),
            $file = formdata.get('video'),
            error_bar = $('#uploads .errors'),
            $fileId = (new Date()).getTime().toString(36);

        var upload_chunk = function ($file, $fileId, $start) {

            // Force Stop Recursion
            // if (self.globals.is_uploading[file_type] == 'aborted') {
            //     return self.globals.is_uploading[file_type] = null;
            // }

            var chunk,
                $chunkSize = 524288, // 0.5 MB
                total = $start + $chunkSize;

            if (total >= $file.size) {
                total = $file.size
            }

            if ($file.mozSlice) {
                // Mozilla based
                chunk = $file.mozSlice($start, total);
            } else if ($file.webkitSlice) {
                // Chrome, Safari, Konqueror and webkit based
                chunk = $file.webkitSlice($start, total);
            } else {
                // Opera and other standards browsers
                chunk = $file.slice($start, total);
            }
            $('#btn-send').prop("disabled", true);
            /*self.globals.is_uploading[file_type] =*/
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-File-Id', $fileId);
                    xhr.setRequestHeader('X-File-Name', $file.name);
                    xhr.setRequestHeader('X-File-Size', $file.size);
                    xhr.setRequestHeader('X-File-Resume', 1);
                },
                contentType: "application/json",
                data: chunk,
                processData: false,
                success: function (response) {

                    // if(typeof response == 'string') {
                    //     response = {
                    //         'status' : 0,
                    //         'error' : response,
                    //     };
                    // }
                    console.log(response);
                    console.log(typeof response);
                    var $newStartpos = 100 * total / $file.size;
                    progress_bar.css('width', $newStartpos + '%');

                    if (!response.finish && !response.status == 1) {

                        if (!response.error) {

                            upload_chunk($file, $fileId, total)
                        } else {

                            //self.globals.is_uploading[file_type] = null;

                            error_bar.html(response.error)
                                .parent()
                                .removeClass('text-success')
                                .addClass('text-danger');
                            container.addClass('stop').removeClass('uploading');

                        }
                    } else {

                        //self.globals.is_uploading[file_type] = null;

                        if (!response.error) {

                            //self.globals.is_uploading['profile_pic'] = false;
                            container.removeClass('uploading').addClass('stop');
                            progress_bar.css("width", '0%');
                            $('#btn-send').prop("disabled", false);
                            block
                                .append(success_message)
                                .addClass('text-success');
                            error_bar
                                .removeClass('text-danger')
                                .addClass('text-success')
                                // .html(headerVars.dictionary.video_upload_success)
                                .parent()
                                .removeClass('text-danger')
                                .addClass('text-success');

                            var template = $('' +
                                '<input type="hidden" name="hash[]" value="' + response.hash + '" />' +
                                '                            <input type="hidden" name="ext[' + response.hash + '][]" value="' + response.ext + '" />' +
                                '').appendTo(block);

                            container.addClass('haveVideo').find('.additional-block').append(template);

                            $('.gallery-page .dropdown-item.attach-video').addClass('disabled')

                        } else {

                            block.remove();
                            var text = response.error;

                            $('#uploads .errors').html(text);
                            $('html, body').animate({
                                scrollTop: 0
                            }, 1000);

                            container.addClass('stop').removeClass('uploading');
                        }
                    }
                }
            });
        }

        upload_chunk($file, $fileId, 0);
    },

    init_multiple_file_upload: function () {
        var e_id = $('input[name="escort_id"]').val();

        var url = '/verify/upload-photos?escortid=' + e_id,
            $file_input = $(event.target),
            container = $file_input.parents('.picture-container');
        block = container.find('.additional-block'),
            progress_bar_wrapper = block.find('.progress-bar-wrapper'),
            id = $file_input.attr('id');

        var file_count = $file_input.get(0).files.length + container.find('.additional-block .picture-info').length;

        if (file_count > 2) {
            return Notify.alert("warning", headerVars.dictionary.maximum_alloed_file_count + ': 2')
        }

        var progress_bar = $('<div class="progress-bar progress-bar-striped progress-bar-animated" style="width:0%"></div>');
        progress_bar_wrapper.empty().append(progress_bar);

        $file_input.simpleUpload(url, {
            allowedExts: ["jpg", "jpeg", "png"],
            allowedTypes: ["image/jpeg", "image/png", "image/x-png", "image/jpg"],
            limit: 2,
            expect: "JSON",
            start: function (file) {

            },
            progress: function (progress) {
                progress_bar.width(progress + "%");
            },
            success: function (response) {
                if (response.status == "1") {
                    var image_url = response.photo_url ? response.photo_url : "/images/default-image.jpeg";

                    var template = $("" +
                        "<div class=\" d-inline-block mt-4 \">" +
                        "<div class=\"picture-info d-flex mr-2 mb-2\">" +
                        "<div class=\"image havePicture\"></div>" +
                        "</div>" +
                        //"<div class=\"mt-4 autocomplete-container flex-grow-1 unset-height\" autocomplete=\"off\">" +
                        //"<textarea name=\"comment[" + response.hash + "][]\" placeholder=\"Comment ...\" rows=\"3\"></textarea>" +
                        //"</div>" +
                        "<input type=\"hidden\" name=\"hash[]\" value=\"" + response.hash + "\" />" +
                        "<input type=\"hidden\" name=\"ext[" + response.hash + "][]\" value=\"" + response.ext + "\" />" +
                        "</div> " +
                        "").appendTo(block).find('.image').css('background-image', "url(" + image_url + ")").end();

                    container.find('.additional-block').append(template);

                    if (file_count >= 2) {
                        container.addClass('havePicture')
                    }
                } else {
                    var text = response.error;
                    $('#uploads .errors').html('<p>' + text + '</p>');
                    $('html, body').animate({scrollTop: 0}, 1000);
                }
                setTimeout(function () {
                    progress_bar.slideUp();
                }, 1500);
            },
            error: function (error) {
                //block.remove();
                var text = error.message;

                $('#uploads .errors').html(text);
                $('html, body').animate({
                    scrollTop: 0
                }, 1000);

            }
        })
    },

    init_single_file_upload: function () {
        var e_id = $('input[name="escort_id"]').val();
        var url = '/verify/upload-photos?escortid=' + e_id,
            $file_input = $(event.target),
            block = $('<div class="additional-block"></div>'),
            progress_bar = $('<div class="progress-bar progress-bar-striped progress-bar-animated" style="width:0%"></div>'),
            id = $file_input.attr('id'),
            container = $file_input.parents('.picture-container');


        if (container.hasClass('havePicture')) {
            container
                .removeClass('havePicture')
                .find('div.image')
                .css('background-image', 'url()')
                .end()
                .find('.additional-block')
                .remove();
        }

        block.append(progress_bar)
        container
            .find('.additional-block')
            .remove()
            .end()
            .append(block)


        $file_input.simpleUpload(url, {
            allowedExts: ["jpg", "jpeg", "png"],
            allowedTypes: ["image/jpeg", "image/png", "image/x-png", "image/jpg"],
            limit: 1,
            expect: "JSON",
            start: function (file) {
            },
            progress: function (progress) {
                progress_bar.width(progress + "%");
            },
            success: function (response) {
                if (response.status == "1") {
                    var image_url = response.photo_url ? response.photo_url : "/images/default-image.jpeg";


                    var template = $("" +
                        //"<div class=\"autocomplete-container flex-grow-1 unset-height\" autocomplete=\"off\">" +
                        //"<textarea name=\"comment[" + response.hash + "][]\" placeholder=\"Comment ...\" rows=\"3\"></textarea>" +
                        //"</div>" +
                        "<input type=\"hidden\" name=\"hash[]\" value=\"" + response.hash + "\" />" +
                        "<input type=\"hidden\" name=\"ext[" + response.hash + "][]\" value=\"" + response.ext + "\" />" +
                        "").appendTo(block);

                    container
                        .addClass('havePicture')
                        .find('div.image')
                        .css('background-image', 'url(' + image_url + ')')
                        .end()
                        .find('.additional-block')
                        .append(template);
                } else {
                    var text = response.error;
                    $('#uploads .errors').html('<p>' + text + '</p>');
                    $('html, body').animate({scrollTop: 0}, 1000);
                }
                setTimeout(function () {
                    progress_bar.slideUp();
                }, 1500)
            },
            error: function (error) {
                block.remove();
                var text = error.message;

                $('#uploads .errors').html(text);
                $('html, body').animate({
                    scrollTop: 0
                }, 1000);

            }
        });
    },

    verify_escorts_action: function (page) {

        var $container = $('#ui-view');
        $container.LoadingOverlay('show');

        $.ajax({
            url: lang_id + '/verify/ajax-escorts',
            type: 'post',
            data: {
                'per_page': 10,
                'escorts_page': page,
                'search_content': $('#not-verified-escort-search').val()
            },
            success: function (resp) {
                $container
                    .html(resp)
                    .LoadingOverlay('hide');
            }
        });

        return false;
    },

    load: function (url, escort_id) {

        //if(!Controls.confirmationChangeTab()) return false;

        url = lang_id + url;

        var data = {};

        if (typeof (escort_id) === 'undefined') {
            escort_id = 0;
        } else {
            data = {
                escort_id: escort_id,
                choose_escort: true
            };
        }

        var container = $("#ui-view");

        container.LoadingOverlay("show", {
            color: "rgba(239, 243, 249, 0.80)",
            zIndex: 1000
        });

        $.ajax({
            url: url,
            type: 'post',
            data: data,
            success: function (resp) {
                container.html(resp).LoadingOverlay("hide", true);

                // if( url.search('idcard') != -1 ){
                //     Verification.Init(container, escort_id);
                // }
            }
        });

        return false;
    },

    initUploader: function () {

        $('#verification-form').submit(function (event) {

            event.preventDefault();

            var data = new FormData(document.querySelector('#verification-form'));

            $("#ui-view").LoadingOverlay("show", true);
            $.ajax({
                url: '/private/verify/idcard',
                type: 'POST',
                processData: false,
                contentType: false,
                data: data,
                success: function (data) {
                    $('#ui-view').html(data).LoadingOverlay("hide", true);
                    Verification.init();
                }
            });
        });


        $('#overview-form').submit(function (event) {
            event.preventDefault();

            var data = $('#overview-form').serializeArray();
            $("#ui-view").LoadingOverlay("show", true);

            $.ajax({
                url: '/private/verify/idcard-overview',
                type: 'POST',
                data: data,
                success: function (data) {
                    $('#ui-view').html(data).LoadingOverlay("hide", true);
                    Verification.init();
                }
            });

        });


        $('body').on('change', '.custom-file-input', function () {
            $(this).next('.form-control-file').addClass("selected").html($(this).val());
        });
    },
    initEscortsProfiles: function () {
        $(window).resize(function () {
            if ($(window).width() < 567) {
                $('#verification_escorts a[data-ajax-url]').each(function () {
                    $(this).attr('data-ajax-url-disabled', $(this).attr('data-ajax-url'));
                    $(this).removeAttr('data-ajax-url');
                });
            } else {
                $('#verification_escorts a[data-ajax-url-disabled]').each(function () {
                    $(this).attr('data-ajax-url', $(this).attr('data-ajax-url-disabled'));
                    $(this).removeAttr('data-ajax-url-disabled');
                });
            }
        });

        $('.escort-thumb').click(function () {
            $('.escort-thumb').removeClass('selected_verify_escort');
            $(this).addClass('selected_verify_escort');
            $('#verify_escort_mobile').removeClass('btn-default').addClass('btn-success');
        });

        $('#verify_escort_mobile').click(function () {

            if ($(this).attr('data-ajax-url')) return false;

            var selected_escort = $('.selected_verify_escort');

            if (!selected_escort.length) return false;

            var url = $('.selected_verify_escort').find('a[data-ajax-url-disabled]').attr('data-ajax-url-disabled');
            $(this).attr('data-ajax-url', url);
            $(this).trigger('click');

        });

        $(window).trigger('resize');

    }

};


$(document).ready(function () {
    Verification.init();
});