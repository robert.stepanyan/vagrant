var Prices_page = function () {};

Prices_page.prototype.templates = {
    rate_item: function rate_item(params) {
        return "<div class=\"incall-rates rate-item justify-content-between\" id=\"" + params.type + "-rates-" + params.time + "-" + params.timestamp.id + "\">\n            <div class=\"incall-name\"> " + params.time + " " + params.timestamp.name + " <span class=\"text-grey\">(" + params.amount + " " + params.currency.name + ")</span></div>\n            <div class=\"incall-remove remove-rate-item text-grey d-flex align-items-center\" name=\"incall\">\n                <p class=\"uppercase\">remove</p>\n                <i class=\"fa fa-times\"></i>\n            </div>\n\n            <input type=\"hidden\" value='" + params.json + "' name=\"rates[" + params.type + "][]\">\n        </div>";
    }
};

Prices_page.prototype.event_handlers = {

    update_hidden_meta: function () {
        var block = $(this).parents('.call-option-row'),
            meta_input = block.find('.calltype-metadata');

        try {
            var data = JSON.parse(meta_input.val());

            if (block.data('type') == 'travel') {
                data['time'] = block.find('.amount-input').val();
                data['currency'] = block.find('.currency-select').val();
                data['type'] = block.data('type');
            } else {
                data['price'] = block.find('.amount-input').val();
                data['currency'] = block.find('.currency-select').val();
                data['type'] = block.data('type');
            }

            data = JSON.stringify(data);
            meta_input.val(data);

        } catch (e) {
            console.warn(e.message);
        }

    },

    toggle_payment_methods: function () {
        $(this).toggleClass("tapped");

        var $checkbox = $(this).find('input[type="checkbox"]').first();
        $checkbox.prop('checked', !$checkbox.is(':checked'));
    },

    add_rate: function (event) {
        var self = $(event.target);
        var block = $($(self).data('target'));
        var container = $($(self).data('container'));

        var data = {
            time: block.find('.time-value').val(),
            timestamp: {
                id: block.find('.timestamp-value').val(),
                name: block.find('.timestamp-value').parent().find('.dropdown-toggle-val').html()
            },
            amount: block.find('.amount-value').val(),
            currency: {
                id: block.find('.currency-value').val(),
                name: block.find('.currency-value').parent().find('.dropdown-toggle-val').html()
            },
            type: $(self).data('type')
        };

        // Validation
        if (!data.timestamp.id || !data.timestamp.name ||
            !data.currency.id || !data.currency.name ||
            !data.time || !data.amount) return console.warn("Some data are invalid, check here ~ ", data);
        //
        data['json'] = JSON.stringify({
            time: data.time,
            unit: data.timestamp.id,
            price: data.amount,
            currency: data.currency.id
        });

        var duplication = container.find($('#' + data.type + '-rates-' + data.time + '-' + data.timestamp.id));
        if (duplication.length == 0)
            container.append(this.templates.rate_item(data));
        else {
            duplication.replaceWith(this.templates.rate_item(data));
        }

    },

    remove_rate_item: function () {
        $(this).parents('.rate-item')
            .slideUp(function () {
                $(this).remove()
            });
    }
}

Prices_page.prototype.bind_events = function () {
    $(document).on("change", ".call-option-row input:not(hidden), .call-option-row select", this.event_handlers.update_hidden_meta);
    $(document).on('click', '.rate-add-btn', this.event_handlers.add_rate.bind(this));
    $(document).on('click', '.remove-rate-item', this.event_handlers.remove_rate_item);
    $(document).on('click', '.prices-page .button-tap', this.event_handlers.toggle_payment_methods)
}

Prices_page.prototype.init = function () {
    this.bind_events();

    return this;
}

$(function () {

    if (!window.PRICES_PAGE)
        window.PRICES_PAGE = (new Prices_page()).init();

})