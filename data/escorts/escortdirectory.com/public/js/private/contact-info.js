"use strict";

var Contact_Info_Page = function () {};

Contact_Info_Page.prototype.init = function () {
    this.bind_events();
    this.init_multiselect();
    this.removeLoaders();
    return this;
};

Contact_Info_Page.prototype.update = function () {
    this.init_multiselect();
    this.removeLoaders();
    return this;
};


Contact_Info_Page.prototype.bind_events = function () {

};

Contact_Info_Page.prototype.removeLoaders = function () {
    $('.main').LoadingOverlay("hide", true);
    $('.wait-for-load').removeClass("wait-for-load");
};

Contact_Info_Page.prototype.init_multiselect = function () {

    
    $('.multiple-dropdown-box').multiselect({

        onChange: function (option, checked, select) {
            select = $(this.$select);
            var list = $(this.$ul)
            var labels = [];
            list.find('input:checked').each(function() {
                labels.push($(this).val());
            })

            select.val(labels);
        },
        buttonText: function(options) {
            var labels = [];
            options.each(function () {
                labels.push($(this).text())
            });

            if (options.length > 3) {
                return labels[0] + ',' + labels[1] + ' +' + options.length;
            } else if (options.length >= 1) {
                return labels.join(',');
            } else {
                return headerVars.dictionary.nothing_selected;
            }
        }
    });
    $('.multiple-dropdown-box').parents('.dropdown-selection').first().removeClass('d-none').addClass('d-flex');

}


$(function () {
    if (!window.CONTACT_INFO_PAGE)
        window.CONTACT_INFO_PAGE = (new Contact_Info_Page()).init();

    window.CONTACT_INFO_PAGE.update();
});

$('.geolocation').on('keyup',function () {
    var searchIng = $(this).val();
    var states = $('li.states');
    if (searchIng && states)
    {
        states.hide();
    }else{
        states.show();
    }
    searchIng = new RegExp(searchIng.toUpperCase());
    $(this).parent('ul').find('li.searchable').each(function() {
        var searchIn = $(this).children('a').text().trim();
        searchIn = searchIn.toUpperCase();
        var states = $('li.states');
        if (!searchIng.test(searchIn))
        {
            $(this).hide();
        }else{
            $(this).show();
        }
    });
});
$('#languageFilter').click(function () {
    var parent = $(this).next('ul');
    setTimeout(function(){parent.children('input').focus();},10);
});