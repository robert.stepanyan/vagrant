/**
 * Created by Zhora on 23.10.2017.
 */

var Notify = {
    alert: function alert(type, message, delay) {
        var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
        var max = options.max || 1;
        var hash = Sceon.cyrb53(message);
        if ($('[data-hash="' + hash + '"]').length >= max) return;
        $.notify({
            message: '<strong data-hash="' + hash + '">' + message + '<strong>'
        }, {
            type: type,
            delay: delay ? delay : 3000,
            animate: {
                enter: 'animated fadeIn',
                exit: 'animated fadeOut'
            },
            placement: {
                from: "top",
                align: "right"
            },
            offset: {
                x: screen.width > 576 ? 20 : 0,
                y: 70,
                spacing: 10,
                z_index: 9999999
            }
        });
    }
};
