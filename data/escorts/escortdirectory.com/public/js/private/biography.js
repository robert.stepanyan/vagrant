var Biography_page = (function () {

    function Biography_page() {}

    Biography_page.prototype.templates = {
        blocked_country: function (val, id) {
            return '<div class="country-blocked justify-content-between">'+
                    '<div class="country-name">' + val + '</div>'+
                    '<div class="country-unblock text-grey d-flex align-items-center">'+
                        '<p class="uppercase">[ Unblock ]</p>'+
                        '<i class="fa fa-times"></i>'+
                    '</div>'+
                    '<input type="hidden" name="block_countries[]" value="'+id+'" />'+
                '</div>';
        }
    };
    
    Biography_page.prototype.event_handlers = {
        toggle_keywords: function () {
            $(this).toggleClass("tapped");
    
            var $checkbox = $(this).find('input[type="checkbox"]').first();
            $checkbox.prop('checked', !$checkbox.is(':checked'));
        },
        block_country: function () {
            var val = $("#blockCity").find(".dropdown-toggle-val").html();
            var id = $("#block_country").val();
    
            if ($("#blockCity").find(".dropdown-toggle-val").hasClass('pristine')) return console.info('Nothing selected ');
            if ($('input[name="block_countries[]"][value="' + id + '"]').length > 0) return console.info('Already Blocked');
    
            var container = this.templates.blocked_country(val, id);
            $(".country-blocked-container").append(container);
        },
        unblock_country: function () {
            $(this).parents(".country-blocked").slideUp('slow', function () {
                $(this).remove();
            });
        }
    };
    
    Biography_page.prototype.bind_events = function () {

        if (!window.BIOGRAPHY_PAGE){
            $(document).on("click", ".biography-page .button-keyword-tap, .apps-available-wrapper .button-tap", this.event_handlers.toggle_keywords);
            $(document).on("click", ".block-country-trigger", this.event_handlers.block_country.bind(this));
            $(document).on("click", ".country-unblock", this.event_handlers.unblock_country);

           /* $('body').on('mouseup','.dropdown-toggle',function () {
                var $body = $('body');
                var ariaExpand = $(this).attr('aria-expanded');

                if (ariaExpand === 'false' || !ariaExpand) {
                    $body.addClass('freezed');
                } else{
                    $body.removeClass('freezed');
                }
            });

            $(document).mouseup(function(e)
            {
                var container = $('.dropdown-toggle');

                // if the target of the click isn't the container nor a descendant of the container
                if (!container.is(e.target))
                {
                    $('body').removeClass('freezed');
                }
            });*/
        }
    };

    Biography_page.prototype.init_autosize_inputs = function () {
        var $inputs = $('input[data-autosize="true"]');

        $inputs.on('input', function() {
            var inputWidth = $(this).textWidth();
            $(this).css({
                width: inputWidth + 30
            })
        }).trigger('input');
    };
    
    Biography_page.prototype.init = function () {

        try {
            this.bind_events();
            this.init_autosize_inputs();
        } catch (e) {
            console.error("Whoops there was an error !!!", e);
        }
        return this;
    }

    return Biography_page;

})();

$(function () {
    window.BIOGRAPHY_PAGE = (new Biography_page()).init();
})

$(document).ready(function(){
    'use strict';
    $('a.dropdown-toggle').attr('tabindex',0);
    $('a.dropdown-toggle').blur(function () {
        $('body').removeClass('scroll-hidden');
    });
    $('a.dropdown-toggle').click(function () {
        $('body').addClass('scroll-hidden');
    });
});

$('.geolocation').on('keyup',function () {
    var searchIng = $(this).val();
    var states = $('li.states');
    if (searchIng && states)
    {
        states.hide();
    }else{
        states.show();
    }
    searchIng = new RegExp(searchIng.toUpperCase());
    $(this).parent('ul').find('li.searchable').each(function() {
        var searchIn = $(this).children('a').text().trim();
        searchIn = searchIn.toUpperCase();
        var states = $('li.states');
        if (!searchIng.test(searchIn))
        {
            $(this).hide();
        }else{
            $(this).show();
        }
    });
});
$('#blockCity, #nationalityFilter').click(function () {
    var parent = $(this).next('ul');
    setTimeout(function(){parent.children('input').focus();},10);
});
