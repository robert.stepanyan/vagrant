/*
var gotd = {

    init: function(){
        this.container = $('#ui-view'),
            this.initEvents();
    },

    initEvents: function(){
        alert();
        self = this;
        $('.action-buttons').on('click',function(){
            $(this).attr('disabled',true);
            let isAgency = $('input[name=is_agency]').length;
            let type = $(this).data('type');
            self.container.LoadingOverlay("show", {
                color: "rgba(239, 243, 249, 0.80)",
                zIndex: 1000
            });
            if ( isAgency <= 0 ) {
                if (type == 'misscall') {
                    self.phoneCall();
                    $('#phone_verification-action-sms').slideUp();
                } else {
                    self.sms();
                    $('#phone_verification-action-misscall').slideUp();
                }

                $(`#phone_verification-action-${type}`).slideDown();
            }else{
                var phoneNumber;
                $('input[name=phone]').each(function (index,val) {
                    if (val.checked) {
                        phoneNumber = val.value;
                    }
                });
                var agencyId = $('input[name=agency_id]').val();
                if (type == 'misscall') {
                    self.agencyPhoneCall(phoneNumber,agencyId);
                    $('#phone_verification-action-sms').slideUp();
                } else {
                    self.agencySms(phoneNumber,agencyId);
                    $('#phone_verification-action-misscall').slideUp();
                }

                $(`#phone_verification-action-${type}`).slideDown();
            }
        });

        $('input[name=phone]').on('change', function (e) {
            let isAgency = $('input[name=is_agency]');
            if (isAgency.length > 0) {
                $('.collapse').removeClass('show');
                $('#phone_verification-action-misscall').slideUp();
                $('#phone_verification-action-sms').slideUp();
                $('.action-buttons').attr('disabled',false);
            }
        });

        $('.confirmation-form').on('submit',function(event){
            event.preventDefault();

            let type = $(this).data('type');
            self.container.LoadingOverlay("show", {
                color: "rgba(239, 243, 249, 0.80)",
                zIndex: 1000
            });

            $.ajax({
                url: '/phone-verification/confirm-code/',
                method: 'POST',
                data: $( this ).serializeArray(),
                dataType: 'json',
                success: function (resp) {
                    if(resp.status === 'success'){
                        self.container.html(resp.html);
                    }
                    else{
                        Notify.alert('danger', 'Wrong verification code ');
                    }
                    self.container.LoadingOverlay("hide");
                }
            });
        });
    },

    phoneCall: function(){
        let self = this;
        $.ajax({
            url: '/phone-verification/phone-call/',
            method: 'POST',
            dataType: 'json',
            success: function (resp) {

                if(resp.status === 'success'){


                    $("#cellNo").inputmask(resp.first + "9999",{
                        placeholder: "x",
                        showMaskOnHover: false
                        //clearMaskOnLostFocus: false
                    });
                }
                else{
                    Notify.alert('danger', 'Error please try later ');
                }
                self.container.LoadingOverlay("hide");

            }
        });
    },

    agencyPhoneCall: function(phone,agencyId){
        let self = this;
        $.ajax({
            url: '/phone-verification/agency-phone-call/',
            method: 'POST',
            dataType: 'json',
            data: {phone:phone,agency_id:agencyId},
            success: function (resp) {

                if(resp.status === 'success'){


                    $("#cellNo").inputmask(resp.first + "9999",{
                        placeholder: "x",
                        showMaskOnHover: false
                        //clearMaskOnLostFocus: false
                    });
                }
                else{
                    Notify.alert('danger', 'Error please try later ');
                }
                self.container.LoadingOverlay("hide");

            }
        });
    },

    sms: function(){
        let self = this;
        $.ajax({
            url: '/phone-verification/send-sms/',
            method: 'POST',
            success: function (r) {
                self.container.LoadingOverlay("hide");
            }
        });
    },

    agencySms: function(phone,agencyId){
        let self = this;
        $.ajax({
            url: '/phone-verification/agency-send-sms/',
            method: 'POST',
            data: {phone:phone,agency_id:agencyId },
            success: function (r) {
                self.container.LoadingOverlay("hide");
            }
        });
    }

}

$(document).ready(function () {
    phoneVerification.init();
});*/
