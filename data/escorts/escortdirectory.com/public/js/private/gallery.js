var Cropper = (function () {

    function Cropper() {
        this.el = null;
        this.els = {};

        this.disabled = false;
        this.max = {
            x: 0,
            y: 0
        };

        this.moved = false;
        this.draggie = null;
        this.initial = {};

        return this;
    };

    Cropper.globals = {
        escort: function() {
            return $('input[name=escort]').val() || $('input[name=escort_id]').val();
        }
    };

    Cropper.rotate = function (e) {
        e.preventDefault();

        var $btn = $(e.target),
            $parent = $btn.parents('.settings-list'),
            $wrapper = $btn.parents('.image-item.wrapper'),
            photo_id = $parent.data('photo'),
            degree = 90,
            self = this,
            extension = $wrapper.attr('cubix:photo-ext'),
            hash = $wrapper.attr('cubix:photo-hash');

        $wrapper.addClass('disabled');

        if ($btn.data('direction') == 'right') {
            degree = -90;
        }

        $.ajax({
            url: '/private/photos?a=set-rotate&escort=' + Cropper.globals.escort() + '&hash=' + hash + '&extension=' + extension,
            type: 'get',
            data: {
                photo_id: photo_id,
                degree: degree
            },
            success: function (resp) {
                resp = JSON.parse(resp);

                $wrapper.removeClass('disabled');

                if (resp.error) {
                    alert('An error occured');
                } else {

                    var preventCache = (new Date()).getTime();
                    var image = $wrapper.find('img.image');

                    var re = /((\?|\#).*)$/;
                    var newImage = image.attr('src').replace(re, "");

                    image
                        .attr('src', newImage + '?cache=' + preventCache)
                        .css({
                            left: 0,
                            top: 0
                        });
                }
            }
        });
    }

    Cropper.prototype.save_adjustment = function (args) {

        this.disable();

        args = Object.assign({}, args, {
            px: args.x / 265,
            py: args.y / 362
        });

        var data = Object.assign({}, {
            a: 'set-adj',
            photo_id: this.el.attr('cubix:photo-id'),
            escort: Cropper.globals.escort()
        }, args);
        data.x = Math.abs(data.x);
        data.y = Math.abs(data.y);
        data.px = data.px * this.ratio.x;
        data.py = data.py * this.ratio.y;
        $.ajax({
            url: '/private/photos',
            type: 'get',
            data: data,
            success: function (resp) {
                resp = JSON.parse(resp);

                if (resp.error) {
                    alert('An error occured');
                } else {
                    this.set_initial(args.x, args.y);
                }

                this.enable();

            }.bind(this)
        });
    }

    Cropper.prototype.centerize = function(initial){
        var never_cropped_before = !$(this.el).attr('cubix:initial');

        if( never_cropped_before && $(this.els.img).width()< $(this.el).width()) {
            initial.x = Math.abs($(this.el).width() - $(this.els.img).width()) / 2
        }

        if(never_cropped_before &&  $(this.els.img).height() < $(this.el).height()) {
            initial.y = Math.abs($(this.el).height() - $(this.els.img).height()) / 2
        }

        this.set_initial(initial.x, initial.y);
    }

    Cropper.prototype.initialize = function (el) {

        var self = this;
        this.ratio = {
            x : 150/265,
            y : 205/362
        };
        this.el = el;
        this.els.img = $(this.el).find('img').get(0);

        try {
            var initial = JSON.parse($(el).attr('cubix:initial'));
        }catch(e) {
        }

        if(typeof initial == "undefined") {
            initial = {x: 0, y: 0};
        }

        this.els.img.addEventListener('load', function () {
            self.max = {
                x: $(self.els.img).width() - $(self.el).width(),
                y: $(self.els.img).height() - $(self.el).height()
            }
            console.log("onload", self.max);
        });

        if (initial) {
            initial.x = initial.x >0 ? -1*initial.x : initial.x;
            initial.y = initial.y >0 ? -1*initial.y : initial.y;
            initial.x  = initial.x / this.ratio.x;
            initial.y  = initial.y / this.ratio.y;
            this.set_initial(initial.x, initial.y);
        }

        var position = initial;

        interact(this.els.img).draggable({
            listeners: {
                start (event) {
                    console.log(event)
                },
                move (event) {
                    position.x += event.dx;
                    position.y += event.dy;

                    if (self.max.x <= 0 || self.max.y <= 0){
                        self.max.x = $(self.els.img).width() - $(self.el).width();
                        self.max.y = $(self.els.img).height() - $(self.el).height()
                    }

                    if ( position.x > 0 ) position.x = 0;
                    if ( position.y > 0) position.y = 0;

                    if ( Math.abs(position.x) >= self.max.x) {
                        position.x =  -1*Math.abs(self.max.x);
                    }

                    if (Math.abs(position.y) >= self.max.y ) {
                        position.y = -1*Math.abs(self.max.y);
                    }

                    event.target.style.left = position.x + 'px';
                    event.target.style.top = position.y + 'px';
                },
                end(event) {
                    self.save_adjustment(position);
                }
            }
        })

        return this;
    }

    Cropper.prototype.enable = function () {
        $(this.el).removeClass('disabled');
        this.disabled = false;
    }

    Cropper.prototype.disable = function () {
        $(this.el).addClass('disabled');
        this.disabled = true;
    }

    Cropper.prototype.update = function () {
        $(this.els.img).css({
            left: this.initial.x,
            top: this.initial.y
        });
    }

    Cropper.prototype.set_initial = function (x, y) {
        x = x > 0? -1*x : x;
        y = y > 0? -1*y : y;
        this.initial = {
            x: x,
            y: y
        };
        this.update();
    }

    return Cropper;

})();

var Gallery_page = (function () {

    function Gallery_page() {
    };

    Gallery_page.prototype.globals = {

        sortable_images_config: {
            animation: 250,
            handle: '.drag-by-this',
            onEnd: function (evt) {
                var ids = [],
                    escort_id = $('input[name="escort_id"]').val();

                $('#gallery-images-list input[name="photo_id"], #additional-images-container input[name="photo_id"]').each(function () {
                    ids.push($(this).val());
                });

                $.ajax({
                    url: 'private/photos',
                    method: "get",
                    data: {
                        a: 'sort',
                        photo_id: ids,
                        escort: escort_id,
                    }
                })
            },

        },
        is_uploading: {
            profile_pic: null,
            natural_pic: null,
            video: null,
        }
    }

    Gallery_page.prototype.event_handlers = {

        trigger_file_dialog: function () {
            $(this).siblings("input").click();
        },

        on_drag_btn_click: function (e) {
            var $btn = $(e.target);
            $btn
                .parents('.image-item')
                .toggleClass('drag-enabled')
                .find('img.image')
                .trigger('click');
        },

        remove_image: function (event) {
            var $button = $(event.target),
                filename = $button.data('filename'),
                file_id = $button.attr('data-photo-id'),
                escort_id = $('input[name="escort"]').val();

            if ($button.hasClass('completely'))
                $button.closest(".gallery-image").fadeOut();
            else{
                $button.closest('.gallery-image').siblings("input").click();
                $button.closest(".gallery-image").removeClass("existance").find(".image,.remove-image").remove();
            }

            var index = this.globals.uploaded_files.indexOf(filename);
            if (index >= 0) this.globals.uploaded_files.splice(index, 1);

            $.ajax({
                url: "/private/photos",
                method: 'get',
                data: {
                    a: 'delete',
                    photo_id: [file_id],
                    escort: escort_id
                }
            })

            return false;
        },

        init_upload: function (event) {
            // Gallery-Step.phtml

            var self = this,
                url = $('.upload-url').val(),
                $file_input = $(event.target),
                progressBar = $('<div class="mt-2 progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>'),
                mode = $file_input.is("[multiple]") ? 'multiple' : 'single',
                result_container = mode == 'multiple' ? $('#additional-images-container > .image-blocks') : $file_input.parent().find("div.gallery-image");

            $file_input.simpleUpload(url, {
                allowedExts: ["jpg", "jpeg", "png"],
                allowedTypes: ["image/jpeg", "image/png", "image/x-png", "image/jpg"],
                limit: 50,
                maxFileSize: 5000000, //5MB in bytes
                filename: null,
                lastModified: null,
                start: function (file) {
                    this.filename = file.name;
                    this.lastModified = file.lastModified;
                    var samePhoto = $('span.same-photo');
                    if ($.inArray(file.name+file.lastModified, self.globals.uploaded_files) == -1) {
                        self.globals.uploaded_files.push(file.name+file.lastModified);

                    } else {
                        var error_template = '<span class="same-photo">' + headerVars.dictionary.dont_upload_same_photo + '</span>';
                        if ( samePhoto.length < 1 )
                        {
                            Notify.alert('danger', error_template);
                        }

                        return false;
                    }
                    if (mode == 'multiple') {
                        $('#additional-images-container > .errors').prepend(progressBar)
                    } else {
                        $('#uploads').append(progressBar);
                    }

                },
                progress: function (progress) {
                    progressBar.css('width', progress + '%')
                },
                success: function (data) {
                    progressBar.fadeOut();
                    var response = {};
                    try {
                        response = JSON.parse(data);

                        if (response.error == 0 && response.finish) {

                            var image_url = response && response.photo_url ? response.photo_url : "/images/default-image.jpeg";

                            if (mode == 'single') // Single File
                            {
                                result_container
                                    .addClass("existance")
                                    .append("<div class=\"image\" style=\"background-image: url(" + image_url + (");\"></div>" +
                                        "<div data-filename=\"" + this.filename+this.lastModified + "\" class=\"remove-image edit-image\" data-photo-id=\"" + response.photo_id + "\"></div>" +
                                        "<input type=\"hidden\" name=\"photo_id\" value=\"" + response.photo_id + "\" />" +
                                        ""));
                            } else {

                                $('#additional-images-container > .image-blocks').append("" +
                                    "<div class=\"gallery-image mr-2 existance\">" +
                                        "<div class=\"image\" style=\"background-image: url(" + image_url + (");\"></div>" +
                                        "<div data-filename=\"" + this.filename+this.lastModified + "\" class=\"remove-image completely\" data-photo-id=\"" + response.photo_id + "\"></div>" +
                                        "<input type=\"hidden\" name=\"photo_id\" value=\"" + response.photo_id + "\" />" +
                                    "</div>" +
                                    ""))
                            }
                        } else {
                            var index = self.globals.uploaded_files.indexOf(this.filename+this.lastModified);

                            var error_template = "<p class='text-danger'>" + response.error + "</p>";

                            if (mode == 'multiple')
                                $('#additional-images-container > .errors').html(error_template).hide().fadeIn(500);
                            else
                                $('#uploads > .errors').html(error_template).hide().fadeIn(500);
                        }


                    } catch (e) {
                        var index = self.globals.uploaded_files.indexOf(this.filename+this.lastModified);

                        console.warn(e.message)
                        var image_url = response && response.photo_url ? response.photo_url : "/images/default-image.jpeg";

                        if (mode == 'single') // Single File
                        {
                            result_container
                                .addClass("existance")
                                .append("" +
                                    "<div class=\"image\" style=\"background-image: url(" + image_url + (");\"></div>" +
                                    "<div data-filename=\"" + this.filename+this.lastModified + "\" class=\"remove-image edit-image\" data-photo-id=\"" + response.photo_id + "\"></div>" +
                                    "<input type=\"hidden\" name=\"photo_id\" value=\"" + response.photo_id + "\" />" +
                                    ""));
                        } else {

                            $('#additional-images-container > .image-blocks').append("" +
                                "<div class=\"gallery-image mr-2 existance\">" +
                                "<div class=\"image\" style=\"background-image: url(" + image_url + (");\"></div>" +
                                "<div data-filename=\"" + this.filename+this.lastModified + "\" class=\"remove-image completely\" data-photo-id=\"" + response.photo_id + "\"></div>" +
                                "<input type=\"hidden\" name=\"photo_id\" value=\"" + response.photo_id + "\" />" +
                                "</div>" +
                                ""))
                        }
                    }
                },
                error: function (error) {
                    if(error.name == 'InvalidFileExtensionError' || error.name == 'InvalidFileTypeError'){
                        alert('Please upload only image files \r\n (.jpg or .png) in this step.');
                    }else{
                        alert(error.message);
                    }
                    var index = self.globals.uploaded_files.indexOf(this.filename+this.lastModified);
                    $(progressBar).remove();
                }

            });
        },

        trigger_file: function (e) {
            e.stopPropagation();
            console.log("439");
            if (e.target !== e.currentTarget) return;
            console.log($(e.currentTarget));

            var $elm = $(e.currentTarget);
            $elm
                .find('input[type=file]')
                .trigger('click')
                .end()
                .parents('.dropdown-toggle')
                .dropdown('toggle')
        },

        toggle_settings_bar: function (e) {
            e.stopPropagation();

            var $container = $(this).parents(".settings-container").find(".settings-list");
            $container.stop(true, true).fadeToggle();

            $('html, body').click(function () {
                $container.stop(true, true).fadeOut();
            })
        },

        set_main: function (e) {
            var $btn = $(e.target),
                $parent = $btn.parents('.settings-list'),
                photo_id = $parent.data('photo'),
                ids = [photo_id];

            $.ajax({
                method: "GET",
                url: '/private/photos',
                data: {
                    a: 'set-main',
                    photo_id: ids,
                    escort_id: Cropper.globals.escort()
                },
                beforeSend: function () {
                    $btn.closest('.image-item.wrapper').addClass('disabled')
                },
                success: function (result) {
                    $('.image-item.main-pic')
                        .removeClass('main-pic')
                        .addClass('public-pic');

                    $btn.closest('.image-item.wrapper')
                        .removeClass('disabled private-pic public-pic')
                        .addClass('main-pic');
                }
            });
        },

        delete_picture: function (e) {
            var $btn = $(e.target),
                id = $btn.parents('.settings-list').attr('data-photo'),
                $wrapper = $btn.parents('.image-item'),
                self = this;

            if (id == 'natural')
                return this.event_handlers.delete_natural_picture(e);

            $wrapper.addClass('disabled');

            $.ajax({
                url: '/private/photos',
                type: 'get',
                data: {
                    a: 'delete',
                    'photo_id': [id],
                    escort: $('input[name=escort]').val()
                },
                success: function (resp) {
                    resp = JSON.parse(resp);

                    if (resp.status) {
                        $wrapper.remove();
                    } else {
                        alert(resp.error);
                    }
                }
            });
        },

        delete_natural_picture: function (e) {

            var $btn = $(e.target),
                $wrapper = $btn.parents('.image-item');

            $wrapper.addClass('disabled')

            $.ajax({
                url: '/private/natural-pic',
                type: "GET",
                data: {
                    'a': 'delete'
                },
                success: function (resp) {
                    $wrapper.remove();
                    $('.natural-picture-container-description').hide();
                }
            })
        },

        set_public_private: function (e) {

            var $btn = $(e.target),
                $parent = $btn.parents('.settings-list'),
                photo_id = $parent.data('photo'),
                $wrapper = $btn.parents('.image-item.wrapper'),
                ids = [photo_id];

            var data = {
                a: 'make-public',
                photo_id: ids,
                escort: Cropper.globals.escort()
            }

            $wrapper.addClass('disabled');

            if ($btn.hasClass('set-private'))
                data.a = "make-private"

            $.ajax({
                type: "GET",
                url: "/private/photos",
                data: data,
                success: function (resp) {
                    if ($btn.hasClass('set-private'))
                        $wrapper
                            .removeClass("public-pic main")
                            .addClass('private-pic');
                    else
                        $wrapper
                            .removeClass("private-pic")
                            .addClass('public-pic');

                    $wrapper.removeClass('disabled');
                }

            })
        },

        open_video: function (e) {

            var $elm = $(e.currentTarget),
                width = $elm.attr('data-width').trim(),
                height = $elm.attr('data-height').trim(),
                video = $elm.attr('data-rel').trim(),
                image = $elm.attr('data-src').trim(),
                video = $('#video-config').val() + video + '_' + height + 'p.mp4';

            if (image) image = image.replace('m330', 'orig');

            $.fancybox.open({
                src: video,
                opts: {
                    caption: 'EscortDirectory.com',
                    thumb: image,
                    protect: true,
                    width: width,
                    height: height,
                    transitionEffect: 'rotate',
                    buttons: [
                        "close"
                    ],
                }
            });

        },

        delete_video: function (e) {
            e.stopPropagation();
            e.preventDefault();

            var $btn = $(e.target),
                $wrapper = $btn.parents('.image-item.video'),
                video_id = $wrapper.data('id');

            $wrapper.addClass('disabled');

            $.ajax({

                url: '/private/video',
                type: 'get',
                data: {
                    a: 'delete',
                    video_id: video_id,
                    escort: Cropper.globals.escort()
                },
                success: function (resp) {
                    $wrapper.remove();
                    $('.gallery-page .dropdown-item.attach-video').removeClass('disabled')
                }
            })
        },

        hide_progress_status: function (e) {

            var $btn = $(e.target),
                target = $btn.data('abort-target');

            $btn.siblings('.text').html('');
            $btn.parents('.progress-wrapper').removeClass('uploading stop');

            switch (target) {
                case 'natural_pic':
                    $('.natural-photo-form').trigger('reset');
                    break;

                case "profile_pic":
                    $('.profile-photo-form').trigger('reset');
                    break;

                case "video":
                    $('.video-upload-form').trigger('reset');
                    break;
            }
        },

        abort_uploading: function (e) {

            var $btn = $(e.target),
                target = $btn.data('abort-target');

            var xhr = this.globals.is_uploading[target];
            $btn.parents('.progress-wrapper').hide();

            switch (target) {
                case 'natural_pic':
                    $('.natural-photo-form').trigger('reset');
                    break;

                case "profile_pic":
                    $('.profile-photo-form').trigger('reset');
                    break;

                case "video":
                    $('.video-upload-form').trigger('reset');
                    break;
            }

            if (xhr) {
                xhr.abort();
                this.globals.is_uploading[target] = 'aborted';
            }

        },

        submit_parent_form: function (e) {
            var $elm = $(e.target);
            $elm.parent().trigger('submit');
        }
    }

    Gallery_page.prototype.bind_events = function () {
        $('input.natural-photo-file, input.profile-photo-file, input.video-file').change(this.event_handlers.submit_parent_form.bind(this));
        $('.process-status .close').click(this.event_handlers.hide_progress_status);
        $('.abort-upload').click(this.event_handlers.abort_uploading.bind(this));

        if (!window.GALLERY_PAGE)
            $(document)
                .on("click", ".attach-file", this.event_handlers.trigger_file)
                .on("click", ".gallery-image:not(.existance)", this.event_handlers.trigger_file_dialog)
                .on("click", ".remove-image", this.event_handlers.remove_image.bind(this))
                .on("change", "input[type=file].gallery-image", this.event_handlers.init_upload.bind(this))
                .on("click", ".settings-trigger", this.event_handlers.toggle_settings_bar)
                .on("click", '.set-main', this.event_handlers.set_main)
                .on("click", ".rotate", Cropper.rotate)
                .on("click", ".settings-list .delete", this.event_handlers.delete_picture.bind(this))
                .on("click", ".set-private, .set-public", this.event_handlers.set_public_private.bind(this))
                .on('click', '.video-trigger', this.event_handlers.open_video.bind(this))
                .on('click', '.delete-video', this.event_handlers.delete_video.bind(this))
            $('.gallery-page').on('click', '.drag-enable-btn', this.event_handlers.on_drag_btn_click.bind(this))
    }

    Gallery_page.prototype.init_images_crop = function () {
        $('.image-item.wrapper').each(function () {
            (new Cropper()).initialize($(this));
        });
    }

    Gallery_page.prototype.init_sortable_images = function () {
        this.globals.uploaded_files = [];
        var elm = document.querySelector("#gallery-images-list");

        if(elm) {
            Sortable.create(elm, this.globals.sortable_images_config);
        }
    }

    Gallery_page.prototype.init_natural_photos = function () {

        var self = this;
        var file_type = 'natural_pic';

        $('.natural-photo-form').submit(function (e) {

            e.preventDefault();

            if (self.globals.is_uploading[file_type]) {
                return alert("File is uploading, please wait ...");
            }

            var $form = $(this);
            var progressBar = $('.natural-pics-progress-bar .progress-bar');
            var $progressBarWrapper = $('.natural-pics-progress-bar');
            var error_bar = $('.natural-pics-progress-bar .process-status .text');
            var total = 0;

            $progressBarWrapper.addClass('uploading').removeClass('stop');
            self.globals.is_uploading[file_type] = true;

            error_bar.html('');
            progressBar.css('width', 0 + '%')
            $('.natural-pics-progress-bar').show();

            var formdata = new FormData(this),
                $file = formdata.get('natural-photo-file'),
                $fileId = (new Date()).getTime().toString(36);

            var upload_chunk = function ($file, $fileId, $start) {

                // Force Stop Recursion
                if (self.globals.is_uploading[file_type] == 'aborted') {
                    return self.globals.is_uploading[file_type] = null;
                }

                var chunk,
                    $chunkSize = 524288, // 0.5 MB
                    total = $start + $chunkSize;

                if (total >= $file.size) {
                    total = $file.size
                }

                if ($file.mozSlice) {
                    // Mozilla based
                    chunk = $file.mozSlice($start, total);
                } else if ($file.webkitSlice) {
                    // Chrome, Safari, Konqueror and webkit based
                    chunk = $file.webkitSlice($start, total);
                } else {
                    // Opera and other standards browsers
                    chunk = $file.slice($start, total);
                }

                self.globals.is_uploading[file_type] = $.ajax({
                    type: "POST",
                    url: $form.attr('action'),
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('X-File-Id', $fileId);
                        xhr.setRequestHeader('X-File-Name', $file.name);
                        xhr.setRequestHeader('X-File-Size', $file.size);
                        xhr.setRequestHeader('X-File-Resume', 1);
                    },
                    contentType: "application/json",
                    data: chunk,
                    dataType: ' json',
                    processData: false,
                    success: function (response) {

                        var $newStartpos = 100 * total / $file.size;
                        progressBar.css('width', $newStartpos + '%')

                        if (!response.finish) {

                            if (!response.error) {

                                upload_chunk($file, $fileId, total)
                            } else {
                                self.globals.is_uploading['natural_pic'] = null;

                                error_bar.html(response.error)
                                    .parent()
                                    .removeClass('text-success')
                                    .addClass('text-danger');
                                $progressBarWrapper.addClass('stop').removeClass('uploading');
                            }
                        } else {
                            self.globals.is_uploading['natural_pic'] = null;

                            if (response.error) {
                                error_bar.html(response.error)
                                    .parent()
                                    .removeClass('text-success')
                                    .addClass('text-danger');
                                $progressBarWrapper.addClass('stop').removeClass('uploading');

                            } else {
                                self.globals.is_uploading['profile_pic'] = false;
                                $progressBarWrapper.addClass('stop').removeClass('uploading');
                                progressBar.css('width', '0%')

                                error_bar.html(headerVars.dictionary.photo_upload_success)
                                    .parent()
                                    .removeClass('text-danger')
                                    .addClass('text-success');

                                var template = '<div class="image-item  pending ">' +
                                    '<div class="image" style="background-image: url(' + response.photo_url + ')"></div>' +

                                    '<div class="pending-info">' +
                                    '<i class="far fa-clock"></i>' +
                                    '<p class="uppercase weight">' + (headerVars.dictionary.pending || "") + '</p>' +
                                    '</div>' +

                                    '<div class="options-bar d-flex justify-content-between">' +
                                    '<div class="likes-container d-flex align-items-center">' +
                                    //'<div class="likes"><i class="fas fa-heart"></i></div>'+
                                    //'<div class="likes-value"></div>'+
                                    '</div>' +
                                    '<div class="settings-container d-flex align-items-center">' +
                                    '<div class="picture-status">' +
                                    '<span class="status-natural">' + (headerVars.dictionary.natural || "") + '</span>' +
                                    '</div>' +
                                    '<div class="settings-trigger"><i class="fas fa-cog"></i></div>' +
                                    '<div class="settings-list" data-photo="natural">' +
                                    '<div class="setting delete">' + (headerVars.dictionary.delete || "") + '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>'
                                '</div>';

                                $('.natural-pics').empty().append(template);
                                $('.natural-picture-container-description').show();
                            }
                        }
                    }
                })

            }
            upload_chunk($file, $fileId, 0);
        })
    }

    Gallery_page.prototype.init_editor = function () {
        APPLICATION.update_tinymce_editors();
    };

    Gallery_page.prototype.init_profile_photos = function () {

        var self = this;
        var file_type = 'profile_pic';

        $('.profile-photo-form').submit(function (e) {

            e.preventDefault();

            if (self.globals.is_uploading[file_type]) {
                return alert("File is uploading, please wait ...");
            }

            var $form = $(this);
            var progressBar = $('.profile-pics-progress-bar .progress-bar');
            var $progressBarWrapper = $('.profile-pics-progress-bar');
            var error_bar = $('.profile-pics-progress-bar .process-status .text');
            var filecontrol_previx = 'filecontrol-' + gallery_id;
            var gallery_id = $('.g_id').first().val();
            var total = 0;

            $progressBarWrapper.addClass('uploading').removeClass('stop');

            error_bar.html('');
            progressBar.css('width', 0 + '%')
            $('.profile-pics-progress-bar').show();

            var formdata = new FormData(this),
                $file = formdata.get('profile-photo-file'),
                file_type = 'profile_pic',
                $fileId = (new Date()).getTime().toString(36);

            var upload_chunk = function ($file, $fileId, $start) {

                // Force Stop Recursion
                if (self.globals.is_uploading[file_type] == 'aborted') {
                    return self.globals.is_uploading['profile_pic'] = null;
                }

                var chunk,
                    $chunkSize = 524288, // 0.5 MB
                    total = $start + $chunkSize;

                if (total >= $file.size) {
                    total = $file.size
                }

                if ($file.mozSlice) {
                    // Mozilla based
                    chunk = $file.mozSlice($start, total);
                } else if ($file.webkitSlice) {
                    // Chrome, Safari, Konqueror and webkit based
                    chunk = $file.webkitSlice($start, total);
                } else {
                    // Opera and other standards browsers
                    chunk = $file.slice($start, total);
                }

                self.globals.is_uploading[file_type] = $.ajax({
                    type: "POST",
                    url: $form.attr('action'),
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('X-File-Id', $fileId);
                        xhr.setRequestHeader('X-File-Name', $file.name);
                        xhr.setRequestHeader('X-File-Size', $file.size);
                        xhr.setRequestHeader('X-File-Resume', 1);
                    },
                    contentType: "application/json",
                    data: chunk,
                    dataType: ' json',
                    processData: false,
                    success: function (response) {

                        var $newStartpos = 100 * total / $file.size;
                        progressBar.css('width', $newStartpos + '%')

                        if (!response.finish) {

                            if (!response.error) {
                                upload_chunk($file, $fileId, total)
                            } else {
                                self.globals.is_uploading['profile_pic'] = null;

                                error_bar.html(response.error)
                                    .parent()
                                    .addClass('text-danger');
                                $progressBarWrapper.addClass('stop').removeClass('uploading');
                            }
                        } else {
                            self.globals.is_uploading[file_type] = null;
                            if (!response.error && response.error != 0) {
                                error_bar.html(response.error)
                                    .parent()
                                    .removeClass('text-success')
                                    .addClass('text-danger');
                                $progressBarWrapper.addClass('stop').removeClass('uploading');

                            } else {

                                self.globals.is_uploading[file_type] = false;
                                $progressBarWrapper.removeClass('uploading').addClass('stop');
                                progressBar.css("width", '0%')

                                error_bar.html(headerVars.dictionary.photo_upload_success)
                                    .parent()
                                    .removeClass('text-danger')
                                    .addClass('text-success');

                                var template = '<div class="image-item wrapper public-pic pending"' +
                                    'cubix:photo-id="' + response.photo_id + '"' +
                                    'cubix:initial="' + response.args + '"' +
                                    'cubix:photo-hash="' + response.hash + '"' +
                                    'cubix:photo-ext="' + response.ext + '"' +
                                    '>' +
                                    '<img style="left: 50%; top: 50%; transform: translate(-50%, -50%)" class="image" src="' + response.photo_url + '">' +
                                    '<div class="drag-by-this"></div>' +
                                    '<div class="pending-info">' +
                                    '<i class="far fa-clock"></i>' +
                                    '<p class="uppercase weight">' + (headerVars.dictionary.pending || "") + '</p>' +
                                    '</div>' +
                                    '<div class="options-bar d-flex justify-content-between">' +
                                    '<div class="likes-container d-flex align-items-center">' +
                                    //'<div class="likes"><i class="fas fa-heart"></i></div>'+
                                    //'<div class="likes-value"></div>'+
                                    '</div>' +
                                    '<div class="settings-container d-flex align-items-center">' +
                                    '<div class="picture-status">' +
                                    '<span class="status-main">' + (headerVars.dictionary.main || "") + '</span>' +
                                    '<span class="status-private">' + (headerVars.dictionary.private || "") + '</span>' +
                                    '<span class="status-public">' + (headerVars.dictionary.public || "") + '</span>' +
                                    '<span class="status-disabled">' + (headerVars.dictionary.disabled || "") + '</span>' +
                                    '</div>' +
                                    '<div class="settings-trigger"><i class="fas fa-cog"></i></div>' +
                                    '<div class="settings-list" data-photo="' + response.photo_id + '">' +
                                    '<div class="setting set-main">' + (headerVars.dictionary.make_profile_picture || "") + '</div>' +
                                    '<div class="setting set-private">' + (headerVars.dictionary.make_private_picture || "") + '</div>' +
                                    '<div class="setting set-public">' + (headerVars.dictionary.make_public_picture || "") + '</div>' +
                                    '<div class="setting rotate" data-direction="left">' + (headerVars.dictionary.rotate_left || "") + '</div>' +
                                    '<div class="setting rotate" data-direction="right">' + (headerVars.dictionary.rotate_right || "") + '</div>' +
                                    '<div class="setting delete">' + (headerVars.dictionary.delete || "") + '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '<input type="checkbox" name="photo_id" data-approved = "<?=$photo->is_approved?>" value="' + response.photo_id + '" />' +
                                    '</div>';


                                $(template).prependTo('#gallery-images-list');

                                self.init_images_crop();

                            }
                        }
                    }
                });
            }

            upload_chunk($file, $fileId, 0);
        });

    }

    Gallery_page.prototype.init_video_upload = function () {

        var self = this,
            params = '',
            $form = $('.video-upload-form'),
            file_type = 'video';

        if ($('input[name="escort"]').length)
            params = '&escort_id=' + $('input[name="escort"]').val();

        $form.submit(function (e) {

            e.preventDefault();

            if (self.globals.is_uploading[file_type] != null) {
                return alert("File is uploading, please wait ...");
            }

            var $form = $(this);
            var progressBar = $('.video-progress-bar .progress-bar');
            var $progressBarWrapper = $('.video-progress-bar');
            var error_bar = $('.video-progress-bar .process-status .text');
            var total = 0;

            var formdata = new FormData(this),
                $file = formdata.get('video'),
                $fileId = (new Date()).getTime().toString(36);
			
            $progressBarWrapper.addClass('uploading').removeClass('stop');
            self.globals.is_uploading[file_type] = true;

            error_bar.html('');
            progressBar.css('width', 0 + '%')
            $('.video-progress-bar').show();

            var upload_chunk = function ($file, $fileId, $start) {
				
                // Force Stop Recursion
                if (self.globals.is_uploading[file_type] == 'aborted') {
                    return self.globals.is_uploading[file_type] = null;
                }

                var chunk,
                    $chunkSize = 524288, // 0.5 MB
                    total = $start + $chunkSize;

                if (total >= $file.size) {
                    total = $file.size
                }
				
                if ($file.mozSlice) {
                    // Mozilla based
                    chunk = $file.mozSlice($start, total);
                } else if ($file.webkitSlice) {
                    // Chrome, Safari, Konqueror and webkit based
                    chunk = $file.webkitSlice($start, total);
                } else {
                    // Opera and other standards browsers
                    chunk = $file.slice($start, total);
                }

				var fileName = $file.name.replace(/[^a-zA-Z0-9\.]/g, '');	
				console.log(fileName);
                self.globals.is_uploading[file_type] = $.ajax({
                    type: "POST",
                    url: $form.attr('action'),
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('X-File-Id', $fileId);
                        xhr.setRequestHeader('X-File-Name', fileName);
                        xhr.setRequestHeader('X-File-Size', $file.size);
                        xhr.setRequestHeader('X-File-Resume', 1);
                    },
                    contentType: "application/json",
                    data: chunk,
                    dataType: ' json',
                    processData: false,
                    success: function (response) {
						console.log(response);
                        var $newStartpos = 100 * total / $file.size;
                        progressBar.css('width', $newStartpos + '%')

                        if (!response.finish) {

                            if (!response.error) {

                                upload_chunk($file, $fileId, total)
                            } else {

                                self.globals.is_uploading[file_type] = null;

                                error_bar.html(response.error)
                                    .parent()
                                    .removeClass('text-success')
                                    .addClass('text-danger');
                                $progressBarWrapper.addClass('stop').removeClass('uploading');

                            }
                        } else {

                            self.globals.is_uploading[file_type] = null;

                            if (!response.error) {

                                self.globals.is_uploading['profile_pic'] = false;
                                $progressBarWrapper.removeClass('uploading').addClass('stop');
                                progressBar.css("width", '0%')

                                error_bar.html(headerVars.dictionary.video_upload_success)
                                    .parent()
                                    .removeClass('text-danger')
                                    .addClass('text-success');

                                $('.gallery-page .dropdown-item.attach-video').addClass('disabled')

                            } else {

                                error_bar.html(response.error)
                                    .parent()
                                    .removeClass('text-success')
                                    .addClass('text-danger');
                                $progressBarWrapper.addClass('stop').removeClass('uploading');
                            }
                        }
                    }
                });
            }

            upload_chunk($file, $fileId, 0);

        })


    }

    Gallery_page.prototype.init = function () {
        try {
            this.bind_events();
            this.init_video_upload();
            this.init_natural_photos();
            this.init_profile_photos();
            this.init_editor();
            if(!Sceon.isMSIE()) {
                this.init_images_crop();
                this.init_sortable_images();
            }else{
                alert("Your current browser doesnt support some graphical features for Images like drag and drop, crop and others ...");
            }



        } catch (e) {
            console.warn("Whoops there was an error", e);
        } finally {
            return this;
        }
    }

    return Gallery_page;

})();

$(function () {
    window.GALLERY_PAGE = (new Gallery_page()).init();

    $('.previous-step').click(function () {
        GALLERY_PAGE.globals.uploaded_files = [];
    });

    if (GALLERY_PAGE.globals.uploaded_files.length > 0)
        GALLERY_PAGE.globals.uploaded_files = [];
});