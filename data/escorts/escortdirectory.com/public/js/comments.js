var Comments = (function () {

    var escort_id = null;
    var pending_requests = {
        getting_cities: null
    };
    var templates = {
        loading: function () {
            return "Loading ...";
        },
        mobile_loading: function mobile_loading() {
            return "<li class='disabled'><a><label>Loading ...</label></a></li>";
        }
    };

    function init() {
        bind_events();
        autocomplete(document.getElementById('countryInput'));
    }

    function set_filter(filter) {
        set_hash(make_hash(filter));
    }

    function set_hash(hash) {
        if (hash.length) {
            history.replaceState(null, null, ' ');
            document.location.search = hash;
        } else {
            document.location.hash = '';
        }

        if(Sceon.isMobile()) {
            var country = $('#comments-country-search-mobile .dropdown-toggle-val').text();
            var city = $('#comments-city-search-mobile .dropdown-toggle-val').text();

            var country_id = $('#comments-country-search-mobile').find('input[checked]').val();
            var city_id = $('#comments-city-search-mobile').find('input[checked]').val();

            var locationStr = headerVars.dictionary.city_region_or_country;
            if(country_id) {
                locationStr = country;

                if(city_id) {
                    locationStr += ", " + city;
                }
            }
            $('#myCountry-mobile-place').text(locationStr);
        }
    }

    function make_hash(filter) {
        var hash = '';
        var sep = '&';
        var value_glue = ',';
        var map = {};

        var hidden_inputs = $('.searchbar input[type=hidden]');
        hidden_inputs.each(function (input) {
            if ($(this).attr('name') && $(this).attr('name')) {
                var index = $(this).attr('name');
                map[index] = $(this).val();
            }
        });

        var input_texts = $('.searchbar input[type=text]');
        input_texts.each(function (input) {
            if ($(this).val().length > 0 && $(this).attr('name')) {
                var index = $(this).attr('name');
                map[index] = $(this).val();
            }
        });

        var input_checkbox = $('.searchbar input[type=checkbox]:checked');
        input_checkbox.each(function (input) {
            if ($(this).val().length > 0 && $(this).attr('name')) {
                var index = $(this).attr('name');
                map[index] = $(this).val();
            }
        });

        var selects = $('.searchbar select');
        selects.each(function (input) {
            if ($(this).val() && $(this).attr('name')) {
                var index = $(this).attr('name');
                map[index] = $(this).val();
            }
        });

        if(Sceon.isMobile()) {
            var country_id = $('#comments-country-search-mobile').find('input[checked]').val();
            var city_id = $('#comments-city-search-mobile').find('input[checked]').val();

            map['country_id'] = country_id;
            map['city_id'] = city_id;
        }

        map = Object.assign({}, map, filter);

        for (var i in map) {
            if(i == 'page') {
                hash += i + '=' + map[i];
            } else {
                hash += i + '=' + map[i] + sep;
            }
        }

        return hash;
    }

    function parse_hash() {
        var hash = document.location.hash.substring(1),
            filter = '',
            params = hash.split('&'),
            not_array_params = ['page', 'showname', 'country_id', 'city_id'];

        if (!hash.length) {
            return {};
        }

        params.forEach(function (param) {
            var key_value = param.split('=');
            var key = key_value[0];
            var val = key_value[1];

            if (val === undefined) return;

            if (key == 'showname') {
                $('#showname').val(val);
            } else if (key == 'country_id') {
                $('#country').val(val);
            } else if (key == 'city_id') {
                $('#city').val(val);
            }

            val = val.split(',');

            val.forEach(function (it) {
                filter += key + ((!not_array_params.includes(key)) ? '[]=' : '=') + it + '&';
            });
        });

        return filter;
    }

    function load_comments(params) {

        var $container = $('#comments_container'),
            filters = parse_hash(),
            url = lang_id + '/comments/ajax-list';

        if ( filters.length ) {
            filters = Sceon.parseQueryString(filters);
        }

        $container.LoadingOverlay('show');

        $.ajax({
            url: url,
            type: 'get',
            data: Object.assign({}, {escort_id: escort_id}, filters || {}),
            success: function (resp) {
                $container.html(resp);
                $container.LoadingOverlay('hide');
                $('.to-the-moon').trigger('click');
            }
        });
    }

    function on_hash_change() {
        var data = parse_hash();
        load_comments(data);
    }

    function toggle_searchbar() {
        var $searchbar = $('.searchbar .searchbar-wrapper');
        $searchbar.toggleClass('show');

        if($searchbar.hasClass('show')) {
            $('#countryInput').focus()
        }

    }

    function hide_mobile_searchbar() {
        $('.searchbar .searchbar-wrapper').removeClass('show');
        $('#myCountry-mobile-place').html($('#countryInput').val());
    }

    function on_country_changed() {

        return new Promise(function (res, rej) {
            var $country_select = $("#comments-search-country"),
                $cities_select = $("#comments-search-city"),
                type = 'comments-city-search-select';

            if(!$country_select.val()) return rej();

            $cities_select.empty();
            $cities_select.siblings('ul[data-type="comments-city-search-select"]').empty().append(templates.loading());
            $cities_select.siblings('.dropdown-toggle').find('.dropdown-toggle-val').html(headerVars.dictionary.all_cities);

            $.ajax({
                url: '/geography/ajax-get-cities-comments?json=true',
                data: {
                    country_id: $country_select.val()
                },
                success: function (r) {
                    construct_dropdown({
                        regions: r,
                        select: $cities_select,
                        ul: $cities_select.siblings('ul[data-type="comments-city-search-select"]'),
                    }).then(function() {
                        res();
                    })
                }
            });

        });

    }

    function on_mobile_country_changed() {

        return new Promise(function(res, rej) {

            if (pending_requests.getting_cities) return console.log("Wait please");

            var $cities_dropdown = $("#comments-city-search-mobile"),
                country_id = $('#comments-country-search-mobile input:checked').val(),
                type = 'comments-city-search-mobile';

            if(!country_id) return rej();

            $cities_dropdown.find('ul').empty().append(templates.mobile_loading());
            $cities_dropdown.find('.dropdown-toggle-val').html(headerVars.dictionary.all_cities);

            pending_requests.getting_cities = $.ajax({
                url: '/geography/ajax-get-cities??json=true',
                data: {
                    country_id: country_id
                },
                success: function (r) {
                    pending_requests.getting_cities = null
                    construct_mobile_dropdown({
                        regions: r,
                        dropdown: $cities_dropdown,
                    }).then(function() {
                        res();
                    })
                }
            }).done(function () {
                pending_requests.getting_cities = null
            });

        });
    }

    function construct_dropdown(params) {
        return new Promise(function(res) {
            try {
                var cities = JSON.parse(params.regions);
                var visible_html = "<ul class=\"sub-dropdown-menu pl-1\" style=\"list-style: none\"> ";
                var data_html = "";

                cities.data.forEach(function(city) {
                    data_html += "<option value=\"".concat(city.id, "\">").concat(city.title, "</option>");
                    visible_html += "<li data-val=\"".concat(city.id, "\"><a href=\"javascript:void(0)\">").concat(city.title, "</a></li>");
                });
                visible_html += "</ul> ";

                params.select.html(data_html).val(null); // This is only <option> s
                params.ul.html(visible_html); // This one is for ul>li that is visible for user

                if(cities.data.length === 1) {
                    set_city_selected(cities.data[0].id);
                }

            } catch (e) {
                console.warn(e.message);
            }finally {
                res();
            }
        });

    }

    function construct_mobile_dropdown(params) {
        return new Promise(function(res) {
            try {
                var cities = JSON.parse(params.regions);
                var visible_html = "<ul class=\"sub-dropdown-menu pl-1\" style=\"list-style: none\"> ";

                cities.data.forEach(function(city) {
                    visible_html += `<li>
                    <label class="radio" title="${city.title}">
                        <input type="radio" name="country_id" value="${city.id}"> <a>${city.title} </a>
                    </label>
                </li>`;
                });
                visible_html += "</ul> ";

                params.dropdown.find('ul').html(visible_html); // This is only <option> s

                if(cities.data.length === 1) {
                    set_city_selected(cities.data[0].id);
                }
            } catch (e) {
                console.warn(e.message);
            } finally {
                res();
            }
        })

    }

    function on_location_changed() {
        var val = $(this).attr('data-val');
        $(this).parents('.dropdown').find('select').val(val).trigger('change');
    }

    function removeDropdownSelected(obj){
        var val = $(obj).parents(".dropdown").find(".dropdown-toggle-val").html();
        $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
            $(this).show();
        });
        $(obj).parents(".dropdown").find(".dropdown-menu").find("li").each(function(){
            if ($(this).find("a").html() == val) {
                $(this).hide();
            }
        });
    }

    function sorins_freacking_function () {
        var val = $(this).html(),
            value = $(this).closest('li').attr('data-val'),
            dataVal = $(this).closest('li').attr('value');

        $(this).parents('.dropdown-menu').find('input:checked').removeAttr('checked');
        $(this).closest('li').find('input').attr('checked', 'checked');

        var type = $(this).parents(".dropdown-menu").attr("data-type");
        $("#" + type).find(".dropdown-toggle-val").html(val);
        $("#" + type).find(".dropdown-toggle-val").attr('data-val', dataVal);
        $('#' + type).parent().find('select').val(value).trigger('change');

        removeDropdownSelected(this);
    }

    function bind_events() {
        if ("onhashchange" in window) {
            $(window).on('hashchange', on_hash_change)
        }else{
            alert("Your browser version is suspended, Update required!")
        }
        $('.searchbar-trigger, .dismiss-searchbar').on('click', toggle_searchbar);
        $('.search-button-trigger').on('click', hide_mobile_searchbar);
        $('.searchbar-wrapper').on('click', '.dropdown-menu li', on_location_changed);
        $('#comments-country, #comments-search-country').on('change', on_country_changed);
        $("body.comments").on("click", ".dropdown-menu li a", sorins_freacking_function);
        $("body.comments").on("click", "#comments-country-search-mobile li a", on_mobile_country_changed);
    }

    return {
        init: init,
        load_comments: load_comments,
        set_filter: set_filter
    }
})();


$(function(){
    Comments.init();
})
