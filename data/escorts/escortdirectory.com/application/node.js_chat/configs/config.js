function Config() {
	var config = {
		production : {
			common : {
				applicationId : 69,
				listenPort : 8888,
				host: 'dev.escortdirectory.com',
				authPath : '/get_user_info.php',
				sessionCacheTime: 1000 * 60 * 10, //10 minutes
				sessionCheckInterval : 1 * 60 * 60 * 1000, //1 hour
				imagesHost : 'http://pic.escortdirectory.com/escortdirectory.com/',
				avatarSize : 'lvthumb',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusLsdf8A9',
				blWordsLifeTime : 12 * 60 * 60 * 1000, //12 hours
				redisBlWordsDb: 69,
				redisBlWordsKey: 'bl-words-ed'
			},
			db : {
				user:     'chat_ed',
				database: 'escortdirectory_com_backend',
				password: 'kEHJnc74n91wEn34',
				host:     'server.escortdirectory.com'
			},
			redis: {
				host: 'localhost',
				port: 6379
			},
			memcache : {
				host : 'localhost',
				port : 11211
			}
		},
		development: {
			common : {
				applicationId : 69,
				listenPort : 8888,
				host: 'www.escortdirectory.com.dev',
				authPath: '/get_user_info.php',
				sessionCacheTime: 1000 * 60 * 10, //10 minutes
				sessionCheckInterval : 30 * 1000, //1 hour
				imagesHost : 'http://pic.escortdirectory.com/escortdirectory.dev/',
				avatarSize : 'lvthumb',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusLsdf8A9',
				blWordsLifeTime : 12 * 60 * 60 * 1000, //12 hours
				redisBlWordsDb: 0,
				redisBlWordsKey: 'bl-words-ef'
			},
			db : {
				user:     'root',
				database: 'escortdirectory_com_backend',
				password: '123456',
				host:     '127.0.0.1'
			},
			redis: {
				host: 'localhost',
				port: 6379
			},
			memcache : {
				host : 'localhost',
				port : 11211
			}
		}
	};
	
	this.get = function() {
		if (typeof type == 'undefined') {
			type = process.env.mode;
		}

		if ( type == 'development' ) {
			return config.development;
		} else {
			return config.production;
		}
	}
}

function get(type) {
	conf = new Config();
	return conf.get(type);
}



exports.get = get;
