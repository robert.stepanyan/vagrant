<?php

class Api_DictionaryController extends Zend_Controller_Action
{
    /**
     * @var Zend_Db_Adapter_Mysqli
     */
    protected $_db;

    public function init()
    {
        $this->_db = Zend_Registry::get('db');
        $this->_redis = Zend_Registry::get('redis');
        $this->view->layout()->disableLayout();
    }

    /**
     * Get escorts with xml rpc and insert all rows into the short table
     */
    public function indexAction()
    {
        $errors = array();

        $client = Cubix_Api_XmlRpc_Client::getInstance();

        $langs = $client->call('Application.getLangs');
        var_dump($langs);
        exit;

        $lang_ids = array();

        $this->_db->query('TRUNCATE langs');
        foreach ( $langs as $lang ) {
            $this->_db->insert('langs', $lang);
            $lang_ids[] = $lang['id'];
        }

        $this->_db->query('TRUNCATE application_langs');
        $app_id = Cubix_Application::getId();
        foreach ( $langs as $lang ) {
            $this->_db->insert('application_langs', array(
                'application_id' => $app_id,
                'lang_id' => $lang['id']
            ));
        }

        $dic = $client->call('Application.getDictionary', array($lang_ids));

        $this->_db->query('TRUNCATE dictionary');
        foreach ( $dic as $row ) {
            $this->_db->insert('dictionary', $row);
        }

        die;
    }

    protected function _call($method, $params = array())
    {
        return Cubix_Api::getInstance()->call($method, $params);

        $url = Cubix_Api_XmlRpc_Client::getServer() . '/?api_key=' . Cubix_Api_XmlRpc_Client::getApiKey();
        $url .= '&method=' . $method;

        foreach ( $params as $i => $param ) {
            $params[$i] = 'params[]=' . urlencode($param);
        }

        $url .= '&' . implode('&', $params);

        return unserialize(file_get_contents($url));
    }

    public function plainAction()
    {

        $errors = array();

        $client = new Cubix_Api_XmlRpc_Client();

        $datum = $status = array();
        try {
            // Transfer Langs Table
            /*$datum['langs'] = $this->_call('getLangs');
            $status[] = 'Transfered Data of `langs` Table';

            // Transfer Application Langs Table
            $datum['application_langs'] = $this->_call('getAppLangs');
            $status[] = 'Transfered Data of `application_langs` Table';*/

            // Transfer Dictionary
            $datum['dictionary'] = $this->_call('getDictionary');
            $status[] = 'Transfered Data of `dictionary` Table';
			
            foreach ( $datum as $table => $data ) {
                if ( $data ) {
                    $this->_db->query('TRUNCATE `' . $table . '`');
                    foreach ( $data as $id => $row ) {
                        $row = (array) $row;
                        $row['id'] = $id;
                        $row['application_id'] = Cubix_Application::getId();
                        $this->_db->insert($table, $row);
                    }

                    $status[] = 'Updated Data of `' . $table . '` Table (' . count($data) . ' rows in total)';
                }
            }
            // <--

            $status[] = 'Transfer Successfully Completed!';
        }
        catch ( Exception $e ) {
            $status[] = 'ERROR: Exception Cauth';
            $status[] = $e->__toString();
        }

        echo implode('<br/>' . "\r\n", $status);
        ob_flush();
        die;
    }

    public function seoPlainAction()
    {
        $seo_ents = $this->_call('getSeoEntities');

        if ( count($seo_ents) > 0 ) {
            $this->_db->query('TRUNCATE seo_entities');
            foreach ($seo_ents as $se)
            {
                $this->_db->insert('seo_entities', $se);
            }
        }

        $seo_ents_inst = $this->_call('getSeoEntitiesInstances');

        if ( count($seo_ents_inst) > 0 ) {
            $this->_db->query('TRUNCATE seo_entity_instances');
            foreach ($seo_ents_inst as $sei)
            {
                $this->_db->insert('seo_entity_instances', $sei);
            }
        }

        $seo_robots_google_id = $this->_call('getSR');

        $this->_db->update('applications', array('robots' => $seo_robots_google_id['body'], 'm_robots' => $seo_robots_google_id['m_body'], 'google_id' => $seo_robots_google_id['google_id']), $this->_db->quoteInto('id = ?', Cubix_Application::getId()));

        die('Done!');
    }

    public function emailTemplatesAction()
    {
        $client = Cubix_Api_XmlRpc_Client::getInstance();

        $ets = $client->call('Application.getEmailTemplates', array(Cubix_Application::getId()));


        $this->_db->query('TRUNCATE email_templates');
        /*$this->_db->query('DROP TABLE IF EXISTS `email_templates`;');
        $this->_db->query('
        CREATE TABLE `email_templates` (
        `id` varchar(255) NOT NULL,
        `application_id` int(10) unsigned NOT NULL,
        `from_addr` varchar(255) NOT NULL,
        `subject` varchar(255) DEFAULT NULL,
        `body_plain` text,
        `body` text,
        `lang_id` varchar(2) NOT NULL,
        PRIMARY KEY (`id`,`lang_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');*/

        foreach ( $ets as $et ) {
            $this->_db->insert('email_templates', $et);
        }

        die;
    }

    public function geographyAction()
    {
		
        $cities = array();
        $client = Cubix_Api_XmlRpc_Client::getInstance();
        foreach ( array('countries', 'regions', 'cities') as $section ) {
            $data = $client->call('Application.getGeography', array($section));
            if($section == 'cities')$cities = $data;
            $this->_db->query('TRUNCATE ' . $section);

            foreach ( $data as $row ) {
                if ( isset($row['ordering']) ) unset($row['ordering']);

                $this->_db->insert($section, $row);
				}
            }
			
        ignore_user_abort(true);
        ob_start();
        echo (json_encode(array('status' => 'success' )));

        // usually it needs to be closed in this case it load forever in our server
        // header('Connection', 'close');
        // header('Content-Length: '.ob_get_length());

        ob_end_flush();
        ob_flush();
        flush();

        session_write_close();
        fastcgi_finish_request();

        $status = $this->nearby_cities_update();
        var_dump($status);
        die();
    }

    public function nearby_cities_update( $cities = array() ){

        $client = Cubix_Api_XmlRpc_Client::getInstance();

        if( empty($cities) ){
            $cities = $client->call('Application.getCities');
        }

        if(is_resource($this->_redis->socket)){
            $this->__redis_flush_nearby_cities();
            $this->__insert_close_cities('redis', $cities);
        }

        $stat = $this->__insert_close_cities('mysql', $cities);

        return $stat;
    }

    public function __redis_flush_nearby_cities(){
        try {
            foreach($this->_redis->hKeys('nearby_cities') as $key){
                $this->_redis->hDel('nearby_cities', $key);
            }
            return true;
        } catch (Exception $e) {

        }
    }

    public function __insert_close_cities($store_location,  $cities = array() ){

        $densely_populated_countries_iso = array( 'usa', 'be', 'fr', 'nl', 'uk', 'es', 'it', 'at', 'gb' );
        $loop_count = 0;
        foreach ($cities as $city) {
            if( is_null($city['latitude']) || is_null($city['latitude']) ){
                continue;
            }
            ( in_array($city['iso'], $densely_populated_countries_iso) ? $distance = 100 : $distance = 200 );

            do {
                $nerby_cities = $this->_db->fetchAll("SELECT id, ((2 * 6371 *
						ATAN2(SQRT(
							POWER(SIN((RADIANS(".$city['latitude']." - latitude))/2), 2) +
							COS(RADIANS(latitude)) *
							COS(RADIANS(".$city['latitude'].")) *
							POWER(SIN((RADIANS(".$city['longitude']." - longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(".$city['latitude']." - latitude))/2), 2) +
							COS(RADIANS(latitude)) *
							COS(RADIANS(".$city['latitude'].")) *
							POWER(SIN((RADIANS(".$city['longitude']." - longitude))/2), 2)
						))
						)
					)) AS distance
				FROM cities 
				having distance < ". $distance);
                $distance += 10;
                $i++;
            } while ( count($nerby_cities) < 5 );

            $nearby_cities_string = '';

            if(is_array($nerby_cities)){
                foreach( $nerby_cities as $nerby_city ){
                    $nearby_cities_string .= $nerby_city->id . ',';
                }
                $nearby_cities_string = rtrim($nearby_cities_string, ',');
            }

            //inserting to redis or updating mysql
            if($store_location == 'redis'){
                $close_cities_list[$city['iso'].'_'.$city['title_geoip']] = rtrim($nearby_cities_string, ',');
                $this->_redis->hSet('nearby_cities', $city['iso'].'_'.$city['title_geoip'], $nearby_cities_string);
            }elseif($store_location == 'mysql'){
                $this->_db->update('cities', array('nearby_cities' => $nearby_cities_string), $this->_db->quoteInto('id = ?', $city['id']));
            }

            $loop_count++;
        }
        return $loop_count;
    }

    public function geographyPlainAction()
    {
		ini_set('memory_limit', '6000M');
		ini_set("pcre.recursion_limit", "8000");
		set_time_limit(0);
		$startT =  microtime(true);
        $datum = array();
        $cities = array();

        $client = new Cubix_Api_XmlRpc_Client();
        foreach ( array('countries', 'regions', 'cities', 'cityzones') as $section ) {
            //echo("Getting data for: " . $section) . "\r\n<br/>";
            $datum[$section] = $client->call('getGeography', array($section));
            if($section == 'cities')$cities = $datum[$section];
        }

        foreach ( $datum as $section => $data ) {
            if ( $data ) {
                $this->_db->query('TRUNCATE ' . $section);
				$bulk_object = new Cubix_BulkInserter($this->_db, $section);
				$c = 0;
				
                foreach ( $data as $row ) {
                    if ( isset($row['ordering']) ) unset($row['ordering']);
										
					$c++;
					if($c === 1){
						$bulk_object->addKeys(array_keys($row));
					}
				
					$bulk_object->addFields($row);				
                    //$this->_db->insert($section, $row);
                    //echo("Inserting " . $section . ": " . $row['title_en']) . "\r\n<br/>";
                }
				
				$bulk_object->insertBulk();
				//echo $section. " Bulk of ". $c . " rows inserted \r\n<br/>";
            }
        }
		
		ignore_user_abort(true);
		ob_start();
		echo (json_encode(array('status' => 'success' )));

        // usually it needs to be closed in this case it load forever in our server
         header('Connection', 'close');
         header('Content-Length: '.ob_get_length());

        ob_end_flush();
        ob_flush();
        flush();

        session_write_close();
        fastcgi_finish_request();

        $status = $this->nearby_cities_update();
		$endT =  microtime(true);
		echo "took ". ($endT - $startT) . "seconds\r\n";
        echo (json_encode(array('status' => 'success' )));
        die;
    }

    public function staticContentAction()
    {
        $this->_db->query('TRUNCATE static_content');

        foreach ( $this->_call('getStaticContent') as $page ) {
            $this->_db->insert('static_content', $page);
        }

        die;
    }

    public function glossaryAction()
    {
        $client = Cubix_Api_XmlRpc_Client::getInstance();

        $ets = $client->call('Application.geGlossary', array());

        $this->_db->query('TRUNCATE glossary');

        foreach ( $ets as $et ) {
            $this->_db->insert('glossary', $et);
        }

        die;
    }
}
