<?php


class Api_CitiesController extends Zend_Controller_Action
{
    /**
     * @var Zend_Db_Adapter_Mysqli
     */
    protected $_db;

    public function init()
    {
        $this->client = Cubix_Api_XmlRpc_Client::getInstance();
        $this->model = new Model_ixsBanners();
        $this->cli = new Cubix_Cli();

        $this->_db = Zend_Registry::get('db');
        $this->view->layout()->disableLayout();
    }

    /**
     * @return void;
     */
    public function makeDuplicateSlugsUniqueAction()
    {
        $this->cli->colorize('blue')->out('Fetching Cities with duplicate slug');
        $sql = "SELECT title_en, slug, count( slug ) AS c 
            FROM cities
            GROUP BY slug
            HAVING c > 1";

        $rows = $this->_db->fetchAll($sql);
        $this->cli->out('Found: ' . count($rows) . ' cities');

        foreach ($rows as $row) {

            $slugDuplicateCities =  $this->_db->fetchAll("SELECT * FROM cities WHERE slug = '{$row->slug}' ");
            foreach($slugDuplicateCities as $city) {
                $this->cli->colorize('green')->out('Working on city: ' . $city->title_en . '|' . $city->slug );

                $country = $this->_db->fetchRow("SELECT slug FROM countries WHERE id = {$city->country_id} ");
                $region = $this->_db->fetchRow("SELECT slug FROM regions WHERE id = {$city->region_id} ");
                $newSlug = [$city->slug];

                if(!empty($country) AND !empty($country->slug)) {
                    $newSlug[] = $country->slug;
                }

                if(!empty($region) AND !empty($region->slug)) {
                    $newSlug[] = $region->slug;
                }

                $newSlug = implode('-', $newSlug);
                $this->_db->query("UPDATE cities SET slug = '{$newSlug}' WHERE id = {$city->id}");
            }

        }
        $this->cli->colorize('blue')->out('Done :) ♥')->reset();
        exit;
    }

    
}
