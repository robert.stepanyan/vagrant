<?php

class Api_IxsController extends Zend_Controller_Action
{
    /**
     * @var Zend_Db_Adapter_Mysqli
     */
    protected $_db;

    /**
     * @var Cubix_Api_XmlRpc_Client
     */
    protected $client;

    /**
     * @var
     */
    protected $model;

    /**
     * @var Cubix_Cli
     */
    private $cli;

    public function init()
    {
        $this->client = Cubix_Api_XmlRpc_Client::getInstance();
        $this->model = new Model_ixsBanners();
        $this->cli = new Cubix_Cli();

        $this->_db = Zend_Registry::get('db');
        $this->view->layout()->disableLayout();
    }

    protected function syncWithBackend()
    {
        $this->cli->clear();
        $this->cli->colorize('yellow')->out('Fetching Banners data from backend. ')->reset();
        $rows = $this->client->call('Escorts.getBannersEd');
        $this->cli->colorize('green')->out(count($rows) . ' Banners Fetched.')->reset();

        $this->cli->colorize('purple')->out('Downloading images. ')->reset();
        $c = $this->model->copyImages($rows);
        $this->cli->colorize('green')->out($c . ' Images downloaded.')->reset();

        $this->model->fill($rows);

        return true;
    }

    public function ixsBannersAction()
    {
        $this->syncWithBackend();
        exit();
    }

}
