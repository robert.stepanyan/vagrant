<?php

/**
 * Class Api_AnalyticsController
 */
class Api_AnalyticsController extends Zend_Controller_Action
{
    /**
     * @var Zend_Db_Adapter_Mysqli
     */
    protected $_db;

    /**
     * @var Cubix_Api
     */
    protected $_client;

    public function init()
    {
        // Getting DB instance
        // ---------------------------
        $this->_db = Zend_Registry::get('db');

        // Prevent actions from trying to show any view
        // -------------------------------
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    /**
     * @var Cubix_Cli
     */
    private $_cli;

    /**
     * This action retrievers data from redis
     *  and stores them into DB (MySQL)
     */
    public function saveCachedViewsRedisClusterAction()
    {
        ini_set('memory_limit', '3024M');
        // Init cli
        // --------------------------------
        $cli = $this->_cli = new Cubix_Cli();
        $cli->clear();

        Cubix_Cli::setPidFile('/var/run/ed-save-cached-views-sync.pid');
        if (Cubix_Cli::isRunning()) {
            $cli->colorize('red')->out('Sync is already running, exitting...')->reset();
            exit(1);
        }

        $cli->out('Fetching cached views from Redis ...');

        // Getting the redis instance
        // -------------------------------
        $redis = Zend_Registry::get('redis');

        // Loop trough every type of visit action
        // -----------------------------------------
        $types = ['city-view', 'listing-view', 'profile-view'];
        $prefix = Model_Analytics::CACHE_PREFIX;

        foreach ($types as $typeIndex => $type) {

            // Getting specific keys from Redis
            // Parse the key, and fill array with escort's data
            // --------------------------
            $escorts = [];
            $keys = [];
            $pattern = $prefix . ':' . $type . ':*';
            $step = pow(10, 6);

            $cli->out('---------------------- Fetching pattern ' . $pattern . ' -------------------------');

            $masters = $redis->_masters();
            // Temporary!! Just Check in 1 master
            //if(count($masters) > 2) $masters = array_slice($masters, 0, 2);

            foreach ($masters as $master) {
                $cli->out('Starting to fetch on Replica: ' . $master[0] . ':' . $master[1]);

                $it = null;
                $chunk = 0;

                do {
                    $views = $redis->scan($it, $master, $pattern, $step) ?? [];

                    $chunk++;
                    $cli->out('Looping trough keys in replica ...' . number_format(@count($views)) . ' Keys Checked');

                    if (is_array($views) && !empty($views)) {
                        foreach ($views as $view) {
                            $keys[] = $view;
                            @list($_prefix, $action, $escortId, $city) = explode(':', $view);

                            $userIps = $redis->sMembers($view);

                            $escorts[$escortId] = isset($escorts[$escortId]) ? $escorts[$escortId] : [
                                'profileViewsCount' => 0,
                                'cityViewsCount'    => 0,
                            ];

                            $escorts[$escortId]['profileViewsCount'] = $escorts[$escortId]['profileViewsCount'] += count($userIps);

                            if($city) {
                                $escorts[$escortId]['cityViewsCount'] = $escorts[$escortId]['cityViewsCount'] += count($userIps);
                            }
                        }
                    }

                } while ($views !== false);

                $cli->out('Checked everything on Replica: ' . $master[0] . ':' . $master[1]);
            }
            // -----------------------------------------

            $cli->out('Starting to remove keys from redis | Keys count ' . count($keys));
            $redis->del($keys);

            // Generating table name with prefix, plurals and others
            // ----------------------------------
            $table = 'escort_' . str_replace('-', '_', $type) . 's_per_week';
            $date = date('Y-m-d');

            // Printing to console
            // ---------------------------
            $cli->out('Inserting into table ' . $table);
            $cli->out('Escorts: ' . count($escorts));


            $inserted = 0;
            $overall = count($escorts);
            $tablesOverall = count($types);

            $escortsToInsert = [];
            $escortsToUpdate = [];

            foreach ($escorts as $id => $escort) {
                $inserted++;
                $cli->out('Escorts NO: ' . $inserted . ' of ' . $overall . '| TABLE [' . ($typeIndex + 1) . '/' . $tablesOverall . ']');

                // In db we store amount of views for every day
                // so this is just retrieving escort's current visit data
                // ------------------------------------------------------------
                $todays_record = $this->_db->fetchRow("SELECT id FROM $table WHERE escort_id = ? and `date` = ?  LIMIT 1", [$id, $date]);

                $viewsField = $type == 'city-view' ? 'cityViewsCount' : 'profileViewsCount';

                // If cron didn't work today yet, insert new row
                // ---------------------------------
                if (empty($todays_record)) {

                    $escortsToInsert[$id] = [
                        'escortId'   => $id,
                        'viewsCount' => 1,
                        'date'       => $date,
                    ];

                } else {

                    // If cron already inserted today some date,
                    // we just increment amount of visits
                    // --------------------------------------
                    $escortsToUpdate[$todays_record->id] = [
                        'viewsCount' => $escort[$viewsField],
                        'date'       => $date,
                        'id'         => $todays_record->id
                    ];
                }

                unset($escorts[$id]);
            }

            // Bulk Insert
            if (!empty($escortsToInsert)) {
                $insertSql = "INSERT INTO $table (views_count, escort_id, `date`) VALUES ";
                foreach ($escortsToInsert as $row) {
                    $insertSql .= "({$row['viewsCount']}, {$row['escortId']}, '{$row['date']}'),";
                }

                $cli->colorize('green')->out('Inserted: ' . count($escortsToInsert));
                $this->_db->query(substr($insertSql, 0, -1));
            }

            // Bulk Update
            if (!empty($escortsToUpdate)) {
                $viewCountCases = [];
                $rowIds = [];
                foreach ($escortsToUpdate as $row) {
                    $viewCountCases [] = "id = {$row['id']} THEN views_count + {$row['viewsCount']}";
                    $rowIds[] = $row['id'];
                }
                $viewCountCases = implode(' WHEN ', $viewCountCases);
                $rowIds = implode(',', $rowIds);
                $updateSql = "UPDATE $table SET views_count = (CASE WHEN {$viewCountCases} END) WHERE id IN ($rowIds) ";

                $cli->colorize('green')->out('Updated: ' . count($escortsToUpdate));
                $this->_db->query($updateSql);
            }

            unset($keys);
        }


        $cli->out('Done ! :)');
        return;
    }

    //we have replaced redis cluster with redis, 
    //and this specific funciton is for that maybe we will revert back, so funciton saveCachedViewsAction I will keep //alive for now

    public function saveCachedViewsAction()
    {

        ini_set('memory_limit', '3024M');
      
        $cli = $this->_cli = new Cubix_Cli();
        $cli->clear();

        Cubix_Cli::setPidFile('/var/run/ed-save-cached-views-sync.pid');
        if (Cubix_Cli::isRunning()) {
            $cli->colorize('red')->out('Sync is already running, exitting...')->reset();
            exit(1);
        }

         $cli->out('Fetching cached views from Redis ...');

        $redis = Zend_Registry::get('redis');

        // Loop trough every type of visit action
        $types = ['city-view', 'listing-view', 'profile-view'];
        $prefix = Model_Analytics::CACHE_PREFIX;

        foreach ($types as $typeIndex => $type) {

            // Getting specific keys from Redis
            // Parse the key, and fill array with escort's data

            $escorts = [];
            $keys = [];
            $pattern = $prefix . ':' . $type . ':*';
            $step = pow(10, 6);

            $views = $redis->keys($pattern);
            
            if (is_array($views) && !empty($views)) {
                foreach ($views as $view) {    
                     $keys[] = $view;                 
                    @list($_prefix, $action, $escortId, $city) = explode(':', $view);
                    
                    $userIps = $redis->sMembers($view);

                    $escorts[$escortId] = isset($escorts[$escortId]) ? $escorts[$escortId] : [
                        'profileViewsCount' => 0,
                        'cityViewsCount'    => 0,
                    ];

                    $escorts[$escortId]['profileViewsCount'] = $escorts[$escortId]['profileViewsCount'] += count($userIps);

                    if($city) {
                        $escorts[$escortId]['cityViewsCount'] = $escorts[$escortId]['cityViewsCount'] += count($userIps);
                    }
                }
            }else{
                $cli->out('views not found');die;
            }

           

            $cli->out('Starting to remove keys from redis | Keys count ' . count($keys));
            $redis->del($keys);

            // Generating table name with prefix, plurals and others
            // ----------------------------------
            $table = 'escort_' . str_replace('-', '_', $type) . 's_per_week';
            $date = date('Y-m-d');

            // Printing to console
            // ---------------------------
             $cli->out('Inserting into table ' . $table);
             $cli->out('Escorts: ' . count($escorts));


            $inserted = 0;
            $overall = count($escorts);
            $tablesOverall = count($types);

            $escortsToInsert = [];
            $escortsToUpdate = [];

            foreach ($escorts as $id => $escort) {
                $inserted++;
                $cli->out('Escorts NO: ' . $inserted . ' of ' . $overall . '| TABLE [' . ($typeIndex + 1) . '/' . $tablesOverall . ']');

                // In db we store amount of views for every day
                // so this is just retrieving escort's current visit data
                // ------------------------------------------------------------
                $todays_record = $this->_db->fetchRow("SELECT id FROM $table WHERE escort_id = ? and `date` = ?  LIMIT 1", [$id, $date]);

                $viewsField = $type == 'city-view' ? 'cityViewsCount' : 'profileViewsCount';

                // If cron didn't work today yet, insert new row
                // ---------------------------------
                if (empty($todays_record)) {

                    $escortsToInsert[$id] = [
                        'escortId'   => $id,
                        'viewsCount' => 1,
                        'date'       => $date,
                    ];

                } else {

                    // If cron already inserted today some date,
                    // we just increment amount of visits
                    // --------------------------------------
                    $escortsToUpdate[$todays_record->id] = [
                        'viewsCount' => $escort[$viewsField],
                        'date'       => $date,
                        'id'         => $todays_record->id
                    ];
                }

                unset($escorts[$id]);
            }

            // Bulk Insert
            if (!empty($escortsToInsert)) {
                $insertSql = "INSERT INTO $table (views_count, escort_id, `date`) VALUES ";
                foreach ($escortsToInsert as $row) {
                    $insertSql .= "({$row['viewsCount']}, {$row['escortId']}, '{$row['date']}'),";
                }

                $cli->colorize('green')->out('Inserted: ' . count($escortsToInsert));
                $this->_db->query(substr($insertSql, 0, -1));
            }

            // Bulk Update
            if (!empty($escortsToUpdate)) {
                $viewCountCases = [];
                $rowIds = [];
                foreach ($escortsToUpdate as $row) {
                    $viewCountCases [] = "id = {$row['id']} THEN views_count + {$row['viewsCount']}";
                    $rowIds[] = $row['id'];
                }
                $viewCountCases = implode(' WHEN ', $viewCountCases);
                $rowIds = implode(',', $rowIds);
                $updateSql = "UPDATE $table SET views_count = (CASE WHEN {$viewCountCases} END) WHERE id IN ($rowIds) ";

                $cli->colorize('green')->out('Updated: ' . count($escortsToUpdate));
                $this->_db->query($updateSql);
            }

            unset($keys);
        }   


        $cli->out('Done ! :)');

        return;
    }

    /**
     * This action removes views that are older than 7 days
     */
    public function removeOldViewsAction()
    {

        // Init cli
        // --------------------------------
        $cli = $this->_cli = new Cubix_Cli();
        $cli->clear();

        // Printing out to the console
        // -------------------------
        $cli->out('Removing old views (older than 7 days)');

        // Loop trough every type of action
        // -------------------------------------------
        $types = ['profile-view', 'listing-view', 'city-view'];
        $prefix = Cubix_Application::getId();
        foreach ($types as $type) {

            // Generate table name, with prefix, plurals and others
            // -----------------------------------------------------
            $table = 'escort_' . str_replace('-', '_', $type) . 's_per_week';

            // Remove every row, that is older than 7 days
            // -------------------------------
            $this->_db->query("DELETE FROM $table WHERE `date` < NOW() - INTERVAL 1 WEEK  ");
        }

        $cli->out('Done !');

        return;
    }


}