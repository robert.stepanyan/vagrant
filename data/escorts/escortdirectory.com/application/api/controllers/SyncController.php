<?php

class Api_SyncController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;

	protected $_debug = true;

	protected $_log;

	/**
	 * @var Cubix_Api
	 */
	protected $_client;

	public function init()
	{
		$this->_db = Zend_Registry::get('db');
		$this->_client = Cubix_Api::getInstance();

		$this->view->layout()->disableLayout();
	}

	/**
	 * @var Cubix_Cli
	 */
	private $_cli;

    public function diffAction()
    {
        $cli = $this->_cli = new Cubix_Cli();
        $cli->clear();
        ini_set('memory_limit', '1024M');
        if ( Cubix_Cli::pidIsRunning('/var/run/ed-sync.pid') ) {
            $cli->colorize('red')->out('Big Sync is running, exitting...')->reset();
            exit(1);
        }

        // <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
        Cubix_Cli::setPidFile('/var/run/ed-diff-sync.pid');
        if ( Cubix_Cli::isRunning() ) {
            $cli->colorize('red')->out('Sync is already running, exitting...')->reset();
            exit(1);
        }
        $log = $this->_log = new Cubix_Cli_Log('/var/log/ed-diff-sync.log');
        // </editor-fold>


        $sync_start_date = time();
        $start = 0; $limit = 50;
        do {
            // <editor-fold defaultstate="collapsed" desc="Fetch Data from API">
            $cli->out('Fetching data from api (max 100)...');
            $data = $this->_client->call('sync.getDiffEscorts', array($start++ * $limit, $limit), 60);
            //var_dump($data);die;
            // </editor-fold>

            // IMPORTANT! This array will contain ids of escorts which were successuflly
            // removed or updated this array will be passed to api to mark those escorts
            // in journal as synchronized
            $success = array();

            // <editor-fold defaultstate="collapsed" desc="Process escorts which need to be removed">
            if ( count($data['removed']) > 0 ) {
                $cli->colorize('purple')
                    ->out('Need to remove ' . ($count = count($data['removed'])) . ' escorts...')
                    ->reset();
                foreach ( $data['removed'] as $i => $escort ) {
                    $percent = round((($i + 1) / $count) * 100) . '%';
                    // Shorthand variables
                    list($escort_id, $showname) = $escort;

                    Model_Escort_List::removeEscortFromElastic($escort_id); // EDIR-2591

                    try {
                        $this->_removeEscort($escort_id);
                        $success[] = $escort_id;
                        $result = true;
                    }
                    catch ( Exception $e ) {
                        $result = false;
                        // If error occured just log the error and display output in red color
                        $log->error("Unable to remove escort '$showname': " . $e->getMessage());
                    }

                    // Pretty output
                    $cli->out('[' . ($i + 1) . '/' . $count . ']', false)
                        ->colorize($result ? 'green' : 'red')
                        ->out(' Remove ' . $showname . '...', false)
                        ->column(60)
                        ->reset()
                        ->out($percent, true);

                }

                $cli->out();
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Process escorts which need to be updated">
            if ( count($data['updated']) > 0 ) {
                $cli->colorize('purple')
                    ->out('Need to update ' . ($count = count($data['updated'])) . ' escorts...')
                    ->reset();
                foreach ( $data['updated'] as $i => $escort ) {
                    $percent = round((($i + 1) / $count) * 100) . '%';
                    // Shorthand variables
                    $escort_id = $escort['profile']['id'];
                    $showname = $escort['profile']['showname'];

                    //$escort['profile']['is_main_premium_spot'] = 0;
                    //$escort['profile']['is_tour_premium'] = 0;

                    // <editor-fold defaultstate="collapsed" desc="Remove escort at first">
                    try {
                        $this->_removeEscort($escort_id);
                        $result = true;
                    }
                    catch ( Exception $e ) {
                        $result = false;
                        $log->error("Unable to remove escort '$showname': " . $e->getMessage());
                    }

                    $cli->out('[' . ($i + 1) . '/' . $count . ']', false)
                        ->colorize($result ? 'green' : 'red')
                        ->out(' Remove ' . $showname . '...', false)
                        ->column(60)
                        ->reset()
                        ->out($percent, true);
                    // </editor-fold>

                    // If there is error removing error just ignore this one
                    if ( ! $result ) continue;

                    try {
                        // Insert escort and all correspoinding data (services, cities, profiles, photos, etc.))

                        $this->_insertEscort($escort);
                        $success[] = $escort_id;

                        Model_Escort_List::fetchEscortAndStoreIntoCache([$escort_id]); // EDIR-2591

                        $result = true;
                    }
                    catch ( Exception $e ) {
                        $result = false;
                        // Just log into file if something happend and ignore
                        $log->error("Unable to insert escort '$showname': " . $e->getMessage());
                    }

                    // Pretty output
                    $cli->out('[' . ($i + 1) . '/' . $count . ']', false)
                        ->colorize($result ? 'green' : 'red')
                        ->out(' Insert ' . $showname . '...', false)
                        ->column(60)
                        ->reset()
                        ->out($percent, true);
                }

                $cli->out();
            }
            // </editor-fold>

            // If there were successfull escort syncs
            if ( count($success) > 0 ) {
                // <editor-fold defaultstate="collapsed" desc="Finally mark escorts as sync done to prevent continious syncing">
                // We don't want any extra data to be passed to API
                $success = array_unique($success);
                // Pass to API to mark these escorts as sync done
				
				
                $this->_client->call('sync.markEscortsDone', array($success, $sync_start_date));
				
                // </editor-fold>

                // And clear the cache
            }
            else {
                $cli->colorize('yellow')->out('Nothing happend :)')->reset();
            }
        } while ( count($data['removed']) > 0 || count($data['updated']) > 0 );


        $this->agencySplitAction();


        // At the end we need to repopulate cache table for optimized sqls
        // this table contains rows that describe in which city escort will be shown
        // and why is she there, because of upcoming, tour or just working city
        // also there are some reduntunt fields such as is_premium, is_agency,
        // gender and so on
        $cli->out()->out('Populating cache tables...', false);
        try {
            //$this->_populateCacheTables();
            $cli->column(60)->colorize('green')->out('success');

            $clear_cache = Model_Applications::clearCache();
            $cli->reset()->out($clear_cache);
        }
        catch ( Exception $e ) {
            $cli->column(60)->colorize('red')->out('error');
            $log->error($e->getMessage());
        }
        $cli->out();

        exit;
    }

    public function manualDiffAction()
    {


        ini_set('memory_limit', '1024M');

        // <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">

        $sync_start_date = time();
        $start = 0; $limit = 50;
        do {
            // <editor-fold defaultstate="collapsed" desc="Fetch Data from API">
            $data = $this->_client->call('sync.getDiffEscorts', array($start++ * $limit, $limit), 60);
            //var_dump($data);die;
            // </editor-fold>

            // IMPORTANT! This array will contain ids of escorts which were successuflly
            // removed or updated this array will be passed to api to mark those escorts
            // in journal as synchronized
            $success = array();

            // <editor-fold defaultstate="collapsed" desc="Process escorts which need to be removed">
            if ( count($data['removed']) > 0 ) {
                $count = count($data['removed']);

                foreach ( $data['removed'] as $i => $escort ) {
                    $percent = round((($i + 1) / $count) * 100) . '%';
                    // Shorthand variables
                    list($escort_id, $showname) = $escort;

                    Model_Escort_List::removeEscortFromElastic($escort_id); // EDIR-2591

                    try {
                        $this->_removeEscort($escort_id);
                        $success[] = $escort_id;
                        $result = true;
                    }
                    catch ( Exception $e ) {
                        $result = false;
                        // If error occured just log the error and display output in red color
                    }

                    // Pretty output
                }
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Process escorts which need to be updated">
            if ( count($data['updated']) > 0 ) {
                $count = count($data['updated']);
                foreach ( $data['updated'] as $i => $escort ) {
                    $percent = round((($i + 1) / $count) * 100) . '%';
                    // Shorthand variables
                    $escort_id = $escort['profile']['id'];
                    $showname = $escort['profile']['showname'];

                    //$escort['profile']['is_main_premium_spot'] = 0;
                    //$escort['profile']['is_tour_premium'] = 0;

                    // <editor-fold defaultstate="collapsed" desc="Remove escort at first">
                    try {
                        $this->_removeEscort($escort_id);
                        $result = true;
                    }
                    catch ( Exception $e ) {
                        $result = false;
                    }

                    // If there is error removing error just ignore this one
                    if ( ! $result ) continue;

                    try {
                        // Insert escort and all correspoinding data (services, cities, profiles, photos, etc.))

                        $this->_insertEscort($escort);
                        $success[] = $escort_id;

                        Model_Escort_List::fetchEscortAndStoreIntoCache([$escort_id]); // EDIR-2591

                        $result = true;
                    }
                    catch ( Exception $e ) {
                        $result = false;
                        // Just log into file if something happend and ignore
                    }

                    // Pretty output
                }

            }
            // </editor-fold>

            // If there were successfull escort syncs
            if ( count($success) > 0 ) {
                // <editor-fold defaultstate="collapsed" desc="Finally mark escorts as sync done to prevent continious syncing">
                // We don't want any extra data to be passed to API
                $success = array_unique($success);
                // Pass to API to mark these escorts as sync done


                $this->_client->call('sync.markEscortsDone', array($success, $sync_start_date));

                // </editor-fold>

                // And clear the cache
            }

        } while ( count($data['removed']) > 0 || count($data['updated']) > 0 );


        $this->agencySplitAction();


        // At the end we need to repopulate cache table for optimized sqls
        // this table contains rows that describe in which city escort will be shown
        // and why is she there, because of upcoming, tour or just working city
        // also there are some reduntunt fields such as is_premium, is_agency,
        // gender and so on
        try {
            //$this->_populateCacheTables();
            $clear_cache = Model_Applications::clearCache();
        }
        catch ( Exception $e ) {
            print_r($e);
        }

        exit;
    }

	private function _removeEscort($escort_id)
	{
		$this->_db->delete('escorts', $this->_db->quoteInto('id = ?', $escort_id));
		$this->_db->delete('escort_services', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_keywords', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_cities', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_cityzones', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_photos', $this->_db->quoteInto('escort_id = ?', $escort_id));

		// $this->_db->delete('escort_photo_votes', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_galleries', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('natural_pics', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('video', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('upcoming_tours', $this->_db->quoteInto('id = ?', $escort_id));
		$this->_db->delete('escort_profiles', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('premium_cities', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_working_times', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_filter_data', $this->_db->quoteInto('escort_id = ?', $escort_id));
		//$this->_db->delete('escort_travel_countries', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_travel_continents', $this->_db->quoteInto('escort_id = ?', $escort_id));
//		$this->_db->delete('escorts_in_cities', $this->_db->quoteInto('escort_id = ?', $escort_id));

		$this->_populateCacheTablesByEscortId($escort_id);
	}

    public function testAction() {
        $this->_populateCacheTablesByEscortId(93382);
        die("Ok");
	}

    private function _populateCacheTablesByEscortId($escort_id)
    {
        /*try {
            $this->_db->query('DELETE FROM escorts_in_countries WHERE escort_id = ?', $escort_id);

            $sql = '
                INSERT INTO escorts_in_countries (escort_id, country_id, gender, is_agency, is_tour, is_upcoming, is_premium)
                SELECT x.escort_id, x.country_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium
                FROM ((
                    SELECT
                        e.id AS escort_id, ct.country_id,
                        e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
                        0 AS is_premium
                    FROM cities AS ct
                    INNER JOIN escort_cities ec ON ec.city_id = ct.id
                    INNER JOIN escorts e ON e.id = ec.escort_id
                    WHERE FIND_IN_SET(1, e.products) > 0
                    GROUP BY ct.country_id
                ) UNION (
                    SELECT
                        e.id AS escort_id, ct.country_id,
                        e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
                        e.is_tour_premium AS is_premium
                    FROM upcoming_tours ut
                    INNER JOIN cities ct ON ct.id = ut.tour_city_id
                    INNER JOIN escorts e ON e.id = ut.id
                    WHERE  FIND_IN_SET(7, e.products) > 0 AND
                        DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY
                    GROUP BY ct.country_id
                ) UNION (
                    SELECT
                        e.id AS escort_id, ct.country_id,
                        e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
                        e.is_tour_premium AS is_premium
                    FROM escorts e
                    INNER JOIN cities ct ON ct.id = e.tour_city_id
                    WHERE
                        e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0
                    GROUP BY ct.country_id
                )) x WHERE x.escort_id = ?
            ';
            $this->_db->query($sql, $escort_id);
        }
        catch ( Exception $e ) {
            throw new Exception("Unable to repopulate cache table 'escorts_in_countries'\nError: {$e->getMessage()}");
        }*/

        try {
            $this->_db->query('DELETE FROM escorts_in_cities WHERE escort_id = ?', $escort_id);

            /**           !!! ATTENTION !!!
             * -------------------------------------------
             *    Note! Just figured out some magic, after spending some time to find it.
             *    Be careful whenever you make a change in this query!
             *    I also found it in @location change it also please.
             *        1) @location = @path + @method
             *        2) @path = "api/EscortsController.php"
             *        3) @method = "_postSync"
             * -------------------------------------------
             *    P.S.
             *    I hope GOD will punish the Dev who wrote duplicate of this query without adding some comment... -__-
             * -------------------------------------------
             */
            $sql = '
			INSERT INTO escorts_in_cities (escort_id, country_id, region_id, city_id, gender, is_agency, is_tour, is_upcoming, is_premium, 
				is_inactive, is_vip, type, is_new, is_base, ordering, participate_in_listing, should_display_agency_escort, longitude, latitude)
				SELECT x.escort_id, x.country_id, x.region_id, x.id AS city_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium, 
				x.is_inactive, x.is_vip, x.type, x.is_new, x.is_base, x.ordering, x.participate_in_listing, x.should_display_agency_escort, x.longitude, x.latitude
				FROM ((
					SELECT
						ct.id AS id, ct.country_id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
						@isPremium1 := EXISTS (SELECT 1 FROM premium_cities WHERE escort_id = e.id AND city_id = ct.id) AS is_premium, 
						e.is_inactive, @isVip1 := (e.is_vip AND @isPremium1) as is_vip, e.type, e.is_new,
						(e.city_id = ct.id) AS is_base,
						(
							(
								@isPremium1 = 0
								AND @isVip1 = 0
								AND (
									(
										e.package_id <> 103
										AND e.package_id <> 104
									)
									OR e.package_id IS NULL
								)
							)
							OR (
								@isPremium1 = 1
								AND @isVip1 = 0
								AND FIND_IN_SET(15, e.products) > 0
							)
							OR (
								@isVip1 = 1
								AND FIND_IN_SET(15, e.products) > 0
							)
					   ) AS participate_in_listing,
						(e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR @isPremium1 = 1)) AS should_display_agency_escort,
						 ct.longitude, ct.latitude
					FROM cities AS ct
					INNER JOIN escort_cities ec ON ec.city_id = ct.id
					INNER JOIN escorts e ON e.id = ec.escort_id
					WHERE FIND_IN_SET(1, e.products) > 0 AND e.id = ?
				) UNION (
					SELECT
						ut.tour_city_id AS id, ct.country_id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
						@isPremium2 := e.is_tour_premium AS is_premium, e.is_inactive, @isVip2 := (e.is_vip AND @isPremium2), e.type, e.is_new,
						FALSE AS is_base,
						(
							(
								@isPremium2 = 0
								AND @isVip2 = 0
								AND (
									(
										e.package_id <> 103
										AND e.package_id <> 104
									)
									OR e.package_id IS NULL
								)
							)
							OR (
								@isPremium2 = 1
								AND @isVip2 = 0
								AND FIND_IN_SET(15, e.products) > 0
							)
							OR (
								@isVip2 = 1
								AND FIND_IN_SET(15, e.products) > 0
							)
					   ) AS participate_in_listing,
						(e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR is_premium = 1)) AS should_display_agency_escort,
						ct.longitude, ct.latitude
					FROM upcoming_tours ut
					INNER JOIN cities ct ON ct.id = ut.tour_city_id
					INNER JOIN escorts e ON e.id = ut.id
					WHERE FIND_IN_SET(7, e.products) > 0 AND
						DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY AND e.id = ?
				) UNION (
					SELECT
						e.tour_city_id AS id, ct.country_id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
						@isPremium3 := e.is_tour_premium AS is_premium, e.is_inactive, @isVip3 := (e.is_vip AND @isPremium3), e.type, e.is_new,
						TRUE AS is_base,
						(
							(
								@isPremium3 = 0
								AND @isVip3 = 0
								AND (
									(
										e.package_id <> 103
										AND e.package_id <> 104
									)
									OR e.package_id IS NULL
								)
							)
							OR (
								@isPremium3 = 1
								AND @isVip3 = 0
								AND FIND_IN_SET(15, e.products) > 0
							)
							OR (
								@isVip3 = 1
								AND FIND_IN_SET(15, e.products) > 0
							)
					   ) AS participate_in_listing,
						(e.agency_id IS NULL OR (e.show_agency_escorts = 1 OR @isPremium3 = 1)) AS should_display_agency_escort,
						 ct.longitude, ct.latitude 
					FROM escorts e
					INNER JOIN cities ct ON ct.id = e.tour_city_id
					WHERE
						e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0 AND e.id = ?
				)) x
			';
            $this->_db->query($sql, array($escort_id,$escort_id,$escort_id));
        }
        catch ( Exception $e ) {
            throw new Exception("Unable to repopulate cache table 'escorts_in_cities'\nError: {$e->getMessage()}");
        }

        /*try {
            $this->_db->query('DELETE FROM escorts_in_cityzones WHERE escort_id = ?', $escort_id);

            $sql = '
                INSERT INTO escorts_in_cityzones (escort_id, city_id, cityzone_id, gender, is_agency, is_tour, is_upcoming, is_premium)
                SELECT x.escort_id, x.id AS city_id, x.cityzone_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium
                FROM ((
                    SELECT
                        cz.city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
                        e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
                        EXISTS (SELECT 1 FROM premium_cities WHERE escort_id = e.id AND city_id = cz.id) AS is_premium
                    FROM cityzones AS cz
                    INNER JOIN escort_cityzones ecz ON ecz.city_zone_id = cz.id
                    INNER JOIN escorts e ON e.id = ecz.escort_id
                    INNER JOIN cities ct ON ct.id = cz.city_id
                    WHERE  FIND_IN_SET(1, e.products) > 0
                ) UNION (
                    SELECT
                        ut.tour_city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
                        e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
                        e.is_tour_premium AS is_premium
                    FROM upcoming_tours ut
                    INNER JOIN cities ct ON ct.id = ut.tour_city_id
                    INNER JOIN escorts e ON e.id = ut.id
                    INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id
                    INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id AND cz.city_id = ct.id
                    WHERE  FIND_IN_SET(7, e.products) > 0 AND
                        DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY
                ) UNION (
                    SELECT
                        e.tour_city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
                        e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
                        e.is_tour_premium AS is_premium
                    FROM escorts e
                    INNER JOIN cities ct ON ct.id = e.tour_city_id
                    INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id
                    INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id AND cz.city_id = ct.id
                    WHERE
                        e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0
                )) x WHERE x.escort_id = ?
            ';
            $this->_db->query($sql, $escort_id);
        }
        catch ( Exception $e ) {
            throw new Exception("Unable to repopulate cache table 'escorts_in_cityzones'\nError: {$e->getMessage()}");
        }*/
    }

    private function _populateCacheTables()
    {
        try {
            $this->_db->query('TRUNCATE escorts_in_countries');
            $sql = '
				INSERT INTO escorts_in_countries (escort_id, country_id, gender, is_agency, is_tour, is_upcoming, is_premium)
				SELECT x.escort_id, x.country_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium
				FROM ((
					SELECT
						e.id AS escort_id, ct.country_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
						0 AS is_premium
					FROM cities AS ct
					INNER JOIN escort_cities ec ON ec.city_id = ct.id
					INNER JOIN escorts e ON e.id = ec.escort_id
					WHERE /*ct.country_id = 71 AND e.is_on_tour = 0 AND*/ FIND_IN_SET(1, e.products) > 0
					GROUP BY ct.country_id
				) UNION (
					SELECT
						e.id AS escort_id, ct.country_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM upcoming_tours ut
					INNER JOIN cities ct ON ct.id = ut.tour_city_id
					INNER JOIN escorts e ON e.id = ut.id
					WHERE /*ct.country_id = 71 AND e.is_on_tour = 0 AND*/ FIND_IN_SET(7, e.products) > 0 AND
						DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY
					GROUP BY ct.country_id
				) UNION (
					SELECT
						e.id AS escort_id, ct.country_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM escorts e
					INNER JOIN cities ct ON ct.id = e.tour_city_id
					WHERE
						e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0
					GROUP BY ct.country_id
				)) x
			';
            $this->_db->query($sql);
        }
        catch ( Exception $e ) {
            throw new Exception("Unable to repopulate cache table 'escorts_in_countries'\nError: {$e->getMessage()}");
        }

        try {
            $this->_db->query('TRUNCATE escorts_in_cities');
            $sql = '
			INSERT INTO escorts_in_cities (escort_id, country_id, region_id, city_id, gender, is_agency, is_tour, is_upcoming, is_premium, is_base, ordering)
				SELECT x.escort_id, x.country_id, x.region_id, x.id AS city_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium, x.is_base, x.ordering
				FROM ((
					SELECT
						ct.id AS id, ct.country_id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
						EXISTS (SELECT 1 FROM premium_cities WHERE escort_id = e.id AND city_id = ct.id) AS is_premium,
						(e.city_id = ct.id) AS is_base
					FROM cities AS ct
					INNER JOIN escort_cities ec ON ec.city_id = ct.id
					INNER JOIN escorts e ON e.id = ec.escort_id
					WHERE 1 /*AND e.is_on_tour = 0*/ AND FIND_IN_SET(1, e.products) > 0
				) UNION (
					SELECT
						ut.tour_city_id AS id, ct.country_id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
						e.is_tour_premium AS is_premium,
						FALSE AS is_base
					FROM upcoming_tours ut
					INNER JOIN cities ct ON ct.id = ut.tour_city_id
					INNER JOIN escorts e ON e.id = ut.id
					WHERE 1 /* e.is_on_tour = 0*/ AND FIND_IN_SET(7, e.products) > 0 AND
						DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY
				) UNION (
					SELECT
						e.tour_city_id AS id,  ct.country_id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
						e.is_tour_premium AS is_premium,
						TRUE AS is_base
					FROM escorts e
					INNER JOIN cities ct ON ct.id = e.tour_city_id
					WHERE
						e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0
				)) x
			';
            $this->_db->query($sql);

            /*$sql = '
                DELETE eic FROM escorts_in_cities AS eic
                WHERE ((SELECT e.is_on_tour FROM escorts e WHERE e.id = eic.escort_id) AND eic.is_tour = 0 AND eic.is_base = 0 AND eic.is_upcoming <> 1)
            ';
            $this->_db->query($sql);*/

            //file_put_contents('eic.log', date("d/m/Y H:i:s", time()) . ' - ' . $this->_db->fetchOne('SELECT COUNT(*) FROM escorts_in_cities') . PHP_EOL, FILE_APPEND);
        }
        catch ( Exception $e ) {
            throw new Exception("Unable to repopulate cache table 'escorts_in_cities'\nError: {$e->getMessage()}");
        }

        try {
            $this->_db->query('TRUNCATE escorts_in_cityzones');
            $sql = '
				INSERT INTO escorts_in_cityzones (escort_id, city_id, cityzone_id, gender, is_agency, is_tour, is_upcoming, is_premium)
				SELECT x.escort_id, x.id AS city_id, x.cityzone_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium
				FROM ((
					SELECT
						cz.city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
						EXISTS (SELECT 1 FROM premium_cities WHERE escort_id = e.id AND city_id = cz.id) AS is_premium
					FROM cityzones AS cz
					INNER JOIN escort_cityzones ecz ON ecz.city_zone_id = cz.id
					INNER JOIN escorts e ON e.id = ecz.escort_id
					INNER JOIN cities ct ON ct.id = cz.city_id
					WHERE /*ct.country_id = 71 AND e.is_on_tour = 0 AND*/ FIND_IN_SET(1, e.products) > 0
				) UNION (
					SELECT
						ut.tour_city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM upcoming_tours ut
					INNER JOIN cities ct ON ct.id = ut.tour_city_id
					INNER JOIN escorts e ON e.id = ut.id
					INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id
					INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id AND cz.city_id = ct.id
					WHERE /*ct.country_id = 71 AND e.is_on_tour = 0 AND*/ FIND_IN_SET(7, e.products) > 0 AND
						DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY
				) UNION (
					SELECT
						e.tour_city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM escorts e
					INNER JOIN cities ct ON ct.id = e.tour_city_id
					INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id
					INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id AND cz.city_id = ct.id
					WHERE
						e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0
				)) x
			';
            $this->_db->query($sql);
        }
        catch ( Exception $e ) {
            throw new Exception("Unable to repopulate cache table 'escorts_in_cityzones'\nError: {$e->getMessage()}");
        }


    }

    private function _insertEscort($escort)
    {
        // $cli = $this->_cli;
        $escort_id = $escort['profile']['id'];
        $showname = $escort['profile']['showname'];

        // Blacklisted countries
        $bl_countries_count = $this->_db->query('SELECT COUNT(id) AS count FROM countries')->fetch();
        $bl_countries_count = (object) $bl_countries_count;
        $bl_countries_fields = ceil($bl_countries_count->count / 63);

        $bl_countries_set_values = array();
        for ( $bli = 1; $bli <= $bl_countries_fields; $bli++ ) {
            $_set_values = $this->_db->fetchRow('DESCRIBE escorts blacklisted_countries_' . $bli);
            $set = substr($_set_values->Type,5,strlen($_set_values->Type)-7);
            $bl_countries_set_values[$bli] = preg_split("/','/", $set);
        }


        $blacklisted_countries = $escort['profile']['blacklisted_countries'];
        unset($escort['profile']['blacklisted_countries']);
        // Blacklisted countries


        if ( isset($escort['products']) ) {
            $escort['profile']['products'] = implode(',', $escort['products']);
        }
        // Insert main escort row
        $this->_db->insert('escorts', $escort['profile']);


        // Blacklisted countries
        $set_value_update = array();
        foreach ( $bl_countries_set_values as $field_num => $set_vals ) {
            foreach( $set_vals as $set_value ) {
                if ( in_array($set_value, $blacklisted_countries) ) {
                    $set_value_update[$field_num][] = $set_value;
                } else {
                    $set_value_update[$field_num][] = 9999;
                }
            }
        }


        if ( count($set_value_update) ) {
            foreach($set_value_update as $k => $vals) {
                $this->_db->update('escorts', array('blacklisted_countries_' . $k => join(',', $vals)), $this->_db->quoteInto('id = ?', $escort_id));
            }
        }
        // Blacklisted countries


        // Insert escort services
        foreach ( $escort['services'] as $service ) {
            $service['escort_id'] = $escort_id;
            try {
                if (!$this->_db->fetchOne('SELECT true FROM escort_services WHERE escort_id = '.$escort_id.' AND service_id = ' . $service['service_id']))
                {
                    $this->_db->insert('escort_services', $service);
                }
            }
            catch ( Exception $e ) {
                throw new Exception("Unable to insert service for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($service, true));
            }
        }

        // Insert escort keywords
        foreach ( $escort['keywords'] as $keyword ) {
            $keyword['escort_id'] = $escort_id;
            try {
                $this->_db->insert('escort_keywords', $keyword);
            }
            catch ( Exception $e ) {
                throw new Exception("Unable to insert keyword for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($keyword, true));
            }
        }

        // Insert escort working cities
        foreach ( $escort['cities'] as $city ) {
            $city = array('escort_id' => $escort_id, 'city_id' => $city);
            try {
                $this->_db->insert('escort_cities', $city);
            }
            catch ( Exception $e ) {
                throw new Exception("Unable to insert city for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($city, true));
            }
        }

        // Insert escort travel countries
        /*foreach ( $escort['travel_countries'] as $country ) {
            $country = array('escort_id' => $escort_id, 'country_id' => $country);
            try {
                $this->_db->insert('escort_travel_countries', $country);
            }
            catch ( Exception $e ) {
                throw new Exception("Unable to insert travel country for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($country, true));
            }
        }*/

        // Insert escort travel continents
        foreach ( $escort['travel_continents'] as $c ) {
            $c = array('escort_id' => $escort_id, 'continent_id' => $c);
            try {
                $this->_db->insert('escort_travel_continents', $c);
            }
            catch ( Exception $e ) {
                throw new Exception("Unable to insert travel continent for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($c, true));
            }
        }

        // Insert escort cityzones
        foreach ( $escort['cityzones'] as $cityzone ) {
            $cityzone = array('escort_id' => $escort_id, 'city_zone_id' => $cityzone);
            try {
                $this->_db->insert('escort_cityzones', $cityzone);
            }
            catch ( Exception $e ) {
                throw new Exception("Unable to insert cityzone for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($cityzone, true));
            }
        }

        // Insert escort photos
        foreach ( $escort['photos'] as $photo ) {
            $photo['escort_id'] = $escort_id;
            try {
                $this->_db->insert('escort_photos', $photo);
            }
            catch ( Exception $e ) {
                throw new Exception("Unable to insert photo for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($photo, true));
            }
        }

        // Insert escort photo votes
        // if ( count($escort['photo_votes']) ) {
        //     foreach ( $escort['photo_votes'] as $photo ) {
        //         $photo['escort_id'] = $escort_id;
        //         try {
        //             $this->_db->insert('escort_photo_votes', $photo);
        //         }
        //         catch ( Exception $e ) {
        //             throw new Exception("Unable to insert photo vote for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($photo, true));
        //         }
        //     }
        // }

        // Insert escort photo galleries
        if ( count($escort['escort_galleries']) ) {
            foreach ( $escort['escort_galleries'] as $gallery ) {
                try {
                    $this->_db->insert('escort_galleries', $gallery);
                }
                catch ( Exception $e ) {
                    throw new Exception("Unable to insert photo gallery for '$showname'\nError: {$e->getMessage()}\n" . var_export($gallery, true));
                }
            }
        }

        // Insert escort natural pics
        if ( count($escort['natural_pics']) ) {
            foreach ( $escort['natural_pics'] as $pic ) {
                try {
                    $this->_db->insert('natural_pics', $pic);
                }
                catch ( Exception $e ) {
                    throw new Exception("Unable to insert natural pic for '$showname'\nError: {$e->getMessage()}\n" . var_export($pic, true));
                }
            }
        }

        // Insert escort videos
        if (isset($escort['videos']) && !empty($escort['videos']))
        {
            foreach ( $escort['videos'] as $video ) {

                if(isset($video['escort_id']))
                {
                    $video_id = $video['id'];
                    try {
                        $this->_db->insert('video', $video);
                        $insert_id = $this->_db->lastInsertId();
                        foreach ($escort['videos']['image'] as $key => &$img)
                        {
                            if($img['video_id'] == $video_id)
                            {
                                $data = $img;
                                $data['video_id'] = $insert_id;
                                if($key == 2 ){
                                    $data['is_mid'] = 1;
                                }
                                else{
                                    $data['is_mid'] = 0;
                                }
                                $this->_db->insert('video_image', $data) ;
                            }
                        }
                    }
                    catch ( Exception $e ) {
                        throw new Exception("Unable to insert video for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($video, true));
                    }
                }
            }
        }

        // Insert escort upcoming tours
        foreach ( $escort['tours'] as $tour ) {
            $tour['id'] = $escort_id;
            try {
                $this->_db->insert('upcoming_tours', $tour);
            }
            catch ( Exception $e ) {
                throw new Exception("Unable to insert tour for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($tour, true));
            }
        }

        // Insert escort profile
        try {
            if ( isset($escort['snapshot']) ) {
                $snapshot = array('escort_id' => $escort_id, 'data' => serialize($escort['snapshot']));

                $filter_data = new Model_Api_EscortFilterData();
                $filter_row = $filter_data->getData((object)$escort['snapshot']);

                $this->_db->insert('escort_filter_data', $filter_row);
                $this->_db->insert('escort_profiles', $snapshot);
            }
        }
        catch ( Exception $e ) {
            throw new Exception("Unable to insert profile for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($snapshot, true));
        }


        // Finally if escort has premium cities insert them
        if ( isset($escort['premiums']) ) {
            foreach ( $escort['premiums'] as $city ) {
                $city = array('escort_id' => $escort_id, 'city_id' => $city);
                try {
                    $this->_db->insert('premium_cities', $city);
                }
                catch ( Exception $e ) {
                    throw new Exception("Unable to insert premium city for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($city, true));
                }
            }
        }

        foreach ( $escort['times'] as $time ) {
            $time['escort_id'] = $escort_id;
            try {
                $this->_db->insert('escort_working_times', $time);
            }
            catch ( Exception $e ) {
                throw new Exception("Unable to insert working time for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($time, true));
            }
        }

        $this->_populateCacheTablesByEscortId($escort_id);
    }

    public function reviewsAction()
    {
        ini_set('memory_limit', '1024M');
        ini_set("pcre.recursion_limit", "16777");
        $start_time = microtime(true);
        // <editor-fold defaultstate="collapsed" desc="Init CLI">
        $log = $this->_log = new Cubix_Cli_Log('/var/log/ed-reviews-sync.log');
        $cli = $this->_cli = new Cubix_Cli();
        $cli->clear();
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
        Cubix_Cli::setPidFile('/var/run/ed-reviews-sync.pid');
        if ( Cubix_Cli::isRunning() ) {
            $cli->colorize('red')->out('Sync is already running, exitting...')->reset();
            exit(1);
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Transfer Reviews">
        $chunk = 4000;
        $part = 0;
        $cli->out('Fetching reviews data from api (chunk size: ' . $chunk . ')...');
        $reviews = array();
        do {
            $cli->reset()->out('part #' . ($part + 1), false);
            $data = $this->_client->call('getEscortReviewsV2', array($part++ * $chunk, $chunk));
            if ( isset($data['error']) ) {
                $cli->column(60)->colorize('red')->out('error');
                $log->error('Error when fetching data from api: ' . $data['error']);
                continue;
            }

            $reviews = array_merge($reviews, $data['result']);
            $cli->column(60)->colorize('green')->out('success');
        } while ( count($data['result']) > 0 );
        $data = null; unset($data);
        $cli->reset()->out('Done fetching data (' . count($reviews) . ' reviews fetched).')->out();
        // </editor-fold>


        // <editor-fold defaultstate="collapsed" desc="Insert data into database">
        $cli->out('Truncate table `reviews`', false);
        try {
            $this->_db->query('TRUNCATE `reviews`');
            $cli->column(60)->colorize('green')->out('success');
        }
        catch ( Exception $e ) {
            $cli->column(60)->colorize('red')->out('error');
            $log->error('Error when truncating reviews table: ' . $e->getMessage());
            exit(1);
        }

        $cli->reset()->out('Inserting reviews...');
        $bulk_object = new Cubix_BulkInserter($this->_db, 'reviews');
        $c = 0;
        $row_limit = 5000;
        try {
            foreach ( $reviews as $review ) {
                $c++;
                if($c === 1){
                    $bulk_object->addKeys(array_keys($review));
                }

                $bulk_object->addFields($review);

                if( $c % $row_limit == 0){
                    $sql = $bulk_object->insertBulk($cli);
                    $cli->out("Bulk of ". $c . " rows inserted \n\r", false);
                }

            }

            if( $c % $row_limit !== 0 ){
                $bulk_object->insertBulk($cli);
                $cli->out("Bulk of ". $c . " rows inserted \n\r", false);
            }
        }
        catch ( Exception $e ) {
            $cli->colorize('red')->out('Could not insert reviews #----'. $e->getMessage() );
            $log->error('Error when inserting reviews # : ' . $e->getMessage());
        }
        $cli->out('Successfully inserted ' . $c . ' reviews.');
        // </editor-fold>
        $end_time = microtime(true);
        // Just print the execution time
        $cli->colorize('purple')->out('Execution took ' . round($end_time - $start_time, 4) . ' seconds')->reset()->out();
        exit(0);
    }

    public function commentsAction()
    {
        ini_set('memory_limit', '1024M');
        ini_set("pcre.recursion_limit", "16777");
        $start_time = microtime(true);
        // <editor-fold defaultstate="collapsed" desc="Init CLI">
        $log = $this->_log = new Cubix_Cli_Log('/var/log/ed-comments-sync.log');
        $cli = $this->_cli = new Cubix_Cli();
        $cli->clear();
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
        Cubix_Cli::setPidFile('/var/run/ed-comments-sync.pid');
        if ( Cubix_Cli::isRunning() ) {
            $cli->colorize('red')->out('Sync is already running, exitting...')->reset();
            exit(1);
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Transfer Comments">
        $chunk = 5000;
        $part = 0;
        $cli->out('Fetching comments data from api (chunk size: ' . $chunk . ')...');
        $comments = array();
        do {
            $cli->reset()->out('part #' . ($part + 1), false);
            $data = $this->_client->call('getCommentsV2', array($part++ * $chunk, $chunk));
            if ( isset($data['error']) ) {
                $cli->column(60)->colorize('red')->out('error');
                $log->error('Error when fetching data from api: ' . $data['error']);
                continue;
            }

            $comments = array_merge($comments, $data['result']);
            $cli->column(60)->colorize('green')->out('success');
        } while ( count($data['result']) > 0 );
        $data = null; unset($data);
        $cli->reset()->out('Done fetching data (' . count($comments) . ' comments fetched).')->out();
        // </editor-fold>


        // <editor-fold defaultstate="collapsed" desc="Insert data into database">
        $cli->out('Truncate table `comments`', false);
        try {
            $this->_db->query('TRUNCATE `comments`');
            $cli->column(60)->colorize('green')->out('success');
        }
        catch ( Exception $e ) {
            $cli->column(60)->colorize('red')->out('error');
            $log->error('Error when truncating comments table: ' . $e->getMessage());
            exit(1);
        }

        $cli->reset()->out('Inserting comments...');
        $c = 0;
        foreach ( $comments as $comment ) {
            try {
                if ( ! $comment['is_premium'] ) {
                    $comment['is_premium'] = 0;
                }
                if ( ! $comment['comments_count'] ) {
                    $comment['comments_count'] = 0;
                }
                $this->_db->insert('comments', $comment);
                $c++;
            }
            catch ( Exception $e ) {
                $cli->colorize('red')->out('Could not insert comment #' . $comment['id'] . '')->reset();
                $log->error('Error when inserting comment #' . $comment['id'] . ': ' . $e->getMessage());
            }
        }
        $cli->out('Successfully inserted ' . $c . ' comments.');
        // </editor-fold>

        if (Cubix_Application::getId() == APP_ED) {
            // <editor-fold defaultstate="collapsed" desc="Transfer Comments">
            $chunk = 5000;
            $part = 0;
            $cli->out('Fetching agency comments data from api (chunk size: ' . $chunk . ')...');
            $agency_comments = array();
            do {
                $cli->reset()->out('agnecy comments part #' . ($part + 1), false);
                $data = $this->_client->call('getAgencyComments', array($part++ * $chunk, $chunk));

                if ( isset($data['error']) ) {
                    $cli->column(60)->colorize('red')->out('error');
                    $log->error('Error when fetching data from api: ' . $data['error']);
                    continue;
                }

                $agency_comments = array_merge($agency_comments, $data['result']);
                $cli->column(60)->colorize('green')->out('success');
            } while ( count($data['result']) > 0 );
            $data = null; unset($data);
            $cli->reset()->out('Done fetching data (' . count($agency_comments) . ' comments fetched).')->out();
            // </editor-fold>


            // <editor-fold defaultstate="collapsed" desc="Insert data into database">
            $cli->out('Truncate table `agency_comments`', false);
            try {
                $this->_db->query('TRUNCATE `agency_comments`');
                $cli->column(60)->colorize('green')->out('success');
            }
            catch ( Exception $e ) {
                $cli->column(60)->colorize('red')->out('error');
                $log->error('Error when truncating comments table: ' . $e->getMessage());
                exit(1);
            }

            $cli->reset()->out('Inserting comments...');
            $cli->reset()->out(var_export($agency_comments));
            $c = 0;


            foreach ( $agency_comments as $agency_comment ) {
                try {
                    if ( ! $agency_comment['comments_count'] ) {
                        $agency_comment['comments_count'] = 0;
                    }
                    $this->_db->insert('agency_comments', $agency_comment);
                    $c++;
                }
                catch ( Exception $e ) {
                    $cli->colorize('red')->out('Could not insert agency comment #' . $agency_comment['id'] . '')->reset();
                    $cli->out($e);
                    $log->error('Error when inserting agency comment #' . $agency_comment['id'] . ': ' . $e->getMessage());
                }
            }
            $cli->out('Successfully inserted ' . $c . ' agency comments.');
            // </editor-fold>
        }

        $end_time = microtime(true);
        // Just print the execution time
        $cli->colorize('purple')->out('Execution took ' . round($end_time - $start_time, 4) . ' seconds')->reset()->out();
        exit(0);
    }

	public function chatOnlineCronAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/6a-chat-online-sync.log');
		// $cli = $this->_cli = new Cubix_Cli();
		// $cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/6a-chat-online-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			// $cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		$online_users = @file_get_contents('http://www.6annonce.com:35555/roomonlineusers.js?roomid=1');

		// Reset all online users
		$this->_db->update('escorts', array('is_online' => 0), $this->_db->quoteInto('is_online = ?', 1));

		if ( strlen($online_users) > 0 ) {
			$online_users = substr($online_users, 20, (strlen($online_users) - 21));

			$online_users = json_decode($online_users);

			$usernames = array();
			if ( count($online_users) > 0 ) {
				foreach ( $online_users as $user ) {
					$usernames[] = $user->name;
					echo "Escort {$user->name} is online now ! \n\r";
				}
			}
			
			$this->_db->query('UPDATE escorts SET is_online = 1 WHERE username IN (\'' . implode('\',\'', $usernames) . '\')');
		}

		die;
	}
	
	public function photoRotateAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/6a-photo-rotate-sync.log');
		// $cli = $this->_cli = new Cubix_Cli();
		// $cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/6a-photo-rotate-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			// $cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

	
		$chunk = 4000;
		$part = 0;
		// $cli->out('Fetching escorts data from database (chunk size: ' . $chunk . ')...');
		
		$photos_model = new Model_Escort_Photos();
		do {
			// $cli->reset()->out('part #' . ($part + 1), false);
			$escorts = $this->_db->fetchAll('SELECT id, showname from escorts LIMIT ' . $part++ * $chunk . ', ' . $chunk);
			foreach($escorts as $escort)
			{
				$photos = $photos_model->getRotatablePicsByEscortId($escort->id);
				
				if(!is_null($photos)){
					
					$next_main_image = 0;
					$rotate_done = 0;
					$i = 0;
					
					foreach($photos['results'] as $photo){
						$i++;
						if($i == 1){
							$first_photo_id = $photo->id;
						}
						if($next_main_image == 1){
							$photos_model->setMainImageFront($photo->id);
							$rotate_done = 1;
							BREAK;
						}
						if($photo->is_main == 1){
							$next_main_image = 1;
						}
						if($photos['count'] == $i && $rotate_done == 0 ){
							$photos_model->setMainImageFront($first_photo_id);
						}	
					}
				}
				
				
			}
			
		} while ($escorts);
		// $cli->column(60)->colorize('green')->out('success');
		// $cli->reset()->out('Rotating Main Images Done ')->out();
		unset($escorts);
		
		$clear_cache = Model_Applications::clearCache();
		// $cli->reset()->out($clear_cache); 
		
		exit(0);
	}

	/*public function agencySplitAction()
	{
		ini_set('memory_limit', '1024M');
		$client = new Cubix_Api_XmlRpc_Client();
		try{
			//unset agency escorts 
			$this->_db->query('UPDATE escorts SET show_agency_escorts = 0 WHERE agency_id IS NOT NULL');

			$m_agencies = new Model_Agencies();
			$agencies = $m_agencies->getAllIds();
			
			foreach($agencies as $agency){
				$rand_escort_ids =  $this->_db->fetchAll("SELECT id from escorts WHERE agency_id = ? ORDER BY RAND() LIMIT 3", array($agency->agency_id));
				if(count($rand_escort_ids) > 0 ) {
					foreach($rand_escort_ids as $escort){
						$this->_db->query('UPDATE escorts SET show_agency_escorts = 1 WHERE id = ?', array($escort->id));
					}
				}
			}
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
			exit(1);
		}

		$clear_cache = file_get_contents(Cubix_Application::getById(Cubix_Application::getId())->url . "/api/escorts/clear-cache");
		
		exit(1);
	}*/

	public function agencySplitAction()
	{
		ini_set('memory_limit', '1024M');
		$client = new Cubix_Api_XmlRpc_Client();

		$db = $this->_db;

		try{

			//unset agency escorts 
			$this->_db->query('UPDATE escorts SET show_agency_escorts = 0 WHERE agency_id IS NOT NULL');
			
			$agencies = $db->fetchAll('SELECT agency_id FROM club_directory WHERE escorts_count > 0');
			if ( count($agencies) ) {
				foreach( $agencies as $agency ) {
					$agency_cities_list = $db->fetchAll('
						SELECT e.id AS escort_id, e.city_id FROM escorts e 						
						WHERE e.agency_id = ?
						GROUP BY e.city_id
					', array($agency->agency_id));

					if ( count($agency_cities_list) ) {
						foreach ($agency_cities_list as $agency_city) {
							$rand_escort_ids =  $this->_db->fetchAll("SELECT id from escorts WHERE agency_id = ? AND city_id = ? ORDER BY RAND() LIMIT 3", array($agency->agency_id, $agency_city->city_id));
							
							

							if(count($rand_escort_ids) > 0 ) {
								foreach($rand_escort_ids as $escort){
									$this->_db->query('UPDATE escorts SET show_agency_escorts = 1 WHERE id = ?', array($escort->id));
								}
							}
						}
					}
				}
			}			

		}
		catch ( Exception $e ) {
			echo $e->getMessage();
			exit(1);
		}

		//$clear_cache = file_get_contents(Cubix_Application::getById(Cubix_Application::getId())->url . "/api/escorts/clear-cache");
		
		//exit(1);
	}

	public function fillExchangeRatesAction()
	{
		ini_set('memory_limit', '1024M');
		$db = $this->_db;

		$json_data = file_get_contents('https://openexchangerates.org/api/latest.json?app_id=03ed18dc394f4e6e93c978704d8544f0');
		$data = json_decode($json_data);

		if ( count($data->rates) ) {
			$db->query('TRUNCATE `exchange_rates`');

			foreach( $data->rates as $iso => $rate ) {
				$db->insert('exchange_rates', array('iso' => $iso, 'rate' => $rate));
			}
		}	

		//$clear_cache = file_get_contents(Cubix_Application::getById(Cubix_Application::getId())->url . "/api/escorts/clear-cache");
		exit(1);
	}

	public function updateEscortsRandomNumbersAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/update-random-escorts-' . Cubix_Application::getId() . '.log');
		// $cli = $this->_cli = new Cubix_Cli();
		// $cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/log/update-random-escorts-' . Cubix_Application::getId() . '.log');
		if ( Cubix_Cli::isRunning() ) {
			// $cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		$this->_db->query('UPDATE escorts SET ordering = RAND() * 100000');
		$this->_db->query('UPDATE escorts_in_cities SET ordering = RAND() * 100000');
		
		// $cli->out('Successfully updated escort random numbers.');		
		
		
		$clear_cache = Model_Applications::clearCache();
		// $cli->reset()->out($clear_cache); 
		
		exit(0);
	}

	public function watchedEscortsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$limit = 1000;
		// $client = Cubix_Api_XmlRpc_Client::getInstance();

		/* Delete 1 day old guest sessions */
		$this->_db->query('DELETE FROM watched_escorts WHERE DATE(date) < DATE(DATE_ADD(NOW(), INTERVAL -1 DAY)) AND user_id IS NULL');
		/**/

		while(1)
		{
			$result = $this->_db->fetchAll('SELECT * FROM watched_escorts WHERE is_sync = 0 AND user_id > 0 ORDER BY id ASC LIMIT ' . $limit);

			if ($result)
			{
				$last = end($result);
				$last_id = $last->id;

				$insert = 'INSERT INTO watched_escorts(`session_id`,`user_id`,`escort_id`,`date`,`is_sync`) VALUES ';

				foreach($result as $res)
				{
					$insert .= "('" . $res->session_id . "',";
					$insert .= $res->user_id . ",";
					$insert .= $res->escort_id . ",";
					$insert .= "'" . $res->date . "',";
					$insert .= "1),";
				}

				$insert = substr($insert, 0, strlen($insert) - 1);

				// $ret = $client->call('Members.syncWatchedEscorts', array($insert));

				if (is_null($ret))
				{
					$this->_db->query('UPDATE watched_escorts SET is_sync = 1 WHERE is_sync = 0 AND id <= ?', $last_id);
				}
				else
				{
					var_dump($ret);die;
				}
			}
			else
			{
				die('Done!!!');
			}
		}
	}
}