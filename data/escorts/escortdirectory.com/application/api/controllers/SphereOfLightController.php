<?php

/**
 * His Eduard Hovhannisyan's Majesty Requests and request
 * in the name of His Majesty all those whom is may concern
 * to allow the bearer to use freely without let or hindrance the code written below.
 *
 * NOTE:
 * If you add more functionality, please dont be lazy pussy-ass, add some comments
 *
 * Class SphereOfLightController
 * @author Eduard Hovhannisyan
 */
class Api_SphereOfLightController extends Zend_Controller_Action
{
    /**
     * @var Zend_Db_Adapter_Mysqli
     */
    protected $db;

    /**
     * @var Cubix_Api_XmlRpc_Client
     */
    protected $client;

    /**
     * @var Cubix_Cli
     */
    private $cli;

    /**
     * First step of actions lifecycle.
     * @return void
     */
    public function init(): void
    {
        $this->client = Cubix_Api_XmlRpc_Client::getInstance();
        $this->cli = new Cubix_Cli();

        $this->db = Zend_Registry::get('db');
        $this->view->layout()->disableLayout();
    }

    /**
     * Because sometimes big sync is not working,
     * some escorts will not be updated, and their "About me" text will
     * be empty in front listing. Call this every time you see missing text
     * on some escort.
     *
     * @return void
     */
    public function fixMissingBiographyTextsAction(): void
    {
        try {

            $this->cli->clear();
            $this->cli->colorize('yellow')->out('Fetching escorts with missing About me texts')->reset();

            $whereClause = "
                (CHAR_LENGTH(about_en) <= 3 || CHAR_LENGTH(about_en) IS NULL) AND 
                (CHAR_LENGTH(about_de) <= 3 || CHAR_LENGTH(about_de) IS NULL) AND 
                (CHAR_LENGTH(about_es) <= 3 || CHAR_LENGTH(about_es) IS NULL) AND 
                (CHAR_LENGTH(about_fr) <= 3 || CHAR_LENGTH(about_fr) IS NULL) AND 
                (CHAR_LENGTH(about_it) <= 3 || CHAR_LENGTH(about_it) IS NULL) AND 
                (CHAR_LENGTH(about_ro) <= 3 || CHAR_LENGTH(about_ro) IS NULL) 
            ";
            $escorts = $this->db->fetchAll("SELECT id FROM escorts WHERE $whereClause");

            $ids = [];
            foreach ($escorts as $escort) $ids[] = $escort->id;
            $rows = $this->client->call('SphereOfLight.getEscortsById', [$ids]);

            $index = $overall = count($rows);
            $updated = 0;
            $error = 0;

            $this->cli->colorize('green')->out('Found ' . $overall . ' escorts to update')->reset();

            while ($index--) {
                $row = $rows[$index];

                $fields = [];
                foreach (['en', 'de', 'es', 'fr', 'it', 'ro'] as $lng) {
                    if (!empty($row["about_$lng"])) {
                        $fields["about_$lng"] = $row["about_$lng"];
                    }
                }

                if (!empty($fields)) {
                    $this->db->update('escorts', $fields, [
                        $this->db->quoteInto('id = ?', $row['escort_id']),
                    ]);
                    $updated++;
                } else {
                    $error++;
                }

                $this->cli->colorize('green')->out("[$updated/$overall] Updated | Escorts Without About Me: $error")
                    ->reset();
            }

            $this->cli->colorize('green')->out("Done! Enjoy your life.")->reset();

        } catch (\Exception $exception) {
            $this->cli->colorize('red')->out($exception->getMessage())->reset();
        }

        exit;
    }

    /**
     * Whenever users complete their steps while moving forward through
     * the steps, sometimes the PARTIALLY_COMPLETE status (or mth like that)
     * is not being removed. And in that case, this methods can help.
     *
     * @return void
     */
    public function removeUselessPartiallyStatusAction(): void
    {
        try {

            $this->cli->clear();
            $this->cli->colorize('yellow')->out('Fetching escorts that have "PARTIALLY_COMPLETE" status')->reset();

            $rows = $this->client->call('SphereOfLight.getPartiallyCompleteEscorts');

            // Store escorts as map to access fast
            $escortsMap = [];
            foreach ($rows as $row) $escortsMap[$row['escort_id']] = $row;

            // Selecting escorts with their last step
            // to check if they really completed their profile
            $ids = [];
            foreach ($escortsMap as $row) $ids[] = $row['escort_id'];
            $escorts = $this->db->fetchAll("SELECT escort_id, last_step FROM profile_steps WHERE escort_id IN ( " . implode(',', $ids) . ") ");

            // Add last step to escorts map
            foreach ($escorts as $row) $escortsMap[$row->escort_id]['last_step'] = $row->last_step;

            $this->cli->colorize('green')->out('Found ' . count($escortsMap) . ' escorts.')->reset();

            $index = $overall = count($escortsMap);
            $updated = 0;
            $considered = 0;
            $skipped = 0;
            $profileStepNeeded = 2;

            foreach ($escortsMap as $current) {
                $considered++;

                if (empty($current['showname'])) {
                    $skipped++;
                    $this->cli->colorize('yellow')->out("Escort with ID " . $current['escort_id'] . " had no showname, skipping her ...")
                        ->reset();
                    continue;
                }

                if (empty($current['last_step'])) {
                    $skipped++;
                    $this->cli->colorize('red')->out("Escort with ID " . $current['escort_id'] . " had no steps saved, skipping her ...")
                        ->reset();
                    continue;
                }

                $this->cli->colorize('green')->out("Working on " . $current['showname'] . " her last step was " . $current['last_step'])
                    ->reset();

                // Escorts that have still not complete
                // information in their profiles
                if ($current['last_step'] <= $profileStepNeeded) {
                    $skipped++;
                    $this->cli->colorize('yellow')->out("Profile step was less than $profileStepNeeded, skipping her")
                        ->reset();
                    continue;
                }

                // Remove status from escort
                $this->client
                    ->call('Escorts.removeStatusBit', [
                        $current['escort_id'],
                        Cubix_EscortStatus::ESCORT_STATUS_PARTIALLY_COMPLETE
                    ]);
                $updated++;

                $left = $overall - $considered;
                $this->cli->colorize('green')->out("[$updated/$overall] Updated |Escorts Left: [$left]  | Escorts Skipped: $skipped")
                    ->reset();
            }

            $this->cli->colorize('green')->out("[$updated/$overall] Updated | Escorts Skipped: $skipped")
                ->reset();

            $this->cli->colorize('green')->out("Done! UPDATED: $updated |OVERALL: $overall |Skipped: $skipped --- Enjoy your life.")->reset();

        } catch (\Exception $exception) {
            $this->cli->colorize('red')->out($exception->getMessage())->reset();
        }

        exit;
    }
}

?>