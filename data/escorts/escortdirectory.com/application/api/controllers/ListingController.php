<?php

use Elasticsearch\ClientBuilder;

class Api_ListingController extends Zend_Controller_Action
{
    /**
     * @var Zend_Db_Adapter_Mysqli
     */
    protected $_db;
    protected $_debug = true;
    protected $_log;

    /**
     * @var Cubix_Api
     */
    protected $_client;

    /**
     * @var ClientBuilder
     */
    protected $elasticClient;

    public function init()
    {
        $this->_db = Zend_Registry::get('db');
        $this->_client = Cubix_Api::getInstance();

        $this->view->layout()->disableLayout();
        $this->elasticClient = Zend_Registry::get('elastic-client');
    }

    /**
     * @var Cubix_Cli
     */
    private $_cli;

    public function syncAction()
    {
        ini_set('memory_limit', '1024M');

        $errors = array();

        // <editor-fold defaultstate="collapsed" desc="Init CLI">
        $log = $this->_log = new Cubix_Cli_Log('/var/log/ed-listing-sync.log');
        //$log = $this->_log = new Cubix_Cli_Log('D:/em-diff-sync.log');
        $cli = $this->_cli = new Cubix_Cli();
        $cli->clear();
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
        Cubix_Cli::setPidFile('/var/run/ed-listing-sync.pid');
        if ( Cubix_Cli::isRunning() ) {
            $cli->colorize('red')->out('Sync is already running, exitting...')->reset();
            exit(1);
        }
        // </editor-fold>

        $cli->out('Fetching \'club_directory\' table dump from api...');
        $table_dump = $this->_client->call('listing.getTableDumpStandart', array());

        if ( $table_dump ) {

            try {
                $this->_db->query('TRUNCATE TABLE club_directory');
                $cli->out('Dropping \'club_directory\' table ...');
                $this->_db->query('DROP TABLE IF EXISTS club_directory');
            }
            catch ( Exception $e ) {
                $errors[] = array('title' => 'Could not drop club_directory table', 'exception' => $e);
            }

            try {
                $cli->out('Creating \'club_directory\' table ...');
                $this->_db->query($table_dump);
            }
            catch ( Exception $e ) {
                $errors[] = array('title' => 'Could not create club_directory table', 'exception' => $e);
            }
        }

        $this->_db->query('TRUNCATE TABLE club_directory');

        $start = 0; $limit = 100;
        do {
            $cli->colorize('green')->out('Fetching agencies data from api (max 100)...')->reset();
            $data = $this->_client->call('listing.getAgenciesStandart', array($start++ * $limit, $limit));

            $count = count($data);

            if ( $count ) {
                foreach ( $data as $i => $res ) {
                    $cli->out('[' . ($i+1) . '/' . $count . '] Inserting agency: ' . $res['club_name'])->reset();

                    $this->_db->insert('club_directory', $res);
                }
            }

        } while ( count($data) > 0 );

        exit;
    }

    /**
     * Stores escort profiles into ElasticSearch
     * @return void
     */
    public function cacheListingEscortsAction(): void
    {
        ini_set('memory_limit', '2024M');

        // Init Cli
        // ----------------------------
        $cli = $this->_cli = new Cubix_Cli();
        $cli->clear();
        // ----------------------------

        $cli->colorize('green');
        $cli->out('Fetching escorts to store into ElasticSearch');

        // Be careful if u change this SQL
        // Apply same changes in List.php inside fetchEscortAndStoreIntoCache also
        // --------------------------------------

        $sql = "SELECT
                    @has_premium_package :=
                    IF
                        (
                            FIND_IN_SET( 4, e.products ) > 0 
                            OR FIND_IN_SET( 5, e.products ) > 0 
                            OR FIND_IN_SET( 15, e.products ) > 0,
                            1,
                            0 
                        ) AS has_premium_package,
                    
                    @was_vip := (
                            SELECT
                                eic.is_vip 
                            FROM
                                escorts_in_cities eic
                            WHERE
                                eic.is_premium = 1 
                                AND eic.escort_id = e.id 
                            ORDER BY
                                eic.is_vip 
                                LIMIT 1 
                            ) AS is_vip,
                    IF ( @has_premium_package AND ( NOT @was_vip ), 1, 0 ) AS is_premium,
       
                    (
                        SELECT
                            CONCAT(eic.city_id, \"@\", CONCAT_WS(\";\", c.title_en, c.title_de, c.title_fr, c.title_es, c.title_it, c.title_ro))
                        FROM
                            escorts_in_cities eic
                        INNER JOIN cities c ON c.id = eic.city_id
                        WHERE
                            eic.is_base = 1 
                            AND eic.escort_id = e.id 
                        ORDER BY
                            eic.is_vip 
                            LIMIT 1 
                    ) AS base_city_concated,

                    e.id,
                    e.showname,
                    e.age,
                    UNIX_TIMESTAMP( e.date_registered ) AS date_registered,
                    e.user_id,
                    e.is_pornstar,
                    e.last_hand_verification_date,
                    e.is_inactive,
                    UNIX_TIMESTAMP( e.date_activated ) AS date_activated,
                    ct.slug AS city_slug,
                    ct.id AS city_id,
                    e.rates,
                    e.incall_type,
                    e.outcall_type,
                    e.is_new,
                    cr.id AS country_id,
                    cr.slug AS country_slug,
                    eic.gender,
                    eic.type,
                    GROUP_CONCAT( ct.title_en ) AS working_cities,
                    cr.title_en AS country,
                    ct.title_en AS city_title_en,
                    ct.title_de AS city_title_de,
                    ct.title_es AS city_title_es,
                    ct.title_ro AS city_title_ro,
                    ct.title_it AS city_title_it,
                    ct.title_fr AS city_title_fr,
                    cr.title_en AS country_title_en,
                    cr.title_de AS country_title_de,
                    cr.title_es AS country_title_es,
                    cr.title_ro AS country_title_ro,
                    cr.title_it AS country_title_it,
                    cr.title_fr AS country_title_fr,
                    ctt.title_en AS tour_city_en,
                    ctt.title_de AS tour_city_de,
                    ctt.title_es AS tour_city_es,
                    ctt.title_ro AS tour_city_ro,
                    ctt.title_it AS tour_city_it,
                    ctt.title_fr AS tour_city_fr,
                    e.about_en AS about_en,
                    e.about_de AS about_de,
                    e.about_es AS about_es,
                    e.about_ro AS about_ro,
                    e.about_it AS about_it,
                    e.about_fr AS about_fr,
                    e.verified_status,
                    e.photo_hash,
                    e.photo_ext,
                    e.photo_args,
                    e.photo_status,
                    e.incall_currency,
                    e.incall_price,
                    e.outcall_currency,
                    e.outcall_price,
                    e.comment_count,
                    e.review_count,
                    e.travel_place,
                    e.email,
                    e.website,
                    e.phone_instr,
                    e.phone_instr_no_withheld,
                    e.phone_instr_other,
                    e.phone_country_id,
                    e.disable_phone_prefix,
                    e.phone_number_free AS phone_number,
                    e.hit_count,
                    e.slogan,
                    e.incall_price,
                    e.date_last_modified,
                    e.hh_is_active,
                    e.is_on_tour AS is_tour,
                    e.tour_date_from,
                    e.tour_date_to,
                    e.user_id,
                    e.is_suspicious,
                    ct.id AS city_id,
                    e.agency_name,
                    e.agency_slug,
                    e.agency_id,
                    ulrt.refresh_date AS refresh_date,
                    eic.is_inactive
                FROM
                    escorts_in_cities eic
                    INNER JOIN escorts e ON e.id = eic.escort_id
                    INNER JOIN cities ct ON ct.id = eic.city_id
                    INNER JOIN countries cr ON cr.id = ct.country_id
                    LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
                    LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
                    LEFT JOIN currencies cur ON cur.id = e.incall_currency
                    LEFT JOIN exchange_rates er ON er.iso = cur.title 
                GROUP BY
                    eic.escort_id 
                ORDER BY e.id";
        // --------------------------------------

        $step = 1000;
        $pointer = 0;
        $done = 0;

        // If index exists lets clear it
        // --------------------------------------
        $params = ['index' => 'listing_escorts'];
        $exists = $this->elasticClient->indices()->exists($params);
        if ($exists) $this->elasticClient->indices()->delete($params);
        // --------------------------------------

        do {
            #if($pointer > 1) die;
            $skip = ($pointer++) * $step;
            $take = $step;

            $runnableSql = $sql . " LIMIT {$skip}, {$take}";
            $rows = $this->_db->fetchAll($runnableSql);

            $i = count($rows);
            while ($i--) {
                if (Model_Escort_List::storeEscortIntoCache($rows[$i])) {
                    $done++;
                    $cli->out($done . ' Escorts Stored Into ElasticSearch');
                }
            }

            if (count($rows) < $step) break;

        } while (count($rows) > 0);


        exit;
    }

    /**
     * Syncs search redirects from backend, only those who has redirect_url
     * @return void
     */
    public function setupSearchRedirectsAction(): void
    {
        ini_set('memory_limit', '2024M');

        // Init Cli
        // ----------------------------
        $cli = $this->_cli = new Cubix_Cli();
        $cli->clear();
        // ----------------------------

        $cli->colorize('yellow');
        $cli->out('Truncating table ');
        $this->_db->query('TRUNCATE TABLE search_redirects');
        $cli->colorize('green')->out(' Truncated ');

        $cli->colorize('yellow');
        $cli->out('Fetching redirects to store into Database');

        $chunk = 0;
        $done = 0;
        $sql = "INSERT INTO search_redirects (query, redirect_url) VALUES (?, ?) ";

        do {
            $redirects = $this->_client->call('listing.getSearchesWithRedirectUrl', array($chunk));

            foreach ($redirects as $redirect) {
                $this->_db->query($sql, [
                    'query' => $redirect['query'],
                    'redirect_url' => $redirect['redirect_url']
                ]);
                $done++;
                $cli->colorize('green')->out('Inserted '. $done . ' recored(s)');
            }

            $chunk++;
        } while (count($redirects) > 0);

        $cli->colorize('purple')->out('Done Enjoy! :)) ♥');
        exit;
    }
}