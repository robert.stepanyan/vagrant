<?php


class AdvancedSearchController extends Zend_Controller_Action
{

public static $linkHelper;
public static $mapper = NULL;
public function init()
{
    self::$linkHelper = $this->view->getHelper('GetLink');
}

private function setMapper(){

    $mapper = new Cubix_Filter_Mapper();

    $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'k', 'filter' => 'Keyword', 'field' => 'ek.keyword_id', 'filterField' => 'fd.keywords', 'queryJoiner' => 'OR', 'weight' => 10)) );
    $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'l', 'filter' => 'Language', 'field' => 'e.languages', 'filterField' => 'fd.language', 'queryJoiner' => 'OR', 'weight' => 20)) );
    $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'e', 'filter' => 'Ethnicity', 'field' => 'e.ethnicity', 'filterField' => 'fd.ethnicity', 'queryJoiner' => 'OR', 'weight' => 30)) );
    //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'n', 'filter' => 'Nationality', 'field' => 'e.nationality_id', 'filterField' => 'fd.nationality', 'queryJoiner' => 'OR', 'weight' => 40)) );
    $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'hc', 'filter' => 'HairColor', 'field' => 'e.hair_color', 'filterField' => 'fd.hair_color', 'queryJoiner' => 'OR', 'weight' => 50)) );
    //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'hl', 'filter' => 'HairLength', 'field' => 'e.hair_length', 'filterField' => 'fd.hair_length', 'queryJoiner' => 'OR', 'weight' => 60)) );
    $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'cs', 'filter' => 'CupSize', 'field' => 'e.cup_size', 'filterField' => 'fd.cup_size', 'queryJoiner' => 'OR', 'weight' => 70)) );
    $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'ec', 'filter' => 'EyeColor', 'field' => 'e.eye_color', 'filterField' => 'fd.eye_color', 'queryJoiner' => 'OR', 'weight' => 80)) );
    $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'ph', 'filter' => 'PubicHair', 'field' => 'e.pubic_hair', 'filterField' => 'fd.pubic_hair', 'queryJoiner' => 'OR', 'weight' => 90)) );
    //tatto
    //piercing
    $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'sf', 'filter' => 'ServiceFor', 'field' => 'e.sex_availability', 'filterField' => 'fd.service_for', 'queryJoiner' => 'OR', 'weight' => 120)) );
    $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'se', 'filter' => 'Service', 'field' => 'es.service_id', 'filterField' => 'fd.services', 'queryJoiner' => 'OR', 'weight' => 130)) );

    //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'ihr', 'filter' => 'IncallHotel', 'field' => 'e.incall_hotel_room', 'filterField' => 'fd.available_for_incall_hotel', 'queryJoiner' => 'OR', 'weight' => 140)) );
    $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'in', 'filter' => 'Incall', 'field' => 'e.incall_type', 'filterField' => 'fd.available_for_i', 'queryJoiner' => 'OR', 'weight' => 150)) );
    $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'out', 'filter' => 'Outcall', 'field' => 'e.outcall_type', 'filterField' => 'fd.available_for_o', 'queryJoiner' => 'OR', 'weight' => 160)) );
    $mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'tp', 'filter' => 'Travel', 'field' => 'e.travel_place', 'filterField' => 'fd.available_for_travel', 'queryJoiner' => 'OR', 'weight' => 170)) );

    //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'a', 'filter' => 'Age', 'field' => 'e.age', 'filterField' => 'fd.age', 'weight' => 50)) );
    //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'h', 'filter' => 'Height', 'field' => 'e.height', 'filterField' => 'fd.height', 'queryJoiner' => 'OR', 'weight' => 110)) );
    //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'w', 'filter' => 'Weight', 'field' => 'e.weight', 'filterField' => 'fd.weight', 'queryJoiner' => 'OR', 'weight' => 100)) );
    //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'sm', 'filter' => 'Smoker', 'field' => 'e.is_smoking', 'filterField' => 'fd.smoker', 'queryJoiner' => 'OR', 'weight' => 120)) );
    //$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'v', 'filter' => 'Verified', 'field' => 'e.verified_status', 'filterField' => 'fd.verified', 'queryJoiner' => 'AND', 'weight' => 140)) );

    self::$mapper = $mapper;

}

public function indexAction()
{


    //echo $route_name = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
    if($this->_request->pushstate_filters){
        Model_Escort_List::convertPushState($this->_request->pushstate_filters);
    }

    // <editor-fold defaultstate="collapsed" desc="If it is ajax request, disable the layout and render only list.phtml">
    if ( ! is_null($this->_getParam('ajax')) || $this->_request->isPost() ) {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setScriptAction('list');
        $this->view->is_ajax = true;
    } else {
        setcookie("show_all_escorts", 0, 0, "/", "www." . preg_replace('#www.|test.#', '', $_SERVER['HTTP_HOST']));
        $_COOKIE['show_all_escorts'] = 0;
    }
    // </editor-fold>

    // SEARCH PARAMS
    //$s_showname = $this->view->s_showname = $this->_request->showname;
    $alert_id = (int)$this->_getParam('alert_id');
    if($alert_id && $alert_id > 0){
        $cur_user = Model_Users::getCurrent();
        if(!$cur_user || $cur_user->user_type != 'member'){
            $this->_redirect($this->view->getLink('signin'));
            exit();
        }
    }

    //autocomplate fields
    $ts_type = $this->view->ts_type = $this->_getParam('ts_type');
    $ts_slug = $this->view->ts_slug = $this->_getParam('ts_slug');

    $show_all_hidden_escorts = $this->view->show_all_hidden_escorts = (int) $this->_getParam('sahe', 0);

    $country = $this->view->country = (int)$this->_getParam('country_id');
    $region = $this->view->region = (int)$this->_getParam('region_id');
    $zone = $this->view->city = (int)$this->_getParam('zone_id');
    $city = $this->view->city = (int)$this->_getParam('city_id');

    $s_name = $this->view->s_name = htmlspecialchars($this->_getParam('name'));
    //$s_agency = $this->view->s_agency = $this->_request->agency;
    $s_agency_slug = $this->view->s_agency_slug = $this->_getParam('agency_slug');
    $s_gender = $this->view->s_gender = $this->_getParam('gender', array());
    //$s_city_slug = $this->view->s_city_slug = $this->_request->city_slug;
    $s_phone = $this->view->s_phone = htmlspecialchars($this->_getParam('phone', null));
    $s_age_from = $this->view->s_age_from = (int)$this->_getParam('age_from');
    $s_age_to = $this->view->s_age_to = (int)$this->_getParam('age_to');
    $s_orientation = $this->view->s_orientation = (array) $this->_getParam('orientation');
    $s_keywords = $this->view->s_keywords = (array) $this->_getParam('k');
    
    // $s_height_from = $this->view->s_height_from = (int)$this->_getParam('height_from');
    // $s_height_to = $this->view->s_height_to = (int)$this->_getParam('height_to');
    // $s_weight_from = $this->view->s_weight_from = (int)$this->_getParam('weight_from');
    // $s_weight_to = $this->view->s_weight_to = (int)$this->_getParam('weight_to');
    
    $s_tatoo = $this->view->s_tatoo = (int)$this->_getParam('tatoo');
    $s_piercing = $this->view->s_piercing = (int)$this->_getParam('piercing');
    

     $s_height_from = $this->view->s_height_from = (int)explode('-', $this->_getParam('height_range'))[0];
     $s_height_to = $this->view->s_height_to = (int)explode('-', $this->_getParam('height_range'))[1];
     $s_weight_from = $this->view->s_weight_from = (int)explode('-', $this->_getParam('weight_range'))[0];
     $s_weight_to = $this->view->s_weight_to = (int)explode('-', $this->_getParam('weight_range'))[1];


    $s_ethnicity = $this->view->s_ethnicity = (array) $this->_getParam('e');
    $s_language = $this->view->s_language = (array) $this->_getParam('l');
    $s_nationality = $this->view->s_nationality = (array) $this->_getParam('n');
    $s_hair_color = $this->view->s_hair_color = (array)$this->_getParam('hc');
    $s_hair_length = $this->view->s_hair_length = (array)$this->_getParam('hl');
    $s_cup_size = $this->view->s_cup_size = (array) $this->_getParam('cs');
    $s_eye_color = $this->view->s_eye_color = (array)$this->_getParam('ec');
    $s_pubic_hair = $this->view->s_pubic_hair = (array) $this->_getParam('ph');

    $s_currency = $this->view->s_currency = htmlspecialchars($this->_getParam('s_currency', (!empty($_COOKIE['currency']) ? $_COOKIE['currency'] : Model_Countries::getGeoCurrency())));
    $s_price_from = $this->view->s_price_from = (int)$this->_getParam('price_from');
    $s_price_to = $this->view->s_price_to = (int)$this->_getParam('price_to');
    $s_incall_outcall = $this->view->s_incall_outcall = (int)$this->_getParam('incall_outcall');
    $s_sex_availability = $this->view->s_sex_availability = (array) $this->_getParam('sf');
    $s_service = $this->view->s_service = (array) $this->_getParam('se');

    //$s_incall_other = $this->view->s_incall_other = (int)$this->_request->in;
    //$s_outcall_other = $this->view->s_outcall_other = (int)$this->_request->out;
    $s_incall = $this->view->s_incall = (array)$this->_getParam('in');
    $s_incall_hotel_room = $this->view->s_incall_hotel_room = (array) $this->_getParam('ihr');
    $s_outcall = $this->view->s_outcall = (array)$this->_getParam('out');
    $s_travel_place = $this->view->s_travel_place = (array) $this->_getParam('tp');

    $s_real_pics = $this->view->s_real_pics = (int)$this->_getParam('s_real_pics');
    $s_verified_contact = $this->view->s_verified_contact = (int)$this->_getParam('s_verified_contact');
    $s_with_video = $this->view->s_with_video = (int)$this->_getParam('s_with_video');
    $s_pornstar = $this->view->s_pornstar = (int)$this->_getParam('s_pornstar');
    $s_available_for_travel = $this->view->s_available_for_travel = (int)$this->_getParam('s_available_for_travel');
    $s_is_online_now = $this->view->s_is_online_now = (int)$this->_getParam('s_is_online_now');
    $s_natural_photo = $this->view->s_natural_photo = (int)$this->_getParam('s_natural_photo');
    $this->view->sort = ($this->_getParam('sort')) ? $this->_getParam('sort') : $_COOKIE['sorting'];

    $this->view->filterBoxes = [
        ['name' => 's_real_pics', 'title' => 'quick_search_real_pics', 'isVisible' => 1],
        ['name' => 's_verified_contact', 'title' => 'quick_search_verified_contact', 'isVisible' => 1],
        ['name' => 's_with_video', 'title' => 'quick_search_video', 'isVisible' => 1],
        ['name' => 's_is_online_now', 'title' => 'quick_search_online', 'isVisible' => 1],
        ['name' => 's_available_for_travel', 'title' => 'available_for_travel', 'isVisible' => 1]
    ];

    $this->view->p_id = $this->_request->getParam('p_id');
    $this->view->page = $this->_request->getParam('page');
    $this->view->user_cleared_location_searchbar = $user_cleared_location_searchbar = $this->_request->getParam('is_location_cleared', 0);
    $this->view->hideOrdering = false;

    $p_top_category = $this->view->p_top_category = htmlspecialchars($this->_getParam('p_top_category', 'escorts'));
    $p_country_slug = $this->view->p_country_slug = htmlspecialchars($this->_getParam('p_country_slug'));
    $p_country_id = $this->view->p_country_id = (int) $this->_getParam('p_country_id');
    $escortType = $this->view->escortType = $this->_getParam('escortType', 'escort');
    $default_sorting = "close-to-me";
    $current_sorting = $this->_getParam('sort', $default_sorting);

    $geoData = Cubix_Geoip::getClientLocation();

    if ($_GET['loc'] === 'ro')
    {
        $geoData =   array(
            "ip"=> "92.86.88.223",
            "country" => "Romania",
            "country_iso" => "ro",
            "region" => "Bucuresti",
            "city" => "Bucharest",
            "latitude" => "44.432250976562",
            "longitude" => "26.106260299683"
        );
    }

    if($_GET['geo_check'] == 1) {
        var_dump($geoData);die;
    }
    $this->view->is_search = 0;
    if(isset($s_name)) {
        $this->view->is_search = 1;
    }
    if($current_sorting != 'close-to-me') {
        if($p_country_slug && $p_country_id && !$country){
            $country = $this->view->country = $p_country_id;
        }

        $p_city_slug = $this->view->p_city_slug = $this->_getParam('p_city_slug');
        $p_city_id = $this->view->p_city_id = (int) $this->_getParam('p_city_id');

        if($p_city_slug && $p_city_id && !$country){
            $city = $this->view->city = $p_city_id;
            $country = $this->view->country = Cubix_Geography_Cities::getCountryIdBySlug($p_city_slug);
        }
    }

    $looped = false; // This is used to detect if goto operator moved you back to START_FILTERING
    START_FILTERING: // Remember the point to make filtering again if needed

    $s_filter_params = array(
        'name' => ($s_name) ? $s_name . "%" : '',
        'agency-name' => $s_agency_slug,
        'gender' => is_array($s_gender) ? $s_gender : [$s_gender],
        'phone' => ($s_phone) ? "%" . $s_phone . "%" : '',
        'age-from' => $s_age_from,
        'age-to' => $s_age_to,
        'orientation' => $s_orientation,
        'keywords' => $s_keywords,
        'height-from' => $s_height_from,
        'height-to' => $s_height_to,
        'weight-from' => $s_weight_from,
        'weight-to' => $s_weight_to,
        'tatoo' => $s_tatoo,
        'piercing' => $s_piercing,
        'alert_id' => $alert_id,
        'ethnicity' => $s_ethnicity,
        'language' => $s_language,
        'nationality' => $s_nationality,
        'hair-color' => $s_hair_color,
        'hair-length' => $s_hair_length,
        'cup-size' => $s_cup_size,
        'eye-color' => $s_eye_color,
        'pubic-hair' => $s_pubic_hair,
        'currency' => $s_currency,
        'price-from' => $s_price_from,
        'price-to' => $s_price_to,
        'incall-outcall' => $s_incall_outcall,
        'sex-availability' => $s_sex_availability,
        'service' => $s_service,
        'incall' => $s_incall,
        'incall-hotel-room' => $s_incall_hotel_room,
        'outcall' => $s_outcall,
        'travel-place' => $s_travel_place,
        'real-pics' => $s_real_pics,
        'verified-contact' => $s_verified_contact,
        'with-video' => $s_with_video,
        'pornstar' => $s_pornstar,
        'travel_place' => $s_available_for_travel,
        'is-online-now' => $s_is_online_now,
        'show-all-hidden-escorts' => $show_all_hidden_escorts,
        'with-natural-photo'=> $s_natural_photo
    );


    if ( $ts_type == 'escort' ) {
        $s_filter_params['name'] = $ts_slug;
    } else if ( $ts_type == 'escort-see-all' ) {
        $s_filter_params['name'] = $ts_slug . "%";
    }

    $cache = Zend_Registry::get('cache');
    $this->view->cache = $cache;

    $static_page_key = 'main';
    $is_tour = false;
    $is_upcomingtour = false;
    $gender = empty($s_gender) ? GENDER_FEMALE : $s_gender;
    $is_agency = null;
    $is_new = false;
    $category = ESCORT_TYPE_ESCORT;

    $s_config = Zend_Registry::get('system_config');
    $e_config = Zend_Registry::get('escorts_config');
    $this->view->showMainPremiumSpot = $s_config['showMainPremiumSpot'];

    if ( ! $s_config['showMainPremiumSpot'] ) {
        $static_page_key = 'regular';
    }

    $filtered_location = [];
    if ( $this->_request->page )
    {
        $page = $this->_request->page;
    }elseif ( $this->_getParam('page') )
    {
        $page = $this->_getParam('page');
    }else{
        $page = 1;
    }

    $params = array(
        'sort' => 'random',
        'filter' => array(),
        'page' => $page
    );
    if (!empty($city)) {
        $city_data = Model_Cities::getCountryRegionByCityID($city);
        $filtered_location = array_merge($filtered_location, (array)$city_data);


        $this->view->city_title = $city_data->city_title;
    }

    // If we have don't have cityID, but regionID,
    // then select all data about region
    // --------------------------------------

    // If we have don't have cityID and regionID, but countryID,
    // then select all data about country
    // --------------------------------------
    if (!empty($country)) {
        $country_data = Model_Countries::getCountryByID($country);



        $this->view->country_title = $country_data->country_title;
        $model = new Cubix_Geography_Cities();
        $country_cities = $model->ajaxGetAll(0, $country);

        $this->view->country_cities = $country_cities;

        $filtered_location = array_merge($filtered_location, (array)$country_data);
    }
    // --------------------------------------
    // When listing is sorted by "close to me / nearby"
    // remove country from filters
    // ------------------------------------
    if($current_sorting != 'close-to-me') {

        // If user searched for cityzone, then fetch its info
        // and fill in top searchbar
        // --------------------------------------
        if (!empty($zone)) {
            $zone_data = Model_Cityzones::getZoneCityCountryByID($zone);
            $filtered_location = array_merge($filtered_location, (array)$zone_data);
        }

        // If we have cityID, fetch all info about it
        // to fill in top searchbar
        // --------------------------------------

        if (!empty($region)) {
            $region_data = Model_Regions::getRegionCountryByID($region);
            $filtered_location = array_merge($filtered_location, (array)$region_data);
        }



        // Store the location which user filtered last time
        // to put it in comments page ( in the filter panel )
        // !UPDATE! Not only in comments page, in listing also
        // ---------------------------------------
        $location_session = @new Zend_Session_Namespace('last-filtered-location');
        $location_session->setExpirationHops(60 * 60);
        // ---------------------------------------

        // This block of code fills locationbar and search parameters
        // with last time searched location
        // ---------------------------------------
        if( false ) { // Commented temporary
            if (!empty($location_session->data) && empty($filtered_location) && !$user_cleared_location_searchbar) {
                $filtered_location = &$location_session->data;

                $city = $filtered_location['city_id'];
                $country = $filtered_location['country_id'];
                $region = $filtered_location['region_id'];
            }
        }
        // ---------------------------------------

        // If users location is detected but user didnt make any location searches
        // Need to also check if user didnt clear himself the searchbar, otherwise this means he just doesnt want to see escorts from his location
        // then put that into searchbar
        // -------------------------------------
        if( false ) { // Commented temporary
            if (!$user_cleared_location_searchbar && !empty($geoData) && isset($geoData['city']) && empty($filtered_location) && !$looped && empty($location_session->data)) {
                $locationFilledWithGeo = true;
                $modelCities = new Model_Cities();
                $geo_city = $this->view->geo_city = $modelCities->getByGeoIp();
                $filtered_location = (array)$geo_city;

                $city = $geo_city['city_id'];
                $country = $geo_city['country_id'];
                $region = $geo_city['region_id'];
                $s_currency = $this->view->s_currency = Model_Countries::getGeoCurrency();
            }
        }
        // -------------------------------------
    }
    // ------------------------------------

    $location_session->data = $this->view->filtered_location = $filtered_location; // Store filtered location into session

    $is_active_filter = false;
    foreach( $s_filter_params as $i => $par ) {
        if ( is_array($par) ) {
            if( count($par) ) {
                $is_active_filter = true;
                break;
            }
        } else {
            if( $par ) {
                $is_active_filter = true;
                break;
            }
        }
    }

    if (!$is_active_filter)
    {
        if ($country || $city)
            $is_active_filter = true;
    }

    $this->view->is_active_filter = $is_active_filter;

    // Filtering escorts by their type (Agency, Independent ...)
    // ----------------------------------------------
    switch($escortType) {
        case 'independent-escorts':
            $this->view->searchEscortType = 'independent-escorts';
            $params['filter'][1] = array('field' => 'independent-escorts', 'value' => array());
            break;
        case 'agencies':
        case 'agency-escorts':
            $this->view->searchEscortType = 'agency-escorts';
            $params['filter'][1] = array('field' => 'agencies', 'value' => array());
            break;
        default:
            $this->view->searchEscortType = 'escorts';
            break;
    }
    // ----------------------------------------------

    if ( $p_top_category) {
        switch( $p_top_category )
        {
            case 'newest':
                $static_page_key = 'newest';
                $is_agency = null;
                $is_new = true;
                $params['filter'][0] = array('field' => 'newest', 'value' => array());
                continue;
            case 'newest-independent':
                $static_page_key = 'newest-independent';
                $is_agency = 0;
                $is_new = true;
                $params['filter'][0] = array('field' => 'newest-independent', 'value' => array());
                continue;
            case 'newest-agency-escorts':
                $static_page_key = 'newest-agency-escorts';
                $is_agency = 1;
                $is_new = true;
                $params['filter'][0] = array('field' => 'newest-agency-escorts', 'value' => array());
                continue;
            case 'escorts':
                if(is_numeric($p_city_id) && $p_city_id != 0){
                    $static_page_key = 'cities-escorts';
                }else{
                    $static_page_key = 'escorts';
                    if($this->_getParam('online_now')){
                        $static_page_key = 'online-escorts';
                    }
                }
                $params['filter'][] = array('field' => 'escorts', 'value' => array());
                $this->view->searchCategory = GENDER_FEMALE . '_' . ESCORT_TYPE_ESCORT;
                continue;
            case 'escorts-only':
                $static_page_key = 'escorts-only';
                $is_agency = 0;
                $params['filter'][] = array('field' => 'escorts-only', 'value' => array());
                continue;
            case 'independent-escorts':
                $static_page_key = 'independent-escorts';
                $is_agency = 0;
                $params['filter'][] = array('field' => 'independent-escorts', 'value' => array());
                continue;
            case 'agency-escorts':
                $static_page_key = 'agency-escorts';
                $params['filter'][] = array('field' => 'agency', 'value' => array());
                continue;
            case 'agencies':
                $static_page_key = 'agencies';
                $is_agency = 1;
                $params['filter'][] = array('field' => 'agencies', 'value' => array());
                continue;
            case 'bdsm':
                $static_page_key = 'bdsm';
                $is_agency = null;
                $category = ESCORT_TYPE_BDSM;
                $params['filter'][0] = array('field' => 'bdsm', 'value' => array());
                $this->view->searchCategory = GENDER_FEMALE . '_' . ESCORT_TYPE_BDSM;
                continue;
            case 'bdsm-boys':
                $static_page_key = 'bdsm';
                $gender = GENDER_MALE;
                $category = ESCORT_TYPE_BDSM;
                $params['filter'][0] = array('field' => 'bdsm-boys', 'value' => array());
                continue;
            case 'bdsm-trans':
                $static_page_key = 'bdsm';
                $gender = GENDER_TRANS;
                $category = ESCORT_TYPE_BDSM;
                $params['filter'][0] = array('field' => 'bdsm-trans', 'value' => array());
                continue;
            case 'boys':
                $static_page_key = 'boys';
                $gender = GENDER_MALE;
                $params['filter'][0] = array('field' => 'boys', 'value' => array());
                $this->view->searchCategory = GENDER_MALE;
                continue;
            /*case 'boys-trans':
                $static_page_key = 'boys-trans';
                $params['filter'][0] = array('field' => 'boys-trans', 'value' => array());
                continue;
            case 'boys-heterosexual':
                $static_page_key = 'boys-heterosexual';
                $params['filter'][0] = array('field' => 'boys-heterosexual', 'value' => array());
                continue;
            case 'boys-bisexual':
                $static_page_key = 'boys-bisexual';
                $params['filter'][0] = array('field' => 'boys-bisexual', 'value' => array());
                continue;
            case 'boys-homosexual':
                $static_page_key = 'boys-homosexual';
                $params['filter'][0] = array('field' => 'boys-homosexual', 'value' => array());
                continue;*/
            case 'trans':
                $static_page_key = 'trans';
                $gender = GENDER_TRANS;
                $params['filter'][0] = array('field' => 'trans', 'value' => array());
                $this->view->searchCategory = GENDER_TRANS;
                continue;
            case 'citytours':
                $static_page_key = 'citytours';
                $this->view->hideOrdering = true;
                $is_tour = true;
                $gender = null;
                $params['filter'][] = array('field' => 'tours', 'value' => array());
                $this->view->searchCategory = GENDER_FEMALE . '_' . ESCORT_TYPE_ESCORT;
                continue;
            case 'upcomingtours':
                $static_page_key = 'upcomingtours';
                $is_tour = true;
                $is_upcomingtour = true;
                $gender = null;
                $params['filter'][] = array('field' => 'upcomingtours', 'value' => array());
                continue;
            case 'regular':
                $static_page_key = 'regular';
                $params['filter'][] = array('field' => 'regular', 'value' => array());
                continue;
            case 'massage':
                $static_page_key = 'massage';
                $gender = GENDER_FEMALE;
                $category = ESCORT_TYPE_MASSAGE;
                $params['filter'][0] = array('field' => 'massage', 'value' => array());
                $this->view->searchCategory = GENDER_FEMALE . '_' . ESCORT_TYPE_MASSAGE;
                continue;
            case 'massage-boys':
                $static_page_key = 'massage-boys';
                $static_page_key = 'massage-boys';
                $gender = GENDER_MALE;
                $category = ESCORT_TYPE_MASSAGE;
                $params['filter'][0] = array('field' => 'massage-boys', 'value' => array());
                continue;
            case 'massage-trans':
                $static_page_key = 'massage-trans';
                $gender = GENDER_TRANS;
                $category = ESCORT_TYPE_MASSAGE;
                $params['filter'][0] = array('field' => 'massage-trans', 'value' => array());
                continue;
            case 'view-alert':
                $static_page_key = 'view-alert';
                $params['filter'][0] = array('field' => 'view-alert', 'value' => array());
                continue;
            default:
                $this->_response->setRawHeader('HTTP/1.1 404 Not Found');
                $this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
                return;
        }

    }

    $list_types = array('gallery', 'list');

    if ( isset($_COOKIE['list_type']) && $_COOKIE['list_type'] ) {
        $list_type = $_COOKIE['list_type'];

        if ( ! in_array($list_type, $list_types) ) {
            $list_type = $list_types[0];
        }

        $this->view->render_list_type = $list_type;
    } else {
        $list_type = $list_types[0];
        $this->view->render_list_type = $list_types[0];
    }

    if ( $city ) {
        //$params['city'] = $p_city_slug;
        $params['filter'][] = array('field' => 'city_id', 'value' => array($city));
    }

    if ( $region ) {
        $params['filter'][] = array('field' => 'region_id', 'value' => array($region));
    }

    if ( $country ) {
        //$params['country'] = $p_country_slug;
        $params['filter'][] = array('field' => 'country_id', 'value' => array($country));
    }

    $this->view->static_page_key = $static_page_key;

    $filter_params = array(
        'order' => 'e.date_registered DESC',
        'limit' => array('page' => 1),
        'filter' => array()
    );


    // <editor-fold defaultstate="collapsed" desc="Map parameters from url to SQL where clauses">
    $filter_map = array(
        'name' => 'e.showname LIKE ?',
        'agency-name' => 'e.agency_slug = ?',
        'phone' => 'e.contact_phone_parsed LIKE ?',
        'age-from' => 'e.age >= ?',
        'age-to' => 'e.age <= ?',
        'height-from' => 'e.height >= ?',
        'height-to' => 'e.height <= ?',
        'weight-from' => 'e.weight >= ?',
        'weight-to' => 'e.weight <= ?',
        'tatoo' => 'e.tatoo = ?',
        'piercing' => 'e.piercing = ?',
        'real-pics' => 'e.verified_status = 2',
        'with-video' => 'with-video',
        'verified-contact' => 'e.last_hand_verification_date IS NOT NULL',
        'pornstar' => 'e.is_pornstar = 1',
        'travel_place' => 'e.travel_place IN (1, 2, 3)',
        'incall-other' => 'e.incall_other IS NOT NULL AND e.incall_other <> ""',
        'outcall-other' => 'e.outcall_other IS NOT NULL AND e.outcall_other <> ""',
        'region' => 'r.slug = ?',
        'city' => 'ct.slug = ?',
        'city_id' => 'eic.city_id = ?',
        'region_id' => 'eic.region_id = ?',
        'country_id' => 'eic.country_id = ?',
        'country' => 'cr.slug = ?',
        'cityzone' => 'c.id = ?',
        'zone' => 'cz.slug = ?',
        'boys-heterosexual' => 'e.sex_orientation = ' . ORIENTATION_HETEROSEXUAL . ' AND e.gender = ' . GENDER_MALE,
        'boys-bisexual' => 'e.sex_orientation = ' . ORIENTATION_BISEXUAL . ' AND e.gender = ' . GENDER_MALE,
        'boys-homosexual' => 'e.sex_orientation = ' . ORIENTATION_HOMOSEXUAL . ' AND e.gender = ' . GENDER_MALE,
        'escorts' => 'eic.gender = ' . GENDER_FEMALE . ' AND eic.type = ' . ESCORT_TYPE_ESCORT,
        'escorts-only' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
        'agencies' => 'eic.is_agency = 1',
        'independent-escorts' => 'eic.is_agency = 0 ',
        'agency' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE . ' AND eic.type = ' . ESCORT_TYPE_ESCORT,
        'boys-trans' => '(eic.gender = ' . GENDER_MALE . ' OR eic.gender = ' . GENDER_TRANS . ')',
        'boys' => 'eic.gender = ' . GENDER_MALE,
        'trans' => 'eic.gender = ' . GENDER_TRANS,
        'bdsm' => 'eic.gender = ' . GENDER_FEMALE .' AND eic.type = '.ESCORT_TYPE_BDSM,

        'bdsm-boys' => 'eic.gender = ' . GENDER_MALE. ' AND eic.type = '.ESCORT_TYPE_BDSM,
        'bdsm-trans' => 'eic.gender = ' . GENDER_TRANS . ' AND eic.type = '.ESCORT_TYPE_BDSM,
        'newest' => 'eic.gender = ' . GENDER_FEMALE,
        'newest-independent' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
        'newest-agency-escorts' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE,
        'tours' => 'eic.is_tour = 1',
        'upcomingtours' => 'eic.is_upcoming = 1',
        'massage' => 'eic.gender = '. GENDER_FEMALE. ' AND eic.type = '.ESCORT_TYPE_MASSAGE ,
        'massage-boys' => 'eic.gender = '. GENDER_MALE. ' AND eic.type = '.ESCORT_TYPE_MASSAGE,
        'massage-trans' => 'eic.gender = '. GENDER_TRANS. ' AND eic.type = '.ESCORT_TYPE_MASSAGE,
        'is-online-now' => 'ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1'
    );


    // <editor-fold defaultstate="collapsed" desc="Get page from request parameters">
    if ( $this->_request->page ) {
        $params['page'] = $this->_request->page;
    }

    if ( $this->_request->p_page && ! $this->_request->page ) {
        $params['page'] = $this->_request->p_page;
    }

    $page = intval($params['page']);
    if ( $page == 0 ) {
        $page = 1;
    }
    $filter_params['limit']['page'] = $this->view->page = $page;
    // </editor-fold>

    foreach ( $params['filter'] as $i => $filter ) {
        if ( ! isset($filter_map[$filter['field']]) ) continue;

        $value = $filter['value'];

        $filter_params['filter'][$filter_map[$filter['field']]] = $value;
    }

    /*if ( $online_now ) {
        unset($filter_params['filter']['eic.gender = 1']);
    }*/
    // </editor-fold>

    foreach ( $s_filter_params as $i => $s_filter ) {
        //if ( ! isset($filter_map[$i]) ) continue;

        if ( $s_filter && isset($filter_map[$i])) {
            $filter_params['filter'][$filter_map[$i]] = $s_filter;
            unset($s_filter_params[$i]);
        }
        elseif(!$s_filter || ( is_array($s_filter) && count($s_filter) == 0)){
            unset($s_filter_params[$i]);
        }
    }

    //Currency rates log
    $default_currency = "USD";
    $current_currency = $this->_getParam('currency', $default_currency);

    if ( $current_currency != $_COOKIE['currency'] && $this->_getParam('s') ) {
        setcookie("currency", $current_currency, strtotime('+1 year'), "/");
        $_COOKIE['currency'] = $current_currency;
    }

    if ( isset($_COOKIE['currency']) ) {
        $current_currency = $_COOKIE['currency'];
        $this->_setParam('currency', $current_currency);
    }

    $this->view->current_currency = $current_currency;

    $sort_map = array(
        'close-to-me' => 'ordering DESC',
        'recommended_escorts' => 'last_modified_date DESC',
        'newest' => 'date_registered DESC',
        'last-contact-verification' => 'last_hand_verification_date DESC',
        'price-asc' => 'price-asc', // will be modified from escorts
        'price-desc' => 'price-desc', // will be modified from escorts
        'last_modified' => 'date_last_modified DESC',
        'last-connection' => 'refresh_date DESC',
        'most-viewed' => 'hit_count DESC',
        'random' => 'ordering DESC',
        'alpha' => 'showname ASC',
        'age' => '-age DESC',
    );

    if ( !array_key_exists($current_sorting, $sort_map) ) {
        $current_sorting = $default_sorting;
        $this->_setParam('sort', $default_sorting);
    }

    if ( $current_sorting != $_COOKIE['sorting'] && $this->_getParam('sorting') ) {
        setcookie("sorting", $current_sorting, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
        setcookie("sortingMap", $sort_map[$current_sorting]['map'], strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
        $_COOKIE['sorting'] = $current_sorting;
        $_COOKIE['sortingMap'] = $sort_map[$current_sorting]['map'];
    }

    /*if ( isset($_COOKIE['sorting']) ) {
        $current_sorting = $_COOKIE['sorting'];
        $this->_setParam('sort', $current_sorting);
    }*/

    if ( $current_sorting ) {
        $tmp = $sort_map[$current_sorting];
        unset($sort_map[$current_sorting]);
        $sort_map = array_merge(array( $current_sorting => $tmp), $sort_map);
    }

    if ( isset($sort_map[$params['sort']]) ) {
        $filter_params['order'] = $sort_map[$params['sort']];
        $this->view->sort_param = $params['sort'];
    }

    $this->view->sort_map = $sort_map;
    $this->view->current_sort = $current_sorting;
    $params['sort'] = $current_sorting;
    // </editor-fold>

    // $model = new Model_EscortsV2(); // we dont use it))

    $count = 0;
    $nearbyCount = 0;

    // <editor-fold defaultstate="collapsed" desc="Cache key generation">
    $filters_str = '';


    $combined_filters = array_merge((array) $filter_params['filter'], (array) $s_filter_params);
    foreach ( $combined_filters as $k => $filter ) {
        if ( ! is_array($filter) ) {
            $filters_str .= $k . '_' . $filter;
        }
        else {
            $filters_str .= $k;
            foreach($filter as $f) {
                $filters_str .= '_' . $f;
            }
        }
    }

    /*$where_query_str = '';
    if ( $where_query ) {
        if ( count($where_query) ) {
            foreach ( $where_query as $query ) {
                if ( isset($query['filter_query']) && $query['filter_query'] ) {
                    $where_query_str .= $query['filter_query'];
                }
            }
        }
    }*/

    // </editor-fold>

    $per_page_def = $e_config['perPage'];

    $this->view->per_page = $per_page = Model_Escort_List::HOMEPAGE_LISTING_SIZE; // $this->_getParam('per_page', $per_page_def);

    $cache_key = 'v2_' . Cubix_I18n::getLang() . '_' . $params['sort'] . '_' . $filter_params['limit']['page'] . '_' . $filters_str . $where_query_str. (string) $has_filter . $per_page . $p_top_category . $country . $city . (int) $is_upcomingtour . (int) $is_tour . $list_type;
    $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
    $this->view->params = $params;

    $this->view->is_main_page = $is_main_page = ($static_page_key == 'escorts' && ! $city && ! $country );
    $this->view->filter_params = $filter_params['filter'];

    // If we are on main premium spot forward to corresponding action

    if ( $static_page_key == 'happy-hours' ) {
        $this->_forward('happy-hours');
        return;
    }

    if ( $static_page_key == 'chat-online' ) {
        $this->_forward('chat-online');
        return;
    }


    foreach($s_filter_params as $k => $value ) {
        if ( is_array($value) ) {
            foreach( $value as $val ) {
                $cache_key .= '_' . $val . '_';
            }
        } else {
            $cache_key .= '_' . $k . '_' . $value;
        }
    }

    $geo_cache = "";

    if ( $params['sort'] == 'close-to-me' ) {

        if ( strlen($geoData['country']) ) {
            $geo_cache = md5($geoData['latitude'] . $geoData['longitude']);
            $cache_key .= $geo_cache;
        }
    }

    $cache_key .= $this->view->render_list_type;
    $cache_key .= Cubix_Countries::blacklistCountryCacheKeyCase();
    $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
    $cache_key = sha1($cache_key);

    //$cache_key_count = $cache_key . '_count';

    //$count = $cache->load($cache_key_count);

    if ( $is_active_filter ) {
        $filter_params['filter']['show_all_agency_escorts'] = true;
    }

    $only_newest = false;
    if ( in_array($static_page_key, array('newest', 'newest-independent', 'newest-agency-escorts'))) {
        $params['sort'] = 'newest';
        $filter_params['filter'] ['e.is_new = 1'] = array();
        $only_newest = true;
    }

    // EDIR-2554
    // ---------------------------------------
    $config = Zend_Registry::get('system_config');
    $payemnt_disabled_countries = explode(',',$config['paymentDisabledCountries']);
   
    if(in_array($country, $payemnt_disabled_countries) || in_array($location_session->data['country_id'], $payemnt_disabled_countries) ||
        in_array(strtolower($geoData['country_iso']), array('us', 'fr'))
     ){
        $paymentdisabledforcountry = true;
    }
    // ---------------------------------------
    $escorts = array();

    if(isset($s_name)){

    }

    $isInitialLoad = empty($_GET);

    if (!$isInitialLoad) {
        list($escorts) = Model_Escort_List::getFilteredForAdvancedSearch($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $per_page, $count, $s_filter_params, false, $geoData, $list_type, $p_top_category, $city, $country, $current_currency, $paymentdisabledforcountry, $nearbyCount);
    }
    else {
        $escorts = [];
    }
    $this->view->no_cache = true;
    $this->view->isInitialLoad = $isInitialLoad;



    if(empty($escorts['vips'])) unset($escorts['vips']);
    if(empty($escorts['premiums'])) unset($escorts['premiums']);

    // Now the SATANIC logic comes :) (EDIR-2222)
    // We will remove everything from location step by step (e.g. First remove city then region  ...) and
    // make search again and again untill we get at least 1 escort then continue
    // -----------------------------------
    if (count($escorts) < 1 && $locationFilledWithGeo) {

        $looped = true;

        if(isset($geoData['city'])) {
            unset($geoData['city'], $city);
            goto START_FILTERING;
        }

        if(isset($region)) {
            unset($region);
            goto START_FILTERING;
        }

        if(isset($geoData['country'])) {
            unset($geoData['country'], $geoData['country_iso'], $country);
            goto START_FILTERING;
        }
    }
    // -----------------------------------
    if ($params['sort'] == 'close-to-me')
    {
        $this->view->found_escorts_count = $nearbyCount;
    }else{
        $this->view->found_escorts_count = $count;
    }
    // EDIR - 2560
    // If the city or country was specified, but in search we have less than 15 escorts
    // this function is going to generate other 12 escorts from cities
    // that are near to selected one
    // ---------------------------------------
    if(count($escorts) < 15 ) {
        $excludedEscortIds = [];
        foreach($escorts as $key => $escort) {
            if ($key == 'premiums')
            {
                foreach ($escort as $prem_escorts)
                {
                    if(isset($prem_escorts->id) AND !empty($prem_escorts->id))
                        $excludedEscortIds[] = $prem_escorts->id;
                }
            }elseif ($key == 'vips'){
                foreach ($escort as $vip_escorts)
                {
                    if(isset($vip_escorts->id) AND !empty($vip_escorts->id))
                        $excludedEscortIds[] = $vip_escorts->id;
                }
            }else{
                if(isset($escort->id) AND !empty($escort->id))
                    $excludedEscortIds[] = $escort->id;
            }
        }

        list(
            $escortsAround,
            $aroundLocationTitle,
            ) = Model_Escort_List::getAroundEscorts($filter_params['filter'], $excludedEscortIds, $per_page, $page, $p_top_category, $city, $country);

        if (count($escortsAround))
        {
            foreach ($escortsAround as $k => $e_around)
            {
                if (in_array($e_around->id,$excludedEscortIds))
                {
                    unset($escortsAround[$k]);
                }
            }
        }

        $this->view->escortsAround = $escortsAround;
        $this->view->aroundLocationTitle = $aroundLocationTitle;
    }
    // ---------------------------------------
    $this->view->global_cache_key = $cache_key . $_COOKIE['show_all_escorts'] . $current_currency;

    // Video Merge
    $escorts_video = array();
    if( ! empty($escorts) ) {
        $video_cache_key = "video_escort_list_" . Cubix_Application::getId() . $this->view->global_cache_key;

        if ( !$vescorts = $cache->load($video_cache_key) ) {
            $e_id = array();

            foreach($escorts as $e)
            {
                if( !is_null($e->id)){
                    $e_id[] = $e->id;
                }
            }
            if($escorts['vips']){
                foreach($escorts['vips'] as $e)
                {
                    $e_id[] = $e->id;
                }
            }
            if($escorts['premiums']){
                foreach($escorts['premiums'] as $e)
                {
                    $e_id[] = $e->id;
                }
            }
            $video = new Model_Video();
            $vescorts = $video->GetEscortsVideoArray($e_id);
            $cache->save($vescorts, $video_cache_key, array());
        }

        if(!empty($vescorts))
        {
            $app_id = Cubix_Application::getId();
            foreach($vescorts as &$v)
            {
                $photo = new Cubix_ImagesCommonItem(array(
                    'hash'=> $v->hash,
                    'width'=> $v->width,
                    'height'=> $v->height,
                    'ext'=> $v->ext,
                    'application_id'=> $app_id
                ));

                $v->photo = $photo->getUrl('orig');
                $escorts_video[$v->escort_id] = $v;
            }
            //$config = Zend_Registry::get('videos_config');
            //$host =  Cubix_Application::getById(Cubix_Application::getId())->host;
            //$this->view->video_rtmp = $config['Media'].'mp4:'.$host;
            //$this->view->video_source = $config['remoteUrl'].$host;
        }
    }

    // Hardcode position for VIP Ads, if they match to criteria
    // --------------------------------------------------
    if($escorts && !empty($escorts['vips'])) {
        $escorts['vips'] = $this->_hardcode_vips_position([
            'country' => $country,
            'city' => $city,
            'vips' => $escorts['vips']
        ]);
    }
    // --------------------------------------------------

    // Save for every escort, new individual "visit"
    // This means someone has checked her/his profile
    // ------------------------------------------
    $this->_store_visit_on_every_escort([
        'escorts' =>  array_merge((array) $escorts, (array) $escorts['vips'], (array) $escorts['premiums'] ),
        'city' => $city,
    ]);
    // ------------------------------------------

    if(!$paymentdisabledforcountry) { // EDIR-2554

        // Add banner to escorts array
        // -------------------------------------
        if ( !empty($escorts['vips'])) {
            switch (count($escorts['vips'])) {
                case 1:
                case 2:
                    if ($page < 2) {
                        if(count($escorts['vips'])  == 1) {
                            if(count($escorts['premiums']) == 1) {
                                $escorts['premiums'][] = 'vip-banner';
                            }else{
                                array_splice($escorts['premiums'], $s_config['vipbanner'] - 1, 0, 'vip-banner');
                            }
                        }elseif(count($escorts['vips'])  == 1){
                            array_splice($escorts, $s_config['vipbanner'], 0, 'vip-banner');
                        }else{
                            $escorts['vips'][] = 'vip-banner';
                        }
                    }
                    $count++;
                    break;
                default:
                    if ($page < 2) {
                        array_splice($escorts['vips'], $s_config['vipbanner'], 0, 'vip-banner');
                    }
                    $count++;
                    break;
            }
        } elseif (!empty($escorts['premiums'])) {
            switch (count($escorts['premiums'])) {
                case 1:
                case 2:
                    if ($page < 2) {
                        if (count($escorts['premiums']) == 1) {
                            array_splice($escorts, 2, 0, 'vip-banner');
                        } else {
                            $escorts['premiums'][] = 'vip-banner';
                        }
                    }
                    $count++;
                    break;
                default:
                    if ($page < 2) {
                        array_splice($escorts['premiums'], $s_config['vipbanner'], 0, 'vip-banner');
                    }
                    $count++;
                    break;
            }
        } else {

            // Another task from Katie (EDIR-1895)
            // --------------------------------------
            if (count($escorts) > 0) {
                $vips_premiums = 0;
                if ($page < 2) {
                    array_splice($escorts, $s_config['vipbanner'] + $vips_premiums, 0, 'vip-banner');
                }
                $count++;
            }
            // --------------------------------------
        }
        // -------------------------------------
    }


    $this->view->escorts_video = $escorts_video;
    $this->view->pagination_count = $count;
    $this->view->escorts = $escorts;
    $this->view->is_tour = ( !$is_upcomingtour && $is_tour) ? 1 : 0;
    $this->view->is_upcomingtour = $is_upcomingtour ? 1 : 0;
    $this->view->gender = $gender;
    $this->view->is_agency = $is_agency;
    $this->view->is_new = $is_new;
    $this->view->category = $category;

    /* watched escorts */
    if ($list_type != 'simple')
    {
        $escorts_buf = $escorts;
        $ids = array();
        $watched_escorts = array();

        if (isset($escorts_buf['premiums']))
        {
            foreach ($escorts_buf['premiums'] as $e)
            {
                if(is_string($e)){
                    continue;
                }
                $ids[] = $e->id;
            }

            unset($escorts_buf['premiums']);
        }

        if (isset($escorts_buf['vips']))
        {
            foreach ($escorts_buf['vips'] as $e)
            {
                if(is_string($e)){
                    continue;
                }
                $ids[] = $e->id;
            }

            unset($escorts_buf['vips']);
        }

        foreach ($escorts_buf as $e)
        {
            if(is_string($e)){
                continue;
            }
            $ids[] = $e->id;
        }

        $ids = array_filter($ids);
        if ($ids)
        {
            $ids_str = trim(implode(',', $ids), ',');
            $user = Model_Users::getCurrent();
            $modelM = new Model_Members();

            if ($user)
            {
                $escorts_watched_type = $user->escorts_watched_type;

                if (!$escorts_watched_type) $escorts_watched_type = 1;

                $res = $modelM->getFromWatchedEscortsForListing($ids_str, $escorts_watched_type, $user->id, session_id());
            }
            else
            {
                $res = $modelM->getFromWatchedEscortsForListing($ids_str, WATCH_TYPE_PER_SESSION, null, session_id());
            }

            if ($res)
            {
                foreach ($res as $r)
                {
                    $watched_escorts[] = $r->escort_id;
                }
            }
        }

        $this->view->watched_escorts = $watched_escorts;
    }
    /**/

    $this->view->has_filter = $has_filter;


    //$this->view->newest_escorts = $newest_escorts;

    $this->view->montly_price_vip = $this->get_montly_price('vip', $country, $p_top_category, $geoData);
    $this->view->montly_price_premium = $this->get_montly_price('premium', $country, $p_top_category, $geoData);
    $current_filter = $this->getCurrentFilter();

    $existing_filters = Model_Escort_List::getActiveFilter($filter_params['filter'], $current_filter, $is_upcomingtour, $is_tour);
    $this->view->lookingFor = (!$is_agency)? $gender : 'agencies';
    $this->view->interestedIn = $category;
    $this->getFilterV2Action($existing_filters);

}
private function getCurrentFilter($existing_filters = array())
{
    if(is_null(self::$mapper)){
        $this->setMapper();
    }

    $filter = new Cubix_FilterV2( array('mapper' => self::$mapper->getAll(), 'request' => $this->_request, 'existingFilters' => $existing_filters));
    return $filter->makeQuery();
}

public function getFilterV2Action($existing_filters = array(), $p_top_category = null)
{
    if ( ! is_null($this->_getParam('ajax'))){
        $this->view->layout()->disableLayout();
    }
    $current_filter = array();
    if(is_null(self::$mapper)){
        $this->setMapper();
    }

    if ( ! is_null($this->_getParam('ajax')) && ! is_null($this->_getParam('filter_params')) ) {
        $filter_params = (array) json_decode(base64_decode($this->_getParam('filter_params')));
        $p_top_category = (!empty($p_top_category)) ? $p_top_category : $this->_getParam('p_top_category');
        $is_upcomingtour =  $this->_getParam('is_upcomingtour');
        $is_tour = $this->_getParam('is_tour');

        if ($this->_getParam('with_data')){
            $post_params = $this->getPostFilters();
            $filter_params = array_merge($post_params, $filter_params);
            $current_filter = $this->getCurrentFilter();
        }

        $existing_filters = Model_Escort_List::getActiveFilter($filter_params, $current_filter, $is_upcomingtour, $is_tour);
        $filter_defines = Model_Escort_List::getFilterDefines($p_top_category, $is_upcomingtour, $is_tour);
    }

    $filter = new Cubix_FilterV2( array('mapper' => self::$mapper->getAll(), 'request' => $this->_request, 'existingFilters' => $existing_filters, 'filterDefines' => $filter_defines) );
    /*if($existing_filters){
        var_dump($filter->renderED());die;
    }*/

    //$this->view->count = $existing_filters['result']->cnt;
    $this->view->count = $existing_filters['result']->cnt;
    $this->view->filter = $filter;
//        $this->view->filter = $filter->renderED();

}

private function _store_visit_on_every_escort($params) {

    if(empty($params['escorts'])) return false;

    // Filtered city
    // ----------------------
    $city = $params['city'];
    // ----------------------

    foreach($params['escorts'] as $_escort) {

        // Validating, because of vips and premiums,
        // there could be no vip or premium for some cities
        if( !empty($_escort) && is_object($_escort)) {
            // Store into redis
            // ------------------------------------
            Model_Analytics::new_escort_visit([
                'escort' => $_escort->id,
                'visitor' => Cubix_Geoip::getIP(),
                'action-type' => Model_Analytics::ACTION_TYPE_LISTING // From which page was the view
            ]);
            // ------------------------------------

            // If the visitor, selected some city from the filters
            // and escort is from that city
            // ---------------------------------------------
            if (!empty($city) && $city == $_escort->city_id) {
                Model_Analytics::new_escort_visit([
                    'escort' => $_escort->id,
                    'visitor' => Cubix_Geoip::getIP(),
                    'action-type' => Model_Analytics::ACTION_TYPE_CITY, // From which page was the view
                    'city' => $city
                ]);
            }
            // ---------------------------------------------
        }
    }

}
public function get_montly_price($advertise_type, $country, $category, $geoData = false)
{
    if($category == 'escorts'){
        if(!is_null($country)){
            // SELECTED COUNTRY
            if(in_array($country, array(68, 10, 24 , 23, 33, 67 ))){
                // A COUNTRIES
                if($advertise_type == 'vip'){
                    return 33;
                }elseif($advertise_type == 'premium'){
                    return 25;
                }
            }else{
                // B COUNTRIES
                if($advertise_type == 'vip'){
                    return 25;
                }elseif($advertise_type == 'premium'){
                    return 17;
                }
            }
        }else{
            // if country geo detected
            if( in_array( strtolower($geoData['country_iso']), array('us', 'ca', 'de' , 'fr', 'it', 'gb' )) ){
                // A COUNTRIES
                if($advertise_type == 'vip'){
                    return 33;
                }elseif($advertise_type == 'premium'){
                    return 25;
                }
            }else{
                // B COUNTRIES
                if($advertise_type == 'vip'){
                    return 25;
                }elseif($advertise_type == 'premium'){
                    return 17;
                }
            }
        }
    }elseif( in_array( $category, array( 'bdsm', 'massage' ) ) ){
        if(!is_null($country)){
            // SELECTED COUNTRY
            if(in_array($country, array(68, 10, 24 , 23, 33, 67 ))){
                // A COUNTRIES
                return 20;
            }else{
                // B COUNTRIES
                return 16;

            }
        }else{
            // if country geo detected
            if( in_array( strtolower($geoData['country_iso']), array('us', 'ca', 'de' , 'fr', 'it', 'gb' )) ){
                // A COUNTRIES
                return 20;

            }else{
                // B COUNTRIES
                return 16;
            }
        }
    }

}

public function _hardcode_vips_position(Array $args)
{
    $country = $args['country'];
    $city = $args['city'];
    $vips = $args['vips'];

    if (empty($vips)) return;

    // Escort VIP reorder in spots
    // -------------------------------------------------------
    $escort_places_arr = Model_VipHardcodes::getAll();

    // Group "escort places", Set escort_id as identifier
    // -------------------------------------------------------
    $count_of_ads_in_this_area = 0;
    $escort_places = [];
    foreach ($escort_places_arr as $key => $place) {

        $country_matches = $place->country_id == $country;
        $city_matches = $place->city_id == $city;

        if (($country && $country_matches) || ($city && $city_matches)) {
            $count_of_ads_in_this_area++;
            $escort_places[$place->escort_id] = $place;
        }
    }

    // Creating empty array, this will be used
    // to store sed_cards inside, but in ordered position
    // -------------------------------------------------------------
    $ordered_vips = range(0, count($vips) - 1);
    // -------------------------------------------------------------

    // Put sed_cards into their hardcoded place
    // -------------------------------------------------------------
    foreach ($vips as $id => $escort) {
        $pp_data = $escort_places[$escort->id];
        if (!is_null($pp_data)) {

            // Check if current filtered country and city are in hardcoded list
            // -------------------------------------------
            $country_matches = $pp_data->country_id == $country;
            $city_matches = $pp_data->city_id == $city;
            // -------------------------------------------

            if ((($country && $country_matches) || ($city && $city_matches)) && count($vips) > $count_of_ads_in_this_area) {
                $escort->hardcoded_id = $pp_data->agency_id;
                $ordered_vips[$pp_data->place] = $escort;
            }
        }
    }
    // -------------------------------------------------------------

    // Filling up the ordered array with other VIP non hardcoded Ads
    // -------------------------------------------------------------
    foreach ($vips as $id => $escort)
        if (!isset($escort_places[$escort->id]))
            foreach ($ordered_vips as $key => $val)
                if (is_numeric($val)) {
                    $ordered_vips[$key] = $escort;
                    break;
                }
    // -------------------------------------------------------------

    // Shuffle Ads if their Agency is the same
    // -------------------------------------------------------------
    for ($i = 0; $i < count($ordered_vips); $i++) {
        for ($j = 0; $j < count($ordered_vips); $j++) {
            if (!is_null($ordered_vips[$i]->hardcoded_id) && $ordered_vips[$i]->hardcoded_id == $ordered_vips[$j]->hardcoded_id && (rand(0, 10) / 10) < 0.5) {
                $temp = $ordered_vips[$i];
                $ordered_vips[$i] = $ordered_vips[$j];
                $ordered_vips[$j] = $temp;
            }
        }
    }

    // -------------------------------------------------------------
    $ordered_vips = array_filter($ordered_vips, function($x) {
        return !is_numeric($x);
    });

    return $ordered_vips;
}
}