<?php

class CityAlertsController extends Zend_Controller_Action 
{
	/* @var $_model Model_CityAlerts*/
	private $_model;
	private $user;
		
	public function init() 
	{
		$this->user = $this->view->user = Model_Users::getCurrent();
		$this->view->layout()->setLayout('private');
		
		if ( ! $this->user ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		else if ($this->user->user_type != 'member')
		{
			$this->_redirect($this->view->getLink('dashboard'));
			return;
		}
		
		$this->_model = new Model_CityAlerts();
	}
	
	public function indexAction() 
	{
		$userAlertList = $this->_model->getEventList($this->user->id);

		$photosModel = new Model_Escort_Photos;
		$sortedAlertId = array();
		if( $userAlertList){
			foreach ($userAlertList as $key => $item) {
				// Get Photo
				$image = false;
				$photo = $photosModel->getHashAndExt($item['escort_id']);

				if(!empty($photo)){
					$p = array('application_id' => Cubix_Application::getId(), 'escort_id' => $item['escort_id'], 'hash' => $photo[$key]->hash, 'ext' => $photo[$key]->ext);
					$po = new Model_Escort_PhotoItem($p);
					$image = $po->getUrl('new-entries');
				}
				$item['photo'] = $image;

				$sortedAlertId[$item['event_alert_id']][] = $item;
			}
		}

		$this->view->userAlertList = $sortedAlertId;
		$this->view->layout()->disableLayout();
	}
	
	public function ajaxGetAction() 
	{
		$this->view->layout()->disableLayout();
		$this->view->items = $this->_model->getAllV2($this->user->id);
	}
	
	public function removeAction() 
	{
		$this->view->layout()->disableLayout();
		
		if ($this->_request->isPost())
		{
			$id = intval($this->_request->id);
			
			if (!$id) die;
			
			$this->_model->removeV2($id, $this->user->id);
		}
		
		die;
	}
	
	public function suspendAction() 
	{
		$this->view->layout()->disableLayout();
		
		if ($this->_request->isPost())
		{
			$id = intval($this->_request->id);
			
			if (!$id) die;
			
			$this->_model->suspend($id, $this->user->id);
		}
		
		die;
	}

	public function resumeAction() 
	{
		$this->view->layout()->disableLayout();
		
		if ($this->_request->isPost())
		{
			$id = intval($this->_request->id);
			
			if (!$id) die;
			
			$this->_model->resume($id, $this->user->id);
		}
		
		die;
	}

	public function addAction() 
	{
		$this->view->layout()->disableLayout();
				
		$country = intval($this->_request->country);

		$city_id_1 = intval($this->_request->city_id_1);
		$city_id_2 = intval($this->_request->city_id_2);
		$city_id_3 = intval($this->_request->city_id_3);

		$cities = array($city_id_1,$city_id_2,$city_id_3);

		$escort_type = intval($this->_request->escort_type);
		$escort_gender = intval($this->_request->escort_gender);
		$send_email = intval($this->_request->send_email);
		$subject = $this->_request->alert_subject;


		if ($this->_request->isPost())
		{
			$validator = new Cubix_Validator();

			if (!$country) {
				$validator->setError('country');

				$return = array('status' => 0, 'message' => "Country required");
			}

			$cities = array_unique($cities);
			$checkCity = $this->_model->checkCityExistsV2($country, $cities, $this->user->id, $escort_type, $escort_gender);


			if($checkCity['status'] == 0){
				$validator->setError('error');
				switch ($checkCity['code']) {
					case 'city_exist':
						$message = "City exist";
						break;	
					default:
						$message = 'unknown error';
						break;
				}
				$return = array('status'=>0,'message'=>$message);
			}

			if ( $validator->isValid() ) 
			{
				$data = array(
					'user_id' => $this->user->id,
					'country_id' => $country,
					'cities' => $cities,
					'escort_type' => $escort_type,
					'escort_gender' => $escort_gender,
					'send_email' => $send_email,
					'subject' => $subject,
					'date' => new Zend_Db_Expr('NOW()')
				);

				$this->_model->addV2($data);
				
				$return = array('status'=>1);
			}

			die(json_encode($return));
		}
	}

	public function saveAction() 
	{
		$this->view->layout()->disableLayout();
				
		$country = intval($this->_request->country);
		$id = intval($this->_request->id);

		$city_id_1 = intval($this->_request->city_id_1);
		$city_id_2 = intval($this->_request->city_id_2);
		$city_id_3 = intval($this->_request->city_id_3);

		$cities = array($city_id_1,$city_id_2,$city_id_3);

		$escort_type = intval($this->_request->escort_type);
		$escort_gender = intval($this->_request->escort_gender);
		$send_email = intval($this->_request->send_email);
		$subject = $this->_request->alert_subject;
		
		if ($this->_request->isPost())
		{
			$validator = new Cubix_Validator();
			
			if (!$id){
				$validator->setError('country');
				$return = array('status'=>0,'message'=>"Save ID required");
			}

			if (!$country){
				$validator->setError('country');
				$return = array('status'=>0,'message'=>"Country required");
			}

			$cities = array_unique($cities);
			$checkCity = $this->_model->checkCityExistsV2($country, $cities, $this->user->id, $escort_type, $escort_gender,$id);


			if($checkCity['status'] == 0){
				$validator->setError('error');
				switch ($checkCity['code']) {
					case 'city_exist':
						$message = "City exist";
						break;	
					default:
						$message = 'unknown error';
						break;
				}
				$return = array('status'=>0,'message'=>$message);
			}

			if ( $validator->isValid() ) 
			{
				$data = array(
					'id' => $id,
					'user_id' => $this->user->id,
					'country_id' => $country,
					'cities' => $cities,
					'escort_type' => $escort_type,
					'escort_gender' => $escort_gender,
					'send_email' => $send_email,
					'subject' => $subject
				);

				$this->_model->save($data);
				
				$return = array('status'=>1);
			}

			die(json_encode($return));
		}
	}

	public function clearAction() 
	{
		$this->view->layout()->disableLayout();
		$response = array('status' => 0);
		if ($this->_request->isPost())
		{
			$id = intval($this->_getParam('id', 0));
			if (!$id) {
				echo json_encode($response);
				die;
			}
			
			$this->_model->clearList($id, $this->user->id);
			$response['status'] = 1;
		}
		echo json_encode($response);
		die;
	}

	public function checkIfExistAction()
	{
		$this->view->layout()->disableLayout();
		$country = intval($this->_request->country_id);
		$city_id_1 = intval($this->_request->city_id_1);
		$city_id_2 = intval($this->_request->city_id_2);
		$city_id_3 = intval($this->_request->city_id_3);

		$cities = array($city_id_1,$city_id_2,$city_id_3);

		$checkCity = $this->_model->checkCityExistsV2ED($country, $cities, $this->user->id);
		die(json_encode($checkCity));
	}
}

?>
