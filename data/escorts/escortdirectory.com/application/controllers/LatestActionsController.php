<?php

class LatestActionsController extends Zend_Controller_Action
{
    private function _temporaryRedirect()
    {
        // Permanent redirection
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: /");
        exit();
    }

	public function init()
	{
        return $this->_temporaryRedirect();

        $this->model = new Model_LatestActions();
		$this->client = new Cubix_Api_XmlRpc_Client();
	}
	
	public function indexAction()
	{
		$defines = $this->view->defines = Zend_Registry::get('defines');
		//$this->view->cities = $this->model->getAllCitiesForFilter();
		
		$filter = array();
		
		$this->view->page = $page = 1;
		$this->view->per_page = $per_page = 36;
		
		$escorts = $this->model->getAll($filter, $page, $per_page, $count);
		
		$this->view->escorts = $escorts;
		$this->view->count = $count;
		
		$this->view->action_counts = $this->model->getActionCounts($filter);		
	}

	public function ajaxListAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$this->view->defines = Zend_Registry::get('defines');
		
		$page = intval($req->page);
		if ($page < 1) $page = 1;
		$this->view->page = $page;

		$per_page_def = 36;
		$per_page = $this->_getParam('per_page', $per_page_def);
		$this->view->per_page = $per_page;
		
		$filter = array();
		
		/*$city = (int)$req->city;
		$this->view->country = $country = (int)$req->country;*/
		$action = (int)$req->action_type;
		
		/*if ( $city ) {
			$filter['city_id'] = $city;
		}

		if ( $country ) {
			$filter['country_id'] = $country;
		}*/
		
		if ( $action ) {
			$filter['action_type'] = $action;
		}
		
		$this->view->escorts = $this->model->getAll($filter, $page, $per_page, $count);
		$this->view->count = $count;
	}
	
	public function ajaxGetDetailsAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->addScriptPath($this->view->getScriptPath('latest-actions'));
		
		$escort_id = $this->view->escort_id = (int)$this->_request->escort_id;
		$act = (int)$this->_request->act;
		$entity_id = (int)$this->_request->entity_id;
		
		if ( !$escort_id || ! in_array($act, Cubix_LatestActions::$_actions_arr) ) {
			die(':)');
		}
		
		switch ($act) {
			case Cubix_LatestActions::ACTION_NEW_PHOTO:
				$view_script = "photo";
				$this->view->photos = $this->model->getPhotoDetails($escort_id);
			break;
			case Cubix_LatestActions::ACTION_PROFILE_UPDATE:
				$view_script = "profile";
				$m_escort = new Model_EscortsV2();
				$this->view->showname = $m_escort->getShownameById($escort_id);
				$data = $this->model->getProfileDetails($escort_id);
				$this->view->new_profile = $new = $data['new'];
				$this->view->old_profile = $old = $data['old'];
				$this->view->changes = $this->model->differProfile($old, $new);
			break;
			case Cubix_LatestActions::ACTION_TOUR_UPDATE:
				$view_script = "tours";
				$m_escort = new Model_EscortsV2();
				$this->view->showname = $m_escort->getShownameById($escort_id);
				$this->view->tours = $this->model->getTourDetails($escort_id);
			break;
			case Cubix_LatestActions::ACTION_NEW_REVIEW:
				$view_script = "reviews";
				$m_escort = new Model_EscortsV2();
				$this->view->showname = $m_escort->getShownameById($escort_id);
				$this->view->reviews = $this->model->getReviewDetails($escort_id);
			break;
			case Cubix_LatestActions::ACTION_NEW_COMMENT:
				$view_script = "comments";
				$this->view->comments = $this->model->getCommentDetails($escort_id);
			break;
		}
		
		$this->_helper->viewRenderer->setScriptAction("details/" . $view_script);
	}

	public function ajaxHeaderAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$this->view->defines = Zend_Registry::get('defines');
		//$this->view->cities = $this->model->getAllCitiesForFilter();

		/*$this->view->city = $city = (int)$req->city;
		$this->view->country = $country = (int)$req->country;*/
		$this->view->action_type = $action_type = (int)$req->action_type;

		$filter = array();

		$action = (int)$req->action_type;

		/*if ( $city ) {
			$filter['city_id'] = $city;
		}

		if ( $country ) {
			$filter['country_id'] = $country;
		}*/

		if ( $action ) {
			$filter['action_type'] = $action;
		}

		$this->view->action_counts = $this->model->getActionCounts($filter);
	}
}