<?php

class CommentsController extends Zend_Controller_Action
{
    private $filtered_location;

	public function init()
	{

	    $this->view->cache = Zend_Registry::get('cache');

        //$this->view->layout()->disableLayout();
		$this->model = new Model_Comments();
        $this->location_session = @new Zend_Session_Namespace('last-filtered-location');

        // Check maybe user comes from a page, where he made some filtering and selected a country / city
        // if he didnt then we need to check, maybe we got his geo data. If yes, then fill
        // that in the search bar.
        // ---------------------------------------
        if (empty($this->location_session->data)) {
            $geoData = Cubix_Geoip::getClientLocation();

            if (!empty($geoData['country'])) {
                $modelCities = new Model_Cities();
                $geo_city = $this->view->geo_city =  $modelCities->getByGeoIp();
                $this->location_session->data = $this->filtered_location = (array)$geo_city;
            }
        }
        // ---------------------------------------

        $this->view->filtered_location = $this->filtered_location = $this->location_session->data;

    }

    public function indexAction()
    {
        // Asked By Marian
        // ---------------------------------------
        header('HTTP/1.1 301 Moved Permanently');
        header('Location: /');
        die;
        // ---------------------------------------
        $this->view->layout()->setLayout('main');

        $this->view->page = $page = 1;
        $this->view->p_top_category = 'comments';

        $config = Zend_Registry::get('reviews_config');
        $this->view->per_page = $per_page = $config['commentsPerPage'];

        $filter = array();

        $comments = $this->model->getLatestComments($page, $per_page, $filter);

        $count = $comments['count'];

        unset($comments['count']);
        $resComments = array();

        if ($comments)
        {
            foreach ($comments as $k => $comment)
            {
                $resComments[$k]['comments'] = $comment;

                if (isset($comment[0]))
                {
                    $data['hash'] = $comment[0]['hash'];
                    $data['ext'] = $comment[0]['ext'];
                    $data['escort_id'] = $k;
                    $data['application_id'] = Cubix_Application::getId();

                    $photo_url = new Model_Escort_PhotoItem($data);

                    $resComments[$k]['escort']['photo'] = $photo_url->getUrl('lcthumb_v3');
                    $resComments[$k]['escort']['showname'] = $comment[0]['showname'];
                    $resComments[$k]['escort']['escort_id'] = $comment[0]['escort_id'];
                    $resComments[$k]['escort']['city_title'] = $comment[0]['city_title'];
                    $resComments[$k]['escort']['country_title'] = $comment[0]['country_title'];
                }
            }
        }

        $this->view->comments = $resComments;
        $this->view->count = $count;
    }

    public function ajaxListAction()
    {
        $request = $this->_request;

        $page = intval($request->page);
        if ($page < 1) $page = 1;
        $this->view->page = $page;


        $config = Zend_Registry::get('reviews_config');
        $per_page_def = 10; //$config['commentsPerPage'];
        $per_page = $this->_getParam('per_page', $per_page_def);
        $this->view->per_page = $per_page;

        $filter = array();

        $filter['country_id'] = intval($request->country_id);
        $filter['city_id'] = empty($request->city_id) ? null : intval($request->city_id);

        if(empty($filter['city_id']) && empty($filter['country_id'])) {
            $filter['country_id'] = $this->filtered_location['country_id'];
            $filter['city_id'] = $this->filtered_location['city_id'];
        } else {
            $this->filtered_location['country_id'] = $filter['country_id'];
            $this->filtered_location['city_id'] = $filter['city_id'];
        }

        $this->location_session->data = $this->view->filtered_location = $this->filtered_location;

        $filter['showname'] = $request->showname;
        $filter['member'] = $request->member;
        $filter['date_added_f'] = intval($request->date_added_f);
        $filter['date_added_t'] = intval($request->date_added_t);

        $region = is_array($request->region_id) ? array_filter($request->region_id, function($value) { return !empty($value); }) : null;
        $zone = is_array($request->zone_id) ? array_filter($request->zone_id, function($value) { return !empty($value); }) : null;

        // Check if user didnt make ANY filter, then comes another check xD
        // maybe we stored the location, that he searched before or we found it's geo data
        // ------------------------------------------
        if(empty($request->country_id) && empty($request->city_id) && empty($region) && empty($zone) && !$this->_request->isXmlHttpRequest()) {
            if(empty($filter['country_id'])) {
                $filter['country_id'] = $this->view->filtered_location['country_id'];
            }

            if(empty($filter['city_id'])) {
                $filter['city_id'] = $this->view->filtered_location['city_id'];
            }
        }
        // ------------------------------------------

        if ($request->gender)
        {
            if (is_array($request->gender))
                $filter['gender'] = implode(',', $request->gender);
            else
                $filter['gender'] = $request->gender;
        }
        else
            $filter['gender'] = null;

        $comments = $this->model->getLatestComments($page, $per_page, $filter);

        $count = $comments['count'];
        unset($comments['count']);
        $resComments = array();

        if ($comments)
        {
            foreach ($comments as $k => $comment)
            {
                $resComments[$k]['comments'] = $comment;

                if (isset($comment[0]))
                {
                    $data['hash'] = $comment[0]['hash'];
                    $data['ext'] = $comment[0]['ext'];
                    $data['escort_id'] = $k;
                    $data['application_id'] = Cubix_Application::getId();

                    $photo_url = new Model_Escort_PhotoItem($data);

                    $resComments[$k]['escort']['photo'] = $photo_url->getUrl('lcthumb_v3');
                    $resComments[$k]['escort']['showname'] = $comment[0]['showname'];
                    $resComments[$k]['escort']['escort_id'] = $comment[0]['escort_id'];
                    $resComments[$k]['escort']['city_title'] = $comment[0]['city_title'];
                    $resComments[$k]['escort']['country_title'] = $comment[0]['country_title'];
                }
            }
        }

        $this->view->comments = $resComments;
        $this->view->count = $count;

        if($this->getRequest()->isXmlHttpRequest()){
            $this->view->layout()->disableLayout();
            $this->_helper->viewRenderer->setScriptAction('list');
        }
    }

    public function getEscortCommentsAction()
    {
        $escort_id = $this->_getParam('escort_id');
        $req = $this->_request;
        $page = $req->page;

        if (!$page)
            $page = 1;

        $per_page = 5;
        $resComments = array();
        $count = 0;
        $comments = $this->model->getEscortLatestComments($escort_id, $page, $per_page);

        if ($comments)
        {
            $resComments['comments'] = $comments['comments'];
            $count = $comments['count'];
        }

        $this->view->comments = $resComments;
        $this->view->page = $page;
        $this->view->count = $count;
        $this->view->escort_id = $escort_id;
        $this->view->per_page = $per_page;
    }

    public function profileCommentsAction()
    {
        $this->view->layout()->disableLayout();

        $this->view->escort_id = $escort_id = intval($this->_request->escort_id);
        $PmIsBlockedByEscort = Cubix_Api_XmlRpc_Client::getInstance()->call('Users.getPmIsBlocked', array($escort_id, 'escort'));
        $this->view->PmIsBlocked = !$PmIsBlockedByEscort && $this->user->pm_is_blocked;
        $allowRevCom = Cubix_Api_XmlRpc_Client::getInstance()->call('Reviews.allowRevCom', array($escort_id));
        if ($allowRevCom['disabled_comments']){die;}
        $page = intval($this->_request->page);
        if ($page < 1) $page = 1;
        $this->view->page = $page;

        $this->view->per_page = $per_page = 1;

        $comments = $this->model->getEscortComments($page, $per_page, $count, $escort_id);

        $this->view->comments = $comments;
        $this->view->comments_count = $count;
    }

    public function profileMemberCommentsAction()
    {
        $this->view->layout()->disableLayout();

        $this->view->user_id = $user_id = $this->_request->user_id;
        $this->view->username = $username = $this->_request->username;

        $page = intval($this->_request->page);
        if ($page < 1) $page = 1;
        $this->view->page = $page;

        $this->view->per_page = $per_page = 1;

        $comments = $this->model->getMemberComments($page, $per_page, $count, $user_id);

        $this->view->comments = $comments;
        $this->view->comments_count = $count;
    }

    public function profileAgencyCommentsAction()
    {
        $this->view->layout()->disableLayout();

        $this->view->agency_id = $agency_id = $this->_request->agency_id;

        $page = intval($this->_request->page);
        if ($page < 1) $page = 1;
        $this->view->page = $page;

        $this->view->per_page = $per_page = 1;

        $comments = $this->model->getAgencyComments($page, $per_page, $count, $agency_id);

        $this->view->comments = $comments;
        $this->view->comments_count = $count;
    }

    /*******************************/

	public function ajaxShowAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$config = Zend_Registry::get('escorts_config');
		$per_page = $config['comments']['perPage'];

		$page = $req->page;
		if ( ! $page )
			$page = 1;

		$escort_id = intval($req->escort_id);
		$city = Model_EscortsV2::getCityByEscortId($escort_id);

		$count = 0;
		$comments = array();
		
		$comments = $this->model->getEscortComments($page, $per_page, $count, $escort_id);

		foreach( $comments as $k => $comment ) {
			$comments[$k]->insert_date = $this->getCommentTime($comment->time);
		}

		$this->view->comments = $comments;
		$this->view->page = $page;
		$this->view->count = $count;
		$this->view->per_page = $per_page;

		$this->view->escort_id = $escort_id;
		$this->view->city = $city;
	}

	protected function getCommentTime($insertdate)
	{
        $tm = explode(" ", $insertdate);
        $days = explode("-", $tm[0]);
        $hours = explode(":", $tm[1]);
        
        $now = time();
		$u_insertdate = strtotime($insertdate);
		$inserted = mktime(date("H", $u_insertdate), date('i', $u_insertdate), date("s", $u_insertdate), date("m", $u_insertdate), date("d", $u_insertdate), date('Y', $u_insertdate));
		//$inserted = mktime($hours[0],$hours[1],$hours[2],$days[1],$days[2],$days[0]);
        $diff = $now - $inserted;
		
        $activedays = abs(floor($diff / 86400));
        $activehours = abs(floor(($diff % 86400) / 3600));
        $activeminutes = abs(floor((($diff % 86400) % 3600) / 60));

        if(date("m-d-Y", strtotime($insertdate)) == date("m-d-Y"))
        {
        	$rslt = Cubix_I18n::translate('today');
        }

        if ($activedays >= 2) {
           //$rslt = str_replace('%TIME%', $activedays . " " . ((1 < $activedays)? $LNG['days'] : $LNG['day']), $LNG['time_ago']);
			if ( $activedays > 1 )
				$rslt = Cubix_I18n::translate('days_ago', array('DAYS' => $activedays));
			else
				$rslt = Cubix_I18n::translate('day_ago', array('DAYS' => $activedays));
        }
        elseif (0 < $activedays && 2 > $activedays) {
            $rslt = Cubix_I18n::translate('yesterday');
        }
        else {
            if (0 < $activehours) {
                //$rslt = str_replace('%TIME%', $activehours . " " . ((1 < $activehours)? $LNG['hours'] : $LNG['hour']) . " " . $activeminutes . "  " . ((1 < $activeminutes)? strtolower($LNG['minutes']) : strtolower($LNG['minute'])), $LNG['time_ago']);
                //$rslt = str_replace('%TIME%', $activehours . " " . ((1 < $activehours)? $LNG['hours'] : $LNG['hour']) . " " . $activeminutes . "  " . ((1 < $activeminutes)? strtolower($LNG['minutes']) : strtolower($LNG['minute'])), $LNG['time_ago']);
				if ( $activehours > 1 )
					$rslt = Cubix_I18n::translate('hours_ago', array('HOURS' => $activehours));
				else
					$rslt = Cubix_I18n::translate('hour_ago', array('HOURS' => $activehours));
            }
            else {
                //$rslt = str_replace('%TIME%', $activeminutes . " " . ((1 < $activeminutes)? $LNG['minutes'] : $LNG['minute']), $LNG['time_ago']);
				if ( $activeminutes > 1 )
					$rslt = Cubix_I18n::translate('minutes_ago', array('MINUTES' => $activeminutes));
				else
					$rslt = Cubix_I18n::translate('minute_ago', array('MINUTES' => $activeminutes));
            }
        }

        return $rslt;
    }

	public function ajaxAddCommentAction()
	{
        $this->_helper->layout->disableLayout();
        $config = Zend_Registry::get('escorts_config');
		$textLength = $config['comments']['textLength'];

		$req = $this->_request;

		$this->view->escort_id = $escort_id = intval($req->escort_id);
		$this->view->user_id = $user_id = intval($req->user_id);
		
		$this->view->comment_id = $comment_id = intval($req->comment_id);
		$current_user = Model_Users::getCurrent();
		$this->view->user_type  =  $user_type = $current_user->user_type;
		
		/*if ( $comment_id ) {
			$comment = $this->model->getComment($comment_id);
		}*/

		$comment = $req->comment;
		$g_recaptcha = $this->_getParam('g-recaptcha-response');

		if ( $req->isPost() ) {

			$session = new Zend_Session_Namespace('captcha');
			$blackListModel = new Model_BlacklistedWords();
			$errors = array();

			if ( empty($g_recaptcha) ) {
				$errors[] = __('captcha_required');
			}

			if ( strlen($comment) < $textLength ) {
				$errors[] =  __('comment_is_short',array('LENGTH' => $textLength));
			}

			/*if ( $comment_id && ( $comment_id == $comment->id ) ) {
				$errors[] = 'cannot_reply';
			}*/

			if ( count($errors) > 0 ) {
				die(json_encode($errors));
			}
			else {
				$data = array(
					'user_id' => $user_id,
					'escort_id' => $escort_id,
					'status' => Model_Comments::COMMENT_NOT_APPROVED,
					'message' => $comment,
					'user_type' => $user_type
				);

				if ( $comment_id ) {
					$data['is_reply_to'] = $comment_id;
				}
				
				if($blackListModel->checkWords($data['message'])){
					
					$data['has_bl'] = 1;
				}
				
				// ip
				if ($_SERVER["HTTP_X_FORWARDED_FOR"] != "")
				{
					// for proxy
					$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
				}
				else
				{
					// for normal user
					$ip = $_SERVER["REMOTE_ADDR"];
				}
				
				$data['ip'] = $ip;
				//
				$comment = $this->model->addComment($data);
                die(json_encode([
                    'status' => 'success',
                    'comment' => $comment
                ]));
            }
		}
	}

	public function ajaxVoteAction()
	{
		$current_user = Model_Users::getCurrent();
		$req = $this->_request;

		if ($current_user && $current_user->isMember() ) {
			$comment_id = intval($req->comment_id);
			$escort_id = intval($req->escort_id);
			$type = $req->type;

			$cook_name = "v2_is_escort_comment_vote_" . $escort_id . '_' . $comment_id;
			$cook_value = array("comment_added_" . $comment_id . '_' . $escort_id);

			if ( ! isset( $_COOKIE[$cook_name]) ) {
				setcookie($cook_name, serialize($cook_value), strtotime('+1 year'), "/", 'www.' . Cubix_Application::getById(Cubix_Application::getId())->host);

				$this->model->vote($type, $comment_id);
				die;
			}
			else
			{
				//$this->view->alredy_voted = true;
				die('voted');
			}
		}
		else {
			$this->view->not_loged_in = true;
		}
	}
}
