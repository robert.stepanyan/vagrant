<?php
class IndexController extends Zend_Controller_Action
{
    public function linksAction()
    {
//        $this->view->layout()->setLayout('main');

//        if (!is_null($this->_getParam('ajax'))) {
//            $side_bar = $this->view->render('escorts/side-bar-countries.phtml');
//            $body = $this->view->render('index/links.phtml');
//            die(json_encode(array('head_title' => 'Links', 'side_bar' => $side_bar, 'body' => $body)));
//        }
    }

    public function robotsAction()
    {
        $this->_request->setParam('no_tidy', true);

        $this->view->layout()->disableLayout();
        $model = new Model_Robots();

        $robots = $model->get();

        header('Content-Type: text/plain; charset=UTF-8', true);
        echo $robots;
        die;
    }

    public function getEscortsViewCountAction()
    {
        $this->view->layout()->disableLayout();
        $escort_id = $this->view->escort_id = (int) $this->_request->escort_id;
    }

    /*public function goAction()
    {
        $link = $_GET['link'];

        //insert in table for statistics
        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $client->call('Escorts.insertStatisticsRow', array('id' => (int)$_GET['id'], 'user_type' => $_GET['user_type'], 'link' => $_GET['link']));
        //
        if ( ! preg_match('#^http(s)?://#', $link) ) $link = 'http://' . $link;

        header('Location: ' . $link);
        die;
    }*/

    public function getZoneCityCountryAction(){
        $cache_key = 'dev_zone_city_cointry_list_' . Cubix_I18n::getLang();
        $cache = Zend_Registry::get('cache');


        if(!$ZoneCityCountryList = $cache->load($cache_key)){
//            $Countries = Model_Statistics::getCountries();
//            $Cities = Model_Statistics::getCities();
//            $Regions = Model_Statistics::getRegions();
//            $Zones = Model_Statistics::getZones();
            $ZoneCityCountryList = Model_Statistics::getZoneCityCountry();
            $cache->save($ZoneCityCountryList,$cache_key,array());
        }

        die(json_encode($ZoneCityCountryList));
    }

    public function ajaxPlayVideoAction()
    {

        $this->view->layout()->disableLayout();
        $escort_id = $this->_request->escort_id;
        $video_id = $this->_request->video_id;

        if($video_id == 0 ){
            echo '[{"default":false,"file":"https:\/\/video.escortdirectory.com\/escortdirectory.com\/53238\/589b0dfbbf491_480p.mp4","label":"480p","width":848,"height":480},{"default":true,"file":"https:\/\/video.escortdirectory.com\/escortdirectory.com\/53238\/589b0dfbbf491_360p.mp4","label":"360p","width":636,"height":360}]';die;
        }
        $video_model = new Model_Video();
        $video = $video_model->getManifestData($escort_id, $video_id);
        $dimensions = $video_model->dimensions;
        $configs = Zend_Registry::get('videos_config');
        $path = $configs['remoteUrl'].'escortdirectory.com/'.$escort_id.'/'.$video->video;
        $qualities = array();

        foreach($dimensions as $dimension){
            if($video->height >= $dimension['height']){
                $width = $video->width / $video->height * $dimension['height'];

                $qualities[] = array(
                    'default' => $dimension['height'] == 360 ? true : false,
                    'file' => $path.'_'.$dimension['height'].'p.mp4',
                    'label'=> $dimension['height'].'p',
                    'width' => $width,
                    'height' => $dimension['height']
                );
            }
        }

        die(json_encode($qualities));
    }

    public function jsInitAction()
    {
        $this->view->layout()->disableLayout();
        $req = $this->_request;

        $view = $this->view->act = $req->view;

        //$req->setParam('lang_id', $req->lang);

        $lang = $this->view->lang = $req->lang;

        if ( $view == 'main-page' ) {

        }
        else if ( $view == 'main-simple' ) {

        }
        else if ( $view == 'main' ) {
            $this->view->filter_params = $req->filter_params;
            $this->view->is_upcomingtour = $req->is_upcomingtour;
            $this->view->is_tour = $req->is_tour;
            $this->view->p_top_category = $req->p_top_category;
        }
        else if ( $view == 'private' ) {

        }
        else if ( $view == 'private-v2' ) {

        }
        else if ( $view == 'profile' ) {
            $this->view->show_follow_success = $req->follow_success;
        }
        else if ( $view == 'profile-v2' ) {

        }
        else if ( $view == 'agency' ) {
            $this->view->show_follow_success = $req->follow_success;
        }

        /*header('Content-type: text/javascript');
        $expires = 60*60*24*14;
        header("Pragma: public");
        header("Cache-Control: maxage=" . $expires);
        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');*/

    }

    private function _hardcode_banner_positions(& $rows)
    {
        $banners = [];

        foreach ($rows as $row)
        {
            // new jira EDIR-89
            if( isset($this->_request->country) && $row->banner_id == 4288 && in_array($this->_request->country, array(23, 33, 66, 4, 67))){
                $banners[] = $row;
            }
            elseif($row->type == 'fixed') {
                $index = $row->frozen_position;

                if (!isset($banners[$index])) {
                    $banners[$index] = $row; // Hardcode at index that was set from backend
                } else {
                    // If something was already at that index
                    // push it to the end of array, to not overwrite it
                    // -----------------------
                    $tmp = $banners[$index];
                    $banners[$index] = $row;
                    $banners[] = $tmp;
                    // -----------------------
                }
            }

            else {
                // If no hardcode position was set from backend for this banner
                // then it will be automatically at the end
                // --------------------------------
                $banners[] = $row;
                // --------------------------------
            }
        }
        return $banners;
    }

    public function fetchGovazdsAction()
    {
        $geoData = Cubix_Geoip::getClientLocation();
        $page = intval($this->_request->page);
        $countryId = intval($this->_request->country);
        $cityId = intval($this->_request->city);
        $bannersCount = intval($this->_request->bannersCount);
        $region = intval($this->_request->region);

        if(!empty($cityId)) {
            $modelCity = new Model_Cities();
            $city = $modelCity->getById($cityId);
            $countryId = $city->country_id;
        }

        $model = new Model_ixsBanners();
        $extraSuperDuperPremiumBannersWooHoo = [];
        $exclude = [];

        if ($countryId == 68) {
            if (Zend_Registry::get('isMobile')) {
                $extraSuperDuperPremiumBannersWooHoo[] = '4205';
                $exclude[] = '4206';
            } else {
                $extraSuperDuperPremiumBannersWooHoo[] = '4206';
                $exclude[] = '4205';
            }
        }

        //NOTE - 4392 EDIR-219 banner IXS_ID
//        $isAmsterdam = ($countryId == 27 && $cityId == 131) || strtolower($geoData['city']) == 'amsterdam';
//        if ($isAmsterdam){
//            $extraSuperDuperPremiumBannersWooHoo[] = '4392';
//        }

        $extraSuperDuperPremiumBannersWooHoo[] = '4066';
        $rows = $model->getListingBanners($page, $countryId, $cityId, $extraSuperDuperPremiumBannersWooHoo, $bannersCount, $exclude, $region);
        $banners = $this->_hardcode_banner_positions($rows);

        echo json_encode([
            'items' => $banners,
        ]);
        die;
    }

    public function subscribeAction()
    {
        if ( $this->_request->isPost() ) {
            $email = $this->_request->email;
            if ( null === $email || !strlen($email) ) {
                echo json_encode(['msgs' => ['email' => 'Email is required'], 'status' => 'error']);
                die;
            }
            elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $email) ) {
                echo json_encode(['msgs' => ['email' => 'Wrong email format'], 'status' => 'error']);
                die;
            }
            $config = Zend_Registry::get('newsman_config');

            if ($config)
            {
                $list_id = $config['list_id'];
                $segment = $config['member'];
                $nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);

                $subscriber_id = $nm->subscribe($list_id, $email, '');
                $is_added = $nm->bindToSegment($subscriber_id, $segment);
            }
            echo json_encode(['msgs' => ['email' => 'Added'], 'status' => 'success']);
            die;
        } else{

            $this->view->layout()->disableLayout();
            $this->_helper->viewRenderer('index/subscribe', null, true);
        }
    }


    public function citySearchLogAction()
    {
        $this->view->layout()->disableLayout();
        if ( !$this->_request->isPost() ) die;

        $model = new Model_SearchLog();

        $model->add($this->_request->getPost());
        die;

    }
}