<?php

class AgenciesController extends Zend_Controller_Action
{
    public static $linkHelper;

    public function init()
    {
        self::$linkHelper = $this->view->getHelper('GetLink');
        $this->view->cache = Zend_Registry::get('cache');
    }

    public function indexAction()
    {

    }

    public function listAction()
    {

        if ($this->_request->getParam('pushstate_filters')) {
            Model_Escort_List::convertPushState($this->_getParam('pushstate_filters'));
        }

        $this->view->p_id = $this->_getParam('p_id');

        $this->view->static_page_key = 'agencies';
        if ($this->_getParam('filter') == '2') {
            $this->view->static_page_key = 'bdsm-agencies';
        } elseif ($this->_getParam('filter') == '3') {
            $this->view->static_page_key = 'massage-agencies';
        }

        $list_types = array('list');
        $list_type_a = $this->_getCookie('list_type_a');
        if ($list_type_a) {
            $list_type = $list_type_a;

            if (!in_array($list_type, $list_types)) {
                $list_type = $list_types[0];
            }

            $this->view->render_list_type = $list_type;
        } else {
            $list_type = $list_types[0];
            $this->view->render_list_type = $list_types[0];
        }

        //Currency rates log
        $default_currency = "USD";
        $current_currency = $this->_getParam('currency', $default_currency);
        $Currency = $this->_getCookie('currency');
        if ($current_currency != $Currency && $this->_getParam('s')) {
            setcookie("currency", $current_currency, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $this->_request->getServer('HTTP_HOST')));
            $_COOKIE['currency'] = $current_currency;
        }
        $CurrencyCookie = $this->_getCookie('currency');
        if (!empty($CurrencyCookie)) {
            $current_currency = $this->_getCookie('currency');;
            $this->_setParam('currency', $current_currency);
        }

        $this->view->current_currency = $current_currency;
        //Currency rates log

        $default_sorting = "alpha";
        $current_sorting = $this->_getParam('agency_sort', $default_sorting);

        $sort_map = array(
            'newest' => 'date_registered DESC',
            'last_modified' => 'cd.last_modified DESC',
            'last-connection' => 'refresh_date DESC',
            'close-to-me' => 'cd.id ASC',
            'most-viewed' => 'hit_count DESC',
            'random' => 'RAND()',
            'by-country' => 'cr.title_en ASC',
            'by-city' => 'ct.title_en ASC',
            'alpha' => 'cd.club_name ASC',
        );

        if (!array_key_exists($current_sorting, $sort_map)) {
            $current_sorting = $default_sorting;
            $this->_setParam('agency_sort', $default_sorting);
        }
        $AgencySort = $this->_getCookie('agency_sort');
        if ($current_sorting != $AgencySort && $this->_getParam('s')) {
            setcookie("a_sorting", $current_sorting, strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            setcookie("a_sortingMap", $sort_map[$current_sorting]['map'], strtotime('+1 year'), "/", "www." . preg_replace('#www.|dev.#', '', $_SERVER['HTTP_HOST']));
            $_COOKIE['a_sorting'] = $current_sorting;
            $_COOKIE['a_sortingMap'] = $sort_map[$current_sorting]['map'];
        }
        $ASortingCookie = $this->_getCookie('a_sorting');
        if (!empty($ASortingCookie)) {
            $current_sorting = $this->_getCookie('a_sorting');
            $this->_setParam('agency_sort', $current_sorting);
        }

        if ($current_sorting) {
            $tmp = $sort_map[$current_sorting];
            unset($sort_map[$current_sorting]);
            $sort_map = array_merge(array($current_sorting => $tmp), $sort_map);
        }

        if ($current_sorting) {
            $sorting = $current_sorting;
        }

        $this->view->sort_map = $sort_map;
        $this->view->current_sort = $current_sorting;


        $p_top_category = $this->view->p_top_category = $this->_getParam('p_top_category', 'agencies');

        $p_city_id = $this->view->city = (int)$this->_getParam('city_id');
        $online_now = $this->view->online_now = (int)$this->_getParam('online_now');
        $p_country_id = $this->view->country = (int)$this->_getParam('country_id');
        $p_region_id = $this->view->region_id = (int)$this->_getParam('region_id');
        $p_zone_id = $this->view->zone_id = (int)$this->_getParam('zone_id');
        $agency = $this->view->agency = $this->_getParam('agency');
        $this->view->lookingFor = 'agencies';
        $this->view->interestedIn = ESCORT_TYPE_ESCORT;
        if ($p_top_category == 'massage-agencies'){
            $this->view->interestedIn = ESCORT_TYPE_MASSAGE;
        }
        elseif ($p_top_category == 'bdsm-agencies'){
            $this->view->interestedIn = ESCORT_TYPE_BDSM;
        }

        if ($p_zone_id){
            $this->view->locationTitle = Model_Statistics::getLocationTitle($p_zone_id, 'zone');
        }elseif ($p_city_id){
            $this->view->locationTitle = Model_Statistics::getLocationTitle($p_city_id, 'city');
        }elseif ($p_region_id){
            $this->view->locationTitle = Model_Statistics::getLocationTitle($p_region_id, 'region');
        }elseif ($p_country_id){
            $this->view->locationTitle = Model_Statistics::getLocationTitle($p_country_id, 'country');
        }
        $this->view->is_active_filter = false;

        if ($p_city_id || $p_country_id || $agency)
            $this->view->is_active_filter = true;

        $page = intval($this->_getParam('page'));

        if ($page == 0) {
            $page = 1;
        }

        $e_config = Zend_Registry::get('escorts_config');

        $per_page_def = $e_config['agencyPerPage'];

        $per_page = $this->_getParam('per_page', $per_page_def);

        $this->view->per_page = $per_page;

        $geoData = array();

        if ($sorting == 'close-to-me') {
            $geoData = Cubix_Geoip::getClientLocation();
        }

        $m_agencies = new Model_Agencies();

        $count = 0;

        $agency_has_esc_type = 1;

        if ($this->_getParam('filter')) {
            $agency_has_esc_type = $this->_getParam('filter');
        }


        $lng = Cubix_I18n::getLang();
        $this->client = new Cubix_Api_XmlRpc_Client();
        $getCountriesHavingAgency = $this->client->call('Agencies.getCountriesHavingAgency', array($lng));

        $agencies = $m_agencies->getAll($page, $per_page, $p_city_id, $sorting, $p_country_id, $count, $list_type, $geoData, $online_now, $agency, $agency_has_esc_type, $p_zone_id, $p_region_id);

        $this->view->count = $count;
        $this->view->page = $page;
        $this->view->per_page = $per_page;
        $this->view->agencies = $agencies;
        $this->view->countries = $getCountriesHavingAgency;

        $this->_request;
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->view->is_ajax = true;
            $this->view->layout()->disableLayout();
            $escort_list = $this->view->render('agencies/ajax-list.phtml');
            if ($this->getRequest()->getHeader('X-PJAX', false)){
                die($escort_list);
            }
            die(json_encode(array('escort_list' => $escort_list)));
        }
    }

    public function showAction()
    {
        $this->_helper->viewRenderer->setScriptAction("profile/profile");

        $agency_id = (int)$this->_getParam('agency_id');

        $m_agencies = new Model_Agencies();

        $agency = $this->view->agency = $m_agencies->getLocalById($agency_id);

        if ($agency) {
            $params = array('e.agency_id = ?' => $agency_id);
            $follow_model = new Model_Follow();
            $escorts_count = 0;
            $this->view->a_page = $page = 1;
            $allowRevCom = Cubix_Api_XmlRpc_Client::getInstance()->call('Reviews.allowRevCom', array($agency->user_id, 'u'));
            $this->view->disabled_comm = $allowRevCom['disabled_comments'];
            $this->view->disabled_follow = Cubix_Api_XmlRpc_Client::getInstance()->call('Agencies.getField', array($agency->id, 'disabled_follow'));;
            $this->view->a_per_page = $per_page = 6;
            $this->view->a_escorts = Model_Escort_List::getAgencyEscorts($params, 'newest', $page, $per_page, $escorts_count);
            $this->view->a_count = $escorts_count;
            $this->view->is_followed = $follow_model->checkIsFollowed("agency", $agency_id);
            $this->view->followers_count = $follow_model->getCount("agency", $agency_id);
            $this->view->is_agency = true;

        } else {
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: " . $this->view->getLink('navigation-link', array('top_category' => 'agencies')));
        }
    }

    public function escortsAction()
    {
        $this->_helper->viewRenderer->setScriptAction("profile/escorts");

        $agency_id = (int)$this->_getParam('agency_id');

        $m_agencies = new Model_Agencies();

        $agency = $this->view->agency = $m_agencies->getLocalById($agency_id);

        if ($agency) {
            $params = array('e.agency_id = ?' => $agency_id);

            $escorts_count = 0;
            $this->view->a_page = $page = 1;
            $this->view->a_per_page = $per_page = 1000;
            $this->view->a_escorts = Model_Escort_List::getAgencyEscorts($params, 'newest', $page, $per_page, $escorts_count);
            $this->view->a_count = $escorts_count;

        } else {
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: " . $this->view->getLink('navigation-link', array('top_category' => 'agencies')));
        }
    }

    public function ajaxEscortsAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setScriptAction("profile/ajax-escorts");

        $agency_id = (int)$this->_getParam('agency_id');

        $page = (int)$this->_getParam('page');
        if ($page < 1) $page = 1;
        $this->view->a_page = $page;
        $this->view->a_per_page = $per_page = 36;

        $params = array('e.agency_id = ?' => $agency_id);
        $this->view->a_escorts = Model_Escort_List::getAgencyEscorts($params, 'newest', $page, $per_page, $escorts_count);
        $this->view->a_count = $escorts_count;
    }

    /**
     * @param $cookieName
     * @return null|string
     */
    protected function _getCookie($cookieName)
    {
        return $this->getRequest()->getCookie($cookieName);
    }

    public function getCountriesHavingAgencyAction() {

        $lng = Cubix_I18n::getLang();
        $this->client = new Cubix_Api_XmlRpc_Client();
        $getCountriesHavingAgency = $this->client->call('Agencies.getCountriesHavingAgency', array($lng));

        echo json_encode(array(
            'data' => $getCountriesHavingAgency
        ));
        die;
    }
}
