<?php

class BlogController extends Zend_Controller_Action
{
    /**
     * @var Model_Blog
     */
	protected $model;
	public function init()
	{
		$this->model = new Model_Blog();
		$this->blog_config = Zend_Registry::get('blog_config');
	}

	public function indexAction()
	{
	    if($_GET['edo']) {
            $result = Model_Applications::clearCache();
            var_dump($result);die;
        }

		$this->view->topPosts = $this->model->getTopPostsForSlideshow(6);
		$this->view->special = $this->model->getSpecial();
		$this->view->categories = $this->model->getCategories();
		$this->view->dateDetails = $this->model->getDateDetails(9);
	}

    public function archiveAction()
    {
        $perPage = 9;
        $page    = intval($this->_request->page);
        if ($page < 1) $page = 1;
        $date                   = preg_replace('#\/blog\/#', '', $_SERVER['REQUEST_URI']);
        $date                   = explode('.', $date);
        $month                  = $date[0];
        $year                   = $date[1];
        $year                   = preg_replace('/[^\p{L}\p{N}\s]/u', '', $year);
        $this->view->year       = $year;
        $this->view->month      = $month;
        $this->view->categories = $this->model->getCategories();
        $this->view->dateDetails = $this->model->getDateDetails(9);
        $this->view->archive    = $this->model->archive($month, str_replace('/', '', $year));
    }
	
	public function ajaxPostsAction()
	{
		$homepage = intval($this->_request->homepage);

		if($homepage == 1){
			$this->view->layout()->disableLayout();
			$this->view->homepage = $homepage;
			$this->_helper->viewRenderer->setScriptAction('category');
			return $this->categoryAction();
		}

		$this->view->layout()->disableLayout();
		
		$page = intval($this->_request->page);
		$category_id = intval($this->_request->category_id);
		
		if ($page < 1)
			$page = 1;
		
		$this->view->page = $page;
		
		$perPage = 9; // intval($this->_request->perPage);
		$this->view->per_page = $perPage;

		$filter = array();
		
		if (trim($this->_request->search))
		{
			$filter['search'] = trim($this->_request->search);
			$this->view->search = true;
			$this->view->searchVal = $this->_request->search;
		}

		if($category_id){
			$filter['category_id'] = $category_id;
		}
		
		$ret = $this->model->getPosts($filter, $page, $perPage);
				
		$this->view->data = $ret['data'];
		$this->view->count = $ret['count'];

	}
	
	public function detailsAction()
	{
		$id = intval($this->_request->id);
		$slug = $this->_request->slug;
		

		$ret = $this->model->getPost($id, $slug);

		$this->view->category = $this->model->getCategory($ret['data']->category_id);

		$this->view->post = $ret['data'];
		$this->view->photos = $ret['photos'];

		$this->model->updateViewCount($id);

		/* more random posts */
		$ids = array($id);
		$count = 4;
		$this->view->data = $this->model->getRandomPosts(implode(',', $ids), $count, $ret['data']->category_id);
	}

	public function categoryAction()
	{
		$id = intval($this->_request->id);
		$slug = $this->_request->slug;
		
		$page = intval($this->_request->page);

		if ($page < 1)
			$page = 1;
		
		$per_page = intval($this->_request->per_page);

		if($per_page < 1)
			$per_page = 9;

		$this->view->page = $page;
		$this->view->per_page = $per_page;

		if (trim($this->_request->search))
		{
			$this->view->search = true;
			$this->view->searchVal = $this->_request->search;
		}

		if($id){
			$this->view->category = $this->model->getCategory($id);
		}
	}
	
	public function ajaxCommentsAction()
	{
		$this->view->layout()->disableLayout();
		
		$page = intval($this->_request->page);
		
		if ($page < 1)
			$page = 1;
		
		$this->view->page = $page;
		$this->view->page_repllies = 1;
		
		$this->view->per_page = $per_page = $this->blog_config['comments']['perPage'];
		$this->view->per_page_replies = $per_page_replies = $this->blog_config['replies']['perPage'];
		
		$post_id = intval($this->_request->post_id);
		
		$ret = $this->model->getComments($post_id, $page, $per_page, $per_page_replies);
		
		$this->view->comments = $ret['data'];
		$this->view->count = $ret['count'];
	}
	
	public function ajaxRepliesAction()
	{
		$this->view->layout()->disableLayout();
		
		$page = intval($this->_request->page);
		
		if ($page < 1)
			$page = 1;
		
		$this->view->page_repllies = $page;
		$this->view->per_page_replies = $per_page_replies = $this->blog_config['replies']['perPage'];
		
		$comment_id = intval($this->_request->comment_id);
		$post_id = $this->model->getPostIdByCommentId($comment_id);
		
		$this->view->replies = $this->model->getReplies($post_id, $comment_id, $page, $per_page_replies);
	}
	
	public function ajaxAddCommentAction()
	{
		$this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
		
		if ($this->_request->isPost())
		{
			$post_id = intval($this->_request->post_id);
			$name = trim($this->_request->name);
			$email = trim($this->_request->email);
			$comment = trim($this->_request->comment);
			$captche = $this->_request->captche;

			$session = new Zend_Session_Namespace('captcha');

			$error = array();
			$valid = new Cubix_Validator();
			
			if ( $session->captcha != strtolower(trim($captche)) ) {
				$error[] = 'captcha';
			}

			if (!$post_id)
				$error[] = 'post_id';

			if (!strlen($name))
				$error[] = 'name';

			if (!strlen($email) || !$valid->isValidEmail($email))
				$error[] = 'email';

			if (!strlen($comment))
				$error[] = 'comment';

			if (count($error))
				die(json_encode(array('error' => $error)));


			$ip = Cubix_Geoip::getIP();
			
			$data = array(
				'post_id' => $post_id,
				'comment' => $comment,
				'name' => $name,
				'email' => $email,
				'is_reply' => 0,
				'ip' => $ip
			);
			
			$this->model->addComment($data);
			
			die(json_encode(array('success' => TRUE)));
		}
	}
	
	public function ajaxAddReplyAction()
	{
		$this->view->layout()->disableLayout();
		
		$this->view->comment_id = $comment_id = intval($this->_request->comment_id);
		
		if (!$comment_id)
			die;
		
		if ($this->_request->isPost())
		{
			$name = trim($this->_request->name);
			$email = trim($this->_request->email);
			$comment = trim($this->_request->comment);

			$error = array();
			$valid = new Cubix_Validator();

			if (!strlen($name))
				$error[] = 'name';

			if (!strlen($email) || !$valid->isValidEmail($email))
				$error[] = 'email';

			if (!strlen($comment))
				$error[] = 'comment';

			if (count($error))
				die(json_encode(array('error' => $error)));
			
			if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if (isset($_SERVER['REMOTE_ADDR']))
                $ip = $_SERVER['REMOTE_ADDR'];
            else
                $ip = 'UNKNOWN';
			
			$post_id = $this->model->getPostIdByCommentId($comment_id);
			
			$data = array(
				'post_id' => $post_id,
				'comment' => $comment,
				'name' => $name,
				'email' => $email,
				'is_reply' => $comment_id,
				'ip' => $ip
			);
			
			$this->model->addComment($data);
			
			die(json_encode(array('success' => TRUE)));
		}
	}
}
