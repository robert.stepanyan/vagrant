<?php
class MyfollowersController extends Zend_Controller_Action {
	
	public function init() {
		$this->user = $this->view->user = Model_Users::getCurrent();
		
		if ( ! $this->user ) {

			if($this->getRequest()->isXmlHttpRequest()){
				header("HTTP/1.1 403 Forbidden");
			}else{
				$this->_redirect($this->view->getLink('signin'));
			}

			return;
		}
		
		$this->_model = new Model_Follow();
		$cache = Zend_Registry::get('cache');
		$cache_key =  'v2_user_pva_' . $this->user->id;
		
		if ( $this->user->isAgency() ) {
			
			if ( ! $agency = $cache->load($cache_key) ) {
				$agency = $this->user->getAgency();
				$cache->save($agency, $cache_key, array(), 300);
			}
			
			$this->type = 'agency';
			$this->agency = $agency;
			$this->type_id = $agency->id;
			
			$this->view->active_agency_escorts = $agency->getEscortsWithPackages();
			$this->view->all_agency_escorts = $agency->getEscorts();
			
			
			
		}
		else if ( $this->user->isEscort() ) {

			if ( ! $escort = $cache->load($cache_key) ) {
				$escort = $this->user->getEscort();
				$cache->save($escort, $cache_key, array(), 300);
			}
			$this->escort =  $escort;
			$this->type = 'escort';
			$this->type_id = $escort->id;
		}
		else{
			$this->_redirect($this->view->getLink('private-v2'));
		}
		
	}
	
	public function indexAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->countries = $this->_model->getCountries($this->type, $this->type_id);
				
	}
	
	public function getFollowersAction()
	{
		$this->view->layout()->disableLayout();
		
		if ($this->_request->isPost())
		{
			$form_data = new Cubix_Form_Data($this->_request);

			$fields = array(
				'username' => 'notags|special',
				'date_from' => 'int-nz',
				'date_to' => 'int-nz',
				'country_id' => 'int-nz',
				'city_id' => 'int-nz',
				'a_escort_id' => 'int-nz',
				'page' => 'int-nz',
				'sort_field' => 'notags|special',
				'sort_dir' => 'notags|special'
			);

			$form_data->setFields($fields);
			$data = $form_data->getData();

			if (!in_array($data['sort_field'], array('r.creation_date',	'r.meeting_date'))){
				$data['sort_field'] = 'fu.id';
			}

			if (!in_array($data['sort_dir'], array('asc', 'desc'))){
				$data['sort_dir'] = 'desc';
			}
		}
		else{
			$data = array();
		}
		
		if($this->user->isEscort()){
			$data['escort_id'] = $this->escort->id;
		}
		else{
			$data['agency_id'] = $this->agency->id;
		}

		$data['with_member_data'] = True;

        $followers_data = $this->_model->getFollowers($data);
        $members_model = new Model_Members();

		foreach($followers_data[0] as $k => &$v){
            $v['photo'] = $members_model->getLogoUrl($v['hash'], $v['ext'], $v['member_id'], 'thumb_cropper_ed');
		}

		$this->view->followers = $followers_data[0];
		$this->view->count = $followers_data[1];
		$this->view->per_page = 10;
		$this->view->page = $data['page'] ? $data['page'] : 1;
		
	}
	
	public function toggleFavoriteAction()
	{
		$is_favorite = intval($this->_getParam('is_favorite'));
		$follower_id =  intval($this->_getParam('follower_id'));
		$follow_model = new Model_Follow();
		$follow_model->toggleFavorite($this->type, $this->type_id, $follower_id, $is_favorite);
		die;
	}
	
	public function toggleNewsletterAction()
	{
		$newsletter = intval($this->_getParam('newsletter'));
		$follower_id =  intval($this->_getParam('follower_id'));
		$follow_model = new Model_Follow();
		$follow_model->toggleNewsletter($this->type, $this->type_id, $follower_id, $newsletter);
		die;
	}
	
	public function addCommentAction()
	{		
		$this->view->layout()->disableLayout();
		$this->view->follower_id = $follower_id = intval($this->_request->id);
		
		if (!$follower_id) die;

		if ($this->_request->isPost())
		{
			$form_data = new Cubix_Form_Data($this->_request);
		
			$fields = array(
				'id' => 'int-nz',
				'comment' => 'notags|special',
			);

			$form_data->setFields($fields);
			$data = $form_data->getData();
			$validator = new Cubix_Validator();

			if (strlen(trim($data['comment'])) == 0)
				$validator->setError('comment', 'Required');

			if ($validator->isValid())
			{
				$follow_model = new Model_Follow();
				$follow_model->addComment($this->type, $this->type_id, $data);
				echo json_encode(array('status' => 'ok'));
			}
			else
			{
				$status = $validator->getStatus();
				echo(json_encode($status));
			}

			die;
		}
	}
	
	public function getCitiesAction()
	{
		$country_id = intval($this->_request->id);
		$cities = $this->_model->getCities($this->type, $this->type_id, $country_id);
		die(json_encode($cities));
	}		
}