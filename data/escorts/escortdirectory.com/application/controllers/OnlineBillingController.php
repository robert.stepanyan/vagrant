<?php

class OnlineBillingController extends Zend_Controller_Action
{
    /**
     * @return Zend_Session_Namespace
     */
    protected static function _getSession()
    {
        return new Zend_Session_Namespace('advertise');
    }

    public function init() {
        self::_getSession()->setExpirationSeconds(60 * 20); // 20 minutes
    }

    public $packages = [
        'single' => [
            ['title' => 'vip', 'price' => 27, 'days' => 15, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 49, 'days' => 30, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 99, 'days' => 90, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 20, 'days' => 15, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 34, 'days' => 30, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 75, 'days' => 90, 'country_type' => 'B'],
            ['title' => 'premium', 'price' => 15, 'days' => 15, 'country_type' => 'A'],
            ['title' => 'premium', 'price' => 29, 'days' => 30, 'country_type' => 'A'],
            ['title' => 'premium', 'price' => 75, 'days' => 90, 'country_type' => 'A'],
            ['title' => 'premium', 'price' => 10, 'days' => 15, 'country_type' => 'B'],
            ['title' => 'premium', 'price' => 19, 'days' => 30, 'country_type' => 'B'],
            ['title' => 'premium', 'price' => 51, 'days' => 90, 'country_type' => 'B'],
        ],
        'bulk' => [
            ['title' => 'vip', 'price' => 80, 'days' => 15, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 140, 'days' => 30, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 250, 'days' => 90, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 59, 'days' => 15, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 95, 'days' => 30, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 200, 'days' => 90, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 159, 'days' => 15, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 275, 'days' => 30, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 580, 'days' => 90, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 117, 'days' => 15, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 185, 'days' => 30, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 380, 'days' => 90, 'country_type' => 'B'],
        ],
        'banner' => [
            ['title' => '30', 'price' => 50, 'days' => 30],
            ['title' => '90', 'price' => 120, 'days' => 90],
            ['title' => 'vip', 'price' => 30, 'days' => null ],
        ],
        'other' => [
            // female In MASSAGE
            ['title' => 'vip', 'price' => 25, 'days' => 30, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 48, 'days' => 90, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 20, 'days' => 30, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 60, 'days' => 90, 'country_type' => 'A'],
        ],
        'other_2' => [
//            male massage bdsm  and ts
            ['title' => 'vip', 'price' => 16, 'days' => 15, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 29, 'days' => 30, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 59, 'days' => 90, 'country_type' => 'A'],
            ['title' => 'vip', 'price' => 12, 'days' => 15, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 20, 'days' => 30, 'country_type' => 'B'],
            ['title' => 'vip', 'price' => 45, 'days' => 90, 'country_type' => 'B'],

            ['title' => 'premium', 'price' => 9, 'days' => 15, 'country_type' => 'A'],
            ['title' => 'premium', 'price' => 17, 'days' => 30, 'country_type' => 'A'],
            ['title' => 'premium', 'price' => 45, 'days' => 90, 'country_type' => 'A'],
            ['title' => 'premium', 'price' => 6, 'days' => 15, 'country_type' => 'B'],
            ['title' => 'premium', 'price' => 11, 'days' => 30, 'country_type' => 'B'],
            ['title' => 'premium', 'price' => 30, 'days' => 90, 'country_type' => 'B'],
        ]
    ];

    /*


    15 days - 16 eur
30 days – 29 eur
90 days – 59 eur
     */

    public function indexAction()
    {
    }

    public function advertiseAction()
    {

        $message = self::_getSession()->message;

        if(!empty($message)) {
            $this->view->message = $message;
            self::_getSession()->message = null;
        }

        $this->view->packages = $this->packages;

        if($this->_request->country){
            $this->view->city = 0;
            $this->view->country_id = $this->_request->country;
        }elseif($this->_request->city){
            $this->view->city = $this->_request->city;
            $this->view->country_id = (new Model_Countries)->getCountryIdByCityId($this->_request->city);
        }


    }

    public function generateMmgLinkAction()
    {
        $this->view->layout()->disableLayout();
        $request = $this->_request;

        if (!$this->_request->isPost() && !is_int($request->id)) die("Invalid Request");
        

        $mmgBill = new Model_MmgBillAPIV2();
        $client = new Cubix_Api_XmlRpc_Client();
        $id = $this->_getParam('id');
        $country_id = $this->_getParam('country');
        $city_id = $this->_getParam('city');
        $hash  = base_convert(time(), 10, 36);
        $this->user = Model_Users::getCurrent();

        $escort_id = 0;
        $agency_id = 0;
        $user_type  = 'not-logged';

        
        if($this->user){
            $user_id = $this->user->id;
            $user_type = $this->user->isAgency() ? USER_TYPE_AGENCY : USER_TYPE_SINGLE_GIRL;
            if($user_type == USER_TYPE_SINGLE_GIRL){
                $escort_id = $this->user->escort_data['escort_id'];
            }else{
                $agency_id = $this->user->agency_data['agency_id'];
            }
        }else{
            $user_id = 0;
        }

        
        

        $key = explode('-' , $id) [0];
        $index = explode('-' ,$id) [1];

        $package = $this->packages[$key][$index];

        $package['country'] = (new Model_Countries)->getCountryByID($country_id)->country_title;
        $data = array('advertise_page' => true, 'city_id' => $city_id, 'country' => $country_id, 'amount' => $package['price'], 'days' => $package['days'], 'package_title' => $package['title'], 'country_type' => $package['country_type'], 'package_id' => $id);

        $post_data[] = array(
            'user_id' => $user_id,
            'escort_id' => $escort_id,
            'agency_id' => $agency_id,
            'package_id'=> null,
            'data' => serialize($data),
            'hash' => $hash
        );

        if($city_id) {
            $package['city'] = (new Model_Cities)->getById($city_id)->title;
        }

        self::_getSession()->package = $package;

        if(APPLICATION_ENV == 'production') {
            $postback_url = 'http://www.escortdirectory.com';
        }else{
            $postback_url = 'http://www.escortdirectory.com.test';
        }


        $amount = $client->call(
            'OnlineBillingV2.addToShoppingCartED',
            array(
                $post_data,
                $user_id,
                $user_type,
                $hash
            )
        );

        $hosted_url = $mmgBill->getHostedPageUrl($package['price'], 'ADZ'.$hash, $postback_url.$this->view->getLink('advertise-mmg-response'));

        if (filter_var($hosted_url, FILTER_VALIDATE_URL) === FALSE) {
            $status = 'not-valid-url';
        }else{
            $status = 'success';
        }

        die(json_encode(array('status' => $status, 'url' =>  $hosted_url, 'package' => $package)));
    }

    public function advertiseMmgResponseAction()
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $request = $this->_request;
        $payment_result = [];
        
       
        if (isset($request->txn_status) and $request->txn_status == 'APPROVED') {

            $hash = str_ireplace('ADZ', '', $request->ti_mer);

            $shoping_card = $client->call( 'OnlineBillingV2.updateShoppingCartED', array( $hash ) );
            if(!is_array( $shoping_card)) return $this->_redirect('/advertise');

            $data = unserialize($shoping_card['data']);
          


            $payment_result['status'] = $request->txn_status;            
            $payment_result['merchant'] = $request->mid;
            $payment_result['mmg'] = $request->ti_mmg;
            $payment_result['ti_mer'] = $request->ti_mer;
            $payment_result['amount'] = $data['amount'];

            if ($request->mmg_errno != '0000')
                $payment_result['error'] = $request->mmg_errno;

        } else {
            $payment_result['status'] = 'REJECTED';
        }


        $this->view->payment_status = $payment_result['status'];
        $this->view->payment_result = $payment_result;
    }

    public function advertisePaymentEmailAction(){

        $request = $this->_request;

        $payment_result = (isset($request->payment_data) && !empty($request->payment_data)) ? unserialize($request->payment_data) : [];
        $payment_result['email'] = isset($request->email) ? $request->email : '- - -';
        $payment_result['Link'] = isset($request->link) ? $request->link : '- - -';

        $current_user = Model_Users::getCurrent();

        if(!empty($current_user)){

            if ( $current_user->user_type == 'agency' ) {
                $payment_result['User'] = $current_user->agency_data['agency_id'];
            }
            else if ( $current_user->user_type == 'escort' ) {
                $payment_result['User'] = $current_user->escort_data['escort_id'];
            }
        }else{
            $payment_result['User'] = 'Was not authenticated';
        }

        $payment_details_string = '<ul>';
        foreach($payment_result as $key => $val) {
            $payment_details_string .= "<li> <b> $key </b> $val </li>";
        }
        $payment_details_string .= '</ul>';

        if(APPLICATION_ENV == 'production') {
            $to = 'sales@escortdirectory.com';

        }else{
            $to = 'edhovhannisyan97@gmail.com';
        }

        $mail_sent = Cubix_Email::sendTemplate('payment_details_v1', $to, array(
            'details' => $payment_details_string
        ));


        if($mail_sent){
            $message = [
                'type' => "success",
                'text' => __('email_send_success')
            ];
        }else{
            $message = [
                'type' => "danger",
                'text' => __('email_send_error')
            ];
        }

        self::_getSession()->message = $message;
        self::_getSession()->package = null;
        $this->_redirect('/advertise');
    }

}