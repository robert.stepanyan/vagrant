<?php

class OnlineBillingV2Controller extends Zend_Controller_Action {

	const PACKAGE_STATUS_PENDING  	= 1;
	const PACKAGE_STATUS_ACTIVE   	= 2;
	const PACKAGE_STATUS_EXPIRED  	= 3;
	const PACKAGE_STATUS_CANCELLED 	= 4;
	const PACKAGE_STATUS_UPGRADED 	= 5;
	const PACKAGE_STATUS_SUSPENDED 	= 6;

	private static $packages = array(108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119);
	private static $packages_monthly = array(130, 132);
	protected $_session;
	/* @var $client Cubix_Api_XmlRpc_Client*/
protected $client;
	public function init() {

        $config = Zend_Registry::get('system_config');
        $this->payemntDisabledCountries = explode(',',$config['paymentDisabledCountries']);

		$this->_session = new Zend_Session_Namespace('online_billing');
		$this->_session->setExpirationSeconds(60 * 60);

		$this->view->layout()->disableLayout();
		$this->view->user = $this->user = Model_Users::getCurrent();

    	$action = $this->_request->getActionName();
		$anonym_actions = array('success', 'failure', 'mmg-postback' );

		if (!$this->user && !in_array($action, $anonym_actions)) {

			if($this->getRequest()->isXmlHttpRequest()){
				header("HTTP/1.1 403 Forbidden");
			}else{
				$this->_redirect($this->view->getLink('signin'));
			}
			return;
		}

		if (!in_array($action, $anonym_actions) && !$this->user->isAgency() && !$this->user->isEscort()) {
			$this->_redirect($this->view->getLink('private'));
		}

		$this->client = new Cubix_Api_XmlRpc_Client();
    }

    public function overlappingCheck($packageActivationDate, $duration){
        $today = date('d-m-Y', time());
        if (empty($packageActivationDate)){
            $packageActivationDate = $today;
        }
        $escort_id = $this->user->escort_data['escort_id'];
        $agency_id = $this->user->agency_data['agency_id'];
        $tours = $this->client->call('getEscortToursV2', array($escort_id, $this->_request->lang_id, $agency_id));
        $packageEndDate = date('d-m-Y', strtotime($packageActivationDate. " + $duration days"));
        $tours = $tours[0];
        foreach ($tours as $tour) {
            $tourFrom = date('d-m-Y', $tour['date_from']);
            $tourTo   = date('d-m-Y', $tour['date_to']);

            if ($tourFrom > $packageActivationDate && $tourFrom < $packageEndDate) {
                die(json_encode(array('status' => 'overlapped')));
            } elseif ($tourTo > $packageActivationDate && $tourTo < $packageEndDate) {
                die(json_encode(array('status' => 'overlapped')));
            }
        }
        return false;
    }

	public function indexAction()
	{
		if ($this->user->isEscort()) {
			$escorts_model = new Model_EscortsV2();
			$escort_id = $this->user->escort_data['escort_id'];

			$escort = $escorts_model->get($escort_id, null);

            if (in_array(strval($escort->country_id), $this->payemntDisabledCountries)) {
                die(json_encode(array(
                    'user_type' => 'escort',
                    'reason' =>  'no_packages_availble',
                )));
            }

			$is_pseudo_escort = $this->client->call(
				'OnlineBillingV2.isPseudoEscort', array($escort_id)
			);

            $workingLocations  = array(
                array(
                    'city_id' => $escort->base_city_id,
                    'title' => $escort->base_city_en,
                    'is_base'=>1
                )
            );
            $AdditionalCities = $escort->getAdditionalCities($escort->base_city_id);
            foreach ($AdditionalCities as $additionalCity){
                $city = array(
                    'city_id' => $additionalCity['city_id'],
                    'title' => $additionalCity['city_title'],
                    'is_base'=>0
                );
                array_push($workingLocations, $city);
            }
			$user_type = $is_pseudo_escort ? USER_TYPE_AGENCY : USER_TYPE_SINGLE_GIRL;
			$escort->is_pseudo_escort = $is_pseudo_escort;

			$escort_packages = $this->client->call(
				'OnlineBillingV2.checkIfHasPaidPackage', array($escort_id)
			);


			$is_package_purchase_allowed = true;
			$logger = '';

			if (count($escort_packages)) {
				list($ongoing_package, $pending_package) = $escort_packages;

				if ($ongoing_package && ($ongoing_package['status'] == self::PACKAGE_STATUS_PENDING)) {
					$is_package_purchase_allowed = false;
                    $logger = 'Has ongoing pending package';
				}
			}

			if ($escort->escort_status != ESCORT_STATUS_ACTIVE) {
				$is_package_purchase_allowed = false;
                $logger = 'Is not active Escort';
            }

			$available_packages = $this->client->call(
				'OnlineBillingV2.getPackagesListED',
				array($user_type, $escort->gender, self::$packages, $escort->type)
			);

			// Let's check if there is no package for the user to buy
            // ---------------------------------------
            $reason = null;
			if(count($available_packages) <= 0) {

			    // First and most common case is that user is Trans
                // ------------------------
                if($escort->gender == GENDER_TRANS) {
                    $reason = __('no_available_package_for_trans');
                }else{
                    $reason = __("no_packages_avaible");
                }
            }

            // USA and France
            $tours_country_ids = [];
            if ( isset( $escort->tours ) && count( $escort->tours ) > 0 )
            {
                foreach ( $escort->tours as $tour )
                {
                    if ( (int)$tour['country_id'] === 68 || (int)$tour['country_id'] == 23)
                    {
                        $tours_country_ids[] = (int)$tour['country_id'];
                    }
                }
            }
            $ads_upgrade_accepted = true;
            if ( in_array(23 , $tours_country_ids ) || in_array(68 , $tours_country_ids )) $ads_upgrade_accepted = false;
            if( strtolower( $escort->country_iso ) == 'us' || strtolower( $escort->country_iso ) == 'fr' || $ads_upgrade_accepted === false){
            	$escort->error == true;
            	$available_packages = [];
            	$reason = __("no_packages_avaible");
            }


            $client = Cubix_Api_XmlRpc_Client::getInstance();
            $package_country = $client->call('Escorts.getPackageCountryId', array($escort_id));
            $escortTours = $this->client->call('getEscortToursV2', array($escort_id, $this->_request->lang_id));
            $premium_country_change = '';
            if (!empty($package_country)){
                if($package_country['country_type']){
                    $countries_a = array(10,24,33,67);

                    if((in_array( $escort->country_id, $countries_a) && $package_country['country_type'] == 2) || (!in_array( $escort->country_id, $countries_a) && $package_country['country_type'] == 1)){
                        $premium_country_change = __('premium_country_change');;
                    }

                }

            }


            // end USA and France
            die(json_encode(array(
				'user_type' => 'escort',
                'all_escorts_from_us_fr' => '',
				'premium_country_change' => $premium_country_change,
				'escort' => $escort,
				'escort_packages' => $escort_packages,
				'is_package_purchase_allowed' => $is_package_purchase_allowed,
				'available_packages' => $available_packages,
                'working_locations' => $workingLocations,
                'reason' => $reason,
                'logger' => $logger
			)));
		} elseif ($this->user->isAgency()) {
			$user_type = USER_TYPE_AGENCY;
			$agency_id = $this->user->agency_data['agency_id'];

			$agency = $this->user->getAgency();
            /*if (in_array(strval($agency->country_id), $this->payemntDisabledCountries)) {
                die(json_encode(array(
                    'user_type' => 'agency',
                    'reason' =>  'no_packages_availble',
                )));
            }*/

            $agency_escorts = $this->client->call('OnlineBillingV2.getAgencyEscortsED', array($agency_id));
			$agency_disabled_escorts = $this->client->call('OnlineBillingV2.getAgencyDisabledEscortsED', array($agency_id));
			$escortGenders = [];
			$escortTypes = [];
			$escorts_count_us_fr = 0;
			$all_escorts_from_us_fr = '';
            $agency_escorts_count = count($agency_escorts);

			foreach($agency_escorts as $i => $escort) {
                if (in_array(strtolower($escort['country_iso']), ['us', 'fr'])) {
                    $escorts_count_us_fr++;
					unset($agency_escorts[$i]);
					continue;
				}

				$escort = new Model_EscortV2Item($escort);
				$cur_main_photo = $escort->getMainPhoto();
				$agency_escorts[$i]['photo'] = $cur_main_photo->getUrl('agency_pt_v2');

                foreach ($escort['working_locations'] as $j => $location){
                    $titleKey = Cubix_I18n::getTblField('title');
                    $city = array(
                        'city_id' => $location['id'],
                        'title' => $location[$titleKey],
                        'is_base'=>(int) $escort->base_city_id == $location['id']
                    );
                    $escort['working_locations'][$j]=$city;
                }

				$agency_escorts[$i]['working_locations'] = $escort['working_locations'];
                $escortGenders[] = $escort->gender;
                $escortTypes[] = $escort->type;
//				if ($escort['gender'] !== 1) {
//					unset($escort);
//				}
			}
            if($agency_escorts_count > 0 && ($agency_escorts_count == $escorts_count_us_fr)){
                $all_escorts_from_us_fr = 'all_escorts_from_us_fr';
            }
			foreach($agency_disabled_escorts as $i => $escort) {
				if(strtolower($escort['country_iso']) == 'us' ){
					unset($agency_disabled_escorts[$i]);
					continue;
				}

				$escort = new Model_EscortV2Item($escort);
				$cur_main_photo = $escort->getMainPhoto();
				$agency_disabled_escorts[$i]['photo'] = $cur_main_photo->getUrl('agency_pt_v2');
                // $escortGenders[] = $escort->gender;
                // $escortTypes[] = $escort->type;
//				if ($escort['gender'] !== 1) {
//					unset($escort);
//				}
			}

            $escortGenders = array_unique($escortGenders, SORT_NUMERIC );
            $escortTypes = array_unique($escortTypes, SORT_NUMERIC );
			$available_packages = $this->client->call(
				'OnlineBillingV2.getPackagesListED',
				array($user_type, $escortGenders, array_merge(self::$packages_monthly, self::$packages), $escortTypes)
			);

			$agency_discounts = $this->client->call('OnlineBillingV2.getAgencyDiscounts');

            // Let's check if there is no package for the user to buy
            // ---------------------------------------
            $reason = null;
            if(count($available_packages) <= 0) {

                // First and most common case is that user is Trans
                // ------------------------
                if($escort->gender == GENDER_TRANS) {
                    $reason = __('no_available_package_for_trans');
                }else{
                    $reason = __("no_packages_avaible");
                }
            }

            // $agency_disabled_escorts = array();
			die(json_encode(array(
				'user_type' => 'agency',
                'premium_country_change' => '',
                'all_escorts_from_us_fr' => $all_escorts_from_us_fr,
				'agency' => $this->user->agency_data,
				'escorts' => $agency_escorts,
				'disabled_escorts' => $agency_disabled_escorts,
				'available_packages' => $available_packages,
				'agency_discounts' =>  $agency_discounts,
				'reason' =>  $reason
			)));
		}
	}

	public function checkoutAction() {
		if (!$this->_request->isPost()) die();
		$request = $this->_request;


		$hash  = base_convert(time(), 10, 36);
		$cart = $request->cart;

		$user_id = $this->user->id;
		$user_type = $this->user->isAgency() ? USER_TYPE_AGENCY : USER_TYPE_SINGLE_GIRL;
        $promo_code  = strip_tags($request->promo_code);

		$post_data = array();

		// ESCORT
		if ($this->user->isEscort()) {
			$data = array();

            $escorts_model = new Model_EscortsV2();
            $escort = $escorts_model->get($cart[0]['escortId'], null);
            $country_data = $escort->getCountryData();

			if ($cart[0]['activation_date']) {
				$data['activation_date'] = $cart[0]['activation_date'];
			}
			if ($cart[0]['premium_cities']) {
				$data['premium_cities'] = $cart[0]['premium_cities'];
			}

			$post_data[] = array(
				'user_id' => $user_id,
				'escort_id' => $cart[0]['escortId'],
				'agency_id' => null,
				'package_id'=> $cart[0]['packageId'],
				'data' => serialize($data),
				'hash' => $hash,
				'country_id' => $country_data->country_id,
                'promo_code' => $promo_code
            );
            $this->overlappingCheck($cart[0]['activation_date'], $cart[0]['duration']);
        }

		// AGENCY

		if ($this->user->isAgency()) {
			foreach($cart as $package) {
				$data = array();

				if ($package['activation_date']) {
					$data['activation_date'] = $package['activation_date'];
				}

				if ($package['premium_cities']) {
					$data['premium_cities'] = $package['premium_cities'];
				}

				if ($package['discount']) {
					$data['discount'] = $package['discount'];
				}
                $escorts_model = new Model_EscortsV2();
                $escort = $escorts_model->get($package['escortId'], null);
                $country_data = $escort->getCountryData();


				$post_data[] = array(
					'user_id' => $user_id,
					'escort_id' => $package['escortId'],
					'agency_id' => null,
					'package_id'=> $package['packageId'],
					'data' => serialize($data),
					'hash' => $hash,
                    'country_id' => $country_data->country_id,
                    'promo_code' => $promo_code
				);
                 $this->overlappingCheck($package['activation_date'], $package['duration']);
			}
		}

		$amount = $this->client->call(
			'OnlineBillingV2.addToShoppingCartED',
			array(
				$post_data,
				$user_id,
				$user_type,
				$hash
			)
		);

        $promoData = $this->client->call('OnlineBillingV2.checkPromoCode',array($promo_code));

        if ($promoData['amount'])
        {
            if( $amount > $promoData['amount'] )
            {
                $amount = $amount - $promoData['amount'];
            }else{
                $amount = 0;
            }
        }elseif($promoData['percent']){
            $amount = $amount - ( $amount * $promoData['percent'] ) / 100;
        }

		$mmgBill = new Model_MmgBillAPIV2();

		$hosted_url = $mmgBill->getHostedPageUrl(
			$amount, 'SZ' . $this->user->id . 'Z' . $hash, 'http://www.escortdirectory.com' . $this->view->getLink('mmg-postback'));

		die(json_encode(array('status' => 'success', 'url' =>  $hosted_url)));
	}

	public function mmgPostbackAction() {
		$this->view->layout()->setLayout('main');
		$this->view->transaction_status = $this->_request->txn_status;
	}

	public function successAction() {
	}

	public function failureAction()	{
	}

	public function movePackageAction(){
		if ( ! $this->user->isAgency() ) die;

		$agency = $this->user->getAgency();

		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'from_escort_id' => 'int-nz',
			'to_escort_id' => 'int-nz',
			'city_ids' => 'arr-int'
		));
		$data = $form->getData();

		if ( !$agency->hasEscort($data['from_escort_id']) || ! $agency->hasEscort($data['to_escort_id']) ) {
			die;
		}

		$result = $this->client->call('premium_switchActivePackagesForED', array($data['from_escort_id'], $data['to_escort_id'], $data['city_ids']));

		die(json_encode(array('status' => $result['success'], 'message' =>  $result['message'])));
	}


    public function checkPromoCodeAction() {
        if (!$this->_request->isPost()) die();
        $request = $this->_request;

        $promo_code  = strip_tags($request->promo_code);

        $promoCode = $this->client->call('OnlineBillingV2.checkPromoCode',array($promo_code));

        if ($promoCode)
        {
            die(json_encode(array('status' => 'success', 'promo_data' =>  $promoCode)));
        }else{
            die(json_encode(array('status' => 'error', 'message' =>  'Invalid Promo code')));
        }
    }
}