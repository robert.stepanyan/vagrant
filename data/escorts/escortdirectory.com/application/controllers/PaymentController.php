<?php

/**
 * @autor Eduard Hovhannisyan
 * @created 24/06/2020
 *
 * Class PaymentController
 */
class PaymentController extends Zend_Controller_Action
{
    /**
     * @var Array
     */
    private $paymentMethods;

    /**
     * @var Cubix_Api_XmlRpc_Client
     */
    private $client;

    /**
     * @return void
     */
    public function init()
    {
        $this->view->layout()->setLayout('main');
        $this->paymentMethods = [
            'default' => 'mmg',
        ];
        $this->client = new Cubix_Api_XmlRpc_Client();
    }

    /**
     * Home page for /payments
     *
     * @acceptsRequests GET
     * @return void
     */
    public function indexAction()
    {
        $selectedKey = $this->getRequest()->getParam('paymentMethodKey', 'default');

        if (array_key_exists($selectedKey, $this->paymentMethods) === FALSE) return $this->_redirect($this->view->getLink('home'));

        $selectedPaymentMethod = $this->paymentMethods[$selectedKey];
        $this->view->paymentMethod = $selectedPaymentMethod;
    }

    public function guestPaymentAction()
    {
        $selectedKey = $this->getRequest()->getParam('paymentMethodKey', 'default');
        $price = $this->getRequest()->getParam('price', 'default');

        if (array_key_exists($selectedKey, $this->paymentMethods) === FALSE) return $this->_redirect($this->view->getLink('home'));

        $selectedPaymentMethod = $this->paymentMethods[$selectedKey];
        $this->view->paymentMethod = $selectedPaymentMethod;
        $this->view->price = $price;
    }

    /**
     * Endpoint action to handle MmgBill redirection,
     * whenever user completes payment process in https://secure.mmgbill.com/
     *
     * @acceptsRequests GET
     * @return void
     * @throws Zend_Exception
     */
    public function mmgResponseAction()
    {
        $request = $this->_request;
        $paymentResult = [];

        if (isset($request->txn_status) and $request->txn_status == 'APPROVED') {

            $hash = str_ireplace('ADZ', '', $request->ti_mer);

            $shoppingCard = $this->client->call( 'OnlineBillingV2.updateShoppingCartED', array( $hash ) );
            if(!is_array( $shoppingCard)) return $this->_redirect('/payment');

            $data = unserialize($shoppingCard['data']);

            $paymentResult['Transaction ID'] = $request->ti_mmg;
            $paymentResult['Amount'] = intval($data['amount']) . ' EUR';

            $this->view->success = true;
            $this->view->response = $paymentResult;

            if ($request->mmg_errno != '0000')
                $payment_result['error'] = $request->mmg_errno;

        } else {
            $this->view->success = false;
        }
    }

    /**
     * Is used only for ajax request.
     *
     * @acceptsRequests GET | POST*
     * @return void
     */
    public function checkoutAction()
    {
        try {
            $paymentMethod = $this->_request->paymentMethod;

            $methodName = 'checkoutWith' . $paymentMethod;

            if (!method_exists($this, $methodName)) throw new \Exception('Invalid Payment method!');

            $result = call_user_func([$this, $methodName]);
            echo json_encode($result, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES);

        } catch (\Exception $e) {
            echo json_encode([
                'status' => 'error',
                'error' => $e->getMessage()
            ]);
        }

        exit;
    }

    /**
     * Only $this->checkoutAction is allowed to call this method.
     * Please keep it private or protected.
     *
     * @acceptsRequests POST
     * @return array
     * @throws \Exception
     */
    private function checkoutWithMmg()
    {
        try {
            $mmgBill = new Model_MmgBillAPIV2();
            $hash = base_convert(time(), 10, 36);
            $amount = $this->_request->amount;

            $transactionId = 'ADZ' . $hash;
            $postBackUrl = APPLICATION_ENV == 'production' ? 'https://www.escortdirectory.com' : 'http://www.escortdirectory.com.test';

            $hostedUrl = $mmgBill->getHostedPageUrl($amount, $transactionId, $postBackUrl . $this->view->getLink('dedicated-payment-mmg-response'));

            $this->client->call(
                'OnlineBillingV2.addToShoppingCartED',
                array(
                    [
                        [
                            'user_id' => 0,
                            'escort_id' => 0,
                            'agency_id' => 0,
                            'package_id' => 0,
                            'data' => serialize([
                                'page' => 'dedicated-payment',
                                'paymentMethod' => 'mmg',
                                'amount' => $amount
                            ]),
                            'hash' => $hash
                        ]
                    ], 0, 0, $hash
                )
            );

        } catch (\Exception $e) {
            return array(
                'status' => 'error',
                'error' => $e->getMessage()
            );
        }

        return array(
            'status' => 'mmg_success',
            'url' => $hostedUrl,
        );
    }
}
