<?php

class FollowController extends Zend_Controller_Action
{	
	public function init()
	{
		$this->user = Model_Users::getCurrent();
		
	}

	public function removeAction()
	{
		
		$this->view->layout()->disableLayout();
		$model = new Model_Follow();
		
		$data = new Cubix_Form_Data($this->getRequest());

		$fields = array(
			'type' => '',
			'type_id' => 'int',
		);

		$data->setFields($fields);
		$post_data = $data->getData();
		if(!in_array($post_data['type'], array('escort',"agency"))){
			die;
		}
		
		if($this->user && $this->user->user_type == "member"){
			$model->remove($this->user->id, $post_data['type_id'], $post_data['type']);
			$this->user->follow_list = $model->getFollowList($this->user->id);
			Model_Users::setCurrent($this->user);
		}
		else{
			$follow_session = new Zend_Session_Namespace('follow_info');
			
			if($follow_session->follow && is_array($follow_session->follow)){
				foreach($follow_session->follow as $key => $follow){
					if($follow['user_type'] == $post_data['type'] && $follow['user_type_id'] == $post_data['type_id']){
						unset($follow_session->follow[$key]);
					}
				}
			}
		}
		
		die;
	}
	
	/*public function addoldAction()
	{
		$this->view->layout()->disableLayout();
		$model = new Model_Follow();
		
		$req = $this->_request;
		$logged_in = false;
		
		if($this->user && $this->user->user_type == "member"){
			$logged_in = true;
		}
		
		$this->view->logged_in = $logged_in;
		
		if ( $req->isPost() ) {
			$data = new Cubix_Form_Data($this->getRequest());

			$fields = array(
				'user_type' => '',
				'user_type_id' => 'int',
				'events' => 'arr-int',
				'cities' => 'arr-int',
				'short_comment' => 'notags|special',
				'follow_type' => 'int',
				'follow_email' => 'notags|special'
			);

			$data->setFields($fields);
			$post_data = $data->getData();
			
			if(!in_array($post_data['user_type'], array('escort',"agency"))){
				die;
			}
			
			$validator = new Cubix_Validator();
						
			if($post_data['follow_type'] == Model_Follow::FOLLOW_TYPE_EMAIL){
				
				if ( ! strlen($post_data['follow_email']) && $post_data['follow_type'] == Model_Follow::FOLLOW_TYPE_EMAIL ) {
					$validator->setError('follow_email', Cubix_I18n::translate('email_required'));
				}
				elseif( ! $validator->isValidEmail($post_data['follow_email'])) {
					$validator->setError('follow_email', Cubix_I18n::translate('wrong_email_format'));
				}
				elseif($email_error = $model->checkEmailExists($post_data['follow_email'],$post_data['user_type'],$post_data['user_type_id'])){
					
					if($email_error == 1){
						$validator->setError('follow_email', Cubix_I18n::translate('email_exist'));
					}
					else{
						$validator->setError('follow_email', Cubix_I18n::translate('email_escort_is_followed'));
					}
				}
			}
			
			
			if(count($post_data['events']) == 0){
				$validator->setError('user_type_id', Cubix_I18n::translate('follow_select_alert'));
			}
			elseif(in_array(FOLLOW_ESCORT_NEW_CITY,$post_data['events']) && count($post_data['cities']) == 0 ){
				$validator->setError('user_type_id', Cubix_I18n::translate('follow_select_city'));
			}
			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				echo(json_encode($result));	die;
			}
			else {
				
				$session = new Zend_Session_Namespace('follow_info');
				$result['type'] = $post_data['user_type'];
				if ($post_data['follow_type'] == 0 && $logged_in)
				{
					if($model->checkUserFollows($this->user->id,$post_data['user_type'],$post_data['user_type_id'])){
						die('following already');
					}
					
					if(!$model->checkUserExists($this->user->id)){
						$result['show_confirmation'] = 1;
					}
					
					$follow_data = array(
						'user_id' => $this->user->id,
						'type' => $post_data['user_type'],
						'type_id' => $post_data['user_type_id'],
						'short_comment' => $validator->removeEmoji($post_data['short_comment'])
					);
					
					$model->add($follow_data, $post_data['events'], $model->encode($post_data['cities']));
					$this->user->follow_list = $model->getFollowList($this->user->id);
					Model_Users::setCurrent($this->user);
				}
				elseif($post_data['follow_type'] == Model_Follow::FOLLOW_TYPE_EMAIL){
					
					$follower_id = $model->checkEmailFollowerExists($post_data['follow_email']);
					if($follower_id){
						$follow_data = array(
							'email' => $post_data['follow_email'],
							'type' => $post_data['user_type'],
							'type_id' => $post_data['user_type_id'],
						);
						$model->add($follow_data,$post_data['events'],$model->encode($post_data['cities']));
						$result['show_confirmation'] = 1;
					}
					else{
						$hash = md5( microtime() * rand());
						$follow_data = array(
							'email' => $post_data['follow_email'],
							'email_hash' => $hash,
							'type' => $post_data['user_type'],
							'type_id' => $post_data['user_type_id'],
							'status' => 0
						);
						$model->add($follow_data,$post_data['events'],$model->encode($post_data['cities']));

						$email_tpl = "follow_email_confirmation_escort_v1";

						if($post_data['user_type'] == "escort"){

							$model_photo = new Model_Escort_Photos();
							$photo_object = $model_photo->getMain($follow_data['type_id']);	
							$photo = $photo_object->getUrl('follow_email_thumb');	
						}
						else{

							$agency_model = new Model_Agencies();
							$agency_logo = $agency_model->getLogo($follow_data['type_id']);
							$agency_photo = $agency_logo->getLogoUrl(false, 'follow_email_thumb');
							$photo = $agency_photo ? $agency_photo : _st('newsletter/alerts/follow_def.jpg');
						}

						$tpl_data = array(
							'photo' => $photo,
							'email' => $follow_data['email'],
							'activation_hash' => $follow_data['email_hash'],
						);

						Cubix_Email::sendTemplate($email_tpl, $follow_data['email'], $tpl_data);
						
						$config = Zend_Registry::get('newsman_config');

						if ($config)
						{
							$list_id = $config['list_id'];
							$segment = $config['member'];
							$nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);

							$subscriber_id = $nm->subscribe($list_id, $follow_data['email'], 'follow member');
							$nm->bindToSegment($subscriber_id, $segment);
						}
						
						$result['show_email_confirm_info'] = 1;
					}
				}
				elseif($post_data['follow_type'] == Model_Follow::FOLLOW_TYPE_AFTER_SIGNIN){
					$result['show_login_popup'] = 1;
					$session->follow[] = $post_data;
				}
				elseif($post_data['follow_type'] == Model_Follow::FOLLOW_TYPE_AFTER_SIGNUP){
					$session->follow[] = $post_data;
					$result['redirect'] = $this->view->getLink('signup').'?user_type=member';
				}
				$session->success_info = $post_data;
				die(json_encode($result));
				
			}
		}
		else{
			$id = $this->view->id = (int) $req->id;
			$type = $this->view->type = $req->type;

			if ( ! $id || ! $type ) die;

			$contries_model = new Cubix_Geography_Countries();
			$this->view->countries = $contries_model->ajaxGetAll(false);
		}
	}*/
	
	public function addAction()
	{
		$this->view->layout()->disableLayout();
		$model = new Model_Follow();
		
		$req = $this->_request;
		$logged_in = false;
		
		if($this->user && $this->user->user_type == "member"){
			$logged_in = true;
		}
			
		if ( $req->isPost() ) {
			$data = new Cubix_Form_Data($this->getRequest());

			$fields = array(
				'user_type' => '',
				'user_type_id' => 'int',
				'follow_type' => 'int',
				'follow_email' => 'notags|special'
			);

			$data->setFields($fields);
			$post_data = $data->getData();
			
			if(!in_array($post_data['user_type'], array('escort',"agency"))){
				die;
			}

			if($post_data['user_type'] == 'escort'){
				$post_data_events = array(FOLLOW_ESCORT_NEW_COMMENT, FOLLOW_ESCORT_NEW_PROFILE, FOLLOW_ESCORT_NEW_PICTURE, FOLLOW_ESCORT_NEW_REVIEW, FOLLOW_ESCORT_NEW_PRICE);
			}
			else{
				$post_data_events = array(FOLLOW_AGENCY_NEW_FEMALE, FOLLOW_AGENCY_NEW_MALE, FOLLOW_AGENCY_NEW_TRANS, FOLLOW_AGENCY_NEW_COMMENT, FOLLOW_AGENCY_NEW_REVIEW);
			}
			
			$validator = new Cubix_Validator();
						
			if($post_data['follow_type'] == Model_Follow::FOLLOW_TYPE_EMAIL){
				
				if ( ! strlen($post_data['follow_email']) && $post_data['follow_type'] == Model_Follow::FOLLOW_TYPE_EMAIL ) {
					$validator->setError('follow_email', Cubix_I18n::translate('email_required'));
				}
				elseif( ! $validator->isValidEmail($post_data['follow_email'])) {
					$validator->setError('follow_email', Cubix_I18n::translate('wrong_email_format'));
				}
				elseif($email_error = $model->checkEmailExists($post_data['follow_email'],$post_data['user_type'],$post_data['user_type_id'])){
					
					if($email_error == 1){
						$validator->setError('follow_email', Cubix_I18n::translate('email_exist'));
					}
					else{
						$validator->setError('follow_email', Cubix_I18n::translate('email_escort_is_followed'));
					}
				}
			}
					
			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				echo(json_encode($result));	die;
			}
			else {
				$session = new Zend_Session_Namespace('follow_info');
				$result['type'] = $post_data['user_type'];

				if ($post_data['follow_type'] == 0 && $logged_in)
				{
					if($model->checkUserFollows($this->user->id,$post_data['user_type'],$post_data['user_type_id'])){
						die('following already');
					}
					
					/*if(!$model->checkUserExists($this->user->id)){
						$result['show_confirmation'] = 1;
					}*/
					
					$follow_data = array(
						'user_id' => $this->user->id,
						'type' => $post_data['user_type'],
						'type_id' => $post_data['user_type_id'],
					);

                    $model->add($follow_data, $post_data_events);
                    $this->user->follow_list = $model->getFollowList($this->user->id);
					Model_Users::setCurrent($this->user);
				}
				elseif($post_data['follow_type'] == Model_Follow::FOLLOW_TYPE_EMAIL)
				{
					$follower_id = $model->checkEmailFollowerExists($post_data['follow_email']);
					
					if($follower_id){
						$follow_data = array(
							'email' => $post_data['follow_email'],
							'type' => $post_data['user_type'],
							'type_id' => $post_data['user_type_id'],
						);
						
						$model->add($follow_data,$post_data_events, $model->encode($post_data['cities']));
						//$result['show_confirmation'] = 1;
					}
					else{
						$hash = md5( microtime() * rand());
						$follow_data = array(
							'email' => $post_data['follow_email'],
							'email_hash' => $hash,
							'type' => $post_data['user_type'],
							'type_id' => $post_data['user_type_id'],
							'status' => 0
						);
						$model->add($follow_data, $post_data_events, $model->encode($post_data['cities']));


						if($post_data['user_type'] == "escort"){
							$email_tpl = "follow_email_confirmation_escort_v1";

							$model_photo = new Model_Escort_Photos();
							$model_escort = new Model_EscortsV2();
							$showname = $model_escort->getShownameById($follow_data['type_id']); 
							$photo_object = $model_photo->getMain($follow_data['type_id']);	
							$photo = $photo_object->getUrl('follow_email_thumb');
							$url = Cubix_Application::getById(Cubix_Application::getId())->host . $this->view->getLink('profile', array('showname' => $showname, 'escort_id' => $follow_data['type_id']));
						}
						else{
							$email_tpl = "follow_email_confirmation_agency_v1";
							
							$agency_model = new Model_Agencies();
							$showname = $agency_model->getNameById($follow_data['type_id']);
							$slug = $agency_model->getSlugById($follow_data['type_id']);
							$agency_logo = $agency_model->getLogo($follow_data['type_id']);
							$agency_photo = $agency_logo->getLogoUrl(false, 'follow_email_thumb');
							$photo = $agency_photo ? $agency_photo : _st('newsletter/alerts/follow_def.jpg');
							$url = Cubix_Application::getById(Cubix_Application::getId())->host .$this->view->getLink('agency-profile', array('agency_slug' => $slug, 'agency_id' => $follow_data['type_id']));
						}

						$tpl_data = array(
							'showname' => $showname,
							'url' => $url,
							'photo' => $photo,
							'email' => $follow_data['email'],
							'activation_hash' => $follow_data['email_hash'],
						);
						
						Cubix_Email::sendTemplate($email_tpl, $follow_data['email'], $tpl_data);
						
						$config = Zend_Registry::get('newsman_config');

						if ($config)
						{
							$list_id = $config['list_id'];
							$segment = $config['member'];
							$nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);

							$subscriber_id = $nm->subscribe($list_id, $follow_data['email'], 'follow member');
							$nm->bindToSegment($subscriber_id, $segment);
						}
						
						$result['show_email_confirm_info'] = 1;
					}
				}
				elseif($post_data['follow_type'] == Model_Follow::FOLLOW_TYPE_AFTER_SIGNIN){
					$result['show_login_popup'] = 1;
					$post_data['events'] = $post_data_events;
					$session->follow[] = $post_data;
				}
				elseif($post_data['follow_type'] == Model_Follow::FOLLOW_TYPE_AFTER_SIGNUP){
					$session->follow[] = $post_data;
					$result['redirect'] = $this->view->getLink('signup').'?user_type=member';
				}
				$session->success_info = $post_data;
				die(json_encode($result));
				
			}
		}
		else{
			$id = intval($req->id);
			$type  = $req->type;

			if ( ! $id || ! $type ) die;
			
			if($type == "escort"){
				$escort_model = new Model_EscortsV2();
				$photo_model = new Model_Escort_Photos();
				$showname = $escort_model->getShownameById($id);
				$photo = $photo_model->getMain($id)->getUrl('follow_thumb_pop');
			}
			else{
				$agency_model = new Model_Agencies();
				$agency_row = $agency_model->get($id);
				$showname = $agency_row->showname;
				$photo = null;
			}
			$this->view->user_id = $id;
			$this->view->user_type = $type;
			$this->view->showname = $showname;
			$this->view->photo = $photo;
		}
	
	}
	
	public function successAction()
	{
		$this->view->layout()->disableLayout();
		$follow_session = new Zend_Session_Namespace('follow_info');
		
		if($follow_session->success_info){
			
			$city_model = new Model_Cities();
			$cities = array();

			foreach($follow_session->success_info['cities'] as $key => $city){
				$city_data = $city_model->getById($city);
				$cities[$key]['city'] = $city_data->title;
				$cities[$key]['country'] = $city_data->country_title;
			}
						
			if($follow_session->success_info['user_type'] == "escort"){
				$escort_model = new Model_EscortsV2();
				$photo_model = new Model_Escort_Photos();
				$showname = $escort_model->getShownameById($follow_session->success_info['user_type_id']);
				$photo = $photo_model->getMain($follow_session->success_info['user_type_id'])->getUrl('follow_thumb_pop');
			}
			else{
				$agency_model = new Model_Agencies();
				$agency_row = $agency_model->get($follow_session->success_info['user_type_id']);
				$showname = $agency_row->showname;
				$photo = null;
			}
			$this->view->user_type = $follow_session->success_info['user_type'];
			$this->view->cities = $cities;
			$this->view->showname = $showname;
			$this->view->events = $follow_session->success_info['events'];
			$this->view->photo = $photo;
			unset($follow_session->success_info);
		}
	}
	
	public function confirmEmailAction()
	{
        $this->view->layout()->setLayout('main');
		$hash = $this->_getParam('hash');
		$email = $this->_getParam('email');





		$model = new Model_Follow();
		$follow_info = $model->confirmEmail($email, $hash);



		if ( $follow_info) {

            $geoData = Cubix_Geoip::getClientLocation();
            $modelCities = new Model_Cities();
            $geo_city = $this->view->geo_city = $modelCities->getByGeoIp();
            list(
                $escortsAround,
                $aroundLocationTitle,
                ) = Model_Escort_List::getAroundEscorts([], [], 6, 1,'escorts', $geo_city['city_id'], $geo_city['country_id']);

            $this->view->escortsAround = $escortsAround;
            $this->view->aroundLocationTitle = $aroundLocationTitle;



            $this->_helper->viewRenderer->setScriptAction('index');
			/*if($follower['user_type'] == "escort"){
				$this->_response->setRedirect($this->view->getLink('profile',array('showname' => $follower['showname'], 'escort_id' => $follower['user_type_id'], 'follow_success' => $follower['id'])));
			}
			else{
				$this->_response->setRedirect($this->view->getLink('agency-profile',array('agency_slug' => $follower['agency_slug'], 'agency_id' => $follower['user_type_id'], 'follow_success' => $follower['id'])));
			}*/


		}
		else{
			$this->_response->setRedirect($this->view->getLink());
		}

	}
	
	public function emailConfirmInfoAction()
	{
		$this->view->layout()->disableLayout();
	}		
}