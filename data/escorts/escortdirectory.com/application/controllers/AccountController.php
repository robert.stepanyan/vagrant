<?php

class AccountController extends Zend_Controller_Action
{
    public static $linkHelper;

    protected $_session;

    public function init()
    {
        $this->_request->setParam('no_tidy', true);

        $anonym = array('membership-type', 'signup', 'signin', 'signin-mobile', 'forgot', 'activate', 'check', 'sign-in-up', 'choose-vip-package', 'change-pass', 'secure-signin');
        $this->blacklisted_usernames = array('admin', 'moderator', 'webmaster', 'manager', 'sales');
        $this->user = Model_Users::getCurrent();

        if (!$this->user && !in_array($this->_request->getActionName(), $anonym)) {
            $this->_response->setRedirect($this->view->getLink('signin'));
            return;
        }

        if ($this->user && isset($_GET['referrer'])) {
            if (strpos($_GET['referrer'], 'private') === false) {
                $this->_response->setRedirect(Cubix_Application::getById()->url . '/' . $_GET['referrer']);
            } else {
                $_GET['referrer'] = str_replace('private/', 'private#', $_GET['referrer']);
                $this->_response->setRedirect(Cubix_Application::getById()->url . '/' . $_GET['referrer']);
            }
        }

        $this->_session = new Zend_Session_Namespace('private');

        if (!isset($_COOKIE['ec_session_closed'])) {
            setcookie('ec_session_closed', true, strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
        }
		
		if($_GET['sisis'] == 1){
			var_dump(session_id(),  session_save_path());die;
		}

    }


    public function indexAction()
    {
        if ($this->user->isAgency() || $this->user->isEscort() || $this->user->isMember()) {
            $this->_redirect($this->view->getLink('private'));
        }
    }

    public function signupAction()
    {
        $req = $this->_request;
        $this->view->type = $type = $this->_getParam('user_type');

        if(!in_array(strtolower($type), ['escort', 'member', 'agency']) && !$this->_request->isPost()) {
            return $this->_redirect($this->view->getLink('signin'));
        }

        $this->view->layout()->setLayout('register');
        $client = new Cubix_Api_XmlRpc_Client();

        $signup_i18n = $this->view->signup_i18n = (object)array(
            'username_invalid' => 'Username must be at least 6 characters long',
            'username_invalid_characters' => 'Only "a-z", "0-9", "-" and "_" are allowed',
            'username_exists' => 'Username already exists, please choose another',
            'password_invalid' => 'Password must contain at least 6 characters',
            'password2_invalid' => 'Passwords must be equal',
            'email_invalid' => 'Email is invalid, please provide valid email address',
            'email_exists' => 'Email already exists, please enter another',
            'form_errors' => 'Please check errors in form',
            'terms_required' => 'You have to accept terms and conditions before continue',
            'domain_blacklisted' => 'Domain is blacklisted',
            'user_type' => 'Choose your business',
            'username_equal_password' => __('username_equal_password'),
        );

        if($type == 'escort') {
            define(JOLYNE, 196);
            define(CARRIE, 197);
            define(ANDREW, 201);
            define(ADMIN, 1); // Default

            $sales_persons = $client->call('Users.getEdSalesPersons');
            $allowed_admins = [JOLYNE, CARRIE, ANDREW, ADMIN];

            foreach($sales_persons as $key => &$sales){
                if(! in_array($sales['id'], $allowed_admins)){
                    unset($sales_persons[$key]);
                }

                if($sales['id'] == ADMIN) {
                    $sales['username'] = __('doesnt_matter');
                }
            }

            $this->view->sales_persons = $sales_persons;

        }

        if ($this->_request->isPost()) {


            $user_type = $this->_getParam('user_type', $type);
            $user_t = $this->_getParam('user_type');
            $g_recaptcha_response = $this->_getParam('g-recaptcha-response');

            $validator = new Cubix_Validator();

            if (!in_array($user_type, array('member', 'escort', 'agency'))) {
                $validator->setError('user_type', $signup_i18n->user_type);
            }

            if ($user_type == 'escort' && !in_array($user_t, array('member', 'vip-member', 'escort', 'agency'))) {
                /* Update Grigor */
                $validator->setError('user_type', $signup_i18n->user_type);
                /* Update Grigor */

            }

            $data = new Cubix_Form_Data($this->_request);
            $fields = array(
                'promotion_code' => '',
                'username' => 'notags|xss',
                'password' => '',
                'email' => 'notags|xss',
                'terms' => 'int',
                'g-recaptcha-response' => '',
                'sales_manager_id' => 'int'
                //'recaptcha_response_field' => ''
            );
            $data->setFields($fields);
            $data = $data->getData();

            $data['username'] = substr($data['username'], 0, 24);
            $data['user_type'] = $user_t;
            $this->view->data = $data;

            $has_bl_username = false;
            foreach ($this->blacklisted_usernames as $bl_username) {
                if (strpos($data['username'], $bl_username) !== false) {
                    $has_bl_username = true;
                    BREAK;
                }
            }

            $model = new Model_Users();


            if (strlen($data['username']) < 6) {
                $validator->setError('username', $signup_i18n->username_invalid);
            } elseif (!preg_match('/^[-_a-z0-9]+$/i', $data['username'])) {
                $validator->setError('username', $signup_i18n->username_invalid_characters);
            } //elseif (stripos($data['username'], 'admin') !== FALSE) {
            elseif ($has_bl_username) {
                $validator->setError('username', $signup_i18n->username_exists);
            } elseif ($client->call('Users.getByUsername', array($data['username']))) {
                $validator->setError('username', $signup_i18n->username_exists);
            }

            if ($data['password'] == $data['username']) {
                $validator->setError('password', $signup_i18n->username_equal_password);
            }

            if (strlen($data['password']) < 6) {
                $validator->setError('password', $signup_i18n->password_invalid);
            }
//			elseif ( $data['password'] != $data['password2'] ) {
//				$validator->setError('password2', $signup_i18n->password2_invalid);
//			}

            if (!preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email'])) {
                $validator->setError('email', $signup_i18n->email_invalid);
            } elseif ($client->call('Application.isDomainBlacklisted', array($data['email']))) {
                $validator->setError('email', $signup_i18n->domain_blacklisted);
            } elseif ($client->call('Users.getByEmail', array($data['email']))) {
                $validator->setError('email', $signup_i18n->email_exists);
            }

            if (!$data['terms']) {
                $validator->setError('terms', $signup_i18n->terms_required);
            }
            if ($g_recaptcha_response == '') {
                $validator->setError('captcha', 'Captcha is required');
            }


            if (is_null($this->_getParam('ajax'))) {
                /*$captcha = Cubix_Captcha::verify($this->_request->recaptcha_response_field);

                $captcha_errors = array(
                    'invalid-site-public-key' => 'We weren\'t able to verify the public key.',
                    'invalid-site-private-key' => 'We weren\'t able to verify the private key.',
                    'invalid-request-cookie' => 'The challenge parameter of the verify script was incorrect.',
                    'incorrect-captcha-sol' => 'The CAPTCHA solution was incorrect.',
                    'verify-params-incorrect' => 'The parameters to /verify were incorrect, make sure you are passing all the required parameters.',
                    'invalid-referrer' => 'reCAPTCHA API keys are tied to a specific domain name for security reasons.',
                    'recaptcha-not-reachable' => 'reCAPTCHA never returns this error code. A plugin should manually return this code in the unlikely event that it is unable to contact the reCAPTCHA verify server.'
                );



                if (!is_bool($captcha) ) {
                    $validator->setError('captcha', $captcha_errors[$captcha]);
                }*/
                // if ( ! strlen($data['captcha']) ) {
                // $validator->setError('captcha', 'Captcha is required');
                // }
                // else {
                // 	$session = new Zend_Session_Namespace('captcha');
                // 	$orig_captcha = $session->captcha;

                // 	if ( strtolower($data['captcha']) != $orig_captcha ) {
                // 		$validator->setError('captcha', 'Captcha is invalid');
                // 	}
                // }
            }

            $this->view->errors = array();
            $result = $validator->getStatus();

            if (!$validator->isValid()) {
                $this->view->errors = $result['msgs'];
                if (!is_null($this->_getParam('ajax'))) {
                    echo(json_encode($result));
                    ob_flush();
                    die;
                }
            } else {
                // will set the most free sales user id

                // Users model
                $user = new Model_UserItem(array(
                    'username' => $data['username'],
                    'email' => $data['email'],
                    'user_type' => $user_type == 'vip-member' ? 'member' : $user_type
                ));

                /*if (in_array($data['promotion_code'], array('123456', '9988')))
                    $user->sales_user_id = 16;
                else
                    Model_Hooks::preUserSignUp($user);*/

                //EDIR-737
                $geoData = Cubix_Geoip::getClientLocation();
                $country_is = $geoData['country_iso'];
                if (in_array($user_type,array('escort','agency')) && $data['sales_manager_id'] == 1){
                    if (in_array($country_is,array('br','pt')))
                    {
                        $user->sales_user_id = 213;//BARBARA//EDIR-737
                    }else if (in_array($country_is,array('it')))
                    {
                        $user->sales_user_id = 201;//ANDREW//EDIR-737
                    }else if (!in_array($country_is,array('us')))
                    {
                        $lastUserId = $model->getLastUserId();
                        if ($lastUserId % 2 == 0)
                        {
                            $user->sales_user_id = 201;//ANDREW//EDIR-737
                        }else{
                            $user->sales_user_id = 197;//CARRIE//EDIR-737
                        }
                    }else if (in_array($country_is,array('us')))
                    {
                        $lastUserId = $model->getLastUserId();
                        if ($lastUserId % 2 == 0)
                        {
                            $user->sales_user_id = 205;//usa_sales1//EDIR-737
                        }else{
                            $user->sales_user_id = 206;//usa_sales2//EDIR-737
                        }
                    }
                }else{
                    $user->sales_user_id = $data['sales_manager_id'];
                }
                // EDIR-737 end
                
                $salt_hash = Cubix_Salt::generateSalt($data['email']);
                $user->salt_hash = $salt_hash;
                $user->password = Cubix_Salt::hashPassword($data['password'], $salt_hash);

                $new_user = $model->save($user);

                /* add to newsman */
                $subscribe = $this->_getParam('subscribe');

                if ($subscribe) {
                    $ip = Cubix_Geoip::getIP();

                    $m_n_i = new Cubix_Newsman_Ids();
                    $n_ids = $m_n_i->get(Cubix_Application::getId());

                    if (count($n_ids)) {
                        $list_id = reset(array_keys($n_ids));
                        $conf = $m_n_i->getConf();

                        try {
                            $client = new Cubix_Newsman_Client($conf['user_id'], $conf['api_key']);
                            $client->setCallType("rest");
                            $client->setApiUrl("https://ssl.nzsrv.com/api");

                            $subscriber_id = $client->subscriber->saveSubscribe($list_id, $data['email'], '', '', $ip, array());

                            $n_user_type = $user_type;
                            if ($n_user_type == 'vip-member') $n_user_type = 'member';

                            if (isset($n_ids[$list_id][$n_user_type]))
                                $segment_id = $n_ids[$list_id][$n_user_type];
                            else
                                $segment_id = $n_ids[$list_id]['member'];

                            if ($subscriber_id) {
                                $client->subscriber->addToSegment(
                                    $subscriber_id,
                                    $segment_id
                                );
                            }
                        } catch (Exception $e) {
                            //var_dump($e);die;
                        }
                    }
                }
                /**/

                if ('escort' == $user_type) {
                    $m_escorts = new Model_Escorts();

                    $escort = new Model_EscortItem(array(
                        'user_id' => $user->new_user_id
                    ));

                    // will set current app country id
                    Model_Hooks::preEscortSignUp($escort);

                    $escort = $m_escorts->save($escort);

                    // will update escort status bits using api
                    Model_Hooks::postEscortSignUp($escort);
                } elseif ('agency' == $user_type) {
                    $m_agencies = new Model_Agencies();

                    $agency = new Model_AgencyItem(array(
                        'user_id' => $user->new_user_id,
                        'name' => ucfirst($user->username)
                    ));

                    // will set current app country id
                    Model_Hooks::preAgencySignUp($agency);

                    $agency = $m_agencies->save($agency);

                    //
                    Model_Hooks::postAgencySignUp($agency);
                } elseif ('member' == $user_type || 'vip-member' == $user_type) {
                    unset($this->_session->want_premium);
                    $m_members = new Model_Members;

                    $member = new Model_MemberItem(array(
                        'user_id' => $user->new_user_id,
                        'email' => $user->email
                    ));

                    $this->view->n_member = $m_members->save($member);
                }

                //newsletter email log
                $emails = array(
                    'old' => null,
                    'new' => $user->email
                );

                Cubix_Api::getInstance()->call('addNewlstterEmailLog', array($user->new_user_id, $user_type, 'add', $emails));
                //
//                var_dump('asdsasdasda');die;
                // will send activation email
                $user->reg_type = $user_type;
                Model_Hooks::postUserSignUp($user);
                $this->view->user_id = $user->new_user_id;
                $this->view->ok = true;


                // If user was Escort then we need to redirect to Private Area
                // It is available for them but is limited
                // ------------------------------------
                if('escort' == $user_type) {
                    $user = $model->getByUsernamePassword($data['username'], $data['password']);
                    Zend_Session::regenerateId();
                    Model_Users::setCurrent($user);
                    return $this->_response->setRedirect($this->view->getLink('dashboard') . '#complete-profile-step');
                }
                // ------------------------------------


                if (!is_null($this->_getParam('ajax'))) {
                    $result['msg'] = "
						<h1 style='margin-bottom:20px'><img src='/img/" . Cubix_I18n::getLang() . "_h1_memberssignup.gif' alt='Members signup' title='Members signup' /></h1>
						<div style='width:520px; background: none; margin-bottom:0' class='cbox-small'>
							<h3>" . Cubix_I18n::translate('congratulations') . "</h3>
							<p>" . Cubix_I18n::translate('successfully_signed_up', array('SITE_URL' => Cubix_Application::getById()->url, 'SITE_NAME' => Cubix_Application::getById()->title)) . "</p>

							<h3>" . Cubix_I18n::translate('complete_your_registration') . "</h3>
							<p>&nbsp;</p>

							<h4>
								" . Cubix_I18n::translate('signup_successful_04') . "
							</h4>
						</div>
					";
                    $result['signup'] = true;
                    echo json_encode($result);
                    die;
                } else {
                    if ($user_type == 'vip-member') {
                        $this->_session->want_premium = (object)array('member_data' => array('id' => $this->view->n_member->getId()), 'email' => $user->email);
                        header('Location: /' . Cubix_I18n::getLang() . '/private-v2/upgrade');
                        exit;
                        //$this->_helper->viewRenderer->setScriptAction('signup-success-cc');
                    } else {
                        $this->_helper->viewRenderer->setScriptAction('signup-success');
//                        $session = new Zend_Session_Namespace('resent_email_' . $req->user_id);
//
//                        if (!isset($session->resend_email)) {
//                            $session->resend_email = 1;
//                        } else {
//                            $session->resend_email += 1;
//                        }
//
//                        if ($session->resend_email <= 5) {
//                            $this->resendEmail($req->user_id);
//                            $this->view->message = __('email_resent');
//                        } else {
//                            $this->view->lock_resend_email = 1;
//                            header('Location: /' . Cubix_I18n::getLang());
//                            exit;
//                        }
//
//                        $this->view->user_type = $req->user_type;
//                        $this->view->user_id = $req->user_id;
//                        $this->view->ok = true;
                    }
                }
            }
        } elseif (isset($req->user_id) && is_numeric($req->user_id)) {
//            header('Location: /' . Cubix_I18n::getLang() . '/private-v2/signup-success');

            $session = new Zend_Session_Namespace('resent_email_' . $req->user_id);
            $fromPage = $this->_request->getParam('fromPage', null);

            if (!isset($session->resend_email)) {
                $session->resend_email = 1;
            } else {
                $session->resend_email += 1;
            }

            if ($fromPage == 'complete_profile' || $session->resend_email <= 5) {
                $this->resendEmail($req->user_id);
                $this->view->message = __('email_resent');
            } else {
                $this->view->lock_resend_email = 1;
                header('Location: /' . Cubix_I18n::getLang());
                exit;
            }

            if($fromPage == 'complete_profile') {
                die(json_encode([
                    'status' => 'success',
                    'msg' =>  __('email_resent')
                ]));
            }

            $this->view->user_type = $req->user_type;
            $this->view->user_id = $req->user_id;
            $this->view->ok = true;
            $this->_helper->viewRenderer->setScriptAction('signup-success');
        }

    }


    public function checkusernameAction(){
        $req = $this->_request;
        $client = new Cubix_Api_XmlRpc_Client();
        $validator = new Cubix_Validator();
        $username = $req->username;

        $signup_i18n = $this->view->signup_i18n = (object)array(
            'username_invalid' => 'Username must be at least 6 characters long',
            'username_invalid_characters' => 'Only "a-z", "0-9", "-" and "_" are allowed',
            'username_exists' => 'Username already exists, please choose another'
        );

        $model = new Model_Users();
        $has_bl_username = false;
        foreach ($this->blacklisted_usernames as $bl_username) {
            if (strpos($username, $bl_username) !== false) {
                $has_bl_username = true;
                BREAK;
            }
        }
        $err = '';
        if (strlen($username) < 6) {
            $err = $signup_i18n->username_invalid;
        } elseif (!preg_match('/^[-_a-z0-9]+$/i', $username)) {
            $err = $signup_i18n->username_invalid_characters;
        } //elseif (stripos($data['username'], 'admin') !== FALSE) {
        elseif ($has_bl_username) {
            $err = $signup_i18n->username_exists;
        } elseif ($client->call('Users.getByUsername', array($username))) {
            $err = $signup_i18n->username_exists;
        }

        echo $err;die;


    }
    public function signinMobileAction(){
        $this->view->layout()->setLayout('register');
    }

    public function signinAction()
    {
        $defs = Zend_Registry::get('defines');
        $page_type = $this->_request->getPathInfo();
        if ($page_type == 'private/advertise') {
            $this->_helper->viewRenderer('private/advertise', null, true);
        } elseif ($page_type == 'private/login') {
            $this->_helper->viewRenderer('private/login', null, true);
        }
        $this->view->layout()->setLayout('register');
        $this->view->page_type = $this->_getParam('type');

        //if first time login(activation of profile)
        $this->view->activated = $this->_request->activated;

        $this->view->data = array('username' => '');
        $this->view->errors = array();

        if ($this->_request->isPost()) {

            $username = trim($this->_getParam('username'));
            $password = trim($this->_getParam('password'));
            $remember_me = intval($this->_getParam('signed_in'));
            $ignore_unverified_email = $this->_getParam('ignore-unverified-email');

            $this->view->data['username'] = $username;

            $validator = new Cubix_Validator();

            if (!strlen($username)) {
                $validator->setError('username', __('required_username'));
            }

            if (!strlen($password)) {
                $validator->setError('password', __('required_password'));
            }

            if (strlen($username) && strlen($password)) {
                $model = new Model_Users();

                if (!$user = $model->getByUsernamePassword($username, $password)) {
                    $validator->setError('username', __('wrong_user_pass'));
                }

                if ($user) {
                    if ($this->_hasStatusBit($user->escort_data['escort_status'], ESCORT_STATUS_ADMIN_DISABLED) || $this->_hasStatusBit($user->escort_data['escort_status'], ESCORT_STATUS_DELETED)) {
                        $validator->setError('status_error', __('status_error'));
                    }

                    // Private Area is available for not verified emails if the user-type is ESCORT ONLY
                    // And this is possible only if user is sending request from login page
                    // If its from popup, then we just show, that the account is not available
                    // -----------------------------
                    if ( !($user->isEscort() && $ignore_unverified_email) && $user->status == -1) {
                        $validator->setError('verification_error', __('verification_error'));
                    }
                    // -----------------------------

                    if ($user->status == -4) {
                        $validator->setError('disabled_error', __('account_disabled'));
                    }

                }
            }

            $result = $validator->getStatus();

            if (!$validator->isValid()) {

                $this->view->errors = $result['msgs'];

                if (!is_null($this->_getParam('ajax'))) {
                    echo(json_encode($result));
                    ob_flush();
                    die;
                }
            } else {
                $times = [];
                $has_active_package = false;
                $is_susp = false;

                $client = new Cubix_Api_XmlRpc_Client();

                if ($user->user_type == 'agency') {
                    $_SESSION['fc_gender'] = 2;
                } else if ($user->user_type == 'member') {
                    $_SESSION['fc_gender'] = 1;
                } else if ($user->user_type == 'escort') {
                    $_SESSION['fc_gender'] = 2;

                    $t = microtime(TRUE);
                    $has_active_package = $client->call('Escorts.hasPaidActivePackageForEscort', array($user->id));
                    $times['hasPaidActivePackageForEscort'] = microtime(TRUE) - $t;

                    $m_e = new Model_EscortsV2();
                    $is_susp = $m_e->isSuspicious($user->id);
                }

                $_SESSION['fc_username'] = $username;
                $_SESSION['fc_password'] = $password;
				
				$user->lang = $client->call('Users.getLang', array($user->id, Cubix_I18n::getLang()));

                // UNCOMMENT WHEN DEPLOYED
                //Zend_Session::regenerateId();

                $user->sign_hash = md5(rand(100000, 999999) * microtime(true));
                $user->has_active_package = $has_active_package;
                $user->is_susp = $is_susp;

                // Commented temporary
                // -----------------------------------------------------

                /* Setting chat info */
                 $chat_info = array(
                    'nickName'	=> $user->username,
                    'userId'	=> $user->id,
                    'userType'	=> $user->user_type,
                    'imIsBlocked' => $user->im_is_blocked,
                    'link' => $this->view->getLink('member-info', array('username' => $user->username))
				);

                // -----------------------------------------------------


                if ($user->user_type == 'escort') {
                    $user->need_phone_verification = $client->call('Users.needPhoneVerificationByUserId', array($user->id));
					if($user->need_phone_verification) {
                        $hash = '#phone-verification';
                    }
                    // Commented temporary
                    // -----------------------------------------------------

                    //$m = new Model_EscortsV2();

                    //$e = $m->getByUserId($user->id);

                    /* MARCH 8 LOTTERY */
                    /*if($e->gender == GENDER_FEMALE){
                        $user->march8_status = $model->checkMarch8Lottery($user->id);
					}*/
                    /* $chat_info['nickName'] = $e->showname;
                    $chat_info['isSusp'] = $is_susp;
                    $chat_info['hasActivePackage'] = $has_active_package;
                    $chat_info['link'] = $this->view->getLink('profile', array(
                        'showname' => $e->showname,
                        'escort_id' => $e->id));
                    $chat_info['firstRow'] = $e->age ?
                        $e->age . ', ' . $defs['gender_options'][$e->gender] :
                        $defs['gender_options'][$e->gender];

                    $chat_info['secondRow'] = $e->base_city . ', ' . $e->country;

                    //Getting main image url
                    $parts = array();
                    if ( strlen($e->id) > 2 ) {
                        $parts[] = substr($e->id, 0, 2);
                        $parts[] = substr($e->id, 2);
                    }
                    else {
                        $parts[] = '_';
                        $parts[] = $e->id;
                    }
                    $catalog = implode('/', $parts);

                    $app = Cubix_Application::getById();
                    $chat_info['avatar'] = APP_HTTP . '://pic.' . $app->host . '/' . $app->host . '/' . $catalog . '/' . $e->photo_hash . '_lvthumb.' . $e->photo_ext;

                    if ($e->photo_hash) {
                        $user->avatar = APP_HTTP . '://pic.' . $app->host . '/' . $app->host . '/' . $catalog . '/' . $e->photo_hash . '_pa_avatar.' . $e->photo_ext;
					} */

                    //-----------------------------------------------------

                } elseif ($user->user_type == 'agency') {
                    $user->need_phone_verification = $client->call('Users.agencyNeedPhoneVerification', array($user->id));
                    $m = new Model_Agencies();
                    $a = $m->getByUserId($user->id);
                    $m->updateLastModified($a->id);

                    // when agency is missing any of these: agency name, phone number or booking email
                    // at each login we open directly the agency profile
                    // -----------------------------------------------------
                    if(empty($a->phone) || empty($a->name) || empty($a->email)) {
                        $hash = '#agency-profile';
                    }
                    if($user->need_phone_verification) {
                        $hash = '#phone-verification';
                    }
                    // -----------------------------------------------------


                    /* MARCH 8 LOTTERY */
                    /*$user->march8_status = $model->checkMarch8Lottery($user->id);*/

                    // Commented temporary
                    // -----------------------------------------------------

                    // $chat_info['nickName'] = $a->name;
                    // $chat_info['hasActivePackage'] = $has_active_package;

                    // -----------------------------------------------------

                } elseif ($user->user_type == 'member') {
                    //$chat_info['link'] = '/member/' . $user->username;
                    $follow_model = new Model_Follow();
                    $follow_model->checkPendingFollows($user->id, true);
                    $user->follow_list = $follow_model->getFollowList($user->id);

                    /* watching settings */
                    $m_m = new Model_Members();
                    $additional_info = $m_m->getAdditionalInfo($user->id);
                    $user->escorts_watched_type = $additional_info['escorts_watched_type'];
                    /**/
                }

                // Commented temporary
                // -----------------------------------------------------

                //$user->chat_info = $chat_info;

                // -----------------------------------------------------
                Model_Users::setCurrent($user);
                // Nor More Needed
                // -----------------------------------------------------

                // if ( $remember_me ) {
                //     $this->addRememberMeCook($username, $password);
                // }

                // -----------------------------------------------------

                $first_timeLogin = Model_Users::get_last_login_date($user->id);
                $first_timeLogin = (is_null($first_timeLogin) ? true : false);

                Model_Reviews::createCookieClientID($user->id);
                Model_Hooks::preUserSignIn($user->id);

                if (!is_null($this->_getParam('ajax'))) {
                    $result['signin_type'] = ucfirst($user->user_type);
                    $result['msgs'] = Cubix_I18n::translate('login_success');

                    // Hash can be specified above to (check in agency case)
                    // ---------------------------------
                    if(empty($hash))
                        $hash = '';
                    // ---------------------------------

                    if (
                        $user->user_type == 'escort' && (
                            (!$user->getEscort()->hasProfile()) ||
                            ($user->getEscort()->status & Model_Escorts::ESCORT_STATUS_PARTIALLY_COMPLETE) ||
                            $first_timeLogin ||
                            $user->status == -1
                        )
                    ) {
                        $hash = '#complete-profile-step';
                    }

                    $result['url'] = rtrim($this->view->getLink('dashboard'), '/') . $hash;

                    echo json_encode($result);
                } else {
                    if ($first_timeLogin) {
                        $hash = ($user->user_type == 'escort') ? '#complete-profile-step' : '#add-new';
                        $this->_response->setRedirect($this->view->getLink('dashboard') . $hash);
                    } else {
                        // Hash can be specified above to (check in agency case)
                        // ---------------------------------
                        if(empty($hash))
                            $hash = ($user->user_type == 'escort' && $user->getEscort()->status == 67) ? '#' : '';
                        // ---------------------------------

                        $this->_response->setRedirect($this->view->getLink('dashboard') . $hash);
                    }
                }

                //login into forum
                // -----------------------
                $forum2 = new Cubix_Forum2Api();
                $ret = $forum2->Login($user->username, $user->email);
				
                // -----------------------
                // Start Background Process
                // -----------------------------------------------------
                // ignore_user_abort();
                // session_write_close();
                // fastcgi_finish_request();
                if ( INTERNET_EXPLORER === true )
                {
                    header('location:/dashboard');
                }
                die;

                // -----------------------------------------------------
            }
        }
    }

    public function signoutAction()
    {
        Model_Users::setCurrent(NULL);
        Zend_Session::regenerateId();

        $cookie_name = 'signin_remember_' . Cubix_Application::getId();
        if (isset($_COOKIE[$cookie_name]) && $_COOKIE[$cookie_name]) {
            setcookie($cookie_name, null, strtotime('- 1 year'), "/");
        }
        $redirect_link = $this->view->getLink();

        $this->_response->setRedirect($redirect_link);
    }

    public function forgotAction()
    {
        $this->view->layout()->setLayout('register');
        $this->view->data = array('email' => '');
        $this->view->errors = array();

        if ($this->_request->isPost()) {
            $this->view->data['email'] = $email = trim($this->_getParam('email'));

            if (!strlen($email)) {
                $this->view->errors['email'] = Cubix_I18n::translate('fp_please_enter_email');
                return;
            }

            $model = new Model_Users();
            if (!($user = $model->forgotPassword($email))) {
                $this->view->errors['email'] = Cubix_I18n::translate('fp_email_not_registered');
                return;
            }

            Cubix_Email::sendTemplate('forgot_verification', $email, array(
                'username' => $user['username'],
                'hash' => $user['email_hash']
            ));

            $this->_helper->viewRenderer->setScriptAction('forgot-success');
        }
    }

    public function changePassAction()
    {
        $this->view->layout()->setLayout('register');

        $client = new Cubix_Api_XmlRpc_Client();
        if ($this->_request->isPost()) {
            $pass = $this->_getParam('password');
            $conf_pass = $this->_getParam('password2');
            $hash = $this->_getParam('hash');
            $id = intval($this->_getParam('id'));
            $validator = new Cubix_Validator();
            if (strlen($pass) < 6) {
                $validator->setError('password', __('password_invalid'));
            } elseif ($pass != $conf_pass) {
                $validator->setError('password2', __('passwords_dont_match'));
            } elseif (!preg_match('/[a-f0-9]{32}/', $hash)) {
                $validator->setError('hash', 'Invalid hash !');
            }
            $this->view->errors = array();
            $result = $validator->getStatus();

            if (!$validator->isValid()) {
                $this->view->errors = $result['msgs'];
            } else {
                $client->call('Users.checkUpdatePassword', array($id, $hash, $pass));
                $this->view->password_changed = true;
                #$this->_helper->viewRenderer->setScriptAction('change-pass-success');
            }
        } else {
            $hash = $this->_getParam('hash');
            $username = $this->_getParam('username');
            $error = false;
            if (!isset($username) || !isset($hash)) {
                $error = true;
            }
            $username = substr($username, 0, 24);
            if (!preg_match('/^[-_a-z0-9 ]+$/i', $username)) {
                $error = true;
            } elseif (!preg_match('/[a-f0-9]{32}/', $hash)) {
                $error = true;
            }

            if (!$error) {
                $id = $client->call('Users.getByUsernameMailHash', array($username, $hash));
                if ($id !== false) {
                    $this->view->id = $id;
                    $this->view->hash = $hash;

                } else {
					$this->_helper->viewRenderer->setScriptAction('forgot-pass-expired');
                    //$error = true;
                }

            }

            if ($error) {
                $this->_response->setRawHeader('HTTP/1.1 404 Not Found');
                $this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
            }
        }
    }

    public function activateAction()
    {
        $hash = $this->_getParam('hash');
        $email = $this->_getParam('email');

        $model = new Model_Users();


        if (!$user = $model->activate($email, $hash)) {
            $this->_response->setRedirect($this->view->getLink('signin'));
            return;
        }

        Model_Hooks::postUserActivate($user);
        $this->_response->setRedirect($this->view->getLink('signin'));
    }


    public function resendEmail($user_id)
    {

        $user = new Model_Users();
        $user = $user->getById($user_id);
        $user->activation_hash = $user->getUserActivationHash();
        $user->reg_type = $user->user_type;
        Model_Hooks::postUserSignUp($user);

    }

    private function _hasStatusBit($old_status, $status)
    {
        $result = true;
        $result = ($old_status & $status) && $result;
        return $result;
    }


    private function addRememberMeCook($username, $password)
    {
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $key = '_-_SignInRememberMe_-_' . Cubix_Application::getId();

        $login_data = serialize(array('username' => $username, 'password' => $password));

        $crypt_login_data = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $login_data, MCRYPT_MODE_ECB, $iv);
        $crypt_login_data = base64_encode($crypt_login_data);

        $cookie_name = 'signin_remember_' . Cubix_Application::getId();
        $cookie_lifetime = strtotime('+21 days');

        if ($this->getRequest()->getCookie($cookie_name)) {
            setcookie($cookie_name, $crypt_login_data, $cookie_lifetime, "/");
            $_COOKIE[$cookie_name] = $crypt_login_data;
        }
    }

    public function secureSigninAction()
    {
        $this->view->layout()->disableLayout();

        $secure_token = $this->_getParam('secure_token');

        $validator = new Cubix_Validator();
        $model = new Model_Users();
        $user = $model->getByToken($secure_token);

        $result = $validator->getStatus();

        if ( $user ) {
            $has_active_package = false;
            $is_susp = false;

            $client = new Cubix_Api_XmlRpc_Client();

            if ( $user->user_type == 'agency' ) {
                $_SESSION['fc_gender'] = 2;
            }
            else if ( $user->user_type == 'member' ) {
                $_SESSION['fc_gender'] = 1;
            }
            else if ( $user->user_type == 'escort' ) {
                $_SESSION['fc_gender'] = 2;

                /***********************************/
                $has_active_package = $client->call('Escorts.hasPaidActivePackageForEscort', array($user->id));
                $m_e = new Model_EscortsV2();
                $is_susp = $m_e->isSuspicious($user->id);
            }

            $user->lang = $client->call('Users.getLang', array($user->id, Cubix_I18n::getLang()));

            // UNCOMMENT WHEN DEPLOYED
            //Zend_Session::regenerateId();

            $user->sign_hash = md5(rand(100000, 999999) * microtime(true));
            $user->has_active_package = $has_active_package;
            $user->is_susp = $is_susp;

            if ( $user->user_type == 'escort' ) {
				if($user->need_phone_verification) {
					$hash = '#phone-verification';
				}
                $user->need_phone_verification = $client->call('Users.needPhoneVerificationByUserId', array($user->id));

            } elseif ( $user->user_type == 'agency' ) {
                $m = new Model_Agencies();
                $a = $m->getByUserId($user->id);
                // $m->updateLastModified($a->id);
                $user->need_phone_verification = $client->call('Users.agencyNeedPhoneVerification', array($user->id));


            } elseif ( $user->user_type == 'member' ) {
                $follow_model = new Model_Follow();
                $follow_model->checkPendingFollows($user->id, true);
                $user->follow_list = $follow_model->getFollowList($user->id);

                $m_m = new Model_Members();
                $additional_info = $m_m->getAdditionalInfo($user->id);
                $user->escorts_watched_type = $additional_info['escorts_watched_type'];
            }

            Model_Users::setCurrent($user);
            Model_Reviews::createCookieClientID($user->id);

            $first_timeLogin  = Model_Users::get_last_login_date( $user->id );
            $first_timeLogin = ( is_null($first_timeLogin) ?  true : false);

            Model_Hooks::preUserSignIn($user->id);

            // login into forum
            $forum2 = new Cubix_Forum2Api();
            $forum2->Login($user->username, $user->email);
            //

            if ( ! is_null($this->_getParam('ajax')) ) {
                $result['signin_type'] = ucfirst($user->user_type);
                echo json_encode($result);
                die;
            }
            else {
                if($first_timeLogin){
                    $hash  = ($user->user_type == 'escort') ? '#complete-profile-step':'#add-new';
                    $this->_response->setRedirect($this->view->getLink('dashboard').$hash);
                }else{
					if(empty($hash)){
						$hash = ($user->user_type == 'escort' && $user->getEscort()->status == 67) ? '#':'';
					}
                    $this->_response->setRedirect(rtrim($this->view->getLink('dashboard'), '/').$hash);
                }
            }
        }
    }

}
