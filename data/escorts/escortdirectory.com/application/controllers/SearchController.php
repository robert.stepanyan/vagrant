<?php

use Elasticsearch\ClientBuilder;

class SearchController extends Zend_Controller_Action
{

    public static $linkHelper;
    public static $cache;

    /**
     * @var Model_SearchRedirects
     */
    private $model;

    public function init()
    {
        self::$linkHelper = $this->view->getHelper('GetLink');
        self::$cache = $this->view->cache = Zend_Registry::get('cache');

        $this->model = new Model_SearchRedirects();
    }

    public function indexAction(){

        $searchKey = $this->view->searchKey = htmlspecialchars($this->_getParam('s'));
        $search_word = trim($searchKey);
        $this->view->page = $page = $this->_getParam('page', 1);
        $this->view->parsedSearchKey = explode(' ', ucwords($searchKey));


        // Log Searched Query
        // ---------------------------------------
        $model = new Model_SearchLog();
        if( !empty($searchKey) ){
            $data['query'] = $searchKey;
            $data['mobile_attempts'] = (int) Zend_Registry::get('isMobile');
            $data['desktop_attempts'] = (int) !Zend_Registry::get('isMobile');
            $model->add($data);
        }

        // Check if we have some url, that matches current search query
        // Then if it exists we redirect to that page :) Welcome to hell...
        // -------------------------------
        if($page <= 1) {
            $url = $this->_forwardToPageForSearch($search_word);
            if (!empty($url)) {
                return $this->_redirect($url);
            }
        }

        // Selecting escorts
        // -----------------------------
        $page_size = Model_Escort_List::HOMEPAGE_LISTING_SIZE - 1;

        $params = [
            'query' => $searchKey,
            'offset' => ($page - 1) * $page_size,
            'page_size' => $page_size,
        ];

        $result = Service_Escorts::filterListingMatches($params);
//        $similarResults used for 'Similar words suggestions' basic search.
//        $similarResults = Service_Escorts::getSimilarWords($searchKey);
        Service_Escorts::mutateDataForCurrentLang($result['hits']);
        #$escorts = Model_Escort_List::getListingDataByIds([10258], null, null, 'premiums');
        // -----------------------------

//        $similarWords = [];
//        foreach ($similarResults['hits']['hits'] as $_row) {
//            $term = $_row['_source']['showname'];
//            if (strtolower($searchKey) === strtolower($term)) continue;
//            $similarWords[] = strtolower($term);
//        }
//        $similarWords = array_unique($similarWords);

        
        /////EDIR-745////
        $client = new Cubix_Api_XmlRpc_Client();
        $geoData = Cubix_Geoip::getClientLocation();
        if (count($result['hits']))
        {
            foreach ($result['hits'] as $key => $esc_ort)
            {
                $blockedCountries = $client->call('Escorts.getBlockedCountries', array($esc_ort->id));
                if (in_array($geoData['country_iso'],array_keys($blockedCountries)))
                {
                    unset($result['hits'][$key]);
                }
            }
        }
        $result['total_count'] = count($result['hits']);
        ////////////////


        $this->view->found_total_count = $this->view->pagination_count = $result['total_count'];
        $model->updateCount(array('count' => $this->view->found_total_count, 'query' => $search_word));
        $this->view->search_value = $searchKey;
        $this->view->escorts = $result['hits'];
        $this->view->per_page = $per_page = $page_size;
//        $this->view->similarWords = $similarWords;
        if ($this->getRequest()->getHeader('X-PJAX', false) && IS_MOBILE){
            $escort_list = $this->view->render('search/index.phtml');
            die(mb_convert_encoding($escort_list, "UTF-8"));
        }

    }

    public function paging($count, $page, $perPage, $c = 6, $lf = 2)
    {
        $pages = ceil($count / $perPage);

        $start_page = 1;
        $end_page = $pages;

        if ( $c < $pages )  {
            $start_page = 1;
            $end_page = $c;
            if ( $lf < $page ) {
                $start_page = $page - $lf;
                $end_page = $page + $lf;
                if ( $page + $lf > $pages ) {
                    $start_page = $pages - $c;
                    $end_page = $pages;
                }
            }
        }

        $pages_arr = array();

        for ($i = $start_page; $i <= $end_page; $i++) {
            $pages_arr[] = $i;
        }

        return $pages_arr;
    }

    /**
     * Returns the url to which page you must be redirected
     * if you search for a specific things.
     * E.g.
     * @param String $query
     * @return String|null
     */
    private function _forwardToPageForSearch(String $query): ?String
    {
        // First of all check inside hardcoded url redirects made by admins
        // -----------------------------
        $link = $this->_checkForFixedRedirect($query);
        if(!empty($link)) {
            header("HTTP/1.1 301 Moved Permanently");
            return APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/' . $link;
        }
        // -----------------------------

        return null;

        /*$query = strtolower($query);
        $cacheKey = preg_replace('/\s+/', '', $query);
        $cacheKey = preg_replace('/[^A-Za-z0-9\-]/', '', $cacheKey); // Removes special chars.

        if(! $link = self::$cache->load($cacheKey))
        {
            $city = new Model_Cities();
            $categoryMap = [
                'escorts' => ['escort', 'female', 'girl', 'woman'],
                'boys' => ['boy', 'men', 'guy', 'gay', 'dude'],
                'trans' => ['trans', 'shemale'],
            ];
            $searchKeyWords = preg_split('/\s+/', $query); // Split into words

            // Detect which category was searched for
            // --------------------------
            $category = 'escorts'; // Default category
            foreach ($searchKeyWords as $word) {
                foreach ($categoryMap as $key => $variants) {
                    foreach($variants as $variant) {
                        if(stripos($word, $variant) !== false) {
                            $category = $key;
                            break;
                        }
                    }
                }
            }
            // --------------------------

            // Search between words for a name of a location
            // ---------------------------------------
            foreach ($searchKeyWords as $word) {
                if (strlen($word) < 3) continue;

                $foundRow = $city->checkExistence($word);

                if(count($foundRow) > 1) {
                    $foundRow = $city->getClosestMatchedFrom($foundRow, $searchKeyWords);
                }else {
                    $foundRow = $foundRow[0];
                }


                if (!empty($foundRow)) { // We have location matching to that query
                    if ($foundRow->city_id)  // Means it was a city
                        $link = self::$linkHelper->getLink('city', ['slug' => $foundRow->city_slug, 'id' => $foundRow->city_id, 'category' => $category]);
                    else if ($foundRow->region_id) { // It was region
                        $link = self::$linkHelper->getLink('country', array('slug' => $foundRow->country_slug, 'id' => $foundRow->country_id, 'category' => $category));
                        $link .= "?region_id=" . $foundRow->region_id;
                    } else if ($foundRow->country_id)  // It was country
                        $link = self::$linkHelper->getLink('country', ['slug' => $foundRow->country_slug, 'id' => $foundRow->country_id, 'category' => $category]);

                    self::$cache->save($link, $cacheKey);
                    break;
                }
            }
            // ---------------------------------------
        }

        return $link;*/
    }

    /**
     * Searches exclusively inside hardcoded url from backend
     * @param String $query
     * @return String|null
     */
    private function _checkForFixedRedirect(String $query): ?String
    {
        return $this->model->getByQuery($query);
    }

}