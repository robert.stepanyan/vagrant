<?php
class GotdController extends Zend_Controller_Action
{
	
	public function init()
	{
		$this->client = new Cubix_Api_XmlRpc_Client();
		$this->session = new Zend_Session_Namespace('gotd');
		$cache = Zend_Registry::get('cache');
		$this->view->user = $this->user = Model_Users::getCurrent();

        if ( ! $this->user ) {

            if($this->getRequest()->isXmlHttpRequest()){
                header("HTTP/1.1 403 Forbidden");
            }else{
                $this->_redirect($this->view->getLink('signin'));
            }

            return;
        }
		
		//$this->model = new Model_Escorts();
		$cache_key =  'v2_user_pva_' . $this->user->id;

		if ( $this->user->isAgency() ) {
			if ( ! $agency = $cache->load($cache_key) ){
				$agency = $this->user->getAgency();
				$cache->save($agency, $cache_key, array(), 300);
			}

			$this->agency = $this->view->agency = $agency;
			//$this->view->agency_escorts = Model_Escorts::getAgencyEscorts($agency->id);
		}
		else if ( $this->user->isEscort() ) {
			if ( ! $escort = $cache->load($cache_key) ) {
				$escort = $this->user->getEscort();
				$cache->save($escort, $cache_key, array(), 300);
			}

			$this->escort = $this->view->escort = $escort;
		}
	}

    public function indexAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setScriptAction('gotd');
        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $this->view->booked_days = $booked_days = $client->call('Billing.getGotdBookedDays');
        $this->view->selectedPayment = 'mmgbill';

        $is_agency = false;
        if ( $this->user->isAgency() ) {
            $is_agency = true;
            $escorts = $client->call('Billing.getAgencyEscortsForGotdED', array($this->agency->id));
            $escortsGendersArray = array();
            $has_girl_escort = false;
            $has_boy_escort = false;
            foreach ($escorts as $escort)
            {
                $escortsGendersArray[] = $escort['gender'];
            }
            if (in_array(1,$escortsGendersArray))
                $has_girl_escort = true;
            if (in_array(2,$escortsGendersArray))
                $has_boy_escort = true;

            $this->view->has_girl_escort = $has_girl_escort;
            $this->view->has_boy_escort = $has_boy_escort;
            if ( !$has_boy_escort )
            {
                $this->view->escorts = $escorts;
            }elseif ( $has_boy_escort )
            {
                $this->view->escorts = $escorts;
            }
        } else {
            $lang_id = Cubix_I18n::getLang();
            $this->view->working_cities =  $client->call('Escorts.getEscortWorkingCities', array($this->escort->id,$lang_id));
            $this->view->escort_packages = $escort_packages = $client->call('Billing.getEscortPackagesForGotdED', array($this->escort->id, Cubix_Application::getId()));
            $escort_package = $escort_packages[0];
            if (!empty($escort_package)){
                $expDateHour = date("G", $escort_package['expiration_date']);
                if ($expDateHour <= 5 ){
                    $escort_package['expiration_date'] = strtotime("-1 day", $escort_package['expiration_date']);
                }
            }
            $this->view->escort_package = $escort_package;
        }

        $this->view->is_agency = $is_agency;

        $this->view->region_relations = $region_relations = array();

        $this->view->gotd_product = $product = $client->call('Billing.getProduct', array(16));

        $this->view->user_id = $this->user->id;

        if ( $this->_request->isPost() ) {
            $hash  = base_convert(time(), 10, 36);
            $errors = array();
            $data = array(
                'city_id'	=> $this->_request->city_id,
                'escort_id' => $this->_request->escort_id,
                'days'		=> $this->_request->days,
                'package'	=> $this->_request->package
            );

            if ( ! $is_agency && count($escort_packages) == 1 ) {
                $data['package'] = 0;
            }

            if ( ! $data['city_id'] ) {
                $errors['city_id'] = Cubix_I18n::translate('city_required');
            } else {
                $city_also_check[] = $data['city_id'];
                if ( isset($region_relations[$data['city_id']]) ) {
                    $city_also_check = $region_relations[$data['city_id']];
                }
            }
            $this->view->city_also_check = $city_also_check;

            if ( ! strlen($data['package']) ) {
                $errors['package'] = Cubix_I18n::translate('selecte_package');
            } else {

                if ( $is_agency ) {

                } else {
                    $this->view->escort_package = $escort_package = $escort_packages[$data['package']];
                }
            }


            if ( $is_agency ) {
                if ( ! $data['escort_id'] ) {
                    $errors['escort_id'] = Cubix_I18n::translate('escort_required');
                }
            } else {
                $data['escort_id'] = $this->escort->id;
            }


            $selected_days = array();
            if ( $data['days'] ) {
                $selected_days = explode(';', $data['days']);
            }
            if ( ! count($selected_days) ) {
                $errors['days'] = Cubix_I18n::translate('date_required');
            } else {
                foreach($selected_days as $k => $day) {
                    $day = explode(',', $day);
                    //set to 5 to avoid daylight saving
                    $selected_days[$k] = mktime(5,0,0, $day[1], $day[2], $day[0]);
                }
            }

            foreach($selected_days as $day) {
                //Checking date to be not booked
                if ( $data['city_id'] ) {
                    foreach($city_also_check as $ct) {
                        foreach($booked_days[$ct] as $dt) {
                            if ( date('Y-m-d', $day) == date('Y-m-d', strtotime($dt['date'])) ) {
                                $errors['days'] = Cubix_I18n::translate('day_already_booked');
                                break;
                            }
                        }
                    }
                }

                //Escort could have only one package per day
                if ( $data['city_id'] ) {

                    $escort_gotds = $client->call('Billing.getEscortGotdBookedDays', array($data['escort_id']));

                    foreach($escort_gotds as $gotd) {
                        if ( date('Y-m-d', $day) == date('Y-m-d', strtotime($gotd['date'])) ) {
                            if ($this->escort['gender'] == 2)
                            {
                                $errors['days'] = Cubix_I18n::translate('only_one_botd_per_day');
                            }else{
                                $errors['days'] = Cubix_I18n::translate('only_one_gotd_per_day');
                            }
                        }
                    }

                    foreach($booked_days as $city_id => $dt) {
                        foreach($dt as $d) {
                            if ( date('Y-m-d', $day) == date('Y-m-d', strtotime($d['date'])) && $data['escort_id'] == $d['escort_id'] ) {
                                if ($this->escort['gender'] == 2)
                                {
                                    $errors['days'] = Cubix_I18n::translate('only_one__botd_per_day');
                                }else{
                                    $errors['days'] = Cubix_I18n::translate('only_one_gotd_per_day');
                                }
                            }
                        }
                    }
                }

                //Checking date to be in package active range
                if ( $data['escort_id'] ) {
                    if ( $is_agency ) {
                        $package = $escorts[$data['escort_id']]['packages'][$data['package']];
                        $package['expiration_date'] += 5 * 60 * 60;
                        if ( $day < $package['date_activated'] || $day > $package['expiration_date']  ) {
                            $errors['days'] = Cubix_I18n::translate('date_must_be_in_package_range');
                        }
                    }  else {
                        //If not in range of active and pending packages
                        //add 5 hours to avoid daylight saving
                        $escort_package['expiration_date'] += 5 * 60 * 60;
                        if ( ! ( $day > $escort_package['date_activated'] && $day < $escort_package['expiration_date'] ) ) {
                            $errors['days'] = Cubix_I18n::translate('date_must_be_in_package_range');
                        }
                    }
                }
            }
            if ( $is_agency ) {
                if ( in_array($escorts[$data['escort_id']]['packages'][$data['package']]['package_id'], array(125,102,128,130,132,107,105,97) )) {
                    $product['price'] = GIRL_OF_THE_DAY_WITHOUT_PACKAGE_PRICE;
                }else{
                    $product['price'] = GIRL_OF_THE_DAY_WITH_PACKAGE_PRICE;
                }
            } else {
                if ( in_array($escort_package['package_id'], array(125,102,128,130,132,107,105,97))) {
                    if ( $this->escort->gender == 1 )
                    {
                        $product['price'] = GIRL_OF_THE_DAY_WITHOUT_PACKAGE_PRICE;
                    }elseif( $this->escort->gender == 2 ){
                        $product['price'] = BOY_OF_THE_DAY_WITHOUT_PACKAGE_PRICE;
                    }
                }else{
                    if ( $this->escort->gender == 1 )
                    {
                        $product['price'] = BOY_OF_THE_DAY_WITH_PACKAGE_PRICE;
                    }elseif( $this->escort->gender == 2 ){
                        $product['price'] = BOY_OF_THE_DAY_WITH_PACKAGE_PRICE;
                    }
                }
            }

            $this->view->gotd_product = $product;

            $this->view->data = $data;
            $this->view->selected_days_count = $selected_days_count = count($selected_days);


            if ( count($errors) ) {
                $this->view->gotd_errors = $errors;

                if($this->getRequest()->isXmlHttpRequest()) {
                    exit(json_encode([
                        'status' => 'error',
                        'msgs' => $errors
                    ]));
                }

            } else {
                $book_id = $client->call('Billing.bookGotd', array($data['escort_id'], $data['city_id'], $selected_days));

                if ( $book_id ) {
                    $amount = $selected_days_count * $product['price'];
                    if($this->_request->payment_gateway == "mmgbill"){

                        $book_id = str_replace(':', 'x', $book_id);
                        $mmgBill = new Model_MmgBillAPIV2();

                        $hosted_url = $mmgBill->getHostedPageUrl(
                            $amount,
                            'GZ' . $book_id,
                            'http://www.escortdirectory.com' . $this->view->getLink('mmg-postback')
                        );

                    }elseif ($this->_request->payment_gateway == 'paysafe') {
                        $first_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->username));
                        $last_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->chat_info['nickName']));

                        if( empty($first_name)){
                            $first_name = 'NAME';
                        }

                        if(empty($last_name)){
                            $last_name = 'LASTNAME';
                        }

                        $reference = 'gotd-' . $book_id;

                        $customer = new Cubix_2000charge_Model_Customer();
                        $customer->setEmail($this->user->email);
                        $customer->setCountry("GB");
                        $customer->setFirstName($first_name);
                        $customer->setLastName($last_name);

                        $payment = new Cubix_2000charge_Model_Payment();
                        $payment->setPaymentOption("paysafe");
                        $payment->setHolder($first_name.' '.$last_name);

                        $transaction = new Cubix_2000charge_Model_Transaction();
                        $transaction->setCustomer($customer);
                        $transaction->setPayment($payment);
                        $transaction->setAmount($amount * 100);
                        $transaction->setCurrency("EUR");
                        $transaction->setIPAddress(Cubix_Geoip::getIP());
                        $transaction->setMerchantTransactionId($reference);

                        $host = 'https://' . $_SERVER['SERVER_NAME'];
                        $redirectUrls = new Cubix_2000charge_Model_RedirectUrls();
                        $redirectUrls->setReturnUrl($host . $this->view->getLink('billing-paysafe-success'));
                        $redirectUrls->setCancelUrl($host . $this->view->getLink('billing-paysafe-failure'));
                        $transaction->setRedirectUrls($redirectUrls);
                        $client = new Cubix_Api_XmlRpc_Client();
                        $res = Cubix_2000charge_Transaction::post($transaction);
                        $client->call('OnlineBillingV2.storeToken', array($res->id, $this->user->id, '2000charge'));
                        $hosted_url = $res->redirectUrl;

                    }
                }
                die(json_encode(array('status' => 'success', 'url' =>  $hosted_url)));
            }
        }
    }

    public function gotdSuccessAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setScriptAction('gotd-responses');
        $this->view->key = 'gotd_success_message';
    }

    public function gotdFailureAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setScriptAction('gotd-responses');
        $this->view->key = 'gotd_failure_message';
    }
	
}
