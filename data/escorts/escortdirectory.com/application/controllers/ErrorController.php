<?php

class ErrorController extends Zend_Controller_Action
{
    public function errorAction()
    {
        $this->view->layout()->setLayout('main');

        $cache = Zend_Registry::get('cache');
        $this->view->cache = $cache;

        $content = '<div class="clear mb-30 mobile"></div>';
        $errors = $this->_getParam('error_handler');
        $error_msg = $this->_getParam('error_msg');
        $param = $this->_getParam('param');
        $error_type = $this->_getParam('error_type');


        if (isset($error_type)) {
            $errors->type = $this->_getParam('error_type');
        }


        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER :
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION :
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE :
                $this->_response->setRawHeader('HTTP/1.1 404 Not Found');

                $content .= "<h1 class='uppercase'>" . __('error') . "</h1>";
                $content .= '<h2 class="uppercase">' . __('404_page_not_found') . "</h2>" . PHP_EOL;
                $content .= '<div class="clear mb-10"></div>';
                $content .= "<p class='text-grey'>" . __('page_was_not_found') . "</p>";

                $this->view->error_title = __('404_page_not_found');
                $this->view->error_type = __('not_found');

                break;
            case 'member-not-found' :
                $this->_response->setRawHeader('HTTP/1.1 404 Not Found');

                $content .= "<h1 class='uppercase'>" . __('error') . "</h1>";
                $content .= '<h2 class="uppercase">' . __('member_not_found') . "</h2>" . PHP_EOL;
                $content .= '<div class="clear mb-10"></div>';
                $content .= "<p class='text-grey'>" . __('member_was_not_found') . "</p>";

                $this->view->error_title = __('member_not_found');
                $this->view->error_type = __('member_was_not_found');

                break;
            case 'permission_denied':
                $this->_response->setRawHeader('HTTP/1.1 403 Forbidden');
                $content .= "<h1 class='uppercase'>" . __('error') . "</h1>";
                $content .= "<h2  class='uppercase'> 403 " . __('forbidden') . "</h2>";
                $content .= '<div class="clear mb-10"></div>';
                $content .= "<p class='text-grey'>" . __('permission_denied') . "</p>";
                break;
            default :
                $exception = $errors->exception;

                $this->view->error_title = __('unexpected_error');
                $this->view->error_type = __('unexpected');

                if ($exception) {
                    $content .= "<h2 class='uppercase'>" . __('error') . "</h2>" . PHP_EOL;
                    $content .= "<p class='text-grey'>" . __('seems_error') . "</p>";
                    $content .= '<div class="clear mb-10"></div>';

                    Cubix_Debug::log('URL: ' . $_SERVER['REQUEST_URI'] . ", " . $_SERVER['HTTP_REFERER'] . "\n" . $exception->getMessage() . PHP_EOL . $exception->getTraceAsString(), 'ERROR');
                } else if ($error_msg) {
                    $content = "<h2 class='uppercase'>" . $error_msg . "</h2>";

                    if ($param) {
                        $param = " (" . $param . ")";
                    }

                    Cubix_Debug::log('URL: ' . $_SERVER['REQUEST_URI'] . ", " . $_SERVER['HTTP_REFERER'] . "\n" . $error_msg . $param, 'ERROR');
                }
                break;
        }

        // EDIR-2572
        // ------------------------------------
        $geoData = Cubix_Geoip::getClientLocation();
        $modelCities = new Model_Cities();
        $geo_city = $this->view->geo_city = $modelCities->getByGeoIp();

        list(
            $escortsAround,
            $aroundLocationTitle,
            ) = Model_Escort_List::getAroundEscorts([], [], 6, 1,'escorts', $geo_city['city_id'], $geo_city['country_id']);

        $this->view->escortsAround = $escortsAround;
        $this->view->aroundLocationTitle = $aroundLocationTitle;
        // ------------------------------------

        $this->getResponse()->clearBody();
        $this->view->content = $content;
    }

	

	public function noDefaultLangAction()
	{
		header('HTTP/1.1 503 Service Temporarily Unavailable');

		$error_msg = $this->_getParam('error_msg');

		$m = ' ( No Default Language Defined. Please resync. )';

		$this->view->content = $content = "<h1>" . $error_msg . $m . "</h1>";

		if ( $param ) {
			$param = " (" . $param . ")";
		}

		Cubix_Debug::log($error_msg . $param . $m, 'ERROR');

		$content = $this->view->render('error/no-default-lang.phtml');
		echo $content;
		
		die;
	}
}
