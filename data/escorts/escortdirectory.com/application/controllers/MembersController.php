<?php

class MembersController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$username = $this->_request->username;
		$model = new Model_Members();
		$member_info = $model->getMembersInfoByUsername($username);
		
		if(!array_key_exists('username', $member_info)){
             header('HTTP/1.1 301 Moved Permanently');
             header('Location: /member/not-found');
             die;
		}
		
		if($member_info['langs']){
			$member_info['langs'] = unserialize($member_info['langs']);	
		}

		$this->view->member_info = $member_info;
		$this->view->username = $username;
		$this->view->avatar = $model->getLogoUrl($member_info['hash'], $member_info['ext'], $member_info['member_id'], 'thumb_cropper_ed');
		
		$comments = $model->getCommentsInfo($member_info['user_id']);
		$this->view->comments_count = $comments['count'];
		$this->view->last_comment = $comments['last'];

		$reviews = $model->getReviewsInfo($member_info['user_id']);
		$this->view->reviews_count = $reviews['count'];
		$this->view->last_review = $reviews['last'];
		
		$this->view->DEFINITIONS = Zend_Registry::get('defines');
		
		$follow_model = new Model_Follow();
		$this->view->top10 = $follow_model->getTop10($member_info['user_id']);
		
	}

	public function followInfoAction(){
		$this->view->layout()->disableLayout();
	}
}
