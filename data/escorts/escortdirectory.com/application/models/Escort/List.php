<?php

define('LISTING_SIZE', Zend_Registry::get('isMobile') ? 44 : 45);

class Model_Escort_List extends Cubix_Model
{
	const HOMEPAGE_LISTING_SIZE = LISTING_SIZE;

    /**
     * Pass array filled with escort ids and this func
     * will remove all data from elastic matching those ids
     * @param int $escortID
     * @param String $index
     * @return bool
     */
    public static function removeEscortFromElastic(Int $escortID, String $index = 'listing_escorts'): bool
    {
        try {
            $elasticClient = Zend_Registry::get('elastic-client');
            $params = ['index' => $index];

            if (!is_numeric($escortID)) return false;

            $params['id'] = $escortID;
            $response = $elasticClient->delete($params);

            return (strtolower($response['result']) == 'deleted');

        }catch (\Exception $e) {
            return false;
        }

    }

    /**
     * Pass array filled with escort ids and this func
     * will get all the data about escort and put into ElasticSearch
     * @param array $escortIds
     * @return bool
     */
    public static function fetchEscortAndStoreIntoCache (Array $escortIds) : bool
    {
        foreach($escortIds as $id) {

            if(!is_numeric($id)) continue;

            $sql = "SELECT
                    @has_premium_package :=
                    IF
                        (
                            FIND_IN_SET( 4, e.products ) > 0 
                            OR FIND_IN_SET( 5, e.products ) > 0 
                            OR FIND_IN_SET( 15, e.products ) > 0,
                            1,
                            0 
                        ) AS has_premium_package,
                    
                    @was_vip := (
                            SELECT
                                eic.is_vip 
                            FROM
                                escorts_in_cities eic
                            WHERE
                                eic.is_premium = 1 
                                AND eic.escort_id = e.id 
                            ORDER BY
                                eic.is_vip 
                                LIMIT 1 
                            ) AS is_vip,
                    IF ( @has_premium_package AND ( NOT @was_vip ), 1, 0 ) AS is_premium,
       
                    (
                        SELECT
                            CONCAT(eic.city_id, \"@\", CONCAT_WS(\";\", c.title_en, c.title_de, c.title_fr, c.title_es, c.title_it, c.title_ro))
                        FROM
                            escorts_in_cities eic
                        INNER JOIN cities c ON c.id = eic.city_id
                        WHERE
                            eic.is_base = 1 
                            AND eic.escort_id = e.id 
                        ORDER BY
                            eic.is_vip 
                            LIMIT 1 
                    ) AS base_city_concated,
       
                    e.id,
                    e.showname,
                    e.age,
                    UNIX_TIMESTAMP( e.date_registered ) AS date_registered,
                    e.user_id,
                    e.is_pornstar,
                    e.last_hand_verification_date,
                    e.is_inactive,
                    UNIX_TIMESTAMP( e.date_activated ) AS date_activated,
                    ct.slug AS city_slug,
                    ct.id AS city_id,
                    e.rates,
                    e.incall_type,
                    e.outcall_type,
                    e.is_new,
                    cr.id AS country_id,
                    cr.slug AS country_slug,
                    eic.gender,
                    eic.type,
                    GROUP_CONCAT( ct.title_en ) AS working_cities,
                    cr.title_en AS country,
                    ct.title_en AS city_title_en,
                    ct.title_de AS city_title_de,
                    ct.title_es AS city_title_es,
                    ct.title_ro AS city_title_ro,
                    ct.title_it AS city_title_it,
                    ct.title_fr AS city_title_fr,
                    cr.title_en AS country_title_en,
                    cr.title_de AS country_title_de,
                    cr.title_es AS country_title_es,
                    cr.title_ro AS country_title_ro,
                    cr.title_it AS country_title_it,
                    cr.title_fr AS country_title_fr,
                    ctt.title_en AS tour_city_en,
                    ctt.title_de AS tour_city_de,
                    ctt.title_es AS tour_city_es,
                    ctt.title_ro AS tour_city_ro,
                    ctt.title_it AS tour_city_it,
                    ctt.title_fr AS tour_city_fr,
                    e.about_en AS about_en,
                    e.about_de AS about_de,
                    e.about_es AS about_es,
                    e.about_ro AS about_ro,
                    e.about_it AS about_it,
                    e.about_fr AS about_fr,
                    e.verified_status,
                    e.photo_hash,
                    e.photo_ext,
                    e.photo_args,
                    e.photo_status,
                    e.incall_currency,
                    e.incall_price,
                    e.outcall_currency,
                    e.outcall_price,
                    e.comment_count,
                    e.review_count,
                    e.travel_place,
                    e.email,
                    e.website,
                    e.phone_instr,
                    e.phone_instr_no_withheld,
                    e.phone_instr_other,
                    e.phone_country_id,
                    e.disable_phone_prefix,
                    e.phone_number_free AS phone_number,
                    e.hit_count,
                    e.slogan,
                    e.incall_price,
                    e.date_last_modified,
                    e.hh_is_active,
                    e.is_on_tour AS is_tour,
                    e.tour_date_from,
                    e.tour_date_to,
                    e.user_id,
                    e.is_suspicious,
                    ct.id AS city_id,
                    e.agency_name,
                    e.agency_slug,
                    e.agency_id,
                    ulrt.refresh_date AS refresh_date,
                    eic.is_inactive
                FROM
                    escorts_in_cities eic
                    INNER JOIN escorts e ON e.id = eic.escort_id
                    INNER JOIN cities ct ON ct.id = eic.city_id
                    INNER JOIN countries cr ON cr.id = ct.country_id
                    LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
                    LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
                    LEFT JOIN currencies cur ON cur.id = e.incall_currency
                    LEFT JOIN exchange_rates er ON er.iso = cur.title 

                WHERE eic.escort_id = {$id}
                    
                GROUP BY
                    eic.escort_id 
                ORDER BY e.id";


            $escort = self::db()->fetchRow($sql);

            if(!empty($escort)) {
                return self::storeEscortIntoCache($escort);
            }
        }

        return false;
    }

    /**
     * Pass escort data as an argument, and it will update
     * cache (ElasticSearch) data
     * @param stdClass $escort
     * @return bool
     */
    public static function storeEscortIntoCache(stdClass & $escort) : bool
    {
        $elasticClient = Zend_Registry::get('elastic-client');
        $fields = [
            'is_inactive', 'id', 'is_tour', 'is_suspicious',
            'verified_status',  'date_activated', 'date_registered',
            'age', 'country_id', 'is_pornstar', 'travel_place',
            'is_new', 'phone_country_id', 'disable_phone_prefix',
            'city_slug', 'city_id', 'rates', 'incall_type', 'outcall_type',
            'country_slug', 'photo_hash', 'photo_ext', 'photo_args', 'photo_status',
            'application_id', 'incall_currency', 'incall_price', 'outcall_currency',
            'outcall_price', 'comment_count', 'review_count', 'website', 'phone_instr',
            'phone_instr_no_withheld', 'phone_instr_other', 'phone_country_id', 'disable_phone_prefix',
            'hit_count', 'slogan', 'date_last_modified', 'hh_is_active', 'tour_date_from', 'tour_date_to',
            'agency_slug', 'agency_id', 'user_id',

            'country_title_en',  'country_title_de',
            'country_title_es',  'country_title_ro',
            'country_title_it',  'country_title_fr',

            'city_title_en',  'city_title_de',
            'city_title_es',  'city_title_ro',
            'city_title_it',  'city_title_fr',

            'tour_city_en',  'tour_city_de',
            'tour_city_es',  'tour_city_ro',
            'tour_city_it',  'tour_city_fr',

            'about_en',  'about_de',
            'about_es',  'about_ro',
            'about_it',  'about_fr',

            'is_vip', 'is_premium', 'working_cities',
            'showname', 'email', 'phone_number',
            'agency_name', 'is_inactive'
        ];

        $body = [];
        foreach($fields as $field) {
            $body[$field] = $escort->{$field} ?? null;
        }
        $body['working_cities'] = explode(',', $body['working_cities']);

        // Temporary change, fast VERY URGENT, sorry guys :(
        // --------------------------------
        $baseCityData = [];

        $baseCityData['id'] = $body['city_id'] = explode('@', $escort->base_city_concated)[0] ?? null;

        $baseCityTitlesI18n = explode(';', explode('@', $escort->base_city_concated)[1]);
        $baseCityData['title_en'] = $body['city_title_en'] = $baseCityTitlesI18n[0] ?? null;
        $baseCityData['title_de'] = $body['city_title_de'] = $baseCityTitlesI18n[1] ?? null;
        $baseCityData['title_fr'] = $body['city_title_fr'] = $baseCityTitlesI18n[2] ?? null;
        $baseCityData['title_es'] = $body['city_title_es'] = $baseCityTitlesI18n[3] ?? null;
        $baseCityData['title_it'] = $body['city_title_it'] = $baseCityTitlesI18n[4] ?? null;
        $baseCityData['title_ro'] = $body['city_title_ro'] = $baseCityTitlesI18n[5] ?? null;

        $body['city'] = $baseCityData;
        // --------------------------------

        // Setting up gender possibilities
        // -----------------------------
        switch ($escort->gender) {
            case GENDER_FEMALE:
                $body['gender'] = ['female', 'woman', 'girl', 'girls'];
                break;
            case GENDER_MALE:
                $body['gender'] = ['male', 'men', 'boy', 'boys', 'guy', 'guys', 'gay', 'gays'];
                break;
            case GENDER_TRANS:
                $body['gender'] = ['trans', 'shemale'];
                break;
        }
        // -----------------------------

        // Setting up type possibilities
        // -----------------------------
        switch ($escort->type) {
            case ESCORT_TYPE_ESCORT:
                $body['type'] = ['escort', 'escorts'];
                break;
            case ESCORT_TYPE_BDSM:
                $body['type'] = ['bdsm', 'dominatrix', 'dom', 'sub', 'bondage'];
                break;
            case ESCORT_TYPE_MASSAGE:
                $body['type'] = ['massage'];
                break;
        }
        // -----------------------------

        $params = [
            'index' => 'listing_escorts',
            'id'    => $escort->id,
            'body'  => $body
        ];

        $response = $elasticClient->index($params);
        return ($response['_shards']['successful'] > 0 && $response['_shards']['failed'] == 0);
    }

    /**
     * @return array
     */
    public function getNewestForSitemap() : array {

        $sql = '
            SELECT eic.escort_id as id, e.showname, e.date_last_modified
            FROM escorts_in_cities eic 
            INNER JOIN escorts e ON e.id = eic.escort_id 
            LEFT JOIN users_last_refresh_time ulrt on ulrt.escort_id = eic.escort_id 
            WHERE 
                  FIND_IN_SET(21, e.products) > 0 AND 
                  e.is_new = 1 AND eic.participate_in_listing = 1 
                  AND eic.is_base = 1
            GROUP BY eic.escort_id 
            ORDER BY e.id 
            LIMIT 15000
        ';

        $result = self::db()->fetchAll($sql);

        return $result;
    }

    public static function getActiveFilter($filter, $where_query = array(), $is_upcomingtour = false, $is_tour = false)
	{
		if ($filter['show_all_agency_escorts']) {
			unset($filter['show_all_agency_escorts']);		
		}

		/*$full_text = "";
		if ( isset($filter['search_string']) && $filter['search_string'] ) {
			if ( isset($filter['search_string'][0]) && strlen($filter['search_string'][0]) ) {
				$full_text = " AND MATCH (search_string) AGAINST ('" . $filter['search_string'][0] . "' IN BOOLEAN MODE) ";
			}
		}*/
		unset($filter['search_string']);		

		if ( $is_tour ) {
			if ( isset($filter['eic.gender = 1']) ) unset($filter['eic.gender = 1']);

			if ( ! $is_upcomingtour ) {
				$filter['eic.is_tour = 1'] = array();
			}
			else {
				$filter['eic.is_upcoming = 1'] = array();
			}
		} 
		else {
			// Only escorts with base city (or city tour) will be shown in this list
			if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
				$filter['eic.is_base = 1'] = array();
			}
			else {
				$filter['eic.is_upcoming = 0'] = array();
			}
		}

		$join = " ";

		if ( isset($filter['incall-outcall']) && $filter['incall-outcall'] ) {
			if($filter['incall-outcall'] == 1){
				$filter['e.incall_type IS NOT NULL'] = array();
			}
			elseif($filter['incall-outcall'] == 2){
				$filter['e.outcall_type IS NOT NULL'] = array();
			}
		}
		unset($filter['incall-outcall']);

		if ( isset($filter['orientation']) && count($filter['orientation']) ) {			
			$filter['e.sex_orientation IN ( ' . implode(',', $filter['orientation']) . ' )'] = array();
		}
		unset($filter['orientation']);

		$current_currency = "USD";
		if ( isset($filter['currency']) && $filter['currency'] ) {
			$current_currency = $filter['currency'];
		}
		
		$has_price_filter = false;
		if ( $filter['currency'] || $filter['price-from'] || $filter['price-to'] ) {
			$currency_rate = Model_Currencies::getSelectedCurrencyRate($current_currency);
			$has_price_filter = $current_currency;
			$join .= " 
				INNER JOIN currencies cur ON cur.id = e.incall_currency 
				INNER JOIN exchange_rates er ON er.iso = cur.title 
			";
		}
		
		if ( isset($filter['price-from']) && $filter['price-from'] ) { 
			$filter['(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' >= ' . $filter['price-from']] = array();
		}
		
		if ( isset($filter['price-to']) && $filter['price-to'] ) {
			$filter['(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' <= ' . $filter['price-to']] = array();
		}
		
		unset($filter['currency']);
		unset($filter['price-from']);
		unset($filter['price-to']);

		if ( isset($filter['cr.id = ?']) ) {
			$join .= " INNER JOIN countries cr ON cr.id = e.country_id ";
		}

		if ( isset($filter['r.id = ?']) ) {
			$join .= " INNER JOIN regions r ON r.id = eic.region_id	";
		}

		if ( isset($filter['is-online-now']) && $filter['is-online-now'] ) {
			$filter['ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1'] = array();
		}
		
		if ( (isset($filter['is-online-now']) && $filter['is-online-now']) || isset($filter['ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1']) ) {
			$join .= " INNER JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id ";
		}
		unset($filter['is-online-now']);
		unset($filter['ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1']);

		if ( isset($filter['with-video']) && $filter['with-video'] ) {
			$filter['e.has_video = 1'] = array();
			//$join .= " INNER JOIN video v ON v.escort_id = e.id ";
		}
		unset($filter['with-video']);

		// Caching logic 
		
		ksort($filter);
		$cache = Zend_Registry::get('cache');
		$cache_filter = self::_generateCacheKey($filter);
		$cache_filter = preg_replace('#[^a-zA-Z0-9<>()=_]#', '_', $cache_filter);
		$cache_wq = '';
		
		foreach($where_query as $where_q){
			if(strlen($where_q['filter_query']) > 0 ){
				$cache_wq .= '_' . $where_q['filter_query'] . '_';
			}
		}
		
		$cache_wq = preg_replace('#[^a-zA-Z0-9<>()=_]#', '_', $cache_wq);
		$cache_key = 'active_filters_'.Cubix_Application::getId().'_';
				
		$cache_key .= sha1($cache_filter.'_'.$cache_wq);
		/*$filter_benchmark = array(
			'filter' => $cache_filter,
			'where_query' => $cache_wq,
			'cache_key' => $cache_key
		);*/
		if ( !$results = $cache->load($cache_key) ) {
			$where = self::getWhereClause($filter, true);
			//var_dump($where);die;

			if(is_null($where)){ 
				$where = ' 1 ';
			}

			$wqa = array();
			if ( count($where_query) ) {
				foreach( $where_query as $wq ) {

					if ( strlen($wq['query']) ) {
						$where_query = ' AND ' . $wq['filter_query'];

						$where .= $where_query;
						$wqa[] = array('filter_query' => $wq['filter_query'], 'query' => $wq['query'] );
					}
				}
			}

			$metadata = self::db()->describeTable('escort_filter_data');
			$column_names = array_keys($metadata);
			unset($column_names[0]);	

			$select = array('COUNT(*) as cnt ');
			foreach ( $column_names as $column ) {
				$select[] = 'SUM(' . $column . ') AS ' . $column;
			}
			
			$sql = '
				SELECT 
					' . implode(', ', $select) . ' 
				FROM ( SELECT fd.*
				FROM escort_filter_data fd
				INNER JOIN escorts_in_cities eic ON eic.escort_id = fd.escort_id
				INNER JOIN escorts e ON e.id = eic.escort_id
				INNER JOIN cities ct ON eic.city_id = ct.id
				' . $join . '
				' . (! is_null($where) ? 'WHERE ' . $where : '') . '
				GROUP BY fd.escort_id ) f
			';
			
			//var_dump($sql);die;
			
			// TO FORCE INNER JOIN currencies, exchange_rates tables
			if($has_price_filter){
				$filter['currency'] = $has_price_filter;
			}

            $results = array('result' => self::db()->fetchRow($sql), 'global_filter' => $filter , 'where_query' => $wqa, 'is_upcoming' => $is_upcomingtour, 'is_tour' => $is_tour);
            $cache->save($results, $cache_key);
		}
		/*else{
			$filter_benchmark['from_cache'] = 1;
		}
		self::db()->insert('filter_benchmark', $filter_benchmark);*/
		return $results;
	}
	
	public static function getFilterDefines($p_top_category, $is_upcomingtour, $is_tour)
	{
		$cache_key = 'defines_cache_key_' . Cubix_Application::getId(). '_lang_'. Cubix_I18n::getLang().'_'. preg_replace('#[^a-zA-Z0-9_]#', '_', $p_top_category);
		$cache = Zend_Registry::get('cache');
		if ( !$defined_values = $cache->load($cache_key) ) {
			$defs = Zend_Registry::get('definitions');
			$defined_values = $defs['escorts_filter_v2'];

			switch($p_top_category )
			{
				case 'newest':
					$filter["eic.gender = 1"] = array();
					break;
				case 'escorts':
					$filter["eic.gender = 1 AND e.type = 1"] = array();
					break;
				case 'agency-escorts':
					$filter["eic.gender = 1 AND e.type = 1"] = array();
					break;
				case 'bdsm':
					$filter["eic.gender = 1 AND e.type = 2"] = array();
					break;
				case 'bdsm-boys':
					$filter["eic.gender = 2 AND e.type = 2"] = array();
					break;
				case 'bdsm-trans':
					$filter["eic.gender = 3 AND e.type = 2"] = array();
					break;
				case 'boys':
					$filter["eic.gender = 2 AND e.type = 1"] = array();
					break;
				case 'trans':
					$filter["eic.gender = 3 AND e.type = 1"] = array();
					break;
				case 'citytours':
					$filter["eic.is_tour = 1"] = array();
					break;
				case 'upcomingtours':
					$filter["eic.is_upcoming = 1"] = array();
					break;
				case 'massage':
					$filter["eic.gender = 1 AND e.type = 3"] = array();
					break;
				case 'massage-boys':
					$filter["eic.gender = 2 AND e.type = 3"] = array();
					break;
				case 'massage-trans':
					$filter["eic.gender = 3 AND e.type = 3"] = array();
					break;
				default:
					$filter["eic.gender = 1"] = array();
			}	

			$exists_filters = self::getActiveFilter($filter, array(), $is_upcomingtour, $is_tour);
			$exists_filter_results = $exists_filters['result'];
			
			foreach($defined_values as $title => $list){
								
				if($title == 'available-for-incall' || $title == 'available-for-outcall'){
					$prefix = 'available_for_';
				}
				else{
					$prefix = str_replace('-','_',$title ) . '_';
				}
				
				foreach( $list as $k => $value ) {
					$key = $prefix . strtolower(str_replace('-','_', $value['value']));

					if ( !$exists_filter_results->$key) {
						unset($defined_values[$title][$k]);
					}
				}
			}
						
			$cache->save($defined_values, $cache_key);
		}
		return $defined_values;
			
	}		
			
	public static function getMainPremiumSpot()
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		$sql = '
			SELECT
				/* escorts table */
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
				e.rates, e.incall_type, e.outcall_type, e.is_new, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status, e.is_pornstar, e.last_hand_verification_date,
				e.products, e.slogan, e.date_last_modified,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.is_suspicious,e.comment_count, e.review_count,
				
				ct.title_' . $lng . ' AS city,

				/* seo_entity_instances table */
				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,

				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				/* misc */
				1 AS is_premium, ' . Cubix_Application::getId() . ' AS application_id, e.hh_is_active, e.is_online
			FROM escorts e
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = e.id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 2 AND se.slug = "escort")
			WHERE
				eic.gender = ' . GENDER_FEMALE . ' AND e.is_main_premium_spot = 1
			GROUP BY e.id
		';

		$cache = Zend_Registry::get('cache');
		$cache_key = 'main_premium_spot_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang();

		if ( ! $escorts = $cache->load($cache_key) ) {
			
			$escorts = self::db()->fetchAll($sql);
			
			/* seo heading escorts */
			if ($escorts)
			{
				$es = array();
				foreach ($escorts as $e)
				{
					$es[] = $e->id;
				}
				$es_str = implode(',', $es);

				$query = "SELECT e.id, sei.heading_escort_{$lng} as heading_escort
					FROM escorts e
					INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
					INNER JOIN seo_entity_instances sei ON sei.primary_id = eic.city_id
					INNER JOIN seo_entities se ON se.application_id = " . Cubix_Application::getId() . " AND se.slug = 'city' AND sei.entity_id = se.id
					WHERE e.id IN ({$es_str})
				";
				$result = self::db()->fetchAll($query);

				foreach ($result as $r)
				{				
					foreach ($escorts as $e)
					{
						if ($e->id == $r->id)
						{
							$e->heading_escort = $r->heading_escort;
							break;
						}
					}
				}
			}
			/**/

			$cache->save($escorts, $cache_key, array());
		}

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		shuffle($escorts);

		// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$sess->main_premium_spot = array();
		foreach ( $escorts as $escort ) {
			$sess->main_premium_spot[] = $escort->showname;
		}
		// </editor-fold>


		return $escorts;
	}

    public static function getFilteredWhereQuery($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $only_vip = false, $s_filter_params = array(), $is_grouped = false, $geo_data = array(), $list_type = 'simple', $city_id = null, $country_id = null, $current_currency = null)
    {
        $lng = Cubix_I18n::getLang();
        $sql_join = ' ';
        $fields = ' ';
        $having = [];
        $is_premium = "eic.is_premium,";
        $OrderInactiveVipPremiums = '';
        if($filter['premium_regular']){
            unset($filter['premium_regular']);
        }
        // Remove VIP and premium escorts from normal listing
        elseif ( ! in_array($ordering, array('vips', 'premiums', 'gotd')) ) {
            $filter['eic.participate_in_listing = 1'] = array();
            unset($filter['eic.is_inactive = 0']);
            //$OrderInactiveVipPremiums = " eic.is_inactive ASC, eic.is_vip DESC, eic.is_premium DESC, ";
            $is_premium = "IF( eic.is_premium OR
	EXISTS (
		SELECT
			1
		FROM
			escorts_in_cities AS eic1
		WHERE
			eic1.escort_id = eic.escort_id
		AND eic1.is_premium = 1
	), 1, 0) AS is_premium,";
        }
        else {

            unset($filter['eic.is_premium = 0 AND eic.is_vip = 0']);
            unset($filter['(( e.package_id <> 103 AND e.package_id <> 104) OR e.package_id IS NULL)']);
            $filter['eic.is_inactive = 0'] = array();
        }
        // Remove VIP and premium escorts from normal listing

        if ( isset($filter['remove_package_query']) ) {
            unset($filter['eic.is_premium = 0 AND eic.is_vip = 0']);
            unset($filter['(( e.package_id <> 103 AND e.package_id <> 104) OR e.package_id IS NULL)']);
            unset($filter['remove_package_query']);
        }

        if ( isset($s_filter_params['currency']) && $s_filter_params['currency'] && ($s_filter_params['price-from'] || $s_filter_params['price-to']) ) {
            $current_currency = $s_filter_params['currency'];
        }

        $currency_rate = Model_Currencies::getSelectedCurrencyRate($current_currency);
        if ( isset($s_filter_params['price-from']) && $s_filter_params['price-from'] ) {
            $filter[] = '(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' >= ' . $s_filter_params['price-from'];
        }
        if ( isset($s_filter_params['price-to']) && $s_filter_params['price-to'] ) {
            $filter[] = '(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' <= ' . $s_filter_params['price-to'];
        }

        if (date('G') < 2)
            $date = 2;
        else
            $date = date('G');

        if ( 'price' == substr($ordering, 0, 5) ) {
            $filter[] = 'e.incall_price IS NOT NULL';
        }

        /* #### Advanced search sql
        No need to check s_filter_params as for vip and premium they are already added in filter array
        UPDATE (EDUARD): In this version we as someone mentioned above, it doesnt work as it should so I removed the condition
        */
        if ( TRUE or in_array($ordering, array('vips', 'premiums')) ) {
            if ( isset($s_filter_params['gender']) && count($s_filter_params['gender']) ) {
                unset($filter['eic.gender = 1']);
                $filter['eic.gender = ? '] = intval($s_filter_params['gender']);
            }

            if (isset($s_filter_params['keywords']) && count($s_filter_params['keywords'])) {
                $fields .= ' ,GROUP_CONCAT( ek.keyword_id ) AS keywords ';
                foreach ($s_filter_params['keywords'] as $keyword) {
                    $having[] = "FIND_IN_SET($keyword, keywords) ";
                }
                $sql_join .= " INNER JOIN escort_keywords ek ON ek.escort_id = e.id ";
            }

            if ( isset($s_filter_params['orientation']) && count($s_filter_params['orientation']) ) {
                $filter[] = 'e.sex_orientation IN ( ' . implode(',', $s_filter_params['orientation']) . ' )';
            }

            if ( isset($s_filter_params['ethnicity']) && count($s_filter_params['ethnicity']) ) {
                $filter[] = 'e.ethnicity IN ( ' . implode(',', $s_filter_params['ethnicity']) . ' )';
            }

            if ( isset($s_filter_params['language']) && count($s_filter_params['language']) ) {
                $languagesQ = '';
                foreach ($s_filter_params['language'] as $value) {
                    $languagesQ .= " e.languages like '%".$value."%' AND";
                }
                $languagesQ = substr($languagesQ, 0, -3);
                $filter[] = " (".$languagesQ. " ) ";
            }

            if ( isset($s_filter_params['nationality']) && count($s_filter_params['nationality']) ) {
                $languagesQ = ' e.nationality_id IN(';
                foreach ($s_filter_params['nationality'] as $value) {
                    $languagesQ .= $value.",";
                }
                $languagesQ = substr($languagesQ, 0, -1);
                $languagesQ .= ") ";
                $filter[] = $languagesQ;
            }

            if ( isset($s_filter_params['hair-color']) && count($s_filter_params['hair-color']) ) {
                $filter[] = 'e.hair_color IN ( ' . implode(',', $s_filter_params['hair-color']) . ' )';
            }

            if ( isset($s_filter_params['hair-length']) && count($s_filter_params['hair-length']) ) {
                $filter[] = 'e.hair_length IN ( ' . implode(',', $s_filter_params['hair-length']) . ' )';
            }

            if ( isset($s_filter_params['cup-size']) && count($s_filter_params['cup-size']) ) {
                $filter[] = 'e.cup_size IN ( "' . implode('","', $s_filter_params['cup-size']) . '" )';
            }

            if ( isset($s_filter_params['eye-color']) && count($s_filter_params['eye-color']) ) {
                $filter[] = 'e.eye_color IN ( ' . implode(',', $s_filter_params['eye-color']) . ' )';
            }

            if ( isset($s_filter_params['pubic-hair']) && count($s_filter_params['pubic-hair']) ) {
                $filter[] = 'e.pubic_hair IN ( ' . implode(',', $s_filter_params['pubic-hair']) . ' )';
            }

            if ( isset($s_filter_params['sex-availability']) && count($s_filter_params['sex-availability']) ) {
                $service_forQ = '(';

                foreach ($s_filter_params['sex-availability'] as $value) {
                    $service_forQ .= " FIND_IN_SET('". $value."' ,e.sex_availability) OR";
                }
                $service_forQ = substr($service_forQ, 0, -2).") ";
                $filter[] = $service_forQ;

            }

            if ( isset($s_filter_params['service']) && count($s_filter_params['service']) ) {
                // EDIR-2492
                foreach($s_filter_params['service'] as $serviceId) {
                    $having[] = ' FIND_IN_SET ( ' . $serviceId . ', service_id )';
                }
                $fields .= ', GROUP_CONCAT(es.service_id) AS service_id ';
                //$filter[] = 'es.service_id IN ( ' . implode(',', $s_filter_params['service']) . ' )';
                $sql_join .= " INNER JOIN escort_services es ON es.escort_id = e.id ";
            }

            if ( isset($s_filter_params['incall-outcall']) && $s_filter_params['incall-outcall'] ) {
                if($s_filter_params['incall-outcall'] == 1){
                    $filter[] = 'e.incall_type IS NOT NULL';
                }
                elseif($s_filter_params['incall-outcall'] == 2){
                    $filter[] = 'e.outcall_type IS NOT NULL';
                }
            }

            if ( isset($s_filter_params['incall']) && count($s_filter_params['incall']) ) {
                $incall_string = '';
                foreach($s_filter_params['incall'] as $incall_val){
                    $value = explode('_', $incall_val);
                    $incall_string .= $value[1].',';
                }
                $incall_string = substr($incall_string, 0, -1);
                $filter[] = 'e.incall_type IN ( ' . $incall_string . ' )';
            }

            if ( isset($s_filter_params['incall-hotel-room']) && count($s_filter_params['incall-hotel-room']) ) {
                $filter[] = 'e.incall_hotel_room IN ( ' . implode(',', $s_filter_params['incall-hotel-room']) . ' )';
            }

            if ( isset($s_filter_params['outcall']) && count($s_filter_params['outcall']) ) {
                $outcall_string = '';
                foreach($s_filter_params['outcall'] as $incall_val){
                    $value = explode('_', $incall_val);
                    $outcall_string .= $value[1].',';
                }
                $outcall_string = substr($outcall_string, 0, -1);
                $filter[] = 'e.outcall_type IN ( ' . $outcall_string . ' )';
            }

            if ( isset($s_filter_params['travel-place']) && count($s_filter_params['travel-place']) ) {
                $filter[] = 'e.travel_place IN ( ' . implode(',', $s_filter_params['travel-place']) . ' )';
            }

            if ( (isset($s_filter_params['is-online-now']) && $s_filter_params['is-online-now']) || isset($filter['ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1']) ) {
                $filter[] = 'ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1';
            }
            unset($filter['ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1']);
            unset($filter['is-online-now']);

            if ( isset($filter['online-now']) && $filter['online-now'] ) {
                $filter[] = 'ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1';
            }
            unset($filter['online-now']);
        }

        if(in_array($ordering, array('vips', 'premiums', 'gotd'))) {
            /*if ( isset($s_filter_params['keywords']) && count($s_filter_params['keywords']) ) {
                $sql_join .= " INNER JOIN escort_keywords ek ON ek.escort_id = e.id ";
            }
            if ( isset($s_filter_params['service']) && count($s_filter_params['service']) ) {
                $sql_join .= " INNER JOIN escort_services es ON es.escort_id = e.id ";
            }*/
            if($ordering == 'gotd'){
                $fields .= ', if(FIND_IN_SET(16, e.products) > 0, 1, 0 ) as is_gotd ';
            }

            if($ordering == 'vips'){
                $fields .= ', if(FIND_IN_SET(18, e.products) > 0, 1, 0 ) as is_vip_plus , if(FIND_IN_SET(19, e.products) > 0, 1, 0 ) as is_vip2';
            }
        }

        /* #### END advanced sql */


        $natural_photo = false;
        if (isset($s_filter_params['with-natural-photo']) && $s_filter_params['with-natural-photo'] ||
            isset($filter['with-natural-photo']) && $filter['with-natural-photo']) {
            $natural_photo = true;
            $sql_join .=  " INNER JOIN natural_pics n_pics ON n_pics.escort_id = e.id ";
        }

        unset($filter['with-natural-photo']);
        /*if ( isset($s_filter_params['with-video']) && $s_filter_params['with-video'] ) {
            $sql_join .= " INNER JOIN video v ON v.escort_id = e.id ";
        }*/
        /* #### new search sql */



        // Only escorts with base city (or city tour) will be shown in this list
        if ( ! $city_id && ! $country_id ) {
            $filter['eic.is_base = 1'] = array();
        } else {
            $filter[] = 'eic.is_upcoming = 0';
        }

        if ( $ordering == "premiums" || $ordering == "vips" ) {
            $del_val = "eic.is_base = 1";
            while(($key = array_search($del_val, $filter)) !== false) {
                unset($filter[$key]);
            }
        }


        $show_splited_agency_escorts = true;
        if ( isset($filter['show_all_agency_escorts']) ) {
            unset($filter['show_all_agency_escorts']);
            $show_splited_agency_escorts = false;
        }

        if ( in_array($ordering, array('vips', 'premiums')) ) {
            unset($filter['show_all_agency_escorts']);
            unset($filter['eic.should_display_agency_escort = 1']);
            $show_splited_agency_escorts = false;
        }

        if ( $show_splited_agency_escorts ) {
            $filter['eic.should_display_agency_escort = 1'] = array();
        }

        //$with_video = false;
        if ( isset($filter['with-video']) && $filter['with-video'] ) {
            $filter['e.has_video = 1'] = array();
            //$with_video = true;
            //$sql_join .= " INNER JOIN video v ON v.escort_id = e.id ";
        }
        unset($filter['with-video']);


        if(isset($s_filter_params['alert_id']) && $s_filter_params['alert_id'] > 0){
            $cur_user = Model_Users::getCurrent();
            if($cur_user && $cur_user->user_type == 'member'){
                $cityAlertModel =  new Model_CityAlerts();
                $escortIds = $cityAlertModel->getAlertEscorts($s_filter_params['alert_id'],$cur_user->id);

                if($escortIds){
                    $escortExist = array();
                    $in = 'e.id IN (';
                    foreach ($escortIds as $escortId) {
                        if(!in_array($escortId['escort_id'], $escortExist)){
                            $in .= $escortId['escort_id'].',';
                            $escortExist[] = $escortId['escort_id'];
                        }
                    }
                    $in = substr($in, 0, -1);
                    $in .= ')';
                    $filter[$in] = array();
                }
            }else{
                die();
            }
        }

        if(isset($filter['original_sort'])) {
            $originalSort = $filter['original_sort'];
            unset($filter['original_sort']);
        }

        $taggedWhere = '';
        if(isset($filter['tagged_escorts'])) {
            $taggedWhere .= ' AND e.id IN ('.$filter['tagged_escorts'].')';
            unset($filter['tagged_escorts']);
        }

        $where = self::getWhereClause($filter, true);
        $order = self::_mapSorting($ordering, $is_grouped);

        $where .= $taggedWhere;
        /*if ( $with_video ) {
            $filter['with-video'] = 'with-video';
        }*/

        if($natural_photo){
            $filter['with-natural-photo'] = 'with-natural-photo';
        }

        $page = (int) $page;
        if ( $page < 1 ) $page = 1;
        $limit = ($page_size * ($page - 1)) . ', ' . $page_size;

        $group_by = " GROUP BY eic.escort_id ";
        $distance = ', NULL AS distance ';

        $mustShowPaidNearbyGirls = $originalSort == 'close-to-me' && in_array($ordering, array('vips', 'premiums'));
        $mustShowCloseEscorts = ($mustShowPaidNearbyGirls) ||
            ($ordering == 'close-to-me' && count($geo_data) && !in_array($ordering, array('vips', 'premiums')));

        if($mustShowPaidNearbyGirls) {
            $order = ' distance ASC, ' . $order;
        }

        if ($mustShowCloseEscorts) {
            if (array_key_exists("latitude", $geo_data) && strlen($geo_data['latitude']) > 0 && array_key_exists("longitude", $geo_data) && strlen($geo_data['longitude']) > 0) {
                $distance = ',
					((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						))
						)
					)) AS distance
				';
            }
        }

        $fields .= $distance;
        is_null($where) ? $where = ' 1 ' . Cubix_Countries::blacklistCountryWhereCase() : $where .= Cubix_Countries::blacklistCountryWhereCase();

        // Setup "is online" filter
        // -----------------------------
        if(isset($s_filter_params['is-online-now']) && $s_filter_params['is-online-now'] == '1') {
            $having[] = ' is_login = 1 ';
        }
        // -----------------------------

        $isQueryForRegularEscorts = in_array($ordering, array('vips', 'premiums')) == false;
        $aboutField = Model_EscortsV2::nonEmptyMultilangField('about', 'e', $lng);

        $sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar, e.last_hand_verification_date, e.is_inactive,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated, ct.slug AS city_slug, ct.id AS city_id, ct.title_' . $lng . ' AS city, e.rates,
				e.incall_type, e.outcall_type, e.is_new, cr.id AS country_id, cr.slug AS country_slug, cr.title_' . $lng . ' AS country, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_args, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
				e.travel_place, e.email, e.website, e.phone_instr, e.phone_instr_no_withheld, e.phone_instr_other,
				e.phone_country_id, e.disable_phone_prefix, e.phone_number_free AS phone_number,
				IF (ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE), 1, 0) AS is_login, e.allow_show_online,
				e.hit_count, e.slogan, e.incall_price, e.date_last_modified, '.$is_premium.' eic.is_vip, e.hh_is_active,
				e.is_on_tour AS is_tour, e.tour_date_from, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city,
				e.is_suspicious, e.is_online, ct.id AS city_id, ' . $aboutField . ', e.agency_name,
				(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' AS incall_price_converted,
				e.agency_slug, e.agency_id, ulrt.refresh_date AS refresh_date ' . $fields . '
			FROM escorts_in_cities eic /*USE INDEX(by_googo3)*/
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
			/*LEFT JOIN countries crtt ON crtt.id = ctt.country_id*/
			' . $sql_join . '
			LEFT JOIN currencies cur ON cur.id = e.incall_currency
			LEFT JOIN exchange_rates er ON er.iso = cur.title

			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			' . $group_by . (! empty($having) ? ' HAVING ' . implode(' AND ', $having) : '' ) . ' 
			ORDER BY ' .$OrderInactiveVipPremiums. $order .
            ( $isQueryForRegularEscorts ? ' LIMIT ' . $limit : '');

        if($ordering == 'vips' && $_GET['___'] == 'v') {
            die($sql);
        }

        if($ordering == 'premiums' && $_GET['___'] == 'p') {
            die($sql);
        }

        $escorts = self::db()->fetchAll($sql);
        $count = self::db()->fetchOne('SELECT FOUND_ROWS()');

        return $escorts;
    }

    public static function getQueryFrom (&$filter, &$ordering = 'random', &$page = 1, &$page_size = 50, &$s_filter_params = array(), &$is_grouped = false, &$geo_data = array(), &$list_type = 'simple', &$city_id = null, &$country_id = null, &$current_currency = null, $forPage = null)
    {

        $sql_join = ' ';
        $fields = ' ';
        $having = [];
        //$is_premium = ",eic.is_premium";
        $OrderInactiveVipPremiums = '';

        if($filter['premium_regular']){
            unset($filter['premium_regular']);

        }

        // Remove VIP and premium escorts from normal listing
        elseif ( ! in_array($ordering, array('vips', 'premiums')) AND $forPage != 'advanced_search' ) {
            $filter['eic.participate_in_listing = 1'] = array();
            unset($filter['eic.is_inactive = 0']);
            $OrderInactiveVipPremiums = " eic.is_inactive ASC, ";


        }
        else {
            unset($filter['eic.is_premium = 0 AND eic.is_vip = 0']);
            unset($filter['(( e.package_id <> 103 AND e.package_id <> 104) OR e.package_id IS NULL)']);
            $filter['eic.is_inactive = 0'] = array();
        }

        // Remove VIP and premium escorts from normal listing

        if ( isset($filter['tagged_escorts']) ) {
            $filter[] = 'e.id IN ('.$filter['tagged_escorts'].')';
            unset($filter['tagged_escorts']);
        }

        if ( isset($filter['remove_package_query']) ) {
            unset($filter['eic.is_premium = 0 AND eic.is_vip = 0']);
            unset($filter['(( e.package_id <> 103 AND e.package_id <> 104) OR e.package_id IS NULL)']);
            unset($filter['remove_package_query']);
        }

        if ( isset($s_filter_params['currency']) && $s_filter_params['currency'] && ($s_filter_params['price-from'] || $s_filter_params['price-to']) ) {
            $current_currency = $s_filter_params['currency'];
        }

        $currency_rate = Model_Currencies::getSelectedCurrencyRate($current_currency);
        $inner_join_rates = false;
        if ( isset($s_filter_params['price-from']) && $s_filter_params['price-from'] ) {
            $filter[] = '(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' >= ' . $s_filter_params['price-from'];
            $inner_join_rates = true;
        }
        if ( isset($s_filter_params['price-to']) && $s_filter_params['price-to'] ) {
            $filter[] = '(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' <= ' . $s_filter_params['price-to'];
            $inner_join_rates = true;
        }

        if($inner_join_rates){
            $sql_join .= " INNER JOIN currencies cur ON cur.id = e.incall_currency ";
            $sql_join .= " INNER JOIN exchange_rates er ON er.iso = cur.title ";
        }

        if (date('G') < 2)
            $date = 2;
        else
            $date = date('G');

        /*if ( 'price' == substr($ordering, 0, 5) ) {
            $filter[] = 'e.incall_price IS NOT NULL';
        }*/

        /* #### Advanced search sql
        No need to check s_filter_params as for vip and premium they are already added in filter array*/
        
        if ( ! in_array($ordering, array('vips', 'premiums')) ) {
            if ( isset($s_filter_params['gender']) && count($s_filter_params['gender']) ) {
                unset($filter['eic.gender = 1']);
                $filter['eic.gender = ? '] = intval($s_filter_params['gender']);
            }

            if (isset($s_filter_params['keywords']) && count($s_filter_params['keywords'])) {
                $fields .= ' ,GROUP_CONCAT( ek.keyword_id ) AS keywords ';
                foreach ($s_filter_params['keywords'] as $keyword) {
                    $having[] = "FIND_IN_SET($keyword, keywords) ";
                }
                $sql_join .= " INNER JOIN escort_keywords ek ON ek.escort_id = e.id ";
            }

            if ( isset($s_filter_params['orientation']) && count($s_filter_params['orientation']) ) {
                $filter[] = 'e.sex_orientation IN ( ' . implode(',', $s_filter_params['orientation']) . ' )';
            }

            if ( isset($s_filter_params['ethnicity']) && count($s_filter_params['ethnicity']) ) {
                $filter[] = 'e.ethnicity IN ( ' . implode(',', $s_filter_params['ethnicity']) . ' )';
            }

            if ( isset($s_filter_params['language']) && count($s_filter_params['language']) ) {
                $languagesQ = '';
                foreach ($s_filter_params['language'] as $value) {
                    $languagesQ .= " e.languages like '%".$value."%' AND";
                }
                $languagesQ = substr($languagesQ, 0, -3);
                $filter[] = " (".$languagesQ. " ) ";
            }

            if ( isset($s_filter_params['nationality']) && count($s_filter_params['nationality']) ) {
                $languagesQ = ' e.nationality_id IN(';
                foreach ($s_filter_params['nationality'] as $value) {
                    $languagesQ .= $value.",";
                }
                $languagesQ = substr($languagesQ, 0, -1);
                $languagesQ .= ") ";
                $filter[] = $languagesQ;
            }

            if ( isset($s_filter_params['hair-color']) && count($s_filter_params['hair-color']) ) {
                $filter[] = 'e.hair_color IN ( ' . implode(',', $s_filter_params['hair-color']) . ' )';
            }

            if ( isset($s_filter_params['hair-length']) && count($s_filter_params['hair-length']) ) {
                $filter[] = 'e.hair_length IN ( ' . implode(',', $s_filter_params['hair-length']) . ' )';
            }

            if ( isset($s_filter_params['cup-size']) && count($s_filter_params['cup-size']) ) {
                $filter[] = 'e.cup_size IN ( "' . implode('","', $s_filter_params['cup-size']) . '" )';
            }

            if ( isset($s_filter_params['eye-color']) && count($s_filter_params['eye-color']) ) {
                $filter[] = 'e.eye_color IN ( ' . implode(',', $s_filter_params['eye-color']) . ' )';
            }

            if ( isset($s_filter_params['pubic-hair']) && count($s_filter_params['pubic-hair']) ) {
                $filter[] = 'e.pubic_hair IN ( ' . implode(',', $s_filter_params['pubic-hair']) . ' )';
            }

            if ( isset($s_filter_params['sex-availability']) && count($s_filter_params['sex-availability']) ) {
                $service_forQ = '(';

                foreach ($s_filter_params['sex-availability'] as $value) {
                    $service_forQ .= " FIND_IN_SET('". $value."' ,e.sex_availability) AND";
                }
                $service_forQ = substr($service_forQ, 0, -3).") ";
                $filter[] = $service_forQ;

            }

            if ( isset($s_filter_params['service']) && count($s_filter_params['service']) ) {
                // EDIR-2492
                foreach($s_filter_params['service'] as $serviceId) {
                    $having[] = ' FIND_IN_SET ( ' . $serviceId . ', service_id )';
                }
                    #$filter[] = 'es.service_id IN ( ' . implode(',', $s_filter_params['service']) . ' )';
                $sql_join .= " INNER JOIN escort_services es ON es.escort_id = e.id ";
                $fields .= ', GROUP_CONCAT(es.service_id) AS service_id ';
            }

            if ( isset($s_filter_params['incall-outcall']) && $s_filter_params['incall-outcall'] ) {
                if($s_filter_params['incall-outcall'] == 1){
                    $filter[] = 'e.incall_type IS NOT NULL';
                }
                elseif($s_filter_params['incall-outcall'] == 2){
                    $filter[] = 'e.outcall_type IS NOT NULL';
                }
            }

            if ( isset($s_filter_params['incall']) && count($s_filter_params['incall']) ) {
                $incall_string = '';
                foreach($s_filter_params['incall'] as $incall_val){
                    $value = explode('_', $incall_val);
                    $incall_string .= $value[1].',';
                }
                $incall_string = substr($incall_string, 0, -1);
                $filter[] = 'e.incall_type IN ( ' . $incall_string . ' )';
            }

            if ( isset($s_filter_params['incall-hotel-room']) && count($s_filter_params['incall-hotel-room']) ) {
                $filter[] = 'e.incall_hotel_room IN ( ' . implode(',', $s_filter_params['incall-hotel-room']) . ' )';
            }

            if ( isset($s_filter_params['outcall']) && count($s_filter_params['outcall']) ) {
                $outcall_string = '';
                foreach($s_filter_params['outcall'] as $incall_val){
                    $value = explode('_', $incall_val);
                    $outcall_string .= $value[1].',';
                }
                $outcall_string = substr($outcall_string, 0, -1);
                $filter[] = 'e.outcall_type IN ( ' . $outcall_string . ' )';
            }

            if ( isset($s_filter_params['travel-place']) && count($s_filter_params['travel-place']) ) {
                $filter[] = 'e.travel_place IN ( ' . implode(',', $s_filter_params['travel-place']) . ' )';
            }

            /*if ( (isset($s_filter_params['is-online-now']) && $s_filter_params['is-online-now']) || $ordering == 'last-connection'  ) {
                $sql_join .= " INNER JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id ";
            }*/

            if ( isset($filter['is-online-now']) && $filter['is-online-now'] || isset($filter['ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1']) ) {
                /*$sql_join .= " INNER JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id ";*/
                $filter[] = 'ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1';
            }
            unset($filter['is-online-now']);
            unset($filter['ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1']);

        }
        else{
            if ( isset($s_filter_params['keywords']) && count($s_filter_params['keywords']) ) {
                $sql_join .= " INNER JOIN escort_keywords ek ON ek.escort_id = e.id ";
            }
            if ( isset($s_filter_params['service']) && count($s_filter_params['service']) ) {
                $sql_join .= " INNER JOIN escort_services es ON es.escort_id = e.id ";
            }

            if($ordering == 'vips'){
                $fields .= ', if(FIND_IN_SET(18, e.products) > 0, 1, 0 ) as is_vip_plus , if(FIND_IN_SET(19, e.products) > 0, 1, 0 ) as is_vip2';
            }

        }

        /* #### END advanced sql */


        $natural_photo = false;
        if (isset($s_filter_params['with-natural-photo']) && $s_filter_params['with-natural-photo'] ||
            isset($filter['with-natural-photo']) && $filter['with-natural-photo']) {
            $natural_photo = true;
            $sql_join .=  " INNER JOIN natural_pics n_pics ON n_pics.escort_id = e.id ";
        }

        unset($filter['with-natural-photo']);
        /*if ( isset($s_filter_params['with-video']) && $s_filter_params['with-video'] ) {
            $sql_join .= " INNER JOIN video v ON v.escort_id = e.id ";
        }*/
        /* #### new search sql */



        // Only escorts with base city (or city tour) will be shown in this list
        if ( ! $city_id ) {
            $filter['eic.is_base = 1'] = array();
        } else {
            $filter[] = 'eic.is_upcoming = 0';
        }

        if ( $ordering == "premiums" || $ordering == "vips" ) {
            $del_val = "eic.is_base = 1";
            while(($key = array_search($del_val, $filter)) !== false) {
                unset($filter[$key]);
            }
        }


        $show_splited_agency_escorts = true;
        if ( isset($filter['show_all_agency_escorts']) ) {
            unset($filter['show_all_agency_escorts']);
            $show_splited_agency_escorts = false;
        }

        if ( in_array($ordering, array('vips', 'premiums')) ) {
            unset($filter['show_all_agency_escorts']);
            unset($filter['eic.should_display_agency_escort = 1']);
            $show_splited_agency_escorts = false;
        }

        if ( $show_splited_agency_escorts ) {
            $filter['eic.should_display_agency_escort = 1'] = array();
        }

        //$with_video = false;
        if ( isset($filter['with-video']) && $filter['with-video'] ) {
            $filter['e.has_video = 1'] = array();
            //$with_video = true;
            //$sql_join .= " INNER JOIN video v ON v.escort_id = e.id ";
        }
        unset($filter['with-video']);

        if ($ordering == 'filled-with-around-cities') {
            unset($filter['eic.country_id = ?']);
            unset($filter['eic.city_id = ?']);
            $filter[] = 'e.last_login_date IS NOT NULL';
            $filter[] = 'e.age IS NOT NULL';
        }

        if(isset($s_filter_params['alert_id']) && $s_filter_params['alert_id'] > 0){
            $cur_user = Model_Users::getCurrent();
            if($cur_user && $cur_user->user_type == 'member'){
                $cityAlertModel =  new Model_CityAlerts();
                $escortIds = $cityAlertModel->getAlertEscorts($s_filter_params['alert_id'],$cur_user->id);

                if($escortIds){
                    $escortExist = array();
                    $in = 'e.id IN (';
                    foreach ($escortIds as $escortId) {
                        if(!in_array($escortId['escort_id'], $escortExist)){
                            $in .= $escortId['escort_id'].',';
                            $escortExist[] = $escortId['escort_id'];
                        }
                    }
                    $in = substr($in, 0, -1);
                    $in .= ')';
                    $filter[$in] = array();
                }
            }else{
                die();
            }
        }

        $where = self::getWhereClause($filter, true);
        $order = self::_mapSorting($ordering, $is_grouped, $forPage);

        if($forPage == 'advanced_search') {
            $fields .= ', e.is_vip, e.is_premium';
        }

        /*if($forPage != 'advanced_search') {      //EDIR-324   //commented for EDIR-443
            if($ordering == 'newest') {
                $where .= ' AND COALESCE(e.date_activated, e.date_registered) > NOW() - INTERVAL 1 WEEK ';
            }
        }*/

        if($natural_photo){
            $filter['with-natural-photo'] = 'with-natural-photo';
        }

        $page = (int) $page;
        if ( $page < 1 ) $page = 1;
       
        $skip = ($page_size * ($page - 1));

        if ($forPage != 'advanced_search' AND !empty($_SESSION['first_page_size']) && is_int($_SESSION['first_page_size'])){
            $skip += abs($_SESSION['first_page_size'] - $page_size);
        }

        if ( $page_size === 6 )
        {
            $skip = 0;
            if ( $country_id )
                $where .= ' AND eic.country_id = '.$country_id;


        }

        $take = $page_size;
        $limit = $skip . ', ' . $take;

        // --------------------------------------------

        if ($ordering == 'filled-with-around-cities') {

            // EDIR-2560
            // ---------------------------
            if (isset($s_filter_params['eic.city_id IN']) && !empty($s_filter_params['eic.city_id IN'])) {
                $where .= ' AND eic.city_id IN ( ' . $s_filter_params['eic.city_id IN'] . ' )';
                unset($s_filter_params['eic.city_id IN']);
            }

            if(isset($s_filter_params['eic.escort_id NOT IN']) && !empty($s_filter_params['eic.escort_id NOT IN'])) {
                $where .= ' AND eic.escort_id NOT IN ( ' . $s_filter_params['eic.escort_id NOT IN'] . ' )';
                unset($s_filter_params['eic.escort_id NOT IN']);
            }
            // ---------------------------
        }

        $group_by = " GROUP BY eic.escort_id ";
        $distance = ', NULL AS distance ';
        
        if($s_filter_params['eic.escort_id NOT IN']){
            $where .= ' AND eic.escort_id NOT IN ( ' . $s_filter_params['eic.escort_id NOT IN'] . ' )';
        }

        is_null($where) ? $where = ' 1 ' . Cubix_Countries::blacklistCountryWhereCase() : $where .= Cubix_Countries::blacklistCountryWhereCase();
        //$geo_data = array("ip"=>"130.193.121.242","country"=>"Armenia","country_iso"=>"am","region"=>"Kotayk'","city"=>"Abovyan","latitude"=>floatval(40.26741027832),"longitude"=>floatval(44.626560211182));
        if ( $ordering == 'close-to-me' && count($geo_data) && is_null($city_id) && is_null($country_id) && !in_array($ordering, array('vips', 'premiums'))) {
            if (array_key_exists("latitude", $geo_data) && strlen($geo_data['latitude']) > 0 && array_key_exists("longitude", $geo_data) && strlen($geo_data['longitude']) > 0) {
                $distance = ',
					((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - eic.latitude))/2), 2) +
							COS(RADIANS(eic.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - eic.longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - eic.latitude))/2), 2) +
							COS(RADIANS(eic.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - eic.longitude))/2), 2)
						))
						)
					)) AS distance
				';
            }
        }

        if ($forPage != 'advanced_search' && count($geo_data) && is_null($city_id) && is_null($country_id) )
        {
            $nearby_city_ids = Model_Cities::getNearbyCityIds($geo_data);
            if(strlen($nearby_city_ids) > 0){
                $where .= ' AND eic.city_id IN ('.$nearby_city_ids.')';
            }
        }elseif ( $forPage == 'advanced_search' && is_null($city_id) && is_null($country_id) ){
            $nearby_city_ids = Model_Cities::getNearbyCityIds($geo_data);
            if(strlen($nearby_city_ids) > 0){
                $where .= ' OR eic.city_id IN ('.$nearby_city_ids.') AND '.$where;
            }
        }

        if($ordering == 'price-asc' || $ordering == 'price-desc'){
            $sql_join .= " LEFT JOIN currencies cur ON cur.id = IFNULL( e.incall_currency, e.outcall_currency ) ";
            $sql_join .= " LEFT JOIN exchange_rates er ON er.iso = cur.title";
            $fields .= ',( IFNULL( e.incall_price, e.outcall_price ) / er.rate) * ' . round($currency_rate->rate) . ' AS incall_price_converted';
        }
        $fields .= $distance;

        if($ordering == 'recommended_escorts') {
            $fields .= ',CASE 
                    WHEN ulrt.refresh_date > e.date_last_modified THEN ulrt.refresh_date
                    ELSE e.date_last_modified
                end as last_modified_date';
        }

        if($forPage != 'advanced_search') {      //EDIR-324
            if($ordering == 'age') {
                $where .= ' AND age IS NOT NULL ';
            }
        }

        return array(
            $fields,
            $sql_join,
            $where,
            $group_by,
            $OrderInactiveVipPremiums,
            $order,
            $limit,
            $having
        );
    }

    /**
     * @param $filter
     * @param string $ordering
     * @param int $page
     * @param int $page_size
     * @param int $count
     * @param bool $sess_name
     * @param bool $only_vip
     * @param array $s_filter_params
     * @param bool $is_grouped
     * @param array $geo_data
     * @param string $list_type
     * @param null $city_id
     * @param null $country_id
     * @param null $current_currency
     * @param int $vipPremCount
     * @param array $having
     * @return array
     */
    public static function getEscortIdsOfQuery($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $only_vip = false, $s_filter_params = array(), $is_grouped = false, $geo_data = array(), $list_type = 'simple', $city_id = null, $country_id = null, $current_currency = null, $vipPremCount = 0, $having = [], &$nearbyCount = 0)
    {

        $location_sub_query = '';

        if (($country_id || $city_id) AND $sess_name != 'advanced_search') {
            $location_sub_query = ' FIND_IN_SET(21, e.products) > 0 AND ( eic.is_vip != 1 AND eic.is_premium != 1) AND ';
        }

        list(
            $fields,
            $sql_join,
            $where,
            $group_by,
            $OrderInactiveVipPremiums,
            $order,
            $limit,
            $having
        ) = self::getQueryFrom($filter, $ordering, $page, $page_size, $s_filter_params, $is_grouped, $geo_data, $list_type, $city_id , $country_id, $current_currency, $sess_name);

        $sql = '
			SELECT SQL_CALC_FOUND_ROWS
				eic.escort_id as id, eic.is_featured * (FLOOR(RAND() * 10) + 1) as is_featured
				 ' . $fields . '
			FROM escorts_in_cities eic FORCE INDEX (for_selbyids_query)
			INNER JOIN escorts e ON e.id = eic.escort_id
			LEFT JOIN users_last_refresh_time ulrt on ulrt.escort_id = eic.escort_id
			' . $sql_join . '
			' . (!is_null($where) ? 'WHERE ' . $location_sub_query . $where : '') . '
			' . $group_by . '
			' . (!empty($having) ? ' HAVING ' . implode(' AND ', $having) : '') . '

			ORDER BY ' . $OrderInactiveVipPremiums . $order . '
			LIMIT ' . $limit . '
		';

    	if($_GET['QAQ2']) {
    	    die($sql);
        }

        $escorts = self::db()->fetchAll($sql);
        $escort_ids = array();
        foreach($escorts as $escort){
            $escort_ids[] = $escort->id;
        }
        $count = self::db()->fetchOne('SELECT FOUND_ROWS()');

        if ($ordering == 'close-to-me' && $sess_name == 'advanced_search')
        {
            $nbcsql = preg_replace('/OR eic\.city_id IN/','AND eic.city_id IN', $sql);
            $nbcsql = substr($nbcsql, 0, strpos($nbcsql, "LIMIT"));
            self::db()->fetchAll($nbcsql);
            $nearbyCount = self::db()->fetchOne('SELECT FOUND_ROWS()');
            setcookie('near_by_count', $nearbyCount, time() + (60 * 10), "/");
        }elseif( $sess_name == 'advanced_search' ){
            setcookie('near_by_count', $count, time() + (60 * 10), "/");
        }
        return $escort_ids;
    }

    public static function getListingDataByIds($escort_ids, $current_currency, $city_id = null, $order = null, $forPage = null)
    {
        $currency_rate = Model_Currencies::getSelectedCurrencyRate($current_currency);
        $lng = $_COOKIE['ln'] ? $_COOKIE['ln'] : Cubix_I18n::getLang();

        $where = '';
        if($city_id){
            $where = ' AND eic.city_id = '. $city_id;
        }

        if($order) {
            $order = self::_mapSorting($order);
        }

        $fields = [
            'is_vip' => 'eic.is_vip',
            'is_premium' => 'eic.is_premium',
        ];

        // For Advanced Search we need to show VIP/PREMIUM badge
        // no matter what city/country is searched
        // if the girl has at least one vip in some city, she MUST be vip in advanced search
        // ---------------------------

        // ---------------------------
        /*if ($forPage == 'advanced_search') {
            $fields['is_vip'] = 'e.is_vip';
            $fields['is_premium'] = 'e.is_premium';
        }*/

        $aboutField = Model_EscortsV2::nonEmptyMultilangField('about', 'e', $lng);

        $sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar, e.last_hand_verification_date, e.is_inactive,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated, ct.slug AS city_slug, ct.id AS city_id, ct.title_' . $lng . ' AS city, e.rates,
				e.incall_type, e.outcall_type, e.is_new, cr.id AS country_id, cr.slug AS country_slug, cr.title_' . $lng . ' AS country, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_args, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
				e.travel_place, e.email, e.website, e.phone_instr, e.phone_instr_no_withheld, e.phone_instr_other,
				e.phone_country_id, e.disable_phone_prefix, e.phone_number_free AS phone_number,
				IF (ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE), 1, 0) AS is_login, e.allow_show_online,
				e.hit_count, e.slogan, e.incall_price, e.date_last_modified,e.last_login_date, ' . implode(',', $fields) . ', e.hh_is_active,
				e.is_on_tour AS is_tour, e.tour_date_from, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city,
				e.is_suspicious, e.is_online, ct.id AS city_id, '.$aboutField.', e.agency_name,
				(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' AS incall_price_converted,
				e.agency_slug, e.agency_id, ulrt.refresh_date AS refresh_date, e.gender AS gender
			FROM escorts_in_cities eic /*USE INDEX(by_googo3)*/
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = e.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
			LEFT JOIN currencies cur ON cur.id = e.incall_currency
			LEFT JOIN exchange_rates er ON er.iso = cur.title

			WHERE e.id IN ('. implode(',',$escort_ids) .')' . $where .'
			GROUP BY eic.escort_id
			/*ORDER BY */
			'.(!empty($order) ? ' ORDER BY ' . $order : '' ).'
		';

        if($_GET['QAQ3']) {
            die($sql);
        }

        $escorts = self::db()->fetchAll($sql);

        return $escorts;
    }

    public static function getFilteredForAdvancedSearch($filter, $ordering = 'random', $page = 1, $pageSize = self::HOMEPAGE_LISTING_SIZE, &$count = 0, $s_filter_params = array(), $is_grouped = false, $geo_data = array(), $list_type = 'simple', $p_top_category = 'escorts', $city_id = null, $country_id = null, $current_currency = null, $isUsa = false, &$nearbyCount = 0)
    {
        if ($page == 1) {
            $pageSize = self::HOMEPAGE_LISTING_SIZE - ($isUsa ? 0 : 1); // For the first page, fucking banner is there
        }


        $vip_premium_escorts = array();
        $cache = Zend_Registry::get('cache');

        // Cases when vip or premiums are not going to be visible
        // -----------------------------------------
        $isPageForVipAllowed = in_array($ordering, ['recommended_escorts', 'close-to-me']);
        // -----------------------------------------

        if ($isPageForVipAllowed && !in_array($ordering, array('vips', 'premiums')) && !in_array($p_top_category, array('newest', 'newest-independent', 'newest-agency-escorts', 'citytours', 'upcomingtours', 'all-joined'))) {

            $count_premiums = $count_vips = 0;
            $filter___p = $filter;
            $filter___p['original_sort'] = $ordering;

            $cache_tail = Cubix_Application::getId() . '_' . $p_top_category . '_' . $country_id . '_' . $city_id . '_' . $geo_data['country_iso'] . '_' . $page;
            $cache_filter = self::_generateCacheKey(array_merge((array)$filter, (array)$s_filter_params));
            $cache_tail .= preg_replace('#[^a-zA-Z0-9<>=_]#', '_', $cache_filter);

            $filter___p[' eic.is_premium = 1 AND eic.is_vip = 0'] = array();
            if (!$city_id && !$country_id) {
                // National Premium
                //http://jira.sceon.am/browse/EDIR-1766 is_home argument added  just for this task to detect home

                //if($is_home){
                if ($geo_data['country_iso']) { //  false added for this EDIR-37
                    // 4 - Main premium spot
                    $filter___p['( FIND_IN_SET(4, e.products) > 0 OR ( cr.iso = ? AND FIND_IN_SET(15, e.products ) > 0 )) '] = $geo_data['country_iso'];
                } else {
                    $filter___p['FIND_IN_SET(4, e.products) > 0'] = array();
                }
                //}
            } elseif (!$city_id && $country_id) {
                // 15 Country premium spot
                $filter___p['FIND_IN_SET(15, e.products) > 0'] = array();
            } else {
                // 5 City premium spot
                $filter___p['FIND_IN_SET(5, e.products) > 0'] = array();
            }


            $cache_key_premiums = sha1("premium_escorts_" . $cache_tail);
            if (!$vip_premium_escorts['premiums'] = $cache->load($cache_key_premiums)) {
                $vip_premium_escorts['premiums'] = self::getFilteredWhereQuery($filter___p, 'premiums', 0, self::HOMEPAGE_LISTING_SIZE, $count_premiums, false, false, $s_filter_params, false, $geo_data, $list_type, $city_id, $country_id);
                $cache->save($vip_premium_escorts['premiums'], $cache_key_premiums, array());
            } else {
                $count_premiums = count($vip_premium_escorts['premiums']);
            }

            $premiumEscortsIds = array();
            if (count($vip_premium_escorts['premiums']))
            {
                foreach ($vip_premium_escorts['premiums'] as $premium)
                {
                    $premiumEscortsIds[] = $premium->id;
                }
            }

            $filter___v = $filter;
            $filter___v['eic.is_vip = 1'] = array();
            $filter___v['original_sort'] = $ordering;

            if (!$city_id && !$country_id) {
                // 4 - Main premium spot
                //if($is_home){
                if ($geo_data['country_iso']) {  //  false added for this EDIR-37

                    $filter___v['( FIND_IN_SET(4, e.products) > 0 OR ( cr.iso = ? AND FIND_IN_SET(15, e.products ) > 0 )) '] = $geo_data['country_iso'];
                } else {
                    $filter___v['FIND_IN_SET(4, e.products) > 0'] = array();
                }
                //}
            } elseif (!$city_id && $country_id) {
                // 15 Country premium spot
                $filter___v['FIND_IN_SET(15, e.products) > 0'] = array();
            } else {
                // 5 City premium spot
                $filter___v['FIND_IN_SET(5, e.products) > 0'] = array();
            }

            $cache_key_vips = sha1("vip_escorts_" . $cache_tail);
            if (!$vip_premium_escorts['vips'] = $cache->load($cache_key_vips)) {
                $vip_premium_escorts['vips'] = self::getFilteredWhereQuery($filter___v, 'vips', 0, self::HOMEPAGE_LISTING_SIZE, $count_vips, false, false, $s_filter_params, false, $geo_data, $list_type, $city_id, $country_id);
                $cache->save($vip_premium_escorts['vips'], $cache_key_vips, array());
            } else {
                $count_vips = count($vip_premium_escorts['vips']);
            }

            $vipEscortsIds = array();
            if (count($vip_premium_escorts['vips']))
            {
                foreach ($vip_premium_escorts['vips'] as $vip)
                {
                    $vipEscortsIds[] = $vip->id;
                }
            }
        }

        $escort_ids = self::getEscortIdsOfQuery($filter, $ordering, $page, $pageSize, $count, 'advanced_search', false, $s_filter_params, $is_grouped, $geo_data, $list_type, $city_id, $country_id, $current_currency, 0, [], $nearbyCount);

        $escort_ids = array_diff($escort_ids,$premiumEscortsIds);
        $escort_ids = array_diff($escort_ids,$vipEscortsIds);

        if (count($escort_ids)) {
            $escorts = self::getListingDataByIds($escort_ids, $current_currency, $city_id, null, 'advanced_search');
        } else {
            $escorts = array();
        }

        if ($isPageForVipAllowed) {
            $count += $count_vips + $count_premiums;
        }

        $escorts_by_id = array();
        foreach ($escorts as $escort) {
            $escorts_by_id[$escort->id] = $escort;
        }

        $escorts = array();
        foreach ($escort_ids as $id) {
            array_push($escorts, $escorts_by_id[$id]);
        }

        $escorts = array_merge($vip_premium_escorts, $escorts);

        return [
            $escorts
        ];
    }

    /**
     * @param $filter
     * @param string $ordering
     * @param int $page
     * @param int $page_size
     * @param int $count
     * @param bool $sess_name
     * @param bool $only_vip
     * @param array $s_filter_params
     * @param bool $is_grouped
     * @param array $geo_data
     * @param string $list_type
     * @param string $p_top_category
     * @param null $city_id
     * @param null $country_id
     * @param null $current_currency
     * @param bool $isUsa
     * @return array
     */
    public static function getFiltered($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $only_vip = false, $s_filter_params = array(), $is_grouped = false, $geo_data = array(), $list_type = 'simple', $p_top_category = 'escorts', $city_id = null, $country_id = null, $current_currency = null, $isUsa = false, $excludedEscortIds = array(), $for_inactive_escort = false, &$nearbyCount = 0)
    {

        $count_vips = $count_premiums = 0;
        $gotd_vip_premium_escorts = array();
        $cache = Zend_Registry::get('cache');
        $availableFilters = [];

        // Cases when vip or premiums are not going to be visible
        // -----------------------------------------
        $isPageForVipAllowed = in_array($ordering, ['recommended_escorts', 'close-to-me']);
        // -----------------------------------------

        if ($isPageForVipAllowed && !in_array($ordering, array('gotd', 'vips', 'premiums')) && !in_array($p_top_category, array('newest', 'newest-independent', 'newest-agency-escorts', 'citytours', 'upcomingtours', 'all-joined'))) {

            $count_premiums = $count_vips = $count_gotds = 0;

            $filter___g = $filter;
            $filter___g['original_sort'] = $ordering;

            $cache_tail = Cubix_Application::getId() . '_' . $p_top_category . '_' . $country_id . '_' . $city_id . '_' . $geo_data['country_iso'] . '_' . $page;
            $cache_filter = self::_generateCacheKey(array_merge((array)$filter, (array)$s_filter_params));
            $cache_tail .= preg_replace('#[^a-zA-Z0-9<>=_]#', '_', $cache_filter);

            $filter___g['FIND_IN_SET(16, e.products) > 0'] = array();


            $cache_key_gotd = sha1("gotd_escorts_" . $cache_tail);
            if (!$gotd_vip_premium_escorts['gotd'] = $cache->load($cache_key_gotd)) {
                $gotd_vip_premium_escorts['gotd'] = self::getFilteredWhereQuery($filter___g, 'gotd', 0, self::HOMEPAGE_LISTING_SIZE, $count_gotds, false, false, $s_filter_params, false, $geo_data, $list_type, $city_id, $country_id);
                $cache->save($gotd_vip_premium_escorts['gotd'], $cache_key_gotd, array());
            } else {
                $count_gotds = count($gotd_vip_premium_escorts['gotd']);
            }


            $filter___p = $filter;
            $filter___p['original_sort'] = $ordering;

            $cache_tail = Cubix_Application::getId() . '_' . $p_top_category . '_' . $country_id . '_' . $city_id . '_' . $geo_data['country_iso'] . '_' . $page;
            $cache_filter = self::_generateCacheKey(array_merge((array)$filter, (array)$s_filter_params));
            $cache_tail .= preg_replace('#[^a-zA-Z0-9<>=_]#', '_', $cache_filter);

            $filter___p[' eic.is_premium = 1 AND eic.is_vip = 0 AND FIND_IN_SET(16, e.products) = 0'] = array();
            if (!$city_id && !$country_id) {
                // National Premium
                //http://jira.sceon.am/browse/EDIR-1766 is_home argument added  just for this task to detect home

                //if($is_home){
                if ($geo_data['country_iso']) { //  false added for this EDIR-37
                    // 4 - Main premium spot
                    $filter___p['( FIND_IN_SET(4, e.products) > 0 OR ( cr.iso = ? AND FIND_IN_SET(15, e.products ) > 0 )) '] = $geo_data['country_iso'];
                } else {
                    $filter___p['FIND_IN_SET(4, e.products) > 0'] = array();
                }
                //}
            } elseif (!$city_id && $country_id) {
                // 15 Country premium spot
                $filter___p['FIND_IN_SET(15, e.products) > 0'] = array();
            } else {
                // 5 City premium spot
                $filter___p['FIND_IN_SET(5, e.products) > 0'] = array();
            }


            $cache_key_premiums = sha1("premium_escorts_" . $cache_tail);
            if (!$gotd_vip_premium_escorts['premiums'] = $cache->load($cache_key_premiums)) {
                $gotd_vip_premium_escorts['premiums'] = self::getFilteredWhereQuery($filter___p, 'premiums', 0, self::HOMEPAGE_LISTING_SIZE, $count_premiums, false, false, $s_filter_params, false, $geo_data, $list_type, $city_id, $country_id);
                $cache->save($gotd_vip_premium_escorts['premiums'], $cache_key_premiums, array());
            } else {
                $count_premiums = count($gotd_vip_premium_escorts['premiums']);
            }

            $filter___v = $filter;
            $filter___v['eic.is_vip = 1 AND FIND_IN_SET(16, e.products) = 0'] = array();
            $filter___v['original_sort'] = $ordering;

            if (!$city_id && !$country_id) {
                // 4 - Main premium spot
                //if($is_home){
                if ($geo_data['country_iso']) {  //  false added for this EDIR-37

                    $filter___v['( FIND_IN_SET(4, e.products) > 0 OR ( cr.iso = ? AND FIND_IN_SET(15, e.products ) > 0 )) '] = $geo_data['country_iso'];
                } else {
                    $filter___v['FIND_IN_SET(4, e.products) > 0'] = array();
                }
                //}
            } elseif (!$city_id && $country_id) {
                // 15 Country premium spot
                $filter___v['FIND_IN_SET(15, e.products) > 0'] = array();
            } else {
                // 5 City premium spot
                $filter___v['FIND_IN_SET(5, e.products) > 0'] = array();
            }

            $cache_key_vips = sha1("vip_escorts_" . $cache_tail);
            if (!$gotd_vip_premium_escorts['vips'] = $cache->load($cache_key_vips)) {
                $gotd_vip_premium_escorts['vips'] = self::getFilteredWhereQuery($filter___v, 'vips', 0, self::HOMEPAGE_LISTING_SIZE, $count_vips, false, false, $s_filter_params, false, $geo_data, $list_type, $city_id, $country_id);
                $cache->save($gotd_vip_premium_escorts['vips'], $cache_key_vips, array());
            } else {
                $count_vips = count($gotd_vip_premium_escorts['vips']);
            }

        }

        $pageSize = self::HOMEPAGE_LISTING_SIZE;
        if ($isPageForVipAllowed == false || $page > 1) {
            unset($gotd_vip_premium_escorts['vips']);
            unset($gotd_vip_premium_escorts['premiums']);
		}

        $gotdVipPremiumCount = $count_vips + $count_premiums + $count_gotds;
        // Fill for first page empty spaces
        // ------------------------------
        if ($page == 1) {
            $pageSize = self::HOMEPAGE_LISTING_SIZE - ($isUsa ? 0 : 1); // For the first page, fucking banner is there

            $gotdVipPremiumCount = $count_vips + $count_premiums + $count_gotds;

            $escortsFillDiff = $gotdVipPremiumCount % 3;
            if ($gotdVipPremiumCount > 0 && $escortsFillDiff > 0) {
                $pageSize += (3 - $escortsFillDiff);
            }
            $_SESSION['first_page_size'] = $pageSize;
        }
        // ------------------------------
        if ( $for_inactive_escort )
            $pageSize = 6;

        if(!empty($excludedEscortIds)) $s_filter_params['eic.escort_id NOT IN'] = implode(',', $excludedEscortIds);

        $escort_ids = self::getEscortIdsOfQuery($filter, $ordering, $page, $pageSize, $count, $sess_name, $only_vip, $s_filter_params, $is_grouped, $geo_data, $list_type, $city_id, $country_id, $current_currency, 0, array(), $nearbyCount);

        if (count($escort_ids)) {
            $escorts = self::getListingDataByIds($escort_ids, $current_currency, $city_id);
        } else {
            $escorts = array();
        }

        // total count of all escorts

        if ($isPageForVipAllowed) {
            $count += $count_vips + $count_premiums;
        }

        $escorts_by_id = array();
        foreach ($escorts as $escort) {
            $escorts_by_id[$escort->id] = $escort;
        }

        $escorts = array();
        foreach ($escort_ids as $id) {
            array_push($escorts, $escorts_by_id[$id]);
        }

        $escorts = array_merge($gotd_vip_premium_escorts, $escorts);

        return [
            $escorts, $availableFilters, $gotdVipPremiumCount
        ];
    }

    /**
     * @param $filters
     * @param $excludedEscortIds
     * @param $limit
     * @param $p_top_category
     * @param $city
     * @param $country
     * @return array
     * @throws Exception
     */
    public static function getAroundEscorts($filters, $excludedEscortIds, $limit, $page, $p_top_category, $city, $country, $geoData = array(), $for_inactive_escort = false)
    {
        $cityWasGenerated = false;
        $escortsAround = [];
        $aroundLocationTitle = '';

        if (empty($city) && !empty($country)) {
            $cityWasGenerated = true;
            $cityRow = (new Model_Cities())->getByCountry($country, 1);
            if (isset($cityRow[0]))
                $city = $cityRow[0]->id;
        }

        if ( !empty($city) ) {
            if ( $for_inactive_escort )
            {
                $cityRow = (new Model_Cities())->getById($city);
                $escortsAround = Model_Escort_List::getEscortsFromClosestCities(array($city), $excludedEscortIds, $filters, $limit ,$page , [], false, $p_top_category, $city, $country);
                if ( count($escortsAround) < 6)
                {
                    $escortsAroundCountry = Model_Escort_List::getEscortsFromSameCountry(array(), $excludedEscortIds, $filters, $limit, $page, [], false, $p_top_category, $city, $country, $geoData, $for_inactive_escort);
                    $aroundLocationTitle = $cityRow->country_title;
                    $escortsAround = array_merge($escortsAround,$escortsAroundCountry);
                }else{
                    $aroundLocationTitle = $cityRow->title ;
                }
            }else{
                $escortsAround = Model_Escort_List::getEscortsFromSameCountry(array(), $excludedEscortIds, $filters, $limit, $page, [], false, $p_top_category, $city, $country, $geoData);
            }
			
            if( empty($escortsAround)){
                $nearCitiesIds = Model_Cities::getNearbyCitiesOf([$city]);
                $escortsAround = Model_Escort_List::getEscortsFromClosestCities($nearCitiesIds, $excludedEscortIds, $filters, $limit, $page, [], false, $p_top_category, $city, $country);

                // This is disabled by Eduard Hovhannisyan
                // Check task: EDIR-280
                if(false && count($escortsAround) < $limit) {
                    $citiesAroundNearCity = Model_Cities::getNearbyCitiesOf(array_slice($nearCitiesIds, 0, 6));
                    $escortsAround_2 = Model_Escort_List::getEscortsFromClosestCities($citiesAroundNearCity, $excludedEscortIds, $filters, $limit ,$page , [], false, $p_top_category, $city, $country);
                    $escortsAround = array_merge($escortsAround, $escortsAround_2);
                }

                $cityRow = (new Model_Cities())->getById($city);

                if ($cityWasGenerated == false) {
                    $aroundLocationTitle .= $cityRow->title . ', ';
                }

                $aroundLocationTitle .= $cityRow->country_title;
                 return [
                    $escortsAround,
                    $aroundLocationTitle
                ];
            }else{
                 return [
                    $escortsAround,
                    $aroundLocationTitle
                ];
            }
           
        }
       
    }

    /**
     * EDIR-2560
     * Returns specified amount of escorts + their data, that are in near cities of given one.
     * @return array
     */
    public static function getEscortsFromSameCountry($cityIds, $excludedEscortIds, $filter, $pageSize, $page, $s_filter_params, $current_currency,$p_top_category, $city, $country, $geoData, $for_inactive_escort = false)
    {

        $caunt = 0;
        unset($filter['eic.city_id = ?']);
        $filter['show_all_agency_escorts'] = true;
        $isUsa = true;// to avoid advertisment one banner

        list($escorts, $_availableFilters, $vipPremiumCount) = Model_Escort_List::getFiltered($filter, 'close-to-me', $page, 50, $caunt, 'regular_list', false, [], null, $geoData, 'gallery', 'escorts', null, $country, $current_currency, $isUsa, $excludedEscortIds, $for_inactive_escort);

        $vips = $escorts['vips'];
        $premiums = $escorts['premiums'];

        if(empty($escorts['vips'])) unset($escorts['vips']);
        if(empty($escorts['premiums'])) unset($escorts['premiums']);

        unset($escorts['premiums']);
        unset($escorts['vips']);
		unset($escorts['gotd']);
		
        foreach($premiums as $premium){
            array_unshift($escorts, $premium);
        }

        return $escorts;
    }

     /**
     * EDIR-514
     * Returns specified amount of escorts + their data
     * @return array
     */
    public static function getEscortsFromClosestCities($cityIds, $excludedEscortIds, $filter, $pageSize, $page, $s_filter_params, $current_currency)
    {
        $s_filter_params['eic.city_id IN'] = implode(',', $cityIds);
        if(!empty($s_filter_params)) $s_filter_params['eic.escort_id NOT IN'] = implode(',', $excludedEscortIds);

        $escort_ids = self::getEscortIdsOfQuery($filter, 'filled-with-around-cities' , $page , $pageSize, $count, false, false, $s_filter_params , null, null, null, null, $current_currency);

        if(count($escort_ids)){
            $escorts = self::getListingDataByIds($escort_ids, $current_currency, null);
        }else{
            $escorts = array();
        }

        $escorts_by_id = array();
        foreach ($escorts as $escort) {
            $escorts_by_id[$escort->id] = $escort;
        }

        $escorts = array();
        foreach ($escort_ids as $id) {
            array_push($escorts, $escorts_by_id[$id]);
        }

        return $escorts;
    }

   	public static function getAgencyEscorts($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0)
	{
		$lng = Cubix_I18n::getLang();

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;


		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$group_by = " GROUP BY eic.escort_id ";

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar, e.last_hand_verification_date,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated, ct.slug AS city_slug, ct.id AS city_id, ct.title_' . $lng . ' AS city, e.rates,
				e.incall_type, e.outcall_type, e.is_new, cr.id AS country_id, cr.slug AS country_slug, cr.title_' . $lng . ' AS country, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
				e.travel_place, e.email, e.website, e.phone_instr, e.phone_instr_no_withheld, e.phone_instr_other,
				e.phone_country_id, e.disable_phone_prefix, e.phone_number_free AS phone_number,
				IF (ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE), 1, 0) AS is_login, e.allow_show_online,
				e.hit_count, e.slogan, e.incall_price, e.date_last_modified, eic.is_premium, e.is_vip, e.hh_is_active,
				e.is_on_tour AS is_tour, e.tour_date_from, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city,
				e.is_suspicious, e.is_online, ct.id AS city_id, e.about_' . $lng . ' AS about, e.agency_name,
				e.agency_slug, e.agency_id
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id			
			LEFT JOIN escort_services es ON es.escort_id = e.id
			LEFT JOIN escort_keywords ek ON ek.escort_id = e.id
			LEFT JOIN currencies cur ON cur.id = e.incall_currency
			LEFT JOIN exchange_rates er ON er.iso = cur.title

			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			' . $group_by . '
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		';

		if($_GET['QAQ']) {
		    die($sql);
        }

		$escorts = self::db()->fetchAll($sql);

		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		return $escorts;
	}

    public static function getTours($is_upcoming, $filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $s_filter_params = array(), $is_grouped = false, $geo_data = array(), $list_type = 'simple', $current_currency = null)
    {
        $lng = Cubix_I18n::getLang();
        $sql_join = ' ';
        $fields = '';
        $having = [];
        /*EDIR-2237
        if ( isset($s_filter_params['gender']) && count($s_filter_params['gender']) ) {
            $filter[] = 'eic.gender IN ( ' . implode(',', $s_filter_params['gender']) . ' )';
        }*/

        if ( isset($s_filter_params['service']) && count($s_filter_params['service']) ) {
            // EDIR-2492
            foreach($s_filter_params['service'] as $serviceId) {
                $having[] = ' FIND_IN_SET ( ' . $serviceId . ', service_id )';
            }
            $fields .= ', GROUP_CONCAT(es.service_id) AS service_id ';
            #$filter[] = 'es.service_id IN ( ' . implode(',', $s_filter_params['service']) . ' )';
        }

        if ( isset($filter['with-video']) && $filter['with-video'] ) {
            $filter['e.has_video = 1'] = array();
            //$join .= " INNER JOIN video v ON v.escort_id = e.id ";
        }
        unset($filter['with-video']);

        if ( isset($filter['eic.gender = 1']) ) unset($filter['eic.gender = 1']);

        if ( ! $is_upcoming ) {
            $filter[] = 'eic.is_tour = 1';
        }
        else {
            $filter[] = 'eic.is_upcoming = 1';
        }

        unset($filter['online-now']);

        if ( isset($filter['show_all_agency_escorts']) ) {
            unset($filter['show_all_agency_escorts']);
        }

        if ( isset($s_filter_params['with-natural-photo']) && $s_filter_params['with-natural-photo'] ) {
            $sql_join .= " INNER JOIN natural_pics n_pics ON n_pics.escort_id = e.id ";
        }

        $where = self::getWhereClause($filter, true);
        $order = self::_mapSorting($ordering, $is_grouped);

        $page = (int) $page;
        if ( $page < 1 ) $page = 1;
        $skip = ($page_size * ($page - 1));
        $take = $page_size;
        $limit = $skip . ', ' . $take;

        $fields .= ' , NULL AS distance ';
        if ( $ordering == 'close-to-me' && count($geo_data) ) {
            if (array_key_exists("latitude", $geo_data) && strlen($geo_data['latitude']) > 0 && array_key_exists("longitude", $geo_data) && strlen($geo_data['longitude']) > 0) {
                $fields = '
					 , ((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						))
						)
					)) AS distance
				';
            }
        }

        $currency_rate = Model_Currencies::getSelectedCurrencyRate($current_currency);

        $order = 'eic.is_vip DESC, eic.is_premium DESC,' . $order;

        is_null($where) ? $where = ' 1 ' . Cubix_Countries::blacklistCountryWhereCase() : $where .= Cubix_Countries::blacklistCountryWhereCase();

        $sql = '
			SELECT SQL_CALC_FOUND_ROWS
                case 
                    WHEN ulrt.refresh_date > e.date_last_modified THEN ulrt.refresh_date
                    ELSE e.date_last_modified
                end as last_modified_date,
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar, e.last_hand_verification_date,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated, ct.slug AS city_slug, ct.id AS city_id, ct.title_' . $lng . ' AS city, e.rates,
				e.incall_type, e.outcall_type, e.is_new, cr.id AS country_id, cr.slug AS country_slug, cr.title_' . $lng . ' AS country, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_args, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
				e.travel_place, e.email, e.website, e.phone_instr, e.phone_instr_no_withheld, e.phone_instr_other,
				e.phone_country_id, e.disable_phone_prefix, e.phone_number_free AS phone_number,
				e.hit_count, e.slogan, e.incall_price, e.date_last_modified, eic.is_premium, e.is_vip, e.is_inactive, e.hh_is_active,
				e.is_suspicious, e.is_online, ct.id AS city_id, e.about_' . $lng . ' AS about, e.agency_id, e.agency_name, e.agency_slug,
				(e.incall_price / er.rate) * ' . round($currency_rate->rate) . ' AS incall_price_converted,
				eic.is_tour ' .
            (! $is_upcoming ? ', e.tour_date_from, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city' : // if
                ($is_upcoming ? ', ut.tour_date_from, ut.tour_date_to, ctu.title_' . $lng . ' AS tour_city' : '')) // else
            . $fields . '
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			'.$sql_join.'
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id

			LEFT JOIN currencies cur ON cur.id = e.incall_currency
			LEFT JOIN exchange_rates er ON er.iso = cur.title
			
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . ($is_upcoming ? 'INNER JOIN upcoming_tours ut ON ut.id = eic.escort_id INNER JOIN cities ctu ON ctu.id = ut.tour_city_id' : 'LEFT JOIN cities ctt ON ctt.id = e.tour_city_id') . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			' . (! empty($having) ? ' HAVING ' . implode(' AND ', $having) : '') . '
			ORDER BY eic.is_inactive, last_modified_date DESC,' . $order . '
			LIMIT ' . $limit . '
		';

        $escorts = self::db()->fetchAll($sql);
        $count = self::db()->fetchOne('SELECT FOUND_ROWS()');

        if ( count($escorts) && $is_grouped && $list_type == 'simple' ) {

            $grouped_escorts = array();
            foreach($escorts as $k => $escort) {

                $group_field = $escort->city;
                if ( $ordering == 'by-country' && ! isset($filter['ct.id = ?']) && ! isset($filter['cr.id = ?']) ) {
                    $group_field = $escort->country;
                }

                $no_group_sorting = array('newest', 'random', 'alpha', 'most-viewed');
                if ( in_array($ordering, $no_group_sorting) ) {
                    $group_field = '';
                } else {
                    $group_field = $group_field . ' ' . __('escorts');
                }

                $grouped_escorts['city_groups'][$group_field][] = $escort;

                if ( $ordering != 'close-to-me' && $ordering != 'newest' ) {
                    ksort($grouped_escorts['city_groups']);
                }

            }

            $escorts = $grouped_escorts;
        }
        return $escorts;
    }

   	private static function _mapSortingAgencies($param)
	{
		$map = array(
			'alpha' => 'cd.is_premium DESC, cd.club_name ASC',
			'random' => 'cd.is_premium DESC, RAND()',
			'last_modified' => 'cd.is_premium DESC, cd.last_modified DESC',
			'premium' => 'cd.is_premium DESC',
			'by-city' => 'ct.title_en ASC',
			'by-country' => 'cr.title_en ASC',
			'most-viewed' => 'cd.hit_count DESC',
			'close-to-me' => 'distance ASC',
		);

		$order = 'cd.club_name ASC';
		if ( isset($map[$param]) ) {
			$order = $map[$param];
		}
		elseif ( false !== strpos($param, 'FIELD') ) {
			$order = $param;
		}

		return $order;
	}

	private static function _mapSorting($param, $is_grouped = false, $forPage = null)
	{
		$map = array(
			'close-to-me' => 'distance ASC, is_featured DESC ',
			'last-contact-verification' => 'e.last_hand_verification_date DESC, is_featured DESC',
			'last-connection' => 'ulrt.refresh_date DESC, is_featured DESC',


			'by-city' => 'ct.title_en ASC, eic.ordering DESC',
			'by-country' => 'cr.title_en ASC, eic.ordering DESC',
			'random' => 'is_featured DESC, eic.ordering DESC',
			'alpha' => 'e.showname ASC, is_featured DESC',
			'most-viewed' => 'e.hit_count DESC, is_featured DESC',
			'newest' => 'COALESCE(e.date_activated, e.date_registered) DESC, e.id DESC, is_featured DESC',
			'recommended_escorts' => 'is_featured DESC, last_modified_date DESC,CASE WHEN e.city_id = eic.city_id THEN 0 ELSE 1 END, e.city_id',

			'premiums' => 'e.is_vip DESC, eic.is_premium DESC, eic.ordering DESC',
			'vips' => 'e.is_vip DESC, is_vip_plus DESC, is_vip2 DESC, eic.ordering DESC',
			'price-asc' => 'ISNULL( incall_price_converted ) ASC, incall_price_converted ASC, is_featured DESC',
			'price-desc' => 'ISNULL( incall_price_converted ) ASC, incall_price_converted DESC, is_featured DESC',

			'last_modified' => 'e.date_last_modified DESC',
			'age' => 'e.age ASC',
		);

		$order = 'e.ordering DESC';
		if ( isset($map[$param]) ) {
			$order = $map[$param];
		}
		elseif ( false !== strpos($param, 'FIELD') ) {
			$order = $param;
		}

        if($forPage == 'advanced_search' && $param != 'close-to-me') {
            $order = 'e.is_vip DESC, e.is_premium DESC, ' . $order;
        }elseif ($forPage == 'advanced_search' && $param == 'close-to-me')
        {
            $order = 'CASE WHEN distance IS NULL THEN 1 else 0 END, distance ASC, is_featured DESC, e.is_vip DESC, e.is_premium DESC';
        }
        
		return $order;
	}

	private static function _generateCacheKey($filters)
	{
		$cache_key = '';
		foreach($filters as $k => $value ) {
            if ( is_array($value) ) {
				$cache_key .= $k;
                foreach( $value as $val ) {
                    $cache_key .= '_' . $val . '_';
                }
            } else {
                $cache_key .= $k. '_' . $k . '_' . $value;
            }
        }
		return $cache_key;
	}

	public static function getPrevNext($showname)
	{
		return array(null, null);
		// Retrieve criterias from session
		$sid = 'v2_sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$page = $sess->page;
		$page_size = $sess->page_size;
		$pages_count = $sess->pages_count;

		list($filter, $ordering) = $sess->criteria;
		foreach ( $sess->escorts as $i => $name ) {
			if ( $name == $showname ) {
				$prev_i = $i - 1;
				$next_i = $i + 1;

				break;
			}
		}

		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);

		if ( ! isset($prev_i) && ! isset($next_i) ) {
			return array(null, null);
		}

		// This is first escort, we need to fetch data from prev page
		if ( isset($prev_i) && (! isset($sess->escorts[$prev_i]) || $prev_i < 0) && $page > 1 ) {
			$sess->escorts = array_slice($sess->escorts, 0, $page_size * 2);

			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page - 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($escorts, $sess->escorts);
			
			$prev_i = $page_size - 1;
		}

		
		echo "Escorts after 'first' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);
		
		// If this last escort on page, we need to fetch data from next page
		if ( isset($next_i) &&  (! isset($sess->escorts[$next_i]) || $next_i > count($sess->escorts) - 1 ) && $page < $pages_count ) {
			if ( count($sess->escorts) >= $page_size * 3 ) {
				$sess->escorts = array_slice($sess->escorts, $page_size, $page_size * 2);
				$next_i -= $page_size;
			}
			
			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page + 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($sess->escorts, $escorts);
		}

		echo "Escorts after 'last' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);
		
		return array(isset($prev_i) ? $sess->escorts[$prev_i] : null, isset($next_i) ? $sess->escorts[$next_i] : null);
	}
	
	public static function getVIPEscorts($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $only_vip = false, $is_tour = false)
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}
		
		if ( $only_vip ) {
			$filter[] = 'FIND_IN_SET(17, e.products) > 0';

			if ( isset($filter['ct.slug = ?']) && ! $is_tour ) {
				$country_id = Model_Statistics::getCountryByCitySlug($filter['ct.slug = ?'][0]);
				$filter[] = 'e.country_id = ' . $country_id;
				unset($filter['ct.slug = ?']);
			}
		}
		
		// Only escorts with base city (or city tour) will be shown in this list
		if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
			$filter[] = 'eic.is_base = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 0';
		}

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;
		
		$j = "";
		$f = "";
		if ( isset($filter['f.user_id = ?']) ) {
			$j = " INNER JOIN favorites f ON f.escort_id = e.id ";
			$f = " , f.user_id AS fav_user_id ";
		}

		$order = str_replace('eic.is_premium', 'IF(eic.is_tour = 1, 0, eic.is_premium)', $order);
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id, e.is_pornstar,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,

				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,
				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				e.hit_count, e.slogan, e.incall_price,
				e.date_last_modified,
				/*IF(eic.is_tour = 1 AND eic.is_premium = 1, 0, eic.is_premium) AS is_premium,*/ 
				IF(eic.is_tour = 1, 0, eic.is_premium) AS is_premium,
				IF(FIND_IN_SET(17, e.products) > 0, 1, 0) AS is_vip,
				e.hh_is_active, e.is_suspicious, e.is_online ' . $f . ', ct.id AS city_id, e.about_' . $lng . ' AS about
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = e.country_id
			LEFT JOIN regions r ON r.id = eic.region_id
			' . $j . '
			LEFT JOIN escort_cityzones ecz ON ecz.escort_id = eic.escort_id
			LEFT JOIN cityzones cz ON cz.id = ecz.city_zone_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 2 AND se.slug = "escort")
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		'; 

		$escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		
		
		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		if ( $sess_name ) {
			// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->id;
			}
			
			$sess->{$sess_name . '_criterias'} = array(
				'filter'	=> $filter,
				'sort'	=> $ordering,
				'page'		=> $page
			);
			// </editor-fold>
		}

		return $escorts;
	}

	public static function getForJson()
	{
		$lng = 'en';

		$sql = '
			SELECT
				e.id,
				e.user_id,
				e.showname, 
				e.age,
				e.gender,
				e.sex_orientation,
				e.height,
				e.weight,

				UNIX_TIMESTAMP(e.date_registered) AS registered_at,
				e.last_hand_verification_date AS last_hand_verification_at,
				e.date_last_modified AS last_modified_at,
				UNIX_TIMESTAMP(e.date_activated) AS activated_at,

				cr.id AS country_id, 
				cr.slug AS country_slug, 
				cr.title_' . $lng . ' AS country_title,

				ct.id AS city_id,
				ct.slug AS city_slug,				
				ct.title_' . $lng . ' AS city_title,
				
				e.is_pornstar,
				e.incall_type, 
				e.outcall_type, 
				e.is_new,
				e.verified_status,
				e.allow_show_online,
				e.is_suspicious, 
				e.is_online,
				eic.is_premium, 
				e.is_vip, 
				e.hh_is_active,
				e.is_on_tour AS is_tour,

				e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
							
				e.incall_currency, 
				e.incall_price, 
				e.outcall_currency, 
				e.outcall_price, 

				e.comment_count, 
				e.review_count,

				e.email, 
				e.website, 
				e.phone_instr, 
				e.phone_instr_no_withheld, 
				e.phone_instr_other,
				e.phone_country_id, 
				e.disable_phone_prefix, 
				e.phone_number_free AS phone_number,
				
				e.hit_count, 
				e.slogan,
				
				e.tour_date_from, 
				e.tour_date_to, 
				ctt.id AS tour_city_id,
				ctt.slug AS tour_city_slug,
				ctt.title_' . $lng . ' AS tour_city_title,
				
				e.about_' . $lng . ' AS about,

				e.agency_id,
				e.agency_slug,
				e.agency_name,

				e.sex_availability AS services_for,
				e.languages			
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN countries cr ON cr.id = ct.country_id			
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id			
			LEFT JOIN currencies cur ON cur.id = e.incall_currency
			LEFT JOIN exchange_rates er ON er.iso = cur.title
			WHERE 1
			AND eic.is_base = 1
			GROUP BY eic.escort_id
		';

		$escorts = self::db()->fetchAll($sql);

		$date_format = 'D M d Y H:i:s O';

		foreach ($escorts as $i => $escort) {
			$escort = new Model_EscortV2Item($escort);

			$escorts[$i]->country = array('id' => $escorts[$i]->country_id, 'slug' => $escorts[$i]->country_slug, 'title' => $escorts[$i]->country_title);
			unset($escorts[$i]->country_id);
			unset($escorts[$i]->country_slug);
			unset($escorts[$i]->country_title);

			$escorts[$i]->city = array('id' => $escorts[$i]->city_id, 'slug' => $escorts[$i]->city_slug, 'title' => $escorts[$i]->city_title);
			unset($escorts[$i]->city_id);
			unset($escorts[$i]->city_slug);
			unset($escorts[$i]->city_title);

			$escorts[$i]->agency = array('id' => $escorts[$i]->agency_id, 'slug' => $escorts[$i]->agency_slug, 'title' => $escorts[$i]->agency_name);
			unset($escorts[$i]->agency_id);
			unset($escorts[$i]->agency_slug);
			unset($escorts[$i]->agency_name);

			if ( $escorts[$i]->is_tour ) {
				$escorts[$i]->tour = array(
					'date' => array(
						'start' => date($date_format, strtotime($escorts[$i]->tour_date_from)), 
						'end' => date($date_format, strtotime($escorts[$i]->tour_date_to))
					), 
					'city' => array(
						'id' => $escorts[$i]->tour_city_id, 
						'slug' => $escorts[$i]->tour_city_slug, 
						'title' => $escorts[$i]->tour_city_title) 
				);
			} else {
				$escorts[$i]->tour = array(
					'date' => array(
						'start' => null, 
						'end' => null
					), 
					'city' => array(
						'id' => null, 
						'slug' => null,
						'title' => null)
				);
			}
			unset($escorts[$i]->tour_date_from);
			unset($escorts[$i]->tour_date_to);
			unset($escorts[$i]->tour_city_id);
			unset($escorts[$i]->tour_city_slug);
			unset($escorts[$i]->tour_city_title);

			$escorts[$i]->image_url = $escort->getMainPhoto()->getUrl('thumb_ed_v3');
			unset($escorts[$i]->photo_hash);
			unset($escorts[$i]->photo_ext);
			unset($escorts[$i]->photo_status);
			unset($escorts[$i]->application_id);
			
			$escorts[$i]->about = mb_convert_encoding(substr( strip_tags($escorts[$i]->about), 0, 450 ), "UTF-8" );

			$keywords = self::db()->fetchAll('SELECT keyword_id AS id FROM escort_keywords WHERE escort_id = ?', $escorts[$i]->id);
			$_keywords = array();
			foreach ($keywords as $key => $keyword) {
				$_keywords[] = $keyword->id;
			}
			$escorts[$i]->keywords = $_keywords;

			$services = self::db()->fetchAll('SELECT service_id AS id FROM escort_services WHERE escort_id = ?', $escorts[$i]->id);
			$_services = array();
			foreach ($services as $key => $service) {
				$_services[] = $service->id;
			}
			$escorts[$i]->services = $_services;

			$escorts[$i]->languages = explode(',', $escorts[$i]->languages);
			$escorts[$i]->services_for = explode(',', $escorts[$i]->services_for);

			$escorts[$i]->registered_at = date($date_format, $escorts[$i]->registered_at);
			if ( $escorts[$i]->last_hand_verification_at ) {
				$escorts[$i]->last_hand_verification_at = date($date_format, strtotime($escorts[$i]->last_hand_verification_at));
			} else {
				$escorts[$i]->last_hand_verification_at = null;
			}
			$escorts[$i]->last_modified_at = date($date_format, strtotime($escorts[$i]->last_modified_at));
			if ( $escorts[$i]->activated_at ) {
				$escorts[$i]->activated_at = date($date_format, $escorts[$i]->activated_at);
			} else {
				$escorts[$i]->activated_at = null;
			}
		}

		return $escorts;
	}
	
	public static function convertPushState($pushstate_filters)
	{
		$request = Zend_Controller_Front::getInstance()->getRequest();
		$push_state_array = explode(';',$pushstate_filters);
		array_pop($push_state_array);

		foreach($push_state_array as $filter){
			list($key, $value) = explode('=',$filter);

			$values = explode(',',$value);
			if(count($values) > 1){
				$value_array = array();
				foreach($values as $val){
					$value_array[] = $val;
				}
				$request->setParam($key, $value_array);
			}
			else{
				$request->setParam($key, $value);
			}
		}
	}

	public static function checkEscortCityIsPremium($escort_id, $base_city_id){


         $sql = 'SELECT COUNT(*) as count_row FROM premium_cities WHERE escort_id = '.$escort_id.' AND city_id = '.$base_city_id;

        $result = self::db()->fetchOne($sql);

        return $result;
    }
}
