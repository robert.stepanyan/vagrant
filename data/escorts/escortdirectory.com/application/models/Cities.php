<?php

class Model_Cities extends Cubix_Model
{
    protected $_table = 'cities';

    public function getByCountry($id, $limit = null)
    {
        if(!is_int($id)) return [];

        $sql = '
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM cities c
			WHERE country_id = ' . $id . '
			ORDER BY title
		';

        if (!empty($limit)) {
            $sql .= ' LIMIT ' . intval($limit);
        }

        return parent::_fetchAll($sql);
    }

    public static function getCountryRegionByCityID($id)
    {
        $lng = Cubix_I18n::getLang();

        $sql = 'SELECT
            ct.id as city_id,
            c.id as country_id,
            r.id as region_id,
            
            ct.slug as city_slug,
            c.slug as country_slug,
            ct.title_' . $lng . ' as city_title,
            c.title_' . $lng . ' as country_title,
            r.title_' . $lng . ' as region_title
        FROM
            cities ct
        LEFT JOIN countries c on c.id = ct.country_id
        LEFT JOIN regions r on ct.region_id = r.id
        WHERE
            ct.id = ?';

        $result = self::db()->fetchAll($sql, $id);
        return isset($result[0]) ? $result[0] : null;
    }

    public function getCountryIdById($id)
    {
        $sql = '
			SELECT country_id
			FROM cities c
			WHERE id = ' . $id . '
		';

        return parent::_fetchRow($sql);
    }

    public function getIdBySlug($slug)
    {
        $sql = '
			SELECT id
			FROM cities
			WHERE slug = "' . $slug . '"
		';

        return parent::_fetchRow($sql);
    }

    public static function getCoordinatesById($id)
    {
        $sql = '
			SELECT id, latitude, longitude, country_id
			FROM cities
			WHERE id = ?
		';

        return self::db()->fetchRow($sql, $id);
    }

    public function getByCountries($ids)
    {
        $ids_str = implode(',', $ids);
        $sql = '
			SELECT c.id, c.country_id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM cities c
			WHERE country_id IN (' . $ids_str . ')
			ORDER BY title
		';

        return parent::_fetchAll($sql);
    }

    public function getByIds($ids, $simple = true)
    {
        if(is_array($ids)) {
            $ids = implode(',', $ids);
        }

        if($simple) {
            return parent::_fetchAll('SELECT id, ' . Cubix_I18n::getTblField('title') . ' as title FROM cities WHERE id IN (' . $ids . ') ORDER BY title ASC');
        }else {
            $sql = 'SELECT 
                    c.id as city_id, c.title_en as city_title_en, c.slug as city_slug,
                    ct.id as country_id, ct.title_en as country_title_en, ct.slug as country_slug,
                    r.id as region_id, r.title_en as region_title_en, r.slug as region_slug
                FROM cities c
                INNER JOIN countries ct ON ct.id = c.country_id
                LEFT JOIN regions r ON r.id = c.region_id
                WHERE c.id IN (' . $ids . ')';

            return parent::_fetchAll($sql);
        }
    }

    public function getAll()
    {
        return parent::_fetchAll('SELECT id, ' . Cubix_I18n::getTblField('title') . ' as title, country_id, region_id FROM cities ORDER BY title ASC');
    }

    public function getCitiesContainEscorts()
    {
        $lng = Cubix_I18n::getLang();

        return parent::_fetchAll(
            'SELECT
                id,
                c.country_id,
                c.title_'.$lng.' as title,
                c.region_id,
                count( eic.escort_id ) as escortsCount
            FROM
                cities c
                LEFT JOIN escorts_in_cities eic ON eic.city_id = c.id
                group by eic.city_id'
        );
    }


    public function getByGeoIp()
    {
        $geoData = Cubix_Geoip::getClientLocation();

        if (!is_null($geoData)) {
            // Hard code Exception for Hong Kong
            if ($geoData['country'] == "Hong Kong") {
                $geoData['city'] = "Hong Kong";
            }

            $log = false;

            if (strlen($geoData['country'])) {

                $country = parent::getAdapter()->fetchRow('SELECT id, slug, title_en AS title FROM countries WHERE title_geoip = ?', $geoData['country']);

                if ($country) {

                    $city = null;
                    if (strlen($geoData['city'])) {
                        $sql = 'SELECT c.title_en, c.slug, c.id, r.id as region_id, r.title_en as region_title, r.slug as region_slug 
                                FROM cities c
                                LEFT JOIN regions r 
                                ON r.id = c.region_id WHERE c.title_geoip = ?';
                        $city = parent::getAdapter()->fetchRow($sql, $geoData['city']);
                    }

                    return array(
                        'city_id' => (isset($city->id) ? $city->id : null),
                        'city_title' => (isset($city->title_en) ? $city->title_en : null),
                        'city_slug' => (isset($city->slug) ? $city->slug : null),
                        'country_id' => $country->id,
                        'country_slug' => $country->slug,
                        'country_title' => $country->title,
                        'region_id' => $city->region_id,
                        'region_title' => $city->region_title,
                        'region_slug' => $city->region_slug,
                    );
                } else {
                    $log = true;
                }

            } else {
                $log = false;
            }

            if ($log) {
                $geoData['city'] = utf8_encode($geoData['city']);

                $c = parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM geoip_missing_cities WHERE city = ?', $geoData['city']);

                if ($c == 0)
                    parent::getAdapter()->insert('geoip_missing_cities', array(
                        'ip' => $geoData['ip'],
                        'country' => $geoData['country'],
                        'country_iso' => $geoData['country_iso'],
                        'region' => $geoData['region'],
                        'city' => $geoData['city'],
                        'latitude' => $geoData['latitude'],
                        'longitude' => $geoData['longitude']
                    ));

                return null;
            }
        } else
            return null;
    }

    public function getById($id)
    {
        return $this->db()->query('SELECT c.*, ' . Cubix_I18n::getTblField('c.title') . ' as title, ' . Cubix_I18n::getTblField('co.title') . ' as country_title FROM cities c INNER JOIN countries co ON co.id = c.country_id WHERE c.id = ?', $id)->fetch();
    }

    public static function getNearbyCityIds($geo_data)
    {
        $redis = Zend_Registry::get('redis');
        $key = $geo_data['country_iso'] . '_' . $geo_data['city'];
        $ids = $redis->hGet('nearby_cities', $key);

        if ($ids) {
            return $ids;
        } else {

            self::db()->insert('geoip_missing_nearby_cities', array(
                'ip' => $geo_data['ip'],
                'country' => $geo_data['country'],
                'country_iso' => $geo_data['country_iso'],
                'region' => $geo_data['region'],
                'city' => $geo_data['city'],
                'latitude' => $geo_data['latitude'],
                'longitude' => $geo_data['longitude']
            ));

            $sql = '
				SELECT nearby_cities , ((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - latitude))/2), 2) +
							COS(RADIANS(latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - latitude))/2), 2) +
							COS(RADIANS(latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - longitude))/2), 2)
						))
						)
					)) AS distance
				FROM cities
				HAVING distance is not null
				ORDER BY distance
				LIMIT 1
			';

            return self::db()->fetchOne($sql);
        }


    }

    /**
     * @param array $ids
     * @return array
     */
    public static function getNearbyCitiesOf(Array $ids)
    {
        if (empty($ids)) {
            throw new \Exception('City ids cannot be null');
        }

        $ids = implode(',', $ids);
        $sql = "SELECT nearby_cities FROM cities WHERE id IN ($ids) ";
        $rows = self::db()->fetchAll($sql);

        $result = [];
        foreach ($rows as $row) {
            if (!empty($row->nearby_cities))
                $result = array_merge($result, explode(',', $row->nearby_cities));
        }

        $result = array_filter($result, function ($value) {
            return !empty($value) && $value !== '';
        });

        return $result;
    }

    /**
     * If the word is like a city, country, region
     * @param $word
     * @return mixed
     */
    public function checkExistence($word)
    {
        $search_string = $this->db()->quote("$word%");

        $sql = "SELECT
                    null as city_id,
                    null as city_slug,
                    null as region_id,
                    null as region_slug,
                    cn.id AS country_id,
                    cn.slug AS country_slug
                FROM countries AS cn 
                WHERE cn.title_en LIKE $search_string 
                    UNION
                SELECT
                    c.id AS city_id,
                    c.slug AS city_slug,
                    null as region_id,
                    null as region_slug,
                    null AS country_id,
                    null AS country_slug
                FROM cities AS c 
                WHERE c.title_en LIKE $search_string
                    UNION
                SELECT
                    null as city_id,
                    null as city_slug,
                    r.id AS region_id,
                    r.slug AS region_slug,
                    null AS country_id,
                    null AS country_slug
                FROM regions r
                WHERE r.title_en LIKE $search_string LIMIT 5";

        return $this->db()->fetchAll($sql);
    }

    /**
     * Checks inside array of cities, which matches most keywords
     * @param $cities
     * @param $keywords
     * @return stdClass
     */
    public function getClosestMatchedFrom(Array $cities, Array $keywords) : stdClass
    {
        $closest = ['row' => null, 'matches' => 0];

        $ids = [];
        foreach($cities as $city) {
            if(!isset($city->city_id)) continue;
            $ids[] = $city->city_id;
        }

        $rows = $this->getByIds($ids, false);

        foreach($rows as & $row) {
            $row->matches = 0;
            foreach($keywords as $word) {
                if(isset($row->city_title_en) && stripos($row->city_title_en, $word) !== false) $row->matches++;
                if(isset($row->city_slug) && stripos($row->city_slug, $word) !== false) $row->matches++;

                if(isset($row->country_title_en) && stripos($row->country_title_en, $word) !== false) $row->matches++;
                if(isset($row->country_slug) && stripos($row->country_slug, $word) !== false) $row->matches++;

                if(isset($row->region_title_en) && stripos($row->region_title_en, $word) !== false) $row->matches++;
                if(isset($row->region_slug) && stripos($row->region_slug, $word) !== false) $row->matches++;
            }

            if ($closest['matches'] <= $row->matches ) {
                $closest['row'] = $row;
                $closest['matches'] = $row->matches;
            }


            if($closest['matches'] == 6) break; // No more need to check others, this matches fully
        }

        if(is_null($closest['row'])) return $cities[0];

        return $closest['row'];
    }

    /**
     * @param $country_id
     * @param null $limit
     * @param array $exclude
     * @return array
     */
    public function popularCitiesByCountryId($country_id, $limit = null, $excludeCities = [])
    {
        $country_id = intval($country_id);
        $where = '';

        if (!empty($excludeCities)) {
            $where .= ' AND c.id NOT IN (' . implode($excludeCities) . ')';
        }

        $sql = 'select c.id, c.' . Cubix_I18n::getTblField('title') . ' as title
                from escorts_in_cities eic
                inner join cities c on c.id = eic.city_id
                where eic.country_id = ' . $country_id . ' ' . $where . '
                group by eic.city_id order by count(eic.city_id) desc';

        if (!empty($limit)) {
            $sql .= ' LIMIT ' . intval($limit);
        }


        return parent::_fetchAll($sql);
    }

    public function getAllForGotd()
    {
        $sql = '
			SELECT c.id, c.country_id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM cities c
			ORDER BY title
		';

        return parent::_fetchAll($sql);
    }
}
