<?php

class Model_EscortV2Item extends Cubix_Model_Item
{
	public function __construct($rowData = array())
	{
		parent::__construct($rowData);

		$this->setThrowExceptions(false);
	}

	public function hasProduct($product_id)
	{	
		if ( ! isset($this->products) ) return false;

		return in_array($product_id, explode(',', $this->products));
	}

	public function hasStatusBit($status)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		return $client->call('Escorts.hasStatusBit', array($this->getId(), $status));
	}
	
	public function setStatusBit($status)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$client->call('Escorts.setStatusBit', array($this->getId(), $status));
	}
	
	public function removeStatusBit($status)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$client->call('Escorts.removeStatusBit', array($this->getId(), $status));
	}

	public function getTravelPlace($escort_id)
	{
		$sql = "SELECT travel_place FROM escorts WHERE id = ?";

		return $this->_adapter->fetchOne($sql, $escort_id);
	}

	public function getHandVerificationDate()
	{
		$sql = "SELECT last_hand_verification_date FROM escorts WHERE id = ?";

		return $this->_adapter->fetchOne($sql, $this->getId());
	}

	public function isPornStar()
	{
		$sql = "SELECT is_pornstar FROM escorts WHERE id = ?";

		return $this->_adapter->fetchOne($sql, $this->getId());
	}

	public function getIsVip()
	{
		$sql = "SELECT is_vip FROM escorts WHERE id = ?";

		return $this->_adapter->fetchOne($sql, $this->getId());
	}

	public function isOnline()
	{
		$sql = "
			SELECT 
				IF (ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE), 1, 0) AS is_login, e.allow_show_online
			FROM escorts e
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = e.user_id
			WHERE e.id = ?
		";

		$data = $this->_adapter->fetchRow($sql, $this->getId());

		$is_online = false;

		if ( $data->is_login && $data->allow_show_online ) {
			$is_online = true;			
		}

		return $is_online;
	}

	public function hasVideo(){
	    $sql = "SELECT TRUE FROM video where escort_id = ? LIMIT 1";
        return $this->_adapter->fetchOne($sql, $this->getId());
    }

	public function getIsPremium($escort_id)
	{
		$sql = "SELECT TRUE FROM escorts_in_cities WHERE escort_id = ? AND is_premium = 1";

		return $this->_adapter->fetchOne($sql, $escort_id);
	}

	public function isNew()
	{
		$sql = "
			SELECT
				UNIX_TIMESTAMP(date_registered) AS date_registered, UNIX_TIMESTAMP(date_activated) AS date_activated
			FROM escorts
			WHERE id = ? AND is_new = 1
		";

		return $this->_adapter->query($sql, $this->getId())->fetch();
	}
	
	public function getLangs()
	{
		return $this->langs;
		
		return $this->_adapter->query('
			SELECT el.lang_id AS id, el.level, l.title FROM escort_langs el
			LEFT JOIN langs l ON l.id = el.lang_id 
			WHERE el.escort_id = ?
		', $this->getId())->fetchAll();
	}

	public function getLangsSortedByLevel()
	{
		$fn_order_title = create_function('$a, $b', '
			if ( is_object($a) && is_object($b) ) {
				return strnatcmp($a->level, $b->level);
			}
			elseif ( is_array($a) && is_array($b) ) {
				return strnatcmp($a["level"], $b["level"]);
			}
			else return 0;
		');

		
		$langs = $this->langs;

		usort($langs, $fn_order_title);
		$langs = array_reverse($langs);

		return $langs;
	}
	
	public function isInFavorites()
	{
		$sql = 'SELECT 1 FROM favorites WHERE user_id = ? AND escort_id = ?';

		return $this->_adapter->query($sql, array(Model_Users::getCurrent()->id, $this->getId()))->fetch();
	}

	public function isChatOnline()
	{
		$sql = 'SELECT is_online FROM escorts WHERE id = ?';

		return $this->_adapter->fetchOne($sql, array($this->getId()));
	}

	public function getLastLoginDate(){
       $escort_user = $this->_adapter->query('SELECT user_id FROM escorts WHERE id = ? ', $this->getId())->fetch();
        return Cubix_Api_XmlRpc_Client::getInstance()->call('Users.get_last_login_date_v2', array($escort_user->user_id));
    }

	public function getMoreEscorts(array $city_ids, $limit = 10)
	{
		$sql = '
			SELECT e.showname FROM escorts e 
			INNER JOIN escort_cities ec ON e.id = ec.escort_id

			/*INNER JOIN escort_profiles ep ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN applications a ON a.id = u.application_id*/

			/*INNER JOIN countries c ON c.id = e.country_id*/
			/*INNER JOIN cities cc ON cc.id = ec.city_id*/
			
			/*INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1*/

			WHERE ec.city_id IN (?) AND e.id <> ?

			GROUP BY e.id
			ORDER BY RAND()
			LIMIT ?
		';
		
		$cities = implode(',', $city_ids);
		
		return $this->_adapter->query($sql, array($cities, $this->getId(), $limit))->fetchAll(Zend_Db::FETCH_ASSOC);
	}

	public function getSetcardInfo()
	{
		return (object) $this->getData(array('last_login_date','date_last_modified', 'date_registered', 'hit_count'));
		
		return $this->_adapter->query('
			SELECT e.date_last_modified, e.date_registered, ep.hit_count FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN escort_profiles ep ON ep.escort_id = e.id
			WHERE e.id = ?
		', $this->getId())->fetch();
    }

	function getDateByTimeZone($h, $sign, $datum = true, $dst = true) {
		//$sign = "+"; // Whichever direction from GMT to your timezone. + or -
		//$h = "1"; // offset for time (hours)

		if ($dst == true) {
			$daylight_saving = date('I');
			if ($daylight_saving) {
				if ($sign == "-") {
					$h = $h - 1;
				}
				else {
					$h = $h + 1;
				}
			}
		}
		echo $sifn . " " . $h;
		$hm = $h * 60;
		$ms = $hm * 60;
		
		if ($sign == "-") {
			$timestamp = time() - ($ms);
		}
		else { 
			$timestamp = time() + ($ms);
		}
		$gmdate = gmdate("m.d.Y. H:i", $timestamp);
		if($datum == true) {
			return $gmdate;
		}
		else
			return $timestamp;
	}

	public function getIsNowOpen()
	{		
		$is_now_open = $this->_adapter->query('
			SELECT is_now_open
			FROM escorts
			WHERE id = ?
		', array('id' => $this->getId()))->fetch();

		return $is_now_open->is_now_open;
	}

	public function getCountryData()
	{
		$lng = Cubix_I18n::getLang();

		$escort = $this->_adapter->query('
			SELECT e.country_id, cr.title_' . $lng . ' AS title
			FROM escorts e
			INNER JOIN countries cr ON cr.id = e.country_id
			WHERE e.id = ?
		', array('id' => $this->getId()))->fetch();

		return $escort;
	}
	
	public function getWorkTimes()
	{

		$data = $this->getData(array('is_open', 'working_times', 'available_24_7'));
		$working_times = $data['working_times'];
		if ( count($working_times) ) {
			$h = date('G');
			$m = date('i');
			$h = intval($h) * 60 + intval($m);

			$data['is_open'] = false;

			$day_index = date('N', time());
			if ( isset($working_times[$day_index]) && $working_times[$day_index] ) {
				$wt = $working_times[$day_index]; if ( ! is_object($wt) ) $wt = (object) $wt;
				$wt_from = intval($wt->from) * 60 + intval($wt->from_m);
				$wt_to = intval($wt->to) * 60 + intval($wt->to_m);

				if (
					( $wt->day_index == $day_index && $wt_from <= $h && $wt_to >= $h ) ||
					( $wt->day_index == $day_index && $wt_from <= $h && $wt_from > $wt->to ) 
				) {
					$data['is_open'] = true;
				}
				else {
					$data['is_open'] = false;
				}
			}

			// Previous day
			$day_index = (1 == $day_index ? 7 : $day_index - 1);
			if ( ! $data['is_open'] && isset($working_times[$day_index]) && $working_times[$day_index] ) {
				$wt = $working_times[$day_index];
				$wt_from = intval($wt->from) * 60 + intval($wt->from_m);
				$wt_to = intval($wt->to) * 60 + intval($wt->to_m);

				if ( $wt_to >= $h && $wt_from > $wt_to ) {
					$data['is_open'] = true;
				}
			}
		}

		return $data;
	}
	
	public function getAdditionalCities($base_city_id)
	{
		//return array();

        $client = Cubix_Api_XmlRpc_Client::getInstance();
        return $client->call('Escorts.AdditionalCities', array($this->getId(),$base_city_id));


	}

	public function getCityzones()
	{
		return $this->_adapter->query('
			SELECT
				cz.slug AS zone_slug, cz.' . Cubix_I18n::getTblField('title') . ' AS zone_title,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title, ct.id AS city_id,
				c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, ct.slug AS city_slug
			FROM cityzones cz
			INNER JOIN escort_cityzones ecz ON ecz.city_zone_id = cz.id
			INNER JOIN cities ct ON ct.id = cz.city_id
			INNER JOIN countries c ON c.id = ct.country_id
			INNER JOIN regions r ON r.id = ct.region_id
			WHERE ecz.escort_id = ?
		', array($this->getId()))->fetchAll();

	}
	
	public function getTravelConditions()
	{
		return (object) $this->getData(array(
			'min_book_time',
			'min_book_time_unit',
			'has_down_pay',
			'down_pay',
			'down_pay_cur',
			'reservation'
		));
	}
	
	public function getTravelPaymentMethods()
	{
		$data = (object) $this->getData(array(
			'travel_payment_methods'
		));
		
		$pay_methods = array();
	
		if ( count($data->travel_payment_methods) ) {
			foreach ( $data->travel_payment_methods as $d ) {
				$d = (object) $d;
				$pay_methods[] = $d->payment_method;
				if ($d->payment_method == PM_OTHER){
					$pay_methods['other_text'] = $d->specify;
				}
			}
		}
		
		return $pay_methods;
	}
	
	public function getContacts()
	{
		$contacts = (object) $this->getData(array(
			'base_city_en',
			'base_city_pt',
			'base_city_ro',
			'base_city_it',
			'base_city_gr',
			'base_city_fr',
			'base_city_de',
			'base_city_es',
			'base_city_ru',
			'base_city_id',
			'country_en',
			'country_pt',
			'country_ro',
			'country_it',
			'country_gr',
			'country_fr',
			'country_de',
			'country_es',
			'country_ru',
			'country_slug',
			'country_iso',
			'region_slug',
			'region_title_en',
			'region_title_pt',
			'region_title_ro',
			'region_title_it',
			'region_title_gr',
			'region_title_fr',
			'region_title_de',
			'region_title_es',
			'region_title_ru',
			'city_slug',
			'vac_date_to',
			'phone_number',
			'phone_number_alt',
			'phone_instr',
			'phone_instr_no_withheld',
			'phone_instr_other',
			'phone_country_id',
			'disable_phone_prefix',
			'club_name',
			'street',
			'street_no',
			'website',
			'email',
			'available_24_7',
			'zip',
			'prefered_contact_methods'));

		$row = $this->_adapter->fetchRow("SELECT co.id as country_id, r.id as region_id, c.slug as city_slug FROM countries co 
            INNER JOIN cities c ON c.country_id = co.id 
            LEFT JOIN regions r ON r.id = c.region_id 
            WHERE c.id = ?", $contacts->base_city_id
        );
       

		$contacts->country_id = $row->country_id;
		$contacts->city_slug = $row->city_slug;
		$contacts->region_id = $row->region_id;

		return $contacts;
	}
	
	public function getCitytours()
	{
		$tours = array();
		
		$tours[] = $this->_adapter->query('
			SELECT e.tour_id AS id, e.tour_date_from AS date_from, e.tour_date_to AS date_to, e.tour_phone AS phone, c.id AS country_id,
				e.tour_city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country_title,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, ct.slug AS city_slug, ct.id AS city_id,
				e.is_on_tour AS is_in_tour,
				0 AS is_upcoming_tour
			FROM escorts e

			INNER JOIN cities ct ON ct.id = e.tour_city_id
			INNER JOIN countries c ON c.id = ct.country_id
			
			LEFT JOIN regions r ON r.id = ct.region_id
			WHERE e.id = ?
		', $this->getId())->fetch();

		$up_tours = $this->_adapter->query('
			SELECT ut.tour_id AS id, ut.tour_date_from AS date_from, ut.tour_date_to AS date_to, ut.phone, c.id AS country_id,
				ut.tour_id AS tour_city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country_title,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, ct.slug AS city_slug, ct.id AS city_id,
				0 AS is_in_tour,
				1 AS is_upcoming_tour
			FROM escorts e

			INNER JOIN upcoming_tours ut ON ut.id = e.id
			INNER JOIN cities ct ON ct.id = ut.tour_city_id
			INNER JOIN countries c ON c.id = ct.country_id

			LEFT JOIN regions r ON r.id = ct.region_id
			WHERE e.id = ?
		', $this->getId())->fetchAll();

		/*var_dump($tours);
		var_dump($up_tours);*/

		if ( isset($tours[0]->id) && ! isset($tours[0]->id) ) {
			return $tours;
		}
		else if ( ! isset($tours[0]->id) && isset($up_tours[0]->id) ) {
			return $up_tours;
		}
		else if ( isset($up_tours[0]->id) && isset($tours[0]->id) ) {
			$tours = array_merge($tours, $up_tours);
		}

		if( ! isset($tours[0]->id) && ! isset($up_tours[0]->id) ) {
			return false;
		}

		return $tours;
	}
	
	public function getBio()
	{
		return (object) $this->getData(array(
			'gender',
			 Cubix_I18n::getTblField('nationality_title'),
			'birth_date',
			'eye_color',
			'hair_color',
			'hair_length',
			'height',
			'weight',
			'bust',
			'waist',
			'breast_type',
			'hip',
			'shoe_size',
			'cup_size',
			'dress_size',
			'is_smoking',
			'is_drinking',
			'pubic_hair',
			'measure_units',
			'incall_type',
			'incall_hotel_room',
			'incall_other',
			'outcall_type',
			'outcall_other',
			'sex_orientation',
			'sex_availability',
			'ethnicity',
			 Cubix_I18n::getTblField('about'),
			'characteristics',
			'cuisine', 
			'drink', 
			'flower', 
			'perfume', 
			'designer', 
			'interests',
			'sports', 
			'hobbies',
			'gifts',
			'tatoo',
			'piercing',
			'home_city_id'
		));
	}	
	
	public function getAbout()
	{
		return (object) $this->getData(array(
			'showname',
			'nationality_iso',
			'nationality_title_en',
			'nationality_title_it',
			'nationality_title_gr',
			'nationality_title_fr',
			'nationality_title_de',
			'nationality_title_es',
			'nationality_title_pt',
			'nationality_title_ro',
			'nationality_title_ru',
			'birth_date',
			'eye_color',
			'hair_color',
			'hair_length',
			'height',
			'weight',
			'bust',
			'waist',
			'breast_type',
			'hip',
			'shoe_size',
			'cup_size',
			'dress_size',
			'is_smoking',
			'is_drinking',
			'pubic_hair',
			'measure_units',

			'zip',
			
			'incall_type',
			'incall_hotel_room',
			'incall_other',
			'outcall_type',
			'outcall_other',

			'sex_orientation',
			'sex_availability',
			'ethnicity',
			'about_en',
			'about_it',
			'about_gr',
			'about_fr',
			'about_de',
			'about_pt',
			'about_ro',
			'about_es',
			'about_ru',
			'characteristics',
			'cuisine', 
			'drink', 
			'flower', 
			'perfume', 
			'designer', 
			'interests',
			'sports', 
			'hobbies',
			'gifts',
			'tatoo',
			'piercing',
			'special_rates'
		));
		
		/*$about = $this->_adapter->query('
			SELECT e.showname, n.iso AS nationality_iso, n.' . Cubix_I18n::getTblField('title') . ' AS nationality_title, FLOOR(DATEDIFF(CURDATE(), ep.birth_date) / 365.25) as age, ep.eye_color,
				ep.hair_color, ep.hair_length, ep.height, ep.weight, ep.bust, ep.waist, ep.hip,
				ep.shoe_size, ep.breast_size, ep.dress_size, ep.is_smoker,
				ep.measure_units, ep.availability, ep.sex_availability, ep.ethnicity,
				ep.' . Cubix_I18n::getTblField('about') . ' AS about, ep.characteristics, ep.cuisine, ep.drink, ep.flower, ep.perfume, ep.designer,	ep.interests, ep.sports, ep.hobbies, ep.special_rates
			FROM escorts e
			LEFT JOIN escort_profiles ep ON ep.escort_id = e.id
			
			LEFT JOIN nationalities n ON n.id = ep.nationality_id
			
			WHERE e.id = ?
		', $this->getId())->fetch();
		

		return $about;*/
	}	
	
	public function getServices()
	{
		/*$datum = (object) $this->getData(array('svc_kissing', 'svc_blowjob', 'svc_cumshot', 'svc_69', 'svc_anal', 'svc_additional'));
		
		if ( $datum->svc_kissing ) {
			return $datum;
		}
		
		if ( $datum->svc_blowjob ) {
			return $datum;
		}
		
		if ( $datum->svc_cumshot ) {
			return $datum;
		}
		
		if ( $datum->svc_69 ) {
			return $datum;
		}
		
		if ( $datum->svc_anal ) {
			return $datum;
		}
		
		if ( $datum->svc_additional ) {
			return $datum;
		}
		
		return false;*/
		
		$services = $this->_adapter->query('
			SELECT service_id, price, currency_id
			FROM escort_services
			WHERE escort_id = ?
			ORDER BY service_id ASC
		', $this->getId())->fetchAll();
				
		return $services;
	}

	public function getKeywords()
	{
		$keywords = $this->_adapter->query('
			SELECT keyword_id
			FROM escort_keywords
			WHERE escort_id = ?
			ORDER BY keyword_id ASC
		', $this->getId())->fetchAll();

		return $keywords;
	}

	public function getAdditionalServices()
	{
		return (object) $this->getData(array(
			'additional_service_en',
			'additional_service_it',
			'additional_service_gr',
			'additional_service_fr',
			'additional_service_de',
			'additional_service_es',
			'additional_service_pt',
			'additional_service_ro',
			'additional_service_ru',
		));
	}

	public function getHHRatesInfo()
	{
		$hh = $this->_adapter->query('
			SELECT hh_is_active, showname, UNIX_TIMESTAMP(hh_date_from) AS hh_date_from, UNIX_TIMESTAMP(hh_date_to) AS hh_date_to FROM escorts
			WHERE id = ?
		', $this->getId())->fetch();

		return $hh;
	}

	public function getHHRates()
	{
		$DEFINITIONS = Zend_Registry::get('defines');

		$currencies = $DEFINITIONS['currencies'];
		$time_units = $DEFINITIONS['time_unit_options'];

		$rates = $this->_adapter->query('
			SELECT rates FROM escorts
			WHERE id = ?
		', $this->getId())->fetch();

		if ( is_string($rates->rates) ) {
			$rates = @unserialize($rates->rates);
		}
		
		if ( ! is_array($rates) ) {
			return array();
		}

		if ( isset($rates['travel_rates']) ) {
			unset($rates['travel_rates']);
		}

		foreach ( $rates as $i => $rate ) {
			$rates[$i] = (object) $rate;
		}

		$ret = array();
		foreach ( $rates as $rate ) {
			
			$new_values = array(
				'currency_title'  => $currencies[$rate->currency_id],
				'time_unit_title' => $time_units[$rate->time_unit]
			);
			$rate = (object) array_merge((array) $rate, $new_values);
			@$ret[$rate->availability][] = $rate;
		}

		$ret = $this->_sortRates($ret);

		return $ret;
	}
	
	public function getTravelRates()
	{
		$DEFINITIONS = Zend_Registry::get('defines');

		$currencies = $DEFINITIONS['currencies'];
		$time_units = $DEFINITIONS['time_unit_options'];

		$rates = $this->_adapter->query('
			SELECT rates FROM escorts
			WHERE id = ?
		', $this->getId())->fetch();

		if ( is_string($rates->rates) ) {
			$rates = @unserialize($rates->rates);
		}
		
		if ( ! is_array($rates['travel_rates']) ) {
			return array();
		}

		foreach ( $rates as $i => $rate ) {
			$rates[$i] = (object) $rate;
		}

		$ret = array();
		
		foreach ( $rates['travel_rates'] as $rate ) {
			
			$new_values = array(
				'currency_title'  => $currencies[$rate->currency_id],
				'time_unit_title' => $time_units[$rate['time_unit']]
			);
			$rate = (object) array_merge((array) $rate, $new_values);
			@$ret[$rate->availability][] = $rate;
		}

		$ret = $this->_sortRates($ret);

		return $ret;
	}
	
	public function getRates()
	{
		$DEFINITIONS = Zend_Registry::get('defines');
		
		//$currencies = $DEFINITIONS['currencies'];
        // Use 1 for currency title showcase, 2 - for symbols
		$currencies = Model_Currencies::getAllAssoc(2);
		$time_units = $DEFINITIONS['time_unit_options'];
		
		if ( is_string($this->rates) ) {
			$rates = @unserialize($this->rates);
		}
		else {
			$rates = $this->rates;
		}
		
		if ( ! is_array($rates) ) {
			return array();
		}
		
		foreach ( $rates as $i => $rate ) {
			$rates[$i] = (object) $rate;
		}
		
		

		$ret = array();
		foreach ( $rates as $rate ) {
			$new_values = array(
				'currency_title'  => $currencies[$rate->currency_id],
				'time_unit_title' => $time_units[$rate->time_unit]
			);
			$rate = (object) array_merge((array) $rate, $new_values);
			@$ret[$rate->availability][] = $rate;
		}

		$ret = $this->_sortRates($ret);

		return $ret;
	}

	protected function _sortRates($rates)
	{
		$ret = array();

		foreach ( $rates as $k => $rate )
		{
			$new_array = array();
			$over = false;
			$b_weekend = false;
			$b_dinner = false;
			$b_add_hour = false;

			$overnight = null;
			$add_hour = null;
			$weekend = null;
			$dinner_date = null;

			foreach ( $rate as $r ) {
				if ($r->type == 'overnight') {
					$overnight = $r;
					$over = true;
				}
				elseif ($r->type == 'additional-hour') {
					$add_hour = $r;
					$b_add_hour = true;
				}
				elseif ($r->type == 'weekend') {
					$weekend = $r;
					$b_weekend = true;
				}
				elseif ($r->type == 'dinner-date') {
					$dinner_date = $r;
					$b_dinner = true;
				}
				elseif ($r->time_unit == TIME_DAYS) {
					$new_array[($r->time * 1440)] = $r;
				}
				elseif ($r->time_unit == TIME_HOURS) {
					$new_array[($r->time * 60)] = $r;
				}
				elseif ($r->time_unit == TIME_MINUTES) {
					$new_array[$r->time] = $r;
				}
			}

			ksort($new_array);

			$sorted = array();

			foreach ($new_array as $rate) {
				$sorted[] = $rate;
			}

			if ($over)
				$sorted[] = $overnight;
			if ($b_add_hour)
				$sorted[] = $add_hour;
			if ($b_dinner)
				$sorted[] = $dinner_date;
			if ($b_weekend)
				$sorted[] = $weekend;
			$sorted = (object) $sorted;
			$ret[$k] = $sorted;
		}
		
		return $ret;
	}
	
	public function getPhotos($page = 1, &$count = null, $with_main = true, $only_privates = false, $config = null, $perPage = null, $new_list = false, $portrait_first = false,$gallery_id = null)
	{
		if(!$config){
            $config = Zend_Registry::get('escorts_config');
        }

        if($perPage){
           $config['photos']['perPage'] = $perPage;
        }
		
		$where = '';
		
		if ( ! $with_main ) {
			$where .= ' AND ep.is_main = 0 ';
		}
		
		if ( $only_privates ) {
			$where .= ' AND ep.type = 3';
		}
		// Only normal photos
		// else {
		// 	$where .= ' AND ep.type <> 3';
		// }
		
		if ( isset($gallery_id) ) {
			$where .= ' AND ep.gallery_id = '.$gallery_id ;
		}
		/*if ($new_list)
			$where .= ' AND ep.type <> 3';*/

		if ( ! is_null($page) && ($page = intval($page)) < 1 ) $page = 1;
		$limit = '';
		if ( ! is_null($page) && $count ) {
			//$limit = 'LIMIT ' . ($page - 1) * $config['photos']['perPage'] . ', ' . $config['photos']['perPage'];
			$limit = ' LIMIT ' . ($page - 1) * $count . ', ' . $count;
		}
		
		$ordering = " ORDER BY ep.is_main DESC, ep.ordering ASC, ep.id ASC ";
		
		if ( $portrait_first ) {
			$ordering = " ORDER BY ep.is_portrait DESC, ep.is_main DESC, ep.ordering, ep.id ASC ";
		}
		
		$photos = $this->getAdapter()->query("
			SELECT SQL_CALC_FOUND_ROWS " . Cubix_Application::getId() . " AS application_id, ep.* FROM escort_photos ep
			WHERE ep.escort_id = ? {$where} {$ordering}
			{$limit}
		", $this->getId())->fetchAll();

		$countSql = "
			SELECT FOUND_ROWS();
		";

		$count = $this->getAdapter()->fetchOne($countSql);

		foreach ($photos as $i => $photo) {
			$photos[$i] = new Model_Escort_PhotoItem($photo);
		}
		
		return $photos;
	}
	
	public function getPhotosCount()
	{
		$countSql = "
			SELECT COUNT(ep.id) as count 
			FROM escort_photos ep
			/*INNER JOIN escorts e ON e.id = ep.escort_id*/
			WHERE ep.escort_id = ? /*{$where}*/
		";
		
		$count = $this->getAdapter()->query($countSql, $this->getId())->fetch()->count;
		
		return $count;
	}
	
	public function getPhoto($id)
	{
		$photo = $this->_adapter->query('
			SELECT * FROM escort_photos 
			WHERE escort_id = ? AND id = ?
		', array($this->getId(), $id))->fetch();
        $photo->application_id =  $this->application_id;
 
		return new Model_Escort_PhotoItem($photo);
		
		return array();
	}
	
	public function getMainPhoto()
	{
		/*$photo = $this->_adapter->query('
			SELECT * FROM escort_photos 
			WHERE escort_id = ? AND hash = ?
		', array($this->id, $this->photo_hash))->fetch();

		return new Model_Escort_PhotoItem($photo);*/

		 return new Model_Escort_PhotoItem(array(
		 	'application_id' => $this->application_id,
		 	'escort_id' => $this->id,
		 	'hash' => $this->photo_hash,
		 	'ext' => $this->photo_ext,
		 	'photo_status' => $this->photo_status,
            'args'=>$this->photo_args
		 ));
	}
	
	public function getVacation()
	{
		$sql = "SELECT vac_date_from, vac_date_to FROM escorts WHERE id = ?";
		
		return $this->getAdapter()->query($sql, $this->getId())->fetch();
	}
	
	public function updateVacation($vac_date_from, $vac_date_to )
	{
		$db = $this->getAdapter();
		$escort_id = $this->getId();
		$db->update('escorts', array('vac_date_from' => $vac_date_from ,'vac_date_to' =>  $vac_date_to), $db->quoteInto('id = ?',$escort_id ));
	}


	public function getVerifyRequest()
	{
		$model = new Model_VerifyRequests();
		
		$item = $model->getByEscortId($this->getId());
		
		return $item;
	}
	
	public function isVerified()
	{
		$sql = "SELECT verified_status FROM escorts WHERE id = ?";

		$status = $this->getAdapter()->query($sql, $this->getId())->fetch();

		if ( $status->verified_status == Model_Escorts::VERIFIED_STATUS_VERIFIED || $status->verified_status == Model_Escorts::VERIFIED_STATUS_VERIFIED_RESET)
		{
			return true;
		}

		return false;
	}

	public function isSuspicious()
	{
		$sql = "SELECT is_suspicious FROM escorts WHERE id = ?";

		$status = $this->getAdapter()->query($sql, $this->getId())->fetch();

		return $status->is_suspicious;
	}
	
	public function isInAgency($agency_id)
	{
		return $this->agency_id == $agency_id;
	}

	public function vote($member_id)
	{
		if ( ! $this->id ) {
			throw new Exception('ID is required property');
		}

		$client = Cubix_Api::getInstance();
		return $client->call('voteForEscort', array($member_id, $this->id));
	}

	/**
	 * Need this func for ability to serialize this object
	 *
	 * @author GuGo
	 */
	public function __wakeup()
	{
		$this->setAdapter(Model_Escorts::getAdapter());
	}

	public function getProfile()
	{
		$model = new Model_Escorts();

		return $model->getProfile($this->getId());
	}

	public function hasZeroPackage()
	{
		$sql = "SELECT package_id FROM escorts WHERE id = ?";

		$package_id = $this->getAdapter()->fetchOne($sql, $this->getId());

		if ( Cubix_Application::getId() == 2 && $package_id == 97 ) {
			return true;
		}
		else if ( Cubix_Application::getId() == 1 && $package_id == 100 ) {
			return true;
		}

		return false;
	}
	
	public function getPhotosApi($page = 1, &$count = null, $with_main = true, $portrait_first = false, $gallery_id = null )
	{
		$config = Zend_Registry::get('escorts_config');
	
		$data = array(
			'page' => $page,
			'per_page' => 1000, //$config['photos']['perPage'],
			'with_main' => $with_main,
			'escort_id' => $this->getId(),
			'portrait_first' => $portrait_first,
			'gallery_id' => $gallery_id
		);
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$photos = $client->call('Escorts.getPhotos', array($data));
		
		$count = $photos['count'];

		$photos_l = array();
		foreach ($photos['list'] as $i => $photo) {
			$photos_l[$i] = new Model_Escort_PhotoItem($photo);
		}
		
		return $photos_l;
	}
	
	public function getPhotoGalleries($escort_id)
	{
		$galleries =  $this->_adapter->fetchAssoc('
			SELECT id , title, is_main 
			FROM escort_galleries
			WHERE escort_id = ?
		', $escort_id);
		
		if(count($galleries) == 0){
			$galleries = array( 0 => array('id' => 0, 'title' => 'Gallery 1', 'is_main' => 1 ) );
		}
		return $galleries;
	}
	
	public function arrangeBio($data)
	{
		$lng = Cubix_I18n::getLang();
		$DEFINITIONS = Zend_Registry::get('defines');
		
		// key -> dictionary_id, value ->field
		$bio_map = array(
			'gender' => 'gender_options|gender',
			'ethnic' => 'ethnic_options|ethnicity',
			'nationality' => 'no-def|nationality_title_'.$lng,
			'age' => 'no-def|birth_date',
			'eyes' => 'eye_color_options|eye_color',
			'hair' => 'hair_color_options|hair_color',
			'hair_length' => 'hair_length_options|hair_length',
			'pubic_hair' => 'pubic_hair_options|pubic_hair',
			'tattoo' => 'tatoo_options|tatoo',
			'piercing' => 'piercing_options|piercing',
			'height' => 'no-def|height',
			'weight' => 'no-def|weight',
			'bust-waist-hip' => 'no-def|bust',
			'cup_size' => 'no-def|cup_size',
			'breast_type' => 'breast_type_options|breast_type',
			'shoe_size' => 'no-def|shoe_size',
			'dress_size' => 'no-def|dress_size',
			'smoker' => 'y_n_oc_options|is_smoking',
			'service_for' => 'sex_availability_options|sex_availability',
			'drinking' => 'y_n_oc_options|is_drinking',
			'characteristics' => 'no-def|characteristics',
			'incall_type' => 'no-def|incall_type',
			'outcall_type' => 'no-def|outcall_type',
			'orientation' => 'sexual_orientation_options|sex_orientation',
			'favorite_cuisine' => 'no-def|cuisine', 			
			'favorite_drink' => 'no-def|drink',
			'favorite_flower' => 'no-def|flower',
			'favorite_perfume' => 'no-def|perfume',
			'favorite_designer' => 'no-def|designer',
			'interests' => 'no-def|interests',
			'sports' => 'no-def|sports',
			'gifts' => 'no-def|gifts',
			'other_hobbies' => 'no-def|hobbies',
			'languages' => 'no-def|languages',
			'home_city' => 'no-def|home_city_id' 
		);
		
		
		foreach($bio_map as $key => $value){
			list($def_key, $field) = explode( "|", $value);
			
			if(isset($data->{$field}) && $data->{$field}){
				
				if($key == 'age'){
					$bio_map[$key] = Cubix_Utils::dateToAge($data->birth_date);
				}
				elseif($key == 'height'){
					$bio_map[$key] =  $data->height . ' cm (' . Cubix_UnitsConverter::convert($data->height, 'cm', 'ftin') . ')';
				}
				elseif($key == 'weight'){
					$bio_map[$key] =  $data->weight . ' kg (' . (int) Cubix_UnitsConverter::convert($data->weight, 'kg', 'lbs') . ' lbs)';
				}
				elseif($key == 'bust-waist-hip'){
					$bio_map[$key] = $data->bust .'-'. $data->waist .'-'.$data->hip;
				}
				elseif($key == 'service_for'){
					
					$service_avail_for = explode(',', $data->sex_availability);
					$service_array = array();
					foreach($service_avail_for as $for){
						$service_array[] = $DEFINITIONS[$def_key][$for];
					}
					$bio_map[$key] = implode(', ', $service_array);
				}
				elseif($key == 'home_city'){
					
					$m_city = new Cubix_Geography_Cities();
					$home_city = $m_city->get($data->home_city_id);
					$m_country = new Cubix_Geography_Countries();
					$home_country = $m_country->get($home_city->country_id);
					$bio_map[$key] = $home_city->{'title_' . $lng} . ' (' . $home_country->{'title_' . $lng} . ')';
				}
				else{
					$bio_map[$key] = $def_key == 'no-def' ? $data->{$field} : $DEFINITIONS[$def_key][$data->{$field}];
				}
			}
			else{
				unset($bio_map[$key]);
			}
		}
		
		$count = ( count($bio_map) + count($bio_map['languages']) - 1);
		return array('rows' => $bio_map, 'count' => $count);
		
	}

    public function getAge()
    {
        $sql = "
			SELECT age
			FROM escorts
			WHERE id = ?
		";

        return $this->_adapter->fetchOne($sql, $this->getId());
    }
}
