<?php

class Model_Agencies extends Cubix_Model
{
	protected $_table = 'agencies';
	protected $_itemClass = 'Model_AgencyItem';

	public static function hit($agency_id)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		$client->call('Agencies.updateHitsCount', array($agency_id));

		// for front DB
		self::getAdapter()->query('UPDATE club_directory SET hit_count = hit_count + 1 WHERE agency_id = ?', $agency_id);
	}
	
	public function getNameById($id)
	{
		$sql = "SELECT club_name
			FROM club_directory
			WHERE id = ?
		";
		
		return self::db()->fetchOne($sql, $id);
		
	}

	public function getSlugById($id)
	{
		$sql = "SELECT club_slug
			FROM club_directory
			WHERE id = ?
		";
		
		return self::db()->fetchOne($sql, $id);
		
	}

    public function getPmIsBlocked($agency_id)
    {
        $client = Cubix_Api_XmlRpc_Client::getInstance();

        return $client->call('Agencies.getPmIsBlocked', array($agency_id));
    }

    public function getRevComById($id)
    {
        return parent::getAdapter()->query('SELECT disabled_reviews, disabled_comments FROM escorts WHERE agency_id = ?', $id)->fetch();
    }
	
	public function getByUserId($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$agency = $client->call('Agencies.getByUserId', array($user_id));
		
		$agency = new Model_AgencyItem($agency);
		
		return $agency;
	}

	public static function updateLastModified($agency_id)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		$client->call('Agencies.updateLastModified', array($agency_id));

		
	}
	
	public function get($agency_id)
	{
		$sql = "
			SELECT a.agency_id, a.club_name AS showname, a.email
			FROM club_directory a
			WHERE a.agency_id = ?
		";

		return parent::_fetchRow($sql, $agency_id);
	}

	public function getAllIds()
	{
		$sql = "
			SELECT a.agency_id
			FROM club_directory a
		";

		return parent::_fetchAll($sql);
	}
	
	public function getAll($page = 1, $page_size = 20, $city_id = null, $sorting = null, $country_id = null, &$count = 0, $list_type = 'simple', $geo_data = array(), $online_now = false, $agency_filter = null, $agency_has_esc_type = 1, $zone_id = null, $region_id = null)
	{
		$lng = Cubix_I18n::getLang();

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;

		$where = " WHERE 1 ";

		if ( $city_id ) {
			$where .= " AND eic.city_id = " . (int) $city_id;
		}
		$join = '';
		if ( $zone_id) {
		    $join = ' LEFT JOIN escorts_in_cityzones as eicz on  eicz.escort_id = e.id ';
			$where .= " AND eicz.cityzone_id = " . (int) $zone_id;
		}
		if ( $region_id) {
			$where .= " AND eic.region_id = " . (int) $region_id;
		}

		if ( $country_id ) {
			$where .= " AND e.country_id = " . (int) $country_id;
		}

		if ( $online_now ) {
			$where .= " AND ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1 ";
		}

		if ($agency_filter)
		{
			$where .= " AND cd.club_name LIKE '" . $agency_filter . "%'";
		}
		
		$order = self::_mapSorting($sorting);
	
		$fields = ' NULL AS distance ';
		if ( $sorting == 'close-to-me' && count($geo_data) ) {
			if (array_key_exists("latitude", $geo_data) && strlen($geo_data['latitude']) > 0 && array_key_exists("longitude", $geo_data) && strlen($geo_data['longitude']) > 0) {
				$fields = '
					((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						))
						)
					)) AS distance
				';
			}
		}

		// Select other about texts if current is empty
        // ---------------------------------
		$about_field = [];
        $langs = Cubix_Application::getLangs();
        foreach($langs as $_l) {
            $about_field[] = "WHEN LENGTH(cd.about_{$_l->id}) > 0 THEN cd.about_" . $_l->id;
        }
        $about_field = implode(' ', $about_field);
        $about_field = ' 
          CASE
            WHEN LENGTH(cd.about_' . $lng . ') > 0 THEN cd.about_' . $lng . '
            '.$about_field.'
          ELSE cd.about_' . $lng . ' 
          END AS about
        ';
        // ---------------------------------

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				cd.agency_id AS id,
				cd.club_name AS showname,
				cd.is_new,
				UNIX_TIMESTAMP(cd.date_registered) AS date_registered,
				cd.user_id,			
				ct.slug AS city_slug,
				ct.id AS city_id,
				ct.title_' . $lng . ' AS city,			
				cr.id AS country_id,
				cr.slug AS country_slug,
				cr.title_' . $lng . ' AS country,				
				cd.photo_hash,
				cd.photo_ext,				
				69 AS application_id,			
				cd.email,
				cd.web AS website,				
				cd.phone_country_id,				
				cd.phone AS phone_number,
				cd.hit_count,				
				cd.last_modified AS date_last_modified,
				cd.is_premium,				
				'.$about_field.',
				cd.club_name AS agency_name,
				cd.club_slug AS agency_slug,
				/*cd.escorts_count,*/
				cd.verified_escorts_count,
				IF (ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE), 1, 0) AS is_login, cd.allow_show_online,
				cd.agency_id AS agency_id, ' . $fields . '


				/*cd.*, ct.title_' . $lng . ' AS city_title, cr.id AS country_id, cr.slug AS country_slug, cr.title_' . $lng . ' AS country_title, ct.slug AS city_slug,
				ct.id AS city_id, ' . $fields . '*/
			FROM club_directory cd
			INNER JOIN escorts e ON e.agency_id = cd.agency_id AND e.type = '.$agency_has_esc_type.'
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id '
            . $join .
			' LEFT JOIN cities ct ON ct.id = cd.city_id
			LEFT JOIN countries cr ON cr.id = cd.country_id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = cd.user_id
			' . $where . ' AND cd.escorts_count > 0 
			GROUP BY cd.agency_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		';

		$agencies = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		if ( count($agencies) ) {
			foreach ($agencies as $agency)
			{
				$params = array('e.agency_id = ?' => $agency->id,'e.type = ?' => $agency_has_esc_type);
				if ( $country_id ) {
					$params['e.country_id = ?'] = $country_id;
				}
				$escorts_count = 0;
				$agency->a_escorts = Model_Escort_List::getAgencyEscorts($params, 'newest', 1, 15, $escorts_count);
				$agency->escorts_count = $escorts_count;
			}
		}
		
		if ( count($agencies) && $list_type == 'simple' ) {
			$grouped_agencies = array();
			foreach($agencies as $k => $agency) {

				if ( $sorting == 'by-country' ) {
					$group_field = $agency->country;
				} else {
					$group_field = $agency->city;
				}
				$def_group = __('international');

				if ( $agency->is_international && ! $group_field ) {
					$grouped_agencies['city_groups'][$def_group][] = $agency;
				} else if ( $group_field ) {
					$grouped_agencies['city_groups'][$group_field][] = $agency;
				} else if ( ! $agency->is_international && ! $group_field ) {
					$grouped_agencies['city_groups'][$def_group][] = $agency;
				}
				
				if ( $sorting != 'close-to-me' && $sorting != 'newest' && $sorting != 'most-viewed' ) {
					ksort($grouped_agencies['city_groups']);
				}
			}

			if ( $page == 1 && $list_type = 'simple' ) {
				
				$premium_escorts = self::getAllPremiums($city_id, $country_id);

				if ( count($premium_escorts) ) {
					foreach( $premium_escorts as  $k => $p_escort ) {

						if ( $p_escort->is_premium ) {
							$grouped_agencies['premiums'][] = $p_escort;
						}
					}
				}
			}

			$agencies = $grouped_agencies;
		}
	
		return $agencies;
	}

	public static function getAllPremiums($city_id = null, $country_id = null)
	{
		$lng = Cubix_I18n::getLang();

		$where = " WHERE 1 ";

		if ( $city_id ) {
			$where .= " AND cd.city_id = " . (int) $city_id;
		}

		if ( $country_id ) {
			$where .= " AND cd.country_id = " . (int) $country_id;
		}

		$order = self::_mapSorting($sorting);

		$sql = '
			SELECT
				cd.agency_id AS id,
				cd.club_name AS showname,
				cd.is_new,
				UNIX_TIMESTAMP(cd.date_registered) AS date_registered,
				cd.user_id,			
				ct.slug AS city_slug,
				ct.id AS city_id,
				ct.title_' . $lng . ' AS city,			
				cr.id AS country_id,
				cr.slug AS country_slug,
				cr.title_' . $lng . ' AS country,				
				cd.photo_hash,
				cd.photo_ext,				
				69 AS application_id,			
				cd.email,
				cd.web AS website,				
				cd.phone_country_id,				
				cd.phone AS phone_number,
				cd.hit_count,				
				cd.last_modified AS date_last_modified,
				cd.is_premium,				
				cd.about_' . $lng . ' AS about,
				cd.club_name AS agency_name,
				cd.club_slug AS agency_slug,
				cd.escorts_count,
				IF (ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE), 1, 0) AS is_login, cd.allow_show_online,
				cd.agency_id AS agency_id,
				1 AS is_agency
			FROM club_directory cd
			LEFT JOIN cities ct ON ct.id = cd.city_id
			LEFT JOIN countries cr ON cr.id = cd.country_id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = cd.user_id
			' . $where . '
			GROUP BY cd.agency_id
			ORDER BY ' . $order . '
		';
		

		return self::db()->fetchAll($sql);
	}

	public static function getButtonAgenciesCounts($country_id = null, $city_id = null, $online_now = false) 
	{
		$geo_where = "WHERE 1";

		if ( $country_id ) {
			$geo_where .= " AND cr.id = " . $country_id . " ";	
		}

		if ( $city_id ) {
			$geo_where .= " AND ct.id = " . $city_id . " ";	
		}

		if ( $online_now ) {
			$geo_where .= "  AND ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE) AND allow_show_online = 1 ";	
		}		

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				TRUE
			FROM club_directory cd
			LEFT JOIN cities ct ON ct.id = cd.city_id
			LEFT JOIN countries cr ON cr.id = cd.country_id
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = cd.user_id
			' . $geo_where . '
			GROUP BY cd.agency_id
		';

		self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		return $count;
	}

	private static function _mapSorting($param)
	{
		$map = array(
			'alpha' => 'cd.is_premium DESC, cd.club_name ASC',
			'random' => 'cd.is_premium DESC, RAND()',
			'last_modified' => 'cd.is_premium DESC, cd.last_modified DESC',
			'premium' => 'cd.is_premium DESC',
			'by-city' => 'ct.title_en ASC',
			'by-country' => 'cr.title_en ASC',
			'most-viewed' => 'cd.hit_count DESC',
			'close-to-me' => 'distance ASC',
			'last-connection' => 'ulrt.refresh_date DESC',
			'newest' => 'cd.date_registered DESC',
		);

		$order = 'cd.club_name ASC';
		if ( isset($map[$param]) ) {
			$order = $map[$param];
		}
		elseif ( false !== strpos($param, 'FIELD') ) {
			$order = $param;
		}

		return $order;
	}

    /**
     * @param Cubix_Model_Item $agency
     * @return Cubix_Model_Item
     * @throws Exception
     */
	public function save($agency)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$agency_id = $client->call('Agencies.save', array( (array) $agency));
		$agency->setId($agency_id);
		
		return $agency;
	}

	public function getLocalById($agency_id)
	{
		$lng = Cubix_I18n::getLang();

		$sql = '
			SELECT 
				*, ct.title_' . $lng . ' AS city_title, cr.id AS country_id, cr.slug AS country_slug,
				cr.title_' . $lng . ' AS country_title, ct.slug AS city_slug, ct.id AS city_id, cd.viber, cd.whatsapp
			FROM club_directory cd
			LEFT JOIN cities ct ON ct.id = cd.city_id
			LEFT JOIN countries cr ON cr.id = cd.country_id
			WHERE cd.agency_id = ?
		';

		return parent::_fetchRow($sql, $agency_id);
	}

	public function getCountsById($agency_id)
	{
		return self::db()->fetchRow("SELECT escorts_count, verified_escorts_count FROM club_directory WHERE agency_id = ?", $agency_id);
	}
	
	public function getLogo($agency_id)
	{
		$agency = $this->_db->fetchRow('
			SELECT photo_hash as logo_hash , photo_ext as logo_ext FROM club_directory WHERE agency_id = ?
		', $agency_id);
		
		if ( ! $agency ) return null;
		
		$agency->application_id = Cubix_Application::getId();
		
		return new Model_AgencyItem($agency);		
	}

	public function getLogoUrl($logo_hash, $logo_ext, $thumb_size)
	{

		if ( $logo_hash ) {

			$images_model = new Cubix_Images();
			
			return $images_model->getMemberUrl(new Cubix_Images_Entry(array(
				'application_id' => APP_ED,
				'catalog_id' => 'agencies',
				'size' => $thumb_size,
				'hash' => $logo_hash,
				'ext' => $logo_ext
			)));
		}

		return NULL;
	}

    public function getSettings($escort_id, $settings)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $escort = $client->call('Agencies.getField', array($escort_id, $settings));
        return $escort;
    }
    public function updateField($escort_id, $field,$value)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $escort = $client->call('Agencies.updateField', array($escort_id, $field,$value));
        return $escort;
    }
}