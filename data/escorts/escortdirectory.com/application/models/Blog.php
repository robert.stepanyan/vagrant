<?php

class Model_Blog extends Cubix_Model
{
    public function getPosts($filter, $page, $per_page)
    {
        $sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				b.id, b.slug, b.title_en, ' . Cubix_I18n::getTblField('b.title') . ' AS title, b.short_post_en,
				' . Cubix_I18n::getTblField('b.short_post') . ' AS short_post, UNIX_TIMESTAMP(b.date) AS date, i.hash, i.ext
			FROM blog_posts b
			LEFT JOIN blog_images i ON i.post_id = b.id
			WHERE 1 
		';

        if (strlen($filter['category_id']))
        {
            $sql .= 'AND category_id = '.intval($filter['category_id']).'';
        }

        if (strlen($filter['search']))
        {
            $sql .= '
			     AND (
			     b.title_en LIKE "%' . addslashes($filter['search']) . '%"
			     OR b.short_post_en LIKE "%' . addslashes($filter['search']) . '%"
			     OR b.post_en LIKE "%' . addslashes($filter['search']) . '%"
			     OR ' . Cubix_I18n::getTblField('b.title') . ' LIKE "%' . addslashes($filter['search']) . '%"
			     OR ' . Cubix_I18n::getTblField('b.short_post') . ' LIKE "%' . addslashes($filter['search']) . '%"
			     OR ' . Cubix_I18n::getTblField('b.post') . ' LIKE "%' . addslashes($filter['search']) . '%"
			     )
			';
        }

        // if (strlen($filter['month']) && strlen($filter['year']))
        // {
        // 	$sql .= ' AND MONTH(b.date) = ' . $filter['month'] . ' AND YEAR(b.date) = ' . $filter['year'];
        // }

        $sql .= '
			GROUP BY b.id
			ORDER BY b.date DESC
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;

        $data = parent::getAdapter()->query($sql)->fetchAll();
        $count = parent::getAdapter()->fetchOne('SELECT FOUND_ROWS()');

        if ($data)
        {
            foreach ($data as $k => $v)
            {
                $photo = new Cubix_ImagesCommonItem(array('hash' => $v->hash, 'ext' => $v->ext, 'application_id' => Cubix_Application::getId()));

                $data[$k]->photo = $photo;
            }
        }

        $items = array(
            'data'	=> $data,
            'count'	=> $count
        );

        return $items;
    }

    public function getRandomPosts($ex_ids, $count, $category_id = false)
    {
        $where = '';
        if($category_id){
            $where .= ' AND category_id = '.intval($category_id).'';
        }
        $sql = '
			SELECT * FROM (SELECT
				b.id, b.slug, b.title_en, ' . Cubix_I18n::getTblField('b.title') . ' AS title, b.short_post_en,
				' . Cubix_I18n::getTblField('b.short_post') . ' AS short_post, UNIX_TIMESTAMP(b.date) AS date, i.hash, i.ext
			FROM blog_posts b
			LEFT JOIN blog_images i ON i.post_id = b.id
			WHERE b.id NOT IN (' . $ex_ids . ')'.$where.'
			GROUP BY b.id
			ORDER BY RAND()
			LIMIT ' . $count . ') t ORDER BY date DESC';

        $data = parent::getAdapter()->query($sql)->fetchAll();

        if ($data)
        {
            foreach ($data as $k => $v)
            {
                $photo = new Cubix_ImagesCommonItem(array('hash' => $v->hash, 'ext' => $v->ext, 'application_id' => Cubix_Application::getId()));

                $data[$k]->photo = $photo;
            }
        }

        return $data;
    }

    public function getPost($id, $slug)
    {

        $data = parent::getAdapter()->query('
			SELECT
				b.id, b.title_en, ' . Cubix_I18n::getTblField('b.title') . ' AS title, b.post_en, b.slug, b.category_id,
				' . Cubix_I18n::getTblField('b.post') . ' AS post, UNIX_TIMESTAMP(b.date) AS date, count(c.id) as comment_count
				FROM blog_posts b
				LEFT JOIN blog_comments c ON c.post_id = b.id
				WHERE b.id = ? AND b.slug = ?
		', array($id, $slug))->fetch();


        $photos = parent::getAdapter()->query('SELECT hash, ext FROM blog_images WHERE post_id = ? ORDER BY image_id', $id)->fetchAll();

        foreach ($photos as $k => $photo)
        {
            $photos[$k] = new Cubix_ImagesCommonItem(array('hash' => $photo->hash, 'ext' => $photo->ext, 'application_id' => Cubix_Application::getId()));
        }

        return array('data' => $data, 'photos' => $photos);
    }

    public function getComments($post_id, $page, $per_page, $per_page_replies)
    {
        $sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				id, post_id, comment, name, UNIX_TIMESTAMP(date) AS date
			FROM blog_comments
			WHERE post_id = ? AND is_reply = 0 
		';

        $sql .= '
			ORDER BY date DESC
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;

        $data = parent::getAdapter()->query($sql, array($post_id))->fetchAll();
        $count = parent::getAdapter()->fetchOne('SELECT FOUND_ROWS()');

        if ($data)
        {
            foreach ($data as $k => $v)
            {
                $data[$k]->replies = $this->getReplies($post_id, $v->id, 1, $per_page_replies);
            }
        }

        $items = array(
            'data'	=> $data,
            'count'	=> $count
        );

        return $items;
    }

    public function addComment($data)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call('Blog.addComment', array($data));
    }

    public function getPostIdByCommentId($comment_id)
    {
        return parent::getAdapter()->fetchOne('SELECT post_id FROM blog_comments WHERE id = ?', $comment_id);
    }

    public function getReplies($post_id, $comment_id, $page, $per_page)
    {
        $sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				id, post_id, comment, name, UNIX_TIMESTAMP(date) AS date, is_reply, by_admin
			FROM blog_comments
			WHERE post_id = ? AND is_reply = ? 
		';

        $sql .= '
			ORDER BY date DESC
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;

        $data = parent::getAdapter()->query($sql, array($post_id, $comment_id))->fetchAll();
        $count = parent::getAdapter()->fetchOne('SELECT FOUND_ROWS()');

        $items = array(
            'data'	=> $data,
            'count'	=> $count
        );

        return $items;
    }

    public function updateViewCount($id)
    {
        $this->getAdapter()->query('UPDATE blog_posts SET view_count = view_count + 1 WHERE id = ?', array($id));

        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $client->call('Blog.updateViewCount', array($id));
    }

    public function getTopPostsForSlideshow($limit = 10)
    {
        $sql = '
			SELECT b.id, b.slug, b.title_en, ' . Cubix_I18n::getTblField('b.title') . ' AS title, b.hash, b.ext, UNIX_TIMESTAMP(b.date) AS date, count(c.id) as comment_count,
			' . Cubix_I18n::getTblField('b.short_post') . ' AS short_post, b.short_post_en AS short_post_en
			FROM blog_posts b
			LEFT JOIN blog_comments c ON c.post_id = b.id
			WHERE slider = 1
			group by b.id
			ORDER BY date DESC
			LIMIT ' . $limit . '
		';

        $data = parent::getAdapter()->query($sql)->fetchAll();
        if ($data)
        {
            foreach ($data as $k => $v)
            {
                $photo = new Cubix_ImagesCommonItem(array('hash' => $v->hash, 'ext' => $v->ext, 'application_id' => Cubix_Application::getId()));

                $data[$k]->photo = $photo;
            }
        }

        return $data;
    }

    public function getLatestPost()
    {
        $sql = '
			SELECT b.id, b.slug, b.title_en, ' . Cubix_I18n::getTblField('b.title') . ' AS title, b.hash, b.ext, UNIX_TIMESTAMP(b.date) AS date, count(c.id) as comment_count,
			' . Cubix_I18n::getTblField('b.short_post') . ' AS short_post, b.short_post_en as short_post_en
			FROM blog_posts b
			LEFT JOIN blog_comments c ON c.post_id = b.id
			group by b.id
			ORDER BY id DESC
			LIMIT 10
		';

        $data = parent::getAdapter()->query($sql)->fetch();
        if ($data)
        {
            $photo = new Cubix_ImagesCommonItem(array('hash' => $data->hash, 'ext' => $data->ext, 'application_id' => Cubix_Application::getId()));

            $data->photo = $photo;

        }

        return $data;
    }

    public function getSpecial()
    {
        $sql = '
			SELECT b.id, b.slug, b.title_en, ' . Cubix_I18n::getTblField('b.title') . ' AS title, b.hash, b.ext,
			' . Cubix_I18n::getTblField('b.short_post') . ' AS short_post
			FROM blog_posts b
			WHERE special = 1
			ORDER BY date DESC
			LIMIT 4
		';

        $data = parent::getAdapter()->query($sql)->fetchAll();
        if ($data)
        {
            foreach ($data as $k => $v)
            {
                $photo = new Cubix_ImagesCommonItem(array('hash' => $v->hash, 'ext' => $v->ext, 'application_id' => Cubix_Application::getId()));

                $data[$k]->photo = $photo;
            }
        }

        return $data;
    }

    public function getCategories()
    {

        $sql = '
			SELECT id, slug, title_en, ' . Cubix_I18n::getTblField('title') . ' AS title, layout
			FROM blog_category
			WHERE removed != 1 AND status = 1 
			ORDER BY position ASC
		';

        $data = parent::getAdapter()->query($sql)->fetchAll();

        if ($data)
        {
            foreach ($data as $k => $v)
            {
                $res = $this->getCategoryPosts($v->id,3);
                $data[$k]->posts = $res['data'];
            }
        }

        return $data;
    }

    public function getCategory($id)
    {

        $sql = '
			SELECT id, slug, title_en, ' . Cubix_I18n::getTblField('title') . ' AS title, layout
			FROM blog_category
			WHERE removed != 1 AND status = 1 AND id = ?
		';

        $data = parent::getAdapter()->query($sql,array($id))->fetchAll();

        if(!$data)return false;

        $cat = $data[0];

        return $cat;
    }

    public function getDateDetails($limit = 9)
    {
        $sql = '
			SELECT DISTINCT MONTH( date ) as b_month, YEAR( date ) as b_year
			FROM blog_posts
			ORDER BY b_year DESC, b_month DESC
			LIMIT ' . $limit . '
			';

        $data  = parent::getAdapter()->query($sql)->fetchAll();
        $count = parent::getAdapter()->fetchOne('SELECT FOUND_ROWS()');
        if (!$data) return false;

        return array(
            'data'  => $data,
            'count' => $count
        );
    }

    public function archive($month, $year)
    {
        $sql = "
			SELECT SQL_CALC_FOUND_ROWS 
				b.id, b.slug, b.title_en, " . Cubix_I18n::getTblField('b.title') . " AS title, b.short_post_en,
				" . Cubix_I18n::getTblField('b.short_post') . " AS short_post, UNIX_TIMESTAMP(b.date) AS date, i.hash, i.ext
			FROM blog_posts b
			LEFT JOIN blog_images i ON i.post_id = b.id
			WHERE MONTH(b.date) = $month AND YEAR(b.date) = $year
		";

        $sql  .= "
			GROUP BY b.id
			ORDER BY b.date DESC
            ";
        $data = parent::getAdapter()->query($sql)->fetchAll();

        if ($data) {
            foreach ($data as $k => $v) {
                $photo = new Cubix_ImagesCommonItem(array('hash' => $v->hash, 'ext' => $v->ext, 'application_id' => Cubix_Application::getId()));

                $data[$k]->photo = $photo;
            }
        }
        return $data;
    }

    public function getCategoryPosts($categoryId,$perPage = false, $page = false){

        if( ! $page ) $page = 1;

        $sql = '
			SELECT SQL_CALC_FOUND_ROWS b.id, b.slug, b.title_en, ' . Cubix_I18n::getTblField('b.title') . ' AS title, b.hash, b.ext, UNIX_TIMESTAMP(b.date) AS date, count(c.id) as comment_count,
			' . Cubix_I18n::getTblField('b.short_post') . ' AS short_post, 	b.short_post_en AS short_post_en
			FROM blog_posts b
			LEFT JOIN blog_comments c ON c.post_id = b.id
			WHERE category_id = ?
			group by b.id
			ORDER BY date DESC
		';

        if($perPage && $page){
            $sql .= ' LIMIT ' . ($page - 1) * $perPage . ', ' . $perPage;
        }elseif($perPage){
            $sql .= ' LIMIT ' . $perPage;
        }

        $posts = parent::getAdapter()->query($sql,array($categoryId))->fetchAll();
        $count = parent::getAdapter()->fetchOne('SELECT FOUND_ROWS()');

        if($posts){
            foreach ($posts as $k => $v){
                $photo = new Cubix_ImagesCommonItem(array('hash' => $v->hash, 'ext' => $v->ext, 'application_id' => Cubix_Application::getId()));

                $posts[$k]->photo = $photo;
            }
        }

        $response = array(
            'data'	=> $posts,
            'count'	=> $count
        );

        return $response;
    }
}
