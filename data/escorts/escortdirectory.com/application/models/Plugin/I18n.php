<?php
class Model_Plugin_I18n extends Zend_Controller_Plugin_Abstract{
	public function __construct()
	{
		
	}

	public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
	{
		$user = Model_Users::getCurrent();

		if ( ! is_null($user) ) {
            $action = $this->getRequest()->getActionName();
            //$data = Model_Users::getLastRefreshTime($user);

            /*$limit = 60 * 60 * 24; // one day
            if (!in_array($action, Zend_Registry::get('AVOID_AUTH_CHECKING_ACTIONS')) && $data->diff > $limit) {
                Model_Users::setCurrent(NULL);
            }else{
                Model_Users::extendSession();
            }*/

            if (!in_array($action, Zend_Registry::get('AUTOMATIC_ACTIONS'))) {
                Model_Users::setLastRefreshTime($user);
            }
        }

		//$this->rememberMeSignIn();
	}

	public function rememberMeSignIn()
	{
		$cookie_name = 'signin_remember_' . Cubix_Application::getId();

		if ( isset($_COOKIE[$cookie_name]) ) {
			$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
			$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
			$key = '_-_SignInRememberMe_-_' . Cubix_Application::getId();

			$crypt_login_data = $_COOKIE[$cookie_name];
			$crypt_login_data = base64_decode($crypt_login_data);
			$login_data = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypt_login_data, MCRYPT_MODE_ECB, $iv);

			$login_data = @unserialize($login_data);
			
			try {
				if ( is_array($login_data) ) {

					$model = new Model_Users();

					if ( ! $user = $model->getByUsernamePassword($login_data['username'], $login_data['password']) ) {
						$this->view->errors['username'] = 'Wrong username/password combination';
						return;
					}
					//print_r($user);die;
					if ( STATUS_ACTIVE != $user->status ) {
						$this->view->errors['username'] = 'Your account is not active yet';
						return;
					}

					Zend_Session::regenerateId();
					$user->sign_hash = md5(rand(100000, 999999) * microtime(true));
					Model_Users::setCurrent($user);

					/*set client ID*/
					Model_Reviews::createCookieClientID($user->id);
					/**/


					/*if ( ! Models_Auth::getAuth()->hasIdentity() ) {
						Models_Auth::auth($login_data['username'], $login_data['password'], Estate_Application::ID);
					}*/
				}
			}
			catch (Exception $e) {

			}
		}
	}
	
	public function preDispatch(Zend_Controller_Request_Abstract $request)
    {

  		//   	if ( ! isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $request->getActionName() != 'js-init-exact' && $request->getActionName() != 'js-init' ) {
		// 	if ( isset($_COOKIE['ec_count']) ) {
		// 		$count = ($_COOKIE['ec_count'] < 3) ? $_COOKIE['ec_count'] + 1 : 1;				
		// 		setcookie("ec_count", $count, 0, "/");
		// 	} else {
		// 		setcookie("ec_count", 1, 0, "/");
		// 	}
		// }

		/*$config = Zend_Registry::get('system_config');

		if ($request->getModuleName() != 'api' )
		{
			if ( $request->getControllerName() != 'private' && $request->getActionName() != 'activate' )
			{
				if ( $config['showSplash'] )
				{
					if ( ( ! isset($_COOKIE['18']) || ! $_COOKIE['18'] ) &&
						($request->getControllerName() != 'index' && $request->getActionName() != 'splash') &&
						($request->getControllerName() != 'error' && $request->getActionName() != 'error') )
					{

						$then = $_SERVER['REQUEST_URI'];
						if ( $_SERVER['REQUEST_URI'] == '' )
							$then = '/';

						header('HTTP/1.1 301 Moved Permanently');
						header('Location: /splash?then=' . urlencode($then));
						die;
					}
				}
				elseif ( ($request->getControllerName() == 'index' && $request->getActionName() == 'splash') )
				{
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: /');
					die;
				}
			}
		}*/

		/*if ( $_SERVER['HTTP_HOST'] != 'www.6annonce.com' && 
				($request->getControllerName() != 'redirect' ||
				($request->getControllerName() != 'escorts' && $request->getActionName() != 'profile')) &&
				($request->getModuleName() != 'api') ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: http://www.6annonce.com' . $_SERVER['REQUEST_URI']);
			exit;
		}*/
		
		//1 - disable chat
		//2 - node chat
		if ( ! $request->getParam('chatVersion') ) {
			$request->setParam('chatVersion', 2);
		}
    }

	public function routeShutdown(Zend_Controller_Request_Abstract $request)
	{

		if ( ! isset($_COOKIE['last_visit_date']) ) {
			$time = time();
            setcookie('last_visit_date', $time, [
                'expires' => time() + (1 * 365 * 24 * 60 * 60),
                'path' => '/',
                'domain' => APP_DOMAIN,
                'samesite' => 'Strict',
            ]);

			$_COOKIE['last_visit_date'] = $time;
        } else {
			if ( (time() - $_COOKIE['last_visit_date']) / 3600 > 12 ) {
				$time = time();
                setcookie('last_visit_date', $time, [
                    'expires' => time() + (1 * 365 * 24 * 60 * 60),
                    'path' => '/',
                    'domain' => APP_DOMAIN,
                    'samesite' => 'Strict',
                ]);
				$_COOKIE['last_visit_date'] = $time;
			}
		}


		$lang_id = $request->lang_id;
		
		//https://sceonteam.atlassian.net/browse/EDIR-459
		if(isset($request->p_city_id) && $request->p_city_id == 191){
			$default_lang_id = 'fr';
            $_COOKIE['ln'] = 'fr';
		}elseif( isset($request->p_city_id) ){
            $is_germany = Model_Cities::getCountryIdById($request->p_city_id);

            if ($is_germany->country_id == 24)
            {
                $default_lang_id = 'de';
                $_COOKIE['ln'] = 'de';
            }
		}else{
			$default_lang_id = 'en';
		}

		$cache = Zend_Registry::get('cache');
		$cache_key =  'application_get_all_langs_' . Cubix_Application::getId();
		if ( ! $lngs = $cache->load($cache_key) ) {
			$lngs = Cubix_I18n::getLangs();
			$cache->save($lngs, $cache_key, array());
		}
				
		if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !IS_MOBILE_DEVICE){
			
			/*if ( ! is_null($lang_id) && $lang_id == $default_lang_id && ! $request->isPost() ) {
				setcookie('ln', $default_lang_id, time() + 31536000, '/', APP_DOMAIN);
				$_COOKIE['ln'] = $default_lang_id;
				$uri = trim($_SERVER['REQUEST_URI'], '/');
				$uri = explode('/', $uri);
				array_shift($uri);
				$uri = implode('/', $uri);
				header('Location: /' . $uri);
				die;
			}*/
			
			if($lang_id) {//&& $_COOKIE['ln'] != $lang_id

				if (in_array($lang_id, $lngs) ) {
                    setcookie('ln', $lang_id, [
                        'expires' => time() + 31536000,
                        'path' => '/',
                        'domain' => APP_DOMAIN,
                        'samesite' => 'Strict',
                    ]);
					$_COOKIE['ln'] = $lang_id;
					$default_lang_id = $lang_id;
					$uri = trim($_SERVER['REQUEST_URI'], '/');
					$uri = explode('/', $uri);

					

					if($request->getControllerName() == 'escorts' && in_array($request->getActionName(), array('profile-v2', 'index')) ) {
						if($default_lang_id == 'en' && end($uri) == 'en') {
							array_pop($uri);
							$uri = implode('/', $uri);
							//header('HTTP/1.1 301 Moved Permanently');
							header('Location: /' . $uri . '/');
							die;
						}
					} else {

						array_shift($uri);
						$uri = implode('/', $uri);
						//header('HTTP/1.1 301 Moved Permanently');
						header('Location: /' . $uri);
						die;
					}
				}
			}
			elseif(isset($_COOKIE['ln']) && $_COOKIE['ln'] ) {
				if ( ! in_array($_COOKIE['ln'], $lngs) ) {
                    setcookie('ln', '', [
                        'expires' => time() - 3600,
                        'path' => '/',
                        'domain' => APP_DOMAIN,
                        'samesite' => 'Strict',
                    ]);
					$_COOKIE['ln'] = null;
				}
				else{
					$default_lang_id = $_COOKIE['ln'];

					if($request->getControllerName() == 'escorts' && $request->getActionName() == 'profile-v2' && $default_lang_id != 'en') {
                        $uri = trim($_SERVER['REQUEST_URI'], '/');
                        $uri = parse_url($uri);
                        if ($uri['query']){
                            $location = rtrim($uri['path'], '/') . '/' . $default_lang_id . '?' . $uri['query'];
                        }
                        else{
                            $location = rtrim($uri['path'], '/') . '/' . $default_lang_id;
                        }

                        header('Location: /' . $location);
                        die;
					}
				}
				/*elseif($_COOKIE['ln'] != "en" && !$request->isPost()) {
					$uri = trim($_SERVER['REQUEST_URI'], '/');
					$uri = explode('/', $uri);
					array_unshift($uri, $_COOKIE['ln']);
					$uri = implode('/', $uri);
					header('Location: '.APP_HTTP.'://'.APP_DOMAIN.'/'.$uri);
					die;
				}*/
			}
			else{

				$geo_location = Cubix_Geoip::getClientLocation();
				if($geo_location){

					$geo_lang = strtolower($geo_location['country_iso']);
					$geo_lang = in_array($geo_lang, array('gb','us')) ? 'en' : $geo_lang;
					if ( in_array($geo_lang, $lngs) ) {
						/*$uri = trim($_SERVER['REQUEST_URI'], '/');
						$uri = explode('/', $uri);
						array_unshift($uri, $geo_lang);
						$uri = implode('/', $uri);
						header('Location: '.APP_HTTP.'://'.APP_DOMAIN.'/'.$uri);*/
						$default_lang_id = $geo_lang;
					}
				}

                setcookie('ln', $default_lang_id, [
                    'expires' => time() + 31536000,
                    'path' => '/',
                    'domain' => APP_DOMAIN,
                    'samesite' => 'Strict',
                ]);
				$_COOKIE['ln'] = $default_lang_id;
			}
		}
		else{

			if (in_array($lang_id, $lngs) ) {
				$default_lang_id = $lang_id;
			}
		}
						
		$request->setParam('lang_id', $default_lang_id);
			
		Cubix_I18n::init();

		require 'Cubix/defines.php';

		$defines = Zend_Registry::get('defines');
		$this->customize_defines($defines, $DEFINITIONS);

		Zend_Registry::set('definitions', $DEFINITIONS);
	}

    private function customize_defines(&$old, &$new)
    {
        $new['language_options'] = $this->order_langs($old['language_options']);

        Zend_Registry::set('defines', $old);
    }

    private function order_langs(&$langs)
    {
        asort($langs);
    }
}
