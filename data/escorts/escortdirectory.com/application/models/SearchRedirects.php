<?php

class Model_SearchRedirects extends Cubix_Model
{
    public function __construct()
    {
        $this->cache = Zend_Registry::get('cache');
    }

    public function getByQuery(String $query) : ?String
    {
        $sql = "SELECT redirect_url FROM search_redirects WHERE `query` = '$query' LIMIT 1";
        return $this->db()->fetchOne($sql);
    }

    public function getListingBanners($page, $country, $city = null, $extraSuperDuperPremiumBannersWooHoo = [], $bannersCount = 15, $exclude = array())
    {
        $overAll = $this->getOverallCount();
        $superBanners = '-1';
        $needToFetch = $bannersCount;

        if( !empty($extraSuperDuperPremiumBannersWooHoo)) {
            $superBanners = implode(',', array_merge($exclude, $extraSuperDuperPremiumBannersWooHoo));
        }else{
            $superBanners = implode(',', $exclude);
        }

        // Collecting Fixed Banners
        // ---------------------------------
        $location = [];
        if (!empty($country)) $location [] = " frozen_country_id = " . $country;
        if (!empty($city)) $location [] = " ( frozen_city_id = " . $city . " OR frozen_city_id = 0 )";
        if (count($location)) {
            $location = " AND (" . implode(' AND ', $location) . ")";
        } else {
            $location = '';
        }

        if($location == '') {
            $fixedRows = [];
        } else {
            $sql = "SELECT * from ixs_banners WHERE zone_id IN ( 'x323139', 'x36' ) AND type = 'fixed' AND banner_id NOT IN ($superBanners) $location ORDER BY frozen_position ASC LIMIT 15";
            $fixedRows = $this->getAdapter()->fetchAll($sql);
        }
        // ---------------------------------

        $needToFetch -= count($fixedRows);

        // Collecting Banners that are in every single page
        // -------------------------------------
        if (!empty($superBanners)) {
            $sql = "SELECT * from ixs_banners WHERE banner_id in ($superBanners)";
            $superBannerRowsUnordered = $this->getAdapter()->fetchAll($sql);

            // Order them in the way it was defined in array
            // ---------------------------------------------
            $superBannerRows = [];
            foreach($superBannerRowsUnordered as $row) {
                $index = array_search($row->banner_id, $extraSuperDuperPremiumBannersWooHoo);
                $superBannerRows[$index] = $row;
            }
            ksort($superBannerRows);
            // ---------------------------------------------

        }else{
            $superBannerRows = [];
        }
        // -------------------------------------

        $needToFetch -= count($superBannerRows);

        // Collecting Rotating Banners
        // -----------------------------------
        $sql = "SELECT * from ixs_banners WHERE zone_id IN ( 'x323139', 'x36' ) AND type = 'rotating' AND banner_id NOT IN ($superBanners) ORDER BY RAND() LIMIT $needToFetch";
        $rotatingRows = $this->getAdapter()->fetchAll($sql);
        // -----------------------------------

        // Fixed Banners are going to be visible only on the first page
        // ---------------------------------------
        if ($page > 1) {
            $result = array_merge($superBannerRows, $rotatingRows);
        } else {
            $result = array_merge($fixedRows, $superBannerRows, $rotatingRows);
        }
        // ---------------------------------------

        return $result;
    }

    public function insert($images, $zone)
    {

        $sql = "INSERT INTO ixs_banners ( `id`, `ixs_id`, `zone_id`, `zone_rotate`, `zone_rotation`,
										 `banner_id`, `filename`, `target`,`title` , `link`, `cmp_id`,
										 `user_id`, `size`, `md5_hash`)";
        $sql_value = 'VALUES ';

        foreach ($images as $image) {

            $image_arr = (array)$image;
            $image_arr['zone_id'] = $zone;

            $image_data[0] = (isset($image_arr['id']) ? $image_arr['id'] : '');
            $image_data[1] = (isset($image_arr['zone_id']) ? $image_arr['zone_id'] : '');
            $image_data[2] = (isset($image_arr['zone_rotate']) ? $image_arr['zone_rotate'] : '');
            $image_data[3] = (isset($image_arr['zone_rotation']) ? $image_arr['zone_rotation'] : '');
            $image_data[4] = (isset($image_arr['banner_id']) ? $image_arr['banner_id'] : '');
            $image_data[5] = (isset($image_arr['filename']) ? $image_arr['filename'] : '');
            $image_data[6] = (isset($image_arr['target']) ? $image_arr['target'] : '');
            $image_data[7] = (isset($image_arr['title']) ? $image_arr['title'] : 'notitle');
            $image_data[8] = (isset($image_arr['link']) ? $image_arr['link'] : '');
            $image_data[9] = (isset($image_arr['cmp_id']) ? $image_arr['cmp_id'] : '');
            $image_data[10] = (isset($image_arr['user_id']) ? $image_arr['user_id'] : '');
            $image_data[11] = (isset($image_arr['size']) ? $image_arr['size'] : '');
            $image_data[12] = (isset($image_arr['md5_hash']) ? $image_arr['md5_hash'] : '');

            $sql_value .= '(null,';
            //$last_key = end(array_keys($image));

            foreach ($image_data as $key => $value) {
                $sql_value .= '"' . $value . '"';
                if ($key < 12) {
                    $sql_value .= ',';
                }
            }
            $sql_value .= '),';

            unset($image_data[$key]);
        }

        $sql .= $sql_value;


        try {
            $this->getAdapter()->query(rtrim($sql, ','));
            return TRUE;
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function swapTempTable()
    {
        $this->getAdapter()->query("DROP TABLE IF EXISTS `ixs_banners`");
        $this->getAdapter()->query("RENAME TABLE `ixs_banners_tmp` TO `ixs_banners`");
    }

    public function fill(Array $seeds, $temp = true)
    {
        $table = $temp ? 'ixs_banners_tmp' : 'ixs_banners';
        $sql = "INSERT INTO $table ( `id`, `type`, `frozen_position`, `frozen_country_id`, `frozen_city_id`,
										 `ixs_id`, `zone_id`, `zone_rotate`,`zone_rotation` , `banner_id`, `filename`,
										 `target`, `title`, `link`, `cmp_id`, `user_id`, `size`, `md5_hash`) VALUES ";
        foreach ($seeds as $seed) {
            $sql .= '(';
            foreach ($seed as $key => $value) {
                $sql .= '"' . $value . '",';
            }
            $sql = substr($sql, 0, -1); // Remove last comma
            $sql .= '),';
        }
        $sql = substr($sql, 0, -1); // Remove last comma

        try {
            return $this->getAdapter()->query($sql);
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function createTmpTable()
    {
        $this->getAdapter()->query("DROP TABLE IF EXISTS `ixs_banners_tmp`");

        $sql = "
        CREATE TABLE `ixs_banners_tmp`  (
            `id` int(11) NOT NULL AUTO_INCREMENT,
          `type` enum('rotating','fixed') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'rotating',
          `frozen_position` int(3) NULL DEFAULT NULL,
          `frozen_country_id` int(11) NULL DEFAULT NULL,
          `frozen_city_id` int(11) NULL DEFAULT NULL,
          `ixs_id` int(11) NULL DEFAULT NULL,
          `zone_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `zone_rotate` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `zone_rotation` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `banner_id` int(11) NULL DEFAULT NULL,
          `filename` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `target` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `link` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `cmp_id` int(11) NULL DEFAULT NULL,
          `user_id` int(11) NULL DEFAULT NULL,
          `size` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `md5_hash` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
          `is_follow` int(11) NULL DEFAULT 0,
          `sort` int(11) NULL DEFAULT 0,
          PRIMARY KEY (`id`) USING BTREE,
          UNIQUE INDEX `unique_banner_id`(`banner_id`) USING BTREE
        ) ENGINE = InnoDB AUTO_INCREMENT = 2637 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;";

        return $this->getAdapter()->query($sql);
    }

    static public function get($zone_id = null, $user_id = null, $size = null)
    {
        $where = array(
            'ixs.zone_id = ?' => $zone_id,
            'ixs.user_id = ?' => $user_id,
            'ixs.size = ?' => $size
        );

        $where = self::getWhereClause($where, true);
        $sql = '
			SELECT *
			FROM ixs_banners ixs
			' . (!is_null($where) ? 'WHERE ' . $where : '');

        $result = parent::_fetchAll($sql);
        return $result;
    }

    public static function get_right_sidebar_banners($zone_id = null, $rand_count = false)
    {

        if (isset($zone_id)) {
            $model_ixs = new Model_ixsBanners();
            $banners = $model_ixs->get($zone_id);

            if ($rand_count) {
                $random_banners = array();
                $random_els = array_rand($banners, $rand_count);
                if ($rand_count == 1) {
                    $random_banners[] = $banners[$random_els];
                    return $random_banners;
                } else {
                    foreach ($random_els as $random_el) {
                        $random_banners[] = $banners[$random_el];
                    }
                    $banners = $random_banners;
                }

            }

            return $banners;

        }

        return "";
    }

    public function truncate()
    {
        $sql = 'truncate table ixs_banners';
        if ($this->getAdapter()->query($sql)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function copyImages($imgs)
    {
        $copycount = 0;
        foreach ($imgs as $img) {

            if (!file_exists('./images/ixsGovazd/' . $img['filename'])) {

                $sourceurl = self::HOST . 'uploads/' . $img['filename'];

                $result = @copy($sourceurl, './images/ixsGovazd/' . $img['filename']);
                if (!$result) {
                    return 'image copy error';
                }
            }else{

            }

            $copycount++;
        }
        return $copycount;
    }
}
