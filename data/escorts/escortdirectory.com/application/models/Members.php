<?php

class Model_Members extends Cubix_Model
{
    /**
     * @param Cubix_Model_Item $member
     * @return Cubix_Model_Item
     * @throws Exception
     */
	public function save($member)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$member_id = $client->call('Members.saveMember', array( (array) $member));
		$member->setId($member_id);

		return $member;
	}
	
	public function getEscortAlerts($user_id, $escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.getEscortAlerts', array($user_id, $escort_id));
	}
	
	public function memberAlertsSave($user_id, $escort_id, $events, $extra = null)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.alertsSave', array($user_id, $escort_id, $events, $extra));
	}
	
	public function getAlerts($user_id)
	{
		//$client = new Cubix_Api_XmlRpc_Client();
		//return $client->call('Members.getAlerts', array($user_id));
		$client = Cubix_Api::getInstance();
		return $client->call('getMemberAlerts', array($user_id));
	}

	public function getMembersInfoByUsername($username)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.getMembersInfoByUsername', array($username));
	}

	public function getMembersDataByUserId($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Users.getById', array($user_id));
	}
	
	public function getMemberStats($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('members.getMemberStats', array($user_id));
	}
	
	public function getAdditionalInfo($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Members.getAdditionalInfo', array($user_id));
	}

	public function addToWatchedEscorts($sess_id, $escort_id, $user_id = null)
	{
		$this->getAdapter()->insert('watched_escorts', array(
			'session_id' => $sess_id,
			'escort_id' => $escort_id,
			'user_id' => $user_id,
			'date' => new Zend_Db_Expr('NOW()')
		));
	}

	public function getFromWatchedEscorts($sess_id, $escort_id)
	{
		return $this->getAdapter()->fetchOne('SELECT TRUE FROM watched_escorts WHERE session_id = ? AND escort_id = ?', array($sess_id, $escort_id));
	}

	public function updateToWatchedEscorts($sess_id, $escort_id)
	{
		$this->getAdapter()->query('UPDATE watched_escorts SET date = ' . new Zend_Db_Expr('NOW()') . ' WHERE session_id = ? AND escort_id = ?', array($sess_id, $escort_id));
	}

	public function updateToWatchedEscortsForUser($sess_id, $escort_id, $user_id)
	{
		$this->getAdapter()->query('UPDATE watched_escorts SET date = ' . new Zend_Db_Expr('NOW()') . ' WHERE session_id = ? AND escort_id = ? AND user_id = ?', array($sess_id, $escort_id, $user_id));
	}

	public function getFromWatchedEscortsForUser($sess_id, $escort_id, $user_id, $date)
	{
		return $this->getAdapter()->fetchOne('SELECT TRUE FROM watched_escorts WHERE session_id = ? AND escort_id = ? AND user_id = ? AND DATE(date) = ?', array($sess_id, $escort_id, $user_id, $date));
	}

    public function getFromWatchedEscortsForListing($escort_ids, $watch_type, $user_id = null, $sess_id = null)
    {
        if (!$user_id)
        {
            $res = $this->getAdapter()->query('
				SELECT escort_id FROM watched_escorts WHERE session_id = ? AND escort_id IN (' . $escort_ids . ') GROUP BY escort_id
			', array($sess_id))->fetchAll();
        }
        else
        {
            switch ($watch_type)
            {
                case WATCH_TYPE_PER_SESSION:
                    $res = $this->getAdapter()->query('
						SELECT escort_id FROM watched_escorts WHERE session_id = ? AND escort_id IN (' . $escort_ids . ') GROUP BY escort_id
					', array($sess_id))->fetchAll();
                    break;
                case WATCH_TYPE_PER_DAY:
                    $res = $this->getAdapter()->query('
						SELECT escort_id
						FROM watched_escorts
						WHERE user_id = ? AND escort_id IN (' . $escort_ids . ') AND DATE(date) = ?
						GROUP BY escort_id
					', array($user_id, date('Y-m-d')))->fetchAll();
                    break;
                case WATCH_TYPE_PER_LAST_3_DAYS:
                    $res = $this->getAdapter()->query('
						SELECT escort_id
						FROM watched_escorts
						WHERE user_id = ? AND escort_id IN (' . $escort_ids . ') AND DATE(date) > DATE(DATE_ADD("' . date('Y-m-d') . '", INTERVAL - 3 DAY))
						GROUP BY escort_id
					', array($user_id))->fetchAll();
                    break;
                case WATCH_TYPE_PER_LAST_7_DAYS:
                    $res = $this->getAdapter()->query('
						SELECT escort_id
						FROM watched_escorts
						WHERE user_id = ? AND escort_id IN (' . $escort_ids . ') AND DATE(date) > DATE(DATE_ADD("' . date('Y-m-d') . '", INTERVAL - 7 DAY))
						GROUP BY escort_id
					', array($user_id))->fetchAll();
                    break;
                case WATCH_TYPE_PER_ALL_TIME:
                    $res = $this->getAdapter()->query('
						SELECT escort_id
						FROM watched_escorts
						WHERE user_id = ? AND escort_id IN (' . $escort_ids . ')
						GROUP BY escort_id
					', array($user_id))->fetchAll();
                    break;
            }
        }

        return $res;
    }

	public function updateWatchedType($user_id, $type)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$client->call('Members.updateWatchedType', array($user_id, $type));
	}

	public function getReviewsInfo($user_id)
	{
		$countSql = "
			SELECT COUNT(id)
			FROM reviews
			WHERE user_id = ? AND status = ? AND is_deleted = 0
		";

		$count = parent::getAdapter()->fetchOne($countSql, array($user_id, REVIEW_APPROVED));

		$sql = "
			SELECT `creation_date` FROM reviews WHERE user_id = ? AND status = ? AND is_deleted = 0 ORDER BY `creation_date` DESC LIMIT 1
		";

		$last = parent::getAdapter()->fetchOne($sql, array($user_id, REVIEW_APPROVED));

		return array('count' => $count, 'last' => $last);
	}

	public function getCommentsInfo($user_id)
	{
		$countSql = "
			SELECT COUNT(id)
			FROM comments
			WHERE user_id = ? AND is_reply_to = 0
		";

		$count = parent::getAdapter()->fetchOne($countSql, array($user_id));

		$sql = "
			SELECT `time` FROM comments WHERE user_id = ? AND is_reply_to = 0 ORDER BY `time` DESC LIMIT 1
		";

		$last = parent::getAdapter()->fetchOne($sql, array($user_id));

		return array('count' => $count, 'last' => $last);
	}
	
	public function getLogoUrl($logo_hash, $logo_ext, $member_id, $thumb_size)
	{

		if ( $logo_hash ) {

			$images_model = new Cubix_Images();
			
			return $images_model->getMemberUrl(new Cubix_Images_Entry(array(
				'application_id' => APP_ED,
				'catalog_id' => 'members',
				'size' => $thumb_size,
				'hash' => $logo_hash,
				'ext' => $logo_ext
			)), $member_id );
		}

		return NULL;
	}
	
	public function addLogo($member_id, $hash, $ext)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$client->call('Members.addLogo', array($member_id, $hash, $ext));
	}		
	
	public function removeLogo($member_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$return = $client->call('Members.removeLogo', array($member_id));
		
	}		
}
