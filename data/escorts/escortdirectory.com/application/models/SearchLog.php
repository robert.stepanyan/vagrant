<?php

class Model_SearchLog extends Cubix_Model
{
    /**
     * Inserts new value into table or increments count of last inserted
     * if the query matches
     * @param array $data
     * @return void
     * @throws null
     */
    public function add(Array $data): void
    {
        try {
            $client = Cubix_Api_XmlRpc_Client::getInstance();
            $client->call('SearchLog.add', [$data]);
        } catch (\Exception $e) {

        }
    }
    public function updateCount(Array $data): void
    {
        try {
            $client = Cubix_Api_XmlRpc_Client::getInstance();
            $res = $client->call('SearchLog.updatecount', array($data) );
        } catch (\Exception $e) {

        }
    }
}
