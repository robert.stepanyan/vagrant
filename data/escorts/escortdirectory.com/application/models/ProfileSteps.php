<?php

class Model_ProfileSteps extends Cubix_Model
{
    protected $_table = 'profile_steps';

    public function update_where($data)
    {
        $db = Zend_Registry::get('db');

        $db->update($this->_table, array(
			'content' => $data['content'],
			'last_step' => $data['last_step']
        ), $db->quoteInto($data[type] . '_id = ?', $data['type'] == 'agency' ? $data['agency_id'] : $data['id']));
    }

    public function create($data)
    {
        return parent::getAdapter()->insert($this->_table, $data);
    }

    public function getLastStep($escortID)
    {
        $sql = "
			SELECT last_step FROM $this->_table
			WHERE escort_id = ?
		";

        $r = parent::getAdapter()->fetchOne($sql, $escortID);
        return $r === false ? null : $r;
    }

    public function get_where($data)
    {
        $where = " $data[type]_id = ? ";

        $sql = "
			SELECT * FROM $this->_table
			WHERE $where
		";

        $r = parent::getAdapter()->fetchRow($sql, $data['type'] == 'agency' ? $data['agency_id'] : $data['id']);
        return $r === false ? [] : (Array) $r;
    }


    public function remove_where($data)
    {
        $where = " $data[type]_id = ? ";

        $sql = "
            DELETE FROM $this->_table
            WHERE $where
		";

        $r = parent::getAdapter()->query($sql, $data['type'] == 'agency' ? $data['agency_id'] : $data['id']);
        return $r;
    }
}
