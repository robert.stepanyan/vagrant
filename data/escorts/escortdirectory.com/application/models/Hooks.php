<?php

class Model_Hooks
{
	/**
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected static $_db;
	
	/**
	 * @var Cubix_Api_XmlRpc_Client
	 */
	protected static $_client;
	
	public static function init()
	{
		self::$_client = Cubix_Api_XmlRpc_Client::getInstance();
		self::$_db = Zend_Registry::get('db');
	}

	public static function preUserSignIn($user_id)
	{
		$ip = Cubix_Geoip::getIP();
		
		self::$_client->call('Users.updateLastLoginDate', array($user_id, $ip, $_SERVER['HTTP_USER_AGENT'], session_id()));
	}
	
	public static function preUserSignUp(&$user)
	{
		//var_dump(self::$_client->call('Users.getRandomSalesPerson', array())); die;
		//$user->sales_user_id = self::$_client->call('Users.getRandomSalesPerson', array());
		$user->sales_user_id = 1; // Admin
	}
	
	public static function postUserSignUp($user)
	{
		$reg_type = '';
		if ( $user->reg_type == 'escort' ) {
			$reg_type = 'signup-independent';
		}
		else if ( $user->reg_type == 'agency' ) {
			$reg_type = 'signup-agency';
		}
		else if ( $user->reg_type == 'member' ) {
			$reg_type = 'signup-freemember';
		}
		else if ( $user->reg_type == 'vip-member' ) {
			$reg_type = 'signup-vipmember';
		}
		
		$client = new Cubix_Api_XmlRpc_Client();
		$client->call('Users.getLang', array($user->id, Cubix_I18n::getLang()));
		
		switch($user->reg_type){
		case 'escort':
			Cubix_Email::sendTemplate('mail_confirmation_escort_v1', $user->email, array(
			'activation_hash' => $user->activation_hash,
			'email' => $user->email,
			'username' => $user->username,
			'reg_type' => $reg_type
			));
		break;
		case 'agency':
			Cubix_Email::sendTemplate('mail_confirmation_agency_v1', $user->email, array(
				'activation_hash' => $user->activation_hash,
				'email' => $user->email,
				'username' => $user->username,
				'reg_type' => $reg_type
			));
		break;
		default:
			Cubix_Email::sendTemplate('mail_confirmation_member_v1', $user->email, array(
				'activation_hash' => $user->activation_hash,
				'email' => $user->email,
				'username' => $user->username,
				'reg_type' => $reg_type
			));
		break;
		}
		
	}
	
	public static function postUserActivate($user)
	{
		
		switch($user['user_type']){
			case 'escort':
				$esc_id = Cubix_Api::getInstance()->call('customSelect', array( 'id' , 'escorts', array('user_id' => $user['id']) ));
				Cubix_Email::sendTemplate('account_confirmed_escort_v1', $user['email'], array(
					'username' => $user['username'],
					'user_id' => $esc_id['id']
				));
			break;
			case 'agency':
				//$agency_id = Cubix_Api::getInstance()->call('customSelect', array( 'id' , 'agencies', array('user_id' => $user['id']) ));
				$back_user = Cubix_Api::getInstance()->call('customSelect', array( ['first_name', 'email', 'skype', 'phone'] , 'backend_users', array('id' => $user['sales_user_id']) ));
				
				$data = [
					'agency' => $user['username'],
					'username' => $user['username'],
					'sales_name' => $back_user['first_name'],
					'sales_email' => $back_user['email'],
					'skype' => $back_user['skype'],
					'whatsapp' => $back_user['phone'],
				];
				
				//USA sales
				if(in_array($user['sales_user_id'], [205,206])){
					Cubix_Email::sendTemplate('account_confirmed_agency_usa_v2', $user['email'], $data);
				}
				else{
					Cubix_Email::sendTemplate('account_confirmed_agency_v2', $user['email'], $data, $back_user['email'], $back_user['first_name'], $user['lang']);
				}
				
			break;
			
			default:
				Cubix_Email::sendTemplate('account_confirmed_member_v1', $user['email'], array(
					'username' => $user['username']
				));
			break;
		}
	}
	
	public static function preEscortSignUp(&$escort)
	{
		$escort->country_id = Cubix_Application::getById(Cubix_Application::getId())->country_id;
		
	}
	
	public static function postEscortSignUp($escort)
	{
		self::$_client->call('Escorts.setStatusBit', array($escort->getId(), array(
			Model_Escorts::ESCORT_STATUS_IS_NEW,
			Model_Escorts::ESCORT_STATUS_NO_PROFILE,
			Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS
		)));
	}
	
	public static function preAgencySignUp(&$agency)
	{
		$agency->country_id = Cubix_Application::getById(Cubix_Application::getId())->country_id;
	}
	
	public static function postAgencySignUp(&$agency)
	{
		
	}
}
