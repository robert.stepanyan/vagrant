<?php

class Model_Countries extends Cubix_Model {

	protected $_table = 'countries';

	public function getCountries($exclude = array()) {
		$where = '';
		if (!empty($exclude)) {
			$where = implode(',', $exclude);
			$where = ' WHERE id NOT IN (' . $where . ')';
		}
		$sql = '
			SELECT c.id,
				c.iso,
				c.slug,
				c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM countries c
			' . $where . '
			ORDER BY title
		';

		return parent::_fetchAll($sql);
	}

    public function getAll()
    {
        return parent::getAdapter()->query('SELECT * FROM countries')->fetchAll();
    }

    public function getCountriesContainEscorts()
    {
        $lng = Cubix_I18n::getLang();

        return parent::_fetchAll(
            'SELECT
                c.id,
                c.title_'.$lng.' AS title,
                count( es.id ) AS escortsCount 
            FROM
                countries c
                LEFT JOIN escorts es ON es.country_id = c.id 
            GROUP BY id'
        );
    }

    public static function getCountryByID($id)
    {
        $lng = Cubix_I18n::getLang();

        $sql = 'SELECT
            c.id as country_id,
            c.iso,
            c.slug as country_slug,
            c.title_' . $lng . ' as country_title
        FROM
            countries c
        WHERE
            c.id = ?';

        $result = self::db()->fetchAll($sql, $id);
        return isset($result[0]) ? $result[0] : null;
    }

	public function getBlockCountries( $escort_id ){
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		return $client->call('Escorts.getBlockCountries', array($escort_id));
	}

	public function getCountryIsoByIp( $ip ){
		if (!$ip)
        {
	        $ip = Cubix_Geoip::getIP();
        }

        $geoip = geoip_country_code_by_name($ip);

		return $geoip;
	}
	
	public function getPhonePrefixs(){
		$result =  $this->_db->fetchAll('
			SELECT id ,'.Cubix_I18n::getTblField('title') . ' as title, phone_prefix FROM countries_phone_code WHERE phone_prefix is not NULL order by phone_prefix DESC
			');
		return $result;
	}

	public function getPhoneCountries() {
		$sql = '
			SELECT cpc.id,
				cpc.' . Cubix_I18n::getTblField('title') . ' as title,
				cpc.phone_prefix,
				cpc.ndd_prefix
			FROM countries_phone_code cpc order by cpc.title_en
		';
		return parent::_fetchAll($sql);
	}

    public function getPhoneCountryIsoById($id) {
        $db = Zend_Registry::get('db');
        $sql = '
			SELECT cpc.iso
			FROM countries_phone_code cpc
			WHERE cpc.id = ?
		';
        return $db->fetchOne($sql,$id);
    }
    public function getPhoneCountryPrefixByIso($id) {
        $db = Zend_Registry::get('db');
        $sql = '
			SELECT cpc.phone_prefix
			FROM countries_phone_code cpc
			WHERE cpc.iso = ?
		';
        return $db->fetchOne($sql,$id);
    }

	public static function  getPhonePrefixById($id){
		$db = Zend_Registry::get('db');
		$sql = '
			SELECT cpc.phone_prefix
			FROM countries_phone_code cpc
			WHERE cpc.id = ?
		';
		return $db->fetchOne($sql,$id);
	}
	public static function  getPhoneNddPrefixById($id){
		$db = Zend_Registry::get('db');
		$sql = '
			SELECT cpc.ndd_prefix
			FROM countries_phone_code cpc
			WHERE cpc.id = ?
		';
		return $db->fetchOne($sql,$id);
	}

	public static function  getCountryIdByCityId($city_id){
		$db = Zend_Registry::get('db');
		$sql = '
			SELECT country_id
			FROM cities 
			WHERE id = ?
		';
		return $db->fetchOne($sql,$city_id);
	}

	public static function  getCountryByCityId($city_id){
		$db = Zend_Registry::get('db');
		$sql = '
			SELECT co.id, co.slug, co.' . Cubix_I18n::getTblField('title') . ' as title
			FROM cities c
			INNER JOIN countries co ON co.id = c.country_id
			WHERE c.id = ?
		';
		return $db->fetchRow($sql,$city_id);
	}
	
	public static function getContinentCountries() 
	{
		$db = Zend_Registry::get('db');
		
		$c_sql = '
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM continents c
			ORDER BY c.ordering
		';

		$continents = $db->fetchAll($c_sql);
		
		if ( count($continents) > 0 ) {
			foreach( $continents as $k => $continent ) {
				$sql = '
					SELECT c.id,
						c.iso,
						c.slug,
						c.' . Cubix_I18n::getTblField('title') . ' as title
					FROM countries c
					WHERE c.continent_id = ?
				';
				
				$continents[$k]->countries = $db->fetchAll($sql, $continent->id);
			}
		}
		
		return $continents; 
	}
	
	public static function getTravelContinentCountriesByEscort($escort_id) 
	{
		$db = Zend_Registry::get('db');
		
		$c_sql = '
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM continents c
			ORDER BY c.ordering
		';

		$continents = $db->fetchAll($c_sql);
		
		if ( count($continents) > 0 ) {
			foreach( $continents as $k => $continent ) {
				$sql = '
					SELECT c.id,
						c.iso,
						c.slug,
						c.' . Cubix_I18n::getTblField('title') . ' as title
					FROM countries c
					INNER JOIN escort_travel_countries etc ON etc.country_id = c.id
					WHERE c.continent_id = ? AND etc.escort_id = ?
				';
				
				$continents[$k]->countries = $db->fetchAll($sql, array($continent->id, $escort_id));
			}
		}
		
		return $continents; 
	}	
	
	public static function getTravelCountriesByEscort($escort_id) 
	{
		$db = Zend_Registry::get('db');
		
		$sql = '
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM countries c
			INNER JOIN escort_travel_continents etc ON etc.continent_id = c.continent_id
			WHERE etc.escort_id = ?
			ORDER BY c.title_en
		';

		$countries = $db->fetchAll($sql, $escort_id);
		
		
		return $countries;
	}

	public static function getContinentId($country_id)
	{
		$db = Zend_Registry::get('db');

		$sql = '
			SELECT continent_id
			FROM countries
			WHERE id = ?
		';
		$continent_id = $db->fetchOne($sql, $country_id);

		return $continent_id;
	}

    public static function getGeoCurrency()
    {
        $geoData = Cubix_Geoip::getClientLocation();
        $db = Zend_Registry::get('db');

        if (!isset($geoData['country_iso'])) return null;

        $countryRow = $db->fetchRow("SELECT id FROM countries WHERE iso = '{$geoData['country_iso']}' ");
        if (empty($countryRow)) return null;

        $currency = Model_Applications::getDefaultCurrencyTitle() ? : self::getCountryCurrency($countryRow->id);

        return $currency;
    }

    /*
	 * @param int $countryId
	 * @return string
	 * @author Eduard
     */
    public static function getCountryCurrency($countryId)
    {
         $redis = Zend_Registry::get('redis');
         $db = Zend_Registry::get('db');
         $cacheKey = 'currency:' . $countryId;

         // If currency was not found, probably redis is empty
         // so we need to fill it from db
         // -----------------------------------
         if (!$currency = $redis->get($cacheKey)) {

             $sql = 'SELECT currency, id FROM countries';
             $rows = $db->fetchAll($sql);
             foreach ($rows as $row) {
                 $redis->set('currency:' . $row->id, $row->currency);
             }

             $currency = $redis->get($cacheKey);
         }
        // -----------------------------------

        // $config = Zend_Registry::get('system_config');
        return empty($currency) ? Model_Applications::getDefaultCurrencyTitle() : $currency;
    }
	
	public function getByGeoIp()
	{
		$geoData = Cubix_Geoip::getClientLocation();
		
		if (!is_null($geoData))
		{			
			if (strlen($geoData['country_iso']))
			{
				$c_iso = strtolower($geoData['country_iso']);
				$ordering = 'e.is_vip DESC, eic.is_premium DESC, FIELD(cr.iso, "' . $c_iso . '") DESC, eic.ordering DESC';
				
				return array('ordering' => $ordering, 'country_iso' => $c_iso);
			}
		}
		else
			return null;
	}

    public static function getByIso($iso)
    {

        $sql = 'SELECT
            id as country_id
        FROM
            countries
        WHERE
            iso = ?';

        return self::db()->fetchOne($sql, $iso);
    }
}