<?php

class Model_AgencyItem extends Cubix_Model_Item
{
    public function __construct($rowData = array())
    {
        parent::__construct($rowData);

        $this->setThrowExceptions(false);
    }

	public function hasEscort($escort_id)
	{
		return Cubix_Api::getInstance()->call('hasAgencyEscort', array($this->getId(), $escort_id));
	}

	public function getEscorts($params = array(), &$escorts_count = null, $ex_escort_id = null)
	{
		if ( ! isset($params['filter']) ) {
			$params['filter'] = array();
		}
		$params['filter']['e.agency_id = ?'] = $this->id;
		unset($params['limit']);
		
		$client = new Cubix_Api_XmlRpc_Client();
		$escorts = $client->call('Agencies.getActiveEscorts', array($this->id));
		return $escorts;
	}
	public function getHaveMessageEscorts($params = array(), &$escorts_count = null, $ex_escort_id = null)
	{
		if ( ! isset($params['filter']) ) {
			$params['filter'] = array();
		}
		$params['filter']['e.agency_id = ?'] = $this->id;
		unset($params['limit']);

		$client = new Cubix_Api_XmlRpc_Client();
		$escorts = $client->call('Agencies.getHaveMessageEscorts', array($this->id));
		return $escorts;
	}

	public static function hasAddedAnyEscort($agency_id) {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call('Agencies.hasAddedAnyEscort', array($agency_id));
    }

    public static function hasFemaleEscort($agency_id) {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call('Agencies.hasFemaleEscort', array($agency_id));
    }

    public function getEscortsPerPage($page = 1, $per_page = 10,$status = null,$is_susp = false, $showname = '',  $order = null, $showName = null){
        
        $client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Agencies.getEscortsByStatus', array($this->id,$page,$per_page,$status,$is_susp,$showname,$order,$showName));
    }

    public function getAgencyActiveEscorts($agency_id){
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call('Agencies.getAgenciesActiveEscorts', array($agency_id));
    }
	
	public function getWorkTimes()
	{
		$ret = array('is_open' => $this->is_open, 'working_times' => $this->working_times);
		return $ret;
	}
	
	public function getLogoUrl($from_server = false, $thumb_size = 'thumb_cropper_ed')
	{
		if ( $this->logo_hash ) {
			$images_model = new Cubix_Images();

			if ( $from_server ) {
				return $images_model->getServerUrl(new Cubix_Images_Entry(array(
					'application_id' => $this->application_id,
					'catalog_id' => 'agencies',
					'size' => $thumb_size,
					'hash' => $this->logo_hash,
					'ext' => $this->logo_ext
				)));
			}
			else {
				return $images_model->getUrl(new Cubix_Images_Entry(array(
					'application_id' => $this->application_id,
					'catalog_id' => 'agencies',
					'size' => $thumb_size,
					'hash' => $this->logo_hash, 
					'ext' => $this->logo_ext
				)));
			}
		}
		
		return NULL;
	}

	public function getLogoUrlV2($logo_hash, $logo_ext, $thumb_size = null)
	{
		if ( $logo_hash ) {

			$images_model = new Cubix_Images();

			return $images_model->getUrl(new Cubix_Images_Entry(array(
				'application_id' => APP_ED,
				'catalog_id' => 'agencies',
				'size' => $thumb_size,
				'hash' => $logo_hash,
				'ext' => $logo_ext
			)));
		}

		return NULL;
	}

	public function getLinkedAgencies()
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$result = $client->call('Agencies.getLinkedAgencies', array($this->id));
		
		if ( false === $result ) {
			throw new Exception('Gettink linked agencies failed');
		}
		
		return $result;
	}

	public function isOnline()
	{
		$sql = "
			SELECT 
				IF (ulrt.refresh_date >= DATE_ADD(NOW(), INTERVAL -5 MINUTE), 1, 0) AS is_login, cd.allow_show_online
			FROM club_directory cd
			LEFT JOIN users_last_refresh_time ulrt ON ulrt.user_id = cd.user_id
			WHERE cd.agency_id = ?
		";

		$data = $this->_adapter->fetchRow($sql, $this->getAgencyId());

		$is_online = false;

		if ( $data->is_login && $data->allow_show_online ) {
			$is_online = true;			
		}

		return $is_online;
	}
	
	public function search_agancy_escorts($search_word , $agancyid =  0 ){
		
		if($agancyid == 0){$agancyid = $this->id;}
		
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Agencies.getEscortsByShownameOrid', array($search_word, $agancyid, 1) ) ;
		
	}
}
