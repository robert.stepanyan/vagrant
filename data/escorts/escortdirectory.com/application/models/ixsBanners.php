<?php

class Model_ixsBanners extends Cubix_Model
{
    //const HOST = 'https://adzz.io/';
    const HOST = 'https://ixspublic.com/';

    /**
     * @var Redis
     */
    private $cache;

    public function __construct()
    {
        $this->cache = Zend_Registry::get('redis');
    }

    public function getOverallCount()
    {
        $cacheKey = Cubix_Application::getId() . ':listing_banners_overall';
        if (!$overall = $this->cache->get($cacheKey)) {
            $overall = $this->getAdapter()->fetchOne('SELECT count(id) FROM ixs_banners');
            $this->cache->set($cacheKey, $overall);
        }

        return $overall;
    }

    public function getListingBanners($page, $country, $city = null, $extraSuperDuperPremiumBannersWooHoo = [], $bannersCount = 15, $exclude = array(), $region = null)
    {
        $overAll = $this->getOverallCount();
        $superBanners = '-1';
        $needToFetch = $bannersCount;

        if( !empty($extraSuperDuperPremiumBannersWooHoo)) {
            $superBanners = implode(',', array_merge($exclude, $extraSuperDuperPremiumBannersWooHoo));
        }else{
            $superBanners = implode(',', $exclude);
        }

        // Collecting Fixed Banners
        // ---------------------------------
        $location = [];
//        if (!empty($country)) $location [] = " frozen_country_id = " . $country;
//        if (!empty($city)) $location [] = " ( frozen_city_id = " . $city . " OR frozen_city_id = 0 )";


        if (!empty($city)) {
            $location [] = "( frozen_city_id = " . $city. " OR frozen_city_id = 0 )";
        }

        if (!empty($region)) {
            $location [] = " frozen_region_id = " . $region;
        }

        if(!empty($country)) {
            if ($country == 68) {
                $location[] = " frozen_country_id = " . $country;
            }
            else {
                $location[] = "( frozen_country_id = " . $country. " OR frozen_country_id = 9999 )"  /* EDIR-1018, creating constant 9999, for all countries except US */;
            }
        }
        if (count($location)) {
            $location = " AND (" . implode(' AND ', $location) . ")";
        } else {
            $location = '';
        }

        if($location == '') {
            $fixedRows = [];
        }
// else {
//           if ($country == 68){
//               $sql = "SELECT * from ixs_banners WHERE banner_id NOT IN ( 5598 ) AND zone_id IN ( 'x323139', 'x36' ) ORDER BY frozen_position ASC LIMIT 15";
//           }
            else{
                $sql = "SELECT * from ixs_banners WHERE zone_id IN ( 'x323139', 'x36' ) AND type = 'fixed' AND banner_id NOT IN ($superBanners) $location ORDER BY frozen_position ASC LIMIT 15";
            }
            $fixedRows = $this->getAdapter()->fetchAll($sql);
       // }
        // ---------------------------------

        $needToFetch -= count($fixedRows);

        // Collecting Banners that are in every single page
        // -------------------------------------
        if (!empty($superBanners)) {
            $sql = "SELECT * from ixs_banners WHERE banner_id in ($superBanners)";
            $superBannerRowsUnordered = $this->getAdapter()->fetchAll($sql);

            // Order them in the way it was defined in array
            // ---------------------------------------------
            $superBannerRows = [];
            foreach($superBannerRowsUnordered as $row) {
                $index = array_search($row->banner_id, $extraSuperDuperPremiumBannersWooHoo);
                $superBannerRows[$index] = $row;
            }
            ksort($superBannerRows);
            // ---------------------------------------------

        }else{
            $superBannerRows = [];
        }
        // -------------------------------------

        $needToFetch -= count($superBannerRows);

        // Collecting Rotating Banners
        // -----------------------------------

        // new jira EDIR-89
        $hardcoded_banner_prio = '';
        if( isset($country) && in_array($country, array(23, 33, 66, 4, 67) ) ){
            $hardcoded_banner_prio = 'field(banner_id,4288) desc,';
        }

        $noMoreEnergyForThisHardcodedBanners = 'field(banner_id,4394) desc,';
        if( $needToFetch < 0 )
        {
            $needToFetch = 0;
        }

        if ($country == 68) {
            $additionalPartOfSql = '`frozen_country_id` != 9999 AND '; /* EDIR-1018, creating constant 9999, for all countries except US */
        }
        else {
            $additionalPartOfSql = '';
        }
        $sqlForHighPriorityBanners = "SELECT * from ixs_banners WHERE zone_id IN ( 'x323139', 'x36' ) AND ".$additionalPartOfSql ." `low_priority` <> 1 AND `type` = 'rotating' AND banner_id NOT IN ($superBanners) ORDER BY " . $hardcoded_banner_prio . " " . $noMoreEnergyForThisHardcodedBanners . " RAND() LIMIT $needToFetch";

        $rotatingRowsHighPriority = $this->getAdapter()->fetchAll($sqlForHighPriorityBanners);


        $sqlForLowPriorityBanners = "SELECT * from ixs_banners WHERE zone_id IN ( 'x323139', 'x36' ) AND `low_priority` = 1 AND ".$additionalPartOfSql ."  `type` = 'rotating' AND banner_id NOT IN ($superBanners) ORDER BY " . $hardcoded_banner_prio . " " . $noMoreEnergyForThisHardcodedBanners . " RAND() LIMIT $needToFetch";

        $rotatingRowsLowPriority = $this->getAdapter()->fetchAll($sqlForLowPriorityBanners);

        $rotatingRows = array_merge($rotatingRowsHighPriority, $rotatingRowsLowPriority);
        // -----------------------------------

        // Fixed Banners are going to be visible only on the first page
        // ---------------------------------------
        if ($page > 1) {
            $result = array_merge($superBannerRows, $rotatingRows);
        } else {
            $result = array_merge($fixedRows, $superBannerRows, $rotatingRows);
        }
        // ---------------------------------------

        return $result;
    }

    public function insert($images, $zone)
    {

        $sql = "INSERT INTO ixs_banners ( `id`, `ixs_id`, `zone_id`, `zone_rotate`, `zone_rotation`,
										 `banner_id`, `filename`, `target`,`title` , `link`, `cmp_id`,
										 `user_id`, `size`, `md5_hash`)";
        $sql_value = 'VALUES ';

        foreach ($images as $image) {

            $image_arr = (array)$image;
            $image_arr['zone_id'] = $zone;

            $image_data[0] = (isset($image_arr['id']) ? $image_arr['id'] : '');
            $image_data[1] = (isset($image_arr['zone_id']) ? $image_arr['zone_id'] : '');
            $image_data[2] = (isset($image_arr['zone_rotate']) ? $image_arr['zone_rotate'] : '');
            $image_data[3] = (isset($image_arr['zone_rotation']) ? $image_arr['zone_rotation'] : '');
            $image_data[4] = (isset($image_arr['banner_id']) ? $image_arr['banner_id'] : '');
            $image_data[5] = (isset($image_arr['filename']) ? $image_arr['filename'] : '');
            $image_data[6] = (isset($image_arr['target']) ? $image_arr['target'] : '');
            $image_data[7] = (isset($image_arr['title']) ? $image_arr['title'] : 'notitle');
            $image_data[8] = (isset($image_arr['link']) ? $image_arr['link'] : '');
            $image_data[9] = (isset($image_arr['cmp_id']) ? $image_arr['cmp_id'] : '');
            $image_data[10] = (isset($image_arr['user_id']) ? $image_arr['user_id'] : '');
            $image_data[11] = (isset($image_arr['size']) ? $image_arr['size'] : '');
            $image_data[12] = (isset($image_arr['md5_hash']) ? $image_arr['md5_hash'] : '');

            $sql_value .= '(null,';
            //$last_key = end(array_keys($image));

            foreach ($image_data as $key => $value) {
                $sql_value .= '"' . $value . '"';
                if ($key < 12) {
                    $sql_value .= ',';
                }
            }
            $sql_value .= '),';

            unset($image_data[$key]);
        }

        $sql .= $sql_value;


        try {
            $this->getAdapter()->query(rtrim($sql, ','));
            return TRUE;
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function fill(Array $seeds)
    {
        $this->getAdapter()->query("DELETE FROM `ixs_banners`");

        $table = 'ixs_banners';
        $sql = "INSERT INTO $table ( `id`, `type`, `frozen_position`, `frozen_country_id`, `frozen_city_id`,
										 `ixs_id`, `zone_id`, `zone_rotate`,`zone_rotation` , `banner_id`, `filename`,
										 `target`, `title`, `link`, `cmp_id`, `user_id`, `size`, `md5_hash`, `is_follow`, `sort`, `low_priority`, `frozen_region_id`) VALUES ";
        foreach ($seeds as $seed) {
            $sql .= '(';
            foreach ($seed as $key => $value) {
                $sql .= '"' . $value . '",';
            }
            $sql = substr($sql, 0, -1); // Remove last comma
            $sql .= '),';
        }
        $sql = substr($sql, 0, -1); // Remove last comma

        try {
            return $this->getAdapter()->query($sql);
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    static public function get($zone_id = null, $user_id = null, $size = null)
    {
        $where = array(
            'ixs.zone_id = ?' => $zone_id,
            'ixs.user_id = ?' => $user_id,
            'ixs.size = ?' => $size
        );

        $where = self::getWhereClause($where, true);
        $sql = '
			SELECT *
			FROM ixs_banners ixs
			' . (!is_null($where) ? 'WHERE ' . $where : '');

        $result = parent::_fetchAll($sql);
        return $result;
    }

    public static function get_right_sidebar_banners($zone_id = null, $rand_count = false)
    {

        if (isset($zone_id)) {
            $model_ixs = new Model_ixsBanners();
            $banners = $model_ixs->get($zone_id);

            if ($rand_count) {
                $random_banners = array();
                $random_els = array_rand($banners, $rand_count);
                if ($rand_count == 1) {
                    $random_banners[] = $banners[$random_els];
                    return $random_banners;
                } else {
                    foreach ($random_els as $random_el) {
                        $random_banners[] = $banners[$random_el];
                    }
                    $banners = $random_banners;
                }

            }

            return $banners;

        }

        return "";
    }

    public function truncate()
    {
        $sql = 'truncate table ixs_banners';
        if ($this->getAdapter()->query($sql)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function copyImages($imgs)
    {
        $copycount = 0;
        foreach ($imgs as $img) {

            if (!file_exists('./images/ixsGovazd/' . $img['filename'])) {

                $sourceurl = self::HOST . 'uploads/' . $img['filename'];

                $result = @copy($sourceurl, './images/ixsGovazd/' . $img['filename']);
                if (!$result) {
                    return 'image copy error';
                }
            }else{

            }

            $copycount++;
        }
        return $copycount;
    }
}
