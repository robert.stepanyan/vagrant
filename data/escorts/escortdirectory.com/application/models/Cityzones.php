<?php

class Model_Cityzones extends Cubix_Model
{
    protected $_table = 'regions';

    public static function getZoneCityCountryByID($id)
    {
        $lng = Cubix_I18n::getLang();

        $sql = 'SELECT
                cz.id as zone_id,
                c.id as country_id,
                ct.id as city_id,
                
                ct.title_' . $lng . ' as city_title,
                cz.title_' . $lng . ' as zone_title,
                c.title_' . $lng . ' as country_title
                
            FROM cityzones cz
            LEFT JOIN cities ct on ct.id = cz.city_id
            LEFT JOIN countries c on c.id = ct.country_id
            WHERE cz.id = ?';

        $result = self::db()->fetchAll($sql, $id);
        return isset($result[0]) ? $result[0] : null;
    }

}
