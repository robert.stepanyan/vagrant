<?php

class Zend_View_Helper_GetPrice extends Zend_View_Helper_Abstract
{
	public function getPrice($rates)
	{
		$rate = false;
		$rateArr = array(
		    'original_rate'=>'',
		    'user_rate'=>'',
        );
		foreach ($rates[AVAILABLE_INCALL] as $r) {
			if($r->type != null){ continue; }
			if($r->availability == AVAILABLE_INCALL && $r->time_unit == TIME_HOURS && $r->time == 1){
				$rate = $r;
				break;
			}
		}
		if(!$rate){
			foreach ($rates[AVAILABLE_OUTCALL] as $r) {
				if($r->type != null){ continue; }
				if($r->availability == AVAILABLE_OUTCALL && $r->time_unit == TIME_HOURS && $r->time == 1){
					$rate = $r;
					break;
				}
			}
		}
		if(!$rate){
			foreach ($rates[AVAILABLE_INCALL] as $r) {
				if($r->type != null){ continue; }
				$rate = $r;
				break;
			}
		}
		if(!$rate){
			foreach ($rates[AVAILABLE_OUTCALL] as $r) {
				if($r->type != null){ continue; }
				$rate = $r;
				break;
			}
		}

		if($rate){
	        if(isset($_COOKIE['currency'])){
				$current_currency = $_COOKIE['currency'];
	        }else{
				$current_currency = "USD";
	        }

			$currencies = Model_Currencies::getAllAssoc();

			$user_currency_rate = Model_Currencies::getSelectedCurrencyRate($currencies[$rate->currency_id]);
			$selected_currency_rate = Model_Currencies::getSelectedCurrencyRate($current_currency);

			$DEFINITIONS = Zend_Registry::get('defines');
			$time_units = $DEFINITIONS['time_unit_options'];
			if($rate->time_unit == TIME_MINUTES && $rate->time == 60){
				$rate->time_unit = TIME_HOURS;
				$rate->time = 1;
			}
            $rateArr['original_rate'] = number_format($rate->price).' '.$rate->currency_title.' / '.$rate->time.' '.substr($time_units[$rate->time_unit],0,1);
			if($currencies[$rate->currency_id] != $current_currency){
				$converted_price = round(($rate->price / $user_currency_rate->rate) * $selected_currency_rate->rate);
				$price = "~".number_format($converted_price).' '.$current_currency;
			}else{
				$price = number_format($rate->price).' '.$rate->currency_title;
			}

			$rateArr['user_rate'] = $price.' / '.$rate->time.' '.substr($time_units[$rate->time_unit],0,1);
			return $rateArr;
		}

		return false;
	}
}
