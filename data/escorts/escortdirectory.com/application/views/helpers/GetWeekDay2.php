<?php

class Zend_View_Helper_GetWeekDay2
{
	public function getWeekDay2($day_index)
	{
		$week_day = '';

		switch($day_index)
		{
			case 1:
				$week_day = 'monday';
			break;
			case 2:
				$week_day = 'tuesday';	
			break;
			case 3:
				$week_day = 'wednesday';	
			break;
			case 4:
				$week_day = 'thursday';	
			break;
			case 5:
				$week_day = 'friday';	
			break;
			case 6:
				$week_day = 'saturday';
			break;
			case 7:
				$week_day = 'sunday';
			break;
		}
		
		return $week_day;
	}
}
