<?php

class Zend_View_Helper_LangUrl extends Zend_View_Helper_Abstract
{
	public function langUrl()
	{
		$url = $_SERVER['REQUEST_URI'];
		$url = explode('/', trim($url, '/'));
		array_unshift($url, '%s');
		$url = preg_replace('#\?ajax#', "", $url);
		$lang_url = '/' . implode('/', $url);
		$lang_url = rtrim($lang_url, '/') . '/';
		return $lang_url; 
	}
}
