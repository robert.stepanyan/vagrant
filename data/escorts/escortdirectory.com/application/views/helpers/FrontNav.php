<?php
class Zend_View_Helper_FrontNav extends Zend_View_Helper_Abstract
{
	public function frontNav($hide = array())
	{		
		$navs = array();
		$user_loged = Model_Users::getCurrent();
		$is_loged = (!$user_loged ? false : true );
         
		$navs['escorts'] = array( 'title' => 'Escorts', 'icon' => '',  'link' => '');

		$navs['escorts']['sub']['new_escorts'] =  array('title' => 'New Escorts', 'icon' => '',  
			'link' => $this->view->GetLink('escorts', array('nuove'), true) );

		$navs['escorts']['sub']['independent_Ladies'] = array( 'title' => 'Independent Ladies', 'icon' => '', 
			'link' => $this->view->GetLink('escorts', array('independents'), true)	);

		$navs['escorts']['sub']['agency_girls'] = array( 'title' => 'Agency girls', 'icon' => '', 
			'link' => $this->view->GetLink('escorts', array('agency'), true) );

		$navs['escorts']['sub']['men'] = array( 'title' => 'Men', 'icon' => '', 
			'link' => $this->view->GetLink('escorts', array('boys'), true) );

		$navs['escorts']['sub']['trans'] = array( 'title' => 'Trans', 'icon' => '', 
			'link' => $this->view->GetLink('escorts', array('trans'), true) );

		$navs['escorts']['sub']['city_tours'] = array( 'title' => 'City Tours', 'icon' => '', 
			'link' => $this->view->GetLink('escorts', array('tours'), true) );

		$navs['agencies'] = array( 'title' => 'Agencies', 'icon' => '', 
		 'link' => $this->view->GetLink('agencies'));


        if(!$is_loged){
            $navs['login_register'] = array( 'title' => 'Login / Register', 'icon' => '',
                'link' => $this->view->GetLink('signin'));
        }

        $navs['advertise_with_us'] = array( 'title' => 'Advertise', 'icon' => '',
            'link' => $this->view->GetLink('advertise'));
         $navs['videos'] = array( 'title' => 'Videos', 'icon' => '',  'is_new' => true,
            'link' => $this->view->GetLink('videos'));

        $navs['classifies_ads'] = array( 'title' => 'Classifies Ads', 'icon' => '',
            'link' => $this->view->GetLink('classified-ads-index') );

		$navs['updates'] = array( 'title' => 'Updates', 'icon' => '',  'link' => '', 'class' => '');

		$navs['updates']['sub']['reviews'] = array( 'title' => 'Reviews', 'icon' => '',  
			'link' => $this->view->GetLink('reviews') );

		$navs['updates']['sub']['comments'] = array( 'title' => 'Comments', 'icon' => '',  
			'link' => $this->view->GetLink('comments'));

		$navs['contact'] = array( 'title' => 'Contact', 'icon' => '',
		 'link' => $this->view->GetLink('contact-us') );

        $navs['termsAndConditions'] = array( 'title' => 'T&C', 'icon' => '',
            'link' => $this->view->GetLink('terms') );

        if($is_loged){
        	$navs['dashboard'] = array( 'title' => 'Dashboard', 'icon' => '',
                'link' => $this->view->GetLink('private'));
			$navs['log_out'] = array( 'title' => 'Log-out', 'icon' => '', 'link' => $this->view->GetLink('signout'));
		}

		if(is_array($hide)){
			foreach ($hide as $key) {
				unset($navs[$key]);
			}
		}else{
			unset($navs[$hide]);
		}
		

		return $navs;
	}

}