<?php

class Zend_View_Helper_BubbleTextsWidget extends Zend_View_Helper_Abstract
{
	public function bubbleTextsWidget($c, $page, $per_page, $country_id)
	{
        try {
            $client = Cubix_Api_XmlRpc_Client::getInstance();
            $bubbles = $client->call('Escorts.getBubbleTexts', array($page, $per_page, null, $c, $country_id));
        }
        catch ( Exception $e ) {
            $bubbles = array('texts' => array(), 'count' => 0);
        }

        foreach ( $bubbles['texts'] as &$text ) {
            $text = new Model_EscortV2Item($text);
        }
		
		$this->view->per_page = $per_page;
		$this->view->page = $page;
		$this->view->bubbles = $bubbles;

		return $this->view->render('widgets/bubble-texts.phtml');
	}
}
