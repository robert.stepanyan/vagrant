<?php

class Zend_View_Helper_VotingWidgetV2 extends Zend_View_Helper_Abstract
{
	public function votingWidgetV2($escort, $only_content = false)
	{
		$user = Model_Users::getCurrent();

		try {
			$client = Cubix_Api::getInstance();
			$result = $client->call('getEscortVotingData', array($escort->id));
		} catch ( Exception $e ) {
			$result = array();
		}

		$escort->voted_members = ($members = @unserialize($result['voted_members'])) ? $members : array();
		$escort->votes_count = $result['votes_count'];

		if ( 0 != $escort->votes_count ) {
			$escort->votes = $escort->votes_count;
		}
		else {
			$escort->votes = null;
		}
		
		$this->view->escort = $escort;

		if ( $user && ('member' != $user->user_type || in_array($user->id, $escort->voted_members)) ) {
			$this->view->already_voted = in_array($user->id, $escort->voted_members);
			if ( $only_content ) {
				return $this->view->render('widgets/voting-v2/voted.phtml');
			}
			else {
				$this->view->show_result = true;
			}
		}

		return $this->view->render('widgets/voting-v2/widget.phtml');
	}
}
