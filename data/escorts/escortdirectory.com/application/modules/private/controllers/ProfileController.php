<?php

class Private_ProfileController extends Zend_Controller_Action
{
	/**
	 * @var Model_Escort_Profile
	 */
	protected $profile;

	/**
	 * @var Model_EscortItem
	 */
	protected $escort;

	/**
	 * @var Zend_Session_Namespace
	 */
	protected $session;

	protected $steps = array();
	
	protected $_posted = false;

	protected $steps_order = [
		1 => 'biographyStep',
		2 => 'aboutMeStep',
		3 => 'workingCitiesStep',
		4 => 'servicesStep'
	];

	public function preDispatch()
	{
		if ( ! is_null($this->_getParam('ajax')) ) 
			$this->view->layout()->disableLayout();

	}

	public function init()
	{
		$this->_request->setParam('no_tidy', true);

		$anonym = array();

		$this->view->user = $this->user = Model_Users::getCurrent();
		
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {

			if($this->getRequest()->isXmlHttpRequest()){
				header("HTTP/1.1 403 Forbidden");
			}else{
				$this->_redirect($this->view->getLink('signin'));
			}

			return;
		}

		$this->view->addScriptPath($this->view->getScriptPath('private'));

		$this->step = $this->_getParam('step'); 
	
		$escort_id = $this->view->escort_id = intval($this->_getParam('escort'));
		if ( $this->user->isAgency() && $escort_id > 1 ) {
			$this->agency = $agency = $this->user->getAgency();
			if ( ! $agency->hasEscort($escort_id) ) {
				//return $this->_redirect($this->view->getLink('private-v2-escorts'));
			}

			$escorts = new Model_Escorts();
			$this->escort = $escorts->getById($escort_id);
		}
		elseif ( $this->user->isAgency() ) {

			$this->agency = $agency = $this->user->getAgency();
			$this->escort = new Model_EscortItem(array('id' => null));
		}
		else {
			$this->escort = $this->user->getEscort();
			if ( is_null($this->escort) ) $this->escort = new Model_EscortItem(array('id' => null));
		}

        if(isset($this->escort->is_suspicious) && $this->escort->is_suspicious && $this->_request->getActionName() != 'gallery'){
			$this->_redirect($this->view->getLink('private'));
			return;
		}


		$session_hash = '';
		if ( $this->_request->session_hash ) {
			$session_hash = $this->view->session_hash = $this->_request->session_hash;
		}
		else {
			$session_hash = $this->view->session_hash = md5(microtime() * time());
		}

		$this->session = new Zend_Session_Namespace('profile-data-' . $session_hash);
		
		if($this->user->isAgency()){
			$user_data = [
				'type' => 'agency',
				'agency_id' => $this->agency->id,
			];
		}else{
			$user_data = [
				'type' => 'escort',
				'id' => $this->escort->id,
			];
		}

		// Get Profile data and set up the 'Profile Steps'
		$this->profile = $this->view->profile = $this->escort->getProfile();
        $this->profile->setSession($this->session, $user_data);
		//

		// Determine the mode depending on if user has profile
		$this->mode = $this->view->mode = ((isset($this->escort->id) && $this->escort->id && $this->escort->hasProfile() && $this->step != 'finish') ? 'update' : 'create');

		if($this->user->isEscort()) {
			$is_profile_partially = (
                ($this->escort->status & Model_Escorts::ESCORT_STATUS_PARTIALLY_COMPLETE) ||
                ($this->escort->status & Model_Escorts::ESCORT_STATUS_NO_PROFILE) ||
                ($this->user->status == -1)
            );
			$this->mode = $this->view->mode = !$is_profile_partially ? 'update' : 'create';

			if($this->mode == 'create') {
                $lastStep = $this->profile->getLastStep($this->escort->id);

                if($lastStep && $lastStep > 4) {
                    $this->mode = $this->view->mode = 'update';
                    $this->profile->setMode(Model_Escort_Profile::MODE_REALTIME);
                }
            }
		}
		//

        if( ($this->user->isAgency() && 'create' == $this->mode) || $this->user->isEscort() ) {
            $this->profile->load(true);
        }

        // If we are in update mode, then we need to set the profile model
		// update in real-time mode instead of updating session
		if (  'update' == $this->mode ) {
			$this->profile->setMode(Model_Escort_Profile::MODE_REALTIME);
            $this->profile->load(true);
        }

		$this->defines = $this->view->defines = Zend_Registry::get('defines');

		$config = Zend_Registry::get('escorts_config');
        if ( 'create' == $this->mode ) {
            $steps = array('completeProfileStep','biographyStep', 'aboutMeStep', 'workingCitiesStep', 'servicesStep',
                           'workingTimesStep','contactInfoStep','galleryStep', 'finishStep');
        }else{
            $steps = array('biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times');

			if ($config['profile']['rates'])
				$steps[] = 'prices';

            $steps[] = 'contact-info';
            $steps[] = 'gallery';
        }
		
		$this->steps = $steps;
		$this->view->layout()->setLayout('main');

	}

    public function indexAction()
    {
        $config = Zend_Registry::get('escorts_config');
        $titles = array('biography', 'about_me', 'languages', 'working_cities', 'services', 'working_times');

        if ($config['profile']['rates'])
            $titles[] = 'prices';

        $titles[] = 'contact_info';
        $titles[] = 'gallery';
        $titles[] = 'finish';

        foreach ( $titles as $i => $key ) {
            $titles[$i] = __('pv2_tab_' . $key);
        }

        // If the mode is update, i.e. user already has a profile revision
        // we don't need the last step "Finish"
        if ( $this->mode == 'update' ) {
            unset($this->steps[count($this->steps) - 1]);
            unset($titles[count($titles) - 1]);
        }

        $this->view->steps = array_combine($this->steps, $titles);

        $this->_posted = $this->_request->isPost();

        $result = $this->_do();
        $then = $this->_getParam('then');

        // If the user clicked the update button, and the result is successfull
        // and we don't need to switch to another tab, just display
        // a user friendly message
        if ( $this->_posted && true === $result && ! strlen($then) ) {
            $this->view->status = 'The section "' . $this->view->steps[$this->view->step] . '" has been successfully updated!';
        }

        if ( false === $result || ! strlen($then) ) { return; }

        switch (true) {
            case ($then == ':next'):
                $i = array_search($this->_getParam('step'), $this->steps);
                if ( $i == count($this->steps) - 1 ) return;

                $step = $this->steps[$i + 1];

                break;
            case ($then == ':back'):
                $i = array_search($this->_getParam('step'), $this->steps);
                if ( $i == 0 ) return;

                $step = $this->steps[$i - 1];

                break;
            default:

                if ( ! in_array($then, $this->steps) ) {
                    return;
                }

                $step = $then;
        }

        $this->_setParam('step', $step);
        $this->_setParam('then', '');
        $this->_posted = false;

        $this->_do();
    }

    /*Completing Profile Steps*/
    public function biographyStepAction(){


        if($this->_getParam('step') == 'biographyStep' AND $this->_request->isPost()) { // Whenever post comes from Biography Step EXACTLY!

            $validator = new Cubix_Validator();
            $bio_data = $this->_validateBiographyStep($validator);

            $lang_validator = new Cubix_Validator();
            $lang_data = $this->_validateLanguages($validator);

            $this->view->data = $data = array_merge(
                (array) $bio_data,
                (array) $lang_data
            );

            $response = array(
                'success' => false
            );

            $this->profile->setAvailableApps($data);

            if (!$validator->isValid() || !$lang_validator->isValid()) {
                $status = array_merge(
                    (array)$validator->getStatus(),
                    (array)$lang_validator->getStatus()
                );

                $this->view->errors = $status['msgs'];
                $response['errors'] = $status['msgs'];

                $this->profile->last_step = $this->current_step;
                $this->profile->update($data);

            } else {

                if ($this->current_step == $this->profile->last_step) $this->profile->last_step++;
                elseif ($this->current_step > $this->profile->last_step) $this->profile->last_step = $this->current_step;

                $this->profile->update($data);

                $response['success'] = false;
                $response['data'] = $this->aboutMeStepAction();
                $response['last_step'] = $this->current_step;

                die(json_encode($response));
            }
        }else{

            $countryModel = new Model_Countries();
            $this->view->countries = $countryModel->getCountries();

            $this->view->phone_countries = $countryModel->getPhoneCountries();
            $this->view->data = array_merge(
                (array) $this->profile->getBiography(),
                (array) $this->profile->getWorkingCities(),
                (array) $this->profile->getServices(),
                (array) $this->profile->getLanguages(),
                (array) $this->profile->getContactInfo(),
                (array) $this->profile->getAvailableApps()
            );
            $response['success'] = true;

            if($this->user->isAgency()) {
                $this->view->escort_id = $escort_id = !empty($this->profile->escort_id) ? $this->profile->escort_id : null;

                if(is_null($escort_id)) {
                    $this->view->escort_id = $escort_id = $this->finishStepAction(true);
                    $this->profile->escort_id = $escort_id;
                    $this->profile->update($this->profile);
                }

            }else{
                $this->view->escort_id = $escort_id = $this->escort->id;
            }

            if ( isset($this->view->data['city_id']) && $this->view->data['city_id'] ) {
                $cities = new Cubix_Geography_Cities();
                $city = $cities->get($this->view->data['city_id']);
                $country_id = $city->country_id;

                $this->view->country_id = $country_id;
                $this->view->cities = $cities->ajaxGetAll(null, $country_id);
            }
        }

        $response['data'] = $this->view->render('profile/biography-step.phtml');
        $response['prevent_popup'] = TRUE;

        die(json_encode($response));
    }

    public function aboutMeStepAction(){

        $response['prevent_popup'] = TRUE;

        if($this->_getParam('then') && $this->_getParam('then') == 'back'){

            $response['success'] = true;
            unset($_POST['then']);
            $response['data'] = $this->biographyStepAction();

            die($response);
        }

        if($this->_getParam('step') == 'aboutMeStep') {

            if ($this->_request->isPost()) {

                $validator = new Cubix_Validator();
                $blackListModel = new Model_BlacklistedWords();
                $dataAboutMe = $this->_validateAboutMe($validator);
                $this->view->data = $data = $dataAboutMe;

                $dataAboutMe['about_has_bl'] = 0;

                $response = array(
                    'success' => false
                );

                if (!$validator->isValid()) {

                    $status = $validator->getStatus();

                    $this->view->errors = $status['msgs'];

                    $response['errors'] = $status['msgs'];

                    $this->profile->last_step = $this->current_step;
                    $this->profile->update($dataAboutMe);

                } else {

                    if($this->current_step == $this->profile->last_step) $this->profile->last_step++;
                    elseif ($this->current_step > $this->profile->last_step) $this->profile->last_step = $this->current_step;
                    $this->profile->update($dataAboutMe);
                    $this->profile->setComplete();

                    $response['success'] = true;
                    $response['data'] = $this->workingCitiesStepAction();
                    $response['last_step'] = $this->current_step;

                    die(json_encode($response));
                }
            }
        }else{
            $response['success'] = true;
            $this->profile->load();
            $this->view->data = $this->profile;

            if($this->user->isAgency()) {
                $this->view->escort_id = $escort_id = (!empty($this->profile->escort_id) && $this->profile->escort_id > 1) ? $this->profile->escort_id : null;

                if(is_null($escort_id)) {
                    $this->view->escort_id = $escort_id = $this->finishStepAction(true);
                    $this->profile->escort_id = $escort_id;
                    $this->profile->update($this->profile);
                }

            }else{
                $this->view->escort_id = $escort_id = $this->escort->id;
            }

            // Prepare Gallery photos
            // --------------------------------------------------
            $client = Cubix_Api_XmlRpc_Client::getInstance();
            $cur_gallery_id = $this->_getParam('gallery_id');
            $cur_gallery_id = is_null($cur_gallery_id) ? 0 : intval($cur_gallery_id);
            $this->view->cur_gallery_id = $cur_gallery_id;
            $galleries = $client->call('getPhotoGalleriesED', array($escort_id));

            foreach($galleries as $i => $gallery) {
                $galleries[$i]['photos'] = $this->_loadPhotos($gallery['fake_id']);
            }
            $this->view->galleries = $galleries;
            // --------------------------------------------------


        }

        $response['data'] = $this->view->render('profile/about-me-step.phtml');

        die(json_encode($response));
    }

    public function workingCitiesStepAction(){

        $response['prevent_popup'] = TRUE;
        $this->view->data = $data = array_merge(
            $this->profile->getWorkingCities(),
            $this->profile->getBiography()
        );

        $countyModel = new Model_Countries();
        $this->view->countries = $countyModel->getCountries();

        $this->defines = $this->view->defines = Zend_Registry::get('defines');

        if($this->_getParam('then') && $this->_getParam('then') == 'back'){
            unset($_POST['then']);
            $response['success'] = true;
            $response['data'] = $this->aboutMeStepAction();
            die($response);
        }

        if($this->_getParam('step') == 'workingCitiesStep') {

            if ($this->_request->isPost()) {

                $validator = new Cubix_Validator();
                $this->view->data = $data = $this->_validateWorkingCitiesStep($validator);

                $response = array(
                    'success' => false,
                    'prevent_popup' => true
                );

                if (!$validator->isValid()) {

                    $status = $validator->getStatus();
                    $response['errors'] = $status['msgs'];

                    $this->profile->last_step = $this->current_step;
                    $this->profile->update($data);


                } else {
                    if($this->current_step == $this->profile->last_step) $this->profile->last_step++;
                    elseif ($this->current_step > $this->profile->last_step) $this->profile->last_step = $this->current_step;
                    $this->profile->update($data);

                    $client = Cubix_Api_XmlRpc_Client::getInstance();

                    $results = $client->call('Escorts.updateTravelPlace', array($this->escort->id, $data['travel_place']));
                    unset($data['travel_place']);

                    $response['success'] = false;
                    $response['data'] = $this->servicesStepAction();
                    $response['finish'] = true;
                    $response['prevent_popup'] = false;

                    die(json_encode($response));
                }
            }
        }else{
            $response['success'] = true;
            $this->profile->load();
            $this->view->data = array_merge(
                (array) $this->profile,
                (array) $this->profile->getBiography(),
                (array) $this->profile->getServices()
            );
        }

        $response['data'] = $this->view->render('profile/working-cities-step.phtml');
        die(json_encode($response));
    }

    public function servicesStepAction(){

        $response['prevent_popup'] = TRUE;
        $didFinishSteps = $this->profile->last_step > 4;

        if( $didFinishSteps && !$this->user->isAgency() ) {
            $page = $this->view->render('profile/finish-step.phtml');
        }else {
            $page = $this->view->render('profile/services-step.phtml');
        }

        if($this->_getParam('then') && $this->_getParam('then') == 'back'){
            unset($_POST['then']);
            $response['success'] = true;
            $response['data'] = $this->workingCitiesStepAction();
            $response['last_step'] = $this->current_step;

            die($response);
        }

        if($this->_getParam('step') == 'servicesStep') {

            if ($this->_request->isPost()) {

                $validator = new Cubix_Validator();
                $this->view->data = $data = $this->_validateServicesStep($validator);
                $response = array(
                    'success' => false
                );

                $this->profile->update($data);

                if (!$validator->isValid()) {

                    $status = $validator->getStatus();
                    $response['errors'] = $status['msgs'];
                    $this->profile->update($data);
                } else {
                    $this->profile->last_step = 5;
                    $this->profile->update($data);

                    $response['success'] = true;
                    $response['data'] = $this->view->render('profile/finish-step.phtml');
                    $this->view->escort_id = $escort_id = $response['escort_id'] = $this->finishStepAction();

                    die(json_encode($response));
                }

            }
        }else{
            $response['success'] = true;
            $this->profile->load();
            $this->view->data = $this->profile;
        }

        $response['data'] = $page;
        die(json_encode($response));
    }

    public function completeProfileStepAction(){

        if($this->_request->isPost()){
            return $this->biographyStepAction();
        } else {

            if ($this->profile->last_step >= 5) {
                $response = [];
                $response['data'] = $this->view->render('profile/finish-step.phtml');
                die(json_encode($response));
            }

            if (isset($this->steps_order[$this->profile->last_step])) {

                if($this->profile->last_step <= 2 AND $this->profile->areStepsDone() == false) {
                    $this->profile->setPartially();
                }else if ($this->profile->last_step > 2){
                    $this->profile->setComplete();
                }

                return $this->{$this->steps_order[$this->profile->last_step] . 'Action'}();
            }
        }

        return $this->view->render('profile/complete-profile-step.phtml');
    }

    protected function _do()
    {
        $step = $this->_getParam('step');
        $this->current_step = array_search($step, $this->steps_order);

        if(!$this->current_step) $this->current_step = 0;
        if($this->mode == 'update') $this->current_step = 5;

        if($this->current_step <= 2 AND $this->profile->areStepsDone() == false) {
            $this->profile->setPartially();
        }else if ($this->current_step > 2){
            $this->profile->setComplete();
        }


        // User requested editing of profile without specifying
        // the step, i.e. if he/she clicks on profile icon in private are
        // load the profile from api into current session
        /*if ( ! in_array($step, $this->steps) && $step != 'galleryStep' && $this->mode != 'update' ) {
            $this->profile->load(true);
            $step = reset($this->steps);
        }*/

        $this->view->step = $step;
        $method = str_replace('-', ' ', $step);
        $method = ucwords($method);
        $method = str_replace(' ', '', $method);
        $method[0] = strtolower($method[0]);

        $this->_helper->viewRenderer->setScriptAction($step);
        /*if ( $method == 'gallery' ){
            if ( $this->mode == 'create' ) {
                $this->finishAction();
                $tid = (int)$this->view->escort_id;
                $this->_redirect($this->view->getLink('private-v2-profile', array('step' => 'gallery', 'escort' => $tid, 'show_success' => 1), true));
            }
        }*/

        die($this->{$method . 'Action'}());
    }
    /*End Of Profile Steps Logic*/

    public function sortTop10Action()
    {
        $ids = $this->_getParam('sort_id');

        if ( ! is_array($ids) || ! count($ids) ) {
            die;
        }

        $follow_model = new Model_Follow();
        $follow_model->sortTop10($this->user->id,$ids);
        die;
    }

    public function favoritesRemoveAction()
    {
        $this->view->layout()->disableLayout();

        $form = new Cubix_Form_Data($this->_request);
        $fields = array(
            'id' => 'int-nz'
        );

        $form->setFields($fields);
        $data = $form->getData();

        $model = new Model_Follow();
        $model->removeFavorites($this->user->id, $data['id']);
        $model->reorderingTop10($this->user->id);

        die(json_encode(array('success' => true)));
    }

    public function simpleAction(){
        
            $this->view->simple = true;

            $this->view->layout()->setLayout('simple-profile');
            $this->view->addScriptPath($this->view->getScriptPath('private'));

            $this->_helper->viewRenderer->setScriptAction("simple");

            

            $this->agency = $agency = $this->user->getAgency();
			$this->escort = new Model_EscortItem(array('id' => null));

            $this->_posted = $this->_request->isPost();
            $data = array();
            $errors = array();
            $is_error = false;
            $counter = 0;

            $this->view->step = 'biography';
			
            foreach($this->steps as $step){
                $method = str_replace('-', ' ', $step);
                $method = ucwords($method);
                $method = str_replace(' ', '', $method);
                $method[0] = strtolower($method[0]);
                
                if($step === 'finish' && $this->_posted && !$is_error){

                     $this->finishAction();
                     $this->view->step = 'finish';
                     //$this->_helper->viewRenderer->setScriptAction('finish');
					 $tid = (int)$this->view->escort_id;
					 $this->_redirect($this->view->getLink('private-v2-profile', array('step' => 'gallery', 'escort' => $tid, 'show_success' => 1), true));
                }elseif($step !== 'finish'){
					if ( $step === 'gallery' ){
						continue;
					}
                     $this->{$method . 'Action'}();
                }else{
                    continue;
                }
                
                $tmpData = $this->view->data;
                if($tmpData){
                    $data = array_merge($data,$tmpData);
                }

                $tmpErrors = $this->view->errors;
                if($tmpErrors){
                    $errors = array_merge($errors,$tmpErrors);
                    $is_error = true;
                }
                $counter++;
            }

            $this->view->data = $data;

            if(!$this->_posted){
                $this->view->data['phone_number'] = $agency['phone'];
                $this->view->data['email'] = $agency['email'];
                $this->view->data['website'] = $agency['web'];
            }

			$this->view->errors = $errors;			
	}


	protected function _validateBiographyStep($validator) {

		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'showname' => '',
			'type' => 'int-nz',
			'gender' => 'int-nz',
			'sex_availability' => '',
			'country_id' => 'int-nz',
			'city_id' => 'int-nz',
			'cities' => 'arr-int',
			'cityzones' => 'arr-int',
			'email' => '',
			'website' => 'notags|special',
			'viber' => 'int',
			'whatsapp' => 'int',
			'wechat' => 'int',
			'telegram' => 'int',
			'ssignal' => 'int',
			'phone_instr' => 'int-nz',
			'phone_instr_no_withheld' => 'int-nz',
			'phone_instr_other' => 'notags|special',
			'phone_prefix' => '',
			'phone_number' => '',
		));
		$data = $form->getData();

		$defines = Zend_Registry::get('defines');
		$data['showname'] = preg_replace('/(\s)+/','$1', trim($data['showname']));

		// Base City logic and more
        // ------------------------------
        if ( ! is_array($data['cities']) ) $data['cities'] = array();
        $cityIdArray = [];
        $data['cities'] = array_unique($data['cities']);
        $data['cities'] = array_slice($data['cities'], 0, 3);

        foreach ( $data['cities'] as $i => $city_id ) {
            $data['cities'][$i] = array('city_id' => $city_id);
            if(in_array($city_id, $cityIdArray)){
                unset($data['cities'][$i]);
            }
            $cityIdArray[] = $city_id;

            if ( ! $city_id ) {
                unset($data['cities'][$i]);
            }
        }
        $needToAddBaseCity = true;
        foreach ($data['cities'] as $id => $city){

            // If base city was the same as other city somehow
            if($data['city_id'] == $city['city_id']) {
                unset($data['cities'][$id]);
            }

            if ($city['city_id'] == $data['city_id'] || $city['city_id'] == $city) {
                $needToAddBaseCity = false;
                break;
            }
        }

        if ($needToAddBaseCity && $data['city_id']){
            $data['cities'][] = array('city_id'=>$data['city_id']);
        }

        // ------------------------------


        if ( ! strlen($data['showname']) ) {
			$validator->setError('showname', Cubix_I18n::translate('showname_required'));
		}
		elseif(mb_strlen($data['showname']) > 25 ){
				$validator->setError('showname', Cubix_I18n::translate('sys_error_showname_no_longer'));
		}
		elseif ( ! preg_match('#^[-_a-z0-9\s]+$#i', $data['showname']) ) {
			$validator->setError('showname', Cubix_I18n::translate('sys_error_must_contain'));
		}

		if (! strlen($data['phone_prefix']) && ! strlen($data['phone_number']) && ! strlen($data['email']))
		{
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_email_required'));
		}
		else
		{
			if (  strlen($data['phone_prefix']) && ! strlen($data['phone_number']) && ! strlen($data['email']) )
			{
				$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_required'));
			}
			elseif (!strlen($data['phone_prefix']) && strlen($data['phone_number']))
			{
				$validator->setError('phone_prefix', Cubix_I18n::translate('sys_error_country_calling_code_required'));
			}
			elseif (strlen($data['phone_prefix']) && strlen($data['phone_number']))
			{

                list($country_id, $phone_prefix,$ndd_prefix) = explode('-',$data['phone_prefix']);

                $check_phone_number = $phone_prefix.$data['phone_number'];
                $countyModel = new Model_Countries();

                $country_iso = strtoupper($countyModel->getPhoneCountryIsoById($country_id));
                $is_valid_phone = Cubix_PhoneNumber::isValid($check_phone_number,$country_iso, null);
                $phone = $this->_parsePhoneNumber($data['phone_number']);
				if (preg_match("/^(\+|00)/", trim($data['phone_number'])))
				{
					$validator->setError('phone_number', Cubix_I18n::translate('sys_error_enter_phone_without_country_code'));
				}
                elseif (substr($data["phone_number"], 0, 1) == '0')
                {
                    $validator->setError('phone_number', Cubix_I18n::translate('sys_error_enter_phone_without_zero'));
                }
				elseif ($is_valid_phone != 1)
				{
					$validator->setError('phone_number', Cubix_I18n::translate('sys_error_invalid_phone_number'));
				}
				else
				{
					$data['contact_phone_free'] = preg_replace('/[^0-9]/', ' ', $data['phone_number']);
					$data['phone_number'] = $phone;
				}
			}
		}

        $client = Cubix_Api_XmlRpc_Client::getInstance();
        if ($phone)
        {
            list($country_id,$phone_prefix,$ndd_prefix) = explode('-',$data['phone_prefix']);
            $data['phone_country_id'] = intval($country_id);

            $data['contact_phone_parsed'] = preg_replace('/^'.$ndd_prefix.'/', '', $phone);
            $data['contact_phone_parsed'] = '00'.intval($phone_prefix).$data['contact_phone_parsed'];

            $data['contact_phone_free'] = preg_replace('/^'.$ndd_prefix.'/', '', $data['contact_phone_free']);
            //$data['contact_phone_free'] = '00'.$phone_prefix.$data['contact_phone_free'];

            $agency_id = $this->user->isAgency() ? $this->agency->id : null;
            $escort_id = $this->escort->id ? $this->escort->id : null;

            $results = $client->call('Escorts.existsByPhone', array($data['contact_phone_parsed'], $escort_id, $agency_id));

            $resCount = is_array($results) ? count($results) : 0;
            if ( $resCount > 0 ) {
                $validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_already_exists'));
            }

        }

        $data['phone_exists'] = $data['contact_phone_parsed'];
        unset($data['phone_prefix']);

		if ( ! $data['city_id']  ) {
			$validator->setError('city_id', Cubix_I18n::translate('sys_error_base_city_required'));
		}

		if ( strlen($data['email']) && ! $this->_validateEmailAddress($data['email']) ) {
			$validator->setError('email', Cubix_I18n::translate('invalid_email'));
		}

		if ( strlen($data['website']) && ! $this->_validateWebsiteUrl($data['website']) ) {
			$validator->setError('website', Cubix_I18n::translate('sys_error_invalid_url'));
		}

		if ( ! is_array($data['sex_availability']) ) {
			$data['services'] = array();

			$validator->setError('services_offered_for_error', Cubix_I18n::translate('sys_error_at_least_one_service_required'));
		}

		foreach ( $data['sex_availability'] as $i => $opt ) {
			if ( ! isset($this->defines['sex_availability_options'][$opt]) ) {
				unset($data['sex_availability'][$i]);
			}
		}
		$data['sex_availability'] = count($data['sex_availability']) ?
			implode(',', $data['sex_availability']) : null;

		
		if(isset($defines['gender_options'][5])){
			unset($defines['gender_options'][5]);
		}

		if ( is_null($data['type'] ) ) {
			$validator->setError('type', Cubix_I18n::translate('category_required'));
		}
		
		if ( is_null($data['gender']) ) {
			$validator->setError('gender', Cubix_I18n::translate('gender_required'));
		}
		else if ( ! array_key_exists($data['gender'], $defines['gender_options']) ) {
			$validator->setError('gender', Cubix_I18n::translate('sys_error_gender_is_invalid'));
		}

		return $data;

	}

	protected function _validateBiography($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'showname' => '',
			'slogan' => 'notags|special',
			'type' => 'int-nz',
			'gender' => 'int-nz',
			'age' => 'int-nz',
			'ethnicity' => 'int-nz',
			'nationality_id' => 'int-nz',
			'home_city_id' => 'int-nz',
			'hair_color' => 'int-nz',
			'hair_length' => 'int-nz',
			'eye_color' => 'int-nz',
			'measure_units' => 'int-nz',
			'height' => 'int-nz',
			'weight' => 'int-nz',
			'dress_size' => '',
			'shoe_size' => 'int-nz',
			'bust' => 'int-nz',
			'waist' => 'int-nz',
			'hip' => 'int-nz',
			'cup_size' => '',
			'breast_type' => '',
			'pubic_hair' => 'int-nz',
			'block_countries' => 'arr-int',
			'keywords' => 'arr-int',
		));
		$data = $form->getData();


		$defines = Zend_Registry::get('defines');
		$data['showname'] = preg_replace('/(\s)+/','$1', trim($data['showname']));
		$escorts_model = new Model_Escorts();
		if ( ! strlen($data['showname']) ) {
			$validator->setError('showname', Cubix_I18n::translate('showname_required'));
		}
		elseif(mb_strlen($data['showname']) > 25 ){
				$validator->setError('showname', Cubix_I18n::translate('sys_error_showname_no_longer'));
		}
		elseif ( ! preg_match('#^[-_a-z0-9\s]+$#i', $data['showname']) ) {
			$validator->setError('showname', Cubix_I18n::translate('sys_error_must_contain'));
		}

		if ( ! strlen($data['home_city_id']) ) {
			$data['home_city_id'] = null;
		}

		if ( ! strlen($data['slogan']) ) {
			$data['slogan'] = null;
		}
		
		else{
			$data['slogan'] = $validator->urlCleaner($data['slogan'], Cubix_Application::getById()->host);
			$data['slogan'] = $validator->removeEmoji($data['slogan']);
		}
		
		if(isset($defines['gender_options'][5])){
			unset($defines['gender_options'][5]);
		}

		if ( is_null($data['type'] ) ) {
			$validator->setError('type', Cubix_I18n::translate('category_required'));
		}
		
		if ( is_null($data['gender']) ) {
			$validator->setError('gender', Cubix_I18n::translate('gender_required'));
		}
		else if ( ! array_key_exists($data['gender'], $defines['gender_options']) ) {
			$validator->setError('gender', Cubix_I18n::translate('sys_error_gender_is_invalid'));
		}

//		if ( $this->mode != 'create' && is_null($data['eye_color']) ) {
//			$validator->setError('eye_color', Cubix_I18n::translate('eye_color_required'));
//		}

		// if ( is_null($data['hair_color']) ) {
		// 	$validator->setError('hair_color', Cubix_I18n::translate('hair_color_required'));
		// }

		$age = $data['age']; unset($data['age']);
		if ( ! is_null($age) ) {
            $data['birth_date'] = strtotime(date('Y-m-d', strtotime(date('Y') - $age . '-01-01') ));

		}

		if ( ! in_array($data['measure_units'], array(METRIC_SYSTEM, ROYAL_SYSTEM)) ) {
			$data['measure_units'] = METRIC_SYSTEM;
		}

		if ( ! in_array($data['cup_size'], $this->defines['breast_size_options']) ) {
			$data['cup_size'] = null;
		}

		if ( Cubix_Application::getById()->measure_units == ROYAL_SYSTEM ) {
			if ( $data['height'] ) {
				$data['height'] = Cubix_UnitsConverter::convert(stripslashes($data['height']), 'ftin', 'cm');
			}

			if ( $data['weight'] ) {
				$data['weight'] = Cubix_UnitsConverter::convert($data['weight'], 'lbs', 'kg');
			}
		}
		
		foreach ( $data['block_countries'] as $i => $zone_id ) {
			if ( $zone_id != '0' ) {
				$data['block_countries'][$i] = array('country_id' => $zone_id);
			}
			else {
				unset($data['block_countries'][$i]);
			}
		}

		$data['height'] = intval($data['height']);
		if ( ! $data['height'] ) $data['height'] = null;
		if ( Cubix_Application::getById()->measure_units != ROYAL_SYSTEM ) {
			$data['weight'] = intval($data['weight']);
		}
		if ( ! $data['weight'] ) $data['weight'] = null;

		return $data;
	}

	private function run_step($step) {

        $method = str_replace('-', ' ', $step);
        $method = ucwords($method);
        $method = str_replace(' ', '', $method);
        $method[0] = strtolower($method[0]);

        $html = $this->{$method . 'Action'}();
        
        return [
            'html' => $html,
            'data' => $this->view->data,
            'errors' => $this->view->errors
        ];

    }

	public function fullAction() {

		$steps = ['biography','status', 'about-me', 'languages', 'working-cities', 'services', 'working-times',
			'prices', 'contact-info', 'gallery', 'video', 'natural-pic'];
		
		$this->view->steps = array_slice($steps, 0, -2);
        if ( $this->user->isAgency() && $this->mode == 'create') {
        	$this->view->steps = array_slice($steps, 0, -3);
        	$steps = array_slice($steps, 0, -3);
        	$this->view->escort_profile_creating = true;
        }

		$data = array();
        $errors = array();
        $sections = '';

		$this->profile->setMode(Model_Escort_Profile::MODE_REALTIME);
		$this->profile->load(true);

        $client = Cubix_Api::getInstance();
        $data = $client->call('getEscortProfileDataFront', array($this->escort->id));
        $this->profile->destruct($data);

        $this->view->layout()->disableLayout();
        $requested_page = $this->_getParam('page');


        // If There is request to get only some part of Escort's data
        // -------------------------------------------------------
        if (!empty($requested_page)) {
            // If It is request for only one page, else it is all pages
            // -------------------------------------------------------
            if (in_array($requested_page, $steps)) {

                // Run the action that handles the page logic
                // -----------------------------------------

                $result = $this->run_step($requested_page);
                // -----------------------------------------

                // Combine all html that is generated after running the page logic
                // -----------------------------------------
                $sections.= $result['html'];
                // -----------------------------------------

                // If there are any errors after running the page logic,
                // simply combine them to show at once
                // -----------------------------------------
                if ($result['errors']) {
                    $errors = array_merge((array)$errors, (array)$result['errors']);
                    $is_error = true;
                }
                // -----------------------------------------

            } elseif ($requested_page == 'full') {

                foreach ($steps as $step) {

                    // Run the action that handles the page logic
                    // -----------------------------------------
                    $result = $this->run_step($step);
                    // -----------------------------------------

                    // Combine all html that is generated after running the page logic
                    // -----------------------------------------
                    $sections.= $result['html'];
                    // -----------------------------------------

                    // If there are any errors after running the page logic,
                    // simply combine them to show at once
                    // -----------------------------------------
                    if ($result['errors']) {
                        $errors = array_merge((array)$errors, (array)$result['errors']);
                        $is_error = true;
                    }
                    // -----------------------------------------
                }
            }

            $showInCallAndOutCallFreeTextBlocks = true;
            if ($data['country_id'] === 68 || $data['country_id'] === 23) {
                $showInCallAndOutCallFreeTextBlocks = false;
            }

            $data['showInCallAndOutCallFreeTextBlocks'] = $showInCallAndOutCallFreeTextBlocks;

            $data = array_merge((array)$data, (array)$this->view->data);

            // Send response
            // ----------------------------
            die(json_encode([
                'html' => $sections,
                'page' => $requested_page,
                //'data' => $this->view->data
            ]));
            // ----------------------------
        }
        // -------------------------------------------------------



        if ( $this->_request->isPost() ) {
        	$response = array(
        		'success' => false,
        		'errors' => $errors 
        	);

			if ( !count($errors) ) {
				$data = array();

				if ( $this->user->isAgency() ) {
					$data['agency_id'] = $this->agency->getId();
				}

				$data['user_id'] = $this->user->getId();
			
				$res = $this->profile->flush($data);
				
				$response['success'] = true;
				$response['escort_id'] = $res;

			}


			die(json_encode($response));

        }

		$this->view->sections = $sections;

		$this->view->data = $data;
		$this->view->errors = $errors;
		$this->view->escort_id = $this->_getParam('escort');

	}

	public function statusAction(){
	    $is_offline = ($this->escort->status & Model_Escorts::ESCORT_STATUS_OFFLINE) || ($this->escort->status & Model_Escorts::ESCORT_STATUS_AWAITING_PAYMENT);
        $responce = '';
	    if($is_offline){
	        $responce = '<div class="mt-20 alert alert-danger" role="alert"><span>'. __('offline_ad_text').'</span></div>';
        }
	    return $responce;
    }

	public function biographyAction()
	{
        $countyModel = new Model_Countries();
		$this->view->countries = $countyModel->getCountries();

		#var_dump($this->mode);die;
		if ( $this->_request->isPost() && $this->_getParam('page') != 'biography') {
			$validator = new Cubix_Validator();
			
			
			$this->view->data = $data = $this->_validateBiography($validator);
			if(null != $this->view->data['birth_date']){
            	$this->view->age = date("Y") - date("Y",$this->view->data['birth_date']);
            }else{
            	$this->view->age = 0;
            }
			$home_city_id = $this->view->data['home_city_id'];

			$home_city_country = '';
			if ( $home_city_id ) {
				$m_city = new Cubix_Geography_Cities();
				$home_city = $m_city->get($home_city_id);

				$m_country = new Cubix_Geography_Countries();
				$home_country = $m_country->get($home_city->country_id);

				$home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
			}
			$this->view->home_city_country = $home_city_country;

			if ( $this->_getParam('dont') ) {
				return false;
			}

			$response = array(
				'success' => false
			);
			
			if ( ! $validator->isValid() ) {

				$status = $validator->getStatus();
				
				$this->view->errors = $status['msgs'];

				if ( $this->mode == 'create' && $this->_request->then != ":back" ) {
					$this->_request->setParam('then', '');
				}

				$response['errors'] = $status['msgs'];

			} else {

                $response['res'] = $this->profile->update($data);
				$response['success'] = true;
				$response['mode'] = $this->profile->getMode();

			}

			// US, France set status pending
			$countries = new Cubix_Geography_Countries();
			$country = $countries->get($this->escort->country_id);
			$config_system = Zend_Registry::get('system_config');
			$slogan_status = $config_system['sloganApprovation'];

			if(in_array($country->iso, ['us', 'fr'])) $slogan_status = 0;
			//

			$this->_addSlogan($data['slogan'], $this->escort->id, $slogan_status);
			$slogan = $this->_getSlogan($this->escort->id);
			$this->view->slogan = $slogan['text'];
			$this->view->slogan_status = $slogan['status'];

			if ( $this->_request->getParam('step')) {
				die(json_encode($response));
			}
		}
		else {

			$this->view->data = $this->profile->getBiography();

			if(null != $this->view->data['birth_date']){
            	$this->view->age = date("Y") - date("Y",$this->view->data['birth_date']);
            }else{
            	$this->view->age = 0;
            }
			$this->view->data['type'] = $this->escort->type;

			$home_city_id = $this->view->data['home_city_id'];

			$home_city_country = '';
			if ( $home_city_id ) {
				$m_city = new Cubix_Geography_Cities();
				$home_city = $m_city->get($home_city_id);

				$m_country = new Cubix_Geography_Countries();
				$home_country = $m_country->get($home_city->country_id);

				$home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
			}
			
			$this->view->data['block_countries'] = $countyModel->getBlockCountries($this->escort->id);

			$this->view->home_city_country = $home_city_country;
			$slogan = $this->_getSlogan($this->escort->id);
			$this->view->slogan = $slogan['text'];
			$this->view->slogan_status = $slogan['status'];
			
		}
        return $this->view->render('/profile/biography.phtml');
	}

	protected function _addSlogan($slogan, $escort_id, $status = 1)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$client->call('Escorts.addSlogan', array($slogan, $escort_id, $status));

		return true;
	}

	protected function _getSlogan($escort_id)
	{
		if ( $escort_id ) {
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$slogan = $client->call('Escorts.getSlogan', array($escort_id));

			return $slogan;
		}
		else {
			return false;
		}
	}

	protected function _validateAboutMe($validator)
	{
		$blackListModel = new Model_BlacklistedWords();
		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'is_smoking' => 'int-nz',
			'is_drinking' => 'int-nz',
			'tatoo' => 'int-nz',
			'piercing' => 'int-nz',
			'characteristics' => 'notags|special',
			'cuisine' => 'notags|special',
			'drink' => 'notags|special',
			'flower' => 'notags|special',
			'perfume' => 'notags|special',
			'designer' => 'notags|special',
			'interests' => 'notags|special',
			'sports' => 'notags|special',
			'gifts' => 'notags|special',
			'hobbies' => 'notags|special'
		);

		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$fields['about_' . $lng] = 'xss';
		}
		
		$form->setFields($fields);
		$data = $form->getData();
        $a_exists = array();

        $langs = Cubix_Application::getLangs();
        foreach($langs as $id => $lng) {
            $langs[$lng->id] = $lng;
        }

        $hasValidDesc = false;
		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			
			if($bl_words = $blackListModel->checkWords($data['about_' . $lng], Model_BlacklistedWords::BL_TYPE_ABOUT)) {
				foreach($bl_words as $bl_word){
					$pattern = '/' . preg_quote($bl_word, '/') . '/';
					$data['about_' . $lng] = preg_replace($pattern, '<abbr class = "black-listed" >' . $bl_word . '</abbr>', $data['about_' . $lng]);
				}

				$validator->setError('about_me_text', 'You can`t use word "'.$blackListModel->getWords() .'"');
				break;
			}
			else
			{	
				$data['about_' . $lng] = $validator->urlCleaner($data['about_' . $lng], Cubix_Application::getById()->host);
				$data['about_' . $lng] = $validator->removeEmoji($data['about_' . $lng]);
                $data['about_' . $lng] = strip_tags(htmlspecialchars($data['about_' . $lng]), '<b><i><strong><em><p><span>');
                $clean_text_length = strlen(trim(strip_tags($data['about_' . $lng])));

				if( $clean_text_length > 200 ){
                    $hasValidDesc = true;
                }
			}
			
			if (strlen($data['about_' . $lng]))
				$a_exists[] = $lng;
		}

		if(!$hasValidDesc) {
            $tmpText = Cubix_I18n::translate('about_text_min_length',array('LENGTH'=>200));
            $validator->setError('about_me_text', $tmpText);
        }

		if (!count($a_exists))
			$validator->setError('about_me_text', Cubix_I18n::translate('about_me_text_required'));
		
		$data['characteristics'] = $validator->urlCleaner($data['characteristics'], Cubix_Application::getById()->host);
		
		return $data;
	}

	public function aboutMeAction()
	{
        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $this->view->escort = $this->escort;

        // Check location using user's IP for USA
        $geoData = Cubix_Geoip::getClientLocation();
        $hasUnitedStatedIP = $geoData['country_iso'] == 'us';

        // Check in approved working cities for USA
        $isWorkingInUSA = $this->escort->country_id == 68;

        // Check in tours cities for USA
        $hasTourInUSA = $client->call('Escorts.hasTour', array($this->escort->id, ['c.id = 68'], ['countries']));
        $this->view->hasAnythingRelatedToUSA = $isWorkingInUSA || $hasUnitedStatedIP || $hasTourInUSA;

        if ( $this->_request->isPost() && $this->_getParam('page') != 'about-me') {
			$validator = new Cubix_Validator();
			$blackListModel = new Model_BlacklistedWords();
			$this->view->data = $data = $this->_validateAboutMe($validator);

			$data['about_has_bl'] = 0;

			$response = array(
        		'success' => false
        	);
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];

				if ( $this->mode == 'create' && $this->_request->then != ":back" ) {
					$this->_request->setParam('then', '');
				}

				$response['errors'] = $status['msgs'];
			} else {

				$this->profile->update($data);

				$response['success'] = true;
				$response['profile'] = true;
			}


			if ( $this->_request->getParam('step')) {
				die(json_encode($response));
			}
		}
		else {
			$this->view->data = $this->profile->getAboutMe();
		}

        return $this->view->render('/profile/about-me.phtml');
    }

	public function _validateLanguages($validator)
	{
		$defines = Zend_Registry::get('defines');
		
		$langs = $this->_getParam('langs', array());
		if ( ! is_array($langs) ) $langs = array();

		$data = array('langs' => array());

		$invalid_lang = false;
		foreach ( $langs as $lang_id => $level ) {
			$level = intval($level); if ( $level < 1 ) continue;
			$data['langs'][] = array('lang_id' => $lang_id, 'level' => $level);

			if ( ! array_key_exists($lang_id, $defines['language_options']) ) {
				$invalid_lang = true;
			}
		}

		if ( ! count($data['langs']) ) {
			$validator->setError('language_error', Cubix_I18n::translate('sys_error_language_required'));
		}
		else if ( $invalid_lang ) {
			$validator->setError('language_error', Cubix_I18n::translate('sys_error_lang_is_invalid'));
		}

		return $data;
	}

	public function languagesAction()
	{
		if ( $this->_request->isPost() && $this->_getParam('page') != 'languages') {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateLanguages($validator);

			$response = array(
        		'success' => false
        	);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];

				if ( $this->mode == 'create' && $this->_request->then != ":back" ) {
					$this->_request->setParam('then', '');
				}

				$response['errors'] = $status['msgs'];
			} else {

				$this->profile->update($data);
				$response['errors'] = $status['msgs'];
				$response['success'] = true;
			}

			if ( $this->_request->getParam('step')) {
				die(json_encode($response));
			}
		}
		else {
			$this->view->data = $this->profile->getLanguages();
		}

        return $this->view->render('/profile/languages.phtml');
	}

	protected function _validateWorkingCitiesStep($validator) 
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'hair_color' => 'int-nz',
			'hair_length' => 'int-nz',
			'eye_color' => 'int-nz',
			'measure_units' => 'int-nz',
			'height' => 'int-nz',
			'weight' => 'int-nz',
			'cup_size' => '',
			'sex_orientation' => 'int-nz',
			'services' => 'arr-int',
			'keywords' => 'arr-int',
			'service_prices' => 'arr-int',
			'service_currencies' => 'arr-int',
			'block_countries' => 'arr-int',
            'travel_place' => ''
		));
		$data = $form->getData();

//		if ( is_null($data['eye_color']) ) {
//			$validator->setError('eye_color', Cubix_I18n::translate('eye_color_required'));
//		}

		if( !isset($data['block_countries']) )
			$data['block_countries'] = [];
		else {
			$block_countries = [];

			foreach($data['block_countries'] as $id){
				$block_countries[] = ['country_id' => $id];
			}
			$data['block_countries'] = $block_countries;
		}
			

		foreach ( $data['services'] as $i => $svc ) {
			$price = isset($data['service_prices'][$svc]) ? intval($data['service_prices'][$svc]) : null;
			if ( ! $price ) $price = null;

			$currency = isset($data['service_currencies'][$svc]) ? intval($data['service_currencies'][$svc]) : null;
			if ( ! $currency ) $currency = null;
			
			$data['services'][$i] = array('service_id' => $svc, 'price' => $price, 'currency_id' => $currency);
		}

		unset($data['service_prices']);
		unset($data['service_currencies']);


		if ( Cubix_Application::getById()->measure_units == ROYAL_SYSTEM ) {
			if ( $data['height'] ) {
				$data['height'] = Cubix_UnitsConverter::convert(stripslashes($data['height']), 'ftin', 'cm');
			}

			if ( $data['weight'] ) {
				$data['weight'] = Cubix_UnitsConverter::convert($data['weight'], 'lbs', 'kg');
			}
		}
		$data['height'] = intval($data['height']);

		if ( ! $data['height'] ) $data['height'] = null;
		if ( Cubix_Application::getById()->measure_units != ROYAL_SYSTEM ) {
			$data['weight'] = intval($data['weight']);
		}
		if ( ! $data['weight'] ) $data['weight'] = null;

		return $data;
	}

	protected function _validateWorkingCities($validator)
	{

		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'country_id' => 'int-nz',
			'city_id' => 'int-nz',
			'cities' => 'arr-int',
			'cityzones' => 'arr-int',
			'outcall' => 'int',
			'outcall_type' => 'int-nz',
			'outcall_other' => 'notags|special',
            'zip' => '',
            'incall' => 'int',
            'incall_type' => 'int-nz',
            'incall_hotel_room' => 'int-nz',
            'incall_other' => 'notags|special',
            'travel_place' => 'int-nz',
		);

		if($this->mode == 'create') {
			unset(
				$fields['country_id'],
				$fields['city_id'],
				$fields['cities'],
				$fields['cityzones'],
				$fields['travel_place']
			);
		}

		$form->setFields($fields);

		$data = $form->getData();

		if($this->mode != 'create' ) {
			if ( ! is_array($data['cities']) ) $data['cities'] = array();
			$invalid_city = false;

			if(count($data['cities']) > 3){
            	$validator->setError('sys_error_max_city_limit', Cubix_I18n::translate('sys_error_max_city_limit'));
			}

            foreach ($data['cities'] as $i => $city_id) {
                $data['cities'][$i] = array('city_id' => $city_id);
                if (!$city_id) {
                    unset($data['cities'][$i]);
                }

               	if( !Cubix_Geography_Cities::isFromCountry( $city_id, $data['country_id'] )){
               		$validator->setError('not_allowed_to_add_city_from_different_countries', Cubix_I18n::translate('not_allowed_to_add_city_from_different_countries'));
               	}
            }
			$data['cities'][] = array('city_id'=>$data['city_id']);
			
			// Anti-Hack: Ignore trailing cities
			//$data['cities'] = array_slice($data['cities'], 0, Cubix_Application::getById()->max_working_cities);
	
			if ( ! is_array($data['cityzones']) ) $data['cityzones'] = array();
	
			foreach ( $data['cityzones'] as $i => $zone_id ) {
				if ( $zone_id != '0' ) {
					$data['cityzones'][$i] = array('city_zone_id' => $zone_id);
				}
				else {
					unset($data['cityzones'][$i]);
				}
			}
		}
		
		if ( ! $data['incall'] ) {
			$data['incall_type'] = null;
			$data['incall_hotel_room'] = null;
			$data['incall_other'] = null;
			$data['zip'] = null;
		}
		else{
			if ( !$data['incall_type'] ) {
			$validator->setError('zip', Cubix_I18n::translate('sys_error_choose_suboption'));
			$data['incall_type'] = 999;
			}
		}

		if ( ! $data['outcall'] ) {
			$data['outcall_type'] = null;
			$data['outcall_other'] = null;
		}
		

		if ( $data['incall'] && ! strlen($data['zip']) ) {
			//$validator->setError('zip', 'zip code required');
            $data['zip'] = null;
		}
		unset($data['incall']);
		unset($data['outcall']);
		
		if ( ! is_null($data['incall_type']) && ! $validator->validateZipCode($data['zip'], false) ) {
			//$validator->setError('zip', 'Invalid zip code');
            $data['zip'] = null;
		}
		
		$data['zip'] = $validator->urlCleaner($data['zip'], Cubix_Application::getById()->host);
		$data['incall_other'] = $validator->urlCleaner($data['incall_other'], Cubix_Application::getById()->host);
		$data['outcall_other'] = $validator->urlCleaner($data['outcall_other'], Cubix_Application::getById()->host);
		
		if ( $this->mode != 'create' && (! $data['travel_place'] || intval($data['travel_place']) == 0 )) {
			$validator->setError('travel_place', Cubix_I18n::translate('travel_place_required'));
		}

		if ( ! is_null($data['incall_type']) && $data['incall_type'] == 2 ) {
			if ( ! isset($data['incall_hotel_room']) ) {
				$validator->setError('incall_hotel_room', Cubix_I18n::translate('sys_error_choose_hotel_room'));
			}
		}


		if ( $this->mode != 'create' && ! $data['city_id']  ) {
			$validator->setError('country_id', Cubix_I18n::translate('sys_error_base_city_required'));
		}
		/*else if ( ! Cubix_Geography_Cities::isFromApplicationCountry($data['city_id']) ) {
			$validator->setError('country_id', 'Base city is not from ' . Cubix_Application::getById()->country_title);
		}
		else if ( $invalid_city ) {
			$validator->setError('country_id', 'Selected cities are not from ' . Cubix_Application::getById()->country_title);
		}*/

		if ( $this->mode != 'create' && ! $data['country_id'] ) {
			$validator->setError('country_id', Cubix_I18n::translate('coutry_required'));
		}
		/*else if ( $data['country_id'] != Cubix_Application::getById()->country_id ) {
			$validator->setError('country_id', 'invalid country selected');
		}*/
		return $data;
	}

	public function workingCitiesAction()
	{

		$data = $this->profile->getWorkingCities();
		
		$countries = new Cubix_Geography_Countries();

		$this->view->countries = $countries->ajaxGetAll(false);

		$cities = new Cubix_Geography_Cities();
		$country_id = Cubix_Application::getById()->country_id;

		if ( isset($data['city_id']) && $data['city_id'] ) {
			$city = $cities->get($data['city_id']);
			$country_id = $city->country_id;
			$this->view->country_id = $country_id;
		}

		$region_cities = $this->view->cities = $cities->ajaxGetAll(null, $country_id);
		
		$this->view->regions = $region_cities;
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$this->view->travel_place = $client->call('Escorts.getTravelPlace', array($this->escort->id));

        $this->view->package_country = $client->call('Escorts.getPackageCountryId', array($this->escort->id));


		if ( $this->_request->isPost() && $this->_getParam('page') != 'working-cities') {
  			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateWorkingCities($validator);

			if ( isset($data['city_id']) && $data['city_id'] ) {
				$city = $cities->get($data['city_id']);
				
				$country_id = $city->country_id;
				$this->view->country_id = $country_id;
				$this->view->cities = $cities->ajaxGetAll(null, $country_id);
			}

			$response = array(
	    		'success' => false 
	    	);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				$response['errors'] = $status['msgs'];

				if ( $this->mode == 'create' && $this->_request->then != ":back" ) {
					$this->_request->setParam('then', '');
				}
			} else {

                $client = Cubix_Api_XmlRpc_Client::getInstance();

                //change sales for the USA and France profiles
                $newCountryId = $data["country_id"];
                $backend_user = $this->user->getSalesPerson();
                $backend_user_id = $backend_user['id'];
                $userType = $this->user->user_type;

                if (!empty($newCountryId) && in_array($newCountryId, array(68, 23))) {
                    $client->call('Users.changeSaleForUsaAndFrance', array($this->user->id, $newCountryId, $backend_user_id, $userType));
                }

                //$results = $client->call('Escorts.updateTravelCountries', array($this->escort->id, $data['travel_countries']));
				//unset($data['travel_countries']);
				$results = $client->call('Escorts.updateTravelPlace', array($this->escort->id, $data['travel_place']));
				unset($data['travel_place']);
				//$this->view->selected_travel_countries = $client->call('Escorts.getTravelCountries', array($this->escort->id));
				$this->view->travel_place = $client->call('Escorts.getTravelPlace', array($this->escort->id));
				
				$this->profile->update($data);

				// Reset slogans and bubble texts if user was from US, FRANCE
				$country = $countries->get($data['country_id']);
				if (in_array($country->iso, array('us', 'fr'))) {
					$response['slogans_bubble_flushed'] = $client->call('Escorts.resetSlogansAndBubbleText', array($this->escort->id));
					$response['premium_cities_flushed'] = $client->call('Escorts.resetPremiumCities', array($this->escort->id));
				}else{

                    $escort_old_city_data = $this->profile->getWorkingCities();
                    $escort_old_city_id = $escort_old_city_data['city_id'];

                    if($escort_old_city_id != $data['city_id']){
                        $response['premium_cities_restore_flushed'] = $client->call('Escorts.restorePremiumCities', array($this->escort->id,$data['country_id'],$escort_old_city_id,$data['city_id']));
                    }


                }
				//

				$response['success'] = true;
				
			}



			if ( $this->_request->getParam('step')) {
				die(json_encode($response));
			}
		}
		else {
			
			
			$data = $this->profile->getWorkingCities();

            $showInCallAndOutCallFreeTextBlocks = true;
            if ($country_id === 68 || $country_id === 23) {
                $showInCallAndOutCallFreeTextBlocks = false;
            }

			$data['showInCallAndOutCallFreeTextBlocks'] = $showInCallAndOutCallFreeTextBlocks;

			$param_cities = array();
			

			if ( count($data['cities']) ) {
				foreach ( $data['cities'] as $city ) {
					$param_cities[] = $city['city_id'];
				}
			}
			if($data['city_id']){
				$param_cities[] = $data['city_id'];
			}

			$this->_request->setParam('ajax_cities', implode(',', $param_cities));


			$this->getCityzonesAction();
			//$this->view->layout()->enableLayout();

			$data['country_id'] = $country_id;

			$this->view->data = $data;
		}

        return $this->view->render('/profile/working-cities.phtml');
	}

	public function getCityzonesAction()
	{
		$this->view->layout()->disableLayout();
		
		$cities = $this->_request->ajax_cities;
		
		$cities = trim($cities, ',');
		$cities = explode(',', $cities);
		$cz_model = new Cubix_Geography_Cityzones();
		$all_cityzones = array();
	
		if ( count($cities) ) {
			foreach( $cities as $city ) {
				$cityzones = $cz_model->ajaxGetAll($city);
				if ( count($cityzones) ) {
					$all_cityzones = array_merge($all_cityzones, $cityzones);
					
				}
					
			}
		}
		
		if ( 1 == (int)$this->_request->ajax) {
			echo json_encode($all_cityzones);
			die();
		}
		else {
			$this->view->all_cityzones = $all_cityzones;
			
		}
		
	}

	public function _validateServices($validator)
	{		
		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'sex_orientation' => 'int-nz',
			'sex_availability' => '',
			'services' => 'arr-int',
			'keywords' => 'arr-int',
			'service_prices' => 'arr-int',
			'service_currencies' => 'arr-int'
		);

		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$fields['additional_service_' . $lng] = 'xss';
		}

		$form->setFields($fields);
		$data = $form->getData();


		if ( ! is_array($data['services']) ) $data['services'] = array();
		
		foreach ( $data['services'] as $i => $svc ) {
			$price = isset($data['service_prices'][$svc]) ? intval($data['service_prices'][$svc]) : null;
			if ( ! $price ) $price = null;

			$currency = isset($data['service_currencies'][$svc]) ? intval($data['service_currencies'][$svc]) : null;
			if ( ! $currency ) $currency = null;
			
			$data['services'][$i] = array('service_id' => $svc, 'price' => $price, 'currency_id' => $currency);
		}


		if ( ! is_array($data['sex_availability']) ) {
			$data['services'] = array();

			$validator->setError('services_offered_for_error', Cubix_I18n::translate('sys_error_at_least_one_service_required'));
		}

		foreach ( $data['sex_availability'] as $i => $opt ) {
			if ( ! isset($this->defines['sex_availability_options'][$opt]) ) {
				unset($data['sex_availability'][$i]);
			}
		}
		$data['sex_availability'] = count($data['sex_availability']) ?
			implode(',', $data['sex_availability']) : null;

		unset($data['service_prices']);
		unset($data['service_currencies']);
		
		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$data['additional_service_' . $lng] = $validator->urlCleaner($data['additional_service_' . $lng], Cubix_Application::getById()->host);
			$data['additional_service_' . $lng] = $validator->removeEmoji($data['additional_service_' . $lng]);
            $data['additional_service_' . $lng] = strip_tags(htmlspecialchars($data['additional_service_' . $lng]), '<b><i><strong><em><p><span>');
		}
		
		return $data;
	}

	public function _validateServicesStep($validator) 
	{
		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'zip' => '',
			'incall' => 'int',
			'incall_type' => 'int-nz',
			'incall_hotel_room' => 'int-nz',
			'incall_other' => 'notags|special',
			'outcall' => 'int',
			'outcall_type' => 'int-nz',
			'outcall_other' => 'notags|special',
		);
		$form->setFields($fields);
		$data = $form->getData();

		if ( ! $data['incall'] ) {
			$data['incall_type'] = null;
			$data['incall_hotel_room'] = null;
			$data['incall_other'] = null;
			$data['zip'] = null;
		}
		else{
			if ( !$data['incall_type'] ) {
			$validator->setError('zip', Cubix_I18n::translate('sys_error_choose_suboption'));
			$data['incall_type'] = 999;
			}
		}

		if ( ! $data['outcall'] ) {
			$data['outcall_type'] = null;
			$data['outcall_other'] = null;
		}

		if ( $data['incall'] && ! strlen($data['zip']) ) {
			//$validator->setError('zip', 'zip code required');
            $data['zip'] = null;
		}
		unset($data['incall']);
		unset($data['outcall']);
		
		if ( ! is_null($data['incall_type']) && ! $validator->validateZipCode($data['zip'], false) ) {
			//$validator->setError('zip', 'Invalid zip code');
            $data['zip'] = null;
		}
		
		$data['zip'] = $validator->urlCleaner($data['zip'], Cubix_Application::getById()->host);
		$data['incall_other'] = $validator->urlCleaner($data['incall_other'], Cubix_Application::getById()->host);
		$data['outcall_other'] = $validator->urlCleaner($data['outcall_other'], Cubix_Application::getById()->host);

		if ( ! is_null($data['incall_type']) && $data['incall_type'] == 2 ) {
			if ( ! isset($data['incall_hotel_room']) ) {
				$validator->setError('incall_hotel_room', Cubix_I18n::translate('sys_error_choose_hotel_room'));
			}
		}

		return $data;

	}

	public function servicesAction()
	{
        $this->view->escort = $this->escort;

		if ( $this->_request->isPost() && $this->_getParam('page') != 'services') {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateServices($validator);

			$response = array(
        		'success' => false
        	);
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				$response['errors'] = $status['msgs'];

				if ( $this->mode == 'create' && $this->_request->then != ":back" ) {
					$this->_request->setParam('then', '');
				}
			} else {

				$this->profile->update($data);
				$response['success'] = true;
			}

			if ( $this->_request->getParam('step')) {
				die(json_encode($response));
			}
		}
		else {
			$data = $this->profile->getServices();

			$data1 = $this->profile->getWorkingCities();
			$cities = new Cubix_Geography_Cities();
		
			if ( isset($data1['city_id']) && $data1['city_id'] ) {
				$city = $cities->get($data1['city_id']);
				$country_id = $city->country_id;
				$this->escort->country_id = $country_id;
			}
			$this->view->data = $data;
		}

        return $this->view->render('/profile/services.phtml');
	}

	protected function format_working_times()
	{
		foreach ($_POST['time_from'] as $key => $value) {
			$from = explode(':', $value);
			
			if(isset($from[0]) AND ! empty($from[0]))
				$_POST['time_from'][$key] = $from[0];
	
			if(isset($from[1]) AND ! empty($from[1]))
				$_POST['time_from_m'][$key] = $from[1];
		}

		foreach ($_POST['time_to'] as $key => $value) {
			$to = explode(':', $value);
			
			if(isset($to[0]) AND ! empty($to[0]))
				$_POST['time_to'][$key] = $to[0];
	
			if(isset($to[1]) AND ! empty($to[1]))
				$_POST['time_to_m'][$key] = $to[1];
		}
		
	}

	protected function _validateWorkingTimes($validator)
	{
		$this->format_working_times();
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'available_24_7' => 'int-nz',
			'night_escort' => 'int-nz',
			'day_index' => 'arr-int',
			'time_from' => 'date',
			'time_to' => 'date',
			'time_from_m' => 'date',
			'time_to_m' => 'date',
			'vac_date_from_day' => 'int-nz',
			'vac_date_from_month' => 'int-nz',
			'vac_date_from_year' => 'int-nz',
			'vac_date_to_day' => 'int-nz',
			'vac_date_to_month' => 'int-nz',
			'vac_date_to_year' => 'int-nz',
			'night_escorts' => 'arr-int'
		));
		$data = $form->getData();

		$_data = array('available_24_7' => $data['available_24_7'], 'night_escort' => $data['night_escort'],'times' => array(),'vac_date_from' => null,'vac_date_to' => null);

		for ( $i = 1; $i <= 7; $i++ ) {
			if ( isset($data['day_index'][$i]) && isset($data['time_from'][$i]) && isset($data['time_from_m'][$i]) && isset($data['time_to'][$i]) && isset($data['time_to_m'][$i]) ) {
				$night_escort =  in_array($i,$data['night_escorts'])?1:0;
				$_data['times'][] = array(
					'day_index' => $i,
					'time_from' => $data['time_from'][$i],
					'time_from_m' => $data['time_from_m'][$i],
					'time_to' => $data['time_to'][$i],
					'time_to_m' => $data['time_to_m'][$i],
					'night_escorts'=>$night_escort
				);
			}
		}
		if(isset($data['vac_date_from_day']) || isset($data['vac_date_from_month']) || isset($data['vac_date_from_year']) || isset($data['vac_date_to_day']) || isset($data['vac_date_to_month']) || isset($data['vac_date_to_year'])){

			$date_from = $data['vac_date_from_year']."-".$data['vac_date_from_month']."-".$data['vac_date_from_day'];
			$date_to = $data['vac_date_to_year']."-".$data['vac_date_to_month']."-".$data['vac_date_to_day'];

			if( isset($data['vac_date_from_day']) && isset($data['vac_date_from_month']) && isset($data['vac_date_from_year']) && isset($data['vac_date_to_day']) && isset($data['vac_date_to_month']) && isset($data['vac_date_to_year'])){

				if ( strtotime($date_from) >= strtotime($date_to) )
				{
					$validator->setError('vacation', Cubix_I18n::translate('sys_error_invalid_date_interval'));
				}
				else if ( strtotime($date_to) < strtotime(date('Y-m-d')) )
				{
					$validator->setError('vacation', Cubix_I18n::translate('sys_error_invalid_date_interval'));
				}
			}
			else{
				$validator->setError('vacation', Cubix_I18n::translate('sys_error_invalid_date_interval'));
			}
			$_data['vac_date_from'] = $date_from;
			$_data['vac_date_to'] = $date_to;
		}
		return $_data;
	}

	public function workingTimesAction()
	{
		$vacation = new Model_EscortV2Item(array('id' =>$this->escort->id  ));
		$this->view->escort_id = $this->escort->id;
		if ( $this->_request->isPost() && $this->_getParam('page') != 'working-times') {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateWorkingTimes($validator);

			$this->format_working_times();
			$form = new Cubix_Form_Data($this->_request);
			$form->setFields(array(
				'available_24_7' => 'int-nz',
				'night_escort' => 'int-nz',
				'day_index' => 'arr-int',
				'time_from' => 'date',
				'time_to' => 'date',
				'time_from_m' => 'date',
				'time_to_m' => 'date',
				'vac_date_from_day' => 'int-nz',
				'vac_date_from_month' => 'int-nz',
				'vac_date_from_year' => 'int-nz',
				'vac_date_to_day' => 'int-nz',
				'vac_date_to_month' => 'int-nz',
				'vac_date_to_year' => 'int-nz',
				'night_escorts' => 'arr-int'
			));

			$response = array(
        		'success' => false,
				'errors' => $errors,
				'all' => $form->getData(),
        	);
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				$response['errors'] = $status['msgs'];

				if ( $this->mode == 'create' && $this->_request->then != ":back" ) {
					$this->_request->setParam('then', '');
				}
			} else {
				if ( $this->mode == 'create' ) {
					//$this->_request->setParam('then', ':next');
				}

				$this->profile->update($data);

				$vacation->updateVacation($data['vac_date_from'], $data['vac_date_to']);

				$response['success'] = true;
			}

			if ( $this->_request->getParam('step')) {
				die(json_encode($response));
			}
		}
		else {

			$vac = (array) $vacation->getVacation();
			$data = $this->profile->getWorkingTimes();
			$data = array_merge($data,$vac);
			$this->view->data = $data;

		}

        return $this->view->render('/profile/working-times.phtml');
	}

	protected function _validatePrices($validator)
	{
		$rates = $this->_getParam('rates');

		if ( ! is_array($rates) ) {
			$rates = array();
		}

		$data = array('rates' => array());

		//$currencies = array_keys($this->defines['currencies']);
		$currencies = array_keys(Model_Currencies::getAllAssoc());
		$units = array_keys($this->defines['time_unit_options']);
		
		foreach ( $rates as $availability => $_rates ) {
			if ( $availability == 'incall' ) { $availability = 1; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			elseif ( $availability == 'outcall' ) { $availability = 2; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			elseif ( $availability == 'travel' ) { $availability = 0; $types = array('weekend'); }
			else continue;
			
			foreach ( $_rates as $rate ) {
									
				if(is_array($rate)){
					$rate = reset($rate);
				}
				
				if ( get_magic_quotes_gpc() ) $rate = stripslashes($rate);
				$rate = @json_decode($rate);

				if ( is_object($rate) ) $rate = (array) $rate;
				if ( $rate === false || ! is_array($rate) ) {
					continue;
				}

				// Case when rate is standart and some data are not valid
				if ( ! isset($rate['type']) && ( ! isset($rate['time']) || ! isset($rate['unit']) || ! isset($rate['price']) || ! isset($rate['currency']) ) ) {
					continue;
				}
				// Case when rate is a type of custom and some data are not valid
				elseif ( isset($rate['type']) && ( ! isset($rate['price']) || ! isset($rate['currency']) || ! in_array($rate['type'], $types) ) ) {
					continue;
				}

				// If price is invalid
				$price = intval($rate['price']);
				if ( $price <= 0 ) continue;

				// If currency is invalid
				$currency = intval($rate['currency']);
				if ( ! in_array($currency, $currencies) ) {
					continue;
				}
				
				// If rate is custom validate data add only type, price and currency fields
				if ( ! isset($rate['type']) ) {
					$time = intval($rate['time']);
					if ( $time <= 0 ) continue;

					$unit = intval($rate['unit']);
					if ( ! in_array($unit, $units) ) continue;

					$data['rates'][] = array('availability' => $availability, 'time' => $time, 'time_unit' => $unit, 'price' => $price, 'currency_id' => $currency);
				}
				// Otherwize add also time and time unit
				else {
					$data['rates'][] = array('availability' => $availability, 'type' => $rate['type'], 'price' => $price, 'currency_id' => $currency);
				}
			}
		}
		
		$min_book_time = $this->_getParam('min_book_time');
		$min_book_time_unit = $this->_getParam('min_book_time_unit');
		$has_down_pay = $this->_getParam('has_down_pay');
		$down_pay = $this->_getParam('down_pay');
		$down_pay_cur = $this->_getParam('down_pay_cur');
		$travel_payment_method = $this->_getParam('travel_payment_method');
		$specify = $this->_getParam('specify');
		$reservation = $this->_getParam('reservation');
		
		$data['min_book_time'] = intval($this->_getParam('min_book_time'));
		$data['min_book_time_unit'] = $this->_getParam('min_book_time_unit');
		$data['has_down_pay'] = $this->_getParam('has_down_pay');
		$data['down_pay'] = intval($this->_getParam('down_pay'));
		$data['down_pay_cur'] = $this->_getParam('down_pay_cur');
		$data['reservation'] = $this->_getParam('reservation');
		$data['travel_payment_methods'][] = $this->_getParam('travel_payment_method');
		$data['travel_other_method'][] = $this->_getParam('specify');
		
		return $data;
	}

	public function	pricesAction()
	{
	    $this->view->escort = $this->escort;
		$this->_request->setParam('no_tidy', true);
				
		if ( $this->_request->isPost() && $this->_getParam('page') != 'prices') {
			$validator = new Cubix_Validator();

			$data = $this->_validatePrices($validator);
			$data_r = $this->profile->reconstructRates($data['rates']);
			$rates = $data['rates'];
			unset($data['rates']);
			$this->view->data = array('rates' => $data_r, 'conditions' => $data);
			$data['rates'] = $rates;

			$response = array(
        		'success' => false,
        		'errors' => $errors 
        	);
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				$response['errors'] = $status['msgs'];

				if ( $this->mode == 'create' && $this->_request->then != ":back" ) {
					$this->_request->setParam('then', '');
				}
			} else {
				if ( $this->mode == 'create' ) {
					//$this->_request->setParam('then', ':next');
				}

				$this->profile->update($data);

				$response['success'] = true;
			}

			if ( $this->_request->getParam('step')) {
				die(json_encode($response));
			}
		}
		else {
			$data = $this->profile->getPrices();
			$data2 = $this->profile->getTravelPaymentMethods();
			$data3 = $this->profile->getAboutMe();
			
			$data['conditions'] = array_merge($data2, $data3);
			$this->view->data = $data;
		}

        return $this->view->render('/profile/prices.phtml');
	}

	public function _validateContactInfo($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'phone_prefix' => '',
			'phone_number' => '',
			'phone_instr' => 'int-nz',
			'phone_instr_no_withheld' => 'int-nz',
			'phone_instr_other' => 'notags|special',
			'email' => '',
			'website' => 'notags|special',
			'club_name' => 'notags|special',
			'street' => 'notags|special',
			'street_no' => 'notags|special',
			'display_address' => 'int',
			'viber' => 'int',
			'whatsapp' => 'int',
			'wechat' => 'int',
			'telegram' => 'int',
			'ssignal' => 'int',
			'prefered_contact_methods' => '',
            'apps_availble' => 'arr'
		));

		$data = $form->getData();
		$data['display_address'] = (bool) $data['display_address'];
        $data['contact_phone_parsed'] = null;
		$data['contact_phone_free'] = null;
		$data['phone_country_id'] = null;
		$phone = null;

		// This is alternative, if apps availble will be via the multiselect
        // ---------------------------------------------
		if(isset($data['apps_availble'])) {
		    foreach($data['apps_availble'] as $app_name) {
		        $data[strtolower($app_name)] = 1;
            }
        }
        // ---------------------------------------------

		if (! strlen($data['phone_prefix']) && ! strlen($data['phone_number']) && ! strlen($data['email']))
		{
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_email_required'));
		}
		else
		{
			if (  strlen($data['phone_prefix']) && ! strlen($data['phone_number']) && ! strlen($data['email']) )
			{
				$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_required'));
			}
			elseif (!strlen($data['phone_prefix']) && strlen($data['phone_number']))
			{
				$validator->setError('phone_prefix', Cubix_I18n::translate('sys_error_country_calling_code_required'));
			}
			elseif (strlen($data['phone_prefix']) && strlen($data['phone_number']))
			{

                list($country_id, $phone_prefix,$ndd_prefix) = explode('-',$data['phone_prefix']);

                $check_phone_number = $phone_prefix.$data['phone_number'];
                $countyModel = new Model_Countries();

                $country_iso = strtoupper($countyModel->getPhoneCountryIsoById($country_id));
                $is_valid_phone = Cubix_PhoneNumber::isValid($check_phone_number,$country_iso, null);
                $phone = $this->_parsePhoneNumber($data['phone_number']);

				if (preg_match("/^(\+|00)/", trim($data['phone_number'])))
				{
					$validator->setError('phone_number', Cubix_I18n::translate('sys_error_enter_phone_without_country_code'));
				}
                elseif (substr($data["phone_number"], 0, 1) == '0')
                {
                    $validator->setError('phone_number', Cubix_I18n::translate('sys_error_enter_phone_without_zero'));
                }
				elseif ($is_valid_phone != 1)
				{
					$validator->setError('phone_number', Cubix_I18n::translate('sys_error_invalid_phone_number'));
				}
				else
				{
					$data['contact_phone_free'] = preg_replace('/[^0-9]/', ' ', $data['phone_number']);
					$data['phone_number'] = $phone;
				}
			}
		}
		
		/*if ( ! strlen($data['phone_prefix']) ) {
			$validator->setError('phone_prefix', Cubix_I18n::translate('sys_error_country_calling_code_required'));
		}
		if ( ! strlen($data['phone_number']) ) {
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_required'));
		}
		elseif(preg_match("/^(\+|00)/", trim($data['phone_number'])) ) {
				$validator->setError('phone_number', Cubix_I18n::translate('sys_error_enter_phone_without_country_code'));
			}
		else if ( false === ($phone = $this->_parsePhoneNumber($data['phone_number'])) || ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_number'])) {
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_invalid_phone_number'));
		}
		else {
			$data['phone_number'] = $phone;

		}*/
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		if ($phone)
		{   
			list($country_id,$phone_prefix,$ndd_prefix) = explode('-',$data['phone_prefix']);
            $data['phone_country_id'] = intval($country_id);
			
			$data['contact_phone_parsed'] = preg_replace('/^'.$ndd_prefix.'/', '', $phone);
			$data['contact_phone_parsed'] = '00'.intval($phone_prefix).$data['contact_phone_parsed'];
			
			$data['contact_phone_free'] = preg_replace('/^'.$ndd_prefix.'/', '', $data['contact_phone_free']);
			//$data['contact_phone_free'] = '00'.$phone_prefix.$data['contact_phone_free'];
			
			$agency_id = $this->user->isAgency() ? $this->agency->id : null;
			$escort_id = $this->escort->id ? $this->escort->id : null;
			
			$results = $client->call('Escorts.existsByPhone', array($data['contact_phone_parsed'], $escort_id, $agency_id));
			
			$resCount = is_array($results) ? count($results) : 0;
			if ( $resCount > 0 ) {
				$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_already_exists'));
			}
			
		}
		$data['phone_exists'] = $data['contact_phone_parsed'];
		// unset($data['phone_prefix']);
		if ( ! in_array($data['phone_instr'], array_keys($this->defines['phone_instructions'])) ) {
			$data['phone_instr'] = null;
		}

		if ( ! is_null($data['phone_instr_no_withheld']) ) {
			$data['phone_instr_no_withheld'] = 1;
		}
		
		if ( ! is_null($data['phone_instr_other']) && $this->_hasPhoneNumber($data['phone_instr_other']) ) {
			$validator->setError('phone_instr_other', Cubix_I18n::translate('sys_error_invalid_other'));
		}

		if ( strlen($data['email']) && ! $this->_validateEmailAddress($data['email']) ) {
			$validator->setError('email', Cubix_I18n::translate('invalid_email'));
		}

		if ( strlen($data['website']) && ! $this->_validateWebsiteUrl($data['website']) ) {
			$validator->setError('website', Cubix_I18n::translate('sys_error_invalid_url'));
		}
		
		$contact_data = $client->call('Escorts.getWebPhone', array($this->escort->id));
		
		if ( strlen($data['website'])){
			$old_website = $contact_data['website'];
			if ($old_website != $data['website']){
				$data['website_changed'] = 1;
			}
		}
		if ( strlen($data['contact_phone_parsed'])){
			$old_phone = $contact_data['contact_phone_parsed'];
			if ($old_phone != $data['contact_phone_parsed']){
				$data['phone_changed'] = 1;
				$data['old_phone'] = $old_phone;
				$this->user->need_phone_verification = true;
				Model_Users::setCurrent($this->user);
			}
		}
		
		$data['phone_instr_other'] = $validator->urlCleaner($data['phone_instr_other'], Cubix_Application::getById()->host);
		$data['club_name'] = $validator->urlCleaner($data['club_name'], Cubix_Application::getById()->host);
		$data['street'] = $validator->urlCleaner($data['street'], Cubix_Application::getById()->host);
		$data['street_no'] = $validator->urlCleaner($data['street_no'], Cubix_Application::getById()->host);
		

		if(isset($data['prefered_contact_methods'])){
			foreach ($data['prefered_contact_methods'] as $k => $method) {
				if(!strlen($method)){
					unset($data['prefered_contact_methods'][$k]);
				}
			}
			foreach ($data['prefered_contact_methods'] as $method) {
				if($method == 6){
					$data['prefered_contact_methods'] = array('6');
					break;		
				}
			}
			$data['prefered_contact_methods'] = serialize(array_unique(array_values($data['prefered_contact_methods'])));
		}
		return $data;
	}

	public function contactInfoAction()
	{
		$countyModel = new Model_Countries();
		$this->view->phone_countries = $countyModel->getPhoneCountries();
		if ( $this->_request->isPost() && $this->_getParam('page') != 'contact-info' ) {

			$validator = new Cubix_Validator();

			$data = $this->view->data = $this->_validateContactInfo($validator);
            $this->view->phone_prefix_id = $data['phone_country_id'];

			$response = array(
        		'success' => false
        	);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				$response['errors'] = $status['msgs'];

				if ( $this->mode == 'create' && $this->_request->then != ":back" ) {
					$this->_request->setParam('then', '');
				}
			} else {
				if ( $this->mode == 'create' ) {
					//$this->_request->setParam('then', ':next');
				}

				$this->profile->setAvailableApps($data);
				$this->profile->update($data);
				$response['success'] = true;
			}


			if ( $this->_request->getParam('step')) {
				die(json_encode($response));
			}
		}
		else {
			$data = $this->profile->getContactInfo();
			$apps = $this->profile->getAvailableApps();

			$data = array_merge($apps, $data);
			$phone_prfixes = $countyModel->getPhonePrefixs();
			$this->view->phone_prefix_id = $data['phone_country_id'];
			if($data['phone_number']){
				if(preg_match('/^(\+|00)/',trim($data['phone_number'])))
				{
					$phone_prefix_id = NULL;
					$phone_number = preg_replace('/^(\+|00)/', '',trim($data['phone_number']));
					foreach($phone_prfixes as $prefix)
					{
						if(preg_match('/^('.$prefix->phone_prefix.')/',$phone_number))
						{
							$phone_prefix_id = $prefix->id;
							$data['phone_number'] = preg_replace('/^('.$prefix->phone_prefix.')/', '',$phone_number);
							break;
						}

					}
					$this->view->phone_prefix_id = $phone_prefix_id;
				}
			}
			$this->view->data = $data;

        }

        return $this->view->render('/profile/contact-info.phtml');
    }

	public function galleryAction()
	{
		$escort_id = $this->view->escort_id;

		$this->view->user = $this->user;
		if($this->_getParam('video_upload')){
			$this->_forward('video');
		}
		elseif($this->_getParam('natural_pic')){
			$this->_forward('natural-pic');
		}
		else{

            $client = new Cubix_Api_XmlRpc_Client();
			$this->view->is_message = false;
			if ( $this->_getParam('show_success') ){
				$this->view->is_message = true;
			}

			if ( $this->user->isEscort() ) {
				$escort = $this->user->getEscort();
				$escort_id = $escort->id;

			}
			else {

				$escort_id = intval($this->_getParam('escort'));

				if ( 1 > $escort_id ) die;

				if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
					die("This agency has no escorts");
				}

				$model = new Model_Escorts();
				$this->escort = $escort = $model->getById($escort_id);
			}

			$cur_gallery_id = $this->_getParam('gallery_id');
			$cur_gallery_id = is_null($cur_gallery_id) ? 0 : intval($cur_gallery_id);
			$this->view->cur_gallery_id = $cur_gallery_id;
			$galleries = $client->call('getPhotoGalleriesED', array($escort_id));

			foreach($galleries as $i => $gallery) {
				$galleries[$i]['photos'] = $this->_loadPhotos($gallery['fake_id']);
			}

			$this->view->galleries = $galleries;
			// $photos = $this->_loadPhotos($cur_gallery_id);

			$photos_ids = array();
			foreach ( $photos as $photo ) {
				$photo_ids[] = intval($photo['id']);
			}

			$action = $this->_getParam('a');

			$photos_min_count = Cubix_Application::getById(Cubix_Application::getId())->min_photos;

			$this->view->is_private = $is_private = (bool) intval($this->_getParam('is_private'));



			if ( 'set-main' == $action || ! is_null($this->_getParam('set_main')) ) {
				$ids = $this->_getParam('photo_id');
				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
				}

				$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED;
				if(count($ids) == 1){
					$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED_ONE;
				}

				$photo_id = reset($ids);
				if ( ! in_array($photo_id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}
				$photo = new Model_Escort_PhotoItem(array('id' => $photo_id ,'escort_id' => $escort_id));
				$photo->setRotatePics($ids);
				$result = $photo->setMain();
				$client->call('Escorts.setPhotoRotateType', array($escort_id, $rotate_type));
				$escort->photo_rotate_type = $rotate_type;
				$this->_loadPhotos(null);
			}
			elseif ( 'delete' == $action || ! is_null($this->_getParam('delete')) ) {
				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
					}
				}

				$photo = new Model_Escort_Photos();
				$result = $photo->remove($ids);

				$this->_loadPhotos(null);
			}
			elseif ( 'upload' == $action || ! is_null($this->_getParam('upload')) ) {

					$set_photo = false;
					$config = Zend_Registry::get('images_config');
					$new_photos = array();
					$upload_errors = array();

					foreach ( $_FILES as $i => $file )
					{
						try {

							if ( $is_private ){
								if ( strpos($i, 'public_') === 0 ){
									continue;
								}
							}else{
								if ( strpos($i, 'private_') === 0 ){
									continue;
								}
							}

							if ( ! isset($file['name']) || ! strlen($file['name']) ) {
								continue;
							}
							else {
								$set_photo = true;
							}

							$img_ext = strtolower(@end(explode('.', $file[name])));
							if (!in_array( $img_ext , $config['allowedExts'])){
								throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
							}

							// Save on remote storage
							$images = new Cubix_Images();
							$image = $images->save($file['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $file['name']))));

							$image = new Cubix_Images_Entry($image);
							$image->setSize('sthumb');
							$image->setCatalogId($escort->id);
							$image_url = $images->getUrl($image);


							$photo_arr = array(
								'escort_id' => $escort->id,
								'hash' => $image->getHash(),
								'ext' => $image->getExt(),
								'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD,
								'creation_date' => date('Y-m-d H:i:s', time())
							);

							if ( $client->call('Escorts.isPhotoAutoApproval', array($escort_id)) ) {
								$photo_arr['is_approved'] = 1;
							}
							/*else if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_PROFILE))) ) {
								$photo_arr['is_approved'] = 1;
							}*/

							$photo = new Model_Escort_PhotoItem($photo_arr);

							$model = new Model_Escort_Photos();
							$photo = $model->save($photo);

							$new_photos[] = $photo;
						} catch ( Exception $e ) {
							$upload_errors[$i] = $file['name'] . ' (' . $e->getMessage() . ')';
						}

				}

				if ( ! $set_photo ) {
					$this->view->uploadError = Cubix_I18n::translate('sys_error_select_photo');
				}

				$this->view->newPhotos = $new_photos;
				$this->view->uploadErrors = $upload_errors;
			}
			elseif ( 'set-adj' == $action ) {
				$photo_id = intval($this->_getParam('photo_id'));

				if ( ! in_array($photo_id, $photo_ids) ) {
					die(json_encode(array('error' => 'An error occured')));
				}

				$photo = new Model_Escort_PhotoItem(array(
					'id' => $photo_id
				));

				try {
					$hash = $photo->getHash();
					$result = array(
						'x' => intval($this->_getParam('x')),
						'y' => intval($this->_getParam('y')),
						'px' => floatval($this->_getParam('px')),
						'py' => floatval($this->_getParam('py'))
					);
					$photo->setCropArgs($result);

					// Crop All images
					$size_map = array(
						'backend_thumb' => array('width' => 150, 'height' => 205),
						'medium' => array('width' => 225, 'height' => 300),
						'thumb' => array('width' => 150, 'height' => 200),
						'nlthumb' => array('width' => 120, 'height' => 160),
						'sthumb' => array('width' => 76, 'height' => 103),
						'lvthumb' => array('width' => 75, 'height' => 100),
						'agency_p100' => array('width' => 90, 'height' => 120),
						't100p' => array('width' => 117, 'height' => 97)
					);
					$conf = Zend_Registry::get('images_config');

					get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
					// echo $conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;

					$catalog = $escort_id;
					$a = array();
					if ( is_numeric($catalog) ) {
						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}
					}
					elseif ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
						array_shift($a);
						$catalog = $a[0];

						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}

						$parts[] = $a[1];
					}

					$catalog = implode('/', $parts);

					foreach($size_map as $size => $sm) {
						get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
					}
				}
				catch ( Exception $e ) {
					die(json_encode(array('error' => 'An error occured')));
				}

				die(json_encode(array('success' => true)));
			}
			elseif ( ! is_null($this->_getParam('make_private')) || ! is_null($this->_getParam('make_public')) ) {

				if ( ! is_null($this->_getParam('make_private')) ) {
					$type = 'private';
				} elseif ( ! is_null($this->_getParam('make_public')) ) {
					$type = 'public';
				} else {
					die;
				}

				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
					}
				}

				foreach ( $ids as $id ) {
					$photo = new Model_Escort_PhotoItem(array('id' => $id));
					$photo->make($type == 'private' ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD);
				}

				$this->_loadPhotos(null);
			}
			elseif ( 'sort' == $action ) {
				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					die(Cubix_I18n::translate('sys_error_select_at_least_on_photo'));
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						die(Cubix_I18n::translate('sys_error_invalid_id_photo'));
					}
				}

				$model = new Model_Escort_Photos();
				$model->reorder($ids);

				die;
			}elseif ( $this->_posted ){
				$data = array();
				$this->_redirect($this->view->getLink('private'));
				return;
				//return $this->profile->update($data, 'gallery');
			}
			$this->view->escort = $escort;
		}
			$this->view->videoTab = $this->videoAction();
			$this->view->naturalPicTab = $this->naturalPicAction();

        return $this->view->render('/profile/gallery.phtml');
    }

	public function videoAction()
	{
		$this->view->user = $this->user;
		$video_model = new Model_Video();
		$video_config =  Zend_Registry::get('videos_config');
		$client = new Cubix_Api_XmlRpc_Client();
		$this->view->is_message = false;
		if ( $this->_getParam('show_success') ){
			$this->view->is_message = true;
		}

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;

		}
		else {

			$escort_id = intval($this->_getParam('escort'));
			
			if ( 1 > $escort_id ) die;

			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}

			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}
		
		$video = $video_model->GetEscortVideo($escort_id,$video_config['VideoCount']);
		$this->view->host =  Cubix_Application::getById(Cubix_Application::getId())->host;
		$this->view->config =  Zend_Registry::get('videos_config');
		$this->view->video_photo = $video[0];
		$this->view->video = $video[1];
		$this->view->escort = $escort;

		// return $this->view->render('private/profile/video.phtml');
		
	}
	
	public function naturalPicAction()
	{
		$this->view->user = $this->user;
		$nat_model = new Model_NaturalPic();
		$client = new Cubix_Api_XmlRpc_Client();
		
		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;

		}
		else {

			$escort_id = intval($this->_getParam('escort'));
			
			if ( 1 > $escort_id ) die;

			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}

			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}
		
		$this->view->nat_pic = $nat_pic = $nat_model->get($escort_id);
		
		if($nat_pic){
			$photo_arr = array(
				'hash' => $nat_pic['hash'], 
				'ext' => $nat_pic['ext']
			);
			$images = new Cubix_Images();
			$image = new Cubix_Images_Entry($photo_arr);
			$image->setSize('nat_pic_thumb');
			$image->setCatalogId($escort_id. '/natural-pics');
			$this->view->nat_image_url = $images->getUrl($image);
		}
		
		// return $this->view->render('private/profile/natural-pic.phtml');
	}
	
	private function _loadPhotos($gallery_id)
	{
		$public_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, false,$gallery_id));
		
		$private_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, true,$gallery_id));

		return $this->view->photos = array_merge($public_photos, $private_photos);
	}

	protected function _parsePhoneNumber($phone)
	{
		$phone = preg_replace('/[^0-9]+/', '', $phone);

		if ( ! strlen($phone) || ! is_numeric($phone) ) {
			return false;
		}

		/*if ( '00' != substr($phone, 0, 2) ) {
			if ( ! is_null($escort_id) ) {
				$iso = $this->profile->country_id;
				if ( $iso && isset($this->defines['dial_codes'][$iso]) ) {
					$phone = '00' . $this->defines['dial_codes'][$iso] . $phone;
				}
			}
		}*/

		return $phone;
	}

	protected function _hasPhoneNumber($string)
	{
		$string = preg_replace('/[^0-9]+/', '', $string);

		if ( strlen($string) > 8 ) {
			return true;
		}

		return false;
	}

	protected function _validateEmailAddress($email)
	{
		return Cubix_Validator::validateEmailAddress($email);
	}

	protected function _validateWebsiteUrl($url)
	{
		//return (bool) preg_match('/^((http|https):\/\/)?(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i', $url);
		if (strtolower(substr($url, 0, 7)) != 'http://' && strtolower(substr($url, 0, 8)) != 'https://' && strtolower(substr($url, 0, 4)) != 'www.')
			return false;
		else
			return true;
	}

	public function finishAction()
	{
		$data = array();
		if ( $this->user->isAgency() ) {
			$data['agency_id'] = $this->agency->getId();
		}

		$data['user_id'] = $this->user->getId();
		// Send the data from session to API, a new revision of the profile will be created!
		$res = $this->profile->flush($data);

		if ( ! $res ) {
			$this->_redirect($this->view->getLink('private'));
		}

		$this->view->escort_id = $res;
	}

	public function ajaxChangeCityAction()
	{
		$this->view->layout()->disableLayout();

		$city_id = intval($this->_getParam('city_id'));
		$data = $this->profile->getWorkingCities();

		$model = new Cubix_Geography_Countries();
		if ( ! $model->hasCity($data['country_id'], $city_id) ) {
			die('Invalid city id');
		}

		$data['city_id'] = $city_id;
		foreach ( $data['cities'] as $i => $city ) {
			if ( $city_id == $city['city_id'] ) {
				$data['cities'][$i]['city_id'] = $city_id;
			}
		}

		$result = $this->profile->update($data);

		$response = array('status' => 'error');

		if ( $result ) {
			$response['status'] = 'ok';
			$response['city'] = Cubix_Geography_Cities::getTitleById($city_id);
		}

		die(json_encode($response));
	}

	public function ajaxChangeZipAction()
	{
		$zip = $this->_getParam('zip');
		$data = $this->profile->getWorkingCities();

		if ( ! $data['incall_type'] ) {
			die('There is no need for zip code');
		}

		$validator = new Cubix_Validator();

		if ( ! $validator->validateZipCode($zip, true) ) {
			die(json_encode(array('status' => 'error')));
		}

		$data['zip'] = $zip;


		$result = $this->profile->update($data );

		$response = array('status' => 'error');

		if ( $result ) {
			$response['status'] = 'ok';
			$response['zip'] = $zip;
		}

		die(json_encode($response));
	}

    public function workingTimesStepAction()
    {
        if($this->_getParam('then') && $this->_getParam('then') == 'back'){
            unset($_POST['then']);
            $response['success'] = true;
            $response['data'] = $this->servicesStepAction();
            die($response);
        }

        $vacation = new Model_EscortV2Item(array('id' => $this->escort->id));
		$this->view->escort_id = $this->escort->id;
		
        if ($this->_getParam('step') == 'workingTimesStep') {

            if ($this->_request->isPost()) {

                $validator = new Cubix_Validator();
                $this->view->data = $data = $this->_validateWorkingTimes($validator);

                $response = array(
                    'success' => false
				);
				
                if (!$validator->isValid()) {

                    $status = $validator->getStatus();
                    $this->view->errors = $status['msgs'];
					$response['errors'] = $status['msgs'];
					
                } else {

                    if(count($this->profile->times) > 0){
                       unset($data['available_24_7']);
                       if(count($data['times']) == 0){
                           unset($data['times']);
                       }
					}
					
                    // $this->profile->update($data, 'working-times');
                    $vacation->updateVacation($data['vac_date_from'], $data['vac_date_to']);
                    $response['success'] = true;
                    $response['data'] = $this->contactInfoStepAction();
                    die(json_encode($response));
                }
            }
        } else {
            $response['success'] = true;
        }

        $vac = (array)$vacation->getVacation();
        $data = $this->profile->getWorkingTimes();
        $data = array_merge($data, $vac);
        $this->view->data = $data;

        $response['data'] = $this->view->render('private/profile/working-times-step.phtml');
        die(json_encode($response));
    }

    public function workingTimesAvailableStepAction(){
        $this->profile->load();
        $this->view->data = $this->profile;
        return $this->view->render('private/profile/working-times-available-step.phtml');
    }

    public function contactInfoStepAction(){
		$response['prevent_popup'] = TRUE;

        if($this->_getParam('then') && $this->_getParam('then') == 'back'){
            unset($_POST['then']);
            $response['success'] = true;
            $response['data'] = $this->servicesStepAction();
            die($response);
        }

        $countyModel = new Model_Countries();
        $this->view->phone_countries = $countyModel->getPhoneCountries();

        if ($this->_getParam('step') == 'contactInfoStep') {
            if ($this->_request->isPost()) {

                $validator = new Cubix_Validator();

				$data = $this->view->data = $this->_validateContactInfo($validator);
				
                $this->view->phone_prefix_id = $data['phone_country_id'];

                $response = array(
                    'success' => false,
					'errors' => $errors,
				);

                if (!$validator->isValid()) {
                    $status = $validator->getStatus();

                    $this->view->errors = $status['msgs'];
					$response['errors'] = $status['msgs'];
					
					$this->profile->last_step = $this->current_step;
					$this->profile->update($data);

                } else {

					if($this->current_step == $this->profile->last_step) $this->profile->last_step++;
					elseif ($this->current_step > $this->profile->last_step) $this->profile->last_step = $this->current_step;
					$this->profile->update($data);

                    $this->profile->setAvailableApps($data);
                    $response['success'] = true;
                    $response['data'] = $this->galleryStepAction();
                    die(json_encode($response));
                }
            }
        }else{
            $response['success'] = true;
            $this->profile->load();

            $this->view->data = $this->profile;
        }

        $response['data'] = $this->view->render('profile/contact-info-step.phtml');
        die(json_encode($response));
    }

    public function galleryStepAction(){

		$response['prevent_popup'] = TRUE;
		
        if($this->_getParam('then') && $this->_getParam('then') == 'back'){
            unset($_POST['then']);
            $response['success'] = true;
            $response['data'] = $this->contactInfoStepAction();
            die($response);
        }

        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $cur_gallery_id = $this->_getParam('gallery_id');
        $cur_gallery_id = is_null($cur_gallery_id) ? 0 : intval($cur_gallery_id);
        $this->view->cur_gallery_id = $cur_gallery_id;
        $galleries = $client->call('getPhotoGalleriesED', array($this->escort->id));

        foreach($galleries as $i => $gallery) {
            $galleries[$i]['photos'] = $this->_loadPhotos($gallery['fake_id']);
        }

        if($this->profile->escort_id){
            $this->view->escort_id = $escort_id = $this->profile->escort_id;
        }else{
            $this->view->escort_id = $escort_id = $this->finishStepAction();
        }

        if ($this->_getParam('step') == 'contactInfoStep' && $this->_request->isPost()){
            $this->session->escort_id = $escort_id;
        }

        if ($this->_getParam('step') == 'galleryStep') {
            $response['success'] = true;
            if ($this->user->isAgency() && $this->_request->isPost()){
                $this->view->escort_id = ($this->session->escort_id) ? $this->session->escort_id : '';
                unset($this->session->escort_id);
            }
			$response['data'] = $this->view->render('profile/finish-step.phtml');
			$response['finish'] = true;
			$response['escort_id'] = escort_id;
			$response['prevent_popup'] = false;
			 
			die(json_encode($response));
        }else{
            $response['success'] = true;
        }

        $this->view->galleries = $galleries;

        $response['data'] = $this->view->render('profile/gallery-step.phtml');
        die(json_encode($response));
    }

	public function finishStepAction($avoid_validation = false, $debug = false)
	{
		$data = array();

		if ($this->user->isAgency()) {
			$data['agency_id'] = $this->agency->getId();
		}

		$data['user_id'] = $this->user->getId();

		//for checking in blacklisted ips and make owner disabled / ip blocked
    	$bl_ip = Cubix_Geoip::getIP();		
		$data['bl_ip'] = $bl_ip;

		$this->profile->load(true);
		$res = $this->profile->flush($data, $avoid_validation);

		$response['success'] = true;
		$response['escort_id'] = $res;

		if($this->user->isAgency() && $avoid_validation === false) {
			$this->profile->clean();
		}

		return $res;

	}

    public function countButtonClickAction()
    {
        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $user = $this->user;
        $buttonId = $this->_request->getParam('button_id');
        $button = 0;
        switch ($buttonId) {
            case 'phone-verification':
                $button = ESCORT_CLICKED_PHONE_VERIFICATION;
                break;
            case '100-verification':
                $button = ESCORT_CLICKED_100_VERIFICATION;
                break;
            case 'upgrade-now':
                $button = ESCORT_CLICKED_UPGRADE_NOW;
                break;
            default:
                $button = 0;
        }

        if ($this->_request->isPost())
        {
            $click = $client->call('Escorts.setButtonClickED', array($this->user->id, $button)) ;
        }
        echo '<pre>';var_dump($click);die;
    }

}