<?php

/**
 * Class Service_Escorts
 * @author Eduard
 */
class Service_Escorts
{
    /**
     * @param array $params
     * @return array
     */
    public static function filterListingMatches(Array & $params): Array
    {
        // Added search result highlight feature - $highlight
        // Added additional words suggestions

        $client = Zend_Registry::get('elastic-client');
        $title = Cubix_I18n::getTblField('title');
        $profileFields = [
            'showname^16',
            'city_'. $title,
            'country_title_en',
            'phone_number',
            //'gender^4',
            //'type^4',
            // 'working_cities',
            // 'email^2', EDIR-492
            //'agency_name^1',
        ];

        $params_elastic = [
            'index' => 'listing_escorts',
            'body' => [
                '_source' => $params['fields'] ?? [],
                "from" => $params['offset'], 'size' => $params['page_size'],
                'query' => [
                    'multi_match' => [
                        "fields" => $profileFields,
                        'query' => $params['query'],
                        'type' => 'phrase_prefix', // use cross_fields for 2 or more word search
                        "operator" => "AND"
                    ]
                ],
                'sort' => [
                    'is_inactive' => [
                        'order' => 'asc'
                    ],
                    'is_vip' => [
                        'order' => 'desc'
                    ],
                    'is_premium' => [
                        'order' => 'desc'
                    ],
                    '_score' =>[
                        'order' => 'desc'
                    ]
                ],
//                'highlight' => [
//                   'pre_tags' => ['<mark>'],
//                    'post_tags' => ['</mark>'],
//                    'fields' => [
//                        'showname' => new \stdClass(),
//                        'country_title_en' => new \stdClass(),
//                        'working_cities' => new \stdClass()
//                    ]
//                ],
            ]
        ];
        $results = $client->search($params_elastic);

        return [
            'hits' => $results['hits']['hits'] ?? [],
            'total_count' => $results['hits']['total'] ?? 0
        ];
    }

    /* Similar words suggestions function
//    public static function getSimilarWords($word)
//    {
//        $client = Zend_Registry::get('elastic-client');
//
//        $params_elastic = [
//            'index' => 'listing_escorts',
//            'body' => [
//                '_source' => ['showname'],
//                'query' => [
//                    'fuzzy' => [
//                        'showname' => [
//                            'value' => $word,
//                            'fuzziness' => '5',
//                            'max_expansions' => '5',
//                        ],
//                    ],
//                ],
//            ]
//        ];
//        return $client->search($params_elastic);
//    } */
    /**
     * @param array $rows
     */
    public static function mutateDataForCurrentLang(Array & $rows): void
    {
        $lng = Cubix_I18n::getLang();
        foreach ($rows as & $row)
        {
            //$highlight = $row['highlight'];
            $row = $row['_source'];
            $row['country'] = $row['country_title_' . $lng] ?? null;
            $row['city'] = $row['city_title_' . $lng] ?? null;
            $row['tour_city'] = $row['tour_city_' . $lng] ?? null;
            $row['about'] = isset($row['about_' . $lng]) && !empty($row['about_' . $lng]) ? $row['about_' . $lng] : $row['about_en'];

            // Highlighting whole name, country and city

//            if(isset($highlight['showname']))
//                $row['showname'] = $highlight['showname'][0];
//            if(isset($highlight['country_title_en']))
//                $row['country'] = $highlight['country_title_en'][0];
//            if(isset($highlight['working_cities']))
//                $row['city'] = $highlight['working_cities'][0];

            $row = (object) $row;
        }
    }
}

?>