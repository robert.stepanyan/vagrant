<?php

class Cubix_Api_Module_Members extends Cubix_Api_Module
{
	static public function getIdByUsername($username)
	{
		return self::db()->fetchOne('
			SELECT m.id
			FROM users u
			INNER JOIN members m ON m.user_id = u.id
			WHERE u.username = ?
		', $username);
	}

	static public function setPremium($member_id, $expires, $is_recurring)
	{
		$updated = self::db()->update('members', array(
			'is_premium' => 1,
			'date_expires' => new Zend_Db_Expr('FROM_UNIXTIME(' . $expires . ')'),
			'is_recurring' => (bool) $is_recurring
		), self::db()->quoteInto('id = ?', $member_id));

		if ( $updated ) {
			self::_logPremium(null, $member_id, self::STATUS_PREMIUM_UPDATED, 'Expiration set to ' . date('d M Y, H:i:s', $expires));

			// in escortforumit.xxx we have a forum
			if ( 1 == self::app()->id ) {
				$forum = new Cubix_ForumApi();
				if ( false == $forum->SetPremium(1, self::getUserIdById($member_id)) ) {
					self::_logPremium(null, $member_id, self::STATUS_FORUM_PREMIUM, 'Could not set member as premium in FORUM!');
				}
			}
		}

		return $updated;
	}

	static public function setPremiumExpired($member_id)
	{
		$updated = self::db()->update('members', array(
			'is_premium' => 0
		), self::db()->quoteInto('id = ?', $member_id));

		if ( $updated ) {
			self::_logPremium(null, $member_id, self::STATUS_PREMIUM_EXPIRED, 'Membership has expired!');

			// in escortforumit.xxx we have a forum
			if ( 1 == Cubix_Application::getId() ) {
				$forum = new Cubix_ForumApi();
				if ( false == $forum->SetPremium(0, self::getUserIdById($member_id)) ) {
					self::_logPremium(null, $member_id, self::STATUS_FORUM_PREMIUM, 'Could not set member expired in FORUM!');
				}
			}
		}

		return $updated;
	}
	
	static public function cancelPremium($user_id)
	{
		$updated = self::db()->update('members', array(
			'is_premium' => 0,
			'is_recurring' => 0,
			'date_expires' => null
		), self::db()->quoteInto('user_id = ?', $user_id));

		/*$member_id = self::db()->fetchOne('SELECT id FROM members WHERE user_id = ?', $user_id);
		
		if ( $updated ) {
			self::_logPremium(null, $member_id, self::STATUS_PREMIUM_EXPIRED, 'Membership is canceled!');

			// in escortforumit.xxx we have a forum
			if ( 1 == Cubix_Application::getId() ) {
				$forum = new Cubix_ForumApi();
				if ( false == $forum->SetPremium(0, $user_id) ) {
					self::_logPremium(null, $member_id, self::STATUS_FORUM_PREMIUM, 'Could not cancel membership in FORUM!');
				}
			}
		}*/

		return $updated;
	}

	const STATUS_TRANSACTION_SUCCESS = 1;
	const STATUS_TRANSACTION_FAIL = 2;
	const STATUS_TRANSACTION_RECURRING_ERROR = 6;
	const STATUS_TRANSACTION_RECURRING_SUCCESS = 7;
	const STATUS_PREMIUM_UPDATED = 3;
	const STATUS_PREMIUM_EXPIRED = 4;
	const STATUS_MEMBER_NOT_FOUND = 5;
	const STATUS_FORUM_PREMIUM = 8;
	const STATUS_PREMIUM_EXPIRATION_FAILED = 9;

	static private function _logPremium($callback_id, $member_id, $status, $desc = null)
	{
		self::db()->insert('members_premium_log', array(
			'callback_id' => $callback_id,
			'member_id' => $member_id,
			'date' => new Zend_Db_Expr('NOW()'),
			'status' => $status,
			'desc' => $desc
		));
	}
}