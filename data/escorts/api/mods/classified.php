<?php

class Cubix_Api_Module_Classified extends Cubix_Api_Module
{
	const EVENT_TYPE_UPDATED  = 1;
	const EVENT_TYPE_REMOVED  = 2;

	public function getAdsTableDump()
	{
		$f = '';

		if (Cubix_Application::getId() == APP_BL)
			$f = '`page_link` varchar(255) DEFAULT NULL,';

        if (Cubix_Application::getId() == APP_EG_CO_UK || Cubix_Application::getId() == APP_6B ){
            $f = '`country_prefix` int DEFAULT NULL,';
        }
        if (Cubix_Application::getId() == APP_A6){
            $f = '
            		`web` varchar(255) DEFAULT NULL,
            		`phone_parsed` varchar(255) DEFAULT NULL,
            		`phone_prefix_id` int(10) DEFAULT 1,
            		`sms_available` tinyint(1) DEFAULT 0,
            		`whatsapp_available` tinyint(1) DEFAULT 0,
            		`premium_period` tinyint(1) DEFAULT NULL,
            ';
        }
		return '
			CREATE TABLE `classified_ads` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`user_id` int(10) unsigned DEFAULT NULL,
			`category_id` int(5) unsigned NOT NULL,
			/*`city_id` int(5) unsigned NOT NULL,*/
			`phone` varchar(255) DEFAULT NULL,
			`email` varchar(255) DEFAULT NULL,
			`title` varchar(255) NOT NULL,
			`slug` varchar(255) DEFAULT NULL,
			`text` text NOT NULL,
			`approvation_date` datetime DEFAULT NULL,
			`is_premium` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
			`search_text` text,
			`view_count` int(10) unsigned NOT NULL DEFAULT \'0\',
			' . $f . '
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
		';
	}
	
	public function getAdsImagesTableDump()
	{
		return '
			CREATE TABLE `classified_ads_images` (
			`ad_id` int(10) unsigned NOT NULL,
			`id` int(10) unsigned NOT NULL,
			`hash` varchar(32) NOT NULL,
			`ext` varchar(5) NOT NULL,
			`is_portrait` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
			`width` int(10) unsigned DEFAULT NULL,
			`height` int(10) unsigned DEFAULT NULL,
			PRIMARY KEY (`ad_id`, `id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		';
	}
	
	public function getAdsCitiesTableDump()
	{
		return '
			CREATE TABLE `classified_ads_cities` (
			`ad_id` int(10) unsigned NOT NULL,
			`city_id` int(10) unsigned NOT NULL,
			PRIMARY KEY (`ad_id`,`city_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		';
	}
	
	public function getAds($start, $limit = 100)
	{
		set_time_limit(0);

		$start = (int) $start;
		if ( $start < 0 ) $start = 0;
		$limit = (int) $limit;
		if ( $limit < 100 ) $limit = 100;
		
		$f = '';
		if ( self::app()->id == APP_BL ) {
			$f = ', view_count, page_link';
		}

		if ( self::app()->id == APP_A6 ) {
			$f = ', web, phone_parsed, phone_prefix_id, sms_available, whatsapp_available, TIMESTAMPDIFF(day, premium_activation_date, premium_expiration_date) as premium_period';
		}

        if ( self::app()->id == APP_EG_CO_UK || self::app()->id  == APP_6B ) {
            $f = ', country_prefix';
        }
				
		$ads = self::db()->fetchAll('
			SELECT 
				id, user_id, category_id, /*city_id,*/ phone, email, title, text, approvation_date, is_premium, view_count,
				search_text ' . $f . ' 
			FROM classified_ads
			WHERE status = 2 AND is_disabled = 0 AND application_id = ?
			LIMIT ' . $start . ', ' . $limit . '
		', array(self::app()->id), Zend_Db::FETCH_OBJ);
		
		return $ads;
	}
	
	public function getAdsImages($start, $limit = 100)
	{
		set_time_limit(0);

		$start = (int) $start;
		if ( $start < 0 ) $start = 0;
		$limit = (int) $limit;
		if ( $limit < 100 ) $limit = 100;
				
		$ads = self::db()->fetchAll('
			SELECT 
				cai.ad_id, cai.image_id AS id, i.hash, i.ext, i.is_portrait, i.width, i.height
			FROM classified_ads_images cai
			INNER JOIN classified_ads ca ON ca.id = cai.ad_id
			INNER JOIN images i ON i.id = cai.image_id
			WHERE status = 2 AND is_disabled = 0 AND application_id = ?
			LIMIT ' . $start . ', ' . $limit . '
		', array(self::app()->id), Zend_Db::FETCH_OBJ);
		
		return $ads;
	}
	
	public function getAdsCities($start, $limit = 100)
	{
		set_time_limit(0);

		$start = (int) $start;
		if ( $start < 0 ) $start = 0;
		$limit = (int) $limit;
		if ( $limit < 100 ) $limit = 100;
				
		$ads = self::db()->fetchAll('
			SELECT 
				cac.ad_id, cac.city_id
			FROM classified_ads_cities cac
			INNER JOIN classified_ads ca ON ca.id = cac.ad_id
			WHERE status = 2 AND is_disabled = 0 AND application_id = ?
			LIMIT ' . $start . ', ' . $limit . '
		', array(self::app()->id), Zend_Db::FETCH_OBJ);
		
		return $ads;
	}





	public function getActiveAds()
	{
		set_time_limit(0);

		$ads = self::db()->fetchAll('
			SELECT
				*
			FROM classified_ads
			WHERE status = 2 AND is_disabled = 0 AND is_sync_done = 0
		', Zend_Db::FETCH_OBJ);

		if ( count($ads) ) {
			$city_sql = '
			SELECT 
				cac.city_id, c.title_en AS title
			FROM classified_ads_cities cac 
			INNER JOIN cities c ON c.id = cac.city_id
			WHERE cac.ad_id = ?';

			$image_sql = '
			SELECT 
				cai.args, i.id AS image_id, i.hash, i.ext, i.is_portrait, i.width, i.height, ' . Cubix_Application::getId() . ' AS application_id
			FROM classified_ads_images cai 
			INNER JOIN images i ON i.id = cai.image_id
			WHERE cai.ad_id = ?';
			// EDIR-861 :  We take from APP_BL for APP_ED only the first 5 pics.
            if ( self::app()->id == APP_BL ) {
                $image_sql .= ' LIMIT 5';
            }

			foreach($ads as $i => $ad) {
				$ads[$i]['cities'] = self::db()->fetchAll($city_sql, array($ad['id']));
				$ads[$i]['images'] = self::db()->fetchAll($image_sql, array($ad['id']));
			}
		}
		
		return $ads;
	}

	public function setIsSyncDone($ad_ids)
	{
		set_time_limit(0);
		
		try{
			if ( is_array($ad_ids) && count($ad_ids) ) {
				foreach ($ad_ids as $id) {
					self::db()->update('classified_ads', array('is_sync_done' => 1), self::db()->quoteInto('id = ?', $id));
				}
			}
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		return $ad_ids;
	}
	
	
	/********************* LATEST ACTIONS ****************************/
	
	public function getLatestActionsTableDump()
	{
		return '
			CREATE TABLE `latest_actions` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`sales_user_id` int(10) unsigned DEFAULT NULL,
			`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`escort_id` int(10) unsigned NOT NULL,
			`action_type` smallint(5) unsigned NOT NULL,
			`comment` varchar(255) DEFAULT NULL,
			PRIMARY KEY (`id`),
			KEY `escort_id` (`escort_id`),
			KEY `action_type` (`action_type`)
			) ENGINE=InnoDB AUTO_INCREMENT=490 DEFAULT CHARSET=utf8;
		';
	}
	
	public function getLatestActionsDetailsTableDump()
	{
		return '
			CREATE TABLE `latest_actions_details` (
				`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`escort_id` int(10) NOT NULL,
				`action_type` int(10) NOT NULL,
				`entity_id` int(10) NOT NULL,
				`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				PRIMARY KEY (`id`),
				KEY `escort_id` (`escort_id`),
				KEY `action_type` (`action_type`)
			  ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
		';
	}
	
	public function getLatestActions($start, $limit = 100)
	{
		set_time_limit(0);

		$start = (int) $start;
		if ( $start < 0 ) $start = 0;
		$limit = (int) $limit;
		if ( $limit < 100 ) $limit = 100;

		$where = " AND DATE(date) = DATE(NOW()) ";
		if ( Cubix_Application::getId() == APP_ED ) {
			$where = " AND DATE(date) > DATE( DATE_SUB( NOW() , INTERVAL 2 DAY ) ) ";
		}
				
		$ads = self::db()->fetchAll('
			SELECT 
				la.*
			FROM latest_actions la
			INNER JOIN escorts e ON e.id = la.escort_id 
			INNER JOIN users u ON u.id = e.user_id
			WHERE u.application_id = ? ' . $where . '
			LIMIT ' . $start . ', ' . $limit . '
		', array(self::app()->id), Zend_Db::FETCH_OBJ);
		
		return $ads;
	}
	
	public function getLatestActionsDetails($start, $limit = 100)
	{
		set_time_limit(0);

		$start = (int) $start;
		if ( $start < 0 ) $start = 0;
		$limit = (int) $limit;
		if ( $limit < 100 ) $limit = 100;

		$where = " AND DATE(date) = DATE(NOW()) ";
		if ( Cubix_Application::getId() == APP_ED ) {
			$where = " AND DATE(date) > DATE( DATE_SUB( NOW() , INTERVAL 2 DAY ) ) ";
		}
				
		$ads = self::db()->fetchAll('
			SELECT 
				la.*
			FROM latest_actions_details la
			INNER JOIN escorts e ON e.id = la.escort_id 
			INNER JOIN users u ON u.id = e.user_id
			WHERE u.application_id = ? ' . $where . '
			LIMIT ' . $start . ', ' . $limit . '
		', array(self::app()->id), Zend_Db::FETCH_OBJ);
		
		return $ads;
	}

	public function getAdUpdatesA6()
	{
		set_time_limit(0);
		$ads_to_remove = array();
		$ads_to_update = array();
		$ids_to_update = array();

		$record_ids = array();
		$update_ids = array();
		$inactive_ids = array();

		$records = self::db()->fetchAll('
			SELECT 
				id, ad_id, event_type
			FROM classified_ads_updates
			WHERE is_sync_done = 0
			
		');
		
		if($records){
			$in = 'IN (';
			foreach ($records as $record) {
				if($record['event_type'] == self::EVENT_TYPE_REMOVED){
					$ads_to_remove[] = $record['ad_id'];
				}else{
					$update_ids[] = $record['ad_id'];
					$in .= $record['ad_id'].",";
				}
				$record_ids[] = $record['id'];
			}
			if(count($update_ids)){
				$in = substr($in, 0, -1).")";

				$inactive_ads = self::db()->fetchAll('
					SELECT 
						id
					FROM classified_ads
					WHERE (status != 2 OR is_disabled = 1) AND id '. $in
				, array(), Zend_Db::FETCH_OBJ);

				if($inactive_ads){
					foreach ($inactive_ads as $ad) {
						$ads_to_remove[] = $ad->id;
						$inactive_ids[] = $ad->id;
					}
				}
			} 
		}
		$ids_to_update = array_diff($update_ids, $inactive_ids);
		$in = implode(',', $ids_to_update);
		$in = 'IN (' . $in . ')';
		if(count($ids_to_update)){
			$ads_to_update['ads'] = self::db()->fetchAll('
				SELECT 
					id, user_id, category_id, phone, email, title, text, approvation_date, is_premium, view_count,
					search_text , web, phone_parsed, phone_prefix_id, sms_available, whatsapp_available, TIMESTAMPDIFF(day, premium_activation_date, premium_expiration_date) as premium_period 
				FROM classified_ads
				WHERE id '. $in
			, array(), Zend_Db::FETCH_OBJ);
			$ads_to_update['images'] = self::db()->fetchAll('
				SELECT 
					cai.ad_id, cai.image_id AS id, i.hash, i.ext, i.is_portrait, i.width, i.height
				FROM classified_ads_images cai
				INNER JOIN images i ON i.id = cai.image_id
				WHERE cai.ad_id ' . $in
			, array(), Zend_Db::FETCH_OBJ);	
			$ads_to_update['cities'] = self::db()->fetchAll('
				SELECT 
					cac.ad_id, cac.city_id
				FROM classified_ads_cities cac
				WHERE cac.ad_id ' . $in
			, array(), Zend_Db::FETCH_OBJ);
			
		}
		if(count($record_ids)){
			$in = 'IN (' . implode(',', $record_ids) . ')';
			self::db()->query('UPDATE classified_ads_updates SET is_sync_done = 1 WHERE id ' . $in);

		}
		return array('ids_to_update' => $ids_to_update, 'ads_to_update' => $ads_to_update, 'ads_to_remove' => $ads_to_remove);

	}
}
