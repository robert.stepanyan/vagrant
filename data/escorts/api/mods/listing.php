<?php

class Cubix_Api_Module_Listing extends Cubix_Api_Module
{
	
	public function getTableDump()
	{
		return '
			CREATE TABLE `club_directory` (
				`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
				`user_id` int(10) UNSIGNED NULL DEFAULT NULL ,
				`agency_id` int(10) UNSIGNED NULL DEFAULT NULL ,
				`escort_id` int(10) UNSIGNED NULL DEFAULT NULL ,
				`club_name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL ,
				`club_slug`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL ,
				`showname`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL ,
				`city_id` int(10) UNSIGNED NULL DEFAULT NULL ,
				`area_id` int(10) UNSIGNED NULL DEFAULT NULL ,
				
				`disabled_comments` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
				`comments` int(10) UNSIGNED NOT NULL DEFAULT 0,
				
				`area_slug` varchar(50) DEFAULT NULL,
				`city_slug` varchar(50) DEFAULT NULL,

				`city_title_en` varchar(50) DEFAULT NULL,
				`city_title_de` varchar(50) DEFAULT NULL,
				`city_title_it` varchar(50) DEFAULT NULL,
				`city_title_fr` varchar(50) DEFAULT NULL,
				`city_title_gr` varchar(50) DEFAULT NULL,
				`city_title_ro` varchar(50) DEFAULT NULL,
				
				`area_title_en` varchar(50) DEFAULT NULL,
				`area_title_de` varchar(50) DEFAULT NULL,
				`area_title_it` varchar(50) DEFAULT NULL,
				`area_title_fr` varchar(50) DEFAULT NULL,
				`area_title_gr` varchar(50) DEFAULT NULL,
				`area_title_ro` varchar(50) DEFAULT NULL,
				
				`zip` varchar(10) DEFAULT NULL,
				`address` varchar(255) DEFAULT NULL,
				`email` varchar(50) DEFAULT NULL,
				`web` varchar(255) DEFAULT NULL,
				`phone` varchar(50) DEFAULT NULL,
				
				`photo_hash` varchar(255) DEFAULT NULL,
				`photo_ext` varchar(4) DEFAULT NULL,
				
				`latitude` varchar(255) DEFAULT NULL,
				`longitude` varchar(255) DEFAULT NULL,

				`is_premium` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
				`filter_criteria` tinyint(1) UNSIGNED DEFAULT NULL,
				
				`search_string` text,
				
				PRIMARY KEY (`id`),
				UNIQUE KEY `user_id` (`user_id`)
			) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
		';
	}

	public function getTableDumpStandart()
	{
		$about = '';

		if (self::app()->id == APP_AE)
			$about = ' `about_my` text, `about_cn` text, ';
		elseif (self::app()->id == APP_BL)
			$about = ' `about_nl` text, ';
		elseif (self::app()->id == APP_ED) 
			$about = '
				`about_cz` text,
				`about_pl` text,
				`about_sk` text,
			    `allow_show_online` tinyint(1) unsigned DEFAULT "1",
			    `viber` tinyint(1) unsigned DEFAULT 0,
			    `verified_escorts_count` int(10) UNSIGNED NULL DEFAULT 0 ,
			    `whatsapp` tinyint(1) unsigned DEFAULT 0,
			 ';
		elseif (self::app()->id == APP_A6) 
			$about = '
				`about_cz` text,
				`about_hu` text,
				`about_pl` text,
				`about_sk` text,
			    `allow_show_online` tinyint(1) unsigned DEFAULT "1",
			    `viber` tinyint(1) unsigned DEFAULT 0,
			    `entrance` tinyint(1) unsigned DEFAULT 0,
			    `wellness` tinyint(1) unsigned DEFAULT 0,
			    `food` tinyint(1) unsigned DEFAULT 0,
			    `outdoor` tinyint(1) unsigned DEFAULT 0,
			    `verified_escorts_count` int(10) UNSIGNED NULL DEFAULT 0 ,
			    `whatsapp` tinyint(1) unsigned DEFAULT 0,
				`zip` varchar(10) DEFAULT NULL,
			 ';
		elseif (self::app()->id == APP_EG_CO_UK) 
			$about = '
				`allow_show_online` tinyint(1) unsigned DEFAULT "1",
			    `verified_escorts_count` int(10) UNSIGNED NULL DEFAULT 0,			    
			 ';

		return '
			CREATE TABLE `club_directory` (
				`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
				`user_id` int(10) UNSIGNED NULL DEFAULT NULL ,
				`agency_id` int(10) UNSIGNED NULL DEFAULT NULL ,

				`last_modified` timestamp NULL DEFAULT NULL,
				`date_registered` timestamp NULL DEFAULT NULL,
				`is_new`  tinyint(1) UNSIGNED NOT NULL,

				`club_name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL ,
				`club_slug`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL ,

				`escorts_count` int(10) UNSIGNED NULL DEFAULT 0 ,

				`city_id` smallint(5) unsigned DEFAULT NULL,
				`country_id` smallint(5) unsigned DEFAULT NULL,

				`address` varchar(255) DEFAULT NULL,
				`email` varchar(50) DEFAULT NULL,
				`web` varchar(255) DEFAULT NULL,
				`check_website` tinyint(1) unsigned DEFAULT NULL,
				`block_website` tinyint(1) DEFAULT NULL,

				`phone` varchar(50) DEFAULT NULL,
				`contact_phone_parsed` varchar(50) DEFAULT NULL,
				`phone_country_id` smallint(5) unsigned DEFAULT NULL,
				`phone_instr` tinyint(1) unsigned DEFAULT NULL,
				`phone_instructions` varchar(255) DEFAULT NULL,
				`is_anonymous` tinyint(1) DEFAULT 0,
				`phone_1` varchar(50) DEFAULT NULL,
				`contact_phone_parsed_1` varchar(50) DEFAULT NULL,
				`phone_country_id_1` smallint(5) unsigned DEFAULT NULL,
				`phone_instr_1` tinyint(1) unsigned DEFAULT NULL,
				`phone_instructions_1` varchar(255) DEFAULT NULL,
				`is_anonymous_1` tinyint(1) DEFAULT 0,
				`phone_2` varchar(50) DEFAULT NULL,
				`contact_phone_parsed_2` varchar(50) DEFAULT NULL,
				`phone_country_id_2` smallint(5) unsigned DEFAULT NULL,
				`phone_instr_2` tinyint(1) unsigned DEFAULT NULL,
				`phone_instructions_2` varchar(255) DEFAULT NULL,
				`is_anonymous_2` tinyint(1) DEFAULT 0,

				`about_en` text,
				`about_it` text,
				`about_gr` text,
				`about_pt` text,
				`about_fr` text,
				`about_de` text,
				`about_es` text,
				`about_ro` text,
				`about_ru` text,' . $about . '

				`working_times` text,
				`available_24_7` tinyint(1) unsigned DEFAULT NULL,
				`is_open` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,

				`photo_hash` varchar(255) DEFAULT NULL,
				`photo_ext` varchar(4) DEFAULT NULL,

				`latitude` varchar(255) DEFAULT NULL,
				`longitude` varchar(255) DEFAULT NULL,

				`is_premium` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
				`is_international` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
				`hit_count` int(11) NOT NULL DEFAULT 0,

				PRIMARY KEY (`id`),
				UNIQUE KEY `user_id` (`user_id`),
				KEY `agency_id` (`agency_id`),
  				KEY `is_premium` (`is_premium`)
			) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
		';
	}
	
	public function getAgencies($start, $limit = 100)
	{
		set_time_limit(0);

		$start = (int) $start;
		if ( $start < 0 ) $start = 0;
		$limit = (int) $limit;
		if ( $limit < 100 ) $limit = 100;
				
		$agencies = self::db()->fetchAll('
			SELECT 
				IF(op.id IS NULL, 0, 1) AS is_premium,
				a.user_id, a.id AS agency_id, a.display_name AS club_name, a.disabled_comments,
				a.slug AS club_slug, a.fake_city_id AS city_id, a.city_id AS area_id,
				IF(ac.id IS NULL, 0, 1) AS comments,
				' . Cubix_I18n::getTblFields('fc.title', null, false, 'city_title') . ',
				' . Cubix_I18n::getTblFields('c.title', null, false, 'area_title') . ',
				a.zip, a.address, a.email, a.web, a.phone, a.logo_hash AS photo_hash, a.logo_ext AS photo_ext,
				a.latitude, a.longitude, a.filter_criteria,
				c.slug AS area_slug, fc.slug AS city_slug,
				CONCAT(a.name, " ", a.display_name, " ", a.zip, " ", a.address, " ", a.email, " ", a.web, " ", a.phone, " ", fc.' . Cubix_I18n::getTblField('title') . ', " ", c.' . Cubix_I18n::getTblField('title') . ') AS search_string
			FROM agencies a
			INNER JOIN f_cities fc ON fc.id = a.fake_city_id 
			LEFT JOIN cities c ON c.id = a.city_id 
			INNER JOIN escorts e ON e.agency_id = a.id AND e.status = 32
			LEFT JOIN order_packages op ON op.escort_id = e.id AND op.order_id IS NOT NULL AND op.status = 2
			LEFT JOIN agency_comments ac ON ac.agency_id = a.id AND ac.status = 1
			INNER JOIN users u ON u.id = a.user_id AND u.application_id = ' . self::app()->id . '
			WHERE 
				a.show_in_club_directory = 1 AND u.status = 1 AND filter_criteria IS NOT NULL AND filter_criteria <> 0
			GROUP BY a.id
			LIMIT ' . $start . ', ' . $limit . '
		', array(), Zend_Db::FETCH_OBJ);
		
		return $agencies;
	}

    /**
     * @param int $chunk
     * @return mixed
     */
	public function getSearchesWithRedirectUrl($chunk = 0)
    {
        $step = 200;
        $skip = $chunk * $step;
        $take = $step;

        $sql = "SELECT query, redirect_url FROM search_log 
            WHERE redirect_url IS NOT NULL 
            AND redirect_url != ''
            LIMIT $skip, $take";

        return self::db()->fetchAll($sql);
    }

	public function getAgenciesStandart($start, $limit = 100)
	{
		set_time_limit(0);

		$start = (int) $start;
		if ( $start < 0 ) $start = 0;
		$limit = (int) $limit;
		if ( $limit < 100 ) $limit = 100;

		$fields = "";
		if (self::app()->id == APP_ED) {
			$fields = " allow_show_online, a.viber, a.whatsapp, ";
		} else if (self::app()->id == APP_EG_CO_UK) {
			$fields = " allow_show_online, ";
		} else if (self::app()->id == APP_A6) {
			$fields = " a.entrance,a.wellness,a.food,a.outdoor,";
		}
		

		$agencies = self::db()->fetchAll('
			SELECT
				a.is_premium,
				a.user_id, a.id AS agency_id, a.name AS club_name,
				a.slug AS club_slug, COUNT(e.id) AS escorts_count,

				' . Cubix_I18n::getTblFields('a.about', null, false, 'about') . ',
				IF (u.date_registered > (CURDATE() - INTERVAL 3 DAY), 1, 0) AS is_new,
				u.date_registered,

				a.address, a.email, a.web, a.phone, a.contact_phone_parsed, a.phone_country_id, a.phone_instructions, a.is_anonymous, a.phone_instr,
				a.phone_1, a.contact_phone_parsed_1, a.phone_country_id_1, a.phone_instructions_1, a.is_anonymous_1, a.phone_instr_1,
				a.phone_2, a.contact_phone_parsed_2, a.phone_country_id_2, a.phone_instructions_2, a.is_anonymous_2, a.phone_instr_2,
				a.logo_hash AS photo_hash, a.logo_ext AS photo_ext, a.check_website, a.block_website,
				' . $fields . '
				a.latitude, a.longitude, a.available_24_7, a.city_id, a.country_id, a.last_modified, a.hit_count, a.is_international
			FROM agencies a
			LEFT JOIN escorts e ON e.agency_id = a.id AND e.status = 32
			LEFT JOIN order_packages op ON op.escort_id = e.id AND op.order_id IS NOT NULL AND op.status = 2
			INNER JOIN users u ON u.id = a.user_id AND u.application_id = ' . self::app()->id . '
			WHERE
				u.status = 1 ' . (self::app()->id == APP_ED ? " AND a.status = " . AGENCY_STATUS_ONLINE : "") . '
			GROUP BY a.id
			LIMIT ' . $start . ', ' . $limit . '
		', array(), Zend_Db::FETCH_OBJ);

		if ( count($agencies) ) {
			$hour = date('G');
			$day = date('N');

			foreach($agencies as $k => $agency) {

				// 100% verified escorts count
				if (self::app()->id == APP_ED || self::app()->id == APP_EG_CO_UK ) {
					$v_sql = '
						SELECT COUNT(DISTINCT(e.id))
						FROM escorts e
						INNER JOIN users u ON u.id = e.user_id
						WHERE u.status = 1 AND e.verified_status = 2 AND e.status = 32 AND e.agency_id = ? AND u.application_id = ?
					';

					$agencies[$k]->verified_escorts_count = self::db()->fetchOne($v_sql, array($agency->agency_id, self::app()->id));
				}

				$agencies[$k]->working_times = serialize(self::db()->fetchAll('SELECT * FROM agency_working_times WHERE agency_id = ?', array($agency->agency_id)));
				$agencies[$k]->is_open = self::db()->fetchOne('
					SELECT
						IF (
						(
							awt.day_index = ' . $day . '
							AND awt.time_from = 0
							AND awt.time_to = 0
						)
						OR
						(
							awt.day_index = ' . $day . '
							AND awt.time_from <= ' . $hour . '
							AND awt.time_to >= ' . $hour . '
						)
						OR
						(
							awt.day_index = ' . $day . '
							AND awt.time_from <= ' . $hour . '
							AND awt.time_from > awt.time_to
						)
						OR
						(
							awt.day_index = ' . ( 1 == $day ? 7 : $day - 1 ) . '
							AND awt.time_to >= ' . $hour . '
							AND awt.time_from  > awt.time_to
						), 1, 0)
					FROM agency_working_times awt
					WHERE awt.agency_id = ? AND awt.day_index = ?
				', array($agency->agency_id, date('N')));
			}
		}

		return $agencies;
	}
	
	public function getAgenciesStandartA6($start, $limit = 100)
	{
		set_time_limit(0);
		
		/*ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);*/

		$start = (int) $start;
		if ( $start < 0 ) $start = 0;
		$limit = (int) $limit;
		if ( $limit < 100 ) $limit = 100;

		$fields = "";
		if (self::app()->id == APP_A6) {
			$fields = " allow_show_online, a.viber, a.whatsapp, ";
		} else if (self::app()->id == APP_EG_CO_UK) {
			$fields = " allow_show_online, ";
		}

		$agencies = self::db()->fetchAll('
			SELECT
				a.is_premium,
				a.user_id, a.id AS agency_id, a.name AS club_name,
				a.slug AS club_slug, COUNT(e.id) AS escorts_count,

				' . Cubix_I18n::getTblFields('a.about', null, false, 'about') . ',
				IF (u.date_registered > (CURDATE() - INTERVAL 3 DAY), 1, 0) AS is_new,
				u.date_registered,

				a.address, a.email, a.web, a.phone, a.contact_phone_parsed, a.phone_country_id, a.phone_instructions, a.is_anonymous, a.phone_instr,
				a.phone_1, a.contact_phone_parsed_1, a.phone_country_id_1, a.phone_instructions_1, a.is_anonymous_1, a.phone_instr_1,
				a.phone_2, a.contact_phone_parsed_2, a.phone_country_id_2, a.phone_instructions_2, a.is_anonymous_2, a.phone_instr_2,
				a.logo_hash AS photo_hash, a.logo_ext AS photo_ext, a.check_website, a.block_website,
				a.entrance, a.wellness, a.food, a.outdoor, a.zip,
				' . $fields . '
				a.latitude, a.longitude, a.available_24_7, a.city_id, a.country_id, a.last_modified, a.hit_count
			FROM agencies a
			LEFT JOIN escorts e ON e.agency_id = a.id AND e.status = 32
			LEFT JOIN order_packages op ON op.escort_id = e.id AND op.order_id IS NOT NULL AND op.status = 2
			INNER JOIN users u ON u.id = a.user_id AND u.application_id = ' . self::app()->id . '
			WHERE
				u.status = 1 ' . (self::app()->id == APP_ED ? " AND a.status = " . AGENCY_STATUS_ONLINE : "") . '
			GROUP BY a.id
			LIMIT ' . $start . ', ' . $limit . '
		', array(), Zend_Db::FETCH_OBJ);

		if ( count($agencies) ) {
			$hour = date('G');
			$day = date('N');

			foreach($agencies as $k => $agency) {

				// 100% verified escorts count
				if ( self::app()->id == APP_A6 ) {
					$v_sql = '
						SELECT COUNT(DISTINCT(e.id))
						FROM escorts e
						INNER JOIN users u ON u.id = e.user_id
						WHERE u.status = 1 AND e.verified_status = 2 AND e.status = 32 AND e.agency_id = ? AND u.application_id = ?
					';

					$agencies[$k]->verified_escorts_count = self::db()->fetchOne($v_sql, array($agency->agency_id, self::app()->id));
				}

				$agencies[$k]->working_times = serialize(self::db()->fetchAll('SELECT * FROM agency_working_times WHERE agency_id = ?', array($agency->agency_id)));
				$agencies[$k]->is_open = self::db()->fetchOne('
					SELECT
						IF (
						(
							awt.day_index = ' . $day . '
							AND awt.time_from = 0
							AND awt.time_to = 0
						)
						OR
						(
							awt.day_index = ' . $day . '
							AND awt.time_from <= ' . $hour . '
							AND awt.time_to >= ' . $hour . '
						)
						OR
						(
							awt.day_index = ' . $day . '
							AND awt.time_from <= ' . $hour . '
							AND awt.time_from > awt.time_to
						)
						OR
						(
							awt.day_index = ' . ( 1 == $day ? 7 : $day - 1 ) . '
							AND awt.time_to >= ' . $hour . '
							AND awt.time_from  > awt.time_to
						), 1, 0)
					FROM agency_working_times awt
					WHERE awt.agency_id = ? AND awt.day_index = ?
				', array($agency->agency_id, date('N')));
			}
		}

		return $agencies;
	}
	
	public function getEscorts($start, $limit = 100)
	{
		set_time_limit(0);

		$start = (int) $start;
		if ( $start < 0 ) $start = 0;
		$limit = (int) $limit;
		if ( $limit < 100 ) $limit = 100;
				
		$agencies = self::db()->fetchAll('
			SELECT 
				e.user_id, e.id AS escort_id, e.showname,
				e.fake_city_id AS city_id, e.city_id AS area_id, e.disabled_comments, e.comments,
				' . Cubix_I18n::getTblFields('fc.title', null, false, 'city_title') . ',
				' . Cubix_I18n::getTblFields('c.title', null, false, 'area_title') . ',
				e.fake_zip AS zip, CONCAT(ep.street, " ", ep.street_no) AS address, ep.email, ep.website AS web, ep.contact_phone_parsed AS phone, 
				eph.hash AS photo_hash, eph.ext AS photo_ext,
				e.latitude, e.longitude,
				c.slug AS area_slug, fc.slug AS city_slug,
				CONCAT(e.showname, " ", e.fake_zip, " ", ep.street, " ", ep.street_no, " ", ep.email, " ", ep.website, " ", ep.contact_phone_parsed, " ", fc.' . Cubix_I18n::getTblField('title') . ', " ", c.' . Cubix_I18n::getTblField('title') . ') AS search_string
			FROM escorts e
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			INNER JOIN escort_photos eph ON eph.escort_id = e.id AND eph.is_main = 1
			INNER JOIN f_cities fc ON fc.id = e.fake_city_id 
			LEFT JOIN cities c ON c.id = e.city_id 
			INNER JOIN order_packages op ON op.escort_id = e.id AND op.order_id IS NOT NULL AND op.status = 2
			INNER JOIN users u ON u.id = e.user_id AND u.application_id = ' . self::app()->id . '
			WHERE 
				u.status = 1 AND u.user_type = "escort" AND e.status = 32
				AND ep.street IS NOT NULL AND ep.street <> \'\'
			GROUP BY e.id
			LIMIT ' . $start . ', ' . $limit . '
		', array(), Zend_Db::FETCH_OBJ);
		
		return $agencies;
	}
}
