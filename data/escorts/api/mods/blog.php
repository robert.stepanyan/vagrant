<?php

class Cubix_Api_Module_Blog extends Cubix_Api_Module
{
	public function getBlogPostsTableDump()
	{
		$f = '';

		if (Cubix_Application::getId() == APP_ED)
			$f = '
			`title_de` varchar(255) DEFAULT NULL,
			`short_post_de` text,
			`post_de` text,
			`title_es` varchar(255) DEFAULT NULL,
			`short_post_es` text,
			`post_es` text,
			`title_fr` varchar(255) DEFAULT NULL,
			`short_post_fr` text,
			`post_fr` text,
			`title_pt` varchar(255) DEFAULT NULL,
			`short_post_pt` text,
			`post_pt` text,
			`title_ro` varchar(255) DEFAULT NULL,
			`short_post_ro` text,
			`post_ro` text,
			`title_ru` varchar(255) DEFAULT NULL,
			`short_post_ru` text,
			`post_ru` text,
			`title_cz` varchar(255) DEFAULT NULL,
			`short_post_cz` text,
			`post_cz` text,
			`title_sk` varchar(255) DEFAULT NULL,
			`short_post_sk` text,
			`post_sk` text,
			`title_pl` varchar(255) DEFAULT NULL,
			`short_post_pl` text,
			`post_pl` text,
			';

		return '
			CREATE TABLE `blog_posts` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`slug` varchar(255) NOT NULL,
			`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`hash` varchar(255) DEFAULT NULL,
			`ext` varchar(255) DEFAULT NULL,
			`month_count` int(10) unsigned DEFAULT NULL,
			`title_en` varchar(255) DEFAULT NULL,
			`short_post_en` text,
			`post_en` text,
			`title_it` varchar(255) DEFAULT NULL,
			`short_post_it` text,
			`post_it` text,' . $f . '
			`view_count` int(10) unsigned NOT NULL DEFAULT 0,
			`category_id` int(10) unsigned NOT NULL DEFAULT 0,
			`special` tinyint(1) unsigned NOT NULL DEFAULT 0,
			`slider` tinyint(1) unsigned NOT NULL DEFAULT 0,
			PRIMARY KEY (`id`),
			KEY `month_count` (`month_count`)
			) ENGINE=InnoDB  AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
		';
	}

	public function getBlogImagesTableDump()
	{
		return '
			CREATE TABLE `blog_images` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`post_id` int(10) unsigned NOT NULL,
		    `image_id` int(10) unsigned NOT NULL,
		    `hash` varchar(255) DEFAULT NULL,
			`ext` varchar(255) DEFAULT NULL,
		    PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
		';
	}
	
	public function getBlogCommentsTableDump()
	{
		return '
			CREATE TABLE `blog_comments` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`post_id` int(10) unsigned NOT NULL,
			`comment` text NOT NULL,
			`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`name` varchar(255) DEFAULT NULL,
			`email` varchar(255) DEFAULT NULL,
			`is_reply` int(10) unsigned NOT NULL DEFAULT 0,
			`by_admin` tinyint(1) unsigned NOT NULL DEFAULT 0,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
		';
	}
	
	public function getBlogCategoryTableDump()
	{
		return '
			CREATE TABLE `blog_category` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`slug` varchar(255) NOT NULL,
			`title_en` varchar(255) DEFAULT NULL,
			`title_de` varchar(255) DEFAULT NULL,
			`title_es` varchar(255) DEFAULT NULL,
			`title_fr` varchar(255) DEFAULT NULL,
			`title_pt` varchar(255) DEFAULT NULL,
			`title_ro` varchar(255) DEFAULT NULL,
			`title_ru` varchar(255) DEFAULT NULL,
			`title_it` varchar(255) DEFAULT NULL,
			`title_cz` varchar(255) DEFAULT NULL,
			`title_sk` varchar(255) DEFAULT NULL,
			`title_pl` varchar(255) DEFAULT NULL,
			`layout` tinyint(1) DEFAULT NULL,
			`position` tinyint(4) DEFAULT NULL,
			`status` tinyint(1) DEFAULT "1",
			`removed` tinyint(1) DEFAULT "0",
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;
		';
	}

	public function getBlogPosts($start, $limit = 100)
	{
		set_time_limit(0);

		$start = (int) $start;
		if ( $start < 0 ) $start = 0;
		$limit = (int) $limit;
		if ( $limit < 100 ) $limit = 100;

		$f = '';

		if (Cubix_Application::getId() == APP_ED)
			$f = '
			b.title_de, b.short_post_de, b.post_de, b.title_es, b.short_post_es, b.post_es,
			b.title_fr, b.short_post_fr, b.post_fr, b.title_pt, b.short_post_pt, b.post_pt,
			b.title_cz, b.short_post_cz, b.post_cz, b.title_sk, b.short_post_sk, b.post_sk,
			b.title_ro, b.short_post_ro, b.post_ro, b.title_pl, b.short_post_pl, b.post_pl,
			b.title_ru, b.short_post_ru, b.post_ru,';
					
		$posts = self::db()->fetchAll('
			SELECT
				b.id, b.slug,
				b.title_en, b.short_post_en, b.post_en,
				b.title_it, b.short_post_it, b.post_it, ' . $f . '
				b.date, i.hash, i.ext, b.month_count, b.view_count, b.category_id, b.special, b.slider
			FROM blog_posts b
			LEFT JOIN images i ON b.cover_image_id = i.id
			WHERE b.status = ? 
			LIMIT ' . $start . ', ' . $limit, array(1), Zend_Db::FETCH_OBJ);

		return $posts;
	}

	public function getBlogImages($start, $limit = 100)
	{
		set_time_limit(0);

		$start = (int) $start;
		if ( $start < 0 ) $start = 0;
		$limit = (int) $limit;
		if ( $limit < 100 ) $limit = 100;

		$posts = self::db()->fetchAll('
			SELECT
				bi.post_id, bi.image_id, i.hash, i.ext
			FROM blog_images bi
			INNER JOIN blog_posts b ON b.id = bi.post_id
			LEFT JOIN images i ON bi.image_id = i.id
			WHERE b.status = ?
			LIMIT ' . $start . ', ' . $limit, array(1), Zend_Db::FETCH_OBJ);

		return $posts;
	}
	
	public function getBlogComments($start, $limit = 100)
	{
		set_time_limit(0);

		$start = (int) $start;
		if ( $start < 0 ) $start = 0;
		$limit = (int) $limit;
		if ( $limit < 100 ) $limit = 100;
				
		$ads = self::db()->fetchAll('
			SELECT 
				c.id, c.post_id, c.comment, c.date, c.name, c.email, c.is_reply, c.by_admin
			FROM blog_comments c 
			INNER JOIN blog_posts p ON p.id = c.post_id
			WHERE c.status = ? AND p.status = ? 
			LIMIT ' . $start . ', ' . $limit, array(2, 1), Zend_Db::FETCH_OBJ);
		
		return $ads;
	}

	public function getBlogCategory()
	{
		$category = self::db()->fetchAll('
			SELECT * FROM blog_category
			WHERE status = ? AND removed = ?', array(1,0), Zend_Db::FETCH_OBJ);
		return $category;
	}
}
