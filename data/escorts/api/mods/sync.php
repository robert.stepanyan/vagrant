<?php

class Cubix_Api_Module_Sync extends Cubix_Api_Module
{
	public static function getDiffEscorts($start, $limit = 100)
	{
		set_time_limit(0);

		$start = (int) $start;
		if ( $start < 0 ) $start = 0;
		$limit = (int) $limit;
		if ( $limit < 10 ) $limit = 10;

		$whr = "";
		$join = "";
		/*if (self::app()->id == APP_ED) {
			$whr .= " AND e.sync_escort = 0 AND u.sales_user_id <> 96 ";	
		}*/

		if (self::app()->id == APP_ED)
		{
			$whr .= ' AND (ag.status = ' . AGENCY_STATUS_ONLINE . ' OR ag.status IS NULL ) ';
			$join .= ' LEFT JOIN agencies ag ON e.agency_id = ag.id';
		}
		
		$paid_package_apps = array(APP_EF, APP_A6);
		$paid_join = ' ';
		$paid_where = ' ';
		if ( in_array(self::app()->id, $paid_package_apps) ) {

			$zero_package_where = ' ';
			if ( self::app()->id == APP_EF ) {
				$zero_package_where = ' AND op.package_id <> 100 ';
			}

			$paid_join = ' INNER JOIN order_packages op ON op.escort_id = e.id ';
			$paid_where = ' AND e.status & 32 ' . $zero_package_where . ' AND op.`status` = 2 AND op.order_id IS NOT NULL ';
		}

		// <editor-fold defaultstate="collapsed" desc="Find Updated Escorts">
		$escorts = self::db()->fetchAll('
			SELECT e.id, e.showname, e.status
			FROM journal j
			INNER JOIN escorts e ON e.id = j.escort_id
			'. $join .'
			INNER JOIN users u ON u.id = e.user_id AND u.application_id = ' . self::app()->id . '
			WHERE j.is_sync_done = 0 ' . $whr . '
			GROUP BY e.id
			LIMIT ' . $start . ', ' . $limit . '
		', array(), Zend_Db::FETCH_OBJ);

		
		$updated_escorts = $updated_escort_ids = array();
		$removed_escorts = array();
		$escortHasNotStatusBit = array(
            Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED,
            Cubix_EscortStatus::ESCORT_STATUS_ADMIN_DISABLED,
            Cubix_EscortStatus::ESCORT_STATUS_NO_ENOUGH_PHOTOS,
            Cubix_EscortStatus::ESCORT_STATUS_DELETED,
            Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION,
        );
		if (in_array(self::app()->id, array(APP_EG_CO_UK, APP_ED,/*APP_EF, APP_AE, APP_BL, APP_6B*/))){
		    array_push($escortHasNotStatusBit,Cubix_EscortStatus::ESCORT_STATUS_OFFLINE);
        }
		foreach ( $escorts as $i => $escort ) {
			$status = new Cubix_EscortStatus($escort->id, $escort->status);
			if ( $status->hasStatusBit(array(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE)) &&
				! $status->hasStatusBit($escortHasNotStatusBit)
			) {
				$updated_escorts[] = array($escort->id, $escort->showname, $escort->status);
				$updated_escort_ids[] = $escort->id;
			}
			else {
				$removed_escorts[] = array($escort->id, $escort->showname);
			}
		}
		// </editor-fold>

		// Fetch escort data only when there are updated escorts
		
		$and6_fields = "";
		$about_soft = "";
		
		$hits_count = "
			(
				SELECT
					SUM(count)
				FROM escort_hits_daily
				WHERE escort_id = e.id AND date >= DATE_ADD(DATE(NOW()), INTERVAL -30 DAY)
			) AS hit_count,";
		
		if ( self::app()->id == APP_A6 || self::app()->id == APP_A6_AT ) {
			$and6_fields = " 
				, ag.city_id AS a_city_id, ag.fake_city_id AS a_fake_city_id, 
				ag.zip AS a_zip, ag.address AS a_address, ag.name AS a_name, 
				ag.slug AS a_slug, e.fake_city_id AS f_city_id, e.fake_zip AS f_zip, 
				ag.is_anonymous AS a_is_anonymous, ep.about_ro,
				ag.email AS a_email, ag.phone AS a_phone, ag.phone_instructions AS a_phone_instructions, 
				ag.phone_country_id AS a_phone_country_id
			";
								
			$hits_count = "
			(
				SELECT
					SUM(count)
				FROM escort_hits_daily
				WHERE escort_id = e.id
			) AS hit_count,";
			
			/*$hits_count = " 0 AS hit_count ";*/
			
			
		}
		
		if(self::app()->id == 1 || self::app()->id == 2){
			$about_soft ="
				 ep.about_soft_en ,
				 ep.about_soft_it ,
				 ep.about_soft_gr ,
				 ep.about_soft_fr ,
				 ep.about_soft_de ,
				 ep.about_soft_pt ,
				 ep.about_soft_nl ,
				 ep.about_soft_my ,
				 ep.about_soft_cn ,
				 e.gallery_is_checked,
			";
		}
		
		$is_vip = 'e.is_vip,';
		$is_premium = '';

		$wh = "";
		
		if (self::app()->id == APP_ED)
		{
            $paymentsDisabledCountries = [23 /* France */, 68 /* US */];
			$is_premium = 'e.is_premium,';
			$is_vip = '
				IF((SELECT TRUE FROM order_packages op 
				INNER JOIN order_package_products opp ON op.id = opp.order_package_id
				WHERE op.order_id IS NOT NULL AND op.`status` = 2 AND op.escort_id = e.id AND opp.product_id = 17
				AND e.country_id NOT IN (' . implode(',', $paymentsDisabledCountries) . ')
				), 1, 0) AS is_vip,
			';
			
			$wh .= ' AND (ag.status = ' . AGENCY_STATUS_ONLINE . ' OR ag.status IS NULL ) ';

			//premium packages ids
			$is_premium = '
				IF((SELECT TRUE FROM order_packages op 
				WHERE op.order_id IS NOT NULL AND op.`status` = 2 AND op.escort_id = e.id 
				AND op.package_id IN(114, 115, 116, 117, 118, 119) 
				AND e.country_id NOT IN (' . implode(',', $paymentsDisabledCountries) . ')
				), 1, 0) AS is_premium,
			';

		}		
		
		
		if (in_array(self::app()->id, array(APP_6B, APP_6C, APP_EG, APP_EG_CO_UK, APP_ED)))
		{
			$wh .= " AND (e.activation_date IS NULL OR DATE(e.activation_date) <= CURDATE()) ";
		}

		$fields = " ";
		$joins = " ";
		if ( self::app()->id == APP_ED ) {
			$fields .= "
				ep.about_es,
				ep.about_ro,
				ep.about_ru,
				vr.date AS date_last_verified,
				ep.email,
	  			ep.website,
	  			ep.phone_instr,
				ep.phone_instr_no_withheld,
				ep.phone_instr_other,
				ep.phone_country_id,
				ep.disable_phone_prefix,
				ep.phone_number,
				ep.contact_phone_free AS phone_number_free,
				e.status_active_date,
				e.last_hand_verification_date,
				IF ( NOT e.agency_id IS NULL, 1, 0 ) AS is_agency,
				ep.tatoo,
				ep.piercing,
				ep.wechat,
				ep.telegram,
				ep.ssignal,
			";

			$joins = " LEFT JOIN verify_requests vr ON vr.escort_id = e.id AND vr.`status` = 2 AND vr.type = 2 AND vr.id = (SELECT MAX(id) FROM verify_requests WHERE escort_id = vr.escort_id) ";
		}

		if ( self::app()->id == APP_EG_CO_UK ) {
			$fields .= "
				e.last_hand_verification_date,
				ep.telegram,
				ep.ssignal,
			";
		}

		if ( self::app()->id == APP_EG ) {
			$fields .= "
				e.last_hand_verification_date,
				ep.telegram,
			";
		}
		if ( self::app()->id == APP_6C ) {
			$fields .= "
				e.last_hand_verification_date,
			";
		}

		if ( self::app()->id == APP_6B ) {
			$fields .= "
				e.last_hand_verification_date,
				ep.telegram,
				ep.ssignal,
			";
		}

        if (in_array(self::app()->id, array(APP_AE, APP_EM))) {
            $fields .= "
				ep.telegram,
				ep.ssignal,
			";
        }

		if ( self::app()->id == APP_EF ) {
			$fields .= "
				ep.phone_instr, e.skype_call_status,
				ep.telegram,
			";
		}

		if ( self::app()->id == APP_A6 ) {
			$fields .= "
				e.pseudo_escort,
			 	e.cam_booking_status, 
			 	ep.telegram,
			";
		}
		
		if ( in_array(self::app()->id, array(APP_EF, APP_A6, APP_BL))) {
			$fields .= "
				e.cam_status, e.cam_preview_url, e.cam_channel_url, 
			";
		}
		
		if ( in_array(self::app()->id, array(APP_EF, APP_ED, APP_A6, APP_EG_CO_UK, APP_6B, APP_BL))) {
			$fields .= "
				IF((SELECT TRUE FROM video v WHERE v.escort_id = e.id AND v.status = 'approve' LIMIT 1 ), 1, 0) AS has_video,
			";
		}
		
		if ( self::app()->id == APP_EF || self::app()->id == APP_ED ) {
			$is_new =	' IF ((e.date_registered > (CURDATE() - INTERVAL ' . self::config()->newIntervalInDays . ' DAY)) AND (e.mark_not_new = 0), 1, 0) AS is_new, ';
		}else{
			$is_new .=	' IF (e.date_registered > (CURDATE() - INTERVAL ' . self::config()->newIntervalInDays . ' DAY), 1, 0) AS is_new, ';
		}

        if ( self::app()->id == APP_BL ) {
            $fields .= "
				e.is_pornstar,
				ep.telegram,
				ep.ssignal,
				e.cam_booking_status,
			";
        }

		if ( self::app()->id == APP_EI ) {
            $fields .= "
	  			ep.phone_instr,
				ep.phone_instr_no_withheld,
				ep.phone_instr_other,
				ep.phone_country_id,
				ep.disable_phone_prefix,
				ep.phone_number,
				ep.telegram,
			";
		}

        if (self::app()->id == APP_6B)
        {
            $fields .= "
	  			ep.twitter_name,
			";
        }

		$aboutAPP_A6 = '';
		if(self::app()->id == APP_A6){
			$aboutAPP_A6 = ' ep.about_es,ep.about_hu, ';
		}
                
               if ( self::app()->id == APP_BL ) {
			$fields .= "
	  			ep.snapchat,
				ep.snapchat_username,
			";
		}

        //New inactive status
        if (in_array(self::app()->id, array(APP_ED, APP_EG_CO_UK))){
            $fields .=" IF(e.status & ".ESCORT_STATUS_INACTIVE.", 1, 0) as is_inactive, ";
        }

		$result = array();
		if ( count($updated_escort_ids) > 0 ) {
			// <editor-fold defaultstate="collapsed" desc="Main Profile Select">
			$sql = '
				SELECT
					e.id,
					u.id AS user_id,
					e.agency_id,
					e.showname,
					u.username,
					es.text AS slogan,
					e.gender,
					e.country_id,
					e.city_id,
					e.cityzone_id,
					e.verified_status,
					e.is_suspicious,
					e.suspicious_comment,
					e.susp_comment_show,
					e.type,
					e.master_id,
					
					0 AS review_count,
					0 AS comment_count,
					e.block_website,
					e.check_website,
					e.is_pornstar,
					e.bad_photo_history,
					ep.ethnicity,
					ep.nationality_id,
					/*FLOOR(DATEDIFF(CURDATE(), ep.birth_date) / 365.25) AS age,*/
					/*DATEDIFF(CURDATE(), ep.birth_date) / 365.25 AS age,*/
					ep.birth_date,
					ep.eye_color,
					ep.hair_color,
					ep.hair_length,
					ep.height,
					ep.weight,
					ep.cup_size,
					ep.breast_type,
					ep.dress_size,
					ep.shoe_size,
					ep.pubic_hair,
					ep.is_smoking,
					ep.is_drinking,

					ep.cuisine,
					ep.drink,
					ep.flower,
					ep.gifts,
					ep.perfume,
					ep.designer,
					ep.interests,
					ep.sports,
					ep.hobbies,
					
					ep.min_book_time,
					ep.min_book_time_unit,
					
					ep.has_down_pay,
					ep.down_pay,
					ep.down_pay_cur,
					
					ep.reservation,

					ep.zip,
					ep.incall_type,
					ep.incall_hotel_room,
					ep.incall_other,
					ep.outcall_type,
					ep.outcall_other,

					ep.sex_orientation,
					ep.sex_availability,
					e.is_open AS is_now_open,
					IF ( NOT t.id IS NULL, 1, 0 ) AS is_on_tour,
					IF ( NOT t.id IS NULL, t.id, NULL ) AS tour_id,
					IF ( NOT t.id IS NULL, t.city_id, NULL ) AS tour_city_id,

					'.$is_new.'

					e.date_registered,
					e.date_last_modified,
					u.last_login_date,
					eph.hash AS photo_hash,
					eph.ext AS photo_ext,
					eph.status AS photo_status,
					eph.args AS photo_args,

					t.date_from AS tour_date_from,
					t.date_to AS tour_date_to,
					t.minutes_from,
					t.minutes_to,
					t.phone AS tour_phone,
					e.vac_date_from,
					e.vac_date_to,
					' . $hits_count . '
					ep.available_24_7,
					(SELECT package_id FROM order_packages WHERE escort_id = e.id AND status = ' . PACKAGE_STATUS_ACTIVE . ' LIMIT 1) AS package_id,
					ep.night_escort,
					ag.name AS agency_name,
					ag.slug AS agency_slug,
					eb.text AS bubble_text,
					e.votes_sum,
					e.votes_count,
					e.voted_members,
					e.fuckometer,
					e.fuckometer_rank,
					e.show_popunder,
					gotm.month AS gotm_month,
					gotm.year AS gotm_year,

					IF(e.hh_status = ' . HH_STATUS_ACTIVE . ' AND e.has_hh = 1, 1, 0) AS hh_is_active,
					e.hh_date_from,
					e.hh_date_to,
					e.hh_motto,

					ep.about_en,
					ep.about_it,
					ep.about_gr,
					ep.about_fr,
					ep.about_de,
					ep.about_pt,
					ep.about_nl,
					ep.about_my,
					ep.about_cn,
					' .$aboutAPP_A6. '
					' . $fields . '
					ep.contact_phone_parsed,
					'. $about_soft .'
					ep.special_rates,
					'. $is_vip .'
					'. $is_premium .'
					IF (ag.id IS NULL OR ag.disabled_reviews = 0, e.disabled_reviews, ag.disabled_reviews) AS disabled_reviews,
					IF (ag.id IS NULL OR ag.disabled_comments = 0, e.disabled_comments, ag.disabled_comments) AS disabled_comments,
					ep.travel_place,
					ag.block_website AS a_block_website,
					e.latitude,
					e.longitude,
					u.allow_show_online,
					ep.phone_additional_1,
					ep.phone_additional_2,
					ep.phone_country_id_1,
					ep.phone_country_id_2,
					ep.disable_phone_prefix_1,
					ep.disable_phone_prefix_2,
					ep.phone_instr_1,
					ep.phone_instr_2,
					ep.phone_instr_no_withheld_1,
					ep.phone_instr_no_withheld_2,
					ep.phone_instr_other_1,
					ep.phone_instr_other_2,
					ep.viber,
					ep.whatsapp,
					ep.viber_1,
					ep.whatsapp_1,
					ep.viber_2,
					ep.whatsapp_2,
					ag.web AS a_web
					' . $and6_fields . '
				FROM escort_profiles_v2 ep

				INNER JOIN escorts e ON ep.escort_id = e.id
				INNER JOIN users u ON u.id = e.user_id

				LEFT JOIN agencies ag ON ag.id = e.agency_id

				LEFT JOIN escort_bubbletexts eb ON eb.escort_id = e.id AND eb.status = 1 AND eb.approvation = 1
				LEFT JOIN escort_slogans es ON es.escort_id = e.id AND es.status = 1 
				LEFT JOIN girl_of_month gotm ON gotm.escort_id = e.id AND gotm.was_gotm = 0

				LEFT JOIN escort_working_times ewt ON ewt.escort_id = e.id

				INNER JOIN escort_cities ec ON ec.escort_id = e.id
				INNER JOIN countries c ON c.id = e.country_id
				INNER JOIN cities cc ON cc.id = ec.city_id

				INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1

				LEFT JOIN tours t ON t.escort_id = e.id AND DATE(t.date_from) <= CURDATE() AND DATE(t.date_to) >= CURDATE()

				' . $joins . '

				WHERE e.id IN (' . implode(', ', $updated_escort_ids) . ') ' . $wh .' 
					/*' . ( self::app()->id == 9 ? 'AND (SELECT COUNT(*) FROM escort_photos WHERE escort_id = e.id AND type = 2) > 0' : '') . '*/
				GROUP BY e.id
			';
			// </editor-fold>

			$escorts = self::db()->fetchAll($sql);
			
			$kk = 0;
			foreach ( $escorts as $i => $escort ) {
				$kk ++;
				$row = array();
				
				if ( self::app()->id == APP_A6 || self::app()->id == APP_A6_AT ) {
					$b_photos = self::db()->fetchAll('SELECT width, height FROM escort_photos WHERE escort_id = ?', array($escort['id']));

					$has_big_photos = 1;
					$min_sidesize = 600;

					if ( count($b_photos) ) {
						foreach( $b_photos AS $ph ) {
							if ( $ph['width'] < $min_sidesize || $ph['height'] < $min_sidesize ) {
								$has_big_photos = 0;
								break;
							}
						}
					}

					$escort['has_big_photos'] = $has_big_photos;
				}
				
				// Fetch escort spoken languages
				$langs = array();
				foreach ( self::db()->fetchAll('SELECT * FROM escort_langs WHERE escort_id = ?', $escort['id']) as $lang ) {
					$langs[] = $lang['lang_id'];
				}

				// Reviews count
				$r_c = self::db()->fetchOne('
					SELECT COUNT(r.id)
					FROM reviews r
					INNER JOIN users u ON u.id = r.user_id
					WHERE r.escort_id = ? AND r.status = 2 AND is_deleted = 0 AND u.status = 1
				', $escort['id']);
				$escort['review_count'] = $r_c;
				// Reviews count

				// Comments count
				$c_c = self::db()->fetchOne('
					SELECT COUNT(c.id)
					FROM comments c
					INNER JOIN escorts e ON e.id = c.escort_id
					INNER JOIN escort_photos ep ON ep.escort_id = e.id AND ep.is_main = 1
					INNER JOIN users u ON u.id = c.user_id
					WHERE c.escort_id = ? AND c.status = 1 AND (e.status & ' . ESCORT_STATUS_ACTIVE  . '
						AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . '
						AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . '
						AND NOT e.status & ' . ESCORT_STATUS_DELETED . ') AND u.status = ' . USER_STATUS_ACTIVE . '
				', $escort['id']);
				$escort['comment_count'] = $c_c;
				// Comments count

				// Age
				$escort['age'] = Cubix_Utils::dateToAge($escort['birth_date']);
				unset($escort['birth_date']);
				// Age

				// Blacklisted countries
				$escort_blocked_countries = self::db()->fetchAll('
					SELECT country_id
					FROM escort_blocked_countries				
					WHERE escort_id = ?
				', array($escort['id']));
				
				$escort_blocked_countries_arr = array();
				foreach ( $escort_blocked_countries as $bl_country ) {
					$escort_blocked_countries_arr[] = $bl_country['country_id'];
				}

				$escort['blacklisted_countries'] = $escort_blocked_countries_arr;
				// Blacklisted countries

				$t = self::db()->fetchOne('SELECT time_to FROM escort_working_times WHERE escort_id = ? AND day_index = ?', array($escort['id'], date('N')));
				if (strlen($t) > 0)
					$escort['close_time'] = $t;
				else
					$escort['close_time'] = null;

				$escort['languages'] = implode(',', $langs);

				// Fill random ordering to not ORDER BY RAND()
				$escort['ordering'] = rand(10000, 50000);

				$row['profile'] = $escort;

				// Fill escort rates find the price value for sorting
				self::_fillEscortRates($row);
				
				if ( in_array(self::app()->id, array(APP_ED, 33, 22)) ) {
					self::_fillEscortTravelPaymentMethods($row);
					//self::_fillEscortTravelCountries($row);
					self::_fillEscortTravelContinents($row);
				}

				// Fill escorts products with some shorthand fields
				self::_fillEscortProducts($row);

				self::_fillEscortUpcomingTours($row);

				self::_fillEscortServices($row);
				
				self::_fillEscortKeywords($row);

				self::_fillEscortCities($row);

				self::_fillEscortCityZones($row);

				self::_fillEscortPhotos($row);

				if ( in_array(self::app()->id, array(APP_ED, APP_EF, APP_EG_CO_UK, APP_EG, APP_6B, APP_6C, APP_EM, APP_BL, APP_AE)) ) {
					self::_fillEscortPhotoVotes($row);
				}
				
				self::_fillEscortVideos($row);
				if ( in_array(self::app()->id, array(APP_BL, APP_EF, APP_ED )) ) {
					self::_fillEscortNaturalPics($row);
					self::_fillEscortGalleries($row);
				}
				if ( in_array(self::app()->id, array(APP_BL, APP_EF, APP_ED,APP_EG_CO_UK )) ) {
					self::_fillEscortEuroLottery($row);
				}
				self::_fillEscortProfiles($row);

				self::_fillEscortWorkingTimes($row);

				if ( self::app()->id == APP_A6 || self::app()->id == APP_A6_AT ) {
					self::_fillEscortAdditionalAreas($row);
				}

				
				if (self::app()->id == APP_6A)
				{
					self::_fillEscortVipRequest($row);
				}
				
				$result[] = $row;
			}           
		}


		
		return array('removed' => $removed_escorts, 'updated' => $result);
	}

	/*public static function markEscortsDone(array $escort_ids)
	{
		if ( 0 == count($escort_ids) ) return true;
		self::db()->update('journal', array(
			'is_sync_done' => 1,
			'date_done' => new Zend_Db_Expr('NOW()')
		), 'escort_id IN (' . implode(', ', $escort_ids) . ')');

		return true;
	}*/

	public static function markEscortsDone(array $escort_ids, $sync_start_date)
	{
		
		try{
			if ( 0 == count($escort_ids) ) return true;
			
//			self::db()->update('journal', array(
//				'is_sync_done' => 1,
//				'date_done' => new Zend_Db_Expr('NOW()')
//			), self::db()->quoteInto(array('escort_id IN (' . implode(', ', $escort_ids) . ')', 'is_sync_done = 0', 'UNIX_TIMESTAMP(date) < ' . $sync_start_date)));
//			
			self::db()->update('journal', array(
				'is_sync_done' => 1,
				'date_done' => new Zend_Db_Expr('NOW()')
			), array( 'escort_id IN (' . implode(', ', $escort_ids) . ')' , 'is_sync_done = 0', 'UNIX_TIMESTAMP(date) < ' . $sync_start_date ));
			}
		catch (Exception $e) { 
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return true;
	}

	const PRODUCT_NATIONAL_LISTING			= 1;
	const PRODUCT_INTERNATIONAL_DIRECTORY	= 2;
	const PRODUCT_GIRL_OF_THE_MONTH			= 3;
	const PRODUCT_MAIN_PREMIUM_SPOT			= 4;
	const PRODUCT_CITY_PREMIUM_SPOT			= 5;
	const PRODUCT_TOUR_PREMIUM_SPOT			= 6;
	const PRODUCT_TOUR_ABILITY				= 7;
	const PRODUCT_NO_REVIEWS				= 8;
	const PRODUCT_ADDITIONAL_CITY			= 9;
	const PRODUCT_SEARCH					= 10;

	private static function _fillEscortProducts(&$row)
	{
		$escort_id = $row['profile']['id'];
		
		$new_flag_projects = array(APP_6A, APP_EF, APP_A6, APP_A6_AT);
			
		if ( in_array(Cubix_Application::getId(), $new_flag_projects) ) {

			$package = self::db()->fetchRow('
				SELECT
					COUNT(op.id) AS count
				FROM order_packages op
				WHERE op.escort_id = ? AND op.application_id = ? AND op.order_id IS NOT NULL AND op.status <> 4
			', array($escort_id, Cubix_Application::getId()));

			$first_package = true;
			if ( $package['count'] > 1 ) {
				$first_package = false;
			}

			$inactive_days_60 = false;
			$package = self::db()->fetchRow('
				SELECT
					DATEDIFF(CURDATE(), op.expiration_date) AS inactive_days
				FROM order_packages op
				WHERE op.escort_id = ? AND op.application_id = ? AND op.order_id IS NOT NULL AND op.status = 3 AND op.expiration_date IS NOT NULL
				ORDER BY op.expiration_date DESC
				LIMIT 1
			', array($escort_id, Cubix_Application::getId()));

			if ( $package['inactive_days'] >= 60 ) {
				$inactive_days_60 = true;
			}
			
			$date_field = 'op.date_activated';
				
			$is_moved_package = self::db()->fetchRow('
				SELECT
					IF (op.date_activated < e.date_registered, 1, 0) AS is_moved_package
				FROM order_packages op
				INNER JOIN escorts e ON e.id = op.escort_id
				WHERE op.escort_id = ? AND op.application_id = ? AND op.status = ?
			', array($escort_id, Cubix_Application::getId(), PACKAGE_STATUS_ACTIVE));

			if ( $is_moved_package['is_moved_package'] ) {
				$date_field = 'e.date_registered';
			}			
			
			if (Cubix_Application::getId() == APP_EF)
			{
				$is_newSql = ' IF ((' . $date_field . ' > (CURDATE() - INTERVAL ' . self::config()->newIntervalInDays . ' DAY)) AND (e.mark_not_new = 0), 1, 0) AS is_new ';
			}else{
				$is_newSql = ' IF (' . $date_field . ' > (CURDATE() - INTERVAL ' . self::config()->newIntervalInDays . ' DAY), 1, 0) AS is_new ';
			}

			$is_new_by_package = self::db()->fetchRow('
				SELECT
					op.date_activated, '. $is_newSql .'
				FROM order_packages op
				INNER JOIN escorts e ON e.id = op.escort_id
				WHERE op.escort_id = ? AND op.application_id = ? AND op.status = ?
			', array($escort_id, Cubix_Application::getId(), PACKAGE_STATUS_ACTIVE));
			 
			if ( $is_new_by_package ) 
			{
				$row['profile']['date_activated'] = $is_new_by_package['date_activated'];
			}
			
			if ( $first_package || $inactive_days_60 ) 
			{
				if ( $is_new_by_package ) {
					$row['profile']['is_new'] = $is_new_by_package['is_new'];
				}
			}

            if (Cubix_Application::getId() == APP_EF)
            {
                $has_active_moved_package = self::db()->fetchOne('
                    SELECT
                            TRUE 
                        FROM
                            packages_moving_history pmh
                            INNER JOIN order_packages op ON op.id = pmh.order_package_id 
                        WHERE
                            ( pmh.to_escort_id = ? ) 
                            AND op.STATUS <> ? 
                            AND pmh.date = (
                            SELECT
                                MAX( date ) 
                            FROM
                                packages_moving_history 
                            WHERE
                                to_escort_id = ? 
                            OR from_escort_id = ? 
                            )
                            AND pmh.date > ( CURDATE() - INTERVAL 60 DAY )
				    ', array($escort_id,PACKAGE_STATUS_ACTIVE,$escort_id,$escort_id));



                if ($has_active_moved_package){
                    $row['profile']['is_new'] = 0;
                }else{
                    $has_moved_spent_package = self::db()->fetchOne('
                    SELECT TRUE 
                        FROM
                            packages_moving_history pmh
                            INNER JOIN order_packages op ON op.id = pmh.order_package_id
                            INNER JOIN packages_moving_history pmh2 ON pmh.order_package_id = pmh2.order_package_id 
                            AND pmh2.to_escort_id = ? 
                        WHERE
                            pmh.from_escort_id = ? 
                            AND pmh.date = ( SELECT MAX( date ) FROM packages_moving_history WHERE to_escort_id = ? OR from_escort_id = ? ) 
                            AND DATEDIFF( pmh.date, pmh2.date ) > 60
				    ', array($escort_id,$escort_id,$escort_id,$escort_id));

                    if ($has_moved_spent_package)
                    {
                        $row['profile']['is_new'] = 0;
                    }
                }



            }

			/*$is_new_by_package = self::db()->fetchRow('
				SELECT
					op.date_activated, 
					IF (op.date_activated > (CURDATE() - INTERVAL ' . self::config()->newIntervalInDays . ' DAY), 1, 0) AS is_new_activated,
					IF (e.date_registered > (CURDATE() - INTERVAL ' . self::config()->newIntervalInDays . ' DAY), 1, 0) AS is_new_registered
				FROM order_packages op
				INNER JOIN escorts e ON e.id = op.escort_id
				WHERE op.escort_id = ? AND op.application_id = ? AND op.status = ?
			', array($escort_id, Cubix_Application::getId(), PACKAGE_STATUS_ACTIVE));
			
			if ( $is_new_by_package ) {
				$row['profile']['date_activated'] = $is_new_by_package['date_activated'];
			}
			
			if ( $is_new_by_package && ( $first_package || $inactive_days_60 ) ) {
				$row['profile']['is_new'] = 1;
			}*/

		} elseif(in_array(Cubix_Application::getId(), array(APP_BL))) {		
		
			if (Cubix_Application::getId() == APP_ED){
				$is_new_sql = ' IF ((op.date_activated > (CURDATE() - INTERVAL ' . self::config()->newIntervalInDays . ' DAY)) AND (e.mark_not_new = 0), 1, 0) AS is_new ';
			}
			else{
				$is_new_sql = ' IF (op.date_activated > (CURDATE() - INTERVAL ' . self::config()->newIntervalInDays . ' DAY), 1, 0) AS is_new ';
			}
			
			$is_new_by_package = self::db()->fetchRow('
				SELECT
					op.date_activated, '. $is_new_sql .'
				FROM order_packages op
				INNER JOIN escorts e ON e.id = op.escort_id
				WHERE op.escort_id = ? AND op.application_id = ? AND op.status = ? AND op.order_id IS NOT NULL
			', array($escort_id, Cubix_Application::getId(), PACKAGE_STATUS_ACTIVE));

			if ( $is_new_by_package ) {
				$row['profile']['is_new'] = $is_new_by_package['is_new'];
				$row['profile']['date_activated'] = $is_new_by_package['date_activated'];
			}
					
		}
		
		// <editor-fold defaultstate="collapsed" desc="Get Escort Products">
		// Get all products of this escort
		$order_package_id = null;
		
		$products = self::db()->fetchAll('
			SELECT
				op.id AS order_package_id,
				opp.product_id, op.gotd_status, op.gotd_city_id
			FROM
				order_package_products opp
			INNER JOIN order_packages op ON op.id = opp.order_package_id
			WHERE op.escort_id = ? AND op.application_id = ? AND op.status = ?
		', array($escort_id, self::app()->id, PACKAGE_STATUS_ACTIVE));

		// If there are no products in order_package_products
		// try to get from base package_products table
		if ( 0 == count($products) ) {
			$products = self::db()->fetchAll('
				SELECT
					op.id AS order_package_id,
					pp.product_id, op.gotd_status, op.gotd_city_id
				FROM order_packages op
				INNER JOIN package_products pp ON pp.package_id = op.package_id
				WHERE op.escort_id = ? AND op.application_id = ? AND op.status = ? AND pp.is_optional = 0
			', array($escort_id, self::app()->id, PACKAGE_STATUS_ACTIVE));
		}
		// </editor-fold>
		
		/**/
		if (self::app()->id == APP_BL)
		{
			$ops = self::db()->query('
				SELECT status 
				FROM order_packages 
				WHERE escort_id = ? AND application_id = ? AND order_id IS NOT NULL
			', array($escort_id, self::app()->id))->fetchAll();

			if (count($ops))
			{
				$not_display = true;

				foreach ($ops as $op)
				{
					if (in_array($op['status'], array(PACKAGE_STATUS_ACTIVE, 5, 6))) //active, upgraded, suspended
					{
						$not_display = false;
						break;
					}
				}
				
				if ($not_display && count($products)) 
				{
					$dsp = self::db()->fetchOne('SELECT display_after_package_expire FROM escorts WHERE id = ?', $escort_id);
					
					if (!$dsp)
					{
						$pr = $products[0];
						$new_pr = array('order_package_id' => $pr['order_package_id'], 'product_id' => 77, 'gotd_status' => null, 'gotd_city_id' => null); // 77 - dont_display
						$products[] = $new_pr;
					}
				}
			}
		}
		/**/

		if ( count($products) > 0 ) {
			// Just transform to flat array
			$order_package_id = $products[0]['order_package_id'];
			foreach ($products as $k => $product) {
				// Girl of the day
				if ( (Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT || Cubix_Application::getId() == APP_6A || Cubix_Application::getId() == APP_EF) && 
						$product['gotd_status'] != 2 && $product['product_id'] == 15 ) {
					unset($products[$k]);
				} else if ( in_array(Cubix_Application::getId(),array(APP_BL,APP_ED)) && $product['gotd_status'] != 2 && $product['product_id'] == 16 ) {
					unset($products[$k]);
				}
				else if ( Cubix_Application::getId() == APP_EG_CO_UK && $product['gotd_status'] != 2 && $product['product_id'] == 18 ) {
					unset($products[$k]);
				}
				else {
					$products[$k] = $product['product_id'];
					$row['profile']['gotd_city_id'] = $product['gotd_city_id'];
				}
			}
			$row['products'] = $products;
			
			// If escort has PRODUCT_CITY_PREMIUM_SPOT fetch also her
			// premium cities
			if ( in_array(self::PRODUCT_CITY_PREMIUM_SPOT, $products) ) {
				$p_cities = self::db()->fetchAll('
					SELECT
						pe.city_id
					FROM premium_escorts pe
					INNER JOIN order_packages op ON op.id = pe.order_package_id
					WHERE pe.order_package_id = ?
				', $product['order_package_id']);
				$premiums = array();
				foreach ( $p_cities as $i => $city ) {
					$premiums[] = (int) $city['city_id'];
				}
				if ( count($premiums) > 0 ) {
					$row['premiums'] = $premiums;
				}
			}

			// Some shorthand flags in escorts table
			$row['profile'] += array(
				'is_main_premium_spot' => (int) in_array(self::PRODUCT_MAIN_PREMIUM_SPOT, $products),
				'is_tour_premium' => (int) in_array(self::PRODUCT_TOUR_PREMIUM_SPOT, $products)
			);
			
			if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {
				$row['order_package_id'] = $order_package_id;
			}
		}
		
	}

	private static function _fillEscortRates(&$row)
	{
		$rates = self::db()->fetchAll('
			SELECT
				availability, type, time, time_unit, price, currency_id, hh_price, plus_taxi
			FROM escort_rates
			WHERE escort_id = ? AND availability <> 0
		', $row['profile']['id']);

        // Store incall and outcall prices into array while looping trough them
        // to check afterwards if no in/outcall price (for 1 our) was found
        // then calculate manually for example from 2hours having price of 2500$
        // ---------------------------------------------
        $prices = ['incall' => [], 'outcall' => []];
        // ---------------------------------------------

		foreach ( $rates as $rate ) {
			if ( ! $rate['type'] && intval($rate['price']) > 0 ) { // for regular rates and defined prices

                if (AVAILABLE_INCALL == $rate['availability']) {
                    $prices['incall'][] = $rate;
                } elseif (AVAILABLE_OUTCALL == $rate['availability']) {
                    $prices['outcall'][] = $rate;
                }

				if ( ( TIME_HOURS == $rate['time_unit'] && 1 == $rate['time']) || ( TIME_MINUTES == $rate['time_unit'] && 60 == $rate['time'] ) ) {

					if ( AVAILABLE_INCALL == $rate['availability'] ) { 
						$row['profile']['incall_price'] = $rate['price'];
						$row['profile']['incall_currency'] = $rate['currency_id'];
					} elseif ( AVAILABLE_OUTCALL == $rate['availability'] ) {
						$row['profile']['outcall_price'] = $rate['price'];
						$row['profile']['outcall_currency'] = $rate['currency_id'];
					}

				}
			}
			/*if ( ! $rate['type'] && // for regular rate
					TIME_HOURS == $rate['time_unit'] && // for hour
					1 == $rate['time'] && // for one hour
					AVAILABLE_INCALL == $rate['availability'] && // for incall
					intval($rate['price']) > 0 // we need defined price
				) {

				$row['profile']['incall_price'] = $rate['price'];
				$row['profile']['incall_currency'] = $rate['currency_id'];

				break;
			}
			elseif ( ! $rate['type'] && // for regular rate
						TIME_HOURS == $rate['time_unit'] && // for hour
						1 == $rate['time'] && // for one hour
						AVAILABLE_OUTCALL == $rate['availability'] && // for outcall
						intval($rate['price']) > 0 // we need defined price
				) {

				$row['profile']['outcall_price'] = $rate['price'];
				$row['profile']['outcall_currency'] = $rate['currency_id'];
			}*/
		}

        // Now the important part!
        // If loop described above didn't find in/outcall price with 1hour or 60min added by Escort
        // then we Have to calculate it manually by dividing price into hours
        // ---------------------------------------------
        if (self::app()->id == APP_ED)
        {
            foreach (['incall', 'outcall'] as $type) {
                if (empty($row['profile'][$type . '_price'])) {

                    $defaultAddedPrice = isset($prices[$type][0]) ? $prices[$type][0] : null;
                    if (!empty($defaultAddedPrice)) {

                        if (TIME_HOURS == $defaultAddedPrice['time_unit']) {
                            $row['profile'][$type . '_price'] = intval($defaultAddedPrice['price'] / $defaultAddedPrice['time']);
                        } else if (TIME_MINUTES == $defaultAddedPrice['time_unit']) {
                            $row['profile'][$type . '_price'] = intval($defaultAddedPrice['price'] / ($defaultAddedPrice['time'] / 60));
                        }

                        $row['profile'][$type . '_currency'] = $defaultAddedPrice['currency_id'];
                    }
                }
            }
        }
        // ---------------------------------------------


        $t_rates = self::db()->fetchAll('
			SELECT
				availability, type, time, time_unit, price, currency_id, hh_price
			FROM escort_rates
			WHERE escort_id = ? AND availability = 0
		', $row['profile']['id']);
		
		$rates['travel_rates'] = $t_rates;
		
		$row['profile']['rates'] = serialize($rates);
	}
	
	private static function _fillEscortTravelPaymentMethods(&$row)
	{
		$travel_rates = self::db()->fetchAll('
			SELECT
				payment_method, specify
			FROM travel_payment_methods
			WHERE escort_id = ?
		', $row['profile']['id']);		
		
		$row['profile']['travel_payment_methods'] = serialize($travel_rates);
	}

	private static function _fillEscortUpcomingTours(&$row)
	{
		$row['tours'] = self::db()->fetchAll('
			SELECT
				IF ( NOT t.id IS NULL, t.id, NULL ) AS tour_id,
				IF ( NOT t.id IS NULL, t.city_id, NULL ) AS tour_city_id,
				t.date_from AS tour_date_from,
				t.date_to AS tour_date_to,
				t.minutes_from,
				t.minutes_to,
				t.phone
			FROM escorts e
			LEFT JOIN tours t ON t.escort_id = e.id
			WHERE e.id = ? AND DATE(t.date_from) > DATE(NOW())
			ORDER BY t.date_from ASC
			/*LIMIT 1*/
		', $row['profile']['id']);
	}

	private static function _fillEscortServices(&$row)
	{
		$row['services'] = self::db()->fetchAll('
			SELECT
				service_id, price, currency_id
			FROM escort_services
			WHERE escort_id = ?
		', $row['profile']['id']);
	}
	
	private static function _fillEscortKeywords(&$row)
	{
		$row['keywords'] = self::db()->fetchAll('
			SELECT
				keyword_id
			FROM escort_keywords
			WHERE escort_id = ?
		', $row['profile']['id']);
	}

	private static function _fillEscortCities(&$row)
	{
		$row['cities'] = array();
		$cities = self::db()->fetchAll('
			SELECT city_id
			FROM escort_cities
			WHERE escort_id = ?
		', $row['profile']['id']);

		foreach ( $cities as $city ) {
			$row['cities'][] = $city['city_id'];
		}
	}
	
	/*private static function _fillEscortTravelCountries(&$row)
	{
		$row['travel_countries'] = array();
		$countries = self::db()->fetchAll('
			SELECT country_id
			FROM escort_travel_countries
			WHERE escort_id = ?
		', $row['profile']['id']);

		foreach ( $countries as $country ) {
			$row['travel_countries'][] = $country['country_id'];
		}
	}*/
	
	private static function _fillEscortTravelContinents(&$row)
	{
		$row['travel_continents'] = array();
		$continents = self::db()->fetchAll('
			SELECT continent_id
			FROM escort_travel_continents
			WHERE escort_id = ?
		', $row['profile']['id']);

		foreach ( $continents as $c ) {
			$row['travel_continents'][] = $c['continent_id'];
		}
	}

	private static function _fillEscortCityZones(&$row)
	{
		$cityzones = self::db()->fetchAll('
			SELECT city_zone_id
			FROM escort_cityzones
			WHERE escort_id = ?
		', $row['profile']['id']);

		$row['cityzones'] = array();
		foreach ( $cityzones as $cz ) {
			$row['cityzones'][] = $cz['city_zone_id'];
		}
	}

	private static function _fillEscortPhotos(&$row)
	{
		$arg = '';
		if ( self::app()->id == APP_A6 || self::app()->id == APP_A6_AT ) {
			$arg .= ', gotd';
		}
		
		if ( in_array(self::app()->id, array(APP_EF)) ) {
			$arg .= ', profile_boost, is_mobile_cover, is_valentines';
		}
		
		if ( in_array(self::app()->id, array(APP_EF, APP_BL, APP_ED)) ) {
			$arg .= ', gallery_id';
		}
		
		if ( in_array(self::app()->id, array(APP_ED)) ) {
			$arg .= ', popunder_args, is_popunder';
		}
		
		$photos = self::db()->fetchAll('
			SELECT id, hash, ext, type, status, is_main, ordering, args, is_portrait, width, height , is_rotatable, creation_date ' . $arg . '
			FROM escort_photos
			WHERE escort_id = ? AND is_approved = 1 AND type <> ? AND retouch_status <> 1
		', array($row['profile']['id'], ESCORT_PHOTO_TYPE_DISABLED) );

		$row['photos'] = $photos;
	}

	private static function _fillEscortPhotoVotes(&$row)
	{
		$votes = self::db()->fetchAll('
			SELECT *
			FROM escort_photo_votes
			WHERE escort_id = ?
		', array($row['profile']['id']) );

		$row['photo_votes'] = $votes;
	}

	private static function _fillEscortVideos(&$row)
	{		
		$fields = '';

		if (self::app()->id == APP_A6) {
			$fields .= ', vote_count, is_votd, votd_city_id';
		}

		if (self::app()->id == APP_EF) {
			$fields .= ', cover_status , cover_ext , cover_hash ';
		}

		$sql = "SELECT id, video, escort_id, `date`, title, height, width" . $fields . 
			" FROM video WHERE  escort_id = ? AND `status` = 'approve' ";

		$row['videos'] = self::db()->fetchAll($sql, array($row['profile']['id']) );
	
		$row['videos']['image'] = array();
		if(!empty($row['videos']))
		{	$v_id='';
			foreach($row['videos'] as $v)
			{
				$v_id.="'".$v['id']."',";
			}
			$v_id = substr($v_id,0,strlen($v_id)-1);
			$row['videos']['image'] = self::db()->fetchAll("SELECT `hash`,ext,width,height,video_id FROM images JOIN video_image ON id=image_id AND video_id IN($v_id) ORDER BY id") ;
		}
				
	}
	
	private static function _fillEscortEuroLottery(&$row)
	{		
		$euro_lottery = self::db()->fetchAll("SELECT id, escort_id, hash, ext FROM euro_lottery WHERE escort_id = ? AND status <> -1 ",array($row['profile']['id']) );
		foreach ( $euro_lottery as &$lottery ) {
			$lottery['votes'] = self::db()->fetchOne("SELECT COUNT(euro_vote_for) FROM members WHERE euro_vote_for = ? GROUP BY euro_vote_for",array($row['profile']['id']) );
		}
		
		$row['euro_lottery'] = $euro_lottery;
	}
	
	private static function _fillEscortNaturalPics(&$row)
	{		
		$row['natural_pics'] = self::db()->fetchAll("SELECT id, escort_id, hash, ext FROM natural_pics WHERE escort_id = ? AND status = 2",array($row['profile']['id']) );
	
	}
	
	private static function _fillEscortGalleries(&$row)
	{		
		
		if ( self::app()->id == APP_ED) {
			$row['escort_galleries'] = self::db()->fetchAll("( SELECT eg.id , eg.escort_id, eg.title, eg.is_main
														FROM escort_galleries eg
														WHERE eg.escort_id = ? AND eg.is_main = 1 )
														UNION
														( SELECT eg.id, eg.escort_id, eg.title, eg.is_main 
														FROM escort_photos ep
														INNER JOIN escort_galleries eg ON eg.id = ep.gallery_id
														WHERE eg.escort_id = ? AND eg.status = 1 AND ep.type <> 5 AND ep.is_approved = 1
														GROUP BY ep.gallery_id
														HAVING COUNT(ep.id) > 0 ) 
														", array($row['profile']['id'],$row['profile']['id']) );
		}
		else{
			$row['escort_galleries'] = self::db()->fetchAll("SELECT eg.id, eg.escort_id, eg.title".$arg." 
														FROM escort_photos ep
														INNER JOIN escort_galleries eg ON eg.id = ep.gallery_id
														WHERE eg.escort_id = ? AND eg.status = 1 AND ep.type <> 5 AND ep.is_approved = 1
														GROUP BY ep.gallery_id
														HAVING COUNT(ep.id)  > 0 ", array($row['profile']['id']) );
		}
		
	}
	
	private static function _fillEscortProfiles(&$row)
	{
		$data = self::db()->fetchOne('
			SELECT data FROM escort_profiles_snapshots_v2 WHERE escort_id = ?
		', $row['profile']['id']);

		$row['snapshot'] = unserialize($data);
	}

	private static function _fillEscortWorkingTimes(&$row)
	{	//night_escorts
		$times = self::db()->fetchAll('
			SELECT
				day_index, time_from, time_from_m, time_to, time_to_m
			FROM escort_working_times
			WHERE escort_id = ?
		', $row['profile']['id']);

		$row['times'] = $times;
	}
	
	private static function _fillEscortAdditionalAreas(&$row)
	{
		$areas = self::db()->fetchAll('
			SELECT
				aa.order_package_id, aa.escort_id, aa.area_id
			FROM additional_areas aa
			WHERE aa.escort_id = ? AND aa.order_package_id = ?
		', array($row['profile']['id'], $row['order_package_id']));

		$row['add_areas'] = $areas;
	}
	
	private static function _fillEscortVipRequest(&$row)
	{
		$vip_verified = self::db()->fetchOne('
			SELECT TRUE
			FROM vip_requests 
			WHERE escort_id = ? AND status = 2
			ORDER BY id DESC 
			LIMIT 1
		', $row['profile']['id']);
		
		if ($vip_verified)
			$row['profile']['vip_verified'] = 1;
		else
			$row['profile']['vip_verified'] = 0;
	}

	///// for BL only /////////
	public static function getExtraCities()
	{
		try {
			$sql = '
				SELECT p.city_id, p.is_premium, o.escort_id, u.user_type, e.gender
				FROM package_extra_cities p
				INNER JOIN order_packages o ON o.id = p.order_package_id AND o.`status` = 2 AND o.application_id = ?
				INNER JOIN escorts e ON e.id = o.escort_id
				INNER JOIN users u ON u.id = e.user_id
			';

			return self::db()->query($sql, array(self::app()->id))->fetchAll();
		}
		catch (Exception $e) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public static function getExtraVipCountries()
	{
		try {
			$sql = '
				SELECT o.escort_id, p.country_id  
				FROM package_vip_extra_countries p
				INNER JOIN order_packages o ON o.id = p.order_package_id AND o.status = 2
			';

			return self::db()->query($sql)->fetchAll();
		}
		catch (Exception $e) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public static function getActiveEscortsIDS()
	{
		try{
			$sql = '
				SELECT id FROM escorts WHERE status & 32 and agency_id is null
			';

			return self::db()->query($sql)->fetchAll();
		}
		catch (Exception $e) { 
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return true;
	}

	public static function getEscortsNewHistory($last_id = 0)
	{
		$sql = "SELECT * from escorts_new_history where id >". $last_id. " LIMIT 100";
		return self::db()->query($sql)->fetchAll();
	}
}
