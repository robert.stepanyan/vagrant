<?php

/**
 * JSON-API Escorts Module
 */
class Cubix_Api_Module_Escorts extends Cubix_Api_Module
{
	const PACKAGE_ACTIVATION_TYPE_AFTER_CURRENT_PACKAGE_EXPIRES = 6;
	const PACKAGE_ACTIVATION_TYPE_AFTER_EXACT_DATE = 6;
	const PACKAGE_STATUS_PENDING = 1;
	
	const PHONE_PACKAGE_1_DAY_PASS = 19;
	const PHONE_PACKAGE_3_DAY_PASS = 20;
	const PHONE_PACKAGE_5_DAY_PASS = 23;
	
	/* EXTEND PACKAGE */
	const PACKAGE_DIAMOND_LIGHT = 102;
	const PACKAGE_DIAMOND_PREMIUM = 101;
	const PACKAGE_GOLD_TOUR_PREMIUM = 100;
	
	const DL_PRODUCT_PLUS_3_DAYS = 17;
	const DL_PRODUCT_PLUS_6_DAYS = 18;
	const DL_PRODUCT_PLUS_9_DAYS = 20;
	const DP_PRODUCT_PLUS_3_DAYS = 21;
	const DP_PRODUCT_PLUS_6_DAYS = 22;
	const DP_PRODUCT_PLUS_9_DAYS = 23;

	const GT_PRODUCT_PLUS_3_DAYS = 24;
	const GT_PRODUCT_PLUS_6_DAYS = 25;
	
	public static $EXTEND_PACKAGE_PRODUCTS = array(
		self::DL_PRODUCT_PLUS_3_DAYS,
		self::DL_PRODUCT_PLUS_6_DAYS,
		self::DL_PRODUCT_PLUS_9_DAYS,
		self::DP_PRODUCT_PLUS_3_DAYS,
		self::DP_PRODUCT_PLUS_6_DAYS,
		self::DP_PRODUCT_PLUS_9_DAYS,

		self::GT_PRODUCT_PLUS_3_DAYS,
		self::GT_PRODUCT_PLUS_6_DAYS,
	);
	/* EXTEND PACKAGE */

	static public function getActivePackagesExpiration($escort_id)
	{
		// get active package
		$sql = '
			SELECT
				op.id AS opid, op.date_activated, op.expiration_date, p.name, op.status, op.package_id
			FROM order_packages op
			INNER JOIN packages p ON p.id = op.package_id
			WHERE op.escort_id = ? AND p.is_default = 0 AND p.is_active != 0 AND (op.status = 2 OR op.status = 6)
			ORDER BY op.date_activated DESC
			LIMIT 1
		';
		$active = self::db()->fetchRow($sql, $escort_id);

		if ( ! $active ) return null;
		
		if ( Cubix_Application::getId() == APP_A6 || Cubix_Application::getId() == APP_A6_AT ) {			
			$products = self::db()->fetchAll('SELECT product_id FROM order_package_products WHERE order_package_id = ?', array($active['opid']));
			$prod_data = array();
			if ( count($products) ) {
				foreach ( $products as $prod ) {
					$prod_data[] = $prod['product_id'];
				}
			}
			$active['products'] = $prod_data;
			
			if ( in_array($active['package_id'], array(self::PHONE_PACKAGE_1_DAY_PASS, self::PHONE_PACKAGE_3_DAY_PASS, self::PHONE_PACKAGE_5_DAY_PASS)) ) {
				$phone_package_exp = self::db()->fetchRow('SELECT expiration_date AS phone_exp_date FROM phone_packages WHERE order_package_id = ?', array($active['opid']));
				$active['phone_exp_date'] = $phone_package_exp['phone_exp_date'];
			}
		}
		
		
		// get pending packages (which will be activated after current's expiration
        $ukWhere = '';
        if (Cubix_Application::getId() == APP_EG_CO_UK)
            $ukWhere = ' OR op.activation_type = ' . self::PACKAGE_ACTIVATION_TYPE_AFTER_EXACT_DATE;

		$sql = '
			SELECT
				p.name, op.period
			FROM order_packages op
			INNER JOIN orders o ON o.id = op.order_id
			INNER JOIN packages p ON p.id = op.package_id
			WHERE
				op.escort_id = ?
				AND p.is_default = 0
				AND op.status = ' . self::PACKAGE_STATUS_PENDING . '
				AND o.order_date >= ?
				AND op.activation_type = ' . self::PACKAGE_ACTIVATION_TYPE_AFTER_CURRENT_PACKAGE_EXPIRES . '
				'. $ukWhere .'
			ORDER BY o.order_date
			LIMIT 1
		';
		$after = self::db()->fetchRow($sql, array($escort_id, new Zend_Db_Expr($active->date_activated)));
		
		return array('active' => $active, 'after' => $after);
	}
	
	static public function getSuspendDate($op_id)
	{
		$sql = "
			SELECT
				UNIX_TIMESTAMP(suspend_date) AS suspend_date
			FROM suspend_resume_dates
			WHERE order_package_id = ? AND resume_date IS NULL
		";

		$date =  self::db()->fetchRow($sql, array($op_id));
			
		return $date;
	}
	
	static public function getSuspendDaysLeft($op_id)
	{
		$sql = "
			SELECT
				days_left
			FROM order_packages
			WHERE id = ?
		";
		
		$date =  self::db()->fetchRow($sql, array($op_id));
			
		return $date;
	}

    /*
     * Get escort active package id
     * @param escort id
     * @return array
     *  */

    public function getActivePackage($escort_id) {

        $sql = "
			SELECT op.package_id
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			WHERE e.id = ? AND op.status = 2 
			ORDER BY op.date_activated DESC
		";

        $package = self::db()->query($sql, array($escort_id))->fetch();

        return $package;
    }

    public function getActiveGotdExpiration($escort_id) {

        $sql = "
			SELECT
                go.escort_id,
                e.showname,
                go.creation_date,
                go.activation_date,
                c.title_en as city,
                go.`status`
            FROM
                gotd_orders go
                INNER JOIN escorts e ON e.id = go.escort_id
                INNER JOIN cities c ON c.id = go.city_id 
            WHERE
                e.id = ? AND go.`status` = 2
		";

        $gotd = self::db()->fetchAll($sql, array($escort_id));

        return $gotd;
    }

}
