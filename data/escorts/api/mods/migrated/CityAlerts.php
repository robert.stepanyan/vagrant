<?php

class Cubix_Api_Module_CityAlerts extends Cubix_Api_Module
{	
	public function getAll($user_id)
	{		
		if (is_array($user_id)) $user_id = reset($user_id);
		
		return self::db()->query('
			SELECT ca.id, c.' . Cubix_I18n::getTblField('title') . ' AS city, ca.escorts, ca.agencies, ca.notification, ca.period, cu.' . Cubix_I18n::getTblField('title') . ' AS country 
			FROM city_alerts ca 
			INNER JOIN cities c ON c.id = ca.city_id 
			INNER JOIN countries cu ON cu.id = c.country_id 
			WHERE ca.user_id = ? 
			ORDER BY city ASC
		', $user_id)->fetchAll();
	}
	
	public function getAllV2($user_id)
	{		
		if (is_array($user_id)) $user_id = reset($user_id);
		
		$alerts = self::db()->query('
			SELECT ca.id, ca.escort_type, ca.escort_gender, ca.send_email, ca.subject, ca.enabled, ca.all_city, cu.id AS country_id 
			FROM city_alerts_v2 ca
			INNER JOIN countries cu ON cu.id = ca.country_id 
			WHERE ca.user_id = ? 
			ORDER BY ca.id DESC
		', $user_id)->fetchAll();

		if($alerts){
			foreach ($alerts as $alert) {
				$cities = self::db()->query('
					SELECT c.id AS city_id 
					FROM city_alerts_cities cc
					INNER JOIN cities c ON c.id = cc.city_id 
					WHERE cc.alert_id = ? 
				', $alert->id)->fetchAll();
				$alert->cities = $cities;
			}
		}
			// c.' . Cubix_I18n::getTblField('title') . ' AS city,
			// LEFT JOIN city_alerts_cities cc ON cc.alert_id = ca.id
			// INNER JOIN cities c ON c.id = cc.city_id 
		return $alerts;
	}

	public function remove($id, $user_id)
	{
		if (is_array($id)) $id = reset($id);
		if (is_array($user_id)) $user_id = reset($user_id);
		
		$own = self::db()->fetchOne('SELECT TRUE FROM city_alerts WHERE id = ? AND user_id = ?', array($id, $user_id));
		
		if ($own)
		{
			self::db()->delete('city_alerts', self::db()->quoteInto('id = ?', $id));
		}
	}

	public function removeV2($id, $user_id)
	{
		if (is_array($id)) $id = reset($id);
		if (is_array($user_id)) $user_id = reset($user_id);
		
		$own = self::db()->fetchOne('SELECT TRUE FROM city_alerts_v2 WHERE id = ? AND user_id = ?', array($id, $user_id));
		
		if ($own)
		{
			self::db()->delete('city_alerts_v2', self::db()->quoteInto('id = ?', $id));
			self::db()->delete('city_alerts_cities', self::db()->quoteInto('alert_id = ?', $id));
		}
	}
	
	public function suspend($id, $user_id)
	{
		if (is_array($id)) $id = reset($id);
		if (is_array($user_id)) $user_id = reset($user_id);
		
		$own = self::db()->fetchOne('SELECT TRUE FROM city_alerts_v2 WHERE id = ? AND user_id = ?', array($id, $user_id));
		
		if ($own)
		{
			self::db()->update('city_alerts_v2', array('enabled' => 0), self::db()->quoteInto('id = ?', $id));
		}
	}

	public function resume($id, $user_id)
	{
		if (is_array($id)) $id = reset($id);
		if (is_array($user_id)) $user_id = reset($user_id);
		
		$own = self::db()->fetchOne('SELECT TRUE FROM city_alerts_v2 WHERE id = ? AND user_id = ?', array($id, $user_id));
		
		if ($own)
		{
			self::db()->update('city_alerts_v2', array('enabled' => 1), self::db()->quoteInto('id = ?', $id));
		}
	}

	public function add($data)
	{
		self::db()->insert('city_alerts', $data);
	}
	
	public function addV2($data)
	{
		$cities = $data['cities'];
		unset($data['cities']);

		$data['enabled'] = 1;

		self::db()->insert('city_alerts_v2', $data);
		$alertId =  self::db()->lastInsertId();
		
		if($data['all_city'] == 0){
			foreach($cities as $city) {
				$city = (int)$city;
				if($city){
					$cityData = array(); 
					$cityData['alert_id'] = $alertId;
					$cityData['city_id'] = $city;
					self::db()->insert('city_alerts_cities', $cityData);
				}
			}
		}else{
			foreach($cities as $city) {
				$cityData = array(); 
				$cityData['alert_id'] = $alertId;
				$cityData['city_id'] = $city->id;
				self::db()->insert('city_alerts_cities', $cityData);
			}
		}
	}

	public function save($data)
	{
		$own = self::db()->fetchOne('SELECT TRUE FROM city_alerts_v2 WHERE id = ? AND user_id = ?', array($data['id'], $data['user_id']));
		if(!$own){
			return false;
		}

		//remove old cities
		self::db()->delete('city_alerts_cities', self::db()->quoteInto('alert_id = ?', $data['id']));

		$cities = $data['cities'];
		unset($data['cities']);

		$alertId =  $data['id'];
		unset($data['id']);
		self::db()->update('city_alerts_v2', $data, self::db()->quoteInto('id = ?', $alertId));
		
		if($data['all_city'] == 0){
			foreach($cities as $city) {
				$city = (int)$city;
				if($city){
					$cityData = array(); 
					$cityData['alert_id'] = $alertId;
					$cityData['city_id'] = $city;
					self::db()->insert('city_alerts_cities', $cityData);
				}
			}
		}else{
			foreach($cities as $city) {
				$cityData = array(); 
				$cityData['alert_id'] = $alertId;
				$cityData['city_id'] = $city->id;
				self::db()->insert('city_alerts_cities', $cityData);
			}
		}
	}

	public function checkCityExists($city_id, $user_id)
	{
		if (is_array($city_id)) $city_id = reset($city_id);
		if (is_array($user_id)) $user_id = reset($user_id);
		
		return self::db()->fetchOne('SELECT TRUE FROM city_alerts WHERE city_id = ? AND user_id = ?', array($city_id, $user_id));
	}

    public function CityExistsPreCheck($country, $cities, $user_id)
    {
        $dataArray = self::db()->fetchAll('SELECT * FROM city_alerts_v2 WHERE country_id = ? AND user_id = ?', array($country, $user_id));
        if($dataArray){
            if($cities[0] == '-1'){
                foreach ($dataArray as $data) {
                    if ($data->all_city === 1 )
                    {
                        return array('status' => 0);
                    }
                }
                return array('status' => 1);
            }

            foreach ($dataArray as $data) {
                $oldAlertCities = self::db()->fetchAll('SELECT city_id FROM city_alerts_cities WHERE alert_id = ? ', array($data->id));
                if($oldAlertCities){
                    foreach ($oldAlertCities as $oldAlertCity) {
                        foreach ($cities as $city) {
                            if($city == $oldAlertCity->city_id){
                                return array('status' => 0);
                            }
                        }
                    }
                }
            }
        }
        return array('status' => 1);
    }

	public function checkCityExistsV2($country, $cities, $user_id, $escort_type, $escort_gender, $id)
	{
		if($id){
			$dataArray = self::db()->fetchAll('SELECT * FROM city_alerts_v2 WHERE country_id = ? AND user_id = ? AND escort_type = ? AND escort_gender = ?  AND id != ?', array($country, $user_id, $escort_type, $escort_gender, $id));
		}else{
			$dataArray = self::db()->fetchAll('SELECT * FROM city_alerts_v2 WHERE country_id = ? AND user_id = ? AND escort_type = ? AND escort_gender = ?', array($country, $user_id, $escort_type, $escort_gender));
		}

		if($dataArray){			
			if($cities[0] == '-1'){
				foreach ($dataArray as $data) {
                    if ($data->all_city === 1 && self::app()->id == APP_ED)
                    {
                        return array('status' => 0, 'code' => "city_exist");
                    }
					self::db()->delete('city_alerts_v2', self::db()->quoteInto('id = ?', $data->id));
					self::db()->delete('city_alerts_cities', self::db()->quoteInto('alert_id = ?', $data->id));
				}
				return array('status' => 1);
			}

			foreach ($dataArray as $data) {
				$oldAlertCities = self::db()->fetchAll('SELECT city_id FROM city_alerts_cities WHERE alert_id = ? ', array($data->id));
				if($oldAlertCities){
					foreach ($oldAlertCities as $oldAlertCity) {
						foreach ($cities as $city) {
							if($city == $oldAlertCity->city_id){
								return array('status' => 0, 'code' => "city_exist");	
							}
						}
					}
				}
			}
		}
		return array('status' => 1);
	}

	public function getEventList($user_id){

		$events = self::db()->query('
			SELECT ua.alert_id as event_alert_id, ua.city_id, ca.escort_type, ca.escort_gender, ca.all_city, ca.subject, ua.escort_id, cu.' . Cubix_I18n::getTblField('title') . ' AS country, c.' . Cubix_I18n::getTblField('title') . ' AS city 
			FROM user_city_alert ua
			INNER JOIN cities c ON c.id = ua.city_id 
			INNER JOIN city_alerts_v2 ca ON ca.id = ua.alert_id 
			INNER JOIN countries cu ON cu.id = ca.country_id 
			WHERE ua.user_id = ?
			ORDER BY ua.alert_id DESC
			LIMIT 100
		', $user_id)->fetchAll();
		return $events;
	}
	public function clearList($id, $user_id){
		if (is_array($id)) $id = reset($id);
		if (is_array($user_id)) $user_id = reset($user_id);
		
		self::db()->delete('user_city_alert', array(self::db()->quoteInto('alert_id = ?', $id)), self::db()->quoteInto('user_id = ?', $user_id) );
	}

	public function getAlertEscorts($id, $user_id){
		if (is_array($id)) $id = reset($id);
		if (is_array($user_id)) $user_id = reset($user_id);
		
		$ids = self::db()->query('
			SELECT ua.escort_id 
			FROM user_city_alert ua
			WHERE ua.user_id = ? AND alert_id = ?
		', array($user_id,$id))->fetchAll();

		return $ids;
	}
}
