<?php

class Cubix_Api_Module_Blog extends Cubix_Api_Module
{		
	public function addComment($data)
	{		
		self::db()->insert('blog_comments', $data);
	}

	public function updateViewCount($id)
	{
		self::db()->query('UPDATE blog_posts SET view_count = view_count + 1 WHERE id = ?', array($id));
	}
}
