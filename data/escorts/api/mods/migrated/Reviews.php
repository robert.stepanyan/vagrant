<?php

class Cubix_Api_Module_Reviews extends Cubix_Api_Module
{	
	public function getReviewbyID($review_id, $lng = 'en'){
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.showname, e.id AS escort_id,r.t_user_info as author_username, m.email as author_email, u.email as owner_email, UNIX_TIMESTAMP(r.creation_date) AS creation_date, c.title_' . $lng . ' AS city_title, c.id AS city_id,
				a.name AS agency_name, a.id as agency_id, r.fuckometer, r.id, UNIX_TIMESTAMP(r.meeting_date) AS meeting_date, r.looks_rating, r.services_rating, cn.title_' . $lng . ' AS country_title, cn.id AS country_id, cn.iso AS country_iso,
				cn.title_' . $lng . ' AS country_title, r.review, r.services_comments, r.escort_comment, r.meeting_place, r.duration,
				r.duration_unit, r.price, cr.title AS currency, r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69, r.s_anal, r.s_sex, r.s_multiple_sex,
				r.s_breast, r.s_attitude, r.s_conversation, r.s_availability, r.s_photos, r.status as review_status
			FROM reviews r
			INNER JOIN escorts e ON e.id = r.escort_id
			INNER JOIN members m ON m.user_id = r.user_id
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN agencies a ON e.agency_id = a.id
			LEFT JOIN cities c ON c.id = r.city
			LEFT JOIN countries cn ON cn.id = c.country_id
			LEFT JOIN currencies cr ON cr.id = r.currency
			WHERE r.id = '.$review_id;
		
		return self::db()->fetchAll($sql);
		//return self::db()->fetchAll($sql)[0];
	}
	
	public function allowRevCom($escort_id,$mode = 'e')
	{
	    $field = '';
	    if ($mode == 'e'){
	        $field = 'id';
        }elseif ($mode == 'u'){
	        $field = 'user_id';
        }elseif ($mode == 'a'){
            $field = 'agency_id';
        }
        if (!$field){
            return false;
        }

		if (is_array($escort_id)) $escort_id = reset($escort_id);

		return self::db()->query('SELECT disabled_reviews, disabled_comments FROM escorts WHERE `'.$field.'` = ?', $escort_id)->fetch();
	}

	public function setAllowRevCom($escort_id, $act, $val)
	{
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		if (is_array($act)) $act = reset($act);
		if (is_array($val)) $val = reset($val);

		if ($act == 'r')
		{
			$field = 'disabled_reviews';
		}
		else
		{
			$field = 'disabled_comments';
		}

		self::db()->update('escorts', array($field => $val), self::db()->quoteInto('id = ?', $escort_id));
	}

	public function setAllowRevComByUserId($user_id, $act, $val)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($act)) $act = reset($act);
		if (is_array($val)) $val = reset($val);

		if ($act == 'r')
		{
			$field = 'disabled_reviews';
		}
		else
		{
			$field = 'disabled_comments';
		}

		self::db()->update('escorts', array($field => $val), self::db()->quoteInto('user_id = ?', $user_id));
	}

	public function getEscortShowname($escort_id)
	{
		if (is_array($escort_id)) $escort_id = reset($escort_id);

		return self::db()->fetchOne('SELECT showname FROM escorts WHERE id = ?', array($escort_id));
	}

	public function getReviewsForMember($user_id, $lng, $filter, $page, $per_page, $sort_field, $sort_dir)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($lng)) $lng = reset($lng);
		if (is_array($page)) $page = reset($page);
		if (is_array($per_page)) $per_page = reset($per_page);
		if (is_array($sort_field)) $sort_field = reset($sort_field);
		if (is_array($sort_dir)) $sort_dir = reset($sort_dir);

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.showname, e.id AS escort_id, UNIX_TIMESTAMP(r.creation_date) AS creation_date, c.title_' . $lng . ' AS city_title, c.id AS city_id,
				a.name AS agency_name, r.fuckometer, r.id, UNIX_TIMESTAMP(r.meeting_date) AS meeting_date, r.looks_rating, r.services_rating, cn.title_' . $lng . ' AS country_title, cn.id AS country_id, cn.iso AS country_iso,
				cn.title_' . $lng . ' AS country_title, r.review, r.services_comments, r.escort_comment, r.meeting_place, r.duration,
				r.duration_unit, r.price, cr.title AS currency, r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69, r.s_anal, r.s_sex, r.s_multiple_sex,
				r.s_breast, r.s_attitude, r.s_conversation, r.s_availability, r.s_photos, r.status as review_status
			FROM reviews r
			INNER JOIN escorts e ON e.id = r.escort_id
			LEFT JOIN agencies a ON e.agency_id = a.id
			LEFT JOIN cities c ON c.id = r.city
			LEFT JOIN countries cn ON cn.id = c.country_id
			LEFT JOIN currencies cr ON cr.id = r.currency
			WHERE 1
		';

		$where = ' AND r.is_deleted = 0 AND r.user_id = ? ';

		if (isset($filter['showname']) && strlen($filter['showname']))
			$where .= ' AND e.showname LIKE "' . $filter['showname'] . '%"';

		if (isset($filter['date_added_f']) && strlen($filter['date_added_f']))
			$where .= ' AND DATE(r.creation_date) >= "' . date('Y-m-d', $filter['date_added_f']) . '"';

		if (isset($filter['date_added_t']) && strlen($filter['date_added_t']))
			$where .= ' AND DATE(r.creation_date) <= "' . date('Y-m-d', $filter['date_added_t']) . '"';

		if (isset($filter['meeting_date_f']) && strlen($filter['meeting_date_f']))
			$where .= ' AND DATE(r.meeting_date) >= "' . date('Y-m-d', $filter['meeting_date_f']) . '"';

		if (isset($filter['meeting_date_t']) && strlen($filter['meeting_date_t']))
			$where .= ' AND DATE(r.meeting_date) <= "' . date('Y-m-d', $filter['meeting_date_t']) . '"';

		if (isset($filter['city']) && strlen($filter['city']))
			$where .= ' AND c.title_' . $lng . ' LIKE "' . $filter['city'] . '%"';

		if (isset($filter['country_id']) && strlen($filter['country_id']))
			$where .= ' AND cn.id = "' . $filter['country_id'] . '"';

		if (isset($filter['city_id']) && strlen($filter['city_id']))
			$where .= ' AND r.city = "' . $filter['city_id'] . '"';

		if (isset($filter['commented']) && strlen($filter['commented']))
		{
			if ($filter['commented'] == 'yes')
				$where .= ' AND r.escort_comment <> "" ';
			else
				$where .= ' AND (r.escort_comment = "" OR r.escort_comment IS NULL) ';
		}

		$sql .= $where;

		$sql .= '
			GROUP BY r.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql = 'SELECT FOUND_ROWS()';

		$items = self::db()->query($sql, $user_id)->fetchAll();
		$count = intval(self::db()->fetchOne($countSql));

		return array($items, $count);
	}

	public function getReviewsCountryCity($id, $type, $lng, $filter){

		if($type == 'escort'){
			if (is_array($id)) $id = reset($id);
			if (is_array($lng)) $lng = reset($lng);

			$sql = '
				SELECT
					UNIX_TIMESTAMP(r.creation_date) AS creation_date, c.title_' . $lng . ' AS city_title, c.id AS city_id, r.fuckometer, r.id,
					UNIX_TIMESTAMP(r.meeting_date) AS meeting_date, r.looks_rating, r.services_rating, cn.title_' . $lng . ' AS country_title, cn.id AS country_id, cn.iso AS country_iso,
					r.review, r.services_comments, r.escort_comment, r.meeting_place, r.duration, r.duration_unit, r.price, cr.title AS currency,
					r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69, r.s_anal, r.s_sex, r.s_multiple_sex, r.s_breast, r.s_attitude, r.s_conversation,
					r.s_availability, r.s_photos, u.username, u.id as member_id
				FROM reviews r
				INNER JOIN users u ON u.id = r.user_id
				LEFT JOIN cities c ON c.id = r.city
				LEFT JOIN countries cn ON cn.id = c.country_id
				LEFT JOIN currencies cr ON cr.id = r.currency
				WHERE 1
			';

			$where = ' AND r.status = ? AND r.is_deleted = 0 AND r.escort_id = ? ';

			if (isset($filter['member']) && strlen($filter['member']))
				$where .= ' AND u.username LIKE "' . $filter['member'] . '%"';

			if (isset($filter['date_added_f']) && strlen($filter['date_added_f']))
				$where .= ' AND DATE(r.creation_date) >= "' . date('Y-m-d', $filter['date_added_f']) . '"';

			if (isset($filter['date_added_t']) && strlen($filter['date_added_t']))
				$where .= ' AND DATE(r.creation_date) <= "' . date('Y-m-d', $filter['date_added_t']) . '"';

			if (isset($filter['commented']) && strlen($filter['commented']))
			{
				if ($filter['commented'] == 'yes')
					$where .= ' AND r.escort_comment <> "" ';
				else
					$where .= ' AND (r.escort_comment = "" OR r.escort_comment IS NULL) ';
			}

			$sql .= $where;

			$items = self::db()->query($sql, array(REVIEW_APPROVED, $id))->fetchAll();
		}elseif($type == 'agency'){
			if (is_array($id)) $id = reset($id);
			if (is_array($lng)) $lng = reset($lng);

			$escorts = self::getAgencyEscorts($id);

			$ids = array();

			foreach ($escorts as $item)
				$ids[] = $item->id;

			if (count($ids) > 0)
				$ids_str = implode(',', $ids);
			else
				$ids_str = 0;


			$sql = '
				SELECT SQL_CALC_FOUND_ROWS
					UNIX_TIMESTAMP(r.creation_date) AS creation_date, c.title_' . $lng . ' AS city_title, c.id AS city_id, r.fuckometer, r.id,
					UNIX_TIMESTAMP(r.meeting_date) AS meeting_date, r.looks_rating, r.services_rating, cn.title_' . $lng . ' AS country_title, cn.id AS country_id, cn.iso AS country_iso,
					r.review, r.services_comments, r.escort_comment, r.meeting_place, r.duration, r.duration_unit, r.price, cr.title AS currency,
					r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69, r.s_anal, r.s_sex, r.s_multiple_sex, r.s_breast, r.s_attitude, r.s_conversation,
					r.s_availability, r.s_photos, u.username, e.showname, r.escort_id
				FROM reviews r
				INNER JOIN escorts e ON e.id = r.escort_id
				INNER JOIN users u ON u.id = r.user_id
				LEFT JOIN cities c ON c.id = r.city
				LEFT JOIN countries cn ON cn.id = c.country_id
				LEFT JOIN currencies cr ON cr.id = r.currency
				WHERE 1
			';

			$where = ' AND r.status = ? AND r.is_deleted = 0 AND r.escort_id IN (' . $ids_str . ') ';

			if (isset($filter['escort_id']) && strlen($filter['escort_id']))
				$where .= ' AND r.escort_id = ' . intval($filter['escort_id']);

			if (isset($filter['member']) && strlen($filter['member']))
				$where .= ' AND u.username LIKE "' . $filter['member'] . '%"';

			if (isset($filter['date_added_f']) && strlen($filter['date_added_f']))
				$where .= ' AND DATE(r.creation_date) >= "' . date('Y-m-d', $filter['date_added_f']) . '"';

			if (isset($filter['date_added_t']) && strlen($filter['date_added_t']))
				$where .= ' AND DATE(r.creation_date) <= "' . date('Y-m-d', $filter['date_added_t']) . '"';

			if (isset($filter['meeting_date_f']) && strlen($filter['meeting_date_f']))
				$where .= ' AND DATE(r.meeting_date) >= "' . date('Y-m-d', $filter['meeting_date_f']) . '"';

			if (isset($filter['meeting_date_t']) && strlen($filter['meeting_date_t']))
				$where .= ' AND DATE(r.meeting_date) <= "' . date('Y-m-d', $filter['meeting_date_t']) . '"';

			if (isset($filter['city']) && strlen($filter['city']))
				$where .= ' AND c.title_' . $lng . ' LIKE "' . $filter['city'] . '%"';


			if (isset($filter['commented']) && strlen($filter['commented']))
			{
				if ($filter['commented'] == 'yes')
					$where .= ' AND r.escort_comment <> "" ';
				else
					$where .= ' AND (r.escort_comment = "" OR r.escort_comment IS NULL) ';
			}

			$sql .= $where;

			$items = self::db()->query($sql, array(REVIEW_APPROVED))->fetchAll();
		}elseif($type == 'member'){
			if (is_array($id)) $id = reset($id);
			if (is_array($lng)) $lng = reset($lng);

			$sql = '
				SELECT SQL_CALC_FOUND_ROWS
					e.showname, e.id AS escort_id, UNIX_TIMESTAMP(r.creation_date) AS creation_date, c.title_' . $lng . ' AS city_title, c.id AS city_id,
					a.name AS agency_name, r.fuckometer, r.id, UNIX_TIMESTAMP(r.meeting_date) AS meeting_date, r.looks_rating, r.services_rating, cn.title_' . $lng . ' AS country_title, cn.id AS country_id, cn.iso AS country_iso,
					cn.title_' . $lng . ' AS country_title, r.review, r.services_comments, r.escort_comment, r.meeting_place, r.duration,
					r.duration_unit, r.price, cr.title AS currency, r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69, r.s_anal, r.s_sex, r.s_multiple_sex,
					r.s_breast, r.s_attitude, r.s_conversation, r.s_availability, r.s_photos
				FROM reviews r
				INNER JOIN escorts e ON e.id = r.escort_id
				LEFT JOIN agencies a ON e.agency_id = a.id
				LEFT JOIN cities c ON c.id = r.city
				LEFT JOIN countries cn ON cn.id = c.country_id
				LEFT JOIN currencies cr ON cr.id = r.currency
				WHERE 1
			';

			$where = ' AND r.is_deleted = 0 AND r.user_id = ? ';

			if (isset($filter['showname']) && strlen($filter['showname']))
				$where .= ' AND e.showname LIKE "' . $filter['showname'] . '%"';

			if (isset($filter['date_added_f']) && strlen($filter['date_added_f']))
				$where .= ' AND DATE(r.creation_date) >= "' . date('Y-m-d', $filter['date_added_f']) . '"';

			if (isset($filter['date_added_t']) && strlen($filter['date_added_t']))
				$where .= ' AND DATE(r.creation_date) <= "' . date('Y-m-d', $filter['date_added_t']) . '"';

			if (isset($filter['meeting_date_f']) && strlen($filter['meeting_date_f']))
				$where .= ' AND DATE(r.meeting_date) >= "' . date('Y-m-d', $filter['meeting_date_f']) . '"';

			if (isset($filter['meeting_date_t']) && strlen($filter['meeting_date_t']))
				$where .= ' AND DATE(r.meeting_date) <= "' . date('Y-m-d', $filter['meeting_date_t']) . '"';

			if (isset($filter['city']) && strlen($filter['city']))
				$where .= ' AND c.title_' . $lng . ' LIKE "' . $filter['city'] . '%"';

			if (isset($filter['commented']) && strlen($filter['commented']))
			{
				if ($filter['commented'] == 'yes')
					$where .= ' AND r.escort_comment <> "" ';
				else
					$where .= ' AND (r.escort_comment = "" OR r.escort_comment IS NULL) ';
			}

			$sql .= $where;

			$items = self::db()->query($sql, $id)->fetchAll();

		}

		$countries = array();
		$cities = array();

		foreach ($items as $item) {
			if(!in_array($item->country_id, $countries)){
				$countries[] = $item->country_id;	
			}
			if(!in_array($item->city_id, $cities)){
				$cities[$item->country_id][] = $item->city_id;
			}
		}

		return array($countries,$cities);
	}
	public function getReviewsForEscort($escort_id, $lng, $filter, $page, $per_page, $sort_field, $sort_dir)
	{
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		if (is_array($lng)) $lng = reset($lng);
		if (is_array($page)) $page = reset($page);
		if (is_array($per_page)) $per_page = reset($per_page);
		if (is_array($sort_field)) $sort_field = reset($sort_field);
		if (is_array($sort_dir)) $sort_dir = reset($sort_dir);

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				UNIX_TIMESTAMP(r.creation_date) AS creation_date, c.title_' . $lng . ' AS city_title, c.id AS city_id, r.fuckometer, r.id,
				UNIX_TIMESTAMP(r.meeting_date) AS meeting_date, r.looks_rating, r.services_rating, cn.title_' . $lng . ' AS country_title,
				cn.id AS country_id, cn.iso AS country_iso, r.services_rating, r.looks_rating,
				r.review, r.services_comments, r.escort_comment, r.meeting_place, r.duration, r.duration_unit, r.price, cr.title AS currency,
				r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69, r.s_anal, r.s_sex, r.s_multiple_sex, r.s_breast, r.s_attitude, r.s_conversation,
				r.s_availability, r.s_photos, u.username, u.id as member_id
			FROM reviews r
			INNER JOIN users u ON u.id = r.user_id
			LEFT JOIN cities c ON c.id = r.city
			LEFT JOIN countries cn ON cn.id = c.country_id
			LEFT JOIN currencies cr ON cr.id = r.currency
			WHERE 1
		';

		$where = ' AND r.status = ? AND r.is_deleted = 0 AND r.escort_id = ? ';

		if (isset($filter['member']) && strlen($filter['member']))
			$where .= ' AND u.username LIKE "' . $filter['member'] . '%"';

		if (isset($filter['date_added_f']) && strlen($filter['date_added_f']))
			$where .= ' AND DATE(r.creation_date) >= "' . date('Y-m-d', $filter['date_added_f']) . '"';

		if (isset($filter['date_added_t']) && strlen($filter['date_added_t']))
			$where .= ' AND DATE(r.creation_date) <= "' . date('Y-m-d', $filter['date_added_t']) . '"';

		if (isset($filter['meeting_date_f']) && strlen($filter['meeting_date_f']))
			$where .= ' AND DATE(r.meeting_date) >= "' . date('Y-m-d', $filter['meeting_date_f']) . '"';

		if (isset($filter['meeting_date_t']) && strlen($filter['meeting_date_t']))
			$where .= ' AND DATE(r.meeting_date) <= "' . date('Y-m-d', $filter['meeting_date_t']) . '"';

		if (isset($filter['city']) && strlen($filter['city']))
			$where .= ' AND c.title_' . $lng . ' LIKE "' . $filter['city'] . '%"';

		if (isset($filter['country_id']) && strlen($filter['country_id']))
			$where .= ' AND cn.id = "' . $filter['country_id'] . '"';

		if (isset($filter['city_id']) && strlen($filter['city_id']))
			$where .= ' AND r.city = "' . $filter['city_id'] . '"';

		if (isset($filter['commented']) && strlen($filter['commented']))
		{
			if ($filter['commented'] == 'yes')
				$where .= ' AND r.escort_comment <> "" ';
			else
				$where .= ' AND (r.escort_comment = "" OR r.escort_comment IS NULL) ';
		}

		$sql .= $where;

		$sql .= '
			GROUP BY r.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql = 'SELECT FOUND_ROWS()';

		$items = self::db()->query($sql, array(REVIEW_APPROVED, $escort_id))->fetchAll();
		$count = intval(self::db()->fetchOne($countSql));

		return array($items, $count);
	}

	public function getReviewsForAgency($agency_id, $lng, $filter, $page, $per_page, $sort_field, $sort_dir)
	{
		if (is_array($agency_id)) $agency_id = reset($agency_id);
		if (is_array($lng)) $lng = reset($lng);
		if (is_array($page)) $page = reset($page);
		if (is_array($per_page)) $per_page = reset($per_page);
		if (is_array($sort_field)) $sort_field = reset($sort_field);
		if (is_array($sort_dir)) $sort_dir = reset($sort_dir);

		$escorts = self::getAgencyEscorts($agency_id);

		$ids = array();

		foreach ($escorts as $item)
			$ids[] = $item->id;

		if (count($ids) > 0)
			$ids_str = implode(',', $ids);
		else
			$ids_str = 0;


		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				UNIX_TIMESTAMP(r.creation_date) AS creation_date, c.title_' . $lng . ' AS city_title, c.id AS city_id, r.fuckometer, r.id,
				UNIX_TIMESTAMP(r.meeting_date) AS meeting_date, r.looks_rating, r.services_rating, cn.title_' . $lng . ' AS country_title, cn.id AS country_id, cn.iso AS country_iso,
				r.review, r.services_comments, r.escort_comment, r.meeting_place, r.duration, r.duration_unit, r.price, cr.title AS currency,
				r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69, r.s_anal, r.s_sex, r.s_multiple_sex, r.s_breast, r.s_attitude, r.s_conversation,
				r.s_availability, r.s_photos, u.username, e.showname, r.escort_id
			FROM reviews r
			INNER JOIN escorts e ON e.id = r.escort_id
			INNER JOIN users u ON u.id = r.user_id
			LEFT JOIN cities c ON c.id = r.city
			LEFT JOIN countries cn ON cn.id = c.country_id
			LEFT JOIN currencies cr ON cr.id = r.currency
			WHERE 1
		';

		$where = ' AND r.status = ? AND r.is_deleted = 0 AND r.escort_id IN (' . $ids_str . ') ';

		if (isset($filter['escort_id']) && strlen($filter['escort_id']))
			$where .= ' AND r.escort_id = ' . intval($filter['escort_id']);

		if (isset($filter['member']) && strlen($filter['member']))
			$where .= ' AND u.username LIKE "' . $filter['member'] . '%"';

		if (isset($filter['date_added_f']) && strlen($filter['date_added_f']))
			$where .= ' AND DATE(r.creation_date) >= "' . date('Y-m-d', $filter['date_added_f']) . '"';

		if (isset($filter['date_added_t']) && strlen($filter['date_added_t']))
			$where .= ' AND DATE(r.creation_date) <= "' . date('Y-m-d', $filter['date_added_t']) . '"';

		if (isset($filter['meeting_date_f']) && strlen($filter['meeting_date_f']))
			$where .= ' AND DATE(r.meeting_date) >= "' . date('Y-m-d', $filter['meeting_date_f']) . '"';

		if (isset($filter['meeting_date_t']) && strlen($filter['meeting_date_t']))
			$where .= ' AND DATE(r.meeting_date) <= "' . date('Y-m-d', $filter['meeting_date_t']) . '"';

		if (isset($filter['city']) && strlen($filter['city']))
			$where .= ' AND c.title_' . $lng . ' LIKE "' . $filter['city'] . '%"';

		if (isset($filter['country_id']) && strlen($filter['country_id']))
			$where .= ' AND cn.id = "' . $filter['country_id'] . '"';

		if (isset($filter['city_id']) && strlen($filter['city_id']))
			$where .= ' AND r.city = "' . $filter['city_id'] . '"';

		if (isset($filter['commented']) && strlen($filter['commented']))
		{
			if ($filter['commented'] == 'yes')
				$where .= ' AND r.escort_comment <> "" ';
			else
				$where .= ' AND (r.escort_comment = "" OR r.escort_comment IS NULL) ';
		}

		$sql .= $where;

		$sql .= '
			GROUP BY r.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql = 'SELECT FOUND_ROWS()';

		$items = self::db()->query($sql, array(REVIEW_APPROVED))->fetchAll();
		$count = intval(self::db()->fetchOne($countSql));

		return array($items, $count, $escorts);
	}

	public function getAgencyEscorts($agency_id)
	{
		if (is_array($agency_id)) $agency_id = reset($agency_id);

		return self::db()->query('
			SELECT id, showname FROM escorts WHERE agency_id = ? AND ((status & ?) OR (status & ?) OR (status & ?) OR (status & ?)) ORDER BY showname
		', array(
			$agency_id,
			Cubix_EscortStatus::ESCORT_STATUS_ACTIVE,
			Cubix_EscortStatus::ESCORT_STATUS_NO_ENOUGH_PHOTOS,
			Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED,
			Cubix_EscortStatus::ESCORT_STATUS_PROFILE_CHANGED
		))->fetchAll();
	}

	public function getEscortInfo($escort_id)
	{
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		
		$sql = '
			SELECT
				e.id, e.showname, u.application_id, eph.hash AS photo_hash, eph.ext AS photo_ext, e.status,
				ep.contact_phone_parsed
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			WHERE e.id = ?
		';
		
		return self::db()->query($sql, $escort_id)->fetch();
	}
}
