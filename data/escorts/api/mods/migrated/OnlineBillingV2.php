<?php

class Cubix_Api_Module_OnlineBillingV2 extends Cubix_Api_Module
{
	
	//Orders activation condition
	const CONDITION_AFTER_RECEIVING_PAYMENT_DETAILS = 8;
	const CONDITION_AFTER_PAID = 5;
	const CONDITION_WITHOUT_PAYMENT = 10;
	
	//Packages activation type
	const ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES = 6;
	const ACTIVATE_AT_EXACT_DATE = 7;
	const ACTIVATE_ASAP = 9;
	
	//Packages statuses
	const PACKAGE_STATUS_PENDING  = 1;
	const PACKAGE_STATUS_ACTIVE   = 2;
	const PACKAGE_STATUS_EXPIRED  = 3;
	const PACKAGE_STATUS_CANCELLED = 4;
	const PACKAGE_STATUS_UPGRADED = 5;
	const PACKAGE_SUSPENDED = 6;

	//Order statuses
	const ORDER_STATUS_PENDING = 1;
	// const STATUS_CANCELLED = 2;
	const ORDER_STATUS_PAID = 3;
	// const STATUS_CHARGEBACK = 4;
	const ORDER_STATUS_PAYMENT_DETAILS_RECEIVED = 5;
	const ORDER_STATUS_PAYMENT_REJECTED = 6;
	const ORDER_STATUS_CLOSED = 7;
	
	//Transfers statuses
	const TRANSFER_STATUS_PENDING = 1;
	const TRANSFER_STATUS_CONFIRMED = 2;
	const TRANSFER_STATUS_REJECTED = 3;
	const TRANSFER_STATUS_AUTO_REJECTED = 4;
	
	const TRANSFER_TYPE_PHONE_BILLING = 7;
	const TRANSFER_TYPE_CREDIT_CARD = 3;
	
	const IVR_BACKEND_USER_ID = 100;
	const IVR_GOTD_BACKEND_USER_ID = 102;
	
	const GOTD_STATUS_PENDING = 1;
	const GOTD_STATUS_ACTIVE  = 2;
	const GOTD_STATUS_EXPIRED = 3;
	
	const GOTD_ORDER_PENDING = 1;
	const GOTD_ORDER_PAID = 2;
	const GOTD_ORDER_EXPIRED = 3;
	
	
	const BOOST_PROFILE_ORDER_PENDING = 1;
	const BOOST_PROFILE_ORDER_PAID = 2;
	const BOOST_PROFILE_ORDER_EXPIRED = 3;

	const BUMP_PROFILE_ORDER_PENDING = 1;
	const BUMP_PROFILE_ORDER_PAID = 2;
	const BUMP_PROFILE_TRANSFERED_FRONT = 3;
	
	
	
	/* EXTEND PACKAGE */
	const PACKAGE_DIAMOND_LIGHT = 102;
	const PACKAGE_DIAMOND_PREMIUM = 101;
	
	const DL_PRODUCT_PLUS_3_DAYS = 17;
	const DL_PRODUCT_PLUS_6_DAYS = 18;
	const DL_PRODUCT_PLUS_9_DAYS = 20;
	const DP_PRODUCT_PLUS_3_DAYS = 21;
	const DP_PRODUCT_PLUS_6_DAYS = 22;
	const DP_PRODUCT_PLUS_9_DAYS = 23;
	
	const ESCORT_STATUS_NO_PROFILE = 1;
    const ESCORT_STATUS_NO_ENOUGH_PHOTOS = 2;
    const ESCORT_STATUS_NOT_APPROVED = 4;
    const ESCORT_STATUS_OWNER_DISABLED = 8;
    const ESCORT_STATUS_ADMIN_DISABLED = 16;
    const ESCORT_STATUS_ACTIVE = 32;
    const ESCORT_STATUS_IS_NEW = 64;
    const ESCORT_STATUS_PROFILE_CHANGED = 128;
    const ESCORT_STATUS_DELETED = 256;
	
	public static $EXTEND_PACKAGE_PRODUCTS = array(
		self::DL_PRODUCT_PLUS_3_DAYS,
		self::DL_PRODUCT_PLUS_6_DAYS,
		self::DL_PRODUCT_PLUS_9_DAYS,
		self::DP_PRODUCT_PLUS_3_DAYS,
		self::DP_PRODUCT_PLUS_6_DAYS,
		self::DP_PRODUCT_PLUS_9_DAYS
	);
	/* EXTEND PACKAGE */
	
	
	public static function checkIfHasPaidPackage($escort_id) {
		//Checking if escort has active, pending or suspended package
		//Returns these packages
		if ( is_array($escort_id) ) {
			$escort_id = reset($escort_id);
		}

		$sql = "
			SELECT op.period, op.date_activated, UNIX_TIMESTAMP(op.expiration_date) AS expiration_date, 
			op.package_id, op.activation_type, DATE(op.activation_date) as activation_date, op.status, p.name AS package_name
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN packages p ON p.id = op.package_id
			WHERE e.id = ? AND (op.status = 2 OR op.status = 6 OR op.status = 1) AND op.order_id IS NOT NULL
			ORDER BY op.date_activated DESC
		";

		$packages = self::db()->fetchAll($sql, array($escort_id));
		
		return $packages;
	}

	public static function checkIfHasActivePackage($escort_id) {
		if ( is_array($escort_id) ) {
			$escort_id = reset($escort_id);
		}

		$sql = "
			SELECT op.period, op.date_activated, UNIX_TIMESTAMP(op.expiration_date) AS expiration_date, 
			op.package_id, op.activation_type, DATE(op.activation_date) as activation_date, op.status, p.name AS package_name
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN packages p ON p.id = op.package_id
			WHERE e.id = ? AND op.status = 2
		";

		$packages = self::db()->fetchRow($sql, array($escort_id));
		
		return $packages;
	}

	public static function getPremiumCities($escort_id) {
		if ( is_array($escort_id) ) {
			$escort_id = reset($escort_id);
		}

		$sql = "
			SELECT GROUP_CONCAT(pe.city_id,'') as premium_cities
			FROM escorts e
			INNER JOIN order_packages op ON e.id = op.escort_id
			INNER JOIN packages p ON p.id = op.package_id
			LEFT JOIN premium_escorts pe ON pe.order_package_id = op.id 
			WHERE e.id = ? AND op.status = 2
			GROUP BY e.id
		";

		$cities = self::db()->fetchOne($sql, array($escort_id));
		
		return $cities;
	}
	
	
	public function getPackageById($id){
		
		$sql = "SELECT	name FROM packages WHERE id = ? ";
		return self::db()->fetchOne($sql, array($id));
	}
	
	public function getPackagesList($user_type, $gender, $is_pseudo_escort = null, $package_ids = array(), $profile_type = null)
	{
		$where = "";
		
		if ( count($package_ids) ) {
			 $where = 'AND p.id IN (' . implode(',', $package_ids) . ')';
		}
		
		$opt_where = "";
		if ( /*self::app()->id == APP_A6 || */self::app()->id == APP_A6_AT ) {
			$where .= " AND p.is_for_phone_billing = 0 ";
		}

		if ( in_array(self::app()->id, array(APP_EF, APP_6A)) ) {			
			if ( $profile_type == PROFILE_TYPE_DUO ) {
				if ( $gender == GENDER_FEMALE ) {
					$gender = GENDER_DUO_FEMALE;
				} elseif ( $gender == GENDER_MALE ) {
					$gender = GENDER_DUO_MALE;
				} elseif ( $gender == GENDER_TRANS ) {
					$gender = GENDER_DUO_TRANS;
				} elseif ( $gender == GENDER_DOMINA ) {
					$gender = GENDER_DUO_DOMINA;
				} elseif ( $gender == GENDER_BDSM ) {
					$gender = GENDER_DUO_BDSM;
				}
			}
		}
		
		if ( $gender ) {
			$where .= " AND pp.gender = {$gender} ";
			$opt_where .= " AND pr.gender = {$gender} ";
		}
		
		$sql = "
			SELECT
				p.id,
				p.name,
				p.available_for,
				pp.price,
				if(pp.crypto_price <> 0, pp.crypto_price, pp.price) as crypto_price,
				p.period,
				p.is_default
            FROM packages p
			INNER JOIN package_prices pp ON pp.package_id = p.id
            WHERE
				p.application_id = ?
                AND pp.user_type = ?
                AND p.is_active = 1
				{$where}
				AND p.ordering IS NOT NULL
			GROUP BY p.id
			ORDER BY p.ordering ASC
		";

		
		try {
			$packages_list = self::db()->fetchAll($sql, array(Cubix_Application::getId(), $user_type));

			if ( count($packages_list) ) {
				foreach ( $packages_list as $i => $package ) {
					if ( self::app()->id == APP_A6 ) {
						switch($package->id) {
							case 24:
							$packages_list[$i]->price = 350;
							break;
						case 25:
							$packages_list[$i]->price = 430;
							break;
						default:
							break;
						}
					}

					$opt_prod_sql = "
						SELECT
							p.id, p.name, pr.price
						FROM package_products pp
						INNER JOIN products p ON p.id = pp.product_id
						INNER JOIN product_prices pr ON p.id = pr.product_id
						WHERE 
							pp.package_id = ? 
							AND pp.is_optional = 1
							AND pr.application_id = ?
							AND pr.user_type = ?
							AND p.is_hidden = 0
							{$opt_where}
						GROUP BY p.id
					";
					$packages_list[$i]->optional_products = self::db()->fetchAll($opt_prod_sql, array($package->id, Cubix_Application::getId(), $user_type));
					
					if ( $user_type == USER_TYPE_AGENCY ) {
						$package_prices_sql = "
							SELECT
								price, gender
							FROM package_prices
							WHERE package_id = ? AND user_type = ?
						";

						$packages_list[$i]->package_prices = self::db()->fetchAll($package_prices_sql, array($package->id, USER_TYPE_AGENCY));
					}
				}
			}
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return $packages_list;
	}

	public function getPackagesListED($user_type, $gender, $package_ids = array(), $category = null)
	{
		$where = "";

		if ( count($package_ids) ) {
			 $where = 'AND p.id IN (' . implode(',', $package_ids) . ')';
		}

		$opt_where = "";

		if ( $gender && is_numeric($gender) ) {
			$where .= " AND pp.gender = {$gender} ";
			$opt_where .= " AND pr.gender = {$gender} ";
		}elseif ($gender && is_array($gender)){
            $where .= " AND pp.gender IN(".implode(',', $gender).") ";
            $opt_where .= " AND pr.gender IN(".implode(',', $gender).") ";
        }

		if ( $category && is_numeric($category)) {
			$where .= " AND pp.category = {$category} ";
			$opt_where .= " AND pr.escort_type = {$category} ";
		}elseif ( $category && is_array($category)) {
			$where .= " AND pp.category IN(".implode(',', $category).") ";
			$opt_where .= " AND pr.escort_type IN(".implode(',', $category).") ";
		}

		$sql = "
			SELECT
				p.id,
				p.name,
				p.available_for,
				pp.price,
				p.period,
				p.is_default,
			    pp.gender,
			    GROUP_CONCAT(CONCAT(pp.gender,':', pp.category,':',pp.price)) as package_prices
            FROM packages p
			INNER JOIN package_prices pp ON pp.package_id = p.id
            WHERE
				p.application_id = ?
                AND pp.user_type = ?
                AND p.is_active = 1
				{$where}
				AND p.ordering IS NOT NULL
			GROUP BY p.id
			ORDER BY p.ordering ASC
		";

		try {
			$packages_list = self::db()->fetchAll($sql, array(Cubix_Application::getId(), $user_type));

			if ( count($packages_list) ) {
				foreach ( $packages_list as $i => $package ) {
					$opt_prod_sql = "
						SELECT
							p.id, p.name, pr.price
						FROM package_products pp
						INNER JOIN products p ON p.id = pp.product_id
						INNER JOIN product_prices pr ON p.id = pr.product_id
						WHERE 
							pp.package_id = ? 
							AND pp.is_optional = 1
							AND pr.application_id = ?
							AND pr.user_type = ?
							AND p.is_hidden = 0
							{$opt_where}
						GROUP BY p.id
					";
					$packages_list[$i]->optional_products = self::db()->fetchAll($opt_prod_sql, array($package->id, Cubix_Application::getId(), $user_type));
					if ( $user_type == USER_TYPE_AGENCY ) {
						$package_prices_sql = "
							SELECT
								price, gender, category
							FROM package_prices
							WHERE package_id = ? AND user_type = ?
						";
						$packages_list[$i]->package_prices = self::db()->fetchAll($package_prices_sql, array($package->id, USER_TYPE_AGENCY));
					}
				}
			}
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $packages_list;
	}

	public function isPseudoEscort($escort_id)
	{
		return self::db()->fetchOne('SELECT TRUE FROM escorts WHERE id = ? AND pseudo_escort = 1', array($escort_id));		
	}

    public function isVerified($escort_id)
    {
        return self::db()->fetchOne('SELECT TRUE FROM escorts e WHERE e.id = ? AND e.verified_status = 2 ', array($escort_id));
    }
	
	public function isZonaRossa($escort_id)
	{
		return self::db()->fetchOne('SELECT TRUE FROM escorts e INNER JOIN cities c ON e.city_id = c.id WHERE e.id = ? AND c.zona_rossa = 1', array($escort_id));	
	}
	
	public function usedLastQpackage($escort_id)
	{
		return self::db()->fetchOne("SELECT TRUE FROM order_packages
				WHERE escort_id = ? AND package_id IN (121,124,127,130,133,136,139,142,145) 
				AND date_activated >= '2020-05-04' ", array($escort_id));		
	}
	
	public function getNeedAgeVerification($escort_id)
	{
		return self::db()->fetchOne('SELECT need_age_verification FROM escorts WHERE id = ? ', array($escort_id));		
	}
	
	public function getOptionalProducts($package_id, $user_type, $gender, $profile_type = null)
	{
		if ( in_array(self::app()->id, array(APP_EF, APP_6A)) ) {			
			if ( $profile_type == PROFILE_TYPE_DUO ) {
				if ( $gender == GENDER_FEMALE ) {
					$gender = GENDER_DUO_FEMALE;
				} elseif ( $gender == GENDER_MALE ) {
					$gender = GENDER_DUO_MALE;
				} elseif ( $gender == GENDER_TRANS ) {
					$gender = GENDER_DUO_TRANS;
				} elseif ( $gender == GENDER_DOMINA ) {
					$gender = GENDER_DUO_DOMINA;
				} elseif ( $gender == GENDER_BDSM ) {
					$gender = GENDER_DUO_BDSM;
				}
			}
		}		
		
		$opt_prod_sql = "
			SELECT
				p.id, p.name, pr.price
			FROM package_products pp
			INNER JOIN products p ON p.id = pp.product_id
			INNER JOIN product_prices pr ON p.id = pr.product_id
			WHERE 
				pp.package_id = ? 
				AND pp.is_optional = 1
				AND pr.application_id = ?
				AND pr.gender = ?
				AND pr.user_type = ?
				AND p.is_hidden = 0 AND p.id <> 8
			GROUP BY p.id
		";
		$opt_products = self::db()->fetchAll($opt_prod_sql, array($package_id, Cubix_Application::getId(), $gender, $user_type));
		
		return $opt_products;
	}
	
	public function getWorkingLocations($escort_id, $only_zona_rossa = null)
	{
		
		$where = '';
		if($only_zona_rossa){
			$where .= 'AND c.zona_rossa = 1'; 
		}
		
		$sql = '
			SELECT 
				c.id, c.' . Cubix_I18n::getTblFields('title') . ' AS title
			FROM escort_cities ec
			INNER JOIN cities c ON c.id = ec.city_id
			WHERE ec.escort_id = ? 
 		'. $where;
		
		return self::db()->fetchAll($sql, array($escort_id));
	}


	public function addToShoppingCartED($data, $user_id, $user_type, $hash)
	{
		
		try {
			if ( count($data) ) {

				foreach($data as $package) {
					self::db()->insert('shopping_cart', $package);
				}

				return self::getCartPriceED($user_id, $user_type, $hash);
			}
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return true;
	}

    public function checkPromoCode($promoCode)
    {
        $sql = 'SELECT * 
                FROM
                    discounts 
                WHERE
                    promo_code = ? AND status = ?';
        return self::db()->fetchRow($sql, array($promoCode,PROMO_CODE_STATUS_ACTIVE));
    }

	public function updateShoppingCartED($hash){

		self::db()->query('
				UPDATE shopping_cart
				SET status = 1
				WHERE package_id is null
				AND hash = ?
			', array($hash));
		if(Cubix_Application::getId() == APP_ED){
			$data = self::db()->fetchRow('SELECT *  FROM shopping_cart WHERE hash = ? order by id desc limit 1', array($hash));
			$status = new Cubix_EscortStatus($data->escort_id);
			$status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_AWAITING_PAYMENT);
			$status->save();
			
			return $data;
		} else {
			return self::db()->fetchRow('SELECT *  FROM shopping_cart WHERE hash = ? order by id desc limit 1', array($hash));
		}
	}

	public function addToShoppingCart($data, $user_id, $user_type, $hash, $is_crypto = false) 
	{

		try {
			if ( count($data) ) {
				// no need to delete after hash  self::db()->query('DELETE FROM shopping_cart WHERE user_id = ?', array($user_id)); 
				
				foreach($data as $package) {
					if ( $package['agency_id'] === '' ) {
						$package['agency_id'] = null;
					}
					self::db()->insert('shopping_cart', $package);
				}

				return self::getCartPrice($user_id, $user_type, $hash, $is_crypto); 
			}
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return true;
	}
	
	public static function getCartPrice($user_id, $user_type, $hash, $is_crypto = false) 
	{
		//sc.user_id = ? AND pp.user_type = ? AND sc.hash = ?
		$field = $joinWhere = '';

		$price_field = $is_crypto ? 'if(pp.crypto_price <> 0, pp.crypto_price, pp.price) as price' : 'pp.price';
		$product_price_field = $is_crypto ? 'if(pr.crypto_price <> 0, pr.crypto_price, pr.price) as price' : 'pr.price';
		
        if (self::app()->id == APP_ED){
            $joinWhere = ' AND e.type = pp.category';
            $field = ', pp.category ';
        }
		$sql = "
			SELECT 
				sc.id AS shopping_cart_id, e.id AS escort_id, e.showname, p.id AS package_id, p.name, {$price_field} , sc.data {$field}
			FROM shopping_cart sc
			INNER JOIN escorts e ON e.id = sc.escort_id
			INNER JOIN packages p ON p.id = sc.package_id
			INNER JOIN package_prices pp ON pp.package_id = p.id AND e.gender = pp.gender {$joinWhere}
			WHERE
				sc.user_id = ? AND pp.user_type = ? AND sc.hash = ?
			GROUP BY e.id
		";

		$packages = self::db()->fetchAll($sql, array($user_id, $user_type, $hash)); 

		//return $sql;
		
		$amount = 0;

		if ( count($packages) ) {
			
			foreach ( $packages as $i => $package ) {
				$unit_amount = 0;

				$profile_type = null;
				if ( in_array(self::app()->id, array(APP_EF, APP_6A)) ) {
					$profile_type = self::db()->fetchOne('SELECT profile_type FROM escorts WHERE id = ?', $package->escort_id);
				}
				
				$data = unserialize($package->data);
			
				$opt_products = $data['optional_products'];
				
				foreach ($opt_products as $key=> $opt_product) {
					if(is_null($opt_product)){
						unset($opt_products[$key]);
					}
				}

				$gender = self::_getEscortGender($package->escort_id);
				$pr = $package->price;
				if ( in_array(self::app()->id, array(APP_EF, APP_6A)) ) {			
					if ( $profile_type == PROFILE_TYPE_DUO ) {
						if ( $gender == GENDER_FEMALE ) {
							$gender = GENDER_DUO_FEMALE;
						} elseif ( $gender == GENDER_MALE ) {
							$gender = GENDER_DUO_MALE;
						} elseif ( $gender == GENDER_TRANS ) {
							$gender = GENDER_DUO_TRANS;
						} elseif ( $gender == GENDER_DOMINA ) {
							$gender = GENDER_DUO_DOMINA;
						} elseif ( $gender == GENDER_BDSM ) {
							$gender = GENDER_DUO_BDSM;
						}
						
						$price_sql = "
							SELECT 
								{$price_field}
							FROM shopping_cart sc
							INNER JOIN escorts e ON e.id = sc.escort_id
							INNER JOIN packages p ON p.id = sc.package_id
							INNER JOIN package_prices pp ON pp.package_id = p.id
							WHERE
								sc.user_id = ? AND user_type = ? AND sc.escort_id = ? AND sc.hash = ? AND pp.gender = ?
							GROUP BY e.id
						";
						
						$pr = self::db()->fetchOne($price_sql, array($user_id, $user_type, $package->escort_id, $hash, $gender));
					}
				}
				
				if ( in_array(self::app()->id, array(APP_A6)) ) {
					switch($package->package_id) {
						case 24:
							$pr = 350;
							break;
						case 25:
							$pr = 430;
							break;
						default:
							break;
					}
				}
				
				if ( isset($data['period']) ) {
					$pr = ceil(($pr / 30) * $data['period']);
				}

				$unit_amount += $pr;

				if ( count($opt_products) ) {
				    $where = '';
				    if (self::app()->id == APP_ED){
				        $where = " AND pr.escort_type = {$package->category} ";
                    }
					$opt_prod_sql = "
						SELECT
							p.id, p.name, pr.price
						FROM products p
						INNER JOIN product_prices pr ON p.id = pr.product_id
						WHERE 
							p.id IN (" . implode(',', $opt_products) . ")
							AND pr.application_id = ?
							AND pr.user_type = ?
							AND pr.gender = ?
							{$where}
						GROUP BY p.id
					";
					
					$optional_products = self::db()->fetchAll($opt_prod_sql, array(Cubix_Application::getId(), $user_type, $gender));
					foreach($optional_products as $product) {
						$unit_amount += $product->price;

						if ( Cubix_Application::getId() == 16 && $product->id == 18 ) {
							$unit_amount += (count($data['additional_areas']) - 1) * $product->price;
						}
					}
				}
				
				if ( count($data['gotd']) ) {
					$prod_id = 15;
					if ( Cubix_Application::getId() == APP_BL ) {
						$prod_id = 15;
					}
					
					$gotd_price = self::db()->fetchOne("
						SELECT
							{$product_price_field}
						FROM products p
						INNER JOIN product_prices pr ON p.id = pr.product_id
						WHERE 
							p.id = ?
							AND pr.application_id = ?
							AND pr.user_type = ?
							AND pr.gender = ?
						GROUP BY p.id
					", array($prod_id, Cubix_Application::getId(), $user_type, $gender));
					$ids_count = 0;
					foreach($data['gotd'] as $ids) {
						$ids = explode(':', $ids);
						$ids_count += count($ids);
					}
					
					$amount += $gotd_price * $ids_count;
					
				}
				if ( Cubix_Application::getId() == APP_A6 ) {
					if ( count($data['votd']) ) {
						$prod_id = 22;
					
						$votd_price = self::db()->fetchOne("
							SELECT
								{$product_price_field}
							FROM products p
							INNER JOIN product_prices pr ON p.id = pr.product_id
							WHERE 
								p.id = ?
								AND pr.application_id = ?
								AND pr.user_type = ?
								AND pr.gender = ?
							GROUP BY p.id
						", array($prod_id, Cubix_Application::getId(), $user_type, $gender));
						$ids_count = 0;
						foreach($data['votd'] as $ids) {
							$ids = explode(':', $ids);
							$ids_count += count($ids);
						}
						$amount += $votd_price * $ids_count;
					}
				}
				if ($data['discount']) {
					$unit_amount *= (100 - $data['discount'])/100;
				}

				$amount += $unit_amount;
			}
		}
		
		return $amount;
	}

    public static function getCartPriceED($user_id, $user_type, $hash)
    {
        $joins = '';
        $fields = '';

        if(in_array(self::app()->id, [APP_EG_CO_UK, APP_6A, APP_6B])) {
            $joins .= ' INNER JOIN package_prices pp ON pp.package_id = p.id AND e.gender = pp.gender ';
        }else {
            $joins .= ' INNER JOIN package_prices pp ON pp.package_id = p.id AND e.gender = pp.gender AND e.type = pp.category ';
            $fields .= ', pp.category';
        }

        $sql = "
			SELECT 
				sc.id AS shopping_cart_id, e.id AS escort_id, e.showname, p.id AS package_id, p.name, pp.price, sc.data $fields
			FROM shopping_cart sc
			INNER JOIN escorts e ON e.id = sc.escort_id
			INNER JOIN packages p ON p.id = sc.package_id
			$joins
			WHERE
				sc.user_id = ? AND pp.user_type = ? AND sc.hash = ?
			GROUP BY e.id
		";

        $packages = self::db()->fetchAll($sql, array($user_id, $user_type,$hash));

        $amount = 0;
        if ( count($packages) ) {

            foreach ( $packages as $i => $package ) {
                $unit_amount = 0;

                $data = unserialize($package->data);

                $opt_products = $data['optional_products'];
                $premium_cities = $data['premium_cities'];

                foreach ($opt_products as $key=> $opt_product) {
                    if(is_null($opt_product)){
                        unset($opt_products[$key]);
                    }
                }

                $gender = self::_getEscortGender($package->escort_id);
                $pr = $package->price;

                if ( isset($data['period']) ) {
                    $pr = ceil(($pr / 30) * $data['period']);
                }

                //$pr += (count($premium_cities) -1) * $pr * 0.1; //add 10% of package price for each premium city expect base city
                // UPD: https://sceonteam.atlassian.net/browse/EDIR-875 discount disabled for task.

                $unit_amount += $pr;

                if ( count($opt_products) ) {
                    $opt_prod_sql = "
						SELECT
							p.id, p.name, pr.price
						FROM products p
						INNER JOIN product_prices pr ON p.id = pr.product_id
						WHERE 
							p.id IN (" . implode(',', $opt_products) . ")
							AND pr.application_id = ?
							AND pr.user_type = ?
							AND pr.gender = ?
							AND pr.escort_type = ?
						GROUP BY p.id
					";

                    $optional_products = self::db()->fetchAll($opt_prod_sql, array(APP_ED, $user_type, $gender,$package->category));
                    foreach($optional_products as $product) {
                        $unit_amount += $product->price;
                    }
                }

                if ( count($data['gotd']) ) {
                    $prod_id = 15;

                    $product = self::db()->fetchRow("
						SELECT
							p.id, p.name, pr.price
						FROM products p
						INNER JOIN product_prices pr ON p.id = pr.product_id
						WHERE 
							p.id = ?
							AND pr.application_id = ?
							AND pr.user_type = ?
							AND pr.gender = ?
						GROUP BY p.id
					", array($prod_id, Cubix_Application::getId(), $user_type, $gender));
                    $ids_count = 0;
                    foreach($data['gotd'] as $ids) {
                        $ids = explode(':', $ids);
                        $ids_count += count($ids);
                    }
                    $amount += $product->price * $ids_count;
                }
                if ($data['discount']) {
                    $unit_amount *= (100 - $data['discount'])/100;
                }

                $amount += $unit_amount;
            }
        }

        return ceil($amount);
    }
	
	public static function _getEscortGender($escort_id)
	{
		return self::db()->fetchOne('SELECT gender FROM escorts WHERE id = ?', array($escort_id));
	}
	
	public function storeToken($token, $user_id, $type = null , $tracking_member_code = null, $amount = null)
	{
		$token_data = array(
			'token' => $token, 
			'user_id' => $user_id,
		);
		
		if(isset($type)){
			$token_data['type'] = $type; 
		}
				
		$exists = self::db()->fetchOne('SELECT TRUE FROM payment_gateway_tokens WHERE token = ?', array($token));
		
		if($exists == 1){
			return 1;
		}
		
		if ( ! $exists ) {
			self::db()->insert('payment_gateway_tokens', $token_data);
		}
		
		return true;
		
	}

	public function storeFaxin($user_id, $paymentid, $reference, $amount, $status = 0)
	{
		$data = array(
			'paymentid' => $paymentid, 
			'user_id'	=> $user_id,
			'reference'		=> $reference,
			'amount'	=> $amount,
			'created'	=> new Zend_Db_Expr('NOW()'),
			'updated'	=> NULL
		);
			
		$exists = self::db()->fetchOne('SELECT TRUE FROM payment_gateway_faxin WHERE paymentid = ?', array($paymentid));
		
		if($exists == 1){
			return 1;
		}
		
		if ( ! $exists ) {
			self::db()->insert('payment_gateway_faxin', $data);
		}
		
		return true;
		
	}
	
	public function storeStatus($token, $user_id, $response)
	{
		try{
			self::db()->insert('payment_statuses', array('token' => $token, 'user_id' => $user_id, 'response' => $response));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		return true;
	}

	public function getAmountForPowerCash($transaction_id){
       $sql = "
       SELECT
	        response,
	        token 
            FROM
	        payment_statuses 
            WHERE
	        token = $transaction_id
       ";
        $data = self::db()->query($sql)->fetchAll();
        return $data;
    }
	
	public function getAgencyEscorts($agency_id, $sort = 'e.showname ASC')
	{
		$fields = "";
		
		if(self::app()->id == APP_EF){
			$fields = ",e.need_age_verification";
		} elseif (self::app()->id == APP_A6){
			$fields = ",e.city_id, e.date_last_modified, e.date_registered";
		}
		$sql = '
			SELECT
				e.id,
				e.showname,
				e.status,
				e.gender,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.application_id,
				e.verified_status
				'.$fields.' 
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id
			WHERE 
				u.application_id = ?
				AND e.agency_id = ? 
				AND NOT e.status & ? 
				AND NOT e.status & ?
				AND NOT e.status & ?
				AND e.status & ?
 			GROUP BY e.id
			ORDER BY ' . $sort;

		try {
			$escorts = self::db()->query($sql, 
				array(
					Cubix_Application::getId(), 
					$agency_id, 
					ESCORT_STATUS_DELETED, 
					ESCORT_STATUS_TEMPRARY_DELETED, 
					ESCORT_STATUS_IS_NEW,
					ESCORT_STATUS_ACTIVE
				)
			)->fetchAll();
			
			
			if ( count($escorts) ) {
				foreach( $escorts as $i => $escort ) {
					$escorts[$i]->escort_packages = self::checkIfHasPaidPackage($escort->id);
				}
			}
			
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $escorts;
	}

	public function getAgencyEscortsA6($agency_id, $sort = 'e.date_last_modified ASC')
	{
		$where = '';

		if($agency_id == 3039){
			$where = ' AND e.date_last_modified > DATE_SUB(NOW(),INTERVAL 1 YEAR) ';
		}

		$sql = '
			SELECT
				e.id,
				e.showname,
				e.status,
				e.gender,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.application_id,
				e.verified_status,
				IF(v.status = "approve", 1 , 0) AS has_approved_video 
				,e.city_id, e.date_last_modified, e.date_registered,e.cam_booking_status
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id and eph.is_main = 1 and eph.is_approved = 1
			LEFT JOIN video AS v ON v.escort_id = e.id
			WHERE 
				u.application_id = ?
				AND e.agency_id = ? 
				AND NOT e.status & ? 
				AND NOT e.status & ?
				AND NOT e.status & ?
				AND e.gender IS NOT NULL
				AND ( e.status & ? OR e.status & ?)
				'.$where.'
 			GROUP BY e.id
			ORDER BY ' . $sort;

		try {
			$escorts = self::db()->query($sql, 
				array(
					Cubix_Application::getId(), 
					$agency_id, 
					ESCORT_STATUS_DELETED, 
					ESCORT_STATUS_TEMPRARY_DELETED, 
					ESCORT_STATUS_IS_NEW,
					ESCORT_STATUS_ACTIVE,
					ESCORT_STATUS_OWNER_DISABLED
				)
			)->fetchAll();
			
			
			if ( count($escorts) ) {
				foreach( $escorts as $i => $escort ) {
					$escorts[$i]->escort_packages = self::checkIfHasPaidPackage($escort->id);
				}
			}
			
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $escorts;
	}

	
	
	public function getAgencyEscortsED($agency_id)
	{
		$sql = '
			SELECT
				e.id,
				e.showname,
				e.status,
				e.gender,
				e.type,
				e.city_id AS base_city_id,
				 '.Cubix_I18n::getTblFields('c.title', null, false, 'base_city').',
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.application_id,
				e.verified_status,
				e.date_last_modified,
				e.date_registered,
				co.iso AS country_iso,
				' . Cubix_I18n::getTblFields('co.title', null, false, 'country') . '
			FROM escorts e
			LEFT JOIN cities as c ON c.id = e.city_id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN countries co ON co.id = e.country_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id
			WHERE 
				u.application_id = ?
				-- AND e.gender = 1
				AND e.agency_id = ? 
				AND NOT e.status & ? 
				AND NOT e.status & ?
				AND NOT e.status & ?
				AND e.status & ?
				AND eph.is_main = 1 
 			GROUP BY e.id
			ORDER BY e.showname ASC';

		try {
			$escorts = self::db()->query($sql, 
				array(
					Cubix_Application::getId(), 
					$agency_id, 
					ESCORT_STATUS_DELETED, 
					ESCORT_STATUS_TEMPRARY_DELETED, 
					ESCORT_STATUS_IS_NEW,
					ESCORT_STATUS_ACTIVE
				)
			)->fetchAll();
			
			
			if ( count($escorts) ) {
				foreach( $escorts as $i => $escort ) {
					$escorts[$i]->escort_packages = self::checkIfHasPaidPackage($escort->id);
					$escorts[$i]->active_package = self::checkIfHasActivePackage($escort->id);
					$escorts[$i]->premium_cities = self::getPremiumCities($escort->id);
					$escorts[$i]->working_locations = self::getWorkingLocations($escort->id);
				}
			}
			
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $escorts;
	}

	public function getAgencyDisabledEscortsED($agency_id)
	{
		$sql = '
			SELECT
				e.id,
				e.showname,
				e.status,
				e.gender,
				e.type,
				e.city_id AS base_city_id,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.application_id,
				e.verified_status,
				e.date_last_modified,
				e.date_registered,
				co.iso AS country_iso,
				' . Cubix_I18n::getTblFields('co.title', null, false, 'country') . '
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN countries co ON co.id = e.country_id
			INNER JOIN order_packages op ON e.id = op.escort_id
			INNER JOIN packages p ON p.id = op.package_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id
			WHERE 
				u.application_id = ?
				AND e.gender = 1
				AND co.iso <> "us"
				AND e.agency_id = ? 
				AND ( e.status & ? 
				OR e.status & ? )
				AND NOT e.status & ?
				AND NOT e.status & ?
				AND eph.is_main = 1 
				AND op.status = 2
				AND p.id = 128
 			GROUP BY e.id
			ORDER BY e.showname ASC';


		try {
			$escorts = self::db()->query($sql, 
				array(
					Cubix_Application::getId(), 
					$agency_id, 
					ESCORT_STATUS_OWNER_DISABLED, 
					ESCORT_STATUS_TEMPRARY_DELETED, 
					ESCORT_STATUS_IS_NEW,
					ESCORT_STATUS_ACTIVE
				)
			)->fetchAll();
			
			
			if ( count($escorts) ) {
				foreach( $escorts as $i => $escort ) {
					$escorts[$i]->escort_packages = self::checkIfHasPaidPackage($escort->id);
					$escorts[$i]->active_package = self::checkIfHasActivePackage($escort->id);
					$escorts[$i]->premium_cities = self::getPremiumCities($escort->id);
				}
			}
			
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $escorts;
	}

	public function getAgencyEscortsEGUKsc($agency_id)
	{
		$sql = '
			SELECT
				e.id,
				e.showname,
				e.status,
				e.gender,
				e.city_id AS base_city_id,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.application_id,
				e.verified_status,
				e.date_last_modified,
				e.date_registered,
				co.iso AS country_iso,
				' . Cubix_I18n::getTblFields('co.title', null, false, 'country') . '
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN countries co ON co.id = e.country_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id
			WHERE 
				u.application_id = ?
				AND e.agency_id = ? 
				AND  e.status & ? 
			
 			GROUP BY e.id
			ORDER BY e.showname ASC';

		try {
			$escorts = self::db()->query($sql, 
				array(
					Cubix_Application::getId(), 
					$agency_id, 
					ESCORT_STATUS_ACTIVE, 
				)
			)->fetchAll();
			
			
			if ( count($escorts) ) {
				foreach( $escorts as $i => $escort ) {
					$escorts[$i]->escort_packages = self::checkIfHasPaidPackage($escort->id);
				}
			}
			
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $escorts;
	}
		
			
	public function getExpiringEscorts($agency_id)
	{
		

		$sql = '
			SELECT
				e.id,
				e.showname,
				e.status,
				e.gender,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.application_id,
				e.verified_status,
				DATEDIFF(op.expiration_date, DATE(NOW())) as days_left
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id
			INNER JOIN order_packages op ON op.escort_id = e.id
			WHERE 
				u.application_id = ?
				AND e.agency_id = ? 
				AND NOT e.status & ? 
				AND NOT e.status & ?
				AND NOT e.status & ?
				AND e.status & ?
				AND op.order_id IS NOT NULL
				AND op.status = ?
				AND DATEDIFF(op.expiration_date, DATE(NOW())) <= ?
 			GROUP BY e.id
		';
		try {
			$escorts = self::db()->query($sql, 
				array(
					Cubix_Application::getId(), 
					$agency_id, 
					ESCORT_STATUS_DELETED, 
					ESCORT_STATUS_TEMPRARY_DELETED, 
					ESCORT_STATUS_IS_NEW,
					ESCORT_STATUS_ACTIVE,
					self::PACKAGE_STATUS_ACTIVE,
					5
				)
			)->fetchAll();
			
			
			if ( count($escorts) ) {
				foreach( $escorts as $i => $escort ) {
					$escort_packages = self::checkIfHasPaidPackage($escort->id);
					
					//Need only escorts without pending packages
					if ( count( $escort_packages ) ) {
						$current_package = $escort_packages[0];
						$pending_package = $escort_packages[1];

						if ( $pending_package ) {
							unset($escorts[$i]);
						}
					}
				}
			}
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $escorts;
	}
	
	public function getActivePackageForProfileBoost($escort_id)
	{
		$sql = '
			SELECT op.date_activated, op.expiration_date, op.period
			FROM order_packages op
			INNER JOIN order_package_products opp ON opp.order_package_id = op.id
			WHERE op.escort_id = ? AND op.status IN (?) /*AND opp.product_id = ?*/
		';
		
		return self::db()->fetchRow($sql, array($escort_id, PACKAGE_STATUS_ACTIVE/*, PRODUCT_BOOST_PROFILE*/));
	}
	
	public function getActivePackageForProfileBumps($escort_id){
		if ( is_array($escort_id) ) {
			$escort_id = reset($escort_id);
		}

		$sql = "
			SELECT op.date_activated, op.expiration_date, op.period, GROUP_CONCAT(pe.city_id,'') as premium_cities, p.name
			FROM escorts e
			INNER JOIN order_packages op ON e.id = op.escort_id
			INNER JOIN packages p ON p.id = op.package_id
			LEFT JOIN premium_escorts pe ON pe.order_package_id = op.id 
			WHERE e.id = ? AND op.status = 2
			GROUP BY e.id
		";

		$cities = self::db()->fetchROW($sql, array($escort_id));
		
		return $cities;
	}
	
	public function getEscortPackagesForProfileBoost($escort_id)
	{
		$sql = '
			SELECT 
				op.id AS order_package_id, op.period, DATE(op.date_activated) AS date_activated, DATE(op.expiration_date) AS expiration_date,
				DATE(op.activation_date) AS activation_date, op.package_id, op.activation_type, op.status,
				p.name AS package_name
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN packages p ON op.package_id = p.id
			/*INNER JOIN package_products pp ON pp.package_id = p.id*/
			WHERE 
				e.id = ? AND e.status = ? AND op.status IN (?, ?)
			ORDER BY op.date_activated DESC
		';
		
		$packages = self::db()->fetchAll($sql, array(
			$escort_id,
			self::ESCORT_STATUS_ACTIVE,
			self::PACKAGE_STATUS_ACTIVE,
			self::PACKAGE_STATUS_PENDING
			/*PRODUCT_BOOST_PROFILE*/
		));

		if ( ! $packages ) return false;

		
		if ( count($packages) > 1 ) {
			//The case when user has active package and one pending package with activation type after_current_package_expires
			//packages[0] is the active package, packages[1] is the pending package
			if ( $packages[1]->activation_type == self::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES && ! $packages[1]->expiration_date ) {
				$packages[1]->date_activated = $packages[0]->expiration_date;
				$packages[1]->expiration_date = date('Y-m-d', strtotime($packages[1]->date_activated . ' + ' . $packages[1]->period . ' day' ));
			} else if ( $packages[1]->activation_type == self::ACTIVATE_AT_EXACT_DATE && ! $packages[1]->expiration_date ) {
				$packages[1]->date_activated = $packages[1]->activation_date;
				$packages[1]->expiration_date = date('Y-m-d', strtotime($packages[1]->activation_date . ' + ' . $packages[1]->period . ' day')) ;
			}
		}
		
		$result = array();
		
		foreach($packages as $package) {
			unset($package->activation_date);
			unset($package->activation_type);
			$result[$package->order_package_id] = $package;
		}
		
		return $result;
	}
	
	public function getBookedProfileBoosts($city, $escort_id)
	{	
		$sql = '
			SELECT pbo.activation_date AS date, hour_from, hour_to, CONCAT_WS(":", pbo.activation_date, hour_from, hour_to) AS date_id,
			if(pbo.city_id, pbo.city_id, "index") AS city_id, pbo.escort_id
			FROM profile_boost_orders pbo
			WHERE DATE(pbo.activation_date) >= DATE(NOW()) AND pbo.status IN (?,?) 
		';
		
		if ( ! is_null($city) ) {
			if ( is_numeric($city) ) {
				$sql .= ' AND city_id = ' . $city;
			} else {
				$sql .= ' AND city_id IS NULL';
			}
		}
		
		$rows = self::db()->fetchAll($sql, array(self::BOOST_PROFILE_ORDER_PENDING, self::BOOST_PROFILE_ORDER_PAID));
		
		$result = array(
			'all'	=> array(),
			'escort'	=> array()
		);
		//Only 5 boost profiles per city/index
		$booked_hours = array();
		foreach($rows as $row) {
			if ( ! isset($booked_hours[$row->date_id]) ) {
				$booked_hours[$row->date_id] = 1;
			} else {
				$booked_hours[$row->date_id] += 1;
			}

			if ( $booked_hours[$row->date_id] >= 5 ) {
				$result['all'][$row->date][] = array(
					'hour_from'	=> $row->hour_from,
					'hour_to'	=> $row->hour_to,
					'escort_id'	=> $row->escort_id
				);
			}
		}
		
		//Getting escort booked days
		$result['escort'] = self::db()->fetchAll('
			SELECT if(city_id, city_id, "index") AS city_id, activation_date AS date, hour_from, hour_to
			FROM profile_boost_orders
			WHERE escort_id = ? AND DATE(activation_date) >= DATE(NOW()) AND status IN (?, ?)
		', array($escort_id, self::BOOST_PROFILE_ORDER_PENDING, self::BOOST_PROFILE_ORDER_PAID));
		
		
		return $result;
		
	}
	
	
	public function bookProfileBoost($escort_id, $city_id, $activation_dates, $client_cookie_id =null, $ip =null)
	{
		if ( ! $escort_id || ! $activation_dates ) return false;
		
		//Getting active package for this escort
		$sql = 'SELECT id, date_activated, expiration_date FROM order_packages WHERE escort_id = ? AND status = 2';
		$order_package = self::db()->fetchRow($sql, array($escort_id));
		if ( ! $order_package ) return false;
		
		$booked_ids = array();
		foreach($activation_dates as $activation_date) {
		    $profile_boost_order = array(
                'creation_date'		=> date('Y-m-d H:i:s', time()),
                'activation_date'	=> $activation_date['date'],
                'hour_from'			=> $activation_date['hour_from'],
                'hour_to'			=> $activation_date['hour_to'],
                'escort_id'			=> $escort_id,
                'city_id'			=> $city_id,
                'status'			=> self::BOOST_PROFILE_ORDER_PENDING,
                'order_package_id'	=> $order_package->id
            );
		    if (Cubix_Application::getId() == APP_EF){
		        $profile_boost_order['client_cookie_id'] = $client_cookie_id;
		        $profile_boost_order['ip'] = $ip;
            }
			self::db()->insert('profile_boost_orders', $profile_boost_order);
			$booked_ids[] = self::db()->lastInsertId();
		}

		return implode(':', $booked_ids);
	}

	public function bookBump($escort_id, $city_id, $amount, $bumps_count, $frequency, $client_cookie_id =null, $ip =null)
	{

		if ( ! $escort_id || !$frequency || !$bumps_count || !$bumps_count ) return false;
		
		//Getting active package for this escort
		$sql = 'SELECT id, date_activated, expiration_date FROM order_packages WHERE escort_id = ? AND status = 2';

		$order_package = self::db()->fetchRow($sql, array($escort_id));

		if ( ! $order_package ) return false;
		
		$booked_ids = array();
		
		  $bump_boost_order = array(
                'creation_date'		=> date('Y-m-d H:i:s', time()),
                'escort_id'			=> $escort_id,
                'amount'			=> $amount,
                'bumps_count'		=> $bumps_count,
                'frequency'			=> $frequency,
                'city_id'			=> $city_id,
                'status'			=> self::BOOST_PROFILE_ORDER_PENDING,
                'order_package_id'	=> $order_package->id
            );
		    if (Cubix_Application::getId() == APP_EF){
		        $bump_boost_order['client_cookie_id'] = $client_cookie_id;
		        $bump_boost_order['ip'] = $ip;
            }

			self::db()->insert('profile_bumps_orders', $bump_boost_order);
			$booked_ids[] = self::db()->lastInsertId();

		return implode(':', $booked_ids);
	}
	
	
	public function confirmProfileBoost($id, $application_id, $additional_info)
	{	
		$ids = explode(':', $id);
		
		if ( ! count($ids) ) return false;
		
		$row = self::db()->fetchRow('
			SELECT pbo.*, e.user_id
			FROM profile_boost_orders AS pbo
			INNER JOIN escorts e ON e.id = pbo.escort_id
			WHERE pbo.id = ? AND pbo.status = ?', array($ids[0], self::BOOST_PROFILE_ORDER_PENDING)
		);
		
		
		$boost_product_price = 5;
		if ( is_null($row->city_id) ) {
			$boost_product_price = 10;
		}
		$total_amount = $boost_product_price * count($ids);
		
		if ( ! $row ) return false;
		
		try {
			self::db()->beginTransaction();
			
			$order_package = self::db()->fetchRow('
				SELECT id, order_id
				FROM order_packages 
				WHERE id = ?
			', array($row->order_package_id));
			
			//-->Increasing order price by price of PROFILE BOOST product
			self::db()->query('
				UPDATE orders
				SET price = price + ?
				WHERE id = ?
			', array($total_amount, $order_package->order_id));
			
			self::db()->query('
				UPDATE order_packages
				SET price = price + ?
				WHERE id = ?
			', array($total_amount, $order_package->id));
			
			//<--
			//
			//-->Inserting new transfer
			
			$transfer_data = array(
				'transfer_type_id'	=> self::TRANSFER_TYPE_PHONE_BILLING,
				'user_id'			=> $row->user_id,
				'backend_user_id'	=> self::IVR_GOTD_BACKEND_USER_ID,// this user is also for epg payments
				'date_transfered'	=> date('Y-m-d H:i:s'),
				'amount'			=> $total_amount,
				'application_id'	=> $application_id,
				'status'			=> self::TRANSFER_STATUS_CONFIRMED,
				'first_name'		=> 'IVR-Gotd',
				'last_name'			=> 'IVR-Gotd',
				'phone_billing_paid'	=> 1
			);
			
			unset($transfer_data['phone_billing_paid']);
			$transfer_data['transfer_type_id'] = self::TRANSFER_TYPE_CREDIT_CARD;
			$transfer_data['is_self_checkout'] = 1;
			$transfer_data['transaction_id'] = $additional_info['transaction_id'];
			$transfer_data['payment_system'] = $additional_info['payment_system'];
			$transfer_data['cc_number'] = $additional_info['card_number'];
			
			if ($additional_info['first_name']) {
				$transfer_data['first_name'] = $additional_info['first_name'];
				$transfer_data['last_name'] = 'PB';
			}

			if ($application_id == APP_EF){
                $transfer_data['client_cookie_id'] = $row->client_cookie_id;
                $transfer_data['ip'] = $row->ip;
            }
			
			self::db()->insert('transfers', $transfer_data);
			
			$transfer_id = self::db()->lastInsertId();
			
			self::db()->insert('transfer_confirmations', array(
				'transfer_id' => $transfer_id,
				'backend_user_id' => self::IVR_GOTD_BACKEND_USER_ID,// this user is also for epg payments
				'amount' => doubleval($total_amount),
				'date' => new Zend_Db_Expr('NOW()')
			));
			
			self::db()->insert('transfer_orders', array(
				'order_id'	=> $order_package->order_id,
				'transfer_id'	=> $transfer_id
			));
			//<--
			
			//-->Seting status paid 
			self::db()->query('UPDATE profile_boost_orders SET status = ?, transfer_id = ? WHERE id IN (' . implode(',', $ids) . ')', array(self::BOOST_PROFILE_ORDER_PAID, $transfer_id));
			//<--
			
			self::db()->commit();
		} catch(Exception $ex) {
			self::db()->rollBack();
			return $ex;
		}
		
		
		$data = self::db()->fetchRow('
			SELECT e.id, e.showname, if(c.id, c.title_en, "Index") AS city, group_concat(CONCAT(pbo.activation_date,":", pbo.hour_from)) AS activation_date, bu.email AS sales_email
			FROM profile_boost_orders pbo
			INNER JOIN escorts e ON e.id = pbo.escort_id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			LEFT JOIN cities c ON c.id = pbo.city_id
			WHERE pbo.id IN (' . implode(',', $ids) . ')
			GROUP BY escort_id
		');
		
		return $data;
	}
	

	public function confirmProfileBump($id, $application_id, $additional_info)
	{	
		$ids = explode(':', $id);
		
		if ( ! count($ids) ) return false;
		
		$row = self::db()->fetchRow('
			SELECT pbump.*, e.user_id
			FROM profile_bumps_orders AS pbump
			INNER JOIN escorts e ON e.id = pbump.escort_id
			WHERE pbump.id = ? AND pbump.status = ?', array($ids[0], self::BUMP_PROFILE_ORDER_PENDING)
		);
		
		if ( ! $row ) return false;
		
		try {
			self::db()->beginTransaction();
			
			$order_package = self::db()->fetchRow('
				SELECT id, order_id
				FROM order_packages 
				WHERE id = ?
			', array($row->order_package_id));
			
			//-->Increasing order price by price of PROFILE BOOST product
			self::db()->query('
				UPDATE orders
				SET price = price + ?
				WHERE id = ?
			', array($row->amount, $order_package->order_id));
			
			self::db()->query('
				UPDATE order_packages
				SET price = price + ?
				WHERE id = ?
			', array($row->amount, $order_package->id));
			
			//<--
			//
			//-->Inserting new transfer
			
			$transfer_data = array(
				'transfer_type_id'	=> self::TRANSFER_TYPE_PHONE_BILLING,
				'user_id'			=> $row->user_id,
				'backend_user_id'	=> self::IVR_GOTD_BACKEND_USER_ID,// this user is also for epg payments
				'date_transfered'	=> date('Y-m-d H:i:s'),
				'amount'			=> $row->amount,
				'application_id'	=> $application_id,
				'status'			=> self::TRANSFER_STATUS_CONFIRMED,
				'first_name'		=> 'IVR-Gotd',
				'last_name'			=> 'IVR-Gotd',
				'phone_billing_paid'	=> 1
			);
			
			unset($transfer_data['phone_billing_paid']);
			$transfer_data['transfer_type_id'] = self::TRANSFER_TYPE_CREDIT_CARD;
			$transfer_data['is_self_checkout'] = 1;
			$transfer_data['transaction_id'] = $additional_info['transaction_id'];
			$transfer_data['payment_system'] = $additional_info['payment_system'];
			$transfer_data['cc_number'] = $additional_info['card_number'];
			
			if ($additional_info['first_name']) {
				$transfer_data['first_name'] = $additional_info['first_name'];
				$transfer_data['last_name'] = 'PB';
			}

			if ($application_id == APP_EF){
                $transfer_data['client_cookie_id'] = $row->client_cookie_id;
                $transfer_data['ip'] = $row->ip;
            }
			
			self::db()->insert('transfers', $transfer_data);
			
			$transfer_id = self::db()->lastInsertId();
			
			self::db()->insert('transfer_confirmations', array(
				'transfer_id' => $transfer_id,
				'backend_user_id' => self::IVR_GOTD_BACKEND_USER_ID,// this user is also for epg payments
				'amount' => doubleval($row->amount),
				'date' => new Zend_Db_Expr('NOW()')
			));
			
			self::db()->insert('transfer_orders', array(
				'order_id'	=> $order_package->order_id,
				'transfer_id'	=> $transfer_id
			));
			//<--
			
			//-->Seting status paid 
			self::db()->query('UPDATE profile_bumps_orders SET status = ?, transfer_id = ?, activation_date = now() WHERE id IN (' . implode(',', $ids) . ')', array(self::BUMP_PROFILE_ORDER_PAID, $transfer_id));
			//<--
			
			self::db()->commit();
		} catch(Exception $ex) {
			self::db()->rollBack();
			return $ex;
		}
		
		
		$data = self::db()->fetchRow('
			SELECT e.id, e.showname, if(c.id, c.title_en, "Index") AS city, pbump.activation_date AS activation_date, pbump.bumps_count, pbump.frequency, bu.email AS sales_email, u.username, u.email as user_email, u.id as user_id
			FROM profile_bumps_orders pbump
			INNER JOIN escorts e ON e.id = pbump.escort_id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			LEFT JOIN cities c ON c.id = pbump.city_id
			WHERE pbump.id IN (' . implode(',', $ids) . ')
			GROUP BY escort_id
		');
		
		return $data;
	}
	
	
	/*----->EXPO PRODUCT functionality*/
	
	
	public function addExpoOrder($escort_id, $order_package_id, $range)
	{
		self::db()->insert('expo_orders', array(
			'creation_date'		=> date('Y-m-d H:i:s', time()),
			'escort_id'			=> $escort_id,
			'status'			=> 1,
			'order_package_id'	=> $order_package_id,
			'start_date'		=> $range['from'],
			'end_date'		=> $range['to'],
		));
		
		$order_id = self::db()->lastInsertId();

		return $order_id;
	}
	
	public function confirmExpoOrder($id, $additional_info)
	{
		$application_id = Cubix_Application::getId();
		
		$row = self::db()->fetchRow('
			SELECT eo.*, e.user_id, op.package_id
			FROM expo_orders AS eo
			INNER JOIN escorts e ON e.id = eo.escort_id
			INNER JOIN order_packages op ON eo.order_package_id = op.id
			WHERE eo.id = ? AND eo.status = ?', array($id, 1)
		);
		
		if ( ! $row ) return false;

		$days_count = (strtotime($row->end_date) - strtotime($row->start_date)) / (60*60*24);
		$days_count += 1;
		$expo_prodcut_price = self::getExpoProductPrice($row->package_id);
		$total_amount = $expo_prodcut_price * $days_count;//add count of days
		
		try {
			self::db()->beginTransaction();
			
			$order_package = self::db()->fetchRow('
				SELECT id, order_id
				FROM order_packages 
				WHERE id = ?
			', array($row->order_package_id));
			
			//-->Increasing order price by price of EXPO PRODUCT product
			self::db()->query('
				UPDATE orders
				SET price = price + ?
				WHERE id = ?
			', array($total_amount, $order_package->order_id));
			
			self::db()->query('
				UPDATE order_packages
				SET price = price + ?
				WHERE id = ?
			', array($total_amount, $order_package->id));
			
			//<--
			//
			//-->Inserting new transfer
			
			$transfer_data = array(
				'transfer_type_id'	=> self::TRANSFER_TYPE_CREDIT_CARD,
				'user_id'			=> $row->user_id,
				'backend_user_id'	=> self::IVR_GOTD_BACKEND_USER_ID,// this user is also for epg payments
				'date_transfered'	=> date('Y-m-d H:i:s'),
				'amount'			=> $total_amount,
				'application_id'	=> $application_id,
				'status'			=> self::TRANSFER_STATUS_CONFIRMED,
				'first_name'		=> 'IVR-Gotd',
				'last_name'			=> 'IVR-Gotd',
				'is_self_checkout'	=> 1,
				'transaction_id'	=> $additional_info['transaction_id'],
				'cc_number'			=> $additional_info['card_number']
			);
			
			self::db()->insert('transfers', $transfer_data);
			
			$transfer_id = self::db()->lastInsertId();
			
			self::db()->insert('transfer_confirmations', array(
				'transfer_id' => $transfer_id,
				'backend_user_id' => self::IVR_GOTD_BACKEND_USER_ID,// this user is also for epg payments
				'amount' => doubleval($total_amount),
				'date' => new Zend_Db_Expr('NOW()')
			));
			
			self::db()->insert('transfer_orders', array(
				'order_id'	=> $order_package->order_id,
				'transfer_id'	=> $transfer_id
			));
			//<--
			
			//-->Seting status paid 
			self::db()->query('UPDATE expo_orders SET status = ?, transfer_id = ? WHERE id = ?', array(2, $transfer_id, $id));
			//<--
			
			self::db()->commit();
		} catch(Exception $ex) {
			self::db()->rollBack();
			return $ex;
		}
		
		$data = self::db()->fetchRow('
			SELECT e.id, e.showname, bu.email AS sales_email, eo.start_date, eo.end_date
			FROM expo_orders eo
			INNER JOIN escorts e ON e.id = eo.escort_id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE eo.id = ?
		', array($id));
		
		return $data;		
	}
	
	public function getEscortPackagesForExpo($escort_id)
	{
		$sql = '
			SELECT 
				op.id AS order_package_id, op.period, DATE(op.date_activated) AS date_activated, DATE(op.expiration_date) AS expiration_date,
				DATE(op.activation_date) AS activation_date, op.package_id, op.activation_type, op.status,
				p.name AS package_name
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN packages p ON op.package_id = p.id
			WHERE 
				e.id = ? AND e.status = ? AND op.status IN (?, ?)
			ORDER BY op.date_activated DESC
		';
		
		$packages = self::db()->fetchAll($sql, array(
			$escort_id,
			self::ESCORT_STATUS_ACTIVE,
			self::PACKAGE_STATUS_ACTIVE,
			self::PACKAGE_STATUS_PENDING
			/*PRODUCT_BOOST_PROFILE*/
		));

		if ( ! $packages ) return false;
		
		if ( $packages[0]->activation_type == self::ACTIVATE_ASAP && $packages[0]->status == PACKAGE_STATUS_ACTIVE ) {
			$packages[0]->expiration_date = date('Y-m-d', strtotime($packages[0]->expiration_date . "-1 days"));
		}
		
		
		if ( count($packages) > 1 ) {
			//The case when user has active package and one pending package with activation type after_current_package_expires
			//packages[0] is the active package, packages[1] is the pending package
			if ( $packages[1]->activation_type == self::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES && ! $packages[1]->expiration_date ) {
				$packages[1]->date_activated = date('Y-m-d', strtotime($packages[0]->expiration_date . "+1 days"));
				$packages[1]->expiration_date = date('Y-m-d', strtotime($packages[1]->date_activated . ' + ' . ($packages[1]->period - 1) . ' day' ));
			} else if ( $packages[1]->activation_type == self::ACTIVATE_AT_EXACT_DATE && ! $packages[1]->expiration_date ) {
				$packages[1]->date_activated = $packages[1]->activation_date;
				$packages[1]->expiration_date = date('Y-m-d', strtotime($packages[1]->activation_date . ' + ' . ($packages[1]->period - 1) . ' day')) ;
			}
		}
		
		$result = array();
		
		foreach($packages as $package) {
			unset($package->activation_date);
			unset($package->activation_type);
			$result[$package->order_package_id] = $package;
		}
		
		return $result;
	}
	
	public static function getExpoProductPrice($package_id)
	{
		switch($package_id) {
			case 101:
			case 102:
				return 6;
				break;
			case 10:
			case 80:
				return 12;
				break;
			case 11:
			case 79:
				return 16;
				break;
			case 9:
			case 78:
				return 21;
				break;
			default: 
				return 12;
				break;
		}
	}
	
	public function getEscortExpoOrders($escort_id)
	{
		return self::db()->fetchAll('
			SELECT start_date, end_date
			FROM expo_orders
			WHERE escort_id = ? AND (status = 2 OR status = 3)
		', array($escort_id));
	}
	
	
	/*<-----EXPO PRODUCT functionality*/
	
	public static function getProduct($product_id)
	{
		$product = self::db()->fetchRow('
			SELECT p.name, pp.price
			FROM products p
			INNER JOIN product_prices pp ON p.id = pp.product_id
			WHERE p.id = ?
		', array($product_id));
		
		return $product;
	}

	public function getAgencyDiscounts()
	{
		return self::db()->fetchAll('SELECT id, value_from, value_to, discount FROM agency_discounts');
	}

	public function activate_free_package_a6($package_id, $escort_id, $hash){

		$shopping_cart = self::db()->fetchRow('SELECT id, user_id, data FROM shopping_cart WHERE  escort_id = ? AND hash = ?', array($escort_id, $hash));
		$data = unserialize( $shopping_cart->data );

		$previous_order_package_id = self::db()->fetchOne('SELECT id FROM order_packages WHERE order_id IS NOT NULL AND `status` = ? AND escort_id = ?', array(2, $escort_id));
		$activation_type = 9;

		if ( $previous_order_package_id ) {
			$activation_type = 6;
		} else {
			$previous_order_package_id = null;
		}

		$activation_date = null;
		if ( $data['activation_date'] ) {
			$previous_order_package_id = null;
			$activation_type = 7;
			$activation_date = $data['activation_date'];
		}

		$order_data = array(
			'user_id'	=> 1,
			'backend_user_id' => 70, //$this->_backend_user_id SUPER ADMIN,
			'status' => 3, //Model_Billing_Orders::STATUS_PAID
			'activation_condition' => 10, //Model_Billing_Orders::CONDITION_WITHOUT_PAYMENT
			'order_date' => new Zend_Db_Expr('NOW()'),
			'payment_date'	=> new Zend_Db_Expr('NOW()'),
			'system_order_id' => 'FREE 5 DAYS',
			'price' => 1, // Temporary price. Will be updated after foreach
			'packages_count'	=> count($shopping_cart),
			'application_id' => Cubix_Application::getId(),
			'use_balance' => 0,
			'is_self_checkout' => 1
		);
		
		parent::db()->insert('orders', $order_data);
		$order_id = parent::db()->lastInsertId();
		
		$order_package_data = array(
			'order_id'			=> $order_id,
			'escort_id'			=> $escort_id,
			'package_id'		=> $package_id,
			'application_id'	=> Cubix_Application::getId(),
			'base_period'		=> 5,
			'period'			=> 5,
			'activation_type'	=> $activation_type,
			'base_price'		=> 0,
			'price'				=> 0,
			'previous_order_package_id'	=> $previous_order_package_id,
			'activation_date'	=>  date('Y-m-d H:i:00', $activation_date)
		);
		
		self::db()->insert('order_packages', $order_package_data);
		$order_package_id = self::db()->lastInsertId();

		if( $order_package_id > 0 ){
			self::db()->insert('order_package_products', array('product_id' => 1, 'is_optional' => 0, 'order_package_id'=> $order_package_id ) );
			self::db()->insert('order_package_products', array('product_id' => 10, 'is_optional' => 0, 'order_package_id'=> $order_package_id ) );
		}

		if ( isset($data['premium_cities']) && count($data['premium_cities']) ) {
			foreach( $data['premium_cities'] as $prem_city ) {
				self::db()->insert('premium_escorts', array('order_package_id' => $order_package_id, 'city_id' => $prem_city));
			}
		}

		if ( isset($data['additional_areas']) && count($data['additional_areas']) ) {
			foreach( $data['additional_areas'] as $area ) {
				self::db()->insert('additional_areas', array('order_package_id' => $order_package_id, 'escort_id' => $escort_id, 'area_id' => $area));
			}
		}

		self::db()->update('shopping_cart', array('status' => 1), array( parent::db()->quoteInto('user_id  = ?', $shopping_cart->user_id), parent::db()->quoteInto('hash = ?', $hash)));

		return $order_package_id;
	}
	
	public function saveCoinsomeRequest($user_id, $payment_type, $price, $hash, $gotd_orders_ids = Null )
	{
		try{
			$data = array(
				'user_id' => $user_id,
				'payment_type' => $payment_type,
				'price' => $price,
				'hash' => $hash,
				'gotd_orders_ids' => $gotd_orders_ids
			);

			self::db()->insert('coinsome_orders', $data);
		} catch(Exception $ex) {
			self::db()->rollBack();
			return $ex;
		}
		return self::db()->lastinsertId();
	}		
}
