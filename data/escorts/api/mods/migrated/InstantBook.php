<?php
class Cubix_Api_Module_InstantBook extends Cubix_Api_Module
{		
	
	public function getInstantBookings($escort_id)
	{
		$sql = "SELECT `id`, `hash`, `duration`,  `location`, `outcall_address`, `special_request`,
			`phone`, `contact_type`, `wait_time`, `escort_id`, `status`, `request_date`, `booking_date`
			FROM instant_book_requests 
			WHERE 
				escort_id = ? AND UTC_TIMESTAMP() < `booking_date`
			ORDER BY `status` DESC
		";

		return self::db()->fetchAll($sql, $escort_id);
	}

	public function getHistory($page, $per_page, $filter)
	{
		
		$sql = 
			'SELECT SQL_CALC_FOUND_ROWS
				`id`, `hash`, CONCAT(`hour`,":", `min`) AS start_time, `duration`,  `location`, `outcall_address`, `special_request`,
				`phone`, `contact_type`, `wait_time`, `escort_id`, `status`, UNIX_TIMESTAMP(`request_date`) as request_date
			FROM instant_book_requests
			WHERE 1
			';

		if ( $filter['date_from'] ) {
			$sql .= " AND DATE(`request_date`) >= DATE(FROM_UNIXTIME({$filter['date_from']})) ";
		}
		
		if ( $filter['date_to'] ) {
			$sql .= " AND DATE(`request_date`) <= DATE(FROM_UNIXTIME({$filter['date_to']})) ";
		}
		
		if ($filter['escort_id']) {
			$sql .= " AND `escort_id` = {$filter['escort_id']}";
		}

		$sql .= '
			ORDER BY `request_date` DESC
			LIMIT ' . (($page - 1) * $per_page) . ', ' . $per_page . '
		';
		$count_sql = 'SELECT FOUND_ROWS()';
		
		$entities = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne($count_sql);

		return array('entities' => $entities, 'count' => $count);
	}

	public function getInstantBookContactInfo($escort_id)
	{

		$sql_from_settings = "SELECT `escort_id`, `country_code`, `phone_number`, `phone_number_parsed`, `receive_sms` FROM instant_book_settings WHERE escort_id = ?";
		$result = self::db()->fetchRow($sql_from_settings, $escort_id);

		if (is_null($result) || is_null($result->phone_number)) {
			$sql_from_confirmed = "SELECT `escort_id`, `new_phone` AS phone_number, NULL AS `receive_sms`, `new_phone_parsed` AS phone_number_parsed, `country_code` FROM confirm_phone_number WHERE escort_id = ?";

			$result = self::db()->fetchRow($sql_from_confirmed, $escort_id);
		}

		return $result;
	}
	
	public function setInstantBookContactInfo($data)
	{
		$escort_id = $data['escort_id'];
		$country_code = $data['country_code'];
		$phone_number = $data['phone_number'];
		$phone_number_parsed = $data['phone_number_parsed'];
		$receive_sms = $data['receive_sms'];

		$sql = "INSERT INTO instant_book_settings (`escort_id`, `country_code`, `phone_number`, `phone_number_parsed`, `receive_sms`) 
			VALUES({$escort_id}, '{$country_code}', '{$phone_number}', '{$phone_number_parsed}', {$receive_sms}) ON DUPLICATE KEY UPDATE `country_code` = '{$country_code}', `phone_number` = '{$phone_number}', `phone_number_parsed` = '{$phone_number_parsed}', `receive_sms` = {$receive_sms}";
		return self::db()->query($sql);
	}
	
	public function userMadeRequest($session_id)
	{
		$hash = '/booking#'.$session_id;
		$sql = "SELECT TRUE FROM instant_book_requests WHERE hash = ?";
		return self::db()->fetchOne($sql, $hash);
	}
}
