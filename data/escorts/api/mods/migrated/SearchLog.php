<?php

class Cubix_Api_Module_SearchLog extends Cubix_Api_Module
{
    protected static $table = 'search_log';

    /**
     * Inserts new value into table or increments count of last inserted
     * if the query matches
     * @param array $data
     * @return bool
     * @throws \Exception
     */
    public function add($data)
    {
        $date = date("Y-m-d H:i:s");
        $sql = '
            INSERT INTO search_log (query, mobile_attempts, desktop_attempts, last_search_date) VALUES(?, ?, ?, ?)
            ON DUPLICATE KEY UPDATE mobile_attempts = mobile_attempts + ?, desktop_attempts = desktop_attempts + ?, last_search_date = ?
        ';

        $bind = array(
            strtolower($data['query']),
            $data['mobile_attempts'],
            $data['desktop_attempts'],
            $date,
            $data['mobile_attempts'],
            $data['desktop_attempts'],
            $date,
        );

        try {
            self::db()->query($sql, $bind);
        }catch(\Exception $e) {
            return $e->getMessage();
        }

    }

    public function updatecount($data){
       try {
        $sql = "update search_log set escorts_count = ".addslashes($data['count'])." where `query` = '".addslashes($data['query'])."'";
            self::db()->query($sql, $bind);
        }catch(\Exception $e) {
            return $e->getMessage();
        }
    }

}
