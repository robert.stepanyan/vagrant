<?php
class Cubix_Api_Module_Credits extends Cubix_Api_Module 
{
	public function addRequest($data){
		
		$db = self::db();
		$db->insert('credit_requests', $data);
    
		return $db->lastInsertId();
	}
	
	public function getConfirmedBumps(){
		return self::db()->fetchAll('SELECT * FROM profile_bumps_orders WHERE status = 2');
	}
}