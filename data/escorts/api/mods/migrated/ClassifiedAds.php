<?php

class Cubix_Api_Module_ClassifiedAds extends Cubix_Api_Module
{

	const EVENT_TYPE_UPDATED  = 1;
	const EVENT_TYPE_REMOVED  = 2;

	public function save($data)
	{
		$photos = $data['photos'];
		$data = $data['data'];
		$cities = $data['cities'];
		
		unset($data['cities']);
		try {
			
			if ( ! count($cities) ) {
				throw new Exception('No city has been defined');
			}
			
			$ip = $data['ip'];
			
			if ($ip)
			{
				$ips = explode(',', $ip);
				$ip = trim($ips[0]);
			}
			
			$insert_data = array(
				'user_id' => $data['user_id'],
				'application_id' => self::app()->id,
				'category_id' => $data['category'],
				//'city_id' => $data['city'],
				'phone' => $data['phone'],
				'email' => $data['email'],
				'period' => $data['duration'],
				'title' => $data['title'],
				'text' => $data['text'],
				'search_text' => $data['title'] . ' ' . $data['text'] . ' ' . $data['phone'] . ' ' . $data['email'],
				'ip' => $ip
			);

			if (self::app()->id == APP_A6)
			{
				$insert_data['web'] = $data['web'];
				$insert_data['phone_parsed'] = $data['phone_parsed'];
				$insert_data['phone_prefix_id'] = $data['phone_prefix_id'];
				$insert_data['sms_available'] = $data['sms_available'];
				$insert_data['whatsapp_available'] = $data['whatsapp_available'];
			}

			if (self::app()->id == APP_BL)
			{
				$insert_data['page_link'] = $data['page_link'];
			}

			if ( (self::app()->id == APP_EG_CO_UK || self::app()->id == APP_6B) && isset($data['country_prefix']))
			{
				$insert_data['country_prefix'] = $data['country_prefix'];
			}
			


			if (self::app()->id == APP_ED)
			{
				$is_ip_blacklisted = self::db()->fetchOne('SELECT block FROM blacklisted_ips WHERE ip = ?', $ip);
				if($is_ip_blacklisted)
				{
					$insert_data['status'] = 3;
					$insert_data['is_disabled'] = 1;
					$insert_data['is_ip_blocked'] = 1;
				}
			}

			self::db()->insert('classified_ads', $insert_data);
			
			$ad_id = self::db()->lastInsertId();
		
			if ( count($photos) ) {
				$k = 1;
				foreach( $photos as $key => $photo ) {
					if ( $k > 8  ) break;
					if(self::app()->id == APP_A6){
						self::db()->insert('classified_ads_images', array('ad_id' => $ad_id, 'image_id' => $key));
					} else{
						self::db()->insert('classified_ads_images', array('ad_id' => $ad_id, 'image_id' => $photo['image_id']));
					}
					$k++;
				}
			}
	
			
			if ( count($cities) ) {
				$k = 1;
				foreach( $cities as $city ) {
					if ( $k > 3  ) break;
					self::db()->insert('classified_ads_cities', array('ad_id' => $ad_id, 'city_id' => $city));
					$k++;
				}
			}	
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}	

		return $ad_id;
	}
	
	public function getPackages()
	{
		try {
			$packages = self::db()->fetchAll('SELECT * FROM classified_ads_packages WHERE application_id = ?', array(self::app()->id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}	

		return $packages;
	}
	
	public function getPackageById($id)
	{		
		try {
			$package = self::db()->fetchRow('SELECT * FROM classified_ads_packages WHERE application_id = ? AND id = ?', array(self::app()->id, $id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}	

		return $package;
	}
	
	public function updateViewCount($ad_id)
	{		
		try {
			$package = self::db()->query('UPDATE classified_ads SET view_count = view_count + 1 WHERE id = ?', array($ad_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function checkAdByPhone($phone)
	{		
		try {
			$ad = self::db()->fetchRow('SELECT TRUE FROM classified_ads WHERE application_id = ? AND phone = ? AND DATE(NOW()) = DATE(creation_date)', array(self::app()->id, $phone));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}	

		return $ad;
	}

	public function checkAdByIp($ip)
	{		
		try {
			$ad = self::db()->fetchRow('SELECT count(id) as count FROM classified_ads WHERE application_id = ? AND ip = ? AND DATE(NOW()) = DATE(creation_date)', array(self::app()->id, $ip));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}	

		return $ad;
	}

	public function addFeedback($data)
	{
		try {
			$data += array('date' => new Zend_Db_Expr('NOW()'));
			self::db()->insert('classified_ads_feedbacks', $data);
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function getFeedbacksBySenderEmail($data)
	{
		try {
			$count = self::db()->fetchOne('SELECT count(*) as count FROM escort_feedbacks WHERE application_id = ? AND email LIKE ? AND date > DATE_SUB(NOW(), INTERVAL ? HOUR)', array(self::app()->id, '%'.$data['email'].'%', $data['from_date']));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		return $count;
	}

	public function getFeedbacksBySenderAndReceiverEmails($data)
	{
		try {
			$count = self::db()->fetchOne('SELECT count(*) as count FROM escort_feedbacks WHERE application_id = ? AND email LIKE ? AND to_addr LIKE ? AND date > DATE_SUB(NOW(), INTERVAL ? HOUR)', array(self::app()->id, '%'.$data['email'].'%', '%'.$data['to_addr'].'%', $data['from_date']));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		return $count;
	}

	public function checkPhoneInProfile($phone)
	{
		return self::db()->query('
			SELECT p.escort_id, e.showname
			FROM escort_profiles_v2 p
			INNER JOIN escorts e ON e.id = p.escort_id
			WHERE p.contact_phone_parsed = ?
		', $phone)->fetch();
	}

    public function checkCreationIntervalByIp($ip, $interval) {
	    $sql = "select count(id) as c from classified_ads as ca
               WHERE ca.ip = '{$ip}'
               AND ca.creation_date > NOW() - INTERVAL {$interval} HOUR
               ";
        return self::db()->fetchOne($sql);
	}

	public function getListForUser($filter = array(), $page = 1, $perPage = 20)
	{
		if( ! $page ) $page = 1;
		
		$limit = ' LIMIT ' . ($page - 1) * $perPage . ', ' . $perPage;
		$fields = '';
		$where = '';
		$order = '';
								
		$where = '';
		$order = ' id DESC ';
		
		if ( isset($filter['category']) && $filter['category'] ) {
			$where .= ' AND ca.category_id = '.$filter['category'] ;
		}
		
		if ( isset($filter['city']) && $filter['city'] ) {
			$where .= ' AND cac.city_id = '.$filter['city'] ;
		}
		
		if ( isset($filter['text']) && $filter['text'] ) {
			$where .= ' AND ca.search_text LIKE '.'%' . $filter['text'] . '%' ;
		}

		if ( isset($filter['user_id']) && $filter['user_id'] ) {
			$where .= ' AND ca.user_id = '.$filter['user_id'] ;
		}
		$sql = "
			SELECT SQL_CALC_FOUND_ROWS ca.*, c." . Cubix_I18n::getTblField('title') . " AS city_title
			FROM classified_ads ca
			INNER JOIN classified_ads_cities cac ON cac.ad_id = ca.id
			INNER JOIN cantons c ON c.id = cac.city_id
			WHERE 1 {$where}
			GROUP BY ca.id
			ORDER BY {$order}
			{$limit}
		";
		$items = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		
		if ( count($items) ) {
			foreach ( $items as $i => $item ) {
				$items[$i]->images = self::db()->fetchAll('
					SELECT 
						i.hash, i.ext
					FROM images i
					INNER JOIN classified_ads_images cai ON i.id = cai.image_id
					WHERE ad_id = ?
				', array($item->id));
			}
		}
		
		return array('ads' => $items, 'count' => $count);
	}

	//only for AND6
	public function getAdForUser($user_id, $id)
	{
		$sql = "
			SELECT ca.*, c." . Cubix_I18n::getTblField('title') . " AS city_title, TIMESTAMPDIFF(day, premium_activation_date, premium_expiration_date) as premium_period
			FROM classified_ads ca
			INNER JOIN classified_ads_cities cac ON cac.ad_id = ca.id
			INNER JOIN cantons c ON c.id = cac.city_id
			WHERE ca.user_id = ".$user_id ." AND ca.id = " . $id . "
			GROUP BY ca.id 
			ORDER BY premium_period DESC, id DESC 
		";
		$ad = self::db()->fetchRow($sql);
		if($ad){
			$ad->images = self::db()->fetchAll('
					SELECT 
						i.hash, i.ext
					FROM images i
					INNER JOIN classified_ads_images cai ON i.id = cai.image_id
					WHERE ad_id = ?
				', array($ad->id));
		}
		
		return $ad;
	}

	public function repostAd($id, $user_id)
	{
		$sql = "
			SELECT  true
			FROM classified_ads ca
			WHERE id = ? AND user_id = ? and status = 3 and is_disabled = 0
		";

		$result = self::db()->fetchOne($sql, array($id, $user_id));
		if($result){
			$updated = self::db()->query("UPDATE classified_ads SET approvation_date = null, expiration_date = null, status = 1 WHERE id = {$id}");
			if($updated){
				return true;
			}
		}
		return false;
	}

	public function deleteAd($id, $user_id)
	{
		$sql = "
			SELECT  true
			FROM classified_ads ca
			WHERE id = ? AND user_id = ?
		";

		$result = self::db()->fetchOne($sql, array($id, $user_id));
		if($result){
			self::db()->query("delete from classified_ads_images WHERE ad_id = {$id}");
			self::db()->query("delete from classified_ads_cities WHERE ad_id = {$id}");
			self::db()->query("delete from classified_ads WHERE id = {$id}");

			$sql = '
				SELECT
					TRUE
				FROM classified_ads_updates
				WHERE ad_id = ? AND is_sync_done = 0
			';

			$exists = self::db()->fetchOne($sql,array($id));
			if(!$exists){
				self::db()->insert('classified_ads_updates', array('ad_id' => $id, 'event_type' => self::EVENT_TYPE_REMOVED));
			} else{
				self::db()->query('UPDATE classified_ads_updates SET event_type = ? WHERE ad_id = ? AND is_sync_done = 0', array( self::EVENT_TYPE_REMOVED, $id ));
			}
			return true;
		}
		return false;
	}


	public function payIvrA6($data)
	{
		$id = $data['ad_id'];
		$premium_activation_date = new Zend_Db_Expr('NOW()');
		$premium_expiration_date = new Zend_Db_Expr('DATE_ADD(NOW(), INTERVAL ' . $data['package_period'] . ' DAY)');
		try {
			self::db()->query("UPDATE classified_ads SET is_premium = 1, premium_activation_date = {$premium_activation_date}, premium_expiration_date = {$premium_expiration_date} WHERE id = {$id}");
			self::db()->insert('classified_ads_transfers', $data);

			$sql = '
				SELECT
					TRUE
				FROM classified_ads_updates
				WHERE ad_id = ? AND is_sync_done = 0
			';

			$exists = self::db()->fetchOne($sql,array($id));
			if(!$exists){
				self::db()->insert('classified_ads_updates', array('ad_id' => $id, 'event_type' => self::EVENT_TYPE_UPDATED));
			} else{
				self::db()->query('UPDATE classified_ads_updates SET event_type = ? WHERE ad_id = ? AND is_sync_done = 0', array( self::EVENT_TYPE_UPDATED, $id ));
			}
			
			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	public function checkAdByUserId($user_id, $id)
	{
		$ad = self::db()->fetchOne('SELECT TRUE  FROM classified_ads WHERE user_id = ? AND id = ? AND (status = 2 OR status = 1) AND is_disabled = 0', array($user_id, $id));

		return $ad;
	}

}
