<?php

class Cubix_Api_Module_Users extends Cubix_Api_Module
{
	const LOGIN_BLOCKED_IP = 1;
	const LOGIN_BLOCKED_USER = 2;
	const LOGIN_FAIL_ATTEMPT = 3;
	
	public function getDataByUsername($username)
	{		
		$user = self::db()->fetchRow('
			SELECT id, user_type, email FROM users
			WHERE username = ?
		', array($username));
		
		if ( ! $user ) return false;
		
		$phone = "";
		if ( $user->user_type == 'escort' ) {
			$phone = self::db()->fetchOne('
				SELECT ep.contact_phone_parsed FROM escort_profiles_v2 ep
				INNER JOIN escorts e ON e.id = ep.escort_id
				WHERE e.user_id = ?
			', array($user->id));
		}
		else if ( $user->user_type == 'agency' ) {
			$phone = self::db()->fetchOne('SELECT phone FROM agencies WHERE user_id = ?', array($user->id));
		}
		
		$user->phone = $phone;
		
		return $user;
	}

	public function getBySigninHash($hash)
	{
		
		try {

			$sql = "SELECT id FROM users WHERE status <> -5 AND status <> -1 AND signin_hash = ? AND application_id = ?";

			if (in_array(self::app()->id, array(33, 22)))
				$sql .= " AND (user_type = 'escort' OR user_type = 'member')";

			$user_id = self::db()->fetchOne($sql, array($hash, self::app()->id));
			
			if ( ! $user_id ) {
				return null;
			}

			$user = self::getDataById($user_id);

			if ( $user->user_type == 'member' ) {
				$user->member_data = self::getMemberData($user_id);
			}

			if ( $user->user_type == 'agency' ) {
				$user->agency_data = self::getAgencyData($user_id);
			}

			if ( $user->user_type == 'escort' ) {
				$user->escort_data = self::getEscortData($user_id);
				if ( ! $user->escort_data ) {
					return null;
				}
			}

			return $user;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function removeSigninHash($id)
	{
		$result = self::db()->update('users', array('signin_hash' => ''), 'id = ' . $id);

		if ($result) {
			return true;
		} else {
			return false;
		}
	}
	
    public function deactivate($user_id)
    {
        self::db()->update('users', array('status' => '-4'), 'id = ' . $user_id);
    }

    public function addComment($field)
    {
        $comment_data = array(
            'remember_me' => NULL,
            'date' => date('Y-m-d H:i:s'),
            'email_sent' => 0,
            'comment' => 'Blacklisted IP',
            'admin_verified' => 0,
            'sales_user_id' => -1
        );
        foreach ($field as $key => $val)
        {
            $comment_data[$key] = $val;
        }

        return self::db()->insert('escort_comments', $comment_data);
    }

	public function getByUsernamePassword($username, $password, $ip = null)
	{
		try {
			$app_id = self::app()->id;
			if($app_id == APP_EF){
				
				if(self::ipIsBlocked($ip)){
					return array('error_status' => self::LOGIN_BLOCKED_IP);
				}
			}
						
			$salt_hash = self::getSaltByUsername($username);

			if ( ! $salt_hash ) {
				return null;
			}

            $password = Cubix_Salt::hashPassword($password, $salt_hash);

			$sql = "SELECT id FROM users WHERE status <> -5 AND username = ? AND password = ? ";

			if (in_array($app_id, array(33, 22)))
				$sql .= " AND (user_type = 'escort' OR user_type = 'member')";
			
			// if(self::app()->id == APP_EF){
			// 	$sql .= " AND (fail_login_time < '" .(time() - (FAIL_LOGIN_CAPTCH_MIN * 60))."' OR fail_login_count < ".FAIL_LOGIN_CAPTCH_COUTN.")";
			// }

            $user_id = self::db()->fetchOne($sql, array($username, $password));

			if ( ! $user_id ) {
				if($app_id == APP_EF){
					$by_username = self::db()->fetchRow('
						SELECT id, email, fail_login_time, fail_login_count FROM users
						WHERE username = ?
					', array($username));
					
					if($by_username){
						$by_username->fail_login_count = self::failLoginAttempt($by_username->id, $ip, $by_username->fail_login_time, $by_username->fail_login_count);
						return array(
							'error_status' => self::LOGIN_FAIL_ATTEMPT,
							'email' => $by_username->email,
							'fail_count' => $by_username->fail_login_count
						);
					}
				}
					
				return null;
			}

			if($app_id == APP_EF){
				
				if(self::userIsBlocked($user_id, $ip)){
					return array('error_status' => self::LOGIN_BLOCKED_USER);
				}
				self::clearFailLoginCount($user_id);
			}
						
			$user = self::getDataById($user_id);


			if ( $user->user_type == 'member' ) {
				$user->member_data = self::getMemberData($user_id);
			}

			if ( $user->user_type == 'agency' ) {
				$user->agency_data = self::getAgencyData($user_id);
                if (in_array($app_id, array(APP_ED,APP_EG_CO_UK))){
                    $user->need_phone_verification = self::agencyNeedPhoneVerification($user_id);
                }
			}

			if ( $user->user_type == 'escort' ) {
				$user->escort_data = self::getEscortData($user_id);
				
				if (!$user->escort_data ) {
					return null;
				}
				
				if (in_array($app_id, array(APP_ED,APP_EG_CO_UK))){
					$user->need_phone_verification = self::needPhoneVerification($user->escort_data->escort_id);
				}
			}

			// Last login 
			/*if($user->status == STATUS_ACTIVE){
				$country_id = null;

				if ($geoData = self::getClientLocation($ip))
				{
					$country_iso = $geoData['country_iso'];
					$country = self::getCountryByIso($country_iso);
					if ($country)
						$country_id = intval($country->id);
				}
				
				self::db()->insert('users_last_login', array(
					'user_id' => $user_id,
					'ip' => $ip,
					'country_id' => $country_id
				));
			}*/
			//

			return $user;


		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public static function getMemberData($user_id)
	{
		if ( is_array($user_id) ) $user_id = intval($user_id);

		return self::db()->fetchRow('
			SELECT
				*
			FROM members WHERE user_id = ?
		', array($user_id));
	}

	public static function getUserData($user_id)
	{
		if ( is_array($user_id) ) $user_id = intval($user_id);

		return self::db()->fetchRow('
			SELECT
				*
			FROM users WHERE id = ?
		', array($user_id));
	}

	public static function getAgencyData($user_id)
	{
		if ( is_array($user_id) ) $user_id = intval($user_id);
		$app_id = self::app()->id;
		
		$f = '';
		if ( $app_id == APP_A6 ) {
			$f .= ' , name, address, zip, fake_city_id, email, web AS website, phone AS phone_number ';
		}
	 	
		if ( $app_id == APP_EF ) {
			$f .= ' , name ';
		}else if( $app_id ) {
            $f .= ' , city_id, country_id ';
        }
		
		$agency_data = self::db()->fetchRow('
			SELECT
				id AS agency_id ' . $f . '
			FROM agencies WHERE user_id = ?
		', array($user_id));
		
		if($app_id == APP_EF){
			$escorts = self::db()->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetchAll();
		
			if ($escorts){
				$arr = array();

				foreach ($escorts as $escort){
					$arr[] = $escort->id;
				}
				
				$agency_data->has_active_package = self::db()->fetchOne('SELECT	TRUE FROM order_packages WHERE escort_id IN (' . implode(',', $arr) . ') AND order_id IS NOT NULL AND status = ?', 2);
			}
			else{
				$agency_data->has_active_package = false;
			}
		}
		
		return $agency_data;
	}

	public static function getEscortData($user_id)
	{
		if ( is_array($user_id) ) $user_id = intval($user_id);
		
		$app_id = self::app()->id;
		if ( in_array($app_id, array(APP_EF))){
            $escort_data =  self::db()->fetchRow('
				SELECT
					e.id AS escort_id,
					e.status AS escort_status,
					e.showname,	e.gender,
					ep.hash as photo_hash, ep.ext as photo_ext, e.block_cam
				FROM escorts e
				LEFT JOIN escort_photos ep ON ep.escort_id = e.id AND ep.is_main = 1
				WHERE e.user_id = ?  
		    ', array($user_id));
			
			if ($escort_data)
			{			
				$escort_data->has_active_package = self::db()->fetchOne('SELECT	TRUE FROM order_packages WHERE escort_id = ? AND order_id IS NOT NULL AND status = ?', array($escort_data->escort_id, 2));
			}
			
			return $escort_data;
			
        }
        if ( in_array($app_id, array(APP_A6, APP_6B, APP_EG_CO_UK, APP_ED, APP_AE))){
            return self::db()->fetchRow('
                SELECT
                    id AS escort_id, status as escort_status, showname
                FROM escorts WHERE user_id = ?
		    ', array($user_id));
        }else if (in_array($app_id, [APP_BL])){
            return self::db()->fetchRow('
                SELECT
                    e.id AS escort_id, e.status as escort_status, e.showname,
                   e.city_id, e.country_id, package_id
				FROM escorts  e
				LEFT JOIN order_packages op ON op.escort_id = e.id AND op.status = 2   
				WHERE user_id = ?
		    ', array($user_id));
        }else{
            return self::db()->fetchRow('
                SELECT
                    id AS escort_id
                FROM escorts WHERE user_id = ? AND status <> 16
            ', array($user_id));
        }
	}

	public function getEscortDataTemp()
	{
		$escort_data =  self::db()->fetchAll('
			SELECT
				e.id AS escort_id, u.id,
				e.status AS escort_status,
				e.showname,	e.gender, u.email, u.cams_fake_email,
				true as has_active_package
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN order_packages op ON op.escort_id = e.id 
			INNER JOIN escort_skype es ON es.escort_id = op.escort_id
			WHERE op.order_id IS NOT NULL AND op.status = 2 AND es.status = 1 AND e.id = 110082 
			GROUP BY op.escort_id
		');

		/*if ($escort_data)
		{			
			$escort_data->has_active_package = self::db()->fetchOne('SELECT	TRUE FROM order_packages WHERE escort_id = ? AND order_id IS NOT NULL AND status = ?', array($escort_data->escort_id, 2));
		}*/

		return $escort_data;
	}
	
	public static function needPhoneVerification($escort_id)
	{
		$data = self::db()->fetchRow('SELECT e.last_hand_verification_date, ep.phone_exists FROM escorts e
									INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
									WHERE e.id = ?', $escort_id);
		
		if(is_null($data->last_hand_verification_date) && $data->phone_exists){
			return true;
		}
		else{
			return false;
		}
	}

    public static function needPhoneVerificationByUserId($user_id)
    {
        $data = self::db()->fetchRow('SELECT
                                                e.last_hand_verification_date,
                                                ep.phone_exists,
                                                ep.contact_phone_parsed 
                                            FROM
                                                escorts e
                                                INNER JOIN users u ON u.id = e.user_id
                                                INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id 
                                            WHERE
                                             u.id = ?', $user_id);

//        if (in_array(Cubix_Application::getId(), array(APP_ED))) {
//            $us_canada_pattern = "/^001([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})([0-9]{4})$/i";
//            $is_us_canada = preg_match($us_canada_pattern, $data->contact_phone_parsed);
//            if ($is_us_canada)
//                return false;
//        }
        if(is_null($data->last_hand_verification_date) && $data->phone_exists){
            return true;
        }
        else{
            return false;
        }
    }

    public static function agencyNeedPhoneVerification($user_id)
    {
        $last_hand_verified = self::db()->fetchRow('SELECT last_hand_verification_date as verification_date FROM agencies WHERE user_id = ?', $user_id);
        if (!is_null($last_hand_verified->verification_date)) return false;

        $verified_Phones_blank = self::db()->fetchAll('SELECT phone FROM agencies_phone_verification WHERE user_id = ? AND STATUS = 1', $user_id);
        $verified_Phones = array();
        foreach ($verified_Phones_blank as $item)
        {
            $verified_Phones[] = $item->phone;
        }
        $where1 = '';
        $where2 = '';
        if ( count($verified_Phones) > 0 )
        {
            $where1 = 'AND ag.contact_phone_parsed NOT IN ('.implode(',',$verified_Phones).')';
            $where2 = 'AND epv.contact_phone_parsed NOT IN ('.implode(',',$verified_Phones).')';
        }
        $sql = 'SELECT
                        ag.contact_phone_parsed phone
                    FROM
                        agencies ag 
                    WHERE
                        ag.user_id = ? AND ag.contact_phone_parsed IS NOT NULL '.$where1.'  UNION ALL
                    SELECT
                        epv.contact_phone_parsed phone 
                    FROM
                        agencies ag
                        INNER JOIN escorts e ON e.agency_id = ag.id
                        INNER JOIN escort_profiles_v2 epv ON e.id = epv.escort_id 
                    WHERE
                        ag.user_id = ? 
                        AND epv.contact_phone_parsed IS NOT NULL  '.$where2.'
                    GROUP BY
                        epv.contact_phone_parsed 
                        LIMIT 6';

        $phones =  self::db()->fetchAll($sql, array($user_id,$user_id));

        if( empty($phones) && empty( $verified_Phones ) )
        {
            return false;
        }

        if( count($phones) > 0 ){
            /*if (in_array(Cubix_Application::getId(), array(APP_ED))) {
                $us_canada_pattern = "/^001([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})([0-9]{4})$/i";
                foreach ($phones as $phone) {
                    if (preg_match($us_canada_pattern, $phone->phone)) {
                        return false;
                    }
                }
            }*/
            return true;
        }
        else{
            return false;
        }
    }

	public function updateLastLoginDate($user_id, $ip = null, $user_agent = null, $session_id = null)
	{
		if ( is_array($user_id) ) $user_id = intval(reset($user_id));
		if ( is_array($ip) ) $ip = reset($ip);
		if ( is_array($user_agent) ) $user_agent = reset($user_agent);
		if ( is_array($session_id) ) $session_id = reset($session_id);
		
		if ($ip)
		{
			$ips = explode(',', $ip);
			$ip = trim($ips[0]);
		}

		// <editor-fold defaultstate="collapsed" desc="If there is/are other users who has/have logged in from the same ip">
		$sql = '
			SELECT DISTINCT(ull.user_id) AS user_id
			FROM users_last_login ull
			WHERE ip = ? AND ull.user_id <> ?
		';
		// try to find other users with same ip
		$users = self::db()->fetchAll($sql, array($ip, $user_id), Zend_Db::FETCH_OBJ);
		if ( count($users) ) {
			// if we don't have warning about this user, just add it
			if ( ! ($wid = self::db()->fetchOne('SELECT id FROM suspicious_warning WHERE user_id = ?', $user_id)) ) {
				self::db()->insert('suspicious_warning', array('user_id' => $user_id, 'date' => new Zend_Db_Expr('NOW()')));
				$wid = self::db()->lastInsertId();
			}
			// else if there is warning about this user, just update it's date
			else {
				self::db()->update('suspicious_warning', array('date' => new Zend_Db_Expr('NOW()')), self::db()->quoteInto('id = ?', $wid));
			}

			// delete old records of matches from database
			self::db()->query('DELETE FROM suspicious_warning_user WHERE warning_id = ? AND type = 1', $wid);

			// and add them back again (the list may differ from old warning)
			foreach ( $users as $user ) {
				self::db()->insert('suspicious_warning_user', array('warning_id' => $wid, 'user_id' => $user->user_id, 'type' => 1));
			}
		}
		// </editor-fold>
		
		/* Last login */			
		$country_id = null;
		
		if ($geoData = Cubix_Geoip::getClientLocation($ip))
		{
			$country_iso = $geoData['country_iso'];
			$country = self::getCountryByIso($country_iso);
			if ($country)
				$country_id = intval($country->id);
		}

		$ull_data = array(
			'user_id' => $user_id,
			'ip' => $ip,
			'country_id' => $country_id,
			'user_agent' => $user_agent,
			'session_id' => $session_id
		);

		$app_id = self::app()->id;
		if($app_id == APP_ED){
			$is_ip_blacklisted = self::db()->fetchOne('SELECT block FROM blacklisted_ips WHERE ip = ?', $ip);
			if($is_ip_blacklisted){
				$ull_data['ip_blacklisted'] = 1;
			}
		}

		self::db()->insert('users_last_login', $ull_data);
		//
		if (in_array( $app_id, array(APP_ED,APP_EG_CO_UK) ))
        {
            return self::db()->update('users', array('last_login_date' => new Zend_Db_Expr('NOW()'), 'last_ip' => $ip, 'expiration_date' => null), self::db()->quoteInto('id = ?', $user_id));
        }else{
            return self::db()->update('users', array('last_login_date' => new Zend_Db_Expr('NOW()'), 'last_ip' => $ip), self::db()->quoteInto('id = ?', $user_id));
        }
	}

	private static function _updateLastLoginDate($user_id, $ip = null, $user_agent = null, $session_id = null)
	{
		if ( is_array($user_id) ) $user_id = intval(reset($user_id));
		if ( is_array($ip) ) $ip = reset($ip);
		if ( is_array($user_agent) ) $user_agent = reset($user_agent);
		if ( is_array($session_id) ) $session_id = reset($session_id);
		
		if ($ip)
		{
			$ips = explode(',', $ip);
			$ip = trim($ips[0]);
		}

		// <editor-fold defaultstate="collapsed" desc="If there is/are other users who has/have logged in from the same ip">
		$sql = '
			SELECT DISTINCT(ull.user_id) AS user_id
			FROM users_last_login ull
			WHERE ip = ? AND ull.user_id <> ?
		';
		// try to find other users with same ip
		$users = self::db()->fetchAll($sql, array($ip, $user_id), Zend_Db::FETCH_OBJ);
		if ( count($users) ) {
			// if we don't have warning about this user, just add it
			if ( ! ($wid = self::db()->fetchOne('SELECT id FROM suspicious_warning WHERE user_id = ?', $user_id)) ) {
				self::db()->insert('suspicious_warning', array('user_id' => $user_id, 'date' => new Zend_Db_Expr('NOW()')));
				$wid = self::db()->lastInsertId();
			}
			// else if there is warning about this user, just update it's date
			else {
				self::db()->update('suspicious_warning', array('date' => new Zend_Db_Expr('NOW()')), self::db()->quoteInto('id = ?', $wid));
			}

			// delete old records of matches from database
			self::db()->query('DELETE FROM suspicious_warning_user WHERE warning_id = ? AND type = 1', $wid);

			// and add them back again (the list may differ from old warning)
			foreach ( $users as $user ) {
				self::db()->insert('suspicious_warning_user', array('warning_id' => $wid, 'user_id' => $user->user_id, 'type' => 1));
			}
		}
		// </editor-fold>
		
		/* Last login */			
		$country_id = null;

		if ($geoData = Cubix_Geoip::getClientLocation($ip))
		{
			$country_iso = $geoData['country_iso'];
			$country = self::getCountryByIso($country_iso);
			if ($country)
				$country_id = intval($country->id);
		}

		self::db()->insert('users_last_login', array(
			'user_id' => $user_id,
			'ip' => $ip,
			'country_id' => $country_id,
			'user_agent' => $user_agent,
			'session_id' => $session_id
		));
		//
		
		
		return self::db()->update('users', array('last_login_date' => new Zend_Db_Expr('NOW()'), 'last_ip' => $ip), self::db()->quoteInto('id = ?', $user_id));
	}
	
	public static function getDataById($user_id)
	{
		if ( is_array($user_id) ) $user_id = intval(reset($user_id));

		$filed = '';
		if(self::app()->id == APP_ED){
			$filed .= " ,lang, pm_is_blocked";
		}
		elseif(self::app()->id == APP_EF){
			$filed .= " ,password_reset_required, cams_fake_email";
		}
		elseif(self::app()->id == APP_BL){
			$filed .= " ,cams_fake_email";
		}elseif(self::app()->id == APP_EG_CO_UK){
            $filed .= " ,sales_user_id";
        }
		return self::db()->fetchRow('
			SELECT 
				id, user_type, username, status, country_id, city_id, email, im_is_blocked, last_login_date, activation_hash,
				recieve_newsletters, allow_show_online '.$filed.'
			FROM users WHERE id = ?
		', array($user_id));
	}

	protected static function getSaltByUsername($username)
	{
		$salt = self::db()->fetchOne('
			SELECT salt_hash
			FROM users
			WHERE username = ? AND status <> -5
		', array($username));
		
		if ( ! $salt ) {
			return null;
		}
		
		return $salt;
	}
	
	public function isPasswordTrue($user_id, $password)
	{		
		$salt_hash = self::getSaltById($user_id);
		
		$password = Cubix_Salt::hashPassword($password, $salt_hash);
						
		$user = self::db()->fetchRow('
			SELECT TRUE
			FROM users
			WHERE id = ? AND password = ?
		', array($user_id, $password));
		
		if ( ! $user ) {
			return false;
		}
		
		return true;
	}
	
	protected static function getSaltById($user_id)
	{
		$salt = self::db()->fetchOne('
			SELECT salt_hash
			FROM users
			WHERE id = ?
		', array($user_id));
		
		if ( ! $salt ) {
			return null;
		}
		
		return $salt;
	}
	
	public function updatePassword($user_id, $new_pass)
	{
		if ( is_array($user_id) ) $user_id = reset($user_id);
		if ( is_array($new_pass) ) $new_pass = reset($new_pass);

		$salt_hash = Cubix_Salt::generateSalt($new_pass);
		
		$pass = Cubix_Salt::hashPassword($new_pass, $salt_hash);
		
		try {
			self::db()->update('users', array('password' => $pass, 'salt_hash' => $salt_hash), self::db()->quoteInto('id = ?', $user_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return true;
	}
	
	
	
	public function getByUsername($username)
	{		
		try {
			return self::db()->fetchRow('
				SELECT u.id FROM users u
				LEFT JOIN escorts e ON e.user_id = u.id WHERE u.username = ? AND NOT ( IF(e.status IS NOT NULL ,e.status, 0) & ?) AND u.status <> -5
			', array($username, Cubix_EscortStatus::ESCORT_STATUS_DELETED));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function changeSaleForUsaAndFrance($userId, $countryId, $backend_user_id, $userType)
	{
        $salesId = 1; //For France - admin

        if ($countryId === 68) {
            $salesId = 205; //For USA - USA 1
        }

        try {
            self::db()->update('users', array('sales_user_id' => $salesId), self::db()->quoteInto('id = ?', $userId));

            //implementing query which creates a logs in 'sales_change_history' table when escort country changes
            self::db()->insert('sales_change_history', array('user_id' => $userId, 'modifier_id' => SALES_CHANGED_BY_ESCORT_MODIFIER_ID, 'user_type' => $userType, 'from_sales_id' => $backend_user_id, 'to_sales_id' => $salesId, 'date' => new Zend_Db_Expr('NOW()') ) );
        }
        
        catch ( Exception $e ) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
	}
	
	public function getByEmail($email)
	{		
		return self::db()->fetchRow('
			SELECT id FROM users WHERE email = ? AND status <> -5
		', $email);
	}
	
	public function getById($id)
	{
		$member_fields = ", m.about_me ";
		if(self::app()->id == APP_ED){
			$member_fields .= " ,m.hash, m.ext,m.gender , m.show_gender, m.show_location, m.show_about_me, m.show_languages, m.langs, m.interests, m.show_interests";
		}
		
		$user = self::db()->fetchRow('
			SELECT u.* '. $member_fields . '
			FROM users u
			LEFT JOIN members m ON u.id = m.user_id
			WHERE u.id = ?
		', array($id));
		
		if ( ! $user ) {
			return null;
		}
		
		return $user;
	}
	
	public function activate($email, $hash)
	{
		$fields = '';
		try{
			if(self::app()->id == APP_ED){
				$fields .= ",sales_user_id , lang";
			}
			$user = self::db()->fetchRow('
				SELECT id, email, username, user_type'. $fields .' FROM users WHERE email = ? AND activation_hash = ?
			', array($email, $hash));
			
			if ( $user ) {
				self::db()->update('users', array(
					'activation_hash' => null,
					'status' => STATUS_ACTIVE
				), array(
					self::db()->quoteInto('id = ?', $user->id)
				));
				
				return $user;
			}
			
			return $user;	
		}catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function updateProfile($id, $data)
	{
		try {
			self::db()->update('members', $data, self::db()->quoteInto('user_id = ?', $id));
			unset($data['about_me']);
			
			if(self::app()->id == APP_ED){
				unset($data['gender']);
				unset($data['interests']);
				unset($data['show_interests']);
				unset($data['show_gender']);
				unset($data['show_location']);
				unset($data['show_about_me']);
				unset($data['show_languages']);
				unset($data['langs']);
			}
			self::db()->update('users', $data, self::db()->quoteInto('id = ?', $id));
			
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return true;
	}
	
	public function save($data)
	{
		try {
			$data['username'] = strtolower($data['username']);
			self::db()->insert('users', $data);
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return self::db()->lastInsertId();
	}

	public function updateActivationHash($user_id)
	{
		$hash = md5(microtime() * rand());
		try {			
			self::db()->update('users', array('activation_hash' => $hash), self::db()->quoteInto('id = ?', $user_id));
			
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return $hash;
	}

	
	
	//
	
	const BAKCNED_SUPER_ADMIN = 'superadmin';
	const BACKEND_SALES_PERSON = 'sales manager';
	const BACKEND_ADMIN = 'admin';
	
	public function getMostFreeSalesId()
	{
		$sales_persons = self::db()->fetchOne('
			SELECT
				bu.id, bu.username, COUNT(u.id) AS escorts_count
			FROM backend_users bu
			LEFT JOIN users u ON u.sales_user_id = bu.id AND u.application_id = bu.application_id
			WHERE 
				bu.is_disabled = FALSE AND 
				bu.type = ? AND 
				bu.application_id = ?
			GROUP BY bu.id
			ORDER BY escorts_count ASC
			LIMIT 1
		', array(self::BACKEND_SALES_PERSON, self::app()->id));
		
		return $sales_persons;
	}

	public function getRandomSalesPerson()
	{
		$where = ''; 

		if ( self::app()->id == 1 ) {
			//FOR SANDRA
			$where = " AND id <> 4 ";
		}
		if ( self::app()->id == 2 ) {
			$where = " AND id IN (/*16,*/ 19) ";
		}
		else if ( self::app()->id == 5 ) {
			$where = " AND id IN (101, 102) ";
		}
		else if ( self::app()->id == 7 ) {
			$where = " AND id IN (73, 76) ";
		}
		else if ( self::app()->id == APP_6B ) {
			$where = " AND id IN (84, 83) ";
		}
		else if ( self::app()->id == 18 ) {
			$where = " AND id IN (74, 79) ";
		}
		
		if ( self::app()->id != 1 ) {
			$user = self::db()->fetchRow("
				SELECT
					id
				FROM backend_users
				WHERE is_disabled <> 1 AND application_id = ? AND (type = ? OR type = ?) {$where}
				ORDER BY RAND()
				LIMIT 1
			", array(self::app()->id, self::BACKEND_ADMIN, self::BACKEND_SALES_PERSON));
		}
		else {
			$user = self::db()->fetchRow("
				SELECT
					id
				FROM backend_users
				WHERE is_disabled <> 1 AND application_id = ? AND type = ? {$where}
				ORDER BY RAND()
				LIMIT 1
			", array(self::app()->id, self::BACKEND_SALES_PERSON));
		}
		
		return $user->id;
	}

	public function getSalesPersons()
	{
		return self::db()->fetchAll('
			SELECT
				id, username, first_name, last_name
			FROM backend_users
			WHERE is_disabled = FALSE AND application_id = ? AND type = ?
		', array(self::app()->id, self::BACKEND_SALES_PERSON));
	}

	public function getEdSalesPersons()
    {
        return self::db()->fetchAll('
			SELECT
				id, username, first_name, last_name
			FROM backend_users
			WHERE is_disabled = FALSE AND application_id = ? AND (type = ? || type = ?)
		', array(self::app()->id, self::BACKEND_SALES_PERSON, self::BACKEND_ADMIN));
    }

    public function getEdSalesPersonsForPhoneVerifications()
    {
        return self::db()->fetchAll('
			SELECT
				id, username, first_name, last_name , phone , working_phone
			FROM backend_users
			WHERE is_disabled = FALSE AND application_id = ? AND (type = ? || type = ?) AND phone_verification_allowed = 1
		', array(self::app()->id, self::BACKEND_SALES_PERSON, self::BACKEND_ADMIN));
    }

	public function getSalesPersonById($sales_id)
	{		
		return self::db()->fetchRow('
			SELECT
				id, username, first_name, last_name, email
			FROM backend_users
			WHERE is_disabled = FALSE AND application_id = ? AND ( type = ? OR type = ? ) AND id = ?
		', array(self::app()->id, self::BACKEND_SALES_PERSON, self::BACKEND_ADMIN, $sales_id));
	}

	public function getSalesAdminPersons()
	{
		return self::db()->fetchAll('
			SELECT
				id, username, first_name, last_name
			FROM backend_users
			WHERE is_disabled = FALSE AND application_id = ? AND ( type = ? OR type = ? )
		', array(self::app()->id, self::BACKEND_SALES_PERSON, self::BACKEND_ADMIN));
	}

	public function getSalesEmails()
	{
		return self::db()->fetchAll('
			SELECT
				id, LOWER(username) as username, email
			FROM backend_users
			WHERE is_disabled = FALSE AND application_id = ? AND type = ?
		', array(self::app()->id, self::BACKEND_SALES_PERSON));
	}

    public function getSalesEmail($user_id)
    {
        return self::db()->fetchOne('
			SELECT
				bu.email
			FROM backend_users bu
			LEFT JOIN users u ON u.sales_user_id = bu.id 
			WHERE u.id = ?
		', array($user_id));
    }
	
	public function getSalesSkypeStatuses()
	{
		return self::db()->fetchAssoc('
			SELECT
				skype, skype_status
			FROM backend_users
			WHERE is_disabled = FALSE AND application_id = ? AND type = ?
		', array(self::app()->id, self::BACKEND_SALES_PERSON));
	}
	
	public function getSalesSkypeStatusById($bu_id)
	{
		return self::db()->fetchOne('
			SELECT
				skype_status
			FROM backend_users
			WHERE id = ? 
		', array($bu_id));
	}
	
	public function getUserSalesPerson($user_id)
	{		
		return self::db()->fetchRow('
			SELECT
                bu.id, bu.username, bu.first_name, bu.last_name, bu.email, bu.skype, bu.phone 
			FROM users u
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			WHERE u.id = ?
		', $user_id);
	}
	
	public function getUserSalesPersonById($id)
	{
		return self::db()->fetchRow('
			SELECT
                bu.id, bu.username, bu.first_name, bu.last_name, bu.email, bu.skype, bu.phone 
            FROM backend_users bu
			WHERE bu.id = ?
		', $id);
	}

	public function assignEscort($user_id, $escort_id)
	{
		$user_id = intval($user_id);
		if ( ! $user_id ) self::_getRandomSalesId();
		$escort_id = intval($escort_id);
		
		self::db()->query('
			UPDATE users u
			INNER JOIN escorts e ON e.user_id = u.id
			SET u.sales_user_id = ?
			WHERE e.id = ?
		', array($user_id, $escort_id));
	}
	
	public function assignAgency($user_id, $agency_id)
	{
		$user_id = intval($user_id);
		if ( ! $user_id ) $user_id = self::_getRandomSalesId();
		$agency_id = intval($agency_id);
		
		self::db()->query('
			UPDATE users u
			INNER JOIN agencies a ON a.user_id = u.id
			SET u.sales_user_id = ?
			WHERE a.id = ?
		', array($user_id, $agency_id));
	}
	
	public function assignToSales($sales_user_id, $user_id)
	{
		$sales_user_id = intval($sales_user_id);
		if ( ! $sales_user_id ) $sales_user_id = self::_getRandomSalesId();
		$user_id = intval($user_id);
		
		self::db()->query('
			UPDATE users u
			SET u.sales_user_id = ?
			WHERE u.id = ?
		', array($sales_user_id, $user_id));
	}
	
	protected static function _getRandomSalesId()
	{
		return self::db()->fetchOne('
			SELECT
				id
			FROM backend_users
			WHERE application_id = ? AND is_disabled = FALSE AND type = ?
			ORDER BY RAND()
			LIMIT 1
		', array(self::app()->id, self::BACKEND_SALES_PERSON));
	}
	
	public function isSalesValid($user_id)
	{
		$user_id = intval($user_id);
		
		return self::db()->fetchCol('
			SELECT
				TRUE
			FROM backend_users
			WHERE id = ? AND is_disabled = FALSE AND application_id = ? AND type = ?
		', array($user_id, self::app()->id, self::BACKEND_SALES_PERSON));
	}

	public function resetPassword($email)
	{
		$user = self::db()->fetchRow('
			SELECT id, username, email FROM users WHERE email = ?
		', $email);

		if ( ! $user ) {
			return NULL;
		}

		$password = md5(microtime() * rand());
		$password = substr($password, 0, 10);

		self::updatePassword($user->id, $password);

		$user->password = $password;

		return $user;
	}

	/* Geo IP */
	public static function getClientLocation($ip)
    {
		if (!function_exists('geoip_record_by_name'))
			return null;

		if (is_array($ip))
			$ip = reset($ip);

		try {
			$geoip = @geoip_record_by_name($ip);
		}
		catch (Exception $e)
		{
			return null;
		}
		
		if ($geoip)
		{
			$geoData = array(
				'country' => $geoip['country_name'],
				'country_iso' => $geoip['country_code'],
				'region' => $geoip['region'],
				'city' => $geoip['city'],
				'latitude' => $geoip['latitude'],
				'longitude' => $geoip['longitude']
			);

			return $geoData;
		}
		else
			return null;
    }

	public static function getCountryByIso($country_iso)
	{
		return self::db()->fetchRow('SELECT * FROM countries WHERE iso = ?', $country_iso);
	}

	public function forgotPassword($email)
	{
		$user = self::db()->fetchRow('
			SELECT id, username, email, user_type FROM users WHERE email = ? AND NOT status & 256
		', $email);

		if ( ! $user ) {
			return NULL;
		}
		$hash = md5(microtime() * rand());
		try {
			self::db()->update('users', array( 'email_hash' => $hash), self::db()->quoteInto('id = ?', $user->id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		$user->email_hash = $hash;

		return $user;
	}

	public function forgotPasswordByEmail($email)
	{
		$user = self::db()->fetchRow('
			SELECT id, username, email FROM users WHERE email = ? AND NOT status & 256
		', $email);

		if ( ! $user ) {
			return NULL;
		}
		$hash = md5(microtime() * rand());
		try {
			self::db()->update('users', array( 'email_hash' => $hash), self::db()->quoteInto('id = ?', $user->id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		$user->email_hash = $hash;

		return $user;
	}

	public function forgotPasswordByEmailUsername($email, $username)
	{
		$user = self::db()->fetchRow('
			SELECT id, username, email FROM users WHERE email = ? AND username = ? AND status != -5
		', array($email, $username));

		if ( ! $user ) {
			return NULL;
		}
		$hash = md5(microtime() * rand());
		try {
			self::db()->update('users', array( 'email_hash' => $hash), self::db()->quoteInto('id = ?', $user->id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		$user->email_hash = $hash;

		return $user;
	}

	public function forgotPasswordByUsername($username)
	{
		$user = self::db()->fetchRow('
			SELECT id, username, email FROM users WHERE username = ? AND NOT status & 256
		', $username);

		if ( ! $user ) {
			return NULL;
		}
		$hash = md5(microtime() * rand());
		try {
			self::db()->update('users', array( 'email_hash' => $hash), self::db()->quoteInto('id = ?', $user->id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		$user->email_hash = $hash;

		return $user;
	}

	public function passChangeNotRequired($username)
	{
		$user = self::db()->fetchRow('
			SELECT id, username, email FROM users WHERE username = ? AND NOT status & 256
		', $username);

		if ( ! $user ) {
			return NULL;
		}
		
		try {
			self::db()->update('users', array( 'password_reset_required' => 0), self::db()->quoteInto('id = ?', $user->id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return true;
	}

	public function passChangeNotRequiredById($id){
		$user = self::db()->fetchRow('
			SELECT id, username, email FROM users WHERE id = ? AND NOT status & 256
		', $id);

		if ( ! $user ) {
			return NULL;
		}
		
		try {
			self::db()->update('users', array( 'password_reset_required' => 0), self::db()->quoteInto('id = ?', $id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return true;
	}

	public function getByUsernameMailHash($username, $hash)
	{
			$id = self::db()->fetchOne('
			SELECT id FROM users WHERE username = ? AND email_hash = ?
		', array($username, $hash));
			return $id;
	}
	
	public static function checkUpdatePassword($id, $hash, $pass)
	{
		$check = self::db()->fetchOne('
			SELECT true FROM users WHERE id = ? AND email_hash = ? 
	  ', array($id, $hash));
		if($check){
			try {
				self::updatePassword($id, $pass);	
				self::db()->update('users', array( 'email_hash' => NULL), self::db()->quoteInto('id = ?', $id));
			}
			catch ( Exception $e ) {
				return array('error' => Cubix_Api_Error::Exception2Array($e));
			}
			return true; 
		}
		else{
			return false;
		}
	}

	public function addProblemReport($name, $email,$problem, $report, $escort_id, $user_id, $report_type = null, $photo_link = null)
	{		
		if (!is_null($user_id))
			$user_id = $user_id;
		
		try {

            if(in_array(Cubix_Application::getId(), array(APP_ED))){
                $arr = array(
                    'application_id'=> self::app()->id,
                    'name' => $name ,
                    'email' => $email ,
                    'problem'=>$problem,
                    'report' => $report,
                    'escort_id' => $escort_id,
                    'report_type' => $report_type,
                    'photo_link' => $photo_link,
                    'date' => new Zend_Db_Expr('NOW()')
                );
            }else{
                $arr = array(
                    'application_id'=> self::app()->id,
                    'name' => $name ,
                    'email' => $email ,
                    'problem'=>$problem,
                    'report' => $report,
                    'escort_id' => $escort_id,
                    'date' => new Zend_Db_Expr('NOW()')
                );
            }



			if (!is_null($user_id) && $user_id)
				$arr['user_id'] = $user_id;
			
			self::db()->insert('report_problem', $arr);
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function addSuspPhoto($data)
	{		
		try {
			$data['date'] = new Zend_Db_Expr('NOW()');
			
			self::db()->insert('suspicious_photos_requests', $data);
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function delete($id) 
	{
		try {
			self::db()->delete('users', self::db()->quoteInto('id = ?', $id));
		} catch (Exception $e) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function getAllEmails($date_from, $date_to, $app_id)
	{
		ini_set('memory_limit', '1024M');
		set_time_limit(0);

		if ( ! $date_to ) {
			$date_to = time();
		}

		$sql = "SELECT id AS user_id, email, username, user_type, date_registered, '" . Cubix_Application::getDefaultLang() . "' AS lang_id FROM users WHERE status = 1 AND date_registered >= FROM_UNIXTIME(?) AND date_registered <= FROM_UNIXTIME(?) AND application_id = ? ORDER BY id DESC";

		$data = self::db()->fetchAll($sql, array($date_from, $date_to, $app_id));

		return $data;
	}
	
	public function removeSystemDisable($id)
	{
		if (is_array($id)) $id = reset($id);
		
		self::db()->update('users', array('system_disabled' => 0), self::db()->quoteInto('id = ?', $id));
	}
	
	public function updateShowOnline($user_id, $flag)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($flag)) $flag = reset($flag);
		
		self::db()->update('users', array('allow_show_online' => $flag), self::db()->quoteInto('id = ?', $user_id));
		
		$escorts = self::db()->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetchAll();
		
		if ($escorts)
		{
			foreach ($escorts as $e)
				Cubix_SyncNotifier::notify($e->id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'change Now Online status to ' . $flag));
		}
	}

	public function updateEnablePm($user_id, $flag)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($flag)) $flag = reset($flag);

		self::db()->update('users', array('pm_is_blocked' => (int)$flag), self::db()->quoteInto('id = ?', $user_id));

//		$escorts = self::db()->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetchAll();
//
//		if ($escorts)
//		{
//			foreach ($escorts as $e)
//				Cubix_SyncNotifier::notify($e->id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'change Now Online status to ' . $flag));
//		}
	}

	public function getShowOnlineStatus($user_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		
		return self::db()->fetchOne('SELECT allow_show_online FROM users WHERE id = ?', $user_id);
	}
	
	//Need this function for switching agency
	//We have userId there and need to get new agency
	public function getAgencyUser($user_id)
	{
		$user = self::getDataById($user_id);
		$user->agency_data = self::getAgencyData($user_id);
		return $user;
		
	}

	public static function failLoginAttempt($user_id, $ip, $fail_login_time, $fail_login_count )
	{
		
		if($fail_login_time != null && ($fail_login_time > (time() - (FAIL_LOGIN_CAPTCH_MIN * 60)))) //fail login less X min
		{
			if($fail_login_count + 1 >= FAIL_LOGIN_BLOCK_USER_COUNT){
				$ipExist = self::db()->fetchRow('
					SELECT true FROM block_ip
					WHERE ip = ?
				', array($ip));

				if($ipExist){
					self::db()->update('block_ip', array('exp_time' => time() + (FAIL_LOGIN_BLOCK_USER_MIN * 60)), self::db()->quoteInto('ip = ?', $ip));	
				}else{
					self::db()->insert('block_ip', array(
						'ip' => $ip,
						'exp_time' => time() + (FAIL_LOGIN_BLOCK_USER_MIN * 60),
						'user_id' => $user_id,
					));	
				}
			}

			self::db()->update('users', array('fail_login_count' => $fail_login_count + 1, 'fail_login_time' => time()), self::db()->quoteInto('id = ?', $user_id));
			return $fail_login_count + 1;
		}else{
			self::db()->update('users', array('fail_login_count' => 1, 'fail_login_time' => time()), self::db()->quoteInto('id = ?', $user_id));
			return 1;
		}
		
	}

	public static function ipIsBlocked($ip){
		$ipExist = self::db()->fetchRow('
			SELECT true FROM block_ip
			WHERE ip = ? AND exp_time > ?
		', array( $ip, time()));
		
		
		if ( ! $ipExist ) return false;

		return true;
	}

	public static function userIsBlocked($id, $ip){

		$userBlocked = self::db()->fetchRow('
			SELECT true FROM block_ip
			WHERE user_id = ? AND exp_time > ?
		', array( $id, time()));
	
		if ( ! $userBlocked ) return false;

		$ipExist = self::db()->fetchRow('
						SELECT true FROM block_ip
						WHERE ip = ?
					', array($ip));

		if(!$ipExist){
			self::db()->insert('block_ip', array(
								'ip' => $ip,
								'exp_time' => time() + (FAIL_LOGIN_BLOCK_USER_MIN * 60),
								'user_id' => $id,
							));
		}
		return true;
	}

	public static function clearFailLoginCount($id){
		self::db()->update('users', array('fail_login_count' => 0, 'fail_login_time' => null), self::db()->quoteInto('id = ?', $id));	
	}

	public function getBlockedUsername(){

		$blockedUsername = self::db()->fetchAll('SELECT word FROM blocked_username');
		
		
		if ( ! $blockedUsername ) return false;

		return $blockedUsername;
	}

	public function get_last_login_date($userid){
		
		return self::db()->fetchOne('SELECT last_login_date FROM users WHERE id  = ?', array($userid));
	}
	public function get_last_login_date_v2($userid){

		return self::db()->fetchOne('SELECT login_date FROM users_last_login WHERE user_id  = ? ORDER BY login_date desc', array($userid));
	}

	public function get_status_by_email($email){
		
		return self::db()->fetchOne('SELECT status FROM users WHERE email  = ?', array($email));
	}

	public function getLang($userid, $page_lang){

		$lang = self::db()->fetchOne('SELECT lang FROM users WHERE id  = ?', array($userid));
		if(!$lang){
			$lang = $page_lang;
			self::db()->update('users', array('lang' => $page_lang), self::db()->quoteInto('id = ?', $userid));
		}
		
		return $lang;
	}
	// public function getPreferLang($id,$type){

	// 	if (is_array($id)) $id = reset($id);
	// 	if (is_array($type)) $type = reset($type);

	// 	if($type == 'escort'){
	// 		$lang = self::db()->fetchOne('SELECT u.lang FROM users u LEFT JOIN escorts e ON e.user_id = u.id WHERE id  = ?', array($userid));
	// 	}
	// 	return $lang;
	// }

    public function getUserActivationHash($userid){
        return self::db()->fetchOne('SELECT activation_hash FROM users WHERE id  = ?', array($userid));
    }


    public function getPmIsBlocked($shownameOrId,$type = 'escort'){

        $field = is_numeric($shownameOrId) ? ( $type == 'escort' ? 'id' :'agency_id') : 'showname' ;

        $sql = "SELECT u.pm_is_blocked from users as u 
                INNER JOIN escorts as e on u.id = e.user_id 
                WHERE u.application_id = ? AND  e." . $field." = ?";
        $row = self::db()->fetchOne($sql,  array(self::app()->id, $shownameOrId));
        return $row;
    }
    public function getPmIsBlockedByUserId($userID){

        $sql = "SELECT u.pm_is_blocked from users as u 
                WHERE u.application_id = ? AND u.id = ?;";
        $row = self::db()->fetchOne($sql,  array(self::app()->id, $userID));
        return $row;
    }

    public function getUserByToken($secure_token)
    {
		try {
			$sql = "SELECT id FROM users WHERE status <> -5 AND secure_token = ? AND expiration_date > now() AND application_id = ?";

			if (in_array(self::app()->id, array(33, 22)))
				$sql .= " AND (user_type = 'escort' OR user_type = 'member')";

			$user_id = self::db()->fetchOne($sql, array($secure_token, self::app()->id));

			if ( ! $user_id ) {
				return null;
			}

			$user = self::getDataById($user_id);

			if ( $user->user_type == 'member' ) {
				$user->member_data = self::getMemberData($user_id);
			}

			if ( $user->user_type == 'agency' ) {
				$user->agency_data = self::getAgencyData($user_id);
			}

			if ( $user->user_type == 'escort' ) {
				$user->escort_data = self::getEscortData($user_id);
				
				if ( ! $user->escort_data ) {
					return null;
				}
				
				if (in_array(self::app()->id, array(APP_ED))){
					$user->need_phone_verification = self::needPhoneVerification($user->escort_data->escort_id);
				}
			}

			return $user;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
    }

    public function insertSalesAgentIsChanged( $data )
    {
        try {
            $updateData = [];
            $updateData[] = SALES_CHANGE_SMS_IS_OLD_TRY ;
            $updateData[] = $data['user_id'];
            $updateData[] = SALES_CHANGE_APPROOVED ;
            $updateSql = ' UPDATE sales_agent_sms 
                            SET `status` = ?
                            WHERE
                                `user_id` = ?
                                AND `status` < ?';
            self::db()->fetchAll( $updateSql , $updateData );

            return self::db()->insert('sales_agent_sms', $data );
        } catch (Exception $e ) {
            return $e->getMessage();
        }
    }

    public function checkSmsCountIn24H( $user_id )
    {
        try {
            $sql = 'SELECT count(DATE_SUB(`created_at`, INTERVAL 24 HOUR)) as count FROM `sales_agent_sms` WHERE user_id = ?';
            return self::db()->fetchAll( $sql , $user_id );
        } catch (Exception $e ) {
            return $e->getMessage();
        }
    }

    public function checkSmsCode( $data )
    {
        try {
            $sql = 'SELECT
                    COUNT(*) as isset
                    FROM
                        `sales_agent_sms` 
                    WHERE
                        `code` = ? 
                        AND `user_id` = ? 
                        AND `status` = ?';
            return self::db()->fetchAll( $sql , $data );
        } catch (Exception $e ) {
            return $e->getMessage();
        }
    }

    public function changeStatus( $status )
    {
        try {
            $sql = 'UPDATE `sales_agent_sms`
                    SET `status` = ? 
                    WHERE
                        `user_id` = ? 
                        AND `code` = ?
                        AND `status` = ?';
            return self::db()->fetchAll( $sql , $status );
        } catch (Exception $e ) {
            return $e->getMessage();
        }
    }

    public function getCityByUserId( $user_id )
    {
        try{
            $sql = 'SELECT
                        city_id 
                    FROM
                        `escorts` 
                    WHERE
                        user_id = ?';
            $result = self::db()->fetchRow($sql, $user_id);

            return $result;
        }catch (Exception $e){
            return $e;
        }
    }

    public function getCityLocations($city_id)
    {
        try {
            $sql = 'SELECT longitude, latitude 
                    FROM `cities` 
                    WHERE id = ?';

            return self::db()->fetchRow($sql, $city_id);
        } catch (Exception $e) {
            return $e;
        }
    }

    public function updateSalesHash($user_id,$new_sales,$new_sales_hash)
    {
        return self::db()->update('users', array('new_sales_user_id' => $new_sales, 'new_sales_hash' => $new_sales_hash), 'id = '.$user_id);
    }
	
	public function saveCamsFakeEmail($user_id, $email)
	{
		return self::db()->update('users', array('cams_fake_email' => $email), 'id = '.$user_id);
	}		

    public function getLastUserId()
    {
        try{
            $sql = 'SELECT
                        id
                    FROM
                        `users` 
                    ORDER BY id DESC 
                    LIMIT 1';
            $result = self::db()->fetchOne($sql);

            return $result;
        }catch (Exception $e){
            return $e;
        }
    }

    public function getIsOnline($id)
    {
        try{
            $sql = 'SELECT
                        is_online
                    FROM
                        `backend_users` 
                    WHERE id = '.$id;

            $result = self::db()->fetchOne($sql);

            return $result;
        }catch (Exception $e){
            return $e;
        }
    }
}
