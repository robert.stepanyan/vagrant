<?php

class Cubix_Api_Module_Cities extends Cubix_Api_Module
{
    public function saveGeoData($data)
    {
        $city_id = $data['city_id'];
        unset( $data['city_id'] );

        if( !$city_id ) return false;

        try {
            self::db()->update('cities', $data, self::db()->quoteInto('id = ?', $city_id));
        }
        catch ( Exception $e ) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

    public function getAvailableTags($cityId)
    {

        if( !$cityId ) return false;

        try {
            $escortsInCity = self::db()->fetchCol('SELECT
                                                                ec.escort_id
                                                            FROM
                                                                escort_cities ec
                                                                INNER JOIN escorts e ON e.id = ec.escort_id 
                                                            WHERE
                                                                ( e.`status` & ? OR e.STATUS & ? ) 
                                                                AND ec.city_id = ?',array(ESCORT_STATUS_ACTIVE,ESCORT_STATUS_OWNER_DISABLED,$cityId));


            $escortsTags = self::db()->fetchAll('SELECT
                                                        etg.tag_id,
                                                        tg.title 
                                                    FROM
                                                        escort_tags etg
                                                        LEFT JOIN tags tg ON etg.tag_id = tg.id 
                                                    WHERE
                                                        etg.escort_id IN (' . implode(',',$escortsInCity) . ') ');


            $cityTags = array();
            if ( count($escortsTags) )
            {
                foreach ( $escortsTags as $key => $tag )
                {
                    $cityTags[] = $tag->tag_id;
                }
            }

            $countTags = array_count_values($cityTags);

            $finalTagsArray = array();
            foreach ($countTags as $tagId => $tagCount)
            {
                if ($tagCount > 10)
                {
                    $finalTagsArray[] = $tagId;
                }
            }

            $defaultTags = self::db()->fetchAll('SELECT
                                                        id, title 
                                                    FROM
                                                        tags');

            foreach ($defaultTags as $k => $tag)
            {
                if( !in_array($tag->id,$finalTagsArray) )
                {
                    unset($defaultTags[$k]);
                }
            }

            return $defaultTags;
        }
        catch ( Exception $e ) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }


    public function getLocations($city_id)
    {
        try {
            $sql = 'SELECT longitude, latitude 
                    FROM `cities` 
                    WHERE id = ?';

            return self::db()->fetchRow($sql, $city_id);
        } catch (Exception $e) {
            return $e;
        }
    }
}