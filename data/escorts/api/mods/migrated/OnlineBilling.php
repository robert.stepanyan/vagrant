<?php

class Cubix_Api_Module_OnlineBilling extends Cubix_Api_Module
{
	
	//Orders activation condition
	const CONDITION_AFTER_RECEIVING_PAYMENT_DETAILS = 8;
	const CONDITION_AFTER_PAID = 5;
	const CONDITION_WITHOUT_PAYMENT = 10;
	
	//Packages activation type
	const ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES = 6;
	const ACTIVATE_AT_EXACT_DATE = 7;
	const ACTIVATE_ASAP = 9;
	
	//Packages statuses
	const PACKAGE_STATUS_PENDING  = 1;
	const PACKAGE_STATUS_ACTIVE   = 2;
	const PACKAGE_STATUS_EXPIRED  = 3;
	const PACKAGE_STATUS_CANCELLED = 4;
	const PACKAGE_STATUS_UPGRADED = 5;
	const PACKAGE_SUSPENDED = 6;

	//Order statuses
	const ORDER_STATUS_PENDING = 1;
	// const STATUS_CANCELLED = 2;
	const ORDER_STATUS_PAID = 3;
	// const STATUS_CHARGEBACK = 4;
	const ORDER_STATUS_PAYMENT_DETAILS_RECEIVED = 5;
	const ORDER_STATUS_PAYMENT_REJECTED = 6;
	const ORDER_STATUS_CLOSED = 7;
	
	//Transfers statuses
	const TRANSFER_STATUS_PENDING = 1;
	const TRANSFER_STATUS_CONFIRMED = 2;
	const TRANSFER_STATUS_REJECTED = 3;
	const TRANSFER_STATUS_AUTO_REJECTED = 4;
	
	const TRANSFER_TYPE_PHONE_BILLING = 7;
	
	const IVR_BACKEND_USER_ID = 100;
	
	const GOTD_STATUS_PENDING = 1;
	const GOTD_STATUS_ACTIVE  = 2;
	const GOTD_STATUS_EXPIRED = 3;
	
	const GOTD_ORDER_PENDING = 1;
	const GOTD_ORDER_PAID = 2;
	const GOTD_ORDER_EXPIRED = 3;
	
	/* EXTEND PACKAGE */
	const PACKAGE_DIAMOND_LIGHT = 102;
	const PACKAGE_DIAMOND_PREMIUM = 101;
	const PACKAGE_GOLD_TOUR_PREMIUM = 100;

	const DL_PRODUCT_PLUS_3_DAYS = 17;
	const DL_PRODUCT_PLUS_6_DAYS = 18;
	const DL_PRODUCT_PLUS_9_DAYS = 20;
	const DP_PRODUCT_PLUS_3_DAYS = 21;
	const DP_PRODUCT_PLUS_6_DAYS = 22;
	const DP_PRODUCT_PLUS_9_DAYS = 23;

	const GT_PRODUCT_PLUS_3_DAYS = 24;
	const GT_PRODUCT_PLUS_6_DAYS = 25;
	
	public static $EXTEND_PACKAGE_PRODUCTS = array(
		self::DL_PRODUCT_PLUS_3_DAYS,
		self::DL_PRODUCT_PLUS_6_DAYS,
		self::DL_PRODUCT_PLUS_9_DAYS,
		self::DP_PRODUCT_PLUS_3_DAYS,
		self::DP_PRODUCT_PLUS_6_DAYS,
		self::DP_PRODUCT_PLUS_9_DAYS,

		self::GT_PRODUCT_PLUS_3_DAYS,
		self::GT_PRODUCT_PLUS_6_DAYS,
	);
	/* EXTEND PACKAGE */
	
	public function getAgencyEscortsByPackage($package_id, $agency_id)
	{
		
		
		$sql = '
			SELECT
				e.id,
				e.showname,
				e.status,
				e.gender,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.application_id,
				e.verified_status 
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id
			INNER JOIN package_prices pp ON pp.gender = e.gender
			WHERE 
				u.application_id = ? 
				AND e.agency_id = ? 
				AND NOT e.status & ? 
				AND NOT e.status & ?
				AND NOT e.status & ?
				AND e.status & ?
				
				AND pp.package_id = ?
				AND pp.user_type = ?
 			GROUP BY e.id
		';

		try {
			$escorts = self::db()->query($sql, 
				array(
					Cubix_Application::getId(), 
					$agency_id, 
					ESCORT_STATUS_DELETED, 
					ESCORT_STATUS_TEMPRARY_DELETED, 
					ESCORT_STATUS_IS_NEW,
					ESCORT_STATUS_ACTIVE,
					
					$package_id,
					USER_TYPE_AGENCY
				)
			)->fetchAll();
			
			$exclude_statuses = array(self::PACKAGE_STATUS_ACTIVE, self::PACKAGE_STATUS_PENDING, self::PACKAGE_SUSPENDED);
			
			if ( count($escorts) ) {
				foreach( $escorts as $i => $escort ) {
					$escort_packages = self::_getEscortPackages($escort->id);
					if ( count($escort_packages) ) {
						foreach ( $escort_packages as $package ) {
							if ( in_array($package->status, $exclude_statuses) ) {
								unset($escorts[$i]);
							}
						}
					}
				}
			}
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $escorts;
	}
	
	private static function _getEscortPackages($escort_id)
	{
		$sql = "
			SELECT op.id, op.status
			FROM order_packages op
			WHERE op.escort_id = ? AND op.application_id = ? AND order_id IS NOT NULL
		";
		
		return self::db()->fetchAll($sql, array($escort_id, Cubix_Application::getId()));
	}

	public function getShoppingCart($user_id, $user_type)
	{
		
		
		$sql = "
			SELECT 
				sc.id AS shopping_cart_id, e.id AS escort_id, e.showname, p.id AS package_id, p.name, pp.price, sc.data
			FROM shopping_cart sc
			INNER JOIN escorts e ON e.id = sc.escort_id
			INNER JOIN packages p ON p.id = sc.package_id
			INNER JOIN package_prices pp ON pp.package_id = p.id AND e.gender = pp.gender
			WHERE
				sc.user_id = ? AND user_type = ?
			GROUP BY e.id
		";
		
		$packages = self::db()->fetchAll($sql, array($user_id, $user_type));
		
		if ( count($packages) ) {
			
			foreach ( $packages as $i => $package ) {
				$data = unserialize($package->data);
			
				$opt_products = $data['optional_products'];
				$gender = self::_getEscortGender($package->escort_id);
				if ( count($opt_products) ) {						
					$opt_prod_sql = "
						SELECT
							p.id, p.name, pr.price
						FROM products p
						INNER JOIN product_prices pr ON p.id = pr.product_id
						WHERE 
							p.id IN (" . implode(',', $opt_products) . ")
							AND pr.application_id = ?
							AND pr.user_type = ?
							AND pr.gender = ?
						GROUP BY p.id
					";

					$packages[$i]->optional_products = self::db()->fetchAll($opt_prod_sql, array(Cubix_Application::getId(), $user_type, $gender));
				}
				
				$premium_cities = $data['premium_cities'];				
				if ( count($premium_cities) ) {						
					$sql = '
						SELECT
							c.' . Cubix_I18n::getTblField('title') . ' AS title
						FROM cities c
						
						WHERE 
							c.id IN (' . implode(',', $premium_cities) . ')
					';

					$packages[$i]->premium_cities = self::db()->fetchAll($sql, array());
				}
			}
		}
		
		return $packages;
	}
	
	public function removeFromShoppingCart($user_id, $shopping_cart_id)
	{
		
		
		try {
			self::db()->query('DELETE FROM shopping_cart WHERE user_id = ? AND id = ?', array($user_id, $shopping_cart_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return true;
	}
	
	public static function _getEscortGender($escort_id)
	{
		return self::db()->fetchOne('SELECT gender FROM escorts WHERE id = ?', array($escort_id));
	}
	
	public function addToShoppingCart($data)
	{
		try {
			if ( count($data) ) {
				foreach($data as $package) {
					self::db()->query('DELETE FROM shopping_cart WHERE user_id = ? AND escort_id = ?', array($package['user_id'], $package['escort_id']));
					self::db()->insert('shopping_cart', $package);
				}
			}
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return true;
	}
	
	public function getShoppingCartItem($shopping_cart_id, $user_id)
	{
		
		
		$sql = "
			SELECT
				sc.*
			FROM shopping_cart sc
			WHERE 
				sc.id = ? AND sc.user_id = ?
		";
		
		$item = self::db()->fetchRow($sql, array($shopping_cart_id, $user_id));
		
		$data = unserialize($item->data);
		
		if ( isset($data['premium_cities']) && count($data['premium_cities']) ) {
			$sql = '
				SELECT 
					c.id, c.' . Cubix_I18n::getTblField('title') . ' AS title
				FROM cities c
				WHERE c.id IN (' . implode(',', $data['premium_cities']) . ')
			';			
			$cities = self::db()->fetchAll($sql);
			
			if ( $cities ) {
				$item->premium_cities = $cities;
			}
		}
		
		return $item;
	}
	
	public function getWorkingLocations($escort_id)
	{
		
		
		$sql = '
			SELECT 
				c.id, c.' . Cubix_I18n::getTblField('title') . ' AS title
			FROM escort_cities ec
			INNER JOIN cities c ON c.id = ec.city_id
			WHERE ec.escort_id = ?
 		';
		
		return self::db()->fetchAll($sql, array($escort_id));
	}
	
	public function getOptionalProducts($package_id)
	{
		
		
		$opt_prod_sql = "
			SELECT
				p.id, p.name, pr.price
			FROM package_products pp
			INNER JOIN products p ON p.id = pp.product_id
			INNER JOIN product_prices pr ON p.id = pr.product_id
			WHERE 
				pp.package_id = ? 
				AND pp.is_optional = 1
				AND pr.application_id = ?
				AND p.is_hidden = 0 AND p.id <> 8
			GROUP BY p.id
		";
		
		$opt_products = self::db()->fetchAll($opt_prod_sql, array($package_id, Cubix_Application::getId()));
		
		return $opt_products;
	}
	
	public function getPackagesList($user_type, $gender, $is_pseudo_escort = null)
	{
		
		
		$where = "";
		$opt_where = "";
		if ( self::app()->id == APP_A6 || self::app()->id == APP_A6_AT ) {
			$where .= " AND p.is_for_phone_billing = 0 ";
		}
		
		if ( $user_type == USER_TYPE_SINGLE_GIRL || $is_pseudo_escort ) {
			$where .= " AND pp.gender = {$gender} ";
			$opt_where .= " AND pr.gender = {$gender} ";
		}
		
		$sql = "
			SELECT
				p.id,
				p.name,
				p.available_for,
				pp.price,
				p.period,
				p.is_default
            FROM packages p
			INNER JOIN package_prices pp ON pp.package_id = p.id
            WHERE
				p.application_id = ?
                AND pp.user_type = ?
                AND p.is_active = 1
				{$where}
				AND p.ordering IS NOT NULL
			GROUP BY p.id
			ORDER BY p.ordering ASC
		";
		
		
		try {
			$packages_list = self::db()->fetchAll($sql, array(Cubix_Application::getId(), $user_type));

			if ( count($packages_list) ) {
				foreach ( $packages_list as $i => $package ) {
					$opt_prod_sql = "
						SELECT
							p.id, p.name, pr.price
						FROM package_products pp
						INNER JOIN products p ON p.id = pp.product_id
						INNER JOIN product_prices pr ON p.id = pr.product_id
						WHERE 
							pp.package_id = ? 
							AND pp.is_optional = 1
							AND pr.application_id = ?
							AND pr.user_type = ?
							AND p.is_hidden = 0
							{$opt_where}
						GROUP BY p.id
					";
					$packages_list[$i]->optional_products = self::db()->fetchAll($opt_prod_sql, array($package->id, Cubix_Application::getId(), $user_type));
					
					if ( $user_type == USER_TYPE_AGENCY ) {
						$package_prices_sql = "
							SELECT
								price, gender
							FROM package_prices
							WHERE package_id = ? AND user_type = ?
						";

						$packages_list[$i]->package_prices = self::db()->fetchAll($package_prices_sql, array($package->id, USER_TYPE_AGENCY));
					}
				}
			}
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return $packages_list;
	}
	
	public function addPremiumCity($shopping_cart_id, $user_id, $premium_cities)
	{
		$sql = "
			SELECT
				sc.*
			FROM shopping_cart sc
			WHERE 
				sc.id = ? AND sc.user_id = ?
		";
		
		try {
			$item = self::db()->fetchRow($sql, array($shopping_cart_id, $user_id));

			$data = unserialize($item->data);

			$data['premium_cities'] = $premium_cities;

			self::db()->update('shopping_cart', array('data' => serialize($data)), self::db()->quoteInto('id = ?', $shopping_cart_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return true;
	}
	
	public function getOrderHistory($user_id, $data)
	{
		
		$page = $data['page'];
		$per_page = $data['per_page'];
		$order_field = $data['order_field'];
		$order_dir = $data['order_dir'];
		
		
		if ( $page < 1 ) $page = 1;
		if ( $per_page < 1 ) $per_page = 1;
		$page--;
		$offset = $page * $per_page;
		$limit = $per_page;
		
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				oh.id, oh.user_id, oh.escort_id, oh.agency_id, oh.package_id,
				oh.order_id, oh.order_package_id, oh.transfer_id, oh.data,
				oh.showname, oh.package_title, oh.total_price, oh.period,
				UNIX_TIMESTAMP(oh.order_date) AS order_date,
				op.status AS package_status
			FROM online_billing_order_history oh
			INNER JOIN order_packages op ON op.id = oh.order_package_id
			WHERE oh.user_id = ?
			ORDER BY ' . $order_field . ' ' . $order_dir . '
			LIMIT ?, ?
		';
		
		try {		
			$orders = self::db()->fetchAll($sql, array($user_id, $offset, $limit));
			$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return array('result' => $orders, 'count' => $count);
	}
	
	public function checkIfHasPaidPackage($escort_id) {
		//Checking if escort has active, pending or suspended package
		//Returns these packages
		

		$sql = "
			SELECT op.period, op.date_activated, op.expiration_date, op.package_id, op.activation_type, op.status
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			WHERE e.id = ? AND (op.status = 2 OR op.status = 6 OR op.status = 1) AND op.order_id IS NOT NULL
			ORDER BY op.date_activated DESC
		";

		$package = self::db()->query($sql, array($escort_id))->fetch();
		
		return $package;
	}
	
	public function getExtendProducts($package_id)
	{
		
		
		$sql = "
			SELECT
				p.id, p.name, ppr.price
			FROM products p
			INNER JOIN package_products pp ON pp.product_id = p.id
			INNER JOIN product_prices ppr ON p.id = ppr.product_id

			WHERE pp.package_id = ? AND pp.is_optional = 1 AND ppr.application_id = ?
			GROUP BY p.id
		";

		$opt_products = self::db()->fetchAll($sql, array($package_id, Cubix_Application::getId()));
		
		$ext_products = array();
		foreach ( $opt_products as $product ) {
			if ( in_array($product->id, self::$EXTEND_PACKAGE_PRODUCTS) ) {				
				$ext_products[] = $product;
			}
		}
		
		return $ext_products;
	}
	
	public function storeToken($token, $user_id)
	{
		$is_exists = self::db()->fetchOne('SELECT TRUE FROM payment_gateway_tokens WHERE token = ?', array($token));
		
		if ( ! $is_exists ) {
			self::db()->insert('payment_gateway_tokens', array('token' => $token, 'user_id' => $user_id));
		}
		
		return true;
		
	}
	
	public function isPseudoEscort($escort_id)
	{
		return self::db()->fetchOne('SELECT TRUE FROM escorts WHERE id = ? AND pseudo_escort = 1', array($escort_id));		
	}
		
}
