<?php

class Cubix_Api_Module_Verification extends Cubix_Api_Module
{
	const TYPE_WEBCAM = 1;
	const TYPE_IDCARD = 2;
	
	const INFORM_EMAIL = 1;
	const INFORM_PHONE = 2;
	
	const STATUS_PENDING = 1;
	const STATUS_VERIFIED = 2;
	const STATUS_CONFIRMED = 3;
	const STATUS_REJECTED = 4;
	const STATUS_BLUR_VERIFY = 7;
	
	const VIP_STATUS_PENDING = 1;
	const VIP_STATUS_REJECTED = 3;
	
	public function save($request)
	{
		$request = (array) $request;
		for( $i = 1; $i <= 3; $i++ )
		{
			if ( isset($request['date_' . $i]) )
			{
				$request['date_' . $i] = new Zend_Db_Expr('FROM_UNIXTIME(' . $request['date_' . $i] . ')');
			}
		}
		
		try {
			self::db()->insert('verify_requests', $request);
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return self::db()->lastInsertId();
	}
	
	public function addPhoto($data)
	{
		try {
			self::db()->insert('verify_files', $data);
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function getByEscortId($escort_id)
	{
		$escort_id = intval($escort_id);
				
		$item = self::db()->fetchRow('
			SELECT 
				*, 
				UNIX_TIMESTAMP(date_1) AS date_1, 
				UNIX_TIMESTAMP(date_2) AS date_2, 
				UNIX_TIMESTAMP(date_3) AS date_3 
			FROM verify_requests 
			WHERE escort_id = ? AND status <> ? AND status <> ?
		', array($escort_id, self::STATUS_REJECTED, self::STATUS_VERIFIED));
		
		return $item;
	}

	public function getLastByEscortId($escort_id)
	{
		$escort_id = intval($escort_id);
				
		$item = self::db()->fetchRow('
			SELECT 
				status, reason_ids, reason_text
			FROM verify_requests
			WHERE escort_id = ? ORDER BY id DESC
		', array($escort_id));

		return $item;
	}
	
	public function getBluredPhotos($escort_id)
	{
		$escort_id = intval($escort_id);
				
		$blured = self::db()->fetchOne('
			SELECT blur_verify_ids 
			FROM verify_requests 
			WHERE escort_id = ? AND status = ? 
			ORDER BY id DESC LIMIT 1
		', array($escort_id, self::STATUS_BLUR_VERIFY));
		
		$photo_ids = implode(',',unserialize($blured));
		
		if(strlen($photo_ids) > 0){
			$photos =  self::db()->fetchAll(" SELECT " . Cubix_Application::getId() . " AS application_id,ep.escort_id, ep.hash, ep.ext,ep.width, ep.height FROM escort_photos ep
				WHERE ep.id IN (".$photo_ids.")
			", $escort_id);
		}
		else{
			return false;
		}
		return $photos;
	}
	
	/******************* VIP Requests **********************/
	
	public function isVipAvailableForEscort($escort_id)
	{		
		if (is_array($escort_id))
			$escort_id = intval(reset($escort_id));
		
		if ($escort_id)
		{
			$vip_req = self::db()->query('
				SELECT status, NOW() > DATE_ADD(action_date, INTERVAL 1 MONTH) AS allow 
				FROM vip_requests 
				WHERE escort_id = ? 
				ORDER BY id DESC 
				LIMIT 1
			', $escort_id)->fetch();
						
			if (!$vip_req || ($vip_req->status == self::VIP_STATUS_REJECTED && $vip_req->allow))
				return TRUE;
			
			return FALSE;
		}
	}
	
	public function isVipAvailableForAgency($user_id)
	{		
		$escorts = self::db()->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetchAll();
		
		if ($escorts)
		{
			$arr = array();
			
			foreach ($escorts as $escort)
				$arr[] = $escort->id;
			
			$p_escorts = self::db()->query('SELECT escort_id FROM order_packages WHERE escort_id IN (' . implode(',', $arr) . ') AND order_id IS NOT NULL AND status = ?', 2)->fetchAll();
			
			if ($p_escorts)
			{
				$escs = array();
				
				foreach ($p_escorts as $e)
					$eses[] = $e->escort_id;
				
				$escorts_list = self::db()->query('
					SELECT v.id, v.escort_id, v.status, NOW() > DATE_ADD(v.action_date, INTERVAL 1 MONTH) AS allow 
					FROM vip_requests v 
					WHERE v.escort_id IN (' . implode(',', $eses) . ') AND v.id = (SELECT MAX(id) FROM vip_requests WHERE escort_id = v.escort_id)
				')->fetchAll();
				
				if ($escorts_list)
				{
					$list = array();
					$allow_count = 0;
						
					foreach ($escorts_list as $esc)
					{						
						if ($esc->status == self::VIP_STATUS_REJECTED && $esc->allow)
						{
							$list[$esc->escort_id] = 1;
							$allow_count ++;
						}
						else
							$list[$esc->escort_id] = 0;
					}
					
					if (count($p_escorts) > count($escorts_list) || $allow_count > 0)
						return array('allow' => TRUE, 'all_escorts' => $p_escorts, 'r_escorts' => $list);
					else
						return array('allow' => FALSE, 'all_escorts' => NULL, 'r_escorts' => NULL);
				}
				else
					return array('allow' => TRUE, 'all_escorts' => $p_escorts, 'r_escorts' => NULL);
			}
			else
				return array('allow' => FALSE, 'all_escorts' => NULL, 'r_escorts' => NULL);
		}
		
		return array('allow' => FALSE, 'all_escorts' => NULL, 'r_escorts' => NULL);
	}
		
	public function sendVipRequest($escort_id, $owner)
	{
		if (is_array($escort_id))
			$escort_id = intval(reset($escort_id));
		
		if (is_array($owner))
			$owner = intval(reset($owner));
		
		self::db()->insert('vip_requests', array(
			'escort_id' => $escort_id,
			'creation_date' => new Zend_Db_Expr('NOW()'),
			'request_owner' => $owner,
			'status' => self::VIP_STATUS_PENDING
		));
	}
	
	public function saveAgeVerification($data)
	{		
		try {
			$age_verification = array(
				'escort_id' => $data['escort_id'],
				'idcard_id' => $data['idcard_id'],
				'name' => $data['name'],
				'last_name' => $data['last_name'],
				'born_date' => $data['year']."-".$data['month']."-".$data['day'],
				'creation_date' => new Zend_Db_Expr('NOW()'),
				'status' => isset( $data['status']) ? $data['status'] : 2
			);
			self::db()->insert('age_verifications', $age_verification);
			$last_id = self::db()->lastInsertId();
			
			$photo_count = count($data['photos']);
			
			for($i = 0 ; $i < $photo_count ; $i++ ){
				
				$data["photos"][$i]['request_id'] = $last_id;
				self::db()->insert('age_verification_files', $data["photos"][$i]);
			}
			if( !isset( $data['status'])){
				self::db()->update('escorts', array('need_age_verification' => 2), array( self::db()->quoteInto('id = ?', $data['escort_id'])));
			}
			
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $last_id;
	}

	public function addIdCardInAgeVerification($id_card ,$age_verification_id){

	    self::db()->update('age_verifications', array('idcard_id' => $id_card), array( self::db()->quoteInto('id = ?', $age_verification_id)));

        return self::db()->lastInsertId();
    }
	
	public function getNaturalPic($escort_id)
	{	
		if (is_array($escort_id))
			$escort_id = intval(reset($escort_id));
		
		try {
			 return self::db()->fetchRow('SELECT status, hash, ext FROM natural_pics WHERE escort_id = ?', array($escort_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return self::db()->lastInsertId();
	}
	
	public function saveNaturalPic($data)
	{		
		try {
			self::db()->delete('natural_pics', self::db()->quoteInto('escort_id = ?', $data['escort_id']));
			self::db()->insert('natural_pics', $data);
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return self::db()->lastInsertId();
	}
	
	public function deleteNaturalPic($escort_id)
	{	
		if (is_array($escort_id))
			$escort_id = intval(reset($escort_id));
		
		try {
			self::db()->delete('natural_pics', self::db()->quoteInto('escort_id = ?', $escort_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return self::db()->lastInsertId();
	}
	
	public function getPhoneVerificationDetails($escort_id)
	{
		return self::db()->fetchRow('SELECT e.last_hand_verification_date, ep.phone_exists FROM	escorts e
									INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
									WHERE e.id = ?', $escort_id);
	}

    public function getPhoneVerificationActiveMethodes()
    {
        return self::db()->fetchCol('SELECT verification_methodes FROM	verification_types WHERE status = 1',array());
    }

    public function getAgencyVerifiedPhones($user_id)
    {
        $getVerifiedPhones = self::db()->fetchAll('SELECT phone FROM agencies_phone_verification WHERE user_id = ? AND STATUS = 1', $user_id);
        $verifiedPhones    = array();
        foreach ($getVerifiedPhones as $item) {
            $verifiedPhones[] = $item->phone;
        }
        return $verifiedPhones;
    }

    public function getAgencyPhoneVerificationDetails($user_id)
    {
        $hand_verified = false;
        $last_hand_verified = self::db()->fetchRow('SELECT last_hand_verification_date as verification_date FROM agencies WHERE user_id = ?', $user_id);
        if (!is_null($last_hand_verified->verification_date)) $hand_verified = true;

        $verified_Phones_blank = self::db()->fetchAll('SELECT phone FROM agencies_phone_verification WHERE user_id = ? AND STATUS = 1', $user_id);
        $verified_Phones = array();
        foreach ($verified_Phones_blank as $item)
        {
            $verified_Phones[] = $item->phone;
        }
        $where1 = '';
        $where2 = '';
        if ( count($verified_Phones) > 0 )
        {
            $where1 = 'AND ag.contact_phone_parsed NOT IN ('.implode(',',$verified_Phones).')';
            $where2 = 'AND epv.contact_phone_parsed NOT IN ('.implode(',',$verified_Phones).')';
        }
        $sql = 'SELECT
                        ag.contact_phone_parsed phone
                    FROM
                        agencies ag 
                    WHERE
                        ag.user_id = ? AND ag.contact_phone_parsed IS NOT NULL '.$where1.'  UNION ALL
                    SELECT
                        epv.contact_phone_parsed phone 
                    FROM
                        agencies ag
                        INNER JOIN escorts e ON e.agency_id = ag.id
                        INNER JOIN escort_profiles_v2 epv ON e.id = epv.escort_id 
                    WHERE
                        ag.user_id = ? 
                        AND epv.contact_phone_parsed IS NOT NULL  '.$where2.'
                    GROUP BY
                        epv.contact_phone_parsed 
                        LIMIT 6';
        $phones =  self::db()->fetchAll($sql, array($user_id,$user_id));
        $no_phone_number = false;
        $filtered_phones = array();
        if( empty($phones) && empty( $verified_Phones ) )
        {
            $no_phone_number = true;
        }else{
            foreach ($phones as $phone)
            {
                $filtered_phones[] = $phone->phone;
            }
        }
        return array($filtered_phones,$no_phone_number,$hand_verified);
    }

	public function addPhoneVerification($data)
	{
		try {
			self::db()->insert('escorts_phone_verification', $data);
			return self::db()->lastInsertId();
		}catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

    public function addAgencyPhoneVerification($data)
    {
        try {
            self::db()->insert('agencies_phone_verification', $data);
            return self::db()->lastInsertId();
        }catch ( Exception $e ) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }
	
	public function updatePhoneVerification($id, $escort_id, $data)
	{
		try {
			$data['tries'] = new Zend_Db_Expr('tries + 1');
			self::db()->update('escorts_phone_verification', $data,self::db()->quoteInto('id = ?', $id));
			if($data['status'] == 1){
				self::db()->update(
					'escorts', 
					array(
						'last_hand_verification_date' => new Zend_Db_Expr('NOW()'), 
						'hand_verification_sales_id' => -1 ), 
					self::db()->quoteInto('id = ?', $escort_id)
				);
			}
		return true;
		}catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
	}

    public function updateAgencyPhoneVerification($id, $data)
    {
        try {
            $data['tries'] = new Zend_Db_Expr('tries + 1');
            self::db()->update('agencies_phone_verification', $data,self::db()->quoteInto('id = ?', $id));

            return true;
        }catch ( Exception $e ) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

    }
}
