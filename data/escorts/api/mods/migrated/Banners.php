<?php

/**
 * @author Eduard Hovhannisyan
 * @createdAt 07/06/2020
 *
 * Class Cubix_Api_Module_Banners
 *
 * Originally created for And6, check in "/banners-purhcase" page.
 */
class Cubix_Api_Module_Banners extends Cubix_Api_Module
{
    const TABLE_ORDERS = 'banner_orders';
    const TABLE_ORDER_FILES = 'banner_order_files';

    const STATUS_PENDING    = 1;
    const STATUS_PAID       = 2;
    const STATUS_REJECTED   = 3;

    /**
     * @param $data
     * @return mixed
     */
    public function createOrder($data)
    {
        self::db()->insert(self::TABLE_ORDERS, array(
            'phone_cc'          => $data['phone_cc'],
            'phone_number'      => $data['phone_number'],
            'email'             => $data['email'],
            'website'           => $data['website'],
            'payment_method'    => $data['payment_method'],
            'text'              => $data['text'],
            'customer_ip'       => $data['customer_ip'],
            'banner_id'         => $data['banner_id'],
            'created_at'        => date('Y-m-d H:i:s'),
        ));

        $createdOrderId = self::db()->lastInsertId();

        $files = $data['files'];
        foreach ($files as $file) {
            self::db()->insert(self::TABLE_ORDER_FILES, array(
                'banner_order_id' => $createdOrderId,
                'path' => $file,
            ));
        }

        return $createdOrderId;
    }

    /**
     * @param $orderId
     * @param $data
     */
    public function updateOrder($orderId, $data)
    {
        $whereClause = self::db()->quoteInto('id = ?', $orderId);
        $fieldsToUpdate = array('status' => $data['status']);

        if (isset($data['transaction_id'])) {
            $fieldsToUpdate['transaction_id'] = $data['transaction_id'];
        }

        self::db()->update(self::TABLE_ORDERS, $fieldsToUpdate, $whereClause);
    }

    /**
     * @param $orderId
     * @return mixed
     */
    public function getOrderById($orderId)
    {
        // Select Order
        $sql = 'SELECT 
            bo.text, bo.phone_cc, bo.phone_number, bo.phone_number, bo.email,
            bo.transaction_id, bo.website, bo.banner_id, 
            bo.payment_method, bo.customer_ip, bo.created_at
        FROM ' . self::TABLE_ORDERS . ' bo WHERE id = ?';
        $order = self::db()->fetchRow($sql, array($orderId));

        // Select files for current order
        $sql = 'SELECT * FROM ' . self::TABLE_ORDER_FILES . ' WHERE banner_order_id = ?';
        $files = self::db()->fetchAll($sql, array($orderId));

        $order->files = $files;
        return $order;
    }
}
