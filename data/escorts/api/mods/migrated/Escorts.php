<?php



class Cubix_Api_Module_Escorts extends Cubix_Api_Module 
{

	//Order statuses
	const ORDER_STATUS_PENDING = 1;
	// const STATUS_CANCELLED = 2;
	const ORDER_STATUS_PAID = 3;
	// const STATUS_CHARGEBACK = 4;
	const ORDER_STATUS_PAYMENT_DETAILS_RECEIVED = 5;
	const ORDER_STATUS_PAYMENT_REJECTED = 6;
	const ORDER_STATUS_CLOSED = 7;
	
	const PACKAGE_STATUS_PENDING  = 1;
    const PACKAGE_STATUS_ACTIVE             = 2;
	const PACKAGE_STATUS_SUSPENDED = 6;
	
	const PACKAGE_ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES = 6;
	const PACKAGE_ACTIVATE_AT_EXACT_DATE = 7;
	const PACKAGE_ACTIVATE_ASAP = 9;
	
	const AGE_CERIFICATION_REQUEST_SENT = 1;
	const AGE_CERIFICATION_REQUEST_TYPE_ONLINE = 1;
       
        
        
        
       
	
	
    public function getHitsCount($escort_id) {
        $escort_id = intval($escort_id);

		if(Cubix_Application::getId() == 69){
        		return intval(self::db()->fetchOne('SELECT SUM(count) FROM escort_hits_daily WHERE escort_id = ?', $escort_id));
		}
        return intval(self::db()->fetchOne('SELECT SUM(count) FROM escort_hits_daily WHERE escort_id = ? AND date >= DATE_ADD(DATE(NOW()), INTERVAL -90 DAY)', $escort_id));
    }

    public function getHitsForWeek($escort_id) {
        $escort_id = intval($escort_id);

        return  self::db()->query('SELECT `count`,`date` FROM escort_hits_daily WHERE escort_id = ? AND date >= DATE_ADD(DATE(NOW()), INTERVAL -7 DAY)', array($escort_id))->fetchAll();
    }


	public function updateGalleryHitsCount( $data ){
		if (!is_array($data) || 0 == count($data))
            return false;

		set_time_limit(0);
		ini_set('memory_limit', '512M');

        $errors = array();
        $return = array();
        
        foreach ($data as $escort_id => $row) {
            if (is_array($row)) {
                $escort_id = intval($escort_id);

                try {
                    $date = @$row[0];
                    $count = @$row[1];

                    $does_exist = (bool) self::db()->fetchOne('SELECT TRUE FROM escort_photo_hits_daily WHERE escort_id = ? AND date = DATE(FROM_UNIXTIME(?))', array($escort_id, $date));


                    if ($does_exist) {
                        $row_data = array(
                            'count' => new Zend_Db_Expr('count + ' . intval($count))
                        );

                        self::db()->update('escort_photo_hits_daily', $row_data, array(
                            self::db()->quoteInto('escort_id = ?', $escort_id),
                            self::db()->quoteInto('date = DATE(FROM_UNIXTIME(?))', $date)
                        ));
                    } else {
                        $row_data = array(
                            'count' => new Zend_Db_Expr(intval($count))
                        );

                        self::db()->insert('escort_photo_hits_daily', array_merge($row_data, array('date' => new Zend_Db_Expr('DATE(NOW())'), 'escort_id' => $escort_id)));
                    }
                } catch (Exception $e) {
                    $errors[$escort_id] = Cubix_Api_Error::Exception2Array($e);
                    $errors[$escort_id]['doesexists'] = $does_exist;
                }
            }else{
                $return[] = $escort_id;
            }

        }
//        return $return;

        if (!count($errors)) {
            return true;
        }else{
            
        }

        return $errors;
	}

    public function updateHitsCount($data) 
	{
		//if( Cubix_Application::getId() == 18){return '123';}
        
		if (!is_array($data) || 0 == count($data))
            return false;

		set_time_limit(0);
		ini_set('memory_limit', '512M');
		
        $errors = array();
        $return = array();
        foreach ($data as $escort_id => $row) {
            if (is_array($row)) {
                $escort_id = intval($escort_id);

                try {
                    $date = @$row[0];
                    $count = @$row[1];

                    $does_exist = (bool) self::db()->fetchOne('SELECT TRUE FROM escort_hits_daily WHERE escort_id = ? AND date = DATE(FROM_UNIXTIME(?))', array($escort_id, $date));


                    if ($does_exist) {
                        $row_data = array(
                            'count' => new Zend_Db_Expr('count + ' . intval($count))
                        );

                        self::db()->update('escort_hits_daily', $row_data, array(
                            self::db()->quoteInto('escort_id = ?', $escort_id),
                            self::db()->quoteInto('date = DATE(FROM_UNIXTIME(?))', $date)
                        ));
                    } else {
                        $row_data = array(
                            'count' => new Zend_Db_Expr(intval($count))
                        );

                        self::db()->insert('escort_hits_daily', array_merge($row_data, array('date' => new Zend_Db_Expr('DATE(NOW())'), 'escort_id' => $escort_id)));
                    }
                } catch (Exception $e) {
                    $errors[$escort_id] = Cubix_Api_Error::Exception2Array($e);
                    $errors[$escort_id]['doesexists'] = $does_exist;
                }
            }else{
                $return[] = $escort_id;
            }

        }
//        return $return;

        if (!count($errors)) {
            return true;
        }

        return $errors;
    }

    const REVISION_STATUS_NOT_APPROVED = 1;
    const REVISION_STATUS_APPROVED = 2;
    const REVISION_STATUS_DECLINED = 3;

    const VERIFIED_STATUS_NOT_VERIFIED = 1;
    const VERIFIED_STATUS_VERIFIED = 2;
    const VERIFIED_STATUS_VERIFIED_RESET = 3;

    public function getTableSqlDump() {
        return '
			CREATE TABLE `escorts` (
				`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
				`agency_id` int(10) UNSIGNED NULL DEFAULT NULL ,
				`showname`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
				`gender`  tinyint(1) UNSIGNED NOT NULL ,
				`country_id`  int(10) UNSIGNED NULL DEFAULT NULL ,
				`city_id`  int(10) UNSIGNED NULL DEFAULT NULL ,
				`cityzone_id`  int(10) UNSIGNED NULL DEFAULT NULL ,
				`verified_status`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 ,
				`ethnicity`  varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
				`nationality_id` int(10) UNSIGNED NULL DEFAULT NULL,
				`age`  tinyint(2) UNSIGNED NULL DEFAULT NULL ,
				`eye_color`  tinyint(2) UNSIGNED NULL DEFAULT NULL ,
				`hair_color`  tinyint(2) NULL DEFAULT NULL ,
				`hair_length`  tinyint(2) NULL DEFAULT NULL ,
				`height`  int(3) UNSIGNED NULL DEFAULT NULL ,
				`weight`  int(3) NULL DEFAULT NULL ,
				`breast_size`  char(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
				`dress_size`  int(2) NULL DEFAULT NULL ,
				`shoe_size`  int(3) NULL DEFAULT NULL ,
				`is_smoker`  tinyint(1) UNSIGNED NULL DEFAULT NULL ,
				`availability`  tinyint(2) UNSIGNED NULL DEFAULT NULL ,
				`sex_availability`  set("men","women","couples","trans","gays","plus2") CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
				`languages`  set("en","fr","de","it","pt","ru","es","gr") CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
				`svc_kissing`  tinyint(1) UNSIGNED NULL DEFAULT NULL ,
				`svc_blowjob`  tinyint(1) UNSIGNED NULL DEFAULT NULL ,
				`svc_69`  tinyint(1) UNSIGNED NULL DEFAULT NULL ,
				`svc_anal`  tinyint(1) UNSIGNED NULL DEFAULT NULL ,
				`is_now_open`  tinyint(1) NULL DEFAULT NULL ,
				`is_on_tour`  tinyint(1) UNSIGNED NOT NULL ,
				`tour_id`  int(10) UNSIGNED NULL DEFAULT NULL ,
				`tour_city_id`  int(10) NULL DEFAULT NULL ,
				`tour_date_from`  timestamp NULL DEFAULT NULL ,
				`tour_date_to`  timestamp NULL DEFAULT NULL ,
				`vac_date_from`  date NULL DEFAULT NULL ,
				`vac_date_to`  date NULL DEFAULT NULL ,
				`is_new`  tinyint(1) UNSIGNED NOT NULL ,
				`date_registered`  timestamp NULL DEFAULT NULL ,
				`date_last_modified`  timestamp NULL DEFAULT NULL ,
				
				`photo_hash`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
				`photo_ext`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
				`photo_status`  tinyint(1) NOT NULL DEFAULT 3 ,
				
				
				`rates`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
				`hit_count` int(11) unsigned DEFAULT \'0\',
				PRIMARY KEY (`id`)
			)
			ENGINE=InnoDB
			DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
			AUTO_INCREMENT=1
			ROW_FORMAT=COMPACT;
		';
    }

    public function getUpcomingToursTableSqlDump() {
        return "
			CREATE TABLE `upcoming_tours` (
			  `id` int(10) NOT NULL,
			  `tour_id` int(10) DEFAULT NULL,
			  `tour_city_id` int(10) DEFAULT NULL,
			  `tour_date_from` date DEFAULT NULL,
			  `tour_date_to` date DEFAULT NULL,
			  PRIMARY KEY (`id`)
			  
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		";
    }

    public function getEscortCitiesSqlDump() {
        return "
			CREATE TABLE `escort_cities` (
			  `escort_id` int(10) unsigned NOT NULL,
			  `city_id` int(10) unsigned NOT NULL,
			PRIMARY KEY (`escort_id`,`city_id`),
			KEY `escort_id` (`escort_id`),
			KEY `city_id` (`city_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		";
    }

    public function getPhotosTableSqlDump() {
        return '
			CREATE TABLE `escort_photos` (
				`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
				`escort_id`  int(10) UNSIGNED NOT NULL ,
				`hash`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
				`ext`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
				`type`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 ,
				`status`  tinyint(1) UNSIGNED NOT NULL DEFAULT 3 ,
				`is_main`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
				`ordering`  int(5) UNSIGNED NOT NULL DEFAULT 1 ,
				`args` varchar(255) DEFAULT NULL,
				PRIMARY KEY (`id`)
			)
			ENGINE=InnoDB
			DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
			AUTO_INCREMENT=585
			CHECKSUM=0
			ROW_FORMAT=DYNAMIC
			DELAY_KEY_WRITE=0;
		';
    }
	
    public function existsByShowname($showname, $escort_id) {

        if ($escort_id)
            return (bool) self::db()->fetchOne('SELECT TRUE FROM escorts WHERE showname = ? AND id <> ? AND NOT (e.status & ?)', array($showname, $escort_id, self::ESCORT_STATUS_DELETED));
        else
            return (bool) self::db()->fetchOne('SELECT TRUE FROM escorts WHERE showname = ? AND NOT (e.status & ?)', array($showname, self::ESCORT_STATUS_DELETED));
    }

    /**
     *
     * @param array $agency_id
     * @return mixed
     */
    public function getAll($agency_id = null) {
        $where = '';
        if (!is_null($agency_id)) {
            $agency_id = intval($agency_id);
            $where .= ' AND e.agency_id = ' . $agency_id . ' ';
        }

        $sql = '
			SELECT
				e.id,
				e.agency_id,
				e.showname,
				e.gender,
				e.country_id,
				e.city_id,
				e.cityzone_id,
				e.verified_status,
				ep.ethnicity,
				ep.nationality_id,
				FLOOR(DATEDIFF(CURDATE(), ep.birth_date) / 365.25) AS age,
				ep.eye_color,
				ep.hair_color,
				ep.hair_length,
				ep.height,
				ep.weight,
				ep.breast_size,
				ep.dress_size,
				ep.shoe_size,
				ep.is_smoker,
				ep.availability,
				ep.sex_availability,
				/* lang */
				/* rates */
				ep.svc_kissing,
				ep.svc_blowjob,
				ep.svc_69,
				ep.svc_anal,
				(
					(
						ewt.day_index = ' . date('N') . '
						AND ewt.time_from <= ' . date('G') . '
						AND ewt.time_to >= ' . date('G') . '
					)
					OR
					(
						ewt.day_index = ' . date('N') . '
						AND ewt.time_from <= ' . date('G') . '
						AND ewt.time_from > ewt.time_to
					)
					OR
					(
						ewt.day_index = ' . ( (1 == date('N')) ? 7 : date('N') - 1 ) . '
						AND ewt.time_to >= ' . date('G') . '
						AND ewt.time_from  > ewt.time_to 
					)
				) AS is_now_open,
				IF ( NOT t.id IS NULL, 1, 0 ) AS is_on_tour,
				IF ( NOT t.id IS NULL, t.id, NULL ) AS tour_id,
				IF ( NOT t.id IS NULL, t.city_id, NULL ) AS tour_city_id,
				
				IF (u.date_registered > (CURDATE() - INTERVAL ' . self::config()->newIntervalInDays . ' DAY), 1, 0) AS is_new,
				
				u.date_registered,
				e.date_last_modified,
				
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				
				t.date_from AS tour_date_from,
				t.date_to AS tour_date_to,
				ep.vac_date_from,
				ep.vac_date_to,
				ep.hit_count
			FROM escort_profiles ep
			
			INNER JOIN escorts e ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN applications a ON a.id = u.application_id
			
			LEFT JOIN escort_working_times ewt ON ewt.escort_id = e.id
			LEFT JOIN escort_langs el ON el.escort_id = e.id
			INNER JOIN escort_cities ec ON ec.escort_id = e.id
			
			INNER JOIN countries c ON c.id = e.country_id
			INNER JOIN cities cc ON cc.id = ec.city_id
			LEFT JOIN regions r ON r.id = cc.region_id
			LEFT JOIN cityzones cz ON cz.id = e.cityzone_id
			
			INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			
			LEFT JOIN tours t ON t.escort_id = e.id AND DATE(t.date_from) <= CURDATE() AND DATE(t.date_to) > CURDATE()
			
			WHERE u.application_id = ? AND u.status = ? AND eph.is_main = 1' . $where . ' AND (e.status & ' . self::ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . self::ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . self::ESCORT_STATUS_ADMIN_DISABLED . ')
 			GROUP BY e.id
		';

        $countSql = '
			SELECT 
				COUNT(DISTINCT(e.id)) AS count
			FROM escorts e
			
			INNER JOIN escort_profiles ep ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN applications a ON a.id = u.application_id
			
			LEFT JOIN escort_working_times ewt ON ewt.escort_id = e.id
			LEFT JOIN escort_langs el ON el.escort_id = e.id
			
			INNER JOIN countries c ON c.id = e.country_id
			INNER JOIN cities cc ON cc.id = e.city_id
			LEFT JOIN regions r ON r.id = cc.region_id
			LEFT JOIN cityzones cz ON cz.id = e.cityzone_id
			
			INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1' . $where . '
			
			WHERE u.application_id = ? AND u.status = ? AND (e.status & ' . self::ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . self::ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . self::ESCORT_STATUS_ADMIN_DISABLED . ')
		';

        try {
            $result = self::db()->query($sql, array(self::app()->id, STATUS_ACTIVE))->fetchAll();

            foreach ($result as $i => $row) {

                $result[$i]->rates = serialize(self::db()->query('SELECT * FROM escort_rates WHERE escort_id = ?', $row->id)->fetchAll());

                $langs = array();
                foreach (self::db()->query('SELECT * FROM escort_langs WHERE escort_id = ?', $row->id)->fetchAll() as $row) {
                    $langs[] = $row->lang_id;
                }

                $result[$i]->languages = implode(',', $langs);
            }

            $count = self::db()->fetchOne($countSql, array(self::app()->id, STATUS_ACTIVE));
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return array('result' => $result, 'count' => $count);
    }

    public function getFeaturedGirls()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');

        $sql = "SELECT * FROM featured_girls";
        try {
            $result = self::db()->query($sql)->fetchAll();
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return $result;
    }

    /**
     *
     * @param array $agency_id
     * @return mixed
     */
    public function getAllAgencyEscorts($agency_id = null, $params = array()) {

        if (!$agency_id) {
            return 'Wrong Agency id';
        }

        $limit = '';
        if (count($params) > 0) {
            /* --> Construct limit */
            if (isset($params['limit'])) {
                $limit = 'LIMIT ' . ($params['limit']['page'] - 1) * (($params['limit']['perPage']) ? $params['limit']['perPage'] : $config['perPage']) . ', ' . (($params['limit']['perPage']) ? $params['limit']['perPage'] : $config['perPage']);
            }
        }

        /* e.id,
          e.showname,
          e.age,
          UNIX_TIMESTAMP(e.date_registered) AS date_registered,
          cc.' . Cubix_I18n::getTblField('title') . ' AS city,

          e.rates,

          e.incall_type,
          e.outcall_type,

          e.is_new,
          e.verified_status,
          e.photo_hash,
          e.photo_ext,
          e.photo_status,
          ' . Cubix_Application::getId() . ' AS application_id,
          e.city_id,
          e.tour_city_id,
          cc1.' . Cubix_I18n::getTblField('title') . ' AS tour_city,
          e.tour_date_from,
          e.tour_date_to,
          e.hit_count
          ' . $what . ' */

        /* <-- */
        $sql = '
			SELECT
				e.id,
				e.showname,
				e.status,
				e.gender,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.application_id,
				e.verified_status,
				ep.phone_number AS contact_phone,
				ep.incall_type,
				ep.outcall_type,
				e.city_id,
				cc.' . Cubix_I18n::getTblField('title') . ' AS city
			FROM escort_profiles_v2 ep

			INNER JOIN escorts e ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN cities cc ON cc.id = e.city_id

			LEFT JOIN escort_photos eph ON eph.escort_id = e.id AND eph.is_main = 1

			WHERE u.application_id = ? AND e.agency_id = ? AND NOT e.status & ?
 			GROUP BY e.id
			' . $limit . '
		';

        $countSql = '
			SELECT
				COUNT(DISTINCT(e.id)) AS count
			FROM escorts e

			INNER JOIN escort_profiles ep ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id



			WHERE u.application_id = ? AND e.agency_id = ? AND NOT e.status & ?
		';

        /* try {
          $result = self::db()->query($sql, array(self::app()->id, STATUS_ACTIVE))->fetchAll();

          foreach ( $result as $i => $row ) {

          $result[$i]->rates = serialize( self::db()->query('SELECT * FROM escort_rates WHERE escort_id = ?', $row->id)->fetchAll());

          $langs = array();
          foreach ( self::db()->query('SELECT * FROM escort_langs WHERE escort_id = ?', $row->id)->fetchAll() as $row ) {
          $langs[] = $row->lang_id;
          }

          $result[$i]->languages = implode(',', $langs);
          }

          $count = self::db()->fetchOne($countSql, array(self::app()->id, STATUS_ACTIVE));
          }
          catch ( Exception $e ) {
          return array('error' => Cubix_Api_Error::Exception2Array($e));
          } */
        try {
            $count = self::db()->fetchOne($countSql, array(self::app()->id, $agency_id, self::ESCORT_STATUS_DELETED));
            $result = self::db()->query($sql, array(self::app()->id, $agency_id, self::ESCORT_STATUS_DELETED))->fetchAll();
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        foreach ($result as $i => $row) {

            $result[$i]->rates = serialize(self::db()->query('SELECT * FROM escort_rates WHERE escort_id = ?', $row->id)->fetchAll());

            /* $langs = array();
              foreach ( self::db()->query('SELECT * FROM escort_langs WHERE escort_id = ?', $row->id)->fetchAll() as $row ) {
              $langs[] = $row->lang_id;
              }

              $result[$i]->languages = implode(',', $langs); */
        }

        return array('result' => $result, 'count' => $count);
    }

    /**
     *
     * @return mixed
     */
    public function getAllUpcomingTours() {
        $where = ' AND DATE(t.date_from) > DATE(NOW()) ';

        $sql = '
			SELECT
				e.id,
				IF ( NOT t.id IS NULL, t.id, NULL ) AS tour_id,
				IF ( NOT t.id IS NULL, t.city_id, NULL ) AS tour_city_id,
				t.date_from AS tour_date_from,
				t.date_to AS tour_date_to
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			/*INNER JOIN escorts e ON ep.escort_id = e.id
			
			INNER JOIN applications a ON a.id = u.application_id
			
			LEFT JOIN escort_working_times ewt ON ewt.escort_id = e.id
					
			INNER JOIN escort_cities ec ON ec.escort_id = e.id
			
			
			INNER JOIN cities cc ON cc.id = t.city_id*/

			LEFT JOIN tours t ON t.escort_id = e.id
			INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			
			
			
			WHERE u.application_id = ? AND u.status = ? AND eph.is_main = 1' . $where . '
 			GROUP BY e.id
		';

        try {
            $result = self::db()->query($sql, array(self::app()->id, STATUS_ACTIVE))->fetchAll();
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return $result;
    }

    /**
     *
     * @return mixed
     */
    public function getEscortCities() {
        $sql = '
			SELECT ec.* FROM escort_cities ec
			INNER JOIN escorts e ON e.id = ec.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE u.application_id = ? AND u.status = ? AND (e.status & ' . self::ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . self::ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . self::ESCORT_STATUS_ADMIN_DISABLED . ')
			/*GROUP BY ec.escort_id*/
		';

        try {
            $result = self::db()->query($sql, array(self::app()->id, STATUS_ACTIVE))->fetchAll();
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return $result;
    }

	
    public function _whereStatus($field, $status) {
        if (!is_array($status))
            $status = array($status);

        $ret = '';
        $i = 0;
        foreach ($status as $st) {
            $ret .= " ({$field} & {$st}) ";
            $i++;
            if ($i < count($status))
                $ret .= "AND";
        }

        return $ret;
    }

    /**
     *
     * @return mixed
     */
    public function getAllPhotos() {
        $start = 0;
        $limit = 200;

        if (is_array($start))
            $start = intval(reset($start));
        if (is_array($limit))
            $limit = intval(reset($limit));

        $sql = '
			SELECT eph.* 
			FROM escort_photos eph
			INNER JOIN escorts e ON e.id = eph.escort_id
			/*INNER JOIN escort_profiles ep ON ep.escort_id = e.id*/
			INNER JOIN users u ON u.id = e.user_id
			WHERE u.application_id = ? AND u.status = ? AND (e.status & ' . self::ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . self::ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . self::ESCORT_STATUS_ADMIN_DISABLED . ')
			LIMIT ' . $start . ', ' . $limit . '
		';

        $countSql = '
			SELECT COUNT(eph.id) as count 
			FROM escort_photos eph
			INNER JOIN escorts e ON e.id = eph.escort_id
			/*INNER JOIN escort_profiles ep ON ep.escort_id = e.id*/
			INNER JOIN users u ON u.id = e.user_id
			WHERE u.application_id = ? AND u.status = ? AND (e.status & ' . self::ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . self::ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . self::ESCORT_STATUS_ADMIN_DISABLED . ')
		';

        try {
            $result = self::db()->query($sql, array(self::app()->id, STATUS_ACTIVE))->fetchAll();
            // $count = self::db()->fetchOne($countSql, array(self::app()->id, STATUS_ACTIVE));
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return array('result' => $result, 'count' => 0);
    }

    /**
     *
     * @param array $showname
     * @param array $lang_id
     * @return mixed
     */
    public function getProfile($showname, $lang_id, $status_excl = false) {


        Zend_Controller_Front::getInstance()->getRequest()->setParam('lang_id', $lang_id);

        

        $where = "  ";
        if (!$status_excl) {
            $where = " AND u.status = ? ";
        }

        $sql = '
			SELECT
				e.id,
				e.agency_id,
				e.showname,
				e.is_pornstar,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				u.application_id,
				ep.contact_email,
				u.username,
				eph.status AS photo_status,
				
				/* Contacts Section */
				ct.' . Cubix_I18n::getTblField('title') . ' AS base_city,
				ct.id AS base_city_id,
				c.' . Cubix_I18n::getTblField('title') . ' AS country,
				c.slug AS country_slug,
				c.iso AS country_iso,
				r.slug AS region_slug,
				r.' . Cubix_I18n::getTblField('title') . ' AS region_title,
				ct.slug AS city_slug,
				
				IF(((DATE(ep.vac_date_from) <= CURDATE() AND DATE(ep.vac_date_to) > CURDATE()) OR DATE(ep.vac_date_from) > CURDATE()), ep.vac_date_to, 0) AS vac_date_to,
				ep.contact_phone,
				ep.phone_instructions,
				ep.contact_web,
				ep.contact_email,
				ep.contact_zip,
				
				/* About Section */
				FLOOR(DATEDIFF(CURDATE(), ep.birth_date) / 365.25) as age,
				n.iso AS nationality_iso,
				n.' . Cubix_I18n::getTblField('title') . ' AS nationality_title,
				ep.eye_color,
				ep.hair_color,
				ep.hair_length,
				ep.height,
				ep.weight,
				ep.bust,
				ep.waist,
				ep.hip,
				ep.shoe_size,
				ep.breast_size,
				ep.dress_size,
				ep.is_smoker,
				ep.measure_units,
				ep.availability,
				ep.sex_availability,
				ep.ethnicity,
				ep.' . Cubix_I18n::getTblField('about') . ' AS about,
				ep.characteristics,
				
				/* SetcardInfo Section */
				e.date_last_modified,
				e.date_registered,
				/*ep.hit_count,*/
				(
					SELECT
						SUM(count)
					FROM escort_hits_daily
					WHERE escort_id = e.id AND DATE_ADD(date, INTERVAL 90 DAY) >= DATE(NOW())
				) AS hit_count,
				
				/* Services Section */
				ep.svc_kissing,
				ep.svc_blowjob,
				ep.svc_cumshot,
				ep.svc_69,
				ep.svc_anal,
				ep.' . Cubix_I18n::getTblField('svc_additional') . ' AS svc_additional
			FROM escorts e
			INNER JOIN escort_profiles ep ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id			
			INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			
			INNER JOIN countries c ON c.id = e.country_id
			INNER JOIN cities ct ON ct.id = e.city_id 
			INNER JOIN regions r ON r.id = ct.region_id
			
			LEFT JOIN nationalities n ON n.id = ep.nationality_id

			LEFT JOIN escort_hits_daily ehd ON ehd.escort_id = e.id AND DATE_ADD(ehd.date, INTERVAL 90 DAY) >= DATE(NOW())

			WHERE u.application_id = ? ' . $where . ' AND e.' . ( is_numeric($showname) ? 'id' : 'showname' ) . ' = ? AND NOT (e.status & 256)
			GROUP BY e.id
		';

        try {
            $filter = array(self::app()->id, STATUS_ACTIVE, $showname);
            if ($status_excl) {
                $filter = array(self::app()->id, $showname);
            }

            if (!$escort = self::db()->fetchRow($sql, $filter)) {
                return array('error' => 'Escort with showname or id `' . print_r($showname, true) . '` could not be found');
            }
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }



        if (!$escort) {
            return null;
        }

        $escort->langs = self::db()->query('
			SELECT el.lang_id AS id, el.level, l.title FROM escort_langs el
			LEFT JOIN langs l ON l.id = el.lang_id 
			WHERE el.escort_id = ?
		', $escort->id)->fetchAll();

        $escort->rates = self::db()->query('
			SELECT availability, time, time_unit, price, currency_id FROM escort_rates
			WHERE escort_id = ?
		', $escort->id)->fetchAll();

        $escort->tours = self::db()->query('
			SELECT t.id, t.date_from, t.date_to, t.phone, t.country_id, 
				t.city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country_title, 
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				c.slug AS country_slug, r.slug AS region_slug, ct.slug AS city_slug, 
				IF(DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()), 1, 0) AS is_in_tour,
				IF(DATE(t.date_from) > DATE(NOW()), 1, 0) AS is_upcoming_tour
			FROM tours t
				
			INNER JOIN countries c ON c.id = t.country_id
			INNER JOIN cities ct ON ct.id = t.city_id 
			INNER JOIN regions r ON r.id = ct.region_id
			WHERE t.escort_id = ? AND (DATE(t.date_from) >= DATE(NOW()) OR  DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()))
			ORDER BY t.date_from ASC
		', $escort->id)->fetchAll();

        $escort->is_open = self::db()->fetchOne('
			SELECT
				IF ((
					ewt.day_index = ' . date('N') . '
					AND ewt.time_from <= ' . date('G') . '
					AND ewt.time_to >= ' . date('G') . '
				) OR (
					ewt.day_index = ' . date('N') . '
					AND ewt.time_from <= ' . date('G') . '
					AND ewt.time_from > ewt.time_to
				) OR (
					ewt.day_index = ' . ( 1 == date('N') ? 7 : date('N') - 1 ) . '
					AND ewt.time_to >= ' . date('G') . '
					AND ewt.time_from  > ewt.time_to 
				), 1, 0)
			FROM escort_working_times ewt
			WHERE ewt.escort_id = ? AND ewt.day_index = ?
		', array($escort->id, date('N')));

        $working_times = self::db()->query('
			SELECT ewt.day_index, ewt.time_from AS `from`, ewt.time_to AS `to`
			FROM escort_working_times ewt
			WHERE ewt.escort_id = ?
		', $escort->id)->fetchAll();

        $wts = array();
        foreach ($working_times as $i => $wt) {
            $wts[$wt->day_index] = $wt;
        }

        $escort->working_times = $wts;

        return $escort;
    }

    /**
     *
     * @param array $showname
     * @param array $lang_id
     * @return mixed
     */
    public function getProfileV2($showname, $lang_id, $status_excl = false, $only_active = false) {
        

        Zend_Controller_Front::getInstance()->getRequest()->setParam('lang_id', $lang_id);
//        return array($showname, $lang_id, $status_excl);

        $where = "  ";
        if ( !$status_excl ) {
            $where = " AND u.status = ? ";
        }

        if ( $only_active ) {
        	$where .= " AND e.status & 32 ";
        }

        $fields = '';
        if ( in_array(Cubix_Application::getId(), array(APP_EF)) ) {
        	$fields.= 'e.need_age_verification,';
        } 
        
        
        if ( in_array(Cubix_Application::getId(), array(APP_BL)) ) {
        	$fields.= ' ep.snapchat, ep.snapchat_username, ';
        }

        if ( in_array(Cubix_Application::getId(), array(APP_ED)) ) {
        	$fields.= ' u.pm_is_blocked, e.country_id, e.disabled_follow,';
        }

        $sql = '
			SELECT
				e.id,
				e.user_id,
				e.type,
				e.agency_id,
				e.showname,
				e.gender,
                e.block_website,
				e.status AS escort_status,
				e.is_pornstar,
				' . Cubix_I18n::getTblFields('ct.title', null, null, 'city_title') . ',
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				u.application_id,
				u.username,
				eph.status AS photo_status,

				/* Contacts Section */
				' . Cubix_I18n::getTblFields('ct.title', null, null, 'base_city') . ',
				ct.id AS base_city_id,
				' . Cubix_I18n::getTblFields('c.title', null, null, 'country') . ',
				c.slug AS country_slug,
				c.iso AS country_iso,
				r.slug AS region_slug,
				' . Cubix_I18n::getTblFields('r.title', null, null, 'region_title') . ',
				ct.slug AS city_slug,

				IF(((DATE(e.vac_date_from) <= CURDATE() AND DATE(e.vac_date_to) > CURDATE()) OR DATE(e.vac_date_from) > CURDATE()), e.vac_date_to, 0) AS vac_date_to,
				ep.phone_number,
                ep.contact_phone_parsed as contact_phone_parsed,
				ep.phone_number_alt,
				ep.phone_instr,
				ep.phone_instr_no_withheld,
				ep.phone_instr_other,
				ep.phone_country_id,
				ep.website,
				ep.email,
				ep.zip,
				ep.club_name,
				ep.street,
				ep.street_no,
				ep.address_additional_info,

				/* About Section */
				/*FLOOR(DATEDIFF(CURDATE(), ep.birth_date) / 365.25) as age,*/
				ep.birth_date,
				n.iso AS nationality_iso,
				' . Cubix_I18n::getTblFields('n.title', null, null, 'nationality_title') . ',
				ep.eye_color,
				ep.hair_color,
				ep.hair_length,
				ep.height,
				ep.weight,
				ep.bust,
				ep.waist,
				ep.hip,
				ep.shoe_size,
				ep.cup_size,
				ep.breast_type,
				ep.dress_size,
				ep.is_smoking,
				ep.pubic_hair,
				ep.is_drinking,
				ep.measure_units,
                                
				ep.cuisine,
				ep.drink,
				ep.flower,
				ep.gifts,
				ep.perfume,
				ep.designer,
				ep.interests,
				ep.sports,
				ep.hobbies,
				
				ep.min_book_time,
				ep.min_book_time_unit,
				
				ep.has_down_pay,
				ep.down_pay,
				ep.down_pay_cur,
				
				ep.reservation,

				ep.incall_type,
				ep.incall_hotel_room,
				ep.incall_other,
				ep.outcall_type,
				ep.outcall_other,

				ep.sex_orientation,
				ep.sex_availability,
				ep.ethnicity,
				' . Cubix_I18n::getTblFields('ep.about', null, null, 'about') . ',
				ep.characteristics,

				/* SetcardInfo Section */
				e.date_last_modified,
				e.date_registered,
				/*ep.hit_count,*/
				(
					SELECT
						SUM(count)
					FROM escort_hits_daily
					WHERE escort_id = e.id AND DATE_ADD(date, INTERVAL 90 DAY) >= DATE(NOW())
				) AS hit_count,

				/* Services Section */
				' . Cubix_I18n::getTblFields('ep.additional_service', null, null, 'additional_service') . ',
				ep.available_24_7,
				' . $fields . '
				ep.special_rates,
				v.id as video_id
			FROM escorts e
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1

			INNER JOIN countries c ON c.id = e.country_id
			INNER JOIN cities ct ON ct.id = e.city_id
			LEFT JOIN video v ON v.escort_id = e.id and v.status = "approve"
			LEFT JOIN regions r ON r.id = ct.region_id

			LEFT JOIN nationalities n ON n.id = ep.nationality_id

			LEFT JOIN escort_hits_daily ehd ON ehd.escort_id = e.id AND DATE_ADD(ehd.date, INTERVAL 90 DAY) >= DATE(NOW())

			WHERE u.application_id = ? ' . $where . ' AND e.' . ( is_numeric($showname) ? 'id' : 'showname' ) . ' = ? AND NOT (e.status & 256)
			GROUP BY e.id
		';
		
        try {
            $filter = array(self::app()->id, STATUS_ACTIVE, $showname);
            if ($status_excl) {
                $filter = array(self::app()->id, $showname);
            }

            if (!$escort = self::db()->fetchRow($sql, $filter)) {
                $result = array('error' => 'Escort with showname or id `' . print_r($showname, true) . '` could not be found');

                if ( in_array(Cubix_Application::getId(), array(APP_ED)) ) {
                    $escort_status = self::db()->fetchRow('SELECT `status` FROM escorts WHERE ' . ( is_numeric($showname) ? 'id' : 'showname') .' = '.$showname);

                    if ($escort_status->status)
                        $result['escort_status'] = $escort_status->status;
                }

                return $result;
            }
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        

        if (!$escort) {
            return null;
        }

        $escort->langs = self::db()->query('
			SELECT el.lang_id AS id, el.level, l.title FROM escort_langs el
			LEFT JOIN langs l ON l.id = el.lang_id
			WHERE el.escort_id = ?
		', $escort->id)->fetchAll();

        $escort->rates = self::db()->query('
			SELECT availability, type, time, time_unit, price, currency_id FROM escort_rates
			WHERE escort_id = ?
		', $escort->id)->fetchAll();
		
		if ( in_array(self::app()->id, array(33, 22, APP_ED)) ) {
                       	$escort->travel_payment_methods = self::db()->query('
				SELECT payment_method, specify FROM travel_payment_methods
				WHERE escort_id = ?
			', $escort->id)->fetchAll();
		}
                       

        $escort->services = self::db()->query('
			SELECT service_id, price
			FROM escort_services
			WHERE escort_id = ?
		', $escort->id)->fetchAll();
		
        $escort->keywords = self::db()->query('
			SELECT keyword_id
			FROM escort_keywords
			WHERE escort_id = ?
		', $escort->id)->fetchAll();

        $escort->tours = self::db()->query('
			SELECT t.id, t.date_from, t.date_to, t.phone, t.country_id,
				t.city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country_title,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				c.slug AS country_slug, r.slug AS region_slug, ct.slug AS city_slug,
				IF(DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()), 1, 0) AS is_in_tour,
				IF(DATE(t.date_from) > DATE(NOW()), 1, 0) AS is_upcoming_tour
			FROM tours t

			INNER JOIN countries c ON c.id = t.country_id
			INNER JOIN cities ct ON ct.id = t.city_id
			INNER JOIN regions r ON r.id = ct.region_id
			WHERE t.escort_id = ? AND (DATE(t.date_from) >= DATE(NOW()) OR  DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()))
			ORDER BY t.date_from ASC
		', $escort->id)->fetchAll();
       
        $escort->is_open = self::db()->fetchOne('
			SELECT
				IF ((
					ewt.day_index = ' . date('N') . '
					AND ewt.time_from <= ' . date('G') . '
					AND ewt.time_to >= ' . date('G') . '
				) OR (
					ewt.day_index = ' . date('N') . '
					AND ewt.time_from <= ' . date('G') . '
					AND ewt.time_from > ewt.time_to
				) OR (
					ewt.day_index = ' . ( 1 == date('N') ? 7 : date('N') - 1 ) . '
					AND ewt.time_to >= ' . date('G') . '
					AND ewt.time_from  > ewt.time_to
				), 1, 0)
			FROM escort_working_times ewt
			WHERE ewt.escort_id = ? AND ewt.day_index = ?
		', array($escort->id, date('N')));

        $working_times = self::db()->query('
			SELECT ewt.day_index, ewt.time_from AS `from`, ewt.time_from_m AS `from_m`, ewt.time_to AS `to`, ewt.time_to_m AS `to_m`
			FROM escort_working_times ewt
			WHERE ewt.escort_id = ?
		', $escort->id)->fetchAll();

        $wts = array();
        foreach ($working_times as $i => $wt) {
            $wts[$wt->day_index] = $wt;
        }

        $escort->working_times = $wts;

        return $escort;
    }
    public function getProfileEGUKsc($showname, $lang_id, $status_excl = false, $only_active = false) {
        

        Zend_Controller_Front::getInstance()->getRequest()->setParam('lang_id', $lang_id);
//        return array($showname, $lang_id, $status_excl);

        $where = "  ";
        if ( !$status_excl ) {
            $where = " AND u.status = ? ";
        }

        if ( $only_active ) {
        	$where .= " AND e.status & 32 ";
        }
        
        $fields = '';
        

        $sql = '
			SELECT
				e.id,
				e.type,
				e.agency_id,
				e.showname,
				e.gender,
                e.block_website,
				e.status AS escort_status,
				e.is_pornstar,
				' . Cubix_I18n::getTblFields('ct.title', null, null, 'city_title') . ',
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				u.application_id,
				u.username,
				eph.status AS photo_status,

				/* Contacts Section */
				' . Cubix_I18n::getTblFields('ct.title', null, null, 'base_city') . ',
				ct.id AS base_city_id,
				' . Cubix_I18n::getTblFields('c.title', null, null, 'country') . ',
			
				ct.slug AS city_slug,

				IF(((DATE(e.vac_date_from) <= CURDATE() AND DATE(e.vac_date_to) > CURDATE()) OR DATE(e.vac_date_from) > CURDATE()), e.vac_date_to, 0) AS vac_date_to,
				ep.phone_number,
                ep.contact_phone_parsed as contact_phone_parsed,
				ep.phone_number_alt,
				ep.phone_instr,
				ep.phone_instr_no_withheld,
				ep.phone_instr_other,
				ep.phone_country_id,
				ep.website,
				ep.email,
				ep.zip,
				ep.club_name,
				ep.street,
				ep.street_no,
				ep.address_additional_info,

				/* About Section */
				/*FLOOR(DATEDIFF(CURDATE(), ep.birth_date) / 365.25) as age,*/
				ep.birth_date,
				ep.eye_color,
				ep.hair_color,
				ep.hair_length,
				ep.height,
				ep.weight,
				ep.bust,
				ep.waist,
				ep.hip,
				ep.shoe_size,
				ep.cup_size,
				ep.breast_type,
				ep.dress_size,
				ep.is_smoking,
				ep.pubic_hair,
				ep.is_drinking,
				ep.measure_units,
                                
				ep.cuisine,
				ep.drink,
				ep.flower,
				ep.gifts,
				ep.perfume,
				ep.designer,
				ep.interests,
				ep.sports,
				ep.hobbies,
				
				ep.min_book_time,
				ep.min_book_time_unit,
				
				ep.has_down_pay,
				ep.down_pay,
				ep.down_pay_cur,
				
				ep.reservation,

				ep.incall_type,
				ep.incall_hotel_room,
				ep.incall_other,
				ep.outcall_type,
				ep.outcall_other,

				ep.sex_orientation,
				ep.sex_availability,
				ep.ethnicity,
				' . Cubix_I18n::getTblFields('ep.about', null, null, 'about') . ',
				ep.characteristics,

				
				e.date_last_modified,
				e.date_registered,
			

				' . Cubix_I18n::getTblFields('ep.additional_service', null, null, 'additional_service') . ',
				ep.available_24_7,
				' . $fields . '
				ep.special_rates
			FROM escorts e
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1

			INNER JOIN countries c ON c.id = e.country_id
			LEFT JOIN cities ct ON ct.id = e.city_id
			
			WHERE u.application_id = ? ' . $where . ' AND e.' . ( is_numeric($showname) ? 'id' : 'showname' ) . ' = ? AND NOT (e.status & 256)
			GROUP BY e.id
		';
		
        try {
        	$filter = array(self::app()->id, STATUS_ACTIVE, $showname);
            if ($status_excl) {
                $filter = array(self::app()->id, $showname);
            }

            if (!$escort = self::db()->fetchRow($sql, $filter)) {
                return array('error' => 'Escort with showname or id `' . print_r($showname, true) . '` could not be found');
            }
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        

        if (!$escort) {
            return null;
        }

        $escort->langs = self::db()->query('
			SELECT el.lang_id AS id, el.level, l.title FROM escort_langs el
			LEFT JOIN langs l ON l.id = el.lang_id
			WHERE el.escort_id = ?
		', $escort->id)->fetchAll();

        $escort->rates = self::db()->query('
			SELECT availability, type, time, time_unit, price, currency_id FROM escort_rates
			WHERE escort_id = ?
		', $escort->id)->fetchAll();
		
		if ( in_array(self::app()->id, array(33, 22, APP_ED)) ) {
                       	$escort->travel_payment_methods = self::db()->query('
				SELECT payment_method, specify FROM travel_payment_methods
				WHERE escort_id = ?
			', $escort->id)->fetchAll();
		}
                       

        $escort->services = self::db()->query('
			SELECT service_id, price
			FROM escort_services
			WHERE escort_id = ?
		', $escort->id)->fetchAll();
		
        $escort->keywords = self::db()->query('
			SELECT keyword_id
			FROM escort_keywords
			WHERE escort_id = ?
		', $escort->id)->fetchAll();

        $escort->tours = self::db()->query('
			SELECT t.id, t.date_from, t.date_to, t.phone, t.country_id,
				t.city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country_title,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				c.slug AS country_slug, r.slug AS region_slug, ct.slug AS city_slug,
				IF(DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()), 1, 0) AS is_in_tour,
				IF(DATE(t.date_from) > DATE(NOW()), 1, 0) AS is_upcoming_tour
			FROM tours t

			INNER JOIN countries c ON c.id = t.country_id
			INNER JOIN cities ct ON ct.id = t.city_id
			INNER JOIN regions r ON r.id = ct.region_id
			WHERE t.escort_id = ? AND (DATE(t.date_from) >= DATE(NOW()) OR  DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()))
			ORDER BY t.date_from ASC
		', $escort->id)->fetchAll();
       
        $escort->is_open = self::db()->fetchOne('
			SELECT
				IF ((
					ewt.day_index = ' . date('N') . '
					AND ewt.time_from <= ' . date('G') . '
					AND ewt.time_to >= ' . date('G') . '
				) OR (
					ewt.day_index = ' . date('N') . '
					AND ewt.time_from <= ' . date('G') . '
					AND ewt.time_from > ewt.time_to
				) OR (
					ewt.day_index = ' . ( 1 == date('N') ? 7 : date('N') - 1 ) . '
					AND ewt.time_to >= ' . date('G') . '
					AND ewt.time_from  > ewt.time_to
				), 1, 0)
			FROM escort_working_times ewt
			WHERE ewt.escort_id = ? AND ewt.day_index = ?
		', array($escort->id, date('N')));

        $working_times = self::db()->query('
			SELECT ewt.day_index, ewt.time_from AS `from`, ewt.time_from_m AS `from_m`, ewt.time_to AS `to`, ewt.time_to_m AS `to_m`
			FROM escort_working_times ewt
			WHERE ewt.escort_id = ?
		', $escort->id)->fetchAll();

        $wts = array();
        foreach ($working_times as $i => $wt) {
            $wts[$wt->day_index] = $wt;
        }

        $escort->working_times = $wts;

        return $escort;
    }
    private static function _whereStatus2($field, $status, $excl_status = array()) {
        if (!is_null($status)) {
            if (!is_array($status))
                $status = array($status);
        }
        else {
            $status = array();
        }
        if (!is_array($excl_status))
            $excl_status = array($excl_status);

        $ret = array();

        foreach ($status as $st) {
            $ret[] = "({$field} & {$st})";
        }

        foreach ($excl_status as $st) {
            $ret[] = " NOT ({$field} & {$st})";
        }

        return $ret;
    }

    /**
     *
     * @param array $escort_id
     * @param array $city_ids
     * @param array $limit
     * @return mixed
     */
    public function getMoreEscorts($escort_id, $city_ids, $limit) {
        $sql = '
			SELECT e.showname FROM escorts e 
			INNER JOIN escort_cities ec ON e.id = ec.escort_id
			
			INNER JOIN escort_profiles ep ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN applications a ON a.id = u.application_id
			
			INNER JOIN countries c ON c.id = e.country_id
			INNER JOIN cities cc ON cc.id = ec.city_id
			
			INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			
			WHERE ec.city_id IN (?) AND e.id <> ?
			
			GROUP BY e.id
			ORDER BY RAND()
			LIMIT ?
		';

        $cities = implode(',', $city_ids);

        try {
            $result = self::db()->query($sql, array($cities, $escort_id, $limit))->fetchAll();
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return $result;
    }

    /**
     *
     * @param array $user_id
     * @return mixed
     */
    
    
    public function getByUserId($user_id) 
    {
		$fields = "";
		$fields .= (Cubix_Application::getId() == APP_EF ) ? ', e.need_age_verification , e.need_age_verify_video, e.need_idcard_video ' : '';

		if (Cubix_Application::getId() == APP_ED ) {
			$fields .= ', FLOOR(DATEDIFF(CURDATE(), ep.birth_date) / 365.25) AS age, 
				ct.' . Cubix_I18n::getTblField('title') . ' AS base_city, 
				c.' . Cubix_I18n::getTblField('title') . ' AS country,
				u.pm_is_blocked,
				e.type';
		}
		
		if (Cubix_Application::getId() == APP_BL ) {
			$fields .= ' , e.is_vaccinated ';
		}

		$sql = '
			SELECT
				e.id, e.showname, u.email, ep.contact_phone_parsed, ep.phone_sms_verified, e.auto_approval, e.verified_status,e.photo_rotate_type, u.application_id,
				e.country_id, e.city_id, e_p.hash AS photo_hash, e_p.ext AS photo_ext, e_p.status AS photo_status, bu.username as sales_person,
				e.gender,e.status,e.is_suspicious,e.deleted_date, e.disabled_reviews, e.disabled_comments'. $fields .'
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN backend_users bu ON bu.id = u.sales_user_id
			LEFT JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			LEFT JOIN escort_photos e_p ON e_p.escort_id = e.id AND e_p.is_main = 1';
		if (Cubix_Application::getId() == APP_ED) {
			$sql .= '
				LEFT JOIN countries c ON c.id = e.country_id
				LEFT JOIN cities ct ON ct.id = e.city_id
			';
		}


		$sql .= ' WHERE u.id = ?';
		
        $user = self::db()->fetchRow($sql, $user_id);
		
        if (!$user) {
            return null;
        }
		
        return $user;
	}

	/**
	 * @param int $escort_id
	 * @return mixed
	 */
	
	public static function getInstantBookEscorts($escort_id)
	{
		$id = (int) $escort_id;
		$where = '';

		if ($id) {
			$where = ' WHERE escort_id = ' . $id;
		}

		$result = self::db()->fetchAll('
			SELECT id, escort_id, start_date, end_date, incall, outcall
			FROM instant_book_escorts ' . $where
		);

		if (!$result) {
			return null;
		}

		return $result;
	}

    /**
     *
     * @param array $id
     * @return mixed
     */
    public static function getById($id) {
        $id = intval($id);

		$fields = "";
        $join = "";
		$fields .= (Cubix_Application::getId() == APP_EF ) ? ', e.need_idcard_video ' : '';
		$fields .= (Cubix_Application::getId() == APP_ED ) ? ', e.type, u.pm_is_blocked ' : '';
		$fields .= (Cubix_Application::getId() == APP_A6 ) ? ', c.title_'. Cubix_I18n::getLang() . ' as f_city, e.slogan' : '';
        $join .= (Cubix_Application::getId() == APP_A6 ) ? 'INNER JOIN cities c ON c.id = e.city_id' : '';

        $escort = self::db()->fetchRow('
			SELECT
				e.id, u.email, ep.phone_number, u.application_id, e.verified_status,e.auto_approval, e.agency_id, e.showname, e.status, e.gender, e.photo_rotate_type,
				e_p.hash AS photo_hash, e_p.ext AS photo_ext, e_p.status AS photo_status, e_p.is_main, ep.phone_number AS contact_phone,e.is_suspicious, e.disabled_reviews, e.disabled_comments
				'.$fields.'
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
			LEFT JOIN escort_photos e_p ON e_p.escort_id = e.id AND e_p.is_main = 1
			' . $join . '
			WHERE e.id = ? GROUP BY e.id
		', $id);

        if (!$escort) {
            return null;
        }

        return $escort;
    }

    // Status manipulaions

    const ESCORT_STATUS_NO_PROFILE         = 1;     // 000000001
    const ESCORT_STATUS_NO_ENOUGH_PHOTOS   = 2;     // 000000010
    const ESCORT_STATUS_NOT_APPROVED       = 4;     // 000000100
    const ESCORT_STATUS_OWNER_DISABLED     = 8;     // 000001000
    const ESCORT_STATUS_ADMIN_DISABLED     = 16;    // 000010000
    const ESCORT_STATUS_ACTIVE             = 32;    // 000100000
    const ESCORT_STATUS_IS_NEW             = 64;    // 001000000
    const ESCORT_STATUS_PROFILE_CHANGED    = 128;   // 010000000
	const ESCORT_STATUS_DELETED            = 256;   // 100000000
    const ESCORT_STATUS_PARTIALLY_COMPLETE = 32768;	// 1000000000000000
    const ESCORT_STATUS_INACTIVE           = 16384; // 100000000000000

    const ESCORT_STATUS_TEMPRARY_DELETED = 1024;  // 10000000000

    public function setStatusBit($escort_id, $status) {
        $escort_id = (is_array($escort_id)) ? reset($escort_id) : $escort_id;

        if (!is_array($status))
            $status = array($status);

        $old_status = self::db()->fetchOne('SELECT status FROM escorts WHERE id = ?', $escort_id);

        foreach ($status as $bit) {
            $old_status = $old_status | $bit;
        }

		return self::db()->update('escorts', array('status' => $old_status), self::db()->quoteInto('id = ?', $escort_id));
    }

	public function activate($escort_id)
	{
		
		$status = new Cubix_EscortStatus($escort_id);
		$status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
		$status->save();
	}

	public function deactivate($escort_id)
	{
		
		$status = new Cubix_EscortStatus($escort_id);
		$status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED);
		$status->save();
	}

    public function addDelData($escort_id, $hash,$leaving_reason){
        $escort_id = (is_array($escort_id)) ? reset($escort_id) : $escort_id;
        
        

        self::db()->update('escorts', array('del_hash' => $hash,'leaving_reason' => $leaving_reason), self::db()->quoteInto('id = ?', $escort_id));
    }


    public function removeStatusBit($escort_id, $status)
    {
        $escort_id = (is_array($escort_id)) ? reset($escort_id) : $escort_id;

        if (!is_array($status))
            $status = array($status);

        $old_status = self::db()->fetchOne('SELECT status FROM escorts WHERE id = ?', $escort_id);

        foreach ($status as $bit) {
            if ((boolean)($old_status & $bit)) {
                $old_status = $old_status & ~$bit;
            }
        }

        return self::db()->update('escorts', array('status' => $old_status), self::db()->quoteInto('id = ?', $escort_id));
    }

    public function hasStatusBit($escort_id, $status) {
        $escort_id = (is_array($escort_id)) ? reset($escort_id) : $escort_id;

        $old_status = self::db()->fetchOne('SELECT status FROM escorts WHERE id = ?', $escort_id);

		foreach ($status as $bit) {
			if((boolean)($old_status & $bit)){
				return true;
			}
        }

        return false;
    }

    public static function getProfileByUserId($user_id) {
        if (is_array($user_id))
            $user_id = reset($user_id);

        $sql = '
			SELECT
				e.id,
				e.type,
				e.agency_id,
				e.showname,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				u.application_id,
				ep.contact_email,
				u.username,
				eph.status AS photo_status,
				e.gender,
				
				/* Contacts Section */
				ct.id AS city_id,
				ct.' . Cubix_I18n::getTblField('title') . ' AS base_city,
				ct.id AS base_city_id,
				c.' . Cubix_I18n::getTblField('title') . ' AS country,
				c.slug AS country_slug,
				r.slug AS region_slug,
				r.' . Cubix_I18n::getTblField('title') . ' AS region_title,
				ct.slug AS city_slug,
				
				ep.vac_date_to,
				ep.contact_phone,
				ep.phone_instructions,
				ep.contact_web,
				ep.contact_email,
				ep.contact_zip,
				
				/* About Section */
				UNIX_TIMESTAMP(ep.birth_date) AS birth_date,
				n.id AS nationality_id,
				n.' . Cubix_I18n::getTblField('title') . ' AS nationality_title,
				ep.eye_color,
				ep.hair_color,
				ep.hair_length,
				ep.height,
				ep.weight,
				ep.bust,
				ep.waist,
				ep.hip,
				ep.shoe_size,
				ep.breast_size,
				ep.dress_size,
				ep.is_smoker,
				ep.measure_units,
				ep.availability,
				ep.sex_availability,
				ep.ethnicity,
				' . Cubix_I18n::getTblFields('ep.about', self::app()->id) . ',
				ep.characteristics,
				
				/* SetcardInfo Section */
				e.date_last_modified,
				u.date_registered,
				ep.hit_count,
				
				/* Services Section */
				ep.svc_kissing,
				ep.svc_blowjob,
				ep.svc_cumshot,
				ep.svc_69,
				ep.svc_anal,
				' . Cubix_I18n::getTblFields('ep.svc_additional', self::app()->id) . ',
			FROM escorts e
			INNER JOIN escort_profiles ep ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id			
			INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			
			INNER JOIN countries c ON c.id = e.country_id
			INNER JOIN cities ct ON ct.id = e.city_id 
			INNER JOIN regions r ON r.id = ct.region_id
			
			LEFT JOIN nationalities n ON n.id = ep.nationality_id
			
			WHERE u.id = ?
		';

        try {
            if (!$escort = self::db()->fetchRow($sql, $user_id)) {
                return array('error' => 'Escort with user_id `' . $user_id . '` could not be found');
            }
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        if (!$escort) {
            return null;
        }

        $escort->langs = self::db()->query('
			SELECT el.lang_id AS id, el.level, l.title FROM escort_langs el
			LEFT JOIN langs l ON l.id = el.lang_id 
			WHERE el.escort_id = ?
		', $escort->id)->fetchAll();

        $working_times = self::db()->query('
			SELECT ewt.day_index, ewt.time_from AS `from`, ewt.time_to AS `to`
			FROM escort_working_times ewt
			WHERE ewt.escort_id = ?
		', $escort->id)->fetchAll();

        $wts = array();
        foreach ($working_times as $i => $wt) {
            $wts[$wt->day_index] = $wt;
        }

        $escort->working_times = $wts;

        return $escort;
    }

    public function delete($escort_id) {
        $escort_id = intval($escort_id);

        $user = self::db()->query('SELECT u.id, u.user_type FROM escorts e INNER JOIN users u ON u.id = e.user_id WHERE e.id = ?', $escort_id)->fetch();

        try {
            if ($user && 'escort' == $user->user_type) {
                self::db()->delete('users', self::db()->quoteInto('id = ?', $user->id));
            }

            self::db()->delete('escorts', self::db()->quoteInto('id = ?', $escort_id));
            self::db()->delete('escort_profiles', self::db()->quoteInto('escort_id = ?', $escort_id));
            self::db()->delete('escort_rates', self::db()->quoteInto('escort_id = ?', $escort_id));
            self::db()->delete('escort_working_times', self::db()->quoteInto('escort_id = ?', $escort_id));
            self::db()->delete('escort_photos', self::db()->quoteInto('escort_id = ?', $escort_id));
            self::db()->delete('escort_langs', self::db()->quoteInto('escort_id = ?', $escort_id));
            self::db()->delete('escort_cities', self::db()->quoteInto('escort_id = ?', $escort_id));
            self::db()->delete('escort_cityzones', self::db()->quoteInto('escort_id = ?', $escort_id));
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

    public function deleteV2($escort_id) {
        $escort_id = intval($escort_id);

        //$user = self::db()->query('SELECT u.id, u.user_type FROM escorts e INNER JOIN users u ON u.id = e.user_id WHERE e.id = ?', $escort_id)->fetch();

        try {
            /*if ($user && 'escort' == $user->user_type) {
                self::db()->update('users', array('status' => USER_STATUS_DELETED), self::db()->quoteInto('id = ?', $user->id));
            }*/

			$status = new Cubix_EscortStatus($escort_id);
			$status->setStatus(Cubix_EscortStatus::ESCORT_STATUS_DELETED);
			$status->save();
			
           
            /* self::db()->delete('escorts', self::db()->quoteInto('id = ?', $escort_id));
              self::db()->delete('escort_profiles', self::db()->quoteInto('escort_id = ?', $escort_id));
              self::db()->delete('escort_rates', self::db()->quoteInto('escort_id = ?', $escort_id));
              self::db()->delete('escort_working_times', self::db()->quoteInto('escort_id = ?', $escort_id));
              self::db()->delete('escort_photos', self::db()->quoteInto('escort_id = ?', $escort_id));
              self::db()->delete('escort_langs', self::db()->quoteInto('escort_id = ?', $escort_id));
              self::db()->delete('escort_cities', self::db()->quoteInto('escort_id = ?', $escort_id));
              self::db()->delete('escort_cityzones', self::db()->quoteInto('escort_id = ?', $escort_id)); */
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

    public function deleteTemporary($escort_id, $by_owner = false) {
        

        $escort_id = intval($escort_id);
       
//        $user = self::db()->query('SELECT u.id, u.user_type FROM escorts e INNER JOIN users u ON u.id = e.user_id WHERE e.id = ?', $escort_id)->fetch();
        
        try {
            $status = new Cubix_EscortStatus($escort_id);

            self::db()->update('escorts', array( 'deleted_date' =>  new Zend_Db_Expr('NOW()') ), self::db()->quoteInto('id = ?', $escort_id));
            
            $sql = "
                SELECT op.period, op.date_activated, op.expiration_date, p.name,op.order_id
                FROM order_packages op
                INNER JOIN escorts e ON e.id = op.escort_id
                INNER JOIN packages p ON p.id = op.package_id
                WHERE e.id = ? AND p.is_default = 0 AND op.status = 2
                ORDER BY op.date_activated DESC
            ";
            $package = self::db()->query($sql, array($escort_id))->fetch();

            if($package->order_id){
                return json_encode(array("success" => false,"message" => "Cannot Delete Escort Which has Package"));
             }

            if($by_owner == false) {
                $status->setStatusBit(self::ESCORT_STATUS_ADMIN_DISABLED);
            }

            $status->setStatusBit(self::ESCORT_STATUS_TEMPRARY_DELETED);
            $status->save();
            return json_encode(array("success" => true,"message" => ""));
//            return true;

        } catch (Exception $e) {
            return json_encode(array("success" => false,"message" => Cubix_Api_Error::Exception2Array($e)));
        }
    }
//1.1 
    public function restore($escort_id, $by_owner = false){
        $escort_id = intval($escort_id);

        try {

            $status = new Cubix_EscortStatus($escort_id);

            $sql = "
                SELECT op.period, op.date_activated, op.expiration_date, p.name,op.order_id
                FROM order_packages op
                INNER JOIN escorts e ON e.id = op.escort_id
                INNER JOIN packages p ON p.id = op.package_id
                WHERE e.id = ? AND p.is_default = 0 AND op.status = 2
                ORDER BY op.date_activated DESC
            ";

            $status->removeStatusBit(self::ESCORT_STATUS_TEMPRARY_DELETED);
            if($by_owner == false) {
                $status->setStatusBit(self::ESCORT_STATUS_ADMIN_DISABLED);
            }

            $status->save();

            
        }catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }


    public static function getProfileLatestRevision($escort_id) {
        $escort_id = intval($escort_id);

        if (!$escort_id)
            return false;

        $revision = self::db()->fetchOne('SELECT MAX(revision) FROM profile_updates WHERE escort_id = ?', $escort_id);
        $revision = intval($revision);

        return self::_getProfileByRevision($escort_id, $revision);
    }

    protected static function _getProfileByRevision($escort_id, $revision) {
        $profile = self::db()->fetchOne('
			SELECT data FROM profile_updates
			WHERE escort_id = ? AND revision = ?
		', array($escort_id, $revision));

        if (!$profile) {
            $user_id = self::db()->fetchRow('
				SELECT user_id FROM escorts
				WHERE id = ?
			', array($escort_id));

            $profile = self::getProfileByUserId($user_id->user_id);

            return $profile;
        }

        return unserialize($profile);
    }

    public static function getEscortProfileOverview($escort_id) {
        $escort_id = intval($escort_id);

        $data = array();
        if (!$escort_id)
            return false;

        $data['revision'] = self::db()->fetchRow('SELECT date_updated, status FROM profile_updates_v2 WHERE escort_id = ? ORDER BY REVISION DESC', $escort_id);
        $data['contact_info'] = self::db()->fetchRow('SELECT phone_number, phone_country_id, disable_phone_prefix, website FROM escort_profiles_v2 WHERE escort_id = ?', $escort_id);


        return $data;
    }

    public function updateShowname($escort_id, $showname) {
        try {
            
            
            self::db()->update('escorts', array('showname' => $showname), self::db()->quoteInto('id = ?', $escort_id));
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

    public function getBannersEd()
    {
        $db = self::db();
        $sql = "SELECT * FROM ixs_banners";

        try{
            return $db->fetchAll($sql);
        }catch(\Exception $e) {
            echo $sql;
            die($e->getMessage());
        }
    }


	public function updatePremiumCredits($escort_id, $count) {
        try {
            
            self::db()->update('escorts', array('premium_credits' => $count), self::db()->quoteInto('id = ?', $escort_id));
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }


    public static function updateProfile($escort_id, $data, $add_data = array()) {
        try {
            //if ( $add_data )
            $escort_id = intval($escort_id);

            if (!$escort_id) {
                $_d = array('showname' => $data['showname'], 'gender' => $data['gender'], 'status' => 66/* self::ESCORT_STATUS_IS_NEW */, 'date_registered' => new Zend_Db_Expr('CURRENT_TIMESTAMP'));
                $_d = array_merge($_d, $add_data);

                $escort_id = self::_insertEscort($_d);
                


                self::db()->insert('escort_profiles', array('escort_id' => $escort_id));
                
                //return array('error' => 'Wrong escort_id');
            }          


            $last_approved_revision = self::db()->fetchOne('SELECT MAX(revision) FROM profile_updates WHERE escort_id = ? AND status = ?', array($escort_id, self::REVISION_STATUS_APPROVED));
            $last_approved_revision = intval($last_approved_revision);
            $last_unapproved_revision = self::db()->fetchOne('SELECT MAX(revision) FROM profile_updates WHERE escort_id = ? AND status = ?', array($escort_id, self::REVISION_STATUS_NOT_APPROVED));
            $last_unapproved_revision = intval($last_unapproved_revision);

            $revision = 0;
            // We have an unapproved revision later than approved revision
            if ($last_unapproved_revision > $last_approved_revision) {
                // Then we have to compare with unapproved revision
                $revision = $last_unapproved_revision;
            }
            // Otherwise compare with approved revision
            else {
                $revision = $last_approved_revision;
            }

            // Refresh escort rates
            $data['rates'] = self::getRates(array($escort_id));
            foreach ($data['rates'] as $i => $rate) {
                $data['rates'][$i] = (array) $rate;
            }

            // --> Compare revisions
            $old_profile = false;

            if ($revision > 1) {
                $old_profile = self::_getProfileByRevision($escort_id, $revision - 1);
            }

            if (!$old_profile) {
                $old_profile = array();
            }

            $result = self::_differProfile($old_profile, $data);
            // <--
            // Process profile update only if something changed in requested profile after last update
            if (count($result['added']) || count($result['changed']) || count($result['removed'])) {
                if ($last_unapproved_revision >= $revision && $last_unapproved_revision > 0) {
                    self::db()->update('profile_updates', array(
                        'date_updated' => new Zend_Db_Expr('NOW()'),
                        'data' => serialize($data)
                            ), array(
                        self::db()->quoteInto('escort_id = ?', $escort_id),
                        self::db()->quoteInto('revision = ?', $last_unapproved_revision)
                    ));
                } else {
                    self::db()->insert('profile_updates', array(
                        'escort_id' => $escort_id,
                        'revision' => $revision + 1,
                        'date_updated' => new Zend_Db_Expr('NOW()'),
                        'data' => serialize($data)
                    ));
                }

                return $result;
            }
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return false;
    }

    protected static function _insertEscort($data) {
        self::db()->insert('escorts', $data);
        return self::db()->lastInsertId();
    }

    protected static function _differProfile($old, $new) {
        $old = (array) $old;
        $new = (array) $new;

        $added = array();
        $changed = array();
        $removed = array();
        foreach ($new as $field => $value) {
            if ((!isset($old[$field]) || !$old[$field]) && $value) {
                $added[] = $field;
            } elseif (!$value && (isset($old[$field]) && $old[$field])) {
                $removed[] = $field;
            } elseif (isset($old[$field]) && $old[$field] != $value) {
                $changed[] = $field;
            }
        }

        return array('added' => $added, 'changed' => $changed, 'removed' => $removed);
    }

    public function hasProfile($user_id) {
        $user_id = intval($user_id);

        $has_profile = (bool) self::db()->fetchRow('
			SELECT 1
			FROM profile_updates pu
			INNER JOIN escorts e ON e.id = pu.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE u.id = ?
		', $user_id);

        if (!$has_profile) {
            return (bool) self::db()->fetchRow('
				SELECT 1
				FROM escort_profiles ep
				INNER JOIN escorts e ON e.id = ep.escort_id
				INNER JOIN users u ON u.id = e.user_id
				WHERE u.id = ?
			', $user_id);
        }

        return $has_profile;
    }

    // Photos

    const PHOTO_STATUS_VERIFIED = 1;
    const PHOTO_STATUS_NOT_VERIFIED = 2;
    const PHOTO_STATUS_NORMAL = 3;

    public function setMainPhoto($photo_id, $sign_hash = array('')) {
        $photo_id = intval($photo_id);
        

        try {
            $is_approved = (in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK))) ? '' : 'AND is_approved = 1';
            $photo = self::db()->fetchRow("SELECT * FROM escort_photos WHERE id = ? {$is_approved}", $photo_id);

            if (!$photo) {
                return array('error' => 'Photo with id `' . $photo_id . '` does not exist');
            }
			
			self::db()->query('
				UPDATE escort_photos ep 
				SET ep.is_main = FALSE
				WHERE ep.escort_id = ?
			', $photo->escort_id);

            self::db()->query('UPDATE escort_photos SET is_main = TRUE WHERE id = ?', $photo_id);
			
			self::db()->update('escorts',  array('date_last_modified' => new Zend_Db_Expr('NOW()')), array(self::db()->quoteInto('id = ?', $photo->escort_id )) ) ;
			$snapshot = self::db()->query('SELECT data FROM escort_profiles_snapshots_v2 WHERE escort_id = ?', $photo->escort_id)->fetch();
                
			$snapshot = unserialize($snapshot->data);

			$snapshot->photo_hash = $photo->hash;
			$snapshot->photo_ext = $photo->ext;
			$snapshot->photo_status = $photo->status;

			$snapshot = serialize($snapshot);

			self::db()->query('UPDATE escort_profiles_snapshots_v2 SET data = ? WHERE escort_id = ?', array($snapshot, $photo->escort_id));
//			self::db()->insert('photo_updates', array(
//				'escort_id' => $photo->escort_id,
//				'sign_hash' => $sign_hash,
//				'action' => 'set-main',
//				'data' => serialize(array(
//					'id' => $photo->id,
//					'hash' => $photo->hash,
//					'ext' => $photo->ext
//				))
//			));

            Cubix_SyncNotifier::notify($photo->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array(
				'id' => $photo_id,
				'action' => 'made main'
			));
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return true;
    }

    public function setValentinesPhoto($photo_id) {
        $photo_id = intval($photo_id);


        try {
//            $is_approved = (in_array(Cubix_Application::getId(),array(APP_ED,APP_EG_CO_UK))) ? '' : 'AND is_approved = 1';
            $is_approved = '';
            $photo = self::db()->fetchRow("SELECT * FROM escort_photos WHERE id = ? {$is_approved}", $photo_id);

            if (!$photo) {
                return array('error' => 'Photo with id `' . $photo_id . '` does not exist');
            }

            $isBlocked = self::db()->fetchOne("SELECT true FROM escort_photos WHERE is_valentines >= 3 AND id = ? ", $photo_id);

            if ($isBlocked) {
                return array('error' => 'blocked_valentines_photo');
            }

            self::db()->query('
				UPDATE escort_photos ep 
				SET ep.is_valentines = 0
				WHERE ep.escort_id = ? AND ep.is_valentines < 3
			', $photo->escort_id);

            self::db()->query('UPDATE escort_photos SET is_valentines = 1 WHERE id = ?', $photo_id);

            self::db()->update('escorts',  array('date_last_modified' => new Zend_Db_Expr('NOW()')), array(self::db()->quoteInto('id = ?', $photo->escort_id )) ) ;

            $snapshot = self::db()->query('SELECT data FROM escort_profiles_snapshots_v2 WHERE escort_id = ?', $photo->escort_id)->fetch();

            $snapshot = unserialize($snapshot->data);

            $snapshot->photo_hash = $photo->hash;
            $snapshot->photo_ext = $photo->ext;
            $snapshot->photo_status = $photo->status;

            $snapshot = serialize($snapshot);

            self::db()->query('UPDATE escort_profiles_snapshots_v2 SET data = ? WHERE escort_id = ?', array($snapshot, $photo->escort_id));
            Cubix_SyncNotifier::notify($photo->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array(
                'id' => $photo_id,
                'action' => 'made main'
            ));
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return true;
    }

	 public function setMobileCover($photo_id, $sign_hash = array('')) {
        $photo_id = intval($photo_id);
        
        try {
            $photo = self::db()->fetchRow('SELECT * FROM escort_photos WHERE id = ? AND is_approved = 1', $photo_id);

            if (!$photo) {
                return array('error' => 'Photo with id `' . $photo_id . '` does not exist');
            }
			
			self::db()->query('
				UPDATE escort_photos ep 
				SET ep.is_mobile_cover = 0
				WHERE ep.escort_id = ?
			', $photo->escort_id);

            self::db()->query('UPDATE escort_photos SET is_mobile_cover = 1 WHERE id = ?', $photo_id);
			self::db()->update('escorts',  array('date_last_modified' => new Zend_Db_Expr('NOW()')), array(self::db()->quoteInto('id = ?', $photo->escort_id )) ) ;
			
            Cubix_SyncNotifier::notify($photo->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array(
				'id' => $photo_id,
				'action' => 'set mobile cover'
			));
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return true;
    }
	
	 public function setGotdPhoto($photo_id, $sign_hash = array('')) {
        $photo_id = intval($photo_id);
        

        try {
            $photo = self::db()->fetchRow('SELECT * FROM escort_photos WHERE id = ? AND is_approved = 1', $photo_id);

            if ( ! $photo ) {
                return array('error' => 'Photo with id `' . $photo_id . '` does not exist');
            }

            self::db()->query('
				UPDATE escort_photos ep SET ep.gotd = 0
				WHERE ep.escort_id = ?
			', $photo->escort_id);

            self::db()->query('UPDATE escort_photos SET gotd = 1 WHERE id = ?', $photo_id);

			Cubix_SyncNotifier::notify($photo->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array(
				'id' => $photo_id,
				'action' => 'made gotd'
			));
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return true;
    }

    public function setPhotoCropArgs($photo_id, $args) {
        $photo_id = intval($photo_id);
        

        try {
            $photo = self::db()->fetchRow('SELECT escort_id, hash FROM escort_photos WHERE id = ?', $photo_id);
            if (!$photo) {
                return array('error' => 'Photo with id `' . $photo . '` does not exist');
            }

            /* >>> Clear cached photos from pic server */
            $config = Zend_Registry::get('config');
            $params = array(
                'a' => 'clear_cache',
                'app' => self::app()->host,
                'eid' => $photo->escort_id,
                'hash' => $photo->hash
            );

            @get_headers($url = $config->picServer . '/get_image.php?' . http_build_query($params));
            /* <<< */

            // Update args in database
            self::db()->query('
				UPDATE escort_photos SET args = ? WHERE id = ?
			', array($args, $photo_id));

			Cubix_SyncNotifier::notify($photo->escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array(
				'id' => $photo_id,
				'action' => 'adj changed'
			));
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return true;
    }

    public function removePhotos($ids, $sign_hash = '') {
        
        

        foreach ($ids as $i => $id) {
            $ids[$i] = intval($id);
            if (!$ids[$i])
                unset($ids[$i]);
        }

        try {
            $photos = self::db()->query('SELECT * FROM escort_photos WHERE id IN (' . implode(', ', $ids) . ')')->fetchAll();
			
            $escort_id = null;
            $main_photo = null;
            foreach ($photos as $photo) {
                if ($photo->is_main) {
                    $main_photo = $photo;
                }

                $escort_id = $photo->escort_id;
				
				self::db()->insert('escort_photo_history', array(
					'escort_id' => $photo->escort_id,
					'hash' => $photo->hash,
					'ext' => $photo->ext,
				    'date' => new Zend_Db_Expr('NOW()')
				));
				
				
		
//				self::db()->insert('photo_updates', array(
//					'escort_id' => $photo->escort_id,
//					'sign_hash' => $sign_hash,
//					'action' => 'delete',
//					'data' => serialize(array(
//						'id' => $photo->id,
//						'hash' => $photo->hash,
//						'ext' => $photo->ext
//					))
//				));

				Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_DELETED, array(
					'id' => $photo->id,
					'hash' => $photo->hash,
					'ext' => $photo->ext
				));
            }

            self::db()->delete('escort_photos', 'id IN (' . implode(', ', $ids) . ') ');

            if (!is_null($escort_id)) {
				self::db()->update('escorts',  array('date_last_modified' => new Zend_Db_Expr('NOW()')), self::db()->quoteInto('id = ?', $escort_id ) ) ;
                $status = new Cubix_EscortStatus($escort_id);
                $status->save();
            }

            if (!is_null($main_photo)) {
                self::db()->query('UPDATE escort_photos SET is_main = 1 WHERE escort_id = ? AND type IN(1,2) ORDER BY ordering ASC LIMIT 1', $main_photo->escort_id);
                
				$snapshot = self::db()->query('SELECT data FROM escort_profiles_snapshots_v2 WHERE escort_id = ?', $main_photo->escort_id)->fetch();
                
				$snapshot = unserialize($snapshot->data);
				
				$main = self::db()->query('SELECT hash, ext, status FROM escort_photos WHERE is_main = 1 AND escort_id = ?', $main_photo->escort_id)->fetch();
				$snapshot->photo_hash = $main->hash;
				$snapshot->photo_ext = $main->ext;
				$snapshot->photo_status = $main->status;
				
				$snapshot = serialize($snapshot);
				
				self::db()->query('UPDATE escort_profiles_snapshots_v2 SET data = ? WHERE escort_id = ?', array($snapshot, $main_photo->escort_id));
				
				return self::db()->fetchOne('SELECT id FROM escort_photos WHERE is_main = 1 AND escort_id = ?', $main_photo->escort_id);
            }
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return true;
    }

	function setNotApprovedPhoto($photo_id)
	{
		global $db;
		
				
		try{
			$db->query('UPDATE escort_photos SET is_approved = 0 WHERE id = ?', $photo_id);
			return $db->lastInsertId();
		}
		catch ( Exception $e ) {
			return Cubix_Api_Error::Exception2Array($e);
		}
		return true;
	}

    public function getHash($photo_id) {
        return self::db()->fetchOne('SELECT hash FROM escort_photos WHERE id = ?', $photo_id);
    }
	
	public function getHashExt($photo_id) {
        return self::db()->fetchRow('SELECT hash, ext FROM escort_photos WHERE id = ?', $photo_id);
    }

    public function profileStatus($escort_id, $act){
        

        $status = new Cubix_EscortStatus($escort_id);
        switch ($act){
            case 'activate':
                $status->removeStatusBit(self::ESCORT_STATUS_INACTIVE);
                break;
            case 'enable':
                $status->removeStatusBit(self::ESCORT_STATUS_OWNER_DISABLED);
                break;
            case 'disable':
            default :
                $status->setStatusBit(self::ESCORT_STATUS_OWNER_DISABLED);
                break;
        }
        
        return $status->save();
    }

    public function getStatus($escort_id)
    {
        return self::db()->fetchOne('SELECT status FROM escorts WHERE id = ?', array($escort_id));
    }

    public function deleteProfile($escort_id){
        
        $status = new Cubix_EscortStatus($escort_id);
        $status->setStatusBit(self::ESCORT_STATUS_DELETED);
        return $status->save();
    }


    public function addPhoto($photo, $sign_hash = '') {
        

        try {
            $escort_status = self::db()->fetchOne('SELECT verified_status FROM escorts WHERE id = ?', $photo['escort_id']);

            if (self::VERIFIED_STATUS_VERIFIED == $escort_status || self::VERIFIED_STATUS_VERIFIED_RESET == $escort_status) {
                $photo['status'] = self::PHOTO_STATUS_NOT_VERIFIED;

                if (self::VERIFIED_STATUS_VERIFIED_RESET != $escort_status) {
                    self::db()->update('escorts', array('verified_status' => self::VERIFIED_STATUS_VERIFIED_RESET), self::db()->quoteInto('id = ?', $photo['escort_id']));
                }
            }

			self::db()->update('escorts',  array('date_last_modified' => new Zend_Db_Expr('NOW()')), self::db()->quoteInto('id = ?', $photo['escort_id'] ) ) ;
            self::db()->insert('escort_photos', $photo);
					
            $photo_id = self::db()->lastInsertId();

            Cubix_SyncNotifier::notify($photo['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_ADDED, array('id' => $photo_id));
			Cubix_AlertEvents::notify($photo['escort_id'], ALERT_ME_NEW_PICTURES, array('id' => $photo_id));
			
			if($photo['is_approved'] == 1 && Cubix_Application::getId() == APP_ED){
				Cubix_FollowEvents::notify($photo['escort_id'], 'escort', FOLLOW_ESCORT_NEW_PICTURE , array('rel_id' => $photo_id),false);
				
			}

            $status = new Cubix_EscortStatus($photo['escort_id']);
            $status->save();

//			self::db()->insert('photo_updates', array(
//				'escort_id' => $photo['escort_id'],
//				'sign_hash' => $sign_hash,
//				'action' => 'upload',
//				'data' => serialize(array(
//					'id' => $photo_id,
//					'hash' => $photo['hash'],
//					'ext' => $photo['ext']
//				))
//			));

            return $photo_id;
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

	public function hasMainPhoto($escort_id) {
        $escort_id = intval($escort_id);

        $res = self::db()->query('SELECT COUNT(id) AS cc FROM escort_photos WHERE escort_id = ? AND is_main = 1 AND type <> 3 AND type <> 5', $escort_id)->fetch();
		
		return $res->cc;
    }
	
	public function isMainPhoto($photo_id) {
        $photo_id = intval($photo_id);

        $res = self::db()->query('SELECT IF(is_main, 1, 0) AS cc FROM escort_photos WHERE id = ?', $photo_id)->fetch();
		
		return $res->cc;
    }
	
	public function getApprovedPhoto($escort_id, $photo_ids){
		return self::db()->fetchOne('SELECT id FROM escort_photos WHERE 
			is_approved = 1 AND type IN(1,2) AND retouch_status <> 1 AND escort_id = ? AND id IN('.$photo_ids.') 
			ORDER BY id LIMIT 1', $escort_id );
	}

    public function getValentinesPhoto($escort_id, $photo_ids){
        return self::db()->fetchOne('SELECT id FROM escort_photos WHERE 
			 type IN(1,2) AND retouch_status <> 1 AND escort_id = ? AND id IN('.$photo_ids.') 
			ORDER BY id LIMIT 1', $escort_id );
    }

    //
    public static function getRates($escort_id) {
        $escort_id = intval($escort_id);

        return self::db()->query('SELECT availability, time, time_unit, price, currency_id AS currency FROM escort_rates WHERE escort_id = ? ORDER BY CONCAT(time, " ", time_unit)', $escort_id)->fetchAll();
    }

    public function addRate($rate) {
        try {
            $old_rate = self::db()->fetchRow('
				SELECT * FROM escort_rates WHERE escort_id = ? AND availability = ? AND time = ? AND time_unit = ?
			', array($rate['escort_id'], $rate['availability'], $rate['time'], $rate['time_unit']));

            if (!$old_rate) {
                self::db()->insert('escort_rates', $rate);
            } else {
                self::db()->update('escort_rates', $rate, array(
                    self::db()->quoteInto('escort_id = ?', $rate['escort_id']),
                    self::db()->quoteInto('availability = ?', $rate['availability']),
                    self::db()->quoteInto('time = ?', $rate['time']),
                    self::db()->quoteInto('time_unit = ?', $rate['time_unit'])
                ));
            }

            self::_updateRatesRevision($rate['escort_id']);

            return $rate;
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

    public function deleteRates($escort_id, $rates) {
        $escort_id = intval($escort_id);

        try {
            foreach ($rates as $rate) {
                $rate = explode(':', $rate);

                self::db()->query('
					DELETE FROM escort_rates 
					WHERE escort_id = ?
					AND availability = ? AND time = ? AND time_unit = ? AND price = ? AND currency_id = ?
				', array_merge(array($escort_id), $rate));
            }

           	self::_updateRatesRevision($escort_id);
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return true;
    }

    protected static function _updateRatesRevision($escort_id) {
        // Recall profile revision update to refresh rates
        $revision = self::getProfileLatestRevision(array($escort_id));

       	self::updateProfile(array($escort_id), $revision);
    }

    //
    public function getTours($escort_id) {
        try {
            return self::db()->query('
				SELECT t.*, UNIX_TIMESTAMP(t.date_from) AS date_from, UNIX_TIMESTAMP(t.date_to) AS date_to, ' . Cubix_I18n::getTblField('c.title') . ' AS city_title
				FROM tours t
				INNER JOIN cities c ON c.id = t.city_id
				WHERE escort_id = ?
			', $escort_id)->fetchAll();
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

    public function addTour($tour) {
        try {
            $old_tour = self::db()->fetchRow('SELECT id FROM tours WHERE city_id = ? AND country_id = ? AND escort_id = ?', array($tour['city_id'], $tour['country_id'], $tour['escort_id']));

            $tour_data = $tour;
            $tour_data['date_from'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $tour['date_from'] . ')');
            $tour_data['date_to'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $tour['date_to'] . ')');

            if (!$old_tour) {
                self::db()->insert('tours', $tour_data);
                $tour['id'] = self::db()->lastInsertId();
            } else {
                self::db()->update('tours', $tour_data, array(
                    self::db()->quoteInto('id = ?', $old_tour->id)
                ));
                $tour['id'] = $old_tour->id;
            }

			Cubix_SyncNotifier::notify($tour['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_TOUR_ADDED, array(
				'id' => $tour['id']
			));
			
			Cubix_LatestActions::addDirectAction($tour['escort_id'], Cubix_LatestActions::ACTION_TOUR_UPDATE);
			Cubix_LatestActions::addDirectActionDetail($tour['escort_id'], Cubix_LatestActions::ACTION_TOUR_UPDATE, $tour['id']);
			$date = date('Y-m-d H:i:s');
			$status  =$tour_data['date_from']<= $date && $tour_data['date_to']>= $date?ESCORT_ON_TOUR:ESCORT_PLANNED;
			Cubix_AlertCityEvents::notify($tour['escort_id'], $status,$tour);
			return self::db()->fetchRow('
				SELECT t.*, UNIX_TIMESTAMP(t.date_from) AS date_from, UNIX_TIMESTAMP(t.date_to) AS date_to, ' . Cubix_I18n::getTblField('c.title') . ' AS city_title 
				FROM tours t
				INNER JOIN cities c ON c.id = t.city_id
				WHERE t.id = ?
			', $tour['id']);
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

    public function deleteTours($escort_id, $tour_ids) {
        $escort_id = intval($escort_id);

        try {
            foreach ($tour_ids as $id) {
                $id = intval($id);
                if (!$id)
                    continue;

                self::db()->delete('tours', array(
                    self::db()->quoteInto('escort_id = ?', $escort_id),
                    self::db()->quoteInto('id = ?', $id)
                ));

				Cubix_SyncNotifier::notify($tour['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_TOUR_DELETED, array(
					'id' => $id
				));
            }
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return true;
    }

    public function addVacation($escort_id, $data) {
        $escort_id = intval($escort_id);

        try {
            self::db()->update('escorts', $data, self::db()->quoteInto('id = ?', $escort_id));
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

    public function removeVacation($escort_id) {
        $escort_id = intval($escort_id);

        try {
            self::db()->update('escorts', array('vac_date_from' => null, 'vac_date_to' => null), self::db()->quoteInto('id = ?', $escort_id));
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

    public function getVacation($escort_id){
        return self::db()->query('SELECT vac_date_from, vac_date_to FROM escorts WHERE id = ?',array($escort_id) )->fetchAll();
    }

    public function save($data) {
        $data['date_registered'] = new Zend_Db_Expr('CURRENT_TIMESTAMP');

        try {
            self::db()->insert('escorts', $data);
			$last_id = self::db()->lastInsertId();
			self::setAdminVerifiedStatus($last_id, 9); //self registered;
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
		
        return $last_id;
    }

    /* Update Karo */

    public function checkDeletionHash($escort_id,$hash){
        
        return (bool)self::db()->fetchOne('SELECT true FROM escorts WHERE id = ? AND del_hash = ?' , array($escort_id, $hash));//

    }

    public function getBubbleTexts($page, $per_page, $region = null, $cat = null ,$country_id = null,$city_id = null)
	{
        if ( $page < 1 ) $page = 1;
		if ( $per_page < 1 ) $per_page = 1;

		$page--;
		$select = '';
        $groupBy = 'GROUP BY e.user_id';

		if(false and self::app()->id == APP_EG_CO_UK){
			$offset = ($page - 1) * $per_page;
		}else{
			$offset = $page * $per_page;
		}

	
		$limit = $per_page;
		$j = "";
		$w = "";
		$bind = array(self::app()->id, $offset, $limit);

		if(self::app()->id == APP_EG_CO_UK){

			$j = "INNER JOIN cities ct ON ct.id = e.city_id";
			$select .= ' ct.*, ';

			if ($city_id)
            {
                $w .= 'AND c.id = ' . $city_id;
            }
		}

        if (self::app()->id == APP_ED)
        {
            $j .= 'INNER JOIN countries co ON e.country_id = co.id';
            $w .= 'AND e.country_id = '.$country_id;
        }

		if ( self::app()->id == APP_A6  ) {
			$j = "
				INNER JOIN cities ct ON ct.id = e.city_id
				INNER JOIN regions r ON r.id = ct.region_id
			";
            $groupBy = 'GROUP BY e.id';
			$w = "AND r.slug = ?";
			
			$bind = array(self::app()->id, $region, $offset, $limit);
		}
		
        $g = null;

        if (!is_null($cat))
        {
            switch ($cat)
            {
                case 'escorts':
                    $g = GENDER_FEMALE;
                    break;
                case 'trans':
                    $g = GENDER_TRANS;
                    break;
                case 'boys':
                    $g = GENDER_MALE;
                    break;
                case 'bdsm':
                    $g = GENDER_BDSM;
                    break;
            }
        }

        if (!is_null($g))
        {
            $w .= ' AND e.gender = ' . $g;
        }
        $select .= 'e.id,
					e.showname,
					u.application_id,
					eph.hash AS photo_hash,
					eph.ext AS photo_ext,
					eph.status AS photo_status,
					bt.text,
					c.title_en AS city,
					UNIX_TIMESTAMP(bt.date) AS `date`';

        try {
            $return['texts'] = self::db()->query('
				SELECT SQL_CALC_FOUND_ROWS '.$select.'
				FROM escort_bubbletexts bt
				INNER JOIN escorts e ON e.id = bt.escort_id
				INNER JOIN cities c ON e.city_id = c.id
				INNER JOIN order_packages op ON e.id = op.escort_id AND op.status = 2
				INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
				INNER JOIN users u ON u.id = e.user_id
				' . $j . '
				WHERE
					u.application_id = ? AND
					u.status = 1 AND
					(e.status & ' . self::ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . self::ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . self::ESCORT_STATUS_ADMIN_DISABLED . ')
					AND bt.status = 1 
					AND bt.approvation = 1 
					AND op.package_id <> 97
					' . $w . ' 
					' .$groupBy. '
				ORDER BY bt.date DESC
				LIMIT ?, ?
			', $bind)->fetchAll();
//            Grouped by user_id to avoid spam from agencies -EGUK-258-
			$return['count'] = self::db()->fetchOne('SELECT FOUND_ROWS()');

            return $return;

        }
		catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e),'count' => 0);
        }
    }

    public function getLastBubbleText( $escort_id )
    {
        $sql = 'SELECT text,`date` FROM escort_bubbletexts WHERE escort_id = ? AND status = 1 ORDER BY `date` desc LIMIT 1';
        
        return self::db()->query($sql, $escort_id)->fetch();
    }

    public function getCitiesWithStatusMessages( $country_id ){
        $query = '
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' as title, COUNT(bt.id) as bresult
			FROM cities c
            INNER JOIN escorts e ON e.city_id = c.id
            INNER JOIN escort_bubbletexts bt ON bt.escort_id = e.id
            INNER JOIN order_packages op ON e.id = op.escort_id AND op.status = 2
			WHERE c.country_id = ' . $country_id . '
			    AND (e.status & ' . self::ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . self::ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . self::ESCORT_STATUS_ADMIN_DISABLED . ')
			    AND bt.status = 1 
                AND bt.approvation = 1 
                AND op.package_id <> 97
            GROUP BY c.id
			HAVING bresult IS NOT NULL
			ORDER BY title
		';

        return  self::db()->query($query, array())->fetchAll();
    }
    /* Update Karo */

    public function getLatestBubbleTexts($count) {
        

        try {
            return self::db()->query('
				SELECT
					e.id,
					e.showname,
					u.application_id,
					eph.hash AS photo_hash,
					eph.ext AS photo_ext,
					eph.status AS photo_status,
					bt.text
				FROM escort_bubbletexts bt
				INNER JOIN escorts e ON e.id = bt.escort_id
				INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
				INNER JOIN users u ON u.id = e.user_id

				WHERE
					u.application_id = ? AND
					u.status = 1 AND
					(e.status & ' . self::ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . self::ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . self::ESCORT_STATUS_ADMIN_DISABLED . ')
					AND bt.status = 1
				GROUP BY e.id
				ORDER BY bt.date DESC
				LIMIT ?
			', array(self::app()->id, $count))->fetchAll();
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

    public function getBubbleTextsMobile($page, $per_page, $city = null, $cat = null, $gps = array())
    {
        if ( $page < 1 ) $page = 1;
        if ( $per_page < 1 ) $per_page = 10;

        $page--;

        $offset = $page * $per_page;

        $limit = $per_page;

        $j = $w = $_geo_select = $_order = '';

        $bind = array(self::app()->id, $offset, $limit);
        if ( self::app()->id == APP_A6 || self::app()->id == APP_A6_AT ) {
            if( is_null( $city ) && count( $gps ) == 2 && $gps['long'] && $gps['lot'] ){
                $_geo_select = " ((2 * 6371 *
                    ATAN2(
                        SQRT(
                            POWER(SIN((RADIANS(" . $gps['lot'] . " - e.latitude))/2), 2) +
                            COS(RADIANS(e.latitude)) *
                            COS(RADIANS(" . $gps['lot'] . ")) *
                            POWER(SIN((RADIANS(" . $gps['long'] . " - e.longitude))/2), 2)
                        ),
                        SQRT(1-(
                            POWER(SIN((RADIANS(" . $gps['lot'] . " - e.latitude))/2), 2) +
                            COS(RADIANS(e.latitude)) *
                            COS(RADIANS(" . $gps['lot'] . ")) *
                            POWER(SIN((RADIANS(" . $gps['long'] . " - e.longitude))/2), 2)
                        ))
                    )
                )) AS dist,";

                $_order = 'dist';

                $bind = array(self::app()->id, $offset, $limit);
            } else {
                // $_geo_select = 'ct.region_id,';
                $j = "
                    INNER JOIN cities ct ON ct.id = e.city_id
                    INNER JOIN regions r ON r.id = ct.region_id
                ";
                if($city){
                    $w = "AND ct.id = ?";
                    $bind = array(self::app()->id, $city, $offset, $limit);
                }

                $_order = 'bt.date';

            }

        }

        $g = null;

        if (!is_null($cat))
        {
            switch ($cat)
            {
                case 'escorts':
                    $g = GENDER_FEMALE;
                    break;
                case 'trans':
                    $g = GENDER_TRANS;
                    break;
                case 'boys':
                    $g = GENDER_MALE;
                    break;
                case 'bdsm':
                    $g = GENDER_BDSM;
                    break;
            }
        }

        if (!is_null($g))
        {
            $w .= ' AND e.gender = ' . $g;
        }

        try {

            $query = '
				SELECT SQL_CALC_FOUND_ROWS
				    ' . $_geo_select . '
					e.id,
					e.city_id,
					e.longitude,
					e.latitude,
					e.showname,
					u.application_id,
					eph.hash AS photo_hash,
					eph.ext AS photo_ext,
					eph.status AS photo_status,
					bt.text,
					UNIX_TIMESTAMP(bt.date) AS `date`
				FROM escort_bubbletexts bt
				INNER JOIN escorts e ON e.id = bt.escort_id
				INNER JOIN order_packages op ON e.id = op.escort_id AND op.status = 2
				INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
				INNER JOIN users u ON u.id = e.user_id
				' . $j . '
				WHERE
					u.application_id = ? AND
					u.status = 1 AND
					(e.status & ' . self::ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . self::ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . self::ESCORT_STATUS_ADMIN_DISABLED . ')
					AND bt.status = 1 
					AND bt.approvation = 1 
					AND op.package_id <> 97
					' . $w . '
				GROUP BY e.id
				ORDER BY ' . $_order . ' DESC
				LIMIT ?, ?
			';

            if($cat == 1){ return $query;}

            $return['texts'] = self::db()->query($query, $bind)->fetchAll();

            $return['count'] = self::db()->fetchOne('SELECT FOUND_ROWS()');

            return $return;

        }
        catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e),'count' => 0);
        }
    }

    public function getBubbleText($escort_id) {
        
        try {
            return self::db()->query('
				SELECT
					bt.id, bt.date, bt.text, bt.status
				FROM escort_bubbletexts bt
				INNER JOIN escorts e ON e.id = bt.escort_id
				WHERE bt.escort_id = ? AND bt.status = 1 AND e.gender <> 2
				ORDER BY bt.date DESC
			', $escort_id)->fetch();
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
	}
	
	public function resetSlogansAndBubbleText($escort_id){

		self::db()->query('UPDATE escort_slogans SET status = 0 WHERE escort_id = ' . $escort_id);

		$last_bubble_text = self::db()->fetchRow('SELECT id, text, status FROM escort_bubbletexts WHERE escort_id = ? ORDER BY date DESC LIMIT 1', $escort_id);
		if ($last_bubble_text->id){
            self::db()->query('UPDATE escort_bubbletexts SET approvation = 0 WHERE escort_id = ' . $escort_id . ' AND id = ' . $last_bubble_text->id);
        }

		return true;
	}
	public function resetPremiumCities($escort_id){

        $package = self::db()->fetchRow('
		SELECT op.id AS opid
		FROM order_packages op
		INNER JOIN packages p ON p.id = op.package_id
		WHERE
			op.escort_id = ? AND
			op.status = ? AND
			op.application_id = ?
	', array($escort_id, 2, self::app()->id));


        if($package){
            self::db()->query('DELETE FROM premium_escorts WHERE order_package_id = ?', $package->opid);
        }


		return true;
	}


	public function getPackageCountryId($escort_id){
        $countries_a = array(10,24,33,67);
        $package = self::db()->fetchRow('
		SELECT op.id AS opid,op.country_id
		FROM order_packages op
		INNER JOIN packages p ON p.id = op.package_id and ordering is not null
		WHERE
			op.escort_id = ? AND
			op.status = ? AND
			op.application_id = ?
	', array($escort_id, 2, self::app()->id));
        $responce = array();
        if($package){
            $responce['op_id'] = $package->opid;
            if($package->country_id > 0){
                $responce['country_id'] = $package->country_id;
                if(in_array($package->country_id, $countries_a)){
                    // Country Type A
                    $responce['country_type'] = 1;
                }else{
                    // Country Type B
                    $responce['country_type'] = 2;
                }
            }



        }
        return $responce;

    }

	public function restorePremiumCities($escort_id, $country_id,$old_city_id,$city_id){

        $package = self::db()->fetchRow('
		SELECT op.id AS opid,op.country_id
		FROM order_packages op
		INNER JOIN packages p ON p.id = op.package_id
		WHERE
			op.escort_id = ? AND
			op.status = ? AND
			op.application_id = ?
	', array($escort_id, 2, self::app()->id));



        $countries_a = array(10,24,33,67);
        $can_restore = false;
        if($package){
            if(in_array($country_id, $countries_a) && in_array($package->country_id, $countries_a)){
                $can_restore = true;
            }elseif (!in_array($country_id, $countries_a) && !in_array($package->country_id, $countries_a)){
                $can_restore = true;
            }
            if($can_restore){
                self::db()->query('DELETE FROM premium_escorts WHERE order_package_id = ? AND city_id = ?', array($package->opid, $old_city_id));
                self::db()->query('DELETE FROM premium_escorts WHERE order_package_id = ? AND city_id = ?', array($package->opid, $city_id));
                self::db()->insert('premium_escorts', array('order_package_id' => $package->opid, 'city_id' => $city_id));
                
            }else{
                self::db()->query('DELETE FROM premium_escorts WHERE order_package_id = ?', $package->opid);
            }

        }
		return true;
	}

    public function addBubbleText($data, $appovation = 1) {
		if (is_array($appovation))
			$appovation = reset($appovation);
		$appovation = intval($appovation);
		
		// if(self::app()->id == APP_ED){
		// 	$appovation = 1;
		// }

        try {
        	if ( self::app()->id != APP_ED ) {
	            $one_per_day = self::db()->fetchOne('SELECT TRUE FROM escort_bubbletexts WHERE escort_id = ? AND DATE(date) = DATE(NOW())', $data['escort_id']);

	            if ($data['status'] != 0) {
	                if ($one_per_day) {
	                    return 'one_per_day_limit';
	                }
	            }
			}
            $last = self::db()->fetchRow('SELECT id, text, status FROM escort_bubbletexts WHERE escort_id = ? ORDER BY date DESC LIMIT 1', $data['escort_id']);

			if ( $last ) {
				self::db()->query('UPDATE escort_bubbletexts SET status = 0 WHERE escort_id = ' . $data['escort_id'] . ' AND id <> ' . $last->id);
			}
			
            if ($data['status'] == 0) {
                self::db()->update('escort_bubbletexts', array('status' => 0), self::db()->quoteInto('id = ?', $last->id));
				Cubix_SyncNotifier::notify($data['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_BUBBLE_TEXT, array(
					'old' => $last->text,
					'action' => 'removed'
				));
                return;
            }
			elseif ($data['status'] == 1) {
                if ($last && $data['text'] == $last->text) {
					if ( self::app()->id == APP_A6 || self::app()->id == APP_A6_AT ) {
						self::db()->update('escort_bubbletexts', array('status' => 1, 'date' => new Zend_Db_Expr('NOW()')), self::db()->quoteInto('id = ?', $last->id));
					}
					else {
						self::db()->update('escort_bubbletexts', array('status' => 1), self::db()->quoteInto('id = ?', $last->id));
					}
					Cubix_SyncNotifier::notify($data['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_BUBBLE_TEXT, array(
						'old' => $last->text,
						'action' => 'activated'
					));
                } else {
					self::db()->update('escort_bubbletexts', array('status' => 0), self::db()->quoteInto('escort_id = ?', $data['escort_id']));
					$data['approvation'] = $appovation;
                    self::db()->insert('escort_bubbletexts', $data);
					Cubix_SyncNotifier::notify($data['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_BUBBLE_TEXT, array(
						'old' => $last->text,
						'new' => $data['text'],
						'action' => 'changed'
					));
                }
            }
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

    public function getPhotosCount($escort_id) {


        $countSql = "
			SELECT COUNT(ep.id) as count
			FROM escort_photos ep
			WHERE ep.escort_id = ? AND ep.is_approved = 1
		";

        return self::db()->query($countSql, array($escort_id))->fetch();
    }

    public function getAllPhotosCount($escort_id) {


        $countSql = "
			SELECT COUNT(ep.id) as count
			FROM escort_photos ep
			WHERE ep.escort_id = ?
		";

        return self::db()->query($countSql, array($escort_id))->fetch();
    }

    public function addSlogan($slogan, $escort_id, $autoApprove) {
		
		try {
			$status = intval($autoApprove);
			$old_slogan = self::db()->fetchOne('SELECT text FROM escort_slogans WHERE escort_id = ?', $escort_id);
			
			if($slogan == $old_slogan){
				return true;
			}
		
			if ( strlen($slogan) ) {				
								
				if ($old_slogan)
				{
				    if ($old_slogan != $slogan){
                        $arr = array(
                            'text' => $slogan,
                            'status' => $status,
                            'date' => new Zend_Db_Expr('NOW()')
                        );

                        self::db()->update('escort_slogans', $arr, self::db()->quoteInto('escort_id = ?', $escort_id));
                        $log = '*** updated *** - ' . $slogan;
                        Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
                            'slogan' => $log
                        ));
                    }

				}
				else
				{
					$arr = array(
						'escort_id' => $escort_id,
						'status' => $status,
						'text' => $slogan
					);
					
					self::db()->insert('escort_slogans', $arr);
					$log = '*** inserted *** - ' . $slogan;
                    Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
                        'slogan' => $log
                    ));
				}
			}
			else
			{
			    if ($old_slogan){
                    self::db()->delete('escort_slogans', self::db()->quoteInto('escort_id = ?', $escort_id));

                    Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
                        'slogan' => '*** removed ***'
                    ));
                }

			}
		} catch (Exception $e) {
			$errors[$escort_id] = Cubix_Api_Error::Exception2Array($e);
		}

        return true;
    }

    public function getSlogan($escort_id) {
        

        $sql = "
			SELECT text, status
			FROM escort_slogans
			WHERE escort_id = ? 
			ORDER by date DESC
		";

        $slogan = self::db()->query($sql, array($escort_id))->fetch();

        return array('text' => $slogan->text, 'status' => $slogan->status);
    }

    public function getActivePackage($escort_id) {
        

        $sql = "
			SELECT op.period, op.date_activated, op.expiration_date, p.name
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN packages p ON p.id = op.package_id
			WHERE e.id = ? AND p.is_default = 0 AND op.status = 2
			ORDER BY op.date_activated DESC
		";

        $package = self::db()->query($sql, array($escort_id))->fetch();

        return $package;
    }
	
	public function getActivePackageWithZero($escort_id) {
        

        $sql = "
			SELECT op.period, op.date_activated, op.expiration_date, p.name
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN packages p ON p.id = op.package_id
			WHERE e.id = ? AND op.status = 2
			ORDER BY op.date_activated DESC
		";

        $package = self::db()->query($sql, array($escort_id))->fetch();

        return $package;
    }
	
	const PHONE_PACKAGE_1_DAY_PASS = 19;
	const PHONE_PACKAGE_3_DAY_PASS = 20;
	
	public function checkIfHasPackage($escort_id) {
		//Checking if escort has active, pending or suspended package
		//Returns these packages
		

		$sql = "
			SELECT op.period, op.date_activated, op.expiration_date, op.package_id, op.activation_type, op.status
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			WHERE e.id = ? AND (op.status = 2 OR op.status = 6 OR op.status = 1)
			ORDER BY op.date_activated DESC
		";
		
		$package = self::db()->query($sql, array($escort_id))->fetch();
		
		return $package;
	}
	
	public function hasProduct($escort_id, $product_id) 
	{
		$sql = '
			SELECT TRUE
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN order_package_products opp ON opp.order_package_id = op.id
			WHERE 
			e.id = ? AND ( e.status & ? OR e.status & ?) AND op.status = ? AND opp.product_id = ? 
		';
		
		return self::db()->fetchOne($sql, array(
			$escort_id,
			self::ESCORT_STATUS_ACTIVE,
			self::ESCORT_STATUS_OWNER_DISABLED,
			self::PACKAGE_STATUS_ACTIVE,
			$product_id
		));
		
	}
	
	//FOR GOTD PAYMENT ONLY
	public function checkIfHasActivePackage($escort_id) 
	{
		

		$sql = '
			SELECT 
				op.period, UNIX_TIMESTAMP(op.date_activated) AS date_activated, UNIX_TIMESTAMP(op.expiration_date) AS expiration_date,
				UNIX_TIMESTAMP(op.activation_date) AS activation_date, op.package_id, op.activation_type, op.status, e.status AS escort_status
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN orders o ON o.id = op.order_id
			WHERE 
				e.id = ? AND ( e.status = ? OR e.status = ? OR ( e.status & ? <> 0 AND e.status & ? <> 0 ) ) AND
				(
					op.status = ? OR op.status = ?
				) AND
				o.status IN (' . implode(',', array(self::ORDER_STATUS_PAID, self::ORDER_STATUS_PAYMENT_DETAILS_RECEIVED)) . ')
			ORDER BY op.date_activated DESC
		';
		
		$packages = self::db()->fetchAll($sql, array(
			$escort_id,
			self::ESCORT_STATUS_ACTIVE,
			self::ESCORT_STATUS_OWNER_DISABLED,
            self::ESCORT_STATUS_ACTIVE,
            self::ESCORT_STATUS_PROFILE_CHANGED,
			self::PACKAGE_STATUS_ACTIVE,
			self::PACKAGE_STATUS_PENDING,
		));
		
		if ( ! $packages ) return false;
		
		if ( count($packages) > 1 ) {
			//The case when user has active package and one pending package with activation type after_current_package_expires
			//packages[0] is the active package, packages[1] is the pending package
			//Merging two packages lifetime in one
			$package = $packages[0];
			if ( $packages[1]->activation_type == self::PACKAGE_ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES ) {
				$package->expiration_date = $package->expiration_date + 60 * 60 * 24 * ($packages[1]->period);
			} else if ( $packages[1]->activation_type == self::PACKAGE_ACTIVATE_AT_EXACT_DATE ) {
				$package->pending_package_activation_date = strtotime(date('Y-m-d', $packages[1]->activation_date));
				$package->pending_package_expiration_date = $packages[1]->activation_date + 60 * 60 * 24 * ($packages[1]->period - 1);
			}
            if(!in_array(Cubix_Application::getId(), array(APP_BL)))
            {
                $package->expiration_date -= 60 * 60 * 24;//Fixing expiration date
            }
			unset($package->activation_date);
			unset($package->period);
		} else {
			$package = $packages[0];
			
			if ( $package->activation_type == self::PACKAGE_ACTIVATE_AT_EXACT_DATE && ! $package->expiration_date ) {
				if ( $package->package_id != self::PHONE_PACKAGE_1_DAY_PASS && $package->package_id != self::PHONE_PACKAGE_3_DAY_PASS ) {
					$package->activation_date = strtotime(date('Y-m-d', $package->activation_date));
					$package->period -= 1;
				}
				$package->date_activated = $package->activation_date;
				$package->expiration_date = $package->activation_date + 60 * 60 * 24 * $package->period;
			} else {
                if(!in_array(Cubix_Application::getId(), array(APP_BL)))
                {
                    $package->expiration_date -= 60 * 60 * 24;//Fixing expiration date
                }
			}
			unset($package->activation_date);
			unset($package->period);
		}
		
		return $package;
	}

	
    public function existsByPhone($phone, $escort_id, $agency_id)
    {

	   $filters = '';
	   $filtersUnion = '';

	   if ($escort_id){
		   $filters .= " AND ep.escort_id <> ". $escort_id;
	   }
	   
	   if ($agency_id){
		   $agency_ids = array($agency_id);
		   include_once("Agencies.php");
		   $ag_model = new Cubix_Api_Module_Agencies();
		   $linked_agencies = $ag_model->getLinkedAgencies($agency_id);
		   if($linked_agencies){
				foreach($linked_agencies as $agency){
					$agency_ids[] = $agency->id;
				}
		   }
		   $filters .= " AND (( e.agency_id NOT IN ( ". implode( ',' , $agency_ids) .  ") OR e.agency_id IS NULL ))" ;
           $filtersUnion .= " AND a.id NOT IN ( ". implode( ',' , $agency_ids) .  ")" ;
	   } else {
		   //$filters .= " OR a.contact_phone_parsed = $phone";
	   }
		$sql ="
			SELECT agency_id, ep.escort_id FROM escort_profiles_v2 ep
            INNER JOIN escorts as e ON e.id = ep.escort_id
            INNER JOIN users u ON u.id = e.user_id
            LEFT JOIN agencies a ON a.user_id = u.id
			WHERE ep.phone_exists = ? {$filters} AND ep.phone_exists != \"\" AND ( NOT e.status & ?
			OR ( e.status & ? AND DATEDIFF( now(), e.deleted_date) < 60 ))
			GROUP BY e.agency_id
			UNION
	        (SELECT a.id as agency_id, NULL as escort_id
		     FROM agencies AS a
		     WHERE a.contact_phone_parsed = '{$phone}' {$filtersUnion}
	         )
		";

	   $result =  self::db()->fetchAll($sql,  array($phone, self::ESCORT_STATUS_DELETED,self::ESCORT_STATUS_DELETED));//
       return $result;

    }

	public function isBlacklistedPhone($phone)
	{
		if (is_array($phone)) reset($phone);
		
		return self::db()->fetchOne('SELECT TRUE FROM blacklisted_phones WHERE phone = ?', $phone);
	}
	
	public function isAutoApproval($escort_id)
    {       
       
       
       $res = self::db()->fetchOne('
			SELECT auto_approval FROM escorts WHERE id = ?
		',  array($escort_id));
	   
	   return $res;
    }


    public function getLocationCoordinates($escort_id){

        $res = self::db()->fetchAll('SELECT ct.title_en as default_title_en,ct.latitude as default_latitude,ct.longitude as default_longitude,ep.location_lat,ep.location_lng,ep.location_map_address FROM escort_profiles_v2 ep 
INNER JOIN escorts e ON ep.escort_id = e.id
INNER JOIN cities ct ON ct.id = e.city_id
WHERE ep.escort_id = ?
		',  array($escort_id));

        return $res[0];
    }

    public function getLngLat($escort_id)
    {
        try {
            $sql = 'SELECT longitude, latitude FROM `escorts` WHERE id = ?';
            $result = self::db()->fetchAll($sql, $escort_id);
            return $result[0];
        } catch (Exception $e) {
            return $e;
        }
    }

	public function getBlockCountries($escort_id){

	    if(in_array(Cubix_Application::getId(), array(APP_BL,APP_EI,APP_A6))){
            $countries_table = 'countries_all';
        }else{
            $countries_table = 'countries';
        }
		$result =  self::db()->fetchAll('
			SELECT  c.id as country_id,
				c.iso,
				c.slug,
				c.' . Cubix_I18n::getTblField('title') . ' as title
				FROM escort_blocked_countries as ebc
				INNER JOIN '.$countries_table.' c ON c.id = ebc.country_id
				WHERE ebc.escort_id = ?
		',  array($escort_id));


		return $result;
	}
	
	public function isPhotoAutoApproval($escort_id)
    {       
       
       
       $res = self::db()->fetchOne('
			SELECT photo_auto_approval FROM escorts WHERE id = ?
		',  array($escort_id));
	   
	   return $res;
    }

	public function getTotalViews( $escort_id ){
		
		return intval(self::db()->fetchOne('SELECT SUM(count) FROM escort_hits_daily WHERE escort_id = ? ', $escort_id));
	}

	public function updateSchedule( $escort_id, $data ){
		if (!is_array($data) || 0 == count($data))
            return false;

		
		$errors = array();
		try{
			self::db()->delete('schedule', self::db()->quoteInto('escort_id = ?', $escort_id));
			if ( count( $data ) > 0 ) {
				foreach( $data as $day => $type ) {
	//				strtotime($day);

					if ( $type['type'] > 0 ){
						$sch = array(
							'escort_id' => $escort_id,
							'type' => (int)$type['type'],
							'day' => $day
						);

						self::db()->insert('schedule', $sch);
					}
				}
			}
		}catch (Exception $e) {
			$errors[$escort_id] = Cubix_Api_Error::Exception2Array($e);
		}

		if (!count($errors)) {
            return true;
        }

        return $errors;

	}

	public function getSchedule ( $escort_id ){
		
		$sql = "SELECT * FROM schedule WHERE escort_id = $escort_id";
		$schedules = self::db()->fetchAll($sql);

		$res = array();

		if ( count( $schedules ) > 0 ){
			foreach ( $schedules as $schedule ){
				$res[$schedule->type][] = $schedule;
			}
		}

		return $res;
	}
	
	public function updateTravelCountries($escort_id, $data)
	{
		
		
		try {
			self::db()->delete('escort_travel_countries', self::db()->quoteInto('escort_id = ?', $escort_id));
			
			if ( count($data) > 0 ) {
				foreach ( $data as $d ) {
					if ( intval($d) != 0 ) {
						self::db()->insert('escort_travel_countries', array('escort_id' => $escort_id, 'country_id' => $d));
					}
				}
			}
			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
				'escort_id' => $escort_id,
				'country_id' => $d
			));
		
		} catch (Exception $e) {
			$errors[$escort_id] = Cubix_Api_Error::Exception2Array($e);
		}

		if ( !count($errors) ) {
            return true;
        }

        return $errors;
	}
	
	public function getTravelCountries($escort_id)
	{
		
		
		$sql = "SELECT country_id FROM escort_travel_countries WHERE escort_id = ?";
		
		$travel_countries = self::db()->fetchAll($sql, $escort_id);
		
		$tcc = array();
		if ( count($travel_countries) > 0 ) {
			foreach ( $travel_countries as $tc ) {
				$tcc[] = $tc->country_id;
			}
		}
		
		return $tcc;
	}
	
	public function getTravelContinents($escort_id)
	{
		
		
		$sql = "SELECT continent_id FROM escort_travel_continents WHERE escort_id = ?";
		
		$travel_continents = self::db()->fetchAll($sql, $escort_id);
		
		$tcc = array();
		if ( count($travel_continents) > 0 ) {
			foreach ( $travel_continents as $tc ) {
				$tcc[] = $tc->continent_id;
			}
		}
		
		return $tcc;
	}
	
	public function getTravelPlace($escort_id)
	{
		
		
		$sql = "SELECT travel_place FROM escort_profiles_v2 WHERE escort_id = ?";
		
		return self::db()->fetchOne($sql, $escort_id);
	}
	
	public function updateTravelPlace($escort_id, $travel_place, $sales_user_id = null)
	{
        if (!empty($sales_user_id) && intval($sales_user_id) > 0){
            $sales_user_id = intval($sales_user_id);
        }else{
            $sales_user_id = null;
        }
				
		try {
			self::db()->update('escort_profiles_v2', array('travel_place' => $travel_place), self::db()->quoteInto('escort_id = ?', $escort_id));
			self::db()->delete('escort_travel_continents', self::db()->quoteInto('escort_id = ?', $escort_id));
									
			switch ($travel_place)
			{
				case WORLDWIDE:
					$continents = array(1, 2, 3, 4, 5, 6);
					break;
				case USA_AND_CANADA:
					$continents = array(5, 6);
					break;
				case USA_ONLY:
				default:
					$continents = array(5);
					break;
			}
			
			foreach ($continents as $c)
				self::db()->insert('escort_travel_continents', array('escort_id' => $escort_id, 'continent_id' => $c));
			
			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
				'escort_id' => $escort_id,
                'escort_travel_continents'=>$continents,
                'travel_place' => $travel_place
			), $sales_user_id);
		
		} catch (Exception $e) {
			$errors[$escort_id] = Cubix_Api_Error::Exception2Array($e);
		}
	}
	
	public function switchAgency($escort_id, $new_agency_id) 
	{
		include_once("Agencies.php");
		//CHECKING IF WRITE AGENCY GIVEN
		$escort = self::getById($escort_id);
		$ag_model = new Cubix_Api_Module_Agencies();
		$linked_agencies = $ag_model->getLinkedAgencies($escort->agency_id);
		
		$is_found = false;
		foreach($linked_agencies as $l_ag) {
			if ( $l_ag->id == $new_agency_id ) $is_found = true;
		}
		
		if ( ! $is_found ) return;
		
		//CHANGING ESCORT AGENCY_ID
		try {
			self::db()->update('escorts', array('agency_id' => $new_agency_id), self::db()->quoteInto('id = ?', $escort_id));

			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array(
				'escort_id' => $escort_id,
				'agency_id' => $new_agency_id
			));
			
			return true;
		} catch (Exception $e) {
			$errors[$escort_id] = Cubix_Api_Error::Exception2Array($e);
		}
	}
	
	
	public function getPhotos($data)
	{
		$page = $data['page'];
		$per_page = $data['per_page'];
		$with_main = $data['with_main'];
		$escort_id = $data['escort_id'];
		$portrait_first = $data['portrait_first'];
		$only_normal = $data['only_normal'];
		$gallery_id = $data['gallery_id'];
		$sedcard = isset($data['sedcard']) ? $data['sedcard'] : false;
		$disabled = isset($data['disabled_status']) ? $data['disabled_status'] : false;
		$where = '';
		$APP_ID = self::app()->id;
		if ( ! $with_main ) {
			$where .= ' AND ep.is_main = 0 ';
		}
		
		// Only normal photos
		if ( $only_normal ) {
			$where .= ' AND ep.type <> 3 AND ep.type <> 5';
		}

		// Without disabled photos
		if ( $disabled ) {
			$where .= ' AND ep.type <> 4 ';
		}
		
		if ( isset($gallery_id) ) {
			$where .= ' AND ep.gallery_id = '.$gallery_id ;
		}
		
		if ( ! is_null($page) && ($page = intval($page)) < 1 ) $page = 1;
		$limit = '';
		if ( ! is_null($page) ) {
			$limit = 'LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
		}
		
		$ordering = " ORDER BY ep.is_main DESC, ep.ordering ASC, ep.id ASC ";
		
		if ( $portrait_first ) {
			$ordering = " ORDER BY ep.is_portrait DESC, ep.is_main DESC, ep.ordering ASC, ep.id ASC ";
		}
        if ($sedcard && in_array($APP_ID, array(APP_A6, APP_EG_CO_UK, APP_ED, APP_BL, APP_6B, APP_AE,))){
            $ordering = " ORDER BY ep.ordering ASC, ep.id ASC ";
        }
		$photos = self::db()->fetchAll("
			SELECT SQL_CALC_FOUND_ROWS $APP_ID AS application_id, ep.* FROM escort_photos ep
			WHERE ep.escort_id = ? {$where} {$ordering}
			{$limit}
		", array($escort_id));

		$countSql = "SELECT FOUND_ROWS();";
		$count = self::db()->fetchOne($countSql);
		
		return array('list' => $photos, 'count' => $count);
	}

	public function AdditionalCities($esort_id,$base_city_id){

        return self::db()->query('
			SELECT ct.' . Cubix_I18n::getTblField('title') . ' AS city_title, ct.id AS city_id, c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, ct.slug AS city_slug FROM cities ct
			INNER JOIN escort_cities ec ON ec.city_id = ct.id
			INNER JOIN countries c ON c.id = ct.country_id
			LEFT JOIN regions r ON r.id = ct.region_id
			WHERE ec.escort_id = ? AND ec.city_id <> ?
		', array($esort_id, $base_city_id))->fetchAll();
    }

	public function getPhotoById($id){
        $photo = self::db()->fetchRow('
			SELECT * FROM escort_photos WHERE id = ?
		', $id);
        return $photo;
    }
	
	public static function setAdminVerifiedStatus($esort_id, $status)
	{
		if ( self::db()->fetchOne('SELECT TRUE FROM escort_profiles_v2 WHERE escort_id = ?', $esort_id) ) {
			self::db()->update('escort_profiles_v2', array('admin_verified'=> $status), self::db()->quoteInto('escort_id = ?', $esort_id));
		}
		else{
			self::db()->insert('escort_profiles_v2', array('escort_id'=> $esort_id, 'admin_verified'=> $status));
		}
	
	}
	
	public function getPackagesCount($escort_id)
	{
		return self::db()->fetchOne('SELECT	COUNT(op.id) AS count FROM order_packages op WHERE op.escort_id = ? AND op.order_id IS NOT NULL', $escort_id);
	}

	public function getAgencyPackagesCount($user_id)
	{
		$escorts = self::db()->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetchAll();
		
		if ($escorts)
		{
			$arr = array();
			
			foreach ($escorts as $escort)
				$arr[] = $escort->id;
			
			return self::db()->fetchOne('SELECT	COUNT(op.id) AS count FROM order_packages op WHERE op.escort_id IN (' . implode(',', $arr) . ') AND op.order_id IS NOT NULL');
		}
		else
			return 0;
	}
	
	public function setPhotoRotateType($escort_id, $type)
	{	
		
		self::db()->update('escorts', array('photo_rotate_type' => $type), self::db()->quoteInto('id = ?', $escort_id));
				
	}
	
	public function setRotatePhotos($escort_id, $photo_ids)
	{	
		
		$where = '';
		if(count($photo_ids) > 0 ){
			$where = " AND id IN (".implode(',' ,$photo_ids).") ";  
		}
		
		self::db()->update('escort_photos', array('is_rotatable' => 0), self::db()->quoteInto('escort_id = ?', $escort_id));
		self::db()->query('UPDATE escort_photos SET is_rotatable = 1 WHERE escort_id = ? ' .$where, array($escort_id));
		
		return true;
	}

	public function hasPaidActivePackageForEscort($user_id)
	{
		$escort = self::db()->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetch();
		
		if ($escort)
		{			
			return self::db()->fetchOne('SELECT	TRUE FROM order_packages WHERE escort_id = ? AND order_id IS NOT NULL AND status = ?', array($escort->id, 2));
		}
		else
			return FALSE;
	}

	public function girlForumAccess($user_id)
	{
		$escort = self::db()->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetch();
		
		if ($escort)
		{			
			return self::db()->fetchOne('SELECT	TRUE FROM order_packages WHERE escort_id = ? AND order_id IS NOT NULL LIMIT 1', array($escort->id));
		}
		else
			return FALSE;
	}
	
	public function hasPaidActivePackageForAgency($user_id)
	{
		$escorts = self::db()->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetchAll();
		
		if ($escorts)
		{
			$arr = array();
			
			foreach ($escorts as $escort)
				$arr[] = $escort->id;
			
			return self::db()->fetchOne('SELECT	TRUE FROM order_packages WHERE escort_id IN (' . implode(',', $arr) . ') AND order_id IS NOT NULL AND status = ?', 2);
		}
		else
			return FALSE;
	}
	
	public function hasPaidActivePackageByEscortId($escort_id)
	{
		
		return self::db()->fetchOne('SELECT	TRUE FROM order_packages WHERE escort_id = ? AND order_id IS NOT NULL AND status = ?', array($escort_id, 2));
	}
			
	public function addLatestAction($data)
	{
		try {
			self::db()->insert('latest_actions', $data);

			return true;

		} catch( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function addLatestActionDetail($data)
	{
		try {
			self::db()->insert('latest_actions_details', $data);

			return true;

		} catch( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function getUrgentMessageForEscort($escort_id)
	{
		return self::db()->fetchOne('SELECT urgent_message FROM escorts WHERE id = ?', $escort_id);
	}
	
	public function getUrgentMessageForAgency($user_id)
	{
		return self::db()->query('SELECT showname, urgent_message FROM escorts WHERE user_id = ? AND LENGTH(urgent_message) > 0', $user_id)->fetchAll();
	}
	
	public function setUrgentMessageForEscort($escort_id)
	{
		self::db()->update('escorts', array('urgent_message' => NULL), self::db()->quoteInto('id = ?', $escort_id));
	}
	
	public function setUrgentMessageForAgency($user_id)
	{
		self::db()->update('escorts', array('urgent_message' => NULL), self::db()->quoteInto('user_id = ?', $user_id));
	}
	
	public function getRejectedVerificationForEscort($escort_id)
	{
		$r = self::db()->query('SELECT reason_ids, reason_text, status, is_read, is_timout FROM verify_requests WHERE escort_id = ? ORDER BY id DESC LIMIT 1', $escort_id)->fetch();
		
		if ($r && $r->status == 4 && $r->is_read == 0 && strlen($r->reason_ids))
		{
			return array('reason_ids' => $r->reason_ids, 'reason_text' => $r->reason_text);
		}
		elseif($r && $r->status == 2 && $r->is_timout == 1 && $r->is_read == 0 )
		{
			return array('is_timeout' => 1);
		}
		elseif($r && $r->status == 2 && $r->is_read == 0 && self::app()->id == APP_6B )
		{
			return array('is_verified' => 1);
		}
		elseif($r && $r->status == 7  && $r->is_read == 0 )
		{
			return array('is_blur_verify' => 1);
		}	
		return NULL;
	}

    public function getRejectedCertificationForEscort($escort_id)
    {
        //return '';
        return self::db()->query('SELECT reject_reason FROM age_verifications WHERE escort_id = ? ORDER BY id DESC LIMIT 1', $escort_id)->fetch();
    }
	
	public function getRejectedVerificationForAgency($user_id)
	{
		$escorts = self::db()->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetchAll();
		
		if ($escorts)
		{
			$e_ids = array();
			
			foreach ($escorts as $e)
			{
				$e_ids[] = $e->id;
			}
			
			$rs = self::db()->query('
				SELECT v.id,v.escort_id, v.reason_ids, v.reason_text, v.status, v.is_read, e.showname, v.is_timout
				FROM verify_requests v 
				INNER JOIN escorts e ON e.id = v.escort_id 
				WHERE v.escort_id IN (' . implode(',', $e_ids) . ') AND v.id = (SELECT MAX(id) FROM verify_requests WHERE escort_id = v.escort_id)
			')->fetchAll();
			
			if ($rs)
			{
				$arr = array();
				
				foreach ($rs as $r)
				{
					
					if ($r->status == 4 && $r->is_read == 0 && strlen($r->reason_ids))
					{
						$arr[$r->showname] = array('reason_ids' => $r->reason_ids, 'reason_text' => $r->reason_text);
					}
					elseif($r->status == 2 && $r->is_timout == 1 && $r->is_read == 0 )
					{
						$arr[$r->showname] = array('is_timeout' => 1);
					}
					elseif($r->status == 2 && $r->is_read == 0 && self::app()->id == APP_6B )
					{
						$arr[$r->showname] = array('is_verified' => 1);
					}
					elseif($r && $r->status == 7  && $r->is_read == 0 )
					{
						$arr[$r->showname] = array('is_blur_verify' => 1, 'escort_id' => $r->escort_id);
					}	
				}
				
				if (count($arr) > 0)
					return $arr;
			}
		}
		
		return NULL;
	}
	
	public function setRejectedVerificationForEscort($escort_id)
	{
		self::db()->update('verify_requests', array('is_read' => 1), self::db()->quoteInto('escort_id = ?', $escort_id));
	}
	
	public function setRejectedVerificationForAgency($user_id)
	{
		$escorts = self::db()->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetchAll();
		
		if ($escorts)
		{
			$e_ids = array();
			
			foreach ($escorts as $e)
			{
				$e_ids[] = $e->id;
			}
			
			self::db()->query('UPDATE verify_requests SET is_read = 1 WHERE escort_id IN (' . implode(',', $e_ids) . ')');
		}
	}
	
	public function getTourDetails($escort_id)
	{
		$where = " DATE(lad.date) = DATE(NOW()) ";
		if ( self::app()->id == APP_ED ) {
			$where = " DATE(lad.date) >= DATE( DATE_SUB( NOW() , INTERVAL 2 DAY ) ) ";
		}

		$sql = "
			SELECT 
				t.date_from, t.date_to, t.phone, c.title_en as city_title, e.showname
			FROM latest_actions_details lad
			INNER JOIN tours t ON t.id = lad.entity_id
			INNER JOIN cities c ON c.id = t.city_id
			INNER JOIN escorts e ON e.id = lad.escort_id
			WHERE lad.action_type = 3 AND lad.escort_id = ? AND DATE(t.date_to) >= DATE(NOW()) AND " . $where . "
			GROUP BY t.id
		";
		return self::db()->fetchAll($sql, array($escort_id));
	}
	
	public function getLatestActionProfileDetails($escort_id)
	{
		
		
		$new_rev = self::db()->fetchOne('SELECT MAX(revision) FROM profile_updates_v2 WHERE escort_id = ? AND status = 2', array($escort_id));
		$old_rev = self::db()->fetchOne('SELECT MAX(revision) FROM profile_updates_v2 WHERE escort_id = ? AND status = 2 AND revision < ?', array($escort_id, $new_rev));
		
		if ( ! $old_rev ) {
			$old_rev = self::db()->fetchOne('SELECT MAX(revision) FROM profile_updates_v2 WHERE escort_id = ? AND status = 2 AND revision = ?', array($escort_id, $new_rev));
		}
		
		$new = unserialize(self::db()->fetchOne('SELECT data FROM profile_updates_v2 WHERE escort_id = ? AND revision = ?', array($escort_id, $new_rev)));
		
		$old = array();
		if ( $old_rev ) {
			$old = unserialize(self::db()->fetchOne('SELECT data FROM profile_updates_v2 WHERE escort_id = ? AND revision = ?', array($escort_id, $old_rev)));
		}
		
		return array('new' => $new, 'old' => $old);
	}
	
	public function getWebsite($escort_id)
	{
		
		
		return self::db()->fetchOne('SELECT website FROM escort_profiles_v2 WHERE escort_id = ? ', array($escort_id));
		
	}
	
	public function getWebPhone($escort_id)
	{
		
		
		return self::db()->fetchRow('SELECT website, phone_exists as contact_phone_parsed FROM escort_profiles_v2 WHERE escort_id = ? ', array($escort_id));
		
	}
	
	public function getCurrentShowname($escort_id)
	{
		
		
		return self::db()->fetchOne('SELECT showname FROM escorts WHERE id = ? ', array($escort_id));
		
	}
	
	public function getEscortPhotosList($escort_id, $only_privates)
	{
		
		$where = '';
		$ordering = 'ORDER BY ep.ordering ASC ';
		if ( self::app()->id == APP_A6 ) {
			$where = ' AND ep.type <> 5 ';
            $ordering .= ', ep.id ASC ';
		}


		if ( $only_privates ) {
			$where .= ' AND ep.type = 3';
		}
		else {
			$where .= ' AND ep.type <> 3';
		}

		$photos = self::db()->fetchAll("
			SELECT " . self::app()->id . " AS application_id, ep.* FROM escort_photos ep
			WHERE ep.escort_id = ? {$where} {$ordering}
		", $escort_id);

		return $photos;
	}
	
	public function getEscortArchivedPhotosList($escort_id)
	{
		

		$where .= ' AND ep.type = 5';

		$photos = self::db()->fetchAll("
			SELECT " . self::app()->id . " AS application_id, ep.* FROM escort_photos ep
			WHERE ep.escort_id = ? {$where} ORDER BY ep.ordering ASC
		", $escort_id);

		

		return $photos;
	}

	public function insertPhoneConfirm($escort_id, $agency_id = 0, $old_number, $new_number, $new_number_parsed, $code, $prefix)
	{	
		$escort_id = intval($escort_id);
		$agency_id = intval($agency_id);


		$row_data = array(
			'escort_id' => $escort_id,
			'agency_id' => $agency_id,
			'old_phone' => $old_number,
			'new_phone' => $new_number,
			'new_phone_parsed' => $new_number_parsed,
			'country_code' => $prefix,
			'code' => $code,
			'status' => 0
		);
		
		try {			

			$exists = self::db()->fetchOne('SELECT TRUE FROM confirm_phone_number WHERE escort_id = ? AND agency_id = ? ', array($escort_id, $agency_id));
			if ( $exists ) {
				$row_data['sms_send_count'] = new Zend_Db_Expr("sms_send_count + 1");
				return self::db()->update('confirm_phone_number',$row_data, array(
                            self::db()->quoteInto('escort_id = ?', $escort_id),
                            self::db()->quoteInto('agency_id = ?', $agency_id)
                        ));
			} else {
				$row_data['sms_send_count'] = 1;
				self::db()->insert('confirm_phone_number',$row_data);
			}
		
		} catch( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return 1;
	}
	
	public function getPhoneConfirmStatus($escort_id, $agency_id, $new_phone)
	{	
		
		return  self::db()->fetchOne("SELECT `status` FROM confirm_phone_number WHERE escort_id = ? AND agency_id = ? AND  new_phone_parsed = ?", array( $escort_id, $agency_id, $new_phone));
	}
	
	public function escortHasConfirmedPhone($escort_id)
	{	
		return  self::db()->fetchOne("SELECT `status` FROM confirm_phone_number WHERE escort_id = ? AND status = ?", array($escort_id,1));
	}

	public function setPhoneConfirmStatus($escort_id, $agency_id = 0, $status)
	{	
		return self::db()->update('confirm_phone_number', array('status' => $status), array(
								self::db()->quoteInto('escort_id = ?', $escort_id),
								self::db()->quoteInto('agency_id = ?', $agency_id))
						);
	}

	public function getPhoneConfirmStatusByNumber($escort_id, $new_phone)
	{		
	return  self::db()->fetchOne("SELECT `status` FROM confirm_phone_number WHERE escort_id = ? AND  new_phone = ?", array( $escort_id, $new_phone));
	}
	public function hasConfirmedPhone($escort_id)
	{		
	return  self::db()->fetchOne("SELECT `status` FROM confirm_phone_number WHERE escort_id = ? AND  status = 1", array( $escort_id));
	}
	public function checkConfirmStatusOpt($escort_id, $agency_id, $phone)
	{	
		
		try{
			$status = self::db()->fetchOne("SELECT `status` FROM confirm_phone_number WHERE escort_id = ? AND agency_id = ? AND  new_phone_parsed != ? ", array( $escort_id, $agency_id, $phone));
			if($status == 1){
				self::db()->update('confirm_phone_number', array('status' => 0), array(
								self::db()->quoteInto('escort_id = ?', $escort_id),
								self::db()->quoteInto('agency_id = ?', $agency_id))
						);
			}
		} catch( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function isSmsVerified($escort_id)
	{	
		
		return  self::db()->fetchOne("SELECT phone_sms_verified FROM escort_profiles_v2 WHERE escort_id = ? ", array($escort_id));
	}
	
	public  function checkConfirmCode($escort_id,$agency_id,$code)
	{	
		$escort_id = intval($escort_id);
		$agency_id = intval($agency_id);
		$code =  intval($code);
		$data =  self::db()->fetchRow('SELECT old_phone, new_phone, new_phone_parsed, code ,country_code FROM confirm_phone_number WHERE escort_id = ? AND agency_id = ? AND code = ?  ',array($escort_id,$agency_id,$code));
		$result = 0;
		try {
			if(!empty($data))
			{	
				if($escort_id){
					Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHONE_CHANGED, array('code'=>$data->code,'before'=>$data->old_phone,'after'=>$data->new_phone_parsed));
				}
				$result = self::db()->query("UPDATE confirm_phone_number SET `status`='1',confirm_date = NOW(), confirm_count = confirm_count+1 WHERE escort_id = ? AND agency_id = ? AND code = ?",array($escort_id,$agency_id,$code))->rowCount();
			}
		}
		catch (Exception $e)
		{	
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		return array('success'=>$result,'data'=>$data);
	}
	
	public function setRetocuh($escort_id,$photo_ids)
	{
		
			try {
			$result =  self::db()->query("UPDATE escort_photos SET retouch_status =1 WHERE id IN($photo_ids) AND escort_id=$escort_id ")->rowCount();
			if($result>0)
			  Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array(
				'id' => $photo_ids,
				'action' => 'send retouch'
			));
			
			return $result;

		} catch( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function getSameClientIDEscorts($client_id, $user_id)
	{
		if (is_array($client_id)) $client_id = reset($client_id);
		if (is_array($user_id)) $user_id = reset($user_id);
		
		return self::db()->query('
			SELECT e.id, e.showname 
			FROM client_cookie c 
			INNER JOIN users u ON c.user_id = u.id AND u.user_type = "escort" 
			INNER JOIN escorts e ON u.id = e.user_id AND e.is_suspicious = 2 
			WHERE c.user_id <> ? AND c.client_id = ?
		', array($user_id, $client_id))->fetchAll();
	}
	
	
	
	public function getWinCode($escort_id)
	{	
		
		return self::db()->fetchOne('SELECT win_code FROM xmas_calendar WHERE date = ? AND win_id  = ?'
			, array(date("Y-m-d"),$escort_id));
	}
	
	public function getWinCodeByUserId($user_id)
	{	
		
		return self::db()->fetchOne('SELECT xc.win_code, xc.win_id FROM xmas_calendar xc 
							INNER join escorts e ON xc.win_id = e.id
							WHERE  xc.checked = 1  AND xc.date = ? AND e.user_id = ?'
			, array(date("Y-m-d"),$user_id));
	}

	public function checkSmsOnly($escort_id)
	{
		if (is_array($escort_id)) $escort_id = reset($escort_id);

		return self::db()->fetchOne('
			SELECT TRUE
			FROM escort_profiles_v2 ep
			INNER JOIN order_packages op ON ep.escort_id = op.escort_id AND op.status = 2
			WHERE ep.escort_id = ? AND ep.phone_instr = 2 AND op.package_id NOT IN (100, 101, 102, 103)
		', $escort_id);
	}

	public function checkSmsOnlyForAgency($user_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);

		$escorts = self::db()->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetchAll();

		if ($escorts)
		{
			$e_ids = array();

			foreach ($escorts as $e)
			{
				$e_ids[] = $e->id;
			}

			$r = self::db()->query('
				SELECT ep.escort_id, ep.showname
				FROM escort_profiles_v2 ep
				INNER JOIN order_packages op ON ep.escort_id = op.escort_id AND op.status = 2
				WHERE ep.escort_id IN (' . implode(',', $e_ids) . ') AND ep.phone_instr = 2 AND op.package_id NOT IN (100, 101, 102, 103)
			')->fetchAll();

			return $r;
		}

		return NULL;
	}
	
	public function checkVerificationTimeLimit($escort_id)
	{
		$escort_id = intval($escort_id);
		
		$sql = '
			SELECT UNIX_TIMESTAMP(MAX(vr.date)) as date FROM verify_requests vr
			INNER JOIN escorts e ON e.id = vr.escort_id 
			WHERE e.verified_status = 2 AND e.stays_verified = 0 AND vr.status = 2 AND vr.escort_id = ?
			GROUP BY vr.escort_id	
			HAVING DATE_ADD(DATE_SUB(now(), INTERVAL 6 MONTH), INTERVAL 14 DAY) > MAX(vr.date)
		';
		
		$result = self::db()->fetchOne($sql, array($escort_id));
		return $result;
	}		

	/*only for and6.ch */
	public function getSendOnlineNowSms($escort_id)
	{
		$escort_id = intval($escort_id);

		return self::db()->fetchOne('SELECT send_online_now_sms FROM escorts WHERE id = ?', array($escort_id));
	}

	/*only for and6.ch */
	public function setSendOnlineNowSms($escort_id, $status)
	{
		$escort_id = intval($escort_id);
		$status = intval($status);

		self::db()->query('UPDATE escorts SET send_online_now_sms = ? WHERE id = ?', array($status, $escort_id));
	}

	public function getField($escort_id, $field) {
        $escort_id = intval($escort_id);        

        return self::db()->fetchOne('SELECT ' . $field . ' FROM escorts WHERE id = ?', $escort_id);
    }

    public function updateField($escort_id, $field, $value) {
        $escort_id = intval($escort_id);        

        self::db()->update('escorts', array($field => $value),  self::db()->quoteInto('id = ?', $escort_id));
    }
	
	public function checkAgeCertification($escort_id)
	{
		$escort_id = intval($escort_id);        
		
		$sql = " SELECT e.showname, u.email FROM escorts e
				INNER JOIN users u ON u.id = e.user_id
				WHERE e.id = ? AND e.need_age_verification = 0 ";
		
		$escort_data = self::db()->fetchRow($sql, $escort_id);
		
		if($escort_data){
			
			$update_data = array(
				'need_age_verification' => self::AGE_CERIFICATION_REQUEST_SENT,
				'age_verify_type' => self::AGE_CERIFICATION_REQUEST_TYPE_ONLINE,
				'age_verify_date' => new Zend_Db_Expr('NOW()'),
				'age_verify_disabled' => 1
			);
			
			self::db()->update('escorts', $update_data , self::db()->quoteInto('id = ?', $escort_id));
			$status = new Cubix_EscortStatus($escort_id);
			$status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_NEED_AGE_VERIFICATION);
			$status->save();
			
			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'age cerification automatic '));
			return $escort_data;
			
		}
		else{
			return false;
		}
		
	}

	public function getUserByPhone( $userType, $contact_phone_parsed ){
		$user = false;
		if($userType == 'escort'){
			$user = self::db()->fetchRow('SELECT e.id as escort_id, e.user_id, u.username, u.user_type, u.email, ep.contact_phone_parsed as phone
				  FROM escorts as e
				  LEFT JOIN users u ON u.id = e.user_id
				  LEFT JOIN escort_profiles_v2 ep ON e.id = ep.escort_id 
				  WHERE ep.contact_phone_parsed = ? AND u.user_type = "escort" AND u.status = '.STATUS_ACTIVE, 
				  $contact_phone_parsed);
		}
		if($userType == 'agency'){
			$user = self::db()->fetchRow('SELECT ag.user_id, u.username, u.user_type, u.email, ag.contact_phone_parsed as phone
				  FROM agencies as ag
				  LEFT JOIN users u ON u.id = ag.user_id
				  WHERE ag.contact_phone_parsed = ? AND u.user_type = "agency" AND u.status = '.STATUS_ACTIVE, 
				  $contact_phone_parsed);

		}
		return $user;
	}

	public function saveActivationCode($code,$activationAvailableTime,$user_id){
		$sql = 'UPDATE users SET activation_key = '.$code.', activation_available = '.$activationAvailableTime.' WHERE id = ?';
		
		if(self::db()->query($sql, $user_id)){
			return true;
		}

		return false;
	}

	public function checkActivationCode($username,$code){
		$user = self::db()->fetchRow('SELECT id, activation_key, activation_available
				  FROM users
				  WHERE username = ? AND status = '.STATUS_ACTIVE, 
				  $username);

		$return = array();
		$return['status'] = 0;

		if(!$user){
			$return['error_code'] = "user_not_exist";
			return $return;
		}
		if($user->activation_key != $code){
			$return['error_code'] = "wrong_code";
			return $return;
		}

		if(time() > $user->activation_available){
			$return['error_code'] = "code_expired";
			return $return;	
		}


		$return['user_id'] = $user->id;
		$return['status'] = 1;
		return $return; 
	}

	public function forceChangePassword($username,$code,$newPass){
		$checkCode = self::checkActivationCode($username,$code);

		if($checkCode['status'] == 0){
			return false;
		}else{
			$salt_hash = Cubix_Salt::generateSalt($newPass);

			$pass = Cubix_Salt::hashPassword($newPass, $salt_hash);
			
			try {
				self::db()->update('users', array('password' => $pass, 'salt_hash' => $salt_hash, 'activation_key' => '', 'activation_available' => ''), self::db()->quoteInto('id = ?', $checkCode['user_id']));
				self::db()->update('reseted_password', array('activation' => 1, 'activation_date' => date('Y-m-d H:i:s')), self::db()->quoteInto('code = ?', $code), self::db()->quoteInto('user_id = ?',  $checkCode['user_id']));
			}
			catch ( Exception $e ) {
				return array('error' => Cubix_Api_Error::Exception2Array($e));
			}
			return true;	
		}
	}

	public function getEmailCollectingEscorts(){
		$sql = '
			SELECT
				ecp.id, ecp.escort_id, ecp.photo_id, e.showname, e.package_expiration_date  
			FROM email_collecting_photos ecp
			INNER JOIN escorts e ON e.id = ecp.escort_id AND e.active_package_id IS NOT NULL 
			INNER JOIN escort_photos ep on ep.id = ecp.photo_id
			LEFT join order_packages op on  op.escort_id = e.id 
			where (e.status & 32)
			GROUP BY e.id
			ORDER BY op.`status`, rand()
			limit 5
		';

		return self::db()->fetchAll($sql);
	}
	
	public function saveGeoInfo($escort_id, $ip, $country, $country_iso, $city)
	{
		$data = array(
			'escort_id' => $escort_id,
			'country' => $country,
			'country_iso' => $country_iso,
			'city' => $city,
			'ip' => $ip
		);
		self::db()->insert('escorts_geo_data', $data);
	}
	
	public function getGeoInfo($filter)
	{
		$where = "";
		if( $filter['last_days'] && isset($filter['last_days']) ){
			$where = " AND DATE(egd.date) > DATE_ADD(NOW(), INTERVAL -".$filter['last_days']." DAY)";
		}
		
		$sql = '
			SELECT
				egd.country, egd.country_iso, egd.ip, UNIX_TIMESTAMP(egd.date) as register_date , u.email, e.showname, e.gender, ep.sex_orientation 
			FROM escorts_geo_data egd
			INNER JOIN escorts e ON e.id = egd.escort_id
			INNER JOIN escort_profiles_v2 ep on ep.escort_id = egd.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE egd.country_iso IN ("us","ca","gb","au","it","fr","de")'. $where .'
			ORDER BY egd.id
		';
	
		$escorts = self::db()->fetchAll($sql);
		foreach($escorts as $escort){
			
			$escort->source = "escortdirectory.com";
			
			switch($escort->gender){
				case GENDER_FEMALE:
					$escort->gender = "female";
					BREAK;
				case GENDER_MALE:
					$escort->gender = "male";
					BREAK;
				case GENDER_TRANS:
					$escort->gender = "trans";
					BREAK;
				default :
					$escort->gender = null;
			}
			
			switch($escort->sex_orientation){
				case ORIENTATION_HETEROSEXUAL:
					$escort->sex_orientation = "heterosexual";
					BREAK;
				case ORIENTATION_BISEXUAL:
					$escort->sex_orientation = "bisexual";
					BREAK;
				case ORIENTATION_HOMOSEXUAL:
					$escort->sex_orientation = "homosexual";
					BREAK;
				default :
					$escort->sex_orientation = null;
			}
		}
		
		return $escorts;
	}

	public function insertStatisticsRow($id,$user_type,$link){
        $link = str_replace('https://','',$link);
        $link = str_replace('http://','',$link);

        try {
            $data = array(
                'link' => (strpos($link, 'www.') !== false) ? $link : 'www.'.$link,
                'escort_id' => ($user_type == 'escort') ? $id : null,
                'agency_id' => ($user_type == 'agency') ? $id : null,
                'user_type' => $user_type,
                'date' => new Zend_Db_Expr('NOW()')
            );
            self::db()->insert('website_statistics', $data);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getCityCountryInfo($escort_id)
    {
      return self::db()->fetchAll('SELECT c.id, co.iso, c.title_' . Cubix_I18n::getLang() . ' AS title
			FROM escorts e
			INNER JOIN cities c ON c.id = e.city_id
			INNER JOIN countries co ON co.id = c.country_id
			WHERE e.id = ?', array($escort_id));
    }

    public function hasVideo($escort_id)
    {
        return self::db()->fetchOne('SELECT true from `video` where escort_id = ? and status = "approve" ', array($escort_id));
    }

    public function voteForVideo($video_id, $pre_video_id)
    {
		try {
			if ($pre_video_id) {
				self::db()->update('video', 
					array('vote_count' => new Zend_Db_Expr('vote_count - 1')),
					self::db()->quoteInto('id = ?', $pre_video_id)
				);
			}
	
			return self::db()->update('video',
				array('vote_count' => new Zend_Db_Expr('vote_count + 1')),
				self::db()->quoteInto('id = ?', $video_id)
			);
		} catch (Exception $e) {
			return $e->getMessage();
		}
    }

    public function updateCoordinates($coord, $id)
    {
    	return self::db()->update('escorts', 
					$coord,
					self::db()->quoteInto('id = ?', $id)
				);
    }

    public function getEscortsByCityOrHomeCityId($city_id)
    {
        try {
            $sql = "SELECT id, city_id, home_city_id
                    FROM escorts 
                    WHERE city_id = ? or home_city_id = ?";
            return self::db()->fetchAll($sql, array($city_id, $city_id));
        } catch (Exception $e) {
            return $e;
        }
    }

    public function getEscortCitiesByCityId($city_id)
    {
        try {
            $sql = "SELECT *
                    FROM escort_cities
                    WHERE city_id = ?";
            return self::db()->fetchAll($sql, array($city_id));
        } catch (Exception $e) {
            return $e;
        }
    }

    public function getEscortIdsByCityId($city_id)
    {
        try {
            $sql = "SELECT GROUP_CONCAT( escort_id ) as escort_ids
                    FROM (
                        SELECT id as escort_id FROM escorts WHERE home_city_id = ? OR city_id = ?
                        UNION DISTINCT 
                        SELECT escort_id FROM escort_cities WHERE city_id = ?
                    ) as escort_ids";

            return self::db()->fetchOne($sql, array($city_id, $city_id, $city_id));
        } catch (Exception $e) {
            return $e;
        }
    }

    public function getLastUpdatedProfilesByEscortIds($escort_ids)
    {
        try {
            if (is_array($escort_ids)) {
                $escort_ids = implode(', ', $escort_ids);
            }

            $sql = 'SELECT p1.* 
                    FROM profile_updates_v2 AS p1
                    INNER JOIN (
                        SELECT max(id) lastUpdatedProfile, escort_id
                        FROM profile_updates_v2
                        GROUP BY escort_id
                    ) p2
                      ON p1.escort_id = p2.escort_id
                        AND p1.id = lastUpdatedProfile
                    WHERE p1.escort_id IN  ('. $escort_ids .')
                    ORDER BY p1.id DESC';

            return self::db()->fetchAll($sql);
        } catch (Exception $e) {
            return $e;
        }
    }

    public function getProfileSnapshotsByEscortIds($escort_ids)
    {
        try {
            if (is_array($escort_ids)) {
                $escort_ids = implode(', ', $escort_ids);
            }

            $sql = 'SELECT escort_id, `data`
                    FROM escort_profiles_snapshots_v2
                    WHERE escort_id IN  ('. $escort_ids .')';

            return self::db()->fetchAll($sql);
        } catch (Exception $e) {
            return $e;
        }
    }

    public function getCitiesByEscortId( $escort_id )
    {
        try{
            $sql = "SELECT
                        city_id 
                    FROM
                        escort_cities 
                    WHERE
                        escort_id = ?";
            $result = self::db()->fetchAll($sql, $escort_id);

            return $result;
        }catch (Exception $e){
            return $e;
        }
    }
    public function getCitiesDataByEscortId( $escort_id )
    {
        try{
            $sql = 'SELECT
                        ec.city_id,
                        cc.' . Cubix_I18n::getTblField('title') . ' AS title
                    FROM
                        escort_cities ec
                        INNER JOIN cities cc ON cc.id = ec.city_id
                    WHERE
                        ec.escort_id = ?';
            $result = self::db()->fetchAll($sql, $escort_id);

            return $result;
        }catch (Exception $e){
            return $e;
        }
    }
	
	public function getPaxumEmail($escort_id)
	{
		$sql = "SELECT paxum_email FROM escort_skype
                    WHERE
                        escort_id = ?";
        return self::db()->fetchOne($sql, $escort_id);
	}
	
	public function updateCamStatus($id, $data)
    {
    	return self::db()->update('escorts', 
					$data,
					self::db()->quoteInto('id = ?', $id)
				);
    }

    /**
     * Check if specified escort has any tour with specified filters.
     *
     * @param Int $escortId
     * @param array $filters | example ['c.id IN (1,2)', "(ct.slug = 'yerevan' OR c.slug = 'canada')"]
     * @param array $joins | example ['countries', 'cities' ...]
     * @return bool
     */
    public function hasTour($escortId, $filters = array(), $joins = array())
    {
        $joinsStr = ' ';
        foreach ($joins as $joinItem) {
            switch ($joinItem) {
                case 'countries':
                    $joinsStr .= 'INNER JOIN countries c ON c.id = t.country_id ';
                    break;
                case 'cities':
                    $joinsStr .= 'INNER JOIN cities ct ON ct.id = t.city_id ';
                    break;
                case 'regions':
                    $joinsStr .= 'INNER JOIN regions r ON r.id = ct.region_id ';
                    break;
            }
        }
        unset($joins); // Deallocate memory

        $filtersStr = implode(' AND ', $filters);
        unset($filters); // Deallocate memory

        // To get only active tours
        $filtersStr .= ' AND ((DATE(t.date_from) <= CURDATE() AND DATE(t.date_to) >= CURDATE()) OR DATE(t.date_from) > CURDATE())';
        $filtersStr .= ' AND t.escort_id = ?';

        $sql = 'SELECT true FROM tours t ' . $joinsStr . ' WHERE ' . $filtersStr;
        unset($filtersStr); // Deallocate memory

        return self::db()->fetchOne($sql, intval($escortId));
    }

    public function setButtonClickED($user_id,$button_id)
    {
        $user_clicked_ever = self::db()->fetchOne('SELECT click_count FROM buttons_clicks_track WHERE user_id = ? AND button_id = ?', array($user_id,$button_id));
        $data = array('user_id' => $user_id,'button_id' =>  $button_id);
        try {
            if ( $user_clicked_ever && $user_clicked_ever > 0 ) {
                $data['click_count'] = $user_clicked_ever + 1;

                return self::db()->update('buttons_clicks_track', $data, array(self::db()->quoteInto('user_id = ?', $user_id), self::db()->quoteInto('button_id = ?', $button_id))
                );
            } else {
                $data['click_count'] = 1;
                return self::db()->insert('buttons_clicks_track', $data);
            }
        }catch (Exception $e)
        {
            return $e;
        }
    }

    public static function getActiveEscorts($page)
    {
        if ($page < 1) {
            $page = 1;
        }
        $take = 100;
        $skip = ($page - 1) * $take;
        $sql = "
			SELECT SQL_CALC_FOUND_ROWS
			        e.status,
			        e.id,
				    ep_v2.website AS website,
                    ep_v2.about_en AS description,
                    ep_v2.showname AS name,
                    c.title_en AS city,
                    ep_v2.zip AS zip_code,
                    a.`name` AS agency_name,
                    FLOOR(DATEDIFF(CURDATE(), ep_v2.birth_date) / 365.25) AS age, 
                    ep_v2.breast_type,
                    ep_v2.dress_size,
                    ep_v2.shoe_size,
                    ep_v2.cup_size,
                    ep_v2.hair_color,
                    ep_v2.hair_length,
                    ep_v2.ethnicity,
                    ep_v2.height,
                    ep_v2.weight,
                    ep_v2.measure_units,
                    ep_v2.bust,
                    ep_v2.waist,
                    ep_v2.hip,
                    ep_v2.pubic_hair as kitty,
                    ep_v2.is_smoking as smokes,
                    e.is_pornstar as pornstar,
                    ep_v2.piercing,
                    ep_v2.tatoo as tattoo,
                    e.gender,
                    ep_v2.phone_number AS phone,
                    ep_v2.phone_number_alt AS phone2,
                    ep_v2.email
            FROM       
			escort_profiles_v2 ep_v2
	        LEFT JOIN escorts e ON ep_v2.escort_id = e.id
	        LEFT JOIN escort_photos ep ON ep.escort_id = ep_v2.escort_id
	        LEFT JOIN cities c ON e.city_id = c.id
	        LEFT JOIN agencies a ON e.agency_id = a.id
	        LEFT JOIN reviews r ON e.id = r.escort_id
	        WHERE e.status & ".Cubix_EscortStatus::ESCORT_STATUS_ACTIVE." AND e.country_id = '215' AND e.gender != '2'
	        AND NOT (e.status & ".Cubix_EscortStatus::ESCORT_STATUS_DELETED.")
	        AND NOT (e.status & ".Cubix_EscortStatus::ESCORT_STATUS_NO_PROFILE.")
	        AND NOT (e.status & ".Cubix_EscortStatus::ESCORT_STATUS_TEMPRARY_DELETED.")
	        AND NOT (e.status & ".Cubix_EscortStatus::ESCORT_STATUS_SUSPICIOUS_DELETED.")
	        GROUP BY e.id
	        LIMIT $skip, $take
		";
        $escorts = self::db()->fetchAll($sql);

        $countSql = "SELECT FOUND_ROWS();";
        $count = self::db()->fetchOne($countSql);

        return ['list' => $escorts, 'count' => $count];
    }

    public static function getReviewsByEscortIds($ids)
    {
        $ids = implode(',', $ids);
        $sql = " 
			SELECT
                r.escort_id,
                r.t_meeting_date AS meeting_date,
                r.t_meeting_place AS meeting_place,
                r.t_meeting_duration AS meeting_duration,
                r.t_comments AS comments,
                r.services_comments,
                r.review,
                r.meeting_date,
                r.duration,
                r.duration_unit,
                r.price,
                r.currency,
                r.looks_rating,
                r.services_rating,
                r.services_comments,
                r.review,
                ct.title_en as city,
                r.s_kissing as kissing,
                r.s_blowjob as blowjob,
                r.s_cumshot as cumshot,
                r.s_69 as 69_pos,
                r.s_anal as anal,
                r.s_sex as sex,
                r.s_attitude as attitude,
                r.s_conversation as conversation,
                r.s_breast as breast,
                r.s_multiple_sex as multiple_sex,
                r.s_availability as availability,
                r.s_photos as photos,
                r.t_user_info as user,
                r.is_suspicious,
                r.is_deleted,
                r.deleted_date,
                r.reason,
                r.escort_comment,
                r.disabled_reason,
                r.modified,
                r.answer
                FROM
	            reviews r
	            INNER JOIN currencies c ON c.id = r.currency
	            INNER JOIN cities ct ON ct.id = r.city
	            WHERE r.escort_id IN ($ids)
		";
        $reviews = self::db()->fetchAll($sql);
        return $reviews;
    }

    public static function getPhotosByEscortIds($ids)
    {
        $ids = implode(',', $ids);
        $sql = "
			SELECT * FROM
	        escort_photos ep
	        WHERE ep.escort_id IN ($ids)
		";
        $photos = self::db()->fetchAll($sql);
        return $photos;
    }

    public static function getVideosByEscortIds($ids)
    {
        $ids = implode(',', $ids);
        $sql = "
			SELECT * FROM
	        video v
	        WHERE v.escort_id IN ($ids)
		";
        $videos = self::db()->fetchAll($sql);
        return $videos;
    }

    public static function getEscortServices($ids)
    {
        $ids = implode(',', $ids);
        $sql = "
			SELECT * FROM
	        escort_services es
	        WHERE es.escort_id IN ($ids)
		";
        $services = self::db()->fetchAll($sql);
        return $services;
    }


    public static function getEscortIncallServices($ids)
    {
        $ids = implode(',', $ids);
        $sql = "
			SELECT 
	        er.escort_id,
	        er.time,
	        er.time_unit,
	        er.hh_price,
	        er.availability,
	        er.price,
	        cr.title,
	        er.type
	        FROM escort_rates er
	        INNER JOIN currencies cr on cr.id = er.currency_id 
	        WHERE er.escort_id IN ($ids) AND er.availability != 2
		";
        $incallServicesRates = self::db()->fetchAll($sql);
        return $incallServicesRates;
    }

    public static function getEscortOutcallServices($ids)
    {
        $ids = implode(',', $ids);
        $sql = "
			SELECT 
	        er.escort_id,
	        er.time,
	        er.time_unit,
	        er.hh_price,
	        er.availability,
	        er.price,
	        cr.title,
	        er.type
	        FROM escort_rates er
	        INNER JOIN currencies cr on cr.id = er.currency_id 
	        WHERE er.escort_id IN ($ids) AND er.availability != 1
		";
        $outcallServicesRates = self::db()->fetchAll($sql);
        return $outcallServicesRates;
    }

    public static function getEscortDashboardData($escort_id)
    {
        $sql = "
			SELECT
                max( v.id ) AS video_id,
                e.last_hand_verification_date,
                max( go.id ) AS gotd,
                op.package_id 
            FROM
                escorts e
                LEFT JOIN video v ON v.escort_id = e.id 
                AND v.`status` = 'approve'
                LEFT JOIN gotd_orders go ON go.escort_id = e.id 
                AND go.STATUS = 2
                LEFT JOIN order_packages op ON op.escort_id = e.id 
                AND op.`status` = 2 
                AND op.order_id IS NOT NULL 
            WHERE
                e.id = ?
		";
        $data = self::db()->fetchRow($sql,array($escort_id));
        return $data;
    }

    public static function getChatOnlineEscortsUK($user_ids,$page = 0,$per_page = 3)
    {
        $user_ids_str = implode(',', $user_ids);
        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    e.id AS id,
                    e.gender,
                    e.showname,
                    ep.`hash` AS photo_hash,
                    e.user_id,
                    ep.ext AS photo_ext,
                    ".Cubix_Application::getId()." AS application_id,
                    ct.title_en AS city 
                FROM
                    escorts e
                    INNER JOIN cities ct ON ct.id = e.city_id
                    INNER JOIN escort_photos ep ON ep.escort_id = e.id 
                    AND ep.is_main = 1 
                WHERE
                    e.status & ".Cubix_EscortStatus::ESCORT_STATUS_ACTIVE." 
                    AND e.user_id IN ( ".$user_ids_str." )
                GROUP BY
                    e.id
                    LIMIT ?,?"		;

        $escorts = self::db()->fetchAll($sql,array($page,$per_page));

        $count = self::db()->fetchOne('SELECT FOUND_ROWS()');

        return array('escorts' => $escorts,'count' => $count);
    }

    public static function getBlockedCountries($escort_id)
    {
        $sql = "SELECT c.iso FROM escort_blocked_countries ebc INNER JOIN countries c ON c.id = ebc.country_id
			WHERE ebc.escort_id = ?";

        return self::db()->fetchAssoc($sql,array($escort_id));
    }

    public function getEscortWorkingCities($escort_id,$lang_id)
    {
        $sql = "SELECT c.id, c.title_{$lang_id} as title FROM escort_cities ec INNER JOIN cities c on c.id = ec.city_id WHERE ec.escort_id = ?";

        return self::db()->fetchAll($sql,array($escort_id));
    }
    public function getEscortVaccinated($escort_id){
    	return self::db()->fetchOne('SELECT is_vaccinated from escorts where id=?', array($escort_id));
    }
    public function updateEscortVaccinated($escort_id, $is_vaccinated){
		return self::db()->update('escorts', array('is_vaccinated' => (int)$is_vaccinated), self::db()->quoteInto('id = ?', $escort_id));
    }

    public function setNewHistory($escort_id, $day, $type){
    	return self::db()->query("INSERT into escorts_new_history (escort_id, expires_at, type) VAlUES (".$escort_id.", NOW() + INTERVAL ".$day." DAY, ".$type);
    }

    public function checkEscortLast60Days($escort_id){
    	$sql = "SELECT expires_at as expires_at from escorts_new_history where escort_id = '".$escort_id."' AND type='2' order by id DESC LIMIT 1 ";
		return self::db()->query($sql)->fetch();
    }

    public function checkEscortGets1DayNew($escort_id){
		$sql = "SELECT count(*) as `count` from escorts_new_history where escort_id = '".$escort_id."' AND expires_at > NOW() - INTERVAL 60 DAY";
		return self::db()->query($sql)->fetch();
	}

	public function checkNewEscort($escort_id){
		$sql = "SELECT true as new_escort from escorts_new_history where escort_id='".$escort_id."'";
		return self::db()->query($sql)->fetch();
	}

	public function getEscortVideoLink($escort_id){
		$sql = "SELECT video_link from escort_profiles_v2 where escort_id = ".$escort_id;
		return self::db()->fetchOne($sql);
	}

    public function getTaggedEscorts($tagId)
    {
        $sql = 'SELECT
                    escort_id 
                FROM
                    escort_tags 
                WHERE
                    tag_id = ?';

        return self::db()->fetchCol($sql,array($tagId));
    }

    public function insertIntoTracker($data)
    {
        return self::db()->insert('tag_tracker', $data);;
    }

    public function isImported($escort_id)
    {
        $sql = "SELECT true from importer_data where escort_id = ".$escort_id;
        return self::db()->fetchOne($sql);
    }


}
