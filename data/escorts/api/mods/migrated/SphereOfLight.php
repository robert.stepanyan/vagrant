<?php

/**
 * Class Cubix_Api_Module_SphereOfLight
 * @author Eduard Hovhannisyan
 */
class Cubix_Api_Module_SphereOfLight extends Cubix_Api_Module
{
    /**
     * Returns escorts by specified ids
     * Note! Is written originally for EDIR only.
     *
     * @param $ids
     * @return mixed
     */
    public function getEscortsById($ids)
    {
        $sql = "SELECT escort_id, about_en, about_de, about_es, about_fr, about_it, about_ro
			FROM escort_profiles_v2 WHERE escort_id in (" . implode(',', $ids) . ")";
        return self::db()->fetchAll($sql);
    }

    /**
     *  Returns Array of escorts, who's actual status includes 'partially_complete' status
     *
     * @return mixed
     */
    public function getPartiallyCompleteEscorts()
    {
        $sql = "SELECT id as escort_id, status, showname FROM escorts WHERE status & " . Cubix_EscortStatus::ESCORT_STATUS_PARTIALLY_COMPLETE;
        return self::db()->fetchAll($sql);
    }
}

?>