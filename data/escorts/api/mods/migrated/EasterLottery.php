<?php

class Cubix_Api_Module_EasterLottery extends Cubix_Api_Module
{
	
	public function check($user_id, $lang)
	{
		
		try{
			$exists = self::db()->fetchOne(" SELECT TRUE FROM easter_logged_users WHERE user_id = ?", array($user_id));
			$was_winner = self::db()->fetchOne(" SELECT TRUE FROM easter_winners WHERE user_id = ?", array($user_id));
			
			if($exists || $was_winner){
				return array('status' => 0);
			}
			
			$user_data = array(
				'user_id' => $user_id,
				'login_date' => new Zend_Db_Expr('NOW()')
			);
			self::db()->insert('easter_logged_users', $user_data);
			self::_addLoginCount();
			$is_winner = self::_checkWinner($lang);
			
			if($is_winner){
                $user_data['prize'] = $is_winner;
				self::db()->insert('easter_winners', $user_data);
				return  array('status' => 2, 'text' => $is_winner);
			}
			else{
				return  array('status' => 1);
			}
		} catch (Exception $e) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
		
	private function _addLoginCount()
	{
		self::db()->query('UPDATE easter_login_counts SET count = count + 1 WHERE id = ?', mt_rand(1,10));
	}
	
	private function _checkWinner($lang)
	{
		$sum = self::db()->fetchOne(" SELECT SUM(count) FROM easter_login_counts ");
		$is_winner = self::db()->fetchOne(" SELECT text_".$lang." FROM easter_win_tries WHERE date = CURDATE() AND  login_try = ?", array($sum));
		return $is_winner;
	}

    public function setPopupShowed($user_id)
    {
        self::db()->insert('easter_showed_popup', array('user_id' => $user_id));
    }

    public function getPopupShowed($user_id)
    {
        return self::db()->fetchOne(" SELECT TRUE FROM easter_showed_popup WHERE user_id = ?", array($user_id));
    }
}
