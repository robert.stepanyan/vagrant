<?php

class Cubix_Api_Module_Members extends Cubix_Model
{
	const BL_MEMBER_COMMENTS_STATUS_CONFIRMED = 2;
	
	public function saveMember($data)
	{
		try {
			self::db()->insert('members', $data);
			$new_id = self::db()->lastInsertId();
			self::db()->update('members', array(
				'payment_ref' => md5('m_' . $new_id)
			), self::db()->quoteInto('id = ?', $new_id));
			return $new_id;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function getEscortAlerts($user_id, $escort_id)
	{
		$escort_id = intval($escort_id);
		$user_id = intval($user_id);
		
		try {
			return self::db()->query('SELECT * FROM alert_users WHERE user_id = ? AND escort_id = ? ORDER BY event ASC', array($user_id, $escort_id))->fetchAll();
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function alertsSave($user_id, $escort_id, $events, $extra = null)
	{
		$escort_id = intval($escort_id);
		$user_id = intval($user_id);
		
		$c = 0;
		
		try {
			self::db()->query('DELETE FROM alert_users WHERE user_id = ? AND escort_id = ?', array($user_id, $escort_id));
						
			foreach (explode(',', $events) as $ev)
			{
				if (intval($ev) > 0)
				{
					$arr = array(
						'user_id' => $user_id,
						'escort_id' => $escort_id,
						'event' => intval($ev),
						'date' => new Zend_Db_Expr('NOW()')
					);
					
					if ($ev == ALERT_ME_CITY && strlen($extra) > 0)
					{
						$a = array();
						
						foreach (explode(',', $extra) as $e)
							if (intval($e) > 0)
								$a[] = intval($e);
						
						$extra = implode(',', $a);
						
						if (strlen($extra) > 0)
							$arr['extra'] = $extra;
					}
					
					self::db()->insert('alert_users', $arr);
				}
			}
			
			$c = self::db()->fetchOne('SELECT COUNT(*) FROM alert_users WHERE user_id = ? AND escort_id = ?', array($user_id, $escort_id));
			
			return $c;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function getAlerts($user_id)
	{
		$user_id = intval($user_id);
		
		try {
			return self::db()->query('SELECT escort_id, event, extra FROM alert_users WHERE user_id = ? ORDER BY event ASC', array($user_id))->fetchAll();
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function getEDAlertsGrouped($user_id, $filter, $page, $per_page)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($page)) $page = reset($page);
		if (is_array($per_page)) $per_page = reset($per_page);
		
		try {
			$sql = '
				SELECT escort_id, event, extra, status  
				FROM alert_users 
				WHERE user_id = ? 
			';
			
			$countSql = '
				SELECT COUNT(DISTINCT(escort_id))   
				FROM alert_users 
				WHERE user_id = ? ';

			if (isset($filter['show']))
			{
				$where = ' AND status = ' . $filter['show'];
			}
			
			$sql .= $where;
			$countSql .= $where;

			$sql .= '
				GROUP BY escort_id 
				ORDER BY escort_id DESC 
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;

			$items = array(
				'data'	=> self::db()->query($sql, $user_id)->fetchAll(),
				'count'	=> self::db()->fetchOne($countSql, $user_id)
			);

			return $items;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function getEDAlerts($user_id, $escorts)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		
		if (!count($escorts))
		{
			return array();
		}
		
		try {
			$escorts_str = implode(',', $escorts);
			
			$sql = '
				SELECT escort_id, event, extra, status  
				FROM alert_users 
				WHERE user_id = ? AND escort_id IN (' . $escorts_str . ')
			';
			
			return self::db()->query($sql, $user_id)->fetchAll();
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function alertSetStatus($user_id, $escort_id, $status)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		if (is_array($status)) $status = reset($status);
		
		try {
			self::db()->query('UPDATE alert_users SET status = ? WHERE user_id = ? AND escort_id = ?', array($status, $user_id, $escort_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function isInAlerts($user_id, $escort_id)
	{
		$escort_id = intval($escort_id);
		$user_id = intval($user_id);
		
		try {
			return self::db()->fetchOne('SELECT COUNT(*) FROM alert_users WHERE user_id = ? AND escort_id = ?', array($user_id, $escort_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	function getAlertsByUserId($user_id)
	{
		$alerts = self::db()->fetchAll('SELECT escort_id FROM alert_users WHERE user_id = ? GROUP BY escort_id', array($user_id));
		$alert_array = array();
		foreach($alerts as $alert){
			$alert_array[] = $alert->escort_id;
		}

		return $alert_array;
	}

	public function getMembers($page = 1 , $per_page = 25 , $filter = array(), $sort_field = 'username', $sort_dir=" ASC "){
		
		$sql = 'SELECT u.id,
				u.username,
				u.last_ip,
				UNIX_TIMESTAMP(u.date_registered) AS creation_date, 
				c.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				co.' . Cubix_I18n::getTblField('title') . ' AS country_title
			FROM members m
			INNER JOIN users u ON u.id = m.user_id
			LEFT JOIN cities c ON c.id = m.city_id
			LEFT JOIN countries co ON co.id = m.country_id
			WHERE u.status = ?
		';
		
		if ( strlen($filter['username']) && strlen( $filter['username'] ) < 25 ){
			$replaced_username = preg_replace('#(^[a-z])(^[0-9])(^_)(^-)+$#i', '' , $filter['username']);
			$filter['username'] = $replaced_username;
			$where .= self::quote(' AND u.username LIKE ?', '%' . $filter['username'] . '%');
		}
			
		$sql .= $where;
		$countSql =  '
			SELECT count(u.id)
			FROM members m
			INNER JOIN users u ON u.id = m.user_id
			LEFT JOIN cities c ON c.id = m.city_id
			LEFT JOIN countries co ON co.id = m.country_id
			WHERE u.status = ? 

		'.$where;
	
		$sql .= 'GROUP BY u.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';
		$rowsCount = self::db()->fetchOne($countSql, USER_STATUS_ACTIVE);
		$items['rowsCount'] = $rowsCount;
		$items['members'] = self::db()->fetchAll($sql, USER_STATUS_ACTIVE);
		return $items;
	}
	
	public function getMembersInfoByUsername($username)
	{
		$fields = "";

		if ( in_array(Cubix_Application::getId(), array(APP_6A, APP_EF)) ) {
			$fields = " m.rating, ";			
		}
		
		if ( in_array(Cubix_Application::getId(), array(APP_ED)) ) {
			$fields = "m.id as member_id, m.hash, m.ext, m.gender , m.show_gender, m.show_location, m.show_about_me, m.show_languages, m.langs, m.show_top10, u.last_login_date, ";	
		}
		
		$sql = 'SELECT u.username,u.id AS user_id, u.date_registered, m.comments_count, m.email, m.reviews_count, m.about_me,
			 c.'. Cubix_I18n::getTblField('title') . ' AS country,
			  ' . $fields . '
			 cc.'. Cubix_I18n::getTblField('title') . ' AS city, m.email, u.email AS reg_email
			 FROM users AS u
			 INNER JOIN members m ON m.user_id = u.id
			 LEFT JOIN countries c ON c.id = m.country_id
			 LEFT JOIN cities cc ON cc.id = m.city_id
			 WHERE u.username = ? AND u.status = ?';
		
		try {
			$member_info = self::db()->fetchRow($sql, array($username, USER_STATUS_ACTIVE));
			$member_info->last_comment_date =  $dfd= self::getLastCommentDate($member_info->user_id);
			$member_info->last_review_date = self::getLastReviewDate($member_info->user_id);
			return $member_info;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

    public function getMembersInfoById($user_id)
    {

        $sql = 'SELECT u.username,u.id AS user_id, u.date_registered, m.comments_count, m.email, m.reviews_count, m.about_me,m.email, u.email AS reg_email
			 FROM users AS u
			 INNER JOIN members m ON m.user_id = u.id
			 WHERE m.user_id = ?';

        try {
            $member_info = self::db()->fetchRow($sql, array($user_id));
            return $member_info;
        }
        catch ( Exception $e ) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

	public static function getLastCommentDate($user_id)
	{
		$user_id = is_array($user_id) ? intval(reset($user_id)) : $user_id;
		$sql = 'SELECT MAX(time) FROM comments WHERE user_id = ? ';
		return self::db()->fetchOne($sql, array($user_id));
	}

	public static function getLastReviewDate($user_id)
	{
		$user_id = is_array($user_id) ? intval(reset($user_id)) : $user_id;
		$sql = 'SELECT MAX(creation_date) FROM reviews WHERE user_id = ? ';
		return self::db()->fetchOne($sql, array($user_id));
	}
	
	public static function getCommentsCount($user_id)
	{
		$user_id = is_array($user_id) ? intval(reset($user_id)) : $user_id;
		$sql = 'SELECT COUNT(id) FROM comments WHERE user_id = ? ';
		return self::db()->fetchOne($sql, array($user_id));
	}
	
	public static function getReviewsCount($user_id)
	{
		$user_id = is_array($user_id) ? intval(reset($user_id)) : $user_id;
		$sql = 'SELECT COUNT(id) FROM reviews WHERE user_id = ? ';
		return self::db()->fetchOne($sql, array($user_id));
	}
	
	public function getUsernameById($user_id)
	{
		$user_id = is_array($user_id) ? intval(reset($user_id)) : $user_id;
		$sql = 'SELECT username FROM users WHERE id = ? ';
		return self::db()->fetchOne($sql, array($user_id));
	}
	
	public function addBlacklistedComment($escort_id, $user_id, $comment)
	{
		
		
		self::db()->query('INSERT INTO blacklisted_members_comments (escort_id, user_id, comment, date) VALUES(?, ?, ?, NOW())', array($escort_id, $user_id, $comment));
	}
	
	public function getBlacklistedMembers($username, $page, $per_page)
	{
		
		
		$bind = array();
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				bmc.id, bmc.comment AS last_comment, count(bmc.user_id) AS comments_count, u.username, e.showname AS escort_showname, bmc.user_id
			FROM blacklisted_members_comments bmc
			INNER JOIN users u ON u.id = bmc.user_id
			INNER JOIN escorts e ON e.id = bmc.escort_id
		';
		
		$where = 'WHERE bmc.status = ? ';
		$bind[] = self::BL_MEMBER_COMMENTS_STATUS_CONFIRMED;
		
		if ( strlen($username) ) {
			$where .= 'AND u.username LIKE ? ';
			$bind[] = '%' . $username . '%';
		}
		$sql .= $where;
		
		$sql .= '
			GROUP BY bmc.user_id
			ORDER BY bmc.date DESC
			LIMIT ' . ($page - 1) * $per_page . ', ' . $page * $per_page;
		
		$bl_members = array(
			'data'	=> self::db()->fetchAll($sql, $bind),
			'count'	=> self::db()->fetchOne('SELECT FOUND_ROWS()')
		);
		
		return $bl_members;
	}
	
	public function getBlacklistedMemberComments($user_id, $page, $per_page)
	{
		
		
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				bmc.id, bmc.comment, bmc.date, u.username, e.showname AS escort_showname, bmc.user_id
			FROM blacklisted_members_comments bmc
			INNER JOIN users u ON u.id = bmc.user_id
			INNER JOIN escorts e ON e.id = bmc.escort_id
			WHERE u.id = ? AND bmc.status = ?
		';
		$sql .= '
			ORDER BY bmc.date DESC
			LIMIT ' . ($page - 1) * $per_page . ', ' . $page * $per_page;
		
		$bl_members = array(
			'data'	=> self::db()->fetchAll($sql, array($user_id, self::BL_MEMBER_COMMENTS_STATUS_CONFIRMED)),
			'count'	=> self::db()->fetchOne('SELECT FOUND_ROWS()')
		);
		
		return $bl_members;
	}
	
	public function checkIfBlacklisted($user_id)
	{
		
		
		return self::db()->fetchOne('SELECT TRUE FROM blacklisted_members_comments WHERE user_id = ? AND status = ? GROUP BY user_id', array($user_id, self::BL_MEMBER_COMMENTS_STATUS_CONFIRMED));
	}
	
	/* favorites */
	public function getFavorites($user_id, $filter, $page, $per_page)
	{
		$config = Zend_Registry::get('config');
		
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($page)) $page = reset($page);
		if (is_array($per_page)) $per_page = reset($per_page);
		
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				f.user_id, f.escort_id AS escort_id, f.escort_id AS id, f.comment,
				f.rank, e.showname, ep.hash AS photo_hash, ep.ext AS photo_ext, 
				e.status, c.'. Cubix_I18n::getTblField('title') . ' AS city, 
				' . Cubix_Application::getId() . ' AS application_id, e.verified_status,
				IF (e.date_registered > (CURDATE() - INTERVAL ' . $config->newIntervalInDays . ' DAY), 1, 0) AS is_new,
				e.is_suspicious, e.gender, e.user_id AS escort_user_id, op.package_id
			FROM favorites f
			INNER JOIN escorts e ON e.id = f.escort_id
			INNER JOIN escort_photos ep ON ep.escort_id = e.id AND ep.is_main = 1
			INNER JOIN cities c ON e.city_id = c.id 
			LEFT JOIN order_packages op ON op.escort_id = f.escort_id AND op.status = 2
			WHERE f.user_id = ? 
		';
		
		if (strlen($filter['showname']))
		{
			$sql .= ' AND e.showname LIKE "' . $filter['showname'] . '%" ';
		}
		
		if (strlen($filter['city']))
		{
			$sql .= ' AND e.city_id = ' . $filter['city'];
		}
		
		if (isset($filter['act']))
		{
			if ($filter['act'] == 2)
			{
				if (Cubix_Application::getId() == APP_6A)
					$sql .= ' AND e.status = 32 AND LENGTH(op.package_id) > 0 AND op.package_id <> 97 ';
				elseif (Cubix_Application::getId() == APP_EF)
					$sql .= ' AND e.status = 32 AND LENGTH(op.package_id) > 0 AND op.package_id <> 100 ';
				elseif (Cubix_Application::getId() == APP_A6)
					$sql .= ' AND e.status = 32 AND LENGTH(op.package_id) > 0 ';
				else
					$sql .= ' AND e.status = 32 ';
			}
			elseif ($filter['act'] == 3)
			{
				if (Cubix_Application::getId() == APP_6A)
					$sql .= ' AND (e.status <> 32 OR LENGTH(op.package_id) = 0 OR op.package_id = 97) ';
				elseif (Cubix_Application::getId() == APP_EF)
					$sql .= ' AND (e.status <> 32 OR LENGTH(op.package_id) = 0 OR op.package_id = 100) ';
				elseif (Cubix_Application::getId() == APP_A6)
					$sql .= ' AND (e.status <> 32 OR LENGTH(op.package_id) = 0) ';
				else
					$sql .= ' AND e.status <> 32 ';
			}
		}
		
		$sql .= '
			GROUP BY f.escort_id
			ORDER BY f.add_date DESC
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
		


		$items = array(
			'data'	=> self::db()->fetchAll($sql, array($user_id)),
			'count'	=> self::db()->fetchOne('SELECT FOUND_ROWS()')
		);
		
		return $items;
	}

	public function getEDFavorites($user_id, $filter, $order_by, $page, $per_page)
	{
		$config = Zend_Registry::get('config');
		
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($page)) $page = reset($page);
		if (is_array($per_page)) $per_page = reset($per_page);
		if (is_array($order_by)) $order_by = reset($order_by);
		
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				f.user_id, f.escort_id AS escort_id, f.escort_id AS id, f.comment,
				e.showname, ep.hash AS photo_hash, ep.ext AS photo_ext, 
				e.status, c.id AS city_id, c.'. Cubix_I18n::getTblField('title') . ' AS city, 
				' . Cubix_Application::getId() . ' AS application_id, e.verified_status,
				IF (e.date_registered > (CURDATE() - INTERVAL ' . $config->newIntervalInDays . ' DAY), 1, 0) AS is_new,
				e.is_suspicious, e.gender, e.user_id AS escort_user_id, op.package_id
			FROM favorites f
			INNER JOIN escorts e ON e.id = f.escort_id
			INNER JOIN escort_photos ep ON ep.escort_id = e.id AND ep.is_main = 1
			INNER JOIN cities c ON e.city_id = c.id 
			LEFT JOIN order_packages op ON op.escort_id = f.escort_id AND op.status = 2
			WHERE f.user_id = ? 
		';
		
		if (strlen($filter['showname']))
		{
			$sql .= ' AND e.showname LIKE "' . $filter['showname'] . '%" ';
		}
		
		if (strlen($filter['city']))
		{
			$sql .= ' AND e.city_id = ' . $filter['city'];
		}
		
		if (isset($filter['act']))
		{
			if ($filter['act'] == 2)
			{
				if (Cubix_Application::getId() == APP_6A)
					$sql .= ' AND e.status = 32 AND LENGTH(op.package_id) > 0 AND op.package_id <> 97 ';
				elseif (Cubix_Application::getId() == APP_EF)
					$sql .= ' AND e.status = 32 AND LENGTH(op.package_id) > 0 AND op.package_id <> 100 ';
				elseif (Cubix_Application::getId() == APP_A6)
					$sql .= ' AND e.status = 32 AND LENGTH(op.package_id) > 0 ';
				else
					$sql .= ' AND e.status = 32 ';
			}
			elseif ($filter['act'] == 3)
			{
				if (Cubix_Application::getId() == APP_6A)
					$sql .= ' AND (e.status <> 32 OR LENGTH(op.package_id) = 0 OR op.package_id = 97) ';
				elseif (Cubix_Application::getId() == APP_EF)
					$sql .= ' AND (e.status <> 32 OR LENGTH(op.package_id) = 0 OR op.package_id = 100) ';
				elseif (Cubix_Application::getId() == APP_A6)
					$sql .= ' AND (e.status <> 32 OR LENGTH(op.package_id) = 0) ';
				else
					$sql .= ' AND e.status <> 32 ';
			}
		}

		$group_by = " f.escort_id ";
		if ( ! strlen($order_by) ) {
			$order_by = "alpha";
		}

		$order_by_str = " f.add_date DESC ";
		switch ($order_by) {
			case "alpha":
				$order_by_str = " e.showname ASC ";
				break;
			case "date_added":
				$order_by_str = " f.add_date DESC ";
				break;
			case "by_city":
				$order_by_str = " c.title_en ASC ";
				$group_by = " c.id ";
				break;
		}
		
		$sql .= '
			GROUP BY ' . $group_by . '
			ORDER BY ' . $order_by_str . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
		


		$items = array(
			'data'	=> self::db()->fetchAll($sql, array($user_id)),
			'count'	=> self::db()->fetchOne('SELECT FOUND_ROWS()')
		);
		
		return $items;
	}

	public function updateCommentED($user_id, $escort_id, $note)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		if (is_array($note)) $note = reset($note);
		
		self::db()->update('favorites', array(
			'comment' => $note
		), array(
			self::db()->quoteInto('user_id = ?', $user_id), 
			self::db()->quoteInto('escort_id = ?', $escort_id)
		));
	}
	
	public function getFavoritesTop10($user_id)
	{
		$config = Zend_Registry::get('config');
		
		if (is_array($user_id)) $user_id = reset($user_id);
				
		$sql = '
			SELECT 
				f.user_id, f.escort_id AS escort_id, f.escort_id AS id, f.comment,
				f.rank, e.showname, ep.hash AS photo_hash, ep.ext AS photo_ext, 
				e.status, ' . Cubix_Application::getId() . ' AS application_id,
				c.'. Cubix_I18n::getTblField('title') . ' AS city,
				e.verified_status, IF (e.date_registered > (CURDATE() - INTERVAL ' . $config->newIntervalInDays . ' DAY), 1, 0) AS is_new,
				e.is_suspicious, f.is_hidden, op.package_id, f.comment_status, f.type, f.id AS f_id 
			FROM favorites f
			INNER JOIN escorts e ON e.id = f.escort_id
			INNER JOIN escort_photos ep ON ep.escort_id = e.id AND ep.is_main = 1
			INNER JOIN cities c ON e.city_id = c.id 
			LEFT JOIN order_packages op ON op.escort_id = f.escort_id AND op.status = 2
			WHERE f.user_id = ? AND f.rank > 0
			GROUP BY f.id 
			ORDER BY f.rank ASC
		';
				
		$items = self::db()->fetchAll($sql, array($user_id));
		
		return $items;
	}
	
	public function updateFavoritesComment($data)
	{
		$cur_comment = self::db()->fetchOne('SELECT comment FROM favorites WHERE user_id = ? AND escort_id = ?', array($data['user_id'], $data['escort_id']));
		$is_edited = 0;
		
		if (strlen($cur_comment) > 0 && $cur_comment != $data['comment'])
			$is_edited = 1;
		
		self::db()->update('favorites', array(
			'comment' => $data['comment'],
			'comment_date' => new Zend_Db_Expr('NOW()'),
			'is_edited' => $is_edited,
			'comment_status' => 1 // Pending
		), array(
			self::db()->quoteInto('user_id = ?', $data['user_id']), 
			self::db()->quoteInto('escort_id = ?', $data['escort_id'])
		));
	}
	
	public function removeFromFavorites($user_id, $escort_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		
		self::db()->delete('favorites', array(
			self::db()->quoteInto('user_id = ?', $user_id), 
			self::db()->quoteInto('escort_id = ?', $escort_id)
		));
	}
	
	public function reorderingTop10($user_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
						
		$items = self::db()->fetchAll('SELECT escort_id FROM favorites WHERE user_id = ? AND rank > 0 ORDER BY rank ASC', array($user_id));
		
		if ($items)
		{
			$c = 1;
			
			foreach ($items as $item)
			{
				self::db()->update('favorites', array(
					'rank' => $c
				), array(
					self::db()->quoteInto('user_id = ?', $user_id), 
					self::db()->quoteInto('escort_id = ?', $item->escort_id)
				));
				
				$c++;
			}
		}
	}

	public function recalculateRating($escort_id)
	{
		if (is_array($escort_id)) $escort_id = reset($escort_id);

		$ranks = self::db()->query("SELECT rank FROM favorites WHERE rank > 0 AND escort_id = ?", $escort_id)->fetchAll();

		$rating = 0;

		if ($ranks)
		{
			foreach ($ranks as $r)
			{
				if ($r->rank == 1)
				{
					$rating += 4;
				}
				elseif ($r->rank == 2)
				{
					$rating += 3;
				}
				elseif ($r->rank == 3)
				{
					$rating += 2;
				}
				else
				{
					$rating ++;
				}
			}
		}

		self::db()->update('favorites', array('rating' => $rating), self::db()->quoteInto('escort_id = ?', $escort_id));
	}
	
	public function addToFavorites($user_id, $escort_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($escort_id)) $escort_id = reset($escort_id);

		$has = self::db()->fetchOne('SELECT TRUE FROM favorites WHERE user_id = ? AND escort_id = ?', array($user_id, $escort_id));

		if (!$has)
		{
			self::db()->insert('favorites', array(
				'user_id' => $user_id,
				'escort_id' => $escort_id,
				'application_id' => Cubix_Application::getId()
			));
		}
	}
	
	public function isInFavorites($user_id, $escort_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		
		
		return self::db()->fetchOne('SELECT 1 FROM favorites WHERE user_id = ? AND escort_id = ?', array($user_id, $escort_id));
	}
	
	public function favoritesAddToTop($user_id, $escort_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		
		$c = self::db()->fetchOne('SELECT COUNT(*) FROM favorites WHERE user_id = ? AND escort_id = ? AND rank > 0', array($user_id, $escort_id));
		
		if (!$c)
		{
			$max = self::db()->fetchOne('SELECT	MAX(f.rank) FROM favorites f inner join escorts e on e.id = f.escort_id WHERE e.status & 32 and f.user_id = ? AND f.rank > 0', $user_id);
			
			if (!$max)
				$max = 0;
			
			if ($max < 10)
			{
				$rank = $max + 1;
				
				self::db()->update('favorites', array(
					'rank' => $rank
				), array(
					self::db()->quoteInto('user_id = ?', $user_id), 
					self::db()->quoteInto('escort_id = ?', $escort_id)
				));
			}
			else
			{
				return 'error';
			}
		}
	}
	
	public function favoritesRemoveFromTop($user_id, $escort_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		
		self::db()->update('favorites', array(
			'rank' => NULL
		), array(
			self::db()->quoteInto('user_id = ?', $user_id), 
			self::db()->quoteInto('escort_id = ?', $escort_id)
		));
	}
	
	public function favoritesMoveUp($user_id, $escort_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		
		$cur_rank = self::db()->fetchOne('SELECT rank FROM favorites WHERE user_id = ? AND escort_id = ?', array($user_id, $escort_id));
		
		if (!$cur_rank || $cur_rank == 1)
			return;
		
		$new_rank = $cur_rank - 1;
		
		$replace_escort = self::db()->fetchOne('SELECT escort_id FROM favorites WHERE user_id = ? AND rank = ?', array($user_id, $new_rank));
		
		self::db()->update('favorites', array(
			'rank' => $cur_rank
		), array(
			self::db()->quoteInto('user_id = ?', $user_id), 
			self::db()->quoteInto('escort_id = ?', $replace_escort)
		));
		
		self::db()->update('favorites', array(
			'rank' => $new_rank
		), array(
			self::db()->quoteInto('user_id = ?', $user_id), 
			self::db()->quoteInto('escort_id = ?', $escort_id)
		));
	}
	
	public function favoritesMoveDown($user_id, $escort_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		
		$cur_rank = self::db()->fetchOne('SELECT rank FROM favorites WHERE user_id = ? AND escort_id = ?', array($user_id, $escort_id));
		$max_rank = self::db()->fetchOne('SELECT MAX(rank) FROM favorites WHERE user_id = ? AND rank > 0', $user_id);
		
		if (!$cur_rank || $cur_rank == $max_rank)
			return;
		
		$new_rank = $cur_rank + 1;
		
		$replace_escort = self::db()->fetchOne('SELECT escort_id FROM favorites WHERE user_id = ? AND rank = ?', array($user_id, $new_rank));
		
		self::db()->update('favorites', array(
			'rank' => $cur_rank
		), array(
			self::db()->quoteInto('user_id = ?', $user_id), 
			self::db()->quoteInto('escort_id = ?', $replace_escort)
		));
		
		self::db()->update('favorites', array(
			'rank' => $new_rank
		), array(
			self::db()->quoteInto('user_id = ?', $user_id), 
			self::db()->quoteInto('escort_id = ?', $escort_id)
		));
	}
	
	public function changeFavCommentType($user_id, $f_id, $type)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($f_id)) $f_id = reset($f_id);
		if (is_array($type)) $type = reset($type);
		
		self::db()->update('favorites', array(
			'type' => $type
		), array(
			self::db()->quoteInto('user_id = ?', $user_id), 
			self::db()->quoteInto('id = ?', $f_id)
		));
	}
	
	public function getFavoriteRequests($user_id, $owner_user_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($owner_user_id)) $owner_user_id = reset($owner_user_id);
		
		return self::db()->query('SELECT fav_id, status FROM favorite_requests WHERE user_id = ? AND owner_user_id = ?', array($user_id, $owner_user_id))->fetchAll();
	}
	
	public function sendFavRequest($user_id, $f_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($f_id)) $f_id = reset($f_id);
		
		$owner = self::db()->fetchOne('SELECT user_id FROM favorites WHERE id = ?', $f_id);
		$exists = self::db()->fetchOne('SELECT COUNT(*) FROM favorite_requests WHERE user_id = ? AND owner_user_id = ? AND fav_id = ?', array($user_id, $owner, $f_id));
		
		if (!$exists)
		{
			self::db()->insert('favorite_requests', array(
				'user_id' => $user_id,
				'fav_id' => $f_id,
				'owner_user_id' => $owner,
				'status' => 1
			));
		}
	}
	
	public function getFavPendingRequestsCount($user_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		
		return self::db()->fetchOne('
			SELECT COUNT(*) FROM favorite_requests fr 
			INNER JOIN favorites f ON f.id = fr.fav_id 
			WHERE fr.owner_user_id = ? AND fr.status = ? AND f.type = ? AND f.rank > 0
		', array($user_id, 1, 2));
	}
	
	public function getFavPendingRequests($user_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		
		return self::db()->query("
			SELECT f.user_id, f.fav_id, IF(u.user_type = 'escort', e.showname, IF(u.user_type = 'agency', a.name, u.username)) AS name, f.id AS req_id
			FROM favorite_requests f 
			INNER JOIN users u ON u.id = f.user_id
			LEFT JOIN escorts e ON e.user_id = f.user_id
			LEFT JOIN agencies a ON a.user_id = f.user_id
			WHERE f.owner_user_id = ? AND f.status = ?
		", array($user_id, 1))->fetchAll();
	}
	
	public function shareFavComment($user_id, $req_id, $type)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($req_id)) $req_id = reset($req_id);
		if (is_array($type)) $type = reset($type);
		
		self::db()->update('favorite_requests', array(
			'status' => $type
		), array(
			self::db()->quoteInto('owner_user_id = ?', $user_id), 
			self::db()->quoteInto('id = ?', $req_id)
		));
		
		return self::db()->query('
			SELECT u.username, u.email, fv.comment, e.showname, e.id AS escort_id
			FROM favorite_requests f 
			INNER JOIN users u ON f.user_id = u.id 
			INNER JOIN favorites fv ON fv.id = f.fav_id 
			INNER JOIN escorts e ON e.id = fv.escort_id 
			WHERE f.id = ?
		', $req_id)->fetch();
	}
	
	public function getChoices($filter, $page, $per_page)
	{		
		if (is_array($page)) $page = reset($page);
		if (is_array($per_page)) $per_page = reset($per_page);
		
		$field = '';
		$where = '';
		$join = '';
		$group = ' f.escort_id ';
		$order = '';
		
		if (!isset($filter['show']))
		{
			$where .= ' AND f.rank > 0 ';
		}
		elseif ($filter['show'] == 'top1_only')
		{
			$where .= ' AND f.rank = 1 ';
			$order = ' COUNT(f.rank) DESC ';
		}
		elseif ($filter['show'] == 'top3_only')
		{
			$where .= ' AND f.rank >= 1 AND f.rank <= 3 ';
		}
		elseif ($filter['show'] == 'gainers')
		{
			$field = ', (COUNT(f.user_id) - fh.count) AS ccount ';
			$join = ' INNER JOIN favorites_history fh ON fh.escort_id = f.escort_id ';
			$where .= ' AND f.rank >= 1 ';
			$group = ' f.escort_id HAVING ccount >= 1 ';
			$order = ' ccount DESC ';
		}
		elseif ($filter['show'] == 'loosers')
		{
			$field = ', (COUNT(f.user_id) - fh.count) AS ccount ';
			$join = ' INNER JOIN favorites_history fh ON fh.escort_id = f.escort_id ';
			$where .= ' AND f.rank >= 1 ';
			$group = ' f.escort_id HAVING ccount <= -1 ';
			$order = ' ccount ASC ';
		}
		else
		{
			$where .= ' AND f.rank > 0 ';
		}
		
		if (strlen($filter['showname']))
		{
			$where .= ' AND e.showname LIKE "' . $filter['showname'] . '%" ';
		}
		
		if (intval($filter['city']) > 0)
		{
			$where .= ' AND e.city_id = ' . $filter['city'];
		}
		
		if (intval($filter['with_comments']) == 1)
		{
			$where .= ' AND LENGTH(f.comment) > 0 AND ((f.type = 1 AND f.comment_status <> 1) OR f.type = 2) ';
		}
		
		if (Cubix_Application::getId() == APP_6B)
		{
			$where .= ' AND NOT e.status &' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status &' . ESCORT_STATUS_DELETED . ' ';
		
		}else{
			if (intval($filter['active_girls']) == 1)
			{
				$where .= ' AND e.status = 32 ';
				
				if (Cubix_Application::getId() == APP_6A)
				{
					$where .= ' AND op.package_id > 0 AND op.package_id <> 97 ';
				}
				elseif (Cubix_Application::getId() == APP_EF)
				{
					$where .= ' AND op.package_id > 0 AND op.package_id <> 100 ';
				}
				elseif (Cubix_Application::getId() == APP_A6)
				{
					$where .= ' AND op.package_id > 0 ';
				}
			}
		}
		
		//Katie's disgusting request 
		if(Cubix_Application::getId() == APP_EF){
			$where .= ' AND e.id <>  45057 ';
		}
		if (isset($filter['show']) && $filter['show'] == 'most_girls')
		{
			$order = ' COUNT(f.rank) DESC ';
		}
		elseif (!isset($filter['show']) || !in_array($filter['show'], array('gainers', 'loosers', 'top1_only')))
		{
			switch ($filter['sort_by'])
			{
				case 'showname':
					$order = ' e.showname ASC ';
					break;
				case 'random':
					$order = ' RAND() ';
					break;
				case 'rating':
					$order = ' f.rating DESC, f.add_date DESC ';
					break;
				case 'date_added':
				default:
					$order = ' f.add_date DESC ';
					break;
			}
		}
		
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				f.escort_id AS escort_id, f.escort_id AS id, e.showname, 
				ep.hash AS photo_hash, ep.ext AS photo_ext, e.status, 
				c.'. Cubix_I18n::getTblField('title') . ' AS city, 
				' . Cubix_Application::getId() . ' AS application_id, e.gender, op.package_id ' . $field . '
			FROM favorites f
			INNER JOIN users u ON u.id = f.user_id AND u.`status` <> -4 AND u.`status` <> -5 
			INNER JOIN members m ON m.user_id = u.id AND m.is_suspicious = 0 
			INNER JOIN escorts e ON e.id = f.escort_id
			INNER JOIN escort_photos ep ON ep.escort_id = e.id AND ep.is_main = 1
			INNER JOIN cities c ON e.city_id = c.id 
			LEFT JOIN order_packages op ON op.escort_id = f.escort_id AND op.status = 2 ' . $join . '
			WHERE 1 ' . $where . '
			GROUP BY ' . $group . '
			ORDER BY ' . $order . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
		

		$escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		
		foreach ($escorts as $k => $escort)
		{
			$top1 = self::db()->fetchOne('SELECT COUNT(id) FROM favorites WHERE escort_id = ? AND rank = 1', $escort->escort_id);
			$top2 = self::db()->fetchOne('SELECT COUNT(id) FROM favorites WHERE escort_id = ? AND rank = 2', $escort->escort_id);
			$top3 = self::db()->fetchOne('SELECT COUNT(id) FROM favorites WHERE escort_id = ? AND rank = 3', $escort->escort_id);
			$top10 = self::db()->fetchOne('SELECT COUNT(id) FROM favorites WHERE escort_id = ? AND rank > 0', $escort->escort_id);
			$comments = self::db()->fetchOne('
				SELECT COUNT(id) FROM favorites WHERE escort_id = ? AND LENGTH(comment) > 0 AND ((type = 1 AND comment_status <> 1) OR type = 2)
			', $escort->escort_id);
			
			$escorts[$k]->top1 = $top1;
			$escorts[$k]->top2 = $top2;
			$escorts[$k]->top3 = $top3;
			$escorts[$k]->top10 = $top10;
			$escorts[$k]->comments = $comments;
		}
		
		$items = array(
			'data'	=> $escorts,
			'count'	=> $count
		);
		
		return $items;
	}
	
	public function getByRanks($escort_id, $rank)
	{
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		if (is_array($rank)) $rank = reset($rank);
		
		if (in_array($rank, array(1, 2, 3)))
		{
			$users = self::db()->query('
				SELECT u.username, (SELECT COUNT(id) FROM favorites WHERE user_id = f.user_id AND rank > 0) AS total_count 
				FROM favorites f 
				INNER JOIN users u ON u.id = f.user_id 
				WHERE f.escort_id = ? AND f.rank = ?
			', array($escort_id, $rank))->fetchAll();
		}
		elseif ($rank == 10)
		{
			$users = self::db()->query('
				SELECT u.username, f.rank, (SELECT COUNT(id) FROM favorites WHERE user_id = f.user_id AND rank > 0) AS total_count 
				FROM favorites f 
				INNER JOIN users u ON u.id = f.user_id 
				WHERE f.escort_id = ? AND f.rank > 0
			', array($escort_id))->fetchAll();
		}
		
		return $users;
	}
	
	public function getCommentsByEscort($escort_id)
	{
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		
		return self::db()->query('
			SELECT u.username, f.comment, UNIX_TIMESTAMP(f.comment_date) as comment_date, f.type, f.comment_status, f.id AS f_id  
			FROM favorites f 
			INNER JOIN users u ON u.id = f.user_id 
			WHERE f.escort_id = ? AND LENGTH(f.`comment`) > 0 AND ((type = 1 AND comment_status <> 1) OR type = 2)
			ORDER BY f.comment_date DESC
		', array($escort_id))->fetchAll();
	}

	public function getCommentByEscortED($user_id, $escort_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		
		return self::db()->query('
			SELECT u.username, f.comment
			FROM favorites f 
			INNER JOIN users u ON u.id = f.user_id 
			WHERE f.escort_id = ? AND f.user_id = ?
		', array($escort_id, $user_id))->fetch();
	}
	
	public function getFavoriteRequestsByUser($user_id, $escort_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		
		return self::db()->query('
			SELECT fr.fav_id, fr.status 
			FROM favorite_requests fr 
			INNER JOIN favorites f ON f.id = fr.fav_id 
			WHERE fr.user_id = ? AND f.escort_id = ?
		', array($user_id, $escort_id))->fetchAll();
	}
	
	public function getEscortsTopOfUsers($escort_users)
	{
		$ret = array();
		
		foreach ($escort_users as $escort => $users)
		{
			$top_users = self::db()->query('SELECT user_id FROM favorites WHERE escort_id = ? AND user_id IN (' . $users . ') AND rank > 0', $escort)->fetchAll();
			
			if ($top_users)
			{
				foreach ($top_users as $t)
				{
					$ret[$escort][] = $t->user_id;
				}
			}
		}
		
		return $ret;
	}
	/* end favorites */
	
	public function allowVote($user_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		
		return self::db()->fetchOne('SELECT allow_vote FROM members WHERE user_id = ?', $user_id);
	}
	
	public function setAllowVote($user_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		
		self::db()->update('members', array('allow_vote' => 1), self::db()->quoteInto('user_id = ?', $user_id));
	}

	/*public function mig()
	{
		$escorts = self::db()->query('select escort_id, count(rank) as count from favorites GROUP BY escort_id having count > 0')->fetchAll();

		return $escorts;
	}*/

	public function getAdditionalInfo($user_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);

		$fields = '';

		if (in_array(Cubix_Application::getId(), array(APP_EF, APP_ED, APP_EG_CO_UK)))
			$fields = ', m.escorts_watched_type ';

		return self::db()->query('SELECT m.is_suspicious, u.status ' . $fields . ' FROM members m INNER JOIN users u ON u.id = m.user_id WHERE m.user_id = ?', $user_id)->fetch();
	}

	public function updateWatchedType($user_id, $type)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($type)) $type = reset($type);

		self::db()->update('members', array('escorts_watched_type' => $type), self::db()->quoteInto('user_id = ?', $user_id));
	}

	public function syncWatchedEscorts($insert)
	{
		if (is_array($insert)) $insert = reset($insert);

		try {
			self::db()->query($insert);
		} catch (Exception $e) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function searchMember($username, $user_id)
	{
		$sql = 'SELECT u.username as name, u.id
			 FROM users AS u
			 WHERE 
			 	u.status = ?
			 AND u.user_type = "member"	
			 AND u.username like ?
			 AND  u.id != ?
			 ORDER BY POSITION(? in u.username) ASC';
		
		try {
			$members = self::db()->fetchAll($sql, array(USER_STATUS_ACTIVE,'%'.$username.'%', $user_id,$username));
			return $members;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function follow($followed_id, $events, $follower_id, $escortId, $edit_mode){

		try {

			if(!$edit_mode){
				$alreadyFollowed = self::alreadyFollowed($followed_id,$follower_id);

				if($alreadyFollowed){
					return array('status' => 0 ,'error' => "alreadyFollowed");
				}
			}

			$data = array();
			$data['followed_id'] = $followed_id;	
			$data['follower_id'] = $follower_id;
			if($escortId){
				$data['escort_id'] = $escortId;
			}

			$data['comment'] = 0;
			$data['review'] = 0;
			$data['pin_entry'] = 0;
			$data['pin_response'] = 0;

			if(in_array('comment',$events))
				$data['comment'] = 1;

			if(in_array('review',$events))
				$data['review'] = 1;

			if(in_array('pin_entry',$events))
				$data['pin_entry'] = 1;

			if(in_array('pin_response',$events))
				$data['pin_response'] = 1;

			$data['created'] = date("Y-m-d H:m:s");

			if($edit_mode){

				$update = self::db()->update('follow', array(
					'comment' => $data['comment'],
					'review' => $data['review'],
					'pin_entry' => $data['pin_entry'],
					'pin_response' => $data['pin_response']
				), array(
					self::db()->quoteInto('followed_id = ?', $data['followed_id']),
					self::db()->quoteInto('follower_id = ?', $data['follower_id'])
				));

			}else{
				self::db()->insert('follow', $data);
				
			}
			
			return array('status' => 1);
			
		}
		catch ( Exception $e ) {
			return array('status' => 0, 'error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function alreadyFollowed($followed_id,$follower_id){
		$alreadyFollowed = self::db()->fetchRow('SELECT id FROM follow WHERE followed_id = ? AND follower_id = ?', array($followed_id, $follower_id));
		return $alreadyFollowed;
	}

	public function getFollowers($user_id, $type, $page, $per_page, $search){

		if($type == 'followed'){
			$sqlData = 'SELECT u.username as name, u.id, f.id as f_id, f.status, u.user_type
			 FROM follow AS f';
		}elseif($type == 'follower'){
			$sqlData = 'SELECT u.username as name, u.id, f.id as f_id, f.status, u.user_type, f.escort_id, es.showname
			 FROM follow AS f';
		}
		
		
		$sqlCount = 'SELECT COUNT(*) FROM follow AS f';

		$sql = '';
		if($type == 'followed'){
			$sql .=	' LEFT JOIN users u ON u.id = f.followed_id';
		}elseif($type == 'follower'){
			$sql .=	' LEFT JOIN users u ON u.id = f.follower_id ';
			$sql .=	' LEFT JOIN escorts es ON es.id = f.escort_id ';
		}

		$sql .=	' WHERE 
			 	u.status = ?';
		
		if($type == 'followed'){
			$sql .=	' AND  f.follower_id = ?';
		}elseif($type == 'follower'){
			$sql .=	' AND  f.followed_id = ?';
		}
		
		if($search){
			$sql .=	' AND u.username LIKE ?';
			$search_name = '%'.$search.'%';
		}	

		$sqlCount = $sqlCount.' '.$sql;
		
		$sql .= ' ORDER BY f.status ASC, f.created DESC
					LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;

		$sqlData = $sqlData.' '.$sql;

		try {
			if($search){
				$members = array(
					'data'	=> self::db()->fetchAll($sqlData, array(USER_STATUS_ACTIVE, $user_id, $search_name)),
					'count'	=> self::db()->fetchOne($sqlCount, array(USER_STATUS_ACTIVE, $user_id, $search_name))
				);
			}else{
				$members = array(
					'data'	=> self::db()->fetchAll($sqlData, array(USER_STATUS_ACTIVE, $user_id)),
					'count'	=> self::db()->fetchOne($sqlCount, array(USER_STATUS_ACTIVE, $user_id))
				);
			}
			return $members;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}	
	}

	public function unFollow($follow_id, $user_id){

		try {
			$result = self::db()->delete('follow', array(
				self::db()->quoteInto('id = ?', $follow_id), 
				self::db()->quoteInto('follower_id = ?', $user_id)
			));

			if($result){
				return array('status' => 1);
			}else{
				return array('status' => 0);
			}
		}
		catch ( Exception $e ) {
			return array('status' => 0);
		}
	}

	public function removeMyFollower($follow_id, $user_id){

		try {
			$result = self::db()->delete('follow', array(
				self::db()->quoteInto('id = ?', $follow_id), 
				self::db()->quoteInto('followed_id = ?', $user_id)
			));

			if($result){
				return array('status' => 1);
			}else{
				return array('status' => 0);
			}
		}
		catch ( Exception $e ) {
			return array('status' => 0);
		}
	}
	
	public function deny($follow_id, $user_id){

		try {
			$result = self::db()->delete('follow', array(
				self::db()->quoteInto('id = ?', $follow_id), 
				self::db()->quoteInto('followed_id = ?', $user_id)
			));

			if($result){
				return array('status' => 1);
			}else{
				return array('status' => 0);
			}
		}
		catch ( Exception $e ) {
			return array('status' => 0);
		}
	}

	public function approve($follow_id, $user_id){

		try {
			$result =  self::db()->query('UPDATE follow SET status = ? WHERE followed_id = ? AND id = ?', array(1, $user_id, $follow_id));

			if($result){
				return array('status' => 1);
			}else{
				return array('status' => 0);
			}
		}
		catch ( Exception $e ) {
			return array('status' => 0);
		}
	}

	public function getFollowCount($member_id){

		$sql = 'SELECT COUNT(*) as cnt
			 FROM follow
			 WHERE 
			 	followed_id = ? AND status = 1';

		try {
			$count = self::db()->fetchAll($sql, array($member_id));
			return $count[0]->cnt;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}	
	}

	public function getEvents($user_id, $followed_id){
		try {
			return self::db()->fetchRow('SELECT * FROM follow WHERE follower_id = ? AND followed_id = ?', array($user_id, $followed_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}	
	}
	
	public function addEuroLotteryVote($user_id, $escort_id){
		
		try {
			if (is_array($user_id)) $user_id = reset($user_id);
			if (is_array($escort_id)) $type = reset($escort_id);

			self::db()->update('members', array('euro_vote_for' => $escort_id), self::db()->quoteInto('user_id = ?', $user_id));
			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'Euro Lottery Vote Added'));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}		
	}
	
	public function getMemberStats($user_id){
		try {
			$stats = array(
				'reviews_count' => 0,
				'comment_count' => 0,
				'last_review_date' => null,
				'last_comment_date' => null
			);
			$stats['reviews_count'] = self::getReviewsCount($user_id);
			$stats['comment_count'] = self::getCommentsCount($user_id);
			$stats['last_review_date'] = self::getLastReviewDate($user_id);
			$stats['last_comment_date'] =  self::getLastCommentDate($user_id);
			return $stats;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function addLogo($id,$hash, $ext)
	{
		self::db()->update('members', array('hash' => $hash, 'ext' => $ext), self::db()->quoteInto('id = ?', $id));	
	}
	
	public function removeLogo($id)
	{
		self::db()->update('members', array('hash' => null, 'ext' => null), self::db()->quoteInto('id = ?', $id));
	}
	
	public function addTokenRequest($data)
	{
		$db = self::db();
		$db->insert('token_requests', $data);
    
		return $db->lastInsertId();
	}
	
	public function updateTokenRequest($id, $user_id, $tokens, $data)
	{
		$db = self::db();
		$db->update('token_requests', $data, self::db()->quoteInto('id = ?', $id));
		//$db->update('members', array('tokens' => new Zend_Db_Expr("tokens + $tokens ")), $db->quoteInto('user_id = ?', $user_id));
    
		return true;
	}
	
	public function checkStatus($id)
	{
		try {
			return self::db()->fetchOne('SELECT true FROM token_requests WHERE id = ? AND status <> 0', array($id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}	
    }
	
	public function checkVideoCallStatus($id)
	{
		try {
			return self::db()->fetchOne('SELECT true FROM video_chat_requests WHERE id = ? AND status <> 0', array($id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}	
    }

}
