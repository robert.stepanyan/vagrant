<?php

class Cubix_Api_Module_Xmas extends Cubix_Api_Module
{
	
	// OLD XMAS GAME LOGIC 
	/*public function setTermsAgree($user_id){
		try{
			self::db()->query("UPDATE users SET xmas_agree = 1 WHERE id = ?", array($user_id));
			} catch (Exception $e) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function getTermsAgree($user_id){
		return  self::db()->fetchOne("SELECT xmas_agree FROM users WHERE id = ?", array($user_id));
	}
	
	public function checkEscort($user_id,$escort_id)
	{
		try{
			$current_date = date("Y-m-d");
			self::_addVote($user_id, $current_date);
			$result = self::db()->fetchRow('SELECT id, win_code	FROM xmas_calendar WHERE date = ? AND win_id  = ?'
			, array($current_date,$escort_id));
			
			if($result){
				self::db()->query("UPDATE xmas_calendar SET checked = 1 WHERE id = ?", array($result->id));
				return $result->win_code;
			}
		} catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
		
		return false;
	}
	
	public function checkAgency($user_id)
	{
		try{
			$current_date = date("Y-m-d");
			self::_addVote($user_id, $current_date);
			$escorts = self::db()->fetchAll('SELECT id FROM escorts WHERE user_id = ?', array($user_id));
			$escort_ids = array();
			foreach ($escorts AS $escort){
					$escort_ids[] = $escort->id; 
			}
			
			$result = self::db()->fetchRow('SELECT id, win_code FROM xmas_calendar WHERE date = ? AND win_id IN ('.implode(',',$escort_ids). ') ', array( date("Y-m-d")) );
			
			if($result){
				self::db()->query("UPDATE xmas_calendar SET checked = 1 WHERE id = ?", array($result->id));
				return $result->win_code;
			}
		} catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
		return false;
	}
	
	public function getUserVotes($user_id, $escort_id = null)
	{	
		//NOT PUSHED 0 ; FORGOT 1 ; PUSHED 2;
		$votes_array = array();
		$votes = self::db()->fetchAll("SELECT DAYOFMONTH(vote_date) as vote_day FROM xmas_tries WHERE user_id = ?", array($user_id));
		
		$current_day = date('j');
		for($i = 1; $i <= 25; $i++ )
		{
			if($i >= $current_day){
				$votes_array[$i] = array( 'vote' => 0, 'win_code' => '' );
			}
			else{
				$votes_array[$i] = array( 'vote' => 1, 'win_code' => '' );
			}
		}
		
		foreach($votes as $vote){
			$votes_array[$vote->vote_day]['vote'] = 2;
		}
		
		if(is_null($escort_id)){
			$escorts = self::db()->fetchAll('SELECT id FROM escorts WHERE user_id = ?', array($user_id));
			$escort_ids = array();
			foreach ($escorts AS $escort){
					$escort_ids[] = $escort->id; 
			}
			if(count($escort_ids) > 0){
				$winers = self::db()->fetchAll('SELECT DAYOFMONTH(date) as vote_day, win_id, win_code FROM xmas_calendar WHERE win_id IN ('.implode(',',$escort_ids). ') ');
			}
			else{
				$winers = null;
			}
		}
		else{
			$winers = self::db()->fetchAll('SELECT DAYOFMONTH(date) as vote_day, win_id, win_code FROM xmas_calendar WHERE win_id = ?', array($escort_id) );
		}
		
		if(count($winers) > 0){
			foreach($winers as $win){
				$votes_array[$win->vote_day]['win_code'] = $win->win_code;
				
			}
		}
		
		return $votes_array;
	}
	
	private function _addVote($user_id, $date)
	{
		$dublicity = self::db()->fetchOne("SELECT TRUE FROM xmas_tries WHERE user_id = ? AND vote_date = ? ", array($user_id,$date));
		if(!$dublicity){
			self::db()->insert("xmas_tries", array("user_id" => $user_id, "vote_date" => $date));
		}
	}*/
	
	// OLD XMAS LOGIC END 
	
	public function check($user_id, $lang)
	{
		try{
			$exists = self::db()->fetchOne(" SELECT TRUE FROM xmas_logged_users WHERE user_id = ?", array($user_id));
			$was_winner = self::db()->fetchOne(" SELECT TRUE FROM xmas_winners WHERE user_id = ?", array($user_id));
			
			
			if($exists || $was_winner){
				return array('status' => 0);
			}

            $is_Diamond = false;
            if ( Cubix_Application::getId() == APP_EF ) {
                /*$hadPackage = self::_checkHadPackage($user_id);
                if(!$hadPackage){
                  return array('status' => 0);
                }*/
                $has_package = self::_checkHasPackage($user_id);
                if(!$has_package){
                    return array('status' => 0);
                }else{
                    $is_Diamond = self::_checkIsDiamond($user_id);
                }
            }

            $user_data = array(
                'user_id' => $user_id,
                'login_date' => new Zend_Db_Expr('NOW()')
            );


            self::db()->insert('xmas_logged_users', $user_data);


            if (!$is_Diamond)
            {
                self::_addLoginCount();
                $is_winner = self::_checkWinner($lang);
            }else{
                self::_addDiamondLoginCount();
                $is_winner = self::_checkDiamondWinner($lang);
            }
			
			if($is_winner){
				$user_data['win_try_id'] = $is_winner->id;
				self::db()->insert('xmas_winners', $user_data);
				return  array('status' => 2, 'text' => $is_winner->text);
			}
			else{
				return  array('status' => 1);
			}
		} catch (Exception $e) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
		
	private function _addLoginCount()
	{
		self::db()->query('UPDATE xmas_login_counts SET count = count + 1 WHERE id = ?', mt_rand(1,10));
	}

    private function _addDiamondLoginCount()
    {
        self::db()->query('UPDATE xmas_login_counts SET count = count + 1 WHERE id = ?', mt_rand(11,20));
    }

    private function _checkWinner($lang)
    {
        $sum = self::db()->fetchOne(" SELECT SUM(count) FROM xmas_login_counts WHERE id < 11");
        $is_winner = self::db()->fetchRow(" SELECT id, text_".$lang." AS text FROM xmas_win_tries WHERE date = CURDATE() AND  login_try = ? AND is_diamond = 0", array($sum));
        return $is_winner;
    }

    private function _checkDiamondWinner($lang)
    {
        $sum = self::db()->fetchOne(" SELECT SUM(count) FROM xmas_login_counts WHERE id >= 11");
        $is_winner = self::db()->fetchRow(" SELECT id, text_".$lang." AS text FROM xmas_win_tries WHERE date = CURDATE() AND  login_try = ? AND is_diamond = 1", array($sum));
        return $is_winner;
    }

    private function _checkHasPackage($user_id)
    {
        return self::db()->fetchOne("SELECT TRUE FROM escorts e
                INNER JOIN order_packages op ON op.escort_id = e.id 
                WHERE op.order_id IS NOT NULL AND op.status = ? AND e.user_id = ?", array(PACKAGE_STATUS_ACTIVE,$user_id));
    }

    private function _checkIsDiamond($user_id)
    {
        return self::db()->fetchOne("SELECT TRUE FROM escorts e
                INNER JOIN order_packages op ON op.escort_id = e.id 
                WHERE op.order_id IS NOT NULL AND op.status = ? AND e.user_id = ? AND op.package_id IN (101,102,121,124,145,156,183,186) ", array(PACKAGE_STATUS_ACTIVE,$user_id));
    }
	
	private function _checkHadPackage($user_id)
	{
		return self::db()->fetchOne("SELECT true FROM escorts e
				INNER JOIN order_packages ep ON e.id = ep.escort_id 
				WHERE ep.base_price <> 0 AND DATE_ADD(ep.activation_date, INTERVAL 6 MONTH) >= DATE(NOW()) AND e.user_id = ? ", array($user_id));

	}

    public function setPopupShowed($user_id)
    {
        if (!$this->getPopupShowed($user_id)) {
            self::db()->insert('xmas_showed_popup', array('user_id' => $user_id));
        }
    }

    public function getPopupShowed($user_id)
    {
        return self::db()->fetchOne(" SELECT TRUE FROM xmas_showed_popup WHERE user_id = ?", array($user_id));
    }
}
