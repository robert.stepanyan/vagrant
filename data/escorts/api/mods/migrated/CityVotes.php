<?php

class Cubix_Api_Module_CityVotes extends Cubix_Api_Module
{
    public function save( $user_id, $user_type, $city_id ){
        /*if( $this->_getMonthCount( $user_id ) )
            return '>3';*/
        if( !self::_getMonthCount( $user_id ) ){
            return array( 'note_number' => '1', 'date' => self::_getVoteExpiredLimit() );
        }

        if( !$city_id ) return;

        if ( is_array($user_id) ) $user_id = reset( $user_id );
        if ( is_array($user_type) ) $user_type = reset( $user_type );
        if ( is_array($city_id) ) $city_id = reset( $city_id );

        self::db()->insert( 'city_votes_records', array(
            'user_id' => $user_id,
            'user_type' => $user_type,
            'city_id' => $city_id,
            'vote_date' => new Zend_Db_Expr('NOW()')
        ));
    }

    public function getIfExpired( $user_id ){
        if( !self::_getMonthCount( $user_id ) ){
            return array( 'note_number' => '1', 'date' => self::_getVoteExpiredLimit() );
        }
    }

    public function getData( $lang, $page ){
        $perPage = 5;
        $offset = ( $page - 1 ) * $perPage;

        return self::db()->query('
            SELECT
                c.title_' . $lang . ' AS city,
                COUNT( cvr.city_id ) AS city_count,
                SUM( if( cvr.user_type = "escort", 1, if( cvr.user_type = "agency", 1,  0 ) ) ) AS votesL,
                SUM( if( cvr.user_type = "member", 1, 0) ) AS votesR,
                cvr.city_id AS city_id
            FROM city_votes_records cvr
            LEFT JOIN cities c ON c.id = cvr.city_id
            WHERE MONTH( vote_date ) = ' . date('m') . ' AND YEAR( vote_date ) = ' . date('Y') . '
            GROUP BY cvr.city_id
            ORDER BY ( SUM( if( cvr.user_type = "escort", 1, if( cvr.user_type = "agency", 1,  0 ) ) ) + SUM( if( cvr.user_type = "member", 1, 0) ) ) DESC
            LIMIT ' . $offset . ', ' . $perPage
        )->fetchAll();
        // COUNT(cvr.user_type) AS city_votes,
    }

    public function getCitiesCount(){

        return self::db()->query('
            SELECT
                COUNT( DISTINCT city_id ) AS count
            FROM city_votes_records
            WHERE MONTH( vote_date ) = ' . date('m') . ''
        )->fetch();
    }

    public function getUsersByCityId( $city_id ){
        return self::db()->query( 'SELECT distinct user_id
        FROM city_votes_records
        WHERE city_id = ? AND user_type = ? AND MONTH( vote_date ) = ? AND YEAR( vote_date ) = ?',
            array( $city_id, "escort", date('m'), date('Y') ) )->fetchAll();
    }

    private function _getMonthCount( $user_id ){
        $count = self::db()->query( 'SELECT COUNT(*)
        AS vote_count
        FROM city_votes_records
        WHERE user_id = ? AND MONTH( vote_date ) = ? AND YEAR( vote_date ) = ?',
            array( $user_id, date('m'), date('Y') ) )->fetch();

        $count = $count->vote_count;

        if ( $count < 0 || $count > 2 ) return false;
        return true;
    }

    private function _getVoteExpiredLimit(){
        $curr = time();
        $last = mktime( 23,59,59, date('m') + 1 , 0, date('Y') );
        $seconds = $last - $curr;
        return self::_secondsToDatePartly( $seconds );
    }

    private function _secondsToDatePartly( $secs ){
        $r = array();

        if( $secs>=86400 ){ $r['days'] = floor($secs/86400); $secs=$secs%86400; }
        if( $secs>=3600 ){ $r['hours'] = floor($secs/3600); $secs=$secs%3600; }
        if( $secs>=60 ){ $r['minutes'] = floor($secs/60); $secs=$secs%60; }

        return $r;
    }
}