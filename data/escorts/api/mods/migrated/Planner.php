<?php

class Cubix_Api_Module_Planner extends Cubix_Api_Module
{	
	public function add($data)
	{		
		self::db()->insert('planner', $data);
	}
	
	public function getEvents($user_id, $escort_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($escort_id)) $escort_id = reset($escort_id);

		if ($escort_id)
		{
			$where = ' escort_id = ' . $escort_id;
		}
		else
		{
			$where = ' user_id = ' . $user_id;
		}
		
		return self::db()->query('SELECT date, type FROM planner WHERE ' . $where . ' ORDER BY date ASC')->fetchAll();
	}
	
	public function getCurrentMonthEventsInfo($user_id, $escort_id, $date)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		if (is_array($date)) $date = reset($date);
		
		if ($escort_id)
		{
			$where = ' escort_id = ' . $escort_id;
		}
		else
		{
			$where = ' user_id = ' . $user_id;
		}
		
		if (is_null($date))
		{
			$m = date('m');
			$y = date('Y');
		}
		else
		{
			$parts = explode('-', $date);
			
			$m = strlen($parts[0]) == 1 ? '0' . $parts[0] : $parts[0];
			$y = $parts[1];
		}
		
		$info = self::db()->query('SELECT date, text, escort_id FROM planner WHERE MONTH(date) = ? AND YEAR(date) = ? AND ' . $where . ' ORDER BY date ASC', array($m, $y))->fetchAll();
		$ret = array();
		$ret2 = array();
		
		if ($info)
		{
			foreach ($info as $i)
			{
				$d = intval(date("d", strtotime($i->date)));
				
				if (!isset($ret[$d]))
					$ret[$d] = $i;
				
				if (!isset($ret[$d]))
					$ret2[$d] = 1;
				else
					$ret2[$d] += 1;
			}
		}
		
		return array('items' => $ret, 'counts' => $ret2);
	}
	
	public function getEventsInfo($date, $user_id, $escort_id)
	{
		if (is_array($date)) $date = reset($date);
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		
		if ($escort_id)
		{
			$where = ' escort_id = ' . $escort_id;
		}
		else
		{
			$where = ' user_id = ' . $user_id;
		}
		
		return self::db()->query('SELECT id, date, text, alert FROM planner WHERE DATE(date) = ? AND ' . $where . ' ORDER BY date ASC', $date)->fetchAll();
	}
	
	public function isOwner($user_id, $id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($id)) $id = reset($id);
		
		return self::db()->fetchOne('SELECT TRUE FROM planner WHERE user_id = ? AND id = ?', array($user_id, $id));
	}
	
	public function remove($id)
	{
		if (is_array($id)) $id = reset($id);
		
		self::db()->delete('planner', self::db()->quoteInto('id = ?', $id));
	}
	
	public function get($id)
	{
		if (is_array($id)) $id = reset($id);
		
		return self::db()->query('SELECT * FROM planner WHERE id = ?', $id)->fetch();
	}
	
	public function edit($data)
	{
		$id = $data['id'];
		unset($data['id']);
		
		self::db()->update('planner', $data, self::db()->quoteInto('id = ?', $id));
	}

	public function getAgencyEscorts($agency_id)
	{
		if (is_array($agency_id)) $agency_id = reset($agency_id);

		return self::db()->query('
			SELECT id, showname FROM escorts WHERE agency_id = ? AND ((status & ?) OR (status & ?) OR (status & ?) OR (status & ?)) ORDER BY showname
		', array(
			$agency_id,
			Cubix_EscortStatus::ESCORT_STATUS_ACTIVE,
			Cubix_EscortStatus::ESCORT_STATUS_NO_ENOUGH_PHOTOS,
			Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED,
			Cubix_EscortStatus::ESCORT_STATUS_PROFILE_CHANGED
		))->fetchAll();
	}

	public function isOwnerAgency($escort_id, $agency_id)
	{
		if (is_array($escort_id)) $escort_id = reset($escort_id);
		if (is_array($agency_id)) $agency_id = reset($agency_id);

		return self::db()->fetchOne('SELECT TRUE FROM escorts WHERE id = ? AND agency_id = ?', array($escort_id, $agency_id));
	}
}
