<?php

class Cubix_Api_Module_Application extends Cubix_Api_Module
{
	public function isDomainBlacklisted($domain)
	{
		$db = Zend_Registry::get('db');
		
		$sql = "SELECT 1 FROM blacklisted_domains WHERE ( ? REGEXP CONCAT('^([a-z]+://)?[^/]*', domain, '(.*)$') ) OR ( ? REGEXP CONCAT('', domain, '$') ) OR ( ? REGEXP CONCAT('([a-z]+://)?[^/]*', domain, '') )";
		
		return (boolean) $db->query($sql, array($domain, $domain, $domain))->fetch();
	}
	
	public function getLangs()
	{
		return Cubix_Application::getLangs(self::app()->id);
	}
	
	public function getDictionary($lang_ids)
	{
		$fields = array();
		foreach ( $lang_ids as $id ) {
			$fields[] = 'value_' . $id;
		}
		
		$dic = self::db()->fetchAll('
			SELECT id, ' . implode(', ', $fields) . ' FROM dictionary WHERE application_id IS NULL
		', array(), Zend_Db::FETCH_ASSOC);
		
		foreach ( $dic as $i => $row ) {
			unset($dic[$i]);
			$dic[$row['id']] = $row;
		}
		
		$diff_dic = self::db()->fetchAll('
			SELECT id, ' . implode(', ', $fields) . ' FROM dictionary WHERE application_id = ?
		', array(self::app()->id), Zend_Db::FETCH_ASSOC);
		
		foreach ( $diff_dic as $row ) {
			foreach ( $row as $field => $value ) {
				if ( 'id' == $field ) continue;
				if ( ! is_null($value) ) {
					$dic[$row['id']][$field] = $value;
				}
			}
		}
		
		return $dic;
	}

	public function getEmailTemplates($app_id)
	{
		$sql = '
			SELECT *
			FROM email_templates
			WHERE application_id = ?
		';

		return self::db()->fetchAll($sql, $app_id);
	}

	public function getGeography($section)
	{		
		$data = self::db()->fetchAll('
			SELECT * FROM ' . $section . '
		');
		
		return $data;
	}

	public function getCities()
	{		
		$data = self::db()->fetchAll('SELECT ci.id, ci.title_geoip, longitude, latitude, country_id, iso
			FROM cities ci INNER JOIN countries co on co.id = ci.country_id
		');
		
		return $data;
	}

	public function getClosestCities($city, $distance)
	{			

		$sql = "SELECT id, ((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(".$city['latitude']." - latitude))/2), 2) +
							COS(RADIANS(latitude)) *
							COS(RADIANS(".$city['latitude'].")) *
							POWER(SIN((RADIANS(".$city['longitude']." - longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(".$city['latitude']." - latitude))/2), 2) +
							COS(RADIANS(latitude)) *
							COS(RADIANS(".$city['latitude'].")) *
							POWER(SIN((RADIANS(".$city['longitude']." - longitude))/2), 2)
						))
						)
					)) AS distance
				FROM cities 
				having distance < ". $distance;
		return self::db()->fetchAll($sql);
	}


	public function getRedirectPage($url, $referer)
	{
		
		$row = self::db()->fetchRow('
			SELECT id, redirect_page
			FROM redirect_pages
			WHERE url = ?
		', array($url));

		if ( ! $row ) {
			self::db()->insert('redirect_pages', array(
				'url'	=> $url,
				'referer'	=> $referer,
				'hits_count'	=> 1,
				'date'	=> new Zend_Db_Expr('NOW()'),
				'application_id'	=> Cubix_Application::getId()
			));
			return false;
		} else {
			self::db()->update('redirect_pages', array('hits_count' => new Zend_Db_Expr('hits_count + 1'), 'date' => new Zend_Db_Expr('NOW()')), array( self::db()->quoteInto('id = ?', $row->id)));
		}
		
		return $row->redirect_page;
	}

	public function saveImageV2($data)
	{		
		try {
            self::db()->insert('images', $data);
			$image_id = self::db()->lastInsertId();
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
		
		return $image_id;
	}

	public function saveCoverPic($data)
	{		
		try {
            $n = self::db()->update('video', array('cover_hash' => $data['cover_hash'], 'cover_ext' => $data['cover_ext'], 'cover_status' => $data['cover_status']), array( self::db()->quoteInto('escort_id = ?', $data['escort_id']), self::db()->quoteInto('id = ?', $data['video_id'])));
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
		
		return $n;
	}

	public function deleteCoverPic($data)
	{
		try {
			$n = self::db()->update('video', array('cover_hash' => null, 'cover_ext' => null, 'cover_status' => null), array( self::db()->quoteInto('escort_id = ?', $data['escort_id']), self::db()->quoteInto('id = ?', $data['video_id'])));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $n;
	}
	
	public function saveVideoV2($data)
	{		
		try {
		
            self::db()->insert('video', $data);
			$image_id = self::db()->lastInsertId();
			if(is_numeric($image_id))
			Cubix_SyncNotifier::notify($data['escort_id'],Cubix_SyncNotifier::EVENT_ESCORT_VIDEO_ADDED);
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
		
		return $image_id;
	}
	public function saveVideoImageV2($data)
	{		
		try {
            self::db()->insert('video_image', $data);
			$image_id = self::db()->lastInsertId();
        } catch (Exception $e) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
		
		return $image_id;
	}
	
	public function UserVideoCount($escort_id)
	{
		$escort_id = self::db()->quote($escort_id);
		return self::db()->fetchAll("SELECT count(id) AS quantitiy,`status` FROM video WHERE escort_id=$escort_id GROUP BY escort_id,`status` ORDER by `status` ");
	}
	
	public function getPendingVideo($escort_id)
	{
		return self::db()->fetchRow("SELECT v.id, v.video, v.height, v.width, i.hash as image_hash, i.ext as image_ext FROM video v
								INNER JOIN video_image vi ON vi.video_id = v.id
								INNER JOIN images i ON i.id = vi.image_id
								WHERE v.escort_id = ? AND v.status = 'pending' LIMIT 1", array($escort_id));
	}
	
	public function getVideoIds($escort_id)
	{
		return self::db()->fetchAll("SELECT id FROM video WHERE escort_id = ?",array($escort_id));
	}	
	
	public function RemoveVideo($id,$videos)
	{
		self::db()->query("DELETE FROM video WHERE id IN($videos) AND escort_id=$id ");
		self::db()->query("DELETE FROM images WHERE id IN(SELECT image_id FROM video_image WHERE video_id IN($videos))");
		self::db()->query("DELETE FROM video_image WHERE video_id IN($videos)");
		
		$id = str_replace("'",'', $id);
		Cubix_SyncNotifier::notify($id,Cubix_SyncNotifier::EVENT_ESCORT_VIDEO_DELETED);
		return 1;
	}
	
	public function GetVideo($escort_id)
	{
		try{
			$sql_video = "SELECT * FROM video WHERE escort_id= ? ORDER BY `date` DESC ";
			$video = self::db()->fetchRow($sql_video, array($escort_id));

			if(!empty($video))
			{
				$sql_image = " SELECT i.hash, i.ext, i.width, i.height FROM video_image vi 
							INNER JOIN images i ON vi.image_id = i.id 
							WHERE vi.video_id = ? 
							ORDER BY id DESC LIMIT 1";
				
				$photo = self::$_db->fetchRow($sql_image,$video->id );
				$photo->application_id = Cubix_Application::getId();
				$video->photo = $photo; 

			}
			return $video;

		} catch (Exception $e) {
          
			return array('error' => Cubix_Api_Error::Exception2Array($e));
			
        }
	}
	
	public function GetVideos($escort_id,$limit)
	{		
		$sql_video = "SELECT * FROM video WHERE escort_id=$escort_id  ORDER BY `date` DESC LIMIT 0,$limit";
				
		$video = self::db()->fetchAll($sql_video);
		$photo = array();
		if(!empty($video))
		{
			$v_id = '';
			foreach ($video as $v)
			{
				$v_id.="'".$v->id."',";
			}
			$v_id = substr($v_id,0,strlen($v_id)-1);
		
			$sql_image = "SELECT * FROM images JOIN (SELECT * FROM video_image WHERE video_id IN($v_id)) AS v ON images.id=v.image_id";
			$photo =  self::db()->fetchAll($sql_image);
		}
		return array($photo,$video);
	}
	
	public function GetVideosV2($escort_id,$limit)
	{
		$sql_video = "SELECT * FROM video WHERE escort_id=$escort_id  ORDER BY `date` DESC LIMIT 0,$limit";
		$video = self::db()->fetchAll($sql_video);
				
		if(!empty($video))
		{
			$sql_image = "SELECT i.hash, i.ext, i.width, i.height FROM video_image vi 
						  INNER JOIN images i ON vi.image_id = i.id 
						  WHERE vi.video_id = ? ";
			
			foreach ($video as &$v)
			{
				$v->photo = self::$_db->fetchRow($sql_image,$v->id );
			}
		}
		
		return $video;
	}
	
	public function GetAgencyEscortVideo($agency_id,$escrots)
	{	
		foreach ($escrots as &$esc)
		{	
			$vcount =  self::db()->fetchRow("SELECT count(id) AS quantity FROM video WHERE escort_id= ? GROUP BY escort_id", array($esc->id));
			$esc->vcount = $vcount->quantity;
			
		}
		 return $escrots;
	}
	public function GetEscortVideoCount($escort_id)
	{
        $vcount =  self::db()->fetchRow("SELECT count(id) AS quantity FROM video WHERE escort_id=$escort_id");
        return $vcount->quantity;
	}
	public function SaveAnalytic($insert)
	{	
		try {
            self::db()->query($insert);
        } catch (Exception $e) {
          
			return array('error' => Cubix_Api_Error::Exception2Array($e));
			
        }
	}

	public function geGlossary()
	{
		$sql = '
			SELECT *
			FROM glossary
		';

		return self::db()->fetchAll($sql);
	}

	public function subscriberLog($email)
	{
		$row = self::db()->fetchRow('
			SELECT email
			FROM subscriber_log
			WHERE email = ?
		', array($email));

		if ( ! $row ) {
			self::db()->insert('subscriber_log', array(
				'email'	=> $email,
				'subscription_date'	=> new Zend_Db_Expr('NOW()')
			));
		}
		return false;
	}

	public function GetEscortHasApprovedVideo($escort_id)
	{
		$sql_video = "SELECT true FROM video WHERE escort_id= ? and status = 'approve'";
		$video = self::db()->fetchOne($sql_video, array($escort_id));
		return $video;
		
	}

    function getActivePaymentMethodes($app_id) {
        return self::db()->fetchAll("SELECT title, slug FROM `payment_methodes` WHERE application_id = ? AND `status` = 1", array($app_id));
    }

    public function checkTableExist($table_name)
    {
        $sql = "SELECT TRUE
                FROM information_schema.tables 
                WHERE table_schema = DATABASE()
                AND table_name = ?";

        return self::db()->fetchOne($sql, array($table_name));
    }
}
	