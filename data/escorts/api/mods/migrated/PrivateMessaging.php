<?php
class Cubix_Api_Module_PrivateMessaging extends Cubix_Api_Module 
{
	
	const ESCORT_STATUS_ACTIVE = 32;
	const PACKAGE_STATUS_ACTIVE = 2;
	
	const PM_STATUS_NOT_CONFIRMED = 1;
	const PM_STATUS_CONFIRMED = 2;
	
	public static function sendMessage($participant_type_from, $participant_type_to, $from_id, $to_id, $body, $for_paids_only = true, $check_blw = true ) 
	{
		if ( $participant_type_from == $participant_type_to && $from_id == $to_id ) return;
		
		$participant_type_from = is_array($participant_type_from) ? reset($participant_type_from) : $participant_type_from;
		$participant_type_to = is_array($participant_type_to) ? reset($participant_type_to) : $participant_type_to;
		$from_id = is_array($from_id) ? reset($from_id) : $from_id;
		$to_id = is_array($to_id) ? reset($to_id) : $to_id;
		$body = is_array($body) ? reset($body) : $body;
		$for_paids_only = is_array($for_paids_only) ? reset($for_paids_only) : $for_paids_only;
		
		if ( $participant_type_to == 'member' ) {
			$is_active = self::db()->fetchOne('SELECT TRUE FROM users WHERE id = ? AND status = 1', array($to_id));
			if ( ! $is_active ) {
				return;
			}
		}
		
		if ( $for_paids_only && $participant_type_to == 'escort'  ) { //Message can be sent only to active escorts
			
			$sql = '
				SELECT TRUE
				FROM escorts e
				INNER JOIN users u ON u.id = e.user_id
				INNER JOIN order_packages op ON op.escort_id = e.id
				WHERE 
					u.application_id = ? AND e.id = ? AND e.status = ? AND op.status = ? AND op.order_id IS NOT NULL
			';
			$is_active = self::db()->fetchOne($sql, array(self::app()->id, $to_id, self::ESCORT_STATUS_ACTIVE, self::PACKAGE_STATUS_ACTIVE));
			if ( ! $is_active ) {
				return;
			}
		}
		
		
		try {
			self::db()->beginTransaction();
			$thread_id = self::getThreadId($participant_type_from, $participant_type_to, $from_id, $to_id);
			$field_from = self::getIdentityField($participant_type_from);
			$field_to = self::getIdentityField($participant_type_to);
			
			$status = self::PM_STATUS_CONFIRMED;
			$flag = PM_FLAG_SENT;
			
			if ($check_blw && self::checkForBlacklistedWords($body) ) {
				$status = self::PM_STATUS_NOT_CONFIRMED;
				$flag = PM_FLAG_BLOCKED_BY_WORD;
			}
			
			//blocked by another participant
			$part_is_blocked = self::db()->fetchOne('SELECT is_blocked FROM threads_participants WHERE thread_id = ? AND '. $field_to . ' = ? ', array($thread_id, $to_id));
			if ( $part_is_blocked ) {
				$status = PM_STATUS_NOT_CONFIRMED;
				$flag = PM_FLAG_BLOCKED_BY_USER;
			}
			
			
			if ( $participant_type_from == 'member' ) {
				$is_blocked = self::db()->fetchOne('SELECT pm_is_blocked FROM users WHERE id = ?', array($from_id));
				if ( $is_blocked ) {
					$status = PM_STATUS_NOT_CONFIRMED;
					$flag = PM_FLAG_BLOCKED_BY_USER;
				}
			}
			
			self::db()->insert('messages', array(
				'thread_id' => $thread_id, 
				'body' => $body, 
				'date' => date('Y-m-d H:i:s'),
				'status' => $status,
				'flag'	=> $flag,
				$field_from => $from_id
			));
			$message_id = self::db()->lastInsertId();

			//Updating last message id and date
			if ( $status == self::PM_STATUS_CONFIRMED ) {
				self::db()->update('threads', array('last_message_id' => $message_id, 'last_message_date' => date('Y-m-d H:i:s')), self::db()->quoteInto('id = ?', $thread_id));
				self::db()->query('UPDATE threads_participants SET has_unread_message = 1, is_deleted = 0 WHERE ' . $field_to . '= ? AND thread_id = ?', array($to_id, $thread_id));
				
				//If sender remove thread recover it
				self::db()->query('UPDATE threads_participants SET is_deleted = 0 WHERE ' . $field_from . '= ? AND thread_id = ?', array($from_id, $thread_id));
			} else {
				self::db()->update('threads', array(/*'last_message_id' => $message_id,*/ 'last_message_date' => date('Y-m-d H:i:s')), self::db()->quoteInto('id = ?', $thread_id));
				self::db()->query('UPDATE threads_participants SET has_unread_message = 0, is_deleted = 0 WHERE ' . $field_to . '= ? AND thread_id = ?', array($to_id, $thread_id));
			}
			
			self::db()->commit();
		} catch( Exception $ex ) {
			self::db()->rollBack();
			throw $ex;
		}
		return true;
	}
	
	public function sendMessages($participant_type_from, $from_id, $participants_to, $body)
	{		
		foreach($participants_to as $participant_to) {
			self::sendMessage($participant_type_from, $participant_to['type'], $from_id, $participant_to['id'], $body);
		}
		
		return true;
	}
	
	public function getThreads($type, $id, $page = null, $per_page = null, $escort_id = null)
	{
		if ( $type == 'agency' ) {
			$sql = '
				SELECT SQL_CALC_FOUND_ROWS 
				t.id, t.last_message_date, tp1.has_unread_message, m.body, e2.showname AS escort_showname, e1.showname AS self_escort_showname, e1.id AS self_escort_id, u.username AS member_username, tp2.user_id, tp2.escort_id,
				if (ac.contact_escort_id OR ac.contact_user_id, 1, 0 ) AS is_in_contacts, tp1.is_blocked
				FROM threads t 
				INNER JOIN threads_participants tp1 ON tp1.thread_id = t.id
				INNER JOIN escorts e1 ON e1.id = tp1.escort_id AND e1.agency_id = ?
				INNER JOIN threads_participants tp2 ON tp2.thread_id = tp1.thread_id AND (tp2.escort_id <> e1.id OR tp2.escort_id IS NULL)
				INNER JOIN messages m ON m.id = t.last_message_id
				LEFT JOIN escorts e2 ON e2.id = tp2.escort_id

				LEFT JOIN users u ON u.id = tp2.user_id
				LEFT JOIN agency_contacts ac ON (ac.contact_escort_id = tp2.escort_id  OR ac.contact_user_id = tp2.user_id ) AND ac.agency_id = ?
				WHERE tp1.is_deleted = 0 /*AND tp1.is_blocked = 0*/ ' . ( $escort_id ? ' AND tp1.escort_id = ' . $escort_id : '') . '
				ORDER BY tp1.has_unread_message DESC, t.last_message_date DESC
			';
			
			$bind = array($id, $id);
		} else {
			$field = self::getIdentityField($type);
			$sql = '
				SELECT SQL_CALC_FOUND_ROWS 
					t.id, t.last_message_date, tp1.has_unread_message, m.body, e.showname AS escort_showname, e2.showname AS self_escort_showname, u.username AS member_username, tp2.user_id, tp2.escort_id,
					if (pc.contact_escort_id OR pc.contact_user_id, 1, 0 ) AS is_in_contacts, tp1.is_blocked
				FROM threads t 
				INNER JOIN threads_participants tp1 ON tp1.thread_id = t.id AND tp1.' . $field  . ' = ?
				INNER JOIN threads_participants tp2 ON tp2.thread_id = tp1.thread_id AND (tp2. ' . $field . ' <> ? OR tp2. ' . $field . ' IS NULL)
				INNER JOIN messages m ON m.id = t.last_message_id
				LEFT JOIN escorts e ON e.id = tp2.escort_id
				LEFT JOIN escorts e2 ON e2.id = tp1.escort_id
				LEFT JOIN users u ON u.id = tp2.user_id
				LEFT JOIN participants_contacts pc ON (pc.contact_escort_id = tp2.escort_id  OR pc.contact_user_id = tp2.user_id ) AND pc.' . $field . ' = ?
				WHERE tp1.is_deleted = 0 /*AND tp1.is_blocked = 0*/
				ORDER BY tp1.has_unread_message DESC, t.last_message_date DESC
			';
			$bind = array($id, $id, $id);
		}
		if ( $page && $per_page ) {
			$sql .= 'LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
		}
		$data = self::db()->fetchAll($sql, $bind);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		
		return array('data' => $data, 'count' => $count);
	}
	
	public function getThread($id, $type, $participant_id, $page = null, $per_page = null, $order = 'desc')
	{		
		$field = self::getIdentityField($type);
		
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS m.body, UNIX_TIMESTAMP(m.date) AS date, UNIX_TIMESTAMP(m.read_date) AS read_date, m.user_id, m.escort_id
			FROM threads t
			INNER JOIN threads_participants tp ON tp.thread_id = t.id AND tp.' . $field . ' = ?
			INNER JOIN messages m ON m.thread_id = t.id
			WHERE t.id = ? AND (m.status = ? OR m.' . $field . ' = ? )
			ORDER BY m.date ' . $order ;
		
		if ( $page && $per_page ) {
			$sql .= ' LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
		}
		
		$data =  self::db()->fetchAll($sql, array($participant_id, $id, self::PM_STATUS_CONFIRMED, $participant_id));
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		self::db()->query('UPDATE threads_participants SET has_unread_message = 0 WHERE ' . $field . '= ? AND thread_id = ?', array($participant_id, $id));
		self::db()->query('UPDATE messages SET read_date = NOW() WHERE thread_id = ? AND read_date IS NULL AND status = ?', array($id, self::PM_STATUS_CONFIRMED));
		
		return array('data' => $data, 'count' => $count);
	}
	
	//Getting second participant
	public function getThreadParticipant($id, $type, $participant_id)
	{		
		$field = self::getIdentityField($type);
		
		$sql = '
			SELECT e.id AS escort_id, e.showname AS escort_showname, u.id AS user_id, u.username AS member_username, u.status
			FROM threads t
			INNER JOIN threads_participants tp ON tp.thread_id = t.id AND (tp. ' . $field . ' <> ? OR tp.' . $field . ' IS NULL)
			LEFT JOIN escorts e ON tp.escort_id = e.id
			LEFT JOIN users u ON u.id = tp.user_id
			WHERE t.id = ?
		';
		return self::db()->fetchRow($sql, array($participant_id, $id));
	}
	
	public function removeThread($id, $type, $participant_id)
	{		
		$field = self::getIdentityField($type);
        if( $field == 'agency_id' && Cubix_Application::getId() == APP_ED ) $field = ' escort_agency_id OR agency_id ';
		self::db()->query('UPDATE threads_participants SET is_deleted = 1 WHERE ' . $field . '= ? AND thread_id = ?', array($participant_id, $id));
		
		return true;
	}
	
	public function blockThread($id, $type, $participant_id, $a_escort_id = null)
	{	
		
		if ( $type == 'agency' ) {
			self::db()->query('UPDATE threads_participants SET is_blocked = 1 WHERE escort_id = ? AND thread_id = ?', array($a_escort_id, $id));
			
			//Getting blocked escort to remove from agency_contacts
			$p_id = self::db()->fetchOne('SELECT escort_id FROM threads_participants WHERE thread_id = ? AND escort_id <> ?', array($id, $a_escort_id));
			self::db()->query('DELETE FROM agency_contacts WHERE contact_escort_id = ?', array($p_id));
			
		} else {
			$field = self::getIdentityField($type);
			self::db()->query('UPDATE threads_participants SET is_blocked = 1 WHERE ' . $field . '= ? AND thread_id = ?', array($participant_id, $id));
			
			//Getting blocked escort to remove from participants_contacts
			$p_id = self::db()->fetchOne('SELECT escort_id FROM threads_participants WHERE thread_id = ? AND ' . $field . ' <> ?', array($id, $participant_id));
			self::db()->query('DELETE FROM participants_contacts WHERE contact_escort_id = ?', array($p_id));
		}

		return true;
	}
	
	public function unblockThread($id, $type, $participant_id, $a_escort_id = null)
	{	
		
		if ( $type == 'agency' ) {
			self::db()->query('UPDATE threads_participants SET is_blocked = 0 WHERE escort_id = ? AND thread_id = ?', array($a_escort_id, $id));
			
			//Getting blocked escort to remove from agency_contacts
			//$p_id = self::db()->fetchOne('SELECT escort_id FROM threads_participants WHERE thread_id = ? AND escort_id <> ?', array($id, $a_escort_id));
			//self::db()->query('DELETE FROM agency_contacts WHERE contact_escort_id = ?', array($p_id));
			
		} else {
			$field = self::getIdentityField($type);
			self::db()->query('UPDATE threads_participants SET is_blocked = 0 WHERE ' . $field . '= ? AND thread_id = ?', array($participant_id, $id));
			
			//Getting blocked escort to remove from participants_contacts
			//$p_id = self::db()->fetchOne('SELECT escort_id FROM threads_participants WHERE thread_id = ? AND ' . $field . ' <> ?', array($id, $participant_id));
			//self::db()->query('DELETE FROM participants_contacts WHERE contact_escort_id = ?', array($p_id));
		}

		return true;
	}
	
	public function removeContact($id, $type, $participant_id, $participant_type)
	{		
		$participant_field = self::getIdentityField($participant_type);
		if ( $type == 'agency' ) {
			self::db()->query('DELETE FROM agency_contacts WHERE agency_id = ? AND contact_' . $participant_field . ' = ?', array($id, $participant_id));
		} else {
			$field = self::getIdentityField($type);

			self::db()->query('DELETE FROM participants_contacts WHERE ' . $field . '= ? AND contact_' . $participant_field . ' = ?', array($id, $participant_id));
		}
		
		return true;
	}
	
	public function addContact($id, $type, $participant_id, $participant_type)
	{		
		$participant_field = self::getIdentityField($participant_type);
		
		if ( $type == 'agency' ) {
			$count = self::db()->fetchOne('SELECT count(*) FROM agency_contacts WHERE agency_id = ?', array($id));
			if ( $count >= 10 ) {
				die;
			}
			self::db()->insert('agency_contacts', array('agency_id' => $id, 'contact_' . $participant_field => $participant_id));
		} else {
			$field = self::getIdentityField($type);
			
			$count = self::db()->fetchOne('SELECT count(*) FROM participants_contacts WHERE ' . $field . ' = ?', array($id));
			if ( $count >= 10 ) {
				die;
			}
			self::db()->insert('participants_contacts', array($field => $id, 'contact_' . $participant_field => $participant_id));
		}
		
		return true;
	}
	
	public function getParticipants($search, $type, $id, $count = 10)
	{		
		$members_sql = '
			SELECT u.id, u.username
			FROM members m
			INNER JOIN users u ON u.id = m.user_id
			WHERE status = 1 AND u.username LIKE ? ' . ( $type == 'member' ? ' AND u.id <> ' . $id : '' ) . '
			ORDER BY u.username
			LIMIT ?
		';
		$members = self::db()->fetchAll($members_sql, array($search . '%', $count / 2));
		
		$escorts_sql = '
			SELECT e.id, e.showname
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN order_packages op ON op.escort_id = e.id
			WHERE e.showname LIKE ? AND e.status = ? AND op.status = ? AND op.order_id IS NOT NULL ' . ( $type == 'escort' || $type == 'agency' ? ' AND e.id <> ' . $id : '' ) . '
			ORDER BY e.showname
			LIMIT ?
			';
		$escorts = self::db()->fetchAll($escorts_sql, array($search . '%', self::ESCORT_STATUS_ACTIVE, self::PACKAGE_STATUS_ACTIVE, $count / 2));
		
		return array('members' => $members, 'escorts' => $escorts);
	}
	
	public function getContacts($type, $id, $page = null, $per_page = null)
	{

		if ( $type == 'agency' ) {
			$sql = '
				SELECT SQL_CALC_FOUND_ROWS 
					contact_user_id AS user_id, contact_escort_id AS escort_id, 
					e.showname AS escort_showname, u.username AS member_username,
					IF((u.id AND u.status = 1) OR (op.order_id AND e.status = 32),1,0) AS is_active
				FROM agency_contacts ac
				LEFT JOIN escorts e ON e.id = ac.contact_escort_id
				LEFT JOIN users u ON u.id = ac.contact_user_id
				LEFT JOIN order_packages op ON op.escort_id = e.id AND op.status = ' . self::PACKAGE_STATUS_ACTIVE . ' AND op.order_id IS NOT NULL
				WHERE ac.agency_id = ?
			';
		} else {
			$field = self::getIdentityField($type);

			$sql = '
				SELECT SQL_CALC_FOUND_ROWS 
					contact_user_id AS user_id, contact_escort_id AS escort_id, 
					e.showname AS escort_showname, u.username AS member_username,
					IF((u.id AND u.status = 1) OR (op.order_id AND e.status = 32 AND e.status = ' . self::ESCORT_STATUS_ACTIVE . '),1,0) AS is_active
				FROM participants_contacts pc
				LEFT JOIN escorts e ON e.id = pc.contact_escort_id
				LEFT JOIN users u ON u.id = pc.contact_user_id
				LEFT JOIN order_packages op ON op.escort_id = e.id AND op.status = ' . self::PACKAGE_STATUS_ACTIVE . ' AND op.order_id IS NOT NULL
				WHERE pc.' . $field . ' = ?
			';
		}
		
		if ( $page && $per_page ) {
			$sql .= 'LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
		}
		
		$data = self::db()->fetchAll($sql, array($id));
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		
		return array('data' => $data, 'count' => $count);
	}
	
	//Checking if escort active and have active package
	public function checkIfActiveHasPackage($escort_id)
	{
		return self::db()->fetchOne('
			SELECT true
			FROM escorts e
			INNER JOIN order_packages op ON e.id = op.escort_id
			WHERE e.id = ? AND op.status = ? AND e.status & ? AND op.order_id IS NOT NULL
		',array($escort_id, self::PACKAGE_STATUS_ACTIVE, self::ESCORT_STATUS_ACTIVE));
	}
	
	public function getUnreadThreadsCount($type, $id)
	{
		if ( $type == 'agency' ) {
                $count = self::db()->fetchOne('
				SELECT count(*) 
				FROM threads_participants tp
				INNER JOIN escorts e ON e.id = tp.escort_id AND e.agency_id = ?
				WHERE tp.has_unread_message = 1 AND tp.is_deleted = 0 AND tp.is_blocked = 0
			', array($id));

		} else {
			
			$field = self::getIdentityField($type);
			
			if(Cubix_Application::getId() == APP_ED) {
				$count = self::db()->fetchOne('SELECT count(*) FROM threads_participants AS tp 
					INNER JOIN threads AS t ON tp.thread_id = t.id
					WHERE ' . $field . '= ? AND has_unread_message = 1 AND is_deleted = 0 AND is_blocked = 0 AND last_message_id is not null', array($id));
			} else {
				$count = self::db()->fetchOne('SELECT count(*) FROM threads_participants 
					WHERE ' . $field . '= ? AND has_unread_message = 1 AND is_deleted = 0 AND is_blocked = 0', array($id));
			}
			
		}
		
		return $count;
	}
	
	//Participant_type = ['escort', 'member'].
	//'escort' case get thread by escort_id
	//'member' case get thread by user_id
	private static function getThreadId($participant_type_from, $participant_type_to, $from_id, $to_id) 
	{
		
		$field_from = self::getIdentityField($participant_type_from);
		$field_to = self::getIdentityField($participant_type_to);
		
		$sql = '
			SELECT tp1.thread_id
			FROM threads_participants tp1
			INNER JOIN threads_participants tp2 ON tp2.thread_id = tp1.thread_id AND tp2.' . $field_from . ' = ?
			WHERE tp1.' . $field_to . ' = ?
		';
		
		$thread_id = self::db()->fetchOne($sql, array($from_id, $to_id));
		
		//If no thread for these users create it.
		if ( ! $thread_id ) {
			try {
				self::db()->beginTransaction();
				
				//Creating new thread
				self::db()->insert('threads', array());
				$thread_id = self::db()->lastInsertId();

				//Inserting rows in threads_participants for each participant
				self::db()->insert('threads_participants', array('thread_id' => $thread_id, $field_from => $from_id, 'has_unread_message' => 0));
				self::db()->insert('threads_participants', array('thread_id' => $thread_id, $field_to => $to_id));
				
				self::db()->commit();
			} catch (Exception $ex) {
				self::db()->rollBack();
				
				throw $ex;
			}
		}
		
		return $thread_id;
	}
	
	//In PV we have two type of user escort and member
	//If it is independent or agency escort we take escort_id as identity
	//If member user_id
	private static function getIdentityField($type)
	{
		//return $type;
		switch ( $type ) {
			case 'escort':
			case 'agency':
				$field = 'escort_id';
				break;
			case 'member':
				$field = 'user_id';
		}
		
		return $field;
	}
	
	private static function checkForBlacklistedWords($text)
	{
		$type = 4;
		$sql = '
			SELECT id, name, search_type
			FROM blacklisted_words
			WHERE types & ?';
        $res = array();
		$symbols = array("\r\n", "\n", "\r");
        $wordList = self::db()->fetchAll($sql, array($type));
		$text = strtolower(str_replace($symbols, "",$text));
        if( count($wordList) > 0 ){
            foreach ( $wordList as $word ){
                if(strlen(trim($word->name)) > 0)
				{	
					$search_words = strtolower(str_replace( $symbols, "", $word->name));
					
					if($word->search_type == 1){
						$result = strpos($text, $search_words);
					}
					else{
						$result = preg_match("/(\W+)".$search_words."(\W+)/", $text);
						$result = $result == 0 ? false : true; 
					}
					
					if ($result !== false) {

						$res[] = $word->name;

					}
				}
            }
        }
        
        return $res;
	}
	
	public function notifyByMail($user_id,$notify)
	{
		self::db()->update('users', array('recieve_pm_email' => $notify), self::db()->quoteInto('id = ?', $user_id));
	}
	
	public function notifyByMailText($user_id,$notify)
	{
		self::db()->update('users', array('recieve_pm_email_text' => $notify), self::db()->quoteInto('id = ?', $user_id));
	}
	
	public function getNotifications($user_id)
	{
		return self::db()->fetchRow('
				SELECT  recieve_pm_email,recieve_pm_email_text
				FROM users
				WHERE id = ?
			', array($user_id));
	}
	
}