<?php

class Cubix_Api_Module_Comments extends Cubix_Api_Module
{
	// comments status
	const COMMENT_ACTIVE = 1;
	const COMMENT_NOT_APPROVED = -3;
	const COMMENT_DISABLED = -4;

	public function getEscortComments($page = 1, $per_page = 3, $escort_id)
	{
		$limit = '';
		if ( ! is_null($page) ) {
			$limit = ' LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
		}

		try {
			$comments = self::db()->fetchAll("
				SELECT c.*, u.username FROM comments c
				INNER JOIN users u ON u.id = c.user_id
				WHERE c.escort_id = ? AND (c.status = ?) AND c.is_reply_to = 0
				ORDER BY c.time DESC
				{$limit}
			", array($escort_id, self::COMMENT_ACTIVE));

			foreach ( $comments as $i => $comment )
			{
				$comments[$i]->replied_comment = self::db()->fetchAll("
					SELECT c.*, u.username FROM comments c
					INNER JOIN users u ON u.id = c.user_id
					WHERE (c.status = ?) AND c.is_reply_to = ?
				", array(self::COMMENT_ACTIVE, $comment->id));
			}

			$countSql = "
				SELECT COUNT(c.id)
				FROM comments c
				INNER JOIN users u ON u.id = c.user_id
				WHERE c.escort_id = ? AND (c.status = ? OR c.status = ?) AND c.is_reply_to = 0
			";

			$count = self::db()->fetchOne($countSql, array($escort_id, self::COMMENT_ACTIVE, self::COMMENT_NOT_APPROVED));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return array('data' => $comments, 'count' => $count);
	}

	public function getAgencyComments($page = 1, $per_page = 3, $agency_id)
	{
		$limit = '';
		if ( ! is_null($page) ) {
			$limit = ' LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
		}

		try {
			$comments = self::db()->fetchAll("
				SELECT ac.*, u.username FROM agency_comments ac
				INNER JOIN users u ON u.id = ac.user_id
				WHERE ac.agency_id = ? AND (ac.status = ?) AND ac.is_reply_to = 0
				ORDER BY ac.time DESC
				{$limit}
			", array($agency_id, self::COMMENT_ACTIVE));

			foreach ( $comments as $i => $comment )
			{
				$comments[$i]->replied_comment = self::db()->fetchAll("
					SELECT ac.*, u.username FROM agency_comments ac
					INNER JOIN users u ON u.id = ac.user_id
					WHERE (ac.status = ?) AND ac.is_reply_to = ?
				", array(self::COMMENT_ACTIVE, $comment->id));
			}

			$countSql = "
				SELECT COUNT(ac.id)
				FROM agency_comments ac
				INNER JOIN users u ON u.id = ac.user_id
				WHERE ac.agency_id = ? AND (ac.status = ? OR ac.status = ?) AND ac.is_reply_to = 0
			";

			$count = self::db()->fetchOne($countSql, array($agency_id, self::COMMENT_ACTIVE, self::COMMENT_NOT_APPROVED));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}



		return array('data' => $comments, 'count' => $count);
	}

	public function getComment($comment_id)
	{
		return self::db()->fetchRow("
			SELECT c.*, u.username FROM comments c
			INNER JOIN users u ON u.id = c.user_id
			WHERE c.id = ?
		", array($comment_id));
	}

	public function addComment($data)
	{
		try {
			if ( Cubix_Application::getId() == APP_6A ) {
				$isBlocked = self::db()->fetchOne('SELECT TRUE FROM escort_blocked_users WHERE escort_id = ? AND user_id = ?', array($data['escort_id'], $data['user_id']));

				if ( $isBlocked ) {
					return false;
				}
			}

			$is_suspicious = self::db()->fetchOne('SELECT is_suspicious FROM members WHERE user_id = ?', $data['user_id']);

			if ($is_suspicious == 1)
				$data['status'] = -4;

			$data['is_suspicious'] = self::_isSuspicious($data['escort_id'], $data['user_id'], $data['ip']);

			self::db()->insert('comments', $data);
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		$id = self::db()->lastInsertId();

		if( Cubix_Application::getId() == APP_BL || Cubix_Application::getId() == APP_EF){
			Cubix_AlertEvents::addFollowAlert($data['user_id'], FOLLOW_ALERT_COMMENT, $id);
		}

		return $id;
	}

	private static function _isSuspicious($escort_id, $user_id, $ip)
	{
		$db = self::db();

		// 2 comments with same IP and same escort but different poster
		$sql = "SELECT DISTINCT user_id FROM comments WHERE escort_id = " . $escort_id . " AND ip = '" . $ip . "'";

		$items = $db->query($sql)->fetchAll();

		if ($items && (count($items) > 1 || $user_id != $items[0]->user_id))
		{
			//COMMENT_SUSPICIOUS_SAME_ESCORT_AND_IP
			return 1;
		}

		// comment poster IP identical with other user account with comments
		$sql = "SELECT DISTINCT u.id FROM comments c LEFT JOIN users u ON c.user_id = u.id WHERE u.last_ip = '" . $ip . "'";

		$items = $db->query($sql)->fetchAll();

		if ($items && count($items) > 1)
		{
			//COMMENT_SUSPICIOUS_USER_IP_DUPLICITY
			return 2;
		}

		// comment poster has identical clientID cookie with escort
		if (($escort_user_id = $db->fetchOne("SELECT user_id FROM escorts WHERE id = " . $escort_id)) > 0)
		{
			$sql = "
				SELECT cc1.client_id
				FROM (client_cookie AS cc1, client_cookie AS cc2)
				WHERE cc1.user_id = " . $escort_user_id . " AND cc2.user_id = " . $user_id . " AND cc1.client_id = cc2.client_id";
			$items = $db->query($sql)->fetchAll();

			if ($items && count($items) > 0)
			{
				//COMMENT_SUSPICIOUS_UNIQUE_COOKIE_ID_DUPLICITY
				return 3;
			}
		}

		return 0;
	}

	public function addAgencyComment($data)
	{
		try {
			self::db()->insert('agency_comments', $data);
			self::db()->query('UPDATE members SET agency_comments_count = agency_comments_count + 1 WHERE user_id = ?', $data['user_id']);
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return self::db()->lastInsertId();
	}

	public function vote($type, $comment_id)
	{
		try {
			$data = array();
			if ( $type == 'vote-up' ) {
				$data['thumbup_count'] = new Zend_Db_Expr('thumbup_count + 1');
			}
			else {
				$data['thumbdown_count'] = new Zend_Db_Expr('thumbdown_count + 1');
			}

			self::db()->update('comments', $data, self::db()->quoteInto('id = ?', $comment_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function voteAgency($type, $comment_id, $user_id)
	{
		try {
			$data = array();
			if ( $type == 'vote-up' ) {
				$data['thumbup_count'] = new Zend_Db_Expr('thumbup_count + 1');
				$data['thumbup_users'] = new Zend_Db_Expr("CONCAT_WS(',', thumbup_users, '" . $user_id . "')");
			}
			else {
				$data['thumbdown_count'] = new Zend_Db_Expr('thumbdown_count + 1');
				$data['thumbdown_users'] = new Zend_Db_Expr("CONCAT_WS(',', thumbdown_users, '" . $user_id . "')");
			}

			self::db()->update('agency_comments', $data, self::db()->quoteInto('id = ?', $comment_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function  getCommentsByUserId($user_id, $page = 1,$per_page = 5,$count = NULL)
	{
		$limit = '';
		if ( ! is_null($page) ) {
			$limit = ' LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
		}
		try {
		$comments = self::db()->fetchAll("
			SELECT SQL_CALC_FOUND_ROWS
			c.escort_id as id, c.user_id, c.time, c.message , e.showname, ep.hash as photo_hash , ep.ext as photo_ext
			FROM comments c
			INNER JOIN escorts e ON e.id = c.escort_id
			INNER JOIN escort_photos ep ON ep.escort_id = e.id
			WHERE ep.is_main = 1 AND c.status = 1 AND c.user_id = ?
			ORDER BY c.time DESC
			{$limit}
		", array($user_id));

		$count =self::db()->fetchOne('SELECT FOUND_ROWS()');

		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return array('data' => $comments, 'count' => $count);
	}

	public function addEscortComment($escort_id , $agency_id , $comment)
    {
        self::db()->insert('escort_comments', array(
            'escort_id' => $escort_id,
            'agency_id' => $agency_id,
            'comment'   => $comment
        ));
    }
}
