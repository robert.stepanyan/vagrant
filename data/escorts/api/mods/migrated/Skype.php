<?php
class Cubix_Api_Module_Skype extends Cubix_Api_Module 
{
	public function get($escort_id){
		$escort_data = self::db()->fetchAll('
			SELECT * FROM escort_skype WHERE escort_id = ?
		', array($escort_id));

		 return $escort_data[0];
	}

	public function update($data){

		$data['escort_id'] = intval($data['escort_id']);
		
		$sql = 'INSERT INTO escort_skype(escort_id,contact_option,escort_skype_username,paxum_email,status,price_30,price_60) 
					VALUES 
					   (?,"'.$data['contact_option'].'","'.$data['escort_skype_username'].'","'.$data['paxum_email'].'",'.$data['status'].', '.$data['30min'].', '.$data['60min'].') 
					ON DUPLICATE KEY UPDATE contact_option = "'.$data['contact_option'].'", escort_skype_username = "'.$data['escort_skype_username'].'",paxum_email = "'.$data['paxum_email'].'",status = '.$data['status'].',price_30 = '.$data['30min'].',price_60 = '.$data['60min'].'';
					
        if(in_array(Cubix_Application::getId(), array(APP_EF, APP_A6))){
            self::db()->query('INSERT INTO escort_skype_history (escort_id,contact_option,escort_skype_username,paxum_email,status,price_30,price_60,created_at) VALUES(?,?,?,?,?, ?, ?, NOW())',
                array($data['escort_id'],$data['contact_option'],$data['escort_skype_username'], $data['paxum_email'], $data['status'],$data['30min'],$data['60min']));

        }

		self::db()->query($sql, $data['escort_id']);
		self::db()->query('UPDATE escorts SET skype_call_status = ? WHERE id = ?', array($data['status'], $data['escort_id']));
		
		if($data['status'] == 1){
			Cubix_SyncNotifier::notify($data['escort_id'],Cubix_SyncNotifier::EVENT_SKYPE_CALL_ACTIVATED);
		}elseif($data['status'] == 0){
			Cubix_SyncNotifier::notify($data['escort_id'],Cubix_SyncNotifier::EVENT_SKYPE_CALL_DEACTIVATED);
		}
		
		return array('status' => true);	
	}

	public function updateand6($data){

		$data['escort_id'] = intval($data['escort_id']);
		
		$sql = 'INSERT INTO escort_skype(escort_id,escort_skype_username,paxum_email,status,price_30,price_60,skype_payment_option,bank_wire_beneficiary_name,bank_wire_city,bank_wire_iban,bank_wire_bic) 
					VALUES 
					   (?,"'.$data['escort_skype_username'].'","'.$data['paxum_email'].'",'.$data['status'].', '.$data['30min'].', '.$data['60min'].', "'.$data['skype_payment_option'].'", "'.$data['bank_wire_beneficiary_name'].'", "'.$data['bank_wire_city'].'","'.$data['bank_wire_iban'].'", "'.$data['bank_wire_bic'].'") 
					ON DUPLICATE KEY UPDATE escort_skype_username = "'.$data['escort_skype_username'].'",paxum_email = "'.$data['paxum_email'].'",status = '.$data['status'].',price_30 = '.$data['30min'].',price_60 = '.$data['60min'].', skype_payment_option ="'.$data['skype_payment_option'].'", bank_wire_beneficiary_name ="'.$data['bank_wire_beneficiary_name'].'", bank_wire_city ="'.$data['bank_wire_city'].'",  bank_wire_iban ="'.$data['bank_wire_iban'].'", bank_wire_bic ="'.$data['bank_wire_bic'].'"';
				
		self::db()->query($sql, $data['escort_id']);
		self::db()->query('UPDATE escorts SET skype_call_status = ? WHERE id = ?', array($data['status'], $data['escort_id']));
		
		if($data['status'] == 1){
			Cubix_SyncNotifier::notify($data['escort_id'],Cubix_SyncNotifier::EVENT_SKYPE_CALL_ACTIVATED);
		}elseif($data['status'] == 0){
			Cubix_SyncNotifier::notify($data['escort_id'],Cubix_SyncNotifier::EVENT_SKYPE_CALL_DEACTIVATED);
		}
		
		return array('status' => true);	
	}

	public function earnedsum($escort_id){
		$sum = self::db()->fetchOne('
			SELECT SUM(amount) FROM  video_chat_requests  WHERE status = 2 AND escort_id = ?
		', array($escort_id));

		 return $sum;
	}

	public function earnedsumagency($agency_id){
		$sum = self::db()->fetchOne('
			SELECT SUM(amount) FROM  video_chat_requests  WHERE status = 2 AND agency_id = ?
		', array($agency_id));

		 return $sum;
	}

    public function getSkypeBookingRequests($escort_id,$page = 1,$per_page = 5)
    {
        $limit = 'LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;

        try{
            $sql = "SELECT
                        * 
                    FROM
                        video_chat_requests 
                    WHERE
                        escort_id = ? ORDER BY progress_status ASC ".$limit;

            $result = self::db()->fetchAll($sql, array($escort_id));

            return $result;
        }catch (Exception $e){
            return $e;
        }
    }
    public function getSkypeBookingRequestsCount($escort_id,$page = 1,$per_page = 5)
    {
        try{
            $sql = "SELECT COUNT(*) as `count` FROM video_chat_requests WHERE escort_id = ?;";

            $result = self::db()->fetchAll($sql, array($escort_id));

            return $result;
        }catch (Exception $e){
            return $e;
        }
    }

    public function skypeSetStatus($id,$status)
    {
        $result = self::db()->query('UPDATE `video_chat_requests` SET `progress_status` = ? WHERE `id` = ?', array($status,$id));

        return $result;
    }

}
