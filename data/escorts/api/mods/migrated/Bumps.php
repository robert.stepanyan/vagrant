<?php
class Cubix_Api_Module_Bumps extends Cubix_Api_Module 
{
	public function getConfirmedBumps(){
		return self::db()->fetchAll('SELECT * FROM profile_bumps_orders WHERE status = 2');
	}

    public function confirmBumpsTransfer($profile_bump_order_id){
        return self::db()->update('profile_bumps_orders', array('status' => 3), self::db()->quoteInto('id = ?', $profile_bump_order_id));
    }

    public function getBumpOrder($bump_id){
		return self::db()->fetchRow('
			SELECT
				pb.*,
				c.title_en,
				c.title_it
			FROM
				profile_bumps_orders pb
			inner join cities c on c.id = pb.city_id
			WHERE
				pb.id = ?
		', $bump_id);
	}
}
