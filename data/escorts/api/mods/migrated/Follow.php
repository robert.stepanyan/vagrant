<?php

class Cubix_Api_Module_Follow extends Cubix_Api_Module
{	
	public function getAll($params)
	{
		try{
			$limit = $per_page = $params['per_page'];
			$start = ($params['page'] - 1) * $per_page;
			$lang = $params['lang'];
			
			$where = "";
			if ( isset($params['user_id']) && $params['user_id']) {
				$where .= " AND fu.user_id = " . $params['user_id'];
			}

			if ( strlen($params['showname']) ) {
				$where .= " AND ( a.name LIKE '". $params['showname'] . "%' OR e.showname LIKE '" . $params['showname'] . "%') ";
			}

			if (isset($params['in_top_10']) && is_numeric($params['in_top_10'])){
			    $where .= " AND fu.in_top_10 = {$params['in_top_10']} ";
            }

			$sql = "
				SELECT SQL_CALC_FOUND_ROWS	fu.id, fu.type, fu.type_id, fu.short_comment, e.showname, a.name as agency_name, 
				a.slug as agency_slug, a.logo_hash as agency_hash, a.logo_ext as agency_ext, ep.hash, ep.ext, in_top_10,
				UPPER(cr.iso) as country, c.title_".$lang." as city, cr.title_".$lang." as country_name FROM follow_users fu
				LEFT JOIN escorts e ON fu.type = 'escort' AND fu.type_id = e.id
				LEFT JOIN agencies a ON fu.type = 'agency' AND fu.type_id = a.id
				LEFT JOIN countries cr ON e.country_id = cr.id OR a.country_id =  cr.id
				LEFT JOIN cities c ON e.city_id = c.id OR a.city_id = c.id
				LEFT JOIN escort_photos ep ON fu.type = 'escort' AND ep.escort_id = fu.type_id AND ep.is_main = 1
				WHERE 1 ". $where ."
				ORDER BY fu.id DESC
				LIMIT " . $start . ", " . $limit . "
			";

			$countSql = 'SELECT FOUND_ROWS() AS count';
			
			$result = self::db()->fetchAll($sql);
			$count = self::db()->fetchOne($countSql);

			return array('result' => $result, 'count' => $count);
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

    public function getMemberFollowingCount($meber_id)
    {
        $count = self::db()->fetchOne('
			SELECT count(id) as cnt FROM follow_users
			WHERE status = 1 AND user_id = ? 
		', array($meber_id));

        return $count;
    }
	
	function getCount($type, $type_id)
	{
		$user_id = (int)$user_id;

		$count = self::db()->fetchOne('
			SELECT count(id) as cnt FROM follow_users
			WHERE status = 1 AND type = ? AND type_id = ? 
		', array($type, $type_id));

		return $count;	
	}

	public function getFollowList($user_id)
	{		
		$follow_list = self::db()->fetchAll('
			SELECT type , type_id FROM follow_users
			WHERE user_id = ? 
		', $user_id);
		
		$follow_array_list = array();
		foreach($follow_list as $follow){
			$follow_array_list[] =  $follow->type. '-'. $follow->type_id;
		}
		
		return $follow_array_list;
	}
	
	public function getFollowEvents($follow_user_id)
	{		
		$events = self::db()->fetchAssoc('
			SELECT event, extra FROM follow_users_events
			WHERE follow_user_id = ? 
		', $follow_user_id);
			
		$follow = self::db()->fetchRow('
			SELECT id, type , type_id, short_comment FROM follow_users
			WHERE id = ? 
		', $follow_user_id);
		
		return array('data' => $follow, 'events' => $events);
	}
	
	public function remove($user_id, $user_type_id, $user_type)
	{
		$follow_user_id = self::db()->fetchOne('SELECT id FROM follow_users WHERE user_id = ? AND type_id = ? AND type = ? ', array($user_id, $user_type_id, $user_type));
		
		if ($follow_user_id)
		{
			self::db()->delete('follow_users', self::db()->quoteInto('id = ?', $follow_user_id));
			self::db()->delete('follow_users_events', self::db()->quoteInto('follow_user_id = ?', $follow_user_id));
		}
	}
	
	public function add($data,$events, $cities)
	{
		try{
			self::db()->insert('follow_users', $data);
			$follow_id = self::db()->lastInsertId();
			if(Cubix_Application::getId() == APP_ED){
				if($data['user-id']){
					self::db()->query('UPDATE follow_users SET lang = ? WHERE user_id = ?', array($data['lang'], $data['user_id']));
				}else{
					self::db()->query('UPDATE follow_users SET lang = ? WHERE email = ?', array($data['lang'], $data['email']));

				}
			}
			return self::addEvents($follow_id, $events, $cities);
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function edit($follow_user_id, $data, $events, $cities)
	{
		try{
			self::db()->update('follow_users', $data,self::db()->quoteInto('id = ?', $follow_user_id));
			self::db()->delete('follow_users_events', self::db()->quoteInto('follow_user_id = ?', $follow_user_id));
			self::addEvents($follow_user_id, $events, $cities);
			if(Cubix_Application::getId() == APP_ED){
				if($data['user-id']){
					self::db()->query('UPDATE follow_users SET lang = ? WHERE user_id = ?', array($data['lang'], $data['user_id']));
				}else{
					self::db()->query('UPDATE follow_users SET lang = ? WHERE email = ?', array($data['lang'], $data['email']));

				}
			}
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	private function addEvents($id,$events,$extra)
	{
		foreach($events as $event){
			$data = array(
				'follow_user_id' => $id, 
				'event' => $event
			);
			if($event == FOLLOW_ESCORT_NEW_CITY || $event == FOLLOW_AGENCY_NEW_CITY){
				$data['extra'] = $extra;
			}
			self::db()->insert('follow_users_events', $data);
		}
	}
	
	public function reorderTop10($user_id,$sort_ids) 
	{
		foreach ( $sort_ids as $i => $sort_id ) {
			self::db()->query('UPDATE follow_users SET rank = ? , in_top_10 = 1 WHERE id = ? AND user_id = ?', array(($i + 1), $sort_id,$user_id));
		}
	}
	
	public function getTop10($user_id,$lang) {
		
		return self::db()->fetchAll("
			SELECT SQL_CALC_FOUND_ROWS	fu.id, fu.type, fu.type_id, fu.short_comment, e.showname, a.name as agency_name, 
				a.slug as agency_slug, a.logo_hash as agency_hash, a.logo_ext as agency_ext, ep.hash, ep.ext, in_top_10,
				UPPER(cr.iso) as country, c.title_{$lang} as city FROM follow_users fu
				LEFT JOIN escorts e ON fu.type = 'escort' AND fu.type_id = e.id
				LEFT JOIN agencies a ON fu.type = 'agency' AND fu.type_id = a.id
				LEFT JOIN countries cr ON e.country_id = cr.id OR a.country_id =  cr.id
				LEFT JOIN cities c ON e.city_id = c.id OR a.city_id = c.id
				LEFT JOIN escort_photos ep ON fu.type = 'escort' AND ep.escort_id = fu.type_id AND ep.is_main = 1
			WHERE fu.in_top_10 = 1 AND fu.user_id = ? 
			ORDER BY fu.rank
			LIMIT 10
		", $user_id);
	}
	
	public function favoritesMoveUp($user_id, $id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($id)) $id = reset($id);
		
		$cur_rank = self::db()->fetchOne('SELECT rank FROM follow_users WHERE user_id = ? AND id = ?', array($user_id, $id));
		
		if (!$cur_rank || $cur_rank == 1)
			return;
		
		$new_rank = $cur_rank - 1;
		
		$replace_id = self::db()->fetchOne('SELECT id FROM follow_users WHERE user_id = ? AND rank = ?', array($user_id, $new_rank));
		
		self::db()->update('follow_users', array(
			'rank' => $cur_rank
		), array(
			self::db()->quoteInto('user_id = ?', $user_id), 
			self::db()->quoteInto('id = ?', $replace_id)
		));
		
		self::db()->update('follow_users', array(
			'rank' => $new_rank
		), array(
			self::db()->quoteInto('user_id = ?', $user_id), 
			self::db()->quoteInto('id = ?', $id)
		));
	}
	
	public function favoritesMoveDown($user_id, $id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($id)) $id = reset($id);
		
		$cur_rank = self::db()->fetchOne('SELECT rank FROM follow_users WHERE user_id = ? AND id = ?', array($user_id, $id));
		$max_rank = self::db()->fetchOne('SELECT MAX(rank) FROM follow_users WHERE user_id = ? AND rank > 0', $user_id);
		
		if (!$cur_rank || $cur_rank == $max_rank)
			return;
		
		$new_rank = $cur_rank + 1;
		
		$replace_id = self::db()->fetchOne('SELECT id FROM follow_users WHERE user_id = ? AND rank = ?', array($user_id, $new_rank));
		
		self::db()->update('follow_users', array(
			'rank' => $cur_rank
		), array(
			self::db()->quoteInto('user_id = ?', $user_id), 
			self::db()->quoteInto('id = ?', $replace_id)
		));
		
		self::db()->update('follow_users', array(
			'rank' => $new_rank
		), array(
			self::db()->quoteInto('user_id = ?', $user_id), 
			self::db()->quoteInto('id = ?', $id)
		));
	}
	
	public function removeFavorites($user_id, $id)
	{
		self::db()->update('follow_users', array(
			'rank' => 0,
			'in_top_10' => 0
		), array(
			self::db()->quoteInto('user_id = ?', $user_id), 
			self::db()->quoteInto('id = ?', $id)
		));
	}
	
	public function reorderingTop10($user_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
						
		$items = self::db()->fetchAll('SELECT id FROM follow_users WHERE user_id = ? AND rank > 0 ORDER BY rank ASC', array($user_id));
		
		if ($items)
		{
			$c = 1;
			
			foreach ($items as $item)
			{
				self::db()->update('follow_users', array(
					'rank' => $c
				), array(
					self::db()->quoteInto('user_id = ?', $user_id), 
					self::db()->quoteInto('id = ?', $item->id)
				));
				
				$c++;
			}
		}
	}
	
	public function checkEmailExists($email,$type, $type_id)
	{
		try{
			$email_exist = self::db()->fetchOne('
				SELECT TRUE FROM users WHERE email = ? AND status <> -5
			', $email);

			if($email_exist ){
				return 1;
			}
			
			$follow_exists = self::db()->fetchOne('SELECT TRUE FROM follow_users WHERE email = ? AND type = ? AND type_id = ?', array($email,$type, $type_id));
			return $follow_exists ? 2 : 0;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function checkUserExists($user_id)
	{
		return self::db()->fetchOne('SELECT TRUE FROM follow_users WHERE user_id = ? ', array($user_id));
	}
	
	public function checkEmailFollowerExists($email)
	{
		return self::db()->fetchOne('SELECT id FROM follow_users WHERE email = ? AND status = 1', array($email));
	}
	
	public function checkUserFollows($user_id,$type, $type_id)
	{
		try{
			return self::db()->fetchOne('SELECT TRUE FROM follow_users WHERE user_id = ? AND type = ? AND type_id = ?', array($user_id,$type, $type_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function confirmEmail($email, $hash)
	{
		$follow = self::db()->fetchRow("
			SELECT fu.id, fu.type as user_type, fu.type_id as user_type_id, e.showname, e.gender, e.country_id,
			a.name as agency_name, a.slug as agency_slug, a.logo_hash as agency_hash, a.logo_ext as agency_ext 
			FROM follow_users fu
			LEFT JOIN escorts e ON fu.type = 'escort' AND fu.type_id = e.id
			LEFT JOIN agencies a ON fu.type = 'agency' AND fu.type_id = a.id
			WHERE fu.email = ? AND fu.email_hash = ?
		", array($email, $hash));
		
		if ( $follow ) {
			self::db()->update('follow_users', array(
				'status' => 1,
				'email_hash' => NULL
			), array(
				self::db()->quoteInto('email = ?', $email)
			));
			
			/*$events = self::db()->fetchAssoc('
				SELECT event, extra FROM follow_users_events
				WHERE follow_user_id = ? 
			', $follow->id);*/
			
			return $follow;
		}
		
		return null;
	}
	
	public function checkHasFollowsWithEmail($user_id, $email)
	{
		$follows = self::db()->fetchAll("SELECT id FROM follow_users WHERE email = ? AND status = 1", array($email));
		if($follows){
			foreach($follows as $follow){
				self::db()->update('follow_users', array('user_id' => $user_id) , self::db()->quoteInto('id = ?', $follow->id) ); 
			}
		}
	}
	
	public function showTop10($member_id, $show)
	{
		self::db()->update('members', array('show_top10' => $show) , self::db()->quoteInto('id = ?', $member_id) ); 
	}
	
	public function isTop10Public($member_id)
	{
		return self::db()->fetchOne("SELECT show_top10 FROM members WHERE id = ?", $member_id);
	}		
	public function getFollowers($filter, $lang)
	{
		$per_page = 10;
		$page = $filter['page'] ? $filter['page'] : 1;
		$join = '';
		$fields = '';
		$where = '';
				
		if (isset($filter['escort_id']) && strlen($filter['escort_id'])){
			$where .= ' AND fu.type_id = ' . $filter['escort_id']. ' AND fu.type = "escort"';
		}

		if(isset($filter['with_pictures'])) {
			$join .= "LEFT JOIN escort_photos ep ON fu.type = 'escort' AND ep.escort_id = fu.user_id AND ep.is_main = 1";
			$fields .= ", ep.hash as hash, ep.ext As ext";
		}else if (isset($filter['with_member_data'])) {
            $join .= " LEFT JOIN members m on m.user_id = fu.user_id ";
            $fields .= ", m.hash as hash, m.ext As ext, m.id as member_id ";
        }
		
		if (isset($filter['agency_id']) && strlen($filter['agency_id'])){
			$fields .= ", e.showname";
			$join .= " LEFT JOIN escorts e ON e.id = fu.type_id AND fu.type = 'escort'";
			if(isset($filter['a_escort_id']) && strlen($filter['a_escort_id'])){
				$where .= ' AND fu.type_id = ' . $filter['a_escort_id']. ' AND fu.type = "escort"';
				//$where .= ' AND (( fu.type_id = ' . $filter['agency_id']. ' AND fu.type = "agency") OR ( fu.type_id = ' . $filter['a_escort_id']. ' AND fu.type = "escort"))' ;
			}
			else{
				$where .= ' AND (( fu.type_id = ' . $filter['agency_id']. ' AND fu.type = "agency") OR (e.agency_id = ' . $filter['agency_id'].' )) ';
			}
		}
				
		if (isset($filter['username']) && strlen($filter['username'])){
			$where .= ' AND u.username LIKE "' . $filter['username'] . '%"';
		}
		if (isset($filter['date_from']) && strlen($filter['date_from'])){
			$where .= ' AND DATE(fu.date) >= "' . date('Y-m-d', $filter['date_from']) . '"';
		}
		if (isset($filter['date_to']) && strlen($filter['date_to'])){
			$where .= ' AND DATE(fu.date) <= "' . date('Y-m-d', $filter['date_to']) . '"';
		}
		if (isset($filter['city_id']) && strlen($filter['city_id'])){
			$where .= ' AND c.id = '. $filter['city_id'];
		}
		if (isset($filter['country_id']) && strlen($filter['country_id'])){
			$where .= ' AND co.id = ' . $filter['country_id'];
		}
		
		$sql = "SELECT SQL_CALC_FOUND_ROWS fu.id, fu.user_id, fu.email, fu.type, fu.type_id, UNIX_TIMESTAMP(fu.date) as follow_date,
				fu.is_favorite, fu.newsletter, fu.comment, u.username, c.title_".$lang. " AS city , 
				co.title_".$lang." AS country, co.iso as country_iso ". $fields. "
				FROM follow_users fu
				LEFT JOIN users u ON u.id = fu.user_id
				LEFT JOIN countries co ON co.id = u.country_id
				LEFT JOIN cities c ON c.id = u.city_id
				".$join."
				WHERE fu.status = 1";
		
		$sql .= $where;
		//GROUP BY fu.id
        if( $filter['sort_field'] ){
            $sql .= '
			    ORDER BY ' . $filter['sort_field'] . ' ' . $filter['sort_dir'];
        }


		$sql .= '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$countSql = 'SELECT FOUND_ROWS()';
		
		$items = self::db()->fetchAll($sql);
		$count = intval(self::db()->fetchOne($countSql));
		
		return array($items, $count);
	}
	
	public function getCountries($type, $type_id, $lang)
	{
		$sql = "SELECT co.id, co.title_".$lang." AS title FROM follow_users fu
				INNER JOIN users u ON u.id = fu.user_id
				INNER JOIN countries co ON co.id = u.country_id
				WHERE fu.status = 1 AND fu.type = ? AND type_id = ?
				GROUP BY co.id 
				ORDER BY co.title_".$lang;
		return self::db()->fetchAll($sql,array($type, $type_id));
	}
	
	public function getCities($type, $type_id,$country_id, $lang)
	{
		$sql = "SELECT c.id, c.title_".$lang." AS title FROM follow_users fu
				INNER JOIN users u ON u.id = fu.user_id
				INNER JOIN cities c ON c.id = u.city_id
				WHERE fu.status = 1 AND fu.type = ? AND type_id = ? AND c.country_id = ?
				GROUP BY c.id 
				ORDER BY c.title_".$lang;
		return self::db()->fetchAll($sql,array($type, $type_id, $country_id));
	}
	
	public function toggleFavorite($type, $type_id, $follower_id, $is_favorite)
	{
		self::db()->update('follow_users', array(
			'is_favorite' => $is_favorite
		), array(
			self::db()->quoteInto('type = ?', $type),
			self::db()->quoteInto('type_id = ?', $type_id), 
			self::db()->quoteInto('id = ?', $follower_id)
		));
	}
	
	public function toggleNewsletter($type, $type_id, $follower_id, $newsletter)
	{
		self::db()->update('follow_users', array(
			'newsletter' => $newsletter
		), array(
			self::db()->quoteInto('type = ?', $type),
			self::db()->quoteInto('type_id = ?', $type_id), 
			self::db()->quoteInto('id = ?', $follower_id)
		));
	}
	
	public function addComment($type, $type_id, $follower_id, $comment)
	{
		self::db()->update('follow_users', array(
			'comment' => $comment
		), array(
			self::db()->quoteInto('type = ?', $type),
			self::db()->quoteInto('type_id = ?', $type_id), 
			self::db()->quoteInto('id = ?', $follower_id)
		));
	}		
	
}
