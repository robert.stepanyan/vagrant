<?php

class Cubix_Api_Module_Punter extends Cubix_Api_Module
{
	public function newPunter($expires_at, $message, $user_id, $parent = 0){
		
		$created_at = date("Y-m-d H:i:s");
		if($parent > 0){
			$expire_time = NUll;
			$msg = array('success' => 'Your answer to the question has been added');
			$status = 2;
		} else {
			$expire_time = date("Y-m-d H:i:s", time() + ($expires_at * 24 * 60 * 60));
			$msg = array('success' => 'Your question has been posted');
			$status = 1;
		}
		try {

			self::db()->insert('punter_messages', array(
				'user_id' => $user_id,
				'message' => $message,
				'created_at' => $created_at,
				'expires_at' => $expire_time,
				'status' => $status,
				'for' => 'member',
				'parent' => $parent
			));
			
			
			return json_encode($msg);
		}
		catch ( Exception $e ) {
			return $e;
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}


	}

	public function getAllPunters($user_id, $page, $per_page = 10){

		try {
			$user_punters = self::db()->query('SELECT users.username as answer_username, answer.id as answer_id, answer.message as answer_message, answer.created_at as answer_created_at, answer.user_id as answer_user_id, question.id as question_id, question.message as question_message, question.created_at as question_created_at, question.expires_at as question_expires_at from punter_messages AS question LEFT JOIN punter_messages as answer ON question.id = answer.parent LEFT JOIN users on answer.user_id=users.id WHERE question.user_id ='.$user_id. ' AND question.parent = 0 ORDER BY question.id DESC, answer.id DESC LIMIT '. (($page - 1) * $per_page) . ', '. ($per_page * $page))->fetchAll();
			$other_punters = self::db()->query('SELECT users.username as question_username, question.*, answer.id as answer_id, answer.created_at as answer_created_at, answer.message as answer_message FROM punter_messages as question  LEFT join punter_messages as answer on question.id = answer.parent AND answer.user_id = '.$user_id.' LEFT JOIN users on question.user_id = users.id where question.user_id <> '.$user_id.' AND question.status = 2 AND question.expires_at > NOW() AND question.parent = 0 ORDER BY question.id DESC LIMIT '. (($page - 1) * $per_page) . ', '. ($per_page * $page))->fetchAll();
			$count = self::db()->query('SELECT count(*) as count from punter_messages where parent = 0 AND STATUS = 2 AND expires_at > NOW()')->fetch();
			
			return array('user_punters' => $user_punters, 'other_punters' => $other_punters, 'count' => $count->count);
		}
		catch (Exception $e){
			return $e;
		}
	}

	public function getNewPunters(){

		try {
			$punters = self::db()->query('SELECT punter_messages.*, users.username from punter_messages LEFT JOIN users on punter_messages.user_id = users.id where punter_messages.status = 2 AND punter_messages.parent = 0 ORDER BY punter_messages.id DESC LIMIT 3')->fetchAll();
			return $punters;
		} catch(Exception $e){
			return $e;
		}
	}




}











?>