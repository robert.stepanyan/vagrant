<?php

class Cubix_Api_Module_Video extends Cubix_Api_Module
{
	
	public function getNewRejectedVideos($escort_id)
	{
		try{
			$new_rejected_videos = self::db()->fetchAll(" SELECT * FROM video WHERE escort_id = ? AND show_popup = ? AND status = ?", array($escort_id, 1, 'rejected'));
			self::db()->query('UPDATE video SET show_popup = 0 WHERE escort_id = ? AND show_popup = ? AND status = ?', array($escort_id, 1, 'rejected'));
			return $new_rejected_videos;
		} catch (Exception $e) {
			return array();
		}
	}
		
}
