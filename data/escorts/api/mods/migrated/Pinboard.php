<?php

class Cubix_Api_Module_Pinboard extends Cubix_Api_Module
{	
	public function getPosts($user_type, $filter, $page, $per_page)
	{
		if (is_array($user_type)) $user_type = reset($user_type);
		if (is_array($page)) $page = reset($page);
		if (is_array($per_page)) $per_page = reset($per_page);
		
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS 
				p.id, p.title, p.post, UNIX_TIMESTAMP(p.date) AS date, p.user_id, p.show_username, u.username, p.status 
			FROM pin_posts p 
			INNER JOIN users u ON u.id = p.user_id 
			WHERE (p.status = ? OR p.status = ?) AND p.application_id = ? 
		';
		
		if (in_array($user_type, array('escort', 'agency')))
			$sql .= ' AND p.user_type <> "member" ';
		else
			$sql .= ' AND p.user_type = "member" ';
		
		if (strlen($filter['search_text']))
		{
			$sql .= ' AND (p.title LIKE "%' . mysql_escape_string($filter['search_text']) . '%" OR p.post LIKE "%' . mysql_escape_string($filter['search_text']) . '%")';
		}
		
		$sql .= '
			ORDER BY p.date DESC
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
		
		$items = array(
			'data'	=> self::db()->fetchAll($sql, array(PINBOARD_STATUS_APPROVED, PINBOARD_STATUS_CLOSED, Cubix_Application::getId())),
			'count'	=> self::db()->fetchOne('SELECT FOUND_ROWS()')
		);
		
		return $items;
	}
	
	public function getMyPostsCount($user_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		
		return self::db()->fetchOne('SELECT COUNT(id) FROM pin_posts WHERE user_id = ?', $user_id);
	}
	
	public function getMyRepliedPostsCount($user_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		
		return self::db()->fetchOne('
			SELECT COUNT(DISTINCT(pr.post_id)) 
			FROM pin_replies pr 
			INNER JOIN pin_posts p ON p.id = pr.post_id AND (p.status = ? OR p.status = ?)
			WHERE pr.user_id = ? AND p.user_id <> ?
		', array(PINBOARD_STATUS_APPROVED, PINBOARD_STATUS_CLOSED, $user_id, $user_id));
	}
	
	public function getMyPostsRepliesCount($user_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		
		return self::db()->fetchOne('
			SELECT COUNT(DISTINCT(pr.post_id)) 
			FROM pin_replies pr 
			INNER JOIN pin_posts p ON p.id = pr.post_id 
			WHERE p.user_id = ?
		', array($user_id));
	}
	
	public function getMyPosts($user_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		
		return self::db()->query('SELECT id, title, post, UNIX_TIMESTAMP(date) AS date, status FROM pin_posts WHERE user_id = ? ORDER BY date DESC', $user_id)->fetchAll();
	}
	
	public function closePost($user_id, $post_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($post_id)) $post_id = reset($post_id);
		
		$is_user_post = self::db()->fetchOne('SELECT TRUE FROM pin_posts WHERE user_id = ? AND id = ? AND status = ?', array($user_id, $post_id, PINBOARD_STATUS_APPROVED));
		
		if ($is_user_post)
		{
			self::db()->update('pin_posts', array('status' => PINBOARD_STATUS_CLOSED), self::db()->quoteInto('id = ?', $post_id));
		}
	}
	
	public function openPost($user_id, $post_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($post_id)) $post_id = reset($post_id);
		
		$is_user_post = self::db()->fetchOne('SELECT TRUE FROM pin_posts WHERE user_id = ? AND id = ?', array($user_id, $post_id));
		
		if ($is_user_post)
		{
			self::db()->update('pin_posts', array('status' => PINBOARD_STATUS_PENDING), self::db()->quoteInto('id = ?', $post_id));
		}
	}
	
	public function addPost($user_id, $user_type, $title, $post, $vis, $alert)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($user_type)) $user_type = reset($user_type);
		if (is_array($title)) $title = reset($title);
		if (is_array($post)) $post = reset($post);
		if (is_array($vis)) $vis = reset($vis);
		if (is_array($alert)) $alert = reset($alert);
		
		self::db()->insert('pin_posts', array(
			'user_id' => $user_id,
			'user_type' => $user_type,
			'title' => $title,
			'post' => $post,
			'date' => new Zend_Db_Expr('NOW()'),
			'status' => PINBOARD_STATUS_PENDING,
			'application_id' => Cubix_Application::getId(),
			'show_username' => $vis,
			'alert' => $alert
		));
	}
	
	public function getPost($user_id, $post_id)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($post_id)) $post_id = reset($post_id);
		
		return self::db()->query('SELECT id, title, post, show_username, alert FROM pin_posts WHERE user_id = ? AND id = ?', array($user_id, $post_id))->fetch();
	}
	
	public function editPost($user_id, $post_id, $title, $post, $vis, $alert)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($post_id)) $post_id = reset($post_id);
		if (is_array($title)) $title = reset($title);
		if (is_array($post)) $post = reset($post);
		if (is_array($vis)) $vis = reset($vis);
		if (is_array($alert)) $alert = reset($alert);
		
		$is_user_post = self::db()->fetchOne('SELECT status FROM pin_posts WHERE user_id = ? AND id = ?', array($user_id, $post_id));
		
		if ($is_user_post)
		{
			if ($is_user_post == PINBOARD_STATUS_CLOSED)
				$status = PINBOARD_STATUS_CLOSED;
			else
				$status = PINBOARD_STATUS_PENDING;
			
			self::db()->update('pin_posts', array(
				'title' => $title,
				'post' => $post,
				'status' => $status,
				'modification_date' => new Zend_Db_Expr('NOW()'),
				'show_username' => $vis,
				'alert' => $alert
			), self::db()->quoteInto('id = ?', $post_id));
		}
	}
	
	public function addReply($user_id, $user_type, $post_id, $reply)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($user_type)) $user_type = reset($user_type);
		if (is_array($post_id)) $post_id = reset($post_id);
		if (is_array($reply)) $reply = reset($reply);
		
		$ret = NULL;
		
		if (in_array($user_type, array('escort', 'agency')))
			$ty = ' AND p.user_type <> "member" ';
		else
			$ty = ' AND p.user_type = "member" ';
		
		$is_valid_post = self::db()->query('
			SELECT p.user_id, p.alert, u.username AS username_owner, u.email AS email_owner 
			FROM pin_posts p 
			INNER JOIN users u ON u.id = p.user_id 
			WHERE p.id = ? ' . $ty . ' AND p.status = ?
		', array($post_id, PINBOARD_STATUS_APPROVED))->fetch();
		
		if ($is_valid_post)
		{
			self::db()->insert('pin_replies', array(
				'post_id' => $post_id,
				'user_id' => $user_id,
				'reply' => $reply,
				'date' => new Zend_Db_Expr('NOW()')
			));
			
			/* checking for alert */
			if ($is_valid_post->user_id != $user_id && $is_valid_post->alert)
			{
				$username_replyer = '';
				
				if ($user_type == 'member')
					$username_replyer = self::db()->fetchOne('SELECT username FROM users WHERE id = ?', $user_id);
				
				$ret = array(
					'username_replyer' => $username_replyer,
					'username_owner' => $is_valid_post->username_owner,
					'email_owner' => $is_valid_post->email_owner
				);
			}
		}
		
		return $ret;
	}
	
	public function getMyReplies($user_id, $filter)
	{
		if (is_array($user_id)) $user_id = reset($user_id);
		
		$where = '';
		
		if ($filter['search_text'])
		{
			$where .= ' AND (p.title LIKE "%' . mysql_escape_string($filter['search_text']) . '%" OR p.post LIKE "%' . mysql_escape_string($filter['search_text']) . '%")';
		}
		
		if ($filter['date_from'])
		{
			$where .= ' AND DATE(FROM_UNIXTIME(' . $filter['date_from'] . ')) <= DATE(p.date) ';
		}
		
		if ($filter['date_to'])
		{
			$where .= ' AND DATE(FROM_UNIXTIME(' . $filter['date_to'] . ')) >= DATE(p.date) ';
		}
		
		if ($filter['post_id'])
		{
			$where .= ' AND p.id = ' . $filter['post_id'];
		}
		
		$order = ' p.date DESC';
		
		if ($filter['show_order'] == 'oldest_to_newest')
		{
			$order = ' p.date ASC';
		}
		elseif ($filter['show_order'] == 'newest_replies')
		{
			$order = ' pr.date DESC';
		}
		
		$res = self::db()->query('
			SELECT p.id AS post_id, p.title, p.post, UNIX_TIMESTAMP(p.date) AS post_date, p.status, p.user_id AS owner_id, pr.user_id  
			FROM pin_posts p 
			INNER JOIN pin_replies pr ON p.id = pr.post_id 
			WHERE p.user_id <> ? AND pr.user_id = ? AND (p.status = ? OR p.status = ?) ' . $where . '
			GROUP BY p.id 
			ORDER BY ' . $order . ' 
		', array($user_id, $user_id, PINBOARD_STATUS_APPROVED, PINBOARD_STATUS_CLOSED))->fetchAll();
		
		return $res;
	}
	
	public function seeMyReplies($post_id, $user_id, $user_type, $page, $per_page)
	{
		if (is_array($post_id)) $post_id = reset($post_id);
		if (is_array($user_id)) $user_id = reset($user_id);
		if (is_array($user_type)) $user_type = reset($user_type);
		if (is_array($page)) $page = reset($page);
		if (is_array($per_page)) $per_page = reset($per_page);
		
		$items = array();
		
		if (in_array($user_type, array('escort', 'agency')))
			$ty = ' AND user_type <> "member" ';
		else
			$ty = ' AND user_type = "member" ';
		
		/*
		$owner_id = self::db()->fetchOne('SELECT user_id FROM pin_posts WHERE id = ? ' . $ty, $post_id);
		
		if ($owner_id)
		{
			$sql = '
				SELECT SQL_CALC_FOUND_ROWS 
					pr.id AS reply_id, pr.reply, UNIX_TIMESTAMP(pr.date) AS reply_date, pr.user_id, u.username 
				FROM pin_replies pr 
				INNER JOIN users u ON u.id = pr.user_id 
				WHERE pr.post_id = ? AND (pr.user_id = ? OR pr.user_id = ?) 
				GROUP BY pr.id 
				ORDER BY pr.date ASC
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
			
			$items = array(
				'data'	=> self::db()->fetchAll($sql, array($post_id, $user_id, $owner_id)),
				'count'	=> self::db()->fetchOne('SELECT FOUND_ROWS()')
			);
		}
		*/
		
		$has = self::db()->fetchOne('SELECT TRUE FROM pin_posts WHERE id = ? ' . $ty, $post_id);
		
		if ($has)
		{			
			$sql = '
				SELECT SQL_CALC_FOUND_ROWS 
					pr.id AS reply_id, pr.reply, UNIX_TIMESTAMP(pr.date) AS reply_date, pr.user_id, u.username 
				FROM pin_replies pr 
				INNER JOIN users u ON u.id = pr.user_id 
				WHERE pr.post_id = ? 
				GROUP BY pr.id 
				ORDER BY pr.date ASC
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
			
			$items = array(
				'data'	=> self::db()->fetchAll($sql, array($post_id)),
				'count'	=> self::db()->fetchOne('SELECT FOUND_ROWS()')
			);
		}
		
		return $items;
	}
	
	public function getMyPostsReplies($user_id, $filter)
	{				
		if (is_array($user_id)) $user_id = reset($user_id);
		
		$where = '';
		
		if ($filter['search_text'])
		{
			$where .= ' AND (p.title LIKE "%' . mysql_escape_string($filter['search_text']) . '%" OR p.post LIKE "%' . mysql_escape_string($filter['search_text']) . '%")';
		}
		
		if ($filter['date_from'])
		{
			$where .= ' AND DATE(FROM_UNIXTIME(' . $filter['date_from'] . ')) <= DATE(p.date) ';
		}
		
		if ($filter['date_to'])
		{
			$where .= ' AND DATE(FROM_UNIXTIME(' . $filter['date_to'] . ')) >= DATE(p.date) ';
		}
		
		if ($filter['post_id'])
		{
			$where .= ' AND p.id = ' . $filter['post_id'];
		}
		
		$order = ' p.date DESC';
		
		if ($filter['show_order'] == 'oldest_to_newest')
		{
			$order = ' p.date ASC';
		}
		elseif ($filter['show_order'] == 'newest_replies')
		{
			$order = ' pr.date DESC';
		}
		
		$res = self::db()->query('
			SELECT p.id AS post_id, p.title, p.post, UNIX_TIMESTAMP(p.date) AS post_date, p.status, p.user_id AS owner_id 
			FROM pin_posts p 
			INNER JOIN pin_replies pr ON p.id = pr.post_id 
			WHERE p.user_id = ? ' . $where . '
			GROUP BY p.id 
			ORDER BY ' . $order . ' 
		', array($user_id))->fetchAll();
		
		return $res;
	}
	
	public function seeMyPostsReplies($post_id, $user_type, $page, $per_page)
	{
		if (is_array($post_id)) $post_id = reset($post_id);
		if (is_array($user_type)) $user_type = reset($user_type);
		if (is_array($page)) $page = reset($page);
		if (is_array($per_page)) $per_page = reset($per_page);
		
		$items = array();
		
		if (in_array($user_type, array('escort', 'agency')))
			$ty = ' AND user_type <> "member" ';
		else
			$ty = ' AND user_type = "member" ';
		
		$has = self::db()->fetchOne('SELECT TRUE FROM pin_posts WHERE id = ? ' . $ty, $post_id);
		
		if ($has)
		{			
			$sql = '
				SELECT SQL_CALC_FOUND_ROWS 
					pr.id AS reply_id, pr.reply, UNIX_TIMESTAMP(pr.date) AS reply_date, pr.user_id, u.username 
				FROM pin_replies pr 
				INNER JOIN users u ON u.id = pr.user_id 
				WHERE pr.post_id = ? 
				GROUP BY pr.id 
				ORDER BY pr.date ASC
				LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
			
			$items = array(
				'data'	=> self::db()->fetchAll($sql, array($post_id)),
				'count'	=> self::db()->fetchOne('SELECT FOUND_ROWS()')
			);
		}
		
		return $items;
	}
}
