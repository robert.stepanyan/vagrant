<?php

class Cubix_Api_Module_Support extends Cubix_Api_Module 
{

	public function insert($data)
	{
		try {
			self::db()->insert('support_tickets', $data);
			
			return self::db()->lastInsertId();
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function get($ticket_id)
	{
		$ticket =  self::db()->fetchRow('
			SELECT * FROM support_tickets WHERE id = ?
		', $ticket_id);
		
		if(isset($ticket->from_backend_user)){
			$ticket->sent_by = self::db()->fetchOne(' SELECT username FROM backend_users WHERE id = ? ', $ticket->from_backend_user);
		}
		return $ticket;
	}
	
	public function getSalesInfo($ticket_id)
	{
		

		return self::db()->fetchRow('
			SELECT bu.email FROM support_tickets st
			INNER JOIN backend_users bu ON bu.id = st.backend_user_id
			WHERE st.id = ?
		', $ticket_id);
	}

	public function isUserTicket($data)
	{
		return self::db()->fetchOne('
			SELECT TRUE FROM support_tickets WHERE user_id = ? AND id = ?
		', array($data['user_id'], $data['ticket_id']));
	}

	public function isClosed($ticket_id)
	{
		
		return self::db()->fetchOne('
			SELECT TRUE FROM support_tickets WHERE status = 2 AND id = ?
		', array($ticket_id));
	}

    public function getAllPaginated ($user_id, $page = null, $per_page = null, $status = null) {
        $sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM support_tickets WHERE user_id = ? ';

        if($status) {
            $sql .= " AND status = $status ";
        }
        $sql .= ' ORDER BY creation_date DESC ';

        if ( $page && $per_page ) {
            $sql .= ' LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
        }

        $data = self::db()->fetchAll($sql, $user_id);
        $count = self::db()->fetchOne('SELECT FOUND_ROWS()');

        return array('data' => $data, 'count' => $count);
    }

	public function getAll($user_id)
	{


		return self::db()->fetchAll('
			SELECT * FROM support_tickets WHERE user_id = ? ORDER BY creation_date DESC
		', $user_id);
	}

	public function addComment($data)
	{
		try {
			self::db()->insert('ticket_comments', $data);
			if ( self::app()->id == APP_ED )
            {
                $update_data = array('status_progress' => 4, 'last_updated_date' => new Zend_Db_Expr('NOW()'));
            }else{
                $update_data = array('status_progress' => 4);
            }
			self::db()->update('support_tickets', $update_data ,self::db()->quoteInto('id = ?', $data['ticket_id']));
			return true;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function getComments($ticket_id)
	{
		return self::db()->fetchAll('
			SELECT * FROM ticket_comments WHERE ticket_id = ? AND status = 0
		', $ticket_id);
	}
	
	public function read($ticket_id)
	{
		self::db()->update('support_tickets', array('is_read' => 1), self::db()->quoteInto('id = ?', $ticket_id));
	}
	public function comment_read($ticket_id)
	{
		if( self::app()->id == APP_ED || self::app()->id == APP_BL  || self::app()->id == APP_EG_CO_UK  ){
			self::db()->update('ticket_comments', array('read_date' => date('Y-m-d H:i:s')), self::db()->quoteInto('ticket_id = ?', $ticket_id));
		}
	}
	
	public function getUnreadsCount($user_id)
	{
		return self::db()->fetchOne('
			SELECT COUNT(*) FROM support_tickets WHERE user_id = ? AND is_read = 0
		', $user_id);
	}

	public function getUnreadsCountV2($user_id)
	{
		return self::db()->fetchOne('
			SELECT COUNT(*) FROM support_tickets WHERE user_id = ? AND is_read = 0 AND status_progress = 3
		', $user_id);
	}
	
	public function addAttached($data)
	{
		try {
			self::db()->insert('ticket_attached_files', $data);
			return self::db()->lastInsertId();
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function getAttachedFile($attached_file_id){
	    if( !is_numeric($attached_file_id) ) return false;

        return self::db()->fetchAll('SELECT * FROM ticket_attached_files WHERE id = ?
		', $attached_file_id);
    }
	
	public function getAttached($ticket_id)
	{
		
		$ticket = self::db()->fetchRow('
			SELECT * FROM support_tickets WHERE id = ?
		', $ticket_id);
		
		if(isset($ticket->from_backend_user)){
			$ticket->sent_by = self::db()->fetchOne(' SELECT username FROM backend_users WHERE id = ? ', $ticket->from_backend_user);
		}
		$attached_files = self::db()->fetchAll('SELECT * FROM ticket_attached_files WHERE ticket_comment_id IS NULL AND ticket_id = ?
		', $ticket_id);
		$data = array('ticket'=> $ticket , 'attach' => $attached_files  );
		
		return $data;
	}
	
	public function getCommentsAttached($ticket_id)
	{
		

        $tickets = self::db()->fetchAll('
            SELECT * FROM ticket_comments WHERE ticket_id = ? AND status = 0
        ', $ticket_id);
		
		
		foreach($tickets as &$ticket)
		{
			$ticket->attached_files = self::db()->fetchAll('SELECT * FROM ticket_attached_files WHERE ticket_comment_id = ?
										', $ticket->id);
		}
		return $tickets;
		
	}
	
	public function addCommentAttached($data)
	{
		try {
			self::db()->insert('ticket_comments', $data['ticket']);
			$last_insert_id = self::db()->lastInsertId();
            if ( self::app()->id == APP_ED )
            {
                $update_data = array('status_progress' => 4, 'last_updated_date' => new Zend_Db_Expr('NOW()'));
            }else{
                $update_data = array('status_progress' => 4);
            }
			self::db()->update('support_tickets', $update_data ,self::db()->quoteInto('id = ?', $data['ticket']['ticket_id']));
			if($data['attach']){
				foreach($data['attach'] as $file_id){
					self::db()->update('ticket_attached_files', array('ticket_comment_id'=>$last_insert_id,'ticket_id'=> $data['ticket']['ticket_id']),self::db()->quoteInto('id = ?', $file_id));
				}
			}
			return true;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function insertAttached($data)
	{
		try {
			self::db()->insert('support_tickets', $data['ticket']);
			$last_insert_id = self::db()->lastInsertId();
			if($data['attach']){
				foreach($data['attach'] as $file_id){
					self::db()->update('ticket_attached_files', array('ticket_id'=>$last_insert_id),self::db()->quoteInto('id = ?', $file_id));
				}
			}
			return $last_insert_id;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function getFaq($user_type, $lng)
	{
		if (is_array($user_type)) $user_type = reset($user_type);
		if (is_array($lng)) $lng = reset($lng);
		
		try {
			return self::db()->query('
				SELECT '. Cubix_I18n::getTblField('question', $lng) . ' AS question, '. Cubix_I18n::getTblField('answer', $lng) . ' AS answer
				FROM faq 
				WHERE FIND_IN_SET(?, type) > 0 AND application_id = ? AND status = 1
				ORDER BY id DESC
			', array($user_type, self::app()->id))->fetchAll();
		}
		catch ( Exception $e ) {
			//return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
}
