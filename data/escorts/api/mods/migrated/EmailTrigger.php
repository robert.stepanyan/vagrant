<?php

    class Cubix_Api_Module_EmailTrigger extends Cubix_Api_Module
    {
        const TRIGGER_SIGNUP = 1; //recurring

        const TRIGGER_APPROVED_PROFILE_PENNDING_AC = 2;
        const TRIGGER_APPROVED_PROFILE_MISSING_AC = 3;
        const TRIGGER_APPROVED_PROFILE_APPROVED_AC = 4;
        const TRIGGER_APPROVED_PROFILE_REJECTED_AC = 5;

        const TRIGGER_WIZARD = 6; //recurring

        const TRIGGER_APPROVED_AC_APPROVED_PROFILE = 7;
        const TRIGGER_APPROVED_AC_PENNDING_PROFILE = 8;
        const TRIGGER_REJECTED_AC = 9;

        const TRIGGER_COMPLETED_PROFILE_MISSING_AC = 10; // recurring

        //difference between this and TRIGGER_APPROVED_PROFILE_MISSING_AC, TRIGGER_APPROVED_PROFILE_MISSING_AC is sent once while this is a reminder


        /**
         * @param $user_id int
         * @param $trigger_type int
         * @return boolean
         */

        public function add($user_id, $trigger_type, $params, $escort_id)
        {
  
            if($escort_id){
                $u = self::db()->fetchRow('
                    SELECT user_id FROM escorts
                    WHERE id = ?', array($escort_id), Zend_Db::FETCH_OBJ);

                $user_id = $u->user_id;
            }
            
            if(!in_array($user_id, array(427747,250871))){
                return false;
            }

            $data = array(
                'user_id' => $user_id,
                'trigger_type' => $trigger_type,
                'params' => serialize($params),
                'last_sent' => new Zend_Db_Expr('NOW()'),
                'escort_id' => $escort_id,
            );

            switch ($trigger_type) {
                case self::TRIGGER_SIGNUP:

                    $data['send_interval'] = 25;
                    $data['send_count'] = 12;

                    break;
                case self::TRIGGER_WIZARD:

                    $data['send_interval'] = 25;
                    $data['send_count'] = 3;

                    break;
                case self::TRIGGER_COMPLETED_PROFILE_MISSING_AC:

                    $data['send_interval'] = 25;
                    $data['send_count'] = 3;

                    break;

                default:
                    $data['send_interval'] = 0;
                    $data['send_count'] = 1;     
            }


            self::db()->insert('email_triggers', $data);


            return true;
        }


        /**
         * @param $user_id int
         * @param $trigger_type int
         * @return boolean
         */

        public function remove($user_id, $trigger_type, $escort_id)
        {
            if($escort_id){
                $where = "escort_id = {$escort_id} AND trigger_type = {$trigger_type}";
            }else if($user_id){
                $where = "user_id = {$user_id} AND trigger_type = {$trigger_type}";
            }

            $result = self::db()->delete('email_triggers', $where);

            return $result;
        }
    }