<?php

class Cubix_Api_Module_Billing extends Cubix_Api_Module
{
	
	//Orders activation condition
	const CONDITION_AFTER_RECEIVING_PAYMENT_DETAILS = 8;
	const CONDITION_AFTER_PAID = 5;
	const CONDITION_WITHOUT_PAYMENT = 10;
	
	//Packages activation type
	const ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES = 6;
	const ACTIVATE_AT_EXACT_DATE = 7;
	const ACTIVATE_ASAP = 9;
	
	//Packages statuses
	const PACKAGE_STATUS_PENDING  = 1;
	const PACKAGE_STATUS_ACTIVE   = 2;
	const PACKAGE_STATUS_EXPIRED  = 3;
	const PACKAGE_STATUS_CANCELLED = 4;
	const PACKAGE_STATUS_UPGRADED = 5;
	const PACKAGE_SUSPENDED = 6;
	
	//Order statuses
	const ORDER_STATUS_PENDING = 1;
	// const STATUS_CANCELLED = 2;
	const ORDER_STATUS_PAID = 3;
	// const STATUS_CHARGEBACK = 4;
	const ORDER_STATUS_PAYMENT_DETAILS_RECEIVED = 5;
	const ORDER_STATUS_PAYMENT_REJECTED = 6;
	const ORDER_STATUS_CLOSED = 7;
	
	//Transfers statuses
	const TRANSFER_STATUS_PENDING = 1;
	const TRANSFER_STATUS_CONFIRMED = 2;
	const TRANSFER_STATUS_REJECTED = 3;
	const TRANSFER_STATUS_AUTO_REJECTED = 4;
	
	const TRANSFER_TYPE_PHONE_BILLING = 7;
	const TRANSFER_TYPE_CREDIT_CARD = 3;
	
	const IVR_BACKEND_USER_ID = 100;
	const IVR_GOTD_BACKEND_USER_ID = 102;
	const IVR_VOTD_BACKEND_USER_ID = 102;
	
	const GOTD_STATUS_PENDING = 1;
	const GOTD_STATUS_ACTIVE  = 2;
	const GOTD_STATUS_EXPIRED = 3;
	
	const GOTD_ORDER_PENDING = 1;
	const GOTD_ORDER_PAID = 2;
	const GOTD_ORDER_EXPIRED = 3;

	const VOTD_ORDER_PENDING = 1;
	const VOTD_ORDER_PAID = 2;
	const VOTD_ORDER_EXPIRED = 3;
	
	
	const ESCORT_STATUS_NO_PROFILE = 1;
    const ESCORT_STATUS_NO_ENOUGH_PHOTOS = 2;
    const ESCORT_STATUS_NOT_APPROVED = 4;
    const ESCORT_STATUS_OWNER_DISABLED = 8;
    const ESCORT_STATUS_ADMIN_DISABLED = 16;
    const ESCORT_STATUS_ACTIVE = 32;
    const ESCORT_STATUS_IS_NEW = 64;
    const ESCORT_STATUS_PROFILE_CHANGED = 128;
    const ESCORT_STATUS_DELETED = 256;

    const ESCORT_STATUS_TEMPRARY_DELETED = 1024;
	
	public function getPackage($id) {
		$sql = '
			SELECT
				p.*
			FROM packages p
			WHERE id = ?
		';
		return self::db()->fetchRow($sql, array($id));
	}
	
	
	public function getPackages($is_for_phone = 0, $application_id = null)
	{
		$sql = '
			SELECT
				p.*
			FROM packages p
			INNER JOIN applications a ON a.id = p.application_id
			WHERE 1
		';
		$where = ' AND is_active = 1 ';
		if ($is_for_phone) {
			$where .= 'AND p.is_for_phone_billing = 1';
		}
		
		$sql .= $where;
		
		return self::db()->fetchAll($sql);
	}
	
	public function gePackagesWithPrice($app_id, $user_type, $gender, $package_id = null)
	{		
		if ( ! is_null($package_id) ) {
			$package_id = $package_id;
		}

		$where = "";
		if ( $package_id ) {
			$where .= " AND p.id = {$package_id} ";
		}

		$sql = "
			SELECT 
				p.id,
				p.name,
				p.available_for,
				pp.price,
				p.period,
				p.is_default
            FROM packages p
			INNER JOIN package_prices pp ON pp.package_id = p.id
            WHERE
				p.application_id = ?
                AND pp.user_type = ?
                AND pp.gender = ?
				{$where}			
		";
		if ( $package_id ) {
			return self::db()->fetchRow($sql, array($app_id, $user_type, $gender));
		} else {
			return self::db()->fetchAll($sql, array($app_id, $user_type, $gender));
		}
	}
	
	public function addPackage($data)
	{
		$order_data = $data['order_data'];
		$packages_data = $data['packages_data'];
		
		$is_free = false;
		if ( isset($order_data['is_free']) && $order_data['is_free'] ) {
			$is_free = true;
		}
		unset($order_data['is_free']);
		
		try {
			self::db()->beginTransaction();
			
			if ( ! isset($data['system_order_id']) ) {
				$order_data['system_order_id'] = self::_getNewOrderId();
			}
			
			$order_data['status'] = self::ORDER_STATUS_PAYMENT_DETAILS_RECEIVED;
			$order_data['activation_condition'] = self::CONDITION_AFTER_PAID;
			$order_data['use_balance'] = 0;
			$order_data['backend_user_id']	= self::IVR_BACKEND_USER_ID;
			
			
			self::db()->insert('orders', $order_data);
			$order_id = self::db()->lastInsertId();
			
			foreach($packages_data as $package) {
				$package['order_id'] = $order_id;
				$package['status'] = self::PACKAGE_STATUS_PENDING;
				if ( $package['activation_type'] == 'specific_date' ) {
					$package['activation_type'] = self::ACTIVATE_AT_EXACT_DATE;
					$package['activation_date'] = date('Y-m-d H:i:00', $package['activation_date']);
				} else {
					$package['activation_type'] = self::ACTIVATE_ASAP;
					$package['activation_date'] = null;
				}
				
				$products = self::getProducts($package['package_id'], $package['application_id'], $package['is_agency'], $package['gender']);

				$additional_products = self::getAdditionalProducts($package['package_id'], $package['application_id'], $package['is_agency'], $package['gender']);
				unset($package['is_agency']);
				unset($package['gender']);
				
				self::db()->insert('order_packages', $package);
				$order_package_id = self::db()->lastInsertId();
				
				if ( count($products) > 0 )
				{
					foreach( $products as $product )
					{
						$package_product_data = array(
							'order_package_id' => $order_package_id,
							'product_id' => $product->id,
							'is_optional' => 0,
							'price' => $product->price
						);
						self::db()->insert('order_package_products', $package_product_data);
						
					}
				}
				if ( count($additional_products) > 0 )
				{
					foreach( $additional_products as $product )
					{
						$package_product_data = array(
							'order_package_id' => $order_package_id,
							'product_id' => $product->id,
							'is_optional' => 1,
							'price' => $product->price
						);
						self::db()->insert('order_package_products', $package_product_data);

					}
				}
			}
			
			//Collecting data to add transfer
			$transfer_data = array(
				'transfer_type_id'	=> self::TRANSFER_TYPE_PHONE_BILLING,
				'user_id'	=> $order_data['user_id'],
				'backend_user_id'	=> self::IVR_BACKEND_USER_ID,
				'date_transfered' => date('Y-m-d H:i:s'),
				'amount'	=> $order_data['price'],
				'application_id'	=> $order_data['application_id'],
				'status'			=> self::TRANSFER_STATUS_PENDING,
				'first_name'		=> 'IVR',
				'last_name'		=> 'IVR'
			);
			
			if ( $is_free ) {
				$transfer_data['phone_billing_paid'] = 1;
			}
			
			self::db()->insert('transfers', $transfer_data);
			$transfer_id =  self::db()->lastInsertId();
			
			self::db()->insert('transfer_orders', array('transfer_id' => $transfer_id, 'order_id' => $order_id));
			
			self::db()->commit();
		} catch(Exception $ex) {
			self::db()->rollBack();
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return $transfer_id;
	}
	
	public static function getProducts($package_id, $app_id, $user_type, $gender)
	{
		if ( $user_type ) {
			$user_type = USER_TYPE_AGENCY;
		}
		else {
			$user_type = USER_TYPE_SINGLE_GIRL;
		}

		$productss = self::db()->query('
			SELECT
				pp.product_id, p.name, pr.price
			FROM package_products pp
			INNER JOIN products p ON p.id = pp.product_id
			INNER JOIN product_prices pr ON pr.product_id = p.id
			WHERE pp.is_optional = 0 AND pp.package_id = ? AND pr.application_id = ? AND pr.user_type = ? AND pr.gender = ?
		', array($package_id, $app_id, $user_type, $gender))->fetchAll();


		$products = array();
		foreach($productss as $k => $product) {
			$products[$k]->id = $product->product_id;
			$products[$k]->name = Cubix_I18n::translate($product->name);
			$products[$k]->price = $product->price;
		}

		return $products;
	}
	public static function getAdditionalProducts($package_id, $app_id, $user_type, $gender)
	{
		if ( $user_type ) {
			$user_type = USER_TYPE_AGENCY;
		}
		else {
			$user_type = USER_TYPE_SINGLE_GIRL;
		}

		$productss = self::db()->query('
			SELECT
				pp.product_id, p.name, pr.price
			FROM package_products pp
			INNER JOIN products p ON p.id = pp.product_id
			INNER JOIN product_prices pr ON pr.product_id = p.id
			WHERE pp.is_optional = 1 AND pp.package_id = ? AND pr.application_id = ? AND pr.user_type = ? AND pr.gender = ?
		', array($package_id, $app_id, $user_type, $gender))->fetchAll();


		$products = array();
		foreach($productss as $k => $product) {
			$products[$k]->id = $product->product_id;
			$products[$k]->name = Cubix_I18n::translate($product->name);
			$products[$k]->price = $product->price;
		}

		return $products;
	}

	public static function getProduct($product_id)
	{
		$product = self::db()->fetchRow('
			SELECT p.name, pp.price, pp.crypto_price
			FROM products p
			INNER JOIN product_prices pp ON p.id = pp.product_id
			WHERE p.id = ?
		', array($product_id));
		
		return $product;
	}
	
	public static function getTransfer($transfer_id)
	{
		if ( is_array($transfer_id) ) {
			$transfer_id = reset($transfer_id);
		}
		$sql = 'SELECT id, amount FROM transfers WHERE id = ?';
		
		return self::db()->fetchRow($sql, array($transfer_id));
	}
	
	public function confirmTransfer($data)
	{
		$transfer_id = $data['transfer_id'];
		$amount = $data['amount'];
		$payment_id = $data['payment_id'];
		
		$transfer = self::getTransfer($transfer_id);
		if ( ! $transfer ) {
			self::addLog($transfer_id, $payment_id, 'No transfer with such id');
			return;
		}
		$transfer->amount = doubleval($transfer->amount);
		
		if ( $transfer->amount != $amount ) {
			self::addLog($transfer_id, $payment_id, 'Transfered and paid amounts are different');
			return;
		}
		
		//Checking if this payment already confirmed. Avoiding ivr repetition callbacks
		$is_already_confirmed = self::db()->fetchOne('
			SELECT TRUE 
			FROM transfers
			WHERE id = ? AND phone_billing_paid = 1
		', array($transfer->id));
		
		if ( $is_already_confirmed ) {
			return false;
		}
		
		try {
			$order = self::db()->fetchRow('
				SELECT o.* 
				FROM transfer_orders t
				INNER JOIN orders o ON o.id = t.order_id
				WHERE t.transfer_id = ? AND o.status = ? AND o.backend_user_id = ?', 
			array($transfer->id, self::ORDER_STATUS_PAYMENT_DETAILS_RECEIVED, self::IVR_BACKEND_USER_ID));
			
			if ( ! $order ) {
				self::addLog($transfer_id, $payment_id, 'No pending order for this transfer');
				return;
			}
			
			self::db()->beginTransaction();
			
			/*self::db()->insert('transfer_confirmations', array(
				'transfer_id' => $transfer->id,
				'backend_user_id' => self::IVR_BACKEND_USER_ID,
				'amount' => $transfer->amount,
				'date' => date('Y-m-d H:i:s')
			));*/
			
			self::db()->update('transfers', array('phone_billing_paid' => 1, 'date_transfered' => date('Y-m-d H:i:s') ), self::db()->quoteInto('id = ?', $transfer->id));
			//self::db()->update('orders', array('status' => self::ORDER_STATUS_PAID	), self::db()->quoteInto('id = ?', $order->id));
			/*$sms = new Cubix_2sms_Sms();
			$res = $sms->send('Order was bought by phone billing.', 'AND6', '0041796023276');*/
			self::db()->commit();
			
			self::addLog($transfer_id, $payment_id, 'Paid');
			
		} catch( Exception $ex ) {
			self::db()->rollBack();
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return true;
	}
	
	protected static function _generateOrderId()
	{
		$order_id = md5(microtime() * microtime() + microtime());
		$order_id = substr(strtoupper($order_id), 0, 8);

		return $order_id;
	}
	
	public static function isOrderIdExist($order_id)
	{
		$sql = "SELECT TRUE FROM orders WHERE system_order_id = ?";

		return self::db()->fetchOne($sql, array($order_id));
	}

	protected static function _getNewOrderId()
	{
		$order_id = self::_generateOrderId();
		while( self::isOrderIdExist($order_id) ) {
			$order_id = self::_generateOrderId();
		}
		return $order_id;
	}
	
	protected static function addLog($transfer_id, $payment_id = null,  $comment = "")
	{
		self::db()->insert('phone_billing_log', array(
			'transfer_id'	=> $transfer_id,
			'payment_id'	=> $payment_id,
			'comment'		=> $comment,
			'date'			=> date('Y-m-d H:i:s')
		));
	}
	
	const PHONE_PACKAGE_1_DAY_PASS = 19;
	const PHONE_PACKAGE_3_DAY_PASS = 20;
	const PHONE_PACKAGE_5_DAY_PASS = 23;
	
	public function deleteOrderByTransferId($transfer_id)
	{
		try {
			$order = self::db()->fetchRow('SELECT order_id AS id FROM transfer_orders WHERE transfer_id = ?', array($transfer_id));

			if ( ! $order ) return false;
			
			//return $order->id;
			//self::db()->beginTransaction();
			
			self::db()->query('DELETE FROM transfers WHERE id = ?', array($transfer_id));
			self::db()->query('DELETE FROM orders WHERE id = ?', array($order->id));
			
		} catch( Exception $e ) {
			
			//self::db()->rollBack();
			
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return true;
	}
	
	public function deletePhoneTransfersAndOrders($escort_id)
	{
		try {
			
			$not_active_order_ids = self::db()->fetchAll('
				SELECT order_id AS id FROM order_packages WHERE escort_id = ? AND package_id IN(?, ?, ?) AND status = ?
			', array($escort_id, self::PHONE_PACKAGE_1_DAY_PASS, self::PHONE_PACKAGE_3_DAY_PASS, self::PHONE_PACKAGE_5_DAY_PASS, self::PACKAGE_STATUS_PENDING));

			if ( ! count($not_active_order_ids) ) return false;
						
			foreach ( $not_active_order_ids as $order ) {				
				$transfer = self::db()->fetchRow('SELECT transfer_id AS id FROM transfer_orders WHERE order_id = ?', array($order->id));
				
				self::db()->query('DELETE FROM transfers WHERE id = ?', array($transfer->id));
				self::db()->query('DELETE FROM orders WHERE id = ?', array($order->id));
			}
			
		} catch( Exception $e ) {
			
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return true;
	}
	
	public function getEscortPhonePackageData($transfer_id)
	{		
		try {
			$order = self::db()->fetchRow('SELECT order_id AS id FROM transfer_orders WHERE transfer_id = ?', array($transfer_id));

			if ( ! $order ) return false;
			
			return self::db()->fetchRow('SELECT escort_id, activation_type, activation_date FROM order_packages WHERE order_id = ?', array($order->id));	
			
		} catch( Exception $e ) {			
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function getGotdBookedDays()
	{
		//Getting booked days from order_packages table
		//No need to select from order_packages, all in gotd_orders
		// $sql = '
		// 	SELECT DATE(gotd_activation_date) AS date, gotd_city_id AS city_id, escort_id
		// 	FROM order_packages
		// 	WHERE DATE(gotd_activation_date) >= DATE(NOW()) AND status = 2
		// 	ORDER BY gotd_city_id
		// ';
		// $order_packages_rows = self::db()->fetchAll($sql);

		//Getting booked days from gotd_orders
		$sql = '
			SELECT go.activation_date AS date, go.city_id, go.escort_id
			FROM gotd_orders go
			LEFT JOIN order_packages op ON go.order_package_id = op.id
			WHERE DATE(go.activation_date) >= DATE(NOW())
		';
		
		$rows = self::db()->fetchAll($sql, array());
		
		// $rows = array_merge($order_packages_rows, $gotd_orders_rows);
		
		//Grouping dates by city_id
		$result = array();
		
		if ( Cubix_Application::getId() == APP_BL ) {
			//Custom logic for bl. 10 gotd per day in one city  new jira (BEN-7)
			$dates_per_city = array();
			
			//in some cities allowed to have more then default gotd
			$exceptions = array(
				803	=> 15
			);
			
			foreach($rows as $row) {
				if ( ! isset($result[$row->city_id]) ) {
					$result[$row->city_id] = array();
				}
				
				if ( ! isset($dates_per_city[$row->city_id]) ) {
					$dates_per_city[$row->city_id] = array();
				}
				
				if ( ! isset($dates_per_city[$row->city_id][$row->date]) ) {
					$dates_per_city[$row->city_id][$row->date] = 1;
				} else {
					$dates_per_city[$row->city_id][$row->date] += 1;
				}
				
				if ( ($dates_per_city[$row->city_id][$row->date] >= 10 && is_null($exceptions[$row->city_id])) || ( $dates_per_city[$row->city_id][$row->date] >= $exceptions[$row->city_id]  && isset($exceptions[$row->city_id]) ) ) {
					$result[$row->city_id][] = array(
						'date' => $row->date,
						'escort_id'	=> $row->escort_id
					);
				}
			}
		} else {
			//in some cities allowed to have more then one gotd simultaneously
			$exceptions = array(
				431	=> 6,
				758	=> 3,
				669 => 3,
                621	=> 3
            );
			$in_cities = array();
			
			foreach($rows as $row) {
				if ( ! isset($result[$row->city_id]) ) {
					$result[$row->city_id] = array();
				}

				if ( ! is_null($exceptions[$row->city_id]) ) {
					if ( is_null($in_cities[$row->city_id]) ) $in_cities[$row->city_id] = array();
						
					if ( ! $in_cities[$row->city_id][$row->date] ) {
						$in_cities[$row->city_id][$row->date] = 1;
					} else {
						$in_cities[$row->city_id][$row->date] += 1;
					}
				}

				if ( is_null($exceptions[$row->city_id]) || $in_cities[$row->city_id][$row->date] >= $exceptions[$row->city_id]  ) {
					$result[$row->city_id][] = array(
						'date' => $row->date,
						'escort_id'	=> $row->escort_id
					);
				}
			}
		}
		
		
		
		
		return $result;
		
	}
	
	public function getVotdBookedDays()
	{
		$sql = '
			SELECT vo.activation_date AS date, vo.city_id, vo.escort_id
			FROM votd_orders vo
			LEFT JOIN order_packages op ON vo.order_package_id = op.id
			WHERE DATE(vo.activation_date) >= DATE(NOW())
		';
		
		$rows = self::db()->fetchAll($sql, array());
		
		$result = array();
		
		$in_cities = array();
		
		foreach($rows as $row) {
			if ( ! isset($result[$row->city_id]) ) {
				$result[$row->city_id] = array();
			}

			$result[$row->city_id][] = array(
				'date' => $row->date,
				'escort_id'	=> $row->escort_id
			);
		}
		
		return $result;
		
	}
	
	public function getEscortGotdBookedDays($escort_id, $current_gotd_id = NULL)
	{
		$sql_where = "";
		if($current_gotd_id){
			$sql_where  = self::db()->quoteInto(' AND go.id <> ?', $current_gotd_id);
		}
		$gotds = self::db()->fetchAll('
			SELECT go.id, go.activation_date AS date, go.city_id, go.escort_id
			FROM gotd_orders go
			WHERE DATE(go.activation_date) >= DATE(NOW()) AND (go.status = 1 OR go.status = 2) AND go.escort_id = ?
			'. $sql_where
		, array($escort_id));
		
		return $gotds;
	}

	public function getEscortVotdBookedDays($escort_id, $current_votd_id = NULL)
	{
		$sql_where = "";
		if($current_votd_id){
			$sql_where  = self::db()->quoteInto(' AND vo.id <> ?', $current_votd_id);
		}
		$votds = self::db()->fetchAll('
			SELECT vo.activation_date AS date, vo.city_id, vo.escort_id
			FROM votd_orders vo
			WHERE DATE(vo.activation_date) >= DATE(NOW()) AND (vo.status = 1 OR vo.status = 2) AND vo.escort_id = ?
			'. $sql_where
		, array($escort_id));
		
		return $votds;
	}
	
	public function bookGotd($escort_id, $city_id, $activation_dates, $client_cookie_id = null, $ip=null)
	{
		if ( ! $escort_id || ! $city_id || ! $activation_dates ) return false;
		
		$where = '';
		if (in_array(Cubix_Application::getId(), [APP_EF, APP_A6])){
			$where = ' AND order_id IS NOT NULL ';
		}
		
        if (Cubix_Application::getId() == APP_EG_CO_UK){
            $expiration_field = 'IF(expiration_date IS NOT NULL,UNIX_TIMESTAMP( expiration_date ),UNIX_TIMESTAMP( NOW()+ INTERVAL 1 YEAR )) AS expiration_date';
        }else{
            $expiration_field = 'expiration_date';
        }
		//Getting active/pending packages for this escort
		$sql = 'SELECT id, date_activated, '.$expiration_field.' FROM order_packages WHERE escort_id = ? AND status IN (1, 2) '. $where. ' ORDER BY id ASC';
		$order_packages = self::db()->fetchAll($sql, array($escort_id));
		
		$gotd_orders = array();
		foreach($activation_dates as $activation_date) {
			if ( $order_packages ) {
				
				if ( count($order_packages) == 1 ) {
					//If has one package means has no pending packages 
					//And gotd is bought for this package
					$order_package_id = $order_packages[0]->id;
				} else {
					//If has more than one package means has also pending package.
					//Must check for which one gotd is bought

                    if (Cubix_Application::getId() != APP_EG_CO_UK) {
                        $expiration_date = strtotime('-1 day', strtotime($order_packages[0]->expiration_date));//Fixing 1 day problem
                    }else{
                        $expiration_date = $order_packages[0]->expiration_date;
                    }

					//Adding 5 hours. $activateion_date comes with the same logic
					//FAST SOLUTION
					//Timestamp return -2 hours
					$expiration_date += 5*60*60;

					//First package is active one
					if ( $activation_date > $expiration_date ) {
						//Means bought for the next(pending) package
						$order_package_id = $order_packages[1]->id;
					} else {
						//Means bought for the current(active) package
						$order_package_id = $order_packages[0]->id;
					}
				}
			} else {
				$order_package_id = null;
			}

			$gotd_order =  array(
                'creation_date'		=> date('Y-m-d H:i:s', time()),
                'activation_date'	=> date('Y-m-d', $activation_date),
                'escort_id'			=> $escort_id,
                'city_id'			=> $city_id,
                'status'			=> self::GOTD_ORDER_PENDING,
                'order_package_id'	=> $order_package_id
            );

            /*if (Cubix_Application::getId() == APP_BL){
                $gotd_order['creation_date'] = gmdate('Y-m-d H:i:s', time());
            }*/

            if (Cubix_Application::getId() == APP_EF){
                $gotd_order['client_cookie_id'] = $client_cookie_id;
                $gotd_order['ip'] = $ip;
            }
			self::db()->insert('gotd_orders', $gotd_order);
			$gotd_orders[] = self::db()->lastInsertId();
		}

		return implode(':', $gotd_orders);
	}

	public function bookVotd($escort_id, $city_id, $activation_dates, $client_cookie_id = null, $ip=null)
	{
		if ( ! $escort_id || ! $city_id || ! $activation_dates ) return false;
		
		//Getting active/pending packages for this escort
		$sql = 'SELECT id, date_activated, expiration_date FROM order_packages WHERE escort_id = ? AND status IN (1, 2) ORDER BY id ASC';
		$order_packages = self::db()->fetchAll($sql, array($escort_id));
		
		$votd_orders = array();
		foreach($activation_dates as $activation_date) {
			if ( $order_packages ) {
				
				if ( count($order_packages) == 1 ) {
					//If has one package means has no pending packages 
					//And gotd is bought for this package
					$order_package_id = $order_packages[0]->id;
				} else {
					//If has more than one package means has also pending package.
					//Must check for which one gotd is bought

					$expiration_date = strtotime('-1 day', strtotime($order_packages[0]->expiration_date));//Fixing 1 day problem
					//Adding 5 hours. $activateion_date comes with the same logic
					//FAST SOLUTION
					//Timestamp return -2 hours
					$expiration_date += 5*60*60;

					//First package is active one
					if ( $activation_date > $expiration_date ) {
						//Means bought for the next(pending) package
						$order_package_id = $order_packages[1]->id;
					} else {
						//Means bought for the current(active) package
						$order_package_id = $order_packages[0]->id;
					}
				}
			} else {
				$order_package_id = null;
			}

			$votd_order =  array(
                'creation_date'		=> date('Y-m-d H:i:s', time()),
                'activation_date'	=> date('Y-m-d', $activation_date),
                'escort_id'			=> $escort_id,
                'city_id'			=> $city_id,
                'status'			=> self::GOTD_ORDER_PENDING,
                'order_package_id'	=> $order_package_id
            );

			self::db()->insert('votd_orders', $votd_order);
			$votd_orders[] = 'VD_' . self::db()->lastInsertId();
		}

		return implode(':', $votd_orders);
	}
	
	
	public function confirmGotd($id, $application_id, $additional_info)
	{	
		$ids = explode(':', $id);
		
		if ( ! count($ids) ) return false;
		
		$gotd_product_id = GIRL_OF_THE_DAY;
		if ( $application_id == APP_BL ) {
			$gotd_product_id = 16;
		}
		
		if($additional_info['payment_system'] == 'coinsome'){
			$gotd_product_price = intval(self::getProduct($gotd_product_id)->crypto_price);
		}
		else{
			$gotd_product_price = intval(self::getProduct($gotd_product_id)->price);
		}

		$row = self::db()->fetchRow('
			SELECT go.*, e.user_id, op.package_id
			FROM gotd_orders go
			INNER JOIN escorts e ON e.id = go.escort_id
			LEFT JOIN order_packages op ON op.id = go.order_package_id
			WHERE go.id = ?', array($ids[0])
		);
		
		if ( $application_id == APP_BL && $row->package_id == 9 ) {
			$gotd_product_price = 30;
		}

		if ($application_id == APP_EG_CO_UK && in_array($row->package_id, array(105, 43, 106)))
        {
            $gotd_product_price = GIRL_OF_THE_DAY_WITHOUT_PACKAGE_PRICE;
        }

        if ($application_id == APP_ED && in_array($row->package_id, array(125,102,128,130,132,107,105)))
        {
            $gotd_product_price = GIRL_OF_THE_DAY_WITHOUT_PACKAGE_PRICE;
        }

		$total_amount = $gotd_product_price * count($ids);

		if ( ! $row ) return false;

        switch ($application_id) {
            case APP_EG_CO_UK:
                $salesId = 199;
                break;
            case APP_ED:
                $salesId = 216;
                break;
            default:
                $salesId = self::IVR_GOTD_BACKEND_USER_ID;
        }

		try {
			self::db()->beginTransaction();
			
			$order_package = self::db()->fetchRow('
				SELECT id, order_id
				FROM order_packages 
				WHERE id = ?
			', array($row->order_package_id));
			
			//-->Increasing order price by price of GOTD product
			self::db()->query('
				UPDATE orders
				SET price = price + ?
				WHERE id = ?
			', array($total_amount, $order_package->order_id));
			
			self::db()->query('
				UPDATE order_packages
				SET price = price + ?
				WHERE id = ?
			', array($total_amount, $order_package->id));
			//<--
			//
			//-->Inserting new transfer
			$transfer_data = array(
				'transfer_type_id'	=> self::TRANSFER_TYPE_PHONE_BILLING,
				'user_id'			=> $row->user_id,
				'backend_user_id'	=> $salesId,
				'date_transfered'	=> date('Y-m-d H:i:s'),
				'amount'			=> $total_amount,
				'application_id'	=> $application_id,
				'status'			=> self::TRANSFER_STATUS_CONFIRMED,
				'first_name'		=> 'IVR-Gotd',
				'last_name'			=> 'IVR-Gotd',
				'phone_billing_paid'	=> 1
			);
			if ( $application_id != APP_A6 ) {
				unset($transfer_data['phone_billing_paid']);
				$transfer_data['transfer_type_id'] = self::TRANSFER_TYPE_CREDIT_CARD;
				$transfer_data['is_self_checkout'] = 1;
				$transfer_data['transaction_id'] = $additional_info['transaction_id'];
			}

			if ($application_id == APP_EF){
                $transfer_data['client_cookie_id'] = $row->client_cookie_id;
                $transfer_data['ip'] = $row->ip;
            }
			
			if ($additional_info['payment_system']) {
				$transfer_data['payment_system'] = $additional_info['payment_system'];
			}
			
			if ($additional_info['first_name']) {
				$transfer_data['first_name'] = $additional_info['first_name'];
				$transfer_data['last_name'] = 'Gotd';
			}
			
			self::db()->insert('transfers', $transfer_data);
			
			$transfer_id = self::db()->lastInsertId();
			
			if ( in_array($application_id, array(APP_A6, APP_BL, APP_EF, APP_EG_CO_UK)) ) {
				self::db()->insert('transfer_confirmations', array(
					'transfer_id' => $transfer_id,
					'backend_user_id' => self::IVR_GOTD_BACKEND_USER_ID,// this user is also for epg payments
					'amount' => doubleval($total_amount),
					'date' => new Zend_Db_Expr('NOW()')
				));
			}
			
			if($order_package->order_id){
				self::db()->insert('transfer_orders', array(
					'order_id'	=> $order_package->order_id,
					'transfer_id'	=> $transfer_id
				));
			}
			//<--
			
			//-->Seting status paid 
			if ( $additional_info['order_package_id'] ) {
				self::db()->query('UPDATE gotd_orders SET status = ?, transfer_id = ?, order_package_id = ? WHERE id IN (' . implode(',', $ids) . ')', array(self::GOTD_ORDER_PAID, $transfer_id, $additional_info['order_package_id']));
			} else {
				self::db()->query('UPDATE gotd_orders SET status = ?, transfer_id = ? WHERE id IN (' . implode(',', $ids) . ')', array(self::GOTD_ORDER_PAID, $transfer_id));
			}


			// if($application_id == APP_A6) {
			// 	try{
			// 	    $gotd_orders = self::db()->fetchAll('
			// 	    	SELECT go.activation_date AS date, c.title_en AS city, u.email AS email
			// 	    	FROM gotd_orders AS go
			// 	    	INNER JOIN cities c ON c.id = go.city_id
			// 	    	INNER JOIN users u ON u.id = ?
			// 	    	WHERE go.id IN (' . implode(',', $ids) . ') ', array($row->user_id));

			// 	    foreach ($gotd_orders as $i => $gotd_order) {
			// 	    	// if($gotd_order->email) {
			// 	    	// 	Cubix_Email::sendTemplate('gotd_purchase_confirm', $gotd_order->email, array(
			// 	    	// 	    'city' => $gotd_order->city,
			// 	    	// 	    'date' => $gotd_order->date
			// 	    	// 	));
			// 	    	// }

			// 	    	// Cubix_Email::sendTemplate('gotd_purchase_confirm', 'yerem1992@gmail.com', array(
			// 	    	//     'city' => $gotd_order->city,
			// 	    	//     'date' => $gotd_order->date
			// 	    	// ));
			// 	    }
			// 	}catch (Exception $e){

			// 	}
			// }

			//<--		
			
			self::db()->commit();
		} catch(Exception $ex) {
			self::db()->rollBack();
			return $ex;
		}
		
		
		$data = self::db()->fetchRow('
			SELECT e.id, e.showname, c.title_en AS city, group_concat(go.activation_date) AS activation_date, bu.email AS sales_email, u.email as email, group_concat(c.title_en) AS group_city
			FROM gotd_orders go
			INNER JOIN escorts e ON e.id = go.escort_id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			INNER JOIN cities c ON c.id = go.city_id
			WHERE go.id IN (' . implode(',', $ids) . ')
			GROUP BY escort_id
		');
		
		return $data;
	}

	public function confirmVotd($id, $application_id, $additional_info)
	{	
		$ids = explode(':', $id);
		
		if ( ! count($ids) ) return false;
		
		$votd_product_id = VIDEO_OF_THE_DAY;
		
		$votd_product_price = intval(self::getProduct($votd_product_id)->price);

		$row = self::db()->fetchRow('
			SELECT vo.*, e.user_id, op.package_id
			FROM votd_orders vo
			INNER JOIN escorts e ON e.id = vo.escort_id
			LEFT JOIN order_packages op ON op.id = vo.order_package_id
			WHERE vo.id = ?', array($ids[0])
		);
		

		$total_amount = $votd_product_price * count($ids);

		if ( ! $row ) return false;
		
		try {
			self::db()->beginTransaction();
			
			$order_package = self::db()->fetchRow('
				SELECT id, order_id
				FROM order_packages 
				WHERE id = ?
			', array($row->order_package_id));
			
			//-->Increasing order price by price of VOTD product
			self::db()->query('
				UPDATE orders
				SET price = price + ?
				WHERE id = ?
			', array($total_amount, $order_package->order_id));
			
			self::db()->query('
				UPDATE order_packages
				SET price = price + ?
				WHERE id = ?
			', array($total_amount, $order_package->id));
			//<--
			//
			//-->Inserting new transfer
			$transfer_data = array(
				'transfer_type_id'	=> self::TRANSFER_TYPE_PHONE_BILLING,
				'user_id'			=> $row->user_id,
				'backend_user_id'	=> self::IVR_VOTD_BACKEND_USER_ID,// this user is also for epg payments
				'date_transfered'	=> date('Y-m-d H:i:s'),
				'amount'			=> $total_amount,
				'application_id'	=> $application_id,
				'status'			=> self::TRANSFER_STATUS_CONFIRMED,
				'first_name'		=> 'IVR-Votd',
				'last_name'			=> 'IVR-Votd',
				'phone_billing_paid'	=> 1
			);
			
			if ($additional_info['payment_system']) {
				$transfer_data['payment_system'] = $additional_info['payment_system'];
			}
			
			if ($additional_info['first_name']) {
				$transfer_data['first_name'] = $additional_info['first_name'];
				$transfer_data['last_name'] = 'Votd';
			}
			
			self::db()->insert('transfers', $transfer_data);
			
			$transfer_id = self::db()->lastInsertId();
			
			if ( in_array($application_id, array(APP_A6, APP_BL, APP_EF)) ) {
				self::db()->insert('transfer_confirmations', array(
					'transfer_id' => $transfer_id,
					'backend_user_id' => self::IVR_VOTD_BACKEND_USER_ID,// this user is also for epg payments
					'amount' => doubleval($total_amount),
					'date' => new Zend_Db_Expr('NOW()')
				));
			}
			
			self::db()->insert('transfer_orders', array(
				'order_id'	=> $order_package->order_id,
				'transfer_id'	=> $transfer_id
			));
			//<--
			
			//-->Seting status paid 
			if ( $additional_info['order_package_id'] ) {
				self::db()->query('UPDATE votd_orders SET status = ?, transfer_id = ?, order_package_id = ? WHERE id IN (' . implode(',', $ids) . ')', array(self::VOTD_ORDER_PAID, $transfer_id, $additional_info['order_package_id']));
			} else {
				self::db()->query('UPDATE votd_orders SET status = ?, transfer_id = ? WHERE id IN (' . implode(',', $ids) . ')', array(self::VOTD_ORDER_PAID, $transfer_id));
			}
			
			self::storeCardInfo($row->user_id, array(
				'card_number' => $additional_info['card_number'], 
				'card_name' => $additional_info['card_name'], 
				'gateway' => $additional_info['gateway'],
				'token'	=> $additional_info['token'],
				'netbanx_profile_id'	=> $additional_info['netbanx_profile_id']
			));
			
			self::db()->commit();
		} catch(Exception $ex) {
			self::db()->rollBack();
			return $ex;
		}
		
		
		$data = self::db()->fetchRow('
			SELECT e.id, e.showname, c.title_en AS city, group_concat(vo.activation_date) AS activation_date, bu.email AS sales_email, u.email as email, group_concat(c.title_en) AS group_city
			FROM votd_orders vo
			INNER JOIN escorts e ON e.id = vo.escort_id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN backend_users bu ON bu.id = u.sales_user_id
			INNER JOIN cities c ON c.id = vo.city_id
			WHERE vo.id IN (' . implode(',', $ids) . ')
			GROUP BY escort_id
		');
		
		return $data;
	}
	
	public function addBubblePayment($data)
	{
		$tmp_bubble_id = $data['ref_id'];
		
		$tmp_bubble = self::db()->fetchRow('SELECT escort_id, text, 1 AS status, approvation FROM escort_paid_bubbletexts WHERE id = ?', array($tmp_bubble_id));
		
		$tr_data = array('escort_id' => $tmp_bubble->escort_id, 'amount' => $data['amount'], 'payment_id' => $data['payment_id']);		
		self::db()->insert('bubble_phone_transfers', $tr_data);
		
		self::db()->update('escort_bubbletexts', array('status' => 0), array(self::db()->quoteInto('escort_id = ?', $tmp_bubble->escort_id), self::db()->quoteInto('status = ?', 1)) );
		self::db()->insert('escort_bubbletexts', (array) $tmp_bubble);
		
		self::db()->delete('escort_paid_bubbletexts', self::db()->quoteInto('id = ?', $tmp_bubble_id));
		
		/*self::db()->insert('bubble_phone_transfers', $data);
		
		$transfer_id = self::db()->lastInsertId();
		
		self::db()->update('escorts', array('bubble_transfer_id' => $transfer_id), self::db()->quoteInto('id = ?', $escort_id));*/
	}
	
	public function findTransactionId($transaction_id)
	{		
		$transfer = self::db()->fetchOne('SELECT TRUE FROM transfers WHERE transaction_id = ?', array($transaction_id));
		$spec_transfer = self::db()->fetchOne('SELECT TRUE FROM special_transfers WHERE transaction_id = ?', array($transaction_id));
		
		return $transfer || $spec_transfer;
	}
	
	public function getPremiumCities($escort_id)
	{
		
	}

    public function getEscortPackagesForGotdEG($escort_id, $application_id)
    {
        $sql = '
        SELECT
            op.id AS order_package_id,
            op.period,
            UNIX_TIMESTAMP( op.date_activated ) AS date_activated,
            UNIX_TIMESTAMP( op.expiration_date ) AS expiration_date,
            UNIX_TIMESTAMP( op.activation_date ) AS activation_date,
            op.package_id,
            op.activation_type,
            op.STATUS,
            e.STATUS AS escort_status,
            p.NAME AS package_name 
        FROM
            order_packages op
            INNER JOIN escorts e ON e.id = op.escort_id
            INNER JOIN packages p ON op.package_id = p.id
            INNER JOIN package_products pp ON pp.package_id = p.id 
        WHERE
            e.id = ? 
            AND ( e.STATUS & ? OR e.STATUS & ? ) 
            AND ( op.STATUS = ? OR op.STATUS = ? ) 
            AND pp.product_id = 18
        ORDER BY
            op.date_activated DESC
		';

        $packages = self::db()->fetchAll($sql, array(
            $escort_id,
            self::ESCORT_STATUS_ACTIVE,
            self::ESCORT_STATUS_OWNER_DISABLED,
            self::PACKAGE_STATUS_ACTIVE,
            self::PACKAGE_STATUS_PENDING,
        ));

        if ( ! $packages ) return false;


        if ( $packages[0]->activation_type == self::ACTIVATE_AT_EXACT_DATE && ! $packages[0]->expiration_date ) {
            if ( $packages[0]->package_id != self::PHONE_PACKAGE_1_DAY_PASS && $packages[0]->package_id != self::PHONE_PACKAGE_3_DAY_PASS ) {
                $packages[0]->activation_date = strtotime(date('Y-m-d', $packages[0]->activation_date));
                $packages[0]->period -= 1;
            }

            $packages[0]->date_activated = $packages[0]->activation_date;
            $packages[0]->expiration_date = $packages[0]->activation_date + 60 * 60 * 24 * $packages[0]->period;
        } else {
            $packages[0]->expiration_date -= 60 * 60 * 24 ;//Fixing expiration date
            $packages[0]->expiration_date += 60 * 60 * 5; //adding 5 hours to avoid timezone diff
        }

        if (count($packages) == 1 && in_array($packages[0]->package_id,[43, 44]))
        {
            $packages[0]->expiration_date = strtotime(date('Y-m-d', time() + 60 * 60 * 24 * 30));
        }

        unset($packages[0]->activation_date);


        if ( count($packages) > 1 ) {
            //The case when user has active package and one pending package with activation type after_current_package_expires
            //packages[0] is the active package, packages[1] is the pending package
            if ( $packages[1]->activation_type == self::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES && ! $packages[1]->expiration_date ) {
                $packages[1]->date_activated = $packages[0]->expiration_date + 60 * 60 * 24;
                $packages[1]->expiration_date = $packages[0]->expiration_date + 60 * 60 * 24 * ($packages[1]->period) + 60 * 60 * 5; //adding 5 hours to avoid timezone diff
            } else if ( $packages[1]->activation_type == self::ACTIVATE_AT_EXACT_DATE && ! $packages[1]->expiration_date ) {
                $packages[1]->date_activated = strtotime(date('Y-m-d', $packages[1]->activation_date));
                $packages[1]->expiration_date = $packages[1]->activation_date + 60 * 60 * 24 * ($packages[1]->period);
            }else if ($packages[1]->activation_type == self::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES && $packages[1]->expiration_date){
                $packages[1]->date_activated = strtotime(date('Y-m-d', $packages[0]->expiration_date + 60 * 60 * 24));//EGUK-251
            }
            else {
                $packages[1]->expiration_date -= 60 * 60 * 24;//Fixing expiration date
            }

            unset($packages[1]->activation_date);
        }


        return $packages;
    }

    public function getEscortPackagesForGotdED($escort_id, $application_id)
    {
        $sql = '
        SELECT
            op.id AS order_package_id,
            op.period,
            UNIX_TIMESTAMP( op.date_activated ) AS date_activated,
            UNIX_TIMESTAMP( op.expiration_date ) AS expiration_date,
            UNIX_TIMESTAMP( op.activation_date ) AS activation_date,
            op.package_id,
            op.activation_type,
            op.STATUS,
            e.STATUS AS escort_status,
            p.NAME AS package_name 
        FROM
            order_packages op
            INNER JOIN escorts e ON e.id = op.escort_id
            INNER JOIN packages p ON op.package_id = p.id
            INNER JOIN package_products pp ON pp.package_id = p.id 
        WHERE
            e.id = ? 
            AND ( e.STATUS & ? OR e.STATUS & ? ) 
            AND ( op.STATUS = ? OR op.STATUS = ? ) 
            AND pp.product_id = 16
        ORDER BY
            op.date_activated DESC
		';

        $packages = self::db()->fetchAll($sql, array(
            $escort_id,
            self::ESCORT_STATUS_ACTIVE,
            self::ESCORT_STATUS_OWNER_DISABLED,
            self::PACKAGE_STATUS_ACTIVE,
            self::PACKAGE_STATUS_PENDING,
        ));

        if ( ! $packages ) return false;


        if ( $packages[0]->activation_type == self::ACTIVATE_AT_EXACT_DATE && ! $packages[0]->expiration_date ) {
            if ( $packages[0]->package_id != self::PHONE_PACKAGE_1_DAY_PASS && $packages[0]->package_id != self::PHONE_PACKAGE_3_DAY_PASS ) {
                $packages[0]->activation_date = strtotime(date('Y-m-d', $packages[0]->activation_date));
                $packages[0]->period -= 1;
            }

            $packages[0]->date_activated = $packages[0]->activation_date;
            $packages[0]->expiration_date = $packages[0]->activation_date + 60 * 60 * 24 * $packages[0]->period;
        }elseif (!$packages[0]->activation_type  && $packages[0]->date_activated && ! $packages[0]->expiration_date ){
            $packages[0]->activation_date = $packages[0]->date_activated;
            $packages[0]->expiration_date = $packages[0]->activation_date + 60 * 60 * 24 * 365;
        } else {
            $packages[0]->expiration_date -= 60 * 60 * 24 ;//Fixing expiration date
            $packages[0]->expiration_date += 60 * 60 * 5; //adding 5 hours to avoid timezone diff
        }

        if (count($packages) == 1 && in_array($packages[0]->package_id,[43, 44]))
        {
            $packages[0]->expiration_date = strtotime(date('Y-m-d', time() + 60 * 60 * 24 * 30));
        }

        unset($packages[0]->activation_date);


        if ( count($packages) > 1 ) {
            //The case when user has active package and one pending package with activation type after_current_package_expires
            //packages[0] is the active package, packages[1] is the pending package
            if ( $packages[1]->activation_type == self::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES && ! $packages[1]->expiration_date ) {
                $packages[1]->date_activated = $packages[0]->expiration_date + 60 * 60 * 24;
                $packages[1]->expiration_date = $packages[0]->expiration_date + 60 * 60 * 24 * ($packages[1]->period) + 60 * 60 * 5; //adding 5 hours to avoid timezone diff
            } else if ( $packages[1]->activation_type == self::ACTIVATE_AT_EXACT_DATE && ! $packages[1]->expiration_date ) {
                if ( $packages[1]->date_activated == $packages[0]->expiration_date )
                {
                    $packages[1]->date_activated = $packages[0]->expiration_date + 60 * 60 * 24;
                }else{
                    $packages[1]->date_activated = strtotime(date('Y-m-d', $packages[1]->activation_date));
                }
                $packages[1]->expiration_date = $packages[1]->activation_date + 60 * 60 * 24 * ($packages[1]->period);
            }else if ($packages[1]->activation_type == self::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES && $packages[1]->expiration_date){
                $packages[1]->date_activated = strtotime(date('Y-m-d', $packages[0]->expiration_date + 60 * 60 * 24));//EGUK-251
            }
            else {
                $packages[1]->expiration_date -= 60 * 60 * 24;//Fixing expiration date
            }

            unset($packages[1]->activation_date);
        }


        return $packages;
    }
	
	public function getEscortPackagesForGotd($escort_id, $application_id) 
	{
		
		$gotd_product_id = GIRL_OF_THE_DAY;
		if ( in_array($application_id,array(APP_BL,APP_ED))) {
			$gotd_product_id = 16;
		}
		$where = ' AND pp.product_id = ?';
		if($application_id == APP_A6){
			$votd_product_id = 22;
			$where .= ' OR pp.product_id = ' . $votd_product_id;
		}
		$sql = '
			SELECT 
				op.id AS order_package_id, op.period, UNIX_TIMESTAMP(op.date_activated) AS date_activated, UNIX_TIMESTAMP(op.expiration_date) AS expiration_date,
				UNIX_TIMESTAMP(op.activation_date) AS activation_date, op.package_id, op.activation_type, op.status, e.status AS escort_status, 
				p.name AS package_name
			FROM order_packages op
			INNER JOIN escorts e ON e.id = op.escort_id
			INNER JOIN orders o ON o.id = op.order_id
			INNER JOIN packages p ON op.package_id = p.id
			INNER JOIN package_products pp ON pp.package_id = p.id
			WHERE 
				e.id = ? AND ( e.status & ? OR e.status & ? )  ' . $where . ' AND
				(
					op.status = ? OR op.status = ?
				) AND
				o.status IN (' . implode(',', array(self::ORDER_STATUS_PAID, self::ORDER_STATUS_PAYMENT_DETAILS_RECEIVED)) . ')
			ORDER BY op.date_activated DESC
		';

		$packages = self::db()->fetchAll($sql, array(
			$escort_id,
			self::ESCORT_STATUS_ACTIVE,
			self::ESCORT_STATUS_OWNER_DISABLED,
			$gotd_product_id,
			self::PACKAGE_STATUS_ACTIVE,
			self::PACKAGE_STATUS_PENDING,
		));

		if ( ! $packages ) return false;

		
		if ( $packages[0]->activation_type == self::ACTIVATE_AT_EXACT_DATE && ! $packages[0]->expiration_date ) {
			if ( $packages[0]->package_id != self::PHONE_PACKAGE_1_DAY_PASS && $packages[0]->package_id != self::PHONE_PACKAGE_3_DAY_PASS ) {
				$packages[0]->activation_date = strtotime(date('Y-m-d', $packages[0]->activation_date));
				$packages[0]->period -= 1;
			}
			
			$packages[0]->date_activated = $packages[0]->activation_date;
			$packages[0]->expiration_date = $packages[0]->activation_date + 60 * 60 * 24 * $packages[0]->period;
		} else {
			$packages[0]->expiration_date -= 60 * 60 * 24 ;//Fixing expiration date
			$packages[0]->expiration_date += 60 * 60 * 5; //adding 5 hours to avoid timezone diff
		}
		unset($packages[0]->activation_date);
		
		
		if ( count($packages) > 1 ) {
			//The case when user has active package and one pending package with activation type after_current_package_expires
			//packages[0] is the active package, packages[1] is the pending package
			if ( $packages[1]->activation_type == self::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES && ! $packages[1]->expiration_date ) {
				$packages[1]->date_activated = $packages[0]->expiration_date + 60 * 60 * 24;
				$packages[1]->expiration_date = $packages[0]->expiration_date + 60 * 60 * 24 * ($packages[1]->period) + 60 * 60 * 5; //adding 5 hours to avoid timezone diff
			} else if ( $packages[1]->activation_type == self::ACTIVATE_AT_EXACT_DATE && ! $packages[1]->expiration_date ) {
				$packages[1]->date_activated = strtotime(date('Y-m-d', $packages[1]->activation_date));
				$packages[1]->expiration_date = $packages[1]->activation_date + 60 * 60 * 24 * ($packages[1]->period - 1);
			} else {
				$packages[1]->expiration_date -= 60 * 60 * 24;//Fixing expiration date
			}
			
			unset($packages[1]->activation_date);
		}
		
		
		return $packages;
	}
	
	//Getting escorts with their packages
	public function getAgencyEscortsForGotd($agency_id)
	{
		ini_set('memory_limit', '2048M');

		$sql = '
			SELECT
				e.id,
				e.showname,
				e.gender,
				u.application_id,
				e.verified_status,
				op.order_id,
				op.package_id,
				op.status,
				op.status AS package_status,
				op.package_id,
				op.activation_type,
				UNIX_TIMESTAMP(op.date_activated) AS date_activated,
				UNIX_TIMESTAMP(op.expiration_date) AS expiration_date,
				UNIX_TIMESTAMP(op.activation_date) AS activation_date,
				op.period,
				p.name AS package_name,
				p.id AS package_id
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN order_packages op ON op.escort_id = e.id
			INNER JOIN orders o ON o.id = op.order_id
			INNER JOIN packages p ON p.id = op.package_id
			WHERE 
				e.agency_id = ? AND e.status = ? AND 
				(op.status = ? OR op.status = ? ) AND
				o.status IN (' . implode(',', array(self::ORDER_STATUS_PAID, self::ORDER_STATUS_PAYMENT_DETAILS_RECEIVED)) . ')
 			ORDER BY e.id, op.id
		';
		
		try {
			$rows = self::db()->query($sql, array(
				$agency_id, 
				self::ESCORT_STATUS_ACTIVE, 
				self::PACKAGE_STATUS_ACTIVE,
				self::PACKAGE_STATUS_PENDING
			))->fetchAll();
			
			$escorts = array();
			
			foreach($rows as $i => $row) {
				if ( $row->activation_type == self::ACTIVATE_AT_EXACT_DATE && ! $row->expiration_date ) {
					$row->date_activated = $row->activation_date;
					$row->expiration_date = $row->date_activated + 60 * 60 * 24 * $row->period;
				} elseif ( $row->activation_type == self::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES && ! $row->expiration_date ) {
					$row->date_activated = $rows[$i - 1]->expiration_date + 60 * 60 * 24;
					$row->expiration_date = $row->date_activated + 60 * 60 * 24 * $row->period;
				} else {
					$row->expiration_date -= 60 * 60 * 24;//Fixing expiration date
				}
				
				unset($row->activation_date);
				unset($row->period);
				
				
				if ( ! $escorts[$row->id] ) {
					$escorts[$row->id] = $row;
					$escorts[$row->id]->packages = array();
				}
				
				$escorts[$row->id]->packages[] = array(
					'date_activated' => $row->date_activated,
					'expiration_date'	=> $row->expiration_date,
					'activation_type'	=> $row->activation_type,
					'package_name'		=> $row->package_name,
					'package_id'		=> $row->package_id
				);
			}
			
			return $escorts;
			
			
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

    public function getAgencyEscortsForGotdEG($agency_id)
    {
        ini_set('memory_limit', '2048M');

        $sql = '
			SELECT
				e.id,
				e.showname,
				e.gender,
				u.application_id,
				e.verified_status,
				op.order_id,
				op.package_id,
				op.status,
				op.status AS package_status,
				op.package_id,
				op.activation_type,
				UNIX_TIMESTAMP(op.date_activated) AS date_activated,
				UNIX_TIMESTAMP( op.expiration_date ) AS expiration_date,
				UNIX_TIMESTAMP(op.activation_date) AS activation_date,
				op.period,
				p.name AS package_name,
				p.id AS package_id
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN order_packages op ON op.escort_id = e.id
			INNER JOIN packages p ON p.id = op.package_id
			WHERE 
				e.agency_id = ? AND e.status = ? AND 
				(op.status = ? OR op.status = ? )
				AND op.application_id = ?
 			ORDER BY e.id, op.id
		';

        try {
            $rows = self::db()->query($sql, array(
                $agency_id,
                self::ESCORT_STATUS_ACTIVE,
                self::PACKAGE_STATUS_ACTIVE,
                self::PACKAGE_STATUS_PENDING,
                Cubix_Application::getId()
            ))->fetchAll();

            $escorts = array();

            foreach($rows as $i => $row) {
                if ( $row->activation_type == self::ACTIVATE_AT_EXACT_DATE && ! $row->expiration_date ) {
                    $row->date_activated = $row->activation_date;
                    $row->expiration_date = $row->date_activated + 60 * 60 * 24 * $row->period;
                } elseif ( $row->activation_type == self::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES && ! $row->expiration_date ) {
                    $row->date_activated = $rows[$i - 1]->expiration_date + 60 * 60 * 24;
                    $row->expiration_date = $row->date_activated + 60 * 60 * 24 * $row->period;
                } else {
                    $row->expiration_date -= 60 * 60 * 24;//Fixing expiration date
                }

                if ( in_array($row->package_id, [43, 44, 105, 106, 107]) )
                {
                    $row->expiration_date = strtotime(date('Y-m-d', time() + 60 * 60 * 24 * 30));
                }

                unset($row->activation_date);
                unset($row->period);


                if ( ! $escorts[$row->id] ) {
                    $escorts[$row->id] = $row;
                    $escorts[$row->id]->packages = array();
                }

                $escorts[$row->id]->packages[] = array(
                    'date_activated' => $row->date_activated,
                    'expiration_date'	=> $row->expiration_date,
                    'activation_type'	=> $row->activation_type,
                    'package_name'		=> $row->package_name,
                    'package_id'		=> $row->package_id
                );
            }

            return $escorts;


        }
        catch ( Exception $e ) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

    public function getAgencyEscortsForGotdED($agency_id)
    {
        ini_set('memory_limit', '2048M');

        $sql = '
			SELECT
				e.id,
				e.showname,
				e.gender,
				u.application_id,
				e.verified_status,
				op.order_id,
				op.package_id,
				op.status,
				op.status AS package_status,
				op.package_id,
				op.activation_type,
				UNIX_TIMESTAMP(op.date_activated) AS date_activated,
				UNIX_TIMESTAMP(op.expiration_date) AS expiration_date,
				UNIX_TIMESTAMP(op.activation_date) AS activation_date,
				op.period,
				p.name AS package_name,
				p.id AS package_id
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN order_packages op ON op.escort_id = e.id
			INNER JOIN packages p ON p.id = op.package_id
			WHERE 
				e.agency_id = ? AND ( e.status = ? OR e.status & ? ) AND 
				(op.status = ? OR op.status = ? ) AND e.gender = 1
 			ORDER BY e.id, op.id
		';

        try {
            $rows = self::db()->query($sql, array(
                $agency_id,
                self::ESCORT_STATUS_ACTIVE,
                self::ESCORT_STATUS_OWNER_DISABLED,
                self::PACKAGE_STATUS_ACTIVE,
                self::PACKAGE_STATUS_PENDING
            ))->fetchAll();

            $escorts = array();

            foreach($rows as $i => $row) {
                $cities = self::db()->query('SELECT ec.city_id, c.title_en as title FROM escort_cities ec INNER JOIN cities c ON c.id = ec.city_id WHERE escort_id = ?',array($row->id))->fetchAll();
                if ( $row->activation_type == self::ACTIVATE_AT_EXACT_DATE && ! $row->expiration_date ) {
                    $row->date_activated = $row->activation_date;
                    $row->expiration_date = $row->date_activated + 60 * 60 * 24 * $row->period;
                } elseif ( $row->activation_type == self::ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES && ! $row->expiration_date ) {
                    $row->date_activated = $rows[$i - 1]->expiration_date + 60 * 60 * 24;
                    $row->expiration_date = $row->date_activated + 60 * 60 * 24 * $row->period;
                } else {
                    $row->expiration_date -= 60 * 60 * 24;//Fixing expiration date
                }

                if ( in_array($row->package_id,array(125,102,128,130,132,107,105,97)) )
                {
                    if ($row->date_activated > time() - 60 * 60 * 24 * 365)
                    {
                        $row->expiration_date = $row->date_activated + 60 * 60 * 24 * 365;
                    }else{
                        $row->expiration_date = time() + 60 * 60 * 24 * 365;
                    }
                }

                unset($row->activation_date);
                unset($row->period);


                if ( ! $escorts[$row->id] ) {
                    $escorts[$row->id] = $row;
                    $escorts[$row->id]->packages = array();
                }

                $escorts[$row->id]->packages[] = array(
                    'date_activated' => $row->date_activated,
                    'expiration_date'	=> $row->expiration_date,
                    'activation_type'	=> $row->activation_type,
                    'package_name'		=> $row->package_name,
                    'package_id'		=> $row->package_id
                );

                foreach ($cities as $city)
                {
                    $escorts[$row->id]->cities[] = $city;
                }
            }

            return $escorts;


        }
        catch ( Exception $e ) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

	public static function storeCardInfo($user_id, $data)
	{
		try {
			if ( $data['token'] && strlen($data['card_number']) ) {
				$is_exists = parent::db()->fetchOne('
					SELECT TRUE FROM user_credit_cards WHERE user_id = ? AND number = ? AND gateway = ?
					', array($user_id, $data['card_number'], $data['gateway']));

				preg_match('/master|visa|amex/', strtolower($data['card_name']), $matches);

				if ( ! $is_exists ) {
					parent::db()->insert('user_credit_cards', array(
						'user_id'	=> $user_id,
						'number'	=> $data['card_number'],
						'token' => $data['token'],
						'gateway'	=> $data['gateway'],
						'type'	=> $matches[0]
					));
				}
			}

			if ( $data['netbanx_profile_id'] && strlen($data['netbanx_profile_id']) ) {
				$is_exists = parent::db()->fetchOne('SELECT TRUE FROM netbanx_profile_ids WHERE user_id = ?', array($user_id));
				if ( ! $is_exists ) {
					parent::db()->insert('netbanx_profile_ids', array(
						'user_id'	=> $user_id,
						'profile_id'	=> $data['netbanx_profile_id']
					));
				}
			}

		} catch(Exception $ex) {
		}
	}

	public function storeIvrSc($data)
	{
		parent::db()->insert('ivr_callbacks', $data);
	}		
				
	// for and6.ch ONLY
	public function getSmsReceivers($mode = null)
	{
		if (in_array($mode, array(1, 2))) //1 - gotd, 2 - daypass
		{
			if ($mode == 1)
				$type = 'gotd';
			else
				$type = 'daypass';
		}
		else
			$type = 'other';

		$res = self::db()->query('SELECT number FROM receivers WHERE ' . $type . ' = 1')->fetchAll();

		$numbers = array();

		if ($res)
		{
			foreach ($res as $r)
			{
				$numbers[] = $r->number;
			}
		}

		return $numbers;
	}
}
