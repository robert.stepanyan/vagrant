<?php

class Cubix_Api_Module_March8Lottery extends Cubix_Api_Module
{
	
	public function check($user_id)
	{
		try{
			$exists = self::db()->fetchOne(" SELECT TRUE FROM march8_logged_users WHERE user_id = ?", array($user_id));
			$was_winner = self::db()->fetchOne(" SELECT TRUE FROM march8_winners WHERE user_id = ?", array($user_id));
			
			if($exists || $was_winner){
				return 0;
			}
			
			$user_data = array(
				'user_id' => $user_id,
				'login_date' => new Zend_Db_Expr('NOW()')
			);
			self::db()->insert('march8_logged_users', $user_data);
			self::_addLoginCount();
			$is_winner = self::_checkWinner();
			
			if($is_winner){
				self::db()->insert('march8_winners', $user_data);
				return 2;
			}
			else{
				return 1;
			}
		} catch (Exception $e) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
		
	private function _addLoginCount()
	{
		self::db()->query('UPDATE march8_login_counts SET count = count + 1 WHERE id = ?', mt_rand(1,10));
	}
	
	private function _checkWinner()
	{
		$sum = self::db()->fetchOne(" SELECT SUM(count) FROM march8_login_counts ");
		$is_winner = self::db()->fetchOne(" SELECT TRUE FROM march8_win_tries WHERE date = CURDATE() AND  login_try = ?", array($sum));
		return $is_winner;
	}		
}
