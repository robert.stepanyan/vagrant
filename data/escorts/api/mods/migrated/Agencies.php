<?php

class Cubix_Api_Module_Agencies extends Cubix_Api_Module
{
    const PACKAGE_ACTIVATION_TYPE_AFTER_CURRENT_PACKAGE_EXPIRES = 6;

	const ESCORT_STATUS_NO_PROFILE 			= 1;		// 000000001
	const ESCORT_STATUS_NO_ENOUGH_PHOTOS 	= 2;		// 000000010
	const ESCORT_STATUS_NOT_APPROVED 		= 4;		// 000000100
	const ESCORT_STATUS_OWNER_DISABLED 		= 8;		// 000001000
	const ESCORT_STATUS_ADMIN_DISABLED 		= 16;		// 000010000
	const ESCORT_STATUS_ACTIVE 				= 32;		// 000100000
	const ESCORT_STATUS_IS_NEW				= 64;		// 001000000
	const ESCORT_STATUS_PROFILE_CHANGED		= 128;		// 010000000
	const ESCORT_STATUS_DELETED				= 256;		// 100000000
	const ESCORT_STATUS_PARTIALLY_COMPLETE	= 32768;	// 1000000000000000
	const ESCORT_STATUS_IP_BLOCKED          = 65536;	// 10000000000000000

    const ESCORT_STATUS_TEMPRARY_DELETED	= 1024;		// 100000000
    const ESCORT_STATUS_SUSPICIOUS_DELETED	= 2048;		// 100000000

	const PACKAGE_STATUS_PENDING  = 1;
    const PACKAGE_STATUS_ACTIVE             = 2;
	const PACKAGE_STATUS_SUSPENDED = 6;
	
	
	const PHONE_PACKAGE_1_DAY_PASS = 19;
	const PHONE_PACKAGE_3_DAY_PASS = 20;

	//Order statuses
	const ORDER_STATUS_PENDING = 1;
	// const STATUS_CANCELLED = 2;
	const ORDER_STATUS_PAID = 3;
	// const STATUS_CHARGEBACK = 4;
	const ORDER_STATUS_PAYMENT_DETAILS_RECEIVED = 5;
	const ORDER_STATUS_PAYMENT_REJECTED = 6;
	const ORDER_STATUS_CLOSED = 7;
	
	const PACKAGE_ACTIVATE_AFTER_CURRENT_PACKAGE_EXPIRES = 6;
	const PACKAGE_ACTIVATE_AT_EXACT_DATE = 7;
	const PACKAGE_ACTIVATE_ASAP = 9;
	
	/* EXTEND PACKAGE */
	const PACKAGE_DIAMOND_LIGHT = 102;
	const PACKAGE_DIAMOND_PREMIUM = 101;
	const PACKAGE_GOLD_TOUR_PREMIUM = 100;
	
	const DL_PRODUCT_PLUS_3_DAYS = 17;
	const DL_PRODUCT_PLUS_6_DAYS = 18;
	const DL_PRODUCT_PLUS_9_DAYS = 20;
	const DP_PRODUCT_PLUS_3_DAYS = 21;
	const DP_PRODUCT_PLUS_6_DAYS = 22;
	const DP_PRODUCT_PLUS_9_DAYS = 23;

	const GT_PRODUCT_PLUS_3_DAYS = 24;
	const GT_PRODUCT_PLUS_6_DAYS = 25;

	public static $EXTEND_PACKAGE_PRODUCTS = array(
		self::DL_PRODUCT_PLUS_3_DAYS,
		self::DL_PRODUCT_PLUS_6_DAYS,
		self::DL_PRODUCT_PLUS_9_DAYS,
		self::DP_PRODUCT_PLUS_3_DAYS,
		self::DP_PRODUCT_PLUS_6_DAYS,
		self::DP_PRODUCT_PLUS_9_DAYS,

		self::GT_PRODUCT_PLUS_3_DAYS,
		self::GT_PRODUCT_PLUS_6_DAYS,
	);
	/* EXTEND PACKAGE */
	
	public function updateHitsCount($agency_id) 
	{		
        try {
			self::db()->query('UPDATE agencies SET hit_count = hit_count + 1 WHERE id = ?', $agency_id);				
		} catch (Exception $e) {
			return false;
		}
      
        return true;
    }
	
	public function save($data)
	{
		try {
			self::db()->insert('agencies', $data);
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}	
		
		return self::db()->lastInsertId();
	}    
	
	public function getAllEscorts($agency_id)
	{
		return self::db()->query('SELECT id FROM escorts WHERE agency_id = ?', $agency_id)->fetchAll();
	}
	
	public function getNotActiveEscorts($agency_id, $page, $dash_per_page)
	{
		if ( $page < 1 ) $page = 1;
		if ( $dash_per_page < 1 ) $dash_per_page = 1;

		$page--;

		$offset = $page * $dash_per_page;
		$limit = $dash_per_page;

		$sql = '
			SELECT
				e.id,
				e.showname,
				e.status,
				e.gender
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			WHERE u.application_id = ? AND e.agency_id = ?/* AND NOT e.status & 32*/ AND NOT e.status & ? AND NOT e.status & ?
			LIMIT ?, ?
		';

		$countSql = '
			SELECT
				COUNT(e.id) as count
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			WHERE u.application_id = ? AND e.agency_id = ?/* AND NOT e.status & 32*/ AND NOT e.status & ? AND NOT e.status & ?
		';

		$escorts = self::db()->query($sql, array(self::app()->id, $agency_id, self::ESCORT_STATUS_DELETED, self::ESCORT_STATUS_ACTIVE, $offset, $limit))->fetchAll();
		$count = self::db()->fetchOne($countSql, array(self::app()->id, $agency_id, self::ESCORT_STATUS_DELETED, self::ESCORT_STATUS_ACTIVE));

		$data = array();

		$data['result'] = $escorts;
		$data['count'] = $count;

		return $data;
	}

	public function getActiveEscorts($agency_id)
	{
		ini_set('memory_limit', '4048M');

		$sql = '
			SELECT
				e.id,
				e.showname,
				e.status,
				e.gender,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.application_id,
				e.verified_status,
				(select 0 AS free_bubble_left from escort_bubbletexts eb where eb.escort_id = e.id and DATE(eb.date) = DATE(NOW()) GROUP BY eb.escort_id ) AS free_bubble_left
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id
			/*LEFT JOIN escort_bubbletexts eb ON eb.escort_id = e.id and eb.`status` = 1*/
			/*LEFT JOIN bubble_phone_transfers bpt ON bpt.id = e.bubble_transfer_id*/
			WHERE u.application_id = ? AND e.agency_id = ? AND NOT e.status & ? AND NOT e.status & ?
 			GROUP BY e.id
		';

		try {
			$result = self::db()->query($sql, array(self::app()->id, $agency_id, self::ESCORT_STATUS_DELETED, self::ESCORT_STATUS_TEMPRARY_DELETED))->fetchAll();
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $result;
	}
	public function getHaveMessageEscorts($agency_id)
	{
		ini_set('memory_limit', '4048M');

		$sql = '
			SELECT
				e.id,
				e.showname,
				e.status,
				e.gender,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.application_id,
				e.verified_status,
				(select 0 AS free_bubble_left from escort_bubbletexts eb where eb.escort_id = e.id and DATE(eb.date) = DATE(NOW()) GROUP BY eb.escort_id ) AS free_bubble_left
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN threads_participants tp ON tp.escort_id = e.id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id
			/*LEFT JOIN escort_bubbletexts eb ON eb.escort_id = e.id and eb.`status` = 1*/
			/*LEFT JOIN bubble_phone_transfers bpt ON bpt.id = e.bubble_transfer_id*/
			WHERE u.application_id = ? AND e.agency_id = ? AND NOT e.status & ? AND NOT e.status & ?
 			GROUP BY e.id
		';

		try {
			$result = self::db()->query($sql, array(self::app()->id, $agency_id, self::ESCORT_STATUS_DELETED, self::ESCORT_STATUS_TEMPRARY_DELETED))->fetchAll();
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $result;
	}

	public function getActiveEscortsWithOrdering($agency_id)
	{
		ini_set('memory_limit', '4048M');

		$sql = '
			SELECT
				e.id,
				e.showname,
				e.status,
				e.gender,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.application_id,
				e.verified_status,
				(select 0 AS free_bubble_left from escort_bubbletexts eb where eb.escort_id = e.id and DATE(eb.date) = DATE(NOW()) GROUP BY eb.escort_id ) AS free_bubble_left
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id
			LEFT JOIN order_packages AS op ON op.escort_id = e.id
			/*LEFT JOIN escort_bubbletexts eb ON eb.escort_id = e.id and eb.`status` = 1*/
			/*LEFT JOIN bubble_phone_transfers bpt ON bpt.id = e.bubble_transfer_id*/
			WHERE u.application_id = ? AND e.agency_id = ? AND NOT e.status & ? AND NOT e.status & ?
 			GROUP BY e.id
 			ORDER BY e.is_suspicious ASC, op.status DESC
		';

		try {
			$result = self::db()->query($sql, array(self::app()->id, $agency_id, self::ESCORT_STATUS_DELETED, self::ESCORT_STATUS_TEMPRARY_DELETED))->fetchAll();
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $result;
	}
	
	public function getActiveEscortsWithoutPackages($agency_id)
	{
		ini_set('memory_limit', '2048M');
		

		$sql = '
			SELECT
				e.id,
				e.showname,
				e.gender,
				u.application_id,
				e.verified_status,
				op.order_id,
				op.status,
				op.status AS package_status,
				op.package_id,
				op.activation_type
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN order_packages op ON op.escort_id = e.id
			WHERE 
				u.application_id = ? AND e.agency_id = ? AND e.status & ?
 			ORDER BY e.id
		';

		try {
			$escorts = array();
			$rows = self::db()->query($sql, array(self::app()->id, $agency_id, self::ESCORT_STATUS_ACTIVE))->fetchAll();
			
			//return $rows;
			
			$escort_id = null;
			$row_id = null;
			$has_package = false;
			$rows_count = count($rows);
			
			foreach($rows as $k => $row) {
				if ( is_null($escort_id) ) {
					$escort_id = $row->id;
					$row_id = $k;
				}
				
				if ( $escort_id != $row->id ) {
					if ( ! $has_package ) {
						$escorts[] = $rows[$row_id];
					}
					
					$escort_id = $row->id;
					$row_id = $k;
					$has_package = false;
				}
				
				
				if ( $has_package ) {
					continue;
				}
				
				if ( ! is_null($row->order_id) && in_array($row->package_status, array(self::PACKAGE_STATUS_ACTIVE, self::PACKAGE_STATUS_PENDING, self::PACKAGE_STATUS_SUSPENDED ))  ) {
					$has_package = true;
				}
				
				if ( $k == $rows_count - 1 ) {
					if ( ! $has_package ) {
						$escorts[] = $rows[$row_id];
					}
				}
			}
			
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $escorts;
	}
	
	public function getActiveEscortsWithPackages($agency_id)
	{
		ini_set('memory_limit', '2048M');

		$sql = '
			SELECT
				e.id,
				e.showname,
				e.gender,
				u.application_id,
				e.verified_status
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN order_packages op ON op.escort_id = e.id
			WHERE 
				u.application_id = ? AND e.agency_id = ? AND e.status & ? AND op.status = ? AND op.order_id IS NOT NULL
 			ORDER BY e.id
		';

		try {
			$escorts = array();
			$rows = self::db()->query($sql, array(self::app()->id, $agency_id, self::ESCORT_STATUS_ACTIVE, self::PACKAGE_STATUS_ACTIVE))->fetchAll();
			
			return $rows;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $escorts;
	}

	public static function getAgencyProfileOverview($user_id) {
		
        $user_id = intval($user_id);
        
        $data = array();
        if (!$user_id)
            return false;

        $escortsIds =  self::db()->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetchAll();
        $agency_id =  self::db()->fetchRow('SELECT id as agency_id FROM agencies WHERE user_id = ?', $user_id)->agency_id;
		$in = '';
		if($escortsIds){
			$in = 'WHERE escort_id IN (';
			foreach ($escortsIds as $eId) {
				$in .= $eId->id.",";
			}
			$in = substr($in, 0, -1).")";
			$data['revision'] = self::db()->fetchRow('SELECT date_updated, status FROM profile_updates_v2 '.$in.' ORDER BY REVISION DESC');
		}else{
			$data['revision'] = false;	
		}
        $data['contact_info'] = self::db()->fetchRow('SELECT phone, phone_country_id, phone_1, phone_country_id_1, phone_2, phone_country_id_2, web FROM agencies WHERE user_id = ?', $user_id);
        $data['escortCountWithPackage'] = count(self::getActiveEscortsWithPackages($agency_id));

        return $data;
    }

	public function getActiveEscortsForGotd($agency_id)
	{
		ini_set('memory_limit', '2048M');

		$sql = '
			SELECT
				e.id,
				e.showname,
				e.gender,
				u.application_id,
				e.verified_status,
				op.order_id,
				op.package_id,
				op.status,
				op.status AS package_status,
				op.package_id,
				op.activation_type,
				UNIX_TIMESTAMP(op.date_activated) AS date_activated,
				UNIX_TIMESTAMP(op.expiration_date) AS expiration_date,
				UNIX_TIMESTAMP(op.activation_date) AS activation_date,
				op.period
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN order_packages op ON op.escort_id = e.id
			INNER JOIN orders o ON o.id = op.order_id
			WHERE 
				u.application_id = ? AND e.agency_id = ? AND e.status = ? AND 
				(op.status = ? OR (op.package_id IN (' . implode(',', array(self::PHONE_PACKAGE_1_DAY_PASS, self::PHONE_PACKAGE_3_DAY_PASS)) . ') AND op.status = ? )) AND
				o.status IN (' . implode(',', array(self::ORDER_STATUS_PAID, self::ORDER_STATUS_PAYMENT_DETAILS_RECEIVED)) . ')
 			ORDER BY e.id
		';

		try {
			$rows = self::db()->query($sql, array(
				self::app()->id, 
				$agency_id, 
				self::ESCORT_STATUS_ACTIVE, 
				self::PACKAGE_STATUS_ACTIVE,
				self::PACKAGE_STATUS_PENDING
			))->fetchAll();
			
			foreach($rows as $i => $row) {
				if ( $row->activation_type == self::PACKAGE_ACTIVATE_AT_EXACT_DATE && ! $row->date_activated ) {//1/3 day pass case
					$rows[$i]->date_activated = $row->activation_date;
					$rows[$i]->expiration_date = $row->date_activated + 60 * 60 * 24 * $row->period;
				} else {
					$rows[$i]->expiration_date -= 60 * 60 * 24;//Fixing expiration date
				}
				unset($rows[$i]->activation_date);
				unset($rows[$i]->period);
			}
			
			return $rows;
			
			
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $escorts;
	}
	
	
	public function getActiveEscortsForProfileBoost($agency_id, $sort)
	{
		ini_set('memory_limit', '2048M');
		
		if ( ! $sort ) $sort = 'e.showname ASC';
		
		$sql = '
			SELECT
				e.id,
				e.showname,
				e.gender,
				u.application_id,
				e.verified_status,
				op.order_id,
				op.package_id,
				op.status,
				op.status AS package_status,
				op.package_id,
				op.activation_type,
				UNIX_TIMESTAMP(op.date_activated) AS date_activated,
				UNIX_TIMESTAMP(op.expiration_date) AS expiration_date,
				UNIX_TIMESTAMP(op.activation_date) AS activation_date,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				op.period
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN order_packages op ON op.escort_id = e.id
			INNER JOIN orders o ON o.id = op.order_id
			INNER JOIN order_package_products opp ON opp.order_package_id = op.id /*AND opp.product_id = ?*/
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			WHERE
				u.application_id = ? AND e.agency_id = ? AND e.status = ? AND 
				op.status = ? AND
				o.status IN (' . implode(',', array(self::ORDER_STATUS_PAID, self::ORDER_STATUS_PAYMENT_DETAILS_RECEIVED)) . ')
			GROUP BY e.id
 			ORDER BY ' . $sort ;

		try {
			$rows = self::db()->query($sql, array(
				/*PRODUCT_BOOST_PROFILE,*/
				self::app()->id, 
				$agency_id, 
				self::ESCORT_STATUS_ACTIVE, 
				self::PACKAGE_STATUS_ACTIVE,
			))->fetchAll();
			
			foreach($rows as $i => $row) {
				$rows[$i]->expiration_date -= 60 * 60 * 24;//Fixing expiration date
				unset($rows[$i]->activation_date);
				unset($rows[$i]->period);
			}
			
			return $rows;
			
			
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $escorts;
	}

/* Update Grigor */
    public function getEscortsByStatus($agency_id, $page = 1, $per_page = 9,$status = null, $is_susp = false, $showname = null, $order = null ,$showName = null)
    {
        $sql = '
			SELECT id, name, slug FROM agencies WHERE id = ?
		';
        $agency = self::db()->fetchRow($sql, $agency_id);

        if (!$agency) {
            return null;
        }

        if ($status == null) {
            /* Inactive */
            if (Cubix_Application::getId() == APP_ED) {
                $status = " AND NOT e.status & " . self::ESCORT_STATUS_ACTIVE ." AND NOT e.status & " . self::ESCORT_STATUS_NO_ENOUGH_PHOTOS." AND NOT e.status & " . self::ESCORT_STATUS_TEMPRARY_DELETED. " AND ( " .
                    "e.status & " . self::ESCORT_STATUS_OWNER_DISABLED .
                    " OR e.status & " . self::ESCORT_STATUS_ADMIN_DISABLED .
                    " )";
            } else {
                $status = " AND ( 
                NOT e.status & " . self::ESCORT_STATUS_ACTIVE . " OR
                    (e.status & " . self::ESCORT_STATUS_OWNER_DISABLED . ")
                       )  AND
                      NOT e.status & " . self::ESCORT_STATUS_TEMPRARY_DELETED . "";//reset($status);
            }

        } elseif ($status == -2) {
            /* Not completed */
            if (Cubix_Application::getId() == APP_ED) {
                $status = " AND NOT e.status & " . self::ESCORT_STATUS_ACTIVE . " AND (" .
                    "    e.status & " . Cubix_EscortStatus::ESCORT_STATUS_NO_PROFILE .
                    " OR e.status & " . Cubix_EscortStatus::ESCORT_STATUS_NO_ENOUGH_PHOTOS .
                    " OR e.status & " . Cubix_EscortStatus::ESCORT_STATUS_PARTIALLY_COMPLETE .
                    " OR e.status & " . Cubix_EscortStatus::ESCORT_STATUS_NOT_APPROVED .
                    " ) ".
                      " AND NOT e.status & " . Cubix_EscortStatus::ESCORT_STATUS_TEMPRARY_DELETED.
                    " AND NOT e.status & " . Cubix_EscortStatus::ESCORT_STATUS_ADMIN_DISABLED .
                    " AND NOT e.status & " . Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED;
            }else{
                $status = " AND NOT e.status & " . self::ESCORT_STATUS_ACTIVE . " AND (" .
                    "    e.status & " . Cubix_EscortStatus::ESCORT_STATUS_NO_PROFILE .
                    " OR e.status & " . Cubix_EscortStatus::ESCORT_STATUS_NO_ENOUGH_PHOTOS .
                    " OR e.status & " . Cubix_EscortStatus::ESCORT_STATUS_NOT_APPROVED .
                    " ) " .
                    " AND NOT e.status & " . Cubix_EscortStatus::ESCORT_STATUS_ADMIN_DISABLED .
                    " AND NOT e.status & " . Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED;
            }

        } elseif ($status == -1) {
            $status = " AND (e.status & " . self::ESCORT_STATUS_TEMPRARY_DELETED ." OR e.status & " . self::ESCORT_STATUS_DELETED. " OR e.status & " . self::ESCORT_STATUS_SUSPICIOUS_DELETED.")";
        } elseif ($status == 2) {
            /* agency escorts for packages */
            $status = " AND ( e.status & " . self::ESCORT_STATUS_ACTIVE . " OR ( e.status & " . self::ESCORT_STATUS_OWNER_DISABLED .
                " AND NOT e.status & " . self::ESCORT_STATUS_NO_PROFILE .
                " AND NOT e.status & " . self::ESCORT_STATUS_NO_ENOUGH_PHOTOS .
                " AND NOT e.status & " . self::ESCORT_STATUS_PROFILE_CHANGED .
                ") ) /*AND  ( e.need_age_verification = 1 OR e.need_age_verification = 4 )*/";
        } elseif ($status == 3) {
            /*Offline escorts*/

            $status = " AND e.status & " . Cubix_EscortStatus::ESCORT_STATUS_OFFLINE .
                " AND NOT e.status & " . self::ESCORT_STATUS_ADMIN_DISABLED;
        } elseif ($status == 5){
            /*ALL escorts*/
            $status = " AND e.status <> 0";
        }else {
            /* Active */
            if(Cubix_Application::getId() == APP_ED) {
                $status = " AND (e.status & " . self::ESCORT_STATUS_ACTIVE . " OR e.status & " . self::ESCORT_STATUS_IS_NEW . " OR e.status & ".self::ESCORT_STATUS_PROFILE_CHANGED." ) " .
                " AND NOT e.status & " . self::ESCORT_STATUS_OWNER_DISABLED .
                " AND NOT e.status & " . self::ESCORT_STATUS_PARTIALLY_COMPLETE .
                " AND NOT e.status & " . self::ESCORT_STATUS_NO_ENOUGH_PHOTOS .
                " AND NOT e.status & " . self::ESCORT_STATUS_TEMPRARY_DELETED .
                " AND NOT e.status & " . self::ESCORT_STATUS_ADMIN_DISABLED .
//                " AND NOT e.status & " . Cubix_EscortStatus::ESCORT_STATUS_NO_ENOUGH_PHOTOS .    //EDIR-305
                " AND NOT e.status & " . Cubix_EscortStatus::ESCORT_STATUS_IP_BLOCKED;
            }else{
                $status = " AND e.status & ".self::ESCORT_STATUS_ACTIVE." AND NOT e.status & ".self::ESCORT_STATUS_OWNER_DISABLED;
            }
        }

        $page = intval($page);
		$per_page = intval($per_page);

		if ( $page < 1 ) $page = 1;
		if ( $per_page < 1 ) $per_page = 1;

		$page--;
		$offset = $page * $per_page;
		$limit = $per_page;
		// <--

		$wh = "";
		if ( $is_susp ) {
			$wh = " AND e.is_suspicious <> 0 ";
		}
		
		if(is_numeric($showname)){
			$wh .= " AND e.id LIKE '%" . $showname . "%' "; 
		}
		elseif ( strlen( $showname ) ) {
			$wh .= " AND e.showname LIKE '%" . $showname . "%' "; 
		}
		
		$order = ( $order )   ?  $order : 'p.name DESC, e.showname ASC' ;
		$sel = "  ";
		if ( self::app()->id == APP_A6 || self::app()->id == APP_A6_AT ) {
			$sel = " , op.days_left ";
		}
		
		if(self::app()->id == APP_EF){
			$sel = " , e.need_age_verification ";
		}
            $sql = '
			SELECT
			e.id,
			e.showname,
			e.status,
			e.gender,
			e.deleted_date,
			e.is_suspicious,
			e.city_id,
            ct.title_en,
			eph.hash AS photo_hash,
			eph.ext AS photo_ext,
			eph.status AS photo_status,
			u.application_id,
			p.name AS package,
			GROUP_CONCAT(pe.city_id,"") as premium_cities,
			op.date_activated,
			op.expiration_date,
			DATEDIFF(op.expiration_date, NOW()) AS expires,
			op.id AS opid,
			op.status as op_status,
			op.package_id,

			e.has_hh, e.hh_status
			' . $sel . ',
			pck.name package_name
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			LEFT JOIN order_packages op ON op.escort_id = e.id AND (op.status = ? OR op.status = 6) AND op.order_id IS NOT NULL
			LEFT JOIN packages p ON p.id = op.package_id 
			LEFT JOIN premium_escorts pe ON pe.order_package_id = op.id 
			LEFT JOIN cities ct ON ct.id = e.city_id
			LEFT JOIN packages pck ON pck.id = op.package_id
			WHERE u.application_id = ? AND e.agency_id = ? ';

                if( $showName == null )
                {
                    $sql .= $status . ' AND NOT e.status & ? ' . $wh . ' GROUP BY e.id	ORDER BY ' . $order . '	LIMIT ?, ? ';
                }else{
                    $sql .= $status . ' AND e.showname LIKE "%'.$showName.'%" GROUP BY e.id';
                }
    	# AND NOT e.status & ?            AND op.status = ?		
        $countSql = '
			SELECT 
				COUNT(DISTINCT e.id)
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id
			WHERE u.application_id = ? AND e.agency_id = ? '.$status.' AND NOT e.status & ? ' . $wh . '
		';
        # AND NOT e.status & ?

		try {
            //self::PACKAGE_STATUS_ACTIVE,
            if($showName == null)
            {
                $escorts = self::db()->query($sql, array(self::PACKAGE_STATUS_ACTIVE,self::app()->id, $agency->id,self::ESCORT_STATUS_DELETED,$offset,$limit))->fetchAll();//,$status1
            }else{
                $escorts = self::db()->query($sql, array(self::PACKAGE_STATUS_ACTIVE,self::app()->id, $agency->id))->fetchAll();//,$status1
            }

			if(!$escorts && $offset){
                $offset = $offset-$per_page;
                $escorts = self::db()->query($sql, array(self::PACKAGE_STATUS_ACTIVE,self::app()->id, $agency->id,self::ESCORT_STATUS_DELETED,$oeder,$offset,$limit))->fetchAll();//,$status1
            }
            
			if ( self::app()->id == 16 ) {
				if ( count($escorts) ) {
					foreach ( $escorts as $k => $esc ) {
						$products = self::db()->fetchAll('SELECT product_id FROM order_package_products WHERE order_package_id = ?', array($esc->opid));
						$prod_data = array();
						if ( count($products) ) {
							foreach ( $products as $prod ) {
								$prod_data[] = $prod->product_id;
							}
						}
						$escorts[$k]->products = $prod_data;
						
						if ( in_array($esc->package_id, array(self::PHONE_PACKAGE_1_DAY_PASS, self::PHONE_PACKAGE_3_DAY_PASS)) ) {
							$phone_package_exp = self::db()->fetchRow('SELECT expiration_date AS phone_exp_date FROM phone_packages WHERE order_package_id = ?', array($esc->opid));
							$escorts[$k]->phone_exp_date = $phone_package_exp->phone_exp_date;
						}
					}
				}
			}
			
			if ( self::app()->id == APP_6A ) {
				if ( count($escorts) ) {
					foreach ( $escorts as $k => $esc ) {
						$show_extend_package = false;
		
						if ( in_array($esc->package_id, array(self::PACKAGE_DIAMOND_LIGHT, self::PACKAGE_DIAMOND_PREMIUM, self::PACKAGE_GOLD_TOUR_PREMIUM)) ) {
							$sql = "
								SELECT
									product_id
								FROM order_package_products
								WHERE order_package_id = ?
							";

							$products = self::db()->fetchAll($sql, array($esc->opid));

							$show_extend_package = true;

							foreach ( $products as $product ) {

								if ( in_array($product->product_id, self::$EXTEND_PACKAGE_PRODUCTS)) {
									$show_extend_package = false;
									break;
								}
							}
						}
						//$show_extend_package = false;
						$escorts[$k]->show_extend_button = $show_extend_package;
					}
				}
			}
			
            $agency->escorts = $escorts;
			
            $count = self::db()->fetchOne($countSql, array(self::app()->id, $agency->id,self::ESCORT_STATUS_DELETED));//,$status1
            $agency->escorts_count = $count;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $agency;

        
    }

    public function getEscortsByStatusOtherCase($agency_id, $page = 1, $per_page = 9,$status = null, $is_susp = false, $showname = null, $order = null )
    {
        $sql = '
			SELECT id, name, slug FROM agencies WHERE id = ?
		';

		$agency = self::db()->fetchRow($sql, $agency_id);

		if ( ! $agency ) {
			return null;
		}
        
        if($status == null){
            /* Inactive */ 
            $status = " AND ( 
                NOT e.status & ".self::ESCORT_STATUS_ACTIVE." OR
                    (e.status & ".self::ESCORT_STATUS_OWNER_DISABLED.")
                       )  AND
                      NOT e.status & ".self::ESCORT_STATUS_TEMPRARY_DELETED."";//reset($status);
        }elseif($status == -1){
            $status = " AND (e.status & ".self::ESCORT_STATUS_TEMPRARY_DELETED.")";
        }elseif($status == 2){
			/* agency escorts for packages */
			$status = " AND ( e.status & " . self::ESCORT_STATUS_ACTIVE . " OR ( e.status & ".self::ESCORT_STATUS_OWNER_DISABLED .
					" AND NOT e.status & " . self::ESCORT_STATUS_NO_PROFILE .
					" AND NOT e.status & " . self::ESCORT_STATUS_NO_ENOUGH_PHOTOS .
					" AND NOT e.status & " . self::ESCORT_STATUS_PROFILE_CHANGED .
					") ) /*AND  ( e.need_age_verification = 1 OR e.need_age_verification = 4 )*/";
		} else {
            /* Active */
            $status = " AND e.status & ".self::ESCORT_STATUS_ACTIVE." AND NOT e.status & ".self::ESCORT_STATUS_OWNER_DISABLED;
        }
        
		$page = intval($page);
		$per_page = intval($per_page);

		if ( $page < 1 ) $page = 1;
		if ( $per_page < 1 ) $per_page = 1;

		$page--;
		$offset = $page * $per_page;
		$limit = $per_page;
		// <--

		$wh = "";
		if ( $is_susp ) {
			$wh = " AND e.is_suspicious <> 0 ";
		}
		
		if(is_numeric($showname)){
			$wh .= " AND e.id LIKE '%" . $showname . "%' "; 
		}
		elseif ( strlen( $showname ) ) {
			$wh .= " AND e.showname LIKE '%" . $showname . "%' "; 
		}
		
		$order = ( $order )   ?  $order : 'p.name DESC, e.showname ASC' ;
		$sel = "  ";
		if ( self::app()->id == APP_A6 || self::app()->id == APP_A6_AT ) {
			$sel = " , op.days_left ";
		}
		
		if(self::app()->id == APP_EF){
			$sel = " , e.need_age_verification ";
		}

		$sql = '
			SELECT
				e.id,
				e.showname,
				e.status,
				e.gender,
                e.deleted_date,
                e.is_suspicious,
                e.city_id,
                ct.title_en,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.application_id,
                p.name AS package,
                GROUP_CONCAT(pe.city_id,"") as premium_cities,
                op.date_activated,
                op.expiration_date,
                DATEDIFF(op.expiration_date, NOW()) AS expires,
                op.id AS opid,
                op.status as op_status,
				op.package_id,
				op2.status as has_pending,

				e.has_hh, e.hh_status
				' . $sel . '
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
            LEFT JOIN order_packages op ON op.escort_id = e.id  AND (((op.status = ? OR op.status = 6) AND op.order_id IS NOT NULL) OR (op.order_id IS NULL AND op.package_id = 106 AND (op.status = 2 OR op.status = 6)) )
            LEFT JOIN order_packages op2 ON op2.escort_id = e.id  AND op2.status = 1
            LEFT JOIN packages p ON p.id = op.package_id 
            LEFT JOIN premium_escorts pe ON pe.order_package_id = op.id 
            LEFT JOIN cities ct ON ct.id = e.city_id
			WHERE u.application_id = ? AND e.agency_id = ? '.$status.' AND NOT e.status & ? ' . $wh . '
 			GROUP BY e.id
			ORDER BY '.$order.'
            LIMIT ?, ?
		';
    	# AND NOT e.status & ?            AND op.status = ?

        $countSql = '
			SELECT 
				COUNT(DISTINCT e.id)
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id
			WHERE u.application_id = ? AND e.agency_id = ? '.$status.' AND NOT e.status & ? ' . $wh . '
		';
        # AND NOT e.status & ?

        try {
            //self::PACKAGE_STATUS_ACTIVE,

            $escorts = self::db()->query($sql, array(self::PACKAGE_STATUS_ACTIVE,self::app()->id, $agency->id,self::ESCORT_STATUS_DELETED,$offset,$limit))->fetchAll();//,$status1
//            return [
//                $sql,
//                self::PACKAGE_STATUS_ACTIVE,
//                self::app()->id,
//                $agency->id,
//                self::ESCORT_STATUS_DELETED,
//                $offset,
//                $limit
//            ];

			if(!$escorts && $offset){
                $offset = $offset-$per_page;
                $escorts = self::db()->query($sql, array(self::PACKAGE_STATUS_ACTIVE,self::app()->id, $agency->id,self::ESCORT_STATUS_DELETED,$oeder,$offset,$limit))->fetchAll();//,$status1
            }
            
			if ( self::app()->id == 16 ) {
				if ( count($escorts) ) {
					foreach ( $escorts as $k => $esc ) {
						$products = self::db()->fetchAll('SELECT product_id FROM order_package_products WHERE order_package_id = ?', array($esc->opid));
						$prod_data = array();
						if ( count($products) ) {
							foreach ( $products as $prod ) {
								$prod_data[] = $prod->product_id;
							}
						}
						$escorts[$k]->products = $prod_data;
						
						if ( in_array($esc->package_id, array(self::PHONE_PACKAGE_1_DAY_PASS, self::PHONE_PACKAGE_3_DAY_PASS)) ) {
							$phone_package_exp = self::db()->fetchRow('SELECT expiration_date AS phone_exp_date FROM phone_packages WHERE order_package_id = ?', array($esc->opid));
							$escorts[$k]->phone_exp_date = $phone_package_exp->phone_exp_date;
						}
					}
				}
			}
			
			if ( self::app()->id == APP_6A ) {
				if ( count($escorts) ) {
					foreach ( $escorts as $k => $esc ) {
						$show_extend_package = false;
		
						if ( in_array($esc->package_id, array(self::PACKAGE_DIAMOND_LIGHT, self::PACKAGE_DIAMOND_PREMIUM, self::PACKAGE_GOLD_TOUR_PREMIUM)) ) {
							$sql = "
								SELECT
									product_id
								FROM order_package_products
								WHERE order_package_id = ?
							";

							$products = self::db()->fetchAll($sql, array($esc->opid));

							$show_extend_package = true;

							foreach ( $products as $product ) {

								if ( in_array($product->product_id, self::$EXTEND_PACKAGE_PRODUCTS)) {
									$show_extend_package = false;
									break;
								}
							}
						}
						//$show_extend_package = false;
						$escorts[$k]->show_extend_button = $show_extend_package;
					}
				}
			}
			
            $agency->escorts = $escorts;
			
            $count = self::db()->fetchOne($countSql, array(self::app()->id, $agency->id,self::ESCORT_STATUS_DELETED));//,$status1
            $agency->escorts_count = $count;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $agency;

        
    }

/* Update Grigor */

	public function getEscorts($agency_id, $page, $per_page, $excl_escort_id = null)
	{
		$sql = '
			SELECT id, name, slug FROM agencies WHERE id = ?
		';
		
		$agency = self::db()->fetchRow($sql, $agency_id);
		
		if ( ! $agency ) {
			return null;
		}
		
		// --> Calculate offset and limit
		$page = intval($page);
		$per_page = intval($per_page);
		
		if ( $page < 1 ) $page = 1;
		if ( $per_page < 1 ) $per_page = 1;
		
		$page--;
		
		$offset = $page * $per_page;
		$limit = $per_page;
		// <--
		
		// --> If an escort id is passed that needs to be excluded, construct a simple where clause
		
		
		$excl_clause = '';
		if ( $excl_escort_id ) {
            $excl_escort_id = intval($excl_escort_id);
			$excl_clause = ' AND e.id <> ' . $excl_escort_id;
		}
		// <--
		
		$sql = '
			SELECT
				e.id,
				e.showname,
				u.application_id,
				
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status
			FROM escort_profiles ep
			
			INNER JOIN escorts e ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN applications a ON a.id = u.application_id
			
			INNER JOIN escort_photos AS eph ON eph.escort_id = e.id
			
			WHERE u.application_id = ? AND u.status = ? AND e.status & 32 AND e.agency_id = ? AND eph.is_main = 1' . $excl_clause . '
			LIMIT ?, ?
		';
		
		$escorts = self::db()->query($sql, array(self::app()->id, STATUS_ACTIVE, $agency->id, $offset, $limit))->fetchAll();
		
		$agency->escorts = $escorts;
		
		$countSql = '
			SELECT
				COUNT(e.id)
			FROM escort_profiles ep
			
			INNER JOIN escorts e ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN applications a ON a.id = u.application_id
			
			INNER JOIN escort_photos AS eph ON eph.escort_id = e.id
			
			WHERE u.application_id = ? AND u.status = ? AND e.status & 32 AND e.agency_id = ? AND eph.is_main = 1' . $excl_clause . '
		';
		
		$count = self::db()->fetchOne($countSql, array(self::app()->id, STATUS_ACTIVE, $agency->id));
		
		$agency->escorts_count = $count;
		
		return $agency;
	}
	
	public function getByName($name)
	{
		try {
			
			$sql = 'SELECT * FROM agencies WHERE name = ?';
			
			$agency = self::db()->fetchRow($sql, $name);
			
			if ( ! $agency ) {
				return null;
			}
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return $agency;
	}
	
	public function getInfo($agency_slug, $agency_id)
	{
		try {
			
			$fields = '';
			$join = '';
			
			if (self::app()->id == APP_A6 || self::app()->id == APP_A6_AT)
			{
				$fields = ',
					a.entrance,
					a.wellness,
					a.food,
					a.outdoor,
					a.address,
					a.zip,
					'. Cubix_I18n::getTblFields('c.title').' AS city ';
				
				$join = ' LEFT JOIN f_cities c ON c.id = a.fake_city_id ';
			}
			
			$sql = '
				SELECT
					a.id,
					a.name,
					a.phone,
					a.phone_country_id,
					a.phone_instructions,
					a.email,
					a.web,
					a.block_website,
					a.last_modified,
					a.logo_hash,
					a.logo_ext,
					u.application_id,
					u.date_registered AS creation_date,
					a.hit_count,
					a.available_24_7,
					'. Cubix_I18n::getTblFields('a.about') . $fields . '
				FROM agencies a
				LEFT JOIN users u ON u.id = a.user_id' . $join . '
				WHERE a.slug = ? AND a.id = ? AND u.status = ?
			';
			
			$agency = self::db()->fetchRow($sql, array($agency_slug, $agency_id, USER_STATUS_ACTIVE));
			
			if ( ! $agency ) {
				return null;
			}
			
			$hour = date('G');
			$day = date('N');
			
			$agency->is_open = self::db()->fetchOne('
				SELECT
					IF (
					(
						awt.day_index = ' . $day . '
						AND awt.time_from = 0
						AND awt.time_to = 0
					) 
					OR
					(
						awt.day_index = ' . $day . '
						AND awt.time_from <= ' . $hour . '
						AND awt.time_to >= ' . $hour . '
					) 
					OR 
					(
						awt.day_index = ' . $day . '
						AND awt.time_from <= ' . $hour . '
						AND awt.time_from > awt.time_to
					) 
					OR 
					(
						awt.day_index = ' . ( 1 == $day ? 7 : $day - 1 ) . '
						AND awt.time_to >= ' . $hour . '
						AND awt.time_from  > awt.time_to 
					), 1, 0)
				FROM agency_working_times awt
				WHERE awt.agency_id = ? AND awt.day_index = ?
			', array($agency->id, date('N')));
			
			
			$data = self::db()->query('
				SELECT awt.day_index, awt.time_from, awt.time_to
				FROM agency_working_times awt
				WHERE awt.agency_id = ?
			', $agency->id)->fetchAll();
			
			$result = array();
			foreach ( $data as $row ) {
				$result[$row->day_index] = array(
					'from' => $row->time_from,
					'to' => $row->time_to
				);
			}
			
			$agency->working_times = $result;
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		return $agency;
	}

	public function getByUserId($user_id)
	{
		$user_id = intval($user_id);
		
		$f = '';
		if ( self::app()->id == APP_A6 ) {
			$f .= ' , a.fake_city_id, a.zip, a.address, a.display_name, a.is_club, a.status ';
		}

		if ( self::app()->id == APP_ED ) {
			$f .= ' , a.args, a.viber, a.whatsapp, a.wechat, a.status, a.telegram, a.ssignal, 
				ct.' . Cubix_I18n::getTblField('title') . ' AS base_city, 
				c.' . Cubix_I18n::getTblField('title') . ' AS country';
		}
        if ( self::app()->id == APP_EF ) {
            $f .= ' , a.contact_phone_parsed ';
        }

		$sql = "
			SELECT 
				a.id, a.name, a.phone_country_id, a.phone, a.phone_instructions, a.email, a.web, a.slug,
				a.last_modified, a.logo_hash, a.logo_ext, u.application_id, 
				u.date_registered AS creation_date, a.hit_count, a.country_id, a.city_id,
				u.username, a.disabled_reviews, a.disabled_comments, a.is_anonymous " . $f . "
			FROM agencies a
			LEFT JOIN users u ON u.id = a.user_id";

		if (self::app()->id == APP_ED) {
			$sql .= '
				LEFT JOIN countries c ON c.id = a.country_id
				LEFT JOIN cities ct ON ct.id = a.city_id
			';
		}

		$sql .= ' WHERE u.id = ?';
		return self::db()->fetchRow($sql, $user_id);
	}

	public function getByUserIdED($user_id)
	{
		$user_id = intval($user_id);
		
		$sql_agency = "
			SELECT 
				a.id, a.name, a.phone_country_id, a.phone, a.phone_instructions, a.email,
				a.web, a.slug, a.last_modified, a.logo_hash, a.logo_ext, u.application_id, 
				u.date_registered AS creation_date, a.hit_count, a.country_id, a.city_id,
				u.username, a.disabled_reviews, a.disabled_comments, a.is_anonymous, a.args, 
				a.viber, a.whatsapp, a.wechat, a.telegram, a.ssignal, a.about_en, a.about_it, a.about_pt, a.about_fr, a.about_de,
				a.about_es, a.about_ro, a.about_ru, a.about_services, a.about_areas_covered
			FROM agencies a
			LEFT JOIN users u ON u.id = a.user_id
			WHERE u.id = ?
		";

		$agency_data = self::db()->fetchRow($sql_agency, $user_id);

		$sql_working_times = "
			SELECT awt.day_index, awt.time_from, awt.time_to, awt.time_from_m, awt.time_to_m
			FROM agency_working_times awt
			WHERE awt.agency_id = ?
		";

		$sql_languages = "
			SELECT al.agency_id, al.level, al.lang_id
			FROM agency_langs al
			WHERE al.agency_id = ?
		";
		
		return array(
			'agency_data' => $agency_data,
			'working_times' => self::db()->fetchAll($sql_working_times, $agency_data->id),
			'languages' => self::db()->fetchAll($sql_languages, $agency_data->id)
		);
	}
	
	public function hasEscort($agency_id, $escort_id, $in_linked_agencies = null )
	{
		$agency_id = intval($agency_id);
		$escort_id = intval($escort_id);
		
		if ( $in_linked_agencies ) {
			$linked_agencies = self::db()->fetchAll('
				SELECT a.id, a.name
				FROM linked_agencies l_a
				INNER JOIN agencies a ON l_a.agency2 = a.id
				WHERE l_a.agency1 = ?
			', array($agency_id));
			
			
			
			$linked_ids = array($agency_id);
			foreach($linked_agencies as $l_agency) {
				$linked_ids[] = $l_agency->id;
			}
			
			return self::db()->fetchOne('SELECT 1 FROM escorts WHERE agency_id IN (' . implode(',', $linked_ids) . ') AND id = ?', array($escort_id));
			
			
		} else {
			return self::db()->fetchOne('SELECT 1 FROM escorts WHERE agency_id = ? AND id = ?', array($agency_id, $escort_id));
		}
		
	}
	
	private static function _getSlugCount($agency_id, $slug)
	{
		return self::db()->fetchOne('SELECT COUNT(id) FROM agencies WHERE id <> ? AND slug = ?', array($agency_id, $slug));
	}
	
	private static function clear_string($str)
	{
		$str = strtolower($str);

		$str = preg_replace("#[^0-9a-z]#", '-', $str);

		$str = preg_replace("#\s+#", '-', $str);

		$str = self::___clean_str($str);

		return $str;
	}
	
	private static function ___clean_str($str)
	{
		$str = strtolower($str);
		$str = str_replace(array(' ', '_'), '-', $str);
		while ( FALSE !== @strpos('--', $str) ) {
			$str = str_replace('--', '-', $str);
		}

		$str = str_replace(array('(', ')'), '', $str);

		return $str;
	}

	public function updateAgencyProfile($agency_id, $data)
	{

		$agency_id = intval($agency_id);
		
		/* slug */
		$slug = self::clear_string(strtolower($data['name']));
		
		while (self::_getSlugCount($agency_id, $slug) > 0)
			$slug .= rand(1, 9);
		
		$data['slug'] = $slug;
		/**/
		
		$wt = $data['working_times'];
		$available_24_7 = $data['available_24_7'];
		unset($data['working_times']);
		unset($data['application_id']);

		try {
			self::db()->delete('agency_working_times', self::db()->quoteInto('agency_id = ?', $agency_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		try {
			if(self::app()->id == APP_EG_CO_UK){
				if ( ! $available_24_7 ) {
					foreach($wt as $day => $time){
						self::db()->insert('agency_working_times', 
							array(
								'agency_id' => $agency_id, 
								'day_index' => $day, 
								'time_from' => $time['from'],
								'time_to' => $time['to'])
						);
					}	
				}
			}else{
				if ( ! $available_24_7 ) {
					for($i = 0; $i <= 6; $i++)
					{
						if( isset($wt[$i]) )
						{
							self::db()->insert('agency_working_times', 
								array(
									'agency_id' => $agency_id, 
									'day_index' => $i + 1, 
									'time_from' => $wt[$i]['time_from'],
									'time_from_m' => $wt[$i]['time_from_m'],
									'time_to' => $wt[$i]['time_to'],
									'time_to_m' => $wt[$i]['time_to_m']));
						}
					}
				}
			}
			
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
		
		
		// Agency Languages
		if(self::app()->id == APP_ED){
			$langs = $data['langs'];
			unset($data['langs']);

			try {
				self::db()->delete('agency_langs', self::db()->quoteInto('agency_id = ?', $agency_id));
			} catch (Exception $e) {
				return array('error' => Cubix_Api_Error::Exception2Array($e));
			}

			try {
				for ($i=0; $i < count($langs); $i++) { 
					self::db()->insert('agency_langs', array('agency_id' => $agency_id, 'level' => $langs[$i]["level"], 'lang_id' => $langs[$i]['lang_id']));
				}
			} catch (Exception $e) {
				return array('error' => Cubix_Api_Error::Exception2Array($e));	
			}
		}
		// Agency Languages

		try {
			$old_city_id = self::db()->fetchOne('SELECT city_id FROM agencies WHERE id = ?', $agency_id);
			
			if ($old_city_id != $data['city_id'])
				Cubix_CityAlertEvents::notify ($data['city_id'], NULL, $agency_id);
			
			unset($data['region_id']);
			
			self::db()->update('agencies', $data, self::db()->quoteInto('id = ?', $agency_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function updateAgencyLogo($agency_id, $data)
	{
		$agency_id = intval($agency_id);

		try {			
			self::db()->update('agencies', $data, self::db()->quoteInto('id = ?', $agency_id));
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}

	public function get($agency_id)
	{		
		$sql = "
			SELECT a.id, a.user_id, a.name, a.slug, a.phone, a.phone_instructions, a.email, a.web, a.last_modified, a.logo_hash, a.logo_ext, u.application_id, u.date_registered AS creation_date, a.hit_count
			FROM agencies a
			LEFT JOIN users u ON u.id = a.user_id
			WHERE a.id = ?
		";

		return self::db()->fetchRow($sql, $agency_id);
	}

	public function getByOldId($old_agency_id)
	{
		$sql = "
			SELECT a.id, a.name, a.slug, a.phone, a.phone_instructions, a.email, a.web, a.last_modified, a.logo_hash, a.logo_ext, u.application_id, u.date_registered AS creation_date, a.hit_count
			FROM agencies a
			LEFT JOIN users u ON u.id = a.user_id
			WHERE a.__id = ?
		";

		return self::db()->fetchRow($sql, $old_agency_id);
	}
	
	public function getLinkedAgencies($agency_id)
	{
		ini_set('memory_limit', '2024M');
		//return array();
		$sql = '
			SELECT a.id, a.name
			FROM linked_agencies l_a
			INNER JOIN agencies a ON l_a.agency2 = a.id
			WHERE l_a.agency1 = ?
		';
		
		return self::db()->fetchAll($sql, $agency_id);
	}
	
	public function delete($id) 
	{
		try {
			self::db()->delete('agencies', self::db()->quoteInto('id = ?', $id));
		} catch (Exception $e) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function getPhotos($agency_id, $page, $per_page)
	{
		if (is_array($agency_id))
			$agency_id = reset($agency_id);
		if (is_array($page))
			$page = reset($page);
		if (is_array($per_page))
			$per_page = intval(reset($per_page));
		
		$photos = self::db()->query("
			SELECT SQL_CALC_FOUND_ROWS id, hash, ext FROM agency_photos 
			WHERE agency_id = ? 
			ORDER BY id ASC
			LIMIT " . ($page - 1) * $per_page . ", " . $per_page, 
		$agency_id)->fetchAll();

		$countSql = "SELECT FOUND_ROWS();";
		$count = self::db()->fetchOne($countSql);
		
		return array('list' => $photos, 'count' => $count);
	}
	
	public function removePhotos($ids)
	{		
		try {
			self::db()->query('DELETE FROM agency_photos WHERE id IN (' . $ids . ')');
		} catch (Exception $e) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}
	}
	
	public function addPhoto($agency_id, $image)
	{		
		self::db()->insert('agency_photos', array(
			'agency_id' => $agency_id,
			'hash' => $image['hash'],
			'ext' => $image['ext'],
		));
	}
	
	public function getComments($agency_id, $page, $per_page)
	{
		if (is_array($agency_id))
			$agency_id = reset($agency_id);
		if (is_array($page))
			$page = reset($page);
		if (is_array($per_page))
			$per_page = intval(reset($per_page));
		
		$photos = self::db()->query("
			SELECT SQL_CALC_FOUND_ROWS a.comment, u.username 
			FROM agency_comments a 
			INNER JOIN users u ON a.user_id = u.id 
			WHERE a.status = 1 AND a.agency_id = ?
			ORDER BY a.date DESC
			LIMIT " . ($page - 1) * $per_page . ", " . $per_page, 
		$agency_id)->fetchAll();

		$countSql = "SELECT FOUND_ROWS();";
		$count = self::db()->fetchOne($countSql);
		
		return array('list' => $photos, 'count' => $count);
	}


    public function getAgenciesActiveEscorts($agency_id)
    {
        $sql = '
			SELECT id, name, slug FROM agencies WHERE id = ?
		';

        $agency = self::db()->fetchRow($sql, $agency_id);

        if ( ! $agency ) {
            return null;
        }

        $sql = '
			SELECT
				e.id,
				e.showname,
				e.status,
				e.gender,
                e.deleted_date,
                e.is_suspicious,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.application_id,
                p.name AS package,
                op.expiration_date,
                DATEDIFF(op.expiration_date, NOW()) AS expires,
                op.id AS opid,
                op.status as op_status,
				op.package_id,
				e.has_hh, e.hh_status
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
            LEFT JOIN order_packages op ON op.escort_id = e.id  AND op.status = ? AND op.order_id IS NOT NULL
            LEFT JOIN packages p ON p.id = op.package_id
			WHERE e.agency_id = ?  AND e.status = ? 
 			GROUP BY e.id
		';

        $countSql = '
			SELECT
				COUNT(DISTINCT e.id)
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id
			WHERE e.agency_id = ?  AND e.status = ? ';

        try {
            $agency->escorts = self::db()->query( $sql, array(self::PACKAGE_STATUS_ACTIVE, $agency->id , self::ESCORT_STATUS_ACTIVE))->fetchAll();//,$status1

            $agency->escorts_count = self::db()->fetchOne( $countSql, array( $agency->id , self::ESCORT_STATUS_ACTIVE));//,$status1
        }
        catch ( Exception $e ) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return $agency;
    }

	public function getEscortsByStatusED($agency_id, $page = 1, $per_page = 4, $p_status = null, $is_susp = false, $order = null )
	{
		$sql = '
			SELECT id, name, slug FROM agencies WHERE id = ?
		';

		$agency = self::db()->fetchRow($sql, $agency_id);

		if ( ! $agency ) {
			return null;
		}

		/*if($status == null){
			//Inactive
			$status = " AND (
                NOT e.status & ".self::ESCORT_STATUS_ACTIVE." OR
                    (e.status & ".self::ESCORT_STATUS_OWNER_DISABLED.")
                       )  AND
                      NOT e.status & ".self::ESCORT_STATUS_TEMPRARY_DELETED."";//reset($status);
		}elseif($status == -1){
			$status = " AND (e.status & ".self::ESCORT_STATUS_TEMPRARY_DELETED.")";
		}else{
			// Active
			$status = " AND e.status & ".self::ESCORT_STATUS_ACTIVE." AND NOT e.status & ".self::ESCORT_STATUS_OWNER_DISABLED;
		}*/

		$status = ' ';
		if ( $p_status == 'active_owner_disabled' ) {
			$status = " AND (e.status & " . self::ESCORT_STATUS_ACTIVE . " OR e.status & " . self::ESCORT_STATUS_OWNER_DISABLED . ") ";
		}

		$page = intval($page);
		$per_page = intval($per_page);

		if ( $page < 1 ) $page = 1;
		if ( $per_page < 1 ) $per_page = 1;

		$page--;
		$offset = $page * $per_page;
		$limit = $per_page;
		// <--

		$wh = " ";
		if ( $is_susp ) {
			$wh = " AND e.is_suspicious = 1 ";
		}

		$order = (strlen($order) && $order != '0')   ?  $order : 'p.name DESC' ;

		$sql = '
			SELECT
				e.id,
				e.showname,
				e.status,
				e.gender,
                e.deleted_date,
                e.is_suspicious,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				eph.status AS photo_status,
				u.application_id,
                p.name AS package,
                op.expiration_date,
                DATEDIFF(op.expiration_date, NOW()) AS expires,
                op.id AS opid,
                op.status as op_status,
				op.package_id,
				e.has_hh, e.hh_status
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
            LEFT JOIN order_packages op ON op.escort_id = e.id  AND op.status = ? AND op.order_id IS NOT NULL
            LEFT JOIN packages p ON p.id = op.package_id
			WHERE u.application_id = ? AND e.agency_id = ? ' . $status . ' AND NOT e.status & ? ' . $wh . '
 			GROUP BY e.id
			ORDER BY ' . $order . '
            LIMIT ?, ?
		';

		$countSql = '
			SELECT
				COUNT(DISTINCT e.id)
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id
			WHERE u.application_id = ? AND e.agency_id = ? ' . $status . ' AND NOT e.status & ? ' . $wh . '
		';


		try {
			$agency->escorts = self::db()->query($sql, array(self::PACKAGE_STATUS_ACTIVE, self::app()->id, $agency->id, self::ESCORT_STATUS_DELETED, $offset, $limit))->fetchAll();//,$status1

			$agency->escorts_count = self::db()->fetchOne($countSql, array(self::app()->id, $agency->id,self::ESCORT_STATUS_DELETED));//,$status1
		}
		catch ( Exception $e ) {
			return array('error' => Cubix_Api_Error::Exception2Array($e));
		}

		return $agency;
	}

	public function hasAddedAnyEscort($agency_id)
    {
        $sql = 'SELECT 1
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			WHERE u.application_id = ? AND e.agency_id = ? ';

        return self::db()->fetchOne($sql, array(self::app()->id, $agency_id));
    }

    public function hasFemaleEscort($agency_id)
    {
        $sql = 'SELECT TRUE
			FROM escorts e
			INNER JOIN agencies a ON a.id = e.agency_id
			WHERE e.agency_id = ? AND e.gender = ?';

        return self::db()->fetchOne($sql, array( $agency_id, GENDER_FEMALE));
    }

	public function getPhoneById($agency_id)
	{
		return self::db()->fetchOne("SELECT contact_phone_parsed FROM agencies WHERE id = ?", $agency_id);
	}
	
	public function getAllAgencyIds()
	{
		return self::db()->fetchAll("SELECT a.id FROM agencies a
								INNER JOIN users u ON u.id = a.user_id 
								WHERE u.status = 1 AND u.application_id = ?", array(self::app()->id) );
		
	}
	
	public function hasAgeVerEscort($agency_id)
	{
		return self::db()->fetchOne("SELECT 1 FROM escorts WHERE agency_id = ? AND ( need_age_verification = 1 OR need_age_verification = 2 OR need_age_verification = 4) AND (status & 32 OR status & 8)", array($agency_id));
	}

	public function hasAgeVerEscortMob($agency_id)
	{
		return self::db()->fetchAll("SELECT id, showname FROM escorts WHERE agency_id = ? AND need_age_verification = 4", array( $agency_id ));
	}
	
	public function getNeedVerEscorts($agency_id)
	{
		return self::db()->fetchAll(" SELECT e.id, e.showname FROM escorts e 
									  WHERE e.agency_id = ? AND NOT e.status & ? AND  ( e.need_age_verification = 1 OR e.need_age_verification = 4) ", array($agency_id, self::ESCORT_STATUS_DELETED));
	}
	
	public function checkVerificationTimeLimit($user_id)
	{
		$user_id = intval($user_id);
		
		$escorts = self::db()->fetchAll('SELECT id FROM escorts WHERE verified_status = 2 AND NOT status & ? AND user_id = ?', array(self::ESCORT_STATUS_DELETED, $user_id));

		if ($escorts)
		{
			$e_ids = array();

			foreach ($escorts as $e)
			{
				$e_ids[] = $e->id;
			}
		
			$sql = '
				SELECT e.id,e.showname, UNIX_TIMESTAMP(MAX(vr.date)) as date  FROM verify_requests vr
				INNER JOIN escorts e ON e.id = vr.escort_id 
				WHERE  e.verified_status = 2 AND e.stays_verified = 0 AND vr.status = 2 AND vr.escort_id IN ('.implode(',',$e_ids). ')
				GROUP BY vr.escort_id	
				HAVING DATE_ADD(DATE_SUB(now(), INTERVAL 6 MONTH), INTERVAL 14 DAY) > MAX(vr.date)
			';
			
			$result = self::db()->fetchAll($sql);
			return $result;
		}
		return NULL;
	}
	
	public function updateLastModified($agency_id)
	{
		try {
			self::db()->query('UPDATE agencies SET last_modified = NOW() WHERE id = ?', $agency_id);				
		} catch (Exception $e) {
			return false;
		}
      
        return true;
	}

    public function getActiveEscortsInfo($agency_id)
    {
        ini_set('memory_limit', '4048M');

        $sql = '
			SELECT
				e.id,
				e.showname,
				e.status
			FROM escorts e
		      INNER JOIN users u ON u.id = e.user_id
			WHERE u.application_id = ? AND e.agency_id = ? AND e.status & ? 
 			GROUP BY e.id
		';

        try {
            $result = self::db()->query($sql, array(self::app()->id, $agency_id, self::ESCORT_STATUS_ACTIVE))->fetchAll();
        }
        catch ( Exception $e ) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }

        return $result;
    }

    public function getAgencyEscortsHitsForWeek($agency_id) {
        $agency_id = intval($agency_id);
        return self::db()->query('SELECT e.showname,SUM(h.count) as count 
        		FROM agencies ag
				INNER JOIN escorts e on e.agency_id = ag.id
				INNER JOIN escort_hits_daily h ON h.escort_id = e.id AND date >= DATE_ADD(DATE(NOW()), INTERVAL - 7 DAY)
				where ag.id = ?
				GROUP BY h.escort_id 
				order by SUM(h.count) DESC limit 10', array($agency_id))->fetchAll();
  	}

    public function getPmIsBlocked($agency_id)
    {
        $sql = "SELECT u.pm_is_blocked from users as u 
                INNER JOIN escorts as e on u.id = e.user_id 
                WHERE u.application_id = ? AND  e.agency_id = ? limit 1";
        $row = self::db()->fetchOne($sql,  array(self::app()->id, $agency_id));
        return $row;
  	}

    public function getField($agency_id, $field) {
        $agency_id = intval($agency_id);

        return self::db()->fetchOne('SELECT ' . $field . ' FROM agencies WHERE id = ?', $agency_id);
    }

    public function updateField($agency_id, $field, $value) {
        $agency_id = intval($agency_id);

        self::db()->update('agencies', array($field => $value),  self::db()->quoteInto('id = ?', $agency_id));
    }

    public function getAgenciesActiveGotdExpiration($agency_id) {

        $sql = "
			SELECT
                go.escort_id,
                e.showname,
                go.creation_date,
                go.activation_date,
                c.title_en as city,
                go.`status`
            FROM
                gotd_orders go
                INNER JOIN escorts e ON e.id = go.escort_id
                INNER JOIN cities c ON c.id = go.city_id 
            WHERE
                go.`status` = 2 AND e.agency_id = ?
		";

        $gotd = self::db()->fetchAll($sql, array($agency_id));

        return $gotd;
    }


    public function getActiveEscortsPackages($agency_id)
    {
        ini_set('memory_limit', '2048M');

        $sql = '
			SELECT
				op.id AS opid, op.date_activated, op.expiration_date, p.name, op.status, op.package_id
			FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN order_packages op ON op.escort_id = e.id
            INNER JOIN packages p ON p.id = op.package_id 
			WHERE 
				u.application_id = ? AND e.agency_id = ? AND e.status & ? AND op.status = ? AND op.order_id IS NOT NULL
 			ORDER BY e.id
		';

        try {
            $active = self::db()->query($sql, array(self::app()->id, $agency_id, self::ESCORT_STATUS_ACTIVE, self::PACKAGE_STATUS_ACTIVE))->fetchAll();

            if ( ! $active ) return null;

            $sql = '
			SELECT
                p.`name`,
                op.period 
            FROM
                order_packages op
                INNER JOIN orders o ON o.id = op.order_id
                INNER JOIN packages p ON p.id = op.package_id
                INNER JOIN escorts e ON e.id = op.escort_id 
                INNER JOIN agencies ag ON ag.id = e.agency_id 
            WHERE
                ag.id = ?
				AND p.is_default = 0
				AND op.status = ' . self::PACKAGE_STATUS_PENDING . '
				AND o.order_date >= ?
				AND op.activation_type = ' . self::PACKAGE_ACTIVATION_TYPE_AFTER_CURRENT_PACKAGE_EXPIRES . '
			ORDER BY o.order_date
			LIMIT 1
		';

            $after = self::db()->fetchRow($sql, array($agency_id, new Zend_Db_Expr($active->date_activated)));

            return array('active' => $active, 'after' => $after);
        }
        catch ( Exception $e ) {
            return array('error' => Cubix_Api_Error::Exception2Array($e));
        }
    }

    public function getUpdatesBy($agency_id)
    {
        $sql = "SELECT updates_by FROM agencies WHERE id = ?";
        return self::db()->fetchOne($sql, array($agency_id));
    }

    public function getCountriesHavingAgency($lng = 'en')
    {

        $sql = 'SELECT
                c.id,
                c.iso,
                c.title_' . $lng . ' as country_title,
                a.`name` as agency_name
            FROM countries c
            LEFT JOIN agencies a ON c.id = a.country_id
            WHERE 1
            GROUP BY c.id';

        return self::db()->fetchAll($sql);
    }

}
