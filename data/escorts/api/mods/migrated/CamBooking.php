<?php
class Cubix_Api_Module_CamBooking extends Cubix_Api_Module 
{
	public function get($escort_id){
		$escort_data = self::db()->fetchAll('
			SELECT * FROM escort_cam_bookings WHERE escort_id = ?
		', array($escort_id));

		 return $escort_data[0];
	}

	public function saveCamSettings($data){
	
		$data['escort_id'] = intval($data['escort_id']);
		
		$sql = 'INSERT INTO escort_cam_bookings(escort_id,status,price_30,price_60,contact_option) 
					VALUES 
					   (?,'.$data['status'].', '.$data['30min'].', '.$data['60min'].',"'.$data['contact_option'].'") 
					ON DUPLICATE KEY UPDATE contact_option = "'.$data['contact_option'].'",status = '.$data['status'].',price_30 = '.$data['30min'].',price_60 = '.$data['60min'].'';
		
		self::db()->query($sql, $data['escort_id']);
		self::db()->query('UPDATE escorts SET cam_booking_status = ? WHERE id = ?', array($data['status'], $data['escort_id']));
		
		if($data['status'] == 1){
			Cubix_SyncNotifier::notify($data['escort_id'],Cubix_SyncNotifier::EVENT_CAM_BOOKING_ACTIVATED);
		}elseif($data['status'] == 0){
			Cubix_SyncNotifier::notify($data['escort_id'],Cubix_SyncNotifier::EVENT_CAM_BOOKING_DEACTIVATED);
		}
		
		return array('status' => true);	
	}

    public function save($data)
    {
        $db = self::db();
        $db->insert('video_chat_requests', $data);
    
        return $db->lastInsertId();
    }
    
    public function update($id, $data)
    {
        $db = self::db();
        $escort_id_exists = $db->fetchOne('SELECT escort_id FROM video_chat_requests WHERE id = ?', array($id));
        if($escort_id_exists){
            $db->update('video_chat_requests', $data, self::db()->quoteInto('id = ?', $id));
            return $escort_id_exists;
        }
        else{
            return false;
        }
    }
    
    public function checkBySession($id, $session)
    {
        $db = self::db();
        return $db->fetchOne('SELECT TRUE FROM video_chat_requests WHERE id = ? AND session_id = ? AND status = 1', array($id, $session));
    }
    
    public function getData($escort_id)
    {
        $db = self::db();
        return $db->fetchRow('SELECT e.showname, es.* FROM escort_cam_bookings es
                        INNER JOIN escorts e ON e.id = es.escort_id WHERE es.escort_id = ?', array($escort_id));
    }
    
    public function getStatus($id)
    {
        $db = self::db();
        return $db->fetchOne('SELECT status FROM video_chat_requests WHERE id = ?', array($id));
    }
    
    public function getPrice($escort_id, $duration)
    {
        $db = self::db();
        if(in_array($duration, array(15,30,60))){
            if(self::app()->id == APP_A6 || self::app()->id == APP_EF){
                if($duration == 30){
                    $fake_duration = 60;
                }
                elseif($duration == 15){
                    $fake_duration = 30;
                }
                $field = 'price_'.$fake_duration;
            }
            else{
                $field = 'price_'.$duration;
            }
            return $db->fetchOne('SELECT '. $field. ' from escort_cam_bookings  WHERE escort_id = ?', array($escort_id));
        }
    }
    
    public function getForSuccess($request_id)
    {
        $db = self::db();
        return $db->fetchRow('SELECT e.id,u.id as user_id, u.username, u.user_type, u.email, u.sales_user_id, ep.showname, ep.contact_phone_parsed as phone, vchr.duration, es.escort_cam_bookings_username FROM video_chat_requests vchr
                            INNER JOIN escorts e ON e.id = vchr.escort_id
                            INNER JOIN users u ON u.id = e.user_id
                            INNER JOIN escort_cam_bookings es ON es.escort_id = vchr.escort_id
                            INNER JOIN escort_profiles_v2 ep ON ep.escort_id = vchr.escort_id
                            WHERE vchr.id = ?', array($request_id));
    }       
        
    public function data_to_show_client( $request_id ){
        $db = self::db();

        return $db->fetchRow('SELECT vr.duration,vr.amount,vr.schedule_time,es.escort_id, es.contact_option, e.showname, e.cam_channel_url, es.contact_option, pf.contact_phone_parsed, u.email, vr.user_id as user_id, u.sales_user_id as backend_user
                                FROM video_chat_requests vr
                                inner join escort_cam_bookings es on es.escort_id = vr.escort_id
                                inner join escorts e on e.id = es.escort_id
                                inner join escort_profiles_v2 pf on pf.escort_id = e.id
                                inner join users u on u.id = e.user_id
                                where vr.id = ?', array($request_id));
    }

}
