<?php

class System_Service
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;
	
	protected $_config;
	
	protected $_app;
	
	public function __construct()
	{
		$this->_db = Zend_Registry::get('db');
		$this->_config = Zend_Registry::get('config');
		$this->_app = Zend_Registry::get('app');
	}
}
