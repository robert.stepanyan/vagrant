<?php

class System_Api
{
	public static function getAppByApiKey($api_key)
	{
		$db = Zend_Registry::get('db');
		$app = $db->query('
			SELECT * FROM applications WHERE api_key = ?
		', $api_key)->fetch();
		
		if ( ! $app ) return null;
		
		return $app;
	}
}
