<?php

global $config, $db, $app, $DEFINITIONS;
$config = Zend_Registry::get('config');
$db = Zend_Registry::get('db');
$db->setFetchMode(Zend_Db::FETCH_ASSOC);
$app = Zend_Registry::get('app');

function addActivePackageWhereParams($escort_alias = 'e') {
    global $app;
    $zero_packages = array(
        APP_EF => 100
    );
    $paid_package_apps = array(APP_EF, APP_A6);

    if (in_array($app->id, $paid_package_apps)) {

        $where = ' AND ' . $escort_alias . '.status & 32 AND op.package_id <> ' . $zero_packages[$app->id] . ' AND op.order_id IS NOT NULL AND op.status = 2 ';
        if ($app->id == APP_A6) {
            $where = ' AND ' . $escort_alias . '.status & 32 AND op.order_id IS NOT NULL AND op.status = 2 ';
        }

        return array(
            'where' => $where,
            'join' => ' INNER JOIN order_packages op ON op.escort_id = ' . $escort_alias . '.id '
        );
    }

    return array(
        'where' => ' ',
        'join' => ' '
    );
}

function getEscortsTableDump() {
    return '
		CREATE TABLE `escorts` (
			`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
			`agency_id` int(10) UNSIGNED NULL DEFAULT NULL ,
			`showname`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
			`gender`  tinyint(1) UNSIGNED NOT NULL ,
			`country_id`  int(10) UNSIGNED NULL DEFAULT NULL ,
			`city_id`  int(10) UNSIGNED NULL DEFAULT NULL ,
			`cityzone_id`  int(10) UNSIGNED NULL DEFAULT NULL ,
			`verified_status`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 ,
			`ethnicity`  varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
			`nationality_id` int(10) UNSIGNED NULL DEFAULT NULL,
			`age`  tinyint(2) UNSIGNED NULL DEFAULT NULL ,
			`eye_color`  tinyint(2) UNSIGNED NULL DEFAULT NULL ,
			`hair_color`  tinyint(2) NULL DEFAULT NULL ,
			`hair_length`  tinyint(2) NULL DEFAULT NULL ,
			`height`  int(3) UNSIGNED NULL DEFAULT NULL ,
			`weight`  int(3) NULL DEFAULT NULL ,
			`breast_size`  char(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
			`dress_size`  int(2) NULL DEFAULT NULL ,
			`shoe_size`  int(3) NULL DEFAULT NULL ,
			`is_smoker`  tinyint(1) UNSIGNED NULL DEFAULT NULL ,
			`availability`  tinyint(2) UNSIGNED NULL DEFAULT NULL ,
			`sex_availability`  set("men","women","couples","trans","gays","plus2") CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
			`languages`  set("en", "fr", "de", "it", "pt", "ru", "es", "sa", "be", "bg", "cn", "cz", "hr", "nl", "fi", "gr", "hu", "in", "jp", "lv", "pl", "ro", "ro", "tr", "pk", "sk", "sl", "dk", "no", "se", "hk", "bd", "kr", "vn", "ua", "ir", "np", "th", "sv", "da", "rs") CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
			`svc_kissing`  tinyint(1) UNSIGNED NULL DEFAULT NULL ,
			`svc_blowjob`  tinyint(1) UNSIGNED NULL DEFAULT NULL ,
			`svc_69`  tinyint(1) UNSIGNED NULL DEFAULT NULL ,
			`svc_anal`  tinyint(1) UNSIGNED NULL DEFAULT NULL ,
			`svc_cumshot`  tinyint(1) UNSIGNED NULL DEFAULT NULL ,
			`is_now_open`  tinyint(1) NULL DEFAULT NULL ,
			`is_on_tour`  tinyint(1) UNSIGNED NOT NULL ,
			`tour_id`  int(10) UNSIGNED NULL DEFAULT NULL ,
			`tour_city_id`  int(10) NULL DEFAULT NULL ,
			`tour_date_from`  date NULL DEFAULT NULL ,
			`tour_date_to`  date NULL DEFAULT NULL ,
			`vac_date_from`  date NULL DEFAULT NULL ,
			`vac_date_to`  date NULL DEFAULT NULL ,
			`is_new`  tinyint(1) UNSIGNED NOT NULL ,
			`date_registered`  date NULL DEFAULT NULL ,
			`date_last_modified`  date NULL DEFAULT NULL ,

			`photo_hash`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
			`photo_ext`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
			`photo_status`  tinyint(1) NOT NULL DEFAULT 3 ,


			`rates`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
			`hit_count` int(11) unsigned DEFAULT \'0\',
			`ordering`  int(5) UNSIGNED NOT NULL ,
			PRIMARY KEY (`id`),
			KEY `agency_id` (`agency_id`),
			KEY `city_id` (`city_id`),
			KEY `cityzone_id` (`cityzone_id`),
			KEY `showname` (`showname`),
			KEY `gender` (`gender`),
			KEY `is_now_open` (`is_now_open`),
			KEY `is_on_tour` (`is_on_tour`),
			KEY `tour_id` (`tour_id`),
			KEY `tour_city_id` (`tour_city_id`),
			KEY `is_new` (`is_new`),
			KEY `date_registered` (`date_registered`),
			KEY `date_last_modified` (`date_last_modified`)
		) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
	';
}

function getEscortsV2TableDump() {
    global $db, $app;

    $fields = "";
    $about_soft = "";
    if ($app->id == APP_A6) {
        $fields = "
			`a_city_id` int(10) unsigned DEFAULT NULL,
			`a_fake_city_id` int(10) unsigned DEFAULT NULL,
			`a_zip` varchar(10) DEFAULT NULL,
			`a_address` varchar(255) DEFAULT NULL,
			`a_name` varchar(60) DEFAULT NULL,
			`a_slug` varchar(150) DEFAULT NULL,
			`f_city_id` int(10) unsigned DEFAULT NULL,
			`f_zip` varchar(10) DEFAULT NULL,
			`a_is_anonymous` tinyint(1) unsigned DEFAULT 0,

			`a_email` varchar(50) DEFAULT NULL,
			`a_phone` varchar(30) DEFAULT NULL,
			`a_phone_instructions` varchar(255) DEFAULT NULL,
			`a_phone_country_id` smallint(5) unsigned DEFAULT NULL,
			`pseudo_escort` tinyint(1) DEFAULT 0,
		";
    }

    if ($app->id == APP_6A || $app->id == APP_EF) {
        $about_soft = "
			`about_soft_en` text,
			`about_soft_it` text,
			`about_soft_gr` text,
			`about_soft_fr` text,
			`about_soft_de` text,
			`about_soft_pt` text,
			`about_soft_nl` text,
			`about_soft_my` text,
			`about_soft_cn` text,
			`gallery_is_checked` tinyint(1) unsigned NOT NULL DEFAULT 0,
		";
    }

    if ($app->id == APP_6A) {
        $fields = "
			`vip_verified` tinyint(1) unsigned DEFAULT 0,
		";
    }

    $fields_bl = '';
    if ($app->id == APP_BL) {
        $fields_bl .= "
			`snapchat` tinyint(1) unsigned DEFAULT 0,
			`snapchat_username` varchar(255) DEFAULT NULL,
            `ssignal` tinyint(1) unsigned DEFAULT 0,
			
		";
    }

    $fields_6nun = '';
    if ($app->id == APP_6B) {
        $fields_6nun .= "
			`twitter_name` varchar(255) DEFAULT NULL,
			`ssignal` tinyint(1) unsigned DEFAULT 0,
		";
    }

    if ($app->id == APP_ED) {
        $fields = "
			`email` varchar(255) DEFAULT NULL,
  			`website` varchar(255) DEFAULT NULL,
  			`phone_instr` tinyint(1) DEFAULT NULL,
			`phone_instr_no_withheld` tinyint(1) DEFAULT NULL,
			`phone_instr_other` varchar(255) DEFAULT NULL,
			`phone_country_id` smallint(4) unsigned DEFAULT NULL,
			`disable_phone_prefix` tinyint(1) unsigned NOT NULL DEFAULT '0',
			`phone_number` varchar(100) DEFAULT NULL,
			`phone_number_free` varchar(100) DEFAULT NULL,
			`show_agency_escorts` tinyint(1) unsigned DEFAULT 0,
			`status_active_date`  datetime NULL DEFAULT NULL,
			`last_hand_verification_date` timestamp NULL DEFAULT NULL,
			`is_agency` tinyint(1) DEFAULT 0,
			`about_ru` text,
			`about_cz` text,
			`about_pl` text,
			`about_sk` text,
			`tatoo` tinyint(1) unsigned DEFAULT NULL,
  			`piercing` tinyint(1) unsigned DEFAULT NULL,
  			`wechat` tinyint(1) unsigned DEFAULT 0,
  			`ssignal` tinyint(1) unsigned DEFAULT 0,
		";
    }

    if ($app->id == APP_EI) {
        $fields = "
  			`phone_instr` tinyint(1) DEFAULT NULL,
			`phone_instr_no_withheld` tinyint(1) DEFAULT NULL,
			`phone_instr_other` varchar(255) DEFAULT NULL,
			`phone_country_id` smallint(4) unsigned DEFAULT NULL,
			`disable_phone_prefix` tinyint(1) unsigned NOT NULL DEFAULT '0',
			`phone_number` varchar(100) DEFAULT NULL,
		";
    }

    if ($app->id == APP_EG_CO_UK) {
        $fields = "
			`show_on_main` tinyint(1) unsigned DEFAULT 0,
			`last_hand_verification_date` timestamp NULL DEFAULT NULL,
			`ssignal` tinyint(1) unsigned DEFAULT 0,
		";
    }
    if ( in_array( $app->id, array( APP_AE,APP_EM ))) {
        $fields = "
			`ssignal` tinyint(1) unsigned DEFAULT 0,
		";
    }
    if ($app->id == APP_EG) {
        $fields = "
			`last_hand_verification_date` timestamp NULL DEFAULT NULL,
		";
    }
    if ($app->id == APP_6C) {
        $fields = "
			`last_hand_verification_date` timestamp NULL DEFAULT NULL,
		";
    }

    if ($app->id == APP_6B) {
        $fields = "
			`last_hand_verification_date` timestamp NULL DEFAULT NULL,
		";
    }

	if ($app->id == APP_EF) {
        $fields = "
			`phone_instr` tinyint(1) DEFAULT NULL,
			`skype_call_status` tinyint(1) DEFAULT 0,
		";
    }
	
    // Create fields and fill predefined values for blacklisted countries
    if ($app->id == APP_BL || $app->id == APP_A6) {
        $countries = $db->query('SELECT id FROM countries_all')->fetchAll();
    }
    else {
        $countries = $db->query('SELECT id FROM countries')->fetchAll();
    }

    $c_arr = array();
    $i = 0;
    foreach ($countries as $k => $id) {
        if ($k % 62 == 0) {
            $i++;
        }
        $c_arr[$i][] = $id['id'];
    }

    $blacklisted_countries_set_str = "";
    foreach ($c_arr as $k => $ca) {
        array_push($ca, 9999);
        $blacklisted_countries_set_str .= '`blacklisted_countries_' . $k . '` set("' . join('", "', $ca) . '") DEFAULT \'\',';
    }
    // Create fields and fill predefined values for blacklisted countries

    $aboutAPP_A6 = '';
    if ($app->id == APP_A6) {
        $aboutAPP_A6 = ' `about_hu` text,';
    }

	if ($app->id == APP_EF || $app->id == APP_A6 || $app->id == APP_BL) {
        $fields .= "
			`cam_status` varchar(20) NOT NULL DEFAULT '',
			`cam_preview_url`  varchar(100) NULL DEFAULT NULL,
			`cam_channel_url`  varchar(100) NULL DEFAULT NULL,
			`cam_booking_status` tinyint(1) DEFAULT 0,
		";
    }
	
    $dump = '
		CREATE TABLE `escorts` (
			`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
			`user_id`  int(10) UNSIGNED NOT NULL ,
			`agency_id` int(10) UNSIGNED NULL DEFAULT NULL ,
			`showname`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
			`username` varchar(30) NOT NULL,
			`slogan`  varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL ,
			`gender`  tinyint(1) UNSIGNED NOT NULL ,
			`country_id`  int(10) UNSIGNED NULL DEFAULT NULL ,
			`city_id`  int(10) UNSIGNED NULL DEFAULT NULL ,
			`cityzone_id`  int(10) UNSIGNED NULL DEFAULT NULL ,
			`verified_status`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 ,

			' . $blacklisted_countries_set_str . '

			`is_suspicious`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
			`suspicious_comment` text,
			`susp_comment_show` tinyint(1) unsigned NOT NULL DEFAULT \'0\',

			`ethnicity`  varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
			`nationality_id` int(10) UNSIGNED NULL DEFAULT NULL,
			`bubble_text` text,

			`agency_name` varchar(60) default NULL,
			`agency_slug` varchar(60) default NULL,
			`votes_sum` int(10) unsigned NOT NULL DEFAULT \'0\',
			`votes_count` int(10) unsigned NOT NULL DEFAULT \'0\',
			`voted_members` text,
			`gotm_month` tinyint(2) unsigned NULL DEFAULT NULL,
			`gotm_year` smallint(4) unsigned NULL DEFAULT NULL,
			`fuckometer` decimal(3,1) unsigned NOT NULL DEFAULT \'0.0\',
			`fuckometer_rank` varchar(255) NULL DEFAULT \'\',
			`close_time` tinyint(2) DEFAULT NULL,

			`hh_is_active` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
			`hh_date_from` timestamp NULL DEFAULT NULL,
			`hh_date_to` timestamp NULL DEFAULT NULL,
			`hh_motto` varchar(150) DEFAULT NULL,

			`review_count` int(10) unsigned NOT NULL DEFAULT \'0\',
			`comment_count` int(10) unsigned NOT NULL DEFAULT \'0\',

			`block_website` tinyint(1) unsigned NULL DEFAULT \'0\',
			`check_website` tinyint(1) unsigned NULL DEFAULT \'0\',
			`is_pornstar` tinyint(1) unsigned NULL DEFAULT \'0\',
			`bad_photo_history` tinyint(1) unsigned NULL DEFAULT \'0\',

			`about_en` text,
			`about_it` text,
			`about_gr` text,
			`about_fr` text,
			`about_de` text,
			`about_pt` text,
			`about_nl` text,
			`about_my` text,
			`about_cn` text,
			`about_es` text,
			`about_ro` text,
			' . $aboutAPP_A6 . '
			' . $about_soft . '

			`age`  tinyint(2) UNSIGNED NULL DEFAULT NULL ,
			`eye_color`  tinyint(2) UNSIGNED NULL DEFAULT NULL ,
			`hair_color`  tinyint(2) NULL DEFAULT NULL ,
			`hair_length`  tinyint(2) NULL DEFAULT NULL ,
			`height`  int(3) UNSIGNED NULL DEFAULT NULL ,
			`weight` float(12,8) unsigned DEFAULT NULL,
			`cup_size`  char(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
			`breast_type`  tinyint(2) UNSIGNED NULL DEFAULT NULL ,
			`dress_size`  varchar(3) DEFAULT NULL,
			`pubic_hair` tinyint(1) unsigned DEFAULT NULL,
			`shoe_size`  int(3) NULL DEFAULT NULL ,
			`is_smoking`  tinyint(1) UNSIGNED NULL DEFAULT NULL ,
			`is_drinking`  tinyint(1) UNSIGNED NULL DEFAULT NULL ,

			`cuisine` varchar(255) DEFAULT NULL,
			`drink` varchar(255) DEFAULT NULL,
			`flower` varchar(255) DEFAULT NULL,
			`gifts` varchar(255) DEFAULT NULL,
			`perfume` varchar(255) DEFAULT NULL,
			`designer` varchar(255) DEFAULT NULL,
			`interests` varchar(255) DEFAULT NULL,
			`sports` varchar(255) DEFAULT NULL,
			`hobbies` varchar(255) DEFAULT NULL,

			`min_book_time` VARCHAR(255) DEFAULT NULL,
			`min_book_time_unit` TINYINT(1) DEFAULT NULL,

			`has_down_pay` INT(1) DEFAULT NULL,
			`down_pay` VARCHAR(255) DEFAULT NULL,
			`down_pay_cur` INT(10) DEFAULT NULL,

			`reservation` INT(10) DEFAULT NULL,

			`zip` varchar(100) DEFAULT NULL,
		    `incall_type` tinyint(1) unsigned DEFAULT NULL,
		    `incall_hotel_room` tinyint(1) unsigned DEFAULT NULL,
		    `incall_other` varchar(50) DEFAULT NULL,
		    `outcall_type` tinyint(1) unsigned DEFAULT NULL,
		    `outcall_other` varchar(50) DEFAULT NULL,

			`sex_orientation` tinyint(1) unsigned DEFAULT NULL,
			`sex_availability`  set("men","women","couples","trans","gays","plus2") CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
			`languages`  set("en", "fr", "de", "it", "pt", "ru", "es", "sa", "be", "bg", "cn", "cz", "hr", "nl", "fi", "gr", "hu", "in", "jp", "lv", "pl", "ro", "tr", "pk", "sk", "sl", "dk", "no", "se", "hk", "bd", "kr", "vn", "ua", "ir", "np", "th", "sv", "da", "rs") CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,

			`is_now_open`  tinyint(1) NULL DEFAULT NULL ,
			`is_on_tour`  tinyint(1) UNSIGNED NOT NULL ,
			`tour_id`  int(10) UNSIGNED NULL DEFAULT NULL ,
			`tour_city_id`  int(10) NULL DEFAULT NULL ,
			`tour_date_from`  timestamp NULL DEFAULT NULL ,
			`tour_date_to`  timestamp NULL DEFAULT NULL ,
			`minutes_from` smallint(4) unsigned DEFAULT NULL,
			`minutes_to` smallint(4) unsigned DEFAULT NULL,
			`vac_date_from`  date NULL DEFAULT NULL ,
			`vac_date_to`  date NULL DEFAULT NULL ,
			`tour_phone` varchar(35) DEFAULT NULL ,
			`is_new`  tinyint(1) UNSIGNED NOT NULL ,
			`date_registered`  datetime NULL DEFAULT NULL ,
			`date_last_modified`  datetime NULL DEFAULT NULL ,
			`date_last_verified`  datetime NULL DEFAULT NULL ,
			`date_activated` date DEFAULT NULL,
			`last_login_date`  timestamp NULL DEFAULT NULL ,

			`photo_hash`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
			`photo_ext`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
			`photo_status`  tinyint(1) NOT NULL DEFAULT 3 ,
			`photo_args` varchar(255) DEFAULT NULL,


			`rates`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
			`travel_payment_methods`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
			`hit_count` int(11) unsigned DEFAULT \'0\',
			`ordering`  int(5) UNSIGNED NOT NULL ,
			`available_24_7`  tinyint(1) UNSIGNED DEFAULT NULL ,
			`night_escort`  tinyint(1) UNSIGNED DEFAULT NULL ,
			`is_online` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
			`is_vip` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
			`disabled_reviews` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
			`disabled_comments` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
			`travel_place` int(11) unsigned DEFAULT \'3\',
			`special_rates`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
			`gotd_city_id` int(10) unsigned DEFAULT NULL ,
			`a_web` varchar(255) DEFAULT NULL,
			`a_block_website` tinyint(1) DEFAULT NULL,
			`contact_phone_parsed`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
			`has_big_photos` tinyint(1) DEFAULT 0,
			`has_video` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
			`latitude` varchar(255) DEFAULT NULL,
			`longitude` varchar(255) DEFAULT NULL,
			`allow_show_online` tinyint(1) unsigned DEFAULT 1,
			`phone_additional_1` varchar(100) DEFAULT NULL,
			`phone_additional_2` varchar(100) DEFAULT NULL,
			`phone_country_id_1` smallint(4) unsigned DEFAULT NULL,
			`phone_country_id_2` smallint(4) unsigned DEFAULT NULL,
			`disable_phone_prefix_1` tinyint(1) unsigned DEFAULT 0,
			`disable_phone_prefix_2` tinyint(1) unsigned DEFAULT 0,
			`phone_instr_1` tinyint(1) DEFAULT NULL,
			`phone_instr_2` tinyint(1) DEFAULT NULL,
			`phone_instr_no_withheld_1` tinyint(1) DEFAULT NULL,
			`phone_instr_no_withheld_2` tinyint(1) DEFAULT NULL,
			`phone_instr_other_1` varchar(255) DEFAULT NULL,
			`phone_instr_other_2` varchar(255) DEFAULT NULL,
			`telegram` tinyint(1) unsigned DEFAULT 0,
			`viber` tinyint(1) unsigned DEFAULT 0,
			`whatsapp` tinyint(1) unsigned DEFAULT 0,
			`viber_1` tinyint(1) unsigned DEFAULT 0,
			`whatsapp_1` tinyint(1) unsigned DEFAULT 0,
			`viber_2` tinyint(1) unsigned DEFAULT 0,
			`whatsapp_2` tinyint(1) unsigned DEFAULT 0,
            ' . $fields_bl . '
			' . $fields_6nun . '
			`show_popunder` tinyint(1) unsigned DEFAULT 0,
			`type` TINYINT( 1 ) unsigned NULL DEFAULT 1,
			`master_id` INT( 11 ) unsigned NULL,
			' . $fields . '
			PRIMARY KEY (`id`),
			KEY `agency_id` (`agency_id`),
			KEY `city_id` (`city_id`),
			KEY `cityzone_id` (`cityzone_id`),
			KEY `showname` (`showname`(4)),
			KEY `gender` (`gender`),
			KEY `is_now_open` (`is_now_open`),
			KEY `is_on_tour` (`is_on_tour`),
			KEY `tour_id` (`tour_id`),
			KEY `tour_city_id` (`tour_city_id`),
			KEY `is_new` (`is_new`),
			KEY `date_registered` (`date_registered`),
			KEY `hit_count` (`hit_count`),
			KEY `date_last_modified` (`date_last_modified`),
			KEY `gender_isontour` (`gender`, `is_on_tour`),
			KEY `uid_gender` (`user_id`, `gender`)
		) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
	';

    return $dump;
}

function getUpcomingToursTableDump() {
    return "
		CREATE TABLE `upcoming_tours` (
		  `id` int(10) NOT NULL,
		  `tour_id` int(10) DEFAULT NULL,
		  `tour_city_id` int(10) DEFAULT NULL,
		  `tour_date_from` timestamp NULL DEFAULT NULL,
		  `tour_date_to` timestamp NULL DEFAULT NULL,
		  `minutes_from` smallint(4) unsigned DEFAULT NULL,
		  `minutes_to` smallint(4) unsigned DEFAULT NULL,
		  `phone` varchar(35) DEFAULT NULL,
		  KEY `tour_id` (`tour_id`),
		  KEY `tour_city_id` (`tour_city_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
	";
}

function getExtraVipCountriesDump() {
    return "
		CREATE TABLE `extra_vip_countries` (
		`escort_id` int(10) unsigned DEFAULT NULL,
		`country_id` int(10) unsigned DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
}

function getExtraVipCountries() {
    global $db;

    $sql = '
		SELECT o.escort_id, p.country_id
		FROM package_vip_extra_countries p
		INNER JOIN order_packages o ON o.id = p.order_package_id AND o.status = 2
	';

    try {
        $result = $db->query($sql)->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return $result;
}

function getEscortCitiesTableDump() {
    return "
		CREATE TABLE `escort_cities` (
		  `escort_id` int(10) unsigned NOT NULL,
		  `city_id` int(10) unsigned NOT NULL,
		  /*`is_tour` tinyint(1) unsigned NOT NULL DEFAULT 0,*/
		PRIMARY KEY (`escort_id`,`city_id`),
		KEY `escort_id` (`escort_id`),
		KEY `city_id` (`city_id`),
		KEY `eid_cid` (`escort_id`, `city_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
	";
}

/*function getEscortTravelCountriesTableDump()
{
	return "
		CREATE TABLE `escort_travel_countries` (
		  `escort_id` int(10) unsigned NOT NULL,
		  `country_id` int(10) unsigned NOT NULL,
		PRIMARY KEY (`escort_id`,`country_id`),
		KEY `escort_id` (`escort_id`),
		KEY `country_id` (`country_id`),
		KEY `eid_cid` (`escort_id`, `country_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
	";
}*/

function getEscortTravelContinentsTableDump() {
    return "
		CREATE TABLE `escort_travel_continents` (
		  `escort_id` int(10) unsigned NOT NULL,
		  `continent_id` int(10) unsigned NOT NULL,
		  PRIMARY KEY (`escort_id`,`continent_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
}

function getEscortCityzonesTableDump() {
    return "
		CREATE TABLE `escort_cityzones` (
		  `escort_id` int(10) NOT NULL,
		  `city_zone_id` int(10) NOT NULL,
		  PRIMARY KEY (`escort_id`,`city_zone_id`),
		  KEY `escort_id` (`escort_id`),
		  KEY `city_zone_id` (`city_zone_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
	";
}

function getEscortgalleriesTableDump() {
    global $app;

    $fields = '';
    if ($app->id == APP_ED) {
        $fields = '`is_main`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,';
    }

    return "
		CREATE TABLE `escort_galleries` (
			`id` int(10) NOT NULL,
			`escort_id` int(10) NOT NULL,
			`title` varchar(50) DEFAULT NULL,
			" . $fields . "
			PRIMARY KEY (`id`),
			KEY `escort_id` (`escort_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
	";
}

function getNaturalPicsTableDump() {
    return "
		CREATE TABLE `natural_pics` (
			`id` int(10) NOT NULL,
			`escort_id` int(10) NOT NULL,
			`hash`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
			`ext`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
			PRIMARY KEY (`id`),
			KEY `escort_id` (`escort_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
	";
}

function getEuroLotteryTableDump() {
    return "
		CREATE TABLE `euro_lottery` (
			`id` int(10) NOT NULL,
			`escort_id` int(10) NOT NULL,
			`hash`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
			`ext`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
			`votes` int(6) UNSIGNED NOT NULL DEFAULT 0,
			`ordering`  int(5) UNSIGNED NOT NULL DEFAULT 1 ,
			PRIMARY KEY (`id`),
			KEY `escort_id` (`escort_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
	";
}

function getEscortPhotosTableDump() {
    global $db, $app;

    if ($app->id == APP_A6 || $app->id == APP_A6_AT || $app->id == APP_6A) {
        return '
			CREATE TABLE `escort_photos` (
				`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
				`escort_id`  int(10) UNSIGNED NOT NULL ,
				`gallery_id`  int(10) UNSIGNED NOT NULL DEFAULT 0,
				`hash`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
				`ext`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
				`type`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 ,
				`status`  tinyint(1) UNSIGNED NOT NULL DEFAULT 3 ,
				`is_main`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
				`is_mobile_cover`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
				`ordering`  int(5) UNSIGNED NOT NULL DEFAULT 1 ,
				`args` varchar(255) DEFAULT NULL,
				`_inter_index`  int(10) UNSIGNED DEFAULT NULL,
				`is_approved`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
				`double_approved` tinyint(1) unsigned NOT NULL DEFAULT 0,
				`gotd` tinyint(1) unsigned NOT NULL DEFAULT 0,
				`profile_boost` tinyint(1) unsigned NOT NULL DEFAULT 0,
				`is_portrait` tinyint(3) unsigned NOT NULL DEFAULT 0,
				`width` int(10) unsigned DEFAULT NULL,
				`height` int(10) unsigned DEFAULT NULL,
				`retouch_status` tinyint(1) unsigned DEFAULT 0,
				`is_tmp_main`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
				`is_rotatable`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
				`is_suspicious`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
				`creation_date` timestamp NULL DEFAULT NULL,
				PRIMARY KEY (`id`),
				KEY `eid_im_ord` (`escort_id`, `is_main`, `ordering`),
				KEY `eid_im_t_ord` (`escort_id`, `is_main`, `type`, `ordering`)
			) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
		';
    }

    if ($app->id == APP_EF) {
        return '
			CREATE TABLE `escort_photos` (
				`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
				`escort_id`  int(10) UNSIGNED NOT NULL ,
				`gallery_id`  int(10) UNSIGNED NOT NULL DEFAULT 0,
				`hash`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
				`ext`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
				`type`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 ,
				`status`  tinyint(1) UNSIGNED NOT NULL DEFAULT 3 ,
				`is_main`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
				`is_valentines`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
				`is_mobile_cover`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
				`ordering`  int(5) UNSIGNED NOT NULL DEFAULT 1 ,
				`args` varchar(255) DEFAULT NULL,
				`_inter_index`  int(10) UNSIGNED DEFAULT NULL,
				`is_approved`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
				`double_approved` tinyint(1) unsigned NOT NULL DEFAULT 0,
				`gotd` tinyint(1) unsigned NOT NULL DEFAULT 0,
				`profile_boost` tinyint(1) unsigned NOT NULL DEFAULT 0,
				`is_portrait` tinyint(3) unsigned NOT NULL DEFAULT 0,
				`width` int(10) unsigned DEFAULT NULL,
				`height` int(10) unsigned DEFAULT NULL,
				`retouch_status` tinyint(1) unsigned DEFAULT 0,
				`is_tmp_main`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
				`is_rotatable`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
				`is_suspicious`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
				`creation_date` timestamp NULL DEFAULT NULL,
				PRIMARY KEY (`id`),
				KEY `eid_im_ord` (`escort_id`, `is_main`, `ordering`),
				KEY `eid_im_t_ord` (`escort_id`, `is_main`, `type`, `ordering`)
			) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
		';
    }

    return '
		CREATE TABLE `escort_photos` (
			`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
			`escort_id`  int(10) UNSIGNED NOT NULL ,
			`gallery_id`  int(10) UNSIGNED NOT NULL DEFAULT 0,
			`hash`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
			`ext`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
			`type`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 ,
			`status`  tinyint(1) UNSIGNED NOT NULL DEFAULT 3 ,
			`is_main`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
			`ordering`  int(5) UNSIGNED NOT NULL DEFAULT 1 ,
			`args` varchar(255) DEFAULT NULL,
			`_inter_index`  int(10) UNSIGNED DEFAULT NULL,
			`is_approved`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
			`double_approved` tinyint(1) unsigned NOT NULL DEFAULT 0,
			`is_portrait` tinyint(3) unsigned NOT NULL DEFAULT 0,
			`width` int(10) unsigned DEFAULT NULL,
			`height` int(10) unsigned DEFAULT NULL,
			`retouch_status` tinyint(1) unsigned DEFAULT 0,
			`is_tmp_main`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
			`is_rotatable`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
			`is_suspicious`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
			`is_popunder`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
			`popunder_args` varchar(255) DEFAULT NULL,
			`creation_date` timestamp NULL DEFAULT NULL,
			PRIMARY KEY (`id`),
			KEY `eid_im_ord` (`escort_id`, `is_main`, `ordering`),
			KEY `eid_im_t_ord` (`escort_id`, `is_main`, `type`, `ordering`)
		) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
	';
}

function getEscortProfilesTableDump() {
    return '
		CREATE TABLE `escort_profiles` (
			`escort_id`  int(10) UNSIGNED NOT NULL ,
			`data` longtext NOT NULL,
			`lang_id` varchar(2) NOT NULL,
			PRIMARY KEY (`escort_id`, `lang_id`),
			KEY `escort_id` (`escort_id`),
			KEY `lang_id` (`lang_id`)
		) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
	';
}

function getEscortProfilesV2TableDump() {
    return '
		CREATE TABLE `escort_profiles` (
			`escort_id`  int(10) UNSIGNED NOT NULL ,
			`data` longtext NOT NULL,
			PRIMARY KEY (`escort_id`),
			KEY `escort_id` (`escort_id`)
		) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
	';
}

function getEscortServicesV2TableDump() {
    return '
		CREATE TABLE `escort_services` (
		  `escort_id` int(10) unsigned NOT NULL,
		  `service_id` int(10) unsigned NOT NULL,
		  `price` decimal(8,2) unsigned DEFAULT NULL,
		  `currency_id`  int(2) UNSIGNED NULL DEFAULT NULL ,
		  PRIMARY KEY (`escort_id`,`service_id`),
		  UNIQUE KEY `eid_sid` (`escort_id`,`service_id`) USING BTREE,
		  KEY `escort_id` (`escort_id`),
		  KEY `service_id` (`service_id`)
		) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
	';
}

function getEscortKeywordsTableDump() {
    return '
		CREATE TABLE `escort_keywords` (
		`escort_id` int(10) unsigned NOT NULL,
		`keyword_id` int(10) unsigned NOT NULL,
		PRIMARY KEY (`escort_id`,`keyword_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci
	';
}

function getEscortProductsTableDump() {
    return '
		CREATE TABLE `escort_products` (
		  `escort_id` int(10) unsigned NOT NULL,
		  `product_id` int(10) unsigned NOT NULL,
		  PRIMARY KEY (`escort_id`,`product_id`),
		  KEY `escort_id` (`escort_id`),
		  KEY `product_id` (`product_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	';
}

function getPremiumCitiesTableDump() {
    return '
		CREATE TABLE `premium_cities` (
		  `escort_id` int(10) unsigned NOT NULL,
		  `city_id` int(10) unsigned NOT NULL,
		  /*PRIMARY KEY (`escort_id`,`city_id`),*/
		  KEY `escort_id` (`escort_id`),
		  KEY `city_id` (`city_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	';
}

function getCommentsTableDump() {
    return '
		CREATE TABLE `comments` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`user_id` int(11) NOT NULL,
		`user_type`  enum("member","escort","agency") CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT "member" ,
		`escort_id` int(11) NOT NULL,
		`time` datetime NOT NULL,
		`is_reply_to` int(11) NOT NULL,
		`status` tinyint(4) DEFAULT \'1\',
		`message` text NOT NULL,
		`thumbup_count` int(11) NOT NULL,
		`thumbdown_count` int(11) NOT NULL,
		PRIMARY KEY (`id`),
		KEY `user_id` (`user_id`),
		KEY `escort_id` (`escort_id`)
		) ENGINE=InnoDB AUTO_INCREMENT=275 DEFAULT CHARSET=utf8;
	';
}

function getEscortFilterDataTableDump($use_metric_system) {
    global $db;

    $DEFINITIONS = Zend_Registry::get('defines');
    $filter = require('Cubix/filter-v2.php');

    if (Cubix_Application::getId() == APP_ED) {
        unset($filter['available-for-incall']);
        unset($filter['available-for-outcall']);
    }

    $nationalities = $db->fetchAll('SELECT * FROM nationalities');

    $nationality_options = array();
    foreach ($nationalities as $n) {
        $nationality_options[] = array(
            //'title' => $n['title_' . Cubix_I18n::getLang()],
            'value' => $n['id']
        );
    }

    $filter['nationality'] = $nationality_options;

    if ($use_metric_system) {
        $filter['height'] = $filter['m_height'];
        unset($filter['m_height']);
        $filter['weight'] = $filter['m_weight'];
        unset($filter['m_weight']);
    }
    $fields = array('`escort_id` int(10) /*NOT NULL*/');
    foreach ($filter as $key => $group) {
        $key = preg_replace('#-#', '_', $key);
        foreach ($group as $grp) {
            $index = strtolower(preg_replace('#-#', '_', $grp['value']));
            $fields[] = '`' . $key . '_' . $index . '`' . ' tinyint(1) NOT NULL DEFAULT \'0\' ';
        }
    }

    $dump_sql = '
		CREATE TABLE `escort_filter_data` (' . implode(',', $fields) . ',
			KEY `escort_id` (`escort_id`)
			
		)ENGINE=InnoDB DEFAULT CHARSET=utf8;';;

    return $dump_sql;
}

function getCommentsV2($start = null, $limit = 0) {
    global $db, $app;

    $limit = intval($limit);

    $fields = "";
    if (in_array($app->id, array(APP_6A, APP_EF))) {
        $fields = " m.rating AS member_rating, c.disable_type, ";
    }

    $sql = '
		SELECT
			c.id, c.user_id, c.escort_id, c.time, c.is_reply_to, c.status, c.message, c.thumbup_count,
			ep.ext AS photo_ext, ep.hash AS photo_hash, e.showname, e.disabled_comments,
			' . $fields . ' c.thumbdown_count, IF(m.is_premium, 1, 0) AS is_premium, u.username, m.comments_count,
			c.user_type, e.city_id AS e_city_id, e.country_id AS e_country_id, e.gender
		FROM comments c
		INNER JOIN escorts e ON e.id = c.escort_id
		INNER JOIN escort_photos ep ON ep.escort_id = e.id AND ep.is_main = 1
		INNER JOIN users u ON u.id = c.user_id
		LEFT JOIN members m ON m.user_id = c.user_id
		WHERE
			u.application_id = ? AND (e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ') AND u.status = ' . USER_STATUS_ACTIVE . '
			AND (c.status = 1 OR (c.status = -4 AND (c.disable_type = 4 OR c.disable_type = 5)))
		GROUP BY c.id
		' . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') . '
	';

    try {
        $result = $db->query($sql, array($app->id))->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return array('result' => $result, 'count' => 0);
}

function getComments() {
    global $config, $db, $app;

    $sql = '
		SELECT c.*
		FROM comments c
		INNER JOIN escorts e ON e.id = c.escort_id
	';

    try {
        $result = $db->query($sql)->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return $result;
}

function getAgencyComments($start = null, $limit = 0) {
    global $db, $app;

    $limit = intval($limit);

    try {
        $comments = $db->query("
			SELECT ac.*, u.username FROM agency_comments ac
			INNER JOIN users u ON u.id = ac.user_id
			WHERE ac.is_reply_to = 0 AND ac.status = ?
			ORDER BY ac.time DESC
			" . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : ''),
            array(AGENCY_COMMENT_STATUS_ACTIVE))->fetchAll();

        foreach ($comments as $i => $comment) {
            $comments[$i]->replied_comment = $db->query("
				SELECT ac.*, u.username FROM agency_comments ac
				INNER JOIN users u ON u.id = ac.user_id
				WHERE ac.status = ? AND ac.is_reply_to = ? 
			", array(AGENCY_COMMENT_STATUS_ACTIVE, $comment->id))->fetchAll();
        }

        $countSql = "
			SELECT COUNT(ac.id)
			FROM agency_comments ac
			INNER JOIN users u ON u.id = ac.user_id
			WHERE ac.status = ? AND ac.is_reply_to = 0
		";

        $count = $db->fetchOne($countSql, array(AGENCY_COMMENT_STATUS_ACTIVE));
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return array('result' => $comments, 'count' => $count);
}

//

/*define('ESCORT_STATUS_NO_PROFILE', 			1);		// 000000001
define('ESCORT_STATUS_NO_ENOUGH_PHOTOS', 	2);		// 000000010
define('ESCORT_STATUS_NOT_APPROVED', 		4);		// 000000100
define('ESCORT_STATUS_OWNER_DISABLED', 		8);		// 000001000
define('ESCORT_STATUS_ADMIN_DISABLED', 		16);	// 000010000
define('ESCORT_STATUS_ACTIVE', 				32);	// 000100000
define('ESCORT_STATUS_IS_NEW',				64);	// 001000000
define('ESCORT_STATUS_PROFILE_CHANGED',		128);	// 010000000*/

define('PACKAGE_STATUS_ACTIVE', 2);
define('PRODUCT_CITY_PREMIUM_SPOT', 5);

function getEscorts($start = null, $limit = 0) {
    global $config, $db, $app;

    $limit = intval($limit);

    if ($app->id == APP_EF) {
        $is_new = ' IF ((e.date_registered > (CURDATE() - INTERVAL ' . $config->newIntervalInDays . ' DAY)) AND (e.mark_not_new = 0), 1, 0) AS is_new, ';
    }
    else {
        $is_new = ' IF (e.date_registered > (CURDATE() - INTERVAL ' . $config->newIntervalInDays . ' DAY), 1, 0) AS is_new, ';
    }

    $sql = '
		SELECT
			e.id,
			e.agency_id,
			e.showname,
			e.gender,
			e.country_id,
			e.city_id,
			e.cityzone_id,
			e.verified_status,
			ep.ethnicity,
			ep.nationality_id,
			FLOOR(DATEDIFF(CURDATE(), ep.birth_date) / 365.25) AS age,
			ep.eye_color,
			ep.hair_color,
			ep.hair_length,
			ep.height,
			ep.weight,
			ep.breast_size,
			ep.dress_size,
			ep.shoe_size,
			ep.is_smoker,
			ep.availability,
			ep.sex_availability,
			/* lang */
			/* rates */
			ep.svc_kissing,
			ep.svc_blowjob,
			ep.svc_69,
			ep.svc_anal,
			ep.svc_cumshot,
			(
				(
					ewt.day_index = ' . date('N') . '
					AND ewt.time_from <= ' . date('G') . '
					AND ewt.time_to >= ' . date('G') . '
				)
				OR
				(
					ewt.day_index = ' . date('N') . '
					AND ewt.time_from <= ' . date('G') . '
					AND ewt.time_from > ewt.time_to
				)
				OR
				(
					ewt.day_index = ' . ((1 == date('N')) ? 7 : date('N') - 1) . '
					AND ewt.time_to >= ' . date('G') . '
					AND ewt.time_from  > ewt.time_to
				)
			) AS is_now_open,
			IF ( NOT t.id IS NULL, 1, 0 ) AS is_on_tour,
			IF ( NOT t.id IS NULL, t.id, NULL ) AS tour_id,
			IF ( NOT t.id IS NULL, t.city_id, NULL ) AS tour_city_id,

			' . $is_new . '

			e.date_registered,
			e.date_last_modified,

			eph.hash AS photo_hash,
			eph.ext AS photo_ext,
			eph.status AS photo_status,

			t.date_from AS tour_date_from,
			t.date_to AS tour_date_to,
			ep.vac_date_from,
			ep.vac_date_to,
			(
				SELECT
					SUM(count)
				FROM escort_hits_daily
				WHERE escort_id = e.id AND DATE_ADD(date, INTERVAL 90 DAY) >= DATE(NOW())
			) AS hit_count
		FROM escort_profiles ep

		INNER JOIN escorts e ON ep.escort_id = e.id
		INNER JOIN users u ON u.id = e.user_id
		INNER JOIN applications a ON a.id = u.application_id

		LEFT JOIN escort_working_times ewt ON ewt.escort_id = e.id
		LEFT JOIN escort_langs el ON el.escort_id = e.id
		INNER JOIN escort_cities ec ON ec.escort_id = e.id

		INNER JOIN countries c ON c.id = e.country_id
		INNER JOIN cities cc ON cc.id = ec.city_id
		LEFT JOIN regions r ON r.id = cc.region_id
		LEFT JOIN cityzones cz ON cz.id = e.cityzone_id

		INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1

		LEFT JOIN tours t ON t.escort_id = e.id AND DATE(t.date_from) <= CURDATE() AND DATE(t.date_to) > CURDATE()

		/*LEFT JOIN escort_hits_daily ehd ON ehd.escort_id = e.id AND DATE_ADD(ehd.date, INTERVAL 90 DAY) >= DATE(NOW())*/

		WHERE
			u.application_id = ? AND
			u.status = ? AND
			(e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ')
			/*' . ($app->id == 9 ? 'AND (SELECT COUNT(*) FROM escort_photos WHERE escort_id = e.id AND type = 2) > 0' : '') . '*/
		GROUP BY e.id
		' . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') . '
	';

    $countSql = '
		SELECT
			COUNT(DISTINCT(e.id)) AS count
		FROM escorts e

		INNER JOIN escort_profiles ep ON ep.escort_id = e.id
		INNER JOIN users u ON u.id = e.user_id
		INNER JOIN applications a ON a.id = u.application_id

		LEFT JOIN escort_working_times ewt ON ewt.escort_id = e.id
		LEFT JOIN escort_langs el ON el.escort_id = e.id

		INNER JOIN countries c ON c.id = e.country_id
		INNER JOIN cities cc ON cc.id = e.city_id
		LEFT JOIN regions r ON r.id = cc.region_id
		LEFT JOIN cityzones cz ON cz.id = e.cityzone_id

		INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1

		WHERE
			u.application_id = ? AND
			u.status = ? AND
			(e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ')
	';

    try {
        $result = $db->query($sql, array($app->id, STATUS_ACTIVE))->fetchAll();

        $active_package_products = array();
        $premium_cities = array();
        foreach ($result as $i => $row) {

            $result[$i]['ordering'] = rand(10000, 50000);

            /* >>> Set the main image the first soft pic */
            /*if ( $app->id == 9 ) {
				$is_main_soft = $db->fetchOne('SELECT TRUE FROM escort_photos WHERE escort_id = ? AND type = 2 AND is_main = 1', array($row['id']));
				if ( ! $is_main_soft ) {
					$photo = $db->fetchRow('SELECT hash AS photo_hash, ext AS photo_ext, status AS photo_status FROM escort_photos WHERE escort_id = ? AND type = 2', array($row['id']));
					$result[$i] = array_merge($result[$i], $photo);
				}
			}*/
            /* <<< */

            $result[$i]['rates'] = serialize($db->query('SELECT * FROM escort_rates WHERE escort_id = ?', $row['id'])->fetchAll());

            $langs = array();
            foreach ($db->query('SELECT * FROM escort_langs WHERE escort_id = ?', $row['id'])->fetchAll() as $lang) {
                $langs[] = $lang['lang_id'];
            }

            $result[$i]['languages'] = implode(',', $langs);

            $products = $db->query('
				SELECT
					op.id AS order_package_id,
					op.escort_id,
					opp.product_id
				FROM
					order_package_products opp
				INNER JOIN order_packages op ON op.id = opp.order_package_id
				WHERE op.escort_id = ? AND op.application_id = ? AND op.status = ?
			', array($row['id'], $app->id, PACKAGE_STATUS_ACTIVE))->fetchAll();

            if (!count($products)) {
                $products = $db->query('
					SELECT
						op.id AS order_package_id,
						op.escort_id,
						pp.product_id
					FROM order_packages op
					INNER JOIN package_products pp ON pp.package_id = op.package_id
					WHERE op.escort_id = ? AND op.application_id = ? AND op.status = ? AND pp.is_optional = 0
				', array($row['id'], $app->id, PACKAGE_STATUS_ACTIVE))->fetchAll();
            }

            if (count($products)) {
                foreach ($products as $k => $product) {
                    if ($product['product_id'] == PRODUCT_CITY_PREMIUM_SPOT) {
                        $premium_cities[] = $db->query('
							SELECT
								op.escort_id,
								pe.city_id
							FROM premium_escorts pe
							INNER JOIN order_packages op ON op.id = pe.order_package_id
							WHERE pe.order_package_id = ?
						', $product['order_package_id'])->fetchAll();
                    }
                    unset($product['order_package_id']);
                    $products[$k] = $product;
                }

                $active_package_products[] = $products;
            }
        }

        $count = $db->fetchOne($countSql, array($app->id, STATUS_ACTIVE));
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return array('result' => $result, 'count' => $count, 'active_package_products' => $active_package_products, 'premium_cities' => $premium_cities);
}

function getEscortsV2($start = null, $limit = 0) {
    global $config, $db, $app;

    $limit = intval($limit);

    $app_id = intval($app->id);

    $wh = " e.country_id = a.country_id AND ";
    if (in_array($app_id, array(APP_ED, 11, 33, 30, 22))) {
        $wh = " ";
    }

    if (in_array($app_id, array(APP_6B, APP_6C, APP_EG, APP_EG_CO_UK, APP_ED))) {
        $wh .= " (e.activation_date IS NULL OR DATE(e.activation_date) <= CURDATE()) AND ";
    }

    $flds = " gotm.month AS gotm_month,
			gotm.year AS gotm_year, ";

    if ($app_id == 5 || $app_id == 7 || $app_id == 9) {
        $flds = " ";
    }
    $about_soft = "";
    $and6_fields = "";
    $hits_count = "
			(
				SELECT
					SUM(count)
				FROM escort_hits_daily
				WHERE escort_id = e.id AND date >= DATE_ADD(DATE(NOW()), INTERVAL -30 DAY)
			) AS hit_count,";
    if ($app->id == APP_A6) {

        $and6_fields = "
			, ag.city_id AS a_city_id, ag.fake_city_id AS a_fake_city_id,
			ag.zip AS a_zip, ag.address AS a_address, ag.name AS a_name,
			ag.slug AS a_slug, e.fake_city_id AS f_city_id, e.fake_zip AS f_zip,
			ag.is_anonymous AS a_is_anonymous,
			ag.email AS a_email, ag.phone AS a_phone, ag.phone_instructions AS a_phone_instructions,
			ag.phone_country_id AS a_phone_country_id
		";

        $hits_count = "
			(
				SELECT
					SUM(count)
				FROM escort_hits_daily
				WHERE escort_id = e.id
			) AS hit_count,";
    }
    if ($app->id == 1 || $app->id == 2) {
        $about_soft = "
			ep.about_soft_en,
			ep.about_soft_it,
			ep.about_soft_gr,
			ep.about_soft_fr,
			ep.about_soft_de,
			ep.about_soft_pt,
			ep.about_soft_nl,
			ep.about_soft_my,
			ep.about_soft_cn,
			e.gallery_is_checked,

		";
    }

    $is_vip = 'e.is_vip,';

    if ($app->id == APP_ED) {
        $is_vip = '
			IF((SELECT TRUE FROM order_packages op
			INNER JOIN order_package_products opp ON op.id = opp.order_package_id
			WHERE op.order_id IS NOT NULL AND op.`status` = 2 AND op.escort_id = e.id AND opp.product_id = 17), 1, 0) AS is_vip,
		';

        $wh .= ' (ag.status = ' . AGENCY_STATUS_ONLINE . ' OR ag.status IS NULL ) AND ';
    }

    $joins = " ";
    if ($app->id == APP_ED) {
        $flds .= "
			vr.date AS date_last_verified,
			ep.email,
  			ep.website,
  			ep.phone_instr,
			ep.phone_instr_no_withheld,
			ep.phone_instr_other,
			ep.phone_country_id,
			ep.disable_phone_prefix,
			ep.phone_number,
			ep.contact_phone_free AS phone_number_free,
			e.status_active_date,
			e.last_hand_verification_date,
			IF ( NOT e.agency_id IS NULL, 1, 0 ) AS is_agency,
			ep.tatoo,
			ep.piercing,
			ep.wechat,
			ep.telegram,
			ep.ssignal,
		";
        $joins = " LEFT JOIN verify_requests vr ON vr.escort_id = e.id AND vr.`status` = 2 AND vr.type = 2 AND vr.id = (SELECT MAX(id) FROM verify_requests WHERE escort_id = vr.escort_id) ";
    }

    if ($app->id == APP_EG_CO_UK) {
        $flds .= "
			e.last_hand_verification_date,
			ep.telegram,
			ep.ssignal,
		";
    }

    if (in_array($app->id,array(APP_AE,APP_EM))) {
        $flds .= "
			ep.telegram,
			ep.ssignal,
		";
    }

    if ($app->id == APP_EG) {
        $flds .= "
			e.last_hand_verification_date,
		";
    }
    if ($app->id == APP_6C) {
        $flds .= "
			e.last_hand_verification_date,
		";
    }

    if ($app->id == APP_EF ) {
        $flds .= "
			e.skype_call_status, e.cam_status, e.cam_preview_url, e.cam_channel_url,
		";
    }

    if ( $app->id == APP_A6 || $app->id == APP_BL) {
        $flds .= "
			e.cam_booking_status, e.cam_status, e.cam_preview_url, e.cam_channel_url,
		";
    }

    if ($app->id == APP_6B) {
        $flds .= "
			e.last_hand_verification_date,
			ep.telegram,
			ep.ssignal,
		";
    }

    if (in_array($app->id, array(APP_EF, APP_ED, APP_A6, APP_6B, APP_EG_CO_UK, APP_BL))) {
        $flds .= "IF((SELECT TRUE FROM video v 
				WHERE v.escort_id = e.id AND v.status = 'approve' LIMIT 1 ), 1, 0) AS has_video,
			";
    }

    if ($app->id == APP_EF || $app->id == APP_ED) {
        $flds .= "
			ep.phone_instr,
		";
        $is_new = ' IF ((e.date_registered > (CURDATE() - INTERVAL ' . $config->newIntervalInDays . ' DAY)) AND (e.mark_not_new = 0), 1, 0) AS is_new, ';
    }
    else {
        $is_new = ' IF (e.date_registered > (CURDATE() - INTERVAL ' . $config->newIntervalInDays . ' DAY), 1, 0) AS is_new, ';
    }

    if ($app->id == APP_BL) {
        $flds .= "
				e.is_pornstar,
				ep.telegram,
			    ep.ssignal,
			";
    }

    if ($app->id == APP_EI) {
        $flds .= "
  			ep.phone_instr,
			ep.phone_instr_no_withheld,
			ep.phone_instr_other,
			ep.phone_country_id,
			ep.disable_phone_prefix,
			ep.phone_number,
		";
    }


    $sql_active_package = addActivePackageWhereParams();

    $aboutAPP_A6 = '';
    if ($app->id == APP_A6) {
        $aboutAPP_A6 = ' ep.about_es,ep.about_hu, ';
    }

    if ($app->id == APP_BL) {
        $flds .= "
  			ep.snapchat,
			ep.snapchat_username,
		";
    }


    $fields_6nun = '';
    if ($app->id == APP_6B) {
        $fields_6nun .= "
			, ep.twitter_name
		";
    }

    $escortStatuses = ' e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ' AND NOT e.status & ' . ESCORT_STATUS_NO_ENOUGH_PHOTOS . ' AND NOT e.status & ' . ESCORT_STATUS_NEED_AGE_VERIFICATION;
    if (in_array($app_id, array(APP_EG_CO_UK, APP_ED, /*APP_EF, APP_AE, APP_BL, APP_6B*/))) {
        $escortStatuses .= ' AND NOT e.status & ' . ESCORT_STATUS_OFFLINE;
    }
    elseif (in_array($app_id, array(APP_EF))) {
        $escortStatuses = ' e.status IN (32,160) ';
    }
	if (in_array($app_id, array(APP_ED))) {
        $escortStatuses .= ' AND NOT e.status & '. ESCORT_STATUS_AWAITING_PAYMENT;
    }
    if (in_array($app_id, array(APP_ED, APP_EG_CO_UK))) {
        $flds .= " IF(e.status & " . ESCORT_STATUS_INACTIVE . ", 1, 0) as is_inactive, ";
    }
    $sql = '
		SELECT SQL_CALC_FOUND_ROWS
			e.id,
			u.id AS user_id,
			e.agency_id,
			e.showname,
			u.username,
			es.text AS slogan,
			e.gender,
			e.country_id,
			e.city_id,
			e.cityzone_id,
			e.verified_status,
			e.type,
			e.master_id,

			e.is_suspicious,
			e.suspicious_comment,
			e.susp_comment_show,

			0 AS review_count,
			0 AS comment_count,
			e.block_website,
			e.check_website,
			e.is_pornstar,
			e.bad_photo_history,
			ep.ethnicity,
			ep.nationality_id,
			/*FLOOR(DATEDIFF(CURDATE(), ep.birth_date) / 365.25) AS age,*/
			/*DATEDIFF(CURDATE(), ep.birth_date) / 365.25 AS age,*/
			ep.birth_date,
			ep.eye_color,
			ep.hair_color,
			ep.hair_length,
			ep.height,
			ep.weight,
			ep.cup_size,
			ep.breast_type,
			ep.dress_size,
			ep.shoe_size,
			ep.pubic_hair,
			ep.is_smoking,
			ep.is_drinking,

			ep.cuisine,
			ep.drink,
			ep.flower,
			ep.gifts,
			ep.perfume,
			ep.designer,
			ep.interests,
			ep.sports,
			ep.hobbies,

			ep.min_book_time,
			ep.min_book_time_unit,

			ep.has_down_pay,
			ep.down_pay,
			ep.down_pay_cur,

			ep.reservation,

			ep.zip,
			ep.incall_type,
			ep.incall_hotel_room,
			ep.incall_other,
			ep.outcall_type,
			ep.outcall_other,

			ep.sex_orientation,
			ep.sex_availability,
			/* lang */
			/* rates */
			e.is_open AS is_now_open,
			IF ( NOT t.id IS NULL, 1, 0 ) AS is_on_tour,
			IF ( NOT t.id IS NULL, t.id, NULL ) AS tour_id,
			IF ( NOT t.id IS NULL, t.city_id, NULL ) AS tour_city_id,

			' . $is_new . '

			e.date_registered,
			e.date_last_modified,
			u.last_login_date,
			eph.hash AS photo_hash,
			eph.ext AS photo_ext,
			eph.status AS photo_status,
			eph.args AS photo_args,

			t.date_from AS tour_date_from,
			t.date_to AS tour_date_to,
			t.minutes_from,
			t.minutes_to,
			t.phone AS tour_phone,
			e.vac_date_from,
			e.vac_date_to,
			' . $hits_count . '
			ep.available_24_7,
			ep.night_escort,

			ag.name AS agency_name,
			ag.slug AS agency_slug,
			eb.text AS bubble_text,
			e.votes_sum,
			e.votes_count,
			e.voted_members,
			' . $flds . '
			/*null AS gotm_month,
			null AS gotm_year,*/
			e.fuckometer,
			e.fuckometer_rank,
			e.show_popunder,

			IF(e.hh_status = ' . HH_STATUS_ACTIVE . ' AND e.has_hh = 1, 1, 0) AS hh_is_active,
			e.hh_date_from,
			e.hh_date_to,
			e.hh_motto,

			ep.about_en,
			ep.about_it,
			ep.about_gr,
			ep.about_fr,
			ep.about_de,
			ep.about_pt,
			ep.about_nl,
			ep.about_my,
			ep.about_cn,
			' . $aboutAPP_A6 . '
			' . $about_soft . '
			' . $is_vip . '
			IF (ag.id IS NULL OR ag.disabled_reviews = 0, e.disabled_reviews, ag.disabled_reviews) AS disabled_reviews,
			IF (ag.id IS NULL OR ag.disabled_comments = 0, e.disabled_comments, ag.disabled_comments) AS disabled_comments,
			ep.travel_place,
			ag.block_website AS a_block_website,
			ag.web AS a_web,
			e.latitude,
			e.longitude,
			u.allow_show_online,
			ep.phone_additional_1,
			ep.phone_additional_2,
			ep.phone_country_id_1,
			ep.phone_country_id_2,
			ep.disable_phone_prefix_1,
			ep.disable_phone_prefix_2,
			ep.phone_instr_1,
			ep.phone_instr_2,
			ep.phone_instr_no_withheld_1,
			ep.phone_instr_no_withheld_2,
			ep.phone_instr_other_1,
			ep.phone_instr_other_2,
			ep.viber,
			ep.whatsapp,
			ep.viber_1,
			ep.whatsapp_1,
			ep.viber_2,
			ep.whatsapp_2,
			ep.contact_phone_parsed
			' . $and6_fields . '
			' . $fields_6nun . '
		FROM escort_profiles_v2 ep

		INNER JOIN escorts e ON ep.escort_id = e.id
		INNER JOIN users u ON u.id = e.user_id
		INNER JOIN applications a ON a.id = u.application_id

		LEFT JOIN girl_of_month gotm ON gotm.escort_id = e.id AND gotm.was_gotm = 0
		LEFT JOIN agencies ag ON ag.id = e.agency_id
		LEFT JOIN escort_bubbletexts eb ON eb.escort_id = e.id AND eb.status = 1 AND eb.approvation = 1/* AND e.gender <> 2*/
		LEFT JOIN escort_slogans es ON es.escort_id = e.id AND es.status = 1

		/*LEFT JOIN escort_working_times ewt ON ewt.escort_id = e.id
		LEFT JOIN escort_langs el ON el.escort_id = e.id*/
		INNER JOIN escort_cities ec ON ec.escort_id = e.id

		INNER JOIN countries c ON c.id = e.country_id
		INNER JOIN cities cc ON cc.id = ec.city_id
		LEFT JOIN regions r ON r.id = cc.region_id
		LEFT JOIN cityzones cz ON cz.id = e.cityzone_id

		INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1

		LEFT JOIN tours t ON t.escort_id = e.id AND DATE(t.date_from) <= CURDATE() AND DATE(t.date_to) >= CURDATE()
		' . $sql_active_package['join'] . '
		' . $joins . '

		/*LEFT JOIN escort_hits_daily ehd ON ehd.escort_id = e.id AND DATE_ADD(ehd.date, INTERVAL 90 DAY) >= DATE(NOW())*/

		WHERE
			u.application_id = ' . intval($app->id) . ' AND
			u.status = ? AND
			' . $wh . '
			(' . $escortStatuses . ')
			' . $sql_active_package['where'] . '
		GROUP BY e.id
		' . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') . '
	';

    try {
        //return $sql;
        $result = $db->query($sql, array(STATUS_ACTIVE))->fetchAll();

        $active_package_products = array();
        $premium_cities = array();
        $invalids = array();

        /*$bl_countries = $db->query('SELECT id FROM countries')->fetchAll();
		$bl_c_i = 0;
		foreach( $bl_countries as $k => $id ) {
			if ( $k % 63 == 0 ) {
				$bl_c_i++;
			}
		}*/

        foreach ($result as $i => $row) {
            $result[$i]['ordering'] = rand(10000, 50000);
            $result[$i]['is_new'] = $result[$i]['is_new'] == 1 ? 1 : 0;
            $result[$i]['date_activated'] = null;

            $new_flag_projects = array(APP_6A, APP_EF, APP_A6, APP_A6_AT);

            if (in_array($app->id, $new_flag_projects)) {

                $package = $db->fetchRow('
					SELECT
						COUNT(op.id) AS count
					FROM order_packages op
					WHERE op.escort_id = ? AND op.application_id = ? AND op.order_id IS NOT NULL AND op.status <> 4
				', array($row['id'], $app->id));

                $first_package = true;
                if ($package['count'] > 1) {
                    $first_package = false;
                }

                $inactive_days_60 = false;
                $package = $db->fetchRow('
					SELECT
						DATEDIFF(CURDATE(), op.expiration_date) AS inactive_days
					FROM order_packages op
					WHERE op.escort_id = ? AND op.application_id = ? AND op.order_id IS NOT NULL AND op.status = 3 AND op.expiration_date IS NOT NULL
					ORDER BY op.expiration_date DESC
					LIMIT 1
				', array($row['id'], $app->id));

                if ($package['inactive_days'] >= 60) {
                    $inactive_days_60 = true;
                }

                $date_field = 'op.date_activated';

                $is_moved_package = $db->fetchRow('
					SELECT
						IF (op.date_activated < e.date_registered, 1, 0) AS is_moved_package
					FROM order_packages op
					INNER JOIN escorts e ON e.id = op.escort_id
					WHERE op.escort_id = ? AND op.application_id = ? AND op.status = ?
				', array($row['id'], $app->id, PACKAGE_STATUS_ACTIVE));

                if ($is_moved_package['is_moved_package']) {
                    $date_field = 'e.date_registered';
                }

                if ($app->id == APP_EF) {
                    $is_newSql = ' IF ((' . $date_field . ' > (CURDATE() - INTERVAL ' . $config->newIntervalInDays . ' DAY )) AND (e.mark_not_new = 0), 1, 0) AS is_new ';
                }
                else {
                    $is_newSql = ' IF (' . $date_field . ' > (CURDATE() - INTERVAL ' . $config->newIntervalInDays . ' DAY ), 1, 0) AS is_new ';
                }

                $is_new_by_package = $db->fetchRow('
					SELECT
						op.date_activated, ' . $is_newSql . ' 
					FROM order_packages op
					INNER JOIN escorts e ON e.id = op.escort_id
					WHERE op.escort_id = ? AND op.application_id = ? AND op.status = ?
				', array($row['id'], $app->id, PACKAGE_STATUS_ACTIVE));

                if ($is_new_by_package) {
                    $result[$i]['date_activated'] = $is_new_by_package['date_activated'];
                }

                if ($first_package || $inactive_days_60) {
                    if ($is_new_by_package) {
                        $result[$i]['is_new'] = $is_new_by_package['is_new'];
                    }
                }

                if ($app->id == APP_EF)
                {
                    $has_moved_package = $db->fetchOne('
                    SELECT
                        true
                    FROM
                        packages_moving_history pmh
					INNER JOIN order_packages op ON op.id = pmh.order_package_id 	
                    WHERE
						( pmh.from_escort_id = ? OR ( pmh.to_escort_id = ? AND op.status <> ?) ) 
                        AND pmh.date > ( CURDATE() - INTERVAL 60 DAY ) 
				    ', array($row['id'],$row['id'], PACKAGE_STATUS_ACTIVE));

                    if ($has_moved_package)
                        $result[$i]['is_new'] = 0;

                }

                /*if ( $is_new_by_package ) {
					$result[$i]['date_activated'] = $is_new_by_package['date_activated'];
				}

				if ( $is_new_by_package && ( $first_package || $inactive_days_60 ) ) {
					$result[$i]['is_new'] = 1;
				}*/

            }
            elseif (in_array(Cubix_Application::getId(), array(APP_BL))) {

                if (Cubix_Application::getId() == APP_ED) {
                    $is_new_sql = ' IF ((op.date_activated > (CURDATE() - INTERVAL ' . $config->newIntervalInDays . ' DAY)) AND (e.mark_not_new = 0), 1, 0) AS is_new ';
                }
                else {
                    $is_new_sql = ' IF (op.date_activated > (CURDATE() - INTERVAL ' . $config->newIntervalInDays . ' DAY), 1, 0) AS is_new ';
                }

                $is_new_by_package = $db->fetchRow('
					SELECT
						op.date_activated, ' . $is_new_sql . '
					FROM order_packages op
					INNER JOIN escorts e ON e.id = op.escort_id
					WHERE op.escort_id = ? AND op.application_id = ? AND op.status = ? AND op.order_id IS NOT NULL
				', array($row['id'], $app->id, PACKAGE_STATUS_ACTIVE));
                if ($is_new_by_package) {
                    $result[$i]['is_new'] = $is_new_by_package['is_new'];
                    $result[$i]['date_activated'] = $is_new_by_package['date_activated'];
                }
            }

            if ($app->id == APP_A6 || $app->id == APP_A6_AT) {
                $b_photos = $db->fetchAll('SELECT width, height FROM escort_photos WHERE escort_id = ?', array($row['id']));

                $has_big_photos = 1;
                $min_sidesize = 600;

                if (count($b_photos)) {
                    foreach ($b_photos AS $ph) {
                        if ($ph['width'] < $min_sidesize || $ph['height'] < $min_sidesize) {
                            $has_big_photos = 0;
                            break;
                        }
                    }
                }

                $result[$i]['has_big_photos'] = $has_big_photos;
            }

            /* >>> Set the main image the first soft pic */
            /*if ( $app->id == 9 ) {
				$is_main_soft = $db->fetchOne('SELECT TRUE FROM escort_photos WHERE escort_id = ? AND type = 2 AND is_main = 1', array($row['id']));
				if ( ! $is_main_soft ) {
					$photo = $db->fetchRow('SELECT hash AS photo_hash, ext AS photo_ext, status AS photo_status FROM escort_photos WHERE escort_id = ? AND type = 2', array($row['id']));
					$result[$i] = array_merge($result[$i], $photo);
				}
			}*/
            /* <<< */

            /************* VIP Request ****************/
            if ($app->id == APP_6A) {
                $vip_verified = $db->fetchOne('SELECT TRUE FROM vip_requests WHERE escort_id = ? AND status = 2	ORDER BY id DESC LIMIT 1', $row['id']);

                if ($vip_verified)
                    $result[$i]['vip_verified'] = 1;
                else
                    $result[$i]['vip_verified'] = 0;
            }
            /******************************************/

            //$result[$i]['close_time'] = $db->fetchOne('SELECT time_to FROM escort_working_times WHERE escort_id = ? AND day_index = ?', array($row['id'], date('N')));
            $t = $db->fetchOne('SELECT time_to FROM escort_working_times WHERE escort_id = ? AND day_index = ?', array($row['id'], date('N')));
            if (strlen($t) > 0)
                $result[$i]['close_time'] = $t;
            else
                $result[$i]['close_time'] = null;

            $travel_payment_methods = $db->query('SELECT payment_method, specify FROM travel_payment_methods WHERE escort_id = ?', $row['id'])->fetchAll();
            $rates = $db->query('SELECT * FROM escort_rates WHERE escort_id = ? AND availability <> 0', $row['id'])->fetchAll();
            $rates['travel_rates'] = $db->query('SELECT * FROM escort_rates WHERE escort_id = ? AND availability = 0', $row['id'])->fetchAll();
            $result[$i]['rates'] = serialize($rates);
            $result[$i]['travel_payment_methods'] = serialize($travel_payment_methods);

            $langs = array();
            foreach ($db->query('SELECT * FROM escort_langs WHERE escort_id = ?', $row['id'])->fetchAll() as $lang) {
                $langs[] = $lang['lang_id'];
            }

            $result[$i]['languages'] = implode(',', $langs);

            // Reviews count
            $r_c = $db->fetchOne('
				SELECT COUNT(r.id)
				FROM reviews r
				INNER JOIN users u ON u.id = r.user_id
				WHERE r.escort_id = ? AND r.status = 2 AND is_deleted = 0 AND u.status = 1
			', $row['id']);
            $result[$i]['review_count'] = $r_c;
            // Reviews count

            // Comments count
            $c_c = $db->fetchOne('
				SELECT COUNT(c.id)
				FROM comments c
				INNER JOIN escorts e ON e.id = c.escort_id
				INNER JOIN escort_photos ep ON ep.escort_id = e.id AND ep.is_main = 1
				INNER JOIN users u ON u.id = c.user_id
				WHERE c.escort_id = ? AND c.status = 1 AND (e.status & ' . ESCORT_STATUS_ACTIVE . '
					AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . '
					AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . '
					AND NOT e.status & ' . ESCORT_STATUS_DELETED . ') AND u.status = ' . USER_STATUS_ACTIVE . '
			', $row['id']);
            $result[$i]['comment_count'] = $c_c;
            // Comments count

            // Age
            $result[$i]['age'] = Cubix_Utils::dateToAge($row['birth_date']);
            unset($row['birth_date']);
            unset($result[$i]['birth_date']);
            // Age

            // Blacklisted countries
            $escort_blocked_countries = $db->fetchAll('
				SELECT country_id
				FROM escort_blocked_countries
				WHERE escort_id = ?
			', array($row['id']));

            $escort_blocked_countries_arr = array();
            foreach ($escort_blocked_countries as $bl_country) {
                $escort_blocked_countries_arr[] = $bl_country['country_id'];
            }

            $result[$i]['blacklisted_countries'] = $escort_blocked_countries_arr;
            // Blacklisted countries


            $products = $db->query('
				SELECT
					op.id AS order_package_id,
					op.escort_id,
					opp.product_id
				FROM
					order_package_products opp
				INNER JOIN order_packages op ON op.id = opp.order_package_id
				WHERE op.escort_id = ? AND op.application_id = ? AND op.status = ?
			', array($row['id'], $app->id, PACKAGE_STATUS_ACTIVE))->fetchAll();

            if (!count($products)) {
                $products = $db->query('
					SELECT
						op.id AS order_package_id,
						op.escort_id,
						pp.product_id
					FROM order_packages op
					INNER JOIN package_products pp ON pp.package_id = op.package_id
					WHERE op.escort_id = ? AND op.application_id = ? AND op.status = ? AND pp.is_optional = 0
				', array($row['id'], $app->id, PACKAGE_STATUS_ACTIVE))->fetchAll();
            }

            if (count($products)) {
                //file_put_contents('sync.log', var_export($products, true), FILE_APPEND);

                foreach ($products as $k => $product) {
                    if ($product['product_id'] == PRODUCT_CITY_PREMIUM_SPOT) {
                        $premium_cities[] = $db->query('
							SELECT
								op.escort_id,
								pe.city_id
							FROM premium_escorts pe
							INNER JOIN order_packages op ON op.id = pe.order_package_id
							WHERE pe.order_package_id = ?
						', $product['order_package_id'])->fetchAll();
                    }

                    unset($product['order_package_id']);
                    $products[$k] = $product;
                }

                $active_package_products[] = $products;
            }
            else {
                $invalids[] = $row['id'];
            }
        }

        $count = $db->fetchOne('SELECT FOUND_ROWS()');
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return array('result' => $result, 'count' => $count, 'active_package_products' => $active_package_products, 'premium_cities' => $premium_cities, 'invalids' => $invalids);
}

function getAdditionalAreas($start = null, $limit = 0) {
    global $db, $app;

    $limit = intval($limit);

    $sql_active_package = addActivePackageWhereParams();

    $sql = '
		SELECT
			aa.order_package_id, aa.escort_id, aa.area_id
		FROM additional_areas aa
		INNER JOIN escorts e ON e.id = aa.escort_id
		INNER JOIN users u ON e.user_id = u.id
		INNER JOIN order_packages op ON op.id = aa.order_package_id
		LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
		WHERE
			u.application_id = ? AND
			u.status = ? AND
			op.status = ' . PACKAGE_STATUS_ACTIVE . ' AND
			(e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ')
			' . $sql_active_package['where'] . '
		' . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') . '
	';

    try {
        $result = $db->query($sql, array($app->id, STATUS_ACTIVE))->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return array('result' => $result, 'count' => 0);
}

function getProducts($start = null, $limit = 0) {
    global $config, $db, $app;

    $limit = intval($limit);

    $join = " INNER JOIN order_package_products opp ON opp.order_package_id = op.id ";
    if ( /*$app->id == 69 ||*/ $app->id == 16 || $app->id == APP_A6_AT || $app->id == 30 || $app->id == 22) {
        $join = " LEFT JOIN package_products opp ON opp.package_id = op.package_id ";
    }

    $field = '';

    if ($app->id == APP_EF) {
        $field = ', e.is_vip ';
    }

    $sql_active_package = addActivePackageWhereParams();

    $sql = '
		SELECT SQL_CALC_FOUND_ROWS
			e.id, opp.product_id, op.package_id, op.gotd_status, op.gotd_city_id ' . $field . '
			/*IF (op.date_activated > (CURDATE() - INTERVAL ' . $config->newIntervalInDays . ' DAY), 1, 0) AS is_new*/
		FROM escorts e

		INNER JOIN order_packages op ON op.escort_id = e.id
		' . $join . '
		INNER JOIN users u ON u.id = e.user_id
		INNER JOIN applications a ON a.id = u.application_id
		INNER JOIN escort_cities ec ON ec.escort_id = e.id
		INNER JOIN countries c ON c.id = e.country_id
		INNER JOIN cities cc ON cc.id = ec.city_id
		INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
		WHERE
			u.application_id = ? AND
			u.status = ? AND
			op.status = ' . PACKAGE_STATUS_ACTIVE . ' AND
			(e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ')
			' . $sql_active_package['where'] . '
		' . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') . '
	';

    try {
        $result = $db->query($sql, array($app->id, STATUS_ACTIVE))->fetchAll();
        $count = $db->fetchOne('SELECT FOUND_ROWS()');
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    /**/

    if ($app->id == APP_BL) {
        if (count($result)) {
            $escs = array();
            $escorts = array();

            foreach ($result as $r) {
                if (!in_array($r['id'], $escs)) {
                    $escs[] = $r['id'];
                    $escorts[] = array('id' => $r['id'], 'package_id' => $r['package_id']);
                }
            }

            //file_put_contents('/tmp/sync.log', var_export($escorts, true), FILE_APPEND);

            foreach ($escorts as $esc) {
                $ops = $db->query('
					SELECT status
					FROM order_packages
					WHERE escort_id = ? AND application_id = ? AND order_id IS NOT NULL
				', array($esc['id'], $app->id))->fetchAll();

                if (count($ops)) {
                    $not_display = true;

                    foreach ($ops as $op) {
                        if (in_array($op['status'], array(PACKAGE_STATUS_ACTIVE, 5, 6))) //active, upgraded, suspended
                        {
                            $not_display = false;
                            break;
                        }
                    }

                    if ($not_display) {
                        $dsp = $db->fetchOne('SELECT display_after_package_expire FROM escorts WHERE id = ?', $esc['id']);

                        if (!$dsp)
                            $result[] = array('id' => $esc['id'], 'product_id' => 77, 'package_id' => $esc['package_id'], 'gotd_status' => null, 'gotd_city_id' => null); // 77 - dont_display
                    }
                }
            }
        }
    }
    /**/

    return array('result' => $result, 'count' => $count);
}

function getEscortReviewsV2($start = null, $limit = 0) {
    global $db, $app;

    $limit = intval($limit);

    $field_ef = "";
    if (Cubix_Application::getId() == APP_EF) {
        $field_ef = " r.disable_type, m.rating AS member_rating, r.escort_comment_status,";
    }

    $field_ed = '';
    $join_ed = '';
    $where_ed = '';
    if (Cubix_Application::getId() == APP_ED) {
        $field_ed = '
			(SELECT COUNT(id) FROM reviews WHERE status = 2 AND escort_id = r.escort_id) AS e_count,
			(SELECT COUNT(id) FROM reviews WHERE status = 2 AND user_id = r.user_id) AS m_count,
			cn.id AS country,quality,location_review,country,
		';
        $join_ed = '
			LEFT JOIN cities c ON c.id = r.city
			LEFT JOIN countries cn ON cn.id = c.country_id
		';
        $where_ed = '
			 e.country_id != 68 AND cn.id != 68 AND 
		';
    }

    $sql = '
		SELECT
			r.id, r.user_id, u.username, r.escort_id, r.meeting_date, r.duration, r.duration_unit,
			r.price, r.currency, r.looks_rating, r.services_rating, r.services_comments,
			r.review, r.fuckometer, r.creation_date, r.last_modified, r.city, r.is_fake_free,
			r.city_is_in_escort_profile, r.meeting_place, r.s_kissing, r.s_blowjob, r.s_cumshot,
			r.s_69, r.s_anal, r.s_sex, r.s_attitude, r.s_conversation, r.s_breast, r.s_multiple_sex,
			r.s_availability, r.s_photos, r.t_user_info, r.t_meeting_date, r.t_meeting_time,
			r.t_meeting_duration, r.t_meeting_place, r.t_comments, r.sms_status, r.sms_number,
			r.ip, r.is_suspicious, r.application_id, r.status, r.is_deleted, r.deleted_date,
			r.reason, r.sms_unique, r.escort_comment, m.is_premium, m.reviews_count, u.date_registered,
			e.showname, e.gender, a.name AS agency_name, ' . $field_ed . $field_ef . '
			eph.hash AS photo_hash,	eph.ext AS photo_ext, e.city_id AS e_city_id, e.country_id AS e_country_id, r.disabled_reason,
			e.fuckometer AS e_fuckometer, e.fuckometer_rank AS e_fuckometer_rank/*, op.package_id AS e_package_id*/
		FROM reviews r
		INNER JOIN users u ON r.user_id = u.id
		INNER JOIN members m ON r.user_id = m.user_id
		INNER JOIN escorts e ON e.id = r.escort_id
		/*LEFT JOIN order_packages op ON op.escort_id = e.id AND op.status = 2*/
		LEFT JOIN agencies a ON e.agency_id = a.id
		LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1 ' . $join_ed . '
		WHERE
			 ' . $where_ed . 'r.application_id = ? AND r.is_deleted = 0 AND u.status = ' . USER_STATUS_ACTIVE . '
			/*AND NOT e.status & ' . ESCORT_STATUS_DELETED . '*/
			AND r.status = 2 OR (r.status = 3 AND (r.disable_type = 4 OR r.disable_type = 5)) 
		GROUP BY r.id
		' . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') . '
	';

    try {
        $result = $db->query($sql, array($app->id))->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return array('result' => $result, 'count' => 0);
}

function getEscortImportedReviews($start = null, $limit = 0) {
    global $db, $app;

    $sql = '
			SELECT *
			FROM reviews_imported ir WHERE ir.`status` = 1 '. (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') ;


    try {
        $result = $db->query($sql)->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return array('result' => $result, 'count' => 0);
}

function getEscortWorkingTimesV2($start = null, $limit = 0) {
    global $db, $app;

    $limit = intval($limit);

    $sql_active_package = addActivePackageWhereParams();

    $sql = '
		SELECT
			ewt.*
		FROM escort_working_times ewt
		INNER JOIN escorts e ON e.id = ewt.escort_id
		INNER JOIN users u ON u.id = e.user_id
		' . $sql_active_package['join'] . '
		WHERE
			u.application_id = ?
			AND (e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ')
			' . $sql_active_package['where'] . '
		' . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') . '
	';

    try {
        $result = $db->query($sql, array($app->id))->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return array('result' => $result, 'count' => 0);
}

function getEscortServicesV2() {
    global $config, $db, $app;
    set_time_limit(0);
    ini_set('memory_limit', '1024M');

    $sql_active_package = addActivePackageWhereParams();

    $sql = '
		SELECT
			es.*
		FROM escort_services es
		INNER JOIN escorts e ON e.id = es.escort_id
		INNER JOIN users u ON u.id = e.user_id
		' . $sql_active_package['join'] . '
		WHERE
			u.application_id = ? AND u.status = ? AND e.status & 32
			' . $sql_active_package['where'] . '
	';

    try {
        $result = $db->query($sql, array($app->id, STATUS_ACTIVE))->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return $result;
}

function getEscortKeywords() {
    global $db, $app;
    set_time_limit(0);
    ini_set('memory_limit', '1024M');

    $sql = '
		SELECT
			ek.*
		FROM escort_keywords ek
		INNER JOIN escorts e ON e.id = ek.escort_id
		INNER JOIN users u ON u.id = e.user_id

		WHERE u.application_id = ? AND u.status = ? AND e.status & 32
	';

    try {
        $result = $db->query($sql, array($app->id, STATUS_ACTIVE))->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return $result;
}

function getUpcomingTours() {
    global $config, $db, $app;

    $sql_active_package = addActivePackageWhereParams();

    $sql = '
		SELECT
			e.id,
			IF ( NOT t.id IS NULL, t.id, NULL ) AS tour_id,
			IF ( NOT t.id IS NULL, t.city_id, NULL ) AS tour_city_id,
			t.date_from AS tour_date_from,
			t.date_to AS tour_date_to,
			t.minutes_from, t.minutes_to,
			t.phone
		FROM escorts e
		INNER JOIN users u ON u.id = e.user_id
		' . $sql_active_package['join'] . '
		LEFT JOIN tours t ON t.escort_id = e.id
		INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
		WHERE
			u.application_id = ? AND u.status = ? AND eph.is_main = 1 AND DATE(t.date_from) > DATE(NOW())
			' . $sql_active_package['where'] . '
		ORDER BY e.id, t.date_from ASC
	';

    try {
        $result = $db->query($sql, array($app->id, STATUS_ACTIVE))->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return $result;
}

function getEscortCities() {
    global $config, $db, $app;

    try {

        $sql_active_package = addActivePackageWhereParams();

        $sql = '
			SELECT ec.* FROM escort_cities ec
			INNER JOIN escorts e ON e.id = ec.escort_id
			INNER JOIN users u ON u.id = e.user_id
			' . $sql_active_package['join'] . '
			WHERE
				u.application_id = ? AND u.status = ?
				AND (e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ')
				' . $sql_active_package['where'] . '
		';

        $cities = $db->query($sql, array($app->id, STATUS_ACTIVE))->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return $cities;
}

function getEscortTravelCountries() {
    global $config, $db, $app;

    try {
        $sql = '
			SELECT ec.* FROM escort_travel_countries ec
			INNER JOIN escorts e ON e.id = ec.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE
				u.application_id = ? AND u.status = ? AND (e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ')
				/*' . ($app->id == 9 ? 'AND (SELECT COUNT(*) FROM escort_photos WHERE escort_id = e.id AND type = 2) > 0' : '') . '*/
		';

        $countries = $db->query($sql, array($app->id, STATUS_ACTIVE))->fetchAll();

        $result = $countries;
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return $result;
}

function getEscortTravelContinents() {
    global $config, $db, $app;

    try {
        $sql = '
			SELECT ec.* FROM escort_travel_continents ec
			INNER JOIN escorts e ON e.id = ec.escort_id
			INNER JOIN users u ON u.id = e.user_id
			WHERE
				u.application_id = ? AND u.status = ? AND (e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ')
				/*' . ($app->id == 9 ? 'AND (SELECT COUNT(*) FROM escort_photos WHERE escort_id = e.id AND type = 2) > 0' : '') . '*/
		';

        $cs = $db->query($sql, array($app->id, STATUS_ACTIVE))->fetchAll();

        $result = $cs;
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return $result;
}

///// for BL only /////////
function getExtraCities() {
    global $db, $app;

    try {
        $sql = '
			SELECT p.city_id, p.is_premium, o.escort_id, u.user_type, e.gender
			FROM package_extra_cities p
			INNER JOIN order_packages o ON o.id = p.order_package_id AND o.`status` = 2 AND o.application_id = ?
			INNER JOIN escorts e ON e.id = o.escort_id
			INNER JOIN users u ON u.id = e.user_id
		';

        return $db->query($sql, array($app->id))->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }
}

///////////////////////////

function getEscortCityzones() {
    global $config, $db, $app;

    $sql_active_package = addActivePackageWhereParams();

    $sql = '
		SELECT ec.* FROM escort_cityzones ec
		INNER JOIN escorts e ON e.id = ec.escort_id
		INNER JOIN users u ON u.id = e.user_id
		' . $sql_active_package['join'] . '
		WHERE
			u.application_id = ? AND u.status = ?
			AND (e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ')
			' . $sql_active_package['where'] . '
	';

    try {
        $result = $db->query($sql, array($app->id, STATUS_ACTIVE))->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return $result;
}

function getEscortPhotos($start = null, $limit = 0) {
    global $config, $db, $app;

    $limit = intval($limit);

    $sql_active_package = addActivePackageWhereParams();

    $statusCond = "e.status IN (32, 160)";

    if (in_array($app->id, array(APP_ED, APP_EG_CO_UK))) {
        $statusCond = '(e.status IN (32, 160) OR e.status & ' . ESCORT_STATUS_INACTIVE . ')';
    }

    $sql = '
		SELECT eph.*
		FROM escort_photos eph
		INNER JOIN escorts e ON e.id = eph.escort_id
		INNER JOIN users u ON u.id = e.user_id
		' . $sql_active_package['join'] . '
		WHERE
			u.application_id = ? AND u.status = ? AND eph.is_approved = 1 AND eph.type <> ? AND eph.retouch_status <> 1 
			AND ' . $statusCond . '  
			' . $sql_active_package['where'] . '
			ORDER BY eph.id		
		' . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') . '
	';

    try {
        $result = $db->query($sql, array($app->id, STATUS_ACTIVE, ESCORT_PHOTO_TYPE_DISABLED))->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return array('result' => $result, 'count' => 0);
}

function getEscortPhotoVotes($start = null, $limit = 0) {
    global $config, $db, $app;

    $limit = intval($limit);

    $sql_active_package = addActivePackageWhereParams();

    $sql = '
		SELECT epv.*
		FROM escort_photo_votes epv
		INNER JOIN escorts e ON e.id = epv.escort_id
		' . $sql_active_package['join'] . '
		WHERE
			e.gender = 1 AND e.status & 32
			' . $sql_active_package['where'] . '
			ORDER BY epv.id	
		' . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') . '
	';

    try {
        $result = $db->query($sql)->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return array('result' => $result, 'count' => 0);
}

function getEscortgalleries($start = null, $limit = 0) {
    global $config, $db, $app;

    $limit = intval($limit);

    if (Cubix_Application::getId() == APP_ED) {
        $sql = '
			( SELECT eg.id , eg.escort_id, eg.title, eg.is_main
			FROM escort_galleries eg
			WHERE eg.is_main = 1 )
			UNION
			( SELECT eg.id, eg.escort_id, eg.title, eg.is_main
			FROM escort_photos ep
			INNER JOIN escort_galleries eg ON eg.id = ep.gallery_id
			INNER JOIN escorts e ON e.id = eg.escort_id
			WHERE e.status & 32 AND eg.status = 1 AND ep.type <> 5 AND ep.is_approved = 1
			GROUP BY ep.gallery_id
			HAVING COUNT(ep.id) OR eg.is_main = 1  > 0 )
			' . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') . '
		';
    }
    else {
        $sql = '
			SELECT eg.id, eg.escort_id, eg.title FROM escort_photos ep
			INNER JOIN escort_galleries eg ON eg.id = ep.gallery_id
			INNER JOIN escorts e ON e.id = eg.escort_id
			WHERE e.status & 32 AND eg.status = 1 AND ep.type <> 5 AND ep.is_approved = 1
			GROUP BY ep.gallery_id HAVING COUNT(ep.id)  > 0
			' . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') . '
		';
    }

    try {
        $result = $db->query($sql)->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return array('result' => $result, 'count' => 0);
}

function getEuroLottery($start = null, $limit = 0) {
    global $config, $db, $app;

    $limit = intval($limit);

    $sql = '
		SELECT el.id, el.escort_id, el.hash, el.ext FROM euro_lottery el
		INNER JOIN escorts e ON e.id = el.escort_id
		WHERE e.status & 32 AND el.status <> -1
		' . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') . '
	';

    try {
        $result = $db->query($sql)->fetchAll();

        foreach ($result as &$row) {
            $row['votes'] = $db->fetchOne("SELECT COUNT(m.euro_vote_for) FROM members m 
												INNER JOIN users u ON m.user_id = u.id
												WHERE m.euro_vote_for = ? AND u.status = 1
												GROUP BY m.euro_vote_for", array($row['escort_id']));
        }
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return array('result' => $result, 'count' => 0);
}


function getNaturalPics($start = null, $limit = 0) {
    global $config, $db, $app;

    $limit = intval($limit);

    $sql = '
		SELECT np.id, np.escort_id, np.hash, np.ext FROM natural_pics np
		INNER JOIN escorts e ON e.id = np.escort_id
		WHERE e.status & 32 AND np.status = 2
		' . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') . '
	';

    try {
        $result = $db->query($sql)->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return array('result' => $result, 'count' => 0);
}

function getEscortProfiles($start = null, $limit = 0) {
    global $config, $db, $app;

    $limit = intval($limit);

    $sql = '
		SELECT
			ep.escort_id, ep.data, ep.lang_id
		FROM escort_profiles_snapshots ep
		INNER JOIN escorts e ON ep.escort_id = e.id
		INNER JOIN users u ON u.id = e.user_id
		WHERE u.application_id = ? AND u.status = ? AND (e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ')
			/*' . ($app->id == 9 ? 'AND (SELECT COUNT(*) FROM escort_photos WHERE escort_id = e.id AND type = 2) > 0' : '') . '*/
		' . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') . '
	';

    $countSql = '
		SELECT
			COUNT(ep.escort_id)
		FROM escort_profiles_snapshots ep
		INNER JOIN escorts e ON ep.escort_id = e.id
		INNER JOIN users u ON u.id = e.user_id
		WHERE u.application_id = ? AND u.status = ? AND (e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ')
	';

    $result = $db->fetchAll($sql, array($app->id, STATUS_ACTIVE));
    $count = $db->fetchOne($countSql, array($app->id, STATUS_ACTIVE));

    return array('result' => $result, 'count' => $count);

    // Code below is commented out
    $where = '';

    try {
        $sql = '
			SELECT
				e.id,
				e.agency_id,
				e.showname,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				eph.hash AS photo_hash,
				eph.ext AS photo_ext,
				u.application_id,
				ep.contact_email,
				u.username,
				eph.status AS photo_status,

				/* Contacts Section */
				ct.' . Cubix_I18n::getTblField('title') . ' AS base_city,
				ct.id AS base_city_id,
				c.' . Cubix_I18n::getTblField('title') . ' AS country,
				c.slug AS country_slug,
				c.iso AS country_iso,
				r.slug AS region_slug,
				r.' . Cubix_I18n::getTblField('title') . ' AS region_title,
				ct.slug AS city_slug,
				IF(((DATE(ep.vac_date_from) <= CURDATE() AND DATE(ep.vac_date_to) > CURDATE()) OR DATE(ep.vac_date_from) > CURDATE()), ep.vac_date_to, 0) AS vac_date_to,
				ep.contact_phone,
				ep.phone_instructions,
				ep.contact_web,
				ep.contact_email,
				ep.contact_zip,

				/* About Section */
				FLOOR(DATEDIFF(CURDATE(), ep.birth_date) / 365.25) as age,
				n.iso AS nationality_iso,
				n.' . Cubix_I18n::getTblField('title') . ' AS nationality_title,
				ep.eye_color,
				ep.hair_color,
				ep.hair_length,
				ep.height,
				ep.weight,
				ep.bust,
				ep.waist,
				ep.hip,
				ep.shoe_size,
				ep.breast_size,
				ep.dress_size,
				ep.is_smoker,
				ep.measure_units,
				ep.availability,
				ep.sex_availability,
				ep.ethnicity,
				ep.' . Cubix_I18n::getTblField('about') . ' AS about,
				ep.characteristics,

				ep.cuisine,
				ep.drink,
				ep.flower,
				ep.gifts,
				ep.perfume,
				ep.designer,
				ep.interests,
				ep.sports,
				ep.hobbies,

				ep.min_book_time,
				ep.min_book_time_unit,

				ep.has_down_pay,
				ep.down_pay,
				ep.down_pay_cur,

				ep.reservation,

				/* SetcardInfo Section */
				e.date_last_modified,
				e.date_registered,
				(
					SELECT
						SUM(count)
					FROM escort_hits_daily
					WHERE escort_id = e.id AND DATE_ADD(date, INTERVAL 90 DAY) >= DATE(NOW())
				) AS hit_count,

				/* Services Section */
				ep.svc_kissing,
				ep.svc_blowjob,
				ep.svc_cumshot,
				ep.svc_69,
				ep.svc_anal,
				ep.' . Cubix_I18n::getTblField('svc_additional') . ' AS svc_additional
			FROM escort_profiles ep

			INNER JOIN escorts e ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN applications a ON a.id = u.application_id

			LEFT JOIN nationalities n ON n.id = ep.nationality_id

			LEFT JOIN escort_working_times ewt ON ewt.escort_id = e.id
			LEFT JOIN escort_langs el ON el.escort_id = e.id
			INNER JOIN escort_cities ec ON ec.escort_id = e.id

			INNER JOIN countries c ON c.id = e.country_id
			INNER JOIN cities ct ON ct.id = ec.city_id
			LEFT JOIN regions r ON r.id = ct.region_id
			LEFT JOIN cityzones cz ON cz.id = e.cityzone_id

			INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
			/*LEFT JOIN escort_hits_daily ehd ON ehd.escort_id = e.id AND DATE_ADD(ehd.date, INTERVAL 90 DAY) >= DATE(NOW())*/

			WHERE u.application_id = ? AND u.status = ? AND eph.is_main = 1' . $where . ' AND (e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ')
			GROUP BY e.id
		';

        $countSql = '
			SELECT
				COUNT(DISTINCT(e.id)) AS count
			FROM escort_profiles ep

			INNER JOIN escorts e ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN applications a ON a.id = u.application_id

			INNER JOIN escort_cities ec ON ec.escort_id = e.id

			INNER JOIN countries c ON c.id = e.country_id
			INNER JOIN cities cc ON cc.id = ec.city_id

			INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1

			WHERE u.application_id = ? AND u.status = ? AND eph.is_main = 1' . $where . ' AND (e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ')
		';

        $result = $db->fetchAll($sql, array($app->id, STATUS_ACTIVE));
        $count = $db->fetchOne($countSql, array($app->id, STATUS_ACTIVE));

        $data = array();
        foreach ($result as $i => $row) {
            $row['langs'] = $db->query('
				SELECT el.lang_id AS id, el.level, l.title FROM escort_langs el
				LEFT JOIN langs l ON l.id = el.lang_id
				WHERE el.escort_id = ?
			', $row['id'])->fetchAll();

            $row['rates'] = $db->query('
				SELECT availability, time, time_unit, price, currency_id FROM escort_rates
				WHERE escort_id = ?
			', $row['id'])->fetchAll();

            $row['travel_payment_methods'] = $db->query('
				SELECT payment_method, specify FROM travel_payment_methods
				WHERE escort_id = ?
			', $row['id'])->fetchAll();

            $row['tours'] = $db->query('
				SELECT t.id, t.date_from, t.date_to, t.phone, t.country_id,
					t.city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country_title,
					ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
					c.slug AS country_slug, r.slug AS region_slug, ct.slug AS city_slug,
					IF(DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()), 1, 0) AS is_in_tour,
					IF(DATE(t.date_from) > DATE(NOW()), 1, 0) AS is_upcoming_tour
				FROM tours t

				INNER JOIN countries c ON c.id = t.country_id
				INNER JOIN cities ct ON ct.id = t.city_id
				INNER JOIN regions r ON r.id = ct.region_id
				WHERE t.escort_id = ? AND (DATE(t.date_from) >= DATE(NOW()) OR  DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()))
				ORDER BY t.date_from ASC
			', $row['id'])->fetchAll();

            $row['is_open'] = $db->fetchOne('
				SELECT
					IF ((
						ewt.day_index = ' . date('N') . '
						AND ewt.time_from <= ' . date('G') . '
						AND ewt.time_to >= ' . date('G') . '
					) OR (
						ewt.day_index = ' . date('N') . '
						AND ewt.time_from <= ' . date('G') . '
						AND ewt.time_from > ewt.time_to
					) OR (
						ewt.day_index = ' . (1 == date('N') ? 7 : date('N') - 1) . '
						AND ewt.time_to >= ' . date('G') . '
						AND ewt.time_from  > ewt.time_to
					), 1, 0)
				FROM escort_working_times ewt
				WHERE ewt.escort_id = ? AND ewt.day_index = ?
			', array($row['id'], date('N')));

            $working_times = $db->query('
				SELECT ewt.day_index, ewt.time_from AS `from`, ewt.time_to AS `to`
				FROM escort_working_times ewt
				WHERE ewt.escort_id = ?
			', $row['id'])->fetchAll();

            $wts = array();
            foreach ($working_times as $i => $wt) {
                $wts[$wt['day_index']] = $wt;
            }

            $row['working_times'] = $wts;

            // $result[$i] = array('escort_id' => $row['id'], 'data' => $row);
            $data[] = array('escort_id' => $row['id'], 'data' => serialize($row));
        }

        return array('result' => $data, 'count' => $count);
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }
}

function getEscortProfilesV2($start = null, $limit = 0) {
    global $config, $db, $app;

    $limit = intval($limit);

    $sql_active_package = addActivePackageWhereParams();

    $sql = '
		SELECT SQL_CALC_FOUND_ROWS
			ep.escort_id, ep.data
		FROM escort_profiles_snapshots_v2 ep
		INNER JOIN escorts e ON ep.escort_id = e.id
		INNER JOIN users u ON u.id = e.user_id
		' . $sql_active_package['join'] . '
		WHERE
			u.application_id = ? AND u.status = ?
			AND (e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . ')
			' . $sql_active_package['where'] . '
		' . (!is_null($start) && $limit > 0 ? 'LIMIT ' . intval($start) . ', ' . $limit . '' : '') . '
	';

    $result = $db->fetchAll($sql, array($app->id, STATUS_ACTIVE));
    $count = $db->fetchOne('SELECT FOUND_ROWS()');

    return array('result' => $result, 'count' => $count);
}

function getLangs() {
    global $config, $db, $app;
    return Cubix_Application::getLangs($app->id);
}

function getAppLangs() {
    global $config, $db, $app;

    $app_langs = $db->fetchAll('
		SELECT * FROM application_langs WHERE application_id = ?
	', array($app->id));

    return $app_langs;
}

function getDictionary() {
    global $config, $db, $app;

    $lang_ids = Cubix_Application::getLangs($app->id);

    $fields = array();
    foreach ($lang_ids as $id) {
        $id = (array)$id;
        $fields[] = 'value_' . $id['id'];
    }

    $dic = $db->fetchAll('
		SELECT id, ' . implode(', ', $fields) . ' FROM dictionary WHERE application_id IS NULL
	', array(), Zend_Db::FETCH_ASSOC);

    foreach ($dic as $i => $row) {
        unset($dic[$i]);
        $dic[$row['id']] = $row;
    }

    $diff_dic = $db->fetchAll('
		SELECT id, ' . implode(', ', $fields) . ' FROM dictionary WHERE application_id = ?
	', array($app->id), Zend_Db::FETCH_ASSOC);

    foreach ($diff_dic as $row) {
        foreach ($row as $field => $value) {
            if ('id' == $field) continue;
            if (!is_null($value)) {
                $dic[$row['id']][$field] = $value;
            }
        }
    }

    return $dic;
}

function getStaticContent() {
    global $db, $app;

    return $db->fetchAll('SELECT * FROM static_content WHERE application_id = ?', $app->id);
}

function getSeoEntities() {
    global $config, $db, $app;

    $data = $db->fetchAll('
		SELECT * FROM seo_entities WHERE application_id = ?
	', $app->id);

    return $data;
}

function getSeoEntitiesInstances() {
    global $config, $db, $app;

    $data = $db->fetchAll('
		SELECT sei.* FROM seo_entity_instances sei
		INNER JOIN seo_entities se ON sei.entity_id = se.id
		/*WHERE (se.slug = "escort" AND sei.is_published = 1) OR (se.slug <> "escort")*/
	');

    return $data;
}

function getSeoEntitiesInstancesEF() {
    global $config, $db, $app;

    $data = $db->fetchAll('
		SELECT sei.* FROM seo_entity_instances sei
		INNER JOIN seo_entities se ON sei.entity_id = se.id
		INNER JOIN order_packages op ON op.escort_id = sei.primary_id
		WHERE op.package_id <> 100 AND op.order_id IS NOT NULL AND op.status = 2
	');

    return $data;
}


function getSR() {
    global $config, $db, $app;

    $data = $db->fetchRow('
		SELECT body, m_body, google_id FROM seo_robots WHERE application_id = ?
	', array($app->id));

    return $data;
}

function getGeography($section) {
    global $db;

    $data = $db->fetchAll('
		SELECT * FROM ' . $section . '
	');

    return $data;
}

function getActiveComments() {
    global $config, $db, $app;

    $data = $db->fetchAll('
		SELECT * FROM comments WHERE status = 1
	');

    return $data;
}

/*define('ESCORT_STATUS_NO_PROFILE', 			1);		// 000000001
define('ESCORT_STATUS_NO_ENOUGH_PHOTOS', 	2);		// 000000010
define('ESCORT_STATUS_NOT_APPROVED', 		4);		// 000000100
define('ESCORT_STATUS_OWNER_DISABLED', 		8);		// 000001000
define('ESCORT_STATUS_ADMIN_DISABLED', 		16);	// 000010000
define('ESCORT_STATUS_ACTIVE',				32);	// 000100000
define('ESCORT_STATUS_IS_NEW',				64);	// 001000000
define('ESCORT_STATUS_PROFILE_CHANGED',		128);	// 010000000

define('STATUS_NO_PROFILE',                 -2);
define('STATUS_NO_PHOTOS',                  -1);
define('STATUS_WAITING_APPROVAL',           -5);
define('STATUS_OWNER_DISABLED',             -3);
define('STATUS_ADMIN_DISABLED',             -4);
define('STATUS_ACTIVE',                     1);*/

define('APP_MIN_PHOTOS', 1);

function fixEscortStatus($escort_id) {
    global $config, $db, $app;

    $escort = $db->fetchRow('
		SELECT e.id, e.showname, COUNT(DISTINCT(eph.id)) AS photos_count, e.status
		FROM escorts e
		INNER JOIN users u ON u.id = e.user_id
		INNER JOIN escort_profiles ep ON ep.escort_id = e.id
		LEFT JOIN escort_photos eph ON eph.escort_id = e.id AND eph.type <> 3
		WHERE e.id = ?
		GROUP BY e.id
	', $escort_id);


    $status = 0;
    $ss = array();

    if (APP_MIN_PHOTOS > $escort['photos_count'] || STATUS_NO_PHOTOS == $escort['status']) {
        $status |= ESCORT_STATUS_NO_ENOUGH_PHOTOS;
        $ss[] = 'no_enough_photos';
    }

    if (STATUS_NO_PROFILE == $escort['status']) {
        $status |= ESCORT_STATUS_NO_PROFILE;
        $ss[] = 'no_profile';
    }

    if (STATUS_WAITING_APPROVAL == $escort['status'] && !($status & ESCORT_STATUS_NO_ENOUGH_PHOTOS)) {
        $status |= ESCORT_STATUS_NOT_APPROVED;
        $ss[] = 'not_approved';
    }

    if (STATUS_ADMIN_DISABLED == $escort['status']) {
        $status |= ESCORT_STATUS_ADMIN_DISABLED;
        $ss[] = 'admin_disabled';
    }

    if (STATUS_OWNER_DISABLED == $escort['status']) {
        $status |= ESCORT_STATUS_OWNER_DISABLED;
        $ss[] = 'owner_disabled';
    }

    if ($status & ESCORT_STATUS_NO_PROFILE && 0 == $escort['photos_count']) {
        $status |= ESCORT_STATUS_IS_NEW;
        $ss[] = 'is_new';
    }

    if (!($status & ESCORT_STATUS_NO_ENOUGH_PHOTOS) && STATUS_ACTIVE == $escort['status']) {
        $status |= ESCORT_STATUS_ACTIVE;
        $ss[] = 'active';
    }

    //echo $escort['showname'] . ': ' . implode(' ', $ss) . "\r\n";

    $db->query('UPDATE escorts SET status = ? WHERE id = ?', array($status, $escort_id));

}

function copyEscort($escort_id) {
    global $config, $db, $app;

    $url = "http://www.escortforumit.xxx/xml/profile.php?escort_id=" . $escort_id;
    $escort_data = file_get_contents($url);

    $status = 'copied';

    $escort_data = unserialize($escort_data);

    $_nationalities = $db->fetchAll('SELECT id, iso FROM nationalities');
    $nationalities = array();
    foreach ($_nationalities as $nat) {
        $nationalities[$nat['iso']] = $nat['id'];
    }

    $_countries = $db->fetchAll('SELECT id, iso FROM countries');
    $countries = array();
    foreach ($_countries as $country) {
        $countries[$country['iso']] = $country['id'];
    }

    $_cities = $db->fetchAll('SELECT id, internal_id FROM cities');
    $cities = array();
    foreach ($_cities as $city) {
        $cities[$city['internal_id']] = $city['id'];
    }

    $escort_data['user']['city_id'] = $cities[$escort_data['user']['city_id']];

    $escort_data['escort']['city_id'] = $cities[$escort_data['escort']['city_id']];
    $escort_data['escort']['country_id'] = $countries[$escort_data['escort']['country_id']];

    if ($escort_data['escort_profile']['nationality_id']) {
        $escort_data['escort_profile']['nationality_id'] = $nationalities[$escort_data['escort_profile']['nationality_id']];
    }

    if (isset($escort_data['agency']) && $escort_data['agency']) {
        $escort_data['agency']['country_id'] = $countries[$escort_data['agency']['country_iso']];
    }

    if (count($escort_data['escort_cities']) > 0) {
        foreach ($escort_data['escort_cities'] as $k => $city) {
            $escort_data['escort_cities'][$k]['city_id'] = $cities[$city['city_id']];
        }
    }

    if (count($escort_data['escort_tours']) > 0) {
        foreach ($escort_data['escort_tours'] as $k => $tour) {
            $escort_data['escort_tours'][$k]['country_id'] = $countries[$tour['country_iso']];
        }
    }

    try {
        $user_id = $escort_data['user']['id'];
        if ($db->fetchOne('SELECT TRUE FROM users WHERE id = ?', $user_id)) {
            unset($escort_data['user']['id']);
            $db->update('users', $escort_data['user'], $db->quoteInto('id = ?', $user_id));
        }
        else {
            $db->insert('users', $escort_data['user']);
        }

        if (isset($escort_data['agency']) && $escort_data['agency']) {
            $agency_id = $db->fetchOne('SELECT id FROM agencies WHERE user_id = ?', $user_id);
            if ($agency_id) {
                $db->update('agencies', $escort_data['agency'], $db->quoteInto('user_id = ?', $user_id));
            }
            else {
                $db->update('agencies', $escort_data['agency']);
                $agency_id = $db->lastInsertId();
            }
            $escort_data['escort']['agency_id'] = $agency_id;
        }

        $escort_id = $escort_data['escort']['id'];
        if ($db->fetchOne('SELECT TRUE FROM escorts WHERE id = ?', $escort_id)) {
            unset($escort_data['escort']['id']);
            $db->update('escorts', $escort_data['escort'], $db->quoteInto('id = ?', $escort_id));
            $db->update('escort_profiles', $escort_data['escort_profile'], $db->quoteInto('escort_id = ?', $escort_id));
            $status = 'updated';
        }
        else {
            $db->insert('escorts', $escort_data['escort']);
            $db->insert('escort_profiles', $escort_data['escort_profile']);
        }

        // Migrating escort cities
        $db->delete('escort_cities', $db->quoteInto('escort_id = ?', $escort_id));
        if (count($escort_data['escort_cities']) > 0) {
            foreach ($escort_data['escort_cities'] as $city) {
                $db->insert('escort_cities', $city);
            }
        }

        //Migrating escort rates
        $db->delete('escort_rates', $db->quoteInto('escort_id = ?', $escort_id));
        if (count($escort_data['escort_rates']) > 0) {
            foreach ($escort_data['escort_rates'] as $rate) {
                $rates = array(
                    'escort_id' => $rate['escort_id'],
                    'availability' => $rate['available_for'],
                    'time' => $rate['time'],
                    'time_unit' => $rate['time_unit'],
                    'price' => $rate['price'],
                    'currency_id' => $rate['currency_id']
                );

                $db->insert('escort_rates', $rates);
            }
        }

        //Migrating escort langs
        $db->delete('escort_langs', $db->quoteInto('escort_id = ?', $escort_id));
        if (count($escort_data['escort_langs']) > 0) {
            foreach ($escort_data['escort_langs'] as $lang) {
                $langs = array(
                    'escort_id' => $lang['escort_id'],
                    'level' => $lang['level'],
                    'lang_id' => $lang['lng_iso']
                );

                $db->insert('escort_langs', $langs);
            }
        }

        //Migrating escort working times
        $db->delete('escort_working_times', $db->quoteInto('escort_id = ?', $escort_id));
        if (count($escort_data['escort_working_times']) > 0) {
            foreach ($escort_data['escort_working_times'] as $wt) {
                $wts = array(
                    'escort_id' => $wt['escort_id'],
                    'day_index' => $wt['day'],
                    'time_from' => $wt['time_from'],
                    'time_to' => $wt['time_to']
                );

                $db->insert('escort_working_times', $wts);
            }
        }

        //Migrating escort photos
        $db->delete('escort_photos', $db->quoteInto('escort_id = ?', $escort_id));
        if (count($escort_data['escort_photos']) > 0) {
            foreach ($escort_data['escort_photos'] as $photo) {
                $db->insert('escort_photos', $photo);
            }
        }

        //Migrating escort comments
        $db->delete('comments', $db->quoteInto('escort_id = ?', $escort_id));
        if (count($escort_data['escort_comments']) > 0) {
            foreach ($escort_data['escort_comments'] as $comment) {
                $db->insert('comments', $comment);
            }
        }

        //Migrating escort tours
        $db->delete('tours', $db->quoteInto('escort_id = ?', $escort_id));
        if (count($escort_data['escort_tours']) > 0) {
            foreach ($escort_data['escort_tours'] as $tour) {
                $tours = array(
                    'escort_id' => $tour['escort_id'],
                    'date_from' => $tour['date_from'],
                    'date_to' => $tour['date_to'],
                    'email' => $tour['email'],
                    'phone' => $tour['phone'],
                    'country_id' => $countries[$tour['country_iso']],
                    'city_id' => $tour['city_id']
                );

                $db->insert('tours', $tours);
            }
        }

        fixEscortStatus($escort_id);

        return array('status' => 'success', 'params' => $status);
    } catch (Exception $e) {
        return array('status' => 'error', 'msg' => $e->getMessage());
    }
}


function updateHits($data) {
    global $db;
    if (!is_array($data) || 0 == count($data)) return false;

    $errors = array();

    foreach ($data as $escort_id => $count) {
        $escort_id = intval($escort_id);

        try {
            $does_exist = (bool)$db->fetchOne('SELECT TRUE FROM escort_hits_daily WHERE escort_id = ? AND date = DATE(NOW())', array($escort_id));

            $row_data = array(
                'count' => new Zend_Db_Expr('count + ' . intval($count))
            );

            if ($does_exist) {
                $db->update('escort_hits_daily', $row_data, array(
                    $db->quoteInto('escort_id = ?', $escort_id),
                    'date = DATE(NOW())'
                ));
            }
            else {
                $db->insert('escort_hits_daily', $row_data + array('date' => new Zend_Db_Expr('DATE(NOW())'), 'escort_id' => $escort_id));
            }
        } catch (Exception $e) {
            $errors[$escort_id] = Cubix_Api_Error::Exception2Array($e);
            continue;
        }
    }

    if (!count($errors)) {
        return true;
    }

    return $errors;
}

function reorderEscortPhotos($photos) {
    global $db;

    foreach ($photos as $i => $photo_id) {
        $db->query('UPDATE escort_photos SET ordering = ? WHERE id = ?', array($i, $photo_id));
    }

    $escort_id = $db->fetchOne('SELECT escort_id FROM escort_photos WHERE id = ?', array($photo_id));

    Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array(
        'action' => 'made ordering',
        'order' => $photos
    ));
}

function makeEscortPhoto($photo_id, $type) {
    global $db;
    $db->query('UPDATE escort_photos SET type = ? WHERE id = ?', array($type, $photo_id));

    $escort_id = $db->fetchOne('SELECT escort_id FROM escort_photos WHERE id = ?', $photo_id);
    if (1 > $db->fetchOne('SELECT COUNT(*) FROM escort_photos WHERE escort_id = ? AND is_main = 1 AND type <> 3 AND type <> 5', $escort_id)) {
        $db->query('UPDATE escort_photos SET is_main = 1 WHERE escort_id = ? AND type <> 3 AND type <> 5 LIMIT 1', $escort_id);
    }
    $db->query('UPDATE escort_photos SET is_main = 0 WHERE escort_id = ? AND (type = 3 OR type = 5)', $escort_id);

    if (!is_null($escort_id)) {
        $status = new Cubix_EscortStatus($escort_id);
        $status->save();
        resetPhotoVerification($escort_id);

    }

    Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array(
        'id' => $photo_id,
        'action' => 'status changed'
    ));
}

function resetPhotoVerification($escort_id) {
    global $db;
    $has_verification = $db->fetchOne('SELECT TRUE FROM escorts WHERE id = ? AND verified_status = 2 ', $escort_id);
    if ($has_verification) {
        $has_not_verified_public = $db->fetchOne('SELECT TRUE FROM escort_photos WHERE escort_id = ? AND ( type = ' . ESCORT_PHOTO_TYPE_SOFT . ' OR type = ' . ESCORT_PHOTO_TYPE_HARD . ' )  AND status <> 1', $escort_id);
        if (!$has_not_verified_public) {
            $db->update('escorts', array('verified_status' => 2, 'is_suspicious' => 0), $db->quoteInto('id = ?', $escort_id));
        }
        else {
            $db->update('escorts', array('verified_status' => 1), $db->quoteInto('id = ?', $escort_id));
        }
    }
}

function getEscortPhotoCount($escort_id) {
    global $db;

    return $db->fetchOne('SELECT COUNT(id) FROM escort_photos WHERE escort_id = ? AND type <> ? AND type <> ? ', array($escort_id, ESCORT_PHOTO_TYPE_ARCHIVED, ESCORT_PHOTO_TYPE_DISABLED));

}

///////////////// Profile Manipulations \\\\\\\\\\\\\\\\\\\\\\\\\\

global $PROFILE_SECTIONS;
global $PROFILE_SECTIONS_OLD;
// >>>>> Attention! The array is order sensitive! Do not change the ordering of fields, just append new fields...
$PROFILE_SECTIONS = array(
    'biography' => array('showname', 'gender', 'birth_date', 'ethnicity', 'nationality_id', 'measure_units', 'hair_color', 'hair_length', 'eye_color', 'height', 'weight', 'dress_size', 'shoe_size', 'bust', 'waist', 'hip', 'cup_size', 'pubic_hair', 'tatoo', 'piercing', 'home_city_id', 'breast_type', 'type'),
    'about-me' => array_merge(Cubix_I18n::getTblFields('about', null, true), array('is_smoking', 'is_drinking', 'characteristics', 'cuisine', 'drink', 'flower', 'gifts', 'perfume', 'designer', 'interests', 'sports', 'hobbies', 'min_book_time_unit', 'min_book_time', 'has_down_pay', 'down_pay', 'down_pay_cur', 'reservation')),
    'languages' => array('langs'),
    'working-cities' => array('country_id', 'city_id', 'cities', 'zip', 'incall_type', 'incall_hotel_room', 'incall_other', 'outcall_type', 'outcall_other'),
    'services' => array_merge(array('sex_orientation', 'sex_availability', 'services', 'keywords', 'pornstar_link'), Cubix_I18n::getTblFields('additional_service', null, true)),
    'working-times' => array('available_24_7', 'night_escort', 'times'),
    'prices' => array('rates', 'travel_payment_methods', 'travel_other_method', 'special_rates'),
    'contact-info' => array('phone_country_id', 'phone_number', 'phone_number_alt', 'phone_instr', 'phone_instr_no_withheld', 'phone_instr_other', 'email', 'website', 'club_name', 'street', 'street_no', 'contact_phone_parsed', 'disable_phone_prefix', 'address_additional_info', 'prefered_contact_methods', 'show_phone', 'skype_id', 'video_link')
);

if ($app->id != APP_ED) {
    unset($PROFILE_SECTIONS['contact-info'][14]);
    unset($PROFILE_SECTIONS['biography'][22]);
}

if ($app->id != APP_EG_CO_UK) {
    unset($PROFILE_SECTIONS['services'][4]);
}

if ($app->id != APP_A6) {
    unset($PROFILE_SECTIONS['contact-info'][15]);
    unset($PROFILE_SECTIONS['contact-info'][16]);
    unset($PROFILE_SECTIONS['contact-info'][17]);
}

function _getProfileSectionFields($section) {
    global $PROFILE_SECTIONS;

    if (!isset($PROFILE_SECTIONS[$section])) {
        return null;
    }

    return $PROFILE_SECTIONS[$section];
}

function _getProfileAllFields() {
    global $PROFILE_SECTIONS;

    $fields = array();
    foreach (array_values($PROFILE_SECTIONS) as $_fields) {
        $fields = array_merge($fields, $_fields);
    }

    return $fields;
}

function getEscortProfileData($escort_id, $revision_id = null, $section = null) {

    global $PROFILE_SECTIONS;
    $sections = $PROFILE_SECTIONS;
    global $db, $app;

    try {
        if (in_array($app->id, array(APP_EF, APP_EG_CO_UK))) {
            $base_city_id = $db->fetchRow('SELECT city_id FROM escorts WHERE id = ?', $escort_id);
            $city_id = $base_city_id['city_id'];

            $escort_cities = $db->fetchAll('SELECT escort_id, city_id FROM escort_cities WHERE escort_id = ?', $escort_id);
            $e_cities = array();

            foreach ($escort_cities as $e_city) {
                $e_cities[] = array('escort_id' => $e_city['escort_id'], 'city_id' => $e_city['city_id']);
            }
            $cities = $e_cities;

            $escort_cityzones = $db->fetchAll('SELECT escort_id, city_zone_id FROM escort_cityzones WHERE escort_id = ?', $escort_id);
            $e_cityzones = array();

            foreach ($escort_cityzones as $e_zone) {
                $e_cityzones[] = array('escort_id' => $e_zone['escort_id'], 'city_zone_id' => $e_zone['city_zone_id']);
            }
            $city_zones = $e_cityzones;
        }

        if ($revision_id) {
            $data = _getProfileByRevisionV2($escort_id, $revision_id);

            if (in_array($app->id, array(APP_EF, APP_EG_CO_UK))) {
                $data['city_id'] = $city_id;
                $data['cities'] = $cities;
                $data['cityzones'] = $city_zones;
            }
        }
        else {
            $data = getProfileLatestRevisionV2($escort_id, null, REVISION_STATUS_APPROVED);

            if (in_array($app->id, array(APP_EF, APP_EG_CO_UK))) {
                $data['city_id'] = $city_id;
                $data['cities'] = $cities;
                $data['cityzones'] = $city_zones;
            }

            return $data;
        }

        if (is_null($data)) return null;

        if (is_null($section)) {
            $fields = _getProfileAllFields();
        }
        else {
            $fields = _getProfileSectionFields($section);

            if (is_null($fields)) {
                return array('error' => 'Invalid section supplied');
            }
        }

        $result = array();
        foreach ($fields as $field) {
            $result[$field] = isset($data[$field]) ? $data[$field] : null;
        }

        if (in_array('langs', $fields) && is_null($result['langs'])) {
            $result['langs'] = array();
        }

        if (in_array('cities', $fields) && is_null($result['cities'])) {
            $result['cities'] = array();
        }

        if (in_array('services', $fields) && is_null($result['services'])) {
            $result['services'] = array();
        }

        if (in_array('keywords', $fields) && is_null($result['keywords'])) {
            $result['keywords'] = array();
        }

        if (in_array('times', $fields) && is_null($result['times'])) {
            $result['times'] = array();
        }

        if (in_array('rates', $fields) && is_null($result['rates'])) {
            $result['rates'] = array();
        }

        $cityzones = $db->fetchAll('
			SELECT *
			FROM escort_cityzones
			WHERE
				escort_id = ?
		', array($escort_id));

        if (!count($cityzones)) {
            $result['cityzones'] = array();
        }
        else {
            $result['cityzones'] = $cityzones;
        }
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return $result;
}

function getEscortProfileDataFront($escort_id, $revision_id = null, $section = null) {
    global $PROFILE_SECTIONS;
    $sections = $PROFILE_SECTIONS;
    global $db, $app;

    if ($revision_id) {
        $data = _getProfileByRevisionV2($escort_id, $revision_id);

    }
    else {
        $data = getProfileLatestRevisionV2($escort_id, null, null);
    }

    if (is_null($data)) return null;

    if (is_null($section)) {
        $fields = _getProfileAllFields();
    }
    else {
        $fields = _getProfileSectionFields($section);

        if (is_null($fields)) {
            return array('error' => 'Invalid section supplied');
        }
    }

    $result = array();
    foreach ($fields as $field) {
        $result[$field] = isset($data[$field]) ? $data[$field] : null;
    }

    if (in_array('langs', $fields) && is_null($result['langs'])) {
        $result['langs'] = array();
    }

    if ($app->id == APP_EF) {
        $base_city_id = $db->fetchRow('SELECT city_id FROM escorts WHERE id = ?', $escort_id);
        $result['city_id'] = $base_city_id['city_id'];

        $escort_cities = $db->fetchAll('SELECT escort_id, city_id FROM escort_cities WHERE escort_id = ?', $escort_id);
        $e_cities = array();

        foreach ($escort_cities as $e_city) {
            $e_cities[] = array('escort_id' => $e_city['escort_id'], 'city_id' => $e_city['city_id']);
        }
        $result['cities'] = $e_cities;
        if (in_array('cities', $fields) && is_null($result['cities'])) {
            $result['cities'] = array();
        }
    }
    else {
        if (in_array('cities', $fields) && is_null($result['cities'])) {
            $result['cities'] = array();
        }
    }

    if (in_array('services', $fields) && is_null($result['services'])) {
        $result['services'] = array();
    }

    if (in_array('keywords', $fields) && is_null($result['keywords'])) {
        $result['keywords'] = array();
    }

    if (in_array('times', $fields) && is_null($result['times'])) {
        $result['times'] = array();
    }

    if (in_array('rates', $fields) && is_null($result['rates'])) {
        $result['rates'] = array();
    }

    if ($app->id == APP_A6 || $app->id == APP_A6_AT) {
        $f_data = $db->fetchRow('
			SELECT fake_city_id, fake_zip, latitude, longitude
			FROM escorts
			WHERE id = ?
		', array($escort_id));

        $result['fake_city_id'] = $f_data['fake_city_id'];
        $result['fake_zip'] = $f_data['fake_zip'];
        $result['latitude'] = $f_data['latitude'];
        $result['longitude'] = $f_data['longitude'];
    }

    $cityzones = $db->fetchAll('
		SELECT *
		FROM escort_cityzones
		WHERE
			escort_id = ?
	', array($escort_id));

    if (!count($cityzones)) {
        $result['cityzones'] = array();
    }
    else {
        $result['cityzones'] = $cityzones;
    }

    return $result;
}

function getEscortProfileStruct() {
    global $PROFILE_SECTIONS;
    $sections = $PROFILE_SECTIONS;

    $fields = _getProfileAllFields();

    $result = array();
    foreach ($fields as $field) {
        $result[$field] = isset($data[$field]) ? $data[$field] : null;
    }

    $result['langs'] = array();
    $result['cities'] = array();
    $result['services'] = array();
    $result['keywords'] = array();
    $result['times'] = array();
    $result['rates'] = array();

    return $result;
}

function hasEscortProfile($user_id) {
    global $db;

    return (bool)$db->fetchOne('
		SELECT 1
		FROM profile_updates pu
		INNER JOIN escorts e ON pu.escort_id = e.id
		WHERE
			e.user_id = ?
	', array(
        $user_id
    ));
}

function hasEscortProfileV2($user_id) {
    global $db;

    return (bool)$db->fetchOne('
		SELECT 1
		FROM profile_updates_v2 pu
		INNER JOIN escorts e ON pu.escort_id = e.id
		WHERE
			e.user_id = ?
		GROUP BY e.user_id
	', array(
        $user_id
    ));
}

function hasEscortProfileByEscortIdV2($escort_id) {
    global $db;

    return (bool)$db->fetchOne('
		SELECT 1
		FROM profile_updates_v2 pu
		INNER JOIN escorts e ON pu.escort_id = e.id
		WHERE
			e.id = ?
		GROUP BY e.id
	', array(
        $escort_id
    ));
}

function getProfileLatestRevision($escort_id) {
    global $db, $app;

    $revision = $db->fetchOne('SELECT MAX(revision) FROM profile_updates WHERE escort_id = ?', $escort_id);
    $revision = intval($revision);

    return _getProfileByRevision($escort_id, $revision);
}

function getProfileLatestRevisionV2($escort_id, $revision_id = null, $status = null) {
    global $db, $app;

    if (!is_null($revision_id)) {
        $revision = $revision_id;
    }
    else {
        if (!is_null($status)) {
            $revision = $db->fetchOne('SELECT MAX(revision) FROM profile_updates_v2 WHERE escort_id = ? AND status = ?', array($escort_id, $status));
        }
        else {
            $revision = $db->fetchOne('SELECT MAX(revision) FROM profile_updates_v2 WHERE escort_id = ?', $escort_id);
        }

        $revision = intval($revision);
    }

    return _getProfileByRevisionV2($escort_id, $revision);
}

function _getProfileByRevisionV2($escort_id, $revision) {
    global $db, $app;

    $profile = $db->fetchOne('
		SELECT data FROM profile_updates_v2
		WHERE escort_id = ? AND revision = ?
	', array($escort_id, $revision));
    $profile = unserialize($profile);

    if (!$profile) {
        if (!$escort_id) {
            $profile = getEscortProfileStruct();

        }
        else {
            $profile = _getRealProfile($escort_id);
        }
    }

    return $profile;
}

function _getProfileByRevision($escort_id, $revision) {
    global $db, $app;

    $profile = $db->fetchOne('
		SELECT data FROM profile_updates
		WHERE escort_id = ? AND revision = ?
	', array($escort_id, $revision));
    $profile = unserialize($profile);

    if (!$profile) {
        $profile = _getRealProfile($escort_id);
    }

    return $profile;
}

function _getRealProfile($escort_id) {
    global $db, $app, $PROFILE_SECTIONS;
    $sections = $PROFILE_SECTIONS;

    $sections['biography'][0] = 'IF (ep.showname IS NULL, e.showname, ep.showname) AS showname';
    $sections['biography'][1] = 'IF (ep.gender IS NULL, e.gender, ep.gender) AS gender';
    $sections['biography'][2] = 'UNIX_TIMESTAMP(ep.birth_date) AS birth_date';
    unset($sections['languages'][0]);
    unset($sections['working-cities'][2]);
    $sections['working-cities'][0] = 'e.country_id';
    $sections['working-cities'][1] = 'e.city_id';
    unset($sections['services'][2]);
    unset($sections['services'][3]);
    unset($sections['working-times'][2]);
    unset($sections['prices'][0]);
    unset($sections['prices'][1]);
    unset($sections['prices'][2]);
    $sections['contact-info'][6] = 'ep.email';

    $fields = array();
    foreach (array_values($sections) as $_fields) {
        $fields = array_merge($fields, $_fields);
    }

    $sql = array(
        'tables' => array('escorts e'),
        'fields' => array_merge($fields, array()),
        'joins' => array(
            'INNER JOIN users u ON u.id = e.user_id',
            'INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id'
        ),
        'where' => array('e.id = ?' => $escort_id),
        'group' => 'e.id',
        'order' => false,
        'page' => false
    );

    $sql = Cubix_Model::getSql($sql);

    if (!$profile = $db->fetchRow($sql)) {
        $fields = array();
        foreach (array_values($PROFILE_SECTIONS) as $_fields) {
            $fields = array_merge($fields, $_fields);
        }

        $values = array();
        for ($i = 0; $i < count($fields); $i++) {
            $values[] = null;
        }

        $profile = array_combine($fields, $values);
    }

    $profile['langs'] = $db->fetchAll('SELECT lang_id, level FROM escort_langs WHERE escort_id = ?', $escort_id);
    $profile['cities'] = $db->fetchAll('SELECT city_id FROM escort_cities WHERE escort_id = ?', $escort_id);
    $profile['services'] = $db->fetchAll('SELECT service_id, price FROM escort_services WHERE escort_id = ?', $escort_id);
    $profile['keywords'] = $db->fetchAll('SELECT keyword_id FROM escort_keywords WHERE escort_id = ?', $escort_id);
    $profile['times'] = $db->fetchAll('SELECT day_index, time_from, time_from_m, time_to, time_to_m,night_escorts FROM escort_working_times WHERE escort_id = ?', $escort_id);
    $profile['rates'] = $db->fetchAll('SELECT availability, type, time, time_unit, price, currency_id FROM escort_rates WHERE escort_id = ?', $escort_id);
    //$profile['conditions'] = $db->fetchAll('SELECT min_book_time, min_book_time_unit, has_down_pay, down_pay, down_pay_cur, reservation FROM escort_profiles_v2 WHERE escort_id = ?', $escort_id);
    //$profile['conditions']['travel_payment_method'] = $db->fetchAll('SELECT payment_method, specify FROM travel_payment_methods WHERE escort_id = ?', $escort_id);

    return $profile;
}

define('REVISION_STATUS_NOT_APPROVED', 1);
define('REVISION_STATUS_APPROVED', 2);
define('REVISION_STATUS_DECLINED', 3);

function updateEscortProfile($escort_id, $data, $section = null) {
    global $db;

    if (get_magic_quotes_gpc()) {
        $data = stripslashes($data);
    }

    $data = unserialize($data);

    if (!is_array($data)) return;

    $revision = $db->fetchOne('SELECT MAX(revision) AS revision FROM profile_updates WHERE escort_id = ?', array($escort_id));
    $status = $db->fetchOne('SELECT status FROM profile_updates WHERE escort_id = ? AND revision = ?', array($escort_id, $revision));


    // --> Compare revisions
    $old_profile = getProfileLatestRevision($escort_id);

    if (!$old_profile) {
        $old_profile = array();
    }

    $profile = array_merge($old_profile, $data);

    if (!isset($profile['langs']) || !is_array($profile['langs'])) {
        $profile['langs'] = array();
    }

    foreach ($profile['langs'] as $i => $lang) {
        $profile['langs'][$i]['escort_id'] = $escort_id;
    }

    if (!isset($profile['cities']) || !is_array($profile['cities'])) {
        $profile['cities'] = array();
    }

    foreach ($profile['cities'] as $i => $city) {
        $profile['cities'][$i]['escort_id'] = $escort_id;
    }

    if (!isset($profile['services']) || !is_array($profile['services'])) {
        $profile['services'] = array();
    }

    foreach ($profile['services'] as $i => $service) {
        $profile['services'][$i]['escort_id'] = $escort_id;
    }

    if (!isset($profile['keywords']) || !is_array($profile['keywords'])) {
        $profile['keywords'] = array();
    }

    foreach ($profile['keywords'] as $i => $keyword) {
        $profile['keywords'][$i]['escort_id'] = $escort_id;
    }

    if (!isset($profile['times']) || !is_array($profile['times'])) {
        $profile['times'] = array();
    }

    foreach ($profile['times'] as $i => $time) {
        $profile['times'][$i]['escort_id'] = $escort_id;
    }

    if (!isset($profile['rates']) || !is_array($profile['rates'])) {
        $profile['rates'] = array();
    }

    foreach ($profile['rates'] as $i => $time) {
        $profile['rates'][$i]['escort_id'] = $escort_id;
    }

    // var_dump($old_profile);var_dump($data);var_dump($profile);die;
    // <--

    if ($revision > 0 && $status == REVISION_STATUS_NOT_APPROVED) {
        $db->update('profile_updates', array(
            'date_updated' => new Zend_Db_Expr('NOW()'),
            'data' => serialize($profile)
        ), array(
            $db->quoteInto('escort_id = ?', $escort_id),
            $db->quoteInto('revision = ?', $revision)
        ));
    }
    else {
        $db->insert('profile_updates', array(
            'escort_id' => $escort_id,
            'revision' => $revision + 1,
            'date_updated' => new Zend_Db_Expr('NOW()'),
            'data' => serialize($profile)
        ));
    }

    return true;
}

function updateEscortProfileV2($escort_id, $data, $revision_id = null, $section = null, $backend = false) {
    global $db, $app;

    try {
        if (get_magic_quotes_gpc()) {
            $data = stripslashes($data);
        }
        $sales_user_id = null;
        $data = unserialize($data);
        $bl_ip = $data['bl_ip'];
        // $bl_ip = '178.46.208.206';
        unset($data['bl_ip']);
        if (isset($data['backend_user_id']) && intval($data['backend_user_id']) > 0) {
            $sales_user_id = intval($data['backend_user_id']);
        }

        unset($data['backend_user_id']);

        $it_prefix_changed = false;
        if (isset($data['it_prefix_changed'])) {
            $it_prefix_changed = true;
            unset($data['it_prefix_changed']);
        }

        $profile_type = null;
        if (isset($data['profile_type']) && $data['profile_type']) {
            $profile_type = $data['profile_type'];
            unset($data['profile_type']);
        }

        $is_pseudo = false;
        if (isset($data['pseudo_escort']) && $data['pseudo_escort']) {
            $is_pseudo = true;
            unset($data['pseudo_escort']);
        }

        if (isset($data['website_changed'])) {
            $db->update('escorts', array('check_website' => 0), array($db->quoteInto('id = ?', $escort_id)));
            unset($data['website_changed']);
        }


        if (in_array($app->id, array(APP_EF))) {
            if($data['location_lat'] == ''){
                $location_lat = null;
            }
            if($data['location_lng'] == ''){
                $location_lng = null;
            }
            $db->update('escort_profiles_v2', array('location_map_address' => $data['location_map_address'],'location_lat' => $location_lat, 'location_lng' => $location_lng), $db->quoteInto('escort_id = ?', $escort_id));
            unset($data['location_lat']);
            unset($data['location_lng']);
            unset($data['location_map_address']);
        }

        if (in_array($app->id, array(APP_EG_CO_UK))) {
            $old_coords = $db->fetchRow('SELECT latitude, longitude FROM escorts WHERE id = ?', array($escort_id));
            if(isset($data['longitude']) && isset($data['latitude']) && strlen($data['longitude']) && strlen($data['latitude']) && $old_coords->latitude != $data['latitude'] && $old_coords->longitude != $data['longitude']){
                $db->update('escorts', array(
                    'latitude' => $data['latitude'],
                    'longitude' => $data['longitude']
                ), $db->quoteInto('id = ?', $escort_id));
            }
            unset($data['longitude']);
            unset($data['latitude']);
        }

		if (!is_array($data)) return;

        if (!is_null($revision_id)) {
            $revision = $revision_id;
        }
        else {
            $revision = $db->fetchOne('SELECT MAX(revision) AS revision FROM profile_updates_v2 WHERE escort_id = ?', array($escort_id));
        }
        $status = $db->fetchOne('SELECT status FROM profile_updates_v2 WHERE escort_id = ? AND revision = ?', array($escort_id, $revision));


        // --> Compare revisions
        $old_profile = getProfileLatestRevisionV2($escort_id);

        //Working location autoapprove
        if (in_array($app->id, array(APP_EF))) {
            unset($old_profile['city_id']);
            unset($old_profile['cities']);
            unset($old_profile['cityzones']);
        }

        // if ( in_array($app->id, array(APP_ED)) ) {
        // 	$package = premium_getEscortActivePackage($escort_id, 'en');

        // 	if($package && $package['cities'] > 0){
        // 		if($data['city_id'] != $package['cities'][0]['id']){
        // 			premium_updatePremiumCities($package['opid'], array($data['city_id']), $escort_id);
        // 		}
        // 	}
        // }

        if (!$old_profile) {
            $old_profile = array();
        }

        $profile = array_merge($old_profile, $data);

        $autoApprooveCities = false;

        if (in_array($app->id,array(APP_6B,APP_ED)))
        {
            if($old_profile['country_id'] && ($data['country_id'] == $old_profile['country_id']))
            {
                $db->update('escorts', array('city_id' => $profile['city_id'], 'date_last_modified' => new Zend_Db_Expr('NOW()')), $db->quoteInto('id = ?', $escort_id));
                $cc = $db->fetchAll('SELECT city_id FROM escort_cities WHERE escort_id = ?', $escort_id);
                $oldCities = array();
                if ($cc)
                    foreach ($cc as $c) {
                        $oldCities[] = $c['city_id'];
                    }
                $db->delete('escort_cities', $db->quoteInto('escort_id = ?', $escort_id));
                // Inserting escort cities
                $cities = $profile['cities'];
                if (count($cities) > 0) {
                    foreach ($cities as $city) {
                        $city['escort_id'] = $escort_id;
                        $db->insert('escort_cities', $city);

                        if (!in_array($city['city_id'], $oldCities)) {
                            Cubix_AlertEvents::notify($escort_id, ALERT_ME_CITY, array('id' => $city['city_id'], 'type' => 'working'));
                            Cubix_CityAlertEvents::notify($city['city_id'], $escort_id, NULL);
                            if ($status == REVISION_STATUS_APPROVED)
                            {
                                $autoApprooveCities = true;
                            }
                        }
                    }
                }
            }
        }

        // If there is no row in escorts or profile_updates_v2 table, insert them
        if (false === ($showname = $db->fetchOne('SELECT showname FROM escorts WHERE id = ?', $escort_id))) {
            $insert_arr = array(
                'showname' => $profile['showname'],
                'gender' => $profile['gender'],
                'agency_id' => isset($profile['agency_id']) ? $profile['agency_id'] : null,
                'user_id' => isset($profile['user_id']) ? $profile['user_id'] : null,
                'country_id' => isset($profile['country_id']) ? $profile['country_id'] : null,
                'city_id' => isset($profile['city_id']) ? $profile['city_id'] : null,
                'status' => ESCORT_STATUS_NO_PROFILE | ESCORT_STATUS_PROFILE_CHANGED | ESCORT_STATUS_IS_NEW | ESCORT_STATUS_NO_ENOUGH_PHOTOS /*| ( isset($profile['agency_id']) ? ESCORT_STATUS_OWNER_DISABLED : 0 )*/
            );
            if ($app->id == APP_ED) {
                $insert_arr['type'] = $profile['type'];
            }
            $db->insert('escorts', $insert_arr);
            $escort_id = $db->lastInsertId();
        }
        if (in_array($app->id, array(APP_EG_CO_UK))) {
            $db->update('escorts', array('gender' => $profile['gender']), $db->quoteInto('id = ?', $escort_id));
            $db->update('escort_profiles_v2', array('gender' => $profile['gender']), $db->quoteInto('escort_id = ?', $escort_id));
        }

        //	else {
        //		$_auto_approval = $db->fetchOne('SELECT auto_approval FROM escorts WHERE id = ?', $escort_id);
        //
        //		if ( ! $_auto_approval && ! $backend ) {
        //			$_status = $db->fetchOne('SELECT status FROM escorts WHERE id = ?', $escort_id);
        //			if ( ! ( $_status & ESCORT_STATUS_PROFILE_CHANGED ) ) {
        //				$db->update('escorts', array('status' => $_status | ESCORT_STATUS_PROFILE_CHANGED), array($db->quoteInto('id = ?', $escort_id)));
        //			}
        //		}
        //	}
        //Working location autoapprove
        /*EGUK-47*/


		// PHONE GRINDER FOR FAST BACKEND SEARCHING ONLY
		if (in_array($app->id, array(APP_A6)) && isset($data['phone_changed'])) {
			$bulk_object = new Cubix_BulkInserter($db, 'phone_grinder');
			$bulk_object->addKeys(array('phone_part','escort_id'));
			$db->delete('phone_grinder', $db->quoteInto('escort_id = ?', $escort_id));
			foreach( Cubix_Utils::phoneGrinder($data['contact_phone_parsed']) as $part_phone){
				$bulk_object->addFields(array('phone_part' => $part_phone, 'escort_id' => $escort_id ));	
			}
			$bulk_object->insertBulk();
		}
		
		if (isset($data['phone_changed'])) {
            if ($escort_id) {
				$escort_data = array('phone_changed' => new Zend_Db_Expr('NOW()'));
				
				if (in_array($app->id, array(APP_ED, APP_EG_CO_UK)) && ( $hand_check = _getHandcheckData($escort_id)) ){
					if($hand_check['hand_verification_sales_id'] !== -1){
						$back_user = $db->fetchOne('SELECT username FROM backend_users WHERE id = ? ', array($hand_check['hand_verification_sales_id']));
					}
					else{
						$back_user = 'system';
					}
					
					$escort_comment = array(
						'escort_id' => $escort_id,
						'agency_id' =>  isset($profile['agency_id']) ? $profile['agency_id'] : null,
						'date' => new Zend_Db_Expr('NOW()'),
						'sales_user_id' => 	$hand_check['hand_verification_sales_id'],
						'comment' => 'phone number was changed, last hand check verified date '. $hand_check['last_hand_verification_date'] . ', '. $back_user .' , for '. $data['old_phone'] . ' and '.$old_profile['email']
					);	
					
					$db->insert('escort_comments', $escort_comment);
					
					$escort_data['last_hand_verification_date'] = null; 
					$escort_data['hand_verification_sales_id'] = null;
					
				} 
								
                $db->update('escorts', $escort_data , array($db->quoteInto('id = ?', $escort_id)));
                Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHONE_CHANGED, array('before' => $data['old_phone'], 'after' => $data['contact_phone_parsed']), $sales_user_id);
            }
            $autoApprooveCities = false;
            unset($profile['phone_changed']);
            unset($profile['old_phone']);
        }

        if (in_array($app->id, array(APP_EG_CO_UK,APP_6B,APP_ED)))
        {
            $where = "";
            
            if(isset($profile['old_city_id']))
            {
                $admin = true;
                $where .= " AND pres.city_id = ".$profile['old_city_id'];
            }else{
                $admin = false;
                $where .= " AND pres.city_id = e.city_id";
            }
            $sql = "SELECT pres.city_id as city_id, pres.order_package_id
                    FROM
                        escorts e
                        INNER JOIN order_packages op ON op.escort_id = e.id
                        INNER JOIN premium_escorts pres ON pres.order_package_id = op.id 
                    WHERE
                        e.id = ? ";
            $where .= " AND op.`status` <= 2 ";
            $sql .= $where;
            $old_premium_data = $db->fetchAll($sql , array( $escort_id ) );
            if ( $old_premium_data )
            {
                foreach ( $old_premium_data as $old )
                {
                    if ( $profile['city_id'] != $old['city_id'] )
                    {
                        $db->update('premium_escorts' , array( 'city_id' => $profile['city_id'] ) , 'order_package_id = '.$old['order_package_id'].' AND city_id = '.$old['city_id'] );
                    }
                }
            }
			
			if ( $admin === true)
            {
                unset( $profile['old_city_id'] );
                unset( $profile['old_cities_ids'] );
            }
        }


        if (in_array($app->id, array(APP_EG_CO_UK))) {

            if (isset($data['premium_cities'])) {
                $opid = premium_getEscortActiveOrderPackageId($escort_id, 'en');
                $db->query('DELETE FROM premium_escorts WHERE order_package_id = ?', $opid);
                if (count($data['premium_cities']) > 0) {
                    foreach ($data['premium_cities'] as $c) {
                        $db->insert('premium_escorts', array('order_package_id' => $opid, 'city_id' => $c));
                    }
                }
                unset($data['premium_cities']);
            }

        }

        if (in_array($app->id, array(APP_EF,APP_EG_CO_UK))) {
            $db->update('escorts', array('city_id' => $profile['city_id'], 'date_last_modified' => new Zend_Db_Expr('NOW()')), $db->quoteInto('id = ?', $escort_id));
            if ($app->id != APP_EG_CO_UK)
            {
                unset($profile['city_id']);
            }

            $cc = $db->fetchAll('SELECT city_id FROM escort_cities WHERE escort_id = ?', $escort_id);
            $old_cities = array();
            if ($cc)
                foreach ($cc as $c) {
                    $old_cities[] = $c['city_id'];
                }

            $db->delete('escort_cities', $db->quoteInto('escort_id = ?', $escort_id));
            // Inserting escort cities
            $cities = $profile['cities'];
            if (count($cities) > 0) {
                foreach ($cities as $city) {
                    $city['escort_id'] = $escort_id;
                    $db->insert('escort_cities', $city);

                    if (!in_array($city['city_id'], $old_cities)) {
                        Cubix_AlertEvents::notify($escort_id, ALERT_ME_CITY, array('id' => $city['city_id'], 'type' => 'working'));
                        Cubix_CityAlertEvents::notify($city['city_id'], $escort_id, NULL);
                    }
                }
            }
            if ($app->id != APP_EG_CO_UK) {
                unset($profile['cities']);

                $db->delete('escort_cityzones', $db->quoteInto('escort_id = ?', $escort_id));
                // Inserting escort cityzones
                $zones = $data['cityzones'];
                if (count($zones) > 0) {
                    foreach ($zones as $zone) {
                        $zone['escort_id'] = $escort_id;
                        $db->insert('escort_cityzones', $zone);
                    }
                }

                unset($profile['cityzones']);
            }
			Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('esc_id' => $escort_id), $sales_user_id);
        }


        if (in_array($app->id, array(APP_EF, APP_6A)) && $profile_type) {
            $db->update('escorts', array('profile_type' => $profile_type), $db->quoteInto('id = ?', $escort_id));
        }

        if ($is_pseudo) {
            $db->update('escorts', array('pseudo_escort' => 1), array($db->quoteInto('id = ?', $escort_id)));
        }

        if (isset($profile['display_address'])) {
            $profile['display_address'] = intval($profile['display_address']);
        }

        if (isset($profile['night_escort'])) {
            $profile['night_escort'] = $profile['night_escort'];
        }

        if (!isset($profile['langs']) || !is_array($profile['langs'])) {
            $profile['langs'] = array();
        }

        foreach ($profile['langs'] as $i => $lang) {
            $profile['langs'][$i]['escort_id'] = $escort_id;
        }

        if (!isset($profile['cityzones']) || !is_array($profile['cityzones'])) {
            $profile['cityzones'] = array();
        }

        foreach ($profile['cityzones'] as $i => $zone) {
            $profile['cityzones'][$i]['escort_id'] = $escort_id;
        }

        if (!isset($profile['cities']) || !is_array($profile['cities'])) {
            $profile['cities'] = array();
        }

        foreach ($profile['cities'] as $i => $city) {
            $profile['cities'][$i]['escort_id'] = $escort_id;
        }

        if (!isset($profile['services']) || !is_array($profile['services'])) {
            $profile['services'] = array();
        }

        foreach ($profile['services'] as $i => $service) {
            $profile['services'][$i]['escort_id'] = $escort_id;
        }

        if (!isset($profile['keywords']) || !is_array($profile['keywords'])) {
            $profile['keywords'] = array();
        }

        foreach ($profile['keywords'] as $i => $keyword) {
            $profile['keywords'][$i]['escort_id'] = $escort_id;
        }

        if (!isset($profile['times']) || !is_array($profile['times'])) {
            $profile['times'] = array();
        }

        foreach ($profile['times'] as $i => $time) {
            $profile['times'][$i]['escort_id'] = $escort_id;
        }

        if (!isset($profile['rates']) || !is_array($profile['rates'])) {
            $profile['rates'] = array();
        }

        foreach ($profile['rates'] as $i => $time) {
            $profile['rates'][$i]['escort_id'] = $escort_id;
        }




    /* Grigor */
    $oldBirthDate = date('Y',$old_profile['birth_date']);
    $newBirthDate = date('Y',$data['birth_date']);

    if ( $oldBirthDate == $newBirthDate ) {
		$profile['birth_date'] = $old_profile['birth_date'];
	}

	//return array('old' => $old_profile, 'new' => $profile);

	// <--


	if ( ! $db->fetchOne('SELECT TRUE FROM escort_profiles_v2 WHERE escort_id = ?', $escort_id) ) {
		$db->insert('escort_profiles_v2', array(
			'showname' => $profile['showname'],
			'escort_id' => $escort_id
		));
	}

        if (isset($profile['slogan']) && $profile['slogan']) {
            $db->update('escorts', array('slogan' => $profile['slogan']), $db->quoteInto('id = ?', $escort_id));
        }
        if ($app->id == APP_ED) {
            if (isset($profile['type']) && $profile['type']) {
                $db->update('escorts', array('type' => $profile['type']), $db->quoteInto('id = ?', $escort_id));
            }
            if (isset($profile['agency_id']))
            {
                $db->update('escorts', array('city_id' => $profile['city_id'], 'country_id' => $profile['country_id']), $db->quoteInto('id = ?', $escort_id)); //EDIR-298
            }
            $is_ip_blacklisted = $db->fetchOne('SELECT block FROM blacklisted_ips WHERE ip = ?', $bl_ip);
            if ($is_ip_blacklisted) {
                $m_status = new Cubix_EscortStatus($escort_id, null, false, false);
                $m_status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ADMIN_DISABLED);
                $m_status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_IP_BLOCKED);
                $m_status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
                $m_status->save();
                $db->update('escorts', array('status_comment' => 'IP blacklisted'), $db->quoteInto('id = ?', $escort_id));
            }
        }

        if (isset($profile['vac_date_from']) && isset($profile['vac_date_to'])) {
            $db->update('escorts', array('vac_date_from' => $profile['vac_date_from'], 'vac_date_to' => $profile['vac_date_to']), $db->quoteInto('id = ?', $escort_id));
        }
        unset($profile['vac_date_from']);
        unset($profile['vac_date_to']);

        /* get latitude and longitude */
        $country_title = $db->fetchOne('SELECT title_en FROM countries WHERE id = ?', array($profile['country_id']));
        $address_data = array();
        if ($app->id == APP_A6) {
            $fake_city_id = $db->fetchOne('SELECT fake_city_id FROM escorts WHERE id = ?', array($escort_id));
            $city_title = $db->fetchOne('SELECT title_en FROM f_cities WHERE id = ?', array($fake_city_id));

            $address_data = array(
                $profile['street_no'] . ' ' . $profile['street'],
                $city_title,
                $country_title
            );
        } /*else {
		if ( isset($profile['city_id']) ) {
			$city_title = $db->fetchOne('SELECT title_en FROM cities WHERE id = ?', array($profile['city_id']));

			$address_data = array(
				$data['street_no'] . ' ' . $data['street'],
				$city_title,
				$country_title
			);
		}
	}*/
        if ($app->id == APP_A6) {
        	$old_coords = $db->fetchRow('SELECT latitude, longitude FROM escorts WHERE id = ?', array($escort_id));
        	if(isset($data['longitude']) && isset($data['latitude']) && strlen($data['longitude']) && strlen($data['latitude']) && $old_coords->latitude != $data['latitude'] && $old_coords->longitude != $data['longitude']){
        		$coord = array(
                    'latitude' => $data['latitude'],
                    'longitude' => $data['longitude']
                );
                $db->update('escorts', $coord, $db->quoteInto('id = ?', $escort_id));
        	}
        	else{
	            if (!empty($address_data)) {
	                $location = str_replace(" ", "+", implode(',', $address_data));
	                $url = "http://open.mapquestapi.com/geocoding/v1/address?key=V33bo8GqPedNwMOUCrAvmnHzzyszaw83&location=" . $location;
	                $url = preg_replace('# #', '+', $url);
	                $geo_data = file_get_contents($url);
	                if(!$geo_data){
	                	// Cubix_Email::send('tigranyeng@gmail.com', 'mapquestapi expired 2IUAiWs5TEXGwOPGlGoPe9fnpaIniyFZ', 'mapquestapi expired 2IUAiWs5TEXGwOPGlGoPe9fnpaIniyFZ');
	                	$url = "http://open.mapquestapi.com/geocoding/v1/address?key=2IUAiWs5TEXGwOPGlGoPe9fnpaIniyFZ&location=" . $location;
		                $url = preg_replace('# #', '+', $url);
		                $geo_data = file_get_contents($url);
	                }
	                if($geo_data){
		                $geo_data = json_decode($geo_data);
	                	if(strpos($geo_data->results[0]->locations[0]->latLng->lng, '-') !== false){
		                	$city_coords = $db->fetchRow('SELECT latitude, longitude FROM f_cities WHERE id = ?', array($profile['fake_city_id']));
		                	$coord = array(
			                    'latitude' => $city_coords->latitude,
			                    'longitude' => $city_coords->longitude
			                );
		                }else{
			                $coord = array(
			                    'latitude' => $geo_data->results[0]->locations[0]->latLng->lat,
			                    'longitude' => $geo_data->results[0]->locations[0]->latLng->lng
			                );
		                }
		                
		                $db->update('escorts', $coord, $db->quoteInto('id = ?', $escort_id));
	                }else{
	                	Cubix_Email::send('tigranyeng@gmail.com', 'api mapquestapi 2 expired 2IUAiWs5TEXGwOPGlGoPe9fnpaIniyFZ', 'mapquestapi expired 2IUAiWs5TEXGwOPGlGoPe9fnpaIniyFZ ' . $url);
	                }
	            }
        	}
        	unset($data['longitude']);
            unset($data['latitude']);
        }
        /* get latitude and longitude */

        if ($app->id == APP_A6 || $app->id == APP_A6_AT) {
            if (isset($profile['fake_city_id']) && $profile['fake_city_id']) {
                $db->update('escorts', array('fake_city_id' => $profile['fake_city_id']), $db->quoteInto('id = ?', $escort_id));
            }
            if (isset($profile['fake_zip']) && $profile['fake_zip']) {
                $db->update('escorts', array('fake_zip' => $profile['fake_zip']), $db->quoteInto('id = ?', $escort_id));
            }

            unset($profile['fake_city_id']);
            unset($profile['fake_zip']);

            Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('esc_id' => $escort_id), $sales_user_id);
        }

        /*if ( isset($profile['home_city_id']) && $profile['home_city_id'] ) {
		$db->update('escorts', array('home_city_id' => $profile['home_city_id']), $db->quoteInto('id = ?', $escort_id));
	}*/

        if (count($profile['cityzones'])) {
            $db->delete('escort_cityzones', $db->quoteInto('escort_id = ?', $escort_id));
            foreach ($profile['cityzones'] as $zone) {
                $db->insert('escort_cityzones', array(
                    'escort_id' => $escort_id,
                    'city_zone_id' => $zone['city_zone_id']
                ));
            }
        }
        else {
            if ($app->id == APP_ED || $app->id == APP_EG_CO_UK) {
                $db->delete('escort_cityzones', $db->quoteInto('escort_id = ?', $escort_id));
            }
        }

        /* Grigor */
        if (isset($profile['block_countries'])) {
            $db->delete('escort_blocked_countries', $db->quoteInto('escort_id = ?', $escort_id));
            if (count($profile['block_countries'])) {
                foreach ($profile['block_countries'] as $country) {
                    $db->insert('escort_blocked_countries', array(
                        'escort_id' => $escort_id,
                        'country_id' => $country['country_id']
                    ));
                }
            }
            unset($profile['block_countries']);
        }

        if (isset($profile['blocked_members'])) {
            $db->delete('escort_blocked_users', $db->quoteInto('escort_id = ?', $escort_id));
            if (count($profile['blocked_members'])) {
                foreach ($profile['blocked_members'] as $member_id) {
                    $db->insert('escort_blocked_users', array(
                        'escort_id' => $escort_id,
                        'user_id' => $member_id
                    ));
                }
            }
            unset($profile['blocked_members']);
        }


        /*if (isset($profile['conditions']))
	{
		$db->delete('travel_payment_methods', $db->quoteInto('escort_id = ?', $escort_id));
		foreach ($profile['conditions']['travel_payment_method'] as $pm)
		{
			$arr = array(
				'escort_id' => $escort_id,
				'payment_method' => $pm
			);

			if ($pm == PM_OTHER)
				$arr['specify'] = $profile['conditions']['specify'];

			$db->insert('travel_payment_methods', $arr);
		}
	}*/


        $db->delete('travel_payment_methods', $db->quoteInto('escort_id = ?', $escort_id));
        $travel_payment_methods = $profile['travel_payment_methods'];
        $travel_other_method = $profile['travel_other_method'];

        if (count($travel_payment_methods[0])) {
            foreach ($travel_payment_methods[0] as $method) {
                $d = array('escort_id' => $escort_id, 'payment_method' => $method);
                if ($method == PM_OTHER) {
                    $d['specify'] = $travel_other_method[0];
                }
                $db->insert('travel_payment_methods', $d);
            }
        }

        /*unset($profile['travel_payment_methods']);
	unset($profile['travel_other_method']);*/



	// If there is no showname, set it
	if ( ! strlen($showname) ) {
		$db->update('escorts', array(
			'showname' => $profile['showname']
		), array($db->quoteInto('id = ?', $escort_id)));
	}

	if ( in_array($app->id, array(APP_EF,APP_EG_CO_UK)) ) {
		snapshotProfileV2($escort_id);
	}


	$profile_changes = differProfile($old_profile, $profile);
	
	
	$and_6_autoapproval = false;

        if ($app->id == APP_A6 && $status == REVISION_STATUS_APPROVED) {
            $free_text_fields = array('showname', 'slogan', 'website', 'phone_instr_other', 'about_de', 'about_es', 'about_hu', 'about_it', 'about_fr', 'about_en', 'about_ro', 'additional_service_de', 'additional_service_it', 'additional_service_fr', 'additional_service_en', 'additional_service_es', 'additional_service_ro', 'additional_service_hu', 'video_link');

            $added = $profile_changes['added'];
            $changed = $profile_changes['changed'];

            $contains_free_text_fields = count(array_intersect($added, $free_text_fields)) || count(array_intersect($changed, $free_text_fields));


            if (!$contains_free_text_fields) {
                $and_6_autoapproval = true;
            }
        }

        //auto approve revision for edir
        $edir_autoapproval = false;
        if($app->id == APP_ED && $status == REVISION_STATUS_APPROVED){
            $free_text_fields = array(
                'showname',
                'gender',
                'birth_date',
                'ethnicity',
                'nationality_id',
                'measure_units',
                'hair_color',
                'eye_color',
                'height',
                'weight',
                'dress_size',
                'shoe_size',
                'bust',
                'waist',
                'hip',
                'cup_size',
                'pubic_hair',
                'home_city_id',
                'about_en',
                'about_it',
                'about_fr',
                'about_de',
                'about_pt',
                'about_ro',
                'is_smoking',
                'is_drinking',
                'characteristics',
                'cuisine',
                'drink',
                'flower',
                'gifts',
                'perfume',
                'designer',
                'interests',
                'sports',
                'hobbies',
                'min_book_time_unit',
                'min_book_time',
                'has_down_pay',
                'down_pay',
                'down_pay_cur',
                'reservation',
                'langs',
                'country_id',
                'zip',
                'incall_type',
                'incall_hotel_room',
                'incall_other',
                'outcall_type',
                'outcall_other',
                'sex_orientation',
                'sex_availability',
                'services',
                'keywords',
                'additional_service_en',
                'additional_service_it',
                'additional_service_fr',
                'additional_service_de',
                'additional_service_pt',
                'additional_service_ro',
                'available_24_7',
                'night_escort',
                'times',
                'rates',
                'travel_payment_methods',
                'travel_other_method',
                'special_rates',
                'phone_country_id',
                'phone_number',
                'phone_number_alt',
                'phone_instr',
                'phone_instr_no_withheld',
                'phone_instr_other',
                'email',
                'website',
                'club_name',
                'street',
                'street_no',
                'contact_phone_parsed',
                'disable_phone_prefix',
                'address_additional_info',
                'admin_verified',
                'free_comment',
                'due_date',
                'slogan',
                'cityzones',
                'agency_id',
                'phone_exists',
                'status_comment',
                'hair_length',
                'tatoo',
                'piercing',
                'breast_type',
                'about_es',
                'about_ru',
                'additional_service_es',
                'additional_service_ru',
                'phone_additional_1',
                'phone_additional_2',
                'disable_phone_prefix_1',
                'disable_phone_prefix_2',
                'phone_instr_1',
                'phone_instr_2',
                'phone_instr_no_withheld_1',
                'phone_instr_no_withheld_2',
                'phone_instr_other_1',
                'phone_instr_other_2',
                'phone_sms_verified',
                'viber',
                'whatsapp',
                'viber_1',
                'whatsapp_1',
                'viber_2',
                'whatsapp_2',
                'prefered_contact_methods',
                'about_has_bl',
                'type',
                'phone_prefix',
                'display_address',
                'wechat',
                'telegram',
                'ssignal',
                'apps_availble',
                'contact_phone_free',
            );
            $added = $profile_changes['added'];
            $changed = $profile_changes['changed'];
            $contains_free_text_fields = count(array_intersect($added, $free_text_fields)) || count(array_intersect($changed, $free_text_fields));
            if (!$contains_free_text_fields) {
                $edir_autoapproval = true;
            }
        }


	$is_changed = false;
	foreach ( $profile_changes as $change ) {
		if ( count($change) > 0 ) {
			$is_changed = true;
		}
	}
	
	if ( ! $is_changed ) {
	    Cubix_SyncNotifier::notify($escort_id,Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, $profile_changes,$sales_user_id);
	    return $escort_id;
	}

        // TEST unserialize and send email if return false
        $serialized_data = serialize($profile);
        if (false === unserialize($serialized_data)) {
            return $escort_id;
        }

		
        if ($revision > 0 && $status == REVISION_STATUS_NOT_APPROVED) {
            $db->update('profile_updates_v2', array(
                'date_updated' => new Zend_Db_Expr('NOW()'),
                'data' => $serialized_data
            ), array(
                $db->quoteInto('escort_id = ?', $escort_id),
                $db->quoteInto('revision = ?', $revision)
            ));

            Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_REV_UPDATED, array('rev' => $revision), $sales_user_id);

        }
        else {
            $revision += 1;

            $db->insert('profile_updates_v2', array(
                'escort_id' => $escort_id,
                'revision' => $revision,
                'date_updated' => new Zend_Db_Expr('NOW()'),
                'data' => $serialized_data
            ));
            Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_REV_CREATED, array('rev' => $revision), $sales_user_id);
        }
        $_auto_approval = $db->fetchOne('SELECT auto_approval FROM escorts WHERE id = ?', $escort_id);

        if (($_auto_approval && !$backend && !$it_prefix_changed) || $and_6_autoapproval || $edir_autoapproval || $autoApprooveCities ) {
            saveRevision($profile, $revision, $escort_id);
        }
        elseif (!$it_prefix_changed && in_array(Cubix_Application::getId(), array(APP_EF, APP_BL,APP_EG_CO_UK))) {

            $profile_array = array(
                'phone_exists' => $profile['phone_exists'],
                'phone_country_id' => $profile['phone_country_id'],
                'phone_number' => $profile['phone_number'],
                'contact_phone_parsed' => $profile['contact_phone_parsed']);

            if (isset($profile['phone_sms_verified'])) {
                $profile_array['phone_sms_verified'] = $profile['phone_sms_verified'];
            }
            $db->update('escort_profiles_v2', $profile_array, $db->quoteInto('escort_id = ?', $escort_id));
            snapshotProfileV2($escort_id);
        }
        else {
            $db->update('escort_profiles_v2', array('phone_exists' => $profile['phone_exists']), $db->quoteInto('escort_id = ?', $escort_id));
        }
		
        addSeoEntityInstance($escort_id, $profile);
        Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, $profile_changes, $sales_user_id);
        $status = new Cubix_EscortStatus($escort_id, null, false, true, $sales_user_id);
        $status->save();

        if ( in_array($app->id, array(APP_ED,APP_EG_CO_UK)) && $profile['agency_id'] )
        {
            $sql = 'SELECT
                        apv.date	
                    FROM
                        `agencies_phone_verification` apv
                        INNER JOIN escorts e ON e.agency_id = apv.agency_id
                        INNER JOIN escort_profiles_v2 epv2 ON epv2.escort_id = e.id 
                        AND epv2.phone_exists = apv.phone 
                    WHERE
                        e.id = ? 
                        AND apv.STATUS = 1';
            $agency_phone_verified = $db->fetchRow($sql , array( $escort_id ) );
            if ( count($agency_phone_verified) > 0 )
            {
                $db->update('escorts', array(
                    'last_hand_verification_date' => $agency_phone_verified['date']
                ), array($db->quoteInto('id = ?', $escort_id)));
            }
        }

    }
    catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }
    return $escort_id;
}

function getAvailableApps($escort_id) {
    global $db;
    $sel = '';
    if (Cubix_Application::getId() == APP_BL) {
        $sel .= ', snapchat, snapchat_username';
    }
    if (Cubix_Application::getId() == APP_ED) {
        $sel .= ', wechat, telegram, ssignal';
    } else if (in_array(Cubix_Application::getId(), [APP_A6, APP_EG_CO_UK, APP_BL, APP_6B, APP_AE, APP_EM, APP_EF, APP_EI])) {
        $sel .= ', telegram';
        if (in_array(Cubix_Application::getId(), array(APP_EG_CO_UK,APP_BL,APP_6B,APP_AE,APP_EM)))
        {
            $sel .= ', ssignal';
        }
    }

    return $db->query("SELECT viber, whatsapp {$sel} FROM escort_profiles_v2 WHERE escort_id = ?", $escort_id)->fetch();
}


function setAvailableApps($escort_id, $viber, $whatsapp, $snapchat = null, $snapchat_username = null, $wechat = null, $telegram = null, $signal = null) {
    global $db;

    $data = array('viber' => $viber, 'whatsapp' => $whatsapp);

    if (Cubix_Application::getId() == APP_BL) {

        $data['snapchat'] = $snapchat;
        $data['snapchat_username'] = $snapchat_username;
        $data['ssignal'] = $signal;
    }

    if (Cubix_Application::getId() == APP_ED) {
        $data['wechat'] = $wechat;
        $data['telegram'] = $telegram;
        $data['ssignal'] = $signal;
    }

    if (in_array(Cubix_Application::getId(), [APP_A6, APP_EG_CO_UK, APP_BL, APP_6B, APP_AE, APP_EM, APP_EF, APP_EI])) {
        $data['telegram'] = $telegram;
        if (in_array(Cubix_Application::getId(), array(APP_EG_CO_UK,APP_6B,APP_AE,APP_EM)))
        {
            $data['ssignal'] = $signal;
        }
    }

    return $db->update('escort_profiles_v2', $data, $db->quoteInto('escort_id = ?', $escort_id));
}

function getSocialLinks($escort_id, $fields) {
    if (Cubix_Application::getId() != APP_6B) return null;

    global $db;

    return $db->query("SELECT {$fields} FROM escort_profiles_v2 WHERE escort_id = ?", $escort_id)->fetch();
}

function setSocialLinks($escort_id, $socials) {
    if (Cubix_Application::getId() != APP_6B) return null;

    global $db;

    return $db->update('escort_profiles_v2', $socials, $db->quoteInto('escort_id = ?', $escort_id));
}

function addSeoEntityInstance($escort_id, $params) {
    global $db, $app;

    $app_entity_ids = array(
        1 => 47,
        2 => 2,
        16 => 2,
        25 => 2,
        17 => 2,
        18 => 2,
        11 => 2,
        30 => 2,
        5 => 1,
        7 => 3,
        9 => 4,
        33 => 2,
        22 => 2,
        6 => 2,
    );

    $app_langs = array(
        1 => array('it', 'en'),
        2 => array('fr', 'en'),
        16 => array('de', 'fr', 'it', 'en'),
        25 => array('de', 'en'),
        17 => array('pt', 'en'),
        18 => array('fr', 'en'),
        11 => array('en', 'cn', 'my'),
        30 => array('fr', 'nl', 'en'),
        5 => array('en'),
        7 => array('en'),
        9 => array('it', 'en'),
        33 => array('en'),
        22 => array('en'),
        6 => array('de', 'fr', 'it', 'en'),
        69 => array('es', 'fr', 'it', 'en'),
    );

    //$db->query('DELETE FROM seo_entity_instances WHERE entity_id = ? AND primary_id = ?', array($app_entity_ids[$app->id], $escort_id));

    $data = array(
        'entity_id' => $app_entity_ids[$app->id],
        'primary_id' => $escort_id,
        'is_published' => 1,
        'primary_title' => $params['showname'] . '(' . $escort_id . ')'
    );

    $def_lang = $app_langs[$app->id][0];
    $about_me = substr(strip_tags($params['about_' . $def_lang]), 0, 152);

    if (Cubix_Application::getId() != APP_ED) {
        if (strlen($about_me)) {

            foreach ($app_langs[$app->id] as $lang_id) {
                $data['meta_description_' . $lang_id] = $about_me;
            }

            $seo_data = $db->fetchRow('SELECT * FROM seo_entity_instances
				WHERE entity_id = ? AND primary_id = ?',
                array($app_entity_ids[Cubix_Application::getId()], $escort_id));

            if ($seo_data) {
                $seo_entity_id = $seo_data->id;
                unset($seo_data->id);
                $seo_data = array_filter((array)$seo_data);
                $data = array_merge($seo_data, $data);

                $db->update('seo_entity_instances', $data, $db->quoteInto('id = ?', $seo_entity_id));
            }
            else {
                $db->insert('seo_entity_instances', $data);
            }

        }
    }
}

function differProfile($old, $new) {
    $old = (array)$old;
    $new = (array)$new;

    $added = array();
    $changed = array();
    $removed = array();
    foreach ($new as $field => $value) {
        if ((!isset($old[$field]) || !$old[$field]) && $value) {
            $added[] = $field;
        }
        elseif (!$value && (isset($old[$field]) && $old[$field])) {
            $removed[] = $field;
        }
        elseif ((!isset($old[$field]) || !$old[$field]) && !$value) {
            continue;
        }
        elseif ($old[$field] != $value && $field != 'phone_number') {
            $changed[] = $field;
        }
        elseif (($field == 'phone_number')  && $old[$field] !== $value) {
            $changed[] = $field;
        }
    }

    return array('added' => $added, 'changed' => $changed, 'removed' => $removed);
}

function saveRevision($profile_revision, $latest_revision, $escort_id) {
    global $db;

    updateRevision($latest_revision, REVISION_STATUS_APPROVED, $escort_id);
    applyRevision($profile_revision, $escort_id);
}

function updateRevision($revision, $status, $escort_id) {
    global $db;

    if ($status == REVISION_STATUS_APPROVED) {
        Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_REV_APPROVED, array('rev' => $revision));
        Cubix_AlertEvents::notify($escort_id, ALERT_ME_MODIFICATION_ON_PROFILE, array('rev' => $revision));

        Cubix_LatestActions::addDirectAction($escort_id, Cubix_LatestActions::ACTION_PROFILE_UPDATE);
        Cubix_LatestActions::addDirectActionDetail($escort_id, Cubix_LatestActions::ACTION_PROFILE_UPDATE, $revision);

        if (Cubix_Application::getId() == APP_ED) {
            Cubix_FollowEvents::notify($escort_id, 'escort', FOLLOW_ESCORT_NEW_PROFILE, array('rel_id' => $escort_id), true);
        }
    }
    elseif ($status == REVISION_STATUS_DECLINED) {
        Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_REV_DECLINED, array('rev' => $revision));
    }

    $db->update('profile_updates_v2', array(
        'status' => $status
    ), array(
            $db->quoteInto('escort_id = ?', $escort_id),
            $db->quoteInto('revision = ?', $revision))
    );
}

function applyRevision($data, $escort_id) {
    global $db, $app;

    if (is_object($data)) $data = (array)$data;
    if (isset($data['data'])) $data = $data['data'];

    if (array_key_exists('slogan', $data)) {
        unset($data['slogan']);
    }

    $escort_data = array(
        'showname' => $data['showname'],
        'gender' => $data['gender'],
        'country_id' => $data['country_id'],
        //'city_id' => $data['city_id'],
        'free_comment' => $data['free_comment'],
        'status_comment' => $data['status_comment'],
        'home_city_id' => $data['home_city_id'],
        'date_last_modified' => new Zend_Db_Expr('NOW()'),
        'due_date' => ($data['due_date']) ? new Zend_Db_Expr('FROM_UNIXTIME(' . $data['due_date'] . ')') : null
    );

    if ($app->id != APP_ED) {
        unset($data['prefered_contact_methods']);
    }

    if (!in_array($app->id, array(APP_EF))) {
        $escort_data['city_id'] = $data['city_id'];
    }

    $agency_id = $data['agency_id'];
    /*if ( $item->data['user_id'] ) {
		$data = array_merge($data, array('user_id' => $item->data['user_id']));
	}

	if ( $item->data['agency_id'] ) {
		$data = array_merge($data, array('agency_id' => $item->data['agency_id']));
	}*/

    $db->update('escorts', $escort_data, $db->quoteInto('id = ?', $escort_id));

    unset($data['free_comment']);
    unset($data['status_comment']);
    unset($data['agency_id']);
    unset($data['user_id']);
    unset($data['is_pornstar']);
    unset($data['bad_photo_history']);

    $db->delete('escort_langs', $db->quoteInto('escort_id = ?', $escort_id));
    // Inserting escort langs
    $langs = $data['langs'];
    if (count($langs) > 0) {
        foreach ($langs as $lang) {
            $lang['escort_id'] = $escort_id;
            $db->insert('escort_langs', $lang);
        }
    }
    unset($data['langs']);

    if (!in_array($app->id, array(APP_EF))) {
        $cc = $db->fetchAll('SELECT city_id FROM escort_cities WHERE escort_id = ?', $escort_id);
        $old_cities = array();
        if ($cc)
            foreach ($cc as $c) {
                $old_cities[] = $c['city_id'];
            }

        $db->delete('escort_cities', $db->quoteInto('escort_id = ?', $escort_id));
        // Inserting escort cities
        $cities = $data['cities'];
        if (count($cities) > 0) {
            foreach ($cities as $city) {
                $city['escort_id'] = $escort_id;
                $db->insert('escort_cities', $city);

                if (!in_array($city['city_id'], $old_cities)) {
                    Cubix_AlertEvents::notify($escort_id, ALERT_ME_CITY, array('id' => $city['city_id'], 'type' => 'working'));
                    Cubix_CityAlertEvents::notify($city['city_id'], $escort_id, NULL);
                    if (Cubix_Application::getId() == APP_ED) {
                        if ($agency_id) {
                            Cubix_FollowEvents::notify($agency_id, 'agency', FOLLOW_AGENCY_NEW_CITY, array('rel_id' => $city['city_id'], 'rel_id2' => FOLLOW_NEW_CITY_CITY, 'rel_id3' => $data['esc_id']), true);
                        }
                        Cubix_FollowEvents::notify($escort_id, 'escort', FOLLOW_ESCORT_NEW_CITY, array('rel_id' => $city['city_id'], 'rel_id2' => FOLLOW_NEW_CITY_CITY), true);
                    }
                }
            }
        }
        unset($data['cities']);


        $db->delete('escort_cityzones', $db->quoteInto('escort_id = ?', $escort_id));
        // Inserting escort cityzones
        $zones = $data['cityzones'];
        if (count($zones) > 0) {
            foreach ($zones as $zone) {
                $zone['escort_id'] = $escort_id;
                $db->insert('escort_cityzones', $zone);
            }
        }
        unset($data['cityzones']);
    }

    $db->delete('escort_services', $db->quoteInto('escort_id = ?', $escort_id));
    // Inserting escort services
    $services = $data['services'];
    if (count($services) > 0) {
        foreach ($services as $service) {
            $service['escort_id'] = $escort_id;
            $db->insert('escort_services', $service);
        }
    }
    unset($data['services']);

    $db->delete('escort_keywords', $db->quoteInto('escort_id = ?', $escort_id));
    // Inserting escort keywords
    $keywords = $data['keywords'];
    if (count($keywords) > 0) {
        foreach ($keywords as $keyword) {
            $k = array('escort_id' => $escort_id, 'keyword_id' => $keyword);
            $db->insert('escort_keywords', $k);
        }
    }
    unset($data['keywords']);

    $db->delete('escort_working_times', $db->quoteInto('escort_id = ?', $escort_id));

    // Inserting escort working times
    $times = $data['times'];
    if (count($times) > 0) {
        foreach ($times as $time) {
            $time['escort_id'] = $escort_id;
            $db->insert('escort_working_times', $time);
        }
    }
    unset($data['times']);

    // Inserting escort rates
    $rates = $data['rates'];

    if (checkRatesChanged($escort_id, $rates)) {

        $hh_rates = $db->fetchAll('SELECT availability, type, time, time_unit, hh_price FROM escort_rates WHERE escort_id = ?', $escort_id, Zend_Db::FETCH_ASSOC);
        $db->delete('escort_rates', $db->quoteInto('escort_id = ?', $escort_id));
        foreach ($rates as $rate) {
            $rate['escort_id'] = $escort_id;
            $db->insert('escort_rates', $rate);
        }

        $quote = array();
        foreach ($hh_rates as $hh_rate) {
            $quote = array(
                $db->quoteInto('escort_id = ?', $escort_id),
                $db->quoteInto('availability = ?', $hh_rate['availability']),

                $db->quoteInto('time = ?', $hh_rate['time']),
                $db->quoteInto('time_unit = ?', $hh_rate['time_unit'])
            );
            if ($hh_rate['type']) {
                $quote[] = $db->quoteInto('type = ?', $hh_rate['type']);
            }
            $db->update('escort_rates', array('hh_price' => $hh_rate['hh_price']), $quote);
        }

        if (Cubix_Application::getId() == APP_ED) {
            Cubix_FollowEvents::notify($escort_id, 'escort', FOLLOW_ESCORT_NEW_PRICE, array('rel_id' => $escort_id), true);
        }
    }

    unset($data['rates']);

    $data['escort_id'] = $escort_id;
    unset($data['id']);


    //$data['birth_date'] = strtotime('-' . $data['birth_date'] . ' year');

    $bd = is_numeric($data['birth_date']) ? date('Y-m-d', $data['birth_date']) : $data['birth_date'];

    $data['birth_date'] = $bd ? $bd/*new Zend_Db_Expr('FROM_UNIXTIME(' . $data['birth_date'] . ')')*/ : null;

	unset($data['phone']);
    unset($data['phone_prefix']);
    unset($data['phone_prefix_1']);
    unset($data['phone_prefix_2']);
    unset($data['country_id']);
    unset($data['city_id']);
    unset($data['service_currencies']);
    unset($data['home_city_id']);
    unset($data['due_date']);
    unset($data['travel_payment_methods']);
    unset($data['travel_other_method']);
    unset($data['profile_type']);
    unset($data['latitude']);
    unset($data['longitude']);


    if ($app->id == APP_EF) {
        unset($data['cities']);
        unset($data['cityzones']);
    }

    if ($app->id == APP_ED) {
        unset($data['type']);
    }


    if ($db->fetchOne('SELECT TRUE FROM escort_profiles_v2 WHERE escort_id = ?', $escort_id)) {
        $data['escort_id'] = $escort_id;

        $db->update('escort_profiles_v2', $data, $db->quoteInto('escort_id = ?', $escort_id));

        /*if ( $escort_id == 49901 ) {
			//$db->update('escort_profiles_v2', array('contact_phone_parsed' => '0037455998819'), $db->quoteInto('escort_id = ?', $escort_id));
			file_put_contents(APPLICATION_PATH . '/../logs/dump.log', print_r($data, 1), FILE_APPEND);
		}*/
    }
    else
        $db->insert('escort_profiles_v2', $data);
    unset($data['admin_verified']);

    addSeoEntityInstance($escort_id, $data);

    /*$m_snapshot = new Model_Escort_SnapshotV2(array('id' => $this->getId()));
	$m_snapshot->snapshotProfileV2();*/
    snapshotProfileV2($escort_id);
}

function getFullProfileV2($escort_id) {
    global $db, $app;
    $fields = '';

    if ($app->id == APP_BL) {
        $fields = 'is_pornstar, ';
    }

    if ($app->id == APP_ED) {
        $fields = 'ep.travel_place, e.type, ep.prefered_contact_methods, ';
    }

	if ($app->id == APP_A6) {
        $fields = 'ep.show_phone, ep.skype_id, ep.video_link, ';
    }
	
    $sql = "
		SELECT
			e.id, {$fields} e.agency_id, e.showname,  e.gender, ct.title_it AS city_title_it, ct.title_en AS city_title_en, eph.hash AS photo_hash, eph.ext AS photo_ext, e.block_website, e.check_website,
			u.application_id, ep.email, u.username, eph.status AS photo_status, " . Cubix_I18n::getTblFields('ct.title', null, null, 'base_city') . ", ct.id AS base_city_id,
			c.title_it AS country_it, c.title_en AS country_en, c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, r.title_it AS region_title_it,
			r.title_en AS region_title_en, ct.slug AS city_slug, IF(((DATE(e.vac_date_from) <= CURDATE() AND DATE(e.vac_date_to) > CURDATE()) OR DATE(e.vac_date_from) > CURDATE()),
			e.vac_date_to, 0) AS vac_date_to, ep.phone_number,ep.phone_country_id, ep.phone_number_alt, ep.phone_instr, ep.disable_phone_prefix ,ep.phone_sms_verified, ep.phone_instr_no_withheld, ep.phone_instr_other, ep.club_name, ep.website,
			ep.email, ep.zip, ep.street, ep.street_no, ep.address_additional_info, ep.available_24_7, e.home_city_id, UNIX_TIMESTAMP(ep.birth_date) AS birth_date, n.iso AS nationality_iso, " .
        Cubix_I18n::getTblFields('n.title', null, null, 'nationality_title') . ", ep.eye_color, ep.hair_color, ep.hair_length, ep.height, ep.weight, ep.bust, ep.waist, ep.hip, ep.shoe_size,
			ep.cup_size, ep.breast_type, ep.dress_size, ep.is_smoking, ep.is_drinking, ep.measure_units, ep.sex_availability, ep.ethnicity,
			ep.about_it AS about_it, ep.about_en AS about_en, ep.about_gr AS about_gr, ep.about_fr AS about_fr,	ep.about_de AS about_de, ep.about_pt AS about_pt, ep.about_nl AS about_nl, ep.about_my AS about_my,	ep.about_cn AS about_cn,
			ep.characteristics, ep.pubic_hair , ep.tatoo , ep.piercing , ep.incall_type, ep.incall_hotel_room, ep.incall_other, ep.outcall_type, ep.outcall_other, ep.sex_orientation, e.date_last_modified,
			e.date_registered, u.last_login_date, (SELECT SUM(count) FROM escort_hits_daily WHERE escort_id = e.id AND DATE_ADD(date, INTERVAL 90 DAY) >= DATE(NOW())) AS hit_count,
			". Cubix_I18n::getTblFields('ep.additional_service') . " , ep.cuisine, ep.drink, ep.flower, ep.gifts, ep.perfume, ep.designer,
			ep.interests, ep.sports, ep.hobbies, ep.min_book_time, ep.min_book_time_unit, ep.has_down_pay, ep.down_pay, ep.down_pay_cur, ep.has_down_pay, ep.down_pay, ep.down_pay_cur, ep.reservation
		FROM escort_profiles_v2 ep
		INNER JOIN escorts e ON ep.escort_id = e.id
		INNER JOIN users u ON u.id = e.user_id
		INNER JOIN applications a ON a.id = u.application_id
		LEFT JOIN nationalities n ON n.id = ep.nationality_id
		LEFT JOIN escort_working_times ewt ON ewt.escort_id = e.id
		LEFT JOIN escort_langs el ON el.escort_id = e.id
		INNER JOIN escort_cities ec ON ec.city_id = e.city_id
		INNER JOIN countries c ON c.id = e.country_id
		INNER JOIN cities ct ON ct.id = ec.city_id
		LEFT JOIN regions r ON r.id = ct.region_id
		LEFT JOIN cityzones cz ON cz.id = e.cityzone_id
		LEFT JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1
		/*LEFT JOIN escort_hits_daily ehd ON ehd.escort_id = e.id AND DATE_ADD(ehd.date, INTERVAL 90 DAY) >= DATE(NOW())*/
		WHERE e.id = ?
		 GROUP BY e.id
	";

    $profile = $db->fetchRow($sql, $escort_id, Zend_Db::FETCH_OBJ);

    /*if ( $escort_id == 30594 ) {
			//$db->update('escort_profiles_v2', array('contact_phone_parsed' => '0037455998819'), $db->quoteInto('escort_id = ?', $escort_id));
			file_put_contents(APPLICATION_PATH . '/../logs/dump.log', print_r($profile, 1), FILE_APPEND);
	}*/

    $profile->langs = $db->fetchAll('
		SELECT el.lang_id AS id, el.level, l.title FROM escort_langs el
		LEFT JOIN langs l ON l.id = el.lang_id
		WHERE el.escort_id = ?
	', array($escort_id), Zend_Db::FETCH_OBJ);

    $profile->rates = $db->fetchAll('
		SELECT availability, time, time_unit, price, currency_id, type FROM escort_rates
		WHERE escort_id = ?
	', array($escort_id), Zend_Db::FETCH_OBJ);

    $profile->travel_payment_methods = $db->fetchAll('
		SELECT payment_method, specify FROM travel_payment_methods
		WHERE escort_id = ?
	', array($escort_id), Zend_Db::FETCH_OBJ);

    $profile->tours = $db->fetchAll('
		SELECT t.id, t.date_from, t.date_to, t.phone, t.country_id,
			t.city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country_title,
			ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
			c.slug AS country_slug, r.slug AS region_slug, ct.slug AS city_slug,
			c.iso AS country_iso,
			IF(DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()), 1, 0) AS is_in_tour,
			IF(DATE(t.date_from) > DATE(NOW()), 1, 0) AS is_upcoming_tour
		FROM tours t

		INNER JOIN countries c ON c.id = t.country_id
		INNER JOIN cities ct ON ct.id = t.city_id
		INNER JOIN regions r ON r.id = ct.region_id
		WHERE t.escort_id = ? AND (DATE(t.date_from) >= DATE(NOW()) OR  DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()))
		ORDER BY t.date_from ASC
	', array($escort_id), Zend_Db::FETCH_OBJ);

    $profile->is_open = $db->fetchOne('
		SELECT
			IF ((
				ewt.day_index = ' . date('N') . '
				AND ewt.time_from <= ' . date('G') . '
				AND ewt.time_to >= ' . date('G') . '
			) OR (
				ewt.day_index = ' . date('N') . '
				AND ewt.time_from <= ' . date('G') . '
				AND ewt.time_from > ewt.time_to
			) OR (
				ewt.day_index = ' . (1 == date('N') ? 7 : date('N') - 1) . '
				AND ewt.time_to >= ' . date('G') . '
				AND ewt.time_from  > ewt.time_to
			), 1, 0)
		FROM escort_working_times ewt
		WHERE ewt.escort_id = ? AND ewt.day_index = ?
	', array($escort_id, date('N')));

    $working_times = $db->fetchAll('
		SELECT ewt.day_index, ewt.time_from AS `from`, ewt.time_from_m AS `from_m`, ewt.time_to AS `to`, ewt.time_to_m AS `to_m`
		FROM escort_working_times ewt
		WHERE ewt.escort_id = ?
	', array($escort_id), Zend_Db::FETCH_OBJ);

    $wts = array();
    foreach ($working_times as $i => $wt) {
        $wts[$wt->day_index] = $wt;
    }

    $profile->working_times = $wts;

    return $profile;
}


function snapshotProfileV2($escort_id) {
    global $db;
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    set_time_limit(0);

    $has_old = $db->fetchOne('SELECT TRUE FROM escort_profiles_snapshots_v2 WHERE escort_id = ?', array($escort_id));

    $data = array('data' => serialize(getFullProfileV2($escort_id)));

    if (!$has_old) {
        $db->insert('escort_profiles_snapshots_v2', $data + array('escort_id' => $escort_id));
    }
    else {
        $db->update('escort_profiles_snapshots_v2', $data, array($db->quoteInto('escort_id = ?', $escort_id)));
    }
}


function snapshotProfileV2ByUserId($user_id)
{
	global $db;
	set_time_limit(0);

    $escorts = $db->fetchAll('SELECT id FROM escorts WHERE user_id = ?', array($user_id));

    if ($escorts) {
        foreach ($escorts as $escort) {
            snapshotProfileV2($escort['id']);
        }
    }
}

function makeVirtualSnapshotFromRevision($escort_id) {
    global $db, $app;

    //Get last upproved or unapproved revision data
    $revision = $db->fetchOne('
		select `data` from profile_updates_v2 where escort_id = ? and revision = (select MAX(revision) from profile_updates_v2 where escort_id = ?)
	', array($escort_id, $escort_id));

    $revision = unserialize($revision);
    //return $revision;
    $common_params = array(
        'agency_id', 'showname', 'gender', 'disabled_reviews', 'block_website', 'check_website', 'phone_country_id',
        'phone_number', 'phone_number_alt', 'phone_instr', 'phone_instr_no_withheld', 'phone_instr_other',
        'disable_phone_prefix', 'club_name', 'website', 'email', 'zip', 'street', 'street_no', 'address_additional_info',
        'available_24_7', 'is_open', 'home_city_id', 'birth_date', 'eye_color', 'hair_color', 'hair_length', 'height',
        'weight', 'bust', 'waist', 'hip', 'shoe_size', 'cup_size', 'dress_size', 'is_smoking',
        'is_drinking', 'measure_units', 'sex_availability', 'ethnicity', 'characteristics', 'pubic_hair',
        'incall_type', 'incall_hotel_room', 'incall_other', 'outcall_type', 'outcall_other',
        'sex_orientation', 'special_rates', 'about_en', 'about_it', 'about_gr', 'about_fr', 'about_de',
        'date_last_modified', 'date_registered', 'hit_count',

        'additional_service_en',
        'additional_service_it',
        'additional_service_gr',
        'additional_service_fr',
        'additional_service_de'
    );

    // For escortmeetings and pinkconnection
    if ($app->id == APP_ED || $app->id == 33 || $app->id == 22) {
        $add_params = array(
            'cuisine', 'drink', 'flower', 'gifts', 'perfume', 'designer', 'interests',
            'sports', 'hobbies', 'min_book_time', 'min_book_time_unit',
            'has_down_pay', 'down_pay', 'down_pay_cur', 'reservation'
        );
        $common_params = array_merge($common_params, $add_params);
    }

    $snapshot = array();

    $escort_data = $db->fetchRow('
		SELECT
			disabled_reviews, block_website, check_website, agency_id
		FROM escorts
		WHERE id = ?
	', array($escort_id));

    $snapshot['id'] = $escort_id;

    foreach ($common_params as $param) {
        $snapshot[$param] = null;
        if (isset($revision[$param]))
            $snapshot[$param] = $revision[$param];
    }

    if (isset($revision['city_id']) && $revision['city_id']) {
        $main_city_titles = $db->fetchRow('SELECT ' . Cubix_I18n::getTblFields('title', null, null, 'city_title') . ' FROM cities WHERE id = ?', array($revision['city_id']));
        if ($main_city_titles) {
            foreach ($main_city_titles as $k => $title) {
                if ($title)
                    $snapshot[$k] = $title;
            }
        }
    }

    $snapshot['agency_id'] = $escort_data['agency_id'];

    $main_photo = $db->fetchRow('
		SELECT
			hash AS photo_hash, ext AS photo_ext, status AS photo_status
		FROM escort_photos
		WHERE escort_id = ? AND is_main = 1
	', array($escort_id));

    $snapshot['photo_hash'] = null;
    $snapshot['photo_ext'] = null;
    $snapshot['photo_status'] = null;

    if ($main_photo) {
        $snapshot['photo_hash'] = $main_photo['photo_hash'];
        $snapshot['photo_ext'] = $main_photo['photo_ext'];
        $snapshot['photo_status'] = $main_photo['photo_status'];
    }

    $snapshot['application_id'] = $app->id;

    if (isset($revision['user_id']))
        $snapshot['username'] = $db->fetchOne('SELECT username FROM users WHERE id = ?', array($revision['user_id']));

    if (isset($revision['city_id']) && $revision['city_id']) {
        $base_city_titles = $db->fetchRow('SELECT ' . Cubix_I18n::getTblFields('title', null, null, 'base_city') . ' FROM cities WHERE id = ?', array($revision['city_id']));
        if ($base_city_titles) {
            foreach ($base_city_titles as $k => $title) {
                if ($title)
                    $snapshot[$k] = $title;
            }
        }

        $snapshot['base_city_id'] = $revision['city_id'];

        $snapshot['city_slug'] = $db->fetchOne('SELECT slug AS city_slug FROM cities WHERE id = ?', array($revision['city_id']));
    }

    if (isset($revision['country_id']) && $revision['country_id']) {
        $country_titles = $db->fetchRow('SELECT ' . Cubix_I18n::getTblFields('title', null, null, 'country') . ' FROM countries WHERE id = ?', array($revision['country_id']));
        if ($country_titles) {
            foreach ($country_titles as $k => $title) {
                if ($title)
                    $snapshot[$k] = $title;
            }
        }

        $country_data = $db->fetchRow('
			SELECT
				c.slug AS country_slug, c.iso AS country_iso,
				r.slug AS region_slug, r.id AS region_id
			FROM countries c
			LEFT JOIN regions r ON r.country_id = c.id
			WHERE c.id = ?
		', array($revision['country_id']));

        if ($country_data) {
            $snapshot['country_slug'] = $country_data['country_slug'];
            $snapshot['country_iso'] = $country_data['country_iso'];
            $snapshot['region_slug'] = $country_data['region_slug'];
        }

        if (isset($country_data['region_id'])) {
            $region_titles = $db->fetchRow('
				SELECT ' . Cubix_I18n::getTblFields('title', null, null, 'region_title') . '
				FROM regions
				WHERE id = ?
			', array($country_data['region_id']));

            if ($region_titles) {
                foreach ($region_titles as $k => $title) {
                    if ($title)
                        $snapshot[$k] = $title;
                }
            }
        }
    }

    $snapshot['vac_date_to'] = 0;

    $snapshot['nationality_iso'] = null;
    if (isset($revision['nationality_id'])) {
        $snapshot['nationality_iso'] = $db->fetchOne('SELECT iso AS nationality_iso FROM nationalities WHERE id = ?', array($revision['nationality_id']));

        $nationality_titles = $db->fetchRow('SELECT ' . Cubix_I18n::getTblFields('title', null, null, 'nationality_title') . ' FROM nationalities WHERE id = ?', array($revision['nationality_id']));

        foreach ($nationality_titles as $k => $title) {
            $snapshot[$k] = $title;
        }
    }

    if (count($revision['langs'])) {
        foreach ($revision['langs'] as $lang) {
            $snapshot['langs'][] = array('id' => $lang['lang_id'], 'level' => $lang['level'], 'title' => $db->fetchOne('SELECT title FROM langs WHERE id = ?', array($lang['lang_id'])));
        }
    }

    if (count($revision['rates'])) {
        foreach ($revision['rates'] as $rate) {
            $snapshot['rates'][] = array(
                'availability' => $rate['availability'],
                'time' => $rate['time'],
                'time_unit' => $rate['time_unit'],
                'price' => $rate['price'],
                'currency_id' => $rate['currency_id'],
                'type' => (isset($rate['type'])) ? $rate['type'] : null
            );
        }
    }

    if (count($revision['services'])) {
        foreach ($revision['services'] as $service) {
            unset($service['escort_id']);
            $snapshot['services'][] = $service;
        }
    }

    if (count($revision['keywords'])) {
        foreach ($revision['keywords'] as $keyword) {
            unset($keyword['escort_id']);
            $snapshot['keywords'][] = $keyword;
        }
    }

    if (count($revision['times'])) {
        foreach ($revision['times'] as $time) {
            unset($time['escort_id']);
            $snapshot['working_times'][$time['day_index']] = array(
                'day_index' => $time['day_index'],
                'from' => $time['time_from'],
                'from_m' => $time['time_from_m'],
                'to' => $time['time_to'],
                'to_m' => $time['time_to_m']
            );
        }
    }

    // Special for and6
    if ($app->id == APP_A6 || $app->id == APP_A6_AT) {
        if ($snapshot['agency_id']) {
            $sql = '
				SELECT
					fc.' . Cubix_I18n::getTblField('title') . ' AS city_title,
					c.' . Cubix_I18n::getTblField('title') . ' AS area_title,
					ag.zip AS zip, ag.address AS street, ag.name AS studio_name,
					ag.slug AS agency_slug,ag.is_anonymous AS is_anonymous,
					ag.email AS a_email, ag.phone AS a_phone, ag.phone_instructions AS a_phone_instructions,
					ag.block_website AS a_block_website, ag.phone_country_id AS a_phone_country_id,
					ag.web AS a_web
				FROM agencies ag
				LEFT JOIN f_cities fc ON fc.id = ag.fake_city_id
				LEFT JOIN cities c ON c.id = ag.city_id
				WHERE ag.id = ?
			';
            $snapshot['agency_contacts'] = $db->fetchRow($sql, array($snapshot['agency_id']));
        }

        $snapshot['esc_contacts'] = $db->fetchRow('
			SELECT
				e.fake_zip AS zip, fc.' . Cubix_I18n::getTblField('title') . ' AS city_title
			FROM escorts e
			LEFT JOIN f_cities fc ON fc.id = e.fake_city_id
			WHERE e.id = ?
		', array($snapshot['id']));
    }

    return $snapshot;
}


function hasAgencyEscort($agency_id, $escort_id) {
    global $db;

    return $agency_id == $db->fetchOne('SELECT agency_id FROM escorts WHERE id = ?', $escort_id);
}


/***************************************************************************************************/
function premium_getAgencyPremiumEscorts($agency_id, $lang_id = 'en') {
    $agency_id = intval($agency_id);

    global $db, $app;

    $where = ' ';

    if ($app->id == APP_A6) {
        $where = ' AND p.id IN (12, 14, 19, 20, 23, 25) ';
    }

    $escorts = $db->fetchAll('
		SELECT p.name AS package, p.id as package_id, e.id, e.showname, DATEDIFF(op.expiration_date, NOW()) AS expires, op.id AS opid, DATE(op.expiration_date)
		FROM escorts e
		INNER JOIN order_packages op ON op.escort_id = e.id
		INNER JOIN order_package_products opp ON op.id = opp.order_package_id
		INNER JOIN packages p ON p.id = op.package_id
		WHERE
			e.agency_id = ? AND
			op.status = ? AND
			op.application_id = ? AND
			op.order_id IS NOT NULL AND
            e.is_suspicious = 0 AND opp.product_id = 1
            ' . $where . '
		GROUP BY e.id
	', array($agency_id, 2, $app->id));

    foreach ($escorts as $i => $escort) {
        if (premium_getEscortCitiesLimit($escort['id']) > 0) {
            if ($app->id != APP_A6 ) {
                $escorts[$i]['cities'] = $db->fetchAll('
					SELECT c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
					FROM premium_escorts pe
					INNER JOIN cities c ON pe.city_id = c.id
					WHERE pe.order_package_id = ?
				', array($escort['id'], $escort['opid']));
            }
            else {
                $escorts[$i]['cities'] = $db->fetchAll('
					SELECT c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
					FROM escort_cities ec
					INNER JOIN cities c ON ec.city_id = c.id
					WHERE ec.escort_id = ?
				', array($escort['id'], $escort['id']));
            }
        }

        $escorts[$i]['working_cities'] = $db->fetchAll('
			SELECT c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
			FROM escort_cities ec
			INNER JOIN cities c ON ec.city_id = c.id
			WHERE ec.escort_id = ?
		', array($escort['id'], $escort['id']));
    }

    return $escorts;
}

function premium_getEscortsWithGotd($agency_id, $lang = 'en') {
    $agency_id = intval($agency_id);
    global $db, $app;

    $result = $db->fetchAll('
		SELECT
			DATE(op.gotd_activation_date) AS activation_date, op.gotd_city_id AS city_id, c.title_' . $lang . ' AS city_title, e.showname, CONCAT("opid-", op.id) AS id
		FROM order_packages op
		INNER JOIN escorts e ON e.id = op.escort_id
		INNER JOIN cities c ON c.id = op.gotd_city_id
		WHERE
			e.agency_id = ' . $agency_id . ' AND
			op.status = 2 AND
			op.application_id = ' . $app->id . ' AND
			op.order_id IS NOT NULL AND
			op.gotd_status IS NOT NULL AND
			op.gotd_status <> 3
		UNION ALL
		SELECT
			DATE(go.activation_date) AS activation_date, go.city_id, c.title_' . $lang . ' AS city_title, e.showname, CONCAT("goid-", go.id) AS id
		FROM gotd_orders go
		INNER JOIN order_packages op ON op.id = go.order_package_id
		INNER JOIN escorts e ON e.id = go.escort_id
		INNER JOIN cities c ON c.id = go.city_id
		WHERE
			e.agency_id = ' . $agency_id . ' AND
			op.status = 2 AND
			op.application_id = ' . $app->id . ' AND
			op.order_id IS NOT NULL AND
			go.status <> 1 AND
			DATE(go.activation_date) >= DATE(NOW())
	');

    return $result;
}

function premium_getEscortsForGotdSwitch($agency_id, $selected) {
    $agency_id = intval($agency_id);
    global $db, $app;

    $selected = explode('-', $selected);
    if ($selected[0] == "goid") {
        $selected_gotd = $db->fetchRow('SELECT DATE(activation_date) AS activation_date, escort_id FROM gotd_orders WHERE id = ?', array($selected[1]));
    }
    else {
        $selected_gotd = $db->fetchRow('SELECT DATE(gotd_activation_date) AS activation_date, escort_id FROM order_packages WHERE id = ?', array($selected[1]));
    }

    $result = $db->fetchAll('
		SELECT
			DATE(op.gotd_activation_date) AS gotd_activation_date, op.gotd_status,
			op.gotd_city_id, e.showname, e.id
		FROM order_packages op
		INNER JOIN escorts e ON e.id = op.escort_id
		LEFT JOIN gotd_orders go ON go.order_package_id = op.id
		WHERE
			e.agency_id = ? AND
			op.status = ? AND
			op.application_id = ? AND
			op.order_id IS NOT NULL AND
			DATE(?) >= DATE(op.date_activated) AND DATE(?) <= DATE(op.expiration_date) AND
			e.id <> ? AND
			(op.gotd_activation_date IS NULL OR DATE(op.gotd_activation_date) <> DATE(?) ) AND
			(go.activation_date IS NULL OR (DATE(go.activation_date) <> DATE(?) AND go.status <> 1))
		GROUP BY e.id
	', array($agency_id, 2, $app->id, $selected_gotd['activation_date'], $selected_gotd['activation_date'], $selected_gotd['escort_id'], $selected_gotd['activation_date'], $selected_gotd['activation_date']));


    return $result;
}


function premium_switchGotd($from_id, $to_id) {
    global $db, $app;

    $from_id = explode('-', $from_id);
    $type = $from_id[0];
    $from_id = $from_id[1];

    $target_package = premium_getEscortActivePackage($to_id, 'en');

    try {
        $db->beginTransaction();

        if ($type == "goid") {// Case when sourc gotd in gotd_orders table
            $selected_gotd = $db->fetchRow('SELECT DATE(activation_date) AS activation_date, city_id, escort_id FROM gotd_orders WHERE id = ?', array($from_id));

            //Inserting new row in gotd_orders for existing gotd and removing from order_packages
            if (!is_null($target_package['gotd_status']) && $target_package['gotd_status'] != 3) {

                $db->insert('gotd_orders', array(
                    'creation_date' => date('Y-m-d H:i:s', time()),
                    'activation_date' => date('Y-m-d', strtotime($target_package['gotd_activation_date'])),
                    'city_id' => $target_package['gotd_city_id'],
                    'status' => 2,
                    'escort_id' => $to_id,
                    'order_package_id' => $target_package['opid']
                ));

                $db->query('UPDATE order_packages SET gotd_status = NULL, gotd_activation_date = NULL, gotd_city_id = NULL WHERE id = ?', array($target_package['opid']));
                $db->query('DELETE FROM order_package_products WHERE product_id = ? AND order_package_id = ?', array(15, $target_package['opid']));
            }

            //Updating row changing escort_id and order_package_id
            $db->query('UPDATE gotd_orders SET escort_id = ?, order_package_id = ? WHERE id = ?', array($to_id, $target_package['opid'], $from_id));
        }
        else {// Case when source gotd in order_packages table
            $selected_gotd = $db->fetchRow('SELECT DATE(gotd_activation_date) AS activation_date, gotd_city_id AS city_id, escort_id FROM order_packages WHERE id = ?', array($from_id));

            //Remove fields from order_packages table and inserting new row in gotd_orders table
            $db->query('UPDATE order_packages SET gotd_status = NULL, gotd_activation_date = NULL, gotd_city_id = NULL WHERE id = ?', array($from_id));
            $db->query('DELETE FROM order_package_products WHERE product_id = ? AND order_package_id = ?', array(15, $from_id));

            $db->insert('gotd_orders', array(
                'creation_date' => date('Y-m-d H:i:s', time()),
                'activation_date' => $selected_gotd['activation_date'],
                'city_id' => $selected_gotd['city_id'],
                'status' => 2,
                'escort_id' => $to_id,
                'order_package_id' => $target_package['opid']
            ));

        }


        $db->commit();
    } catch (Exception $e) {
        $db->rollBack();
        throw $e;
        return false;
    }
    return true;
}

function premium_getAgencyNonPremiumEscorts($agency_id) {
    $agency_id = intval($agency_id);

    global $db, $app;

    $escorts = $db->fetchAll('
		SELECT e.id, e.showname
		FROM escorts e
		WHERE e.agency_id = ? AND e.is_suspicious = 0 AND e.id NOT IN (
			SELECT e.id
			FROM escorts e
			INNER JOIN order_packages op ON op.escort_id = e.id
			WHERE
			e.agency_id = ? AND
			op.status IN (1,2) AND
			op.application_id = ? AND
			op.order_id IS NOT NULL
		) AND NOT e.status & ' . ESCORT_STATUS_DELETED . '
		ORDER BY e.showname
	', array($agency_id, $agency_id, $app->id));

    return $escorts;
}

function getLinkedAgencies($agency_id) {
    global $db, $app;

    ini_set('memory_limit', '2024M');
    //return array();
    $sql = '
		SELECT a.id, a.name
		FROM linked_agencies l_a
		INNER JOIN agencies a ON l_a.agency2 = a.id
		WHERE l_a.agency1 = ?
	';

    return $db->fetchAll($sql, $agency_id);
}

function premium_getLinkedAgencyNonPremiumEscorts($agency_id) {
    global $db, $app;

    $agency_id = intval($agency_id);

    //$model = new Cubix_Api_Module_Agencies();
    $linked_agencies = getLinkedAgencies($agency_id);

    $linked_ids = array($agency_id);
    foreach ($linked_agencies as $l_agency) {
        $linked_ids[] = $l_agency['id'];
    }

    $escorts = $db->fetchAll('
		SELECT e.id, CONCAT(e.showname,"(", a.name, ")") AS showname
		FROM escorts e
		INNER JOIN agencies a ON e.agency_id = a.id
		WHERE e.agency_id IN ( ' . implode(',', $linked_ids) . ' ) AND e.is_suspicious = 0 AND e.id NOT IN (
			SELECT e.id
			FROM escorts e
			INNER JOIN order_packages op ON op.escort_id = e.id
			WHERE
			e.agency_id IN ( ' . implode(',', $linked_ids) . ' ) AND
			op.status IN (1,2) AND
			op.application_id = ? AND
			op.order_id IS NOT NULL
		) AND e.status & ' . ESCORT_STATUS_ACTIVE . ' AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . '
		ORDER BY e.showname
	', array($app->id));

    return $escorts;
}

function premium_getEscortWorkingCities($escort_id) {
    global $db;

    $escort_id = intval($escort_id);

    $working = $db->fetchAll('
		SELECT
			ec.city_id AS id
		FROM escort_cities ec
		WHERE ec.escort_id = ?
	', array($escort_id));

    foreach ($working as $i => $city) {
        $working[$i] = $city['id'];
    }

    return $working;
}

function premium_getEscortCities($escort_id, $lang_id) {
    global $db;

    $escort_id = intval($escort_id);

    $working = $db->fetchAll('
		SELECT
			c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
		FROM escort_cities ec
		INNER JOIN cities c ON ec.city_id = c.id
		WHERE ec.escort_id = ?
	', array($escort_id, $escort_id));

    $result = array('working' => $working);

    $cities_limit = premium_getEscortCitiesLimit($escort_id);
    if ($cities_limit > 0) {
        $package = premium_getEscortActivePackage($escort_id, $lang_id);
        $result['premium'] = $package['cities'];
    }

    return $result;
}
function premium_getEscortActiveOrderPackageId($escort_id, $lang_id) {
    $escort_id = intval($escort_id);

    global $db, $app;

    $package = $db->fetchRow('
		SELECT op.id AS opid FROM order_packages op
		WHERE
			op.escort_id = ? AND
			op.status = ? AND
			op.application_id = ?
	', array($escort_id, 2, $app->id));

    if (!$package) return 0;

    return $package['opid'];
}
function premium_getEscortActivePackageForPremiumCities($escort_id, $lang_id) {
    $escort_id = intval($escort_id);
    // if ($package = API_Static_Cache::get('active_package_' . $escort_id)) {
    //     return $package;
    // }

    global $db, $app;

    $package = $db->fetchRow('
		SELECT p.name AS package, DATEDIFF(op.expiration_date, NOW()) AS expires, op.id AS opid, op.gotd_city_id, op.gotd_activation_date, op.gotd_status,
		op.order_id, op.package_id, op.date_activated, op.expiration_date
		FROM order_packages op
		INNER JOIN packages p ON p.id = op.package_id
		WHERE
			op.escort_id = ? AND
			op.status = ? AND
			op.application_id = ?
	', array($escort_id, 2, $app->id));

    if (!$package) return null;

    $package['products'] = $db->fetchAll('
		SELECT p.id, p.name
		FROM order_package_products opp
		INNER JOIN products p ON p.id = opp.product_id
		WHERE opp.order_package_id = ?
	', array($package['opid']));

    $package['cities'] = $db->fetchAll('
		SELECT c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
		FROM premium_escorts pe
		INNER JOIN cities c ON pe.city_id = c.id
		WHERE pe.order_package_id = ?
	', array($escort_id, $package['opid']));

    $package['all_cities'] = $db->fetchAll('
		SELECT c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
		FROM escort_cities ec
		INNER JOIN cities c ON ec.city_id = c.id
		WHERE ec.escort_id = ?
	', array($escort_id, $escort_id));

    API_Static_Cache::set('active_package_' . $escort_id, $package);

    return $package;
}
function premium_getEscortActivePackage($escort_id, $lang_id) {
    $escort_id = intval($escort_id);
    // if ($package = API_Static_Cache::get('active_package_' . $escort_id)) {
    //     return $package;
    // }

    global $db, $app;

    $package = $db->fetchRow('
		SELECT p.name AS package, DATEDIFF(op.expiration_date, NOW()) AS expires, op.id AS opid, op.gotd_city_id, op.gotd_activation_date, op.gotd_status,
		op.order_id, op.package_id, op.date_activated, op.expiration_date
		FROM order_packages op
		INNER JOIN packages p ON p.id = op.package_id
		WHERE
			op.escort_id = ? AND
			op.status = ? AND
			op.application_id = ? AND
			op.order_id IS NOT NULL
	', array($escort_id, 2, $app->id));

    if (!$package) return null;

    $package['products'] = $db->fetchAll('
		SELECT p.id, p.name
		FROM order_package_products opp
		INNER JOIN products p ON p.id = opp.product_id
		WHERE opp.order_package_id = ?
	', array($package['opid']));

    $package['cities'] = $db->fetchAll('
		SELECT c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
		FROM premium_escorts pe
		INNER JOIN cities c ON pe.city_id = c.id
		WHERE pe.order_package_id = ?
	', array($escort_id, $package['opid']));

    $package['all_cities'] = $db->fetchAll('
		SELECT c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
		FROM escort_cities ec
		INNER JOIN cities c ON ec.city_id = c.id
		WHERE ec.escort_id = ?
	', array($escort_id, $escort_id));

    API_Static_Cache::set('active_package_' . $escort_id, $package);

    return $package;
}

function premium_getEscortActiveMonthlyPackageForEguk($escort_id, $lang_id) {
    $escort_id = intval($escort_id);
    // if ($package = API_Static_Cache::get('active_package_' . $escort_id)) {
    //     return $package;
    // }

    global $db, $app;

    $package = $db->fetchRow('
		SELECT p.name AS package, DATEDIFF(op.expiration_date, NOW()) AS expires, op.id AS opid, op.gotd_city_id, op.gotd_activation_date, op.gotd_status,
		op.order_id, op.package_id, op.date_activated, op.expiration_date
		FROM order_packages op
		INNER JOIN packages p ON p.id = op.package_id
		WHERE
			op.escort_id = ? AND
			op.status = ? AND
			op.application_id = ? AND
			op.package_id = 106
	', array($escort_id, 2, $app->id));

    if (!$package) return null;

    $package['products'] = $db->fetchAll('
		SELECT p.id, p.name
		FROM order_package_products opp
		INNER JOIN products p ON p.id = opp.product_id
		WHERE opp.order_package_id = ?
	', array($package['opid']));

    $package['cities'] = $db->fetchAll('
		SELECT c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
		FROM premium_escorts pe
		INNER JOIN cities c ON pe.city_id = c.id
		WHERE pe.order_package_id = ?
	', array($escort_id, $package['opid']));

    $package['all_cities'] = $db->fetchAll('
		SELECT c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
		FROM escort_cities ec
		INNER JOIN cities c ON ec.city_id = c.id
		WHERE ec.escort_id = ?
	', array($escort_id, $escort_id));

    API_Static_Cache::set('active_package_' . $escort_id, $package);

    return $package;
}

function premium_getEscortActivePackageForED($escort_id, $lang_id) {
    $escort_id = intval($escort_id);
    if ($package = API_Static_Cache::get('active_package_' . $escort_id)) {
        return $package;
    }

    global $db, $app;

    $package = $db->fetchRow('
		SELECT p.name AS package, DATEDIFF(op.expiration_date, NOW()) AS expires, op.id AS opid, op.gotd_city_id, op.gotd_activation_date, op.gotd_status,
		op.order_id, op.package_id, op.date_activated, op.expiration_date
		FROM order_packages op
		INNER JOIN packages p ON p.id = op.package_id
		WHERE
			op.escort_id = ? AND
			op.status = ? AND
			op.application_id = ? 
	', array($escort_id, 2, $app->id));

    if (!$package) return null;

    $package['products'] = $db->fetchAll('
		SELECT p.id, p.name
		FROM order_package_products opp
		INNER JOIN products p ON p.id = opp.product_id
		WHERE opp.order_package_id = ?
	', array($package['opid']));

    $package['cities'] = $db->fetchAll('
		SELECT c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
		FROM premium_escorts pe
		INNER JOIN cities c ON pe.city_id = c.id
		WHERE pe.order_package_id = ?
	', array($escort_id, $package['opid']));

    $package['all_cities'] = $db->fetchAll('
		SELECT c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
		FROM escort_cities ec
		INNER JOIN cities c ON ec.city_id = c.id
		WHERE ec.escort_id = ?
	', array($escort_id, $escort_id));

    API_Static_Cache::set('active_package_' . $escort_id, $package);

    return $package;
}

function premium_updatePremiumCities($op_id, $cities, $escort_id) {
    global $db, $app;

    try {
        $db->query('DELETE FROM premium_escorts WHERE order_package_id = ?', $op_id);

        if (count($cities) > 0) {
            foreach ($cities as $c) {
                $db->insert('premium_escorts', array('order_package_id' => $op_id, 'city_id' => $c));
            }
        }

        Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('action' => 'premium city change', 'cities' => implode(',', $cities)));
    } catch (Exception $e) {

    }
}

function premium_getEscortCitiesLimit2($escort_id) {
    global $db, $app;

    if ($app->id == APP_A6 || $app->id == APP_A6_AT) return 1;
	
	if($app->id == APP_EG_CO_UK){

		$package = premium_getEscortActivePackageForPremiumCities($escort_id, 'en');
	}else{
		$package = premium_getEscortActivePackage($escort_id, 'en');
	}


    $limit = 0;
    if ($package) {
        foreach ($package['products'] as $product) {
            if ($product['id'] == PREMIUM_PRODUCT_CITY_SPOT) {
                $limit++;
            }
            elseif ($product['id'] == PREMIUM_PRODUCT_ADDITIONAL_CITY_1) {
                $limit += 1;
            }
            elseif ($product['id'] == PREMIUM_PRODUCT_ADDITIONAL_CITY_2) {
                $limit += 2;
            }
            elseif ($product['id'] == PREMIUM_PRODUCT_ADDITIONAL_CITY_3) {
                $limit += 3;
            }
        }
    }

    return $limit;
}

function premium_getEscortCitiesLimit($escort_id) {
    global $db, $app;

    if ($app->id == APP_A6 ) return 1;

    $package = premium_getEscortActivePackage($escort_id, 'en');

    $limit = 0;
    if ($package) {
        foreach ($package['products'] as $product) {
            if ($product['id'] == PREMIUM_PRODUCT_CITY_SPOT) {
                $limit++;
            }
            elseif ($product['id'] == PREMIUM_PRODUCT_ADDITIONAL_CITY) {
                $limit += 3;
            }
        }
    }

    return $limit;
}

function premium_getEscortCitiesLimitForED($escort_id) {
    global $db, $app;

    $package = premium_getEscortActivePackageForED($escort_id, 'en');

    $limit = 0;
    if ($package) {
        foreach ($package['products'] as $product) {
            if ($product['id'] == PREMIUM_PRODUCT_CITY_SPOT) {
                $limit++;
            }
            elseif ($product['id'] == PREMIUM_PRODUCT_ADDITIONAL_CITY) {
                $limit += 3;
            }
            elseif ($product['id'] == PREMIUM_PRODUCT_ADDITIONAL_CITY_1) {
                $limit += 1;
            }
            elseif ($product['id'] == PREMIUM_PRODUCT_ADDITIONAL_CITY_2) {
                $limit += 2;
            }
            elseif ($product['id'] == PREMIUM_PRODUCT_ADDITIONAL_CITY_3) {
                $limit += 3;
            }
        }
    }

    return $limit;
}

define('PREMIUM_PRODUCT_CITY_SPOT', 5);
define('PREMIUM_PRODUCT_ADDITIONAL_CITY', 9);
define('PREMIUM_PRODUCT_ADDITIONAL_CITY_1', 11);
define('PREMIUM_PRODUCT_ADDITIONAL_CITY_2', 12);
define('PREMIUM_PRODUCT_ADDITIONAL_CITY_3', 13);

function premium_switchActivePackages($from_id, $to_id, $cities, $backend_user_id = null)
{
	if ( ! $from_id ) {
		throw new Exception('$from_id is required parameter');
	}
	
    global $db, $app;


    if ($app->id == APP_BL) {
        $source_package = getEscortActivePackage($from_id, 'en', true);
    }
    else {
        $source_package = premium_getEscortActivePackage($from_id, 'en');
	}

    if($app->id == APP_EG_CO_UK && !$source_package) {
    	// Monthly Package
    	$source_package = premium_getEscortActiveMonthlyPackageForEguk($from_id, 'en');
    }

	if($app->id == APP_EG_CO_UK && $source_package && $source_package['package'] == 'MonthlyPlus') {
		$to_escort = $db->fetchRow('SELECT agency_id, gender, country_id FROM escorts WHERE id = ?', $to_id);

		if($to_escort['gender'] != 1 || $to_escort['country_id'] == 216) {
			return array('success' => false, 'error' => 'Monthly Plus package can moved only into female escorts. '); // and !us
		}

	}

	if($app->id == APP_EG_CO_UK && $source_package && $source_package['package'] == 'Monthly Package') {
		$to_escort = $db->fetchRow('SELECT agency_id, gender, country_id FROM escorts WHERE id = ?', $to_id);

		if($to_escort['gender'] != 1 || $to_escort['country_id'] == 216) {
			return array('success' => false, 'error' => 'Monthly package can moved only into female escorts. '); // and !us
		}

	}

    $target_package = premium_getEscortActivePackage($to_id, 'en');

    if ($app->id != APP_A6 && $app->id != APP_EG_CO_UK) {
        $cities_limit = premium_getEscortCitiesLimit($from_id);

        foreach ($cities as $i => $city) {
            $cities[$i] = intval($city);
            if (!$cities[$i]) unset($cities[$i]);
        }

        if (count($cities) > $cities_limit) {
            return array('success' => false, 'error' =>'Cities limit is exceeded! Max: ' . $cities_limit . ', Actual: ' . count($cities) );
        }
		elseif($cities_limit > 0 && count($cities) == 0){
			return array('success' => false, 'error' => 'you have to select at least one Premium city ');
		}
    }
	
    // The case when agency wants to change only premium cities of escort
    if (false) {
//		try {
//			$db->beginTransaction();
//
//			if ( $cities_limit > 0 ) {
//				$wcities = premium_getEscortWorkingCities($from_id);
//
//				$db->delete('premium_escorts', array($db->quoteInto('order_package_id = ?', $source_package['opid'])));
//
//				foreach ( $cities as $city_id ) {
//					$city_id = intval($city_id);
//					if ( ! in_array($city_id, $wcities) ) {
//						continue;
//					}
//
//					$db->insert('premium_escorts', array(
//						'order_package_id' => $source_package['opid'],
//						'city_id' => $city_id
//					));
//				}
//			}
//
//			$db->commit();
//		}
//		catch ( Exception $e ) {
//			$db->rollback();
//			throw $e;
//		}

        return true;
    }
    // The case when agency wants to switch packages of two escorts
    // (source->target, target->source, at the same time)
    elseif ($source_package && $target_package) {
        throw new Exception('Switch has not been implemented yet');
    }
    // Case when agency wants to assign a package from an escort to another one,
    // which doesn't have any active package (we ignore the default package)
    elseif (($from_id == $to_id) || ($source_package && !$target_package)) { 
        try {
            $db->beginTransaction();

            // Select the active default package
            $active_opid = $db->fetchOne('SELECT id FROM order_packages WHERE escort_id = ? AND status = 2 AND order_id IS NULL', $to_id);

            if ($active_opid) {
                // Cancel the active default packages
                $db->query('UPDATE order_packages SET status = 4 WHERE id = ?', $active_opid);
            }

            //Assign "Zero Package" OR "Monthly" package for APP_ED(EDIR-2119)
            if ($app->id != APP_A6) {
                if ($app->id == APP_BL) {
                    //Do not assign any package after package moved
                    //assignPackage($from_id, 9);
                }
                elseif ($app->id == APP_EG_CO_UK) {
                	$from_escort = $db->fetchRow('SELECT agency_id, gender, country_id FROM escorts WHERE id = ?', $from_id);

                    $count = $db->fetchOne('
						SELECT
							COUNT(*) as count
						FROM escorts e
						INNER JOIN order_packages op ON op.escort_id = e.id
						INNER JOIN packages p ON op.package_id = p.id
						WHERE e.agency_id = ? AND op.status = ? AND p.id = ?
					', array($from_escort['agency_id'], 2, 106));

                	if($from_escort['gender'] != 1 || $from_escort['country_id'] == 216) {
                	    assignPackage($from_id, 43);
                	} elseif ($count < 3) {
                        //Assign 'Monthly Package' package
                        assignPackage($from_id, 106);
                    }
                    else {
                        //Assign 'Minimum' package
                        assignPackage($from_id, 105);
                    }

                    
                }
                // assign "Monthly package" (NOTE ticket EDIR-2119)
                elseif ($app->id == APP_ED) {
                    $from_escort = $db->fetchRow('SELECT agency_id, gender, country_id FROM escorts WHERE id = ?', $from_id);

                    $count = $db->fetchOne('
						SELECT
							COUNT(*) as count
						FROM escorts e
						INNER JOIN order_packages op ON op.escort_id = e.id
						INNER JOIN packages p ON op.package_id = p.id
						WHERE e.agency_id = ? AND op.status = ? AND p.id = ?
					', array($from_escort['agency_id'], 2, 128));

                    if($from_escort['gender'] != 1 || $from_escort['country_id'] == 68) {
                    	assignPackage($from_id, 102);
                    } elseif ($count < 3) {
                        //Assign 'Monthly Package' package
                        assignPackage($from_id, 128);
                    }
                    else {
                        //Assign 'Minimum' package
                        assignPackage($from_id, 125);
                    }

                }
                else {
                    assignPackage($from_id, 97);
                }

            }


            // Get old inactive default package
            /*$old_inactive_opid = $db->fetchOne('SELECT MAX(id) FROM order_packages WHERE escort_id = ? AND status <> 2 AND order_id IS NULL', $from_id);
			if ( $old_inactive_opid ) {
				// Activate it
				$db->query('UPDATE order_packages SET status = 2 WHERE id = ?', $old_inactive_opid);
			}*/

            // And finally give the package to another escort


            //Sometimes happens that unique of order_id-package_id-escort_id is break
            //It happens when both escorts packages are bought with one order, and then they try to move package between these escorts
            //In that case we should prevent package movement
            /*--->*/
            $is_dublicate = $db->fetchOne('SELECT TRUE FROM order_packages WHERE order_id = ? AND package_id = ? AND escort_id = ?', array($source_package['order_id'], $source_package['package_id'], $to_id));
            if ($is_dublicate) {
            	$fake_id_to_swap_package = 1;
            	$db->query('UPDATE order_packages SET escort_id = ? WHERE escort_id = ? AND id = ?', array($fake_id_to_swap_package, $from_id, $source_package['opid'])); 
            	$db->query('UPDATE order_packages SET escort_id = ? WHERE escort_id = ? AND order_id = ?', array($from_id, $to_id, $source_package['order_id'])); 
				$db->query('UPDATE order_packages SET escort_id = ? WHERE escort_id = ? AND id = ?', array($to_id, $fake_id_to_swap_package, $source_package['opid'])); 
                //return array('success' => false, 'error' => 'You can\'t move package between these escorts. They have the same order.');
            }else{
            	$db->query('UPDATE order_packages SET escort_id = ? WHERE escort_id = ? AND id = ?', array($to_id, $from_id, $source_package['opid']));
            }
            /*<---*/

            
            $db->query('UPDATE gotd_orders SET escort_id = ? WHERE order_package_id = ?', array($to_id, $source_package['opid']));

            if ($app->id == APP_A6) {
                $db->query('UPDATE additional_areas SET escort_id = ? WHERE escort_id = ? AND order_package_id = ?', array($to_id, $from_id, $source_package['opid']));
            }

            if ($app->id == APP_EG_CO_UK) {
                $db->delete('premium_escorts', array($db->quoteInto('order_package_id = ?', $source_package['opid'])));

                $wcities = premium_getEscortWorkingCities($to_id);

                foreach ($wcities as $working_city) {

                    $db->insert('premium_escorts', array(
                        'order_package_id' => $source_package['opid'],
                        'city_id' => $working_city
                    ));
                }
            }

            if ($app->id != APP_A6 && $app->id != APP_EG_CO_UK) {
                // Delete all cities
                $db->delete('premium_escorts', array($db->quoteInto('order_package_id = ?', $source_package['opid'])));

                // If there is a limit on cities that means that this package has
                // at least city premium spot product

                if ($cities_limit > 0) {
                    $wcities = premium_getEscortWorkingCities($to_id);

                    foreach ($cities as $city_id) {
                        $city_id = intval($city_id);
                        if (!in_array($city_id, $wcities)) {
                            continue;
                        }

                        $db->insert('premium_escorts', array(
                            'order_package_id' => $source_package['opid'],
                            'city_id' => $city_id
                        ));
                    }
                }
            }

            if ($app->id == APP_6A) {
                $latest_rev = $db->fetchOne('SELECT MAX(revision) FROM profile_updates_v2 WHERE escort_id = ?', $to_id);
                $db->query('UPDATE profile_updates_v2 SET status = ? WHERE revision = ? AND escort_id = ?', array(REVISION_STATUS_NOT_APPROVED, $latest_rev, $to_id));

                $m_status = new Cubix_EscortStatus($to_id, null, false, false);
                $m_status->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_PROFILE_CHANGED);
                $m_status->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
                $m_status->save();
            }

            $db->insert('packages_moving_history', array(
                'order_package_id' => $source_package['opid'],
                'from_escort_id' => $from_id,
                'to_escort_id' => $to_id,
                'backend_user_id' => $backend_user_id,
                'date' => new Zend_Db_Expr('NOW()')
            ));

            $db->update('escorts', array(
                'active_package_id' => null,
                'active_package_name' => null,
                'package_activation_date' => null,
                'package_expiration_date' => null,
                'date_last_modified' => new Zend_Db_Expr('NOW()')
            ),
                $db->quoteInto('id = ?', $from_id)
            );

            $db->update('escorts', array(
                'active_package_id' => $source_package['package_id'],
                'active_package_name' => $source_package['package'],
                'package_activation_date' => $source_package['date_activated'],
                'package_expiration_date' => $source_package['expiration_date'],
                'date_last_modified' => new Zend_Db_Expr('NOW()')
            ),
                $db->quoteInto('id = ?', $to_id)
            );

            Cubix_SyncNotifier::notify($from_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED);
            Cubix_SyncNotifier::notify($to_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED);

            Cubix_SyncNotifier::notify($from_id, Cubix_SyncNotifier::EVENT_ED_LISTING_PRODUCT_DELETED);
            Cubix_SyncNotifier::notify($to_id, Cubix_SyncNotifier::EVENT_ED_LISTING_PRODUCT_ADDED);

            $db->commit();
        } catch (Exception $e) {
            $db->rollback();
            throw $e;
        }

        return true;
    }
    else {
        throw new Exception('Both escorts does not have any active packages');
    }
}


function premium_switchActivePackagesForED($from_id, $to_id, $cities, $backend_user_id = null) {
    if (!$from_id) {
        throw new Exception('$from_id is required parameter');
    }

    if (!$to_id) {
        throw new Exception('$to_id is required parameter');
    }

    global $db, $app;

    // source_package must be paid package or default 'Monthly Package' and
    // target_package must be not paid default packages 'Monthly Package' or 'Minimum'
    $source_package = premium_getEscortActivePackageForED($from_id, 'en');
    $target_package = premium_getEscortActivePackage($to_id, 'en');

    // there are order_id is not null (paid) order_packages with default packages ??
    if ($source_package && (!$target_package || in_array($target_package['package'], array('Minimum', 'Monthly Package')))) {
        try {
            $db->beginTransaction();

            if (!in_array($source_package['package'], array('Monthly Package', 'MonthlyPlus A', 'MonthlyPlus B'))) {
                $cities_limit = premium_getEscortCitiesLimitForED($from_id);

                foreach ($cities as $i => $city) {
                    $cities[$i] = intval($city);
                    if (!$cities[$i]) unset($cities[$i]);
                }

                if (count($cities) > $cities_limit) {
                    $msg = 'Cities limit is exceeded! Max: ' . $cities_limit . ', Actual: ' . count($cities);
                    $result = array("success" => false, "message" => $msg);
                    return $result;
                }
            }

            // there are order_id is not null (paid) order_packages with default packages ??
            if ($target_package && in_array($target_package['package'], array('Minimum', 'Monthly Package'))) {
                // Select the active default package
                $active_opid = $db->fetchOne('SELECT id FROM order_packages WHERE escort_id = ? AND status = 2', $to_id);
            }
            else {
                $active_opid = $db->fetchOne('SELECT id FROM order_packages WHERE escort_id = ? AND status = 2 AND order_id IS NULL', $to_id);
            }


            if ($active_opid) {
                // Cancel the active default packages
                $db->query('UPDATE order_packages SET status = 4 WHERE id = ?', $active_opid);
            }

            $agency_id = $db->fetchOne('SELECT agency_id FROM escorts WHERE id = ?', $from_id);

            $count = $db->fetchOne('
				SELECT
					COUNT(*) as count
				FROM escorts e
				INNER JOIN order_packages op ON op.escort_id = e.id
				INNER JOIN packages p ON op.package_id = p.id
				WHERE e.agency_id = ? AND op.status = ? AND p.id = ?
			', array($agency_id, 2, 128));

            if ($count < 3) {
                //Assign 'Monthly Package' package
                assignPackage($from_id, 128);
                $from_escort_active_package_id = 128;
                $from_escort_active_package_name = 'Monthly Package';
            }
            else {
                //Assign 'Minimum' package
                assignPackage($from_id, 125);
                $from_escort_active_package_id = 125;
                $from_escort_active_package_name = 'Minimum';
            }

            // And finally give the package to another escort

            //Sometimes happens that unique of order_id-package_id-escort_id is break
            //It happens when both escorts packages are bought with one order, and then they try to move package between these escorts
            //In that case we should prevent package movement
            /*--->*/
            $is_dublicate = $db->fetchOne('SELECT TRUE FROM order_packages WHERE order_id = ? AND package_id = ? AND escort_id = ?', array($source_package['order_id'], $source_package['package_id'], $to_id));
            if ($is_dublicate) {
                return array('success' => false, 'error' => 'You can\'t move package between these escorts. They have the same order.');
            }
            /*<---*/

            $db->query('UPDATE order_packages SET escort_id = ? WHERE escort_id = ? AND id = ?', array($to_id, $from_id, $source_package['opid']));


            if (!in_array($source_package['package'], array('Monthly Package', 'MonthlyPlus A', 'MonthlyPlus B'))) {

                $premiumCitiesCount1 = $db->fetchOne('SELECT COUNT(*) FROM premium_escorts WHERE order_package_id = ?', $source_package['opid']);

                // Delete all cities
                $db->delete('premium_escorts', array($db->quoteInto('order_package_id = ?', $source_package['opid'])));

                // If there is a limit on cities that means that this package has
                // at least city premium spot product

                if ($cities_limit > 0) {
                    $wcities = premium_getEscortWorkingCities($to_id);

                    foreach ($cities as $city_id) {
                        $city_id = intval($city_id);
                        if (!in_array($city_id, $wcities)) {
                            continue;
                        }

                        $db->insert('premium_escorts', array(
                            'order_package_id' => $source_package['opid'],
                            'city_id' => $city_id
                        ));
                    }

                    $premiumCitiesCount2 = $db->fetchOne('SELECT COUNT(*) FROM premium_escorts WHERE order_package_id = ?', $source_package['opid']);

                    if ($premiumCitiesCount1 && !$premiumCitiesCount2) {
                        $db->rollback();
                        $result = array("success" => false, "message" => "Escort working cities don't exist in package cities!");
                        return $result;
                    }

                }

            }

            $db->insert('packages_moving_history', array(
                'order_package_id' => $source_package['opid'],
                'from_escort_id' => $from_id,
                'to_escort_id' => $to_id,
                'backend_user_id' => $backend_user_id,
                'date' => new Zend_Db_Expr('NOW()')
            ));

            $db->update('escorts', array(
                'active_package_id' => $from_escort_active_package_id,
                'active_package_name' => $from_escort_active_package_name,
                'package_activation_date' => new Zend_Db_Expr('NOW()'),
                'package_expiration_date' => null,
                'date_last_modified' => new Zend_Db_Expr('NOW()')
            ),
                $db->quoteInto('id = ?', $from_id)
            );

            $db->update('escorts', array(
                'active_package_id' => $source_package['package_id'],
                'active_package_name' => $source_package['package'],
                'package_activation_date' => $source_package['date_activated'],
                'package_expiration_date' => $source_package['expiration_date'],
                'date_last_modified' => new Zend_Db_Expr('NOW()')
            ),
                $db->quoteInto('id = ?', $to_id)
            );

            Cubix_SyncNotifier::notify($from_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED);
            Cubix_SyncNotifier::notify($to_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED);

            Cubix_SyncNotifier::notify($from_id, Cubix_SyncNotifier::EVENT_ED_LISTING_PRODUCT_DELETED);
            Cubix_SyncNotifier::notify($to_id, Cubix_SyncNotifier::EVENT_ED_LISTING_PRODUCT_ADDED);

            $db->commit();
        } catch (Exception $e) {
            $db->rollback();
            // return $e->getMessage();

            throw $e;
        }

        $result = array("success" => true, "message" => "Package moved correctly!");
        return $result;
    }
    else {
        throw new Exception('Both escorts does not have any active packages');
    }
}


function assignPackage($escort_id, $package_id) {
    global $db;

    $app_id = Cubix_Application::getId();

    $db->insert('order_packages', array('escort_id' => $escort_id, 'package_id' => $package_id, 'application_id' => $app_id, 'status' => 2, 'date_activated' => new Zend_Db_Expr('NOW()')));
    $order_package_id = $db->lastInsertId();

    $packages_products = $db->fetchAll('SELECT product_id, is_optional FROM package_products WHERE package_id = ?', $package_id);

    foreach ($packages_products as $product) {
        $db->insert('order_package_products', array('order_package_id' => $order_package_id, 'product_id' => $product['product_id'], 'is_optional' => $product['is_optional'], 'price' => 0));
    }
}

/***************************************************************************************************/


/* -------------- METHODS CONCERNING USERS DATA MANAGEMENT -------------------*/

function updateUserData($user_id, $data) {
    global $db;
    $db->update('users', $data, $db->quoteInto('id = ?', $user_id));
}

function updateUserPassword($user_id, $password, $new_password) {
    global $db;

    $salt = _getUserPasswordSalt($user_id);

    $password = Cubix_Salt::hashPassword($password, $salt);

    $is_valid = (bool)$db->fetchOne('
		SELECT TRUE FROM users
		WHERE id = ? AND password = ?
	', array($user_id, $password));

    if (!$is_valid) {
        return false;
    }

    $new_password = Cubix_Salt::hashPassword($new_password, $salt);

    $db->update('users', array(
        'password' => $new_password
    ), $db->quoteInto('id = ?', $user_id));

    return true;
}

function updateUserRecieveNewsletters($user_id, $flag, $ip = null) {
    global $db;

    try {

        $m_n_i = new Cubix_Newsman_Ids();
        $n_ids = $m_n_i->get(Cubix_Application::getId());

        if (count($n_ids)) {
            $list_id = reset(array_keys($n_ids));
            $conf = $m_n_i->getConf();

			if(!$ip){
				$ip = Cubix_Geoip::getIP();
			}
			
            $client = new Cubix_Newsman_Client($conf['user_id'], $conf['api_key']);
            $client->setCallType("rest");
            $client->setApiUrl("https://ssl.nzsrv.com/api");

            $email = $db->fetchOne('SELECT email FROM users WHERE id = ?', $user_id);
			
            if ($email) {
                if ($flag) {
                    $subscriber_id = $client->subscriber->saveSubscribe($list_id, $email, '', '', $ip, array());
                }
                else {
                    $subscriber_id = $client->subscriber->saveUnsubscribe($list_id, $email, $ip);
                }
				
            }
        }

        $db->update('users', array(
            'recieve_newsletters' => $flag
        ), $db->quoteInto('id = ?', $user_id));
    } catch (Zend_XmlRpc_Client_FaultException $e) {
        return $e->getMessage();
    }
}

function updatePreferedLang($user_id, $lang_id) {
    global $db;
    if (is_array($user_id)) $user_id = reset($user_id);
    if (is_array($lang_id)) $lang_id = reset($lang_id);

    $db->update('users', array('lang' => $lang_id), $db->quoteInto('id = ?', $user_id));
}

function _getUserPasswordSalt($user_id) {
    global $db;

    $salt = $db->fetchOne('
		SELECT salt_hash FROM users WHERE id = ?
	', array($user_id));

    return $salt;
}

function existsByEmail($email, $user_id) {
    global $db;

    return $db->fetchOne('
		SELECT TRUE FROM users WHERE email = ? AND id <> ?
	', array($email, $user_id));
}


/* -------------- METHODS CONCERNING TOURS MANAGEMENT -------------------*/
function getEscortTours($escort_id, $lang_id, $agency_id = 0, $page = 0, $per_page = 0, $is_old_tour = false) {
    global $db;

    $escort_id = intval($escort_id);
    $agency_id = intval($agency_id);
    $date_filter = $is_old_tour ? "DATE(t.date_to) < CURDATE()" : "((DATE(t.date_from) <= CURDATE() AND DATE(t.date_to) >= CURDATE()) OR DATE(t.date_from) > CURDATE())";
    $limit_filter = ($page > 0 && $per_page > 0) ? " ORDER BY e.showname LIMIT " . ($page - 1) * $per_page . ", " . $per_page : "";
    $tours = $db->fetchAll('
		SELECT
			SQL_CALC_FOUND_ROWS
			e.id AS escort_id, e.showname, t.id, c.title_' . $lang_id . ' AS country, c.id AS country_id, ct.title_' . $lang_id . ' AS city, ct.id AS city_id,
			UNIX_TIMESTAMP(t.date_from) AS date_from, UNIX_TIMESTAMP(t.date_to) AS date_to, t.phone, t.email
		FROM tours t
		INNER JOIN escorts e ON e.id = t.escort_id
		INNER JOIN countries c ON c.id = t.country_id
		INNER JOIN cities ct ON ct.id = t.city_id
		WHERE 1 ' . ($escort_id > 0 ? ' AND t.escort_id = ' . $escort_id : '') . ($agency_id > 0 ? ' AND e.agency_id = ' . $agency_id . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED : '') . '
		AND ' . $date_filter . $limit_filter
    );
    $count_tours = $db->fetchOne('SELECT FOUND_ROWS()');

    return array($tours, $count_tours);
}

function getEscortToursV2($escort_id, $lang_id, $agency_id = 0, $page = 0, $per_page = 0, $is_old_tour = false) {
    global $db;

    $escort_id = intval($escort_id);
    $agency_id = intval($agency_id);
    $date_filter = $is_old_tour ? "DATE(t.date_to) < CURDATE()" : "((DATE(t.date_from) <= CURDATE() AND DATE(t.date_to) >= CURDATE()) OR DATE(t.date_from) > CURDATE())";
    $limit_filter = ($page > 0 && $per_page > 0) ? " ORDER BY t.date_from, t.minutes_from ASC LIMIT " . ($page - 1) * $per_page . ", " . $per_page : "";
    $tours = $db->fetchAll('
		SELECT
			SQL_CALC_FOUND_ROWS
			e.id AS escort_id, e.showname, t.id, c.title_' . $lang_id . ' AS country, c.id AS country_id, ct.title_' . $lang_id . ' AS city, ct.id AS city_id,
			UNIX_TIMESTAMP(t.date_from) AS date_from, UNIX_TIMESTAMP(t.date_to) AS date_to, t.phone, t.email, t.minutes_from, t.minutes_to
		FROM tours t
		INNER JOIN escorts e ON e.id = t.escort_id
		INNER JOIN countries c ON c.id = t.country_id
		INNER JOIN cities ct ON ct.id = t.city_id
		WHERE 1 ' . ($escort_id > 0 ? ' AND t.escort_id = ' . $escort_id : '') . ($agency_id > 0 ? ' AND e.agency_id = ' . $agency_id . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED : '') . '
		AND ' . $date_filter . $limit_filter
    );
    $count_tours = $db->fetchOne('SELECT FOUND_ROWS()');

    return array($tours, $count_tours);
}

function updateTourHistory($user_id, $value) {
    global $db;

    $user_id = intval($user_id);
    $value = intval($value);

    $db->update('users', array('disable_tour_history' => $value), $db->quoteInto('id = ?', $user_id));
}

function getTourHistory($user_id) {
    global $db;

    $user_id = intval($user_id);

    return $db->fetchOne('SELECT disable_tour_history FROM users WHERE id = ?', $user_id);
}

function getEscortTour($tour_id, $lang_id) {
    global $db;

    $tour_id = intval($tour_id);

    $tour = $db->fetchRow('
		SELECT
			e.id AS escort_id, e.showname, t.id, c.title_' . $lang_id . ' AS country, c.id AS country_id, ct.title_' . $lang_id . ' AS city, ct.id AS city_id,
			UNIX_TIMESTAMP(t.date_from) AS date_from, UNIX_TIMESTAMP(t.date_to) AS date_to, t.phone, t.email
		FROM tours t
		INNER JOIN escorts e ON e.id = t.escort_id
		INNER JOIN countries c ON c.id = t.country_id
		INNER JOIN cities ct ON ct.id = t.city_id
		WHERE t.id = ' . $tour_id . '
	');

    return $tour;
}

function getEscortTourV2($tour_id, $lang_id) {
    global $db;

    $tour_id = intval($tour_id);

    $tour = $db->fetchRow('
		SELECT
			e.id AS escort_id, e.showname, t.id, c.title_' . $lang_id . ' AS country, c.id AS country_id, ct.title_' . $lang_id . ' AS city, ct.id AS city_id,
			UNIX_TIMESTAMP(t.date_from) AS date_from, UNIX_TIMESTAMP(t.date_to) AS date_to, t.phone, t.email, t.minutes_from, t.minutes_to
		FROM tours t
		INNER JOIN escorts e ON e.id = t.escort_id
		INNER JOIN countries c ON c.id = t.country_id
		INNER JOIN cities ct ON ct.id = t.city_id
		WHERE t.id = ' . $tour_id . '
	');

    return $tour;
}

function addEscortTour($escort_id, $data) {
    global $db;

    $data['escort_id'] = $escort_id;
    $date_from = $data['date_from'];
    $date_to = $data['date_to'];
    $data['date_from'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $data['date_from'] . ')');
    $data['date_to'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $data['date_to'] . ')');

    $db->insert('tours', $data);
    $tour_id = $db->lastInsertId();

    Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_TOUR_ADDED, array(
        'id' => $tour_id
    ));

    Cubix_LatestActions::addDirectAction($escort_id, Cubix_LatestActions::ACTION_TOUR_UPDATE);
    Cubix_LatestActions::addDirectActionDetail($escort_id, Cubix_LatestActions::ACTION_TOUR_UPDATE, $tour_id);

    $date = date('d F, Y', $date_from) . ' - ' . date('d M, Y', $date_to);
    Cubix_AlertEvents::notify($escort_id, ALERT_ME_CITY, array('id' => $data['city_id'], 'type' => 'tour', 'date' => $date));

    if (Cubix_Application::getId() == APP_ED) {
        $agency_id = $db->fetchOne('SELECT e.agency_id FROM escorts e WHERE e.id = ?', $escort_id);

        if ($agency_id) {
            Cubix_FollowEvents::notify($agency_id, 'agency', FOLLOW_AGENCY_NEW_CITY, array('rel_id' => $data['city_id'], 'rel_id2' => FOLLOW_NEW_CITY_TOUR, 'rel_id3' => $escort_id), true);
        }
        Cubix_FollowEvents::notify($escort_id, 'escort', FOLLOW_ESCORT_NEW_CITY, array('rel_id' => $data['city_id'], 'rel_id2' => FOLLOW_NEW_CITY_TOUR), true);
    }

    return true;
}

function updateEscortTour($escort_id, $tour_id, $data) {
    global $db;

    $data['escort_id'] = $escort_id;
    $date_from = $data['date_from'];
    $date_to = $data['date_to'];
    $data['date_from'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $data['date_from'] . ')');
    $data['date_to'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $data['date_to'] . ')');
    if (in_array(Cubix_Application::getId(), array(APP_EF, APP_6A, APP_BL, APP_AE, APP_6B, APP_6C, APP_EG, APP_EG_CO_UK))) {
        $data['minutes_from'] = isset($data['minutes_from']) ? $data['minutes_from'] : null;
        $data['minutes_to'] = isset($data['minutes_to']) ? $data['minutes_to'] : null;
    }
    $old_city_id = $db->fetchOne('SELECT city_id FROM tours WHERE id = ?', $tour_id);

    $db->update('tours', $data, array($db->quoteInto('id = ?', $tour_id)));

    Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_TOUR_UPDATED, array(
        'id' => $tour_id
    ));

    Cubix_LatestActions::addDirectAction($escort_id, Cubix_LatestActions::ACTION_TOUR_UPDATE);
    Cubix_LatestActions::addDirectActionDetail($escort_id, Cubix_LatestActions::ACTION_TOUR_UPDATE, $tour_id);

    if ($old_city_id != $data['city_id']) {
        $date = date('d F, Y', $date_from) . ' - ' . date('d M, Y', $date_to);
        Cubix_AlertEvents::notify($escort_id, ALERT_ME_CITY, array('id' => $data['city_id'], 'type' => 'tour', 'date' => $date));
        if (Cubix_Application::getId() == APP_ED) {
            $agency_id = $db->fetchOne('SELECT e.agency_id FROM escorts e WHERE e.id = ?', $escort_id);

            if ($agency_id) {
                Cubix_FollowEvents::notify($agency_id, 'agency', FOLLOW_AGENCY_NEW_CITY, array('rel_id' => $data['city_id'], 'rel_id2' => FOLLOW_NEW_CITY_TOUR, 'rel_id3' => $escort_id), true);
            }
            Cubix_FollowEvents::notify($escort_id, 'escort', FOLLOW_ESCORT_NEW_CITY, array('rel_id' => $data['city_id'], 'rel_id2' => FOLLOW_NEW_CITY_TOUR), true);
        }
    }

    return true;
}

function removeEscortTour($escort_id, $tour_id) {
    global $db;

    if ($escort_id != $db->fetchOne('SELECT escort_id FROM tours WHERE id = ?', $tour_id)) return;
    $db->delete('tours', $db->quoteInto('id = ?', $tour_id));

    Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_TOUR_DELETED, array(
        'id' => $tour_id
    ));

    return true;
}

function getEscortPhotosList($escort_id, $only_privates, $gallery_id = null) {
    global $db, $app;

    $where = '';
    if ($app->id == APP_A6 || $app->id == APP_BL || $app->id == APP_EF) {
        $where = ' AND ep.type <> 5 AND ep.type <> 4 ';
    }

    if ($only_privates) {
        $where .= ' AND ep.type = 3';
    }
    else {
        $where .= ' AND ep.type <> 3';
    }

    if (!is_null($gallery_id)) {
        $where .= ' AND ep.gallery_id = ' . $gallery_id;
    }
    $ordering = 'ORDER BY ep.ordering ASC, ep.id ASC';
    if ($app->id == APP_EF) {
        $ordering = 'ORDER BY ep.is_main DESC, ep.ordering ASC, ep.id ASC';
    }
    $photos = $db->query("
		SELECT " . Cubix_Application::getId() . " AS application_id, ep.* FROM escort_photos ep
		WHERE ep.escort_id = ? {$where} {$ordering}
	", $escort_id)->fetchAll();

    return $photos;
}

function getEscortPhotosByOrdering($escort_id) {
    global $db, $app;

    $where = '';
    if ($app->id == APP_A6 || $app->id == APP_BL || $app->id == APP_EF) {
        $where = ' AND type <> 5 AND type <> 4 ';
    }

    $ordering = ' ORDER BY is_main DESC, type ASC, ordering ASC ';

    return $db->query("SELECT * FROM escort_photos WHERE escort_id = ? {$where}{$ordering}", $escort_id)->fetchAll();
}


function getEscortPhotoIds($escort_id) {
    global $db;

    $photos = $db->query("
		SELECT ep.id FROM escort_photos ep
		WHERE ep.escort_id = ?
	", $escort_id)->fetchAll();

    return $photos;
}

function getEscortArchivedPhotosList($escort_id) {
    global $db;

	$where = '';
	$where .= ' AND ep.type = 5';

    /*if ( $only_privates ) {
		$where .= ' AND ep.type = 3';
	}
	else {
		$where .= ' AND ep.type <> 3';
	}*/

    $photos = $db->query("
		SELECT " . Cubix_Application::getId() . " AS application_id, ep.* FROM escort_photos ep
		WHERE ep.escort_id = ? {$where} ORDER BY ep.ordering ASC
	", $escort_id)->fetchAll();

    return $photos;
}

function getPhotoGalleries($escort_id, $gallery_id = null) {
    global $db;
    try {
        $where = "";
        if ($gallery_id) {
            $where = " AND id <> " . $gallery_id;
        }
        $galleries = $db->fetchAll("
			SELECT eg.id, eg.title FROM escort_galleries eg
			WHERE eg.escort_id = ? " . $where . " ORDER BY eg.id
		", $escort_id);

        if (count($galleries) == 0) {
            for ($i = 2; $i <= 5; $i++) {
                $gallery = array('escort_id' => $escort_id, 'title' => 'Gallery ' . $i);
                $db->insert('escort_galleries', $gallery);
                $gallery['id'] = $db->lastInsertId();
                $galleries[] = $gallery;
            }
        }
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }
    return $galleries;

}

function getPhotoGalleriesED($escort_id, $gallery_id = null) {
    global $db;
    try {
        $where = "";
        if ($gallery_id) {
            $where = " AND id <> " . $gallery_id;
        }
        $galleries = $db->fetchAll("
			SELECT eg.id, eg.title, eg.is_main, eg.status FROM escort_galleries eg
			WHERE eg.escort_id = ? " . $where . " ORDER BY eg.id
		", $escort_id);

        if (count($galleries) == 0) {
            for ($i = 1; $i <= 3; $i++) {
                $gallery = array('escort_id' => $escort_id, 'title' => 'Gallery ' . $i);
                if ($i == 1) {
                    $gallery['is_main'] = 1;
                }
                $db->insert('escort_galleries', $gallery);
                $gallery['id'] = $db->lastInsertId();
                $galleries[] = $gallery;
            }
        }

        foreach ($galleries as &$gallery) {
            if ($gallery['is_main']) {
                $gallery['fake_id'] = 0;
            }
            else {
                $gallery['fake_id'] = $gallery['id'];
            }
            unset($gallery['is_main']);
        }
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }
    return $galleries;

}

function getApprovedPhotos($escort_id) {
    global $db;

    $photos = $db->query("
		SELECT ep.id FROM escort_photos ep
		INNER JOIN escorts e ON ep.escort_id = e.id AND e.is_suspicious = 0
		WHERE ep.is_approved = 1  AND ep.escort_id = ? AND ( ep.type = 1 OR ep.type = 2 )
	", $escort_id)->fetchAll();

    return $photos;
}

function changeGalleryName($escort_id, $gallery_id, $gallery_name) {
    global $db;

    $check = $db->fetchOne("SELECT TRUE FROM escort_galleries WHERE id = ? AND escort_id = ? ", array($gallery_id, $escort_id));

    if ($check) {
        $db->update('escort_galleries', array('title' => $gallery_name), $db->quoteInto('id = ?', $gallery_id));
        Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array(
            'gallery_name' => $gallery_name,
            'action' => 'gallery name changed'
        ));
    }
    return $check;
}

function changeGallery($escort_id, $photo_id, $gallery_id, $type = null) {
    global $db;
    $check = true;
    if ($gallery_id != 0) {
        $check = $db->fetchOne("SELECT TRUE FROM escort_galleries WHERE id = ? AND escort_id = ? ", array($gallery_id, $escort_id));
    }

    if ($check) {
        $photo_data = array('gallery_id' => $gallery_id);
        if ($type) {
            $photo_data['type'] = $type;
        }
        $db->update('escort_photos', $photo_data, $db->quoteInto('id = ?', $photo_id));

        if ($type) {
            $status = new Cubix_EscortStatus($escort_id);
            $status->save();
        }

        Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array(
            'id' => $photo_id,
            'gallery_id' => $photo_data['gallery_id'],
            'action' => 'gallery changed'
        ));
    }
    return $check;
}

function setGalleryStatus($escort_id, $gallery_id, $status) {
    global $db;
    $check = true;
    $check = $db->fetchOne("SELECT TRUE FROM escort_galleries WHERE id = ? AND escort_id = ? ", array($gallery_id, $escort_id));

    if ($check) {
        $db->update('escort_galleries', array('status' => $status), $db->quoteInto('id = ?', $gallery_id));
        Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_PHOTO_UPDATED, array(
            'gallery_id' => $gallery_id,
            'action' => 'status changed'
        ));
    }
    return $gallery_id;
}

function getGalleryStatus($gallery_id) {
    global $db;
    return $db->fetchOne('SELECT status FROM escort_galleries WHERE id = ?', array($gallery_id));
}

function getEscortsSearch($showname) {
    global $db, $app;

    return $db->query("SELECT showname AS name FROM escorts WHERE showname LIKE '" . $showname . "%' AND status & 32 ORDER BY showname ASC")->fetchAll();
}

function getMembersSearch($username) {
    global $db, $app;

    return $db->query("SELECT username AS name FROM users WHERE username LIKE '" . $username . "%' AND status = 1 ORDER BY username ASC")->fetchAll();
}

function getAgenciesSearch($name) {
    global $db, $app;

    return $db->query("
		SELECT a.name AS name
		FROM agencies a
		LEFT JOIN users u ON a.user_id = u.id
		WHERE a.name LIKE '" . $name . "%' AND u.status = 1 AND LENGTH(a.name) > 0
		ORDER BY username ASC
	")->fetchAll();
}

function getReviews($page, $per_page, $filter, $sort_field, $sort_dir, $lng) {
    global $db, $app;

    $sql = '
		SELECT
			e.id AS escort_id, u.username, UNIX_TIMESTAMP(u.date_registered) AS date_registered, e.showname, UNIX_TIMESTAMP(r.creation_date) AS creation_date, r.looks_rating,
			r.services_rating, r.is_fake_free, c.title_' . $lng . ' AS city_title,  a.id AS agency_id, a.agency_slug AS agency_slug, a.name AS agency_name, r.fuckometer, m.is_premium, m.reviews_count
		FROM reviews r
		INNER JOIN users u ON r.user_id = u.id
		INNER JOIN members m ON r.user_id = m.user_id
		INNER JOIN escorts e ON e.id = r.escort_id
		LEFT JOIN agencies a ON e.agency_id = a.id
		LEFT JOIN cities c ON c.id = r.city
		WHERE 1
	';

    $countSql = '
		SELECT COUNT(DISTINCT(r.id))
		FROM reviews r
		INNER JOIN users u ON r.user_id = u.id
		INNER JOIN escorts e ON e.id = r.escort_id
		LEFT JOIN cities c ON c.id = r.city
		WHERE 1
	';

    $where = ' AND r.application_id = ' . $app->id . ' AND r.status = 2 AND r.is_deleted = 0';

    if (is_array($filter)) {
        if (isset($filter['city_id']) && strlen($filter['city_id']))
            $where .= ' AND r.city = ' . $filter['city_id'];

        if (isset($filter['member_name']) && strlen($filter['member_name']))
            $where .= " AND u.username LIKE '" . $filter['member_name'] . "%'";

        if (isset($filter['username']) && strlen($filter['username']))
            $where .= " AND u.username = '" . $filter['username'] . "'";

        if (isset($filter['showname']) && strlen($filter['showname']))
            $where .= " AND e.showname LIKE '" . $filter['showname'] . "%'";
    }

    $sql .= $where;
    $countSql .= $where;

    $sql .= '
		GROUP BY r.id
		ORDER BY ' . $sort_field . ' ' . $sort_dir . '
		LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
	';

    $count = intval($db->fetchOne($countSql));
    $items = $db->query($sql)->fetchAll();

    return array($items, $count);
}

function getEscortsReviews($page, $per_page, $filter, $sort_field, $sort_dir, $lng) {
    global $db, $app;

    $sql = '
		SELECT
			e.showname, c.title_' . $lng . ' AS city_title, a.name AS agency_name, AVG(looks_rating) AS avg_looks, AVG(services_rating) AS avg_services, e.reviews AS reviews
		FROM reviews r
		LEFT JOIN escorts e ON e.id = r.escort_id
		LEFT JOIN agencies a ON e.agency_id = a.id
		LEFT JOIN escort_cities e_c ON e_c.escort_id = e.id
		LEFT JOIN cities c ON c.id = e_c.city_id
		WHERE 1
	';

    $countSql = '
		SELECT COUNT(DISTINCT(e.id))
		FROM reviews r
		LEFT JOIN escorts e ON e.id = r.escort_id
		WHERE 1
	';

    $where = ' AND r.application_id = ' . $app->id . ' AND r.status = 2 AND r.is_deleted = 0';

    if (is_array($filter)) {
        if (isset($filter['showname']) && strlen($filter['showname']))
            $where .= " AND e.showname LIKE '" . $filter['showname'] . "%'";

        if (isset($filter['country_id']) && strlen($filter['country_id']))
            $where .= ' AND e.country_id = ' . $filter['country_id'];
    }

    $sql .= $where;
    $countSql .= $where;

    $sql .= '
		GROUP BY e.id
		ORDER BY ' . $sort_field . ' ' . $sort_dir . '
		LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
	';

    $count = intval($db->fetchOne($countSql));

    $items = $db->query($sql)->fetchAll();

    return array($items, $count);
}

function getAgencies_old($page, $per_page, $filter, $sort_field, $sort_dir, $lng) {
    global $db, $app;

    $sql = '
		SELECT
			a.name AS agency_name, c.title_' . $lng . ' AS country_title, COUNT(e.id) AS escorts
		FROM agencies a
		LEFT JOIN escorts e ON a.id = e.agency_id AND e.status & 32
		LEFT JOIN users u ON u.id = a.user_id
		LEFT JOIN countries c ON c.id = a.country_id
		WHERE 1
	';

    /*$countSql = '
		SELECT COUNT(DISTINCT(a.id))
		FROM agencies a
		LEFT JOIN escorts e ON a.id = e.agency_id
		LEFT JOIN users u ON u.id = a.user_id
		WHERE 1
	';*/

    $countSql = $sql;

    $where = ' AND u.status = 1 AND LENGTH(a.name) > 0';

    if (is_array($filter)) {
        if (isset($filter['name']) && strlen($filter['name']))
            $where .= " AND a.name LIKE '" . $filter['name'] . "%'";

        if (isset($filter['country_id']) && strlen($filter['country_id']))
            $where .= ' AND a.country_id = ' . $filter['country_id'];
    }

    $sql .= $where;
    $countSql .= $where;

    $sql .= '
		GROUP BY a.id HAVING COUNT(e.id) > 0
		ORDER BY ' . $sort_field . ' ' . $sort_dir . '
		LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
	';

    $countSql .= ' GROUP BY a.id HAVING COUNT(e.id) > 0';

    //$count = intval($db->fetchOne($countSql));
    $items = $db->query($sql)->fetchAll();
    $count = count($db->query($countSql)->fetchAll());

    return array($items, $count);
}

function getAgencies($page, $per_page, $ids, $filter, $sort_field, $sort_dir, $lng) {
    global $db, $app;

    $sql = '
		SELECT
			a.id, a.name AS agency_name, c.title_' . $lng . ' AS country_title
		FROM agencies a
		LEFT JOIN escorts e ON a.id = e.agency_id AND e.status & 32
		LEFT JOIN users u ON u.id = a.user_id
		LEFT JOIN countries c ON c.id = a.country_id
		WHERE 1
	';

    /*$countSql = '
		SELECT COUNT(DISTINCT(a.id))
		FROM agencies a
		LEFT JOIN escorts e ON a.id = e.agency_id
		LEFT JOIN users u ON u.id = a.user_id
		WHERE 1
	';*/

    $countSql = $sql;

    $where = ' AND u.status = 1 AND LENGTH(a.name) > 0 AND a.id IN (' . $ids . ')';

    if (is_array($filter)) {
        if (isset($filter['name']) && strlen($filter['name']))
            $where .= " AND a.name LIKE '" . $filter['name'] . "%'";

        if (isset($filter['country_id']) && strlen($filter['country_id']))
            $where .= ' AND a.country_id = ' . $filter['country_id'];
    }

    $sql .= $where;
    $countSql .= $where;

    $sql .= '
		GROUP BY a.id
		ORDER BY ' . $sort_field . ' ' . $sort_dir . '
		LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
	';

    $countSql .= ' GROUP BY a.id ';

    //$count = intval($db->fetchOne($countSql));
    $items = $db->query($sql)->fetchAll();
    $count = count($db->query($countSql)->fetchAll());

    return array($items, $count);
}

function getReviewsCities($lng) {
    global $db, $app;

    $sql = '
		SELECT
			r.id,
			c.id AS city_id,
			c.title_' . $lng . ' AS city_title
		FROM reviews r
		LEFT JOIN cities c ON c.id = r.city
		WHERE r.application_id = ' . $app->id . ' AND r.status = 2 AND r.is_deleted = 0 AND c.country_id = ' . $app->country_id . '
		GROUP BY c.id
		ORDER BY c.title_' . $lng . '
	';

    return $db->query($sql)->fetchAll();
}

function getEscortReviews($escort_showname, $page, $per_page, $lng) {
    global $db, $app;

    $sql = '
		SELECT
			e.id AS escort_id, e.fuckometer AS e_fuckometer, u.username, r.looks_rating, r.services_rating, r.is_fake_free, c.title_' . $lng . ' AS city_title, r.meeting_place,
			r.duration, r.duration_unit, r.price, r.currency, r.fuckometer, r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69, r.s_anal, r.s_anal, r.s_sex, r.s_attitude,
			r.s_conversation, r.s_breast, r.s_multiple_sex,	r.s_availability, r.s_photos, r.services_comments, r.review, UNIX_TIMESTAMP(r.meeting_date) AS meeting_date,
			r.escort_comment,  m.is_premium, m.reviews_count
		FROM reviews r
		INNER JOIN users u ON r.user_id = u.id
		INNER JOIN escorts e ON e.id = r.escort_id
		INNER JOIN members m ON r.user_id = m.user_id
		LEFT JOIN agencies a ON e.agency_id = a.id
		LEFT JOIN cities c ON c.id = r.city
		WHERE r.application_id = ' . $app->id . ' AND r.status = 2 AND r.is_deleted = 0 AND e.showname = ?
		ORDER BY r.creation_date DESC
		LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
	';

    $countSql = '
		SELECT
			COUNT(DISTINCT(r.id))
		FROM reviews r
		INNER JOIN users u ON r.user_id = u.id
		INNER JOIN escorts e ON e.id = r.escort_id
		LEFT JOIN agencies a ON e.agency_id = a.id
		LEFT JOIN cities c ON c.id = r.city
		WHERE r.application_id = ' . $app->id . ' AND r.status = 2 AND r.is_deleted = 0 AND e.showname = ?
	';

    $items = $db->query($sql, array($escort_showname))->fetchAll();
    $count = $db->fetchOne($countSql, array($escort_showname));

    return array($items, $count);
}

function getEscortLastReview($escort_id) {
    global $db, $app;

    $sql = '
		SELECT SQL_CALC_FOUND_ROWS
			u.username, r.looks_rating, r.services_rating, r.is_fake_free, r.meeting_place, r.duration, r.duration_unit, r.price, r.currency,
			r.fuckometer, r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69, r.s_anal, r.s_anal, r.s_sex, r.s_attitude, r.s_conversation, r.s_breast, r.s_multiple_sex,
			r.s_availability, r.s_photos, r.services_comments, r.review, UNIX_TIMESTAMP(r.meeting_date) AS meeting_date, UNIX_TIMESTAMP(r.creation_date) AS creation_date
		FROM reviews r
		INNER JOIN users u ON r.user_id = u.id
		WHERE r.application_id = ' . $app->id . ' AND r.status = 2 AND r.is_deleted = 0 AND r.escort_id = ?
		ORDER BY r.creation_date DESC
		LIMIT 1
	';

    $countSql = 'SELECT FOUND_ROWS()';

    $item = $db->fetchRow($sql, array($escort_id));
    $count = $db->fetchOne($countSql);

    return array($item, $count);
}

function getTopReviewers($count, $lng) {
    global $db, $app;

    $sql = '
		SELECT
			r.user_id, u.username, UNIX_TIMESTAMP(u.date_registered) AS date_registered, e.showname, UNIX_TIMESTAMP(r.creation_date) AS creation_date,
			a.name AS agency_name, m.is_premium, m.reviews_count
		FROM reviews r
		INNER JOIN users u ON r.user_id = u.id
		INNER JOIN members m ON r.user_id = m.user_id
		INNER JOIN escorts e ON e.id = r.escort_id
		LEFT JOIN agencies a ON e.agency_id = a.id
		WHERE r.application_id = ' . $app->id . ' AND r.status = 2 AND r.is_deleted = 0
		GROUP BY r.user_id
		ORDER BY m.reviews_count DESC
		LIMIT ?
	';

    /*$sql = '
		SELECT
			u.id, u.username, e.showname, IF(a.id NOT IS NULL, a.name, NULL), m.last_review_date
		FROM members m
		INNER JOIN users u ON m.user_id = u.id
		INNER JOIN reviews r ON r.user_id = u.id
		INNER JOIN escorts e ON e.id = r.last_escort_id
		INNER JOIN agencies a ON a.user_id = e.user_id
		WHERE r.application_id = ' . $app->id . ' AND m.reviews_count > 0
		GROUP BY m.id
		ORDER BY m.reviews_count DESC
		LIMIT ?
	';*/

    $items = $db->query($sql, $count)->fetchAll();

    return $items;
}

function getTopLadies($count, $lng) {
    global $db, $app;

    $sql = '
		SELECT
			r.escort_id, e.showname, e.fuckometer, e.fuckometer_rank, a.name AS agency_name,
			AVG(looks_rating) AS avg_looks, AVG(services_rating) AS avg_services, m.is_premium, m.reviews_count
		FROM reviews r
		INNER JOIN users u ON r.user_id = u.id
		INNER JOIN members m ON r.user_id = m.user_id
		INNER JOIN escorts e ON e.id = r.escort_id
		LEFT JOIN agencies a ON e.agency_id = a.id
		WHERE r.application_id = ' . $app->id . ' AND r.status = 2 AND r.is_deleted = 0 AND e.status & 32 AND NOT e.status & 256
		GROUP BY e.id
		ORDER BY e.fuckometer DESC
		LIMIT ?
	';

    $items = $db->query($sql, $count)->fetchAll();

    return $items;
}

function getLastReviewByEscort($escort_id) {
    global $db, $app;

    $sql = '
		SELECT
			UNIX_TIMESTAMP(r.creation_date) AS creation_date, u.username
		FROM reviews r
		LEFT JOIN users u ON r.user_id = u.id
		WHERE r.application_id = ' . $app->id . ' AND r.status = 2 AND r.is_deleted = 0 AND r.escort_id = ?
		ORDER BY r.creation_date DESC
		LIMIT 1
	';

    $item = $db->query($sql, $escort_id)->fetch();

    return $item;
}

function getLastReviewByMember($user_id) {
    global $db, $app;

    $sql = '
		SELECT
			UNIX_TIMESTAMP(r.creation_date) AS creation_date, e.showname
		FROM reviews r
		LEFT JOIN escorts e ON e.id = r.escort_id
		WHERE r.application_id = ' . $app->id . ' AND r.status = 2 AND r.is_deleted = 0 AND r.user_id = ?
		ORDER BY r.creation_date DESC
		LIMIT 1
	';

    $item = $db->query($sql, $user_id)->fetch();

    return $item;
}

function getEscortDetailsForReviews($escort_id, $lng) {
    global $db;

    $sql = 'SELECT showname FROM escorts WHERE id = ?';
    $showname = $db->fetchOne($sql, $escort_id);

    $sql = 'SELECT c.id, c.title_' . $lng . ' AS title FROM cities c LEFT JOIN escort_cities e_c ON e_c.city_id = c.id WHERE e_c.escort_id = ?';
    $cities = $db->query($sql, $escort_id)->fetchAll();

    return array($showname, $cities);
}

/*define('KISSING',                         12);
define('KISSING_WITH_TONGUE',               13);
define('BLOWJOB_WITH_CONDOM',               12);
define('BLOWJOB_WITHOUT_CONDOM',            13);
define('CUMSHOT_IN_MOUTH_SPIT',             12);
define('CUMSHOT_IN_MOUTH_SWALLOW',          13);
define('YES',                               1);
define('SEX_ENTHUSIASTIC',                  11);
define('SEX_PASSIVE',                       12);
define('SEX_ACTIVE',                        13);
define('ATTITUDE_FRIENDLY',                 11);
define('ATTITUDE_UNFRIENDLY',               12);
define('ATTITUDE_POSHY',                    13);
define('ATTITUDE_NORMAL',                   14);
define('CONVERSATION_LANGUAGE_PROBLEMS',    11);
define('CONVERSATION_VERY_SIMPLE',          12);
define('CONVERSATION_NORMAL',               13);
define('CONVERSATION_INTELLIGENT',          14);
define('AVAILABILITY_EASY',                 11);*/

function getFuckometer($data) {
    global $db;

    $fuckometer = 71;

    $fuckometer += ($data['looks_rate']) * 0.45;
    $fuckometer += ($data['services_rate']) * 0.45;
    $fuckometer += (BLOWJOB_WITHOUT_CONDOM == $data['s_blowjob']) ? 4 : 0;
    $fuckometer += (BLOWJOB_WITH_CONDOM == $data['s_blowjob']) ? 2 : 0;
    $fuckometer += (KISSING_WITH_TONGUE == $data['s_kissing']) ? 4 : 0;
    $fuckometer += (KISSING == $data['s_kissing']) ? 2 : 0;
    $fuckometer += (CUMSHOT_IN_MOUTH_SPIT == $data['s_cumshot']) ? 1 : 0;
    $fuckometer += (CUMSHOT_IN_MOUTH_SWALLOW == $data['s_cumshot']) ? 1.5 : 0;
    $fuckometer += (YES == $data['s_anal']) ? 1 : 0;
    $fuckometer += (YES == $data['s_69']) ? 1 : 0;
    $fuckometer += (YES == $data['s_multiple_times_sex']) ? 1 : 0;
    $fuckometer += (SEX_ENTHUSIASTIC == $data['s_sex']) ? 1.5 : 0;
    $fuckometer += (SEX_PASSIVE == $data['s_sex']) ? -2 : 0;
    $fuckometer += (SEX_ACTIVE == $data['s_sex']) ? 0.5 : 0;
    $fuckometer += (ATTITUDE_FRIENDLY == $data['s_attitude']) ? 1.5 : 0;
    $fuckometer += (ATTITUDE_UNFRIENDLY == $data['s_attitude']) ? -2 : 0;
    $fuckometer += (ATTITUDE_POSHY == $data['s_attitude']) ? -1 : 0;
    $fuckometer += (ATTITUDE_NORMAL == $data['s_attitude']) ? 0.5 : 0;
    $fuckometer += (CONVERSATION_LANGUAGE_PROBLEMS == $data['s_conversation']) ? -2 : 0;
    $fuckometer += (CONVERSATION_NORMAL == $data['s_conversation']) ? 1 : 0;
    $fuckometer += (CONVERSATION_INTELLIGENT == $data['s_conversation']) ? 1.5 : 0;
    $fuckometer += (CONVERSATION_VERY_SIMPLE == $data['s_conversation']) ? -0.5 : 0;
    $fuckometer += (AVAILABILITY_EASY == $data['s_availability']) ? 1 : 0;

    // reviews count bonus
    $count = $db->fetchOne('SELECT reviews FROM escorts WHERE id = ?', $data['escort_id']);

    if (5 < $count && 10 >= $count)
        $fuckometer += 0.5;
    else if (10 < $count && 20 >= $count)
        $fuckometer += 1;
    else if (20 < $count)
        $fuckometer += 2;

    $fuckometer = round($fuckometer, 1);

    return $fuckometer;
}

function getTrustedMembers() {
    global $db;

    $FUCKOMETER_TRUSED_USER_REVIEWS = 4;

    $members = $db->fetchAll('
		SELECT DISTINCT r.user_id
		FROM reviews r
		GROUP BY r.user_id
		HAVING COUNT(*) >= ?
	', $FUCKOMETER_TRUSED_USER_REVIEWS, Zend_Db::FETCH_NUM);

    foreach ($members as $i => $member) {
        $members[$i] = $member[0];
    }

    return $members;
}

function calcEscortFuckometer($escort_id) {
    global $db;

    $TOTAL_FUCKOMETER_ESCORT_MIN_REVIEWS = 6;
    $TOTAL_FUCKOMETER_USER_MIN_REVIEWS = 1;

    // checking escort's reviews count
    $escort_reviews_count = $db->fetchOne('SELECT COUNT(id) FROM reviews WHERE escort_id = ? AND status = 2', $escort_id);

    if ($escort_reviews_count < $TOTAL_FUCKOMETER_ESCORT_MIN_REVIEWS)
        return 0;

    // list of trusted members user_ids
    $trusted = getTrustedMembers();

    $result = $db->fetchAll('
		SELECT r.user_id,
			COUNT(r.user_id) AS count,
			SUM(r.fuckometer) AS sum,
			m.reviews_count
		FROM reviews r
		LEFT JOIN members m ON m.user_id = r.user_id
		WHERE r.escort_id = ? AND r.status IN (2, 4) AND m.reviews_count >= ?
		GROUP BY r.user_id
	', array($escort_id, $TOTAL_FUCKOMETER_USER_MIN_REVIEWS));

    // calculate fuckometer
    $count = $count_trusted = $sum = 0;
    foreach ($result as $data) {
        $trusted_k = in_array($data['user_id'], $trusted) ? 2 : 1;

        $count += ($data['count'] * $trusted_k);
        //$count_trusted += in_array($data['user_id'], $trusted) ? $data['count'] : 0;
        $sum += ($data['sum'] * $trusted_k);
    }

    $fuckometer = 0;
    if ($count != 0) {
        $fuckometer = $sum / $count;
    }

    // reviews count bonus
    switch (true) {
        case (5 < $count && 10 >= $count):
            $fuckometer += 0.5;
            break;
        case (10 < $count && 20 >= $count):
            $fuckometer += 1;
            break;
        case (20 < $count):
            $fuckometer += 2;
            break;
    }

    return $fuckometer;
}

function getEscortProfileForReviews($escort_id) {
    global $db;

    $sql = 'SELECT contact_phone_parsed FROM escort_profiles_v2 WHERE escort_id = ?';
    return $db->query($sql, $escort_id)->fetch();
}

function checkSmsUnique($sms_unique) {
    global $db;

    $sql = 'SELECT COUNT(id) FROM reviews WHERE sms_unique = ?';
    $dubl = $db->fetchOne($sql, $sms_unique);

    if ($dubl == 0)
        return true;
    else
        return false;
}

function outbox($escort_id, $phone_to, $phone_from, $text, $application_id, $identificationNumber = null, $is_video_call = false, $pass = null) {
    global $db;

	if (!strlen($escort_id)) $escort_id = null;
	$row = array(
		'escort_id' => $escort_id,
		'phone_to' => $phone_to,
		'phone_from' => $phone_from,
		'text' => $text,
		'application_id' => $application_id,
		'year' => date('Y')
	);

	if ($application_id == APP_6B) {
		$row['identificationNumber'] = $identificationNumber;
	}

	if($is_video_call){
		$row['is_video_call'] = 1;
	}

	if($pass){
		$row['pass'] = $pass;
	}

	$db->insert('sms_outbox', $row);

	return $db->lastInsertId();
}

function getMember($user_id) {
    global $db;

    return $db->query('SELECT * FROM members WHERE user_id = ?', $user_id)->fetch();
}

function addReview($req) {
    global $db, $app;

    if ($app->id == APP_6A) {
        $isBlocked = $db->fetchOne('SELECT TRUE FROM escort_blocked_users WHERE escort_id = ? AND user_id = ?', array($req['escort_id'], $req['user_id']));

        if ($isBlocked) {
            return false;
        }
    }

    $profile = getEscortProfileForReviews($req['escort_id']);

    do {
        $sms_unique = substr(md5(microtime()), 0, 5);
    }
    while (!checkSmsUnique($sms_unique));

    /* status */
    $status = 1;
    $mem_susp = 0;

    $mem = getMember($req['user_id']);
    if ($mem['is_suspicious'] == 1) {
        $status = 3;
        $mem_susp = 1;
    }
    /**/

    $data = array(
        'user_id' => $req['user_id'],
        'escort_id' => $req['escort_id'],
        'meeting_date' => trim($req['m_date']),
        'duration' => trim($req['duration']),
        'duration_unit' => $req['duration_unit'],
        'price' => trim($req['price']),
        'currency' => intval($req['currency']) > 0 ? intval($req['currency']) : 2, // 2 is EUR
        'looks_rating' => $req['looks_rate'],
        'services_rating' => $req['services_rate'],
        'services_comments' => trim($req['services_comments']),
        'review' => trim($req['review']),
        'fuckometer' => getFuckometer($req),
        'creation_date' => new Zend_Db_Expr('NOW()'),
        'city' => intval($req['city_id']),
        'city_is_in_escort_profile' => $req['meeting_city'] != 'other' ? 1 : 0,
        'meeting_place' => $req['meeting_place'],
        's_kissing' => $req['s_kissing'],
        's_blowjob' => $req['s_blowjob'],
        's_cumshot' => $req['s_cumshot'],
        's_69' => $req['s_69'],
        's_anal' => $req['s_anal'],
        's_sex' => $req['s_sex'],
        's_attitude' => $req['s_attitude'],
        's_conversation' => $req['s_conversation'],
        's_breast' => $req['s_breast'],
        's_multiple_sex' => $req['s_multiple_times_sex'],
        's_availability' => $req['s_availability'],
        's_photos' => $req['s_photos'],
        't_user_info' => trim($req['t_user_info']),
        't_meeting_date' => trim($req['t_meeting_date']),
        't_meeting_time' => $req['hrs'] . ':' . $req['min'],
        't_meeting_duration' => trim($req['t_meeting_duration']),
        't_meeting_place' => trim($req['t_meeting_place']),
        't_comments' => trim($req['t_comments']),
        'sms_number' => $profile['contact_phone_parsed'],
        'ip' => $req['ip'],
        'is_suspicious' => IsSuspicious($req['escort_id'], $req['user_id'], $req['ip']),
        'application_id' => $app->id,
        'sms_unique' => $sms_unique,
        'status' => $status,
    );

    if (Cubix_Application::getId() == APP_EF && $req['is_mobile']) {
        $data['is_mobile'] = $req['is_mobile'];
    }

    if (Cubix_Application::getId() == APP_ED) {
        $data["quality"] = (int)$req['quality'];
        $data["location_review"] = $req['incall_location_review'];
        $data["country"] = intval($req['country_id']);
    }

    $db->insert('reviews', $data);

    if (Cubix_Application::getId() == APP_BL || Cubix_Application::getId() == APP_EF) {
        $rid = $db->lastInsertId();
        Cubix_AlertEvents::addFollowAlert($req['user_id'], FOLLOW_ALERT_REVIEW, $rid);
    }

    updateEscortFuckometer($req['escort_id']);

    return array($sms_unique, $profile['contact_phone_parsed'], $mem_susp);
}


function editReview($id, $user_id, $review) {
    global $db;

    /* status */
    $status = 1;

    $mem = getMember($user_id);
    if ($mem['is_suspicious'] == 1)
        $status = 3;
    /**/

    $r = $db->query('SELECT review, old_review FROM reviews WHERE id = ?', $id)->fetch();

    $arr = array(
        'review' => trim($review),
        'status' => $status,
        'modified' => 1
    );

    if (strlen($r['old_review']) == 0) {
        $arr['old_review'] = $r['review'];
    }

    $db->update('reviews', $arr, $db->quoteInto('id = ?', $id));
}

function smsSent($sms_unique) {
    global $db;

    $db->update('reviews', array('sms_status' => 1), $db->quoteInto('sms_unique = ?', $sms_unique));
}

function update2SMSData($id, $msg_id = null, $status = 0) {
    global $db;

    $data = array();
    if ($msg_id)
        $data = array_merge($data, array('msg_id_2sms' => $msg_id));

    if ($status !== false)
        $data = array_merge($data, array('status_2sms' => $status));

    $db->update('sms_outbox', $data, $db->quoteInto('id = ?', $id));
}

function IsSuspicious($escort_id, $user_id, $ip) {
    global $db;

    // 2 reviews with same IP and same escort but different poster
    $sql = "SELECT DISTINCT user_id FROM reviews WHERE escort_id = " . $escort_id . " AND ip = '" . $ip . "'";
    $items = $db->query($sql)->fetchAll();

    if ($items && (count($items) > 1 || $user_id != $items[0]['user_id'])) {
        //REVIEW_SUSPICIOUS_SAME_ESCORT_AND_IP
        return 1;
    }

    // review poster IP identical with other user account with reviews
    $sql = "SELECT DISTINCT u.id FROM reviews r LEFT JOIN users u ON r.user_id = u.id WHERE u.last_ip = '" . $ip . "'";
    $items = $db->query($sql)->fetchAll();

    if ($items && count($items) > 1) {
        //REVIEW_SUSPICIOUS_USER_IP_DUPLICITY
        return 2;
    }

    // review poster has identical clientID cookie with escort
    if (($escort_user_id = $db->fetchOne("SELECT user_id FROM escorts WHERE id = " . $escort_id)) > 0) {
        $sql = "
			SELECT cc1.client_id
			FROM (client_cookie AS cc1, client_cookie AS cc2)
			WHERE cc1.user_id = " . $escort_user_id . " AND cc2.user_id = " . $user_id . " AND cc1.client_id = cc2.client_id";
        $items = $db->query($sql)->fetchAll();

        if ($items && count($items) > 0) {
            //REVIEW_SUSPICIOUS_UNIQUE_COOKIE_ID_DUPLICITY
            return 3;
        }
    }

    return 0;
}

function getReviewsForMember($user_id, $lng) {
    global $db, $app;

    $sql = '
		SELECT
			e.showname, e.id AS escort_id, UNIX_TIMESTAMP(r.creation_date) AS creation_date, c.title_' . $lng . ' AS city_title, a.name AS agency_name,
			r.fuckometer, r.status, r.id, r.modified
		FROM reviews r
		LEFT JOIN users u ON r.user_id = u.id
		LEFT JOIN escorts e ON e.id = r.escort_id
		LEFT JOIN agencies a ON e.agency_id = a.id
		LEFT JOIN cities c ON c.id = r.city
		WHERE r.application_id = ' . $app->id . ' AND r.is_deleted = 0 AND r.user_id = ?
		ORDER BY r.creation_date DESC
	';

    return $db->query($sql, $user_id)->fetchAll();
}

function hasNeedModificationReviews($user_id) {
    global $db;

    return $db->fetchOne('SELECT COUNT(id) FROM reviews WHERE (status = ? OR status = ?) AND user_id = ?', array(5, 4, $user_id)); // 5 - need_modification, 4 - system_disabled
}

function getReviewById($id, $lng) {
    global $db;

    return $db->query('
		SELECT
			r.user_id, r.status, e.showname, r.meeting_date, c.title_' . $lng . ' AS city_title, r.meeting_place, r.duration, r.duration_unit, r.looks_rating, r.services_rating,
			r.s_attitude, r.s_conversation, r.s_availability, r.review, r.t_user_info, r.t_meeting_date, r.t_meeting_time, r.t_meeting_duration, r.t_meeting_place, r.t_comments,
			r.price, r.currency, r.services_comments, r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69, r.s_anal, r.s_sex, r.s_breast, r.s_multiple_sex, r.s_photos, r.is_deleted, r.id
		FROM reviews r
		LEFT JOIN escorts e ON e.id = r.escort_id
		LEFT JOIN cities c ON c.id = r.city
		WHERE r.id = ?
	', $id)->fetch();
}

function getReviewsForEscort($user_id, $lng, $esc_id = NULL, $page = 1, $per_page = 50) {
    global $db, $app;

    $escort_id = $esc_id ? $esc_id : $db->fetchOne('SELECT id FROM escorts WHERE user_id = ?', $user_id);

    $limit = '';
    if (!is_null($page)) {
        $limit = ' LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
    }

    $adv_query = '';
    if (Cubix_Application::getId() == APP_EF || Cubix_Application::getId() == APP_ED) {
        $adv_query .= ' r.looks_rating, r.services_rating, UNIX_TIMESTAMP(r.meeting_date) AS meeting_date,';
    }

    $sql = '
		SELECT
			SQL_CALC_FOUND_ROWS
			r.id,' . $adv_query . ' e.showname, e.id AS escort_id, u.username, UNIX_TIMESTAMP(r.creation_date) AS creation_date, c.title_' . $lng . ' AS city_title, a.name AS agency_name, r.fuckometer, r.escort_comment
		FROM reviews r
		LEFT JOIN users u ON r.user_id = u.id
		LEFT JOIN escorts e ON e.id = r.escort_id
		LEFT JOIN agencies a ON e.agency_id = a.id
		LEFT JOIN cities c ON c.id = r.city
		WHERE r.application_id = ' . $app->id . ' AND r.status = 2 AND r.is_deleted = 0 AND r.escort_id = ?
		ORDER BY r.creation_date DESC
	' . $limit;

    $countSql = 'SELECT FOUND_ROWS() AS count';

    $result = $db->fetchAll($sql, array($escort_id));
    $count = $db->fetchOne($countSql);

    return array('reviews' => $result, 'count' => $count);
}

function getPendingReviewsForEscort($user_id, $esc_id = NULL, $page = 1, $per_page = 50) {
    global $db, $app;

    $escort_id = $esc_id ? $esc_id : $db->fetchOne('SELECT id FROM escorts WHERE user_id = ?', $user_id);

    $limit = '';
    if (!is_null($page)) {
        $limit = ' LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
    }

    $sql = '
		SELECT
			SQL_CALC_FOUND_ROWS
			r.id, e.showname, r.t_user_info, r.t_meeting_date, r.t_meeting_time, r.t_meeting_duration, r.t_meeting_place
		FROM reviews r
		LEFT JOIN escorts e ON e.id = r.escort_id
		WHERE r.application_id = ' . $app->id . ' AND r.status = 1 AND (r.answer IS NULL OR r.answer = 0) AND r.is_deleted = 0 AND r.escort_id = ?
		ORDER BY r.creation_date DESC
	' . $limit;

    $countSql = 'SELECT FOUND_ROWS() AS count';

    $result = $db->fetchAll($sql, array($escort_id));
    $count = $db->fetchOne($countSql);

    return array('reviews' => $result, 'count' => $count);
}

function checkPendingReviewForEscort($user_id, $review_id) {
    global $db;

    $escort_id = $db->fetchOne('SELECT id FROM escorts WHERE user_id = ?', $user_id);

    $r = $db->fetchOne('SELECT COUNT(id) FROM reviews WHERE escort_id = ? AND id = ? AND status = 1 AND (answer IS NULL OR answer = 0) AND is_deleted = 0', array($escort_id, $review_id));

    if ($r > 0)
        return true;
    else
        return false;
}

function setPendingReviewForEscort($review_id, $answer) {
    global $db;

    $arr = array(
        'answer' => $answer
    );

    if ($answer == 1) {
        if (Cubix_Application::getId() != APP_6A) {
            $arr['status'] = 3; //disabled
        }
    }
    $db->update('reviews', $arr, $db->quoteInto('id = ?', $review_id));
}

function getReviewsForAgencyEscorts($user_id, $lng, $page = 1, $per_page = 50) {
    global $db, $app;

    $items = $db->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetchAll();

    $ids = array();
    foreach ($items as $item) {
        $ids[] = $item['id'];
    }
    if (count($ids) > 0) {
        $ids_str = implode(',', $ids);
    }
    else {
        $ids_str = 0;
    }

    $limit = '';
    if (!is_null($page)) {
        $limit = ' LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
    }

    $adv_query = '';
    if (Cubix_Application::getId() == APP_EF || Cubix_Application::getId() == APP_ED) {
        $adv_query .= ' r.looks_rating, r.services_rating, UNIX_TIMESTAMP(r.meeting_date) AS meeting_date,';
    }

    $sql = '
		SELECT
			SQL_CALC_FOUND_ROWS
			r.id,' . $adv_query . ' e.showname, e.id AS escort_id, u.username, UNIX_TIMESTAMP(r.creation_date) AS creation_date, c.title_' . $lng . ' AS city_title, a.name AS agency_name, r.fuckometer, r.escort_comment
		FROM reviews r
		LEFT JOIN users u ON r.user_id = u.id
		LEFT JOIN escorts e ON e.id = r.escort_id
		LEFT JOIN agencies a ON e.agency_id = a.id
		LEFT JOIN cities c ON c.id = r.city
		WHERE r.application_id = ' . $app->id . ' AND r.status = 2 AND r.is_deleted = 0 AND r.escort_id IN (' . $ids_str . ')
		ORDER BY r.creation_date DESC
	' . $limit;

    $countSql = 'SELECT FOUND_ROWS() AS count';

    $result = $db->fetchAll($sql);
    $count = $db->fetchOne($countSql);

    return array('reviews' => $result, 'count' => $count);
}

function getPendingReviewsForAgencyEscorts($user_id, $lng = 'en', $page = 1, $per_page = 50) {
    global $db, $app;

    $items = $db->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetchAll();

    $ids = array();
    foreach ($items as $item) {
        $ids[] = $item['id'];
    }
    if (count($ids) > 0) {
        $ids_str = implode(',', $ids);
    }
    else {
        $ids_str = 0;
    }

    $limit = '';
    if (!is_null($page)) {
        $limit = ' LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
    }

    $sql = '
		SELECT
			SQL_CALC_FOUND_ROWS
			r.id, e.showname, r.t_user_info, r.t_meeting_date, r.t_meeting_time, r.t_meeting_duration, r.t_meeting_place
		FROM reviews r
		LEFT JOIN escorts e ON e.id = r.escort_id
		WHERE r.application_id = ' . $app->id . ' AND r.status = 1 AND (r.answer IS NULL OR r.answer = 0) AND r.is_deleted = 0 AND r.escort_id IN (' . $ids_str . ')
		ORDER BY r.creation_date DESC
	' . $limit;

    $countSql = 'SELECT FOUND_ROWS() AS count';

    $result = $db->fetchAll($sql);
    $count = $db->fetchOne($countSql);

    return array('reviews' => $result, 'count' => $count);
}

function checkPendingReviewForAgencyEscort($user_id, $review_id) {
    global $db;

    $items = $db->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetchAll();

    $ids = array();
    foreach ($items as $item) {
        $ids[] = $item['id'];
    }
    if (count($ids) > 0) {
        $ids_str = implode(',', $ids);
    }
    else {
        $ids_str = 0;
    }

    $r = $db->fetchOne('SELECT COUNT(id) FROM reviews WHERE escort_id IN (' . $ids_str . ') AND id = ? AND status = 1 AND (answer IS NULL OR answer = 0) AND is_deleted = 0', array($review_id));

    if ($r > 0)
        return true;
    else
        return false;
}

function checkEscortReview($escort_user_id, $rev_id) {
    global $db;

    $escort_id = $db->fetchOne('SELECT id FROM escorts WHERE user_id = ?', $escort_user_id);
    $item = $db->query('SELECT escort_comment FROM reviews WHERE id = ? AND escort_id = ?', array($rev_id, $escort_id))->fetch();

    if ($item) {
        if (strlen($item['escort_comment']) > 0)
            return false;
        else
            return true;
    }
    else
        return false;
}

function checkEscortReviewForAgency($user_id, $rev_id) {
    global $db;

    $items = $db->query('SELECT id FROM escorts WHERE user_id = ?', $user_id)->fetchAll();

    $ids = array();
    foreach ($items as $item) {
        $ids[] = $item['id'];
    }
    $ids_str = implode(',', $ids);

    $item = $db->query('SELECT escort_comment FROM reviews WHERE id = ? AND escort_id IN (' . $ids_str . ')', array($rev_id))->fetch();

    if ($item) {
        if (strlen($item['escort_comment']) > 0)
            return false;
        else
            return true;
    }
    else
        return false;
}

function addReviewComment($review_id, $comment) {
    global $db;
    $data = array('escort_comment' => $comment);
    if (Cubix_Application::getId() == APP_6A || Cubix_Application::getId() == APP_EF) {
        $data['escort_comment_status'] = REVIEW_REPLY_PENDING;
    }

    $db->update('reviews', $data, $db->quoteInto('id = ?', $review_id));
}

function reviewsSearch($req, $page, $per_page, $lng) {
    global $db, $app;

    $fields = '';
    $where = '';
    $join = '';

    if (isset($req['showname']) && strlen($req['showname']))
        $where .= " AND e.showname LIKE '%" . $req['showname'] . "%'";

    if (isset($req['gender']) && strlen($req['gender']))
        $where .= " AND e.gender = " . $req['gender'];

    /*if (isset($req['locate']))
	{
		if ($req['locate'] == 'fr')
			//$where .= " AND e.gender = " . $req['gender'];
		elseif ($req['locate'] == 'other')
			//$where .= " AND e.gender = " . $req['gender'];
	}*/

    if (isset($req['city']) && strlen($req['city'])) {
        $fields .= ", c.title_" . $lng . " AS city ";
        $where .= " AND c.id = " . $req['city'];
        $join .= " LEFT JOIN escort_cities e_c ON e.id = e_c.escort_id LEFT JOIN cities c ON c.id = e_c.city_id ";
    }

    if (isset($req['trust']) && $req['trust'] == 'fake_free')
        $where .= " AND r.is_fake_free = 1";

    if (isset($req['kissing']) && strlen($req['kissing']))
        $where .= " AND r.s_kissing = " . $req['kissing'];

    if (isset($req['blowjob']) && strlen($req['blowjob']))
        $where .= " AND r.s_blowjob = " . $req['blowjob'];

    if (isset($req['cumshot']) && strlen($req['cumshot']))
        $where .= " AND r.s_cumshot = " . $req['cumshot'];

    if (isset($req['s_69']) && strlen($req['s_69']))
        $where .= " AND r.s_69 = " . $req['s_69'];

    if (isset($req['anal']) && strlen($req['anal']))
        $where .= " AND r.s_anal = " . $req['anal'];

    if (isset($req['looks_from']) && $req['looks_from'] != -1)
        $where .= " AND r.looks_rating >= " . $req['looks_from'];

    if (isset($req['looks_to']) && $req['looks_to'] != -1)
        $where .= " AND r.looks_rating <= " . $req['looks_to'];

    if (isset($req['services_from']) && $req['services_from'] != -1)
        $where .= " AND r.services_rating >= " . $req['services_from'];

    if (isset($req['services_to']) && $req['services_to'] != -1)
        $where .= " AND r.services_rating <= " . $req['services_to'];

    if (isset($req['fuckometer_from']) && $req['fuckometer_from'] != -1)
        $where .= " AND r.fuckometer >= " . $req['fuckometer_from'];

    if (isset($req['fuckometer_to']) && $req['fuckometer_to'] != -1)
        $where .= " AND r.fuckometer <= " . $req['fuckometer_to'];

    if (isset($req['p_date']) && $req['p_date'] == 'other') {
        if (isset($req['p_date_type']) && strlen($req['p_date_type'])) {
            $where .= " AND r.creation_date >= DATE_ADD(NOW(), INTERVAL -" . $req['p_date_type'] . " DAY) ";
        }
    }

    if (isset($req['sex']) && strlen($req['sex']))
        $where .= " AND r.s_sex = " . $req['sex'];

    if (isset($req['multiple_times_sex']) && strlen($req['multiple_times_sex']))
        $where .= " AND r.s_multiple_sex = " . $req['multiple_times_sex'];

    if (isset($req['breast']) && strlen($req['breast']))
        $where .= " AND r.s_breast = " . $req['breast'];

    if (isset($req['attitude']) && strlen($req['attitude']))
        $where .= " AND r.s_attitude = " . $req['attitude'];

    if (isset($req['conversation']) && strlen($req['conversation']))
        $where .= " AND r.s_conversation = " . $req['conversation'];

    if (isset($req['availability']) && strlen($req['availability']))
        $where .= " AND r.s_availability = " . $req['availability'];

    if (isset($req['photos']) && strlen($req['photos']))
        $where .= " AND r.s_photos = " . $req['photos'];

    $sql = "
		SELECT
			e.showname, e.date_registered, e.id AS id, r.application_id, e_p.hash AS photo_hash, e_p.ext AS photo_ext, e_p.status AS photo_status " . $fields . "
		FROM reviews r
		LEFT JOIN escorts e ON e.id = r.escort_id
		LEFT JOIN escort_photos e_p ON e.id = e_p.escort_id AND e_p.is_main = 1 " . $join . "
		WHERE r.application_id = " . $app->id . " AND r.status = 2 AND r.is_deleted = 0 " . $where . "
		GROUP BY e.id
		ORDER BY r.creation_date DESC
		LIMIT " . ($page - 1) * $per_page . ", " . $per_page . "
	";

    $countSql = "
		SELECT
			COUNT(DISTINCT(e.id))
		FROM reviews r
		LEFT JOIN escorts e ON e.id = r.escort_id " . $join . "
		WHERE r.application_id = " . $app->id . " AND r.status = 2 AND r.is_deleted = 0 " . $where . "
	";

    $items = $db->query($sql)->fetchAll();
    $count = $db->fetchOne($countSql);

    return array($items, $count);
}

function cookieClientID($user_id, $client_id) {
    global $db;

    // <editor-fold defaultstate="collapsed" desc="If there is/are other user(s) who has/have logged in with same client_id">
    $sql = '
		SELECT DISTINCT(user_id) AS user_id
		FROM client_cookie
		WHERE client_id = ? AND user_id <> ?
	';
    // try to find other users with same client_id
    $users = $db->fetchAll($sql, array($client_id, $user_id));
	
    // if there are matched clients
    if (count($users)) {
        // if we don't have warning about this user, just add it
        if (!($wid = $db->fetchOne('SELECT id FROM suspicious_warning WHERE user_id = ?', $user_id))) {
            $db->insert('suspicious_warning', array('user_id' => $user_id, 'date' => new Zend_Db_Expr('NOW()')));
            $wid = $db->lastInsertId();
        }
        // else if there is warning about this user, just update it's date
        else {
            $db->update('suspicious_warning', array('date' => new Zend_Db_Expr('NOW()')), $db->quoteInto('id = ?', $wid));
        }

		$bulk_object = new Cubix_BulkInserter($db, 'suspicious_warning_user');
		$bulk_object->addKeys(array('warning_id', 'user_id' , 'type'));
		
		foreach ( $users as $user ) {
			$bulk_object->addFields(array('warning_id' => $wid, 'user_id' => $user['user_id'], 'type' => 2));
		}
		
        // delete old records of matches from database
        $db->query('DELETE FROM suspicious_warning_user WHERE warning_id = ? AND type = 2', $wid);
		// and add them back again (the list may differ from old warning)
		$bulk_object->insertBulk();
        /*foreach ($users as $user) {
            $db->insert('suspicious_warning_user', array('warning_id' => $wid, 'user_id' => $user['user_id'], 'type' => 2));
        }*/
    }
    // </editor-fold>


    $sql = "SELECT COUNT(client_id) FROM client_cookie WHERE user_id = " . $user_id . " AND client_id = '" . $client_id . "'";
    $count = $db->fetchOne($sql);

    if ($count == 0) {
        $db->insert('client_cookie', array(
            'client_id' => $client_id,
            'user_id' => $user_id,
            'created' => new Zend_Db_Expr('NOW()')
        ));
    }
}

/**
 * get last client cookie id for user
 * @param $user_id
 * @return mixed
 */
function getClientCookieId($user_id) {
    global $db;

    $sql = '
		SELECT client_id
		FROM client_cookie
		WHERE user_id = ?
	';
    $client_cookie = $db->fetchRow($sql, array($user_id));
    return $client_cookie->client_id;
}

/**
 * Need to be called after adding a review for escort
 *
 * @param int $escort_id
 * @global Zend_Db_Adapter_Mysqli $db
 */
function updateEscortFuckometer($escort_id) {
    global $db;

    $db->update('escorts', array(
        'fuckometer' => calcEscortFuckometer($escort_id)
    ), array($db->quoteInto('id = ?', $escort_id)));
}

function getMonthGirls($lang_id, $page, $per_page, $get_history, $showname) {
    global $db, $app;

    $page = intval($page);
    if ($page < 1) $page = 1;
    $limit = $per_page = intval($per_page);
    $start = ($page - 1) * $per_page;

    $count = 0;

    if (!$get_history) {
        $gotm = getCurrentGOTM($lang_id);
        $exclude = $gotm ? array($gotm['id']) : array();

        $where = '';
        if (isset($showname) && strlen($showname)) {
            $where .= " AND e.showname LIKE '" . $showname . "%'";
        }

        if (in_array(Cubix_Application::getId(), array(30, 11))) // beneluxxx, asianescorts (multicountry projects)
        {
            $countries = $db->fetchAll('SELECT id FROM countries');
            $cids = array();

            foreach ($countries as $country) {
                $cids[] = $country['id'];
            }

            $countries_str = implode(',', $cids);

            $sql = '
				SELECT
					e.id, e.showname, e.city_id,
					eph.hash AS photo_hash, eph.ext AS photo_ext,
					e.votes_count,

					e.top20
				FROM escorts e
				INNER JOIN escort_photos eph ON eph.escort_id = e.id AND eph.is_main = 1
				WHERE
					e.votes_count >= 1 AND e.status & 32 AND
					e.gender = 1 AND e.country_id IN (' . $countries_str . ') ' . (count($exclude) > 0 ? ' AND e.id NOT IN (' . implode(',', $exclude) . ')' : '') . $where . '
				GROUP BY e.id
				ORDER BY e.votes_count DESC
				LIMIT ' . $start . ', ' . $limit . '
			';

            $bind = array();

            $count = $db->fetchOne('
				SELECT COUNT(DISTINCT(e.id))
				FROM escorts e
				INNER JOIN escort_photos eph ON eph.escort_id = e.id AND eph.is_main = 1
				WHERE
					e.votes_count >= 1 AND e.status & 32 AND
					e.gender = 1 AND e.country_id IN (' . $countries_str . ')' . (count($exclude) > 0 ? ' AND e.id NOT IN (' . implode(',', $exclude) . ')' : '') . $where . '
			');
        }
        else {
            $sql = '
				SELECT
					e.id, e.showname, e.city_id,
					eph.hash AS photo_hash, eph.ext AS photo_ext,
					e.votes_count,

					e.top20
				FROM escorts e
				INNER JOIN escort_photos eph ON eph.escort_id = e.id AND eph.is_main = 1
				WHERE
					e.votes_count >= 1 AND e.status & 32 AND
					e.gender = 1 AND e.country_id = ?' . (count($exclude) > 0 ? ' AND e.id NOT IN (' . implode(',', $exclude) . ')' : '') . $where . '

				GROUP BY e.id
				ORDER BY e.votes_count DESC
				LIMIT ' . $start . ', ' . $limit . '
			';

            $bind = array($app->country_id);

            $count = $db->fetchOne('
				SELECT COUNT(DISTINCT(e.id))
				FROM escorts e
				INNER JOIN escort_photos eph ON eph.escort_id = e.id AND eph.is_main = 1
				WHERE
					e.votes_count >= 1 AND e.status & 32 AND
					e.gender = 1 AND e.country_id = ?' . (count($exclude) > 0 ? ' AND e.id NOT IN (' . implode(',', $exclude) . ')' : '') . $where . '
			', array($app->country_id));
        }

    }
    else {
        $sql = '
			SELECT
				e.id, e.showname, e.city_id,
				eph.hash AS photo_hash, eph.ext AS photo_ext, eph.status AS photo_status,
				e.votes_count,
				e.top20,
				gotm.year, gotm.month
			FROM girl_of_month gotm
			INNER JOIN escorts e ON e.id = gotm.escort_id
			INNER JOIN escort_photos eph ON eph.escort_id = e.id AND eph.is_main = 1
			WHERE gotm.escort_id > 0 AND gotm.application_id = ?
			ORDER BY gotm.year DESC, gotm.month DESC
		';

        $bind = array($app->id);
    }

    $result = $db->fetchAll($sql, $bind);

    foreach ($result as $i => $row) {
        $result[$i]['city'] = $db->fetchOne('SELECT title_' . $lang_id . ' FROM cities WHERE id = ?', $row['city_id']);
    }

    return array('escorts' => $result, 'count' => $count);
}

function getCurrentGOTM($lang_id) {
    global $db, $app;

    $month = intval(date('n'));
    $year = intval(date('Y'));

    $month--;
    if (0 == $month) {
        $month = 12;
        $year--;
    }
    if (!$lang_id) {
        $lang_id = 'en';
    }
    $where = '';
    $join = '';

    if (in_array($app->id,array(APP_EF)))
    {
        $join = ' INNER JOIN order_packages op ON op.escort_id = e.id';
        $where = ' AND op.status = 2 AND op.order_id IS NOT NULL';
    }

    $escort = $db->fetchRow('
		SELECT
			e.id, e.showname,
			eph.hash AS photo_hash, eph.ext AS photo_ext, eph.status AS photo_status,
			e.votes_count, ' . $app->id . ' AS application_id,
			FLOOR(DATEDIFF(CURDATE(), ep.birth_date) / 365.25) AS age,
			c.title_' . $lang_id . ' AS city
		FROM girl_of_month gotm
		INNER JOIN escorts e ON e.id = gotm.escort_id
		INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
		INNER JOIN escort_photos eph ON eph.escort_id = e.id AND eph.is_main = 1
		INNER JOIN cities c ON c.id = e.city_id
		'.$join.'
		WHERE gotm.year = ? AND gotm.month = ? AND gotm.application_id = ?
		AND NOT e.status & ' . ESCORT_STATUS_OWNER_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_ADMIN_DISABLED . ' AND NOT e.status & ' . ESCORT_STATUS_DELETED . $where
        , array($year, $month, $app->id));

    if (!$escort) return false;

    $escort['month'] = $month;
    $escort['year'] = $year;

    return $escort;
}

function getCurrentGOTD($lang_id) {
    global $db, $app;

    $escort = $db->fetchRow('
		SELECT
			e.id, e.showname,
			eph.hash AS photo_hash, eph.ext AS photo_ext, eph.status AS photo_status,
			e.votes_count, ' . $app->id . ' AS application_id,
			FLOOR(DATEDIFF(CURDATE(), ep.birth_date) / 365.25) AS age,
			c.title_' . $lang_id . ' AS city
		FROM escorts e
		INNER JOIN escort_profiles_v2 ep ON ep.escort_id = e.id
		INNER JOIN escort_photos eph ON eph.escort_id = e.id AND eph.is_main = 1
		INNER JOIN cities c ON c.id = e.city_id
		WHERE FIND_IN_SET(?, e.products)
	', array());

    if (!$escort) return false;

    return $escort;
}

function isGOTM($escort_id) {
    global $db, $app;

    $month = intval(date('n'));
    $year = intval(date('Y'));

    $res = $db->fetchRow('SELECT year, month FROM girl_of_month WHERE escort_id = ? AND application_id = ? AND NOT (year = ? AND month = ?) ORDER BY year DESC, month DESC LIMIT 1', array($escort_id, $app->id, $year, $month));

    if ($res)
        return $res;
    else
        return null;
}

function getEscortVotingData($escort_id) {
    global $db;

    return $db->fetchRow('SELECT votes_sum, votes_count, voted_members FROM escorts WHERE id = ?', $escort_id);
}

/* newsletter emails log */
function addNewlstterEmailLog($user_id, $user_type, $action, $emails) {
    global $db;

    $user_id = intval($user_id);
    $acts = array('add', 'edit', 'delete');
    $types = array('escort', 'member', 'agency');

    if (!in_array($action, $acts))
        return;

    if (!in_array($user_type, $types))
        return;

    if ($user_id == 0)
        return;

    if (!is_array($emails))
        return;

    $db->insert('newsletter_email_log', array(
        'user_id' => $user_id,
        'user_type' => $user_type,
        'action' => $action,
        'old_email' => isset($emails['old']) ? $emails['old'] : null,
        'new_email' => isset($emails['new']) ? $emails['new'] : null,
        'date' => new Zend_Db_Expr('NOW()')
    ));
}

/**/

/**
 * @param int $member_id
 * @param int $escort_id
 * @param int $rating
 * @return bool true if success, false if member has already voted
 * @global Zend_Db_Adapter_Mysqli $db
 */
function voteForEscort($member_id, $escort_id) {
    global $db;

    $voted_members = $db->fetchOne('SELECT voted_members FROM escorts WHERE id = ?', $escort_id);
    $voted_members = ($voted_members && $d = @unserialize($voted_members)) ? $d : array();

    if (in_array($member_id, $voted_members)) {
        return false;
    }

    //$voted_members[] = $member_id;
    array_push($voted_members, $member_id);

    $db->query('
		UPDATE escorts
		SET votes_count = votes_count + 1,
			voted_members = ?
		WHERE id = ?
	', array(serialize($voted_members), $escort_id));

    return true;
}

class API_Static_Cache
{
    private static $_cache = array();

    public static function set($key, $value) {
        self::$_cache[$key] = $value;
    }

    public static function get($key) {
        return isset(self::$_cache[$key]) ? self::$_cache[$key] : null;
    }
}


function getEscortActivePackageForPremiumCities($escort_id, $lang_id, $default_package = false) {

    global $db, $app;





    $add_city_products = array(
        PREMIUM_PRODUCT_CITY_SPOT,
        PREMIUM_PRODUCT_ADDITIONAL_CITY_1,
        PREMIUM_PRODUCT_ADDITIONAL_CITY_2,
        PREMIUM_PRODUCT_ADDITIONAL_CITY_3,
    );

    $escort_id = intval($escort_id);

   /* if ($package = API_Static_Cache::get('active_package_' . $escort_id)) {
        return $package;
    }*/
    $where = '';
    if($app->id == APP_EG_CO_UK)
    {
        $where .= ' op.order_id IS NOT NULL AND ';
    }
    $package = $db->fetchRow('
		SELECT p.name AS package, DATEDIFF(op.expiration_date, NOW()) AS expires, op.id AS opid,op.escort_id as id,e.showname as escort_showname, op.package_id, op.date_activated, op.expiration_date
		FROM order_packages op
		INNER JOIN packages p ON p.id = op.package_id
        INNER JOIN escorts e ON e.id = op.escort_id
		WHERE
			op.escort_id = ? AND
			op.status = ? AND
			'.$where.'
			op.application_id = ?
	', array($escort_id, 2, $app->id));

    if (!$package) return null;

    $package['products'] = $db->fetchAll('
		SELECT p.id, p.name
		FROM order_package_products opp
		INNER JOIN products p ON p.id = opp.product_id
		WHERE opp.order_package_id = ?
	', array($package['opid']));

    if (premium_getEscortCitiesLimit($escort_id) > 0  || ($app->id == APP_EG_CO_UK && premium_getEscortCitiesLimit2($escort_id) > 0)) {
        $package['cities'] = $db->fetchAll('
            SELECT c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
            FROM premium_escorts pe
            INNER JOIN cities c ON pe.city_id = c.id
            WHERE pe.order_package_id = ?
        ', array($escort_id, $package['opid']));
    }

    if($app->id == APP_EG_CO_UK){
        $additional_counts = $db->fetchRow('SELECT COUNT(*) AS count_premium_cities FROM order_package_products where order_package_id=? AND product_id IN (' . implode(',', $add_city_products) . ')', array($package['opid']));
        $package['count_premium_cities'] = $additional_counts['count_premium_cities'];
    }
    API_Static_Cache::set('active_package_' . $escort_id, $package);

    return $package;
}

function getEscortActivePackage($escort_id, $lang_id, $default_package = false) {

    global $db, $app;


    $add_city_products = array(
        PREMIUM_PRODUCT_ADDITIONAL_CITY,
        PREMIUM_PRODUCT_ADDITIONAL_CITY_1,
        PREMIUM_PRODUCT_ADDITIONAL_CITY_2,
        PREMIUM_PRODUCT_ADDITIONAL_CITY_3,
    );

    $escort_id = intval($escort_id);

    if ($package = API_Static_Cache::get('active_package_' . $escort_id)) {
        return $package;
    }

    $package = $db->fetchRow('
		SELECT p.name AS package, DATEDIFF(op.expiration_date, NOW()) AS expires, op.id AS opid,op.escort_id as id,e.showname as escort_showname, op.package_id, op.date_activated, op.expiration_date
		FROM order_packages op
		INNER JOIN packages p ON p.id = op.package_id
        INNER JOIN escorts e ON e.id = op.escort_id
		WHERE
			op.escort_id = ? AND
			op.status = ? AND
			op.application_id = ?
			' . (!$default_package ? 'AND	op.order_id IS NOT NULL' : '') . '
	', array($escort_id, 2, $app->id));

    if (!$package) return null;

    $package['products'] = $db->fetchAll('
		SELECT p.id, p.name
		FROM order_package_products opp
		INNER JOIN products p ON p.id = opp.product_id
		WHERE opp.order_package_id = ?
	', array($package['opid']));

    if (premium_getEscortCitiesLimit($escort_id) > 0  || ($app->id == APP_EG_CO_UK && premium_getEscortCitiesLimit2($escort_id) > 0)) {
        $package['cities'] = $db->fetchAll('
            SELECT c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
            FROM premium_escorts pe
            INNER JOIN cities c ON pe.city_id = c.id
            WHERE pe.order_package_id = ?
        ', array($escort_id, $package['opid']));
    }

    if($app->id == APP_EG_CO_UK){
        $additional_counts = $db->fetchRow('SELECT COUNT(*) AS count_premium_cities FROM order_package_products where order_package_id=? AND product_id IN (' . implode(',', $add_city_products) . ')', array($package['opid']));
        $package['count_premium_cities'] = $additional_counts['count_premium_cities'];
    }
    API_Static_Cache::set('active_package_' . $escort_id, $package);

    return $package;
}


define('HH_STATUS_ACTIVE', 1);
define('HH_STATUS_PENDING', 2);
define('HH_STATUS_EXPIRED', 3);

function getHappyHourStatus($escort_id) {
    global $db, $app;

    $status = $db->fetchOne("SELECT hh_status FROM escorts WHERE id = ?", $escort_id);

    return $status;
}

function dateDiff($date_start, $date_end) {
    $difference = $date_end - $date_start;
    $hours = floor((($difference / 3600) * 60) / 60); // 3600 seconds in an hour

    return $hours;
}

function getAgencyUsedHHHours($agency_id, $escort_id) {
    global $db, $app;

    $escorts = $db->query("SELECT UNIX_TIMESTAMP(hh_date_from) AS hh_date_from, UNIX_TIMESTAMP(hh_date_to) AS hh_date_to FROM escorts WHERE agency_id = ? AND has_hh = 1 AND ( hh_status = 1 || hh_status = 2 )  AND id <> ? AND status & 32", array($agency_id, $escort_id))->fetchAll();

    $time = 0;
    if (count($escorts)) {
        foreach ($escorts as $escort) {
            $time += dateDiff($escort['hh_date_from'], $escort['hh_date_to']);
        }
    }

    return intval($time);
}

function getHappyHour($escort_id) {
    global $db, $app;

    $escort = $db->query("SELECT id AS escort_id, agency_id, has_hh, hh_status, UNIX_TIMESTAMP(hh_date_from) AS hh_date_from, UNIX_TIMESTAMP(hh_date_to) AS hh_date_to, hh_motto, hh_save, showname FROM escorts WHERE id = ?", $escort_id)->fetch();

    $rates = $db->query("SELECT * FROM escort_rates WHERE escort_id = ?", $escort_id)->fetchAll();

    $escort['incall_rates'] = array();
    $escort['outcall_rates'] = array();

    if (count($rates) > 0) {
        foreach ($rates as $rate) {
            if ($rate['availability'] == AVAILABLE_INCALL) {
                $escort['incall_rates'][] = $rate;
            }
            else {
                $escort['outcall_rates'][] = $rate;
            }
        }
    }

    return $escort;
}

function setHappyHour($data) {
    global $db, $app;

    if (count($data) > 0) {
        foreach ($data as $escort_id => $d) {

            $hh_status = getHappyHourStatus($escort_id);

            if ($hh_status == HH_STATUS_PENDING) {

                if (!strlen($d['hh_date_from']) || !strlen($d['hh_date_to'])) {
                    continue;
                }

                $escort_data = array(
                    'hh_motto' => $d['hh_motto'],
                    'hh_date_from' => new Zend_Db_Expr("FROM_UNIXTIME(" . $d['hh_date_from'] . ")"),
                    'hh_date_to' => new Zend_Db_Expr("FROM_UNIXTIME(" . $d['hh_date_to'] . ")"),
                    'hh_save' => $d['hh_save'],
                    //'hh_status' => HH_STATUS_PENDING,
                    'has_hh' => 1
                );

                if (count($d['incall_rates']) > 0) {
                    foreach ($d['incall_rates'] as $rate_id => $incall_rate) {
                        if (!strlen($incall_rate)) $incall_rate = null;
                        $db->update("escort_rates", array('hh_price' => $incall_rate), $db->quoteInto('id = ?', $rate_id));
                    }
                }

                if (count($d['outcall_rates']) > 0) {
                    foreach ($d['outcall_rates'] as $rate_id => $outcall_rate) {
                        if (!strlen($outcall_rate)) $outcall_rate = null;
                        $db->update("escort_rates", array('hh_price' => $outcall_rate), $db->quoteInto('id = ?', $rate_id));
                    }
                }
            }
            else {
                $escort_data = array(
                    'hh_save' => $d['hh_save'],
                );
            }

            $db->update("escorts", $escort_data, $db->quoteInto('id = ?', $escort_id));
        }
    }
}

function resetHappyHour($escort_id) {
    global $db, $app;

    $hh_status = getHappyHourStatus($escort_id);

    if ($hh_status == HH_STATUS_PENDING) {
        $escort_data = array(
            'hh_motto' => null,
            'hh_date_from' => null,
            'hh_date_to' => null,
            'hh_save' => 0,
            'hh_status' => HH_STATUS_PENDING,
            'has_hh' => 0
        );
        $db->update("escorts", $escort_data, $db->quoteInto('id = ?', $escort_id));
        $db->update("escort_rates", array('hh_price' => null), $db->quoteInto('escort_id = ?', $escort_id));
    }
}

function deepResetHappyHour($escort_id) {
    global $db, $app;

    $escort_data = array(
        'hh_motto' => null,
        'hh_date_from' => null,
        'hh_date_to' => null,
        'hh_save' => 0,
        'hh_status' => HH_STATUS_PENDING,
        'has_hh' => 0
    );
    $db->update("escorts", $escort_data, $db->quoteInto('id = ?', $escort_id));
    $db->update("escort_rates", array('hh_price' => null), $db->quoteInto('escort_id = ?', $escort_id));
}

function checkRatesChanged($escort_id, $rates) {
    global $db;

    $cur_rates = $db->fetchAll('SELECT availability, type, time, time_unit, price, currency_id, escort_id FROM escort_rates WHERE escort_id = ?', $escort_id, Zend_Db::FETCH_ASSOC);
    foreach ($cur_rates as $key => $cur_rate) {
        if (is_null($cur_rate['type'])) {
            unset($cur_rates[$key]['type']);
        }
        else {
            unset($cur_rates[$key]['time']);
            unset($cur_rates[$key]['time_unit']);
        }
    }

    $rate_changed = false;

    if (count($cur_rates) != count($rates)) {
        $rate_changed = true;
    }
    else {
        $matches = 0;
        $cycles = 0;
        if (count($rates) > 0) {
            foreach ($rates as $key1 => $array1) {

                $cycles++;
                foreach ($cur_rates as $key2 => $array2) {
                    if ($array1 == $array2) {
                        $matches++;
                        unset($cur_rates[$key2]);
                        BREAK;
                    }
                }

                if ($matches != $cycles) {
                    $rate_changed = true;
                    BREAK;
                }
            }
        }
        else {
            deepResetHappyHour($escort_id);
        }
    }

    return $rate_changed;
}

function addClientToBlacklist($data) {
    global $db, $app;

    if (isset($data['date'])) {
        $data['date'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $data['date'] . ')');
    }

    $photos = $data['photos'];
    unset($data['photos']);

    try {
        $db->insert("blacklisted_clients", $data);
        $bl_id = $db->lastInsertId();
        if ($bl_id && count($photos)) {

            foreach ($photos as $photo) {
                $db->insert('blacklisted_clients_images', array('bl_id' => $bl_id, 'image_id' => $photo));
            }
        }
    } catch (Exception $e) {

    }
}

function getBlacklistedClients($params) {
    global $db, $app;

    $criteria = $params[0];
    $page = $params[1];
    $per_page = $params[2];
    $lang = $params[3];

    $page = intval($page);
    if ($page < 1) $page = 1;
    $limit = $per_page = intval($per_page);
    $start = ($page - 1) * $per_page;

    $result = array();
    $count = 0;

    $criteria_where = "";
    if (strlen($criteria)) {
        $criteria_where = " AND (client_name LIKE '%" . $criteria . "%' OR client_phone LIKE '%" . $criteria . "%' OR `comment` LIKE '%" . $criteria . "%') ";
    }

    $sql = "
		SELECT SQL_CALC_FOUND_ROWS
			UNIX_TIMESTAMP(bc.date) AS date, bc.client_name, bc.client_phone, bc.comment, c.title_{$lang} AS city_title, bc.id
		FROM blacklisted_clients bc
		LEFT JOIN cities c ON c.id = bc.city_id
		WHERE
			bc.application_id = ? AND bc.is_approved = 1 {$criteria_where}
		GROUP BY bc.id
		ORDER BY bc.date DESC
		LIMIT " . $start . ", " . $limit . "
	";

    $countSql = 'SELECT FOUND_ROWS() AS count';

    $result = $db->fetchAll($sql, array($app->id));
    $count = $db->fetchOne($countSql);

    foreach ($result as $i => $r) {
        $sql = "SELECT image_id, hash, ext
					FROM blacklisted_clients_images as bli
					LEFT JOIN images i ON i.id = bli.image_id
					WHERE bli.bl_id = " . $r['id'] . "
				";
        $images = $db->fetchAll($sql);
        if (!empty($images)) {
            $result[$i]['images'] = $images;
        }
    }

    return array('clients' => $result, 'count' => $count);
}

function getFaqByType($type, $lng) {
    global $db, $app;

    return $db->query('SELECT question_' . $lng . ' AS question, answer_' . $lng . ' AS answer FROM faq WHERE application_id = ? AND status = 1 AND FIND_IN_SET(?, type) ORDER BY id DESC', array($app->id, $type))->fetchAll();
}


function addFeedback($data) {
    global $db;

    try {
        $data += array('date' => new Zend_Db_Expr('NOW()'));
        $db->insert('escort_feedbacks', $data);
        $id = $db->lastInsertId();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    // check in DB
    $email = $data['email'];
    $name = $data['name'];

    $c = $db->fetchOne('SELECT COUNT(id) FROM users WHERE email = ?', $email);

    if ($c == 0) {
        $list_id = 3; // 6annone.com-members

        addEmailToNewsletter($list_id, $email, $name);
    }
    //

    return $id;
}

function addEmailToNewsletter($list_id, $email, $name = '') {
    $m = new Cubix_NewsletterApi();

    $params['requesttype'] = 'subscribers';
    $params['requestmethod'] = 'AddSubscriberToList';

    $params['details'] = array(
        'emailaddress' => $email,
        'mailinglist' => $list_id,
        'format' => 'h',
        'confirmed' => 1,
        'customfields' => array(
            'fieldid' => 2,
            'value' => $name
        )
    );

    $m->process($params);
}

function getBlockedWords() {
    global $config, $db, $app;

    $data = $db->fetchAll('
		SELECT * FROM blacklisted_words
	');

    return $data;
}

function getBlockedWordscountries() {
    global $config, $db, $app;

    $data = $db->fetchAll('
		SELECT * FROM blacklisted_words_countries
	');

    return $data;
}

function getBlockedEmails() {
    global $config, $db, $app;

    $data = $db->fetchAll('
		SELECT name FROM blacklisted_emails
	');

    return $data;
}


function checkFeedback($data) {
    global $db;
    try {
        $message = $data['message'];
        $email = $data['email'];
        /*$repCount = $db->fetchOne("
            SELECT COUNT(*) as count FROM escort_feedbacks WHERE message = '$message' AND email = '$email' GROUP BY email
        ");

        if ( $repCount > 8 ){
            $blackData = array();
            $messageIsset = $db->fetchOne("
                SELECT TRUE FROM blacklisted_words WHERE name = '$message'
            ");
            $emailIsset = $db->fetchOne("
                SELECT TRUE FROM blacklisted_emails WHERE name = '$email'
            ");
            if ( !$messageIsset ){
                $blackData['name'] = $message;
                $db->insert('blacklisted_words', $blackData);
            }
            if ( !$emailIsset ){
                $blackData['name'] = $email;
                $db->insert('blacklisted_emails', $blackData);
            }
            return false;
        }*/

        //New logic to avoid spam by Vahag
        $repeatPeriod = 15 * 60; // 15 minutes
        $repCount = $db->fetchOne("
            SELECT COUNT(*) as count FROM escort_feedbacks WHERE message = '$message' AND email = '$email' AND UNIX_TIMESTAMP() - UNIX_TIMESTAMP(date) < $repeatPeriod GROUP BY email
        ");

        if ($repCount > 3) {
            $emailIsset = $db->fetchOne("
                SELECT TRUE FROM blacklisted_emails WHERE name = '$email'
            ");

            if (!$emailIsset) {
                $db->insert('blacklisted_emails', array(
                    'name' => $email,
                    'block_date' => date('Y-m-d H:m:i', time())
                ));
            }
            return false;
        }

        return true;
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }
}

function checkEmail($data) {
    global $db;
    try {
        $email = $data['email'];

        $result = $db->fetchOne("SELECT count(*) FROM blacklisted_emails WHERE name = '$email'");
        list($e_name, $e_domain) = explode('@', $email);
        $resultdomain = $db->fetchOne("SELECT count(*) FROM blacklisted_emails WHERE name = ? ", array('@' . $e_domain));

        if ($result || $resultdomain) {
            return true;
        }

        return false;
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }
}

function checkMemberEmail($data) {
    global $db;
    try {
        $email = $data['email'];
        preg_match("/.+@([a-zA-Z0-9_.]+[.])*([a-zA-Z0-9_]+[.][a-zA-Z]{2,4})$/", $email, $matches);
        $email_host = $matches[2];
        $result = $db->fetchOne("SELECT count(*) FROM member_blacklisted_emails WHERE name = '$email_host'");

        if ($result) {
            return true;
        }

        return false;
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }
}

function getBlockedCountries() {
    global $db;

    $data = $db->fetchAll('
		SELECT * FROM escort_blocked_countries
	');

    return $data;


}

function getBlockedCountriesV2($start = null, $limit = 0) {
    global $db, $app;

    $limit = intval($limit);

    $sql = '
		SELECT bl.* FROM escort_blocked_countries bl
		INNER JOIN escorts e ON e.id = bl.escort_id
		AND (e.status & ' . ESCORT_STATUS_ACTIVE . ' OR e.status & ' . ESCORT_STATUS_INACTIVE . ')
		LIMIT ' . intval($start) . ', ' . $limit;

    try {
        $result = $db->query($sql)->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return array('result' => $result, 'count' => 0);
}

function bookEscort($data) {
    global $db;

    $data['request_date'] = $data['request_date'] ? new Zend_Db_Expr('FROM_UNIXTIME(' . $data['request_date'] . ')') : null;
    $data['contact'] = serialize($data['contact']);
    $data['preferred_time'] = serialize($data['preferred_time']);
    $data['creation_date'] = new Zend_Db_Expr('NOW()');

    $data = $db->insert('booking_requests', $data);
}

function SyncNotifier($escort_id, $event, $data) {

    Cubix_SyncNotifier::notify($escort_id, $event, $data);
}

function premium_getDiamondEscort($escort_id, $lang_id = 'en') {
    $escort_id = intval($escort_id);

    global $db, $app;

    $escorts = $db->fetchAll('
		SELECT p.name AS package, e.id, e.showname, DATEDIFF(op.expiration_date, NOW()) AS expires, op.id AS opid
		FROM escorts e
		INNER JOIN order_packages op ON op.escort_id = e.id
		INNER JOIN order_package_products opp ON op.id = opp.order_package_id
		INNER JOIN packages p ON p.id = op.package_id
		WHERE
			e.id = ? AND
			op.status = ? AND
			op.application_id = ? AND
			op.order_id IS NOT NULL AND
            e.is_suspicious = 0 AND opp.product_id = 1
		GROUP BY e.id
	', array($escort_id, 2, $app->id));

    foreach ($escorts as $i => $escort) {
        if (premium_getEscortCitiesLimit($escort['id']) > 0) {
            $escorts[$i]['cities'] = $db->fetchAll('
				SELECT c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
				FROM premium_escorts pe
				INNER JOIN cities c ON pe.city_id = c.id
				WHERE pe.order_package_id = ?
			', array($escort['id'], $escort['opid']));
        }

        $escorts[$i]['working_cities'] = $db->fetchAll('
			SELECT c.id, c.title_' . $lang_id . ' AS title, ((SELECT city_id FROM escorts WHERE id = ?) = c.id) AS is_base
			FROM escort_cities ec
			INNER JOIN cities c ON ec.city_id = c.id
			WHERE ec.escort_id = ?
		', array($escort['id'], $escort['id']));
    }

    return $escorts;
}

function addareas_getEscorts($id, $user_type) {
    $id = intval($id);

    global $db, $app;

    if ($user_type == 'escort') {
        $escorts = $db->fetchAll('
			SELECT e.id, e.showname, op.add_areas_count, op.id AS order_package_id
			FROM escorts e
			INNER JOIN order_packages op ON op.escort_id = e.id
			WHERE
				e.id = ? AND
				op.status = ? AND
				op.application_id = ?
			GROUP BY e.id
		', array($id, 2, $app->id));
    }
    else if ($user_type == 'agency') {
        $escorts = $db->fetchAll('
			SELECT e.id, e.showname, op.add_areas_count, op.id AS order_package_id
			FROM escorts e
			INNER JOIN order_packages op ON op.escort_id = e.id
			WHERE
				e.agency_id = ? AND
				op.status = ? AND
				op.application_id = ? AND
				op.add_areas_count > 0
			GROUP BY e.id
		', array($id, 2, $app->id));
    }

    foreach ($escorts as $i => $escort) {
        $escorts[$i]['add_areas'] = $db->fetchAll('
			SELECT c.id, c.title_de AS title
			FROM additional_areas aa
			INNER JOIN cities c ON aa.area_id = c.id
			WHERE aa.escort_id = ? AND aa.order_package_id = ?
		', array($escort['id'], $escort['order_package_id']));
    }

    return $escorts;
}

function addareas_getAddAreas($escort_id, $op_id) {
    $escort_id = intval($escort_id);
    $op_id = intval($op_id);

    global $db, $app;

    $add_areas = $db->fetchAll('
		SELECT c.id, c.title_de AS title
		FROM additional_areas aa
		INNER JOIN cities c ON aa.area_id = c.id
		WHERE aa.escort_id = ? AND aa.order_package_id = ?
	', array($escort_id, $op_id));

    return $add_areas;
}

function addareas_getEscortData($escort_id) {
    $escort_id = intval($escort_id);

    global $db, $app;

    $data = $db->fetchRow('
		SELECT e.id, e.showname, op.add_areas_count, op.id AS order_package_id
		FROM escorts e
		INNER JOIN order_packages op ON op.escort_id = e.id
		WHERE
			e.id = ? AND
			op.status = ? AND
			op.application_id = ?
	', array($escort_id, 2, $app->id));

    return $data;
}

function getPackage($id) {
    global $db, $app;

    $sql = '
		SELECT
			op.id,
			e.id AS escort_id,
			e.showname,
			e.agency_id,
			e.gender,

			op.id AS order_package_id,
			op.order_id,
			op.application_id,
			op.base_period,
			op.period,
			op.discount,
			op.discount_fixed,
			op.surcharge,
			op.price,
			op.base_price,
			op.date_activated,
			op.expiration_date,
			/*ADDDATE(op.date_activated, op.period) AS expiration_date,*/

			p.name AS package_name,
			p.id AS package_id,
			c.iso AS app_iso,
			u.balance,
			u.id AS user_id,

			ag.name AS agency_name,

			o.status AS order_status,
			op.status,
			o.id AS order_id
		FROM escorts e
		INNER JOIN users u ON u.id = e.user_id
		LEFT JOIN agencies ag ON ag.id = e.agency_id
		LEFT JOIN order_packages op ON op.escort_id = e.id
		LEFT JOIN orders o ON o.id = op.order_id
		INNER JOIN packages p ON op.package_id = p.id
		INNER JOIN applications a ON a.id = op.application_id
		LEFT JOIN countries c ON c.id = a.country_id
		WHERE op.id = ?
	';

    return $db->fetchRow($sql, array($id));
}

function addareas_updateEscortAreas($add_areas, $escort_id, $op_id) {
    $escort_id = intval($escort_id);
    $op_id = intval($op_id);

    global $db, $app;

    $db->query('DELETE FROM additional_areas WHERE order_package_id = ? AND escort_id = ?', array($op_id, $escort_id));

    foreach ($add_areas as $area_id) {
        $data = array(
            'order_package_id' => $op_id,
            'escort_id' => $escort_id,
            'area_id' => $area_id
        );

        $db->insert('additional_areas', $data);
    }

    Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('esc_id' => $escort_id));
}

function updateExpirationDate($new_exp_date, $op_id, $escort_id) {
    global $db, $app;

    $db->update('escorts', array('package_expiration_date' => new Zend_Db_Expr('FROM_UNIXTIME(' . $new_exp_date . ')')), $db->quoteInto('id = ?', $escort_id));
    $db->update('order_packages', array('expiration_date' => new Zend_Db_Expr('FROM_UNIXTIME(' . $new_exp_date . ')')), $db->quoteInto('id = ?', $op_id));
}


function suspresume_SuspendPackage($op_id, $date) {
    global $db, $app;

    if (!$op_id) return;

    $package = getPackage($op_id);


    $data = array(
        'status' => 6,
        'date_suspended' => new Zend_Db_Expr('NOW()'),
        'sus_res_date' => null
    );

    if ($date) {
        $data['sus_res_date'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $date . ')');
    }

    $db->update('order_packages', $data, $db->quoteInto('id = ?', $op_id));

    $sr_date = array(
        'order_package_id' => $op_id,
        'suspend_date' => new Zend_Db_Expr('NOW()')
    );
    $db->insert('suspend_resume_dates', $sr_date);

    $days_left = _getDaysLeft($package, $op_id);
    $db->update('order_packages', array('days_left' => $days_left), $db->quoteInto('id = ?', $op_id));

    Cubix_SyncNotifier::notify($package['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('escort_id' => $package['escort_id'], 'action' => 'package suspended'));

    $st = new Cubix_EscortStatus($package['escort_id']);
    $st->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);
    $st->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED);

    $st->save();
}

function suspresume_ResumePackage($op_id, $date) {
    global $db, $app;

    if (!$op_id) return;

    $package = getPackage($op_id);

    $suspend_max_date = $db->fetchRow('SELECT MAX(suspend_date) AS max_date FROM suspend_resume_dates WHERE order_package_id = ?', array($op_id));
    $db->query('UPDATE suspend_resume_dates SET resume_date = NOW() WHERE suspend_date = ? AND order_package_id = ?', array($suspend_max_date['max_date'], $op_id));

    $expiration_date = $db->fetchOne('SELECT UNIX_TIMESTAMP(expiration_date) AS expiration_date FROM order_packages WHERE id = ?', array($op_id));
    $days_to_extend = _getDaysToExtend($package, $op_id);


    $new_expiration_date = $expiration_date + ($days_to_extend * 24 * 60 * 60);

    updateExpirationDate($new_expiration_date, $op_id, $package['escort_id']);

    $data = array(
        'status' => 2,
        'date_suspended' => null,
        'sus_res_date' => null
    );

    if ($date) {
        $data['sus_res_date'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $date . ')');
    }

    $db->update('order_packages', $data, $db->quoteInto('id = ?', $op_id));

    $db->update('order_packages', array('days_left' => null), $db->quoteInto('id = ?', $op_id));

    Cubix_SyncNotifier::notify($package['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_UPDATED, array('escort_id' => $package['escort_id'], 'action' => 'package resumed'));

    $st = new Cubix_EscortStatus($package['escort_id']);
    $st->removeStatusBit(Cubix_EscortStatus::ESCORT_STATUS_OWNER_DISABLED);
    $st->setStatusBit(Cubix_EscortStatus::ESCORT_STATUS_ACTIVE);

    $st->save();
}

function dateDiffDays($date_start, $date_end) {
    $difference = $date_end - $date_start;
    $days = $difference / 86400; // 3600 seconds in an hour

    if ($days < 0) $days = 0;

    return $days;
}

function _getDaysToExtend($package, $op_id) {
    global $db, $app;

    $suspend_resume_date = $db->fetchRow('SELECT suspend_date, resume_date FROM suspend_resume_dates WHERE order_package_id = ? ORDER BY resume_date DESC', array($op_id));

    $extend_days = dateDiffDays(strtotime($suspend_resume_date['suspend_date']), strtotime($suspend_resume_date['resume_date']));
    if ($extend_days > 1) {
        $extend_days = floor($extend_days);
    }

    return $extend_days;
}

function suspresume_Details($op_id) {
    global $db, $app;

    if (!$op_id) return;

    $package = getPackage($op_id);

    return $db->fetchAll('SELECT suspend_date, resume_date FROM suspend_resume_dates WHERE order_package_id = ?', array($op_id));
}

function _getDaysLeft($package, $op_id) {
    global $db, $app;


    $suspend_date = $db->fetchOne('SELECT UNIX_TIMESTAMP(DATE(suspend_date)) FROM suspend_resume_dates WHERE order_package_id = ? ORDER BY suspend_date DESC', $op_id);
    $expiration_date = $db->fetchOne('SELECT UNIX_TIMESTAMP(expiration_date) expiration_date FROM order_packages WHERE id = ?', $op_id);

    $days_left = dateDiffDays($suspend_date, $expiration_date); //Fixing expiration_date logic

    return $days_left;
}

function getStatistics($data) {
    global $db, $app;

    $sort_array = array(
        1 => "showname",
        2 => "sedcard_view_count",
        3 => "message_count",
        4 => "photo_view_count",
    );

    $id = $agency_id = intval($data['agency_id']);
    $escort_id = intval($data['escort_id']);
    $date_from = intval($data['date_from']);
    $date_to = intval($data['date_to']);

    $order_by = intval($data['order_by']);
    $order_dir = intval($data['order_dir']);


    $order = '';
    $where = '';
    if ($agency_id) {
        $where = 'agency_id = ?';
    }

    if ($escort_id) {
        $where = 'e.id = ?';
        $id = $escort_id;
    }

    $order_d = '';
    if ($order_dir == 1) {
        $order_d = " DESC";
    }

    if (key_exists($order_by, $sort_array)) {
        $order = "ORDER BY `$sort_array[$order_by]`$order_d";
    }

    $where2 = 'escort_id = ?';
    if ($date_from) {
        $where2 .= ' AND DATE(date) >= DATE(FROM_UNIXTIME(' . $date_from . '))';
    }

    if ($date_to) {
        $where2 .= ' AND DATE(date) <= DATE(FROM_UNIXTIME(' . $date_to . '))';
    }

    $results = $db->fetchAll('
		SELECT e.showname,
			e.id,
			sr.type,
			sr.status
		FROM escorts e
		LEFT JOIN statistics_reports sr ON sr.escort_id = e.id
		WHERE ' . $where . '  AND NOT e.status & ' . ESCORT_STATUS_DELETED . '
	', array($id));


    foreach ($results as $key => $result) {

        $results[$key]['sedcard_view_count'] = $db->fetchOne('
			SELECT SUM(count) as v_c FROM escort_hits_daily 
			WHERE ' . $where2 . ' GROUP BY escort_id
			', array($result['id']));

        $results[$key]['message_count'] = $db->fetchOne('
			SELECT COUNT(id) FROM escort_feedbacks 
			WHERE ' . $where2 . ' GROUP BY escort_id
			', array($result['id']));

        $results[$key]['photo_view_count'] = $db->fetchOne('
			SELECT SUM(count) FROM escort_photo_hits_daily
			WHERE ' . $where2 . ' GROUP BY escort_id
			', array($result['id']));
    }

    /*$results = $db->fetchAll('
		SELECT e.showname,
			e.id,
			v_c as sedcard_view_count,
			m_c as message_count,
			p_c as photo_view_count,
			sr.type,
			sr.status
		FROM escorts e
		LEFT JOIN
		(
		SELECT s.escort_id,SUM(p_c) as p_c,SUM(m_c) as m_c,SUM(v_c) as v_c FROM
		(
		SELECT escort_id,NULL as p_c ,NULL as m_c,SUM(count) as v_c FROM
		escort_hits_daily ehd WHERE '.$where2.' GROUP BY escort_id
		UNION
		SELECT escort_id,NULL as p_c ,COUNT(id) as m_c,NULL as v_c FROM
		escort_feedbacks ef WHERE '.$where2.' GROUP BY escort_id
		UNION
		SELECT escort_id,SUM(count) as p_c,NULL as m_c,NULL as v_c FROM
		escort_photo_hits_daily ephd WHERE '.$where2.' GROUP BY escort_id
		) s
		 GROUP BY s.escort_id ) as tmp
		ON tmp.escort_id = e.id
		LEFT JOIN statistics_reports sr ON sr.escort_id = e.id
		WHERE '.$where.'  AND NOT e.status & ' . ESCORT_STATUS_DELETED . '
		GROUP BY e.id
			'.$order.'
	', array($id));*/


    return $results;
}

function updateReport($data) {
    global $db, $app;

    if ($tmp = $db->fetchRow('SELECT * FROM statistics_reports WHERE escort_id = ?', $data['escort_id'])) {
        $escort_id = $data['escort_id'];
        unset($data['escort_id']);


        if ($data['status'] != $tmp['status']) {
            $data['status'] = ($data['status'] + $tmp['status']);
        }

        return $db->update('statistics_reports', $data, $db->quoteInto('escort_id = ?', $escort_id));
    }
    else {
        return $db->insert('statistics_reports', $data);
    }
}

function getReportStatus($escort_id) {
    global $db, $app;
    $escort_id = intval($escort_id);

    return $db->fetchOne('SELECT status FROM statistics_reports WHERE escort_id = ?', $escort_id);
}

function contactLog($to_email, $from_user_id, $ip = null) {
    global $db, $app;

    $arr = array(
        'date' => new Zend_Db_Expr('DATE(NOW())'),
        'application_id' => $app->id,
        'to_email' => $to_email,
        'ip' => $ip
    );

    if (!is_null($from_user_id))
        $arr['from_user_id'] = $from_user_id;

    $db->insert('contact_log', $arr);
}


function checkIp($ip) {
    global $db, $app;
    return $db->fetchOne('SELECT block FROM blacklisted_ips WHERE ip = ?', $ip);
}

function blockIp($agent, $ip) {
    global $db, $app;

    $isset = $db->fetchOne('SELECT true FROM blacklisted_ips WHERE ip = ?', $ip);

    if (!$isset) {
        $db->query("INSERT INTO blacklisted_ips ( user_agent, ip, block ) VALUES ('" . $agent . "', '" . $ip . "', 0)");
    }

    return 1;
}


function getBlockedIps() {
    global $db, $app;
    return $db->fetchAll('SELECT * FROM blacklisted_ips WHERE block = 1');
}

function getMemberAlerts($user_id) {
    global $db;

    $user_id = intval($user_id);

    return $db->fetchAll('SELECT escort_id, event, extra FROM alert_users WHERE user_id = ? ORDER BY escort_id ASC', array($user_id));
}

function getBubbleText($escort_id) {
    global $db;

    try {
        $ret = $db->query('
			SELECT
				bt.id, bt.date, bt.text, bt.status,
				IF( DATE(bt.date) = DATE(NOW()), 0, 1) AS free_bubble_left/*,
				IF( date(bpt.date) = date(NOW()), 1, 0 ) AS has_paid_bubble*/
			FROM escort_bubbletexts bt
			INNER JOIN escorts e ON e.id = bt.escort_id
			/*LEFT JOIN bubble_phone_transfers bpt ON bpt.id = e.bubble_transfer_id*/
			WHERE bt.escort_id = ? AND bt.status = 1 AND e.gender <> 2
			ORDER BY bt.date DESC
		', $escort_id)->fetch();

        $has_today = $db->query('
			SELECT
				TRUE AS dd
			FROM escort_bubbletexts bt
			INNER JOIN escorts e ON e.id = bt.escort_id

			WHERE bt.escort_id = ? AND e.gender <> 2 AND DATE(bt.date) = DATE(NOW())
			ORDER BY bt.date DESC
		', $escort_id)->fetch();

        $ret['has_today'] = $has_today['dd'];

        return $ret;

    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }
}

function addBubbleText($data, $appovation) {
    global $db, $app;

    if ($app->id == APP_ED) {
        $appovation = 1;
    }

    try {
        $one_per_day = $db->fetchOne('SELECT TRUE FROM escort_bubbletexts WHERE escort_id = ? AND DATE(date) = DATE(NOW()) /*AND status = 1*/', $data['escort_id']);

        if ($data['status'] != 0) {
            if ($one_per_day) {
                return 'one_per_day_limit';
            }
        }

        $last = $db->fetchRow('SELECT id, text, status FROM escort_bubbletexts WHERE escort_id = ? ORDER BY date DESC LIMIT 1', $data['escort_id']);

        if ($last) {
            $db->query('UPDATE escort_bubbletexts SET status = 0 WHERE escort_id = ' . $data['escort_id'] . ' AND id <> ' . $last['id']);
        }

        if ($data['status'] == 0) {
            $db->update('escort_bubbletexts', array('status' => 0), $db->quoteInto('id = ?', $last['id']));
            Cubix_SyncNotifier::notify($data['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_BUBBLE_TEXT, array(
                'old' => $last['text'],
                'action' => 'removed'
            ));
            return;
        }
        elseif ($data['status'] == 1) {

            if ($last && $data['text'] == $last['text']) {
                if ($app->id == APP_A6 || $app->id == APP_A6_AT) {
                    $db->update('escort_bubbletexts', array('status' => 1, 'date' => new Zend_Db_Expr('NOW()')), $db->quoteInto('id = ?', $last['id']));
                }
                else {
                    $db->update('escort_bubbletexts', array('status' => 1), $db->quoteInto('id = ?', $last['id']));
                }
                Cubix_SyncNotifier::notify($data['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_BUBBLE_TEXT, array(
                    'old' => $last['text'],
                    'action' => 'activated'
                ));
            }
            else {
                $db->update('escort_bubbletexts', array('status' => 0), $db->quoteInto('escort_id = ?', $data['escort_id']));
                $data['approvation'] = intval($appovation);
                $db->insert('escort_bubbletexts', $data);
                Cubix_SyncNotifier::notify($data['escort_id'], Cubix_SyncNotifier::EVENT_ESCORT_BUBBLE_TEXT, array(
                    'old' => $last['text'],
                    'new' => $data['text'],
                    'action' => 'changed'
                ));
            }
        }
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }
}

function addPaidBubbleText($data, $appovation) {
    global $db, $app;

    try {

        $data['approvation'] = intval($appovation);
        $db->insert('escort_paid_bubbletexts', $data);

        return $db->lastInsertId();

    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }
}


function updateEscortProfileSimple($escort_id, $data) {

    global $db;
    $escort_id = intval($escort_id);

    try {
        $db->update('escort_profiles_v2', $data, $db->quoteInto('escort_id = ?', $escort_id));

        snapshotProfileV2($escort_id);

        //Revisions update
        $revision_data = $db->fetchRow('SELECT data, revision FROM profile_updates_v2 WHERE escort_id = ? ORDER BY revision DESC LIMIT 1', array($escort_id));

        if ($revision_data) {
            $revision = $revision_data['revision'];
            $revision_data = unserialize($revision_data['data']);
            $revision_data = array_merge($revision_data, $data);

            // TEST unserialize and send email if return false
            $serialized_data = serialize($revision_data);
            if (false === unserialize($serialized_data)) {
                return;
            }

            $db->update('profile_updates_v2', array('data' => $serialized_data), array($db->quoteInto('escort_id = ?', $escort_id), $db->quoteInto('revision = ?', $revision)));
            Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_UPDATED);
        }
        else {

            // TEST unserialize and send email if return false
            $serialized_data = serialize($data);
            if (false === unserialize($serialized_data)) {
                return;
            }

            $revision += 1;

            $db->insert('profile_updates_v2', array(
                'escort_id' => $escort_id,
                'revision' => $revision,
                'date_updated' => new Zend_Db_Expr('NOW()'),
                'data' => $serialized_data
            ));
            Cubix_SyncNotifier::notify($escort_id, Cubix_SyncNotifier::EVENT_ESCORT_REV_CREATED, array('rev' => $revision));
        }


    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

}

function updateAgencyPhones($agency_id, $phone_data)
{
	global $db;
	try{
		$agency_escorts = $db->fetchAll('SELECT id, status FROM escorts WHERE agency_id = ? AND NOT status & 256 ', $agency_id);
				
		/*ignore_user_abort(true);
		ob_start();
		echo serialize(array('bool' => true));
		header('Connection', 'close');
		header('Content-Length: '.ob_get_length());	
		ob_end_flush();
		ob_flush();
		flush();
		session_write_close();
		fastcgi_finish_request();*/
		
		foreach($agency_escorts as $i => $escort){
			
			
			updateEscortProfileSimple($escort['id'], $phone_data);
			
		}
	 } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }	
		
}

function getOnlineNews($app_id) {
    global $db;

    try {
        return $db->fetchRow('
			SELECT	' . Cubix_I18n::getTblFields('subject', $app_id) . ', title, start_date
			FROM online_news
			WHERE status = 1
		');
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

}

function getVideosTableSqlDump() {

    global $app;

    $fields = '';

    if ($app->id == APP_A6) {
        $fields .= '`vote_count` int(10) UNSIGNED NOT NULL DEFAULT 0,';
        $fields .= '`is_votd` TINYINT(1) DEFAULT 0,';
        $fields .= '`votd_city_id` INT(10) DEFAULT NULL,';
    }

    if ($app->id == APP_EF) {
        $fields .= '`cover_hash` VARCHAR(32) DEFAULT NULL,';
        $fields .= '`cover_ext` VARCHAR(5) DEFAULT NULL,';
        $fields .= '`cover_status` TINYINT(1) DEFAULT NULL,';
    }

    return '
		CREATE TABLE `video` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`escort_id` int(10) unsigned NOT NULL,
		`video` varchar(255) NOT NULL,
		`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		`title` varchar(255) NOT NULL,
		`height`  smallint(4) UNSIGNED NOT NULL DEFAULT 0,
		`width`  smallint(4) UNSIGNED NOT NULL DEFAULT 0,' .
        $fields .
        ' PRIMARY KEY (`id`),
		KEY `escort_id` (`escort_id`)
		)
		ENGINE=InnoDB
		AUTO_INCREMENT=0
		DEFAULT CHARSET=utf8;
	';
}

function getVideosImageTableSqlDump() {
    return "
		CREATE TABLE `video_image` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`video_id` int(11) NOT NULL,
			`hash` varchar(255) NOT NULL,
			`ext` varchar(25) NOT NULL,
			`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			`width` int(11) NOT NULL,
			`height` int(11) NOT NULL,
			`is_approve` enum('0','1') NOT NULL DEFAULT '1',
			`is_mid` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
			PRIMARY KEY (`id`),
			KEY `video_image_ibfk_1` (`video_id`)
			/*CONSTRAINT `video_image_ibfk_1` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE ON UPDATE CASCADE*/
			)
			ENGINE=InnoDB
			AUTO_INCREMENT=0
			DEFAULT CHARSET=utf8
	";
}

function getAllVideo() {
    global $db, $app;

    $data = array('video' => array(), 'image' => array());

    $sql_active_package = addActivePackageWhereParams();

    $fields = '';
    if ($app->id == APP_A6) {
        $fields .= ', v.vote_count, v.is_votd, v.votd_city_id';
    }

    if ($app->id == APP_EF) {
        $fields .= ', v.cover_status , v.cover_ext , v.cover_hash';
    }

    $data['video'] = $db->fetchAll("
		SELECT
			v.id, v.video, v.escort_id, v.`date`, v.title, v.height, v.width " . $fields .
        " FROM video v
		INNER JOIN escorts e ON e.id = v.escort_id
		" . $sql_active_package['join'] . "
		WHERE v.`status` = 'approve' " . $sql_active_package['where'] . "
	");

    if (!empty($data['video'])) {
        $v_id = '';

        foreach ($data['video'] as $v) {
            $v_id .= "'" . $v['id'] . "',";
        }

        $v_id = substr($v_id, 0, strlen($v_id) - 1);

        $images = $db->fetchAll("SELECT `hash`, ext, width, height, video_id FROM images JOIN video_image ON id = image_id AND video_id IN($v_id) ");
        foreach ($images as $img) {
            if (!isset($data['image'][$img['video_id']])) {
                $data['image'][$img['video_id']] = array();
            }
            $data['image'][$img['video_id']][] = $img;
        }

    }

    return $data;
}

function getEscortSmsLng($escort_id) {
    global $db;

    return $db->fetchOne('SELECT u.sms_lng FROM users u INNER JOIN escorts e ON e.user_id = u.id WHERE e.id = ?', $escort_id);
}

function setUserSmsLng($user_id, $lng) {
    global $db;

    $db->update('users', array('sms_lng' => $lng), $db->quoteInto('id = ?', $user_id));
}

function setSmsVerified($escort_id) {
    global $db;

    $escort_id = intval($escort_id);
    try {
        $db->update('escort_profiles_v2', array('phone_sms_verified' => 1), $db->quoteInto('escort_id = ?', $escort_id));
        snapshotProfileV2($escort_id);
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }
}


function isInAlerts($user_id, $escort_id) {
    global $db;

    try {
        return $db->fetchOne('SELECT COUNT(*) FROM alert_users WHERE user_id = ? AND escort_id = ?', array($user_id, $escort_id));
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }
}

function isInFavorites($user_id, $escort_id) {
    global $db;

    return $db->fetchOne('SELECT 1 FROM favorites WHERE user_id = ? AND escort_id = ?', array($user_id, $escort_id));
}

function getFavorites($user_id) {
    global $db;

    $favorites = $db->fetchAll('SELECT escort_id FROM favorites WHERE user_id = ? ', array($user_id));
    $favorites_array = array();
    foreach ($favorites as $favorite) {
        $favorites_array[] = $favorite['escort_id'];
    }

    return $favorites_array;
}

function getBlacklistedClientsHotels($params) {
    global $db, $app;

    $search_str = $params[0];
    $country_id = $params[1];
    $page = $params[2];
    $per_page = $params[3];
    $type = $params[4];
    $lang = $params[5];

    $page = intval($page);
    if ($page < 1) $page = 1;
    $limit = $per_page = intval($per_page);
    $start = ($page - 1) * $per_page;

    $result = array();
    $count = 0;

    $criteria_where = "";
    if (strlen($search_str)) {
        $criteria_where = " AND (name LIKE '%" . $search_str . "%' OR phone LIKE '%" . $search_str . "%' OR `comment` LIKE '%" . $search_str . "%') ";
    }

    if (strlen($country_id)) {
        $criteria_where = " AND bc.country_id = " . $country_id;
    }

    $sql = "
		SELECT SQL_CALC_FOUND_ROWS
			UNIX_TIMESTAMP(bc.date) AS date, bc.name, bc.phone, bc.comment, c.title_{$lang} AS city_title, bc.id, bc.type,
			cr.title_{$lang} AS country_title
		FROM blacklisted_clients_hotels bc
		LEFT JOIN countries cr ON cr.id = bc.country_id
		LEFT JOIN cities c ON c.id = bc.city_id
		WHERE
			bc.application_id = ? AND bc.is_approved = 1 {$criteria_where} AND bc.type = '{$type}'
		GROUP BY bc.id
		ORDER BY bc.date DESC
		LIMIT " . $start . ", " . $limit . "
	";

    $countSql = 'SELECT FOUND_ROWS() AS count';

    $result = $db->fetchAll($sql, array($app->id));
    $count = $db->fetchOne($countSql);

    foreach ($result as $i => $r) {
        $sql = "SELECT image_id, hash, ext
					FROM blacklisted_clients_images as bli
					LEFT JOIN images i ON i.id = bli.image_id
					WHERE bli.bl_id = " . $r['id'] . "
				";
        $images = $db->fetchAll($sql);
        if (!empty($images)) {
            $result[$i]['images'] = $images;
        }
    }

    return array('clients' => $result, 'count' => $count);
}

function addClientHotelToBlacklist($data) {
    global $db, $app;

    if (isset($data['date'])) {
        $data['date'] = new Zend_Db_Expr('FROM_UNIXTIME(' . $data['date'] . ')');
    }

    $photos = $data['photos'];
    unset($data['photos']);


    try {
        $db->insert("blacklisted_clients_hotels", $data);
        $bl_id = $db->lastInsertId();

        if ($bl_id && count($photos)) {

            foreach ($photos as $photo) {
                $db->insert('blacklisted_clients_images', array('bl_id' => $bl_id, 'image_id' => $photo));
            }
        }
    } catch (Exception $e) {

    }
}

function getAdminBlacklistEntries() {
    global $db, $app;

    $result = array();
    $count = 0;

    $sql = '
			SELECT
				id, comment
			FROM admin_blacklist_entry
			WHERE is_approved = 1
			ORDER BY id desc
		';

    $result = $db->fetchAll($sql);
    foreach ($result as $i => $r) {
        $sql = "SELECT image_id, hash, ext
					FROM admin_blacklist_entry_images as abe
					LEFT JOIN images i ON i.id = abe.image_id
					WHERE abe.ae_id = " . $r['id'] . "
				";

        $images = $db->fetchAll($sql);
        if (!empty($images)) {
            $result[$i]['images'] = $images;
        }
    }

    return array('entries' => $result, 'count' => count($result));
}

function getNeedVideoVerify($escort_id) {
    global $db;

    return $db->fetchOne("SELECT need_age_verify_video FROM escorts WHERE id = ? ", array($escort_id));
}

function getIdCardVideoVerify($escort_id) {
    global $db;

    return $db->fetchOne("SELECT need_100verify_video FROM escorts WHERE id = ? ", array($escort_id));
}


function getProfileBoosts() {
    global $db, $app;

    $sql = '
		SELECT escort_id, city_id, activation_date, hour_from, hour_to
		FROM profile_boost_orders
		WHERE activation_date = CURDATE() AND status = 2
	';

    try {
        $result = $db->query($sql, array($app->id))->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return array('result' => $result, 'count' => 0);
}

function ratePhotoFeedPhoto($photo_id, $rate, $ip, $escort_id) {
    global $db, $app;

    try {
        $result = $db->insert('escort_photo_votes', array('photo_id' => $photo_id, 'rating' => $rate, 'ip' => $ip, 'escort_id' => $escort_id));
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return $result;
}

/**
 *
 * Selecting from Backend DB by passed db table name and column
 * This is for very small select not for joins
 *
 * @param array columns name you can pass ALL for getting all columns
 * @param string table name
 * @param where must be two dimensional array (key, value)
 * @return   array
 *
 */

function customSelect($columns, $table, $where = array()) {
    global $db;

    $sql = 'SELECT ';

    if (is_array($columns)) {
        foreach ($columns as $column) {

            if ($column === end($columns)) {
                $sql .= $column;
            }
            else {
                $sql .= $column . ', ';
            }

        }
    }
    elseif ($columns == 'ALL' || $columns == '*') {
        $sql = 'SELECT *';
    }
    elseif (is_string($columns)) {
        $sql = 'SELECT ' . $columns;
    }

    $sql .= ' FROM ' . $table . ' WHERE 1';

    if (is_array($where)) {
        foreach ($where as $column => $value) {
            $sql .= " AND $column = '$value'";
        }
    }

    try {
        $result = $db->query($sql)->fetchAll();
    } catch (Exception $e) {
        return array('error' => Cubix_Api_Error::Exception2Array($e));
    }

    return $result[0];
}

function getVipSpots() {
    global $config, $db, $app;

    $data = $db->fetchAll('
		SELECT * FROM vip_hardcodes
	');

    return $data;
}

function _getHandcheckData($escort_id)
{
	global $db;
    return $db->fetchRow('SELECT last_hand_verification_date, hand_verification_sales_id
						FROM escorts WHERE last_hand_verification_date IS NOT NULL AND id = ?', array($escort_id));
	
}
