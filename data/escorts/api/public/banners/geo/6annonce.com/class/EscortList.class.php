<?php
require_once("./config/conf.php");
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class EscortList
{
    private $shortListMode = false;
    private $geoData = array();
    private $template;
    private $application_id = 2;
    private $_application = "6annonce.com";
    private $escortCount;

    private $country;
    private $region;
    private $city;

	private $db_params = array();
	
    /* Constructor ----- */
    public function __construct($params = null)
    {
		global $mysql_params;

        // Setting up parameters
        if(is_array($params))
        {
            if(isset($params['template']))
                $this->template = $params['template'];

            if(isset($params['escortCount']))
                $this->escortCount = $params['escortCount'];

            if(isset($params['application_id']))
                $this->application_id = $params['application_id'];

            if(isset($params['shortListMode']))
                $this->shortListMode = $params['shortListMode'];

            if(isset($params['geoData']) && is_array($params['geoData']))
            {
                $this->geoData = $params['geoData'];

                // Setting country/region/city
                $this->country = str_replace(" ", "-", strtolower($this->geoData['country_iso']));
                $this->city = str_replace(" ", "-", strtolower($this->geoData['city_name']));
                $this->region = ($this->geoData['region_short_name']) ? $this->geoData['region_short_name'] : '';
            }
        }

		$this->db_params = $mysql_params;
		
		// Initializing DB connection
		mysql_connect($this->db_params['HOST'], $this->db_params['LOGIN'], $this->db_params['PASSWORD']);
		mysql_select_db($this->db_params['NAME']);

		mysql_query('SET NAMES `utf8`');
    }

    /* Setting template and escort count on the banner */
    public function setTemplate($template, $escortCount)
    {
        $this->template = (string) $template;
        $this->escortCount = (int) $escortCount;
    }

    /*  Enabling/Disabling ShortList mode   */
    public function setShortlistMode($mode)
    {
        $this->shortlist = ((bool)$mode) ? true : false;
    }

    /* Main method: returns the list of escorts required for banner */
    public function getList()
    {
        if ($this->shortListMode == true)
		{
            $list = $this->getEscortsShortlisted();

			if (count($list) < $this->escortCount)
			{
				$this->shortListMode = false;
				$list = $this->getEscortsAll();
			}

			return $list;
        }
        else
		{
			return $this->getEscortsAll();
        }
    }

    /* returns Escorts from shortlisted table */
    private function getEscortsShortlisted()
    {
		//$file = $this->template . '.php';
		$file = $this->template;
		$sql = $this->baseSql(" AND b.template = '{$file}' AND c.slug = '{$this->city}'");
		$result = mysql_query($sql);

		$ids = array();
		$array = array();

		while( $row = mysql_fetch_object($result) )
		{
			$array[] = $row;
			$ids[] = $row->id;
		}

		if ( $limit = ($this->escortCount  -  sizeof($ids)) > 0)
		{
			$where = sizeof($ids) ? ' AND b.escort_id NOT IN (' . implode(', ', $ids) . ')' : '';

			$sql = "
				SELECT
					b.escort_id AS id,
					e.showname,
					c.title_en AS city_name,
					ep.hash AS photo_hash,
					ep.ext AS photo_ext,
					b.template AS template
				FROM banner_escorts b
				INNER JOIN escorts e ON e.id = b.escort_id
				INNER JOIN users u ON u.id = e.user_id
				INNER JOIN escort_cities ec ON b.escort_id = ec.escort_id
				INNER JOIN cities c ON c.id = ec.city_id
				LEFT JOIN regions r ON r.id = c.region_id
				INNER JOIN escort_photos ep ON ep.escort_id = e.id
				INNER JOIN applications a ON a.id = {$this->application_id} AND a.country_id = u.country_id
				WHERE ep.is_main = 1 AND e.status = 32 AND e.country_id = 216 AND u.application_id = {$this->application_id} AND r.state = '{$this->region}' AND b.template = '{$file}' {$where}
				ORDER BY RAND()
				LIMIT {$limit}
			";

			$result = mysql_query($sql);

			while( $row = mysql_fetch_object($result) )
			{
				$array[] = $row;
				$ids[] = $row->id;
			}
		}
		else {
			if ( count($array) ) {
				foreach( $array as $k => $a ) {
					$entry = array(
						'escort_id' => $a->id,
						'ext' => $a->photo_ext,
						'hash' => $a->photo_hash,
						'size' => 'thumb'
					);
					$array[$k]->photo = $this->_getFullPath($entry);
				}
			}
			return $array;
		}

		if ( $limit = ($this->escortCount - count($array)) > 0 )
		{
			$where = sizeof($ids) ? " AND b.escort_id NOT IN (" . implode(", ", $ids) . ") AND b.template = '{$file}'" : " AND b.template = '{$file}'";
			$sql = $this->baseSql($where);

			$result = mysql_query($sql);

			while( $row = mysql_fetch_object($result) )
			{
				$array[] = $row;
			}

		}

		if ( count($array) ) {
			foreach( $array as $k => $a ) {
				$entry = array(
					'escort_id' => $a->id,
					'ext' => $a->photo_ext,
					'hash' => $a->photo_hash,
					'size' => 'thumb'
				);
				$array[$k]->photo = $this->_getFullPath($entry);
			}
		}

		return $array;
    }

    /* returns Escorts from whole DB */
    private function getEscortsAll()
    {
		$sql = $this->baseSql("AND c.slug = '{$this->city}'");

		$result = mysql_query($sql);

		$ids = array();
		$array = array();

		while( $row = mysql_fetch_object($result) )
		{
			$array[] = $row;
			$ids[] = $row->id;
		}

		if ( ($limit = $this->escortCount - sizeof($array)) > 0 )
		{
			$where = count($ids) ? 'AND e.id NOT IN (' . implode(', ', $ids) . ')' : '';
			$sql = $this->baseSql($where);

			$result = mysql_query($sql);

			while( $row = mysql_fetch_object($result) )
			{
				$array[] = $row;
			}
		}

		if ( count($array) ) {
			foreach( $array as $k => $a ) {
				$entry = array(
					'escort_id' => $a->id,
					'ext' => $a->photo_ext,
					'hash' => $a->photo_hash,
					'size' => 'thumb'
				);
				$array[$k]->photo = $this->_getFullPath($entry);
			}
		}

		return $array;
    }

    /* Prepares the base SQL statement */
    private function baseSql($where)
    {
		if ($this->shortListMode == false)
		{
			$sql = "
				SELECT
					e.id,
					e.showname,
					ep.hash AS photo_hash,
					ep.ext AS photo_ext,
					c.title_en AS city_name,
					c.slug AS city_slug
				FROM
					escorts AS e
				INNER JOIN cities c ON c.id = e.city_id
				INNER JOIN escort_photos ep ON ep.escort_id = e.id
				INNER JOIN users u ON u.id = e.user_id
				INNER JOIN applications a ON a.id = {$this->application_id} AND a.country_id = u.country_id
				WHERE ep.is_main = 1 AND e.status = 32 AND e.gender = 1 AND e.country_id = 71 AND u.application_id = {$this->application_id} {$where}
				GROUP BY e.id
				ORDER BY RAND()
				LIMIT {$this->escortCount}
			";
			
		}
		else
		{
			$sql = "
				SELECT
					b.escort_id AS id,
					e.showname,
					c.title_en AS city_name,
					ep.hash AS photo_hash,
					ep.ext AS photo_ext,
					b.template
				FROM banner_escorts b
				INNER JOIN escorts e ON e.id = b.escort_id
				INNER JOIN users u ON u.id = e.user_id
				INNER JOIN escort_cities ec ON b.escort_id = ec.escort_id
				INNER JOIN cities c ON c.id = ec.city_id
				INNER JOIN escort_photos ep ON ep.escort_id = e.id
				INNER JOIN applications a ON a.id = {$this->application_id} AND a.country_id = u.country_id
				WHERE ep.is_main = 1 AND e.status & 32 AND e.country_id = 71 AND u.application_id = {$this->application_id} {$where}
				GROUP BY b.escort_id
				ORDER BY RAND()
				LIMIT {$this->escortCount}
			";
		}

		return $sql;
    }

	protected function _getFullPath($entry)
	{
		$application = $this->_application;

		/*if ( isset($entry->application_id) ) {
			$application = Cubix_Application::getById($entry->application_id)->host;
		}*/

		$catalog = $entry['escort_id'];
		$parts = array($catalog);

		$a = array();
		if ( is_numeric($catalog) ) {
			$parts = array();

			if ( strlen($catalog) > 2 ) {
				$parts[] = substr($catalog, 0, 2);
				$parts[] = substr($catalog, 2);
			}
			else {
				$parts[] = '_';
				$parts[] = $catalog;
			}
		}
		else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
			array_shift($a);
			$catalog = $a[0];

			$parts = array();

			if ( strlen($catalog) > 2 ) {
				$parts[] = substr($catalog, 0, 2);
				$parts[] = substr($catalog, 2);
			}
			else {
				$parts[] = '_';
				$parts[] = $catalog;
			}

			$parts[] = $a[1];
		}

		$catalog = implode('/', $parts);

		$path = '/' . $application . '/' . $catalog . '/' . $entry['hash'] . (isset($entry['size']) ? '_' . $entry['size'] : '') . '.' . $entry['ext'];

		return $path;
	}
}

?>
