<?php
error_reporting(E_ALL);

//require_once("../../include/global_lite.php");
require_once("class/GeoBanner.class.php");

$template = substr(basename(__FILE__), 0, strrpos(basename(__FILE__), '.'));

$params = array (
	'template' => $template,
	'escortCount' => 2,
	'shortListMode' => ($_GET['mode'] == 'short_list' ? true : false),
	'application_id' => 7
);

$geo = new GeoBanner($params);

$geo->renderHTML();
