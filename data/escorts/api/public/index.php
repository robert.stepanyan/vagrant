<?php

set_time_limit(0);
ini_set('display_errors', 1);

defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

	set_include_path(implode(PATH_SEPARATOR, array(
	realpath(APPLICATION_PATH . '/../../library'),
	realpath(APPLICATION_PATH . '/../application'),
	get_include_path(),
)));

$app_env = 'production';
if ( preg_match('#.test$#', $_SERVER['SERVER_NAME']) ) {
	$app_env = 'development'; 
}

define('APPLICATION_ENV', $app_env);

/* --> Initialize Autoloader */
require('Zend/Loader/Autoloader.php');
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->registerNamespace('Cubix_');
//$autoloader->registerNamespace('Service_');
$autoloader->registerNamespace('System_');
/* <-- */

/*$cache = Zend_Cache::factory('Core', 'Blackhole', array('automatic_serialization' => true));
Zend_Registry::set('cache', $cache);*/
$frontendOptions = array(
	'lifetime' => null,
	'automatic_serialization' => true
);

$backendOptions = array(
	'servers' => array(
		array(
			'host' => 'localhost',
			'port' => '11211',
			'persistent' =>  true
		)
	),
);

$cache = Zend_Cache::factory(new Zend_Cache_Core($frontendOptions), new Zend_Cache_Backend_Memcached($backendOptions), $frontendOptions);
Zend_Registry::set('cache', $cache);

/* --> Initialize Config */
require('Zend/Config/Xml.php');

$config_file = 'config.xml';

if ( preg_match('#\.(([^.]+?)\.(co\.uk|com|pw|xxx|ch|net|at))(\.test)?$#', $_SERVER['SERVER_NAME'], $a) ) {
	array_shift($a);
	$hostname = reset($a);
	define('HOSTNAME', $hostname);
	$config_file = $hostname . '.xml';
}

$config = new Zend_Config_Xml('../application/configs/' . $config_file, APPLICATION_ENV);
Zend_Registry::set('config', $config);
Cubix_Api_Module::setConfig($config);

date_default_timezone_set($config->defaultTimeZone);
/* <-- */

/* --> Setup Database Connection */
$db_config = $config->database;

if(HOSTNAME == 'escortforumit.xxx' && getenv('MYSQL_HOST')){
	$db = Zend_Db::factory($db_config->adapter, array(
		'host' => getenv('MYSQL_HOST'),
		'username' => getenv('MYSQL_USER'),
		'password' => getenv('MYSQL_PASS'),
		'port' => getenv('MYSQL_PORT') ? getenv('MYSQL_PORT') : 3306,
		'dbname' => getenv('MYSQL_DBNAME'),
		'adapterNamespace' => 'Cubix_Db_Adapter'
	));	
}
else{
	$db = Zend_Db::factory($db_config->adapter, array(
		'host' => $db_config->connection->host,
		'username' => $db_config->connection->username,
		'password' => $db_config->connection->password,
		'dbname' => $db_config->connection->dbname,
		'adapterNamespace' => 'Cubix_Db_Adapter'
	));
}
$db->setFetchMode(Zend_Db::FETCH_OBJ);

Zend_Registry::set('db', $db);
Cubix_Model::setAdapter($db);
Cubix_Api_Module::setDb($db);
$db->query('SET NAMES `utf8`');
/* <-- */

/* --> Put definitions into the registry */
$req = new Zend_Controller_Request_Http();
$req->setParam('lang_id', 'en');
Zend_Controller_Front::getInstance()->setRequest($req);

Cubix_I18n::init();

require 'Cubix/defines.php';
Zend_Registry::set('defines', $DEFINITIONS);
/* <-- */


function __default_api_error_handler($errno, $errstr, $errfile, $errline)
{
	ob_start();
	
	switch ($errno) {
		case E_USER_ERROR:
		echo "[ERROR]\t\t$errstr\n";
		echo "on line $errline in file $errfile\n";
		break;
	
	case E_USER_WARNING:
		echo "[WARNING]\t\t$errstr\n";
		echo "on line $errline in file $errfile\n";
		break;
	
	case E_USER_NOTICE:
		echo "[NOTICE]\t\t$errstr\n";
		echo "on line $errline in file $errfile\n";
		break;
	
	default:
		echo "[UNKNOWN, $errno;]\t\t$errstr\n";
		echo "on line $errline in file $errfile\n";
		break;
	}
	
	$output = ob_get_clean();
	if ( preg_match('#sync.php#', $output) )
		//file_put_contents(APPLICATION_PATH . '/../logs/php.log', $output, FILE_APPEND);
	
	if ( $errno == E_USER_ERROR ) {
		exit(1);
	}
	
//	echo $output;
	
	if ( $errno == E_USER_ERROR ) {
		exit(1);
	}
	
	/* Don't execute PHP internal error handler */
	return true;
}

set_error_handler('__default_api_error_handler');


$method = isset($_REQUEST['method']) ? $_REQUEST['method'] : null;
$api_key = isset($_REQUEST['api_key']) ? $_REQUEST['api_key'] : null;
$is_v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : null;


if ( is_null($api_key) ) {
	echo 'No api key was specified';
}
else {
	$app = System_Api::getAppByApiKey($api_key);

	if ( is_null($app) ) {
		echo 'Invalid api key was specified';
	}
	else {
		Cubix_Application::setId($app->id);
		Zend_Registry::set('app', $app);
		Cubix_Api_Module::setApp($app);

		require('sync.php');
		if ( is_callable($method) ) {
            if (extension_loaded('newrelic')) { // Ensure PHP agent is available
                newrelic_name_transaction('API:' . $method);
            }

			$postdata = file_get_contents('php://input');
			$params = unserialize($postdata);
			if (!is_array($params)) {
				$params = array();
			}

			try {
				$data = call_user_func_array($method, $params);
				if ( false === $data ) {
					$data = array('bool' => false);
				}
			}
			catch ( Exception $e ) {
//					$data = array('error' => Cubix_Api_Error::Exception2Array($e));
				throw $e;
			}

			$data = serialize($data);

			echo $data;
		}
		// Namespace was requested
		elseif ( preg_match('#^(.+?)\.(.+)$#', $method, $a) > 0 ) {
			array_shift($a);
			list($mod, $method) = $a;

            if (extension_loaded('newrelic')) { // Ensure PHP agent is available
                newrelic_name_transaction('API:' . $mod . '::' . $method);
            }
			
			$migrated_path = "";
			if ( $is_v2 ) {
				$migrated_path = "migrated/";
				$db->setFetchMode(Zend_Db::FETCH_OBJ);
			}
			

			if ( is_file($file = '../mods/' . $migrated_path . $mod . '.php') ) {
				require_once $file;

				$module_class = 'Cubix_Api_Module_' . ucfirst($mod);

				if ( class_exists($module_class) ) {
					if ( method_exists($module_class, $method) ) {
						$postdata = file_get_contents('php://input');
						
						$params = unserialize($postdata);
						if (!is_array($params)) {
							$params = array();
						}
						//$params = isset($_REQUEST['params']) ? (array) unserialize($_REQUEST['params']) : array();

						try {
							$data = call_user_func_array(array($module_class, $method), $params);
							if ( null === $data ) {
								$data = array('__bool' => null, '$module_class' => $module_class, '$method' => $method);
							}
						}
						catch ( Exception $e ) {
							$data = array('__internal' => $e->__toString());
						}

						function object_to_array($obj) {
							if(is_object($obj)) $obj = (array) $obj;
							if(is_array($obj)) {
								$new = array();
								foreach($obj as $key => $val) {
									$new[$key] = object_to_array($val);
								}
							}
							else $new = $obj;
							return $new;
						}

						$data = object_to_array($data);
						/*if (is_array($data) || is_object($data)) {
							foreach ((array) $data as $i => $item) {
								if (is_array($data)) {
									$data[$i] = object_to_array($item);
								}
								elseif (is_object($data)) {
									$data->$i = object_to_array($item);
								}
							}
						}*/

						//var_dump($data);
						$data = serialize($data);
						echo $data;
					}
					else {
						echo 'Module ' . $mod . ' does not support method ' . $method;
					}
				}
				else {
					echo 'Class ' . $module_class . ' does not exist';
				}
			}
			else {
				echo 'Invalid module ' . $mod;
			}
		}
		else {
			echo 'Invalid method specified';
		}
	}
}
