<?php

class Model_Activity_Client_Session
{
	protected $_data;
	
	public function __construct()
	{
		$this->_data = array(
			
		);
	}
	
	public static function getByClientSessionId($client_id, $session_id)
	{
		$db = Zend_Registry::get('db');
		
		$row = $db->query('
			SELECT 
				id, client_id, session_id, user_id
			FROM activity_client_sessions
			WHERE client_id = ? AND session_id = ?
		', array($client_id, $session_id))->fetch();
		
		if ( ! $row ) {
			return null;
		}
		
		$session = new Model_Activity_Client_Session();
		$session->setId($row->id);
		$session->setClientId($row->client_id);
		$session->setSessionId($row->session_id);
		$session->setUserId($row->user_id);
		
		return $session;
	}
	
	public function setId($id)
	{
		$this->_data['id'] = $id;
	}
	
	public function setClientId($client_id)
	{
		$this->_data['client_id'] = $client_id;
	}
	
	public function setSessionId($session_id)
	{
		/*if ( ! preg_match('#^[a-f0-9]{32}$#', $session_id) ) {
			throw new Exception('Wrong session_id supplied, allowed only a-f, 0-9 and length must be 32');
		}*/
		
		$this->_data['session_id'] = $session_id;
	}
	
	public function setUserId($user_id)
	{
		$this->_data['user_id'] = $user_id;
	}
	
	public function save()
	{
		$db = Zend_Registry::get('db');
		
		if ( ! isset($this->_data['client_id']) || is_null($this->_data['client_id']) ) {
			throw new Exception('Client_id is required');
		}
		
		if ( ! isset($this->_data['session_id']) || is_null($this->_data['session_id']) ) {
			throw new Exception('Session_id is required');
		}
		
		$db->insert('activity_client_sessions', $this->_data);
		
		$this->setId($db->lastInsertId());
	}
	
	public function updateAccessTime()
	{
		$db = Zend_Registry::get('db');
		
		if ( ! isset($this->_data['id']) ) {
			throw new Exception('Id is required');
		}
		
		$db->update('activity_client_sessions', array(
			'date_last_access' => new Zend_Db_Expr('NOW()')
		), $db->quoteInto('id = ?', $this->_data['id']));
	}
	
	public function getData()
	{
		return (object) $this->_data;
	}
}
