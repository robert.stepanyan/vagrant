<?php

class Model_Countries extends Cubix_Model {

	protected $_table = 'countries';

	public function getCountries($exclude = array()) {
		$where = '';
		if (!empty($exclude)) {
			$where = implode(',', $exclude);
			$where = ' WHERE id NOT IN (' . $where . ')';
		}
		$sql = '
			SELECT c.id,
				c.iso,
				c.slug,
				c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM countries c
			' . $where . '
		';

		return parent::_fetchAll($sql);
	}

	public function getBlockCountries( $escort_id ){
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		return $client->call('Escorts.getBlockCountries', array($escort_id));
	}

	public function getCountryIsoByIp( $ip ){
		if (!$ip)
        {
	        $ip = Cubix_Geoip::getIP();
        }

        $geoip = geoip_country_code_by_name($ip);

		return $geoip;
	}
	
	public function getPhonePrefixs(){
		$result =  $this->_db->fetchAll('
			SELECT id ,'.Cubix_I18n::getTblField('title') . ' as title, phone_prefix FROM countries_phone_code WHERE phone_prefix is not NULL order by phone_prefix DESC
			');
		return $result;
	}
	public function getPhoneCountries() {
		$sql = '
			SELECT cpc.id,
				cpc.' . Cubix_I18n::getTblField('title') . ' as title,
				cpc.phone_prefix,
				cpc.ndd_prefix
			FROM countries_phone_code cpc
		';
		return parent::_fetchAll($sql);
	}

	public static function  getPhonePrefixById($id){
		$db = Zend_Registry::get('db');
		$sql = '
			SELECT cpc.phone_prefix
			FROM countries_phone_code cpc
			WHERE cpc.id = ?
		';
		return $db->fetchOne($sql,$id);
	}
	
	public static function getContinentCountries() 
	{
		$db = Zend_Registry::get('db');
		
		$c_sql = '
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM continents c
			ORDER BY c.ordering
		';

		$continents = $db->fetchAll($c_sql);
		
		if ( count($continents) > 0 ) {
			foreach( $continents as $k => $continent ) {
				$sql = '
					SELECT c.id,
						c.iso,
						c.slug,
						c.' . Cubix_I18n::getTblField('title') . ' as title
					FROM countries c
					WHERE c.continent_id = ?
				';
				
				$continents[$k]->countries = $db->fetchAll($sql, $continent->id);
			}
		}
		
		return $continents; 
	}
	
	public static function getTravelContinentCountriesByEscort($escort_id) 
	{
		$db = Zend_Registry::get('db');
		
		$c_sql = '
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM continents c
			ORDER BY c.ordering
		';

		$continents = $db->fetchAll($c_sql);
		
		if ( count($continents) > 0 ) {
			foreach( $continents as $k => $continent ) {
				$sql = '
					SELECT c.id,
						c.iso,
						c.slug,
						c.' . Cubix_I18n::getTblField('title') . ' as title
					FROM countries c
					INNER JOIN escort_travel_countries etc ON etc.country_id = c.id
					WHERE c.continent_id = ? AND etc.escort_id = ?
				';
				
				$continents[$k]->countries = $db->fetchAll($sql, array($continent->id, $escort_id));
			}
		}
		
		return $continents; 
	}	
	
	public static function getTravelCountriesByEscort($escort_id) 
	{
		$db = Zend_Registry::get('db');
		
		$sql = '
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' as title
			FROM countries c
			INNER JOIN escort_travel_continents etc ON etc.continent_id = c.continent_id
			WHERE etc.escort_id = ?
			ORDER BY c.title_en
		';

		$countries = $db->fetchAll($sql, $escort_id);
		
		
		return $countries;
	}

	public static function getContinentId($country_id)
	{
		$db = Zend_Registry::get('db');

		$sql = '
			SELECT continent_id
			FROM countries
			WHERE id = ?
		';
		$continent_id = $db->fetchOne($sql, $country_id);

		return $continent_id;
	}
}