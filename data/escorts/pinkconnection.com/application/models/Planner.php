<?php

class Model_Planner extends Cubix_Model
{
	public function add($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Planner.add', array($data));
	}
	
	public function getEvents($user_id, $escort_id = NULL)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Planner.getEvents', array($user_id, $escort_id));
	}
	
	public function getCurrentMonthEventsInfo($user_id, $escort_id = null, $date = NULL)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Planner.getCurrentMonthEventsInfo', array($user_id, $escort_id, $date));
	}
	
	public function getEventsInfo($date, $user_id, $escort_id = NULL)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Planner.getEventsInfo', array($date, $user_id, $escort_id));
	}
	
	public function isOwner($user_id, $id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Planner.isOwner', array($user_id, $id));
	}
	
	public function remove($id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Planner.remove', array($id));
	}
	
	public function get($id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Planner.get', array($id));
	}
	
	public function edit($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Planner.edit', array($data));
	}

	public function isOwnerAgency($escort_id, $agency_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Planner.isOwnerAgency', array($escort_id, $agency_id));
	}

	public function getAgencyEscorts($agency_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Planner.getAgencyEscorts', array($agency_id));
	}
}
