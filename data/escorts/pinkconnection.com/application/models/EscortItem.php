<?php

class Model_EscortItem extends Cubix_Model_Item
{
	public function hasProduct($product_id)
	{	
		return $this->_adapter->query('
			SELECT TRUE FROM escort_products
			WHERE escort_id = ? AND product_id = ?
		', array($this->getId(), $product_id))->fetchAll();
	}

	public function hasStatusBit($status)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		return $client->call('Escorts.hasStatusBit', array($this->getId(), $status));
	}
	
	public function setStatusBit($status)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$client->call('Escorts.setStatusBit', array($this->getId(), $status));
	}
	
	public function removeStatusBit($status)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$client->call('Escorts.removeStatusBit', array($this->getId(), $status));
	}
	
	public function getLangs()
	{
		return $this->langs;
		
		return $this->_adapter->query('
			SELECT el.lang_id AS id, el.level, l.title FROM escort_langs el
			LEFT JOIN langs l ON l.id = el.lang_id 
			WHERE el.escort_id = ?
		', $this->getId())->fetchAll();
	}
	
	public function isInFavorites()
	{
		$sql = 'SELECT 1 FROM favorites WHERE user_id = ? AND escort_id = ?';

		return $this->_adapter->query($sql, array(Model_Users::getCurrent()->id, $this->getId()))->fetch();
	}
	
	public function getMoreEscorts(array $city_ids, $limit = 10)
	{
		/*$client = new Cubix_Api_XmlRpc_Client();
		$result = $client->call('Escorts.getMoreEscorts', array($this->getId(), $city_ids, $limit));
		
		if ( isset($result['error']) ) {
			print_r($result['error']);
			die;
		}
		
		return $result;*/
		
		$sql = '
			SELECT e.showname FROM escorts e 
			INNER JOIN escort_cities ec ON e.id = ec.escort_id

			/*INNER JOIN escort_profiles ep ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN applications a ON a.id = u.application_id*/

			/*INNER JOIN countries c ON c.id = e.country_id*/
			/*INNER JOIN cities cc ON cc.id = ec.city_id*/
			
			/*INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1*/

			WHERE ec.city_id IN (?) AND e.id <> ?

			GROUP BY e.id
			ORDER BY RAND()
			LIMIT ?
		';
		
		$cities = implode(',', $city_ids);
		
		return $this->_adapter->query($sql, array($cities, $this->getId(), $limit))->fetchAll(Zend_Db::FETCH_ASSOC);
	}

	public function getSetcardInfo()
	{
		return (object) $this->getData(array('date_last_modified', 'date_registered', 'hit_count'));
		
		return $this->_adapter->query('
			SELECT e.date_last_modified, e.date_registered, ep.hit_count FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN escort_profiles ep ON ep.escort_id = e.id
			WHERE e.id = ?
		', $this->getId())->fetch();
    }
	
	public function getWorkTimes()
	{
		return $this->getData(array('is_open', 'working_times'));
		
		$is_open = $this->_adapter->query('
			SELECT
				IF ((
					ewt.day_index = ' . date('N') . '
					AND ewt.time_from <= ' . date('G') . '
					AND ewt.time_to >= ' . date('G') . '
				) OR (
					ewt.day_index = ' . date('N') . '
					AND ewt.time_from <= ' . date('G') . '
					AND ewt.time_from > ewt.time_to
				) OR (
					ewt.day_index = ' . ( 1 == date('N') ? 7 : date('N') - 1 ) . '
					AND ewt.time_to >= ' . date('G') . '
					AND ewt.time_from  > ewt.time_to 
				), 1, 0) AS is_open
			FROM escort_working_times ewt
			WHERE ewt.escort_id = ? AND ewt.day_index = ?
		', array($this->getId(), date('N')))->fetch()->is_open;
		
		
		$data = $this->_adapter->query('
			SELECT ewt.day_index, ewt.time_from, ewt.time_to
			FROM escort_working_times ewt
			WHERE ewt.escort_id = ?
		', $this->getId())->fetchAll();
		
		$result = array();
		foreach ( $data as $row ) {
			$result[$row->day_index] = array(
				'from' => $row->time_from,
				'to' => $row->time_to
			);
		}
		
		$ret = array('is_open' => $is_open, 'working_times' => $result);
		
		return $ret;
	}
	
	public function getAdditionalCities($base_city_id)
	{
		//return array();
		
		return $this->_adapter->query('
			SELECT ct.' . Cubix_I18n::getTblField('title') . ' AS city_title, ct.id AS city_id, c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, ct.slug AS city_slug FROM cities ct
			INNER JOIN escort_cities ec ON ec.city_id = ct.id
			
			INNER JOIN countries c ON c.id = ct.country_id
			 
			INNER JOIN regions r ON r.id = ct.region_id
			WHERE ec.escort_id = ? AND ct.id <> ?
		', array($this->getId(), $base_city_id))->fetchAll();
		
	}

	public function getCityzones()
	{
		return $this->_adapter->query('
			SELECT
				cz.slug AS zone_slug/*, cz.' . Cubix_I18n::getTblField('title') . ' AS zone_title,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title*/, ct.id AS city_id,
				c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, ct.slug AS city_slug
			FROM cityzones cz
			INNER JOIN escort_cityzones ecz ON ecz.city_zone_id = cz.id
			INNER JOIN cities ct ON ct.id = cz.city_id
			INNER JOIN countries c ON c.id = ct.country_id
			INNER JOIN regions r ON r.id = ct.region_id
			WHERE ecz.escort_id = ?
		', array($this->getId()))->fetchAll();

	}
	
	public function getContacts()
	{
		return (object) $this->getData(array('base_city', 'base_city_id', 'country', 'country_slug', 'country_iso', 'region_slug', 'region_title', 'city_slug', 'vac_date_to',
			'contact_phone', 'phone_instructions', 'contact_web', 'contact_email', 'contact_zip'));
		
		$contacts = $this->_adapter->query('
			SELECT ct.' . Cubix_I18n::getTblField('title') . ' AS base_city, ct.id AS base_city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country,
				ep.contact_phone, ep.phone_instructions, ep.contact_web, ep.contact_email, ep.contact_zip,
				c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, r.' . Cubix_I18n::getTblField('title') . ' AS region_title, ct.slug AS city_slug, ep.vac_date_to
			FROM escorts e
			INNER JOIN escort_profiles ep ON ep.escort_id = e.id	
			INNER JOIN countries c ON c.id = e.country_id
			INNER JOIN cities ct ON ct.id = e.city_id 
			INNER JOIN regions r ON r.id = ct.region_id
			WHERE e.id = ?
		', $this->getId())->fetch();
		
		return $contacts;
	}
	
	public function getCitytours()
	{
		$tours = array();
		foreach ( $this->tours as $tour ) {
			$tours[] = (object) $tour;
		}
		
		return $tours;
		
		$tours = $this->_adapter->query('
			SELECT t.id, t.date_from, t.date_to, t.phone, t.country_id, 
				t.city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country_title, 
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, ct.slug AS city_slug,
				IF(DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()), 1, 0) AS is_in_tour,
				IF(DATE(t.date_from) > DATE(NOW()), 1, 0) AS is_upcoming_tour
			FROM tours t
				
			INNER JOIN countries c ON c.id = t.country_id
			INNER JOIN cities ct ON ct.id = t.city_id 
			INNER JOIN regions r ON r.id = ct.region_id
			WHERE t.escort_id = ? AND (DATE(t.date_from) >= DATE(NOW()) OR  DATE(t.date_from) <= DATE(NOW()) AND DATE(t.date_to) >= DATE(NOW()))
			ORDER BY t.date_from ASC
		', $this->getId())->fetchAll();
		
		return $tours;
	}
	
	public function getAbout()
	{
		return (object) $this->getData(array('showname', 'nationality_iso', 'nationality_title', 'age', 'eye_color', 'hair_color', 'hair_length',
			'height', 'weight', 'bust', 'waist', 'hip', 'shoe_size', 'breast_size', 'dress_size', 'is_smoker', 'measure_units', 
			'availability', 'sex_availability', 'ethnicity', 'about', 'characteristics', 'cuisine', 'drink', 'flower', 'gifts', 'perfume', 'designer', 'interests', 'sports', 'hobbies'));
		
		$about = $this->_adapter->query('
			SELECT e.showname, n.iso AS nationality_iso, n.' . Cubix_I18n::getTblField('title') . ' AS nationality_title, FLOOR(DATEDIFF(CURDATE(), ep.birth_date) / 365.25) as age, ep.eye_color,
				ep.hair_color, ep.hair_length, ep.height, ep.weight, ep.bust, ep.waist, ep.hip,
				ep.shoe_size, ep.breast_size, ep.dress_size, ep.is_smoker,
				ep.measure_units, ep.availability, ep.sex_availability, ep.ethnicity,
				ep.' . Cubix_I18n::getTblField('about') . ' AS about, ep.characteristics, ep.cuisine, ep.drink, ep.flower, ep.gifts, ep.perfume, ep.designer,	ep.interests, ep.sports, ep.hobbies
			FROM escorts e
			LEFT JOIN escort_profiles ep ON ep.escort_id = e.id
			
			LEFT JOIN nationalities n ON n.id = ep.nationality_id
			
			WHERE e.id = ?
		', $this->getId())->fetch();
		
		return $about;
	}	
	
	public function getServices()
	{
		$datum = (object) $this->getData(array('svc_kissing', 'svc_blowjob', 'svc_cumshot', 'svc_69', 'svc_anal', 'svc_additional'));
		
		if ( $datum->svc_kissing ) {
			return $datum;
		}
		
		if ( $datum->svc_blowjob ) {
			return $datum;
		}
		
		if ( $datum->svc_cumshot ) {
			return $datum;
		}
		
		if ( $datum->svc_69 ) {
			return $datum;
		}
		
		if ( $datum->svc_anal ) {
			return $datum;
		}
		
		if ( $datum->svc_additional ) {
			return $datum;
		}
		
		return false;
		
		$services = $this->_adapter->query('
			SELECT svc_kissing AS kissing, svc_blowjob AS blowjob, svc_cumshot AS cumshot, svc_69 AS p_69, svc_anal AS anal, svc_additional AS additional FROM escort_profiles
			WHERE escort_id = ?
		', $this->getId())->fetch();
				
		if($services->kissing)
			return $services;
		if($services->blowjob)
			return $services;
		if($services->cumshot)
			return $services;
		if($services->p_69)
			return $services;
		if($services->anal)
			return $services;
		if($services->additional)
			return $services;
		
		return false;
	}
	
	public function getRates()
	{
		$DEFINITIONS = Zend_Registry::get('defines');
		
		$currencies = $DEFINITIONS['currencies'];
		$time_units = $DEFINITIONS['time_unit_options'];
		
		/*$rates = $this->_adapter->query('
			SELECT availability, time, time_unit, price, currency_id FROM escort_rates
			WHERE escort_id = ?
		', $this->getId())->fetchAll();*/
		
		if ( is_string($this->rates) ) {
			$rates = @unserialize($this->rates);
		}
		else {
			$rates = $this->rates;
		}
		
		//print_r($rates);
		
		if ( ! is_array($rates) ) {
			return array();
		}
		
		foreach ( $rates as $i => $rate ) {
			$rates[$i] = (object) $rate;
		}
		
		$ret = array();
		foreach ( $rates as $rate ) {
			$new_values = array(
				'currency_title'  => $currencies[$rate->currency_id],
				'time_unit_title' => $time_units[$rate->time_unit]
			);
			$rate = (object) array_merge((array) $rate, $new_values);
			@$ret[$rate->availability][] = $rate;
		}

		$ret = $this->_sortRates($ret);

		return $ret;
	}

	protected function _sortRates($rates)
	{
		$ret = array();

		foreach ($rates as $k => $rate)
		{
			$new_array = array();
			$over = false;

			foreach ($rate as $r)
			{
				if ($r->time_unit == TIME_DAYS)
				{
					$new_array[($r->time * 1440)] = $r;
				}
				elseif ($r->time_unit == TIME_HOURS)
				{
					$new_array[($r->time * 60)] = $r;
				}
				elseif ($r->time_unit == TIME_MINUTES)
				{
					$new_array[$r->time] = $r;
				}
				elseif ($r->time_unit == TIME_OVERNIGHT)
				{
					$overnight = $r;
					$over = true;
				}
			}

			ksort($new_array);

			$sorted = array();

			foreach ($new_array as $rate)
			{
				$sorted[] = $rate;
			}

			if ($over)
				$sorted[] = $overnight;
			$sorted = (object) $sorted;
			$ret[$rate->availability] = $sorted;
		}
		
		return $ret;
	}
	
	public function getPhotos($page = 1, &$count = null, $with_main = true, $only_privates = false)
	{
		$config = Zend_Registry::get('escorts_config');
		
		$where = '';
		
		if ( ! $with_main ) {
			$where .= ' AND ep.is_main = 0 ';
		}
		
		if ( $only_privates ) {
			$where .= ' AND ep.type = 3';
		}
		else {
			$where .= ' AND ep.type <> 3';
		}
		
		$limit = '';
		if ( ! is_null($page) ) {
			$limit = 'LIMIT ' . ($page - 1) * $config['photos']['perPage'] . ', ' . $config['photos']['perPage'];
		}
		
		$photos = $this->getAdapter()->query("
			SELECT " . Cubix_Application::getId() . " AS application_id, ep.* FROM escort_photos ep
			/*INNER JOIN escorts e ON e.id = ep.escort_id*/
			WHERE ep.escort_id = ? {$where} ORDER BY ep.ordering ASC
			{$limit}
		", $this->getId())->fetchAll();
		
		foreach ($photos as $i => $photo) {
			$photos[$i] = new Model_Escort_PhotoItem($photo);
		}
		
		$countSql = "
			SELECT COUNT(ep.id) as count 
			FROM escort_photos ep
			/*INNER JOIN escorts e ON e.id = ep.escort_id*/
			WHERE ep.escort_id = ? {$where}
		";
		
		$count = $this->getAdapter()->query($countSql, $this->getId())->fetch()->count;
		
		return $photos;
	}
	
	public function getPhotosCount()
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$count = $client->call('Escorts.getPhotosCount', array($this->getId()));
		return $count['count'];


		$countSql = "
			SELECT COUNT(ep.id) as count
			FROM escort_photos ep
			/*INNER JOIN escorts e ON e.id = ep.escort_id*/
			WHERE ep.escort_id = ? /*{$where}*/
		";

		$count = $this->getAdapter()->query($countSql, $this->getId())->fetch()->count;

		return $count;
	}
	
	public function getPhoto($index)
	{
		/*$photos = $this->_adapter->query('
			SELECT * FROM escort_photos 
			WHERE escort_id = ? AND ordering = ?
		', array($this->getId(), $index))->fetch();
		
		return new Model_Escort_PhotoItem($photo);*/
		
		return array();
	}
	
	public function getMainPhoto()
	{
		if ( is_null($this->photo_hash) ) return null;

		return new Model_Escort_PhotoItem(array(
			'application_id' => $this->application_id,
			'escort_id' => $this->id,
			'hash' => $this->photo_hash,
			'ext' => $this->photo_ext,
			'photo_status' => $this->photo_status
		));
	}
	
	public function getVacation()
	{
		$sql = "SELECT vac_date_from, vac_date_to FROM escorts WHERE id = ?";
		
		return $this->getAdapter()->query($sql, $this->getId())->fetch();
	}

	public function getVerifyRequest()
	{
		$model = new Model_VerifyRequests();
		
		$item = $model->getByEscortId($this->getId());
		
		return $item;
	}
	
	public function isVerified()
	{
		return ($this->verified_status == Model_Escorts::VERIFIED_STATUS_VERIFIED || $this->verified_status == Model_Escorts::VERIFIED_STATUS_VERIFIED_RESET) ;
	}
	
	public function isInAgency($agency_id)
	{
		return $this->agency_id == $agency_id;
	}

	/**
	 * Need this func for ability to serialize this object
	 *
	 * @author GuGo
	 */
	public function __wakeup()
	{
		$this->setAdapter(Model_Escorts::getAdapter());
	}

	public function getProfile()
	{
		$model = new Model_Escorts();

		return $model->getProfile($this->getId());
	}

	public function hasProfile()
	{
		$client = Cubix_Api::getInstance();
		return $client->call('hasEscortProfileByEscortIdV2', array($this->getId()));
	}
}
