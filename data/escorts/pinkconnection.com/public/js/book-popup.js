Cubix.BPopup = {};


Cubix.BPopup.inProcess = false;

Cubix.BPopup.url = '';

Cubix.BPopup.Show = function (box_height, box_width, y_offset) {
	if ( Cubix.BPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();

	//var y_offset = 30;

	var container = new Element('div', { 'class': 'BPopup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0,
        position: 'absolute',
        'z-index': 101,
        //background: '#fff'
	}).inject(document.body);
	
	Cubix.BPopup.inProcess = true;

	new Request({
		url: Cubix.BPopup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.BPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'bpopup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.BPopup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');		

			new DatePicker(container.getElements('.date')[0], { pickerClass: 'datepicker',
			positionOffset: { x: -10, y: 0 }, allowEmpty: true, format: 'd M Y' });

			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.BPopup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
		}
	}).send();
		
	
	return false;
}

Cubix.BPopup.Send = function (e) {
	e.stop();
   
	var overlay = new Cubix.Overlay($$('.BPopup-wrapper')[0], { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();

	this.set('send', {
		onSuccess: function (resp) {

			overlay.enable();		
			
			$$('.BPopup-wrapper')[0].set('html', resp);
               
			container = $$('.BPopup-wrapper')[0];
			   
			new DatePicker(container.getElements('.date')[0], { pickerClass: 'datepicker',
			positionOffset: { x: -10, y: 0 }, allowEmpty: true, format: 'd M Y' });
			   
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.BPopup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
			   
			var close_btn = new Element('div', {
				html: '',
				'class': 'bpopup-close-btn-v2'
			}).inject($$('.BPopup-wrapper')[0]);

			close_btn.addEvent('click', function() {
				$$('.BPopup-wrapper').destroy();
				$$('.overlay').destroy();
			});
			
		}.bind(this)
	});

	this.send();
}