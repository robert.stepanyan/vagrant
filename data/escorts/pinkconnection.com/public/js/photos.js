/* --> Photos */
Cubix.Photos = {};

Cubix.Photos.url = ''; // Must be set from php
Cubix.Photos.container = '';

Cubix.Photos.Load = function (lang, page, escort_id) {
	if ( lang == 'fr' ) lang = false;
	var url = (lang ? '/' + lang : '') + '/escorts/photos?photo_page=' + page + '&escort_id=' + escort_id + '&mode=ajax';
	
	Cubix.Photos.Show(url);
	
	return false;
}

Cubix.Photos.Show = function (url) {
	var overlay = new Cubix.Overlay($(Cubix.Photos.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			$(Cubix.Photos.container).set('html', resp);
			overlay.enable();

			$$('.for-premium').addEvent('click', function(e){
				e.stop();
				Cubix.Popup.Show('500', '520');
			});
		}
	}).send();
}
/* <-- */

window.addEvent('domready', function(){
	$$('.for-premium').addEvent('click', function(e){
		e.stop();
		Cubix.Popup.Show('500', '520');
	});
});