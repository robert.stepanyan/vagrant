/* --> Feedback */
Cubix.FPopup = {};


Cubix.FPopup.inProcess = false;

Cubix.FPopup.url = '';

Cubix.FPopup.Show = function (box_height, box_width) {
	if ( Cubix.FPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();

	var y_offset = 30;

	var container = new Element('div', { 'class': 'FPopup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0
	}).inject(document.body);
	
	Cubix.FPopup.inProcess = true;

	new Request({
		url: Cubix.FPopup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.FPopup.inProcess = false;
			container.set('html', resp);

			

			var close_btn = new Element('div', {
				html: '',
				'class': 'FPopup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.FPopup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
			
			/*var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.FPopup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });*/
		}
	}).send();
		
	
	return false;
}