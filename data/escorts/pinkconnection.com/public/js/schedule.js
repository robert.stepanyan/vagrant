var MyPicker = new Class({
	Extends: DatePicker,

	availableDays: [],
	notAvailableDays: [],
	selectAllCheckboxes: [],
	monthInterval: 4,
	mainContainerId: 'picker-container',
	hideOtherMonthDays : true,
	editable: false,


	setEventDays: function(availableDays, notAvailableDays){
		if ( availableDays ){
			this.availableDays = availableDays;
		}

		if ( notAvailableDays ){
			this.notAvailableDays = notAvailableDays;
		}		
	},

	setEditable: function ( editable ){
		if ( editable ){
			this.editable = editable;
		}
	},


	initialize: function(attachTo, options, availableDays, notAvailableDays, editable) {

		
		this.setEventDays(availableDays, notAvailableDays);
		this.setEditable( editable );

		this.attachTo = attachTo;
		this.setOptions(options).attach();
		if (this.options.timePickerOnly) {
			this.options.timePicker = true;
			this.options.startView = 'time';
		}

		

//		console.log(availableDays);
		
		
		this.formatMinMaxDates();
		document.addEvent('mousedown', this.close.bind(this));
	},

	attach: function() {
		// toggle the datepicker through a separate element?
		if ($chk(this.options.toggleElements)) {
			var togglers = $$(this.options.toggleElements);
			document.addEvents({
				'keydown': function(e) {
					if (e.key == "tab") {
						this.close(null, true);
					}
				}.bind(this)
			});
		};

		// attach functionality to the inputs
		$$(this.attachTo).each(function(item, index) {

			// never double attach
			if (item.retrieve('datepicker')) return;

			// determine starting value(s)
			if ($chk(item.get('value'))) {
				var init_clone_val = this.format(new Date(this.unformat(item.get('value'), this.options.inputOutputFormat)), this.options.format);
			} else if (!this.options.allowEmpty) {
				var init_clone_val = this.format(new Date(), this.options.format);
			} else {
				var init_clone_val = '';
			}

			// create clone
			var display = item.getStyle('display');
			var clone = item

			.setStyle('display', this.options.debug ? display : 'none')
			.store('datepicker', true) // to prevent double attachment...
			.clone()
			.store('datepicker', true) // ...even for the clone (!)
			.removeProperty('name')    // secure clean (form)submission
			.setStyle('display', display)
			.set('value', init_clone_val)
			.inject(item, 'after');
			this.onFocus(item, clone);

			// events
			if ($chk(this.options.toggleElements)) {
				togglers[index]
				.setStyle('cursor', 'pointer')
				.addEvents({
					'click': function(e) {
						this.onFocus(item, clone);
					}.bind(this)
				});
				clone.addEvents({
					'blur': function() {
						item.set('value', clone.get('value'));
					}
				});
			} else {
				clone.addEvents({
					'keydown': function(e) {
						if (this.options.allowEmpty && (e.key == "delete" || e.key == "backspace")) {
							item.set('value', '');
							e.target.set('value', '');
							this.close(null, true);
						} else if (e.key == "tab") {
							this.close(null, true);
						} else {
							e.stop();
						}
					}.bind(this),
					'focus': function(e) {
						this.onFocus(item, clone);
					}.bind(this)
				});
			}
		}.bind(this));
	},


	dayclick: function ( obj ){
		if ( !this.editable ){
			return;
		}
		var item = (this.choice.month+1)+'/'+this.choice.day+'/'+this.choice.year;
		if ( obj.hasClass ('available') == true  ){
			//							console.log('dgd');
			obj.removeClass('available');
			this.availableDays.erase(item);
			this.notAvailableDays.include(item);
			obj.addClass('not-available');
		} else {
			if ( obj.hasClass ('not-available') == true ){
				obj.removeClass('not-available');
				this.notAvailableDays.erase(item);
			} else{
				this.availableDays.include(item);
				obj.addClass('available');
			}
		}
	},

	renderMonth: function( interator ) {
		var month = this.d.getMonth();
		
		month += interator;
			
		if ( month > 11 ){
			month = month - 12;
			this.d.setYear( this.d.getFullYear() + 1 );
		}
		

		
		
		this.d.setDate(1);
		this.d.setMonth( month );

//		var selectAllMonth = month;
//		var selectAllYear = this.d.getFullYear();
//		var selectAll = this.d;
		var selectAll = new Date();
		selectAll.setDate(1);
		selectAll.setMonth( month );
		selectAll.setYear( this.d.getFullYear() );




		this.picker.getElement('.titleText').set('text', this.options.months[month] + ' ' + this.d.getFullYear());

		
		while (this.d.getDay() != this.options.startDay) {
			this.d.setDate(this.d.getDate() - 1);
			
		}

		var container = new Element('div', {
			'class': 'days'
		}).inject(this.newContents);
		var titles = new Element('div', {
			'class': 'titles'
		}).inject(container);
		var d, i, classes, e, weekcontainer;

		for (d = this.options.startDay; d < (this.options.startDay + 7); d++) {
			new Element('div', {
				'class': 'title day day' + (d % 7)
			}).set('text', this.options.days[(d % 7)].substring(0,this.options.dayShort)).inject(titles);
		}

		var available = false;
		var t = this.today.toDateString();

		//						var sd = Date.parse('01/01/00');
		//var xsss = this.dateFromObject( sd ).toDateString();
		//						this.dateToObject( Date.parse('10/12/1982') );
		//						var sd = new Date( Date.parse('9/11/1982') );
		////						console.log(sd);
		//						console.log( this.dateToObject( sd ) );
		//
		//	var schoice = this.dateToObject( sd );


		var currentChoice = this.dateFromObject(this.choice).toDateString();



		for (i = 0; i < 42; i++) {
			classes = [];
			classes.push('day');
			classes.push('day'+this.d.getDay());
			if (this.d.toDateString() == t) classes.push('today');
			if (this.d.toDateString() == currentChoice) classes.push('selected');
			if (this.d.getMonth() != month) classes.push('otherMonth');
			

			var dataPicker = this;


			/* Available Select */
			Array.each(this.availableDays, function(day, index){
				var sd = new Date( Date.parse( day ) );

				var tmp_a = dataPicker.dateFromObject( dataPicker.dateToObject( sd ) ).toDateString();

				if (dataPicker.d.toDateString() == tmp_a) {
					classes.push('available');
				};
			});

			/* Not Available Select */

			Array.each(this.notAvailableDays, function(day, index){
				var sd = new Date( Date.parse( day ) );

				var tmp_a = dataPicker.dateFromObject( dataPicker.dateToObject( sd ) ).toDateString();

				if (dataPicker.d.toDateString() == tmp_a) {
					classes.push('not-available');
				};
			});


			if (i % 7 == 0) {
				weekcontainer = new Element('div', {
					'class': 'week week'+(Math.floor(i/7))
				}).inject(container);
			}

			e = new Element('div', {
				'class': classes.join(' ')
			}).set('text', this.d.getDate()).inject(weekcontainer);

			if ( e.hasClass('otherMonth') && this.hideOtherMonthDays ){
				e.set('text','');
			}
			if (this.limited('date')) {
				e.addClass('unavailable');
				if (available) {
					this.limit.right = true;
				} else if (this.d.getMonth() == month) {
					this.limit.left = true;
				}
			} else {
				available = true;
				e.addEvent('click', function(e, d) {
					if (this.options.timePicker) {
						this.d.setDate(d.day);
						this.d.setMonth(d.month);
						this.mode = 'time';
						this.render('fade');
					} else {
						this.select(d);
					}
				}.bindWithEvent(this, {
					day: this.d.getDate(),
					month: this.d.getMonth(),
					year: this.d.getFullYear()
				}));
			}

			e.addEvent('click', this.dayclick.bind(this, e));

			this.d.setDate(this.d.getDate() + 1);


		}



		if ( this.editable ){
			var checkAllDiv = new Element('div', {
				'class': 'checkAll'
			}).inject(this.slider);

			var checkAllDiv = new Element('label', {
				'class': ''
			}).inject( checkAllDiv );

			var checked = false;
			Array.each(this.selectAllCheckboxes, function(checkbox, index){
				if ( checkbox == (selectAll.getMonth()+'-'+selectAll.getFullYear()) ){
					checked = true;
				}
			});


			var checkBoxSpan = new Element('span', {
				}).inject(checkAllDiv);

			if ( checked ){
				new Element('input', {
					'type': 'checkbox',
					'checked': 'checked',
					'value': selectAll.getMonth()+'-'+selectAll.getFullYear()
				}).addEvent('click', this.checkAll.bind(this, [selectAll, 0])).inject(checkBoxSpan);
			}else{
				new Element('input', {
					'type': 'checkbox',
					'value': selectAll.getMonth()+'-'+selectAll.getFullYear()
				}).addEvent('click', this.checkAll.bind(this, [selectAll, 1])).inject(checkBoxSpan);
			}


			new Element('span', {
				'class': 'checkAllSpan'
				}).set('text', 'Select all').inject(checkAllDiv);

			if (!available) this.limit.right = true;
		}

		
	},


	render: function(fx) {
		
		for ( var i = 0; i < this.monthInterval; i++){
			this.constructPicker();



			// remember current working date
			var startDate = new Date(this.d.getTime());

			// intially assume both left and right are allowed
			this.limit = {
				right: false,
				left: false
			};

			// render! booty!
			if (this.mode == 'decades') {
				this.renderDecades();
			} else if (this.mode == 'year') {
				this.renderYear();
			} else if (this.mode == 'time') {
				this.renderTime();
				this.limit = {
					right: true,
					left: true
				}; // no left/right in timeview
			} else {
				this.renderMonth(i);
			}

			this.picker.getElement('.previous').setStyle('visibility', this.limit.left ? 'hidden' : 'visible');
			this.picker.getElement('.next').setStyle('visibility', this.limit.right ? 'hidden' : 'visible');
			this.picker.getElement('.titleText').setStyle('cursor', this.allowZoomOut() ? 'pointer' : 'default');

			// restore working date
			this.d = startDate;

			// if ever the opacity is set to '0' it was only to have us fade it in here
			// refer to the constructPicker() function, which instantiates the picker at opacity 0 when fading is desired
			if (this.picker.getStyle('opacity') == 0) {
				this.picker.tween('opacity', 0, 1);
			}
		}
		
		// animate
		if ($chk(fx)) this.fx(fx);
	},

	constructPicker: function() {
		this.picker = new Element('div', {
			'class': this.options.pickerClass
		}).inject($(this.mainContainerId));
		if (this.options.useFadeInOut) {
			this.picker.setStyle('opacity', 0).set('tween', {
				duration: this.options.animationDuration
			});
		}

		var h = new Element('div', {
			'class': 'header'
		}).inject(this.picker);
		var titlecontainer = new Element('div', {
			'class': 'title'
		}).inject(h);
		new Element('div', {
			'class': 'previous'
		}).addEvent('click', this.previous.bind(this)).set('text', '').inject(h);
		new Element('div', {
			'class': 'next'
		}).addEvent('click', this.next.bind(this)).set('text', '').inject(h);
		new Element('div', {
			'class': 'closeButton'
		}).addEvent('click', this.close.bindWithEvent(this, true)).set('text', 'x').inject(h);
		new Element('span', {
			'class': 'titleText'
		}).addEvent('click', this.zoomOut.bind(this)).inject(titlecontainer);

		var b = new Element('div', {
			'class': 'body'
		}).inject(this.picker);
		this.bodysize = b.getSize();
		this.slider = new Element('div', {
			styles: {
				position: 'absolute',
				top: 0,
				left: 0,
				width: 2 * this.bodysize.x,
				height: this.bodysize.y
			}
		})
	.set('tween', {
		duration: this.options.animationDuration,
		transition: Fx.Transitions.Quad.easeInOut
	}).inject(b);


	
	

	this.oldContents = new Element('div', {
		styles: {
			position: 'absolute',
			top: 0,
			left: this.bodysize.x,
			width: this.bodysize.x,
			height: this.bodysize.y
		}
	}).inject(this.slider);
	this.newContents = new Element('div', {
		styles: {
			position: 'absolute',
			top: 0,
			left: 0,
			width: this.bodysize.x,
			height: this.bodysize.y
		}
	}).inject(this.slider);

},
	


	checkAll: function( day, ischecked ){

		var checkboxField = day.getMonth()+'-'+day.getFullYear();
		
		if ( ischecked == 0 ){
			this.selectAllCheckboxes.erase( checkboxField );
		}else{
			this.selectAllCheckboxes.combine( [checkboxField] );
		}

		var daysIn = daysInMonth(day.getMonth()+1, day.getFullYear());

		var values = [];
		for ( var i = 1; i <= daysIn; i++){
			//day.setDate(i);
			var selected = new Object();
			selected.day = i;
			selected.month = (day.getMonth());
			selected.year = day.getFullYear();

			var event_day = '01';
			if ( i < 10 ){
				event_day = '0'+i;
			}else{
				event_day = i;
			}
			if ( ischecked == 0 ){
				this.unselect(selected, 1);
				this.availableDays.erase((day.getMonth()+1)+"/"+event_day+"/"+day.getFullYear());

			}else{
				values.combine([(day.getMonth()+1)+"/"+event_day+"/"+day.getFullYear()]);
				this.select(selected, 1);
			}
		}

		this.availableDays.combine(values);

		$$('.' + this.options.pickerClass ).destroy();

		
		this.render();
	},


	close: function(e, force) {
		return;
	},

	previous: function() {
		$$('.' + this.options.pickerClass ).destroy();

			if (this.mode == 'decades') {
				this.d.setFullYear(this.d.getFullYear() - this.options.yearsPerPage);
			} else if (this.mode == 'year') {
				this.d.setFullYear(this.d.getFullYear() - 1);
			} else if (this.mode == 'month') {
				this.d.setDate(1);
				this.d.setMonth(this.d.getMonth() - 1);
			}
			this.render();
		},

		next: function() {
			$$('.' + this.options.pickerClass ).destroy();

			if (this.mode == 'decades') {
				this.d.setFullYear(this.d.getFullYear() + this.options.yearsPerPage);
			} else if (this.mode == 'year') {
				this.d.setFullYear(this.d.getFullYear() + 1);
			} else if (this.mode == 'month') {
				this.d.setDate(1);
				this.d.setMonth(this.d.getMonth() + 1);
			}
			this.render();
		},

	select: function(values, skipIfExists) {
//							console.log(values);
		if ( !this.editable ){
			return;
		}
		this.choice = $merge(this.choice, values);
		var d = this.dateFromObject(this.choice);
		var key = this.choice.year+'-'+(this.choice.month+1)+'-'+this.choice.day;

		var splatted = d.toDateString();
		this.input.set('value', this.format(d, this.options.inputOutputFormat));



		//1 available
		//2 non available
		//3 non
		var date = splatted;

		if ( ! date ) return;

		var found = false;
		$$('#sh-events input[type=hidden]').each(function (el) {
//			console.log(el);
			var _date = el.get('value');
			if ( date == _date ) {
				found = true;
				el.set('value', date);

				var sub_div = el.getParent('div').getChildren('.typeClass');

				if ( sub_div.get('value') == 0 ){
					sub_div.set('value', 1);
				}else{
					if ( !skipIfExists ){
						if ( sub_div.get('value') == 1 ){
							sub_div.set('value', 2);
						} else {
							if ( sub_div.get('value') == 2 ){
								sub_div.set('value', 0);
							}
						}
					}
					
				}
			}
		});

		var selected_country_count = $$('#sh-events input[type=hidden]').length + 1;
		//console.log(selected_country_count);
		if ( ! found ) {
			var eventday = new Element('div', {
				'class': 'eventday'
			}).inject($('sh-events'));

			new Element('input', {
				type: 'hidden',
				name: 'date_names[]',
				value: date
			}).inject(eventday);

			new Element('input', {
				type: 'hidden',
				name: 'selected_days['+key+'][type]',
				value: 1,
				'class': 'typeClass'
			}).inject(eventday);

		}

		//this.options.onSelect(d);
	},


	unselect: function(values) {
		if ( !this.editable ){
			return;
		}
		this.choice = $merge(this.choice, values);
		var d = this.dateFromObject(this.choice);
		var key = this.choice.year+'-'+(this.choice.month+1)+'-'+this.choice.day;

		var splatted = d.toDateString();
		this.input.set('value', this.format(d, this.options.inputOutputFormat));

		var date = splatted;

		if ( ! date ) return;

		var found = false;
		$$('#sh-events input[type=hidden]').each(function (el) {

			var _date = el.get('value');
			if ( date == _date ) {
				found = true;
				
				el.set('value', date);

				var sub_div = el.getParent('div').getChildren('.typeClass');

				if ( sub_div.get('value') == 1  ){
					sub_div.set('value', 0);
				}
				
				
				
			}
		});

		//console.log(selected_country_count);
		if ( ! found ) {
			var eventday = new Element('div', {
				'class': 'eventday'
			}).inject($('sh-events'));

			new Element('input', {
				type: 'hidden',
				name: 'date_names[]',
				value: date
			}).inject(eventday);

			new Element('input', {
				type: 'hidden',
				name: 'selected_days['+key+'][type]',
				value: 0,
				'class': 'typeClass'
			}).inject(eventday);

		}

		//this.options.onSelect(d);
	},

});


function daysInMonth(month,year) {
	var dd = new Date(year, month, 0);
	return dd.getDate();
} 