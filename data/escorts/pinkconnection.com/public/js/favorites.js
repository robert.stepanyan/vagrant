/* --> Favorites */
Cubix.Favorites = {};

Cubix.Favorites.containerMain = 'common-favs';
Cubix.Favorites.url = '/private/ajax-favorites';
Cubix.Favorites.containerTop = 'top-favs';
Cubix.Favorites.urlTop = '/private/ajax-favorites-top';
Cubix.Favorites.search_input_text = 'search by showname';
Cubix.Favorites.showname = "";
Cubix.Favorites.allowScroll = true;

Cubix.Favorites.Init = function () {
	Cubix.Favorites.InitSearchInput('search');
	
	if(Cookie.read('favoriteAct')){
		Cubix.Favorites.Show({page: 1, act: Cookie.read('favoriteAct')}, false);
	}else{
		Cubix.Favorites.Show({page: 1}, false);
	}
	
	Cubix.Favorites.ShowTop();
	
	return false;
}

Cubix.Favorites.InitSearchInput = function (input) {
	Cubix.Favorites.InitSearchInput.input = $(input);
	
	this.timer = null;
		
	Cubix.Favorites.InitSearchInput.input.addEvent('focus', function() {
		if ( this.get('value') == Cubix.Favorites.search_input_text ) {
			this.set('value', '');
			this.removeClass('def-text');
		}	
	});
	
	Cubix.Favorites.InitSearchInput.input.addEvent('blur', function() {
		if ( ! this.get('value').length ) {
			this.set('value', Cubix.Favorites.search_input_text);
			this.addClass('def-text');
		}
	});
	
	Cubix.Favorites.InitSearchInput.input.addEvents({
		keyup: function () {
			$clear(this.timer);
			this.timer = setTimeout('Cubix.Favorites.InitSearchInput.KeyUp(Cubix.Favorites.InitSearchInput.input)', 500);
		}.bind(this)
	});
}

Cubix.Favorites.InitSearchInput.KeyUp = function (input) {	
	var value = ( input.get('value').length && input.get('value') != Cubix.Favorites.search_input_text ) ? input.get('value') : '';
	
	Cubix.Favorites.Show({showname: value, page: 1}, false);
}

Cubix.Favorites.Show = function(data, allowScroll) {
	$('fav-msg').set('class', '');
	$('fav-msg').set('html', '');
	$('fav-msg').addClass('none');
	
	var container = Cubix.Favorites.containerMain;
	data.showname = data.showname ? data.showname : Cubix.Favorites.showname
	
	var myScroll = new Fx.Scroll(window);
	var overlay = new Cubix.Overlay($(container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Favorites.url,
		method: 'get',
		data: data,
		onSuccess: function (resp) {			
			$(container).set('html', resp);
			
			/* personal comment */
			openPC();
			/**/
			
			overlay.enable();
			
			if (allowScroll) {
				myScroll.toElement($(container));
			}
		}
	}).send();
	
	return false;
}

function openPC()
{
	$$('.plus').addEvent('click', function() {
		var self = this;

		self.removeClass('plus');
		self.addClass('minus');

		var controlBox = self.getParent().getParent();
		controlBox.setStyle('height', '218px');
		var form = self.getNext('form');
		form.removeClass('none');
		
		closePC();
	});
}

function closePC()
{
	$$('.minus').addEvent('click', function() {
		var self = this;

		self.removeClass('minus');
		self.addClass('plus');

		var controlBox = self.getParent().getParent();
		controlBox.setStyle('height', '100px');
		var form = self.getNext('form');
		form.addClass('none');
		
		openPC();
	});
}

Cubix.Favorites.ShowTop = function() {
	var container = Cubix.Favorites.containerTop;
	
	var myScroll = new Fx.Scroll(window);
	var overlay = new Cubix.Overlay($(container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: 0,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: Cubix.Favorites.urlTop,
		method: 'get',
		onSuccess: function (resp) {			
			$(container).set('html', resp);
			overlay.enable();			
		}
	}).send();
	
	return false;
}

Cubix.Favorites.AddToTop10 = function(escortId) {
	var myScroll = new Fx.Scroll(window);
	
	var data = {
		escort_id: escortId
	}
	
	new Request.JSON({
		url: '/private/ajax-favorites-add-to-top',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			if (resp.success) {
				Cubix.Favorites.Show({page: 1}, false);
				Cubix.Favorites.ShowTop();
				
				$('fav-msg').removeClass('none');
				$('fav-msg').addClass('msg-suc');
				$('fav-msg').set('html', resp.success);
				
				myScroll.toElement($('favorites'));
			}
			else
			{
				$('fav-msg').removeClass('none');
				$('fav-msg').addClass('msg-err');
				$('fav-msg').set('html', resp.error);
				
				myScroll.toElement($('favorites'));
			}
		}
	}).send();
	
	return false;
}

Cubix.Favorites.AddComment = function(form) {
	var data = {
		comment: form.getElement('textarea').get('value'),
		escort_id: form.getElement('input').get('value')
	} 
	
	var formOverlay = new Cubix.Overlay(form, {
		loader: _st('loader-circular-tiny.gif'),
		position: '50%'
	});
	
	formOverlay.disable();
	
	new Request.JSON({
		url: '/private/add-fav-comment',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			if (resp.success) {
				formOverlay.enable();
				Cubix.Favorites.ShowTop();
			}
		}
	}).send();
	
	return false;
}

Cubix.Favorites.Remove = function(escortId) {
	var myScroll = new Fx.Scroll(window);
	
	var data = {
		escort_id: escortId
	}
	
	new Request.JSON({
		url: '/private/remove-from-favorites',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			if (resp.success) {
				Cubix.Favorites.Show({page: 1}, false);
				Cubix.Favorites.ShowTop();
				
				$('fav-msg').removeClass('none');
				$('fav-msg').addClass('msg-suc');
				$('fav-msg').set('html', resp.success);
				
				myScroll.toElement($('favorites'));
			}
		}
	}).send();
	
	return false;
}

Cubix.Favorites.Up = function(escortId) {	
	var data = {
		escort_id: escortId
	}
	
	new Request.JSON({
		url: '/private/ajax-favorites-up',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			if (resp.success) {
				Cubix.Favorites.ShowTop();
			}
		}
	}).send();
	
	return false;
}

Cubix.Favorites.Down = function(escortId) {	
	var data = {
		escort_id: escortId
	}
	
	new Request.JSON({
		url: '/private/ajax-favorites-down',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			if (resp.success) {
				Cubix.Favorites.ShowTop();
			}
		}
	}).send();
	
	return false;
}

Cubix.Favorites.RemoveFromTop10 = function(escortId) {
	var myScroll = new Fx.Scroll(window);
	
	var data = {
		escort_id: escortId
	}
	
	new Request.JSON({
		url: '/private/ajax-favorites-remove-from-top',
		method: 'post',
		data: data,
		onSuccess: function (resp) {
			if (resp.success) {
				Cubix.Favorites.Show({page: 1}, false);
				Cubix.Favorites.ShowTop();
				
				$('fav-msg').removeClass('none');
				$('fav-msg').addClass('msg-suc');
				$('fav-msg').set('html', resp.success);
				
				myScroll.toElement($('favorites'));
			}
		}
	}).send();
	
	return false;
}