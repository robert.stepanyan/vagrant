/* --> Photos */
Cubix.PrivatePhotos = {};

Cubix.PrivatePhotos.url = ''; // Must be set from php
Cubix.PrivatePhotos.container = '';

Cubix.PrivatePhotos.Load = function (lang, page, escort_id) {
	var url = '/' + lang + '/escorts/private-photos?photo_page=' + page + '&escort_id=' + escort_id;
	
	Cubix.PrivatePhotos.Show(url);
	
	return false;
}

Cubix.PrivatePhotos.Show = function (url) {
	var overlay = new Cubix.Overlay($(Cubix.PrivatePhotos.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			$(Cubix.PrivatePhotos.container).set('html', resp);
			overlay.enable();
		}
	}).send();
}
/* <-- */
