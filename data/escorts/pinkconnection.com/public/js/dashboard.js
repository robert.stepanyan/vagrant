/* --> Photos */
Cubix.Dashboard = {};

Cubix.Dashboard.url = ''; // Must be set from php
Cubix.Dashboard.container = '';

Cubix.Dashboard.Load = function (lang, page, agency_id) {
	var url = '/private-v2/agency-dashboard?de_page=' + page + '&agency_id=' + agency_id;

	Cubix.Dashboard.Show(url);
	
	return false;
}

Cubix.Dashboard.Show = function (url) {
	var overlay = new Cubix.Overlay($(Cubix.Dashboard.container), {
		loader: _st('loader-small.gif'),
		position: '50%',
		offset: {
			left: -11,
			top: -1
		}
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {			
			$(Cubix.Dashboard.container).set('html', resp);
			overlay.enable();
		}
	}).send();
}