<?php
/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.2.2
 * License Manager
 * Last Updated: $LastChangedDate: 2011-07-19 19:39:43 -0400 (Tue, 19 Jul 2011) $
 * </pre>
 *
 * @author 		$Author: bfarber $
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		http://www.invisionpower.com/community/board/license.html
 * @package		IP.Board
 * @subpackage	Core
 * @link		http://www.invisionpower.com
 * @version		$Rev: 9291 $
 */

if ( ! defined( 'IN_ACP' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded 'admin.php'.";
	exit();
}


class admin_core_tools_licensekey extends ipsCommand
{
	/**
	 * HTML object
	 *
	 * @var		object
	 */
	protected $html;
	
	/**#@+
	 * URL bits
	 *
	 * @var		string
	 */
	public $form_code		= '';
	public $form_code_js	= '';
	/**#@-*/	
	
	/**
	 * Main entry point
	 *
	 * @param	object		ipsRegistry reference
	 * @return	@e void
	 */
	public function doExecute( ipsRegistry $registry )
	{
		/* Load lang and skin */
		$this->registry->class_localization->loadLanguageFile( array( 'admin_tools' ) );
		$this->html = $this->registry->output->loadTemplate( 'cp_skin_tools' );
				
		/* URLs */
		$this->form_code    = $this->html->form_code    = 'module=tools&amp;section=licensekey';
		$this->form_code_js = $this->html->form_code_js = 'module=tools&section=licensekey';
		
		/* We need a classFileManagement object */
		require_once( IPS_KERNEL_PATH . 'classFileManagement.php' );
		$this->classFileManagement = new classFileManagement();

		/* What to do */
		switch( $this->request['do'] )
		{
			case 'remove':
				$this->remove();
			break;
			
			case 'activate':
				$this->activate();
			break;
			
			case 'overview':
			default:
				$this->overview();
			break;
		}
		
		/* Output */
		$this->registry->output->html_main .= $this->registry->output->global_template->global_frame_wrapper();
		$this->registry->output->sendOutput();
	}

	/**
	 * Removes a license key
	 *
	 * @return	@e void
	 */
	public function remove()
	{
		/* Remove the key */
		$this->DB->update( 'core_sys_conf_settings', array( 'conf_value' => '' ), "conf_key='ipb_reg_number'" );
		
		/* Rebuild the cache */
		$this->cache->rebuildCache( 'settings', 'global' );
		$this->cache->setCache( 'licenseData', array(), array( 'array' => 1 ) );
		
		/* Done */
		$this->registry->output->silentRedirect( $this->settings['base_url'] . $this->form_code );
	}
	
	/**
	 * Activates a license
	 *
	 * @return	@e void
	 */
	public function activate()
	{	
		/* Query the api */
		$response = $this->classFileManagement->getFileContents( "http://license.invisionpower.com/?a=activate&key={$this->request['license_key']}&url={$this->request['domain_key']}" );
		
		if( $response == 'NO_KEY' )
		{
			$this->registry->output->global_message = $this->lang->words['license_key_notfound'];
		}
		else
		{
			/* Decode */
			$response = json_decode( $response, true );
	
			if( $response['result'] == 'ok' )
			{
				$this->DB->update( 'core_sys_conf_settings', array( 'conf_value' => $this->request['license_key'] ), "conf_key='ipb_reg_number'" );
				$this->cache->rebuildCache( 'settings', 'global' );
				$this->cache->setCache( 'licenseData', array(), array( 'array' => 1 ) );
			}
			else
			{
				$this->registry->output->global_message = $this->lang->words['license_key_bad'];
			}
		}

		$this->registry->output->silentRedirectWithMessage( $this->settings['base_url'] . $this->form_code );
	}
	
	/**
	 * Displays license information
	 *
	 * @return	@e void
	 */
	public function overview()
	{
		/* Show activation form if we have no key */
		if( ! $this->settings['ipb_reg_number'] )
		{
			$this->activateForm();
			return;
		}
		
		/* Get License Data from cache */
		$licenseData = $this->cache->getCache( 'licenseData' );
		
		if( ! $licenseData || $this->request['refresh'] == 1 || ( IPS_UNIX_TIME_NOW - $licenseData['_cached_date'] ) > 86400 )
		{
			/* Query the api */
			$response = $this->classFileManagement->getFileContents( "http://license.invisionpower.com/?a=info&key={$this->settings['ipb_reg_number']}&url={$this->settings['board_url']}&flush=1" );

			if ( $response == 'NO_KEY' )
			{			
				$this->DB->update( 'core_sys_conf_settings', array( 'conf_value' => '' ), "conf_key='ipb_reg_number'" );
				$this->cache->rebuildCache( 'settings', 'global' );
				$this->cache->setCache( 'licenseData', array(), array( 'array' => 1 ) );
				
				$this->registry->output->global_message = $this->lang->words['license_key_bad'];
				$this->registry->output->silentRedirectWithMessage( $this->settings['base_url'] . 'app=core&module=tools&section=licensekey' );
			}
						
			/* Get License Data */
			$licenseData = json_decode( $response, true );
			
			/* Copyright Removal? */
			if ( $licenseData['cr'] )
			{
				IPSLib::updateSettings( array( 'ipb_copy_number' => $licenseData['cr'], 'ips_cp_purchase' => 1 ) );
			}
			elseif ( $this->settings['ips_cp_purchase'] )
			{
				IPSLib::updateSettings( array( 'ips_cp_purchase' => 0 ) );
			}

			/* Save to cache */
			$licenseData['_cached_date']	= time();
			$licenseData['key']['_expires']	= $licenseData['key']['_expires'] ? $licenseData['key']['_expires'] : 9999999999;
			$licenseData['key']['expires']	= $licenseData['key']['expires'] ? $licenseData['key']['expires'] : 9999999999;
			
			$this->cache->setCache( 'licenseData', $licenseData, array( 'array' => 1 ) );
		}

		/* Date */
		$licenseData['_cached_date'] = $this->lang->formatTime( $licenseData['_cached_date'] );

		/* Output */
		$this->registry->output->html .= $this->html->licenseKeyStatusScreen( $this->settings['ipb_reg_number'], $licenseData );
	}
	
	/**
	 * Displays license key status
	 *
	 * @return	@e void
	 */
	public function activateForm()
	{		
		/* Key Input */
		$keyInput = $this->registry->output->formInput( 'license_key', $this->request['license_key'] );
		
		/* Domain Input */
		$domainInput = $this->registry->output->formInput( 'domain_key', $this->settings['board_url'] );
		
		/* Output */
		$this->registry->output->html .= $this->html->activateForm( $keyInput, $domainInput );
	}
}