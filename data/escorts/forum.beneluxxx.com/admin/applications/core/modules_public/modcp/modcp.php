<?php
/**
 * @file		modcp.php 	Moderator control panel
 * $Copyright: (c) 2001 - 2011 Invision Power Services, Inc.$
 * $License: http://www.invisionpower.com/company/standards.php#license$
 * $Author: ips_terabyte $
 * @since		2/14/2011
 * $LastChangedDate: 2011-04-20 04:18:03 -0400 (Wed, 20 Apr 2011) $
 * @version		v3.2.2
 * $Revision: 8400 $
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded 'admin.php'.";
	exit();
}

/**
 *
 * @class		public_core_modcp_modcp
 * @brief		Moderator control panel
 * 
 */
class public_core_modcp_modcp extends ipsCommand
{	
	/**
	 * Main function executed automatically by the controller
	 *
	 * @param	object		$registry		Registry object
	 * @return	@e void
	 */
	public function doExecute( ipsRegistry $registry ) 
	{
		//-----------------------------------------
		// Load basic things
		//-----------------------------------------

		$this->registry->class_localization->loadLanguageFile( array( 'public_modcp' ) );
		
		$this->registry->output->setTitle( $this->lang->words['modcp_page_title'] );

		//-----------------------------------------
		// Which road are we going to take?
		//-----------------------------------------
		
		switch( $this->request['do'] )
		{
			default:
			case 'index':
				$this->_indexPage();
			break;
		}

		//-----------------------------------------
		// Output
		//-----------------------------------------

		$this->registry->getClass('output')->addNavigation( $this->lang->words['modcp_navbar'], "app=core&amp;module=modcp" );
		$this->registry->getClass('output')->addContent( $this->output );
		$this->registry->output->sendOutput();
	}
	
	/**
	 * Show the mod CP portal
	 *
	 * @return	@e void
	 */
	protected function _indexPage()
	{
		//-----------------------------------------
		// Init
		//-----------------------------------------
		
		ipsRegistry::getAppClass('forums');
		$this->request['tab']     = empty($this->request['tab'])	 ? 'index' : trim($this->request['tab']);
		$this->request['fromapp'] = empty($this->request['fromapp']) ? 'index' : trim($this->request['fromapp']);
		
		$_plugins	= array();
		$_tabs		= array();
		$_activeNav = array( 'primary' => 'index', 'secondary' => 'index' );
		$_output	= '';
		$moderator	= $this->registry->class_forums->getModerator();
		$tab		= $this->request['tab'];
		$app		= $this->request['fromapp'];
		
		/**
		 * Loop through all apps and get plugins
		 * 
		 * @note	When updating this code below remember to update also the core in public_core_reports_reports
		 */
		foreach( IPSLib::getEnabledApplications() as $appDir => $appData )
		{
			if( is_dir( IPSLib::getAppDir( $appDir ) . '/extensions/modcp' ) )
			{
				try
				{
					foreach( new DirectoryIterator( IPSLib::getAppDir( $appDir ) . '/extensions/modcp' ) as $file )
					{
						if( ! $file->isDot() && $file->isFile() )
						{
							if( preg_match( "/^plugin_(.+?)\.php$/", $file->getFileName(), $matches ) )
							{
								//-----------------------------------------
								// We load each plugin so it can determine
								// if it should show based on permissions
								//-----------------------------------------
								
								$classToLoad = IPSLib::loadLibrary( $file->getPathName(), 'plugin_' . $appDir . '_' . $matches[1], $appDir );
								$_plugins[ $appDir ][ $matches[1] ] = new $classToLoad( $this->registry );

								if( $_plugins[ $appDir ][ $matches[1] ]->canView( $moderator ) )
								{
									//-----------------------------------------
									// Hacky solution - we want forum plugins to
									// come first as they're the most used
									//-----------------------------------------
									
									if( $appDir == 'forums' AND !empty($_tabs[ $_plugins[ $appDir ][ $matches[1] ]->getPrimaryTab() ]) )
									{
										array_unshift( $_tabs[ $_plugins[ $appDir ][ $matches[1] ]->getPrimaryTab() ], array( $_plugins[ $appDir ][ $matches[1] ]->getSecondaryTab(), $appDir, $matches[1] ) );
									}
									else
									{
										$_tabs[ $_plugins[ $appDir ][ $matches[1] ]->getPrimaryTab() ][] = array( $_plugins[ $appDir ][ $matches[1] ]->getSecondaryTab(), $appDir, $matches[1] );
									}
									
									/* Sort active tab */
									if ( $appDir == $app && $tab == $matches[1] )
									{
										$_activeNav = array( 'primary' => $_plugins[ $appDir ][ $matches[1] ]->getPrimaryTab(), 'secondary' => $_plugins[ $appDir ][ $matches[1] ]->getSecondaryTab() );
									}
								}
							}
						}
					}
				} catch ( Exception $e ) {}
			}
		}
		
		//-----------------------------------------
		// If we can't view any tabs, show an error
		//-----------------------------------------
		
		if( !count($_tabs) )
		{
			$this->registry->output->showError( $this->lang->words['modcp_no_access'], 10194.12, false, null, 403 );
		}
		
		//-----------------------------------------
		// Pass the necessary template variables into the plugin
		//-----------------------------------------

		$this->registry->output->getTemplate('modcp')->templateVars['tabs'] = $_tabs;
		$this->registry->output->getTemplate('modcp')->templateVars['activeNav'] = $_activeNav;

		//-----------------------------------------
		// Get appropriate content to show
		//-----------------------------------------

		if( $tab AND $app AND isset($_plugins[ $app ][ $tab ]) )
		{
			$_output = $_plugins[ $app ][ $tab ]->executePlugin( $moderator );
		}
		else
		{
			$_output = $this->registry->output->getTemplate('modcp')->memberLookup();
		}

		//-----------------------------------------
		// Output
		//-----------------------------------------
		
		$this->output .= $this->registry->output->getTemplate('modcp')->portalPage( $_output, $_tabs, $_activeNav );
	}
}