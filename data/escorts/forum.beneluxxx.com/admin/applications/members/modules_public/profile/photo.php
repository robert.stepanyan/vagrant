<?php

/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.2.2
 * Ignore a user
 * Last Updated: $Date: 2011-05-25 10:30:28 -0400 (Wed, 25 May 2011) $
 * </pre>
 *
 * @author 		$Author: ips_terabyte $
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		http://www.invisionpower.com/community/board/license.html
 * @package		IP.Board
 * @subpackage	Members
 * @link		http://www.invisionpower.com
 * @since		20th February 2002
 * @version		$Revision: 8887 $
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.";
	exit();
}

class public_members_profile_photo extends ipsCommand
{
	/**
	 * Class entry point
	 *
	 * @param	object		Registry reference
	 * @return	@e void		[Outputs to screen/redirects]
	 */
	public function doExecute( ipsRegistry $registry )
	{
		/* Load lang file */
		$this->registry->class_localization->loadLanguageFile( array( 'public_profile' ), 'members' );
		
		/* Load library */
		$classToLoad  = IPSLib::loadLibrary( IPS_ROOT_PATH . 'sources/classes/member/photo.php', 'classes_member_photo' );
		$this->photo = new $classToLoad( $registry );
		
		switch( $this->request['do'] )
		{
			case 'save':
				$this->_save();
			break;
			default:
			case 'show':
				$output = $this->_show();
			break;
			case 'remove':
				$this->_remove();
			break;
		}
		
		/* Got anything to show? */
		if ( $output )
		{
			$this->registry->output->addContent( $output );
			$this->registry->output->setTitle( $this->lang->words['pe_title'] . ' - ' . ipsRegistry::$settings['board_name'] );
			$this->registry->output->addNavigation( $this->lang->words['pe_title'], '' );
			$this->registry->output->sendOutput();
		}
	}
	
	/**
	 * Removes pics
	 *
	 * @return	@e void [HTML]
	 */
	protected function _remove()
	{
		
		
	}
	
	/**
	 * Saves data
	 *
	 * @return	@e void [HTML]
	 */
	protected function _save()
	{
		$getJson     = intval( $this->request['getJson'] );
		$photoType   = $this->request['photoType'];
		$gravatar	 = $this->request['gravatar'];
		
		if ( $getJson )
		{
			$classToLoad = IPSLib::loadLibrary( IPS_KERNEL_PATH . 'classAjax.php', 'classAjax' );
			$ajax = new $classToLoad();
		}
		
		/* Do it */
		try
		{
			$photo = $this->photo->save( $this->memberData, $photoType, $gravatar );
			
			if ( is_array( $photo ) )
			{
				if ( $getJson )
				{
					$photo['oldThumb'] = $this->memberData['pp_small_photo'];
					return $ajax->returnJsonArray( $photo, false, 'text/html' );
				}
				else
				{
					/* redirect to show */
					$this->registry->output->redirectScreen( $this->lang->words['pp_photo_edited'], $this->settings['base_url'] . 'showuser=' . $this->memberData['member_id'], $this->memberData['members_seo_name'] );
				}
			}
		}
		catch( Exception $error )
		{
			$msg = $error->getMessage();
			
			switch ( $msg )
			{
				default:
					if ( $getJson )
					{
						$msg = ( ! empty( $this->lang->words[ 'pp_' . $msg ] ) ) ? $this->lang->words[ 'pp_' . $msg ] : $this->lang->words[ 'pp_generic_error' ];
						$ajax->returnJsonError( $msg, 'text/html' );
						exit();
					}
					else
					{
						$this->registry->getClass('output')->showError( $this->lang->words[ 'pp_' . $msg ], 1027, null, null, 403 );
					}
				break;
				case 'PROFILE_DISABLED':
					if ( $getJson )
					{
						$ajax->returnJsonError( 'member_profile_disabled', 'text/html' );
						exit();
					}
					else
					{
						$this->registry->getClass('output')->showError( $this->lang->words['member_profile_disabled'], 1027, null, null, 403 );
					}
				break;
			}
		}
	}
	
	/**
	 * Display the photo editor
	 *
	 * @return	@e void [HTML]
	 */
	protected function _show()
	{
		return $this->photo->getEditorHtml( $this->memberData );
	}
 	
}