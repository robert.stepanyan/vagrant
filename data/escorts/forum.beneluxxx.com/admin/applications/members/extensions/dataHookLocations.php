<?php
/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.2.2
 * Define data hook locations (Members)
 * Last Updated: $Date: 2011-05-18 04:49:10 -0400 (Wed, 18 May 2011) $
 * </pre>
 *
 * @author 		$Author: ips_terabyte $
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		http://www.invisionpower.com/community/board/license.html
 * @package		IP.Board
 * @subpackage	Core
 * @link		http://www.invisionpower.com
 * @version		$Rev: 8812 $
 */

$dataHookLocations = array(

	/* MESSENGER DATA LOCATIONS */
	array( 'messengerSendReplyData', 'Messenger: Reply data'),
	array( 'messengerSendTopicData', 'Messenger: New conversation, topic data' ),
	array( 'messengerSendTopicFirstPostData', 'Messenger: New conversation, first post' ),
	
	/* PROFILE DATA LOCATIONS */
	array( 'profileCommentNew', 'Profile: New comment' ),
	array( 'profileFriendsNew', 'Profile: New friend' ),
	
);