<?php

/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.2.2
 * Profile Plugin Library
 * Last Updated: $Date: 2011-05-31 15:22:26 -0400 (Tue, 31 May 2011) $
 * </pre>
 *
 * @author 		$Author: bfarber $
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		http://www.invisionpower.com/community/board/license.html
 * @package		IP.Board
 * @subpackage	Members
 * @link		http://www.invisionpower.com
 * @since		20th February 2002
 * @version		$Revision: 8931 $
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.";
	exit();
}

class profile_friends extends profile_plugin_parent
{
	/**
	 * Feturn HTML block
	 *
	 * @param	array		Member information
	 * @return	string		HTML block
	 */
	public function return_html_block( $member=array() ) 
	{
		//-----------------------------------------
		// Got a member?
		//-----------------------------------------
		
		if ( ! is_array( $member ) OR ! count( $member ) )
		{
			return $this->registry->getClass('output')->getTemplate('profile')->tabNoContent( 'err_no_aboutme_to_show' );
		}
		
		$friends	= array();

		//-----------------------------------------
		// Grab the friends
		//-----------------------------------------

		$this->DB->build( array( 'select'		=> 'f.*',
								 'from'			=> array( 'profile_friends' => 'f' ),
								 'where'		=> 'f.friends_member_id=' . $member['member_id'] . ' AND f.friends_approved=1',
								 //'order'		=> 'm.members_display_name ASC',
								 'add_join'		=> array(
													  1 => array( 'select' => 'pp.*',
																  'from'   => array( 'profile_portal' => 'pp' ),
																  'where'  => 'pp.pp_member_id=f.friends_friend_id',
																  'type'   => 'left' ),
												 	  2 => array( 'select' => 'm.*',
																  'from'   => array( 'members' => 'm' ),
																  'where'  => 'm.member_id=f.friends_friend_id',
																  'type'   => 'left' ) 
													) 
								) 		);
		$outer	= $this->DB->execute();
		
		//-----------------------------------------
		// Get and store...
		//-----------------------------------------
		
		while( $row = $this->DB->fetch($outer) )
		{
			if( $row['member_id'] )
			{
				$friends[ $row['members_display_name'] ]	= IPSMember::buildDisplayData( $row, 0 );
			}
		}
		
		ksort($friends);
		
		$content = $this->registry->getClass('output')->getTemplate('profile')->tabFriends( $friends, $member );
		
		//-----------------------------------------
		// Macros...
		//-----------------------------------------
		
		$content = $this->registry->output->replaceMacros( $content );
		
		//-----------------------------------------
		// Return content..
		//-----------------------------------------
		
		return $content ? $content : $this->registry->getClass('output')->getTemplate('profile')->tabNoContent( 'err_no_aboutme_to_show' );
	}
	
}