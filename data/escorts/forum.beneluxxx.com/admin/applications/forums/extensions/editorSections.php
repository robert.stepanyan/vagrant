<?php

/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.2.2
 * BBCode Management : Determines if bbcode can be used in this section
 * Last Updated: $LastChangedDate: 2010-12-17 07:40:13 -0500 (Fri, 17 Dec 2010) $
 * </pre>
 *
 * @author 		$Author: ips_terabyte $
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		http://www.invisionpower.com/community/board/license.html
 * @package		IP.Board
 * @subpackage	Forums
 * @link		http://www.invisionpower.com
 * @version		$Rev: 7442 $
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.";
	exit();
}

/*
 * An array of key => value pairs
 * When going to parse, the key should be passed to the editor
 *  to determine which bbcodes should be parsed in the section
 *
 */
$BBCODE	= array(
				'topics'			=> ipsRegistry::getClass('class_localization')->words['ctype__topics'],
				'announcement'		=> ipsRegistry::getClass('class_localization')->words['ctype__announcement'],
				'rules'				=> ipsRegistry::getClass('class_localization')->words['ctype__rules'],
				);