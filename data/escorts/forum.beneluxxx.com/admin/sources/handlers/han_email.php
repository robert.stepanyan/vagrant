<?php
/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.2.2
 * API: Core
 * Last Updated: $Date: 2011-07-19 04:32:04 -0400 (Tue, 19 Jul 2011) $
 * </pre>
 *
 * @author 		$Author: mark $
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		http://www.invisionpower.com/community/board/license.html
 * @package		IP.Board
 * @link		http://www.invisionpower.com
 * @version		$Rev: 9276 $
 */
 
if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.";
	exit();
}

class hanEmail
{
	/**
	 * Emailer object reference
	 *
	 * @var		object
	 */
	public $emailer;
	
	/**
	 * Email header
	 *
	 * @var		string
	 */
	public $header;
	
	/**
	 * Email footer
	 *
	 * @var		string
	 */
	public $footer;
	
	/**
	 * Email from
	 *
	 * @var		string
	 */
	public $from;
	
	/**
	 * Email to
	 *
	 * @var		string
	 */
	public $to;
	
	/**
	 * Email cc's
	 *
	 * @var	array
	 */
	public $cc		= array();
	
	/**
	 * Email bcc's
	 *
	 * @var	array
	 */
	public $bcc		= array();
	
	/**
	 * Email subject
	 *
	 * @var		string
	 */
	public $subject;
	
	/**
	 * Email body
	 *
	 * @var		string
	 */
	public $message;
	
	/**
	 * HTML Mode
	 *
	 * @var		bool
	 */
	public $html_email = FALSE;
		
	/**
	 * Temp word swapping array
	 *
	 * @var		array
	 */
	protected $_words;
	
	/**
	 * Headers to pass to email lib
	 *
	 * @var		array
	 */
	protected $temp_headers		= array();
	
	protected $_attachments       = array();
	
	/**#@+
	 * Registry Object Shortcuts
	 *
	 * @var		object
	 */
	protected $registry;
	protected $DB;
	protected $settings;
	protected $request;
	protected $lang;
	protected $member;
	protected $memberData;
	/**#@-*/
	
	/**
	 * Construct
	 *
	 * @param	object		ipsRegistry reference
	 * @return	@e void
	 */
	public function __construct( ipsRegistry $registry )
	{
		/* Make object */
		$this->registry = $registry;
		$this->DB       = $this->registry->DB();
		$this->settings =& $this->registry->fetchSettings();
		$this->request  =& $this->registry->fetchRequest();
		$this->lang     = $this->registry->getClass('class_localization');
		$this->member   = $this->registry->member();
		$this->memberData =& $this->registry->member()->fetchMemberData();
	}
	
	/**
	 * Init method (setup stuff)
	 *
	 * @return	@e void
	 */
    public function init()
    {
		$this->header   = $this->settings['email_header'] ? $this->settings['email_header'] : '';
		$this->footer   = $this->settings['email_footer'] ? $this->settings['email_footer'] : '';

		$classToLoad = IPSLib::loadLibrary( IPS_KERNEL_PATH . 'classEmail.php', 'classEmail' );

		$this->emailer = new $classToLoad(
										 array(
										 		'debug'			=> $this->settings['fake_mail'] ? $this->settings['fake_mail'] : '0',
										 		'debug_path'	=> DOC_IPS_ROOT_PATH . '_mail',
										 		'smtp_host'		=> $this->settings['smtp_host'] ? $this->settings['smtp_host'] : 'localhost',
										 		'smtp_port'		=> intval($this->settings['smtp_port']) ? intval($this->settings['smtp_port']) : 25,
										 		'smtp_user'		=> $this->settings['smtp_user'],
										 		'smtp_pass'		=> $this->settings['smtp_pass'],
										 		'smtp_helo'		=> $this->settings['smtp_helo'],
										 		'method'		=> $this->settings['mail_method'],
										 		'wrap_brackets'	=> $this->settings['mail_wrap_brackets'],
										 		'extra_opts'	=> $this->settings['php_mail_extra'],
										 		'charset'		=> IPS_DOC_CHAR_SET,
										 		'html'			=> $this->html_email,
												)
										 );
										 
    }
    
    /**
     * Clear out any temporary headers
     *
     * @return	@e void
     */
    public function clearHeaders()
    {
    	$this->temp_headers	= array();
    }
    
    /**
     * Manually set an email header
     *
     * @param	string	Header key
     * @param	string	Header value
     * @return	@e void
     */
    public function setHeader( $key, $value )
    {
    	$this->temp_headers[ $key ]	= $value;
    }
    
	/**
	 * Send an email
	 *
	 * @return	boolean		Email sent successfully
	 */
	public function sendMail()
	{
		//$this->emailer->clearEmail();
		//$this->emailer->clearError();
		$this->init();
		if( $this->emailer->error )
		{
			return $this->fatalError( $this->emailer->error_msg, $this->emailer->error_help );
		}
		
		/* Add attachments if any */
		if ( count( $this->_attachments ) )
		{
			foreach( $this->_attachments as $a )
			{
				$this->emailer->addAttachment( $a[0], $a[1], $a[2] );
			}
		}
		
		$this->settings['board_name'] = $this->cleanMessage($this->settings['board_name']);
		
		$this->emailer->setFrom( $this->from ? $this->from : $this->settings['email_out'], $this->settings['board_name'] );
		$this->emailer->setTo( $this->to );
		
		foreach( $this->cc as $cc )
		{
			$this->emailer->addCC( $cc );
		}
		foreach( $this->bcc as $bcc )
		{
			$this->emailer->addBCC( $bcc );
		}
		
		if( count($this->temp_headers) )
		{
			foreach( $this->temp_headers as $k => $v )
			{
				$this->emailer->setHeader( $k, $v );
			}
		}

		//-----------------------------------------
		// Added strip_tags for the subject 4.16.2010
		// so we can have links in subject for inline
		// notifications and still use the subject
		//-----------------------------------------
		
		$this->emailer->setSubject( $this->_cleanSubject($this->subject) );
		$this->emailer->setBody( $this->message );
		$this->emailer->sendMail();
		
		// Unset HTML setting to remain backwards compatibility
		$this->html_email = FALSE;
		
		if( $this->emailer->error )
		{
			return $this->fatalError( $this->emailer->error_msg, $this->emailer->error_help );
		}
		
		return true;
	}
		
	/**
	 * Retrieve an email template
	 *
	 * @param	string		Template key
	 * @param	string		Language to use
	 * @param	string		Language file to load
	 * @param	string		Application of language file
	 * @return	@e void
	 */
	public function getTemplate( $name, $language="", $lang_file='public_email_content', $app='core' )
	{
		//-----------------------------------------
		// Check..
		//-----------------------------------------
		
		if( $name == "" )
		{
			$this->error++;
			$this->fatalError( "A valid email template ID was not passed to the email library during template parsing", "" );
		}
		
		//-----------------------------------------
		// Default?
		//-----------------------------------------

		if( ! $language )
		{
			$language = IPSLib::getDefaultLanguage();
		}
		
		//-----------------------------------------
		// Check and get
		//-----------------------------------------
		
		$this->registry->class_localization->loadLanguageFile( array( $lang_file ), $app, $language, TRUE );
		
		//-----------------------------------------
		// Stored KEY?
		//-----------------------------------------
		
		if ( ! isset($this->lang->words[ $name ]) )
		{
			if ( $language == IPSLib::getDefaultLanguage() )
			{
				$this->fatalError( "Could not find an email template with an ID of '{$name}'", "" );
			}
			else
			{
				$this->registry->class_localization->loadLanguageFile( array( $lang_file ), $app, IPSLib::getDefaultLanguage() );
				
				if ( ! isset($this->lang->words[ $name ]) )
				{
					$this->fatalError( "Could not find an email template with an ID of '{$name}'", "" );
				}
			}
		}
		
		//-----------------------------------------
		// Subject?
		//-----------------------------------------
		
		if ( isset( $this->lang->words[ 'subject__'. $name ] ) )
		{
			$this->subject = stripslashes( $this->lang->words[ 'subject__'. $name ] );
		}
		
		//-----------------------------------------
		// Return
		//-----------------------------------------
		
		$this->template = stripslashes($this->lang->words['email_header']) . stripslashes($this->lang->words[ $name ]) . stripslashes($this->lang->words['email_footer']);
	}
		
	/**
	 * Builds an email from a template, replacing variables
	 *
	 * @param	array		Replacement keys to values
	 * @param	bool		Do not "clean"
	 * @return	@e void
	 */
	public function buildMessage( $words, $noClean=false )
	{
		if( $this->template == "" )
		{
			$this->error++;
			$this->fatalError( "Could not build the email message, no template assigned", "Make sure a template is assigned first." );
		}
		
		$this->message = $this->template;

		if( $noClean )
		{
			$this->message	= str_replace( "\n", "<br />", str_replace( "\r", "", $this->message ) );
		}

		//-----------------------------------------
		// Add some default words
		//-----------------------------------------
		
		$words['BOARD_ADDRESS'] = $this->settings['board_url'] . '/index.' . $this->settings['php_ext'];
		$words['WEB_ADDRESS']   = $this->settings['home_url'];
		$words['BOARD_NAME']    = $this->settings['board_name'];
		$words['SIGNATURE']     = $this->settings['signature'] ? $this->settings['signature'] : '';
		
		//-----------------------------------------
		// Swap the words
		// 10.7.08 - Added replacements in subject
		//-----------------------------------------
		
		if( !$noClean )
		{
			foreach( $words as $k => $v )
			{
				$words[ $k ]	= $this->cleanMessage( $v );
			}
		}

		$this->_words = $words;
		
		$this->message = preg_replace_callback( "/<#(.+?)#>/", array( &$this, '_swapWords' ), $this->message );
		$this->subject = preg_replace_callback( "/<#(.+?)#>/", array( &$this, '_swapWords' ), $this->subject );

		$this->_words = array();
	}
	
	/**
	 * Replaces key with value
	 *
	 * @param	string		Key
	 * @return	string		Replaced variable
	 */
	protected function _swapWords( $matches )
	{
		return $this->_words[ $matches[1] ];
	}
	
	/**
	 * Cleans the email subject
	 *
	 * @param	string		In text
	 * @return	string		Out text
	 */
	protected function _cleanSubject( $subject )
	{
		$subject = strip_tags( $subject );
		
		$subject = str_replace( "&#036;", "\$", $subject );
		$subject = str_replace( "&#33;" , "!" , $subject );
		$subject = str_replace( "&#34;" , '"' , $subject );
		$subject = str_replace( "&#39;" , "'" , $subject );
		$subject = str_replace( "&#124;", '|' , $subject );
		$subject = str_replace( "&#38;" , '&' , $subject );
		$subject = str_replace( "&#58;" , ":" , $subject );
		$subject = str_replace( "&#91;" , "[" , $subject );
		$subject = str_replace( "&#93;" , "]" , $subject );
		$subject = str_replace( "&#064;", '@' , $subject );
		$subject = str_replace( "&nbsp;" , ' ', $subject );
		$subject = str_replace( "&amp;" , '&', $subject );
		
		return $subject;
	}
		
	/**
	 * Cleans an email message
	 *
	 * @param	string		Email content
	 * @return	string		Cleaned email content
	 */
	public function cleanMessage( $message = "" ) 
	{
		$message = preg_replace( '#\[quote(?:[^\]]+?)?\](.+?)\[/quote\]#s', "<br /><br />------------ QUOTE ----------<br />\\1<br />-----------------------------<br /><br />" , $message );
		$message = preg_replace_callback( '#\[url=(.+?)\](.+?)\[/url\]#', array( $this, "_formatUrl" ), $message );
		

		//-----------------------------------------
		// Unconvert smilies 'cos at this point they are img tags
		//-----------------------------------------
		
		$message = IPSText::unconvertSmilies( $message );
		
		//-----------------------------------------
		// We may want to adjust this later, but for
		// now just strip any other html
		//-----------------------------------------

		$message = strip_tags( $message, '<br>' );

		IPSText::getTextClass( 'bbcode' )->parse_html		= 0;
		IPSText::getTextClass( 'bbcode' )->parse_nl2br		= 1;
		IPSText::getTextClass( 'bbcode' )->parse_bbcode		= 0;
		
		$message = IPSText::getTextClass('bbcode')->stripAllTags( $message, true );

		//-----------------------------------------
		// Bear with me...
		//-----------------------------------------
		
		$message = str_replace( "\n"			, "\r\n", $message );
		$message = str_replace( "\r"			, ""	, $message );
		$message = str_replace( "<br>"			, "\r\n", $message );
		$message = str_replace( "<br />"		, "\r\n", $message );
		$message = str_replace( "\r\n\r\n"		, "\r\n", $message );
		
		$message = str_replace( "&quot;", '"' , $message );
		$message = str_replace( "&#092;", "\\", $message );
		$message = str_replace( "&#036;", "\$", $message );
		$message = str_replace( "&#33;" , "!" , $message );
		$message = str_replace( "&#34;" , '"' , $message );
		$message = str_replace( "&#39;" , "'" , $message );
		$message = str_replace( "&#40;" , "(" , $message );
		$message = str_replace( "&#41;" , ")" , $message );
		$message = str_replace( "&lt;"  , "<" , $message );
		$message = str_replace( "&gt;"  , ">" , $message );
		$message = str_replace( "&#124;", '|' , $message );
		$message = str_replace( "&amp;" , "&" , $message );
		$message = str_replace( "&#38;" , '&' , $message );
		$message = str_replace( "&#58;" , ":" , $message );
		$message = str_replace( "&#91;" , "[" , $message );
		$message = str_replace( "&#93;" , "]" , $message );
		$message = str_replace( "&#064;", '@' , $message );
		$message = str_replace( "&#60;" , '<' , $message );
		$message = str_replace( "&#62;" , '>' , $message );
		$message = str_replace( "&nbsp;" , ' ', $message );

		return $message;
	}
	
	/**
	 * Format url for email
	 *
	 * @param	array		preg_replace matches
	 * @return	string		Formatted url
	 */
	public function _formatUrl( $matches ) 
	{
		$matches[1]	= str_replace( array( '"', "'", '&quot;', '&#039;', '&#39;' ), '', $matches[1] );
		
		return $matches[2] . ' (' . $matches[1] . ')';
	}
	
	/**
	 * Add an attachment to the current email
	 *
	 * @param	string	File data
	 * @param	string	File name
	 * @param	string	File type (MIME)
	 * @return	@e void
	 */
	public function addAttachment( $data="", $name="", $ctype='application/octet-stream' )
	{
		$this->_attachments[] = array( $data, $name, $ctype );
	}
	
	/**
	 * Log a fatal error
	 *
	 * @param	string		Message
	 * @param	string		Help key (deprecated)
	 * @return	bool
	 */
	protected function fatalError( $msg, $help="" )
	{
		$this->DB->insert( 'mail_error_logs',
										array(
												'mlog_date'     => time(),
												'mlog_to'       => $this->to,
												'mlog_from'     => $this->from,
												'mlog_subject'  => $this->subject,
												'mlog_content'  => substr( $this->message, 0, 200 ),
												'mlog_msg'      => $msg,
												'mlog_code'     => $this->emailer->smtp_code,
												'mlog_smtp_msg' => $this->emailer->smtp_msg
											 )
									  );
		
		return false;
	}	
}