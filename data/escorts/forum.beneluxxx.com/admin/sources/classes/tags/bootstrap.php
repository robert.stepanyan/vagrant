<?php

/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.2.2
 * Tagging: Bootstrap
 * Matt Mecham
 * Last Updated: $Date: 2011-03-21 10:32:36 -0400 (Mon, 21 Mar 2011) $
 * </pre>
 *
 * @author 		$Author: ips_terabyte $
 * @copyright	(c) 2001 - 2011 Invision Power Services, Inc.
 * @license		http://www.invisionpower.com/community/board/license.html
 * @package		IP.Board
 * @link		http://www.invisionpower.com
 * @since		24 Feb 2011
 * @version		$Revision: 8134 $
 *
 */

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.";
	exit();
}

class classes_tags_bootstrap
{
	/**
	 * App object
	 *
	 * @var array
	 */
	static protected $apps;
	
	/**
	 * Construct
	 *
	 * @param	string		Application
	 * @param	string		Area
	 * @return	string
	 */
	public static function run( $app=null, $area=null )
	{
		if ( $app === null OR $area === null )
		{
			trigger_error( "App or area missing from classes_like", E_USER_WARNING );
		}
		
		/* Pointless comment! */
		$_file	= IPSLib::getAppDir( $app ) . '/extensions/tags/' . $area . '.php';
		$_key	= ( $app && $area ) ? md5( $app . $area ) : 'default';
		 
		/* Get from cache if already cached */
		if ( isset( self::$apps[ $_key ] ) )
		{
			return self::$apps[ $_key ];
		}
		
		/* Get other classes */
		require_once( IPS_ROOT_PATH . 'sources/classes/tags/abstract.php');/*noLibHook*/
		
		if ( $app && $area )
		{
			/* Otherwise create object and cache */
			if ( is_file( $_file ) )
			{
				$classToLoad = IPSLib::loadLibrary( $_file, 'tags_' . $app . '_' . $area, $app );
				
				if ( class_exists( $classToLoad ) )
				{
					self::$apps[ $_key ] = new $classToLoad();
					self::$apps[ $_key ]->setApp( $app );
					self::$apps[ $_key ]->setArea( $area );
					self::$apps[ $_key ]->init();
					
				}
				else
				{
					throw new Exception( "No tags class available for $app - $area" );
				}
			}
			else
			{
				throw new Exception( "No tags class available for $app - $area" );
			}
		}
		else
		{
			$classToLoad = IPSLib::loadLibrary( IPS_ROOT_PATH . 'sources/classes/tags/extensions/default.php', 'tags_default' );
			self::$apps[ $_key ] = new $classToLoad();
			self::$apps[ $_key ]->setApp( $app );
			self::$apps[ $_key ]->setArea( $area );
			self::$apps[ $_key ]->init();
		}
		
		return self::$apps[ $_key ];
	}
}