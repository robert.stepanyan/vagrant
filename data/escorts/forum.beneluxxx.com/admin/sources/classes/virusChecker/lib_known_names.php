<?php
/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.2.2
 * Virus scanner: known blacklisted filenames
 * Last Updated: $Date: 2010-12-17 08:01:38 -0500 (Fri, 17 Dec 2010) $
 * </pre>
 *
 * @author 		$Author: ips_terabyte $
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		http://www.invisionpower.com/community/board/license.html
 * @package		IP.Board
 * @link		http://www.invisionpower.com
 * @since		Tue. 17th August 2004
 * @version		$Rev: 7445 $
 *
 */

$KNOWN_NAMES = array(
'lang_choise.php',
'00.php',
'd6.php',
'r57.php',
'skin_admin.php',
'test.php3',
'1.php',
'2.php',
'.php',
'mysql.php',
'shell.php',
'c99.php',
'info.php',
'skin_msq.php',
'lang_msq.php',
'skin_posts.php',
'lang_posts.php',
);
