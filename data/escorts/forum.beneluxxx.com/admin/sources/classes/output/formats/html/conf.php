<?php

/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.2.2
 * Ouput format: HTML
 * (Matt Mecham)
 * Last Updated: $Date: 2010-12-17 08:01:38 -0500 (Fri, 17 Dec 2010) $
 * </pre>
 *
 * @author 		$Author: ips_terabyte $
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 * @license		http://www.invisionpower.com/community/board/license.html
 * @package		IP.Board
 * @link		http://www.invisionpower.com
 * @since		9th March 2005 11:03
 * @version		$Revision: 7445 $
 *
 */
$config = array();

/* Identifies publicly as... */
$config['identifies_as'] = 'HTML';

/* Gateway file? */
$config['gateway_file']  = 'index.php';