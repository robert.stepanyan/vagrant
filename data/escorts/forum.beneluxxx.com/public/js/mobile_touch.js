document.observe("dom:loaded", function() 
{
	$$("#secondary_navigation a").each( function(e) { Event.observe(e, "click", loadUrl ); } );	
	$$(".touch-row").each(function(e) { Event.observe(e, "click", touchRowClick); addArrow(e); });
	$$("a.prev, a.next").invoke("on", "click", loadUrl);
	$$(".post").each(function(e) { Event.observe(e, "click", postClick); });
	$$('.sd_content').invoke('on', 'click', '.sd_content', toggleDeletedContent);
	Event.observe($('options-button'), "click", openNavigation);
	if($('filter-option')) {Event.observe($('filter-option'), "click", openFilter);}
	$('options-button').setStyle({'display': 'block'});
	if( $('nav_markread') ){
		$('nav_markread').down('a').observe('click', markAsRead);
	};
	
	if( $('show_skins') ){
		$('show_skins').on('click', function(e){
			$('show_skins').hide();
			$('show_skins_box').show();
		});
	}
	if( $('show_langs') ){
		$('show_langs').on('click', function(e){
			$('show_langs').hide();
			$('show_langs_box').show();
		});
	}
	
	/* Set this here to 'toggle' works later */
	$('shade').setStyle({'display': 'none'});
	
	if ( $('filter-letters') ){
		$('filter-letters').toggleClassName('hidden');
	}
	
	// Set up user nav box
	var items = $("user_navigation").down("ul").select("li").size();
	var diff = 3 - (items % 3);
	
	for(i=0; i<diff; i++){
		$("user_navigation").down("ul").insert({bottom: new Element("li").addClassName("dummy").insert( new Element("span") ) });
	}
});

function toggleDeletedContent(e, element)
{
	Event.stop(e);
	
	var id = element.id.replace('seeContent_', '');
	
	$('postsDelete-' + id).hide();
	$('post-' + id).show();	
}

function markAsRead(e)
{
	if( !confirm( ipb.lang['clear_markboard'] ) ){
		Event.stop(e);
	}	
}

function mobileFilter( e, element )
{
	// Does the pane exist?
	if( !$( element.id + '_pane' ) ){
		return;
	}
	
	$('shade').toggle();
	$( element.id + '_pane' ).show();
}

function closePane( e, element )
{
	Event.stop(e);
	$(element).up(".ipsFilterPane").hide();
	$('shade').hide();
}

/**
 * Add the touch arrow */
function addArrow(e)
{
	d = e.getDimensions();
	t = ( d.height / 2 ) - 18;
	
	if ( ! e.inspect().match( '<h2' ) )
	{
		e.insert( { 'top' : new Element( 'div', { 'class': 'touch-row-arrow', 'style': 'margin-top:' + t + 'px !important' } ) } );
	}
}

function touchRowClick()
{
	$$('#' + this.id + ' a.title').each(function(e) { loadUrl( e ); });
}

function loadUrl( e )
{
	/* Show loading box */
	var content = LOADING_TEMPLATE.evaluate();
	
	$('ipbwrapper').insert( { 'after' : content } );
	positionCenter( $('loadingBox') );
	
	window.location = e.href;
}

function postClick()
{
	if( $(this.id + '-controls') ){
		$(this.id + '-controls').toggleClassName('visible');
	}
}

function openNavigation()
{
	//vp = document.viewport.getDimensions();
	
	var elem = $( document.body ).getLayout();
	$('user_navigation').toggle();
	$('user_navigation').setStyle( { 'position': 'absolute', 'width': elem.get('margin-box-width') + 'px' } );
	$('shade').toggle();
}

function openFilter()
{
	if ( $('filter-letters') )
	{
		$('filter-letters').toggleClassName('hidden');
	}
	
	$('filter-option').setStyle({'display': 'none'});
}

function positionCenter( elem, dir )
{
	if( !$(elem) ){ return; }
	elem_s = $(elem).getDimensions();
	window_s = document.viewport.getDimensions();
	window_offsets = document.viewport.getScrollOffsets();

	center = { 	left: ((window_s['width'] - elem_s['width']) / 2),
				 top: ((window_s['height'] - elem_s['height']) / 2)
			};

	if ( window_offsets['top'] )
	{
		center['top'] += window_offsets['top'];
	}
	
	if( typeof(dir) == 'undefined' || ( dir != 'h' && dir != 'v' ) )
	{
		$(elem).setStyle('top: ' + center['top'] + 'px; left: ' + center['left'] + 'px');
	}
	else if( dir == 'h' )
	{
		$(elem).setStyle('left: ' + center['left'] + 'px');
	}
	else if( dir == 'v' )
	{
		$(elem).setStyle('top: ' + center['top'] + 'px');
	}
	
	$(elem).setStyle('position: fixed');
}