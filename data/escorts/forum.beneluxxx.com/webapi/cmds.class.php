<?php

class WebApi_Commands
{
	const DOMAIN = '.beneluxxx.com';
	const URL = 'https://forum.beneluxxx.com/';

	public function RegisterUser($user_name, $email) 
	{
		global $DB;
		
		$member_id = $DB->fetchOne('SELECT member_id FROM members WHERE name = ?', $user_name);
		
		if ( $member_id ) 
		{
			$DB->delete('members', $DB->quoteInto('member_id = ?', $user->member_id));
			$DB->delete('profile_portal', $DB->quoteInto('pp_member_id = ?', $user->member_id));
			$DB->delete('pfields_content', $DB->quoteInto('member_id = ?', $user->member_id));
		}
				
		$DB->insert('members', array(
			'name' => $user_name,
			'member_group_id' => 3,
			'email' => $email,
			'joined' => time(),
			'allow_admin_mails' => 1,
			'language' => 1,
			'msg_show_notification' => 1,
			'members_auto_dst' => 1,
			'members_display_name' => $user_name,
			'members_seo_name' => $user_name,
			'members_l_display_name' => $user_name,
			'members_l_username' => $user_name,
			'members_profile_views' => 1,
			'member_banned' => 0,
			'member_uploader' => 'flash'
		));
		
		$id = $DB->lastInsertId();
		
		$DB->insert('profile_portal', array(
			'pp_member_id' => $id,
			'pp_setting_count_friends' => 1,
			'pp_setting_count_comments' => 1,
			'pp_setting_count_visitors' => 1
		));
		
		$DB->insert('pfields_content', array(
			'member_id' => $id
		));
	}
	
	public function UpdateEmail($user_name, $email) 
	{
		global $DB;
		
		$DB->update('members', array('email' => $email), $DB->quoteInto('name = ?', $user_name));
	}
	
	public function DeleteUser($user_name) 
	{
		global $DB;
		
		$member_id = $DB->fetchOne('SELECT member_id FROM members WHERE name = ?', $user_name);
		
		if ($member_id)
		{
			$DB->delete('members', $DB->quoteInto('member_id = ?', $member_id));
			$DB->delete('profile_portal', $DB->quoteInto('pp_member_id = ?', $member_id));
			$DB->delete('pfields_content', $DB->quoteInto('member_id = ?', $member_id));
		}
	}
	
	public function BannUser($user_name, $status) 
	{
		global $DB;
		
		if ($status == 1)
		{
			$member_group_id = 5;
			$member_banned = 1;
			$members_auto_dst = 0;
		}
		else
		{
			$member_group_id = 3;
			$member_banned = 0;
			$members_auto_dst = 1;
		}
				
		$DB->update('members', array(
			'member_group_id' => $member_group_id,
			'member_banned' => $member_banned,
			'members_auto_dst' => $members_auto_dst
		), $DB->quoteInto('name = ?', $user_name));
	}
	
	public function Login($user_name = null, $email = null) 
	{
		global $DB, $sid, $LOG;
		
		$sc = 'f45F7ndfDSABL';
		$v = md5($user_name . $sc);
		
		$user = $DB->query('SELECT * FROM members WHERE name = ?', $user_name)->fetch();
	
		if ( ! $user ) {
			$this->RegisterUser($user_name, $email);
			$this->Login($user_name, $email);
			return TRUE;
		}
		else {
			$do = false;
			
			if (isset($_REQUEST['has_package']))
			{
				if ($_REQUEST['has_package'] == 1)
				{
					$r_p = '';
					$do = true;
				}
				elseif ($_REQUEST['has_package'] == 0)
				{
					$r_p = '1';
					$do = true;
				}
				
				if ($do)
				{
					if ($user['restrict_post'] != $r_p)
						$DB->update('members', array('restrict_post' => $r_p), $DB->quoteInto('member_id = ?', $user['member_id']));
				}
			}
						
			require_once 'Zend/Http/Client.php';
			require_once 'Zend/Http/CookieJar.php';
			
			$url = self::URL . 'index.php?app=core&module=global&section=login&do=process';
			
			$httpClient = new Zend_Http_Client($url, array('maxredirects' => 3));

			$jar = new Zend_Http_CookieJar();
			$httpClient->setCookieJar($jar);
			
			$httpClient->setParameterPost(array(
				'ips_username' => $user_name,
				'auth_key' => '880ea6a14ea49e853634fbdc5015a024',//'f7606e2e846101aae5c99e4a10e8eb97'
				'cx_ip_address' => $_REQUEST['ip_address'],
				'cx_user_agent' => $_REQUEST['user_agent'],
				'v' => $v
			));
			$response = $httpClient->request(Zend_Http_Client::POST);
			
			require 'Cubix_Uri.php';
			$cookie = $jar->getCookie(Cubix_Uri::factory('https://' . self::DOMAIN), 'session_id');
			
			//$session_id = $jar->getCookie(Cubix_Uri::factory(self::DOMAIN, 'session_id'))->getValue();
			$session_id = $cookie->getValue();
						
			return json_encode(array(
				//'response' => var_export($response->getBody(), true),
				'session_id' => $session_id, 
				'member_key' => md5($user['email'] . '&' . $user['member_login_key'] . '&' . $user['joined'])
			));
		}
		
		return TRUE;
	}
	
	
	public function LogOut() 
	{	
		require_once 'Zend/Http/Client.php';
		require_once 'Zend/Http/CookieJar.php';
		
		$url = self::URL . 'index.php?app=core&module=global&section=login&do=logout&k=' . $_REQUEST['member_key'];
		
		$httpClient = new Zend_Http_Client($url, array('maxredirects' => 0));
		$jar = new Zend_Http_CookieJar();
		
		$httpClient->setCookieJar($jar);
		$jar->addCookie('session_id=' . $_REQUEST['session_id'] . '; path=/; domain=' . self::DOMAIN);
		
		$httpClient->setParameterPost(array(
			'cx_ip_address' => $_REQUEST['ip_address'],
			'cx_user_agent' => $_REQUEST['user_agent']
		));
		$response = $httpClient->request(Zend_Http_Client::POST);
		
		return TRUE;
	}
	
	public function AddReviewAsTopicAndPost($title, $title_seo, $ip, $post)
	{
		// this is for forum.beneluxxx.com ONLY
		global $DB;

		try{
			$member_id = 12396; // add topic and post via this member
			$member = $DB->query('SELECT members_display_name, members_seo_name FROM members WHERE member_id = ?', $member_id)->fetch();

			$forum_id = 8; // forum for reviews

			// add topic
			$DB->insert('topics', array(
				'title' => $title,
				'state' => 'open',
				'posts' => 1,
				'starter_id' => $member_id,
				'start_date' => time(),
				'last_poster_id' => $member_id,
				'last_post' => time(),
				'starter_name' => $member['members_display_name'],
				'last_poster_name' => $member['members_display_name'],
				'poll_state' => 0,
				'last_vote' => 0,
				'views' => 0,
				'forum_id' => $forum_id,
				'approved' => 1,
				'author_mode' => 1,
				'pinned' => 0,
				'title_seo' => $title_seo,
				'seo_last_name' => $member['members_seo_name']
			));

			// get added topic id
			$topic_id = $DB->lastInsertId();

			// add post
			$DB->insert('posts', array(
				'author_id' => $member_id,
				'author_name' => $member['members_display_name'],
				'use_sig' => 1,
				'use_emo' => 1,
				'ip_address' => $ip,
				'post_date' => time(),
				'post' => $post,
				'topic_id' => $topic_id,
				'new_topic' => 1,
				'post_key' => md5(time())
			));

			// get added post id
			$post_id = $DB->lastInsertId();

			// update topic first post id
			$DB->update('topics', array('topic_firstpost' => $post_id), $DB->quoteInto('tid = ?', $topic_id));

			// update forum data
			$DB->update('forums', array(
				'topics' => new Zend_Db_Expr('topics + 1'),
				'posts' => new Zend_Db_Expr('posts + 1'),
				'last_post' => time(),
				'last_poster_id' => $member_id,
				'last_poster_name' => $member['members_display_name'],
				'last_title' => $title,
				'last_id' => $topic_id,
				'newest_title' => $title,
				'newest_id' => $topic_id,
				'seo_last_title' => $title_seo,
				'seo_last_name' => $member['members_seo_name']
			), $DB->quoteInto('id = ?', $forum_id));

			// update member data
			$DB->update('members', array(
				'posts' => new Zend_Db_Expr('posts + 1'),
				'last_post' => time(),
				'last_activity' => time()
			), $DB->quoteInto('member_id = ?', $member_id));
		} catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		
		echo 'Successfully added';
	}
}

