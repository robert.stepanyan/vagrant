<?php

require_once 'Zend/Uri/Http.php';
class Cubix_Uri_Http extends Zend_Uri_Http
{
	public function validateHost($host = null)
    {
        if ($host === null) {
            $host = $this->_host;
        }

        // If the host is empty, then it is considered invalid
        if (strlen($host) === 0) {
            return false;
        }

        return true;
    }
}
