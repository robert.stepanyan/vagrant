<?php

/*******************************************************
NOTE: This is a cache file generated by IP.Board on Fri, 22 Jun 2012 08:29:05 +0000 by admin_forum_bl
Do not translate this file as you will lose your translations next time you edit via the ACP
Please translate via the ACP
*******************************************************/



$lang = array( 
'spam_graph_title' => "Inscriptions récentes",
'spam_ok' => "Ok",
'spam_spam' => "Spam",
'statisnotspam' => "Non spam",
'statisspam' => "Spam",
'stats_apr' => "Avril",
'stats_asc' => "Croissant - Les dates les plus vieilles en premières",
'stats_aug' => "Août",
'stats_count' => "Compteur",
'stats_daily' => "Jour",
'stats_date' => "Date",
'stats_datefrom' => "<strong>Depuis la date</strong> ",
'stats_dateto' => "<strong>Jusqu'à la date</strong> ",
'stats_dec' => "Décembre",
'stats_desc' => "Décroissant - Les dates les plus récentes en premières",
'stats_feb' => "Février",
'stats_fromincorrect' => "La date renseignée dans 'Depuis la date :' est incorrecte. Veuillez vérifier l'entrée et ré-essayez.",
'stats_jan' => "Janvier",
'stats_jul' => "Juillet",
'stats_jun' => "Juin",
'stats_mar' => "Mars",
'stats_may' => "Mai",
'stats_monthly' => "Mois",
'stats_msg' => "Statistiques des MP envoyés",
'stats_msg_nav' => "Messages privés envoyés",
'stats_nov' => "Novembre",
'stats_oct' => "Octobre",
'stats_post' => "Statistiques des messages",
'stats_post_nav' => "Nombre de messages",
'stats_reg' => "Statistiques des inscriptions",
'stats_reg_nav' => "Utilisateurs inscrits",
'stats_results' => "Résultats",
'stats_sep' => "Septembre",
'stats_show' => "Montrer",
'stats_sorting' => "<strong>Tri des résultats</strong> ",
'stats_spam' => "Statistiques du service spam",
'stats_timescale' => "<strong>Échelle de temps</strong> ",
'stats_title' => "Centre des statistiques",
'stats_title_results' => "Résultats du centre de statistiques",
'stats_to' => " à ",
'stats_toincorrect' => "La date renseignée dans 'Jusqu'à la date :' est incorrecte. Veuillez vérifier l'entrée et ré-essayez.",
'stats_topic' => "Statistiques des nouveaux sujets",
'stats_topic_nav' => "Sujets démarrés",
'stats_total' => "<strong>Total</strong>",
'stats_views' => "Statistisques des visualisations de sujet",
'stats_views_nav' => "Visualisations des sujets",
'stats_weekly' => "Semaine",
'stats_weekno' => "Semaine #",
'timescale_daily' => "Quotidien -",
'timescale_monthly' => "Mensuel -",
'timescale_weekly' => "Hebdomadaire -",
'topic_view_warning' => "Ces statistiques vous indiquent le nombre de visualisations enregistrées pour les sujets créés durant la période de temps sélectionnée. Elles n'indiquent pas le nombre de visualisations pour cette période.",
 ); 
