<?php

/*******************************************************
NOTE: This is a cache file generated by IP.Board on Fri, 22 Jun 2012 08:29:05 +0000 by admin_forum_bl
Do not translate this file as you will lose your translations next time you edit via the ACP
Please translate via the ACP
*******************************************************/



$lang = array( 
'active_users_titlef' => "%s utilisateur(s) parcour(en)t ce forum",
'announce_row' => "Annonce",
'ascending_order' => "Croissant (A-Z)",
'attach_page_title' => "Fichiers joints",
'attach_post' => "Message #",
'attach_post_date' => "Soumis le",
'attach_size' => "Taille",
'attach_title' => "Fichier joint",
'back_to_forum' => "Retour au forum",
'cat_subforums' => "Sous-forums contenus dans la catégorie",
'cpt_approve_f' => "Rendre visible (Accepter)",
'cpt_close_f' => "Fermer les sujets",
'cpt_delete_f' => "Supprimer les sujets",
'cpt_merge_f' => "Combiner les sujets",
'cpt_move_f' => "Déplacer les sujets",
'cpt_open_f' => "Ouvrir les sujets",
'cpt_pin_f' => "Épingler les sujets",
'cpt_unapprove_f' => "Rendre invisible (Refuser)",
'cpt_undelete_f' => "Restaurer les sujets supprimés",
'cpt_unpin_f' => "Libérer les sujets",
'descending_order' => "Décroissant (Z-A)",
'dltt_delete_from_topic_desc' => "Cette option <strong>supprime définitivement le sujet</strong> du forum.",
'dltt_remove_from_view_desc' => "Cette option <strong>masque le sujet</strong>, mais le laisse dans le forum.",
'dlt_delete_from_topic' => "Supprimer du forum",
'dlt_delete_from_topic_desc' => "Cette option <strong>supprime définitivement le message</strong> du sujet.",
'dlt_delete_from_topic_extra' => "Il pourra être examiné dans le panneau de contrôle du modérateur",
'dlt_delete_now' => "Supprimer maintenant",
'dlt_delete_topic' => "Supprimer",
'dlt_reason' => "Raison :",
'dlt_remove' => "Supprimer",
'dlt_remove_from_view' => "Supprimer de l'affichage",
'dlt_remove_from_view_desc' => "Cette option <strong>masque le message</strong>, mais le laisse dans le sujet.",
'dlt_remove_from_view_extra' => "Les modérateurs auront toujours la capacité de le voir",
'dlt_restore_topic' => "Restaurer",
'dlt_title' => "Supprimer le sujet",
'enter_pass' => "Mot de passe du forum",
'filter_direction' => "Ordre de tri",
'filter_options' => "Cliquer ici pour afficher les options de filtrage",
'filter_sort' => "Trier par",
'filter_time' => "Période :",
'filter_type' => "Voir le type",
'first_unread_post' => "Aller au premier message non-lu ",
'follow_notify_forums' => "forums",
'forum_by' => "Par :",
'forum_last_post_info' => "Infos sur le dernier message",
'forum_led_by' => "Forum modéré par",
'forum_legend' => "Légende du forum",
'forum_management' => "Gestion du forum",
'forum_no_start_topic' => "Vous ne pouvez pas commencer un sujet",
'forum_rules' => "Règles du forum",
'forum_started_by' => "Commencé par",
'forum_stats' => "Statistiques",
'forum_topic' => "Sujet",
'forum_topic_list' => "Sujets dans ce forum",
'from_f_perms' => "Permissions",
'ft_title' => "Suivre ce forum",
'ft_title_stop' => "Cesser de suivre ce forum",
'f_go' => "avec ceux sélectionnés",
'f_pass_submit' => "Continuer",
'goto_forum' => "Aller au forum",
'goto_last_post' => "Aller au dernier message",
'go_back_to' => "Retourner dans %s",
'in_last_topic' => "Dans :",
'jscript_otherpage' => "depuis d'autres pages",
'lastby' => "Dernier par",
'like_follow_f' => "suivre ce forum",
'like_totalcount_follow_f' => "%s membre(s) sui(ven)t ce forum",
'like_ucfirst_follow_f' => "Suivre ce forum",
'like_ucfirst_unfollow_f' => "Cesser de suivre ce forum",
'like_unfollow_f' => "cesser de suivre ce forum",
'load_more_topics' => "Charger davantage de sujets",
'logged_in' => "Vous avez été autorisé à accéder à ce forum",
'mark_as_read' => "Marquer ce forum comme lu",
'mini_page_count' => "%s pages",
'mm_title' => "Multi-modération",
'mod_hidden' => "Afficher les sujets cachés",
'mod_hidden_posts' => "Afficher les sujets avec des messages cachés",
'mod_prumemovetopics' => "Élaguer/Déplacer en masse des sujets",
'mod_prune' => "Élaguer / Déplacer en masse",
'mod_resync' => "Resynchroniser le forum",
'mod_showallinvisible' => "Afficher tous les sujets invisibles",
'mod_showallposts' => "Afficher tous les sujets avec des messages invisibles",
'need_password' => "Saisir le mot de passe du forum",
'need_password_txt' => "<strong>Ce forum est protégé par un mot de passe !</strong><br/ >Vous devez saisir le mot de passe pour accéder à ce forum. Merci de vous assurer que votre navigateur est capable de stocker des cookies temporaires.",
'nmo_t_approve' => "&middot; Approuver %s sujet(s) non-approuvé(s) du forum",
'nmo_t_delete_approve' => "&middot; Supprimer définitivement %s sujet(s) non-approuvé(s) du forum",
'nmo_t_delete_softed' => "&middot; Supprimer définitivement %s sujet(s) supprimé(s) du forum",
'nmo_t_restore' => "&middot; Restaurer %s sujet(s) supprimé(s) du forum",
'no_topics' => "Aucun sujet n'a été trouvé. C'est soit parce qu'il n'y a pas de sujet dans ce forum, soit que les sujets sont plus anciens que la date de sélection courante.",
'pm_hot_new' => "Sujet chaud (nouvelles réponses)",
'pm_hot_no' => "Sujet chaud (pas de nouvelle réponse)",
'pm_locked' => "Ce sujet est fermé",
'pm_moved' => "Ce sujet a été déplacé",
'pm_open_new' => "Sujet ouvert (nouvelles réponses)",
'pm_open_no' => "Sujet ouvert (pas de nouvelle réponse)",
'pm_poll' => "Sondage (nouveaux votes)",
'pm_poll_no' => "Sondage (pas de nouveau vote)",
'poll_prefix' => "Sondage",
'posted_by' => "Posté par",
'remember_options' => "Se souvenir de ces options",
'rename_topic_update' => "Mettre à jour",
'replies' => "réponses",
'reply' => "réponse",
'search_forum' => "Rechercher",
'show_10_days' => "Depuis 10 jours",
'show_15_days' => "Depuis 15 jours",
'show_20_days' => "Depuis 20 jours",
'show_25_days' => "Depuis 25 jours",
'show_30_days' => "Depuis 30 jours",
'show_5_days' => "Depuis 5 jours",
'show_60_days' => "Depuis 60 jours",
'show_7_days' => "Depuis 7 jours",
'show_90_days' => "Depuis 90 jours",
'show_all' => "Tout afficher",
'show_last_visit' => "Depuis ma dernière visite",
'show_mod_tools' => "Afficher les outils de modération",
'show_mod_tools_desc' => "Fournit des outils pour gérer les sujets dans ce forum",
'show_today' => "Aujourd'hui",
'show_year' => "Depuis 365 jours",
'sort_by_attach' => "Fichiers joints",
'sort_by_date' => "Date du dernier message",
'sort_by_last_poster' => "Nom du dernier posteur",
'sort_by_poster' => "Nom du créateur",
'sort_by_replies' => "Nombre de réponses",
'sort_by_start' => "Date de création",
'sort_by_topic' => "Titre",
'sort_by_views' => "Nombre de lectures",
'sort_custom' => "Personnalisé",
'sort_recent' => "Mis à jour récemment",
'sort_replies' => "Avec le plus de réponses",
'sort_start' => "Date de début",
'sort_submit' => "Mettre à jour le filtre",
'sort_views' => "Le plus vu",
'tdb__forumindex' => "Sujet supprimé par %s, ",
'tdb__noreasongi' => "Aucune raison donnée",
'topicfilter_all' => "Sujets : Tous",
'topicfilter_hot' => "Sujets : Chauds",
'topicfilter_ireplied' => "Sujets : Auxquels j'ai répondu",
'topicfilter_istarted' => "Sujets : Que j'ai créés",
'topicfilter_locked' => "Sujets : Fermés",
'topicfilter_moved' => "Sujets : Déplacés",
'topicfilter_open' => "Sujets : Ouverts",
'topicfilter_poll' => "Sujets : Sondages",
'topics' => "sujets",
'topic_attach' => "fichier(s) joint(s) : Voir",
'topic_close' => "Fermer le sujet",
'topic_delete' => "Supprimer le sujet",
'topic_goto_page' => "Aller à la page",
'topic_is_hot' => "Populaire",
'topic_move' => "Déplacer le sujet",
'topic_open' => "Ouvrir le sujet",
'topic_pin' => "Épingler le sujet",
'topic_queued_count' => "Ce sujet a <strong>%s</strong> message(s) en attente. Cliquer pour l'afficher.",
'topic_rename' => "Renommer le sujet",
'topic_select_all' => "Sélectionner tous les sujets",
'topic_start' => "Commencer un sujet",
'topic_started_by' => "Débuté par %s,",
'topic_started_on' => "Sujet commencé :",
'topic_unpin' => "Libérer le sujet",
'using_skin_set' => "Utilise le thème :",
'views' => "vues",
'view_announcement' => "Voir l&#039;annonce",
'view_deleted_posts' => "Afficher les sujets supprimés",
'view_forum_rules' => "Voir les règles du forum",
'view_post' => "Voir le message",
'view_reported_posts' => "Voir les messages signalés",
'view_uapproved_posts' => "Voir les messages refusés",
'you_posted_here' => "Vous avez posté dans ce sujet",
 ); 
