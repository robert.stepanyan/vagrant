<?php

/*******************************************************
NOTE: This is a cache file generated by IP.Board on Fri, 22 Jun 2012 08:29:05 +0000 by admin_forum_bl
Do not translate this file as you will lose your translations next time you edit via the ACP
Please translate via the ACP
*******************************************************/



$lang = array( 
'added_poll' => "Ajout d'un sondage dans le sujet intitulé '%s'",
'add_another_choice' => "Ajouter un autre choix",
'add_another_question' => "Ajouter une autre question",
'add_poll_choice' => "Ajouter un choix",
'already_sub' => "Vous suivez actuellement ce sujet.",
'append_edit' => "<strong>Ajouter</strong> la ligne 'Modifié par' dans ce message ?",
'attach_button' => "Ajouter dans le message",
'attach_button_title' => "Ajoute une balise pour le fichier joint dans le message",
'attach_delete' => "Supprimer",
'attach_delete_title' => "Supprimer ce fichier joint",
'attach_files_help' => "Aide pour les fichiers joints",
'attach_header' => "Joindre des fichiers",
'attach_selected' => "Joindre ce fichier",
'button_preview' => "Prévisualiser",
'clear_selection' => "Effacer la sélection",
'close_poll_form' => "Terminé",
'CODE_ERROR' => "Le code de vérification que vous avez saisi est incorrect",
'domain_not_allowed' => "Vous avez entré un lien vers un site internet interdit par l'administrateur.",
'edited_poll' => "Édition du sondage",
'edited_topic_title' => "Modification du titre ou de la description du sujet de '%s' en '%s' via le formulaire de publication",
'editing_post' => "Modification d'un message ",
'edit_options' => "Options d'édition",
'emoticons_template_title' => "Émoticônes",
'enable_emo' => "<strong>Activer</strong> les émoticônes ?",
'enable_sig' => "<strong>Inclure</strong> ma signature ?",
'enable_track' => "<strong>Suivre</strong> ce sujet ?",
'err_reg_code' => "Le code généré ne correspond pas à celui affiché, un nouveau code a été généré, veuillez re-essayer.",
'flash_not_allowed' => "Les vidéos en Flash ne sont pas autorisées",
'flash_too_big' => "Merci de réduire la taille de l'animation Flash que vous souhaitez envoyer.",
'FORUM_LOCKED' => "Ce forum est 'en lecture seule' et ne permet pas de poster de nouveaux sujets ou messages.",
'guest_captcha' => "Code de confirmation",
'guest_name' => "Saisissez votre nom",
'ignore_first_line' => "Vous avez choisi d'ignorer tous les messages de :",
'invalid_ext' => "Vous n'êtes pas autorisé à utiliser cette extension d'image sur cette communauté.",
'invalid_mime_type' => "L'envoi a échoué. Vous n'êtes pas autorisé à envoyer de fichier avec ce type d'extension de fichier.",
'mob_add_tag' => "Ajouter",
'mob_edit_tags' => "Éditer les étiquettes du sujet",
'mob_remove_tag' => "Supprimer la sélection",
'mob_tags' => "Étiquettes",
'moderate_post' => "Votre message sera examiné par un modérateur avant d'être ajouté à ce sujet.",
'moderate_topic' => "Votre nouveau sujet sera examiné par un modérateur avant d'être ajouté à ce forum.",
'moderator_options' => "Options de modération",
'mod_close' => "Fermer ce sujet",
'mod_close_time' => "Date de fermeture du sujet",
'mod_date_format' => "MM/JJ/AAAA",
'mod_move' => "Déplacer ce sujet",
'mod_nowt' => "Ne rien faire",
'mod_open' => "Ouvrir ce sujet",
'mod_open_time' => "Date d'ouverture du sujet",
'mod_options' => "Après ce message",
'mod_pin' => "Épingler ce sujet",
'mod_pinclose' => "Épingler & fermer ce sujet",
'mod_pinopen' => "Épingler & ouvrir ce sujet",
'mod_time_format' => "HH:MM",
'mod_unpin' => "Libérer ce sujet",
'mod_unpinclose' => "Libérer & fermer ce sujet",
'mod_unpinopen' => "Libérer & ouvrir ce sujet",
'NO_AUTHOR_SET' => "Nous n'avons pas pu déterminer qui a posté ce sujet.",
'NO_CONTENT' => "Vous devez saisir un message.",
'no_dynamic' => "Désolé, les pages dynamiques dans les tags [IMG] ne sont pas autorisées.",
'NO_EDIT_PERMS' => "Vous n'avez pas la permission d'éditer ce sujet.",
'NO_FORUM_ID' => "Nous n'avons pas pu déterminer le forum dans lequel vous avez tenté de poster.",
'no_permission_shared' => "Vous n'avez pas la permission de partager un ou plusieurs des éléments que vous avez essayé de partager.",
'no_post' => "Vous devez saisir un message",
'NO_POSTING_PPD' => "Vous ne pouvez plus ajouter de messages. Vous avez dépassé votre quota de %s messages par jour.",
'NO_POST_FORUM' => "Vous n'avez pas la permission de poster dans ce forum.",
'NO_PPD_DAYS' => "Ceci sera ré-initialisé %s",
'NO_PPD_POSTS' => "Ceci sera ré-initialisé lorsque vous aurez eu plus de %s messages acceptés",
'NO_REPLY_PERM' => "Vous n'avez pas la permission de répondre à ce sujet.",
'NO_REPLY_POLL' => "Ceci est uniquement un sondage et les réponses ne sont pas permises.",
'NO_START_PERM' => "Vous n'avez pas la permission de démarrer de nouveaux sujets dans ce forum.",
'NO_SUCH_FORUM' => "Il apparaît que le forum dans lequel vous avez tenté de poster n'existe pas.",
'NO_SUCH_TOPIC' => "Le sujet dans lequel vous tentez de poster n'a pas pu être trouvé. Il est possible que ce sujet ait été supprimé.",
'no_topic_title' => "Vous devez saisir un titre pour le sujet d'au moins deux caractères.",
'NO_USER_SET' => "Vous devez vous identifier pour poster dans ce sujet.",
'options' => "Options",
'picons_none' => "[ Aucun ]",
'poll_add_link' => "Ajouter un sondage",
'poll_fs_public' => "Rendre les votes publics ?",
'poll_fs_title' => "Titre du sondage",
'poll_manager' => "Gestion du sondage",
'poll_manage_link' => "Gérer le sondage de ce sujet",
'poll_multichoice' => "Permettre les réponses multiples ?",
'poll_only_desc' => "Interdire les réponses et autoriser uniquement les votes ?",
'poll_only_title' => "Faire un sondage uniquement",
'poll_public_warning' => "Si coché, les votes des membres seront rendus publics. <strong>Cela ne peut plus être modifié une fois qu'un vote a eu lieu !</strong>",
'poll_to_many' => "Vous avez plus de réponses possibles que l'administrateur ne le permet pour ce forum. Veuillez en enlever.",
'poll_votes_desc' => "Le nombre de votes que ce choix a (ou démarre avec, s'il s'agit d'un nouveau sondage)",
'post' => "Message",
'posting_new_topic' => "Écriture d'un nouveau sujet",
'post_edited' => "Le message a été modifié",
'post_icon' => "Les icônes de message",
'post_optional' => "(Facultatif)",
'post_options' => "Options du message",
'post_poll' => "Sondage",
'post_preview' => "Prévisualisation du message",
'pp_html1' => "Le HTML est activé - Retour à la ligne manuel",
'pp_html2' => "Le HTML est activé - Retour à la ligne auto",
'pp_htmlstatus' => "Affichage HTML",
'pp_nohtml' => "Le HTML est désactivé",
'preason_for_edit' => "Raison d'édition :",
'question_title' => "Question",
'quote_mismatch' => "Le nombre de balises de citation ouvrantes ne correspond avec le nombre de balises de citation fermantes.",
'reason_for_edit' => "Raison d'édition ",
'REG_CODE_ENTER' => "L'administrateur exige que vous saisissiez un code de vérification afin d'envoyer votre message.",
'remove_choice' => "Supprimer le choix",
'remove_question' => "Supprimer la question",
'replying_in' => "Réponse dans",
'review_topic' => "Voir le sujet complet (ouvre une nouvelle fenêtre)",
'select_several' => "Vous pouvez sélectionner plus d'un fichier à la fois en maintenant la touche Ctrl (sous Windows) ou Command (sous Mac) et en sélectionnant vos fichiers.",
'submit_edit' => "Envoyer le message modifié",
'submit_new' => "Envoyer ce nouveau sujet",
'submit_reply' => "Ajouter ma réponse",
'summary_posted' => "Posté",
'switch_to_advanced' => "Essayez notre uploadeur avancé (nécessite Flash 9)",
'switch_to_basic' => "Essayez notre uploadeur classique",
'too_few_tags' => "Vous devez soumettre au moins %s étiquette(s)",
'too_many_emoticons' => "Vous avez écrit un message avec plus d'émoticônes que cette communauté le permet. Merci de réduire le nombre d'émoticônes dans votre message.",
'too_many_img' => "Désolé, mais avez utilisé plus d'images que vous n'y êtes autorisé.",
'too_many_media' => "Désolé, mais vous avez posté plus de fichiers média que vous êtes autorisés à le faire.",
'too_many_quotes' => "Vous avez dépassé le nombre maximum de citations autorisé.",
'too_many_tags' => "Vous ne pouvez pas soumettre plus de %s étiquette(s)",
'topic' => "Sujet",
'topic_desc' => "La description du sujet",
'topic_information' => "Informations sur le sujet",
'TOPIC_LOCKED' => "Vous n'avez pas la permission de répondre dans les sujets fermés.",
'topic_poll_title' => "Sondage",
'topic_summary' => "Résumé du sujet",
'topic_tags' => "Étiquettes du sujet",
'topic_title' => "Le titre du sujet",
'topic_title_long' => "Le titre du sujet que vous avez saisi est trop long.",
'top_txt_edit' => "Modification d'un message dans",
'top_txt_new' => "Écriture d'un nouveau sujet dans ",
'top_txt_reply' => "En réponse à",
'trouble_uploading' => "Problème d'envoi ?",
'upload_failed' => "L'envoi du fichier a échoué parce que les autorisations convenables n'ont pas été positionnées sur le répertoire 'uploads'. Veuillez contacter l'administrateur de la communauté pour lui signaler cette erreur.",
'upload_title' => "Pièces jointes",
'used_space' => "<strong>%s</strong> utilisé sur <strong>%s</strong> de votre quota d'envoi global (Taille maximale d'un fichier : <strong>%s</strong>)",
'used_space_js' => "<strong>[used]</strong> utilisé sur <strong>[total]</strong> de votre quota d'envoi global (Taille maximale d'un fichier : <strong>%s</strong>)",
 ); 
