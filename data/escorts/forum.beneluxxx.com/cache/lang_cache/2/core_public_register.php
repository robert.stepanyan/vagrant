<?php

/*******************************************************
NOTE: This is a cache file generated by IP.Board on Fri, 22 Jun 2012 08:29:05 +0000 by admin_forum_bl
Do not translate this file as you will lose your translations next time you edit via the ACP
Please translate via the ACP
*******************************************************/



$lang = array( 
'activation_form' => "Formulaire d'activation",
'agree_submit' => "J'ai lu, compris et j'accepte ces termes et conditions",
'agree_to_tos' => "J'ai lu et j'accepte les",
'already_account' => "Si vous avez déjà un compte, vous pouvez vous rendre directement sur la",
'already_have_account' => "Avez-vous déjà un compte pour le forum %s ?",
'auth_text' => "Votre inscription a été envoyée.<br /><br />L'administrateur de la communauté a choisi de demander une validation pour toutes les adresses de courriel. Dans les 10 prochaines minutes (normalement instantanément) vous recevrez un courriel avec les instructions sur la prochaine étape. Ne vous inquiétez pas, cela ne prendra pas longtemps avant que vous puissiez écrire !<br /><br />Le courriel a été envoyé à",
'auto_dst_detection' => "Activer la détection automatique du <acronym title='Daylight Saving Time'>DST</acronym> ?",
'cf_optional' => "Information optionnelle",
'char_cant_use' => "C'est le nom que vous utiliserez pour vous connecter. Vous ne pouvez pas utiliser : <em>[ ] | ; , &#036; &#92; &lt; &gt; &quot;",
'clogin_done' => "Vous êtes maintenant connecté.",
'clogin_submit' => "Continuer...",
'clogin_text' => "C'est la première fois que vous vous connectez, nous avons juste besoin de quelques informations supplémentaires avant d'aller plus loin.",
'clogin_title' => "Bienvenue dans notre communauté",
'complete_form' => "Merci de compléter le formulaire entièrement",
'confirm_over_thirteen' => "Afin de nous conformer à la loi COPPA protégeant les mineurs en ligne, nous devons vérifier que vous avez plus de 13 ans. Si vous avez moins de 13 ans, nous avons besoin d'une <a href='<#FORM_LINK#>'>permission</a> de vos parents avant que vous puissiez utiliser ce site web.",
'connect_account_desc' => "Si vous <em>avez</em> déjà un compte sur ce forum, renseignez cette section :",
'connect_account_submit' => "Relier les comptes",
'connect_account_title' => "Relier à un compte existant sur ce forum",
'connect_already_linked' => "Le compte %s du forum est déjà relié à un utilisateur %s.",
'connect_desc' => "Vous pouvez soit créer un nouveau <strong>compte sur cette communauté</strong>, soit relier votre compte %s à votre compte existant sur la communauté.",
'connect_incorrect_details' => "Adresse de courriel ou mot de passe incorrect",
'connect_password' => "Mot de passe",
'connect_password_desc' => "Entrer le mot de passe que vous utilisez <em>actuellement</em> pour vous connecter à cette communauté",
'connect_sub' => "<strong>%s</strong>, vous y êtes presque !",
'connect_sub_desc' => "Comme vous vous êtes connecté via <strong>%s</strong> pour la première fois, nous avons juste besoin de quelques détails supplémentaires avant de pouvoir poursuivre.",
'connect_unknown' => "Une erreur inconnue s'est produite. Veuillez continuer votre connexion avec un nouveau compte et contactez un administrateur pour qu'il fusionne vos comptes",
'connect_username' => "Entrez l'adresse de courriel de votre compte sur cette communauté",
'connect_username_desc' => "Entrer l'adresse de courriel que vous utilisez <em>actuellement</em> pour vous connecter à cette communauté",
'coppa_additional_info' => "Information COPPA",
'coppa_admin_reminder' => "Pense-bête Admin",
'coppa_admin_remtext' => "Ne pas oublier de créer le compte une fois ce formulaire de consentement reçu et notifier l'utilisateur que son compte est prêt à être utilisé.",
'coppa_bounce' => "Vous devez soit annuler votre enregistrement, soit obtenir de vos parents un formulaire d'enregistrement COPPA complété et nous l'envoyer.",
'coppa_clickhere' => "Cliquer ici pour imprimer le formulaire de consentement parental",
'coppa_continue_button' => "J'ai 13 ans ou plus",
'coppa_email_subject' => "Information d'inscription COPPA",
'coppa_form' => "Formulaire COPPA",
'coppa_form_fill' => "Vous devez saisir votre date de naissance entièrement.",
'coppa_form_text' => "Un parent ou tuteur doit remplir et envoyer ou faxer un <#FORM_LINK#> signé à l'administrateur de la communauté afin qu'un enfant âgé de moins de 13 ans puisse terminer le processus d'inscription.<br />Pour plus d'informations, veuillez contacter l'administrateur de la communauté",
'coppa_info' => "Vérification de l'âge",
'coppa_link' => "Veuillez sélectionner votre de date de naissance",
'coppa_link_form' => "Formulaire d'autorisation",
'coppa_title' => "Inscription COPPA",
'coppa_under_thirteen' => "J'ai moins de 13 ans",
'cp2_text' => " Tous les utilisateurs agés de moins de 13 ans doivent obtenir la permission d'un parent ou d'un tuteur avant de pouvoir participer à ces forums.<br /><br />Une fois le formulaire de consentement parental reçu, un compte sera créé pour vous avec les informations fournies et un courriel vous sera envoyé à l'adresse fournie pour vous notifier que votre compte est prêt à être utilisé.  Une fois identifié, vous pourrez alors changer les détails de votre compte depuis l'espace 'Mes contrôles'.",
'cp2_title' => "Notre politique d'inscription COPPA",
'cpf_address' => "<strong>Adresse postale :</strong>",
'cpf_date' => "Date",
'cpf_email' => "Adresse de courriel",
'cpf_fax' => "<strong>Numéro de fax :</strong>",
'cpf_name' => "Nom et prénoms du parent/tuteur légal",
'cpf_perm_parent' => "Instructions pour le parent ou le tuteur<br /><br />Veuillez imprimer ce formulaire, complétez-le et faxez-le au numéro spécifié (si présent) ou envoyez-le à l'adresse postale ci-dessous.",
'cpf_phone' => "Numéro de téléphone",
'cpf_relation' => "Lien avec l'enfant",
'cpf_sign' => "Veuillez signer le formulaire ci-dessous et nous l'envoyer.<br /><br />Je comprends que je dois consentir à la collecte d'informations nécessaires sur mon enfant sans permettre au site de divulguer ces informations à une tierce partie. Je comprends aussi que ce site ne collectera pas d'information supplémentaire sur mon enfant qu'il n'est raisonnablement nécessaire pour participer à ce site.<br /><br />
									Je confirme que les informations que l'enfant a renseigné sont correctes. Je comprends que les informations du profil peuvent être changées en entrant un mot de passe et je comprends que je peux demander que ce profil d'inscription soit supprimé. Je comprends que je peux contacter le propriétaire de ce site à n'importe quel moment pour connaître quelles informations à propos de mon enfant ont été collectées, pour confirmer comment ces informations sont utilisées et pour demander que le site ne collecte plus d'information sur mon enfant.",
'cpf_signature' => "Signature",
'cpf_title' => "Formulaire d'autorisation COPPA",
'cp_success' => "votre inscription est réussie ! Nous vous redirigeons vers le formulaire d'autorisation.",
'dname_desc' => "Entre 3 et %s caractères",
'dname_name' => "Nom d'affichage",
'dumb_header' => "Activation du compte",
'dumb_submit' => "Envoyer",
'dumb_text' => "Merci de vérifier que vous avez renseigné le formulaire entièrement. Les informations nécessaires sont dans le courriel qui vient de vous être envoyé.",
'email_address' => "Saisissez votre adresse de courriel",
'email_address_confirm' => "Confirmez l'adresse de courriel",
'email_reg_subj' => "Inscription à",
'email_validate_text' => "Vous recevrez un courriel à l'adresse que vous avez donné après l'inscription. Merci de lire le courriel attentivement, vous aurez besoin de valider votre compte en cliquant sur un lien dans ce dernier.",
'errors_found' => "Les erreurs suivantes ont été trouvées",
'err_cf_to_long' => "Vous avez saisi plus de caractères que ce qui est autorisé pour ce champ.",
'err_complete_form' => "Ce champ est requis et doit être complété.",
'err_invalid' => "Les données que vous avez saisi dans ce champ sont dans un format incorrect.",
'err_invalid_email' => "L'adresse de courriel entrée n'est pas une adresse de courriel valide.",
'err_no_username' => "Vous devez saisir un nom d'utilisateur valide compris entre 3 et 32 caractères.",
'err_no_validations' => "Il n'y a pas de demande de validation en cours pour ce membre. Veuillez vérifier le nom et essayer à nouveau.",
'err_q_and_a' => "Votre réponse n'était pas valide. Veuillez réessayer.",
'err_reg_code' => "Le code d'enregistrement ne correspond pas à celui affiché, un nouveau code a été généré. Veuillez essayer à nouveau.",
'fb_complete_reg' => "Terminer l'inscription maintenant",
'fb_logged_in' => "Vous êtes connecté à Facebook",
'fb_login_title' => "Connexion Facebook",
'fb_reg_with' => "Vous pouvez vous enregistrer ici en utilisant votre compte Facebook !",
'fb_welcome' => "Bienvenue,",
'following_errors' => "Les erreurs suivantes ont été trouvées :",
'forum_terms' => "Termes et règles d'utilisation du forum.",
'goto_signin' => "Se connecter",
'just_admin_validation' => "Après l'enregistrement, votre compte sera ajouté à la file d'attente d'administration pour être vérifié et validé.",
'login_w_facebook' => "Connectez-vous avec votre compte Facebook",
'login_w_twitter' => "Connectez-vous avec votre compte Twitter",
'log_with_facebook' => "Se connecter avec facebook",
'lost_pass_form' => "Vous avez perdu votre mot de passe ?",
'lost_your_password' => "Si vous avez perdu votre mot de passe, vous pouvez utiliser ce formulaire pour le ré-initialiser.",
'lpass_text' => "Un courriel contenant de plus amples instructions d'activation a été envoyé. Ce courriel devrait vous arriver dans les 10 prochaines minutes (normalement instantanément).",
'lpass_text_random' => "Un courriel contenant votre nouveau mot de passe a été envoyé à l'adresse renseignée dans votre compte.",
'lpf_pass1' => "Saisissez un nouveau mot de passe",
'lpf_pass11' => "Il ne doit pas dépasser 32 caractères et pas faire moins de trois caractères",
'lpf_pass2' => "Ré-entrez votre nouveau mot de passe",
'lpf_pass22' => "Il doit correspondre exactement",
'lpf_title' => "Récupération de mot de passe perdu",
'lp_email_or' => "<em>-OU-</em> adresse de courriel",
'lp_random_pass' => "Cliquez sur le bouton Envoyer ci-dessous pour qu'un nouveau mot de passe vous soit envoyé par courriel à l'adresse renseignée dans votre compte.",
'lp_random_pass_subject' => "Votre nouveau mot de passe",
'lp_send' => "Envoyer",
'lp_subject' => "Information de récupération du mot de passe",
'lp_text' => "Saisissez votre identifiant ou votre adresse de courriel dans le champ ci-dessous. L'identifiant est <strong>in</strong>sensible à la casse.<br />Une fois le formulaire envoyé, vous recevrez un courriel demandant la validation de cette requête pour s'assurer qu'aucune tentative d'abus n'a eu lieu. Ce courriel contiendra également un lien sur lequel vous devrez cliquer pour avoir plus d'instructions.",
'lp_user_name' => "Saisissez votre pseudo",
'mobile_agree_to_tos' => "J'ACCEPTE",
'mobile_cant_register' => "Nous ne pouvons malheureusement pas offrir l'inscription sur les mobiles pour le moment. Veuillez créer un compte sur un autre équipement, vous serez alors en mesure de vous connecter ici.",
'mobile_cant_register_link' => "Nous sommes dans l'incapacité de vous inscrire via l'interface mobile pour le moment. Cliquez sur le bouton ci-dessous pour afficher la version complète de la communauté et vous inscrire.",
'must_agree_to_terms' => "Vous devez accepter les termes de l'inscription.",
'nav_reg' => "Inscription",
'need_to_create_acc' => "J'ai besoin de créer un <strong>NOUVEAU</strong> compte sur la communauté",
'new_account_desc' => "Si vous <em>n'avez pas</em> de compte sur ce forum, renseignez cette section :",
'new_account_submit' => "Créer un nouveau compte",
'new_account_title' => "Créer un nouveau compte sur ce forum",
'new_password' => "Nouveau mot de passe",
'new_registration_email' => "Inscription à ",
'new_registration_email1' => "Nouvelle inscription à ",
'new_registration_email_spammer' => "Un spammeur s'est peut-être inscrit sur ",
'ne_subject' => "Demande de changement d'adresse de courriel à ",
'passwords_not_match' => "Le contenu des champs mot de passe n'est pas identique.",
'preview_reg_text' => "Votre inscription a été effectuée avec succès. L'administrateur souhaite examiner tous les nouveaux comptes avant d'accorder la permission d'écrire. L'administrateur a été averti de votre inscription.",
'qa_question_desc' => "Ceci aide à nous protéger des spammeurs.",
'qa_question_title' => "Votre question",
'qa_title' => "Question",
'ready_register' => "Rejoindre notre communauté",
'receive_admin_emails' => "Recevoir les courriels en provenance des administrateurs",
'recover_password' => "Récupérer mon mot de passe",
'register' => "Créer un compte",
'registration_form' => "Formulaire d'inscription",
'registration_process' => "Processus d'inscription",
'regstep' => "Étape %s",
'regstep_choose_package' => "Choisir un forfait",
'regstep_confirm' => "Confirmation",
'regstep_your_account' => "Votre compte",
'reg_choose_dname' => "Nom d'utilisateur",
'reg_choose_password' => "Mot de passe",
'reg_choose_password_desc' => "Entre 3 et 32 caractères",
'reg_choose_user' => "Nom d'utilisateur",
'reg_enter_email' => "Adresse de courriel",
'reg_enter_email_desc' => "De sorte que nous puissions vérifier votre identité et vous tenir informé.",
'reg_errors_found' => "Des erreurs ont été trouvées dans votre inscription. Veuillez corriger les champs surlignés ci-dessous.",
'reg_error_chars' => "Le nom d'affichage contient un ou plusieurs de ces caractères illégaux : [ ] | ; ,  &#036; &#92; &lt; &gt; &quot;",
'reg_error_email_ban' => "L'adresse de courriel que vous utilisez n'est pas acceptée sur cette communauté. Merci d'en spécifier une autre.",
'reg_error_email_invalid' => "L'adresse de courriel que vous avez saisie est invalide (ex : nom@domaine.fr)<br /> ou contient des caractères illégaux : [ ] ; # &amp; ! * ' &quot; &lt; &gt; % ( ) { } ? &#092;",
'reg_error_email_nm' => "Les adresses de courriel ne sont pas identiques",
'reg_error_email_taken' => "L'adresse de courriel est déjà utilisée",
'reg_error_no_name' => "Vous devez saisir un nom compris entre 3 et %s caractères.",
'reg_error_no_name_spec' => " Sachez que certains caractères spéciaux utilisent plus d'un caractère.",
'reg_error_username_none' => "Le nom d'utilisateur doit être compris entre 3 et %s caractères",
'reg_error_username_taken' => "Le nom d'utilisateur est déjà pris",
'reg_free' => "C'est gratuit et simple de s'inscrire sur notre communauté ! Vous avez juste besoin de quelques informations vous concernant, et vous serez prêt pour poster votre premier message en un rien de temps !",
'reg_reenter_email' => "Confirmer votre adresse de courriel",
'reg_reenter_password' => "Confirmer le mot de passe",
'reg_step1' => "Informations du compte",
'reg_step2' => "Options de la communauté",
'reg_step3' => "informations additionnelles",
'reg_step3_spam' => "Vérification que vous êtes un humain",
'reg_step4_spam' => "Vérification que vous êtes un humain",
'reg_success' => "Inscription terminée",
'reg_terms_popup_title' => "Termes d'inscription",
'reg_with_facebook' => "S'inscrire en utilisant Facebook",
'required_field' => "Information nécessaire",
'rv_done' => "<strong>Votre demande de re-validation est réussie</strong><br />Un courriel de re-validation a été envoyé à l'adresse de courriel enregistrée pour ce membre. Veuillez vérifier vos courriels dans les prochaines minutes (y compris les dossiers 'junk mail' ou 'spam' que vous pourriez avoir).",
'rv_go' => "Rechercher et envoyer",
'rv_ins' => "Veuillez saisir ci-dessous votre nom d'inscription pour rechercher toutes demandes de validation en cours. Si au moins une est trouvée, le courriel sera ré-envoyé à l'adresse de courriel avec laquelle vous vous êtes inscrit.",
'rv_ins_both' => "Veuillez entrer ci-dessous votre nom ou courriel de connexion pour chercher une demande de validation en attente. En cas de succès, un courriel sera renvoyé à l'adresse utilisée lors de votre inscription.",
'rv_ins_email' => "Veuillez entrer ci-dessous votre courriel de connexion pour chercher une demande de validation en attente. En cas de succès, un courriel sera renvoyé à l'adresse utilisée lors de votre inscription.",
'rv_ins_name' => "Veuillez entrer ci-dessous votre nom de connexion pour chercher une demande de validation en attente. En cas de succès, un courriel sera renvoyé à l'adresse utilisée lors de votre inscription.",
'rv_name' => "Nom de membre enregistré",
'rv_process' => "Processus de re-validation",
'rv_title' => "Envoyer à nouveau le courriel de validation",
'select_day' => "Jour",
'select_month' => "Mois",
'select_year' => "Année",
'sign_in_page' => "page de connexion",
'std_text' => "Merci de vérifier que vous avez renseigné tous les champs, plus particulièrement les champs mot de passe.",
'tc_regbut' => "Inscription",
'tc_text' => "Pour pouvoir continuer, vous devez être d'accord avec ceci :",
'tc_title' => "Les termes de l'inscription et le règlement",
'terms_of_use' => "termes d'utilisation",
'thank_you' => "Merci",
'time_zone' => "Fuseau horaire",
'twitter_login_title' => "Connexion Twitter",
'user_admin_validation' => "Aprés la validation correcte de votre compte, celui-ci sera ajouté à la file d'attente d'administration pour être vérifié et approuvé.",
'user_id' => "ID de l'utilisateur :",
'user_name' => "Entrez le nom d'utilisateur souhaité",
'use_an_existing_acc' => "Je veux me connecter à un compte <strong>EXISTANT</strong> de la communauté",
'validation_complete' => "Validation complète",
'val_key' => "Clé de validation :",
'want_to_save_time' => "Envie de gagner du temps ?",
'why_register' => "Pourquoi s'inscrire ?",
 ); 
