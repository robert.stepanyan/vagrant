Cubix.BitcoinInfo = {};

Cubix.BitcoinInfo.inProcess = false;

Cubix.BitcoinInfo.url = '';

Cubix.BitcoinInfo.Show = function () {
	if ( Cubix.BitcoinInfo.inProcess ) return false;
	
	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', z_index: 200 });
	page_overlay.disable();


	$$('#bitcoin-payment-info-wrapper').addClass('open');
	
	Cubix.BitcoinInfo.inProcess = true;

	$$('#bitcoin-payment-info-button').addEvent('click', function() {
		$$('#bitcoin-payment-info-wrapper').destroy();
		page_overlay.enable();
	});
	
	return false;
}
