Cubix.Follow = {};

Cubix.Follow.lng = ''; // Must be set from php

Cubix.Follow.popupInProcess = false;
Cubix.Follow.popupEditMode = false;

Cubix.Follow.page_overlay = '';


Cubix.Follow.Init = function () {

	var input = $('member-search');

	new Autocompleter.Request.JSON(input, '/follow/ajax-member-search ?>', {
		'minLength': 1,
		postVar: 'member_name',
		indicatorClass: 'autocompleter-loading'

	}).addEvent('onSelection', function (element, selected, value, input) {
		$$('.add-member-form #notif').removeClass('error').hide();	
		$('member-search-id').set('value', selected.inputValue.id);
	});

	// Follow member
	$$('.add_member_btn').addEvent('click', function (e) {
		Cubix.Follow.Follow(e);
	});

	// Follow search member
	$('follow_search').addEvent('keydown:keys(enter)', function (e) {
		e.stop();

		active_type = $$('.follow_tabs_container .active')[0].get('id');
		Cubix.Follow.GetFollowList(active_type,1);
	});


	// Follow member tab
	$$('.follow_tabs_container .follow_tab').addEvent('click', function (e) {
		e.stop();

		if($(this).hasClass('active')){
			return false;
		}
		
		$$('.follow_tabs_container .follow_tab').removeClass('active');
		$(this).addClass('active');

		type = $(this).get('id');


		Cubix.Follow.GetFollowList(type,1);

	});

	Cubix.Follow.GetFollowList('followed',1);
	
	return false;
}

Cubix.Follow.GetFollowList = function (type, page) {
	var overlay = new Cubix.Overlay($('followers_container'), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	overlay.disable();

	search_name = $('follow_search').value;

	var req = new Request({
		method: 'get',
		url: '/follow/ajax-follow-list?type='+type+'&page='+page+'&search='+search_name,
		onComplete: function(response) {
			$('followers_container').set('html',response);
			overlay.enable();
		}
	}).send();
}

Cubix.Follow.Follow = function (e){
	e.stop();

	$$('.add-member-form #notif').removeClass('error').hide();
	var member_id = $('member-search-id').value.toInt();

	if(!member_id){
		var member_id = $$('.FollowPopup-wrapper #member-search-id')[0].value.toInt();
	}

	if(!member_id){
		$$('.add-member-form #notif')[0].addClass('error').set('html',followWords.fm_select_member).setStyle('display', 'inline-block');
		return false;
	}

	var events = [];

	if(!Cubix.Follow.popupInProcess){
		$$(".inform_about_container input[type=checkbox]:checked").each(function(el)
		{
			events.push(el.get('id'));  
		});
	}else{
		$$(".inform_about_container select").each(function(el)
		{
			if(el.getSelected().get('value') == 1){
				events.push(el.get('id')); 	
			}
		});	
	}

	new Request({
		url: '/follow/follow/',
		method: 'post',
		evalScripts: true,
		data: ({ followed_id: member_id, events: events, edit_mode: Cubix.Follow.popupEditMode }),
		onSuccess: function (resp) {
			response = JSON.decode(resp);

			if(response.status == 1){
				$$('.add-member-form #notif')[0].addClass('notif').set('html',followWords.fm_follow_success).setStyle('display', 'inline-block');
				$$('.inform_about_container input').each(function(el) { el.checked = false; });

				if(!Cubix.Follow.popupInProcess){
					$('member-search').set('value','');
					$('member-search-id').set('value','');


					if(!$('followed').hasClass('active')){
						$$('.follow_tabs_container .follow_tab').removeClass('active');	
						$('followed').addClass('active');	
					}
					$('follow_search').set('value','');
					Cubix.Follow.GetFollowList('followed',1);
				}else{
					if(!Cubix.Follow.popupEditMode){

						if($$('.follow-count').length > 0){
							$$('.follow-count .icon').destroy();
							//$$('.follow-count .count')[0].set('html',parseInt($$('.follow-count .count')[0].get('html')) + 1);
						}
					}
					Cubix.Follow.destroyPopup();
				}

			}else if(response.status == 0){
				$$('.add-member-form #notif')[0].addClass('error').set('html',followWords[response.error]).setStyle('display', 'inline-block');
			}
		}.bind(this)
	}).send();
}

Cubix.Follow.destroyPopup = function(){
	$$('.FollowPopup-wrapper').destroy();
	Cubix.Follow.page_overlay.enable();
	Cubix.Follow.popupInProcess = false;
}

Cubix.Follow.unFollow = function (f_id) {
	var overlay = new Cubix.Overlay($('followers_container'), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	overlay.disable();

	var req = new Request({
		method: 'get',
		url: '/follow/un-follow?follow_id='+f_id,
		onComplete: function(response) {			
			response = JSON.decode(response);

			if(response.status == 1){
				$$('div[f_id*='+f_id+']').dispose();
			}

			overlay.enable();
		}
	}).send();
}

Cubix.Follow.removeMyFollower = function (f_id) {
	var overlay = new Cubix.Overlay($('followers_container'), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	overlay.disable();

	var req = new Request({
		method: 'get',
		url: '/follow/remove-my-follower?follow_id='+f_id,
		onComplete: function(response) {			
			response = JSON.decode(response);

			if(response.status == 1){
				$$('div[f_id*='+f_id+']').dispose();
			}

			overlay.enable();
		}
	}).send();
}

Cubix.Follow.approve = function (f_id) {
	var overlay = new Cubix.Overlay($('followers_container'), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	overlay.disable();

	var req = new Request({
		method: 'get',
		url: '/follow/approve?follow_id='+f_id,
		onComplete: function(response) {			
			response = JSON.decode(response);

			if(response.status == 1){
				$$('div[f_id*='+f_id+'] .fm-control-buttons').dispose();
			}

			Cubix.Follow.GetFollowList('follower',1);

			overlay.enable();
		}
	}).send();
}

Cubix.Follow.deny = function (f_id) {
	var overlay = new Cubix.Overlay($('followers_container'), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	overlay.disable();

	var req = new Request({
		method: 'get',
		url: '/follow/deny?follow_id='+f_id,
		onComplete: function(response) {			
			response = JSON.decode(response);

			if(response.status == 1){
				$$('div[f_id*='+f_id+']').dispose();
			}

			overlay.enable();
		}
	}).send();
}

Cubix.Follow.AlreadyFollow = function(){
	box_width = 350;
	box_height  = 150;

	Cubix.Follow.page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	Cubix.Follow.page_overlay.disable();

	var y_offset = 50;

	var container = new Element('div', { 'class': 'FollowPopup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		opacity: 0,
        position: 'absolute',
        'z-index': 101,
        background: '#fff',
        width: box_width,
        height: box_height,
	}).inject(document.body);

	

	new Request({
		url: '/follow/already-followed-popup',
		method: 'post',
		evalScripts: true,
		onSuccess: function (resp) {
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'popup-close-btn-v3'
			}).inject(container);

			close_btn.addEvent('click', function() {
				Cubix.Follow.destroyPopup();
			});

			container.tween('opacity', '1');
			
		}
	}).send();
	
}

Cubix.Follow.followPopup = function (followed_id, action) {
	if(!followed_id || Cubix.Follow.popupInProcess){
		return false;
	}

	
	if(action == 'edit'){
		Cubix.Follow.popupEditMode = true;
	}else{
		var canFollow = true;
		new Request({
			url: '/follow/check-followed',
			method: 'post',
			evalScripts: true,
			async: false,
			data: ({ followed_id: followed_id }),
			onSuccess: function (resp) {
				response = JSON.decode(resp);

				if(response.status == 1){
					canFollow = false;
					Cubix.Follow.AlreadyFollow();
				}

			}
		}).send();

		if(!canFollow){
			return false;
		}
	}

	box_width = 350;
	box_height  = 400;

	Cubix.Follow.page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	Cubix.Follow.page_overlay.disable();

	
	var y_offset = 30;

	var container = new Element('div', { 'class': 'FollowPopup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset+130,
		opacity: 0,
        position: 'absolute',
        'z-index': 101,
        background: '#fff',
        width: box_width,
        height: box_height,
	}).inject(document.body);
	
	Cubix.Follow.popupInProcess = true;

	new Request({
		url: '/follow/follow-popup',
		method: 'post',
		evalScripts: true,
		data: ({ followed_id: followed_id, edit_mode: Cubix.Follow.popupEditMode }),
		onSuccess: function (resp) {
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'popup-close-btn-v3'
			}).inject(container);

			$$('.add_member_cancel')[0].addEvent('click', function(e) {
				e.stop();
				Cubix.Follow.destroyPopup();
				Cubix.Follow.popupInProcess = false;
				return false;
			});

			close_btn.addEvent('click', function() {
				Cubix.Follow.destroyPopup();
				Cubix.Follow.popupInProcess = false;
			});


			if(Cubix.Follow.popupEditMode){
				$$('.FollowPopup-wrapper .add_member_btn')[0].set('value',followWords.save);
			}

			$$('.FollowPopup-wrapper .add_member_btn').addEvent('click', function (e) {
				Cubix.Follow.Follow(e);
			});

			container.tween('opacity', '1');
		}
	}).send();
		
	
	return false;
}


window.addEvent('domready', function () {
	var input = $('member-search');
	if(input){
		Cubix.Follow.Init();
	}
});
