/* --> Escort Photo Roller */
Cubix.PhotoRoller = {};

Cubix.PhotoRoller.prefix = 'http://';
Cubix.PhotoRoller.thumb = 'thumb';

Cubix.PhotoRoller.Clear = function () {
	$$('.escorts .image a').each(function (el) {
		el.eliminate('photo-roller');
		
		var img = el.getElement('img');
		img.eliminate('photo');
		img.eliminate('current');
		
		img.removeEvents('mouseenter');
		img.removeEvents('mouseleave');
	});
};

Cubix.PhotoRoller.Init = function () {
	
	$$('.escorts .image a').each(function (el) {
		
		if ( el.retrieve('photo-roller-' + Cubix.PhotoRoller.thumb) ) return;
		el.store('photo-roller-' + Cubix.PhotoRoller.thumb, true);
		
		var img = el.getElement('img');
		
		if ( ! img.get('id') ) return;
		var e_id = img.get('id').split('-')[1];
		
		if ( ! img.retrieve('photo-' + Cubix.PhotoRoller.thumb) ) img.store('photo-' + Cubix.PhotoRoller.thumb, img.get('src'));

		img.photos = null;
		img.timeoutId;

		img.removeEvents('mouseenter');		
		img.addEvent('mouseenter', function () {
			var selff = this;

			
	       img.timeoutId = setTimeout(function(){
	       		var path = window.location.pathname;
				var category = "home";
				if ( path.test('city_') ) {
					category = 'citypage';
				} else if ( path.test('tours') ) {
					category = 'tourpage';
				}

		       //console.log(category, 'mouseover_thumb', selff.get('el-position'));
		       _gaq.push(['_trackEvent', category, 'mouseover_thumb', selff.get('el-position')]);
		    }, 3000);

			img.stop = false;
			if ( ! img.photos ) {
				new Request({
					url: '/index/escort-photos',
					method: 'get',
					data: { id: e_id },
					onSuccess: function (resp) {
						resp = JSON.decode(resp, true);
						if ( ! resp ) {
						}
						else if ( resp.result == 'error' ) {
							console.log(resp);
						}
						else if ( resp.result == 'success' ) {
							photos = resp.data;

							for ( var i = 0; i < photos.length; i++ ) {
								photos[i] = Cubix.PhotoRoller.GetSrc(e_id, photos[i]);
							}

							img.photos = photos;
							$clear(img.timer);
							img.timer = (function () {Cubix.PhotoRoller.Roll(img, img.photos);}).delay(1000);
						}
					}
				}).send();

				return;
			}

			$clear(img.timer);
			img.timer = (function () {Cubix.PhotoRoller.Roll(img, img.photos);}).delay(1000);
		});

		img.removeEvents('mouseleave');
		img.addEvent('mouseleave', function (e) {
			$clear(img.timer);
			img.stop = true;
			img.set('src', img.retrieve('photo-' + Cubix.PhotoRoller.thumb));
			
			clearTimeout(img.timeoutId);

			return;

			/*Cubix.PhotoRoller.Roll(img, [img.retrieve('photo')], true);*/
		});
	});
};

Cubix.PhotoRoller.Roll = function (img, photos, rollback) {
	rollback = $defined(rollback, false);
	
	var current = img.retrieve('current-' + Cubix.PhotoRoller.thumb);
	if ( ! current ) current = 2;
	current++;
	
	if ( current > photos.length ) current = 1;
	
	$clear(img.timer);
	var image = img.image = new Asset.image(photos[current - 1], {
		onload: function () {
			if ( img.stop ) return;
			img.set('src', photos[current - 1]);
			img.timer = (function () {Cubix.PhotoRoller.Roll(img, photos);}).delay(700);
			image.destroy();

			///image.setStyles({position: 'absolute', left: 0, top: 0, opacity: 0, margin: '1px' }).addClass('escort-thumb').inject(img.getParent('div'));
			// image.get('tween').start(1);
		}
	})/*.set('tween', {property: 'opacity', duration: 200, onComplete: function () {
		img.set('src', photos[current - 1]);

		if ( ! rollback ) {
			img.timer = (function () {Cubix.PhotoRoller.Roll(img, photos);}).delay(1000);
		}

		image.destroy();
		img.image = null;
	}})*/;
	
	img.store('current-' + Cubix.PhotoRoller.thumb, current);
};

Cubix.PhotoRoller.GetSrc = function (e_id, photo) {
	var prefix = Cubix.PhotoRoller.prefix, app = Cubix.PhotoRoller.app, url = '';
	url += prefix;

	if ( e_id.length > 2 ) {
		url += e_id.substring(0, 2) + '/';
		url += e_id.substring(2);
	}
	else {
		url += '_/';
		url += e_id
	}

	photo = photo.split('.');

	url += '/' + photo[0] + '_' + Cubix.PhotoRoller.thumb + '.' + photo[1];
	
	return url;
};
/* <-- */
