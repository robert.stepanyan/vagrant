var Lottery = {};

Lottery.run = function(args){
	console.log('WITH ONLOAD');
	if (args !== null && typeof args === 'object'){
		var cW = args.canvasWidth;
		var cH = args.canvasHeight;
		var cBg = args.bgImage;
		var cBskt = args.basketImage;
		var cEgg = args.eggImage;
		var cCrEgg1 = args.crackedEggImage1;
		var cCrEgg2 = args.crackedEggImage2;
		var cIcon = args.closeIcon;
		var lMsg = args.lotteryMessage;

		Lottery.Canvas.init(cW,cH);
		Lottery.Images.init([cIcon, cBg, cBskt, cEgg, cCrEgg1, cCrEgg2]);
		Lottery.Scenario.setMsg(lMsg);

	} else {
		console.log('missing arguments');
	}
};

Lottery.Canvas = {
	// lottery canvas properties
	element: null,
	width: 0,
	height: 0,
	overlay: null,

	// lottery canvas methods
	init: function(w,h){
		var canvas = document.getElementById('canvas');
		var x = new Cubix.Overlay(document.body, {color: '#edeff9', opacity: 0.8, position: '55%', has_loader: false});
		this.overlay = x;
		this.overlay.disable();
		if (!canvas) {
			this.setCanvas(w,h);
		} else {
			this.element = canvas;
		}
	},

	setCanvas: function(w,h){
		canvas = document.createElement('canvas');
		canvas.id = 'canvas';
		this.width = canvas.width = w;
		this.height = canvas.height = h;
		fallbackMsg = document.createTextNode('Your browser does not support canvas!');
		canvas.appendChild(fallbackMsg);
		this.element = document.body.appendChild(canvas);
	},

	getCanvas: function(){
		return this.element;
	}
};

Lottery.Images = {
	// lottery images class property
	sources: null,
	totalSrcs: 0,
	loadedSrcs: 0,
	imgObj: [],

	// lottery images class methods
	init: function(srcs){
		var self = this;
		this.setSources(srcs);

		var c = Lottery.Canvas.getCanvas();
		c.addEventListener('loaded', function(){
			Lottery.Scenario.start();
		});
		this.silentLoad();
	},

	setSources: function(srcs){
		this.sources = srcs;
	},

	silentLoad: function(){
		var sources = this.sources;
		var self = this;

		this.totalSrcs = sources.length;
		for (var i = sources.length - 1; i >= 0; i--) {
			this.loadToDom(sources[i]);
		}

		var loaded = setInterval(function(){
			var f = self.checkLoaded();
			if (f) {
				clearInterval(loaded);
				Lottery.Images.triggerEvent(Lottery.Canvas.getCanvas(),'loaded', document);
			}
		}, 1000);
	},

	loadToDom: function(src){
		var img = new Image();
		var self = this;

        img.src = src;
        this.imgObj.push(img);

        img.addEventListener("load", function(){
   //      	var r = new XMLHttpRequest();
			// r.open("GET", img.src, true);

			// r.onreadystatechange = function () {
			// 	if (r.readyState != 4 || r.status != 200) return;
				self.loadedSrcs += 1;
			// };
			// r.send();
			
    	});
	},

	checkLoaded: function(){
		return this.totoalSrcs = this.loadedSrcs;
	},

	triggerEvent: function (target, type, doc) {
		//doc = doc || document;
		// if (doc.createEvent) {
		 	//var event = new Event(type);
		 	var event = new CustomEvent(
				type, 
				{
					bubbles: true,
					cancelable: true
				}
			);
		 	target.dispatchEvent(event);
		// } else {
			// var event = doc.createEventObject();
    		//target.fireEvent(type);
		// }
	}
};

Lottery.Animations = {
	// lottery images class property
	ctx: null,
	globalAlpha: 0,

	// lottery images class methods
	defineCanvas: function(){
		var c  = Lottery.Canvas.getCanvas();
		var ctx = c.getContext('2d');
		this.ctx = ctx;
	},

	wrapText: function(context, text, x, y, maxWidth, lineHeight) {
        var words = text.split(' ');
        var line = '';

        for(var n = 0; n < words.length; n++) {
          var testLine = line + words[n] + ' ';
          var metrics = context.measureText(testLine);
          var testWidth = metrics.width;
          if (testWidth > maxWidth && n > 0) {
            context.fillText(line, x, y);
            line = words[n] + ' ';
            y += lineHeight;
          }
          else {
            line = testLine;
          }
        }
        context.fillText(line, x, y);
      },


	slideIn: function(el0, el){
		var self = this;

		var coordY = 300;
		var interval = window.setInterval(function(){
			if (coordY > 0){
				self.ctx.clearRect(0, 0, Lottery.Canvas.width, Lottery.Canvas.height );
				coordY -= 1;
				self.ctx.drawImage(el0, 0, 0);
				self.ctx.drawImage(el, 0, coordY);
			} else {
				clearInterval(interval);
				Lottery.Images.triggerEvent(Lottery.Canvas.getCanvas(),'basketLoaded', document);
			}
		}, 1);
	},

	slideFadeIn: function(el0, el1, el){
		var self = this;
		var coordY = 650;
		var alpha = 0;
		var interval = window.setInterval(function(){
			if (coordY > 150){
				self.ctx.clearRect(0, 0, Lottery.Canvas.width, Lottery.Canvas.height );
				coordY -= 2;
				alpha += 1/100;
				self.ctx.drawImage(el0, 0, 0);
				self.ctx.save(); //remove
				self.ctx.globalAlpha = alpha;
				self.ctx.drawImage(el, 550, coordY);
				self.ctx.restore();
				self.ctx.drawImage(el1, 0, 0);
			} else {
				clearInterval(interval);
				var cnt = 0;
				var shakeInterval = window.setInterval(function(){
					cnt++;
					self.ctx.clearRect(0, 0, Lottery.Canvas.width, Lottery.Canvas.height );
					// self.ctx.save();

					var dx = Math.random()*10;
		  			var dy = Math.random()*10;

					self.ctx.drawImage(el0, 0, 0);
					self.ctx.save(); //remove
		  			self.ctx.translate(dx,dy);
					self.ctx.drawImage(el, 550, 150);
					self.ctx.restore();
					self.ctx.drawImage(el1, 0, 0);
					if (cnt == 400) {
						clearInterval(shakeInterval);
						self.ctx.clearRect(0, 0, Lottery.Canvas.width, Lottery.Canvas.height );
						self.ctx.drawImage(el0, 0, 0);
						self.ctx.drawImage(el, 550, 150);
						self.ctx.drawImage(el1, 0, 0);
						Lottery.Images.triggerEvent(Lottery.Canvas.getCanvas(),'eggLoaded', document);
					}
				}, 1);
			}
		}, 1);
	},

	fadeIn: function(el){
		var self = this;

		var alpha = 0;
		var interval = window.setInterval(function(){
			if (alpha < 1){
				self.ctx.clearRect(0, 0, Lottery.Canvas.width, Lottery.Canvas.height );
				alpha += 0.01;
				self.ctx.globalAlpha = alpha;
				self.ctx.drawImage(el, 0, 0);
			} else {
				clearInterval(interval);
				Lottery.Images.triggerEvent(Lottery.Canvas.getCanvas(),'backgroundLoaded', document);
			}
		}, 1);
	},

	crack:  function(el, el0, el1, el2, el3){
		var self = this;
		var coordY1 = 150;
		var coordY2 = 250;

		var interval = window.setInterval(function(){
			if (coordY1 > 30){
				self.ctx.clearRect(0, 0, Lottery.Canvas.width, Lottery.Canvas.height );
				coordY1 -= 1.8;
				coordY2 += 0.5;
				self.ctx.save();
				self.ctx.drawImage(el, 0, 0);
				
				self.ctx.shadowColor = "#eee";
        		self.ctx.shadowBlur = 5;
        		self.ctx.shadowOffsetX = 0;
        		self.ctx.shadowOffsetY = 10;

				self.ctx.drawImage(el1, 553, coordY1);
				self.ctx.shadowOffsetY = -10;				
				self.ctx.drawImage(el2, 550, coordY2);
				self.ctx.restore();
				self.ctx.drawImage(el0, 0, 0);
				if (coordY1 < 80) {
					self.ctx.textAlign = 'center';
					self.ctx.fillStyle = '#005da9';
					self.ctx.font = 'italic bold 18px sans-serif';
					self.wrapText(self.ctx, Lottery.Scenario.lotteryMsg, Lottery.Canvas.width/2-2, 220, 500, 25);
  				}
			} else {
				clearInterval(interval);
				var alpha = 0;

				var interval = window.setInterval(function(){
					if (alpha < 1){
						alpha += 0.1;
						self.ctx.globalAlpha = alpha;
						self.ctx.drawImage(el3, Lottery.Canvas.width/2-20, 370);
					} else {
						var c = Lottery.Canvas.getCanvas();
						c.onclick = function(){
							console.log(this);
							document.body.removeChild(c);
							Lottery.Canvas.overlay.enable();
						};
						clearInterval(interval);
					}
				});
			}
		}, 1);
	}
};

Lottery.Scenario = {
	// lottery images class parameters
	lotteryMsg: '',

	// lottery images class methods
	start: function(){
		var self = this;
		Lottery.Animations.defineCanvas();
		this.backgroundFade();

		var c = Lottery.Canvas.getCanvas();

		c.addEventListener('backgroundLoaded', function(){
			self.basketSlide();
		});

		c.addEventListener('basketLoaded', function(){
			self.eggSlide();
		});

		c.addEventListener('eggLoaded', function(){
			self.eggCrack();
		});

	},

	setMsg: function(msg){
		this.lotteryMsg = msg;
	},

	backgroundFade: function(){
		var bg = Lottery.Images.imgObj[4];
		Lottery.Animations.fadeIn(bg);
	},
	basketSlide: function(){
		var basket = Lottery.Images.imgObj[3];
		var bg = Lottery.Images.imgObj[4];

		Lottery.Animations.slideIn(bg, basket);
	},
	eggSlide: function(){
		var egg = Lottery.Images.imgObj[2];
		var basket = Lottery.Images.imgObj[3];
		var bg = Lottery.Images.imgObj[4];

		Lottery.Animations.slideFadeIn(bg, basket, egg);
	},
	eggCrack: function(){
		var basket = Lottery.Images.imgObj[3];
		var bg = Lottery.Images.imgObj[4];
		var cEgg2 = Lottery.Images.imgObj[1];
		var cEgg1 = Lottery.Images.imgObj[0];
		var cClose = Lottery.Images.imgObj[5];

		Lottery.Animations.crack(bg, basket, cEgg2, cEgg1, cClose);
	}

};