var Steps = {

    init : function () {
      this.prev();
      this.next();
    },

    prev: function () {
        var self = this;

        var to_prev = $('.to-prev');
        if(!to_prev.length) return false;

        to_prev.on('click', function () {
            self.showLoader();
            var _form = $(this).closest('form.form');
            _form.find('[name="then"]').val(':back');
            _form.submit();
        });

    },

    next: function () {
        var self = this;
        var to_next = $('.to-next');

        if(!to_next.length) return false;

        to_next.on('click', function () {
            self.showLoader();
            var _form = $(this).closest('form.form');
            _form.find('[name="then"]').val(':next');
            _form.submit();

        });

    },

    showLoader: function () {
        MCubix.OverlayV2.init(true);
    },

    hideLoader: function () {
        MCubix.OverlayV2.init(false);
    }

};

$(document).ready(function () {
    Steps.init();
});