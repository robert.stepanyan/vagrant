Cubix.premiumPopup = {};

Cubix.premiumPopup.inProcess = false;

Cubix.premiumPopup.url = '';

Cubix.premiumPopup.Show = function () {
	if ( Cubix.premiumPopup.inProcess ) return false;


	$('#premium-popup').addClass('open');

	Cubix.premiumPopup.inProcess = true;

	return false;
};
Cubix.premiumPopup.hide = function () {
	$('#premium-popup').removeClass('open');
	$('#tour-edit').remove();

	Cubix.premiumPopup.inProcess = false;

	return false;
};
