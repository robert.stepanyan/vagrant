$(document).ready( function(){
    //var page_overlay = new Cubix.Overlay(document.body, {has_loader: false, opacity: 0.5, color: '#000', z_index: 100});
    var escort_id = $('#escort_id').val();

    if ( $('#alm-wr').length )
    {
        var alertme = $('#alm-wr');

        $('#alm-btn').on('click', function(e){
            e.preventDefault();

            if ( alertme.hasClass('none') ) {
                //page_overlay.disable();
                alertme.removeClass('none');
            }
            else {
                //page_overlay.enable();
                alertme.addClass('none');
            }
        });

        $('.alm-x').on('click', function(e) {
            e.preventDefault();

            //page_overlay.enable();
            alertme.addClass('none')
        });
    }

    if ( $('#alm-wr-m').length )
    {
        var alertme_m = $('#alm-wr-m');
        var cities = [];

        $('.alm-btn-mm').on('click', function(e){
            e.preventDefault();

            if ( alertme_m.hasClass('none') ) {
                //page_overlay.disable();

                $.ajax({
                    method: 'get',
                    url: '/escorts/ajax-alerts?escort_id=' + escort_id,
                    success: function( response ) {
                        var object = jQuery.parseJSON(response);
                        var evs = object.events;
                        var cts = object.cities;

                        if (evs.join() == '1,2,3,4')
                        {
                            $('#alm-5').prop('checked', true);
                        }
                        else if (evs.join(',') == '1,2,3,4,6')
                        {
                            $('#alm-5').prop('checked', true);
                            $('#alm-6').prop('checked', true);
                        }
                        else
                        {
                            $.each( evs, function(i, v) {
                                $('#alm-' + v).prop('checked', true);
                            })
                        }

                        // fill cities
                        $('#city-items').empty();
                        $('#city_id').val('');
                        cities = [];

                        $.each( cts, function(i,el) {
                            cities.push(el.id);
                            addCity(el.id, el.title);
                        });

                        removeCity(cities);
                        //
                        checkCHB();
                        alertme_m.removeClass('none');
                    }
                });
            }
            else {
                //page_overlay.enable();
                alertme_m.addClass('none');
            }
        });

        $('.alm-x').on('click', function(e) {
            e.preventDefault();

            //page_overlay.enable();
            alertme_m.addClass('none');
        });

        $('#alm-5').on('change', function(e) {
            e.preventDefault();

            checkCHB();
        });

        $('#alm-save').on('click', function(e) {
            e.preventDefault();

            var i = 0;
            var events = [];

            $("input[type=checkbox]:checked").each(function(j, v)
            {
                events[i] = $(this).attr('id').substr(4, 1);
                i++;
            });

            var ev = events.join(',');

            if (ev == '5')
                ev = '1,2,3,4';
            else if (ev == '5,6')
                ev = '1,2,3,4,6';

            var cu = '';

            if ( ~$.inArray('6', events) )
            {
                var cities_str = cities.join(',');
                cu = '&cities=' + cities_str;
            }


            $.ajax({
                method: 'get',
                url: '/escorts/ajax-alerts-save?escort_id=' + escort_id + '&events=' + ev + cu,
                beforeSend: function(){
                    MCubix.OverlayV2.init( true );
                },
                success: function(response) {
                    MCubix.OverlayV2.init( false );
                    var object = jQuery.parseJSON(response);
                    if (object.count > 0)
                    {
                        if ( $('#alm-b-a').hasClass('none') == true )
                            $('#alm-b-a').removeClass('none');
                        if ( $('#alm-b').hasClass('none') == false )
                            $('#alm-b').addClass('none');
                    }
                    else
                    {
                        if ( $('#alm-b-a').hasClass('none') == false )
                            $('#alm-b-a').addClass('none');
                        if ( $('#alm-b').hasClass('none') == true )
                            $('#alm-b').removeClass('none');
                    }

                    //page_overlay.enable();
                    alertme_m.addClass('none');
                    $('#overlay').addClass('none');
                }
            });
        });

        $('.alm-city-btn').on('click', function () {
            var c = $('#city_id').find(':selected'),
                c_id = parseInt( c.val() ),
                c_txt = c.text().toString();

            $('#alm-6').prop('checked', true);

            if ( c_id != null && !~$.inArray( c_id, cities ) && c.val() )
            {
                cities.push(c_id);
                addCity(c_id, c_txt);
                removeCity(cities);
            }
        });
    }
});

function addCity(id, text)
{
    var __a_elm = $('<a/>', { id: id, 'class': 'city-item' });
    var __div_elm  = $('<div/>', { 'class': 'city-item-wr' });
    __div_elm.append( __a_elm );
    __div_elm.append(text);
    $('#city-items').append( __div_elm );
}

function removeCity(cities)
{
    var c_item = $('.city-item');

    c_item.unbind();
    c_item.on('click', function (e) {

        var id = $(this).attr('id');

        removeByElement(cities, id);
        $(this).parent().remove();
    });
}

function removeByElement(arrayName, arrayElement)
{
    for (var i = 0; i < arrayName.length; i++)
    {
        if (arrayName[i] == arrayElement)
            arrayName.splice(i,1);
    }
}

function checkCHB()
{
    if ( $('input[name="any"]').is(':checked') )
    {
        $('input[name="comment"]').prop('checked', false).prop('disabled', 'disabled');
        $('input[name="profile"]').prop('checked', false).prop('disabled', 'disabled');
        $('input[name="pictures"]').prop('checked', false).prop('disabled', 'disabled');
        $('input[name="review"]').prop('checked', false).prop('disabled', 'disabled');
    }
    else
    {
        $('input[name="comment"]').prop('disabled', false);
        $('input[name="profile"]').prop('disabled', false);
        $('input[name="pictures"]').prop('disabled', false);
        $('input[name="review"]').prop('disabled', false);
    }
}
/* <-- */

