Cubix.BitcoinInfo = {};

Cubix.BitcoinInfo.inProcess = false;

Cubix.BitcoinInfo.url = '';

Cubix.BitcoinInfo.Show = function () {
	if ( Cubix.BitcoinInfo.inProcess ) return false;
	

	$('#bitcoin-payment-info-wrapper').addClass('open');
	
	Cubix.BitcoinInfo.inProcess = true;

	$('#bitcoin-payment-info-button').on('click', function() {
		$('#bitcoin-payment-info-wrapper').remove();
	});
	
	return false;
};


