var Cubix = {};

Cubix.Lang = {};

/*        === JQUERY NEW FUNCTIONALITY AND PLUGINS ===        */
$.fn.outerHTML = function(){
	return jQuery('<div>').append( $(this).clone().removeClass('toHide')).html();
};

MCubix.URI = {
    redirect: function( url ){
        window.location.href = url;
    }
};

$.fn.collapsify = function() {
	var cols = $(this).find('.faq__col'),
		quest_rows = cols.find('.ques'),
		answ_rows = cols.find('.answ'),
		max_height = 0;


	var heights_obj = [];
	quest_rows.each(function(){
		heights_obj.push( Number( $(this).outerHeight() ) );
	});

	max_height = Math.max.apply( Math, heights_obj );
	quest_rows.css({ 'max-height': max_height });

	quest_rows.on('click', function(){
		quest_rows.not($(this)).removeClass('__opened');
		answ_rows.css({ opacity: 0, 'max-height': 0 });

		if( !$(this).hasClass('__opened') ){
			$(this).addClass('__opened');
			animateRow( $(this).closest('.faq__col').find('.answ'), true );
		} else {
			$(this).removeClass('__opened');
			animateRow( $(this).closest('.faq__col').find('.answ'), false );
		}
	});

	function animateRow( row, st ){
		if( st ){
			row.animate({ opacity: 1, 'max-height': 1000 }, 300);

			if( !row.closest('.no-scr').length ) {
				$('html, body').animate({
					scrollTop: (row.closest('.faq__col').first().offset().top )
				}, 300);
			}
		} else {
			row.animate({ opacity: 0, 'max-height': 0 }, 300);
		}
	}
};

jQuery.expr[":"].containsLower = jQuery.expr.createPseudo(function(arg) {
	return function( elem ) {
		return jQuery(elem).text().toLowerCase().indexOf(arg.toLowerCase()) >= 0;
	};
});

jQuery.expr[":"].containsUpper = jQuery.expr.createPseudo(function(arg) {
	return function( elem ) {
		return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
	};
});

//Cubix.ShowTerms = function (link) {
//	window.open(link, 'showTerms', 'menubar=no, resizable=yes, status=no, scrollbars=yes, toolbar=no, width=640, height=480');
//	return false;
//}

var _log = function (data) {
	if ( console && console.log != undefined ) {
		console.log(data);
	}
};

var _st = function (object) {
	var parts = object.split('.');
	if ( parts.length < 2 ) {
		return _log('invalid parameter, must contain filename with extension');
	}

	var ext = parts[parts.length - 1];
	
	var base_url = 'http://st.escortforumit.xxx',
		prefix = '';

	switch ( ext ) {
		case 'jpg':
		case 'gif':
		case 'png':
			prefix = '/img/';
			break;
		case 'css':
			prefix = '/css/';
			break;
		case 'js':
			prefix = '/js/';
			break;
		default:
			return _log('invalid extension "' + ext + '"');
	}

	url = base_url + prefix + object;

	return url;
};

// Taken from viewed-escorts.js
/* --> Viewed Escorts */
Cubix.Viewed = {};

Cubix.Viewed.url = ''; // Must be set from php
Cubix.Viewed.container = '';

Cubix.initMoreLessButton = function() {
	var _elm = $('a.more-less-button');
	if ( ! _elm.length ) return false;

	_elm.on('click', function(e){
		e.stop();

		var more_cities_box = $('div.more-cities-box')[0];

		var curr_title = $(this).html();

		if ( $(this).hasClass('more') ) {
			more_cities_box.removeClass('none');
			$(this).removeClass('more').addClass('less');
		} else if ( $(this).hasClass('less') ) {
			more_cities_box.addClass('none');
			$(this).removeClass('less').addClass('more');
		}

		//this.set('html', this.get('rel')).set('rel', curr_title);
		$(this).html( $(this).attr('rel') ).attr('rel', curr_title);
	});
};

$(document).ready( function() {

	Cubix.initMoreLessButton();
	MCubix.Inputs.StyleMaterial.init();
	
	$('div.flags a').on('click', function(e){
		window.location = $(this).attr('rel');
	});
});

/* START :: INDEX PAGE */
var Index = {};

Index.Cities = {
	init: function () {
		$('#load-more').on('click', function(){
			$('.more-cities').removeClass('hidden');
			$(this).addClass('hidden');
		});
	},

	liveSearch: function () {
		var self = this;
		var searchInput = $('#city-search');

		searchInput.on('keyup', function(e){
			self.filter(searchInput);
		});

	},

	filter: function ( search ){
		var _str = search.val();
		var searchQuery = _str.toLowerCase();

		$('.cities .city .city-title').each( function() {
			var el = $(this);
			var _txt = el.html();
			var cityName = _txt.toLowerCase();
			var __li = el.closest('li'); // ('.__more')

			if( __li.closest('.more-cities').length ) return false;

			if ( !cityName || cityName.indexOf(searchQuery) == -1 ){
                __li.addClass('hidden');
			} else {
				if ( __li.hasClass('hidden') ) {
                    __li.removeClass('hidden');
                }
			}
		});

        if( !_str ){
            $('li.__more').addClass('hidden');
        }
	}
};

MCubix.autoScrollIcon = function( st ){
    var btn = $('.show-more-ajax');

    if( btn.length !== 1 ) return false;

    btn[st ? 'fadeIn' : 'fadeOut' ](300);
};

Index.Escorts = {
	page: null,
	startSearch: null,
    toScroll: true,

	init: function(){
		var self = this;

        $(window).on('scroll', function(){
            var page = $('#load-more-escorts').attr('data-page');
            var data = { "page" : Number( page ) + 1, "ajax": 1 };

            var pos1 = $(document).scrollTop() + $(window).height() + 200,
                pos2 = $(document).outerHeight();

            if( self.toScroll ){
                if( pos1 > pos2 ){
                    MCubix.autoScrollIcon( true );
                    self.toScroll = false;

                    data = { "page" : Number( page ) + 1, "ajax": 1 };
                    self.sendRequest(data);
                }
            }
        });
	},

	scrollEvent: function(){

	},

	sendRequest: function( data ){
		var self = this;
		var block = $('#ajaxResults');
		var pageIndexAtt = $('#load-more-escorts');

		$.get( '/', data, function( resp ){
			//console.log( resp );
			var parent = $( resp );
			var elms = parent.find('.ajax-escort');
			//console.log(elms);
			//elms = elms[1];

			elms.each(function(){
				var el = $(this);
				block.append( el );
			});

			//block.inject(elms, 'after');
			pageIndexAtt.attr('data-page', Number(data['page']));
            self.toScroll = true;
            MCubix.autoScrollIcon( false );
		});
	},

	removeChildElements: function( parent ){
		parent.html('');
	},

	liveSearch: function () {
		var self = this;
		var searchInput = $('#escort-search');
		var block = $('#ajaxResults');
		

		searchInput.on('keyup', function(){
			if ( self.startSearch ) clearTimeout( self.startSearch );

			if ( searchInput.val().length > 0 ) {
                MCubix.OverlayV2.init( true, block );

				self.startSearch = setTimeout( function(){
					self.sendFilter(searchInput.val());
				}, 1000 )
			} else {
				self.removeChildElements( block );
				self.sendRequest({ 'page': 1, 'ajax': 1 });
			}
		});

	},

	sendFilter: function( showname ){
		var self = this;
		var block = $('#ajaxResults');
		var pageIndexAtt = $('#load-more-escorts');

		$.get( '/search?gender[]=1&order=1&search=Cerca&showname=' + showname, { "ajax": 1 }, function( resp ){
			//console.log( resp );
			var parent = $( resp );
			var elms = parent.find('.escort');
			self.removeChildElements( block );

			elms.each(function(elm, i){

				var el = $(this);
				block.append( el );
			});

            MCubix.OverlayV2.init(false, block);

		});
	}

};


/* END :: INDEX PAGE */

/* START :: ESCORT LIST */
var MEscorts = {};

MEscorts.View = {


	init: function() {
		var self = this;
		var viewSwitchers = $('#view-switcher a');
		var viewChoice = self.getChoice('view');

		viewSwitchers.each(function(){
			var el = $(this);

			el.on('click', function(e){
				if ( !$( e.target ).hasClass('active') ){
					var viewSel = e.target.id.split('-')[0];

					self.switchView(viewSel);
					self.setChoice(viewSel);
				}
			});

		});

		if ( typeof viewChoice == "undefined" || viewChoice == null ) {
			self.switchView('grid');
			self.setChoice('grid');
		} else {
			self.switchView(viewChoice);
			self.setChoice(viewChoice);
		}

	},

	setChoice: function(value) {
		MCubix.Cookies.delete('view');
		MCubix.Cookies.set('view', value);
	},

	getChoice: function(name) {
		return MCubix.Cookies.get(name);
	},

	switchView: function(v) {
		var viewPage = $('#city-page');
		var viewSwitchers = $('#view-switcher a');
		var viewSelector = $('#' + v + '-view');

		if ( viewPage.hasClass('list') ) {
			if ( v != 'list' ) {
				viewPage.removeClass('list')
			}
		} else {
			if ( v == 'list' ) {
				viewPage.addClass('list');
			}
		}

		viewSwitchers.each(function(){
			var el = $(this);
			if ( el.hasClass('active') ){
				el.removeClass('active');
			}
		});

		viewSelector.addClass('active');

	}
};

/* END :: ESCORT LIST */

/* START :: ESCORT PROFILE */

MEscorts.Profile = {
	init: function() {
		var self = this;
		var pMenu = $('#menu');
		var pMenuItems = $('#menu li a');

		self.showComment();
		self.deslectAll(pMenuItems);
		self.selectMenu( $('#t_bio') );
		self.hideAll();
		self.showContent('bio');
		self.videoChat();	
		
		pMenuItems.each(function(){
			var el = $(this);

			el.on('click', function(e){
				//var parentCont = $(e.target.closest('li'));
				//var strName = parentCont.attr('id').replace('t_','');

				var parentCont = $(this).closest('li');
				var strName = parentCont.attr('id').replace('t_','');

				self.toggleBtn(strName);
				self.deslectAll(pMenuItems);
				self.selectMenu(parentCont);
				self.scrollToContent(strName);
				self.hideAll();
				self.showContent(strName);

			});
		});

		$('#addComm').on('click', function(e) {
			self.hideCommBtn();

			var commWrapper = $('#comments');

			if (commWrapper.length) {
				var commFromPromise = self.getCommentsBlock();

				commFromPromise.done(function(resp) {
					commWrapper.append(resp);
					self.handleAddComment();

					$('html, body').animate({
				        scrollTop: $('#addCommentWrapper').offset().top
				    }, 500);		
				});
			}
		});
	},

	showComment: function() {
		if (window.location.href.indexOf('#comment_success') < 0) {
			return false;
		} else {
			var notification = $('<div class="success-notification animated slideInUp">Your comment was sent for moderation and will be published shortly. Thank you!</div>');
			$(document.body).append(notification);

			notification.on('click', function() {
				$(this).remove();
			});
		}
	},

	handleAddComment: function() {
		var self = this;

		$('#addCommentWrapper input.btn-close').on('click', function(e) {
			e.preventDefault();

			$('#addCommentWrapper').hide();

			$('#addCommentWrapper').remove();
			$('#comments-tab').trigger('click');
		});

		$('#addCommentWrapper input.btn-save, #addCommentWrapper input.btn-next').on('click', function(e) {
			e.preventDefault();

			$(this).addClass('inactive');
			var msg = $('#comment').val();
			var userId = $('#comment_user_id').val();
			self.saveComments(msg, userId);
		});
	},

	toggleBtn: function(openedItem) {
		var $addComm = $('#addComm'),
			$addRev = $('#addRev');

		if ('reviews_comments' === openedItem) {
			if ($('#reviews-tab.tapped').length > 0) {

				$addComm.toggleClass('slideInUp slideOutDown');

				setTimeout(function() {
					$addComm.toggleClass('slideInUp slideOutDown').addClass('none');
					$addRev.removeClass('none');
				}, 500);

			} else {
				if ($('#addCommentWrapper').length > 0) {
					$('html, body').animate({
				        scrollTop: $('#addCommentWrapper').offset().top
				    }, 500);
					
					return false; 
				}


				$addRev.toggleClass('slideInUp slideOutDown');

				setTimeout(function() {
					$addRev.toggleClass('slideInUp slideOutDown').addClass('none');
					$addComm.removeClass('none');
				}, 500);

			}
		} else {
			$('#addComm').addClass('none');
			$('#addRev').addClass('none');
		}
	},

	hideCommBtn: function() {
		var $addComm = $('#addComm');

		$addComm.toggleClass('slideInUp slideOutDown');

		setTimeout(function() {
			$addComm.toggleClass('slideInUp slideOutDown').addClass('none');
		}, 500);
	},

	hideAll: function() {
		var pPageContainers = $('#page div[id*=_container]');

		pPageContainers.each(function(){
			var el = $(this);

			el.addClass('none');
		});
	},

	deslectAll: function(menu) {
		menu.each(function(){
			var el = $(this);
			var navItemCont = el.closest('li');

			if ( navItemCont.hasClass('active') ){
				navItemCont.removeClass('active');
			}
		});
	},

	showErrors: function(msgs) {
		$('.captcha-error').addClass('toHide');
		$('.comment-error').addClass('toHide');

		$.each(msgs, function(index, el) {
			if (el == 'captcha_error') {
				$('.captcha-error').removeClass('toHide');
			}

			if (el == 'comment_to_short') {
				$('.comment-error').removeClass('toHide');
			}
		});
	},

	saveComments: function(comment, userId) {
		var self = this;

		$.ajax({
			method: 'POST',
			url: lng + '/comments-v2/ajax-add-comment/',
			dataType: 'json',
			data: {
				escort_id: escortId,
				user_id: userId,
				comment: comment,
				url: window.location.href + '#comment_success'
			},

			success: function(resp) {
				if (resp.status == 'error') {
					self.showErrors(resp.msgs);
				} else if (resp.status == 'pending') {
					$('.pending-msg').removeClass('toHide');
					
					window.location.href = lng + '/private/signin-comment';
				} else {
					$('.buttons').remove();
					$('#addCommentWrapper').remove();
					window.location.hash = 'comment_success';
					self.showComment();
					$('.success-msg').removeClass('toHide');
				}

				$('#addCommentWrapper input.btn.inactive').removeClass('inactive');
			}
		});
	},

	getCommentsBlock: function() {
		return $.ajax({
			method: 'GET',
			url: lng + '/comments-v2/ajax-add-comment',
			data: {
				escort_id: escortId
			}
		});
	},

	selectMenu: function(menuItem) {
		menuItem.addClass('active');
	},

	showContent: function(name){
		$('#page div[id*=' + name + '_container]').removeClass('none');

		if ( name == 'bio' ){
			$('#page div#about_container').removeClass('none');
		}
	},

	scrollToContent: function(name){
		window.scrollTo(0, 0);
	},
	
	videoChat: function(){
		
		if( $('#video-chat-box').length > 0){

			if(!headerVars.currentUser){
				$('#video-chat-box .vc-button').on('click', function(){
					
					MCubix.PopupAnim.init(true, {
					ajax: {
						url: '/ef-cams/login',
						method: 'GET',
						callback: function(){
							$('#popup-container').append('<span class="close-icon"></span>');  
							
							$('.close-icon').on('click', function(){
								MCubix.PopupAnim.currentClose();
							});

							$('#popup-container #submit-button').on('click', function(e){
								e.preventDefault();
								$.ajax({
									url: "/private/signin?ajax",
									data: $('#sign-in-popup').serialize(),
									type: 'POST',
									beforeSend: function(){
										 MCubix.OverlayV2.init(true);
									},
									success: function (resp) {
										 MCubix.OverlayV2.init(false);
										resp = jQuery.parseJSON(resp);
										
										if(resp.status == 'error'){
											
											
											$('#popup-container .req').removeClass('req');
											
											

											for ( var field in resp.msgs ) {

												var input = $('#popup-container').find('*[name=' + field + ']');
												input.addClass('req');

												//getErrorElement(input).html(resp.msgs[field]);
											}
										}
										
										else if(resp.signup == true){
											window.location.href = '/private/signup-success';
											return;
										}
										else{
											window.location.reload();
											return;
										}
									}
								});							
							})
							

							if($('#go-signup-button')){
								$('#go-signup-button').on('click', function(e){
									e.preventDefault();
									MCubix.PopupAnim.currentClose();
									MCubix.PopupAnim.init(true, {
										ajax: {
											url: '/ef-cams/signup',
											method: 'GET',
											callback: function(resp){
												$('#popup-container #submit-button').on('click', function(e){
												e.preventDefault();
												$.ajax({
													url: "/private/signup-member?type=member&ajax=1",
													data: $('#popup-container #sign-up-popup').serialize(),
													type: 'POST',
													beforeSend: function(){
														 MCubix.OverlayV2.init(true);
													},
													success: function (resp) {
														 MCubix.OverlayV2.init(false);
														resp = jQuery.parseJSON(resp);
														
														if(resp.status == 'error'){
															
															$('#popup-container .error-notes').remove();
															$('#popup-container .req').removeClass('req');
															
															

															for ( var field in resp.msgs ) {

																var input = $('#popup-container').find('*[name=' + field + ']');
																input.addClass('req');

																//getErrorElement(input).html(resp.msgs[field]);
															}
														}
														
														else if(resp.signup == true){
															window.location.href = '/private/signup-success';
															return;
														}
														else{
															window.location.reload();
															return;
														}
													}
												});							
											})

											},
											
										},
										
										css:{
											background: '#fff',
											margin: '20px 10px',
											'border-radius': '10px'
										}, 

										closing: false
										});
								
								});
							}
						},
					},
					
					css:{
						background: '#fff',
						margin: '20px 10px',
						'border-radius': '10px'
					}, 

					closing: false
					});
				});
			}else{
				if($('video-chat-box') && headerVars.currentUser.user_type == 'member'){
					$('#video-chat-box .vc-button').on('click', function(){
						var data = {
							escort_id: $('#escort_id').val(),
							duration : $(this).data('type'),
							price : $(this).data('price')
						}

						$.ajax({
							url: "/ef-cams/go-to-book",
							data: data,
							type: 'POST',
							beforeSend: function(){
								 MCubix.OverlayV2.init(true);
							},
							success: function (resp) {
								resp = jQuery.parseJSON(resp);

								if(resp.url){
									window.open(resp.url);
								}
								MCubix.OverlayV2.init(false);
							}
						});							
						
					});
				}else{
					$('#video-chat-box .vc-button').on('click', function(){
						alert('Permission denied')
					})
				}
			}
		}
	}
};

/* END :: ESCORT PROFILE */

/* START :: ESCORT REVIEWS */

MEscorts.Reviews = {
	revCount: null,
	read_review: null,
	close_review: null,

	init: function(lang, escShowname, escId){
		var self = this;
		var _elm = $('#load-more');
		var mLoadMore = ( typeof _elm != 'undefined' ) ? _elm : false;

		if( !mLoadMore ) return false;

		var readReviews = $('#reviews a.read-review');

		var page = parseInt(_elm.attr('data-page'));
		var reviews = $('#reviews a[id*=toggle]');

		mLoadMore.on('click', function() {
			self.sendRequest({
				'lang': lang,
				'showname': escShowname,
				'id': escId,
				'page': page,
				'ajax': 1
			});

			_elm.attr('data-page', page + 1);
		});



	},

	setCount: function ( counter ) {
		var self = this;
		self.revCount = counter;
	},

	sendRequest: function( data ) {

		var self = this;

		$.get( data.lang + '/reviews-ajax/' + data.showname + '-' + data.id, { 'page' : data.page }, function( resp ){

			var revCont = $('#reviews');
			var respEl = $(resp);

			revCont.append(respEl);

			var shownRevs = $('#reviews .user-avatar');

			if (self.revCount <= shownRevs.length  ) {
				$('#load-more').empty().remove();
			}

		});
	},

	getReviewById: function ( data, lang, escShowname, escId ) {
		var self = this,
			_data = $(data);

		$.get( lang + '/evaluations/' + escShowname + '-' + escId, {'perpage': 'all', 'ajax': 1}, function( resp ){

			var recedRevs = $(resp);
			var revCont = $('#slidable-' + _data.attr('id'));

			recedRevs.each(function(){
				var el = $(this);

				if ( el.attr('id') == _data.attr('id') ) {
					if (revCont.hasClass('heightAuto')) {
						revCont.removeClass('heightAuto');
						_data.html("&#9658; " + self.read_review);
					} else {
						revCont.empty();
						revCont.append( el.find('div.review-info') );
						revCont.addClass('heightAuto');
						_data.html("&#9658; " + self.close_review);
					}
				}
			});

		});
	}

};

MPage = {};

MPage.Scroll = {
	init: function(){

		var self = this;
		var objScrollTop = $('#scroll-top');
		objScrollTop.hide();

		$(window).on('scroll', function() {
			var intTop = window.pageYOffset;

			//objInfoBar = undefined;
			//objBubbleMsg = undefined;

			var objInfoBar = $('#info-bar-full');
			var objBubbleMsg = $('#bubble-msg');

			if (typeof objBubbleMsg == 'object' && objBubbleMsg){
				objScrollTop.css('bottom', objBubbleMsg.height() + 4);
			} else {
				objScrollTop.css('bottom', 4);
			}

			if (typeof objInfoBar == 'object' && objInfoBar){
				objScrollTop.css('bottom', objInfoBar.height() + 4);
			}

			if ( decTimer ) clearTimeout (decTimer);

			var decTimer = setTimeout( self.showArrow(intTop), 500 );
		});

		objScrollTop.on('click', function(){
			window.scrollTo(0, 0);
		});


	},

	showArrow: function(intTop){
		var objScrollTop = $('#scroll-top');
		var objHeader = $('#header');

		if ( intTop > objHeader.height() ) {
			objScrollTop.removeClass('none');
			objScrollTop.fadeTo( 100, 0.5, function(){
				$(this).fadeIn( 300 );
			});
		} else {
			objScrollTop.removeClass('none');
			objScrollTop.fadeTo( 100, 0, function(){
				$(this).fadeOut();
			});
		}
	}
};

MPage.Sticky = {
	init: function(){
		$(window).on('scroll', function() {
			var scrollTop = window.pageYOffset;
			var st_crumbs = $('#path');
			var st_menu = $('#menu');
			var st_page = $('#page');

			if (scrollTop > 86) {
				st_crumbs.addClass('fixTop');
				st_menu.addClass('fixTop');
				st_page.addClass('mt88');
			} else {
				st_crumbs.removeClass('fixTop');
				st_menu.removeClass('fixTop');
				st_page.removeClass('mt88');
			}
		});
	}
};

/***************** Karo Start *****************/


var Private = {};

Private.Message = {
	init: function( showname, escort_id ){
		var self = this;
		$('.private-messanger').on('click', function(){
			var data = { "participant_id" : $(this).attr('data-id'), "participant_type" : 'escort'/*$(this).getProperty('data-type')*/ }
			//console.log( data );
			self.sendRequest( data, showname, escort_id );
		})
	},

	sendRequest: function( data, showname, escort_id ){
		var self = this;
		var block = $('#private-message-block');
		var wrapper = $('#private-message-wrapper');
		//var scoller = new Fx.Scroll(window);

		$.ajax({
			method: "GET",
			url: '/private-messaging/send-message-ajax',
			data: data,
			beforeSend: function(){
				wrapper.css({ 'display' : 'block', 'opacity' : 1, 'visibility' : 'visible' });
			},
			success: function( resp ){
				if( resp == 'signin' ){
					window.location.href = "/private/signin";
				}
				if ( ! resp ) return;

				block.html(resp);

				$('html, body').animate({
					scrollTop: block.offset().top
				}, 2000);

				self.sendMessage( escort_id );
			}

		});
	},

	sendMessage: function(escort_id){
		var sendButton = $('.red-btn');

		sendButton.on('click', function(e){
			e.preventDefault();

			var data = {
				'participant' : $('#participant').val(),
				//'captcha' : $('#f-captcha').val(),
				'f-message' : $('#privateMessage').val(),
				'escort_id' : escort_id
			};

			$.ajax({
				method: "POST",
				url: '/private-messaging/send-message-ajax',
				data: data,
				beforeSend: function(){

				},
				success: function(_resp){
					var resp = jQuery.parseJSON(_resp);

					if( resp.status == 'error' ){
							if( typeof resp.msgs.captcha != 'undefined' ){
							if( resp.msgs.captcha == '' ){
								$('#f-captcha').addClass('field-error');
							} else {
								$('#f-captcha').addClass('field-error').attr('placeholder', 'incorect value');
							}
						} else {
							$('#f-captcha').removeClass('field-error');
						}

						if( resp.msgs.fmessage == '' ){
							$('#privateMessage').addClass('field-error');
						} else {
							$('#privateMessage').removeClass('field-error');
						}
					} else {
						$('#privateMessage').val('').removeClass('field-error');
						$('#f-captcha').val('').removeClass('field-error');
						$('.send-success').html(resp.msg);
					}
				}

			});
		})
	}
};

Private.User = {
	init: function () {},

	signup: function() {
		var userTypeOptions = $('.reg-top-container input');
		var salsePerson = $('#sales_person');

		userTypeOptions.each(function() {
			var el = $(this);

			el.on('click', function(){
				( el.id == 'user_type_member' ) ? salsePerson.disabled = true : salsePerson.disabled = false;
			});
		});
	}
};


MCubix.ErrorsHandler = {
	elm: null,

	messages: {
		'have_errors': 'Exists error !',
		'city_count': 'City count',
		'city_count_test': 'City count',
		'year': 'Year !',
		'month': 'Month must be between 1 and 12',
		'day': 'Day must be between 1 and 31',
		'day_31': 'Self month doesn\'t have 31 days!',
		'day_feb': 'February doesn\'t have that many days!',
		'date_out': 'Please select currect date'
	},

	doShow: function( elm, key, dict ){
		var self = this;

		if( !dict ) {
			elm.html(self.messages[key]).show();
		} else {
			elm.html(key).show();
		}
		setTimeout(function() {
			elm.html('').hide();
		}, 3000 );
	},

	doHide: function(elm){
		elm.html('').hide();
	}
};

MCubix.Cookies = {
	set: function( cname, cvalue, exdays, domain, path )
	{
		var d = new Date();
		d.setTime( d.getTime() + ( exdays * 24 * 60 * 60 * 1000 ) );
		var expires = "expires=" + d.toGMTString();
		var cookie_str = cname + "=" + cvalue + "; " + expires;

		if ( domain ){
			cookie_str += "; domain=" + encodeURI ( domain );
		}

		if ( path ){
			cookie_str += "; path=" + encodeURI ( path );
		}

		document.cookie = cookie_str;
	},

	get: function( cname )
	{
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for( var i=0; i<ca.length; i++ )
		{
			var c = ca[i].trim();
			if ( c.indexOf( name ) == 0 ) return c.substring( name.length,c.length );
		}
		return "";
	},

	delete: function( cname ){
		document.cookie = cname + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	}
};

Cubix.Overlay = {
	//Implements: Options,

	options: {
		opacity: .8,
		color: '#fff',
		loader: _st('loader-big.gif'),
		position: '140px',
		has_loader: true,
		overlay_class: 'overlay',
		no_relative: false,
		offset: {
			left: 0,
			top: 0,
			right: 0,
			bottom: 0
		},
		z_index: 100
	},

	element: null,
	overlay: null,

	initialize: function (element, options) {
		this.element = $(element);

		this.options = this.mergeOptions( this.options, options ) || {};
		//console.log(this.options);

		if ( ! this.options.no_relative ) {
			this.element.css('position', 'relative');
		}

		this.overlay = this.render();
		this.overlay.append(element);
	},

	mergeOptions: function( obj1, obj2 ){
		var obj3 = {};
		for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
		for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
		return obj3;
	},

	render: function () {
		var self = this;

		var overlay = $('div')
				.css({
					'background-color': self.options.color,
					position: 'absolute',
					left: self.options.offset.left,
					top: self.options.offset.top,
					zIndex: self.options.z_index,

					opacity: self.options.opacity
				});

		overlay.addClass(self.options.overlay_class);

		if ( self.options.has_loader ) {
			overlay.css({
				'background-repeat': 'no-repeat',
				'background-position': '50% ' + self.options.position,
				'background-image': 'url(' + self.options.loader + ')'
			});
		}

		return overlay;
	},

	enable: function () {
		this.overlay.addClass('overlay-hidden');
	},

	disable: function () {
		this.overlay.removeClass('overlay-hidden');
		this.fit();
	},

	fit: function () {
		var el = this.element;

		this.overlay.css({
			width: el.getWidth() + this.options.offset.right,
			height: el.getScrollHeight() + this.options.offset.bottom
		});
	}
};

MCubix.VibrationApi = {
	call: function( interval ){
		if( "vibrate" in navigator ){
			navigator.vibrate = navigator.vibrate || navigator.webkitVibrate || navigator.mozVibrate || navigator.msVibrate;

			if (navigator.vibrate) {
				navigator.vibrate( interval );
			}
		}
	}
};

MCubix.HTML5.Inputs = {
	getTimestamp: function( elm, is_select ){
		var date;

		if( is_select ){
			date = new Date( elm.find(':selected').val() );
		} else {
			date = new Date( elm.val() );
		}

		return ( date.getTime() / 1000 );
	}
};

MCubix.Popup = {
	/*
	*
	* IF OPEN POPUP { content: '<div></div> or jQuery element object', id_length: xxx,
	* 	centered: true or false(default), fullscreen: true or false(default), padding: false or 40px  }
	* IF CLOSE POPUP { id: xxx ( returned id if you opening popup ) }
	*
	* */
	init: function( status, options, callback, callback_variables ){

		var self = this,
			un_id, popup_wrapper, closing;

		closing = !( options.closing === false );

		if( !options.id_length )
			un_id = MCubix.GenerateHash.init( 8 );
		else
			un_id = Number( options.id_length );

		if( status ){
			if( !options.content || $( '#popup-' + options.id ).length > 0 ) return false;

			var inner_pos = $('<div/>', { class: 'inner-pos' }),
				inner_popup = $('<div/>', { class: 'inner-popup' });

			popup_wrapper = $('<div/>', { id: 'MCubix-popup-' + un_id, 'class': 'custom-api-popup' });

			inner_popup.append( options.content);
			if( closing ){
				var cl_btn = $('<span/>', { 'class': 'close-photo' }).text('X');
				inner_popup.append( cl_btn );

				cl_btn.on('click', function(){
					self.init( false, { id: un_id } );
				})
			}

			inner_pos.append( inner_popup );

			if( options.centered ){
				inner_popup.addClass('centered');
			}

			if( options.fullscreen ){
				inner_popup.addClass('full-screen');
			}

			if( options.padding ) inner_popup.css( 'padding', options.padding );
			if( options.css ){
				for( var _i in options.css ){
					inner_popup.css( _i, options.css[ _i ] );
				}
			}

			popup_wrapper.append( inner_pos );
			$('body').append( popup_wrapper ).addClass('noScrolling');

			popup_wrapper.fadeIn('300');

			callback_variables = callback_variables || {};
			if( callback ) callback( callback_variables );

			$('[id^="MCubix-popup-"]').find('.inner-pos').on('click', function(e){
				if( $(this).is( $(e.target) ) )
					self.currentClose();
			});

			return un_id;
		} else {
			if( !options.id ) return false;

			popup_wrapper = $('#MCubix-popup-' + options.id);
			popup_wrapper.fadeOut('300', function(){
				popup_wrapper.remove();
				$('body').removeClass('noScrolling');
			});
		}
	},

	searchData: null,

	initAnim: function( status, options, callback, callback_variables ){
		var self = this,
			un_id, popup_wrapper, closing;
			this.searchData = callback_variables;

		closing = options.closing;
		//console.log( options );

		if( !options.id_length )
			un_id = MCubix.GenerateHash.init( 8 );
		else
			un_id = Number( options.id_length );

		if( status ){
			if( !options.content || $( '#popup-' + options.id ).length > 0 ) return false;

			var inner_pos = $('<div/>', { class: 'inner-pos' }),
					inner_popup = $('<div/>', { class: 'inner-popup' }).addClass('bounceInDown').addClass('animated');

			popup_wrapper = $('<div/>', { id: 'MCubix-popup-anim-' + un_id, 'class': 'custom-api-popup' });

			inner_popup.append( options.content);
			if( closing ){
				var cl_btn = $('<span/>', { 'class': 'close-photo' }).text('×');
				inner_popup.append( cl_btn );

				cl_btn.on('click', function(){
					self.initAnim( false, { id: un_id }, callback, callback_variables );
				})
			}

			inner_pos.append( inner_popup );

			if( options.centered ){
				inner_popup.addClass('centered');
			}

			if( options.fullscreen ){
				inner_popup.addClass('full-screen');
			}

			if( options.padding ) inner_popup.css( 'padding', options.padding );
			if( options.css ){
				for( var _i in options.css ){
					inner_popup.css( _i, options.css[ _i ] );
				}
			}

			popup_wrapper.append( inner_pos );
			$('body').append( popup_wrapper ).addClass('noScrolling');

			popup_wrapper.fadeIn('300');

			$('[id^="MCubix-popup-anim-"]').find('.inner-pos').on('click', function(e){
				if( $(this).is( $(e.target) ) ) {
					self.currentClose( callback, callback_variables );
				}
			});

			return un_id;
		} else {
			if( !options.id ) return false;
			callback_variables = callback_variables || {};

			popup_wrapper = $('#MCubix-popup-anim-' + options.id);
			popup_wrapper.fadeOut('300', function(){
				if( callback ) callback( callback_variables );
				popup_wrapper.remove();
				$('body').removeClass('noScrolling');
			});
		}
	},

	currentClose: function( callback, callback_variables ){
		$('body').find('[id^="MCubix-popup-"]').fadeOut('300', function(){
			callback_variables = callback_variables || {};
			if( callback ) callback( callback_variables );
			$('body').removeClass('noScrolling');
			$(this).remove();
		});
	}
};

/*MCubix.PopupAnim = {
    searchData: null,
    animation: '',
    ajaxData: {},
    content: '',

    init: function( status, options, callback, callback_variables ){
        var self = this,
            un_id, popup_wrapper, closing, content;
        this.searchData = callback_variables;

        closing = options.closing;

        if( !options.id_length )
            un_id = MCubix.GenerateHash.init( 8 );
        else
            un_id = Number( options.id_length );

        if( status ){
            if( $( '#MCubix-popup-anim--' + options.id ).length > 0 ) return false;

            if( options.content ){
                content = options.content;
            } else if( options.ajax && self.validAjax( options.ajax ) ){
                content = self.getAjaxContent( self.ajaxData );
            } else return false;

            self.animation = ( options.animation ) ? '__anim-' + options.animation : '__anim';

            var inner_pos = $('<div/>', { class: 'inner-pos' }),
                inner_popup = $('<div/>', { class: 'inner-popup' }).addClass('bounceInDown').addClass('animated');

            popup_wrapper = $('<div/>', { id: 'MCubix-popup-anim-' + un_id, 'class': 'custom-api-popup' });
            inner_popup.append( content );

            if( closing ){
                var cl_btn = $('<span/>', { 'class': 'close-photo' }).text('×');
                inner_popup.append( cl_btn );

                cl_btn.on('click', function(){
                    self.init( false, { id: un_id }, callback, callback_variables );
                })
            }

            inner_pos.append( inner_popup );

            if( options.centered ){
                inner_popup.addClass('centered');
            }

            if( options.fullscreen ){
                inner_popup.addClass('full-screen');
            }

            if( options.padding ) inner_popup.css( 'padding', options.padding );
            cl_btn.css({ top: inner_popup.css('padding-top'), right: inner_popup.css('padding-right') });

            if( options.css ){
                for( var _i in options.css ){
                    inner_popup.css( _i, options.css[ _i ] );
                }
            }

            popup_wrapper.append( inner_pos );
            $('body').append( popup_wrapper ).addClass('noScrolling');

            if( self.ajaxData.callback ){
                self.ajaxData.callback();
            }

            popup_wrapper.fadeIn('300');
            inner_pos.addClass( self.animation );

            $('[id^="MCubix-popup-anim-"]').find('.inner-pos').on('click', function(e){
                if( $(this).is( $(e.target) ) ) {
                    self.currentClose( callback, callback_variables );
                }
            });

            return un_id;
        } else {
            if( !options.id ) return false;
            callback_variables = callback_variables || {};

            popup_wrapper = $('#MCubix-popup-anim-' + options.id);

            if( self.animation )
                popup_wrapper.find('.inner-pos').removeClass( self.animation );

            popup_wrapper.fadeOut('300', function(){
                if( callback ) callback( callback_variables );
                popup_wrapper.remove();
                $('body').removeClass('noScrolling');
            });
        }
    },

    validAjax: function( ajax_obj ){
        var self = this;
        if( !ajax_obj.url ) return false;

        self.ajaxData.url = ajax_obj.url;
        self.ajaxData.method = ajax_obj.method || 'post';
        self.ajaxData.data = ajax_obj.data || {};
        self.ajaxData.callback = ajax_obj.callback || null;

        if( self.ajaxData.callback ){
            self.ajaxData.callback_vars = ajax_obj.callback_vars || null;
        }

        return true;
    },

    getAjaxContent: function(){
        var self = this,
            content = null;

        if( !self.ajaxData.url ) return false;

        $.ajax({
            url: self.ajaxData.url,
            method: self.ajaxData.method,
            data: self.ajaxData.data,
            async: false,
            beforeSend: function (){
                MCubix.OverlayV2.init(true);
            },
            success: function ( resp ){
                MCubix.OverlayV2.init(false);
                content = $(resp);
            }
        });

        return content;
    },

    currentClose: function( callback, callback_variables ){
        var self = this,
            popup_wrapper = $('[id^="MCubix-popup-"]');

        if( self.animation )
            popup_wrapper.find('.inner-pos').removeClass( self.animation );

        popup_wrapper.fadeOut('300', function(){
            callback_variables = callback_variables || {};
            if( callback ) callback( callback_variables );
            $('body').removeClass('noScrolling');
            $(this).remove();
        });
    }
};*/
				
MCubix.PopupAnim = {
    searchData: null,
    animation: '',
    ajaxData: {},
    content: '',

    init: function( status, options, callback, callback_variables ){
        var self = this,
            un_id, popup_wrapper, closing, content;
        this.searchData = callback_variables;

        closing = options.closing;

        if( !options.id_length )
            un_id = MCubix.GenerateHash.init( 8 );
        else
            un_id = Number( options.id_length );

        if( status ){
            if( $( '#MCubix-popup-anim--' + options.id ).length > 0 ) return false;

            if( options.content ){
                content = options.content;
            } else if( options.ajax && self.validAjax( options.ajax ) ){
                content = self.getAjaxContent( self.ajaxData );
            } else return false;

            self.animation = ( options.animation ) ? '__anim-' + options.animation : '__anim';

            var inner_pos = $('<div/>', { class: 'inner-pos' }),
                inner_popup = $('<div/>', { class: 'inner-popup' }).addClass('animated');

            popup_wrapper = $('<div/>', { id: 'MCubix-popup-anim-' + un_id, 'class': 'custom-api-popup' });
            inner_popup.append( content );

            if( closing ){
                var cl_btn = $('<span/>', { 'class': 'close-photo' }).text('Г—');
                inner_popup.append( cl_btn );

                cl_btn.on('click', function(){
                    self.init( false, { id: un_id }, callback, callback_variables );
                })
            }

            inner_pos.append( inner_popup );

            if( options.centered ){
                inner_popup.addClass('centered');
            }

            if( options.fullscreen ){
                inner_popup.addClass('full-screen');
            }

            if( options.padding ) inner_popup.css( 'padding', options.padding );
			if( closing ){
				cl_btn.css({ top: inner_popup.css('padding-top'), right: inner_popup.css('padding-right') });
			}

            if( options.css ){
                for( var _i in options.css ){
                    inner_popup.css( _i, options.css[ _i ] );
                }
            }

            popup_wrapper.append( inner_pos );
            $('body').append( popup_wrapper ).addClass('noScrolling');

            if( self.ajaxData.callback ){
                if( typeof self.ajaxData.callback_vars !== 'object' || !$.isEmptyObject( self.ajaxData.callback_vars ) ){
                    self.ajaxData.callback( self.ajaxData.callback_vars );
                } else {
                    self.ajaxData.callback();
                }
            }

            popup_wrapper.fadeIn('300');
            inner_pos.addClass( self.animation );

            $('[id^="MCubix-popup-anim-"]').find('.inner-pos').on('click', function(e){
                if( $(this).is( $(e.target) ) ) {
                    self.currentClose( callback, callback_variables );
                }
            });

            return un_id;
        } else {
            if( !options.id ) return false;
            callback_variables = callback_variables || {};

            popup_wrapper = $('#MCubix-popup-anim-' + options.id);

            if( self.animation )
                popup_wrapper.find('.inner-pos').removeClass( self.animation );

            popup_wrapper.fadeOut('300', function(){
                if( callback ) callback( callback_variables );
                popup_wrapper.remove();
                $('body').removeClass('noScrolling');
            });
        }
    },

    validAjax: function( ajax_obj ){
        var self = this;
        if( !ajax_obj.url ) return false;

        self.ajaxData.url = ajax_obj.url;
        self.ajaxData.method = ajax_obj.method || 'post';
        self.ajaxData.data = ajax_obj.data || {};
        self.ajaxData.callback = ajax_obj.callback || null;

        if( typeof self.ajaxData.callback === 'function' ){
            self.ajaxData.callback_vars = ajax_obj.callback_vars || null;
        }

        return true;
    },

    getAjaxContent: function(){
        var self = this,
            content = null;

        if( !self.ajaxData.url ) return false;

        $.ajax({
            url: self.ajaxData.url,
            method: self.ajaxData.method,
            data: self.ajaxData.data,
            async: false,
            beforeSend: function (){
                MCubix.OverlayV2.init(true);
            },
            success: function ( resp ){
                MCubix.OverlayV2.init(false);
                content = $(resp);
			}
        });

        return content;
    },

    currentClose: function( callback, callback_variables ){
        var self = this,
            popup_wrapper = $('[id^="MCubix-popup-"]');

        if( self.animation )
            popup_wrapper.find('.inner-pos').removeClass( self.animation );

        popup_wrapper.fadeOut('300', function(){
            callback_variables = callback_variables || {};
            if( callback ) callback( callback_variables );
            $('body').removeClass('noScrolling');
            $(this).remove();
        });
    }
};

MCubix.GenerateHash = {
	init: function( length ){
		var hash;

		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
					.toString(16)
					.substring(1);
		}

		hash =  s4() + s4() + '-' + s4() + '-' + s4() + '-' +
				s4() + '-' + s4() + s4() + s4();

		if( length && length < 31 ){
			return hash.substr( 0, length );
		}

		return hash;
	}
};

MCubix.OverlayV2 = {
	isBlock: false,
	elm: null,
	init: function( status, elm, load_icon ){
		var self = this,
			overlay, _exist;

		if( elm ) {
			self.isBlock = true;
			self.elm = elm;
		}

		if( !self.elm || !self.isBlock ) {
			overlay = $('#mobileOverlay');

			if( load_icon === false ){
				overlay.css('background-image', 'none');
			}
		} else {
			if ( ! ( self.elm instanceof jQuery ) && ! ( 'jquery' in Object( self.elm ) ) ){
				self.elm = $( self.elm );
			}

			_exist = $('#mobileOverlayBlock');

			if( self.elm.css('position') != 'relative' )
				self.elm.css('position', 'relative');

			if( _exist.length ){ // && !elm
				overlay = _exist;
			} else {
				overlay = $('<div/>', { id: 'mobileOverlayBlock' });
				self.elm.append( overlay );
			}
		}

		if (status) {
			overlay.fadeIn(300);
		} else {
			overlay.fadeOut(300, function(){
				self.isBlock = false;
				self.elm = null;
			});
		}
	},

	/* { message: *, callback: *, callback_variable: *, text_css: { span_color: *hex*, link_color: *hex* } } */
	confirm: function( options ){
		if( !options.callback ) return false;

		var	_link_color = ( options.text_css ) ? options.text_css.link_color : '',
			_span_color = ( options.text_css ) ? options.text_css.span_color : '',
			_confMsg = $('<p/>', { class: 'confirmMsg' }).html( options.message ),
			_contBtn = $('<span/>', { class: 'continueProcess' }).text('Ok'),
			_cancBtn = $('<span/>', { class: 'continueCancel' }).text('Cancel'),
			_btnsWrap = $('<div/>', { class: 'processesPos' }).append( _confMsg ).append( _cancBtn ).append( _contBtn ),
			_btnsPosWrap = $('<div/>', { class: 'processes' }).append(_btnsWrap),
			_confirmBlock = $('<div/>', { id: 'confirmProcess' }).append( _btnsPosWrap );

		if( _link_color ){
			_confMsg.find('span').css('color', _link_color);
		}

		if( _span_color ){
			_confMsg.find('span').css('color', _span_color);
		}

		$('body').append( _confirmBlock );

		_confirmBlock.fadeIn(300, function(){
			_btnsWrap.animate({ top: '35%' }, 200, 'easeOutBack', function(){
				_cancBtn.on('click', function(){
					_btnsWrap.animate({ top: '-50%' }, 200, function(){
						_confirmBlock.fadeOut(300, function(){
							_confirmBlock.remove();
						})
					});
				});

				_contBtn.on('click', function(){
					_btnsWrap.animate({ top: '-50%' }, 200, function(){
						_confirmBlock.fadeOut(300, function(){
							_confirmBlock.remove();
							options.callback( options.callback_variable );
						})
					});
				})
			});
		})
	},

	message: function( options ){
		if( !options.status ) return false;

		var	_confMsg = $('<p/>', { class: 'messageBody' }).html( options.message ),
			_contBtn = $('<span/>', { class: 'statusProcess ' + options.status }),
			_posReal = $('<div/>', { class: 'posReal' }).append( _confMsg ).append( _contBtn ),
			_btnsWrap = $('<div/>', { class: 'processesPos' }).append( _posReal ),
			_btnsPosWrap = $('<div/>', { class: 'processes' }).append(_btnsWrap),
			_confirmBlock = $('<div/>', { id: 'messageProcess' }).append( _btnsPosWrap );

		$('body').append( _confirmBlock );

		_confirmBlock.fadeIn(300, function(){
			_btnsWrap.animate({ bottom : '15%' }, 200, 'easeOutCirc').delay(1000)
				.animate({ bottom: '-50%' }, 200, 'easeInQuint', function(){
					_confirmBlock.remove();
					if( options.callback ){
						options.callback( ( options.callback_variable ) ? options.callback_variable : undefined );
					}
				});
		})
	}
};

MCubix.Agency.Model = {
	body_elm: null,
	escort_id: null,

	init: function( escort_id, body_class ){
		this.body_elm = $('.' + body_class);
		this.escort_id = escort_id;

		this.toVerification();
		this.movePackage();
		//this.editProfile();
		//this.editGallery();
		this.changeStatus();
		this.removeModel();
		this.boosting();
	},

	toVerification: function(){

	},

	movePackage: function( escort_id ){
		var self = this,
			move_btn = $('.options-move-package');

		move_btn.on('click', function(){
			window.location.href = '/private-v2/escort-active-package?escort_id=' + self.escort_id;
			return false;
		});
	},

	boosting: function(){
		var self = this,
			move_btn = $('.options-boost');

		move_btn.on('click', function(){
			$(this).closest('form').submit();
		});
	},

	//editProfile: function(){
	//	var self = this,
	//		_btn = self.body_elm.find('.option-edit');
    //
	//	_btn.on('click', function(e){
	//		e.preventDefault();
	//		window.location.href = '/private-v2/profile?escort=' + self.escort_id;
	//		return false;
	//	})
	//},

	//editGallery: function(){
	//	var self = this,
	//		_btn = self.body_elm.find('.option-edit-gallery');
    //
	//	_btn.on('click', function(e){
	//		e.preventDefault();
	//		window.location.href = '/photo-video?escort=' + self.escort_id;
	//		return false;
	//	})
	//},

	changeStatus: function(){
		var self = this,
			_btn = self.body_elm.find('.status-btn'),
			_status = _btn.attr('data-status');

		_btn.on('click', function(e) {
			MCubix.Agency.Model.Action( self.escort_id, 0, _status );
			MCubix.OverlayV2.init(true);
		})
	},

	removeModel: function(){
		var self = this,
			_btn = self.body_elm.find('.delete-model-btn');

		_btn.on('click', function(e) {
			//alert( self.escort_id );
			MCubix.Agency.Model.Action(self.escort_id, 0, 'delete');
		})
	}
};

MCubix.Collapse = {
	init: function( e ){
		e.collapsify();
	}
};

MCubix.Errors = {
	init: function( msg ){
		//if( msg ) this.showErrors( msg );
	},

	show: function( msg, is_add ){
		if( !msg ) return false;
		var elm = $('#showErrors'),
			inner = elm.find('p');

		if( is_add ){
			msg += ("<br>" + inner.html() );
		}

		if( elm.length > 0 ) {
			if( !elm.find('p').length ) elm.append('</p>');

			elm.fadeIn(300).find('p').html(msg).closest('#showErrors').delay(5000).fadeOut(300, function () {
				$(this).empty();
			});

			return true;
		}

		return false;
	}
};

MCubix.Inform = {
    init: function( msg, is_add, status ){
        if( !status || $.inArray( status, [ 'success', 'warning', 'error' ] ) ) return false;

        if( !msg ) return false;
        var elm = $('#showErrors'),
            inner = elm.find('p');

        elm.addClass('status-' + status);

        if( is_add ){
            msg += ("<br>" + inner.html() );
        }

        if( elm.length > 0 ) {
            if( !elm.find('p').length ) elm.append('</p>');

            elm.fadeIn(300).find('p').html(msg).closest('#showErrors').delay(5000).fadeOut(300, function () {
                $(this).empty();
            });

            return true;
        }

        return false;
    }
};

MCubix.Inputs.StyleMaterial = {
	init: function(){
		var tr_inputs = $('.innerInput.tr').find('input[type="text"]');

		tr_inputs.on('focus', function(){
			$(this).closest('.innerInput').find('label').addClass('active');
		});

		tr_inputs.on('blur', function(){
			var __parent = $(this).closest('.innerInput');

			if( !$(this).val() ) {
				__parent.find('label').removeClass('active');
			}
		});
	}
};

MCubix.URI = {
    redirect: function( url ){
        window.location.href = url;
    }
};

MCubix.VCEcardonPopup = function(checkoutId) {
	
	MCubix.PopupAnim.init(true, {
		ajax: {
			url: '/escorts/ecardon-popup',
			method: 'GET',
			callback: MCubix.VCEcardonPopupCallback,
			callback_vars: { checkoutId: checkoutId}
		},
		css:{
			width: '90%',
			'font-size': '1.2em'
		}, 
		centered: true,
		closing: false
	});


};

MCubix.VCEcardonPopupCallback = function(data) {

	$.ajaxSetup({cache:true});
	var script = document.createElement("script");
	script.src = 'https://oppwa.com/v1/paymentWidgets.js?checkoutId=' + data.checkoutId;
	$('head').append(script);
	var paymetnOptions = '<script>var wpwlOptions = { locale: "<?= $this->lang() ?>", onBeforeSubmitCard: function(){ '+ 
			'if ( $(".wpwl-control-cardHolder").val() === "" ){' +
				'$(".wpwl-control-cardHolder").addClass("wpwl-has-error"); ' +
				'$(".wpwl-control-cardHolder").after("<div class=\'wpwl-hint wpwl-hint-cardHolderError\'>Invalid card holder</div>"); ' +
				'$(".wpwl-button-pay").addClass("wpwl-button-error").attr("disabled", "disabled");return false;}' +
			'else return true; }} </' + 'script>';
	$('head').before(paymetnOptions);
};

/* $(document).ready(function(){
	var _overlays = $('#mobileOverlay').add('#mobileOverlayBlock');

	_overlays.on('click', function(e){
		var target = $(e.target );

		if( target.is('#mobileOverlay') || target.is('#mobileOverlayBlock') ){
			MCubix.OverlayV2.init( false );
		}
	});
}); */

$(document).ready(function() {
    //if( typeof MobileChat === 'undefined' ) return false;

    var chat_icon = $('.chat-icon');
    if( chat_icon.length ){
        chat_icon.on('click', function (e) {

            if( typeof MobileChat !== 'undefined' ) {
                MobileChat.Lists.openChat();
            }

            e.preventDefault();
            if ($(this).attr('data-success') && $(this).attr('data-success') === 'ok') {
                $('#chat-wrapper').removeClass('hide');
                if( $(this).find('.new_messages_count') ){
                    $(this).find('.new_messages_count').remove();
                }
            } else if (!$(this).attr('href') && $(this).attr('data-chat-msg')) {
				showChatNotice( $('#chat-notice'), $(this).attr('data-chat-msg') );
            } else if ($(this).attr('href')) {
                window.location.href = $(this).attr('href');
            }

            if( typeof MobileChat !== 'undefined' ) {
                MobileChat.Lists.sortingConversations();
                MobileChat.Lists.searchInConversations();
            }
        });
    }

    function showChatNotice(el, msg){
    	if( !el.hasClass('show') ){
            var _width = $('#page').outerWidth() - 81;

            el.animate({'width': _width}, 200, function(){
                el.addClass('show').text(msg).css('padding', '5px 10px');

            	setTimeout(function(){
                    el.animate({'width': 0}, 300, function(){
                    	el.empty().removeClass('show').css('padding', '0');
					});
				}, 5000)
			});

		} else return false;
	}

    var chat_online = $('#chatOnline');
    if( chat_online.length ){
        chat_online.on('click', function(e){
            var user_id = parseInt( $(this).attr('data-id') );
            MobileChat.Lists.openChat();

            e.preventDefault();

            $('#chat-wrapper').removeClass('hide');
            var elm = $('#user_' + user_id);

            if( elm.length ){
                elm.trigger('mouseup');
            }
        });
    }
});

navigator.sayswho = function () {
    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];

    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        //return 'IE ' + (tem[1] || '');
        return { name: 'IE ', version: (tem[1] || '') };
    }

    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/);
        //if (tem != null) return 'Opera ' + tem[1];
        if (tem != null) { return { name: 'Opera', version: tem[1] }; }
    }

    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];

    if ((tem = ua.match(/version\/(\d+)/i)) != null) {
        M.splice(1, 1, tem[1]);
    }

    return M;
};