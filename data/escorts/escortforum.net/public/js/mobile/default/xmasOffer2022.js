Cubix.XmasOffer = {};

Cubix.XmasOffer.inProcess = false;

Cubix.XmasOffer.url = '';

Cubix.XmasOffer.Show = function () {
    if (Cubix.XmasOffer.inProcess) return false;

    $('#XmasOfferPopup').addClass('open');

    Cubix.XmasOffer.inProcess = true;

    $('#xmas-offer-cancel-btn').on('click', function() {
        $('#XmasOfferPopup').remove();
    });

    return false;
}