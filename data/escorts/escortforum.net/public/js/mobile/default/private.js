var Controls = {};

/*Controls.Tip = new Class({
	Implements: [Options, Events],

	options: {
		
	},

	initialize: function () {
		this.render();
		this.bind = {
			enter: this.mouseenter.bindWithEvent(this),
			leave: this.mouseleave.bindWithEvent(this)
		};
	},

	render: function () {
		this.tip = new Element('div', { 'class': 'cx-tip' }).inject($(document.body));
	},

	attach: function (el) {
		el.addEvents({
			mouseenter: this.bind.enter,
			mouseleave: this.bind.leave
		});
//		this.mouseenter({ target: el });
	},

	mouseenter: function (e) {
		var l = this.el = e.target;
		var pos = this.el.getPosition($(document.body));

		var t = this.tip;
		if ( ! l.retrieve('tip') ) {
			l.store('tip', l.get('title')).set('title', '');
		}
		
		t.set('html', l.retrieve('tip'));
		t.setStyles({
			left: pos.x + 22,
			top: pos.y - 22,
			display: 'block'
		});
	},

	mouseleave: function (e) {
		this.tip.setStyle('display', 'none');
	}
});

Controls.LngPicker = new Class({
	Implements: [Options, Events],

	options: {

	},

	el: null,
	langs: [],

	initialize: function (el, options) {
		this.setOptions(options || {});
		
		this.el = el;
		this.langs = this.el.getElements('span');
		
		this.bind = {
			click: this.click.bindWithEvent(this)
		};

		this.attach();
		
		this.click({}, this.langs[0]);
	},

	attach: function () {
		this.langs.each(function (lng) {
			lng.addEvent('click', this.bind.click.bindWithEvent(this, [lng]));
		}.bind(this));
	},

	click: function (e, el) {
		this.langs.removeClass('active');
		el.addClass('active');

		this.fireEvent('change', [el]);
	}
});

Controls.SelectBox = new Class({
	Implements: [Options, Events],

	options: {
		disabled: false,
		name: '',
		values: [
			{ value: 1, label: 'EUR' },
			{ value: 2, label: 'USD' },
			{ value: 3, label: 'GBP' },
			{ value: 4, label: 'CHF' }
		],
		selected: 1
	},

	element: {},
	list: {},
	input: {},

	selected: {},

	disable: function () {
		this.element.addClass('cx-selectbox-disabled');
	},

	enable: function () {
		this.element.removeClass('cx-selectbox-disabled');
	},

	getValue: function (value) {
		var found = false;
		this.options.values.each(function (i) {
			if ( found ) return;
			if ( value == i.value ) {
				found = i;
			}
		});

		return found;
	},

	initialize: function (o) {
		this.setOptions(o || {});
		
		this.render();

		this.bind = {
			mouseenter: this.onMouseEnter.bindWithEvent(this),
			mouseleave: this.onMouseLeave.bindWithEvent(this),
			click: this.onClick.bindWithEvent(this),
			select: this.onSelect.bindWithEvent(this),
			hide: this.onHide.bindWithEvent(this)
		};

		this.attach();

		if ( this.options.disabled ) {
			this.disable();
		}
	},

	render: function () {
		this.element = new Element('span', {
			'class': 'cx-selectbox'
		});

		this.current = new Element('em', {
			html: this.getValue(this.options.selected).label
		}).inject(this.element);

		this.list = new Element('div', {
			'class': 'cx-selectbox-list hide'
		}).inject(this.element);

		this.options.values.each(function (i, j) {
			var el = new Element('div', {
				html: i.label
			}).inject(this.list);
			el.store('value', i);
			if ( j == 0 ) {
				el.addClass('selected');
			}
		}.bind(this));

		this.input = new Element('input', {
			type: 'hidden',
			name: this.options.name,
			value: this.getValue(this.options.selected).value
		}).inject(this.element);
	},

	onMouseEnter: function (e) {

	},

	onMouseLeave: function (e) {

	},

	onClick: function (e) {
		if ( this.element.hasClass('cx-selectbox-disabled') ) return;
		e.stop();
		if ( this.list.hasClass('hide') ) {
			this.element.addClass('cx-selectbox-pushed');
			this.list.removeClass('hide');
			$(document.body).addEvent('click', this.bind.hide);
		}
		else {
			this.onHide({});
		}
	},

	onSelect: function (e) {
		this.list.getElements('div').removeClass('selected');
		this.select(e.target);
	},

	onHide: function (e) {
		this.element.removeClass('cx-selectbox-pushed');
		this.list.addClass('hide');
		$(document.body).removeEvent('click', this.bind.hide);
	},

	attach: function () {
		this.element.addEvents({
			mouseenter: this.bind.mouseenter,
			mouseleave: this.bind.mouseleave,
			click: this.bind.click
		});

		this.list.getElements('div').addEvents({
			click: this.bind.select
		});
	},

	select: function (el) {
		var value = el.retrieve('value');
		this.input.set('value', value.value);
		this.current.set('html', value.label);
		this.selected = value;
		el.addClass('selected');
	},

	getElement: function () {
		return this.element;
	},

	privateArea: function () {
		window.location.href = '.';
	}
});*/

$(document).ready( function () {
	//var tip = new Controls.Tip;
    //
	//$('.form span.tip').each(function (_el) {
	//	tip.attach( $(this) );
	//});

	//var maskable = $$('.cbox-ii .gray-box');
	//if ( ! maskable.length ) {
	//	maskable = $$('.agency-logo-wrapper');
	//}
    //
	//if ( maskable.length ) {
	//	Navigation.overlay = new Cubix.Overlay(maskable[0], {
	//		offset: {
	//			left: -1,
	//			top: -1
	//		}
	//	});
	//}
	////Locker.init();
	//Locker.go();
	//Navigation.init();
	//FormErrors.show();
});

/*var Navigation = {
	form: null,
	step: null,
	then: null,
	changed: false,
	initial: null,
	
	init: function () {
		var _form = $('.form');
		
		if ( !_form.length )
			return;
		this.form = _form;
		this.step = this.form.find('input[name=step]');
		this.then = this.form.find('input[name=then]');
		this.dont = this.form.find('input[name=dont]');
		

		this.initial = this.form.serialize();

		this.overlay = Navigation.overlay ? Navigation.overlay : new Cubix.Overlay( this.form.closest('.cbox-ii').find('.gray-box:first'), {
			offset: {
				left: -1,
				top: -1
			}
		});
	},

	enable: function () {
		//this.overlay.enable();
	},

	disable: function () {
		//this.overlay.disable();
	},

	back: function () {
		//this.overlay.disable();
		this.then.val(':back');
		this.form.submit();
		return false;
	},

	next: function () {
		//this.overlay.disable();
		this.then.val(':next');
		this.form.submit();
		return false;
	},

	go: function (step) {
		//this.overlay.disable();
		this.then.val(step);
		this.form.submit();
		return false;
	},

	reload: function () {
		//this.overlay.disable();
		this.dont.val(1);
		this.form.submit();
		return false;
	},

	update: function () {
		if ( typeof(tineMCE) != "undefined" ) tinyMCE.triggerSave();
		//this.overlay.disable();
		this.form.submit();
		return false;
	},

	reset: function () {
		//this.overlay.disable();
		window.location.href = window.location.href;
		return false;
	},

	tab: function (tab) {
		if ( ! this.hasChanged() && ! new Hash(FormErrors.errors).getLength() ) {
			this.overlay.disable();
			return true;
		}

		if ( ! confirm('If you continue, all the changes you\'ve made will be lost!\n\nClick "Cancel" to stay on this page and click update to save current tab') ) {
			return false;
		}
		
		//this.overlay.disable();
		return true;
	},

	privateArea: function () {
		window.location.href = '.';
	},

	hasChanged: function () {
		return this.form.serialize() != this.initial;
	}
};*/

/*var FormErrors = {
	errors: {},
	
	init: function (errors) {
		this.errors = errors || {};
	},
	
	show: function () {
		for ( var field in this.errors ) {
			var el = $$('*[name=' + field + ']')[0];
			if ( ! el ) continue;
			var inner = el.getParent('.inner');
			var clear = inner.getLast('.clear');
			new Element('div', { 'class': 'error', html: this.errors[field] }).inject(clear ? clear : inner, clear ? 'before' : 'bottom');
		}
	}
};

var Locker = {
	cHeight:0,
	rHeight:0,
	cStatus:0,
	rStatus:0,
	
	init:function(){
		// COMMENT
		var commentBox = $('my-comments');
		
		if($defined(commentBox)){
			this.cHeight = commentBox.getStyle('height');
		
			cStatus = Cookie.read('comment_locker_status');
			
			if(cStatus == 1){
				this.cStatus = cStatus;
				commentBox.getPrevious('.locker').addClass('closed');
				commentBox.setStyle('height',0);
			}
		}
		
		// REVIEW
		var reviewBox = $('my-reviews');
		
		if($defined(reviewBox)){
			this.rHeight = reviewBox.getStyle('height');
			
			rStatus = Cookie.read('review_locker_status');
			if(rStatus == 1){
				this.rStatus = rStatus;
				reviewBox.getPrevious('.locker').addClass('closed');
				reviewBox.setStyle('height',0);
			}
		}
	},
		
	go: function(){
		var self = this;
		$$('.locker').addEvent('click', function(){
			var box = this.getNext('div');
			var locker = this;
			var height, autoHeight, cookieName;
			
			// COMMENT
			if(box.get('id') == "my-comments"){
				autoHeight = self.cHeight;
				cookieName = 'comment_locker_status';
			}
			
			// REVIEW
			if(box.get('id') == "my-reviews"){
				autoHeight = self.rHeight;
				cookieName = 'review_locker_status';
				
			}
			
			
			if(locker.hasClass('closed')){
				height = autoHeight;
				locker.removeClass('closed');
				Cookie.write(cookieName, 0, {duration: 365});
			}
			else{
				height = 0;
				locker.addClass('closed');
				Cookie.write(cookieName, 1, {duration: 365});
			}
			var effect = new Fx.Tween(box,  {
							duration: 1000,
							transition:  Fx.Transitions.Quart.easeInOut
							
			});
			//console.log(height);
			effect.start('height', height);
		});
	}

};*/

var Geography = {
	loadCities: function (city, country, city_id, hidden, lang_id) {

		Cubix.Lang.id = ( lang_id ) ? lang_id : '';

		if ( typeof hidden == 'undefined' ) {
			hidden = false;
		}

		var selected, country_id, _country = $(country);

		if ( hidden ) {
			selected = _country.val();
		} else {
			selected = _country.find(':selected');
		}
		
		if ( ! selected.length ) return;

		if ( ! hidden ) {
			country_id = selected.val();
		} else {
			country_id = selected;
		}

		if ( ! country_id ) {
			var _opt = $('<option></option>').val('').html('- city -');
			city.empty();
			city.append( _opt );
			return;
		}

		_country.add(city).attr('disabled', 'disabled');

		$.ajax({
			url: /*Cubix.Lang.id + */'/en/geography/ajax-get-cities',
			data: { country_id: country_id },
			type: 'GET',
			success: function(_resp) {
				var resp = $.parseJSON(_resp),
					__opt;
				if ( ! resp ) return;

				var _opt = $('<option></option>').val('').html('- city -');
				city.empty();
				city.append( _opt );

				$.each(resp.data, function (i, c) {
					var _issel = ( city_id == c.id );
					__opt = $('<option></option>').val(c.id).html(c.title + '&nbsp;&nbsp;').prop( 'selected', _issel );
					console.log( city );
					city.append( __opt );
				});
				_country.add(city).removeAttr('disabled');
			}
		});
	}
};

var changeSystem = function (sys) {
	$$('input[name=measure_units]').set('value', sys);
	return Navigation.reload();
};

var LoadEscortComments = function (params) {

		var comments_container = $('#ajax-escort-comments');
		$('#my-comments').css('height', '');

		//var overlay = new Cubix.Overlay(comments_container, {
		//	loader: _st('loader-small.gif'),
		//	position: '50%'
		//});
		//overlay.disable();

	$.ajax({
		url: "/private-v2/ajax-escort-comments",
		type: 'POST',
		data: params,
		success: function(resp) {
			comments_container.html('');
			comments_container.html(resp);
			Locker.cHeight = $('#my-comments').css('height');
		}
	});

		/*new Request({
			url:  "/private-v2/ajax-escort-comments",
			method: 'post',
			evalScripts: true,
			data: params,
			onSuccess: function ( resp ) {
				comments_container.html('html', '');
				comments_container.html('html', resp);
				Locker.cHeight = $('#my-comments').css('height');
			}
		}).send();*/

		return false;
};

var LoadEscortReviews = function (params) {

	var reviews_container = $('#ajax-escort-reviews');
	$('#my-reviews').css('height','');

	//var overlay = new Cubix.Overlay(reviews_container, {
	//	loader: _st('loader-small.gif'),
	//	position: '50%'
	//});
	//overlay.disable();

	//new Request({
	//	url:  "/private-v2/ajax-escort-reviews",
	//	method: 'post',
	//	evalScripts: true,
	//	data: params,
	//	onSuccess: function (resp) {
	//		reviews_container.set('html','');
	//		reviews_container.set('html',resp);
	//		Locker.rHeight = $('my-reviews').getStyle('height');
    //
	//	}
	//}).send();

	$.ajax({
		url: "/private-v2/ajax-escort-comments",
		type: 'POST',
		data: params,
		success: function(resp) {
			comments_container.html('');
			comments_container.html(resp);
			Locker.cHeight = $('#my-comments').css('height');
		}
	});

	return false;
};

/* START :: INDEX PAGE */
var AgencyList = {};

AgencyList.Escorts = {
	liveSearch: function () {
		var self = this;
		var searchInput = $('#escort-search');

		searchInput.on('keyup', function(e){
			self.filter(searchInput);
		});
	},

	filter: function ( search ){
		var searchQuery = search.val().toLowerCase();

		$('#agencyEscorts .p-escort').each( function(_el) {
			var el = $(this);

			var escortShowname = el.find('.escort-showname').html().toLowerCase();
			var escortId = el.find('.escort-id').html();
			if (escortShowname.indexOf(searchQuery) == -1 && escortId.indexOf(searchQuery) == -1 ){
				el.addClass('hidden');
			}else{
				if ( el.hasClass('hidden') )
					el.removeClass('hidden');
			}
		});
	},

	liveSearchCustom: function ( input_search, block, options ) {
		var self = this,
			searchInput;

		searchInput = ( !input_search ) ? $('#escort-search') : $( input_search );

		searchInput.on('keyup', function(e){
			self.filterCustom( searchInput, block, options );
		});
	},

	filterCustom: function( searchInput, block, options ){
		if( !searchInput || !block || !options ) return false;

		var searchQuery = searchInput.val().toLowerCase(),
			_block = $( block );

		_block.each( function(_el) {
			var el = $(this),
				escortShowname = el.find( options.shownameElement ).attr('data-showname').toLowerCase(),
				escortId = el.find( options.idElement ).attr('data-id');

			if (escortShowname.indexOf(searchQuery) == -1 && escortId.indexOf(searchQuery) == -1 ){
				el.addClass('hidden');
			}else{
				if ( el.hasClass('hidden') )
					el.removeClass('hidden');
			}
		});
	}
};
