Cubix.premiumEditPopup = {};

Cubix.premiumEditPopup.inProcess = false;

Cubix.premiumEditPopup.url = '';

Cubix.premiumEditPopup.Show = function () {
	if ( Cubix.premiumEditPopup.inProcess ) return false;


	$('#premium-edit-popup').addClass('open');

	Cubix.premiumEditPopup.inProcess = true;

	return false;
};
Cubix.premiumEditPopup.hide = function () {
	$('#premium-edit-popup').removeClass('open');
	// $('#tour-edit').remove();

	Cubix.premiumEditPopup.inProcess = false;

	return false;
};
