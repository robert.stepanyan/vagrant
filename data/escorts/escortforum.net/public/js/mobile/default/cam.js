Cubix.Cam = {};
Cubix.Cam.path = 'ef-cams';
//Cubix.Cam.iframeDomain = 'https://projectitalian.thecamapi.dev';
//Cubix.Cam.iframeDomain = 'https://projectitalian.thecamapi.com';
Cubix.Cam.InitListeners = function(){
	window.addEventListener("message", function(message){
				
		let {action, data}  = message.data; 
		
		if(action == 'height_change'){
			if(data.height == 'full_page'){
				let headerHeight = $('header').getSize().y;
				let footerHeight = $('footer').getSize().y;
				let contentHeight = document.documentElement.clientHeight - headerHeight - footerHeight;
				$('iframe-wrapper').setStyle('height', contentHeight);
			}else{
				$('iframe-wrapper').setStyle('height', data.height);
			}
		}
		else if(action == 'url_change'){
			let reg = new RegExp(Cubix.Cam.path + '(\/)?', "g");
			let urlPath = window.location.pathname.replace(reg, "");
			window.history.pushState(data.url, "", '/' + Cubix.Cam.path + data.url);
			//window.history.pushState(data.url, "", data.url);
		}
		else if(action == 'open_reg' || action == 'open_login'){
			Cubix.Cam.Popup.url = '/ef-cams/login/';
			Cubix.Cam.Popup.Show(400,345);
		}
		else if(action == 'open_purchase'){
			Cubix.Cam.Popup.url = '/ef-cams/buy-tokens/';
			Cubix.Cam.Popup.Show(513,375);
		}else if(action == 'balance'){
			if($('balance')){
				$('balance').set('html',Math.round(data.balance));
			}
		}
		else{
			console.log(message.data);
		}
	});
}

Cubix.Cam.SendLoginDetails = function(){
	const iframe = $('iframe');
	if(iframe){
		iframe.addEvent('load', function(){
			if($('cam-token')){
				let token = $('cam-token').get('value');
				this.contentWindow.postMessage({"action": "login", "data": {"token": token}}, "*");
			}
		});
	}
};

Cubix.Cam.initHeaderEvents = function(){
	
	Cubix.Cam.Overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	$('menu').addEvent('click',function(){
		$(this).toggleClass('open');
		$('navigation').toggleClass('show');
	});

	$$('.not-authorized').addEvent('click', function(e){
		e.stop();
		Cubix.Cam.Popup.url = '/ef-cams/login/';
		Cubix.Cam.Popup.Show();
	});

	$$('.buy-tokens').addEvent('click', function(e){
		e.stop();
		Cubix.Cam.Popup.url = '/ef-cams/buy-tokens/';
		Cubix.Cam.Popup.Show();
	});
}

Cubix.Cam.Popup = {};
Cubix.Cam.Popup.inProcess = false;
Cubix.Cam.Popup.Show = function () {
	if ( Cubix.Cam.Popup.inProcess ) return false;

	Cubix.Cam.Overlay.disable();

	var container = new Element('div', { 'id': 'cam-popup'}).setStyles({
		
	}).inject(document.body);
		
	Cubix.Cam.Popup.inProcess = true;
		
	new Request({
		url: Cubix.Cam.Popup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.Cam.Popup.inProcess = false;
			container.set('html', resp);
			var close_btn = new Element('span', {class: 'close-icon'}).inject(container);
					
			close_btn.addEvent('click', function() {
				$('cam-popup').destroy();
				Cubix.Cam.Overlay.enable();
			});
			
			if($('go-signup-button')){
				$('go-signup-button').addEvent('click', function(){
					close_btn.fireEvent('click');
					Cubix.Cam.Popup.url = '/ef-cams/signup/';
					Cubix.Cam.Popup.Show(475,375);
				});
			}
			
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.Cam.Popup.Submit);
			
			container.tween('opacity', '1');
		}
	}).send();
		
	
	return false;
}
Cubix.Cam.Popup.Submit = function (e) {
	e.stop();
	   
	var popupOverlay = new Cubix.Overlay($('popup-container'), { color: '#fdfdfd', opacity: .6, loader: '/img/photo-video/ajax_test.gif' });
	popupOverlay.disable();
	this.set('send', {
		
		onSuccess: function (resp) {
			resp = JSON.decode(resp);
			
			if(resp.status == 'error'){
				popupOverlay.enable();
				$$('#cam-popup .error-notes').destroy();
				$$('#cam-popup .req').removeClass('req');
				
				function getErrorElement(el) {
					var target = el;
					if ( el.get('name') == 'terms' ) {
						target = el.getNext('label');
					}
					return new Element('div', { 'class': 'error-notes' }).inject(target, 'after');
				}

				for ( var field in resp.msgs ) {
					var input = this.getElement('*[name=' + field + ']');
					input.addClass('req');
					getErrorElement(input).set('html', resp.msgs[field]);
				}
			}
			else if(resp.status == 'ecardon_success'){
				Cubix.Cam.Popup.Close();
				Cubix.Cam.Popup.ecardonPopup(resp.checkoutId);
			}
			else if(resp.status == 'twispay_success'){
				var formHtml = resp.form;
				$$('#header')[0].set('html', $$('#header')[0].get('html') + formHtml.replace(/ \"/g, "\""));
				$('twispay-payment-form').submit();
			}
			else if(resp.status == 'payment_failure'){
				alert('Unexpected error please try later');
			}
			else if(resp.signup == true){
				window.location.href = '/private/signup-success';
				return;
			}
			else{
				window.location.reload();
				return;
			}
		}.bind(this)
	});

	this.send();
}
Cubix.Cam.Popup.Send = function (e) {
	e.stop();
	   
	Cubix.Cam.Overlay.disable();

	this.set('send', {
		onSuccess: function (resp) {
			$('video-chat-popup').destroy();
			resp = JSON.decode(resp);
			if(resp.status == 'success'){
				Cubix.Cam.Popup.ecardonPopup(resp.checkoutId);
			}
			else{
				alert('Unexpected error please try later');
			}
			Cubix.Cam.Overlay.enable();
		}.bind(this)
	});

	this.send();
}

Cubix.Cam.Popup.Send = function (e) {
	e.stop();
	   
	Cubix.Cam.Overlay.disable();

	this.set('send', {
		onSuccess: function (resp) {
			$('video-chat-popup').destroy();
			resp = JSON.decode(resp);
			if(resp.status == 'success'){
				Cubix.Cam.Popup.ecardonPopup(resp.checkoutId);
			}
			else{
				alert('Unexpected error please try later');
			}
			Cubix.Cam.Overlay.enable();
		}.bind(this)
	});

	this.send();
}

Cubix.Cam.Popup.Close = function () {
	$('cam-popup').destroy();
	Cubix.Cam.Overlay.enable();
};

Cubix.Cam.Popup.ecardonPopup = function(checkoutId) {
	
	var container = new Element('div', {'class': 'ecardon-popup'}).setStyles({
		
	}).inject(document.body);
	
	
	Cubix.Cam.Overlay.disable();
	
	new Request({
		url: '/ef-cams/ecardon-popup',
		method: 'get',
		onSuccess: function (resp) {
			container.set('html', resp);
			
			container.setStyle('opacity', 1);
			new Element('script', {
				text: 'var wpwlOptions = { locale: "' + Cubix.Lang.id + '", style: "plain", onBeforeSubmitCard: function(){ if ( $$(".wpwl-control-cardHolder").get("value") == "") { $$(".wpwl-control-cardHolder").addClass("wpwl-has-error"); $$(".wpwl-button-pay").addClass("wpwl-button-error").set("disabled", "disabled"); new Element("div", {"class": "wpwl-hint wpwl-hint-cardHolderError", "text": "card holder required"}).inject($$(".wpwl-control-cardHolder")[0], "after");  return false} else return true}}'
			}).inject(container, 'before');
			
			new Element('script', {
				src: "https://oppwa.com/v1/paymentWidgets.js?checkoutId=" + checkoutId
			}).inject(container, 'before');
		}
	}).send();
	
	$$('.overlay').addEvent('click', function(){
		container.destroy();
		this.destroy();
	});
};

window.addEvent('domready', function(){
	Cubix.Cam.InitListeners();
	//Cubix.Cam.SendLoginDetails();
	Cubix.Cam.initHeaderEvents();
	
	window.addEventListener('popstate', function(event){
		let iframe = $('iframe');
		if(iframe){
			iframe.contentWindow.postMessage({"action": "navigate", "data": {"url": event.state}}, "*");
		}
	});
});

