/**
 * Created by Karo on 16.06.2016.
 * Custom code / testing status OK !
 * !!! Important - HTML5 class is defined in define.js
 */

HTML5.ImageUpload = {
    fileInput: null,
    uploadUri: null,

    init: function( input, url ){
        if( !input || !url ){
            alert( 'No set input / url' );
            return false;
        }

        this.fileInput = input;
        this.uploadUri = url;

        if( this.isUploadSupported() ){
            this.imageValidation();
        }
    },

    isUploadSupported: function(){
        if ( navigator.userAgent.match(/(Android (1.0|1.1|1.5|1.6|2.0|2.1))|(Windows Phone (OS 7|8.0))|(XBLWP)|(ZuneWP)|(w(eb)?OSBrowser)|(webOS)|(Kindle\/(1.0|2.0|2.5|3.0))/)) {
            return false;
        }

        if( ( this.fileInput.attr("type") != 'file' ) && this.fileInput.attr('disabled') ){
            return false;
        }

        return true;
    },

    imageValidation: function(){
        var self = this;
        if (window.File && window.FileReader && window.FormData) {

            self.fileInput.on('change', function (e) {
                var file = e.target.files[0];

                if (file) {
                    if (/^image\//i.test(file.type)) {
                        self.readFile(file);
                    } else {
                        alert('Not a valid image!');
                    }
                }
            });
        } else {
            alert("File upload is not supported!");
        }
    },

    readFile: function(file){
        var reader = new FileReader(),
            self = this;

        reader.onloadend = function () {
            self.processFile(reader.result, file.type);
        };

        reader.onerror = function () {
            alert('There was an error reading the file!');
        };

        reader.readAsDataURL(file);
    },

    processFile: function( dataURL, fileType ){
        // dataURL, fileType
        var maxWidth = 80,
            maxHeight = 80,
            self = this;

        var image = new Image();
        image.src = dataURL;

        image.onload = function () {
            var width = image.width;
            var height = image.height;
            var shouldResize = (width > maxWidth) || (height > maxHeight);

            if (!shouldResize) {
                self.sendFile(dataURL);
                return;
            }

            var newWidth;
            var newHeight;

            if (width > height) {
                newHeight = height * (maxWidth / width);
                newWidth = maxWidth;
            } else {
                newWidth = width * (maxHeight / height);
                newHeight = maxHeight;
            }

            var canvas = document.createElement('canvas');

            canvas.width = newWidth;
            canvas.height = newHeight;

            //console.log( newWidth, newHeight ); return;

            var context = canvas.getContext('2d');

            context.drawImage(this, 0, 0, newWidth, newHeight);

            dataURL = canvas.toDataURL(fileType);

            $('.agency-logo-wrapper').append(canvas);

            self.sendFile(dataURL);
        };

        image.onerror = function () {
            alert('There was an error processing your file!');
        };
    },

    sendFile: function(fileData){
        var formData = new FormData(),
            self = this;

        formData.append('imageData', fileData);
        formData.append('ajax', 1);
        formData.append('a', 'update');

        $.ajax({
            type: 'POST',
            url: self.uploadUri,
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                data = $.parseJSON( data );

                if ( data.success ) {
                    alert('Your file was successfully uploaded!');
                } else {
                    alert('There was an error uploading your file!');
                }
            },
            error: function () { // data
                alert('There was an error uploading your file!');
            }
        });
    }
};