Cubix.PowerCash = {};
Cubix.PowerCash.init = function (url) {
    Cubix.PowerCash.inProcess = false;
    $(document).ready(function () {
        var popup = "<div id='powercash-popup-m' style='background-color: transparent'></div>";
        $('body').append(popup);
        var iframe = '<iframe scrolling="no"; frameborder = 0; src="' + url + '" ></iframe>>';
        $('#powercash-popup-m').append(iframe);
        $('#powercash-popup-m').addClass('open');
    });

    return true
};


