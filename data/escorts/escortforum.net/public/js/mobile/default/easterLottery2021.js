Cubix.Easterlottery = {};

Cubix.Easterlottery.inProcess = false;

Cubix.Easterlottery.url = '';

Cubix.Easterlottery.Show = function (arg) {
	if ( Cubix.Easterlottery.inProcess ) return false;

	if (arg === 'winner')
	{
		$('#EasterWinnerPopup').addClass('open');
		$('#EasterPopup').remove();
	}else{
		$('#EasterPopup').addClass('open');
		$('#EasterWinnerPopup').remove();
	}


	Cubix.Easterlottery.inProcess = true;

	$('#EasterButton').on('click', function() {
		$('#EasterPopup').remove();
		$('#EasterWinnerPopup').remove();
	});

	return false;
}