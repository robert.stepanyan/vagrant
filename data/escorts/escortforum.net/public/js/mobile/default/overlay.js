//$.Overlay = function(){};

Cubix.Overlay = {
	//Implements: Options,

	options: {
		opacity: .8,
		color: '#fff',
		loader: _st('loader-big.gif'),
		position: '140px',
		has_loader: true,
		overlay_class: 'overlay',
		no_relative: false,
		offset: {
			left: 0,
			top: 0,
			right: 0,
			bottom: 0
		},
		z_index: 100
	},

	element: null,
	overlay: null,

	initialize: function (element, options) {
		this.element = $(element);

		this.setOptions(options || {});

		if ( ! this.options.no_relative ) {
			this.element.css('position', 'relative');
		}

		this.overlay = this.render();
		this.overlay.append(element);
	},

	render: function () {
		var overlay = $('div')
				.css({
					'background-color': this.options.color,
					position: 'absolute',
					left: this.options.offset.left,
					top: this.options.offset.top,
					zIndex: this.options.z_index,
					opacity: this.options.opacity
				});

		overlay.addClass(this.options.overlay_class);

		if ( this.options.has_loader ) {
			overlay.css({
				'background-repeat': 'no-repeat',
				'background-position': '50% ' + this.options.position,
				'background-image': 'url(' + this.options.loader + ')'
			});
		}

		return overlay;
	},

	enable: function () {
		this.overlay.addClass('overlay-hidden');
	},

	disable: function () {
		this.overlay.removeClass('overlay-hidden');
		this.fit();
	},

	fit: function () {
		var el = this.element;

		this.overlay.css({
			width: el.getWidth() + this.options.offset.right,
			height: el.getScrollHeight() + this.options.offset.bottom
		});
	}
};

//Cubix.Overlay = new $.Overlay();
