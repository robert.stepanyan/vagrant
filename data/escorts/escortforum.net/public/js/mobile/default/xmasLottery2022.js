Cubix.XmasLottery = {};

Cubix.XmasLottery.inProcess = false;

Cubix.XmasLottery.url = '';

Cubix.XmasLottery.Show = function () {
    if (Cubix.XmasLottery.inProcess) return false;

    $('#XmasWinnerPopup').addClass('open');

    Cubix.XmasLottery.inProcess = true;

    $('#xmas-understand').on('click', function () {
        $.ajax({
            type: 'POST',
            url:  "/private-v2/set-xmas-popup-showed",
            success: function (data) {
                $('#XmasWinnerPopup').remove();
            },
            error: function () {
                $('#XmasWinnerPopup').remove();
            }
        });
    });

    return false;
}