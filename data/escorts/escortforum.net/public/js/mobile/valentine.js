$(document).ready(function(){
	/*$('.vote_image_span:not(.valentine_voted)').on('click', function(){
		let pht_id = $(this).data('photo-id');
		let esc_id = $(this).data('escort-id');
		let usr_id = $(this).data('user-id');
		let $this = $(this);
		if(usr_id == ''){
			window.location = "/private/signin";
		}

		$.ajax({
			url: '/escorts/valentine',
			type: 'POST',
			data: {
				photo_id: pht_id,
				escort_id: esc_id,
				user_id: usr_id
			},
			success: function(resp){
				if(!resp.includes('limit')){
					$this.addClass('valentine_voted');
					$this.parent().find('.valentine_image').attr('data-voted', 1);
					$this.css('background', "url('/img/voted-mobile.png')");
					$this.find('.valentine_vote_text').removeClass('valentine_show').addClass('valentine_hide');
					$this.find('.valentine_voted_text').removeClass('valentine_hide').addClass('valentine_show');
					$('.valentine_vote_left').html(3 - resp);
					$this.find('.tooltiptext').css('visibility', 'visible').animate({
  						opacity: 1,
					}, 1000).animate({
						opacity:0
					}, 2000);
				} else {
					$('.valentine_vote_left').html(0);
					$this.find('.tooltiptext').css('visibility', 'visible').animate({
  						opacity: 1,
					}, 1000).animate({
						opacity:0
					}, 2000);
				}
			}
		})
	})*/

	function calculate_resize(orig_width, orig_height, width = null, height = null){
		var k;
		if (width != undefined && orig_width > orig_height){
			k = orig_height / orig_width;
		}  else if (height != undefined && orig_width > orig_height){
		  	k = orig_width  / orig_height;
		} else if (width != undefined && orig_width <= orig_height){
			k = orig_height / orig_width;
		} else if (height != undefined && orig_width <= orig_height){
			k = orig_width  / orig_height;
		}
		if (height != undefined) {
			width = k * height;
		} else if (width) {
			height = k * width;
		}
		return {
			'res_width': Math.round(width), 
			'res_height': Math.round(height)}

	}

	function openGallery($this){
		var pswpElement = document.querySelectorAll('.pswp')[0];

		var items = [];
		let clicked_src = $this.attr('src').replace('mobile_thumb', 'm560');
		let clicked_index = 0;
		$('.escort .valentine_image').each(function(index, el){
			let link = $(el).attr('src').replace('mobile_thumb', 'm560');
			if(link == clicked_src){
				clicked_index = index;
			}
			let img_width = $(el).attr('data-width');
			let img_height = $(el).attr('data-height');
			let res = calculate_resize(img_width, img_height, 560);
			items.push({
				'src': link,
				'w': res['res_width'],
				'h': res['res_height']
			})
		})
		var options = {
			index: clicked_index,
			bgOpacity: 0.8,
			mouseUsed: true
		};

		var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
		gallery.init();

		// initVoteBtn(clicked_src);

		// $('.zoomed_vote_image_span:not(.zoomed_valentine_voted)').on('click touchstart',function(){
		// 	zoomedVote($(this))
		// })

		//on image change
		// gallery.listen('beforeChange', function() {
		//   initVoteBtn(gallery.currItem.src)
		// });

		// //on zoom close
		// gallery.listen('unbindEvents', function() {
		// 	unsetVoteBtn();
		// });

	}

	// function initVoteBtn(img_src){
	// 	let cur_img = img_src.replace('m560', 'mobile_thumb');
	// 	let escort_id = $('img[src="'+cur_img+'"]').data('esc-id');
	// 	let user_id = $('img[src="'+cur_img+'"]').data('user-id');
	// 	let photo_id = $('img[src="'+cur_img+'"]').data('photo-id');
	// 	if($('img[src="'+cur_img+'"]').attr('data-voted') == 1){
	// 		$('.zoomed_vote_image_span').addClass('zoomed_valentine_voted');
	// 		$('.zoom_valentine_vote_text').addClass('valentine_hide').removeClass('valentine_show');
	// 		$('.zoom_valentine_voted_text').removeClass('valentine_hide').addClass('valentine_show');
	// 	} else {
	// 		$('.zoomed_vote_image_span').removeClass('zoomed_valentine_voted');
	// 		$('.zoom_valentine_voted_text').addClass('valentine_hide').removeClass('valentine_show');
	// 		$('.zoom_valentine_vote_text').removeClass('valentine_hide').addClass('valentine_show');
	// 	}
	// 	$('.zoomed_vote_image_span').attr({
	// 		'data-esc-id': escort_id,
	// 		'data-usr-id': user_id,
	// 		'data-photo-id': photo_id
	// 	})
	// }

	// function unsetVoteBtn(){
	// 	$('.zoomed_vote_image_span').attr({
	// 		'data-esc-id': 0,
	// 		'data-usr-id': 0,
	// 		'data-photo-id': 0
	// 	})	
	// }

	// function zoomedVote(el){
	// 	let esc_id = el.attr('data-esc-id');
	// 	let usr_id = el.attr('data-usr-id');
	// 	let pht_id = el.attr('data-photo-id');
	// 	if(usr_id == 0 || usr_id == undefined){
	// 		window.location = "/private/signin";
	// 	}

	// 	$.ajax({
	// 		url: '/escorts/valentine',
	// 		type: 'POST',
	// 		data: {
	// 			photo_id: pht_id,
	// 			escort_id: esc_id,
	// 			user_id: usr_id
	// 		},
	// 		success: function(resp){
	// 			if(!resp.includes('limit')){
	// 				$('img[data-photo-id="'+pht_id+'"]').attr('data-voted', 1);
	// 				$('span.vote_image_span[data-photo-id="'+pht_id+'"]').addClass('valentine_voted').css('background', "url('/img/voted-mobile.png')");
	// 				$('span.vote_image_span[data-photo-id="'+pht_id+'"]').find('.valentine_vote_text').removeClass('valentine_show').addClass('valentine_hide');
	// 				$('span.vote_image_span[data-photo-id="'+pht_id+'"]').find('.valentine_voted_text').removeClass('valentine_hide').addClass('valentine_show');
	// 				$('.valentine_vote_left').html(3 - resp);
	// 				el.addClass('zoomed_valentine_voted');
	// 				el.find('.zoom_valentine_vote_text').removeClass('valentine_show').addClass('valentine_hide');
	// 				el.find('.zoom_valentine_voted_text').removeClass('valentine_hide').addClass('valentine_show');
	// 				$('.zoom_valentine_vote_left').html(3 - resp);
	// 				el.find('.zoom_tooltiptext').css('visibility', 'visible').animate({
 //  						opacity: 1,
	// 				}, 1000).animate({
	// 					opacity:0
	// 				}, 2000);
	// 			} else {
	// 				$('.zoom_valentine_vote_left').html(0);
	// 				el.find('.zoom_tooltiptext').css('visibility', 'visible').animate({
 //  						opacity: 1,
	// 				}, 1000).animate({
	// 					opacity:0
	// 				}, 2000);
	// 			}					
	// 		}
	// 	})

	// }

	$('.valentine_image').on('click', function(){
		openGallery($(this));
	})

	
})