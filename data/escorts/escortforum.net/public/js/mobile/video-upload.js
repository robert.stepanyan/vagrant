HTML5.VideoUpload.Escort = {
    init: function(){
        this.ClickVideo();
        this.Hover();
        this.Remove();
        this.AgencyUpload();
        this.WaitUpload();
        this.PlayVideo();
    },

    ClickVideo: function(){
        var self = this,
            _elm = $('.video > li');

        _elm.on('click', function(){
            var img = $(this).children('img'),
                width = img.attr('data-width')[0],
                height = img.attr('data-height')[0],
                video = img.attr('data-video'),
                image = img.attr('src').replace('m320','orig');

            video = $('#edit_video').attr('config') + video;

            self.Play( video, image, width, height, true );
        });
    },

    Play: function( video, image, width, height, auto ){
        var primary = ( Browser.Plugins.Flash && Browser.Plugins.Flash.version >= 9 ) ? "flash" : "HTML5";

        jwplayer('video-modal').setup({
            height: height,
            width: width,
            image: image,
            autostart: auto,
            skin: {
                name: "bekle"
            },
            logo: {
                file: "/img/video/logo.png"
            },
            /*provider: 'rtmp',
             file:'rtmp://'+video,
             streamer:'rtmp://'+video*/
            file: video
        });
    }
};
