var AttrModal = {

	//parameters
	source: '',
	cookieMaxAge: null,
	animation: '',
	overlay: null,
	modalDiv: null,
	modal: null,
	close: null,
	notShowAgain: null,
	understandButton: null,
	neverShow: false,
	buttonCaption: '',
	labelText: '',
 
	//methods
	start: function(params){
		console.log('started');
		var showModal = this.getCookie('attrModal');
		var show18 = this.getCookie('18');
		if ((!showModal || showModal == 1) && show18 == 1){
			if(!params){
				console.log('Params must be set.')
			} else {
				this.source = params.source || '';
				this.cookieMaxAge = params.cookieMaxAge || 1;
				this.buttonCaption = params.buttonCaption || 'I understand';
				this.labelText = params.labelText || 'Don\'t show again';

				this.loadModal();
			}
		}
	},

	loadModal: function(){
		//loading images into modal
		var img = new Image();
		img.onload = this.constractPopup();
		img.src = this.source;
	},

	constractPopup: function(){
		//constructing overlay
		var overlay = document.createElement('div');
		overlay.style.opacity = 0;
		overlay.id = 'attrOverlay';
		this.overlay = document.body.appendChild(overlay);

		//calling animate overlay
		this.animate(this.overlay);

		//constructing modal container
		var modalDiv = document.createElement('div');
		modalDiv.id = 'attrModalDiv';
		this.modalDiv = this.overlay.appendChild(modalDiv);

		//constructing modal image
		var modalImage = document.createElement('img');
		modalImage.src = this.source;
		modalImage.style.opacity = 0;
		modalImage.id = 'attrModal';
		this.modal = this.modalDiv.appendChild(modalImage);

		//constructing close button
		var closebutton = document. createElement('span');
		closebutton.id = 'attrClose';
		closebutton.style.opacity = 0;
		closebutton.onclick = function(){
			AttrModal.modalDiv.parentNode.removeChild(AttrModal.modalDiv);
			AttrModal.animateOut(AttrModal.overlay);	
			window.setTimeout(function(){ 
				AttrModal.overlay.parentNode.removeChild(AttrModal.overlay);
			}, 1000);

			AttrModal.setCookie();
		};
		this.close = this.modalDiv.appendChild(closebutton);

		//constructing understandButton
		var understandButton = document.createElement('a');
		understandButton.id = 'attrUnderstand';
		understandButton.href = 'javascript: void(0)';
		understandButton.style.opacity = 0;
		understandButton.onclick = function(){
			AttrModal.modalDiv.parentNode.removeChild(AttrModal.modalDiv);
			AttrModal.animateOut(AttrModal.overlay);	
			window.setTimeout(function(){ 
				AttrModal.overlay.parentNode.removeChild(AttrModal.overlay);
			}, 1000);
			AttrModal.setCookie();
		};
		var buttonCaption = document.createTextNode(this.buttonCaption);
		understandButton.appendChild(buttonCaption);
		this.understandButton = this.modalDiv.appendChild(understandButton);


		//constructing notShow again checkbox
		var notShowAgainLabel = document.createElement('label');
		notShowAgainLabel.id = 'attrLabel';
		var notShowAgainTextNode = document.createTextNode(this.labelText);
		notShowAgainLabel.appendChild(notShowAgainTextNode);
		var notShowAgain = document.createElement('input');
		notShowAgain.type = 'checkbox';
		notShowAgainLabel.appendChild(notShowAgain);
		notShowAgainLabel.style.opacity = 0;
		notShowAgain.onclick = notShowAgainLabel.onclick = function(){
			AttrModal.neverShow = true;
		};
		this.notShowAgain = this.modalDiv.appendChild(notShowAgainLabel);

		//calling animate modal
		window.setTimeout(function(){
			AttrModal.animate(AttrModal.modal);
			AttrModal.animate(AttrModal.understandButton);
			AttrModal.animate(AttrModal.notShowAgain);
			AttrModal.animate(AttrModal.close);

		}, 1000);
	},

	animate: function(el){
		var opacity = 0;
		var interval = window.setInterval(
			function(){
				if (opacity < 1){
					opacity = opacity + 0.1;
					el.style.opacity = opacity * opacity;
				} else {
					el.style.opacity = 1;
					window.clearInterval(interval);
				}
			}, 50
		);
		opacity = 0;		
	},

	animateOut: function(el){
		var opacity = 1;
		var interval2 = window.setInterval(
			function(){
				if (opacity > 0){
					opacity = opacity - 0.05;
					el.style.opacity = opacity * opacity;
				} else {
					el.style.opacity = 0;
					window.clearInterval(interval2);
				}
			}, 100
		);

		opacity = 1;
	},

	setCookie: function () {
    	var d = new Date();
   		var expirationDays = (this.neverShow) ? 14 : parseInt(this.cookieMaxAge);

    	d.setTime(d.getTime() + (expirationDays * 24 * 60 * 60 * 1000));
    	var expires = 'expires=' + d.toUTCString();

    	document.cookie = 'attrModal = 0 ;' + expires;
	},

	getCookie: function(name) {
    	var ca = document.cookie.split(';');

    	for (var i = ca.length - 1; i >= 0; i--) {
    		var c = ca[i];
    		while (c.charAt(0)==' ') {
    			c = c.substring(1);
    		}
    		
    		if (c.indexOf(name) == 0) {
    			attrCookie = c.split('=');
    			return attrCookie[1];
    		}
    	};
	}
};
