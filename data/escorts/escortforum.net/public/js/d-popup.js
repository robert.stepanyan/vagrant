/* --> Feedback */
Cubix.DPopup = {};


Cubix.DPopup.inProcess = false;

Cubix.DPopup.url = '';

Cubix.DPopup.Show = function (box_height, box_width) {
	if ( Cubix.DPopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });
	page_overlay.disable();

	var y_offset = 30;

	var container = new Element('div', { 'class': 'DPopup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset+130,
		opacity: 0,
        position: 'absolute',
        'z-index': 101,
        background: '#fff'
	}).inject(document.body);
	
	Cubix.DPopup.inProcess = true;

	new Request({
		url: Cubix.DPopup.url,
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.DPopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'popup-close-btn-v2'
			}).inject(container);

			close_btn.addEvent('click', function() {
				$$('.DPopup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
			
			var forms = container.getElements('form');
			forms.addEvent('submit', Cubix.DPopup.Send);
			forms.each(function (form) { form.set('action', form.get('action') + (form.get('action').indexOf('?') != -1 ? '&' : '?') + 'ajax'); });
		}
	}).send();
		
	
	return false;
}

Cubix.DPopup.Send = function (e) {
	e.stop();
	function getErrorElement(el) {
		var error = el.getPrevious('.error-profile');
		if ( error ) return error;

		var target = el;
		if ( el.get('name') == 'terms' ) {
			target = el.getNext('label');
		}

		return new Element('div', { 'class': 'error-profile' }).inject(target, 'before');
	}
   
	var overlay = new Cubix.Overlay($$('.DPopup-wrapper')[0], { loader: _st('loader-small.gif'), position: '50%', no_relative: true });
	overlay.disable();

	this.set('send', {
		onSuccess: function (resp) {

			resp = JSON.decode(resp);

			this.getElements('.error-profile').destroy();
			this.getElements('.invalid').removeClass('invalid');

			overlay.enable();

			if ( resp.status == 'error' ) {
				for ( var field in resp.msgs ) {
					var input = this.getElement('*[name=' + field + ']');
					input.addClass('invalid');
					getErrorElement(input).set('html', resp.msgs[field]);
				}
			}
			else if ( resp.status == 'success' ) {

				if ( resp.signin ) {
					window.location.reload();
					return;
				}
                
//				this.getParent().set('html', '');
                var el = $('comment').getParent().getParent();
                el.set('html', popupVars['confirmation_mail']);
                el.setStyles({
                    color: "#009900",
                    'font-size': '14px'
                })
                $('deleteProfileTitle').set('html', popupVars['success']);
                
				var close_btn = new Element('div', {
					html: '',
					'class': 'popup-close-btn-v2'
				}).inject($$('.DPopup-wrapper')[0]);

				close_btn.addEvent('click', function() {
					$$('.DPopup-wrapper').destroy();
					$$('.overlay').destroy();
				});
			}
		}.bind(this)
	});

	this.send();
}