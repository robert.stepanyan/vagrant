Cubix.Easterlottery = {};

Cubix.Easterlottery.inProcess = false;

Cubix.Easterlottery.url = '';

Cubix.Easterlottery.Show = function (arg) {
	if ( Cubix.Easterlottery.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', z_index: 200 });
	page_overlay.disable();


	if (arg === 'winner')
	{
		$$('#EasterWinnerPopup').addClass('open');
		$$('#EasterPopup').destroy();
	}else{
		$$('#EasterPopup').addClass('open');
		$$('#EasterWinnerPopup').destroy();
	}


	Cubix.Easterlottery.inProcess = true;

	$$('#EasterButton').addEvent('click', function() {
		$$('#EasterPopup').destroy();
		$$('#EasterWinnerPopup').destroy();
		page_overlay.enable();
	});

	return false;
}