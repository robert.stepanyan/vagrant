Cubix.Expo = {};
Cubix.Expo.overlay;
Cubix.Expo.loader;
Cubix.Expo.monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];

window.addEvent('domready', function(){
	Cubix.Expo.overlay = new Cubix.Overlay($('wrapper'), { has_loader: false, opacity: 0.5, color: '#000' });
});

Cubix.Expo.load = function (action) {
	if ( ! action ) action = '/expo/select-option';
	Cubix.Expo.overlay.disable();
	
	if ( Cubix.Expo.loader ) {
		Cubix.Expo.loader.disable();
	}

    new Request({
        method: 'get',
        url: action,
		evalScripts: true,
        onComplete: function(response) {
            $$('.expoPopupWrap').set('html', response);
			
			if ( $$('.expoPopup').length ) {
				Cubix.Expo.loader = new Cubix.Overlay( $$('.expoPopup')[0], {loader: _st('loader-small.gif'), position: '50%', no_relative: true});
			}
			
			if ( action.indexOf('add-expo-product') !== -1 ) {
				Cubix.Expo.initAddProduct();
			} else if ( action.indexOf('add-expo-package') !== -1 ) {
				Cubix.Expo.initAddPackage();
			}
			
        }
    }).send();
};

Cubix.Expo.closePopup = function () {
	$$('.expoPopup').destroy();
	Cubix.Expo.overlay.enable();
};


Cubix.Expo.initAddProduct = function() {
	
	new DatePicker($('expo-start-date'), {
		pickerClass: 'datepicker',
		positionOffset: { x: -10, y: 0 },
		allowEmpty: true,
		format: 'j M Y', 
		timePicker: false,
		onSelect: Cubix.Expo.calculatePrice
	});
	
	new DatePicker($('expo-end-date'), {
		pickerClass: 'datepicker',
		positionOffset: { x: -10, y: 0 },
		allowEmpty: true,
		format: 'j M Y', 
		timePicker: false,
		onSelect: Cubix.Expo.calculatePrice
	});
	
	
	$$('.btn-pay').addEvent('click', function(e) {
		e.preventDefault();
		
		Cubix.Expo.loader.disable();
		new Request.JSON({
			method: 'post',
			url: '/expo/add-expo-product',
			evalScripts: true,
			data: $$('.expoPopup form')[0].toQueryString().parseQueryString(),
			onComplete: function(response) {
				Cubix.Expo.loader.enable();
				
				$$('.expoPopup .error').set('html', '');
				if ( response.status === 'success' ) {
					window.location = response.url;
				} else {
					for(name in response.msgs) {
						$$('.expoPopup .error.' + name ).set('html', response.msgs[name]);
					}
				}
			}
		}).send();
	});
	
	
	if ( $('escort-id') ) {
		$('escort-id').addEvent('change', function() {
			if ( ! this.get('value') ) {
				Cubix.Expo.fillPackages({});
				return;
			}
			Cubix.Expo.loader.disable();
			new Request.JSON({
				method: 'get',
				url: '/expo/get-escort-packages?escort_id=' + this.get('value'),
				onComplete: function(packages) {
					Cubix.Expo.loader.enable();
					Cubix.Expo.fillPackages(packages);
				}
			}).send();
		});
	}
	
	if ( $('package-id') ) {
		$('package-id').addEvent('change', function() {
			if ( ! this.get('value') ) {
				$('price').set('value', '');
				return;
			}
			
			Cubix.Expo.loader.disable();
			new Request.JSON({
				method: 'get',
				url: '/expo/get-expo-product-price?package_id=' + this.getSelected()[0].get('data-package-id'),
				onComplete: function(response) {
					Cubix.Expo.loader.enable();
					$('price').set('value', response.price);
					Cubix.Expo.calculatePrice();
				}
			}).send();
		});
	}
};

Cubix.Expo.initAddPackage = function() {
	new DatePicker($('expo-start-date'), {
		pickerClass: 'datepicker',
		positionOffset: { x: -10, y: 0 },
		allowEmpty: true,
		format: 'j M Y', 
		timePicker: false,
		onSelect: Cubix.Expo.calculatePrice
	});
	
	new DatePicker($('expo-end-date'), {
		pickerClass: 'datepicker',
		positionOffset: { x: -10, y: 0 },
		allowEmpty: true,
		format: 'j M Y', 
		timePicker: false,
		onSelect: Cubix.Expo.calculatePrice
	});
	
	
	$$('.btn-pay').addEvent('click', function(e) {
		e.preventDefault();
		
		Cubix.Expo.loader.disable();
		new Request.JSON({
			method: 'post',
			url: '/expo/add-expo-package',
			evalScripts: true,
			data: $$('.expoPopup form')[0].toQueryString().parseQueryString(),
			onComplete: function(response) {
				Cubix.Expo.loader.enable();
				
				$$('.expoPopup .error').set('html', '');
				if ( response.status === 'success' ) {
					window.location = response.url;
				} else {
					for(name in response.msgs) {
						$$('.expoPopup .error.' + name ).set('html', response.msgs[name]);
					}
				}
			}
		}).send();
	});
};

Cubix.Expo.calculatePrice = function() {
	var price = 0;
		
	var startDate = $$('input[name="start_date"]')[0].get('value');
	var endDate = $$('input[name="end_date"]')[0].get('value');

	if ( ! startDate || ! endDate ) {
		$('total-expo-price').set('html', '0 EUR');
		return;
	}

	var days = Math.abs((endDate - startDate) / (60 * 60 * 24));

	days += 1;

	price = days * $('price').get('value');

	$('total-expo-price').set('html', price.toFixed() + ' EUR');
};

Cubix.Expo.fillPackages = function(packages) {
	$('package-id').empty();
	new Element('option', {value: '', html: '---'}).inject($('package-id'));
	
	
	
	for(orderPackageId in packages) {
		expirationDate = new Date(packages[orderPackageId].expiration_date);
		dateActivated = new Date(packages[orderPackageId].date_activated);
		new Element('option', {
			value: orderPackageId, 
			html: packages[orderPackageId].package_name + '(' + Cubix.Expo.monthNames[dateActivated.getMonth()] + ' ' + dateActivated.getDate() + ' - ' + Cubix.Expo.monthNames[expirationDate.getMonth()] + ' ' + expirationDate.getDate() + ')',
			'data-package-id': packages[orderPackageId].package_id
		}).inject($('package-id'));
	}
};