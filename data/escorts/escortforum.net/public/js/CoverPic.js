var CoverPic = this.CoverPic = new Class({

	Implements: [Events, Options],

	options: {
		contentWrapper: ".cover-box-wrapper"
	},

	form : null,
    picOverlay : null,
	_mooUpload :{
		texts:{
			it:{
				selectfile : 'Aggiungi foto',
				startupload: 'Carica foto',
				delete     : 'Elimina'
			},
			en:{
				selectfile : 'Add File',
				startupload: 'Start Upload',
				delete     : 'Delete'
			}
		}
	},
		
	initialize: function(form, options) {
		this.setOptions(options);
		this.picOverlay = new Cubix.Overlay(document.getElement(this.options.contentWrapper), {color: '#fdfdfd', opacity: .6, position: '55%', loader: '/img/photo-video/ajax_small.gif'});
		this.form = document.id(form);
		this.initUpload(this.form);
		
		if(!$$('.no-cover-pic')[0]){
			this.initRemovePic();
		}
	},
	
	initUpload: function(form){

		var self1 = this;
		var myUpload = new MooUpload('filecontrol-cover', {
			action: form.action,
			accept: 'image/*',
			method: 'auto',	// Automatic upload method (Choose the best)
			multiple: false,
			deleteBtn: true,
			texts:self1._mooUpload.texts[Cubix.Lang.id],
			onAddFiles: function(){
				var items = $$('#filecontrol-cover_listView .item');
				if(items.length > 1){
					var close = items[0].getElement('.delete');
					close.fireEvent('click', {
						stop: function(){}
					});
				}
			},
			onBeforeUpload: function(){
				form.getElement('.progresscont').removeClass('none').fade('in');
			},
			onFileUpload: function(fileindex, response){ 
				var container = $$('.cover-box')[0];
				
				if(response.finish){
					var item = $('filecontrol-cover_file_' + (fileindex));
					var itemBox = item.getParent('li');
					itemBox.set('tween', { onComplete: function() {this.element.destroy()}});
					itemBox.tween('opacity', 0);
					
					container.set('html','');					
					var div = new Element('div', {'id': 'cover_' + response.cover_id, 'class': 'wrapper-cover'}).inject(container, 'top');
					var strong = new Element('strong', {'html': 'Pending', 'id': 'cover_pic_status' }).inject(div, 'top');
					var img = new Element('img', {'src': response.cover_url, 'width': '349px', 'height': '247px'}).inject(div, 'top');
									
					if(form.getElement('.delete').hasClass('none')){
						self1.initRemovePic();
					}
				}
			},
			onFileUploadError: function(filenum, response){
				if(response.error) {
					$$('.mooupload_error').appendText(response.error);
				}
			},
			onFinishUpload: function() {
				setTimeout(function() {
					var fxProg = new Fx.Tween(form.getElement('.progresscont'),{duration: 2000});
					fxProg.addEvent('complete', function(){this.element.addClass('none'); });
					fxProg.start('opacity','100','0');
				}, 2000);
			}
		});
	},
	
	initRemovePic : function() {
		self1 = this;
		$$('#form-upload-cover .delete').removeClass('none').addEvent('click', function(e){
			e.stop();
			self1.picOverlay.disable();
			var picBox = $$('.cover-box')[0];
			var picWrap = picBox.getElement('.wrapper-cover');
			var coverPicStatus = $('cover_pic_status');
			new Request({
				url: '/photo-video/cover-pic',
				method: 'get',
				data: {'act': 'delete', video_id: picWrap.get('id').replace('cover_' , '')},
				onSuccess: function (resp) {
					picWrap.destroy();
					if(coverPicStatus) {
						coverPicStatus.destroy();
					}
					new Element('div', {'class' : "no-cover-pic" }).inject(picBox, 'top');
					this.addClass('none');
					this.removeEvents('click');
					self1.picOverlay.enable();
				}.bind(this)
			}).send();
		});
	}
});