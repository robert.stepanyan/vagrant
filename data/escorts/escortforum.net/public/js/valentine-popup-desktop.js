Cubix.Valentinepopup = {};

Cubix.Valentinepopup.inProcess = false;

Cubix.Valentinepopup.url = '';

Cubix.Valentinepopup.Show = function () {
    if (Cubix.Valentinepopup.inProcess) return false;


    $$('#valentinePopup').addClass('open');

    Cubix.Valentinepopup.inProcess = true;

    $$('#red-galery-page').addEvent('click', function() {
        $$('#valentinePopup').destroy();
    });

    $$('#close-button-mod-id').addEvent('click', function() {
        $$('#valentinePopup').destroy();
    });

    return false;
}






