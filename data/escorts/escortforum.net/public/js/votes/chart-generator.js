var Charts = {
    labels : null,
    data_1 : null,
    data_2 : null,
    data_3 : null,
    proc : null,
    height: 85,

    init: function( labels, data_1, data_2, data_3 ){
        var self = this;

        self.labels = labels;
        self.data_1 = data_1;
        self.data_2 = data_2;
        self.data_3 = data_3;

        self.renderLabels();
        self.proc = self.checkMax();

        self.renderLeftBars();
        self.renderRightBars();
        self.renderVotedEscorts();
    },

    renderLabels: function(){
        var self = this;

        for ( var i in self.labels ){
            var j = Number( i ) + 1;
            $$( '.label-' + j ).set( 'text', self.labels[i] );
        }
    },

    checkMax: function(){
        var self = this;

        var max_1 = Math.max.apply( null, self.data_1 );
        var max_2 = Math.max.apply( null, self.data_2 );
        return 100 / ( Math.max( max_1, max_2 ) );
    },

    renderLeftBars: function(){
        var self = this;

        for ( var i in self.data_1 ){
            var j = Number( i ) + 1;
            var height = ( ( Number( self.data_1[i] ) * Number( self.proc ) ) * self.height ) / 100;
            var top = self.height - height;
            $$( '.left-' + j ).morph({ 'top': top + '%', 'height': height + '%' }).set( 'data-id', self.data_3[i] );
            if( Number( self.data_1[i] ) > 0 ){
                $$( '.left-' + j  + ' .bar-value' ).set("html", self.data_1[i] + '<br/>Votes');
                $$( '.left-' + j  + ' .bar-value' ).morph({ 'top': '-26%' });
            }
        }
    },

    renderRightBars: function(){
        var self = this;

        for ( var i in self.data_2 ){
            var j = Number( i ) + 1;
            var height = ( ( Number( self.data_2[i] ) * Number( self.proc ) ) * self.height ) / 100;
            var top = self.height - height;
            $$( '.right-' + j ).morph({ 'top': top + '%', 'height': height + '%' });
            if( Number( self.data_2[i] ) > 0 ){
                $$( '.right-' + j  + ' .bar-value' ).set("html", self.data_2[i] + '<br/>Votes');
                $$( '.right-' + j  + ' .bar-value' ).morph({ 'top': '-26%' });
            }
        }
    },

    renderVotedEscorts: function(){
        var self = this, _window = null;

        $$('.columns .bar-left').addEvent('click', function(){
            if( this.getElement('.bar-value').get('html') == '' ){
                return false;
            }

            if( !$(document.body).getElement('.tooltip-votes') ){
                _window = new Element('div', { class: 'tooltip-votes' });
                _window.inject($(document.body));
                var _pos = self.getPosition( this );
                _window.setStyles({ left: ( _pos.x + 40 ) + 'px', top: ( _pos.y - 1 ) + 'px' });
            } else {
                if( !_window ){
                    _window = $$('.tooltip-votes')[0];
                }
            }
            console.log( _window );

            var myDrag = new Drag(_window).detach();

            var e_block = $$('.city-escort');
            if( !$(document.body).getElement('.tooltip-votes') ){
            }

            // console.log( _pos );
            var requestData = new Request ({
                url: '/city-votes/get-escorts',
                method: 'post',
                data: { city_id: this.get('data-id') },
                onRequest: function(){
                    _window.addClass('load');
                    e_block.fade('out');
                },
                onSuccess: function( response ){
                    _window.set('html', response);
                    e_block = $$('.city-escort');
                    var b_header = $$('.city-escorts-heading');
                    e_block.fade('in');
                    _window.removeClass('load');
                    self.closeCityEscorts();

                    b_header.addEvent('mouseenter', function(){
                        myDrag.attach();
                    }).addEvent('mouseleave', function(){
                        myDrag.detach();
                    });

                    if( e_block.length > 4 && e_block.length < 9 ){
                        _window.removeClass('scroll').addClass('large');
                    } else if( e_block.length >= 9 ){
                        _window.removeClass('large').addClass('scroll');
                    } else {
                        _window.removeClass('scroll').removeClass('large');
                    }
                }
            });

            requestData.send();

        });

        /*$$('.columns .bar-left').addEvent('mouseleave', function(){
            this.getElements('.tooltip-votes').destroy();
        });*/
    },

    getPosition: function( element ){
        var xPosition = 0;
        var yPosition = 0;

        while(element) {
            xPosition += (element.offsetLeft/* - element.scrollLeft*/ + element.clientLeft);
            yPosition += (element.offsetTop/* - element.scrollTop*/ + element.clientTop);
            element = element.offsetParent;
        }

        return { x: xPosition, y: yPosition };
    },

    closeCityEscorts: function(){
        $$('.close-city-escorts').addEvent('click', function(){
            this.getParent('.tooltip-votes').destroy();
        });
    }
};
