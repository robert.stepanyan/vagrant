window.addEvent('domready', function() {
    $$('.let').addEvent('click', function() {
        var self = this;

        var l = self.get('rel');

        $$('.let').removeClass('sel');
        $$('.' + l).addClass('sel');

        var data = {};

        data.letter = l;
        data = getData(data);
        sendData(data);

        return false;
    });

    /*$('b-search').addEvent('click', function() {
        var data = getData({});
        sendData(data);

        return false;
    });*/

    var data = {letter: 'a'};
    sendData(data);
});

function getData(data)
{
    /*var s = $('g-search').get('value');

    if (s.length > 0)
        data.search = s;*/

    if ($('letter') && !data.hasOwnProperty('letter'))
    {
        var letter = $('letter').get('value');

        if (letter.length > 0)
            data.letter = letter;
    }

    return data;
}

function sendData(data)
{
    var overlay = new Cubix.Overlay($('glossary'), {
        loader: _st('loader-small.gif'),
        position: '50%',
        offset: {
            left: 0,
            top: -1
        }
    });
    //overlay.enable();

    new Request({
        method: 'get',
        data: data,
        url: '/glossary/get',
        onComplete: function(resp) {
            $('g-content').set('html', resp);

            $('letter').set('value', data.letter);
            //overlay.disable();
        }
    }).send();
}