;(function (window, document) {

    function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
            textbox.addEventListener(event, function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        });
    };

    function ecardonPopup(checkoutId) {

        var overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000' });

        var container = new Element('div', {'class': 'ecardon-popup'}).setStyles({
            left: window.getWidth() / 2 - 460 / 2,
            top: window.getHeight() / 2 - 320 / 2,
            opacity: 0,
            position: 'fixed',
            'z-index': 101,
            'font-size': '14px'
        }).inject(document.body);

        overlay.disable();

        new Request({
            url: '/payment/ecardon-popup',
            method: 'get',
            onSuccess: function (resp) {
                container.set('html', resp);

                container.setStyle('opacity', 1);
                new Element('script', {
                    text: 'var wpwlOptions = { locale: "' + Cubix.Lang.id + '", style: "plain", onBeforeSubmitCard: function(){ if ( $$(".wpwl-control-cardHolder").get("value") == "") { $$(".wpwl-control-cardHolder").addClass("wpwl-has-error"); $$(".wpwl-button-pay").addClass("wpwl-button-error").set("disabled", "disabled"); new Element("div", {"class": "wpwl-hint wpwl-hint-cardHolderError", "text": "card holder required"}).inject($$(".wpwl-control-cardHolder")[0], "after");  return false} else return true}}'
                }).inject(container, 'before');

                new Element('script', {
                    src: "https://oppwa.com/v1/paymentWidgets.js?checkoutId=" + checkoutId
                }).inject(container, 'before');
            }
        }).send();

        $$('.overlay').addEvent('click', function () {
            container.destroy();
            this.destroy();
        });
    };

    function submitPaymentForm(data) {

        $$('.payment-form__wrapper').addClass('processing');

        new Request({
            url: '/payment/checkout',
            data: data,
            method: 'post',
            onSuccess: function (response) {
                response = JSON.decode(response, true);

                $$('.payment-form__wrapper').removeClass('processing');

                switch (response.status) {
                    case 'ecardon_success':
                        ecardonPopup(response.checkoutId);
                        break;

                    case 'powercash_success':
                        Cubix.PowerCash.init(response.start_url);
                        break;

                    case 'bitcoin_success':
                        var formHtml = response.form;
                        $$('body')[0].set('html', $$('body')[0].get('html') + formHtml.replace(/ \"/g, "\""));
                        $('payment-form').submit();
                        break;

                    case 'twispay_success':
                        var formHtml = response.form;
                        $$('body')[0].set('html', $$('body')[0].get('html') + formHtml.replace(/ \"/g, "\""));
                        $('twispay-payment-form').submit();
                        break;

                    case 'error':
                        alert('Currently payment system is not available, please check back soon. ' + (response.error ? 'Details ' + response.error : ''));
                        break;
                }

            }
        }).send();
    }


    document.addEventListener("DOMContentLoaded", function () {

        var numbersOnlyInputs = document.querySelectorAll("[data-validate='true'][data-accept='number']");

        numbersOnlyInputs.forEach(function ($element) {
            setInputFilter($element, function (value) {
                return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
            });
        });

        document.getElementById('pay-btn').addEventListener('click', function () {

            var data = {
                amount: document.getElementById('amount-inp').value,
                paymentMethod: document.querySelector('[name="paymentMethod"]:checked').value,
            };

            if (data.amount <= 0) {
                return alert("Invalid Amount specified");
            }

            if (!['twispay', 'ecardon', 'powercash', 'coinsome'].includes(data.paymentMethod)) {
                return alert("Invalid PaymentMethod");
            }

            submitPaymentForm(data);
        });

    });

})(this, document);