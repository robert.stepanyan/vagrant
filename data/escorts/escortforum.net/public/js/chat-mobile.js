var MobileChat = {
    Lists: {
        checkMessage: null,
        chat: null,
        windowHeight: window.innerHeight,

        init: function( chat ){
            this.chat = chat;
            this.chatSwitcher();
            this.convChatSwitcher();
            this.tabs();
            this.closeLists();
            //this.searchInConversations();
        },

        activeKeyboard: function(el){
            var self = this,
                textWrap = $('[class*="mobile_active_chat_"]'),
                textarea;
            
            if( textWrap ){
                textarea = textWrap.find('textarea');
                textarea.off('focus');

                textarea.on("focus", function() {
                    // /var top = (self.windowHeight - window.innerHeight);
                    //console.log(window.getPosition().x);
                }, false);
            }
        },

        sortingConversations: function(){
            var self = this,
                elms = $('#chat-wrapper .active_chat'),
                wrapper = $('#chat-container'),
                sort = [], sort_on = [], sort_off = [];

            elms.each(function(i, el){
                self.showLastMessage($(this));

                if( $(this).attr('data-status') === 'available' ){
                    sort_on.push(i);
                } else if( $(this).attr('data-status') === 'offline' ){
                    sort_off.push(i);
                }
            });

            sort = sort_on.concat( sort_off );
            for( var j = 0; j < sort.length; j += 1 ){
                wrapper.append( elms[ sort[j] ] );
            }
        },

        showLastMessage: function( el ){
            var elms = el.find('.messaging').find('.user_message');
            if( !elms.length ) return false;

            var msgs = elms[ elms.length - 1 ],
                msg_block = $(msgs).find('.messages');

            if( msg_block && msg_block.html() ){
                if( el.find('.tab_name').find('>span') ){
                    el.find('.tab_name').find('>span').remove();
                }

                el.find('.tab_name').html(el.find('.tab_name').html() + '<span class="last-msg">' + msg_block.find('p:last-child').html() + '</span>');
            }
        },

        chatSwitcher: function(){
            $('.close-chat-mobile').on('click', function () {
                $('.open-chat-mobile').removeClass('hidden');
                $('#baddy-list').addClass('hidden-m');
                $('#chat-wrapper').trigger('chat-on');
            });

            $('.open-chat-mobile').on('click', function () {
                $('.open-chat-mobile').addClass('hidden');
                $('#chat-wrapper').trigger('chat-off');
                $('#baddy-list').removeClass('hidden-m');
            });
        },

        convChatSwitcher: function(){
            // $$('.close-chat-mobile').addEvent('click', function () {
            //     $$('.open-chat-mobile').removeClass('hidden');
            //     $('chat-wrapper').fireEvent('chat-on');
            // });
            //
            // $$('.open-chat-mobile').addEvent('click', function () {
            //     $$('.open-chat-mobile').addClass('hidden');
            //     $('chat-wrapper').fireEvent('chat-off');
            // });
        },

        tabs: function(){
            var tabs = $('.chat-tabs span'),
                self = this;

            tabs.on('click', function(){
                tabs.removeClass('act');
                // this.addClass('act');
                $('.active_chat').each(function(i, el){
                    if( $(this).find('.last-msg').length ){
                        $(this).removeClass('showen');
                    } else {
                        $(this).addClass('showen')
                    }
                });

                if( $(this).attr('data-to') === 'chat' ){
                    $('.to-chat').addClass('act');
                    $('.active_chat').addClass('hidden');
                    $('.baddy_listing').removeClass('hidden');
                    $('.chat-mobile-layout').removeClass('in-conv');
                } else if( $(this).attr('data-to') === 'conv' ){
                    $('.active_chat .tab_header').off();

                    $('.to-conv').addClass('act');
                    $('.active_chat').removeClass('hidden');
                    $('.baddy_listing').addClass('hidden');
                    self.renderAvatars();
                    $('.chat-mobile-layout').addClass('in-conv');
                    // $$('#baddy-list .chat-tabs').setStyle();
                }

                self.openChat();
            });
        },

        closeLists: function(){
            $('.tab_header .tab_name').on('click', function(){
                $('#chat-wrapper').addClass('hide');
            })
        },

        renderAvatars: function(){
            $('#chat-container .active_chat').each( function(i, el){
                if( !$(this).find('._avatar').length ) {
                    var avatar = $(this).find('.tab_header').attr('data-avatar');

                    var avatar_el = $('<span>', {
                        'class': '_avatar'
                    });

                    $(this).append(avatar_el);
                    avatar_el.css('background', 'url(' + avatar + ') center center / 36px no-repeat');
                }
            });
        },

        openChat: function(){
            var self = this;

            $('.active_chat').off();
            $('.active_chat').on('click', function(e){
                if( $(this).parent().attr('id') === 'conv-switcher' || $(this).parent().attr('id') === 'baddy-list' ) return false;

                e.preventDefault();

                self.openChatWindow( $(this) );
            });

            var listener = function (event) {
                /* do something here */
            };

            /*$$('.listing li').each(function(el){
                el.removeEventListener('mouseup', listener, false);
            });*/
            $('.listing li').off('mouseup');

            $('.listing li').on('mouseup', function(e){
                if( $(this).parent().attr('id') === 'conv-switcher' || $(this).parent().attr('id') === 'baddy-list' ) return true;

                //e.preventDefault();

                self.openChatWindow( $(this) );
            });
        },

        openChatWindow: function( el ){
            var id, el_id_split, self = this, list_msgs = $('#baddy-list'), avatar, status, username, elm_av, tab_name, span_text;
            $('.active_chat').removeClass('hidden');

            if( el.hasClass('active_chat') ){
                el_id_split = el.attr('id').split("_");
                id = el_id_split[2];
                elm_av = el.find('.tab_header');
                avatar = elm_av.attr('data-avatar');
                status = ( elm_av.hasClass('available') ) ? 'online' : 'offline';
                tab_name = elm_av.find('.tab_name').clone();
                if( tab_name.find('span') ) tab_name.find('span').remove();
                username = tab_name.text();
            } else if( el.hasClass('bl-escort') || el.hasClass('bl-member') || el.hasClass('bl-agency') ){
                el_id_split = el.attr('id').split("_");
                id = el_id_split[1];
                avatar = el.find('.user_avatar').attr('src');
                status = ( el.find('.user_status').hasClass('available') ) ? 'online' : 'offline';
                username = el.find('.user_name').text();
            }

            var n_msgs = $('#active_chat_' + id);
            if( n_msgs.length ){
                if( n_msgs.find('.new_messages_count') ){
                    n_msgs.removeClass('new_message');
                    n_msgs.find('.new_messages_count').remove();
                }
            }

            var n_conv_msgs = $('#user_' + id);
            if( n_conv_msgs.length ){
                if( n_conv_msgs.find('.new_messages_count') ){
                    n_conv_msgs.removeClass('new_message');
                    n_conv_msgs.find('.new_messages_count').remove();
                }
            }

            var thread_id, exists_el = $('.mobile_active_chat_' + id );

            if( exists_el.length ){
                thread_id = $('.active_chat_' + id ).attr('data-thread');
                exists_el.addClass('show-message').attr('data-thread', thread_id);
            } else {
                //list_msgs.hide();
                list_msgs.find('.to-conv').trigger('click');
                $('.chat-mobile-layout.in-conv').removeClass('in-conv');

                var z = { k: 0 };
                var check = null;

                $('.chat-mobile-layout').addClass('chat-opened');

                var fns = function(){
                    if( z.k === 1 ){
                        clearInterval( check );
                        return false;
                    }

                    var chat_window = $('#active_chat_' + id);
                    if( chat_window.length ){
                        z.k = 1;
                        chat_window = chat_window.find('.active_chat_content');
                        chat_window = chat_window.clone();

                        chat_window.addClass( 'mobile_active_chat_' + id );
                        $('#chat-wrapper').append(chat_window);

                        self.chatPartlyRender( chat_window, { avatar: avatar, username: username, status: status }, id );

                        chat_window = chat_window.addClass('in');

                        setTimeout(function(){
                            chat_window.attr('data-thread', thread_id).addClass('show-message');
                            chat_window.find('.messaging').animate({ scrollTop: chat_window.find('.messaging')[0].scrollHeight }, "slow");
                        }, 100);

                        setTimeout(function(){
                            chat_window.attr( 'data-thread', $('.active_chat_' + id ).attr('data-thread') );
                        }, 100);
                    } else {

                    }
                };

                check = setInterval( fns, 30 );

                if( z.k === 1 ) clearInterval( check );
            }

            self.checkMessage = setInterval(checkForNewMessage, 1000);
            function checkForNewMessage(){
                var elm_or_wr_ap = $('#active_chat_' + id);
                if( !elm_or_wr_ap.length ) return false;

                var origin_msg_wrap = elm_or_wr_ap.find('.messaging'),
                    mobile_msg_wrap = $('.mobile_active_chat_' + id).find('.messaging'),
                    count_origin_el = origin_msg_wrap.find('.messages p'),
                    count_mobile_el = mobile_msg_wrap.find('.messages p'),
                    count_origin = 0, count_mobile = 0;

                $.each(count_origin_el, function(i, elms){
                    count_origin += elms.length;
                });

                $.each(count_mobile_el, function(i, elms){
                    count_mobile += elms.length;
                });

                if( count_origin_el.length > count_mobile_el.length ){
                    mobile_msg_wrap.html(origin_msg_wrap.html());
                    mobile_msg_wrap.animate({ scrollTop: mobile_msg_wrap[0].scrollHeight }, "fast");
                }
            }
        },

        chatPartlyRender: function( el, data, id ){
            var process = true, self = this, header_el, avatar_el, info_name, info_status, info_options,
                info_options_child, info_options_child_1, info_send, header_back, typing;

            if( !( typing = el.find('._typing').length ) ){
                typing = $('<span>', {
                    'class': '_typing'
                });
            } else { process = false; }

            if( !( header_el = el.find('._chat_header').length ) ){
                header_el = $('<span>', {
                    'class': '_chat_header'
                });
            } else { process = false; }

            if( !( header_back = el.find('._chat_back').length ) ){
                header_back = $('<span>', {
                    'class': '_chat_back'
                });
            } else { process = false; }

            if( !( avatar_el = el.find('._chat_avatar').length ) ){
                avatar_el = $('<span>', {
                    'class': '_chat_avatar'
                });
                avatar_el.css('background', 'url("' + data.avatar + '") center center / cover no-repeat');
            } else { process = false; }

            if( !( info_name = el.find('._chat_name').length ) ){
                info_name = $('<span>', {
                    'class': '_chat_name'
                });
                info_name.text(data.username);
            } else { process = false; }

            if( !( info_status = el.find('._chat_status').length ) ){
                info_status = $('<span>', {
                    'class': '_chat_status'
                });
                info_status.text(data.status);
            } else { process = false; }

            if( !( info_options = el.find('._chat_options').length ) ){
                info_options = $('<span>', {
                    'class': '_chat_options'
                });
            } else { process = false; }

            if( !( info_options_child = el.find('._chat_options_child').length ) ){
                info_options_child = $('<span>', {
                    'class': '_chat_options_child'
                });
                info_options_child.text('Block User');
            } else { process = false; }

            /*if( !( info_options_child_1 = el.find('._clear_history').length ) ){
                info_options_child_1 = $('<span>', {
                    'class': '_clear_history'
                });
                info_options_child_1.text('Clear History');
            } else { process = false; }*/

            if( !( info_send = el.find('._chat_send').length ) ){
                info_send = $('<span>', {
                    'class': '_chat_send'
                });
                info_send.attr('data-id', id);
            } else { process = false; }

            if( process ){
                info_options.append(info_options_child);
                //info_options.append(info_options_child_1);

                header_el.append(header_back);
                header_el.append(avatar_el);
                header_el.append(info_name);
                header_el.append(info_status);
                header_el.append(info_options);

                el.append(header_el).append(typing);
                el.find('.textarea_inner').append(info_send);
            }

            self.addChatEvents( el, id );

            return true;
        },

        addChatEvents: function( el, id ){
            this.activeKeyboard();

            var back = el.find('._chat_back'), self = this, origin_el = $('#active_chat_' + id);
            back.on('click', function(){
                el.addClass('hide-message');
                $('.active_chat').removeClass('hidden');
                self.searchInConversations();

                clearInterval( self.checkMessage );
                self.checkMessage = null;

                setTimeout(function(){
                    $('.chat-mobile-layout').addClass('in-conv');

                    var exist_message = el.find('.messaging').html();
                    if( exist_message ){
                        $('#baddy-list').find('.to-conv').trigger('click');
                        self.showLastMessage( origin_el );
                    } else {
                        origin_el.addClass('showen');
                    }

                    $('.active_chat').each(function(i, el){
                        if( $(this).find('.last-msg').length ){
                            $(this).removeClass('showen');
                        } else {
                            $(this).addClass('showen')
                        }
                    });

                    if( !$('.active_chat').length || ( $('.active_chat').length === $('.active_chat.showen').length ) ){
                        $('#baddy-list').find('.to-chat').trigger('click');
                    }

                    $('.chat-mobile-layout').removeClass('chat-opened');
                    el.remove();
                }, 300);
            });

            this.chat.mobileAttachUserTabEvents(id);

            var customEmotions = {
                '>:o'	: 'upset',
                'O:)'	: 'angel',
                '3:)'	: 'devil',
                '>:('	: 'grumpy'
            };
            var emotions = {
                ':)'	: 'smile',
                ':('	: 'frown',
                ':P'	: 'tongue',
                ':D'	: 'grin',
                ':*'	: 'kiss',
                ':o'	: 'gasp',
                ';)'	: 'wink',
                ':v'	: 'pacman',
                ':/'	: 'unsure',
                ':\'('	: 'cry',
                '^_^'	: 'kiki',
                '8)'	: 'glasses',
                'B|)'	: 'sunglasses',
                '<3'	: 'heart',
                '-_-'	: 'squint',
                'o.O'	: 'confused',
                ':3'	: 'colonthree'
            };

            /*var emotion_btn = el.getElement('.emotions_btn');
            emotion_btn.addEvent('click', function(){
                var emotion_cont = el.getElement('.emotions_container');

                if( !$(this).hasClass('in') ){
                    if( !emotion_cont ){
                        var emotionsContainer = new Element('div', {
                            'class': 'emotions_container'
                        });

                        for (var i in emotions) {
                            new Element('span', {
                                'class': 'emotions emotions_' + emotions[i],
                                'rel': i
                            }).inject(emotionsContainer);
                        }

                        for (var i in customEmotions) {
                            new Element('span', {
                                'class': 'emotions emotions_' + customEmotions[i],
                                'rel': i
                            }).inject(emotionsContainer);
                        }
                        emotionsContainer.inject(el.getElement('.emotions_wrapper'));

                        var _emotions = emotionsContainer.getElements('.emotions');

                        _emotions.addEvent('click', function(){
                            var _emotion = this.get('rel');

                            var textarea = emotionsContainer.getParent().getParent().getElement('textarea');
                            textarea.set('value', textarea.get('value') + _emotion + ' ')
                        });


                    } else {
                        emotion_cont.show();
                    }
                    $(this).addClass('in');
                } else {
                    el.getElement('.emotions_container').hide();
                    $(this).removeClass('in');
                }
            });*/

            var user_options = el.find('._chat_options');
            user_options.on('click', function(e){
                if( e.target !== this ) return false;

                $(this)[ ( !$(this).hasClass('in') ) ? 'addClass' : 'removeClass' ]('in');
            });

            el.find('._chat_send').on('click', function(){
                self.sendMessage(el);
            });

            var _chat_options_child = el.find('._chat_options_child');
            _chat_options_child.on('click', function(){
                self.chat.blockUserMobile(id);
                hideOptionsList( el )
            });

            /*var _chat_options_child_1 = el.find('._clear_history');
            _chat_options_child_1.on('click', function(){
                var thread_el = el.find('.messaging'),
                    thread_el_orig = origin_el.find('.messaging'),
                    thread_id = thread_el.attr('data-thread');

                if( thread_id ){
                    self.chat.threadHistory( parseInt( thread_id ) );
                    thread_el.empty();
                    thread_el_orig.empty();
                }
                hideOptionsList( el )
            });*/

            function hideOptionsList( el ){
                el.find('._chat_options').removeClass('in');
            }

            var escort_links = $('._chat_avatar', '._chat_name'), link;
            escort_links.on('click', function(){
                link = $(this).parent('.active_chat_content').find('.textarea_wrapper').attr('data-url');

                if( link ) window.location.href = link;
            });
        },

        sendMessage: function(el){
            var textarea = el.find('textarea'),
                message = textarea.val(),
                id = el.find('._chat_send').attr('data-id');

            textarea.val('').focus();

            if( !message || !id || !this.chat ) return false;

            this.chat.sendMessageMobile( { message: message, id: id } )
        },

        searchInConversations: function(){
            var mob_search = $('#baddy_list_search_mobile');
            mob_search.on('focus', function() {
                if ( $(this).val() == 'Search' ) {
                    $(this).val('');
                }
            });

            mob_search.on('blur', function() {
                if ( $(this).val().length == 0 ) {
                    $(this).val('Search');
                }
            });

            mob_search.on('keyup', function() {
                var items = $('#chat-wrapper').find('.active_chat');

                $.each(items, function(i, item) {
                    var id = parseInt( $(this).attr('id').replace('active_chat_', '') );

                    if ( $(this).find('.tab_name').text().toLowerCase().indexOf($('#baddy_list_search').val()) === -1 ) {
                        $('#active_chat_' + id).css('display', 'none');
                    } else {
                        $('#active_chat_' + id).css('display', 'block');
                    }
                });
            });
        },

        unreadMessage: function(id, unreadMessagesCount){
            //console.log( id, unreadMessagesCount );

            if ( typeof unreadMessagesCount === 'undefined' ) unreadMessagesCount = 1;


            var userTab = $('#user_' + id), count;
            userTab.addClass('new_message');

            if ( ! userTab.find('.new_messages_count').length ) {
                userTab.append($('<div>', {
                    'class' : 'new_messages_count',
                    html : unreadMessagesCount
                }));
            } else {
                count = parseInt( userTab.find('.new_messages_count').html() );
                count += unreadMessagesCount;
                userTab.find('.new_messages_count').html(count);
            }

            var conv_userTab = $('#active_chat_' + id);
            conv_userTab.addClass('new_message');

            if ( ! conv_userTab.find('.new_messages_count').length ) {
                conv_userTab.append($('<div>', {
                    'class' : 'new_messages_count',
                    html : unreadMessagesCount
                }));
            } else {
                count = parseInt( conv_userTab.find('.new_messages_count').html() );
                count += unreadMessagesCount;
                conv_userTab.find('.new_messages_count').html(count);
            }

            var chat_main_icon = $('#chatMainIcon');
            chat_main_icon.addClass('new_message');

            if ( ! chat_main_icon.find('.new_messages_count').length ) {
                chat_main_icon.append($('<span>', {
                    'class' : 'new_messages_count',
                    html : unreadMessagesCount
                }));
            } else {
                count = parseInt( chat_main_icon.find('.new_messages_count').html() );
                count += unreadMessagesCount;
                chat_main_icon.find('.new_messages_count').html(count);
            }

            pulseElm();

            function pulseElm(){
                $("#chatMainIcon").effect( 'pulsate', {}, 500, function(){} );
            }
        }
    }
};