Cubix.ProfileBoost = {}

window.addEvent('domready', function() {
	Cubix.ProfileBoost.loader = new Cubix.Overlay($('profile-boost'), {loader: _st('loader-small.gif'), position: '50%', no_relative: true});
});

Cubix.ProfileBoost.initEscortSelect = function() {
	$$('.escort-item').addEvent('click', function(e) {
		Cubix.ProfileBoost.loader.disable();
		$('escort-id').set('value', this.get('data-id'));
		$$('form')[0].submit();
	});
};


Cubix.ProfileBoost.initCitySelect = function() {
	$$('.city-select-radio').addEvent('click', function(e) {
		$$('.city-select-radio').removeClass('checked');
		this.addClass('checked');

		if( e.target.get('id') == 'city-select' && e.target.get('value') ) return false;
		
		if ( this.get('data-value') == 'index' ) {
			$('city-select')
				.set('disabled', 'disabled')
				.set('value', '');
			
			$('city-id').set('value', 'index');
		} else {
			$('city-select').set('disabled', '');
			$('city-id').set('value', '');
		}
	});
	
	$('city-select').addEvent('change', function() {
		$('city-id').set('value', this.get('value'));
		return false;
	});
	
	$$('.btn-next').addEvent('click', function() {
		Cubix.ProfileBoost.loader.disable();
		$$('form')[0].submit();
	});
	
};

Cubix.ProfileBoost.initHourSelect = function() {
	Cubix.ProfileBoost.Calendar.init();
	
	
	$$('.btn-next').addEvent('click', function() {
		Cubix.ProfileBoost.loader.disable();
		$$('form')[0].submit();
	});
};

Cubix.ProfileBoost.initConfirmation = function() {
	Cubix.ProfileBoost.moniformInit();
	
	var selectedHours = JSON.decode($('selected-hours').get('value'));
	if ( selectedHours ) {
		selectedHours.each(function(item) {
			item = item.split(':');
			Cubix.ProfileBoost.Calendar.addBoost(item[1], item[0]);
		});
	}

	$$('.btn-buy-now').addEvent('click', function () {
		Cubix.ProfileBoost.loader.disable();

		var selectedMethod = $$('.payment-option.checked').get('data-method');

		if (selectedMethod == 'twispay') {
			$$('form')[0].set('send', {
				method: 'post',
				onSuccess: function (response) {
					response = JSON.decode(response);

					if (response.status == 'error') {
						for (name in response.msgs) {
							alert(response.msgs[name]);
							break;
						}
					} else {
						if (response.form) {
							var formHtml = response.form;
							$$('body')[0].set('html', $$('body')[0].get('html') + formHtml.replace(/ \"/g, "\""));
							$('twispay-payment-form').submit();
							return;
						} else {
							console.error(response);
						}
					}

					Cubix.SelfCheckoutWizard.loader.enable();
				}
			});

			$$('form')[0].send();
		} else {
			$$('form')[0].submit();
		}
	});
	
	
	if ( $('mmg-cards') ) {
		var mmgSlide = new Fx.Slide($('mmg-cards'));
		if ( $('payment-gateway').get('value') != 'mmgbill' ) {
			mmgSlide.hide();
		}
	}
	
	
	$$('.payment-option').addEvent('click', function() {
		$$('.payment-option').removeClass('checked');
		this.addClass('checked');
		$('payment-gateway').set('value', this.get('data-method'));
	});
	
	// ECARDON POPUP 
	if ( $defined($('checkout-id')) ) {
		Cubix.ProfileBoost.ecardonPopup($('checkout-id').get('value'));
	}
	
	if ( $$('#mmg-cards .forget-card') ) {
		$$('#mmg-cards .forget-card').addEvent('click', function() {
			var self = this;
			Cubix.ProfileBoost.loader.disable();
			new Request({
				url: '/online-billing-v2/forget-credit-card?card=' + this.getParent('.card-row').get('card-id'),
				method: 'GET',
				onSuccess: function(response) {
					self.getParent('li').destroy();
					if ( $$('#mmg-cards input[name="card"]').length == 1 ) {
						$$('#mmg-cards input[name="card"]').set('checked', 'checked');
						Cubix.ProfileBoost.updateMooniform();
					}
					Cubix.ProfileBoost.loader.enable();
				}
			}).send();
		});
	}
	
	if ( $$('#mmg-cards input[name="card"]').length ) {
		$$('#mmg-cards input[name="card"]').addEvent('change', function() {
			if ( ! this.get('value') ) {
				$$('#mmg-cards input[name="save_info"]').set('disabled', '');
			} else {
				$$('#mmg-cards input[name="save_info"]').set('checked', '');
				$$('#mmg-cards input[name="save_info"]').set('disabled', 'disabled');
			}
			Cubix.ProfileBoost.updateMooniform();
		});
	}
	
	if ( $$('#mmg-cards .set-default') ) {
		$$('#mmg-cards .set-default').addEvent('click', function() {
			var self = this;
			Cubix.ProfileBoost.loader.disable();
			new Request({
				url: '/online-billing-v2/set-default-card?card=' + this.getParent('.card-row').get('card-id'),
				method: 'GET',
				onSuccess: function(response) {
					$$('#mmg-cards .set-default').setStyle('display', 'inline');
					self.setStyle('display', 'none');
					Cubix.ProfileBoost.loader.enable();
				}
			}).send();
		});
	}
};


var monthNames = [],
	weekDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
	daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];


Cubix.ProfileBoost.Calendar = {};
Cubix.ProfileBoost.Calendar.cDate = new Date();
Cubix.ProfileBoost.Calendar.bookedHours = {};
Cubix.ProfileBoost.Calendar.activePackage = {};
Cubix.ProfileBoost.Calendar.init = function() {
	monthNames = $('month-names').get('value').split(',');
	
	Cubix.ProfileBoost.Calendar.bookedHours = JSON.decode($('booked-hours').get('value'));
	Cubix.ProfileBoost.Calendar.activePackage = JSON.decode($('active-package').get('value'));
	
	Cubix.ProfileBoost.Calendar.drawMonth(Cubix.ProfileBoost.Calendar.cDate);
	
	$$('#calendar .prev-btn').addEvent('click', function() {
		Cubix.ProfileBoost.Calendar.cDate.setMonth(Cubix.ProfileBoost.Calendar.cDate.getMonth() - 1);
		Cubix.ProfileBoost.Calendar.drawMonth(Cubix.ProfileBoost.Calendar.cDate);
	});
	$$('#calendar .next-btn').addEvent('click', function() {
		Cubix.ProfileBoost.Calendar.cDate.setMonth(Cubix.ProfileBoost.Calendar.cDate.getMonth() + 1);
		Cubix.ProfileBoost.Calendar.drawMonth(Cubix.ProfileBoost.Calendar.cDate);
	});
	
	
	$$('#time-select .block').addEvent('click', function() {
		if ( ! $$('#calendar td.selected').length ) {
			alert('Select day first.');
			return;
		}
		
		if ( this.hasClass('booked') || this.hasClass('selected-light') || this.hasClass('passed') ) return;
		
		if ( this.hasClass('selected') ) {
			Cubix.ProfileBoost.Calendar.removeBoost(this.get('data-hour'));
		} else {
			Cubix.ProfileBoost.Calendar.addBoost(this.get('data-hour'));
		}
	});
	
	if ( $('package') ) {
		$('package').addEvent('change', function() {
			Cubix.ProfileBoost.Calendar.activePackage.date_activated = this.getSelected()[0].get('data-from');
			Cubix.ProfileBoost.Calendar.activePackage.expiration_date = this.getSelected()[0].get('data-to');
			Cubix.ProfileBoost.Calendar.setActiveDays();
		});
	}
	
	var selectedHours = JSON.decode($('selected-hours').get('value'));
	if ( selectedHours ) {
		selectedHours.each(function(item) {
			item = item.split(':');
			Cubix.ProfileBoost.Calendar.addBoost(item[1], item[0]);
		});
	}
	
};

Cubix.ProfileBoost.Calendar.drawMonth = function(date) {
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	
	if ( month == 13 ) month = 1;
	
	
	$$('#calendar tbody')[0].empty();
	var tr = new Element('tr',{}).inject($$('#calendar tbody')[0]);

	var monthLength = new Date(year, month, 0).getDate();
	previousMonthLength = daysInMonth[month == 1 ? 11 : month - 2];

	startWeekDay = new Date(year, month - 1, 1).getDay();

	if ( startWeekDay == 0 ) startWeekDay = 7;
	var daysToPrepend = startWeekDay - 1;

	var td;
	var weekIndex = 1;


	//Filling empty cells
	for(var i = 1; i <= daysToPrepend; i++) {
		if ( i <= daysToPrepend ) {
			td = new Element('td', {});
		}
		td.inject(tr);

		if ( weekIndex % 7 == 0 ) {
			tr = new Element('tr',{}).inject($$('#calendar tbody')[0]);
			weekIndex = 0;
		}
		weekIndex++;
	}

	for(var i = 1; i <= monthLength; i++) {
		td = new Element('td', {
			'id': + year + '-' + ('0' + month).slice(-2) + '-' + ('0' + i).slice(-2),
			html: '<p>' + i + '</p>'
		});

		td.inject(tr);

		if ( weekIndex % 7 == 0 ) {
			tr = new Element('tr',{}).inject($$('#calendar tbody')[0]);
			weekIndex = 0;
		}
		weekIndex++;

	}
	
	$$('#calendar .month span').set('html', year.toString() + ' ' + monthNames[month - 1]);
	
	Cubix.ProfileBoost.Calendar.setActiveDays();
	Cubix.ProfileBoost.Calendar.setSelectedHours();
};


Cubix.ProfileBoost.Calendar.setActiveDays = function() {
	$$('#calendar td').removeClass('selected').removeClass('active');
	
	var activationDate = new Date(Cubix.ProfileBoost.Calendar.activePackage.date_activated);
	var expirationDate = new Date(Cubix.ProfileBoost.Calendar.activePackage.expiration_date);
	var currDate = new Date();
	
	if ( currDate > activationDate ) {
		activationDate = currDate;
	}
	duration = (expirationDate - activationDate) / (1000*60*60*24) ;
	
	var date = activationDate;
	var day, month;
	for( var i = 0; i < duration; i++ ) {
		date.setDate(date.getDate() + (i == 0 ? 0 : 1));
		
		month = date.getMonth() + 1;
		if ( month == 13 ) month = 1;
		
		month = ('0' + month).slice(-2);
		day = ('0' + date.getDate() ).slice(-2);
		
		if ( $(date.getFullYear() + '-' + month + '-' + day ) ) {
			$(date.getFullYear() + '-' + month + '-' + day).addClass('active');
		}
	}
	
	$$('#calendar td.active').removeEvents().addEvent('click', function() {
		$$('#calendar td').removeClass('selected');
		this.addClass('selected');
		
		$$('#time-select .block').removeClass('booked').removeClass('passed');
		Cubix.ProfileBoost.Calendar.setSelectedHours();
		
		
		if ( Cubix.ProfileBoost.Calendar.bookedHours[this.get('id')] ) {
			Cubix.ProfileBoost.Calendar.bookedHours[this.get('id')].each(function(d) {
				$$('#time-select .block.' + d.hour_from).addClass('booked');
			});
		}
		
		
		var curDate = new Date();
		var selDate = new Date(this.get('id'));
		
		curDate.setHours(0, 0, 0, 0);
		selDate.setHours(0, 0, 0, 0);
		
		if ( curDate.getTime() === selDate.getTime() ) {
			curDate = new Date();
			for( var i = 0; i <= curDate.getHours(); i++ ) {
				$$('#time-select .block.' + i).removeClass('booked').addClass('passed');
			}
		}
	});
};

Cubix.ProfileBoost.Calendar.addBoost = function(hour, date) {
	if ( ! date ) date = $$('#calendar td.selected').get('id');
	
	var item = new Element('div', {
		'class' : 'item',
		id: date + ':' + hour
	});
	new Element('div', {'class': 'remove-btn'}).addEvent('click', function() {
		Cubix.ProfileBoost.Calendar.removeBoost(hour, date);
	}).inject(item);

	new Element('span', {html: date, 'class': 'date'}).inject(item);
	new Element('br').inject(item);
	new Element('span', {
		html: ('0' + hour).slice(-2) + ':00-' + (parseInt(hour) + 1).toString().slice(-2) + ':00',
		'class': 'time'
	}).inject(item);

	new Element('input', {
		type: 'hidden',
		value: date + ':' + hour,
		name: 'boost_profile_dates[]'
	}).inject(item);
	
	item.inject($('selected-container'));
	
	$$('#time-select .' + hour).addClass('selected');
	
	Cubix.ProfileBoost.Calendar.calcOptProductsPrice();
};

Cubix.ProfileBoost.Calendar.removeBoost = function(hour, date) {
	if ( ! date ) date = $$('#calendar td.selected').get('id');
	
	$$('#time-select .' + hour).removeClass('selected').removeClass('selected-light');
	$(date + ':' + hour).destroy();
	
	Cubix.ProfileBoost.Calendar.calcOptProductsPrice();
};

Cubix.ProfileBoost.Calendar.setSelectedHours = function() {
	$$('#time-select .block').removeClass('selected').removeClass('selected-light');
	
	var selectedHours = $$('#selected-container input').get('value');
	selectedHours.each(function(id) {
		id = id.split(':');
		if ( $(id[0]) && $(id[0]).hasClass('selected') ) {
			$$('#time-select .block.' + id[1]).addClass('selected');
		} else {
			$$('#time-select .block.' + id[1]).addClass('selected-light');
		}
	});
};

Cubix.ProfileBoost.Calendar.calcOptProductsPrice = function() 
{
	var boostPrice = $('boost-profile-price').get('value');
	
	var price = 0;
	price += $$('input[name="boost_profile_dates[]"]').length * boostPrice.toInt();
	$('total-price').set('html', price.toFixed(2) + ' EUR');
};


Cubix.ProfileBoost.mooniformInstance = 0;
Cubix.ProfileBoost.moniformInit = function() {
	Cubix.ProfileBoost.mooniformInstance = new Mooniform(container.getElements('input.mooniform'));
};
Cubix.ProfileBoost.updateMooniform = function()
{
	Cubix.ProfileBoost.mooniformInstance.lookup($$('input.mooniform'));
	Cubix.ProfileBoost.mooniformInstance.update();
};

Cubix.ProfileBoost.ecardonPopup = function(checkoutId) {
		
	ecardonOverlay = new Cubix.Overlay(document.body, {has_loader: false, opacity: 0.5, color: '#000', overlay_class: 'ecardon-overlay' });
	ecardonOverlay.disable();
	
	var container = new Element('div', {'class': 'ecardon-popup'}).setStyles({
		left: window.getWidth() / 2 - 460 / 2 ,
		top:  window.getHeight() / 2 - 320 / 2,
		opacity: 0,
        position: 'fixed',
        'z-index': 101,
        'font-size' : '14px'
	}).inject(document.body);
	
	new Request({
		url: '/online-billing-v2/ecardon-popup',
		method: 'get',
		onSuccess: function (resp) {
			container.set('html', resp);

			container.setStyle('opacity', 1);
			new Element('script', {
				text: 'var wpwlOptions = { locale: "' + Cubix.Lang.id + '", style: "plain", onBeforeSubmitCard: function(){ if ( $$(".wpwl-control-cardHolder").get("value") == "") { $$(".wpwl-control-cardHolder").addClass("wpwl-has-error"); $$(".wpwl-button-pay").addClass("wpwl-button-error").set("disabled", "disabled"); new Element("div", {"class": "wpwl-hint wpwl-hint-cardHolderError", "text": "card holder required"}).inject($$(".wpwl-control-cardHolder")[0], "after");  return false} else return true}}'
			}).inject(container, 'before');
			/*new Element('script', {
				text: 'var wpwlOptions = { locale: "' + Cubix.Lang.id + '", style: "plain"}'
			}).inject(container, 'before');*/
			new Element('script', {
				src: "https://oppwa.com/v1/paymentWidgets.js?checkoutId=" + checkoutId
			}).inject(container, 'before');
		}
	}).send();
	
	$$('.ecardon-overlay')[0].addEvent('click', function(){
		container.destroy();
		ecardonOverlay.enable();
	});
};
