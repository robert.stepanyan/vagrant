Cubix.ProfileBoost = {};

$(document).ready( function() {
	//Cubix.ProfileBoost.loader = Cubix.Overlay.initialize($('#profile-boost'), {loader: _st('loader-small.gif'), position: '50%', no_relative: true});
});

Cubix.ProfileBoost.initEscortSelect = function() {
	$('.escort-item').on('click', function(e) {
		//Cubix.ProfileBoost.loader.disable();
		$('#escort-id').val( $(this).attr('data-id') );
		$('form').submit();
	});
};


Cubix.ProfileBoost.initCitySelect = function() {
	$('.city-select-radio').on('click', function(e) {
		$('.city-select-radio').removeClass('checked');
		$(this).addClass('checked');

		console.log( $( e.target ).attr('id') );
		if( $( e.target ).attr('id') == 'city-select' && $( e.target ).val() ) return false;
		
		if ( $(this).attr('data-value') == 'index' ) {
			$('#city-select')
				.attr('disabled', 'disabled')
				.val('');
			
			$('#city-id').val('index');
		} else {
			$('#city-select').removeAttr('disabled');
			$('#city-id').val('');
		}
	});
	
	$('#city-select').on('change', function() {
		//console.log( $(this).find('option:selected').val() );
		$('#city-id').val( $(this).find('option:selected').val() );
		return false;
	});
	
	$('.btn-next').on('click', function() {
		//Cubix.ProfileBoost.loader.disable();
		$('form').submit();
	});
	
};

Cubix.ProfileBoost.initHourSelect = function() {
	Cubix.ProfileBoost.Calendar.init();
	
	$('.btn-next').on('click', function() {
		//Cubix.ProfileBoost.loader.disable();
		$('form').submit();
	});
};

Cubix.ProfileBoost.initConfirmation = function() {
	Cubix.ProfileBoost.moniformInit();
	
	var selectedHours = jQuery.parseJSON( $('#selected-hours').val() );

	if ( selectedHours ) {
		$.each(selectedHours, function(index, value) {
			//console.log( value );
			item = value.split(':');
			Cubix.ProfileBoost.Calendar.addBoost( item[1], item[0] );
		});
	}

	$('.btn-buy-now').on('click', function () {
		//Cubix.ProfileBoost.loader.disable();

		var selectedMethod = $('.payment-option.checked').attr('data-method');
		var oldText = $('.btn-buy-now').html();

		if (selectedMethod == 'twispay') {

			$('.btn-buy-now').html('Loading ...');

			$.ajax({
				type: 'post',
				data: $('form').serializeArray(),
				success: function (response) {
					$('.btn-buy-now').html(oldText);
					response = JSON.parse(response);

					if (response.status == 'error') {
						for (name in response.msgs) {
							alert(response.msgs[name]);
							break;
						}
					} else {
						if (response.form) {
							var formHtml = response.form;
							$('body').append(formHtml.replace(/ \"/g, "\""));
							$('#twispay-payment-form').submit();
							return;
						} else {
							console.error(response);
						}
					}
				}
			});
		} else {
			$('form').submit();
		}

	});
	
	var _mmg = $('#mmg-cards');
	if ( _mmg.length ) {
		if ( $('#payment-gateway').val() != 'mmgbill' ) {
			_mmg.fadeOut();
		}
	}
	
	// Ecardon POPUP 
	
	if ($('#checkout-id').length ){
		MCubix.ecardonPopup($('#checkout-id').val());
	}
	
	$('.payment-option').on('click', function() {
		$('.payment-option').removeClass('checked');
		$(this).addClass('checked');
		$('#payment-gateway').val( $(this).attr('data-method') );
	});

	var _mmg_card = $('#mmg-cards .forget-card');
	/*if ( _mmg_card.length ) {
		_mmg_card.on('click', function() {
			var self = this;
			Cubix.ProfileBoost.loader.disable();
			new Request({
				url: '/online-billing-v2/forget-credit-card?card=' + this.getParent('.card-row').get('card-id'),
				method: 'GET',
				onSuccess: function(response) {
					self.getParent('li').destroy();
					if ( $$('#mmg-cards input[name="card"]').length == 1 ) {
						$$('#mmg-cards input[name="card"]').set('checked', 'checked');
						Cubix.ProfileBoost.updateMooniform();
					}
					Cubix.ProfileBoost.loader.enable();
				}
			}).send();
		});
	}
	
	if ( $$('#mmg-cards input[name="card"]').length ) {
		$$('#mmg-cards input[name="card"]').addEvent('change', function() {
			if ( ! this.get('value') ) {
				$$('#mmg-cards input[name="save_info"]').set('disabled', '');
			} else {
				$$('#mmg-cards input[name="save_info"]').set('checked', '');
				$$('#mmg-cards input[name="save_info"]').set('disabled', 'disabled');
			}
			Cubix.ProfileBoost.updateMooniform();
		});
	}
	
	if ( $$('#mmg-cards .set-default') ) {
		$$('#mmg-cards .set-default').addEvent('click', function() {
			var self = this;
			Cubix.ProfileBoost.loader.disable();
			new Request({
				url: '/online-billing-v2/set-default-card?card=' + this.getParent('.card-row').get('card-id'),
				method: 'GET',
				onSuccess: function(response) {
					$$('#mmg-cards .set-default').setStyle('display', 'inline');
					self.setStyle('display', 'none');
					Cubix.ProfileBoost.loader.enable();
				}
			}).send();
		});
	}*/
};


var monthNames = [],
	weekDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
	daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];


Cubix.ProfileBoost.Calendar = {};
Cubix.ProfileBoost.Calendar.cDate = new Date();
Cubix.ProfileBoost.Calendar.bookedHours = {};
Cubix.ProfileBoost.Calendar.activePackage = {};

Cubix.ProfileBoost.Calendar.init = function() {
	monthNames = $('#month-names').val().split(',');
	
	Cubix.ProfileBoost.Calendar.bookedHours = jQuery.parseJSON($('#booked-hours').val());
	Cubix.ProfileBoost.Calendar.activePackage = jQuery.parseJSON($('#active-package').val());
	
	Cubix.ProfileBoost.Calendar.drawMonth(Cubix.ProfileBoost.Calendar.cDate);
	
	$('#calendar .prev-btn').on('click', function() {
		Cubix.ProfileBoost.Calendar.cDate.setMonth(Cubix.ProfileBoost.Calendar.cDate.getMonth() - 1);
		Cubix.ProfileBoost.Calendar.drawMonth(Cubix.ProfileBoost.Calendar.cDate);
	});

	$('#calendar .next-btn').on('click', function() {
		Cubix.ProfileBoost.Calendar.cDate.setMonth(Cubix.ProfileBoost.Calendar.cDate.getMonth() + 1);
		Cubix.ProfileBoost.Calendar.drawMonth(Cubix.ProfileBoost.Calendar.cDate);
	});
	
	
	$('#time-select .block').on('click', function() {
		if ( ! $('#calendar td.selected').length ) {
			alert('Select day first.');
			return;
		}
		
		if ( $(this).hasClass('booked') || $(this).hasClass('selected-light') || $(this).hasClass('passed') ) return;
		
		if ( $(this).hasClass('selected') ) {
			Cubix.ProfileBoost.Calendar.removeBoost( $(this).attr('data-hour') );
		} else {
			Cubix.ProfileBoost.Calendar.addBoost( $(this).attr('data-hour') );
		}
	});
	
	if ( $('#package').length ) {
		$('#package').on('change', function() {
			Cubix.ProfileBoost.Calendar.activePackage.date_activated = $(this).find('option:selected').attr('data-from');
			Cubix.ProfileBoost.Calendar.activePackage.expiration_date = $(this).find('option:selected').attr('data-to');
			Cubix.ProfileBoost.Calendar.setActiveDays();
		});
	}
	
	var selectedHours = jQuery.parseJSON( $('#selected-hours').val() );
	if ( selectedHours ) {
		$.each(selectedHours, function(index, value) {
			var item = value.split(':');
			Cubix.ProfileBoost.Calendar.addBoost(item[1], item[0]);
		});
	}
	
};

Cubix.ProfileBoost.Calendar.drawMonth = function(date) {
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	
	if ( month == 13 ) month = 1;
	
	var _parent = $('#calendar tbody');
	_parent.empty();

	var tr = $('<tr></tr>');
	_parent.append( tr );

	var monthLength = new Date(year, month, 0).getDate();
	var previousMonthLength = daysInMonth[month == 1 ? 11 : month - 2];

	var startWeekDay = new Date(year, month - 1, 1).getDay();

	if ( startWeekDay == 0 ) startWeekDay = 7;
	var daysToPrepend = startWeekDay - 1;

	var td;
	var weekIndex = 1;


	//Filling empty cells
	for( var i = 1; i <= daysToPrepend; i++ ) {
		if ( i <= daysToPrepend ) {
			td = $('<td></td>');
		}
		tr.append( td );

		if ( weekIndex % 7 == 0 ) {
			tr = $('<tr></tr>');
			_parent.append(tr);
			weekIndex = 0;
		}
		weekIndex++;
	}

	for( var i = 1; i <= monthLength; i++ ) {
		td = $('<td></td>').attr('id', year + '-' + ('0' + month).slice(-2) + '-' + ('0' + i).slice(-2)).html('<p>' + i + '</p>');

		tr.append(td);

		//console.log(weekIndex);

		if ( weekIndex % 7 == 0 ) {
			tr = $('<tr></tr>');
			$('#calendar tbody').append(tr);
			weekIndex = 0;
		}
		weekIndex++;

	}
	
	$('#calendar .month span').html(year.toString() + ' ' + monthNames[month - 1]);
	
	Cubix.ProfileBoost.Calendar.setActiveDays();
	Cubix.ProfileBoost.Calendar.setSelectedHours();
};


Cubix.ProfileBoost.Calendar.setActiveDays = function() {
	$('#calendar td').removeClass('selected').removeClass('active');
	
	var activationDate = new Date(Cubix.ProfileBoost.Calendar.activePackage.date_activated);
	var expirationDate = new Date(Cubix.ProfileBoost.Calendar.activePackage.expiration_date);
	var currDate = new Date();
	
	if ( currDate > activationDate ) {
		activationDate = currDate;
	}
	var duration = (expirationDate - activationDate) / (1000*60*60*24) ;
	
	var date = activationDate;

	var day, month;
	for( var i = 0; i < duration; i++ ) {
		date.setDate(date.getDate() + (i == 0 ? 0 : 1));
		
		month = date.getMonth() + 1;
		if ( month == 13 ) month = 1;
		
		month = ('0' + month).slice(-2);
		day = ('0' + date.getDate() ).slice(-2);
		
		if ( $('#' + date.getFullYear() + '-' + month + '-' + day ) ) {
			$('#' + date.getFullYear() + '-' + month + '-' + day).addClass('active');
		}
	}
	
	$('#calendar td.active').off().on('click', function() {
		$('#calendar td').removeClass('selected');
		$(this).addClass('selected');
		
		$('#time-select .block').removeClass('booked').removeClass('passed');
		Cubix.ProfileBoost.Calendar.setSelectedHours();
		
		
		if ( Cubix.ProfileBoost.Calendar.bookedHours[$(this).get('id')] ) {
			Cubix.ProfileBoost.Calendar.bookedHours[$(this).get('id')].each(function(d) {
				$('#time-select .block.' + d.hour_from).addClass('booked');
			});
		}
		
		
		var curDate = new Date();
		var selDate = new Date( $(this).attr('id') );
		
		curDate.setHours(0, 0, 0, 0);
		selDate.setHours(0, 0, 0, 0);
		
		if ( curDate.getTime() === selDate.getTime() ) {
			curDate = new Date();
			for( var i = 0; i <= curDate.getHours(); i++ ) {
				$('#time-select .block.' + i).removeClass('booked').addClass('passed');
			}
		}
	});
};

Cubix.ProfileBoost.Calendar.addBoost = function(hour, date) {
	if ( ! date ) date = $('#calendar td.selected').attr('id');
	
	var item = $('<div></div>').addClass('item').attr('id', date + '--' + hour);

	var _elm_rev = $('<div></div>').addClass('remove-btn');
	_elm_rev.on('click', function() {
		Cubix.ProfileBoost.Calendar.removeBoost(hour, date);
	});
	item.append( _elm_rev );

	var _elm_d = $('<span></span>').addClass('date').html(date);
	item.append( _elm_d).append('</br>');

	var __hour = ( hour.length == 1 ) ? ( '0' + ( parseInt(hour) + 1 ) ) : ( parseInt(hour) + 1 );
	var _elm_t = $('<span></span>').addClass('time').html( ('0' + hour).slice(-2) + ':00-' + __hour.toString().slice(-2) + ':00' );
	item.append( _elm_t);

	var _elm_i = $('<input>').attr({type : 'hidden', name : 'boost_profile_dates[]'}).val( date + ':' + hour);
	item.append( _elm_i);

	$('#selected-container').append(item);
	
	$('#time-select .' + hour).addClass('selected');
	
	Cubix.ProfileBoost.Calendar.calcOptProductsPrice();
};

Cubix.ProfileBoost.Calendar.removeBoost = function(hour, date) {
	if ( ! date ) date = $('#calendar td.selected').attr('id');
	
	$('#time-select .' + hour).removeClass('selected').removeClass('selected-light');
	//console.log( '#' + date + ':' + hour );
	$('#' + date + '--' + hour).remove();
	
	Cubix.ProfileBoost.Calendar.calcOptProductsPrice();
};

Cubix.ProfileBoost.Calendar.setSelectedHours = function() {
	$('#time-select .block').removeClass('selected').removeClass('selected-light');
	
	var selectedHours = $('#selected-container input');

	if( selectedHours.length ) {
		$.each(selectedHours, function (index, value) {
			//console.log( value );
			var id = $(value).val().split(':');

			if ($(id[0]) && $(id[0]).hasClass('selected')) {
				$('#time-select .block.' + id[1]).addClass('selected');
			} else {
				$('#time-select .block.' + id[1]).addClass('selected-light');
			}
		});
	}
};

Cubix.ProfileBoost.Calendar.calcOptProductsPrice = function() 
{
	var boostPrice = $('#boost-profile-price').val();
	
	var price = 0;
	//console.log( $('input[name="boost_profile_dates[]"]').length * boostPrice );
	price += parseInt( $('input[name="boost_profile_dates[]"]').length * boostPrice );
	$('#total-price').html(price.toFixed(2) + ' EUR');
};


Cubix.ProfileBoost.mooniformInstance = 0;

Cubix.ProfileBoost.moniformInit = function() {
	//Cubix.ProfileBoost.mooniformInstance = new Mooniform($$('.container').getElements('input.mooniform'));
};

Cubix.ProfileBoost.updateMooniform = function()
{
	Cubix.ProfileBoost.mooniformInstance.lookup($$('input.mooniform'));
	Cubix.ProfileBoost.mooniformInstance.update();
};

MCubix.ecardonPopup = function(checkoutId) {
	
	MCubix.PopupAnim.init(true, {
		ajax: {
			url: '/online-billing-v2/ecardon-popup',
			method: 'GET',
			callback: MCubix.ecardonPopupCallback,
			callback_vars: { checkoutId: checkoutId}
		},
		css:{
			width: '90%',
			'font-size': '1.2em'
		}, 
		centered: true,
		closing: false
	});


}
	
MCubix.ecardonPopupCallback = function(data) {

	$.ajaxSetup({cache:true});
	var script = document.createElement("script");
	script.src = 'https://oppwa.com/v1/paymentWidgets.js?checkoutId=' + data.checkoutId;
	$('head').append(script);
	var paymetnOptions = '<script>var wpwlOptions = { locale: "<?= $this->lang() ?>", onBeforeSubmitCard: function(){ '+ 
			'if ( $(".wpwl-control-cardHolder").val() === "" ){' +
				'$(".wpwl-control-cardHolder").addClass("wpwl-has-error"); ' +
				'$(".wpwl-control-cardHolder").after("<div class=\'wpwl-hint wpwl-hint-cardHolderError\'>Invalid card holder</div>"); ' +
				'$(".wpwl-button-pay").addClass("wpwl-button-error").attr("disabled", "disabled");return false;}' +
			'else return true; }} </' + 'script>';
	$('head').before(paymetnOptions);
}


