var PeriodicalExecuter = new Class({
    // name: 'PeriodicalExecuter',
    initialize: function(callback, frequency) {

        this.callback = callback;
        this.frequency = frequency;
        this.currentlyExecuting = false;

        this.registerCallback()
    },

    registerCallback: function() {

        this.stop();
        this.timer = setInterval(this.onTimerEvent.bind(this), this.frequency * 1000);
        return this
    },

    execute: function() {

        this.callback(this);
        return this
    },

    stop: function() {

        if (!this.timer) return this;
        clearInterval(this.timer);
        this.timer = null;
        return this
    },

    onTimerEvent: function() {

        if (!this.currentlyExecuting) {

            try {

                this.currentlyExecuting = true;
                this.execute();
            } finally {

                this.currentlyExecuting = false;
            }
        }

        return this
    }
});

var CountDown = new Class({

    Implements: [Options, Events],
    initialize: function (options) {

        this.setOptions(options);
        if(!this.options.date instanceof Date) this.options.date = new Date(this.options.date);

        this.timer = new PeriodicalExecuter(this.update.bind(this), (this.options.frequency || 1000) / 100);
    },
    stop: function () {

        this.timer.stop();
        return this
    },
    start: function () {

        this.timer.registerCallback();
        return this
    },
    update: function () {

        var millis = Math.max(0, this.options.date.getTime() - new Date().getTime()),
            time = Math.floor(millis / 1000),
            stop = time == 0,
            countdown = {
                days: Math.floor(time / (60 * 60 * 24)),
                time: time,
                millis: millis
            };

        time %= (60 * 60 * 24);

        countdown.hours = Math.floor(time / (60 * 60));
        time %= (60 * 60);
        countdown.minutes = Math.floor(time / 60);
        countdown.second = time % 60;

        this.fireEvent('onChange', countdown);

        if(stop) {

            this.timer.stop();
            this.fireEvent('onComplete');
        }
    }
});
