Cubix.PowerCash = {};

Cubix.PowerCash.init = function (url) {

	Cubix.PowerCash.overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#000', z_index: 200, overlay_class: 'powercash-overlay' });
	Cubix.PowerCash.overlay.disable();

	var popup = new Element('div', {
			'id': 'powercash-popup'
		}).inject(document.body);

	new Element('iframe', {
		'id': 'iframeId',
		'frameborder': '0',
		'scrolling': 'no',
		'src': url,
	}).inject(popup);
	$$('#powercash-popup').addClass('open');

	var closePopup = new Element('div', {
		'id': 'close-popup'
	}).inject(popup);
	closePopup.set('styles', {
		width: '30%',
		height: '120px',
		position: 'absolute',
		left: '0',
		right: '0',
		top : '330px',
		cursor : 'pointer',
		'margin-left' : 'auto',
		'margin-right' : 'auto'
	});

	$$('#close-popup').addEvent('click', function() {
		$$('.powercash-overlay').each(function (elm,indx){
			elm.destroy();
		});

		$('powercash-popup').destroy();
		setTimeout(function (){
			if ($('powercash-popup'))
			{
				$('powercash-popup').destroy();
			}
		},500);
		$$('.overlay').each(function (elem,indx){
			elem.destroy();
		});

		if ($$('.self-checkout-wizard-wrapper'))
		{
			$$('.self-checkout-wizard-wrapper').destroy();
		}
		Cubix.PowerCash.overlay.enable();
	});

		$$('.powercash-overlay').addEvent('click', function() {
		$('powercash-popup').destroy();
		Cubix.PowerCash.overlay.enable();
	});

	Cubix.PowerCash.childListener();
	return true;
}

Cubix.PowerCash.childListener = function(){

	window.addEventListener("message", function(e){
		$('powercash-popup').destroy();
		window.location.href = e.data;
		//Cubix.PowerCash.overlay.enable();
	});
};
