function setCookie(name, value) {
    var expiration_date = new Date();   
    expiration_date.setFullYear(expiration_date.getFullYear(), expiration_date.getMonth(), expiration_date.getDate() + 7 );
    expiration_date = expiration_date.toUTCString();
    var cookie_string = escape(name) + "=" + escape(value) + "; expires=" + expiration_date;
    document.cookie = cookie_string;
}

function deleteCookie (name) {
    var expiration_date = new Date();
    expiration_date.setYear (expiration_date.getYear() - 1);
    expiration_date = expiration_date.toUTCString();
    var cookie_string = escape (name) + "=; expires=" + expiration_date;
    document.cookie = cookie_string;
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
            x = ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
            y = ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
            x = x.replace(/^\s+|\s+$/g,"");
            if (x == c_name) {
                    return unescape(y);
            }
    }
    return false;
}


var profileTabs = function() {	
	var hash = window.location.hash;

	if ( hash == "" || hash == "#" ) {
		hash = "#info";
	}
	
	if ( getCookie("hideTabs") == "true" ) {
		hash = "#info";
		$("profile-tabs").addClass("hideTabs");
		$("show_tabs").show();
		$("hide_tabs").hide();
	}
	
	$$("#profile-tabs a").getParent('li').removeClass("active");
	
	var fullHash = hash.split("=");
	var hash = fullHash[0];
	
	/*if ( fullHash[1] == "map" ) {
		console.log(fullHash[1]);
		$(".NContact").hide();
		$(".NMap").show();
		$("#contactTab").removeClass("active");
		$("#mapTab").addClass("active");
	} else {
		$(".NContact").show();
		$(".NMap").hide();
		$("#contactTab").addClass("active");
		$("#mapTab").removeClass("active");
	}*/

	if( ! $$("#profile-tabs a[href="+hash+"]").length) return ;
	$$("#profile-tabs a[href="+hash+"]")[0].getParent('li').addClass("active");
		
    var el = $$("#right a[rel="+hash.replace("#", "")+"]")[0];
	if(el) {
        var scroll = new Fx.Scroll(
             $("profile-container"),
            { wait: false, duration: 700, transition: Fx.Transitions.Quad.easeInOut }
        );
        scroll.toElement(el);
    }	
};

var initGalleryTabs = function() {
	tabAnimation($$('#gallery-tabs li')[0]);
	$$('#gallery-tabs li').addEvent('click',function(e){
		e.stop();
		tabAnimation(this);
		showGalleryTab(this);
	})
}

var tabAnimation = function(tab){
	var container = $('gallery-tabs');
	curActive = container.getElement('.tab.active');
	container.getElements('.active').removeClass('active');
	tab.addClass('active');
	tab.removeClass('gradient');
	var fxActive = new Fx.Morph(tab, { transition: Fx.Transitions.Quint.easeInOut, duration: 1000, link: 'cancel',
	onComplete: function(){
		tab.addClass('gradient');
	}});
	fxActive.start({'background-color' : '#2461b6',
					'margin-top' : 0,
					'height' : '26px',
					'line-height' : '35px'});

	if(curActive){
		curActive.removeClass('gradient');
		var fxPassive = new Fx.Morph(curActive, { transition: Fx.Transitions.Quint.easeInOut, duration: 1000, link: 'cancel',
		onComplete: function(){
			curActive.addClass('gradient');
		}});
		fxPassive.start({'background-color' : '#e6e6e6',
					'margin-top' : '10px',
					'line-height' : '20px',
					'height' : '16px' });
	}		  
}

var showGalleryTab = function(tab) {

	var a = tab.getElement('a');
	var galleryId = a.get('rel');
	var url = a.get('href');
	var content = $('gallery-content-' + galleryId);
	
	if (content.get('html').clean().length == 0) {
		var overlay = new Cubix.Overlay($$('.gallery-contents')[0], {
			loader: _st('loader-small.gif'),
			position: '50%'
		});
		overlay.disable();
		
		new Request({
			url: url,
			method: 'get',
			onSuccess: function (resp) {
				content.set('html',resp);
				cleanNone(content);
				initGallery();
				initLoadMoreBtn();
				overlay.enable();
			}.bind(this)
		}).send();

	}
	else{
		cleanNone(content);
	}
	
}

var cleanNone = function(content)
{
	$$('.gallery-content').each(function(it, index){
		if(it.hasClass('none') == false){
			it.addClass('none');
		}
	});
	content.removeClass('none');
}

var initLoadMoreBtn = function() {

	
	$$('.load-more').addEvent('click', function(e){
		e.stop();
		var more_photos_list = $(this).getParent('.more-photos').getElement('.more-photos-list');	
		more_photos_list.removeClass('none');
		this.destroy();

		resizeProfile();

	});
};


var reorderTabs = function(){
	var tabs_count = $$("#profile-tabs li").length;
	$$("#profile-tabs li").each(function(it, index){
		it.setStyle('z-index', tabs_count - index);
	});
};

var initCamPicsShuffle = function () {
	try {
		var block = $$('#cam-block');
		block.setStyle('position', 'relative');
		if (!block) return false;
		var url = block.get('data-cam-screenshot')[0];
		var url2 = url + '?t=' + Date.now();
		if (url) {
			$$('.cam-pic-img').setStyle('opacity', '0');
				$$('.cam-pic-img').setStyle('-webkit-backface-visibility', 'hidden');
				$$('.cam-pic-img').setStyle('position', 'absolute');
				$$('.cam-pic-img').setProperty('src', url2);
			// $$('.cam-pic-img').src('url(' + url + '?t=' + Date.now() + ')');
			setTimeout(initCamPicsShuffle, 5000);
			$$('.cam-pic-img').fade('in');
		}
	} catch (e) {
		console.log("initCamPicsShuffle function error:", e)
	}
};

window.addEvent('domready', function(){
	
	reorderTabs();
	
	profileTabs();
	
	if ( Browser.ie && Browser.version < 9 ) {
		var $hash = window.location.hash;
		setInterval(function() {
			$hash2 = window.location.hash;
			if ( $hash != $hash2 ) {
				$hash = $hash2;
				profileTabs();
			}
		}, 50);
	} else {
		window.addEvent("hashchange", function() {
			profileTabs();
		});
	}
	
	initGalleryTabs();
	initGallery();
	initCamPicsShuffle();
	initLoadMoreBtn();
	
	/*var $leftHeight = $("left").getSize().y;
	var $profileHeight = $("profile-container").getSize().y - 4700;
	if ( $leftHeight > $profileHeight ) {
		$("profile-container").setStyle('height', $leftHeight);
	} else {
		$("profile-container").setStyle('height', $profileHeight);
	}*/
	
	
	$("show_tabs").addEvent("click", function() {
		deleteCookie('hideTabs');
		$("profile-tabs").removeClass("hideTabs");
		$(this).hide();
		$("hide_tabs").show();
	});
	$("hide_tabs").addEvent("click", function() {
		setCookie('hideTabs', 'true');
		$("profile-tabs").addClass("hideTabs");
		$(this).hide();
		$("show_tabs").show();
		window.location.hash = "";
	});
	
	/*$("contactTab").addEvent("click", function() {
		$$(".NContact")[0].show();
		$$(".NMap")[0].hide();
		$("contactTab").addClass("active");
		$("mapTab").removeClass("active");
	});
	$("mapTab").addEvent("click", function() {
		$$(".NContact")[0].hide();
		$$(".NMap")[0].show();
		$("mapTab").addClass("active");
		$("contactTab").removeClass("active");
	});*/
	
	
	if ( $(profileVars.lang + '_chat_with_me_btn') ) {
		var chat_btn = $(profileVars.lang + '_chat_with_me_btn');

		if ( ! profileVars.currentUser ) {
		chat_btn.addEvent('click', function(e){
			e.stop();
			Cubix.Popup.Show('489', '652');
		});
		} else {
			chat_btn.addEvent('click', function(e){
				e.stop();
				//window.open('http://www.escortforumit.xxx/index/chat?init_private=<?= $this->escort->username ?>', 'escortforum', 'height=620, width=820');
				window.open(profileVars.chatUrl, 'escortforum', 'height=620, width=820');
			});
		}
	}
	
	if ( $('sedcard-views') ) {
		var overlay = new Cubix.Overlay($('sedcard-views'), {
			loader: _st('loader-circular-tiny.gif'),
			position: '50%',
			offset: {
				left: 0,
				top: -1
			}
		});

		overlay.disable();

		var link = '';
		if ( profileVars.lang != profileVars.defLang ) {
			link = "&lang_id=" + profileVars.lang ;
		}

		new Request({
			url: '/index/sedcard-total-views?escort_id=' + profileVars.escortId + link,
			method: 'get',
			onSuccess: function (resp) {
				$('sedcard-views').set('html', resp);
				overlay.enable();
				if ( resp.length == 0 ){
					$('sedcard-views').setStyle('display','none');
				}
			}
		}).send();
	}
	
	$$('.tell-friend').addEvent('click', function(e){
		e.stop();
		Cubix.RPopup.url = this.get('rel');
		Cubix.RPopup.type = this.get('class');
		Cubix.RPopup.location = document.body;
		
		var c = this.getCoordinates(document.body);
		Cubix.RPopup.Show(c.left + 110, c.top);
	});
	
	$$('.susp-photo').addEvent('click', function(e){
		e.stop();
		Cubix.RPopup.url = this.get('rel');
		Cubix.RPopup.type = this.get('class');
		Cubix.RPopup.location = document.body;
		
		var c = this.getCoordinates(document.body);
		Cubix.RPopup.Show(c.left + 110, c.top);
	});

	$$('.report-problem').addEvent('click', function(e){
		e.stop();
		Cubix.RPopup.url = this.get('rel');
		Cubix.RPopup.type = this.get('class');
		Cubix.RPopup.location = document.body;
		
		var c = this.getCoordinates(document.body);
		Cubix.RPopup.Show(c.left + 130, c.top);
	});	
	
	$$('.go-top').addEvent('click', function(){
		var myFx = new Fx.Scroll(window,{
			offset: {
				'x': 0,
				'y': -5
			}
		});
		//myFx.toTop();
		myFx.toElement('header');
	});
	
	new ScrollSpy({
		min: 1,
		max: 200000,
		onEnter: function(position,state,enters) {
			$$('.go-top').set('styles', {
				position: 'fixed',
				bottom: '25px',
				right: '10px',
				'z-index': '100'
			});
		},
		onLeave: function(position,state,leaves) {
			$$('.go-top').set('style', '');
		},
		container: window
	});
	
	if ( $('voting-widget') ) {
		
		var overlay = new Cubix.Overlay($('voting-widget'), {
			loader: _st('loader-circular-tiny.gif'),
			position: '50%',
			no_relative: true,
			offset: {
				left: 0, top: 0
			}
		});
	
		var form = $('voting-widget').getElement('form');

		if ( form ) {
			form.set('send', {
				onSuccess: function (resp) {
					resp = JSON.decode(resp);

					if ( 'error' == resp.status ) {
						alert(resp.desc);
						overlay.enable();
						return;
					}

					$('voting-widget').getElement('.ajax-container').set('html', resp.html);

					overlay.enable();
				}
			}).addEvent('submit', function (e) {
				e.stop();

				if ( 'true' != Cubix.Comments.is_member ) {
					return Cubix.Popup.Show('500', '520');
				}

				overlay.disable();
				this.send();
			});
		}
	}
	
	if($('instant-book-btn')){
		$('instant-book-btn').addEvent('click', function(){
			Cubix.instantbookPopup.Show(JSON.decode(this.get('data-info')));
		});
	}
	
	if ( $$('.ext-site').length > 0 ) {
		new Request({
			url: '/' + profileVars.lang + '/escorts/ajax-get-personal-site?escort_id=' + profileVars.escortId + '&has_rates=' + window.hasRates + '&has_services=' + window.hasServices,
			method: 'get',
			onSuccess: function (resp) {
				if ( resp.length ) {
					$$('.ext-site')[0].set('html', resp);
					$$('.ext-site')[0].removeClass('none');
				}
			}
		}).send();
	}
	
	if ($defined($$('.chat_login')))
	{
		$$('.chat_login').addEvent('click', function(e) {
			e.stop();
			
			Cubix.Popup.Show('500', '520');
		});
	}
	if ($defined($$('.upcoming-buttons')[0])){
		var upcomingBox = $$('.upcoming')[0];
		var boxHeight = upcomingBox.getHeight() +'px';
		var showButton = $('show-all-upcoming');
		var hideButton = $('hide-upcoming');
		upcomingBox.setStyle('maxHeight','130px');
		
		showButton.addEvent('click',function(){
			var self = this;
			self.fade('out');
			var effect = new Fx.Tween(upcomingBox,  {
								duration: 1000,
								transition:  Fx.Transitions.Quart.easeInOut,
								onComplete: function(){
									
									hideButton.fade('in');
								}

				});
			effect.start('maxHeight', boxHeight);
			return false;
		})
		
		hideButton.addEvent('click',function(){
			var self = this
			self.fade('out');
			var effect = new Fx.Tween(upcomingBox,  {
								duration: 1000,
								transition:  Fx.Transitions.Quart.easeInOut,
								onComplete: function(){
									
									showButton.fade('in');
								}

				});
			effect.start('maxHeight', '130px');
			return false;
		})
	}

	if( !headerVars.currentUser ){
		if($('video-chat-box')){
			$$('#video-chat-box .vc-button').addEvent('click', function(){
				Cubix.Cam.Popup.url = '/ef-cams/login/';
				Cubix.Cam.Popup.Show(400,375);
			})
		}
	}else{
		if($('video-chat-box') && headerVars.currentUser.user_type == 'member'){
			$$('#video-chat-box .vc-button').addEvent('click', function(){
				var overlay = new Cubix.Overlay($$('body')[0], {
					color: '#000',
					has_loader: false,
				});
				overlay.disable();
				var duration = $(this).get('data-type');
				var price = $(this).get('data-price');
				var escortId = $('escort_id').get('value');
				new Request.JSON({
					url: '/ef-cams/go-to-book',
					method: 'post',
					data: {
						'escort_id' : escortId,
						'duration' : duration,
						'price' : price
					},
					onSuccess: function (resp) {
						if(resp.url){
							//window.location = resp.url;
							window.open(resp.url);
							overlay.enable();
						}
					}
				}).send();
			})
		}else{
			$$('#video-chat-box .vc-button').addEvent('click', function(){
				alert('Permission denied !!!!');
			})
		}
	}
	
});