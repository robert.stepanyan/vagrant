Cubix.ProfileBumps = {}
Cubix.ProfileBumps.cryptoPrice = false;

window.addEvent('domready', function () {
    MCubix.OverlayV2.init(true);
    Cubix.ProfileBumps.selectionEvents();
    Cubix.ProfileBumps.limitBumpsAmount();
    Cubix.ProfileBumps.initBumpAmountUpdate();
	Cubix.ProfileBumps.initPaymentOptionSelector();
    MCubix.OverlayV2.init(false);
});

Cubix.ProfileBumps.initBumpAmountUpdate = function () {
	var priceTag = Cubix.ProfileBumps.cryptoPrice ? 'data-crypto-price' : 'data-price';
	$$('#bump-selected-amount').set('html', $$('#bumps-count-select').getSelected()[0].get(priceTag));
    /*$$('#bumps-count-select').addEvent('change', function (e) {
		var priceTag = Cubix.ProfileBumps.cryptoPrice ? 'data-crypto-price' : 'data-price';
		$$('#bump-selected-amount').set('html', this.getSelected()[0].get(priceTag));
    });*/
};

Cubix.ProfileBumps.selectionEvents = function () {
    $$("#bumps-count-select").addEvent('change', function () {
        var should_be_selected = this.getElements(":selected")[0].get('class');
        var should_be_selected2 = $$("#bumps-frequency").getElements(":selected")[0].get('class');

        if(should_be_selected != should_be_selected2){
            $$("#bumps-frequency").getElement('.' + should_be_selected)[0].selected=true;
        }
        Cubix.ProfileBumps.limitBumpsAmount();
        Cubix.ProfileBumps.initBumpAmountUpdate();
    });

    $$("#bumps-frequency").addEvent('change', function () {
        var should_be_selected = this.getElements(":selected")[0].get('class');
        var should_be_selected2 = $$("#bumps-count-select").getElements(":selected")[0].get('class');

        if(should_be_selected != should_be_selected2){
            $$("#bumps-count-select").getElement('.' + should_be_selected)[0].selected=true;
        }
        Cubix.ProfileBumps.limitBumpsAmount();
        Cubix.ProfileBumps.initBumpAmountUpdate();
    });
};

Cubix.ProfileBumps.limitBumpsAmount = function () {
    var package_expiration_mins = $$('#minutes_left_to_package_expiration').get('value')[0];
    var bumps_frequency = $$('#bumps-frequency').getElements(":selected")[0].get('data-hour')[0];
    var bumps_option = $$('#bumps-frequency').getElements(":selected")[0].get('value')[0];
    var bumps_count_select = $$('#bumps-count-select').getElements(":selected")[0].get('value')[0];

    var hour_by_min = 1;
    if (bumps_option == 'bump-option-6') {
        hour_by_min = 1;
    } else {
        hour_by_min = 60;
    }

    var bump_lifespan = bumps_frequency * hour_by_min * bumps_count_select;

    if (bump_lifespan > package_expiration_mins) {
        $$('#selected-bump-exceed-package-expiration').set('html', '');
        $$('#selected-bump-exceed-package-expiration').set('html', 'Bump frequency exceed package expiration date');
        $$('.btn-bump-checkout').addClass('disable');
        $$('.btn-bump-checkout').removeEvents();
    } else {
		$$('#selected-bump-exceed-package-expiration').set('html', '');
        $$('.btn-bump-checkout').removeClass('disable');
        Cubix.ProfileBumps.initBumpCheckout();
    }
};

// For Agency 23.06.2020
Cubix.ProfileBumps.AgencyEscortData = function () {
    if (!$$('#bump-escort-select')) return false;
    $$('#bump-escort-select').addEvent('change', function (e) {
        MCubix.OverlayV2.init(false);
        var escort_id = $(this).getElements(":selected")[0].get('value');

        new Request({
            url: '/profile-bumps/escort-data',
            method: 'POST',
            data: {'escort_id': escort_id},
            onSuccess: function (response) {
                response = JSON.decode(response);
                if(response.status == 'success'){


                    $$('#minutes_left_to_package_expiration').set('value', response.minutes_left_to_package_end)

                    $$('#selected-escort-package-name').set('html', '');
                    $$('#selected-escort-package-name').set('html', response.active_package.name);

                    $$('#selected-escort-package-expire-date').set('html', '');
                    $$('#selected-escort-package-expire-date').set('html', response.active_package.expiration_date);

                    $$('#city-select').set('html', '');
                    for (const city of response.cities) {

                        new Element('option', {
                            id: city.id,
                            value: city.id,
                            disabled: city.disabled,
                            html: city.title + city.is_active,
                        }).inject($$('#city-select')[0], 'inside');

                    }
                    Cubix.ProfileBumps.limitBumpsAmount();
                }
                else{
                    alert('unexpected error');
                    document.location.href="/";
                }

                MCubix.OverlayV2.init(true);
            }
        }).send();
    });
};

Cubix.ProfileBumps.initBumpCheckout = function () {
	$$('.btn-bump-checkout').removeEvents();
    $$('.btn-bump-checkout').addEvent('click', function () {
        
        if ($$("#styled-checkbox-2:checked").length == 0){
                return alert(headerVars.confirm_18_warning);
        }
        if ($$("#styled-checkbox-3:checked").length == 0){
                return alert(headerVars.confirm_terms_warning);
        }

        if (!$$('input[name=payment_gateway]:checked')) {
            return false;
        }

        var selectedMethod = $$('input[name=payment_gateway]:checked')[0].get('value');

        MCubix.OverlayV2.init(true);
        if (selectedMethod == 'twispay' || selectedMethod == 'coinsome') {
            $$('form')[0].set('send', {
                method: 'post',
                onSuccess: function (response) {
                    response = JSON.decode(response);
                    if (response.status === 'error') {
                        $$('#bump-checkout-errors').set('html', '');
                        for (var error in response.errors) {
                            $$('#bump-checkout-errors').set('html', error);
                        }
                    } else {
                        if (response.form) {
                            var formHtml = response.form;
                            $$('body')[0].set('html', $$('body')[0].get('html') + formHtml.replace(/ \"/g, "\""));
                            $('#payment-form').submit();
                        } else {
                            console.error(response);
						}
					}
				}
			});
			$$('form')[0].send();
		} else {
			$$('form')[0].submit();
		}
	});
};

Cubix.ProfileBumps.initPaymentOptionSelector = function() {
	$$('input[name=payment_gateway]').addEvent('change', function () {
		if(this.get('value') == 'coinsome'){
			Cubix.ProfileBumps.cryptoPrice = true;
			$('.btc-notify-text').show();
		}
		else{
			Cubix.ProfileBumps.cryptoPrice = false;
			$('.btc-notify-text').hide();
		}
		Cubix.ProfileBumps.initBumpAmountUpdate();
	});
};
