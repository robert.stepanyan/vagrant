Cubix.PhishingWarning = {};

Cubix.PhishingWarning.inProcess = false;

Cubix.PhishingWarning.url = '';

Cubix.PhishingWarning.Show = function () {
	if ( Cubix.PhishingWarning.inProcess ) return false;




	$$('#PhishingWarningWrapper').addClass('open');

	Cubix.PhishingWarning.inProcess = true;

	$$('#PhishingWarningButton').addEvent('click', function() {
		$$('#PhishingWarningWrapper').destroy();

	});

	return false;
}
