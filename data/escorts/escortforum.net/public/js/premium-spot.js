window.addEvent('domready',function() {
	var finish = false, page = 1, req = new Request({
		method: 'get',
		url: Cubix.Escorts.GetRequestUrl(''),
		data: { page: 1 },
		onSuccess: function (resp) {
			ss.options.min = window.getScrollSize().y - window.getSize().y - 150;
			var target = $$('.escorts')[0];
			target.getElements('.spinner').destroy();

			var rows = new Element('div', { html: resp }).getElements('.row');
			if ( rows.length == 0 ) { finish = true; }
			rows.each(function (row) {
				row.inject(target);
			});
			$$('.paging-list').setStyle('display', 'none');
		}
	});

	window.addEvent('escortsFilterChange', function () { page = 1; finish = false });

	var ss = new ScrollSpy({
		min: window.getScrollSize().y - window.getSize().y - 150,
		onScroll: function (p) {
			if ( req.running || finish ) return;
			if ( p.y > window.getScrollSize().y - window.getSize().y - 150 ) {
				if ( $$('.escorts')[0].getElements('.row').length > 0 ) {
					var hash = document.location.hash.substring(1);
					if(!hash.length){
						req.options.url = Cubix.Escorts.GetRequestUrl("");
						req.options.data.page = ++page;
						req.options.data.is_scroll = 1;

						$$('.escorts')[0].getElements('.row.last').removeClass('last');
						getSpinner().inject($$('.escorts')[0]);
						req.send();
					} 
				}
			}
		},
		container: window
	});

	var getSpinner = function () {
		var el = new Element('div', { 'class': 'spinner' });
		new Element('span', { html: 'Loading more escorts... '}).inject(el);
		return el;
	};
});
