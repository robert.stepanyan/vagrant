var Cubix = {};

Cubix.Lang = {};

Cubix.ShowTerms = function (link) {
	window.open(link, 'showTerms', 'menubar=no, resizable=yes, status=no, scrollbars=yes, toolbar=no, width=640, height=480');
	return false;
};

Cubix.ShowCookiePolicy = function (link) {
	window.open(link, 'showCookiePolicy', 'menubar=no, resizable=yes, status=no, scrollbars=yes, toolbar=no, width=640, height=880');
	return false;
};


var _log = function (data) {
	if ( console && console.log != undefined ) {
		console.log(data);
	}
}

var _st = function (object) {
	var parts = object.split('.');
	if ( parts.length < 2 ) {
		return _log('invalid parameter, must contain filename with extension');
	}

	var ext = parts[parts.length - 1];
	var protocol = (location.protocol === 'https:') ? 'https' : 'http';
	var base_url = protocol + '://st.escortforumit.xxx',
	
		prefix = '';

	switch ( ext ) {
		case 'jpg':
		case 'gif':
		case 'png':
			prefix = '/img/';
			break;
		case 'css':
			prefix = '/css/';
			break;
		case 'js':
			prefix = '/js/';
			break;
		default:
			return _log('invalid extension "' + ext + '"');
	}

	url = base_url + prefix + object;

	return url;
}

// Taken from viewed-escorts.js
/* --> Viewed Escorts */
Cubix.Viewed = {};
Cubix.VideoList = {};

Cubix.Viewed.url = ''; // Must be set from php
Cubix.Viewed.container = '';
Cubix.VideoList.url = '';
Cubix.VideoList.container = '';
//Pagination last _viewed escorts
Cubix.paginate = function () {
	if($('go_left') && $('go_right')){
		$('go_left').addEvent('click',function () {

			var current_page = $$('.show_viewed_escort')[0].getAttribute('page');

			$$('.rapper').removeClass('show_viewed_escort');
			var page = parseInt(current_page) - 1;
			if(page < 1){
				page = $$('.rapper').length;
			}

			$$('.page_'+page).addClass('show_viewed_escort');
		});

		$('go_right').addEvent('click',function () {

			var current_page = $$('.show_viewed_escort')[0].getAttribute('page');
			$$('.rapper').removeClass('show_viewed_escort');
			var pageLength = $$('.rapper').length;
			var page = parseInt(current_page) + 1;
			if(page > pageLength){
				page = 1;
			}
			$$('.page_'+page).addClass('show_viewed_escort');

		})
    }
};

Cubix.initMoreLessButton = function() {
	if ( ! $$('a.more-less-button').length ) return false;

	$$('a.more-less-button').addEvent('click', function(e){
		e.stop();

		var more_cities_box = $$('div.more-cities-box')[0];

		var curr_title = this.get('html');

		if ( this.hasClass('more') ) {
			more_cities_box.removeClass('none');
			this.removeClass('more').addClass('less');
		} else if ( this.hasClass('less') ) {
			more_cities_box.addClass('none');
			this.removeClass('less').addClass('more');
		}

		this.set('html', this.get('rel')).set('rel', curr_title);
	});
};

window.addEvent('domready', function() {

	Cubix.initMoreLessButton();
	
	$$('div.flags a').addEvent('click', function(e){
		var url = this.get('rel');
		window.location = url;
	});

    if ( $(Cubix.VideoList.container) && Cubix.VideoList.url ) {
        var city_id = $(Cubix.VideoList.container).getAttribute('city-id');
        new Request({
            url: Cubix.VideoList.url + '?city_id=' + city_id,
            method: 'get',
            onSuccess: function (resp) {
                if (resp) {
                    $(Cubix.VideoList.container).setStyle('opacity', 0);
                    var myFx = new Fx.Tween($(Cubix.VideoList.container), {
                        duration: 400,
                        onComplete: function () {
                            $(Cubix.VideoList.container).set('html', resp);
                            $(Cubix.VideoList.container).tween('opacity', 0, 1);
                            $(Cubix.VideoList.container).addClass('border');
                            Cubix.paginate();
                        }
                    });
                    myFx.start('height', '180');
                }
            }
        }).send();
    }
	if ( ! $(Cubix.Viewed.container) || ! Cubix.Viewed.url ) return;
	
	new Request({
		url: Cubix.Viewed.url,
		method: 'get',
		onSuccess: function (resp) {
			if(resp.contains('rapper'))
			{
				$(Cubix.Viewed.container).setStyle('opacity', 0);
				var myFx = new Fx.Tween($(Cubix.Viewed.container), {
					duration: 400,
					onComplete: function() {
						$(Cubix.Viewed.container).set('html', resp);
						$(Cubix.Viewed.container).tween('opacity', 0, 1);
						$(Cubix.Viewed.container).addClass('border');
                        Cubix.paginate();
					}
				});
				myFx.start('height', '320');
			}
		}
	}).send();
});

var resizeProfile = function() {
	if ( $$('.cTab')[0] ) {
	
		var leftSide = $('left');
		var profile = $('profile-container');
		var comments = $$('.cTab')[0];

		if ( ! profile.get('rel') ) {
			profile.set('rel', profile.getSize().y);
		}

		var commentsHeight = comments.setStyles({
			visibility: 'hidden',
			display: 'block'
		}).getHeight();

		comments.setStyles({
			visibility: null,
			display: null
		})	

		var profileHeight = 0;

		profileHeight = (profile.get('rel')*1) + (commentsHeight*1) - 4700;


		if ( leftSide.getSize().y > profileHeight ) {
			profile.setStyle('height', left.getSize().y);
		} else {
			profile.setStyle('height', profileHeight);
		}
	} else {
		var $leftHeight = $("left").getCoordinates().height;
		var $profileHeight = $("profile-container").getCoordinates().height - 4600;

		if ( $leftHeight > $profileHeight ) {
			$("profile-container").setStyle('height', $leftHeight);
		} else {
			$("profile-container").setStyle('height', $profileHeight);
		}
	}
};

var initGallery = function() {
	Slimbox.scanPage();
	
	/*$$("#gallery img, #gallery .lupa").removeEvents('click');
	$$("#gallery img, #gallery .lupa").addEvent("click", function() {
		
		if ( this.hasClass('noclick') ) return;
		
		if ( $('image') ) {
			$('image').destroy();
		}
		
		if ( this.hasClass('lupa') ) {
			var src = this.getPrevious('img').get('src');
		} else {
			var src = this.get('src');
		}
		
		if ( src.indexOf('_pp') != -1 )
			src = src.replace("_pp", "_orig");
		else {
			src = src.replace("_pl", "_orig");
		}
		
		im = new Image();
		im.src = src;
		im.onload = function () {
			
			h = im.height;
			w = im.width;

			oh = $(window).getSize().y;
			ow = $(window).getSize().x;

			if ( oh < h ) {
				origH = h;
				h = oh - 40;
				w = h * w / origH;
			}

			if ( ow < w ) {
				origW = w;
				w = ow - 40;
				h = h * w / origW;
			}

			leftt = (w + 20) / 2;
			topr = (h + 20) / 2;

			var divImage = new Element('div', {
				id: 'image',
				styles: {
					'margin-left': -leftt,
					'margin-top': -topr,
					'display': 'block',
					'opacity': '0'
				}
			}).inject($(document.body));

			new Element('a', {
				'html': 'x'
			}).inject(divImage);

			new Element('img', {
				'src': src,
				'width': w,
				'height': h
			}).addEvent('contextmenu', function(e) {
				e.stop();
			}).inject(divImage);

			divImage.addEvent("click", function() {
				$(this).destroy();
			});
			
			divImage.set('tween', { duration: 'short' }).fade('in');
		}.bind(this);
	});*/
};
