/*!
	Slimbox v1.8 - The ultimate lightweight Lightbox clone
	(c) 2007-2011 Christophe Beyls <http://www.digitalia.be>
	MIT-style license.
*/

var Slimbox = (function() {

	// Global variables, accessible to Slimbox only
	var win = window, ie6 = Browser.ie6, options, images, activeImage = -1, activeURL, prevImage, nextImage, compatibleOverlay, middle, centerWidth, centerHeight,

	// Preload images
	preload = {}, preloadPrev = new Image(), preloadNext = new Image(),

	// DOM elements
	overlay, center, image, sizer, prevLink, nextLink, bottomContainer, bottom, caption, number, voteButton, 

	// Effects
	fxOverlay, fxResize, fxImage, fxBottom;

	/*
		Initialization
	*/

	win.addEvent("domready", function() {
		// Append the Slimbox HTML code at the bottom of the document
		$(document.body).adopt(
			$$(
				overlay = new Element("div#lbOverlay", {events: {click: close}}),				
				center = new Element("div#lbCenter"),
				bottomContainer = new Element("div#lbBottomContainer")
			).setStyle("display", "none")
		);

		closeBtn = new Element("a#lbCloseLink[href=#]", {events: {click: close}}).inject(center);

		/*if(window.valentine != undefined && window.valentine && window.user_type == 'member' && window.user_type != undefined){

			var votes_left_count_el = document.getElementsByClassName('valentine_vote_left')
			var votes_left_count = 3;
			if(votes_left_count_el.length != undefined && votes_left_count_el.length != 0){
				votes_left_count = votes_left_count_el[0].innerHTML.trim();
			}
			voteImageSpan = new Element("span", {
				'class': 'zoomed_vote_image_span',
				events: {
					click: vote
				}
			}).inject(center);

			valentineVoteSpan = new Element("span", {
				'class': 'zoomed_valentine_vote_text',
				'text': 'Vote'
			}).inject(voteImageSpan);

			valentineVotedSpan = new Element("span", {
				'class': 'zoomed_valentine_voted_text',
				'text': 'Voted'
			}).inject(voteImageSpan);

			valentineVoteLeftTooltip = new Element("span", {
				'class': 'zoomed_tooltiptext',
				'text': 'Votes left '
			}).inject(voteImageSpan);

			valentineVoteLeftTooltipText = new Element("span", {
				'class': 'zoomed_valentine_vote_left',
				'text':  votes_left_count
			}).inject(valentineVoteLeftTooltip);

		}*/

		image = new Element("div#lbImage").inject(center).adopt(
			sizer = new Element("div", {styles: {position: "relative"}}).adopt(
				prevLink = new Element("a#lbPrevLink[href=#]", {events: {click: previous}}),
				nextLink = new Element("a#lbNextLink[href=#]", {events: {click: next}})
			)
		).addEvent('contextmenu', function(e) {
			e.stop();
		});

		bottom = new Element("div#lbBottom").inject(bottomContainer).adopt(
			caption = new Element("div#lbCaption"),
			number = new Element("div#lbNumber"),
			new Element("div", {styles: {clear: "both"}})
		);
	});


	/*
		Internal functions
	*/

	function position() {
		var scroll = win.getScroll(), size = win.getSize();
		$$(center, bottomContainer).setStyle("left", scroll.x + (size.x / 2));
		if (compatibleOverlay) overlay.setStyles({left: scroll.x, top: scroll.y, width: size.x, height: size.y});
	}

	function setup(open) {
		["object", ie6 ? "select" : "embed"].forEach(function(tag) {
			Array.forEach(document.getElementsByTagName(tag), function(el) {
				if (open) el._slimbox = el.style.visibility;
				el.style.visibility = open ? "hidden" : el._slimbox;
			});
		});

		overlay.style.display = open ? "" : "none";

		var fn = open ? "addEvent" : "removeEvent";
		win[fn]("scroll", position)[fn]("resize", position);
		document[fn]("keydown", keyDown);
	}

	function keyDown(event) {
		var code = event.code;
		// Prevent default keyboard action (like navigating inside the page)
		return options.closeKeys.contains(code) ? close()
			: options.nextKeys.contains(code) ? next()
			: options.previousKeys.contains(code) ? previous()
			: false;
	}

	function previous() {
		return changeImage(prevImage);
	}

	function next() {
		return changeImage(nextImage);
	}

	function changeImage(imageIndex) {
		if ( (imageIndex + 1) > 6 /* && typeof window.valentine == undefined*/) {
			$$('.more-photos-list')[0].removeClass('none');
			$$('.load-more').destroy();
			
			resizeProfile();
		}
		
		if (imageIndex >= 0) {
			activeImage = imageIndex;
			activeURL = images[imageIndex][0];
			prevImage = (activeImage || (options.loop ? images.length : 0)) - 1;
			nextImage = ((activeImage + 1) % images.length) || (options.loop ? 0 : -1);

			stop();
			center.className = "lbLoading";

			preload = new Image();
			preload.onload = animateBox;
			preload.src = activeURL;
		}
		/*if(window.valentine != undefined && window.valentine && window.user_type == 'member' && window.user_type != undefined){
			initVoteButton();
		}*/

		return false;
	}

	function animateBox() {
		center.className = "";
		fxImage.set(0);
		image.setStyles({backgroundImage: "url(" + activeURL + ")", display: ""});
		sizer.setStyle("width", preload.width);
		$$(sizer, prevLink, nextLink).setStyle("height", preload.height);

		caption.set("html", images[activeImage][1] || "");
		number.set("html", (((images.length > 1) && options.counterText) || "").replace(/{x}/, activeImage + 1).replace(/{y}/, images.length));

		if (prevImage >= 0) preloadPrev.src = images[prevImage][0];
		if (nextImage >= 0) preloadNext.src = images[nextImage][0];

		centerWidth = image.offsetWidth;
		centerHeight = image.offsetHeight;
		var top = Math.max(0, middle - (centerHeight / 2)), check = 0, fn;
		if (center.offsetHeight != centerHeight) {
			check = fxResize.start({height: centerHeight, top: top});
		}
		if (center.offsetWidth != centerWidth) {
			check = fxResize.start({width: centerWidth, marginLeft: -centerWidth/2});
		}
		fn = function() {
			bottomContainer.setStyles({width: centerWidth, top: top + centerHeight, marginLeft: -centerWidth/2, visibility: "hidden", display: ""});
			fxImage.start(1);
		};
		if (check) {
			fxResize.chain(fn);
		}
		else {
			fn();
		}
	}

	function animateCaption() {
		if (prevImage >= 0) prevLink.style.display = "";
		if (nextImage >= 0) nextLink.style.display = "";
		fxBottom.set(-bottom.offsetHeight).start(0);
		bottomContainer.style.visibility = "";
	}

	function stop() {
		preload.onload = null;
		preload.src = preloadPrev.src = preloadNext.src = activeURL;
		fxResize.cancel();
		fxImage.cancel();
		fxBottom.cancel();
		$$(prevLink, nextLink, image, bottomContainer).setStyle("display", "none");
	}

	function close() {
		if (activeImage >= 0) {
			stop();
			activeImage = prevImage = nextImage = -1;
			center.style.display = "none";
			fxOverlay.cancel().chain(setup).start(0);
		}

		/*if(window.valentine != undefined && window.valentine && window.user_type == 'member' && window.user_type != undefined){
			var valVoteBtn = $$('.zoomed_vote_image_span');
			valVoteBtn.removeClass('zoom_vote');
			valVoteBtn.removeClass('zoom_voted');
		}*/
		return false;
	}

	/*function initVoteButton() {
		setTimeout(function(){
			var valvotel = $$('a[href='+activeURL+']');	
			var val_voted = valvotel.get('data-voted')[0];
			var valVoteBtn = $$('.zoomed_vote_image_span');
			var valVoteText = $$('.zoomed_valentine_vote_text');
			var valVotedText = $$('.zoomed_valentine_voted_text');
			if(val_voted == 1){
				valVoteBtn.addClass('zoom_voted');
				valVoteBtn.removeClass('zoom_vote');
				valVotedText.addClass('zoom_vote_text_show');
				valVotedText.removeClass('zoom_vote_text_hide');
				valVoteText.addClass('zoom_vote_text_hide');
				valVoteText.removeClass('zoom_vote_text_show');

			} else {
				valVoteBtn.addClass('zoom_vote');
				valVoteBtn.removeClass('zoom_voted');
				valVotedText.removeClass('zoom_vote_text_show');
				valVotedText.addClass('zoom_vote_text_hide');
				valVoteText.removeClass('zoom_vote_text_hide');
				valVoteText.addClass('zoom_vote_text_show');
			}
		}, 1000)
		
	}

	function vote(){
		var valvotel = $$('a[href='+activeURL+']');
		var pp_src = activeURL.replace('pp', 'orig')
		var val_esc_id = valvotel.get('data-escort-id')[0];
		var val_usr_id = valvotel.get('data-user-id')[0];
		var val_photo_id = valvotel.get('data-photo-id')[0];
		var valVoteBtn = $$('.zoomed_vote_image_span');
		var valVoteText = $$('.zoomed_valentine_vote_text');
		var valVotedText = $$('.zoomed_valentine_voted_text');
		var voteBtn = $$('span[data-img-src="'+pp_src+'"]');
		var voteBtnVoteText = $$('span[data-img-src="'+pp_src+'"] .valentine_vote_text');
		var voteBtnVotedText = $$('span[data-img-src="'+pp_src+'"] .valentine_voted_text');
		if(val_usr_id  != undefined && val_usr_id != ''){
	    	if(!this.getAttribute('class').includes('valentine_voted')){
			    new Request({
					url: 'valentine?vote=1',
					method: 'post',
					data: {
						'escort_id': val_esc_id,
						'user_id': val_usr_id,
						'photo_id': val_photo_id
					},
					onSuccess: function (resp) {
						if(!resp.includes('limit')){
							valVoteBtn.addClass('zoom_voted');
							valVoteBtn.removeClass('zoom_vote');
							valVotedText.addClass('zoom_vote_text_show');
							valVotedText.removeClass('zoom_vote_text_hide');
							valVoteText.addClass('zoom_vote_text_hide');
							valVoteText.removeClass('zoom_vote_text_show');
							valvotel.set('data-voted', 1);
							voteBtn.addClass('vote_image_voted');
							voteBtnVoteText.addClass('valentine_hide');
							voteBtnVoteText.removeClass('valentine_show');
							voteBtnVotedText.removeClass('valentine_hide');
							voteBtnVotedText.addClass('valentine_show');
							var zoomed_votes_left = document.getElementsByClassName("zoomed_valentine_vote_left");
							var votes_left = document.getElementsByClassName("valentine_vote_left");
							for(var j = 0; j < zoomed_votes_left.length; j++){
								zoomed_votes_left[j].innerHTML = 3 - resp;
							}
							for(var k = 0; k < votes_left.length; k++){
								votes_left[k].innerHTML = 3 - resp;
							}
						}
					}
				}).send();
			}
	    } else {
	    	close();
	    	document.getElementsByClassName('lb-login-btn')[0].click();
	    }
	}*/
	/*
		API
	*/

	Element.implement({
		slimbox: function(_options, linkMapper) {
			// The processing of a single element is similar to the processing of a collection with a single element
			$$(this).slimbox(_options, linkMapper);

			return this;
		}
	});

	Elements.implement({
		/*
			options:	Optional options object, see Slimbox.open()
			linkMapper:	Optional function taking a link DOM element and an index as arguments and returning an array containing 2 elements:
					the image URL and the image caption (may contain HTML)
			linksFilter:	Optional function taking a link DOM element and an index as arguments and returning true if the element is part of
					the image collection that will be shown on click, false if not. "this" refers to the element that was clicked.
					This function must always return true when the DOM element argument is "this".
		*/
		slimbox: function(_options, linkMapper, linksFilter) {
			linkMapper = linkMapper || function(el) {
				return [el.href, el.title];
			};

			linksFilter = linksFilter || function() {
				return true;
			};

			var links = this;

			links.removeEvents("click").addEvent("click", function() {
				// Build the list of images that will be displayed
				var filteredLinks = links.filter(linksFilter, this);
				return Slimbox.open(filteredLinks.map(linkMapper), filteredLinks.indexOf(this), _options);
			});

			return links;
		}
	});

	return {
		open: function(_images, startImage, _options) {
			options = Object.append({
				loop: false,				// Allows to navigate between first and last images
				overlayOpacity: 0.8,			// 1 is opaque, 0 is completely transparent (change the color in the CSS file)
				overlayFadeDuration: 400,		// Duration of the overlay fade-in and fade-out animations (in milliseconds)
				resizeDuration: 400,			// Duration of each of the box resize animations (in milliseconds)
				resizeTransition: false,		// false uses the mootools default transition
				initialWidth: 250,			// Initial width of the box (in pixels)
				initialHeight: 250,			// Initial height of the box (in pixels)
				imageFadeDuration: 400,			// Duration of the image fade-in animation (in milliseconds)
				captionAnimationDuration: 400,		// Duration of the caption animation (in milliseconds)
				counterText: "Image {x} of {y}",	// Translate or change as you wish, or set it to false to disable counter text for image groups
				closeKeys: [27, 88, 67],		// Array of keycodes to close Slimbox, default: Esc (27), 'x' (88), 'c' (67)
				previousKeys: [37, 80],			// Array of keycodes to navigate to the previous image, default: Left arrow (37), 'p' (80)
				nextKeys: [39, 78]			// Array of keycodes to navigate to the next image, default: Right arrow (39), 'n' (78)
			}, _options || {});

			// Setup effects
			fxOverlay = new Fx.Tween(overlay, {property: "opacity", duration: options.overlayFadeDuration});
			fxResize = new Fx.Morph(center, Object.append({duration: options.resizeDuration, link: "chain"}, options.resizeTransition ? {transition: options.resizeTransition} : {}));
			fxImage = new Fx.Tween(image, {property: "opacity", duration: options.imageFadeDuration, onComplete: animateCaption});
			fxBottom = new Fx.Tween(bottom, {property: "margin-top", duration: options.captionAnimationDuration});

			// The function is called for a single image, with URL and Title as first two arguments
			if (typeof _images == "string") {
				_images = [[_images, startImage]];
				startImage = 0;
			}

			middle = win.getScrollTop() + (win.getHeight() / 2);
			centerWidth = options.initialWidth;
			centerHeight = options.initialHeight;
			center.setStyles({top: Math.max(0, middle - (centerHeight / 2)), width: centerWidth, height: centerHeight, marginLeft: -centerWidth/2, display: ""});
			compatibleOverlay = ie6 || (overlay.currentStyle && (overlay.currentStyle.position != "fixed"));
			if (compatibleOverlay) overlay.style.position = "absolute";
			fxOverlay.set(0).start(options.overlayOpacity);
			position();
			setup(1);

			images = _images;
			options.loop = options.loop && (images.length > 1);
			return changeImage(startImage);
		}
	};

})();


// AUTOLOAD CODE BLOCK (MAY BE CHANGED OR REMOVED)
Slimbox.scanPage = function() {
	$$("a[rel^=lightbox]").slimbox({/* Put custom options here */}, null, function(el) {
		return (this == el) || ((this.rel.length > 8) && (this.rel == el.rel));
	});
};
if (!/android|iphone|ipod|series60|symbian|windows ce|blackberry/i.test(navigator.userAgent)) {
	window.addEvent("domready", Slimbox.scanPage);
}