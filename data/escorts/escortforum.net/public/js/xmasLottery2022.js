Cubix.XmasLottery = {};

Cubix.XmasLottery.inProcess = false;

Cubix.XmasLottery.url = '';

Cubix.XmasLottery.Show = function () {
	if (Cubix.XmasLottery.inProcess) return false;

	var page_overlay = new Cubix.Overlay(document.body, {has_loader: false, opacity: 0.5, color: '#000', z_index: 200});
	page_overlay.disable();

	$$('#XmasWinnerPopup').addClass('open');

	Cubix.XmasLottery.inProcess = true;

	$$('#xmas-understand').addEvent('click', function () {
		page_overlay.enable();
		new Request({
			url:  "/private-v2/set-xmas-popup-showed",
			method: 'post',
			onSuccess: function (resp) {
				$$('#XmasWinnerPopup').destroy();
				$$('.overlay').addClass('overlay-hidden');
			}
		}).send();
	});

	return false;
}