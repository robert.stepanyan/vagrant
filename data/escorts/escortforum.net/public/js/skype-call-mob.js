$(document).ready(function() {
	$('.expand-collapse').on('click', function(){
		$(this).toggleClass('expended');
		$(".expended-area" ).slideToggle( "fast" );
	});
	$('.mobile-skype-mini').on('click', function(){
		$(".expended-area" ).slideToggle( "fast" );
		$('.mobile-skype-call-block').toggleClass('expended');
	});
	

	$("#toggle").change(function() {
	    var ischecked= $(this).is(':checked');
	    submitform();
	}); 

	$('.skype-booking-btn').on('click', function(){
		submitform();
	});

	//this is so stupid omg
	$("#price-60").change(function() {
	    var should_be_selected = $(this).find(":selected").attr('class');
    	$("#price-30").find('.' + should_be_selected).prop('selected', true);
    	settks();

	}); 
	$("#price-30").change(function() {
	    var should_be_selected = $(this).find(":selected").attr('class');
    	$("#price-60").find('.' + should_be_selected).prop('selected', true);
    	settks();

	}); 
});

var settks = function(){
	$('#price30tks').empty();
   $('#price30tks').html( $("#price-30").find(":selected").val() * 10);
   $('#price60tks').empty();
   $('#price60tks').html($("#price-60").find(":selected").val() * 10);
}

var submitform = function(){
	var formData = $('#skype-form').serialize();

	$.ajax({
		method: "POST",
		url: '/skype/update',
		data: formData,
		beforeSend: function(){
			$('.skype-overlay').removeClass('none');
			$('.mobile-skype-container').css('pointer-events', 'none');
		},
		success: function( resp ){
			$('.mobile-skype-container').css('pointer-events', 'auto');
			$('#skype-status').empty();
			 var resp = jQuery.parseJSON( resp );
		     if(resp.status == 'error'){
		        $('input').removeClass('skype-input-error');

		        let inputName = Object.keys(resp.msgs)[0];
		        let inputValue = Object.values(resp.msgs)[0];
		   
		        $('#'+inputName).addClass('skype-input-error');
		        
		      }
		      else{
		        $('#skype-status').html('Settings successfully saved');
		        $('input').removeClass('skype-input-error');
		      }
			$('.skype-overlay').addClass('none')
		}

	});
}