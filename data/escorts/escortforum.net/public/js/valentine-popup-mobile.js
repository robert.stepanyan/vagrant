Cubix.Valentinepopup = {};

Cubix.Valentinepopup.inProcess = false;

Cubix.Valentinepopup.url = '';

Cubix.Valentinepopup.Show = function () {
    if ( Cubix.Valentinepopup.inProcess ) return false;

    // var page_overlay = new Cubix.Overlay(document.body, {has_loader: false, opacity: 0.5, color: '#000', z_index: 200});
    // page_overlay.disable();

    $('#valentinePopup').addClass('open');

    Cubix.Valentinepopup.inProcess = true;

    $('#close-button-mod-id').on('click', function() {
        $('#valentinePopup').remove();
        // page_overlay.enable();
    });

    return false;
};