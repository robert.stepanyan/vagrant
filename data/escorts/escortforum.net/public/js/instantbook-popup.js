function getActualReqs() {
  var reqs = Object.keys(localStorage).filter(function(el) {
    if (~el.indexOf('ireq::')) {
      var dtRule = Date.now() - 24 * 60 * 60 * 1000;
      var mTime = localStorage.getItem(el);

      if (mTime > dtRule) {
        return el;
      } else {
        localStorage.removeItem(el);
      }
    }
  });

  return reqs;
}


var StorageManager = (function() {
  function cleanup() {
    var dtRule = Date.now() - 24 * 60 * 60 * 1000;

    Object.keys(localStorage).forEach(function (item) {
      if (~item.indexOf('ireq::')) {
        var mTime = localStorage.getItem(item);
        (mTime < dtRule) && localStorage.removeItem(item);
      }
    });
  }

  function getReqs() {
    return Object.keys(localStorage).filter(function (item) {
      if (~item.indexOf('ireq::')) {
        return item;
      }
    }) || [];
  }

  function exists(escortId) {
    var existingReqs = Object.keys(localStorage).filter(function (item) {
      if (item === ('ireq::' + escortId)) {
        return item;
      }
    });
    
    if (existingReqs && existingReqs.length) return true;

    return false;
   }

  return {
    cleanup: cleanup,
    getReqs: getReqs,
    exists: exists
  };

})();

Cubix.instantbookPopup = {};

Cubix.instantbookPopup.inProcess = false;

Cubix.instantbookPopup.url = '/escorts/ajax-instant-book';

Cubix.instantbookPopup.Show = function (options) {
  StorageManager.cleanup();

  if (StorageManager.getReqs().length >= 7) {
    alert(instantBookLng.rulteLimit2);
    $('instant-book-btn').removeClass('is-loading');
    return false;
  }

  if (StorageManager.exists($('escort_id').get('value'))) {
    alert(instantBookLng.rulteLimit1);
    $('instant-book-btn').removeClass('is-loading');
    return false;
  }

  if (Cubix.instantbookPopup.inProcess) return false;

  var showInstantNotification = Cookie.read('instant_notif');

  if (!showInstantNotification) {

    var $header = new Element('h2', {
      class: 'Header',
      html: instantBookLng.infoPopupPublicHeader
    });

    var html = instantBookLng.infoPopupPublicBody;

    var $body = new Element('div', {
      class: 'Body',
      id: 'instant-book-popup'
    }).set('html', html);

    var $container = new InstantModal({
      header: $header,
      body: $body,
      id: 'InstantBook',
      close: true
    });


    $container.getElement('.instant-book-info-btn').addEvent('click', function () {
      document.cookie = 'instant_notif=1; expires=Fri, 31 Dec 2024 23:59:59 GMT';
      $container.getElement('.instant-popup-close-btn').click();
      $('instant-book-btn').click();
    });

    $container.show();

    return false;
  }

  if (getActualReqs() >= 7) {
    alert('You can send 7 bookings per day');

    return false;
  }

  var container = new Element('div', {
    id: 'instant-book-popup'
  }).inject(document.body);

  Cubix.instantbookPopup.inProcess = true;

  new Request({
    url: Cubix.instantbookPopup.url,
    method: 'get',
    data: options,
    onSuccess: function (resp) {
      var header = new Element('h2', {
        class: 'Header'
      }).set('text', instantBookLng.bookingPopupHeader);

      var body = new Element('div', {
        class: 'Body',
        id: 'instant-book-popup'
      }).set('html', resp);

      var $container = new InstantModal({
        header: header,
        body: body,
        id: 'InstantBook',
        close: true
      }).show();

      Cubix.instantbookPopup.inProcess = false;
      var $hourElem = $$('[name="hour_min"]')[0];
      var endDateFromEl = $hourElem.get('data-end');
      var y = endDateFromEl.split(' ')[0].split('-')[0];
      var m = endDateFromEl.split(' ')[0].split('-')[1];
      var d = endDateFromEl.split(' ')[0].split('-')[2];
      var h = endDateFromEl.split(' ')[1].split(':')[0];
      var m = endDateFromEl.split(' ')[1].split(':')[1];

      var endDateUTC = new Date(y,m,d,h,m);
      var timeZoneOffset = (new Date()).getTimezoneOffset() * 60 * 1000;
      var curDate = new Date(Date.now() + 15 * 60 * 1000);
      var startDate = curDate;
      var endDate = new Date(endDateUTC.getTime() - timeZoneOffset);
      var startHours = curDate.getHours();
      var endHours = endDate.getHours();

      if (endDate.getTime() > startDate.getTime()) {
        if (endHours < startHours) {
          endHours = endHours + 24;
        }
      } else {
        alert('Escort is not available for instant book anymore.');
        return false;
      }
      var timeSelector = [];

      for (var i = startHours; i <= endHours; i++) {
        var h = (i > 23) ? i - 24 : i;
        var d = (i > 23) ? new Date(startDate.getTime() + 24 * 60 * 60 * 1000) : startDate;
        var dataDateAttr = [d.getFullYear().toString(), ('0' + (d.getMonth() + 1)).slice(-2), ('0' + d.getDate()).slice(-2)].join('-');

        var minArr = [0, 15, 30, 45];
        minArr.each(function (m) {
          var __d = new Date(dataDateAttr.split('-')[0], dataDateAttr.split('-')[1], dataDateAttr.split('-')[2], h, m).getTime();
          if (__d > curDate && __d < endDate) {
            timeSelector.push([('0' + h).slice(-2) + ':' + ('0' + m).slice(-2), dataDateAttr]);
          }
        });
      }

      console.log(timeSelector);

      timeSelector.each(function (el) {
        $hourElem.adopt(new Element('option', {
          value: el[0],
          html: el[0],
          'data-date': el[1]
        }));
      });

      $hourElem.addEvent('change', function () {
        this.blur();
      });

      $$('[name="wait_time"]').addEvent('change', function () {
        this.blur();
      });

      $$('.left-arr').addEvent('click', function () {
        var duration = parseInt($('inst-duration').get('value')) - 1;
        if (duration < 1) return false;
        $('inst-duration').set('value', duration);
        $('inst-duration-text').set('text', duration);
      });

      $$('.right-arr').addEvent('click', function () {
        var duration = parseInt($('inst-duration').get('value')) + 1;
        $('inst-duration').set('value', duration);
        $('inst-duration-text').set('text', duration);
      });

      $$('.location-ref').addEvent('click', function () {
        var location = $('inst-location').get('value');

        if (location == 'incall') {
          $('inst-location').set('value', 'outcall');
          $$('.location-string').set('text', 'Outcall');
          $('inst-outcall-address').show();
        } else {
          $('inst-location').set('value', 'incall');
          $$('.location-string').set('text', 'Incall');
          $('inst-outcall-address').hide();
        }
      });

      $('instant-book-form').addEvent('submit', function (e) {
        e.preventDefault();

        $$('.instant-book-popup-btn')[0].addClass('is-loading');
        $errors = $("InstantBook").getElement('.errors');
        if ($errors) {
          $errors.set('html', '');
        }

        var bookingDateYMD = $$('[name="hour_min"]')[0].getSelected()[0].get('data-date');
        var time = $$('[name="hour_min"]')[0].get("value");
        var bookingDateObj = new Date(bookingDateYMD + 'T' + ('0' + time.split(':')[0]).slice(-2) + ':' + ('0' + time.split(':')[1]).slice(-2) + ':' + '00');
        var bookingDate = bookingDateObj.toISOString();
        window.instantSocket.emit("request_booking", {
          hour: time.split(':')[0],
          min: time.split(':')[1],
          booking_date: bookingDate,
          duration: +$$('[name="duration"]')[0].get("value"),
          location: $$('[name="loacation"]')[0].get("value"),
          outcall_address: $$('[name="outcall"]').length ? $$('[name="outcall"]')[0].get("value") : '',
          special_request: $$('[name="special_request"]')[0].get("value"),
          phone: $$('[name="phone"]')[0].get("value"),
          contact_type: $$('input[name=contact_type]:checked').map(function (e) {
            return e.value;
          }),
          wait_time: +$$('[name="wait_time"]')[0].get("value"),
          escort_id: +$('escort_id').get('value')
        });

        localStorage.setItem(('ireq::' + $('escort_id').get('value')), Date.now());
      });
    }
  }).send();
};