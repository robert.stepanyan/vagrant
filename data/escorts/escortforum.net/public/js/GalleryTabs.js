
(function() {

// make typeOf usable for MooTools 1.2 through 1.5
var typeOf = this.typeOf || this.$type;

var GalleryTabs = this.GalleryTabs = new Class({

	Implements: [Events, Options],

	options: {
		tabSelector: '.tab',
		contentWrapper: '.content-wrapper',
		contentSelector: '.content',
		activeClass: 'active',
		renameIcon: '.rename-gallery'
	},
	
	cropper : null,
	container : null,
    contentSelector : null,
	overlay : null,
	globalOverlay : null,
	tempUrl: null,
	targetTab : null,
	_mooUpload :{
		texts:{
			it:{
				selectfile : 'Aggiungi foto',
				startupload: 'Carica foto',
				clearList     : 'Pulisci'
			},
			en:{
				selectfile : 'Add File',
				startupload: 'Start Upload',
				clearList     : 'Clear List'
			}
		}
	},
	
	initialize: function(container, options, showNow) {
		this.setOptions(options);
		
		this.container = document.id(container);
		this.contentSelector = document.getElement(this.options.contentSelector);
		this.overlay = new Cubix.Overlay(document.getElement(this.options.contentWrapper), {color: '#fdfdfd', opacity: .6, loader: '/img/photo-video/ajax_test.gif', position: '230px'});
		this.globalOverlay = new Cubix.Overlay(document.getElement('body'), {color: '#fdfdfd', opacity: .8, has_loader: false});
		
		
		this.initRenameGallery();
		this.initButtons(true);
		this.initSelectAll();
		this.initGalleryStatusToggle();
				
		this.container.addEvent('click:relay(' + this.options.tabSelector + ')', function(event, tab) {
			event.stop();
			this.show(tab);
		}.bind(this));
		// determine what tab to show right now (default to the 'leftmost' one)
		if (typeOf(showNow) == 'function') {
			showNow = showNow();
		} else {
			showNow = showNow || 0;
		}
		
		this.show(showNow);
	},

	get: function(index) {
		if (typeOf(index) == 'element') {
			// call get with the index of the supplied element (NB: will break if indexOf returns -1)
			return this.get(this.indexOf(index));
		} else {
			var tab = this.container.getElements(this.options.tabSelector)[index];
			return tab
		}
	},

	indexOf: function(element) {
		if (element.match(this.options.tabSelector)) {
			return this.container.getElements(this.options.tabSelector).indexOf(element);
		} else {
			// element is not tab return -1 per convention
			return -1;
		}
	},
	
	animation: function(tab){
		curActive = this.container.getElement('.tab.active');
		this.container.getElements(this.options.tabSelector).removeClass(this.options.activeClass);
		tab.addClass(this.options.activeClass);
		tab.removeClass('gradient');
		var fxActive = new Fx.Morph(tab, { transition: Fx.Transitions.Quint.easeInOut, duration: 1000, link: 'cancel',
		onComplete: function(){
			tab.addClass('gradient');
		}});
		fxActive.start({'background-color' : '#40b3e3',
						'margin-top' : 0,
						'height' : '30px',
						'line-height' : '40px'});
		
		if(curActive){
			curActive.removeClass('gradient');
			var fxPassive = new Fx.Morph(curActive, { transition: Fx.Transitions.Quint.easeInOut, duration: 1000, link: 'cancel',
			onComplete: function(){
				curActive.addClass('gradient');
			}});
			fxPassive.start({'background-color' : '#f0f0f0',
						'margin-top' : '10px',
						'line-height' : '20px',
						'height' : '20px' });
		}		  
	},
		
	show: function(what) {
		
		var tab = this.get(what);
		var url = tab.getElement('a').get('href');
		
		if (tab) {
			this.overlay.disable();
			this.animation(tab);
					
			new Request({
				url: url,
				method: 'get',
				onSuccess: function (resp) {
					this.contentSelector.set('html',resp);
					this.overlay.enable();
					this.initSortables();
					this.initCropper();
					this.initUpload($('form-upload'));
					
				}.bind(this)
			}).send();
			
			//this.fireEvent('change', what);
		}
		// no else, not clear what to do
	},
	
	initRenameGallery: function(){
		var renamePopup = $$('.rename-gallery-popup')[0];
		
		// rename gallery popup		
		this.container.getElements(this.options.renameIcon).addEvent('click', function(event) {
			event.stop();
			this.targetTab = event.target;
			this.tempUrl = this.targetTab.get('href');
			var galleryName = event.target.getPrevious('a').get('text');
			$('gallery-name-f').set('value',galleryName);
			this.globalOverlay.disable();
			renamePopup.removeClass('none');
			new Fx.Tween(renamePopup, {
					property: 'opacity',
					duration: 600
				}).start(0,1);
			
		}.bind(this));
		
		//rename popup Cancel
		renamePopup.getElement('.cancel').addEvent('click', function(){
			new Fx.Tween(renamePopup, {
					property: 'opacity',
					duration: 600,
					onComplete: function() {
						renamePopup.addClass('none');
						this.globalOverlay.enable();
					}.bind(this)
				}).start(1,0);
				
				
		}.bind(this));
		
		//rename popup OK 
		renamePopup.getElement('.ok').addEvent('click', function(){
			
			var galleryName = $('gallery-name-f').get('value');
						
			new Request({
				url: this.tempUrl,
				method: 'get',
				data: {'gallery_name' : galleryName},
				onSuccess: function () {
					
				}
			}).send();
			this.targetTab.getPrevious('a').set('text',galleryName);
			renamePopup.getElement('.cancel').fireEvent('click');
			
		}.bind(this));
	},
	
	getChecked: function(){
		var ids = '';
		
		$$('.photo-list .regular-checkbox:checked').each(function(el) {
			ids += '&photo_id[]=' + el.get('value');
		});
		return ids;
	},
	
	initSelectAll: function(){
		$('checkbox-select-all').addEvent('click', function() {
			var doCheck = this.checked;
			$$('.photo-list .regular-checkbox').each(function(el) { el.checked = doCheck; });
		});
	},
	
	initGalleryStatusToggle: function(){
		// add a relayed click event to handle toggle
		this.contentSelector.addEvent('click:relay(.regular-radio)', function(event,el){
			this.overlay.disable();
			var galleryId = $('gallery-id-field').get('value');
			var status = el.get('value');
						
			new Request({
				url: this.options.galleryStatusLink + '&status=' + status + '&gallery=' + galleryId,
				method: 'get',
				onSuccess: function (resp) {
					this.overlay.enable();
				}.bind(this)
			}).send();
		}.bind(this));
	},
	
	initButtons: function(galleryView){
		var self = this;
		
		if(galleryView){
			$$('#set-main-btn, #set-rotate-btn, #set-mobile-cover-btn, #set-valentines-btn').addEvent('click',function(event){
				event.stop();
				var ids = self.getChecked();

				if(ids){
					self.overlay.disable();
					var galleryId = $('gallery-id-field').get('value');
					ids += '&gallery_id=' + galleryId;
					var url = this.get('href') + ids;
					new Request.JSON({
						url: url,
						method: 'get',
						onSuccess: function (resp) {
							if(resp.success){
								$$('#gallery-block .content')[0].set('html',resp.html);
								self.initSortables();
								self.initCropper();
							}
							else{
								alert(resp.error);
							}
							self.overlay.enable();
						}
					}).send();
				}
				else{
					alert('Please select at least one photo');
				}

			});
			
			/*$('retouch-btn').addEvent('click',function(event){
				event.stop();
				var ids = self.getChecked();
				if(ids){
					self.overlay.disable();
					setTimeout(function(){self.overlay.enable();}, 1000);
				}
				else{
					alert('Please select at least one photo');
				}
			});*/

			if ($('move-archive-btn'))
				$('move-archive-btn').addEvent('click',function(event){
					event.stop();
					var ids = self.getChecked();
					if(ids){
						var galleryId = $('gallery-id-field').get('value');
						ids += '&gallery_id=' + galleryId;
						var url = this.get('href') + ids;
						self.overlay.disable();
						new Request.JSON({
							url: url,
							method: 'get',
							onSuccess: function (resp) {
								if(resp.success){
									$$('.content .regular-checkbox:checked').each(function(el) {
										var li = el.getParent('li');
										li.set('tween', {
											onComplete: function(){
												li.destroy();
											}
										});
										li.fade('out');
									});
								}
								else{
									alert(resp.error);
								}
								self.overlay.enable();
							}
						}).send();
					}
					else{
						alert('Please select at least one photo');
					}
				});
		
			$$('.archive-button').addEvent('click',function(event){
				event.stop();
				MediaTabs.overlay.disable();
				var url = this.get('href')
				new Request({
					url: url,
					method: 'get',
					onSuccess: function (resp) {
						$('gallery-block').set('html',resp);
						self.initButtons(false);
						self.initSelectAll();
						MediaTabs.overlay.enable();
					}
				}).send();
			});
		}
		else{
			$$('.to-galleries-button').addEvent('click',function(event){
				event.stop();
				var galleryTab = $$('.media-tabs a')[0];
				galleryTab.getParent('li').removeClass('active');
				galleryTab.fireEvent('click');
			})
		}
		
		$('remove-btn').addEvent('click',function(event){
			event.stop();
			var ids = self.getChecked();
			if(ids){
				
				var url = this.get('href') + ids;
				new Request.JSON({
					url: url,
					method: 'get',
					onSuccess: function (resp) {
						if(resp.success){
							$$('.photo-list .regular-checkbox:checked').each(function(el) {
								var li = el.getParent('li');
								li.set('tween', {
									onComplete: function(){
										li.destroy();
									}
								});
								li.fade('out');
							});
						}
						else{
							alert(resp.error);
						}
					}
				}).send();
			}
			else{
				alert('Please select at least one photo');
			}
		})
		
		var movePopup = $$('.move-gallery-popup')[0];
		$('move-galery-btn').addEvent('click', function(event){
			event.stop();
			self.globalOverlay.disable();
			self.tempUrl = this.get('href');
			
			var galleryUrl = self.options.getGalleriesLink;
			if(galleryView){
				var galleryId = $('gallery-id-field').get('value');
				galleryUrl += '&gallery_id=' + galleryId; 
			}
			movePopup.removeClass('none');
			new Fx.Tween(movePopup, {
					property: 'opacity',
					duration: 600
				}).start(0,1);
				
			new Request.JSON({
				url: galleryUrl,
				method: 'get',
				onSuccess: function (resp) {
					$('gallery-list').set('html','');
					resp.each(function(el){
						option = new Element('option', {'text': el.title, 'value' : el.id});
						option.inject($('gallery-list'),'bottom');
					});
				}
			}).send();
		});
		
		//Move popup Cancel
		movePopup.getElement('.cancel').addEvent('click', function(){
			new Fx.Tween(movePopup, {
					property: 'opacity',
					duration: 600,
					onComplete: function() {
						movePopup.addClass('none');
						self.globalOverlay.enable();
					}
				}).start(1,0);
		});
		
		//Move popup OK 
		movePopup.getElement('.ok').addEvent('click', function(){
			var ids = self.getChecked();
			if(ids){
				var galleryId = $('gallery-list').get('value');
				ids += '&gallery_id=' + galleryId;
				if(!galleryView){
					ids += '&type=1';
				}
				new Request.JSON({
					url: self.tempUrl + ids,
					method: 'get',
					onSuccess: function (resp) {
						self.globalOverlay.enable();
						if(resp.success){
							$$('.photo-list .regular-checkbox:checked').each(function(el) {
								var li = el.getParent('li');
								li.set('tween', {
									onComplete: function(){
										li.destroy();
									}
								});
								li.fade('out');
							});
						}
						else{
							alert(resp.error);
						}
					}
				}).send();
				new Fx.Tween(movePopup, {
						property: 'opacity',
						duration: 600,
						onComplete: function() {
							movePopup.addClass('none');
						}
					}).start(1,0);
			}
			else{
				alert('Please select at least one photo');
			}
			
		});
		
	},
	
	initSortables: function(){
		
		self = this;
		var sortables = new Sortables($$('.content ul'), {
			clone: function (event, element, list) {
				if ( event.event.button ) { this.reset(); return new Element('div'); }

				var pos = element.getPosition(element.getOffsetParent());
				pos.x += 5;
				pos.y += 5;

				return element.getElement('img').clone(true).setStyles({
					'margin': '0px',
					'position': 'absolute',
					'z-index': 100,
					'visibility': 'hidden',
					'opacity': .7,
					'width': element.getElement('img').getStyle('width')
				}).inject(this.list).position(pos);
			},

			opacity: 1,
			revert: true,
			handle: '.move',

			onStart: function (el) {
				this.drag.addEvent('enter', function () {
					this.started = true;
				}.bind(this));
			},

			onComplete: function (el) {
				if ( ! this.started ) return;
				else this.started = false;

				self.saveOrder(this.mode);
			}
		});
		
	},
	
	saveOrder: function () {
		var ids = [];

		$$('.photo-list li input[type=checkbox]').each(function (el) {
			ids.include(el.get('value'));
		});
		
		new Request({
			url: this.options.sortLink,
			method: 'get',
			data: { act: 'sort', photo_id: ids},
			onSuccess: function (resp) {
			}.bind(this)
		}).send();
		
	},
	
	initCropper: function () {
		var self = this;
		$$('.wrapper').each(function (el) {
			self.cropper = new Cropper(el).addEvent('complete', self.saveAdjustment);
			self.cropper.url = self.options.cropLink;
			self.cropper.rotateUrl = self.options.rotateLink;
		});
	},
	
	initCropperOne: function (el) {
		
		var self = this;
		self.cropper = new Cropper(el).addEvent('complete', self.saveAdjustment);
		self.cropper.url = self.options.cropLink;
		self.cropper.rotateUrl = self.options.rotateLink;
	},
	
	saveAdjustment: function (args) {
		this.disable();
		
		args = $extend({
			px: args.x / 190.0,
			py: args.y / 265.0
		}, args);
		
		new Request({
			url: this.url,
			method: 'get',
			data: $extend({ photo_id: this.el.get('cubix:photo-id')}, args),
			onSuccess: function (resp) {
				resp = JSON.decode(resp);

				if ( resp.error ) {
					alert('An error occured');
					this.revert();
				}
				else {
					this.setInitial(args.x, args.y);
				}

				this.enable();
			}.bind(this)
		}).send();
	},
	
	initUpload: function(form) {

		var self = this;
		if($$('.mooupload_btncontainer').length){
			$('filecontrol').set('html','');
		}
		
		var myUpload = new MooUpload('filecontrol', {
			action: form.get('action') + '&gallery_id=' + $('gallery-id-field').get('value'),
			accept: 'image/*',
			method: 'auto',				// Automatic upload method (Choose the best)
			/*blocksize: 999999,	// Load per one chunk*/
			/*multiple:false,*/
			showclearlist: true,
			texts:self._mooUpload.texts[Cubix.Lang.id],
			onBeforeUpload: function(){
                                
                                if($$('div.filename').length) {
				form.getElement('.progresscont').removeClass('none').fade('in');
                            }
                        },
			onFileUpload: function(fileindex, response){ 
				
				if(response.error == 0 && response.finish){
                                    
					
					var item = $('filecontrol_file_' + (fileindex));
					var itemBox = item.getParent('li');
					itemBox.set('tween', { onComplete: function() {this.element.destroy()}});
					itemBox.tween('opacity', 0);
					
					if ( $$('.no-photos').length ) {
						$$('.no-photos').destroy();
						new Element('ul').inject( $$('.photo-list')[0], 'top');
						new Element('div',{'class': 'clear'}).inject( $$('.photo-list')[0], 'bottom');
						
					}

					var container = $$('.photo-list ul')[0];

					var li = new Element('li').inject(container, 'top');

					li.setStyles({opacity: 0, display: 'block'});
					li.tween('opacity', 1);

					var wrapper = new Element('div', {
						'class': 'wrapper',
						'cubix:photo-id': response.photo_id,
						'cubix:initial': (response.args) ? response.args : ''
					}).inject(li, 'top');
					var img = new Element('img', {'src': response.photo_url}).inject(wrapper, 'top');
                                        
					
					var rotateActions = new Element('div', {'class': 'rotate-actions'}).inject(wrapper, 'bottom');
					new Element('a', {'class':'rotate-right', href:'#' }).inject(rotateActions, 'top');
					new Element('a', {'class':'rotate-left', href:'#' }).inject(rotateActions, 'top');
					
					var actions = new Element('div', {'class': 'actions'}).inject(wrapper, 'bottom');
					var move = new Element('span', {'class':'move'}).inject(actions, 'top');
					var check = new Element('div', {'class':'checkbox-wrapper'}).inject(actions, 'bottom');
					new Element('input', {'id': 'checkbox-'+ response.photo_id, 'class' : 'regular-checkbox', 'type':'checkbox', 'name':'photo_id', 'value': response.photo_id}).inject(check, 'top');
					new Element('label', {'for': 'checkbox-'+ response.photo_id }).inject(check, 'bottom');
					
					if ( response.is_approved == 0 ) {
						var not_approved = new Element('div', {'class': 'not-approved', 'html': 'Not Approved'}).inject(wrapper, 'bottom');
					}
					self.initSortables();
					self.initCropperOne(wrapper);
                                        

				}
			},
			onFileUploadError: function(filenum, response){
                            $('filecontrol_file_' + filenum ).set('html',response.error);

                            if (response.error == "no_main_gallery_photo") {
                                $$('#filecontrol_progresscont').addClass('none');
                                $$('.mooupload_readonly').addClass('none');
                                form.getElement('.popupgalerr').removeClass('none').fade('in');
                                self.globalOverlay.disable();
                                                             
                                form.getElement('.ok').addEvent('click', function () {
                                    self.globalOverlay.enable();
                                    $$('.popupgalerr').addClass('none');
                                    this.removeEvents('click');
                                    
                                });
                                
                            }
                            
                            
                            
			},
			onFinishUpload: function() {
				setTimeout(function() {
					var fxProg = new Fx.Tween(form.getElement('.progresscont'),{duration: 2000});
					fxProg.addEvent('complete', function(){this.element.addClass('none'); });
					fxProg.start('opacity','100','0');
				}, 2000);
			}
		});
	}
	
});

Cropper = new Class({
	Implements: [Events],

	el: null,
	els: {},
	bind: {},
	url: '',
	rotateUrl: '',
	disabled: false,

	max: { x: 0, y: 0 },
	mouse: { start: { x: 0, y: 0 }, now: { x: 0, y: 0 }, diff: { x: 0, y: 0 } },

	moved: false,

	initialize: function (el) {
		this.el = $(el);
		this.els.img = this.el.getElement('img');
		this.els.img.ondragstart = function () { return false; };
		
		var initial = JSON.decode(el.get('cubix:initial'));

		var preventCache = Number.random(1,100000);
		this.els.img.set('src', this.els.img.get('src') + '#' + preventCache);
		this.el.addEvents({
			'mouseover':function() {
				this.getElement(".rotate-actions").fade('in');
			},
			'mouseout':function() {            
				this.getElement(".rotate-actions").fade('out');
			}
		});
		this.el.getElements('.rotate-left, .rotate-right ').addEvent('click', this.rotate.bind(this));
		this.els.img.onload = function () {
			this.bind = {
				start: this.handleStart.bindWithEvent(this),
				move: this.handleMove.bindWithEvent(this),
				end: this.handleEnd.bindWithEvent(this)
			};
			
			this.max = {
				x: this.els.img.clientWidth - this.el.clientWidth,
				y: this.els.img.clientHeight - this.el.clientHeight
			};
			
			this.init();

			if ( initial ) {
				this.setInitial(initial.x, initial.y);
			}
		}.bind(this);
	},

	init: function () {
		this.els.img.addEvent('mousedown', this.bind.start);
	},

	handleStart: function (e) {
		e.stop();

		if ( this.disabled ) return;

		document.addEvent('mousemove', this.bind.move);
		document.addEvent('mouseup', this.bind.end)
		
		this.mouse.start = e.page;

		this.mouse.start.x -= this.mouse.diff.x;
		this.mouse.start.y -= this.mouse.diff.y;
	},

	handleMove: function (e) {
		this.moved = true;

		this.mouse.now = e.page;

		var x = this.mouse.now.x - this.mouse.start.x,
			y = this.mouse.now.y - this.mouse.start.y;
		
		this.mouse.diff = { x: x, y: y };

		if ( this.mouse.diff.x > 0 ) this.mouse.diff.x = 0;
		if ( this.mouse.diff.y > 0 ) this.mouse.diff.y = 0;
	
		if ( -1 * this.mouse.diff.x >= this.max.x ) {
			this.mouse.diff.x = -1 * this.max.x;
		}
		
		if ( -1 * this.mouse.diff.y >= this.max.y ) {
			this.mouse.diff.y = -1 * this.max.y;
		}
		
		this.update();
	},

	handleEnd: function (e) {
		document.removeEvent('mousemove', this.bind.move);
		document.removeEvent('mouseup', this.bind.end);

		if ( this.moved ) {
			this.fireEvent('complete', [this.mouse.diff]);
		}
	},

	update: function () {
		this.els.img.setStyles({
			left: this.mouse.diff.x,
			top: this.mouse.diff.y
		});
	},

	enable: function () {
		this.els.img.set('opacity', 1);
		this.disabled = false;
	},

	disable: function () {
		this.els.img.set('opacity', 0.5);
		this.disabled = true;
	},

	revert: function () {
		this.mouse.diff = this.initial;
		this.update();
	},

	initial: {},

	setInitial: function (x, y) {
		this.mouse.diff = { x: x, y: y };
		this.initial = { x: x, y: y }

		this.update();
	},
	
	rotate: function(e){
		e.stop();
		self = e.target;
		this.disable();
		var photoId = this.el.get('cubix:photo-id');
		var degree = 90;
		if(self.get('class') == 'rotate-right'){
			degree = -90;
		}
		new Request({
			url: this.rotateUrl,
			method: 'get',
			data: { photo_id: photoId, degree: degree },
			onSuccess: function (resp) {
				resp = JSON.decode(resp);

				if ( resp.error ) {
					alert('An error occured');
				}
				else {
					this.setInitial(0,0);
					this.el.set('cubix:initial', JSON.encode(this.initial));
					var preventCache = Number.random(1,100000);
					var image = this.els.img;

					var re = /((\?|\#).*)$/;
					newImage = image.get('src').replace(re, "");
					image.set('src',  newImage + '?cache=' + preventCache).onload = function(){ this.enable(); }.bind(this);
					//image.setStyles({ left:0, top:0 });
					

				}
			}.bind(this)
		}).send();
	}
		
});

})();