var Controls = {};
var Xmas = {};

Controls.steps_start = false;
Controls.Tip = new Class({
	Implements: [Options, Events],

	options: {
		
	},

	initialize: function () {
		this.render();
		this.bind = {
			enter: this.mouseenter.bindWithEvent(this),
			leave: this.mouseleave.bindWithEvent(this)
		};
	},

	render: function () {
		this.tip = new Element('div', { 'class': 'cx-tip' }).inject($(document.body));
	},

	attach: function (el) {
		el.addEvents({
			mouseenter: this.bind.enter,
			mouseleave: this.bind.leave
		});
//		this.mouseenter({ target: el });
	},

	mouseenter: function (e) {
		var l = this.el = e.target;
		var pos = this.el.getPosition($(document.body));

		var t = this.tip;
		if ( ! l.retrieve('tip') ) {
			l.store('tip', l.get('title')).set('title', '');
		}
		
		t.set('html', l.retrieve('tip'));
		t.setStyles({
			left: pos.x + 22,
			top: pos.y - 22,
			display: 'block'
		});
	},

	mouseleave: function (e) {
		this.tip.setStyle('display', 'none');
	}
});

Controls.LngPicker = new Class({
	Implements: [Options, Events],

	options: {

	},

	el: null,
	langs: [],

	initialize: function (el, options) {
		this.setOptions(options || {});
		
		this.el = el;
		this.langs = this.el.getElements('span');
		
		this.bind = {
			click: this.click.bindWithEvent(this)
		};

		this.attach();
		
		this.click({}, this.langs[0]);
	},

	attach: function () {
		this.langs.each(function (lng) {
			lng.addEvent('click', this.bind.click.bindWithEvent(this, [lng]));
		}.bind(this));
	},

	click: function (e, el) {
		this.langs.removeClass('active');
		el.addClass('active');

		this.fireEvent('change', [el]);
	}
});

Controls.SelectBox = new Class({
	Implements: [Options, Events],

	options: {
		disabled: false,
		name: '',
		values: [
			{ value: 1, label: 'EUR' },
			{ value: 2, label: 'USD' },
			{ value: 3, label: 'GBP' },
			{ value: 4, label: 'CHF' }
		],
		selected: 1
	},

	element: {},
	list: {},
	input: {},

	selected: {},

	disable: function () {
		this.element.addClass('cx-selectbox-disabled');
	},

	enable: function () {
		this.element.removeClass('cx-selectbox-disabled');
	},

	getValue: function (value) {
		var found = false;
		this.options.values.each(function (i) {
			if ( found ) return;
			if ( value == i.value ) {
				found = i;
			}
		});

		return found;
	},

	initialize: function (o) {
		this.setOptions(o || {});
		
		this.render();

		this.bind = {
			mouseenter: this.onMouseEnter.bindWithEvent(this),
			mouseleave: this.onMouseLeave.bindWithEvent(this),
			click: this.onClick.bindWithEvent(this),
			select: this.onSelect.bindWithEvent(this),
			hide: this.onHide.bindWithEvent(this)
		};

		this.attach();

		if ( this.options.disabled ) {
			this.disable();
		}
	},

	render: function () {
		this.element = new Element('span', {
			'class': 'cx-selectbox'
		});

		this.current = new Element('em', {
			html: this.getValue(this.options.selected).label
		}).inject(this.element);

		this.list = new Element('div', {
			'class': 'cx-selectbox-list hide'
		}).inject(this.element);

		this.options.values.each(function (i, j) {
			var el = new Element('div', {
				html: i.label
			}).inject(this.list);
			el.store('value', i);
			if ( j == 0 ) {
				el.addClass('selected');
			}
		}.bind(this));

		this.input = new Element('input', {
			type: 'hidden',
			name: this.options.name,
			value: this.getValue(this.options.selected).value
		}).inject(this.element);
	},

	onMouseEnter: function (e) {

	},

	onMouseLeave: function (e) {

	},

	onClick: function (e) {
		if ( this.element.hasClass('cx-selectbox-disabled') ) return;
		e.stop();
		if ( this.list.hasClass('hide') ) {
			this.element.addClass('cx-selectbox-pushed');
			this.list.removeClass('hide');
			$(document.body).addEvent('click', this.bind.hide);
		}
		else {
			this.onHide({});
		}
	},

	onSelect: function (e) {
		this.list.getElements('div').removeClass('selected');
		this.select(e.target);
	},

	onHide: function (e) {
		this.element.removeClass('cx-selectbox-pushed');
		this.list.addClass('hide');
		$(document.body).removeEvent('click', this.bind.hide);
	},

	attach: function () {
		this.element.addEvents({
			mouseenter: this.bind.mouseenter,
			mouseleave: this.bind.mouseleave,
			click: this.bind.click
		});

		this.list.getElements('div').addEvents({
			click: this.bind.select
		});
	},

	select: function (el) {
		var value = el.retrieve('value');
		this.input.set('value', value.value);
		this.current.set('html', value.label);
		this.selected = value;
		el.addClass('selected');
	},

	getElement: function () {
		return this.element;
	},

	privateArea: function () {
		window.location.href = '.';
	}
});

document.addEvent('domready', function () {
	var tip = new Controls.Tip;
	// Animation toggler start
	if($('how-instant-book-works')){
		$('how-instant-book-works').addEvent('click', function(){
			$$('.how-instant-book-works-body').toggleClass('show');
			$$('.how-instant-book-works-body').toggleClass('animated');
		});
	}
	// Animation toggler end

	$$('.form span.tip').each(function (el) {
		tip.attach(el);
	});

	var maskable = $$('.cbox-ii .gray-box');
	if ( ! maskable.length ) {
		maskable = $$('.agency-logo-wrapper');
	}

	if ( maskable.length ) {
		Navigation.overlay = new Cubix.Overlay(maskable[0], {
			offset: {
				left: -1,
				top: -1
			}
		});
	}
	Locker.init();
	Locker.go();
	Navigation.init();
	FormErrors.show();
	
	/********************************/
	if ($defined($('fill_contact_info')))
		$('fill_contact_info').addEvent('click', function(e) {
			e.stop();
			var self = this;

			new Request({
				url: self.get('href'),
				method: 'get',
				onSuccess: function (resp) {
					resp = JSON.decode(resp);

					$('phone_prefix').set('value', resp.phone_prefix);
					$('phone').set('value', resp.phone);
					$('instr-other').set('value', resp.phone_instructions);
					$('email').set('value', resp.email);
					$('website').set('value', resp.web);
				}
			}).send();
		});
	/********************************/

    /* if( Cookie.read('showXmasTerms') ){
        Xmas.init();
    } */
	Xmas.init( true );
	
	$$('.boost_profile').addEvent('click', function(e) {
		e.preventDefault();
		BoostProfilePopup.open();
	});

    Steps.init($('step'));
});

var Steps = {
	init : function (container) {
		if(!container) return false;

        this.initNextTab();
        this.initAboutUs();
        this.initEscortLanguages(container);
        this.initIncallOutcall();
        this.initCityPicker(container);
        this.initWorkingTimes();
        this.initContactInfo();
    },
	initNextTab:function () {

		var self = this;
        $$('.next-tab').addEvent('click', function (e) {
            e.stop();
            Controls.steps_start = true;

            $$('input[name=then]').set('value',this.get('data-value'));

            var wrapper = e.target.getParent('.form-wrapper');

            var form = wrapper.getElement('form');

            if (!$defined(form))
                return;



            var container = $('step');

            var overlay = new Cubix.Overlay(container, {
                offset: {
                    left: -1,
                    top: -1
                }
            });

            if(container.getElements('.languages').length){
                var data = form.toQueryString().parseQueryString();
                ['en','it'].forEach(function(langid) {
                    data['about_' + langid]  = tinyMCE.get('about-' + langid).getContent();
                });
            }else{
            	data = form.toQueryString();
			}


            if($('custom-modal')){
                $('custom-modal').destroy();
            }

            overlay.disable();



			new Request({
                url: form.get('action'),
                data: data,
                method: 'POST',
                onSuccess: function (resp) {
                    resp = JSON.decode(resp, true);

                	if(!resp.success){
                        self.initErrors(resp.errors);
                        overlay.enable();
                    }else{
                	    if(resp.success == 'finish'){
                            Controls.steps_start = false;
                	        window.location.href = '/private-v2/profile/gallery-step?escort='+resp.escort_id;
                	        return false;
                        }else{
                            container.set('html', resp.data);
                            self.init(container);

                            overlay.enable();
                        }
					}


                }
            }).send();

        });
        window.addEventListener("beforeunload", function (e) {
            if(!Controls.steps_start) return true;

            var confirmationMessage = 'It looks like you have been editing something. '
                + 'If you leave before saving, your changes will be lost.';

            (e || window.event).returnValue = confirmationMessage;
            return confirmationMessage;
        });
    },

    initErrors : function (errors) {

        var errors_ = '';
        var i = 1;

        if(errors){
            var i = 1;
            for (value in errors) {
                errors_ += '<p>'+ i++ +' : <span class="error-text">'+ errors[value]+'</span></p>';
            }
        }

        if(errors_){

            CustomPopup.init({
                header:'Notification !',
                body : errors_
            });

            CustomPopup.showModal();
        }
    },
    initAboutUs : function() {
        if ( typeof(tinyMCE) !== 'undefined' ) {
            tinyMCE.init({
                mode : "textareas",
                theme : "advanced",

                plugins: "emotions,advhr,preview,paste",
                theme_advanced_toolbar_location	  : "top",
                theme_advanced_toolbar_align	  : "left",
                editor_selector : 'about-field',
                theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,undo,redo,|,pastetext,pasteword,fullscreen,emotions,preview",
                theme_advanced_buttons2 : "",
                theme_advanced_buttons3_add : "",
                remove_script_host : false,
                relative_urls : false,
                convert_urls : false
            });
        }

        $$('.lng-icon').addEvent('click',function () {
        	$$('.lng-icon').removeClass('active');
        	this.addClass('active');
			var lng_slug = this.get('cx:lng');
			$$('.language_body').addClass('none');
			$$('.body-'+lng_slug).removeClass('none');

        });

        $$('.add_lang').addEvent('click',function () {

        })

    },
    initEscortLanguages : function(container) {

        if ( ! container.getElements('.languages').length ) return;

        var lng_selectbox = container.getElements('select[name="all_languages"]');
        var lng_add_btn = container.getElements('.btn-add-lng')[0];
        var selected_languages = container.getElements('.selected-languages .list')[0];


        lng_add_btn.addEvent('click', function(e){
            e.stop();

            var selected = lng_selectbox.getSelected()[0].get('value');
            var selected_title = lng_selectbox.getSelected()[0].get('html');

            var level_id = container.getElements('input[name="lng-lev"]:checked')[0].get('id');
            var level = $(level_id).get('value');
            var level_title = container.getElements('label[for="' + level_id + '"]').get('text');

            var is_in_list = false;

            if ( selected_languages.getElements('.' + selected).length ) {
                selected_languages.getElements('.' + selected).destroy();
            }

            if ( ! selected || is_in_list ) return;

            var span = new Element('span', {
                'class': selected,
                html: '&nbsp;' + selected_title + '&nbsp;&raquo;&nbsp;' + level_title +  '&nbsp;'
            });
            new Element('img', {'src': '/img/flags/' + selected + '.png'}).inject(span, 'top');
            new Element('a', {'class':'lng-remove', html: '[X Remove]'}).addEvent('click', function(e) {
                e.stop();
                this.getParent('span').destroy();
            }).inject(span);

            new Element('input', {
                'type': 'hidden',
                'name': 'langs[' + selected + ']',
                'value': level,
            }).inject(span);

            span.inject(selected_languages);

        });

        if ( selected_languages.getElements('span').length ) {
            selected_languages.getElements('span').each(function(it){
                it.getElements('a.lng-remove').addEvent('click', function(e){
                    e.stop();
                    this.getParent('span').destroy();
                });
            });
        }
    },

	initIncallOutcall : function () {
		if(!$('working-cities-step')) return false;
        var incallcheckbox = 0, outcallcheckbox = 0;

        var handleIncall = function (el) {
            el.blur();
            var disable = el.get('checked') ? null : 'disabled';
            if(el.get('checked')){
                incallcheckbox = 1;

            }else{
                incallcheckbox = 0;
                if($('onlyOneMessage')){
                    $('onlyOneMessage').destroy();
                }
            }

            $(el).getParent('.inner').getElements('input').each(function (it) {
                if ( it.get('id') == 'incall' ) return;
                it.set('disabled', disable);
                if ( disable ) it.addClass('disabled');
                else it.removeClass('disabled');
            });

            disable = $('hotel-room').get('checked') ? ($('incall').get('checked') ? null : 'disabled') : 'disabled';
            $('hotel-room').getParent('dt').getNext('dl').getElements('input').set('disabled', disable);

            disable = $('incall-other').get('checked') && el.get('checked') ? null : 'disabled';
            $('incall-other').getParent('label').getNext('input').set('disabled', disable)[(disable ? 'add' : 'remove') + 'Class']('disabled');
        };

        $('incall').getParent('.inner').getElements('input').each(function (el) {
            if ( ! ['checkbox', 'radio'].contains(el.get('type')) ) return;

            el.addEvent('change', handleIncall.pass($('incall')));
        });

        var handleOutcall = function (el) {
            el.blur();
            var disable = el.get('checked') ? null : 'disabled';

            if(el.get('checked')){
                outcallcheckbox = 1;

            }else{
                outcallcheckbox = 0;

            }
            $(el).getParent('.inner').getElements('input').each(function (it) {
                if ( it.get('id') == 'outcall' ) return;
                it.set('disabled', disable);
                if ( disable ) it.addClass('disabled');
                else it.removeClass('disabled');
            });

            disable = $('outcall-other').get('checked') && el.get('checked') ? null : 'disabled';
            $('outcall-other').getParent('label').getNext('input').set('disabled', disable)[(disable ? 'add' : 'remove') + 'Class']('disabled');
        };

        $('outcall').getParent('.inner').getElements('input').each(function (el) {
            if ( ! ['checkbox', 'radio'].contains(el.get('type')) ) return;

            el.addEvent('change', handleOutcall.pass($('outcall')));
        });



    },
	initCityPicker : function (container) {
		if(!$('working-cities-step')) return false;

        var els = {
            add: container.getElement('#add-city'),
            //addBase: container.getElement('#base-city'),
            addCityzone: container.getElement('#add-city-zone'),
            remove: container.getElements('.cities-global-container .icon-remove'),
            city_base_id: container.getElements('input[name=city_id]')[0],
            cities: container.getElement('#cities'),
            base_cities: container.getElement('#base_city')
        };


        var initLocations = function () {

            var city_id = els.cities.get('value'),
                city_title = els.cities.getSelected().get('html'),
                country_title = $('country').getSelected().get('html');

            if ( ! city_id ) return;

            var found = false;
            container.getElements('#location input[type=hidden]').each(function (el) {
                var _city_id = el.get('value');
                if ( city_id == _city_id ) {
                    found = true;
                    el.set('value', city_id);
                    el.getPrevious('div.location').getElement('span.city-title').set('html', city_title);
                }
            });

            if ( ! found ) {
                var locationWrp = new Element('div', {'class': 'location-wrp'}).inject($('location'));


                var location = new Element('div', {'class': 'location'}).inject(locationWrp);
                var label = new Element('label', { for: "city-id-" + city_id}).inject(location);
                new Element('span', { 'class': 'city-title', html: city_title}).inject(label);
                new Element('span', { 'class': 'bc-title'}).inject(label);

                new Element('input', {
                    type: 'hidden',
                    name: 'cities[]',
                    value: city_id
                }).inject(locationWrp);

                new Element('a', {'class': 'icon-remove', html: '&nbsp;[X Remove]'}).addEvent('click', remove).inject(locationWrp);
            }
        };

        var clearBaseCity = function () {
            var bc = container.getElements('input[name=city_id]');
            bc.removeEvents('click');
        };

        var remove = function () {
            if ( this.getParent('.location-wrp') ) {
                this.getParent('.location-wrp').destroy();

                clearBaseCity();
            }
        };



        var removeLocationsFromList = function(selected_cities){
            els.indexOf( )
        };

        els.remove.addEvent('click', remove);

        els.add.addEvent('click', function(e) {
            e.stop();

            var selected = els.cities.getSelected();
            var selected_city_id = selected.get('value'),
                added_city_ids = $('location').getElements('input[name=cities]').get('value'),
                matched = false;

            added_city_ids.each(function (city_id) {
                if(selected_city_id == city_id){
                    matched = true;
                    return;
                }
            });

            if (matched) return;

            if ( selected.length == 0 ) {
                return;
            }

            selected = selected[0];
            initLocations();


            return;
        });

        els.base_cities.addEvent('click',function () {
        	var city_id = this.getSelected()[0].get('value');
            $$('#cities option').set('disabled','');
			$$('#cities option').filter(function(item, index){
                item.setAttribute('selected','');
                if(item.get('value') == city_id){
                    item.set('disabled', 'disabled');
                }
            });
            $$('#cities option:nth-child(first)').set('selected','selected');
			if($$('label[for="city-id-'+ city_id +'"]')){
                $$('label[for="city-id-'+ city_id +'"]').getParent().getParent().destroy();
			}
        });


    }

    ,initWorkingTimes:function () {
		if(!$('working-times-step')) return false;

        $$('.work-times tbody input[name^=day_]').addEvent('change', function () {
            this.blur();
            var disable = this.get('checked') ? null : 'disabled';
            var tr = this.getParent('tr')[(disable ? 'add' : 'remove') + 'Class']('work-times-disabled');
            tr.getElements('select').set('disabled', disable);
            this.getParent('tr').getElement('input.night_escort').set('disabled', disable);
        });

        $('selectall').addEvent('change', function () {
            this.blur();
            var flag = this.get('checked') ? true : false;
            $$('.work-times tbody input').set('checked', flag ? 'checked' : '').fireEvent('change');
        });

        $$('.btn-work-days-all').addEvent('click', function () {
            var selects = $('times').getElements('select');

            $$('.work-times tbody input').each(function (el) {
                if ( el.get('checked') ) {
                    el.getParent('tr').getElements('select').each(function (sel, i) {
                        sel.set('value', selects[i].getSelected()[0].get('value')).getParent('td').highlight();
                    });
                }
            });
        });
        $$('.work-times tbody input[name^=day_]').fireEvent('change');
    }

    ,showWorkingTimesPopup : function () {
		CustomPopup.init();
		$$('#custom-modal .modal-content').setStyle('width','700px')
        CustomPopup.showModal();
    }
    ,initContactInfo:function () {
	    if(!$('contact-info-step')) return false;
        var self = this;
	    var Contact = {};


        Contact.ChangePhone = function(){
            if($('change_phone'))
                $('change_phone').addEvent('click', function(e){
                    if($(this).get('disabled')=='' && confirm(Text.confirmationChangePhone))
                    {
                        Contact.SendCode(true,true);
                    }
                });

        };

        Contact.SendCode = function(async,show)
        {
            new Request({
                url: '/private-v2/profile/send-sms',
                type:'POST',
                data:{'phone':$('phone').get('value'),'prefix': $('phone_prefix').getSelected()[0].get('value')},
                async:async,
                onSuccess: function (resp) {
                    if(resp && show)
                    {	if(resp>0)
                    {
                        Cubix.RPopup.url ='/private-v2/profile/get-change-phone-view';
                        Cubix.RPopup.type = 'report-problem';
                        Cubix.RPopup.location = 'phone_container';
                        Cubix.RPopup.Show(340,0,function(){
                        Cubix.RPopup.overlay = new Cubix.Overlay($$('.RPopup-wrapper')[0], { loader: _st('loader-circular-tiny.gif'), position: '50%' });

                            var tip =  new Controls.Tip;
                            tip.attach($$('#confirm>.tip')[0]);

                            $('countr_code').set('value',$('phone_prefix').getSelected()[0].get('html'));
                            $('new_phone').set('value',$('phone').get('value'));

                            $('resend_code').addEvent('click',function(){
                                Contact.SendCode(false,false);
                            });

                            Contact.ConfirmCode();
                        });
                    }
                    else
                    {
                        self.initErrors({'error':resp});
                    }
                    }
                    else if(resp && !show)
                    {
                        setTimeout(function(){Cubix.RPopup.overlay.enable();},1500);
                    }
                }
            }).send();
        };

        Contact.ConfirmCode = function()
        {
            $('confirm_btn').addEvent('click',function(){
                var code = $('confirm_number').get('value');
                var number_pattern =/^[0-9]{4,6}$/;
                if(!number_pattern.test(code))
                {
                    $('confirm_number').addClass('invalid');
                    return false;
                }
                else
                    $('confirm_number').removeClass('invalid');
                new Request({
                    url: '/private-v2/profile/confirm-sms',
                    type:'POST',
                    data:{'code':code},
                    onSuccess:function(resp)
                    {
                        if(resp && resp=='1')
                        {
                            $('confirm_number').removeClass('invalid');
                            alert(Text.confirmationChangedSuccessfuly);

                            setTimeout(function(){
                                $$('.RPopup-wrapper .close-btn-x').fireEvent('click')
                            },1000);
                        }
                        else
                        {
                            $('confirm_number').addClass('invalid');
                        }
                    }

                }).send();
            });
        };
        Contact.ChangePhone();
    }


};

var Navigation = {
	form: null,
	step: null,
	then: null,
	changed: false,
	initial: null,
	
	init: function () {
		
		if (!$defined($$('.form')[0]))
			return;
		this.form = $$('.form')[0];
		this.step = this.form.getElement('input[name=step]');
		this.then = this.form.getElement('input[name=then]');
		this.dont = this.form.getElement('input[name=dont]');
		

		this.initial = this.form.toQueryString();
        if(!this.form.getParent('.cbox-ii')) return false;
		this.overlay = Navigation.overlay ? Navigation.overlay : new Cubix.Overlay(this.form.getParent('.cbox-ii').getFirst('.gray-box'), {
			offset: {
				left: -1,
				top: -1
			}
		});
	},

	enable: function () {
		this.overlay.enable();
	},

	disable: function () {
		this.overlay.disable();
	},

	back: function () {
		this.overlay.disable();
		this.then.set('value', ':back');
		this.form.submit();
		return false;
	},

	next: function () {
		if($('phone_confirmed') && $('phone_confirmed').get('value') == 0){
			Cubix.Popup.ShowPhoneConfirmPopup();
		}else{
			this.overlay.disable();
			this.then.set('value', ':next');
			this.form.submit();
		}

		return false;
	},

	go: function (step) {
		this.overlay.disable();
		this.then.set('value', step);
		this.form.submit();
		return false;
	},

	reload: function () {
		this.overlay.disable();
		this.dont.set('value', 1);
		this.form.submit();
		return false;
	},

	update: function () {
		if ( typeof(tineMCE) != "undefined" ) tinyMCE.triggerSave();
		this.overlay.disable();
		this.form.submit();

		return false;
	},

	reset: function () {
		this.overlay.disable();
		window.location.href = window.location.href;
		return false;
	},

	tab: function (tab) {
		if ( ! this.hasChanged() && ! new Hash(FormErrors.errors).getLength() ) {
			this.overlay.disable();
			return true;
		}

		if ( ! confirm('If you continue, all the changes you\'ve made will be lost!\n\nClick "Cancel" to stay on this page and click update to save current tab') ) {
			return false;
		}
		
		this.overlay.disable();
		return true;
	},

	privateArea: function () {
		window.location.href = '.';
	},

	hasChanged: function () {
		return this.form.toQueryString() != this.initial;
	}
};

var FormErrors = {
	errors: {},
	
	init: function (errors) {
		this.errors = errors || {};
	},
	
	show: function () {
		for ( var field in this.errors ) {
			var el = $$('*[name=' + field + ']')[0];
			if ( ! el ) continue;
			var inner = el.getParent('.inner');
			var clear = inner.getLast('.clear');
			new Element('div', { 'class': 'error', html: this.errors[field] }).inject(clear ? clear : inner, clear ? 'before' : 'bottom');
		}
	}
};

var Locker = {
	cHeight:0,
	rHeight:0,
	cStatus:0,
	rStatus:0,
	
	init:function(){
		/* COMMENT */
		var commentBox = $('my-comments');
		
		if($defined(commentBox)){
			this.cHeight = commentBox.getStyle('height');
		
			cStatus = Cookie.read('comment_locker_status');
			
			if(cStatus == 1){
				this.cStatus = cStatus;
				commentBox.getPrevious('.locker').addClass('closed');
				commentBox.setStyle('height',0);
			}
		}
		
		/* REVIEW  */
		var reviewBox = $('my-reviews');
		
		if($defined(reviewBox)){
			this.rHeight = reviewBox.getStyle('height');
			
			rStatus = Cookie.read('review_locker_status');
			if(rStatus == 1){
				this.rStatus = rStatus;
				reviewBox.getPrevious('.locker').addClass('closed');
				reviewBox.setStyle('height',0);
			}
		}
	},
		
	go: function(){
		var self = this;
		$$('.locker').addEvent('click', function(){
			var box = this.getNext('div');
			var locker = this;
			var height, autoHeight, cookieName;
			
			/* COMMENT */
			if(box.get('id') == "my-comments"){
				autoHeight = self.cHeight;
				cookieName = 'comment_locker_status';
			}
			
			/* REVIEW */
			if(box.get('id') == "my-reviews"){
				autoHeight = self.rHeight;
				cookieName = 'review_locker_status';
				
			}
			
			
			if(locker.hasClass('closed')){
				height = autoHeight;
				locker.removeClass('closed');
				Cookie.write(cookieName, 0, {duration: 365});
			}
			else{
				height = 0;
				locker.addClass('closed');
				Cookie.write(cookieName, 1, {duration: 365});
			}
			var effect = new Fx.Tween(box,  {
							duration: 1000,
							transition:  Fx.Transitions.Quart.easeInOut
							
			});
			//console.log(height);
			effect.start('height', height);
		});
	}

}
var Geography = {
	loadCities: function (city, country, city_id, hidden) {

		if ( ! $defined(hidden) ) {
			hidden = false;
		}

		var selected;
		if ( hidden ) {
			selected = country.get('value');
		}
		else {
			selected = country.getSelected();
		}
		
		if ( ! selected.length ) return;
		if ( ! hidden ) {
			var country_id = selected[0].get('value');
		}
		else {
			var country_id = selected;
		}

		if ( ! country_id ) {
			city.empty();
			new Element('option', { value: '', html: '- city -' }).inject(city);
			return;
		}

		$$(country, city).set('disabled', 'disabled');

		new Request({
			url: '/' + Cubix.Lang.id + '/geography/ajax-get-cities',
			data: { country_id: country_id },
			method: 'get',
			onSuccess: function (resp) {
				resp = JSON.decode(resp, true);
				if ( ! resp ) return;
				city.empty();
				new Element('option', { value: '', html: '- city -' }).inject(city);
				
				resp.data.each(function (c) {
					new Element('option', {
						value: c.id,
						html: c.title + '&nbsp;&nbsp;',
						selected: city_id == c.id ? 'selected' : null
					}).inject(city);
				});
				$$(country, city).set('disabled', null);
			}
		}).send();
	}
}

var changeSystem = function (sys) {
	$$('input[name=measure_units]').set('value', sys);
	return Navigation.reload();
}

var LoadEscortComments = function (params) {

		var comments_container = $('ajax-escort-comments');
		$('my-comments').setStyle('height','');

		var overlay = new Cubix.Overlay(comments_container, {
			loader: _st('loader-small.gif'),
			position: '50%'
		});
		overlay.disable();

		new Request({
			url:  "/private-v2/ajax-escort-comments",
			method: 'post',
			evalScripts: true,
			data: params,
			onSuccess: function (resp) {
				comments_container.set('html','');
				comments_container.set('html',resp);
				Locker.cHeight = $('my-comments').getStyle('height');
			}
		}).send();

		return false;
}

var LoadEscortReviews = function (params) {

		var reviews_container = $('ajax-escort-reviews');
		$('my-reviews').setStyle('height','');

		var overlay = new Cubix.Overlay(reviews_container, {
			loader: _st('loader-small.gif'),
			position: '50%'
		});
		overlay.disable();

		new Request({
			url:  "/private-v2/ajax-escort-reviews",
			method: 'post',
			evalScripts: true,
			data: params,
			onSuccess: function (resp) {
				reviews_container.set('html','');
				reviews_container.set('html',resp);
				Locker.rHeight = $('my-reviews').getStyle('height');

			}
		}).send();

		return false;
};

Xmas = {

    overlay: null,
    isChecked: false,
	isCustomShow: false,

    init: function( is_custom_show ){
        var self = this, xmas_url;
		self.isCustomShow = ( is_custom_show ) ? is_custom_show : false;

		if( self.isCustomShow ){
			xmas_url = "/xmas/get-simple-status";
			if( Cookie.read('xmas_simple_closed') ) return false;
		} else {
			xmas_url = "/xmas/get-status";
		}

//        var box_width = 880;
//        var box_height = 620;
//
//        var y_offset = 0; //50;

        new Request({
            url: xmas_url,
            method: 'post',
            evalScripts: true,
            data: {},
            onRequest: function(){
                Xmas.loading( true );
            },
            onSuccess: function (resp) {
                resp = JSON.decode(resp, true);

                if( !resp || resp.status == 'out' ){
                    Xmas.loading( false );
                    return false;
                }

				if( !self.isCustomShow ) {
					if (resp.status != 'out') {
						self.overlay = new Cubix.Overlay(document.body, {
							has_loader: false,
							opacity: 0.5,
							color: '#000'
						});

						var container = new Element('div', {'id': 'termsPopup'}).inject(document.body);
						self.overlay.disable();
						container.fade('in');
					} else {
						Xmas.loading(false);
					}

					if (resp.status == 'in_paid') {
						self.showPaidPopup('1');
					} else if (resp.status == 'in_game') {
						self.showPaidPopup('2');
					} else {
						return false;
					}
				} else {
					var simple_container = null;
					self.overlay = new Cubix.Overlay( document.body, {
						has_loader: false,
						opacity: 0.5,
						color: '#000'
					});

					if (resp.status == 'in_paid') {
						self.showSimplePopup(1, self.overlay);
					} else if (resp.status == 'in_game') {
						self.showSimplePopup(2, self.overlay);
					} else {
						return false;
					}
				}
            }
        }).send();
    },

	showSimplePopup: function( status, overlay ){
		if( status == 1 ) {
			var _posreal_inner = new Element('div', {'class': 'posFixXmas'}).inject( document.body),
				simple_container = new Element('div', {'id': 'simplePopupXmas'}).inject( _posreal_inner );

			new Request({
				url: "xmas/simple-popup",
				method: 'post',
				evalScripts: true,
				data: { secure: 1 },
				onRequest: function () {

				},
				onSuccess: function ( __html ) {
					overlay.disable();
					Xmas.loading(false);
					simple_container.set( 'html', __html );

					Xmas.simplePopup.init();
				}
			}).send();
		} else if( status == 2 ){
			new Request({
				url: "xmas/get-if-win",
				method: 'post',
				evalScripts: true,
				onRequest: function () {

				},
				onSuccess: function ( __resp ) {
					var resp = JSON.decode( __resp, true );

					if( resp.data.status == 2 ){
						overlay.disable();
						Xmas.winnerPopup.init( resp.data.text );
					} else{
						Xmas.loading(false);
						return false;
					}
				}
			}).send();
		}
	},

	winnerPopup: {
		init: function( __package ){
			var self = this,
				_posreal_inner = new Element('div', {'class': 'posFixWinnerXmas'}).inject( document.body),
				simple_container = new Element('div', {'id': 'simpleWinnerPopupXmas'}).inject( _posreal_inner );

			new Request({
				url: "xmas/winner-popup",
				method: 'post',
				evalScripts: true,
				data: { secure: 1 },
				onRequest: function () {

				},
				onSuccess: function ( __html ) {
					Xmas.loading(false);
					simple_container.set( 'html', ( __html.replace( /\{prize\}/gi, ( '<span class="win-package">' + __package + '</span>' ) ) ) );

					self.bindEvents( true );
				}
			}).send();
		},

		bindEvents: function( __cookie ){
			var __close_btn = $$('.xmas-winner-close');

			__close_btn.addEvent('click', function(){
				$$('.overlay').dispose();
				$$('.posFixWinnerXmas').dispose();

				if( __cookie ){
					Cookie.write('xmas_simple_closed', 1, { duration: 1 });
				}
			})
		}
	},

    showPaidPopup: function( status ){
        var self = this,
            container,
            cont = $('termsPopup');

        new Request({
            url:  "/xmas/get-agree",
            method: 'post',
            evalScripts: true,
            data: {},
            onRequest: function(){

            },
            onSuccess: function (resp) {
                resp = JSON.decode(resp, true);
                self.isChecked = ( resp.status == 1 );

                if( !self.isChecked ){

                    new Request({
                        url:  "/xmas/terms",
                        method: 'post',
                        evalScripts: true,
                        data: { st : status },
                        onRequest: function(){
                            container = new Element('div', { 'id': 'termContainer' }).inject(cont);
                        },
                        onSuccess: function ( resp ) {
                            Xmas.loading( false );
                            $('termsPopup').setStyle( 'background', '#ffffff' );
                            container.setStyle( 'background', '#000000' );
                            new Element('div', { html: resp, class : 'term-wrapper' }).inject( container );
                            if( status == 1 ){ self.setTimer(); }
                            var myScrollable = new Scrollable($('rulesText'));
                            var mooniformInstance = new Mooniform($$('input[type="checkbox"]'));

                            $$('.closeTerms, .decline-en, .decline-it').addEvent('click', function(){
                                //if( this.hasClass('closeTerms') ){
                                Cookie.dispose('showXmasTerms');
                                //}

                                self.overlay.enable();
                                $$('.scrollbar').setStyle('display', 'none');
                                cont.fade('out');
                            });

                            $$('.accept-en, .accept-it').addEvent('click', function(){
                                //mooniformInstance.update();
                                var _checkbox = $('termCheckbox');
                                if( !_checkbox.get('checked') ){
                                    $('termsAgree').addClass('err');
                                } else {
                                    new Request({
                                        url:  "/xmas/set-agree",
                                        method: 'post',
                                        evalScripts: true,
                                        data: { agree : _checkbox.get('value') },
                                        onRequest: function(){
                                            Xmas.loading( true );
                                            //Cookie.write('show-xmas-terms', 1, { duration: 1 });
                                            $$('.accept-en, .accept-it').setStyle('background', '#ffffff url("/img/loader-circular-tiny.gif") center center no-repeat;');
                                        },
                                        onSuccess: function (resp) {
                                            Xmas.loading( false );
                                            resp = JSON.decode(resp, true);
                                            /*self.overlay.enable();
                                            $$('.scrollbar').setStyle('display', 'none');
                                            cont.fade('out');*/

                                            self.renderGame();
                                        }
                                    }).send();
                                }
                            });
                        }
                    }).send();
                } else {
                    self.renderGame();
                }
            }
        }).send();


    },

    renderGame: function( ball_day ){
        var self = this, user_data,
        cont = $('termsPopup');

        new Request({
            url:  "/xmas/game",
            method: 'post',
            evalScripts: true,
            data: {},
            onRequest: function(){
                Xmas.loading( true );
            },
            onSuccess: function ( resp, xhr ) {
                $('termsPopup').setStyle( 'background', '#ffffff' );
                cont.empty(); $$('.scrollbar').dispose();
                var container = new Element('div', { html: resp, class: 'game-wrapper' }).inject( cont );
                Xmas.loading( false );
                user_data = $('userXmasVotes').get('value');
                Christmas.Game.init( user_data, ball_day );

                $$('.closeTerms').addEvent('click', function(){
					Cookie.dispose('showXmasTerms');
                    self.overlay.enable();
                    cont.fade('out');
                });
            }
        }).send();
    },

    loading: function( visible ){
        var elm = $('loadbar');
        if( !elm ){
            new Element('div', { id: 'loadbar', text: 'Loading..' }).inject( $(document.body) );
            elm = $('loadbar');
        }

        if( visible ){
            elm.fade('in');
        } else {
            elm.fade('out');
        }
    },

    setTimer: function(){
        new Request({
            url:  "/xmas/get-timestamps",
            method: 'post',
            evalScripts: true,
            data: {},
            onSuccess: function ( resp ) {
                resp = JSON.decode( resp );
                var endDate = resp.end * 1000;
                //console.log( new Date(new Date().getTime() + 30000) );
                var counterCont = $('counterBlock'),
                    coundown = new CountDown({
//                        date: new Date(new Date().getTime() + 30000),
                        date: new Date( endDate ),
                        frequency: 100,
                        onChange: function(counter) {
                            var days = '',
                                hours = '',
                                minutes = '',
                                seconds = '';

                            //if(counter.days > 0) text = counter.days + ' d ';

                            days = (counter.days >= 10 ? '' : '0') + counter.days;
                            hours = (counter.hours >= 10 ? '' : '0') + counter.hours; // + ':';
                            minutes = (counter.minutes >= 10 ? '' : '0') + counter.minutes; // + ':';
                            seconds = (counter.second >= 10 ? '' : '0') + counter.second; // + ':';
                            //text += ' - ' + (counter.millis > 10 ? '' : '0') + counter.millis + ':';

                            counterCont.getElements('.cDays').set('html', '<span class="cName">Days</span><span class="cSep"></span>' + days);
                            counterCont.getElements('.cHours').set('html', '<span class="cName">Hours</span><span class="cSep"></span>' + hours);
                            counterCont.getElements('.cMinutes').set('html', '<span class="cName">Minutes</span><span class="cSep"></span>' + minutes);
                            counterCont.getElements('.cSeconds').set('html', '<span class="cName">Seconds</span>' + seconds);
                        },
                        //complete
                        onComplete: function () {
                            counterCont.set('text', 'Countdown completed.')
                        }
                    });
            }
        }).send();


    }
};

var Christmas = {
    Game: null
};

/*[ 460,430,400,385,412,390,333,361,347,346,315,324,293,320,291,279,250,250,212,235,195,164,161,130,0 ],*/
/* Change in css add new 2016 comment block */
Christmas.Game = {
    balls_count: 25,
    ballsTops: [ 460,430,400,385,412,390,431,392,380,403,346,334,296,320,335,279,250,268,212,235,195,164,161,130,0 ],
    ballsCount: 7, /* 1 */
    interval: null,
    current_elm: null,
    user_data: null,
    messages: {
        'en': {
            'winner': 'Congratulations! Your winner code is: %winCode%. Please contact your sales agent for more info.',
            'no_winner': 'You did not win! Try your luck again next time'
        },
        'it': {
            'winner': 'Congratulazioni! Il tuo codice vincente è %winCode% Contatta il tuo sales per maggiori informazioni',
            'no_winner': 'Non hai vinto! Ritenta la fortuna con la prossima estrazione'
        }
    },

    init: function( user_data, current_ball ){
        var self = this;
        self.balls_count = 25;
        self.ballsCount = 7; /* 1 */
        self.interval = null;
        self.current_elm = null;
        self.user_data = null;

        self.user_data = user_data;
        self.generateBalls();
        self.setDefaultPositions();
        self.interval = setInterval(function(){ self.animateBalls( current_ball ) }, 50);

        $$('[id^="chr_ball_"]').addEvents({
            'mouseenter': function(){ $(this).setStyle('z-index', 99999999999999) },
            'mouseleave': function(){ $(this).setStyle('z-index', 10) }
        });

        $$('.active.clr').addEvent('click', function(){
            new Request({
                url:  "/xmas/game",
                method: 'post',
                evalScripts: true,
                data: {},
                onRequest: function(){
                    Xmas.loading( true );
                },
                onSuccess: function ( resp, xhr ) {
                    $('termsPopup').setStyle( 'background', '#ffffff' );
                    cont.empty(); $$('.scrollbar').dispose();
                    var container = new Element('div', { html: resp, class: 'game-wrapper' }).inject( cont );
                    Xmas.loading( false );
                    user_data = $('userXmasVotes').get('value');
                    Christmas.Game.init( user_data );

                    $$('.closeTerms').addEvent('click', function(){
                        self.overlay.enable();
                        cont.fade('out');
                    });
                }
            }).send();
        });
    },

    generateBalls: function(){
        var self = this;
        var ball_arr = [];
        var wrapper = $('popup-wrapper');

        for( var i = 1; i < self.balls_count + 1; i += 1 ){
            ball_arr.push( new Element('span', { 'id': 'chr_ball_' + i, 'data-id': i } ) );
        }

        if( ball_arr.length == self.balls_count ){
            ball_arr.each(function (ball) {
                ball.inject(wrapper);
            });
        }

    },

    setDefaultPositions: function(){
        var self = this;
        $$('[id^=chr_ball_]').setStyles({ 'top' : '-60px', 'zIndex' : 10, 'opacity' : 0 });
        $('chr_ball_25').setStyles({ 'top' : '-120px', 'zIndex' : 5 });
    },

    getUserVotes: function(){
        var self = this;
        return JSON.decode( self.user_data );
    },

    animateBalls: function( current_ball ){
        var self = Christmas.Game, elm, elm_id, current_elm,
            ballsTops = self.ballsTops;

        var d = new Date();
        var n = d.getDate();
        var user_data = self.getUserVotes();

        elm_id = 'chr_ball_' + self.ballsCount;
        elm = $(elm_id);

        if( elm ){
			console.log( elm );
            var myEffect = new Fx.Morph(elm_id, {
                duration: 150,
                transition: Fx.Transitions.Back.easeOut
            });

            myEffect.start({
                'top': ballsTops[ self.ballsCount - 1 ],
                'opacity': 1
            });

			elm.addClass( 'clr' );

            var userData = user_data[ self.ballsCount ], winner_msg;

            if( self.ballsCount == self.balls_count ){
                setTimeout( function(){ $$('.current-day-ball').fade('in') }, 1000 );
            }

            if( self.ballsCount == n ){
                self.current_elm = elm;
                elm.addClass('active-ball');
                var current_day = new Element( 'span', { 'class': 'current-day-ball' } );
                current_day.inject($('popup-wrapper'));

                var _top = self.current_elm.getStyle('top').replace('px', '');
                var _left = self.current_elm.getStyle('left').replace('px', '');
                var _width = self.current_elm.getStyle('width').replace('px', '');
                var _height = self.current_elm.getStyle('height').replace('px', '');

                if( self.ballsCount == 25 ){
                    $$('.current-day-ball').setStyles({ 'display' : 'none' });
                } else {
                    $$('.current-day-ball').setStyles({ 'top' : ballsTops[ self.ballsCount - 1 ] - 100,
                        'left': _left - 100,
                        'width': ( Number( _width ) + 200 ) + 'px',
                        'height': ( Number( _height ) + 200 ) + 'px' });
                }

                self.sendVote();
            }

            if( userData ){
                if( userData.vote != 2 ){
                    setTimeout( function(){ elm.removeClass( 'clr' ) }, 1000 );
                } else {
                    setTimeout( function(){ elm.addClass( 'for-game').removeClass('active-ball') }, 1000 );
                    var lang = Cookie.read('ln');
                        if( !lang ) lang = 'it';
                    if( userData.win_code && userData.win_code != '' ){
                        winner_msg = self.messages[ lang ].winner;
                        winner_msg = new Element( 'span', { 'html': winner_msg.split('%winCode%').join('<b>' + userData.win_code + '</b>'), 'class': 'winner-message-ok-' + self.ballsCount } );
                    } else {
                        winner_msg = new Element( 'span', { 'html': self.messages[ lang ].no_winner, 'class': 'winner-message-' + self.ballsCount } );
                    }
                    winner_msg.inject( elm );

                    if( self.ballsCount == current_ball ){
                        elm.setStyle('z-index', '9999999');
                        winner_msg.setStyle('display', 'block');
                    }
                }
            }
            self.ballsCount += 1;
        } else {
            clearInterval(self.interval);
        }
    },

    sendVote: function(){
        var self = this;

        $$('.active-ball').addEvent('click', function(){
            if( $(this).hasClass('active-ball') ){
                var ball_day = Number( $(this).get('data-id') );
                new Request({
                    url:  "/xmas/send-vote",
                    method: 'post',
                    evalScripts: true,
                    data: {},
                    onRequest: function(){
                        Xmas.loading( true );
                    },
                    onSuccess: function ( resp ) {
                        /*$('termsPopup').setStyle( 'background', '#ffffff' );
                        cont.empty(); $$('.scrollbar').dispose();
                        var container = new Element('div', { html: resp, class: 'game-wrapper' }).inject( cont );
                        user_data = $('userXmasVotes').get('value');
                        Christmas.Game.init( user_data );

                        $$('.closeTerms').addEvent('click', function(){
                            self.overlay.enable();
                            cont.fade('out');
                        });*/
                        Xmas.renderGame( ball_day );
                        Xmas.loading( false );
                    }
                }).send();
            }
        });
    }
};

var Scrollable = new Class({
    Implements: [Options, Events],

    options: {
        autoHide: 1,
        fade: 1,
        className: 'scrollbar',

        proportional: true,
        proportionalMinHeight: 15/*,
         onContentHeightChange: function(){}*/
    },

    initialize: function(element, options) {
        this.setOptions(options);

        if (typeOf(element) == 'elements') {
            var collection = [];
            element.each(function(element) {
                collection.push(new Scrollable(element, options));
            });
            return collection;
        }
        else {
            var scrollable = this;
            this.element = document.id(element);
            if (!this.element) return 0;

            this.active = false;
            var _cont = $('rulesText');

            // Renders a scrollbar over the given element
            this.container = new Element('div', {
                'class': this.options.className,
                html: '<div class="knob"></div>'
            }).inject(document.body, 'bottom');
            this.slider = new Slider(this.container, this.container.getElement('div'), {
                mode: 'vertical',
                onChange: function(step) {
                    this.element.scrollTop = ((this.element.scrollHeight - this.element.offsetHeight) * (step / 100));
                }.bind(this)
            });
            this.knob = this.container.getElement('div');
            this.reposition();
            if (!this.options.autoHide) this.container.fade('show');

            this.element.addEvents({
                'mouseenter': function() {
                    if (this.scrollHeight > this.offsetHeight) {
                        scrollable.showContainer();
                    }
                    scrollable.reposition();
                },
                'mouseleave': function(e) {
                    if (!scrollable.isInside(e) && !scrollable.active) {
                        scrollable.hideContainer();
                    }
                },
                // Making the element scrollable via mousewheel
                'mousewheel': function(event) {
                    event.preventDefault();    // Stops the entire page from scrolling when mouse is located over the element
                    if ((event.wheel < 0 && this.scrollTop < (this.scrollHeight - this.offsetHeight)) || (event.wheel > 0 && this.scrollTop > 0)) {
                        this.scrollTop = this.scrollTop - (event.wheel * 30);
                        scrollable.reposition();
                    }
                },
                'Scrollable:contentHeightChange': function() {
                    //this scrollable:contentHeightChange could be fired on the current element in order
                    //to get a custom action invoked (implemented in onContentHeightChange option)
                    scrollable.fireEvent('contentHeightChange');
                }
            });
            this.container.addEvent('mouseleave', function() {
                if (!scrollable.active) {
                    scrollable.hideContainer();
                }
            });
            this.knob.addEvent('mousedown', function(e) {
                scrollable.active = true;
                window.addEvent('mouseup', function(e) {
                    scrollable.active = false;
                    if (!scrollable.isInside(e)) {
                        // If mouse is outside the boundaries of the target element
                        scrollable.hideContainer();
                    }
                    this.removeEvents('mouseup');
                });
            });
            window.addEvents({
                'resize': function() {
                    scrollable.reposition.delay(50,scrollable);
                },
                'mousewheel': function() {
                    if (scrollable.element.isVisible()) scrollable.reposition();
                }
            });

            // Initial hiding of the scrollbar
            if (this.options.autoHide) scrollable.container.fade('hide');

            return this;
        }
    },
    reposition: function() {
        // Repositions the scrollbar by rereading the container element's dimensions/position
        (function() {
            this.size = this.element.getComputedSize();
            this.position = this.element.getPosition();
            var containerSize = this.container.getSize();

            this.size['totalWidth'] = 450;
            this.size['computedTop'] = 0;

            this.container.setStyle('height', this.size['height']).setPosition({
                x: (this.position.x+this.size['totalWidth']-containerSize.x),
                y: (this.position.y+this.size['computedTop'])
            });
            this.slider.autosize();
        }).bind(this).delay(50);

        if (this.options.proportional === true) {
            if (isNaN(this.options.proportionalMinHeight) || this.options.proportionalMinHeight <= 0) {
                throw new Error('Scrollable: option "proportionalMinHeight" is not a positive number.');
            } else {
                var minHeight = Math.abs(this.options.proportionalMinHeight);
                var knobHeight = this.element.offsetHeight * (this.element.offsetHeight / this.element.scrollHeight);
                this.knob.setStyle('height', Math.max(knobHeight, minHeight));
            }
        }

        this.slider.set(Math.round((this.element.scrollTop / (this.element.scrollHeight - this.element.offsetHeight)) * 100));
    },

    /**
     * Scrolls the scrollable area to the bottommost position
     */
    scrollBottom: function() {
        this.element.scrollTop = this.element.scrollHeight;
        this.reposition();
    },

    /**
     * Scrolls the scrollable area to the topmost position
     */
    scrollTop: function() {
        this.element.scrollTop = 0;
        this.reposition();
    },

    isInside: function(e) {
        if (e.client.x > this.position.x && e.client.x < (this.position.x + this.size.totalWidth) && e.client.y > this.position.y && e.client.y < (this.position.y + this.size.totalHeight))
            return true;
        else return false;
    },
    showContainer: function(force) {
        if ((this.options.autoHide && this.options.fade && !this.active) || (force && this.options.fade)) this.container.fade('in');
        else if ((this.options.autoHide && !this.options.fade && !this.active) || (force && !this.options.fade)) this.container.fade('show');
    },
    hideContainer: function(force) {
        if ((this.options.autoHide && this.options.fade && !this.active) || (force && this.options.fade)) this.container.fade('out');
        else if ((this.options.autoHide && !this.options.fade && !this.active) || (force && !this.options.fade)) this.container.fade('hide');
    },
    terminate: function() {
        this.container.destroy();
    }
});


var CamInfoPopup = {};
CamInfoPopup.open = function() {
	if($('CamInfoPopup')){
		$('CamInfoPopup').setStyle('display', 'block');
		
	}
};
CamInfoPopup.close = function() {
	if($('CamInfoPopup')){
		$('CamInfoPopup').setStyle('display', 'none');
	}
};

var BoostProfilePopup = {};
BoostProfilePopup.open = function() {
	if ( $('profile-booster-popup') ) {
		$('profile-booster-popup').removeClass('hidden');
		$$('.navigation-overlay').setStyle('visibility', 'visible');
	}
};
BoostProfilePopup.close = function() {
	if ( $('profile-booster-popup') ) {
		$('profile-booster-popup').addClass('hidden');
		$$('.navigation-overlay').setStyle('visibility', 'hidden');
	}
};

CustomPopup = {

    isOpened:false,
    modal : '',
    init: function (options) {


        if (!$('custom-modal')) {
            var modal = new Element('div', {'id': 'custom-modal'}).inject($(document.body));


            $('custom-modal').set("html",
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<span class="close-popup-button">&times;</span>' +
                '<h2>' + options.header + '</h2>' +
                '</div>' +
                '<div class="modal-body">' + options.body + '<div class="ok-popup"><button class="close-button">OK</button></div></div></div>'
            );
        }
        this.modal = $('custom-modal');


        var self = this;

        $$('.close-popup-button,.close-button').addEvent('click', function () {
            self.hideModal();
        });

    },

    showModal: function () {
       this.modal.setStyle('display','block ');
       this.isOpened = true;
    },
    hideModal: function () {

        this.modal.setStyle('display','none');
        this.isOpened = false;
    }
};