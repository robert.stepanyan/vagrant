window.addEvent('domready', function () {
  toggleSkype();
});

var toggleSkype = function(){
  $$('#toggle').addEvent('change', function(){
       var checkbox = $$('input.toggle-value:checked');
       if(typeof checkbox[0]!='undefined'){
         disableSkypeSettings();
          saveSkypeSettings();
       }else{
           enableSkypeSettings();
          saveSkypeSettings();
       }
  }),
  $$('.skype-booking-btn').addEvent('click', function(){
      saveSkypeSettings();
  });


  //this is so stupid omg
  $$("#price-60").addEvent('change',function() {
      var should_be_selected = $(this).getElements(":selected")[0].get('class');
      $$("#price-30").getElement('.' + should_be_selected)[0].selected=true;
     settks();
  }); 
  $$("#price-30").addEvent('change',function() {
     var should_be_selected = $(this).getElements(":selected")[0].get('class');
     $$("#price-60").getElement('.' + should_be_selected)[0].selected=true;
     settks();
       
  }); 
}

var settks = function(){
   $$('#price30tks').set('html', $$("#price-30").getElements(":selected")[0].get("value") * 10);
   $$('#price60tks').set('html', $$("#price-60").getElements(":selected")[0].get("value") * 10);
}

var saveSkypeSettings = function(){
  loader(1);
  var formObjects = $('skype-form').toQueryString().parseQueryString();
  var ajax = new Request({
    url: '/skype/update',
    method:'POST',
    data: formObjects,
    onSuccess: function(resp){
    resp = JSON.decode(resp);

     if(resp.status == 'error'){
        $$('input').removeClass('skype-input-error');
        let inputName = Object.keys(resp.msgs)[0];
        let inputValues = Object.values(resp.msgs)[0];
        $(inputName).addClass('skype-input-error');
        //inputValues
        $('skype-status').set('html', '');
      }
      else{
        $('skype-status').set('html', 'Settings successfully saved');
        $$('input').removeClass('skype-input-error');
      }
    },
    onComplete:function()
    {
     loader(0);
    }
  });

  ajax.send();
}


var loader = function(status){
   if(status == 1){
     $$('.skype-overlay').removeClass('none');
      $$('.toggle-skype-btn').setStyle('pointer-events', 'none');
   }else{
     $$('.skype-overlay').addClass('none');
     $$('.toggle-skype-btn').setStyle('pointer-events', 'auto');
   }
}

var enableSkypeSettings = function(){
   $$('.skype-block-body').addClass('none');
}
var disableSkypeSettings = function(){
  $$('.skype-block-body').removeClass('none');
}