Cubix.SelfCheckoutWizard = {}

Cubix.SelfCheckoutWizard.userType;
Cubix.SelfCheckoutWizard.url = 'online-billing-v2/wizard';
Cubix.SelfCheckoutWizard.overlay;
Cubix.SelfCheckoutWizard.loader;
Cubix.SelfCheckoutWizard.inProcess = false;
Cubix.SelfCheckoutWizard.width = 664;
Cubix.SelfCheckoutWizard.Price = 0;
Cubix.SelfCheckoutWizard.mooniformInstance = 0;

Cubix.SelfCheckoutWizard.OptProductsPrices = {};

Cubix.SelfCheckoutWizard.addCitiesCount;
Cubix.SelfCheckoutWizard.packWithPremCities;
Cubix.SelfCheckoutWizard.selectProductError;

Cubix.SelfCheckoutWizard.cryptoPrice = false;

Cubix.SelfCheckoutWizard.open = function(el) {
	if ( Cubix.SelfCheckoutWizard.inProcess ) return false;

	Cubix.SelfCheckoutWizard.overlay = new Cubix.Overlay(document.body, {has_loader: false, opacity: 0.5, color: '#000'});
	Cubix.SelfCheckoutWizard.overlay.disable();
	
	var x_offset = 100
		y_offset = -50;
	
	var container = new Element('div', {'class': 'self-checkout-wizard-wrapper'}).setStyles({
		left: window.getWidth() / 2 - Cubix.SelfCheckoutWizard.width / 2 - x_offset,
		top:  el.getPosition().y + y_offset,
		opacity: 0,
        position: 'absolute',
        'z-index': 101,
        background: '#fff'
	}).inject(document.body);
	
	Cubix.SelfCheckoutWizard.inProcess = true;

	new Request({
		url: Cubix.SelfCheckoutWizard.url,
		method: 'get',
		onSuccess: function (resp) {
			Cubix.SelfCheckoutWizard.inProcess = false;
			container.set('html', resp);
			
			Cubix.SelfCheckoutWizard.mooniformInstance = new Mooniform(container.getElements('select, input[type="checkbox"], input[type="radio"]'));
			
			container.setStyle('opacity', 1);
			
			Cubix.SelfCheckoutWizard.initEvents();
			
			Cubix.SelfCheckoutWizard.loader = new Cubix.Overlay(container, {loader: _st('loader-small.gif'), position: '50%', no_relative: true});
		}
	}).send();
}

Cubix.SelfCheckoutWizard.initEvents = function() {
	$$('.self-checkout-wizard-wrapper .close, .self-checkout-wizard-wrapper .cancel-btn, .self-checkout-wizard-wrapper .cancel-btn-small').addEvent('click', function() {
		$$('.self-checkout-wizard-wrapper').destroy();
		Cubix.SelfCheckoutWizard.overlay.enable();
	}); 

	if ($('packageExpirationDate')) {
		var packageExpirationDate = $('packageExpirationDate').get('value') !== "" ? $('packageExpirationDate').get('value') : new Date();
	}

	new DatePicker($('activationDatepicker'), {
		positionOffset: {x: 5, y: 0},
		format: 'j-m-Y',
		minDate: packageExpirationDate
	});

	$('activationType').addEvent('change', function() {
		if (this.get('value') === 'on_date') {
			$$('.activation-date-block')[0].show();
		} else {
			$$('.activation-date-block')[0].hide();
		}
	});

	$('package').addEvent('change', function() {
		var optContainer = $$('.self-checkout-wizard-wrapper .optional-products')[0];
		optContainer.empty();
		$('premium-city').set('value', '').empty().setProperty('disabled', 'disabled');
		$$('.optional-products-container').setStyle('display', 'none');
		$$('.selected-cities')[0].empty();
		$$('.self-checkout-wizard-wrapper span.error').set('html', '');
		
		if ( ! this.get('value') ) {
			Cubix.SelfCheckoutWizard.updateMooniform();
			return;
		}
		
		Cubix.SelfCheckoutWizard.loader.disable();
		var packageId = this.get('value');
		var optProductUrl = 'online-billing-v2/get-optional-products?package_id=' + packageId;
		if ( Cubix.SelfCheckoutWizard.userType == 'agency' ) {
			optProductUrl += '&escort_id=' + $('escort').getSelected()[0].get('value')
		}
		new Request.JSON({
			url: optProductUrl,
			method: 'get',
			onSuccess: function (resp) {
				resp.optional_products.each(function(pr, i) {
					var lbl = new Element('label');
					new Element('input', {
						value: pr.id,
						type: 'checkbox',
						name: 'opt_products[]'
					}).inject(lbl);

					new Element('span', {
						html: pr.name
					}).inject(lbl);

					lbl.inject(optContainer);
					new Element('div', {'class' : 'clear'}).inject(optContainer);
					
					if ( pr.price ) {
						Cubix.SelfCheckoutWizard.OptProductsPrices[pr.id] = pr.price;
					}
				});

				resp.additional_cities.each(function(pr, i) {
					var lbl = new Element('label');
					new Element('input', {
						type: 'radio',
						name: 'opt_product_prem_city',
						value: pr.id
					}).inject(lbl);

					new Element('span', {
						html: pr.name
					}).inject(lbl);

					lbl.inject(optContainer);
					new Element('div', {'class' : 'clear'}).inject(optContainer);
					
					if ( pr.price ) {
						Cubix.SelfCheckoutWizard.OptProductsPrices[pr.id] = pr.price;
					}
				});

				resp.cities.each(function(pr, i) {
					new Element('option', {
						value: pr.id,
						html: pr.title
					}).inject($('premium-city'));
				});
				
				if ( resp.additional_cities.length || resp.isPackageWithPremCities ) {
					$('premium-city').removeProperty('disabled');
				}
				if ( resp.additional_cities.length + resp.optional_products.length > 0 ) {
					$$('.optional-products-container').setStyle('display', 'block');
				}
				
				// Special package temp logic
				/*var specialPackages = [109, 110, 111,112,121,124,127,130,133,136,139,142,145];
				var packArr = packageId.split("-");
				
				if(specialPackages.includes(parseInt(packArr[0]))){
					$$('.activation-date-block')[0].hide();
					$$("#activationType [value='asap']").set('selected', true);
					$$("#activationType [value='on_date']").set('disabled', true);
					$$()
				}else{
					$$("#activationType [value='on_date']").set('disabled', false);
				}*/
				// END of Special temp logic
				
				Cubix.SelfCheckoutWizard.updateMooniform();
				
				Cubix.SelfCheckoutWizard.initOptProductsEvents();
				
				Cubix.SelfCheckoutWizard.UpdatePrice();
				Cubix.SelfCheckoutWizard.loader.enable();
			}
		}).send();
	});
	
	$$('.self-checkout-wizard-wrapper .btn-add').addEvent('click', function() {
		if ( $('premium-city').get('disabled') ) return;
		
		if ( ! $('premium-city').get('value') ) return;
		
		if ( $('prem-city-' +  $('premium-city').get('value')) ) {
			$('prem-city-' +  $('premium-city').get('value')).highlight('#f8e7e7');
			return;
		}
		
		var selectedCitiesCount = $$('.self-checkout-wizard-wrapper .prem-city-row').length,
			allowedCities = 0,
			packageId = $('package').getSelected()[0].get('value'),
			selectedProduct = false;
			
		packageId = packageId.split('-')[0];
		
		if ( Cubix.SelfCheckoutWizard.packWithPremCities.indexOf(packageId) ) {
			allowedCities += 1;
		}
		
		if ( $$('input[name="opt_product_prem_city"]:checked').length )  {
			selectedProduct = $$('input[name="opt_product_prem_city"]:checked')[0].get('value');
			
			if ( Cubix.SelfCheckoutWizard.addCitiesCount[selectedProduct]  ) {
				allowedCities += Cubix.SelfCheckoutWizard.addCitiesCount[selectedProduct];
			}
		}
		
		if ( selectedCitiesCount >= allowedCities ) {
			$('premium-city').getParent('.row').getPrevious('span.error').set('html', Cubix.SelfCheckoutWizard.selectProductError);
			return;
		}
		
		var cont = new Element('div', {
			id: 'prem-city-' +  $('premium-city').get('value'),
			'class': 'prem-city-row'
		});
		var removeBtn = new Element('span', {'class': 'remove-city'}).inject(cont);
		new Element('span', {'class': 'city-name', 'html': $('premium-city').getSelected().get('html')[0]}).inject(cont);
		new Element('input', {
			type: 'hidden',
			name: 'premium_cities[]',
			value: $('premium-city').get('value'),
			'class': 'premium-cities'
		}).inject(cont);
		
		removeBtn.addEvent('click', function() {
			this.getParent('div').destroy();
		});
		
		$('premium-city').set('value', '');
		
		cont.inject($$('.selected-cities')[0]);
		Cubix.SelfCheckoutWizard.updateMooniform();
	});
	
	$$('.self-checkout-wizard-wrapper form').set('send', {
		method: 'post',
		onSuccess: function(response) {
			response = JSON.decode(response);
						
			$$('.self-checkout-wizard-wrapper span.error').set('html', '');
			if ( response.status == 'error' ) {
				for(name in response.msgs) {
					$$('.self-checkout-wizard-wrapper [name="' + name + '"]')[0].getParent('.row').getPrevious('span.error').set('html', response.msgs[name]);
				}
			} else {
				if ( Cubix.SelfCheckoutWizard.userType == 'escort' ) {
					if (response.form) {
						var formHtml = response.form;
						$$('.overlay')[0].set('html', formHtml.replace(/ \"/g, "\""));
						$('payment-form').submit();
						// $('twispay-payment-form').getElement('input[type="submit"]').fireEvent('click');
						return;
					}
					else if(response.checkoutId) {
						Cubix.SelfCheckoutWizard.ecardonPopup(response.checkoutId);
					}
					else if(response.iframe){
						Cubix.PowerCash.init(response.start_url);
					}
					else{
						window.location = response.url;return;
					}
				} else {
					Cubix.SelfCheckoutWizard.addToCart(response.cart_id);
				}
			}
			
			Cubix.SelfCheckoutWizard.loader.enable();
		}
	});
	
	$$('.checkout-btn').addEvent('click', function() {
		if ($$("#styled-checkbox-2:checked").length == 0){
		    return alert(headerVars.confirm_18_warning);
    	}
		if ($$("#styled-checkbox-3:checked").length == 0){
		    return alert(headerVars.confirm_terms_warning);
    	}
    	
		Cubix.SelfCheckoutWizard.Send();
	});
	
	
	/*-->only for agency*/
	
	if ( $('escort') ) {
		$('escort').addEvent('change', function() {
			var optContainer = $$('.self-checkout-wizard-wrapper .optional-products')[0];
			optContainer.empty();
			$('packageExpirationDate').set('value', '');
			$('package').empty().setProperty('disabled', 'disabled');
			$('premium-city').set('value', '').empty().setProperty('disabled', 'disabled');
			$$('.credit-price')[0].set('html', '0.00');
			$$('.crypto-price')[0].set('html', '0.00');
			new Element('option', {
				value: '',
				html: '---'
			}).inject($('package'));
			
			new Element('option', {
				value: '',
				html: '---'
			}).inject($('premium-city'));
			
			$$('.optional-products-container').setStyle('display', 'none');
			$$('.selected-cities')[0].empty();
			$$('.self-checkout-wizard-wrapper span.error').set('html', '');
			
			
			if ( ! this.get('value') ) {
				Cubix.SelfCheckoutWizard.updateMooniform();
				return;
			}
			
			Cubix.SelfCheckoutWizard.loader.disable();
			var escortId = this.get('value');
			new Request.JSON({
				url: 'online-billing-v2/get-escort-data?escort_id=' + escortId,
				method: 'get',
				onSuccess: function(resp) {

					if(resp.packages instanceof Array == false){
						resp.packages = Object.values(resp.packages);
					}
				

					resp.packages.each(function(pr, i) {
						if(pr.id != 107 ){
							new Element('option', {
								value: pr.id + '-' + pr.price + '-' + pr.crypto_price,
								html: pr.name
							}).inject($('package'));
						}
					});
					$('package').removeProperty('disabled');
					
					if(resp.escort_packages.length){
						$('packageExpirationDate').set('value', resp.escort_packages[0]['expiration_date']);
					}
					$('package').removeProperty('disabled');
					
					Cubix.SelfCheckoutWizard.updateMooniform();
				Cubix.SelfCheckoutWizard.loader.enable();
				}
			}).send();
		});
	}
	
	if ( $('add-to-cart') ) {
		$('add-to-cart').addEvent('click', function() {
			Cubix.SelfCheckoutWizard.Send();
		});
	}
	
	$$('.checkout-btn-small').addEvent('click', function() {

		if ($$("#styled-checkbox-2:checked").length == 0){
			    return alert(headerVars.confirm_18_warning);
        }
		if ($$("#styled-checkbox-3:checked").length == 0){
			    return alert(headerVars.confirm_terms_warning);
        }

		Cubix.SelfCheckoutWizard.loader.disable();
		var 
			saveInfo = '',
			card = '';
		if ($('#mmg-cards')) {
			saveInfo = ( ($$('#mmg-cards input[name="save_info"]') && $$('#mmg-cards input[name="save_info"]')[0].checked) ? '&save_info=1' : '');
			card = ( $$('#mmg-cards input[name="card"]').length ? '&card=' + $$('#mmg-cards input[name="card"]:checked')[0].get('value') : '');
		}


		var url = 'online-billing-v2/checkout?payment_gateway=' + $('payment-gateway').get('value') + saveInfo + card;
			
		new Request.JSON({
			url: url,
			method: 'get',
			onSuccess: function(response) {
				if ( response.status == 'error' ) {
					$$('.self-checkout-wizard-wrapper .error.cart')[0].set('html', response.msgs['cart']);
					$$('.self-checkout-wizard-wrapper .error.amount')[0].set('html', response.msgs['amount']);
				} else if (response.form) {

					var formHtml = response.form;
					$$('.overlay')[0].set('html', formHtml.replace(/ \"/g, "\""));
					$('payment-form').submit();
					return;
				}
				else if (response.checkoutId) {
					Cubix.SelfCheckoutWizard.ecardonPopup(response.checkoutId);
				}
				else if(response.iframe){
					Cubix.PowerCash.init(response.start_url);
				}
				else{
					window.location = response.url;return;
				}
				
				Cubix.SelfCheckoutWizard.loader.enable();
				
			}
		}).send();
	});
	/*<--only for agency*/
	
	if ( $('mmg-cards') ) {
		var mmgSlide = new Fx.Slide($('mmg-cards')).hide();
	}
	
	$$('.payment-option').addEvent('click', function() {
		$$('.payment-option').removeClass('checked');
		this.addClass('checked');
		$('payment-gateway').set('value', this.get('data-method'));

		/*if ( $('mmg-cards') ) {
			if ( this.get('data-method') == 'mmgbill' ) {
				mmgSlide.slideIn();
			} else {
				mmgSlide.slideOut();
			}
		}*/
		
		if ( this.get('data-method') == 'coinsome' ) {
			Cubix.SelfCheckoutWizard.cryptoPrice = true;
			$$('.btc-notify-text').show();
			$$('.crypto-price, .total-crypto-price').removeClass('none');
			$$('.credit-price, .total-price').addClass('none');
		}
		else{
			Cubix.SelfCheckoutWizard.cryptoPrice = false;
			$$('.btc-notify-text').hide();
			$$('.crypto-price, .total-crypto-price').addClass('none');
			$$('.credit-price, .total-price').removeClass('none');
		}
		
		Cubix.SelfCheckoutWizard.UpdatePrice();
	});
	
	if ( $$('#mmg-cards .forget-card') ) {
		$$('#mmg-cards .forget-card').addEvent('click', function() {
			var self = this;
			Cubix.SelfCheckoutWizard.loader.disable();
			new Request({
				url: 'online-billing-v2/forget-credit-card?card=' + this.getParent('.card-row').get('card-id'),
				method: 'GET',
				onSuccess: function(response) {
					self.getParent('li').destroy();
					if ( $$('#mmg-cards input[name="card"]').length == 1 ) {
						$$('#mmg-cards input[name="card"]').set('checked', 'checked');
						Cubix.SelfCheckoutWizard.updateMooniform();
					}
					Cubix.SelfCheckoutWizard.loader.enable();
				}
			}).send();
		});
	}
	
	if ( $$('#mmg-cards input[name="card"]').length ) {
		$$('#mmg-cards input[name="card"]').addEvent('change', function() {
			if ( ! this.get('value') ) {
				$$('#mmg-cards input[name="save_info"]').set('disabled', '');
			} else {
				$$('#mmg-cards input[name="save_info"]').set('checked', '');
				$$('#mmg-cards input[name="save_info"]').set('disabled', 'disabled');
			}
			Cubix.SelfCheckoutWizard.updateMooniform();
		});
	}
	
	if ( $$('#mmg-cards .set-default') ) {
		$$('#mmg-cards .set-default').addEvent('click', function() {
			var self = this;
			Cubix.SelfCheckoutWizard.loader.disable();
			new Request({
				url: 'online-billing-v2/set-default-card?card=' + this.getParent('.card-row').get('card-id'),
				method: 'GET',
				onSuccess: function(response) {
					$$('#mmg-cards .set-default').setStyle('display', 'inline');
					self.setStyle('display', 'none');
					Cubix.SelfCheckoutWizard.loader.enable();
				}
			}).send();
		});
	}
}

Cubix.SelfCheckoutWizard.initOptProductsEvents = function() {
	$$('.optional-products-container input').addEvent('change', function(el) {
		Cubix.SelfCheckoutWizard.UpdatePrice();
	});
}

Cubix.SelfCheckoutWizard.Send = function() {
	
    
	Cubix.SelfCheckoutWizard.loader.disable();

	$$('.self-checkout-wizard-wrapper form').send();
}

Cubix.SelfCheckoutWizard.UpdatePrice = function()
{
	var packagePriceData = $('package').getSelected()[0].get('value').split('-');
	if(packagePriceData.length > 1){
		
		var price = packagePriceData[1].toInt();;
		var cryptoPrice = packagePriceData[2].toInt();;
		
		$$('.optional-products-container input:checked').each(function(el) {
			if ( Cubix.SelfCheckoutWizard.OptProductsPrices[el.get('value')] ) {
				var optPrice = Cubix.SelfCheckoutWizard.OptProductsPrices[el.get('value')].toInt();
				price += optPrice;
				cryptoPrice += optPrice;
			}
		});

		$$('.self-checkout-wizard-wrapper .price-block .credit-price').set('html', price.toFixed(2));
		$$('.self-checkout-wizard-wrapper .price-block .crypto-price').set('html', cryptoPrice.toFixed(2));
		$$('.self-checkout-wizard-wrapper .price-block').highlight('#f8e7e7');
	}
}

Cubix.SelfCheckoutWizard.addToCart = function(cartId)
{
	var showname = $('escort').getSelected()[0].get('html'),
		packageName = $('package').getSelected()[0].get('html'),
		cost = $$('.self-checkout-wizard-wrapper .price-block .credit-price')[0].get('html'),
		cryptoCost = $$('.self-checkout-wizard-wrapper .price-block .crypto-price')[0].get('html'),
		premCities = $$('.city-name').get('html');
		
	
	tr = new Element('tr');
	var removeBtn = new Element('a', {'class' : 'remove', 'rel' : cartId});
	
	if(Cubix.SelfCheckoutWizard.cryptoPrice){
		var hidePrice = 'none', hideCrypto = '';
	}
	else{
		var hidePrice = '', hideCrypto = 'none';
	}
	removeBtn.inject(new Element('td').inject(tr));
	new Element('td', {html: showname}).inject(tr);
	new Element('td', {html: packageName}).inject(tr);
	new Element('td', {html: premCities.join(', ')}).inject(tr);
	new Element('td', {html: cost, 'class': 'last credit-price ' + hidePrice}).inject(tr);
	new Element('td', {html: cryptoCost, 'class': 'last crypto-price ' + hideCrypto }).inject(tr);
	
	tr.inject($$('.self-checkout-wizard-wrapper table tbody')[0])
	
	
	totalPrice = $$('.total-price')[0].get('html').toInt() + cost.toInt();
	totalCryptoPrice = $$('.total-crypto-price')[0].get('html').toInt() + cryptoCost.toInt();
	
	$$('.total-price')[0].set('html', totalPrice.toFixed(2));
	$$('.total-crypto-price')[0].set('html', totalCryptoPrice.toFixed(2));
	
	$('escort').getSelected().destroy();
	$('escort').set('value', '').fireEvent('change');
	
	removeBtn.addEvent('click', function() {
		var tr = this.getParent('tr');
		price = tr.getElement('td.credit-price').get('html').toInt();
		cryptoPrice = tr.getElement('td.crypto-price').get('html').toInt();
		
		totalPrice = $$('.total-price')[0].get('html').toInt() - price;
		totalCryptoPrice = $$('.total-crypto-price')[0].get('html').toInt() - cryptoPrice;
		Cubix.SelfCheckoutWizard.loader.disable();
		new Request.JSON({
			url: 'online-billing-v2/remove-from-cart?cart_id=' + this.get('rel'),
			method: 'get',
			onSuccess: function(resp) {
				tr.destroy();
				$$('.total-price')[0].set('html', totalPrice.toFixed(2));
				$$('.total-crypto-price')[0].set('html', totalCryptoPrice.toFixed(2));
				Cubix.SelfCheckoutWizard.loader.enable();
				
				new Element('option', {html: resp.escort.showname + ' #' + resp.escort.id, value: resp.escort.id}).inject($('escort'));
			}
		}).send();
	});
}

Cubix.SelfCheckoutWizard.updateMooniform = function()
{
	Cubix.SelfCheckoutWizard.mooniformInstance.lookup($$('input[type="checkbox"], input[type="radio"]'));
	Cubix.SelfCheckoutWizard.mooniformInstance.update();
}

Cubix.SelfCheckoutWizard.ecardonPopup = function(checkoutId) {
	
	$$('.self-checkout-wizard-wrapper').destroy();
		
	var container = new Element('div', {'class': 'ecardon-popup'}).setStyles({
		left: window.getWidth() / 2 - 460 / 2 ,
		top:  window.getHeight() / 2 - 320 / 2,
		opacity: 0,
        position: 'fixed',
        'z-index': 101,
        'font-size' : '14px'
	}).inject(document.body);
	
	//Cubix.SelfCheckoutWizard.inProcess = true;
	$$('.overlay').setStyle('opacity',0.8);
	
	new Request({
		url: 'online-billing-v2/ecardon-popup',
		method: 'get',
		onSuccess: function (resp) {
			//Cubix.SelfCheckoutWizard.inProcess = false;
			container.set('html', resp);
			
			container.setStyle('opacity', 1);
			new Element('script', {
				text: 'var wpwlOptions = { locale: "' + Cubix.Lang.id + '", style: "plain", onBeforeSubmitCard: function(){ if ( $$(".wpwl-control-cardHolder").get("value") == "") { $$(".wpwl-control-cardHolder").addClass("wpwl-has-error"); $$(".wpwl-button-pay").addClass("wpwl-button-error").set("disabled", "disabled"); new Element("div", {"class": "wpwl-hint wpwl-hint-cardHolderError", "text": "card holder required"}).inject($$(".wpwl-control-cardHolder")[0], "after");  return false} else return true}}'
			}).inject(container, 'before');
			
			new Element('script', {
				src: "https://oppwa.com/v1/paymentWidgets.js?checkoutId=" + checkoutId
			}).inject(container, 'before');
			
			//Cubix.SelfCheckoutWizard.loader = new Cubix.Overlay(container, {loader: _st('loader-small.gif'), position: '50%', no_relative: true});
		}
	}).send();
	
	$$('.overlay').addEvent('click', function(){
		container.destroy();
		Cubix.SelfCheckoutWizard.overlay.enable();
	});
	
	
}