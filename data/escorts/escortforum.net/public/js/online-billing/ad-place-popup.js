Cubix.AdPlacePopup = {};

Cubix.AdPlacePopup.inProcess = false;
Cubix.AdPlacePopup.url = '';

Cubix.AdPlacePopup.Show = function (box_height, box_width, el) {
	if ( Cubix.AdPlacePopup.inProcess ) return false;

	var page_overlay = new Cubix.Overlay(document.body, { has_loader: false, opacity: 0.5, color: '#fff' });
	page_overlay.disable();

	var y_offset = -30;
	var x_offset = 100;

	var container = new Element('div', { 'class': 'online-billing-popup-wrapper'}).setStyles({
		left: window.getWidth() / 2 - box_width / 2,
		//top:  window.getHeight() / 2 - box_height / 2 + window.getScroll().y - y_offset,
		top:  el.getPosition().y + y_offset,
		opacity: 0,
        position: 'absolute',
        'z-index': 101,
        //background: '#fff',
		'box-shadow': 'none'
	}).inject(document.body);
	
	Cubix.AdPlacePopup.inProcess = true;

	new Request({
		url: Cubix.AdPlacePopup.url + '?package_id=' + el.get('rel'),
		method: 'get',
		onSuccess: function (resp) {
			
			Cubix.AdPlacePopup.inProcess = false;
			container.set('html', resp);

			var close_btn = new Element('div', {
				html: '',
				'class': 'online-billing-popup-close-btn'
			}).setStyle('right', 25).inject(container);

			close_btn.addEvent('click', function() {
				$$('.online-billing-popup-wrapper').destroy();
				page_overlay.enable();
			});

			container.tween('opacity', '1');
		}
	}).send();		
	
	return false;
}