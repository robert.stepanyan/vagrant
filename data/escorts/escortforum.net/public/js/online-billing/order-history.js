Cubix.OrderHistory = {};

Cubix.OrderHistory.url = ''; // Must be set from php
Cubix.OrderHistory.container = '';

Cubix.OrderHistory.initOrdering = function() {

	if ( $$('.ord').length > 0 ) {
		$$('.ord').addEvent('click', function() {

			var rel = this.get('rel');
			var page = $$('input[name=page]')[0].get('value');

			var arr = new Array();

			arr = rel.split('|');

			var ord_field = arr[0];
			var ord_dir = '';

			if (arr[1] == undefined)
				ord_dir = 'desc';
			else
				ord_dir = arr[1];

			if ( ord_dir == 'asc' ) {
				ord_dir = 'desc';
			}
			else {
				ord_dir = 'asc';
			}	

			var url = Cubix.OrderHistory.url + '?ord_field=' + ord_field + '&ord_dir=' + ord_dir + '&ajax=1&page=' + page;

			Cubix.OrderHistory.doSortRequest(url);
		});
	}
};

Cubix.OrderHistory.doSortRequest = function(url) {

	var overlay = new Cubix.Overlay($(Cubix.OrderHistory.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});

	overlay.disable();

	var req = new Request({
		method: 'get',
		url: url,
		onComplete: function(response) {
			$(Cubix.OrderHistory.container).set('html', response);
			
			Cubix.OrderHistory.initOrdering();
			
			overlay.enable();
		}
	}).send();
};

Cubix.OrderHistory.Load = function (el) {
	var url = el.get('href');
	url = url + '&ajax=1';
	
	Cubix.OrderHistory.Show(url);
	
	return false;
}

Cubix.OrderHistory.Show = function (url) {
	
	var overlay = new Cubix.Overlay($(Cubix.OrderHistory.container), {
		loader: _st('loader-small.gif'),
		position: '50%'
	});
	
	overlay.disable();
	
	new Request({
		url: url,
		method: 'get',
		onSuccess: function (resp) {
			$(Cubix.OrderHistory.container).set('html', resp);
			overlay.enable();
			
			Cubix.OrderHistory.initOrdering();
		}
	}).send();
}