Cubix.XmasOffer = {};

Cubix.XmasOffer.inProcess = false;

Cubix.XmasOffer.url = '';

Cubix.XmasOffer.Show = function () {
	if (Cubix.XmasOffer.inProcess) return false;

	var page_overlay = new Cubix.Overlay(document.body, {has_loader: false, opacity: 0.5, color: '#000', z_index: 200});
	page_overlay.disable();

	$$('#XmasOfferPopup').addClass('open');

	Cubix.XmasOffer.inProcess = true;

	$$('#xmas-offer-cancel-btn').addEvent('click', function() {
		$$('#XmasOfferPopup').destroy();
		page_overlay.enable();
	});

	return false;
}