window.addEvent('domready', function () {

  function InstantCountDown(params) {
    return this.init(params);
  }

  InstantCountDown.prototype = {
    init: function (params) {
      this.params = params || {};

      this.build();
    },

    build: function () {
      if (!this.params.$el) return false;

      var
        _hours = this.params.timer.h || 0,
        _minutes = this.params.timer.m || 0,
        _seconds = this.params.timer.s || 0;

      this.timeInMs = (_hours * 60 * 60 * 1000) + (_minutes * 60 * 1000) + (_seconds * 1000);
    },

    start: function () {
      var _self = this;

      this.countDownTimer = setInterval(function () {
        if (_self.timeInMs <= 0) {
          _self.params.timeUpCallback && _self.params.timeUpCallback();
          _self.stop();
        }
        _self.timeInMs -= 1000;

        var html = _self._decorateHtml();

        _self._draw(html);
      }, 1000);
    },

    stop: function () {
      clearInterval(this.countDownTimer);
    },

    _decorateHtml: function () {
      var _hours = 0,
        _minutes = 0,
        _seconds = 0,
        _rem = this.timeInMs;
      _hours = Math.floor(_rem / (60 * 60 * 1000));

      if (_hours) _rem = _rem % (60 * 60 * 1000);
      _minutes = Math.floor(_rem / (60 * 1000));

      if (_minutes) _rem = _rem % (60 * 1000);
      _seconds = Math.floor(_rem / (1000));

      var html = '';

      if (_hours) {
        html += '<span class="Countdown__hours">' + ('0' + _hours).slice(-2) + '</span>';
      }

      if (_hours) {
        html += ':';
      }

      html += '<span class="Countdown__minutes">' + ('0' + _minutes).slice(-2) + '</span>';
      html += ':';
      html += '<span class="Countdown__seconds">' + ('0' + _seconds).slice(-2) + '</span>';

      return html;
    },

    _draw: function (html) {
      this.params.$el.set('html', html);
    }
  };

  function InstantModal(params) {
    /**
     * params should contain { header, body }
     */
    return this.init(params);
  };
  InstantModal.prototype = {
    init: function (params) {
      this.params = params || {
        header: new Element('div'),
        body: new Element('div')
      };

      this.build().bindEvents();

      this.html.show = this._show;
      this.html.hide = this._hide;

      return this.html;
    },

    _buildOverlay: function () {
      return new Element('div', {
        id: this.params.id || 'BookingRequest',
        styles: {
          width: '100%',
          height: '100%',
          position: 'fixed',
          backgroundColor: 'rgba(0,0,0,0.2)',
          top: 0,
          left: 0,
          zIndex: 9999
        }
      });
    },

    _buildModal: function () {
      return Element('div', {
        class: 'Modal'
      });
    },

    _buildCloseBtn: function () {
      return Element('div', {
        class: 'instant-popup-close-btn',
        html: '&times;'
      });
    },

    build: function () {
      var Overlay = this._buildOverlay(),
        Modal = this._buildModal();

      if (this.params.close) {
        this.closeBtn = this._buildCloseBtn();
        Modal.adopt(this.closeBtn);
      }

      Modal.adopt(this.params.header);
      Modal.adopt(this.params.body);
      Overlay.adopt(Modal);

      this.Overlay = Overlay;

      $(document.body).adopt(Overlay);

      return this;
    },

    bindEvents: function () {
      var _self = this;

      if (this.closeBtn) {
        this.closeBtn.addEvent('click', function () {
          _self.Overlay.destroy();
          var instantBookBtn = $$('.instant-book-btn');
          var dashIBBlock = $$('.DashboardItem--instant-book');
          var modals = $$('.Modal');

          if (dashIBBlock.length && !modals.length) {
            window.location.reload();
          }

          if (instantBookBtn) {
            instantBookBtn.removeClass('is-loading');
          }
        });
      }

      return this;
    },

    _show: function () {
      this.getElement('.Modal').addClass('shown');
    },

    _hide: function () {
      this.getElement('.Modal').removeClass('shown');
    },

    get html() {
      return this.Overlay;
    }
  };

  !window.InstantCountDown && (window.InstantCountDown = InstantCountDown);
  !window.InstantModal && (window.InstantModal = InstantModal);

  var InstantPrivateProfile = function (params) {
    return this.init(params);
  };


  InstantPrivateProfile.prototype = {
    init: function (params) {
      this.params = params || {};

      this
        .build()
        .bindEvents();
    },

    build: function () {
      window.instantSocket.emit('get_availability', {
        escortId: headerVars.currentUser.escort_data.escort_id.toString(),
      });

      return this;
    },
    bindEvents: function () {
      var
        _self = this,
        $actionBtn = this.params.createForm.getElement('.instant-booking-btn'),
        $cancelBtn = this.params.editFrom.getElement('.cancel'),
        $changeBtn = this.params.editFrom.getElement('.change');

      $actionBtn.addEvent('click', function (e) {
        e.stop();

        // var OneSignal = window.OneSignal || [];
        //
        // OneSignal.push(function () {
        //   var isSupported = OneSignal.isPushNotificationsSupported();
        //
        //   if (isSupported) {
        //     OneSignal.init({
        //       appId: "18971079-7fed-450f-85cf-498b905d15d6",
        //       autoRegister: true
        //     });
        //
        //     OneSignal.on('subscriptionChange', function (isSubscribed) {
        //       if (isSubscribed) {
        //         OneSignal.sendTag('escortId', headerVars.currentUser.escort_data.escort_id);
        //       }
        //     });
        //   }
        // });

        var
          _duration = _self.params.createForm.getElement('[name="duration"]').getSelected().get('value')[0],
          _location = [];
        _self.params.createForm.getElements('[name="location"]:checked').each(function (el) {
          _location.push(el.get('value'))
        });

        window.instantSocket.emit('available_for_booking', {
          escortId: headerVars.currentUser.escort_data.escort_id,
          duration: _duration,
          location: _location
        });
      });

      $cancelBtn.addEvent('click', function (e) {
        e.stop();
        $actionBtn.removeClass('is-loading');
        window.instantSocket.emit('no_longer_available_for_booking', {
          escortId: headerVars.currentUser.escort_data.escort_id
        });
      });

      $changeBtn.addEvent('click', function (e) {
        e.stop();
        $actionBtn.removeClass('is-loading');

        _self.params.editFrom.hide();
        _self.params.createForm.show();
        $actionBtn.removeEvents('click');
        $actionBtn.addEvent('click', function (e) {
          e.stop();

          var
            _duration = _self.params.createForm.getElement('[name="duration"]').getSelected().get('value')[0],
            _location = _self.params.createForm.getElement('[name="location"]:checked').get('value');

          window.instantSocket.emit('available_for_booking_update', {
            escortId: headerVars.currentUser.escort_data.escort_id.toString(),
            duration: _duration,
            location: _location === 'outcall' ? 'outcall' : 'incall'
          });
        });


      });

      return this;
    }
  }

  var InstantBooking = function (params) {
    return this.init(params);
  };

  InstantBooking.prototype = {
    userType: '',
    escortId: '',
    token: '',

    init: function (params) {
      var _self = this;
      this
        .auth(params)
      _self
        .connect()
        .bindEvents()
        .expose();
    },

    auth: function (params) {
      if (!params._user || params._user.user_type !== 'escort') {
        this.userType = 'guest';
      } else {
        this.userType = 'escort';
        this.escortId = params._user.escort_data.escort_id.toString();
      }

      this.token = params._sessionId;

      return this;
    },

    connect: function () {
      this.socket = io('/booking', {
        transports: ['websocket'],
        query: {
          uType: this.userType,
          // escort id if the user is logged in as escort
          eId: this.escortId,
          t: this.token
        }
      });

      return this;
    },

    _getAvailabilityMsg: function (data) {

      var clientEndDate = new Date(data.end_date);
      var mins = Math.floor((clientEndDate.getTime() - new Date().getTime()) / (60 * 1000));
      var time = ('0' + clientEndDate.getHours()).slice(-2) + ':' + ('0' + clientEndDate.getMinutes()).slice(-2);
      var text = 'You are available for instant bookings, for ' +
        (data.incall ? 'incall' : '') + (data.incall && data.outcall ? ' and ' : '') + (data.outcall ? 'outcall, ' : ', ') +
        'for <span class="BannerCountDownTimer">' + mins + '</span> minutes (until ' + time + ')';

      return text;
    },

    bindEvents: function () {
      var _self = this;
      this.socket.on('escort_availability', function (data) {

        var $availableInstantBookBlock = $('instant-form');
        var $availableInstantBookEditBlock = $('instant-edit-box');

        if (!$availableInstantBookBlock && !availableInstantBookEditBlock) return false;

        if (data) {
          var text = _self._getAvailabilityMsg(data);

          var minuteInterval = setInterval(function () {
            var minutesCount = parseInt($$('.BannerCountDownTimer')[0].get('text'));
            if (minutesCount > 0) {
              $$('.BannerCountDownTimer')[0].set('text', --minutesCount);
            } else {
              clearInterval(minuteInterval);
            }
          }, 60 * 1000);

          $availableInstantBookEditBlock.getElement('.info').set('html', text);

          $availableInstantBookBlock.getElement('.instant-overlay').hide();
          $availableInstantBookBlock.hide();
          $availableInstantBookEditBlock.show();
        } else {
          $availableInstantBookBlock.getElement('.instant-overlay').hide();
        }
      });

      this.socket.on('new_booking', function (data) {

        var header = new Element('h2', {
          class: 'Header',
          'text': instantBookLng.bookingRequestHeader
        });

        var paragraphHtml = '';
        var __bd = new Date(data.booking_date);
        var _bookingDateYMD = [('0' + __bd.getDate()).slice(-2), ('0' + (__bd.getMonth() + 1)).slice(-2), __bd.getFullYear()].join('-');
        var _bookingDateHM = [('0' + __bd.getHours()).slice(-2), ('0' + __bd.getMinutes()).slice(-2)].join(':');
        var _bookingDateHtml = _bookingDateHM +
          ' <span style="color: #fff; font-size: 10px; background-color: #d2d2d2; padding: 0 3px; border-radius: 3px;">' +
          _bookingDateYMD +
          '</span>';

        paragraphHtml += '<p><strong>' + instantBookLng.from + '</strong> ' + _bookingDateHtml + '</p>';
        paragraphHtml += '<p><strong>' + instantBookLng.duration + '</strong> ' + data.duration + ' hour(s)</p>';
        paragraphHtml += '<p><strong>' + instantBookLng.location + '</strong> ' + data.location + '</p>';
        if (data.special_request) {
          paragraphHtml += '<p><strong>' + instantBookLng.specialRequest + '</strong>' + data.special_request + '</p>';
        }

        if (data.location === 'outcall') {
          paragraphHtml += '<p><strong>' + instantBookLng.location + '</strong> ' + data.outcall_address + '</p>';
        }

        paragraphHtml += '<p class=\'none\'><strong>' + instantBookLng.phone + '</strong>' + data.phone + '<p>';
        paragraphHtml += '<p class=\'none\'><strong>' + instantBookLng.contactType + '</strong>' + data.contact_type.join(', ') + '<p>';

        if (data.special_request) {
          paragraphHtml += '<p class=\'none\'><strong>' + instantBookLng.specialRequest + '</strong>' + data.special_request + '</p>';
        }

        var body = new Element('div', {
          class: 'Body'
        }).set('html', paragraphHtml);

        var noLongerAvailableBtn = new Element('a', {
          class: 'btn--no-longer-available',
          href: '#',
          text: instantBookLng.noLonger
        }).addEvent('click', function () {
          instantSocket.emit('booking_request_reply', {
            status: 'deny',
            token: data.hash,
            id: data.id
          });

          instantSocket.emit('no_longer_available_for_booking', {
            escortId: data.escort_id
          });

          var overlay = this.getParent('#BookingRequest');
          overlay.hide();

          setTimeout(function () {
            overlay.destroy();
          }, 500);

          instantSocket.emit('no_longer_available', {});
        });

        var denyBookingRequestBtn = new Element('a', {
          class: 'btn--deny-booking-request',
          href: '#',
          text: instantBookLng.deny
        }).addEvent('click', function () {
          var overlay = this.getParent('#BookingRequest');
          overlay.hide();

          setTimeout(function () {
            overlay.destroy();
          }, 500);

          instantSocket.emit('booking_request_reply', {
            status: 'deny',
            token: data.hash,
            id: data.id
          });
        });

        var acceptButton = new Element('a', {
          class: 'btn--accept',
          href: '#',
          text: instantBookLng.accept
        }).addEvent('click', function () {
          var Body = this.getParent('.Body');
          Body.getElement('.btn--no-longer-available').destroy();
          Body.getElement('.btn--deny-booking-request').destroy();

          Body.getElements('.none').each(function ($el) {
            $el.removeClass('none');
          });

          this.set('text', 'Ok');
          this.removeEvents().addEvent('click', function () {
            var overlay = this.getParent('#BookingRequest');
            overlay.hide();

            setTimeout(function () {
              overlay.destroy();
            }, 500);
          });

          instantSocket.emit('booking_request_reply', {
            status: 'accept',
            token: data.hash,
            id: data.id
          });
        });
        body.adopt(noLongerAvailableBtn);
        body.adopt(denyBookingRequestBtn);
        body.adopt(acceptButton);

        new InstantModal({
          header: header,
          body: body,
          close: true
        }).show();
      });

      this.socket.on('request_reply_guest', function (data) {
        // Popup shown

        var $bookingPopup = $('InstantBook');

        if ($bookingPopup) {
          $bookingPopup.getElement('.instant-popup-close-btn').fireEvent('click');
        }
        Cookie.dispose('ib_request_made');
        if (data.status === 'deny') {

          var header = new Element('h2', {
            class: 'Header',
              text: instantBookLng.oops
          })

          var paragraphHtml = instantBookLng.requestDenied + '<br>';

          var body = new Element('div', {
            class: 'Body'
          }).set('html', paragraphHtml);

          var acceptButton = new Element('a', {
            class: 'btn--accept',
            href: '#',
            text: 'Ok'
          }).addEvent('click', function () {
            var overlay = this.getParent('#BookingRequest');
            overlay.hide();

            setTimeout(function () {
              overlay.destroy();
            }, 1000);
          });

          body.adopt(acceptButton);

        } else if (data.status === 'accept') {
          var header = new Element('h2', {
            class: 'Header',
            text: instantBookLng.success
          });

          console.log(data);

          var paragraphHtml = instantBookLng.bookingConfirmedMsg.replace('%SHOWNAME%', data.showName) + '<br>';

          var body = new Element('div', {
            class: 'Body'
          }).set('html', paragraphHtml);

          var acceptButton = new Element('a', {
            class: 'btn--accept',
            href: '#',
            text: 'Ok'
          }).addEvent('click', function () {
            var overlay = this.getParent('#BookingRequest');
            overlay.hide();

            setTimeout(function () {
              overlay.remove();
            }, 1000);
          });

          body.adopt(acceptButton);
        } else if (data.status === 'no_answer') {
          var header = new Element('h2', {
            class: 'Header',
              text: instantBookLng.oops
          }).set('text', 'Oops!');

          var paragraphHtml = instantBookLng.requestNoAnswer + '<br>';

          var body = new Element('div', {
            class: 'Body'
          }).set('html', paragraphHtml);

          var acceptButton = new Element('a', {
            class: 'btn--accept',
            href: '#',
            text: 'Ok'
          }).addEvent('click', function () {
            var overlay = this.getParent('#BookingRequest');
            overlay.hide();

            setTimeout(function () {
              overlay.destroy();
            }, 1000);
          });

          body.adopt(acceptButton);
        }

        new InstantModal({
          header: header,
          body: body
        }).show();

      });

      var instantActionDeny = document.getElements('.booking-action-deny');
      var instantActionUnavailable = document.getElements('.booking-action-unavailable');
      var instantActionAccept = document.getElements('.booking-action-accept');

      if (instantActionDeny && instantActionUnavailable && instantActionAccept) {
        instantActionDeny.addEvent('click', function () {
          instantSocket.emit('booking_request_reply', {
            status: 'deny',
            token: this.get('data-hash'),
            id: this.get('data-id')
          });
          this.getParent('.booking-requests').fade('out');

          var myFx = new Fx.Slide(this.getParent('.booking-requests'), {
            duration: 'long',
            transition: Fx.Transitions.Bounce.easeOut
          });
          myFx.slideOut();

        });

        instantActionUnavailable.addEvent('click', function () {
          window.instantSocket.emit('no_longer_available_for_booking', {
            escortId: headerVars.currentUser.escort_data.escort_id
          });
          instantActionDeny.fireEvent('click');
        });

        instantActionAccept.addEvent('click', function () {
          instantSocket.emit('booking_request_reply', {
            status: 'accept',
            token: this.get('data-hash'),
            id: this.get('data-id')
          });
          location.reload();
        });
      }
      this.socket.on('request_booking_error', function (data) {
        var $form = $('instant-book-form'),
          $instantBookPopupBtn = $$('.instant-book-popup-btn') ? $$('.instant-book-popup-btn')[0] : null,
          errorsHtml = '';

        if (!$form) return false;

        Object.each(data, function (value, key) {
          errorsHtml += '<p style=\'color: red\'>' + value + '</p>';
        });

        if ($form) {
          $form.getElement('.errors').adopt(Elements.from(errorsHtml));
        }

        if ($instantBookPopupBtn) {
          $instantBookPopupBtn.removeClass('is-loading');
        }
      });

      this.socket.on('instant_request_success', function (data) {
        var
          $popup = $('InstantBook'),
          count = data.count * 60 * 1000,
          futureTime = new Date().getTime() + count;

        if (!$popup) return false;

        document.cookie = "ib_request_made=1; expires=" + new Date(futureTime).toUTCString() + "; path=/";
        $popup
          .getElement('form')
          .hide();
        $('countdown').show();

        var countDown = setInterval(function () {
          if (!$$('.countDownTimer')[0]) {
            clearInterval(countDown);
          }
          var timeLeft = futureTime - new Date().getTime(),
            minutes = Math.floor(timeLeft / 60000),
            seconds = Math.floor((timeLeft % 60000) / 1000);

          $$('.countDownTimer')[0].set('text', ('0' + minutes).slice(-2) + ':' + ('0' + seconds).slice(-2));
          if (!minutes && !seconds) {
            clearInterval(countDown);

            var $bookingPopup = $('InstantBook');

            if ($bookingPopup) {
              $bookingPopup.getElement('.instant-popup-close-btn').fireEvent('click');
            }
          }
        }, 1000);
      });

      this.socket.on('available_for_booking_error', function () {
        alert('Something went wrong.');
      });

      this.socket.on('available_for_booking_success', function (data) {

        var $availableInstantBookBlock = $('instant-form');
        var $availableInstantBookEditBlock = $('instant-edit-box');

        if (!$availableInstantBookBlock && !$availableInstantBookEditBlock) return false;

        if (data) {
          var text = _self._getAvailabilityMsg(data);
          $availableInstantBookEditBlock.getElement('.info').set('html', text);

          $availableInstantBookBlock.hide();
          $availableInstantBookEditBlock.show();
        }

        if (data.is_new == 1) {

          var isSuppoted = OneSignal.isPushNotificationsSupported();
          if (isSuppoted) {
            var header = new Element('h2', {
              class: 'Header',
              text: 'SMS Notifications'
            });

            var paragraphHtml = instantBookLng.pushNotifMessage.replace('%number%', $$('[data-phone-number]').get('value'));

            var body = new Element('div', {
              class: 'Body',
              html: paragraphHtml
            });

            var applyButton = new Element('a', {
              class: 'btn--apply',
              href: '#',
              text: instantBookLng.receiveSMS
            }).addEvent('click', function () {
              window.instantSocket.emit('receive_sms');

              var overlay = this.getParents('#BookingRequest')[0];
              overlay.hide();

              setTimeout(function () {
                overlay.destroy();
              }, 1000);
            });

            var cancelButton = new Element('a', {
              class: 'btn--cancel',
              href: '#',
              text: instantBookLng.dontWant
            }).addEvent('click', function () {
              window.instantSocket.emit('do_not_receive_sms');

              var overlay = this.getParents('#BookingRequest');
              overlay.hide();

              setTimeout(function () {
                overlay.destroy();
              }, 1000);
            });

            body.adopt(applyButton);
            body.adopt(cancelButton);

            new InstantModal({
              header: header,
              body: body
            }).show();
          }
        }
      });

      this.socket.on('no_longer_available_for_booking_success', function () {
        var $availableInstantBookBlock = $('instant-form');
        var $availableInstantBookEditBlock = $('instant-edit-box');
        if (!$availableInstantBookBlock && !availableInstantBookEditBlock) return false;

        $availableInstantBookBlock.show();
        $availableInstantBookEditBlock.hide();
      });

      return this;

    },

    expose: function () {
      !window.instantSocket && (window.instantSocket = this.socket);
    }
  };

  window.addEvent('load', function () {
    setTimeout(function () {
      new InstantBooking({
        _user: headerVars.currentUser,
        _sessionId: headerVars.sessionId
      });

      var
        $availableInstantBookBlock = $('instant-form'),
        $availableInstantBookEditBlock = $('instant-edit-box');

      if ($availableInstantBookBlock) {
        new InstantPrivateProfile({
          createForm: $availableInstantBookBlock,
          editFrom: $availableInstantBookEditBlock
        });
      }

      var $countDownTimers = $$('[data-role="countdowntimer"]');
      if ($countDownTimers && $countDownTimers.length) {
        $countDownTimers.each(function ($el) {
          var timerData = {};
          var htmlTimeData = $el.get('html').split(':');
          switch (htmlTimeData.length) {
            case 3:
              timerData = {
                h: parseInt(htmlTimeData[0]),
                m: parseInt(htmlTimeData[1]),
                s: parseInt(htmlTimeData[2])
              };
              break;
            case 2:
              timerData = {
                h: 0,
                m: parseInt(htmlTimeData[0]),
                s: parseInt(htmlTimeData[1])
              };
              break;

          }
          var timer = new InstantCountDown({
            $el: $el,
            timer: timerData,
            timeUpCallback: function () {
              var $parent = $el.getParent('.booking-requests');
              $parent.fade('out');
              setTimeout(function () {
                $parent.destroy();
              }, 500);
            }
          });

          timer.start();
        });
      }

    }, 1000);
  });
});