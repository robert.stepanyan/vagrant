window.addEvent('domready', function() {
	ClickVideo();
	Hover();
	Remove();
	AgencyUpload();
	WaitUpload();
	PlayVideo();

});

var ClickVideo = function()
{
	$$('.video>li').addEvent('click',function(){
		var img = $(this).getChildren('img');
		var width = img.get('owidth')[0];
		var height = img.get('oheight')[0];
		var video = img.get('rel');
		var image = img.get('src')[0].replace('m320','orig');
		video = $('edit_video').get('config')+video;
		Play(video,image,width,height,true);
	});
};
var Play = function(escortId, video,image,width,height,auto, source,from_sedcard) {

    var primary = "HTML5";
    if (Browser.Plugins.Flash && Browser.Plugins.Flash.version >= 9) {
        primary = "flash";
    }
	/*else{
	 var videoFile = video + "_360p.mp4";
	 source = [{file: videoFile}];
	 width = width / height * 360;
	 height = 360;
	 auto = false;
	 }*/


    if (from_sedcard) {

    	height = parseInt(height);
    	width = parseInt(width);

        var start_width = 410;
        var start_height = 310;

        if (height > width) {
            start_height = 600;
        }

        jwplayer('video-modal').setup({
            primary: primary,
            flashplayer: '/jwplayer.flash.swf',
            height: start_height,
            width: start_width,
            image: image,
            autostart: auto,
            logo: {
                file: "/img/video/logo.png"
            },
            skin: {
                name: "bekle"
            },
            sources: source,
			/*provider: 'rtmp',
			 file:'rtmp://'+video,
			 streamer:'rtmp://'+video*/
            events: {
                onReady: function () {

                },
                onQualityChange: function () {
                    var index = playerInstance.getCurrentQuality();
                    if (index != 0) {
                        var qualityList = playerInstance.getQualityLevels();

                        var videoInfo = qualityList[index];
                        playerInstance.resize(start_width, start_height);
                    }
                }
            }
        });
        $$('.video_overlay').destroy();
    } else {
		// for open videos in modal from main_page
        var vm = new VideoModal({
            "hideFooter": true,
            'offsetTop': '20%',
            'width': width,
            'height': height,
            'onAppend': function () {
                jwplayer('video-modal').setup({
                    primary: primary,
                    flashplayer: '/jwplayer.flash.swf',
                    height: height,
                    width: width,
                    image: image,
                    autostart: auto,
                    logo: {
                        file: "/img/video/logo.png"
                    },
                    skin: {
                        name: "bekle"
                    },
                    sources: source,
					/*provider: 'rtmp',
					 file:'rtmp://'+video,
					 streamer:'rtmp://'+video*/
                    events: {
                        onReady: function () {

                        },
                        onQualityChange: function () {
                            var index = playerInstance.getCurrentQuality();
                            if (index != 0) {
                                var qualityList = playerInstance.getQualityLevels();

                                var videoInfo = qualityList[index];
                                playerInstance.resize(videoInfo.width, videoInfo.height);
                                ;
                                $('simple-modal').setStyles({
                                    width: videoInfo.width + 52,
                                    left: (window.getCoordinates().width - videoInfo.width) / 2

                                });
                            }
                        }
                    }
                });
            }
        });
        vm.addButton("Cancel", "btn");
        vm.show({
            "model":"modal",
            "title":"",
            'contents':'<div id="video-modal"></div>'
        });
    }
}
var Hover = function()
{
	  var interval;
	  if($$('.video'))
	  $$('.video').addEvent(
		   'mouseenter',function()
		   {if($(this).getElement('li'))
				{	
					var id = $(this).getElement('li').get('id');
					if(!interval)
					{				
						interval = setInterval(function(){
						var current = $(id).getParent('ul').getElement('.current');
						if(current)
						{var next = current.getNext('li');
							current.removeAttribute('class')
							var html =next?next.addClass('current').get('html'):$(id).addClass('current').get('html');
							html+='<span class="video_play_btn"></span>';
							$(id).set('html',html);
						}
						},1000);
					}
				}
		   }
	  );
	 $$('.video').addEvent('mouseleave',function(){
		clearInterval(interval);
		interval=null;
	  });
};

var WaitUpload = function()
{	if($('upload') && !$('select_escort'))
	$('upload').addEvent('click',function(){
		$$('.video_main_block').setStyles({'cursor':'wait','opacity':0.4});
		$('video_loader').setStyle('display','block');
		$('up_form').submit();
		$$('#video,#upload').set('disabled','disabled');
	});
}
var Remove = function()
{if($('remove'))
	  $("remove").addEvent("click", function(e){
			var checkbox = $$('.main input:checked');
			if(typeof checkbox[0]!='undefined')
			{	
			var rm = new VideoModal({"btn_ok":"Yes", draggable:true,'offsetTop':'25%'});
			
	          rm.show({
	            "model":"confirm",
				"hideBody":true,
	            "callback": function(){          
				var action = $('edit_video').get('action');
				var data = $('edit_video').toQueryString();
				if($('select_escort'))
					data+='&select_escort='+$('select_escort').getSelected()[0].get('value');
				
				var myRequest = new Request({
						url: action,
						method:'POST',
						data:data,
						onSuccess: function(res){
							if(res && res=='1')
							{	$('edit_video').getElements('input[type="checkbox"]:checked').getParent('li').dispose();
								if(document.getElements(".main li").length==0)
								{	if(!$('edit_video'))
									{
										window.location.href= window.location.href;
									}
									else
									$('edit_video').dispose();
								}
								$$('.video_page')[0].setStyle('display','block');
							}
						}
					});
					myRequest.send();
	            },
	            "contents":"<span class='rm_v'>"+$('trans_rem').get('value')+"</span>"
	          });
			  
			}
			else
			{
				alert($('select_video').get('value'));
			}
      });
};

var AgencyUpload = function()
{
if($('select_escort'))
{	
	$('upload').addEvent('click',function(){
		  var value = $('select_escort').getSelected()[0].get('value');
		  if(!value || value==0)
			{
				$('select_escort').setStyles({'background':'#F00','color':'#fff','border-color':'#F00'});
			}
			else
			{	
				$$('.video_main_block').setStyles({'cursor':'wait','opacity':0.4});
				$('video_loader').setStyle('display','block');
				$('up_form').submit();
				$$('#video,#select_escort,#upload').set('disabled','disabled');
			}
	  });
	  
			$('select_escort').addEvent('change',function(){
				var selected = $(this).getSelected()[0];
				var escort_id =selected.get('value');
				if(escort_id!=0) 
				{
				$('select_escort').setStyles({'background':'#fff','color':'#000','border-color':'#000'});
				var count = selected.get('rel');
				var showname = selected.get('text').split('(');
				showname = showname[0];
				if(count==0)
					{ 
						$('video-content').set('html','<span class="empty_video">'+showname+' '+$('trans_empty').get('value')+'</span>');
						$$('#video,#select_escort,#upload').set('disabled','');
					}
					else
					{	
						var action = $('select_escort').get('rel')
						var getVideo = new Request({
								url: action,
								method:'POST',
								data:{'select_escort':escort_id},
								onSuccess: function(res)
								{	if(res)
									{
										$('video-content').set('html',res);
										$$('.video>li').addEvent('click',ClickVideo);
										interval=null;
										
										$$('.video').addEvent('mouseleave',function(){
											clearInterval(interval);
											interval=null;
										});
										$$('.video').addEvent('mouseenter',Hover);
										$("remove").addEvent("click",Remove);
									}
								},
								onComplete:function()
								{
									$$('#video,#select_escort,#upload').set('disabled','');
								}
							});
						getVideo.send();
					}
				}
		});	
	}
};

var PlayVideo = function()
{	if($('video_pop_btn'))
	$$('#video_pop_btn,#video-play-wrapper').addEvent('click',function(){
		var escortId = $('escort_id').get('value');
		//var prop = $('prop').get('value').split('-');
		var prop = $('prop').get('value');
		var video = $('config').get('value');
		var image =$('img').get('value');
		AjaxEscortListVideo(escortId,video,image,prop,true);
	});
}
var EscortListVideo = function(video,image,prop)
{		prop = prop.split('-');
		Play(video,image,prop[0],prop[1],true);
}

var AjaxEscortListVideo = function(escort_id,video,image,prop,from_sedcard)
{		
	var req = new Request.JSON({
		method: 'get',
		url: '/index/ajax-play-video?escort_id=' + escort_id,
		onComplete: function(response) {
			prop = prop.split('-');
			Play(escort_id,video,image,prop[0],prop[1],true, response,from_sedcard);
		}
	}).send();
}