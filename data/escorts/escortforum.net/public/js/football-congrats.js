var footballTimoutId;
window.addEvent('domready', function(){
	startConfetti();
	$('football-congrats').fade('in');
	Cookie.write('closed_football_congrats',1,{duration: 10});
	footballTimoutId = setTimeout(closeFootballPopup, 20000);
	$$('#football-congrats .close').addEvent('click',closeFootballPopup);
});

var closeFootballPopup = function(){
	$('football-congrats').fade('out');
	clearTimeout(footballTimoutId);
	return false;
}