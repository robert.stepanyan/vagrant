var Cubix = {};

Cubix.Lang = {};

Cubix.ShowTerms = function (link) {
	window.open(link, 'showTerms', 'menubar=no, resizable=yes, status=no, scrollbars=yes, toolbar=no, width=640, height=480');
	return false;
}

Cubix.ShowCookiePolicy = function (link) {
    window.open(link, 'showCookiePolicy', 'menubar=no, resizable=yes, status=no, scrollbars=yes, toolbar=no, width=640, height=480');
    return false;
}

var _log = function (data) {
	if ( console && console.log != undefined ) {
		console.log(data);
	}
}

var _st = function (object) {
	var parts = object.split('.');
	if ( parts.length < 2 ) {
		return _log('invalid parameter, must contain filename with extension');
	}

	var ext = parts[parts.length - 1];
	
	var base_url = 'http://st.beneluxxx.com',
		prefix = '';

	switch ( ext ) {
		case 'jpg':
		case 'gif':
		case 'png':
			prefix = '/img/';
			break;
		case 'css':
			prefix = '/css/';
			break;
		case 'js':
			prefix = '/js/';
			break;
		default:
			return _log('invalid extension "' + ext + '"');
	}

	url = base_url + prefix + object;

	return url;
}

window.addEvent('domready', function() {
	
});

/* START :: INDEX PAGE */
var Index = {};

Index.Cities = {
    init: function () {
        $('load-more').addEvent('click', function(){
            $$('.more-cities').removeClass('hidden');
            this.addClass('hidden');
        });
    },

    liveSearch: function () {
        var self = this;
        var searchInput = $('city-search');

        searchInput.addEvent('keyup', function(e){
            self.filter(searchInput);
        });

    },

    filter: function ( search ){
        var searchQuery = search.value.toLowerCase();
        
        $$('.cities .city .city-title').each( function(el) {
            var cityName = el.innerHTML.toLowerCase();
            if (cityName.indexOf(searchQuery) == -1 ){
                el.getParent('li').addClass('hidden');
            }else{
                if ( el.getParent('li').hasClass('hidden') )
                el.getParent('li').removeClass('hidden');
            }
        });
    }
}

Index.Escorts = {
    page: null,
    startSearch: null,

    init: function(){
            var self = this;

            $('load-more-escorts').addEvent('click', function(){
                var page = $('load-more-escorts').get('data-page');
                //if( page == 1 ) page = 2;

                var data = { "page" : Number( page ) + 1, "ajax": 1 }
                self.sendRequest(data);

            })
        },


    sendRequest: function( data ){
        var self = this;
        var block = $('ajaxResults');
        var pageIndexAtt = $('load-more-escorts');

        new Request({
            url: '/',
            data: data,
            method: 'get',
            onProgress: function(){
            },
            onSuccess: function (resp) {
                console.log( resp );
                var parent = Elements.from( resp );
                var elms = parent.getElements('.ajax-escort');
                elms = elms[1];

                elms.each(function(elm, i){
                    block.adopt( elm);
                })
                
                //block.inject(elms, 'after');
                pageIndexAtt.set('data-page', Number(data['page']));
            },
            onComplete: function(){

            }
        }).send();
    },

    removeChildElements: function( parent ){
        parent.set('html', '');
    },

    liveSearch: function () {
        var self = this;
        var searchInput = $('escort-search');
        var block = $('ajaxResults');

        searchInput.addEvent('keyup', function(){
            if ( self.startSearch ) clearTimeout(self.startSearch);
            if ( searchInput.value.length > 0 ) {
                self.startSearch = setTimeout( function(){ 
                    self.sendFilter(searchInput.value); 
                }, 1000 )
            }else{
                self.removeChildElements( block );
                self.sendRequest({ 'page': 1, 'ajax': 1 });
            }
        });

    },

    sendFilter: function( showname ){
        var self = this;
        var block = $('ajaxResults');
        var pageIndexAtt = $('load-more-escorts');

        new Request({
            url: '/search?gender=1&order=1&search=Cerca&showname=' + showname,
            data: { "ajax": 1 },
            method: 'get',
            onProgress: function(){
            },
            onSuccess: function (resp) {
                var parent = Elements.from( resp );
                var elms = parent.getElements( '.escort' );
                self.removeChildElements( block );

                elms.each(function(el){
                    block.adopt( el );
                });
            },
            onComplete: function(){
            }
        }).send();
    }

}


/* END :: INDEX PAGE */

/* START :: ESCORT LIST */
var MEscorts = {};

MEscorts.View = {
    

    init: function() {
        var self = this;
        var viewSwitchers = $$('#view-switcher a');
        var viewChoice = self.getChoice('view');

        viewSwitchers.each(function(el){
            el.addEvent('click', function(e){
                if ( !e.target.hasClass('active') ){
                    var viewSel = e.target.id.split('-')[0];

                    self.switchView(viewSel);
                    self.setChoice(viewSel);
                }
            });

        });

        if ( typeof viewChoice == "undefined" || viewChoice == null ) {
            self.switchView('grid');
            self.setChoice('grid');
        } else {
            self.switchView(viewChoice);
            self.setChoice(viewChoice);
        }

    },

    setChoice: function(value) {
        Cookie.dispose('view');
        Cookie.write('view', value);
    },

    getChoice: function(name) {
        return Cookie.read(name);
    },

    switchView: function(v) {
        var viewPage = $('city-page');
        var viewSwitchers = $$('#view-switcher a');
        var viewSelector = $(v + '-view');

        if ( viewPage.hasClass('list') ) {
            if ( v != 'list' ) {
                viewPage.removeClass('list')
            } 
        } else {
            if ( v == 'list' ) {
                viewPage.addClass('list')
            } 
        }

        viewSwitchers.each(function(el){
            if ( el.hasClass('active') ){
                el.removeClass('active');
            }
        });
        
        viewSelector.addClass('active');

    }
};

/* END :: ESCORT LIST */

/* START :: ESCORT PROFILE */

MEscorts.Profile = {
    init: function() {
        var self = this;
        var pMenu = $('menu');
        var pMenuItems = $$('#menu li a');

        self.deslectAll(pMenuItems);
        self.selectMenu($('t_bio'));
        self.hideAll();
        self.showContent('bio');

        pMenuItems.each(function(el){
            el.addEvent('click', function(e){
                var parentCont = e.target.getParent('li');
                var strName = e.target.getParent('li').id.replace('t_','');

                self.deslectAll(pMenuItems);
                self.selectMenu(parentCont);
                self.scrollToContent(strName);
                self.hideAll();
                self.showContent(strName)
                
            })

        });



    },

    hideAll: function() {
        var pPageContainers = $$('#page div[id*=_container]');

        pPageContainers.each(function(el){
            el.addClass('none');
        });
    },

    deslectAll: function(menu) {
        menu.each(function(el){
            var navItemCont = el.getParent('li');

            if ( navItemCont.hasClass('active') ){
                navItemCont.removeClass('active');
            }
        });

    },

    selectMenu: function(menuItem) {
        menuItem.addClass('active');
    },

    showContent: function(name){
        $$('#page div[id*=' + name + '_container]').removeClass('none');
        
        if (name == 'bio'){
            $$('#page div#about_container').removeClass('none');    
        }
    },

    scrollToContent: function(name){
        new Fx.Scroll(window).start(0, 0);;
    },
}

/* END :: ESCORT PROFILE */

/* START :: ESCORT REVIEWS */
MEscorts.Reviews = {
    revCount: null,
    read_review: null,
    close_review: null,

    init: function(lang, escShowname, escId){
        var self = this;
        var mLoadMore = ( typeof $('load-more') != 'undefined' ) ? $('load-more') : false;

        if( !mLoadMore ) return false;

        var readReviews = $$('#reviews a.read-review');
        
        var page = parseInt($('load-more').get('data-page'));
        var reviews = $$('#reviews a[id*=toggle]');

        mLoadMore.addEvent('click', function() {
            self.sendRequest({
                'lang': lang,
                'showname': escShowname,
                'id': escId,
                'page': page,
                'ajax': 1
            });

            $('load-more').set('data-page', page + 1);
        });
        
        

    },

    setCount: function ( counter ) {
        var self = this;
        self.revCount = counter;
    },

    sendRequest: function( data ) {

        var self = this;

        
        new Request({
            url: data.lang + '/reviews-ajax/' + data.showname + '-' + data.id,
            data: { 'page' : data.page },
            method: 'get',

            onProgress: function() {
            },

            onSuccess: function (resp) {
                var revCont = $('reviews');
                var respEl = Elements.from(resp);

                revCont.adopt(respEl);

                var shownRevs = $$('#reviews .user-avatar');

                if (self.revCount <= shownRevs.length  ) {
                    $('load-more').empty().destroy();
                }

                 
            },

            onComplete: function() {}
        }).send();
    },

    getReviewById: function ( data, lang, escShowname, escId ) {
        var self = this;

        new Request({
            url: lang + '/evaluations/' + escShowname + '-' + escId,
            data: {'perpage': 'all', 'ajax': 1},


            onSuccess: function (resp) {
                var recedRevs = Elements.from(resp);
                var revCont = $('slidable-' + data.id);

                recedRevs.each(function(el){
                    if (el.id == data.id) {
                        
                        if (revCont.hasClass('heightAuto')) {
                            revCont.removeClass('heightAuto');
                            data.set('html', "&#9658; " + self.read_review);
                        } else {
                            revCont.empty();
                            revCont.adopt(el.getElement('div.review-info'));
                            revCont.addClass('heightAuto');
                            data.set('html', "&#9658; " + self.close_review);
                        }
                    }
                });
            }


        }).send();
    }

}

MPage = {}

MPage.Scroll = {
    init: function(){
        
        var self = this;
        var objScrollTop = $('scroll-top');

        window.addEventListener('scroll', function() {
            var intTop = window.pageYOffset;
            
            objInfoBar = undefined;
            objBubbleMsg = undefined;

            var objInfoBar = $('info-bar-full');
            var objBubbleMsg = $('bubble-msg');

            if (typeof objBubbleMsg == 'object' && objBubbleMsg){
                objScrollTop.setStyle('bottom', objBubbleMsg.getSize().y + 4);
            } else {
                objScrollTop.setStyle('bottom', 4);
            }

            if (typeof objInfoBar == 'object' && objInfoBar){
                objScrollTop.setStyle('bottom', objInfoBar.getSize().y + 4);
            }

            objScrollTop.setStyle('opacity', 0);
            if ( decTimer ) clearTimeout (decTimer);

            var decTimer = setTimeout( self.showArrow(intTop), 500 );
        });

        objScrollTop.addEvent('click', function(){
            var eScrollToTop = new Fx.Scroll(window).toTop();
        });
        

    },

    showArrow: function(intTop){
        var objScrollTop = $('scroll-top');
        var objHeader = $('header');
        
        objScrollTop.set('tween', {
            duration: 'short',
        });

        if ( intTop > objHeader.getSize().y ) {
            objScrollTop.removeClass('none');
            objScrollTop.tween('opacity', 0, 0.5);
        } else {
            objScrollTop.removeClass('none');
            objScrollTop.tween('opacity', 0, 0);
        }
    }
}

MPage.Sticky = {
    init: function(){
        window.addEvent('scroll', function() {
            var scrollTop = window.pageYOffset;
            var st_crumbs = $('path');
            var st_menu = $('menu');
            var st_page = $('page');

            if (scrollTop > 86) {
                st_crumbs.addClass('fixTop');
                st_menu.addClass('fixTop');
                st_page.addClass('mt88');
            } else {
                st_crumbs.removeClass('fixTop');
                st_menu.removeClass('fixTop');
                st_page.removeClass('mt88');
            }
        });
    }
}








/***************** Karo Start *****************/


var Private = {};

Private.Message = {
    init: function( showname, escort_id ){
        var self = this;
        $$('.private-messanger').addEvent('click', function(){
            var data = { "participant_id" : $(this).getProperty('data-id'), "participant_type" : 'escort'/*$(this).getProperty('data-type')*/ }
            //console.log( data );
            self.sendRequest( data, showname, escort_id );
        })
    },

    sendRequest: function( data, showname, escort_id ){
        var self = this;
        var block = $('private-message-block');
        var wrapper = $('private-message-wrapper');
        var scoller = new Fx.Scroll(window);

        new Request({
            url: '/private-messaging/send-message-ajax',
            data: data,
            method: 'get',
            onProgress: function(){
                wrapper.setStyles({ 'display' : 'block', 'opacity' : 1, 'visibility' : 'visible' });
            },
            onSuccess: function (resp) {
                //resp = JSON.decode(resp, true);
                if( resp == 'signin' ){
                    window.location.href = "/private/signin";
                }
                if ( ! resp ) return;
                //wrapper.empty();
                block.set('html', resp);
                scoller.toElement(block);
                self.sendMessage( escort_id );
            },
            onComplete: function(){

            }
        }).send();
    },

    sendMessage: function(escort_id){
        var sendButton = $$('.red-btn');

        sendButton.addEvent('click', function(e){
            e.preventDefault();

            var data = {
                'participant' : $('participant').get("value"),
                'captcha' : $('f-captcha').get('value'),
                'message' : $('privateMessage').get('value'),
                'escort_id' : escort_id
            };

            new Request({
                url: '/private-messaging/send-message-ajax',
                data: data,
                method: 'post',
                onSuccess: function (resp) {
                    resp = JSON.decode(resp, true);
                    if( resp.status == 'error' ){
                        if( typeof resp.msgs.captcha != 'undefined' ){
                            if( resp.msgs.captcha == '' ){
                                $('f-captcha').addClass('field-error');
                            } else {
                                $('f-captcha').addClass('field-error').set('value', 'incorect value');
                            }
                        } else {
                            $('f-captcha').removeClass('field-error');
                        }

                        if( resp.msgs.fmessage == '' ){
                            $('privateMessage').addClass('field-error');
                        } else {
                            $('privateMessage').removeClass('field-error');
                        }
                    } else {
                        $('privateMessage').set('value', '').removeClass('field-error');
                        $('f-captcha').set('value', '').removeClass('field-error');
                        $$('.send-success').set('html', resp.msg);
                    }
                }
            }).send();
        })
    }
};

Private.User = {
    init: function () {},

    signup: function() {
        var userTypeOptions = $$('.reg-top-container input');
        var salsePerson = $('sales_person');
        
        userTypeOptions.each(function(el) {
            el.addEvent('click', function(){
                ( el.id == 'user_type_member' ) ? salsePerson.disabled = true : salsePerson.disabled = false;
            });
        });
    }
};



/***************** Karo End *****************/


MCubix.ErrorsHandler = {
  elm: null,

    messages: {
        'have_errors': 'Exists error !',
        'city_count': 'City count',
        'city_count_test': 'City count',
        'year': 'Year !',
        'month': 'Month must be between 1 and 12',
        'day': 'Day must be between 1 and 31',
        'day_31': 'Self month doesn\'t have 31 days!',
        'day_feb': 'February doesn\'t have that many days!',
        'date_out': 'Please select currect date'
    },

    doShow: function( elm, key, dict ){
        var self = this;

        if( !dict ) {
            elm.set('html', self.messages[key]).show();
        } else {            
            elm.set('html', key).show();
        }
        setTimeout(function() {
            elm.set('html', '').hide();
        }, 3000 );
    },

    doHide: function(elm){
        elm.set('html', '').hide();
    }
}