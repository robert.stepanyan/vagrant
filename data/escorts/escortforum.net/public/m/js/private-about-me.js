$(document).ready( function(){
	//var lngPicker = new Controls.LngPicker($$('.lng-picker')[0], {
	//	onChange: function (el) {
	//		var lng = el.get('cx:lng');
	//		$$('.container-about-field').setStyle('display', 'none');
	//		$('container-about-' + lng).setStyle('display', 'block').focus();
	//	}
	//});
	
	if ( typeof(tinyMCE) !== 'undefined' ) {
		tinyMCE.init({
			mode : "textareas",
			theme : "advanced",
			theme_advanced_toolbar_location	  : "top",
			theme_advanced_toolbar_align	  : "left",
			editor_selector : 'about-field',
			theme_advanced_buttons1 : "bold,italic",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3_add : "",
			remove_script_host : false,
			relative_urls : false,
			convert_urls : false
		});
	}
});