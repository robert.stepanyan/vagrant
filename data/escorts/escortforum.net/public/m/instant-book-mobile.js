var Sceon = Sceon || {};

Sceon.getDateDelta = function (futureDt, nowDt) {
  var delta = Math.abs(futureDt - nowDt) / 1000;

  var days = Math.floor(delta / 86400);
  delta -= days * 86400;

  var hours = Math.floor(delta / 3600) % 24;
  delta -= hours * 3600;

  var minutes = Math.floor(delta / 60) % 60;
  delta -= minutes * 60;

  var seconds = delta % 60;

  return {
    h: hours,
    m: minutes,
    s: seconds
  }
}

var StorageManager = (function () {
  function cleanup() {
    var dtRule = Date.now() - 24 * 60 * 60 * 1000;

    Object.keys(localStorage).forEach(function (item) {
      if (~item.indexOf('ireq::')) {
        var mTime = localStorage.getItem(item);
        (mTime < dtRule) && localStorage.removeItem(item);
      }
    });
  }

  function getReqs() {
    return Object.keys(localStorage).filter(function (item) {
      if (~item.indexOf('ireq::')) {
        return item;
      }
    }) || [];
  }

  function exists(escortId) {
    var existingReqs = Object.keys(localStorage).filter(function (item) {
      if (item === ('ireq::' + escortId)) {
        return item;
      }
    });

    if (existingReqs && existingReqs.length) return true;

    return false;
  }

  return {
    cleanup: cleanup,
    getReqs: getReqs,
    exists: exists
  };

})();

var getCookie = function (name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
};

Sceon.instantBookPopup = (function() {
  var
    inProcess = false,
    url = '/escorts/ajax-instant-book';

  function show(options) {
    StorageManager.cleanup();

    if (StorageManager.getReqs().length >= 7) {
      alert(instantBookLng.rulteLimit2);
      $('instant-book-btn').removeClass('is-loading');
      return false;
    }

    if (StorageManager.exists($('escort_id').get('value'))) {
      alert(instantBookLng.rulteLimit1);
      $('instant-book-btn').removeClass('is-loading');
      return false;
    }

    if (inProcess) return false;
    var showInstantNotification = getCookie('instant_notif');

    if (!showInstantNotification) {

        var $header = $('<h2/>', {
            class: 'Header',
            text: instantBookLng.infoPopupPublicHeader
        });

        var html = instantBookLng.infoPopupPublicBody;

        var $body = $('<div />', {
            class: 'Body',
            id: 'instant-book-popup'
        }).html(html);

        var $container = new InstantModal({
            header: $header,
            body: $body,
            id: 'InstantBook',
            close: true
        });


        $container.find('.instant-book-info-btn').on('click', function() {
            document.cookie = 'instant_notif=1; expires=Fri, 31 Dec 2024 23:59:59 GMT';
            $('#InstantBook').remove();
            $('#instant-book-btn').click();
        });

        $container.show();

        return false;
    }

    // document.cookie = 'sandwich=turkey; expires=Fri, 31 Dec 2024 23:59:59 GMT;

    inProcess = true;

    $.ajax({
      url: url,
      method: 'GET',
      data: options,
      success: function(resp) {
        $('#instant-book-btn').removeClass('is-loading');
        
        var $header = $('<h2/>', {
          class: 'Header'
        }).text('Instantly Book an Escort!');

        var $body = $('<div />', {
          class: 'Body',
          id: 'instant-book-popup'
        }).html(resp);

        var $container = new InstantModal({
          header: $header,
          body: $body,
          id: 'InstantBook',
          close: true
        }).show();

        inProcess = false;

        var $hourElem = $('[name="hour_min"]');
        var endDateFromEl = $hourElem.data('end');
        var y = endDateFromEl.split(' ')[0].split('-')[0];
        var m = endDateFromEl.split(' ')[0].split('-')[1];
        var d = endDateFromEl.split(' ')[0].split('-')[2];
        var h = endDateFromEl.split(' ')[1].split(':')[0];
        var m = endDateFromEl.split(' ')[1].split(':')[1];

        var endDateUTC = new Date(y, m, d, h, m);
        console.log(endDateUTC);
        var timeZoneOffset = (new Date()).getTimezoneOffset() * 60*1000;
        var curDate = new Date(Date.now() + 15*60*1000);
        var startDate = curDate;
        var endDate = new Date(endDateUTC.getTime() - timeZoneOffset);
        var startHours = curDate.getHours();
        var endHours = endDate.getHours();
        var timeSelector = [];

        for (var i = startHours; i <= endHours; i++) {
            var h = (i > 23) ? i - 24 : i;
            var d = (i > 23) ? new Date(startDate.getTime() + 24*60*60*1000) : startDate;
            var dataDateAttr = [d.getFullYear().toString(), ('0' + (d.getMonth() + 1)).slice(-2), ('0' + d.getDate()).slice(-2)].join('-');

            var minArr = [0, 15, 30, 45];
            $.each(minArr, function(idx, m) {
                var __d = new Date(dataDateAttr.split('-')[0], dataDateAttr.split('-')[1], dataDateAttr.split('-')[2], h, m).getTime();
                if (__d > curDate && __d < endDate) {
                    timeSelector.push([('0' + h).slice(-2) + ':' + ('0'+ m).slice(-2), dataDateAttr]);
                }
            });
        }

        $.each(timeSelector, function(idx, el) {
          $hourElem.append($('<option/>', {
              value: el[0],
              html: el[0],
              'data-date': el[1]
          }));
        });

        $hourElem.on('change', function() {
            $(this).blur();
        });


        $('[name="wait_time"]').on('change', function() {
          $(this).blur();
        });

        $('.left-arr').on('click', function(){
            if ($('#inst-duration').val() <= 1) return false;

            var duration = parseInt($('#inst-duration').val()) - 1;
            $('#inst-duration').val(duration);
            $('#inst-duration-text').text(duration);
        });

        $('.right-arr').on('click', function(){
          var duration = parseInt($('#inst-duration').val()) + 1;
          $('#inst-duration').val(duration);
          $('#inst-duration-text').text(duration);
        });

        $('.location-ref').on('click', function(){
          var location = $('#inst-location').val();

          if(location == 'incall'){
            $('#inst-location').val('outcall');
            $('.location-string').text('Outcall');
            $('#inst-outcall-address').parent().show();
          }
          else{
            $('#inst-location').val('incall');
            $('.location-string').text('Incall');
            $('#inst-outcall-address').parent().hide();
          }
        });

        $('#instant-book-form').on('submit', function(e) {
          e.preventDefault();

          $('#InstantBook .instant-book-btn-wrapper-inner').addClass('is-loading');
          var $errors = $('instant-book-popup').find('.errors');
          if ($errors.length) {
            $erros.html('');
          }

          var bookingDateYMD = $('[name="hour_min"]').find(':selected').data('date');
          var time = $('[name="hour_min"]').val();
          var bookingDateObj = new Date(bookingDateYMD + 'T' + ('0' + time.split(':')[0]).slice(-2) + ':' + ('0' + time.split(':')[1]).slice(-2) + ':' + '00');
          var bookingDate = bookingDateObj.toISOString();

          var
            duration = +$('[name="duration"]').val(),
            location = $('[name="loacation"]').val(),
            outcall_address = $('[name="outcall"]').length ? $('[name="outcall"]').val() : '',
            special_request = $('[name="special_request"]').val(),
            phone = $('[name="phone"]').val(),
            contact_type = [],
            wait_time = +$('[name="wait_time"]').val(),
            escort_id = +$('#escort_id').val();

            $('[name="contact_type"]:checked').each(function() {
              contact_type.push($(this).val());
            });

          window.instantSocket.emit('request_booking', {
            hour: time.split(':')[0],
            min: time.split(':')[1],
            booking_date: bookingDate,
            duration: duration,
            location: location,
            outcall_address: outcall_address,
            special_request: special_request,
            phone: phone,
            contact_type: contact_type,
            wait_time: wait_time,
            escort_id: escort_id,
          });

          localStorage.addItem(escort_id, Date.now());
        });
      }
    })
  }

  return {
    show: show
  }
})();

Sceon.instantCountDown = function(params) {
  return this.init(params);
};

Sceon.instantCountDown.prototype = {
  init: function (params) {
    this.params = params || {};

    this.build();
  },

  build: function () {
    if (!this.params.$el) return false;

    var
      _hours = this.params.timer.h || 0,
      _minutes = this.params.timer.m || 0,
      _seconds = this.params.timer.s || 0;

    this.timeInMs = (_hours * 60 * 60 * 1000) + (_minutes * 60 * 1000) + (_seconds * 1000);
  },

  start: function () {
    var _self = this;

    this.countDownTimer = setInterval(function () {
      if (_self.timeInMs <= 0) {
        _self.params.timeUpCallback && _self.params.timeUpCallback(_self.params.$el);
        _self.stop();
      }
      _self.timeInMs -= 1000;

      var html = _self._decorateHtml();

      _self._draw(html);
    }, 1000);
  },

  stop: function () {
    clearInterval(this.countDownTimer);
  },

  _decorateHtml: function () {
    var _hours = 0
      , _minutes = 0
      , _seconds = 0
      , _rem = this.timeInMs;
    _hours = Math.floor(_rem / (60 * 60 * 1000));

    if (_hours) _rem = _rem % (60 * 60 * 1000);
    _minutes = Math.floor(_rem / (60 * 1000));

    if (_minutes) _rem = _rem % (60 * 1000);
    _seconds = Math.floor(_rem / (1000));

    var html = '';

    if (_hours) {
      html += '<span class="Countdown__hours">'+ ('0' + _hours).slice(-2) + '</span>';
    }

    if (_hours) {
      html += ':';
    }

    html += '<span class="Countdown__minutes">' +  ('0' + _minutes).slice(-2) + '</span>';
    html += ':';
    html += '<span class="Countdown__seconds">' + ('0' + _seconds).slice(-2) + '</span>';

    if (this.params.customText) {
      html = this.params.customText.replace('%COUNTDOWN%', html);
    }
    return html;
  },

  _draw: function (html) {
    this.params.$el.html(html);
  }
};

$(document).ready( function () {

  function InstantModal (params) {
    /**
     * params should contain { header, body }
     */
    return this.init(params);
  };

  InstantModal.prototype = {
    init: function (params) {
      this.params = params || {
        header: $('div'),
        body: $('div')
      };

      this.build().bindEvents();

      this.html.show = this._show;
      this.html.hide = this._hide;

      return this.html;
    },

    _buildOverlay: function () {
      return $('<div/>', {
        id: this.params.id || 'BookingRequest',
        css: {
          width: '100%',
          height: '100%',
          position: 'fixed',
          backgroundColor: 'rgba(0,0,0,0.2)',
          top: 0,
          left: 0,
          zIndex: 9999
        }
      });
    },

    _buildModal: function () {
      return $('<div/>', {
        class: 'Modal'
      });
    },

    _buildCloseBtn: function() {
      return $('<div/>', {
        class: 'instant-popup-close-btn',
        html: '&times;'
      });
    },

    build: function () {
      var Overlay = this._buildOverlay(),
        Modal = this._buildModal();

      if (this.params.close) {
        this.closeBtn = this._buildCloseBtn();
        Modal.append(this.closeBtn);
      }

      Modal.append(this.params.header);
      Modal.append(this.params.body);
      Overlay.append(Modal);

      this.Overlay = Overlay;

      $(document.body).append(Overlay);

      return this;
    },

    bindEvents: function () {
      var _self = this;

      if (this.closeBtn) {
        this.closeBtn.on('click', function() {
          _self.Overlay.remove();
          var instantBookBtn = $('.instant-book-btn');
          var $dashIBBlock = $('.DashboardItem--instant-book');
          var $modals = $('.Modal');

          if (instantBookBtn.length) {
            instantBookBtn.removeClass('is-loading');
          }

          if ($dashIBBlock.length && !$modals.length) {
            window.location.reload();
          }
        });
      }

      return this;
    },

    _show: function () {
      this.find('.Modal').addClass('shown');
    },

    _hide: function () {
      this.find('.Modal').removeClass('shown');
    },

    get html() {
      return this.Overlay;
    }
  };

  !window.InstantModal && (window.InstantModal = InstantModal);

  var InstantPrivateProfile = function(params) {
    return this.init(params);
  };


  InstantPrivateProfile.prototype = {
    init: function(params) {
      this.params = params || {};

      this
        .build()
        .bindEvents();
    },

    build: function() {

      return this;
    },
    bindEvents: function() {
      /**** INSTANT BOOK PAGE ****/
      var
        _self = this,
        $actionBtn = this.params.createForm.find('.instant-booking-btn'),
        $cancelBtn = this.params.editFrom.find('.cancel'),
        $changeBtn = this.params.editFrom.find('.change');

    	window.instantSocket.emit('get_availability', {
        escortId: headerVars.currentUser.escort_data.escort_id.toString(),
      });

      $actionBtn.on('click', function(e) {
        e.preventDefault();


          var showInstantNotification = getCookie('instant_notif_private');

          if (!showInstantNotification) {

              var $header = $('<h2/>', {
                  class: 'Header',
                  text: instantBookLng.infoPopupPrivateHeader
              });

              var html = instantBookLng.infoPopupPrivateBody;

              var $body = $('<div />', {
                  class: 'Body',
                  id: 'instant-book-popup'
              }).html(html);

              var $container = new InstantModal({
                  header: $header,
                  body: $body,
                  id: 'InstantBook',
                  close: true
              });


              $container.find('.instant-book-info-btn').on('click', function() {
                  document.cookie = 'instant_notif_private=1; expires=Fri, 31 Dec 2024 23:59:59 GMT';
                  $('.instant-popup-close-btn').click();
                  $('.instant-booking-btn').click();
              });

              $container.show();

              return false;
          }





        $(this).addClass('is-loading');

        var OneSignal = window.OneSignal || [];
        var isSupported = OneSignal.isPushNotificationsSupported();

        if (isSupported) {
          OneSignal.init({
            appId: "52e8ac40-e364-40f3-b410-8709168f7ed9",
            autoRegister: true
          });

          OneSignal.on('subscriptionChange', function (isSubscribed) {
            if (isSubscribed) {
              OneSignal.sendTag('escortId', headerVars.currentUser.escort_data.escort_id);
            }
          });
        }

        var 
          _duration = _self.params.createForm.find('[name="duration"] option:selected').val(),
          _location = [];

        _self.params.createForm.find('[name="location"]:checked').each(function() { _location.push($(this).val()) });

          window.instantSocket.emit('available_for_booking', {
            escortId: headerVars.currentUser.escort_data.escort_id,
            duration: _duration,
            location: _location
          });
      });

      $cancelBtn.on('click', function(e) {
        e.preventDefault();
        $(this).removeClass('is-loading');

        window.instantSocket.emit('no_longer_available_for_booking', {
          escortId: headerVars.currentUser.escort_data.escort_id
        });
      });

      $changeBtn.on('click', function (e) {
        e.preventDefault();
        $(this).removeClass('is-loading');

        _self.params.editFrom.hide();
        _self.params.createForm.show();
        $actionBtn.off('click');
        $actionBtn.on('click', function(e) {
          e.preventDefault();

          var
            _duration = _self.params.createForm.find('[name="duration"] option:selected').val(),
            _location = _self.params.createForm.find('[name="location"]:checked').val();

          window.instantSocket.emit('available_for_booking_update', {
            escortId: headerVars.currentUser.escort_data.escort_id.toString(),
            duration: _duration,
            location: _location === 'outcall' ? 'outcall' : 'incall'
          });
        });
      });

      /**** PRIVATE AREA PAGE ****/
      var instantActionDeny = $('.booking-action-deny');
      var instantActionUnavailable = $('.booking-action-unavailable');
      var instantActionAccept = $('.booking-action-accept');

      if (instantActionDeny && instantActionUnavailable && instantActionAccept) {
        instantActionDeny.on('click', function() {
          instantSocket.emit('booking_request_reply',{
            status: 'deny',
            token: $(this).attr("data-hash"),
            id: $(this).attr("data-id")
          });

          var $bookingBlock = $('#booking-' + $(this).attr("data-id")).slideUp('fast');
          var $siblings = $bookingBlock.siblings();
          var $sliderEl = $bookingBlock.closest('[class^="IBSlider--"]');
          var $heading = $sliderEl.siblings('[class^="IBHeading--"]');
          var $subHeading = $heading.next('.IBSubHeading');
          var slider = $sliderEl.get(0).swiper;

          $bookingBlock.remove();

          if (!$siblings.length) {
            slider.destroy();
            $sliderEl.remove();
            $heading.remove();
          } else {
            slider.updateSlides();
            slider.updateSlidesClasses();
            slider.slideTo(0, 100);
            $heading.find('[class^="IBHeading__counter--"]').text('1/' + slider.slides.length);

            if ($subHeading.length && slider.slides.length === 1) {
              $subHeading.remove();
            }
          }
        });
      }

      instantActionUnavailable.on('click', function() {
        instantActionDeny.trigger('click');
        window.instantSocket.emit('no_longer_available_for_booking', {
          escortId: headerVars.currentUser.escort_data.escort_id
        });

        $('#booking-' + $(this).attr("data-id")).slideUp('fast');

        $('.InstantBookItem--available').hide();
        $('.InstantBookItem').show();

      });

      instantActionAccept.on('click', function() {
          instantSocket.emit('booking_request_reply', {
            status: 'accept',
            token: $(this).attr("data-hash"),
            id: $(this).attr("data-id")
          });

          location.reload();
      });

      // COUNTERS
      $('[data-role="countdowntimer"]').each(function() {
        var deltaArr = $(this).html().split(':');
        var isAccepted = $(this).closest('.booking-requests').hasClass('booking-status-accepted');
        var deltaObj = {
          h: deltaArr[0],
          m: deltaArr[1],
          s: deltaArr[2]
        };

        var timer = new Sceon.instantCountDown({
          $el: $(this),
          timer: deltaObj,
          timeUpCallback: function ($html) {
            var $bookingBlock = $('#' + $html.data('id'));
            var $siblings = $bookingBlock.siblings();
            var $sliderEl = $bookingBlock.closest('[class^="IBSlider--"]');
            var $heading = $sliderEl.siblings('[class^="IBHeading--"]');
            var slider = $sliderEl.get(0).swiper;

            $bookingBlock.remove();

            if (!$siblings.length) {
              slider.destroy();
              $sliderEl.remove();
              $heading.remove();
            } else {
              slider.updateSlides();
              slider.updateSlidesClasses();
              slider.slideTo(0, 100);
              $heading.find('[class^="IBHeading__counter--"]').text('1/' + slider.slides.length);
            }
          },
          customText: isAccepted ? instantBookLng.timeLeft : null
        });

        timer.start();
      });

      return this;
    }
  }

  var InstantBooking = function (params) {
    return this.init(params);
  };

  InstantBooking.prototype = {
    userType: '',
    escortId: '',
    token: '',

    init: function (params) {
      var _self = this;
      this
        .auth(params)
        _self
          .connect()
          .bindEvents()
          .expose();
    },

    auth: function (params) {
      if (!params._user || params._user.user_type !== 'escort') {
        this.userType = 'guest';
      } else {
        this.userType = 'escort';
        this.escortId = params._user.escort_data.escort_id.toString();
      }

      this.token = params._sessionId;

      return this;
    },

    connect: function () {
      this.socket = io('/booking', {
        transports: ['websocket'],
        query: {
          uType: this.userType,
          // escort id if the user is logged in as escort
          eId: this.escortId,
          t: this.token
        }
      });

      return this;
    },

    _getAvailabilityMsg: function(data) {

      var clientEndDate = new Date(data.end_date);
      var mins = Math.floor((clientEndDate.getTime() - new Date().getTime())/(60 * 1000));
      var time = ('0' + clientEndDate.getHours()).slice(-2) + ':' + ('0' + clientEndDate.getMinutes()).slice(-2);
      var text = 'You are available for instant bookings, for '
      + (data.incall ? 'incall' : '') + (data.incall && data.outcall ? ' ' + instantBookLng.and + ' '  : '') + (data.outcall ? 'outcall, ' : ', ')
      + 'for <span class="BannerCountDownTimer">' + mins + '</span> minutes (until ' + time  +')';

      return text;
    },

    bindEvents: function () {
      var _self = this;
      this.socket.on('escort_availability', function (data) {
        var $dashboardInstantBlock = $('.DashboardItem--instant-book');

        if ($dashboardInstantBlock.length && data) {
          var clientEndDate = new Date(data.end_date);
          var deltaObj = Sceon.getDateDelta.apply(null, [clientEndDate.getTime(), new Date().getTime()]);
          var timer = new Sceon.instantCountDown({
            $el: $('.item--available-time-left'),
            timer: deltaObj,
            timeUpCallback: function () {
              $('.InstantBookItem--available').hide();
              $('.InstantBookItem').show();
            }
          });

          timer.start();

          $dashboardInstantBlock.find('.InstantBookItem').hide();
          $dashboardInstantBlock.find('.InstantBookItem--available').show();
          $dashboardInstantBlock.find('.instant-overlay').hide();
        } else {
          $dashboardInstantBlock.find('.instant-overlay').hide();
        }

        var $availableInstantBookBlock = $('#instant-form');
        var $availableInstantBookEditBlock = $('#instant-edit-box');

        if (!$availableInstantBookBlock.length && !$availableInstantBookEditBlock.length) return false;

        if (data) {
          var text = _self._getAvailabilityMsg(data);

          var minuteInterval = setInterval(function() {
            var minutesCount = parseInt($('.BannerCountDownTimer').html());
            if (minutesCount > 0) {
              $('.BannerCountDownTimer').html(--minutesCount);
            } else {
              clearInterval(minuteInterval);
            }
          }, 60 * 1000);

          $availableInstantBookEditBlock.find('.info').html(text);

          $('.mobile-instant-overlay').hide();
          $availableInstantBookBlock.hide();
          $availableInstantBookEditBlock.show();
        } else {
          $('.mobile-instant-overlay').hide();
        }
      });

      this.socket.on('new_booking', function (data) {

        var header = $('<h2/>', {
            class: 'Header',
            text: instantBookLng.bookingRequestHeader
        });

        var paragraphHtml = '';
        var __bd = new Date(data.booking_date);

        var _bookingDateYMD = [('0' + __bd.getDate()).slice(-2), ('0' + (__bd.getMonth() + 1)).slice(-2), __bd.getFullYear()].join('-');
        var _bookingDateHM = [('0' + __bd.getHours()).slice(-2), ('0' + __bd.getMinutes()).slice(-2)].join(':');
        var _bookingDateHtml = _bookingDateHM +
          ' <span style="color: #fff; font-size: 10px; background-color: #d2d2d2; padding: 0 3px; border-radius: 3px;">' +
          _bookingDateYMD +
          '</span>';

        paragraphHtml += '<p><strong>' + instantBookLng.from + '</strong> ' + _bookingDateHtml + '</p>';
        paragraphHtml += '<p><strong>' + instantBookLng.duration + '</strong> ' + data.duration + ' hour(s)</p>';
        paragraphHtml += '<p><strong>' + instantBookLng.location + '</strong> ' + data.location + '</p>';
        if (data.special_request) {
          paragraphHtml += '<p><strong>' + instantBookLng.specialRequest + '</strong>' + data.special_request + '</p>';
        }

        if (data.location === 'outcall') {
          paragraphHtml += '<p><strong>' + instantBookLng.location + '</strong> ' + data.outcall_address + '</p>';
        }

        paragraphHtml += '<p class=\'none phone\'><strong>' + instantBookLng.phone + ': </strong><a href="tel:data.phone" data-phone="' + data.phone + '">' + data.phone + '</a><p>';
        paragraphHtml += '<p class=\'none\'><strong>' + instantBookLng.contactType + '</strong>' + data.contact_type.join(', ') + '<p>';

        var body = $('<div/>', {
          class: 'Body'
        }).html(paragraphHtml);

        var noLongerAvailableBtn = $('<a/>', {
          class: 'btn--no-longer-available',
          href: '#',
          text: instantBookLng.noLonger
        }).on('click', function () {
          instantSocket.emit('booking_request_reply', {
            status: 'deny',
            token: data.hash,
            id: data.id
          });

          instantSocket.emit('no_longer_available_for_booking', {
            escortId: data.escort_id
          });

          var overlay = $(this).closest('#BookingRequest');
          overlay.hide();

          setTimeout(function() {
            overlay.remove();
          }, 500);

          instantSocket.emit('no_longer_available', {});
        });

        var denyBookingRequestBtn = $('<a/>', {
          class: 'btn--deny-booking-request',
          href: '#',
          text: instantBookLng.deny
        }).on('click', function () {

          var overlay = $(this).closest('#BookingRequest');
          overlay.hide();

          setTimeout(function() {
            overlay.remove();
          }, 500);

          instantSocket.emit('booking_request_reply', {
            status: 'deny',
            token: data.hash,
            id: data.id
          });
        });

        var acceptButton = $('<a/>', {
          class: 'btn--accept',
          href: '#',
          text: 'Accept'
        }).on('click', function () {
          var Body = $(this).closest('.Body');
          Body.find('.btn--no-longer-available').remove();
          Body.find('.btn--deny-booking-request').remove();

          Body.find('.none').removeClass('none');
          $(this).text('Call Now');
          $(this).off('click').on('click', function() {
            window.open('tel: ' + Body.find('.phone').attr('data-phone'));

            Body.closest('#BookingRequest').remove();
          });

          instantSocket.emit('booking_request_reply', {
            status: 'accept',
            token: data.hash,
            id: data.id
          });
        });
        body.append(noLongerAvailableBtn);
        body.append(denyBookingRequestBtn);
        body.append(acceptButton);

        new InstantModal({
          header: header,
          body: body,
          close: true
        }).show();
      });

      this.socket.on('request_reply_guest', function(data) {
        // Popup shown

        var $bookingPopup = $('#InstantBook');

        if ($bookingPopup.length) {
          $bookingPopup.find('.instant-popup-close-btn').trigger('click');
        }

        document.cookie = 'ib_request_made=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        if (data.status === 'deny') {
          var header = $('<h2/>', {
            class: 'Header',
              text: instantBookLng.oops
          });

          var paragraphHtml = 'Your request was denied! <br>';

          var body = $('<div/>', {
            class: 'Body'
          }).html(paragraphHtml);

          var acceptButton = $('<a/>', {
            class: 'btn--accept',
            href: '#',
            text: 'Ok'
          }).on('click', function() {
            var overlay = $(this).closest('#BookingRequest');
            overlay.hide();

            setTimeout(function() {
              overlay.remove();
            }, 1000);
          });

          body.append(acceptButton);
        } else if (data.status === 'accept') {
          var header = $('<h2/>', {
            class: 'Header',
              text: instantBookLng.great
          });

          var paragraphHtml = instantBookLng.requestAccepted + ' <br>';

          var body = $('<div/>', {
            class: 'Body',
              text: paragraphHtml
          });

          var acceptButton = $('<a/>', {
            class: 'btn--accept',
            href: '#',
            text: 'Ok'
          }).on('click', function() {
            var overlay = $(this).closest('#BookingRequest');
            overlay.hide();

            setTimeout(function() {
              overlay.remove();
            }, 1000);
          });

          body.append(acceptButton);
        } else if (data.status === 'no_answer') {
          var header = $('<h2/>', {
            class: 'Header',
              text: instantBookLng.oops
          });


          var paragraphHtml = instantBookLng.requestNoAnswer + ' <br>';

          var body = $('<div/>', {
            class: 'Body'
          }).html(paragraphHtml);

          var acceptButton = $('<a/>', {
            class: 'btn--accept',
            href: '#',
            text: 'Ok'
          }).on('click', function () {
            var overlay = $(this).closest('#BookingRequest');
            overlay.hide();

            setTimeout(function () {
              overlay.remove();
            }, 1000);
          });

          body.append(acceptButton);
        }

        new InstantModal({
          header: header,
          body: body
        }).show();

      });

      this.socket.on('request_booking_error', function(data) {
        $('.instant-book-btn-wrapper-inner.is-loading').removeClass('is-loading');
        var $form = $('#instant-book-form'),
          errorsHtml = '';

        if (!$form.length) return false;

        $.each(data, function(value, key) {
          errorsHtml += '<p style=\'color: red\'>' + value + '</p>';
        });

        if ($form.length) {
          $form.find('.errors').append($(errorsHtml));
        }
      });

      this.socket.on('instant_request_success', function(data) {
        var
          $popup = $('#InstantBook'),
          count = data.count * 60 * 1000,
          futureTime = new Date().getTime() + count;

        if (!$popup.length) return false;


        document.cookie = "ib_request_made=1; expires=" + new Date(futureTime).toUTCString() + "; path=/";

        $popup
          .find('form')
          .hide();

        $('#countdown').show();

        var countDown = setInterval(function() {
          if (!$('#countDownTimer').length) {
            clearInterval(countDown);
          }
          var timeLeft = futureTime - new Date().getTime(),
            minutes = Math.floor(timeLeft / 60000),
            seconds = Math.floor((timeLeft % 60000) / 1000);

          $('#countDownTimer').text(('0' + minutes).slice(-2) + ':' + ('0' + seconds).slice(-2));
          if (!minutes && !seconds) {
            clearInterval(countDown);

            var $bookingPopup = $('#InstantBook');

            if ($bookingPopup.length) {
              $bookingPopup.find('.instant-popup-close-btn').trigger('click');
            }
          }
        }, 1000);
      });

      this.socket.on('available_for_booking_error', function() {
        alert('Something went wrong.');
      });

      this.socket.on('available_for_booking_success', function(data) {

        var $availableInstantBookBlock = $('#instant-form');
        var $availableInstantBookEditBlock = $('#instant-edit-box');

        if (!$availableInstantBookBlock.length && !$availableInstantBookEditBlock.length) return false;

        if (data) {
          var text = _self._getAvailabilityMsg(data);
          $availableInstantBookEditBlock.find('.info').html(text);

          $availableInstantBookBlock.hide();
          $availableInstantBookEditBlock.show();
        }

        if (data.is_new == 1) {

          var isSuppoted = OneSignal.isPushNotificationsSupported();
          if (isSuppoted) {
            var header = $('<h2/>', {
              class: 'Header'
            }).text('SMS Notifications');
  
            var paragraphHtml = instantBookLng.pushNotifMessage.replace('%number%', $('[data-phone-number]').val());
  
            var body = $('<div/>', {
              class: 'Body'
            }).html(paragraphHtml);
  
            var applyButton = $('<a/>', {
              class: 'btn--apply',
              href: '#',
              text: instantBookLng.receiveSMS
            }).on('click', function() {
              window.instantSocket.emit('receive_sms');
              var overlay = $(this).closest('#BookingRequest');
              overlay.hide();
  
              setTimeout(function() {
                overlay.remove();
              }, 1000);
            });
  
            var cancelButton = $('<a/>', {
              class: 'btn--cancel',
              href: '#',
              text: instantBookLng.dontWant
            }).on('click', function() {
              window.instantSocket.emit('do_not_receive_sms');
  
              var overlay = $(this).closest('#BookingRequest');
              overlay.hide();
  
              setTimeout(function() {
                overlay.remove();
              }, 1000);
            });
  
            body.append(applyButton, cancelButton);
  
            new InstantModal({ header: header, body: body }).show();
          } else {
            var header = $('<h2/>', {
              class: 'Header'
            }).text('SMS Notifications');
  
            var paragraphHtml = instantBookLng.smsBody.replace('%number%', $('[data-phone-number]').val());
  
            var body = $('<div/>', {
              class: 'Body'
            }).html(paragraphHtml);
  
            var applyButton = $('<a/>', {
              class: 'btn--apply',
              href: '#',
              text: instantBookLng.understand
            }).on('click', function() {
              var overlay = $(this).closest('#BookingRequest');
              overlay.hide();
  
              setTimeout(function() {
                overlay.remove();
              }, 1000);
            });
  
            body.append(applyButton);
  
            new InstantModal({ header: header, body: body }).show();
          }
        }
      });

      this.socket.on('no_longer_available_for_booking_success', function() {
        var $availableInstantBookBlock = $('#instant-form');
        var $availableInstantBookEditBlock = $('#instant-edit-box');
        if (!$availableInstantBookBlock.length && !$availableInstantBookEditBlock.length) return false;

        $availableInstantBookBlock.show();
        $availableInstantBookEditBlock.hide();
      });

      return this;

    },

    expose: function () {
      !window.instantSocket && (window.instantSocket = this.socket);
    }
  };

  $(document).ready(function() {
    setTimeout(function () {
      new InstantBooking({
        _user: headerVars.currentUser,
        _sessionId: headerVars.sessionId
      });

      var
        $availableInstantBookBlock = $('#instant-form'),
        $availableInstantBookEditBlock = $('#instant-edit-box');
        $dashInstBlock = $('.DashboardItem--instant-book');

        if ($availableInstantBookBlock.length || $dashInstBlock.length) {
          new InstantPrivateProfile({
            createForm: $availableInstantBookBlock,
            editFrom: $availableInstantBookEditBlock
          });
        }

    }, 1000);

    var $instantBookBtn = $('#instant-book-btn');

    if ($instantBookBtn.length) {
      $instantBookBtn.on('click', function() {
        $(this).addClass('is-loading');
        Sceon.instantBookPopup.show($(this).data('info'));
      });
    }
  });
});
