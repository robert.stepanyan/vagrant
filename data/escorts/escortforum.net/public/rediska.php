<?
var_dump(getenv('REDIS_HOST'), getenv('REDIS_PORT'));
 
$redis = new Redis(); 
$redis->connect(getenv('REDIS_HOST'), getenv('REDIS_PORT')); 
echo "Connection to server sucessfully"; 
//check whether server is running or not 
echo "Server is running: ".$redis->ping().'<br>'; 
$redis->rpush("languages", "french"); // [french]
$redis->rpush("languages", "arabic"); // [french, arabic]

$redis->lpush("languages", "english"); // [english, french, arabic]
$redis->lpush("languages", "swedish"); // [swedish, english, french, arabic]

$redis->lpop("languages"); // [english, french, arabic]
$redis->rpop("languages"); // [english, french]

$redis->llen("languages"); // 2
echo "<br>";
var_dump($redis->lrange("languages", 0, 0)); // returns all elements
echo "<br>";	
//var_dump($redis->lrange("languages", 0, 1)); // [english, french]*/