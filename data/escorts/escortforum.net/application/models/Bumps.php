<?php

class Model_Bumps extends Cubix_Model
{
	public function get_bump_prices() 
	{
		return parent::_fetchAll('SELECT * FROM bumps_prices');
	}
	public function get_active_bumps($escort_id) 
	{
		return $this->_db->query('SELECT * FROM bumps WHERE escort_id = ? and bumps_amount > 0 and city_id is not NULL', array($escort_id))->fetchAll();
	}
	
}
