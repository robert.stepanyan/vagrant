<?php

class Model_PowerCashGateway
{	
	private $checkout_url = 'https://pay4.powercash21.com/powercash21-3-2/payment/init';
	private $secret = 'VE4T7JXYK1';
	
	//private $test_checkout_url =	'https://sandbox.powercash21.com/pay/payment/init';
	//private $test_secret = 'b185';
	
	private $params = array(
		'merchantid' => '92da70f3a7f4fcc7d7b2323f9c847ea477619bd2', //'gateway_test_3d', //'gateway_test',
		'currency' => 'EUR',
		'language' => 'it',
		'payment_method' => 1
	);

	public function __construct($dynamic_params = array())
	{
		foreach($dynamic_params as $key => $value){
			$this->params[$key] = $value;
		}
		$this->calculateChecksum();
		
	}
	
	public function checkout()
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->checkout_url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->params));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$responseData = curl_exec($ch);
		if(curl_errno($ch)) {
			return curl_error($ch);
		}
		curl_close($ch);
		parse_str($responseData, $output);
		return $output;
	}
	
	public function returnStoreData()
	{
		return [
			'currency' => $this->params['currency'],
			'signuture' => $this->params['signature'],
			'amount' => $this->params['amount'],
			'orderid' => $this->params['orderid']
		];
	}
	
	private function calculateChecksum()
	{
		$signature = '';
		ksort($this->params);
		
		foreach( $this->params as $val )
		{		
			$signature .= $val;
		}
		
		$signature .= $this->secret; 
		$checksum = strtolower( sha1( $signature ) );
		$this->params['signature'] =  $checksum;
		return true;
	}
	
}