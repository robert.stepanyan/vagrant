<?php

class Model_StaticPage extends Cubix_Model
{
	public function getBySlug($slug, $app_id, $lang_id)
	{

		$sql = "SELECT * FROM static_content WHERE slug = ? AND application_id = ? AND lang_id = ?";
		$page = self::getAdapter()->fetchRow($sql, array($slug, $app_id, $lang_id));

		return $page;
	}
}
