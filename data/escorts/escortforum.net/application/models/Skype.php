<?php

class Model_Skype extends Cubix_Model
{
	protected $_table = 'escort_skype';

	const STATUS_ENABLED = 1;
	const STATUS_DISABLED = 0;

	public static $contact_options = array(
		1 => 'whatsapp', 
		2 => 'sms', 
		3 => 'phone_call', 
		4 => 'viber' 
	);

	public function update($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$status = $client->call('Skype.update', array($data));
		
		return $status;
	}

	
}
