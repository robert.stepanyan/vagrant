<?php


class Model_Analytics extends Cubix_Model {
	protected $_table = 'analytics';
	
	
	public static function Insert($data,$user_id,$filter,$user_data,$app_id)
	{
		
		$user_id = self::$_db->quote(intval($user_id));
		$filter = self::$_db->quote($filter);
		$user_data['country'] = self::$_db->quote($user_data['country']);
		$user_data['city'] = self::$_db->quote($user_data['city']);
		
		$data = array_unique($data,SORT_REGULAR);
		$tmp = array();
		$unique = array();
		
		foreach ($data as $item) 
		{
			if (!isset($tmp[$item['name']][$item['value']])) 
			{
				$unique[] = $item;
				$tmp[$item['name']][$tmp['value']] =1;
			}
		}
		$filterArray = array('f_n' => 'Nationality','f_af' => 'AvailableFor', 'f_sf' => 'Service For','f_cs' => 'Cup Size','f_a' => 'Age','f_l' => 'Language','f_e' => 'Ethnicity','f_hc' => 'Hair Color','f_hl' => 'Hair Length','f_h' => 'Height','f_w' => 'Weight','f_ec' => 'Eye Color','f_sm' => 'Smoker', 'f_other'=> 'Other');
		
		$date = date('Y-m-d');
		$insert = 'INSERT INTO analytics(`user_id`,`type`,`name`,`value`,`text_value`,`group`,`ip`,`country`,`city`,`filter`,`year_date`,`lat_long`,`region`,`application_id`) VALUES';
		foreach($unique as $f)
		{
			if(isset($f['group']) &&  array_key_exists($f['group'],$filterArray)){
				$f['group'] = $filterArray[$f['group']];
			}
			
			$insert.="('".$user_id."',";
			$insert.="".self::$_db->quote(trim($f['type'])).",";
			$insert.="".self::$_db->quote(trim($f['name'])).",";
			$insert.="".self::$_db->quote(trim($f['value'])).",";
			$insert.="".self::$_db->quote($f['text_value']).",";
			$f['group'] = isset($f['group']) && $f['group']!=''?$f['group']:$f['text_value'];
			$insert.="".self::$_db->quote($f['group']).",";
			$insert.="".self::$_db->quote($user_data['ip']).",";
			$insert.="".self::$_db->quote($user_data['country']).",";
			$insert.="".self::$_db->quote($user_data['city']).",";
			$insert.="".$filter.",";
			$insert.="'".$date."',";
			$insert.="".self::$_db->quote(serialize(array($user_data['latitude'],$user_data['longitude']))).",";
			$insert.="".self::$_db->quote($user_data['region']).",";
			$insert.="'".$app_id."'),";
		}
		$insert = substr($insert,0,strlen($insert)-1);
		
        try {
            self::$_db->query($insert);
        }
        catch (Exception $ex) {
            // TODO: this is probably deadlock, but check error message to be safe
        }
	}
	
	
}

?>
