<?php

class Model_EcardonGateway
{
	//private $CheckoutUrl =	'https://test.oppwa.com/v1/checkouts';
	private $CheckoutUrl =	'https://oppwa.com/v1/checkouts';
	
	private $TokenParams = array(
		//TEST 'authentication.userId' => '8a90818a1c767c4e011c8eca17be05b4',
		//TEST 'authentication.password' => 'demo',
		//TEST 'authentication.entityId' => '8a90818a1c767c4e011c8eca17be05b5',
		
//		'authentication.userId' => '8a90818a1c767c4e011c8eca17be05b4',
//		'authentication.password' => 'demo',
//		'authentication.entityId' => '8a90818a1c767c4e011c8eca17be05b5',
		'authentication.userId' => '8acda4c95e3caf20015e522758984f79',
		'authentication.password' => 'bPqtQGa2Ej',
		'authentication.entityId' => '8acda4cb5e9e0251015e9ec356c70ce7',
		'amount' => 0,
		'currency' => 'EUR',
		'paymentType' => 'DB',
		'descriptor' => '',
		'merchantTransactionId' => ''
	);

	public function __construct($tokenParams = array())
	{
		foreach($tokenParams as $key => $value){
			$this->TokenParams[$key] = $value;
		}
	}
	
	public function checkout()
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->CheckoutUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->TokenParams));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$responseData = curl_exec($ch);
		if(curl_errno($ch)) {
			return curl_error($ch);
		}
		curl_close($ch);
		
		return $responseData;
	}
	
	public function getStatus($checkout_id)
	{
		$url = $this->CheckoutUrl.'/'.$checkout_id.'/payment';
		$url .= '?authentication.userId='. $this->TokenParams['authentication.userId'];
		$url .= '&authentication.password='. $this->TokenParams['authentication.password'];
		$url .= '&authentication.entityId='. $this->TokenParams['authentication.entityId'];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$responseData = curl_exec($ch);
		if(curl_errno($ch)) {
			return curl_error($ch);
		}
		curl_close($ch);
		return $responseData;
	}
	
	public function saveLog($token, $user_id, $response)
	{
		$db = Zend_Registry::get('db');
		$db->insert('ecardon_payment_statuses', array('token' => $token, 'user_id' => $user_id, 'response' => $response));
	}		
			
}