<?php

class Model_XmasGame extends Cubix_Model
{
    public static $start_paid;
    public static $end_paid;
    public static $end_game;

    public static $simple_start;
    public static $simple_paid;
    public static $simple_game;

    public function __construct(){
        $user = Model_Users::getCurrent();

        self::$start_paid = '21-11-2015';
        self::$end_paid = '07-12-2015';
        self::$end_game = '26-12-2015';

        if( $user->id == 277346 ) {
            self::$simple_start = '02-12-2016';
        } else {
            self::$simple_start = '06-12-2016';
        }

        if( $user->id == 260211 ) {
            self::$simple_paid = '06-12-2016';
        } else {
            self::$simple_paid = '10-12-2016';
        }

        self::$simple_game = '25-12-2016';

        /*if( $user->id == 16558 ){
            self::$start_paid = '21-11-2015';
            self::$end_paid = '01-12-2015';
            self::$end_game = '25-12-2015';
        } else {
            self::$start_paid = '21-11-2014';
            self::$end_paid = '01-12-2014';
            self::$end_game = '24-12-2014';
        }*/
    }

	public function saveEscortVote( $data )
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call( 'Xmas.saveEscortVote', $data );
	}

    public function getByUserId( $user_id )
    {
        $client = new Cubix_Api_XmlRpc_Client();

        $user = Model_Users::getCurrent();

        if( $user->user_type == 'escort' ){
            $escort = $user->escort_data;
            return $client->call( 'Xmas.getUserVotes', array( 'user_id' => $user->id, 'escort_id' => $escort['escort_id'] ) );
        } elseif( $user->user_type == 'agency' ){
            return $client->call( 'Xmas.getUserVotes', array( 'user_id' => $user->id ) );
        }
    }

    public function getSimpleWinStatus(){
        $client = new Cubix_Api_XmlRpc_Client();

        $user = Model_Users::getCurrent();
        return $client->call( 'Xmas.check', array( $user->id, Cubix_I18n::getLang() ) );
    }

    public function sendVote()
    {
        $client = new Cubix_Api_XmlRpc_Client();

        $user = Model_Users::getCurrent();

        if( $user->user_type == 'escort' ){
            $escort = $user->escort_data;
            return $client->call( 'Xmas.checkEscort', array( 'user_id' => $user->id, 'escort_id' => $escort['escort_id'] ) );
        } elseif( $user->user_type == 'agency' ){
            return $client->call( 'Xmas.checkAgency', array( 'user_id' => $user->id ) );
        }

        return false;
    }

    public function setAgree( $user_id )
    {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call( 'Xmas.setTermsAgree', array( 'user_id' => $user_id ) );
    }

    public function getAgree( $user_id )
    {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call( 'Xmas.getTermsAgree', array( 'user_id' => $user_id ) );
    }

    public function getXmasStatus(){
        $current_date = time();
        $start_paid = strtotime(self::$start_paid);
        $end_paid = strtotime(self::$end_paid);
        $end_game = strtotime(self::$end_game);

        if( $current_date >= $start_paid && $current_date <= $end_paid ){
            return 'in_paid';
        } elseif( $current_date > $end_paid && $current_date < $end_game ){
            return 'in_game';
        } else {
            return 'out';
        }
    }

    public static function getSimpleXmasStatus(){
        $current_date = time();
        $start_paid = strtotime( self::$simple_start );
        $end_paid = strtotime( self::$simple_paid );
        $end_game = strtotime( self::$simple_game );

        if( $current_date >= $start_paid && $current_date <= $end_paid ){
            return 'in_paid';
        } elseif( $current_date > $end_paid && $current_date < $end_game ){
            return 'in_game';
        } else {
            return 'out';
        }
    }

    public function getIfExpired( $user_id ){
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call( 'CityVotes.getIfExpired', array( 'user_id' => $user_id ) );
    }

    /*public function getIfSimpleExpired( $user_id ){
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call( 'CityVotes.getIfSimpleExpired', array( 'user_id' => $user_id ) );
    }*/

    public function getServerDates(){
        return array( 'now' => time(), 'end' => strtotime( self::$end_paid ) );
    }

}
