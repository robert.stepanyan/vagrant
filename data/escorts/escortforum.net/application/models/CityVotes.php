<?php

class Model_CityVotes extends Cubix_Model
{
	public function save( $data )
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call( 'CityVotes.save', $data );
	}

    public function getData( $page )
    {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call( 'CityVotes.getData', array( 'lang' => Cubix_I18n::getLang(), 'page' => $page ) );
    }

    public function getCitiesCount(){
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call( 'CityVotes.getCitiesCount', array() );
    }

    public function getIfExpired( $user_id ){
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call( 'CityVotes.getIfExpired', array( 'user_id' => $user_id ) );
    }

    public function getEscortsByCityVotes( $city_id ){
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call( 'CityVotes.getUsersByCityId', array( 'city_id' => $city_id ) );
    }

}
