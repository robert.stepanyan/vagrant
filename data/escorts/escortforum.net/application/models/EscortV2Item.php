<?php

class Model_EscortV2Item extends Cubix_Model_Item
{
	public function __construct($rowData = array())
	{
		parent::__construct($rowData);

		$this->setThrowExceptions(false);
	}

	public function hasProduct($product_id)
	{	
		if ( ! isset($this->products) ) $this->products = parent::getAdapter()->fetchOne('SELECT products FROM escorts WHERE id = ?', $this->id);

		return in_array($product_id, explode(',', $this->products));
	}

    public function isCamAvailable()
    {
        return !empty($this->cam_status) && strtolower($this->cam_status) != 'offline';
    }
    public function isVirtualAvailable()
    {
        $responce = 0;
        $virtual_whatsapp = parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM escort_services WHERE service_id = 78 AND escort_id = ?', $this->id);
        $virtual_skype = parent::getAdapter()->fetchOne('SELECT COUNT(*) FROM escort_services WHERE service_id = 79 AND escort_id = ?', $this->id);

        if($virtual_whatsapp > 0){
            $responce += 1;
        }
        if($virtual_skype > 0){
            $responce += 2;
        }
        return $responce;
    }

	public function hasStatusBit($status)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		return $client->call('Escorts.hasStatusBit', array($this->getId(), $status));
	}
	
	public function setStatusBit($status)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$client->call('Escorts.setStatusBit', array($this->getId(), $status));
	}
	
	public function removeStatusBit($status)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$client->call('Escorts.removeStatusBit', array($this->getId(), $status));
	}
	
	public function getLangs()
	{
		return $this->langs;
		
		return $this->_adapter->query('
			SELECT el.lang_id AS id, el.level, l.title FROM escort_langs el
			LEFT JOIN langs l ON l.id = el.lang_id 
			WHERE el.escort_id = ?
		', $this->getId())->fetchAll();
	}
	
	public function isInFavorites()
	{
		$cache = Zend_Registry::get('cache');
		$cache_key = 'favorites_list_for_user_'. Cubix_Application::getId() . '_' . Model_Users::getCurrent()->id;
		
		if ( ! $res = $cache->load($cache_key) ) {
			$modelM = new Model_Members();
			$res = $modelM->isInFavorites(Model_Users::getCurrent()->id);
			$cache->save($res, $cache_key, array());
		}
		
		return $res;
	}
	
	public function isChatOnline()
	{
		$sql = 'SELECT is_online FROM escorts WHERE id = ?';

		return $this->_adapter->fetchOne($sql, array($this->getId()));
	}
	
	public function getMoreEscorts(array $city_ids, $limit = 10)
	{
		/*$client = new Cubix_Api_XmlRpc_Client();
		$result = $client->call('Escorts.getMoreEscorts', array($this->getId(), $city_ids, $limit));
		
		if ( isset($result['error']) ) {
			print_r($result['error']);
			die;
		}
		
		return $result;*/
		
		$sql = '
			SELECT e.showname FROM escorts e 
			INNER JOIN escort_cities ec ON e.id = ec.escort_id

			/*INNER JOIN escort_profiles ep ON ep.escort_id = e.id
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN applications a ON a.id = u.application_id*/

			/*INNER JOIN countries c ON c.id = e.country_id*/
			/*INNER JOIN cities cc ON cc.id = ec.city_id*/
			
			/*INNER JOIN escort_photos AS eph ON eph.escort_id = e.id AND eph.is_main = 1*/

			WHERE ec.city_id IN (?) AND e.id <> ?

			GROUP BY e.id
			ORDER BY RAND()
			LIMIT ?
		';
		
		$cities = implode(',', $city_ids);
		
		return $this->_adapter->query($sql, array($cities, $this->getId(), $limit))->fetchAll(Zend_Db::FETCH_ASSOC);
	}

	public function getSetcardInfo()
	{
		return (object) $this->getData(array('date_last_modified', 'date_registered', 'hit_count'));
		
		return $this->_adapter->query('
			SELECT e.date_last_modified, e.date_registered, ep.hit_count FROM escorts e
			INNER JOIN users u ON u.id = e.user_id
			INNER JOIN escort_profiles ep ON ep.escort_id = e.id
			WHERE e.id = ?
		', $this->getId())->fetch();
    }

	function getDateByTimeZone($h, $sign, $datum = true, $dst = true) {
		//$sign = "+"; // Whichever direction from GMT to your timezone. + or -
		//$h = "1"; // offset for time (hours)

		if ($dst == true) {
			$daylight_saving = date('I');
			if ($daylight_saving) {
				if ($sign == "-") {
					$h = $h - 1;
				}
				else {
					$h = $h + 1;
				}
			}
		}
		echo $sifn . " " . $h;
		$hm = $h * 60;
		$ms = $hm * 60;
		
		if ($sign == "-") {
			$timestamp = time() - ($ms);
		}
		else { 
			$timestamp = time() + ($ms);
		}
		$gmdate = gmdate("m.d.Y. H:i", $timestamp);
		if($datum == true) {
			return $gmdate;
		}
		else
			return $timestamp;
	}

	public function getIsNowOpen()
	{		
		$is_now_open = $this->_adapter->query('
			SELECT is_now_open
			FROM escorts
			WHERE id = ?
		', array('id' => $this->getId()))->fetch();

		return $is_now_open->is_now_open;
	}
	
	public function getWorkTimes()
	{
		/*
		$time_zone_data = $this->_adapter->query('
			SELECT tz.title AS time_zone_title, tz.shift, tz.php_title
			FROM cities c
			LEFT JOIN time_zones tz ON tz.id = c.time_zone_id
			WHERE c.id = ?
		', array('id' => $this->getBaseCityId()))->fetch();
		
		date_default_timezone_set($time_zone_data->php_title);
		$data['time_zone_data'] = $time_zone_data;
		*/

		$data = $this->getData(array('is_open', 'working_times', 'available_24_7'));
		$working_times = $data['working_times'];
		//var_dump($working_times);
		if ( count($working_times) ) {
			$h = date('G');
			$m = date('i');
			$h = intval($h) * 60 + intval($m);

			$data['is_open'] = false;

			$day_index = date('N', time());
			if ( isset($working_times[$day_index]) && $working_times[$day_index] ) {
				$wt = $working_times[$day_index];
				$wt_from = intval($wt->from) * 60 + intval($wt->from_m);
				$wt_to = intval($wt->to) * 60 + intval($wt->to_m);

				if (
					( $wt->day_index == $day_index && $wt_from <= $h && $wt_to >= $h ) ||
					( $wt->day_index == $day_index && $wt_from <= $h && $wt_from > $wt->to ) 
				) {
					$data['is_open'] = true;
				}
				else {
					$data['is_open'] = false;
				}
			}

			// Previous day
			$day_index = (1 == $day_index ? 7 : $day_index - 1);
			if ( ! $data['is_open'] && isset($working_times[$day_index]) && $working_times[$day_index] ) {
				$wt = $working_times[$day_index];
				$wt_from = intval($wt->from) * 60 + intval($wt->from_m);
				$wt_to = intval($wt->to) * 60 + intval($wt->to_m);

				if ( $wt_to >= $h && $wt_from > $wt_to ) {
					$data['is_open'] = true;
				}
			}
		}

		return $data;
	}
	
	public function getAdditionalCities($base_city_id)
	{
		//return array();
		
		return $this->_adapter->query('
			SELECT ct.' . Cubix_I18n::getTblField('title') . ' AS city_title, ct.id AS city_id, c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, ct.slug AS city_slug FROM cities ct
			INNER JOIN escort_cities ec ON ec.city_id = ct.id
			
			INNER JOIN countries c ON c.id = ct.country_id
			 
			INNER JOIN regions r ON r.id = ct.region_id
			WHERE ec.escort_id = ? AND ec.city_id <> ?
		', array($this->getId(), $base_city_id))->fetchAll();
		
	}

	public function getCityzones()
	{
		return $this->_adapter->query('
			SELECT
				cz.slug AS zone_slug, cz.' . Cubix_I18n::getTblField('title') . ' AS zone_title,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title, ct.id AS city_id,
				c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, ct.slug AS city_slug
			FROM cityzones cz
			INNER JOIN escort_cityzones ecz ON ecz.city_zone_id = cz.id
			INNER JOIN cities ct ON ct.id = cz.city_id
			INNER JOIN countries c ON c.id = ct.country_id
			INNER JOIN regions r ON r.id = ct.region_id
			WHERE ecz.escort_id = ?
		', array($this->getId()))->fetchAll();

	}

	public function  getLocationCoordinates(){
        $client = new Cubix_Api_XmlRpc_Client();
        $escort_location = $client->call('Escorts.getLocationCoordinates', array($this->getId()));
        return $escort_location;
    }

	public function getContacts()
	{
		return (object) $this->getData(array(
			'base_city_en',
			'base_city_it',
			'base_city_gr',
			'base_city_fr',
			'base_city_de',
			'base_city_id',
			'country_en',
			'country_it',
			'country_gr',
			'country_fr',
			'country_de',
			'country_slug',
			'country_iso',
			'region_slug',
			'region_title_en',
			'region_title_it',
			'region_title_gr',
			'region_title_fr',
			'region_title_de',
			'city_slug',
			'vac_date_to',
			'phone_number',
			'phone_number_alt',
			'phone_instr',
			'phone_instr_no_withheld',
			'phone_instr_other',
			'phone_country_id',
			'disable_phone_prefix',
			'club_name',
			'street',
			'street_no',
			'website',
			'email',
			'available_24_7',
			'zip')
		);
		
		$contacts = $this->_adapter->query('
			SELECT ct.' . Cubix_I18n::getTblField('title') . ' AS base_city, ct.id AS base_city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country,
				ep.contact_phone, ep.phone_instructions, cpc.phone_country_id, ep.contact_web, ep.contact_email, ep.contact_zip,
				c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, r.' . Cubix_I18n::getTblField('title') . ' AS region_title, ct.slug AS city_slug, ep.vac_date_to
			FROM escorts e
			INNER JOIN escort_profiles ep ON ep.escort_id = e.id	
			INNER JOIN countries c ON c.id = e.country_id
			INNER JOIN cities ct ON ct.id = e.city_id 
			INNER JOIN regions r ON r.id = ct.region_id
			WHERE e.id = ?
		', $this->getId())->fetch();
		
		return $contacts;
	}

	public function getAdditionalPhones($escort_id)
	{
		$additional_phones = $this->_adapter->query('
			SELECT
				phone_additional_1, phone_additional_2, phone_country_id_1, phone_country_id_2, disable_phone_prefix_1, disable_phone_prefix_2,
				phone_instr_1, phone_instr_2, phone_instr_no_withheld_1, phone_instr_no_withheld_2, phone_instr_other_1, phone_instr_other_2
			FROM escorts
			WHERE id = ?
		', $escort_id)->fetch();
		
		return $additional_phones;
	}

	public function getAgencyWeb()
	{
		return $this->_adapter->query('SELECT a_web, a_block_website FROM escorts WHERE id = ?', $this->getId())->fetch();
	}
	
	public function getCitytours()
	{
		$tours = array();
		/*foreach ( $this->tours as $tour ) {
			$tours[] = (object) $tour;
		}
		print_r($tours);
		return $tours;*/
		
		$tours[] = $this->_adapter->query('
			SELECT e.tour_id AS id, e.tour_date_from AS date_from, e.tour_date_to AS date_to, e.minutes_from, e.minutes_to, e.tour_phone AS phone, c.id AS country_id,
				e.tour_city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country_title,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, ct.slug AS city_slug,
				e.is_on_tour AS is_in_tour,
				0 AS is_upcoming_tour
			FROM escorts e

			INNER JOIN cities ct ON ct.id = e.tour_city_id
			INNER JOIN countries c ON c.id = ct.country_id
			
			INNER JOIN regions r ON r.id = ct.region_id
			WHERE e.id = ?
		', $this->getId())->fetch();

		$up_tours = $this->_adapter->query('
			SELECT ut.tour_id AS id, ut.tour_date_from AS date_from, ut.tour_date_to AS date_to, ut.minutes_from, ut.minutes_to, ut.phone, c.id AS country_id,
				ut.tour_id AS tour_city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country_title,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, ct.slug AS city_slug,
				0 AS is_in_tour,
				1 AS is_upcoming_tour
			FROM escorts e

			INNER JOIN upcoming_tours ut ON ut.id = e.id
			INNER JOIN cities ct ON ct.id = ut.tour_city_id
			INNER JOIN countries c ON c.id = ct.country_id

			INNER JOIN regions r ON r.id = ct.region_id
			WHERE e.id = ?
			ORDER BY ut.tour_date_from
		', $this->getId())->fetchAll();

		/*var_dump($tours);
		var_dump($up_tours);*/

		if ( isset($tours[0]->id) && ! isset($tours[0]->id) ) {
			return $tours;
		}
		else if ( ! isset($tours[0]->id) && isset($up_tours[0]->id) ) {
			return $up_tours;
		}
		else if ( isset($up_tours[0]->id) && isset($tours[0]->id) ) {
			$tours = array_merge($tours, $up_tours);
		}

		if( ! isset($tours[0]->id) && ! isset($up_tours[0]->id) ) {
			return false;
		}

		return $tours;
	}
	
	public function getCitytoursV2()
	{
		$tours = array();
		/*foreach ( $this->tours as $tour ) {
			$tours[] = (object) $tour;
		}
		print_r($tours);
		return $tours;*/
		
		$tours[] = $this->_adapter->query('
			SELECT e.tour_id AS id, UNIX_TIMESTAMP(e.tour_date_from) AS date_from, UNIX_TIMESTAMP(e.tour_date_to) AS date_to, e.minutes_from, e.minutes_to, e.tour_phone AS phone, c.id AS country_id,
				e.tour_city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country_title,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, ct.slug AS city_slug,
				e.is_on_tour AS is_in_tour,
				0 AS is_upcoming_tour
			FROM escorts e

			INNER JOIN cities ct ON ct.id = e.tour_city_id
			INNER JOIN countries c ON c.id = ct.country_id
			
			INNER JOIN regions r ON r.id = ct.region_id
			WHERE e.id = ?
		', $this->getId())->fetch();

		$up_tours = $this->_adapter->query('
			SELECT ut.tour_id AS id, UNIX_TIMESTAMP(ut.tour_date_from) AS date_from, UNIX_TIMESTAMP(ut.tour_date_to) AS date_to, ut.minutes_from, ut.minutes_to, ut.phone, c.id AS country_id,
				ut.tour_id AS tour_city_id, c.' . Cubix_I18n::getTblField('title') . ' AS country_title,
				ct.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				c.slug AS country_slug, c.iso AS country_iso, r.slug AS region_slug, ct.slug AS city_slug,
				0 AS is_in_tour,
				1 AS is_upcoming_tour
			FROM escorts e

			INNER JOIN upcoming_tours ut ON ut.id = e.id
			INNER JOIN cities ct ON ct.id = ut.tour_city_id
			INNER JOIN countries c ON c.id = ct.country_id

			INNER JOIN regions r ON r.id = ct.region_id
			WHERE e.id = ?
			ORDER BY ut.tour_date_from
		', $this->getId())->fetchAll();

		/*var_dump($tours);
		var_dump($up_tours);*/

		if ( isset($tours[0]->id) && ! isset($tours[0]->id) ) {
			return $tours;
		}
		else if ( ! isset($tours[0]->id) && isset($up_tours[0]->id) ) {
			return $up_tours;
		}
		else if ( isset($up_tours[0]->id) && isset($tours[0]->id) ) {
			$tours = array_merge($tours, $up_tours);
		}

		if( ! isset($tours[0]->id) && ! isset($up_tours[0]->id) ) {
			return false;
		}

		return $tours;
	}
	
	public function getAbout()
	{
		return (object) $this->getData(array(
			'showname',
			'nationality_iso',
			'nationality_title_en',
			'nationality_title_it',
			'nationality_title_gr',
			'nationality_title_fr',
			'nationality_title_de',
			'birth_date',
			'eye_color',
			'hair_color',
			'hair_length',
			'height',
			'weight',
			'bust',
			'waist',
			'hip',
			'shoe_size',
			'cup_size',
			'breast_type',
			'dress_size',
			'is_smoking',
			'is_drinking',
			'pubic_hair',
			'tatoo',
			'piercing',
			'measure_units',

			'zip',
			
			'incall_type',
			'incall_hotel_room',
			'incall_other',
			'outcall_type',
			'outcall_other',

			'sex_orientation',
			'sex_availability',
			'ethnicity',
			'about_en',
			'about_it',
			'about_gr',
			'about_fr',
			'about_de',
			'about_soft_en',
			'about_soft_it',
			'about_soft_gr',
			'about_soft_fr',
			'about_soft_de',
			'characteristics',
			'special_rates'
		));
		
		$about = $this->_adapter->query('
			SELECT e.showname, n.iso AS nationality_iso, n.' . Cubix_I18n::getTblField('title') . ' AS nationality_title, FLOOR(DATEDIFF(CURDATE(), ep.birth_date) / 365.25) as age, ep.eye_color,
				ep.hair_color, ep.hair_length, ep.height, ep.weight, ep.bust, ep.waist, ep.hip,
				ep.shoe_size, ep.breast_size, ep.dress_size, ep.is_smoker,
				ep.measure_units, ep.availability, ep.sex_availability, ep.ethnicity,
				ep.' . Cubix_I18n::getTblField('about') . ' AS about, ep.characteristics, ep.special_rates
			FROM escorts e
			LEFT JOIN escort_profiles ep ON ep.escort_id = e.id
			
			LEFT JOIN nationalities n ON n.id = ep.nationality_id
			
			WHERE e.id = ?
		', $this->getId())->fetch();
		
		return $about;
	}	
	
	public function getServicesForPreview()
	{
		$services =  $this->getData(array('services'));
		$services = $services['services'];
		
		foreach ( $services as $k => $service ) {
			$services[$k] = (object) $service;
		}
		
		return $services;
	}
	
	public function getServices()
	{
		/*$datum = (object) $this->getData(array('svc_kissing', 'svc_blowjob', 'svc_cumshot', 'svc_69', 'svc_anal', 'svc_additional'));
		
		if ( $datum->svc_kissing ) {
			return $datum;
		}
		
		if ( $datum->svc_blowjob ) {
			return $datum;
		}
		
		if ( $datum->svc_cumshot ) {
			return $datum;
		}
		
		if ( $datum->svc_69 ) {
			return $datum;
		}
		
		if ( $datum->svc_anal ) {
			return $datum;
		}
		
		if ( $datum->svc_additional ) {
			return $datum;
		}
		
		return false;*/
		
		$services = $this->_adapter->query('
			SELECT service_id, price, currency_id
			FROM escort_services
			WHERE escort_id = ?
			ORDER BY service_id ASC
		', $this->getId())->fetchAll();
				
		return $services;
	}

	public function getAdditionalServices()
	{
		return (object) $this->getData(array(
			'additional_service_en',
			'additional_service_it',
			'additional_service_gr',
			'additional_service_fr',
			'additional_service_de'
		));
	}

	public function getHHRatesInfo()
	{
		$hh = $this->_adapter->query('
			SELECT hh_is_active, showname, UNIX_TIMESTAMP(hh_date_from) AS hh_date_from, UNIX_TIMESTAMP(hh_date_to) AS hh_date_to FROM escorts
			WHERE id = ?
		', $this->getId())->fetch();

		return $hh;
	}

	public function getHHRates()
	{
		$DEFINITIONS = Zend_Registry::get('defines');

		$currencies = $DEFINITIONS['currencies'];
		$time_units = $DEFINITIONS['time_unit_options'];

		$rates = $this->_adapter->query('
			SELECT rates FROM escorts
			WHERE id = ?
		', $this->getId())->fetch();

		if ( is_string($rates->rates) ) {
			$rates = @unserialize($rates->rates);
		}
		
		if ( ! is_array($rates) ) {
			return array();
		}

		foreach ( $rates as $i => $rate ) {
			$rates[$i] = (object) $rate;
		}

		$ret = array();
		foreach ( $rates as $rate ) {
			
			$new_values = array(
				'currency_title'  => $currencies[$rate->currency_id],
				'time_unit_title' => $time_units[$rate->time_unit]
			);
			$rate = (object) array_merge((array) $rate, $new_values);
			@$ret[$rate->availability][] = $rate;
		}

		$ret = $this->_sortRates($ret);
		
		if ( ! in_array(Cubix_Application::getId(), array(APP_EM, APP_PC)) ) {
			unset($ret[""]);
		}

		return $ret;
	}
	
	public function getRates()
	{
		$DEFINITIONS = Zend_Registry::get('defines');
		
		//$currencies = $DEFINITIONS['currencies'];
		$currencies = Model_Currencies::getAllAssoc();
		$time_units = $DEFINITIONS['time_unit_options'];
		
		if ( is_string($this->rates) ) {
			$rates = @unserialize($this->rates);
		}
		else {
			$rates = $this->rates;
		}
		
		if ( ! is_array($rates) ) {
			return array();
		}
		
		foreach ( $rates as $i => $rate ) {
			$rates[$i] = (object) $rate;
		}
		
		$ret = array();
		foreach ( $rates as $rate ) {
			$new_values = array(
				'currency_title'  => $currencies[$rate->currency_id],
				'time_unit_title' => $time_units[$rate->time_unit]
			);
			$rate = (object) array_merge((array) $rate, $new_values);
			@$ret[$rate->availability][] = $rate;
		}

		$ret = $this->_sortRates($ret);

		return $ret;
	}

	protected function _sortRates($rates)
	{
		$ret = array();

		foreach ($rates as $k => $rate)
		{
			$new_array = array();
			$over = false;
			$b_weekend = false;
			$b_dinner = false;
			$b_add_hour = false;

			$overnight = null;
			$add_hour = null;
			$weekend = null;
			$dinner_date = null;

			
			foreach ($rate as $r)
			{
				if ($r->type == 'overnight')
				{
					$overnight = $r;
					$over = true;
				}
				elseif ($r->type == 'additional-hour')
				{
					$add_hour = $r;
					$b_add_hour = true;
				}
				elseif ($r->type == 'weekend')
				{
					$weekend = $r;
					$b_weekend = true;
				}
				elseif ($r->type == 'dinner-date')
				{
					$dinner_date = $r;
					$b_dinner = true;
				}
				elseif ($r->time_unit == TIME_DAYS)
				{
					$new_array[($r->time * 1440)] = $r;
				}
				elseif ($r->time_unit == TIME_HOURS)
				{
					$new_array[($r->time * 60)] = $r;
				}
				elseif ($r->time_unit == TIME_MINUTES)
				{
					$new_array[$r->time] = $r;
				}
				
			}

			ksort($new_array);

			$sorted = array();

			foreach ($new_array as $rate)
			{
				$sorted[] = $rate;
			}

			if ($over)
				$sorted[] = $overnight;
			if ($b_add_hour)
				$sorted[] = $add_hour;
			if ($b_dinner)
				$sorted[] = $dinner_date;
			if ($b_weekend)
				$sorted[] = $weekend;
			$sorted = (object) $sorted;
			
			$av = $rate->availability;
			if ( ! $av ) $av = $rate[0]->availability;
			$ret[$av] = $sorted;
		}
		
		return $ret;
	}
	
	public function getPhotos($page = 1, &$count = null, $with_main = true, $only_privates = false, $config = null, $perPage = null, $new_list = false, $portrait_first = false, $gallery_id = null)
	{
		if(!$config){
            $config = Zend_Registry::get('escorts_config');
        }

        if($perPage){
           $config['photos']['perPage'] = $perPage;
        }
		
		$where = '';
		
		if ( ! $with_main ) {
			$where .= ' AND ep.is_main = 0 ';
		}
		
		if ( $only_privates ) {
			$where .= ' AND ep.type = 3';
		}
		// Only normal photos
		else {
			$where .= ' AND ep.type <> 3 AND ep.type <> 5';
		}
		if ( isset($gallery_id) ) {
			$where .= ' AND ep.gallery_id = '.$gallery_id ;
		}
		/*if ($new_list)
			$where .= ' AND ep.type <> 3';*/

		if ( ! is_null($page) && ($page = intval($page)) < 1 ) $page = 1;
		$limit = '';
		if ( ! is_null($page) && $count ) {
			//$limit = 'LIMIT ' . ($page - 1) * $config['photos']['perPage'] . ', ' . $config['photos']['perPage'];
			$limit = ' LIMIT ' . ($page - 1) * $count . ', ' . $count;
		}
		
		$ordering = " ORDER BY ep.is_main DESC, ep.ordering ASC, ep.id ASC ";
		
		if ( $portrait_first ) {
			$ordering = " ORDER BY ep.is_portrait DESC, ep.is_main DESC, ep.ordering, ep.id ASC ";
		}
		
		$cache = Zend_Registry::get('cache');
		$cache_key = 'escort_photos_v2_' . Cubix_Application::getId() . '_' . $this->getId() . '_' . $page . '_' . $with_main . '_' . $only_privates . '_' . $perPage . '_' . $new_list . '_' . $portrait_first;
		$cache_key_count = $cache_key . '_count';
		
		/*$count = $cache->load($cache_key_count);
		if ( ! $photos = $cache->load($cache_key) ) {*/
		
		$photos = $this->getAdapter()->query("
				SELECT " . Cubix_Application::getId() . " AS application_id, ep.* FROM escort_photos ep
			WHERE ep.escort_id = ? {$where} {$ordering}
			{$limit}
		", $this->getId())->fetchAll();

			foreach ($photos as $i => $photo) {
				$photos[$i] = new Model_Escort_PhotoItem($photo);
			}
			
		$countSql = "
			SELECT FOUND_ROWS();
		";

		$count = $this->getAdapter()->fetchOne($countSql);

		
			/*$cache->save($photos, $cache_key, array());
			$cache->save($count, $cache_key_count, array());
		}*/
		
		return $photos;
	}
	
	public function getPhotosCount()
	{
		$countSql = "
			SELECT COUNT(ep.id) as count 
			FROM escort_photos ep
			/*INNER JOIN escorts e ON e.id = ep.escort_id*/
			WHERE ep.escort_id = ? /*{$where}*/
		";
		
		$count = $this->getAdapter()->query($countSql, $this->getId())->fetch()->count;
		
		return $count;
	}
	
	public function getPhoto($id)
	{
		$photo = $this->_adapter->query('
			SELECT * FROM escort_photos 
			WHERE escort_id = ? AND id = ?
		', array($this->getId(), $id))->fetch();
        $photo->application_id =  $this->application_id;
 
		return new Model_Escort_PhotoItem($photo);
		
		return array();
	}
	
	public function getMainPhoto()
	{
		return new Model_Escort_PhotoItem(array(
			'application_id' => $this->application_id,
			'escort_id' => $this->id,
			'hash' => $this->photo_hash,
			'ext' => $this->photo_ext,
			'photo_status' => $this->photo_status
		));
	}
	
	public function getMobileCover()
	{
		$cover = $this->_adapter->fetchRow('
			SELECT hash, ext FROM escort_photos 
			WHERE escort_id = ? AND is_mobile_cover = 1 AND type NOT IN (3,5) 
		', array($this->id));
		
		if($cover){
			return new Model_Escort_PhotoItem(array(
				'application_id' => $this->application_id,
				'escort_id' => $this->id,
				'hash' => $cover->hash,
				'ext' => $cover->ext,
				
			));
		}
		else{
			return new Model_Escort_PhotoItem(array(
				'application_id' => $this->application_id,
				'escort_id' => $this->id,
				'hash' => $this->photo_hash,
				'ext' => $this->photo_ext,
				'photo_status' => $this->photo_status
			));
		}
	}
	
	public function getVacation()
	{
		$sql = "SELECT vac_date_from, vac_date_to FROM escorts WHERE id = ?";
		
		return $this->getAdapter()->query($sql, $this->getId())->fetch();
	}
	
	public function updateVacation($vac_date_from, $vac_date_to )
	{
		$db = $this->getAdapter();
		$escort_id = $this->getId();
		$db->update('escorts', array('vac_date_from' => $vac_date_from ,'vac_date_to' =>  $vac_date_to), $db->quoteInto('id = ?',$escort_id ));
	}

	public function getVerifyRequest()
	{
		$model = new Model_VerifyRequests();
		
		$item = $model->getByEscortId($this->getId());
		
		return $item;
	}
	
	public function isVerified()
	{
		$sql = "SELECT verified_status FROM escorts WHERE id = ?";

		$status = $this->getAdapter()->query($sql, $this->getId())->fetch();

		if ( $status->verified_status == Model_Escorts::VERIFIED_STATUS_VERIFIED || $status->verified_status == Model_Escorts::VERIFIED_STATUS_VERIFIED_RESET )
		{
			return true;
		}

		return false;
	}

	public function isSuspicious()
	{
		$sql = "SELECT is_suspicious FROM escorts WHERE id = ?";

		$status = $this->getAdapter()->query($sql, $this->getId())->fetch();

		return $status->is_suspicious;
	}
	
	public function getSuspiciousData()
	{
		$sql = "SELECT is_suspicious, suspicious_comment, susp_comment_show FROM escorts WHERE id = ?";

		$data = $this->getAdapter()->query($sql, $this->getId())->fetch();

		return $data;
	}
	
	public function isInAgency($agency_id)
	{
		return $this->agency_id == $agency_id;
	}

	public function vote($member_id)
	{
		if ( ! $this->id ) {
			throw new Exception('ID is required property');
		}

		$client = Cubix_Api::getInstance();
		return $client->call('voteForEscort', array($member_id, $this->id));
	}

	/**
	 * Need this func for ability to serialize this object
	 *
	 * @author GuGo
	 */
	public function __wakeup()
	{
		$this->setAdapter(Model_Escorts::getAdapter());
	}

	public function getProfile()
	{
		$model = new Model_Escorts();

		return $model->getProfile($this->getId());
	}

	public function hasZeroPackage()
	{
		$sql = "SELECT package_id FROM escorts WHERE id = ?";

		$package_id = $this->getAdapter()->fetchOne($sql, $this->getId());

		if ( Cubix_Application::getId() == 2 && $package_id == 97 ) {
			return true;
		}
		else if ( Cubix_Application::getId() == 1 && $package_id == 100 ) {
			return true;
		}

		return false;
	}
	
	public function isDiamond()
	{
		$sql = "SELECT IF (FIND_IN_SET(14, products) = 0, 0, 1) AS is_diamond FROM escorts WHERE id = ?";

		return $this->getAdapter()->fetchOne($sql, $this->getId());
	}
	
	public function getPhotosApi($page = 1, &$count = null, $with_main = true, $portrait_first = false, $gallery_id = null )
	{
		$config = Zend_Registry::get('escorts_config');
	
		$data = array(
			'page' => $page,
			'per_page' => 1000, //$config['photos']['perPage'],
			'with_main' => $with_main,
			'only_normal' => true,
			'escort_id' => $this->getId(),
			'portrait_first' => $portrait_first,
			'gallery_id' => $gallery_id
		);
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$photos = $client->call('Escorts.getPhotos', array($data));
		
		$count = $photos['count'];

		$photos_l = array();
		foreach ($photos['list'] as $i => $photo) {
			$photos_l[$i] = new Model_Escort_PhotoItem($photo);
		}
		
		return $photos_l;
	}
	
	public function getPhotoGalleries($escort_id)
	{
		$galleries =  $this->_adapter->fetchAssoc('
			SELECT id , title 
			FROM escort_galleries
			WHERE escort_id = ?
		', $escort_id);
		
		array_unshift($galleries, array('id' => 0, 'title' => 'Main Gallery'));
		return $galleries;
	}

	public function voteValentine($escort_id, $user_id, $photo_id, $ip){
		$sql = "INSERT into escort_valenines_photo_votes (photo_id, escort_id, user_id, rating, vote_date, ip)
				SELECT * FROM (SELECT ".$photo_id." as photo_id, ".$escort_id." as escort_id, ".$user_id." as user_id, 1 as rating, NOW() as vote_date, '". $ip. "' as ip) as temp where NOT EXISTS (SELECT id from escort_valenines_photo_votes where escort_id=".$escort_id." AND photo_id=".$photo_id." AND user_id=".$user_id.")";
		return $this->_adapter->query($sql);

	}
}
