<?php

class Model_VerifyRequest extends Cubix_Model_Item
{
	public function isPending()
	{
		return $this->status == Model_VerifyRequests::STATUS_PENDING;
	}
	
	public function isConfirmed()
	{
		return $this->status == Model_VerifyRequests::STATUS_CONFIRMED && ( $this->date_1_is_confirmed || $this->date_2_is_confirmed || $this->date_3_is_confirmed );
	}
	
	public function isBlur()
	{
		return $this->status == Model_VerifyRequests::STATUS_BLUR_VERIFY;
	}
	
	public function isIdcard()
	{
		return $this->type == Model_VerifyRequests::TYPE_IDCARD;
	}
	
	public function addPhoto(array $data)
	{
		// $data = array('hash' => '', 'ext' => '', 'comment' => '');
		$data['request_id'] = $this->getId();
		
		$client = new Cubix_Api_XmlRpc_Client();
		
		$client->call('Verification.addPhoto', array($data));
		
		//self::getAdapter()->insert('verify_files', $data);
	}
}
