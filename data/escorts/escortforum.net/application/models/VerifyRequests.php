<?php

class Model_VerifyRequests extends Cubix_Model
{
	protected $_table = 'verify_requests';
	protected $_itemClass = 'Model_VerifyRequest';
	
	const TYPE_WEBCAM = 1;
	const TYPE_IDCARD = 2;
	
	const INFORM_EMAIL = 1;
	const INFORM_PHONE = 2;
	
	const STATUS_PENDING = 1;
	const STATUS_VERIFIED = 2;
	const STATUS_CONFIRMED = 3;
	const STATUS_REJECTED = 4;
	const STATUS_BLUR_VERIFY = 7;
	/**
	 * @param Model_VerifyRequest $request
	 * @return Model_VerifyRequest
	 */
	public function save($request)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		
		$new_id = $client->call('Verification.save', array($request));
		/*print_r($request);
		echo "<br/><br/><br/><br/><br/><br/>";
		$new_id = new Model_VerifyRequest($new_id);
		print_r($new_id);die;*/
		
		$request['id'] = $new_id;
		
		/*parent::save($request);
		$request->setId(self::getAdapter()->lastInsertId());*/
		
		return new Model_VerifyRequest($request);
	}
	
	public function getByEscortId($escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		
		$item = $client->call('Verification.getByEscortId', array($escort_id));
		
		if ( $item )
			$item = new Model_VerifyRequest($item);
		
		return $item;
		//$agency = new Model_AgencyItem($agency);
		
		//return $agency;
		
		/*$item = parent::_fetchRow('
			SELECT 
				*, 
				UNIX_TIMESTAMP(date_1) AS date_1, 
				UNIX_TIMESTAMP(date_2) AS date_2, 
				UNIX_TIMESTAMP(date_3) AS date_3 
			FROM verify_requests 
			WHERE escort_id = ? AND status <> ? AND status <> ?
		', array($escort_id, self::STATUS_REJECTED, self::STATUS_VERIFIED));
		
		return $item;*/
	}
	
	public function getalertEscortIds($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		
		$verification_alert_escorts = $client->call('Agencies.checkVerificationTimeLimit', array($user_id));
		$escort_ids = array();
		foreach($verification_alert_escorts as $escort){
			$escort_ids[] = $escort['id'];
		}
		
		return $escort_ids;
	}
	
	public function getBluredVerifyIds($escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Verification.getBluredPhotos', array($escort_id));
	}
	
	public function needVerification($escort_id){
		$client = new Cubix_Api_XmlRpc_Client();
		$vr = $client->call('Verification.getLastByEscortId', array($escort_id));
		
		$no_need_ver_array = array(
			self::STATUS_PENDING, 
			self::STATUS_VERIFIED
		);
				
		if($vr && in_array($vr['status'], $no_need_ver_array)){
			$need_id_verification = false;
		}
		else{
			$need_id_verification = true;
		}
		
		return $need_id_verification;
	}
	
}
