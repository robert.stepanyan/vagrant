<?php
class Model_MmgBillAPI
{
	 private $host		= 'https://secure.mmgbill.com/';
	 private $uri		= 'process/';
	 private $merchantId = 'M1524';
	 private $secretKey = 'GRBOyQdTHZZn862';
	 private $ts = 'd'; //Stands for dynamic system
	 private $currencyCode = '978';
	 
	 public function __construct()
	 {
		 
	 }
	 
	 public function getHostedPageUrl($amount, $transaction_id, $store_info = false, $postback_url = '')
	 {
		$amount = $amount * 100;
		$c1 = $store_info ? 1 : 0;
		
		
		$hash_string = $this->merchantId . $this->ts . $transaction_id . $this->currencyCode . $amount . $c1 . $postback_url . "address" . "email" . $this->secretKey;
		
		$hash = hash("sha256", $hash_string);
		$hash = bin2hex($hash);
		
		$link = $this->host . $this->uri . 
			'?mid=' . $this->merchantId .
			'&ts=' . $this->ts .
			'&cu=' . $this->currencyCode .
			'&ti=' . $transaction_id .
			'&iac=' . $amount .
			'&email=' .
			'&c1=' . $c1 .
			(strlen($postback_url) ? '&surl=' . $postback_url : '') .
			'&address=' .
			'&sh=' . $hash;

		return $link;
	}
	
	public function charge($amount, $transaction_id, $oct)
	{
		$url = 'https://secure.mmgbill.com/api/oneclick.php';
		$ch = curl_init($url);
		
		
		$amount = $amount * 100;
		$hash_string = $this->merchantId . $this->ts . $transaction_id . $this->currencyCode . $amount . $oct .  $this->secretKey;
		
		$hash = hash("sha256", $hash_string);
		$hash = bin2hex($hash);
		
		$data = array(
			'mid' => $this->merchantId,
			'ts' => $this->ts,
			'cu' => $this->currencyCode,
			'ti' => $transaction_id,
			'iac' => $amount,
			'oct'	=> $oct,
			'sh' => $hash
		);
		
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);
		curl_close($ch);
		
		$response = explode(':', $response);
		
		if ( $response[0] != 1 ) {
			return array('success' => false);
		} else {
			return array('success' => true, 'ti_mmg' => $response[2]);
		}
	}
}

?>
