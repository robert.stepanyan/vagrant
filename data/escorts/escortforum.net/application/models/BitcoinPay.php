<?php

class Model_BitcoinPay
{
	private $CheckoutUrl =	'https://www.bitcoinpay.com/api/v1/payment/btc';
	private $Token = 'kkyd8Oyyp4hQ8NjGDTCbfdEf';
			
	private $TokenParams = array(
		"settled_currency" => "EUR",
		"notify_email" => 'badalyano@gmail.com',
		"price" => 1,
		"currency" => 'EUR',
		"reference" =>  '',
		"item" => "",
		"description" => ''
	);

	public function __construct($tokenParams = array())
	{
		
		foreach($tokenParams as $key => $value){
			$this->TokenParams[$key] = $value;
		}
	}
	
	public function checkout()
	{
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->CheckoutUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->TokenParams));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Content-Type: application/json",
			"Authorization: Token " . $this->Token
		));
		
		$responseData = curl_exec($ch);
		if(curl_errno($ch)) {
			return curl_error($ch);
		}
		curl_close($ch);
		
		return $responseData;
	}
	
	public function getTransaction($payment_id)
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://www.bitcoinpay.com/api/v1/transaction-history/".$payment_id);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  "Content-Type: application/json",
		  "Authorization: Token " . $this->Token
		));
		
		$responseData = curl_exec($ch);
		
		if(curl_errno($ch)) {
			return curl_error($ch);
		}
		curl_close($ch);
		return $responseData;
	}
			
}