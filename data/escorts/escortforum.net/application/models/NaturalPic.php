<?php

class Model_NaturalPic extends Cubix_Model
{
	const STATUS_PENDING = 1;
	const STATUS_VERIFIED = 2;
	const STATUS_REMOVED = 3;
	const STATUS_REJECTED = 4;
	
	public function get($escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		
		$data = $client->call('Verification.getNaturalPic', array($escort_id));
		
		return $data;
	}		
	
	public function save($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		
		$id = $client->call('Verification.saveNaturalPic', array($data));
		
		return $id;
	}
	
	public function remove($escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		
		$id = $client->call('Verification.deleteNaturalPic', array($escort_id));
		
		return $id;
	}
}
