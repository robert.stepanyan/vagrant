<?php

class Model_Comments extends Cubix_Model
{
	protected $_table = 'comments';
	protected $_itemClass = 'Model_CommentItem';

	// comments status
	const COMMENT_ACTIVE = 1;
	const COMMENT_NOT_APPROVED = -3;
	const COMMENT_DISABLED = -4;

	const SITEADMIN_USERNAME = 'siteadmin';

	public function getEscortComments($page = 1, $per_page = 3, &$count = null, $escort_id)
	{
		$limit = '';
		if ( ! is_null($page) ) {
			$limit = ' LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
		}

		$comments = parent::_fetchAll("
			SELECT c.* , r.reviews_count FROM comments c
			LEFT JOIN reviews r ON r.user_id = c.user_id	
			WHERE c.escort_id = ? AND c.is_reply_to = 0
			GROUP BY c.id	
			ORDER BY c.time DESC
			{$limit}
		", array($escort_id));

		foreach ( $comments as $i => $comment )
		{
			$comments[$i]->replied_comment = parent::_fetchAll("
				SELECT c.* , r.reviews_count FROM comments c
				LEFT JOIN reviews r ON r.user_id = c.user_id
				WHERE c.is_reply_to = ?
				GROUP BY c.id
			", array($comment->id));
		}

		$countSql = "
			SELECT COUNT(c.id)
			FROM comments c
			WHERE c.escort_id = ? AND c.is_reply_to = 0
		";

		$count = parent::getAdapter()->fetchOne($countSql, array($escort_id));

		return $comments;
	}

	/*public function getEscortComments($page = 1, $per_page = 10, &$count = null, $escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$comments = $client->call('Comments.getEscortComments', array($page, $per_page, $escort_id));

		$count = $comments['count'];

		foreach ($comments['data'] as $i => $comment)
		{
			$comments['data'][$i] = new Model_CommentItem($comment);
		}

		return $comments['data'];
	}*/

	public function getComment($comment_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$comment = $client->call('Comments.getComment', array($comment_id));
	}

	public function addComment($data)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$comment = $client->call('Comments.addComment', array($data));
	}

	public function vote($type, $comment_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$comment = $client->call('Comments.vote', array($type, $comment_id));

		$data = array();
		if ( $type == 'vote-up' ) {
			$data['thumbup_count'] = new Zend_Db_Expr('thumbup_count + 1');
		}
		else {
			$data['thumbdown_count'] = new Zend_Db_Expr('thumbdown_count + 1');
		}

		parent::getAdapter()->update('comments', $data, parent::quote('id = ?', $comment_id));
	}


	public function getLatestComments($page = 1, $per_page = 10, $count = 3)
	{
		$start = 0;

		if (! is_null($page))
		{
			$start = ($page - 1) * $per_page;
		}

		$return = array();

		$sql = '
			SELECT 
				DISTINCT cm.escort_id, cm.user_id, cm.message, cm.time, ei.ext, ei.hash, ei.showname, cm.is_premium, cm.username, cm.comments_count,
				cm.status, cm.member_rating
            FROM
				(SELECT 
					MAX(c.id) as m, c.escort_id as id, c.photo_ext as ext, c.photo_hash as hash, c.showname
				FROM comments c
				WHERE c.disabled_comments = 0 ' . $where . '
				GROUP BY c.escort_id
				ORDER BY m DESC
				LIMIT ?,?)  as ei
            LEFT JOIN comments as cm ON ei.id = cm.escort_id
		    ORDER BY cm.id DESC
		';

		$countSql = '
            SELECT COUNT(DISTINCT c.escort_id) as count
			FROM comments c
			WHERE c.disabled_comments = 0 ' . $where . '
		';

		$results = parent::_fetchAll($sql, array($start, $per_page));
		$resCount = parent::_fetchRow($countSql);

		if ($results)
		{
			foreach ($results as $comment)
			{
				if (count($return[$comment->escort_id]) >= $count)
				{
					$return[$comment->escort_id][$count - 1]['showmore'] = true;
					continue;
				}

				$return[$comment->escort_id][] = $comment;
			}
		}

		$return['count'] = $resCount->count;

		return $return;
	}

	public function getEscortLatestComments($escort_id, $page = 1, $per_page = 10)
	{
		$start = 0;

		if (!is_null($page))
		{
			$start = ($page - 1) * $per_page;
		}

		$sql = '
			SELECT c.user_id, time,message, c.escort_id, c.is_premium, c.username, c.comments_count, c.status, c.member_rating
			FROM comments c 
            WHERE c.escort_id = ? 
			ORDER BY c.id DESC
			LIMIT ?,? 
		';

		$countSql = '
			SELECT COUNT(c.id) count 
			FROM comments c 
            WHERE c.escort_id = ? 
		';

		$results = parent::_fetchAll($sql, array($escort_id, $start, $per_page));
		$resCount = parent::_fetchRow($countSql, array($escort_id));

		if ($results)
		{
			foreach ($results as $comment)
			{
				$return['comments'][] = $comment;
				$return['count'] = $resCount['count'];
			}
		}

		return $return;
	}

	public function getCommentsByEscortIds($escort_ids, $page = 1,$per_page = 3, &$count = NULL)
	{
		$limit = '';
		if ( ! is_null($page) ) {
			$limit = ' LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page;
		}

		$comments = parent::_fetchAll("
			SELECT SQL_CALC_FOUND_ROWS c.*, c.showname
			FROM comments c
			WHERE c.escort_id IN (".implode(',',$escort_ids).") AND (c.status = ?) AND c.is_reply_to = 0
			ORDER BY c.time DESC
			{$limit}
		", array(self::COMMENT_ACTIVE));

		foreach ( $comments as $i => $comment )
		{
			$comments[$i]->replied_comment = parent::_fetchAll("
				SELECT c.* FROM comments c
				WHERE (c.status = ?) AND c.is_reply_to = ?
			", array(self::COMMENT_ACTIVE, $comment->id));
		}

		$count = parent::getAdapter()->fetchOne('SELECT FOUND_ROWS()');
		return $comments;
	}


	public function getCommentsByUserId($user_id, $page = 1,$per_page = 5, &$count = NULL)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$comments = $client->call('Comments.getCommentsByUserId', array($user_id, $page, $per_page ,$count));
		$count = $comments['count'];
		if ( $count > 0 ){
			foreach ( $comments['data'] as &$comment ){
				$comment['application_id'] = Cubix_Application::getId();
				$comment['photo_status'] = 3;
				$comment = new Model_EscortV2Item($comment);
			}
		}
		return (object)$comments['data'];
	}
}
