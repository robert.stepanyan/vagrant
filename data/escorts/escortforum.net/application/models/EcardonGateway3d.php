<?php

class Model_EcardonGateway3d
{
	private $CheckoutUrl =	'https://oppwa.com/v1/checkouts';
	private $Token = 'OGFjZGE0Yzk1ZTNjYWYyMDAxNWU1MjI3NTg5ODRmNzl8YlBxdFFHYTJFag==';
	//private $Token = 'OGFjN2E0Yzg3MTE3NTllODAxNzExYzIzYTg4YjA5ZjJ8V1IybUhIemQ1NQ==';
	//private $CheckoutUrl =	'https://oppwa.com/v1/checkouts';
	
	private $TokenParams = array(
		//'user_login' => '8ac7a4c8711759e801711c23a88b09f2',
		//'password' => 'WR2mHHzd55',
		'entityId' => '8ac9a4c971e8f85b0171edc8612d37f8',
		'amount' => 0,
		'currency' => 'EUR',
		'paymentType' => 'DB',
		'descriptor' => '',
		'merchantTransactionId' => ''
		
	);

	public function __construct($tokenParams = array())
	{
		foreach($tokenParams as $key => $value){
			$this->TokenParams[$key] = $value;
		}
	}
	
	public function checkout()
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->CheckoutUrl);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                   'Authorization:Bearer '.$this->Token));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->TokenParams));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$responseData = curl_exec($ch);
		if(curl_errno($ch)) {
			return curl_error($ch);
		}
		curl_close($ch);
		
		return $responseData;
	}
	
	public function getStatus($checkout_id)
	{
		$url = $this->CheckoutUrl.'/'.$checkout_id.'/payment';
		$url .= '?entityId='. $this->TokenParams['entityId'];
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                   'Authorization:Bearer '.$this->Token));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$responseData = curl_exec($ch);
		if(curl_errno($ch)) {
			return curl_error($ch);
		}
		curl_close($ch);
		return $responseData;
	}
	
	public function saveLog($token, $user_id, $response)
	{
		$db = Zend_Registry::get('db');
		$db->insert('ecardon_payment_statuses', array('token' => $token, 'user_id' => $user_id, 'response' => $response));
	}		
			
}