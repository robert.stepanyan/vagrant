<?php

class Model_AgeVerification extends Cubix_Model
{
	public function save($request)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		
		$new_id = $client->call('Verification.saveAgeVerification', array($request));
					
		return $new_id;
	}
	
	public function getByEscortId($escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		
		$item = $client->call('Verification.getByEscortId', array($escort_id));
		
		if ( $item )
			$item = new Model_VerifyRequest($item);
		
		return $item;
		
	}
}
