<?php

class Model_Reviews
{
	function reviewsSearch($req, $page, $per_page)
	{
		$db = Zend_Registry::get('db');

		$fields = '';
		$where = '';
		$join = '';

		if (isset($req['showname']) && strlen($req['showname']))
			$where .= " AND r.showname LIKE '%" . $req['showname'] . "%'";

		if (isset($req['gender']) && strlen($req['gender']))
			$where .= " AND r.gender = " . $req['gender'];

		if (isset($req['city']) && strlen($req['city']))
			$where .= " AND r.city = " . $req['city'];

		if (isset($req['trust']) && $req['trust'] == 'fake_free')
			$where .= " AND r.is_fake_free = 1";

		if (isset($req['kissing']) && strlen($req['kissing']))
			$where .= " AND r.s_kissing = " . $req['kissing'];

		if (isset($req['blowjob']) && strlen($req['blowjob']))
			$where .= " AND r.s_blowjob = " . $req['blowjob'];

		if (isset($req['cumshot']) && strlen($req['cumshot']))
			$where .= " AND r.s_cumshot = " . $req['cumshot'];

		if (isset($req['s_69']) && strlen($req['s_69']))
			$where .= " AND r.s_69 = " . $req['s_69'];

		if (isset($req['anal']) && strlen($req['anal']))
			$where .= " AND r.s_anal = " . $req['anal'];

		if (isset($req['looks_from']) && $req['looks_from'] != -1)
			$where .= " AND r.looks_rating >= " . $req['looks_from'];

		if (isset($req['looks_to']) && $req['looks_to'] != -1)
			$where .= " AND r.looks_rating <= " . $req['looks_to'];

		if (isset($req['services_from']) && $req['services_from'] != -1)
			$where .= " AND r.services_rating >= " . $req['services_from'];

		if (isset($req['services_to']) && $req['services_to'] != -1)
			$where .= " AND r.services_rating <= " . $req['services_to'];

		if (isset($req['fuckometer_from']) && $req['fuckometer_from'] != -1)
			$where .= " AND r.fuckometer >= " . $req['fuckometer_from'];

		if (isset($req['fuckometer_to']) && $req['fuckometer_to'] != -1)
			$where .= " AND r.fuckometer <= " . $req['fuckometer_to'];

		if (isset($req['p_date']) && $req['p_date'] == 'other')
		{
			if (isset($req['p_date_type']) && strlen($req['p_date_type']))
			{
				$where .= " AND r.creation_date >= DATE_ADD(NOW(), INTERVAL -" . $req['p_date_type'] . " DAY) ";
			}
		}

		if (isset($req['sex']) && strlen($req['sex']))
			$where .= " AND r.s_sex = " . $req['sex'];

		if (isset($req['multiple_times_sex']) && strlen($req['multiple_times_sex']))
			$where .= " AND r.s_multiple_sex = " . $req['multiple_times_sex'];

		if (isset($req['breast']) && strlen($req['breast']))
			$where .= " AND r.s_breast = " . $req['breast'];

		if (isset($req['attitude']) && strlen($req['attitude']))
			$where .= " AND r.s_attitude = " . $req['attitude'];

		if (isset($req['conversation']) && strlen($req['conversation']))
			$where .= " AND r.s_conversation = " . $req['conversation'];

		if (isset($req['availability']) && strlen($req['availability']))
			$where .= " AND r.s_availability = " . $req['availability'];

		if (isset($req['photos']) && strlen($req['photos']))
			$where .= " AND r.s_photos = " . $req['photos'];

		$sql = "
			SELECT SQL_CALC_FOUND_ROWS
				r.showname, r.escort_id AS id, r.application_id, r.photo_hash, r.photo_ext " . $fields . "
			FROM reviews r
			WHERE r.application_id = ? AND r.status = 2 AND r.is_deleted = 0 " . $where . "
			GROUP BY r.escort_id
			ORDER BY r.creation_date DESC
			LIMIT " . ($page - 1) * $per_page . ", " . $per_page . "
		";

		$countSql = 'SELECT FOUND_ROWS()';

		$items = $db->query($sql, Cubix_Application::getId())->fetchAll(Zend_Db::FETCH_ASSOC);
		$count = $db->fetchOne($countSql);

		return array($items, $count);
	}

	public static function getEscortsSearch($showname)
	{
		$db = Zend_Registry::get('db');

		return $db->query("SELECT showname AS name FROM reviews WHERE showname LIKE '" . $showname . "%' GROUP BY showname ORDER BY showname ASC")->fetchAll(Zend_Db::FETCH_ASSOC);
	}

	public static function getEscortReviews($escort_id, $page, $per_page, $r_id = null)
	{
		$db = Zend_Registry::get('db');
		$where_rid = '';

		if( !is_null( $r_id ) ){
			$where_rid = ' AND r.id = ' . $r_id;
		}

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				r.id, r.escort_id, r.e_fuckometer, r.username, r.looks_rating, r.services_rating, r.is_fake_free,
				c.' . Cubix_I18n::getTblField('title') . ' AS city_title, r.meeting_place, r.duration, r.duration_unit, r.price, r.currency,
				r.fuckometer, r.s_kissing, r.s_blowjob, r.s_cumshot, r.s_69, r.s_anal, r.s_anal, r.s_sex, r.s_attitude, r.s_conversation, r.s_breast,
				r.s_multiple_sex, r.s_availability, r.s_photos, r.services_comments, r.review, UNIX_TIMESTAMP(r.meeting_date) AS meeting_date,
				UNIX_TIMESTAMP(r.creation_date) AS creation_date,
				r.escort_comment, r.is_premium, r.reviews_count, r.disabled_reason, r.status, comm.comments_count, r.showname,r.escort_comment_status
			FROM reviews r
			LEFT JOIN cities c ON c.id = r.city
			LEFT JOIN comments comm ON r.user_id = comm.user_id
			WHERE r.application_id = ?' . $where_rid . ' AND r.status = 2 AND r.is_deleted = 0 AND r.escort_id = ?
			GROUP BY r.id
			ORDER BY r.creation_date DESC
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';


		$countSql = 'SELECT FOUND_ROWS()';

		$items = $db->query($sql, array(Cubix_Application::getId(), $escort_id))->fetchAll(Zend_Db::FETCH_ASSOC);
		$count = $db->fetchOne($countSql);

		return array($items, $count);
	}

	public static function getLastReviewByEscort($escort_id)
	{
		$db = Zend_Registry::get('db');

		$sql = '
			SELECT
				UNIX_TIMESTAMP(r.creation_date) AS creation_date, r.username, r.reviews_count
			FROM reviews r
			WHERE r.application_id = ? AND r.status = 2 AND r.is_deleted = 0 AND r.escort_id = ?
			ORDER BY r.creation_date DESC
			LIMIT 1
		';

		$item = $db->query($sql, array(Cubix_Application::getId(), $escort_id))->fetch(Zend_Db::FETCH_ASSOC);

		return $item;
	}

	public static function getTopLadies($count)
	{
		$db = Zend_Registry::get('db');

		$sql = '
			SELECT
				r.escort_id, r.showname, r.e_fuckometer, r.e_fuckometer_rank, r.agency_name, AVG(looks_rating) AS avg_looks,
				AVG(services_rating) AS avg_services, r.is_premium, r.reviews_count, r.photo_hash AS hash, r.photo_ext AS ext, r.application_id
			FROM reviews r
			WHERE r.application_id = ? AND r.status = 2 AND r.is_deleted = 0
			GROUP BY r.escort_id
			ORDER BY r.e_fuckometer DESC
			LIMIT ?
		';

		$items = $db->query($sql, array(Cubix_Application::getId(), $count))->fetchAll(Zend_Db::FETCH_ASSOC);

		return $items;
	}


	public static function getLastReviewByMember($user_id)
	{
		$db = Zend_Registry::get('db');

		$sql = '
			SELECT
				UNIX_TIMESTAMP(r.creation_date) AS creation_date, r.showname, r.photo_hash AS hash, r.photo_ext AS ext, r.application_id, r.escort_id
			FROM reviews r
			WHERE r.application_id = ? AND r.status = 2 AND r.is_deleted = 0 AND r.user_id = ?
			ORDER BY r.creation_date DESC
			LIMIT 1
		';

		$item = $db->query($sql, array(Cubix_Application::getId(), $user_id))->fetch(Zend_Db::FETCH_ASSOC);

		return $item;
	}

	public static function getTopReviewers($count)
	{
		$db = Zend_Registry::get('db');

		$sql = '
			SELECT
				r.user_id, r.username, UNIX_TIMESTAMP(r.date_registered) AS date_registered, r.showname, UNIX_TIMESTAMP(r.creation_date) AS creation_date,
				r.agency_name, r.is_premium, r.reviews_count
			FROM reviews r
			WHERE r.application_id = ? AND r.status = 2 AND r.is_deleted = 0
			GROUP BY r.user_id
			ORDER BY r.reviews_count DESC
			LIMIT ?
		';

		$items = $db->query($sql, array(Cubix_Application::getId(), $count))->fetchAll(Zend_Db::FETCH_ASSOC);

		return $items;
	}

	public static function getEscortsReviews($page, $per_page, $filter, $sort_field, $sort_dir)
	{
		$db = Zend_Registry::get('db');

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				r.escort_id, r.showname, c.' . Cubix_I18n::getTblField('title') . ' AS city_title,
				r.agency_name, AVG(looks_rating) AS avg_looks, AVG(services_rating) AS avg_services,
				COUNT(r.id) AS reviews
			FROM reviews r
			LEFT JOIN cities c ON c.id = r.e_city_id
			WHERE 1
		';

		$countSql = 'SELECT FOUND_ROWS()';

		$where = ' AND r.application_id = ? AND r.status = 2 AND r.is_deleted = 0';

		if (is_array($filter))
		{
			if (isset($filter['showname']) && strlen($filter['showname']))
				$where .= " AND r.showname LIKE '" . $filter['showname'] . "%'";

			if (isset($filter['country_id']) && strlen($filter['country_id']))
				$where .= ' AND r.e_country_id = ' . $filter['country_id'];
		}

		$sql .= $where;

		$sql .= '
			GROUP BY r.escort_id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$items = $db->query($sql, Cubix_Application::getId())->fetchAll(Zend_Db::FETCH_ASSOC);
		$count = intval($db->fetchOne($countSql));

		return array($items, $count);
	}

	public static function getReviewsCities()
	{
		$db = Zend_Registry::get('db');

		$sql = '
			SELECT
				r.id,
				c.id AS city_id,
				c.' . Cubix_I18n::getTblField('title') . ' AS city_title
			FROM reviews r
			LEFT JOIN cities c ON c.id = r.city
			WHERE r.application_id = ? AND r.status = 2 AND r.is_deleted = 0 AND c.country_id = ?
			GROUP BY c.id
			ORDER BY c.' . Cubix_I18n::getTblField('title') . '
		';

		return $db->query($sql, array(Cubix_Application::getId(), Cubix_Application::getById()->country_id))->fetchAll(Zend_Db::FETCH_ASSOC);
	}

	public static function getReviews($page, $per_page, $filter, $sort_field, $sort_dir)
	{
		$db = Zend_Registry::get('db');

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				r.id, r.escort_id, r.username, UNIX_TIMESTAMP(r.date_registered) AS date_registered, r.showname, UNIX_TIMESTAMP(r.meeting_date) AS meeting_date, UNIX_TIMESTAMP(r.creation_date) AS creation_date, r.looks_rating,
				r.services_rating, r.is_fake_free, c.' . Cubix_I18n::getTblField('title') . ' AS city_title, r.agency_name, r.fuckometer, r.is_premium, r.reviews_count,
				r.photo_hash AS hash, r.photo_ext AS ext, r.application_id, r.status
			FROM reviews r
			LEFT JOIN cities c ON c.id = r.city
			WHERE 1
		';

		$countSql = 'SELECT FOUND_ROWS()';

		$where = ' AND r.application_id = ? AND r.status = 2 AND r.is_deleted = 0';

		if (is_array($filter))
		{
			if (isset($filter['city_id']) && strlen($filter['city_id']))
				$where .= ' AND r.city = ' . $filter['city_id'];

			if (isset($filter['member_name']) && strlen($filter['member_name']))
				$where .= " AND r.username LIKE '" . $filter['member_name'] . "%'";

			if (isset($filter['username']) && strlen($filter['username']))
				$where .= " AND r.username = '" . $filter['username'] . "'";

			if (isset($filter['showname']) && strlen($filter['showname']))
				$where .= " AND r.showname LIKE '" . $filter['showname'] . "%'";

			if (isset($filter['gender']) && strlen($filter['gender']))
				$where .= ' AND r.gender IN (' . $filter['gender'] . ')';
		}

		$sql .= $where;

		$sql .= '
			GROUP BY r.id
			ORDER BY ' . $sort_field . ' ' . $sort_dir . '
			LIMIT ' . ($page - 1) * $per_page . ', ' . $per_page . '
		';

		$items = $db->query($sql, Cubix_Application::getId())->fetchAll(Zend_Db::FETCH_ASSOC);
		$count = intval($db->fetchOne($countSql));

		return array($items, $count);
	}

	public static function getCities($country_id)
	{
		$db = Zend_Registry::get('db');

		return $db->query('
			SELECT c.id, c.' . Cubix_I18n::getTblField('title') . ' AS title
			FROM cities c
			LEFT JOIN countries co ON co.id = c.country_id
			WHERE c.country_id = ' . $country_id . '
			ORDER BY title ASC
		')->fetchAll();
	}

	public static function createCookieClientID($user_id)
	{
		$clientID = "";

		if (!isset($_COOKIE['clientID']))
		{
			$clientID = md5(microtime());
		}
		else
		{
			$clientID = $_COOKIE['clientID'];
		}

		if ($clientID != "")
		{
            $_COOKIE['clientID']=$clientID;
			setcookie("clientID", $clientID, time() + 31536000, "/"); // 31536000 = 60*60*24*365 = one year in seconds

			Cubix_Api::getInstance()->call('cookieClientID', array($user_id, $clientID));
		}
	}

	public function getEscortInfo($escort_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Reviews.getEscortInfo', array($escort_id));
	}
}
