<?php

class Model_InstantBook extends Cubix_Model
{
    public function getInstantBookings($escort_id = 0) 
    {
    	$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('InstantBook.getInstantBookings', array($escort_id));
    }

    public function getHistory($page, $per_page, $filter)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call('InstantBook.getHistory', array($page, $per_page, $filter));
    }

    public function getContactInfo($escort_id)
    {  
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call('InstantBook.getInstantBookContactInfo', array($escort_id));
    }
}