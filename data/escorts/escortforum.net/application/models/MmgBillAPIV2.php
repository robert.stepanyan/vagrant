<?php
class Model_MmgBillAPIV2
{
	private $payment_link = 'https://secure.mmgbill.com/payment/?';
	private $merchantId = ''; 
	private $secret_key = ''; 
	private $ts = 'd'; //Stands for dynamic system
	private $currencyCode = '978';
	 
	public function __construct()
	{
		//$this->merchantId = 'M1572';
		//$this->secret_key = 'qaRxBH6laojLkfkIbbUiRupyJLL0rcfv';
		$this->merchantId = IS_DEBUG ? 'M1572' : 'M1524';
		$this->secret_key = IS_DEBUG ? 'qaRxBH6laojLkfkIbbUiRupyJLL0rcfv' : 'GRBOyQdTHZZn862';
	}
	 
	public function getHostedPageUrl($amount, $transaction_id, $store_info = false, $postback_url = '')
	{
		$amount = $amount * 100;
		$c1 = $store_info ? 1 : 0;
		
		$array_request = array(
			'mid' => $this->merchantId,
			'ts' => $this->ts,
			'ti_mer' => $transaction_id,
			'txn_type' => 'SALE',
			'cu_num' => $this->currencyCode,
			'amc' => $amount,
			'c1' => $c1,
			/*'address' => '',
			'name' => '',
			'email' => ''*/
		);
		
		if(strlen($postback_url) > 0){
			$array_request['surl'] = $postback_url;
		}
		
		/* create the hash signature */
		$sh = $this->create_hash($array_request, $this->secret_key);

		/* create the payment link */
		$payment_link = $this->create_payment_link($array_request, $this->payment_link, $sh);
		
		return $payment_link;
	}
	
	//depricated
	public function charge($amount, $transaction_id, $oct)
	{
		$url = 'https://secure.mmgbill.com/api/oneclick.php';
		$ch = curl_init($url);
		
		
		$amount = $amount * 100;
		$hash_string = $this->merchantId . $this->ts . $transaction_id . $this->currencyCode . $amount . $oct .  $this->secretKey;
		
		$hash = hash("sha256", $hash_string);
		$hash = bin2hex($hash);
		
		$data = array(
			'mid' => $this->merchantId,
			'ts' => $this->ts,
			'cu' => $this->currencyCode,
			'ti' => $transaction_id,
			'iac' => $amount,
			'oct'	=> $oct,
			'sh' => $hash
		);
		
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);
		curl_close($ch);
		
		$response = explode(':', $response);
		
		if ( $response[0] != 1 ) {
			return array('success' => false);
		} else {
			return array('success' => true, 'ti_mmg' => $response[2]);
		}
	}

	/* function to create the hash signature */
	private function create_hash ($array_request, $secret_key)
	{
		$hash_string = "";
		ksort($array_request);
		foreach ($array_request as $key => $value) {
			$hash_string .= "|" . $key . $value;
		}

		return hash_hmac("sha256", $hash_string, $secret_key, false);
	}

	/* function to create payment link */
	private function create_payment_link($array_request, $payment_link, $sh) 
	{
		foreach ($array_request as $key => $value) {
			$payment_link .= $key . "=" . $value . "&";
		}

		return $payment_link .= "sh=" . $sh;
	}
}
