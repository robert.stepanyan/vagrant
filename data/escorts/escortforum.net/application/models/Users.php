<?php

class Model_Users extends Cubix_Model
{
	protected $_table = 'users';
	protected $_itemClass = 'Model_UserItem';
	
	const LOGIN_BLOCKED_IP = 1;
	const LOGIN_BLOCKED_USER = 2;
	const LOGIN_FAIL_ATTEMPT = 3;
	
	public function getAll()
	{
		
	}
	
	public function getDataByUsername($username)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Users.getDataByUsername', array($username));
	}

	public function getBySigninHash($hash)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$user = $client->call('Users.getBySigninHash', array($hash));

		if (!$user) return null;
		
		return new Model_UserItem($user);
	}

	public function removeSigninHash($id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Users.removeSigninHash', array($id));
	}
	
	public function getByUsername($username)
	{
		return $this->_fetchRow('
			SELECT id FROM users WHERE username = ?
		', $username);
	}
	
	public function getByUsernamePassword($username, $password)
	{
		$ip = Cubix_Geoip::getIP();
		
		$client = new Cubix_Api_XmlRpc_Client();
		$user = $client->call('Users.getByUsernamePassword', array($username, $password, $ip));

		if ( is_array($user) && isset($user['error']) ) {
			Cubix_Debug::log(print_r($user['error'], true), 'ERROR');
			die;
		}
		
		if ( ! $user ){
			return null;
		}
		elseif(is_array($user) && isset($user['error_status'])  ){
			return $user;
		}
		
		
		return new Model_UserItem($user);
	}

	public function validateIP()
	{
		$ip = Cubix_Geoip::getIP();

		if (!$ip)
			return false;

		$ranges = array(
			array(
				'range_from' =>  "0.0.0.0",
				'range_to' =>  "0.255.255.255",
			),

			array(
				'range_from' =>  "10.0.0.0",
				'range_to' =>  "10.255.255.255",
			),

			array(
				'range_from' =>  "127.0.0.0",
				'range_to' =>  "127.255.255.255",
			),

			/*array(
				'range_from' =>  "169.254.0.0",
				'range_to' =>  "169.254.255.255",
			),

			array(
				'range_from' =>  "172.16.0.0",
				'range_to' =>  "172.31.255.255",
			),

			array(
				'range_from' =>  "192.0.2.0",
				'range_to' =>  "192.0.2.255",
			),

			array(
				'range_from' =>  "192.88.99.0",
				'range_to' =>  "192.88.99.255",
			),*/

			array(
				'range_from' =>  "192.168.0.0",
				'range_to' =>  "192.168.255.255",
			),

			/*array(
				'range_from' =>  "198.18.0.0",
				'range_to' =>  "198.19.255.255",
			),

			array(
				'range_from' =>  "224.0.0.0",
				'range_to' =>  "239.255.255.255",
			),

			array(
				'range_from' =>  "240.0.0.0",
				'range_to' =>  "255.255.255.255",
			),*/
			
		);


		foreach ( $ranges as $range ){
			$range_from = ip2long($range['range_from']);
			$range_to = ip2long($range['range_to']);
			
			$long = ip2long($ip);

			if ( $long < $range_to && $long >  $range_from ){
				return false;
			}
		}


		return $ip;

	}

	public function getById($id)
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$user = $client->call('Users.getDataById', array($id));

		if ( ! $user ) {
			return null;
		}

		return new Model_UserItem($user);
	}
	
	public function getByEmail($email)
	{
		return $this->_fetchRow('
			SELECT id FROM users WHERE email = ?
		', $email);
	}
	
	public function save(Model_UserItem $item)
	{
		$ip = Cubix_Geoip::getIP();
		
		$hash = md5(microtime() * rand());
		$item->registered_ip = $ip;
		
		$item->setActivationHash($hash);
		$item->setApplicationId(Cubix_Application::getId());
		
		$client = new Cubix_Api_XmlRpc_Client();
		$result = $client->call('Users.save', array( (array) $item ));
		
		if ( isset($result['error']) ) {
			return $result;
		}

		$item->new_user_id = $result;

		//parent::save($item);
		
		return $item;
	}
	
	public function activate($email, $hash)
	{
		$user = Cubix_Api_XmlRpc_Client::getInstance()->call('Users.activate', array($email, $hash));		
		return $user;
	}
	
	public function resetPassword($email)
	{
		$user = Cubix_Api_XmlRpc_Client::getInstance()->call('Users.resetPassword', array($email));

		return $user;
		/*$user = self::_fetchRow('
			SELECT id, username, email FROM users WHERE email = ?
		', $email);
		
		if ( ! $user ) {
			return NULL;
		}
		
		$password = md5(microtime() * rand());
		$password = substr($password, 0, 10);
		
		$user->setPassword(md5($password));
		
		parent::save($user);
		
		$user->setPassword($password);
		
		return $user;*/
	}

	public function forgotPassword($email, $username)
	{
		$user = Cubix_Api_XmlRpc_Client::getInstance()->call('Users.forgotPasswordByEmailUsername', array($email,$username));

		return $user;
	}
	
	/**
	 * @var Zend_Session_Namespace
	 */
	protected static $_session;
	
	/**
	 * @return Zend_Session_Namespace
	 */
	protected static function _getSession()
	{
		return new Zend_Session_Namespace('auth');
	}
	
	public static function setCurrent($user)
	{
		self::_getSession()->current_user = $user;
	}

	/**
	 *
	 * @return Model_UserItem
	 */
	public static function getCurrent()
	{
		return self::_getSession()->current_user;
	}

	public static function setLastRefreshTime($user)
	{
		$is_exist = parent::getAdapter()->fetchOne('SELECT TRUE FROM users_last_refresh_time WHERE user_id = ?', $user->id);

		if ( $is_exist ) {			
            try {
                parent::getAdapter()->update('users_last_refresh_time', array(
                    'refresh_date' => new Zend_Db_Expr('NOW()'),
                    'user_agent' => $_SERVER['HTTP_USER_AGENT'],
                    'session_id' => session_id()
                ), parent::getAdapter()->quoteInto('user_id = ?', $user->id));
            }
            catch (Exception $ex) {
                // TODO: this is probably deadlock, but check error message for deadlock anyway
            }
		}
		else {
			$data = array(
				'user_id' => $user->id,
				'user_agent' => $_SERVER['HTTP_USER_AGENT'],
				'session_id' => session_id()
			);

			if ( $user->user_type == 'agency' ) {
				$data = array_merge($data, array('agency_id' => $user->agency_data['agency_id']));
			}
			else if ( $user->user_type == 'escort' ) {
				$data = array_merge($data, array('escort_id' => $user->escort_data['escort_id']));
			}

            try {
                parent::getAdapter()->insert('users_last_refresh_time', $data);
            }
            catch (Exception $ex) {
                // TODO: this is probably deadlock, but check error message for deadlock anyway
            }
		}
	}

	public function failLoginAttempt($user,$ip)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Users.failLoginAttempt', array($user['id'],$ip));
	}

	/*public static function isOnlineNow($escort_id = null, $agency_id = null)
	{
		$field = " agency_id = ?";
		$param = $agency_id;
		if ( ! is_null($escort_id) ) {
			$field = " escort_id = ?";
			$param = $escort_id;
		}

		$last_refresh_time = parent::getAdapter()->fetchOne("SELECT UNIX_TIMESTAMP(refresh_date) AS refresh_date FROM users_last_refresh_time WHERE {$field}", $param);

		if ( $last_refresh_time ) {
			$to_time = time();
			$from_time = $last_refresh_time;
			$diff_minutes = round(abs($to_time - $from_time) / 60, 0);

			if ( $diff_minutes >= 10 ) {
				return false;
			}
			else {
				return true;
			}
		}

		return false;
	}*/
	
	public static function isOnlineNow($user_id, $escort_id)
	{
		$db2 = Zend_Db::factory('mysqli', array(
			'host' => '172.16.1.31',
			'username' => 'cubix',
			'password' => 'JunKijUnGaND0n',
			'dbname' => 'escortforum_net'
		));

		$sql = "
			SELECT COUNT(DISTINCT users.id)
			FROM users
			JOIN arrowchat_status ON users.id = arrowchat_status.userid 
			WHERE ('" . time() . "' - arrowchat_status.session_time - 60 < 120) AND users.id = {$user_id}
		";
		
		$count = $db2->fetchOne($sql);
		
		if ($count > 0)
		{
			$count2 = parent::getAdapter()->fetchOne("SELECT COUNT(*) FROM escorts WHERE id = ? AND FIND_IN_SET(1, products) > 0", $escort_id);
			
			if ($count2 > 0)
				return true;
		}
		
		return false;
	}
	
	public function checkEasterLottery($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('EasterLottery.check', array($user_id, Cubix_I18n::getLang()));
	}
	
	public function checkXmasLottery($user_id)
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Xmas.check', array($user_id, Cubix_I18n::getLang()));
	}

	public function getUserSalesPerson($userId)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        return $client->call('Users.getUserSalesPerson', array($userId));
    }

    public function getByToken($secure_token)
    {
        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $user = $client->call('Users.getUserByToken', array($secure_token));

        if ( ! $user ) {
            return null;
        }

        return new Model_UserItem($user);
    }

    public function get_last_login_date( $userid ){
        return Cubix_Api_XmlRpc_Client::getInstance()->call('Users.get_last_login_date', array($userid));
    }
	
	public function createFakeEmailForCams($user_id)
	{
		return Cubix_Api_XmlRpc_Client::getInstance()->call('Users.createFakeEmailForCams', array($user_id));
	}

	public function getUserVotes($user_id)
	{
		$data = parent::getAdapter()->fetchAll("SELECT photo_id FROM escort_valenines_photo_votes where user_id = ".$user_id);
		$res = [];
		foreach($data as $k => $v){
			$res[] = $v->photo_id;
		}
		return $res;
	}
}

