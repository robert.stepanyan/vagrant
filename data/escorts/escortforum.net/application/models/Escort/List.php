<?php

class Model_Escort_List extends Cubix_Model
{
	public static function getActiveFilter($filter, $where_query = array(), $is_upcomingtour = false, $is_tour = false)
	{
		$cache = Zend_Registry::get('cache');
		
		if ( isset($filter['e.showname LIKE ?']) ) {
			unset($filter['e.showname LIKE ?']);
		}

		if ( isset($filter['virtual_services']) ) {
			unset($filter['virtual_services']);
		}
		
		if ( isset($filter['video']) ) {
			unset($filter['video']);
		}

        if ( isset($filter['review_count']) ) {
            unset($filter['review_count']);
        }

        if ( isset($filter['verified_status']) ) {
            unset($filter['verified_status']);
        }

        if ( isset($filter['is_vip']) ) {
            unset($filter['is_vip']);
        }
		
		if ( isset($filter['has_nat_pic']) ) {
			unset($filter['has_nat_pic']);
		}
		
		 if ( isset($filter['FIND_IN_SET(20, e.products) > 0']) ) {
            unset($filter['FIND_IN_SET(20, e.products) > 0']);
        }
		
		if ( $is_tour ) {
			if ( isset($filter['eic.gender = 1']) ) unset($filter['eic.gender = 1']);
		
			if ( ! $is_upcomingtour ) {
				$filter[] = 'eic.is_tour = 1';
			}
			else {
				$filter[] = 'eic.is_upcoming = 1';
			}
		} else {
			// Only escorts with base city (or city tour) will be shown in this list
			if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
				$filter[] = 'eic.is_base = 1';
			}
			else {
				$filter[] = 'eic.is_upcoming = 0';
			}
		}

        $join = " ";

        if (isset($filter['e.skype_call_status = ?']) || isset($filter['skype_call_status'])) {
            $join = "
				INNER JOIN escorts e ON e.id = fd.escort_id
			";
            unset($filter['skype_call_status']);
        }

        if (isset($filter['with_virtual_services']) || in_array("e.cam_status != 'offline' ", $filter)) {
            $join = "
				INNER JOIN escorts e ON e.id = fd.escort_id
			";
            $filter[] = "e.cam_status != 'offline' ";
            unset($filter['with_virtual_services']);
        }
		
		$where = self::getWhereClause($filter, true);
		
		$wqa = array();
		if ( count($where_query) ) {
			foreach( $where_query as $wq ) {
				
				if ( strlen($wq['query']) ) {
					$where_query = ' AND ' . $wq['filter_query'];
					
					$where .= $where_query;
					$wqa[] = array('filter_query' => $wq['filter_query'], 'query' => $wq['query'] );
				}
			}
		}
		
		$metadata_cache_key = 'metadata_cache_key_' . Cubix_Application::getId();
		$metadata = $cache->load($metadata_cache_key);
		if ( ! $metadata ) {
			$metadata = self::db()->describeTable('escort_filter_data');
			$cache->save($metadata, $metadata_cache_key, array());
		}		
		
		$column_names = array_keys($metadata);
		unset($column_names[0]);
		
		$select = array();
		foreach ( $column_names as $column ) {
			$select[] = 'SUM(' . $column . ') AS ' . $column;
		}

		if ( isset($filter['cz.slug = ?']) ) {
			$join = "
				INNER JOIN escorts_in_cityzones eicz ON eicz.escort_id = fd.escort_id
				INNER JOIN cityzones cz ON cz.id = eicz.cityzone_id
			";
		}


		$sql = '
			SELECT 
				' . implode(', ', $select) . ' 
			FROM ( SELECT fd.*
			FROM escort_filter_data fd
			INNER JOIN escorts_in_cities eic ON eic.escort_id = fd.escort_id
			LEFT JOIN regions r ON r.id = eic.region_id
			INNER JOIN cities ct ON eic.city_id = ct.id
			' . $join . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY fd.escort_id ) f
		';

		$sql_cache_key = strtolower('filter_row_' . Cubix_Application::getId() . '_' . preg_replace('#=|\.| |\'|\"|\-|\(|\)|\>|!|=|,#', '_', $where));
		$row = $cache->load($sql_cache_key);
		//var_dump($row);
		if ( ! $row ) {
			$row = self::db()->fetchRow($sql);			
			$cache->save($row, $sql_cache_key, array());
			//echo "not cache";
		}
		
		//echo $sql;
		return array('result' => $row, 'global_filter' => $filter , 'where_query' => $wqa, 'is_upcoming' => $is_upcomingtour, 'is_tour' => $is_tour ); 
	}

	public static function getActiveFilterMobileV2( $filter, $where_query = array() )
	{
		$cache = Zend_Registry::get('cache');

		if ( isset($filter['e.showname LIKE ?']) ) {
			unset($filter['e.showname LIKE ?']);
		}

		if ( isset($filter['review_count']) ) {
			unset($filter['review_count']);
		}

		if ( isset($filter['verified_status']) ) {
			unset($filter['verified_status']);
		}

		if ( isset($filter['is_vip']) ) {
			unset($filter['is_vip']);
		}

		if ( isset($filter['has_nat_pic']) ) {
			unset($filter['has_nat_pic']);
		}

		if ( isset($filter['FIND_IN_SET(20, e.products) > 0']) ) {
			unset($filter['FIND_IN_SET(20, e.products) > 0']);
		}


		if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
			$filter[] = 'eic.is_base = 1';
		} else {
			$filter[] = 'eic.is_upcoming = 0';
		}

		$where = self::getWhereClause($filter, true);

		$wqa = array();
		if ( count($where_query) ) {
			foreach( $where_query as $wq ) {

				if ( strlen($wq['query']) ) {
					$where_query = ' AND ' . $wq['filter_query'];

					$where .= $where_query;
					$wqa[] = array('filter_query' => $wq['filter_query'], 'query' => $wq['query'] );
				}
			}
		}

		$metadata_cache_key = 'metadata_cache_key_' . Cubix_Application::getId();
		$metadata = $cache->load($metadata_cache_key);
		if ( ! $metadata ) {
			$metadata = self::db()->describeTable('escort_filter_data');
			$cache->save($metadata, $metadata_cache_key, array());
		}

		$column_names = array_keys($metadata);
		unset($column_names[0]);

		$select = array();
		foreach ( $column_names as $column ) {
			$select[] = 'SUM(' . $column . ') AS ' . $column;
		}

		$join = " ";

		if ( isset($filter['cz.slug = ?']) ) {
			$join .= "
				INNER JOIN escorts_in_cityzones eicz ON eicz.escort_id = fd.escort_id
				INNER JOIN cityzones cz ON cz.id = eicz.cityzone_id
			";
		}

		if( isset($filter['video']) ){
			$join .= " INNER JOIN video v ON v.escort_id = e.id ";
			unset($filter['video']);
		}

		$sql = '
			SELECT
				' . implode(', ', $select) . '
			FROM ( SELECT fd.*
			FROM escort_filter_data fd
			INNER JOIN escorts_in_cities eic ON eic.escort_id = fd.escort_id
			INNER JOIN escorts e ON e.id = fd.escort_id
			LEFT JOIN regions r ON r.id = eic.region_id
			INNER JOIN cities ct ON eic.city_id = ct.id
			' . $join . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY fd.escort_id ) f
		';

		$row = self::db()->fetchRow($sql);

		return array( 'result' => $row );
	}

	public static function getMainPremiumSpotMobile($limit = null){
		$lng = Cubix_I18n::getLang();
		/*-->GOTD LOGIC*/
		$gotd_escort = false;
		$exclude = ' ';

		$gender = GENDER_FEMALE;

		$where = Cubix_Countries::blacklistCountryWhereCase();


		$sqlRandomGOTD = '
			SELECT
				e.id, e.showname, e.age,
				e.hit_count,
				ct.title_' . $lng . ' AS city,
				e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,

				epg.hash AS photo_hash_g,
				epg.ext AS photo_ext_g,
				epg.status AS photo_status_g,

				1 AS is_gotd, e.gotd_city_id,
				e.slogan, e.about_' . $lng . ' AS about
			FROM escorts e
			INNER JOIN cities ct ON ct.id = e.gotd_city_id
			LEFT JOIN escort_photos epg ON epg.escort_id = e.id AND epg.gotd = 1
			WHERE FIND_IN_SET(15, e.products) > 0 AND (e.gender = ' . $gender . ') ' . $where . '
		';

		$gotd_escorts = self::db()->fetchAll($sqlRandomGOTD);
		if ( $gotd_escorts ) {
			shuffle($gotd_escorts);
		}

		if ( $gotd_escorts ) {
			foreach($gotd_escorts as $k => $gotd_escort) {
				$photo_hash_g = $gotd_escort->photo_hash_g;
				$photo_ext_g = $gotd_escort->photo_ext_g;
				$photo_status_g = $gotd_escort->photo_status_g;

				if ( $photo_hash_g ) {
					$gotd_escorts[$k]->photo_hash = $photo_hash_g;
					$gotd_escorts[$k] = $photo_ext_g;
					$gotd_escorts[$k] = $photo_status_g;
				}
			}
		}
		/*<--GOTD LOGIC*/

		return $gotd_escorts;
	}
	
	public static function getMainPremiumSpot($limit = null, $page = null)
	{
		$lng = Cubix_I18n::getLang();
		if(null !== $page){

			if ( $page < 1 ) $page = 1;
			$start = ($limit * ($page - 1));

		}
		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		/*-->GOTD LOGIC*/
		$gotd_escort = false;
		$exclude = ' ';
			
		$gender = GENDER_FEMALE;

		$where = Cubix_Countries::blacklistCountryWhereCase();

		$sqlRandomGOTD = '
			SELECT distinct
				e.id, e.showname, e.age,
				e.hit_count, e.has_video,
				ct.title_' . $lng . ' AS city, e.cam_status,
				e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,

				epg.hash AS photo_hash_g,
				epg.ext AS photo_ext_g,
				epg.status AS photo_status_g,
				e.skype_call_status, e.cam_status,
				1 AS is_gotd, e.gotd_city_id,
				e.slogan, e.about_' . $lng . ' AS about,
				eic.last_bump_date
			FROM escorts e				
			INNER JOIN cities ct ON ct.id = e.gotd_city_id
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
			LEFT JOIN escort_photos epg ON epg.escort_id = e.id AND epg.gotd = 1
			WHERE FIND_IN_SET(15, e.products) > 0 AND (e.gender = ' . $gender . ') ' . $where . '
		';

		
		$gotd_escorts = self::db()->fetchAll($sqlRandomGOTD);

		if ( $gotd_escorts ) {
			shuffle($gotd_escorts);

			//$gotd_escort = $gotd_escort[0];
		}

		if ( $gotd_escorts ) {
			$g_ids = array();
			foreach($gotd_escorts as $k => $gotd_escort) {
				$photo_hash_g = $gotd_escort->photo_hash_g;
				$photo_ext_g = $gotd_escort->photo_ext_g;
				$photo_status_g = $gotd_escort->photo_status_g;

				if ( $photo_hash_g ) {
					$gotd_escorts[$k]->photo_hash = $photo_hash_g;
					$gotd_escorts[$k] = $photo_ext_g;
					$gotd_escorts[$k] = $photo_status_g;
				}
				$g_ids[] = $gotd_escort->id;
				
			}
			$exclude = ' AND e.id NOT IN (' . implode(',', $g_ids) . ') ';
		}
		/*<--GOTD LOGIC*/
		
		
		if ( $gotd_escorts && $limit && ($page === 1 || $page === null)) {
			$limit -= count($gotd_escorts);
		}
		$sql = '
			SELECT
				/* escorts table */
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
				e.rates, e.incall_type, e.outcall_type, e.is_new, e.verified_status,
				e.photo_hash, e.photo_ext, e.photo_status, e.cam_status,
				e.products, e.slogan, e.date_last_modified, e.package_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.is_suspicious,e.comment_count, e.review_count, e.disabled_reviews, e.disabled_comments, e.cam_status,
				eic.last_bump_date,
				ct.title_' . $lng . ' AS city,
				e.skype_call_status,	
				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				/* misc */
				1 AS is_premium, 1 AS application_id, e.hh_is_active, e.is_online, e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified,
				IF (FIND_IN_SET(14, e.products) = 0, 0, 1) AS is_diamond,
				IF (e.package_id IN (' . Model_EscortsV2::goldPackages() . '), 1, 0) AS is_gold,
				IF (e.package_id IN (' . Model_EscortsV2::silverPackages() . '), 1, 0) AS is_silver,	
				0 AS is_login, e.is_vip 
			FROM escorts e
			INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
			INNER JOIN cities ct ON ct.id = eic.city_id
			WHERE
				eic.gender = ' . GENDER_FEMALE . ' AND e.is_main_premium_spot = 1 ' . $exclude . $where . '
			GROUP BY e.id
			ORDER BY eic.last_bump_date DESC, is_diamond DESC

		';
		
//		if( $_GET['test'] == 1 ){ echo $where; die; }
		
			if ( $limit ) {
				if(null !== $page){
					$sql .= '
						LIMIT ' . $start . ', ' . $limit . '
					';
				} else{
					$sql .= " LIMIT " . $limit;
				}
			}
				
		$cache = Zend_Registry::get('cache');
		$cache_key = 'main_premium_spot_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang();
		$cache_key .= Cubix_Countries::blacklistCountryCacheKeyCase();
		
		if ( $limit ) {
			$cache_key .= $cache_key . '_xml_premium_new' . $page;
		}

		if ( ! $escorts = $cache->load($cache_key) ) {
			
			$escorts = self::db()->fetchAll($sql);
			
			/* seo heading escorts */
			/*if ($escorts)
			{
				$es = array();
				foreach ($escorts as $e)
				{
					$es[] = $e->id;
				}
				$es_str = implode(',', $es);

				$query = "SELECT e.id, sei.heading_escort_{$lng} as heading_escort
					FROM escorts e
					INNER JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_base = 1
					INNER JOIN seo_entity_instances sei ON sei.primary_id = eic.city_id
					INNER JOIN seo_entities se ON se.application_id = " . Cubix_Application::getId() . " AND se.slug = 'city' AND sei.entity_id = se.id
					WHERE e.id IN ({$es_str})
				";
				$result = self::db()->fetchAll($query);

				foreach ($result as $r)
				{				
					foreach ($escorts as $e)
					{
						if ($e->id == $r->id)
						{
							$e->heading_escort = $r->heading_escort;
							break;
						}
					}
				}
			}*/
			/**/

			if ( ! $limit ) {
				$cache->save($escorts, $cache_key, array());
			}
			else {
				$cache->save($escorts, $cache_key, array(), 1800);
			}
		}



		//shuffle($escorts);
		if ( $gotd_escorts  && ($page === 1 || $page === null)) {
			$escorts = array_merge($gotd_escorts, $escorts);
		}
		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>
		// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$sess->main_premium_spot = array();
		foreach ( $escorts as $escort ) {
			$sess->main_premium_spot[] = $escort->id;
		}
		$sess->main_premium_spot_criterias = array();
		// </editor-fold>

		return $escorts;
	}
	
	public static function getCityPremiumSpot($filter, $ordering = 'random', $sess_name = false)
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}
		
		// Only escorts with base city (or city tour) will be shown in this list
		if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
			$filter[] = 'eic.is_base = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 0';
		}

        if( isset($filter['is_vip']) ){
            $filter[] = "e.is_vip = 1";
        }

		$has_nat_pic = false;
		if (isset($filter['has_nat_pic'])) {
			$has_nat_pic = true;
			unset($filter['has_nat_pic']);
		}
		
        if(isset($filter['vr'])){
            $filter[] = "( e.verified_status = 2 OR e.verified_status = 3 )";
        }

        if(isset($filter['review_count'])){
            $filter[] = "e.review_count > 0";
        }

		$filter[] = 'eic.is_premium = 1';
		$filter[] = 'eic.is_tour = 0';
		
		
		/*-->GOTD LOGIC*/
		$gotd_escort = false;
		$exclude = ' ';
			
		$in_city = false;
		if ( isset($filter['ct.slug = ?']) ) {
			$in_city = true;
			$city_slug = reset($filter['ct.slug = ?']);
		}

		$gender = GENDER_FEMALE;
		if ( isset($filter['eic.gender = 3']) ) {
			$gender = GENDER_TRANS;
		}
		else if ( isset($filter['eic.gender = 2']) ) {
			$gender = GENDER_MALE;
		}
		
		$j = "";
		if(isset($filter['video'])){
			$j .= " INNER JOIN video v ON v.escort_id = e.id ";
			unset($filter['video']);
		}

		if ($has_nat_pic) {
			$j .= " INNER JOIN natural_pics np ON np.escort_id = e.id ";
		}
		
        /*if(isset($filter['review'])){
            $j .= " INNER JOIN reviews rv ON rv.escort_id = e.id ";
            unset($filter['review']);
        }*/
		
		if ( $in_city ) 
		{
			$g_where = ' AND ct.slug = ? ';
			$g_where .= Cubix_Countries::blacklistCountryWhereCase();
			$bind = array($city_slug);
			$sqlGOTDinCity = '
				SELECT
					e.id, e.showname, e.age,
					e.hit_count,
					ct.title_' . $lng . ' AS city,
					e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,
					e.skype_call_status,	
					epg.hash AS photo_hash_g,
					epg.ext AS photo_ext_g,
					epg.status AS photo_status_g,

					1 AS is_gotd, e.gotd_city_id,
					e.slogan, e.about_' . $lng . ' AS about
				FROM escorts e
				INNER JOIN cities ct ON ct.id = e.gotd_city_id
				LEFT JOIN escort_photos epg ON epg.escort_id = e.id AND epg.gotd = 1
				' . $j . '
				WHERE FIND_IN_SET(15, e.products) > 0 ' . $g_where . ' AND (e.gender = ' . $gender . ' OR e.gender = ' . GENDER_TRANS . ')
			';
			$gotd_escorts = self::db()->fetchAll($sqlGOTDinCity, $bind);

			if ( $gotd_escorts ) {
				shuffle($gotd_escorts);
			}
		}
		else 
		{
			$g_where = Cubix_Countries::blacklistCountryWhereCase();
			$sqlGOTDinCity = '
				SELECT
					e.id, e.showname, e.age,
					e.hit_count,
					ct.title_' . $lng . ' AS city,
					e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,

					epg.hash AS photo_hash_g,
					epg.ext AS photo_ext_g,
					epg.status AS photo_status_g,

					1 AS is_gotd, e.gotd_city_id,
					e.slogan, e.about_' . $lng . ' AS about
				FROM escorts e
				INNER JOIN cities ct ON ct.id = e.gotd_city_id
				LEFT JOIN escort_photos epg ON epg.escort_id = e.id AND epg.gotd = 1
				' . $j . '
				WHERE FIND_IN_SET(15, e.products) > 0 ' . $g_where . ' AND (e.gender = ' . $gender . ' OR e.gender = ' . GENDER_TRANS . ')
			';
			$gotd_escorts = self::db()->fetchAll($sqlGOTDinCity, $bind);

			if ( $gotd_escorts ) {
				shuffle($gotd_escorts);
			}
		}
			
		// Get random GOTD
		/*if ( ! $gotd_escort ) {
			$sqlRandomGOTD = '
				SELECT
					e.id, e.showname, e.age,
					e.hit_count,
					ct.title_' . $lng . ' AS city,
					e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,

					epg.hash AS photo_hash_g,
					epg.ext AS photo_ext_g,
					epg.status AS photo_status_g,

					1 AS is_gotd, e.gotd_city_id
				FROM escorts e				
				INNER JOIN cities ct ON ct.id = e.gotd_city_id
				LEFT JOIN escort_photos epg ON epg.escort_id = e.id AND epg.gotd = 1
				WHERE FIND_IN_SET(15, e.products) > 0 AND (e.gender = ' . $gender . ' OR e.gender = ' . GENDER_TRANS . ')
			';


			$gotd_escort = self::db()->fetchAll($sqlRandomGOTD);

			if ( $gotd_escort ) {
				shuffle($gotd_escort);

				$gotd_escort = $gotd_escort[0];
			}
		}*/
		
		
		if ( $gotd_escorts ) {
			$g_ids = array();
			foreach( $gotd_escorts as $k => $gotd_escort ) {
				$photo_hash_g = $gotd_escort->photo_hash_g;
				$photo_ext_g = $gotd_escort->photo_ext_g;
				$photo_status_g = $gotd_escort->photo_status_g;

				if ( $photo_hash_g ) {
					$gotd_escort[$k]->photo_hash = $photo_hash_g;
					$gotd_escort[$k]->photo_ext = $photo_ext_g;
					$gotd_escort[$k]->photo_status = $photo_status_g;
				}

				$g_ids[] = $gotd_escort->id;
			}
			$filter[66] = ' e.id NOT IN (' . implode(',', $g_ids) . ') ';
		}
		
		
		/*<--GOTD LOGIC*/

		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		is_null($where) ? $where = ' 1 ' . Cubix_Countries::blacklistCountryWhereCase() : $where .= Cubix_Countries::blacklistCountryWhereCase();
		
		$order = str_replace('eic.is_premium', ' eic.last_bump_date desc, IF(eic.is_tour = 1, 0, eic.is_premium)', $order);
		$sql = '
			SELECT
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, 1 AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count,
				e.skype_call_status,
				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				e.hit_count, e.slogan, e.incall_price, e.package_id,
				e.date_last_modified,
				IF(eic.is_tour = 1, 0, eic.is_premium) AS is_premium,
				e.hh_is_active, e.is_suspicious, e.is_online, e.is_on_tour, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city, 
				e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified,
				IF (FIND_IN_SET(14, e.products) = 0, 0, 1) AS is_diamond,
				IF (e.package_id IN (' . Model_EscortsV2::goldPackages() . '), 1, 0) AS is_gold,
				IF (e.package_id IN (' . Model_EscortsV2::silverPackages() . '), 1, 0) AS is_silver,	
				ct.id AS city_id,
				0 AS is_login, e.is_vip 
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
			INNER JOIN regions r ON r.id = eic.region_id
			LEFT JOIN escort_cityzones ecz ON ecz.escort_id = eic.escort_id
			LEFT JOIN cityzones cz ON cz.id = ecz.city_zone_id
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . $j . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
		';
		
		$escorts = self::db()->fetchAll($sql);
		//$count = self::db()->fetchOne('SELECT FOUND_ROWS()');
		if ( $gotd_escorts ) {
			$escorts = array_merge($gotd_escorts, $escorts);
		}
		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		if ( $sess_name ) {
			// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->id;
			}
			$sess->{$sess_name . '_criterias'} = array();
			// </editor-fold>
		}

		return $escorts;
	}

	public static function getFiltered($filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $sess_name = false, $only_regular = false, $where_query = array(), $geo_data = array())
	{
		$lng = Cubix_I18n::getLang();

		if (date('G') < 2)
			$date = 2;
		else
			$date = date('G');

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}
		
		if($ordering == 'date-activated'){
			$filter[] = 'e.id <> 54549';
		}
		// Only escorts with base city (or city tour) will be shown in this list
		if ( ! isset($filter['ct.slug = ?']) && ! isset($filter['r.slug = ?']) ) {
			$filter[] = 'eic.is_base = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 0';
		}

		if ( $only_regular ) {
			$filter[] = 'eic.is_premium = 0';
		}

        if(isset($filter['is_vip'])){
            $filter[] = "e.is_vip = 1";
        }

       
        if(isset($filter['verified_status'])){
             $filter[] = "( e.verified_status = 2 OR e.verified_status = 3 )";
        }

        if(isset($filter['review_count'])){
            $filter[] = "e.review_count > 0";
        }
		
		$has_nat_pic = false;
		if (isset($filter['has_nat_pic'])) {
			$has_nat_pic = true;
			unset($filter['has_nat_pic']);
		}

		if (isset($filter['skype_call_status'])) {
            $filter[] = "e.skype_call_status = 1";
			unset($filter['skype_call_status']);
		}
		//  e.skype_call_status = 1 OR
        if (isset($filter['with_virtual_services'])) {
            $filter[] = "( 
              
                es.service_id = 78 OR 
                es.service_id = 79 OR
                (e.cam_status != 'offline' AND CHAR_LENGTH(e.cam_status) > 0)
            )";
            unset($filter['with_virtual_services']);
        }
        $valentine = null;
        if(isset($filter['valentine'])){
        	$valentine = true;
        	unset($filter['valentine']);
        }
        $where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		
		/*-->GOTD LOGIC*/
		$gotd_escort = false;
		$exclude = ' ';

		$in_city = false;
		if ( isset($filter['ct.slug = ?']) ) {
			$in_city = true;
			$city_slug = reset($filter['ct.slug = ?']);
		} elseif ( isset($filter['cz.slug = ?']) ) {
			$cityzone_slug = reset($filter['cz.slug = ?']);
			$m_city = new Model_Cities();
			$city = $m_city->getCityByCityZone($cityzone_slug);

			$city_slug = $city->slug;
			$in_city = true;
		}

		$gender = GENDER_FEMALE;
		if ( isset($filter['eic.gender = 3']) ) {
			$gender = GENDER_TRANS;
		}
		else if ( isset($filter['eic.gender = 2']) ) {
			$gender = GENDER_MALE;
		}

		if ( $in_city )
		{
			$g_where = ' AND ct.slug = ? ';
			$g_where .= Cubix_Countries::blacklistCountryWhereCase();
			$bind = array($city_slug);
			$sqlGOTDinCity = '
				SELECT
					e.id, e.showname, e.age,
					e.hit_count, e.has_video,
					ct.title_' . $lng . ' AS city,
					e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,

					epg.hash AS photo_hash_g,
					epg.ext AS photo_ext_g, e.cam_status,
					epg.status AS photo_status_g,
					e.skype_call_status,
					1 AS is_gotd, e.gotd_city_id,
					e.slogan, e.about_' . $lng . ' AS about
				FROM escorts e
				INNER JOIN cities ct ON ct.id = e.gotd_city_id
				LEFT JOIN escort_photos epg ON epg.escort_id = e.id AND epg.gotd = 1
				WHERE FIND_IN_SET(15, e.products) > 0 ' . $g_where . ' AND (e.gender = ' . $gender . ' OR e.gender = ' . GENDER_TRANS . ')
			';
			$gotd_escorts = self::db()->fetchAll($sqlGOTDinCity, $bind);

//            if( IS_MOBILE_DEVICE ) {
//                echo '<span style="display: none;">';
//                print_r($gotd_escort);
//                echo '</span>';
//            }
			
			// Get random GOTD. Doesn't work for ef, andy's wish ;)
			/*if ( ! $gotd_escort ) {
				$sqlRandomGOTD = '
					SELECT
						e.id, e.showname, e.age,
						e.hit_count,
						ct.title_' . $lng . ' AS city,
						e.photo_hash, e.photo_ext, e.photo_status, ' . Cubix_Application::getId() . ' AS application_id,

						epg.hash AS photo_hash_g,
						epg.ext AS photo_ext_g,
						epg.status AS photo_status_g,

						1 AS is_gotd, e.gotd_city_id
					FROM escorts e				
					INNER JOIN cities ct ON ct.id = e.gotd_city_id
					LEFT JOIN escort_photos epg ON epg.escort_id = e.id AND epg.gotd = 1
					WHERE FIND_IN_SET(15, e.products) > 0 AND (e.gender = ' . $gender . ' OR e.gender = ' . GENDER_TRANS . ')
				';


				$gotd_escort = self::db()->fetchAll($sqlRandomGOTD);

				if ( $gotd_escort ) {
					shuffle($gotd_escort);

					$gotd_escort = $gotd_escort[0];
				}
			}*/

//			if ( $gotd_escort ) {
//
//				$photo_hash_g = $gotd_escort->photo_hash_g;
//				$photo_ext_g = $gotd_escort->photo_ext_g;
//				$photo_status_g = $gotd_escort->photo_status_g;
//
//				if ( $photo_hash_g ) {
//					$gotd_escort->photo_hash = $photo_hash_g;
//					$gotd_escort->photo_ext = $photo_ext_g;
//					$gotd_escort->photo_status = $photo_status_g;
//				}
//
//				if ( $page == 1 )
//					$page_size = $page_size - 1;
//
//				$count = 1;
//				$exclude = ' AND e.id <> ' . $gotd_escort->id . ' ';
//			}

            if ( $gotd_escorts ) {
                $g_ids = array();
                foreach($gotd_escorts as $k => $gotd_escort) {
                    $photo_hash_g = $gotd_escort->photo_hash_g;
                    $photo_ext_g = $gotd_escort->photo_ext_g;
                    $photo_status_g = $gotd_escort->photo_status_g;

                    if ( $photo_hash_g ) {
                        $gotd_escorts[$k]->photo_hash = $photo_hash_g;
                        $gotd_escorts[$k] = $photo_ext_g;
                        $gotd_escorts[$k] = $photo_status_g;
                    }
                    $g_ids[] = $gotd_escort->id;

                }
                $exclude = ' AND e.id NOT IN (' . implode(',', $g_ids) . ') ';
            }

//            if( IS_MOBILE_DEVICE ) {
//                echo '<span style="display: none;">';
//                print_r($gotd_escorts);
//                echo '</span>';
//            }
		}
		/*<--GOTD LOGIC*/
		
		$start = ($page_size * ($page - 1));
		if ( $gotd_escort && $page > 1 ) {
			$start -= 1;
			unset($gotd_escort);
		}
		
		$limit = $start . ', ' . $page_size;
		
		$j = "";
		$f = "";
		if ( isset($filter['f.user_id = ?']) ) {
			$j = " INNER JOIN favorites f ON f.escort_id = e.id ";
			$f = " , f.user_id AS fav_user_id ";
		}
		
		if(isset($filter['video'])){
			$j .= " INNER JOIN video v ON v.escort_id = e.id ";
			unset($filter['video']);
		}

		if ($has_nat_pic) {
			$j .= " INNER JOIN natural_pics np ON np.escort_id = e.id ";
		}
		
        /*if(isset($filter['review'])){
            $j .= " INNER JOIN reviews rv ON rv.escort_id = e.id ";
            unset($filter['review']);
        }*/

        $nr_grouped = '';
        $fields = ' NULL AS distance ';
        if ( $ordering == 'close-to-me' && count($geo_data) ) {
            if (array_key_exists("latitude", $geo_data) && strlen($geo_data['latitude']) > 0 && array_key_exists("longitude", $geo_data) && strlen($geo_data['longitude']) > 0) {
                $nr_grouped = ', ct.id';
                $fields = '
					((2 * 6371 *
						ATAN2(
						SQRT(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						),
						SQRT(1-(
							POWER(SIN((RADIANS(' . $geo_data['latitude'] . ' - ct.latitude))/2), 2) +
							COS(RADIANS(ct.latitude)) *
							COS(RADIANS(' . $geo_data['latitude'] . ')) *
							POWER(SIN((RADIANS(' . $geo_data['longitude'] . ' - ct.longitude))/2), 2)
						))
						)
					)) AS distance
				';
            }
        }
		if ( count($where_query) ) {
			foreach( $where_query as $wq ) {
				if ( strlen($wq['query']) ) {
					$where_query = ' AND ' . $wq['query'];
					$where .= $where_query;
				}
			}
		}
        if($virtual_services){
            $where .= ' AND (es.service_id = 78 OR es.service_id = 79) ';
        }
		is_null($where) ? $where = ' 1 ' . Cubix_Countries::blacklistCountryWhereCase() : $where .= Cubix_Countries::blacklistCountryWhereCase();

		// there are some girl that have tour in their working city and in that case their bump must be prioritized 
        $order = str_replace('eic.is_premium, eic.last_bump_date DESC,', 'eic.last_bump_date DESC, IF(eic.is_tour = 1, 0, eic.is_premium) DESC, ', $order);

        if ( $ordering == 'close-to-me' && count($geo_data) ) {
            if (array_key_exists("latitude", $geo_data) && strlen($geo_data['latitude']) > 0 && array_key_exists("longitude", $geo_data) && strlen($geo_data['longitude']) > 0) {
                $order = self::_mapSorting($ordering);
            }
        }

		if(isset($valentine) && $valentine){
        	$fields .= ' , ep.id as valentine_photo_id ';
        	// $fields .= ' , COUNT(Distinct evpv.id) AS evpv_count ';
        	$j .= ' LEFT JOIN escort_photos ep ON ep.escort_id = e.id ';
        	// $j .= ' LEFT JOIN escort_valenines_photo_votes evpv on evpv.escort_id = e.id';
        	$where = ' eic.is_base = 1  AND ep.is_valentines = 1 ';
        	// $nr_grouped .= ' ,evpv.escort_id ';
        	$order ='
		        	CASE e.id 
		    WHEN 54506 Then 1
		    WHEN 34153 Then 2
		    WHEN 53703 Then 3
		    WHEN 55980 Then 4
		    WHEN 62150 Then 5
		    WHEN 116809 Then 6
		    ELSE 7
		    END';
        	unset($filter['valentine']);
        }

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
				UNIX_TIMESTAMP(e.date_activated) AS date_activated,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, 1 AS application_id,
				e.incall_currency, e.incall_price, e.outcall_currency, e.outcall_price, e.comment_count, e.review_count, e.disabled_reviews, e.disabled_comments,
				e.cam_status,

				IF (sei.primary_id IS NULL, se.title_' . $lng . ', sei.title_' . $lng . ') AS alt,
				IF (e.available_24_7 = 1 OR (e.is_now_open = 1 AND e.close_time >= "' . $date . '"), 1, 0) AS is_late_night,
				e.hit_count, e.slogan, e.incall_price, e.package_id,
				e.date_last_modified, e.skype_call_status,
				IF(eic.is_tour = 1, 0, eic.is_premium) AS is_premium, eic.last_bump_date,
				e.hh_is_active, e.is_suspicious, e.is_online ' . $f . ', e.is_on_tour, e.tour_date_to, ctt.title_' . $lng . ' AS tour_city,
				e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified,
				IF (FIND_IN_SET(14, e.products) = 0, 0, 1) AS is_diamond, ct.id AS city_id,
				IF (e.package_id IN (' . Model_EscortsV2::goldPackages() . '), 1, 0) AS is_gold,
				IF (e.package_id IN (' . Model_EscortsV2::silverPackages() . '), 1, 0) AS is_silver,	
				0 AS is_login, e.is_vip, ' . $fields . '
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN cities ctt ON ctt.id = e.tour_city_id
			INNER JOIN regions r ON r.id = eic.region_id
			' . $j . '
			LEFT JOIN escort_cityzones ecz ON ecz.escort_id = eic.escort_id
			LEFT JOIN cityzones cz ON cz.id = ecz.city_zone_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se ON (se.id = sei.entity_id) OR (se.application_id = 1 AND se.slug = "escort")
			LEFT JOIN escort_services es ON es.escort_id = e.id
			' . (! is_null($where) ? 'WHERE ' . $where . $exclude : '') . '
			GROUP BY eic.escort_id' . $nr_grouped . '
			ORDER BY  ' . $order . '
			LIMIT ' . $limit . '
		';

        $escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		if ( $gotd_escorts ) {
			$escorts = array_merge( $gotd_escorts, $escorts);
		}
		
		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>

		if ( $sess_name ) {
			// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				$sess->{$sess_name}[] = $escort->id;
			}
			
			$sess->{$sess_name . '_criterias'} = array(
				'filter'	=> $filter,
				'sort'	=> $ordering,
				'page'		=> $page
			);
			// </editor-fold>
		}

		return $escorts;
	}

	public static function getTours($is_upcoming, $filter, $ordering = 'random', $page = 1, $page_size = 50, &$count = 0, $where_query = array())
	{
		$lng = Cubix_I18n::getLang();

		if ( 'price' == substr($ordering, 0, 5) ) {
			$filter[] = 'e.incall_price IS NOT NULL';
		}

		if ( isset($filter['eic.gender = 1']) ) unset($filter['eic.gender = 1']);
		
		if ( ! $is_upcoming ) {
			$filter[] = 'eic.is_tour = 1';
		}
		else {
			$filter[] = 'eic.is_upcoming = 1';
		}
		$j = '';

        if (isset($filter['has_nat_pic'])) {
            $j .= " INNER JOIN natural_pics np ON np.escort_id = e.id ";
            unset($filter['has_nat_pic']);
        }

        if(isset($filter['skype_call_status'])){
            $filter[] = 'e.skype_call_status = 1';
            unset($filter['skype_call_status']);
        }
        //   e.skype_call_status = 1 OR
        if (isset($filter['with_virtual_services'])) {
            $filter[] = "( 
             
                es.service_id = 78 OR 
                es.service_id = 79 OR
                (e.cam_status != 'offline' AND CHAR_LENGTH(e.cam_status) > 0)
            )";
            unset($filter['with_virtual_services']);
        }

        if(isset($filter['video'])){
            $j .= " INNER JOIN video v ON v.escort_id = e.id ";
            unset($filter['video']);
        }


		$where = self::getWhereClause($filter, true);
		$order = self::_mapSorting($ordering);

		$order = str_replace('is_diamond DESC,', '', $order);
		if ( $ordering == 'random' ) {
			$order = 'eic.is_premium DESC,(CASE WHEN FIND_IN_SET(14, e.products) then 0 ELSE 1 END), eic.ordering DESC';
		}

		$page = (int) $page;
		if ( $page < 1 ) $page = 1;
		$limit = ($page_size * ($page - 1)) . ', ' . $page_size;
		
		if ( count($where_query) ) {
			foreach( $where_query as $wq ) {
				if ( strlen($wq['query']) ) {
					$where_query = ' AND ' . $wq['query'];
					$where .= $where_query;
				}
			}
		}

		is_null($where) ? $where = ' 1 ' . Cubix_Countries::blacklistCountryWhereCase() : $where .= Cubix_Countries::blacklistCountryWhereCase();

		$sql = '
			SELECT SQL_CALC_FOUND_ROWS
				e.id, e.showname, e.age, UNIX_TIMESTAMP(e.date_registered) AS date_registered, e.user_id,
				ct.title_' . $lng . ' AS city, e.rates, e.incall_type, e.outcall_type, e.is_new, e.cam_status,
				e.verified_status, e.photo_hash, e.photo_ext, e.photo_status, 1 AS application_id,
				IF (sei.primary_id IS NULL, IF(se1.id IS NOT NULL, se1.title_' . $lng . ', se2.title_' . $lng . '), sei.title_' . $lng . ') AS alt,
				e.hit_count, e.slogan, e.incall_price, e.comment_count, e.review_count, e.disabled_reviews, e.disabled_comments,
				e.date_last_modified, e.package_id, e.skype_call_status, e.cam_status,
				eic.is_premium, e.is_online, e.about_' . $lng . ' AS about, UNIX_TIMESTAMP(e.date_last_modified) AS date_last_modified
				' .
				(! $is_upcoming ? ', e.tour_date_from, e.tour_date_to' : // if
				($is_upcoming ? ', ut.tour_date_from, ut.tour_date_to' : '')) // else
				. ',
				IF (FIND_IN_SET(14, e.products) = 0, 0, 1) AS is_diamond,
				IF (e.package_id IN (' . Model_EscortsV2::goldPackages() . '), 1, 0) AS is_gold,
				IF (e.package_id IN (' . Model_EscortsV2::silverPackages() . '), 1, 0) AS is_silver,
				0 AS is_login, e.is_vip 
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			LEFT JOIN seo_entity_instances sei ON sei.primary_id = eic.escort_id
			LEFT JOIN seo_entities se1 ON se1.id = sei.entity_id
			LEFT JOIN seo_entities se2 ON se2.slug = "escort" AND se2.application_id = 1
			LEFT JOIN escort_services es ON es.escort_id = e.id
			'.$j.'
			' . ($is_upcoming ? 'INNER JOIN upcoming_tours ut ON ut.id = eic.escort_id AND ut.tour_city_id = eic.city_id AND ut.tour_date_from <= CURDATE() + INTERVAL 7 DAY ' : '') . '
			' . (! is_null($where) ? 'WHERE ' . $where : '') . '
			GROUP BY eic.escort_id
			ORDER BY ' . $order . '
			LIMIT ' . $limit . '
		';
		
		$escorts = self::db()->fetchAll($sql);
		$count = self::db()->fetchOne('SELECT FOUND_ROWS()');

		// <editor-fold defaultstate="collapsed" desc="Make Escort Alt for SEO">
		$tpl_data = array(
			'app_title' => Cubix_Application::getById()->title
		);

		foreach ( $escorts as $i =>  $escort ) {
			$additional = array(
				'showname' => $escort->showname,
				'city' => $escort->city
			);

			foreach ( $tpl_data + $additional as $tpl_var => $tpl_value ) {
				$escorts[$i]->alt = str_replace('%' . $tpl_var . '%', $tpl_value, $escorts[$i]->alt);
			}
		}
		// </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
		$sid = 'sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);
		
		if ( ! $is_upcoming ) {
			$filter_name = 'tours_list';
			$sess->tours_list = array();
			foreach ( $escorts as $escort ) {
				$sess->tours_list[] = $escort->id;
			}
		}
		else {
			$filter_name = 'up_tours_list';
			$sess->up_tours_list = array();
			foreach ( $escorts as $escort ) {
				$sess->up_tours_list[] = $escort->id;
			}
		}
		
		$sess->{$filter_name . '_criterias'} = array(
			'is_upcoming'	=> $is_upcoming,
			'filter'		=> $filter,
			'sort'		=> $ordering,
			'page'			=> $page
		);
		// </editor-fold>

		return $escorts;
	}

	private static function _mapSorting($param)
	{

		$map = array(
            'close-to-me' => 'distance ASC, ct.id, eic.is_premium DESC, eic.ordering DESC',
			'price-asc' => 'e.incall_price ASC',
			'price-desc' => 'e.incall_price DESC',
			'random' => 'is_diamond DESC, is_gold DESC, is_silver DESC,  eic.is_premium, eic.last_bump_date DESC, eic.ordering DESC',
			'alpha' => 'e.showname ASC',
            'most-viewed' => 'e.hit_count DESC',
			'newest' => 'e.is_new DESC, e.date_activated DESC',
			'date-activated' => 'e.date_activated DESC',
			'last-modified' => 'e.date_last_modified DESC',
			'city-asc' => 'ct.slug ASC'
		);
		
		$order = 'e.ordering DESC';
		if ( isset($map[$param]) ) {
			$order = $map[$param];
		}
		elseif ( false !== strpos($param, 'FIELD') ) {
			$order = $param;
		}

		return $order;
	}

	public static function getPrevNext($showname)
	{
		return array(null, null);
		// Retrieve criterias from session
		$sid = 'v2_sedcard_paging_' . Cubix_Application::getId();
		$sess = new Zend_Session_Namespace($sid);

		$page = $sess->page;
		$page_size = $sess->page_size;
		$pages_count = $sess->pages_count;

		list($filter, $ordering) = $sess->criteria;
		foreach ( $sess->escorts as $i => $name ) {
			if ( $name == $showname ) {
				$prev_i = $i - 1;
				$next_i = $i + 1;

				break;
			}
		}

		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);

		if ( ! isset($prev_i) && ! isset($next_i) ) {
			return array(null, null);
		}

		// This is first escort, we need to fetch data from prev page
		if ( isset($prev_i) && (! isset($sess->escorts[$prev_i]) || $prev_i < 0) && $page > 1 ) {
			$sess->escorts = array_slice($sess->escorts, 0, $page_size * 2);

			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page - 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($escorts, $sess->escorts);
			
			$prev_i = $page_size - 1;
		}

		
		echo "Escorts after 'first' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);
		
		// If this last escort on page, we need to fetch data from next page
		if ( isset($next_i) &&  (! isset($sess->escorts[$next_i]) || $next_i > count($sess->escorts) - 1 ) && $page < $pages_count ) {
			if ( count($sess->escorts) >= $page_size * 3 ) {
				$sess->escorts = array_slice($sess->escorts, $page_size, $page_size * 2);
				$next_i -= $page_size;
			}
			
			$escorts = call_user_func_array(
				array(__CLASS__, $sess->callback),
				array($filter, $ordering, $page + 1, $page_size)
			);
			foreach ( $escorts as $i => $escort ) {
				$escorts[$i] = $escort->showname;
			}

			$sess->escorts = array_merge($sess->escorts, $escorts);
		}

		echo "Escorts after 'last' logic\n";
		echo "Prev_i: $prev_i, Next_i: $next_i\n";
		print_r($sess->escorts);
		
		return array(isset($prev_i) ? $sess->escorts[$prev_i] : null, isset($next_i) ? $sess->escorts[$next_i] : null);
	}

	//escort type ( 0 = ALL, 1 - premium, 2 regular)
	public static function getCityEscortsCount($city, $escort_type = 0 )
	{
		$where_type = "";
		if( $escort_type == 1){
			$where_type = ' AND eic.is_premium = 1 AND eic.is_tour = 0 ';
		}
		elseif($escort_type == 2){
			$where_type = ' AND eic.is_premium = 0';
		}

		$where_type .= Cubix_Countries::blacklistCountryWhereCase();

		$sql = '
			SELECT COUNT(DISTINCT(eic.escort_id))
			FROM escorts_in_cities eic
			INNER JOIN escorts e ON e.id = eic.escort_id
			INNER JOIN cities ct ON ct.id = eic.city_id
			INNER JOIN regions r ON r.id = eic.region_id
			WHERE eic.is_upcoming = 0 AND e.gender = ' . GENDER_FEMALE. ' AND ct.slug = "'. $city.'"'.$where_type. '
		';
		
		return self::db()->fetchOne($sql);
	}
	
	
	public static function getProfileBoosts($city_slug = null)
	{
		$lng = Cubix_I18n::getLang();
		
		$cache = Zend_Registry::get('cache');
		$cache_key = 'ef_boost_profiles_' . ($city_slug ? $city_slug : 'index') . '_' . $lng;
		$cache_key .= Cubix_Countries::blacklistCountryCacheKeyCase();
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
		
		$boosts = $cache->load($cache_key);
		
		if ( $boosts !== false ) {
			return $boosts;
		}

		$where = Cubix_Countries::blacklistCountryWhereCase();

		$sql = '
			SELECT 
				e.showname, e.id,
				if (ep2.id, ep2.hash, ep.hash) AS photo_hash,
				if (ep2.id, ep2.ext, ep.ext) AS photo_ext,
				if (ep2.id, ep2.status, ep.status) AS photo_status,
				ct.title_' . $lng . ' AS boost_city, 
				ct2.title_' . $lng . ' AS premium_city
			FROM profile_boosts pb
			INNER JOIN escorts e ON e.id = pb.escort_id
			LEFT JOIN escorts_in_cities eic ON eic.escort_id = e.id AND eic.is_premium = 1
			LEFT JOIN cities ct2 ON ct2.id = eic.city_id
			LEFT JOIN cities ct ON ct.id = pb.city_id
			LEFT JOIN escort_photos ep ON ep.escort_id = e.id AND ep.is_main = 1
			LEFT JOIN escort_photos ep2 ON ep2.escort_id = e.id AND ep2.profile_boost = 1
			WHERE DATE(pb.activation_date) = DATE(NOW()) AND pb.hour_from = HOUR(NOW()) AND ' . ($city_slug ? ' ct.slug = "' . $city_slug . '"' : 'pb.city_id IS NULL') . $where .'
			GROUP BY e.id
			LIMIT 5';
		
		$boosts = self::db()->fetchAll($sql);
		
		$cache->save($boosts, $cache_key);
		
		return $boosts;
	}
    public static function getLatestVideos($city_id)
    {
		$geo_where = Cubix_Countries::blacklistCountryWhereCase();
        if($city_id){
            $sql = '
				SELECT e.id, e.showname, e.age, vi.hash, vi.ext, ' . Cubix_Application::getId() . ' AS application_id
				FROM escorts_in_cities eic
				INNER JOIN escorts e ON e.id = eic.escort_id
				INNER JOIN video v ON v.escort_id = e.id
				INNER JOIN video_image vi ON vi.video_id = v.id
				WHERE e.has_video = 1 AND eic.city_id = ? '. $geo_where.' 
				GROUP BY eic.escort_id
				ORDER BY eic.ordering DESC
				LIMIT 5
			';

            $videos = self::db()->fetchAll($sql, $city_id);
            $video_count = count($videos);

            if($video_count < 5){
                $sql_add = '
				SELECT e.id, e.showname, e.age, vi.hash, vi.ext, ' . Cubix_Application::getId() . ' AS application_id
				FROM escorts_in_cities eic
				INNER JOIN escorts e ON e.id = eic.escort_id
				INNER JOIN video v ON v.escort_id = e.id
				INNER JOIN video_image vi ON vi.video_id = v.id
				WHERE e.has_video = 1 AND eic.city_id <> ? '. $geo_where.' 
				GROUP BY eic.escort_id
				ORDER BY eic.ordering DESC
				LIMIT '. (5 - $video_count);

                $additional_videos = self::db()->fetchAll($sql_add, $city_id);
                $videos = array_merge($videos, $additional_videos);
            }
        }
        else{
            $sql = '
				SELECT e.id, e.showname, e.age, vi.hash, vi.ext, ' . Cubix_Application::getId() . ' AS application_id
				FROM escorts_in_cities eic
				INNER JOIN escorts e ON e.id = eic.escort_id
				INNER JOIN video v ON v.escort_id = e.id
				INNER JOIN video_image vi ON vi.video_id = v.id
				WHERE e.has_video = 1 '. $geo_where.' 
				GROUP BY eic.escort_id
				ORDER BY eic.ordering DESC
				LIMIT 5
			';
            $videos = self::db()->fetchAll($sql);
        }

        foreach($videos as $video){
            $photo = new Cubix_ImagesCommonItem($video);
            $video->photo = $photo->getUrl('video_thumb');

        }

        return $videos;
    }
}
