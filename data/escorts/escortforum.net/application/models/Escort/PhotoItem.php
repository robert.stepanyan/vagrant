<?php

class Model_Escort_PhotoItem extends Cubix_Model_Item
{
	/**
	 * The image utility library
	 *
	 * @var Cubix_Images
	 */
	protected static $_images;
	
	protected $_app_id = null;
	
	public function __construct($rowData = array())
	{
		parent::__construct($rowData);
		
		if ( empty(self::$_images) ) {
			self::$_images = new Cubix_Images();
		}
	}

	public function reorder($i)
	{
		$this->_adapter->query('UPDATE escort_photos SET ordering = ? WHERE id = ?', array($i, $this->getId()));
	}

	public function make($type = ESCORT_PHOTO_TYPE_HARD, $is_susp = 0)
	{
		if ($is_susp && $type == ESCORT_PHOTO_TYPE_ARCHIVED)
		{
			return;
		}

		Cubix_Api::getInstance()->call('makeEscortPhoto', array($this->getId(), $type));
		$this->_adapter->query('UPDATE escort_photos SET type = ? WHERE id = ?', array($type, $this->getId()));

		$escort_id = $this->_adapter->fetchOne('SELECT escort_id FROM escort_photos WHERE id = ?', $this->getId());
		if ( 1 > $this->_adapter->fetchOne('SELECT COUNT(*) FROM escort_photos WHERE escort_id = ? AND is_main = 1 AND type <> 3 AND type <> 5', $escort_id) ) {
			$this->_adapter->query('UPDATE escort_photos SET is_main = 1 WHERE escort_id = ? AND type <> 3 AND type <> 5 LIMIT 1', $escort_id);
		}
		$this->_adapter->query('UPDATE escort_photos SET is_main = 0 WHERE escort_id = ? AND (type = 3 OR type = 5)', $escort_id);
	}

	public function toJSON($size_name)
	{
		return array(
			'id' => $this->getId(),
			'escort_id' => $this->escort_id,
			'image_url' => $this->getUrl($size_name),
			'type' => $this->type,
			// 'is_verified' => $this->is_verified
		);
	}

	public function getHash()
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Escorts.getHash', array($this->getId()));
	}
	
	public function getHashExt()
	{
		$client = new Cubix_Api_XmlRpc_Client();
		return $client->call('Escorts.getHashExt', array($this->getId()));
	}
	
	public function getUrl($size_name = null, $from_server = false, $show_args = true)
	{
		$data = array(
			'application_id' => $this->application_id,
			'catalog_id' => $this->escort_id,
			'hash' => $this->hash,
			'ext' => $this->ext
		);
		
		if ( $size_name )
		{
			$data['size'] = $size_name;
		}
		
		$args = isset($this->args) ? unserialize($this->args) : null;
		
		$add = '';
		if ($show_args && $args ) {
			$add  = '?args=' . $args['x'] . ':' . $args['y'];
		}
		if ( $from_server )
			return self::$_images->getServerUrl(new Cubix_Images_Entry($data)) . $add;
		else
			return self::$_images->getUrl(new Cubix_Images_Entry($data)) . $add;
	}

	public function setMain()
	{
		$user = Model_Users::getCurrent();
		
		$client = new Cubix_Api_XmlRpc_Client();
		$result = $client->call('Escorts.setMainPhoto', array($this->getId(), $user->sign_hash));
		
		if ( true !== $result ) return array('error' => 'An error occured when setting main photo');
		
		self::getAdapter()->query('UPDATE escorts e INNER JOIN escort_photos ep ON ep.escort_id = e.id SET e.photo_hash = ep.hash, e.photo_ext = ep.ext, e.photo_status = ep.status WHERE ep.id = ?', $this->getId());
		
		$this->getAdapter()->query('
			UPDATE escort_photos ep
			INNER JOIN escort_photos ep1 ON ep.escort_id = ep1.escort_id
			SET ep.is_main = FALSE
			WHERE ep1.id = ?
		', $this->getId());
		
		self::getAdapter()->query('UPDATE escort_photos SET is_main = TRUE WHERE id = ?', $this->getId());
		
		return true;
	}

    public function setValentines()
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $result = $client->call('Escorts.setValentinesPhoto', array($this->getId()));

        if ( $result['error'] ) return array('error' => __($result['error']));

        if ( true !== $result ) return array('error' => 'An error occured when setting main photo');

        self::getAdapter()->query('UPDATE escorts e INNER JOIN escort_photos ep ON ep.escort_id = e.id SET e.photo_hash = ep.hash, e.photo_ext = ep.ext, e.photo_status = ep.status WHERE ep.id = ?', $this->getId());

        $this->getAdapter()->query('
			UPDATE escort_photos ep
			INNER JOIN escort_photos ep1 ON ep.escort_id = ep1.escort_id
			SET ep.is_valentines = 0
			WHERE ep.escort_id = ?
		', $this->escort_id);

        self::getAdapter()->query('UPDATE escort_photos SET is_valentines = 1 WHERE id = ?', $this->getId());

        return true;
    }

	public function setMobileCover()
	{
		$user = Model_Users::getCurrent();
		
		$client = new Cubix_Api_XmlRpc_Client();
		$result = $client->call('Escorts.setMobileCover', array($this->getId(), $user->sign_hash));
		
		if ( true !== $result ) return array('error' => 'An error occured when setting main photo');
		
		$this->getAdapter()->query('
			UPDATE escort_photos ep
			INNER JOIN escort_photos ep1 ON ep.escort_id = ep1.escort_id
			SET ep.is_mobile_cover = 0
			WHERE ep1.id = ?
		', $this->getId());
		
		self::getAdapter()->query('UPDATE escort_photos SET is_mobile_cover = 1 WHERE id = ?', $this->getId());
		
		return true;
	}
	
	public function setRotatePics($pic_ids = NULL)
	{
		$where = '';
		if(!is_null($pic_ids)){
			$where = " AND id IN (".implode(',' ,$pic_ids).") ";  
		}
		$client = new Cubix_Api_XmlRpc_Client();
		$result = $client->call('Escorts.setRotatePhotos', array($this->escort_id, $pic_ids));
		
		if ( true !== $result ) return $result;
		
		self::getAdapter()->query('UPDATE escort_photos SET is_rotatable = 0 WHERE escort_id = ?', array($this->escort_id));
		self::getAdapter()->query('UPDATE escort_photos SET is_rotatable = 1 WHERE escort_id = ?' .$where, array($this->escort_id));
		
		return true;
	}
	
	public function setCropArgs($args)
	{
		$args = serialize($args);
		
		$client = new Cubix_Api_XmlRpc_Client();
		
		$result = $client->call('Escorts.setPhotoCropArgs', array($this->getId(), $args));
		
		if ( true !== $result ) return $result;
		
		self::getAdapter()->query('UPDATE escort_photos SET args = ? WHERE id = ?', array($args, $this->getId()));
		
		return true;
	}
	
	public function isMain()
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$result = $client->call('Escorts.isMainPhoto', array($this->getId()));
		
		return $result;
	}
	public function setNotApproved()
	{
		$client = new Cubix_Api_XmlRpc_Client();
		
		$result = $client->call('Escorts.setNotApprovedPhoto', array($this->getId()));
		
		$db = $this->getAdapter();
		$db->update('escort_photos', array('is_approved' => 0),$db->quoteInto('id = ?', $this->getId()));
		
		return true;
		
	}		
}
