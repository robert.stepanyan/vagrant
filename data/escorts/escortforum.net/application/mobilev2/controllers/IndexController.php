<?php

class Mobilev2_IndexController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_helper->layout->setLayout('mobile-index');
        if(!$this->_getParam('escort_id')){
            $_SESSION['request_url'] = $_SERVER['REQUEST_URI'];
        }
       
		$this->view->static_page_key = 'main';
		$this->user = Model_Users::getCurrent();
        	//die('Mobile version is temporary unavailable');http://m.6annonce.com/remove-from-favorites?=11490
	}

    public function indexAction()
	{
        $gender = GENDER_FEMALE;
        $is_agency = null;
        
        $query = '';
        $where = null;
        if($this->_getParam('q')){
            $query = ltrim($this->_getParam('q'), '/');
            $where = array('ct.slug LIKE ?' => '%'.$query.'%');
        }

        $all_cities = Model_Statistics::getCities(null, $gender, $is_agency, null, false, $where);
        //if(!$this->_getParam('all')){
            $this->view->cities = array_slice($all_cities, 0, 24);
            $rest_cities = array_slice($all_cities, 24, count($all_cities));
            $this->view->more_cities = $rest_cities;
        //}else{
            //$this->view->cities = $all_cities;
            //$this->view->is_all = true;
        //}
        $this->view->query = $query;

        $this->view->menuHome = 1;
        $this->view->total_count = Model_Statistics::getTotalCount($gender, $is_agency, null, false);

        
        $req = $this->_getParam('req');

		$config = Zend_Registry::get('mobile_config');
		

		// Scroll Spy
		//var_dump($this->_request);
		if ( $this->_request->ajax ) {
			$this->view->spy = true;
			$this->view->layout()->disableLayout();
		}

		
		if (preg_match('#/([0-9]+)$#', $req, $a)) {
			$a[1] = intval($a[1]);
			$req = preg_replace('#/[0-9]+$#', '/page_' . $a[1], $req);
		}

		$req = explode('/', $req);

		if ( isset( $req[2] )  ){
			if ( $req[2] == 'upcomingtours' || $req[2] == 'citytours' ){
				$tc = explode('_',$req[1]);
				array_shift($tc);
				$tc = implode('_', $tc);

				if ( $req[2] == 'upcomingtours' ){
					$link = $this->view->getLink('escorts', array('upcomingtours', 'city' => strtolower($tc), 'filter' => null, 'page' => null) );
				}

				if ( $req[2] == 'citytours' ){
					$link = $this->view->getLink('escorts', array('tours', 'city' => strtolower($tc), 'filter' => null, 'page' => null) );
				}

				header('HTTP/1.1 301 Moved Permanently');
				header('Location: '.$link);
				die;
				return;
			}
		}

		$params = array(
			'sort' => 'random',
			'filter' => array(array('field' => 'girls','is_premimum' => 1, 'value' => array())),
			'page' => 1
		);

		$static_page_key = 'main';

		foreach ($req as $r) {
			if (!strlen($r))
				continue;

			$param = explode('_', $r);

			
			if ( isset($param[2]) ){
				preg_match('/[A-Z]/', $param[2], $matches);
				if ( count ( $matches ) > 0  ){
					$link = strtolower($this->view->getLink('escorts'));
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: '.$link);
					die;
					return;
				}
			}

			if (count($param) < 2) {
				switch ($r) {
					case 'nuove':
						//$params['filter'][] = array('field' => 'new_arrivals', 'value' => array());
						$static_page_key = 'nuove';
						$param = array('sort', 'newest');
						break;
					case 'independantes':
						$static_page_key = 'independantes';
						$params['filter'][] = array('field' => 'independantes', 'value' => array());
						continue;
					case 'regular':
						$static_page_key = 'regular';
						$params['filter'][] = array('field' => 'regular', 'value' => array());
						continue;
					case 'agence':
						$static_page_key = 'agence';
						$params['filter'][] = array('field' => 'agence', 'value' => array());
						continue;
					case 'boys':
						$static_page_key = 'boys';
						$params['filter'][0] = array('field' => 'boys', 'value' => array());
						continue;
					case 'trans':
						$static_page_key = 'trans';
						$params['filter'][0] = array('field' => 'trans', 'value' => array());
						continue;
					case 'citytours':
						$static_page_key = 'citytours';
						$this->view->is_tour = $is_tour = true;
						$params['filter'][] = array('field' => 'tours', 'value' => array());
						continue;
					case 'upcomingtours':
						$static_page_key = 'upcomingtours';
						$this->view->is_tour = $is_tour = true;
						$this->view->is_upcomingtour = true;
						$upcoming_tours = true;
						$params['filter'][] = array('field' => 'upcomingtours', 'value' => array());
						continue;
					/* 					case 'blank.html':
					  $this->_forward('blank-html', 'redirect');
					  return; */
					default:
						$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
						$this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
						return;
				}
			}

			$param_name = $param[0];
			array_shift($param);
			$this->view->static_page_key = $static_page_key;

			switch ($param_name) {
				case 'filter':
					$has_filter = true;
					/* >>> For nested menu */
					$selected_item = $menus['filter']->getByValue(implode('_', $param));
					if (!is_null($selected_item)) {
						$menus['filter']->setSelected($selected_item);
					}
					/* <<< */

					$field = reset($param);
					array_shift($param);

					$value = array_slice($param, 0, 2);

					$params['filter'][] = array('field' => $field, 'value' => $value, 'main' => TRUE);
					break;
				case 'page':
					$page = intval(reset($param));

					if ($page < 1) {
						$page = 1;
					}

					$params['page'] = $page;
					break;
				case 'sort':
					$params['sort'] = reset($param);

					$selected_item = $menus['sort']->getByValue($params['sort']);
					if (!is_null($selected_item)) {
						$menus['sort']->setSelected($selected_item);
					}
					$has_filter = true;
					break;
				case 'region':
				case 'state':
					$params['country'] = $param[0];
					$params['region'] = $param[1];
					$params['filter'][] = array('field' => 'region', 'value' => $param[1]);
					break;
				case 'city':
				case 'zone':
					$params[$param_name] = $param[1];
					$params['filter'][] = array('field' => $param_name, 'value' => array($param[1]));
					break;
				case 'name':
					$has_filter = true;
					$params['filter'][] = array('field' => 'name', 'value' => array('%' . $param[0] . '%'));
					break;
				default:
					if (!in_array($param_name, array('nuove', 'independantes', 'regular', 'agence', 'boys', 'trans', 'citytours', 'upcomingtours'))) {
						$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
						$this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
						return;
					}
			}
		}


		$filter_params = array(
			'order' => 'e.date_registered DESC',
			'limit' => array('page' => 3),
			'filter' => array()
		);

		$filter_map = array(
			'verified' => 'e.verified_status = 2',
			'french' => 'e.nationality_id = 15',
			'age' => 'e.age < ? AND e.age > ?',
			'ethnic' => 'e.ethnicity = ?',
			'height' => 'e.height < ? AND e.height > ?',
			'weight' => 'e.weight < ? AND e.weight > ?',
			'cup-size' => 'e.cup_size = ?',
			'hair-color' => 'e.hair_color = ?',
			'hair-length' => 'e.hair_length = ?',
			'eye-color' => 'e.eye_color = ?',
			'dress-size' => 'e.dress_size = ?',
			'shoe-size' => 'e.shoe_size = ?',
			'available-for-incall' => 'e.incall_type IS NOT NULL',
			'available-for-outcall' => 'e.outcall_type IS NOT NULL',
			'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
			'smoker' => 'e.is_smoking = ?',
			'language' => 'FIND_IN_SET(?, e.languages)',
			'now-open' => 'e.is_now_open',
			'region' => 'r.slug = ?',
			'city' => 'ct.slug = ?',
			'cityzone' => 'c.id = ?',
			'zone' => 'cz.slug = ?',
			'name' => 'e.showname LIKE ?',
			'new_arrivals' => 'e.gender = ' . GENDER_FEMALE,
			'independantes' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
			'agence' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE,
			'boys' => 'eic.gender = ' . GENDER_MALE,
			'trans' => 'eic.gender = ' . GENDER_TRANS,
			'girls' => 'eic.gender = ' . GENDER_FEMALE,
			'tours' => 'eic.is_tour = 1',
			'upcomingtours' => 'eic.is_upcoming = 1'
		);

		$params['filter'] = array(array('field' => 'girls','is_premimum' => 1, 'value' => array()));

		foreach ($params['filter'] as $i => $filter) {

			if (!isset($filter_map[$filter['field']]))
				continue;

			$value = $filter['value'];

			if (isset($filter['main'])) {
				if (isset($selected_filter->internal_value)) {
					$value = $selected_filter->internal_value;
				} elseif (!is_null($item = $menus['filter']->getByValue($filter['field'] . ( (isset($value[0]) && $value[0]) ? '_' . $value[0] : '')))) {
					$value = $item->internal_value;
				}
			}

			$filter_params['filter'][$filter_map[$filter['field']]] = $value;
		}

		$page = intval( $this->_getParam('page') );

		if ($page == 0) {
			$page = 1;
		}

		$filter_params['limit']['page'] = $page;
		$count = 0;

		if (isset($params['city'])) {
			$this->view->city = $params['city'];
		}

		$cache = Zend_Registry::get('cache');

		//$escorts = Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], $config['escorts']['perPage'], $count, 'regular_list');
		$main_list_cache_key = strtolower('mobile_main_list');
		$cache_escorts = $cache->load($main_list_cache_key);

		if( $cache_escorts ){
			$escorts = $cache_escorts;
		} else {
			$escorts = Model_Escort_List::getMainPremiumSpot();
			$cache->save($escorts, $main_list_cache_key, array());
		}

		if($this->view->gotm){
			$escorts = array_slice( $escorts, ( $page - 1 ) * $config['escorts']['perPage'], $config['escorts']['perPage'] -1 );
		}else{
			$escorts = array_slice( $escorts, ( $page - 1 ) * $config['escorts']['perPage'], $config['escorts']['perPage'] );
		}

//		foreach ( $escorts as $i => $escort ) {
//			if ($escort->is_premium && $i != 0) {
//				$premiums[] = $escort;
//			}
//		}
//		$this->view->premiun_spot = Model_Escort_List::getMainPremiumSpotMobile();
//		$escorts = array_slice( $escorts, ( $page - 1 ) * $config['escorts']['perPage'], $config['escorts']['perPage'] );

		if ( count( $escorts ) > 0 ){
			// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
			$sess_name = "prev_next";
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ( $escorts as $escort ) {
				//$sess->{$sess_name}[] = $escort->showname;
				$sess->{$sess_name}[] = $escort->id;
			}
			// </editor-fold>
		}
		$res = Model_EscortsV2::getInstantBookEscortsList(1, 1000);
		$this->view->instantbook_escorts = $res['ids'];
		
		$count_city_escorts = Model_Escort_List::getCityEscortsCount($params['city']);
		
		$this->view->is_zone = ( $params['zone'] ) ? true : false;
		$this->view->zone_title = '';
		$this->view->city_count = $count_city_escorts;
		$this->view->params = $params;
		$this->view->escorts = $escorts;

		$this->view->page = $page;
		$this->view->count = $count;
	}


	public function girlOfMonthAction()
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$gotm = $client->call('getCurrentGOTM', array(Cubix_I18n::getLang()));
		$this->view->gotm = $gotm ? (object) $gotm : false;
		$this->_forward('index', "index");
		return;
	}


    public function showVideoAction(){
        if ( $this->_request->ajax ) {
            $this->view->layout()->disableLayout();
        }


    }
	

    private function _orderTitles($a,$b){
        if ( is_object($a) && is_object($b) ) {
			return strnatcmp($a->city_title, $b->city_title);
		}
		elseif ( is_array($a) && is_array($b) ) {
			return strnatcmp($a["city_title"], $b["city_title"]);
		}
		else return 0;
    }

    public function searchAction()
	{
		if ( $this->_request->ajax ) {
			$this->view->layout()->disableLayout();
		}

        $this->view->defines = Zend_Registry::get('defines');

     
        $all_cities = Model_Statistics::getCities();
        $config = Zend_Registry::get('mobile_config');
        $all_zones = Model_Statistics::getZones();


        usort($all_cities, array(self, "_orderTitles"));

        $this->view->menuSearch = 1;
        $this->view->cities = $all_cities;
        $this->view->zones = $all_zones;
        

        if( $this->_getParam('search') )
		{
			$params['filter'] = array();
            $params['order'] = array();

			$this->view->params = $this->_request->getParams();
            
            if( $this->_getParam('order') ) {

                $order_direction = '';
                $order_field = 'date_registered';

                switch($this->_getParam('order')){
                    case '1':
                       $order_field = "date_last_modified";
                       break;
                    case '2':
                       $order_field = "e.showname ASC";
                       break;
                   case '3':
                       $order_field = "e.hit_count DESC";
                       break;
                   case '4':
                       $order_field = "e.is_new DESC";
                       break;
                   case '5':
                       $order_field = "e.date_last_modified DESC";
                       break;
                   case '6':
                       $order_field = "e.outcall_price ASC";
                       break;
                   case '7':
                       $order_field = "e.outcall_price DESC";
                       break;
                }

                if($this->_getParam('order_direction')){
                   $order_direction = $this->_getParam('order_direction');
                }
                $params['order'] = $order_field." ".$order_direction;

			}


            if($this->_getParam('page')){
                $params['limit']['page'] = $this->_getParam('page');
            }
            if($this->_getParam('verified')){
                $params['filter'] = array_merge($params['filter'], array(
					'e.verified_status = ?' => Model_VerifyRequests::STATUS_VERIFIED
				));
            }


			if( $this->_getParam('showname') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.showname LIKE ?' => "%" . trim($this->_getParam('showname')) . "%"
				));
			}


			if( $this->_getParam('gender') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.gender = ?' => $this->_getParam('gender')
				));
			}

			if( $this->_getParam('city') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ct.slug = ?' => $this->_getParam('city')
				));
			}

            if( $this->_getParam('zone')  ) {
				$params['filter'] = array_merge($params['filter'], array(
					'cz.slug = ?' => $this->_getParam('zone')
				));
			}

			if( $this->_getParam('available_for_incall') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.incall_type IS NOT NULL' => true
				));
			}
			if( $this->_getParam('available_for_outcall') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.outcall_type IS NOT NULL' => true
				));
			}

            if( $this->_getParam('italy') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.nationality_id = 22' => true
				));
			}


			/* ------------ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<< --------------- */


            if( $this->_getParam('ethnicity') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.ethnicity IN (' . implode(',', $this->_getParam('ethnicity')) . ')' => array()
				));

			}

            if( $this->_getParam('hair_color') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.hair_color IN (' . implode(',', $this->_getParam('hair_color') ) . ')' => array()
				));
			}

			if( $this->_getParam('hair_length') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.hair_length IN (' . implode(',', $this->_getParam('hair_length') ) . ')' => array()
				));
			}

			if( $this->_getParam('eye_color') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.eye_color IN (' . implode(',', $this->_getParam('eye_color') ) . ')' => array()
				));
			}

            

            if( $this->_getParam('pubic_hair') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.pubic_hair = ?' => $this->_getParam('pubic_hair')
				));
			}


			if( $this->_getParam('drinking') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.drinking = ?' => $this->_getParam('drinking')
				));
			}
        

            $page = 1;
            $page_size = $config['escorts']['perPage'];
            if($this->_getParam('page')){
                $page = $this->_getParam('page');
            }

			$model = new Model_EscortsV2();
			$count = 0;
			$escorts = $model->getSearchAll($params, $count,$page,$page_size);
			Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], 45, $count, 'regular_list', $only_regular, $where_query);
			

			if ( count( $escorts['data'] ) > 0 ){
				// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
				$sess_name = "prev_next";
				$sid = 'sedcard_paging_' . Cubix_Application::getId();
				$sess = new Zend_Session_Namespace($sid);

				$sess->{$sess_name} = array();
				foreach ( $escorts['data'] as $escort ) {
					//$sess->{$sess_name}[] = $escort->showname;
					$sess->{$sess_name}[] = $escort->id;
				}
				// </editor-fold>
			}
			


            $this->view->page = $page;
			$this->view->count = $escorts['count'];
			$this->view->escorts = $escorts['data'];
			$this->view->search_list = true;

			if( !$this->_request->ajax ){
                $this->_helper->viewRenderer->setScriptAction("escort-list");
            }

		}
	}

    public function favoritesAction()
	{
		$page = 1;
        //$page_size = 9;
        if($this->_getParam('page')){
            $page = $this->_getParam('page');
        }

		$this->user = Model_Users::getCurrent();

		$country_id = Cubix_Application::getById()->country_id;
		$modelC = new Model_Cities();
		$this->view->cities = $modelC->getByCountry($country_id);

		/*if ( $this->user->user_type == 'member' ) {
			$this->view->is_member = 1;

			$model = new Model_Users();
			$escorts = $model->getCurrent()->getFavorites();

			$count = $escorts['count'];
			$this->view->escorts = $escorts['escorts'];
			
		}else{
			$cook_name = "favorites";
			$data = unserialize($_COOKIE[$cook_name]);
			if($data && count($data) > 0){
				foreach ( $data as $e ) {
					$ids[] = $e;
				}

				$filter = array('e.id IN (' . implode(', ', $ids) . ')' => array());

				$count = 0;
				$escorts = Model_Escort_List::getFiltered($filter, 'random', $page, $page_size, $count);


				$this->view->escorts = $escorts;

				if ( count( $escorts ) > 0 ){
					// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
					$sess_name = "prev_next";
					$sid = 'sedcard_paging_' . Cubix_Application::getId();
					$sess = new Zend_Session_Namespace($sid);

					$sess->{$sess_name} = array();
					foreach ( $escorts as $escort ) {
						$sess->{$sess_name}[] = $escort->showname;
					}
					// </editor-fold>
				}
			}
		}*/

		$model = new Model_Members();
		$filter = array();

        $this->view->per_page = $per_page = 12;
		$ret = $model->getFavorites($this->user->id, $filter, $page, $per_page);
		$this->view->is_member = ( $this->user->user_type == 'member' ) || null;
		
		$this->view->escorts = $ret['data'];
		$this->view->count = $ret['count'];


        $this->view->page = $page;

        $this->view->menuFavorites = 1;

        $this->_helper->viewRenderer->setScriptAction("favorites-list");

	}

	public function ajaxFavoritesAction(){
		$this->view->layout()->disableLayout();

		$page = $this->view->page = $this->_request->page ? intval($this->_request->page) : 1;

        $this->view->per_page = $per_page = 12;

		$this->view->per_page = $per_page;

		$filter = array();

		if (strlen($this->_request->showname) > 0 ) {
			$showname = preg_replace('/[^-_a-z0-9\s]/i', '', trim($this->_request->showname));
			$filter['showname'] = $showname;
		}

		if (in_array($this->_request->act, array(1, 2, 3)))
			$filter['act'] = $this->_request->act;
		else
			$filter['act'] = 1;

		setcookie("favoriteAct", $filter['act']);

		if ( intval($this->_request->city ))
		{
			$filter['city'] = intval($this->_request->city);
		}

		$model = new Model_Members();

		$ret = $model->getFavorites($this->user->id, $filter, $page, $per_page);

		$this->view->items = $ret['data'];
		$this->view->count = $ret['count'];
	}

    public function escortsAction(){
        
	}

	public function gpsLocationAction()
	{

		$req = $this->_request;

		if(isset($this->_request->lat) && isset($this->_request->long)){
            $lat = $this->_request->lat;
            $lon = $this->_request->long;

        }
        $this->view->getlocation = 1;

        $this->view->page = isset($req->page) && is_numeric($req->page) ? intval($req->page) : 1;
		
		if ( isset( $lat ) && isset( $lon ) )
		{
            $this->view->layout()->disableLayout();
			$config = Zend_Registry::get('mobile_config');
			$limit = $config['escorts']['perPage'];
			$page = $req->page;
			$page = $this->view->page = isset($page) && is_numeric($page) ? intval($page) : 1;
			$page = ($page - 1) * $limit;

			$model = new Model_EscortsV2();

			$this->view->lat = floatval($req->lat);
			$this->view->long = floatval($req->long);
			$escorts = $model->getEscortsByLocation($lat, $lon, $limit, $page);

			$this->view->getlocation = 0;
			$this->view->count = $escorts['count'];
			$this->view->escorts = $escorts['data'];

		} else {
			$this->view->error = 1;
		}
	}
	
	public function jsInitAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$view = $this->view->act = $req->view;
		
		$req->setParam('lang_id', $req->lang);
		
		$lang = $this->view->lang = $req->lang;
		
		if ( $view == 'main-page' ) {
			$banners = $this->view->banners = Zend_Registry::get('banners');
		}
		else if ( $view == 'main-simple' ) {
			$this->view->is_logged_in = $req->is_logged_in;
		}
		else if ( $view == 'main' ) {
			
		}
		else if ( $view == 'private' ) {
			
		}
		else if ( $view == 'private-v2' ) {
			
		}
		else if ( $view == 'profile' ) {
			$this->view->escort_id = $req->escort_id;
			$this->view->has_no_review = $req->has_no_review;
			$this->view->is_member = $req->is_member;
			$this->view->is_escort = $req->is_escort;
		}
		else if ( $view == 'profile-v2' ) {
			$this->view->escort_id = $req->escort_id;
			$this->view->has_no_review = $req->has_no_review;
			$this->view->is_member = $req->is_member;
			$this->view->is_escort = $req->is_escort;
			
			$this->view->is_logged_in = $req->is_logged_in;
		}
		
		header('Content-type: text/javascript');
		/*header('Content-type: text/javascript');
		$expires = 60*60*24*14;
		header("Pragma: public");
		header("Cache-Control: maxage=" . $expires);
		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');*/
	}
	
    public function addToFavoritesAction(){
		$req = $this->_request;
        $escort_id = intval($req->escort_id);
        $cook_name = "favorites";
        $cook_value = array($escort_id);
        if ( ! isset( $_COOKIE[$cook_name]) ) {
            setcookie($cook_name, serialize($cook_value), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
        }
        else {
            $cook_data = unserialize($_COOKIE[$cook_name]);
            if ( is_array($cook_data) ) {
                if ( in_array($cook_value[0], $cook_data) ) {
                    $this->view->alredy_in_favorites = true;
                }
                else {
                    $new_data = array_merge($cook_data, $cook_value);
                    setcookie($cook_name, serialize($new_data), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
                }
            }
        }

		$this->user = Model_Users::getCurrent();

		if ( $this->user->user_type == 'member' ) {
			$user_id = $this->user->id;
			$escort_id = $this->_request->escort_id;
			//$model = new Model_Escorts();
			//$model->addToFavorites($user_id, $escort_id);
			$model = new Model_Members();
			$model->addToFavorites($user_id, $escort_id);
		}


		


        $this->_redirect($_SERVER['HTTP_REFERER']);
    }
    
    
    public function removeFromFavoritesAction(){
		$req = $this->_request;
		$escort_id = intval($req->escort_id);

		if ( $req->ajax && $req->isPost() && !empty( $escort_id ) ) {
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);

			$cook_name = "favorites";
			$cook_value = array($escort_id);
			if ( isset( $_COOKIE[$cook_name]) ) {
				$cook_data = unserialize($_COOKIE[$cook_name]);
				if ( is_array($cook_data) ) {
					if ( in_array($cook_value[0], $cook_data) ) {
						$new_data = array_diff($cook_data, $cook_value);
						setcookie($cook_name, serialize($new_data), strtotime('+1 year'), "/", '.' . Cubix_Application::getById(Cubix_Application::getId())->host);
					}
				}
			}

			if ( $this->user->user_type == 'member' ) {
				$user_id = $this->user->id;
				$escort_id = intval($escort_id);

				$model = new Model_Members();
				$model->removeFromFavorites($user_id, $escort_id);

				/* reordring top 10 */
				$model->reorderingTop10($user_id);
				die ( json_encode( array('status' => 1) ) );
			}
		}

        //$this->_redirect($_SERVER['HTTP_REFERER']);
    }

    public function ajaxFavoritesAddToTopAction(){
        $this->view->layout()->disableLayout();

        $form = new Cubix_Form_Data($this->_request);
        $fields = array(
            'escort_id' => 'int-nz'
        );

        $form->setFields($fields);
        $data = $form->getData();

        $model = new Model_Members();
        $ret = $model->favoritesAddToTop($this->user->id, $data['escort_id']);

        if ($ret == 'error')
            die(json_encode(array('error' => $this->view->t('top_10_limit'))));

        /* recalculation rating */
        $model->recalculateRating($data['escort_id']);
        /**/

        die(json_encode(array('success' => $this->view->t('top_10_added'))));
    }

    public function ajaxFavoritesRemoveFromTopAction(){
        $this->view->layout()->disableLayout();

        $form = new Cubix_Form_Data($this->_request);
        $fields = array(
            'escort_id' => 'int-nz'
        );

        $form->setFields($fields);
        $data = $form->getData();

        $model = new Model_Members();
        $model->favoritesRemoveFromTop($this->user->id, $data['escort_id']);

        /* reordring top 10 */
        $model->reorderingTop10($this->user->id);
        /**/

        /* recalculation rating */
        $model->recalculateRating($data['escort_id']);
        /**/

        die(json_encode(array('success' => $this->view->t('removed_from_top_favorites'))));
    }



    public function addFavCommentAction(){
        $form = new Cubix_Form_Data($this->_request);
        $fields = array(
            'escort_id' => 'int-nz',
            'comment' => 'notags|special'
        );

        $form->setFields($fields);
        $data = $form->getData();

        $data['user_id'] = $this->user->id;

        $model = new Model_Members();
        $model->updateFavoritesComment($data);

        die(json_encode(array('success' => true)));
    }

    public function top10ListAction(){
        $this->per_page = $per_page = 12;
    }

    public function ajaxTop10Action()
    {
        $this->view->layout()->disableLayout();

        $model = new Model_Members();

        $top10 = $model->getFavoritesTop10($this->user->id);
        $this->view->top10 = $top10;
        $this->view->per_page = $per_page = 12;

        if ($top10)
        {
            $max = 0;

            foreach ($top10 as $t)
            {
                if ($t['rank'] > $max)
                    $max = $t['rank'];
            }

            $this->view->max_rank = $max;

            /* requests */
            $all_requests = $model->getFavPendingRequests($this->user->id);

            $arr = array();

            if ($all_requests)
            {
                foreach ($all_requests as $r)
                {
                    if (!isset($arr[$r['fav_id']]))
                    {
                        $arr[$r['fav_id']] = array($r['user_id'] => array('name' => $r['name'], 'req_id' => $r['req_id']));
                    }
                    else
                    {
                        $arr[$r['fav_id']][$r['user_id']] = array('name' => $r['name'], 'req_id' => $r['req_id']);
                    }
                }
            }

            $this->view->requests = $arr;
            /**/
        }
    }

    public function ajaxFavoritesUpAction()
    {
        $this->view->layout()->disableLayout();

        $form = new Cubix_Form_Data($this->_request);
        $fields = array(
            'escort_id' => 'int-nz'
        );

        $form->setFields($fields);
        $data = $form->getData();

        $model = new Model_Members();
        $model->favoritesMoveUp($this->user->id, $data['escort_id']);

        /* recalculation rating */
        $model->recalculateRating($data['escort_id']);
        /**/

        die(json_encode(array('success' => true)));
    }

    public function ajaxFavoritesDownAction()
    {
        $this->view->layout()->disableLayout();

        $form = new Cubix_Form_Data($this->_request);
        $fields = array(
            'escort_id' => 'int-nz'
        );

        $form->setFields($fields);
        $data = $form->getData();

        $model = new Model_Members();
        $model->favoritesMoveDown($this->user->id, $data['escort_id']);

        /* recalculation rating */
        $model->recalculateRating($data['escort_id']);
        /**/

        die(json_encode(array('success' => true)));
    }

    public function feedbackAction()
	{
		$this->_request->setParam('no_tidy', true);

		$menu = array(
            'bio' => array(
                'name' => 'bio',
                'class' => 'w15',
            ),
            'services' => array(
                'name' => 'services',
                'class' => 'w23',
            ),
            'rates' => array(
                'name' => 'rates',
                'class' => 'w18',
            ),
			'tours' => array(
                'name' => 'tab_tours',
                'class' => 'w24',
            ),
            'contacts' => array(
                'name' => 'contact',
                'class' => 'w24',
            ),
            'photos' => array(
                'name' => 'photos',
                'class' => 'w20',
            ));

		$this->view->menu = $menu;


		$model = new Model_EscortsV2();

        $this->_helper->layout->setLayout('mobile');



		// Fetch administrative emails from config
		$config = Zend_Registry::get('feedback_config');
		$site_emails = $config['emails'];


		// Get data from request
		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'to' => '',
			'name' => 'notags|special',
			'email' => '',
			'message' => 'notags|special'
		);
		$data->setFields($fields);
		$data = $data->getData();

		$this->view->data = $data;

		// If the to field is invalid, that means that user has typed by
		// hand, so it's like a hacking attempt, simple die, without explanation
      

		if ( ! is_numeric($data['to']) && ! in_array($data['to'], array_keys($site_emails)) ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}

		// If escort id was specified, fetch it's email and showname,
		// construct the result email template and send it to escort
		if ( is_numeric($data['to']) ) {
			$escort = $model->get($data['to']);
			$this->view->escort = $escort;

            
            
			// If the escort id is invalid that means that it is hacking attempt
			if ( ! $escort ) {
				header('HTTP/1.1 301 Moved Permanently');
                header('Location: /');
                die;
                return;
			}
            
			$data['to_addr'] = $escort->email;
			$data['to_name'] = $escort->showname;
			$data['username'] = $escort->username;
			$email_tpl = 'escort_feedback';

            $this->view->showname = $escort->showname;
		}

		// In order when the form was posted validate fields
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', '' /*'Email is required'*/);
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', '' /*'Wrong email format'*/);
			}

			if ( ! strlen($data['message']) ) {
				$validator->setError('message','' /*'Please type the message you want to send'*/);
			}

			
			$result = $validator->getStatus();
            
            if ( ! $validator->isValid() ) {
                $this->view->errors = $result['msgs'];
			}else{
               

                // Set the template parameters and send it to support or to an escort
                $tpl_data = array(
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'to_addr' => $data['to_addr'],
                    'message' => $data['message'],
                    'escort_showname' => $escort->showname,
                    'escort_id' => $escort->id
                );

                if ( isset($data['username']) ) {
                    $tpl_data['username'] = $data['username'];
                }
                
                Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? $data['email'] : null);

                $this->_redirect('escort/'.$escort->showname.'?success=true#contacts');

            }
		}
	}

	public function robotsAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->layout()->disableLayout();
		$model = new Model_Robots();

		$robots = $model->getMobile();

		header('Content-Type: text/plain; charset=UTF-8', true);
		echo $robots;
		/*echo trim("User-agent: *
Dissallow: /page/terms-and-conditions
Disallow: /favorites
Disallow: /en
Disallow: /search");*/
		die;
	}
	
	/*public function goAction()
	{
        $link = $_GET['link'];

        //insert in table for statistics
        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $client->call('Escorts.insertStatisticsRow', array('id' => (int)$_GET['id'], 'user_type' => $_GET['user_type'], 'link' => $_GET['link']));
        //
        if ( ! preg_match('#^http(s)?://#', $link) ) $link = 'http://' . $link;
        header('Location: ' . $link);
        die;
	}*/

	public function getCityzonesAction()
	{
		$this->view->layout()->disableLayout();

		$city_slug = $this->_request->city;

		$c_model = new Cubix_Geography_Cities();
		$city = $c_model->getBySlug( $city_slug );

		$cz_model = new Cubix_Geography_Cityzones();
		
		$all_cityzones = $cz_model->ajaxGetAll($city->id);

		$this->view->zones = $all_cityzones;
//		var_Dump($this->view->zones);
//		exit;
	}


	public function ajaxGetCitiesAction()
	{
		$this->view->layout()->disableLayout();

		$this->_request->setParam('no_tidy', true);

		$country_id = intval($this->_getParam('country_id'));
		$region_id = intval($this->_getParam('region_id'));

		$model = new Cubix_Geography_Cities();

		$this->view->regions = $model->ajaxGetAll($region_id, $country_id);
		
	}



    public function contactAction()
    {
        $this->_request->setParam('no_tidy', true);

        $client = new Cubix_Api_XmlRpc_Client();
        $is_ajax = ! is_null($this->_getParam('ajax'));

        // If the request is ajax, disable the layout
        if ( $is_ajax ) {
            $this->view->layout()->disableLayout();
        }

        //$this->_request->setParam('to', 'support');
        if (is_null($this->_getParam('to'))){
            $sales = array(
                0 => array(
                    'name' => "Laura",
                    'phone' => "+44 20 3239 0564",
                    'skype' =>  "laura_escortforum",
                    'whatsapp' => "+37254263987, +37259637510",
                    'email' => "laura@escortforum.com",
                    'email_link' => "Laura",
                    'langs' => "Italian, English, Spanish"
                ),
                1 => array(
                    'name' => "Angelo",
                    'phone' => "+41 33 533 34 22",
                    'skype' =>  "angelo_escortforum",
                    'email' => "angelo@escortforum.com",
                    'email_link' => "Angelo",
                    'whatsapp' => "+41798070634",
                    'langs' => "Italian, English, French, Spanish, Portuguese",
                ),
                2 => array(
                    'name' => "Tony",
                    'phone' => "+41 44 586 98 50",
                    //'phone' => "+44 20 719 33 358", removed for https://sceonteam.atlassian.net/browse/EF-208
                    'skype' =>  "tony_escortforum",
                    'email' => "tony@escortforum.com",
                    'email_link' => "Tony",
                    'whatsapp' => "+37257669019",
                ),
                3 => array(
                    'name' => "Francesco",
                    'phone' => "+41 44 586 88 59",
                    'skype' =>  "francesco_escortforum",
                    'email' => "francesco@escortforum.com",
                    'email_link' => "Francesco",
                    'whatsapp' => '+41762031758',
                    'langs' => "Italian, English, Hungarian"
                ),
                4 => array(
                    'name' => "Sandra",
                    'phone' => "+41 44 586 88 46",
                    'skype' =>  "sandra_escortforum",
                    'email' => "sandra@escortforum.com",
                    'email_link' => "Sandra",
                    'langs' => "Italian, English, Hungarian, Greek"
                ),
                5 => array(
                    'name' => "Marco",
                    'phone' => "+41 44 586 88 41",
                    'skype' =>  "marco_escortforum",
                    'email' => "marco@escortforum.com",
                    'email_link' => "Marco",
                ),
                6 => array(
                    'name' => "Alessandro",
                    'phone' => "+41 44 586 68 84",
                    'skype' =>  "alessandro.escortforum",
                    'email' => "alessandro@escortforum.net",
                    'email_link' => "Alessandro",
                    'whatsapp' => "+41798071091",
                    'langs' => "Italian, English, Romanian"
                )
            );
            shuffle($sales);

            $sales_static = array(
                0 => array(
                    'name' => "Forum related",
                    'email' => "forum@escortforum.net",
                    'email_link' => "forum",
                    'langs' => "For anything related to our forum"
                ),
                1 => array(
                    'name' => "Users, reviews & comments",
                    'email' => "info@escortforumit.xxx",
                    'email_link' => "info",
                    'langs' => "Contacts for users, reviews, comments and other"
                ),
                2 => array(
                    'name' => "Problems & complaints",
                    'email' => "support@escortforum.com",
                    'email_link' => "support",
                    'langs' => "Contact for unresolved problems and complaints"
                )
            );

            $sales_statuses = $client->call('Users.getSalesSkypeStatuses');

            $online_count = 0;
            foreach($sales as $key => $sale){
                $sales[$key]['skype_status'] = $sales_statuses[$sales[$key]['skype']]['skype_status'];
                if( $sales[$key]['skype_status'] == 1 ) $online_count += 1;
            }

            $this->view->online_count = $online_count;

            /*foreach($sales as $key => $sale){
                $sales[$key]['skype_status'] = Cubix_Utils::getSkypeStatus($sale['skype']);
            }*/

            $count = count($sales);
            $array_value = array();
            for($i = 0; $i < $count - 1; $i++){
                for($j = 0; $j < $count - 1; $j++){

                    if($sales[$j]['skype_status'] > $sales[$j + 1]['skype_status']){
                        $array_value = $sales[$j];
                        $sales[$j] = $sales[$j + 1];
                        $sales[$j + 1] = $array_value;
                    }
                }
            }
            $this->view->sales = $sales;
            $this->view->sales_static = $sales_static;
        }
        else{
            $model = new Model_EscortsV2();

            $sales_emails = $client->call('Users.getSalesEmails');

            // Fetch administrative emails from config
            $config = Zend_Registry::get('feedback_config');
            $site_emails = $config['emails'];
            foreach($sales_emails as $sale){
                $site_emails[$sale['username']] = $sale['email'];
            }
            // If the to field is invalid, that means that user has typed by
            // hand, so it's like a hacking attempt, simple die, without explanation
            if ( ! is_numeric($this->_getParam('to')) && ! in_array($this->_getParam('to'), array_keys($site_emails)) ) {
                die('fuck');
            }
            // Get data from request
            $data = new Cubix_Form_Data($this->getRequest());
            $fields = array(
                'to' => '',
                'about' => 'int',
                'name' => 'notags|special',
                'email' => '',
                'phone' => '',
                'message' => 'notags|special',
                'captcha' => ''
            );
            $data->setFields($fields);
            $data = $data->getData();

            $data['to_addr'] = $site_emails[$data['to']];
            $email_tpl = 'feedback_template';
            $this->view->data = $data;
        }
        /*
        // If escort id was specified, fetch it's email and showname,
        // construct the result email template and send it to escort
        if ( is_numeric($data['to']) ) {
            $escort = $model->get($data['to']);
            $this->view->escort = $escort;
            //var_dump($escort);die;
            // If the escort id is invalid that means that it is hacking attempt
            if ( ! $escort ) {
                die;
            }

            $data['to_addr'] = $escort->email;
            $data['to_name'] = $escort->showname;
            $data['username'] = $escort->username;
            $email_tpl = 'escort_feedback';
        }
        // Else get administrative email from configuration file
        else {

                $data['to_addr'] = $site_emails[$data['to']];
                $email_tpl = 'feedback_template';
            }
        */


        // In order when the form was posted validate fields
        if ( $this->_request->isPost() ) {

            $validator = new Cubix_Validator();

            if ( !is_numeric($data['to']) ) {
                if (isset($_COOKIE['message_is_sent']))
                {
                    $validator->setError('message_is_sent', 'You have already sent a request please wait for response');
                }
            }

            if ( ! strlen($data['email']) ) {
                $validator->setError('email', 'Email is required');
            }
            elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
                $validator->setError('email', 'Wrong email format');
            }

            if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone']) ) {
                $validator->setError('phone', 'Invalid phone number');
            }
            if ( ! strlen($data['message']) ) {
                $validator->setError('message', 'Please type the message you want to send');
            }

            if ( ! strlen($data['captcha']) ) {
                $validator->setError('captcha', 'Captcha is required');
            }
            else {
                $session = new Zend_Session_Namespace('captcha');
                $orig_captcha = $session->captcha;

                if ( strtolower($data['captcha']) != $orig_captcha ) {
                    $validator->setError('captcha', 'Captcha is invalid');
                }
            }

            $result = $validator->getStatus();

            if ( ! $validator->isValid() ) {
                // If the request was ajax, send the validation result as json
                if ( $is_ajax ) {
                    echo json_encode($result);
                    die;
                }
                // Otherwise, render the phtml and show errors in it
                else {
                    $this->view->errors = $result['msgs'];
                    return;
                }
            }

            $sent_by = '';

            if ( $user = Model_Users::getCurrent() )
            {
                $sent_by = $user->user_type.' '.$user->username.' wrote a message';

            }

            if ( $data['about'] ) {
                $about_escort = $model->get($data['about']);
            }
            if ( ! $about_escort ) $about_escort = (object) array('id' => '', 'showname' => '');

            // Set the template parameters and send it to support or to an escort
            $tpl_data = array(
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'to_addr' => $data['to_addr'],
                'message' => $data['message'],
                'escort_showname' => $about_escort->showname,
                'escort_id' => $about_escort->id,
                'sent_by' => $sent_by
            );

            if ( isset($data['username']) ) {
                $tpl_data['username'] = $data['username'];
            }

            Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? $data['email'] : null);


            // contact me log
            Cubix_Api::getInstance()->call('contactLog', array($data['to_addr'], $user ? $user->id : null));
            //

            if ( $is_ajax ) {
                if ( isset($escort) ) {
                    $result['msg'] = '
						<p style="padding:0 10px"><span class="strong" style="color: #060">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>
					';
                }
                else {
                    $result['msg'] = '
						<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>
					';
                }
                //<a href="#" onclick="return Cubix.Feedback.Show(this.getParent(\'.cbox-small-ii\'))">Click here</a> to hide this message</strong>
                echo json_encode($result);
                die;
            }
            else {
                setcookie("message_is_sent", 1, time() + 604800, "/"); // 604800 = 60*60*24*7 = 7 days
                $this->view->success = TRUE;
            }
        }
    }


	public function captchaAction()
	{
		$this->_request->setParam('no_tidy', true);

		$font = 'css/trebuc.ttf';

		$charset = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';

		$code_length = 5;

		$height = 60;
		$width = 180;
		$code = '';

		for ( $i = 0; $i < $code_length; $i++ )
		{
			$code = $code . substr($charset, mt_rand(0, strlen($charset) - 1), 1);
		}

		$rgb[0] = array(204,0,0);
		$rgb[1] = array(34,136,0);
		$rgb[2] = array(51,102,204);
		$rgb[3] = array(141,214,210);
		$rgb[4] = array(214,141,205);
		$rgb[5] = array(100,138,204);

		$font_size = $height * 0.4;

		$bg = imagecreatefrompng('img/bg_captcha_mob.png');
		$image = imagecreatetruecolor($width, $height);
		imagecopy($image, $bg, 0, 0, 0, 0, imagesx($bg), imagesy($bg));
		$background_color = imagecolorallocate($image, 255, 255, 255);
		$noise_color = imagecolorallocate($image, 20, 40, 100);

		for($i = 0; $i<$code_length; $i++)
		{
			$A[] = rand(-20, 20);
			$C[] = rand(0, 5);
			$text_color = imagecolorallocate($image, $rgb[$C[$i]][0], $rgb[$C[$i]][1], $rgb[$C[$i]][2]);
			imagettftext($image, $font_size, $A[$i], 14 + $i * 30, 40 + rand(-3, 3), $text_color, $font , $code[$i]);
		}

		$session = new Zend_Session_Namespace('captcha');
		$session->captcha = strtolower($code);

		header('Content-Type: image/png');
		imagepng($image);
		imagedestroy($image);

		die;
	}



	public function videoListAction()
	{
		if ($this->_request->ajax) {
			$this->view->layout()->disableLayout();
		} else {
			$this->view->layout()->setLayout('mobile-index');
		}


		$lng = Cubix_I18n::getLang();
		$page = $this->_request->getParam('page', 1);
		$per_page = $this->_request->getParam('per_page', 50);
		$city_id = $this->_request->getParam('city_id');

		/* ALL VIDEOS */

		$model_video = new Model_Video();
		$result = $model_video->getAll($page, $per_page, $city_id);

		$videos = $result['page_data'];
		$total_videos = $result['total_video_data'];

		usort($total_videos, function($a, $b) {
		    return $b->v_count - $a->v_count;
		});

		$primary_cities = array_splice($total_videos, 0, 14);


		usort($primary_cities, function($a, $b) {
		    return strcmp($a->city_title, $b->city_title);
		});

		usort($total_videos, function($a, $b) {
		    return strcmp($a->city_title, $b->city_title);
		});

		$secondary_cities  = $total_videos;


		/* VIDEOS BY CITIES */
		foreach($videos as &$video) {
			$image = new Cubix_ImagesCommonItem($video);
			$video->video_image = $image->getUrl('video_thumb');
		}

		$this->view->primary_cities = $primary_cities;
		$this->view->secondary_cities = $secondary_cities;
		$this->view->videos = $videos;
		$this->view->count = count($total_videos);
		$this->view->lang = $lng;
		$this->view->page = $page;
		$this->view->total_pages = ceil(count($total_videos) / $per_page);
		$this->view->city_id = $city_id;
	}
	
	public function geotargetedAction()
	{
		$geo_ip = Cubix_IP2Location::getInstance();
		$records = $geo_ip->lookup(Cubix_Geoip::getIP(), array(Cubix_IP2Location::COUNTRY_CODE, Cubix_IP2Location::CITY_NAME));
		
		$httpReferer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
		//$records["countryCode"] = 'IT';
		//$records["cityName"] = 'Milan';
		$data = array(
			'country_iso' => $records["countryCode"],
			'city' => $records["cityName"],
			'referer' => $httpReferer
		);
		$status = 0;
		$model = new Model_Geotargeted();
		
		if($records["countryCode"] == "IT"){
			$city_model = new Model_Cities();
			$city = $city_model->getByEnTitle($records["cityName"]);
			$count = Model_EscortsV2::getEscortCountBycityId($city->id);
			if($count >= 5 ){
				$status = 2;
			}
			elseif($count > 0){
				$status = 1;
			}
		}
		$data['status'] = $status;
		$model->save($data);
		if($status == 2){
			$this->_redirect($this->view->getLink('escorts', array('city' => 'it_' . $city->slug)));
		}
		else{
			$this->_redirect('/');
		}
		die;
	}

    public function photoViewerAction()
    {

        $escort_id = $this->_getParam('escort_id');
        $response = array('status' => 'success', 'msg' => '', 'data' => array());

        if ( ! $escort_id ) {
            $response['status'] = 'error';
            $response['msg'] = 'escort id is required';
        }
        else {
            Model_EscortsV2::hit_gallery( intval($escort_id) );
        }

        $this->_helper->json($response);
    }

}
