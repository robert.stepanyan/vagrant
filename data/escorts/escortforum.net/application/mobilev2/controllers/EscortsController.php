<?php

class Mobilev2_EscortsController extends Zend_Controller_Action
{

	public function init()
	{
		$this->_helper->layout->setLayout('mobile-index');

		$_SESSION['request_url'] = $_SERVER['REQUEST_URI'];
		//die('Mobile version is temporary unavailable');
	}

	public function ajaxBubbleAction()
	{
		$this->view->layout()->disableLayout();

		$region = $this->_request->r;
		$page = intval($this->_getParam('page', 1));

		$per_page = 1; //intval($this->_getParam('per_page', 1));
		if ($page < 1) $page = 1;

		$cache = Zend_Registry::get('cache');

		$cache_key = 'mobile_widget_bubble_texts_page_' . $page . '_' . $region;
		if (!$bubbles = $cache->load($cache_key)) {
			try {
				$client = Cubix_Api_XmlRpc_Client::getInstance();
				$bubbles = $client->call('Escorts.getBubbleTexts', array($page, $per_page, $region));

			} catch (Exception $e) {
				$bubbles = array('texts' => array(), 'count' => 0);
			}

			foreach ($bubbles['texts'] as &$text) {
				$text = new Model_EscortV2Item($text);
			}

			$cache->save($bubbles, $cache_key, array(), 300);

		}
		//var_dump($bubbles);
		$this->view->per_page = $per_page;
		$this->view->page = $page;
		$this->view->bubbles = $bubbles;

		$this->view->region = $region;
	}

	public function indexAction()
	{

		$req = $this->_getParam('req');

		$config = Zend_Registry::get('mobile_config');
		

		// Scroll Spy
		if ($this->_request->ajax) {
			$this->view->spy = true;
			$this->view->layout()->disableLayout();
		}


		if (preg_match('#/([0-9]+)$#', $req, $a)) {
			$a[1] = intval($a[1]);
			$req = preg_replace('#/[0-9]+$#', '/page_' . $a[1], $req);
		}


		$req = explode('/', $req);

		if (isset($req[2])) {
			if ($req[2] == 'upcomingtours' || $req[2] == 'citytours') {

				$tc = explode('_', $req[1]);
				array_shift($tc);
				$tc = implode('_', $tc);

				if ($req[2] == 'upcomingtours') {
					$link = $this->view->getLink('escorts', array('upcomingtours', 'city' => strtolower($tc), 'filter' => null, 'page' => null));
				}

				if ($req[2] == 'citytours') {
					$link = $this->view->getLink('escorts', array('tours', 'city' => strtolower($tc), 'filter' => null, 'page' => null));
				}

				header('HTTP/1.1 301 Moved Permanently');
				header('Location: ' . $link);
				die;
				return;
			}
		}

		$params = array(
			'sort' => 'random',
			'filter' => array(array('field' => 'girls', 'value' => array())),
			'page' => 1
		);

		$static_page_key = 'main';

		foreach ($req as $r) {
			if (!strlen($r))
				continue;

			$param = explode('_', $r);


			if (isset($param[2])) {
				preg_match('/[A-Z]/', $param[2], $matches);
				if (count($matches) > 0) {
					$link = strtolower($this->view->getLink('escorts'));
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: ' . $link);
					die;
					return;
				}
			}

			if (count($param) < 2) {
				switch ($r) {
                    case 'virtual':
                        $static_page_key = 'virtual';
                        break;
					case 'nuove':
						//$params['filter'][] = array('field' => 'new_arrivals', 'value' => array());
						$static_page_key = 'nuove';
						$param = array('sort', 'newest');
						//$params['filter'][] = array('field' => 'is_new', 'value' => '1');
						break;
					case 'independantes':
						$static_page_key = 'independantes';
						$params['filter'][] = array('field' => 'independantes', 'value' => array());
						continue;
					case 'regular':
						$static_page_key = 'regular';
						$params['filter'][] = array('field' => 'regular', 'value' => array());
						continue;
					case 'agence':
						$static_page_key = 'agence';
						$params['filter'][] = array('field' => 'agence', 'value' => array());
						continue;
					case 'boys':
						$static_page_key = 'boys';
						$params['filter'][0] = array('field' => 'boys', 'value' => array());
						continue;
					case 'trans':
						$static_page_key = 'trans';
						$params['filter'][0] = array('field' => 'trans', 'value' => array());
						continue;
//					case 'instant-book':
//						$static_page_key = 'instant-book';
//						continue;
					case 'citytours':
						$static_page_key = 'citytours';
						$this->view->is_tour = $is_tour = true;
						$params['filter'][] = array('field' => 'tours', 'value' => array());
						continue;
					case 'upcomingtours':
						$static_page_key = 'upcomingtours';
						$this->view->is_tour = $is_tour = true;
						$this->view->is_upcomingtour = true;
						$upcoming_tours = true;
						$params['filter'][] = array('field' => 'upcomingtours', 'value' => array());
						continue;
                    case 'skype-available':
                        $static_page_key = 'skype-available';
                        $this->view->skype_available = $skype_available = true;
                        $is_city_tour = false;
                        $params['filter'][] = array('field' => 'skype-available', 'value' => '1');
                        continue;
					/* 					case 'blank.html':
					  $this->_forward('blank-html', 'redirect');
					  return; */
					case 'valentine':
                    	$static_page_key = 'valentine';
                    	continue;
					default:
						$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
						$this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
						return;
				}
			}

			$param_name = $param[0];
			array_shift($param);
			$this->view->static_page_key = $static_page_key;
			$this->view->by_newest = false;

			if ( $static_page_key == 'valentine' ) {
				// header('HTTP/1.1 301 Moved Permanently');
				// header('Location: /');
				// return;
				$this->_forward('valentine');
				return;
			}

			switch ($param_name) {
				case 'filter':
					$has_filter = true;
					/* >>> For nested menu */
					$selected_item = $menus['filter']->getByValue(implode('_', $param));
					if (!is_null($selected_item)) {
						//$menus['filter']->setSelected($selected_item);
					}
					/* <<< */

					$field = reset($param);
					array_shift($param);

					$value = array_slice($param, 0, 2);

					$params['filter'][] = array('field' => $field, 'value' => $value, 'main' => true);
					break;
				case 'page':
					$page = intval(reset($param));

					if ($page < 1) {
						$page = 1;
					}

					$params['page'] = $page;
					break;
				case 'sort':
					$params['sort'] = reset($param);
					$this->view->by_newest = true;

					/*$selected_item = $menus['sort']->getByValue($params['sort']);
					if (!is_null($selected_item)) {
						$menus['sort']->setSelected($selected_item);
					}*/
					$has_filter = true;
					break;
				case 'region':
				case 'state':
					$params['country'] = $param[0];
					$params['region'] = $param[1];
					$params['filter'][] = array('field' => 'region', 'value' => $param[1]);
					break;
				case 'city':
				case 'zone':
					$params[$param_name] = $param[1];
					$params['filter'][] = array('field' => $param_name, 'value' => array($param[1]));
					break;
				case 'name':
					$has_filter = true;
					$params['filter'][] = array('field' => 'name', 'value' => array('%' . $param[0] . '%'));
					break;
				default:
					if (!in_array($param_name, array('skype-available','virtual', 'nuove', 'independantes', 'regular', 'agence', 'boys', 'trans', 'citytours', 'upcomingtours' /*, 'instant-book'*/))) {
						$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
						$this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
						return;
					}
			}
		}

//
//
//		if ($static_page_key == 'instant-book') {
//			return $this->_forward('instant-book');
//		}
//

		$filter_params = array(
			'order' => 'e.date_registered DESC',
			'limit' => array('page' => 3),
			'filter' => array()
		);
        if ( $static_page_key == 'virtual') {
            $filter_params['filter']['with_virtual_services'] = 1;
        }
		$filter_map = array(
			'verified' => 'e.verified_status = 2',
			'french' => 'e.nationality_id = 15',
			'age' => 'e.age < ? AND e.age > ?',
			'ethnic' => 'e.ethnicity = ?',
			'height' => 'e.height < ? AND e.height > ?',
			'weight' => 'e.weight < ? AND e.weight > ?',
			'cup-size' => 'e.cup_size = ?',
			'hair-color' => 'e.hair_color = ?',
			'hair-length' => 'e.hair_length = ?',
			'eye-color' => 'e.eye_color = ?',
			'dress-size' => 'e.dress_size = ?',
			'shoe-size' => 'e.shoe_size = ?',
			'available-for-incall' => 'e.incall_type IS NOT NULL',
			'available-for-outcall' => 'e.outcall_type IS NOT NULL',
			'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
			'smoker' => 'e.is_smoking = ?',
			'language' => 'FIND_IN_SET(?, e.languages)',
			'now-open' => 'e.is_now_open',
			'region' => 'r.slug = ?',
			'city' => 'ct.slug = ?',
			'cityzone' => 'c.id = ?',
			'zone' => 'cz.slug = ?',
			'name' => 'e.showname LIKE ?',
			'new_arrivals' => 'e.gender = ' . GENDER_FEMALE,
			'independantes' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
			'agence' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE,
			'boys' => 'eic.gender = ' . GENDER_MALE,
			'trans' => 'eic.gender = ' . GENDER_TRANS,
			'girls' => 'eic.gender = ' . GENDER_FEMALE,
			'tours' => 'eic.is_tour = 1',
			'upcomingtours' => 'eic.is_upcoming = 1',
            'skype-available' => 'e.skype_call_status = ?',
            'is_new' => 'e.is_new = 1',
        );

        foreach ($params['filter'] as $i => $filter) {

			if (!isset($filter_map[$filter['field']]))
				continue;

			$value = $filter['value'];

			if (isset($filter['main'])) {
				if (isset($selected_filter->internal_value)) {
					$value = $selected_filter->internal_value;
				} elseif (!is_null($item = $menus['filter']->getByValue($filter['field'] . ((isset($value[0]) && $value[0]) ? '_' . $value[0] : '')))) {
					$value = $item->internal_value;
				}
			}

			$filter_params['filter'][$filter_map[$filter['field']]] = $value;
		}

		$page = intval($this->_getParam('page'));

		if ($page == 0) {
			$page = 1;
		}


		$filter_params['limit']['page'] = $page;
		$count = 0;

		if (isset($params['city'])) {
			$this->view->city = $params['city'];
		}



		//SHOW UPCOMING TOURS BUTTON LOGIC
		$tours_count = array();
		$tmp_escort_filters = array();
		if (isset($filter_params['filter']['ct.slug = ?'])) {
			$tmp_escort_filters['ct.slug = ?'] = $filter_params['filter']['ct.slug = ?'];
		}
		Model_Escort_List::getTours(true, $tmp_escort_filters, 'random', 1, 50, $u_count);
		Model_Escort_List::getTours(false, $tmp_escort_filters, 'random', 1, 50, $c_count);


		$tours_count[$t_key] = $t_count;
		$this->view->tourCount = $c_count;
		$this->view->upcomingCount = $u_count;
		$__count = 40;

		if (isset($is_tour) && $is_tour) {
			$escorts = Model_Escort_List::getTours($upcoming_tours, $filter_params['filter'], $params['sort'], $filter_params['limit']['page'], /*$config['escorts']['perPage']*/ $__count, $count);
		} else {

			$escorts = Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], /*$config['escorts']['perPage']*/ $__count, $count, true);
		}

        $neared_escorts = array();
        if ( isset($params['city']) ) {
            $c_model = new Model_Cities();

            if( $count < 10 ){
                $id = $c_model->getBySlug( $params['city'] )->id;
                $geo_data = (array) $c_model->getCityGeoData( $id );
                unset( $filter_params['filter']['ct.slug = ?'] );

                $neared_escorts = Model_Escort_List::getFiltered( $filter_params['filter'], 'close-to-me', 1, /*$config['escorts']['perPage']*/ 100, $_c_count, true, false, array(), $geo_data );
                //$neared_escorts = $this->_group_by( '' );
                //print_r( (array)$neared_escorts); die;
            }
        }

		if (count($escorts) > 0) {
			// <editor-fold defaultstate="collapsed" desc="Fill the session for next/prev from sedcard">
			$sess_name = "prev_next";
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$sess = new Zend_Session_Namespace($sid);

			$sess->{$sess_name} = array();
			foreach ($escorts as $escort) {
				//$sess->{$sess_name}[] = $escort->showname;
				$sess->{$sess_name}[] = $escort->id;
			}
			// </editor-fold>
		}

		$res = Model_EscortsV2::getInstantBookEscortsList(1, 1000);
		$this->view->instantbook_escorts = $res['ids'];
		
		$count_city_escorts = Model_Escort_List::getCityEscortsCount($params['city']);

		$this->view->is_zone = ($params['zone']) ? true : false;
		$this->view->zone_title = '';
		$this->view->city_count = $count_city_escorts;
		$this->view->params = $params;
		$this->view->escorts = $escorts;
		$this->view->page = $page;
		$this->view->count = $count;
		$this->view->per_page = $__count;
		$this->view->neared_escorts = $neared_escorts;

		$type = 'escort';
		if( $params['filter'][0]['field'] == 'boys' ){$type = 'boys'; }
		if( $params['filter'][0]['field'] == 'trans' ){ $type = 'trans'; }

		$type_count = 0;
        $cache = Zend_Registry::get('cache');
        $cache_key = 'mobile_escorts_count_type_' . $page . '_' . $params['city'] . $type;

        $cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

//        if( $_GET['test'] == 1 ){
//            print_r( $cache_key ); die;
//        }

        if (!$type_count = $cache->load($cache_key)) {
            $cache->save( count( $escorts ), $cache_key, array());
        }

		if (isset($is_tour) && $is_tour) {
			$this->_helper->viewRenderer->setScriptAction("tour-list");
		} else {
			$this->_helper->viewRenderer->setScriptAction("escort-list");
		}
	}

    function _group_by($key, $data) {
        $result = array();

        foreach($data as $val) {
            if(array_key_exists($key, $val)){
                $result[$val[$key]][] = $val;
            }else{
                $result[""][] = $val;
            }
        }

        return $result;
    }

	public function reviewsAction()
	{
		$lng = Cubix_I18n::getLang();

		$request = $this->_request;
		if ($request->ajax) {
			$this->view->layout()->disableLayout();
			$this->view->ajax = true;
		}
		$filter = array();

		if (isset($request->city_id) && $request->city_id) {
			$filter['city_id'] = intval($request->city_id);
			$this->view->city_id = intval($request->city_id);
		}

		if (isset($request->showname) && $request->showname) {
			$filter['showname'] = $request->showname;
			$this->view->showname = $request->showname;
		}

		if (isset($request->member_name) && $request->member_name) {
			$filter['member_name'] = $request->member_name;
			$this->view->member_name = $request->member_name;
		}

		$ord_field_v = 'creation_date';
		$ord_field = 'r.creation_date';
		$ord_dir_v = 'desc';
		$ord_dir = 'DESC';

		if (count($filter) > 0)
			$arg_filter = $filter;
		else
			$arg_filter = '-999';
		$config = Zend_Registry::get('reviews_config');
		if (isset($request->page) && intval($request->page) > 0) {
			$page = intval($request->page);
		} else
			$page = 1;

		is_array($arg_filter) ? $filter_str = implode('.', $arg_filter) : $filter_str = $arg_filter;

		$ret_revs = Model_Reviews::getReviews($page, $config['perPage'], $arg_filter, $ord_field, $ord_dir);
		$cities = Model_Reviews::getReviewsCities();


		$ret = array($ret_revs, $cities);

		list($ret_revs, $cities) = $ret;
		list($items, $count) = $ret_revs;
		$this->view->items = $items;
		$this->view->count = $count;
		$this->view->cities = $cities;
		$this->view->page = $page;
		$this->view->filter = $filter;

		$this->view->menuReviews = 1;
	}

	public function searchAction()
	{
		//$this->view->layout()->setLayout('main-simple');

		$this->view->quick_links = false;

		$this->view->defines = Zend_Registry::get('defines');

		$all_cities = Model_Statistics::getCities();
		usort($all_cities, array(self, "_orderTitles"));
		$this->view->cities = $all_cities;

		if ($this->_getParam('search')) {

			$this->_helper->viewRenderer->setScriptAction("search-list");
			$params['filter'] = array();
			$params['order'] = array();

			$this->view->params = $this->_request->getParams();
			$this->view->serializedParams = serialize($this->view->params);

			if ($this->_getParam('order')) {

				$order_direction = '';
				$order_field = 'date_registered';

				switch ($this->_getParam('order')) {
					case 'lastmodified':
						$order_field = "date_last_modified";
						break;
					case 'popularity':
						$order_field = "hit_count";
						break;
				}

				if ($this->_getParam('order_direction')) {
					$order_direction = $this->_getParam('order_direction');
				}
				$params['order'] = $order_field . " " . $order_direction;

			}


			if ($this->_getParam('page')) {
				$params['limit']['page'] = $this->_getParam('page');
			}

			if ($this->_getParam('publish')) {
				$params['filter'] = array_merge($params['filter'], array(
					'DATEDIFF(NOW(),e.date_registered) <= ?' => trim($this->_getParam('days'))
				));
			}

			if ($this->_getParam('showname')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.showname LIKE ?' => "%" . trim($this->_getParam('showname')) . "%"
				));
			}

			if ($this->_getParam('agency_slug') && $this->_getParam('agency')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.agency_slug LIKE ?' => "%" . trim($this->_getParam('agency_slug')) . "%"
				));
			}

			if (count($this->_getParam('gender'))) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.gender IN ' . $this->__arrayForProcedureIN($this->_getParam('gender')) => ''
				));
			}

			if ($this->_getParam('city')) {
				$params['filter'] = array_merge($params['filter'], array(
					'ct.slug = ?' => $this->_getParam('city')
				));
			}

			if ($this->_getParam('cityzone')) {
				$params['filter'] = array_merge($params['filter'], array(
					'ecz.city_zone_id = ?' => $this->_getParam('cityzone')
				));
			}

			if ($this->_getParam('phone')) {
				$phone = preg_replace('/^\+/', '00', $this->_getParam('phone'));
				$phone = preg_replace('/[^0-9]+/', '', $phone);
				$params['filter'] = array_merge($params['filter'], array(
					'e.contact_phone_parsed LIKE ?' => "%" . $phone . "%"
				));
			}

			if ($this->_getParam('available_for_incall')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.incall_type IS NOT NULL' => true
				));
			}
			if ($this->_getParam('available_for_outcall')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.outcall_type IS NOT NULL' => true
				));
			}

			if (count($this->_getParam('nationality'))) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.nationality_id IN ' . $this->__arrayForProcedureIN($this->_getParam('nationality')) => ''
				));
			}

			if ($this->_getParam('saf')) {
				$params['filter'] = array_merge($params['filter'], array(
					'FIND_IN_SET( ?, e.sex_availability)' => $this->_getParam('saf')
				));
			}

			if ($this->_getParam('price_from')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.outcall_price >= ?' => $this->_getParam('price_from')
				));
			}

			if ($this->_getParam('price_to')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.outcall_price <= ?' => $this->_getParam('price_to')
				));
			}

			if ($this->_getParam('words')) {
				if ($this->_getParam('words_type') == 1) {
					if ($pos = strpos($this->_getParam('words'), ' ')) {
						$sub = substr($this->_getParam('words'), 0, $pos);
					} else {
						$sub = $this->_getParam('words');
					}

					$params['filter'] = array_merge($params['filter'], array(
						'e.' . Cubix_I18n::getTblField('about') . ' = ?' => $sub
					));
				} elseif ($this->_getParam('words_type') == 2) {
					$params['filter'] = array_merge($params['filter'], array(
						'e.' . Cubix_I18n::getTblField('about') . ' LIKE ?' => "%" . $this->_getParam('words') . "%"
					));
				}
			}

			if ($this->_getParam('has_review_blocked')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.disabled_reviews = ?' => 0
				));
			}

			if ($this->_getParam('has_comment_blocked')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.disabled_comments = ?' => 0
				));
			}

			if ($this->_getParam('is_on_tour')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.is_on_tour = ?' => 1
				));
			}

			/* ------------ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<< --------------- */

			if ($this->_getParam('age_from')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.age >= ?' => $this->_getParam('age_from')
				));
			}
			if ($this->_getParam('age_to')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.age <= ?' => $this->_getParam('age_to')
				));
			}

			if ($this->_getParam('height_from')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.height >= ?' => $this->_getParam('height_from')
				));
			}
			if ($this->_getParam('height_to')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.height <= ?' => $this->_getParam('height_to')
				));
			}

			if (count($this->_getParam('ethnicity'))) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.ethnicity IN ' . $this->__arrayForProcedureIN($this->_getParam('ethnicity')) => ''
				));
			}

			if (count($this->_getParam('hair_color'))) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.hair_color IN ' . $this->__arrayForProcedureIN($this->_getParam('hair_color')) => ''
				));
			}

			if (count($this->_getParam('hair_length'))) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.hair_length IN ' . $this->__arrayForProcedureIN($this->_getParam('hair_length')) => ''
				));
			}

			if (count($this->_getParam('eye_color'))) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.eye_color IN ' . $this->__arrayForProcedureIN($this->_getParam('eye_color')) => ''
				));
			}

			if ($this->_getParam('height_from')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.height >= ?' => $this->_getParam('height_from')
				));
			}

			if ($this->_getParam('height_to')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.height <= ?' => $this->_getParam('height_to')
				));
			}

			if ($this->_getParam('dress_size')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.dress_size = ?' => $this->_getParam('dress_size')
				));
			}

			if ($this->_getParam('shoe_size')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.shoe_size = ?' => $this->_getParam('shoe_size')
				));
			}

			if (count($this->_getParam('cup_size'))) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.cup_size IN ' . $this->__arrayForProcedureIN($this->_getParam('cup_size'), true) => ''
				));
			}

			if ($this->_getParam('pubic_hair')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.pubic_hair = ?' => $this->_getParam('pubic_hair')
				));
			}

			if ($this->_getParam('smoker')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.is_smoking = ?' => $this->_getParam('smoker')
				));
			}

			if ($this->_getParam('drinking')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.is_drinking = ?' => $this->_getParam('drinking')
				));
			}
			if (count($this->_getParam('language'))) {
				$params['filter'] = array_merge($params['filter'], array(
					'CONCAT(",", e.languages, ",") REGEXP ",(' . implode('|', $this->_getParam('language')) . '),"' => ''
				));
			}
			/* Services */
			if ($this->_getParam('services')) {
				$params['filter'] = array_merge($params['filter'], array(
					'es.service_id = ?' => $this->_getParam('services')
				));
			}

			if ($this->_getParam('video')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.has_video = ?' => 1
				));
			}

			if ($this->_getParam('is_vip')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.is_vip = ?' => 1
				));
			}

			if ($this->_getParam('has_nat_pic')) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.has_nat_pic = ?' => 1
				));
			}

			/* Services */
			/* Working Times */

			if ($this->_getParam('day_index')) {
				$dateTimes = array();
				$counter = 0;
				foreach ($this->_getParam('day_index') as $day => $value) {
					$timeFrom = $this->_getParam('time_from');
					$timeFromM = $this->_getParam('time_from_m');
					$timeTo = $this->_getParam('time_to');
					$timeToM = $this->_getParam('time_to_m');
					$dateTimes[$counter]['day_index'] = $day;
					$dateTimes[$counter]['time_from'] = $timeFrom[$day];
					$dateTimes[$counter]['time_from_m'] = $timeFromM[$day];
					$dateTimes[$counter]['time_to'] = $timeTo[$day];
					$dateTimes[$counter]['time_to_m'] = $timeToM[$day];
					$counter++;
				}
				$params['filter'] = array_merge($params['filter'], array(
					'working_times = ?' => $dateTimes
				));
			}

			/* Working Times */

			$page = 1;
			$page_size = 12;
			if ($this->_getParam('page')) {
				$page = $this->_getParam('page');
			}

			$model = new Model_EscortsV2();
			$count = 0;
			$escorts = $model->getSearchAll($params, $count, $page, $page_size);
			$x['times'] = $dateTimes;

			$this->view->data = $x;
			$this->view->page = $page;
			$this->view->count = $escorts['count'];
			$this->view->escorts = $escorts['data'];

			$this->view->search_list = true;
			$escorts_video = array();
			if (!empty($escorts['data'])) {
				$e_id = array();
				foreach ($escorts['data'] as $e) {
					$e_id[] = $e->id;
				}

				$video = new Model_Video();
				$vescorts = $video->GetEscortsVideoArray($e_id);
				if (!empty($vescorts)) {
					$app_id = Cubix_Application::getId();
					foreach ($vescorts as &$v) {
						$photo = new Cubix_ImagesCommonItem(array(
							'hash' => $v->hash,
							'width' => $v->width,
							'height' => $v->height,
							'ext' => $v->ext,
							'application_id' => $app_id
						));

						$v->photo = $photo->getUrl('orig');
						$escorts_video[$v->escort_id] = $v;
					}
					$config = Zend_Registry::get('videos_config');
					$host = Cubix_Application::getById(Cubix_Application::getId())->host;
					$this->view->vconfig = $config['Media'] . 'flv:' . $host;
				}
			}
			$this->view->escorts_video = $escorts_video;
			$res = Model_EscortsV2::getInstantBookEscortsList(1, 1000);
			$this->view->instantbook_escorts = $res['ids'];
			
		}
	}

	/** * Page for instant book escorts */

	public function instantBookAction()
	{
		$model = new Model_EscortsV2();
		
		$result = $model->getInstantBookEscortsList(1, 1000);
		$ids = $result['ids'];

		if (count($ids) > 0) {
			$ids = implode(',', $ids);
			$filter = array(
				'e.id IN (' . $ids . ')'
			);

			$page_num = 1;
			if (isset($page[1]) && $page[1]) {
				$page_num = $page[1];

			}

			$count = 0;
			$escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 50, $count);
		} else {
			$count = 0;
			$escorts = array();
		}

		$this->_helper->viewRenderer->setScriptAction("escort-list");
		$this->view->by_newest = true;
		$this->view->escort_ids = $ids;
		$this->view->count = $count;
		$this->view->escorts = $escorts;
	}

	public function searchFilterDataAction($data = null)
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$params = array(
			'order' => 'e.date_registered DESC',
			'limit' => array('page' => 1),
			'filter' => array()
		);

		if ($this->_getParam('showname')) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.showname LIKE ?' => "%" . trim($this->_getParam('showname')) . "%"
			));
		}

		if ($this->_getParam('phone')) {
			$phone = preg_replace('/^\+/', '00', $this->_getParam('phone'));
			$phone = preg_replace('/[^0-9]+/', '', $phone);
			$params['filter'] = array_merge($params['filter'], array(
				'e.contact_phone_parsed LIKE ?' => "%" . $phone . "%"
			));
		}

		if ($this->_getParam('city')) {
			$params['filter'] = array_merge($params['filter'], array(
				'ct.slug = ?' => $this->_getParam('city')
			));
		}

		if ($this->_getParam('zone')) {
			$params['filter'] = array_merge($params['filter'], array(
				'cz.slug = ?' => $this->_getParam('zone')
			));
		}

		if ($this->_getParam('words')) {
			if ($this->_getParam('words_type') == 1) {
				if ($pos = strpos($this->_getParam('words'), ' ')) {
					$sub = substr($this->_getParam('words'), 0, $pos);
				} else {
					$sub = $this->_getParam('words');
				}

				$params['filter'] = array_merge($params['filter'], array(
					'e.' . Cubix_I18n::getTblField('about') . ' = ?' => $sub
				));
			} elseif ($this->_getParam('words_type') == 2) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.' . Cubix_I18n::getTblField('about') . ' LIKE ?' => "%" . $this->_getParam('words') . "%"
				));
			}
		}

		if (count($this->_getParam('gender'))) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.gender IN ' . $this->__arrayForProcedureIN($this->_getParam('gender')) => ''
			));
		}

		if ($this->_getParam('available_for_incall')) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.incall_type IS NOT NULL' => true
			));
		}

		if ($this->_getParam('available_for_outcall')) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.outcall_type IS NOT NULL' => true
			));
		}

		if ($this->_getParam('age_from')) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.age >= ?' => $this->_getParam('age_from')
			));
		}
		if ($this->_getParam('age_to')) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.age <= ?' => $this->_getParam('age_to')
			));
		}

		if ($this->_getParam('height_from')) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.height >= ?' => $this->_getParam('height_from')
			));
		}
		if ($this->_getParam('height_to')) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.height <= ?' => $this->_getParam('height_to')
			));
		}

		if (count($this->_getParam('hair_color'))) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.hair_color IN ' . $this->__arrayForProcedureIN($this->_getParam('hair_color')) => ''
			));
		}

		if (count($this->_getParam('hair_length'))) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.hair_length IN ' . $this->__arrayForProcedureIN($this->_getParam('hair_length')) => ''
			));
		}

		if (count($this->_getParam('eye_color'))) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.eye_color IN ' . $this->__arrayForProcedureIN($this->_getParam('eye_color')) => ''
			));
		}

		if (count($this->_getParam('cup_size'))) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.cup_size IN ' . $this->__arrayForProcedureIN($this->_getParam('cup_size'), true) => ''
			));
		}

		if (count($this->_getParam('language'))) {
			$params['filter'] = array_merge($params['filter'], array(
				'CONCAT(",", e.languages, ",") REGEXP ",(' . implode('|', $this->_getParam('language')) . '),"' => ''
			));
		}

		if (count($this->_getParam('ethnicity'))) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.ethnicity IN ' . $this->__arrayForProcedureIN($this->_getParam('ethnicity')) => ''
			));
		}

		if (count($this->_getParam('nationality'))) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.nationality_id IN ' . $this->__arrayForProcedureIN($this->_getParam('nationality')) => ''
			));
		}

		if ($this->_getParam('reviews')) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.review_count >= 1' => ''
			));
		}

		if ($this->_getParam('video')) {
			$params['filter'] = array_merge($params['filter'], array(
				'video' => '1'
			));
		}

		if ($this->_getParam('vr')) {
			$params['filter'] = array_merge($params['filter'], array(
				'e.verified_status >= 2' => ''
			));
		}

		$existing_filters = Model_Escort_List::getActiveFilterMobileV2($params['filter']);

		die(json_encode(array($existing_filters)));
	}

    public function getCitiesGeoDataAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $all_cities = Model_Statistics::getAllCities();

        $c_json = json_decode( file_get_contents('cities.json') )->RECORDS;
        $check_data = array();

        foreach( $c_json as $key=>$val ){
            $check_data[ $key ] = $val->title_en;
        }

        for( $i = 0; $i < count( $all_cities ); $i+=1 ){
            $city_name = $all_cities[$i]->title_en;

            $key = array_search($city_name, $check_data);
            if( $key !== 0 && !$key ) continue;

            $lat = $c_json[ $key ]->latitude;
            $lng = $c_json[ $key ]->longitude;

            if( !$lat || !$lng ) continue;

            $coord = array(
                'latitude' => $lat,
                'longitude' => $lng
            );

            Model_Statistics::saveGeoData( $all_cities[$i]->city_id, $coord );
        }
    }

	private function _orderTitles($a,$b){
		if ( is_object($a) && is_object($b) ) {
			return strnatcmp($a->city_title, $b->city_title);
		} elseif (is_array($a) && is_array($b)) {
			return strnatcmp($a["city_title"], $b["city_title"]);
		} else return 0;
	}

	public function ajaxAlertsAction()
	{
		$this->view->layout()->disableLayout();

		$user = Model_Users::getCurrent();
		$escort_id = intval($this->_getParam('escort_id'));

		if ($user->user_type != 'member' || $escort_id == 0) {
			echo json_encode(array('events' => null, 'cities' => null));
			die;
		}

		$model = new Model_Members();

		$events = $model->getEscortAlerts($user->id, $escort_id);

		$arr = array();
		$cities = array();
		$cities_str = '';

		if ($events) {
			foreach ($events as $event) {
				$arr[] = $event['event'];

				if ($event['event'] == ALERT_ME_CITY)
					$cities_str = $event['extra'];
			}
		}

		if (strlen($cities_str) > 0) {
			$modelCities = new Model_Cities();

			$cts = $modelCities->getByIds($cities_str);

			foreach ($cts as $c) {
				$cc = array('id' => $c->id, 'title' => $c->title);
				$cities[] = $cc;
			}
		}

		echo json_encode(array('events' => $arr, 'cities' => $cities));
		die;
	}

	public function ajaxAlertsSaveAction()
	{
		$this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

		$user = Model_Users::getCurrent();
		$escort_id = intval($this->_getParam('escort_id'));
		$events = $this->_getParam('events');
		$cities = $this->_getParam('cities');

		if ($user->user_type != 'member' || $escort_id == 0) {
			echo 'error';
			die;
		}

		$model = new Model_Members();
		$c = $model->memberAlertsSave($user->id, $escort_id, $events, $cities);

		echo json_encode(array('status' => 'success', 'count' => $c));
		die;
	}

	private function __arrayForProcedureIN($_array, $_is_string = false)
	{
		if ($_is_string) {
			$_array = array_map(function ($val) {
				return "'" . $val . "'";
			}, $_array);
		}

		return '(' . implode(",", $_array) . ')';
	}

	public function ajaxGetPersonalSiteAction()
	{
		$this->view->layout()->disableLayout();
		$escort_id = (int)$this->_request->escort_id;
		$api_url = "http://www.escortbook.com/api.php?id=";

		$api = $api_url . urlencode($escort_id);

		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, $api);
		curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl_handle, CURLOPT_USERAGENT, 'escortbooking_api');
		$personal_site = curl_exec($curl_handle);
		curl_close($curl_handle);

		if ($personal_site == "ID NOT FOUND") {
			$this->view->personal_site = false;
		} else {
			$this->view->personal_site = $personal_site;
		}

		$this->view->has_rates = $this->_request->has_rates;
		$this->view->has_services = $this->_request->has_services;
	}

	public function ajaxInstantBookAction()
	{
		$this->view->layout()->disableLayout();
	}
		
	
//
//	/**
//	 * Instant Book Escorts Action
//	 */
//
//	public function instantBookAction() {
//		$action = 'index';
//		if ( ! is_null($this->_getParam('ajax')) ) {
//			$action = 'list';
//		}
//		$this->_helper->viewRenderer->setScriptAction($action);
//
//		$req = $this->_getParam('req');
//		$param = explode('/', $req);
//		$page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );
//		$model = new Model_EscortsV2();
//		$res = $model->getInstantBookEscortsList(1, 1000);
//		$ids = $res['ids'];
//		$this->view->escort_ids = explode(',', $ids);
//		if (strlen($ids) > 0) {
//			$filter = array(
//				'e.id IN (' . $ids . ')'
//			);
//
//			$page_num = 1;
//			if ( isset($page[1]) && $page[1] ) {
//				$page_num = $page[1];
//
//			}
//
//			$count = 0;
//			$escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 50, $count);
//		} else {
//			$count = 0;
//			$escorts = array();
//		}
//
//		$this->view->count = $count;
//		$this->view->escorts = $escorts;
//
//		$filter_body = $this->view->render('escorts/escort-list.phtml');
//
//		if ( ! is_null($this->_getParam('ajax')) ) {
//			$escort_list = $this->view->render('escorts/escort-list.phtml');
//
//			die(json_encode(array('filter_body' => $filter_body, 'escort_list' => $escort_list)));
//		}
//	}

	public function valentineAction()
	{

		// if(!isset($_GET['test'])){
		// 	header('HTTP/1.1 301 Moved Permanently');
		// 	header('Location: /');
		// 	die;
		// }
		$filter = array();
		$filter['valentine'] = 1;
		$per_page = 60;
		$page = 1;

		$this->view->user_votes = array();
		$this->view->user = null;
		$user = Model_Users::getCurrent();
		if(isset($user)){
			$this->view->user = $user;
			$this->view->user_votes = Model_Users::getUserVotes($user->id);
		}

		if($this->_request->isPost() && isset($user)){
			if(count($this->view->user_votes) >= 3){
				var_dump('limit');die;
			}
			$req = $this->_request;
			$model = new Model_EscortV2Item();
			$ip = Cubix_Geoip::getIP();
			$model->voteValentine($req->escort_id, $req->user_id, $req->photo_id, $ip);
			echo count(Model_Users::getUserVotes($user->id));die;
		}

		$escorts = Model_Escort_List::getFiltered($filter, 'random', 1, 100);
		// shuffle($escorts);
		$this->view->escorts = $escorts;
		$this->_helper->viewRenderer->setScriptAction('valentine');
	}
}
