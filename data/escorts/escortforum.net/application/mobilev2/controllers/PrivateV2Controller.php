<?php

class Mobilev2_PrivateV2Controller extends Zend_Controller_Action
{
	/**
	 * @var Model_Escort_Profile
	 */
	protected $profile;

	/**
	 * @var Model_EscortItem
	 */
	protected $escort;

	/**
	 * @var Model_AgencyItem
	 */
	protected $agency;

	public $testEscortId = 72469;

    public $zona_rossa_packages = [162,165,168,171,174,177,183,186];
    
	public static $linkHelper;

	const SECRET_PREFIX = 'A7W9DC';
	const GATEWAY_URL = 'https://www.6payment.com/payment';
	const CURRENCY = 'EUR';

	const STATUS_PENDING  = 1;
	const STATUS_ACTIVE   = 2;
	const STATUS_EXPIRED  = 3;
	const STATUS_CANCELLED = 4;
	const STATUS_UPGRADED = 5;
	const STATUS_SUSPENDED = 6;
	
	public function init()
	{
		//$this->view->layout()->setLayout('private-v2');
		$this->view->layout()->setLayout('mobile-private');

		$cache = Zend_Registry::get('cache');
		self::$linkHelper = $this->view->getHelper('GetLink');
		$this->_request->setParam('no_tidy', true);

		$anonym = array();

		$this->user = Model_Users::getCurrent();
		$this->defines = $this->view->defines = Zend_Registry::get('defines');
		$this->client = Cubix_Api::getInstance();

		if ( in_array($this->_request->getActionName(), array('upgrade')) ) {
			return;
		}

		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			
			if ($_SERVER['HTTP_REFERER']) {
				$session_redirect = new Zend_Session_Namespace('redirect');
				$session_redirect->url = $_SERVER['HTTP_REFERER'];
			}

			$this->_redirect($this->view->getLink('signin'));
			return;
		}

		$cache_key =  'v2_user_pva_' . $this->user->id;

		if ( $this->user->isAgency() ) {

			if ( ! $agency = $cache->load($cache_key) ) {
				$agency = $this->user->getAgency();
				$cache->save($agency, $cache_key, array(), 300);
			}

			$this->agency = $this->view->agency = $agency;

			$this->agencyDashboardAction();
			$this->view->layout()->enableLayout();
		}
		else if ( $this->user->isEscort() ) {

			if ( ! $escort = $cache->load($cache_key) ) {
				$escort = $this->user->getEscort();
				$cache->save($escort, $cache_key, array(), 300);
			}


			if ($this->testEscortId == $escort->id && $this->user->hasProfile() && !in_array($this->_request->getActionName(), array('confirm-phone'))) {
				$client = new Cubix_Api_XmlRpc_Client();
				$status = $client->call('Escorts.escortHasConfirmedPhone', array($escort->id));

				if(!isset($status) || $status == 0){
					$this->_redirect($this->view->getLink('confirm-phone'));	
				}
			}

			$this->escort = $this->view->escort = $escort;

			/* Grigor Update */
			if ( $this->escort->status & Model_Escorts::ESCORT_STATUS_SUSPICIOUS_DELETED &&
				in_array($this->_request->getActionName(), array( 'tours', 'escort-reviews', 'settings', 'support', 'happy-hour', 'client-blacklist', 'plain-photos'  )) ) {
				$this->_redirect($this->view->getLink('private-v2'));
			}
			/* Grigor Update */
		}

		$this->view->user = $this->user;

		$this->view->escort = $this->escort;
	}

	protected $_c = 0;

	protected $steps = array('biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times', 'prices', 'contact-info'/*, 'finish'*/);
	protected $_posted = false;

	public function indexAction()
	{
		$user = $this->user;
		$model = new Model_Users();
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		if ( $user->user_type == 'escort' ) {
			/*$m = new Model_EscortsV2();
			$e = $m->getByUserId($user->id);*/

			/* Easter LOTTERY */
			/*if($e->gender == GENDER_FEMALE){
				$user->easter_lottery = $model->checkEasterLottery($user->id);
			}*/
		} elseif ( $user->user_type == 'agency' ) {
			/*$m = new Model_Agencies();
			$a = $m->getByUserId($user->id);*/

			/* Easter LOTTERY */
			/*$user->easter_lottery = $model->checkEasterLottery($user->id);*/
		}

		$comments_page = 1;
		$comments_per_page = 3;
		$com_model = new Model_Comments();

		$this->view->hideBack = true;

		$reviews_page = 1;
		$reviews_per_page = 10;

		$modelSupport = new Model_Support();
		//$supportUnreadsCount = $modelSupport->getUnreadsCount($this->user->id);

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isAgency() ) {

			$this->_helper->viewRenderer->setScriptAction('agency-index');


			//$private_messaging = new Cubix_PrivateMessaging('agency', $this->agency->id);
			//$unread_threads_count = $private_messaging->getUnreadThreadsCount();

			$nav = array();
			$nav[] = array('id' => 'agency-profile', 'title' => __('pv2_btn_agency_profile'), 'link' => 'private-v2-agency-profile');
		// $nav[] = array('id' => 'profile', 'title' => __('pv2_btn_add_escort_profile'), 'link' => 'private-v2-profile');
			$nav[] = array('id' => 'models', 'title' => __('pv2_btn_manage_models'), 'link' => 'private-v2-escorts');

			//$nav[] = array('id' => 'buy_your_ad', 'title' => __('pv2_btn_buy_your_ad'), 'link' => 'online-billing');


			$nav[] = array('id' => 'tours', 'title' => __('pv2_btn_city_tours'), 'link' => 'private-v2-tours');

			$nav[] = array('id' => 'verification', 'title' => __('pv2_btn_100p_verified'), 'link' => '100p-verify');

			if ($_GET['test'])
                $nav[] = array('id' => 'premium', 'title' => __('pv2_btn_premium_girls'), 'link' => 'private-v2-premium');

            if (!$this->agency->disabled_reviews)
				$nav[] = array('id' => 'myreviews', 'title' => __('pv2_btn_my_escorts_reviews'), 'link' => 'private-v2-escorts-reviews');


			// $nav[] = array('id' => 'premium', 'title' => __('pv2_btn_premium_girls'), 'link' => 'private-v2-premium');
			$nav[] = array('id' => 'support', 'title' => 'Support', 'link' => 'private-v2-support', 'supportUnreadsCount' => $supportUnreadsCount);
			$nav[] = array('id' => 'settings', 'title' => __('pv2_btn_settings'), 'link' => 'private-v2-settings');
			// TODO:
			//$nav[] = array('id' => 'statistics', 'title' => __('pv2_btn_statistics'), 'link' => 'private-v2-statistics');

			//$nav[] = array('id' => 'happy_hour', 'title' => __('pv2_btn_happy_hour'), 'link' => 'private-v2-happy-hour');

			//if ( $this->_request->bl )
			$count = $client->call('Escorts.getAgencyPackagesCount', array($this->user->id));
			if ($count > 0)
			{
				// TODO:
				 $nav[] = array('id' => 'client_blacklist', 'title' => __('pv2_btn_client_blacklist'), 'link' => 'private-v2-client-blacklist');

				//$nav[] = array('id' => 'forum', 'title' => __('pv2_btn_forum'), 'link' => 'girlforum');

				// Forum Api
				/*$forum = new Cubix_Forum2Api(true);
				$data = $forum->getLastTopicFirstPost();
				
				$post_data = json_decode($data);
				
				if ($post_data->post)
				{
					$this->view->last_post = $post_data->post;
					$this->view->last_post_topic_slug = $post_data->topic_slug;
				}*/
				//
			}

			//$nav[] = array('id' => 'faq', 'title' => 'FAQ', 'link' => 'private-v2-faq');
			//$nav[] = array('id' => 'gotd', 'title' => __('pv2_btn_gotd'), 'link' => 'private-v2-gotd');
			//$nav[] = array('id' => 'private_messaging', 'title' => __('private_messages'), 'link' => 'private-messaging', 'unread_threads_count' => $unread_threads_count);
			$nav[] = array('id' => 'logout', 'title' => __('pv2_btn_logout'), 'link' => 'signout');

			$this->view->navigation = $nav;
			$this->view->escorts = $this->agency->getEscorts();

			$this->view->has_age_ver_escort = $this->agency->hasAgeVerEscort();
			$rejected_escorts = $this->agency->getAgeVerEscorts();

			foreach( $rejected_escorts as $index => $rejected_escort ){
				$rejected_escorts[$index]['reasons'] = $this->view->reasons = $client->call('Escorts.getRejectedCertificationForEscort', array( $rejected_escort['id'] ));
			}

			$this->view->rejected_escorts = $rejected_escorts;


			$escort_ids = array();
			foreach($this->view->escorts as $escort){
				$escort_ids[] = $escort['id'];
			}
			$this->view->is_agency = 1;
			$this->view->agency_escorts = Model_Escorts::getAgencyEscorts($this->agency->id);
			$this->view->escort_id = json_encode($escort_ids);

			if (!$this->agency->disabled_comments)
			{
				$this->view->comments_page = $comments_page;
				$this->view->comments_per_page = $comments_per_page;
				$this->view->comments = count($escort_ids) ? $com_model->getCommentsByEscortIds($escort_ids,$comments_page,$comments_per_page, $count): null;
				$this->view->comments_count = $count;
			}
			if (!$this->agency->disabled_reviews)
			{
				$lng = Cubix_I18n::getLang();
				$this->view->reviews_page = $reviews_page;
				$this->view->reviews_per_page = $reviews_per_page;
				$results = Cubix_Api::getInstance()->call('getReviewsForAgencyEscorts', array($this->user->id, $lng, $reviews_page, $reviews_per_page));
				$this->view->reviews = $results['reviews'];
				$this->view->reviews_count = $results['count'];

			}
		}
		else if ( $this->user->isEscort() ) {
			$this->_helper->viewRenderer->setScriptAction('escort-index');

			$private_messaging = new Cubix_PrivateMessaging('escort', $this->escort->id);
			$unread_threads_count = $private_messaging->getUnreadThreadsCount();

			// Determine the mode depending on if user has profile
			$this->mode = (($this->user->hasProfile()) ? 'update' : 'create');

			$nav = array();

			$this->view->escort_package = $escort_package = $client->call('Escorts.checkIfHasActivePackage', array($this->escort->id));

            if($this->_request->getParam('testing')) {
                $nav[] = array('id' => 'instantbook-set-available', 'title' => __('set_available_for_booking'), 'link' => 'instant-book-set-available');
            }

			if(!$this->escort->is_suspicious){
                $nav[] = array('id' => 'profile', 'title' => __('mob_my_profile'), 'link' => 'private-v2-profile');
            }

			if ( ($this->escort->status & Model_Escorts::ESCORT_STATUS_SUSPICIOUS_DELETED) == 0 ) {
				$nav[] = array('id' => 'photos', 'title' => __('pv2_btn_add_edit_photos'), 'link' => 'private-photo-video-main');

				$nav[] = array('id' => 'tours', 'title' => __('pv2_btn_set_tours'), 'link' => 'private-v2-tours');
			}

            // TODO:
			$nav[] = array('id' => 'verification', 'title' => __('pv2_btn_100p_verified'), 'link' => '100p-verify');

            if ( $this->escort->hasProduct(14) /*&& $_GET['test']*/ ) {
                $nav[] = array('id' => 'premium', 'title' => __('pv2_change_premium_city'), 'link' => 'private-v2-premium');
            }

			if ( ($this->escort->status & Model_Escorts::ESCORT_STATUS_SUSPICIOUS_DELETED) == 0 ) {

				//$nav[] = array('id' => 'buy_your_ad', 'title' => __('pv2_btn_buy_your_ad'), 'link' => 'online-billing');

				//$nav[] = array('id' => 'premium', 'title' => 'Go Premium', 'link' => 'private-v2-premium');
				if (!$this->escort->disabled_reviews)
					$nav[] = array('id' => 'myreviews', 'title' => __('pv2_btn_my_reviews'), 'link' => 'private-v2-escort-reviews');

				//$nav[] = array('id' => 'sedcard_stats', 'title' => __('pv2_btn_sedcard_stats'), 'link' => 'private-v2-settings');
				//$nav[] = array('id' => 'my_video', 'title' => __('pv2_btn_my_video'), 'link' => 'private-v2-settings');
				$nav[] = array('id' => 'settings', 'title' => __('pv2_btn_settings'), 'link' => 'private-v2-settings');
				$nav[] = array('id' => 'support', 'title' => 'Support', 'link' => 'private-v2-support', 'supportUnreadsCount' => $supportUnreadsCount);

				//$nav[] = array('id' => 'happy_hour', 'title' => __('pv2_btn_happy_hour'), 'link' => 'private-v2-happy-hour');

				// TODO:
				//$nav[] = array('id' => 'statistics', 'title' => __('pv2_btn_statistics'), 'link' => 'private-v2-statistics');

				//if ( $this->_request->bl )
				//$nav[] = array('id' => 'client_blacklist', 'title' => __('pv2_btn_client_blacklist'), 'link' => 'private-v2-client-blacklist');
			//$nav[] = array('id' => 'faq', 'title' => 'FAQ', 'link' => 'private-v2-faq');
			// if ( $escort_package ) {
				if ($this->user->id == 277346 || $this->user->id == 345573) {
					$nav[] = array('id' => 'instant-book-history', 'title' => __('instant_book_history'), 'link' => 'private-v2-instant-book-history');
				}
			// }

			}

			$count = $client->call('Escorts.getPackagesCount', array($this->escort->id));

			if ($count > 0)
			{
				// TODO:
				$nav[] = array('id' => 'client_blacklist', 'title' => __('pv2_btn_client_blacklist'), 'link' => 'private-v2-client-blacklist');

				//$nav[] = array('id' => 'forum', 'title' => __('pv2_btn_forum'), 'link' => 'girlforum');

				// Forum Api
				$forum = new Cubix_Forum2Api(true);
				$data = $forum->getLastTopicFirstPost();

				$post_data = json_decode($data);

				if ($post_data->post)
				{
					$this->view->last_post = $post_data->post;
					$this->view->last_post_topic_slug = $post_data->topic_slug;
				}
				//
			}

			if ( $escort_package ) {
				$nav[] = array('id' => 'gotd', 'title' => __('pv2_btn_gotd'), 'link' => 'private-v2-gotd');
				$nav[] = array('id' => 'boost_profile', 'title' => __('pv2_btn_boost_profile'), 'link' => 'ob-profile-boost');
			}

			$nav[] = array('id' => 'private_messaging', 'title' => __('private_messages'), 'link' => 'private-messaging', 'unread_threads_count' => $unread_threads_count);
			$nav[] = array('id' => 'logout', 'title' => __('pv2_btn_logout'), 'link' => 'signout');

			$this->view->navigation = $nav;
			$this->view->is_agency = 0;
			$this->view->escort_id = $this->escort->id;
			$this->view->skype = $client->call('Skype.get', array( $this->escort->id ) );
			//$this->view->earned_skype_sum = $client->call('Skype.earnedsum', array( $this->escort->id ) );

			if (!$this->escort->disabled_comments)
			{
				$count = null;
				$this->view->comments_page = $comments_page;
				$this->view->comments_per_page = $comments_per_page;
				$this->view->comments = $com_model->getEscortComments($comments_page,$comments_per_page, $count, $this->escort->id);
				$this->view->comments_count = $count;
			}

			if (!$this->escort->disabled_reviews)
			{
				$lng = Cubix_I18n::getLang();
				$this->view->reviews_page = $reviews_page;
				$this->view->reviews_per_page = $reviews_per_page;
				$results = Cubix_Api::getInstance()->call('getReviewsForEscort', array($this->user->id, $lng,$this->escort->id, $reviews_page, $reviews_per_page));
				$this->view->reviews = $results['reviews'];
				$this->view->reviews_count = $results['count'];
			}

			if( $this->escort->need_age_verification == 4 ){
				//$client->call('Escorts.profileStatus', array($escort_id,$action));
				$this->view->reasons = $client->call('Escorts.getRejectedCertificationForEscort', array( $this->escort->id ));
			}

			// GET INSTANT BOOKINGS BOOKINGS
			$instant_booking_model = new Model_InstantBook();
			$this->view->instant_bookings = $instant_bookings = $instant_booking_model->getInstantBookings($this->escort->id);
			$user = Model_Users::getCurrent(); 
			$instant_bookings_pending = $instant_bookings_accepted = array();
			
			foreach($instant_bookings as $ib) {
				switch($ib['status']) {
					case 'pending':
						$instant_bookings_pending[] = $ib;
						break;
					case 'accept':
						$instant_bookings_accepted[] = $ib;
						break;
					case 'deny':
					break;
				}
			}

			$this->view->instant_bookings_pending = $instant_bookings_pending;
			$this->view->instant_bookings_accepted = $instant_bookings_accepted;
			
		}

		else if ( $this->user->isMember() ) {
			$is_premium = false;
			if ( isset($this->user->member_data) ) {
				$this->view->is_premium = $is_premium = $this->user->member_data['is_premium'];
			}

			//$private_messaging = new Cubix_PrivataeMessaging('member', $this->user->id);
			//$unread_threads_count = $private_messaging->getUnreadThreadsCount();

			$this->_helper->viewRenderer->setScriptAction('member-index');

			// Determine the mode depending on if user has profile
			$this->mode = (($this->user->hasProfile()) ? 'update' : 'create');

			$nav = array();
			$nav[] = array('id' => 'profile', 'title' => __('pv2_btn_member_profile'), 'link' => 'private-v2-member-profile');

			/*if ( ! $is_premium )
				$nav[] = array('id' => 'upgradepremium', 'title' => __('pv2_btn_up_to_premium'), 'link' => 'private-v2-upgrade-premium');*/

			$nav[] = array('id' => 'myfavourites', 'title' => __('pv2_btn_my_favorites'), 'link' => 'favorites');
			// $nav[] = array('id' => 'myalerts', 'title' => __('pv2_btn_my_alerts'), 'link' => 'alerts');
			$nav[] = array('id' => 'myreviews', 'title' => __('pv2_btn_my_reviews'), 'link' => 'private-v2-reviews');
			$nav[] = array('id' => 'settings', 'title' => __('pv2_btn_settings'), 'link' => 'private-v2-settings');
			$nav[] = array('id' => 'support', 'title' => 'Support', 'link' => 'private-v2-support', 'supportUnreadsCount' => $supportUnreadsCount);
			// $nav[] = array('id' => 'faq', 'title' => 'FAQ', 'link' => 'private-v2-faq');
			// $nav[] = array('id' => 'private_messaging', 'title' => __('private_messages'), 'link' => 'private-messaging', 'unread_threads_count' => $unread_threads_count);
			$nav[] = array('id' => 'logout', 'title' => __('pv2_btn_logout'), 'link' => 'signout');

			$this->view->navigation = $nav;

			$this->view->need_modification_review = Cubix_Api::getInstance()->call('hasNeedModificationReviews', array($this->user->id));
		}
		
	}

	private function dateDiff($date_start, $date_end)
	{
	    $difference = $date_end - $date_start;
        $minutes = floor(($difference / 3600) * 60); // 3600 seconds in an hour

		return $minutes;
	}

	const HH_STATUS_ACTIVE  = 1;
	const HH_STATUS_PENDING = 2;
	const HH_STATUS_EXPIRED = 3;

	private function makeDate($week_day, $h_from, $h_to)
	{
		$month = date('m', $week_day);
		$day = date('d', $week_day);
		$year = date('Y', $week_day);

		$day_to = $day;
		if ( $h_to < $h_from ) {
			$day_to += 1;
		}

		return array('hh_date_from' => mktime($h_from, 0, 0, $month, $day, $year), 'hh_date_to' => mktime($h_to, 0, 0, $month, $day_to, $year));
	}

	public function happyHourAction()
	{
		$req = $this->_request;
		$data = $req->data;

		if ( $req->isPost() ) {
			$validator = new Cubix_Validator();
			$def_currency = Model_Applications::getDefaultCurrencyTitle();
			foreach ( $data as $escort_id => $d ) {

				$hh_status = Cubix_Api::getInstance()->call('getHappyHourStatus', array($escort_id));

				if ( ! isset($data[$escort_id]['hh_save']) ) {
					$data[$escort_id]['hh_save'] = 0;
				}

				/*if ( ! strlen($d['hh_motto']) )
					$validator->setError('err_motto', 'Required');*/

				if ( $hh_status == self::HH_STATUS_PENDING )
				{
					if ( ! isset($d['hh_week_day']) || ! isset($d['hh_hour_from']) || ! isset($d['hh_hour_to']) ) {
						$validator->setError('err_date', 'Date is Required');
					}
					else {
						$dates = $this->makeDate($d['hh_week_day'], $d['hh_hour_from'], $d['hh_hour_to']);
						$data[$escort_id]['hh_date_from'] = $dates['hh_date_from'];
						$data[$escort_id]['hh_date_to'] = $dates['hh_date_to'];
						$d['hh_date_from'] = $dates['hh_date_from'];
						$d['hh_date_to'] = $dates['hh_date_to'];


						$diff_minutes = $this->dateDiff($d['hh_date_from'], $d['hh_date_to']);

						if ( ! strlen($d['hh_date_from']) || ! strlen($d['hh_date_to']) )
							$validator->setError('err_date', 'Date is Required');
						else if ( $d['hh_date_to'] < $d['hh_date_from'] )
							$validator->setError('err_date', 'Date until must be bigger then Date From');
						else if ( $d['hh_date_from'] < time() )
							$validator->setError('err_date', 'Date must be in the future');
						else if ( ! $this->user->isAgency() && $diff_minutes > 300 )
							$validator->setError('err_date', 'The duration of happy hour could not be grater then 5 hour !');
					}

					if ( $this->user->isAgency() ) {
						$diff_hour = $diff_minutes / 60;
						$used_time = Cubix_Api::getInstance()->call('getAgencyUsedHHHours', array($this->agency->id, $escort_id));

						if ( $used_time + $diff_hour > 10 )
							$validator->setError('err_date', 'The duration of happy hour could not be grater then ' . (10 - $used_time) . ' hour !');
					}


					if ( isset($d['outcall_rates']) && count($d['outcall_rates']) > 0 ) {
						$has_hh_price = false;
						foreach ( $d['outcall_rates'] as $rate_id => $ir ) {
							if( strlen($ir) ) {

								if ( $d['old_outcall_rates'][$rate_id] < $ir + 10) {
									$validator->setError('err_hh_hour_outcall', 'please make sure to specify smaller price at least 10 '.$def_currency);
								}

								if ( ! $has_hh_price ) {
									$has_hh_price = true;
								}
							}
						}
						if ( ! $has_hh_price )
							$validator->setError('err_hh_hour_outcall', 'At least one "Happy Hour Rate" is Required for Outcall');
					}

					if ( isset($d['incall_rates']) && count($d['incall_rates']) > 0 ) {
						$has_hh_price = false;
						foreach ( $d['incall_rates'] as $rate_id => $ir ) {
							if( strlen($ir) ) {

								if ( $d['old_incall_rates'][$rate_id] < $ir + 10 ) {
									$validator->setError('err_hh_hour', 'please make sure to specify smaller price at least 10 '.$def_currency);
								}

								if ( ! $has_hh_price ) {
									$has_hh_price = true;
								}
							}
						}

						if ( ! $has_hh_price )
							$validator->setError('err_hh_hour', 'At least one "Happy Hour Rate" is Required for Incall');
					}
				}
			}

			if ( $validator->isValid() ) {
				//print_r($data); die;
				Cubix_Api::getInstance()->call('setHappyHour', array($data));
			}

			die(json_encode($validator->getStatus()));
		}

		if ( $this->user->isAgency() ) {
			$this->view->escorts = $escorts_a = $this->agency->getEscortsPerPage(1, 1000, Model_Escorts::ESCORT_STATUS_ACTIVE);
		}
	}

	public function happyHourResetAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$escort_id = intval($req->escort_id);

		$escort_data = Cubix_Api::getInstance()->call('resetHappyHour', array($escort_id));
		die;
	}

	public function happyHourFormAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		Cubix_I18n::setLang($req->lang_id);
		$escort_id = intval($req->escort_id);

		$hh_status = $this->view->hh_status = Cubix_Api::getInstance()->call('getHappyHourStatus', array($escort_id));

		$escort_data = Cubix_Api::getInstance()->call('getHappyHour', array($escort_id));

		if ( $hh_status == Model_EscortsV2::HH_STATUS_EXPIRED ) {
			if ( $escort_data['agency_id'] ) {
				$dateDiff = strtotime('+ 7 days', $escort_data['hh_date_to']) - time();
			}
			else {
				$dateDiff = strtotime('+ 3 days', $escort_data['hh_date_to']) - time();
			}
			$days = floor($dateDiff / (60*60*24));
			$this->view->activate_after_days = $days;
		}

		$this->view->escort_data = $escort_data;
		$this->view->defines = Zend_Registry::get('defines');
	}

	public function escortAgeVerificationAction()
	{
       	$this->view->layout()->setLayout('mobile-private');
        
		if($this->user->isAgency()){
			$this->view->sel_escort_id = intval($this->_request->escort_id);
			//$this->_helper->viewRenderer->setScriptAction('agency-escort-age-verification');
            /*$model = new Model_AgeVerification();
            $AgeVerification = $model->getByEscortId($escort_id);
            $this->view->AgeVerificationStatus = $AgeVerification->status;*/
			
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$agency = $this->user->getAgency();
            $needVerEscorts = $client->call('Agencies.getNeedVerEscorts', array($agency->id));
            // if there was an pending AC
            $pndIds = $this->getPendingVerificationEscortIds();
            if (!empty($pndIds)) {
                $needVerEscorts = array_filter($needVerEscorts, function ($v) use ($pndIds) {
                    return !in_array(intval($v['id']), $pndIds);
                });
                $this->setPendingVerificationEscortIds($pndIds);
            }
			$this->view->is_agency = true;
			$this->view->id_card_certification = true;
            $this->view->needVerEscorts = $needVerEscorts;
		}
		else{
			$escort_data = $this->user->getEscort();
			$this->view->need_age_verification = $escort_data->need_age_verification;
			$this->view->need_video = $escort_data->need_age_verify_video;
			$is_pending = ($v_r = $this->escort->getVerifyRequest()) ? $v_r->isPending() : false ;
			
            if( !$this->escort->isVerified() && !$is_pending ){
                $this->view->id_card_certification = true;
            }
			
            $pndIds = $this->getPendingVerificationEscortIds();
            if (in_array(intval($escort_data['id']),$pndIds)){
                $this->view->success = true;
            }
			
            if($escort_data->need_age_verification == 2 ||$escort_data->need_age_verification == 3){
                $this->setPendingVerificationEscortIds(null);
            }
		}
	}

	public function certifyOverviewAction()
	{
		$this->_helper->viewRenderer->setScriptAction('certification-success');
	}


	/* Grigor Update */

    public function profileStatusAction(){
        if ( $this->user->isEscort() ) {

            $escort = $this->user->getEscort();
			$escort_id = $escort->id;

            $client = Cubix_Api_XmlRpc_Client::getInstance();

            $action = $this->_request->getParam('act');
            $client->call('Escorts.profileStatus', array($escort_id,$action));
            $this->_redirect($this->view->getLink('private-v2'));

        }
    }

    public function profileStatusAjaxAction(){
        if ( $this->user->isEscort() ) {
            $escort = $this->user->getEscort();
            $escort_id = $escort->id;

            $client = Cubix_Api_XmlRpc_Client::getInstance();

            $action = $this->_request->getParam('act');
            $client->call('Escorts.profileStatus', array($escort_id,$action));

            /* clear cache */
            $cache = Zend_Registry::get('cache');
            $cache_key =  'v2_user_pva_' . $this->user->id;
            $cache->remove($cache_key);
            /**/

            //$_SESSION['auth']['current_user']['escort_data']['profile_status'] = $action;
            $_SESSION['profile_status'] = $action;

            if ( !is_null($this->_getParam('ajax')) ) {
                echo(json_encode(array("success" => 1)));
                die;
            } else {
                $this->_redirect($this->view->getLink('private-v2'));
            }
        }
    }

    public function profileDeleteAction(){
        if ( $this->user->isEscort() ) {
            $this->view->layout()->disableLayout();
            $escort = $this->user->getEscort();
			$escort_id = $escort->id;

            if ( $this->_request->isPost() ) {

                $validator = new Cubix_Validator();

                $req = $this->_request;

				if (!trim($req->comment))
                    $validator->setError('comment', 'Required');

                if ( $validator->isValid() ) {
                    $leaving_reason = $req->comment;
                    $del_hash = Cubix_Salt::generateSalt($escort->showname);

                    $client = Cubix_Api_XmlRpc_Client::getInstance();
                    $client->call('Escorts.addDelData', array($escort_id,$del_hash,$leaving_reason));

                    Cubix_Email::sendTemplate('escort_delete', $this->user->email, array(
                        'del_hash' => $del_hash,
                        'email' => $this->user->email,
                        'showname' => $escort->showname
                    ));
                }


                die(json_encode($validator->getStatus()));
            }
        }
    }

    public function profileRestoreAction(){
        if ( $this->user->isEscort() ) {
            $this->view->layout()->disableLayout();
            $escort = $this->user->getEscort();
			$escort_id = $escort->id;

            $client = Cubix_Api_XmlRpc_Client::getInstance();
            $client->call('Escorts.restore', array($escort_id));

            $cache = Zend_Registry::get('cache');
            $cache_key =  'v2_user_pva_' . $this->user->id;
            $cache->remove($cache_key);

            $this->_response->setRedirect($this->view->getLink('private-v2'));
        }
    }

    public function confirmDeletionAction(){
        if ( $this->user->isEscort() ) {

            $escort = $this->user->getEscort();
			$escort_id = $escort->id;

            $hash = $this->_getParam('hash');
            $model = new Model_EscortsV2();

            $us = $model->getById($escort_id);

            if ( $model->checkhash($escort_id, $hash) ) {

                $client = Cubix_Api_XmlRpc_Client::getInstance();
                $res = $client->call('Escorts.deleteTemporary', array($escort_id));

                $res = json_decode($res);

                if( isset($res->success) && $res->success ){
                    $cache = Zend_Registry::get('cache');
                    $cache_key =  'v2_user_pva_' . $this->user->id;
                    $cache->remove($cache_key);

                    $this->_response->setRedirect($this->view->getLink('private-v2'));
                }

            }
        }
        $this->_response->setRedirect($this->view->getLink('private-v2'));
    }


//    public function restoreAction(){
//        if ( $this->user->isAgency() ) {
//			$escort_id = intval($this->_getParam('escort_id'));
//			if ( ! $this->agency->hasEscort($escort_id) ) {
//				die('Permission denied!');
//			}
//		}
//		elseif ( $this->user->isEscort() ) {
//			$escort_id = $this->escort->getId();
//		}
//		if ( ! $escort_id ) die;
//
//
//        $client = Cubix_Api_XmlRpc_Client::getInstance();
//
//        $client->call('Escorts.restore', array($escort_id));
//        exit;
//    }
//
//    public function undoDeletionAction(){
//        if ( $this->user->isEscort() ) {
//
//            $escort = $this->user->getEscort();
//			$escort_id = $escort->id;
//
//            $model = new Model_EscortsV2();
//
//            $us = $model->getById($escort_id);
//
//
//            $client = Cubix_Api_XmlRpc_Client::getInstance();
//            $client->call('Escorts.restore', array($escort_id));
//
//        }
//        $this->_response->setRedirect($this->view->getLink());
//    }
    /* Grigor Update */

	public function agencyDashboardAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$agency_id = $this->agency['id'];
		if ( $req->agency_id ) {
			$agency_id = $req->agency_id;
		}

		$page = 1;
		if ( $req->de_page ) {
			$page = intval($req->de_page);
		}

		if ( $page < 0 ) {
			$page = 1;
		}

		$per_page = 10;

		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$cache = Zend_Registry::get('cache');

		$cache_key =  'v2_agency_escorts_' . $this->agency['id'] . '_page_' . $page . $per_page;
		if ( ! $escorts = $cache->load($cache_key) ) {
			$escorts = $escorts = $client->call('Agencies.getNotActiveEscorts', array($agency_id, $page, $per_page));
			$cache->save($escorts, $cache_key, array(), 300);
		}

		$d_escorts = $escorts['result'];
		$d_escorts_count = $escorts['count'];

		$this->view->dash_escorts = $d_escorts;
		$this->view->dash_escorts_count = $d_escorts_count;
		$this->view->dash_agency_id = $agency_id;
		$this->view->dash_page = $page;
		$this->view->dash_per_page = $per_page;

	}

	public function upgradeAction()
	{
		$this->view->layout()->setLayout('private');

		$sess = new Zend_Session_Namespace('private');

		if ( $this->user && ! $this->user->isMember() ) {
			header('Location: /');
			die;
		}

		if ( ! $this->user && ! $sess->want_premium ) {
			header('Location: /' . Cubix_I18n::getLang() . '/private-v2/signin');
			die;
		}

		if ( $this->user ) {
			$this->view->user = $this->user;
		}
		elseif ( $sess->want_premium ) {
			$this->view->user = $this->user = $sess->want_premium;
		}

		if ( $this->_request->isPost() ) {
			$plan = (int) $this->_getParam('plan');
			switch ( $plan ) {
				case 2:
					$amount = 8995;
					break;
				default:
					$amount = 995;
			}
			$this->_redirect(Cubix_CGP::getPaymentUrl(array(
				'ref' => md5('m_' . $this->user->member_data['id']),
				'email' => $this->user->email,
				'amount' => $amount
			)));
		}
	}

	public function upgradeSuccessAction()
	{

	}

	public function upgradeFailedAction()
	{

	}

	public function settingsAction()
	{
		if ( $this->user->isAgency() ) {
			$this->_helper->viewRenderer->setScriptAction('agency-settings');
		}
		else if ( $this->user->isEscort() ) {
			$this->_helper->viewRenderer->setScriptAction('escort-settings');
		}
		else {
			$this->_helper->viewRenderer->setScriptAction('member-settings');
		}

		if ( $this->user->isMember() ) {
			$this->view->data = $this->user->getData(array(
				'recieve_newsletters'
			));
		}
		else {
			$this->view->data = $this->user->getData(array(
				'country_id', 'city_id', 'email', 'recieve_newsletters'
			));
		}

        //$this->view->escort_status = $_SESSION['auth']['current_user']['escort_data']['profile_status'];
        $this->view->escort_status = $_SESSION['profile_status'];

		if ($this->user->isEscort()) {
			$countyModel = new Model_Countries();
			$this->view->phone_countries = $countyModel->getPhoneCountries();

			$client = new Cubix_Api_XmlRpc_Client();
            $this->view->settings = $settings = $client->call('InstantBook.getInstantBookContactInfo', array($this->escort->id));
        }

		if ( $this->_request->isPost() ) {

			$validator = new Cubix_Validator();

			$form = new Cubix_Form_Data($this->_request);
			if ( $this->user->isMember() ) {

				$save_password = $this->_getParam('password');
				$save_newsletters = $this->_getParam('newsletters');

				$form->setFields(array(
					'password' => '',
					'new_password' => '',
					'new_password_2' => '',
					'recieve_newsletters' => ''
				));
			}
			else {
				$save_profile = $this->_getParam('profile');
				$save_password = $this->_getParam('password');
				$save_newsletters = $this->_getParam('newsletters');
				$save_instant_book_mobile = $this->_getParam('instant_book_mobile');

				$form->setFields(array(
					'country_id' => 'int-nz',
					'city_id' => 'int-nz',
					'email' => '',
					'password' => '',
					'new_password' => '',
					'new_password_2' => '',
					'recieve_newsletters' => '',
					'instant_book_mobile' => ''
				));
			}
			$data = $form->getData();

			switch ( true ) {
				case ! is_null($save_profile):
					$model = new Cubix_Geography_Countries();

					if ( ! is_null($data['country_id']) && ! $model->exists($data['country_id']) ) {
						$data['country_id'] = null;
					}

					if ( ! is_null($data['city_id']) ) {
						if ( is_null($data['country_id']) || ! $model->hasCity($data['country_id'], $data['city_id']) ) {
							$data['city_id'] = null;
						}
					}

					if ( $validator->isValid() ) {
						$this->user->updateData(array(
							'country_id' => $data['country_id'],
							'city_id' => $data['city_id']
						));
						$this->view->data = $this->user->getData(array(
							'country_id', 'city_id', 'email', 'recieve_newsletters'
						));

						//newsletter email log
						Cubix_Api::getInstance()->call('addNewlstterEmailLog', array($this->user->id, $this->user->user_type, 'edit', $emails));
						//
					}
					else {
						$this->view->data = $data;
					}

					break;
				case ! is_null($save_password):
					$password = $this->_getParam('old_password');

					$new_password = $this->_getParam('new_password');
					$new_password_2 = $this->_getParam('new_password_2');

					if( !$password ){
						$validator->setError('old_password', str_replace( "'", "", __('empty_passwd_err') ));
					} elseif ( !$new_password ){
						$validator->setError('new_password', str_replace( "'", "", __('new_password_empty_err') ));
					} elseif ( !$new_password_2 ){
						$validator->setError('new_password_2', str_replace( "'", "", __('new_password_empty_err') ));
					} elseif ( strtolower($new_password) == strtolower($this->user->username)) {
						$validator->setError('new_password', __('username_equal_password'));
						$this->view->usernameError = __('username_equal_password');
					} else {
						if ( $new_password !== $new_password_2 ) {
							$validator->setError('new_password', 'Passwords does not match');
						} else if ( strlen($new_password) > 0 ) {
							try {
								$this->user->updatePassword($password, $new_password);
							}
							catch ( Exception $e ) {
								$validator->setError('old_password', $e->getMessage());
							}
						}
					}

					break;
				case ! is_null($save_newsletters):
					$flag = (bool) $this->_getParam('newsletters');

					$this->user->updateRecieveNewsletters($flag);
					$this->view->data = $this->user->getData(array(
						'country_id', 'city_id', 'email', 'recieve_newsletters'
					));

					break;
				case ! is_null($save_instant_book_mobile):
					$country_code = $this->_getParam('country_code');
					$instant_book_mobile_number = $this->_getParam('instant_book_mobile_number');
					$instant_book_receive_sms = $this->_getParam('instant_book_receive_sms');
					
					list($country_id, $phone_prefix, $ndd_prefix) = explode('-', $country_code);

					$phone_number = preg_replace('/^' . $ndd_prefix . '/', '', $instant_book_mobile_number);
					$phone_number_parsed = '00' . intval($phone_prefix) . $phone_number;

					$data = array(
						'escort_id' => $this->escort->id,
						'country_code' => $country_code,
						'phone_number' => $phone_number,
						'phone_number_parsed' => $phone_number_parsed,
						'receive_sms' => $instant_book_receive_sms
					);
					
					$client = new Cubix_Api_XmlRpc_Client();
					$result = $client->call('InstantBook.setInstantBookContactInfo', array($data));
					$this->_redirect($this->view->getLink('private-v2-settings'));
					break;
			}

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();
				//print_r( $status['msgs'] ); die;
				$this->view->errors = $status['msgs'];
			}
		}
	}

	public function editReviewAction()
	{
		if ($this->user->user_type != 'member')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			if (isset($this->_request->id) && intval($this->_request->id) > 0)
			{
				$id = intval($this->_request->id);
				$lng = Cubix_I18n::getLang();
				$r = Cubix_Api::getInstance()->call('getReviewById', array($id, $lng));

				if (!$r || $r['user_id'] != $this->user->id || $r['is_deleted'] || ($r['status'] != REVIEW_NEED_MODIFICATION && $r['status'] != REVIEW_SYSTEM_DISABLED))
				{
					$this->_redirect($this->view->getLink('private-v2'));
					return;
				}
				else
				{
					$this->view->review = $r;
				}

				if ($this->_request->isPost())
				{
					$req = $this->_request;

					Cubix_Api::getInstance()->call('editReview', array($req->id, $this->user->id, $req->review));

					$this->_redirect($this->view->getLink('private-v2-reviews'));
				}
			}
		}
	}

	public function addReviewAction()
	{
		if ($this->user->user_type != 'member')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			$config_review = Zend_Registry::get('reviews_config');

			if (isset($this->_request->escort_id) && intval($this->_request->escort_id) > 0)
			{
				$escort_id = $this->view->escort_id = intval($this->_request->escort_id);
				$model = new Model_EscortsV2();
				$esc = $model->getRevComById($escort_id);

				if (!$esc || $esc->disabled_reviews)
				{
					$this->_redirect($this->view->getLink());
					return;
				}
				else
				{
					$lng = Cubix_I18n::getLang();
					list($showname, $cities) = Cubix_Api::getInstance()->call('getEscortDetailsForReviews', array($escort_id, $lng));

					if (!$showname)
					{
						$this->_forward('error', 'error', 'default', array('error_msg' => 'Wrong URL.'));
						return;
					}

					$this->view->showname = $showname;
					$this->view->cities = $cities;
					$this->view->username = $this->user->username;
					$this->view->review_text_length = $config_review['textLength'];
				}
			}
			else
			{
				$this->_forward('error', 'error', 'default', array('error_msg' => 'Wrong URL.'));
				return;
			}

			if ($this->_request->isPost())
			{
				$validator = new Cubix_Validator();

                $this->_request->setParam( 'm_date', strtotime( $this->_request->m_date ) );
                $this->_request->setParam( 't_meeting_date', date("d M Y", strtotime( $this->_request->t_meeting_date ) ) );

                if('1970-01-01' != date("Y-m-d", $this->_request->m_date )){
                	$this->view->m_date =  date("Y-m-d", $this->_request->m_date );
                }
                $this->view->t_meeting_date =  date("Y-m-d", strtotime( $this->_request->t_meeting_date ) );

				$req = $this->_request;

				if (intval($req->m_date) == 0)
					$validator->setError('m_date', 'Required');

				if ($req->meeting_city == 'other')
				{
					if (!$req->country_id)
						$validator->setError('country_id', 'Required');

					if (!$req->city_id)
						$validator->setError('city_id', 'Required');
				}else{
					if(!$req->meeting_city)
						$validator->setError('meeting_city', 'Required');
				}

				if (!$req->meeting_place)
					$validator->setError('meeting_place', 'Required');

				if (!trim($req->duration))
					$validator->setError('duration', 'Required');
				elseif (!is_numeric($req->duration))
					$validator->setError('duration', $this->view->t('must_be_numeric'));

				if (!$req->duration_unit)
					$validator->setError('duration_unit', 'Required');

				if (!trim($req->price))
					$validator->setError('currency', 'Required');
				elseif (!is_numeric($req->price))
					$validator->setError('currency', $this->view->t('must_be_numeric'));

				/*if (!$req->currency)
					$validator->setError('currency', 'Required');*/

				if ($req->looks_rate == '-1')
					$validator->setError('looks_rate', 'Required');

				if ($req->services_rate == '-1')
					$validator->setError('services_rate', 'Required');

				/*
				if (!$req->s_kissing)
					$validator->setError('s_kissing', 'Required');

				if (!$req->s_blowjob)
					$validator->setError('s_blowjob', 'Required');

				if (!$req->s_cumshot)
					$validator->setError('s_cumshot', 'Required');

				if (!$req->s_69)
					$validator->setError('s_69', 'Required');

				if (!$req->s_anal)
					$validator->setError('s_anal', 'Required');

				if (!$req->s_sex)
					$validator->setError('s_sex', 'Required');

				if (!$req->s_multiple_times_sex)
					$validator->setError('s_multiple_times_sex', 'Required');

				if (!$req->s_breast)
					$validator->setError('s_breast', 'Required');*/

				if (!$req->s_attitude)
					$validator->setError('s_attitude', 'Required');

				if (!$req->s_conversation)
					$validator->setError('s_conversation', 'Required');

				if (!$req->s_availability)
					$validator->setError('s_availability', 'Required');

				/*if (!$req->s_photos)
					$validator->setError('s_photos', 'Required');
				*/
				if (!trim($req->t_user_info))
					$validator->setError('t_user_info', 'Required');

				if (!trim($req->t_meeting_date))
					$validator->setError('t_meeting_date', 'Required');

				if ($req->hrs == '-1')
					$validator->setError('hrs', 'Required');

				if ($req->min == '-1')
					$validator->setError('hrs', 'Required');

				if (!trim($req->t_meeting_duration))
					$validator->setError('t_meeting_duration', 'Required');

				if (!trim($req->t_meeting_place))
					$validator->setError('t_meeting_place', 'Required');

				/*if (!trim($req->t_comments))
					$validator->setError('t_comments', 'Required');*/
				if (!trim($req->review))
					$validator->setError('review', 'Required');
				elseif(strlen(trim($req->review)) < intval($config_review['textLength']))
					$validator->setError('review',  __('review_is_short',array('LENGTH' => $config_review['textLength'])));
				//$captcha = Cubix_Captcha::verify($this->_request->recaptcha_response_field);

				/*$captcha_errors = array(
					'invalid-site-public-key' => 'We weren\'t able to verify the public key.',
					'invalid-site-private-key' => 'We weren\'t able to verify the private key.',
					'invalid-request-cookie' => 'The challenge parameter of the verify script was incorrect.',
					'incorrect-captcha-sol' => 'The CAPTCHA solution was incorrect.',
					'verify-params-incorrect' => 'The parameters to /verify were incorrect, make sure you are passing all the required parameters.',
					'invalid-referrer' => 'reCAPTCHA API keys are tied to a specific domain name for security reasons.',
					'recaptcha-not-reachable' => 'reCAPTCHA never returns this error code. A plugin should manually return this code in the unlikely event that it is unable to contact the reCAPTCHA verify server.'
				);*/

				//var_dump($captcha);

				/*if ( strlen($captcha) > 5 ) {
					$validator->setError('captcha', $this->view->t('wrong_secure_text')  $captcha_errors[$captcha]);
				}*/
				$captcha = trim($req->captcha);
				if ( ! strlen($captcha ) ) {
					$validator->setError('captcha', 'Required');
				}
				else {
					$session = new Zend_Session_Namespace('captcha');
					$orig_captcha = $session->captcha;

					if ( strtolower($captcha) != $orig_captcha ) {
						$validator->setError('captcha', 'Captcha is invalid');
					}
				}

				$t_user_info = trim($req->t_user_info);
				$t_meeting_date = trim($req->t_meeting_date);
				$t_meeting_time = $req->hrs . ':' . $req->min;
				$t_meeting_duration = trim($req->t_meeting_duration);
				$t_meeting_place = trim($req->t_meeting_place);

				if ($validator->isValid())
				{
					$ip = Cubix_Geoip::getIP();

					$req->setParam('ip', $ip);
					$req->setParam('user_id', $this->user->id);
					$req->setParam('m_date', date('Y-m-d', $req->m_date));

					if ($req->meeting_city == 'other'){
						$req->setParam('city_id', $req->city_id);
					}
					else{
						$req->setParam('city_id', $req->meeting_city);
					}
					
					list($sms_unique, $phone_to, $mem_susp) = Cubix_Api::getInstance()->call('addReview', array($this->getRequest()->getParams()));
					Cubix_Api::getInstance()->call('SyncNotifier', array($escort_id, 29 ,array('escort_id' => $escort_id) ));

					/* get city */
					if ($req->meeting_city != 'other')
						$city_id = $req->meeting_city;
					else
						$city_id = $req->city_id;

					$m_c = new Model_Cities();
					$city = $m_c->getById($city_id);
					$city_title = $city->title;
					/****************/

					if (strlen(trim($phone_to)) > 0 && !$mem_susp)
					{
						$text = $this->view->t('review_sms_template', array(
							'user_info' => $t_user_info,
							'showname' => $showname,
							'date' => $t_meeting_date,
							'city' => $city_title,
							'meeting_place' => $t_meeting_place,
							'duration' => $t_meeting_duration,
							'time' => $t_meeting_time,
							'unique_number' => $sms_unique
						));

						$originator = Cubix_Application::getPhoneNumber(Cubix_Application::getId());

						$phone_from = $originator;
						$sms_id = Cubix_Api::getInstance()->call('outbox', array($escort_id, $phone_to, $phone_from, $text, Cubix_Application::getId()));

						/*$sms = new Cubix_Clickatell_Sms();
						
						$sms->setFrom($phone_from);
						//$sms->setCallback("http://backend.6annonce.com/sms/index/sms-callback2");
						$sms->setCallback(3);
						$sms->setCliMsgId($sms_id);

						$sms->addRecipients($phone_to);

						$sms->setMessage($text);

						$sms->send();
						
						Cubix_Api::getInstance()->call('smsSent', array($sms_unique));*/

						/*$sms = new Cubix_2sms_Sms(Cubix_2sms_Sms::API_KEY_6A);
			
						$res = $sms->send($text, $phone_from, $phone_to);
			
						//$model_sms->update2SMSData($sms_id, $res->msg_id, $res->status);
						
						Cubix_Api::getInstance()->call('smsSent', array($sms_unique));
						Cubix_Api::getInstance()->call('update2SMSData', array($sms_id, $res->msg_id, $res->status));*/

						//sms info
						$config = Zend_Registry::get('system_config');
						$sms_config = $config['sms'];

						$SMS_USERKEY = $sms_config['userkey'];
						$SMS_PASSWORD = $sms_config['password'];
						$SMS_DeliveryNotificationURL = $sms_config['DeliveryNotificationURL'];
						$SMS_NonDeliveryNotificationURL = $sms_config['NonDeliveryNotificationURL'];
						$SMS_BufferedNotificationURL = $sms_config['BufferedNotificationURL'];

						$sms = new Cubix_SMS($SMS_USERKEY, $SMS_PASSWORD);

						$sms->setOriginator($originator);
						$sms->addRecipient($phone_to, (string)$sms_id);
						$sms->setBufferedNotificationURL($SMS_BufferedNotificationURL);
						$sms->setDeliveryNotificationURL($SMS_DeliveryNotificationURL);
						$sms->setNonDeliveryNotificationURL($SMS_NonDeliveryNotificationURL);

						$sms->setContent($text);

						if (1 != $result = $sms->sendSMS())
						{

						}
						else
						{
							Cubix_Api::getInstance()->call('smsSent', array($sms_unique));
						}
					}

					$this->_redirect($this->view->getLink('private-v2-reviews'));
				}
				else
				{
					$status = $validator->getStatus();
					$this->view->errors = $status['msgs'];
					$this->view->review = $req;
				}
			}
		}
	}

	public function reviewsAction()
	{
		if ($this->user->user_type != 'member')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			$lng = Cubix_I18n::getLang();
			$this->view->items = Cubix_Api::getInstance()->call('getReviewsForMember', array($this->user->id, $lng));
		}
	}

	public function alertsAction()
	{
		if ($this->user->user_type != 'member')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			$model = new Model_Members();
			$items = $model->getAlerts($this->user->id);

			if ($items)
			{
				$arr = array();
				$esc_ids = array();

				foreach ($items as $item)
				{
					$arr[$item['escort_id']][] = $item['event'];

					if ($item['event'] == ALERT_ME_CITY)
						$arr[$item['escort_id']]['cities'] = $item['extra'];

					if (!in_array($item['escort_id'], $esc_ids))
						$esc_ids[] = $item['escort_id'];
				}

				$this->view->items = $arr;

				$modelE = new Model_EscortsV2();
				$this->view->escorts = $modelE->getAlertEscrots(implode(',', $esc_ids));

				/* Cities list */
				$app = Cubix_Application::getById();

				if ($app->country_id)
				{
					$modelCities = new Model_Cities();

					$cities = $modelCities->getByCountry($app->country_id);
				}
				else
				{
					$modelCountries = new Model_Countries();
					$modelCities = new Model_Cities();

					$countries = $modelCountries->getCountries();
					$this->view->countries_list = $countries;

					$c = array();

					foreach ($countries as $country)
						$c[] = $country->id;

					$cities = $modelCities->getByCountries($c);
				}

				$this->view->cities_list = $cities;
				/* End Cities list */
			}
		}
	}

	public function addReviewCommentAction()
	{
		$this->view->layout()->disableLayout();

		if ($this->user->user_type != 'escort' && $this->user->user_type != 'agency')
			//$this->_redirect($this->view->getLink('private-v2'));
			die();
		else
		{
			if (isset($this->_request->review_id) && intval($this->_request->review_id) > 0)
			{
				$this->view->review_id = $rev_id = $this->_request->review_id;
				$user_id = $this->user->id;
				$do = false;

				if ($this->user->user_type == 'escort')
				{
					if (Cubix_Api::getInstance()->call('checkEscortReview', array($user_id, $rev_id)))
					{
						$do = true;
					}
					else
						die;
				}
				else
				{
					if (Cubix_Api::getInstance()->call('checkEscortReviewForAgency', array($user_id, $rev_id)))
					{
						$do = true;
					}
					else
						die;
				}

				if ($do)
				{
					if ($this->_request->isPost())
					{
						$validator = new Cubix_Validator();

						if (!$this->_request->comment)
							$validator->setError('comment', 'Required');

						if ($validator->isValid())
						{
							Cubix_Api::getInstance()->call('addReviewComment', array($rev_id, $this->_request->comment));
							$result['status'] = 'success';
							$result['signin'] = true;
							echo json_encode($result);
							die;
						}
						else
						{
							$status = $validator->getStatus();
							$this->view->errors = $status['msgs'];
							if ( ! is_null($this->_getParam('ajax')) ) {
								echo(json_encode($status));
								ob_flush();
								die;
							}
						}
					}
				}
			}
			else
				die;
		}
	}

	public function escortReviewsAction()
	{
		if ($this->user->user_type != 'escort')
			$this->_redirect($this->view->getLink('private-v2'));
		else
		{
			$lng = Cubix_I18n::getLang();
			$results = Cubix_Api::getInstance()->call('getReviewsForEscort', array($this->user->id, $lng));
			$this->view->items = $results['reviews'];

			if ($this->_request->isPost())
			{
				$id = intval($this->_request->id);

				$check = Cubix_Api::getInstance()->call('checkPendingReviewForEscort', array($this->user->id, $id));

				if ($check)
				{
					$answer = null;

					if ($this->_request->no)
						$answer = 1;
					elseif ($this->_request->yes)
						$answer = 2;

					if (!is_null($answer))
					{
						Cubix_Api::getInstance()->call('setPendingReviewForEscort', array($id, $answer));
					}
				}
			}

			$pending = Cubix_Api::getInstance()->call('getPendingReviewsForEscort', array($this->user->id));
			$this->view->items_pending = $pending['reviews'];
		}
	}

	public function escortsReviewsAction()
	{
		if ($this->user->user_type != 'agency'){
			$this->_redirect($this->view->getLink('private-v2'));}
		else
		{
			$lng = Cubix_I18n::getLang();
			$req = $this->_request;
			$this->view->escort_id = $escort_id = ( $req->getParam('escort_id') ) ? $req->getParam('escort_id') : -1;

			if ( $escort_id == -1 ){
				$results = Cubix_Api::getInstance()->call('getReviewsForAgencyEscorts', array($this->user->id, $lng));
				$this->view->items = $results['reviews'];
			}
			else{
				$results = Cubix_Api::getInstance()->call('getReviewsForEscort', array($this->user->id, $lng, $escort_id));
				$this->view->items = $results['reviews'];
			}

			//$results = Cubix_Api::getInstance()->call('getReviewsForAgencyEscorts', array($this->user->id, $lng));
			//$this->view->items = $results['reviews'];
			$this->view->escorts = $this->agency->getEscorts();

			if ($this->_request->isPost())
			{
				$id = intval($this->_request->id);

				$check = Cubix_Api::getInstance()->call('checkPendingReviewForAgencyEscort', array($this->user->id, $id));

				if ($check)
				{
					$answer = null;

					if ($this->_request->no)
						$answer = 1;
					elseif ($this->_request->yes)
						$answer = 2;

					if (!is_null($answer))
					{
						Cubix_Api::getInstance()->call('setPendingReviewForEscort', array($id, $answer));
					}
				}
			}

			$pending = Cubix_Api::getInstance()->call('getPendingReviewsForAgencyEscorts', array($this->user->id));
			$this->view->items_pending = $pending['reviews'];
		}
	}

	public function ajaxReviewsAction()
	{
		$this->view->layout()->disableLayout();
		$lng = Cubix_I18n::getLang();
		$escord_id = intval($this->_request->getParam('escort_id'));
		$r_id = $this->view->r_id = ( $this->_request->getParam('r_id') ) ? $this->_request->getParam('r_id') : null;

		if ($escord_id == -1){
			$results = Cubix_Api::getInstance()->call('getReviewsForAgencyEscorts', array($this->user->id, $lng));
			$this->view->items = $results['reviews'];
		}
		else{
			$results = Cubix_Api::getInstance()->call('getReviewsForEscort', array($this->user->id, $lng,$escord_id, 1, 50, $r_id ));
			$this->view->items = $results['reviews'];
		}
	}

	public function memberProfileAction()
	{
		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user )
		{
			$user_data = $client->call('Users.getById', array($this->user->id));
			$this->view->user_data = $u_d = new Model_UserItem($user_data);
			$this->view->user = $this->user;
		}

		if ( $this->_request->isPost() )
		{
			$data = array();

			$validator = new Cubix_Validator();

			$data = $this->_validateUserProfile($validator);
			//$data['email'] = $u_d->email;
			$this->view->user_data = new Model_UserItem($data);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];

				return;
			}

			try {
				unset($data['email']);
				$result = $client->call('Users.updateProfile', array($this->user->id, $data));
				//newsletter email log
				/*$emails = array(
					'old' => $this->user->email,
					'new' => $data['email']
				);
				Cubix_Api::getInstance()->call('addNewlstterEmailLog', array($this->user->id, $this->user->user_type, 'edit', $emails));*/
				//
			}
			catch (Exception $e) {
				var_dump($client->getLastResponse()); die;
			}

			if ( isset($result['error']) ) {
				print_r($result['error']); die;
			} else {
				if ( $result !== false ) {
					$this->view->saved = true;
					//echo nl2br(print_r($result, true));
				}
			}
		}
	}

	public function _validateUserProfile(Cubix_Validator $validator)
	{
		$req = $this->_request;

		/*$email = $req->email;
		if ( ! strlen($email) ) {
			$validator->setError('email', 'Email is required !');
		}
		$data['email'] = $email;*/

		//Location
		$country = $req->country_id;

		if ( ! $country ) {
			$validator->setError('country_id', 'Country is required !');
		}
		$data['country_id'] = $country;


		$city = $req->city_id;
		if ( ! $city ) {
			$city = null;
		}
		$data['city_id'] = $city;

		$about_me = $req->about_me;
		$data['about_me'] = $about_me;
		return $data;
	}

	protected function _validateAgency(Cubix_Validator $validator, $agency_id)
	{
		$req = $this->_request;

		$defines = Zend_Registry::get('defines');
		$client = new Cubix_Api_XmlRpc_Client();

		$data = array();

		$showname = preg_replace('/(\s)+/','$1', trim($req->agency_name));
		if ( ! strlen($showname) ) {
			$validator->setError('agency_name', 'Required');
		}
		elseif ( ! preg_match('/^[^-][-_a-z0-9\s]+$/i', $showname) ) {
			$validator->setError('agency_name', 'Must begin with letter or number and must contain only alphanumeric characters');
		}
		$data['name'] = $showname;

		// Working times block
		$wds = (array) $req->work_days;
		$work_times = array();
		foreach ( $wds as $d => $nil ) {
			if ( ! ($req->work_times_from[$d]) || ! ($req->work_times_to[$d]) ) {
				$validator->setError('work_times_' . $d, 'Select time interval');
			}

			$work_times[$d] = array('from' => $req->work_times_from[$d], 'to' => $req->work_times_to[$d]);
		}
		$data['working_times'] = $work_times;

		$available_24_7 = $req->available_24_7;
		if ( ! $available_24_7 ) {
			$available_24_7 = null;
		}
		$data['available_24_7'] = $available_24_7;


		// Contacts block

		$contact_phone = $req->phone;
		$phone_prefix = $req->phone_prefix;
		$data['contact_phone_parsed'] = null;
		if ($contact_phone ) {
			if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $contact_phone) ) {
				$validator->setError('phone', 'Invalid phone number');
			}
			elseif(preg_match("/^(\+|00)/", $contact_phone) ) {
				$validator->setError('phone', 'Please enter phone number without country calling code');
			}
			if ( ! ($phone_prefix) ) {
				$validator->setError('phone_prefix', 'Country calling code Required');
			}

		}
		list($country_id,$phone_prefix,$ndd_prefix) = explode('-',$req->phone_prefix);
		unset($data['phone_prefix']);
		$data['phone_country_id'] = intval($country_id);
		$data['contact_phone_parsed'] = $data['phone'] = preg_replace('/[^0-9]+/', '', $contact_phone);
		$data['contact_phone_parsed'] = preg_replace('/^'.$ndd_prefix.'/', '', $data['contact_phone_parsed']);
		$data['contact_phone_parsed'] = '00'.$phone_prefix.$data['contact_phone_parsed'];

		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$results = $client->call('Escorts.existsByPhone', array($data['contact_phone_parsed'], null, $agency_id));

		//var_dump($results);die;
		$resCount = is_array($results) ? count($results) : 0;

		if($resCount > 0) {
			$validator->setError('phone', 'Phone already exists');
			}

		$phone_instructions = $req->phone_instr;
		if ( ! $phone_instructions ) {

		}
		$data['phone_instructions'] = $phone_instructions;

		$contact_email = $req->email;
		if ( ! $contact_email ) {

		}
		$data['email'] = $contact_email;

		$contact_web = $req->web;
		if ( ! $contact_web ) {

		}
		$data['web'] = $contact_web;

		//Location
		$country = $req->country_id;
		if ( ! $country ) {

		}
		$data['country_id'] = Cubix_Application::getById()->country_id;

		$region = $req->region_id;
		if ( ! $region ) {

		}
		$data['region_id'] = $region;

		$city = $req->city_id;
		if ( ! $city ) {

		}
		$data['city_id'] = $city;

		$data['phone_to_all_escorts'] = $req->phone_to_all_escorts;

		if ( strlen($data['email']) && $client->call('Application.isDomainBlacklisted', array($data['email'])) ) {
			$validator->setError('email', 'Domain is blacklisted');
		}

		if ( strlen($data['web']) && $client->call('Application.isDomainBlacklisted', array($data['web'])) ) {
			$validator->setError('web', 'Domain is blacklisted');
		}

		/* // Sales person
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		$sales_user_id = intval($req->sales_user_id);
		if ( ! $sales_user_id ) $sales_user_id = null;

		if ( ! is_null($sales_user_id) ) {
			if ( ! $client->call('Users.isSalesValid', array($sales_user_id)) ) {
				$sales_user_id = null;
			}
		}
		$data['sales_user_id'] = $sales_user_id; */

		$security = new Cubix_Security();
		$i18n_data = Cubix_I18n::getValues($req->getParams(), 'about', '');
			foreach ( $i18n_data as $field => $value ) {
				$value = $security->clean_html($value);
				$i18n_data[$field] = $security->xss_clean($value);
				//$i18n_data[$field] = htmlspecialchars(strip_tags(trim($value)));
			}
		$data = array_merge( $data,$i18n_data);

		//about us
		$about_me_exists = false;
		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			if(strlen($data['about_' . $lng]) > 0){
			    $about_me_exists = true;
				break;
			}
		}
		if ( !$about_me_exists ) {
				$validator->setError('about_me_text', 'About us text required');
		}


		return $data;
	}

	public function agencyProfileAction()
	{
		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isAgency() )
		{
			$agency_data = $client->call('Agencies.getByUserId', array($this->user->id));
			$agency_id = $agency_data['id'];
			$agency_slug = $agency_data['slug'];
			$phone_prefix = $agency_data['phone_country_id'];
			$agency_data = array_merge($agency_data, $client->call('Agencies.getInfo', array($agency_slug, $agency_id)));
			$countyModel = new Model_Countries();

			$this->view->countries = $countyModel->getPhoneCountries();
			$this->view->sales_persons = $client->call('Users.getSalesPersons');
			$this->view->phone_prefix_id = $phone_prefix;

		}

		$this->view->user = $this->user;

		if ( $this->_request->isPost() ) {

			$data = array();

			$validator = new Cubix_Validator();

			$data = $this->_validateAgency($validator, $agency_id);

			/* // Assign escort to a sales person
			$client->call('Users.assignAgency', array($data['sales_user_id'], $agency_id));
			unset($data['sales_user_id']); */

			$phone_to_all_escorts = $data['phone_to_all_escorts'];
			unset($data['phone_to_all_escorts']);

			$data['application_id'] = $agency_data['application_id'];
			$this->view->phone_prefix_id = $data['phone_country_id'];
			$this->view->agency_data = new Model_AgencyItem($data);
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];

				return;
			}

			try {
				$result = $client->call('Agencies.updateAgencyProfile', array($agency_id, $data));
				if($phone_to_all_escorts == 1 ){

					$agency_escorts = $client->call('Agencies.getAllEscorts', array($agency_id));
					$escort_data = array(
							'phone_country_id'	   => $data['phone_country_id'],
							'phone_number'         => $data['phone'],
						    'contact_phone_parsed' => $data['contact_phone_parsed'],
							'phone_exists'         => $data['contact_phone_parsed']
						);

					foreach($agency_escorts as $escort){
						Cubix_Api::getInstance()->call('updateEscortProfileSimple', array( $escort['id'], $escort_data) );

					}

				}
			}
			catch (Exception $e) {
				var_dump($client->getLastResponse()); die;
			}

			if ( isset($result['error']) ) {
				print_r($result['error']); die;
			}
			else {
				if ( $result !== false ) {
					$this->view->saved = true;
					echo nl2br(print_r($result, true));
				}
			}
		}
		else{
			$phone_prfixes = $countyModel->getPhonePrefixs();
			if($agency_data['phone']){
				if(preg_match('/^(\+|00)/',trim($agency_data['phone'])))
				{
					$phone_prefix_id = NULL;
					$phone_number = preg_replace('/^(\+|00)/', '',trim($agency_data['phone']));
					foreach($phone_prfixes as $prefix)
					{
						if(preg_match('/^('.$prefix->phone_prefix.')/',$phone_number))
						{
							$phone_prefix_id = $prefix->id;
							$agency_data['phone'] = preg_replace('/^('.$prefix->phone_prefix.')/', '',$phone_number);
							BREAK;
						}

					}
					$this->view->phone_prefix_id = $phone_prefix_id;
				}
			}
			$this->view->agency_data = new Model_AgencyItem($agency_data);

		}
	}

	public function agencyLogoAction()
	{
		$this->view->layout()->disableLayout();

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isAgency() )
		{
			$agency = $this->user->getAgency();
			$this->view->agency = $agency;

			if ( $this->_request->isPost() )
			{

				if ( $this->_request->a )
				{
					try {
						if ( !$this->_request->imageData ) {
							$this->view->uploadError = 'Please select a photo to upload';
							return;
						}
						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($_FILES['logo']['tmp_name'], 'agencies', $agency->application_id, strtolower(@end(explode('.', $_FILES['logo']['name']))));
						//$image = array( 'ext' => 'jpg', 'hash' => 'grgrsdhrdhrthrdh' );

						$agency_data['logo_hash'] = $image['hash'];
						$agency_data['logo_ext'] = $image['ext'];

						if( $this->_request->ajax ){
							echo json_encode(array("success" => 1));
							die;
						}

						$result = $client->call('Agencies.updateAgencyLogo', array($agency->id, $agency_data));

						$agency = $this->user->getAgency();
						$this->view->agency = $agency;

					} catch (Exception $e) {
						$this->view->uploadError = $e->getMessage();
					}
				}
				else if ( $this->_request->d ) {
					$agency_data['logo_hash'] = null;
					$agency_data['logo_ext'] = null;
					try {
						$client->call('Agencies.updateAgencyLogo', array($agency->id, $agency_data));

						$agency = $this->user->getAgency();
						$this->view->agency = $agency;
					} catch (Exception $e) {
						$this->view->uploadError = $e->getMessage();
					}
				}
			}
		}
	}

	public function toursAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->is_agency = $this->user->isAgency();

		$escort_id = intval($this->_getParam('escort'));

		if ( $escort_id > 0  && !is_null($this->_getParam('info')) ) {
			$model = new Model_Escorts();
			$escort = $model->getById($escort_id);

			if ( ! $escort ) {
				die(json_encode(null));
			}
			else {
				die(json_encode(array(
					'photo' => $escort->getMainPhoto()->getUrl('agency_p100', true),
					'link' => $this->view->getLink('profile', array('showname' => $escort->showname, 'escort_id' => $escort_id))
				)));
			}
		}

		if ( $this->user->isAgency() ) {
			$this->_helper->viewRenderer->setScriptAction('agency-tours');
			$this->view->escorts = $this->agency->getEscorts();
		}
		else {
			$this->_helper->viewRenderer->setScriptAction('escort-tours');
			$this->escort = $this->view->escort = $this->user->getEscort();
		}

		$this->view->disable_tour_history = $this->client->call('getTourHistory', array($this->user->id));
	}

	public function toggleTourHistoryAction()
	{
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$o_t_v = intval($this->_request->o_t_v);

		$o_t_v == 1 ? $o_t_v = 0 : $o_t_v = 1;

		$this->client->call('updateTourHistory', array($this->user->id, $o_t_v));
	}

	public function ajaxToursAction($data = array())
	{
		$this->view->layout()->disableLayout();
		$this->_request->setParam('no_tidy', true);

		$is_old_tour = intval($this->_getParam('old_tour')) ? true : false;
		$page = (isset($this->_request->page) && intval($this->_request->page) > 0) ? intval($this->_request->page) : 1;
		$per_page = 20;
		$this->view->is_agency = $this->user->isAgency();
		$this->view->tours = array();
		$this->view->page = $page;
		$this->view->per_page = $per_page;
        $getall = false;
        $agency_id = 0;

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
            /* Update Grigor */
            $agency =  $this->user->getAgency();
            $agency_id = $agency->id;

            if($escort_id == -1){
                $getall = true;
            }
      		if ( ! $this->agency->hasEscort($escort_id) && !$getall) return;
		} elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}

		if ( is_null($escort_id) ) {
			return;
		}
		/* Update Grigor */
		$tours = $this->client->call('getEscortToursV2', array($escort_id, Cubix_I18n::getLang(), $agency_id,$page, $per_page, $is_old_tour));
        $this->view->tours = $tours[0];
		$this->view->count = $tours[1];
        if($is_old_tour){
		    $this->_helper->viewRenderer->setScriptAction('ajax-old-tours');
	    }

        /* Update Grigor */
	}

	public function editTourAction()
	{
		//$this->view->layout()->disableLayout();
		$this->_request->setParam('no_tidy', true);

		$tour = null;
		if ( ($tour_id = intval($this->_getParam('id'))) > 0 ) {
			$tour = $this->client->call('getEscortTourV2', array($tour_id, $this->_request->lang_id));
		}
		$this->view->tour = $tour;

		$this->view->tour_id = intval($this->_getParam('id'));

		if ( $this->user->isAgency() ) {
            $this->view->escorts = $this->agency->getEscorts();
			$escort_id = $this->view->escort_id = intval($this->_getParam('escort_id'));
			if ( $tour ) $escort_id = $tour['escort_id'];
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}


		$model = new Model_Escorts();
		$this->view->escort = $escort = $model->getById($escort_id);

		if ( $this->_request->isPost() ) {
		    if( $escort_id && $escort_id != -1 ) {
                if ($this->user->isAgency()) {
                    if (!$this->agency->hasEscort($escort_id)) {
                        die('Permission denied!');
                    }
                } elseif ($this->user->isEscort()) {
                    if ($tour && $escort_id != $tour['escort_id']) {
                        die('Permission denied!');
                    }
                }
            }

			$form = new Cubix_Form_Data($this->_request);
			$form->setFields(array(
				'id' => 'int-nz',
				'country_id' => 'int-nz',
				'city_id' => 'int-nz',
				'date_from' => 'int',
                'escort_id' => 'int',
				'date_to' => 'int',
				'is_time_on' => 'int',
				'minutes_from' => '',
				'minutes_to' => '',
				'diff_time' => '',
				'phone' => '',
				'email' => ''
			));
			$i_data = $form->getData();

			$validator = new Cubix_Validator();

            if ( ! $escort_id ) {
                $validator->setError('escort_id', 'Please select escort');
            }

            if ( $this->user->isAgency() ) {
                if ( $escort_id == -1 ) {
                    $validator->setError('escort_id', 'Please select escort');
                }
            }

			$model = new Cubix_Geography_Countries();
			if ( ! $model->exists($i_data['country_id']) ) {
				$validator->setError('country_id', 'Invalid country');
			}

			if ( ! $model->hasCity($i_data['country_id'], $i_data['city_id']) ) {
				$validator->setError('city_id', 'Invalid city');
			}

			if ( $i_data['email'] ) {
				if ( ! Cubix_Validator::validateEmailAddress($i_data['email']) ) {
					$validator->setError('email', 'Invalid email address');
				}
				/*elseif ( Cubix_Application::isDomainBlacklisted($i_data['email']) ) {
					$validator->setError('email', 'Domain is blacklisted');
				}*/
			}

			if ( ! $i_data['date_from'] ) {
				$validator->setError('date_from', 'Date required');
			}
			if ( ! $i_data['date_to'] ) {
				$validator->setError('date_to', 'Date required');
			}
			if ( (! isset($i_data['minutes_from']) && isset($i_data['minutes_to'])) || ( isset($i_data['minutes_from']) && !isset($i_data['minutes_to'])) ) {
				$validator->setError('minutes_from', 'Time required');
			}
			if ( $validator->isValid() ) {
				$tours = $this->client->call('getEscortToursV2', array($escort_id, $this->_request->lang_id));
				$data['tours_tmp'] = $tours[0];

				$i_data['diff_time'] = intval($i_data['diff_time']);
				$date_to = $i_data['date_to'] = strtotime(date('d-m-Y', ($i_data['date_to'] + $i_data['diff_time'])));
				$date_from = $i_data['date_from'] = strtotime(date('d-m-Y', ($i_data['date_from'] + $i_data['diff_time'])));

				$i_data['minutes_from'] = (is_null($i_data['minutes_from']) || $i_data['minutes_from'] == "" )  ? null : intval($i_data['minutes_from']);
				$i_data['minutes_to']   = (is_null($i_data['minutes_to']) || $i_data['minutes_to'] == "" )  ? null : intval($i_data['minutes_to']);

				if($i_data['is_time_on']){
					$date_to = $i_data['date_to'] + $i_data['minutes_to'] * 60;
					$date_from = $i_data['date_from'] + $i_data['minutes_from'] * 60;
				}
				if($i_data['minutes_to'] === null){
					$date_to = $i_data['date_to'] += 60 * 60 * 24 - 1; // 23:59:59 

				}

				unset($i_data['is_time_on']);
				unset($i_data['diff_time']);
				if ( count($data['tours_tmp']) ) {
					foreach ( $data['tours_tmp'] as $t_k => $tr ) {
						unset($data['tours_tmp'][$t_k]['country']);
						unset($data['tours_tmp'][$t_k]['city']);
						$data['tours_tmp'][$t_k]['date_from'] = is_null($data['tours_tmp'][$t_k]['minutes_from']) ? $data['tours_tmp'][$t_k]['date_from'] : $data['tours_tmp'][$t_k]['date_from'] + $data['tours_tmp'][$t_k]['minutes_from'] * 60;
						$data['tours_tmp'][$t_k]['date_to'] = is_null($data['tours_tmp'][$t_k]['minutes_to']) ? $data['tours_tmp'][$t_k]['date_to'] : $data['tours_tmp'][$t_k]['date_to'] + $data['tours_tmp'][$t_k]['minutes_to'] * 60;
					}
				}

				$today = strtotime(date('d-m-Y', time()));
				//echo $today, ' / ', $date_to, ' / ', $date_from; die;

				if ( $date_to < $today || $date_from < $today ) {
					$validator->setError('date_from', Cubix_I18n::translate('sys_error_tour_date_invalid_interval'));
				}
				elseif($date_from >= $date_to ){
					$validator->setError('date_from', Cubix_I18n::translate('sys_error_tour_date_invalid_interval'));
				}
				else{

					foreach ( $data['tours_tmp'] as $t ) {
						if ($t['id'] == $i_data['id']) continue;

						if ( ( $date_from >= $t['date_from'] && $date_from < $t['date_to']) ||
							 ( $date_to > $t['date_from'] && $date_to <= $t['date_to']) ||
							 ( $date_from < $t['date_from'] && $date_to > $t['date_to']) )
						{
							$validator->setError('date_from', Cubix_I18n::translate('sys_error_tour_date_invalid_interval'));
							break;
						}
					}
				}
			}
			unset($data['tours_tmp']);
			if ( $validator->isValid() ) {
				if ( ! $tour ) {
					$this->client->call('addEscortTour', array($escort_id, $i_data));
				}
				else {
					$this->client->call('updateEscortTour', array($escort_id, $tour_id, $i_data));
				}
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function removeToursAction()
	{
		$tours = (array) $this->_getParam('tours');
		foreach ( $tours as $tour_id ) {
			$tour_id = intval($tour_id);

			$tour = $this->client->call('getEscortTour', array($tour_id, $this->_request->lang_id));
			if ( ! $tour ) continue;

			if ( $this->user->isAgency() ) {
				if ( ! $this->agency->hasEscort($tour['escort_id']) ) {
					continue; // wrong owner
				}
			}
			elseif ( $this->user->isEscort() ) {
				if ( $this->escort->getId() != $tour['escort_id'] ) {
					continue; // wrong owner
				}
			}

			$this->client->call('removeEscortTour', array($tour['escort_id'], $tour['id']));
		}

		die;
	}

	public function ajaxToursAddAction()
	{
		$this->_request->setParam('no_tidy', true);

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( ! $this->agency->hasEscort($escort_id) ) die;
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}

		if ( is_null($escort_id) ) die;

		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'id' => 'int-nz',
			'country_id' => 'int-nz',
			'city_id' => 'int-nz',
			'date_from' => 'int',
			'date_to' => 'int',
			'phone' => '',
			'email' => ''
		));
		$i_data = $form->getData();

		$validator = new Cubix_Validator();

		$tours = $this->client->call('getEscortTours', array($escort_id, $this->_request->lang_id));
		$data['tours_tmp'] = $tours[0];
		if ( count($data['tours_tmp']) ) {
			foreach ( $data['tours_tmp'] as $t_k => $tr ) {
				unset($data['tours_tmp'][$t_k]['country']);
				unset($data['tours_tmp'][$t_k]['city']);
			}
		}
		$data['tours_tmp'][] = $i_data;

		$today = strtotime(date('d-m-Y', time()));

		foreach ( $data['tours_tmp'] as $i1 => $tour ) {
			//list($id, $country_id, $city_id, $date_from, $date_to, $phone, $email) = $tour;
			$id = $tour['id'];
			$country_id = $tour['country_id'];
			$city_id = $tour['city_id'];
			$date_from = $tour['date_from'];
			$date_to = $tour['date_to'];
			$phone = $tour['phone'];

			if ( $id == $i_data['id'] ) continue;

			if ( $date_to < $today ) {
				$validator->setError('date_from', Cubix_I18n::translate('sys_error_tour_date_invalid_interval'));
				break;
			}

			if ( $date_from > $date_to ) {
				$validator->setError('date_from', Cubix_I18n::translate('sys_error_tour_date_invalid_interval'));
				break;
			}

			$found = false;

			foreach ( $data['tours_tmp'] as $i2 => $t1 ) {
				if ( $i1 == $i2 ) continue;

				//list($nil, $nil, $d_f, $d_t, $phone) = $t1;
				$d_f = $t1['date_from'];
				$d_t = $t1['date_to'];

				if ( ($d_f >= $date_from && $d_f < $date_to) || ($d_t > $date_from && $d_t <= $date_to) ) {
					$found = true;

					$validator->setError('date_from', Cubix_I18n::translate('sys_error_tour_date_invalid_interval'));
					break;
				}
			}

			if ( $found ) {
				break;
			}
		}
		unset($data['tours_tmp']);

		$model = new Cubix_Geography_Countries();
		if ( ! $model->exists($i_data['country_id']) ) {
			$validator->setError('country', 'Invalid country');
		}

		if ( ! $model->hasCity($i_data['country_id'], $i_data['city_id']) ) {
			$validator->setError('city', 'Invalid city');
		}

		if ( ! $i_data['date_from'] || ! $i_data['date_to'] ) {
			$validator->setError('date', 'Invalid date range');
		}

		if ( $i_data['email'] ) {
			if ( ! Cubix_Validator::validateEmailAddress($i_data['email']) ) {
				$validator->setError('email', 'Invalid email address');
			}/*
			else if (Cubix_Application::isDomainBlacklisted($i_data['email']) ) {
				$validator->setError('email', 'Domain is blacklisted');
			}*/
		}

		if ( $validator->isValid() ) {
			$this->client->call('addEscortTour', array($escort_id, $i_data));
		}
		else {
			die(json_encode($validator->getStatus()));
		}

		$this->ajaxToursAction(array('status' => 'success'));
	}

	public function statisticsAction()
	{
		if ( $this->user->isAgency() ) {
			$this->view->escorts = $this->agency->getEscorts();
			$this->view->is_escort = false;
		}else{
			$this->view->escorts = array( $this->escort );
			$this->view->is_escort = true;
		}
	}

	public function ajaxGetStatisticsAction()
	{
		$this->view->layout()->disableLayout();

		$escort_id = (int) $this->_getParam('stat_escort');
		$date_from = $this->_getParam('date_from');
		$date_to = $this->_getParam('date_to');

		$order_by = $this->_getParam('sort_by');
		$order_dir = $this->_getParam('sort_dir');

		$cache = Zend_Registry::get('cache');


		// <editor-fold defaultstate="collapsed" desc="Cache key generation">
		$tkey = "";

		if ( $escort_id ){
			$tkey .= "_escort_".$escort_id;
		}

		if ( $this->agency->id ){
			$tkey .= "_agency_".$this->agency->id;
		}

		if ( $date != "" ){
			$tkey .= "_date_".date;
		}

		if ( $date_from != "" ){
			$tkey .= "_date_from_".date;
		}

		if ( $date_to != "" ){
			$tkey .= "_date_to_".date;
		}

		if ( $order_by != "" ){
			$tkey .= "_order_by_".$order_by;
		}

		if ( $order_dir != "" ){
			$tkey .= "_order_dir_".$order_dir;
		}
		// </editor-fold>



		$cache_key = 'v2_statistics'.$tkey;

		if ( ! $items = $cache->load($cache_key) )
		{
			$items = Model_Statistics::getEscortStatistics($this->agency->id, $escort_id, $date_from,  $date_to, $order_by, $order_dir);
			$cache->save($items, $cache_key, array());
		}


		$this->view->data = $items;
		//		var_dump( $this->view->data );
		//		exit;
	}

	public function ajaxGetStatisticsReportAction(){

		$this->view->layout()->disableLayout();

		$type = $this->_getParam('type');
		$id = $this->_getParam('id');

		if ( $this->user->isAgency() ) {
			if ( ! $this->agency->hasEscort($id) ) {
				exit; // wrong ownere
			}
		}
		elseif ( $this->user->isEscort() ) {
			if ( $this->escort->getId() != $id ) {
				exit; // wrong owner
			}
		}


		$this->view->status = Model_Statistics::getEscortReportStatus( $id );

		$this->view->type = $type;


	}

	public function ajaxStatisticsReportAction(){
		$this->view->layout()->disableLayout();

		$status = $this->_getParam('status');
		$type = $this->_getParam('type');
		$id = $this->_getParam('id');

		if ( $this->user->isAgency() ) {
			if ( ! $this->agency->hasEscort($id) ) {
				exit; // wrong ownere
			}
		}
		elseif ( $this->user->isEscort() ) {
			if ( $this->escort->getId() != $id ) {
				exit; // wrong owner
			}
		}

		if ( is_array($status) ){
			$status = current($status);
		}
		$type = reset($type);
		if ( $type == 0 ){
			$status = -$status;
		}

		$this->view->status = $status;
		$this->view->type = $type;

		$items = Model_Statistics::updateReport($id, $status, $type, $this->user->email, $this->user->username);


	}

	public function ajaxToursRemoveAction()
	{
		$this->_request->setParam('no_tidy', true);

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( ! $this->agency->hasEscort($escort_id) ) die;
		}
		elseif ( $this->user->isEscort() ) {
			$escort_id = $this->escort->getId();
		}

		$tour_id = intval($this->_getParam('id'));

		$this->client->call('removeEscortTour', array($escort_id, $tour_id));

		$this->ajaxToursAction(array('status' => 'success'));
	}

	private function _loadPhotos()
	{
		//$photos = 

		$public_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, false));
		$nil = null;
		$private_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, true));

		return $this->view->photos = array_merge($public_photos, $private_photos);
	}

	public function photosAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->user = $this->user;

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort'));

			if ( 1 > $escort_id ) die;

			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}

			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}


		$photos = $this->_loadPhotos();

		$photos_ids = array();
		foreach ( $photos as $photo ) {
			$photo_ids[] = intval($photo['id']);
		}

		$action = $this->_getParam('a');

		$photos_min_count = Cubix_Application::getById(Cubix_Application::getId())->min_photos;

		$this->view->is_private = $is_private = (bool) intval($this->_getParam('is_private'));

		if ( ! is_null($action) ) {
			if ( 'set-main' == $action ) {
				$ids = $this->_getParam('photo_id');
				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
				}
				$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED;
				if(count($ids) == 1){
					$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED_ONE;
				}
				$photo_id = reset($ids);
				if ( ! in_array($photo_id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}

				$photo = new Model_Escort_PhotoItem(array('id' => $photo_id ,'escort_id' => $escort_id));
				$photo->setRotatePics($ids);
				$result = $photo->setMain();
				$client->call('Escorts.setPhotoRotateType', array($escort_id, $rotate_type));
				$escort->photo_rotate_type = $rotate_type;

				$this->_loadPhotos();
			}
			elseif('set-cover' == $action){
				$ids = $this->_getParam('photo_id');
				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
				}

				$photo_id = reset($ids);
				if ( ! in_array($photo_id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}

				$photo = new Model_Escort_PhotoItem(array('id' => $photo_id ,'escort_id' => $this->escort_id));
				$photo->setMobileCover();
				$this->_loadPhotos();
			}
			elseif ( 'delete' == $action ) {
				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = 'Please select at least one photo';
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						return $this->view->actionError = 'Invalid id of one of the photos';
					}
				}

				$photo = new Model_Escort_Photos();
				$result = $photo->remove($ids);

				// If count of photos is smaller than required, deactivate escort
				// $photos_count = $escort->getPhotosCount();
				// if ( $photos_count < $photos_min_count ) {
				// 	if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_ACTIVE))) ) {
				// 		$client->call('Escorts.removeStatusBit', array($escort_id, array(
				// 			Model_Escorts::ESCORT_STATUS_ACTIVE
				// 		)));
				// 	}

				// 	if ( ! $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS))) ) {
				// 		$client->call('Escorts.setStatusBit', array($escort_id, array(
				// 			Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS
				// 		)));
				// 	}
				// }

				$this->_loadPhotos();
			}
			elseif ( 'upload' == $action ) {
				try {
					//echo json_encode(array('text' => 'ddd'));die;
					if ( ! isset($_FILES['Filedata']) || ! isset($_FILES['Filedata']['name']) || ! strlen($_FILES['Filedata']['name']) ) {
						$this->view->uploadError = 'Please select a photo to upload';
						return;
					}

					// Save on remote storage
					$images = new Cubix_Images();
					$image = $images->save($_FILES['Filedata']['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $_FILES['Filedata']['name']))));

					$image = new Cubix_Images_Entry($image);
					$image->setSize('sthumb');
					$image->setCatalogId($escort->id);
					$image_url = $images->getUrl($image);


					$photo_arr = array(
						'escort_id' => $escort->id,
						'hash' => $image->getHash(),
						'ext' => $image->getExt(),
						'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD
					);

					/*if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_PROFILE))) || $client->call('Escorts.isAutoApproval', array($escort_id)) ) {
						$photo_arr['is_approved'] = 1;
					}*/

					$photo = new Model_Escort_PhotoItem($photo_arr);

					$model = new Model_Escort_Photos();
					$photo = $model->save($photo);

					// // If count of photos is smaller than required, deactivate escort
					// $photos_count = $escort->getPhotosCount();
					// if ( $photos_count < $photos_min_count ) {
					// 	if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_ACTIVE))) ) {
					// 		$client->call('Escorts.removeStatusBit', array($escort_id, array(
					// 			Model_Escorts::ESCORT_STATUS_ACTIVE
					// 		)));
					// 	}


					// 	if ( ! $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS))) ) {
					// 		$client->call('Escorts.setStatusBit', array($escort_id, array(
					// 			Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS
					// 		)));
					// 	}
					// }
					// // Otherwise activate escort
					// else {
					// 	//echo "cool";
					// 	if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS))) ) {
					// 		$client->call('Escorts.removeStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS)));
					// 	}

					// 	if ( ! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED)) &&
					// 		! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_OWNER_DISABLED)) )
					// 	{
					// 		if (
					// 			(! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_IS_NEW)) &&
					// 			! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_NO_PROFILE)) &&
					// 			! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_PROFILE_CHANGED)))
					// 		) {
					// 			$client->call('Escorts.setStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_ACTIVE));
					// 		}
					// 	}
					// }

					$this->view->newPhoto = $photo;

					echo json_encode(array(
						'status' => 1,
						'name' => "success"
					));
					die;

				} catch ( Exception $e ) {
					echo json_encode(array(
						'status' => 0,
						'error' => $e->getMessage()
					));
					die;
					$this->view->uploadError = $e->getMessage();
				}
			}
			elseif ( 'set-adj' == $action ) {
				$photo_id = intval($this->_getParam('photo_id'));

				if ( ! in_array($photo_id, $photo_ids) ) {
					die(json_encode(array('error' => 'An error occured')));
				}

				$photo = new Model_Escort_PhotoItem(array(
					'id' => $photo_id
				));

				try {
					$hash = $photo->getHash();
					$result = array(
						'x' => intval($this->_getParam('x')),
						'y' => intval($this->_getParam('y')),
						'px' => floatval($this->_getParam('px')),
						'py' => floatval($this->_getParam('py'))
					);
					$photo->setCropArgs($result);

					// Crop All images
					$size_map = array(
						'backend_thumb' => array('width' => 150, 'height' => 205),
						'medium' => array('width' => 225, 'height' => 300),
						'thumb' => array('width' => 150, 'height' => 200),
						'nlthumb' => array('width' => 120, 'height' => 160),
						'sthumb' => array('width' => 76, 'height' => 103),
						'lvthumb' => array('width' => 75, 'height' => 100),
						'agency_p100' => array('width' => 90, 'height' => 120),
						't100p' => array('width' => 117, 'height' => 97)
					);
					$conf = Zend_Registry::get('images_config');

					get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
					// echo $conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;

					$catalog = $escort_id;
					$a = array();
					if ( is_numeric($catalog) ) {
						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}
					}
					else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
						array_shift($a);
						$catalog = $a[0];

						$parts = array();

						if ( strlen($catalog) > 2 ) {
							$parts[] = substr($catalog, 0, 2);
							$parts[] = substr($catalog, 2);
						}
						else {
							$parts[] = '_';
							$parts[] = $catalog;
						}

						$parts[] = $a[1];
					}

					$catalog = implode('/', $parts);

					foreach($size_map as $size => $sm) {
						get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
					}
				}
				catch ( Exception $e ) {
					die(json_encode(array('error' => 'An error occured')));
				}

				die(json_encode(array('success' => true)));
			}
			elseif ( 'make' == substr($action, 0, 4) ) {
				$type = substr($action, 5); if ( ! in_array($type, array('public', 'private')) ) die;

				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					return $this->view->actionError = 'Please select at least one photo';
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						return $this->view->actionError = 'Invalid id of one of the photos';
					}
				}

				foreach ( $ids as $id ) {
					$photo = new Model_Escort_PhotoItem(array('id' => $id));
					$photo->make($type == 'private' ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD);
				}

				$this->_loadPhotos();
			}
			elseif ( 'sort' == $action ) {
				$ids = $this->_getParam('photo_id');

				if ( ! is_array($ids) || ! count($ids) ) {
					die('Please select at least one photo');
				}

				foreach ( $ids as $id ) {
					if ( ! in_array($id, $photo_ids) ) {
						die('Invalid id of one of the photos');
					}
				}

				$model = new Model_Escort_Photos();
				$model->reorder($ids);

				die;
			}
			$this->view->escort = $escort;
			// if ( 'upload' != $action ) {
			// 	$this->view->layout()->disableLayout();
			// 	$this->_helper->viewRenderer->setNoRender(true);
			// 	die;
			// }
		}
	}

	public function plainPhotosAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->user = $this->user;

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort'));

			if ( 1 > $escort_id ) die;

			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}

			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}


		$photos = $this->_loadPhotos();

		$photo_ids = array();
		foreach ( $photos as $photo ) {
			$photo_ids[] = intval($photo['id']);
			}

		$action = $this->_getParam('a');

		$photos_min_count = Cubix_Application::getById(Cubix_Application::getId())->min_photos;

		$this->view->is_private = $is_private = (bool) intval($this->_getParam('is_private'));

		if ( 'set-main' == $action || ! is_null($this->_getParam('set_main')) ) {
			$ids = $this->_getParam('photo_id');
			//$rotate_type = $this->_getParam('rotate');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED;
			if(count($ids) == 1){
				$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED_ONE;
			}

			$photo_id = $client->call('Escorts.getApprovedPhoto', array($escort_id, implode(',',$ids)));

			if(!$photo_id){
				return $this->view->actionError = Cubix_I18n::translate('sys_error_not_approved_photo');
			}
			elseif ( ! in_array($photo_id, $photo_ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
			}
			/*$photo_id = reset($ids);
			if ( ! in_array($photo_id, $photo_ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
			}*/

			$photo = new Model_Escort_PhotoItem(array('id' => $photo_id ,'escort_id' => $escort_id));
			$photo->setRotatePics($ids);
			$result = $photo->setMain();
			$client->call('Escorts.setPhotoRotateType', array($escort_id, $rotate_type));
			$escort->photo_rotate_type = $rotate_type;

			$this->_loadPhotos();
		}
		elseif('set-cover' == $action || !is_null($this->_getParam('set_cover')) ) {
			$ids = $this->_getParam('photo_id');
			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			$photo_id = $client->call('Escorts.getApprovedPhoto', array($escort_id, implode(',',$ids)));

			if(!$photo_id){
				return $this->view->actionError = Cubix_I18n::translate('sys_error_not_approved_photo');
			}
			elseif ( ! in_array($photo_id, $photo_ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
			}

			$photo = new Model_Escort_PhotoItem(array('id' => $photo_id ,'escort_id' => $escort_id));
			$photo->setMobileCover();
			$this->_loadPhotos();
		}
		elseif ( 'delete' == $action || ! is_null($this->_getParam('delete')) ) {
			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}
			}

			$photo = new Model_Escort_Photos();
			$result = $photo->remove($ids);

			$this->_loadPhotos();
		}
		elseif ( 'upload' == $action || ! is_null($this->_getParam('upload')) ) {

				$set_photo = false;
				$config = Zend_Registry::get('images_config');
				$new_photos = array();
				$upload_errors = array();
				$model = new Model_Escort_Photos();

				foreach ( $_FILES as $i => $file )
				{
					try {
						if ( ! isset($file['name']) || ! strlen($file['name']) ) {
							continue;
						}
						else {
							$set_photo = true;
						}

						$img_ext = strtolower(@end(explode('.', $file[name])));
						if (!in_array( $img_ext , $config['allowedExts'])){
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
						}

						/*$photo_count = $model->getEscortPhotoCount($escort_id);

						if ( $photo_count >= Model_Escort_Photos::MAX_PHOTOS_COUNT ){
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img_count_too_much'), Cubix_Images::ERROR_IMAGE_COUNT_LIMIT_OVER);
						}*/

						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($file['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $file['name']))));

						$image = new Cubix_Images_Entry($image);
						$image->setSize('sthumb');
						$image->setCatalogId($escort->id);
						$image_url = $images->getUrl($image);

						$image_size = getimagesize($file['tmp_name']);
						$is_portrait = 0;
						if ( $image_size ) {
							if ( $image_size[0] < $image_size[1] ) {
								$is_portrait = 1;
							}
						}

						$photo_arr = array(
							'escort_id' => $escort->id,
							'hash' => $image->getHash(),
							'ext' => $image->getExt(),
							'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_SOFT,
							'is_portrait' => $is_portrait,
							'width' => $image_size[0],
							'height' => $image_size[1],
							'creation_date' => date('Y-m-d H:i:s', time())
						);

						if ( $client->call('Escorts.isPhotoAutoApproval', array($escort_id)) ) {
							$photo_arr['is_approved'] = 1;
						}
						// commented because of approved pictures bug
						/*else if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_PROFILE))) ) {
							$photo_arr['is_approved'] = 1;
						}*/

						$photo = new Model_Escort_PhotoItem($photo_arr);

						$photo = $model->save($photo);

						$new_photos[] = $photo;
					} catch ( Exception $e ) {
						$upload_errors[$i] = $file['name'] . ' (' . $e->getMessage() . ')';
					}
			}

			if ( ! $set_photo ) {
				$this->view->uploadError = Cubix_I18n::translate('sys_error_select_photo');
			}

			$this->view->newPhotos = $new_photos;
			$this->view->uploadErrors = $upload_errors;
		}
		elseif ( 'set-adj' == $action ) {
			$photo_id = intval($this->_getParam('photo_id'));

			if ( ! in_array($photo_id, $photo_ids) ) {
				die(json_encode(array('error' => 'An error occured')));
			}

			$photo = new Model_Escort_PhotoItem(array(
				'id' => $photo_id
			));

			try {
				$hash = $photo->getHash();
				$result = array(
					'x' => intval($this->_getParam('x')),
					'y' => intval($this->_getParam('y')),
					'px' => floatval($this->_getParam('px')),
					'py' => floatval($this->_getParam('py'))
				);
				$photo->setCropArgs($result);

				// Crop All images
				$size_map = array(
					'backend_thumb' => array('width' => 150, 'height' => 205),
					'medium' => array('width' => 225, 'height' => 300),
					'thumb' => array('width' => 150, 'height' => 200),
					'nlthumb' => array('width' => 120, 'height' => 160),
					'sthumb' => array('width' => 76, 'height' => 103),
					'lvthumb' => array('width' => 75, 'height' => 100),
					'agency_p100' => array('width' => 90, 'height' => 120),
					't100p' => array('width' => 117, 'height' => 97)
				);
				$conf = Zend_Registry::get('images_config');

				get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
				// echo $conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;

				$catalog = $escort_id;
				$a = array();
				if ( is_numeric($catalog) ) {
					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}
				}
				else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
					array_shift($a);
					$catalog = $a[0];

					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}

					$parts[] = $a[1];
				}

				$catalog = implode('/', $parts);

				foreach($size_map as $size => $sm) {
					get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
				}
			}
			catch ( Exception $e ) {
				die(json_encode(array('error' => 'An error occured')));
			}

			die(json_encode(array('success' => true)));
		}
		elseif ( ! is_null($this->_getParam('make_private')) || ! is_null($this->_getParam('make_public')) ) {

			if ( ! is_null($this->_getParam('make_private')) ) {
				$type = 'private';
			} elseif ( ! is_null($this->_getParam('make_public')) ) {
				$type = 'public';
			} else {
				die;
			}

			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}
			}

			foreach ( $ids as $id ) {
				$photo = new Model_Escort_PhotoItem(array('id' => $id));
				$photo->make($type == 'private' ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD);
			}

			$this->_loadPhotos();
		}
		elseif ( 'sort' == $action ) {
			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				die(Cubix_I18n::translate('sys_error_select_at_least_on_photo'));
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					die(Cubix_I18n::translate('sys_error_invalid_id_photo'));
				}
			}

			$model = new Model_Escort_Photos();
			$model->reorder($ids);

			die;
		}
		$this->view->escort = $escort;
			// if ( 'upload' != $action ) {
			// 	$this->view->layout()->disableLayout();
			// 	$this->_helper->viewRenderer->setNoRender(true);
			// 	die;
			// }
	}

	public function escortsAction()
	{
		if ( ! $this->user->isAgency() ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}
		$this->view->s_showname = '';
		$this->view->is_active = (bool) $this->_getParam('is_active');

		$defs = Zend_Registry::get('definitions');
		$filterStructure = $defs['agency_escorts_sort'];

		$sort = new Cubix_NestedMenu(array('childs' => $filterStructure));
		$sort->setId('sort-options');
		$sort->setSelected( $sort->getByValue('paid-package') );

		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->_request->isPost() ) {
			$action = $this->_getParam('a');

			if ( $this->user->isEscort() ) {
				$escort = $this->user->getEscort();
				$escort_id = $escort->id;
			}
			else {
				$escort_id = intval($this->_getParam('escort_id'));

				if ( 0 == $escort_id ) $escort_id = null;

				if ( ! is_null($escort_id) && ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
					die;
				}

				$model = new Model_Escorts();
				$escort = $model->getById($escort_id);
			}

			switch ( $action ) {
				case 'activate':
					$this->view->actionError = '';
					if ( $escort->status & Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED ) {
						$this->view->actionError .= 'The escort is deactivated by administration!<br/>';
					}

					if ( $escort->status & Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS ) {
						$this->view->actionError .= 'You cannot activate this escort, not enough photos!<br/>';
					}


					if ( $escort->status & Model_Escorts::ESCORT_STATUS_IS_NEW || $escort->status & Model_Escorts::ESCORT_STATUS_NO_PROFILE ) {
						$this->view->actionError .= 'The profile of this escort has not been approved yet!<br/>';
					}

					if ( ! strlen($this->view->actionError) ) {
						$client->call('Escorts.activate', array($escort_id));

						$this->view->escorts = $escorts = $this->agency->getEscorts();
					}

					break;
				case 'deactivate':
					$client->call('Escorts.deactivate', array($escort_id));

					$this->view->escorts = $escorts = $this->agency->getEscorts();

					break;
				case 'delete':
					// TODO: escort products deletion. Premium package assigned escorts error.
					$client->call('Escorts.deleteTemporary', array($escort_id));
					$this->view->escorts = $escorts = $this->agency->getEscorts();
					break;
				default:

			}
		}

        $per_page = 4;
        $cur_page = 1;

        $escorts_a = $this->agency->getEscortsPerPage($cur_page, $per_page, 1); //32 = Active
        $escorts_i = $this->agency->getEscortsPerPage($cur_page,$per_page);//default inactive
        $escorts_d = $this->agency->getEscortsPerPage($cur_page,$per_page,-1);//default inactive

        $this->view->escorts_a_count = $escorts_a['escorts_count'];
        $this->view->escorts_i_count = $escorts_i['escorts_count'];
        $this->view->escorts_d_count = $escorts_d['escorts_count'];

        $this->view->escorts_per_page = $per_page;
        $this->view->current_page = $cur_page;

        $this->view->escorts_active = $escorts_a['escorts'];
        $this->view->escorts_inactive = $escorts_i['escorts'];
        $this->view->escorts_deleted = $escorts_d['escorts'];

		$this->view->sort = $sort;

	}

	private function _getPremiumList()
	{
		return Cubix_Api::getInstance()->call('premium_getAgencyPremiumEscorts', array($this->agency->id, $this->view->lang()));
	}

	public function premiumAction()
	{
		if ( $this->user->isAgency() ) {
			$this->_helper->viewRenderer->setScriptAction('agency-premium-v2');


			$this->view->escorts = $this->_getPremiumList();
		}
		elseif ( $this->user->isEscort() ) {
            if ( ! $this->escort->hasProduct(14) ) die;

			$this->_helper->viewRenderer->setScriptAction('escort-premium-v2');

            $this->view->escorts = $this->_getDiamondEscort();
		}
	}

	public function ajaxPremiumSwitchAction()
	{
		// Require user to be an agency
		$this->_helper->viewRenderer->setNoRender(true);
		$this->view->layout()->disableLayout();

		if ( ! $this->user->isAgency() ) die;

		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'from_escort_id' => 'int-nz',
			'to_escort_id' => 'int-nz',
			'city_ids' => 'arr-int'
		));
		$data = $form->getData();

		// If escort does not have any of these escorts
		if ( ! $this->agency->hasEscort($data['from_escort_id']) ||
				! $this->agency->hasEscort($data['to_escort_id']) ) {
			die;
		}

		$client_xml = new Cubix_Api_XmlRpc_Client();
		
		$cities = $client_xml->call('OnlineBillingV2.getPremiumCities', array($data['from_escort_id']));
		$data['city_ids'] = explode(',',$cities);		
		$client = Cubix_Api::getInstance();
		$result = $client->call('premium_switchActivePackages', array($data['from_escort_id'], $data['to_escort_id'], $data['city_ids']));
				
		die(json_encode(array('data' => $this->_getPremiumList(), 'status' => 'success')));
	}

    public function ajaxPremiumSwitchMobileAction()
    {
        // Require user to be an agency
        if ( ! $this->user->isAgency() ) die;

        $form = new Cubix_Form_Data($this->_request);
        $form->setFields(array(
            'from_escort_id' => 'int-nz',
            'to_escort_id' => 'int-nz',
            'city_ids' => 'arr-int'
        ));
        $data = $form->getData();

        // If escort does not have any of these escorts
        if ( ! $this->agency->hasEscort($data['from_escort_id']) ||
            ! $this->agency->hasEscort($data['to_escort_id']) ) {
            die;
        }

        $client = Cubix_Api::getInstance();
        $result = $client->call('premium_switchActivePackages', array($data['from_escort_id'], $data['to_escort_id'], $data['city_ids']));

        if ( $result !== true && $result['success'] == false ) {
            die(json_encode(array('status' => 'failure', 'error' => $result['error'])));
        }

        die(json_encode(array('data' => $this->_getPremiumList(), 'status' => 'success')));
    }

	public function agencyEscortsAction(){
		if ( ! $this->user->isAgency() ) die;

		$sort = $this->view->sort = $this->_getParam('sort');
		$action = $this->view->action = $this->_getParam('for');

		if( !$action || !in_array( $action, array( 1, 2 ) ) ) die;

		if( !$sort ) $sort = 'paid-package';

		switch($sort){
			case 'paid-package':
				$sort = 'p.name DESC, e.showname ASC';
				BREAK;

			case 'alpha':
				$sort = 'e.showname';
				BREAK;

			case 'escort-id':
				$sort = 'e.id';
				BREAK;

			case 'last-modified':
				$sort = 'e.date_last_modified';
				BREAK;

			default:
				$sort = 'p.name DESC';
				BREAK;
		}

		$per_page = 1500;
		$page = 1;

		$client = new Cubix_Api_XmlRpc_Client();
		//$escorts = $client->call('Agencies.getEscortsByStatus', array( $this->agency->id, $page ,$per_page, 2, false, null, $sort ));
		$escorts = $client->call('OnlineBillingV2.getAgencyEscorts', array($this->agency->id));
		
		$session = new Zend_Session_Namespace('self-checkout-wizard');
		$shoping_cart_escort = array_keys($session->cart);
		$shoping_cart_escort_count = count($session->cart);
		if($action == 1){
			foreach($escorts as $i => $escort) {
				$allow = true;
				
				if(in_array($escort['id'], $shoping_cart_escort )){
					unset($escorts[$i]);
					continue;
				}
				
				if ( count( $escort['escort_packages'] ) ) {
					$current_package = $escort['escort_packages'][0];
					$pending_package = $escort['escort_packages'][1];

					if ( $current_package && $current_package['status'] == self::STATUS_ACTIVE ) {
						$escorts[$i]['has_active_package'] = true;
					}

					if ( $current_package && ( $current_package['status'] == self::STATUS_PENDING || $current_package['status'] == self::STATUS_SUSPENDED ) ) {
						$allow = false;
					} elseif ( $pending_package && $pending_package['status'] == self::STATUS_PENDING ) {
						$allow = false;
					} elseif ( $current_package['status'] == self::STATUS_ACTIVE && $current_package['expiration_date'] - time() > 5 *24*60*60 ) {
						$allow = false;
					}

					if ( ! $allow ) {
						unset($escorts[$i]);
					}
				}
				
				
			}
		}
		elseif($action == 2){
			foreach($escorts as $i => $escort) {
				$allow = false;
				
				if ( count( $escort['escort_packages'] ) ) {
					$current_package = $escort['escort_packages'][0];
					$pending_package = $escort['escort_packages'][1];

					if ( $current_package && $current_package['status'] == self::STATUS_ACTIVE ) {
						$escorts[$i]['has_active_package'] = true;
					}
					$allow = true;
				}
				
				if ( ! $allow ) {
					unset($escorts[$i]);
				}
			}
		}
		
		$this->view->escorts_count = count($escorts);
		$this->view->escorts = $escorts;
		$this->view->shopong_cart_escorts_count = $shoping_cart_escort_count;
		
	}

    /* Update Grigor */
    public function ajaxGetAgencyEscortsAction(){
        if ( ! $this->user->isAgency() ) die;

        $this->view->layout()->disableLayout();

        $page = $this->_getParam('cpage');
		$showname = $this->view->showname = $this->_getParam('showname');
		$_from = $this->view->e_from = $this->_getParam('escort_from');

        $isActive = $this->_getParam('isActive');
		$sort = $this->view->sort = $this->_getParam('sort');
        $per_page = 4;

        $status = null;

        $view = "ajax-get-agency-escorts";

		if( !$sort ) $sort = 'paid-package';

		switch($sort){
			case 'paid-package':
				$sort = 'p.name DESC, e.showname ASC';
				BREAK;

			case 'alpha':
				$sort = 'e.showname';
				BREAK;

			case 'escort-id':
				$sort = 'e.id';
				BREAK;

			case 'last-modified':
				$sort = 'e.date_last_modified';
				BREAK;

			default:
				$sort = 'p.name DESC';
				BREAK;

		}

        if($isActive){
           $status =  $isActive;
        }

		//print_r( array($this->agency->id,$page,$per_page,$status,false, $showname, $sort) ); die;

        $client = new Cubix_Api_XmlRpc_Client();
		$escorts = $client->call('Agencies.getEscortsByStatus', array($this->agency->id,$page,$per_page,$status,false, $showname, $sort));

		if( $_from ){
			foreach ( $escorts['escorts'] as $key => $_escort ) {
				if ( $_escort['id'] == $_from ) {
					unset( $escorts['escorts'][$key] );
				}
			}
		}

        $cur_page = $page;

        $this->view->escorts_count = $escorts['escorts_count'];
        $this->view->escortStatus = $status;

        $this->view->escorts_per_page = $per_page;
        $this->view->current_page = $cur_page;

        $this->view->escorts = $escorts['escorts'];


        $this->_helper->viewRenderer->setScriptAction($view);
    }

	public function escortProfileAction(){
		$client = new Cubix_Api_XmlRpc_Client();

		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_getParam('escort_id'));

			if ( 0 == $escort_id ) $escort_id = null;

			if ( ! is_null($escort_id) && ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}

			$model = new Model_Escorts();
			$escort = $this->view->escort = $model->getById($escort_id);

			// $this->view->is_verified = 0;
			// if( $escort->isVerified() ){
			// 	$this->view->is_verified = 1;
			// }

			$package = $this->_getEscortActivePackage($escort_id);
			//print_r($package); die;
			$this->view->package = $package;
		} else {
			die;
		}
	}

    public function ajaxEscortsDoAction(){
        $action = $this->_getParam('a');
        $client = new Cubix_Api_XmlRpc_Client();


        $escort_id = intval($this->_getParam('escort_id'));

        if ( 0 == $escort_id ) $escort_id = null;

        if ( ! is_null($escort_id) && ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
            die;
        }

        $model = new Model_Escorts();
        $escort = $model->getById($escort_id);

        $return['success'] = false;
        $return['message'] = '';


        if($escort){

            switch ( $action ) {
                case 'activate':

                    if ( $escort->status & Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED ) {
                        $return['message'] .= 'The escort is deactivated by administration!<br/>';
                    }

                    if ( $escort->status & Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS ) {
                        $return['message'] .= 'You cannot activate this escort, not enough photos!<br/>';
                    }

					if ( $escort->status & Model_Escorts::ESCORT_STATUS_SUSPICIOUS_DELETED ) {
                        $return['message'] .= 'You cannot activate this escort, suspicious photos!<br/>';
                    }


                    if ( $escort->status & Model_Escorts::ESCORT_STATUS_IS_NEW || $escort->status & Model_Escorts::ESCORT_STATUS_NO_PROFILE ) {
                        $return['message'] .= 'The profile of this escort has not been approved yet!<br/>';
                    }



                    if ( ! strlen($return['message']) ) {
                        $client->call('Escorts.activate', array($escort_id));

                        $return['success'] = true;
                    }

                    break;
                case 'deactivate':
                    $client->call('Escorts.deactivate', array($escort_id));

                    $return['success'] = true;

                    break;
                case 'delete':
                    // TODO: escort products deletion. Premium package assigned escorts error.

                    $res = $client->call('Escorts.deleteTemporary', array($escort_id));
                    if($res){
                        $res = json_decode($res);

                        if ( !$res->success ) {
                            $return['message'] = $res->message;
                        }
                    }

                   // $this->view->escorts = $escorts = $this->agency->getEscorts();
                    break;
                case 'restore':
                    // TODO: escort restore
                    $client->call('Escorts.restore', array($escort_id));
                    $return['success'] = true;
                   // $this->view->escorts = $escorts = $this->agency->getEscorts();
                    break;
                default:

            }
        }

		if( !$this->_getParam('notajax') )
        	die(json_encode($return));
		else
			$this->_redirect($this->view->getLink('private-v2'));
    }
    /* Update Grigor */


	public function ajaxEscortsAction()
	{
		// Require user to be an agency
		if ( ! $this->user->isAgency() ) die;

		$escorts = Cubix_Api::getInstance()->call('premium_getAgencyNonPremiumEscorts', array($this->agency->id));

		die(json_encode($escorts));
	}

	public function ajaxEscortCitiesAction()
	{
		// Require user to be an agency
		if ( ! $this->user->isAgency() ) die;

		$escort_id = intval($this->_getParam('escort_id'));

		// If this escort does not belong to this agency
		if ( ! $this->agency->hasEscort($escort_id) ) die;

		$cities = Cubix_Api::getInstance()->call('premium_getEscortCities', array($escort_id, $this->view->lang()));
		$limit = Cubix_Api::getInstance()->call('premium_getEscortCitiesLimit', array($escort_id));

		die(json_encode(array('cities' => $cities, 'limit' => $limit)));
	}

	public function getBubbleTextAction()
	{
		if ( ! $this->user->isAgency() && ! $this->user->isEscort() ) {
			die();
		}

		$req = $this->_request;

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($req->escort_id);
		}

		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$bubble = $client->call('Escorts.getBubbleText', array($escort_id));

		die(json_encode($bubble));
	}

	public function addBubbleTextAction()
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort_id'));
			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}
		}

		$req = $this->_request;

		$text = strip_tags(mb_substr(stripslashes($req->text), 0, 1000, 'UTF-8'));

		$data = array('escort_id' => $escort_id, 'text' => $text);
		if ( strlen($text) > 0 ) {
			$data = array_merge($data, array('status' => 1));
		}
		else {
			$data = array_merge($data, array('status' => 0));
		}

		$config_system = Zend_Registry::get('system_config');

		$status = $client->call('Escorts.addBubbleText', array($data, $config_system['bubbleTextApprovation']));

		echo $status;

		die;
	}
 
	public function existsByShownameAction()
	{
		$model = new Model_EscortsV2();

		$req = $this->_request;

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort'));

			if ( $escort_id > 0 ) {
				if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
					die;
				}
			}
		}

		//$escort_id = $req->getParam('escort_id', false);
		$showname = $req->getParam('showname', false);

		$status = $model->existsByShowname($showname, $escort_id);

		if ( $status ) {
			$stat = 'true';
		} else {
			$stat = 'false';
		}

		die(json_encode(array('status' => $stat)));
	}


	public function escortActivePackageAction()
	{

        //$this->view->layout()->disableLayout();

        $escort_id = $this->_request->getParam('escort_id');

        $isActive = $this->_request->getParam('isActive');

		$per_page = 10000;
		$cur_page = 1;

		$client = new Cubix_Api_XmlRpc_Client();
		$has_package = $client->call( 'Escorts.checkIfHasActivePackage', array( $escort_id ) );
		if( !$has_package ) die('Invalid request !');

		$escorts_a = $this->agency->getEscortsPerPage($cur_page, $per_page, 1); //32 = Active

		$this->view->escorts_a_count = $escorts_a['escorts_count'];

		$this->view->escorts_per_page = $per_page;
		$this->view->current_page = $cur_page;
		$this->view->escort_id = $escort_id;

		$_act_escorts = $escorts_a['escorts'];

		$this->view->escorts_active = $_act_escorts;

        if ( $this->user->isAgency() && $this->agency->hasEscort($escort_id) ){
			$package = $this->_getEscortActivePackage($escort_id);
            $this->view->package = $package;
            $this->view->isActive = $isActive;
		}
	}

	private function _getEscortActivePackage($escort_id){
			return Cubix_Api::getInstance()->call('getEscortActivePackage', array($escort_id,Cubix_I18n::getLang()));
	}

	public function clientBlacklistAction()
	{
		if ($this->user->user_type == 'escort')
		{
			$m = new Model_EscortsV2();

			$e = $m->getByUserId($this->user->id);
			$client = new Cubix_Api_XmlRpc_Client();
			$count = $client->call('Escorts.getPackagesCount', array($e->id));

			if (!$count)
			{
				$this->_redirect($this->view->getLink('private-v2'));
				return;
			}
		}
		elseif ($this->user->user_type == 'agency')
		{
			$client = new Cubix_Api_XmlRpc_Client();
			$count = $client->call('Escorts.getAgencyPackagesCount', array($this->user->id));

			if (!$count)
			{
				$this->_redirect($this->view->getLink('private-v2'));
				return;
			}
		}
		elseif ($this->user->user_type == 'member')
		{
			$this->_redirect($this->view->getLink('private-v2'));
			return;
		}

		$req = $this->_request;

		$page = 1;
		$per_page = 10;
		if($this->_getParam('page')){
			$page = $this->_getParam('page');
		}

		$criteria = $this->view->criteria = $req->client_criterias;

		$params = array($criteria, $page, $per_page, $req->lang_id);
		$data = Cubix_Api::getInstance()->call('getBlacklistedClients', array($params));

		// Entry part, get entry from API
		$adminEntries = Cubix_Api::getInstance()->call('getAdminBlacklistEntries');
		$this->view->adminEntries = $adminEntries;

		// Check if in cookie set show entry, show entry
		$show_admin_entries = false;
		if ( isset($_COOKIE['is_show_admin_entr']) ) {
			if ( $_COOKIE['is_show_admin_entr'] == 1 ) {
				$show_admin_entries = true;
			}
		}
		$this->view->show_admin_entries = $show_admin_entries;
		$citiesModel = new Model_Cities();
		//

		$result = $this->view->result = $data['clients'];
		$count = $this->view->count = $data['count'];
		$this->view->page = $page;
		$this->view->per_page = $per_page;

		$this->view->cities = $citiesModel->getByCountry( Cubix_Application::getAll()[0]->country_id );
	}

	public function addClientToBlacklistAction()
	{
		$req = $this->_request;

		$validator = new Cubix_Validator();
		$data = array();

		if ( $req->isPost() )
		{
			$date = intval( strtotime( $req->date ) );
			$client_name = $req->client_name;
			$client_phone = $req->client_phone;
			$comment = $req->comment;
			$city_id = $req->city_id_h;
			if(!empty($req->attach)){
				$photos = $req->attach;
			}

			if ( ! $city_id ) {
				$city_id = null;
			}

			if ( ! $date ) {
				$validator->setError('date', 'Date is Required');
			}

			else if ( $date > time() ) {
				$validator->setError('date', 'Date is on Future');
			}

			if ( ! strlen($client_phone) ) {
				$validator->setError('client_phone', 'Client Phone is Required');
			}

			if ( ! strlen($client_name) ) {
				$validator->setError('client_name', 'Client Name is Required');
			}

			if ( ! strlen($comment) ) {
				$validator->setError('comment', 'Comment is Required');
			}

			if ( $validator->isValid() ) {

				$data = array(
						'application_id' => Cubix_Application::getId(),
						'user_id' => $this->user->id,
						'date' => $date,
						'client_name' => $client_name,
						'city_id' => $city_id,
						'client_phone' => $client_phone,
						'comment' => $comment
				);
				if($photos){
					$data['photos'] = $photos;
				}

				Cubix_Api::getInstance()->call('addClientToBlacklist', array($data));
			}

			die(json_encode($validator->getStatus()));
		}
	}

	public function faqAction()
	{
		$lng = Cubix_I18n::getLang();

		$type = $this->user->user_type;

		$config = Zend_Registry::get('faq_config');
		$cache = Zend_Registry::get('cache');
		$cache_key = 'v2_faq_type_' . $type . '_lang_' . $lng;

		if ( ! $items = $cache->load($cache_key) )
		{
			$items = Cubix_Api::getInstance()->call('getFaqByType', array($type, $lng));

			$cache->save($items, $cache_key, array(), $config['cacheTime']);
		}

		$this->view->items = $items;
	}


	public function vipMemberCancelAction()
	{
		$this->view->layout()->disableLayout();

		$user_id = $this->user->id;
		$cancel = $this->_request->cancel;

		if ( $cancel ) {
			try {
				$updated = Cubix_Api::getInstance()->call('members.cancelPremium', array($user_id));
			}
			catch ( Exception $e ) {

			}

			$this->user['member_data']['is_premium'] = 0;
			$this->user['member_data']['is_recurring'] = 0;
			$this->user['member_data']['date_expires'] = null;

			Model_Users::setCurrent($this->user);
		}
	}

	public function ajaxEscortCommentsAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		$com_model = new Model_Comments();
		$page = intval($req->getParam('page'));
		$per_page = intval($req->getParam('per_page'));
		if($req->getParam('is_agency')){
			$escort_id = $req->getParam('escort_id');
			$this->view->comments = $com_model->getCommentsByEscortIds(json_decode($escort_id), $page, $per_page, $count);
			$this->view->is_agency = 1;
		}
		else{
			$escort_id = intval($req->getParam('escort_id'));
			$this->view->comments = $com_model->getEscortComments($page,$per_page, $count, $escort_id);
			$this->view->is_agency = 0;
		}
		$this->view->escort_id = $escort_id;
		$this->view->comments_count = $count;
		$this->view->comments_per_page = $per_page;
		$this->view->comments_page = $page;
	}

	public function ajaxEscortReviewsAction()
	{
		$this->view->layout()->disableLayout();
		$lng = Cubix_I18n::getLang();
		$req = $this->_request;
		$page = intval($req->getParam('page'));
		$per_page = intval($req->getParam('per_page'));
		if($req->getParam('is_agency')){
			$results = Cubix_Api::getInstance()->call('getReviewsForAgencyEscorts', array($this->user->id, $lng, $page, $per_page ));
			$this->view->reviews = $results['reviews'];
			$this->view->is_agency = 1;

		}
		else{
			$escort_id = intval($req->getParam('escort_id'));
			$this->view->escort_id = $escort_id;
			$results = Cubix_Api::getInstance()->call('getReviewsForEscort', array($this->user->id, $lng,$escort_id, $page, $per_page ));
			$this->view->reviews = $results['reviews'];
			$this->view->is_agency = 0;

		}
		$this->view->reviews_count = $results['count'];
		$this->view->reviews_per_page = $per_page;
		$this->view->reviews_page = $page;
	}

	public function addReplyAction()
	{
		$config = Zend_Registry::get('escorts_config');
		$this->view->layout()->disableLayout();
		$user_id = $this->user->id;

		if ( $this->_request->isPost() ) {

			$validator = new Cubix_Validator();

			$req = $this->_request;

			if (!trim($req->comment))
				$validator->setError('comment', __('comment_requiered'));
			elseif(strlen(trim($req->comment)) < intval($config['comments']['textLength']))
				$validator->setError('comment',  __('comment_is_short',array('LENGTH' => $config['comments']['textLength'])));
			if ( $validator->isValid() ) {
				$reply_comment = $req->comment;
				$comment_id = $req->comment_id;
				$escort_id =  $req->escort_id;
				$data = array(
					'user_id' => $user_id,
					'user_type' => 'escort',
					'escort_id' => $escort_id,
					'status' => Model_Comments::COMMENT_NOT_APPROVED,
					'message' => $reply_comment,
					'is_reply_to' => $comment_id
				);
				$comment_model = new Model_Comments();
				$comment_model->addComment($data);
			  }
			die(json_encode($validator->getStatus()));
		}

    }

	public static function _sortItemDecorator($item, $is_first = FALSE, $is_hidden = FALSE)
	{
		$lng = Cubix_I18n::getLang();
		$has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);

		$classes = array();

		if ( $is_first ) $classes[] = 'first';
		if ( $has_childs ) $classes[] = 'sub';

		$link = '#';
		if ( ! $has_childs ) {
			$link = self::$linkHelper->getLink('escorts', array('sort' => $item->value));
		}

		$title = $item->title;
		if ( $is_first && isset($item->title) ) {
			$title = $item->parent . ' - ' . $item->title;
		}

		//$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="' . $link . '"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ sort: \'' . $item->value . '\' })"' : '' ) . '>' . $title . '</a>';
		$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="#"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Agencyescort.Load( \'' .$lng. '\', 1 , 1,\'\', \'' . $item->value .'\')"' : '' ) . '>' . $title . '</a>';

		if ( $is_first ) {
			$html = '<div class="input-w"><div class="i">' .
				$html .
			'</div></div>';
		}

		return $html;
	}

	public function extendPackageAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;

		$client = Cubix_Api_XmlRpc_Client::getInstance();

		$escort_id = $this->view->escort_id = (int) $req->escort_id;
		$package_id = $this->view->package_id = (int) $req->package_id;
		$extension = (int) $req->extension;
		$opid = $this->view->opid = (int) $req->opid;

		$m_escorts = new Model_EscortsV2();
		$escort = $this->view->escort = $m_escorts->get($escort_id);

		$extend_products = $this->view->extend_products = $client->call('OnlineBilling.getExtendProducts', array($package_id));

		if ( $req->isPost() ) {

			if ( $this->user->isAgency() ) {
				if ( $escort->agency_id != $this->user->agency_data['agency_id'] ) {
					die(':)');
				}
			} else if ( $this->user->isEscort() ) {
				if ( $escort_id != $this->user->escort_data['escort_id'] ) {
					die(':)');
				}
			}

			$found = false;
			$amount = 0;
			foreach ( $extend_products as $product ) {
				if ( $product['id'] == $extension ) {
					$found = true;
					$amount = $product['price'];
					break;
				}
			}

			if ( ! $found ) {
				die(':)');
			}

			$this->view->all_is_ok = true;

			$this->view->gateway_url = self::GATEWAY_URL;

			$this->view->amount = number_format($amount, 2);
			$this->view->currency = self::CURRENCY;
			$this->view->reference = 'EP-' . $this->user->id . '|' . $extension . '|' . $opid;

			$client = new Cubix_Api_XmlRpc_Client();
			$epg_payment = new Model_EpgGateway();
			$success_url = 'http://www.6annonce.com/private-v2';

			$token = $epg_payment->getTokenForAuth($this->view->reference, $this->view->amount, $success_url);
			$client->call('OnlineBilling.storeToken', array($token, $this->user->id));

			$this->view->redirect_url = $epg_payment->getPaymentGatewayUrl($token);
			$this->view->use_epg = true;
		}
	}

	public function agencyEscortsListAction()
	{
		if ( ! $this->user->isAgency() ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}

		$this->view->s_showname = '';
		$this->view->is_active = (bool) $this->_getParam('is_active');

		$defs = Zend_Registry::get('definitions');
		$filterStructure = $defs['agency_escorts_sort'];

		$sort = new Cubix_NestedMenu(array('childs' => $filterStructure));
		$sort->setId('sort-options');
		$sort->setSelected( $sort->getByValue('paid-package') );

		$client = new Cubix_Api_XmlRpc_Client();

		$per_page = 10000;
		$cur_page = 1;

		$escorts_a = $this->agency->getEscortsPerPage($cur_page, $per_page, 1); //32 = Active

		$this->view->escorts_a_count = $escorts_a['escorts_count'];

		$this->view->escorts_per_page = $per_page;
		$this->view->current_page = $cur_page;

		$this->view->escorts_active = $escorts_a['escorts'];

		$this->view->sort = $sort;

		$session = new Zend_Session_Namespace('self-checkout-wizard');

		if ( ! count($session->cart) ) {
			$this->view->cart_packages = array();
		} else {
			$this->view->cart_packages = $session->cart;
		}

	}

	public function ajaxAllRotatableAction(){

		try{
			$req = $this->_request;
			$escort_id = intval($req->escort);
			$model = new Model_Escorts();
			$this->escort = $model->getById($escort_id);
			$photos = $this->_loadPhotos();

			$first_valid_photo = null;
			foreach ( $photos as $photo ) {
				if( $photo['type'] != ESCORT_PHOTO_TYPE_PRIVATE &&  $photo['type'] != ESCORT_PHOTO_TYPE_DISABLED  && $photo['status'] != Model_Escort_Photos::STATUS_NOT_VERIFIED && is_null($first_valid_photo)){
					$first_valid_photo = intval($photo['id']);
				}
			}
			$photo = new Model_Escort_PhotoItem(array('id' => $first_valid_photo,'escort_id' => $escort_id));
			$photo->setRotatePics();
			$result = $photo->setMain();

			$client = Cubix_Api_XmlRpc_Client::getInstance();
            $client->call('Escorts.setPhotoRotateType', array($escort_id, Model_Escort_Photos::PHOTO_ROTATE_ALL));

		}
		catch ( Exception $e ) {
			die( $e->getMessage());
		}
		die(json_encode(array('main' => $first_valid_photo)));
	}

	/***************************** Statuses Pages *****************************/

	public function notCompletedAction(){

	}

	public function enoughPicsAction(){

	}

	public function ownerDisabledAction(){
		$escort_id = $this->view->sel_escort_id = intval($this->_request->escort_id);

		if( $escort_id ) {
			$model = new Model_Escorts();
			$this->view->escort = $model->getById($escort_id);
		}
	}

	public function notAgeCertifiedAction(){
		$escort_id = $this->view->sel_escort_id = intval($this->_request->escort_id);

		if( $escort_id ) {
			$model = new Model_Escorts();
			$this->view->escort = $model->getById($escort_id);
		}
	}

	public function hasNoPackageAction(){
		$escort_id = $this->view->sel_escort_id = intval($this->_request->escort_id);

		if( $escort_id ) {
			$model = new Model_Escorts();
			$this->view->escort = $model->getById($escort_id);
		}
	}

	public function instantBookAction() {
		$client = new Cubix_Api_XmlRpc_Client();
		$this->view->settings = $settings = $client->call('InstantBook.getInstantBookContactInfo', array($this->escort->id));
	}

	public function instantBookHistoryAction() {
		$req = $this->_request;
		if ($req->isPost()) {
			$model = new Model_InstantBook();

			$filter = array(
				'date_to' => $req->date_to,
				'date_from' => $req->date_from,
				'escort_id' => $this->escort->id
			);

			$page = $req->page ? $req->page : 1;
			$per_page = $req->per_page ? $req->per_page : 10;


			$data = $model->getHistory($page, $per_page, $filter);

			foreach($data['entities'] as &$d) {
				if ($d['status'] == 'pending' || $d['status'] == 'deny') {
					$d['special_request'] = '';
					$d['phone'] = '';
				}

				if ($d['status'] == 'pending') {
					$d['wait_time'] = (($d['request_date'] + 60*60 + ($d['wait_time'] * 60)) * 1000);
				} else {
					$d['wait_time'] = '';
				}
			}

			die(json_encode($data));
		}
	}

	public function confirmPhoneAction(){
		$this->view->layout()->setLayout('mobile-private-profile');
    	$profile = $this->escort->getProfile();
    	$profile->load();
    	$this->view->addClass = 'layout-profile-index-contact-info';
    	$this->view->data = $data = $profile->getContactInfo();
    	$this->view->phone_prefix_id = $data['phone_country_id'];

    	$countyModel = new Model_Countries();
		$this->view->phone_countries = $countyModel->getPhoneCountries();

		$client = new Cubix_Api_XmlRpc_Client();
		$status = $client->call('Escorts.escortHasConfirmedPhone', array($this->escort->id));

		if(isset($status) && $status == 1){
			$this->_redirect($this->view->getLink('private-v2'));	
		}
    }

    /**
     * @param $data
     * @param $Files
     * @param $need_video
     * @throws Exception
     * @throws Zend_Exception
     */
    private function _validateAgeVerification($data, $Files, $need_video){

        $image_config = Zend_Registry::get('images_config');
        $video_config =  Zend_Registry::get('videos_config');
        $p_min_width = 400;
        $p_min_height = 600;
        $l_min_width = 500;
        $l_min_height = 375;
        if(!$data['escort_id']){
            throw new Exception('Please Select Escort',Cubix_Images::ERROR_IMAGE_INVALID);
        }
        if(!$data['name']){
            throw new Exception('Please Select Name',Cubix_Images::ERROR_IMAGE_INVALID);
        }
        if(!$data['last_name']){
            throw new Exception('Please Select Last Name',Cubix_Images::ERROR_IMAGE_INVALID);
        }

        if(preg_match("/[^a-zA-Z\s]{1,}/", $data['name'])){
            throw new Exception('The Name must contain only latin letters',Cubix_Images::ERROR_IMAGE_INVALID);
        }

        if(preg_match("/[^a-zA-Z\s]{1,}/", $data['last_name'])){
            throw new Exception('The Last Name must contain only latin letters',Cubix_Images::ERROR_IMAGE_INVALID);
        }

        if(!$data['day'] || !$data['month'] || !$data['year']){
            throw new Exception('Please Select Born Date',Cubix_Images::ERROR_IMAGE_INVALID);
        }
		
		$count_images = $this->getCountImages($Files['base_imgs']);
		if($count_images < 2) {
			throw new Exception('Please upload 2 base images. uploaded :'.$count_images, Cubix_Images::ERROR_IMAGE_INVALID);
		}
        	
        if($need_video){
            if( $Files['base_video']["tmp_name"] == ""){
                throw new Exception(Cubix_I18n::translate('age_certification_field_req'), Cubix_Images::ERROR_IMAGE_INVALID);
            }
        }
		
        $Files['eu_imgs'] = is_array($Files['eu_imgs']) ? $Files['eu_imgs'] : array();
		$Files['id_vrf_imgs'] = is_array($Files['id_vrf_imgs']) ? $Files['id_vrf_imgs'] : array();
		$merged_images = array_merge($Files['base_imgs'], $Files['eu_imgs'], $Files['id_vrf_imgs']);
		
        foreach ($merged_images as $key => $image){
            if (!isset($image['tmp_name']) || $image['tmp_name'] == '' ) continue;

            if (!in_array( $image['ext'] , $image_config['allowedExts'])){
                throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
            }
            $img_info = @getimagesize($image['tmp_name']);
            list($width, $height) = $img_info;
            if ( $width < $height ) {
                if ( $width < $p_min_width || $height < $p_min_height ) {
                    throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_min_a6', array('min_width' => $p_min_width, 'min_height' => $p_min_height, 'ratio' => Cubix_I18n::translate('portrait'))), Cubix_Images::ERROR_IMAGE_TOO_SMALL);
                }
            } else {
                if ( $width < $l_min_width || $height < $l_min_height ) {
                    throw new Exception(Cubix_I18n::translate('sys_error_upload_img_size_min_a6', array('min_width' => $l_min_width, 'min_height' => $l_min_height, 'ratio' => Cubix_I18n::translate('landscape'))), Cubix_Images::ERROR_IMAGE_TOO_SMALL);
                }
            }
        }
		
		$Files['base_video'] = is_array($Files['base_video']) ? $Files['base_video'] : array();
		$Files['eu_video'] = is_array($Files['eu_video']) ? $Files['eu_video'] : array();
		$merged_videos = array($Files['base_video'], $Files['eu_video']);
		
		foreach ($merged_videos as $key => $video){
			if($video['tmp_name'] != '') {
				if (!in_array($video['ext'], $video_config['allowedvideoExts'])) {
					throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $video_config['allowedvideoExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
				}
				elseif (number_format(($Files['video']['size'] / pow(1024, 2)), 2) > 10) {
					throw new Exception(Cubix_I18n::translate('sys_error_upload_video_allowed_max_size', array('size' => '10 MB')), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
				}
			}
		}

        
    }

    /**
     * upload escorts age verification files by chunks
     * print response i json
     */
    public function processAgeVerificationFilesAction() {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
		$image_config = Zend_Registry::get('images_config');
        $video_config =  Zend_Registry::get('videos_config');
        
        if ($this->user->isAgency()){
            $escortID = $this->getRequest()->getParam('escort_id');
        }elseif ($this->user->isEscort()){
            $escortID = $this->escort->id;
        }

        try {
            $response = $this->HTML5_upload($escortID);
            if($response['error'] == 0 && $response['finish']) {
                $error = false;
                $extension = end(explode(".", $response['name']));

                if (!in_array(strtolower($extension), array_merge( $image_config['allowedExts'], $video_config['allowedvideoExts'])  ) )
                {
                    $error = 'Not allowed file extension.';
                }

                if ($error) {
                    if (!empty($response['tmp_name'])) {
                        unlink($response['tmp_name']);
                    }
                    $return = array(
                        'status' => '0',
                        'error' => $error
                    );

                } else {
                    $return = array(
                        'status' => '1',
                        'error' => '0',
                        'finish'=>true
                    );
                }
            }elseif ($response['error'] == 0 && !$response['finish']){
                $return = array(
                    'error' => '0',
                    'size' => $response['size']
                );
            }
            else {
                if (!empty($response['tmp_name'])) {
                    unlink($response['tmp_name']);
                }
                $return = array(
                    'status' => '0',
                    'error' => 'File not uploaded'
                );
            }
            echo json_encode($return);
        } catch (Exception $e) {
            echo json_encode(array(
                'status' => 0,
                'error' => $e->getMessage()
            ));
        }
	}

	public function checkVerifyTypeAction()
	{
		$escort_id = intval($this->_request->escort_id);
		$need_video = $this->client->call('getNeedVideoVerify', array($escort_id));
		$verify_model = new Model_VerifyRequests();
		$need_ver = $verify_model->needVerification($escort_id);
		die(json_encode(array(
		     'need_video' => $need_video,
             'need_id_verification' => $need_ver
		)));
	}
	
	public function cleanAgeFilesAction(){
		if ( $this->user->isAgency() ) {
			$escort_id = intval($this->_request->escort_id);
		}else{
			$escort_id = $this->escort->id;
		}
		$this->deleteFiles($escort_id);
		echo $escort_id;
		die;
	}
	
    /**
     * @param $catalog
     * @return array
     * @throws Zend_Exception
     */
    private function HTML5_upload($catalog) {
        $headers = array();
        $headers['Content-Length'] = $this->getRequest()->getServer('CONTENT_LENGTH');
        $headers['X-File-Id'] = $this->getRequest()->getServer('HTTP_X_FILE_ID');
        $headers['X-File-Name'] = $this->getRequest()->getServer('HTTP_X_FILE_NAME');
        $headers['X-File-Resume'] = $this->getRequest()->getServer('HTTP_X_FILE_RESUME');
        $headers['X-File-Size'] = $this->getRequest()->getServer('HTTP_X_FILE_SIZE');

        $response = array();
        $response['id'] = $headers['X-File-Id'];
        $response['name'] = basename($headers['X-File-Name']); 	// Basename for security issues
        $response['size'] = $headers['Content-Length'];
        $response['error'] = UPLOAD_ERR_OK;
        $response['finish'] = FALSE;

        // Is resume?
        $flag = (bool)$headers['X-File-Resume'] ? FILE_APPEND : 0;
        $filename = trim($catalog).'_'.trim($response['id']).'_'.trim($response['name']);
        $response['upload_name'] = $string = preg_replace('/\s+/', '', $filename);

        $config = Zend_Registry::get('images_config');
        $tempDir = !empty($config['temp_dir_path']) ? $config['temp_dir_path'] : sys_get_temp_dir();
        // Write file
        $file = $tempDir.DIRECTORY_SEPARATOR.$response['upload_name'];
        $response['tmp_name'] = $file.'.tmp';
        $chunk = file_get_contents('php://input');
        $bytes  = file_put_contents($file.'.tmp', $chunk, $flag);
        if ($bytes === false){
            $response['error'] = UPLOAD_ERR_CANT_WRITE;
        }
        else
        {
            $response['file_size'] = filesize($file);
            $response['X-File-Size'] = $headers['X-File-Size'];
            if (filesize($file.'.tmp') == $headers['X-File-Size'])
            {
                rename($response['tmp_name'], $file);
                $response['tmp_name'] = $file;
                $response['finish'] = true;
            }
        }

        return $response;
    }

    /**
     * @param $escortId
     * @throws Zend_Exception
     */
    private function deleteFiles($escortId){
        $config = Zend_Registry::get('images_config');
        $tempDir = !empty($config['temp_dir_path']) ? $config['temp_dir_path'] : sys_get_temp_dir();
        $pattern = $tempDir . DIRECTORY_SEPARATOR . $escortId . '_*';
        $files = glob($pattern);
        foreach ($files as $file){
            unlink($file);
        }
    }

    /**
     * @param $escortId
     * @return array
     * @throws Zend_Exception
     */
     private function getFileList($escortId){
        $img_config = Zend_Registry::get('images_config');
		$video_config = Zend_Registry::get('videos_config');
		
        $tempDir = !empty($img_config['temp_dir_path']) ? $img_config['temp_dir_path'] : sys_get_temp_dir();
        $pattern = $tempDir . DIRECTORY_SEPARATOR. $escortId . '_*';
        $FileList = glob($pattern);
        $Files = array();
				
        $age_base_prefix = 'age_base_img_';
        $age_eu_prefix = 'age_eu_img_';
		$id_vrf_prefix = 'id_vrf_img_';
        $age_base_video_prefix = 'age_base_vid_';
        $age_eu_video_prefix = "age_eu_vid_";

        foreach ($FileList as $k => $file){
            $pathInfo = pathinfo($file);
            if ($pathInfo['extension'] == 'tmp'){
                unlink($file);
                continue;
            }
            if (in_array(strtolower($pathInfo['extension']), $img_config['allowedExts'])) {
                if (strpos($file, $age_base_prefix) > 0) {
                    $Files['base_imgs'][] = array(
                        'tmp_name' => $file,
                        'name' => $pathInfo['basename'],
                        'size' => filesize($file),
                        'ext' => strtolower($pathInfo['extension'])
                    );
                }
                elseif (strpos($file, $age_eu_prefix) > 0) {

                    $Files['eu_imgs'][] = array(
                        'tmp_name' => $file,
                        'name' => $pathInfo['basename'],
                        'size' => filesize($file),
                        'ext' => strtolower($pathInfo['extension'])
                    );
                }
				elseif (strpos($file, $id_vrf_prefix) > 0) {

                    $Files['id_vrf_imgs'][] = array(
                        'tmp_name' => $file,
                        'name' => $pathInfo['basename'],
                        'size' => filesize($file),
                        'ext' => strtolower($pathInfo['extension'])
                    );
                }
            }
            elseif(in_array(strtolower($pathInfo['extension']), $video_config['allowedvideoExts'])) {
                if (strpos($file, $age_base_video_prefix) > 0) {
                    $Files['base_video'] = array(
                        'tmp_name' => $file,
                        'name' => $pathInfo['basename'],
                        'size' => filesize($file),
                        'ext' => strtolower($pathInfo['extension'])
                    );
                }
                elseif (strpos($file, $age_eu_video_prefix) > 0) {
                    $Files['eu_video'] = array(
                        'tmp_name' => $file,
                        'name' => $pathInfo['basename'],
                        'size' => filesize($file),
                        'ext' => strtolower($pathInfo['extension'])
                    );
                }
            }
        }

        return $Files;
    }

    /**
     * @param $data
     * @param $Files
     * @param $escort_id
     * @return array
     * @throws Zend_Exception
     */
     private function saveFiles(&$data, $Files, $escort_id) {
        $images = new Cubix_Images();
        $video_config =  Zend_Registry::get('videos_config');
        $verify_photos = array();
		
		$Files['eu_imgs'] = is_array($Files['eu_imgs']) ? $Files['eu_imgs'] : array();
		//$Files['id_vrf_imgs'] = is_array($Files['id_vrf_imgs']) ? $Files['id_vrf_imgs'] : array();
		
        foreach (array_merge($Files['base_imgs'], $Files['eu_imgs']) as $key => $image){
            if (empty($image['tmp_name']) || !is_file($image['tmp_name'])){continue;}
            try{
                $data['photos'][] = $images->save($image['tmp_name'], $escort_id . '/age-verification', $this->escort->application_id, $image['ext']);
                //$verify_photos[] = $images->save($image['tmp_name'], $escort_id . '/verify', $this->escort->application_id, $image['ext']);
            }catch (Exception $e){
                $client = Cubix_Api_XmlRpc_Client::getInstance();
                $emailBody = "<pre>
                                       {$e->getMessage()}
                                       {$e->getTraceAsString()}
                                  </pre>";
                $Attachments = array();
                $trace= $e->getTrace();
                if(!empty($trace[0]['function']) && $trace[0]['function'] == '_cubix_images_ftp_error_handler'){
                    $file = $trace[0]['args'][4]['local'];
                }elseif ($trace[0]['file'] && strpos($trace[0]['file'],'library/Cubix/Images.php')>0){
                    $file = $trace[0]['args'][0];
                }else{
                    foreach ($trace as $line){
                        if (strpos($line['file'],'library/Cubix/Images.php') > 0){
                            $file = $line['args'][0];
                            break;
                        }
                    }
                }
                if ($file && is_file($file)){
                    $item = array(
                        'item'=> file_get_contents($file),
                        'type' => mime_content_type($file),
                        'filename' => basename($file),
                        'name' => basename($file)
                    );
                    $Attachments[] = $item;
                }
                $client->call('Email.send', array("badalyano@gmail.com", '100% AC-main error', $emailBody, true, 'info@escortforumit.xxx', null, Cubix_Application::getId(), $Attachments), 100);
                unlink($image['tmp_name']);
                continue;
            }

        }
		
		$merged_videos = array($Files['base_video'], $Files['eu_video']);
		foreach($merged_videos as $ag_video){
			if($ag_video && $ag_video['tmp_name'] != '') {
				$video = $ag_video['tmp_name'];
				$video_ftp = new Cubix_VideosCommon();
				$video_hash = uniqid() . '_age_certify';
				$video_model = new Cubix_ParseVideo($video, $data['escort_id'], $video_config);
				$video = $video_model->ConvertToMP4();
				$video_ext = 'mp4';
				$name = $video_hash . '.' . $video_ext;
				if ($video_ftp->_storeToPicVideo($video, $data['escort_id'], $name)) {
					$data['photos'][] = array('hash' => $video_hash, 'ext' => $video_ext, 'is_video' => 1);
				}
			}
		}
		
        if(count($Files['id_vrf_imgs']) > 0){
			foreach ($Files['id_vrf_imgs'] as $index => $pic) {
				if (empty($pic['tmp_name']) || !is_file($pic['tmp_name'])) {
					continue;
				}

				$file = $pic['tmp_name'];
				try{
					$verify_photos[$index] = $images->save($file, $data['escort_id'] . '/verify', $this->escort->application_id, $pic['ext']);
				}catch (Exception $e){
					// something went wrong
					$client = Cubix_Api_XmlRpc_Client::getInstance();
					$emailBody = "<pre>
								   {$e->getMessage()}
								   {$e->getTraceAsString()}
							  </pre>";
					$Attachments = array();
					$trace= $e->getTrace();
					if(!empty($trace[0]['function']) && $trace[0]['function'] == '_cubix_images_ftp_error_handler'){
						$file = $trace[0]['args'][4]['local'];
					}elseif ($trace[0]['file'] && strpos($trace[0]['file'],'library/Cubix/Images.php')>0){
						$file = $trace[0]['args'][0];
					}else{
						foreach ($trace as $line){
							if (strpos($line['file'],'library/Cubix/Images.php') > 0){
								$file = $line['args'][0];
								break;
							}
						}
					}
					if ($file && is_file($file)){
						$item = array(
							'item'=> file_get_contents($file),
							'type' => mime_content_type($file),
							'filename' => basename($file),
							'name' => basename($file)
						);
						$Attachments[] = $item;
					}
					$client->call('Email.send', array("badalyano@gmail.com", '100% AC-id-card error', $emailBody, true, 'info@escortforumit.xxx', null, Cubix_Application::getId(), $Attachments), 100);
					unlink($image['tmp_name']);
					continue;
				}
			}
		}
		/*if (!empty($Files['natural-video']) &&  is_file($Files['natural-video']["tmp_name"])) {
			$video_ftp = new Cubix_VideosCommon();
			$video_hash = uniqid() . '_idcard';
			$video = $Files['natural-video']['tmp_name'];
			$video_model = new Cubix_ParseVideo($video, $data['escort_id'], $video_config);
			$video = $video_model->ConvertToMP4();
			$video_ext = 'mp4';
			$name = $video_hash . '.' . $video_ext;
			if ($video_ftp->_storeToPicVideo($video, $data['escort_id'], $name)) {
				$data['natural-video'] = array('hash' => $video_hash, 'ext' => $video_ext, 'is_video' => 1);
			}
		}*/
        return $verify_photos;
    }

    /**
     * @throws Zend_Exception
     */
    public function escortAgeVerificationAjaxAction()
    {
		
        if ( $this->_request->isPost() ) {
            $this->view->layout()->disableLayout();
            $form = new Cubix_Form_Data($this->_request);
            $fields = array(
                'name' => 'notags|special',
                'last_name' => 'notags|special',
                'day' => 'int',
                'month' => 'int',
                'year' => 'int',
                
            );
            $form->setFields($fields);
            $data = $form->getData();

            if($this->user->isAgency()){
                $client = Cubix_Api_XmlRpc_Client::getInstance();
                $agency = $this->user->getAgency();
                $this->view->needVerEscorts = $client->call('Agencies.getNeedVerEscorts', array($agency->id));
                $data['escort_id'] = $this->view->sel_escort_id = intval($this->_request->escort_id);
                $need_video = $this->view->need_video = $this->client->call('getNeedVideoVerify', array($data['escort_id']));
            }
            else{
                $escort_data = $this->user->getEscort();
                $need_video = $escort_data->need_age_verify_video;
                $data['escort_id'] = $escort_data->id;
                $this->view->need_age_verification = $escort_data->need_age_verification;
                $this->view->need_video = $escort_data->need_age_verify_video;
            }

            $Files = $this->getFileList($data['escort_id']);
			
            $this->view->name = $data['name'];
            $this->view->last_name = $data['last_name'];
            $this->view->day = $data['day'];
            $this->view->month = $data['month'];
            $this->view->year = $data['year'];
            $this->view->data = $data;
            $validator = new Cubix_Validator();
            try {
                $this->_validateAgeVerification($data, $Files, $need_video);

                $data['name'] = preg_replace('!\s+!',' ', $data['name']);
                $data['last_name'] = preg_replace('!\s+!',' ', $data['last_name']);
                $data['name'] = strtoupper(trim($data['name']));
                $data['last_name'] = strtoupper(trim($data['last_name']));
            }
            catch (Exception $e) {
                switch ( $e->getCode() ) {
                    case Cubix_Images::ERROR_IMAGE_INVALID:
                        $error = $e->getMessage();
                        break;
                    case Cubix_Images::ERROR_IMAGE_TOO_BIG:
                        $error = $e->getMessage();
                        break;
                    case Cubix_Images::ERROR_IMAGE_TOO_SMALL:
                        $error = $e->getMessage();
                        break;
                    case Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED:
                        $error = $e->getMessage();
                        break;
                    case Cubix_Images::ERROR_IMAGE_COUNT_LIMIT_OVER:
                        $error = $e->getMessage();
                        break;
                    default:
                        $error = 'An unexpected error occured when uploading file'.$e->getMessage();
                }

                $validator->setError('error' , $error);

            }

            if($validator->isValid()){
                $this->view->success = true;
                if($this->user->isAgency()){
                    $client = Cubix_Api_XmlRpc_Client::getInstance();
                    $agency = $this->user->getAgency();
                    $need_ag_verification = $client->call('Agencies.getNeedVerEscorts', array($agency->id));

                }
                $this->addPendingVerificationEscortId($data['escort_id']);
                $html = $this->view->render("private-v2/certification-success.phtml");
                ignore_user_abort(true);
                ob_start();
                echo json_encode(array('html'=> $html, 'error'=>''));
                ob_end_flush();
                ob_flush();
                flush();
                session_write_close();
                fastcgi_finish_request();

                $photos = $this->saveFiles($data, $Files, $data['escort_id']);
				if (count($Files['id_vrf_imgs']) > 0) {
                    $item = new Model_VerifyRequest(array(
                        'type' => Model_VerifyRequests::TYPE_IDCARD,
                        'escort_id' => $data['escort_id']
                    ));

                    $save_data = array(
                        'type' => Model_VerifyRequests::TYPE_IDCARD,
                        'escort_id' => $data['escort_id']
                    );

                    $verifyRequest = new Model_VerifyRequests();

                    $item = $verifyRequest->save($save_data);

                    foreach ($photos as $photo) {
                        $item->addPhoto(array(
                            'hash' => $photo['hash'],
                            'ext' => $photo['ext']
                        ));
                    }

                    /*if ($data['natural-video'] && is_array($data['natural-video'])) {
                        $item->addPhoto($data['natural-video']);
                    }*/

                   $data['idcard_id'] = $item->id;
                }

                $model = new Model_AgeVerification();
                $model->save($data);
                $cache = Zend_Registry::get('cache');
                $cache_key =  'v2_user_pva_' . $this->user->id;
                $cache->remove($cache_key);
            }
            else{
                $error_info = $validator->getStatus();
                $error_info['msgs'];
            }
            $this->deleteFiles($data['escort_id']);
            $html = $this->view->render("private-v2/escort-age-verification-ajax.phtml");
            
            echo json_encode(array('html'=>$html, 'error'=> $error_info['msgs']['error']));
        }
        die;
    }

    /**
     * @param $images
     * @return int
     */
    private function getCountImages($images){
        $count = 0;

        foreach ($images as $image ){
            if(is_file($image['tmp_name']) > 0) $count++;
        }

        return $count;
    }

    private function getSession() {
        $session = new Zend_Session_Namespace('pending_age_verifications');
        $session->setExpirationSeconds(15*60);
        return $session;
    }

    private function getPendingVerificationEscortIds() {
        return $this->getSession()->ids;
    }

    private function setPendingVerificationEscortIds($ids) {
        if (is_numeric($ids)) {
            $this->getSession()->ids = array(intval($ids));
        }
        elseif (is_array($ids)) {
            $this->getSession()->ids = $ids;
        }
        else {
            $this->getSession()->ids = null;
        }
    }

    private function addPendingVerificationEscortId($id){
        $PendingVerificationEscortIds =  $this->getPendingVerificationEscortIds();
        if (!is_array($PendingVerificationEscortIds)){
            $PendingVerificationEscortIds = array();
        }
        array_push($PendingVerificationEscortIds, $id);
        $this->setPendingVerificationEscortIds($PendingVerificationEscortIds);
    }

    /*public function setPopupShowedAction()
    {
        $client = Cubix_Api_XmlRpc_Client::getInstance();

        try {
            $client->call('EasterLottery.setPopupShowed', array($this->user->id));
        }catch (Exception $e)
        {
            die($e);
        }
    }*/


    public function setXmasPopupShowedAction()
    {
        $client = Cubix_Api_XmlRpc_Client::getInstance();

        try {
            $client->call('Xmas.setPopupShowed', array($this->user->id));
            die;
        }catch (Exception $e)
        {
            die($e);
        }
    }

    private function _getDiamondEscort()
    {
        return Cubix_Api::getInstance()->call('premium_getDiamondEscort', array($this->escort->id, $this->view->lang()));
    }

    public function changePremiumCitiesV2Action()
    {
        $this->view->layout()->disableLayout();
        $req = $this->_request;
        $escort_id = intval($req->escort_id);

        if ( ! $escort_id ) return;

        $active_package = Cubix_Api::getInstance()->call('premium_getEscortActivePackage', array($escort_id, 'en'));
        $is_zona_rossa_package = in_array( $active_package["package_id"] , $this->zona_rossa_packages ) ? true : false;

        $spot_cities = array();
        if ( count($active_package['cities']) > 0 ) {
            foreach ($active_package['cities'] as $city) {
                $spot_cities[] = $city['id'];
            }
        }
        $this->view->spot_cities = $spot_cities;
        $this->view->is_zona_rossa_package = $is_zona_rossa_package;

        if($is_zona_rossa_package){
            $city_model = new Model_Cities();
            $zona_rossa_cities = explode(',', $city_model->getZonaRossaIds());
            foreach($active_package['all_cities'] as $key => $city){
                if(!in_array($city['id'], $zona_rossa_cities)){
                    unset($active_package['all_cities'][$key]);
                }
            }
            $this->view->all_cities = $active_package['all_cities'];
        }
        else{
            $this->view->all_cities = $active_package['all_cities'];
        }
        $active_spot_products = array();
        if ( count($active_package['products']) > 0 ) {
            foreach ($active_package['products'] as $product) {
                $active_spot_products[] = $product['id'];
            }
        }

        if ( ! $req->isPost() )
        {
            $model = new Model_EscortsV2();
            $this->view->escort = $escort = $model->getById($escort_id);

            $city_spot_products = array(5,9,11,12,13);

            $has_city_spot_prod = false;
            foreach ( $city_spot_products as $p ) {
                if ( in_array($p, $active_spot_products) ) {
                    $has_city_spot_prod = true;
                }
            }

            $this->view->has_city_spot_prod = $has_city_spot_prod;
        }
        else {
            $cities = $req->cities;

            $validator = new Cubix_Validator();

            /*if ( ! count($spot_cities) ) {
                return;
            }*/

            $cities_limit = $this->view->cities_limit = Cubix_Api::getInstance()->call('premium_getEscortCitiesLimit2', array($escort_id));

            if ( ! count($cities) ) {
                $validator->setError('error', 'Please select at least one city!');
            }

            if ( count($cities) > $cities_limit ) {
                ($cities_limit > 1) ? $city_name = 'cities' : $city_name = 'city';
                $validator->setError('error', "You can check maximum {$cities_limit} {$city_name}!");
            }

            if ( $validator->isValid() ) {
                Cubix_Api::getInstance()->call('premium_updatePremiumCities', array($active_package['opid'], $cities, $escort_id));
            }

            die(json_encode($validator->getStatus()));
        }
    }
}
