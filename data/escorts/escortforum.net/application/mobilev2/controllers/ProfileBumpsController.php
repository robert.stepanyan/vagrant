<?php

class MobileV2_ProfileBumpsController extends Zend_Controller_Action
{
    public static $linkHelper;

    protected $_session;

    public function init()
    {
        self::$linkHelper = $this->view->getHelper('GetLink');
        $this->view->layout()->setLayout('mobile-private');
        $this->view->user = $this->user = Model_Users::getCurrent();

        if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
            $this->_redirect($this->view->getLink('signin'));
            return;
        }

        if ( ! in_array($this->_request->getActionName(), $anonym) && ! $this->user->isAgency() && ! $this->user->isEscort() ) {
            $this->_redirect($this->view->getLink('private-v2'));
        }

        $this->view->headTitle('Private Area', 'PREPEND');
        $this->client = new Cubix_Api_XmlRpc_Client();
        $this->defs = Zend_Registry::get('definitions');
        $this->cache = Zend_Registry::get('cache');
    }

    public function indexAction()
    {

        if ( $this->user->isAgency() ) {
            $this->_redirect(self::$linkHelper->getLink('ob-profile-bump-buy-agency'));
        } elseif($this->user->isEscort()) {
            $this->_redirect(self::$linkHelper->getLink('ob-profile-bump-buy'));
        }else{
            die();
        }
    }

    public function buyBumpsAction(){

        if ( $this->user->isAgency() ) {$this->_redirect(self::$linkHelper->getLink('ob-profile-bump-buy-agency'));}

        $escort_id = $this->user->escort_data['escort_id'];
        $m_escorts = new Model_EscortsV2();
        $m_bumps = new Model_Bumps();
        $cities_model = new Cubix_Geography_Cities();

        $escort = $m_escorts->get($escort_id);

        $this->view->active_package = $active_package = $this->client->call('OnlineBillingV2.getActivePackageForProfileBumps', array($escort_id));

        $expiration_date =  date_create($active_package['expiration_date']);

        $current_date =  date_create();
        if($expiration_date < $current_date){echo 'unexpected date problem'; die;}

        $interval = date_diff($expiration_date, $current_date);

        $minutes_left_to_package_end = $interval->days * 24 * 60;
        $minutes_left_to_package_end += $interval->h * 60;

        if( $minutes_left_to_package_end > 12000 ){
            $optimal_bump_count = 50;
        }
        elseif( $minutes_left_to_package_end > 2400 ){
            $optimal_bump_count = 10;
        }else{
            $optimal_bump_count = 1;
        }

        $active_bumps = $m_bumps->get_active_bumps( $escort_id );
        $current_bumps_cities = array();
        foreach ($active_bumps as $ongoing_bump) {
            $current_bumps_cities[] = $ongoing_bump->city_id;
        }

        $this->view->optimal_bump_count = $optimal_bump_count ;
        $this->view->minutes_left_to_package_end = $minutes_left_to_package_end;
        $this->view->cities = $escort_avaiable_cities = $cities_model->getCitiesByEscortID( $escort_id );
        $this->view->active_package =  $active_package;
        $this->view->bump_hours = $this->defs['BUMP_AVAILABLE_HOURS'];
        $this->view->bump_prices = $bump_price = $m_bumps->get_bump_prices();
        $this->view->current_bumps_cities = $current_bumps_cities;
        $this->view->escort = $escort;

        $escort_avaiable_cities_ids = array();
        foreach ($escort_avaiable_cities as $avaiable_city) {
            $escort_avaiable_cities_ids[] = $avaiable_city->id;
        }

        if( empty( array_diff( $escort_avaiable_cities_ids, $current_bumps_cities ) ) ){
            $this->view->bump_warning = true;
        }else{
            $this->view->bump_warning =  false;
        }

    }

    public function buyBumpsAgencyAction(){
        if($this->user->isEscort()) {$this->_redirect(self::$linkHelper->getLink('ob-profile-bump-buy'));}
        $escort_id = $this->user->escort_data['escort_id'];

        $cities_model = new Cubix_Geography_Cities();
        $bumps_model = new Model_Bumps();
        $cache_key =  'v2_user_pva_' . $this->user->id;

        if ( $this->user->isAgency() ) {

            if ( ! $agency = $this->cache->load($cache_key) ) {
                $agency = $this->user->getAgency();
                $this->cache->save($agency, $cache_key, array(), 300);
            }

            $this->agency = $this->view->agency = $agency;
        }

        $this->view->escorts = $this->agency->getEscortsWithPackages();
        $default_escort_to_show = $this->view->escorts[0];

        $this->view->active_package = $active_package = $this->client->call('OnlineBillingV2.getActivePackageForProfileBumps', array( $default_escort_to_show['id'] ));

        $expiration_date =  date_create($active_package['expiration_date']);

        $current_date =  date_create();

        if($expiration_date < $current_date){echo 'unexpected date problem'; die;}

        $interval = date_diff($expiration_date, $current_date);

        $minutes_left_to_package_end = $interval->days * 24 * 60;
        $minutes_left_to_package_end += $interval->h * 60;

        if($minutes_left_to_package_end > 12000){
            $optimal_bump_count = 50;
        }elseif($minutes_left_to_package_end > 2400){
            $optimal_bump_count = 10;
        }else{
            $optimal_bump_count = 1;
        }

        $active_bumps = $bumps_model->get_active_bumps( $default_escort_to_show['id'] );
        $current_bumps_cities = array();
        foreach ($active_bumps as $ongoing_bump) {
            $current_bumps_cities[] = $ongoing_bump->city_id;
        }


        $this->view->minutes_left_to_package_end = $minutes_left_to_package_end;
        $this->view->optimal_bump_count = $optimal_bump_count ;
        $this->view->active_package_days_left = $interval->d;
        $this->view->active_package =  $active_package;

        $this->view->cities = $escort_avaiable_cities = $cities_model->getCitiesByEscortID( $default_escort_to_show['id'] );
        $this->view->bump_hours = $this->defs['BUMP_AVAILABLE_HOURS'];
        $this->view->current_bumps_cities = $current_bumps_cities;
        $this->view->bump_prices = $bump_price = $bumps_model->get_bump_prices();

        $escort_avaiable_cities_ids = array();
        foreach ($escort_avaiable_cities as $avaiable_city) {
            $escort_avaiable_cities_ids[] = $avaiable_city->id;
        }

        if( empty( array_diff( $escort_avaiable_cities_ids, $current_bumps_cities ) ) ){
            $this->view->bump_warning = true;
        }else{
            $this->view->bump_warning =  false;
        }


    }

    public function checkoutAction(){
        //agencies and escorts
        if ( $this->_request->isPost() ) {

            $errors = array();
            $validate_cities = array();

            if ( $this->user->isAgency() ) {
                $agency = $this->user->getAgency();
                $escort_id = $this->_request->escort_id;

                if ( ! $agency->hasEscort($escort_id) ) {
                    die('Permission denied!');
                }
            } elseif($this->user->isEscort()) {
                $escort_id = $this->user->escort_data['escort_id'];
            }else{
                die();
            }


            $cities_model = new Cubix_Geography_Cities();
            $escort_avaiable_cities = $cities_model->getCitiesByEscortID( $escort_id );

            $bumps_model = new Model_Bumps();
            $bump_price = $bumps_model->get_bump_prices();


            foreach ($escort_avaiable_cities as $key => $city) {
                $validate_cities[] = $city->id;

            }


            if ( ! in_array($this->_request->city_id, $validate_cities ) ) {
                $errors['city_error'] = true;
            }

            if ( ! array_key_exists($this->_request->frequency, $this->defs['BUMP_AVAILABLE_HOURS']) ) {
                $errors['frequency_error'] = true;
            }

            if ( ! in_array($this->_request->bumps_count, array(1, 10, 50) ) ) {
                $errors['bumps_count_error'] = true;
            }

            if ( ! in_array($this->_request->payment_gateway, array('twispay', 'paysafe', 'powercash') ) ) {
                $errors['bumps_gateway_error'] = true;
            }


            if(empty($errors)){

                foreach($bump_price as $price_item){
                    if($price_item->bumps_count == $this->_request->bumps_count){
                        $amount = $price_item->price;
                    }
                }

                $book_id = $this->client->call('OnlineBillingV2.bookBump', array($escort_id, $this->_request->city_id, $amount, $this->_request->bumps_count, $this->_request->frequency, Model_Users::getCurrent()->clientID,Cubix_Geoip::getIP()));

                if( $this->_request->payment_gateway == 'twispay' ){
                    $paymentConfigs = Zend_Registry::get('payment_config');
                    $twispayConfigs = $paymentConfigs['twispay'];

                    /*if (in_array($this->user->id, [345573])) {$amount = 1;}*/

                    $reference = 'pbump-' . $book_id;
                    $data = array(
                        'siteId' => intval($twispayConfigs['siteid']),
                        'cardTransactionMode' => 'authAndCapture',
                        'backUrl' => APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/online-billing-v2/twispay-bump-response?p=no_splash',
                        'invoiceEmail' => '',
                        'customer' => [
                            'identifier' => 'user-' . $this->user->id,
                            'firstName' => $this->user->username,
                            'username' => $this->user->username,
                            'email' => $this->user->email,
                        ],
                        'order' => [
                            'orderId' => $reference,
                            'type' => 'purchase',
                            'amount' => $amount,
                            'currency' => $twispayConfigs['currency'],
                            'description' => 'Escort Profile BUMP desktop EF',
                        ]
                    );

                    $base64JsonRequest = Cubix_Twispay_TwispayApi::getBase64JsonRequest($data);
                    $base64Checksum = Cubix_Twispay_TwispayApi::getBase64Checksum($data, $twispayConfigs['key']);

                    $token = json_encode(['externalOrderId' => $data['order']['orderId']]);
                    $isOrderCreated = $this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id, 'twispay'));

                    if (!$isOrderCreated) {
                        die(json_encode([
                            'status' => 'error',
                            'error'  => 'Twispay is not available at the moment',
                        ]));
                    }

                    try {
                        $paymentFrom = "
                        <form id=\"payment-form\" action='{$twispayConfigs['url']}' method='post' accept-charset='UTF-8'>
                            <input type='hidden' name='jsonRequest' value = \"{$base64JsonRequest}\" >
                            <input type='hidden' name='checksum' value = \"{$base64Checksum}\" >
                            <input type = \"submit\" value = \"Pay\" >
                        </form > ";

                    } catch(Exception $e) {
                        $message = $e->getMessage();
                        file_put_contents('/tmp/twispay_error.log', "\n" . 'UserId: ' . $this->user->id . "\n" . var_export($message, true), FILE_APPEND);

                        die(json_encode(array('status' => 'success', 'url' => APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-unsuccessful'))));
                    }
                    die(json_encode(array('status' => 'success', 'form' => $paymentFrom), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES ));

                }elseif( $this->_request->payment_gateway == 'paysafe' ){

                    $first_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->username));
                    $last_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->chat_info['nickName']));

                    if( empty($first_name)){
                        $first_name = 'NAME';
                    }

                    if(empty($last_name)){
                        $last_name = 'LASTNAME';
                    }

                    $reference = 'pbump-' . $book_id;

                    $customer = new Cubix_2000charge_Model_Customer();
                    $customer->setEmail($this->user->email);
                    $customer->setCountry("IT");
                    $customer->setFirstName($first_name);
                    $customer->setLastName($last_name);

                    $payment = new Cubix_2000charge_Model_Payment();
                    $payment->setPaymentOption("paysafe");
                    $payment->setHolder($first_name.' '.$last_name);

                    $transaction = new Cubix_2000charge_Model_Transaction();
                    $transaction->setCustomer($customer);
                    $transaction->setPayment($payment);
                    $transaction->setAmount($amount * 100);
                    $transaction->setCurrency("EUR");
                    $transaction->setIPAddress(Cubix_Geoip::getIP());
                    $transaction->setMerchantTransactionId($reference);

                    $host = 'https://' . $_SERVER['SERVER_NAME'];
                    $redirectUrls = new Cubix_2000charge_Model_RedirectUrls();
                    $redirectUrls->setReturnUrl($host . $this->view->getLink('ob-successful'));
                    $redirectUrls->setCancelUrl($host .$this->view->getLink('ob-unsuccessful'));
                    $transaction->setRedirectUrls($redirectUrls);

                    $res = Cubix_2000charge_Transaction::post($transaction);
                    $this->client->call('OnlineBillingV2.storeToken', array($res->id, $this->user->id, '2000charge'));
                    $this->_redirect($res->redirectUrl);
                }
				elseif($this->_request->payment_gateway == 'powercash')
				{
					$reference = 'pbump-' . $book_id;
					$params = array(
						'amount' => $amount,
						'orderid' => $reference,
						'email' => $this->user->email,
						'customerip' => Cubix_Geoip::getIP(),
						'url_return' =>  $this->view->getLink('paycash-response')
					);

					$model_powercache = new Model_PowerCashGateway($params);
					$response = $model_powercache->checkout();

					$this->client->call('OnlineBillingV2.storeStatus', array($response['transactionid'], $this->user->id, serialize($model_powercache->returnStoreData())));
					if($response['errorcode'] === '0'){
						$this->_redirect($response['start_url']);
					}
					else{
						$this->_redirect($this->view->getLink('ob-unsuccessful-v2'));
					}
				}
            }
            die(json_encode( array( 'status'=> 'error', 'errors' => $errors) ));
        }

    }

    public function escortDataAction(){
        if ( !$this->_request->isPost() ) { die;}

        $agency = $this->user->getAgency();
        $escort_id = $this->_request->escort_id;


        if ( ! $agency->hasEscort($escort_id) ) {
            die('Permission denied!');
        }

        $cities_model = new Cubix_Geography_Cities();
        $bumps_model = new Model_Bumps();

        $cities = $escort_avaiable_cities = $cities_model->getCitiesByEscortID( $escort_id );
        $escort_active_package = $this->client->call('OnlineBillingV2.getActivePackageForProfileBumps', array($escort_id));

        $active_bumps = $bumps_model->get_active_bumps( $escort_id );
        $current_bumps_cities = array();
        foreach ($active_bumps as $ongoing_bump) {
            $current_bumps_cities[] = $ongoing_bump->city_id;
        }

        foreach ( $cities as $key => $city ) {
            if(in_array($city->id, $current_bumps_cities)){
                $cities[$key]->disabled = 'disabled';
                $cities[$key]->is_active = ' ('. __('active').')';
            }else{
                $cities[$key]->disabled = '';
                $cities[$key]->is_active = '';
            }
        }

        //for testing
        //$expiration_date =  date_create('2020-07-10');

        $expiration_date =  date_create($escort_active_package['expiration_date']);

        $current_date =  date_create();
        $interval = date_diff($expiration_date, $current_date);

        $minutes_left_to_package_end = $interval->days * 24 * 60;
        $minutes_left_to_package_end += $interval->h * 60;

        $escort_avaiable_cities_ids = array();
        foreach ($escort_avaiable_cities as $avaiable_city) {
            $escort_avaiable_cities_ids[] = $avaiable_city->id;
        }
        if( empty( array_diff( $escort_avaiable_cities_ids, $current_bumps_cities ) ) ){
            $bump_warning = __('bump_warning');
        }else{
            $bump_warning = false;
        }

        if(empty($cities)){
            echo json_encode(array('status'=> 'error'));
            die;
        }else{
            echo json_encode(array('status'=> 'success', 'cities'=> $cities, 'minutes_left_to_package_end' => $minutes_left_to_package_end, 'active_package' => $escort_active_package, 'bump_warning' => $bump_warning));
            die;
        }
    }
}
