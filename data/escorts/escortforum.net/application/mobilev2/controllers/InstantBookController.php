<?php

class Mobilev2_InstantBookController extends Zend_Controller_Action {

    private $user;

    public function init(){
        $this->_helper->layout->setLayout('mobile-index');
        $this->user = Model_Users::getCurrent();

        if (!$this->user) {
            $this->_redirect($this->view->getLink('signin'));
            return;
        }

    }



    


    public function indexAction(){}

    public function setAvailableAction(){
        if(!$this->user->isEscort()) return $this->_redirect($this->view->getLink('private-v2'));
    }

    public function bookingSuccessAction(){}

    public function notAnsweredAction(){}

    public function acceptedRequestAction(){}

    public function editAction(){}

    public function receiveRequestAction(){}

    public function receiveBookingAction(){}

}
