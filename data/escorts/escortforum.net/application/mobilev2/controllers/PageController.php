<?php

class Mobilev2_PageController extends Zend_Controller_Action
{
	public function init()
	{

        $_SESSION['request_url'] = $_SERVER['REQUEST_URI'];

		$this->_helper->layout->setLayout('mobile-index');
		
		
	}
	
	public function showAction()
	{
		$slug = $this->_request->page_slug;
		$lang = Cubix_I18n::getLang();
		$app_id = Cubix_Application::getId();
		
		$model = new Model_StaticPage();
		$this->view->page_content = $model->getBySlug($slug, $app_id, $lang);
		
	}
}
