<?php

class Mobilev2_PrivateController extends Zend_Controller_Action
{
    public static $linkHelper;

    /**
     * @var Zend_Session_Namespace
     */
    protected $_session;

    public function init()
    {

        //die('Private area will be available in 20 minutes!');

        $this->_request->setParam('no_tidy', true);
        $this->view->layout()->setLayout('mobile-private');

        $anonym = array('membership-type', 'signup', 'signin', 'auto-signin', 'signin-comment', 'forgot', 'activate', 'check', 'sign-in-up', 'choose-vip-package','change-pass', 'send-activation-code', 'check-activation-code', 'force-change-pass','password-change-required');
        $this->blacklisted_usernames = array( 'admin' , 'moderator', 'webmaster' ,'manager', 'sales' );
        $this->user = Model_Users::getCurrent();

        if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
            $this->_redirect($this->view->getLink('signin'));
            return;
            die;
        }

        $this->view->headTitle('Private Area', 'PREPEND');

        $this->_session = new Zend_Session_Namespace('private');



    }

    public function indexAction()
    {
        if ( $this->user->isAgency() || $this->user->isEscort() || $this->user->isMember() ) {
            $this->_redirect($this->view->getLink('private-v2'));
        }
    }

    public function membershipTypeAction()
    {

    }

    public function signupAction()
    {
        $type = $this->_getParam('type');
        if ( $type ){
            $this->_helper->viewRenderer->setScriptAction('signup-' . $type);
        }

        $this->view->type = $type;
        $signup_i18n = $this->view->signup_i18n = (object) array(
            'username_invalid' => 'Username must be at least 6 characters long',
            'username_invalid_characters' => 'Only "a-z", "0-9", "-" and "_" are allowed',
            'username_exists' => 'Username already exists, please choose another',
            'password_invalid' => 'Password must contain at least 6 characters',
            'password2_invalid' => 'Passwords must be equal',
            'email_invalid' => 'Email is invalid, please provide valid email address',
            'email_exists' => 'Email already exists, please enter another',
            'terms_required' => 'You must agree with terms and conditions',
            'form_errors' => 'Please check errors in form',
            'terms_required' => 'You have to accept terms and conditions before continue',
            'domain_blacklisted' => 'Domain is blacklisted',
            'user_type' => 'Choose your business',
            'sales_requered' => __('sales_requered'),
            'username_equal_password' => __('username_equal_password'),
            'mob_equal_emails' => __('username_equal_password')
        );

        try{
            $client = new Cubix_Api_XmlRpc_Client();
            $sales_persons = $client->call('Users.getSalesPersons');

            //array_unshift($sales_persons, array('id' => 25, 'first_name' => 'Marco'));

            foreach($sales_persons as $key => $sales){
                if($sales['id'] == 100 /* || $sales['id'] == 105 */ ){
                    unset($sales_persons[$key]); // 100 - ef_office , 105 alessandro
                }
            }

            shuffle($sales_persons);
            $this->view->sales_persons = $sales_persons;

            if ( $this->_request->isPost() ) {
                if ($this->_getParam('phone')){
                    die;
                }
                $user_type = $this->_getParam('user_type', $type);
                $user_t = $this->_getParam('user_type');

                $validator = new Cubix_Validator();

                if ( ! in_array($user_type, array('member', 'vip-member', 'escort', 'agency')) ) {
                    die;
                }

               // if ($user_type == 'escort' &&  ! in_array($user_t, array('member', 'vip-member', 'escort', 'agency')) ) {
                    /* Update Grigor */
                   // $validator->setError('user_type', $signup_i18n->user_type);
                    /* Update Grigor */

               // }

                $data = new Cubix_Form_Data($this->_request);
                $fields = array(
                    'sales_person'=> '',
                    'username' => '',
                    'password' => '',
                    'password2' => '',
                    'email' => '',
                    'rem_email' => '',
                    'terms' => 'int',
                    //				'captcha' => ''
                    //'recaptcha_response_field' => ''
                );
                $data->setFields($fields);
                $data = $data->getData();

                $data['username'] = substr($data['username'], 0, 24);
                $data['user_type'] = $user_t;
                $this->view->data = $data;


                $has_bl_username = false;
                foreach($this->blacklisted_usernames as $bl_username){
                    if( strpos( $data['username'], $bl_username) !== false){
                        $has_bl_username = true;
                        BREAK;
                    }
                }

                $client = new Cubix_Api_XmlRpc_Client();
                $model = new Model_Users();


                if ( strtolower($data['password']) == strtolower($data['username'])) {
                    $validator->setError('password', $signup_i18n->username_equal_password);
                }

                if ($user_type == 'member') {
                    $data['sales_person'] = 1;
                }

                if ($user_t != 'member') {
                    if ( $data['sales_person'] == '-5' ) {
                        $validator->setError('sales', $signup_i18n->sales_requered);
                        //$data['sales_person'] = 100; // ef_office
                    }
                }

                if ( strlen($data['username']) < 6 ) {
                    $validator->setError('username', $signup_i18n->username_invalid);
                }
                elseif ( ! preg_match('/^[-_a-z0-9]+$/i', $data['username']) ) {
                    $validator->setError('username', $signup_i18n->username_invalid_characters);
                }
                //elseif (stripos($data['username'], 'admin') !== FALSE) {
                elseif ($has_bl_username) {
                    $validator->setError('username', $signup_i18n->username_exists);
                }
                elseif ( $client->call('Users.getByUsername', array($data['username'])) ) {
                    $validator->setError('username', $signup_i18n->username_exists);
                }

                /*if ( strlen($data['promotion_code']) ) {
                    if ( ! in_array($data['promotion_code'], array('123456', '9988')) ) {
                        $validator->setError('promotion_code', $signup_i18n->promotion_code);
                    }
                }*/


                if ( strlen($data['password']) < 6 ) {
                    $validator->setError('password', $signup_i18n->password_invalid);
                }
                elseif ( is_null($this->_getParam('ajax')) && $data['password'] != $data['password2'] ) {
                    $validator->setError('password2', $signup_i18n->password2_invalid);
                }

                if ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
                    $validator->setError('email', $signup_i18n->email_invalid);
                }
                elseif( $client->call('Application.isDomainBlacklisted', array($data['email'])) )
                {
                    $validator->setError('email', $signup_i18n->domain_blacklisted);
                }
                elseif ( $client->call('Users.getByEmail', array($data['email'])) ) {
                    $validator->setError('email', $signup_i18n->email_exists);
                }

                if ($user_type != 'member') {
                    if( $data['rem_email'] !== $data['email'] ){
                        $validator->setError('rem_email', __('mob_equal_emails'));
                    }
                }

                if ( ! $data['terms'] ) {
                    $validator->setError('terms', $signup_i18n->terms_required);
                }

                if ( is_null($this->_getParam('ajax')) ) {
                    /*$captcha = Cubix_Captcha::verify($this->_request->recaptcha_response_field);

                    $captcha_errors = array(
                        'invalid-site-public-key' => 'We weren\'t able to verify the public key.',
                        'invalid-site-private-key' => 'We weren\'t able to verify the private key.',
                        'invalid-request-cookie' => 'The challenge parameter of the verify script was incorrect.',
                        'incorrect-captcha-sol' => 'The CAPTCHA solution was incorrect.',
                        'verify-params-incorrect' => 'The parameters to /verify were incorrect, make sure you are passing all the required parameters.',
                        'invalid-referrer' => 'reCAPTCHA API keys are tied to a specific domain name for security reasons.',
                        'recaptcha-not-reachable' => 'reCAPTCHA never returns this error code. A plugin should manually return this code in the unlikely event that it is unable to contact the reCAPTCHA verify server.'
                    );



                    if (!is_bool($captcha) ) {
                        $validator->setError('captcha', $captcha_errors[$captcha]);
                    }*/
                    //				if ( ! strlen($data['captcha']) ) {
                    //				$validator->setError('captcha', 'Captcha is required');
                    //				}
                    //				else {
                    //					$session = new Zend_Session_Namespace('captcha');
                    //					$orig_captcha = $session->captcha;
                    //
                    //					if ( strtolower($data['captcha']) != $orig_captcha ) {
                    //						$validator->setError('captcha', 'Captcha is invalid');
                    //					}
                    //				}
                }



                $this->view->errors = array();
                $result = $validator->getStatus();

                if ( ! $validator->isValid() ) {
                    $this->view->errors = $result['msgs'];
                    if ( ! is_null($this->_getParam('ajax')) ) {
                        echo(json_encode($result));
                        ob_flush();
                        die;
                    }
                }
                else {
                    // will set the most free sales user id


                    //Marco 25 goes to 105 Alessandro
                    if ( $data['sales_person'] == '25' ) {
                        $data['sales_person'] = 105;
                    }
                    //109 Laura goes to Angelo 16
                    /*elseif ( $data['sales_person'] == '109' ) {
                        $data['sales_person'] = 16;
                    }*/

                    // Users model
                    $user = new Model_UserItem(array(
                        'username' => $data['username'],
                        'email' => $data['email'],
                        'user_type' => $user_type == 'vip-member' ? 'member' : $user_type,
                        'sales_user_id' => $data['sales_person']
                    ));

                    $bl_model = new Model_BlacklistedWords();
                    $is_bl_email = $bl_model->checkMemberEmail($data['email']);

                    if ( isset($is_bl_email['error']) ) {
                        throw new Exception('API is not available. Please try later.');
                        exit;
                    }
                    // $dont_save = ($is_bl_email && 'member' == $user_type ) ? true : false;

                    $dont_save = ($is_bl_email ) ? true : false; // add for escort and agencies too

                    $salt_hash = Cubix_Salt::generateSalt($data['email']);
                    $user->salt_hash = $salt_hash;
                    $user->password = Cubix_Salt::hashPassword($data['password'], $salt_hash);
                    if(!$dont_save){

                        $new_user = $model->save($user);
                        if ( isset($new_user['error']) ) {
                            throw new Exception('API is not available. Please try later.');
                            exit;
                        }

                        /*USE FORUM*/
                        /*$forum2 = new Cubix_Forum2Api();

                        $forum2->RegisterUser($new_user->username, $new_user->email);*/
                        /**/
                    }

                    if ( 'escort' == $user_type ) {
                        $m_escorts = new Model_Escorts();

                        $escort = new Model_EscortItem(array(
                            'user_id' => $user->new_user_id
                        ));

                        // will set current app country id
                        Model_Hooks::preEscortSignUp($escort);

                        $escort = $m_escorts->save($escort);

                        // will update escort status bits using api
                        Model_Hooks::postEscortSignUp($escort);
                    }
                    elseif ( 'agency' == $user_type ) {
                        $m_agencies = new Model_Agencies();

                        $agency = new Model_AgencyItem(array(
                            'user_id' => $user->new_user_id,
                            'name' => ucfirst($user->username)
                        ));

                        // will set current app country id
                        Model_Hooks::preAgencySignUp($agency);

                        $agency = $m_agencies->save($agency);

                        //
                        Model_Hooks::postAgencySignUp($agency);
                    }
                    elseif ( 'member' == $user_type || 'vip-member' == $user_type ) {
                        unset($this->_session->want_premium);
                        if(!$dont_save){
                            $m_members = new Model_Members;

                            $member = new Model_MemberItem(array(
                                'user_id' => $user->new_user_id,
                                'email'   => $user->email
                            ));

                            $this->view->n_member = $m_members->save($member);
                        }
                    }
                    if(!$dont_save){
                        //newsletter email log
                        $emails = array(
                            'old' => null,
                            'new' => $user->email
                        );
                    }
                    Cubix_Api::getInstance()->call('addNewlstterEmailLog', array($user->new_user_id, $user_type, 'add', $emails));
                    //

                    // will send activation email
                    $user->reg_type = $user_type;

                    if ( ! $dont_save ) {
                        Model_Hooks::postUserSignUp($user);
                    }

                    if ( ! is_null($this->_getParam('ajax')) ) {
                        $result['msg'] = "
							<h1 style='margin-bottom:20px'><img src='/img/" . Cubix_I18n::getLang() . "_h1_memberssignup.gif' alt='Members signup' title='Members signup' /></h1>
							<div style='width:520px; background: none; margin-bottom:0' class='cbox-small'>
								<h3>". Cubix_I18n::translate('congratulations') . "</h3>
								<p>" . Cubix_I18n::translate('successfully_signed_up', array('SITE_URL' => Cubix_Application::getById()->url, 'SITE_NAME' => Cubix_Application::getById()->title)) . "</p>

								<h3>" . Cubix_I18n::translate('complete_your_registration') . "</h3>
								<p>&nbsp;</p>

								<h4>
									" . Cubix_I18n::translate('signup_successful_04') . "
								</h4>
							</div>
						";
                        $result['signup'] = true;
                        echo json_encode($result);
                        die;
                    }
                    else {
                        if ( $user_type == 'vip-member' ) {
                            $this->_session->want_premium = (object) array('member_data' => array('id' => $this->view->n_member->getId()), 'email' => $user->email);
                            header('Location: /' . Cubix_I18n::getLang() . '/private-v2/upgrade');
                            exit;
                            //$this->_helper->viewRenderer->setScriptAction('signup-success-cc');
                        }
                        else {
                            $this->_helper->viewRenderer->setScriptAction('signup-success');
                        }
                    }
                }
            }
        }
        catch (Exception $e){
            $this->view->errors = array('sales' => 'API is not available. Please try later.');
        }
    }

    public function chooseVipPackageAction()
    {

    }

    public function signinAction()
    {
		if ($this->_getParam('is_cams') && Model_Users::getCurrent() && !$this->user->isAgency()) {
			$api_model = new Model_Api_Cams($this->user->id);
			$token = $api_model->userSignin($this->user);
			if($this->_getParam('redirect'))
			{
				$redirect_path = preg_replace('/[^-_a-z0-9\/]/i', '', $this->_getParam('redirect'));
			}		
			$this->_response->setRedirect($this->view->getLink('cams-login-redirect', array('one-time-token' => $token, 'path' => $redirect_path )));
			return;
		}
		
        $this->view->data = array('username' => '');
        $this->view->errors = array();

		// Check session need captcha
		$failLoginSession = new Zend_Session_Namespace('fail_login');
		if(isset($failLoginSession->captcha_time) && $failLoginSession->captcha_time > time()){
			$this->view->use_captcha = true;
		}

		if(isset($failLoginSession->try_last_time) && $failLoginSession->try_last_time > time()){
			$this->view->try_last_time = true;
		}
		
        if ( $this->_request->isPost() ) {

            $username = trim($this->_getParam('username'));
            $password = trim($this->_getParam('password'));


            $this->view->data['username'] = $username;

            $validator = new Cubix_Validator();

            if ( ! strlen($username) ) {
                $validator->setError('username', 'Username is required');
            }


            if ( ! strlen($password) ) {
                $validator->setError('password', 'Password is required');
            }
			
			$captcha = true;
			if( $this->view->use_captcha){
				$captcha = trim($this->_getParam('captcha'));

				if ( ! strlen($captcha) ) {
					$captcha = false;
					$validator->setError('captcha', 'Captcha is required');
				}
				else {
					$session = new Zend_Session_Namespace('captcha');
					$orig_captcha = $session->captcha;
					if ( strtolower($captcha) != $orig_captcha ) {
						$captcha = false;
						$validator->setError('captcha', 'Captcha is invalid');
					}
				}
			}

            if ( strlen($username) && strlen($password) ) {
                $model = new Model_Users();
				
				$user = $model->getByUsernamePassword($username, $password);
               
                if(isset($user->password_reset_required) && $user->password_reset_required == 1){
                    // Redirect to reset pass page
                    $res = $this->_response->setRedirect( $this->view->getLink('password_change_required') );
                    return;
                }

				if ( is_null($user)) {
                    $validator->setError('username', __('wrong_user_pass'));
                }
				elseif(is_array($user) && isset($user['error_status'])  ){
					
					switch( $user['error_status'] ) {
					
						case Model_Users::LOGIN_BLOCKED_IP:
							$validator->setError('blocked_ip', 'blocked_ip');
							continue;
						
						case Model_Users::LOGIN_BLOCKED_USER:
							$validator->setError('blocked_ip', 'blocked_user');
							continue;
							
						case Model_Users::LOGIN_FAIL_ATTEMPT:
							
							// 3 times fails 
							if( $user['fail_count'] >= FAIL_LOGIN_CAPTCH_COUTN ){
								$failLoginSession->captcha_time = time() + (FAIL_LOGIN_CAPTCH_MIN * 60);
							}
							// 8 times fails 
							if( $user['fail_count'] >= FAIL_LOGIN_BLOCK_USER_COUNT )
							{
								/* send email, notif for account is blocked */
								$subject = Cubix_I18n::translateByLng(Cubix_I18n::getLang(), "block_account_email_subject");
								$message = Cubix_I18n::translateByLng(Cubix_I18n::getLang(), "block_account_email_text",array('USERNAME'=> $username ));
								$email_data = array(
									'subject' => $subject ,
									'message' => $message
								);
								Cubix_Email::sendTemplate('universal_message', $user['email'] , $email_data);
								//
								$this->_response->setRedirect($this->view->getLink('private')); // Redirect for correctly view
							}

							if( $user['fail_count'] == FAIL_LOGIN_BLOCK_USER_COUNT - 1 ){ // if last time set Seesion
								$failLoginSession->try_last_time = time() + (FAIL_LOGIN_CAPTCH_MIN * 60);
								$this->_response->setRedirect($this->view->getLink('private')); // Redirect for correctly view
							}else{ // if not last time clear session time
								unset($failLoginSession->try_last_time);
							}
							
							$validator->setError('username', __('wrong_user_pass'));
							continue;
					}
				}
                elseif(is_object($user)){
					
					unset($failLoginSession->captcha_time);
					unset($failLoginSession->try_last_time);
					
                    if($this->_hasStatusBit($user->escort_data['escort_status'],ESCORT_STATUS_ADMIN_DISABLED) || $this->_hasStatusBit($user->escort_data['escort_status'],ESCORT_STATUS_DELETED)){
                        $validator->setError('status_error', __('status_error'));
                    }

                    if($user->status == -1){
                        $validator->setError('verification_error', __('verification_error'));
                    }
					elseif ( STATUS_ACTIVE != $user->status ) {
						$validator->setError('username', __('wrong_user_pass'));
					}
                }

            }

            $result = $validator->getStatus();


            if ( ! $validator->isValid() ) {
                $this->view->errors = $result['msgs'];
                
                if ( ! is_null($this->_getParam('ajax')) ) {
                    echo(json_encode($result));
                    ob_flush();
                    die;
                }
            }
            else {
                unset($_SESSION['profile_status']);
                $has_active_package = false;
                $is_susp = false;
				$system = Zend_Registry::get('system_config');
				
                if ( $user->user_type == 'agency' ) {
                    $_SESSION['fc_gender'] = 2;

                    /***********************************/
                    $client = new Cubix_Api_XmlRpc_Client();
                    $has_active_package = $user->agency_data['has_active_package']; //$client->call('Escorts.hasPaidActivePackageForAgency', array($user->id));
                }
                else if ( $user->user_type == 'member' ) {
                    $_SESSION['fc_gender'] = 1;
                }
                else if ( $user->user_type == 'escort' ) {
                    $_SESSION['fc_gender'] = 2;

                    /***********************************/
                    $client = new Cubix_Api_XmlRpc_Client();
                    $has_active_package = $user->escort_data['has_active_package']; //$client->call('Escorts.hasPaidActivePackageForEscort', array($user->id));
                    $m_e = new Model_EscortsV2();
                    $is_susp = $m_e->isSuspicious($user->id);
                }

                $_SESSION['fc_username'] = $username;
                $_SESSION['fc_password'] = $password;

                Zend_Session::regenerateId();
                $user->sign_hash = md5(rand(100000, 999999) * microtime(true));
                $user->has_active_package = $has_active_package;
                $user->is_susp = $is_susp;

                //-->Setting chat info
                $chat_info = array(
                    'nickName'	=> $user->username,
                    'userId'	=> $user->id,
                    'userType'	=> $user->user_type,
                    'imIsBlocked' => $user->im_is_blocked
                );

                if ( $user->user_type == 'escort' ) {
                    $m = new Model_EscortsV2();
                    $e = $m->getByUserId($user->id);

					$escort_id = $user->escort_data['escort_id'];
					$showname = $user->escort_data['showname'];
					//$user->sid = urlencode(Cubix_Utils::crypter(session_id(), $system['cams']['key'] , $system['cams']['iv'], 'e'));


                        /* Easter LOTTERY */
                        /*$user->easter_lottery = $model->checkEasterLottery($user->id);*/

//                        if ($user->id == 296800) {
                            /* Xmas LOTTERY */
//                            $user->xmas_lottery = $model->checkXmasLottery($user->id);
//                        }


                    $chat_info['nickName'] = $showname . ' (Escort)';
                    $chat_info['isSusp'] = $is_susp;
                    $chat_info['hasActivePackage'] = $has_active_package;
                    $chat_info['link'] = '/accompagnatrici/' . $showname . '-' . $escort_id;

                    //Getting main image url
                    $parts = array();
                    $catalog = $escort_id;
                    $a = array();
                    if ( is_numeric($catalog) ) {
                        $parts = array();

                        if ( strlen($catalog) > 2 ) {
                            $parts[] = substr($catalog, 0, 2);
                            $parts[] = substr($catalog, 2);
                        }
                        else {
                            $parts[] = '_';
                            $parts[] = $catalog;
                        }
                    }
                    else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
                        array_shift($a);
                        $catalog = $a[0];

                        $parts = array();

                        if ( strlen($catalog) > 2 ) {
                            $parts[] = substr($catalog, 0, 2);
                            $parts[] = substr($catalog, 2);
                        }
                        else {
                            $parts[] = '_';
                            $parts[] = $catalog;
                        }

                        $parts[] = $a[1];
                    }

                    $catalog = implode('/', $parts);
                    $app = Cubix_Application::getById();
                    $chat_info['avatar'] = APP_HTTP.'://pic.' . $app->host . '/' . $app->host . '/' . $catalog . '/' . $user->escort_data['photo_hash'] . '_lvthumb.' . $user->escort_data['photo_ext'];
                } elseif ( $user->user_type == 'agency' ) {
                    $m = new Model_Agencies();
                    $a = $m->getByUserId($user->id);

                    /* Easter LOTTERY */
                    /*$user->easter_lottery = $model->checkEasterLottery($user->id);*/

//                    if ($user->id == 296800) {
                        /* Xmas LOTTERY */
//                        $user->xmas_lottery = $model->checkXmasLottery($user->id);
//                    }

                   $chat_info['nickName'] = $user->agency_data['name'] . ' (Agency)';
                    $chat_info['hasActivePackage'] = $has_active_package;
                } elseif ( $user->user_type == 'member' ) {
					//$user->sid = urlencode(Cubix_Utils::crypter(session_id(), $system['cams']['key'] , $system['cams']['iv'], 'e'));
                    $chat_info['link'] = '/member/' . $user->username;
                }

                $user->chat_info = $chat_info;

                Model_Users::setCurrent($user);

                /*set client ID*/
                Model_Reviews::createCookieClientID($user->id);
                /**/

				
                Model_Hooks::preUserSignIn($user->id);

				if ($this->_getParam('is_cams') && $user->user_type !== 'agency') {
					$this->user = Model_Users::getCurrent();
					$api_model = new Model_Api_Cams($this->user->id);
					$token = $api_model->userSignin($this->user);
					if($this->_getParam('redirect'))
					{
						$redirect_path = preg_replace('/[^-_a-z0-9\/]/i', '', $this->_getParam('redirect'));
					}		
					$this->_response->setRedirect($this->view->getLink('cams-login-redirect', array('one-time-token' => $token, 'path' => $redirect_path )));
					return;
				}
				
                if ($session_redirect->url) {
                    $this->_redirect($session_redirect->url);
                }

				
                // this session made for redirecting not logged in users to private area video section (newsletter button)
                // $this->_response->setRedirect was not working  not sure why)) not my code
                $PA_redirection = new Zend_Session_Namespace('pa_redirection');

                if ( ! is_null($this->_getParam('ajax')) ) {

                    $result['msg'] = "
                        <h1><img src='/img/" . Cubix_I18n::getLang() . "_h1_login.gif' alt='' title='' /></h1>
                        <p style='padding:0 10px; width: 520px;'><span class='strong' style='color: #3F3F3F'>" . Cubix_I18n::translate('signin_success') . "</span></p>
                    ";
                    $result['signin'] = true;
                    echo json_encode($result);
                    die;
                }
                
                if( !$user->last_login_date ){
                    if( $user->user_type == 'agency' ) {
                        $this->_response->setRedirect($this->view->getLink('private-v2-escorts'));
                    } else {
                        //redirection to the PA specific section after logged in

                        if(isset($PA_redirection->pa_redirect_url)){
                            $url = 'http://'.$_SERVER['HTTP_HOST'].'/photo-video/ajax-video';
                            unset($PA_redirection->pa_redirect_url);
                            $this->_redirect($url);
                        }else{
                            $this->_response->setRedirect($this->view->getLink('private-v2-profile'));
                        }
                    }
                }else {

                    if(isset($PA_redirection->pa_redirect_url)){
                        $url = 'http://'.$_SERVER['HTTP_HOST'].'/photo-video/ajax-video';
                        unset($PA_redirection->pa_redirect_url);
                        $this->_redirect($url);

                    }else{
                        $this->_response->setRedirect($this->view->getLink('private-v2'));
                    }
                }
            }
        }

    }

    public function signinCommentAction()
    {
        $comm_sess = new Zend_Session_Namespace('comment');

        if (!$comm_sess->data) {
            $this->_response->setRedirect($this->view->getLink('private-v2'));
        }

        $this->view->data = array('username' => '');
        $this->view->errors = array();

        if ( $this->_request->isPost() ) {

            $username = trim($this->_getParam('username'));
            $password = trim($this->_getParam('password'));


            $this->view->data['username'] = $username;

            $validator = new Cubix_Validator();

            if ( ! strlen($username) ) {
                $validator->setError('username', 'Username is required');
            }


            if ( ! strlen($password) ) {
                $validator->setError('password', 'Password is required');
            }


            if ( strlen($username) && strlen($password) ) {
                $model = new Model_Users();

                if ( ! $user = $model->getByUsernamePassword($username, $password) ) {
                    $validator->setError('username', __('wrong_user_pass'));
                }

                if ( $user ) {
                    if ( STATUS_ACTIVE != $user->status ) {
                        $validator->setError('username', 'Your account is not active yet');
                    }
                }


                //if ( $user->user_type == 'member' ){
                if ( $user->user_type == 'member' ){
                    //$validator->setError('password', __('wrong_user_pass') /*__('m_access_for_members')*/);
                }
            }

            $result = $validator->getStatus();


            if ( ! $validator->isValid() ) {
                $this->view->errors = $result['msgs'];
            }
            else {


                Zend_Session::regenerateId();
                $user->sign_hash = md5(rand(100000, 999999) * microtime(true));
                Model_Users::setCurrent($user);

                /*set client ID*/
                Model_Reviews::createCookieClientID($user->id);
                /**/

                Model_Hooks::preUserSignIn($user->id);

                $this->model = new Model_Comments();
                $this->model->addComment($comm_sess->data);

                $this->_redirect($comm_sess->url);
            }
        }
    }

    private function addRememberMeCook($username, $password)
    {
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $key = '_-_SignInRememberMe_-_' . Cubix_Application::getId();

        $login_data = serialize(array('username' => $username, 'password' => $password));

        $crypt_login_data = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $login_data, MCRYPT_MODE_ECB, $iv);
        $crypt_login_data = base64_encode($crypt_login_data);

        $cookie_name = 'signin_remember_' . Cubix_Application::getId();
        $cookie_lifetime = strtotime('+21 days');

        if ( ! isset($_COOKIE[$cookie_name]) ) {
            setcookie($cookie_name, $crypt_login_data, $cookie_lifetime, "/");
            $_COOKIE[$cookie_name] = $crypt_login_data;
        }
    }

    public function signoutAction()
    {
        Model_Users::setCurrent(NULL);
        Zend_Session::regenerateId();
        $session = new Zend_Session_Namespace('self-checkout-wizard');

        $session->unsetAll();
        $session->cart = array();


        /*$cookie_name = 'signin_remember_' . Cubix_Application::getId();
        if ( isset($_COOKIE[$cookie_name]) && $_COOKIE[$cookie_name] ) {
            setcookie($cookie_name, null, strtotime('- 1 year'), "/");
        }

        //USE FORUM
        $girl_forum = false;

        if ($_GET['girl_forum'] == 1)
            $girl_forum = true;

        if ($girl_forum)
        {
            $forum2 = new Cubix_Forum2Api($girl_forum);

            $forum2->LogOut();
        }*/

        $this->_response->setRedirect($this->view->getLink());
    }

    public function activateAction()
    {
        $hash = $this->_getParam('hash');
        $email = $this->_getParam('email');

        $model = new Model_Users();


        if ( ! $user = $model->activate($email, $hash) ) {
            $this->_response->setRedirect($this->view->getLink('signin'));
            return;
        }

        Model_Hooks::postUserActivate($user);
    }

    public function forgotAction()
    {
        $this->view->data = array('email' => '');
        $this->view->errors = array();

        $countyModel = new Model_Countries();
        $this->view->countries = $countyModel->getPhoneCountries();

        if ( $this->_request->isPost() ) {
            $this->view->data['email'] = $email = trim($this->_getParam('email'));
            $this->view->data['user_type'] = $user_type = trim($this->_getParam('user_type'));
            $this->view->data['c_username'] = $username = trim($this->_getParam('c_username'));

            if ( ! strlen($email) ) {
                $this->view->errors['email'] = 'The email is required';
                return;
            }

            if ( ! filter_var($email, FILTER_VALIDATE_EMAIL) ) {
                $this->view->errors['email'] = 'The email is not valid';
                return;
            }

            if (!strlen($username)){
                $this->view->errors['c_username'] = 'The username is required';
                return;
            }

            $model = new Model_Users();

            if ( ! ($user = $model->forgotPassword($email, $username)) ) {
                $this->view->errors['email'] = 'This email or username is not registered on our site';
                return;
            }

            // if user registered as escort/agency and not shared her phone number she wont be able to reset password
            // if($user['user_type'] != $user_type){
            //     $this->view->errors['user_type'] = 'Please select correct usertype';
            //     return;
            // }

            Cubix_Email::sendTemplate('forgot_verification', $email, array(
                'username' => $user['username'],
                'hash' => $user['email_hash']
            ));

            $this->_helper->viewRenderer->setScriptAction('forgot-success');
        }
    }

    public function passwordChangeRequiredAction()
    {
        
        $this->view->data = array('email' => '');
        $this->view->errors = array();

        $countyModel = new Model_Countries();
        $this->view->countries = $countyModel->getPhoneCountries();

        if ( $this->_request->isPost() ) {
            $this->view->data['email'] = $email = trim($this->_getParam('email'));
            $this->view->data['user_type'] = $user_type = trim($this->_getParam('user_type'));
            $this->view->data['c_username'] = $username = trim($this->_getParam('c_username'));

            if ( ! strlen($email) ) {
                $this->view->errors['email'] = 'The Email required';
                return;
            }

            if ( ! filter_var($email, FILTER_VALIDATE_EMAIL) ) {
                $this->view->errors['email'] = 'The Email is not valid';
                return;
            }

            if ( ! strlen($username) ) {
                $this->view->errors['c_username'] = 'The Username required';
                return;
            }

            $model = new Model_Users();

            if ( ! ($user = $model->forgotPassword($email,$username)) ) {
                $this->view->errors['email'] = 'This email or username is not registered on our site';
                return;
            }

             // if user registered as escort/agency and not shared her phone number she wont be able to reset password
            // if($user['user_type'] != $user_type){
            //     $this->view->errors['user_type'] = 'Please select correct usertype';
            //     return;
            // }

            Cubix_Email::sendTemplate('forgot_verification', $email, array(
                'username' => $user['username'],
                'hash' => $user['email_hash']
            ));

            $this->_helper->viewRenderer->setScriptAction('forgot-success');
        }
    }

    public function checkAction()
    {
        $username = $this->_getParam('username');
        $email = $this->_getParam('email');
        $client = new Cubix_Api_XmlRpc_Client();
        $model = new Model_Users();

        $result = array('status' => '');

        if ( ! is_null($username) ) {

            $has_bl_username = false;
            foreach($this->blacklisted_usernames as $bl_username){
                if( strpos( $username, $bl_username) !== false){
                    $has_bl_username = true;
                    BREAK;
                }
            }

            //if (stripos($username, 'admin') === FALSE)
            if (!$has_bl_username)
            {
                if ( ! $client->call('Users.getByUsername', array($username)) ) {
                    $result['status'] = 'not found';
                }
                else {
                    $result['status'] = 'found';
                }
            }
            else
                $result['status'] = 'found';
        }
        elseif ( ! is_null($email) ) {

            if ( $client->call('Application.isDomainBlacklisted', array($email)) )
            {
                $result['status'] = 'domain blacklisted';
            }
            else
            {
                if ( ! $client->call('Users.getByEmail', array($email)) ) {
                    $result['status'] = 'not found';
                }
                else {
                    $result['status'] = 'found';
                }
            }
        }

        die(json_encode($result));
    }

    public function favoritesAction()
    {
        $this->view->favorites = 1;

        $model = new Model_Users();
        $page = $this->_getParam('page') ? intval($this->_getParam('page')) : 1;
        $per_page = 20;
        $filter = array(
            'f.user_id = ?' => $this->user->id,
            'page' => $page,
            'per_page' => $per_page
        );

        $escorts = $model->getCurrent()->getFavorites($filter);

        $this->view->count = $escorts['count'];
        $this->view->escorts = $escorts['escorts'];

        if ( isset($this->_session->msg) ) {
            $this->view->msg = $this->_session->msg;
            unset($this->_session->msg);
        }
    }

    public function removeFromFavoritesAction()
    {
        $user_id = $this->user->id;
        $escort_id = $this->_request->escort_id;

        $model = new Model_Members();
        $model->removeFromFavorites($user_id, $escort_id);

		$cache = Zend_Registry::get('cache');
		$cache_key = 'favorites_list_for_user_'. Cubix_Application::getId() . '_' . $user_id;
				
		if ($res = $cache->load($cache_key) ) {
			$key = array_search($escort_id, $res);
			unset($res[$key]);
			$cache->save($res, $cache_key, array());
		}
		
        /* reordring top 10 */
        $model->reorderingTop10($user_id);
        /**/

        /* recalculation rating */
        $model->recalculateRating($escort_id);
        /**/

        echo json_encode(
            array(
                'msg' => $this->view->t('removed_from_favorites'),
                'process' => 2
            )
        );
        die;
    }

    public function addToFavoritesAction()
    {
        $user_id = $this->_request->user_id;
        $escort_id = $this->_request->escort_id;

        $model = new Model_Members();
        $model->addToFavorites($user_id, $escort_id);

		$cache = Zend_Registry::get('cache');
		$cache_key = 'favorites_list_for_user_'. Cubix_Application::getId() . '_' . $user_id;
		
		if ($res = $cache->load($cache_key) ) {
			$res[] = $escort_id;
			$cache->save($res, $cache_key, array());
		}
		
        $modelE = new Model_Escorts();
        $showname = $modelE->getShownameById($escort_id);

        echo json_encode(
            array(
                'msg' => $this->view->t('escort_added_to_favorites', array('showname' => $showname)),
                'process' => 1
            )
        );

        //header('Location: ' . $this->view->getLink('favorites'));
        die;
    }

    protected function _validate(Cubix_Validator $validator, $escort_id = null)
    {
        $req = $this->_request;

        $defines = Zend_Registry::get('defines');

        $data = array();
        $e_model = new Model_Escorts();
        $showname = trim($req->showname);
        if ( ! strlen($showname) ) {
            $validator->setError('showname', 'Showname is Required');
        }
        elseif ( ! preg_match('/^[^-][-_a-z0-9]+$/i', $showname) ) {
            // $validator->setError('showname', 'Must begin with letter or number and must contain only alphanumeric characters');
        }


        /*if( $escort_id ) {
            if( $e_model->existsByShowname($showname, $escort_id) ) {
                $validator->setError('showname', 'Showname Already exists');
            }
        }
        else {
            if( $e_model->existsByShowname($showname, null) ) {
                $validator->setError('showname', 'Showname Already exists');
            }
        }*/

        $data['showname'] = $showname;

        $ethnicity = intval($req->ethnicity);
        if ( ! $ethnicity ) {

        }
        $data['ethnicity'] = $ethnicity;

        $nationality_id = $req->nationality_id;
        if ( ! $nationality_id ) {

        }
        $data['nationality_id'] = $nationality_id;

        if ( $req->height_feet || $req->height_inches || $req->height || $req->weight ) {
            $measure_units = $req->measure_units;
            if ( ! $measure_units ) {
                $validator->setError('measure_units', 'Measure unit is Required');
            }
            $data['measure_units'] = $measure_units;
        }

        $gender = intval($req->gender);
        if ( ! $gender ) {
            $validator->setError('gender', 'Gender is Required');
        }
        $data['gender'] = $gender;

        /*$b_d = intval($req->birth_date_day);
        $b_m = intval($req->birth_date_month);
        $b_y = intval($req->birth_date_year);
        if ( ($b_d || $b_m || $b_y) && (! $b_d || ! $b_m || ! $b_y) ) {
            $validator->setError('birth_date', 'Pleas select birth date');
            $data['birth_date'] = mktime(0, 0, 0, $b_m, $b_d, $b_y);
        }
        elseif ( $b_d && $b_m && $b_y ) {
            $data['birth_date'] = mktime(0, 0, 0, $b_m, $b_d, $b_y);
        }
        else {
            $data['birth_date'] = 0;
        }*/

        $age = intval($req->birth_date);
        if ( ! $age ) {
            $data['birth_date'] = 0;
        }
        else {
            $data['birth_date'] = $age;
        }

        if ( $measure_units == 'metric' ) {
            $height = $req->height;

            if ( $height && ! is_numeric($height) ) {
                $validator->setError('height', 'Height : Only numbers accepted');
            }
            $data['height'] = intval($height);
        }
        elseif ( $measure_units == 'royal' ) {
            $height_f = $req->height_feet;
            $height_i = $req->height_inches;

            if ( $height_f && $height_i && ( ! is_numeric($height_f) || ! is_numeric($height_i) ) ) {
                $validator->setError('height', 'Height : Only numbers are accepted');
            }
            $data['height'] = intval($height_f) . '|' . intval($height_i);
        }
        else {
            $data['height'] = null;
        }

        $weight = $req->weight;
        if ( $weight && ! is_numeric($weight) ) {
            $validator->setError('weight', 'Weight : Only numbers are accepted');
        }
        $data['weight'] = intval($weight);

        $bust = intval($req->bust); $waist = intval($req->waist); $hip = intval($req->hip);
        if ( ! $bust || ! $waist || ! $hip ) {

        }
        $data['bust'] = $bust; $data['waist'] = $waist; $data['hip'] = $hip;

        $hair_color = intval($req->hair_color);
        if ( ! $hair_color ) {

        }
        $data['hair_color'] = $hair_color;

        $hair_length = intval($req->hair_length);
        if ( ! $hair_length ) {

        }
        $data['hair_length'] = $hair_length;

        $eye_color = intval($req->eye_color);
        if ( ! $eye_color ) {

        }
        $data['eye_color'] = $eye_color;

        $shoe_size = $req->shoe_size;
        if ( strlen($dress_size) && ! is_numeric($shoe_size) ) {
            $validator->setError('shoe_size', 'Shoe size : Only numbers are accepted');
        }
        $data['shoe_size'] = intval($shoe_size);

        $breast_size = $req->breast_size;
        if ( true ) {

        }
        $data['breast_size'] = $breast_size;

        $dress_size = $req->dress_size;
        if ( strlen($dress_size) && ! is_numeric($dress_size) ) {
            $validator->setError('dress_size', 'Dress size : Only numbers are accepted');
        }
        $data['dress_size'] = intval($dress_size);

        $is_smoker = intval($req->is_smoker);
        if ( ! $is_smoker ) {

        }
        $data['is_smoker'] = $is_smoker;

        $availability = intval($req->availability);
        if ( ! $availability ) {
            $validator->setError('availability', 'Please set the availability');
        }
        $data['availability'] = $availability;

        $contact_zip = $req->contact_zip;
        if ( strlen($contact_zip) && ! preg_match('/^([0-9]{5})$/', $contact_zip) ) {
            $validator->setError('contact_zip', 'Wrong zip code format');
        }
        $data['contact_zip'] = $contact_zip;

        $characteristics = $req->characteristics;
        if ( ! $characteristics ) {

        }
        $data['characteristics'] = $characteristics;

        $sex = (array) $req->sex_availability;

        foreach ( $sex as $i => $s ) {
            if ( ! in_array($s, array_keys($defines['sex_availability_options'])) ) {
                unset($sex[$i]);
            }
        }

        if ( ! count($sex) ) {
            $validator->setError('sex_availability', 'Sex availability is Required');
            $data['sex_availability'] = array();
        }
        else {
            $data['sex_availability'] = implode($sex, ',');
        }

        $about = (array) $req->about;
        $langs = Cubix_Application::getLangs();
        foreach ( $langs as $lang ) {
            if ( isset($about[$lang->id]) ) {
                $about[$lang->id] = substr($about[$lang->id], 0, 255);
            }
            else {
                $about[$lang->id] = '';
            }
        }
        $data['about'] = $about;

        // Services block
        $svc_kissing = intval($req->svc_kissing);
        if ( ! $svc_kissing ) {

        }
        $data['svc_kissing'] = $svc_kissing;

        $svc_blowjob = intval($req->svc_blowjob);
        if ( ! $svc_blowjob ) {

        }
        $data['svc_blowjob'] = $svc_blowjob;

        $svc_cumshot = intval($req->svc_cumshot);
        if ( ! $svc_cumshot ) {

        }
        $data['svc_cumshot'] = $svc_blowjob;

        $svc_69 = intval($req->svc_69);
        if ( ! $svc_69 ) {

        }
        $data['svc_69'] = $svc_69;

        $svc_anal = intval($req->svc_anal);
        if ( ! $svc_anal ) {

        }
        $data['svc_anal'] = $svc_anal;

        // TODO: Filter this field
        /*$svc_additional = $req->svc_additional;
        if ( ! $svc_additional ) {

        }
        $data['svc_additional'] = $svc_additional;*/
        $svc_additional = (array) $req->svc_additional;
        $langs = Cubix_Application::getLangs();
        foreach ( $langs as $lang ) {
            if ( isset($svc_additional[$lang->id]) ) {
                $svc_additional[$lang->id] = substr($svc_additional[$lang->id], 0, 255);
            }
            else {
                $svc_additional[$lang->id] = '';
            }
        }
        $data['svc_additional'] = $svc_additional;

        // Working times block
        $wds = (array) $req->work_days;
        $work_times = array();
        foreach ( $wds as $d => $nil ) {
            if ( ! ($req->work_times_from[$d]) || ! ($req->work_times_to[$d]) ) {
                $validator->setError('work_times_' . $d, 'Select time interval');
            }

            $work_times[$d] = array('from' => $req->work_times_from[$d], 'to' => $req->work_times_to[$d]);
        }
        $data['working_times'] = $work_times;

        // Contacts block
        $contact_phone = $req->contact_phone;
        if ( ! $contact_phone ) {

        }
        $data['contact_phone'] = $contact_phone;

        $phone_instructions = $req->phone_instructions;
        if ( ! $phone_instructions ) {

        }
        $data['phone_instructions'] = $phone_instructions;

        $contact_email = $req->contact_email;
        if ( ! $contact_email ) {

        }
        $data['contact_email'] = $contact_email;

        $contact_web = $req->contact_web;
        if ( ! $contact_web ) {

        }
        $data['contact_web'] = $contact_web;

        // Languages block
        $langs = $req->langs;
        if ( ! is_array($langs) ) $langs = array();

        foreach ( $langs as $i => $lang ) {
            $lang = explode(':', $lang);

            // Prevent injection of custom data in langs[] parmeter
            if ( 2 != count($lang) ) {
                unset($langs[$i]);
                continue;
            }
            elseif ( ! in_array($lang[0], array_keys($defines['language_options'])) ) {
                unset($langs[$i]);
                continue;
            }
            elseif ( ! in_array($lang[1], array_keys($defines['language_level_options'])) ) {
                unset($lang[$i]);
                continue;
            }

            $langs[$i] = array('lng' => $lang[0], 'lvl' => $lang[1], 'label' => $defines['language_options'][$lang[0]] . ' - ' . $defines['language_level_options'][$lang[1]]);
        }
        $data['langs'] = $langs;

        if ( ! count($langs) ) {
            $validator->setError('langs', 'Please specify at least one language');
        }

        // Working cities block
        //print_r($req->cities);
        //echo "<br/><br/><br/><br/>";
        //print_r($req->city_id); die;
        $city_id = intval($req->city_id);
        if ( ! $city_id ) {
            $validator->setError('city_id', 'Base city is Required');
        }
        $data['base_city_id'] = $city_id;

        $cities = $req->cities;

        if ( ! is_array($cities) ) $cities = array();
        $country_id = Cubix_Application::getById()->country_id;
        $m_cities = new Cubix_Geography_Cities();
        $cities = array_slice($cities, 0, 4);
        foreach ( $cities as $i => $city_id ) {
            $city_id = intval($city_id);
            if ( ! $city_id ) {
                unset($cities[$i]);
                continue;
            }

            // Check if city_id belongs to current country
            /*$city = $m_cities->get($city_id);

            if ( ! $city || $city->country_id != $country_id ) {
                unset($cities[$i]);
                continue;
            }*/
        }
        $cities = array_values($cities);
        foreach ( $cities as $i => $city_id ) {
            $cities[$i] = array('id' => $city_id);
        }

        $data['cities'] = $cities;
        $data['country_id'] = $req->wl_country;
        if ( ! count($cities) ) {
            $validator->setError('cities', 'At least one working city is required');
        }

        /* // Sales person
        $client = Cubix_Api_XmlRpc_Client::getInstance();

        $sales_user_id = intval($req->sales_user_id);
        if ( ! $sales_user_id ) $sales_user_id = null;

        if ( ! is_null($sales_user_id) ) {
            if ( ! $client->call('Users.isSalesValid', array($sales_user_id)) ) {
                $sales_user_id = null;
            }
        }
        $data['sales_user_id'] = $sales_user_id; */

        return $data;
    }

    public function profileAction()
    {
        $this->view->user = $this->user;

        $client = Cubix_Api_XmlRpc_Client::getInstance();

        $add_data = array('user_id' => $this->user->getId());
        $agency = $this->user->getAgency();

        $client = new Cubix_Api_XmlRpc_Client();

        if ( $this->user->isEscort() /*&& $this->user->hasProfile()*/ ) {
            $escort_id = $this->user->getEscort()->getId();

            $this->view->sales_persons = $client->call('Users.getSalesPersons');
            $this->view->vacation = $this->user->getEscort()->getVacation();
        }
        elseif ( $this->user->isAgency() ) {
            $escort_id = intval($this->_getParam('id'));
            if ( 0 == $escort_id ) $escort_id = null;

            $add_data['agency_id'] = $agency->getId();
            if ( ! is_null($escort_id) ) {
                if ( ! $client->call('Agencies.hasEscort', array($agency->getId(), $escort_id)) ) {
                    die;
                }

                $model = new Model_Escorts();
                $escort = $model->getById($escort_id);

                $this->view->vacation = $escort->getVacation();

                //$add_data['agency_id'] = $agency->getId();

                //$this->view->sitePath()->append($escort->showname, $this->view->getLink('profile', array('showname' => $escort->showname, 'escort_id' => $escort_id)));
            }
        }

        $result = $client->call('Escorts.getProfileLatestRevision', array($escort_id));

        if ( ! $result ) {
            $result = $this->_validate(new Cubix_Validator());
        }
        elseif ( count($result['langs']) > 0 && ! isset($result['langs'][0]['label']) ) {
            $defines = Zend_Registry::get('defines');

            foreach ( $result['langs'] as $i => $lang ) {
                $result['langs'][$i]['label'] = $defines['language_options'][$lang['lng']] . ' - ' . $defines['language_level_options'][$lang['lvl']];
            }
        }

        $profile = new Model_EscortItem($result);

        $countries = new Cubix_Geography_Countries();
        $this->view->countries = $countries->ajaxGetAll(false);

        $region_cities = new Cubix_Geography_Cities();

        $country_id = Cubix_Application::getById()->country_id;
        if ( $profile['base_city_id'] ) {
            $city = $region_cities->get($profile['base_city_id']);

            $country_id = $city->country_id;
        }

        $profile['country_id'] = $country_id;

        $this->view->profile = $profile;

        $region_cities = $this->view->cities = $region_cities->ajaxGetAll(null, $country_id);

        $regions = array();
        foreach ( $region_cities as $city ) {
            if ( ! isset($regions[$city->region_title]) ) {
                $regions[$city->region_title] = array();
            }

            $regions[$city->region_title][] = $city;
        }

        ksort($regions);
        $this->view->region_cities = $regions;

        $this->view->profile->setThrowExceptions(false);

        if ( $this->_request->isPost() ) {
            $data = array();

            $validator = new Cubix_Validator();

            $data = $this->_validate($validator, $escort_id);

            // $client->call('Users.assignEscort', array($data['sales_user_id'], $escort_id));

            $this->view->profile = new Model_EscortItem($data);
            $this->view->profile->setThrowExceptions(false);

            if ( ! $validator->isValid() ) {
                $status = $validator->getStatus();

                $this->view->errors = $status['msgs'];

                return;
            }

            try {
                if ( $escort_id ) {
                    if ( ! $this->user->hasProfile() ) {
                        $client->call('Escorts.removeStatusBit', array($escort_id,
                            Model_Escorts::ESCORT_STATUS_NO_PROFILE
                        ));

                        if ( $this->user->isEscort() ) {
                            $client->call('Escorts.updateShowname', array($escort_id, $data['showname']));
                        }

                        if ( $this->user->isAgency() ) {
                            $client->call('Escorts.setStatusBit', array($escort_id,
                                Model_Escorts::ESCORT_STATUS_PROFILE_CHANGED
                            ));
                        }
                    }
                    else {
                        $client->call('Escorts.setStatusBit', array($escort_id,
                            Model_Escorts::ESCORT_STATUS_PROFILE_CHANGED
                        ));
                    }
                }
                //var_dump($data);die;
                $result = $client->call('Escorts.updateProfile', array($escort_id, $data, $add_data));
            }
            catch ( Exception $e ) {
                // var_dump($client->getLastResponse()); die;
            }

            if ( isset($result['error']) ) {
                // var_dump($result['error']); die;
            }
            else {
                if ( false !== $result ) {
                    $this->view->saved = true;
                    // echo nl2br(print_r($result, true));
                }
            }
        }
    }

    public function photosAction()
    {
        $this->view->user = $this->user;
        $agency = $this->user->getAgency();

        $client = new Cubix_Api_XmlRpc_Client();

        if ( $this->user->isEscort() ) {
            $escort = $this->user->getEscort();
            $escort_id = $escort->id;
        }
        else {
            $escort_id = intval($this->_getParam('id'));

            if ( 0 == $escort_id ) $escort_id = null;

            if ( ! is_null($escort_id) && ! $client->call('Agencies.hasEscort', array($agency->getId(), $escort_id)) ) {
                die;
            }

            $model = new Model_Escorts();
            $escort = $model->getById($escort_id);

            //$this->view->sitePath()->append($escort->showname, $this->view->getLink('profile', array('showname' => $escort->showname, 'escort_id' => $escort_id)));
        }

        $this->view->escort = $escort;
        $this->view->photos = $photos = $escort->getPhotos(null);

        $photos_ids = array();
        foreach ( $photos as $photo ) {
            $photo_ids[] = intval($photo->getId());
        }

        $action = $this->_getParam('a');

        $photos_min_count = Cubix_Application::getById(Cubix_Application::getId())->min_photos;

        if ( ! is_null($action) ) {
            if ( 'set-main' == $action ) {
                $photo_id = intval($this->_getParam('photo_id'));

                if ( ! in_array($photo_id, $photo_ids) ) {
                    die(json_encode(array('error' => 'An error occured when setting main image')));
                }

                $photo = new Model_Escort_PhotoItem(array('id' => $photo_id));
                $result = $photo->setMain();

                if ( true !== $result ) echo json_encode($result);
                else echo json_encode(array('success' => true));
            }
            elseif ( 'delete' == $action ) {
                $ids = $this->_getParam('photo_ids');

                if ( ! is_array($ids) ) {
                    die(json_encode(array('error' => 'An error occured when deleteing image')));
                }

                foreach ( $ids as $id ) {
                    if ( ! in_array($id, $photo_ids) ) {
                        die(json_encode(array('error' => 'An error occured when deleting image')));
                    }
                }

                $photo = new Model_Escort_Photos();
                $result = $photo->remove($ids);

                $photos_count = $escort->getPhotosCount();

                if ( $photos_count < $photos_min_count )
                {
                    if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_ACTIVE))) ) {
                        $client->call('Escorts.removeStatusBit', array($escort_id, array(
                            Model_Escorts::ESCORT_STATUS_ACTIVE
                        )));
                    }

                    if ( ! $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS))) ) {
                        $client->call('Escorts.setStatusBit', array($escort_id, array(
                            Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS
                        )));
                    }
                }

                if ( true !== $result ) echo json_encode(array('error' => 'An error occured when deleting image'));
                else echo json_encode(array('success' => true));
            }
            elseif ( 'upload' == $action ) {
                $is_private = intval($this->_getParam('is_private'));

                try {
                    if ( ! isset($_FILES['photo']) || ! isset($_FILES['photo']['name']) || ! strlen($_FILES['photo']['name']) ) {
                        $this->view->uploadError = 'Please select a photo to upload';
                        return;
                    }

                    // Save on remote storage
                    $images = new Cubix_Images();
                    $image = $images->save($_FILES['photo']['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $_FILES['photo']['name']))));

                    $image = new Cubix_Images_Entry($image);
                    $image->setSize('sthumb');
                    $image->setCatalogId($escort->id);
                    $image_url = $images->getUrl($image);

                    $photo = new Model_Escort_PhotoItem(array(
                        'escort_id' => $escort->id,
                        'hash' => $image->getHash(),
                        'ext' => $image->getExt(),
                        'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_HARD
                    ));

                    $model = new Model_Escort_Photos();
                    $photo = $model->save($photo);

                    $photos_count = $escort->getPhotosCount();

                    if ( $photos_count < $photos_min_count )
                    {
                        if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_ACTIVE))) ) {
                            $client->call('Escorts.removeStatusBit', array($escort_id, array(
                                Model_Escorts::ESCORT_STATUS_ACTIVE
                            )));
                        }


                        if ( ! $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS))) ) {
                            $client->call('Escorts.setStatusBit', array($escort_id, array(
                                Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS
                            )));
                        }


                    }
                    else {

                        if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS))) ) {
                            $client->call('Escorts.removeStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_ENOUGH_PHOTOS)));
                        }

                        if ( ! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_ADMIN_DISABLED)) &&
                            ! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_OWNER_DISABLED)) )
                        {
                            if (
                            (! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_IS_NEW)) &&
                                ! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_NO_PROFILE)) &&
                                ! $client->call('Escorts.hasStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_PROFILE_CHANGED)))
                            )
                            {
                                $client->call('Escorts.setStatusBit', array($escort_id, Model_Escorts::ESCORT_STATUS_ACTIVE));
                            }
                        }

                        /*if ( ! $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_ACTIVE))) ) {
                            $client->call('Escorts.setStatusBit', array($escort_id, array(
                                Model_Escorts::ESCORT_STATUS_ACTIVE
                            )));
                        }*/
                    }

                    /*$result = array(
                        'status' => 'success',
                        'photo' => $photo->toJSON('backend_thumb')
                    );*/
                    $this->view->newPhoto = $photo;
                } catch (Exception $e) {
                    $this->view->uploadError = $e->getMessage();
                }
            }
            elseif ( 'set-adj' == $action ) {
                $photo_id = intval($this->_getParam('photo_id'));

                if ( ! in_array($photo_id, $photo_ids) ) {
                    die(json_encode(array('error' => 'An error occured')));
                }

                $photo = new Model_Escort_PhotoItem(array(
                    'id' => $photo_id
                ));

                try {
                    $hash = $photo->getHash();
                    $result = array(
                        'x' => intval($this->_getParam('x')),
                        'y' => intval($this->_getParam('y')),
                        'px' => floatval($this->_getParam('px')),
                        'py' => floatval($this->_getParam('py'))
                    );
                    $photo->setCropArgs($result);

                    // Crop All images
                    $size_map = array(
                        'backend_thumb' => array('width' => 150, 'height' => 205),
                        'medium' => array('width' => 225, 'height' => 300),
                        'thumb' => array('width' => 150, 'height' => 200),
                        'nlthumb' => array('width' => 120, 'height' => 160),
                        'sthumb' => array('width' => 76, 'height' => 103),
                        'lvthumb' => array('width' => 75, 'height' => 100),
                        'agency_p100' => array('width' => 90, 'height' => 120),
                        't100p' => array('width' => 117, 'height' => 97)
                    );
                    $conf = Zend_Registry::get('images_config');

                    get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
                    // echo $conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;

                    $catalog = $escort_id;
                    $a = array();
                    if ( is_numeric($catalog) ) {
                        $parts = array();

                        if ( strlen($catalog) > 2 ) {
                            $parts[] = substr($catalog, 0, 2);
                            $parts[] = substr($catalog, 2);
                        }
                        else {
                            $parts[] = '_';
                            $parts[] = $catalog;
                        }
                    }
                    else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
                        array_shift($a);
                        $catalog = $a[0];

                        $parts = array();

                        if ( strlen($catalog) > 2 ) {
                            $parts[] = substr($catalog, 0, 2);
                            $parts[] = substr($catalog, 2);
                        }
                        else {
                            $parts[] = '_';
                            $parts[] = $catalog;
                        }

                        $parts[] = $a[1];
                    }

                    $catalog = implode('/', $parts);

                    foreach($size_map as $size => $sm) {
                        // echo $conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y'] . "<br />";
                        get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
                    }

                    /*if ( true !== $result ) {
                        die(json_encode(array('error' => 'An error occured')));
                    }*/
                }
                catch ( Exception $e ) {
                    die(json_encode(array('error' => 'An error occured')));
                }

                echo json_encode(array('success' => true));
            }

            if ( 'upload' != $action ) {
                $this->view->layout()->disableLayout();
                $this->_helper->viewRenderer->setNoRender(true);
                die;
            }
        }
    }

    public function escortsAction()
    {
        $this->view->agency = $agency = $this->user->getAgency();

        $model = new Model_Escorts();
    }

    public function deleteEscortAction()
    {
        $escort_id = intval($this->_getParam('id'));

        if ( ! $escort_id ) die;

        $client = new Cubix_Api_XmlRpc_Client();

        $agency = $this->user->getAgency();

        if ( ! $client->call('Agencies.hasEscort', array($agency->getId(), $escort_id)) ) {
            die;
        }

        $client->call('Escorts.delete', array($escort_id));
        /*$model = new Model_Escorts();
        $model->remove($escort_id);*/

        $this->getResponse()->setRedirect($this->view->getLink('edit-escorts'));
    }

    public function ratesAction()
    {
        $this->view->user = $this->user;
        $agency = $this->user->getAgency();

        $client = new Cubix_Api_XmlRpc_Client();

        if ( $this->user->isEscort() ) {
            $escort = $this->user->getEscort();
        }
        else {
            $model = new Model_Escorts();
            $escort_id = intval($this->_getParam('id'));

            if ( 0 == $escort_id ) $escort_id = null;

            if ( ! is_null($escort_id) && ! $client->call('Agencies.hasEscort', array($agency->getId(), $escort_id)) ) {
                die;
            }

            $escort = $model->getById($escort_id);

            //$this->view->sitePath()->append($escort->showname, $this->view->getLink('profile', array('showname' => $escort->showname, 'escort_id' => $escort_id)));
        }

        $this->view->escort = $escort;

        $this->view->appear = array();

        $a = $this->_getParam('a');
        if ( 'delete' == $a ) {
            $rates = $this->_getParam('rates');
            if ( is_array($rates) ) {
                foreach ( $rates as $i => $rate ) {
                    $rate = explode(':', $rate);
                    if ( 5 != count($rate) ) unset($rate[$i]);
                    foreach ( $rate as $r ) {
                        if ( ! is_numeric($r) ) {
                            unset($rate[$i]);
                        }
                    }
                }

                if ( 0 < count($rates) ) {
                    $client->call('Escorts.deleteRates', array($escort->getId(), $rates));
                }
            }
        }
        elseif ( 'add' == $a ) {
            $data = array('availability' => '', 'time' => '', 'time_unit' => '', 'price' => '', 'currency' => '');
            foreach ( array_keys($data) as $k ) {
                $data[$k] = intval($this->_getParam($k));
            }
            $data['currency_id'] = $data['currency']; unset($data['currency']);
            $data['escort_id'] = intval($escort->getId());

            $this->view->appear = $client->call('Escorts.addRate', array($data));
            unset($this->view->appear['escort_id']);
        }


        $this->view->rates = $client->call('Escorts.getRates', array($escort->getId()));
    }

    public function toursAction()
    {
        $this->view->user = $this->user;
        $agency = $this->user->getAgency();

        $client = new Cubix_Api_XmlRpc_Client();

        if ( $this->user->isEscort() ) {
            $escort = $this->user->getEscort();
        }
        else {
            $model = new Model_Escorts();
            $escort_id = intval($this->_getParam('id'));

            if ( 0 == $escort_id ) $escort_id = null;

            if ( ! is_null($escort_id) && ! $client->call('Agencies.hasEscort', array($agency->getId(), $escort_id)) ) {
                die;
            }

            $escort = $model->getById($escort_id);

            //$this->view->sitePath()->append($escort->showname, $this->view->getLink('profile', array('showname' => $escort->showname, 'escort_id' => $escort_id)));
        }

        $this->view->escort = $escort;

        $this->view->appear = array();

        $this->view->tour = (object) array('city_id' => '', 'email' => '', 'phone' => '', 'date' => array(
            'from' => array('d' => '', 'm' => '', 'y' => ''),
            'to' => array('d' => '', 'm' => '', 'y' => '')
        ));

        $a = $this->_getParam('a');
        if ( 'delete' == $a ) {
            $tours = $this->_getParam('tours');
            if ( is_array($tours) ) {
                foreach ( $tours as $i => $tour_id ) {
                    if ( ! is_numeric($tour_id) ) {
                        unset($tours[$i]);
                    }
                    else {
                        $tours[$i] = intval($tour_id);
                    }
                }

                if ( 0 < count($tours) ) {
                    $client->call('Escorts.deleteTours', array($escort->getId(), $tours));
                }
            }
        }
        elseif ( 'add' == $a ) {
            $this->view->errors = $error = array();

            $country_id = intval(Cubix_Application::getById()->country_id);

            $data = array('city_id' => '', 'date' => '', 'email' => '', 'phone' => '');
            foreach ( array_keys($data) as $k ) {
                $data[$k] = $this->_getParam($k);
            }

            if ( $data['city_id'] ) {
                $cm = new Cubix_Geography_Cities();
                $city = $cm->get($data['city_id']);

                if ( ! $city || $city->country_id != $country_id ) {
                    die;
                }
            }
            else {
                $errors['city'] = 'Required';
            }

            $data['country_id'] = $country_id;
            $data['escort_id'] = intval($escort->getId());

            if ( ! is_array($data['date']) || ! isset($data['date']['from']) || ! isset($data['date']['to']) ) {
                die;
            }
            else {
                $date_from = $data['date']['from']; $date_to = $data['date']['to'];
                if ( ! isset($date_from['d']) || ! isset($date_from['m']) || ! isset($date_from['y'] ) ||
                    ! isset($date_to['d']) || ! isset($date_to['m']) || ! isset($date_to['y'] ) ) {
                    die;
                }

                $f_m = intval($date_from['m']); $f_d = intval($date_from['d']); $f_y = intval($date_from['y']);
                $t_m = intval($date_to['m']); $t_d = intval($date_to['d']); $t_y = intval($date_to['y']);

                if ( ! $f_m || ! $f_d || ! $f_y ) {
                    $errors['date_from'] = 'Required';
                }

                if ( ! $t_m || ! $t_d || ! $t_y ) {
                    $errors['date_to'] = 'Required';
                }

                if ( ! isset($errors['date_from']) && ! isset($errors['date_to']) ) {
                    $date_from = mktime(0, 0, 0, $f_m, $f_d, $f_y);
                    $date_to = mktime(0, 0, 0, $t_m, $t_d, $t_y);
                }
            }

            if ( ! count($errors) ) {
                unset($data['date']);
                $data['date_from'] = $date_from;
                $data['date_to'] = $date_to;

                $this->view->appear = $client->call('Escorts.addTour', array($data));
            }
            else {
                $this->view->errors = $errors;
                $this->view->tour = (object) $data;
            }
        }

        $tours = $this->view->tours = $client->call('Escorts.getTours', array($escort->getId()));
    }

    public function agencyProfileAction()
    {
        $client = new Cubix_Api_XmlRpc_Client();

        if ( $this->user->isAgency() )
        {
            $agency_data = $client->call('Agencies.getByUserId', array($this->user->id));
            $agency_id = $agency_data['id'];
            $agency_data = array_merge($agency_data, $client->call('Agencies.getInfo', array($agency_data['name'])));

            $this->view->sales_persons = $client->call('Users.getSalesPersons');
            $this->view->agency_data = new Model_AgencyItem($agency_data);
        }

        $this->view->user = $this->user;

        if ( $this->_request->isPost() ) {

            if ( $this->_request->a )
            {
                try {
                    if ( ! isset($_FILES['logo']) || ! isset($_FILES['logo']['name']) || ! strlen($_FILES['logo']['name']) ) {
                        $this->view->uploadError = 'Please select a photo to upload';
                        return;
                    }

                    // Save on remote storage
                    $images = new Cubix_Images();
                    $image = $images->save($_FILES['logo']['tmp_name'], 'agencies', $agency_data['application_id'], strtolower(@end(explode('.', $_FILES['logo']['name']))));

                    $agency_data['logo_hash'] = $image['hash'];
                    $agency_data['logo_ext'] = $image['ext'];
                } catch (Exception $e) {
                    $this->view->uploadError = $e->getMessage();
                }
            }

            $data = array();

            $validator = new Cubix_Validator();

            $data = $this->_validateAgency($validator);

            /* // Assign escort to a sales person
            $client->call('Users.assignAgency', array($data['sales_user_id'], $agency_id));
            unset($data['sales_user_id']); */

            $data['logo_hash'] = $agency_data['logo_hash'];
            $data['logo_ext'] = $agency_data['logo_ext'];
            $data['application_id'] = $agency_data['application_id'];

            $this->view->agency_data = new Model_AgencyItem($data);

            if ( ! $validator->isValid() ) {
                $status = $validator->getStatus();

                $this->view->errors = $status['msgs'];

                return;
            }

            try {
                $result = $client->call('Agencies.updateAgencyProfile', array($agency_id, $data));

            }
            catch (Exception $e) {
                var_dump($client->getLastResponse()); die;
            }

            if ( isset($result['error']) ) {
                print_r($result['error']); die;
            }
            else {
                if ( $result !== false ) {
                    $this->view->saved = true;
                    echo nl2br(print_r($result, true));
                }
            }
        }
    }

    protected function _validateAgency(Cubix_Validator $validator)
    {
        $req = $this->_request;

        $defines = Zend_Registry::get('defines');
        $client = new Cubix_Api_XmlRpc_Client();

        $data = array();

        $showname = trim($req->agency_name);
        if ( ! strlen($showname) ) {
            $validator->setError('name', 'Required');
        }
        elseif ( ! preg_match('/^[^-][-_a-z0-9]+$/i', $showname) ) {
            $validator->setError('name', 'Must begin with letter or number and must contain only alphanumeric characters');
        }
        $data['name'] = $showname;

        // Working times block
        $wds = (array) $req->work_days;
        $work_times = array();
        foreach ( $wds as $d => $nil ) {
            if ( ! ($req->work_times_from[$d]) || ! ($req->work_times_to[$d]) ) {
                $validator->setError('work_times_' . $d, 'Select time interval');
            }

            $work_times[$d] = array('from' => $req->work_times_from[$d], 'to' => $req->work_times_to[$d]);
        }
        $data['working_times'] = $work_times;


        // Contacts block
        $contact_phone = $req->contact_phone;
        if ( ! $contact_phone ) {

        }
        $data['phone'] = $contact_phone;

        $phone_instructions = $req->phone_instructions;
        if ( ! $phone_instructions ) {

        }
        $data['phone_instructions'] = $phone_instructions;

        $contact_email = $req->contact_email;
        if ( ! $contact_email ) {

        }
        $data['email'] = $contact_email;

        $contact_web = $req->contact_web;
        if ( ! $contact_web ) {

        }
        $data['web'] = $contact_web;

        //Location
        $country = $req->country_id;
        if ( ! $country ) {

        }
        $data['country_id'] = Cubix_Application::getById()->country_id;

        $region = $req->region_id;
        if ( ! $region ) {

        }
        $data['region_id'] = $region;

        $city = $req->city_id;
        if ( ! $city ) {

        }
        $data['city_id'] = $city;

        if ( strlen($data['email']) && $client->call('Application.isDomainBlacklisted', array($data['email'])) ) {
            $validator->setError('email', 'Domain is blacklisted');
        }

        if ( strlen($data['web']) && $client->call('Application.isDomainBlacklisted', array($data['web'])) ) {
            $validator->setError('web', 'Domain is blacklisted');
        }

        /* // Sales person
        $client = Cubix_Api_XmlRpc_Client::getInstance();

        $sales_user_id = intval($req->sales_user_id);
        if ( ! $sales_user_id ) $sales_user_id = null;

        if ( ! is_null($sales_user_id) ) {
            if ( ! $client->call('Users.isSalesValid', array($sales_user_id)) ) {
                $sales_user_id = null;
            }
        }
        $data['sales_user_id'] = $sales_user_id; */

        return $data;
    }

    public function changePasswordAction()
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $user_id = $this->user->id;

        if ( $this->_request->isPost() ) {
            $data = array();

            $validator = new Cubix_Validator();

            $data = $this->_validatePassword($validator);

            if ( ! $validator->isValid() ) {
                $status = $validator->getStatus();

                $this->view->errors = $status['msgs'];

                return;
            }

            try {
                $result = $client->call('Users.updatePassword', array($user_id, $data['new_pass']));
            }
            catch (Exception $e) {
                var_dump($client->getLastResponse()); die;
            }

            if ( isset($result['error']) ) {
                print_r($result['error']); die;
            }
            else {
                if ( $result !== false ) {
                    $this->view->saved = true;
                    echo nl2br(print_r($result, true));
                }
            }
        }
    }

    protected function _validatePassword(Cubix_Validator $validator)
    {
        $client = new Cubix_Api_XmlRpc_Client();
        $req = $this->_request;

        $curr_pass = $req->curr_pass;
        if ( ! strlen($curr_pass) ) {
            $validator->setError('curr_pass', 'Current Password is required !');
        }
        else if ( strlen($curr_pass) && ! $client->call('Users.isPasswordTrue', array($this->user->id, $curr_pass)) ) {
            $validator->setError('curr_pass', 'Current Password is wrong !');
        }
        $data['curr_pass'] = $curr_pass;


        $new_pass = $req->new_pass;
        if ( ! strlen($new_pass) ) {
            $validator->setError('new_pass', 'New Password is required !');
        }
        else if ( strlen($new_pass) && (strlen($new_pass) < 6 || strlen($new_pass) > 16)  ) {
            $validator->setError('new_pass', 'Password length must be 6 - 16 !');
        }
        $data['new_pass'] = $new_pass;


        $confirm_pass = $req->confirm_pass;
        if ( ! strlen($confirm_pass) ) {
            $validator->setError('confirm_pass', 'Confirm Password is required !');
        }
        else if ( strlen($confirm_pass) && (strlen($confirm_pass) < 6 || strlen($confirm_pass) > 16)  ) {
            $validator->setError('confirm_pass', 'Password length must be 6 - 16 symbols!');
        }
        else if ( strlen($confirm_pass) && strlen($new_pass) ) {
            if ( $new_pass != $confirm_pass )
                $validator->setError('confirm_pass', 'New Password and Confirm Password must be an equal!');
        }
        $data['confirm_pass'] = $confirm_pass;

        return $data;
    }

    public function profileDataAction()
    {
        $client = new Cubix_Api_XmlRpc_Client();

        if ( $this->user )
        {
            $user_data = $client->call('Users.getById', array($this->user->id));

            $this->view->user_data = new Model_UserItem($user_data);
        }

        if ( $this->_request->isPost() )
        {
            $data = array();

            $validator = new Cubix_Validator();

            $data = $this->_validateUserProfile($validator);

            $this->view->user_data = new Model_UserItem($data);

            if ( ! $validator->isValid() ) {
                $status = $validator->getStatus();

                $this->view->errors = $status['msgs'];

                return;
            }

            try {
                $result = $client->call('Users.updateProfile', array($this->user->id, $data));
            }
            catch (Exception $e) {
                var_dump($client->getLastResponse()); die;
            }

            if ( isset($result['error']) ) {
                print_r($result['error']); die;
            }
            else {
                if ( $result !== false ) {
                    $this->view->saved = true;
                    //echo nl2br(print_r($result, true));
                }
            }
        }
    }

    public function _validateUserProfile(Cubix_Validator $validator)
    {
        $req = $this->_request;

        $email = $req->email;
        if ( ! strlen($email) ) {
            $validator->setError('email', 'Email is required !');
        }
        $data['email'] = $email;

        //Location
        $country = $req->country_id;

        if ( ! $country ) {
            $validator->setError('country_id', 'Country is required !');
        }
        $data['country_id'] = $country;


        $city = $req->city_id;
        if ( ! $city ) {
            $city = null;
        }
        $data['city_id'] = $city;

        return $data;
    }

    public function ajaxAddVacationAction()
    {
        $this->view->layout()->disableLayout();

        $client = new Cubix_Api_XmlRpc_Client();

        if ( $this->user )
        {
            if ($this->_request->escort_id)
                $escort_id = $this->_request->escort_id;
            else
                $escort_id = $this->user->getEscort()->getId();
        }


        $data = array();

        $validator = new Cubix_Validator();

        $data = $this->__validateVacationDates($validator);

        if ( ! $validator->isValid() ) {
            $status = $validator->getStatus();

            echo json_encode($status);

            die;
        }

        try {
            $result = $client->call('Escorts.addVacation', array($escort_id, $data));
            $m_escorts = new Model_Escorts();
            $m_escorts->addVacation($escort_id, $data);
        }
        catch (Exception $e) {
            echo json_encode($client->getLastResponse()); die;
        }

        if ( isset($result['error']) ) {
            //print_r($result['error']); die;
        }
        else {
            if ( $result !== false ) {
                $this->view->saved = true;
                //echo nl2br(print_r($result, true));
            }
        }


        die;
    }

    protected function __validateVacationDates(Cubix_Validator $validator)
    {
        $req = $this->_request;

        $date_from = $req->date_from;
        if ( ! $date_from ) {
            //$validator->setError('email', 'Email is required !');
        }
        $data['vac_date_from'] = $date_from;


        $date_to = $req->date_to;
        if ( ! $date_to ) {

        }
        $data['vac_date_to'] = $date_to;

        if ( strtotime($date_from) >= strtotime($date_to) )
        {
            $validator->setError('date_from', 'Invalid Date Interval !');
        }
        else if ( strtotime($date_from) < strtotime(date('Y-m-d')) )
        {
            $validator->setError('date_from', 'Invalid Date Interval !');
        }

        return $data;
    }

    public function ajaxReturnFromVacationAction()
    {
        $this->view->layout()->disableLayout();

        $client = new Cubix_Api_XmlRpc_Client();

        if ( $this->user )
        {
            if ($this->_request->escort_id)
                $escort_id = $this->_request->escort_id;
            else
                $escort_id = $this->user->getEscort()->getId();
        }

        try {
            $result = $client->call('Escorts.removeVacation', array($escort_id));
            $m_escorts = new Model_Escorts();
            $m_escorts->removeVacation($escort_id);

        }
        catch (Exception $e) {
            echo json_encode($client->getLastResponse()); die;
        }

        die;
    }

    public function chooseSalesAction()
    {
        if ( ! is_null($this->_getParam('ajax')) ) {
            $this->view->layout()->disableLayout();
        }

        $client = Cubix_Api_XmlRpc_Client::getInstance();

        if ( $this->_request->isPost() ) {
            $sales_user_id = intval($this->_getParam('sales_person'));
            if ( ! $sales_user_id ) $sales_user_id = null;

            if ( ! is_null($sales_user_id) ) {
                if ( ! $client->call('Users.isSalesValid', array($sales_user_id)) ) {
                    $sales_user_id = null;
                }
            }

            $client->call('Users.assignToSales', array($sales_user_id, $this->user->getId()));

            $this->view->assigend = true;
        }

        $this->_helper->viewRenderer->setScriptAction('sales-chooser');
    }

    public function signInUpAction()
    {
        $this->view->layout()->disableLayout();
    }

    public function changePassAction()
    {
        $client = new Cubix_Api_XmlRpc_Client();
        if ( $this->_request->isPost() ) {
			$username = $this->_getParam('username');
            $pass = $this->_getParam('password');
            $conf_pass = $this->_getParam('password2');
            $hash = $this->_getParam('hash');
            $id = intval($this->_getParam('id'));
            $validator = new Cubix_Validator();
            if ( strlen($pass) < 6 ) {
                $validator->setError('password', __('password_invalid'));
            }
            elseif ( $pass != $conf_pass ) {
                $validator->setError('password2',  __('password_missmatch'));
            }
            elseif ( ! preg_match('/[a-f0-9]{32}/', $hash) ) {
                $validator->setError('hash',  'Invalid hash !');
            }
			elseif ( strtolower($pass) == strtolower($username)) {
				$validator->setError('password', __('username_equal_password'));
			}
            $this->view->errors = array();
            $result = $validator->getStatus();

            if ( ! $validator->isValid() ) {
                $this->view->errors = $result['msgs'];
            }
            else{
				$res = $client->call('Users.checkUpdatePassword', array($id, $hash, $pass));
				if($res === true){
					$client->call('Users.passChangeNotRequiredById', array($id));
				}
                $this->_helper->viewRenderer->setScriptAction('change-pass-success');
            }
        }
        else
        {
            $hash = $this->_getParam('hash');
            $username = $this->_getParam('username');
            $error = false;
            if(!isset($username) || !isset($hash)){
                $error = true;
            }
            $username = substr($username, 0, 24);
            if ( strlen($username) < 1 ) {
                $error = true;
            }
            elseif ( ! preg_match('/^[-_a-z0-9]+$/i', $username) ) {
                $error = true;
            }
            elseif ( ! preg_match('/[a-f0-9]{32}/', $hash) ) {
                $error = true;
            }

            if(!$error)
            {
                $id = $client->call('Users.getByUsernameMailHash', array($username, $hash));
                if($id){
                    $this->view->id = $id;
                    $this->view->hash = $hash;

                }
                else{
                    $error = true;
                }

            }
            if ($error){
                $this->_response->setRawHeader('HTTP/1.1 404 Not Found');
                $this->_forward('error', 'error', null, array('error_msg' => 'There has been an error. Your hash is expired or invalid please try again or contact support'));
            }
        }
    }

    public function sendActivationCodeAction(){
        $this->view->layout()->disableLayout();

        $userType = $this->_getParam('userType');
        $phone = $this->_getParam('phone');
        $phone_prefix = $this->_getParam('phone_prefix');

        $result = array();
        $result['status'] = 0;

        if (!in_array($userType, array('escort', 'agency'))){
            die();
        }
        if(empty($phone_prefix)){
            $result['type'] = 'phone_prefix';
            $result['message'] = Cubix_I18n::translate('required_fields');
            die(json_encode($result));
        }

        if(empty($phone)){
            $result['type'] = 'phone';
            $result['message'] = Cubix_I18n::translate('required_fields');
            die(json_encode($result));
        }

        $model = new Model_EscortsV2();

        $res = $model->sendActivationCode($userType,$phone,$phone_prefix);
        die(json_encode($res));
    }
    public function checkActivationCodeAction(){
        $this->view->layout()->disableLayout();

        $username = $this->_getParam('username');
        $code = $this->_getParam('code');

        $result = array();
        $result['status'] = 0;

        if(empty($username)){
            $result['type'] = 'username';
            $result['message'] = Cubix_I18n::translate('required_fields');
            die(json_encode($result));
        }

        if(empty($code)){
            $result['type'] = 'code';
            $result['message'] = Cubix_I18n::translate('required_fields');
            die(json_encode($result));
        }

        $model = new Model_EscortsV2();

        $res = $model->checkActivationCode($username,$code);

        if($res['status'] == 0){
            if($res['error_code'] == "user_not_exist"){
                $result['type'] = 'username';
                $result['message'] = Cubix_I18n::translate('username_not_registered');
                die(json_encode($result));
            }
            elseif($res['error_code'] == "wrong_code")
            {
                $result['type'] = 'code';
                $result['message'] = Cubix_I18n::translate('forgot_wrong_password');
                die(json_encode($result));

            }
            elseif($res['error_code'] == "code_expired")
            {
                $result['type'] = 'code';
                $result['message'] = Cubix_I18n::translate('forgot_code_expired');
                die(json_encode($result));
            }
        }

        $result['status'] = 1;
        die(json_encode($result));
    }
    public function forceChangePassAction(){
        $this->view->layout()->disableLayout();

        $username = $this->_getParam('username');
        $code = $this->_getParam('code');
        $newPass = $this->_getParam('password');
        $newPass2 = $this->_getParam('password2');

        $result = array();
        $result['status'] = 0;


        if(!preg_match('/(?=^.{6,16}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/', $newPass)){
            $result['type'] = 'password';
            $result['message'] = Cubix_I18n::translate('password_contain');
        }

        if ( strtolower($username) == strtolower($newPass)) {
            $result['type'] = 'password';
            $result['message'] = Cubix_I18n::translate('username_equal_password');
            die(json_encode($result));
        }

        if($newPass != $newPass2){
            $result['type'] = 'password';
            $result['message'] = Cubix_I18n::translate('password_missmatch');
            die(json_encode($result));
        }


        $client = new Cubix_Api_XmlRpc_Client();
        $result = $client->call('Escorts.forceChangePassword', array($username,$code,$newPass));

        if(!$result){
            $result['type'] = 'code';
            die(json_encode($result));
        }else{
            $result = $client->call('Users.passChangeNotRequired', array($username));
            $result = $this->signinAction();
            $result['status'] = 1;
            die(json_encode($result));
        }
    }

    public function signupAlertAction()
    {
        $this->view->layout()->disableLayout();
        $type = $this->_request->type;

        $this->view->type = ($type == 'vip-member') ? 'member' : $type;
    }


    private function _hasStatusBit($old_status, $status)
    {
        $result = true;
        $result = ($old_status & $status) && $result;
        return $result;
    }
}
