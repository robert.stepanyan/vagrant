<?php

class Mobilev2_RedirectController extends Zend_Controller_Action
{
	private $host = '';
	
	public function init()
	{
		$host = $_SERVER['HTTP_HOST'];
		if ( $host !== 'www.escortforumit.xxx' ) {
			$host = APP_HTTP.'://www.escortforumit.xxx';
			$this->host = $host;
		}
	}

	public function indexAction()
	{
		$lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang');

		if ( ! $lang )
		{
			$lang = 'it';
		}

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . "/{$lang}"  . $_SERVER['REQUEST_URI']);
		exit;
	}

	public function mainPageAction()
	{
		$lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang');

		if ( ! $lang )
		{
			//$lang = 'it';
			$lang = '';
		}

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . "/{$lang}");
		exit;
	}

	public function reviewsAction()
	{
		$lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang');

		if ( ! $lang )
		{
			$lang = 'it';
		}

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . "/{$lang}/recensioni");
		exit;
	}

	/*public function reviewsEscortAction()
	{
		$lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang');
		$showname = Zend_Controller_Front::getInstance()->getRequest()->getParam('showname');
		$page = intval(Zend_Controller_Front::getInstance()->getRequest()->getParam('page'));

		if ( ! $lang )
		{
			$lang = 'it';
		}

		$url = $this->host . "/{$lang}/recensioni/{$showname}";
//var_dump($page); die;
		if ( $page ) {
			$url .= '?page=' . $page;
		}

		header("HTTP/1.1 301 Moved Permanently");
		header("Location: {$url}");
		exit;
	}*/
	
	public function reviewsEscortAction()
	{
		$lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang');
		$showname = Zend_Controller_Front::getInstance()->getRequest()->getParam('showname');
		$page = intval(Zend_Controller_Front::getInstance()->getRequest()->getParam('page'));

		if ( ! $lang )
		{
			$lang = 'it';
		}

		$model = new Model_EscortsV2();
		$showname = trim($showname, '-');
		$id = $model->getIdByShowname($showname)->id;
		
		if ($id)
		{
			$url = $this->host . "/{$lang}/recensioni/{$showname}-{$id}";
			
			if ( $page ) {
				$url .= '?page=' . $page;
			}
		}
		else
		{
			$url = $this->host . "/";
		}
		
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: {$url}");
		exit;
	}

	public function escortAction()
	{
		$lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang');
		$showname = Zend_Controller_Front::getInstance()->getRequest()->getParam('showname');
		$p = Zend_Controller_Front::getInstance()->getRequest()->getParam('p');

		if ( ! $lang )
		{
			$lang = 'it';
		}

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . "/{$lang}/accompagnatrici/{$showname}");
		exit;
	}
		
	public function escort2Action()
	{
		$showname = Zend_Controller_Front::getInstance()->getRequest()->getParam('showname');
				
		$model = new Model_EscortsV2();
		$showname = trim($showname, '-');
		$id = $model->getIdByShowname($showname)->id;

		if ($id)
		{
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: ' . $this->host . "/accompagnatrici/{$showname}-{$id}");
		}
		else
		{
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: ' . $this->host . "/");
		}
		
		exit;
	}

	public function regionsAction()
	{
		$req = Zend_Controller_Front::getInstance()->getRequest()->getParam('req');

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . '/it/escorts/region_it_' . str_replace('_', '-', $req));
		exit;
	}

	public function geoAction()
	{
		$req = Zend_Controller_Front::getInstance()->getRequest();

		$lang = $req->getParam('lang');
		$type = $req->getParam('type');
		$geo  = $req->getParam('geo');
		$geoval = $req->getParam('geoval');
		$filter = $req->getParam('filter');
		$page = $req->getParam('page');

		$url = $this->host;
		if ( $lang ) {
			$url .= '/' . $lang;
		}

		$url .= '/escorts';

		switch ( $type ) {
			case 'nuove' :
				$type = 'nuove';
			break;
			case 'independantes' :
				$type = 'independantes';
			break;
			case 'agence' :
				$type = 'agence';
			break;
			case 'boys' :
				$type = 'boys';
			break;
			case 'trans' :
				$type = 'trans';
			break;
			case 'tours' :
				$type = 'tours';
			break;
			case 'upcomingtours' :
				$type = 'upcomingtours';
			break;
			default: $type = '';
		}

		if ( strlen($type) ) {
			$url .= '/' . $type;
		}

		if ( $geoval ) {
			$geoval = str_replace('_', '-', $geoval);
			$url .= '/' . $geo . '_it_' . $geoval;
		}

		if ( ! strlen($type) && ! $geoval ) {
			$url .= '/regular';
		}

		if ( $filter ) {
			switch ( $filter ) {
				case 'incall':
				case 'outcall':
					$filter = 'filter_available-for-' . $filter;
					break;
			}
			
			if ( strlen($filter) ) {
				$url .= '/' . $filter;
			}
		}

		$page = intval($page);
		if ( $page > 0 ) {
			//$url .= '/page_' . ($page + 1);
			$url .= '/' . $page;
		}
		
		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $url);
		exit;
	}

	public function citiesAction()
	{
		$req = Zend_Controller_Front::getInstance()->getRequest();

		$city = $req->getParam('city');
		$lang = $req->getParam('lang');
		$type = $req->getParam('type');

		$type .= ($type) ? '/' : '';

		if ( ! $lang ) {
			$lang = 'it';
		}

		$city_slug = str_replace('_', '-', $city);

		$model = new Cubix_Geography_Cities();
		$city = $model->getBySlug($city_slug);

		$r_model = new Cubix_Geography_Regions();
		$region = $r_model->get($city->region_id);
		
		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . "/{$lang}/escorts/{$type}region_it_" . $region->slug . '/city_' . $city_slug);
		exit;
	}

	public function agencyAction()
	{
		//$req = Zend_Controller_Front::getInstance()->getRequest()->getParam('agencyId');
		$url = explode('=', $_SERVER['REQUEST_URI']);
		$lng_id = explode('&', $url[1]);
		$agency_id = $url[2];

		$client = new Cubix_Api_XmlRpc_Client();
		$agency = $client->call('Agencies.getByOldId', array($agency_id));

		header("HTTP/1.1 301 Moved Permanently");

		if ( ! $agency ) {
			header("Location: ' . $this->host . '/{$lng_id[0]}");
		}
		else {
			header('Location: ' . $this->host . "/{$lng_id[0]}/agency/" . $agency['slug']);
		}
		exit;
	}
	
	public function agencies2Action()
	{
		$lang = Zend_Controller_Front::getInstance()->getRequest()->getParam('lang_id');
		$agencyName = Zend_Controller_Front::getInstance()->getRequest()->getParam('agencyName');
		$req = Zend_Controller_Front::getInstance()->getRequest()->getParam('req');
		
		if ( ! $lang )
		{
			$lang = 'it';
		}
		
		$model = new Model_Agencies();
		$agencyName = trim($agencyName, '-');
		$ag = $model->getIdByName($agencyName);
		$id = $ag['id'];
		
		if (!$req)
			$req = '';

		if ($id)
		{
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: ' . $this->host . "/{$lang}/agency/{$agencyName}-{$id}" . $req);
		}
		else
		{
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: ' . $this->host . "/");
		}
		
		exit;
	}

	public function directoryAction()
	{
		$req = Zend_Controller_Front::getInstance()->getRequest();
		$lang = $req->getParam('lang_id');

		$url = $this->host;
		
		if ( $lang == 'en' ) {
			$url .= '/' . $lang;
		}
		else {
			$url .= '/';
		}

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $url);
		exit;
	}

	public function loginAction()
	{
		$req = Zend_Controller_Front::getInstance()->getRequest();
		$lang = $req->getParam('lng');

		$url = $this->host;

		if ( $lang == 'en' ) {
			$url .= '/' . $lang . '/';
		}
		else {
			$url .= '/';
		}

		$url .= 'private/signin';

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $url);
		exit;
	}

	public function escortListAction()
	{
		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . '/');
		exit;
	}

	public function blankHtmlAction()
	{
//		die(preg_replace('#/blank\.html\??#', '/', $this->_request->getRequestUri()));
//		$this->_request->setRequestUri(preg_replace('#/blank\.html\??#', '', $this->_request->getRequestUri()));
//		$this->getFrontController()->dispatch();
//		exit;

		/*header('HTTP/1.1 301 Moved Permanently');
		header('Location: ' . preg_replace('#/blank\.html#', '', $this->_request->getRequestUri()));
		exit;*/
	}

	public function blankAction()
	{
		$uri = preg_replace('#/blank.html(.+)#', '', $_SERVER['REQUEST_URI']);

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . $uri);
		exit;
	}
	
	public function langEscortAction()
	{
		$req = Zend_Controller_Front::getInstance()->getRequest();
		$lang = $req->getParam('lang');
		
		$uri = preg_replace('#/' . $lang . '#', '', $_SERVER['REQUEST_URI']);

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . $uri);
		exit;
	}
	
	public function onlineChatCityAction()
	{
		$req = Zend_Controller_Front::getInstance()->getRequest();
		$city = $req->getParam('city');
		
		$uri = '/escorts/city_' . $city;

		header("HTTP/1.1 301 Moved Permanently");
		header('Location: ' . $this->host . $uri);
		exit;
	}
}
