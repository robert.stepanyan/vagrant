<?php

class Zend_View_Helper_GetLink
{
	protected static $_params;

	public function getLink($type = '', array $args = array(), $clean = false, $no_args = false)
	{
		$lang_id = Cubix_I18n::getLang();

		$link = '/';
		if ($lang_id != Cubix_Application::getDefaultLang()) {
			$link .= $lang_id . '/';
		}
//		$link .= "mobile/";
		if (isset($args['state']) && 7 != Cubix_Application::getId()) {
			$args['region'] = $args['state'];
			unset($args['state']);
		}

		switch ($type) {
            case 'how-it-works-adv':
				$link .= 'page/how-it-works-adv';
            break;
            case 'how-it-works':
                $link .= 'page/how-it-works';
                break;
            case 'cookie-policy':
                $link .= 'page/cookie-policy';
                break;
            case 'valentines-contest':
                $link .= 'page/valentines-contest';
                break;
            case 'privacy-policy':
            $link .= 'page/privacy-policy';
            break;
                
//            case '4myfans':
//                $link = 'https://www.4myfans.ch?utm_source=efitescortpa&amp;utm_medium=efitescortpa&amp;utm_campaign=efitescortpa';
//                break;
			case 'virtual-escorts':
				$link .= 'escorts/virtual';
				break;
            case 'sugar-dating':
				$link = 'https://offers.refchamp.com/?offer=6&uid=03112eb6-8935-4bc9-9591-1f16e8758836';
				break;
			case 'cams':
				/*$user = Model_Users::getCurrent();
				if($user->sid){
					$link = 'https://m.escortforumcams.com/cam-'.$user->sid;
				}
				else{
					$link .= 'ef-cams';
				}*/
				$link .= 'ef-cams';
				break;
			case 'search':
				$link .= 'search';
				break;
			case 'escorts-list':
				$link .= 'escorts';
				break;
            case 'trans-list':
                $link .= 'escorts/trans';
                break;
            case 'boys-list':
                $link .= 'escorts/boys';
                break;
			case 'newest-escorts':
				$link .= 'escorts/nuove';
				break;
            case 'valentine':
                $link .= 'escorts/valentine';
                break;
			case 'escorts-instantbook';
				$link .= 'escorts/instant-book';
				break;
			case 'video-list':
				$link .= 'video-list';
				break;
            case 'private-v2-profile-location':
                $link .= 'private-v2/profile?step=contact-info#to-map';
                break;
			case 'city-tours':
				$link .= 'escorts/citytours';
				break;
			case 'citytours':
				$link .= 'escorts/citytours';
				break;
			case 'upcomingtours':
				$link .= 'escorts/upcomingtours';
				break;
			case 'base-city':
				$section = isset($args['section']) ? $args['section'] . '/' : '';
				if (strlen($section)) unset($args['section']);

				$link .= 'escorts/' . $section . 'city_' . $args['city'];
				unset($args['city']);
				break;
			case 'escorts':
				if (empty(self::$_params)) {
					$request = Zend_Controller_Front::getInstance()->getRequest();

					$req = trim($request->getParam('req', ''), '/');
					$req = explode('/', $req);

					$params = array();

					foreach ($req as $r) {
						$param = explode('_', $r);
						if (count($param) < 2) {
							$params[] = $r;
							continue;
						}

						$param_name = reset($param);
						array_shift($param);

						$params[$param_name] = implode('_', $param);
					}

					self::$_params = $params;
				}

				if ($clean) {
					$params = array();
				} else {
					$params = self::$_params;
				}

				$link .= 'escorts/';
				foreach ($args as $param => $value) {



					if (!strlen($param) || !strlen($value)) continue;
					if (is_int($param)) {
						if ($value == 'tours') $value = 'citytours';
						$link .= $value . '/';
					} elseif (strlen($param)) {
						if ($param == 'page') {
							if ($value - 1 > 0) {
								$link .= ($value - 1) . '/';
							}
						} else {
							$link .= $param . '_' . $value . '/';
						}
					}
				}

				$link = rtrim($link, '/');

				$args = array();
				break;
			case 'gotm':
				$link .= 'girl-of-month';

				if (isset($args['history']) && $args['history']) {
					$link .= '/history';
				}

				if (isset($args['page'])) {
					if ($args['page'] != 1) {
						$link .= '/page_' . $args['page'];
						//$link .= '/page_' . $args['page'];
					}

					unset($args['page']);
				}
				break;
			case 'profile':
				$link .= 'escort/';
				if (isset($args['type']) && $args['type'] != 'bio') {
					$link .= $args['type'] . '/';
				}
				$link .= $args['showname'] . '-' . $args['escort_id'];
				unset($args['showname']);
				unset($args['type']);
				unset($args['escort_id']);
				break;
			case 'profile-hash':
				$link .= 'escort/';
				$link .= $args['showname'] . '-' . $args['escort_id'] . '#' . $args['type'];
				unset($args['showname']);
				unset($args['escort_id']);
				unset($args['type']);
				break;
			case 'forgot':
				$link .= 'private/forgot';
				break;
			case 'password_change_required':
				$link .= 'private/password-change-required';
			break;
			case 'signup':
				$link .= 'private/signup';

				break;
			case 'signup-escort':
				$link .= 'private/signup-escort';
				break;
            case 'signup-agency':
                $link .= 'private/signup-agency';
                break;
			case 'signup-member':
				$link .= 'private/signup-member';
				break;
			case 'signin':
				$link .= 'private/signin';
				break;
			case 'signout':
				$link .= 'private/signout';
				break;
				break;
			case 'private':
				$link .= 'private';
				break;
			case 'membership-type':
				$link .= 'private/membership-type';
				break;
			case 'feedback':
				$link .= 'feedback';
				break;
			case 'contact':
				$link .= 'contact';
				break;
			case 'add-review-comment':
				$link .= 'private-v2/add-review-comment';
				break;
			case 'confirm-phone':
				$link .= 'private-v2/confirm-phone';
			break;
			case 'private-v2-dash-instant-book':
				$link .= 'private-v2/instant-book';
				break;
			case 'comments-ajax':
				$link .= 'comments/ajax-list';
				break;
			case 'captcha':
				$link .= 'captcha?' . rand();
				//$link = '/img/sample_captcha.gif?' . rand();
				break;
			case 'viewed-escorts':
				$link .= 'escorts/viewed-escorts';
				break;
			case '100p-verify':
				$link .= 'private/verify';
				break;
			case '100p-verify-webcam':
				$link .= 'private/verify/webcam';
				break;
			case '100p-verify-idcard':
				$link .= 'private/verify/idcard';
				break;
			case 'agency':
				$link .= 'agency/' . $args['slug'];
				unset($args['slug']);
				break;

			case 'private-v2-support':
				$link .= 'support';
				break;
			case 'private-v2-instant-book-history':
				$link .= 'private-v2/instant-book-history';
				break;
			case 'favorites':
				$link .= 'favorites';
				break;
			case 'add-to-favorites':
				$link .= 'add-to-favorites';
				break;
			case 'ajax-favorites':
				$link .= 'ajax-favorites';
				break;
			case 'add-to-top':
				$link .= 'ajax-favorites-add-to-top';
				break;
			case 'remove-from-top':
				$link .= 'ajax-favorites-remove-from-top';
				break;
			case 'add-fav-comment':
				$link .= 'add-fav-comment';
				break;

			case 'top10-list':
				$link .= 'top10-list';
				break;
			case 'ajax-top10':
				$link .= 'ajax-top10';
				break;

			case 'm-agency-esc-profile':
				$link .= 'private-v2/escort-profile';
				break;
			case 'mob-move-package':
				$link .= 'private-v2/escort-active-package';
				break;

			#................................> Boost
			case 'ob-profile-boost':
				$link .= 'profile-boost';
				break;
			case 'gotd-boost':
				$link .= 'profile-boost/gotd-boost';
				break;
			case 'ob-profile-boost-select-escort':
				$link .= 'profile-bumps/buy-bumps';
				break;
			case 'ob-profile-boost-select-city':
				$link .= 'profile-bumps/buy-bumps';
				break;
			case 'ob-profile-boost-select-hour':
				$link .= 'profile-bumps/buy-bumps';
				break;
			case 'ob-profile-boost-confirmation':
				$link .= 'profile-bumps/buy-bumps';
				break;
			#................................. <<<<<

            #................................> Bump
            case 'ob-profile-bump-buy':
                $link .= 'profile-bumps/buy-bumps';
                break;
            case 'ob-profile-bump':
                $link .= 'profile-bumps';
                break;
            case 'ob-profile-bump-checkout':
                $link .= 'profile-bumps/checkout';
                break;
            #............Agency.............>
            case 'ob-profile-bump-buy-agency':
                $link .= 'profile-bumps/buy-bumps-agency';
                break;
            case 'ob-profile-bump-agency-escort-edit':
                $link .= 'profile-bumps/agency-escort-edit';
                break;
            case 'ob-profile-bump-agency-escort-buy-bump':
                $link .= 'profile-bumps/agency-escort-buy-bump';
                break;
            #................................. <<<<<

			case 'remove-from-favorites':
				$link .= 'remove-from-favorites';
				break;
			case 'mob-remove-from-favorites':
				$link .= 'private/remove-from-favorites';
				break;
			case 'mob-add-to-favorites':
				$link .= 'private/add-to-favorites';
				break;
			case 'online-billing-v2':
				$link .= 'online-billing-v2';
				break;
			case 'online-billing-v2-wizard':
				$link .= 'online-billing-v2/';
				if ($args['user_type'] == USER_TYPE_AGENCY) {
					$link .= 'wizard-agency';
				} else {
					$link .= 'wizard-independent';
				}
				unset($args['user_type']);
				break;
			case 'online-billing-v2-remove-cart-item':
				$link .= 'online-billing-v2/remove-from-cart';
				break;
			case 'mob-online-billing':
				$link .= 'online-billing-v2/?escort_id=' . $args['escort_id'];
				unset($args['escort_id']);
				break;
			case 'mob-agency-escorts-list':
				$link .= 'private-v2/agency-escorts-list';
				break;
			case 'mob-gotd':
				$link .= 'private-v2/profile/gotd';
				break;
			case 'private-v2-gotd':
				$link .= 'private-v2/profile/gotd';
				break;
			case 'mob-agency-shopping-cart':
				$link .= 'online-billing-v2/agency-cart';
				break;
			case 'ob-successful-v2':
				$link .= 'online-billing-v2/successful-payment';
				break;
			case 'ob-unsuccessful-v2':
				$link .= 'online-billing-v2/unsuccessful-payment';
				break;
			case 'ob-successful-gotd':
				$link .= 'online-billing-v2/successful-payment-gotd';
				break;
			case 'ob-successful-profile-booster':
				$link .= 'online-billing-v2/successful-payment-profile-booster';
				break;
			case 'ob-mmg-postback':
				$link .= 'online-billing-v2/mmg-postback';
				break;
			// Private Area
			case 'edit-profile':
				$link .= 'private/profile';
				break;
			case 'edit-agency-profile':
				$link .= 'private/agency-profile';
				break;
			case 'private-member-profile':
				$link .= 'private/profile';
				break;

			case 'profile-data':
				$link .= 'private/profile-data';
				break;
			case 'edit-photos':
				$link .= 'private/photos';
				break;
			case 'private-reviews':
				$link .= 'private/reviews';
				break;
			case 'private-settings':
				$link .= 'private/settings';
				break;

			case 'payment-type-independent':
				$link .= 'online-billing-v2/wizard-independent';
				break;
			case 'payment-type-agency':
				$link .= 'online-billing-v2/wizard-agency';
				break;
			case 'edit-rates':
				$link .= 'private/rates';
				break;
			case 'edit-escorts':
				$link .= 'private/escorts';
				break;
			case 'delete-escort':
				$link .= 'private/delete-escort';
				break;
			case 'edit-tours':
				$link .= 'private/tours';
				break;
			case 'edit-tour':
				$link .= 'private-v2/edit-tour';
				break;
			case 'remove-tour':
				$link .= 'private-v2/remove-tours';
				break;
			case 'change-passwd':
				$link .= 'private/change-password';
				break;
			case 'private-upgrade-premium':
				$link .= 'private/upgrade';
				break;

			case 'search':
				$link .= 'search';
				break;

			case 'gps-location':
				$link .= 'gps-location';
				break;
			case 'late-night-girls':
				$link .= 'escorts/late-night-girls';
				break;

			case 'terms':
				$link .= 'page/terms-and-conditions';
				break;
			case 'webcam-lm':
				$link .= 'page/webcam-verification-learn-more';
				break;
			case 'passport-lm':
				$link .= 'page/passport-verification-learn-more';
				break;

			case 'external-link':
				$params = '';
				if (isset($args['user_type'])) {
					$params .= '&user_type=' . $args['user_type'];
					unset($args['user_type']);
				}
				if (isset($args['id'])) {
					$params .= '&id=' . $args['id'];
					unset($args['id']);
				}

				$link = '/go?link=' . $args['link'] . $params;
				unset($args['link']);
				break;
			// V2 Private Area
			case 'private-v2':
				$link .= 'private-v2';
				break;
			case 'alerts':
				$link .= 'private-v2/alerts';
				break;
			case 'private-v2-settings':
				$link .= 'private-v2/settings';
				break;
			case 'private-v2-happy-hour':
				$link .= 'private-v2/happy-hour';
				break;
			case 'private-v2-client-blacklist':
				$link .= 'private-v2/client-blacklist';
				break;
			case 'private-v2-add-client-to-blacklist':
				$link .= 'private-v2/add-client-to-blacklist';
				break;
			case 'private-v2-profile':
				$link .= 'private-v2/profile';
				break;
			case 'private-photo-video-main':
				$link .= 'photo-video/gallery-main';
				break;
			case 'private-v2-video':
				$link .= 'photo-video/ajax-video';
				break;
			case 'private-v2-natural-pic':
				$link .= 'photo-video/ajax-natural-pic';
				break;
			case 'private-v2-cover-pic':
				$link .= 'photo-video/ajax-cover-pic';
				break;	
			case 'private-v2-gallery':
				$link .= 'photo-video/ajax-gallery';
				break;
			case 'private-v2-get-gallery':
				$link .= 'photo-video/ajax-get-gallery';
				break;
			case 'private-v2-video-action':
				$link .= 'photo-video/video';
				break;
			case 'private-v2-natural-pic-action':
				$link .= 'photo-video/natural-pic';
				break;
			case 'private-v2-cover-pic-action':
				$link .= 'photo-video/cover-pic';
				break;	
			case 'escort-age-verification':
				$link .= 'private-v2/escort-age-verification';
				break;
            case 'escort-age-verification-ajax':
                $link .= 'private-v2/escort-age-verification-ajax';
                break;
            case 'process-age-verification-files':
                $link .= 'private-v2/process-age-verification-files';
                break;
            case 'process-age-idcard-files':
                $link .= 'private/verify/process-idcard-files';
                break;
            case 'idcard-ajax':
                $link .= 'private/verify/id-card-ajax';
                break;
			/********** Status pages **********/

			case 'private-v2-status-has-no-package':
				$link .= 'private-v2/has-no-package';
				break;
			case 'private-v2-status-not-age-certified':
				$link .= 'private-v2/not-age-certified';
				break;
			case 'private-v2-status-owner-disabled':
				$link .= 'private-v2/owner-disabled';
				break;
			case 'private-v2-status-not-completed':
				$link .= 'private-v2/not-completed';
				break;
			case 'private-v2-status-enough-pics':
				$link .= 'private-v2/enough-pics';
				break;

			case 'private-v2-agency-escorts-for':
				$link .= 'private-v2/agency-escorts';
				break;

			/************** End *************/
			case 'show-video':
				$link .= 'show-video';
				break;

			case 'escort-photo':
				$link .= 'escort/photo';
				break;
			case 'private-v2-plain-photos':
				$link .= 'private-v2/plain-photos';
				break;
			case 'private-v2-tours':
				$link .= 'private-v2/tours';
				break;
			case 'private-v2-tours-edit':
				$link .= 'private-v2/edit-tour';
				break;
			case 'private-v2-ajax-tours':
				$link .= 'private-v2/ajax-tours';
				break;
			case 'private-v2-ajax-tours-add':
				$link .= 'private-v2/ajax-tours-add';
				break;
			case 'private-v2-ajax-tours-remove':
				$link .= 'private-v2/ajax-tours-remove';
				break;
			case 'private-v2-verify':
				$link .= 'private-v2/verify';
				break;
			case 'private-v2-premium':
				$link .= 'private-v2/premium';
				break;
			case 'private-support':
				$link .= 'support';
				break;
			case 'dashboard':
				$link .= 'private-v2';
				break;
			case 'private-support-add-ticket':
				$link .= 'support/add-ticket';
				break;
			case 'private-messaging':
				$link .= 'private-messaging';
				break;
			case 'pv-thread':
				$link .= 'private-messaging/thread?id=' . $args['id'];
				if ($args['escort_id']) {
					$link .= '&escort_id=' . $args['escort_id'];
					unset($args['escort_id']);
				}
				if ($args['page']) {
					$link .= '&page=' . $args['page'];
					unset($args['page']);
				}
				unset($args['id']);
				break;
			case 'new-thread':
				$link .= 'private-messaging/new-thread';
				break;
			case 'pm-send-message':
				$link .= 'private-messaging/send-message';
				break;

			case 'private-v2-agency-profile':
				$link .= 'private-v2/agency-profile';
				break;
			case 'private-v2-member-profile':
				$link .= 'private-v2/member-profile';
				break;
			case 'private-v2-upgrade-premium':
				$link .= 'private-v2/upgrade';
				break;
			case 'ticket-open':
				$link .= 'support/ticket';
				break;
			case 'private-v2-escorts':
				$link .= 'private-v2/escorts';
				break;
			case 'private-v2-escorts-delete':
				$link .= 'private-v2/profile-delete';
				break;
			case 'reviews':
				$link .= 'evaluations';
				break;
			case 'escort-reviews':
				$link .= 'evaluations/' . $args['showname'] . '-' . $args['escort_id'];
				unset($args['showname']);
				unset($args['escort_id']);
				break;
			case 'member-reviews':
				$link .= 'evaluations/member/' . $args['username'];
				unset($args['username']);
				break;
			case 'escorts-reviews':
				$link .= 'evaluations/escorts';
				break;
			case 'agencies-reviews':
				$link .= 'evaluations/agencies';
				break;
			case 'top-reviewers':
				$link .= 'evaluations/top-reviewers';
				break;
			case 'top-ladies':
				$link .= 'evaluations/top-ladies';
				break;
			case 'voting-widget':
				$link .= 'escort/' . $args['showname'] . '-' . $args['escort_id'] . '/vote';
				unset($args['showname']);
				unset($args['escort_id']);
				break;
			case 'private-v2-reviews':
				$link .= 'private-v2/reviews';
				break;
			case 'private-v2-escort-reviews':
				$link .= 'private-v2/escort-reviews';
				break;
			case 'private-v2-escorts-reviews':
				$link .= 'private-v2/escorts-reviews';
				break;
			case 'private-v2-add-review':
				$link .= 'private-v2/add-review';
				break;
			case 'add-review':
				$link .= 'private-v2/add-review/?escort_id=' . $args['escort_id'];
				unset($args['escort_id']);
				break;
			case 'reviews-search':
				$link .= 'evaluations/search';
				break;
			case 'comments':
				$link .= 'comments';
				break;
			case 'morecities':
				$link .= 'allcities';
				break;
			case 'pager':
				$uri = $args['request_url'];
				$uri = preg_replace('#mobile/#', '', $uri);
				$link .= $uri . $args['page'];
				return $link;
				break;
			case 'paycash-response':
				$link = APP_HTTP . '://m.escortforumit.xxx/online-billing-v2/powercash-response';
				break;
            case 'powercash-response':
                $link = APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/payment/powercash-response';
                break;
			case 'ecardon-response':
				$link = APP_HTTP . '://m.escortforumit.xxx/online-billing-v2/ecardon-response';
				break;
			case 'ef-cams-buy-tokens':
				$link .= 'ef-cams/buy-tokens';
				break;
			case 'ef-cams-ecardon-response':
				$link .= 'ef-cams/ecardon-response';
				break;
			case 'ef-cams-escort-dash':
				$link .= 'ef-cams/broadcaster/dashboard';
			break;	
			case 'ef-cams':
				$link = 'https://escortforumit.escortcams.com';
			break;
			case 'ef-cams-member':
				$link .= 'ef-cams'; 
			break;
			case 'ef-cams-broadcaster':
				$encoded_showname = strtolower(str_replace(' ', '-', $args['showname']));
				$link = 'https://escortforumit.escortcams.com/ef-cams/channel/' . $encoded_showname.'-'.$args['escort_id'];
				unset($args['showname']);
				unset($args['escort_id']);
			break;	
			case 'cams-login-redirect':
				$link = 'https://escortforumit.escortcams.com';
				if ($args['path']) {
					$link .= $args['path'];
				}
				unset($args['path']);
			break;
			case 'cams-booking-redirect':
				$link = 'https://escortforumit.escortcams.com/escort/';
				$link .= $args['escort_id'];
				$link .='/channel';
				unset($args['escort_id']);
			break;
			case 'trim':
				$uri = $args['request_url'];
//                $uri = preg_replace('#en/mobile/#', '', $uri);
				$uri = preg_replace('#mobile/#', '', $uri);

				return $uri;
				break;
			case 'instant-book-set-available':
				$link .= 'instant-book/set-available';
				break;
			case 'video-chat-success':
				$link .= 'escorts/video-chat-success';
				
		}

		if (!$no_args) {
			if (count($args)) {
				$link .= '?';
				$params = array();
				foreach ($args as $arg => $value) {
					if (!is_array($value)) {
						$params[] = $arg . '=' . urlencode($value);
					} else {
						foreach ($value as $v) {
							$params[] = $arg . '[]=' . $v;
						}
					}
				}

				$link .= implode('&', $params);
			}
		}

		return $link;
	}
}
