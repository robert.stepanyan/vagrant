<?php

class Zend_View_Helper_SliceString extends Zend_View_Helper_Abstract
{
    public function sliceString( $str, $len, $end_line = true, $end_line_str = '...' ){
        return substr( strip_tags( $str ), 0, $len ) . ( ( $end_line ) ? $end_line_str : '' );
    }
}