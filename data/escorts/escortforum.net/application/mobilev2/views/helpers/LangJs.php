<?php

class Zend_View_Helper_LangJs
{
	public function langJs()
	{
		$lng = '';

		if( Cubix_I18n::getLang() != Cubix_Application::getDefaultLang() ){
			$lng = '/' . Cubix_I18n::getLang();
		}

		return $lng;
	}
}
