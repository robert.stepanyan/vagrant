<?php

class Zend_View_Helper_TabsCreate extends Zend_View_Helper_Abstract
{
	public function tabsCreate($content, $mode)
	{
		$this->view->content = $content;
		$this->view->mode = $mode;
		return $this->view->render('mobile/tabs-create.phtml');
	}
}
