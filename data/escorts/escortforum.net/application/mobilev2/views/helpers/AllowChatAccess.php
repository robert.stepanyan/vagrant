<?php

class Zend_View_Helper_AllowChatAccess
{
    public function allowChatAccess()
    {
        $user = Model_Users::getCurrent();
        $has_active_package = false;
        $is_susp = false;

        if ( $user->user_type == 'escort' ) {
            $client = new Cubix_Api_XmlRpc_Client();
            $has_active_package = $client->call('Escorts.hasPaidActivePackageForEscort', array($user->id));
            $m_e = new Model_EscortsV2();
            $is_susp = $m_e->isSuspicious($user->id);
        }

        $user->has_active_package = $has_active_package;
        $user->is_susp = $is_susp;

        $chat_info = array(
            'nickName'	=> $user->username,
            'userId'	=> $user->id,
            'userType'	=> $user->user_type,
            'imIsBlocked' => $user->im_is_blocked
        );

        if ( $user->user_type == 'escort' ) {
            $m = new Model_EscortsV2();
            $e = $m->getByUserId($user->id);
            $chat_info['nickName'] = $e->showname . ' (Escort)';
            $chat_info['isSusp'] = $is_susp;
            $chat_info['hasActivePackage'] = $has_active_package;
        } elseif ( $user->user_type == 'agency' ) {
            $client = new Cubix_Api_XmlRpc_Client();
            $has_active_package = $client->call('Escorts.hasPaidActivePackageForAgency', array($user->id));

            $m = new Model_Agencies();
            $a = $m->getByUserId($user->id);
            $chat_info['nickName'] = $a->name;
            $chat_info['hasActivePackage'] = $has_active_package;
        }

        $auth = false;
        $data_access = array();

        if ( $chat_info ) {
            if ( $user->user_type == 'member' ) { // Member anyway login
                if( $chat_info['imIsBlocked'] ){
                    $data_access['message'] = 'Your account was blocked !';
                } else {
                    $auth = true;
                }
            } elseif ( $user->user_type == 'escort' ) { // Escort require is not suspicious and has active package
                if( !$chat_info['hasActivePackage'] ){
                    $data_access['message'] = 'You do not have an active package !';
                } elseif( $chat_info['isSusp'] ){
                    $data_access['message'] = 'Your account was suspicious !';
                } elseif( $chat_info['imIsBlocked'] ){
                    $data_access['message'] = 'Your account was blocked !';
                } else {
                    $auth = true;
                }
            } elseif ( $user->user_type == 'agency' ) {// Agency require active package
                if( !$chat_info['hasActivePackage'] ){
                    $data_access['message'] = 'You do not have an active package !';
                } else {
                    $auth = true;
                }
            }
        }

        $data_access['auth'] = $auth;

        return $data_access;
    }
}