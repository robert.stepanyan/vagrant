<?php

class Zend_View_Helper_GetLink
{
	protected static $_params;
	
	public function getLink($type = '', array $args = array(), $clean = false, $no_args = false)
	{
		$lang_id = Cubix_I18n::getLang();

		$link = '/';
		/*if ( $lang_id != Cubix_Application::getDefaultLang() ) {
			$link .= $lang_id . '/';
		}*/
		
		if ( isset($args['state']) && 7 != Cubix_Application::getId() ) {
			$args['region'] = $args['state'];
			unset($args['state']);
		}
		
		switch ( $type ) {
            case 'banner-link-to-spin':
                $link .= 'private/signup-escort?type=escort';
                break;
			case 'photo-feed-index':
				$link .= 'photo-feed';
				break;
			case 'photo-feed-ajax-list':
				$link .= 'photo-feed/ajax-list?ajax';
				break;
			case 'photo-feed-ajax-header':
				$link .= 'photo-feed/ajax-header?ajax';
				break;
			case 'photo-feed-ajax-voting-box':
				$link .= 'photo-feed/ajax-voting-box/';
				break;
            case 'skype-available-listing':
				$link .= 'escorts/skype-available/';
				break;

			case 'photo-feed-ajax-voting-box':
				$link .= 'photo-feed/ajax-voting-box/';
				break;
			case 'photo-feed-ajax-more-photos':
				$link .= 'photo-feed/ajax-get-more-photos/';
				break;
			case 'photo-feed-ajax-vote':
				$link .= 'photo-feed/ajax-vote/';
				break;			

			case 'latest-actions-index':
				$link .= 'latest-actions';
				break;
			case 'latest-actions-ajax-header':
				$link .= 'latest-actions/ajax-header?ajax';
				break;
			case 'latest-actions-ajax-list':
				$link .= 'latest-actions/ajax-list?ajax';
				break;
			case 'latest-actions-ajax-get-details':
				$link .= 'latest-actions/ajax-get-details?ajax';
				break;
			case 'planner':
				$link .= 'planner';
			break;
			case 'glossary':
				$link .= 'glossary';
				break;
			case 'cityalerts':
				$link .= 'city-alerts';
			break;
            case 'feedback-ads':
                $link .= 'feedback-ads';
                break;
            case 'classified-ads-index':
                $link .= 'classified-ads';
                break;
            case 'classified-ads-place-ad':
                $link .= 'classified-ads/place-ad';
                break;
            case 'classified-ads-success':
                $link .= 'classified-ads/success';
                break;
            case 'classified-ads-ajax-filter':
                $link .= 'classified-ads/ajax-filter?ajax';
                break;
            case 'classified-ads-ajax-list':
                $link .= 'classified-ads/ajax-list?ajax';
                break;
            case 'classified-ads-print':
                $link .= 'classified-ads/print';
                break;
            case 'classified-ads-ad':
                $link .= 'classified-ads/ad';
                break;

			case 'online-billing':
				$link .= 'online-billing-v2';
				break;
			case 'ajax-add-package':
				$link .= 'online-billing/ajax-add-package';
				break;
			case 'ajax-shopping-cart':
				$link .= 'online-billing/ajax-get-shopping-cart';
				break;
			case 'ajax-shopping-cart-remove':
				$link .= 'online-billing/ajax-remove-from-shopping-cart';
				break;
			case 'ajax-change-city':
				$link .= 'online-billing/ajax-change-city';
				break;
			case 'ob-checkout':
				$link .= 'online-billing/checkout';
				break;
			case 'ob-order-history':
				$link .= 'online-billing/order-history';
				break;
			case 'ob-ajax-order-history':
				$link .= 'online-billing/ajax-get-order-history';
				break;
			case 'ob-successful':
				$link .= 'online-billing/successful-payment';
				break;
			case 'ob-unsuccessful':
				$link .= 'online-billing/unsuccessful-payment';
				break;
			case 'online-billing-v2':
				$link .= 'online-billing-v2';
				break;
			case 'ob-successful-v2':
				$link .= 'online-billing-v2/successful-payment';
				break;
			case 'ob-unsuccessful-v2':
				$link .= 'online-billing-v2/unsuccessful-payment';
				break;
			case 'ob-mmg-postback':
				$link .= 'online-billing-v2/mmg-postback';
				break;
			case 'online-billing-v2-wizard':
				$link .= 'online-billing-v2/';
				if ( $args['user_type'] == USER_TYPE_AGENCY ) {
					$link .= 'wizard-agency';
				} else {
					$link .= 'wizard-independent';
				}
				unset($args['user_type']);
				break;
			case 'ob-profile-boost':
				$link .= 'profile-boost';
				break;
			case 'ob-profile-boost-select-escort':
				$link .= 'profile-boost/select-escort';
				break;
			case 'ob-profile-boost-select-city':
				$link .= 'profile-boost/select-city';
				break;
			case 'ob-profile-boost-select-hour':
				$link .= 'profile-boost/select-hour';
				break;
			case 'ob-profile-boost-confirmation':
				$link .= 'profile-boost/confirmation';
				break;
			case 'ob-profile-bump':
				$link .= 'profile-bumps';
				break;
			case 'ob-profile-bump':
				$link .= 'profile-bumps';
				break;
			case 'ob-profile-bump-buy':
				$link .= 'profile-bumps/buy-bumps';
				break;
			case 'ob-profile-bump-buy-agency':
				$link .= 'profile-bumps/buy-bumps-agency';
				break;
			case 'ob-profile-bump-checkout':
				$link .= 'profile-bumps/checkout';
				break;
			case 'ob-profile-bump-agency-escort-edit':
				$link .= 'profile-bumps/agency-escort-edit';
				break;
			case 'ob-profile-bump-agency-escort-buy-bump':
				$link .= 'profile-bumps/agency-escort-buy-bump';
				break;
			case 'ob-profile-boost-select-frequency':
				$link .= 'profile-boost/select-frequency';
				break;
			case 'ajax-show-ad-place':
				$link .= 'online-billing/ajax-show-ad-place';
				break;
			case 'get-filter-v2':
				$link .= 'escorts/get-filter-v2';
				break;
			case 'get-gallery-photos':
				$link .= 'escorts/gallery-photos';
				break;
			case 'forum':
				$link = 'http://forum.escortforumit.xxx';
				break;
			case 'girlforum':
				$link = 'https://girlforum.escortforumit.xxx';
				break;
			case 'chat':
				$link = 'http://chat.escort-annonce.com/';
				break;
			case 'cams':
				$link = 'http://www.camplace.com/';
				break;
			case 'film':
				$link = 'http://video.escortforumit.xxx/';
				break;
			case 'search':
				$link .= 'search';
				break;
			case 'escorts-list':
				$link .= 'escorts';
			break;
			case 'base-city':
				$section = isset($args['section']) ? $args['section'] . '/' : '';
				if ( strlen($section) ) unset($args['section']);

				$link .= 'escorts/' . $section . 'city_' . $args['city'];
				unset($args['city']);
			break;
			case 'escorts':
				if ( empty(self::$_params) ) {
					$request = Zend_Controller_Front::getInstance()->getRequest();
					$req = trim($request->getParam('req', ''), '/');
					$req = explode('/', $req);

					
					$params = array();
					
					foreach ($req as $r)  {
						$param = explode('_', $r);
						if ( count($param) < 2 ) {
							$params[] = $r;
							continue;
						}
						
						$param_name = reset($param);
						array_shift($param);
						$params[$param_name] = implode('_', $param);
					}
					
					self::$_params = $params;
				}
				
				if ( $clean ) {
					$params = array();
				}
				else {
					$params = self::$_params;
				}
				
				foreach ( $args as $key => $value ) {
					if ( is_null($value) ) {
						unset($params[$key]);
						continue;
					}
					
					$params[$key] = $value;
				}
				
				$link .= 'escorts/';
				foreach ( $params as $param => $value ) {
					if ( ! strlen($param) || ! strlen($value)) continue;
					if ( is_int($param) ) {
						//if ( $value == 'tours' ) $value = 'citytours';
						$link .= $value . '/';
					}
					elseif ( strlen($param) ) {
						if ( $param == 'page' ) {
							if ( $value - 1 > 0 ) {
								$link .= ($value - 1) . '/';
							}
						}
						else {
							$link .= $param . '_' . $value . '/';
						}
					}
				}
				
				$link = rtrim($link, '/');
				
				$args = array();
			break;
			case 'gotm':
				$link .= 'girl-of-month';

				if ( isset($args['history']) && $args['history'] ) {
					$link .= '/history';
				}
				
				if ( isset($args['page']) ) {
					if ( $args['page'] != 1 ) {
						$link .= '/page_' . $args['page'];
						//$link .= '/page_' . $args['page'];
					}
					
					unset($args['page']);
				}
				break;
			case 'profile':
				$link .= 'accompagnatrici/' . $args['showname'] . '-' . $args['escort_id'];
				unset($args['showname']);
				unset($args['escort_id']);
			break;
			case 'forgot':
				$link .= 'private/forgot';
			break;
			case 'forgot-two':
				$link .= 'private/forgot-two';
			break;
			case 'password_change_required':
				$link .= 'private/password-change-required';
			break;
			case 'signup':
				$link .= 'private/signup-';
				
				if ( ! isset($args['type']) ) {
					$args['type'] = 'member';
				}
				
				$link .= $args['type'];
			break;
			case 'signin':
				$link .= 'private/signin';
			break;
			case 'signin-comment':
				$link .= 'private/signin-comment';
			break;
			case 'signout':
				$link .= 'private/signout';
			break;
			case 'private':
				$link .= 'private';
			break;
			case 'membership-type':
				$link .= 'private/membership-type';
			break;
			case 'feedback':
				$link .= 'feedback';
			break;
			case 'contact':
				$link .= 'contact';
			break;
			case 'add-review-comment':
				$link .= 'private-v2/add-review-comment';
			break;
			case 'captcha':
				$link .= 'captcha?' . rand();
				//$link = '/img/sample_captcha.gif?' . rand();
			break;
			case 'viewed-escorts':
				$link .= 'escorts/viewed-escorts';
			break;
            case 'latest-videos':
                $link .= 'escorts/latest-videos';
                break;
            case 'valentine':
                $link .= 'escorts/valentine';
                break;
			case '100p-verify':
				$link .= 'private/verify';
			break;
			case '100p-verify-webcam':
				$link .= 'private/verify/webcam';
			break;
			case '100p-verify-idcard':
				$link .= 'private/verify/idcard';
			break;
			case 'my-video':
				$link .= 'video/';
			break;
			case 'video-listing':
				$link .= 'escorts/videos';
			break;	
			case 'agency':
				$link .= 'agency/' . $args['slug'] . '-' . $args['id'];
				unset($args['slug']);
				unset($args['id']);
			break;
			
			case 'favorites':
				$link .= 'private/favorites';
			break;
			case 'add-to-favorites':
				$link .= 'private/add-to-favorites';
			break;
			case 'remove-from-favorites':
				$link .= 'private/remove-from-favorites';
			break;
			
			// Private Area
            case 'simple-profile':
				$link .= 'private-v2/profile/simple';
				break;
            case 'fill-contact-info':
				$link .= 'private-v2/ajax-fill-contact-info';
				break;
			case 'edit-profile':
				$link .= 'private/profile';
			break;
			case 'edit-agency-profile':
				$link .= 'private/agency-profile';
			break;
			case 'profile-data':
				$link .= 'private/profile-data';
			break;
			case 'edit-photos':
				$link .= 'private/photos';
			break;
			case 'edit-rates':
				$link .= 'private/rates';
			break;
			case 'edit-escorts':
				$link .= 'private/escorts';
			break;
			case 'delete-escort':
				$link .= 'private/delete-escort';
			break;
			case 'edit-tours':
				$link .= 'private/tours';
			break;
			case 'change-passwd':
				$link .= 'private/change-password';
			break;
			
			case 'search':
				$link .= 'search';
			break;

			case 'late-night-girls':
				$link .= 'escorts/late-night-girls';
			break;
			
			case 'terms':
                $link .= 'page/terms-and-conditions';
            break;
            case 'cookie-policy':
                $link .= 'page/cookie-policy';
                break;
            case 'valentines-contest':
                $link .= 'page/valentines-contest';
                break;
            case 'privacy-policy':
                $link .= 'page/privacy-policy';
                break;
			case 'webcam-lm':
				$link .= 'page/webcam-verification-learn-more';
			break;
			case 'passport-lm':
				$link .= 'page/passport-verification-learn-more';
			break;
			case 'confirm-phone':
				$link .= 'private-v2/confirm-phone';
			break;
            case 'how-it-works-adv':
                $link = 'https://www.escortforumit.xxx/page/how-it-works-adv';
                break;
            case 'how-it-works':
                $link = 'https://www.escortforumit.xxx/page/how-it-works';
                break;
//            case '4myfans':
//                $link = 'https://www.4myfans.ch?utm_source=efitescortpa&amp;utm_medium=efitescortpa&amp;utm_campaign=efitescortpa';
//                break;
			case 'external-link':
                $params='';
                if(isset($args['user_type'])){
                    $params .= '&user_type=' . $args['user_type'];
                    unset($args['user_type']);
                }
                if(isset($args['id'])){
                    $params .= '&id=' . $args['id'];
                    unset($args['id']);
                }

                $link = '/go?link=' . $args['link'] . $params;
                unset($args['link']);
			break;
			// V2 Private Area
			case 'private-v2-gotd':
				$link .= 'private-v2/profile/gotd';
				break;
			case 'private-v2-gotd-success':
				$link = 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/gotd-success';
				break;
			case 'private-v2-gotd-failure':
				$link = 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/gotd-failure';
				break;
			case 'private-v2-gotd-cancel':
				$link = 'http://' . $_SERVER['SERVER_NAME'] . '/private-v2/profile/gotd';
				break;
			case 'private-v2':
				$link .= 'private-v2';
				break;
			case 'private-v2-statistics':
				$link .= 'private-v2/statistics';
				break;
			case 'private-v2-settings':
				$link .= 'private-v2/settings';
				break;
			case 'private-v2-happy-hour':
				$link .= 'private-v2/happy-hour';
				break;
			case 'private-v2-client-blacklist':
				$link .= 'private-v2/client-blacklist';
				break;
			case 'private-v2-add-client-to-blacklist':
				$link .= 'private-v2/add-client-to-blacklist';
				break;
			case 'private-v2-profile':
				$link .= 'private-v2/profile';
				break;
            case 'private-v2-profile-location':
                $link .= 'private-v2/profile?step=contact-info#to-map';
                break;
			case 'private-v2-photos':
				$link .= 'private-v2/photos';
				break;
			case 'private-v2-plain-photos':
				$link .= 'private-v2/plain-photos';
				break;
			case 'private-v2-tours':
				$link .= 'private-v2/tours';
				break;
			case 'private-v2-ajax-tours':
				$link .= 'private-v2/ajax-tours';
				break;
			case 'private-v2-ajax-tours-add':
				$link .= 'private-v2/ajax-tours-add';
				break;
			case 'private-v2-ajax-tours-remove':
				$link .= 'private-v2/ajax-tours-remove';
				break;
			case 'private-v2-verify':
				$link .= 'private-v2/verify';
				break;
			case 'escort-age-verification':
				$link .= 'private-v2/escort-age-verification';
				break;
            case 'escort-age-verification-ajax':
                $link .= 'private-v2/escort-age-verification-ajax';
                break;
			case 'private-v2-faq':
				$link .= 'private-v2/faq';
				break;
			case 'private-v2-premium':
				$link .= 'private-v2/premium';
				break;
			case 'private-v2-support':
				$link .= 'support';
				break;
			case 'private-v2-support-faq':
				$link .= 'support/faq';
				break;
			case 'private-v2-get-urgent-message':
				$link .= 'private-v2/get-urgent-message';
				break;
			case 'private-v2-set-instant-book':
				$link .= 'private-v2/set-instant-book';
				break;
			case 'private-v2-instant-book-history':
				$link .= 'private-v2/instant-book-history';
				break;
			case 'private-v2-set-urgent-message':
				$link .= 'private-v2/set-urgent-message';
				break;
			case 'private-v2-get-rejected-verification':
				$link .= 'private-v2/get-rejected-verification';
				break;
			case 'private-v2-set-rejected-verification':
				$link .= 'private-v2/set-rejected-verification';
				break;
			case 'private-v2-check-sms-only':
				$link .= 'private-v2/check-sms-only';
				break;
			case 'alerts':
				$link .= 'private-v2/alerts';
				break;
			case 'private-v2-agency-profile':
				$link .= 'private-v2/agency-profile';
				break;
			case 'private-v2-member-profile':
				$link .= 'private-v2/member-profile';
				break;
			case 'private-v2-upgrade-premium':
				$link .= 'private-v2/upgrade';
				break;
			case 'ticket-open':
				$link .= 'support/ticket';
				break;
			case 'private-v2-escorts':
				$link .= 'private-v2/escorts';
				break;
            case 'private-v2-escorts-delete':
				$link .= 'private-v2/profile-delete';
				break;
			case 'private-messaging':
				$link .= 'private-messaging';
				break;
			case 'pv-thread':
				$link .= 'private-messaging/thread?id=' . $args['id'];
				if ( $args['escort_id'] ) {
					$link .= '&escort_id=' . $args['escort_id'];
					unset($args['escort_id']);
				}
				unset($args['id']);
				break;
			case 'reviews':
				$link .= 'recensioni';
				break;
			case 'escort-reviews':
				$link .= 'recensioni/' . $args['showname'] . '-' . $args['escort_id'];
				unset ($args['showname']);
				unset ($args['escort_id']);
				break;
			case 'member-reviews':
				$link .= 'recensioni/member/' . $args['username'];
				unset ($args['username']);
				break;
			case 'escorts-reviews':
				$link .= 'recensioni/escorts';
				break;
			case 'agencies-reviews':
				$link .= 'recensioni/agencies';
				break;
			case 'top-reviewers':
				$link .= 'recensioni/top-reviewers';
				break;
			case 'top-ladies':
				$link .= 'recensioni/top-ladies';
				break;
			case 'voting-widget':
				$link .= 'escort/' . $args['showname'] . '-' . $args['escort_id'] . '/vote';
				unset($args['showname']);
				unset($args['escort_id']);
				break;
			case 'private-v2-reviews':
				$link .= 'private-v2/reviews';
				break;
			case 'private-v2-escort-reviews':
				$link .= 'private-v2/escort-reviews';
				break;
			case 'private-v2-escorts-reviews':
				$link .= 'private-v2/escorts-reviews';
				break;
			case 'private-v2-add-review':
				$link .= 'private-v2/add-review';
				break;
			case 'private-v2-add-reply';
				$link .= 'private-v2/add-reply';
				break;
			case 'private-v2-ajax-escort-comments';
				$link .= 'private-v2/ajax-escort-comments';
				break;
			case 'private-v2-ajax-escort-reviews';
				$link .= 'private-v2/ajax-escort-reviews';
				break;
			case 'reviews-search':
				$link .= 'recensioni/search';
				break;
            case 'comments':
				$link .= 'comments';
				break;
			case 'signup-as-vip-member':
				$link .= 'private/signup-vip-member?type=vip-member';
				break;
			case 'members-choice':
				$link .= 'members-choice';
				break;
			case 'instant-book':
				$link .= 'escorts/instant-book';
				break;
            case 'sugar-dating':
				$link = 'https://offers.refchamp.com/?offer=5&uid=03112eb6-8935-4bc9-9591-1f16e8758836';
				break;
			case 'member-info':
				$link .= 'member/'.$args['username'];
				unset($args['username']);
				break;
			case 'member-comments':
				$link .= 'members/get-member-comments';
				break;
			case 'tell-friend':
				$link .= 'escorts/ajax-tell-friend';
				break;
			case 'susp-photo':
				$link .= 'escorts/ajax-susp-photo';
				break;
			case 'report-problem':
				$link .= 'escorts/ajax-report-problem';
				break;
			case 'get-filter':
				$link .= 'escorts/get-filter';
				break;
			case 'expo':
				$link .= 'escorts/expo';
				break;
			case 'private-v2-photo-video':
				$link .= 'photo-video';
				break;
			case 'private-v2-get-galleries':
				$link .= 'photo-video/get-galleries';
				break;
			case 'private-v2-gallery':
				$link .= 'photo-video/ajax-gallery';
				break;
			case 'private-v2-get-gallery':
				$link .= 'photo-video/ajax-get-gallery';
				break;
			case 'private-v2-video':
				$link .= 'photo-video/ajax-video';
				break;
			case 'private-v2-video-action':
				$link .= 'photo-video/video';
				break;
			case 'private-v2-cover-pic-action':
				$link .= 'photo-video/cover-pic';
				break;
			case 'private-v2-video-ready':
				$link .= 'photo-video/is-video-ready';
				break;
			case 'private-v2-natural-pic':
				$link .= 'photo-video/ajax-natural-pic';
				break;
			case 'private-v2-natural-pic-action':
				$link .= 'photo-video/natural-pic';
				break;
			case 'follow':
				$link .= 'follow';
			break;
			case 'ecardon-response':
				$link = APP_HTTP.'://www.escortforumit.xxx/online-billing-v2/ecardon-response';
			break;
			case 'paycash-response':
				$link = APP_HTTP.'://www.escortforumit.xxx/online-billing-v2/powercash-response';
			break;
            case 'powercash-response':
                $link = APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/payment/powercash-response';
                break;
			case 'video-chat-success':
				$link .= 'escorts/video-chat-success';
			break;	
			case 'ef-cams':
				$link = 'https://escortforumit.escortcams.com';
			break;
			case 'ef-cams-member':
				$link .= 'ef-cams'; 
			break;
		
			case 'ef-cams-buy-tokens':
				$link .= 'ef-cams/buy-tokens';
			break;
			case 'ef-cams-ecardon-response':
				$link .= 'ef-cams/ecardon-response';
			break;	
			case 'ef-cams-escort-dash':
				/*$user = Model_Users::getCurrent();
				if($user->sid){
					$link = 'https://www.escortforumcams.com/cam-'.$user->sid.'/broadcaster/dashboard';
				}
				else{
					$link = 'https://www.escortforumcams.com/broadcaster/dashboard';
				}*/
				$link .= 'ef-cams/broadcaster/dashboard';
			break;	
			case 'ef-cams-broadcaster':
				$encoded_showname = strtolower(str_replace(' ', '-', $args['showname']));
				$link = 'https://escortforumit.escortcams.com/channel/' . $encoded_showname.'-'.$args['escort_id'];
				unset($args['showname']);
				unset($args['escort_id']);
			break;
		
			case 'cams-login-redirect':
				$link = 'https://escortforumit.escortcams.com';
				if ($args['path']) {
					$link .= $args['path'];
				}
				unset($args['path']);
			break;
		
			case 'cams-booking-redirect':
				$link = 'https://escortforumit.escortcams.com/escort/';
				$link .= $args['escort_id'];
				$link .='/channel';
				unset($args['escort_id']);
			break;
			
			case 'ef-cams-agency-escorts-login':
				$link .= 'ef-cams/login-agency-escorts';
			break;	
		}

		if ( ! $no_args ) {
			if ( count($args) ) {
				$link .= '?';
				$params = array();
				foreach ( $args as $arg => $value ) {
					if ( ! is_array($value) ) {
						$params[] = $arg . '=' . urlencode($value);
					}
					else {
						foreach ( $value as $v ) {
							$params[] = $arg . '[]=' . $v;
						}
					}
				}

				$link .= implode('&', $params);
			}
		}
		
		return $link;
	}
}
