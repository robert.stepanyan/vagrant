<?php

class Zend_View_Helper_IsInAlerts
{
	public function isInAlerts($user_id, $escort_id)
	{ 
		$cache = Zend_Registry::get('cache');
		$cache_key = 'alert_list_for_user_' . Cubix_Application::getId() . '_'. $user_id;
		
		if ( ! $res = $cache->load($cache_key) ) {
			$client = new Cubix_Api_XmlRpc_Client();
			$res = $client->call('Members.getAlertsByUserId', array($user_id));
			$cache->save($res, $cache_key, array());
		}
		
		if(in_array($escort_id, $res)){
			return TRUE; 
		}
		else{
			return FALSE;
		}
		
	}
}

