<?php

class Zend_View_Helper_Avg
{
	public function avg($number)
	{
		$n = $number - (int)$number;
		
		if ($n < 0.25)
		{
			$n = 0;
		}
		elseif ($n >= 0.25 || $n <= 0.75)
		{
			$n = 0.5;
		}
		elseif ($n > 0.75)
		{
			$n = 1;
		}

		return number_format((int)$number + $n, 1, '.', '');
	}
}

