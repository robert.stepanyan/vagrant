<?php

class Zend_View_Helper_IsGOTM extends Zend_View_Helper_Abstract
{
	public function isGOTM($escort_id)
	{
		$cache = Zend_Registry::get('cache');
		$cache_key = 'isGOTM_escort_' . $escort_id;

		$result = $cache->load($cache_key);

		if ( $result === false )
		{
			//$result = Cubix_Api::getInstance()->call('isGOTM', array($escort_id));
			$m_escort = new Model_EscortsV2();
			$result = $m_escort->isGOTM($escort_id);
			
			$cache->save($result, $cache_key, array(), 3600);
		}
		
		return $result;
	}
}
