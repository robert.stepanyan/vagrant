function Config() {
	var config = {
		production : {
			common : {
				applicationId : 1,
				listenPort : 8888,
				host: 'www.escortforumit.xxx',
				authPath : '/get_user_info.php',
				sessionCacheTime: 1000 * 60 * 10, //10 minutes
				sessionCheckInterval : 1 * 60 * 60 * 1000, //1 hour
				imagesHost : 'http://pic.escortforumit.xxx/escortforumit.xxx/',
				avatarSize : 'lvthumb',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusLsdf8A9',
				blWordsLifeTime : 12 * 60 * 60 * 1000, //12 hours
				redisBlWordsDb: 0,
				redisBlWordsKey: 'bl-words-ef'
			},
			/*db : {
				user:     'ef_net_chat',
				database: 'escortforum_net',
				password: 'nUJkkdi_32sE805',
				host:     'db.ef.net'
			},*/
			db : {
				user:     'chat2',
				database: 'escortforum_net',
				password: 'kEHJnc74n91wEn34',
				host:     'db0'
			},
			redis: {
				host: 'db0',
				port: 6379
			},
			memcache : {
				host : process.env.MEMCACHE_HOST,
				port : process.env.MEMCACHE_PORT
			}
		},
		development: {
			common : {
				applicationId : 1,
				listenPort : 8888,
				host: 'www.escortforumit.xxx.dev',
				authPath: '/get_user_info.php',
				sessionCacheTime: 1000 * 60 * 10, //10 minutes
				sessionCheckInterval : 30 * 1000, //1 hour
				imagesHost : 'http://pic.escortforumit.xxx/escortforumit.xxx/',
				avatarSize : 'lvthumb',
				noPhotoUrl : '/img/chat/img-no-avatar.gif',
				userSettingsBackupPath : 'user_settings.bk',
				key : 'JFusLsdf8A9',
				blWordsLifeTime : 12 * 60 * 60 * 1000, //12 hours
				redisBlWordsDb: 0,
				redisBlWordsKey: 'bl-words-ef'
			},
			db : {
				user:     'sceon',
				database: 'escortforumit.xxx_backend',
				password: '123456',
				host:     '192.168.0.1'
			},
			memcache : {
				host : 'www1.ef.net',
				port : 11211
			}
		}
	};
	
	this.get = function(type) {
		type = 'production';
		if ( type == 'development' ) {
			return config.development;
		} else {
			return config.production;
		}
	}
}

function get(type) {
	conf = new Config();
	return conf.get(type);
}



exports.get = get;
