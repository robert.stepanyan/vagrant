<?php

class Api_SyncController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $_db;

	protected $_debug = true;

	protected $_log;

	/**
	 * @var Cubix_Api
	 */
	protected $_client;

	public function init()
	{
		
		$this->_db = Zend_Registry::get('db');
		$this->_client = Cubix_Api::getInstance();

		$this->view->layout()->disableLayout();
	}

	/**
	 * @var Cubix_Cli
	 */
	private $_cli;

	public function diffAction()
	{
		ini_set('memory_limit', '1024M');
		set_time_limit(0);
		error_reporting(E_ALL);
		ini_set('display_errors', 'on');
		
		if ( Cubix_Cli::pidIsRunning('/var/run/6a-sync.pid') ) {
			$cli->colorize('red')->out('Big Sync is running, exitting...')->reset();
			exit(1);
		}
		
		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>
		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/ef-diff-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		$log = $this->_log = new Cubix_Cli_Log('/var/log/ef-diff-sync.log');
		// </editor-fold>

		$sync_start_date = time();
		$start = 0; $limit = 10;
		do {
			// <editor-fold defaultstate="collapsed" desc="Fetch Data from API">
			$cli->out('Fetching data from api (max 100)...');
			$data = $this->_client->call('sync.getDiffEscorts', array($start++ * $limit, $limit), 360);
			
			// </editor-fold>

			// IMPORTANT! This array will contain ids of escorts which were successuflly
			// removed or updated this array will be passed to api to mark those escorts
			// in journal as synchronized
			$success = array();
			$this->_db->beginTransaction();
			try{
				// <editor-fold defaultstate="collapsed" desc="Process escorts which need to be removed">
				if ( count($data['removed']) > 0 ) {
					$cli->colorize('purple')
						->out('Need to remove ' . ($count = count($data['removed'])) . ' escorts...')
						->reset();
					foreach ( $data['removed'] as $i => $escort ) {
						$percent = round((($i + 1) / $count) * 100) . '%';
						// Shorthand variables
						list($escort_id, $showname) = $escort;

						try {
							$this->_removeEscort($escort_id);
							$success[] = $escort_id;
							$result = true;
						}
						catch ( Exception $e ) {
							$result = false;
							// If error occured just log the error and display output in red color
							$log->error("Unable to remove escort '$showname': " . $e->getMessage());
							throw $e;
						}

						// Pretty output
						$cli->out('[' . ($i + 1) . '/' . $count . ']', false)
							->colorize($result ? 'green' : 'red')
							->out(' Remove ' . $showname . '...', false)
							->column(60)
							->reset()
							->out($percent, true);

					}

					$cli->out();
				}
				// </editor-fold>

				// <editor-fold defaultstate="collapsed" desc="Process escorts which need to be updated">
				if ( count($data['updated']) > 0 ) {
					$cli->colorize('purple')
							->out('Need to update ' . ($count = count($data['updated'])) . ' escorts...')
							->reset();
					foreach ( $data['updated'] as $i => $escort ) {
						$percent = round((($i + 1) / $count) * 100) . '%';
						// Shorthand variables
						$escort_id = $escort['profile']['id'];
						$showname = $escort['profile']['showname'];

						// <editor-fold defaultstate="collapsed" desc="Remove escort at first">
						try {
							$this->_removeEscort($escort_id);
							$result = true;
						}
						catch ( Exception $e ) {
							$result = false;
							$log->error("Unable to remove escort '$showname': " . $e->getMessage());
							throw $e;
						}
						
						$cli->out('[' . ($i + 1) . '/' . $count . ']', false)
							->colorize($result ? 'green' : 'red')
							->out(' Remove ' . $showname . '...', false)
							->column(60)
							->reset()
							->out($percent, true);
						// </editor-fold>

						// If there is error removing error just ignore this one
						if ( ! $result ) continue;

						try {
							// Insert escort and all correspoinding data (services, cities, profiles, photos, etc.))
							$this->_insertEscort($escort);
							$success[] = $escort_id;
							$result = true;
						}
						catch ( Exception $e ) {
							$result = false;
							// Just log into file if something happend and ignore
							$log->error("Unable to insert escort '$showname': " . $e->getMessage());
							throw $e;
						}

						// Pretty output
						$cli->out('[' . ($i + 1) . '/' . $count . ']', false)
							->colorize($result ? 'green' : 'red')
							->out(' Insert ' . $showname . '...', false)
							->column(60)
							->reset()
							->out($percent, true);
					}

					$cli->out();
				}
				// </editor-fold>
				$this->_db->commit();
			}catch( Exception $e ) {
				$success = array();
				$this->_db->rollBack();
				var_dump($e->getMessage());
			}
			
			// If there were successfull escort syncs
			if ( count($success) > 0 ) {
				// <editor-fold defaultstate="collapsed" desc="Finally mark escorts as sync done to prevent continious syncing">
				// We don't want any extra data to be passed to API
				$success = array_unique($success);
				// Pass to API to mark these escorts as sync done
				$this->_client->call('sync.markEscortsDone', array($success, $sync_start_date));
				// </editor-fold>

				// And clear the cache
			}
			else {
				$cli->colorize('yellow')->out('Nothing happend :)')->reset();
			}
		} while ( count($data['removed']) > 0 || count($data['updated']) > 0 );

		// At the end we need to repopulate cache table for optimized sqls
		// this table contains rows that describe in which city escort will be shown
		// and why is she there, because of upcoming, tour or just working city
		// also there are some reduntunt fields such as is_premium, is_agency,
		// gender and so on
		$cli->out()->out('Populating cache tables...', false);
		try {
			$this->_populateCacheTables();
			$cli->column(60)->colorize('green')->out('success');
		}
		catch ( Exception $e ) {
			$cli->column(60)->colorize('red')->out('error');
			$log->error($e->getMessage());
		}
		$cli->out();
		file_put_contents('/var/log/ef-diff-sync-time.log', date("Y-m-d H:i:s")." \n", FILE_APPEND);
		//$clear_cache = file_get_contents(Cubix_Application::getById(Cubix_Application::getId())->url . "/api/escorts/clear-cache");
		$clear_cache = Model_Applications::clearCache(); 
		$cli->reset()->out($clear_cache);
    
		Model_EscortsV2::cacheTransCount();
		exit;
	}

	private function _removeEscort($escort_id)
	{
		$this->_db->delete('escorts', $this->_db->quoteInto('id = ?', $escort_id));
		$this->_db->delete('escort_services', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_cities', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_cityzones', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_photos', $this->_db->quoteInto('escort_id = ?', $escort_id));

		$this->_db->delete('escort_photo_votes', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_galleries', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('natural_pics', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('video', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('upcoming_tours', $this->_db->quoteInto('id = ?', $escort_id));
		$this->_db->delete('escort_profiles', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('premium_cities', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_working_times', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('escort_filter_data', $this->_db->quoteInto('escort_id = ?', $escort_id));
		$this->_db->delete('euro_lottery', $this->_db->quoteInto('escort_id = ?', $escort_id));
//		$this->_db->delete('escorts_in_cities', $this->_db->quoteInto('escort_id = ?', $escort_id));
		
	}

	private function _populateCacheTables()
	{	
		
		try {
			$this->_db->query('TRUNCATE escorts_in_cityzones');
			$sql = '
				INSERT INTO escorts_in_cityzones (escort_id, city_id, cityzone_id, gender, is_agency, is_tour, is_upcoming, is_premium)
				SELECT x.escort_id, x.id AS city_id, x.cityzone_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium
				FROM ((
					SELECT
						cz.city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
						EXISTS (SELECT 1 FROM premium_cities WHERE escort_id = e.id AND city_id = cz.id) AS is_premium
					FROM cityzones AS cz
					INNER JOIN escort_cityzones ecz ON ecz.city_zone_id = cz.id
					INNER JOIN escorts e ON e.id = ecz.escort_id
					INNER JOIN cities ct ON ct.id = cz.city_id
					WHERE ct.country_id = 101 /*AND e.is_on_tour = 0*/ AND FIND_IN_SET(1, e.products) > 0
				) UNION (
					SELECT
						ut.tour_city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM upcoming_tours ut
					INNER JOIN cities ct ON ct.id = ut.tour_city_id
					INNER JOIN escorts e ON e.id = ut.id
					INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id
					INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id AND cz.city_id = ct.id
					WHERE ct.country_id = 101 AND/* e.is_on_tour = 0 AND*/ FIND_IN_SET(7, e.products) > 0 AND
						DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY
				) UNION (
					SELECT
						e.tour_city_id AS id, e.id AS escort_id, cz.id AS cityzone_id,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
						e.is_tour_premium AS is_premium
					FROM escorts e
					INNER JOIN cities ct ON ct.id = e.tour_city_id
					INNER JOIN escort_cityzones ecz ON ecz.escort_id = e.id
					INNER JOIN cityzones cz ON cz.id = ecz.city_zone_id AND cz.city_id = ct.id
					WHERE
						ct.country_id = 101 AND e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0
				)) x
			';
			$this->_db->query($sql);
			
			/*$sql = '
				DELETE eic FROM escorts_in_cities AS eic
				WHERE ((SELECT e.is_on_tour FROM escorts e WHERE e.id = eic.escort_id) AND eic.is_tour = 0 AND eic.is_base = 0)
			';
			$this->_db->query($sql);*/
		}
		catch ( Exception $e ) {
			throw new Exception("Unable to repopulate cache table 'escorts_in_cityzones'\nError: {$e->getMessage()}");
		}
		
		try {
			$this->_db->query('TRUNCATE escorts_in_cities');
			$sql = '
				INSERT INTO escorts_in_cities (escort_id, region_id, city_id, gender, is_agency, is_tour, is_upcoming, is_premium, is_base, ordering)
				SELECT x.escort_id, x.region_id, x.id AS city_id, x.gender, x.is_agency, x.is_tour, x.is_upcoming, x.is_premium, x.is_base, x.ordering
				FROM ((
					SELECT
						ct.id AS id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 0 AS is_upcoming,
						EXISTS (SELECT 1 FROM premium_cities WHERE escort_id = e.id AND city_id = ct.id) AS is_premium,
						(e.city_id = ct.id) AS is_base
					FROM cities AS ct
					INNER JOIN escort_cities ec ON ec.city_id = ct.id
					INNER JOIN escorts e ON e.id = ec.escort_id
					WHERE ct.country_id = 101 /*AND e.is_on_tour = 0*/ AND FIND_IN_SET(1, e.products) > 0
				) UNION (
					SELECT
						ut.tour_city_id AS id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 0 AS is_tour, 1 AS is_upcoming,
						e.is_tour_premium AS is_premium,
						FALSE AS is_base
					FROM upcoming_tours ut
					INNER JOIN cities ct ON ct.id = ut.tour_city_id
					INNER JOIN escorts e ON e.id = ut.id
					WHERE ct.country_id = 101 AND/* e.is_on_tour = 0 AND*/ FIND_IN_SET(7, e.products) > 0 AND
						DATE(ut.tour_date_from) > DATE(NOW()) AND ut.tour_date_from < CURDATE() + INTERVAL 7 DAY
				) UNION (
					SELECT
						e.tour_city_id AS id, ct.region_id, e.id AS escort_id, e.ordering,
						e.gender, (e.agency_id IS NOT NULL) AS is_agency, 1 AS is_tour, 0 AS is_upcoming,
						e.is_tour_premium AS is_premium,
						true AS is_base
					FROM escorts e
					INNER JOIN cities ct ON ct.id = e.tour_city_id
					WHERE
						ct.country_id = 101 AND e.is_on_tour = 1 AND FIND_IN_SET(7, e.products) > 0
				)) x
			';
			$this->_db->query($sql);
			
			//during tour escort visible only in tour city only, except diamond, dreamed
			$sql = '
				DELETE eic FROM escorts_in_cities AS eic
				INNER JOIN escorts esc ON esc.id = eic.escort_id
				WHERE ((SELECT e.is_on_tour FROM escorts e WHERE e.id = eic.escort_id) AND eic.is_tour = 0 AND 
					esc.package_id NOT IN (101,102,103,110,121,124,145,183,186) AND eic.is_upcoming = 0)
			';
			$this->_db->query($sql);
			
			//If escort has tour she lose premium in her premium city except diamond
			/*$sql = '
				UPDATE escorts_in_cities AS eic SET eic.is_premium = 0
				WHERE (SELECT e.is_on_tour FROM escorts e WHERE e.id = eic.escort_id AND e.package_id != 101 AND e.package_id != 102 AND e.package_id != 103 AND e.package_id != 110 ) 
			';
			$this->_db->query($sql);*/

			//Mark Escorts that have tour in her city list
			$sql = 'UPDATE escorts_in_cities AS eic
					INNER JOIN escorts_in_cities eic2 ON eic2.escort_id = eic.escort_id AND eic2.city_id = eic.city_id AND eic2.is_tour = 0 AND eic2.is_upcoming = 0 
					SET eic.is_dublicate_tour = 1
					WHERE eic.is_tour = 1 AND eic.is_premium = 0';
			$this->_db->query($sql);

			//bumps last udpate during sync
			$sql = 'UPDATE escorts_in_cities AS eic INNER JOIN bumps b ON b.escort_id = eic.escort_id AND b.city_id = eic.city_id SET eic.last_bump_date = b.last_bump_date';
			$this->_db->query($sql);
			
			// remove escorts with sms only phone instruction which has not one of these packages (100, 101, 102, 103, 110)
			$escs = $this->_db->query('SELECT id FROM escorts WHERE package_id NOT IN (100, 101, 102, 103, 110, 124,121,124,133,139,145, 183,186) AND phone_instr = 2')->fetchAll();

			if ($escs)
			{
				$esc_arr = array();

				foreach ($escs as $esc)
					$esc_arr[] = $esc->id;

				$this->_db->query('DELETE FROM escorts_in_cities WHERE escort_id IN (' . implode(',', $esc_arr) . ')');
			}
			/////////////////////
		}
		catch ( Exception $e ) {
			throw new Exception("Unable to repopulate cache table 'escorts_in_cities'\nError: {$e->getMessage()}");
		}
	}

	private function _insertEscort($escort)
	{
		$cli = $this->_cli;
		$escort_id = $escort['profile']['id'];
		$showname = $escort['profile']['showname'];
		
		// Blacklisted countries
		$bl_countries_count = $this->_db->query('SELECT COUNT(id) AS count FROM countries')->fetch();
		$bl_countries_fields = ceil($bl_countries_count->count / 63);

		$bl_countries_set_values = array();
		for ( $bli = 1; $bli <= $bl_countries_fields; $bli++ ) {
			$_set_values = $this->_db->fetchRow('DESCRIBE escorts blacklisted_countries_' . $bli);
			$set = substr($_set_values->Type,5,strlen($_set_values->Type)-7);
			$bl_countries_set_values[$bli] = preg_split("/','/", $set);
		}


		$blacklisted_countries = $escort['profile']['blacklisted_countries'];
		unset($escort['profile']['blacklisted_countries']);
		// Blacklisted countries

		if ( isset($escort['products']) ) {
			$escort['profile']['products'] = implode(',', $escort['products']);
			
			if ($escort['profile']['is_vip'] == 1 && !in_array(16, $escort['products'])) //16 - vip product
				$escort['profile']['is_vip'] = 0;
		}
		// Insert main escort row
		$this->_db->insert('escorts', $escort['profile']);

		// Blacklisted countries
		$set_value_update = array();
		foreach ( $bl_countries_set_values as $field_num => $set_vals ) {
			foreach( $set_vals as $set_value ) {
				if ( in_array($set_value, $blacklisted_countries) ) {
					$set_value_update[$field_num][] = $set_value;
				} else {
					$set_value_update[$field_num][] = 9999;
				}
			}
		}


		if ( count($set_value_update) ) {
			foreach($set_value_update as $k => $vals) {
				$this->_db->update('escorts', array('blacklisted_countries_' . $k => join(',', $vals)), $this->_db->quoteInto('id = ?', $escort_id));
			}
		}
		// Blacklisted countries

		// Insert escort services
		foreach ( $escort['services'] as $service ) {
			$service['escort_id'] = $escort_id;
			try {
				$this->_db->insert('escort_services', $service);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert service for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($service, true));
			}
		}

		// Insert escort working cities
		foreach ( $escort['cities'] as $city ) {
			$city = array('escort_id' => $escort_id, 'city_id' => $city);
			try {
				$this->_db->insert('escort_cities', $city);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert city for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($city, true));
			}
		}

		// Insert escort cityzones
		foreach ( $escort['cityzones'] as $cityzone ) {
			$cityzone = array('escort_id' => $escort_id, 'city_zone_id' => $cityzone);
			try {
				$this->_db->insert('escort_cityzones', $cityzone);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert cityzone for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($cityzone, true));
			}
		}

		// Insert escort photos
		foreach ( $escort['photos'] as $photo ) {
			$photo['escort_id'] = $escort_id;
			try {
				$this->_db->insert('escort_photos', $photo);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert photo for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($photo, true));
			}
		}

		// Insert escort photo votes
		if ( count($escort['photo_votes']) ) {
			foreach ( $escort['photo_votes'] as $photo ) {
				$photo['escort_id'] = $escort_id;
				try {
					$this->_db->insert('escort_photo_votes', $photo);
				}
				catch ( Exception $e ) {
					throw new Exception("Unable to insert photo vote for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($photo, true));
				}
			}
		}
		
		// Insert escort photo galleries
		if ( count($escort['escort_galleries']) ) {
			foreach ( $escort['escort_galleries'] as $gallery ) {
				try {
					$this->_db->insert('escort_galleries', $gallery);
				}
				catch ( Exception $e ) {
					throw new Exception("Unable to insert photo gallery for '$showname'\nError: {$e->getMessage()}\n" . var_export($gallery, true));
				}
			}
		}
		
		// Insert escort natural pics
		if ( count($escort['natural_pics']) ) {
			foreach ( $escort['natural_pics'] as $pic ) {
				try {
					$this->_db->insert('natural_pics', $pic);
				}
				catch ( Exception $e ) {
					throw new Exception("Unable to insert natural pic for '$showname'\nError: {$e->getMessage()}\n" . var_export($pic, true));
				}
			}
		}
		
		// Insert escort euro lottery
		if ( count($escort['euro_lottery']) ) {
			foreach ( $escort['euro_lottery'] as $pic ) {
				try {
					$this->_db->insert('euro_lottery', $pic);
				}
				catch ( Exception $e ) {
					throw new Exception("Unable to insert euro lottery for '$showname'\nError: {$e->getMessage()}\n" . var_export($pic, true));
				}
			}
		}
		
		if (isset($escort['videos']) && !empty($escort['videos']))
		{
			foreach ( $escort['videos'] as $video ) {
				
				if(isset($video['escort_id']))
				{	
					$video_id = $video['id'];
					try {
						$this->_db->insert('video', $video);
						$insert_id = $this->_db->lastInsertId();
						foreach ($escort['videos']['image'] as $key => &$img)
						{	
							if($img['video_id'] == $video_id)
							{						
								$data = $img;
								$data['video_id'] = $insert_id;
								if($key == 2 ){
									$data['is_mid'] = 1;
								}
								else{
									$data['is_mid'] = 0;
								}
								$this->_db->insert('video_image', $data) ;
							}
						}
					}
					catch ( Exception $e ) {
						throw new Exception("Unable to insert video for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($video, true));
					}
				}
			}
		}
		
		// Insert escort upcoming tours
		foreach ( $escort['tours'] as $tour ) {
			$tour['id'] = $escort_id;
			try {
				$this->_db->insert('upcoming_tours', $tour);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert tour for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($tour, true));
			}
		}

		// Insert escort profile
		try {
			if ( isset($escort['snapshot']) ) {
				$snapshot = array('escort_id' => $escort_id, 'data' => serialize($escort['snapshot']));
				
				$filter_data = new Model_Api_EscortFilterData();
				$filter_row = $filter_data->getData((object)$escort['snapshot']);
				
				$this->_db->insert('escort_filter_data', $filter_row);
				
				$this->_db->insert('escort_profiles', $snapshot);
			}
		}
		catch ( Exception $e ) {
			throw new Exception("Unable to insert profile for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($snapshot, true));
		}

		// Finally if escort has premium cities insert them
		if ( isset($escort['premiums']) ) {
			foreach ( $escort['premiums'] as $city ) {
				$city = array('escort_id' => $escort_id, 'city_id' => $city);
				try {
					
					$this->_db->insert('premium_cities', $city);
				}
				catch ( Exception $e ) {
					throw new Exception("Unable to insert premium city for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($city, true));
				}
			}
		}

		foreach ( $escort['times'] as $time ) {
			$time['escort_id'] = $escort_id;
			try {
				$this->_db->insert('escort_working_times', $time);
			}
			catch ( Exception $e ) {
				throw new Exception("Unable to insert working time for escort '$showname'\nError: {$e->getMessage()}\n" . var_export($time, true));
			}
		}
	}

	public function reviewsAction()
	{
		ini_set('memory_limit', '1024M');
		ini_set("pcre.recursion_limit", "8000");
		$start_time = microtime(true);
		
		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/ef-reviews-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/ef-reviews-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Transfer Reviews">
		$chunk = 4000;
		$part = 0;
		$cli->out('Fetching reviews data from api (chunk size: ' . $chunk . ')...');
		$reviews = array();
		do {
			$cli->reset()->out('part #' . ($part + 1), false);
			$data = $this->_client->call('getEscortReviewsV2', array($part++ * $chunk, $chunk));
			if ( isset($data['error']) ) {
				$cli->column(60)->colorize('red')->out('error');
				$log->error('Error when fetching data from api: ' . $data['error']);
				continue;
			}

			$reviews = array_merge($reviews, $data['result']);
			$cli->column(60)->colorize('green')->out('success');
		} while ( count($data['result']) > 0 );
		$data = null; unset($data);
		$cli->reset()->out('Done fetching data (' . count($reviews) . ' reviews fetched).')->out();
		// </editor-fold>
		

		// <editor-fold defaultstate="collapsed" desc="Insert data into database">
		$cli->out('Truncate table `reviews`', false);
		try {
			$this->_db->query('TRUNCATE `reviews`');
			$cli->column(60)->colorize('green')->out('success');
		}
		catch ( Exception $e ) {
			$cli->column(60)->colorize('red')->out('error');
			$log->error('Error when truncating reviews table: ' . $e->getMessage());
			exit(1);
		}

		$cli->reset()->out('Inserting reviews...');
		$bulk_object = new Cubix_BulkInserter($this->_db,'reviews');
		$c = 0;
		$row_limit = 5000;
		try {
			foreach ( $reviews as $review ) {
				$c++;
				if($c === 1){
					$bulk_object->addKeys(array_keys($review));
				}
				
				$bulk_object->addFields($review);				

				if( $c % $row_limit == 0){
					$sql = $bulk_object->insertBulk();
					$cli->out("Bulk of ". $c . " rows inserted \n\r", false);
				}
				
			}
			
			if( $c % $row_limit !== 0 ){
				$bulk_object->insertBulk();
				$cli->out("Bulk of ". $c . " rows inserted \n\r", false);
			}
		}
		catch ( Exception $e ) {
			$cli->colorize('red')->out('Could not insert reviews #----'. $e->getMessage() );
			$log->error('Error when inserting reviews # : ' . $e->getMessage());
		}
		$cli->out('Successfully inserted ' . $c . ' reviews.');
		// </editor-fold>

		//$clear_cache = file_get_contents(Cubix_Application::getById(Cubix_Application::getId())->url . "/api/escorts/clear-cache");
		$clear_cache = Model_Applications::clearCache(); 
		$cli->reset()->out($clear_cache);
		$end_time = microtime(true);
        // Just print the execution time
        $cli->colorize('purple')->out('Execution took ' . round($end_time - $start_time, 4) . ' seconds')->reset()->out();
		exit(0);
	}

	public function commentsAction()
	{
		ini_set('memory_limit', '1024M');
		ini_set("pcre.recursion_limit", "16777");
		$start_time = microtime(true);
		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/ef-comments-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/ef-comments-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Transfer Comments">
		$chunk = 5000;
		$part = 0;
		$cli->out('Fetching comments data from api (chunk size: ' . $chunk . ')...');
		$comments = array();
		do {
			$cli->reset()->out('part #' . ($part + 1), false);
			$data = $this->_client->call('getCommentsV2', array($part++ * $chunk, $chunk));
			if ( isset($data['error']) ) {
				$cli->column(60)->colorize('red')->out('error');
				$log->error('Error when fetching data from api: ' . $data['error']);
				continue;
			}

			$comments = array_merge($comments, $data['result']);
			$cli->column(60)->colorize('green')->out('success');
		} while ( count($data['result']) > 0 );
		$data = null; unset($data);
		$cli->reset()->out('Done fetching data (' . count($comments) . ' comments fetched).')->out();
		// </editor-fold>


		// <editor-fold defaultstate="collapsed" desc="Insert data into database">
		$cli->out('Truncate table `comments`', false);
		try {
			$this->_db->query('TRUNCATE `comments`');
			$cli->column(60)->colorize('green')->out('success');
		}
		catch ( Exception $e ) {
			$cli->column(60)->colorize('red')->out('error');
			$log->error('Error when truncating comments table: ' . $e->getMessage());
			exit(1);
		}

		$cli->reset()->out('Inserting comments...');
		$c = 0;
		$bulk_object = new Cubix_BulkInserter($this->_db, 'comments');
		$row_limit = 5000;
		try {
			foreach ( $comments as $comment ) {
				$c++;
				
				if ( ! $comment['is_premium'] ) {
					$comment['is_premium'] = 0;
				}
				if ( ! $comment['comments_count'] ) {
					$comment['comments_count'] = 0;
				}
				
				if($c === 1){
					$bulk_object->addKeys(array_keys($comment));
				}

				$bulk_object->addFields($comment);				

				if( $c % $row_limit == 0){
					$bulk_object->insertBulk();
					$cli->out("Bulk of ". $c . " rows inserted \n\r", false);
				}
			}
			
			if( $c % $row_limit !== 0 ){
				$bulk_object->insertBulk();
				$cli->out("Bulk of ". $c . " rows inserted \n\r", false);
			}
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
			$cli->colorize('red')->out('Could not insert comment #' . $e->getMessage() )->reset();
			$log->error('Error when inserting comment #' . $e->getMessage());
		}
		
		$cli->out('Successfully inserted ' . $c . ' comments.');
		// </editor-fold>
		$end_time = microtime(true);
        // Just print the execution time
        $cli->colorize('purple')->out('Execution took ' . round($end_time - $start_time, 4) . ' seconds')->reset()->out();
		exit(0);
	}

	public function chatOnlineCronAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/ef-chat-online-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/ef-chat-online-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		$online_users = @file_get_contents(APP_HTTP .'://www.escortforumit.xxx:35555/roomonlineusers.js?roomid=1');

		// Reset all online users
		$this->_db->update('escorts', array('is_online' => 0), $this->_db->quoteInto('is_online = ?', 1));

		if ( strlen($online_users) > 0 ) {
			$online_users = substr($online_users, 20, (strlen($online_users) - 21));

			$online_users = json_decode($online_users);

			$usernames = array();
			if ( count($online_users) > 0 ) {
				foreach ( $online_users as $user ) {
					$usernames[] = $user->name;
					echo "Escort {$user->name} is online now ! \n\r";
				}
			}
			
			$this->_db->query('UPDATE escorts SET is_online = 1 WHERE username IN (\'' . implode('\',\'', $usernames) . '\')');
		}

		die;
	}

	public function blockCountriesAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/ef-blocked-countries-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/ef-blocked-countries-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Transfer BL Countries">
		$chunk = 5000;
		$part = 0;
		$cli->out('Fetching data from api (chunk size: ' . $chunk . ')...');
		$bl_countries = array();
		do {
			$cli->reset()->out('part #' . ($part + 1), false);
			$data = $this->_client->call('getBlockedCountriesV2', array($part++ * $chunk, $chunk));
			if ( isset($data['error']) ) {
				$cli->column(60)->colorize('red')->out('error');
				$log->error('Error when fetching data from api: ' . $data['error']);
				continue;
			}

			$bl_countries = array_merge($bl_countries, $data['result']);
			$cli->column(60)->colorize('green')->out('success');
		} while ( count($data['result']) > 0 );
		$data = null; unset($data);
		$cli->reset()->out('Done fetching data (' . count($bl_countries) . ' bl countries fetched).')->out();
		// </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Insert data into database">
		$cli->out('Truncate table `escort_blocked_countries`', false);
		try {
			$this->_db->query('TRUNCATE escort_blocked_countries');
			$cli->column(60)->colorize('green')->out('success');
		}
		catch ( Exception $e ) {
			$cli->column(60)->colorize('red')->out('error');
			$log->error('Error when truncating escort_blocked_countries table: ' . $e->getMessage());
			exit(1);
		}

		$cli->reset()->out('Inserting Blocked Countries...');
		foreach ( $bl_countries as $bl_country ) {
			try {
				$this->_db->insert('escort_blocked_countries', $bl_country);
			}
			catch ( Exception $e ) {
				$log->error('Error when inserting Bl country of escort #' . $bl_country['escort_id'] . ': ' . $e->getMessage());
			}
		}
		$cli->out('Successfully inserted ');
		// </editor-fold>

		die;
	}
	
	public function photoRotateAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/ef-photo-rotate-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/ef-photo-rotate-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

	
		$chunk = 4000;
		$part = 0;
		$cli->out('Fetching escorts data from database (chunk size: ' . $chunk . ')...');
		
		$photos_model = new Model_Escort_Photos();
		do {
			$cli->reset()->out('part #' . ($part + 1), false);
			$escorts = $this->_db->fetchAll('SELECT id, showname from escorts LIMIT ' . $part++ * $chunk . ', ' . $chunk);
			foreach($escorts as $escort)
			{
				$photos = $photos_model->getRotatablePicsByEscortId($escort->id);
				
				if(!is_null($photos)){
					
					$next_main_image = 0;
					$rotate_done = 0;
					$i = 0;
					$cli->out('Rotating Photos of escort - '. $escort->showname.' - '. $escort->id );
					foreach($photos['results'] as $photo){
						$i++;
						if($i == 1){
							$first_photo_id = $photo->id;
						}
						if($next_main_image == 1){
							$photos_model->setMainImageFront($photo->id);
							$rotate_done = 1;
							BREAK;
						}
						if($photo->is_main == 1){
							$next_main_image = 1;
						}
						if($photos['count'] == $i && $rotate_done == 0 ){
							$photos_model->setMainImageFront($first_photo_id);
						}	
					}
				}
				
				
			}
			
		} while ($escorts);
		$cli->column(60)->colorize('green')->out('success');
		$cli->reset()->out('Rotating Main Images Done ')->out();
		unset($escorts);
		
		//$clear_cache = file_get_contents(Cubix_Application::getById(Cubix_Application::getId())->url . "/api/escorts/clear-cache");
		$clear_cache = Model_Applications::clearCache(); 
		$cli->reset()->out($clear_cache);
		
		exit(0);
	}
	
	public function onlineNewsAction()
	{
		$this->_db->query('TRUNCATE online_news');
		$this->_db->insert('online_news', $this->_client->call('getOnlineNews',array(Cubix_Application::getId())));
		die;
	}
	
	public function diffanalyticsAction()
	{	
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$start = 0 ;
		$limit = 1000;
		$sleep = 2;//seconds
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		while(1)
		{
			$result = $this->_db->fetchAll('SELECT * FROM analytics LIMIT ' . $start++ * $limit . ', ' . $limit);

			if(empty($result))
			{	
				$this->_db->query("DELETE FROM analytics");
				die;
			}
			$insert = 'INSERT INTO analytics(`user_id`,`type`,`name`,`value`,`text_value`,`filter`,`group`,`ip`,`country`,`city`,`year_date`,`date`,`lat_long`,`region`,`application_id`) VALUES';

			foreach($result as &$res)
			{			
				$insert.="('".$res->user_id."',";
				$insert.="'".$res->type."',";
				$insert.="".$this->_db->quote($res->name).",";
				$insert.="".$this->_db->quote($res->value).",";
				$res->text_value = preg_replace('/(&.*?;$)|[\:]/iu','',$res->text_value);
					
				$res->text_value = Cubix_I18n::TransByValue(trim($res->text_value));
				
				$insert.="'".$res->text_value."',";
				$insert.="'".$res->filter."',";
				$res->group = preg_replace('/(&.*?;$)|[\:]/iu','',$res->group);
				$res->group = Cubix_I18n::TransByValue(trim($res->group));
				$insert.="".$this->_db->quote($res->group).",";
				$insert.="'".$res->ip."',";
				$insert.="".$this->_db->quote($res->country).",";
				$insert.="".$this->_db->quote($res->city).",";
				$insert.="'".$res->year_date."',";
				$insert.="'".$res->date."',";
				$insert.="".$this->_db->quote($res->lat_long).",";
				$insert.="".$this->_db->quote($res->region).",";
				$insert.="'".$res->application_id."'),";
			}
			$insert = substr($insert,0,strlen($insert)-1);
			$client->call('Application.SaveAnalytic',array($insert));

			sleep($sleep);
		}
	}
	
	
	public function profileBoostAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/ef-profile-boost-sync.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/run/ef-profile-boost-sync.pid');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Profile Boosts">
		$cli->out('Fetching profile boosts from api...');
		$data = $this->_client->call('getProfileBoosts', array());
		if ( isset($data['error']) ) {
			$cli->column(60)->colorize('red')->out('error');
			$log->error('Error when fetching data from api: ' . $data['error']);
			exit(0);
		}
		
		

		$cli->column(60)->colorize('green')->out('success');
		$data = $data['result'];
		$cli->reset()->out('Done fetching data (' . count($data) . ' profile boosts fetched).')->out();
		// </editor-fold>


		// <editor-fold defaultstate="collapsed" desc="Insert data into database">
		$cli->out('Truncate table `profile_boosts`', false);
		try {
			$this->_db->query('TRUNCATE `profile_boosts`');
			$cli->column(60)->colorize('green')->out('success');
		}
		catch ( Exception $e ) {
			$cli->column(60)->colorize('red')->out('error');
			$log->error('Error when truncating profile_boosts table: ' . $e->getMessage());
			exit(1);
		}

		$cli->reset()->out('Inserting profile boosts...');
		$c = 0;
		
		foreach ( $data as $boost ) {
			try {
				$this->_db->insert('profile_boosts', $boost);
				$c++;
			}
			catch ( Exception $e ) {
				$cli->colorize('red')->out('Could not insert boost')->reset();
				$log->error('Error when inserting boosts ' . $e->getMessage());
			}
		}
		$cli->out('Successfully inserted ' . $c . ' boosts.');
		// </editor-fold>
		
		//Clearing cache by tag
		//$clear_cache = file_get_contents(Cubix_Application::getById(Cubix_Application::getId())->url . "/api/escorts/clear-cache");
		$clear_cache = Model_Applications::clearCache(); 
		$cli->reset()->out($clear_cache);
		
		exit(0);
	}

	public function updateEscortsRandomNumbersAction()
	{
		ini_set('memory_limit', '1024M');

		// <editor-fold defaultstate="collapsed" desc="Init CLI">
		$log = $this->_log = new Cubix_Cli_Log('/var/log/update-random-escorts-' . Cubix_Application::getId() . '.log');
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Prevent from over-syncing">
		Cubix_Cli::setPidFile('/var/log/update-random-escorts-' . Cubix_Application::getId() . '.log');
		if ( Cubix_Cli::isRunning() ) {
			$cli->colorize('red')->out('Sync is already running, exitting...')->reset();
			exit(1);
		}
		// </editor-fold>

		$this->_db->query('UPDATE escorts SET ordering = RAND() * 100000');
		$this->_db->query('UPDATE escorts_in_cities SET ordering = RAND() * 100000');
		
		$cli->out('Successfully updated escort random numbers.');		
		
		
		$clear_cache = Model_Applications::clearCache();
		$cli->reset()->out($clear_cache); 
		
		exit(0);
	}

	//checking if escortId 97 is online
	public function checkEscort97Action()
	{
		$exists_in_escorts = $this->_db->fetchOne('SELECT TRUE FROM escorts WHERE id = 97');

		if ( ! $exists_in_escorts ) {
			Cubix_Email::send('kvahagn@gmail.com', 'EscortId 97 is missing in escorts', 'EscortId 97 is missing in escorts, need to check', true);
		}

		$exists_in_cities = $this->_db->fetchOne('SELECT TRUE FROM escorts_in_cities WHERE escort_id = 97 GROUP BY escort_id');

		if ( ! $exists_in_cities ) {
			Cubix_Email::send('kvahagn@gmail.com', 'EscortId 97 is missing in escorts_in_cities', 'EscortId 97 is missing in escorts_in_cities, need to check', true);
		}

		die('done');
	}

	public function watchedEscortsAction()
	{
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$limit = 1000;
		$client = Cubix_Api_XmlRpc_Client::getInstance();

		/* Delete 1 day old guest sessions */
		$this->_db->query('DELETE FROM watched_escorts WHERE DATE(date) < DATE(DATE_ADD(NOW(), INTERVAL -1 DAY)) AND user_id IS NULL');
		/**/

		while(1)
		{
			$result = $this->_db->fetchAll('SELECT * FROM watched_escorts WHERE is_sync = 0 AND user_id > 0 ORDER BY id ASC LIMIT ' . $limit);

			if ($result)
			{
				$last = end($result);
				$last_id = $last->id;

				$insert = 'INSERT INTO watched_escorts(`session_id`,`user_id`,`escort_id`,`date`,`is_sync`) VALUES ';

				foreach($result as $res)
				{
					$insert .= "('" . $res->session_id . "',";
					$insert .= $res->user_id . ",";
					$insert .= $res->escort_id . ",";
					$insert .= "'" . $res->date . "',";
					$insert .= "1),";
				}

				$insert = substr($insert, 0, strlen($insert) - 1);

				$ret = $client->call('Members.syncWatchedEscorts', array($insert));

				if (is_null($ret))
				{
					$this->_db->query('UPDATE watched_escorts SET is_sync = 1 WHERE is_sync = 0 AND id <= ?', $last_id);
				}
				else
				{
					var_dump($ret);die;
				}
			}
			else
			{
				die('Done!!!');
			}
		}
	}
	
	public function populateCacheTablesAction()
	{
		$this->_populateCacheTables();
		die('populating finished ');
	}

	public function bumpAction()
	{
		//every minute transfer bump from back to front if payment confirmed
		$this->_transferBumpsToFront();
		$this->_bumpAds();

		//every EF-228 reseting bump after last bump 12 hours for now :)
		$this->_bumpsreset();
		die;
	}

	public function bump_now($bump_id){
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();

		$bump =  $this->_db->fetchRow('SELECT * FROM bumps WHERE id = ?', $bump_id);
		
		if(!$bump->city_id || !$bump->escort_id || !$bump->frequency || !$bump->bumps_amount >0){
			$cli->out('Bump is not found');die;
		}

		$this->_db->beginTransaction();
		try{
			$result = $this->_db->query("UPDATE escorts_in_cities SET last_bump_date = '".date('Y-m-d H:i:s')."' WHERE escort_id = ".$bump->escort_id. ' AND city_id = '.$bump->city_id);
			$affectedRows = $result->rowCount();
			
			if($affectedRows){
				$this->_db->query("UPDATE bumps SET bumps_amount = bumps_amount - 1, last_bump_date = '".date('Y-m-d H:i:s')."' WHERE id = ".$bump_id);
				$this->_db->insert( 'bumps_history', array('escort_id' => $bump->escort_id, 'city_id' => $bump->city_id, 'bumps_amount'=> $bump->bumps_amount, 'bump_date' => date('Y-m-d H:i:s') , 'action_name' => 'deducted' ) );
				$this->_db->commit();
				$cli->out('bump_id - '.$bump_id .' successfully bumped');
			}else{
				$this->_bumpremove($bump_id);
				$cli->out('bump_id - '.$bump_id .' failed to bump - city does not exists');
			}
			
		}catch( Exception $e ) {
			$success = array();
			$this->_db->rollBack();
			var_dump($e->getMessage());
		}
	}

	public function _bumpAds(){
		$cli = $this->_cli = new Cubix_Cli();
		$cli->clear();

		$defs = Zend_Registry::get('definitions');
		$bump_options = $defs['BUMP_AVAILABLE_HOURS'];
		//Cubix_Email::send('mihranhayrapetyan@gmail.com', 'bumpinio', 'here is bumping op - '.$bump_options , true);
		$bumps = $this->_db->fetchAll('SELECT * FROM bumps WHERE bumps_amount > 0 AND city_id IS NOT NULL');
	
		if($bumps){
			foreach($bumps as $bump){
				
				
				if(!is_null($bump->last_bump_date)){
					$last_bump_date =  date_create($bump->last_bump_date); 
					$current_date =  date_create(date('Y-m-d H:i:s')); 
					
					if( $last_bump_date > $current_date ){$cli->column(60)->colorize('red')->out('bump last date is invalid');}
					
					$interval = date_diff($last_bump_date, $current_date);

					$minutes = $interval->days * 24 * 60;
					$minutes += $interval->h * 60;
					$minutes += $interval->i;
					
					foreach($bump_options as $option => $value){
						
						if($option == $bump->frequency){
							switch ($bump->frequency) {
								case 'bump-option-1':
								case 'bump-option-2':
								case 'bump-option-3':
								case 'bump-option-5':

									if($minutes > $bump_options[$option] * 60){
										$this->bump_now($bump->id);
									}
									break;
								case 'bump-option-4':
									if($minutes > $bump_options[$option] * 60){
										if(date('H') == '10' || date('H') == '22'){
											$this->bump_now($bump->id);
										}
									}
									break;
								case 'bump-option-6':
									$this->bump_now($bump->id);
									break;
								default:
									
									break;
							}
						}
					}
				}else{
					//Ad never been bumped,  first bump

					//custom bumping option
					if( $bump->frequency == 'bump-option-4' ){
						if(date('H') == '10' || date('H') == '22' ){
							$this->bump_now($bump->id);
						}
					}else{
						$this->bump_now($bump->id);
					}
				}
				$cli->colorize('green')->out(' Bumps Done successfully! ')->reset();
			}
		}else{
			$cli->colorize('green')->out(' No pending bumps ')->reset();exit(0);
		}
		
	}

	public function _bumpsreset(){
		//every EF-228 reseting bump after last bump 12 hours for now :)
		$cli = $this->_cli = new Cubix_Cli();
		$bumps = $this->_db->fetchAll('SELECT * FROM bumps WHERE bumps_amount = 0 AND last_bump_date IS NOT NULL');

		$cli->colorize('green')->out(' Bumps reset started' )->reset();
		
		if($bumps){
			foreach($bumps as $bump){
				$last_bump_date =  date_create($bump->last_bump_date); 
				$current_date =  date_create(date('Y-m-d H:i:s')); 

				if( $last_bump_date > $current_date ){$cli->column(60)->colorize('red')->out('last bump date is invalid');}

				$interval = date_diff($last_bump_date, $current_date);

				$minutes = $interval->days * 24 * 60;
				$minutes += $interval->h * 60;
				$minutes += $interval->i;

				//720 minutes = 12 mins
				if($minutes > 240){

					$this->_db->query("UPDATE escorts_in_cities SET last_bump_date = NULL WHERE escort_id = ".$bump->escort_id. ' AND city_id = '.$bump->city_id);
					$this->_db->query("UPDATE bumps SET last_bump_date = NULL WHERE id = ".$bump->id);
					$this->_db->insert( 'bumps_history', array('escort_id' => $bump->escort_id, 'city_id' => $bump->city_id, 'bumps_amount'=> $bump->bumps_amount, 'bump_date' => date('Y-m-d H:i:s'), 'action_name' => 'reset' ) );
					$this->_db->commit();
					$cli->out('bump_id - '. $bump->id .' successfully reset');
				}
			}
		}
	}

	public function _bumpremove($bump_id){
		$cli = $this->_cli = new Cubix_Cli();
		$bump =  $this->_db->fetchRow('SELECT * FROM bumps WHERE id = ?', $bump_id);
		$cli->colorize('green')->out(' Bumps remove started' )->reset();
		
		if($bump){
			$this->_db->query("UPDATE bumps SET last_bump_date = NULL, bumps_amount = 0 WHERE id = ".$bump->id);
			$this->_db->insert( 'bumps_history', array('escort_id' => $bump->escort_id, 'city_id' => $bump->city_id, 'bumps_amount'=> $bump->bumps_amount, 'bump_date' => date('Y-m-d H:i:s'), 'action_name' => 'removed, city not found' ) );
			$this->_db->commit();
			$cli->out('bump_id - '. $bump->id .' successfully removed');
		}
	}

	public function _transferBumpsToFront(){
		$cli = $this->_cli = new Cubix_Cli();
	
		$cli->colorize('yellow')->out(' Transferring paid bumps to frontend ')->reset();

		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$confirmed_bumps = $client->call('Bumps.getConfirmedBumps');

		foreach($confirmed_bumps as $confirmed_bump){

			$this->_db->beginTransaction();
			
			try{
				$sql = "INSERT INTO bumps (escort_id,bumps_amount,city_id,frequency) VALUES (".$confirmed_bump['escort_id'].",".$confirmed_bump['bumps_count'].",".$confirmed_bump['city_id'].",'".$confirmed_bump['frequency']."') 
				ON DUPLICATE KEY UPDATE bumps_amount = bumps_amount + ".$confirmed_bump['bumps_count'].", city_id = ".$confirmed_bump['city_id'].", frequency = '".$confirmed_bump['frequency']."'";
				$result = $this->_db->query($sql);

				if($result){
					$client->call('Bumps.confirmBumpsTransfer', array($confirmed_bump['id']));
				}

				$this->_db->commit();
			}catch( Exception $e ) {
				$success = array();
				$this->_db->rollBack();
				var_dump($e->getMessage());
			}

			$cli->colorize('green')->out(' Bumps transferred successfully to front :) ')->reset();
		}

		$cli->colorize('yellow')->out(' There is no paid bumps ')->reset();
	}
}
