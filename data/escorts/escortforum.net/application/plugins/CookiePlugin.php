<?php
class CookiePlugin extends Zend_Controller_Plugin_Abstract
{
    /**
     * @param Zend_Controller_Request_Abstract $request
     * @throws Exception
     */
    public function dispatchLoopShutdown()
    {
        $request = $this->getRequest();
        $user = Model_Users::getCurrent();
        if(($user instanceof Model_UserItem) && !$user->isMember() && ($request->getActionName() != 'signout' || $request->getActionName() != 'auto-signin' || $request->getActionName() != 'signin')){
            if ($user->clientID) {
                $clientID = $user->clientID;
            }
            elseif (!empty($_COOKIE['clientID'])) {
                $clientID = $_COOKIE['clientID'];
                $user->clientID = $clientID;
                Model_Users::setCurrent($user);
            }
            else {
                $client = Cubix_Api_XmlRpc_Client::getInstance();
                $clientID = $client->call('getClientCookieId', array($user->id));
                if (!$clientID) {
                    $clientID = md5(microtime());
                    $client->call('cookieClientID', array($user->id, $clientID));
                }
                $user->clientID = $clientID;
                Model_Users::setCurrent($user);
            }

            if ($clientID){
                setcookie("clientID", $clientID, time() + 31536000, "/"); // 31536000 = 60*60*24*365 = one year in seconds
            }
        }

    }
}