/*jslint es6*/
/*jslint node*/

const socketServer = require('./socketServer');

socketServer.startInstantBook();
socketServer.startChat();
