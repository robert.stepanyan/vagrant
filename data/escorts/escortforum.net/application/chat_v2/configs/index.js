/*jslint es6*/
/*jslint node*/

"use strict";

module.exports = (() => {
  const configs = {
    production: {
      common: {
        applicationId: 1,
        listenPort: 8888,
        host: (process.env.FRONTEND_HOST || "www.escortforumit.xxx"),
        authPath: "/get_user_info.php",
        sessionCacheTime: 1000 * 60 * 1, //10 minutes
        sessionCheckInterval: 1 * 60 * 60 * 1000, //1 hour
        imagesHost: "http://pic.escortforumit.xxx/escortforumit.xxx/",
        avatarSize: "lvthumb",
        noPhotoUrl: "/img/chat/img-no-avatar.gif",
        userSettingsBackupPath: "user_settings.bk",
        key: "JFusLsdf8A9",
        blWordsLifeTime: 12 * 60 * 60 * 1000, //12 hours
        redisBlWordsDb: 0,
        redisBlWordsKey: "bl-words-ef"
      },
      pushNotifications: {
        restApiKey: "OGE3ZjViZjctODE1NC00MzhiLWI5YmEtZjQ3YTMzNDczNzQz",
        url: "https://onesignal.com/api/v1/notifications",
        appId: "18971079-7fed-450f-85cf-498b905d15d6"
      },
      pushNotificationsMobile: {
        restApiKey: "ODEzMmI4ZGMtMDIwNC00ODg2LTgzMGMtMWY5Yzg5ZWJlZTUw",
        url: "https://onesignal.com/api/v1/notifications",
        appId: "52e8ac40-e364-40f3-b410-8709168f7ed9"
      },
      sms: {
        url:
          (process.env.BACKEND_URL || "https://backend.escortforumit.xxx") +
          "/sms/index/send-instantbook-sms",
        authorization: process.env.BACKEND_HTTP_AUTH // || "Basic enV6dWJhOmlxaWJpcis="
      },
      db: {
        user: process.env.MYSQL_USER || "chat2",
        database: process.env.MYSQL_DBNAME || "escortforum_net",
        password: process.env.MYSQL_PASS || "kEHJnc74n91wEn34",
        host: process.env.MYSQL_HOST || "db0"
      },
      redis: {
        host: process.env.REDIS_HOST || "db0",
        port: process.env.REDIS_PORT || 6379
      },
      memcache: {
        host: process.env.MEMCACHE_HOST || "fe01",
        port: process.env.MEMCACHE_PORT || 11211
      }
    },

    development: {
      common: {
        applicationId: 1,
        listenPort: 8888,
        instantBookListenPort: 8889,
        host: "www.escortforumit.xxx.test",
        authPath: "get_user_info.php",
        sessionCacheTime: 1000 * 60 * 10, //10 minutes
        sessionCheckInterval: 30 * 1000, //1 hour
        imagesHost: "http://pic.escortforumit.xxx/escortforumit.xxx/",
        avatarSize: "lvthumb",
        noPhotoUrl: "/img/chat/img-no-avatar.gif",
        userSettingsBackupPath: "user_settings.bk",
        key: "JFusLsdf8A9",
        blWordsLifeTime: 12 * 60 * 60 * 1000, //12 hours
        redisBlWordsDb: 0,
        redisBlWordsKey: "bl-words-ef"
      },
      db: {
        user: "sceon",
        database: "escortforum_net",
        password: "sceon123456",
        host: "10.10.0.55"
      },
      redis: {
        host: "127.0.0.1",
        port: 6379
      },
      memcache: {
        host: "127.0.0.1",
        port: 49664
      }
    }
  };

  const get = env_ => {
    !env_ && (env_ = "production");

    return configs[env_];
  };

  return { get: get };
})();
