/*jslint es6*/
/*jslint node*/

'use strict';

const MySQL = require('mysql');
const validator = require('validator');
const config = require('../configs').get('production');
const { toMySQLFormat } = require("../utils/");

class Bookings {
  constructor() {
    this.establishMySQLConnection();

    setInterval(() => {
      this.MySQLClient.ping(err => {
        if (err) {
          this.handleMySQLConnectionerr(err);
        }
      });
    }, 5000);
  }

  establishMySQLConnection() {
    this.MySQLClient = MySQL.createConnection({
      user: config.db.user,
      database: config.db.database,
      password: config.db.password,
      host: config.db.host,
      port: 3306
    });

    this.MySQLClient.connect(err => { });
  }

  handleMySQLConnectionerr(err) {
    if (!err.fatal) {
      return false;
    }

    if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
      console.log(
        'The mysql library fired a PROTOCOL_CONNECTION_LOST exception'
      );

      throw err;
    }

    console.log('Re-connecting lost connection: ' + err.stack);
    this.establishMySQLConnection();
  }

  addBookingRequestPromise(data) {
    var assigned = Object.assign({}, data);

    return new Promise((resolve, reject) => {
    let err = {};

      if (!validator.isNumeric(assigned.hour.toString())) {
      err.hour = 'hours must be numeric';
    }

      if (!validator.isNumeric(assigned.min.toString())) {
      err.min = 'minutes must be numeric';
    }

      if (!assigned.duration || !validator.isNumeric(assigned.duration.toString())) {
      err.duration = 'duration must be numeric';
    }

      if (!assigned.location || (assigned.location.toString() !== "incall" && assigned.location.toString() !== "outcall")) {
      err.duration = "location must be either incall or outcall";
    }

      if (!assigned.phone || !assigned.phone.toString().match(/^[0-9| |-]{5,20}$/g)) {
      err.phone = 'invalid phone number';
    }

      if (!data.wait_time || !validator.isNumeric(assigned.wait_time.toString())) {
      err.wait_time = 'wait time must be numeric';
    }
    if (assigned.contact_type && Array.isArray(assigned.contact_type) && assigned.contact_type.length) {
      let filtered = [];

        assigned.contact_type.forEach(cType => filtered.concat(['phone_call', 'sms', 'viber', 'whatsapp'].includes(cType)));

      if (filtered.length) {
        err.contact_type = 'contact types must be one of following: phone_call, sms, viber, whatsapp';
      } else {
        assigned.contact_type = assigned.contact_type.join(',');
      }
    } else {
      err.contact_type = 'at least one of contact types have to be selected';
    }

      if (!assigned.escort_id) {
      err.escort_id = 'escort id must be numeric';
    }

    if (Object.keys(err).length) {
      return resolve({status: 'error', msg: err });
    }

      assigned.request_date = toMySQLFormat(new Date());
      assigned.status = 'pending';
      assigned.booking_date = assigned.booking_date.split('.')[0].replace('T', ' ');

    let _query = `INSERT INTO instant_book_requests SET ?`;

      this.MySQLClient.query(_query, assigned, (error, result, fields) => {
        assigned = null;

        if (error) {
          return reject(error);
        }

        return resolve({status: 'success', msg: fields, insertId: result.insertId});
      });
    });
  }

  updateBookingRequestStatusPromise(data) {
    return new Promise((resolve, reject) => {
      let _data = {
        status: data.status
      };

      let _query = `UPDATE instant_book_requests SET ? WHERE id = ${data.id}`;

      this.MySQLClient.query(_query, _data, (err, result) => {
        if (err) return reject(err);

        return resolve(result);
      });
    });
  }

  initDenyTimeoutRequestsPromise() {
    return new Promise((resolve, reject) => {
      let _date = toMySQLFormat(new Date());
      let _query = `
      SELECT * FROM instant_book_requests
      WHERE request_date < DATE_SUB("${_date}", INTERVAL wait_time MINUTE) AND status = "pending"
      `;
      this.MySQLClient.query(_query, (err, resultSelect) => {
        if (err) return reject(err);
        if (resultSelect.length) {
          let _query = `UPDATE instant_book_requests SET status = "no_answer"
          WHERE request_date < DATE_SUB("${_date}", INTERVAL wait_time MINUTE) AND status = "pending"`;

          this.MySQLClient.query(_query, (err, resultUpdate) => {
            if (err) return reject(err);
            return resolve(resultSelect);
          }); 
        } else {
          resolve(null);
        }
      });
    });
  }
}

module.exports = new Bookings();