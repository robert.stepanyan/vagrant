/*jslint es6*/
/*jslint node*/

'use strict';

const MySQL = require('mysql');
const Memcached = require('memcached');
const config = require('../configs').get('production');
const CACHE = {};
let cacheHit = 0;
let cacheMiss = 0;

setInterval(() => {
  if (!cacheMiss) return;
  console.log('Cache hit/miss ratio: ', cacheHit / cacheMiss);
}, 5000);

class Users {
  constructor() {
    this.usersPrivateList = {};
    this.usersPublicList = {};
    this.usersWithoutSockets = {};
    this.MySQLClient = null;

    this.establishMySQLConnection();

    this.memcachedClient = new Memcached(
      `${config.memcache.host}:${config.memcache.port}`
    );

    setInterval(() => {
      this.MySQLClient.ping(err => {
        if (err) {
          this.handleMySQLConnectionerr(err);
        }
      });
    }, 5000);
  }

  // USER CONNECTIVITY SERVICE

  initOfflineUsersCheckPromise() {
    return new Promise((resolve, reject) => {
      let removedUserIds = [];

      Object.keys(this.usersWithoutSockets).forEach(userId => {
        if (
          this.usersPrivateList[userId] &&
          !this.usersPrivateList[userId].sockets.length &&
          new Date() - this.usersWithoutSockets[userId] >= 2 * 1000
        ) {
          removedUserIds.push(this.removeUser(userId));
        }
      });

      return resolve(removedUserIds);
    });
  }

  // DB CONNECTION SERVICES

  establishMySQLConnection() {
    this.MySQLClient = MySQL.createConnection({
      user: config.db.user,
      database: config.db.database,
      password: config.db.password,
      host: config.db.host,
      port: 3306
    });

    this.MySQLClient.connect(err => {});
  }

  handleMySQLConnectionerr(err) {
    if (!err.fatal) {
      return false;
    }

    if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
      console.log(
        'The mysql library fired a PROTOCOL_CONNECTION_LOST exception'
      );

      throw err;
    }

    console.log('Re-connecting lost connection: ' + err.stack);
    this.establishMySQLConnection();
  }

  // USER LIST GETTERS

  getUsersPrivateList() {
    return this.usersPrivateList;
  }

  getUsersPublicList() {
    return Object.assign({}, this.usersPublicList);
  }

  // USER GETTERS

  async getUserPromise(userId, offline) {
    let user = await this.usersPrivateList[userId];
    if (user) return user;
    if (!offline) return false;

    let offlineUser = await this.getOfflineUserPromise(userId);
    return offlineUser;
  }

  async getUsersPromise(userIds, offline) {
    if (!userIds && !userIds.length) {
      return {};
    }

    if (!offline) {
      return userIds.reduce((a, user) => {
        a[user.info.userId] = user;
        return a;
      }, {});
    }

    let users = await this.getOfflineUserTypesPromise(userIds);
    let members = [];
    let escorts = [];
    let agencies = [];

    users.forEach(user => {
      let userType = user.user_type;
      let userId = user.id;

      switch(userType)  {
        case 'member':
          members.push(userId);
        case 'escort':
          escorts.push(userId);
        case 'agency':
          agencies.push(userId);
      }
    });
    let result = {};
    let escortsData = {};
    let membersData = {};
    let agenciesData = {};

    if (escorts.length) {
      escortsData = await this.getEscortsPromise(escorts);
    }

    if (members.length) {
      membersData = await this.getMembersPromise(members);
    }

    if (agencies.length) {
      agenciesData = await this.getMembersPromise(agencies);
    }

    return Object.assign({}, escortsData, membersData, agenciesData);
  }

  getOfflineUserTypesPromise(userIds) {
    return new Promise((resolve, reject) => {
      let _query = `SELECT id, username, user_type
        FROM users
        WHERE id IN (${userIds.join(',')})`;

        this.MySQLClient.query(_query, (err, result) => {
          if (err) reject(err);
          resolve(result);
        });
    });
  }

  getEscortsPromise(ids) {
    return new Promise((resolve, reject) => {
      let _query = `
        SELECT e.id, e.showname, e.user_id, ep.hash, ep.ext
        FROM escorts e
        INNER JOIN escort_photos ep ON ep.escort_id = e.id AND ep.is_main = 1
        WHERE e.user_id IN (${ids.join(',')})
      `;

      this.MySQLClient.query(_query, (err, result) => {
        if (err || !result.length) {
          return resolve(null);
        } else {
          return resolve(result.reduce((a, escort) => {
            a[escort.user_id] = {
              info: {
                nickName: `${escort.showname} (Escort)`,
                userId: escort.user_id,
                userType: 'escort',
                avatar: this.getEscortImageUrl(
                  escort.id,
                  escort.hash,
                  escort.ext
                ),
                status: this.usersPrivateList[escort.user_id] ? 'online' : 'offline',
                link: `/accompagnatrici/${escort.showname}-${escort.id}`
              },
              sockets: []
            }

            return a;
          }, {}));
        }
      });
    });
  }

  getMembersPromise(ids) {
    return new Promise((resolve, reject) => {
      let _query = `
        SELECT id, username
        FROM users
        WHERE id IN (${ids.join(',')})
      `;

      this.MySQLClient.query(_query, (err, result) => {
        if (err) return resolve(err);

        if (!result.length) {
          return resolve(null);
        } else {
          return resolve(result.reduce((a, member) => {
            a[member.id] = {
              info: {
                nickName: `${member.username}`,
                userId: member.id,
                userType: 'member',
                avatar: config.common.noPhotoUrl,
                status: this.usersPrivateList[member.id] ? 'online' : 'offline',
                link: `/member/${member.username}`
              },
              sockets: []
            }

            return a;
          }, {}));
        }
      });
    });
  }

  getAgenciesPromise(ids) {
    return new Promise((resolve, reject) => {
      let _query = `
        SELECT username, user_id
        FROM users
        WHERE user_id IN (${ids.join(',')})
      `;

      this.MySQLClient.query(_query, (err, result) => {
        if (err || !result.length) {
          return resolve(null);
        } else {
          return resolve(result.reduce((a, agency) => {
            a[agency.user_id] = {
              info: {
                nickName: `${agency.username}`,
                userId: agency.user_id,
                userType: 'agency',
                avatar: config.common.noPhotoUrl,
                status: this.usersPrivateList[agency.user_id] ? 'online' : 'offline',
              },
              sockets: []
            }

            return a;
          }, {}));
        }
      });
    });
  }



  getOfflineUserPromise(userId) {
    if (CACHE[userId] && Date.now() - CACHE[userId].date_cached < 5 * 60 * 1000) {
      cacheHit++;
      return Promise.resolve(CACHE[userId].data);
    }

    return new Promise((resolve, reject) => {
      let _query = `SELECT id, username, user_type
        FROM users
        WHERE id = ${userId}`;
      
      function resolveCached(data) {
        CACHE[userId] = {
          date_cached: Date.now(),
          data
        };
        cacheMiss++;
        resolve(data);
      }

      this.MySQLClient.query(_query, (err, result) => {
        if (err) {
          return reject(err);
        }
        if (Array.isArray(result) && !result.length) {
          switch (result[0].user_type) {
            case 'escort':
              this.getEscortPromise(userId).then(data => resolveCached(data));
              break;
            case 'member':
              this.getMemberPromise(userId).then(data => resolveCached(data));
              break;
            case 'agency':
              this.getAgencyPromise(userId).then(data => resolveCached(data));
              break;
          }
        }
      });
    });
  }

  getOnlineEscortsPromise() {
    return new Promise((resolve, reject) => {
      let onlineEscorts = Object.keys(this.usersPublicList).filter(userId => {
        return this.usersPublicList[userId].userType === 'escort';
      });

      return resolve(onlineEscorts);
    });
  }

  getEscortImageUrl(eId, hash, ext) {
    let pathParts = [];
    let escortId = eId.toString();

    if (escortId.length > 2) {
      pathParts = pathParts
        .concat(escortId.substr(0, 2))
        .concat(escortId.substr(2));
    } else {
      pathParts = pathParts.concat('_').concat(escortId);
    }

    let dirPath = pathParts.join('/');

    return `${config.common.imagesHost}/${dirPath}/${hash}_${
      config.common.avatarSize
      }.${ext}`;
  }

  getEscortPromise(userId) {
    return new Promise((resolve, reject) => {
      let _query = `
        SELECT e.id, e.showname, ep.hash, ep.ext
        FROM escorts e
        INNER JOIN escort_photos ep ON ep.escort_id = e.id AND ep.is_main = 1
        WHERE e.user_id = ${userId}
      `;

      this.MySQLClient.query(_query, (err, result) => {
        if (err || !result.length) {
          return resolve(null);
        } else {
          return resolve({
            info: {
              nickName: `${result[0].showname} (Escort)`,
              userId: userId,
              userType: 'escort',
              avatar: this.getEscortImageUrl(
                result[0].id,
                result[0].hash,
                result[0].ext
              ),
              status: 'offline',
              link: `/accompagnatrici/${result[0].showname}-${result[0].id}`
            },
            sockets: []
          });
        }
      });
    });
  }

  getMemberPromise(userId) {
    return new Promise((resolve, reject) => {
      let _query = `
        SELECT username
        FROM users
        WHERE id = ${userId}
      `;

      this.MySQLClient.query(_query, (err, result) => {
        if (err) return reject(err);

        if (!result.length) {
          return resolve(null);
        } else {
          return resolve({
            info: {
              nickName: `${result[0].username}`,
              userId: userId,
              userType: 'member',
              avatar: config.common.noPhotoUrl,
              status: 'offline',
              link: `/member/${result[0].username}`
            },
            sockets: []
          });
        }
      });
    });
  }

  getAgencyPromise(userId) {
    return new Promise((resolve, reject) => {
      let _query = `
        SELECT username
        FROM users
        WHERE id = ${userId}
      `;

      this.MySQLClient.query(_query, (err, result) => {
        if (err || !result.length) {
          return resolve(null);
        } else {
          return resolve({
            info: {
              nickName: `${result[0].username}`,
              userId: userId,
              userType: 'agency',
              avatar: config.common.noPhotoUrl,
              status: 'offline'
            },
            sockets: []
          });
        }
      });
    });
  }

  // USER SETTERS

  addUser(socketId, info, isPrivate) {
    if (!this.usersPrivateList[info.userId]) {
      this.usersPrivateList[info.userId] = {
        sockets: [],
        info: info
      };
    }

    if (!isPrivate) {
      this.addUserToPublicList(info);
    }

    this.addSocket(info.userId, socketId);

    this.updateOnlineEscortsPromise();
  }

  removeUser(userId) {
    delete this.usersPrivateList[userId];
    delete this.usersPublicList[userId];
    delete this.usersWithoutSockets[userId];

    this.updateOnlineEscortsPromise().then(() => {
      console.log('online escorts updated.');
    }).catch(() => {
      console.log('ERROR: updating online escorts.')
    });

    return userId;
  }

  addUserToPublicList(info) {
    this.usersPublicList[info.userId] = info;
  }

  removeUserFromPublicList(userId) {
    delete this.usersPublicList[userId];
  }

  addSocket(userId, socketId) {
    if (!this.usersPrivateList[userId]) return false;
    this.usersPrivateList[userId].sockets = this.usersPrivateList[userId].sockets.concat(socketId);

    if (this.usersWithoutSockets[userId]) {
      delete this.usersWithoutSockets[userId];
    }
  }

  removeSocket(userId, socketId) {
    if (!this.usersPrivateList[userId]) return false;
    let start = Date.now();
    
    this.usersPrivateList[userId].sockets = this.usersPrivateList[
      userId
    ].sockets.filter(socket => {

      return socket !== socketId}
    );
    console.log(`Removing sockets took ${Date.now() - start}ms`);
    if (this.usersPrivateList[userId].sockets.length === 0) {
      this.usersWithoutSockets[userId] = new Date();
    }
  }

  addUserWithoutSockets(userId) {
    this.usersWithoutSockets[userId] = new Date();
  }

  updateOnlineEscortsPromise() {
    return new Promise((resolve, reject) => {
      this.getOnlineEscortsPromise().then(data => {
        this.memcachedClient.set(`${config.common.applicationId}_chat_online_escorts`, JSON.stringify(data), 1000, (err, result) => {
            if (err) {
              console.log(`ERROR: ${err}`);
              return reject(err);
            }

            return resolve(true);
          });
      });
    });
  }
}

module.exports = new Users();