/*jslint node*/

"use strict";

const fs = require("fs");
const config = require("../configs").get("production");

class Settings {
  constructor() {
    this.settings = {};

    fs.readFile(config.common.userSettingsBackupPath, (err, result) => {
      if (err) return {};

      try {
        this.settings = JSON.parse(result);
      } catch (err) {
        console.log(`Error: ${err}`);
      }
    });
  }

  initSettingsBackup() {
      setInterval(() => {
        fs.writeFile(
          config.common.userSettingsBackupPath,
          JSON.stringify(this.settings),
          (err, result) => {
            if (err) console.error(err);
          }
        );
      }, 60 * 60 * 1000);
  }

  setSettings(userId, settings) {
    this.settings[userId] = settings;
  }

  getSettings(userId) {
    return this.settings[userId] ? this.settings[userId] : {};
  }

  // DIALOG GETTERS

  getOpenDialogs(userId) {
    let _settings = this.getSettings(userId);

    if (_settings.openDialogs && _settings.openDialogs.length > 10) {
      _settings.openDialogs = [];
      this.settings[userId] = _settings;
    }

    return _settings.openDialogs || [];
  }

  // DIALOG SETTERS

  addOpenDialog(userId, dialogId) {

    let _settings = this.getSettings(userId);
    _settings.openDialogs = _settings.openDialogs || [];
    _settings.openDialogs = _settings.openDialogs
      .filter(dialog => dialog !== dialogId)
      .concat(dialogId);

      this.setSettings(userId, _settings);
  }

  removeOpenDialog(userId, dialogId) {
    let _settings = this.getSettings(userId);
    if (_settings.openDialogs) {
      _settings.openDialogs = _settings.openDialogs.filter(
        dialog => dialog !== dialogId
      );
    }

    this.setSettings(userId, _settings);
  }

  setActiveDialog(userId, dialogId) {
    let _settings = this.getSettings(userId);
    _settings.activeDialog = dialogId;

    this.setSettings(userId, _settings);
  }

  removeActiveDialog(userId) {
    this.setSettings((this.getSettings(userId).activeDialog = false));
  }

  getActiveDialog(userId) {
    let _settings = this.getSettings(userId);
    return !_settings.activeDialog ? false : _settings.activeDialog;
  }

  // AVAILABILITY GETTERS

  getAvailability(userId) {
    let _settings = this.getSettings(userId);
    return typeof _settings.availability === 'undefined' ? 1 : _settings.availability;
  }

  // AVAILABILITY SETTERS

  setAvailability(userId, availability) {
    let _availability = availability != 0 ? 1 : 0;
    let _settings = this.getSettings(userId);
    _settings.availability = _availability;

    this.setSettings(userId, _settings);
  }

  setShowOnlyEscorts(userId, showOnlyEscorts) {
    let _settings = this.getSettings(userId);
    _settings.showOnlyEscorts = showOnlyEscorts;

    this.setSettings(userId, _settings);
  }

  getShowOnlyEscorts(userId) {
    let _settings = this.getSettings(userId);
    return !_settings.showOnlyEscorts ? 1 : _settings.showOnlyEscorts;
  }

  setKeepListOpen(userId, keepListOpen) {
    let _settings = this.getSettings(userId);
    _settings.keepListOpen = keepListOpen;

    this.setSettings(userId, _settings);
  }

  getKeepListOpen(userId) {
    let _settings = this.getSettings(userId);
    return !_settings.keepListOpen ? 0 : _settings.keepListOpen;
  }

  setInvisibility(userId, invisibility) {
    let _settings = this.getSettings(userId);
    _settings.invisibility = invisibility;

    this.setSettings(userId, _settings);
  }

  getInvisibility(userId) {
    let _settings = this.getSettings(userId);
    return !_settings.invisibility ? 0 : _settings.invisibility;
  }

  addBlockedUser(userId, blockedUserId) {
    let _settings = this.getSettings(userId);

    _settings.blockedUsers = _settings.blockedUsers || [];
    _settings.blockedUsers = _settings.blockedUsers
      .filter(blockedUser => {
        return blockedUser !== blockedUserId;
      })
      .concat(blockedUserId);
  }

  removeBlockedUser(userId, blockedUserId) {
    let _settings = this.getSettings(userId);
    if (_settings.blockedUsers) {

      _settings.blockedUsers = _settings.blockedUsers.filter(
        blockedUser => blockedUser != blockedUserId
      );
    }
  }

  getBlockedUsers(userId) {
    let _settings = this.getSettings(userId);
    return !_settings.blockedUsers ? [] : _settings.blockedUsers;
  }

  blockedUserId(userId) {
    let _settings = this.getSettings(userId);
    return !_settings.blockedUsers ? [] : _settings.blockedUsers;
  }
}

module.exports = new Settings();
