const chunks = (arr, chunkSize) => {
  let results = [];
  while (arr.length) results.push(arr.splice(0, chunkSize));
  return results;
};

module.exports = (xs, f, concurrency) => {
  if (xs.length == 0) return Promise.resolve();
  return Promise.all(chunks(xs, concurrency).reduce(
    (acc, chunk) => acc.then(() => Promise.all(chunk.map(f))),
    Promise.resolve()
  ));
};