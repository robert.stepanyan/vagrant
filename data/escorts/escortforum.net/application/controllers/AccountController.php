<?php

class AccountController extends Zend_Controller_Action
{

    public function secureSigninAction()
    {
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
        $secure_token = $this->_getParam('secure_token');

        $validator = new Cubix_Validator();
        $model = new Model_Users();
        $user = $model->getByToken($secure_token);

        $result = $validator->getStatus();

        if ( $user ) {

            $has_active_package = false;
            $is_susp = false;

            $client = new Cubix_Api_XmlRpc_Client();

            if ( $user->user_type == 'agency' ) {
                $_SESSION['fc_gender'] = 2;
            }
            else if ( $user->user_type == 'member' ) {
                $_SESSION['fc_gender'] = 1;
            }
            else if ( $user->user_type == 'escort' ) {
                $_SESSION['fc_gender'] = 2;

                /***********************************/
                $has_active_package = $client->call('Escorts.hasPaidActivePackageForEscort', array($user->id));
                $m_e = new Model_EscortsV2();
                $is_susp = $m_e->isSuspicious($user->id);
            }


            // UNCOMMENT WHEN DEPLOYED
            //Zend_Session::regenerateId();

            $user->sign_hash = md5(rand(100000, 999999) * microtime(true));
            $user->has_active_package = $has_active_package;
            $user->is_susp = $is_susp;

			$chat_info = array(
				'nickName'	=> $user->username,
				'userId'	=> $user->id,
				'userType'	=> $user->user_type,
				'imIsBlocked' => $user->im_is_blocked
			);
			
            if ( $user->user_type == 'escort' ) {
				$m = new Model_EscortsV2();
				$e = $m->getByUserId($user->id);
				$chat_info['nickName'] = $e->showname . ' (Escort)';
				$chat_info['isSusp'] = $is_susp;
				$chat_info['hasActivePackage'] = $has_active_package;
				$chat_info['link'] = '/accompagnatrici/' . $e->showname . '-' . $e->id;
				//Getting main image url
				$parts = array();
				if ( strlen($e->id) > 2 ) {
					$parts[] = substr($e->id, 0, 2);
					$parts[] = substr($e->id, 2);
				}
				else {
					$parts[] = '_';
					$parts[] = $e->id;
				}
				$catalog = implode('/', $parts);

				$app = Cubix_Application::getById();
				$chat_info['avatar'] = APP_HTTP.'://pic.' . $app->host . '/' . $app->host . '/' . $catalog . '/' . $e->photo_hash . '_lvthumb.' . $e->photo_ext;

            } elseif ( $user->user_type == 'agency' ) {
                $m = new Model_Agencies();
                $a = $m->getByUserId($user->id);
              	$chat_info['nickName'] = $a->name . ' (Agency)';
				$chat_info['hasActivePackage'] = $has_active_package;


            } elseif ( $user->user_type == 'member' ) {
				$chat_info['link'] = '/member/' . $user->username;
			}

			$user->chat_info = $chat_info;
			
            Model_Users::setCurrent($user);
            Model_Reviews::createCookieClientID($user->id);
            Model_Hooks::preUserSignIn($user->id);

            // login into forum
            $forum2 = new Cubix_Forum2Api();
            $forum2->Login($user->username, $user->email);
            //
			$this->_response->setRedirect($this->view->getLink('private-v2'));
            
        }
    }


}
