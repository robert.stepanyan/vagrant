<?php

class ExpoController extends Zend_Controller_Action
{
	public static $linkHelper;
	
	protected $_session;
	
	const STATUS_PENDING  = 1;
	const STATUS_ACTIVE   = 2;
	const STATUS_EXPIRED  = 3;
	const STATUS_CANCELLED = 4;
	const STATUS_UPGRADED = 5;
	const STATUS_SUSPENDED = 6;
	
	public function init()
	{	
		self::$linkHelper = $this->view->getHelper('GetLink'); 
		
		$this->view->user = $this->user = Model_Users::getCurrent();
		
		$anonym = array();
		
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		
		if ( ! in_array($this->_request->getActionName(), $anonym) && ! $this->user->isAgency() && ! $this->user->isEscort() ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}
		
		$this->view->headTitle('Private Area', 'PREPEND');

		$this->client = new Cubix_Api_XmlRpc_Client();
	}
	
	public function indexAction()
	{
		
	}
	
	public function selectOptionAction()
	{
		$this->view->layout()->disableLayout();
	}

	public function ageVerificationAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->a = $this->_request->a;
	}

	public function addExpoPackageAction()
	{
		$this->view->layout()->disableLayout();
		
		$this->view->price = 25;
		$is_agency = false;
		$agency_id = null;
		
		

		if ( $this->user->isAgency() ) {
			$is_agency = true;
			$agency_id = $this->user->agency_data['agency_id'];
			$escorts = $this->client->call('OnlineBillingV2.getAgencyEscorts', array($agency_id));

			// <editor-fold defaultstate="collapsed" desc="checking if escorts can buy package">
			foreach($escorts as $i => $escort) {
				$allow = true;
				if ( count( $escort['escort_packages'] ) ) {
					$current_package = $escort['escort_packages'][0];
					$pending_package = $escort['escort_packages'][1];

					if ( $current_package && $current_package['status'] == self::STATUS_ACTIVE ) {
						$escorts[$i]['has_active_package'] = true;
					}

					if ( $current_package && ( $current_package['status'] == self::STATUS_PENDING || $current_package['status'] == self::STATUS_SUSPENDED ) ) {
						$allow = false;
					} elseif ( $pending_package && $pending_package['status'] == self::STATUS_PENDING ) {
						$allow = false;
					} elseif ( $current_package['status'] == self::STATUS_ACTIVE && $current_package['expiration_date'] - time() > 5 *24*60*60 ) {
						$allow = false;
					}

					if ( ! $allow ) {
						unset($escorts[$i]);
					}
				}
			}
			// </editor-fold>

			$this->view->escorts = $escorts;
		} else {
			if ( $is_pseudo_escort ) {
				$user_type = USER_TYPE_AGENCY;
			}
			// <editor-fold defaultstate="collapsed" desc="Checking if escort can buy package">
			$escort_packages = $this->client->call('OnlineBillingV2.checkIfHasPaidPackage', array($this->user->escort_data['escort_id']));

			$allow = true;
			if ( count( $escort_packages ) ) {

				$message = '';
				$current_package = $escort_packages[0];
				$pending_package = $escort_packages[1];

				if ( $current_package && ( $current_package['status'] == self::STATUS_PENDING || $current_package['status'] == self::STATUS_SUSPENDED ) ) {
					$allow = false;
					$message = Cubix_I18n::translate('online_billing_has_pending');
				} elseif ( $pending_package && $pending_package['status'] == self::STATUS_PENDING ) {
					$allow = false;
					$message = Cubix_I18n::translate('online_billing_has_pending');
				} elseif ( $current_package['status'] == self::STATUS_ACTIVE && $current_package['expiration_date'] - time() > 5 *24*60*60 ) {
					$allow = false;
					$message = Cubix_I18n::translate('online_billing_has_active');
				}
			}

			if ( ! $allow ) {
				$this->_helper->viewRenderer->setScriptAction("restriction");
				$this->view->error_message = $message;
				return;
			}
			// </editor-fold>
		}

		$this->view->is_agency = $is_agency;
		
		
		if ( ! $this->_request->isPost() ) {
			
		} else {
			
			$validator = new Cubix_Validator();
			$data = array(
				'escort_id' => $this->_request->escort_id,
				'start_date'		=> $this->_request->start_date,
				'end_date'	=> $this->_request->end_date
			);
			
			if ( $is_agency ) {
				if ( ! $data['escort_id'] ) {
					$validator->setError('escort_id', Cubix_I18n::translate('select_escort'));
				}
			} else {
				$data['escort_id'] = $this->user->escort_data['escort_id'];
			}
			
			if ( ! $data['start_date'] || ! $data['end_date'] ) {
				$validator->setError('date_range', Cubix_I18n::translate('select_range'));
			} elseif ( $data['start_date'] < strtotime(date('Y-m-d')) || $data['end_date'] < strtotime(date('Y-m-d')) ) {
				$validator->setError('date_range', Cubix_I18n::translate('select_date_in_future'));
			} elseif ( $data['start_date'] >= $data['end_date'] ) {
				$validator->setError('date_range', Cubix_I18n::translate('invalid_range'));
			} elseif ( $data['end_date'] > strtotime('2015-10-31') ) {
				$validator->setError('date_range', 'Expo available till end of October.');
			} 
			
			$days_count = 0;
			if ( $data['start_date'] && $data['end_date'] ) {
				$days_count = (int)(($data['end_date'] - $data['start_date']) / (60*60*24));
				if ( $days_count < 2 ) {
					$validator->setError('date_range', Cubix_I18n::translate('at_least_x_days', array('days' => 3)));
				}
			}
			
			if ( $validator->isValid() ) {
				$user_type = USER_TYPE_SINGLE_GIRL;
				if ( $is_agency ) {
					$user_type = USER_TYPE_AGENCY;
				} else {
					$is_pseudo_escort = $this->client->call('OnlineBillingV2.isPseudoEscort', array($escort_id));
					if ( $is_pseudo_escort ) {
						$user_type = USER_TYPE_AGENCY;
					}
				}
				
				$req = array();
				
				$req[] = array(
					'user_id' => $this->user->id,
					'escort_id' => $data['escort_id'],
					'agency_id' => $agency_id,
					'package_id' => 115,//Expo package
					'data' => serialize(array('activation_date' => date('Y-m-d', $data['start_date']), 'period' => $days_count + 1) )
				);
				
				$amount = $this->client->call('OnlineBillingV2.addToShoppingCart', array($req,  $this->user->id, $user_type));
				
				if ( $amount > 2500 ) {
					die(json_encode(array('status' => 'error', 'msgs' => array('date_range' => __('max_amount_per_transaction')))));
				}

				// <editor-fold defaultstate="collapsed" desc="Payment part">
				if ( $this->_request->payment_gateway == 'mmgbill' ) {
					$mmgBill = new Model_MmgBillAPI();
					$hosted_url = $mmgBill->getHostedPageUrl($amount, 'SCZ' . $this->user->id . 'Z' . rand(10000,99999));
					die(json_encode(array('status' => 'success', 'url' =>  $hosted_url)));

				} else {
					$nbx = new Model_NetbanxAPI();
					try {
					$order_data = array(
						'total_amount'	=> $amount * 100,
						'currency_code'	=> 'EUR',
						'merchant_ref_num'	=> 'SC-' . $this->user->id . '-' . time(),
						'locale'	=> 'en_US',
						'callback'	=> array( 
							array(
								'format'		=> 'json',
								'rel'			=> 'on_success',
								'retries'		=> 6,
								'returnKeys'	=> array('id', 'transaction.amount', 'transaction.authType', 'transaction.status', 'transaction.currencyCode', 'transaction.merchantRefNum', 'transaction.confirmationNumber', 
									'transaction.card.brand', 'transaction.card.country', 'transaction.card.expiry', 'transaction.card.lastDigits', 'profile.paymentToken', 'profile.id'
								),
								'synchronous'	=> true,
								'uri'			=> APP_HTTP. '://backend.escortforumit.xxx/billing/online-billing/netbanx-callback-sc'
							)
						),
						'redirect'	=> array(
							array(
								'rel'	=> 'on_success',
								'returnKeys'	=> array('id'),
								'uri'	=> APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-successful')
							)
						),
						'link'		=> array(
							array(
								'rel'	=> 'cancel_url',
								'uri'	=> APP_HTTP.'://www.escortforumit.xxx/online-billing-v2'
							),
							array(
								'rel'	=> 'return_url',
								'uri'	=> APP_HTTP.'://www.escortforumit.xxx/online-billing-v2'
							)
						)
					);

					$order = $nbx->create_order($order_data);

					} catch(Exception $e) {
						$message = $e->getMessage();
						file_put_contents('/tmp/netbanx_error.log', "\n" . 'UserId: ' . $this->user->id . "\n" . var_export($message, true), FILE_APPEND);
						die(json_encode(array('status' => 'success', 'url' => APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-unsuccessful'))));
					}
					die(json_encode(array('status' => 'success', 'url' =>  $order->link[0]->uri)));

				}
				
				// </editor-fold>
			}
			
			die(json_encode($validator->getStatus()));

		}
	}

	public function addExpoProductAction()
	{
		$this->view->layout()->disableLayout();
		
		$is_agency = false;
		$agency_id = null;
		
		if ( $this->user->isAgency() ) {
			$is_agency = true;
			$agency_id = $this->user->agency_data['agency_id'];
			$this->view->escorts = $escorts = $this->client->call('Agencies.getActiveEscortsForProfileBoost', array($agency_id));
		} else {
			$this->view->escort_packages = $escort_packages = $this->client->call('OnlineBillingV2.getEscortPackagesForExpo', array($this->user->escort_data['escort_id']));
			
			if ( ! $escort_packages ) {
				$this->_helper->viewRenderer->setScriptAction("restriction");
				$this->view->error_message = 'You don\'t have active package.';
				return;
			}
			
			$active_package = reset($escort_packages);
			$this->view->price = $this->client->call('OnlineBillingV2.getExpoProductPrice', array($active_package['package_id']));
		}
		
		$this->view->is_agency = $is_agency;
		
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			$data = array(
				'escort_id' => $this->_request->escort_id,
				'order_package_id' => $this->_request->order_package_id,
				'start_date'		=> $this->_request->start_date,
				'end_date'	=> $this->_request->end_date
			);
			
			
			if ( $is_agency ) {
				if ( ! $data['escort_id'] ) {
					$validator->setError('escort_id', Cubix_I18n::translate('select_escort'));
				}
			} else {
				$data['escort_id'] = $this->user->escort_data['escort_id'];
			}
			
			if ( $is_agency && ! $data['order_package_id'] ) {
				$validator->setError('order_package_id', Cubix_I18n::translate('select_package'));
			}
			
			if ( ! $is_agency && ! $data['order_package_id'] ) {
				$data['order_package_id'] = $active_package['order_package_id'];
			}
			
			if ( ! $data['start_date'] || ! $data['end_date'] ) {
				$validator->setError('date_range', Cubix_I18n::translate('select_range'));
			} elseif ( $data['start_date'] < strtotime(date('Y-m-d')) || $data['end_date'] < strtotime(date('Y-m-d')) ) {
				$validator->setError('date_range', Cubix_I18n::translate('select_date_in_future'));
			} elseif ( $data['start_date'] >= $data['end_date'] ) {
				$validator->setError('date_range', Cubix_I18n::translate('invalid_range'));
			} elseif ( $data['start_date'] > strtotime('2015-10-31') ) {
				$validator->setError('date_range', 'Expo available till end of October.');
			} 
			
			$days_count = 0;
			if ( $data['start_date'] && $data['end_date'] ) {
				
				$data['start_date'] = strtotime(date('Y-m-d', $data['start_date']));
				$data['end_date'] = strtotime(date('Y-m-d', $data['end_date']));
				
				$days_count = (int)(($data['end_date'] - $data['start_date']) / (60*60*24)) + 1;
				if ( $days_count < 3 ) {
					$validator->setError('date_range', Cubix_I18n::translate('at_least_x_days'));
				}
					
				if ( $data['order_package_id'] && $data['escort_id'] ) {
					if ( $is_agency ) {
						$escort_packages = $this->client->call('OnlineBillingV2.getEscortPackagesForExpo', array($data['escort_id']));
					}
					
					$package = $escort_packages[$data['order_package_id']];
					
					if ( $data['start_date'] < strtotime($package['date_activated']) || $data['end_date'] > strtotime($package['expiration_date']) ) {
						$validator->setError('date_range', Cubix_I18n::translate('select_date_within_package_range'));
					}
					
				}
				
				
				if ( $data['escort_id'] ) {
					$booked_expo_orders = $this->client->call('OnlineBillingV2.getEscortExpoOrders', array($data['escort_id']));
					foreach($booked_expo_orders as $bo) {
						if ( $data['start_date'] <= strtotime($bo['end_date']) && $data['end_date'] >= strtotime($bo['start_date']) ) {
							$validator->setError('date_range', 'Already booked ' . $bo['start_date'] . ' - ' . $bo['end_date']);
							break;
						}
					}
				}
			}
			
			
			if ( $validator->isValid() ) {
				//getting expo product price according selected product
				$package_id = $escort_packages[$data['order_package_id']]['package_id'];
				$price = $this->client->call('OnlineBillingV2.getExpoProductPrice', array($package_id));
				$amount = $price * $days_count;
				
				$book_id = $this->client->call('OnlineBillingV2.addExpoOrder', array($data['escort_id'], $data['order_package_id'], array('from' => date('Y-m-d', $data['start_date']), 'to' => date('Y-m-d', $data['end_date']))));
				
				// <editor-fold defaultstate="collapsed" desc="Payment part">
				if ( $book_id ) {
					if ( $this->_request->payment_gateway == 'mmgbill' ) {
						$mmgBill = new Model_MmgBillAPI();
						$hosted_url = $mmgBill->getHostedPageUrl($amount, 'EXz' . $book_id);
						die(json_encode(array('status' => 'success', 'url' =>  $hosted_url)));

					} else {
						$nbx = new Model_NetbanxAPI();
						try {
						$order_data = array(
							'total_amount'	=> $amount * 100,
							'currency_code'	=> 'EUR',
							'merchant_ref_num'	=> 'EX-' . $book_id,
							'locale'	=> 'en_US',
							'callback'	=> array( 
								array(
									'format'		=> 'json',
									'rel'			=> 'on_success',
									'retries'		=> 6,
									'returnKeys'	=> array('id', 'transaction.amount', 'transaction.authType', 'transaction.status', 'transaction.currencyCode', 'transaction.merchantRefNum', 'transaction.confirmationNumber', 
										'transaction.card.brand', 'transaction.card.country', 'transaction.card.expiry', 'transaction.card.lastDigits', 'profile.paymentToken', 'profile.id'
									),
									'synchronous'	=> true,
									'uri'			=> 'http://backend.escortforumit.xxx/billing/online-billing/netbanx-callback-expo'
								)
							),
							'redirect'	=> array(
								array(
									'rel'	=> 'on_success',
									'returnKeys'	=> array('id'),
									'uri'	=> APP_HTTP. '://www.escortforumit.xxx' . $this->view->getLink('ob-successful')
								)
							),
							'link'		=> array(
								array(
									'rel'	=> 'cancel_url',
									'uri'	=> APP_HTTP. '://www.escortforumit.xxx/online-billing-v2'
								),
								array(
									'rel'	=> 'return_url',
									'uri'	=> APP_HTTP.'://www.escortforumit.xxx/online-billing-v2'
								)
							)
						);

						$order = $nbx->create_order($order_data);

						} catch(Exception $e) {
							$message = $e->getMessage();
							file_put_contents('/tmp/netbanx_error.log', "\n" . 'UserId: ' . $this->user->id . "\n" . var_export($message, true), FILE_APPEND);
							die(json_encode(array('status' => 'success', 'url' => APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-unsuccessful'))));
						}
						die(json_encode(array('status' => 'success', 'url' =>  $order->link[0]->uri)));

					}
				}
				
				// </editor-fold>
			}
			
			
			
			die(json_encode($validator->getStatus()));

		}
	}
	
	public function getEscortPackagesAction()
	{
		$escort_id = $this->_request->escort_id;
		$packages = $this->client->call('OnlineBillingV2.getEscortPackagesForExpo', array($escort_id));
		
		die(json_encode($packages));
	}
	
	public function getExpoProductPriceAction()
	{
		$package_id = $this->_request->package_id;
		$price = $this->client->call('OnlineBillingV2.getExpoProductPrice', array($package_id));
		die(json_encode(array('price' => $price)));
	}
	
}
