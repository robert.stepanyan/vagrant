<?php

class ClassifiedAdsController extends Zend_Controller_Action
{
	/**
	 * @var Zend_Session_Namespace
	 */
	protected $_session;
	protected $_perPages = array(10, 25, 50, 100);

	public function init()
	{
		$this->view->layout()->setLayout('classified-ads');
		
		$this->_session = new Zend_Session_Namespace('classified-ads');
		$this->model = new Model_ClassifiedAds();
		
		$this->view->per_pages = $this->_perPages;
	}
	
	public function indexAction()
	{
		$defines = $this->view->defines = Zend_Registry::get('defines');
		
		$this->view->cities = $this->model->getAllCitiesForFilter(Cubix_Application::getById()->country_id);
		
		
		$this->view->perPage = $perPage = $this->_getParam('pp', 25);
		
		if ( ! in_array($perPage, $this->_perPages) ) {
			$this->view->perPage = $perPage = 25;
		}
		$this->view->page = $page = 1;
		//get Classifield adds list which is not empty EFNET 1038
        $this->view->defines = $this->listClassifieldAdds($defines['classified_ad_categories']);
		$this->view->ads = $this->model->getList($filter, $page, $perPage, $count);
		$this->view->count = $count;
		
		//print_r($this->view->ads);
	}
	
	public function adAction()
	{
		$id = (int)$this->_request->id;
		
		$this->view->ad = $this->model->get($id);
		
		if ( ! isset($this->view->ad->id) ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $this->view->getLink('classified-ads-index'));
			die;
		}
	}

	public function printAction()
	{
		$this->view->layout()->disableLayout();
		
		$ad_id = (int) $this->_request->id;
		
		$this->view->ad = $this->model->get($ad_id);
	}
	
	public function ajaxFilterAction()
	{
		$defines = $this->view->defines = Zend_Registry::get('defines');
		
		$this->view->cities = $this->model->getAllCitiesForFilter(Cubix_Application::getById()->country_id);
		
		$this->view->layout()->disableLayout();
		$req = $this->_request;
        //get Classifield adds list which is not empty EFNET 1038
        $this->view->defines = $this->listClassifieldAdds($defines['classified_ad_categories']);
		$this->view->category = $category = (int)$req->category;
		$this->view->city = $city = (int)$req->city;
		$this->view->text = $text = $req->text;
	}
	
	public function ajaxListAction()
	{
		$defines = $this->view->defines = Zend_Registry::get('defines');
		$m_cities = new Cubix_Geography_Cities();
		$this->view->cities = $m_cities->ajaxGetAll(null, Cubix_Application::getById()->country_id);
		
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$this->view->perPage = $perPage = $this->_getParam('pp', 25);
		
		if ( ! in_array($perPage, $this->_perPages) ) {
			$this->view->perPage = $perPage = 25;
		}
		
		$page = $req->page;
		if ( $page < 1 ) {
			$page = 1;
		}
		$this->view->page = $page;
		
		$filter = array();
		
		$category = $req->category;
		$city = (int)$req->city;
		$text = $req->text;
		
		if ( $category ) {
			$filter['category'] = $category;
		}
		
		if ( $city ) {
			$filter['city'] = $city;
		}
		
		if ( strlen($text) ) {
			$filter['text'] = $text;
		}
		
		$this->view->ads = $this->model->getList($filter, $page, $perPage, $count);
		$this->view->count = $count;
	}
	
	public function placeAdAction()
	{
		$defines = $this->view->defines = Zend_Registry::get('defines');
		$m_cities = new Cubix_Geography_Cities();
		$blackListModel = new Model_BlacklistedWords();
		$this->view->cities = $m_cities->ajaxGetAll(null, Cubix_Application::getById()->country_id);
		
		$steps = array(1, 2, 3, 'finish');
		$edit_mode = false;
		$req = $this->_request;
		$step = $req->getParam('step', 1);
		$edit = (int) $req->getParam('edit');
		
		$plan = $req->getParam('plan', false);
		
		$data = array();
		
		if ( !in_array($step, $steps) ) {
			$step = 1;
		}
		
		if ( isset($this->_session->data) && count($this->_session->data) && $edit ) {
			$edit_mode = true;
		}
		
		if ( ! $req->isPost() && ! $edit ) {
			$this->_session->unsetAll();
		}
		
		if ( ! isset($this->_session->data) && ! count($this->_session->data) && ($step == 2 || $step == 3 || $step == 'finish') ) {
			$step = 1;
		}
		
		
		if ( $edit_mode ) {
			$data = $this->_session->data;
		}
		
		$photos = array();
		if ( count($this->_session->photos) ) {
			$photos = $this->_session->photos;
		}
		
		$errors = array();
		if ( $req->isPost() ) 
		{
			if ( ! $plan ) {
				try {
					$images = new Cubix_ImagesCommon();
					foreach ( $_FILES as $i => $file ) {
						if ( strlen($file['tmp_name']) ) {
							$photo = $images->save($file);
							if ( $photo && count($photos) < 3  ) {
								$photos[$photo['image_id']] = $photo;
							}
						}
					}
				} catch ( Exception $e ) {
					$errors['photos'][] = $e->getMessage();
				}

				$category = (int) $req->category;
				$city = (int) $req->city;
				$cities =  $req->cities;
				$phone = $req->phone;
				$email = $req->email;
				$duration = (int) $req->duration;

				$title = trim($req->title);
				$text = $req->text;

				if ( ! $category ) {
					$errors['category'] = Cubix_I18n::translate('sys_error_required');
				}

				/*if ( ! $city ) {
					$errors['city'] = Cubix_I18n::translate('sys_error_required');
				}*/
				if ( ! count($cities) ) {
					$errors['city'] = Cubix_I18n::translate('sys_error_required');
				}

				if ( ! strlen($title) ) {
					$errors['title'] = Cubix_I18n::translate('sys_error_required');
				}

				$_text = strip_tags(trim($text));
				$_text = str_replace('&nbsp;', '', $_text);
				$_text = preg_replace('/\s+/', '', $_text);
				
				if ( ! strlen($text) ) {
					$errors['text'] = Cubix_I18n::translate('sys_error_required');
				}
				else if ($bl_words = $blackListModel->checkWords($text, Model_BlacklistedWords::BL_TYPE_CLASSIFIED_ADS)){
					foreach($bl_words as $bl_word){
						if($bl_word['type'] == Model_BlacklistedWords::BL_SEARCH_TYPE_EXACT){
							$pattern = '/(\W+|^)'. preg_quote($bl_word['bl_word'], '/') . '(\W+|$)/i';
							$text = preg_replace($pattern, '\1<abbr title="Blacklisted word" class = "black-listed" >' . $bl_word['bl_word'] . '</abbr>\2', $text);
						}
						else{
							$pattern = '/' . preg_quote($bl_word['bl_word'], '/') . '/i';
							$text = preg_replace($pattern, '<abbr title="Blacklisted word" class = "black-listed" >' . $bl_word['bl_word'] . '</abbr>', $text);
						}
					}
					$errors['text'] = 'You can`t use word "'.$blackListModel->getWords().'"';
					
				}
				if ( ! strlen($phone) ) {
					$errors['phone_email'] = Cubix_I18n::translate('sys_error_required');
				} else {
					if ( $this->model->checkAdByPhone($phone) ) {
						$errors['top'] = Cubix_I18n::translate('sys_error_one_ad_per_day');
					}
				}
				
				
				/*if ( ! strlen($email) && ! strlen($phone) ) {
					$errors['phone_email'] = Cubix_I18n::translate('sys_error_phone_email_required');
				}*/

				if ( strlen($email) ) {
					$valid = new Cubix_Validator();
					
					if ( ! $valid->isValidEmail($email) ) {
						$errors['invalid_email'] = Cubix_I18n::translate('invalid_email');
					}				
				}

				$data = array(
					'category' => $category,
					//'city' => $city,
					'cities' => $cities,
					'phone' => $phone,
					'email' => $email,
					'duration' => $duration,
					'title' => $title,
					'text' => $text
				);
				
				$user = Model_Users::getCurrent();
				$data['user_id'] = null;
				if ( $user ) {
					$data['user_id'] = $user->id;
				}
				

				$this->_session->photos = $photos;

				if ( ! count($errors) ) {
					$this->_session->data = $data;
					$step = 2;
				}
			} else {
				//FINISH Stage
				$packages = $defines['classified_ad_packages_arr'];
				
				if ( !in_array($plan, $packages) ) {
					$this->_redirect($this->view->getLink('classified-ads-place-ad'));
				}
				
				/************************* IP ****************************/
				$ip = Cubix_Geoip::getIP();
				
				$data['ip'] = $ip;
				/*********************************************************/
												
				$m_ads = new Model_ClassifiedAds();
				
				$save_data = array('data' => $data, 'photos' => $photos);
					
				$ad_id = $m_ads->save($save_data);
					
				if ( $ad_id ) {
					$this->_session->unsetAll();
				}
				
				if ( $plan == 'free' ) {
					$this->_redirect($this->view->getLink('classified-ads-success') . '?id=' . $ad_id);
				} else {
					
				}
			}
		}
		
		$this->view->errors = $errors;
		$this->view->step = $step;
		$this->view->data = $data;
		$this->view->photos = $photos;
		$this->view->photos_count = count($photos);
		//var_dump($data);
		//var_dump($photos);
	}
	
	public function ajaxRemovePhotoAction()
	{
		$image_id = (int) $this->_request->image_id;
		
		$status = array('status' => 'error');
		if ( isset($this->_session->photos[$image_id]) ) {
			unset($this->_session->photos[$image_id]);
			$status = array('status' => 'success');
		}
		
		die(json_encode($status));
	}
	
	public function successAction()
	{
		$this->view->id = (int) $this->_request->id;
	}
	
	public function errorAction()
	{
		
	}

    //return Classifield adds list which is not empty EFNET 1038
    public function listClassifieldAdds($classifieldAdds){
        $tmpClassifieldAdds = array();

        foreach ($classifieldAdds as $key => $value){
            if($this->model->getList(array('category'=>$key))) {
                $tmpClassifieldAdds[$key] = $value;
            }
        }
        return (array)$tmpClassifieldAdds;
    }
}