<?php

class SkypeController extends Zend_Controller_Action
{
	public function init()
	{
		$this->model = new Model_Skype();
		$this->user = Model_Users::getCurrent();
	}

	public function updateAction()
	{
		if (is_null($this->_getParam('ajax')) ) {
			$form = new Cubix_Form_Data($this->_request);
			$fields = array(
				'escort_id' => 'int-nz',
				'30min' => 'int-nz',
				'60min' => 'int-nz',
				// 'escort_skype_username' => '',
				// 'paxum_email'=> '',
				// 'paxum_email'=> '',
				'contact_option'=> 'int-nz',
				'status'=> ''
			);

			$form->setFields($fields);

			$data = $form->getData();
			
			//permission check
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
			if($escort_id != $data['escort_id'])die('access denied');

			$validator = new Cubix_Validator();

			if($data['status'] == 'on'){
				$data['status'] = 1;
				// if ( ! strlen($data['escort_skype_username']) ) {
				// 	$validator->setError('escort_skype_username', 'Skype username is Required!');
				// }

				// if ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['paxum_email'])  ) {
				// 	$validator->setError('paxum_email', 'Paxum email is Required!');
				// }
			}else{
				$data['status'] = 0;
			}

			if ( ! strlen($data['escort_id']) ) {
				$validator->setError('subject', 'id is Required!');
			}

			if ( ! strlen($data['30min']) ) {
				$validator->setError('30min', ' Price is Required!');
			}

			if ( ! strlen($data['60min']) ) {
				$validator->setError('60min', ' Price is Required!');
			}

			if ( ! is_int($data['contact_option']) ) {
				$validator->setError('contact_option', 'Contact option is not valid!');
			}

			if ( $validator->isValid() ) {
				$status = $this->model->update($data);
				$cams_model = new Model_Api_Cams($this->user->id);
				$fields = $cams_model->prepareEscortDataForApi($this->user);
				$cams_model->escortUpdate($fields);
				die(json_encode($status));
			}else{
				die(json_encode($validator->getStatus()));
			}

		}
		else {
			$this->view->layout()->disableLayout();
			var_dump(124134);die;
		}
		die;
	}

	
}