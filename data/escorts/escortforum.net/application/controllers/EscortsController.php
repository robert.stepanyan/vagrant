<?php

class EscortsController extends Zend_Controller_Action {
	public static $linkHelper;
	
	public function init() {
		//die('This site is temporarily unavailable. Please try later ...');
		self::$linkHelper = $this->view->getHelper('GetLink');
	}
	
	public static function _itemDecorator($item, $is_first = FALSE, $is_hidden = FALSE)	{
		$has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);
		
		$classes = array();
		
		if ( $is_first ) $classes[] = 'first';
		if ( $has_childs ) $classes[] = 'sub';
		
		$link = '#';
		if ( ! $has_childs ) {
			$link = self::$linkHelper->getLink('escorts', array('filter' => $item->value, 'page' => NULL));
		}
		
		$title = $item->title;
		if ( $is_first && isset($item->title) ) {
			$title = $item->parent . ' - ' . $item->title;
		}

		//$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="' . $link . '"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ filter: ' . ($item->value ? "'" . $item->value . "'" : 'null') . ', page: null })"' : '' ) . '>' . $title . '</a>';
		$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="#"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ filter: ' . ($item->value ? "'" . $item->value . "'" : 'null') . ', page: null })"' : '' ) . '>' . $title . '</a>';

		if ( $is_first ) {
			$html = '<div class="input-w"><div class="i">' .
				$html .
			'</div></div>';
		}

		return $html;
	}
	
	public static function _sortItemDecorator($item, $is_first = FALSE, $is_hidden = FALSE) {
		$has_childs = isset($item->childs) && is_array($item->childs) && count($item->childs);
		
		$classes = array();
		
		if ( $is_first ) $classes[] = 'first';
		if ( $has_childs ) $classes[] = 'sub';
		
		$link = '#';
		if ( ! $has_childs ) {
			$link = self::$linkHelper->getLink('escorts', array('sort' => $item->value));
		}
		
		$title = $item->title;
		if ( $is_first && isset($item->title) ) {
			$title = $item->parent . ' - ' . $item->title;
		}

		//$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="' . $link . '"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ sort: \'' . $item->value . '\', page: null })"' : '' ) . '>' . $title . '</a>';
		$html = '<a ' . ($is_hidden ? 'style="display: none" ' : '') . '' . (count($classes) ? 'class="' . implode(' ', $classes) . '" ' : '') . 'href="#"' . ($item->value ? ' rel="' . $item->value . '"' : '') . ' ' . ( true ? 'onclick="return Cubix.Filter.Set({ sort: \'' . $item->value . '\', page: null })"' : '' ) . '>' . $title . '</a>';

		if ( $is_first ) {
			$html = '<div class="input-w"><div class="i">' .
				$html .
			'</div></div>';
		}

		return $html;
	}

	protected function _getFooterText($key = 'main') {
		// $model = new Model_StaticPage();

		// $lang_id = Cubix_I18n::getLang();

		// $page = $model->getBySlug('footer-text-escorts-' . $key, Cubix_Application::getId(), $lang_id);
		
		// if ( $page ) return $page->body;

		$seo = $this->view->seo('home-page-' . $key, null, array());

		if ( $seo ) {
			if ( strlen($seo->footer_text) ) {
				return $seo->footer_text;
			}
		}

		return '';
	}

	/**
	 * Chat Online Escorts Action
	 */

	public function chatOnlineAction() {
		$action = 'index';
		if ( ! is_null($this->_getParam('ajax')) ) {
			$action = 'list';
		}
		$this->_helper->viewRenderer->setScriptAction($action);

		$req = $this->_getParam('req');
		$param = explode('/', $req);
		$page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

		$m_e = new Model_EscortsV2();
		if (!$this->view->allowChat2())
			$res = $m_e->getChat11OnlineEscorts(1, 1000);
		else
			$res = $m_e->getChatNodeOnlineEscorts(1, 1000);
		$ids = $res['ids'];
		$this->view->escort_ids = explode(',', $ids);
		
		if (strlen($ids) > 0)
		{
			$filter = array(
				'e.id IN (' . $ids . ')'
			);

			$page_num = 1;
			if ( isset($page[1]) && $page[1] ) {
				$page_num = $page[1];
			}

			$count = 0;
			$escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 50, $count);
		}
		else
		{
			$count = 0;
			$escorts = array();
		}
		
		$this->view->count = $count;
		$this->view->escorts = $escorts;

		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

		$this->view->chat_online = true;
		
		$filter_body = $this->view->render('escorts/get-filter-v2.phtml');

		if ( ! is_null($this->_getParam('ajax')) ) {
			$escort_list = $this->view->render('escorts/list.phtml');
			
			die(json_encode(array('filter_body' => $filter_body, 'escort_list' => $escort_list)));
		}
	}

	/**
	 * Instant Book Escorts Action
	 */

	public function instantBookAction() {
		$action = 'index';
		if ( ! is_null($this->_getParam('ajax')) ) {
			$action = 'list';
		}
		$this->_helper->viewRenderer->setScriptAction($action);

		$req = $this->_getParam('req');
		$param = explode('/', $req);
		$page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

		$model = new Model_EscortsV2();
		
		$res = $model->getInstantBookEscortsList(1, 1000);
		
		$this->view->escort_ids = $res['ids'];
		
		if ( is_array( $res['ids'] ) && count( $res['ids'] ) > 0) {
			$ids = implode(',', $res['ids']);
			$filter = array(
				'e.id IN (' . $ids . ')'
			);

			$page_num = 1;
			if ( isset($page[1]) && $page[1] ) {
				$page_num = $page[1];

			}

			$count = 0;
			$escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 50, $count);
		} else {
			$count = 0;
			$escorts = array();
		}
		
		$this->view->count = $count;
		$this->view->escorts = $escorts;

		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

		$this->view->chat_online = true;
		
		$filter_body = $this->view->render('escorts/get-filter-v2.phtml');

		if ( ! is_null($this->_getParam('ajax')) ) {
			$escort_list = $this->view->render('escorts/list.phtml');
			
			die(json_encode(array('filter_body' => $filter_body, 'escort_list' => $escort_list)));
		}
		
	}
	
	
	/**
	 * Happy Hours action
	 */

	public function happyHoursAction() {
		$action = 'index';
		if ( ! is_null($this->_getParam('ajax')) ) {
			$action = 'list';
		}
		$this->_helper->viewRenderer->setScriptAction($action);

		$req = $this->_getParam('req');
		$param = explode('/', $req);
		$page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );

		$filter = array('e.gender = ?' => 1, 'e.hh_is_active = ?' => 1);

		$page_num = 1;
		if ( isset($page[1]) && $page[1] ) {
			$page_num = $page[1];
		}

		$count = 0;
		$escorts = Model_Escort_List::getFiltered($filter, 'random', $page_num, 10, $count);

		$this->view->count = $count;
		$this->view->escorts = $escorts;

		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();

		$this->view->hh_page = true;
	}

/**
	 * Main premium spot action
	 */

	public function mainPageAction() {
		$config = Zend_Registry::get('escorts_config');
		$sess = new Zend_Session_Namespace('main-page-premiums');
		$page = (int) $sess->page;
		if ( $page < 1 ) $page = 1;

		$limit = $page * $config['perPage'];
		$action = 'index';

		if ( ! is_null($this->_getParam('ajax')) ) {
			$action = 'list';
			$limit = $config['perPage'];
			$page++;
			$sess->page = $page;
		}else{
			$page = 1;
		}


		$this->_helper->viewRenderer->setScriptAction($action);

		$escorts = Model_Escort_List::getMainPremiumSpot($limit, $page);
		
		$this->view->escorts = $escorts;
		$this->view->no_paging = true;
		$this->view->escorts = $escorts;
		$this->view->no_paging = true;
		
		$escorts_video = array();
		if(!empty($escorts))
		{		
			$e_id = array();
			foreach($escorts as $e)
			{
				$e_id[] = $e->id;
			}
			
			$video = new Model_Video();

			$vescorts = $video->GetEscortsVideoArray($e_id);
			if(!empty($vescorts))
			{	
				$app_id = Cubix_Application::getId();
				foreach($vescorts as &$v)
				{	
					$photo = new Cubix_ImagesCommonItem(array(
						'hash'=> $v->hash,
						'width'=> $v->width,
						'height'=> $v->height,
						'ext'=> $v->ext,
						'application_id'=> $app_id
						));
				
					$v->photo = $photo->getUrl('orig');
					$escorts_video[$v->escort_id] = $v; 
				}
				
				$config = Zend_Registry::get('videos_config');
				$host =  Cubix_Application::getById(Cubix_Application::getId())->host;
				$this->view->vconfig = $config['Media'].'flv:'.$host;
				
			}
		}
		$this->view->escorts_video = $escorts_video;
		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();
		//$this->view->e_ids = Model_EscortsV2::getLateNightGirlsForMainPage();
		//print_r($this->view->e_ids);
				
		$res = Model_EscortsV2::getInstantBookEscortsList(1, 1000);
		$this->view->instantbook_escorts = $res['ids'];

		//////////////////////////////////////////////////////////////
		if ($this->view->allowChat2())
		{
			$model = new Model_EscortsV2();
			
			$res = $model->getChatNodeOnlineEscorts(1, 1000);
		
			$ids = $res['ids'];
			$this->view->escort_ids = explode(',', $ids);
		}

        /////////////////////////////////////////////////
//
		/* watched escorts */
		//$this->view->watched_escorts = $this->_watchedEscorts($escorts);
		/**/
	}

	public function watchedEscortsAction(){
        $this->view->layout()->setLayout('main-simple');
        $viewedEscorts = self::_getLatestViewedList(11);
        if(!$viewedEscorts){
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: /');
                die;
        }
        $ids = array();

        foreach ( $viewedEscorts as $e ) {
            $ids[] = $e['id'];
        }

        $filter = array('e.id IN (' . implode(', ', $ids) . ')' => array());
        $order = 'FIELD(eic.escort_id, ' . implode(', ', $ids) . ')';


        $escorts = Model_Escort_List::getFiltered($filter, $order, 1, 11, $count);

        $this->view->escorts = $escorts;
    }

	private function _watchedEscorts($escorts) {
		$ids = array();
		$watched_escorts = array();

		foreach ($escorts as $e)
		{
			$ids[] = $e->id;
		}

		if ($ids)
		{
			$ids_str = trim(implode(',', $ids), ',');
			$user = Model_Users::getCurrent();
			$modelM = new Model_Members();

			if ($user)
			{
				$escorts_watched_type = $user->escorts_watched_type;

				if (!$escorts_watched_type) $escorts_watched_type = 1;

				$res = $modelM->getFromWatchedEscortsForListing($ids_str, $escorts_watched_type, $user->id, session_id());
			}
			else
			{
				$res = $modelM->getFromWatchedEscortsForListing($ids_str, WATCH_TYPE_PER_SESSION, null, session_id());
			}

			if ($res)
			{
				foreach ($res as $r)
				{
					$watched_escorts[] = $r->escort_id;
				}
			}
		}

		return $watched_escorts;
	}

	/**
	 * New escorts action
	 */
	public function newListAction() {
		$this->view->layout()->setLayout('main-simple');

		$model = new Model_EscortsV2();
		
		$listing_type = 'list';
		if ( isset($_COOKIE['lit']) ) {
			$listing_type = $_COOKIE['lit'];
		}
		
		$req = $this->_getParam('req');
		$param = explode('/', $req);
		$page = explode("_", ( isset($param[2]) ) ? $param[2] : '' );
		
		$filter = array('e.gender = ?' => 1);
		
		$city_params = array();
		
		if (strpos($param[2], 'city') !== FALSE)
		{
			$page = explode("_", ( isset($param[3]) ) ? $param[3] : '' );
			$city_params = explode('_', $param[2]);
		}
		
		if (count($city_params) && $city_params[2])
		{
			$modelC = new Model_Cities();
			$c_id = $modelC->getBySlug($city_params[2])->id;
			$filter['e.city_id = ?'] = $c_id;
			$filter['e.is_new = ?'] = 1;
		}

		$new_cities = $model->getNewEscortsCities();
		
		$ncs = array();
		$arr = array();
		
		if ($new_cities)
		{
			foreach ($new_cities as $n)
			{
				if (!isset($arr[$n->city_id]))
					$arr[$n->city_id] = 1;
				else
					$arr[$n->city_id] ++;
				$ncs[$n->city_id] = $n;
			}
		}
		
		arsort($arr);
		$this->view->cities_counts = $arr;
		$this->view->new_cities = $ncs;
		$this->view->city_slug = $city_params[2];
		$this->view->city_id = $c_id;
		$city_params[2] =  preg_replace('#[^a-zA-Z0-9_]#', '_', $city_params[2]);

		$page_num = 1;
		if ( isset($page[1]) && $page[1] ) {
			$page_num = $page[1];
		}
		
		$params = array(
			'page' => $page_num
		);
		$this->view->params = $params;

		$count = 0;
		$per_page = ($listing_type == 'list') ? 10 : 40;
		//$escorts = Model_Escort_List::getFiltered($filter, 'newest', $page_num, 10, $count, 'new_list');
		
		$cache = Zend_Registry::get('cache');
		$new_cache_key = 'v2_new_escorts_list_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang() . '_page_' . $page_num . $listing_type . $city_params[2];
		$new_cache_key .= Cubix_Countries::blacklistCountryCacheKeyCase();
		$new_c_cache_key = 'v2_new_escorts_list_count_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang() . '_page_' . $page_num . $listing_type . $city_params[2];
		$new_c_cache_key .= Cubix_Countries::blacklistCountryCacheKeyCase();
		
		$count = $cache->load($new_c_cache_key);
		if ( ! $escorts = $cache->load($new_cache_key) ) {
			$escorts = Model_Escort_List::getFiltered($filter, 'date-activated', $page_num, $per_page, $count, 'new_list');

			$new_escorts = array();
			
			if ( count($escorts) ) {
				if ( $listing_type == 'list' ) {
					foreach ( $escorts as $k => $escort ) {
						$cache_key = 'v2_' . $escort->showname . '_new_profile_' . Cubix_I18n::getLang() . '_page_' . $page_num . '_' . $escort->id;
						$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
						$escorts[$k]->profile = $model->get($escort->id, $cache_key);
					}
				} else {
					foreach ( $escorts as $k => $escort ) {
						$new_escorts[$escort->date_activated][] = $escorts[$k]; 
					}
				}
			}
			
			if ( $listing_type == 'grid' || $listing_type == 'xl' ) {
				$escorts = $new_escorts;
			}
			
			$cache->save($escorts, $new_cache_key, array());
			$cache->save($count, $new_c_cache_key, array());
		}

		
		$this->view->count = $count;
		$this->view->escorts = $escorts;

		/* watched escorts */
		$watched_escorts = array();

		if ($listing_type != 'list')
		{
			foreach ($escorts as $escorts_inner)
			{
				$watched_escorts_buf = $this->_watchedEscorts($escorts_inner);
				$watched_escorts = array_merge($watched_escorts, $watched_escorts_buf);
			}
		}
		else
		{
			$watched_escorts = $this->_watchedEscorts($escorts);
		}

		$this->view->watched_escorts = $watched_escorts;
		/**/
		
		$this->view->new_list_cache_key = 'new_list_' . $cache_key;
		
		if ( $listing_type == 'grid' || $listing_type == 'xl' ) {
			$this->_helper->viewRenderer->setScriptAction('new-list-listing');
		}
	}
	
	public function getFilterV2Action($existing_filters = array()) {
		$this->view->layout()->disableLayout();

		if ( ! is_null($this->_getParam('ajax')) && ! is_null($this->_getParam('filter_params')) ) {
			$filter_params = (array) json_decode(base64_decode($this->_request->filter_params));
			$is_upcomingtour = $this->_request->is_upcomingtour;
			$is_tour = $this->_request->is_tour;
			
			$existing_filters = Model_Escort_List::getActiveFilter($filter_params, $current_filter, $is_upcomingtour, $is_tour);
		}
		
		$default_sorting = "r";
		$current_sorting = $this->_request->getParam('sort', $default_sorting);
		
		$sort_types = array(
			'r' => array('title' => 'random', 'map' => 'random'),
			'a' => array('title' => 'alphabetically', 'map' => 'alpha'),
			'mv' => array('title' => 'most_viewed', 'map' => 'most-viewed'),
			'lm' => array('title' => 'last_modified', 'map' => 'last-modified'),
			'n' => array('title' => 'newest_first', 'map' => 'newest'),
			
			//'pa' => array('title' => 'price_ascending', 'map' => 'price-asc'),
			//'pd' => array('title' => 'price_descending', 'map' => 'price-desc'),
			'ca' => array('title' => 'city_ascending', 'map' => 'city-asc'),
		);

		if ( !array_key_exists($current_sorting, $sort_types) ) {
			$current_sorting = $default_sorting;
			$this->_request->setParam('sort', $default_sorting);
		}

		if ( $current_sorting != $_COOKIE['sorting'] && $this->_request->isPost() ) {
			setcookie("sorting", $current_sorting, strtotime('+1 year'), "/", "." . Cubix_Application::getById()->host);
			setcookie("sortingMap", $sort_types[$current_sorting]['map'], strtotime('+1 year'), "/", "." . Cubix_Application::getById()->host);
			$_COOKIE['sorting'] = $current_sorting;
			$_COOKIE['sortingMap'] = $sort_types[$current_sorting]['map'];
		}
		
		if ( isset($_COOKIE['sorting']) && ! $this->_request->isPost() ) {
			$current_sorting = $_COOKIE['sorting'];
			$this->_request->setParam('sort', $current_sorting);
		}
		
		if ( $current_sorting ) {
			$tmp = $sort_types[$current_sorting];
			unset($sort_types[$current_sorting]);
			$sort_types = array_merge(array( $current_sorting => $tmp), $sort_types);
		}
		
		$this->view->sort_types = $sort_types;
		$this->view->params_sort = $sort_types[$current_sorting]['map'];
		$this->view->current_sorting = $current_sorting;
		$this->view->name = $this->_request->name;
		$this->view->video = $this->_request->video;
        $this->view->reviews = $this->_request->reviews;
        $this->view->is_vip = $this->_request->is_vip;
        $this->view->vr = $this->_request->vr;
		$this->view->has_nat_pic = $this->_request->has_nat_pic;
        $this->view->skype_call_status = $this->_request->skype_call_status;
        $this->view->with_virtual_services = $this->_request->with_virtual_services;

		$mapper = new Cubix_Filter_Mapper();
		
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 's', 'filter' => 'Service', 'field' => 'es.service_id', 'filterField' => 'fd.services', 'queryJoiner' => 'OR', 'weight' => 10)) );
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'af', 'filter' => 'AvailableFor', 'field' => 'e.available_for_type', 'queryJoiner' => 'OR', 'weight' => 20)) );
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'sf', 'filter' => 'ServiceFor', 'field' => 'e.sex_availability', 'filterField' => 'fd.service_for', 'queryJoiner' => 'OR', 'weight' => 30)) );
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'cs', 'filter' => 'CupSize', 'field' => 'e.cup_size', 'filterField' => 'fd.cup_size', 'queryJoiner' => 'OR', 'weight' => 40)) );
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'a', 'filter' => 'Age', 'field' => 'e.age', 'filterField' => 'fd.age', 'weight' => 50)) );
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'l', 'filter' => 'Language', 'field' => 'e.languages', 'filterField' => 'fd.language', 'queryJoiner' => 'AND', 'weight' => 60)) );
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'n', 'filter' => 'Nationality', 'field' => 'e.nationality_id', 'filterField' => 'fd.nationality', 'queryJoiner' => 'OR', 'weight' => 65)) );
				
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'hc', 'filter' => 'HairColor', 'field' => 'e.hair_color', 'filterField' => 'fd.hair_color', 'queryJoiner' => 'OR', 'weight' => 80)) );
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'hl', 'filter' => 'HairLength', 'field' => 'e.hair_length', 'filterField' => 'fd.hair_length', 'queryJoiner' => 'OR', 'weight' => 81)) );
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'h', 'filter' => 'Height', 'field' => 'e.height', 'filterField' => 'fd.height', 'weight' => 90)) );
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'w', 'filter' => 'Weight', 'field' => 'e.weight', 'filterField' => 'fd.weight', 'weight' => 100)) );
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'ec', 'filter' => 'EyeColor', 'field' => 'e.eye_color', 'filterField' => 'fd.eye_color', 'queryJoiner' => 'OR', 'weight' => 110)) );
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'sm', 'filter' => 'Smoker', 'field' => 'e.is_smoking', 'filterField' => 'fd.smoker', 'queryJoiner' => 'OR', 'weight' => 120)) );
		$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'e', 'filter' => 'Ethnicity', 'field' => 'e.ethnicity', 'filterField' => 'fd.ethnicity', 'queryJoiner' => 'OR', 'weight' => 130)) );
		//$mapper->setParam(new Cubix_Filter_Mapper_Parameter(array('name' => 'v', 'filter' => 'Verified', 'field' => 'e.verified_status', 'filterField' => 'fd.verified', 'queryJoiner' => 'AND', 'weight' => 140)) );
		
		$filter = new Cubix_FilterV2( array('mapper' => $mapper->getAll(), 'request' => $this->_request, 'existingFilters' => $existing_filters) );

		//if ( ! count($existing_filters) )
		$this->view->filter = $filter->render();


		return $filter->makeQuery();
	}

	public function indexAction() {

		// <editor-fold defaultstate="collapsed" desc="If it is ajax request, disable the layout and render only list.phtml">
		/*$is_ajax = false;
		if ( ! is_null($this->_getParam('ajax')) ) {
			$is_ajax = true;
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setScriptAction('list');
		}*/
		// </editor-fold>

		$this->view->quick_links = true;
		$upcoming_tours = false;

		$this->view->req = $req = $this->_getParam('req');

		$cc_slug = $this->_getParam('city');
		if ( $cc_slug ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /escorts/city_it_' . $cc_slug);
			die;
		}
		
		if ( preg_match('#^/[0-9]{1,5}#', $this->view->req) ) {
			//echo $this->view->req;
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
		}
		
		/**/
		if (strlen($_SERVER['REQUEST_URI']) > 1) {
			$a = explode('/', $_SERVER['REQUEST_URI']);

			if (!end($a))
			{
				$new_url = substr_replace($_SERVER['REQUEST_URI'], "", -1);
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: ' . $new_url);
				die;
			}
		}
		/**/
		
		if ( is_null($this->_getParam('ajax')) && preg_match('#sort#', $req) ) {
			$pos = strpos($req, 'sort');
			$sec_part = substr($req, $pos);			
			$sec_pos = strpos($sec_part, '/');
			
			if ( $sec_pos ) {
				$first = substr($req, 0, $pos - 1);
				$sec = substr($req, $pos + $sec_pos);
				$req = $first . $sec;
			}
			else {
				$req = substr($req, 0, $pos);
			}
			
			if ( ! strpos($req, 'escorts') ) {
				$req = '/escorts' . $req;
			}
			
			if (preg_match('#(/\d{1}/)|(/\d{2}/)|(/\d{3}/)$#', $req) ) {
				$req = preg_replace('#(/\d{1}/)|(/\d{2}/)|(/\d{3}/)$#', '', $req);
			}
			
			if ( Cubix_I18n::getLang() != Cubix_Application::getDefaultLang() ) {
				$req = '/' . Cubix_I18n::getLang() . $req;
			}
			
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $req);
			die;
		}

		if ( is_null($this->_getParam('ajax')) && preg_match('#filter#', $req) ) {
			$pos = strpos($req, 'filter');
			$sec_part = substr($req, $pos);			
			$sec_pos = strpos($sec_part, '/');
			
			if ( $sec_pos ) {
				$first = substr($req, 0, $pos - 1);
				$sec = substr($req, $pos + $sec_pos);
				$req = $first . $sec;
			}
			else {
				$req = substr($req, 0, $pos);
			}
			
			if ( ! strpos($req, 'escorts') ) {
				$req = '/escorts' . $req;
			}
			
			if (preg_match('#(/\d{1}/)|(/\d{2}/)|(/\d{3}/)$#', $req) ) {
				$req = preg_replace('#(/\d{1}/)|(/\d{2}/)|(/\d{3}/)$#', '', $req);
			}
			
			if ( Cubix_I18n::getLang() != Cubix_Application::getDefaultLang() ) {
				$req = '/' . Cubix_I18n::getLang() . $req;
			}
			
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $req);
			die;
		}


		// <editor-fold defaultstate="collapsed" desc="Fix old urls">
		$a = array();
		if ( preg_match('#(/(city|region)_[a-z]{2}_)([^/]+)#', $req, $a) ) {
			array_shift($a);
			
			if( preg_match('#_#', $a[2]) ) {
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: /');
				die;
			}
			
			$a[2] = str_replace('_', '-', $a[2]);
			$req = preg_replace('#(/(city|region)_[a-z]{2}_)([^/]+)#', "$1{$a[2]}", $req);
		}
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Fix for removing page_# from url">
		if ( preg_match('#/([0-9]+)$#', $req, $a) ) {
			$a[1] = intval($a[1]) + 1;
			$req = preg_replace('#/[0-9]+$#', '/page_' . $a[1], $req);
		}

		$has_filter = false;
		if ( preg_match('#^/(filter|sort|name)_(.+)#', $req) ) {
			$req .= '/regular' . $req;
			$has_filter = true;
		}
		
		if ( preg_match('#/(city|region)_[a-z]{2}_([^/]+)/regular#', $req, $b) ) {
			list($full, $r_type, $r_slug) = $b;
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /escorts/regular/' . $r_type . '_it_' . $r_slug);
			die;
		}
		
		/* remove US country code */
		if( preg_match('#city_us_#', $req) ) {
			
			$req = preg_replace('#city_us_#', 'city_it_', $req);
			
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /escorts' . $req);
			die;
		}

		$this->_request->setParam('req', $req);
		// </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Parse out parameters from "pretty" urls">
		$req = explode('/', $req);

		$menus = array('filter' => NULL, 'sort' => NULL);

		$cache = Zend_Registry::get('cache');
		$defs = Zend_Registry::get('definitions');

		$filterStructure = $defs['escorts_filter'];

		$menus['filter'] = new Cubix_NestedMenu(array('childs' => $filterStructure));
		$menus['filter']->setId('filter-options');
		$menus['filter']->setSelected($menus['filter']->getByValue(NULL));
		
		$sc = count($defs['escorts_sort']);
		unset($defs['escorts_sort'][$sc-2]);
		unset($defs['escorts_sort'][$sc-1]);
		
		$menus['sort'] = new Cubix_NestedMenu(array('childs' => $defs['escorts_sort']));
		$menus['sort']->setId('sort-options');
		$menus['sort']->setSelected($menus['sort']->getByValue('random'));
		$this->view->menus = $menus;

		$params = array(
			'sort' => 'random',
			'filter' => array(array('field' => 'girls', 'value' => array())),
			'page' => 1
		);

		$static_page_key = 'main';
		$is_tour = false;
		$is_city_tour = false;
		
		foreach ( $req as $r ) {
			if ( ! strlen($r) ) continue;
			$param = explode('_', $r);

			if ( count($param) < 2 ) {
				
				switch( $r ) {
					case 'virtual':
                        $static_page_key = 'virtual';
					break;
					case 'nuove':
						//$params['filter'][] = array('field' => 'new_arrivals', 'value' => array());
						$static_page_key = 'nuove';
						$param = array('sort', 'newest');
					break;
					case 'independantes':
						$static_page_key = 'independantes';
						$params['filter'][] = array('field' => 'independantes', 'value' => array());
					continue;
					case 'regular':
						$static_page_key = 'regular';
						$params['filter'][] = array('field' => 'regular', 'value' => array());
					continue;
					case 'agence':
						$static_page_key = 'agence';
						$params['filter'][] = array('field' => 'agence', 'value' => array());
					continue;
					case 'boys':
						$static_page_key = 'boys';
						$params['filter'][0] = array('field' => 'boys', 'value' => array());
					continue;
					case 'trans':
						$static_page_key = 'trans';
						$this->view->is_trans = $is_trans = true;
						$params['filter'][0] = array('field' => 'trans', 'value' => array());
					continue;
					case 'tours':
						$static_page_key = 'tours';
						$this->view->is_tour = $is_tour = true;
						$is_city_tour = true;
						$params['filter'][] = array('field' => 'tours', 'value' => array());
					continue;
					case 'upcomingtours':
						$static_page_key = 'upcomingtours';
						$this->view->is_tour = $is_tour = true;
						$this->view->is_upcomingtour = true;
						$upcoming_tours = true;
						$params['filter'][] = array('field' => 'upcomingtours', 'value' => array());
					continue;
					case 'happy-hours':
						$static_page_key = 'happy-hours';
					continue;
					case 'chat-online':
						$static_page_key = 'chat-online';
					continue;
					case 'instant-book':
						$static_page_key = 'instant-book';
					continue;
					case 'videos':
						$static_page_key = 'videos';
						$params['filter'][] = array('field' => 'videos', 'value' => array());
					continue;
					case 'expo':
						$static_page_key = 'expo';
						$this->view->is_expo = true;
						$params['filter'][] = array('field' => 'expo', 'value' => array());
												
						if ( count($req) > 2 ) {
							$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
							$this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
							return;
						}
					continue;
					/*case 'blank.html':
						$this->_forward('blank-html', 'redirect');
						return;*/
                    case 'skype-available':
                        $static_page_key = 'skype-available';
                        $this->view->skype_available = $skype_available = true;
                        $is_city_tour = false;
                        $params['filter'][] = array('field' => 'skype-available', 'value' => '1');
                        continue;
                    case 'valentine':
                    	$static_page_key = 'valentine';
                    	continue;
					default:
						$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
						$this->_forward('error', 'error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
						return;
				}
			}

			$param_name = $param[0];
			array_shift($param);


			if ( $static_page_key == 'nuove' ) {
				$this->_forward('new-list');
				return;
			}

			
			if ( $static_page_key == 'videos' ) {
				$this->_forward('videos');
				return;
			}
			
			if ( $static_page_key == 'valentine' ) {
				// header('HTTP/1.1 301 Moved Permanently');
				// header('Location: /');
				// die;
				$this->_forward('valentine');
				return;
			}

			switch ( $param_name ) {
				case 'filter':
					$has_filter = true;
					/* >>> For nested menu */
					$selected_item = $menus['filter']->getByValue(implode('_', $param));
					if ( ! is_null($selected_item) ) {
						$menus['filter']->setSelected($selected_item);
					}
					/* <<< */
					
					$field = reset($param);
					array_shift($param);
					
					$value = array_slice($param, 0, 2);
					
					$params['filter'][] = array('field' => $field, 'value' => $value , 'main' => TRUE);
				break;
				case 'page':
					$page = intval(reset($param));
					if ( $page < 1 ) {
						$page = 1;
					}
					//FOR city premium spot paging
					if($only_regular && $page > 1 ){
						$params['page'] = $page - 1;
					}
					else{
						$params['page'] = $page;
					}
				break;
				case 'sort':
					$params['sort'] = reset($param);
					
					$selected_item = $menus['sort']->getByValue($params['sort']);
					if ( ! is_null($selected_item) ) {
						$menus['sort']->setSelected($selected_item);
					}
					$has_filter = true;
				break;
				case 'region':
				case 'state':
					$params['country'] = $param[0];
					$params['region'] = $param[1];
					$params['filter'][] = array('field' => 'region', 'value' => $param[1]);
				break;
				case 'city':
				case 'zone':
					$params[$param_name] = $param[1];
					$params['filter'][] = array('field' => $param_name, 'value' => array($param[1]));
					//print_r($params['filter']);
				break;
				case 'name':
					$has_filter = true;
					$srch_str = "";
					if ( count($req) ) {
						foreach( $req as $r ) {
							if ( preg_match('#name_#', $r) ) {
								$srch_str = str_replace("name_", "", $r);
								$srch_str = str_replace("_", "\_", $srch_str);
							}
						}
					}
					
					$params['filter'][] = array('field' => 'name', 'value' => array('%' . $srch_str . '%'));
				break;
				case 'reg':
					$only_regular = ( $param[0] == 'true') ? true : false;
				break;
				default:
					if ( ! in_array($param_name, array('nuove','virtual', 'independantes', 'regular', 'agence', 'boys', 'trans', 'tours', 'upcomingtours', 'happy-hours', 'chat-online', 'expo', 'instant-book', 'skype-available')) ) {
						$this->_response->setRawHeader('HTTP/1.1 404 Not Found');
						$this->_forward('error','error', null, array('error_msg' => '404 Page Not Found. Please try the following links instead.'));
						return;
					}
			}
		}

		// </editor-fold>
		
		$filter_params = array(
			'order' => 'e.date_registered DESC',
			'limit' => array('page' => 1),
			'filter' => array()
		);

        if ( $static_page_key == 'virtual') {
            $filter_params['filter']['with_virtual_services'] = 1;
        }

		// FOR V2 FILTER
		if ( $this->_request->isPost()){
			
			if($this->_request->name ) {
				$has_filter = true;
				$name = $this->_request->name;
				$srch_str = "";

				$srch_str = str_replace("_", "\_", $name);	

				//$params['filter'][] = array('field' => 'name', 'value' => array('%' . $srch_str . '%'));
				$filter_params['filter']['e.showname LIKE ?'] = '%' . $srch_str . '%';
			}
			if( $this->_request->video ) {
				$filter_params['filter']['video'] = 1;
			}

            if( $this->_request->reviews ) {
                $filter_params['filter']['review_count'] = 1;
            }

            if( $this->_request->is_vip ) {
                $filter_params['filter']['is_vip'] = 1;
            }

            if( $this->_request->vr ) {
                $filter_params['filter']['verified_status'] = 2;
            }
			
			if ($this->_request->has_nat_pic ) {
				$filter_params['filter']['has_nat_pic'] = 1;
			}

            if ($this->_request->skype_call_status ) {
                $filter_params['filter']['skype_call_status'] = 1;
            }

            if ($this->_request->with_virtual_services ) {
                $filter_params['filter']['with_virtual_services'] = 1;
            }
		}
				
		// if ( ! is_null($this->_getParam('ajax')) && $static_page_key == 'main' && ! $this->_getParam('main_premium_spot') ) {
		// 	$static_page_key = 'regular';
		// }
		// FOR V2 FILTER

		$only_regular = $this->_getParam('reg');
		
		$this->view->static_page_key = $static_page_key;

		// <editor-fold defaultstate="collapsed" desc="Map parameters from url to SQL where clauses">
		$filter_map = array(
			'verified' => 'e.verified_status = 2',
			'italian' => 'e.nationality_id = 22',

			'age' => 'e.age < ? AND e.age > ?',
			'ethnic' => 'e.ethnicity = ?',
			'height' => 'e.height < ? AND e.height > ?',
			'weight' => 'e.weight < ? AND e.weight > ?',
			'cup-size' => 'e.cup_size = ?',
			'hair-color' => 'e.hair_color = ?',
			'hair-length' => 'e.hair_length = ?',
			'eye-color' => 'e.eye_color = ?',
			'dress-size' => 'e.dress_size = ?',
			'shoe-size' => 'e.shoe_size = ?',
			'available-for-incall' => 'e.incall_type IS NOT NULL',
			'available-for-outcall' => 'e.outcall_type IS NOT NULL',
			'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
			'smoker' => 'e.is_smoking = ?',
			'language' => 'FIND_IN_SET(?, e.languages)',
			'now-open' => 'e.is_now_open',
			'region' => 'r.slug = ?',
			'city' => 'ct.slug = ?',
			'cityzone' => 'c.id = ?',
			'zone' => 'cz.slug = ?',
			'name' => 'e.showname LIKE ?',

			'new_arrivals' => 'e.gender = ' . GENDER_FEMALE,
			'independantes' => 'eic.is_agency = 0 AND eic.gender = ' . GENDER_FEMALE,
			'agency' => 'eic.is_agency = 1 AND eic.gender = ' . GENDER_FEMALE,
			'boys' => 'eic.gender = ' . GENDER_MALE,
			'trans' => 'eic.gender = ' . GENDER_TRANS,
			'girls' => 'eic.gender = ' . GENDER_FEMALE,
			'expo'	=> 'FIND_IN_SET(' . MILAN_EXPO_PRODUCT_ID . ', e.products) > 0',
			
			'tours' => 'eic.is_tour = 1',
			'skype-available' => 'e.skype_call_status = ?',
			'upcomingtours' => 'eic.is_upcoming = 1'
		);

		// Services SQL map
		$services_map = array(
			// English Version
			'en' => array(
				'svc-69-position', 'svc-anal-sex', 'svc-cum-in-mouth',
				'svc-cum-on-face', 'svc-dildo-play-toys', 'svc-french-kissing',
				'svc-girlfriend-experience--gfe-', 'svc-kissing', 'svc-oral-with-condom',
				'svc-oral-without-condom', 'svc-oral-without-condom-to-completion',
				'svc-sex-in-different-positions'
			),

			// French Version
			'it' => array(
				'svc-69-posizione', 'svc-sesso-anale', 'svc-cum-in-bocca', 
				'svc-cum-sulla-faccia', 'svc-dildo-play-toys',
				'svc-french-kissing', 'svc-girlfriend-experience--gfe-',
				'svc-bacio', 'svc-pompini-con-preservativo',
				'svc-pompino-senza-preservativo', 'svc-pompino-scoperto-fino-in-fondo',
				'svc-sesso-in-posizioni-diverse'
			)
		);

		foreach ( $services_map[Cubix_I18n::getLang()] as $value ) $filter_map[$value] = 'es.service_id = ?';
		
		// <editor-fold defaultstate="collapsed" desc="Get page from request parameters">
		if ( $this->_request->page ) {
			$params['page'] = $this->_request->page;
		}
		
		$page = intval($params['page']);
		if ( $page == 0 ) {
			$page = 1;
		}
		$filter_params['limit']['page'] = $page;
		
		// </editor-fold>

		$selected_filter = $menus['filter']->getSelected();
		
		foreach ( $params['filter'] as $i => $filter ) {

			if ( ! isset($filter_map[$filter['field']]) ) continue;
			
			$value = $filter['value'];
			
			if ( isset($filter['main']) ) {
				if ( isset($selected_filter->internal_value) ) {
					$value = $selected_filter->internal_value;
					
					if ( ! is_int($value) ) {
						$value = $selected_filter->title;
					}
				}				
				elseif ( ! is_null($item = $menus['filter']->getByValue($filter['field'] . ( (isset($value[0]) && $value[0]) ? '_' . $value[0] : '')) ) ) {
					$value = $item->internal_value;
					if ( ! is_int($value) ) {
						$value = $selected_filter->title;
					}
				}

			}

			$filter_params['filter'][$filter_map[$filter['field']]] = $value;
		}

		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Sorting parameters SQL mappings">
		$sort_map = array(
			'price-asc' => 'price-asc', // will be modified from escorts
			'price-desc' => 'price-desc', // will be modified from escorts
			'random' => 'ordering DESC',
			'alpha' => 'showname ASC',
			'most-viewed' => 'hit_count DESC',
			'newest' => 'date_registered DESC',
			'last-modified' => 'date_last_modified DESC'
		);
		
		/* NEW SORT MAP */
		$n_sort = $this->_request->sort;
		if ( $this->_request->isPost() && $n_sort ) {
			$new_sort_map = array(
				'mv' => 'most-viewed',
				'n'  => 'newest',
				'lm' => 'last-modified'
			);
			
			if ( isset($new_sort_map[$n_sort]) ) {
				$this->view->sort_param = $new_sort_map[$n_sort];
			}			
		}
		/* NEW SORT MAP */
		
		/*if ( isset($sort_map[$params['sort']]) ) {
			$filter_params['order'] = $sort_map[$params['sort']];
			$this->view->sort_param = $params['sort'];
		}*/
		// </editor-fold>
		
		$model = new Model_EscortsV2();
		$count = 0;

		// <editor-fold defaultstate="collapsed" desc="If it is ajax request, disable the layout and render only list.phtml">
		$has_filter = false;
        if ( ! is_null($this->_getParam('ajax')) ) {
			
			$current_filter = $this->getFilterV2Action();
			
			$existing_filters = Model_Escort_List::getActiveFilter($filter_params['filter'], $current_filter, $upcoming_tours, $is_tour);
			
			$where_query = $this->getFilterV2Action($existing_filters);
			
			$filter_body = $this->view->render('escorts/get-filter-v2.phtml');
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setScriptAction('list');
			
			if ( count($existing_filters['where_query']) ) {
				$has_filter = true;
			}
		}
		// </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Cache key generation FOR OLD FILTER">
		/*$filters_str = '';
		
		foreach ( $filter_params['filter'] as $k => $filter ) {
			if ( ! is_array($filter) ) {
				$filters_str .= $k . '_' . $filter;
			}
			else {
				$filters_str .= $k;
				foreach($filter as $f) {
					$filters_str .= '_' . $f;
				}
			}
		}*/
		// </editor-fold>
		
		// <editor-fold defaultstate="collapsed" desc="Cache key generation FOR NEW FILTER">
		$filters_str = '';
		$f_params = $this->_request->getParams();
		$f_params_exclude = array('lang_id', 'module', 'action', 'controller', 'ajax');
		
		foreach ( $filter_params['filter'] as $k => $filter ) {
			if ( ! is_array($filter) ) {
				$filters_str .= $k . '_' . $filter;
			}
			else {
				$filters_str .= $k;
				foreach($filter as $f) {
					$filters_str .= '_' . $f;
				}
			}
		}
		
		foreach ( $f_params as $k => $value ) {
			if ( ! in_array($k, $f_params_exclude) ) {
				if ( ! is_array($value) ) {
				$filters_str .= $k . '_' . $value;
				}
				else {
					$filters_str .= $k;
					foreach($value as $f) {
						$filters_str .= '_' . $f;
					}
				}
			}


		}

		// </editor-fold>
		
		if ( isset($_COOKIE['sortingMap']) && ! $this->_request->isPost() ) {
			$params['sort'] = $this->view->params_sort = $_COOKIE['sortingMap'];
		} else {
			if ( $this->view->params_sort ) {
				$params['sort'] = $this->view->params_sort;
			} else {
				$params['sort'] = 'random';
			}
		}

		
		$cache_key = 'v2_' . Cubix_I18n::getLang() . '_' . 	$params['sort'] . '_' . $filter_params['limit']['page'] . '_' . $filters_str . (string) $has_filter;
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
		$this->view->params = $params;        

		$this->view->is_main_page = $is_main_page = ($static_page_key == 'main' && ( ! isset($params['city']) && ! isset($params['zone']) && ! isset($params['region']) ));

		$this->view->filter_params = $filter_params['filter'];
		// If we are on main premium spot forward to corresponding action
		if ( ($is_main_page && is_null($this->_getParam('ajax'))) || ($is_main_page && $this->_getParam('is_scroll'))) {			
			return $this->_forward('main-page');
		}
		

		if ( $static_page_key == 'happy-hours' ) {
			return $this->_forward('happy-hours');
		}
		
		if ( $static_page_key == 'chat-online' ) {
			return $this->_forward('chat-online');
		}

		if ( $static_page_key == 'instant-book' ) {
			return $this->_forward('instant-book');
		}
		$city_premium_spot = false;
		//$p_cities = array('milano', 'roma', 'torino', 'bologna');
		//$this->view->city_premium_spot = $city_premium_spot = (isset($filter_params['filter']['ct.slug = ?']) && in_array($filter_params['filter']['ct.slug = ?'][0], $p_cities) ) ? true : false;
		$this->view->city_slug =  $filter_params['filter']['ct.slug = ?'][0];
		
		//SHOW UPCOMING TOURS BUTTON LOGIC
		$cache_key_tours = 'v2_upcoming_tours_count';
		$t_key = isset($params['city']) ? $params['city'] : 'total';
		$tours_count = $cache->load($cache_key_tours);
		if ( ! $tours_count || ! isset($tours_count[$t_key]) ) {
			if ( ! $tours_count ) $tours_count = array();
			$upcoming_filter = array();
			if ( isset($filter_params['filter']['ct.slug = ?']) ) {
				$upcoming_filter['ct.slug = ?'] = $filter_params['filter']['ct.slug = ?'];
			}
			
			Model_Escort_List::getTours(true, $upcoming_filter, 'random', 1, 45, $t_count, $where_query);
			
			$tours_count[$t_key] = $t_count;
			$cache->save($tours_count, $cache_key_tours, array());
		}
		$this->view->show_tours_btn = false;
		if ( ( ! $is_tour || $is_city_tour ) && $tours_count[$t_key] ) {
			$this->view->show_tours_btn = true;
		}
		
		$this->view->only_regular = $only_regular;
		
		$r = 'not_only_reg';
		if ( $only_regular ) {
			$r = 'only_reg';
		}
		
		$cache_key .= $r;
		$cache_key .= Cubix_Countries::blacklistCountryCacheKeyCase();

		if(strlen($cache_key) >= 255) {
            $cache_key = sha1($cache_key);
        }

		$count = $cache->load($cache_key . '_count');
		
		if ( ! $escorts = $cache->load($cache_key) ) {
			if ( isset($is_tour) && $is_tour ) {
				//$premiums = $model->getAllTourMainPremiumSpot($filter_params, $count, $upcoming_tours);
				$escorts = Model_Escort_List::getTours($upcoming_tours, $filter_params['filter'], $params['sort'], $filter_params['limit']['page'], 45, $count, $where_query);
			}
			else {
				
				
				if ( $city_premium_spot && ! $only_regular && !$has_filter && !isset($filter_params['filter']['e.showname LIKE ?'])) {
					
					$escorts = Model_Escort_List::getCityPremiumSpot($filter_params['filter'], $params['sort'], 'city_premium_list');
					$count = Model_Escort_List::getCityEscortsCount($filter_params['filter']['ct.slug = ?'][0], 2 ) + 45;
					
					if ( ! count($escorts) ) {
						$escorts = Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $filter_params['limit']['page'], 45, $count, 'regular_list', $only_regular, $where_query);
						$this->view->only_regular = true;
						$this->view->city_premium_spot = false;
					}
				}
				else {
					
					if( $has_filter ) {
						$only_regular = false;
					}
					
					if ( $only_regular ) {
						$page = $filter_params['limit']['page'] - 1;
					} else {
						$page = $filter_params['limit']['page'];
					}
					$filter_params['filter']['eic.is_dublicate_tour = ?'] = 0;
                    $escorts = Model_Escort_List::getFiltered($filter_params['filter'], $params['sort'], $page, 45, $count, 'regular_list', $only_regular, $where_query);
                }
			}
			$cache->save($escorts, $cache_key, array());
			$cache->save($count, $cache_key . '_count', array());
		}

		// Redirect to city page if old paging url
		if ($filter_params['filter']['ct.slug = ?'][0] && $param_name == 'page')
		{			
			$uri = $this->_request->req;
			$ar = explode('/', $uri);
			$c = count($ar);
			unset($ar[$c - 1]);
			$uri = implode('/', $ar);
			
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /escorts' . $uri);
			die;
		}
		//////////////////////////////////////////
		
		// Redirect to main page if no escort found in city
		if ($param_name == 'city')
		{
			$old_slug = $filter_params['filter']['ct.slug = ?'][0];
			
			if (!$old_slug)
			{
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: /');
				die;
			}
			
			$new_slug = Cubix_Geography_Cities::getSlugByOldSlug($old_slug);
			
			if ($new_slug && strlen($new_slug->slug) && $new_slug->slug != $old_slug)
			{
				$uri = $this->_request->req;
				$uri = preg_replace("#" . $old_slug . "#", $new_slug->slug, $uri);
				
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: /escorts' . $uri);
				die;
			}
			elseif ($count == 0 && is_null($this->_getParam('ajax')) )
			{
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: /');
				die;
			}
		}
		//////////////////////////////////////////////////////


		//FOR city premium spot paging
		if ( $city_premium_spot && $only_regular ) {
			//$params['page'] += 1;			
			$this->view->params = $params;
			$count += 45;
		}
		
		// <editor-fold defaultstate="collapsed" desc="Find and Slice Premium Escorts">
		$premiums = $premiums_fill = array();
		if ( $filter_params['order'] == 'ordering DESC' && ! $has_filter ) {
			foreach ( $escorts as $i => $escort ) {
				if ( ! $escort->is_premium && $i == 0 ) {
					break;
				}
				elseif ( $escort->is_premium && $i == count($escorts) - 1 ) {
					$premiums = $escorts;
					$escorts = $premium_fill = array();
				}

				if ( ! $escort->is_premium ) {
					$c = intval(($i - 1) / 5);
					if ( $c == 0 ) {
						$this->view->one_row = true;
					}
					$c = 5 - ($i - $c * 5);
					$this->view->fake_escorts_count = $c;

					$escorts[$i]->is_first_one = true;
					$premiums = array_slice($escorts, 0, $i);
					$premiums_fill = array_slice($escorts, $i, $c);
					$escorts = array_slice($escorts, $i + $c);

					break;
				}
			}
		}
		// </editor-fold>

		if ( $is_main_page && $filter_params['order'] == 'ordering DESC' ) {
			shuffle($escorts);
		}		

		//var_dump($count);
		$this->view->count = $count;
		$this->view->escorts = $escorts;

		$this->view->premiums = $premiums;
		$this->view->premiums_fill = $premiums_fill;
		$this->view->has_filter = $has_filter;

		/* watched escorts */
		$watched_escorts = $this->_watchedEscorts($escorts);
		$watched_escorts_p = $this->_watchedEscorts($premiums);
		$watched_escorts_pf = $this->_watchedEscorts($premiums_fill);
		$watched_escorts = array_merge($watched_escorts, $watched_escorts_p, $watched_escorts_pf);

		$this->view->watched_escorts = $watched_escorts;
		/**/

		// <editor-fold defaultstate="collapsed" desc="Premiums on right side">
		if ( ! count($premiums) && ! $is_tour && $static_page_key != 'nuove' && $static_page_key != 'boys' )
		{
			$rows = ceil(count($escorts) / 5);
			$per_page = $rows - 1;
			if ( $per_page < 0 ) {
				$per_page = 0;
			}
			
			$right_premiums = Model_Escort_List::getMainPremiumSpot();
			$right_premiums = array_slice($right_premiums, 0, $per_page);
			$this->view->right_premiums = $right_premiums;
		}
		// </editor-fold>

		$this->view->html_cache_key = $cache_key . $static_page_key;
		
		//////////////////////////////////////////////////////////////
		$model = new Model_EscortsV2();
		
		if ($this->view->allowChat2())
		{
			$res = $model->getChatNodeOnlineEscorts(1, 1000);
					
			$ids = $res['ids'];
			$this->view->escort_ids = explode(',', $ids);
		}
		/////////////////////////////////////////////////
		
		$escorts_video = array();
		if(!empty($escorts))
		{		
			$e_id = array();
			foreach($escorts as $e)
			{
				$e_id[] = $e->id;
			}
			foreach ($right_premiums as $e)
			{
				$e_id[] = $e->id;
			}
			foreach ($premiums_fill as $e)
			{
				$e_id[] = $e->id;
			}
			$video = new Model_Video();
			$vescorts = $video->GetEscortsVideoArray($e_id);
			if(!empty($vescorts))
			{	
				$app_id = Cubix_Application::getId();
				foreach($vescorts as &$v)
				{	
					$photo = new Cubix_ImagesCommonItem(array(
						'hash'=> $v->hash,
						'width'=> $v->width,
						'height'=> $v->height,
						'ext'=> $v->ext,
						'application_id'=>$app_id
						));
				
					$v->photo =$photo->getUrl('orig');
					$escorts_video[$v->escort_id] =$v; 
				}
				$config = Zend_Registry::get('videos_config');
				$host =  Cubix_Application::getById(Cubix_Application::getId())->host;
				$this->view->vconfig = $config['Media'].'flv:'.$host;
				
			}
		}
		$this->view->escorts_video = $escorts_video;
		
		$res = Model_EscortsV2::getInstantBookEscortsList(1, 1000);
		$this->view->instantbook_escorts = $res['ids'];
		
		if ( ! is_null($this->_getParam('ajax')) ) {
			$escort_list = $this->view->render('escorts/list.phtml');
			
			die(json_encode(array('filter_body' => $filter_body, 'escort_list' => $escort_list)));
		}
	}


  public function ajaxBubbleAction() {
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		
		$page = intval($this->_getParam('page', 1));
		if ( $page < 1 ) $page = 1;
        $per_page = 10;

		echo $this->view->bubbleTextsWidget($page, $per_page);
  }

	/**
	 *
	 * @param string $r
	 */

	protected function _makeFilterArgument($r) {
		$param = explode('_', $r);

		if ( count($param) < 2 ) {
			switch( $r ) {
				case 'independantes':
				case 'agence':
				case 'boys':
				case 'trans':
				case 'tours':
				case 'upcomingtours':
				return array('field' => $r, 'value' => array());
			}
		}

		$param_name = $param[0];
		array_shift($param);

		switch ( $param_name ) {
			case 'filter':
				$field = reset($param);
				array_shift($param);

				$value = array_slice($param, 0, 2);

				$filter = array('field' => $field, 'value' => $value , 'main' => TRUE);
			break;
			case 'region':
			case 'state':
				$filter = array('field' => 'region', 'value' => $param[1]);
			break;
			case 'city':
			case 'cityzone':
				$filter = array('field' => $param_name, 'value' => array($param[1]));
			break;
			case 'name':
				$filter = array('field' => 'name', 'value' => array('%' . $param[0] . '%'));
			break;
		}

		return $filter;
	}

	/**
	 * Calculates filter counts for each
	 *
	 * @param integer $count
	 * @param array $filterParams
	 * @param Cubix_NestedMenu $menu
	 * @param string $cacheKey
	 */

	protected function _calcFilterCounts($count, $filterParams, Cubix_NestedMenu $menu, $cacheKey = null) {
		$model = new Model_EscortsV2();
		$defs = Zend_Registry::get('defines');

		$filterMap = array(
			'svc-anal' => 'e.svc_anal = ?',
			'svc-kissing' => 'e.svc_kissing = ?',
			'svc-kissing-with-tongue' => 'e.svc_kissing = ?',
			'svc-blowjob-with-condom' => 'e.svc_blowjob = ?',
			'svc-blowjob-without-condom' => 'e.svc_blowjob = ?',
			'svc-cumshot-in-face' => 'e.svc_cumshot = ?',
			'svc-cumshot-in-mouth-swallow' => 'e.svc_cumshot = ?',
			'svc-cumshot-in-mouth-spit' => 'e.svc_cumshot = ?',

			'age' => 'e.age BETWEEN ? AND ?',
			'ethnic' => 'e.ethnicity = ?',
			'height' => 'e.height > ? AND e.height < ?',
			'weight' => 'e.weight > ? AND e.weight < ?',
			'breast-size' => 'e.breast_size = ?',
			'hair-color' => 'e.hair_color = ?',
			'hair-length' => 'e.hair_length = ?',
			'eye-color' => 'e.eye_color = ?',
			'dress-size' => 'e.dress_size = ?',
			'shoe-size' => 'e.shoe_size = ?',
			'available-for' => 'e.availability = ?',
			'service-for' => 'FIND_IN_SET(?, e.sex_availability)',
			'smoker' => 'e.is_smoker = ?',
			'language' => 'FIND_IN_SET(?, e.languages)',
			'now-open' => 'e.is_now_open',
			'region' => 'r.slug = ?',
			'city' => 'cc.slug = ?',
			'cityzone' => 'c.id = ?',
			'name' => 'e.showname LIKE ?',

			'new_arrivals' => 'e.gender = ' . GENDER_FEMALE,
			'independent' => 'e.agency_id IS NULL AND e.gender = ' . GENDER_FEMALE,
			'agence' => 'e.agency_id IS NOT NULL AND e.gender = ' . GENDER_FEMALE,
			'boys' => 'e.gender = ' . GENDER_MALE,
			'trans' => 'e.gender = ' . GENDER_TRANS,
			'girls' => 'e.gender = ' . GENDER_FEMALE,

			'tours' => 'e.is_on_tour = 1',
			'upcomingtours' => 'e.tour_date_from > CURDATE()'
		);

		$counts = array();

		$filterStructure = $defs['escorts_filter'];

		foreach ( $filterStructure as $i => $filter ) {
			$fake = false;
			if ( ! isset($filter['childs']) ) {
				$filter['childs'] = array($filter);
				$fake = true;
			}
			
			foreach ( $filter['childs'] as $j => $child ) {
				$f = $this->_makeFilterArgument('filter_' . $child['value']);

				$newFilter = $filterParams;

				if ( ! isset($filterMap[$f['field']]) ) continue;

				$value = $f['value'];

				if ( isset($f['main']) && $f['main'] ) {
					if ( isset($child['internal_value']) ) {
						$value = $child['internal_value'];
					}
				}

				$newFilter['filter'][$filterMap[$f['field']]] = $value;

				$count = 0; // $model->getCount($newFilter);
				
				if ( $fake ) {
					$filterStructure[$i]['title'] .= " ($count)";
				}
				else {
					$filterStructure[$i]['childs'][$j]['title'] .= " ($count)";;
				}
			}
		}

		if ( ! is_null($cacheKey) ) {
			Zend_Registry::get('cache')->save($filterStructure, $cacheKey, array());
		}

		$menu->setStructure(array('childs' => $filterStructure));
	}

	public function profileAction() {
		$this->view->layout()->setLayout('profile');
		
		$this->view->is_profile = true;
		$this->view->is_preview = $is_preview = $this->_getParam('prev', false);
		
		$showname = $this->_getParam('escortName');
		$escort_id = $this->_getParam('escort_id');
		
		$model = new Model_EscortsV2();
		
		$cache_key = 'v2_' . $showname . '_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);

		$this->view->profile_cache_key = 'html_profile_' . $cache_key;
		
		//$escort = $model->get($showname, $cache_key);
		$escort = $model->get($escort_id, $cache_key);
		
		if (strtolower($escort->showname) != strtolower($showname))
		{			
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
		}

		if ( ! $escort || isset($escort['error']) )
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}

		if ( ! $is_preview ) { 
			if ( isset($escort->escort_status) && $escort->escort_status != 32 ) {
				$this->_forward('profile-disabled');
				$this->_request->setParam('city_slug', $escort->city_slug);
			}		
		}

		$blockModel = new Model_BlockedCountries();
		if ( $blockModel->checkIp($escort->id) ){
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}
		$vacation = new Model_EscortV2Item( array('id' => $escort->id  ));
		$vac = $vacation->getVacation();
		$add_esc_data = $model->getRevComById($escort->id);
		$block_website = $model->getBlockWebsite($escort->id);

		$escort->block_website = $block_website;
		$escort->disabled_reviews = $add_esc_data->disabled_reviews;
		$escort->disabled_comments = $add_esc_data->disabled_comments;
		$escort->vac_date_from = $vac->vac_date_from;
		$escort->vac_date_to = $vac->vac_date_to;
		$this->view->escort_id = $escort->id;
		$this->view->escort = $escort;

		if ( $escort->home_city_id ) {
			$m_city = new Cubix_Geography_Cities();
			$home_city = $m_city->get($escort->home_city_id);

			$m_country = new Cubix_Geography_Countries();
			$home_country = $m_country->get($home_city->country_id);

			$this->view->home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
		}

		$this->view->bubble_text = $model->getBubbleText($escort->id)->bubble_text;

		/* Last Review */
		$cache = Zend_Registry::get('cache');
		$cache_key = 'reviews_' . $escort->id;
		if ( ! $res = $cache->load($cache_key) ) {
			try {
				$res = $model->getEscortLastReview($escort->id);
				
				//$res = Cubix_Api::getInstance()->call('getEscortLastReview', array($escort->id));
			}
			catch ( Exception $e ) {
				$res = array(false, false);
			}
			
			$cache->save($res, $cache_key, array());
		}

		list($review, $rev_count) = $res;
		$this->view->review = $review;
		$this->view->rev_count = $rev_count;
		/* Last Review */

		$this->photosAction($escort);
		
		if ( $escort->agency_id )
			$this->agencyEscortsAction($escort->agency_id, $escort->id);

		$user = Model_Users::getCurrent();
		$this->view->user = $user;

		$paging_key = $this->_getParam('from');
		$paging_keys_map = array(
			'last_viewed_escorts',
			'search_list',
			'regular_list',
			'new_list',
			'profile_disabled_list',
			'premiums',
			'right_premiums',
			'main_premium_spot',
			'tours_list',
			'tour_main_premium_spot',
			'up_tours_list',
			'up_tour_main_premium_spot'
		);
		
		if ( in_array($paging_key, $paging_keys_map) ) {
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$ses = new Zend_Session_Namespace($sid);

			$ses_pages = $ses->{$paging_key};
			$criterias = $ses->{$paging_key . '_criterias'};
			
			if ( ! isset($criterias['first_page']) ) {
				$criterias['first_page'] = $criterias['page'];
			}
			if ( ! isset($criterias['last_page']) ) {
				$criterias['last_page'] = $criterias['page'];
			}
			
			$ses_index = 0;
			if ( is_array($ses_pages) ) {
				$ses_index = array_search($escort_id, $ses_pages);
			}

			if ( isset($ses_pages[$ses_index - 1]) ) {
				$this->view->back_showname = $model->getShownameById($ses_pages[$ses_index - 1]);
				$this->view->back_id = $back_id = $ses_pages[$ses_index - 1];
				$this->view->back_city = $model->getCityByEscortId($back_id);
			} elseif ( $criterias['first_page'] > 1 ) { //Loading previous page escorts
				switch( $paging_key ) {
					case 'regular_list' :
						$prev_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
						break;
					case 'main_premium_spot':
						$prev_page_escorts = array();
						break;
					case 'tours_list':
						$prev_page_escorts = Model_Escort_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
						break;
				}
				
				$p_escorts_count = count($prev_page_escorts);
				if ( count($p_escorts_count) ) {
					$this->view->back_showname = $prev_page_escorts[$p_escorts_count - 1]->showname;
					$this->view->back_id = $prev_page_escorts[$p_escorts_count - 1]->id;
					//Adding previous page escorts to $ses_pages and set in session
					$ids = array();
					foreach($prev_page_escorts as $p_esc) {
						$ids[] = $p_esc->id;
					}

					$criterias['first_page'] -= 1;
					$ses->{$paging_key} = array_merge($ids, $ses_pages);
					$ses->{$paging_key . '_criterias'} = $criterias;
				}
			}

			if ( isset($ses_pages[$ses_index + 1]) ) {
				$this->view->next_showname = $model->getShownameById($ses_pages[$ses_index + 1]);
				$this->view->next_id = $next_id = $ses_pages[$ses_index + 1];
				$this->view->next_city = $model->getCityByEscortId($next_id);
			} else { //Loading next page escorts
				switch( $paging_key ) {
					case 'regular_list' :
						$next_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
						break;
					case 'main_premium_spot':
						$next_page_escorts = array();
						break;
					case 'tours_list':
						$next_page_escorts = Model_Escort_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
						break;
				}
				
				if ( count($next_page_escorts) ) {
					$this->view->next_showname = $next_page_escorts[0]->showname;
					$this->view->next_id = $next_page_escorts[0]->id;
					//Adding next page escorts to $ses_pages and set in session
					foreach($next_page_escorts as $p_esc) {
						$ses_pages[] = $p_esc->id;
					}

					$criterias['last_page'] += 1;
					$ses->{$paging_key} = $ses_pages;
					$ses->{$paging_key . '_criterias'} = $criterias;
				}
			}
			
			$this->view->paging_key = $paging_key;			
		}

		/*list($prev, $next) = Model_Escort_List::getPrevNext($escort->showname);
		
		if ( ! is_null($prev) ) {
			$this->view->back_showname = $prev;
		}
		if ( ! is_null($next) ) {
			$this->view->next_showname = $next;
		}*/

		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();
		
		/* Cities list */
		$app = Cubix_Application::getById();
		
		if ($app->country_id)
		{
			$modelCities = new Model_Cities();
			
			$cities = $modelCities->getByCountry($app->country_id);
		}
		else
		{
			$modelCountries = new Model_Countries();
			$modelCities = new Model_Cities();
			
			$countries = $modelCountries->getCountries();
			$this->view->countries_list = $countries;
			
			$c = array();
			
			foreach ($countries as $country)
				$c[] = $country->id;
			
			$cities = $modelCities->getByCountries($c);
		}
		
		$this->view->cities_list = $cities;
		/* End Cities list */
	}
	
	public function profileV2Action() {
		if ( isset($_GET['v1']) ) {
			return $this->_forward('profile', 'escorts', 'default');
		}
		
		if($_GET['gmp']){
			error_reporting(-1);
			ini_set('error_reporting', E_ALL);
		}
		//<editor-fold defaultstate="collapsed" desc="Redirect to new URLS">
		$route_name = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
		if ( $route_name == 'escort-profile-def-old' ) {
			$this->_redirect(self::$linkHelper->getLink('profile', array('showname' => $this->_getParam('escortName'), 'escort_id' => $this->_getParam('escort_id'))), array('code' => 301));
		}
		////// redirect not active escorts to home page   ///////		
		if (strlen($this->_getParam('f')) && $this->_getParam('f') != 'b' ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
		}
		//////////////////////////////////////////////////////////
		//</editor-fold>
		$route_name = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
		
		$this->view->layout()->setLayout('profile-v2');
		$this->_helper->viewRenderer->setScriptAction("profile-v2/profile");
		
		$this->view->is_profile = true;
		
		$is_preview = $this->_getParam('prev', false);
		if ( strlen($this->_getParam('f')) && $this->_getParam('f') == 'b' ) {
			$is_preview = true;
		}
		$this->view->is_preview = $is_preview;
		
		$this->view->show_advertise_block = $show_advertise_block = $this->_getParam('show_advertise_block', false);
		
		$showname = $this->_getParam('escortName');
		$escort_id = $this->_getParam('escort_id');
		
		$model = new Model_EscortsV2();
		
		$cache_key = 'v2_' . $showname . '_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
		$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
		$this->view->profile_cache_key = 'html_profile_' . $cache_key;
		
		$from_api = false;
		$escort = $model->get($escort_id, $cache_key, $is_preview, $from_api);
		$this->view->from_api = $from_api;
		
		if ($escort->showname != $showname)
		{
			//die('r1');
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
		}
		
		$city = new Cubix_Geography_Cities();
		$city = $city->get($escort->base_city_id);
		$this->_request->setParam('region', $city->region_slug);
		$this->view->base_city = $city;
		
		if ( ! $escort || isset($escort['error']) )
		{
			//die('r2');
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}
		
		$blockModel = new Model_BlockedCountries();
		if ( $blockModel->checkIp($escort->id) ){
			//die('r3');
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			die;
			return;
		}
		//if ( isset($escort->escort_status) && $escort->escort_status != 32 ) {
		//	$this->_forward('profile-disabled');
		//	$this->_request->setParam('city_slug', $escort->city_slug);
		//}
		
		if ($escort->gender == GENDER_MALE){
			$this->_request->setParam('req', "boys");
		}
		elseif ($escort->gender == GENDER_TRANS){
			$this->_request->setParam('req', "trans");
		}
		
		
		$add_esc_data = $model->getRevComById($escort->id);
		$bl = $model->getBlockWebsite($escort->id);
		
		$escort->block_website = $bl->block_website;
		$escort->check_website = $bl->check_website;
		$escort->bad_photo_history = $bl->bad_photo_history;
		$escort->is_vip = $bl->is_vip;
		
		$escort->disabled_reviews = $add_esc_data->disabled_reviews;
		$escort->disabled_comments = $add_esc_data->disabled_comments;
		$this->view->escort_id = $escort->id;
		$vconfig = Zend_Registry::get('videos_config');
		$host =  Cubix_Application::getById(Cubix_Application::getId())->host;
		$video = new Model_Video();
		$escort_video = $video->GetEscortVideo($escort->id,1,NULL,FALSE);
		
		$this->view->natural_pic = $model->getNaturalPic($escort->id);
		$this->view->apps = $model->getApps($escort->id);
		
		$instantBook_escorts = $model->getInstantBookEscortsList(1, 1000);
		if(in_array($escort->id, $instantBook_escorts['ids'])){
			$this->view->instant_book_data = $instantBook_escorts['escorts'][$escort->id];
		}
		else{
			$this->view->instant_book_data = null;
		}
		
		
		
		if($bl->skype_call_status){
			//temporary get from api 
			$model_video_chat = new Model_VideoChatRequests();
			$this->view->video_chat_prices = $model_video_chat->getData($escort->id);
		}


		$model_cam = new Model_Cam();
		$this->view->cam = $model_cam->getData($escort->id);
		

		if(!empty($escort_video[0]))
		{
			if(!is_null($escort_video[1][0]->cover_hash) && $escort_video[1][0]->cover_status == Model_Video::COVER_STATUS_VERIFIED) {
				$app_id = Cubix_Application::getId();
				$cover_arr = array(
					'hash' => $escort_video[1][0]->cover_hash, 
					'ext' => $escort_video[1][0]->cover_ext,
					'application_id' => $app_id
				);
				$photo = new Cubix_ImagesCommonItem($cover_arr);
			} else {
				$photo = new Cubix_ImagesCommonItem($escort_video[0][0]);
			}

			$this->view->photo_video = $photo->getUrl('orig');
            $this->view->gallery_video =($escort_video[1][0]->height > $escort_video[1][0]->width) ? $photo->getUrl('pp') : $photo->getUrl('pl');
			$this->view->vwidth = $photo['width'];
			$this->view->vheight = $photo['height'];

			$this->view->video =$vconfig['Media'].'flv:'.$host.'/'.$escort->id.'/'.$escort_video[1][0]->video;
		}

		$this->view->escort = $escort;

		if ( $escort->home_city_id ) {
			$m_city = new Cubix_Geography_Cities();
			$home_city = $m_city->get($escort->home_city_id);

			$m_country = new Cubix_Geography_Countries();
			$home_country = $m_country->get($home_city->country_id);

			$this->view->home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
		}

		$bt = $model->getBubbleText($escort->id);
		$this->view->bubble_text = $bt ? $bt->bubble_text : null;

		/* Last Review */
		$cache = Zend_Registry::get('cache');
		$cache_key = 'reviews_' . $escort->id;
		if ( ! $res = $cache->load($cache_key) ) {
			try {
				$res = $model->getEscortLastReview($escort->id);
				
				//$res = Cubix_Api::getInstance()->call('getEscortLastReview', array($escort->id));
			}
			catch ( Exception $e ) {
				$res = array(false, false);
			}
			
			$cache->save($res, $cache_key, array());
		}

		list($review, $rev_count) = $res;
		$this->view->review = $review;
		$this->view->rev_count = $rev_count;
		/* Last Review */

		$this->view->galleries = $escort->getPhotoGalleries($escort->id);
		$this->galleryPhotosAction($escort);
		//$this->photosV2Action($escort);
		
		if ( $escort->agency_id )
			$this->agencyEscortsV2Action($escort->agency_id, $escort->id);

		$user = Model_Users::getCurrent();
		$this->view->user = $user;

		$paging_key = $this->_getParam('from');
		$paging_keys_map = array(
			'last_viewed_escorts',
			'search_list',
			'regular_list',
			'new_list',
			'profile_disabled_list',
			'premiums',
			'right_premiums',
			'main_premium_spot',
			'tours_list',
			'tour_main_premium_spot',
			'up_tours_list',
			'up_tour_main_premium_spot'
		);

		if ( in_array($paging_key, $paging_keys_map) ) {
			$sid = 'sedcard_paging_' . Cubix_Application::getId();
			$ses = new Zend_Session_Namespace($sid);

			$ses_pages = $ses->{$paging_key};
			$criterias = $ses->{$paging_key . '_criterias'};
			
			if ( ! isset($criterias['first_page']) ) {
				$criterias['first_page'] = $criterias['page'];
			}
			if ( ! isset($criterias['last_page']) ) {
				$criterias['last_page'] = $criterias['page'];
			}

			$ses_index = 0;
			if ( is_array($ses_pages) ) {
				$ses_index = array_search($escort_id, $ses_pages);
			}

			if ( isset($ses_pages[$ses_index - 1]) ) {
				$this->view->back_showname = $model->getShownameById($ses_pages[$ses_index - 1]);
				$this->view->back_id = $back_id = $ses_pages[$ses_index - 1];
			} elseif ( $criterias['first_page'] > 1 ) { //Loading previous page escorts
				switch( $paging_key ) {
					case 'regular_list' :
						$prev_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
						break;
					case 'main_premium_spot':
						$prev_page_escorts = array();
						break;
					case 'tours_list':
						$prev_page_escorts = Model_Escort_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['first_page'] - 1);
						break;
				}
				
				$p_escorts_count = count($prev_page_escorts);
				if ( count($p_escorts_count) ) {
					$this->view->back_showname = $prev_page_escorts[$p_escorts_count - 1]->showname;
					$this->view->back_id = $prev_page_escorts[$p_escorts_count - 1]->id;
					//Adding previous page escorts to $ses_pages and set in session
					$ids = array();
					foreach($prev_page_escorts as $p_esc) {
						$ids[] = $p_esc->id;
					}

					$criterias['first_page'] -= 1;
					$ses->{$paging_key} = array_merge($ids, $ses_pages);
					$ses->{$paging_key . '_criterias'} = $criterias;
				}
			}

			if ( isset($ses_pages[$ses_index + 1]) ) {
				$this->view->next_showname = $model->getShownameById($ses_pages[$ses_index + 1]);
				$this->view->next_id = $next_id = $ses_pages[$ses_index + 1];
			} else { //Loading next page escorts
				switch( $paging_key ) {
					case 'regular_list' :
						$next_page_escorts = Model_Escort_List::getFiltered($criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
						break;
					case 'main_premium_spot':
						$next_page_escorts = array();
						break;
					case 'tours_list':
						$next_page_escorts = Model_Escort_List::getTours($criterias['is_upcoming'], $criterias['filter'], $criterias['sort'], $criterias['last_page'] + 1);
						break;
				}
				
				if ( count($next_page_escorts) ) {
					$this->view->next_showname = $next_page_escorts[0]->showname;
					$this->view->next_id = $next_page_escorts[0]->id;
					//Adding next page escorts to $ses_pages and set in session
					foreach($next_page_escorts as $p_esc) {
						$ses_pages[] = $p_esc->id;
					}

					$criterias['last_page'] += 1;
					$ses->{$paging_key} = $ses_pages;
					$ses->{$paging_key . '_criterias'} = $criterias;
				}
			}
			
			$this->view->paging_key = $paging_key;
		}

		/*list($prev, $next) = Model_Escort_List::getPrevNext($escort->showname);
		
		if ( ! is_null($prev) ) {
			$this->view->back_showname = $prev;
		}
		if ( ! is_null($next) ) {
			$this->view->next_showname = $next;
		}*/

		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls();
		
		/* Cities list */
		$app = Cubix_Application::getById();
		
		if ($app->country_id)
		{
			$modelCities = new Model_Cities();
			
			$cities = $modelCities->getByCountry($app->country_id);
		}
		else
		{
			$modelCountries = new Model_Countries();
			$modelCities = new Model_Cities();
			
			$countries = $modelCountries->getCountries();
			$this->view->countries_list = $countries;
			
			$c = array();
			
			foreach ($countries as $country)
				$c[] = $country->id;
			
			$cities = $modelCities->getByCountries($c);
		}
		
		$this->view->cities_list = $cities;
		/* End Cities list */
		
		$this->view->escort_user_id = $model->getUserId($escort->id);

		/* watched */
		$model_m = new Model_Members();
		$sess_id = session_id();
		$user_id = null;
		$cur_user = Model_Users::getCurrent();
		
		if ($cur_user)
		{
			$user_id = $cur_user->id;

			$has = $model_m->getFromWatchedEscortsForUser($sess_id, $escort_id, $user_id, date('Y-m-d'));

			if ($has)
			{
				$model_m->updateToWatchedEscortsForUser($sess_id, $escort_id, $user_id);
			}
			else
			{
				$model_m->addToWatchedEscorts($sess_id, $escort_id, $user_id);
			}
		}
		else
		{
			$has = $model_m->getFromWatchedEscorts($sess_id, $escort_id);

			if ($has)
			{
				$model_m->updateToWatchedEscorts($sess_id, $escort_id);
			}
			else
			{
				$model_m->addToWatchedEscorts($sess_id, $escort_id);
			}
		}
		/**/
	}
	
	public function photosV2Action($escort = null) {
		$config = Zend_Registry::get('escorts_config');
		
		$page = $this->_getParam('photo_page');

		$escort_id = $this->_getParam('escort_id');
		$mode = $this->_getParam('mode');
		
		if( $mode == 'ajax' ) {
			$this->view->layout()->disableLayout();
			$model = new Model_EscortsV2();
			$escort = $model->get($escort_id);
			$this->view->escort_id = $escort_id;
		}
		
		if ( ! $page )
			$page = 1;
		
		$count = 0;
		if ( $this->view->from_api ) {
			$photos = $escort->getPhotosApi($page, $count, true, true);
		}
		else {
			$count = 100;
			$photos = $escort->getPhotos($page, $count, true, false, null, null, false, true);
		}
		$this->view->photos = $photos;
		$this->view->photos_count = $count;
		$this->view->photos_page = $page;

		$this->view->photos_perPage = $config['photos']['perPage'];
	}

	public function galleryPhotosAction($escort = null) {
		$config = Zend_Registry::get('escorts_config');
		$page = 1;
		$count = 0;
		$galery_id = intval($this->_getParam('gallery_id'));
		
		if($this->_request->isXmlHttpRequest()){
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setScriptAction("profile-v2/gallery-photos");
			$escort_id = intval($this->_getParam('escort_id'));
			$showname = $this->_getParam('showname');
			$cache_key = 'v2_' . $showname . '_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
			$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
			$model = new Model_EscortsV2();
			$escort = $model->get($escort_id,$cache_key);
			
			if (!$escort || $escort->showname != $showname)
			{
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: /');
				die;
			}

			$city = new Cubix_Geography_Cities();
			$city = $city->get($escort->base_city_id);
			$this->_request->setParam('region', $city->region_slug);
			$this->view->base_city = $city;

			if ( ! $escort || isset($escort['error']) )
			{
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: /');
				die;
				return;
			}

			$blockModel = new Model_BlockedCountries();
			if ( $blockModel->checkIp($escort->id) ){
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: /');
				die;
				return;
			}
			$this->view->escort = $escort;
			$this->view->escort_id = $escort_id;
			
		}

		if (!$escort) {
			header('HTTP/1.1 404 Not Found');
			die('Not Found.');
		}

		if ( $this->view->is_preview || $this->_getParam('is_preview') ) {
			$photos = $escort->getPhotosApi($page, $count, true, false, $galery_id);
		}
		else {
			$count = 100;
			$photos = $escort->getPhotos($page, $count, true, false, null, null, false, false, $galery_id);
		}
		$this->view->photos = $photos;
		$this->view->photos_count = $count;
		$this->view->photos_page = $page;
		$this->view->photos_perPage = $config['photos']['perPage'];
	}
	
	public function profileDisabledAction() {
		$model = new Model_EscortsV2();
		$count = 0;

		$this->view->no_layout_banners = true;
		$this->view->no_paging = true;
		$this->view->no_js = true;

		$params = array('e.gender = 1' => array(), 'ct.slug = ?' => array($this->_request->city_slug));
		$escorts = Model_Escort_List::getFiltered($params, 'random', 1, 50, $count);

		$this->view->count = $count;
		$this->view->escorts = $escorts;
	}
	
	public function agencyEscortsV2Action($agency_id = null, $escort_id = null) {
		$config = Zend_Registry::get('escorts_config');
		
		if ( ! $agency_id )
		{			
			$this->view->layout()->disableLayout();
			$agency_id = $this->view->agency_id = $this->_getParam('agency_id');
		}
		else
			$this->view->agency_id = $agency_id;
		
		if( ! $escort_id )
			$escort_id = $this->view->escort_id = $this->_getParam('escort_id');
		
		$page = intval($this->_getParam('agency_page'));
		if ( $page < 1 )
			$page = 1;

		$params = array('e.agency_id = ?' => $agency_id,'e.id <> ?' => $escort_id);

		$escorts_count = 0;
		$escorts = Model_Escort_List::getFiltered($params, 'random', $page, $config['widgetPerPageV2'], $escorts_count);

		if (count($escorts) ) {
			$this->view->a_escorts = $escorts;
			$this->view->a_escorts_count = $escorts_count;

			$this->view->agencies_page = $page;
			$this->view->widgetPerPage = $config['widgetPerPageV2'];

			$m_esc = new Model_EscortsV2();
			$agency = $m_esc->getAgency($escort_id);

			$this->view->agency = new Model_AgencyItem($agency);
		}
	}
	
	public function agencyEscortsAction($agency_id = null, $escort_id = null) {
		$config = Zend_Registry::get('escorts_config');
		
		if ( ! $agency_id )
		{
			$this->view->layout()->disableLayout();
			$agency_id = $this->view->agency_id = $this->_getParam('agency_id');
		}
		else
			$this->view->agency_id = $agency_id;
		
		if( ! $escort_id )
			$escort_id = $this->view->escort_id = $this->_getParam('escort_id');
		
		$page = intval($this->_getParam('agency_page'));
		if ( $page < 1 )
			$page = 1;

		$params = array('e.agency_id = ?' => $agency_id,'e.id <> ?' => $escort_id);

		$escorts_count = 0;
		$escorts = Model_Escort_List::getFiltered($params, 'random', $page, $config['widgetPerPage'], $escorts_count);

		if (count($escorts) ) {
			$this->view->a_escorts = $escorts;
			$this->view->a_escorts_count = $escorts_count;

			$this->view->agencies_page = $page;
			$this->view->widgetPerPage = $config['widgetPerPage'];

			$m_esc = new Model_EscortsV2();
			$agency = $m_esc->getAgency($escort_id);

			$this->view->agency = new Model_AgencyItem($agency);
		}
	}
	
	public function photosAction($escort = null) {
		$config = Zend_Registry::get('escorts_config');
		
		$page = $this->_getParam('photo_page');

		$escort_id = $this->_getParam('escort_id');
		$mode = $this->_getParam('mode');
		
		if( $mode == 'ajax' ) {
			$this->view->layout()->disableLayout();
			$model = new Model_EscortsV2();
			$escort = $model->get($escort_id);

			Model_EscortsV2::hit_gallery( $escort_id );

			$this->view->escort_id = $escort_id;
		}
		
		if ( ! $page )
			$page = 1;
		
		$count = 0;
		$photos = $escort->getPhotos($page, $count, false);		
		$this->view->photos = $photos;
		$this->view->photos_count = $count;
		$this->view->photos_page = $page;

		$this->view->photos_perPage = $config['photos']['perPage'];
	}
	
	public function privatePhotosAction($escort = null) {
		$config = Zend_Registry::get('escorts_config');
		
		$page = $this->_getParam('photo_page');
		$escort_id = $this->_getParam('escort_id');
		
		if( $escort_id )
		{
			$this->view->layout()->disableLayout();
			$model = new Model_EscortsV2();
			$escort = $model->get($escort_id);
			$this->view->escort_id = $escort_id;
		}
		
		if ( ! $page )
			$page = 1;
			
		$count = 0;
		$this->view->private_photos = $escort->getPhotos($page, $count, false, true);
		$this->view->private_photos_count = $count;
		$this->view->private_photos_page = $page;
		$this->view->private_photos_perPage = $config['photos']['perPage'];
	}
	
	public function viewedEscortsAction() {
		$this->view->layout()->disableLayout();
		$escort_id = intval($this->_getParam('escort_id'));

		if ( $escort_id )
		{
			self::_addToLatestViewedList($escort_id);
			$viewedEscorts = self::_getLatestViewedList(11, $escort_id);
		}
		else
			$viewedEscorts = self::_getLatestViewedList(11);

		if ( ! is_array($viewedEscorts) || count($viewedEscorts) == 0 )
			return false;
		
		
		
		$ve = array();
		
		$ids = array();
		
		foreach ( $viewedEscorts as $e ) {
			$ids[] = $e['id'];
		}
		
		$filter = array('e.id IN (' . implode(', ', $ids) . ')' => array());
		$order = 'FIELD(eic.escort_id, ' . implode(', ', $ids) . ')';
		
		$count = 0;
		$escorts = Model_Escort_List::getFiltered($filter, $order, 1, 11, $count);
		
		$this->view->escorts = $escorts;
	}

	public function lateNightGirlsAction() {
		$this->view->layout()->disableLayout();

		$page = intval($this->_request->l_n_g_page);

		if ($page < 1) $page = 1;
		
		list($this->view->late_night_girls, $this->view->late_night_girls_count) = $this->_getLateNightGirls($page);
		$this->view->l_n_g_page = $page;
	}

	private function _getLateNightGirls($page = 1) {
		return Model_EscortsV2::getLateNightGirls($page);
	}

	private function _addToLatestViewedList($escort_id) {
		$count = 11; // actually we need 10
		$escort = array('id' => $escort_id);
		$sid = 'last_viewed_escorts_' . Cubix_Application::getId();
		
		$ses = new Zend_Session_Namespace($sid);
		
		// Checking if session exists
		// creating if not exists		
		if ( false == (isset($ses->escorts) && is_array($ses->escorts)) )
		{
			$ses->escorts = array();
		}
		else 
		{
			// Checking if escort exists in stack
			// removing escort from stack if exists
			foreach ($ses->escorts as $key => $item)
			{
				if ( $escort['id'] == $item['id'] )
				{
					array_splice($ses->escorts, $key, 1);
				}
			}
		}
		
		// Pushing escort to the end of the stack
		array_unshift($ses->escorts, $escort);
		
		// Checking if
		if ( count($ses->escorts) > $count )
		{	
			array_pop($ses->escorts);
		}
		//var_dump($ses->escorts);
	}
	
	// Getting latest viewed escorts list (stack) 
	// as Array of Assoc
	public function _getLatestViewedList($max, $exclude = NULL) {
		$sid = 'last_viewed_escorts_' . Cubix_Application::getId();
		
		$ses = new Zend_Session_Namespace($sid);

		if ( ! is_array($ses->escorts) )
			return array();

		if ( $exclude )
		{
			$arr = $ses->escorts;
			foreach($arr as $key => $item)
			{
				if($exclude == $item['id'])
				{
					array_splice($arr, $key, 1);
				}	
			}
			$ret = array_slice($arr, 0, $max);
			
			return $arr;
		}

		$ret = array_slice($ses->escorts, 0, $max);
		
		return $ret;
	}
	
	public function searchAction() {
        $this->view->layout()->setLayout('main-simple');
		
		
		$this->view->quick_links = false;

		$this->view->defines = Zend_Registry::get('defines');
		
		if( $this->_getParam('search') )
		{
			$params['filter'] = array();
            $params['order'] = array();
			
			$this->view->params = array_map("htmlspecialchars", $this->_request->getParams());
			$this->view->serializedParams = serialize($this->view->params);

            if( $this->_getParam('order') ) {

                $order_direction = '';
                $order_field = 'date_registered';

                switch($this->_getParam('order')){
                    case 'lastmodified':
                       $order_field = "date_last_modified";
                       break;
                    case 'popularity':
                       $order_field = "hit_count";
                       break;
                }

                if($this->_getParam('order_direction')){
                   $order_direction = $this->_getParam('order_direction');
                }
                $params['order'] = $order_field." ".$order_direction;
				
			}


            if($this->_getParam('page')){
                $params['limit']['page'] = $this->_getParam('page');
            }

            if( $this->_getParam('publish') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'DATEDIFF(NOW(),e.date_registered) <= ?' =>  trim($this->_getParam('days'))
				));
			}

			if( $this->_getParam('showname') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.showname LIKE ?' => "%" . trim($this->_getParam('showname')) . "%"
				));
			}
			
			if( $this->_getParam('agency_slug') && $this->_getParam('agency') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.agency_slug LIKE ?' => "%" . trim($this->_getParam('agency_slug')) . "%"
				));
			}
			
			if( $this->_getParam('gender') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.gender = ?' => $this->_getParam('gender')
				));
			}

			if( $this->_getParam('city') &&  $this->_getParam('city_slug') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ct.slug = ?' => $this->_getParam('city_slug')
				));
			}


			if( $this->_getParam('cityzone') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'ecz.city_zone_id = ?' => $this->_getParam('cityzone')
				));
			}
			
			if( $this->_getParam('phone') ) {
				$phone = preg_replace('/^\+/', '00',$this->_getParam('phone'));
				$phone = preg_replace('/[^0-9]+/', '', $phone);
				$params['filter'] = array_merge($params['filter'], array(
					'e.contact_phone_parsed LIKE ?' => "%".$phone."%"
				));
			}
			
			if( $this->_getParam('available_for_incall') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.incall_type IS NOT NULL' => true
				));
			}
			if( $this->_getParam('available_for_outcall') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.outcall_type IS NOT NULL' => true
				));
			}
			
			if( $this->_getParam('nationality') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.nationality_id = ?' => $this->_getParam('nationality')
				));
			}
			
			if( $this->_getParam('saf') ) {
				$params['filter'] = array_merge($params['filter'], array(					
					'FIND_IN_SET( ?, e.sex_availability)' => $this->_getParam('saf')
				));
			}

            if( $this->_getParam('price_from') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.outcall_price >= ?' => $this->_getParam('price_from')
				));
			}

            if( $this->_getParam('price_to') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.outcall_price <= ?' => $this->_getParam('price_to')
				));
			}

			if( $this->_getParam('words') ) 
			{
				if ($this->_getParam('words_type') == 1)
				{
	            	if ($pos = strpos($this->_getParam('words'), ' ')) {
	            		$sub = substr($this->_getParam('words'), 0, $pos);
	            	}
	            	else {
	            		$sub = $this->_getParam('words');
	            	}
	            	
	            	$params['filter'] = array_merge($params['filter'], array(
						'e.' . Cubix_I18n::getTblField('about') . ' = ?' =>  $sub 
					));
	            }
	            elseif ($this->_getParam('words_type') == 2) 
	            {
	            	$params['filter'] = array_merge($params['filter'], array(
						'e.' . Cubix_I18n::getTblField('about') . ' LIKE ?' => "%" . $this->_getParam('words') . "%"
					));
	            }
			}
			
			if( $this->_getParam('has_review_blocked') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.disabled_reviews = ?' => 0
				));
			}
			
			if( $this->_getParam('has_comment_blocked') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.disabled_comments = ?' => 0
				));
			}
			
			if( $this->_getParam('is_on_tour') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.is_on_tour = ?' => 1
				));
			}
			
			/* ------------ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<< --------------- */
			
			if( $this->_getParam('age_from') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.age >= ?' => $this->_getParam('age_from')
				));
			}			
			if( $this->_getParam('age_to') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.age <= ?' => $this->_getParam('age_to')
				));
			}

            if( $this->_getParam('ethnicity') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.ethnicity = ?' => $this->_getParam('ethnicity')
				));
			}

            if( $this->_getParam('hair_color') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.hair_color = ?' => $this->_getParam('hair_color')
				));
			}

			if( $this->_getParam('hair_length') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.hair_length = ?' => $this->_getParam('hair_length')
				));
			}

			if( $this->_getParam('eye_color') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.eye_color = ?' => $this->_getParam('eye_color')
				));
			}

            if( $this->_getParam('height_from') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.height >= ?' => $this->_getParam('height_from')
				));
			}
			if( $this->_getParam('height_to') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.height <= ?' => $this->_getParam('height_to')
				));
			}	
		
			if( $this->_getParam('dress_size') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.dress_size = ?' => $this->_getParam('dress_size')
				));
			}

            if( $this->_getParam('shoe_size') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.shoe_size = ?' => $this->_getParam('shoe_size')
				));
			}	
								
			if( $this->_getParam('cup_size') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.cup_size = ?' => $this->_getParam('cup_size')
				));
			}
            
            if( $this->_getParam('pubic_hair') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.pubic_hair = ?' => $this->_getParam('pubic_hair')
				));
			}

			if( $this->_getParam('smoker') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.is_smoking = ?' => $this->_getParam('smoker')
				));
			}

			if( $this->_getParam('drinking') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.is_drinking = ?' => $this->_getParam('drinking')
				));
			}			
			if( $this->_getParam('language') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'e.languages LIKE ?' => "%".$this->_getParam('language')."%"
				));
			}			
            /* Services */
            if( $this->_getParam('services') ) {
				$params['filter'] = array_merge($params['filter'], array(
					'es.service_id = ?' => $this->_getParam('services')
				));
			}
            /* Services */
            /* Working Times */
            
            if( $this->_getParam('day_index') ) {
                $dateTimes = array();
                $counter = 0;
                foreach($this->_getParam('day_index') as $day => $value){
                    $timeFrom = $this->_getParam('time_from');
                    $timeFromM = $this->_getParam('time_from_m');
                    $timeTo = $this->_getParam('time_to');
                    $timeToM = $this->_getParam('time_to_m');
                    $dateTimes[$counter]['day_index'] = $day;
                    $dateTimes[$counter]['time_from'] = $timeFrom[$day];
                    $dateTimes[$counter]['time_from_m'] = $timeFromM[$day];
                    $dateTimes[$counter]['time_to'] = $timeTo[$day];
                    $dateTimes[$counter]['time_to_m'] = $timeToM[$day];
                    $counter++;
                }
                $params['filter'] = array_merge($params['filter'], array(
					'working_times = ?' => $dateTimes
				));
			}

            /* Working Times */

            $page = 1;
            $page_size = 50;
            if($this->_getParam('page')){
                $page = $this->_getParam('page');
            }
			
			$model = new Model_EscortsV2();
			$count = 0;
			$escorts = $model->getSearchAll($params, $count,$page,$page_size);
            $x['times'] = $dateTimes;

            $this->view->data = $x;
            $this->view->page = $page;
			$this->view->count = $escorts['count'];
			$this->view->escorts = $escorts['data'];
			$this->view->search_list = true;
			$escorts_video = array();
			if(!empty($escorts['data']))
			{		
				$e_id = array();
				foreach($escorts['data'] as $e)
				{
					$e_id[] = $e->id;
				}

				$video = new Model_Video();
				$vescorts = $video->GetEscortsVideoArray($e_id);
				if(!empty($vescorts))
				{	
					$app_id = Cubix_Application::getId();
					foreach($vescorts as &$v)
					{	
						$photo = new Cubix_ImagesCommonItem(array(
							'hash'=>$v->hash,
							'width'=>$v->width,
							'height'=>$v->height,
							'ext'=>$v->ext,
							'application_id'=>$app_id
							));

						$v->photo =$photo->getUrl('orig');
						$escorts_video[$v->escort_id] =$v; 
					}
					$config = Zend_Registry::get('videos_config');
					$host =  Cubix_Application::getById(Cubix_Application::getId())->host;
					$this->view->vconfig = $config['Media'].'flv:'.$host;
				}
			}
			$this->view->escorts_video = $escorts_video;
			
			$res = Model_EscortsV2::getInstantBookEscortsList(1, 1000);
			$this->view->instantbook_escorts = explode(',', $res['ids']);

		}
	}

	public function gotmAction() {
		$this->view->layout()->setLayout('main-simple');
		$show_history = (bool) $this->_getParam('history');
		
		if ( ! $show_history ) {
			$this->view->gotm = Model_EscortsV2::getCurrentGOTM();
		}
		else {
			$result = Model_EscortsV2::getMonthGirls(1, 40, $show_history);
			$this->view->escorts = $result['escorts'];
			$this->view->count = $result['count'];
			$this->_helper->viewRenderer->setScriptAction('gotm-history');
		}
	}
	
	public function ajaxGotmAction() {
		$this->view->layout()->disableLayout();
		$page = intval($this->_getParam('page', 1));
		if ( $page < 1 ) $page = 1;
		$this->view->per_page = $per_page = 40;
		$this->view->page = $page;
		$showname = trim($this->_getParam('showname'));
		$result = Model_EscortsV2::getMonthGirls($page, $per_page, false, $showname);
		$this->view->escorts = $result['escorts'];
		$this->view->count = $result['count'];
	}

	public function gotmCurrentAction() {
		$this->view->layout()->disableLayout();

		try {
			$gotm = Model_EscortsV2::getCurrentGOTM();

			if ($gotm)
			{
				$escort = new Model_EscortV2Item();
				$escort->setId($gotm->id);
				$gotm_photos = $escort->getPhotos();

				if (!count($gotm_photos))
					$gotm_photos = $escort->getPhotosApi();
			}
		}
		catch ( Exception $e ) { $gotm = false; }

		$this->view->gotm = $gotm;
		$this->view->gotm_photos = $gotm_photos;
	}

	/**
	 * Voting Widget Action
	 */

	public function voteAction() {
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$response = array('status' => null, 'desc' => '', 'html' => '');

		$showname = $this->_getParam('showname');
		$escort_id = $this->_getParam('escort_id');
		$model = new Model_EscortsV2();
		//$escort = $model->get($showname);
		$escort = $model->get($escort_id);

		$user = Model_Users::getCurrent();

		if ( ! $user ) {
			$response['status'] = 'error';
			$response['desc'] = 'not signed in';
		}
		elseif ( 'member' != $user->user_type ) {
			$response['status'] = 'error';
			$response['desc'] = 'only members are allowed to vote';
		}
		elseif ( ! $escort ) {
			$response['status'] = 'error';
			$response['desc'] = 'invalid escort showname';
		}
		elseif ( $escort->hasZeroPackage() ) {
			$response['status'] = 'error';
			$response['desc'] = 'only premium escorts can be voted';
		}
		elseif ( $user->additional_info['is_suspicious'] || $user->additional_info['status'] == -4 ) {
			$response['status'] = 'error';
			$response['desc'] = 'Sorry, you do not allow to vote';
		}
		else {
			try {
				if ( !$user->allow_vote ) 
				{
					$forum_api = new Cubix_Forum2Api();
					$count = $forum_api->getApprovedPostsCountOfMember($user->username);

					if ($count < 3)
					{
						$response['status'] = 'error';
						$response['desc'] = $this->view->t('error_allow_vote');
					}
					else
					{
						$m_m = new Model_Members();
						$m_m->setAllowVote($user->id);
						$user->allow_vote = 1;
						Model_Users::setCurrent($user);
					}
				}
				
				if (!$response['status'])
				{
					$result = $escort->vote($user->id);

					if ( $result !== true ) {
						$response['status'] = 'error';
						$response['desc'] = 'member has already voted';
					}
					else {
						$response['status'] = 'success';
						$response['html'] = $this->view->votingWidget($escort, true);
					}
				}				
			}
			catch ( Exception $e ) {
				$response['status'] = 'error';
				$response['desc'] = 'unexpected error';
			}
		}

		echo json_encode($response);
	}

  public function ajaxOnlineAction() {
        $page = intval($this->_getParam('page', 1));
		if ( $page < 1 ) $page = 1;
        $per_page = 5;
		$this->view->layout()->disableLayout();
        $model = new Model_EscortsV2();
		
		if (!$this->view->allowChat2())
			$results = $model->getChat11OnlineEscorts($page, $per_page);
		else
			$results = $model->getChatNodeOnlineEscorts($page, $per_page);
		
		$this->view->escorts = $results['escorts'];
        $this->view->per_page = $per_page;
		$this->view->page = $page;
        $this->view->count = $results['count'];
    }
	
	public function ajaxAlertsAction() {
		$this->view->layout()->disableLayout();
		
		$user = Model_Users::getCurrent();
		$escort_id = intval($this->_getParam('escort_id'));
		
		if ($user->user_type != 'member' || $escort_id == 0)
		{
			echo json_encode (array());
			die;
		}
		
		$model = new Model_Members();
		
		$events = $model->getEscortAlerts($user->id, $escort_id);
		
		$arr = array();
		$cities = array();
		$cities_str = '';
			
		if ($events)
		{
			foreach ($events as $event)
			{
				$arr[] = $event['event'];
				
				if ($event['event'] == ALERT_ME_CITY)
					$cities_str = $event['extra'];
			}
		}
		
		if (strlen($cities_str) > 0)
		{
			$modelCities = new Model_Cities();
			
			$cts = $modelCities->getByIds($cities_str);
			
			foreach ($cts as $c)
			{
				$cc = array('id' => $c->id, 'title' => $c->title);
				$cities[] = $cc;
			}
		}

		echo json_encode(array('events' => $arr, 'cities' => $cities));
		die;
	}
	
	public function ajaxAlertsSaveAction() {
		$this->view->layout()->disableLayout();
		
		$user = Model_Users::getCurrent();
		$escort_id = intval($this->_getParam('escort_id'));
		$events = $this->_getParam('events');
		$cities = $this->_getParam('cities');
		
		if ($user->user_type != 'member' || $escort_id == 0)
		{
			echo 'error';
			die;
		}
		
		$model = new Model_Members();
		$c = $model->memberAlertsSave($user->id, $escort_id, $events, $cities);
		
		$cache = Zend_Registry::get('cache');
		$cache_key = 'alert_list_for_user_' . Cubix_Application::getId() . '_'. $user->id;
		
		if ($res = $cache->load($cache_key) ) {
			if($events){
				$res[] = $escort_id;
				$cache->save($res, $cache_key, array());
			}
			else{
				$key = array_search($escort_id, $res);
				unset($res[$key]);
				$cache->save($res, $cache_key, array());
			}
		}
		
		echo json_encode(array('status' => 'success', 'count' => $c));
		die;
	}
	
	public function ajaxTellFriendAction() {
		$this->view->layout()->disableLayout();
		$this->view->full_url = $url = $_SERVER['HTTP_REFERER'];
		if($this->_request->isPost()){

			// Get data from request
			$data = new Cubix_Form_Data($this->getRequest());

			$fields = array(
				'name' => 'notags|special',
				'email' => '',
				'friend_name' => 'notags|special',
				'friend_email' => '',
				'message' => 'notags|special',

			);
			$data->setFields($fields);
			$data = $data->getData();

			$validator = new Cubix_Validator();

            if ( ! strlen($data['name']) ) {
				$validator->setError('name', 'Your Name is required');
			}

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Your Email is required');
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			}
			if ( ! strlen($data['friend_name']) ) {
				$validator->setError('friend_name', 'Friend Name is required');
			}

			if ( ! strlen($data['friend_email']) ) {
				$validator->setError('friend_email', 'Friend Email is required');
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['friend_email']) ) {
				$validator->setError('friend_email', 'Wrong email format');
			}

			$result = $validator->getStatus();

			if ( $validator->isValid() ) {

				// Set the template parameters and send it to support
				$email_tpl = 'send_to_friend';
				$tpl_data = array(
					'name' => $data['name'],
					'email' => $data['email'],
					'friend_name' => $data['friend_name'],
					'friend_email' => $data['friend_email'],
					'message' => $data['message'],
					'application_id' => Cubix_Application::getId(),
					'url' => $url
				);
				$data['application_id'] = Cubix_Application::getId();

				Cubix_Email::sendTemplate($email_tpl, $data['friend_email'], $tpl_data, $data['email']);

				$config = Zend_Registry::get('newsman_config');

				$list_id = $config['list_id'];
				$segment = $config['member'];
				$nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);

				$subscriber_id = $nm->subscribe($list_id, $data['email'],$data['name']);
				$is_added = $nm->bindToSegment($subscriber_id, $segment);
				$subscriber_id_friend = $nm->subscribe($list_id, $data['friend_email'],$data['friend_name']);
				$is_added = $nm->bindToSegment($subscriber_id_friend, $segment);
				
				die(json_encode($result));
			}	// Otherwise, render the phtml and show errors in it
			else {
					die(json_encode($result));

			}
		}
	}
	
	public function ajaxSuspPhotoAction() {
		$this->view->layout()->disableLayout();
		
		if ($this->_request->isPost())
		{
			// Get data from request
			$data = new Cubix_Form_Data($this->getRequest());
			
			
			$fields = array(
				'escort_id' => 'int',
				'link_1' => '',
				'link_2' => '',
				'link_3' => '',
				'comment' => 'notags|special'
			);
			
			$data->setFields($fields);
			$data = $data->getData();

			$validator = new Cubix_Validator();

            if (!strlen($data['link_1']))
			{	
				$validator->setError('link_1', $this->view->t('photo_link_required'));
			}
			elseif (!$validator->isValidURL($data['link_1']))
			{
				$validator->setError('link_1', $this->view->t('photo_link_not_valid'));
			}
			
			if (strlen($data['link_2']) && !$validator->isValidURL($data['link_2']))
			{
				$validator->setError('link_2', $this->view->t('photo_link_not_valid'));
			}
			
			if (strlen($data['link_3']) && !$validator->isValidURL($data['link_3']))
			{
				$validator->setError('link_3', $this->view->t('photo_link_not_valid'));
			}
			
			$url = str_replace( '.test' , '', $_SERVER['HTTP_HOST'] );
			$url = str_replace( 'www.' , '', $url );

			$_links = array( 'link_1', 'link_2', 'link_3' );

			foreach( $_links as $_link ){
				if( strpos( $data[ $_link ], $url ) !== false ){
					$validator->setError( $_link, $this->view->t('url_mustbe_from_another_website') );
					break;
				}
			}
			
			if (! strlen($data['comment'])) 
			{
				$validator->setError('comment', $this->view->t('comment_required'));
			}

			
            $result = $validator->getStatus();

			if ($validator->isValid()) 
			{
				$data['application_id'] = Cubix_Application::getId();
				$data['status'] = SUSPICIOUS_PHOTOS_REQUEST_PENDING;
								
				$user = Model_Users::getCurrent();
				$user_id = null;
				
				if ($user)
					$user_id = $user->id;
				
				$data['user_id'] = $user_id;
				
				$client = new Cubix_Api_XmlRpc_Client();
				$client->call('Users.addSuspPhoto', array($data));
			}	
			die(json_encode($result));
		}
	}

	public function ajaxReportProblemAction() {
		$this->view->app=$app= in_array(Cubix_Application::getId(), array(APP_EF,APP_6A))?true:false;
		$this->view->layout()->disableLayout();
		$url = $_SERVER['HTTP_REFERER'];
		if($this->_request->isPost()){
			// Fetch administrative emails from config
			$config = Zend_Registry::get('feedback_config');
			$support_email = $config['emails']['info'];
			$email_tpl = 'report_problem';
			// Get data from request
			$data = new Cubix_Form_Data($this->getRequest());

			$fields = array(
				'escort_id' => 'int',
				'name' => 'notags|special',
				'email' => '',
				'message' => 'notags|special',
				'problem'=>'int'

			);
			$data->setFields($fields);
			$data = $data->getData();
			$user = Model_Users::getCurrent();
			$problem_valid = array(1,2);
			if($user)
			$problem_valid[] = 3;
			
			$validator = new Cubix_Validator();

            if ( ! strlen($data['name']) ) {
				$validator->setError('name', 'Your Name is required');
			}

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Your Email is required');
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			}
			if ( ! strlen($data['message']) ) {
				$validator->setError('message','Please type the report you want to send');
			}
			if ($app && (! strlen($data['problem']) || !in_array($data['problem'],$problem_valid))) {
				$validator->setError('problem','Please choose the problem');
			}
			
            $result = $validator->getStatus();

			if ( $validator->isValid() ) {

				// Set the template parameters and send it to support
				
				$problems = Zend_Registry::get('defines');
				$problem = $problems['report_problems'][$data['problem']][0];
				$tpl_data = array(
					'name' => $data['name'],
					'email' => $data['email'],
					'message' => $data['message'],
					'problem'=>$problem,
					'application_id' => Cubix_Application::getId(),
					'url' => $url
				);
				$data['application_id'] = Cubix_Application::getId();

				try{
					Cubix_Email::sendTemplate($email_tpl, $support_email, $tpl_data, null);
					
					$mv2 = new Model_EscortsV2();
					$client = new Cubix_Api_XmlRpc_Client();
					
					$uid = $mv2->getUserId($data['escort_id']);
					$sales = $client->call('Users.getUserSalesPerson',array($uid));
					if($app)
					Cubix_Email::sendTemplate($email_tpl,$sales['email'], $tpl_data, null);
					
					$user_id = null;
									
					if ($user && $user->user_type == 'member')
						$user_id = $user->id;
					
					
					$client->call('Users.addProblemReport', array($data['name'], $data['email'],$data['problem'], $data['message'], $data['escort_id'], $user_id));

					$config = Zend_Registry::get('newsman_config');
					$list_id = $config['list_id'];
					$segment = $config['member'];
					$nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);
					$subscriber_id = $nm->subscribe($list_id, $data['email'],$data['name']);
					$is_added = $nm->bindToSegment($subscriber_id, $segment);

				}catch ( Exception $e ) {
						die($e->getMessage());
				}
				die(json_encode($result));
			}	// Otherwise, render the phtml and show errors in it
			else {
					die(json_encode($result));

			}

         }
	}
	
	public function getFilterAction() {
		$params = $this->_getParam('json');
		$this->view->layout()->disableLayout();
		$menus = array('filter' => NULL, 'sort' => NULL);

		$defs = Zend_Registry::get('definitions');

		$filterStructure = $defs['escorts_filter'];

		$menus['filter'] = new Cubix_NestedMenu(array('childs' => $filterStructure));
		$menus['filter']->setId('filter-options');
		$menus['filter']->setSelected($menus['filter']->getByValue(NULL));
		
		$menus['sort'] = new Cubix_NestedMenu(array('childs' => $defs['escorts_sort']));
		$menus['sort']->setId('sort-options');
		$menus['sort']->setSelected($menus['sort']->getByValue('random'));
		$this->view->menus = $menus;
		$this->view->params = $params;
	}
	
	public function ajaxGetPersonalSiteAction() {
		
		$this->view->layout()->disableLayout();
		$escort_id = (int) $this->_request->escort_id;
		$api_url = "http://www.escortbook.com/api.php?id=";

        $api = $api_url . urlencode( $escort_id );
		
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $api);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'escortbooking_api');
        $personal_site = curl_exec($curl_handle);
        curl_close($curl_handle);
		
		if ( $personal_site == "ID NOT FOUND" ) {
			$this->view->personal_site = false;
		} else {
            $this->view->personal_site = $personal_site;
        }

		$this->view->has_rates = $this->_request->has_rates;
		$this->view->has_services = $this->_request->has_services;
	}
	
	public function videosAction()
	{
		if($this->_request->isXmlHttpRequest()){
			$this->view->layout()->disableLayout();
			$this->_helper->viewRenderer->setScriptAction('video-list');
		}
		else{
			$this->view->layout()->setLayout('main');
		}
		
		$req = $this->_getParam('req');
		$param = explode('/', $req);
				
		$filter = array();
		$city_params = array();
		
		if (strpos($param[2], 'city') !== FALSE)
		{
			$city_params = explode('_', $param[2]);
		}
		
		if (count($city_params) && $city_params[2])
		{
			$modelC = new Model_Cities();
			$city_id = $modelC->getBySlug($city_params[2])->id;
			$filter['city_id'] = $city_id;
		}
		
		
		$page = $this->_getParam('page') ? intval($this->_getParam('page')) : 1;
		$per_page = 24;
		
		$cache = Zend_Registry::get('cache');
		$cache_key = 'video_listing_' . Cubix_I18n::getLang() . '_' . $page. '_' . $filter['city_id'];
		
		if ( ! $videos = $cache->load($cache_key) ) {
			$video_model = new Model_Video();
			$videos = $video_model->getVideos($filter, $page, $per_page );
			//$cache->save($videos, $cache_key, array());
		}
				
		$this->view->is_video_page = true;
		$this->view->videos = $videos['data'];
		$this->view->count = $videos['count'];
		$this->view->perPage = $per_page;
		$this->view->page = $page;
		
		
		
	}
    public function latestVideosAction() {
        $this->view->layout()->disableLayout();
        $this->view->videos = Model_Escort_List::getLatestVideos($this->_request->city_id);
    }
	
	public function ajaxInstantBookAction()
	{
		$this->view->layout()->disableLayout();
	}

	public function valentineAction()
	{
		
		// if(!isset($_GET['test'])){
			
		// 	header('Location: /');
		// 	header('HTTP/1.1 301 Moved Permanently');
		// 	die;
		// }
		$this->view->layout()->setLayout('main-simple');

		$filter = array();
		$filter['valentine'] = 1;
		$per_page = 60;
		$page = 1;
		$this->view->user_votes = array();

		$user = Model_Users::getCurrent();
		if(isset($user)){
			$this->view->user = $user;
			$this->view->user_votes = Model_Users::getUserVotes($user->id);
		}

		if(isset($this->_request->vote) && $this->_request->isPost() && isset($user)){
			if(count($this->view->user_votes) >= 3){
				var_dump('limit');die;
			}
			$req = $this->_request;
			$ip = Cubix_Geoip::getIP();
			$model = new Model_EscortV2Item();
			$model->voteValentine($req->escort_id, $req->user_id, $req->photo_id, $ip);
			echo count(Model_Users::getUserVotes($user->id));die;
		}

		$escorts = Model_Escort_List::getFiltered($filter, 'random', 1, 100);
		// shuffle($escorts);
		$this->view->escorts = $escorts;
		$this->_helper->viewRenderer->setScriptAction('valentine');




	}

}

