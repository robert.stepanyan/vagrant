<?php

class CamsController extends Zend_Controller_Action
{
	public function init()
	{	
		$this->view->layout()->setLayout('cams');
		/*if(isset($this->_request->sid)){
			$system = Zend_Registry::get('system_config');
			$sid = Cubix_Utils::crypter( urldecode($this->_request->sid), $system['cams']['key'] , $system['cams']['iv'] , 'd');
			setcookie("PHPSISID", $sid, time() + (86400 * 30), "/");
			if($_GET['test'] == 1){
				var_dump($sid,$this->_request->req);die;
			}
			$this->_redirect($this->_request->req);
			die;
		}*/
	}
		
	public function indexAction()
	{
		$this->user = Model_Users::getCurrent();
		$api_model = new Model_Api_Cams($this->user->id);
		$token = $api_model->userSignin($this->user);
		$redirect_path = null;
		if($this->_request->req)
		{
			$redirect_path = preg_replace('/[^-_a-z0-9\/]/i', '', $this->_request->req);
		}		
					
		//echo $this->view->getLink('cams-login-redirect', array('one-time-token' => $this->view->token, 'path' => $req )); die;
		$this->_response->setRedirect($this->view->getLink('cams-login-redirect', array('one-time-token' => $token, 'over18' => 1, 'path' => $redirect_path )));
	}
	
	public function goToBookAction()
	{
		$user = Model_Users::getCurrent();
		$api_model = new Model_Api_Cams($user->id);
		$token = $api_model->userSignin($user);
		$escort_id = intval($this->_request->escort_id);
		$duration = intval($this->_request->duration);
		$amount = intval($this->_request->price);
		
		$model_vc = new Model_VideoChatRequests();
		$data = [
			'escort_id' => $escort_id,
			'duration' => $duration,
			'amount' => $amount,
			'session_id' => session_id(),
			'user_agent' => $_SERVER['HTTP_USER_AGENT'],
			'user_id' => $user->id
		];

		$video_booking_id = $model_vc->add($data);
		
		$params = [
			'booking-purchase-modal' => 1, 
			'escort_id' => $escort_id, 
			'one-time-token' => $token,
			'duration' => $duration,
			'over18' => 1,
			'video-booking-id' => $video_booking_id	
		];
		
		$url = $this->view->getLink('cams-booking-redirect', $params);
		echo json_encode( ['url' => $url]); die;
	}
	
	public function loginAction()
	{
		$this->view->layout()->disableLayout();
	}
	
	public function signupAction()
	{
		$this->view->layout()->disableLayout();
	}
	
	public function buyTokensAction()
	{
		$this->view->layout()->disableLayout();
        $this->view->user = $this->user = Model_Users::getCurrent();
		$defines = Zend_Registry::get('defines');
		if(!$this->user || !$this->user->isMember()){
			die('no permission');
		}

        $defaultPaymentMethod = 'twispay';
        $this->view->paymentMethod = $paymentMethod = empty($this->_request->payment_method) ? $defaultPaymentMethod : $this->_request->payment_method;

		if($this->user->member_data['cam_trusted'] == 1){
			$token_currencies = $defines['EF_CAM_TOKEN_CURRENCIES'];
		}
		else{
			$token_currencies = array_slice($defines['EF_CAM_TOKEN_CURRENCIES'], 0, 3, true);
		}
		
        if($this->_request->isPost()){
					
			$tokens = $this->_request->tokens;

			if(!array_key_exists($tokens, $token_currencies)){
				die(json_encode(array('status' => 'payment_failure' )));
			}
						
			try{					
				$amount = $token_currencies[$tokens];
				
				$data = array(
					'user_id' => $this->user->id,
					'tokens' => $tokens,
					'amount' => $amount,
				);

				$model_member = new Model_Members();
				$id = $model_member->addTokenRequest($data);
                $reference = 'cam-' . $id . '-' . $this->user->id;

                switch (strtolower($paymentMethod)) {
                    case 'ecardon':

                        $TokenParams = array(
                            'amount' => $amount,
                            'descriptor' => $reference,
                            'merchantTransactionId' => $reference
                        );
						
						$ecardon_payment = new Model_EcardonGateway($TokenParams);
						
                        $token_data = $ecardon_payment->checkout();
						$token_data_dec = json_decode($token_data);
                        $client = new Cubix_Api_XmlRpc_Client();

                        if (isset($token_data_dec->result) && $token_data_dec->result->code == '000.200.100') {
                            $token = preg_replace('#[^a-zA-Z0-9.\-]#', '', $token_data_dec->id);
                            $client->call('OnlineBillingV2.storeToken', array($token, $this->user->id, 'ecardon'));
                            die(json_encode(array('status' => 'ecardon_success', 'checkoutId' => $token_data_dec->id)));
                        } else {
                            die(json_encode(array('status' => 'payment_failure')));
                        }
                        break;
                    case 'twispay':
                        $paymentConfigs = Zend_Registry::get('payment_config');
                        $twispayConfigs = $paymentConfigs['twispay'];

                        $data = array(
                            'siteId' => intval($twispayConfigs['siteid']),
                            'cardTransactionMode' => 'authAndCapture',
                            'backUrl' => APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/ef-cams/twispay-response',
                            'invoiceEmail' => '',
                            'customer' => [
                                'identifier' => 'user-' . $this->user->id,
                                'firstName' => $this->user->username,
                                'username' => $this->user->username,
                                'email' => $this->user->email,
                            ],
                            'order' => [
                                'orderId' => $reference,
                                'type' => 'purchase',
                                'amount' => $amount,
                                'currency' => $twispayConfigs['currency'],
                                'description' => 'Cam Tokens Desktop EscortForum',
                            ]
                        );

                        $base64JsonRequest = Cubix_Twispay_TwispayApi::getBase64JsonRequest($data);
                        $base64Checksum = Cubix_Twispay_TwispayApi::getBase64Checksum($data, $twispayConfigs['key']);

                        try {
                            $paymentFrom = "
                                <form id=\"twispay-payment-form\" action='{$twispayConfigs['url']}' method='post' accept-charset='UTF-8'>
                                    <input type='hidden' name='jsonRequest' value = \"{$base64JsonRequest}\" >
                                    <input type='hidden' name='checksum' value = \"{$base64Checksum}\" >
                                    <input type = \"submit\" value = \"Pay\" >
                                </form > ";

                        } catch (Exception $e) {
                            die(json_encode(array('status' => 'success', 'url' => APP_HTTP . '://www.escortforumit.xxx' . $this->view->getLink('ob-unsuccessful'))));
                        }
                        die(json_encode(array('status' => 'twispay_success', 'form' => $paymentFrom), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES));
                        break;
                }
			}
			catch(Exception $ex) {
				die(json_encode(array('status' => 'payment_failure' )));
			}
			
		}
		else{
			$this->view->token_currencies = $token_currencies;
		}
		
	}
	public function ecardonPopupAction()
	{
		$this->view->layout()->disableLayout();
	}		
	
	public function ecaPopupAction()
	{
		$this->view->layout()->disableLayout();
	}		
	public function ecardonResponseAction()
	{
		$token =  preg_replace('#[^a-zA-Z0-9.\-]#', '', $this->_request->id);
		$this->user = Model_Users::getCurrent();	
		if ( ! $token ) $this->_redirect('/ef-cams');
				
		$ecardon = new Model_EcardonGateway();
		$result = $ecardon->getStatus($token);
		
		$result_dec = json_decode($result);
		$client = new Cubix_Api_XmlRpc_Client();
		list($transaction_type, $request_id, $user_id) = explode('-', $result_dec->merchantTransactionId);
		
		$client->call('OnlineBillingV2.storeStatus', array($token, $user_id, $result));
		
		if(isset($result_dec->result) && ( $result_dec->result->code == '000.000.000' ||  $result_dec->result->code == '000.100.110')){
			$defines = Zend_Registry::get('defines');
			$api_model = new Model_Api_Cams($user_id);
			
			$amount = $result_dec->amount;
			$tokens = array_search($result_dec->amount, $defines['EF_CAM_TOKEN_CURRENCIES']);
			$payload = array(
				'purchase_amount' => (float)$amount,
				'purchase_currency' => 'EUR',
				'token_amount' => $tokens
			);
			
			$res = $api_model->memberPurchase($payload);
			
			$token_update = array('status' => 1);
			if($res['status'] != 200){
				$token_update = array( 
					'status' => TOKEN_PAYMENT_PAYED_NOT_RECEIVED_CAMS,
					'cams_header_status' =>  $res['status'],
					'cams_error' => json_encode($res['body']['error'])
				);
			}else{
				$token_update = array( 
					'status' => TOKEN_PAYMENT_PAYED_AND_RECEIVED_CAMS,
					'cams_header_status' =>  $res['status']
				);
			}
			
			Cubix_Email::sendTemplate('token_member_purchase', $this->user->email, array(
				'subject' => 'Il tuo pagamento per '. $tokens . ' token su Escortforumit.xxx Cams',
				'amount' => $amount,
				'username' => $this->user->username,
				'tokens' => $tokens
			));
			
			$model_member = new Model_Members();
			$model_member->updateTokenRequest($request_id, $user_id, $tokens, $token_update);
			$this->view->amount = $amount;
			$this->view->tokens = $tokens;
			$this->view->success = true;

		} 
		else {
			$this->view->success = false;
		}
		
		$this->view->username = $this->user->username;
		$this->view->user_type = 'member';
		$this->view->disable_token_section = 1;
	}

    /**
     * Endpoint action when user completes
     * his payment on Twispay page.
     *
     * @throws Zend_Exception
     * @return void
     */
    public function twispayResponseAction()
    {
        $twispayConfigs = Zend_Registry::get('payment_config')['twispay'];
		$this->user = Model_Users::getCurrent();	
        $decryptedData = Cubix_Twispay_TwispayApi::decryptIpnResponse($this->_request->opensslResult, $twispayConfigs['key']);

        if ($decryptedData['status'] == 'in-progress' || $decryptedData['status'] == 'complete-ok') {

            list($transactionType, $requestId, $userId) = explode('-', $decryptedData['externalOrderId']);
			
			$model_member = new Model_Members();
				
			if($model_member->checkStatus($requestId)){
				$this->_redirect('/ef-cams');
			}
			
            $token = json_encode(['orderId' => $decryptedData['orderId'], 'transactionId' => $decryptedData['transactionId']]);


            $client = new Cubix_Api_XmlRpc_Client();
            $client->call('OnlineBillingV2.storeToken', array($token, $userId, 'twispay'));
            $client->call('OnlineBillingV2.storeStatus', array($token, $userId, json_encode($decryptedData)));

            $defines = Zend_Registry::get('defines');
            
            $amount = $decryptedData['amount'];
            $tokens = array_search($amount, $defines['EF_CAM_TOKEN_CURRENCIES']);

            $payload = array(
                'purchase_amount' => (float)$amount,
                'purchase_currency' => 'EUR',
                'token_amount' => $tokens
            );

            $api_model = new Model_Api_Cams($userId);
            $res = $api_model->memberPurchase($payload);
            if ($res['status'] != 200) {
                $token_update = array(
                    'status' => TOKEN_PAYMENT_PAYED_NOT_RECEIVED_CAMS,
                    'cams_header_status' => $res['status'],
                    'cams_error' => json_encode($res['body']['error'])
                );
            } else {
                $token_update = array(
                    'status' => TOKEN_PAYMENT_PAYED_AND_RECEIVED_CAMS,
                    'cams_header_status' => $res['status']
                );
            }

			Cubix_Email::sendTemplate('token_member_purchase', $this->user->email, array(
				'subject' => 'Il tuo pagamento per '. $tokens . ' token su Escortforumit.xxx Cams',
				'amount' => $amount,
				'username' => $this->user->username,
				'tokens' => $tokens
			));
			
            $model_member->updateTokenRequest($requestId, $userId, $tokens, $token_update);
            $this->view->amount = $amount;
            $this->view->tokens = $tokens;
            $this->view->success = true;

        } else {
            $this->view->success = false;
        }
		$this->view->disable_token_section = 1;
    }

	public function balanceAction()
	{
		die('');
	}
	
	public function loginAgencyEscortsAction()
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$hash = $this->_getParam('hash');
		
		if(!$client->call('Agencies.checkCamHash', array($hash)) ){ die('redirect');
			$this->_redirect('/ef-cams');
		}
		$system = Zend_Registry::get('system_config');
		$this->view->iframe_url = $system['cams']['url'];
		
		//$client->call('Agencies.escortCamLogin', array($this->user->id));	
	}
	
	public function redirectAction()
	{
		header("HTTP/1.1 301 Moved Permanently");
		header('Location: https://escortforumit.xxx/ef-cams/' . $this->_getParam('req'));
		exit;
	}		
	
}