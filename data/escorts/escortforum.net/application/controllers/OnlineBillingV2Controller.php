<?php

class OnlineBillingV2Controller extends Zend_Controller_Action
{
	public static $linkHelper;

	const SECRET_PREFIX = 'ipnI3Inr';
	const GATEWAY_URL = 'https://gateway.cardgateplus.com/';
	const CURRENCY = 'EUR';
	const SITEID = 3076;
	/**
	 * @var Zend_Session_Namespace
	 */
	protected $_session;
	
	const STATUS_PENDING  = 1;
	const STATUS_ACTIVE   = 2;
	const STATUS_EXPIRED  = 3;
	const STATUS_CANCELLED = 4;
	const STATUS_UPGRADED = 5;
	const STATUS_SUSPENDED = 6;

	public static $STATUS_LABELS = array(
		self::STATUS_PENDING  => 'pending',
		self::STATUS_ACTIVE   => 'active',
		self::STATUS_EXPIRED  => 'expired',
		self::STATUS_CANCELLED => 'cancelled',
		self::STATUS_UPGRADED => 'upgraded',
		self::STATUS_SUSPENDED => 'suspended'
	);

	const OPTIONAL_PRODUCT_ADDITIONAL_CITY_1  = 11;
	const OPTIONAL_PRODUCT_ADDITIONAL_CITY_2  = 12;
	const OPTIONAL_PRODUCT_ADDITIONAL_CITY_3  = 13;

	private static $addCitiesCounts = array(
		11	=> 1,
		12	=> 2,
		13	=> 3
	);
	
	public static $ADD_CITIES_ARR = array(
		self::OPTIONAL_PRODUCT_ADDITIONAL_CITY_1,
		self::OPTIONAL_PRODUCT_ADDITIONAL_CITY_2,
		self::OPTIONAL_PRODUCT_ADDITIONAL_CITY_3
	);
	
	public static $pack_with_prem_cities = array(101,102,10,80,106,11,79,11,12,13,110,111,112,121,124,133,136,139,142,145, 165,168,174,177,183,186 );
	public static $zona_rossa_agency_packages = [183, 186];
	public static $zona_rossa_packages = [162, 165, 168, 171, 174, 177, 183, 186];
	
	public function init()
	{	
		$this->_request->setParam('no_tidy', true);
		$this->view->layout()->setLayout('private-v2');
		
		$anonym = array('epg-response', 'successful-payment', 'unsuccessful-payment', 'ecardon-response', 'twispay-response', 'failure', 'success', 'twispay-bump-response', 'powercash-response', 'btc-success');
		
		$this->user = Model_Users::getCurrent();
	
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}
		
		if ( ! in_array($this->_request->getActionName(), $anonym) && ! $this->user->isAgency() && ! $this->user->isEscort() ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}
		
		$this->view->headTitle('Private Area', 'PREPEND');

		$this->_session = new Zend_Session_Namespace('online_billing');
		
		$this->client = new Cubix_Api_XmlRpc_Client();
	}

    public function indexAction()
    {
        $this->view->user = $this->user;
        if ( $this->user->isEscort() ) {

        }

        if ( $this->user->isAgency() ) {
            $user_type = USER_TYPE_AGENCY;
            $p_user_type = USER_TYPE_AGENCY;
            $gender = null;
            $p_gender = null;

            $this->view->packages_list = $this->client->call('OnlineBillingV2.getPackagesList', array($p_user_type, $p_gender, $is_pseudo_escort, array(101)));
        } else {
            $escort_id = $this->user->escort_data['escort_id'];
            $is_pseudo_escort = $this->client->call('OnlineBillingV2.isPseudoEscort', array($escort_id));
			
            $m_escorts = new Model_EscortsV2();
            $escort = $m_escorts->get($escort_id);

            $this->view->need_age_verification = $this->client->call('OnlineBillingV2.getNeedAgeVerification', array($escort_id));
			
            $gender = $escort->gender;
            $p_gender = $escort->gender;

            $user_type = USER_TYPE_SINGLE_GIRL;
            $p_user_type = USER_TYPE_SINGLE_GIRL;

            if ( $is_pseudo_escort ) {
                $p_user_type = USER_TYPE_AGENCY;
            }

			$list_of_packages = array(9, 11, 10, 101, 102);
            $this->view->packages_list = $this->client->call('OnlineBillingV2.getPackagesList', array($p_user_type, $p_gender, $is_pseudo_escort, $list_of_packages));

            //$this->view->packages_list = $this->client->call('OnlineBillingV2.getPackagesList', array($p_user_type, $p_gender, $is_pseudo_escort, array(109, 110, 111, 112)));

            // <editor-fold defaultstate="collapsed" desc="Checking if escort can buy package">
            $escort_packages = $this->client->call('OnlineBillingV2.checkIfHasPaidPackage', array($this->user->escort_data['escort_id']));

            $allow = true;
            if ( count( $escort_packages ) ) {

                $message = '';
                $current_package = $escort_packages[0];
                $pending_package = $escort_packages[1];

                if ( $current_package && ( $current_package['status'] == self::STATUS_PENDING || $current_package['status'] == self::STATUS_SUSPENDED ) ) {
                    $allow = false;
                    $message = 'online_billing_has_pending';
                } elseif ( $pending_package && $pending_package['status'] == self::STATUS_PENDING ) {
                    $allow = false;
                    $message = 'online_billing_has_pending';
                } elseif ( $current_package['status'] == self::STATUS_ACTIVE && $current_package['expiration_date'] - time() > 7 *24*60*60 ) {
                    $allow = false;
                    $message = 'online_billing_has_active';
                }

                if ( ! $allow ) {
                    //$this->view->addScriptPath($this->view->getScriptPath('online-billing'));
                    $this->_helper->viewRenderer->setScriptAction("already-has-package");

                    $this->view->error_message = $message;
                    $this->view->user_type = $user_type;
                    return;
                }
            }
            // </editor-fold>
        }
        $this->view->user_type = $user_type;

        $this->view->pack_with_prem_cities = self::$pack_with_prem_cities;
        $this->view->addCitiesCounts = self::$addCitiesCounts;
    }
	
	public function wizardIndependentAction()
	{

		$this->view->layout()->disableLayout();
		$escort_id = $this->user->escort_data['escort_id'];
		
		// Check if bace city is Zona Rosa (cities fucked up by Corona )
		$is_zona_rossa = $this->client->call('OnlineBillingV2.isZonaRossa', array($escort_id));
				
		$working_locations = $this->client->call('OnlineBillingV2.getWorkingLocations', array($escort_id, $is_zona_rossa));
		$this->view->user = $this->user;
		
		$this->view->escort_id = $escort_id;
		
		$is_pseudo_escort = $this->client->call('OnlineBillingV2.isPseudoEscort', array($escort_id));
		//$this->view->mmg_cards = $this->client->call('OnlineBillingV2.getCards', array($this->user->id, 'mmgbill'));

		$m_escorts = new Model_EscortsV2();
		$gender = $m_escorts->getById($escort_id)->gender;
		
		$user_type = USER_TYPE_SINGLE_GIRL;

		if ( $is_pseudo_escort ) {
			$user_type = USER_TYPE_AGENCY;
		}
		
		if ( ! $this->_request->isPost() ) {
			
			$this->_helper->viewRenderer->setScriptAction("wizard-escort");
			
			$this->view->working_locations = $working_locations;
			
			// <editor-fold defaultstate="collapsed" desc="Checking once again if escort can buy package">
			$escort_packages = $this->client->call('OnlineBillingV2.checkIfHasPaidPackage', array($this->user->escort_data['escort_id']));

			$allow = true;
			$this->view->has_active_package = false;
			$this->view->show_special_packages = true;
			if ( count( $escort_packages ) ) {
				$this->view->package_expiration_date = $escort_packages[0]["expiration_date"];

				$current_package = $escort_packages[0];
				$pending_package = $escort_packages[1];
				
				if ( $current_package && ( $current_package['status'] == self::STATUS_PENDING || $current_package['status'] == self::STATUS_SUSPENDED ) ) {
					$allow = false;
				} elseif ( $pending_package && $pending_package['status'] == self::STATUS_PENDING ) {
					$allow = false;
				} elseif ( $current_package['status'] == self::STATUS_ACTIVE && $current_package['expiration_date'] - time() > 7 *24*60*60 ) {
					$allow = false;
				}
				
				if ( ! $allow ) {
					die;
				}
				
				/*if($current_package['expiration_date'] > strtotime("6th january 2020")){
					$this->view->show_special_packages = false;
				}*/
				
				if ( $current_package ) {
					$this->view->has_active_package = true;
				}
			}
			// </editor-fold>
		} else {
			
			$validator = new Cubix_Validator();
			$request = $this->_request;
			
			$optional_products = array();
			
			if ( ! $request->package_id ) {
				$validator->setError('package_id', __('package_required'));
			}
			
			$package_data = explode('-', $request->package_id);
			$package_id = $package_data[0];
			$prem_cities_count = 0;
			
			if ( $package_id && in_array($package_id, self::$pack_with_prem_cities) ) {
				$prem_cities_count += 1;
			}
						
			if ( $request->opt_product_prem_city ) {
				$optional_products[] = $request->opt_product_prem_city;
				$prem_cities_count = $prem_cities_count + self::$addCitiesCounts[$request->opt_product_prem_city];
			}
			
			if ( count($request->premium_cities) > $prem_cities_count ) {
				$validator->setError('premium-city', __('online_billing_prem_cities_limit', array('COUNT' => $prem_cities_count)));
			}
			
			if ( count($request->premium_cities) < $prem_cities_count ) {
				$validator->setError('premium-city', __('you_need_select_premium_cities', array('COUNT' => $prem_cities_count)));
			}
			
			if ( count($request->opt_products) ) {
				$optional_products = array_merge($optional_products, $request->opt_products);
			}
			
			// NEED AGE Cerfication
			$need_age_verification = $this->client->call('OnlineBillingV2.getNeedAgeVerification', array($escort_id));
			
			if($need_age_verification != 3){
				$validator->setError('package_id', __('mob_age_verif'));
			}
			
			
			/* Special Q packages not active now
			 * if(in_array($package_id, array(121,124,127,130,133,136,139,142,145))){
				$validator->setError('package_id', 'Not allowed to buy this package');
			}*/
			
			/*-->Checking if premium_city in working locations*/
			if ( $request->premium_cities ) {
				$working_ids = array();
				foreach($working_locations as $working_location) {
					$working_ids[] = $working_location['id'];
				}
				foreach($request->premium_cities as $city_id) {
					if ( ! in_array($city_id, $working_ids) ) {
						$validator->setError('premium-city', __('city_is_not_in_working_locations'));
						break;
					}
				}
			}
			/*<--Checking if premium_city in working locations*/

			if ( $validator->isValid() ) {
				
				$optional_products_array =
					array(
						'optional_products' => $optional_products,
						'premium_cities' => $request->premium_cities
					);

				if ($request->activation_type) {
					$optional_products_array['activation_type'] = array( $request->activation_type );
				}

				if( $request->activation_date ){
					$optional_products_array['activation_date'] = date("Y-m-d", $request->activation_date) ;
				}

				$optional_products_array_fix = count($optional_products) || count($request->premium_cities) ? $optional_products_array : array();

				if ($optional_products_array['activation_date']) {
					$optional_products_array_fix['activation_date'] = $optional_products_array['activation_date'];
					$optional_products_array_fix['activation_type'] = $optional_products_array['activation_type'];
				}
				
				/* 
				as fucken mmgbill has limitation for reference we have to make hash shorter
				will work fine till 2038-12-24 09:45:35 -- zzzzzz after will become 7 symbols 
				*/
				
				$hash  = base_convert(time(), 10, 36);
								
				$data[] = array(
					'user_id' => $this->user->id,
					'escort_id' => $escort_id,
					'agency_id' => null,
					'package_id' => $package_id,
					'hash' => $hash,
					'data' => serialize($optional_products_array_fix),
                    'is_mobile' => false,
                    'client_cookie_id'=>Model_Users::getCurrent()->clientID,
                    'ip'=>Cubix_Geoip::getIP(),
				);
				
				$is_crypto = $request->payment_gateway == 'coinsome' ? true : false;
				$amount = $this->client->call('OnlineBillingV2.addToShoppingCart', array($data, $this->user->id, $user_type, $hash, $is_crypto));
				
				if ( $amount > 2500 ) {
					die(json_encode(array('status' => 'error', 'msgs' => array('amount' => __('max_amount_per_transaction')))));
				}
				try {
					
					if ( $request->payment_gateway == 'mmgbill' ) {
						$mmgBill = new Model_MmgBillAPIV2();
						
						if ( $request->card && strlen($request->card) ) {
							$result = $mmgBill->charge($amount * 100, 'SCZ' . $this->user->id . 'Z' . $hash, $request->card);
								
							if ( $result['success'] ) {
								$ch = curl_init(APP_HTTP.'://backend.escortforumit.xxx/billing/online-billing/mmg-bill-callback');
								$data = array(
									'txn_status' => 'APPROVED',
									'is_oneclick' => 1,
									'ti_mmg' => $result['ti_mmg'],
									'ti' => 'SCZ' . $this->user->id . 'Z' . $hash ,
									'ia' => $amount,
									'prev_ti_mmg'	=> $request->card
								);
								curl_setopt($ch, CURLOPT_POST, 1);
								curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								$response = curl_exec($ch);
								curl_close($ch);
								
								die(json_encode(array('status' => 'success', 'url' =>  APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-successful'))));
							} else {
								die(json_encode(array('status' => 'success', 'url' => APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-unsuccessful'))));
							}
							
						}
						
						$hosted_url = $mmgBill->getHostedPageUrl($amount, 'SCZ' . $this->user->id . 'Z' . $hash , $request->save_info, APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-mmg-postback'));

						die(json_encode(array('status' => 'success', 'url' =>  $hosted_url)));
					}
					elseif ($request->payment_gateway == 'netbanx') {
						$nbx = new Model_NetbanxAPI();
						try {
							$order_data = array(
								'total_amount'	=> $amount * 100,
								'currency_code'	=> 'EUR',
								'merchant_ref_num'	=> 'SC-' . $this->user->id . '-' . $hash ,
								'locale'	=> 'en_US',
								'profile'	=> array(
								),
								'callback'	=> array( 
									array(
										'format'		=> 'json',
										'rel'			=> 'on_success',
										'retries'		=> 6,
										'returnKeys'	=> array('id', 'transaction.amount', 'transaction.authType', 'transaction.status', 'transaction.currencyCode', 'transaction.merchantRefNum', 'transaction.confirmationNumber', 
											'transaction.card.brand', 'transaction.card.country', 'transaction.card.expiry', 'transaction.card.lastDigits', 'profile.paymentToken', 'profile.id'
										),
										'synchronous'	=> true,
										'uri'			=> APP_HTTP.'://backend.escortforumit.xxx/billing/online-billing/netbanx-callback-sc'
									)
								),
								'redirect'	=> array(
									array(
										'rel'	=> 'on_success',
										'returnKeys'	=> array('id'),
										'uri'	=> APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-successful')
									)
								),
								'link'		=> array(
									array(
										'rel'	=> 'cancel_url',
										'uri'	=> APP_HTTP.'://www.escortforumit.xxx/online-billing-v2'
									),
									array(
										'rel'	=> 'return_url',
										'uri'	=> APP_HTTP.'://www.escortforumit.xxx/online-billing-v2'
									)
								)
							);
							
							$profile_id = $this->client->call('OnlineBillingV2.getNetbanxProfileId', array($this->user->id));
							
							if ( $profile_id ) {
								$order_data['profile']['id'] = $profile_id;
							} else {
								unset($order_data['profile']);
								//$order_data['profile']['merchantCustomerId'] = 'ef' . $this->user->id;
							}
							
							
							$order = $nbx->create_order($order_data);
						} catch(Exception $e) {
							$message = $e->getMessage();
							file_put_contents('/tmp/netbanx_error.log', "\n" . 'UserId: ' . $this->user->id . "\n" . var_export($message, true), FILE_APPEND);
							/*Problem with saved profile in test account for netbanx
							 * throw error existing merchantCustomerId
							 * getting that profile id and trying to creat order again
							 * can be removed later. 05.12.2014
							 */
							if ( strpos($message, 'Duplicate merchantCustomerId') !== false ) {
								$profile_id = str_replace('Duplicate merchantCustomerId - Existing profile id - ', '', $message);
								file_put_contents('/tmp/netbanx_error.log', "\n" . 'UserId: ' . $this->user->id . ', New profileId: ' . $profile_id, FILE_APPEND);
								unset($order_data['profile']['merchantCustomerId']);
								$order_data['profile']['id'] = $profile_id;
								$order = $nbx->create_order($order_data);
								die(json_encode(array('status' => 'success', 'url' =>  $order->link[0]->uri)));
							}
							
							
							die(json_encode(array('status' => 'success', 'url' => APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-unsuccessful'))));
						}
						die(json_encode(array('status' => 'success', 'url' =>  $order->link[0]->uri)));
					}
					elseif( $request->payment_gateway == 'ecardon'){
						
						$reference = 'SC-' . $this->user->id . '-' . $hash; 
						$TokenParams = array(
							'amount' => $amount,
							'descriptor' => $reference,
							'merchantTransactionId' => $reference
						);
						
						$ecardon_payment = new Model_EcardonGateway($TokenParams);
						
						$token_data = $ecardon_payment->checkout();
						$token_data_dec = json_decode($token_data);
						
						if(isset($token_data_dec->result) && $token_data_dec->result->code == '000.200.100'){
							$token =  preg_replace('#[^a-zA-Z0-9.\-]#', '', $token_data_dec->id);
							$this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id, 'ecardon'));
							die(json_encode(array('status' => 'success', 'checkoutId' => $token_data_dec->id ) ));
						}
						else{
							die(json_encode(array('status' => 'error' )));
						}
					}
					elseif( $request->payment_gateway == 'bitcoinpay'){
						
						$package_name = $this->client->call('OnlineBillingV2.getPackageById',array($package_id));
						
						$reference = 'SC-' . $this->user->id . '-' . $hash; 
						$token_params = array(
							"return_url" => APP_HTTP . '://escortforumit.xxx/online-billing-v2/bitcoin-response',
							"notify_url" => APP_HTTP . '://backend.escortforumit.xxx/billing/online-billing/bitcoin-callback-sc',
							'price' => $amount,
							'reference' => $reference,
							'item' => $package_name,
							'description' => " Self Checkout Desktop",
						);
						
						$bitcoin_payment = new Model_BitcoinPay($token_params);
						
						$token_data = $bitcoin_payment->checkout();
						$token_data_dec = json_decode($token_data);
						
						if(isset($token_data_dec->data->payment_url)){
							//$this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id, 'ecardon'));
							die(json_encode(array('status' => 'success', 'url' =>  $token_data_dec->data->payment_url)));
						}
						else{
							die(json_encode(array('status' => 'error' )));
						}
					}
					elseif ($request->payment_gateway == 'coinsome') {
						
						$order_id = $this->client->call('OnlineBillingV2.saveCoinsomeRequest', array($this->user->id, 'SC', $amount, $hash));
						$coinsome_model = new Cubix_Coinsome();
						$paymentFrom = '
                                <form id="payment-form" method="POST" name="criptoForm" action="'. $coinsome_model->generateFormUrl($order_id) .'">
									<input type="hidden" name="accountId" value="'. $escort_id. '"/>
									<input type="hidden" name="language" value="'. Cubix_I18n::getLang() . '"/>
									<input type="hidden" name="amount" value="'. $amount .'" />
								</form>';
						
						die(json_encode(array('status' => 'success', 'form' => $paymentFrom), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES ));
					}
					elseif ($request->payment_gateway == 'twispay') {
						$twis = new Cubix_Twispay_TwispayApi();

                        $paymentConfigs = Zend_Registry::get('payment_config');
                        $twispayConfigs = $paymentConfigs['twispay'];

                        /*if (in_array($this->user->id, [277346, 250871, 345573])) {
                            $amount = 1;
                        }*/

                        $data = array(
                            'siteId' => intval($twispayConfigs['siteid']),
                            'cardTransactionMode' => 'authAndCapture',
                            'backUrl' => APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/online-billing-v2/twispay-response',
                            'invoiceEmail' => '',
                            'customer' => [
                                'identifier' => 'user-' . $this->user->id,
                                'firstName' => $this->user->username,
                                'username' => $this->user->username,
                                'email' => $this->user->email,
                            ],
                            'order' => [
                                'orderId' => 'SC-' . $this->user->id . '-' . $hash,
                                'type' => 'purchase',
                                'amount' => $amount,
                                'currency' => $twispayConfigs['currency'],
                                'description' => 'Self Checkout EscortForum',
                            ]
                        );

                        $base64JsonRequest = Cubix_Twispay_TwispayApi::getBase64JsonRequest($data);
                        $base64Checksum = Cubix_Twispay_TwispayApi::getBase64Checksum($data, $twispayConfigs['key']);

                        // Store the gateway token for backend to get details about the transaction
                        // using this information
                        $token = json_encode(['externalOrderId' => $data['order']['orderId']]);
                        $isOrderCreated = $this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id, 'twispay'));

                        if (!$isOrderCreated) {
                            die(json_encode([
                                'status' => 'error',
                                'error'  => 'Twispay is not available at the moment',
                            ]));
                        }

                        try {
                            $paymentFrom = "
                                <form id=\"payment-form\" action='{$twispayConfigs['url']}' method='post' accept-charset='UTF-8'>
                                    <input type='hidden' name='jsonRequest' value = \"{$base64JsonRequest}\" >
                                    <input type='hidden' name='checksum' value = \"{$base64Checksum}\" >
                                    <input type = \"submit\" value = \"Pay\" >
                                </form > ";

                        } catch(Exception $e) {
							$message = $e->getMessage();
							file_put_contents('/tmp/twispay_error.log', "\n" . 'UserId: ' . $this->user->id . "\n" . var_export($message, true), FILE_APPEND);
						
							die(json_encode(array('status' => 'success', 'url' => APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-unsuccessful'))));
						}
						die(json_encode(array('status' => 'success', 'form' => $paymentFrom), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES ));
					}
					elseif($request->payment_gateway == '2000charge') {
						
						$first_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->username)); 
						$last_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->chat_info['nickName']));
						
						if( empty($first_name)){
							$first_name = 'NAME';
						}

						if(empty($last_name)){
							$last_name = 'LASTNAME';
						}
						
						$reference = 'SC-' . $this->user->id . '-' . $hash;

						$customer = new Cubix_2000charge_Model_Customer();
						$customer->setEmail($this->user->email);
						$customer->setCountry("IT");
						$customer->setFirstName($first_name);
						$customer->setLastName($last_name);

						$payment = new Cubix_2000charge_Model_Payment();
						$payment->setPaymentOption("paysafe");
						$payment->setHolder($first_name.' '.$last_name);

						$transaction = new Cubix_2000charge_Model_Transaction();
						$transaction->setCustomer($customer);
						$transaction->setPayment($payment);
						$transaction->setAmount($amount * 100);
						$transaction->setCurrency("EUR");
						$transaction->setIPAddress(Cubix_Geoip::getIP());
						$transaction->setMerchantTransactionId($reference);

						$host = 'https://' . $_SERVER['SERVER_NAME'];
						
						$redirectUrls = new Cubix_2000charge_Model_RedirectUrls();
						$redirectUrls->setReturnUrl($host . $this->view->getLink('ob-successful'));
						$redirectUrls->setCancelUrl($host .$this->view->getLink('ob-unsuccessful'));
						$transaction->setRedirectUrls($redirectUrls);
						$res = Cubix_2000charge_Transaction::post($transaction);
						$this->client->call('OnlineBillingV2.storeToken', array($res->id, $this->user->id, '2000charge'));
						die(json_encode(array('status' => 'success', 'url' =>  $res->redirectUrl)));
						
					}
					elseif($request->payment_gateway == 'powercash'){
						
						$reference = 'SC-' . $this->user->id . '-' . $hash; 
						$params = array(
							'amount' => $amount,
							'orderid' => $reference,
							'email' => $this->user->email,
							'customerip' => Cubix_Geoip::getIP(),
							'url_return' => $this->view->getLink('powercash-response')
						);
						
						$model_powercache = new Model_PowerCashGateway($params);
						$response = $model_powercache->checkout();
						
						$this->client->call('OnlineBillingV2.storeStatus', array($response['transactionid'], $this->user->id, serialize($model_powercache->returnStoreData())));
						$response['iframe'] = true;
						echo json_encode($response); die;
					}
				} catch(Exception $ex) {
					throw new Exception(" Payment issue '\nError: {$ex->getMessage()}");
					die(json_encode(array('status' => 'error' )));
				}


				die(json_encode(array('status' => 'success', 'url' =>  $order->link[0]->uri)));
			}
			
			die(json_encode($validator->getStatus()));
		}
		
		$profile_type = $this->client->call('Escorts.getField', array($escort_id, 'profile_type'));
		
		$this->view->template_data = array($user_type, $gender, $is_pseudo_escort, array(), $profile_type);
				
		$list_of_packages = array(9,11,10,78,79,80,101,102,107,109,110,111,112);
		if($is_zona_rossa){
			$list_of_packages = self::$zona_rossa_packages;
		}
 		$this->view->packages = $this->client->call('OnlineBillingV2.getPackagesList', array($user_type, $gender, $is_pseudo_escort, $list_of_packages, $profile_type));
	}
	
	public function wizardAgencyAction()
	{
		$this->view->layout()->disableLayout();
		$agency_id = $this->user->agency_data['agency_id'];
		
		$session = new Zend_Session_Namespace('self-checkout-wizard');
		$this->view->user = $this->user;
		// getCards method does not exists;
//		$this->view->mmg_cards = $this->client->call('OnlineBillingV2.getCards', array($this->user->id, 'mmgbill'));
		
		if ( ! $this->_request->isPost() ) {
			$session->unsetAll();
			$session->cart = array();
			$this->_helper->viewRenderer->setScriptAction("wizard-agency");
			$escorts = $this->client->call('OnlineBillingV2.getAgencyEscorts', array($agency_id));
			
			// <editor-fold defaultstate="collapsed" desc="checking if escorts can buy package">
			foreach($escorts as $i => $escort) {
				$allow = true;
				if ( count( $escort['escort_packages'] ) ) {
					$current_package = $escort['escort_packages'][0];
					$pending_package = $escort['escort_packages'][1];
					
					if ( $current_package && $current_package['status'] == self::STATUS_ACTIVE ) {
						$escorts[$i]['has_active_package'] = true;
					}
					
					if ( $current_package && ( $current_package['status'] == self::STATUS_PENDING || $current_package['status'] == self::STATUS_SUSPENDED ) ) {
						$allow = false;
					} elseif ( $pending_package && $pending_package['status'] == self::STATUS_PENDING ) {
						$allow = false;
					} elseif ( $current_package['status'] == self::STATUS_ACTIVE && $current_package['expiration_date'] - time() > 7 *24*60*60 ) {
						$allow = false;
					}
					
					if ( ! $allow ) {
						unset($escorts[$i]);
					}

				}
			}
			// </editor-fold>

			$this->view->escorts = $escorts;
			
		} else {
			$validator = new Cubix_Validator();
			$request = $this->_request;
			
			$optional_products = array();
			
			$prem_cities_count = 0;
			
			$package_id = null;
			if ( $request->package_id ) {
				$package_id = reset(explode('-', $request->package_id));
			}
			
			if ( $package_id && in_array($request->package_id, self::$pack_with_prem_cities) ) {
				$prem_cities_count += 1;
			}
			
			if ( ! $request->escort_id ) {
				$validator->setError('escort_id', __('escort_required'));
			}
			
			if ( ! $package_id ) {
				$validator->setError('package_id', __('package_required'));
			}
			
			if ( $request->opt_product_prem_city ) {
				$optional_products[] = $request->opt_product_prem_city;
				$prem_cities_count = $prem_cities_count + self::$addCitiesCounts[$request->opt_product_prem_city];
			}
			
			if ( count($request->premium_cities) > $prem_cities_count ) {
				$validator->setError('premium-city', __('online_billing_prem_cities_limit', array('COUNT' => $prem_cities_count)));
			}
			
			if ( count($request->premium_cities) < $prem_cities_count ) {
				$validator->setError('premium-city', __('you_need_select_premium_cities', array('COUNT' => $prem_cities_count)));
			}
			
			if ( count($request->opt_products) ) {
				$optional_products = array_merge($optional_products, $request->opt_products);
			}
			

			// NEED AGE Cerfication
			$need_age_verification = $this->client->call('OnlineBillingV2.getNeedAgeVerification', array($request->escort_id));
			
			if($need_age_verification != 3){
				$validator->setError('escort_id', __('mob_age_verif'));
			}
			
			if(in_array($package_id, array(121,124,145))){
				$validator->setError('package_id', 'Not allowed to buy this package');
			}
			
			/*-->Checking if premium_city in working locations*/
			if ( $request->premium_cities ) {
				
				//Zona Rossa logic for agency
				$is_zona_rossa_package = false;
				if(in_array($package_id, self::$zona_rossa_agency_packages)){
					$is_zona_rossa_package = true;
				}
				
				$working_locations = $this->client->call('OnlineBillingV2.getWorkingLocations', array($request->escort_id, $is_zona_rossa_package));
				$working_ids = array();
				foreach($working_locations as $working_location) {
					$working_ids[] = $working_location['id'];
				}
				foreach($request->premium_cities as $city_id) {
					if ( ! in_array($city_id, $working_ids) ) {
						$validator->setError('premium-city', __('city_is_not_in_working_locations'));
						break;
					}
				}
			}
			/*<--Checking if premium_city in working locations*/
			if ($request->activation_type  == 'on_date' && in_array( $request->package_id, array(121,124,127,130,133,136,139,142,145))) {
				$validator->setError('premium-city', 'activation type not allowed');
			}
			
			if ( $validator->isValid() ) {
				$package_data = explode('-', $request->package_id);

				$optional_products_array =
					array(
						'optional_products' => $optional_products,
						'premium_cities' => $request->premium_cities
					);

				if ($request->activation_type) {
					$optional_products_array['activation_type'] = array( $request->activation_type );
				}
				if( $request->activation_date ){
					$optional_products_array['activation_date'] = date("Y-m-d", $request->activation_date);
				}

				$optional_products_array_fix = count($optional_products) || count($request->premium_cities) ? $optional_products_array : array();

				if ($optional_products_array['activation_date']) {
					$optional_products_array_fix['activation_date'] = $optional_products_array['activation_date'];
					$optional_products_array_fix['activation_type'] = $optional_products_array['activation_type'];
				}

				
				$session->cart[] = array(
					'user_id' => $this->user->id,
					'escort_id' => $request->escort_id,
					'agency_id' => $agency_id,
					'package_id' => $package_id,
					'data' => serialize($optional_products_array_fix),
                    'is_mobile' => false,
                    'client_cookie_id'=>Model_Users::getCurrent()->clientID,
                    'ip'=>Cubix_Geoip::getIP(),
				);
				
				end($session->cart);
				$key = key($session->cart);
				
				die(json_encode(array('status' => 'success', 'cart_id' => $key)));
			}
			
			die(json_encode($validator->getStatus()));
		}
	}
	
	/*-->Only for agencies*/
	public function removeFromCartAction()
	{
		$cart_id = $this->_request->cart_id;
		
		$session = new Zend_Session_Namespace('self-checkout-wizard');
		
		$escort_id = $session->cart[$cart_id]['escort_id'];
		$m_escorts = new Model_EscortsV2();
		$escort = $m_escorts->get($escort_id);
		
		unset($session->cart[$cart_id]);
		die(json_encode(array(
			'status' => 'success', 
			'escort' => array('id' => $escort->id,'showname' => $escort->showname)))
		);
	}
	
	public function checkoutAction()
	{
		$session = new Zend_Session_Namespace('self-checkout-wizard');
		$user_type = USER_TYPE_AGENCY;
		$request = $this->_request;
		
		$validator = new Cubix_Validator();
		
		if ( ! count($session->cart) ) {
			$validator->setError('cart', __('cart_is_empty'));
		}

		$first_escort_id = $session->cart[0]['escort_id'];
		
		// NEED AGE Cerfication
		$need_age_verification = $this->client->call('OnlineBillingV2.getNeedAgeVerification', array($session->cart[0]['escort_id']));
		
		if($need_age_verification != 3){
			$validator->setError('escort', __('mob_age_verif'));
		}

		
		if ( $validator->isValid() ) {
			
			
			// as fucken mmgbill has limitation for reference we have to make hash shorter
			$hash  = base_convert(time(), 10, 36);
			
			foreach($session->cart as &$cart){
				$cart['hash'] = $hash;
			}
			
			$is_crypto = $request->payment_gateway == 'coinsome' ? true : false;
			$amount = $this->client->call('OnlineBillingV2.addToShoppingCart', array($session->cart, $this->user->id, $user_type, $hash, $is_crypto));
			
			/*$epg_payment = new Model_EpgGateway();
			$reference = 'SC-' . $this->user->id . '-' . md5(time());
			$token = $epg_payment->getTokenForAuth($reference, $amount, 'http://www.escortforumit.xxx/online-billing-v2/epg-response');
			$this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id));*/
			
			if ( $amount > 2500 ) {
				die(json_encode(array('status' => 'error', 'msgs' => array('amount' => __('max_amount_per_transaction')))));
			}

			try {
				
				if ( $request->payment_gateway == 'mmgbill' ) {
					$mmgBill = new Model_MmgBillAPIV2();
					if ( $request->card && strlen($request->card) ) {
						$result = $mmgBill->charge($amount, 'SCZ' . $this->user->id . 'Z' . $hash, $request->card);
						
						if ( $result['success'] ) {
							$ch = curl_init(APP_HTTP. '://backend.escortforumit.xxx/billing/online-billing/mmg-bill-callback');
							$data = array(
								'txn_status' => 'APPROVED',
								'is_oneclick' => 1,
								'ti_mmg' => $result['ti_mmg'],
								'ti' => 'SCZ' . $this->user->id . 'Z' . $hash,
								'ia' => $amount * 100,
								'prev_ti_mmg'	=> $request->card
							);
							curl_setopt($ch, CURLOPT_POST, 1);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							$response = curl_exec($ch);
							curl_close($ch);

							die(json_encode(array('status' => 'success', 'url' =>  APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-successful'))));
						} else {
							die(json_encode(array('status' => 'success', 'url' => APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-unsuccessful'))));
						}
					}

					$hosted_url = $mmgBill->getHostedPageUrl($amount, 'SCZ' . $this->user->id . 'Z' . $hash, $request->save_info, APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-mmg-postback'));
					die(json_encode(array('status' => 'success', 'url' =>  $hosted_url)));

				}
				elseif( $request->payment_gateway == 'ecardon'){ 
					
					$reference = 'SC-' . $this->user->id . '-' . $hash;
					$TokenParams = array(
						'amount' => $amount,
						'descriptor' => $reference,
						'merchantTransactionId' => $reference
					);
					
					$ecardon = new Model_EcardonGateway($TokenParams);
					$token_data = $ecardon->checkout();
					$token_data_dec = json_decode($token_data);
					
					if(isset($token_data_dec->result) && $token_data_dec->result->code == '000.200.100'){
						$token =  preg_replace('#[^a-zA-Z0-9.\-]#', '', $token_data_dec->id);
						$this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id, 'ecardon'));
						die(json_encode(array('status' => 'success', 'checkoutId' => $token_data_dec->id ) ));
					}
					else{
						die(json_encode(array('status' => 'error' )));
					}
				}
                elseif ($request->payment_gateway == 'twispay') {

                    $paymentConfigs = Zend_Registry::get('payment_config');
                    $twispayConfigs = $paymentConfigs['twispay'];

                    /*if (in_array($this->user->id, [277346, 250871])) {
                        $amount = 1;
                    }*/

                    $data = array(
                        'siteId' => intval($twispayConfigs['siteid']),
                        'cardTransactionMode' => 'authAndCapture',
                        'backUrl' => APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/online-billing-v2/twispay-response',
                        'invoiceEmail' => '',
                        'customer' => [
                            'identifier' => 'user-' . $this->user->id,
                            'firstName' => $this->user->username,
                            'username' => $this->user->username,
                            'email' => $this->user->email,
                        ],
                        'order' => [
                            'orderId' => 'SC-' . $this->user->id . '-' . $hash,
                            'type' => 'purchase',
                            'amount' => $amount,
                            'currency' => $twispayConfigs['currency'],
                            'description' => 'Self Checkout EscortForum',
                        ]
                    );

                    $base64JsonRequest = Cubix_Twispay_TwispayApi::getBase64JsonRequest($data);
                    $base64Checksum = Cubix_Twispay_TwispayApi::getBase64Checksum($data, $twispayConfigs['key']);

                    // Store the gateway token for backend to get details about the transaction
                    // using this information
                    $token = json_encode(['externalOrderId' => $data['order']['orderId']]);
                    $isOrderCreated = $this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id, 'twispay'));

                    if (!$isOrderCreated) {
                        die(json_encode([
                            'status' => 'error',
                            'error'  => 'Twispay is not available at the moment',
                        ]));
                    }

                    try {
                        $paymentFrom = "
                                <form id=\"payment-form\" action='{$twispayConfigs['url']}' method='post' accept-charset='UTF-8'>
                                    <input type='hidden' name='jsonRequest' value = \"{$base64JsonRequest}\" >
                                    <input type='hidden' name='checksum' value = \"{$base64Checksum}\" >
                                    <input type = \"submit\" value = \"Pay\" >
                                </form > ";

                    } catch(Exception $e) {
                        $message = $e->getMessage();
                        file_put_contents('/tmp/twispay_error.log', "\n" . 'UserId: ' . $this->user->id . "\n" . var_export($message, true), FILE_APPEND);

                        die(json_encode(array('status' => 'success', 'url' => APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-unsuccessful'))));
                    }
                    die(json_encode(array('status' => 'success', 'form' => $paymentFrom), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES ));
                }
				elseif ($request->payment_gateway == 'coinsome') {
						
					$order_id = $this->client->call('OnlineBillingV2.saveCoinsomeRequest', array($this->user->id, 'SC', $amount, $hash));

					$coinsome_model = new Cubix_Coinsome();
					
					$paymentFrom = '
							<form id="payment-form" method="POST" name="criptoForm" action="'. $coinsome_model->generateFormUrl($order_id) .'">
								<input type="hidden" name="accountId" value="'. $first_escort_id. '"/>
								<input type="hidden" name="language" value="'. Cubix_I18n::getLang() . '"/>
								<input type="hidden" name="amount" value="'. $amount .'" />
							</form>';

					die(json_encode(array('status' => 'success', 'form' => $paymentFrom), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES ));
				}
				elseif ($request->payment_gateway == '2000charge') {
					
					$first_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->username)); 
					$last_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->chat_info['nickName']));

					if( empty($first_name)){
						$first_name = 'NAME';
					}

					if(empty($last_name)){
						$last_name = 'LASTNAME';
					}
					
					$reference = 'SC-' . $this->user->id . '-' . $hash;

					$customer = new Cubix_2000charge_Model_Customer();
					$customer->setEmail($this->user->email);
					$customer->setCountry("IT");
					$customer->setFirstName($first_name);
					$customer->setLastName($last_name);

					$payment = new Cubix_2000charge_Model_Payment();
					$payment->setPaymentOption("paysafe");
					$payment->setHolder($first_name.' '.$last_name);

					$transaction = new Cubix_2000charge_Model_Transaction();
					$transaction->setCustomer($customer);
					$transaction->setPayment($payment);
					$transaction->setAmount($amount * 100);
					$transaction->setCurrency("EUR");
					$transaction->setIPAddress(Cubix_Geoip::getIP());
					$transaction->setMerchantTransactionId($reference);

					$host = 'https://' . $_SERVER['SERVER_NAME'];
					$redirectUrls = new Cubix_2000charge_Model_RedirectUrls();
					$redirectUrls->setReturnUrl($host . $this->view->getLink('ob-successful'));
					$redirectUrls->setCancelUrl($host . $this->view->getLink('ob-unsuccessful'));
					$transaction->setRedirectUrls($redirectUrls);

					$res = Cubix_2000charge_Transaction::post($transaction);
					
					$this->client->call('OnlineBillingV2.storeToken', array($res->id, $this->user->id, '2000charge'));
					die(json_encode(array('status' => 'success', 'url' =>  $res->redirectUrl)));
			 	}
				elseif($request->payment_gateway == 'powercash'){
						
					$reference = 'SC-' . $this->user->id . '-' . $hash; 
					$params = array(
						'amount' => $amount,
						'orderid' => $reference,
						'email' => $this->user->email,
						'customerip' => Cubix_Geoip::getIP(),
						'url_return' => $this->view->getLink('paycash-response')
					);

					$model_powercache = new Model_PowerCashGateway($params);
					$response = $model_powercache->checkout();
					
					$this->client->call('OnlineBillingV2.storeStatus', array($response['transactionid'], $this->user->id, serialize($model_powercache->returnStoreData())));
					$response['iframe'] = true;
					echo json_encode($response); die;
				}else {
					$nbx = new Model_NetbanxAPI();
					try {
					$order_data = array(
						'total_amount'	=> $amount * 100,
						'currency_code'	=> 'EUR',
						'merchant_ref_num'	=> 'SC-' . $this->user->id . '-' . $hash,
						'locale'	=> 'en_US',
						'profile'	=> array(),
						'callback'	=> array( 
							array(
								'format'		=> 'json',
								'rel'			=> 'on_success',
								'retries'		=> 6,
								'returnKeys'	=> array('id', 'transaction.amount', 'transaction.authType', 'transaction.status', 'transaction.currencyCode', 'transaction.merchantRefNum', 'transaction.confirmationNumber', 
									'transaction.card.brand', 'transaction.card.country', 'transaction.card.expiry', 'transaction.card.lastDigits', 'profile.paymentToken', 'profile.id'
								),
								'synchronous'	=> true,
								'uri'			=> APP_HTTP. '://backend.escortforumit.xxx/billing/online-billing/netbanx-callback-sc'
							)
						),
						'redirect'	=> array(
							array(
								'rel'	=> 'on_success',
								'returnKeys'	=> array('id'),
								'uri'	=> APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-successful')
							)
						),
						'link'		=> array(
							array(
								'rel'	=> 'cancel_url',
								'uri'	=> APP_HTTP.'://www.escortforumit.xxx/online-billing-v2'
							),
							array(
								'rel'	=> 'return_url',
								'uri'	=> APP_HTTP.'://www.escortforumit.xxx/online-billing-v2'
							)
						)
					);
					
					$profile_id = $this->client->call('OnlineBillingV2.getNetbanxProfileId', array($this->user->id));
					if ( $profile_id ) {
						$order_data['profile']['id'] = $profile_id;
					} else {
						unset($order_data['profile']);
						//$order_data['profile']['merchantCustomerId'] = 'ef' . $this->user->id;
					}

					$order = $nbx->create_order($order_data);

					} catch(Exception $e) {
						$message = $e->getMessage();
						file_put_contents('/tmp/netbanx_error.log', "\n" . 'UserId: ' . $this->user->id . "\n" . var_export($message, true), FILE_APPEND);
						/*Problem with saved profile in test account for netbanx
						 * throw error existing merchantCustomerId
						 * getting that profile id and trying to creat order again
						 * can be removed later. 05.12.2014
						 */
						if ( strpos($message, 'Duplicate merchantCustomerId') !== false ) {
							$profile_id = str_replace('Duplicate merchantCustomerId - Existing profile id - ', '', $message);
							file_put_contents('/tmp/netbanx_error.log', "\n" . 'UserId: ' . $this->user->id . ', New profileId: ' . $profile_id, FILE_APPEND);
							unset($order_data['profile']['merchantCustomerId']);
							$order_data['profile']['id'] = $profile_id;
							$order = $nbx->create_order($order_data);
							die(json_encode(array('status' => 'success', 'url' =>  $order->link[0]->uri)));
						}
						
						die(json_encode(array('status' => 'success', 'url' => APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-unsuccessful'))));
					}
					die(json_encode(array('status' => 'success', 'url' =>  $order->link[0]->uri)));
				}
			} catch(Exception $ex) {
				throw new Exception(" Payment issue '\nError: {$ex->getMessage()}");
				die(json_encode(array('status' => 'error' )));
				
			}
			
			
		}
		
		die(json_encode($validator->getStatus()));
	}
	/*<--Only for agencies*/
	
	public function getOptionalProductsAction()
	{
		$user_type = USER_TYPE_SINGLE_GIRL;
		if ( $this->user->isAgency() ) {
			$user_type = USER_TYPE_AGENCY;
		}
		
		if ( $this->user->isAgency() ) {
			$escort_id = $this->_request->escort_id;
		} else {
			$escort_id = $this->user->escort_data['escort_id'];
		}
		
		
		$m_escorts = new Model_EscortsV2();
		$escort = $m_escorts->get($escort_id);
		$gender = $escort->gender;
		
		$package_id = $this->_request->package_id;
		$package_id = reset(explode('-', $package_id));
		
		$is_pseudo_escort = $this->client->call('OnlineBillingV2.isPseudoEscort', array($escort_id));
		if ( $is_pseudo_escort ) {
			$user_type = USER_TYPE_AGENCY;
		}
		
		$profile_type = null;
		if ( in_array(Cubix_Application::getId(), array(APP_EF, APP_6A)) ) {
			$profile_type = $this->client->call('Escorts.getField', array($escort_id, 'profile_type'));
		}

		$optional_products = $this->client->call('OnlineBillingV2.getOptionalProducts', array($package_id, $user_type, $gender, $profile_type));
		
		$additional_cities = array();
		$additional_cities[] = array('id' => '', 'name' => __('no_additional_city'));
		
		if ( count($optional_products) ) {
			foreach ( $optional_products as $i => $product ) {
				$optional_products[$i]['name'] = Cubix_I18n::translate($product['name']);
				$optional_products[$i]['price'] = (int) $product['price'];
				if ( in_array($product['id'], self::$ADD_CITIES_ARR) ) {
					$additional_cities[] = $optional_products[$i];
					unset($optional_products[$i]);
				}

				
			}
		}
		
		if ( count($additional_cities) == 1 ) {//No additional cities opt product
			$additional_cities = array();
		}
		
		//Angel task - Do not show add cities products for diamond package
		//Reaseon: they always can change prem city with diamond package
		if ( ($package_id == 101 || $package_id == 102) && ($user_type == USER_TYPE_SINGLE_GIRL || $is_pseudo_escort) ) {
			$additional_cities = array();
		}
		
		$is_zona_rossa_package = in_array($package_id,  self::$zona_rossa_agency_packages) ? true : false;
		
		$working_locations = $this->client->call('OnlineBillingV2.getWorkingLocations', array($escort_id, $is_zona_rossa_package));
		
		die(json_encode(array(
			'optional_products'	=> $optional_products,
			'additional_cities'	=> $additional_cities,
			'isPackageWithPremCities'	=> in_array($package_id, self::$pack_with_prem_cities) ? true : false,
			'cities'	=> $working_locations
		)));
	}
	
	public function getEscortDataAction()
	{
		$escort_id = $this->_request->escort_id;
		
		$user_type = USER_TYPE_AGENCY;
		
		$m_escorts = new Model_EscortsV2();
		$escort = $m_escorts->get($escort_id);
		$gender = $escort->gender;
		
		$profile_type = $this->client->call('Escorts.getField', array($escort_id, 'profile_type'));
		
		$purchased_packages = $this->client->call('OnlineBillingV2.checkIfHasPaidPackage', array($escort_id));
		
		$list_of_packages = array(101,102,110);
		// Check if bace city is Zona Rosa (cities fucked up by Corona )
		$is_zona_rossa = $this->client->call('OnlineBillingV2.isZonaRossa', array($escort_id));
		if($is_zona_rossa){
			$list_of_packages = array_merge(self::$zona_rossa_agency_packages, $list_of_packages);
			
		}
				
		$packages = $this->client->call('OnlineBillingV2.getPackagesList', array($user_type, $gender, null, $list_of_packages, $profile_type));
		
		$show_special_packages = true;
		
		// if($purchased_packages[0]['expiration_date'] > strtotime("6th january 2020")){
		// 	$show_special_packages = false;
		// }
		
		foreach($packages as $i => $package) {
			if($package['id'] == 107){
				unset($packages[$i]);
				continue;
			}
			
			if(!$show_special_packages && in_array($package['id'],array( 109,110, 111,112 ))){
				unset($packages[$i]);
				continue;
			}
			
			$packages[$i]['name'] = str_replace('Lite', '', $package['name']) . ' ' . $package['period'] . ' ' . __('days') . ' ('. $package['price'] .' EURO)';
		}
		
		die(json_encode(array(
			'escort_packages' => $purchased_packages,
			'packages' => $packages,
		)));
	}
	
	public function epgResponseAction()
	{
		$this->view->layout()->disableLayout();
		
		$req = $this->_request;
		
		$token = $req->Token;
		if ( ! $token ) $this->_redirect('/');
		
		$epg_payment = new Model_EpgGateway();
		$result = $epg_payment->getTransactionStatus($token);
		
		if ( $result['ResultStatus'] == "OK" && $result['TransactionResult'] == "OK" ) {
			$this->_redirect($this->view->getLink('ob-successful'));
		} else {
			$this->_redirect($this->view->getLink('ob-unsuccessful'));
		}
	}
	
	public function epgGotdResponseAction()
	{
		$this->view->layout()->disableLayout();
		
		$req = $this->_request;
		
		$token = $req->Token;
		if ( ! $token ) $this->_redirect('/');
		
		$epg_payment = new Model_EpgGateway();
		$result = $epg_payment->getTransactionStatus($token);
		
		if ( $result['ResultStatus'] == "OK" && $result['TransactionResult'] == "OK" ) {
			$this->_redirect($this->view->getLink('private-v2-gotd-success'));
		} else {
			$this->_redirect($this->view->getLink('private-v2-gotd-failure'));
		}
	}
	
	public function forgetCreditCardAction()
	{
		$this->client->call('OnlineBillingV2.removeCard', array($this->_request->card));
		die;
	}
	public function setDefaultCardAction()
	{
		$this->client->call('OnlineBillingV2.setDefaultCard', array($this->user->id, $this->_request->card, 'mmgbill'));
		die;
	}
	
	public function mmgPostbackAction()
	{
		$this->view->txn_status = $this->_request->txn_status;
		$this->view->ti_mmg = $this->_request->ti_mmg;
	}
	
	public function successfulPaymentAction()
	{
	}
	
	public function unsuccessfulPaymentAction()
	{
	}
	
	public function ecardonPopupAction()
	{
		$this->view->layout()->disableLayout();
	}
	
	public function ecardonResponseAction()
	{
		$this->view->layout()->disableLayout();
		
		$req = $this->_request;
				
		$token =  preg_replace('#[^a-zA-Z0-9.\-]#', '', $req->id);
		
		if ( ! $token ) $this->_redirect('/');
		
		$ecardon_payment = new Model_EcardonGateway();
		$result = $ecardon_payment->getStatus($token);
		$result_dec = json_decode($result);
		if(isset($this->user->id)){
			$user_id = $this->user->id;
		}
		else{
			list($transaction_type, $ref, $hash) = explode('-', $result_dec->merchantTransactionId);
			$user_id = intval($ref);
		}
		$ecardon_payment->saveLog($token, $user_id, $result);
		$this->client->call('OnlineBillingV2.storeStatus', array($token, $user_id, $result));
		
		if(isset($result_dec->result) && ( $result_dec->result->code == '000.000.000' ||  $result_dec->result->code == '000.100.110')){
			$this->_redirect($this->view->getLink('ob-successful'));
		} 
		else {
			$this->_redirect($this->view->getLink('ob-unsuccessful'));
		}
	}
	
	public function bitcoinResponseAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
				
		if($req->bitcoinpay-status){
			$this->_redirect($this->view->getLink('ob-successful'));
		} 
		else {
			$this->_redirect($this->view->getLink('ob-unsuccessful'));
		}
	}

	public function btcSuccessAction()
	{
		
		$this->view->layout()->disableLayout();
		$req_json = file_get_contents('php://input');
		$payResponse = json_decode($req_json);
		$order_id = intval($payResponse->orderId);
		Cubix_Email::send('badalyano@gmail.com', 'BTC REDIRECT', $req_json, TRUE);
		if($order_id == 0 ){// || $payResponse->status == Cubix_Coinsome::ORDER_PENDING){ 
			header("HTTP/1.1 200 OK");
			die;
		}		
		//if($req->bitcoinpay-status){
			$this->_redirect($this->view->getLink('ob-successful'));
		/*} 
		else {
			$this->_redirect($this->view->getLink('ob-unsuccessful'));
		}*/
	}
	
    public function twispayResponseAction()
    {
        $this->view->layout()->disableLayout();
        $twispayConfigs = Zend_Registry::get('payment_config')['twispay'];

        $decryptedData = Cubix_Twispay_TwispayApi::decryptIpnResponse($this->_request->opensslResult, $twispayConfigs['key']);
        
        if ($decryptedData['transactionStatus'] == 'in-progress' || $decryptedData['transactionStatus'] == 'complete-ok') {
            $this->_redirect($this->view->getLink('ob-successful'));
        } else {
            $this->_redirect($this->view->getLink('ob-unsuccessful'));
        }
    }

	public function powercashResponseAction()
	{
//		$this->view->layout()->disableLayout();
        $client               = new Cubix_Api_XmlRpc_Client();
        $req                  = $this->_request;
        $this->view->error    = $req->errorcode;
        if (!$req->transactionid) {
            $tr_id = 0;
        } else {
            $tr_id = $req->transactionid;
        }
        $data                 = $client->call('OnlineBillingV2.getAmountForPowerCash', array($tr_id));
        $getDataForAmount     = unserialize($data[0]['response']);
        $amount               = $getDataForAmount['amount'];
        $this->view->response = array(
            'transactionid' => $req->transactionid,
            'Amount'         => $amount,
            'Currency'       => 'EUR',
            'Date'           => date('Y-m-d H:i')
        );
	}
	
    public function  twispayBumpResponseAction()
    {
     
        $twispayConfigs = Zend_Registry::get('payment_config')['twispay'];

        $decryptedData = Cubix_Twispay_TwispayApi::decryptIpnResponse($this->_request->opensslResult, $twispayConfigs['key']);
 	
        if ( $decryptedData['transactionStatus'] == 'in-progress' || $decryptedData['transactionStatus'] == 'complete-ok') {
        	
            $userId = str_replace('user-', '', $decryptedData['identifier']);
            $token = json_encode(['orderId' => $decryptedData['orderId'], 'transactionId' => $decryptedData['transactionId']]);
            
            $this->client->call('OnlineBillingV2.storeToken', array($token, $userId, 'twispay'));

            $res = explode('-', $decryptedData['externalOrderId']);
 			$bump_id = $res[1];
           
            $this->view->bump = $bump =  $this->client->call('Bumps.getBumpOrder', array($bump_id));

            switch ($bump['frequency']) {
            	case 'bump-option-1':
            		$frequency_text = __('bump_every_hour', array('hours'=> 4));
            		break;
            	case 'bump-option-2':
            		$frequency_text = __('bump_every_hour', array('hours'=> 8));
            		break;
            	case 'bump-option-3':
            		$frequency_text = __('bump_every_hour', array('hours'=> 12));
            		break;
            	case 'bump-option-4':
            		$frequency_text = __('bump_2_per_day_front');
            		break;
            	case 'bump-option-5':
            		$frequency_text = __('bump_1_per_day_front');
            		break;
            	case 'bump-option-6':
            		$frequency_text = __('not_avaiable');
            		break;
            	default:
            		$frequency_text = '';
            		break;
            }

            $this->view->frequency_text = $frequency_text;

        } else {
            $this->_redirect($this->view->getLink('ob-unsuccessful'));
        }
    }

   
}
