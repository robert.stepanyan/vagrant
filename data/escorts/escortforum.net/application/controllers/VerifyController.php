<?php

class VerifyController extends Zend_Controller_Action
{
	/**
	 * @var Model_Escorts
	 */
	public $model;
	
	/**
	 * @var Model_EscortItem
	 */
	public $escort;
	
	/**
	 * @var Model_VerifyRequests
	 */
	public $verifyRequests;
	
	public function init()
	{
		$this->view->layout()->setLayout('private');
		
		$this->user = Model_Users::getCurrent();

		if ( ! $this->user ) {
			$this->_redirect($this->view->getLink('signin'));
			//$this->_response->setRedirect($this->view->getLink('signin'));
			return;
		}
		
		$this->view->setScriptPath('../application/views/scripts/private');
		
		$this->view->sitePath()->prepend($this->view->t('100_verification'), $this->view->getLink('100p-verify'));
		
		$this->verifyRequests = new Model_VerifyRequests();
		
		$this->model = new Model_Escorts();
		
		$escort_id = intval($this->_getParam('id'));
		
		if ( $escort_id )
		{
			$m_agencies = new Model_Agencies();
			$agency_id = $m_agencies->getByUserId($this->user->id);

			$this->escort = $this->view->escort = $this->model->getById($escort_id);
			
			if ( ! $this->escort->isInAgency($agency_id->id) )
				$this->_response->setRedirect($this->view->getLink('100p-verify'));
		}
		else
		{
			
			$this->escort = $this->view->escort = $this->model->getByUserId($this->user->id);
			
		}
	}
	
	
	protected function _check()
	{

	    $isVerifyRequestSent = (isset($_COOKIE['vrf_pnd']) && $_COOKIE['vrf_pnd'] == $this->escort->id);
		// Checks if there is an active request
		if ( $this->escort->isVerified() && !$this->escort->isVerificationTimeLimit()) {
			$this->_forward('confirmed');
		}
        $this->activeRequest = $this->escort->getVerifyRequest();
		if ( ($this->activeRequest instanceof Model_VerifyRequest) || $isVerifyRequestSent) {
			$this->view->request = $this->activeRequest;
            $isPending = ($this->activeRequest instanceof Model_VerifyRequest) ? $this->activeRequest->isPending() : false;
			if ( $isPending || $isVerifyRequestSent) {
			    if ($isPending){
			        setcookie('vrf_pnd','',strtotime("- 1 year"));
                }
				$this->_forward('pending');
			}
			elseif ( $this->activeRequest->isBlur()) {
				if($this->verifyRequests->getBluredVerifyIds($this->escort->id)){
					$this->_forward('blur');
				}
				else{
					return FALSE;
				}
				
			}
			elseif ( $this->activeRequest->isConfirmed() ) {
				$this->_forward('confirmed');
			}
			
			return TRUE;
		}
		
		return FALSE;
	}
	
	public function pendingAction()
	{
		$this->_helper->viewRenderer('idcard-pending');
	}
	
	public function confirmedAction()
	{
		$this->_helper->viewRenderer('idcard-confirmed');
	}
	
	public function blurAction()
	{
		$this->view->blured_photos = $this->verifyRequests->getBluredVerifyIds($this->escort->id);
		$this->_helper->viewRenderer('idcard-blur');
		if ( $this->_request->isPost() ) {
			
			$photos = array();
			$images = new Cubix_Images();
			$config = Zend_Registry::get('images_config');
			$validator = new Cubix_Validator();
			if(count($_FILES['pic']['name']) > 0){
				foreach($_FILES['pic']['name'] as $index => $pic){
					if (
						! isset($_FILES['pic']['tmp_name'][$index]) ||
						! strlen($_FILES['pic']['tmp_name'][$index]) ||
						! isset($pic) ||	
						! strlen($pic)
					){
						$validator->setError('photos', 'Please upload all requested photos');
						BREAK;
					}

					$ext = explode('.', $pic);
					$ext = strtolower(end($ext));
					try{
						$file = $_FILES['pic']['tmp_name'][$index];
						if (!in_array( $ext , $config['allowedExts'])){
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
						}
						$photos[$index] = $images->save($file, $this->escort->id . '/verify', $this->escort->application_id, $ext);

					}
					catch (Exception $e) {
						$error = '';

						switch ( $e->getCode() ) {
							case Cubix_Images::ERROR_IMAGE_INVALID:
								$error = $e->getMessage();
								break;
							case Cubix_Images::ERROR_IMAGE_TOO_BIG:
								$error = $e->getMessage();
								break;
							case Cubix_Images::ERROR_IMAGE_TOO_SMALL:
								$error = $e->getMessage();
								break;
							case Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED:
								$error = $e->getMessage();
								break;
							default:
								$error = 'An unexpected error occured when uploading file';
						}

						$validator->setError('photo_' . $index, $error);
						continue;
					}	
				}
			}
			else{
				$validator->setError('photos', 'Please upload all requested photos');
			}
			
			if ($validator->isValid() ) {
				$save_data = array(
					'type' => Model_VerifyRequests::TYPE_IDCARD,
					'escort_id' => $this->escort->id
				);

				$item = $this->verifyRequests->save($save_data);

				foreach ( $photos as $photo ) {
					$item->addPhoto(array(
						'hash' => $photo['hash'],
						'ext' => $photo['ext'],
					));
				}
				$this->_helper->viewRenderer->setScriptAction('idcard-success');
			}
			else{
				$result = $validator->getStatus();
				$this->view->errors = $result['msgs'];
			}
		}
	}
	
	public function indexAction()
	{
		if ( ! $this->user ) {
			$this->_response->setRedirect($this->view->getLink('signin'));
			return;
		}
		
		if ( ! $this->user->isAgency() || $this->_getParam('id') )
		{
			$this->view->is_agency = false;
			
			if ( $this->_check() ) {
				return;
			}
		}
		else 
		{
			$this->view->is_agency = true;
			$escorts = $this->user->getAgency()->getEscortsWithOrdering(array(), $count);
			$alert_escortIds = $this->verifyRequests->getalertEscortIds($this->user->id);
			
			foreach ($escorts as $k => $escort)
			{
				$escort = new Model_EscortItem($escort);
				
				if (!in_array($escort->id,$alert_escortIds) && ($escort->isVerified() || is_null($escort['photo_hash'])) )
				{					
					unset($escorts[$k]);
				}
			}
			//var_dump($escorts); die;
			$this->view->escorts = $escorts;
		}
		
		if ( $this->_request->isPost() ) {
			$method = $this->_getParam('method');
			
			if ( ! in_array($method, array('webcam', 'idcard')) ) {
				die;
			}
			
			( intval($this->_getParam('id')) ) ? $escort_id = '?id=' . intval($this->_getParam('id')) : '';			
			
			$this->_response->setRedirect($this->view->getLink('100p-verify-' . $method) . $escort_id);
		}
	}
	
	public function webcamAction()
	{
		if ( $this->_check() ) {
			return;
		}
		
		$this->view->sitePath()->append($this->view->t('verify_webcam_session'), $this->view->getLink('100p-verify-webcam'));
		
		$data = new Cubix_Form_Data($this->_request);
		$fields = array(
			'dates' => '',
			'times' => '',
			'comment' => 'notags|special',
			'soft' => '',
			'soft_username' => 'special',
			'inform_method' => 'int',
			'email' => '',
			'phone' => ''
		);
		$data->setFields($fields);
		$data = $data->getData();
		
		$this->view->data = $data;
		
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();
			
			$data['dates'] = array_slice($data['dates'], 0, 3);
			$data['times'] = array_slice($data['times'], 0, 3);
			
			$selected_dates = array();
			foreach ( $data['dates'] as $i => $date ) {
				if ( $date ) {
					$selected_dates[$i] = $date;
				}
			}
			
			if ( count($selected_dates) < 3 ) {
				$validator->setError('dates', 'Please select at least 3 dates');
			}
			else {
				foreach ( $selected_dates as $index => $date ) {
					if ( $data['dates'][$index] < time() ) {
						$validator->setError('dates', 'Please provide dates in the future');
					}
				}
				
				foreach ( $selected_dates as $index => $date ) {
					if ( date("d M Y", $data['dates'][0]) == date("d M Y", $data['dates'][1]) 
							|| date("d M Y", $data['dates'][0]) == date("d M Y", $data['dates'][2])
							|| date("d M Y", $data['dates'][1]) == date("d M Y", $data['dates'][2]) ) {
						$validator->setError('dates', 'Selected dates must be different');
					}
				}
			}
			
			if ( $validator->isValid('dates') ) {
				foreach ( $selected_dates as $index => $date ) {
					if ( ! isset($data['times'][$index]) || ! $data['times'][$index] ) {
						$validator->setError('times', 'Please select corresponding times');
					}
				}
			}
			
			if ( ! $data['soft'] || ! in_array($data['soft'], array('skype', 'msn')) ) {
				$validator->setError('soft', 'Please select software you\'d like to use');
			}
			
			if ( ! strlen($data['soft_username']) ) {
				$validator->setError('soft_username', 'Username is required');
			}
			
			if ( ! in_array($data['inform_method'], array(Model_VerifyRequests::INFORM_EMAIL, Model_VerifyRequests::INFORM_PHONE)) ) {
				$validator->setError('inform_method', 'Select the way you want to be informed');
			}
			else {
				switch ( $data['inform_method'] ) {
					case Model_VerifyRequests::INFORM_EMAIL:
						if ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
							$validator->setError('email', 'A valid email address is required');
						}
					break;
					case Model_VerifyRequests::INFORM_PHONE:
						if ( ! preg_match('/.+/', $data['phone']) ) {
							$validator->setError('phone', 'A valid phone number is required');
						}
					break;
				}
			}
			
			if ( ! $validator->isValid() ) {
				$result = $validator->getStatus();
				
				$this->view->errors = $result['msgs'];
				return;
			}
			
			$item = new Model_VerifyRequest(array(
				'type' => Model_VerifyRequests::TYPE_WEBCAM,
				'escort_id' => $this->escort->id,
				'comment' => $data['comment'],
				'soft' => $data['soft'],
				'soft_username' => $data['soft_username'],
				'inform_method' => $data['inform_method'],
				'phone' => $data['phone'],
				'email' => $data['email']
				// status default PENDING
			));
			
			$timestamps = $this->_processDates($data);
			
			foreach ( $timestamps as $i => $timestamp ) {
				// that's safe, cause taken from `mktime`
				$item->{'date_' . ($i + 1)} = /*new Zend_Db_Expr('FROM_UNIXTIME(' . */$timestamp/* . ')')*/;
			}
			
			$this->verifyRequests->save($item);
			
			$this->_helper->viewRenderer->setScriptAction('webcam-success');
		}
	}
	
	protected function _processDates($data)
	{
		$timestamps = array();
		
		foreach ( $data['dates'] as $i => $date ) {
			if ( $date > time() && $data['times'][$i] ) {
				list($hours, $minutes) = explode(':', $data['times'][$i]);
				
				$timestamps[$i] = mktime($hours, $minutes, 0, date('m', $date), date('d', $date), date('Y', $date));
			}
		}
		
		return $timestamps;
	}
	
	public function idcardAction()
	{
	    $this->view->isAgency = $this->user->isAgency();
		if ( $this->_check() ) {
			return;
		}

		$this->view->sitePath()->append($this->view->t('verify_picture_upload'), $this->view->getLink('100p-verify-idcard'));
		
		$session = new Zend_Session_Namespace('verify_idcard');
		
		if ( $session->step == 'overview' ) {
			$this->_forward('idcard-overview');
		}
		
		if ( ! is_array($session->photos) ) {
			$session->photos = array();
		}

		$this->view->data['photos'] = array();

		$this->view->escort_id = $this->escort->id;
	}

    public function idCardAjaxAction() {
        $this->view->layout()->disableLayout();
        if ( $this->_request->isPost() ) {
            if($this->user->isAgency()){
                $this->view->escort_id = $this->escort->id;
            }

            $Files = $this->getFiles();
            $validator = new Cubix_Validator();
            //FOR Age Verification
            $data = new Cubix_Form_Data($this->getRequest());
            $data->setFields(array(
                'name' => 'notags|special',
                'image-0-comment' => 'notags|special',
                'image-1-comment' => 'notags|special',
                'image-2-comment' => 'notags|special',
                'image-3-comment' => 'notags|special',
                'image-4-comment' => 'notags|special',
                'last_name' => 'notags|special',
                'day' => 'int',
                'month' => 'int',
                'year' => 'int',
            ));

            $this->view->data = $data = $data->getData();
            $ValidatedFiles = $this->_validateIdCard($data, $Files, $validator);

            if($validator->isValid()){
                setcookie('vrf_pnd',$this->escort->getId(), strtotime( "+ 15 minutes"));

                ignore_user_abort(true);
                ob_start();
                echo $this->view->render("verify/idcard-pending.phtml");
                ob_end_flush();
                ob_flush();
                flush();
                session_write_close();
                fastcgi_finish_request();

                $SavedFiles = $this->saveFiles($ValidatedFiles);
                $photos_age_verify = array();
                $item = new Model_VerifyRequest(array(
                    'type' => Model_VerifyRequests::TYPE_IDCARD,
                    'escort_id' => $this->escort->id
                ));

                $save_data = array(
                    'type' => Model_VerifyRequests::TYPE_IDCARD,
                    'escort_id' => $this->escort->id
                );

                $item = $this->verifyRequests->save($save_data);
                foreach ($SavedFiles['photos'] as $i => $photo) {
                    $commentKey = 'image-' . $i . '-comment';
                    $comment = !empty($data[$commentKey]) ? $data[$commentKey] : '';
                    $comment = strip_tags(htmlspecialchars(substr(trim($comment), 0, 512)));
                    $item->addPhoto(array(
                        'hash' => $photo['hash'],
                        'ext' => $photo['ext'],
                        'comment' => $comment
                    ));
                    $photos_age_verify[] = array(
                        'hash' => $photo['hash'],
                        'ext' => $photo['ext'],
                    );
                }

                if (!empty($SavedFiles['video']) && is_array($SavedFiles['video'])) {
                    $item->addPhoto($SavedFiles['video']);
                }

                $age_verify_model = new Model_AgeVerification();
                $age_verify_data = array(
                    'escort_id' => $this->escort->id,
                    'name' => $data['name'],
                    'last_name' => $data['last_name'],
                    'day' => $data['day'],
                    'month' => $data['month'],
                    'year' => $data['year'],
                    'status' => -1,
                    'photos' => $photos_age_verify,
                    'idcard_id' => $item->id
                );
                $age_verify_model->save($age_verify_data);
            }
            else{
                $result = $validator->getStatus();
                $this->view->errors = $result['msgs'];
                echo $this->view->render("verify/idcard-ajax.phtml");
            }
            $this->deleteFiles();
        }
        die;
	}
	
	public function idcardOverviewAction()
	{
		if ( $this->_check() ) {
			return;
		}
		
		$session = new Zend_Session_Namespace('verify_idcard');
		
		if ( $session->step != 'overview' ) {
			die;
		}
		
		// Get photos from session to attach url and comment later
		$photos = $session->photos;
		$photos_age_verify = array();
		// Get comments from the request
		$comments = $this->_getParam('comment', array());
		if ( ! is_array($comments) ) $comments = array();
		
		// Attach url and comment to each photo uploaded in prev step
		$images = new Cubix_Images();
		foreach ( $photos as $i => $photo ) {
			$photos[$i]['url'] = $images->getServerUrl(new Cubix_Images_Entry(array(
				'catalog_id' => $this->escort->id . '/verify',
				'hash' => $photo['hash'],
				'ext' => $photo['ext'],
				'size' => 't100p',
				'application_id' => $this->escort->getApplicationId()
			)));
			
			$photos[$i]['comment'] = isset($comments[$i]) ? strip_tags(htmlspecialchars(substr(trim($comments[$i]), 0, 512))) : '';
			
			$photos_age_verify[] = array(
				'hash' => $photo['hash'],
				'ext' => $photo['ext'],
			);
		}
		
		$this->view->photos = $photos;
		
		if ( $this->_request->isPost() ) {
			$item = new Model_VerifyRequest(array(
				'type' => Model_VerifyRequests::TYPE_IDCARD,
				'escort_id' => $this->escort->id
				//'inform_method' => $session->inform_method,
				//'phone' => $session->phone,
				//'email' => $session->email
				// status default PENDING
			));

			$save_data = array(
				'type' => Model_VerifyRequests::TYPE_IDCARD,
				'escort_id' => $this->escort->id
			);
			
			$item = $this->verifyRequests->save($save_data);
			
			foreach ( $photos as $photo ) {
				$item->addPhoto(array(
					'hash' => $photo['hash'],
					'ext' => $photo['ext'],
					'comment' => $photo['comment']
				));
			}
			
			if(isset($session->video) && is_array($session->video)){
				$item->addPhoto($session->video);
			}
			
			$age_verify_model = new Model_AgeVerification();
			$age_verify_data = array(
				'escort_id' => $this->escort->id,
				'name' => $session->name,
				'last_name' => $session->last_name,
				'day' => $session->day,
				'month' => $session->month,
				'year' => $session->year,
				'status' => -1,
				'photos' => $photos_age_verify,
				'idcard_id' => $item->id
			);
						
			$age_verify_model->save($age_verify_data);
			$session->unsetAll();
			
			$this->_helper->viewRenderer->setScriptAction('idcard-success');
		}
	}

    public function processIdcardFilesAction() {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $allowed_image_Exts= array('jpg', 'jpeg', 'gif', 'png');
        $allowed_video_formats = array('flv','wmv','3gp','mkv','mp4', 'avi','mov');
        $escortID = $this->getRequest()->getParam('escortID');
        $escortID = $escortID ? $escortID : $this->escort->id;
        try {
            $response = $this->HTML5_upload($escortID);
            if($response['error'] == 0 && $response['finish']) {
                $error = false;
                $extension = end(explode(".", $response['name']));

                if (!in_array(strtolower($extension), array_merge($allowed_image_Exts,$allowed_video_formats)  ) )
                {
                    $error = 'Not allowed file extension.';
                }

                if ($error) {
                    if (!empty($response['tmp_name'])) {
                        unlink($response['tmp_name']);
                    }
                    $return = array(
                        'status' => '0',
                        'error' => $error
                    );

                } else {
                    $return = array(
                        'status' => '1',
                        'error' => '0',
                        'finish'=>true
                    );
                }
            }elseif ($response['error'] == 0 && !$response['finish']){
                $return = array(
                    'error' => '0',
                    'size' => $response['size']
                );
            }
            else {
                unlink($response['tmp_name']);
                $return = array(
                    'status' => '0',
                    'error' => 'Upload error, cannot write.'
                );
            }
            echo json_encode($return);
        } catch (Exception $e) {
            echo json_encode(array(
                'status' => 0,
                'error' => $e->getMessage()
            ));
        }
	}
	
	protected function _processFiles(Cubix_Validator $validator, $Files)
	{
		$indexes = array(0, 1, 2, 3, 4);
		
		$photos = array();
		
		$images = new Cubix_Images();
		$config = Zend_Registry::get('images_config');
		foreach ( $indexes as $index ) {
			if (
				! isset($Files['pic'][$index]['name']) ||
				! isset($Files['pic'][$index]['tmp_name']) ||
				! strlen($Files['pic'][$index]['name']) ||
				! strlen($Files['pic'][$index]['tmp_name'])
			) {
				continue;
			}
			
			$ext = explode('.', $Files['pic'][$index]['name']);
			$ext = strtolower(end($ext));
			
			$file = $Files['pic'][$index]['tmp_name'];
			
			try {

				if (!in_array( $ext , $config['allowedExts'])){
					throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
				}
				$photos[$index] = $images->save($file, $this->escort->id . '/verify', $this->escort->application_id, $ext);
				$images->save($file, $this->escort->id . '/age-verification', $this->escort->application_id, $ext, null, $photos[$index]['hash']);
				$photos[$index]['name'] = $Files['pic'][$index]['name'];
//                unlink($file);
			}
			catch (Exception $e) {
				$error = '';
				
				switch ( $e->getCode() ) {
					case Cubix_Images::ERROR_IMAGE_INVALID:
						$error = $e->getMessage();
						break;
					case Cubix_Images::ERROR_IMAGE_TOO_BIG:
						$error = $e->getMessage();
						break;
					case Cubix_Images::ERROR_IMAGE_TOO_SMALL:
						$error = $e->getMessage();
						break;
					case Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED:
						$error = $e->getMessage();
						break;
					default:
						$error = 'An unexpected error occured when uploading file';
				}
				
				$validator->setError('photo_' . ($index + 1), $error);
				
				continue;
			}
		}
		
                $photos_count = count($photos);
		if ( $photos_count < 2 ) {
                   $validator->setError('photos', __('2_images_required'));
		}
		
		return $photos;
	}

    /**
     * @param $catalog
     * @return array
     * @throws Zend_Exception
     */
    private function HTML5_upload($catalog)
    {
        $headers = array();
        $headers['Content-Length'] = $this->getRequest()->getServer('CONTENT_LENGTH');
        $headers['X-File-Id'] = $this->getRequest()->getServer('HTTP_X_FILE_ID');
        $headers['X-File-Name'] = $this->getRequest()->getServer('HTTP_X_FILE_NAME');
        $headers['X-File-Resume'] = $this->getRequest()->getServer('HTTP_X_FILE_RESUME');
        $headers['X-File-Size'] = $this->getRequest()->getServer('HTTP_X_FILE_SIZE');

        $response = array();
        $response['id'] = $headers['X-File-Id'];
        $response['name'] = basename($headers['X-File-Name']); 	// Basename for security issues
        $response['size'] = $headers['Content-Length'];
        $response['error'] = UPLOAD_ERR_OK;
        $response['finish'] = FALSE;

        // Is resume?
        $flag = (bool)$headers['X-File-Resume'] ? FILE_APPEND : 0;
        $filename = trim($catalog).'_id_'.trim($response['id']).'_'.trim($response['name']);
        $filename = strtolower($filename);
        $response['upload_name'] = $string = preg_replace('/\s+/', '', $filename);
        $config = Zend_Registry::get('images_config');
        $tempDir = !empty($config['temp_dir_path']) ? $config['temp_dir_path'] : sys_get_temp_dir();
        // Write file
        $file = $tempDir.DIRECTORY_SEPARATOR.$response['upload_name'];
        $response['tmp_name'] = $file.'.tmp';
        $chunk = file_get_contents('php://input');
        $bytes  = file_put_contents($file.'.tmp', $chunk, $flag);
        if ($bytes === false){
            $response['error'] = UPLOAD_ERR_CANT_WRITE;
        }
        else
        {
            $response['file_size'] = filesize($file);
            $response['X-File-Size'] = $headers['X-File-Size'];
            if (filesize($file.'.tmp') == $headers['X-File-Size'])
            {
                rename($response['tmp_name'], $file);
                $response['tmp_name'] = $file;
                $response['finish'] = true;
            }
        }

        return $response;
    }

    /**
     * @throws Zend_Exception
     */
    private function deleteFiles(){
	    $config = Zend_Registry::get('images_config');
        $tempDir = !empty($config['temp_dir_path']) ? $config['temp_dir_path'] : sys_get_temp_dir();
        $pattern = $tempDir.DIRECTORY_SEPARATOR.$this->escort->id.'_id_*';
        $FileList = glob($pattern);
        foreach ($FileList as $tmpFile){
            if(is_file($tmpFile)){
                unlink($tmpFile);
            }
        }
    }

    /**
     * @param $data
     * @param $Files
     * @param Cubix_Validator $validator
     * @return array
     * @throws Zend_Exception
     */
    private function _validateIdCard(&$data, $Files, Cubix_Validator &$validator){
        $allowed_video_formats = array('flv','wmv','3gp','mkv','mp4', 'avi','mov');
        if(!$data['day'] || !$data['month'] || !$data['year']){
            $validator->setError('photos', 'Please Select Born Date');
        }

        if(!$data['last_name']){
            $validator->setError('photos', 'Please Select Last Name');
        }

        if ( !$data['name']) {
            $validator->setError('photos', 'Please Select Name');
        }

        if(preg_match("/[^a-zA-Z\s]{1,}/", $data['name'])){
            $validator->setError('photos','The Name must contain only latin letters');
        }
        if(preg_match("/[^a-zA-Z\s]{1,}/", $data['last_name'])){
            $validator->setError('photos','The Last Name must contain only latin letters');
        }

        $data['name'] = preg_replace('!\s+!',' ', $data['name']);
        $data['last_name'] = preg_replace('!\s+!',' ', $data['last_name']);
        $data['name'] = strtoupper(trim($data['name']));
        $data['last_name'] = strtoupper(trim($data['last_name']));

        $video_config =  Zend_Registry::get('videos_config');
        if(!isset($Files['video']["tmp_name"]) || !is_file($Files['video']["tmp_name"])){
            if($this->escort->need_idcard_video) {
                $validator->setError('photos', 'Please upload video');
            }
        }
        else{
            if (!in_array( strtolower($Files['video']['ext']) , $allowed_video_formats)){
                $validator->setError('photos', Cubix_I18n::translate('sys_error_upload_video_allowed_extensions', array('formats' => implode(', ', $allowed_video_formats))));
                unlink($Files['video']["tmp_name"]);
            }
            elseif(number_format(($Files['video']['size']/pow(1024,2)),2) > intval($video_config['VideoMaxSize'])){
                $validator->setError('photos', Cubix_I18n::translate('sys_error_upload_video_allowed_max_size',array('size' =>$this->config['VideoMaxSize'])));
                unlink($Files['video']["tmp_name"]);
            }
        }

        $photos = array();
        $config = Zend_Registry::get('images_config');
        $p_min_width = 400;
        $p_min_height = 600;
        $l_min_width = 500;
        $l_min_height = 375;

        foreach ($Files['pic'] as $index => $file) {
            if (!isset($file['tmp_name']) || !is_file($file['tmp_name'])) {
                continue;
            }
            $img_info = @getimagesize($file['tmp_name']);

            list($width, $height) = $img_info;
            if (!in_array(strtolower($file['ext']), $config['allowedExts'])) {
                $validator->setError('photo_' . ($index + 1), Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))));
                unlink($file['tmp_name']);
                continue;
            }
            if ( $width < $height ) {
                if ( $width < $p_min_width || $height < $p_min_height ) {
                    $validator->setError('photo_' . ($index + 1), Cubix_I18n::translate('sys_error_upload_img_size_min_a6', array('min_width' => $p_min_width, 'min_height' => $p_min_height, 'ratio' => Cubix_I18n::translate('portrait'))));
                    unlink($file['tmp_name']);
                    continue;
                }
            } else {
                if ( $width < $l_min_width || $height < $l_min_height ) {
                    $validator->setError('photo_' . ($index + 1),Cubix_I18n::translate('sys_error_upload_img_size_min_a6', array('min_width' => $l_min_width, 'min_height' => $l_min_height, 'ratio' => Cubix_I18n::translate('landscape'))));
                    unlink($file['tmp_name']);
                    continue;
                }
            }
            $photos[$index] = $file;
        }

        $photos_count = count($photos);
        if ( $photos_count < 2 ) {
            $validator->setError('photos', __('2_images_required'));
        }
        $data['name'] = preg_replace('/(\s)+/','$1', $data['name']);
        $data['last_name'] = preg_replace('/(\s)+/','$1', $data['last_name']);
        return array('video' => $Files['video'], 'pic'=>$photos);
    }

    /**
     * @param $Files
     * @return array
     * @throws Zend_Exception
     */
    private function saveFiles($Files){
	    $photos = array();
        $videoData = array();
        $images = new Cubix_Images();
        $video_config =  Zend_Registry::get('videos_config');
	    foreach ($Files['pic'] as $index => $file){
	        try{
                $photos[$index] = $images->save($file['tmp_name'], $this->escort->id . '/verify', $this->escort->application_id, $file['ext']);
                $images->save($file['tmp_name'], $this->escort->id . '/age-verification', $this->escort->application_id, $file['ext'], null, $photos[$index]['hash']);
                $photos[$index]['name'] = $file['name'];
            }catch (Exception $e){
                $client = Cubix_Api_XmlRpc_Client::getInstance();
                $emailBody = "<pre>
                                       {$e->getMessage()}
                                       {$e->getTraceAsString()}
                                  </pre>";
                $Attachments = array();
                $trace= $e->getTrace();
                if(!empty($trace[0]['function']) && $trace[0]['function'] == '_cubix_images_ftp_error_handler'){
                    $file = $trace[0]['args'][4]['local'];
                }elseif ($trace[0]['file'] && strpos($trace[0]['file'],'library/Cubix/Images.php')>0){
                    $file = $trace[0]['args'][0];
                }else{
                    foreach ($trace as $line){
                        if (strpos($line['file'],'library/Cubix/Images.php') > 0){
                            $file = $line['args'][0];
                            break;
                        }
                    }
                }
                if ($file && is_file($file)){
                    $item = array(
                        'item'=> file_get_contents($file),
                        'type' => mime_content_type($file),
                        'filename' => basename($file),
                        'name' => basename($file)
                    );
                    $Attachments[] = $item;
                }
                $client->call('Email.send', array("narek.amirkhanyan92@gmail.com", '100% verify error', $emailBody, true, 'info@escortforumit.xxx', null, Cubix_Application::getId(), $Attachments), 100);
	            unlink($file['tmp_name']);
	            continue;
            }
        }
        if (isset($Files['video']['tmp_name']) && is_file($Files['video']['tmp_name'])) {
            $video_ftp = new Cubix_VideosCommon();
            $video_hash = uniqid() . '_idcard';
            $video = $Files['video']['tmp_name'];
            $video_model = new Cubix_ParseVideo($video, $this->escort->id, $video_config);
            $video = $video_model->ConvertToMP4();
            $video_ext = 'mp4';
            $name = $video_hash . '.' . $video_ext;

            if ($video_ftp->_storeToPicVideo($video, $this->escort->id, $name)) {
                $videoData = array('hash' => $video_hash, 'ext' => $video_ext, 'is_video' => 1);
            }
        }


        return array('video'=>$videoData, 'photos'=>$photos);
    }

    /**
     * @return array
     * @throws Zend_Exception
     */
    private function getFiles() {
        $allowed_image_Exts= array('jpg', 'jpeg', 'gif', 'png');
        $allowed_video_formats = array('flv','wmv','3gp','mkv','mp4', 'avi','mov');
        $config = Zend_Registry::get('images_config');
        $tempDir = !empty($config['temp_dir_path']) ? $config['temp_dir_path'] : sys_get_temp_dir();
        $pattern = $tempDir.DIRECTORY_SEPARATOR.$this->escort->id.'_id_*';
        $FileList = glob($pattern);
        $Files = array();

        foreach ($FileList as $k => $file){
            $pathInfo = pathinfo($file);
            if ($pathInfo['extension'] == 'tmp'){
                unlink($file);
                continue;
            }
            if (in_array(strtolower($pathInfo['extension']), $allowed_image_Exts)) {
                $Files['pic'][] = array(
                    'tmp_name' => $file,
                    'name' => $pathInfo['basename'],
                    'size' => filesize($file),
                    'ext' => $pathInfo['extension']
                );
            }
            elseif (in_array(strtolower($pathInfo['extension']), $allowed_video_formats)) {
                $Files['video'] = array(
                    'tmp_name' => $file,
                    'name' => $pathInfo['basename'],
                    'size' => filesize($file),
                    'ext' => $pathInfo['extension']
                );
            }
        }

        return $Files;
    }
}
