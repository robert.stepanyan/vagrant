<?php

class ForumController extends Zend_Controller_Action
{
	public function init()
	{
		$this->view->layout()->disableLayout();
	}

	public function indexAction()
	{

	}

	public function bannersAction()
	{

	}

	public function newEscortsAction()
	{
		$cache = Zend_Registry::get('cache');
		$cache_key = 'forum_v2_new_escorts_list_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang() . '_page_' . 1;

		$filter = array('e.gender = ?' => 1);

		if ( ! $escorts = $cache->load($cache_key) ) {

			$escorts = Model_Escort_List::getFiltered($filter, 'date-activated', 1, 10, $count, 'new_list');

			$cache->save($escorts, $cache_key, array());
		}

		$this->view->escorts = $escorts;
	}

	public function diamondEscortsAction()
	{
		$cache = Zend_Registry::get('cache');
		$cache_key = 'forum_v2_diamond_escorts_list_' . Cubix_Application::getId() . '_lang_' . Cubix_I18n::getLang() . '_page_' . 1;

		$filter = array('e.gender = ?' => 1, 'FIND_IN_SET(14, e.products) > ?' => 0);

		if ( ! $escorts = $cache->load($cache_key) ) {

			$escorts = Model_Escort_List::getFiltered($filter, 'random', 1, 8, $count);

			$cache->save($escorts, $cache_key, array());
		}

		shuffle($escorts);

		$this->view->escorts = $escorts;
	}

	public function expoEscortsAction()
	{
		$escorts = Model_EscortsV2::getExpoEscorts(9);

		$this->view->escorts = $escorts;
	}

	public function gAction()
	{
		$geo = Cubix_Geoip::getClientLocation();

		var_dump($geo);

		die;
	}
}
