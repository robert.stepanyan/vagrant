<?php

class IndexController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$this->_forward('index', 'escorts');
	}

	protected function _ajaxError()
	{
		die(json_encode(array('result' => 'error')));
	}
	
	/*public function chatOnlineCronAction()
	{
		ini_set('memory_limit', '1024M');
		
		$db = Zend_Registry::get('db');

		$xml = file_get_contents("php://input");
		$online_users = simplexml_load_string($xml);
		
		// Reset all online users
		$db->update('escorts', array('is_online' => 0), $db->quoteInto('is_online = ?', 1));
		
		$user_ids = array();
		foreach ( $online_users->children() as $user ) {
			$user_ids[] = (string) $user;
		}
		
		if ( count($user_ids) > 0 ) {
			$db->query('UPDATE escorts SET is_online = 1 WHERE user_id IN (\'' . implode('\',\'', $user_ids) . '\')');
		}
		

		die;
	}*/
	
	public function chatOnlineCronAction()
	{
		ini_set('memory_limit', '1024M');
		
		$db = Zend_Registry::get('db');

		$xml = file_get_contents("php://input");
		
		if (preg_match("#presenceFull#", $xml) ) {
		
			$online_users = simplexml_load_string($xml);
			
			// Reset all online users
			$db->update('escorts', array('is_online' => 0), $db->quoteInto('is_online = ?', 1));

			$user_ids = array();
			foreach ( $online_users->children() as $user ) {
				$user_ids[] = (string) $user;
			}

			if ( count($user_ids) > 0 ) {
				$db->query('UPDATE escorts SET is_online = 1 WHERE user_id IN (\'' . implode('\',\'', $user_ids) . '\')');
			}

			$db->query('UPDATE online_in_chat SET online = ?', array(count($user_ids)));
		}
		
		die;
	}

	public function chatOnlineBtnAction()
	{
		return;
		$this->view->layout()->disableLayout();

		$req = $this->_request;

		$username = $req->username;
		try {
			$online_users = file_get_contents(APP_HTTP.'://www.escortforumit.xxx:35555/roomonlineusers.js?roomid=1');
			
			if ( strlen($online_users) > 0 ) {
				$online_users = substr($online_users, 20, (strlen($online_users) - 21));

				$online_users = json_decode($online_users);

				if ( count($online_users) > 0 ) {
					foreach ( $online_users as $user ) {
						if( $user->name == $username ) {
							$this->view->show_btn = true;
							break;
						}
					}
				}
			}
			else {
				$this->view->show_btn = false;
			}
		}
		catch ( Exception $e ) {

		}
	}

	public function escortPhotosAction()
	{
		$id = intval($this->_getParam('id'));
		if ( $id <= 0 ) $this->_ajaxError();

		$escort = new Model_EscortItem();
		$escort->setId($id);

		$photos = $escort->getPhotos();

		$result = array();
		foreach ( $photos as $photo ) {
			$result[] = $photo->hash . '.' . $photo->ext;
		}
		
		die(json_encode(array('result' => 'success', 'data' => $result)));
	}

	public function checkHitsCacheAction()
	{
		$config = Zend_Registry::get('system_config');
		$data = Model_Escorts::getAllCachedHits();

		echo 'Cache Limit: ' . $config['hitsCacheLimit'] . "\n";
		echo 'Total Count: ' . count($data) . "\n\n";

		print_r($data);
		die;
	}

	public function splashAction()
	{
		$this->view->layout()->disableLayout();

		//$escorts = array(142241, 142256, 142257, 142260);

		//Model_Escorts::hit($escorts[rand(0, 3)]);

		/*$this->view->then = $this->_request->then;
		
		if ( $this->_request->enter ) {
			$domain = preg_replace('#^(.+?)([^\.]+)\.([^\.]+)$#', '.$2.$3', $_SERVER['HTTP_HOST']);
			setcookie('18', '1', null, '/', $domain);
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $this->view->then);
		}*/
	}
	
	public function feedbackAction()
	{
		$this->_request->setParam('no_tidy', true);

		$model = new Model_EscortsV2();

		$user = Model_Users::getCurrent();

		$is_premium = false;
		if ( isset($user->member_data) ) {
			$is_premium = $user->member_data['is_premium'];
		}
	
		$this->view->is_premium = $is_premium;

		// Fetch administrative emails from config
		$config = Zend_Registry::get('feedback_config');
		$site_emails = $config['emails'];
		
		$this->view->layout()->setLayout('main-simple');
		
		$is_ajax = ! is_null($this->_getParam('ajax'));

        $feedback = new Model_Feedback();

		// If the request is ajax, disable the layout
		if ( $is_ajax ) {
			$this->view->layout()->disableLayout();
		}
		
		// Get data from request
		$data = new Cubix_Form_Data($this->getRequest());
		$fields = array(
			'to' => '',
			'about' => 'int',
			'name' => 'notags|special',
			'email' => '',
			'message' => 'notags|special',
			'captcha' => '',
			'rand' => ''
		);
		$data->setFields($fields);
		$data = $data->getData();

        $blackListModel = new Model_BlacklistedWords();
		$this->view->data = $data;
		
		// If the to field is invalid, that means that user has typed by 
		// hand, so it's like a hacking attempt, simple die, without explanation
		if ( ! is_numeric($data['to']) && ! in_array($data['to'], array_keys($site_emails)) ) {
			die;
		}
		
		// If escort id was specified, fetch it's email and showname, 
		// construct the result email template and send it to escort
		if ( is_numeric($data['to']) ) {
			$escort = $model->get($data['to']);
			$this->view->escort = $escort;
			//var_dump($escort);die;
			// If the escort id is invalid that means that it is hacking attempt
			if ( ! $escort ) {
				die;
			}
			
			$data['to_addr'] = $escort->email;
			$data['to_name'] = $escort->showname;
			$data['escort_showname'] = $escort->showname;
			$email_tpl = 'escort_feedback';
		}
		// Else get administrative email from configuration file
		else {
			$data['to_addr'] = $site_emails[$data['to']];
			$email_tpl = 'feedback_template';
		}
		
		// In order when the form was posted validate fields
		if ( $this->_request->isPost() ) {
			$validator = new Cubix_Validator();

			if ( !$blackListModel->checkFeedback($data) ){
                $blackListModel->mergeFeedbackData($data);
                //$validator->setError('f_message', 'Do Not repeat The Same Message' /*'Email is required'*/);
            }


			if ( ! strlen($data['email']) ) {
				$validator->setError('f_email', '' /*'Email is required'*/);
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('f_email', '' /*'Wrong email format'*/);
			}elseif( $blackListModel->checkEmail($data['email']) ){
                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_EMAIL;
                $data['status'] = FEEDBACK_STATUS_DISABLED;
                //$validator->setError('f_email', 'This email is blocked');
            }
			
			if ( ! strlen($data['message']) ) {
				$validator->setError('f_message','' /*'Please type the message you want to send'*/);
			}
			/*elseif($blackListModel->checkWords($data['message'])){
                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_WORD;
				$data['status'] = FEEDBACK_STATUS_DISABLED;
                $validator->setError('message', 'You can`t use words "'.$blackListModel->getWords() .'"');
            }*/

            if( isset($data['flag']) && ($data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_EMAIL || $data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_WORD) ){
                $feedback->addFeedback($data);
            }

			/*$captcha = Cubix_Captcha::verify($this->_request->recaptcha_response_field);

			$captcha_errors = array(
				'invalid-site-public-key' => 'We weren\'t able to verify the public key.',
				'invalid-site-private-key' => 'We weren\'t able to verify the private key.',
				'invalid-request-cookie' => 'The challenge parameter of the verify script was incorrect.',
				'incorrect-captcha-sol' => 'The CAPTCHA solution was incorrect.',
				'verify-params-incorrect' => 'The parameters to /verify were incorrect, make sure you are passing all the required parameters.',
				'invalid-referrer' => 'reCAPTCHA API keys are tied to a specific domain name for security reasons.',
				'recaptcha-not-reachable' => 'reCAPTCHA never returns this error code. A plugin should manually return this code in the unlikely event that it is unable to contact the reCAPTCHA verify server.'
			);*/

			//var_dump($captcha);
			
			if ( ! strlen($data['captcha']) ) {
				$validator->setError('captcha', '');//Captcha is required
				}
			else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', '');//Captcha is invalid
				}
			}		
			
			
			$result = $validator->getStatus();
			
			if ( ! $validator->isValid() ) {
				// If the request was ajax, send the validation result as json
				if ( $is_ajax ) {
					echo json_encode($result);
					die;
				}
				// Otherwise, render the phtml and show errors in it
				else {
					$this->view->errors = $result['msgs'];
					return;
				}
			}
			else
			{
				$sess = new Zend_Session_Namespace('feedback'); 
				
				if ($data['rand'] != $sess->rand)
				{
					echo json_encode(array('status' => 'success', 'msg' => '<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>'));
					die;
				}
				
				
				if( isset($data['flag']) && ($data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_EMAIL) ) { // IF blocked by email imitate as sent feedback
					echo json_encode(array('status' => 'success', 'msg' => '<p style="padding:0 10px; margin-top: 50px; width: 330px; font-size: 14px"><span class="strong" style="color: #000">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>'));
					die;
				}
			}

			if ( $data['about'] ) {
				$about_escort = $model->get($data['about']);
			}
			if ( ! $about_escort ) $about_escort = (object) array('id' => '', 'showname' => '');

			$data['application_id'] = Cubix_Application::getId();
			
			if ($config['approvation'] == 1)
			{
				$data['flag'] = FEEDBACK_FLAG_NOT_SEND;
				$data['status'] = FEEDBACK_STATUS_NOT_APPROVED;
			}
			else
			{
				$data['flag'] = FEEDBACK_FLAG_SENT;
				$data['status'] = FEEDBACK_STATUS_APPROVED;
				
				// Set the template parameters and send it to support or to an escort
				$tpl_data = array(
					'name' => $data['name'],
					'email' => $data['email'],
					'to_addr' => $data['to_addr'],
					'message' => $data['message'],
					'escort_showname' => $escort->showname,
					'escort_id' => $escort->id
				);

				if ( isset($data['username']) ) {
					$tpl_data['username'] = $data['username'];
				}
				
				Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? /*$data['email']*/null : null);

				$config = Zend_Registry::get('newsman_config');

				if ($config)
				{
					$list_id = $config['list_id'];
					$segment = $config['member'];
					$nm = new Cubix_Newsman($_SERVER["REMOTE_ADDR"]);

					$subscriber_id = $nm->subscribe($list_id, $data['email'],$data['name']);
					$is_added = $nm->bindToSegment($subscriber_id, $segment);
				}
			}
			
			$feedback->addFeedback($data);
			$sess->unsetAll();
			
			if ( $is_ajax ) {
				if ( isset($escort) ) {
					if ( $is_premium ) {
						$result['msg'] = '
							<p style="padding:0 10px; margin-top: 50px; width: 330px; font-size: 14px"><span class="strong" style="color: #FFF">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>
						';
					}
					else {
						$result['msg'] = '
							<p style="padding:0 10px; margin-top: 50px; width: 330px; font-size: 14px"><span class="strong" style="color: #000">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>
						';
					}
				}
				else {
					$result['msg'] = '
						<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>
					';
				}
				//<a href="#" onclick="return Cubix.Feedback.Show(this.getParent(\'.cbox-small-ii\'))">Click here</a> to hide this message</strong>
				echo json_encode($result);
				die;
			}
			else {
				$this->view->success = TRUE;
			}
		}
	}

    public function feedbackAdsAction()
    {
        $this->_request->setParam('no_tidy', true);

        $m_class = new Model_ClassifiedAds();

        // Fetch administrative emails from config
        $config = Zend_Registry::get('feedback_config');
        $site_emails = $config['emails'];

        $is_ajax = ! is_null($this->_getParam('ajax'));

        $feedback = new Model_Feedback();

        // If the request is ajax, disable the layout
        if ( $is_ajax ) {
            $this->view->layout()->disableLayout();
        }

        // Get data from request
        $data = new Cubix_Form_Data($this->getRequest());

        $fields = array(
            'to' => '',
            'about' => 'int',
            'name' => 'notags|special',
            'email' => '',
            'message' => 'notags|special',
            'captcha' => '',
            'rand' => ''
        );
        $data->setFields($fields);
        $data = $data->getData();

        $blackListModel = new Model_BlacklistedWords();
        $this->view->data = $data;

        // If the to field is invalid, that means that user has typed by
        // hand, so it's like a hacking attempt, simple die, without explanation
        if ( ! is_numeric($data['to']) && ! in_array($data['to'], array_keys($site_emails)) ) {
            die;
        }
        // If escort id was specified, fetch it's email and showname,
        // construct the result email template and send it to escort
        if ( is_numeric($data['to']) ) {

            $ad = $m_class->get($data['to']);
            //var_dump($escort);die;
            // If the escort id is invalid that means that it is hacking attempt
            if ( ! $ad ) {
                die;
            }

            $data['to_addr'] = $ad->email;
            $data['to_name'] = $ad->title;
            $data['escort_showname'] = $ad->title;
            $email_tpl = 'escort_feedback_ads';
        }


        // In order when the form was posted validate fields
        if ( $this->_request->isPost() ) {
            $validator = new Cubix_Validator();

            if ( ! $blackListModel->checkFeedback($data) ){
                $blackListModel->mergeFeedbackData($data);
                // $validator->setError('f_message', 'Do Not repeat The Same Message' /*'Email is required'*/);
            }

            if ( ! strlen($data['email']) ) {
                $validator->setError('f_email', '' /*'Email is required'*/);
            }
            elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
                $validator->setError('f_email', '' /*'Wrong email format'*/);
            }elseif( $blackListModel->checkEmail($data['email']) ){
                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_EMAIL;
                $data['status'] = FEEDBACK_STATUS_DISABLED;
                //$validator->setError('email', 'This email is blocked');
            }

            if ( ! strlen($data['message']) ) {
                $validator->setError('f_message','' /*'Please type the message you want to send'*/);
            }
            elseif($blackListModel->checkWords($data['message'])) {
                $data['flag'] = FEEDBACK_FLAG_BLOCKED_BY_WORD;
                $data['status'] = FEEDBACK_STATUS_DISABLED;
                $validator->setError('f_message', 'You can`t use words "'.$blackListModel->getWords() .'"');
            }

            if( isset($data['flag']) && ($data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_EMAIL || $data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_WORD) ){
                $data['application_id'] = Cubix_Application::getId();
            }

            if ( ! strlen($data['captcha']) ) {
                $validator->setError('captcha', '');//Captcha is required
            }
            else {
                $session = new Zend_Session_Namespace('captcha');
                $orig_captcha = $session->captcha;

                if ( strtolower($data['captcha']) != $orig_captcha ) {
                    $validator->setError('captcha', '');//Captcha is invalid
                }
            }

            $result = $validator->getStatus();

            if ( ! $validator->isValid() ) {
                // If the request was ajax, send the validation result as json
                if ( $is_ajax ) {
                    echo json_encode($result);
                    die;
                }
                // Otherwise, render the phtml and show errors in it
                else {
                    $this->view->errors = $result['msgs'];
                    return;
                }
            }
            else
            {
                $sess = new Zend_Session_Namespace('feedback');

                if ($data['rand'] != $sess->rand)
                {
                    echo json_encode(array('status' => 'success', 'msg' => '<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>'));
                    die;
                }

                if( isset($data['flag']) && ($data['flag'] == FEEDBACK_FLAG_BLOCKED_BY_EMAIL) ) { // IF blocked by email imitate as sent feedback
                    echo json_encode(array('status' => 'success', 'msg' => '<p style="padding:0 10px; margin-top: 50px; width: 330px; font-size: 14px"><span class="strong" style="color: #000">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>'));
                    die;
                }
            }


            $tpl_data = array(
                'name' => $data['name'],
                'email' => $data['email'],
                'to_addr' => $data['to_addr'],
                'message' => $data['message'],
                'escort_showname' => $ad->title,
                'escort_id' => $ad->id
            );

            if ( isset($data['username']) ) {
                $tpl_data['username'] = $data['username'];
            }

            Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($ad) ? $data['email'] : null);

            $sess->unsetAll();

            if ( $is_ajax ) {

                $result['msg'] = '
					<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>
				';

                echo json_encode($result);
                die;
            }
            else {
                $this->view->success = TRUE;
            }
        }
    }

	public function contactAction()
	{
		$this->_request->setParam('no_tidy', true);
		$this->view->layout()->setLayout('main-simple');
		$client = new Cubix_Api_XmlRpc_Client();
		$is_ajax = ! is_null($this->_getParam('ajax'));

		// If the request is ajax, disable the layout
		if ( $is_ajax ) {
			$this->view->layout()->disableLayout();
		}
		
		//$this->_request->setParam('to', 'support');
		if (is_null($this->_getParam('to'))){
			$sales = array(
				0 => array('name' => "Laura",
						'phone' => "+44 20 3239 0564",
						'skype' =>  "laura_escortforum",
						'whatsapp' => "+37254263987, +37259637510",
						'email' => "laura@escortforum.com",
						'langs' => "Italian, English, Spanish"
						),
				1 => array('name' => "Angelo",
						'phone' => "+41 33 533 34 22",
						'skype' =>  "angelo_escortforum",
						'email' => "angelo@escortforum.com",
						'whatsapp' => "+41798070634",
						'langs' => "Italian, English, French, Spanish, Portuguese",
						),
				2 => array('name' => "Tony",
						'phone ' => "+41 44 586 98 50",
						//'phone' => "+44 20 719 33 358", removed for https://sceonteam.atlassian.net/browse/EF-208
						'skype' =>  "tony_escortforum",
						'email' => "tony@escortforum.com",
						'whatsapp' => "+37257669019",
						),
				3 => array('name' => "Francesco",
						'phone' => "+41 44 586 88 59",
						'skype' =>  "francesco_escortforum",
						'email' => "francesco@escortforum.com",
						'whatsapp' => '+41762031758',
						'langs' => "Italian, English, Hungarian"
						),
				4 => array('name' => "Sandra",
						'phone' => "+41 44 586 88 46",
						'skype' =>  "sandra_escortforum",
						'email' => "sandra@escortforum.com",
                        'whatsapp' => "+41798071087",
						'langs' => "Italian, English, Hungarian, Greek"
						),
				5 => array('name' => "Marco",
						'phone' => "+41 44 586 88 41",
						'skype' =>  "marco_escortforum",
						'email' => "marco@escortforum.com",
						),
				6 => array('name' => "Alessandro",
						'phone' => "+41 44 586 68 84",
						'skype' =>  "alessandro.escortforum",
						'email' => "alessandro@escortforum.net",
						'whatsapp' => "+41798071091",
						'langs' => "Italian, English, Romanian"
						),
			);
			shuffle($sales);
			
			$sales_statuses = $client->call('Users.getSalesSkypeStatuses');
			foreach($sales as $key => $sale){
				$sales[$key]['skype_status'] = $sales_statuses[$sales[$key]['skype']]['skype_status'];
			}
			
			/*foreach($sales as $key => $sale){
				$sales[$key]['skype_status'] = Cubix_Utils::getSkypeStatus($sale['skype']);
			}*/

			$count = count($sales);
			$array_value = array();
			for($i = 0; $i < $count - 1; $i++){
				for($j = 0; $j < $count - 1; $j++){

					if($sales[$j]['skype_status'] > $sales[$j + 1]['skype_status']){
						$array_value = $sales[$j];
						$sales[$j] = $sales[$j + 1];
						$sales[$j + 1] = $array_value;
					}
				}
			}
			$this->view->sales = $sales;
		}
		else{
			$model = new Model_EscortsV2();
			
			$sales_emails = $client->call('Users.getSalesEmails');

			// Fetch administrative emails from config
			$config = Zend_Registry::get('feedback_config');
			$site_emails = $config['emails'];
			foreach($sales_emails as $sale){
				$site_emails[$sale['username']] = $sale['email'];
			}
			// If the to field is invalid, that means that user has typed by
			// hand, so it's like a hacking attempt, simple die, without explanation
			if ( ! is_numeric($this->_getParam('to')) && ! in_array($this->_getParam('to'), array_keys($site_emails)) ) {
				die('fuck');
			}
			// Get data from request
			$data = new Cubix_Form_Data($this->getRequest());
			$fields = array(
				'to' => '',
				'about' => 'int',
				'name' => 'notags|special',
				'email' => '',
				'phone' => '',
				'message' => 'notags|special',
				'captcha' => ''
			);
			$data->setFields($fields);
			$data = $data->getData();
			
			$data['to_addr'] = $site_emails[$data['to']];
			$email_tpl = 'feedback_template';
			$this->view->data = $data;
		}
		/*
		// If escort id was specified, fetch it's email and showname,
		// construct the result email template and send it to escort
		if ( is_numeric($data['to']) ) {
			$escort = $model->get($data['to']);
			$this->view->escort = $escort;
			//var_dump($escort);die;
			// If the escort id is invalid that means that it is hacking attempt
			if ( ! $escort ) {
				die;
			}

			$data['to_addr'] = $escort->email;
			$data['to_name'] = $escort->showname;
			$data['username'] = $escort->username;
			$email_tpl = 'escort_feedback';
		}
		// Else get administrative email from configuration file
		else {

				$data['to_addr'] = $site_emails[$data['to']];
				$email_tpl = 'feedback_template';
			}
		*/
		
				
		// In order when the form was posted validate fields
		if ( $this->_request->isPost() ) {
			
			$validator = new Cubix_Validator();
			
			if ( !is_numeric($data['to']) ) {
				if (isset($_COOKIE['message_is_sent']))
				{
					$validator->setError('message_is_sent', 'You have already sent a request please wait for response');
				}
			}

			if ( ! strlen($data['email']) ) {
				$validator->setError('email', 'Email is required');
			}
			elseif ( ! preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $data['email']) ) {
				$validator->setError('email', 'Wrong email format');
			}
			
			if ( ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone']) ) {
				$validator->setError('phone', 'Invalid phone number');
			}
			if ( ! strlen($data['message']) ) {
				$validator->setError('message', 'Please type the message you want to send');
			}

			if ( ! strlen($data['captcha']) ) {
				$validator->setError('captcha', 'Captcha is required');
			}
			else {
				$session = new Zend_Session_Namespace('captcha');
				$orig_captcha = $session->captcha;

				if ( strtolower($data['captcha']) != $orig_captcha ) {
					$validator->setError('captcha', 'Captcha is invalid');
				}
			}

			$result = $validator->getStatus();

			if ( ! $validator->isValid() ) {
				// If the request was ajax, send the validation result as json
				if ( $is_ajax ) {
					echo json_encode($result);
					die;
				}
				// Otherwise, render the phtml and show errors in it
				else {
					$this->view->errors = $result['msgs'];
					return;
				}
			}

			$sent_by = '';
			
			if ( $user = Model_Users::getCurrent() )
			{
				$sent_by = $user->user_type.' '.$user->username.' wrote a message';

			}

			if ( $data['about'] ) {
				$about_escort = $model->get($data['about']);
			}
			if ( ! $about_escort ) $about_escort = (object) array('id' => '', 'showname' => '');

			// Set the template parameters and send it to support or to an escort
			$tpl_data = array(
				'name' => $data['name'],
				'email' => $data['email'],
				'phone' => $data['phone'],
				'to_addr' => $data['to_addr'],
				'message' => $data['message'],
				'escort_showname' => $about_escort->showname,
				'escort_id' => $about_escort->id,
				'sent_by' => $sent_by
			);

			if ( isset($data['username']) ) {
				$tpl_data['username'] = $data['username'];
			}
			
			Cubix_Email::sendTemplate($email_tpl, $data['to_addr'], $tpl_data, isset($escort) ? $data['email'] : null);
			
			
			// contact me log
			Cubix_Api::getInstance()->call('contactLog', array($data['to_addr'], $user ? $user->id : null));
			//
			
			if ( $is_ajax ) {
				if ( isset($escort) ) {
					$result['msg'] = '
						<p style="padding:0 10px"><span class="strong" style="color: #060">' . __('message_sent_to_escort', array('showname' => $escort->showname)) . '</span></p>
					';
				}
				else {
					$result['msg'] = '
						<p style="padding:0 10px"><span class="strong" style="color: #060">Your feedback has been successfully submitted!</span></p>
					';
				}
				//<a href="#" onclick="return Cubix.Feedback.Show(this.getParent(\'.cbox-small-ii\'))">Click here</a> to hide this message</strong>
				echo json_encode($result);
				die;
			}
			else {
				setcookie("message_is_sent", 1, time() + 604800, "/"); // 604800 = 60*60*24*7 = 7 days
				$this->view->success = TRUE;
			}
		}
	}
	
	public function paymentFakeAction()
	{
		header('Location:/payment');
		exit;
		$this->view->layout()->disableLayout();		
	}


	public function captchaAction()
	{
		
		$this->_request->setParam('no_tidy', true);

		$font = 'css/trebuc.ttf';
		
		$charset = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';
		
		$code_length = 5;
		
		$height = 30;
		$width = 90;
		$code = '';
		
		for ( $i = 0; $i < $code_length; $i++ ) 
		{
			$code = $code . substr($charset, mt_rand(0, strlen($charset) - 1), 1);
		}
		
		$rgb[0] = array(204,0,0);
		$rgb[1] = array(34,136,0);
		$rgb[2] = array(51,102,204);
		$rgb[3] = array(141,214,210);
		$rgb[4] = array(214,141,205);
		$rgb[5] = array(100,138,204);
		
		$font_size = $height * 0.4;
		
		$bg = imagecreatefrompng('img/bg_captcha.png');
		$image = imagecreatetruecolor($width, $height);
		imagecopy($image, $bg, 0, 0, 0, 0, imagesx($bg), imagesy($bg));
		$background_color = imagecolorallocate($image, 255, 255, 255);
		$noise_color = imagecolorallocate($image, 20, 40, 100);
		
		for($i = 0; $i<$code_length; $i++)
		{
			$A[] = rand(-20, 20);
			$C[] = rand(0, 5);
			$text_color = imagecolorallocate($image, $rgb[$C[$i]][0], $rgb[$C[$i]][1], $rgb[$C[$i]][2]);
			imagettftext($image, $font_size, $A[$i], 7 + $i * 15, 20 + rand(-3, 3), $text_color, $font , $code[$i]);
		}
		
		$session = new Zend_Session_Namespace('captcha');
		$session->captcha = strtolower($code);
		
		header('Content-Type: image/png');
		imagepng($image);
		imagedestroy($image);
		
		die;
	}
	
	/*public function goAction()
	{
        $link = $_GET['link'];

        //insert in table for statistics
        $client = Cubix_Api_XmlRpc_Client::getInstance();
        $client->call('Escorts.insertStatisticsRow', array('id' => (int)$_GET['id'], 'user_type' => $_GET['user_type'], 'link' => $_GET['link']));
        //
        if ( ! preg_match('#^http(s)?://#', $link) ) $link = 'http://' . $link;

        header('Location: ' . $link);
        die;
	}*/

	public function robotsAction()
	{
		$this->_request->setParam('no_tidy', true);

		$this->view->layout()->disableLayout();
		$model = new Model_Robots();

		$robots = $model->get();

		header('Content-Type: text/plain; charset=UTF-8', true);
		echo $robots;
		die;
	}

	public function benchmarkAction()
	{
		$db = Zend_Registry::get('db');
		$results = $db->fetchAll('SELECT * FROM debug_benchmark');

		$this->view->results = $results;
	}

	public function listDateImgAction()
	{
		$date = $this->_getParam('date');
		$date = intval($date);
		$date = ( date('d.m.Y') == date('d.m.Y', $date) ? $this->view->t('today_new') : date('d.m.Y', $date) );

		$im = imagecreatetruecolor(54, 15);
		imagefill($im, 0, 0, imagecolorallocate($im, 210, 210, 210));
		imagecolortransparent($im, imagecolorallocate($im, 210, 210, 210));
		
		imagettftext($im, 8, 0, 0, 13, imagecolorallocate($im, 0, 0, 0), 'css/arial.ttf', $date);

		header('Content-Type: image/gif');
		imagegif($im);
		imagedestroy($im);
		die;
	}

	public function photoViewerAction()
	{
		if ( $user = Model_Users::getCurrent() ) {
			$member_data = $user->member_data;
		}

		$response = array('status' => 'success', 'msg' => '', 'data' => array());

		$model = new Model_EscortsV2();

		$showname = $this->_getParam('showname');
		$escort_id = $this->_getParam('escort_id');
		//$escort = $model->get($showname);
		$escort = $model->get($escort_id);

		if ( ! $escort ) {
			$response['status'] = 'error';
			$response['msg'] = 'Showname is required';
		}
		else {
			$photos = $escort->getPhotos(null);
			
			Model_EscortsV2::hit_gallery( $escort->id );

			foreach ( $photos as $i => $photo ) {
				$response['data'][$i] = array(
					'id' => $photo->id,
					'private' => ($photo->type == 3) && (! isset($member_data) || ! $member_data['is_premium']),
					'h' => $photo->getUrl('orig'),
					'b' => $photo->getUrl('w514'),
					's' => $photo->getUrl('sthumb')
				);
			}
		}

		$this->_helper->json($response);
	}

	public function chatAction()
	{
		$this->view->layout()->disableLayout();
	}
	
	public function premiumListXmlAction()
	{
		$escorts = Model_Escort_List::getMainPremiumSpot(7);
		
		$writer = new XMLWriter();
		
		$writer->openMemory();
		
		$writer->startDocument('1.0');
		
		$writer->setIndent(4); 
		
			$writer->startElement('escorts');
				if ( count($escorts) > 0 ) {
					foreach ( $escorts as $escort ) {
						 $writer->startElement('escort');
							$writer->writeElement('showname', $escort->showname);
							$writer->writeElement('url', APP_HTTP . "://www.escortforumit.xxx/accompagnatrici/" . $escort->showname);
							
							$available = '';
							if ( $escort->incall_type && $escort->outcall_type ) {
								$available = 'incall/outcall';
							}
							elseif ( $escort->incall_type && ! $escort->outcall_type ) {
								$available = 'incall';
							}
							elseif ( ! $escort->incall_type && $escort->outcall_type ) {
								$available = 'outcall';
							}
							
							$writer->writeElement('available', $available);
							
							$writer->writeElement('city', $escort->city);
						
							$writer->startElement('photos');
								$escort = new Model_EscortV2Item($escort);
								$writer->writeElement('photo', $escort->getMainPhoto()->getUrl('thumb'));
							$writer->endElement();
						 $writer->endElement();
					}
				}
			$writer->endElement();
		
		$writer->endDocument();
		
		echo $writer->flush(true); 
		
		die;
	}
	
	public function autologinChatAction()
	{		
		$user = Model_Users::getCurrent();
		
		if ( session_id() == $_REQUEST['uid'] && $user ) {
			$xml = '<login result="OK">';
		}
		else {
			$xml = '<login result="FAIL" error="error_authentication_failed">';
		}
		
		$xml .= '<userData>';
		$xml .= '<id>' . $user->id . '</id> ';
		$xml .= '<name><![CDATA[' . $user->username . ']]></name>';
		if ( $user->user_type == 'escort' || $user->user_type == 'agency' ) {
			$xml .= '  <gender>female</gender>';
		}
		else {
			$xml .= '  <gender>male</gender>';
		}
		$xml .= '<level>regular</level>';
		$xml .= '</userData>';
		$xml .= '<friends> </friends>';
		$xml .= '<blocks> </blocks>';
		$xml .= '</login>';
		
		echo $xml;
		
		die;
	}
	
	public function noChatAction()
	{
		$this->view->layout()->disableLayout();
	}

	public function sedcardTotalViewsAction(){
		$this->view->layout()->disableLayout();

		$cache = Zend_Registry::get('cache');

		$escort_id = (int)$this->_request->escort_id;

		if ( !$escort_id ){
			exit;
		}

		$cache_key = "sedcart_view_data_total_" . $escort_id . '_' . Cubix_Application::getId();


		$tmp_data = $cache->load(Model_EscortsV2::HITS_CACHE_KEY);
		$total_count = 0;
		if ( $tmp_data ){
			$total_count = (int)$tmp_data[$escort_id][1];
		}

		if ( (! $total = $cache->load($cache_key)) || $total_count < 2  ) {
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$data['escort_id'] = $escort_id;
			$total = $client->call('Escorts.getTotalViews', array($escort_id));
			$cache->save($total, $cache_key, array(), 600);
		}

		$this->view->escort_id = $escort_id;

		$this->view->totalViews = $total + $total_count;
		
		$e_model = new Model_EscortsV2();
		if ( ! $e_model->isNotNewEscort($escort_id) ) {
			$this->view->dont_show = true;
		}
	}
	
	public function jsInitAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$view = $this->view->act = $req->view;
		
		$req->setParam('lang_id', $req->lang);
		
		$lang = $this->view->lang = $req->lang;
		
		if ( $view == 'main-page' ) {
			$banners = $this->view->banners = Zend_Registry::get('banners');
		}
		else if ( $view == 'main-simple' ) {
			$this->view->is_logged_in = $req->is_logged_in;
		}
		else if ( $view == 'main' ) {
			
		}
		else if ( $view == 'private' ) {
			
		}
		else if ( $view == 'private-v2' ) {
			
		}
		else if ( $view == 'profile' ) {
			$this->view->escort_id = $req->escort_id;
			$this->view->has_no_review = $req->has_no_review;
			$this->view->is_member = $req->is_member;
			$this->view->is_escort = $req->is_escort;
		}
		else if ( $view == 'profile-v2' ) {
			$this->view->escort_id = $req->escort_id;
			$this->view->has_no_review = $req->has_no_review;
			$this->view->is_member = $req->is_member;
			$this->view->is_escort = $req->is_escort;
			
			$this->view->is_logged_in = $req->is_logged_in;
		}
		else if ( $view == 'cams' ) {
			$this->view->is_logged_in = $req->is_logged_in;
			$this->view->is_member = $req->is_member;
			$this->view->is_escort = $req->is_escort;
		}
		
		header('Content-type: text/javascript');
		/*header('Content-type: text/javascript');
		$expires = 60*60*24*14;
		header("Pragma: public");
		header("Cache-Control: maxage=" . $expires);
		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');*/
	}
	
	public function jsInitExactAction()
	{
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		
		$view = $this->view->act = $req->view;
		
		$req->setParam('lang_id', $req->lang);
		
		$lang = $this->view->lang = $req->lang;
		
		if ( $view == 'new-list' ) {

		}
		else if ( $view == 'list' ) {
			$this->view->escorts_url = $req->escorts_url;
			$this->view->filter_params = $req->filter_params;
			$this->view->is_upcomingtour = $req->is_upcomingtour;
			$this->view->is_tour = $req->is_tour;
		}
		else if ( $view == 'profile' ) {
			
		}
		else if ( $view == 'splash' ) {
			
		}
		else if ( $view == 'profile-v2' ) {
			
		}
		else if ( $view == 'comment-tabs' ) {
			
		}
		else if ( $view == 'comment-tabs-reversed' ) {
			
		}
		else if ( $view == 'google-maps-v2' ) {
			$this->view->address = $req->address;
		}
		else if ( $view == 'main' ) {
			$gender = $req->gender;
			$is_agency = $req->is_agency;
			
			$cache = Zend_Registry::get('cache');
			$cache_key = 'auto_comp_cities_' . Cubix_Application::getId() . '_' . $gender . '_' . $is_agency;
			$cities_data = $cache->load($cache_key);
			$this->view->data = $cities_data ? $cities_data : '""';
			}
		
		header('Content-type: text/javascript');
		/*$expires = 60*60*24*14;
		header("Pragma: public");
		header("Cache-Control: maxage=" . $expires);
		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');*/
	}
	
	public function oEAction()
	{
		$m = new Model_EscortsV2();
		
		$escorts = $m->getLoginEscorts();
		
		$arr = array();
		
		if ($escorts)
		{
			foreach ($escorts as $e)
			{
				$arr[$e->escort_id] = $e->showname;
			}
		}
		
		echo json_encode($arr);
		die;
	}
	
	public function ajaxPlayVideoAction()
	{
		$this->view->layout()->disableLayout();
		$escort_id = $this->_request->escort_id;
		
		$video_model = new Model_Video();
		$video = $video_model->getManifestData($escort_id);
		$dimensions = $video_model->dimensions;
		$configs = Zend_Registry::get('videos_config');
		$path = $configs['remote_url'].'escortforumit.xxx/'.$escort_id.'/'.$video->video;
		$qualities = array();
		
		foreach($dimensions as $dimension){
			if($video->height >= $dimension['height']){
				$width = $this->video->width / $this->video->height * $dimension['height'];
				$qualities[] = array(
					'file' => $path.'_'.$dimension['height'].'p.mp4',
					'label'=> $dimension['height'].'p'
				);
			}
		}
				
		die(json_encode($qualities));
		/*[{
        file: "/uploads/myVideo720mp4",
        label: "720p HD"
      },{
        file: "/uploads/myVideo360.mp4",
        label: "360p SD",
        "default": "true"
      },{
        file: "/uploads/myVideo180mp4",
        label: "180p Web"
      }]*/
		/*$blockModel = new Model_BlockedCountries();
		if ( $blockModel->checkIp($escort_id) ){
			$result = Cubix_I18n::translate('video_block_country_text');
			die($result);
		}*/
	}

	public function phoneImgAction()
	{
		$escort_id = (int) $this->_request->escort_id;
		$agency_id = (int) $this->_request->agency_id;
		$ad_id = (int) $this->_request->ad_id;
		$font_size = (int) $this->_request->fs;
		$width = (int) $this->_request->w;
		$height = (int) $this->_request->h;
		$v = (int) $this->_request->v;
		$color = $this->_request->c;
		$tr = (int) $this->_request->tr;
		$phone = $this->_request->p;

		$colors = explode(',', $color);		

		if ( ! strlen($phone) ) {
			if ( $escort_id ) {
				$model = new Model_EscortsV2();
				$cache_key = 'v2_' . $escort_id .  '_profile_' . Cubix_I18n::getLang();
				$cache_key =  preg_replace('#[^a-zA-Z0-9_]#', '_', $cache_key);
				$escort = $model->get($escort_id, $cache_key);
				$contacts = $escort->getContacts();
				$filtered_phone =  (isset($contacts->phone_country_id) && !$contacts->disable_phone_prefix) ? '+'.Model_Countries::getPhonePrefixById($contacts->phone_country_id) . $contacts->phone_number : $contacts->phone_number;
			} else if ( $agency_id ) {
				$m_agencies = new Model_Agencies();
				$agency = $m_agencies->getLocalById($agency_id);
				$filtered_phone =  (isset($agency->phone_country_id)) ? '+'.Model_Countries::getPhonePrefixById($agency->phone_country_id) . $agency->phone : $agency->phone;
			} else if ($ad_id) {
				$m_class = new Model_ClassifiedAds();
				$filtered_phone = $m_class->get($ad_id)->phone;
			}
		} else {
			$filtered_phone = $phone;
		}

		// Set the content-type
		header('Content-Type: image/png');

		// Create the image
		$im = imagecreatetruecolor($width, $height);

		// Create some colors
		$white = imagecolorallocate($im, 255, 255, 255);
		$grey = imagecolorallocate($im, 128, 128, 128);
		$black = imagecolorallocate($im, 0, 0, 0);

		array_unshift($colors, $im);
		
		$text_color = call_user_func_array('imagecolorallocate', $colors);

		if ( $tr ) {
			imagefilledrectangle($im, 0, 0, 399, 29, $black);
			imagecolortransparent($im, $black);
		} else {
			imagefilledrectangle($im, 0, 0, 399, 29, $white);
		}
	
		// Replace path by your own font path
		$font = 'css/arial.ttf';

		// Add some shadow to the text
		//imagettftext($im, $font_size, 0, 1, $v + 1, $grey, $font, $filtered_phone);

		// Add the text
		imagettftext($im, $font_size, 0, 0, $v, $text_color, $font, $filtered_phone);

		// Using imagepng() results in clearer text compared with imagejpeg()
		imagepng($im);
		imagedestroy($im);
		die;
	}
	
	public function popunderAction()
	{
		$this->view->layout()->disableLayout();
		$m = new Model_EscortsV2();
		$this->view->escorts = $m->getPopunderEscorts();
	}	
	
	/*public function testAction()
	{
		$system = Zend_Registry::get('system_config');
		$sid = '79c33f1ed7506f191e1c941b74999441';
		echo urlencode(Cubix_Utils::crypter($sid, $system['cams']['key'] , $system['cams']['iv'], 'e'));die;
	}*/		
	public function geotargetedAction()
	{
		$geo_ip = Cubix_IP2Location::getInstance();
		$records = $geo_ip->lookup(Cubix_Geoip::getIP(), array(Cubix_IP2Location::COUNTRY_CODE, Cubix_IP2Location::CITY_NAME));
		
		$httpReferer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
		//$records["countryCode"] = 'IT';
		//$records["cityName"] = 'Agrigento';
		$data = array(
			'country_iso' => $records["countryCode"],
			'city' => $records["cityName"],
			'referer' => $httpReferer
		);
		$status = 0;
		$model = new Model_Geotargeted();
		
		if($records["countryCode"] == "IT"){
			$city_model = new Model_Cities();
			if($records["cityName"] == 'Torino'){
				$records["cityName"] = 'Turin';
			}
			$city = $city_model->getByEnTitle($records["cityName"]);
			$count = Model_EscortsV2::getEscortCountBycityId($city->id);
			if($count >= 5 ){
				$status = 2;
			}
			elseif($count > 0){
				$status = 1;
			}
		}
		$data['status'] = $status;
		$model->save($data);
		if($status == 2){
			$this->_redirect($this->view->getLink('escorts', array('city' => 'it_' . $city->slug)));
		}
		else{
			$this->_redirect('/');
		}
		die;
	}
	
	public function rabbitAction()
	{
		$start_time = microtime(true);
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
		$connection = new Cubix_PhpAmqpLib_Connection_AMQPStreamConnection('bear.rmq.cloudamqp.com', 5672, 'rcgaoxjv', 'NPszYKCFWRyKFSqa-Z8jR-ckXYBNd97B', 'rcgaoxjv');
		$channel = $connection->channel();
		$que_name = 'hello_test';
		$channel->queue_declare($que_name, false, true, false, false);
		for($i = 1; $i <= 5000 ; $i++){
			$msg = new Cubix_PhpAmqpLib_Message_AMQPMessage('Hello World! N '. $i);

			$channel->basic_publish($msg, '', $que_name);
			//echo " [x] Sent 'Hello World!'\n";
		}
		$channel->close();
		$connection->close();
		$end_time = microtime(true);
        // Just print the execution time
		print('Execution took ' . round($end_time - $start_time, 4) . ' seconds');
		die('Done!');
	}	

	public function addVideoBookingAction()
	{
		$client = new Cubix_Api_XmlRpc_Client();
		$users = $client->call('Users.getEscortDataTemp');
		//var_dump($users);die;
		define('IS_DEBUG', false);
		
		foreach($users as $user){
			$model = new Model_Api_Cams($user['id']);
			$data = $model->prepareEscortDataForApiTemp($user);
			
			$ret = $model->escortUpdate($data);
			if($ret['status'] == 404){
				$ret = $model->escortCreate($data);
			}
			echo "\n\n";
			echo $user['escort_id'].'-------'.$user['email'];
			var_dump($ret);
		}
		
		die('success');
	}
	
	public function testGalleryCacheAction()
	{
		$cache = Zend_Registry::get('cache');
		$cache_name = Model_EscortsV2::HITS_GALLERY_CACHE_KEY;
		$data = $cache->load($cache_name);
		var_dump($data);
		die();
	}
	
	public function testHitsCacheAction()
	{
		$cache = Zend_Registry::get('cache');
		$cache_name = Model_EscortsV2::HITS_CACHE_KEY;
		$data = $cache->load($cache_name);
		var_dump($data);
		die();
	}
	
	/*public function removeHitsCacheAction()
	{
		$cache = Zend_Registry::get('cache');
		$cache->save('empty', Model_EscortsV2::HITS_CACHE_KEY);
		die('cache removed');
	}*/		
	
	/*public function netbanxTestAction()
	{
		$nbx = new Model_NetbanxAPITest();
		$order = $nbx->create_order(
			array(
				'total_amount'	=> 1000,
				'currency_code'	=> 'EUR',
				'merchant_ref_num'	=> 'test-' . time(),
				'profile' => array(
					'merchantCustomerId' => 'ef-test',
				),
				'callback'	=> array( 
					array(
						'format'		=> 'json',
						'rel'			=> 'on_success',
						'retries'		=> 6,
						'returnKeys'	=> array('id', 'transaction.amount', 'transaction.authType', 'transaction.status', 'transaction.currencyCode', 'transaction.merchantRefNum', 'transaction.confirmationNumber', 
							'transaction.card.brand', 'transaction.card.country', 'transaction.card.expiry', 'transaction.card.lastDigits'
						),
						'synchronous'	=> true,
						'uri'			=> 'http://backend.escortforumit.xxx/billing/online-billing/netbanx-callback-sc'
					)
				),
				'redirect'	=> array(
					array(
						'rel'	=> 'on_success',
						'returnKeys'	=> array('id'),
						'uri'	=> 'http://www.escortforumit.xxx/test'
					)
				),
				'link'		=> array(
					array(
						'rel'	=> 'cancel_url',
						'uri'	=> 'http://www.escortforumit.xxx/online-billing-v2'
					),
					array(
						'rel'	=> 'return_url',
						'uri'	=> 'http://www.escortforumit.xxx/online-billing-v2'
					)
				)
			)
		);
		var_dump($order->link[0]->uri);die;
	}*/
}