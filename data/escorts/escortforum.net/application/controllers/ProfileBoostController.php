<?php

class ProfileBoostController extends Zend_Controller_Action
{
	public static $linkHelper;
	
	protected $_session;
	
	public function init()
	{	
		$this->_session = new Zend_Session_Namespace('ef-profile-boost');
		$this->_session->setExpirationSeconds(60*60);
		
		self::$linkHelper = $this->view->getHelper('GetLink'); 
		$this->view->layout()->setLayout('private-v2');
		
		$anonym = array('epg-response');
		
		$this->view->user = $this->user = Model_Users::getCurrent();
		
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}

		############### booster disabled and redirected to bumps
		if ( $this->user->isAgency() ) {
			$this->_redirect($this->view->getLink('ob-profile-bump-buy-agency'));
			die;
		} else {
			$this->_redirect($this->view->getLink('ob-profile-bump-buy'));
			die;
		}
		
		if ( ! in_array($this->_request->getActionName(), $anonym) && ! $this->user->isAgency() && ! $this->user->isEscort() ) {
			$this->_redirect($this->view->getLink('private-v2'));
		}
		
		$this->view->headTitle('Private Area', 'PREPEND');

		$this->client = new Cubix_Api_XmlRpc_Client();
	}
	
	public function indexAction()
	{
		if ( $this->user->isAgency() ) {
			$this->_redirect(self::$linkHelper->getLink('ob-profile-boost-select-escort'));
		} else {
			$this->_redirect(self::$linkHelper->getLink('ob-profile-boost-select-city'));
		}
	}
	
	public function selectEscortAction()
	{	
		if ( ! $this->user->isAgency() ) {
			$this->_redirect(self::$linkHelper->getLink('ob-profile-boost-select-city'));
		}
		
		if ( ! $this->_request->isPost() ) {
			
			$agency_id = $this->user->agency_data['agency_id'];
			
			$sort = $this->_request->sort ? $this->_request->sort : 'alphabetically';
			
			$this->view->sort = $sort;
			
			switch($sort){
				case 'alphabetically':
					$sort = 'e.showname ASC';
					BREAK;
				case 'id':
					$sort = 'e.id ASC';
					BREAK;
				case 'newest':
					$sort = 'e.date_registered DESC';
					BREAK;
			}
			
			
			$escorts = $this->client->call('Agencies.getActiveEscortsForProfileBoost', array($agency_id, $sort));
			
			foreach($escorts as $i => $escort) {
				$escort = new Model_EscortV2Item($escort);
				$escorts[$i]['photo'] = $escort->getMainPhoto();
			}
			
			$this->view->escorts = $escorts;
		} else {
			if ( $this->_request->escort_id ) {
				//$m_escorts = new Model_EscortsV2();
				//$this->_session->escort = $m_escorts->get((int)$this->_request->escort_id);
				$this->_session->escort_id = (int)$this->_request->escort_id;
				$this->_redirect(self::$linkHelper->getLink('ob-profile-boost-select-city'));
			}
		}
			
	}
	
	public function selectCityAction()
	{
		if ( $this->user->isAgency() ) {
			if ( ! $this->_session->escort_id ) {
				$this->_redirect(self::$linkHelper->getLink('ob-profile-boost-select-escort'));
			}
		} else {
			$escort_id = $this->user->escort_data['escort_id'];
			//$m_escorts = new Model_EscortsV2();
			//$escort = $m_escorts->get($escort_id);
			//$this->_session->escort = $escort;
			$this->_session->escort_id = $escort_id;//$escort->id;
		}
		
		$cities = new Cubix_Geography_Cities();
		$country_id = Cubix_Application::getById()->country_id;
		$this->view->cities = $cities = $cities->ajaxGetAll(null, $country_id);
		
		$this->view->active_package = $active_package = $this->client->call('OnlineBillingV2.getActivePackageForProfileBoost', array($this->_session->escort_id));
		
		if ( $this->_request->isPost() ) {
			if ( ! strlen($this->_request->city_id) ) {
				$this->view->city_error = true;
			} else {
				$this->_session->city = $this->_request->city_id;
				$this->_session->dates = array();
				$this->_redirect(self::$linkHelper->getLink('ob-profile-boost-select-hour'));
			}
		}
	}
	
	public function selectHourAction()
	{
		
		if ( !$this->_session->escort_id || ! $this->_session->city ) {
			$this->_redirect(self::$linkHelper->getLink('ob-profile-boost-select-escort'));
		}
		$this->view->escort_packages = $escort_packages = $this->client->call('OnlineBillingV2.getEscortPackagesForProfileBoost', array($this->_session->escort_id));
		$this->view->city = $this->_session->city;
		
		$booked_hours = $this->client->call('OnlineBillingV2.getBookedProfileBoosts', array($this->_session->city, $this->_session->escort_id));
		
		$this->view->booked_hours = $booked_hours['all'];
		$this->view->boost_profile_price = $this->getProfileBoostPrice();
		
		if ( $this->_session->dates ) {
			$this->view->boost_profile_dates = $this->_session->dates;
		}
		
		if ( $this->_request->isPost() ) {
			$this->view->boost_profile_dates = $boost_profile_dates = $this->_request->boost_profile_dates;
			$this->view->order_package_id = $this->_request->order_package_id;
			$errors = $this->validateDates($boost_profile_dates, $escort_packages, $booked_hours);
			
			if ( count($errors) ) {
				$this->view->errors = $errors;
			} else {
				$this->_session->dates = $boost_profile_dates;
				$this->_session->order_package_id = $this->_request->order_package_id;
				$this->_redirect(self::$linkHelper->getLink('ob-profile-boost-confirmation'));
			}
		}
	}
	
	public function confirmationAction()
	{
		$request = $this->_request;
		if ( ! $this->_session->escort_id || ! $this->_session->city || ! count($this->_session->dates) ) {
			$this->_redirect(self::$linkHelper->getLink('ob-profile-boost-select-escort'));
		}
		
		$this->view->boost_profile_dates = $this->_session->dates;
		$this->view->boost_profile_price = $this->getProfileBoostPrice();
		$this->view->city = $this->_session->city;
//		$this->view->mmg_cards = $this->client->call('OnlineBillingV2.getCards', array($this->user->id, 'mmgbill'));
		$this->view->payment_gateway = $this->_request->payment_gateway ? $this->_request->payment_gateway : "twispay";
		$this->view->user_id = $this->user->id;
		
		if ( $this->_request->isPost() ) {
			$this->view->card = $this->_request->card;
			$escort_packages = $this->client->call('OnlineBillingV2.getEscortPackagesForProfileBoost', array($this->_session->escort_id));
			$booked_hours = $this->client->call('OnlineBillingV2.getBookedProfileBoosts', array($this->_session->city, $this->_session->escort_id));
			
			$this->view->boost_profile_dates = $boost_profile_dates = $this->_request->boost_profile_dates;
			$errors = $this->validateDates($boost_profile_dates, $escort_packages, $booked_hours);
			
			if ( count($errors) ) {
				$this->view->errors = $errors;
			} else {
				$this->_session->dates = $boost_profile_dates;
				
				$formated_dates = array();
				foreach($boost_profile_dates as $d) {
					list($date, $hour) = explode(':', $d);
					$formated_dates[] = array(
						'date'	=> $date,
						'hour_from'	=> $hour,
						'hour_to'	=> $hour + 1
					);
				}
				
				$city_id = $this->_session->city == 'index' ? null : $this->_session->city;
				$book_id = $this->client->call('OnlineBillingV2.bookProfileBoost', array($this->_session->escort_id, $city_id, $formated_dates, Model_Users::getCurrent()->clientID,Cubix_Geoip::getIP()));
				
				if ( $book_id ) {
					$amount = count($formated_dates) * $this->getProfileBoostPrice();
					$this->_session->unsetAll();
					try {
						if ( $request->payment_gateway == 'mmgbill' ) {
							$mmgBill = new Model_MmgBillAPIV2();
							$book_id = str_replace(':', 'x', $book_id);
							
							if ( $request->card && strlen($request->card) ) {
								$result = $mmgBill->charge($amount, 'PBZ' . $book_id, $request->card);
								if ( $result['success'] ) {
									$ch = curl_init(APP_HTTP. '://backend.escortforumit.xxx/billing/online-billing/mmg-bill-callback');
									$data = array(
										'txn_status' => 'APPROVED',
										'is_oneclick' => 1,
										'ti_mmg' => $result['ti_mmg'],
										'ti' => 'PBZ' . $book_id,
										'ia' => $amount * 100,
										'prev_ti_mmg'	=> $request->card
									);
									
									curl_setopt($ch, CURLOPT_POST, 1);
									curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
									$response = curl_exec($ch);
									curl_close($ch);

									$this->_redirect($this->view->getLink('ob-successful'));
								} else {
									$this->_redirect($this->view->getLink('ob-unsuccessful'));
								}

							}

							$hosted_url = $mmgBill->getHostedPageUrl($amount, 'PBZ' . $book_id, $request->save_info, APP_HTTP. '://www.escortforumit.xxx' . $this->view->getLink('ob-mmg-postback'));
							
							$this->_redirect($hosted_url);
						}
						elseif( $this->_request->payment_gateway == 'ecardon')
						{
							$TokenParams = array(
								'amount' => $amount,
								'descriptor' => 'pb-' . $book_id,
								'merchantTransactionId' => 'pb-' . $book_id
							);

							$ecardon_payment = new  Model_EcardonGateway($TokenParams);

							$token_data = $ecardon_payment->checkout();
							$token_data_dec = json_decode($token_data);

							if(isset($token_data_dec->result) && $token_data_dec->result->code == '000.200.100'){
								$token =  preg_replace('#[^a-zA-Z0-9.\-]#', '', $token_data_dec->id);
								$this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id, 'ecardon'));
								$this->view->checkout_id = $token_data_dec->id;
							}
							else{
								die(json_encode(array('status' => 'error' )));
							}
						}
						elseif( $this->_request->payment_gateway == '2000charge')
						{
							$first_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->username)); 
							$last_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->chat_info['nickName']));
							
							if( empty($first_name)){
								$first_name = 'NAME';
							}

							if(empty($last_name)){
								$last_name = 'LASTNAME';
							}
							
							$reference = 'pb-' . $book_id;
							
							$customer = new Cubix_2000charge_Model_Customer();
							$customer->setEmail($this->user->email);
							$customer->setCountry("IT");
							$customer->setFirstName($first_name);
							$customer->setLastName($last_name);

							$payment = new Cubix_2000charge_Model_Payment();
							$payment->setPaymentOption("paysafe");
							$payment->setHolder($first_name.' '.$last_name);

							$transaction = new Cubix_2000charge_Model_Transaction();
							$transaction->setCustomer($customer);
							$transaction->setPayment($payment);
							$transaction->setAmount($amount * 100);
							$transaction->setCurrency("EUR");
							$transaction->setIPAddress(Cubix_Geoip::getIP());
							$transaction->setMerchantTransactionId($reference);

							$host = 'https://' . $_SERVER['SERVER_NAME'];
							$redirectUrls = new Cubix_2000charge_Model_RedirectUrls();
							$redirectUrls->setReturnUrl($host . $this->view->getLink('ob-successful'));
							$redirectUrls->setCancelUrl($host .$this->view->getLink('ob-unsuccessful'));
							$transaction->setRedirectUrls($redirectUrls);

							$res = Cubix_2000charge_Transaction::post($transaction);
							$this->client->call('OnlineBillingV2.storeToken', array($res->id, $this->user->id, '2000charge'));
							$this->_redirect($res->redirectUrl);
						} 
						elseif( $this->_request->payment_gateway == 'twispay')
						{
                            $paymentConfigs = Zend_Registry::get('payment_config');
                            $twispayConfigs = $paymentConfigs['twispay'];

                            /*if (in_array($this->user->id, [345573])) {
                                $amount = 1;
                            }*/

                            $reference = 'pb-' . $book_id;
                            $data = array(
                                'siteId' => intval($twispayConfigs['siteid']),
                                'cardTransactionMode' => 'authAndCapture',
                                'backUrl' => APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/online-billing-v2/twispay-response',
                                'invoiceEmail' => '',
                                'customer' => [
                                    'identifier' => 'user-' . $this->user->id,
                                    'firstName' => $this->user->username,
                                    'username' => $this->user->username,
                                    'email' => $this->user->email,
                                ],
                                'order' => [
                                    'orderId' => $reference,
                                    'type' => 'purchase',
                                    'amount' => $amount,
                                    'currency' => $twispayConfigs['currency'],
                                    'description' => 'Profile Boost desktop EscortForum',
                                ]
                            );

                            // Store the gateway token for backend to get details about the transaction
                            // using this information
                            $token = json_encode(['externalOrderId' => $data['order']['orderId']]);
                            $isOrderCreated = $this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id, 'twispay'));

                            if (!$isOrderCreated) {
                                die(json_encode([
                                    'status' => 'error',
                                    'error'  => 'Twispay is not available at the moment',
                                ]));
                            }

                            $base64JsonRequest = Cubix_Twispay_TwispayApi::getBase64JsonRequest($data);
                            $base64Checksum = Cubix_Twispay_TwispayApi::getBase64Checksum($data, $twispayConfigs['key']);

                            try {
                                $paymentFrom = "
                                <form id=\"twispay-payment-form\" action='{$twispayConfigs['url']}' method='post' accept-charset='UTF-8'>
                                    <input type='hidden' name='jsonRequest' value = \"{$base64JsonRequest}\" >
                                    <input type='hidden' name='checksum' value = \"{$base64Checksum}\" >
                                    <input type = \"submit\" value = \"Pay\" >
                                </form > ";

                            } catch(Exception $e) {
                                $message = $e->getMessage();
                                file_put_contents('/tmp/twispay_error.log', "\n" . 'UserId: ' . $this->user->id . "\n" . var_export($message, true), FILE_APPEND);

                                die(json_encode(array('status' => 'success', 'url' => APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-unsuccessful'))));
                            }
                            die(json_encode(array('status' => 'success', 'form' => $paymentFrom), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES ));
						}
						else {
							try {
								$reference = 'PB-' . $book_id;
								$nbx = new Model_NetbanxAPI();

								$order_data = array(
									'total_amount'	=> $amount * 100,
									'currency_code'	=> 'EUR',
									'merchant_ref_num'	=> $reference,
									'locale' => 'en_US',
									'profile'	=> array(
									),
									'callback'	=> array( 
										array(
											'format'		=> 'json',
											'rel'			=> 'on_success',
											'retries'		=> 6,
											'returnKeys'	=> array('id', 'transaction.amount', 'transaction.authType', 'transaction.status', 'transaction.currencyCode', 'transaction.merchantRefNum', 'transaction.confirmationNumber', 
												'transaction.card.brand', 'transaction.card.country', 'transaction.card.expiry', 'transaction.card.lastDigits'
											),
											'synchronous'	=> true,
											'uri'			=> APP_HTTP. '://backend.escortforumit.xxx/billing/online-billing/netbanx-callback-boost-profile'
										)
									),
									'redirect'	=> array(
										array(
											'rel'	=> 'on_success',
											'returnKeys'	=> array('id'),
											'uri'	=> APP_HTTP. '://www.escortforumit.xxx' . $this->view->getLink('ob-successful')
										)
									),
									'link'		=> array(
										array(
											'rel'	=> 'cancel_url',
											'uri'	=> APP_HTTP. '://www.escortforumit.xxx' . $this->view->getLink('ob-profile-boost')
										),
										array(
											'rel'	=> 'return_url',
											'uri'	=> APP_HTTP. '://www.escortforumit.xxx' . $this->view->getLink('ob-profile-boost')
										)
									)
								);

								$profile_id = $this->client->call('OnlineBillingV2.getNetbanxProfileId', array($this->user->id));

								if ( $profile_id ) {
									$order_data['profile']['id'] = $profile_id;
								} else {
									$order_data['profile']['merchantCustomerId'] = 'ef' . $this->user->id;
								}

								$order = $nbx->create_order($order_data);
							} catch(Exception $e) {
								$message = $e->getMessage();
								file_put_contents('/tmp/netbanx_error.log', "\n" . 'UserId: ' . $this->user->id . "\n" . var_export($message, true), FILE_APPEND);
								/*Problem with saved profile in test account for netbanx
								 * throw error existing merchantCustomerId
								 * getting that profile id and trying to creat order again
								 * can be removed later. 05.12.2014
								 */
								if ( strpos($message, 'The merchantCustomerId provided for this profile has already been used') !== false ) {
									$profile_id = str_replace('The merchantCustomerId provided for this profile has already been used for another profile - ', '', $message);
									file_put_contents('/tmp/netbanx_error.log', "\n" . 'UserId: ' . $this->user->id . ', New profileId: ' . $profile_id, FILE_APPEND);
									unset($order_data['profile']['merchantCustomerId']);
									$order_data['profile']['id'] = $profile_id;
									$order = $nbx->create_order($order_data);
									$this->_redirect($order->link[0]->uri);
								}


								$this->_redirect(APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-unsuccessful'));
							}
							
							$this->_redirect($order->link[0]->uri);
						}
					} catch(Exception $ex) {
						var_dump($ex);die;
					}
				}
			}
			
			
		}
	}
	
	
	public function validateDates($boost_profile_dates, $packages, $booked_hours)
	{
		$errors = array();
		
		if ( ! count($boost_profile_dates) ) {
			$errors['boosts'] = __('no_dates_selected');
			return $errors;
		}

		foreach($boost_profile_dates as $date) {
			list($date, $hour) = explode(':', $date);
			
			$in_range = false;
			foreach($packages as $package) {
				if ( strtotime($date) >= strtotime($package['date_activated']) &&
					strtotime($date) < strtotime($package['expiration_date'])	
				) {
					$in_range = true;
					break;
				}
			}
			
			if ( ! $in_range ) {
				$errors['boosts'] = __('select_date_in_package_range');
			}
				

			if ( $booked_hours['all'][$date] ) {
				foreach($booked_hours['all'][$date] as $b) {
					if ( $b['hour_from'] == $hour ) {
						$errors['boosts'] = __('date_hour_already_booked');
						break;
					}
				}
			}


			//Checking if escort has booked boost for the same date-hour
			//Escort can have 1 boost on index and 1 in city for the same hour
			foreach($booked_hours['escort'] as $b) {
				if ( $b['date'] == $date && $b['hour_from'] == $hour && 
					(($this->_session->city != 'index' && $b['city_id'] != 'index' ) || ($this->_session->city == 'index' && $b['city_id'] == 'index' ) )
				) {
					$errors['boosts'] = __('already_have_boost_for_time_hour');
				}
			}

		}
		
		return $errors;
	}
	
	private function getProfileBoostPrice()
	{
		$price = 5;
		if ( strtolower($this->_session->city) == 'index' ) {
			$price = 10;
		}
		return $price;
	}
}
