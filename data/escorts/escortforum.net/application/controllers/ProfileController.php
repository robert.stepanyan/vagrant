<?php

class ProfileController extends Zend_Controller_Action
{
	/**
	 * @var Model_Escort_Profile
	 */
	protected $profile;

	/**
	 * @var Model_EscortItem
	 */
	protected $escort;

	/**
	 * @var Zend_Session_Namespace
	 */
	protected $session;

	public static $linkHelper;
	
	
	public $testEscortId = 70413;

	public function init()
	{
        $this->client = new Cubix_Api_XmlRpc_Client();

        self::$linkHelper = $this->view->getHelper('GetLink');
		$this->_request->setParam('no_tidy', true);

		$this->view->testEscortId = $this->testEscortId;

		$anonym = array();

		$this->view->user = $this->user = Model_Users::getCurrent();
		if ( ! $this->user && ! in_array($this->_request->getActionName(), $anonym) ) {
			$this->_redirect($this->view->getLink('signin'));
			return;
		}

		$this->view->layout()->setLayout('private-profile');
		$this->view->addScriptPath($this->view->getScriptPath('private'));

		$this->step = $this->_getParam('step'); 

		$escort_id = intval($this->_getParam('escort'));
		if ( $this->user->isAgency() && $escort_id > 1 ) {
			$this->agency = $agency = $this->user->getAgency();
			if ( ! $agency->hasEscort($escort_id) ) {
				return $this->_redirect($this->view->getLink('private-v2-escorts'));
			}

			$escorts = new Model_Escorts();
			$this->escort = $escorts->getById($escort_id);
		}
		elseif ( $this->user->isAgency() ) {
			$this->agency = $agency = $this->user->getAgency();
			$this->escort = new Model_EscortItem(array('id' => null));
		}
		else {
			$this->escort = $this->user->getEscort();
			if ( is_null($this->escort) ) $this->escort = new Model_EscortItem(array('id' => null));
		}

        
		$this->view->escort = $this->escort;
        
        if(isset($this->escort->is_suspicious) && $this->escort->is_suspicious){
            $this->_redirect($this->view->getLink('private'));
			return;
        }


		// Determine the mode depending on if user has profile
		$this->mode = $this->view->mode = ((isset($this->escort->id) && $this->escort->id && $this->escort->hasProfile() && $this->step != 'finish') ? 'update' : 'create');

		$session_hash = '';
		if ( $this->_request->session_hash ) {
			$session_hash = $this->view->session_hash = $this->_request->session_hash;
		}
		else {
			$session_hash = $this->view->session_hash = md5(microtime() * time());
		}

		$this->session = new Zend_Session_Namespace('profile-data-' . $session_hash);


		$this->profile = $this->view->profile = $this->escort->getProfile();
		$this->profile->setSession($this->session);
		$this->profile->load();
		
		// If we are in update mode, then we need to set the profile model
		// update in real-time mode instead of updating session
		if ( 'update' == $this->mode ) {
			$this->profile->setMode(Model_Escort_Profile::MODE_REALTIME);
		}

		$this->defines = $this->view->defines = Zend_Registry::get('defines');
		
		$config = Zend_Registry::get('escorts_config');

		//Checking specific User Id's  for testing
		if($this->mode == 'create' &&
            (($this->user->isAgency() && $this->agency->id == 1382)  || (!$this->user->isAgency() && ($this->escort->id == 70281)))){

		    $this->view->layout()->setLayout('profile-steps');
            $this->current_step = $this->_getParam('step');
		    $steps = array('biography-step','about-me-step','working-cities-step','services-step','working-times-step','contact-info-step','gallery-step');

		    if(!$this->user->isAgency()){
		        array_unshift($steps,'complete-profile');
            }

		}else {
            $steps = array('biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times');

            if ($config['profile']['rates'] != 0)
                $steps[] = 'prices';

            $steps[] = 'contact-info';
            $steps[] = 'gallery';
            $steps[] = 'finish';
        }
		$this->steps = $steps;
	}

	protected $_c = 0;

	//protected $steps = array('biography', 'about-me', 'languages', 'working-cities', 'services', 'working-times', 'prices', 'contact-info', 'gallery', 'finish');
	protected $steps = array();
	protected $_posted = false;



    public function simpleAction(){
        
            $this->view->simple = true;

            $this->view->layout()->setLayout('simple-profile');
            $this->view->addScriptPath($this->view->getScriptPath('private'));

            $this->_helper->viewRenderer->setScriptAction("simple");

            

            $this->agency = $agency = $this->user->getAgency();
			$this->escort = new Model_EscortItem(array('id' => null));

            $this->_posted = $this->_request->isPost();
            $data = array();
            $errors = array();
            $is_error = false;
			
            $this->view->step = 'biography';

            foreach($this->steps as $step){
                $method = str_replace('-', ' ', $step);
                $method = ucwords($method);
                $method = str_replace(' ', '', $method);
                $method[0] = strtolower($method[0]);
                
                if($step === 'finish' && $this->_posted && !$is_error){

                    $this->finishAction();

                     $this->view->step = 'finish';
//                     $this->_helper->viewRenderer->setScriptAction('finish');
					 $tid = (int)$this->view->escort_id;
					 $this->_redirect($this->view->getLink('private-v2-profile', array('step' => 'gallery', 'escort' => $tid, 'show_success' => 1), true));
                }elseif($step !== 'finish'){
					if ( $step === 'gallery' ){
						continue;
					}
                     $this->{$method . 'Action'}();
                }else{
                    continue;
                }
                
                $tmpData = $this->view->data;
                if($tmpData){
                    $data = array_merge($data,$tmpData);
                }

                $tmpErrors = $this->view->errors;
                if($tmpErrors){
                    $errors = array_merge($errors,$tmpErrors);
                    $is_error = true;
                }
            }

            $this->view->data = $data;
            
            if(!$this->_posted){
                $this->view->data['phone_number'] = $agency['phone'];
                $this->view->data['email'] = $agency['email'];
                $this->view->data['website'] = $agency['web'];
            }

            $this->view->errors = $errors;
		        
    }


	public function indexAction()
	{

		// if($this->escort->is_suspicious){
		// $this->_response->setRedirect($this->view->getLink('signin'));
		// return;
		// }

		
		// var_Dump( $_POST );
		// exit;
		//$titles = array('biography', 'about_me', 'languages', 'working_cities', 'services', 'working_times', 'prices', 'contact_info', 'gallery', 'finish' );
		$config = Zend_Registry::get('escorts_config');
		$titles = array('biography', 'about_me', 'languages', 'working_cities', 'services', 'working_times');
				
		if ($config['profile']['rates'] != 0)
			$titles[] = 'prices';
		
		$titles[] = 'contact_info';
		$titles[] = 'gallery';
		$titles[] = 'finish';
		
		foreach ( $titles as $i => $key ) {
			$titles[$i] = __('pv2_tab_' . $key);
		}
		
		// If the mode is update, i.e. user already has a profile revision
		// we don't need the last step "Finish"
		if ( $this->mode == 'update' ) {
			unset($this->steps[count($this->steps) - 1]);
			unset($titles[count($titles) - 1]);
		}

		$this->view->steps = array_combine($this->steps, $titles);
        $client = new Cubix_Api_XmlRpc_Client();
        $this->view->map_location = $client->call('Escorts.getLocationCoordinates', array($this->escort->id));
		$this->_posted = $this->_request->isPost() && (
			($this->_getParam('then') == ':next' && $this->mode == 'create') ||
			( $this->mode == 'update' )
		);
		$result = $this->_do();

		$then = $this->_getParam('then');

		// If the user clicked the update button, and the result is successfull
		// and we don't need to switch to another tab, just display
		// a user friendly message
		if ( $this->_posted && true === $result && ! strlen($then) ) {
			$this->view->status = 'The section "' . $this->view->steps[$this->view->step] . '" has been successfully updated!';
		}

		if ( false === $result || ! strlen($then) ) { return; }

		switch (true) {
			case ($then == ':next'):
				$i = array_search($this->_getParam('step'), $this->steps);
				if ( $i == count($steps) - 1 ) return;
				
				$step = $this->steps[$i + 1];
				break;
			case ($then == ':back'):
				$i = array_search($this->_getParam('step'), $this->steps);
				if ( $i == 0 ) return;

				$step = $this->steps[$i - 1];

				break;
			default:
				if ( ! in_array($then, $this->steps) ) {
					return;
				}

				$step = $then;
		}

		$this->_setParam('step', $step);
		$this->_setParam('then', '');
		$this->_posted = false;

		$this->_do();
	}

	protected function _do()
	{
		$step = $this->_getParam('step');
		
		// User requested editting of profile without specifying
		// the step, i.e. if he/she clicks on profile icon in private are
		// load the profile from api into current session

		if ( ! in_array($step, $this->steps) ) {
			$this->profile->load(true);
			$step = reset($this->steps);
		}

		$this->view->step = $step;

		$method = str_replace('-', ' ', $step);
		$method = ucwords($method);
		$method = str_replace(' ', '', $method);
		$method[0] = strtolower($method[0]);


		$validator = new Cubix_Validator();
		$about_me_data = $this->profile->getAboutMe();
		$about_me_data['about_it'] = $validator->urlCleaner($about_me_data['about_it'], Cubix_Application::getById()->host);
		$clean_text_length = strlen(trim(strip_tags($validator->specialCharsCleaner($about_me_data['about_it']))));
		if( $clean_text_length > 0 && $clean_text_length < 200 ){
			$this->_helper->viewRenderer->setScriptAction('aboutMe');
			$this->view->step = 'about-me';
			$this->_setParam('about_me_validator', 1);
			return $this->{'aboutMeAction'}();
		}

		$this->_helper->viewRenderer->setScriptAction($step);
		

		if ( $method == 'gallery' ){

			if ( $this->mode == 'create' ) {
				$this->finishAction();
				$tid = (int)$this->view->escort_id;
				$this->_redirect($this->view->getLink('private-v2-profile', array('step' => 'gallery', 'escort' => $tid, 'show_success' => 1), true));
			}
		}

		return $this->{$method . 'Action'}();
	}

	protected function _validateBiography($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'showname' => '',
			'slogan' => 'notags|special',
			'gender' => 'int-nz',
			'age' => 'int-nz',
			'ethnicity' => 'int-nz',
			'nationality_id' => 'int-nz',
			'home_city_id' => 'int-nz',
			'hair_color' => 'int-nz',
			'hair_length' => 'int-nz',
			'eye_color' => 'int-nz',
			'measure_units' => 'int-nz',
			'height' => 'int-nz',
			'weight' => 'int-nz',
			'dress_size' => '',
			'shoe_size' => 'int-nz',
			'bust' => 'int-nz',
			'waist' => 'int-nz',
			'hip' => 'int-nz',
			'cup_size' => '',
			'breast_type' => 'int-nz',
			'pubic_hair' => 'int-nz',
			'tatoo' => 'int-nz',
			'piercing' => 'int-nz',
			'block_countries' => 'arr-int',
			'profile_type' => 'int-nz',
		));
		$data = $form->getData();

		$defines = Zend_Registry::get('defines');
		$data['showname'] = preg_replace('/(\s)+/','$1', trim($data['showname']));
		$escorts_model = new Model_Escorts();
		$blackListModel = new Model_BlacklistedWords();
		if ( ! strlen($data['showname']) ) {
			$validator->setError('showname', Cubix_I18n::translate('showname_required'));
		}
		elseif(mb_strlen($data['showname']) > 25 ){
				$validator->setError('showname', Cubix_I18n::translate('sys_error_showname_no_longer'));
		}
		elseif ( ! preg_match('#^[-_a-z0-9\s]+$#i', $data['showname']) ) {
			$validator->setError('showname', Cubix_I18n::translate('sys_error_must_contain'));
		}
		/*elseif ( $escorts_model->existsByShowname($data['showname'], $this->escort->getId()) ) {
			$validator->setError('showname', 'An escort with same showname exists');
		}*/

		if ( ! strlen($data['home_city_id']) ) {
		// $validator->setError('home_city_a', 'Home city is required');
			$data['home_city_id'] = null;
		}

		if ( ! strlen($data['slogan']) ) {
			$data['slogan'] = null;
		}
		else if( mb_strlen($data['slogan']) > 20 ) {
			$validator->setError('slogan', Cubix_I18n::translate('sys_error_slogan_text_must_be'));
		}
		else if($blackListModel->checkWords($data['slogan'], Model_BlacklistedWords::BL_TYPE_SLOGAN)) {
			$validator->setError('slogan','You can`t use word "'.$blackListModel->getWords() .'"');
		}
		else
		{
			$data['slogan'] = $validator->urlCleaner($data['slogan'], Cubix_Application::getById()->host);
			$data['slogan'] = $validator->removeEmoji($data['slogan']);
		}

		if ( is_null($data['gender']) ) {
			$validator->setError('gender', Cubix_I18n::translate('gender_required'));
		}
		else if ( ! array_key_exists($data['gender'], $defines['gender_options']) ) {
			$validator->setError('gender', Cubix_I18n::translate('sys_error_gender_is_invalid'));
		}

		if ( $this->mode == 'create' ) {
			if ( is_null($data['profile_type']) ) {
				$validator->setError('profile_type', Cubix_I18n::translate('profile_type_required'));
			}
			else if ( ! array_key_exists($data['profile_type'], $defines['profile_type_options']) ) {
				$validator->setError('profile_type', Cubix_I18n::translate('sys_error_profile_type_is_invalid'));
			}
		}

		$age = $data['age']; unset($data['age']);
		if ( ! is_null($age) ) {
			$data['birth_date'] = strtotime('-' . $age . ' year');
		} else {
			$data['birth_date'] = null;
		}

		if ( ! in_array($data['measure_units'], array(METRIC_SYSTEM, ROYAL_SYSTEM)) ) {
			$data['measure_units'] = METRIC_SYSTEM;
		}

		if ( ! in_array($data['cup_size'], $this->defines['breast_size_options']) ) {
			$data['cup_size'] = null;
		}

		if ( Cubix_Application::getById()->measure_units == ROYAL_SYSTEM ) {
			if ( $data['height'] ) {
				$data['height'] = Cubix_UnitsConverter::convert(stripslashes($data['height']), 'ftin', 'cm');
			}

			if ( $data['weight'] ) {
				$data['weight'] = Cubix_UnitsConverter::convert($data['weight'], 'lbs', 'kg');
			}
		}

		foreach ( $data['block_countries'] as $i => $zone_id ) {
			if ( $zone_id != '0' ) {
				$data['block_countries'][$i] = array('country_id' => $zone_id);
			}
			else {
				unset($data['block_countries'][$i]);
			}
		}


		$data['height'] = intval($data['height']);
		if ( ! $data['height'] ) $data['height'] = null;
		if ( Cubix_Application::getById()->measure_units != ROYAL_SYSTEM ) {
			$data['weight'] = intval($data['weight']);
		}
		if ( ! $data['weight'] ) $data['weight'] = null;

		return $data;
	}

	public function biographyAction()
	{
		$client = Cubix_Api_XmlRpc_Client::getInstance();

        $countyModel = new Model_Countries();
		$this->view->countries = $countyModel->getCountries(array(Cubix_Application::getById()->country_id));

		$this->view->profile_type = $client->call('Escorts.getField', array($this->escort->id, 'profile_type'));

		if ( $this->_posted ) {
			$validator = new Cubix_Validator();
			
			$this->view->data = $data = $this->_validateBiography($validator);


			$home_city_id = $this->view->data['home_city_id'];

			$home_city_country = '';
			if ( $home_city_id ) {
				$m_city = new Cubix_Geography_Cities();
				$home_city = $m_city->get($home_city_id);

				$m_country = new Cubix_Geography_Countries();
				$home_country = $m_country->get($home_city->country_id);

				$home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
			}
			$this->view->home_city_country = $home_city_country;

			if ( $this->_getParam('dont') ) {
				return false;
			}

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();
				
				$this->view->errors = $status['msgs'];
				return false;
			}
			
			$this->_addSlogan($data['slogan'], $this->escort->id);
			$slogan = $this->_getSlogan($this->escort->id);
			$this->view->slogan = $slogan['text'];
			$this->view->slogan_status = $slogan['status'];

            /* Show run time Profile Type change view */
            $this->view->profile_type = $data["profile_type"];

			$this->profile->update($data, 'biography');
            $this->view->data['block_countries'] = $countyModel->getBlockCountries($this->escort->id);
		}
		else {
			$this->view->data = $this->profile->getBiography();

			$home_city_id = $this->view->data['home_city_id'];

			$home_city_country = '';
			if ( $home_city_id ) {
				$m_city = new Cubix_Geography_Cities();
				$home_city = $m_city->get($home_city_id);

				$m_country = new Cubix_Geography_Countries();
				$home_country = $m_country->get($home_city->country_id);

				$home_city_country = $home_city->{'title_' . $this->_request->lang_id} . ' (' . $home_country->{'title_' . $this->_request->lang_id} . ')';
			}

			$this->view->data['block_countries'] = $countyModel->getBlockCountries($this->escort->id);
			
		// $this->view->countries = $countyModel->getCountries();
			
			$this->view->home_city_country = $home_city_country;
			$slogan = $this->_getSlogan($this->escort->id);
			$this->view->slogan = $slogan['text'];
			$this->view->slogan_status = $slogan['status'];
		}
	}

	protected function _addSlogan($slogan, $escort_id)
	{
		$config_system = Zend_Registry::get('system_config');
		
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$client->call('Escorts.addSlogan', array($slogan, $escort_id, $config_system['sloganApprovation']));

		return true;
	}

	protected function _getSlogan($escort_id)
	{
		if ( $escort_id ) {
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$slogan = $client->call('Escorts.getSlogan', array($escort_id));

			return $slogan;
		}
		else {
			return false;
		}
	}



	protected function _validateAboutMe($validator)
	{
		$blackListModel = new Model_BlacklistedWords();
		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'is_smoking' => 'int-nz',
			'is_drinking' => 'int-nz',
			'characteristics' => 'notags|special'
		);

		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$fields['about_' . $lng] = 'xss';
		}
		
		$form->setFields($fields);
		$data = $form->getData();
		$checkLanguageLetters = true;
		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
            if($bl_words = $blackListModel->checkWords($data['about_' . $lng], Model_BlacklistedWords::BL_TYPE_ABOUT)) {
				foreach($bl_words as $bl_word){
					if($bl_word['type'] == Model_BlacklistedWords::BL_SEARCH_TYPE_EXACT){
						$pattern = '/(\W+|^)'. preg_quote($bl_word['bl_word'], '/') . '(\W+|$)/i';
						$data['about_' . $lng] = preg_replace($pattern, '\1<abbr title="Blacklisted word" class = "black-listed" >' . $bl_word['bl_word'] . '</abbr>\2', $data['about_' . $lng]);
					}
					else{
						$pattern = '/' . preg_quote($bl_word['bl_word'], '/') . '/i';
						$data['about_' . $lng] = preg_replace($pattern, '<abbr title="Blacklisted word" class = "black-listed" >' . $bl_word['bl_word'] . '</abbr>', $data['about_' . $lng]);

					}
				}

				$validator->setError('about_me_text', 'You can`t use word "'.$blackListModel->getWords() .'"');
				break;
                
			}

			else{
				$data['about_' . $lng] = $validator->urlCleaner($data['about_' . $lng], Cubix_Application::getById()->host);
				$data['about_' . $lng] = $validator->removeEmoji($data['about_' . $lng]);

                if (strpos($data['about_' . $lng], 'bdo') !== false) {
                    $data['about_' . $lng] = str_replace('bdo', ' ', $data['about_' . $lng]);
                }

//                <bdo dir = "rtl">ppAstahW</bdo>

			}

            $data['about_' . $lng] = preg_replace('/[[:^print:]]/', '', $data['about_' . $lng]);
            $wrong_letter['symbol'][] = $this->checkSymbols($data['about_' . $lng]);

            $checkedData = trim($this->stripAsci(strip_tags($data['about_' . $lng])));


            for ($i = 0 ;$i < strlen($checkedData);$i++){
                if(!(mb_detect_encoding($checkedData[$i],'UTF-8'))){
                    $prev_letter = ($i == 0) ? "":mb_substr($checkedData,strpos($checkedData,$checkedData[$i]) - 1,1,'utf-8');
                    $next_letter = ($i == strlen($checkedData))?"" : mb_substr($checkedData,strpos($checkedData,$checkedData[$i]) + 1,1,'utf-8');
                    $current_letter  = mb_substr($checkedData,strpos($checkedData,$checkedData[$i]),1,'utf-8');
                    $wrong_letter['letters'] = $prev_letter .''. $current_letter.''.$next_letter;
                    break;
                }
            }
        }

		$clean_text_length = strlen(trim(strip_tags($data['about_it'])));
		if( $clean_text_length < 200 ){
			$validator->setError('about_me_text', Cubix_I18n::translate('about_text_min_length_it',array('LENGTH'=>200)));
		}

		$checkSymbol = implode($wrong_letter['symbol']);
		if(!empty($wrong_letter['letters'])){
            $validator->setError('error_language', Cubix_I18n::translate('error_language_letter',array('LENGTH'=>200)) . ' - ' .$wrong_letter['letters']);
		} elseif(strlen($checkSymbol) > 1){
            $validator->setError('error_symbol', Cubix_I18n::translate('error_symbol',array('LENGTH'=>200)) . ' - ' .$checkSymbol);
        }

		/*if (strlen($data['about_it']) < 200)
			$validator->setError('about_me_text', $this->view->t('about_text_min_length_it', array('LENGTH' => 200)));*/

		if($blackListModel->checkWords($data['characteristics'], Model_BlacklistedWords::BL_TYPE_SPECIAL_CHAR)) {
			$validator->setError('characteristics','You can`t use word "'.$blackListModel->getWords() .'"');
		}
		else{
			$data['characteristics'] = $validator->urlCleaner($data['characteristics'], Cubix_Application::getById()->host);
		}

		return $data;
	}



    public function aboutMeAction()
	{
		$validator = new Cubix_Validator();
		if ( $this->_posted ) {
			$blackListModel = new Model_BlacklistedWords();

			$this->view->data = $data = $this->_validateAboutMe($validator);

			$data['about_has_bl'] = 0;
			if ( ! $validator->isValid() ) {
				
				$status = $validator->getStatus();
				
				$this->view->errors = $status['msgs'];
				return false;
			}
			/*foreach ( Cubix_I18n::getLangs(true) as $lng ) {
				if($blackListModel->checkWords($data['about_' . $lng], Model_BlacklistedWords::BL_TYPE_ABOUT)){
					$data['about_has_bl'] = 1; BREAK;
				}
			}*/
			return $this->profile->update($data, 'about-me');
			}
		else {
			$this->view->data = $this->profile->getAboutMe();

			if ( $this->_getParam('about_me_validator') === 1 ) {
				$validator->setError('about_me_text', Cubix_I18n::translate('about_text_min_length_it_mandatory',array('LENGTH'=>200)));
				$status = $validator->getStatus();
				$this->view->errors = $status['msgs'];
			}
		}
	}

	public function _validateLanguages($validator)
	{
		$defines = Zend_Registry::get('defines');
		
		$langs = $this->_getParam('langs', array());
		if ( ! is_array($langs) ) $langs = array();

		$data = array('langs' => array());

		$invalid_lang = false;
		foreach ( $langs as $lang_id => $level ) {
			$level = intval($level); if ( $level < 1 ) continue;
			$data['langs'][] = array('lang_id' => $lang_id, 'level' => $level);

			if ( ! array_key_exists($lang_id, $defines['language_options']) ) {
				$invalid_lang = true;
			}
		}

		if ( ! count($data['langs']) ) {
			$validator->setError('language_error', Cubix_I18n::translate('sys_error_language_required'));
		}
		else if ( $invalid_lang ) {
			$validator->setError('language_error', Cubix_I18n::translate('sys_error_lang_is_invalid'));
		}

		return $data;
	}

	public function languagesAction()
	{
		if ( $this->_posted ) {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateLanguages($validator);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}

			return $this->profile->update($data, 'languages');
		}
		else {
			$this->view->data = $this->profile->getLanguages();
		}
	}

	protected function _validateWorkingCities($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'country_id' => 'int-nz',
			'city_id' => 'int-nz',
			'cities' => 'arr-int',
			'cityzones' => 'arr-int',
			'zip' => '',
			'incall' => 'int',
			'incall_type' => 'int-nz',
			'incall_hotel_room' => 'int-nz',
			'incall_other' => 'notags|special',
			'outcall' => 'int',
			'outcall_type' => 'int-nz',
			'outcall_other' => 'notags|special'
		));
		$data = $form->getData();

		if ( ! is_array($data['cities']) ) $data['cities'] = array();

		$invalid_city = false;
		foreach ( $data['cities'] as $i => $city_id ) {
			$data['cities'][$i] = array('city_id' => $city_id);
			if ( ! $city_id ) {
				unset($data['cities'][$i]);
			}
			else if ( ! Cubix_Geography_Cities::isFromApplicationCountry($city_id) ) {
				$invalid_city = true;
				unset($data['cities'][$i]);
			}
		}
		
		// Anti-Hack: Ignore trailing cities
		$data['cities'] = array_slice($data['cities'], 0, Cubix_Application::getById()->max_working_cities);

		if ( ! is_array($data['cityzones']) ) $data['cityzones'] = array();

		foreach ( $data['cityzones'] as $i => $zone_id ) {
			if ( $zone_id != '0' ) {
				$data['cityzones'][$i] = array('city_zone_id' => $zone_id);
			}
			else {
				unset($data['cityzones'][$i]);
			}
		}
		
		if(count($data['cityzones']) > 2){
			$validator->setError('country_id', Cubix_I18n::translate('Only 2 Cityzone can be added!'));
		}
		if ( ! $data['incall'] ) {
			$data['incall_type'] = null;
			$data['incall_hotel_room'] = null;
			$data['incall_other'] = null;
			$data['zip'] = null;
		}
		else{
			if ( !$data['incall_type'] ) {
			$validator->setError('zip', Cubix_I18n::translate('sys_error_choose_suboption'));
			$data['incall_type'] = 999;
			}
		}

		if ( ! $data['outcall'] ) {
			$data['outcall_type'] = null;
			$data['outcall_other'] = null;
		}
		

		if ( $data['incall'] && ! strlen($data['zip']) ) {
		// $validator->setError('zip', 'zip code required');
            $data['zip'] = null;
		} elseif($data['incall'] && strlen($data['zip'])){
			if(strlen($data['zip']) > 5 || !preg_match('/^\d{5}$/', $data['zip'])){
				$validator->setError('zip', Cubix_I18n::translate('sys_error_zip'));
			}
		}
		unset($data['incall']);
		unset($data['outcall']);
		
		if ( ! is_null($data['incall_type']) && ! $validator->validateZipCode($data['zip'], false) ) {
		// $validator->setError('zip', 'Invalid zip code');
            $data['zip'] = null;
		}
		
		$data['zip'] = $validator->urlCleaner($data['zip'], Cubix_Application::getById()->host);
		$data['incall_other'] = $validator->urlCleaner($data['incall_other'], Cubix_Application::getById()->host);
		$data['outcall_other'] = $validator->urlCleaner($data['outcall_other'], Cubix_Application::getById()->host);

		if ( ! is_null($data['incall_type']) && $data['incall_type'] == 2 ) {
			if ( ! isset($data['incall_hotel_room']) ) {
				$validator->setError('incall_hotel_room', Cubix_I18n::translate('sys_error_choose_hotel_room'));
			}
		}

		if ( ! $data['city_id'] && ! count($data['cities']) ) {
			$validator->setError('country_id', Cubix_I18n::translate('sys_error_base_city_required'));
		}
		else if ( ! Cubix_Geography_Cities::isFromApplicationCountry($data['city_id']) ) {
			$validator->setError('country_id', Cubix_I18n::translate('sys_error_base_city_not_from', array('country' => Cubix_Application::getById()->country_title )));
		}
		else if ( $invalid_city ) {
			$validator->setError('country_id', Cubix_I18n::translate('sys_error_selected_cities_not_from', array('country' => Cubix_Application::getById()->country_title )));
		}

		if ( ! $data['country_id'] ) {
			$validator->setError('country_id', Cubix_I18n::translate('sys_error_required'));
		}
		else if ( $data['country_id'] != Cubix_Application::getById()->country_id ) {
			$validator->setError('country_id', Cubix_I18n::translate('sys_error_invalid_country_selected'));
		}
		
		return $data;
	}

	public function workingCitiesAction()
	{
		$data = $this->profile->getWorkingCities();

		$countries = new Cubix_Geography_Countries();
		$this->view->countries = $countries->ajaxGetAll(false);
        $this->view->notCheckedPremiumCity = false;
		$cities = new Cubix_Geography_Cities();

		$country_id = Cubix_Application::getById()->country_id;
		if ( isset($data['city_id']) && $data['city_id'] ) {
			$city = $cities->get($data['city_id']);
			$country_id = $city->country_id;
		}

		$region_cities = $this->view->cities = $cities->ajaxGetAll(null, $country_id);
		/*$regions = array();
		foreach ( $region_cities as $city ) {
			if ( ! isset($regions[$city->region_title]) ) {
				$regions[$city->region_title] = array();
			}

			$regions[$city->region_title][] = $city;
		}

		ksort($regions);*/
		$this->view->regions = $region_cities;
		
		//////////////////////////////////////
		$client = Cubix_Api::getInstance();
		$premium_cities = $client->call('getEscortActivePackage', array($this->profile->getId(), 'en'));

		$p_c = array();
		if ( count($premium_cities['cities']) > 0 ) {
			foreach ( $premium_cities['cities'] as $premium_city ) {
				$p_c[] = $premium_city['id'];
			}
			if(!$this->_posted){
			    if($this->escort->hasProduct(14)){
                    $this->view->notCheckedPremiumCity = !$this->isCheckedPremiumCity($p_c,$data['cities']);
                }
            }
		}
		$this->view->premium_cities = $p_c;
		//////////////////////////////////////
			
		if ( $this->_posted ) {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateWorkingCities($validator);

			

			$param_cities = array();
			if ( count($data['cities']) ) {
				foreach ( $data['cities'] as $city ) {
					$param_cities[] = $city['city_id'];
				}
			}

            if ( count($p_c) > 0 ) {
                if($this->escort->hasProduct(14)) {
                    $this->view->notCheckedPremiumCity = !$this->isCheckedPremiumCity($p_c, $data['cities']);
                }
            }

			$this->_request->setParam('ajax_cities', implode(',', $param_cities));

			$this->getCityzonesAction();
			$this->view->layout()->enableLayout();

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}

			return $this->profile->update($data, 'working-cities');
		}
		else {

			$data = $this->profile->getWorkingCities();
			
			$param_cities = array();
			if ( count($data['cities']) ) {
				foreach ( $data['cities'] as $city ) {
					$param_cities[] = $city['city_id'];
				}
			}

			$this->_request->setParam('ajax_cities', implode(',', $param_cities));
			
			$this->getCityzonesAction();
			$this->view->layout()->enableLayout();

			$data['country_id'] = $country_id;

			$this->view->data = $data;
		}
	}

	public function getCityzonesAction()
	{
		$this->view->layout()->disableLayout();

		$cities = $this->_request->ajax_cities;
		$cities = trim($cities, ',');

		$cities = explode(',', $cities);

		$cz_model = new Cubix_Geography_Cityzones();

		$all_cityzones = array();
		if ( count($cities) ) {
			foreach( $cities as $city ) {
				$cityzones = $cz_model->ajaxGetAll($city);
				if ( count($cityzones) ) {
					$all_cityzones = array_merge($all_cityzones, $cityzones);
				}
			}
		}

		if ( isset($_SERVER['HTTP_X_REQUESTED_WITH']) ) {
			die(json_encode($all_cityzones));
		}
		else {
			$this->view->all_cityzones = $all_cityzones;
		}
	}

	public function _validateServices($validator)
	{		
		$form = new Cubix_Form_Data($this->_request);
		$fields = array(
			'sex_orientation' => 'int-nz',
			'sex_availability' => '',
			'services' => 'arr-int',
			'service_prices' => 'arr-int',
			'service_currencies' => 'arr-int'
		);

		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$fields['additional_service_' . $lng] = 'notags|special';
		}

		$form->setFields($fields);
		$data = $form->getData();

		if ( ! is_array($data['services']) ) $data['services'] = array();

		foreach ( $data['services'] as $i => $svc ) {
			$price = isset($data['service_prices'][$svc]) ? intval($data['service_prices'][$svc]) : null;
			if ( ! $price ) $price = null;

			$currency = isset($data['service_currencies'][$svc]) ? intval($data['service_currencies'][$svc]) : null;
			if ( ! $currency ) $currency = null;
			
			$data['services'][$i] = array('service_id' => $svc, 'price' => $price, 'currency_id' => $currency);
		}

		if ( ! is_array($data['sex_availability']) ) {
			$data['sex_availability'] = array();

			$validator->setError('services_offered_for_error', Cubix_I18n::translate('select_services_for_option'));
		}

		foreach ( $data['sex_availability'] as $i => $opt ) {
			if ( ! isset($this->defines['sex_availability_options'][$opt]) ) {
				unset($data['sex_availability'][$i]);
			}
		}
		$data['sex_availability'] = count($data['sex_availability']) ?
			implode(',', $data['sex_availability']) : null;

		unset($data['service_prices']);
		unset($data['service_currencies']);
		
		foreach ( Cubix_I18n::getLangs(true) as $lng ) {
			$data['additional_service_' . $lng] = $validator->urlCleaner($data['additional_service_' . $lng], Cubix_Application::getById()->host);
			$data['additional_service_' . $lng] = $validator->removeEmoji($data['additional_service_' . $lng]);
		}
		

		return $data;
	}

	public function servicesAction()
	{
		if ( $this->_posted ) {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateServices($validator);

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}
			
			return $this->profile->update($data, 'services');
		}
		else {
			$data = $this->profile->getServices();

			$this->view->data = $data;
		}
	}

	protected function _validateWorkingTimes($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'available_24_7' => 'int-nz',
			'night_escort' => 'int-nz',
			'day_index' => 'arr-int',
			'time_from' => 'arr-int',
			'time_from_m' => 'arr-int',
			'time_to' => 'arr-int',
			'time_to_m' => 'arr-int',
			'vac_date_from_day' => 'int-nz',
			'vac_date_from_month' => 'int-nz',
			'vac_date_from_year' => 'int-nz',
			'vac_date_to_day' => 'int-nz',
			'vac_date_to_month' => 'int-nz',
			'vac_date_to_year' => 'int-nz',
			'night_escorts' => 'arr-int'
		));
		$data = $form->getData();
		
		$_data = array('available_24_7' => $data['available_24_7'], 'night_escort' => $data['night_escort'],'times' => array(),'vac_date_from' => null,'vac_date_to' => null);

		for ( $i = 1; $i <= 7; $i++ ) {
			if ( isset($data['day_index'][$i]) && isset($data['time_from'][$i]) && isset($data['time_from_m']) && isset($data['time_to'][$i]) && isset($data['time_to_m'][$i]) ) {
				$night_escort =  in_array($i,$data['night_escorts'])?1:0;
				$_data['times'][] = array(
					'day_index' => $i,
					'time_from' => $data['time_from'][$i],
					'time_from_m' => $data['time_from_m'][$i],
					'time_to' => $data['time_to'][$i],
					'time_to_m' => $data['time_to_m'][$i],
					'night_escorts'=>$night_escort
				);
			}
		}
		if(isset($data['vac_date_from_day']) || isset($data['vac_date_from_month']) || isset($data['vac_date_from_year']) || isset($data['vac_date_to_day']) || isset($data['vac_date_to_month']) || isset($data['vac_date_to_year'])){

			$date_from = $data['vac_date_from_year']."-".$data['vac_date_from_month']."-".$data['vac_date_from_day'];
			$date_to = $data['vac_date_to_year']."-".$data['vac_date_to_month']."-".$data['vac_date_to_day'];

			if( isset($data['vac_date_from_day']) && isset($data['vac_date_from_month']) && isset($data['vac_date_from_year']) && isset($data['vac_date_to_day']) && isset($data['vac_date_to_month']) && isset($data['vac_date_to_year'])){
				
				if ( strtotime($date_from) >= strtotime($date_to) )
				{
					$validator->setError('vacation', Cubix_I18n::translate('sys_error_invalid_date_interval'));
				}
				else if ( strtotime($date_to) < strtotime(date('Y-m-d')) )
				{
					$validator->setError('vacation', Cubix_I18n::translate('sys_error_invalid_date_interval'));
				}
			}
			else{
				$validator->setError('vacation', Cubix_I18n::translate('sys_error_invalid_date_interval'));
			}
			$_data['vac_date_from'] = $date_from;
			$_data['vac_date_to'] = $date_to;
		}
		
		return $_data;
	}

	public function workingTimesAction()
	{
		$vacation = new Model_EscortV2Item(array('id' =>$this->escort->id  ));
		$this->view->escort_id = $this->escort->id;
		if ( $this->_posted ) {
			$validator = new Cubix_Validator();

			$this->view->data = $data = $this->_validateWorkingTimes($validator);
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();
				
				$this->view->errors = $status['msgs'];
				return false;
			}
			$vacation->updateVacation($data['vac_date_from'], $data['vac_date_to']);
			return $this->profile->update($data, 'working-times');
		}
		else {
			
			$vac = (array) $vacation->getVacation();
			$data = $this->profile->getWorkingTimes();
			$data = array_merge($data,$vac);
			$this->view->data = $data;
			
		}
	}

	protected function _validatePrices($validator)
	{
		$rates = $this->_getParam('rates');

		if ( ! is_array($rates) ) {
			$rates = array();
		}

		$data = array('rates' => array());

		//$currencies = array_keys($this->defines['currencies']);
		$currencies = array_keys(Model_Currencies::getAllAssoc());
		$units = array_keys($this->defines['time_unit_options']);

		foreach ( $rates as $availability => $_rates ) {
			if ( $availability == 'incall' ) { $availability = 1; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			elseif ( $availability == 'outcall' ) { $availability = 2; $types = array('additional-hour', 'overnight', 'dinner-date', 'weekend'); }
			else continue;

			foreach ( $_rates as $rate ) {
				if ( get_magic_quotes_gpc() ) $rate = stripslashes($rate);
				$rate = @json_decode($rate);

				if ( is_object($rate) ) $rate = (array) $rate;
				if ( $rate === false || ! is_array($rate) ) {
					continue;
				}

				// Case when rate is standart and some data are not valid
				if ( ! isset($rate['type']) && ( ! isset($rate['time']) || ! isset($rate['unit']) || ! isset($rate['price']) || ! isset($rate['currency']) ) ) {
					continue;
				}
				// Case when rate is a type of custom and some data are not valid
				elseif ( isset($rate['type']) && ( ! isset($rate['price']) || ! isset($rate['currency']) || ! in_array($rate['type'], $types) ) ) {
					continue;
				}

				// If price is invalid
				$price = intval($rate['price']);
				if ( $price <= 0 ) continue;

				// If currency is invalid
				$currency = intval($rate['currency']);
				if ( ! in_array($currency, $currencies) ) {
					continue;
				}

				// If rate is custom validate data add only type, price and currency fields
				if ( ! isset($rate['type']) ) {
					$time = intval($rate['time']);
					if ( $time <= 0 ) continue;

					$unit = intval($rate['unit']);
					if ( ! in_array($unit, $units) ) continue;

					$data['rates'][] = array('availability' => $availability, 'time' => $time, 'time_unit' => $unit, 'price' => $price, 'currency_id' => $currency);
				}
				// Otherwize add also time and time unit
				else {
					$data['rates'][] = array('availability' => $availability, 'type' => $rate['type'], 'price' => $price, 'currency_id' => $currency);
				}
			}
		}
		
		return $data;
	}


	public function	pricesAction()
	{
		$this->_request->setParam('no_tidy', true);
		
		if ( $this->_posted ) {
			$validator = new Cubix_Validator();

			$data = $this->_validatePrices($validator);
			$this->view->data = array('rates' => $this->profile->reconstructRates($data['rates']));

			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}
			
			return $this->profile->update($data, 'prices');
		}
		else {
			$data = $this->profile->getPrices();
			$this->view->data = $data;
		}
	}

	public function _validateContactInfo($validator)
	{
		$form = new Cubix_Form_Data($this->_request);
		$form->setFields(array(
			'phone_prefix' => '',
            'location_lat' => '',
            'location_lng' => '',
            'location_map_address' => '',
			'phone_number' => '',
			/*'phone_number_alt' => '',*/
			'phone_instr' => 'int-nz',
			'phone_instr_no_withheld' => 'int-nz',
			'phone_instr_other' => 'notags|special',
			'email' => '',
			'website' => 'notags|special',
			'club_name' => 'notags|special',
			'street' => 'notags|special',
			'street_no' => 'notags|special',
			'display_address' => 'int',
			'viber' => 'int',
			'whatsapp' => 'int',
            'telegram' => 'int'
		));
		$data = $form->getData();
		$data['display_address'] = (bool) $data['display_address'];
        $data['contact_phone_parsed'] = null;
		
		$data['phone_country_id'] = null;
		$phone = isset($data['phone_number']) ? $this->_parsePhoneNumber($data['phone_number']) : null;

		/*if (! strlen($data['phone_prefix']) && ! strlen($data['phone_number']) && ! strlen($data['email']))
		{
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_email_required'));
		}
		else
		{
			if (strlen($data['phone_prefix']) && ! strlen($data['phone_number']))
			{
				$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_required'));
			}
			elseif (!strlen($data['phone_prefix']) && strlen($data['phone_number']))
			{
				$validator->setError('phone_prefix', Cubix_I18n::translate('sys_error_country_calling_code_required'));
			}
			elseif (strlen($data['phone_prefix']) && strlen($data['phone_number']))
			{
				if (preg_match("/^(\+|00)/", trim($data['phone_number'])))
				{
					$validator->setError('phone_number', Cubix_I18n::translate('sys_error_enter_phone_without_country_code'));
				}
				elseif (false === ($phone = $this->_parsePhoneNumber($data['phone_number'])) || ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_number']))
				{
					$validator->setError('phone_number', Cubix_I18n::translate('sys_error_invalid_phone_number'));
				}
				else
				{
					$data['phone_number'] = $phone;
				}
			}
		}*/
		
		if ( ! strlen($data['phone_prefix']) ) {
			$validator->setError('phone_prefix', Cubix_I18n::translate('sys_error_country_calling_code_required'));
		}
		if ( ! strlen($data['phone_number']) ) {
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_required'));
		}
		elseif(preg_match("/^(\+|00)/", trim($data['phone_number'])) ) {
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_enter_phone_without_country_code'));
		}
		else if ( false === $phone || ! preg_match("/^[0-9\s\+\-\(\)]+$/i", $data['phone_number'])) {
			$validator->setError('phone_number', Cubix_I18n::translate('sys_error_invalid_phone_number'));
		}
		else {
			$data['phone_number'] = $phone;

		}
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		if ($phone)
		{   //Onyx
			list($country_id,$phone_prefix,$ndd_prefix) = explode('-',$data['phone_prefix']);
			$data['phone_country_id'] = intval($country_id);
			$data['contact_phone_parsed'] = preg_replace('/^'.$ndd_prefix.'/', '', $phone);
			$data['contact_phone_parsed'] = '00'.intval($phone_prefix).$data['contact_phone_parsed'];
			
			$agency_id = $this->user->isAgency() ? $this->agency->id : null;
			$escort_id = $this->escort->id ? $this->escort->id : null;
			
			$results = $client->call('Escorts.existsByPhone', array($data['contact_phone_parsed'], $escort_id, $agency_id));
			
			$resCount = is_array($results) ? count($results) : 0;
			if ( $resCount > 0 ) {
				$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_already_exists'));
			}
			else
			{

				$session = new Zend_Session_Namespace('profile-data');
				
				if(($session->phone != $phone) || $session->phone_prefix_id != $data['phone_country_id'])
				{	
					$client = Cubix_Api_XmlRpc_Client::getInstance();
					$is_agency_phone = false;
					if($this->user->isAgency()){
						$agency_phone = $client->call('Agencies.getPhoneById', array($agency_id));
						
						if($agency_phone == $data['contact_phone_parsed']){
							$is_agency_phone = true;
						}
					}
				
					$status = $client->call('Escorts.getPhoneConfirmStatus', array($escort_id, $agency_id, $data['contact_phone_parsed']));


					if( (!isset($status) || $status == 0) && !$is_agency_phone)
					{
						// look task EFNET-1122
						// REMOVE_FOR_LIVE

						if($this->testEscortId == $this->escort->id){
							if ( $this->escort->hasProfile()) {
								$this->view->invalid_phone = $session->phone;
								$this->view->invalid_prefix = $session->phone_prefix_id;
								$validator->setError('phone_number', Cubix_I18n::translate('sys_error_confirm_phone_number'));
							}else{
								$this->view->phone_confirmed = 0;
							}
						}else{
							$this->view->invalid_phone = $session->phone;
							$this->view->invalid_prefix = $session->phone_prefix_id;
							$validator->setError('phone_number', Cubix_I18n::translate('sys_error_confirm_phone_number'));
						}
					}else{
						$this->view->phone_confirmed = 1;
					}
				}
				
			}

			$bl_phone = $client->call('Escorts.isBlacklistedPhone', array($data['contact_phone_parsed']));
			
			if ($bl_phone)
			{
				$validator->setError('phone_number', Cubix_I18n::translate('sys_error_phone_blocked'));
			}
		}
		$data['phone_exists'] = $data['contact_phone_parsed'];
		unset($data['phone_prefix']);
		if ( ! in_array($data['phone_instr'], array_keys($this->defines['phone_instructions'])) ) {
			$data['phone_instr'] = null;
		}

		if ( ! is_null($data['phone_instr_no_withheld']) ) {
			$data['phone_instr_no_withheld'] = 1;
		}

		if ( ! is_null($data['phone_instr_other']) && $this->_hasPhoneNumber($data['phone_instr_other']) ) {
			$validator->setError('phone_instr_other', Cubix_I18n::translate('sys_error_invalid_other'));
		}
		
		$blackListModel = new Model_BlacklistedWords();
		if($blackListModel->checkWords($data['phone_instr_other'], Model_BlacklistedWords::BL_TYPE_OTHER)) {
			$validator->setError('phone_instr_other','You can`t use word "'.$blackListModel->getWords() .'"');
		}
		
		if ( strlen($data['email']) && ! $this->_validateEmailAddress($data['email']) ) {
			$validator->setError('email', Cubix_I18n::translate('sys_error_invalid_email_address'));
		}

		if ( strlen($data['website']) && ! $this->_validateWebsiteUrl($data['website']) ) {
			$validator->setError('website', Cubix_I18n::translate('sys_error_invalid_url'));
		}
		
		$contact_data = $client->call('Escorts.getWebPhone', array($this->escort->id));
		
		if ( strlen($data['website'])){
			$old_website = $contact_data['website'];
			if ($old_website != $data['website']){
				$data['website_changed'] = 1;
			}
		}
		if ( strlen($data['contact_phone_parsed'])){
			$old_phone = $contact_data['contact_phone_parsed'];
			if ($old_phone != $data['contact_phone_parsed']){
				$data['phone_changed'] = 1;
				$data['old_phone'] = $old_phone;
			}
		}
		
		$data['phone_instr_other'] = $validator->urlCleaner($data['phone_instr_other'], Cubix_Application::getById()->host);
		$data['phone_instr_other'] = $validator->removeEmoji($data['phone_instr_other']);
		$data['club_name'] = $validator->urlCleaner($data['club_name'], Cubix_Application::getById()->host);
		$data['street'] = $validator->urlCleaner($data['street'], Cubix_Application::getById()->host);
		$data['street_no'] = $validator->urlCleaner($data['street_no'], Cubix_Application::getById()->host);
      
		if ( ! $this->escort->hasProfile() && $this->user->isEscort() ) {
			if ( $data['phone_prefix'] != '39' || ( $data['phone_prefix'] == '39' && $data['phone_instr'] == 2) ) {
				$data['pseudo_escort'] = 1;
			}
		}
		//var_dump($data);die;
		return $data;
	}

	public function sendSmsAction()
	{	
		$this->view->layout()->disableLayout();
		$req = $this->_request;
		if($req->isXmlHttpRequest() && $req->isPost()&& isset($req->phone) && isset($req->prefix)) 
		{
			$session = new Zend_Session_Namespace('profile-data');
			if ( !strlen($req->prefix) ) 
				die(Cubix_I18n::translate('sys_error_country_calling_code_required'));
			
			if ( ! strlen($req->phone) ) 
				die(Cubix_I18n::translate('sys_error_phone_required'));
			
			if(preg_match("/^(\+|00)/", trim($req->phone)) ) 
				die(Cubix_I18n::translate('sys_error_enter_phone_without_country_code'));
			
			if ( false === ($phone = $this->_parsePhoneNumber($req->phone)) || ! preg_match("/^[0-9\s\+\-\(\)]+$/i",$req->phone)) 
				die(Cubix_I18n::translate('sys_error_invalid_phone_number'));
			
			list($country_id,$phone_prefix,$ndd_prefix) = explode('-',$req->prefix);
			
			$new_phone = preg_replace('/^'.$ndd_prefix.'/', '', $phone);
			$new_phone_parsed = '00'.intval($phone_prefix).$new_phone;
					
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$agency_id = $this->user->isAgency() ? $this->agency->id : null;
			$escort_id = $session->escort_id  ? $session->escort_id : null;
			
			$results = $client->call('Escorts.existsByPhone', array($new_phone_parsed, $escort_id, $agency_id));
			
			$resCount = is_array($results) ? count($results) : 0;
			if ( $resCount > 0 ) {
				die(Cubix_I18n::translate('sys_error_phone_already_exists'));
			}
			$code = rand(1000, rand(10000,1000000));
			
			$before_data = Model_Countries::getCountryOption($session->phone_prefix_id);
			list($country_id,$phone_prefix,$ndd_prefix) = explode('-',$before_data);
			if($session->phone){
				$old_phone = preg_replace('/^'.$ndd_prefix.'/', '', $session->phone);
				$old_phone = '00'.intval($phone_prefix).$old_phone;
			}
			else{
				$old_phone = "";
			}
			
			$result = $client->call('Escorts.insertPhoneConfirm', array($escort_id,$agency_id ,$old_phone,$new_phone,$new_phone_parsed, $code,$req->prefix));
			
			if($result > 0) 
			{	
				$conf = Zend_Registry::get('system_config');
				$conf = $conf['sms'];
				$sms = new Cubix_SMS($conf['userkey'], $conf['password']);
				$sms->setOriginator(Cubix_I18n::translate('Confirm_Phone_Sms_Title'));
				$sms->setRecipient($new_phone_parsed, md5(microtime()));
				$sms->setContent($code);
				$sms->sendSMS();

                Cubix_Api::getInstance()->call('outbox', array($escort_id, $new_phone_parsed, Cubix_Application::getPhoneNumber(Cubix_Application::getId()), $code, Cubix_Application::getId()));

				die("$result");
			}
			else
				die(Cubix_I18n::translate('sys_error_resend_your_number'));
		}
		else
			die;
	}
	public function confirmSmsAction()
	{	
		$this->view->layout()->disableLayout();
		$req = $this->_request;
	
		if($req->isXmlHttpRequest() && $req->isPost()  && isset($req->code) && preg_match('/^[0-9]{4,6}$/', $req->code))
		{
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$session = new Zend_Session_Namespace('profile-data');
			$agency_id = $this->user->isAgency() ? $this->agency->id : null;
			$escort_id = $session->escort_id  ? $session->escort_id : null;
			
			$result = $client->call('Escorts.checkConfirmCode', array($escort_id,$agency_id,$req->code ));
			
			if($result['success'] == 1)
			{				
				$session = new Zend_Session_Namespace('profile-data');
				list($country_id,$phone_prefix,$ndd_prefix) = explode('-',$result['data']['country_code']);
				$data['phone_changed'] = 1;
				$data['phone_number'] = $result['data']['new_phone'];
				$data['phone_country_id'] = $country_id;
				$data['phone_exists'] = $result['data']['new_phone_parsed'];
				$data['contact_phone_parsed'] = $result['data']['new_phone_parsed'];
				if( $session->phone_prefix_id == 101 && $country_id != 101 ){
					$data['it_prefix_changed'] = 1;
				}
				$this->profile->update($data, 'contact-info');
				$session->phone_prefix_id = $country_id;
				$session->phone = $data['phone_number'];		
			}
			die("{$result['success']}");
		}
	}
	public function contactInfoAction() //Onyx
	{
		$countyModel = new Model_Countries();
		$this->view->phone_countries = $countyModel->getPhoneCountries();
		if ( $this->_posted ) { 

			$validator = new Cubix_Validator();

			$data = $this->view->data = $this->_validateContactInfo($validator);
            $this->view->phone_prefix_id = $data['phone_country_id'];
			if ( ! $validator->isValid() ) {
				$status = $validator->getStatus();

				$this->view->errors = $status['msgs'];
				return false;
			}

			$this->profile->setAvailableApps($data);
            $location_data =  array();
            $location_data['location_lat'] =  $data['location_lat'];
            $location_data['location_lng'] =  $data['location_lng'];
            $location_data['location_map_address'] = $data['location_map_address'];

            $this->view->map_location = $location_data;
			return $this->profile->update($data, 'contact-info');
		}
		else {
			$data = $this->profile->getContactInfo();
			$apps = $this->profile->getAvailableApps();
			$data = array_merge($apps, $data);

			$phone_prfixes = $countyModel->getPhonePrefixs();
			$this->view->phone_prefix_id = $data['phone_country_id'];
			/*if($data['phone_number']){
				if(preg_match('/^(\+|00)/',trim($data['phone_number'])))
				{
					$phone_prefix_id = NULL;
					$phone_number = preg_replace('/^(\+|00)/', '',trim($data['phone_number']));
					foreach($phone_prfixes as $prefix)
					{
						if(preg_match('/^('.$prefix->phone_prefix.')/',$phone_number))
						{
							$phone_prefix_id = $prefix->id;
							$data[phone_number] = preg_replace('/^('.$prefix->phone_prefix.')/', '',$phone_number);
							BREAK;
						}

					}
					$this->view->phone_prefix_id = $phone_prefix_id;
				}
			}*/
			$this->view->data = $data;


            $session = new Zend_Session_Namespace('profile-data');
			$session->phone_prefix_id = $data['phone_country_id'];
			$session->phone = trim($data['phone_number']);
			$session->escort_id = $this->escort->id;

			// REMOVE_FOR_LIVE
			if($this->testEscortId == $this->escort->id){
				if ( !$this->escort->hasProfile() ) {
					$this->view->new_escort = 1;	
				}
			}
			///
			// $agency_id = $this->user->isAgency() ? $this->agency->id : 0;

			// $client = Cubix_Api_XmlRpc_Client::getInstance();			
			// $status = $client->call('Escorts.getPhoneConfirmStatusByNumber', array($this->escort->id, $data['phone_number']));

			// if( (!isset($status) || $status == 0) && !$agency_id)
			// {
			// 	$this->view->confirmed = 0;
			// }else{
			// 	$this->view->confirmed = 1;
			// }
		}
	}

	public function galleryAction()
	{
		// $this->view->layout()->setLayout('private-v2');

		$escort_id = $this->view->escort_id;

		$this->view->user = $this->user;

		$client = new Cubix_Api_XmlRpc_Client();
		$this->view->is_message = false;
		if ( $this->_getParam('show_success') ){
			$this->view->is_message = true;
		}

		if ( $this->user->isEscort() ) {
			$escort = $this->user->getEscort();
			$escort_id = $escort->id;
		}
		else {
			$escort_id = intval($this->_getParam('escort'));

			if ( 1 > $escort_id ) die;

			if ( ! $client->call('Agencies.hasEscort', array($this->agency->getId(), $escort_id)) ) {
				die;
			}

			$model = new Model_Escorts();
			$this->escort = $escort = $model->getById($escort_id);
		}

		
		$photos = $this->_loadPhotos();
		
		$photo_ids = array();
		$soft_photos = array();
		foreach ( $photos as $photo ) {
			$photo_ids[] = intval($photo['id']);
			
			if($photo['type'] == ESCORT_PHOTO_TYPE_SOFT && $photo['is_approved'] == 1 ){
				$soft_photos[] = intval($photo['id']);
			}
			
		}
		
		$action = $this->_getParam('a');

		$photos_min_count = Cubix_Application::getById(Cubix_Application::getId())->min_photos;

		$this->view->is_private = $is_private = (bool) intval($this->_getParam('is_private'));

	
		if ( 'set-main' == $action || ! is_null($this->_getParam('set_main')) ) {
			$ids = $this->_getParam('photo_id');
			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}
			
			$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED;
			if(count($ids) == 1){
				$rotate_type = Model_Escort_Photos::PHOTO_ROTATE_SELECTED_ONE;
			}
			
			$photo_id = $client->call('Escorts.getApprovedPhoto', array($escort_id, implode(',',$ids)));
			if(!$photo_id){
				return $this->view->actionError = Cubix_I18n::translate('sys_error_not_approved_photo');
			}
			elseif ( ! in_array($photo_id, $photo_ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
			}

			$photo = new Model_Escort_PhotoItem(array('id' => $photo_id ,'escort_id' => $escort_id));
			$photo->setRotatePics($ids);
			$result = $photo->setMain();
			$client->call('Escorts.setPhotoRotateType', array($escort_id, $rotate_type));
			$escort->photo_rotate_type = $rotate_type;
			
			$this->_loadPhotos();
		}
		elseif ( 'delete' == $action || ! is_null($this->_getParam('delete')) ) {
			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}
			
			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
				}
			}
			
			$photo = new Model_Escort_Photos();
			$result = $photo->remove($ids);

			$this->_loadPhotos();
		}
		elseif ( 'upload' == $action || ! is_null($this->_getParam('upload')) ) {
				$set_photo = false;
				$config = Zend_Registry::get('images_config');
				$new_photos = array();
				$upload_errors = array();
				$model = new Model_Escort_Photos();
				

				foreach ( $_FILES as $i => $file )
				{
					try {

						if ( $is_private ){
							if ( strpos($i, 'public_') === 0 ){
								continue;
							}
						}else{
							if ( strpos($i, 'private_') === 0 ){
								continue;
							}
						}

						if ( ! isset($file['name']) || ! strlen($file['name']) ) {
							continue;
						}
						else {
							$set_photo = true;
						}

						$img_ext = strtolower(@end(explode('.', $file[name])));
						if (!in_array( $img_ext , $config['allowedExts'])){
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
						}
						
						/*$photo_count = $model->getEscortPhotoCount($escort_id);
						if ( $photo_count >= Model_Escort_Photos::MAX_PHOTOS_COUNT ){
							throw new Exception(Cubix_I18n::translate('sys_error_upload_img_count_too_much'), Cubix_Images::ERROR_IMAGE_COUNT_LIMIT_OVER);
						}*/
						
						// Save on remote storage
						$images = new Cubix_Images();
						$image = $images->save($file['tmp_name'], $escort->id, $escort->application_id, strtolower(@end(explode('.', $file['name']))));

						$image = new Cubix_Images_Entry($image);
						$image->setSize('sthumb');
						$image->setCatalogId($escort->id);
						$image_url = $images->getUrl($image);


						$image_size = getimagesize($file['tmp_name']);
						$is_portrait = 0;
						if ( $image_size ) {
							if ( $image_size[0] < $image_size[1] ) {
								$is_portrait = 1;
							}
						}

						$photo_arr = array(
							'escort_id' => $escort->id,
							'hash' => $image->getHash(),
							'ext' => $image->getExt(),
							'type' => $is_private ? ESCORT_PHOTO_TYPE_PRIVATE : ESCORT_PHOTO_TYPE_SOFT,
							'is_portrait' => $is_portrait,
							'width' => $image_size[0],
							'height' => $image_size[1],
							'creation_date' => date('Y-m-d H:i:s', time())
						);

						if ( $client->call('Escorts.isPhotoAutoApproval', array($escort_id)) ) {
							$photo_arr['is_approved'] = 1;
						}
						// commented because of approved pictures bug 
						/*else if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_PROFILE))) ) {
							$photo_arr['is_approved'] = 1;
						}*/

						$photo = new Model_Escort_PhotoItem($photo_arr);

						
						$photo = $model->save($photo);

						$new_photos[] = $photo;
					} catch ( Exception $e ) {
						$upload_errors[$i] = $file['name'] . ' (' . $e->getMessage() . ')';
					}					
					
			}

			if ( ! $set_photo ) {
				$this->view->uploadError = Cubix_I18n::translate('sys_error_select_photo');
			}

			$this->view->newPhotos = $new_photos;
			$this->view->uploadErrors = $upload_errors;
		}
		elseif ( 'set-adj' == $action ) {
			$photo_id = intval($this->_getParam('photo_id'));
			
			if ( ! in_array($photo_id, $photo_ids) ) {
				die(json_encode(array('error' => 'An error occured')));
			}

			$photo = new Model_Escort_PhotoItem(array(
				'id' => $photo_id
			));

			try {
				$hash = $photo->getHash();
				$result = array(
					'x' => intval($this->_getParam('x')),
					'y' => intval($this->_getParam('y')),
					'px' => floatval($this->_getParam('px')),
					'py' => floatval($this->_getParam('py'))
				);
				$photo->setCropArgs($result);
				
				// Crop All images
				$size_map = array(
					'backend_thumb' => array('width' => 150, 'height' => 205),
					'medium' => array('width' => 225, 'height' => 300),
					'thumb' => array('width' => 150, 'height' => 200),
					'nlthumb' => array('width' => 120, 'height' => 160),
					'sthumb' => array('width' => 76, 'height' => 103),
					'lvthumb' => array('width' => 75, 'height' => 100),
					'agency_p100' => array('width' => 90, 'height' => 120),
					't100p' => array('width' => 117, 'height' => 97)
				);
				$conf = Zend_Registry::get('images_config');

				get_headers($conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash);
				// echo $conf['remote']['url'] . "/get_image.php?a=clear_cache&app=" . Cubix_Application::getById()->host . "&eid=" . $escort_id . "&hash=" . $hash;

				$catalog = $escort_id;
				$a = array();
				if ( is_numeric($catalog) ) {
					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}
				}
				else if ( preg_match('#^([0-9]+)/(.+)$#', $catalog, $a) ) {
					array_shift($a);
					$catalog = $a[0];

					$parts = array();

					if ( strlen($catalog) > 2 ) {
						$parts[] = substr($catalog, 0, 2);
						$parts[] = substr($catalog, 2);
					}
					else {
						$parts[] = '_';
						$parts[] = $catalog;
					}

					$parts[] = $a[1];
				}

				$catalog = implode('/', $parts);

				foreach($size_map as $size => $sm) {
					get_headers($conf['remote']['url'] . "/" . Cubix_Application::getById()->host . "/" . $catalog . "/" . $hash . "_" . $size . ".jpg?args=" . $result['x'] . ":" . $result['y']);
				}
			}
			catch ( Exception $e ) {
				die(json_encode(array('error' => 'An error occured')));
			}

			die(json_encode(array('success' => true)));
		}
		elseif ( ! is_null($this->_getParam('make_private')) || ! is_null($this->_getParam('make_public')) || ! is_null($this->_getParam('make_archived')) ) {
			
			if ( ! is_null($this->_getParam('make_private')) ) {
				$type = 'private';
			} elseif ( ! is_null($this->_getParam('make_public')) ) {
				$type = 'public';
			} elseif ( ! is_null($this->_getParam('make_archived')) ) {
				$type = 'archived';
			} else {
				die;
			}

			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					return $this->view->actionError = Cubix_I18n::translate('sys_error_only_soft_photos');
				}
			}
			
			foreach ( $ids as $id ) {
				$photo = new Model_Escort_PhotoItem(array('id' => $id));
				if($type == 'private'){
					$photo->make(ESCORT_PHOTO_TYPE_PRIVATE);
				}
				elseif($type == 'archived'){ 
					if ( ! in_array($id, $soft_photos) ) {
						return $this->view->actionError = Cubix_I18n::translate('sys_error_only_soft_photos');
					}
					if ( $photo->isMain() ) {
						return $this->view->actionError = Cubix_I18n::translate('sys_error_cant_archive_main_photo');
					}
					$photo->make(ESCORT_PHOTO_TYPE_ARCHIVED);
				}
				else{
					//$photo->setNotApproved();
					$photo->make(ESCORT_PHOTO_TYPE_SOFT);
				}
			}
			
			$this->_loadPhotos();
		}
		elseif ( 'sort' == $action ) {
			$ids = $this->_getParam('photo_id');

			if ( ! is_array($ids) || ! count($ids) ) {
				die(Cubix_I18n::translate('sys_error_select_at_least_on_photo'));
			}

			foreach ( $ids as $id ) {
				if ( ! in_array($id, $photo_ids) ) {
					die(Cubix_I18n::translate('sys_error_invalid_id_photo'));
				}
			}

			$model = new Model_Escort_Photos();
			$model->reorder($ids);

			die;
		}elseif ( $this->_posted ){
			$data = array();
			$this->_redirect($this->view->getLink('private'));
			return;
		// return $this->profile->update($data, 'gallery');
		}
		$this->view->escort = $escort;
	}

	private function _loadPhotos()
	{
		$photos =

		$public_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, false));
		$nil = null;
		$private_photos = Cubix_Api::getInstance()->call('getEscortPhotosList', array($this->escort->id, true));

		return $this->view->photos = array_merge($public_photos, $private_photos);
	}



	protected function _parsePhoneNumber($phone)
	{
		$phone = preg_replace('/[^0-9]+/', '', $phone);

		if ( ! strlen($phone) || ! is_numeric($phone) ) {
			return false;
		}

		/*if ( '00' != substr($phone, 0, 2) ) {
			if ( ! is_null($escort_id) ) {
				$iso = $this->profile->country_id;
				if ( $iso && isset($this->defines['dial_codes'][$iso]) ) {
					$phone = '00' . $this->defines['dial_codes'][$iso] . $phone;
				}
			}
		}*/

		return $phone;
	}

	protected function _hasPhoneNumber($string)
	{
		$string = preg_replace('/[^0-9]+/', '', $string);

		if ( strlen($string) > 8 ) {
			return true;
		}

		return false;
	}

	protected function _validateEmailAddress($email)
	{
		return Cubix_Validator::validateEmailAddress($email);
	}

	protected function _validateWebsiteUrl($url)
	{
		//return (bool) preg_match('/^((http|https):\/\/)?(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i', $url);
		if (strtolower(substr($url, 0, 7)) != 'http://' && strtolower(substr($url, 0, 8)) != 'https://' && strtolower(substr($url, 0, 4)) != 'www.')
			return false;
		else
			return true;
	}

	public function finishAction()
	{
		$data = array();
		if ( $this->user->isAgency() ) {
			$data['agency_id'] = $this->agency->getId();
		}

		$data['user_id'] = $this->user->getId();
		
		// Send the data from session to API, a new revision of the profile will be created!
		$res = $this->profile->flush($data);

		if ($this->mode == 'create')
		{
			$client = Cubix_Api_XmlRpc_Client::getInstance();
			$es = $client->call('Escorts.getById', array($res));
			
			/****************** check cookies *************/
			$clientID = $_COOKIE['clientID'];

			if ($clientID)
			{
				$cookie_escorts = $client->call('Escorts.getSameClientIDEscorts', array($clientID, $this->user->id));

				if ($cookie_escorts)
				{
					$arr = array();

					foreach ($cookie_escorts as $ce)
					{
						$arr[] = $ce['showname'] . ' (' . $ce['id'] . ')';
					}

					$str = implode(', ', $arr);

					$body = 'New registered escort from the same computer:';
					$body .= '<br/>';
					$body .= 'Escort: ' . $es['showname'] . ' (' . $res . ')';
					$body .= '<br/><br/>';
					$body .= 'List of under investigation escorts from same computer:';
					$body .= '<br/>';
					$body .= 'Escorts: ' . $str;

					Cubix_Email::send('info@escortforum.com', 'New Registered Escort From Same Computer', $body, TRUE);
				}
			}
			/**********************************************/
		}
		
		if ( ! $res ) {
			$this->_redirect($this->view->getLink('private'));
		}

		$this->view->escort_id = $res;
	}

	public function ajaxChangeCityAction()
	{
		$this->view->layout()->disableLayout();


		$city_id = intval($this->_getParam('city_id'));
		$data = $this->profile->getWorkingCities();

		$model = new Cubix_Geography_Countries();
		if ( ! $model->hasCity($data['country_id'], $city_id) ) {
			die('Invalid city id');
		}

		$data['city_id'] = $city_id;
		foreach ( $data['cities'] as $i => $city ) {
			if ( $city_id == $city['city_id'] ) {
				$data['cities'][$i]['city_id'] = $city_id;
			}
		}


		
		$result = $this->profile->update($data, 'working-cities');

		$response = array('status' => 'error');

		if ( $result ) {
			$response['status'] = 'ok';
			$response['city'] = Cubix_Geography_Cities::getTitleById($city_id);
		}

		die(json_encode($response));
	}

	public function ajaxChangeZipAction()
	{
		$zip = $this->_getParam('zip');
		$data = $this->profile->getWorkingCities();

		if ( ! $data['incall_type'] ) {
			die('There is no need for zip code');
		}

		$validator = new Cubix_Validator();

		if ( ! $validator->validateZipCode($zip, true) ) {
			die(json_encode(array('status' => 'error')));
		}

		$data['zip'] = $zip;


		$result = $this->profile->update($data, 'working-cities');

		$response = array('status' => 'error');

		if ( $result ) {
			$response['status'] = 'ok';
			$response['zip'] = $zip;
		}

		die(json_encode($response));
	}
	

	public function gotdAction()
	{
		$this->view->layout()->setLayout('private-v2');
		$client = Cubix_Api_XmlRpc_Client::getInstance();
		$this->view->booked_days = $booked_days = $client->call('Billing.getGotdBookedDays');

		$cities = new Cubix_Geography_Cities();
		$country_id = Cubix_Application::getById()->country_id;
		
		$this->view->cities = $cities = $cities->ajaxGetAll(null, $country_id);
//		$this->view->mmg_cards = $client->call('OnlineBillingV2.getCards', array($this->user->id, 'mmgbill'));
		$this->view->payment_gateway = $this->_request->payment_gateway ? $this->_request->payment_gateway : "twispay";
		$this->view->user_id = $this->user->id;
		$is_agency = false;
		if ( $this->user->isAgency() ) {
			$is_agency = true;
			
			$escorts = $client->call('Billing.getAgencyEscortsForGotd', array($this->agency->id));
			//$escorts = $this->agency->getEscortsForGotd();
			
			$this->view->escorts = $escorts;
		} else {
			$this->view->escort_packages = $escort_packages = $client->call('Billing.getEscortPackagesForGotd', array($this->escort->id, Cubix_Application::getId()));
			$this->view->escort_package = $escort_package = $escort_packages[0];
		}
		
		$this->view->is_agency = $is_agency;
		
		$this->view->region_relations = $region_relations = array();
		
		$this->view->gotd_product = $product = $client->call('Billing.getProduct', array(15));
		if ( $this->_request->isPost() ) {
			$this->view->card = $this->_request->card;
			
			$errors = array();
			$data = array(
				'city_id'	=> $this->_request->city_id,
				'escort_id' => $this->_request->escort_id,
				'days'		=> $this->_request->days,
				'package'	=> $this->_request->package
			);
			
			if ( ! $is_agency && count($escort_packages) == 1 ) {
				$data['package'] = 0;
			}
			
			if ( ! $data['city_id'] ) {
				$errors['city_id'] = Cubix_I18n::translate('city_required');
			} else {
				$city_also_check[] = $data['city_id'];
				if ( isset($region_relations[$data['city_id']]) ) {
					$city_also_check = $region_relations[$data['city_id']];
				}
			}
			$this->view->city_also_check = $city_also_check;
			
			if ( ! strlen($data['package']) ) {
				$errors['package'] = Cubix_I18n::translate('selecte_package');
			} else {
				
				if ( $is_agency ) {
					
				} else {
					$this->view->escort_package = $escort_package = $escort_packages[$data['package']];
				}
			}
			
			
			
			if ( $is_agency ) {
				if ( ! $data['escort_id'] ) {
					$errors['escort_id'] = Cubix_I18n::translate('escort_required');
				}
			} else {
				$data['escort_id'] = $this->escort->id;
			}
			
			
			$selected_days = array();
			if ( $data['days'] ) {
				$selected_days = explode(';', $data['days']);
			}
			if ( ! count($selected_days) ) {
				$errors['days'] = Cubix_I18n::translate('date_required');
			} else {
				foreach($selected_days as $k => $day) {
					$day = explode(',', $day);
					//set to 5 to avoid daylight saving
					$selected_days[$k] = mktime(5,0,0, $day[1], $day[2], $day[0]);
				}
			}
			
			if ( count($selected_days) > 2 && $this->_request->payment_gateway == "mmgbill" ) {
				$errors['days'] = Cubix_I18n::translate('gotd_days_limit_per_transaction');
			}
			
			foreach($selected_days as $day) {
				//Checking date to be not booked
				if ( $data['city_id'] ) {
					foreach($city_also_check as $ct) {
						foreach($booked_days[$ct] as $dt) {
							if ( date('Y-m-d', $day) == date('Y-m-d', strtotime($dt['date'])) ) {
								$errors['days'] = Cubix_I18n::translate('day_already_booked');
								break;
							}
						}
					}
				}
				
				//Escort could have only one package per day
				if ( $data['city_id'] ) {
					$escort_gotds = $client->call('Billing.getEscortGotdBookedDays', array($data['escort_id']));

					foreach($escort_gotds as $gotd) {
						if ( date('Y-m-d', $day) == date('Y-m-d', strtotime($gotd['date'])) ) {
							$errors['days'] = Cubix_I18n::translate('only_one_gotd_per_day');
						}
					}
				}
				
				//Checking date to be in package active range
				if ( $data['escort_id'] ) {
					if ( $is_agency ) {
						$package = $escorts[$data['escort_id']]['packages'][$data['package']];
						if ( $day < $package['date_activated'] || $day > $package['expiration_date']  ) {
							$errors['days'] = Cubix_I18n::translate('date_must_be_in_package_range');
						}
					}  else {
						//If not in range of active and pending packages
						//add 5 hours to avoid daylight saving
						
						$escort_package['expiration_date'] += 5 * 60 * 60;
						if ( ! ( $day >= $escort_package['date_activated'] && $day <= $escort_package['expiration_date'] ) ) {
							$errors['days'] = Cubix_I18n::translate('date_must_be_in_package_range');
						}
					}
				}
			}
			
			
			$this->view->data = $data;
			$this->view->selected_days_count = $selected_days_count = count($selected_days);


			if ( count($errors) ) {
				$this->view->gotd_errors = $errors;

                if($this->getRequest()->isXmlHttpRequest()) {
                    exit(json_encode([
                        'status' => 'error',
                        'msgs' => $errors
                    ]));
                }

			} else {
				$book_id = $client->call('Billing.bookGotd', array($data['escort_id'], $data['city_id'], $selected_days, Model_Users::getCurrent()->clientID, Cubix_Geoip::getIP()));
				
				$amount = $selected_days_count * $product['price'];
				
				if ( $book_id ) {
					try {
						if ( $this->_request->payment_gateway == 'mmgbill' ) {
							$mmgBill = new Model_MmgBillAPIV2();
							$book_id = str_replace(':', 'x', $book_id);
							
							if ( $this->_request->card && strlen($this->_request->card) ) {
								$result = $mmgBill->charge($amount, 'G' . $book_id, $this->_request->card);
								if ( $result['success'] ) {
									$ch = curl_init('backend.escortforumit.xxx/billing/online-billing/mmg-bill-callback');
									$data = array(
										'txn_status' => 'APPROVED',
										'is_oneclick' => 1,
										'ti_mmg' => $result['ti_mmg'],
										'ti' => 'GZ' . $book_id,
										'ia' => $amount,
										'prev_ti_mmg'	=> $this->_request->card
									);
									
									curl_setopt($ch, CURLOPT_POST, 1);
									curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
									$response = curl_exec($ch);
									curl_close($ch);

									$this->_redirect($this->view->getLink('ob-successful'));
								} else {
									$this->_redirect($this->view->getLink('ob-unsuccessful'));
								}
							}

							$hosted_url = $mmgBill->getHostedPageUrl($amount, 'GZ' . $book_id, $this->_request->save_info, 'https://www.escortforumit.xxx' . $this->view->getLink('ob-mmg-postback'));
							
							$this->_redirect($hosted_url);
						}
						elseif( $this->_request->payment_gateway == 'ecardon')
						{
							$TokenParams = array(
								'amount' => $amount,
								'descriptor' => 'gotd-' . $book_id,
								'merchantTransactionId' => 'gotd-' . $book_id,
							);

							$ecardon_payment = new  Model_EcardonGateway($TokenParams);

							$token_data = $ecardon_payment->checkout();
							$token_data_dec = json_decode($token_data);

							if(isset($token_data_dec->result) && $token_data_dec->result->code == '000.200.100'){
								$token =  preg_replace('#[^a-zA-Z0-9.\-]#', '', $token_data_dec->id);
								$client->call('OnlineBillingV2.storeToken', array($token, $this->user->id, 'ecardon'));
								$this->view->checkout_id = $token_data_dec->id;
							}
							else{
								die(json_encode(array('status' => 'error' )));
							}
						} 
						elseif ( $this->_request->payment_gateway == 'netbanx' ) {
							try {
								$reference = 'gotd-' . $book_id;
								$nbx = new Model_NetbanxAPI();
								$order_data = array(
									'total_amount'	=> $amount * 100,
									'currency_code'	=> 'EUR',
									'merchant_ref_num'	=> $reference,
									'locale' => 'en_US',
									// 'profile'	=> array(
									// ),
									'callback'	=> array( 
										array(
											'format'		=> 'json',
											'rel'			=> 'on_success',
											'retries'		=> 6,
											'returnKeys'	=> array('id', 'transaction.amount', 'transaction.authType', 'transaction.status', 'transaction.currencyCode', 'transaction.merchantRefNum', 'transaction.confirmationNumber', 
												'transaction.card.brand', 'transaction.card.country', 'transaction.card.expiry', 'transaction.card.lastDigits'
											),
											'synchronous'	=> true,
											'uri'			=> APP_HTTP.'://backend.escortforumit.xxx/billing/online-billing/netbanx-callback-gotd'
										)
									),
									'redirect'	=> array(
										array(
											'rel'	=> 'on_success',
											'returnKeys'	=> array('id'),
											'uri'	=> APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-successful')
										)
									),
									'link'		=> array(
										array(
											'rel'	=> 'cancel_url',
											'uri'	=> APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('private-v2-gotd')
										),
										array(
											'rel'	=> 'return_url',
											'uri'	=> APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('private-v2-gotd')
										)
									)
								);

								// $profile_id = $client->call('OnlineBillingV2.getNetbanxProfileId', array($this->user->id));

								// if ( $profile_id ) {
								// 	$order_data['profile']['id'] = $profile_id;
								// } else {
								// 	$order_data['profile']['merchantCustomerId'] = 'ef' . $this->user->id;
								// }

								$order = $nbx->create_order($order_data);

								$this->_redirect($order->link[0]->uri);
							} catch(Exception $e) {
								$message = $e->getMessage();
								file_put_contents('/tmp/netbanx_error.log', "\n" . 'UserId: ' . $this->user->id . "\n" . var_export($message, true), FILE_APPEND);
								/*Problem with onsaved profile in test account for netbanx
								 * throw error existing merchantCustomerId
								 * getting that profile id and trying to creat order again
								 * can be removed later. 05.12.2014
								 */
								if ( strpos($message, 'Duplicate merchantCustomerId') !== false ) {
									$profile_id = str_replace('The merchantCustomerId provided for this profile has already been used for another profile - ', '', $message);
									file_put_contents('/tmp/netbanx_error.log', "\n" . 'UserId: ' . $this->user->id . ', New profileId: ' . $profile_id, FILE_APPEND);
									unset($order_data['profile']['merchantCustomerId']);
									$order_data['profile']['id'] = $profile_id;
									$order = $nbx->create_order($order_data);
									$this->_redirect($order->link[0]->uri);
								}
								
								
								$this->_redirect('http://www.escortforumit.xxx' . $this->view->getLink('ob-unsuccessful'));
							}
						}
						elseif( $this->_request->payment_gateway == '2000charge')
						{
							$first_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->username)); 
							$last_name = trim(preg_replace('#[^a-zA-Z]#', ' ', $this->user->chat_info['nickName']));
							
							if( empty($first_name)){
								$first_name = 'NAME';
							}

							if(empty($last_name)){
								$last_name = 'LASTNAME';
							}
							
							$reference = 'gotd-' . $book_id;
							
							$customer = new Cubix_2000charge_Model_Customer();
							$customer->setEmail($this->user->email);
							$customer->setCountry("IT");
							$customer->setFirstName($first_name);
							$customer->setLastName($last_name);

							$payment = new Cubix_2000charge_Model_Payment();
							$payment->setPaymentOption("paysafe");
							$payment->setHolder($first_name.' '.$last_name);

							$transaction = new Cubix_2000charge_Model_Transaction();
							$transaction->setCustomer($customer);
							$transaction->setPayment($payment);
							$transaction->setAmount($amount * 100);
							$transaction->setCurrency("EUR");
							$transaction->setIPAddress(Cubix_Geoip::getIP());
							$transaction->setMerchantTransactionId($reference);

							$host = 'https://' . $_SERVER['SERVER_NAME'];
							$redirectUrls = new Cubix_2000charge_Model_RedirectUrls();
							$redirectUrls->setReturnUrl($host . $this->view->getLink('ob-successful'));
							$redirectUrls->setCancelUrl($host .$this->view->getLink('ob-unsuccessful'));
							$transaction->setRedirectUrls($redirectUrls);

							$res = Cubix_2000charge_Transaction::post($transaction);
							$client->call('OnlineBillingV2.storeToken', array($res->id, $this->user->id, '2000charge'));
							$this->_redirect($res->redirectUrl);
						}
						elseif ($this->_request->payment_gateway == 'coinsome') {
						
							$amount = $selected_days_count * $product['crypto_price'];
							$order_id = $this->client->call('OnlineBillingV2.saveCoinsomeRequest', array($this->user->id, 'gotd', $amount, 'gotd', $book_id));

							$coinsome_model = new Cubix_Coinsome();
							$paymentFrom = '
									<form id="payment-form" method="POST" name="criptoForm" action="'. $coinsome_model->generateFormUrl($order_id) .'">
										<input type="hidden" name="language" value="'. Cubix_I18n::getLang() . '"/>
										<input type="hidden" name="amount" value="'. $amount .'" />
									</form>';

							die(json_encode(array('status' => 'success', 'form' => $paymentFrom), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES ));
						}
					
                        elseif( $this->_request->payment_gateway == 'twispay')
                        {
                            $paymentConfigs = Zend_Registry::get('payment_config');
                            $twispayConfigs = $paymentConfigs['twispay'];

                            if (APPLICATION_ENV == 'development' || in_array($this->user->id, [345573, 250871])) {
                                $amount = 1;
                            }

                            $reference = 'gotd-' . $this->user->id . '-' . time();
                            $data = array(
                                'siteId' => intval($twispayConfigs['siteid']),
                                'cardTransactionMode' => 'authAndCapture',
                                'backUrl' => APP_HTTP . '://' . $_SERVER['HTTP_HOST'] . '/online-billing-v2/twispay-response',
                                'invoiceEmail' => '',
                                'customer' => [
                                    'identifier' => 'user-' . $this->user->id,
                                    'firstName' => $this->user->username,
                                    'username' => $this->user->username,
                                    'email' => $this->user->email,
                                ],
                                'order' => [
                                    'orderId' => $reference,
                                    'type' => 'purchase',
                                    'amount' => $amount,
                                    'currency' => $twispayConfigs['currency'],
                                    'description' => $book_id,
                                ]
                            );

                            // Store the gateway token for backend to get details about the transaction
                            // using this information
                            $token = json_encode(['externalOrderId' => $data['order']['orderId']]);
                            $isOrderCreated = $this->client->call('OnlineBillingV2.storeToken', array($token, $this->user->id, 'twispay'));

                            if (!$isOrderCreated) {
                                die(json_encode([
                                    'status' => 'error',
                                    'error'  => 'Twispay is not available at the moment',
                                ]));
                            }

                            $base64JsonRequest = Cubix_Twispay_TwispayApi::getBase64JsonRequest($data);
                            $base64Checksum = Cubix_Twispay_TwispayApi::getBase64Checksum($data, $twispayConfigs['key']);

                            try {
                                $paymentFrom = "
                                <form id=\"payment-form\" action='{$twispayConfigs['url']}' method='post' accept-charset='UTF-8'>
                                    <input type='hidden' name='jsonRequest' value = \"{$base64JsonRequest}\" >
                                    <input type='hidden' name='checksum' value = \"{$base64Checksum}\" >
                                    <input type = \"submit\" value = \"Pay\" >
                                </form > ";

                            } catch(Exception $e) {
                                $message = $e->getMessage();
                                die(json_encode(array('status' => 'success', 'url' => APP_HTTP.'://www.escortforumit.xxx' . $this->view->getLink('ob-unsuccessful'))));
                            }
                            die(json_encode(array('status' => 'success', 'form' => $paymentFrom), JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES ));
                        }
						elseif($this->_request->payment_gateway == 'powercash')
						{
						
							$reference = 'gotd-' . $book_id;
							$params = array(
								'amount' => $amount,
								'orderid' => $reference,
								'email' => $this->user->email,
								'customerip' => Cubix_Geoip::getIP(),
								'url_return' =>  $this->view->getLink('paycash-response')
							);

							$model_powercache = new Model_PowerCashGateway($params);
							$response = $model_powercache->checkout();

							$this->client->call('OnlineBillingV2.storeStatus', array($response['transactionid'], $this->user->id, serialize($model_powercache->returnStoreData())));
                            setcookie('pwcsh_page', 'private-v2-gotd', time() + (86400 * 30), "/");
							echo json_encode($response); die;
						}
                    } catch(Exception $ex) {
						var_dump($ex);die;
					}
				}
			}
		}
	}

	public function getChangePhoneViewAction()
	{
		$this->view->layout()->disableLayout();
	}
	public function gotdSuccessAction()
	{
		$this->view->layout()->setLayout('private-v2');
		$this->view->layout()->global_btn_back_url = self::$linkHelper->getLink('private-v2');
		$this->_helper->viewRenderer->setScriptAction('gotd-responses');
		$this->view->key = 'gotd_success_message';
	}
	
	public function gotdFailureAction()
	{
		$this->view->layout()->setLayout('private-v2');
		$this->view->layout()->global_btn_back_url = self::$linkHelper->getLink('private-v2-gotd');
		$this->_helper->viewRenderer->setScriptAction('gotd-responses');
		$this->view->key = 'gotd_failure_message';
	}

	public function isCheckedPremiumCity($primaryCityId,$CitiesArray){
        foreach ($primaryCityId as $city_id){
            foreach ($CitiesArray as $city) {
                if($city['city_id'] == $city_id){
                    return true;
                }
            }
        }

        return false;
    }

    public function checkSymbols($text){
        $matches = array();
        //allow italian letters
        $withoutSymbols =  array('&nbsp;','&ograve;','&agrave;','&egrave;',
            '&ugrave;','&igrave;','&ccedil;','&deg;','&deg;','&eacute;','&pound;','&amp;','&sect;',
            '&Ugrave;','&Egrave;','&Ograve;','&Eacute;','&Agrave;','&Igrave;','&rsquo;','&acute;',
            '&oacute;','&euro;','&mdash;','&ndash;','&raquo;','&laquo;','&rdquo;','&ldquo;','&hellip;','&aacute;',
            '&gt;','&lt;','&iacute;','&Iacute;','&Icirc;','&icirc;','&iuml;','&Iuml;','&ocirc;','&Ocirc;','&ucirc;','&Ucirc;','&acirc;','&Acirc;',
            '&ecirc;','&Ecirc;','&bull;'
            );
        $t = preg_match_all('/&(.*?){2,8}\;/is', $text, $matches);

        foreach ($withoutSymbols as $key => $value){
            foreach ($matches[0] as $k => $v){
                if($v == $value){
                    unset($matches[0][$k]);
                }
            }
        }

        return implode(' ',$matches[0]);
    }

    public function stripAsci($text){
        $text = str_replace("\xe2\x80\xa8", '<br>', $text);
        $text = str_replace("\xe2\x80\xa9", '<br>', $text);

        $data = preg_replace('#&(.*?){2,8};#si', '',$text );
        $data = str_replace('и','',$data);
        $data = str_replace('я','',$data);
        $data = str_split($data);

        foreach ($data as $key => $value ){
                if(in_array(ord($value),[239,143,184])){
                    unset($data[$key]);
                }
        }

        return implode('',$data);
    }

    public function completeProfileAction(){}

    public function biographyStepAction(){



        if ( $this->current_step == 'biography-step') {
            $validator = new Cubix_Validator();

            $this->view->data = $data = $this->_validateBiography($validator);

            if ( $this->_getParam('dont') ) {
                return false;
            }

            $response = array(
                'success' => false,
                'errors'  => ''
            );

            if ( ! $validator->isValid() ) {
                $status = $validator->getStatus();

                $response['errors'] =  $status['msgs'];

                die(json_encode($response));

            }else{
                $this->view->layout()->disableLayout();
                $this->profile->update($data, 'biography');
                $this->aboutMeStepAction();
            }
        }


        $this->view->data = $this->profile->getBiography();

        if($this->_request->isPost()){
            $response['success'] = true;
            $response['data'] = $this->view->render('private/profile/biography-step.phtml');
            die(json_encode($response));
        }

    }

    public function aboutMeStepAction(){


        $validator = new Cubix_Validator();

        if ( $this->current_step == 'about-me-step'){

            if($this->_getParam('then') == ':back'){
                $this->biographyStepAction();
            }

            $blackListModel = new Model_BlacklistedWords();
            $dataAboutMe = $this->_validateAboutMe($validator);
            $dataLanguages = $this->_validateLanguages($validator);
            $this->view->data = $data = array_merge($dataAboutMe,$dataLanguages);

            $data['about_has_bl'] = 0;

            $response = array(
                'success' => false,
                'errors'  => ''
            );

            if ( ! $validator->isValid() ) {

                $status = $validator->getStatus();

                $response['errors'] = $status['msgs'];

                die(json_encode($response));

            }else{
                $this->profile->update($dataAboutMe, 'about-me');
                $this->profile->update($dataLanguages, 'languages');
                $this->profile->load();
                $this->workingCitiesStepAction();
            }

        }

            $this->view->data = array_merge($this->profile->getAboutMe(),$this->profile->getLanguages());


        $response['success'] = true;
        $response['data'] = $this->view->render('private/profile/about-me-step.phtml');
        die(json_encode($response));
    }

    public function workingCitiesStepAction(){


        $cities = new Cubix_Geography_Cities();
        $country_id = Cubix_Application::getById()->country_id;


        if ( $this->current_step == 'working-cities-step' ) {

            if($this->_getParam('then') == ':back'){
                $this->aboutMeStepAction();
            }

            $validator = new Cubix_Validator();

            $this->view->data = $data = $this->_validateWorkingCities($validator);

            $response = array(
                'success' => false,
                'errors'  => ''
            );

            if ( ! $validator->isValid() ) {
                $status = $validator->getStatus();

                $response['errors'] = $status['msgs'];
                die(json_encode($response));
            }else{

                $data['cities'][] = array('city_id'=>$data['city_id']);

                $this->profile->update($data, 'working-cities');
                $this->servicesStepAction();

            }

        }

        $data = $this->profile->getWorkingCities();
        $this->view->cities = $cities->ajaxGetAll(null, $country_id);
        $this->view->data = $data;

        $response['success'] = true;
        $response['data'] = $this->view->render('private/profile/working-cities-step.phtml');
        die(json_encode($response));
    }

    public function servicesStepAction(){

        if ( $this->current_step == 'services-step' ) {

            if($this->_getParam('then') == ':back'){
                $this->workingCitiesStepAction();
            }

            $validator = new Cubix_Validator();

            $this->view->data = $data = $this->_validateServices($validator);
            $response = array(
                'success' => false,
                'errors'  => ''
            );
            if ( ! $validator->isValid() ) {
                $status = $validator->getStatus();

                $response['errors'] = $status['msgs'];
                die(json_encode($response));
            }

            $this->profile->update($data, 'services');
            $this->workingTimesStepAction();
        }

        $this->view->data = $this->profile->getServices();
        $response['success'] = true;
        $response['data'] = $this->view->render('private/profile/services-step.phtml');
        die(json_encode($response));
    }

    public function workingTimesStepAction(){

        if($this->current_step == 'working-times-step'){

            if($this->_getParam('then') == ':back'){
                $this->servicesStepAction();
            }

            $validator = new Cubix_Validator();

            $this->view->data = $data = $this->_validateWorkingTimes($validator);

            $response = array(
                'success' => false,
                'errors'  => ''
            );

            if ( ! $validator->isValid() ) {

                $status = $validator->getStatus();

                $response['errors'] = $status['msgs'];
                die(json_encode($response));
            }

            if(count($data['times']) > 0){
                unset($data['available_24_7']);
            }

            if(count($this->profile->times) > 0 && count($data['times']) == 0){
                unset($data['times']);
            }

            $this->profile->update($data, 'working-times');
            $this->contactInfoStepAction();
        }

        $data = $this->profile->getWorkingTimes();
        $this->view->data = $data;


        $response['success'] = true;
        $response['data'] = $this->view->render('private/profile/working-times-step.phtml');
        die(json_encode($response));
    }

    public function contactInfoStepAction(){
    	
        $countyModel = new Model_Countries();
        $this->view->phone_countries = $countyModel->getPhoneCountries();

        if ( $this->current_step == 'contact-info-step' ) {

            if($this->_getParam('then') == ':back'){
                $this->workingTimesStepAction();
            }

            $validator = new Cubix_Validator();

            $data = $this->view->data = $this->_validateContactInfo($validator);
            $this->view->phone_prefix_id = $data['phone_country_id'];

            $response = array(
                'success' => false,
                'errors'  => ''
            );

            if ( ! $validator->isValid() ) {
                $status = $validator->getStatus();

                $response['errors'] = $status['msgs'];
                die(json_encode($response));
            }

            $this->profile->setAvailableApps($data);
            $this->profile->update($data, 'contact-info');

            // Wizard // remove Wizard not completed trigger 
        	if (!$this->user->isAgency()) {
	            $et = new Cubix_EmailTrigger($this->user->getId(), 6);
				$et->removeFromDeferred();
        	}
            
            $this->finishAction();
            die(json_encode(array('success'=>'finish','escort_id' => $this->view->escort_id)));

        }

            $data = $this->profile->getContactInfo();
            $apps = $this->profile->getAvailableApps();
            $data = array_merge($apps, $data);
            $phone_prfixes = $countyModel->getPhonePrefixs();
            $this->view->phone_prefix_id = $data['phone_country_id'];

            $this->view->data = $data;
            $session = new Zend_Session_Namespace('profile-data');
            $session->phone_prefix_id = $data['phone_country_id'];
            $session->phone = trim($data['phone_number']);
            $session->escort_id = $this->escort->id;



            $response['success'] = true;
            $response['data'] = $this->view->render('private/profile/contact-info-step.phtml');
            die(json_encode($response));
    }

    public function galleryStepAction()
    {
        $this->view->layout()->setLayout('profile-steps');
        $photos = $this->_loadPhotos();
        $client = Cubix_Api_XmlRpc_Client::getInstance();

        $photo_ids = array();
        $soft_photos = array();
        foreach ($photos as $photo) {
            $photo_ids[] = intval($photo['id']);

            if ($photo['type'] == ESCORT_PHOTO_TYPE_SOFT && $photo['is_approved'] == 1) {
                $soft_photos[] = intval($photo['id']);
            }

        }

        if ( $this->user->isEscort() ) {
            $escort = $this->user->getEscort();
            $this->view->escort_id = $escort_id = $escort->id;
        }
        else {
            $this->view->escort_id = $escort_id = intval($this->_getParam('escort'));

            if ( 1 > $escort_id ) die;


            $model = new Model_Escorts();
            $this->escort = $escort = $model->getById($escort_id);
        }

        if (!is_null($this->_getParam('delete'))) {
            $ids = $this->_getParam('photo_id');

            if (!is_array($ids) || !count($ids)) {
                return $this->view->actionError = Cubix_I18n::translate('sys_error_select_at_least_on_photo');
            }

            foreach ($ids as $id) {
                if (!in_array($id, $photo_ids)) {
                    return $this->view->actionError = Cubix_I18n::translate('sys_error_invalid_id_photo');
                }
            }

            $photo = new Model_Escort_Photos();
            $photo->remove($ids);
        } else if (!is_null($this->_getParam('upload'))) {


            $set_photo = false;
            $config = Zend_Registry::get('images_config');
            $new_photos = array();
            $upload_errors = array();
            $model = new Model_Escort_Photos();

            foreach ($_FILES as $i => $file) {
                try {
                    if ($this->_getParam('is_private')) {
                        if (strpos($i, 'public_') === 0) {
                            continue;
                        }
                    } else {
                        if (strpos($i, 'private_') === 0) {
                            continue;
                        }
                    }

                    if (!isset($file['name']) || !strlen($file['name'])) {
                        continue;
                    } else {
                        $set_photo = true;
                    }

                    $img_ext = strtolower(@end(explode('.', $file[name])));
                    if (!in_array($img_ext, $config['allowedExts'])) {
                        throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
                    }


                    // Save on remote storage
                    $images = new Cubix_Images();
                    $image = $images->save($file['tmp_name'], $this->escort->id, $this->escort->application_id, strtolower(@end(explode('.', $file['name']))));

                    $image = new Cubix_Images_Entry($image);
                    $image->setSize('sthumb');
                    $image->setCatalogId($this->escort->id);
                    $image_url = $images->getUrl($image);


                    $image_size = getimagesize($file['tmp_name']);
                    $is_portrait = 0;
                    if ($image_size) {
                        if ($image_size[0] < $image_size[1]) {
                            $is_portrait = 1;
                        }
                    }

                    $photo_arr = array(
                        'escort_id' => $this->escort->id,
                        'hash' => $image->getHash(),
                        'ext' => $image->getExt(),
                        'type' => ESCORT_PHOTO_TYPE_SOFT,
                        'is_portrait' => $is_portrait,
                        'width' => $image_size[0],
                        'height' => $image_size[1],
                        'creation_date' => date('Y-m-d H:i:s', time())
                    );

                    if ($client->call('Escorts.isPhotoAutoApproval', array($this->escort->id))) {
                        $photo_arr['is_approved'] = 1;
                    }
                    // commented because of approved pictures bug
                    /*else if ( $client->call('Escorts.hasStatusBit', array($escort_id, array(Model_Escorts::ESCORT_STATUS_NO_PROFILE))) ) {
                        $photo_arr['is_approved'] = 1;
                    }*/

                    $photo = new Model_Escort_PhotoItem($photo_arr);


                    $photo = $model->save($photo);

                    $new_photos[] = $photo;
                } catch (Exception $e) {
                    $upload_errors[$i] = $file['name'] . ' (' . $e->getMessage() . ')';
                }
            }

            if (!$set_photo) {
                $upload_errors[] = Cubix_I18n::translate('sys_error_select_photo');
            }

            $this->view->newPhotos = $new_photos;
            $this->view->uploadErrors = $upload_errors;

        }

        $this->_loadPhotos();

    }

    public function finishStepAction(){
        $this->view->layout()->setLayout('profile-steps');
        $this->view->escort_id = $escort_id =  intval($this->_getParam('escort'));

        if (1 > $escort_id) return $this->_redirect($this->view->getLink('private-v2'));


    }

    /*
     * Age Verification and 100% verified process
     *
     * Ticket - EFNET-1083
     *
     * */

    public function ageVerificationAction()
    {
        $this->view->layout()->setLayout('profile-steps');

        if ($this->_request->isPost()) {

            $form = new Cubix_Form_Data($this->_request);
            $fields = array(
                'name' => 'notags|special',
                'last_name' => 'notags|special',
                'day' => 'int',
                'month' => 'int',
                'year' => 'int',
                'real-photos' => 'int'
            );

            $form->setFields($fields);
            $data = $form->getData();

            if ($this->user->isAgency()) {
                $data['escort_id'] = intval($this->_request->escort_id);
            } else {
                $escort_data = $this->user->getEscort();
                $data['escort_id'] = $escort_data->id;
            }

            $this->view->name = $data['name'];
            $this->view->last_name = $data['last_name'];
            $this->view->day = $data['day'];
            $this->view->month = $data['month'];
            $this->view->year = $data['year'];
            $this->view->real_pics = $data['real-photos'];

            $images = new Cubix_Images();

            $config = Zend_Registry::get('images_config');
            $video_config = Zend_Registry::get('videos_config');
            $validator = new Cubix_Validator();

            try {

                if (!$data['escort_id']) {
                    throw new Exception('Please Select Escort', Cubix_Images::ERROR_IMAGE_INVALID);
                }
                if (!$data['name']) {
                    throw new Exception('Please Select Name', Cubix_Images::ERROR_IMAGE_INVALID);
                }
                if (!$data['last_name']) {
                    throw new Exception('Please Select Last Name', Cubix_Images::ERROR_IMAGE_INVALID);
                }
                if (!$data['day'] || !$data['month'] || !$data['year']) {
                    throw new Exception('Please Select Born Date', Cubix_Images::ERROR_IMAGE_INVALID);
                }

                $data['name'] = preg_replace('/(\s)+/', '$1', $data['name']);
                $data['last_name'] = preg_replace('/(\s)+/', '$1', $data['last_name']);


                if ($_FILES['image1']["tmp_name"] == "" || $_FILES['image2']["tmp_name"] == "") {
                    throw new Exception('Please upload both images', Cubix_Images::ERROR_IMAGE_INVALID);
                }


                $ext1 = explode('.', $_FILES['image1']['name']);
                $ext1 = strtolower(end($ext1));
                $ext2 = explode('.', $_FILES['image2']['name']);
                $ext2 = strtolower(end($ext2));

                if (!in_array($ext1, $config['allowedExts']) || !in_array($ext2, $config['allowedExts'])) {
                    throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
                }


                if ($_FILES['video']['tmp_name'] != '') {

                    $video_ext = explode('.', $_FILES['video']['name']);
                    $video_ext = strtolower(end($video_ext));

                    $allowed_video_formats = array('flv', 'wmv', '3gp', 'mkv', 'mp4', 'avi', 'mov');
                    if (!in_array($video_ext, $allowed_video_formats)) {
                        throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $allowed_video_formats))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
                    } elseif (number_format(($_FILES['video']['size'] / pow(1024, 2)), 2) > 10) {
                        throw new Exception(Cubix_I18n::translate('sys_error_upload_video_allowed_max_size', array('size' => '10 MB')), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
                    }

                    $video = $_FILES['video']['tmp_name'];

                    $video_ftp = new Cubix_VideosCommon();
                    $video_hash = uniqid() . '_age_certify';

                    $video_model = new Cubix_ParseVideo($video, $data['escort_id'], $video_config);

                    $video = $video_model->ConvertToMP4();
                    $video_ext = 'mp4';

                    $name = $video_hash . '.' . $video_ext;
                    if ($video_ftp->_storeToPicVideo($video, $data['escort_id'], $name)) {
                        $data['photos'][] = array('hash' => $video_hash, 'ext' => $video_ext, 'is_video' => 1);
                    }

                }


                $image1 = $_FILES['image1']['tmp_name'];
                $image2 = $_FILES['image2']['tmp_name'];
                $data['photos'][] = $images->save($image1, $data['escort_id'] . '/age-verification', $this->escort->application_id, $ext1);
                $data['photos'][] = $images->save($image2, $data['escort_id'] . '/age-verification', $this->escort->application_id, $ext2);

                $verify_photos[] = $images->save($image1, $data['escort_id'] . '/verify', $this->escort->application_id, $ext1);
                $verify_photos[] = $images->save($image2, $data['escort_id'] . '/verify', $this->escort->application_id, $ext2);


            } catch (Exception $e) {
                $error = '';
                switch ($e->getCode()) {
                    case Cubix_Images::ERROR_IMAGE_INVALID:
                        $error = $e->getMessage();
                        break;
                    case Cubix_Images::ERROR_IMAGE_TOO_BIG:
                        $error = $e->getMessage();
                        break;
                    case Cubix_Images::ERROR_IMAGE_TOO_SMALL:
                        $error = $e->getMessage();
                        break;
                    case Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED:
                        $error = $e->getMessage();
                        break;
                    default:
                        $error = 'An unexpected error occured when uploading file';
                }

                $validator->setError('photos', $error);

            }

            if ($data['real-photos']) {
                if (count($_FILES['pic']['name']) > 0) {
                    if (!isset($_FILES['pic']['tmp_name'][0]) || !strlen($_FILES['pic']['tmp_name'][0])) {
                        $validator->setError('natural-photo', 'Please upload photo with sheet of paper ');
                    } else {

                        foreach ($_FILES['pic']['name'] as $index => $pic) {
                            if (
                                !isset($_FILES['pic']['tmp_name'][$index]) ||
                                !strlen($_FILES['pic']['tmp_name'][$index]) ||
                                !isset($pic) ||
                                !strlen($pic)
                            ) {
                                BREAK;
                            }

                            $ext = explode('.', $pic);
                            $ext = strtolower(end($ext));
                            try {
                                $file = $_FILES['pic']['tmp_name'][$index];
                                if (!in_array($ext, $config['allowedExts'])) {
                                    throw new Exception(Cubix_I18n::translate('sys_error_upload_img_allowed_extensions', array('extensions' => implode(', ', $config['allowedExts']))), Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED);
                                }
                                $photos[$index] = $images->save($file, $data['escort_id'] . '/verify', $this->escort->application_id, $ext);

                            } catch (Exception $e) {
                                $error = '';

                                switch ($e->getCode()) {
                                    case Cubix_Images::ERROR_IMAGE_INVALID:
                                        $error = $e->getMessage();
                                        break;
                                    case Cubix_Images::ERROR_IMAGE_TOO_BIG:
                                        $error = $e->getMessage();
                                        break;
                                    case Cubix_Images::ERROR_IMAGE_TOO_SMALL:
                                        $error = $e->getMessage();
                                        break;
                                    case Cubix_Images::ERROR_IMAGE_EXT_NOT_ALLOWED:
                                        $error = $e->getMessage();
                                        break;
                                    default:
                                        $error = 'An unexpected error occured when uploading file';
                                }

                                $validator->setError('natural-photo', $error);
                                continue;
                            }
                        }


                        if ($_FILES['natural-video']["tmp_name"] != "") {

                            $video_ext = explode('.', $_FILES['natural-video']['name']);
                            $video_ext = strtolower(end($video_ext));

                            $allowed_video_formats = array('flv', 'wmv', '3gp', 'mkv', 'mp4', 'avi', 'mov');
                            if (!in_array($video_ext, $allowed_video_formats)) {
                                $validator->setError('natural-video', Cubix_I18n::translate('sys_error_upload_video_allowed_extensions', array('formats' => implode(', ', $allowed_video_formats))));
                            } elseif (number_format(($_FILES['natural-video']['size'] / pow(1024, 2)), 2) > intval($video_config['VideoMaxSize'])) {
                                $validator->setError('natural-video', Cubix_I18n::translate('sys_error_upload_video_allowed_max_size', array('size' => $this->config['VideoMaxSize'])));
                            }

                            if ($validator->isValid()) {

                                $video_ftp = new Cubix_VideosCommon();
                                $video_hash = uniqid() . '_idcard';
                                $video = $_FILES['natural-video']['tmp_name'];

                                $video_model = new Cubix_ParseVideo($video, $data['escort_id'], $video_config);

                                $video = $video_model->ConvertToMP4();
                                $video_ext = 'mp4';

                                $name = $video_hash . '.' . $video_ext;

                                if ($video_ftp->_storeToPicVideo($video, $data['escort_id'], $name)) {
                                    $data['natural-video'] = array('hash' => $video_hash, 'ext' => $video_ext, 'is_video' => 1);
                                }
                            }
                        }
                    }
                } else {
                    $validator->setError('natural-photo', 'Please upload photo with sheet of paper ');
                }
            }


            if ($validator->isValid()) {
                if ($data['real-photos']) {
                    $item = new Model_VerifyRequest(array(
                        'type' => Model_VerifyRequests::TYPE_IDCARD,
                        'escort_id' => $data['escort_id']
                    ));

                    $save_data = array(
                        'type' => Model_VerifyRequests::TYPE_IDCARD,
                        'escort_id' => $data['escort_id']
                    );

                    $verifyRequest = new Model_VerifyRequests();

                    $item = $verifyRequest->save($save_data);

                    foreach (array_merge($photos,$verify_photos) as $photo) {
                        $item->addPhoto(array(
                            'hash' => $photo['hash'],
                            'ext' => $photo['ext']
                        ));
                    }

                    if ($data['natural-video'] && is_array($data['natural-video'])) {
                        $item->addPhoto($data['natural-video']);
                    }

                    $data = array_merge(
                        array(
                            'idcard_id' => $item->id
                        ),
                        $data
                    );
                }


                $model = new Model_AgeVerification();
                $model->save($data);
                return $this->_forward('finish-age-verification');


            } else {

                $error_info = $validator->getStatus();
                $this->view->errors = $error_info['msgs'];

            }
        }

        if(!$this->_getParam('escort_id')){
            return $this->_redirect($this->view->getLink('private-v2'));
        }
        $this->view->escortId = $this->_getParam('escort_id');


    }

    public function finishAgeVerificationAction(){
        $this->view->layout()->setLayout('profile-steps');
    }
}
